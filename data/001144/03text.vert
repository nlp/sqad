<s>
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
(	(	kIx(	(
<g/>
*	*	kIx~	*
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1941	[number]	k4	1941
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
ekonom	ekonom	k1gMnSc1	ekonom
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
spoluzakladatel	spoluzakladatel	k1gMnSc1	spoluzakladatel
Občanské	občanský	k2eAgFnSc2d1	občanská
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
mezi	mezi	k7c7	mezi
roky	rok	k1gInPc7	rok
1991	[number]	k4	1991
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
její	její	k3xOp3gMnSc1	její
první	první	k4xOgMnSc1	první
předseda	předseda	k1gMnSc1	předseda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1992	[number]	k4	1992
<g/>
-	-	kIx~	-
<g/>
1998	[number]	k4	1998
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
svého	svůj	k3xOyFgInSc2	svůj
prvního	první	k4xOgNnSc2	první
a	a	k8xC	a
druhého	druhý	k4xOgInSc2	druhý
kabinetu	kabinet	k1gInSc2	kabinet
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
na	na	k7c4	na
čtyřleté	čtyřletý	k2eAgNnSc4d1	čtyřleté
období	období	k1gNnSc4	období
stal	stát	k5eAaPmAgMnS	stát
předsedou	předseda	k1gMnSc7	předseda
dolní	dolní	k2eAgFnSc2d1	dolní
komory	komora	k1gFnSc2	komora
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2003	[number]	k4	2003
<g/>
-	-	kIx~	-
<g/>
2013	[number]	k4	2013
zastával	zastávat	k5eAaImAgMnS	zastávat
úřad	úřad	k1gInSc4	úřad
prezidenta	prezident	k1gMnSc2	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
politiky	politika	k1gFnSc2	politika
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
bankovní	bankovní	k2eAgMnSc1d1	bankovní
úředník	úředník	k1gMnSc1	úředník
a	a	k8xC	a
prognostik	prognostik	k1gMnSc1	prognostik
<g/>
,	,	kIx,	,
po	po	k7c6	po
listopadu	listopad	k1gInSc6	listopad
1989	[number]	k4	1989
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ministrem	ministr	k1gMnSc7	ministr
financí	finance	k1gFnPc2	finance
ČSSR	ČSSR	kA	ČSSR
ve	v	k7c6	v
vládách	vláda	k1gFnPc6	vláda
národního	národní	k2eAgNnSc2d1	národní
porozumění	porozumění	k1gNnSc2	porozumění
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
národní	národní	k2eAgFnPc4d1	národní
oběti	oběť	k1gFnPc4	oběť
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
zastával	zastávat	k5eAaImAgMnS	zastávat
i	i	k8xC	i
post	post	k1gInSc4	post
vicepremiéra	vicepremiér	k1gMnSc2	vicepremiér
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
inicioval	iniciovat	k5eAaBmAgInS	iniciovat
založení	založení	k1gNnSc4	založení
Občanské	občanský	k2eAgFnSc2d1	občanská
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1992	[number]	k4	1992
<g/>
-	-	kIx~	-
<g/>
1997	[number]	k4	1997
byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
až	až	k6eAd1	až
do	do	k7c2	do
února	únor	k1gInSc2	únor
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
vykonával	vykonávat	k5eAaImAgMnS	vykonávat
z	z	k7c2	z
titulu	titul	k1gInSc2	titul
své	svůj	k3xOyFgFnSc2	svůj
funkce	funkce	k1gFnSc2	funkce
premiéra	premiéra	k1gFnSc1	premiéra
některé	některý	k3yIgFnSc2	některý
pravomoci	pravomoc	k1gFnSc2	pravomoc
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc3	pád
jím	jíst	k5eAaImIp1nS	jíst
vedené	vedený	k2eAgFnSc2d1	vedená
vlády	vláda	k1gFnSc2	vláda
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
nakrátko	nakrátko	k6eAd1	nakrátko
ztratil	ztratit	k5eAaPmAgMnS	ztratit
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
politický	politický	k2eAgInSc4d1	politický
vliv	vliv	k1gInSc4	vliv
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
do	do	k7c2	do
vysoké	vysoký	k2eAgFnSc2d1	vysoká
politiky	politika	k1gFnSc2	politika
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
již	již	k6eAd1	již
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
předsedou	předseda	k1gMnSc7	předseda
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
ODS	ODS	kA	ODS
prohrála	prohrát	k5eAaPmAgFnS	prohrát
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
na	na	k7c4	na
krátký	krátký	k2eAgInSc4d1	krátký
čas	čas	k1gInSc4	čas
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
z	z	k7c2	z
vrcholné	vrcholný	k2eAgFnSc2d1	vrcholná
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2003	[number]	k4	2003
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
v	v	k7c6	v
prezidentské	prezidentský	k2eAgFnSc6d1	prezidentská
volbě	volba	k1gFnSc6	volba
poprvé	poprvé	k6eAd1	poprvé
a	a	k8xC	a
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
pětileté	pětiletý	k2eAgNnSc4d1	pětileté
období	období	k1gNnSc4	období
byl	být	k5eAaImAgMnS	být
znovuzvolen	znovuzvolit	k5eAaPmNgMnS	znovuzvolit
prezidentem	prezident	k1gMnSc7	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
úřadu	úřad	k1gInSc2	úřad
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2013	[number]	k4	2013
opustil	opustit	k5eAaPmAgMnS	opustit
aktivní	aktivní	k2eAgFnSc4d1	aktivní
politiku	politika	k1gFnSc4	politika
a	a	k8xC	a
nadále	nadále	k6eAd1	nadále
se	se	k3xPyFc4	se
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
k	k	k7c3	k
tématům	téma	k1gNnPc3	téma
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
i	i	k8xC	i
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
k	k	k7c3	k
válce	válka	k1gFnSc3	válka
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
či	či	k8xC	či
Evropské	evropský	k2eAgFnSc3d1	Evropská
migrační	migrační	k2eAgFnSc3d1	migrační
krizi	krize	k1gFnSc3	krize
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
Klausů	Klaus	k1gMnPc2	Klaus
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
západočeské	západočeský	k2eAgFnSc2d1	Západočeská
obce	obec	k1gFnSc2	obec
Mileč	Mileč	k1gMnSc1	Mileč
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
Václav	Václav	k1gMnSc1	Václav
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
-	-	kIx~	-
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
povoláním	povolání	k1gNnSc7	povolání
účetní	účetní	k1gFnSc2	účetní
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
Marie	Maria	k1gFnSc2	Maria
rozená	rozený	k2eAgFnSc1d1	rozená
Kailová	Kailová	k1gFnSc1	Kailová
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
-	-	kIx~	-
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
pracovala	pracovat	k5eAaImAgFnS	pracovat
jako	jako	k9	jako
pokladní	pokladní	k1gFnSc1	pokladní
a	a	k8xC	a
průvodkyně	průvodkyně	k1gFnSc1	průvodkyně
turistů	turist	k1gMnPc2	turist
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
také	také	k6eAd1	také
příležitostnou	příležitostný	k2eAgFnSc7d1	příležitostná
autorkou	autorka	k1gFnSc7	autorka
básní	báseň	k1gFnPc2	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
byla	být	k5eAaImAgFnS	být
duchovně	duchovně	k6eAd1	duchovně
spřízněna	spříznit	k5eAaPmNgFnS	spříznit
s	s	k7c7	s
Církví	církev	k1gFnSc7	církev
československou	československý	k2eAgFnSc4d1	Československá
husitskou	husitský	k2eAgFnSc4d1	husitská
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
má	mít	k5eAaImIp3nS	mít
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
mladší	mladý	k2eAgFnSc4d2	mladší
sestru	sestra	k1gFnSc4	sestra
Alenu	Alena	k1gFnSc4	Alena
Jarochovou	Jarochův	k2eAgFnSc7d1	Jarochův
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
po	po	k7c6	po
okupaci	okupace	k1gFnSc6	okupace
Československa	Československo	k1gNnSc2	Československo
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
emigrovala	emigrovat	k5eAaBmAgFnS	emigrovat
i	i	k9	i
s	s	k7c7	s
manželem	manžel	k1gMnSc7	manžel
do	do	k7c2	do
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
žije	žít	k5eAaImIp3nS	žít
nedaleko	nedaleko	k7c2	nedaleko
Curychu	Curych	k1gInSc2	Curych
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
švýcarské	švýcarský	k2eAgNnSc1d1	švýcarské
občanství	občanství	k1gNnSc1	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Klausovi	Klausův	k2eAgMnPc1d1	Klausův
rodiče	rodič	k1gMnPc1	rodič
kladli	klást	k5eAaImAgMnP	klást
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
jeho	jeho	k3xOp3gInPc4	jeho
dobré	dobrý	k2eAgInPc4d1	dobrý
výsledky	výsledek	k1gInPc4	výsledek
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
a	a	k8xC	a
mimoškolní	mimoškolní	k2eAgFnPc4d1	mimoškolní
aktivity	aktivita	k1gFnPc4	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
působil	působit	k5eAaImAgMnS	působit
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
v	v	k7c6	v
Dětském	dětský	k2eAgInSc6d1	dětský
pěveckém	pěvecký	k2eAgInSc6d1	pěvecký
sboru	sbor	k1gInSc6	sbor
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
juniorským	juniorský	k2eAgMnSc7d1	juniorský
i	i	k8xC	i
dorosteneckým	dorostenecký	k2eAgMnSc7d1	dorostenecký
reprezentantem	reprezentant	k1gMnSc7	reprezentant
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
basketbalu	basketbal	k1gInSc6	basketbal
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
získat	získat	k5eAaPmF	získat
zkušenosti	zkušenost	k1gFnPc4	zkušenost
z	z	k7c2	z
pobytů	pobyt	k1gInPc2	pobyt
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
hrál	hrát	k5eAaImAgInS	hrát
celostátní	celostátní	k2eAgFnSc4d1	celostátní
první	první	k4xOgFnSc4	první
ligu	liga	k1gFnSc4	liga
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
sportu	sport	k1gInSc6	sport
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sportovním	sportovní	k2eAgInSc6d1	sportovní
oddíle	oddíl	k1gInSc6	oddíl
Dukla	Dukla	k1gFnSc1	Dukla
Praha	Praha	k1gFnSc1	Praha
pak	pak	k6eAd1	pak
také	také	k9	také
strávil	strávit	k5eAaPmAgInS	strávit
povinnou	povinný	k2eAgFnSc4d1	povinná
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
službu	služba	k1gFnSc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
studoval	studovat	k5eAaImAgMnS	studovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1958	[number]	k4	1958
<g/>
-	-	kIx~	-
<g/>
1963	[number]	k4	1963
ekonomiku	ekonomika	k1gFnSc4	ekonomika
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
obchodu	obchod	k1gInSc2	obchod
na	na	k7c6	na
Vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
ekonomy	ekonom	k1gMnPc4	ekonom
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ovlivnili	ovlivnit	k5eAaPmAgMnP	ovlivnit
jeho	jeho	k3xOp3gNnSc4	jeho
ekonomické	ekonomický	k2eAgNnSc4d1	ekonomické
uvažování	uvažování	k1gNnSc4	uvažování
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
uvedl	uvést	k5eAaPmAgMnS	uvést
Friedricha	Friedrich	k1gMnSc4	Friedrich
Hayeka	Hayeek	k1gMnSc4	Hayeek
<g/>
,	,	kIx,	,
Miltona	Milton	k1gMnSc4	Milton
Friedmana	Friedman	k1gMnSc4	Friedman
<g/>
,	,	kIx,	,
Ludwiga	Ludwiga	k1gFnSc1	Ludwiga
von	von	k1gInSc1	von
Misese	Misese	k1gFnSc1	Misese
<g/>
,	,	kIx,	,
Paula	Paul	k1gMnSc2	Paul
Samuelsona	Samuelson	k1gMnSc2	Samuelson
a	a	k8xC	a
Josefa	Josef	k1gMnSc2	Josef
Schumpetera	Schumpeter	k1gMnSc2	Schumpeter
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
pozdější	pozdní	k2eAgFnSc7d2	pozdější
manželkou	manželka	k1gFnSc7	manželka
Livií	Livie	k1gFnSc7	Livie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Oravy	Orava	k1gFnSc2	Orava
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
dva	dva	k4xCgMnPc4	dva
syny	syn	k1gMnPc4	syn
<g/>
,	,	kIx,	,
Václava	Václava	k1gFnSc1	Václava
(	(	kIx(	(
<g/>
*	*	kIx~	*
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jana	Jana	k1gFnSc1	Jana
(	(	kIx(	(
<g/>
*	*	kIx~	*
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
a	a	k8xC	a
pět	pět	k4xCc4	pět
vnoučat	vnouče	k1gNnPc2	vnouče
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
a	a	k8xC	a
úspěšném	úspěšný	k2eAgInSc6d1	úspěšný
konkursu	konkurs	k1gInSc6	konkurs
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
Klaus	Klaus	k1gMnSc1	Klaus
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Ekonomickém	ekonomický	k2eAgInSc6d1	ekonomický
ústavu	ústav	k1gInSc6	ústav
ČSAV	ČSAV	kA	ČSAV
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
dva	dva	k4xCgInPc4	dva
studijní	studijní	k2eAgInPc4d1	studijní
pobyty	pobyt	k1gInPc4	pobyt
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
na	na	k7c6	na
Università	Università	k1gFnSc6	Università
degli	degnout	k5eAaPmAgMnP	degnout
Studi	Stud	k1gMnPc1	Stud
Federico	Federico	k6eAd1	Federico
II	II	kA	II
v	v	k7c6	v
italské	italský	k2eAgFnSc6d1	italská
Neapoli	Neapol	k1gFnSc6	Neapol
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
na	na	k7c6	na
Cornellově	Cornellův	k2eAgFnSc6d1	Cornellova
univerzitě	univerzita	k1gFnSc6	univerzita
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
kandidát	kandidát	k1gMnSc1	kandidát
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Angažoval	angažovat	k5eAaBmAgInS	angažovat
se	se	k3xPyFc4	se
v	v	k7c6	v
reformních	reformní	k2eAgNnPc6d1	reformní
periodikách	periodikum	k1gNnPc6	periodikum
Tvář	tvář	k1gFnSc1	tvář
a	a	k8xC	a
Literární	literární	k2eAgFnPc1d1	literární
noviny	novina	k1gFnPc1	novina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
publikoval	publikovat	k5eAaBmAgMnS	publikovat
sloupky	sloupek	k1gInPc4	sloupek
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Dalimil	Dalimil	k1gMnSc1	Dalimil
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Pražském	pražský	k2eAgInSc6d1	pražský
jaru	jar	k1gInSc6	jar
byl	být	k5eAaImAgMnS	být
Klaus	Klaus	k1gMnSc1	Klaus
podle	podle	k7c2	podle
svých	svůj	k3xOyFgNnPc2	svůj
slov	slovo	k1gNnPc2	slovo
z	z	k7c2	z
politických	politický	k2eAgInPc2d1	politický
důvodů	důvod	k1gInPc2	důvod
nucen	nutit	k5eAaImNgMnS	nutit
opustit	opustit	k5eAaPmF	opustit
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
kariéru	kariéra	k1gFnSc4	kariéra
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Státní	státní	k2eAgFnSc2d1	státní
banky	banka	k1gFnSc2	banka
československé	československý	k2eAgFnSc2d1	Československá
(	(	kIx(	(
<g/>
SBČS	SBČS	kA	SBČS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
pozicích	pozice	k1gFnPc6	pozice
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
pořádal	pořádat	k5eAaImAgInS	pořádat
pravidelné	pravidelný	k2eAgInPc4d1	pravidelný
semináře	seminář	k1gInPc4	seminář
o	o	k7c6	o
ekonomických	ekonomický	k2eAgFnPc6d1	ekonomická
otázkách	otázka	k1gFnPc6	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Seminářů	seminář	k1gInPc2	seminář
se	se	k3xPyFc4	se
zúčastňovala	zúčastňovat	k5eAaImAgFnS	zúčastňovat
řada	řada	k1gFnSc1	řada
budoucích	budoucí	k2eAgMnPc2d1	budoucí
politiků	politik	k1gMnPc2	politik
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
seminářům	seminář	k1gInPc3	seminář
začala	začít	k5eAaPmAgNnP	začít
Klause	Klaus	k1gMnSc2	Klaus
sledovat	sledovat	k5eAaImF	sledovat
Státní	státní	k2eAgFnSc4d1	státní
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
(	(	kIx(	(
<g/>
StB	StB	k1gFnSc4	StB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
semináře	seminář	k1gInPc1	seminář
považovala	považovat	k5eAaImAgFnS	považovat
za	za	k7c2	za
"	"	kIx"	"
<g/>
pravicové	pravicový	k2eAgFnSc2d1	pravicová
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Založila	založit	k5eAaPmAgFnS	založit
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
a	a	k8xC	a
vedla	vést	k5eAaImAgFnS	vést
svazek	svazek	k1gInSc4	svazek
pod	pod	k7c7	pod
krycím	krycí	k2eAgNnSc7d1	krycí
jménem	jméno	k1gNnSc7	jméno
Kluk	kluk	k1gMnSc1	kluk
<g/>
,	,	kIx,	,
nasadila	nasadit	k5eAaPmAgFnS	nasadit
na	na	k7c4	na
Klause	Klaus	k1gMnSc4	Klaus
své	svůj	k3xOyFgMnPc4	svůj
agenty	agent	k1gMnPc4	agent
<g/>
,	,	kIx,	,
odposlouchávala	odposlouchávat	k5eAaImAgFnS	odposlouchávat
jeho	jeho	k3xOp3gInPc4	jeho
telefony	telefon	k1gInPc4	telefon
<g/>
,	,	kIx,	,
tajně	tajně	k6eAd1	tajně
mu	on	k3xPp3gMnSc3	on
prohledala	prohledat	k5eAaPmAgFnS	prohledat
kancelář	kancelář	k1gFnSc4	kancelář
a	a	k8xC	a
nasadila	nasadit	k5eAaPmAgFnS	nasadit
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
odposlouchávací	odposlouchávací	k2eAgFnPc4d1	odposlouchávací
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
StB	StB	k1gFnSc1	StB
pořádání	pořádání	k1gNnSc2	pořádání
dalších	další	k2eAgInPc2d1	další
seminářů	seminář	k1gInPc2	seminář
znemožnila	znemožnit	k5eAaPmAgFnS	znemožnit
a	a	k8xC	a
svazek	svazek	k1gInSc1	svazek
11	[number]	k4	11
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
t.	t.	k?	t.
r.	r.	kA	r.
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
se	se	k3xPyFc4	se
Klaus	Klaus	k1gMnSc1	Klaus
vrátil	vrátit	k5eAaPmAgMnS	vrátit
k	k	k7c3	k
akademické	akademický	k2eAgFnSc3d1	akademická
činnosti	činnost	k1gFnSc3	činnost
jako	jako	k8xC	jako
analytik	analytika	k1gFnPc2	analytika
v	v	k7c6	v
Prognostickém	prognostický	k2eAgInSc6d1	prognostický
ústavu	ústav	k1gInSc6	ústav
ČSAV	ČSAV	kA	ČSAV
<g/>
.	.	kIx.	.
</s>
<s>
StB	StB	k?	StB
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1988	[number]	k4	1988
založila	založit	k5eAaPmAgFnS	založit
další	další	k2eAgInSc4d1	další
svazek	svazek	k1gInSc4	svazek
pod	pod	k7c7	pod
krycím	krycí	k2eAgNnSc7d1	krycí
jménem	jméno	k1gNnSc7	jméno
Rek	rek	k1gMnSc1	rek
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
bylo	být	k5eAaImAgNnS	být
důvodem	důvod	k1gInSc7	důvod
obnoveného	obnovený	k2eAgInSc2d1	obnovený
zájmu	zájem	k1gInSc2	zájem
StB	StB	k1gFnSc2	StB
o	o	k7c4	o
Klause	Klaus	k1gMnSc4	Klaus
<g/>
,	,	kIx,	,
svazek	svazek	k1gInSc1	svazek
byl	být	k5eAaImAgInS	být
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1989	[number]	k4	1989
skartován	skartován	k2eAgInSc1d1	skartován
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k9	již
Klaus	Klaus	k1gMnSc1	Klaus
cestoval	cestovat	k5eAaImAgMnS	cestovat
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
včetně	včetně	k7c2	včetně
nekomunistických	komunistický	k2eNgFnPc2d1	nekomunistická
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
v	v	k7c6	v
západních	západní	k2eAgNnPc6d1	západní
médiích	médium	k1gNnPc6	médium
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Zákona	zákon	k1gInSc2	zákon
o	o	k7c4	o
zabezpečení	zabezpečení	k1gNnSc4	zabezpečení
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
funkce	funkce	k1gFnSc2	funkce
(	(	kIx(	(
<g/>
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
48	[number]	k4	48
<g/>
/	/	kIx~	/
<g/>
2004	[number]	k4	2004
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
bývalý	bývalý	k2eAgMnSc1d1	bývalý
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
nárok	nárok	k1gInSc1	nárok
na	na	k7c4	na
rentu	renta	k1gFnSc4	renta
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
50	[number]	k4	50
tisíc	tisíc	k4xCgInPc2	tisíc
Kč	Kč	kA	Kč
měsíčně	měsíčně	k6eAd1	měsíčně
<g/>
.	.	kIx.	.
</s>
<s>
Renta	renta	k1gFnSc1	renta
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
splatná	splatný	k2eAgFnSc1d1	splatná
předem	předem	k6eAd1	předem
vždy	vždy	k6eAd1	vždy
do	do	k7c2	do
pěti	pět	k4xCc2	pět
pracovních	pracovní	k2eAgInPc2d1	pracovní
dnů	den	k1gInPc2	den
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
příslušného	příslušný	k2eAgInSc2d1	příslušný
kalendářního	kalendářní	k2eAgInSc2d1	kalendářní
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
má	mít	k5eAaImIp3nS	mít
bývalý	bývalý	k2eAgMnSc1d1	bývalý
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
nárok	nárok	k1gInSc1	nárok
na	na	k7c4	na
paušální	paušální	k2eAgFnSc4d1	paušální
náhradu	náhrada	k1gFnSc4	náhrada
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
kryje	krýt	k5eAaImIp3nS	krýt
zejména	zejména	k9	zejména
nájemné	nájemné	k1gNnSc1	nájemné
kanceláře	kancelář	k1gFnSc2	kancelář
a	a	k8xC	a
odměnu	odměna	k1gFnSc4	odměna
pro	pro	k7c4	pro
asistenta	asistent	k1gMnSc4	asistent
<g/>
,	,	kIx,	,
v	v	k7c6	v
celkové	celkový	k2eAgFnSc6d1	celková
výši	výše	k1gFnSc6	výše
dalších	další	k2eAgInPc2d1	další
50	[number]	k4	50
tisíc	tisíc	k4xCgInPc2	tisíc
Kč	Kč	kA	Kč
měsíčně	měsíčně	k6eAd1	měsíčně
<g/>
.	.	kIx.	.
</s>
<s>
Paušální	paušální	k2eAgFnSc1d1	paušální
náhrada	náhrada	k1gFnSc1	náhrada
je	být	k5eAaImIp3nS	být
splatná	splatný	k2eAgFnSc1d1	splatná
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
rentou	renta	k1gFnSc7	renta
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
po	po	k7c6	po
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
se	se	k3xPyFc4	se
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c4	na
doporučení	doporučení	k1gNnSc4	doporučení
Rity	Rita	k1gFnSc2	Rita
Klímové	Klímová	k1gFnSc2	Klímová
do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
Občanského	občanský	k2eAgNnSc2d1	občanské
fóra	fórum	k1gNnSc2	fórum
(	(	kIx(	(
<g/>
OF	OF	kA	OF
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
osobností	osobnost	k1gFnPc2	osobnost
z	z	k7c2	z
jiného	jiný	k2eAgInSc2d1	jiný
okruhu	okruh	k1gInSc2	okruh
než	než	k8xS	než
disidentského	disidentský	k2eAgNnSc2d1	disidentské
a	a	k8xC	a
uměleckého	umělecký	k2eAgNnSc2d1	umělecké
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1989	[number]	k4	1989
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
československým	československý	k2eAgMnSc7d1	československý
ministrem	ministr	k1gMnSc7	ministr
financí	finance	k1gFnPc2	finance
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
Mariána	Marián	k1gMnSc2	Marián
Čalfy	Čalf	k1gInPc4	Čalf
<g/>
,	,	kIx,	,
nominovaným	nominovaný	k2eAgInSc7d1	nominovaný
za	za	k7c4	za
Občanské	občanský	k2eAgNnSc4d1	občanské
fórum	fórum	k1gNnSc4	fórum
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
června	červen	k1gInSc2	červen
1990	[number]	k4	1990
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
na	na	k7c6	na
stejném	stejný	k2eAgInSc6d1	stejný
postu	post	k1gInSc6	post
pod	pod	k7c7	pod
Čalfovým	Čalfův	k2eAgNnSc7d1	Čalfův
vedením	vedení	k1gNnSc7	vedení
také	také	k9	také
ve	v	k7c6	v
federální	federální	k2eAgFnSc6d1	federální
vládě	vláda	k1gFnSc6	vláda
ČSFR	ČSFR	kA	ČSFR
a	a	k8xC	a
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1991	[number]	k4	1991
byl	být	k5eAaImAgInS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
i	i	k9	i
jejím	její	k3xOp3gMnSc7	její
místopředsedou	místopředseda	k1gMnSc7	místopředseda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1992	[number]	k4	1992
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
předsedou	předseda	k1gMnSc7	předseda
české	český	k2eAgFnSc2d1	Česká
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
až	až	k9	až
do	do	k7c2	do
demise	demise	k1gFnSc2	demise
své	svůj	k3xOyFgFnSc2	svůj
vlastní	vlastní	k2eAgFnSc2d1	vlastní
druhé	druhý	k4xOgFnSc2	druhý
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
tak	tak	k8xC	tak
Klaus	Klaus	k1gMnSc1	Klaus
nepřetržitě	přetržitě	k6eNd1	přetržitě
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
klíčových	klíčový	k2eAgMnPc2d1	klíčový
členů	člen	k1gMnPc2	člen
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
tím	ten	k3xDgInSc7	ten
zdaleka	zdaleka	k6eAd1	zdaleka
nejvlivnějším	vlivný	k2eAgInSc7d3	nejvlivnější
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
se	se	k3xPyFc4	se
směrodatně	směrodatně	k6eAd1	směrodatně
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
přeměně	přeměna	k1gFnSc6	přeměna
socialistické	socialistický	k2eAgFnSc2d1	socialistická
ekonomiky	ekonomika	k1gFnSc2	ekonomika
v	v	k7c4	v
kapitalistickou	kapitalistický	k2eAgFnSc4d1	kapitalistická
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdělení	rozdělení	k1gNnSc4	rozdělení
Československa	Československo	k1gNnSc2	Československo
i	i	k9	i
na	na	k7c4	na
formování	formování	k1gNnSc4	formování
politické	politický	k2eAgFnSc2d1	politická
scény	scéna	k1gFnSc2	scéna
transformací	transformace	k1gFnPc2	transformace
Občanského	občanský	k2eAgNnSc2d1	občanské
fóra	fórum	k1gNnSc2	fórum
a	a	k8xC	a
vytvořením	vytvoření	k1gNnSc7	vytvoření
pravicové	pravicový	k2eAgFnSc2d1	pravicová
Občanské	občanský	k2eAgFnSc2d1	občanská
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Občanské	občanský	k2eAgNnSc1d1	občanské
fórum	fórum	k1gNnSc1	fórum
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
jako	jako	k9	jako
názorově	názorově	k6eAd1	názorově
široce	široko	k6eAd1	široko
rozkročené	rozkročený	k2eAgNnSc4d1	rozkročené
nehierarchické	hierarchický	k2eNgNnSc4d1	nehierarchické
hnutí	hnutí	k1gNnSc4	hnutí
se	s	k7c7	s
slabými	slabý	k2eAgFnPc7d1	slabá
strukturami	struktura	k1gFnPc7	struktura
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
omezovalo	omezovat	k5eAaImAgNnS	omezovat
jeho	jeho	k3xOp3gFnSc4	jeho
akceschopnost	akceschopnost	k1gFnSc4	akceschopnost
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1990	[number]	k4	1990
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
začaly	začít	k5eAaPmAgInP	začít
projevovat	projevovat	k5eAaImF	projevovat
spory	spor	k1gInPc1	spor
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
směřování	směřování	k1gNnSc4	směřování
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
se	se	k3xPyFc4	se
do	do	k7c2	do
nich	on	k3xPp3gMnPc2	on
zapojil	zapojit	k5eAaPmAgMnS	zapojit
několik	několik	k4yIc1	několik
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
<g/>
,	,	kIx,	,
konaných	konaný	k2eAgNnPc6d1	konané
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Zastával	zastávat	k5eAaImAgMnS	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
OF	OF	kA	OF
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
přeměnit	přeměnit	k5eAaPmF	přeměnit
na	na	k7c4	na
jasně	jasně	k6eAd1	jasně
definovaný	definovaný	k2eAgInSc4d1	definovaný
politický	politický	k2eAgInSc4d1	politický
subjekt	subjekt	k1gInSc4	subjekt
s	s	k7c7	s
pevnější	pevný	k2eAgFnSc7d2	pevnější
hierarchií	hierarchie	k1gFnSc7	hierarchie
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
schopen	schopen	k2eAgInSc1d1	schopen
prosadit	prosadit	k5eAaPmF	prosadit
potřebné	potřebný	k2eAgFnPc4d1	potřebná
reformy	reforma	k1gFnPc4	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
již	již	k6eAd1	již
před	před	k7c7	před
volbami	volba	k1gFnPc7	volba
z	z	k7c2	z
OF	OF	kA	OF
odešly	odejít	k5eAaPmAgInP	odejít
některé	některý	k3yIgFnPc4	některý
levicové	levicový	k2eAgFnPc4d1	levicová
skupiny	skupina	k1gFnPc4	skupina
včetně	včetně	k7c2	včetně
sociální	sociální	k2eAgFnSc2d1	sociální
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
,	,	kIx,	,
hnutí	hnutí	k1gNnSc1	hnutí
se	se	k3xPyFc4	se
posunulo	posunout	k5eAaPmAgNnS	posunout
politicky	politicky	k6eAd1	politicky
doprava	doprava	k6eAd1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vítězných	vítězný	k2eAgFnPc6d1	vítězná
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
OF	OF	kA	OF
zesílilo	zesílit	k5eAaPmAgNnS	zesílit
napětí	napětí	k1gNnSc1	napětí
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
vyhranily	vyhranit	k5eAaPmAgFnP	vyhranit
tři	tři	k4xCgFnPc1	tři
názorové	názorový	k2eAgFnPc1d1	názorová
frakce	frakce	k1gFnPc1	frakce
<g/>
:	:	kIx,	:
Klausem	Klaus	k1gMnSc7	Klaus
vedená	vedený	k2eAgFnSc1d1	vedená
pravice	pravice	k1gFnSc1	pravice
<g/>
,	,	kIx,	,
centristé	centrista	k1gMnPc1	centrista
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
bývalých	bývalý	k2eAgMnPc2d1	bývalý
disidentů	disident	k1gMnPc2	disident
(	(	kIx(	(
<g/>
Pavel	Pavel	k1gMnSc1	Pavel
Rychetský	Rychetský	k1gMnSc1	Rychetský
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Dienstbier	Dienstbier	k1gMnSc1	Dienstbier
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Pithart	Pithart	k1gInSc1	Pithart
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
)	)	kIx)	)
a	a	k8xC	a
levice	levice	k1gFnSc1	levice
zastoupená	zastoupený	k2eAgFnSc1d1	zastoupená
hlavně	hlavně	k9	hlavně
členy	člen	k1gMnPc4	člen
klubu	klub	k1gInSc2	klub
Obroda	obroda	k1gFnSc1	obroda
<g/>
.	.	kIx.	.
</s>
<s>
Kongres	kongres	k1gInSc1	kongres
OF	OF	kA	OF
v	v	k7c6	v
září	září	k1gNnSc6	září
1990	[number]	k4	1990
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
o	o	k7c6	o
budoucnosti	budoucnost	k1gFnSc6	budoucnost
hnutí	hnutí	k1gNnSc2	hnutí
přesvědčivě	přesvědčivě	k6eAd1	přesvědčivě
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
Klausově	Klausův	k2eAgFnSc3d1	Klausova
skupině	skupina	k1gFnSc3	skupina
radikálních	radikální	k2eAgMnPc2d1	radikální
reformátorů	reformátor	k1gMnPc2	reformátor
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
pokusila	pokusit	k5eAaPmAgFnS	pokusit
OF	OF	kA	OF
převzít	převzít	k5eAaPmF	převzít
<g/>
.	.	kIx.	.
</s>
<s>
Klaus	Klaus	k1gMnSc1	Klaus
problém	problém	k1gInSc4	problém
transformace	transformace	k1gFnSc2	transformace
Občanského	občanský	k2eAgNnSc2d1	občanské
fóra	fórum	k1gNnSc2	fórum
představil	představit	k5eAaPmAgInS	představit
jako	jako	k9	jako
střet	střet	k1gInSc1	střet
mezi	mezi	k7c7	mezi
pravicí	pravice	k1gFnSc7	pravice
a	a	k8xC	a
levicí	levice	k1gFnSc7	levice
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1990	[number]	k4	1990
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
57	[number]	k4	57
pravicových	pravicový	k2eAgMnPc2d1	pravicový
poslanců	poslanec	k1gMnPc2	poslanec
OF	OF	kA	OF
Meziparlamentní	meziparlamentní	k2eAgInSc1d1	meziparlamentní
klub	klub	k1gInSc1	klub
demokratické	demokratický	k2eAgFnSc2d1	demokratická
pravice	pravice	k1gFnSc2	pravice
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
byl	být	k5eAaImAgMnS	být
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
zvolen	zvolit	k5eAaPmNgMnS	zvolit
předsedou	předseda	k1gMnSc7	předseda
OF	OF	kA	OF
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
na	na	k7c4	na
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
získal	získat	k5eAaPmAgMnS	získat
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
českou	český	k2eAgFnSc4d1	Česká
politiku	politika	k1gFnSc4	politika
a	a	k8xC	a
převzal	převzít	k5eAaPmAgInS	převzít
politickou	politický	k2eAgFnSc4d1	politická
základnu	základna	k1gFnSc4	základna
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
doposud	doposud	k6eAd1	doposud
ovládal	ovládat	k5eAaImAgMnS	ovládat
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
pravice	pravice	k1gFnSc1	pravice
neúspěšně	úspěšně	k6eNd1	úspěšně
pokusila	pokusit	k5eAaPmAgFnS	pokusit
vyloučit	vyloučit	k5eAaPmF	vyloučit
levicovou	levicový	k2eAgFnSc4d1	levicová
frakci	frakce	k1gFnSc4	frakce
z	z	k7c2	z
OF	OF	kA	OF
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1990	[number]	k4	1990
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
Občanské	občanský	k2eAgNnSc1d1	občanské
fórum	fórum	k1gNnSc1	fórum
změnilo	změnit	k5eAaPmAgNnS	změnit
v	v	k7c4	v
politickou	politický	k2eAgFnSc4d1	politická
stranu	strana	k1gFnSc4	strana
s	s	k7c7	s
jasnou	jasný	k2eAgFnSc7d1	jasná
strukturou	struktura	k1gFnSc7	struktura
a	a	k8xC	a
disciplínou	disciplína	k1gFnSc7	disciplína
<g/>
.	.	kIx.	.
</s>
<s>
Kongres	kongres	k1gInSc1	kongres
OF	OF	kA	OF
<g/>
,	,	kIx,	,
konaný	konaný	k2eAgMnSc1d1	konaný
12	[number]	k4	12
<g/>
.	.	kIx.	.
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
návrh	návrh	k1gInSc1	návrh
schválil	schválit	k5eAaPmAgInS	schválit
hlasy	hlas	k1gInPc4	hlas
126	[number]	k4	126
delegátů	delegát	k1gMnPc2	delegát
ze	z	k7c2	z
175	[number]	k4	175
a	a	k8xC	a
přijal	přijmout	k5eAaPmAgInS	přijmout
prozápadní	prozápadní	k2eAgInSc4d1	prozápadní
a	a	k8xC	a
protisocialistický	protisocialistický	k2eAgInSc4d1	protisocialistický
program	program	k1gInSc4	program
<g/>
.	.	kIx.	.
</s>
<s>
Středoví	středový	k2eAgMnPc1d1	středový
a	a	k8xC	a
levicoví	levicový	k2eAgMnPc1d1	levicový
politici	politik	k1gMnPc1	politik
OF	OF	kA	OF
však	však	k9	však
chtěli	chtít	k5eAaImAgMnP	chtít
přeměně	přeměna	k1gFnSc3	přeměna
hnutí	hnutí	k1gNnSc2	hnutí
v	v	k7c4	v
pravicovou	pravicový	k2eAgFnSc4d1	pravicová
stranu	strana	k1gFnSc4	strana
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
rozkol	rozkol	k1gInSc1	rozkol
v	v	k7c6	v
OF	OF	kA	OF
vyhrotil	vyhrotit	k5eAaPmAgMnS	vyhrotit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
schůzce	schůzka	k1gFnSc6	schůzka
u	u	k7c2	u
prezidenta	prezident	k1gMnSc2	prezident
Havla	Havel	k1gMnSc2	Havel
v	v	k7c6	v
Lánech	lán	k1gInPc6	lán
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1991	[number]	k4	1991
se	se	k3xPyFc4	se
obě	dva	k4xCgNnPc1	dva
křídla	křídlo	k1gNnPc1	křídlo
dohodla	dohodnout	k5eAaPmAgNnP	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
alespoň	alespoň	k9	alespoň
do	do	k7c2	do
voleb	volba	k1gFnPc2	volba
<g/>
,	,	kIx,	,
plánovaných	plánovaný	k2eAgFnPc2d1	plánovaná
na	na	k7c4	na
červen	červen	k1gInSc4	červen
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
OF	OF	kA	OF
existovat	existovat	k5eAaImF	existovat
jako	jako	k8xS	jako
koalice	koalice	k1gFnSc1	koalice
dvou	dva	k4xCgFnPc2	dva
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
pravicové	pravicový	k2eAgFnSc2d1	pravicová
strany	strana	k1gFnSc2	strana
vedené	vedený	k2eAgFnSc2d1	vedená
Klausem	Klaus	k1gMnSc7	Klaus
a	a	k8xC	a
centristického	centristický	k2eAgNnSc2d1	centristické
hnutí	hnutí	k1gNnSc2	hnutí
kolem	kolem	k7c2	kolem
bývalých	bývalý	k2eAgMnPc2d1	bývalý
disidentů	disident	k1gMnPc2	disident
<g/>
.	.	kIx.	.
</s>
<s>
Napětí	napětí	k1gNnSc1	napětí
však	však	k9	však
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
a	a	k8xC	a
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
svolání	svolání	k1gNnSc3	svolání
mimořádného	mimořádný	k2eAgInSc2d1	mimořádný
kongresu	kongres	k1gInSc2	kongres
na	na	k7c4	na
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
schválil	schválit	k5eAaPmAgInS	schválit
dohodu	dohoda	k1gFnSc4	dohoda
z	z	k7c2	z
Lán	lán	k1gInSc4	lán
a	a	k8xC	a
v	v	k7c6	v
jejím	její	k3xOp3gMnSc6	její
duchu	duch	k1gMnSc6	duch
přeměnil	přeměnit	k5eAaPmAgMnS	přeměnit
Občanské	občanský	k2eAgNnSc1d1	občanské
fórum	fórum	k1gNnSc1	fórum
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
nadále	nadále	k6eAd1	nadále
skládalo	skládat	k5eAaImAgNnS	skládat
z	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
kolektivních	kolektivní	k2eAgInPc2d1	kolektivní
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
z	z	k7c2	z
pravicové	pravicový	k2eAgFnSc2d1	pravicová
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
z	z	k7c2	z
hnutí	hnutí	k1gNnSc2	hnutí
tvořeného	tvořený	k2eAgInSc2d1	tvořený
Klausovými	Klausův	k2eAgMnPc7d1	Klausův
oponenty	oponent	k1gMnPc7	oponent
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
členové	člen	k1gMnPc1	člen
měli	mít	k5eAaImAgMnP	mít
samostatně	samostatně	k6eAd1	samostatně
působit	působit	k5eAaImF	působit
pod	pod	k7c7	pod
jinými	jiný	k2eAgNnPc7d1	jiné
jmény	jméno	k1gNnPc7	jméno
a	a	k8xC	a
rozdělit	rozdělit	k5eAaPmF	rozdělit
si	se	k3xPyFc3	se
majetek	majetek	k1gInSc4	majetek
OF	OF	kA	OF
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Občanské	občanský	k2eAgNnSc4d1	občanské
fórum	fórum	k1gNnSc4	fórum
samo	sám	k3xTgNnSc1	sám
mělo	mít	k5eAaImAgNnS	mít
zůstat	zůstat	k5eAaPmF	zůstat
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
jen	jen	k9	jen
do	do	k7c2	do
příštích	příští	k2eAgFnPc2d1	příští
voleb	volba	k1gFnPc2	volba
jako	jako	k8xS	jako
název	název	k1gInSc1	název
společné	společný	k2eAgFnSc2d1	společná
poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
frakce	frakce	k1gFnSc2	frakce
<g/>
.	.	kIx.	.
</s>
<s>
Pravice	pravice	k1gFnSc1	pravice
si	se	k3xPyFc3	se
vybrala	vybrat	k5eAaPmAgFnS	vybrat
jméno	jméno	k1gNnSc4	jméno
Občanská	občanský	k2eAgFnSc1d1	občanská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
ODS	ODS	kA	ODS
<g/>
)	)	kIx)	)
a	a	k8xC	a
měla	mít	k5eAaImAgNnP	mít
podporu	podpora	k1gFnSc4	podpora
zejména	zejména	k9	zejména
v	v	k7c6	v
dosavadních	dosavadní	k2eAgFnPc6d1	dosavadní
krajských	krajský	k2eAgFnPc6d1	krajská
organizacích	organizace	k1gFnPc6	organizace
OF	OF	kA	OF
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
centristé	centrista	k1gMnPc1	centrista
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgFnSc4d1	zahrnující
většinu	většina	k1gFnSc4	většina
předklausovského	předklausovský	k2eAgNnSc2d1	předklausovský
vedení	vedení	k1gNnSc2	vedení
OF	OF	kA	OF
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
dali	dát	k5eAaPmAgMnP	dát
název	název	k1gInSc4	název
Občanské	občanský	k2eAgNnSc4d1	občanské
hnutí	hnutí	k1gNnSc4	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Přípravný	přípravný	k2eAgInSc4d1	přípravný
výbor	výbor	k1gInSc4	výbor
ODS	ODS	kA	ODS
s	s	k7c7	s
Klausem	Klaus	k1gMnSc7	Klaus
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
se	se	k3xPyFc4	se
ustavil	ustavit	k5eAaPmAgMnS	ustavit
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1991	[number]	k4	1991
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
pak	pak	k6eAd1	pak
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
ustavující	ustavující	k2eAgInSc1d1	ustavující
kongres	kongres	k1gInSc1	kongres
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
zvolil	zvolit	k5eAaPmAgMnS	zvolit
Klause	Klaus	k1gMnSc4	Klaus
prvním	první	k4xOgMnSc7	první
předsedou	předseda	k1gMnSc7	předseda
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
ODS	ODS	kA	ODS
se	se	k3xPyFc4	se
definovala	definovat	k5eAaBmAgFnS	definovat
jako	jako	k9	jako
pravicová	pravicový	k2eAgFnSc1d1	pravicová
strana	strana	k1gFnSc1	strana
s	s	k7c7	s
konzervativním	konzervativní	k2eAgInSc7d1	konzervativní
programem	program	k1gInSc7	program
a	a	k8xC	a
přijala	přijmout	k5eAaPmAgFnS	přijmout
programové	programový	k2eAgNnSc4d1	programové
prohlášení	prohlášení	k1gNnSc4	prohlášení
Cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
prosperitě	prosperita	k1gFnSc3	prosperita
.	.	kIx.	.
</s>
<s>
Vytvořením	vytvoření	k1gNnSc7	vytvoření
ODS	ODS	kA	ODS
si	se	k3xPyFc3	se
Klaus	Klaus	k1gMnSc1	Klaus
zajistil	zajistit	k5eAaPmAgMnS	zajistit
pevnou	pevný	k2eAgFnSc4d1	pevná
mocenskou	mocenský	k2eAgFnSc4d1	mocenská
základnu	základna	k1gFnSc4	základna
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
pomohl	pomoct	k5eAaPmAgMnS	pomoct
k	k	k7c3	k
rychlému	rychlý	k2eAgNnSc3d1	rychlé
ustavení	ustavení	k1gNnSc3	ustavení
politického	politický	k2eAgNnSc2d1	politické
spektra	spektrum	k1gNnSc2	spektrum
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgNnSc1	jaký
je	být	k5eAaImIp3nS	být
běžné	běžný	k2eAgNnSc1d1	běžné
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1992	[number]	k4	1992
pak	pak	k8xC	pak
ODS	ODS	kA	ODS
v	v	k7c6	v
koalici	koalice	k1gFnSc6	koalice
s	s	k7c7	s
Křesťanskodemokratickou	křesťanskodemokratický	k2eAgFnSc7d1	Křesťanskodemokratická
stranou	strana	k1gFnSc7	strana
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
náskokem	náskok	k1gInSc7	náskok
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
-	-	kIx~	-
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc4	ten
nejvýraznější	výrazný	k2eAgNnSc4d3	nejvýraznější
Klausovo	Klausův	k2eAgNnSc4d1	Klausovo
volební	volební	k2eAgNnSc4d1	volební
vítězství	vítězství	k1gNnSc4	vítězství
vůbec	vůbec	k9	vůbec
-	-	kIx~	-
zatímco	zatímco	k8xS	zatímco
Občanské	občanský	k2eAgNnSc1d1	občanské
hnutí	hnutí	k1gNnSc1	hnutí
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
pod	pod	k7c7	pod
pětiprocentním	pětiprocentní	k2eAgInSc7d1	pětiprocentní
ziskem	zisk	k1gInSc7	zisk
<g/>
,	,	kIx,	,
nutným	nutný	k2eAgNnSc7d1	nutné
ke	k	k7c3	k
vstupu	vstup	k1gInSc3	vstup
do	do	k7c2	do
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
několika	několik	k4yIc6	několik
letech	let	k1gInPc6	let
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
pádu	pád	k1gInSc2	pád
komunismu	komunismus	k1gInSc2	komunismus
mělo	mít	k5eAaImAgNnS	mít
Československo	Československo	k1gNnSc1	Československo
centrálně	centrálně	k6eAd1	centrálně
plánované	plánovaný	k2eAgNnSc1d1	plánované
hospodářství	hospodářství	k1gNnSc1	hospodářství
a	a	k8xC	a
drtivá	drtivý	k2eAgFnSc1d1	drtivá
většina	většina	k1gFnSc1	většina
podniků	podnik	k1gInPc2	podnik
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
ekonomické	ekonomický	k2eAgNnSc1d1	ekonomické
vedení	vedení	k1gNnSc1	vedení
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
převažovali	převažovat	k5eAaImAgMnP	převažovat
bývalí	bývalý	k2eAgMnPc1d1	bývalý
pracovníci	pracovník	k1gMnPc1	pracovník
Prognostického	prognostický	k2eAgInSc2d1	prognostický
ústavu	ústav	k1gInSc2	ústav
ČSAV	ČSAV	kA	ČSAV
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
shodovalo	shodovat	k5eAaImAgNnS	shodovat
na	na	k7c6	na
nutnosti	nutnost	k1gFnSc6	nutnost
zavedení	zavedení	k1gNnSc1	zavedení
tržní	tržní	k2eAgFnSc2d1	tržní
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Představy	představa	k1gFnPc1	představa
o	o	k7c6	o
způsobu	způsob	k1gInSc6	způsob
a	a	k8xC	a
rychlosti	rychlost	k1gFnSc3	rychlost
transformace	transformace	k1gFnSc2	transformace
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
lišily	lišit	k5eAaImAgFnP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
patřil	patřit	k5eAaImAgMnS	patřit
mezi	mezi	k7c4	mezi
zastánce	zastánce	k1gMnSc4	zastánce
co	co	k8xS	co
nejrychlejší	rychlý	k2eAgFnSc1d3	nejrychlejší
privatizace	privatizace	k1gFnSc1	privatizace
a	a	k8xC	a
uvolnění	uvolnění	k1gNnSc1	uvolnění
cen	cena	k1gFnPc2	cena
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
radikály	radikál	k1gInPc1	radikál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zasazoval	zasazovat	k5eAaImAgMnS	zasazovat
se	se	k3xPyFc4	se
za	za	k7c4	za
trh	trh	k1gInSc4	trh
"	"	kIx"	"
<g/>
bez	bez	k7c2	bez
přívlastků	přívlastek	k1gInPc2	přívlastek
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
znamená	znamenat	k5eAaImIp3nS	znamenat
tržní	tržní	k2eAgInSc1d1	tržní
hospodářství	hospodářství	k1gNnSc2	hospodářství
s	s	k7c7	s
doplňkovou	doplňkový	k2eAgFnSc7d1	doplňková
sociální	sociální	k2eAgFnSc7d1	sociální
politikou	politika	k1gFnSc7	politika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikoliv	nikoliv	k9	nikoliv
sociální	sociální	k2eAgInSc4d1	sociální
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
Klaus	Klaus	k1gMnSc1	Klaus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dosavadní	dosavadní	k2eAgInSc1d1	dosavadní
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
systém	systém	k1gInSc1	systém
zkolaboval	zkolabovat	k5eAaPmAgInS	zkolabovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
třeba	třeba	k6eAd1	třeba
rychle	rychle	k6eAd1	rychle
vytvořit	vytvořit	k5eAaPmF	vytvořit
nový	nový	k2eAgInSc4d1	nový
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
svých	svůj	k3xOyFgNnPc2	svůj
slov	slovo	k1gNnPc2	slovo
obával	obávat	k5eAaImAgMnS	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
státní	státní	k2eAgInPc4d1	státní
podniky	podnik	k1gInPc4	podnik
vytunelují	vytunelovat	k5eAaPmIp3nP	vytunelovat
jejich	jejich	k3xOp3gMnPc1	jejich
bývalí	bývalý	k2eAgMnPc1d1	bývalý
komunističtí	komunistický	k2eAgMnPc1d1	komunistický
manažeři	manažer	k1gMnPc1	manažer
<g/>
.	.	kIx.	.
</s>
<s>
Dostal	dostat	k5eAaPmAgMnS	dostat
se	se	k3xPyFc4	se
tím	ten	k3xDgMnSc7	ten
do	do	k7c2	do
sporu	spor	k1gInSc2	spor
s	s	k7c7	s
místopředsedou	místopředseda	k1gMnSc7	místopředseda
federální	federální	k2eAgFnSc2d1	federální
vlády	vláda	k1gFnSc2	vláda
Valtrem	Valtr	k1gMnSc7	Valtr
Komárkem	Komárek	k1gMnSc7	Komárek
<g/>
,	,	kIx,	,
ekonomickými	ekonomický	k2eAgMnPc7d1	ekonomický
poradci	poradce	k1gMnPc1	poradce
českého	český	k2eAgMnSc2d1	český
premiéra	premiér	k1gMnSc2	premiér
Petra	Petr	k1gMnSc2	Petr
Pitharta	Pithart	k1gMnSc2	Pithart
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
zastánci	zastánce	k1gMnPc7	zastánce
pozvolnějšího	pozvolný	k2eAgNnSc2d2	pozvolnější
zavádění	zavádění	k1gNnSc2	zavádění
trhu	trh	k1gInSc2	trh
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
gradualisty	gradualista	k1gMnSc2	gradualista
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
narážely	narážet	k5eAaImAgInP	narážet
jeho	jeho	k3xOp3gFnPc4	jeho
snahy	snaha	k1gFnPc4	snaha
o	o	k7c4	o
rychlou	rychlý	k2eAgFnSc4d1	rychlá
transformaci	transformace	k1gFnSc4	transformace
ekonomiky	ekonomika	k1gFnSc2	ekonomika
na	na	k7c4	na
omezenou	omezený	k2eAgFnSc4d1	omezená
akceschopnost	akceschopnost	k1gFnSc4	akceschopnost
Občanského	občanský	k2eAgNnSc2d1	občanské
fóra	fórum	k1gNnSc2	fórum
a	a	k8xC	a
narůstající	narůstající	k2eAgNnSc4d1	narůstající
napětí	napětí	k1gNnSc4	napětí
ve	v	k7c6	v
vztazích	vztah	k1gInPc6	vztah
se	s	k7c7	s
slovenskou	slovenský	k2eAgFnSc7d1	slovenská
politickou	politický	k2eAgFnSc7d1	politická
reprezentací	reprezentace	k1gFnSc7	reprezentace
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
podrobnější	podrobný	k2eAgInSc1d2	podrobnější
plán	plán	k1gInSc1	plán
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
reforem	reforma	k1gFnPc2	reforma
<g/>
,	,	kIx,	,
vytvořený	vytvořený	k2eAgMnSc1d1	vytvořený
i	i	k9	i
s	s	k7c7	s
Klausovou	Klausův	k2eAgFnSc7d1	Klausova
účastí	účast	k1gFnSc7	účast
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
uveřejněn	uveřejnit	k5eAaPmNgInS	uveřejnit
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Časový	časový	k2eAgInSc1d1	časový
rámec	rámec	k1gInSc1	rámec
a	a	k8xC	a
rozsah	rozsah	k1gInSc1	rozsah
použití	použití	k1gNnSc2	použití
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
metod	metoda	k1gFnPc2	metoda
privatizace	privatizace	k1gFnSc2	privatizace
však	však	k9	však
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
nebyl	být	k5eNaImAgInS	být
přesně	přesně	k6eAd1	přesně
dohodnut	dohodnout	k5eAaPmNgInS	dohodnout
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1990	[number]	k4	1990
začal	začít	k5eAaPmAgMnS	začít
Klaus	Klaus	k1gMnSc1	Klaus
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c6	o
kupónové	kupónový	k2eAgFnSc6d1	kupónová
privatizaci	privatizace	k1gFnSc6	privatizace
jako	jako	k8xC	jako
metodě	metoda	k1gFnSc6	metoda
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
privatizovat	privatizovat	k5eAaImF	privatizovat
největší	veliký	k2eAgFnSc4d3	veliký
část	část	k1gFnSc4	část
podniků	podnik	k1gInPc2	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
restitucím	restituce	k1gFnPc3	restituce
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
navracení	navracení	k1gNnSc1	navracení
majetku	majetek	k1gInSc2	majetek
zabaveného	zabavený	k2eAgMnSc2d1	zabavený
komunisty	komunista	k1gMnSc2	komunista
původním	původní	k2eAgMnPc3d1	původní
majitelům	majitel	k1gMnPc3	majitel
nebyl	být	k5eNaImAgInS	být
Klaus	Klaus	k1gMnSc1	Klaus
nakloněn	nakloněn	k2eAgMnSc1d1	nakloněn
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Jiřího	Jiří	k1gMnSc2	Jiří
Peheho	Pehe	k1gMnSc2	Pehe
snad	snad	k9	snad
z	z	k7c2	z
obavy	obava	k1gFnSc2	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
zpomalit	zpomalit	k5eAaPmF	zpomalit
a	a	k8xC	a
zkomplikovat	zkomplikovat	k5eAaPmF	zkomplikovat
proces	proces	k1gInSc4	proces
privatizace	privatizace	k1gFnSc2	privatizace
<g/>
.	.	kIx.	.
</s>
<s>
Projevilo	projevit	k5eAaPmAgNnS	projevit
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
později	pozdě	k6eAd2	pozdě
i	i	k9	i
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
restitucím	restituce	k1gFnPc3	restituce
církevního	církevní	k2eAgInSc2d1	církevní
majetku	majetek	k1gInSc2	majetek
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
ODS	ODS	kA	ODS
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
na	na	k7c4	na
dvě	dva	k4xCgNnPc4	dva
desetiletí	desetiletí	k1gNnPc4	desetiletí
zablokovala	zablokovat	k5eAaPmAgFnS	zablokovat
a	a	k8xC	a
Klaus	Klaus	k1gMnSc1	Klaus
obvinil	obvinit	k5eAaPmAgMnS	obvinit
katolickou	katolický	k2eAgFnSc4d1	katolická
církev	církev	k1gFnSc4	církev
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
hrát	hrát	k5eAaImF	hrát
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
roli	role	k1gFnSc4	role
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jí	on	k3xPp3gFnSc3	on
nepřísluší	příslušet	k5eNaImIp3nS	příslušet
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
transformační	transformační	k2eAgInPc1d1	transformační
zákony	zákon	k1gInPc1	zákon
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
ekonomiky	ekonomika	k1gFnSc2	ekonomika
byly	být	k5eAaImAgInP	být
přijaty	přijmout	k5eAaPmNgInP	přijmout
na	na	k7c6	na
konci	konec	k1gInSc6	konec
dubna	duben	k1gInSc2	duben
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Valtr	Valtr	k1gMnSc1	Valtr
Komárek	Komárek	k1gMnSc1	Komárek
po	po	k7c6	po
červnových	červnový	k2eAgFnPc6d1	červnová
volbách	volba	k1gFnPc6	volba
1990	[number]	k4	1990
odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
zesílil	zesílit	k5eAaPmAgInS	zesílit
vliv	vliv	k1gInSc4	vliv
Klausova	Klausův	k2eAgNnSc2d1	Klausovo
křídla	křídlo	k1gNnSc2	křídlo
radikálních	radikální	k2eAgMnPc2d1	radikální
reformátorů	reformátor	k1gMnPc2	reformátor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
vláda	vláda	k1gFnSc1	vláda
předložila	předložit	k5eAaPmAgFnS	předložit
Federálnímu	federální	k2eAgNnSc3d1	federální
shromáždění	shromáždění	k1gNnSc1	shromáždění
scénář	scénář	k1gInSc1	scénář
reforem	reforma	k1gFnPc2	reforma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1990	[number]	k4	1990
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
malé	malý	k2eAgFnSc6d1	malá
privatizaci	privatizace	k1gFnSc6	privatizace
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yIgNnSc2	který
se	se	k3xPyFc4	se
dražily	dražit	k5eAaImAgInP	dražit
menší	malý	k2eAgInPc1d2	menší
provozy	provoz	k1gInPc1	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Zásadní	zásadní	k2eAgFnPc1d1	zásadní
reformy	reforma	k1gFnPc1	reforma
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
uvolňování	uvolňování	k1gNnSc1	uvolňování
cen	cena	k1gFnPc2	cena
<g/>
,	,	kIx,	,
protiinflační	protiinflační	k2eAgNnSc1d1	protiinflační
opatření	opatření	k1gNnSc1	opatření
a	a	k8xC	a
zavádění	zavádění	k1gNnSc1	zavádění
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
konvertibility	konvertibilita	k1gFnSc2	konvertibilita
měny	měna	k1gFnSc2	měna
<g/>
,	,	kIx,	,
začaly	začít	k5eAaPmAgInP	začít
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Ceny	cena	k1gFnPc1	cena
zprvu	zprvu	k6eAd1	zprvu
skokově	skokově	k6eAd1	skokově
vzrostly	vzrůst	k5eAaPmAgFnP	vzrůst
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
byla	být	k5eAaImAgFnS	být
již	již	k9	již
inflace	inflace	k1gFnSc1	inflace
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
a	a	k8xC	a
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
jen	jen	k9	jen
na	na	k7c6	na
asi	asi	k9	asi
4	[number]	k4	4
%	%	kIx~	%
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
postiženém	postižený	k2eAgNnSc6d1	postižené
útlumem	útlum	k1gInSc7	útlum
zbrojního	zbrojní	k2eAgInSc2d1	zbrojní
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
asi	asi	k9	asi
12	[number]	k4	12
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1991	[number]	k4	1991
byl	být	k5eAaImAgInS	být
schválen	schválit	k5eAaPmNgInS	schválit
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
velké	velký	k2eAgFnSc6d1	velká
privatizaci	privatizace	k1gFnSc6	privatizace
a	a	k8xC	a
po	po	k7c6	po
bouřlivých	bouřlivý	k2eAgFnPc6d1	bouřlivá
debatách	debata	k1gFnPc6	debata
také	také	k6eAd1	také
druhý	druhý	k4xOgInSc4	druhý
restituční	restituční	k2eAgInSc4d1	restituční
zákon	zákon	k1gInSc4	zákon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
privatizaci	privatizace	k1gFnSc6	privatizace
bylo	být	k5eAaImAgNnS	být
formou	forma	k1gFnSc7	forma
aukcí	aukce	k1gFnPc2	aukce
s	s	k7c7	s
upřednostněním	upřednostnění	k1gNnSc7	upřednostnění
domácích	domácí	k2eAgMnPc2d1	domácí
kupců	kupec	k1gMnPc2	kupec
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
prodáno	prodat	k5eAaPmNgNnS	prodat
nebo	nebo	k8xC	nebo
pronajato	pronajmout	k5eAaPmNgNnS	pronajmout
24	[number]	k4	24
359	[number]	k4	359
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
jednotek	jednotka	k1gFnPc2	jednotka
v	v	k7c6	v
celkové	celkový	k2eAgFnSc6d1	celková
tržní	tržní	k2eAgFnSc6d1	tržní
hodnotě	hodnota	k1gFnSc6	hodnota
31	[number]	k4	31
miliard	miliarda	k4xCgFnPc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
kritika	kritika	k1gFnSc1	kritika
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
aukcích	aukce	k1gFnPc6	aukce
perou	prát	k5eAaImIp3nP	prát
špinavé	špinavý	k2eAgInPc1d1	špinavý
peníze	peníz	k1gInPc1	peníz
představitelé	představitel	k1gMnPc1	představitel
minulého	minulý	k2eAgInSc2d1	minulý
režimu	režim	k1gInSc2	režim
a	a	k8xC	a
různé	různý	k2eAgFnSc2d1	různá
mafie	mafie	k1gFnSc2	mafie
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
organizované	organizovaný	k2eAgFnPc1d1	organizovaná
<g/>
,	,	kIx,	,
zčásti	zčásti	k6eAd1	zčásti
i	i	k9	i
zločinecké	zločinecký	k2eAgFnPc1d1	zločinecká
skupiny	skupina	k1gFnPc1	skupina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odmítal	odmítat	k5eAaImAgMnS	odmítat
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
návrhy	návrh	k1gInPc4	návrh
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
kupci	kupec	k1gMnPc1	kupec
museli	muset	k5eAaImAgMnP	muset
prokazovat	prokazovat	k5eAaImF	prokazovat
původ	původ	k1gInSc4	původ
svého	svůj	k3xOyFgInSc2	svůj
majetku	majetek	k1gInSc2	majetek
<g/>
;	;	kIx,	;
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
dokonce	dokonce	k9	dokonce
citace	citace	k1gFnSc1	citace
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yRgFnSc2	který
on	on	k3xPp3gMnSc1	on
"	"	kIx"	"
<g/>
nezná	znát	k5eNaImIp3nS	znát
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
čistými	čistý	k2eAgInPc7d1	čistý
a	a	k8xC	a
špinavými	špinavý	k2eAgInPc7d1	špinavý
penězi	peníze	k1gInPc7	peníze
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Klaus	Klaus	k1gMnSc1	Klaus
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
zřejmě	zřejmě	k6eAd1	zřejmě
obával	obávat	k5eAaImAgMnS	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
přezkoumávání	přezkoumávání	k1gNnSc1	přezkoumávání
majetku	majetek	k1gInSc2	majetek
kupců	kupec	k1gMnPc2	kupec
by	by	kYmCp3nS	by
privatizaci	privatizace	k1gFnSc3	privatizace
zbrzdilo	zbrzdit	k5eAaPmAgNnS	zbrzdit
<g/>
.	.	kIx.	.
</s>
<s>
Privatizace	privatizace	k1gFnSc1	privatizace
přitom	přitom	k6eAd1	přitom
nebyla	být	k5eNaImAgFnS	být
jen	jen	k9	jen
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
naléhavý	naléhavý	k2eAgInSc4d1	naléhavý
politický	politický	k2eAgInSc4d1	politický
úkol	úkol	k1gInSc4	úkol
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
zemi	zem	k1gFnSc4	zem
navždy	navždy	k6eAd1	navždy
vymanit	vymanit	k5eAaPmF	vymanit
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
"	"	kIx"	"
<g/>
starých	starý	k2eAgFnPc2d1	stará
struktur	struktura	k1gFnPc2	struktura
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
privatizace	privatizace	k1gFnSc1	privatizace
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1992	[number]	k4	1992
a	a	k8xC	a
do	do	k7c2	do
konce	konec	k1gInSc2	konec
září	září	k1gNnSc4	září
1994	[number]	k4	1994
jí	on	k3xPp3gFnSc3	on
prošlo	projít	k5eAaPmAgNnS	projít
3	[number]	k4	3
400	[number]	k4	400
největších	veliký	k2eAgInPc2d3	veliký
podniků	podnik	k1gInPc2	podnik
v	v	k7c6	v
účetní	účetní	k2eAgFnSc6d1	účetní
hodnotě	hodnota	k1gFnSc6	hodnota
přes	přes	k7c4	přes
912	[number]	k4	912
miliard	miliarda	k4xCgFnPc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Postupovalo	postupovat	k5eAaImAgNnS	postupovat
se	se	k3xPyFc4	se
na	na	k7c6	na
základě	základ	k1gInSc6	základ
předložených	předložený	k2eAgInPc2d1	předložený
privatizačních	privatizační	k2eAgInPc2d1	privatizační
projektů	projekt	k1gInPc2	projekt
přímým	přímý	k2eAgInSc7d1	přímý
prodejem	prodej	k1gInSc7	prodej
<g/>
,	,	kIx,	,
veřejnou	veřejný	k2eAgFnSc7d1	veřejná
soutěží	soutěž	k1gFnSc7	soutěž
<g/>
,	,	kIx,	,
aukcí	aukce	k1gFnSc7	aukce
<g/>
,	,	kIx,	,
bezúplatným	bezúplatný	k2eAgInSc7d1	bezúplatný
převodem	převod	k1gInSc7	převod
<g/>
,	,	kIx,	,
prodejem	prodej	k1gInSc7	prodej
na	na	k7c6	na
akciovém	akciový	k2eAgInSc6d1	akciový
trhu	trh	k1gInSc6	trh
nebo	nebo	k8xC	nebo
kupónovou	kupónový	k2eAgFnSc4d1	kupónová
metodou	metoda	k1gFnSc7	metoda
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
si	se	k3xPyFc3	se
za	za	k7c4	za
poplatek	poplatek	k1gInSc4	poplatek
1000	[number]	k4	1000
Kčs	Kčs	kA	Kčs
mohl	moct	k5eAaImAgMnS	moct
každý	každý	k3xTgMnSc1	každý
dospělý	dospělý	k2eAgMnSc1d1	dospělý
Čechoslovák	Čechoslovák	k1gMnSc1	Čechoslovák
opatřit	opatřit	k5eAaPmF	opatřit
vlastnický	vlastnický	k2eAgInSc4d1	vlastnický
podíl	podíl	k1gInSc4	podíl
ve	v	k7c6	v
vybraných	vybraný	k2eAgFnPc6d1	vybraná
firmách	firma	k1gFnPc6	firma
<g/>
.	.	kIx.	.
</s>
<s>
Konaly	konat	k5eAaImAgFnP	konat
se	se	k3xPyFc4	se
dvě	dva	k4xCgFnPc1	dva
vlny	vlna	k1gFnPc1	vlna
kupónové	kupónový	k2eAgFnSc2d1	kupónová
privatizace	privatizace	k1gFnSc2	privatizace
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
rychlosti	rychlost	k1gFnSc2	rychlost
měla	mít	k5eAaImAgFnS	mít
kupónová	kupónový	k2eAgFnSc1d1	kupónová
metoda	metoda	k1gFnSc1	metoda
v	v	k7c6	v
Klausových	Klausových	k2eAgNnPc6d1	Klausových
očích	oko	k1gNnPc6	oko
tu	tu	k6eAd1	tu
přednost	přednost	k1gFnSc1	přednost
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozdávání	rozdávání	k1gNnSc4	rozdávání
státního	státní	k2eAgInSc2d1	státní
majetku	majetek	k1gInSc2	majetek
občanům	občan	k1gMnPc3	občan
mu	on	k3xPp3gMnSc3	on
pomáhalo	pomáhat	k5eAaImAgNnS	pomáhat
získat	získat	k5eAaPmF	získat
podporu	podpora	k1gFnSc4	podpora
před	před	k7c7	před
volbami	volba	k1gFnPc7	volba
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Podpora	podpora	k1gFnSc1	podpora
kupónové	kupónový	k2eAgFnSc2d1	kupónová
privatizace	privatizace	k1gFnSc2	privatizace
vycházela	vycházet	k5eAaImAgNnP	vycházet
z	z	k7c2	z
jejího	její	k3xOp3gNnSc2	její
chápání	chápání	k1gNnSc2	chápání
jako	jako	k8xS	jako
pokusu	pokus	k1gInSc2	pokus
o	o	k7c4	o
spravedlivé	spravedlivý	k2eAgNnSc4d1	spravedlivé
rozdělení	rozdělení	k1gNnSc4	rozdělení
majetku	majetek	k1gInSc2	majetek
mezi	mezi	k7c7	mezi
občany	občan	k1gMnPc7	občan
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
podle	podle	k7c2	podle
Jiřího	Jiří	k1gMnSc2	Jiří
Peheho	Pehe	k1gMnSc2	Pehe
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
byla	být	k5eAaImAgFnS	být
naděje	naděje	k1gFnSc1	naděje
běžných	běžný	k2eAgMnPc2d1	běžný
účastníků	účastník	k1gMnPc2	účastník
na	na	k7c4	na
zbohatnutí	zbohatnutí	k1gNnPc4	zbohatnutí
iluzorní	iluzorní	k2eAgFnPc1d1	iluzorní
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
iniciativy	iniciativa	k1gFnSc2	iniciativa
podnikatelů	podnikatel	k1gMnPc2	podnikatel
<g/>
,	,	kIx,	,
jako	jako	k9	jako
byli	být	k5eAaImAgMnP	být
Viktor	Viktor	k1gMnSc1	Viktor
Kožený	Kožený	k1gMnSc1	Kožený
a	a	k8xC	a
Pavel	Pavel	k1gMnSc1	Pavel
Tykač	Tykač	k1gMnSc1	Tykač
<g/>
,	,	kIx,	,
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
investiční	investiční	k2eAgInPc1d1	investiční
privatizační	privatizační	k2eAgInPc1d1	privatizační
fondy	fond	k1gInPc1	fond
a	a	k8xC	a
shromáždily	shromáždit	k5eAaPmAgFnP	shromáždit
akcie	akcie	k1gFnPc1	akcie
od	od	k7c2	od
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celkové	celkový	k2eAgFnSc2d1	celková
hodnoty	hodnota	k1gFnSc2	hodnota
333	[number]	k4	333
miliard	miliarda	k4xCgFnPc2	miliarda
korun	koruna	k1gFnPc2	koruna
v	v	k7c6	v
akciích	akcie	k1gFnPc6	akcie
z	z	k7c2	z
kupónové	kupónový	k2eAgFnSc2d1	kupónová
privatizace	privatizace	k1gFnSc2	privatizace
shromáždily	shromáždit	k5eAaPmAgInP	shromáždit
fondy	fond	k1gInPc1	fond
226	[number]	k4	226
miliard	miliarda	k4xCgFnPc2	miliarda
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
fondy	fond	k1gInPc1	fond
však	však	k9	však
nebyly	být	k5eNaImAgInP	být
dostatečně	dostatečně	k6eAd1	dostatečně
regulovány	regulován	k2eAgInPc1d1	regulován
a	a	k8xC	a
své	svůj	k3xOyFgMnPc4	svůj
podílníky	podílník	k1gMnPc4	podílník
často	často	k6eAd1	často
okrádaly	okrádat	k5eAaImAgInP	okrádat
<g/>
.	.	kIx.	.
</s>
<s>
Rychlá	rychlý	k2eAgFnSc1d1	rychlá
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
transformace	transformace	k1gFnSc1	transformace
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
předběhla	předběhnout	k5eAaPmAgFnS	předběhnout
vývoj	vývoj	k1gInSc4	vývoj
právního	právní	k2eAgInSc2d1	právní
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
kriminalitu	kriminalita	k1gFnSc4	kriminalita
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
vede	vést	k5eAaImIp3nS	vést
spor	spor	k1gInSc1	spor
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
byla	být	k5eAaImAgFnS	být
privatizace	privatizace	k1gFnSc1	privatizace
přes	přes	k7c4	přes
všechny	všechen	k3xTgInPc4	všechen
problémy	problém	k1gInPc4	problém
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
,	,	kIx,	,
či	či	k8xC	či
zda	zda	k8xS	zda
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
rozkrádání	rozkrádání	k1gNnSc4	rozkrádání
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
"	"	kIx"	"
<g/>
nemá	mít	k5eNaImIp3nS	mít
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
dějinách	dějiny	k1gFnPc6	dějiny
obdoby	obdoba	k1gFnSc2	obdoba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
sám	sám	k3xTgMnSc1	sám
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
případné	případný	k2eAgFnPc1d1	případná
chyby	chyba	k1gFnPc1	chyba
byly	být	k5eAaImAgFnP	být
důsledkem	důsledek	k1gInSc7	důsledek
složitosti	složitost	k1gFnSc2	složitost
celého	celý	k2eAgInSc2d1	celý
transformačního	transformační	k2eAgInSc2d1	transformační
projektu	projekt	k1gInSc2	projekt
a	a	k8xC	a
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
bylo	být	k5eAaImAgNnS	být
mnoho	mnoho	k4c1	mnoho
různých	různý	k2eAgFnPc2d1	různá
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nemohl	moct	k5eNaImAgMnS	moct
řídit	řídit	k5eAaImF	řídit
celý	celý	k2eAgInSc4d1	celý
proces	proces	k1gInSc4	proces
<g/>
;	;	kIx,	;
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
případě	případ	k1gInSc6	případ
sám	sám	k3xTgMnSc1	sám
některé	některý	k3yIgFnSc2	některý
kontroverzní	kontroverzní	k2eAgFnSc2d1	kontroverzní
osoby	osoba	k1gFnSc2	osoba
podporoval	podporovat	k5eAaImAgMnS	podporovat
<g/>
,	,	kIx,	,
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
například	například	k6eAd1	například
<g/>
,	,	kIx,	,
že	že	k8xS	že
Česko	Česko	k1gNnSc1	Česko
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
"	"	kIx"	"
<g/>
více	hodně	k6eAd2	hodně
Kožených	Kožený	k1gMnPc2	Kožený
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
problémem	problém	k1gInSc7	problém
velké	velký	k2eAgFnSc2d1	velká
privatizace	privatizace	k1gFnSc2	privatizace
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
velké	velký	k2eAgFnPc1d1	velká
banky	banka	k1gFnPc1	banka
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
v	v	k7c6	v
majetku	majetek	k1gInSc6	majetek
státu	stát	k1gInSc2	stát
a	a	k8xC	a
podílely	podílet	k5eAaImAgFnP	podílet
se	se	k3xPyFc4	se
na	na	k7c4	na
financování	financování	k1gNnSc4	financování
privatizačních	privatizační	k2eAgInPc2d1	privatizační
projektů	projekt	k1gInPc2	projekt
nebo	nebo	k8xC	nebo
vlastnily	vlastnit	k5eAaImAgInP	vlastnit
investiční	investiční	k2eAgInPc1d1	investiční
fondy	fond	k1gInPc1	fond
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
stát	stát	k1gInSc1	stát
přes	přes	k7c4	přes
ně	on	k3xPp3gMnPc4	on
fakticky	fakticky	k6eAd1	fakticky
dál	daleko	k6eAd2	daleko
podniky	podnik	k1gInPc4	podnik
ovládal	ovládat	k5eAaImAgInS	ovládat
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
banky	banka	k1gFnPc1	banka
poskytovaly	poskytovat	k5eAaImAgFnP	poskytovat
úvěry	úvěr	k1gInPc4	úvěr
i	i	k9	i
na	na	k7c4	na
některé	některý	k3yIgInPc4	některý
pochybné	pochybný	k2eAgInPc4d1	pochybný
projekty	projekt	k1gInPc4	projekt
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
na	na	k7c4	na
popud	popud	k1gInSc4	popud
politiků	politik	k1gMnPc2	politik
na	na	k7c6	na
celostátní	celostátní	k2eAgFnSc6d1	celostátní
i	i	k8xC	i
regionální	regionální	k2eAgFnSc6d1	regionální
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
akciový	akciový	k2eAgInSc1d1	akciový
trh	trh	k1gInSc1	trh
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
až	až	k9	až
se	s	k7c7	s
zpožděním	zpoždění	k1gNnSc7	zpoždění
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nezdravý	zdravý	k2eNgInSc1d1	nezdravý
stav	stav	k1gInSc1	stav
ekonomiky	ekonomika	k1gFnSc2	ekonomika
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
bankovní	bankovní	k2eAgInSc1d1	bankovní
socialismus	socialismus	k1gInSc1	socialismus
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
souviset	souviset	k5eAaImF	souviset
i	i	k9	i
s	s	k7c7	s
Klausovou	Klausův	k2eAgFnSc7d1	Klausova
snahou	snaha	k1gFnSc7	snaha
privatizovat	privatizovat	k5eAaImF	privatizovat
českým	český	k2eAgFnPc3d1	Česká
namísto	namísto	k7c2	namísto
zahraničním	zahraniční	k2eAgMnPc3d1	zahraniční
zájemcům	zájemce	k1gMnPc3	zájemce
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
například	například	k6eAd1	například
Joseph	Joseph	k1gMnSc1	Joseph
Stiglitz	Stiglitz	k1gMnSc1	Stiglitz
<g/>
.	.	kIx.	.
</s>
<s>
Klausův	Klausův	k2eAgInSc1d1	Klausův
obraz	obraz	k1gInSc1	obraz
thatcheristy	thatcherista	k1gMnSc2	thatcherista
až	až	k8xS	až
"	"	kIx"	"
<g/>
tržního	tržní	k2eAgMnSc2d1	tržní
fundamentalisty	fundamentalista	k1gMnSc2	fundamentalista
<g/>
"	"	kIx"	"
tak	tak	k6eAd1	tak
kontrastuje	kontrastovat	k5eAaImIp3nS	kontrastovat
s	s	k7c7	s
názorem	názor	k1gInSc7	názor
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
např.	např.	kA	např.
Jan	Jan	k1gMnSc1	Jan
Macháček	Macháček	k1gMnSc1	Macháček
<g/>
,	,	kIx,	,
že	že	k8xS	že
Klaus	Klaus	k1gMnSc1	Klaus
pouze	pouze	k6eAd1	pouze
používal	používat	k5eAaImAgMnS	používat
tržní	tržní	k2eAgFnSc4d1	tržní
rétoriku	rétorika	k1gFnSc4	rétorika
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
většinu	většina	k1gFnSc4	většina
protržních	protržní	k2eAgFnPc2d1	protržní
reforem	reforma	k1gFnPc2	reforma
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
spíše	spíše	k9	spíše
brzdil	brzdit	k5eAaImAgMnS	brzdit
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
značné	značný	k2eAgFnPc4d1	značná
výhrady	výhrada	k1gFnPc4	výhrada
však	však	k8xC	však
byla	být	k5eAaImAgFnS	být
privatizace	privatizace	k1gFnSc1	privatizace
celkově	celkově	k6eAd1	celkově
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
a	a	k8xC	a
již	již	k6eAd1	již
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
vytvářel	vytvářet	k5eAaImAgInS	vytvářet
soukromý	soukromý	k2eAgInSc1d1	soukromý
sektor	sektor	k1gInSc1	sektor
přes	přes	k7c4	přes
50	[number]	k4	50
%	%	kIx~	%
HDP	HDP	kA	HDP
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
dříve	dříve	k6eAd2	dříve
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gInSc1	jeho
podíl	podíl	k1gInSc1	podíl
zanedbatelný	zanedbatelný	k2eAgInSc1d1	zanedbatelný
<g/>
.	.	kIx.	.
</s>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
federace	federace	k1gFnSc1	federace
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
a	a	k8xC	a
státoprávně	státoprávně	k6eAd1	státoprávně
byla	být	k5eAaImAgFnS	být
poměrně	poměrně	k6eAd1	poměrně
složitá	složitý	k2eAgFnSc1d1	složitá
<g/>
:	:	kIx,	:
existovaly	existovat	k5eAaImAgFnP	existovat
tři	tři	k4xCgFnPc1	tři
vlády	vláda	k1gFnPc1	vláda
(	(	kIx(	(
<g/>
federální	federální	k2eAgFnSc1d1	federální
a	a	k8xC	a
dvě	dva	k4xCgFnPc1	dva
republikové	republikový	k2eAgFnPc1d1	republiková
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvoukomorové	dvoukomorový	k2eAgNnSc1d1	dvoukomorové
Federální	federální	k2eAgNnSc1d1	federální
shromáždění	shromáždění	k1gNnSc1	shromáždění
a	a	k8xC	a
dva	dva	k4xCgInPc1	dva
jednokomorové	jednokomorový	k2eAgInPc1d1	jednokomorový
republikové	republikový	k2eAgInPc1d1	republikový
parlamenty	parlament	k1gInPc1	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
však	však	k9	však
fungovala	fungovat	k5eAaImAgFnS	fungovat
hladce	hladko	k6eAd1	hladko
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
všechna	všechen	k3xTgNnPc4	všechen
důležitá	důležitý	k2eAgNnPc4d1	důležité
rozhodnutí	rozhodnutí	k1gNnPc4	rozhodnutí
přijímalo	přijímat	k5eAaImAgNnS	přijímat
jediné	jediný	k2eAgNnSc1d1	jediné
mocenské	mocenský	k2eAgNnSc1d1	mocenské
centrum	centrum	k1gNnSc1	centrum
<g/>
,	,	kIx,	,
vedení	vedení	k1gNnSc1	vedení
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
vedoucí	vedoucí	k2eAgFnSc2d1	vedoucí
úlohy	úloha	k1gFnSc2	úloha
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1989	[number]	k4	1989
však	však	k9	však
moc	moc	k6eAd1	moc
přešla	přejít	k5eAaPmAgFnS	přejít
na	na	k7c4	na
státní	státní	k2eAgInPc4d1	státní
orgány	orgán	k1gInPc4	orgán
<g/>
,	,	kIx,	,
a	a	k8xC	a
ty	ten	k3xDgInPc4	ten
o	o	k7c4	o
ni	on	k3xPp3gFnSc4	on
začaly	začít	k5eAaPmAgFnP	začít
soutěžit	soutěžit	k5eAaImF	soutěžit
<g/>
.	.	kIx.	.
</s>
<s>
Napětí	napětí	k1gNnSc1	napětí
rychle	rychle	k6eAd1	rychle
rostlo	růst	k5eAaImAgNnS	růst
zejména	zejména	k9	zejména
mezi	mezi	k7c7	mezi
českou	český	k2eAgFnSc7d1	Česká
a	a	k8xC	a
slovenskou	slovenský	k2eAgFnSc7d1	slovenská
politickou	politický	k2eAgFnSc7d1	politická
reprezentací	reprezentace	k1gFnSc7	reprezentace
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mnozí	mnohý	k2eAgMnPc1d1	mnohý
slovenští	slovenský	k2eAgMnPc1d1	slovenský
politici	politik	k1gMnPc1	politik
požadovali	požadovat	k5eAaImAgMnP	požadovat
větší	veliký	k2eAgFnSc4d2	veliký
autonomii	autonomie	k1gFnSc4	autonomie
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
Pozice	pozice	k1gFnSc1	pozice
federální	federální	k2eAgFnSc2d1	federální
vlády	vláda	k1gFnSc2	vláda
postupně	postupně	k6eAd1	postupně
slábla	slábnout	k5eAaImAgFnS	slábnout
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
si	se	k3xPyFc3	se
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
začal	začít	k5eAaPmAgInS	začít
aktivně	aktivně	k6eAd1	aktivně
a	a	k8xC	a
úspěšně	úspěšně	k6eAd1	úspěšně
budovat	budovat	k5eAaImF	budovat
politickou	politický	k2eAgFnSc4d1	politická
základnu	základna	k1gFnSc4	základna
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Slovensko	Slovensko	k1gNnSc1	Slovensko
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
jako	jako	k8xC	jako
federálního	federální	k2eAgMnSc2d1	federální
ministra	ministr	k1gMnSc2	ministr
financí	finance	k1gFnPc2	finance
představovalo	představovat	k5eAaImAgNnS	představovat
problém	problém	k1gInSc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
jej	on	k3xPp3gMnSc4	on
a	a	k8xC	a
Vladimíra	Vladimír	k1gMnSc4	Vladimír
Dlouhého	Dlouhý	k1gMnSc4	Dlouhý
obvinil	obvinit	k5eAaPmAgMnS	obvinit
slovenský	slovenský	k2eAgMnSc1d1	slovenský
premiér	premiér	k1gMnSc1	premiér
Vladimír	Vladimír	k1gMnSc1	Vladimír
Mečiar	Mečiar	k1gMnSc1	Mečiar
<g/>
,	,	kIx,	,
že	že	k8xS	že
nerespektují	respektovat	k5eNaImIp3nP	respektovat
potřeby	potřeba	k1gFnPc4	potřeba
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Mečiarova	Mečiarův	k2eAgFnSc1d1	Mečiarova
slovenská	slovenský	k2eAgFnSc1d1	slovenská
vláda	vláda	k1gFnSc1	vláda
pak	pak	k6eAd1	pak
privatizaci	privatizace	k1gFnSc4	privatizace
záměrně	záměrně	k6eAd1	záměrně
zpomalovala	zpomalovat	k5eAaImAgFnS	zpomalovat
a	a	k8xC	a
narušovala	narušovat	k5eAaImAgFnS	narušovat
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
a	a	k8xC	a
Petr	Petr	k1gMnSc1	Petr
Pithart	Pitharta	k1gFnPc2	Pitharta
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
najít	najít	k5eAaPmF	najít
kompromis	kompromis	k1gInSc4	kompromis
se	s	k7c7	s
Slováky	Slovák	k1gMnPc7	Slovák
a	a	k8xC	a
federaci	federace	k1gFnSc4	federace
udržet	udržet	k5eAaPmF	udržet
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
zřejmě	zřejmě	k6eAd1	zřejmě
někdy	někdy	k6eAd1	někdy
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1991	[number]	k4	1991
došel	dojít	k5eAaPmAgMnS	dojít
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
prosadit	prosadit	k5eAaPmF	prosadit
rychlé	rychlý	k2eAgFnPc4d1	rychlá
reformy	reforma	k1gFnPc4	reforma
i	i	k9	i
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
možného	možný	k2eAgInSc2d1	možný
rozpadu	rozpad	k1gInSc2	rozpad
společného	společný	k2eAgInSc2d1	společný
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Snažil	snažit	k5eAaImAgInS	snažit
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
působit	působit	k5eAaImF	působit
i	i	k9	i
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
a	a	k8xC	a
ODS	ODS	kA	ODS
budoval	budovat	k5eAaImAgMnS	budovat
jako	jako	k9	jako
celostátní	celostátní	k2eAgFnSc4d1	celostátní
federalistickou	federalistický	k2eAgFnSc4d1	federalistická
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
transformace	transformace	k1gFnSc1	transformace
však	však	k9	však
na	na	k7c4	na
Slovensko	Slovensko	k1gNnSc4	Slovensko
dopadla	dopadnout	k5eAaPmAgFnS	dopadnout
tvrději	tvrdě	k6eAd2	tvrdě
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
tam	tam	k6eAd1	tam
byl	být	k5eAaImAgMnS	být
značně	značně	k6eAd1	značně
nepopulární	populární	k2eNgMnSc1d1	nepopulární
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
kontrastovalo	kontrastovat	k5eAaImAgNnS	kontrastovat
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
tehdejší	tehdejší	k2eAgFnSc7d1	tehdejší
oblibou	obliba	k1gFnSc7	obliba
u	u	k7c2	u
českého	český	k2eAgNnSc2d1	české
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
napětí	napětí	k1gNnSc1	napětí
do	do	k7c2	do
česko-slovenských	českolovenský	k2eAgInPc2d1	česko-slovenský
vztahů	vztah	k1gInPc2	vztah
vnesly	vnést	k5eAaPmAgFnP	vnést
parlamentní	parlamentní	k2eAgFnPc1d1	parlamentní
volby	volba	k1gFnPc1	volba
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
pravicová	pravicový	k2eAgFnSc1d1	pravicová
reformní	reformní	k2eAgFnSc1d1	reformní
koalice	koalice	k1gFnSc1	koalice
vedená	vedený	k2eAgFnSc1d1	vedená
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
uspěly	uspět	k5eAaPmAgFnP	uspět
levicovější	levicový	k2eAgFnPc1d2	levicovější
národně	národně	k6eAd1	národně
orientované	orientovaný	k2eAgFnPc1d1	orientovaná
strany	strana	k1gFnPc1	strana
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Mečiarovým	Mečiarův	k2eAgNnSc7d1	Mečiarovo
HZDS	HZDS	kA	HZDS
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znemožňovalo	znemožňovat	k5eAaImAgNnS	znemožňovat
jakákoli	jakýkoli	k3yIgFnSc1	jakýkoli
významnější	významný	k2eAgInPc1d2	významnější
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
na	na	k7c6	na
federální	federální	k2eAgFnSc6d1	federální
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Havel	Havel	k1gMnSc1	Havel
sice	sice	k8xC	sice
pověřil	pověřit	k5eAaPmAgMnS	pověřit
Klause	Klaus	k1gMnSc4	Klaus
sestavením	sestavení	k1gNnSc7	sestavení
federální	federální	k2eAgFnSc2d1	federální
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
ODS	ODS	kA	ODS
ho	on	k3xPp3gNnSc4	on
však	však	k9	však
nominovala	nominovat	k5eAaBmAgFnS	nominovat
na	na	k7c4	na
předsedu	předseda	k1gMnSc4	předseda
české	český	k2eAgFnSc2d1	Česká
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Mečiar	Mečiar	k1gMnSc1	Mečiar
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
svou	svůj	k3xOyFgFnSc4	svůj
účast	účast	k1gFnSc4	účast
ve	v	k7c6	v
federální	federální	k2eAgFnSc6d1	federální
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Klaus	Klaus	k1gMnSc1	Klaus
a	a	k8xC	a
Mečiar	Mečiar	k1gMnSc1	Mečiar
usedli	usednout	k5eAaPmAgMnP	usednout
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
republikových	republikový	k2eAgFnPc2d1	republiková
vlád	vláda	k1gFnPc2	vláda
a	a	k8xC	a
do	do	k7c2	do
federálních	federální	k2eAgFnPc2d1	federální
struktur	struktura	k1gFnPc2	struktura
nominovali	nominovat	k5eAaBmAgMnP	nominovat
jen	jen	k9	jen
málo	málo	k4c4	málo
významných	významný	k2eAgMnPc2d1	významný
politiků	politik	k1gMnPc2	politik
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
federální	federální	k2eAgFnSc2d1	federální
vlády	vláda	k1gFnSc2	vláda
Jana	Jan	k1gMnSc2	Jan
Stráského	Stráský	k1gMnSc2	Stráský
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
zmenšil	zmenšit	k5eAaPmAgInS	zmenšit
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
vliv	vliv	k1gInSc1	vliv
prezidenta	prezident	k1gMnSc2	prezident
Havla	Havel	k1gMnSc2	Havel
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
s	s	k7c7	s
volebním	volební	k2eAgInSc7d1	volební
neúspěchem	neúspěch	k1gInSc7	neúspěch
Občanského	občanský	k2eAgNnSc2d1	občanské
hnutí	hnutí	k1gNnSc2	hnutí
přišel	přijít	k5eAaPmAgInS	přijít
o	o	k7c4	o
významné	významný	k2eAgMnPc4d1	významný
politické	politický	k2eAgMnPc4d1	politický
spojence	spojenec	k1gMnPc4	spojenec
<g/>
,	,	kIx,	,
jakými	jaký	k3yRgMnPc7	jaký
byli	být	k5eAaImAgMnP	být
Petr	Petr	k1gMnSc1	Petr
Pithart	Pithart	k1gInSc4	Pithart
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Rychetský	Rychetský	k1gMnSc1	Rychetský
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Dienstbier	Dienstbier	k1gMnSc1	Dienstbier
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
první	první	k4xOgNnSc1	první
povolební	povolební	k2eAgNnSc1d1	povolební
setkání	setkání	k1gNnSc1	setkání
Klause	Klaus	k1gMnSc2	Klaus
s	s	k7c7	s
Mečiarem	Mečiar	k1gMnSc7	Mečiar
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
společný	společný	k2eAgInSc1d1	společný
stát	stát	k1gInSc1	stát
je	být	k5eAaImIp3nS	být
vážně	vážně	k6eAd1	vážně
ohrožen	ohrozit	k5eAaPmNgInS	ohrozit
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodující	rozhodující	k2eAgFnSc7d1	rozhodující
otázkou	otázka	k1gFnSc7	otázka
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
Slovensko	Slovensko	k1gNnSc1	Slovensko
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaImF	stát
suverénní	suverénní	k2eAgFnSc7d1	suverénní
jednotkou	jednotka	k1gFnSc7	jednotka
<g/>
,	,	kIx,	,
subjektem	subjekt	k1gInSc7	subjekt
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
zůstat	zůstat	k5eAaPmF	zůstat
ve	v	k7c6	v
federaci	federace	k1gFnSc6	federace
<g/>
.	.	kIx.	.
</s>
<s>
ODS	ODS	kA	ODS
preferovala	preferovat	k5eAaImAgFnS	preferovat
pokračování	pokračování	k1gNnSc4	pokračování
federace	federace	k1gFnSc2	federace
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
HZDS	HZDS	kA	HZDS
požadovalo	požadovat	k5eAaImAgNnS	požadovat
konfederaci	konfederace	k1gFnSc4	konfederace
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
politici	politik	k1gMnPc1	politik
se	se	k3xPyFc4	se
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
na	na	k7c6	na
konání	konání	k1gNnSc6	konání
referenda	referendum	k1gNnSc2	referendum
o	o	k7c6	o
budoucím	budoucí	k2eAgNnSc6d1	budoucí
uspořádání	uspořádání	k1gNnSc6	uspořádání
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
nestanovili	stanovit	k5eNaPmAgMnP	stanovit
však	však	k9	však
žádné	žádný	k3yNgNnSc4	žádný
datum	datum	k1gNnSc4	datum
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
referendum	referendum	k1gNnSc1	referendum
nikdy	nikdy	k6eAd1	nikdy
neuskutečnilo	uskutečnit	k5eNaPmAgNnS	uskutečnit
<g/>
.	.	kIx.	.
</s>
<s>
ODS	ODS	kA	ODS
a	a	k8xC	a
HZDS	HZDS	kA	HZDS
se	s	k7c7	s
16	[number]	k4	16
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1992	[number]	k4	1992
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
dohodly	dohodnout	k5eAaPmAgFnP	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
chtějí	chtít	k5eAaImIp3nP	chtít
situaci	situace	k1gFnSc4	situace
vyřešit	vyřešit	k5eAaPmF	vyřešit
do	do	k7c2	do
konce	konec	k1gInSc2	konec
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
vyhrocení	vyhrocení	k1gNnSc3	vyhrocení
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
,	,	kIx,	,
když	když	k8xS	když
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
Federální	federální	k2eAgNnSc1d1	federální
shromáždění	shromáždění	k1gNnSc1	shromáždění
pro	pro	k7c4	pro
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
slovenských	slovenský	k2eAgMnPc2d1	slovenský
poslanců	poslanec	k1gMnPc2	poslanec
s	s	k7c7	s
Havlem	Havel	k1gMnSc7	Havel
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
státu	stát	k1gInSc2	stát
nezvolilo	zvolit	k5eNaPmAgNnS	zvolit
prezidenta	prezident	k1gMnSc4	prezident
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
přijala	přijmout	k5eAaPmAgFnS	přijmout
deklaraci	deklarace	k1gFnSc4	deklarace
svrchovanosti	svrchovanost	k1gFnSc2	svrchovanost
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
prezident	prezident	k1gMnSc1	prezident
Havel	Havel	k1gMnSc1	Havel
okamžitě	okamžitě	k6eAd1	okamžitě
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1992	[number]	k4	1992
se	se	k3xPyFc4	se
Klaus	Klaus	k1gMnSc1	Klaus
s	s	k7c7	s
Mečiarem	Mečiar	k1gMnSc7	Mečiar
na	na	k7c6	na
setkání	setkání	k1gNnSc6	setkání
v	v	k7c6	v
brněnské	brněnský	k2eAgFnSc6d1	brněnská
vile	vila	k1gFnSc6	vila
Tugendhat	Tugendhat	k1gMnPc4	Tugendhat
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
na	na	k7c4	na
rozdělení	rozdělení	k1gNnSc4	rozdělení
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Mečiar	Mečiar	k1gMnSc1	Mečiar
počátkem	počátkem	k7c2	počátkem
srpna	srpen	k1gInSc2	srpen
dohodu	dohoda	k1gFnSc4	dohoda
zpochybnil	zpochybnit	k5eAaPmAgMnS	zpochybnit
a	a	k8xC	a
opět	opět	k6eAd1	opět
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
konfederaci	konfederace	k1gFnSc4	konfederace
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
však	však	k9	však
Klaus	Klaus	k1gMnSc1	Klaus
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
další	další	k2eAgFnSc6d1	další
schůzce	schůzka	k1gFnSc6	schůzka
tamtéž	tamtéž	k6eAd1	tamtéž
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1992	[number]	k4	1992
stvrdili	stvrdit	k5eAaPmAgMnP	stvrdit
původní	původní	k2eAgFnSc4d1	původní
dohodu	dohoda	k1gFnSc4	dohoda
a	a	k8xC	a
chvíli	chvíle	k1gFnSc4	chvíle
před	před	k7c7	před
půlnocí	půlnoc	k1gFnSc7	půlnoc
oznámili	oznámit	k5eAaPmAgMnP	oznámit
veřejnosti	veřejnost	k1gFnSc2	veřejnost
shodu	shoda	k1gFnSc4	shoda
na	na	k7c6	na
rozdělení	rozdělení	k1gNnSc6	rozdělení
Československa	Československo	k1gNnSc2	Československo
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
komplikace	komplikace	k1gFnPc1	komplikace
nastaly	nastat	k5eAaPmAgFnP	nastat
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
<g/>
,	,	kIx,	,
když	když	k8xS	když
Slovensko	Slovensko	k1gNnSc1	Slovensko
přijalo	přijmout	k5eAaPmAgNnS	přijmout
vlastní	vlastní	k2eAgFnSc4d1	vlastní
ústavu	ústava	k1gFnSc4	ústava
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
ještě	ještě	k6eAd1	ještě
platila	platit	k5eAaImAgFnS	platit
ústava	ústava	k1gFnSc1	ústava
federální	federální	k2eAgFnSc1d1	federální
<g/>
,	,	kIx,	,
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
když	když	k8xS	když
Federální	federální	k2eAgNnSc1d1	federální
shromáždění	shromáždění	k1gNnSc1	shromáždění
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
schválit	schválit	k5eAaPmF	schválit
Klausem	Klaus	k1gMnSc7	Klaus
a	a	k8xC	a
Mečiarem	Mečiar	k1gMnSc7	Mečiar
předjednaný	předjednaný	k2eAgInSc4d1	předjednaný
ústavní	ústavní	k2eAgInSc4d1	ústavní
zákon	zákon	k1gInSc4	zákon
umožňující	umožňující	k2eAgInSc1d1	umožňující
zánik	zánik	k1gInSc1	zánik
federace	federace	k1gFnSc2	federace
a	a	k8xC	a
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
přijalo	přijmout	k5eAaPmAgNnS	přijmout
rezoluci	rezoluce	k1gFnSc4	rezoluce
<g/>
,	,	kIx,	,
podporující	podporující	k2eAgInSc4d1	podporující
vznik	vznik	k1gInSc4	vznik
česko-slovenské	českolovenský	k2eAgFnSc2d1	česko-slovenská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Klaus	Klaus	k1gMnSc1	Klaus
to	ten	k3xDgNnSc4	ten
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
stát	stát	k1gInSc1	stát
se	se	k3xPyFc4	se
rozpadne	rozpadnout	k5eAaPmIp3nS	rozpadnout
tak	tak	k6eAd1	tak
jako	jako	k8xS	jako
tak	tak	k6eAd1	tak
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
probíhaly	probíhat	k5eAaImAgInP	probíhat
ostré	ostrý	k2eAgInPc1d1	ostrý
spory	spor	k1gInPc1	spor
s	s	k7c7	s
Mečiarem	Mečiar	k1gMnSc7	Mečiar
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
spojenci	spojenec	k1gMnPc1	spojenec
o	o	k7c4	o
dělení	dělení	k1gNnSc4	dělení
majetku	majetek	k1gInSc2	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Politická	politický	k2eAgFnSc1d1	politická
nestabilita	nestabilita	k1gFnSc1	nestabilita
způsobila	způsobit	k5eAaPmAgFnS	způsobit
i	i	k9	i
výrazný	výrazný	k2eAgInSc4d1	výrazný
pokles	pokles	k1gInSc4	pokles
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
investic	investice	k1gFnPc2	investice
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
setkání	setkání	k1gNnSc6	setkání
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
6	[number]	k4	6
<g/>
.	.	kIx.	.
a	a	k8xC	a
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
však	však	k9	však
Klaus	Klaus	k1gMnSc1	Klaus
a	a	k8xC	a
Mečiar	Mečiar	k1gMnSc1	Mečiar
rozdělení	rozdělení	k1gNnSc2	rozdělení
Československa	Československo	k1gNnSc2	Československo
stvrdili	stvrdit	k5eAaPmAgMnP	stvrdit
podpisy	podpis	k1gInPc4	podpis
a	a	k8xC	a
dojednali	dojednat	k5eAaPmAgMnP	dojednat
vznik	vznik	k1gInSc4	vznik
společných	společný	k2eAgFnPc2d1	společná
komisí	komise	k1gFnPc2	komise
pro	pro	k7c4	pro
dělení	dělení	k1gNnSc4	dělení
majetku	majetek	k1gInSc2	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
jednáních	jednání	k1gNnPc6	jednání
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
shodli	shodnout	k5eAaPmAgMnP	shodnout
i	i	k9	i
na	na	k7c6	na
podobě	podoba	k1gFnSc6	podoba
budoucích	budoucí	k2eAgInPc2d1	budoucí
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
státy	stát	k1gInPc7	stát
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
otázkách	otázka	k1gFnPc6	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Pokus	pokus	k1gInSc1	pokus
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
rozpustit	rozpustit	k5eAaPmF	rozpustit
federaci	federace	k1gFnSc4	federace
ústavním	ústavní	k2eAgInSc7d1	ústavní
zákonem	zákon	k1gInSc7	zákon
ve	v	k7c6	v
Federálním	federální	k2eAgNnSc6d1	federální
shromáždění	shromáždění	k1gNnSc6	shromáždění
neprošel	projít	k5eNaPmAgInS	projít
<g/>
,	,	kIx,	,
opozice	opozice	k1gFnSc1	opozice
trvala	trvat	k5eAaImAgFnS	trvat
na	na	k7c6	na
konání	konání	k1gNnSc6	konání
referenda	referendum	k1gNnSc2	referendum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
stejný	stejný	k2eAgInSc1d1	stejný
zákon	zákon	k1gInSc1	zákon
však	však	k9	však
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
na	na	k7c6	na
opakovaném	opakovaný	k2eAgNnSc6d1	opakované
hlasování	hlasování	k1gNnSc6	hlasování
o	o	k7c4	o
týden	týden	k1gInSc4	týden
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Přispěl	přispět	k5eAaPmAgInS	přispět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
zřejmě	zřejmě	k6eAd1	zřejmě
i	i	k9	i
slib	slib	k1gInSc1	slib
českým	český	k2eAgMnPc3d1	český
poslancům	poslanec	k1gMnPc3	poslanec
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
budou	být	k5eAaImBp3nP	být
převzati	převzít	k5eAaPmNgMnP	převzít
do	do	k7c2	do
nově	nově	k6eAd1	nově
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
Senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
dodrženo	dodržen	k2eAgNnSc1d1	dodrženo
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgInS	být
učiněn	učiněn	k2eAgInSc4d1	učiněn
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
právní	právní	k2eAgInSc4d1	právní
krok	krok	k1gInSc4	krok
k	k	k7c3	k
zániku	zánik	k1gInSc3	zánik
federace	federace	k1gFnSc2	federace
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
obě	dva	k4xCgFnPc4	dva
strany	strana	k1gFnPc4	strana
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
úrovních	úroveň	k1gFnPc6	úroveň
ještě	ještě	k6eAd1	ještě
mnohokrát	mnohokrát	k6eAd1	mnohokrát
setkaly	setkat	k5eAaPmAgFnP	setkat
k	k	k7c3	k
projednání	projednání	k1gNnSc3	projednání
řady	řada	k1gFnSc2	řada
dílčích	dílčí	k2eAgFnPc2d1	dílčí
otázek	otázka	k1gFnPc2	otázka
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ohledně	ohledně	k7c2	ohledně
dělení	dělení	k1gNnSc2	dělení
majetku	majetek	k1gInSc2	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc4	Československo
tak	tak	k6eAd1	tak
mírovou	mírový	k2eAgFnSc7d1	mírová
cestou	cesta	k1gFnSc7	cesta
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
a	a	k8xC	a
rozdělilo	rozdělit	k5eAaPmAgNnS	rozdělit
se	se	k3xPyFc4	se
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
následnické	následnický	k2eAgInPc4d1	následnický
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
se	se	k3xPyFc4	se
ihned	ihned	k6eAd1	ihned
vzájemně	vzájemně	k6eAd1	vzájemně
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
uznaly	uznat	k5eAaPmAgFnP	uznat
a	a	k8xC	a
spořádaně	spořádaně	k6eAd1	spořádaně
si	se	k3xPyFc3	se
rozdělily	rozdělit	k5eAaPmAgInP	rozdělit
majetek	majetek	k1gInSc4	majetek
a	a	k8xC	a
závazky	závazek	k1gInPc4	závazek
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
např.	např.	kA	např.
Stroehlein	Stroehlein	k1gInSc4	Stroehlein
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc4	kolektiv
uváděli	uvádět	k5eAaImAgMnP	uvádět
jako	jako	k9	jako
hlavní	hlavní	k2eAgFnSc4d1	hlavní
příčinu	příčina	k1gFnSc4	příčina
rozdělení	rozdělení	k1gNnSc2	rozdělení
federace	federace	k1gFnSc2	federace
neschopnost	neschopnost	k1gFnSc4	neschopnost
Klause	Klaus	k1gMnSc2	Klaus
a	a	k8xC	a
Mečiara	Mečiar	k1gMnSc4	Mečiar
dohodnout	dohodnout	k5eAaPmF	dohodnout
se	se	k3xPyFc4	se
o	o	k7c6	o
ekonomických	ekonomický	k2eAgFnPc6d1	ekonomická
a	a	k8xC	a
ústavních	ústavní	k2eAgFnPc6d1	ústavní
záležitostech	záležitost	k1gFnPc6	záležitost
společného	společný	k2eAgInSc2d1	společný
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
Klaus	Klaus	k1gMnSc1	Klaus
to	ten	k3xDgNnSc4	ten
přičítal	přičítat	k5eAaImAgMnS	přičítat
naplnění	naplnění	k1gNnSc4	naplnění
snah	snaha	k1gFnPc2	snaha
slovenského	slovenský	k2eAgInSc2d1	slovenský
národa	národ	k1gInSc2	národ
po	po	k7c6	po
samostatnosti	samostatnost	k1gFnSc6	samostatnost
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Pehe	Peh	k1gInSc2	Peh
soudí	soudit	k5eAaImIp3nS	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Klaus	Klaus	k1gMnSc1	Klaus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
vyhodnotil	vyhodnotit	k5eAaPmAgMnS	vyhodnotit
situaci	situace	k1gFnSc4	situace
realisticky	realisticky	k6eAd1	realisticky
a	a	k8xC	a
rychlým	rychlý	k2eAgNnSc7d1	rychlé
rozdělením	rozdělení	k1gNnSc7	rozdělení
federace	federace	k1gFnSc2	federace
zabránil	zabránit	k5eAaPmAgInS	zabránit
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
paralýza	paralýza	k1gFnSc1	paralýza
centrálních	centrální	k2eAgFnPc2d1	centrální
institucí	instituce	k1gFnPc2	instituce
nebo	nebo	k8xC	nebo
aby	aby	kYmCp3nS	aby
dokonce	dokonce	k9	dokonce
napětí	napětí	k1gNnSc1	napětí
eskalovalo	eskalovat	k5eAaImAgNnS	eskalovat
v	v	k7c4	v
násilné	násilný	k2eAgInPc4d1	násilný
střety	střet	k1gInPc4	střet
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
stalo	stát	k5eAaPmAgNnS	stát
v	v	k7c6	v
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyhraných	vyhraný	k2eAgFnPc6d1	vyhraná
volbách	volba	k1gFnPc6	volba
se	s	k7c7	s
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1992	[number]	k4	1992
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
stal	stát	k5eAaPmAgMnS	stát
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
České	český	k2eAgFnPc1d1	Česká
republiky	republika	k1gFnPc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Sestavil	sestavit	k5eAaPmAgMnS	sestavit
koalici	koalice	k1gFnSc4	koalice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
kromě	kromě	k7c2	kromě
Klausovy	Klausův	k2eAgFnSc2d1	Klausova
ODS	ODS	kA	ODS
a	a	k8xC	a
KDS	KDS	kA	KDS
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
kandidovaly	kandidovat	k5eAaImAgInP	kandidovat
v	v	k7c6	v
koalici	koalice	k1gFnSc6	koalice
<g/>
,	,	kIx,	,
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
ještě	ještě	k9	ještě
ODA	ODA	kA	ODA
a	a	k8xC	a
KDU-ČSL	KDU-ČSL	k1gFnSc2	KDU-ČSL
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
předvolební	předvolební	k2eAgFnSc6d1	předvolební
kampani	kampaň	k1gFnSc6	kampaň
hrála	hrát	k5eAaImAgFnS	hrát
osoba	osoba	k1gFnSc1	osoba
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
jako	jako	k8xS	jako
předsedy	předseda	k1gMnSc2	předseda
ODS	ODS	kA	ODS
velkou	velký	k2eAgFnSc4d1	velká
roli	role	k1gFnSc4	role
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
její	její	k3xOp3gFnSc7	její
tváří	tvář	k1gFnSc7	tvář
jako	jako	k9	jako
symbol	symbol	k1gInSc1	symbol
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
transformace	transformace	k1gFnSc2	transformace
a	a	k8xC	a
kuponové	kuponový	k2eAgFnSc2d1	kuponová
privatizace	privatizace	k1gFnSc2	privatizace
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
díky	díky	k7c3	díky
předchozímu	předchozí	k2eAgNnSc3d1	předchozí
vládnímu	vládní	k2eAgNnSc3d1	vládní
angažmá	angažmá	k1gNnSc3	angažmá
byl	být	k5eAaImAgInS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejvýraznějších	výrazný	k2eAgFnPc2d3	nejvýraznější
osobností	osobnost	k1gFnPc2	osobnost
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
politické	politický	k2eAgFnSc2d1	politická
scény	scéna	k1gFnSc2	scéna
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
téměř	téměř	k6eAd1	téměř
30	[number]	k4	30
%	%	kIx~	%
volebních	volební	k2eAgInPc2d1	volební
hlasů	hlas	k1gInPc2	hlas
pro	pro	k7c4	pro
ODS	ODS	kA	ODS
představovalo	představovat	k5eAaImAgNnS	představovat
základ	základ	k1gInSc4	základ
jeho	jeho	k3xOp3gFnSc2	jeho
silné	silný	k2eAgFnSc2d1	silná
pozice	pozice	k1gFnSc2	pozice
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Kolář	Kolář	k1gMnSc1	Kolář
<g/>
,	,	kIx,	,
Syllová	Syllová	k1gFnSc1	Syllová
a	a	k8xC	a
Pecháček	Pecháček	k1gMnSc1	Pecháček
takovou	takový	k3xDgFnSc4	takový
vládu	vláda	k1gFnSc4	vláda
definovali	definovat	k5eAaBmAgMnP	definovat
jako	jako	k8xC	jako
koaliční	koaliční	k2eAgInSc1d1	koaliční
kabinet	kabinet	k1gInSc1	kabinet
s	s	k7c7	s
dominancí	dominance	k1gFnSc7	dominance
premiéra	premiér	k1gMnSc2	premiér
<g/>
,	,	kIx,	,
blížící	blížící	k2eAgFnSc4d1	blížící
se	se	k3xPyFc4	se
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
vládnutí	vládnutí	k1gNnSc6	vládnutí
ke	k	k7c3	k
kancléřskému	kancléřský	k2eAgInSc3d1	kancléřský
modelu	model	k1gInSc3	model
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Klausova	Klausův	k2eAgFnSc1d1	Klausova
síla	síla	k1gFnSc1	síla
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
vycházela	vycházet	k5eAaImAgFnS	vycházet
zejména	zejména	k9	zejména
z	z	k7c2	z
jeho	jeho	k3xOp3gNnSc2	jeho
dominantního	dominantní	k2eAgNnSc2d1	dominantní
postavení	postavení	k1gNnSc2	postavení
ve	v	k7c6	v
vlastní	vlastní	k2eAgFnSc6d1	vlastní
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
z	z	k7c2	z
nadpolovičního	nadpoloviční	k2eAgNnSc2d1	nadpoloviční
zastoupení	zastoupení	k1gNnSc2	zastoupení
ministrů	ministr	k1gMnPc2	ministr
ODS	ODS	kA	ODS
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
rozdělení	rozdělení	k1gNnSc4	rozdělení
Československa	Československo	k1gNnSc2	Československo
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
vláda	vláda	k1gFnSc1	vláda
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
převzala	převzít	k5eAaPmAgFnS	převzít
roli	role	k1gFnSc4	role
vlády	vláda	k1gFnSc2	vláda
samostatného	samostatný	k2eAgInSc2d1	samostatný
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
došlo	dojít	k5eAaPmAgNnS	dojít
proti	proti	k7c3	proti
přání	přání	k1gNnSc3	přání
většiny	většina	k1gFnSc2	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
Stroehlein	Stroehlein	k1gMnSc1	Stroehlein
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
veřejného	veřejný	k2eAgNnSc2d1	veřejné
mínění	mínění	k1gNnSc2	mínění
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
se	se	k3xPyFc4	se
vyslovilo	vyslovit	k5eAaPmAgNnS	vyslovit
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
nezávislých	závislý	k2eNgInPc2d1	nezávislý
států	stát	k1gInPc2	stát
jen	jen	k9	jen
9	[number]	k4	9
%	%	kIx~	%
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
15	[number]	k4	15
%	%	kIx~	%
Slováků	Slovák	k1gMnPc2	Slovák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pozice	pozice	k1gFnSc1	pozice
české	český	k2eAgFnSc2d1	Česká
vlády	vláda	k1gFnSc2	vláda
s	s	k7c7	s
Klausem	Klaus	k1gMnSc7	Klaus
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
zůstala	zůstat	k5eAaPmAgFnS	zůstat
poměrně	poměrně	k6eAd1	poměrně
silná	silný	k2eAgFnSc1d1	silná
po	po	k7c4	po
celé	celý	k2eAgNnSc4d1	celé
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1996	[number]	k4	1996
vláda	vláda	k1gFnSc1	vláda
také	také	k9	také
navázala	navázat	k5eAaPmAgFnS	navázat
na	na	k7c4	na
asociační	asociační	k2eAgFnSc4d1	asociační
dohodu	dohoda	k1gFnSc4	dohoda
s	s	k7c7	s
Evropským	evropský	k2eAgNnSc7d1	Evropské
společenstvím	společenství	k1gNnSc7	společenství
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
a	a	k8xC	a
podala	podat	k5eAaPmAgFnS	podat
přihlášku	přihláška	k1gFnSc4	přihláška
ke	k	k7c3	k
vstupu	vstup	k1gInSc3	vstup
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
ODS	ODS	kA	ODS
měl	mít	k5eAaImAgMnS	mít
Klaus	Klaus	k1gMnSc1	Klaus
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
velmi	velmi	k6eAd1	velmi
silnou	silný	k2eAgFnSc4d1	silná
pozici	pozice	k1gFnSc4	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stranických	stranický	k2eAgInPc6d1	stranický
kongresech	kongres	k1gInPc6	kongres
v	v	k7c6	v
letech	let	k1gInPc6	let
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
i	i	k8xC	i
1994	[number]	k4	1994
neměl	mít	k5eNaImAgInS	mít
při	při	k7c6	při
volbě	volba	k1gFnSc6	volba
předsedy	předseda	k1gMnSc2	předseda
žádného	žádný	k1gMnSc2	žádný
protikandidáta	protikandidát	k1gMnSc2	protikandidát
a	a	k8xC	a
ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
objevila	objevit	k5eAaPmAgFnS	objevit
a	a	k8xC	a
sílila	sílit	k5eAaImAgFnS	sílit
oponentura	oponentura	k1gFnSc1	oponentura
místopředsedy	místopředseda	k1gMnSc2	místopředseda
Josefa	Josef	k1gMnSc2	Josef
Zieleniece	Zieleniece	k1gMnSc2	Zieleniece
<g/>
,	,	kIx,	,
i	i	k8xC	i
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
byl	být	k5eAaImAgMnS	být
Klaus	Klaus	k1gMnSc1	Klaus
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
převahou	převaha	k1gFnSc7	převaha
zvolen	zvolit	k5eAaPmNgMnS	zvolit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1996	[number]	k4	1996
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
sloučení	sloučení	k1gNnSc3	sloučení
ODS	ODS	kA	ODS
s	s	k7c7	s
koaliční	koaliční	k2eAgFnSc7d1	koaliční
KDS	KDS	kA	KDS
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
zůstal	zůstat	k5eAaPmAgMnS	zůstat
nadále	nadále	k6eAd1	nadále
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
výsledného	výsledný	k2eAgInSc2d1	výsledný
subjektu	subjekt	k1gInSc2	subjekt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
si	se	k3xPyFc3	se
ponechal	ponechat	k5eAaPmAgInS	ponechat
název	název	k1gInSc1	název
Občanská	občanský	k2eAgFnSc1d1	občanská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
ODS	ODS	kA	ODS
opět	opět	k6eAd1	opět
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
volby	volba	k1gFnPc4	volba
se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
téměř	téměř	k6eAd1	téměř
30	[number]	k4	30
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
úspěch	úspěch	k1gInSc4	úspěch
ČSSD	ČSSD	kA	ČSSD
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
a	a	k8xC	a
redukce	redukce	k1gFnSc2	redukce
počtu	počet	k1gInSc2	počet
menších	malý	k2eAgFnPc2d2	menší
parlamentních	parlamentní	k2eAgFnPc2d1	parlamentní
stran	strana	k1gFnPc2	strana
jí	on	k3xPp3gFnSc7	on
umožnily	umožnit	k5eAaPmAgFnP	umožnit
sestavit	sestavit	k5eAaPmF	sestavit
pouze	pouze	k6eAd1	pouze
menšinovou	menšinový	k2eAgFnSc4d1	menšinová
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
koalice	koalice	k1gFnSc2	koalice
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
šly	jít	k5eAaImAgFnP	jít
opět	opět	k5eAaPmF	opět
KDU-ČSL	KDU-ČSL	k1gFnPc7	KDU-ČSL
a	a	k8xC	a
ODA	ODA	kA	ODA
<g/>
.	.	kIx.	.
</s>
<s>
Opoziční	opoziční	k2eAgFnSc1d1	opoziční
ČSSD	ČSSD	kA	ČSSD
vyměnila	vyměnit	k5eAaPmAgFnS	vyměnit
toleranci	tolerance	k1gFnSc4	tolerance
vlády	vláda	k1gFnSc2	vláda
za	za	k7c4	za
některé	některý	k3yIgFnPc4	některý
ústavní	ústavní	k2eAgFnPc4d1	ústavní
funkce	funkce	k1gFnPc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Kampaň	kampaň	k1gFnSc1	kampaň
ODS	ODS	kA	ODS
před	před	k7c7	před
samotnými	samotný	k2eAgFnPc7d1	samotná
volbami	volba	k1gFnPc7	volba
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
května	květen	k1gInSc2	květen
a	a	k8xC	a
června	červen	k1gInSc2	červen
1996	[number]	k4	1996
byla	být	k5eAaImAgFnS	být
opět	opět	k6eAd1	opět
postavena	postaven	k2eAgFnSc1d1	postavena
na	na	k7c6	na
osobě	osoba	k1gFnSc6	osoba
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
a	a	k8xC	a
na	na	k7c6	na
volebním	volební	k2eAgNnSc6d1	volební
hesle	heslo	k1gNnSc6	heslo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
údajně	údajně	k6eAd1	údajně
sám	sám	k3xTgMnSc1	sám
vybral	vybrat	k5eAaPmAgMnS	vybrat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Dokázali	dokázat	k5eAaPmAgMnP	dokázat
jsme	být	k5eAaImIp1nP	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc4	ten
dokážeme	dokázat	k5eAaPmIp1nP	dokázat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Setkala	setkat	k5eAaPmAgFnS	setkat
se	se	k3xPyFc4	se
však	však	k9	však
s	s	k7c7	s
menším	malý	k2eAgInSc7d2	menší
účinkem	účinek	k1gInSc7	účinek
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
i	i	k9	i
hůře	zle	k6eAd2	zle
organizovaná	organizovaný	k2eAgFnSc1d1	organizovaná
<g/>
,	,	kIx,	,
Klaus	Klaus	k1gMnSc1	Klaus
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
obměnil	obměnit	k5eAaPmAgInS	obměnit
volební	volební	k2eAgInSc1d1	volební
tým	tým	k1gInSc1	tým
a	a	k8xC	a
kampaň	kampaň	k1gFnSc1	kampaň
se	se	k3xPyFc4	se
za	za	k7c2	za
pochodu	pochod	k1gInSc2	pochod
přizpůsobovala	přizpůsobovat	k5eAaImAgFnS	přizpůsobovat
jeho	jeho	k3xOp3gInPc3	jeho
požadavkům	požadavek	k1gInPc3	požadavek
<g/>
,	,	kIx,	,
se	s	k7c7	s
zdůrazněním	zdůraznění	k1gNnSc7	zdůraznění
prezentace	prezentace	k1gFnSc2	prezentace
jeho	jeho	k3xOp3gFnSc2	jeho
osoby	osoba	k1gFnSc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
strana	strana	k1gFnSc1	strana
získala	získat	k5eAaPmAgFnS	získat
téměř	téměř	k6eAd1	téměř
stejný	stejný	k2eAgInSc4d1	stejný
volební	volební	k2eAgInSc4d1	volební
výsledek	výsledek	k1gInSc4	výsledek
jako	jako	k8xS	jako
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
a	a	k8xC	a
Klaus	Klaus	k1gMnSc1	Klaus
osobně	osobně	k6eAd1	osobně
obdržel	obdržet	k5eAaPmAgMnS	obdržet
také	také	k9	také
nejvíc	hodně	k6eAd3	hodně
preferenčních	preferenční	k2eAgInPc2d1	preferenční
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
zisk	zisk	k1gInSc1	zisk
byl	být	k5eAaImAgInS	být
téměř	téměř	k6eAd1	téměř
4	[number]	k4	4
<g/>
×	×	k?	×
nižší	nízký	k2eAgMnSc1d2	nižší
než	než	k8xS	než
v	v	k7c6	v
předchozích	předchozí	k2eAgFnPc6d1	předchozí
volbách	volba	k1gFnPc6	volba
a	a	k8xC	a
také	také	k9	také
nižší	nízký	k2eAgInPc1d2	nižší
než	než	k8xS	než
preferenční	preferenční	k2eAgInPc1d1	preferenční
body	bod	k1gInPc1	bod
lídra	lídr	k1gMnSc2	lídr
ČSSD	ČSSD	kA	ČSSD
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1996	[number]	k4	1996
tak	tak	k6eAd1	tak
čelil	čelit	k5eAaImAgMnS	čelit
Klaus	Klaus	k1gMnSc1	Klaus
vnitrostranické	vnitrostranický	k2eAgFnSc3d1	vnitrostranická
kritice	kritika	k1gFnSc3	kritika
zejména	zejména	k9	zejména
od	od	k7c2	od
Josefa	Josef	k1gMnSc2	Josef
Zieleniece	Zieleniece	k1gMnSc2	Zieleniece
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
Miroslava	Miroslav	k1gMnSc4	Miroslav
Macka	Macek	k1gMnSc4	Macek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
prosincovém	prosincový	k2eAgInSc6d1	prosincový
kongresu	kongres	k1gInSc6	kongres
zvolen	zvolit	k5eAaPmNgMnS	zvolit
rovněž	rovněž	k9	rovněž
místopředsedou	místopředseda	k1gMnSc7	místopředseda
ODS	ODS	kA	ODS
<g/>
.	.	kIx.	.
</s>
<s>
Klaus	Klaus	k1gMnSc1	Klaus
byl	být	k5eAaImAgMnS	být
opět	opět	k6eAd1	opět
potvrzen	potvrdit	k5eAaPmNgMnS	potvrdit
v	v	k7c6	v
předsednické	předsednický	k2eAgFnSc6d1	předsednická
volbě	volba	k1gFnSc6	volba
bez	bez	k7c2	bez
protikandidáta	protikandidát	k1gMnSc2	protikandidát
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
menším	malý	k2eAgInSc7d2	menší
poměrem	poměr	k1gInSc7	poměr
hlasů	hlas	k1gInPc2	hlas
než	než	k8xS	než
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
zhoršená	zhoršený	k2eAgFnSc1d1	zhoršená
komunikace	komunikace	k1gFnSc1	komunikace
s	s	k7c7	s
vedením	vedení	k1gNnSc7	vedení
strany	strana	k1gFnSc2	strana
projevila	projevit	k5eAaPmAgFnS	projevit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
při	při	k7c6	při
vyjednávání	vyjednávání	k1gNnSc6	vyjednávání
Česko-německé	českoěmecký	k2eAgFnSc2d1	česko-německá
deklarace	deklarace	k1gFnSc2	deklarace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
předseda	předseda	k1gMnSc1	předseda
nerespektoval	respektovat	k5eNaImAgMnS	respektovat
usnesení	usnesení	k1gNnSc4	usnesení
stranického	stranický	k2eAgNnSc2d1	stranické
grémia	grémium	k1gNnSc2	grémium
<g/>
,	,	kIx,	,
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
pěst	pěst	k1gFnSc4	pěst
jednal	jednat	k5eAaImAgMnS	jednat
se	s	k7c7	s
Zemanem	Zeman	k1gMnSc7	Zeman
o	o	k7c6	o
dodatečném	dodatečný	k2eAgNnSc6d1	dodatečné
prohlášení	prohlášení	k1gNnSc6	prohlášení
k	k	k7c3	k
deklaraci	deklarace	k1gFnSc3	deklarace
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedení	vedení	k1gNnSc4	vedení
ODS	ODS	kA	ODS
ostře	ostro	k6eAd1	ostro
odsoudilo	odsoudit	k5eAaPmAgNnS	odsoudit
a	a	k8xC	a
Klaus	Klaus	k1gMnSc1	Klaus
pak	pak	k6eAd1	pak
musel	muset	k5eAaImAgMnS	muset
ustoupit	ustoupit	k5eAaPmF	ustoupit
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc4	druhý
vládu	vláda	k1gFnSc4	vláda
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
popsali	popsat	k5eAaPmAgMnP	popsat
Kolář	Kolář	k1gMnSc1	Kolář
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
jako	jako	k8xS	jako
koaliční	koaliční	k2eAgInSc1d1	koaliční
kabinet	kabinet	k1gInSc1	kabinet
s	s	k7c7	s
dominancí	dominance	k1gFnSc7	dominance
premiéra	premiér	k1gMnSc2	premiér
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
ovšem	ovšem	k9	ovšem
i	i	k9	i
se	s	k7c7	s
silnější	silný	k2eAgFnSc7d2	silnější
pozicí	pozice	k1gFnSc7	pozice
vicepremiérů	vicepremiér	k1gMnPc2	vicepremiér
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
menších	malý	k2eAgFnPc2d2	menší
koaličních	koaliční	k2eAgFnPc2d1	koaliční
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Pozici	pozice	k1gFnSc4	pozice
předsedy	předseda	k1gMnSc2	předseda
oslabovalo	oslabovat	k5eAaImAgNnS	oslabovat
jednak	jednak	k8xC	jednak
paritní	paritní	k2eAgNnSc4d1	paritní
zastoupení	zastoupení	k1gNnSc4	zastoupení
koaličních	koaliční	k2eAgFnPc2d1	koaliční
stran	strana	k1gFnPc2	strana
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
<g/>
,	,	kIx,	,
ODS	ODS	kA	ODS
tedy	tedy	k9	tedy
měla	mít	k5eAaImAgFnS	mít
právě	právě	k9	právě
polovinu	polovina	k1gFnSc4	polovina
a	a	k8xC	a
ostatní	ostatní	k2eAgFnPc1d1	ostatní
strany	strana	k1gFnPc1	strana
se	se	k3xPyFc4	se
dělily	dělit	k5eAaImAgFnP	dělit
o	o	k7c4	o
druhou	druhý	k4xOgFnSc4	druhý
polovinu	polovina	k1gFnSc4	polovina
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jeho	jeho	k3xOp3gInSc1	jeho
manévrovací	manévrovací	k2eAgInSc1d1	manévrovací
prostor	prostor	k1gInSc1	prostor
omezoval	omezovat	k5eAaImAgInS	omezovat
článek	článek	k1gInSc4	článek
3	[number]	k4	3
koaliční	koaliční	k2eAgFnSc2d1	koaliční
dohody	dohoda	k1gFnSc2	dohoda
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
stanovoval	stanovovat	k5eAaImAgMnS	stanovovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechna	všechen	k3xTgNnPc1	všechen
zásadní	zásadní	k2eAgNnPc1d1	zásadní
rozhodnutí	rozhodnutí	k1gNnPc1	rozhodnutí
budou	být	k5eAaImBp3nP	být
přijímána	přijímat	k5eAaImNgNnP	přijímat
až	až	k9	až
po	po	k7c6	po
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
dohodě	dohoda	k1gFnSc6	dohoda
všech	všecek	k3xTgMnPc2	všecek
koaličních	koaliční	k2eAgMnPc2d1	koaliční
partnerů	partner	k1gMnPc2	partner
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
premiér	premiér	k1gMnSc1	premiér
nemohl	moct	k5eNaImAgMnS	moct
obcházet	obcházet	k5eAaImF	obcházet
partnery	partner	k1gMnPc4	partner
silovým	silový	k2eAgNnSc7d1	silové
prohlasováním	prohlasování	k1gNnSc7	prohlasování
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
opozice	opozice	k1gFnSc2	opozice
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
docházelo	docházet	k5eAaImAgNnS	docházet
v	v	k7c6	v
předchozím	předchozí	k2eAgNnSc6d1	předchozí
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
Klausova	Klausův	k2eAgMnSc2d1	Klausův
druhého	druhý	k4xOgNnSc2	druhý
premiérského	premiérský	k2eAgNnSc2d1	premiérské
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1996	[number]	k4	1996
proběhly	proběhnout	k5eAaPmAgInP	proběhnout
také	také	k6eAd1	také
vůbec	vůbec	k9	vůbec
první	první	k4xOgFnPc1	první
volby	volba	k1gFnPc1	volba
do	do	k7c2	do
nově	nově	k6eAd1	nově
ustavené	ustavený	k2eAgFnSc2d1	ustavená
horní	horní	k2eAgFnSc2d1	horní
komory	komora	k1gFnSc2	komora
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
vedly	vést	k5eAaImAgFnP	vést
úvahy	úvaha	k1gFnPc1	úvaha
o	o	k7c6	o
společných	společný	k2eAgFnPc6d1	společná
kandidátkách	kandidátka	k1gFnPc6	kandidátka
vládní	vládní	k2eAgFnSc2d1	vládní
koalice	koalice	k1gFnSc2	koalice
<g/>
,	,	kIx,	,
ODS	ODS	kA	ODS
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
postavit	postavit	k5eAaPmF	postavit
své	svůj	k3xOyFgMnPc4	svůj
vlastní	vlastní	k2eAgMnPc4d1	vlastní
kandidáty	kandidát	k1gMnPc4	kandidát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
voleb	volba	k1gFnPc2	volba
strana	strana	k1gFnSc1	strana
výrazně	výrazně	k6eAd1	výrazně
uspěla	uspět	k5eAaPmAgFnS	uspět
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
však	však	k9	však
před	před	k7c7	před
druhým	druhý	k4xOgNnSc7	druhý
kolem	kolo	k1gNnSc7	kolo
snažil	snažit	k5eAaImAgMnS	snažit
Klaus	Klaus	k1gMnSc1	Klaus
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
neúspěšné	úspěšný	k2eNgNnSc4d1	neúspěšné
kandidáty	kandidát	k1gMnPc7	kandidát
ostatních	ostatní	k2eAgFnPc2d1	ostatní
koaličních	koaliční	k2eAgFnPc2d1	koaliční
stran	strana	k1gFnPc2	strana
k	k	k7c3	k
podpoře	podpora	k1gFnSc3	podpora
kandidátů	kandidát	k1gMnPc2	kandidát
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
setkal	setkat	k5eAaPmAgMnS	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
ostrým	ostrý	k2eAgInSc7d1	ostrý
protestem	protest	k1gInSc7	protest
vedení	vedení	k1gNnSc1	vedení
obou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
KDU-ČSL	KDU-ČSL	k1gFnPc2	KDU-ČSL
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
spojila	spojit	k5eAaPmAgNnP	spojit
s	s	k7c7	s
opoziční	opoziční	k2eAgFnSc7d1	opoziční
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ODS	ODS	kA	ODS
značně	značně	k6eAd1	značně
poškodilo	poškodit	k5eAaPmAgNnS	poškodit
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
KDU-ČSL	KDU-ČSL	k1gMnSc1	KDU-ČSL
a	a	k8xC	a
současně	současně	k6eAd1	současně
vicepremiér	vicepremiér	k1gMnSc1	vicepremiér
v	v	k7c6	v
Klausově	Klausův	k2eAgFnSc6d1	Klausova
vládě	vláda	k1gFnSc6	vláda
Josef	Josef	k1gMnSc1	Josef
Lux	Lux	k1gMnSc1	Lux
tehdy	tehdy	k6eAd1	tehdy
deklaroval	deklarovat	k5eAaBmAgMnS	deklarovat
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
dominance	dominance	k1gFnSc2	dominance
ODS	ODS	kA	ODS
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ovšem	ovšem	k9	ovšem
nebyl	být	k5eNaImAgMnS	být
první	první	k4xOgInSc4	první
ani	ani	k8xC	ani
poslední	poslední	k2eAgInSc4d1	poslední
spor	spor	k1gInSc4	spor
obou	dva	k4xCgMnPc2	dva
politiků	politik	k1gMnPc2	politik
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
země	zem	k1gFnSc2	zem
začala	začít	k5eAaPmAgFnS	začít
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1997	[number]	k4	1997
zpomalovat	zpomalovat	k5eAaImF	zpomalovat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
propadu	propad	k1gInSc3	propad
příjmů	příjem	k1gInPc2	příjem
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
a	a	k8xC	a
Klausova	Klausův	k2eAgFnSc1d1	Klausova
vláda	vláda	k1gFnSc1	vláda
přijala	přijmout	k5eAaPmAgFnS	přijmout
řadu	řada	k1gFnSc4	řada
rozpočtových	rozpočtový	k2eAgInPc2d1	rozpočtový
škrtů	škrt	k1gInPc2	škrt
<g/>
,	,	kIx,	,
označovaných	označovaný	k2eAgFnPc2d1	označovaná
jako	jako	k9	jako
"	"	kIx"	"
<g/>
úsporné	úsporný	k2eAgInPc1d1	úsporný
balíčky	balíček	k1gInPc1	balíček
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
také	také	k9	také
k	k	k7c3	k
měnovému	měnový	k2eAgInSc3d1	měnový
otřesu	otřes	k1gInSc3	otřes
<g/>
.	.	kIx.	.
</s>
<s>
Müller-Rommel	Müller-Rommel	k1gInSc1	Müller-Rommel
s	s	k7c7	s
Mansfeldovou	Mansfeldová	k1gFnSc7	Mansfeldová
popsali	popsat	k5eAaPmAgMnP	popsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
politika	politika	k1gFnSc1	politika
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
neoliberalismu	neoliberalismus	k1gInSc2	neoliberalismus
nebyla	být	k5eNaImAgFnS	být
tak	tak	k6eAd1	tak
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
desetiletí	desetiletí	k1gNnSc2	desetiletí
mohlo	moct	k5eAaImAgNnS	moct
zdát	zdát	k5eAaPmF	zdát
<g/>
,	,	kIx,	,
a	a	k8xC	a
nastalá	nastalý	k2eAgFnSc1d1	nastalá
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
zdrojem	zdroj	k1gInSc7	zdroj
řady	řada	k1gFnSc2	řada
rozporů	rozpor	k1gInPc2	rozpor
ve	v	k7c6	v
vládní	vládní	k2eAgFnSc6d1	vládní
koalici	koalice	k1gFnSc6	koalice
<g/>
.	.	kIx.	.
</s>
<s>
Klaus	Klaus	k1gMnSc1	Klaus
ovšem	ovšem	k9	ovšem
nepochyboval	pochybovat	k5eNaImAgMnS	pochybovat
o	o	k7c6	o
správnosti	správnost	k1gFnSc6	správnost
zvoleného	zvolený	k2eAgInSc2d1	zvolený
způsobu	způsob	k1gInSc2	způsob
transformace	transformace	k1gFnSc2	transformace
a	a	k8xC	a
z	z	k7c2	z
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
potíží	potíž	k1gFnPc2	potíž
vinil	vinit	k5eAaImAgInS	vinit
především	především	k6eAd1	především
úrokovou	úrokový	k2eAgFnSc4d1	úroková
politiku	politika	k1gFnSc4	politika
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
banky	banka	k1gFnSc2	banka
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
prudkému	prudký	k2eAgInSc3d1	prudký
propadu	propad	k1gInSc3	propad
oblíbenosti	oblíbenost	k1gFnSc2	oblíbenost
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
zejména	zejména	k9	zejména
ODS	ODS	kA	ODS
u	u	k7c2	u
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
a	a	k8xC	a
Klaus	Klaus	k1gMnSc1	Klaus
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
po	po	k7c6	po
bouřlivých	bouřlivý	k2eAgNnPc6d1	bouřlivé
koaličních	koaliční	k2eAgNnPc6d1	koaliční
vyjednáváních	vyjednávání	k1gNnPc6	vyjednávání
přikročit	přikročit	k5eAaPmF	přikročit
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1997	[number]	k4	1997
k	k	k7c3	k
personálním	personální	k2eAgFnPc3d1	personální
obměnám	obměna	k1gFnPc3	obměna
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
ztratil	ztratit	k5eAaPmAgMnS	ztratit
dva	dva	k4xCgInPc1	dva
názorově	názorově	k6eAd1	názorově
blízké	blízký	k2eAgInPc1d1	blízký
<g/>
,	,	kIx,	,
proreformní	proreformní	k2eAgMnPc4d1	proreformní
stoupence	stoupenec	k1gMnPc4	stoupenec
<g/>
:	:	kIx,	:
ministra	ministr	k1gMnSc2	ministr
financí	finance	k1gFnPc2	finance
Ivana	Ivan	k1gMnSc2	Ivan
Kočárníka	kočárník	k1gMnSc2	kočárník
a	a	k8xC	a
ministra	ministr	k1gMnSc2	ministr
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
průmyslu	průmysl	k1gInSc2	průmysl
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Dlouhého	Dlouhý	k1gMnSc2	Dlouhý
<g/>
.	.	kIx.	.
</s>
<s>
Klaus	Klaus	k1gMnSc1	Klaus
s	s	k7c7	s
obměněnou	obměněný	k2eAgFnSc7d1	obměněná
vládou	vláda	k1gFnSc7	vláda
požádal	požádat	k5eAaPmAgInS	požádat
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
poslaneckou	poslanecký	k2eAgFnSc4d1	Poslanecká
sněmovnu	sněmovna	k1gFnSc4	sněmovna
o	o	k7c4	o
opětovné	opětovný	k2eAgNnSc4d1	opětovné
vyslovení	vyslovení	k1gNnSc4	vyslovení
důvěry	důvěra	k1gFnSc2	důvěra
a	a	k8xC	a
získal	získat	k5eAaPmAgInS	získat
ji	on	k3xPp3gFnSc4	on
nejnižší	nízký	k2eAgFnSc4d3	nejnižší
možnou	možný	k2eAgFnSc4d1	možná
většinou	většina	k1gFnSc7	většina
díky	díky	k7c3	díky
někdejšímu	někdejší	k2eAgMnSc3d1	někdejší
poslanci	poslanec	k1gMnSc3	poslanec
ČSSD	ČSSD	kA	ČSSD
Jozefu	Jozef	k1gMnSc3	Jozef
Wagnerovi	Wagner	k1gMnSc3	Wagner
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
slib	slib	k1gInSc4	slib
<g/>
,	,	kIx,	,
že	že	k8xS	že
privatizace	privatizace	k1gFnSc1	privatizace
bank	banka	k1gFnPc2	banka
bude	být	k5eAaImBp3nS	být
probíhat	probíhat	k5eAaImF	probíhat
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
a	a	k8xC	a
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
čelila	čelit	k5eAaImAgFnS	čelit
ODS	ODS	kA	ODS
obviněním	obvinění	k1gNnSc7	obvinění
z	z	k7c2	z
nejasného	jasný	k2eNgNnSc2d1	nejasné
financování	financování	k1gNnSc2	financování
<g/>
,	,	kIx,	,
např.	např.	kA	např.
pomocí	pomocí	k7c2	pomocí
darů	dar	k1gInPc2	dar
od	od	k7c2	od
podnikatelů	podnikatel	k1gMnPc2	podnikatel
účastnících	účastnící	k2eAgMnPc2d1	účastnící
se	se	k3xPyFc4	se
privatizace	privatizace	k1gFnSc2	privatizace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
poukázány	poukázat	k5eAaPmNgFnP	poukázat
přes	přes	k7c4	přes
nastrčené	nastrčený	k2eAgFnPc4d1	nastrčená
nebo	nebo	k8xC	nebo
smyšlené	smyšlený	k2eAgFnPc4d1	smyšlená
osoby	osoba	k1gFnPc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
několikamilionových	několikamilionový	k2eAgInPc2d1	několikamilionový
příspěvků	příspěvek	k1gInPc2	příspěvek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
se	se	k3xPyFc4	se
straně	strana	k1gFnSc3	strana
nedařilo	dařit	k5eNaImAgNnS	dařit
přesvědčivě	přesvědčivě	k6eAd1	přesvědčivě
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
.	.	kIx.	.
</s>
<s>
Spekulace	spekulace	k1gFnPc1	spekulace
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
souvislostech	souvislost	k1gFnPc6	souvislost
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
i	i	k9	i
kolem	kolem	k7c2	kolem
samotné	samotný	k2eAgFnSc2d1	samotná
Klausovy	Klausův	k2eAgFnSc2d1	Klausova
osoby	osoba	k1gFnSc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
tzv.	tzv.	kA	tzv.
švýcarské	švýcarský	k2eAgNnSc1d1	švýcarské
konto	konto	k1gNnSc1	konto
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
sloužit	sloužit	k5eAaImF	sloužit
k	k	k7c3	k
nelegálnímu	legální	k2eNgNnSc3d1	nelegální
financování	financování	k1gNnSc3	financování
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yIgMnSc6	který
měl	mít	k5eAaImAgMnS	mít
Klaus	Klaus	k1gMnSc1	Klaus
údajně	údajně	k6eAd1	údajně
vědět	vědět	k5eAaImF	vědět
<g/>
,	,	kIx,	,
a	a	k8xC	a
o	o	k7c6	o
jím	on	k3xPp3gMnSc7	on
údajně	údajně	k6eAd1	údajně
vlastněnou	vlastněný	k2eAgFnSc4d1	vlastněná
vilu	vila	k1gFnSc4	vila
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
vile	vila	k1gFnSc6	vila
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
přinesla	přinést	k5eAaPmAgFnS	přinést
televize	televize	k1gFnSc1	televize
Nova	nova	k1gFnSc1	nova
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
posléze	posléze	k6eAd1	posléze
ukázala	ukázat	k5eAaPmAgFnS	ukázat
jako	jako	k9	jako
nepravdivá	pravdivý	k2eNgFnSc1d1	nepravdivá
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
švýcarském	švýcarský	k2eAgNnSc6d1	švýcarské
kontu	konto	k1gNnSc6	konto
ODS	ODS	kA	ODS
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
zveřejněna	zveřejnit	k5eAaPmNgFnS	zveřejnit
v	v	k7c6	v
Mladé	mladý	k2eAgFnSc6d1	mladá
frontě	fronta	k1gFnSc6	fronta
DNES	dnes	k6eAd1	dnes
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Existenci	existence	k1gFnSc4	existence
konta	konto	k1gNnSc2	konto
ve	v	k7c6	v
švýcarské	švýcarský	k2eAgFnSc6d1	švýcarská
bance	banka	k1gFnSc6	banka
Credit	Credit	k1gFnSc2	Credit
Suisse	Suisse	k1gFnSc2	Suisse
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
sloužit	sloužit	k5eAaImF	sloužit
k	k	k7c3	k
nelegálnímu	legální	k2eNgNnSc3d1	nelegální
financování	financování	k1gNnSc3	financování
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
Švýcarské	švýcarský	k2eAgNnSc1d1	švýcarské
státní	státní	k2eAgNnSc1d1	státní
zastupitelství	zastupitelství	k1gNnSc1	zastupitelství
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
čelil	čelit	k5eAaImAgMnS	čelit
vnitrostranické	vnitrostranický	k2eAgFnSc3d1	vnitrostranická
kritice	kritika	k1gFnSc3	kritika
<g/>
,	,	kIx,	,
např.	např.	kA	např.
poslanec	poslanec	k1gMnSc1	poslanec
Jan	Jan	k1gMnSc1	Jan
Klas	klas	k1gInSc1	klas
a	a	k8xC	a
ministr	ministr	k1gMnSc1	ministr
vnitra	vnitro	k1gNnSc2	vnitro
Jan	Jan	k1gMnSc1	Jan
Ruml	Ruml	k1gMnSc1	Ruml
žádali	žádat	k5eAaImAgMnP	žádat
svolání	svolání	k1gNnSc4	svolání
mimořádného	mimořádný	k2eAgInSc2d1	mimořádný
kongresu	kongres	k1gInSc2	kongres
ODS	ODS	kA	ODS
a	a	k8xC	a
uvažovali	uvažovat	k5eAaImAgMnP	uvažovat
o	o	k7c4	o
vytvoření	vytvoření	k1gNnSc4	vytvoření
názorové	názorový	k2eAgFnSc2d1	názorová
frakce	frakce	k1gFnSc2	frakce
uvnitř	uvnitř	k7c2	uvnitř
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Eskalace	eskalace	k1gFnSc1	eskalace
sporů	spor	k1gInPc2	spor
mezi	mezi	k7c7	mezi
Klausem	Klaus	k1gMnSc7	Klaus
a	a	k8xC	a
Zieleniecem	Zieleniece	k1gMnSc7	Zieleniece
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
Zieleniecovu	Zieleniecův	k2eAgNnSc3d1	Zieleniecovo
srpnovému	srpnový	k2eAgNnSc3d1	srpnové
vzdání	vzdání	k1gNnSc3	vzdání
se	se	k3xPyFc4	se
poslaneckého	poslanecký	k2eAgInSc2d1	poslanecký
mandátu	mandát	k1gInSc2	mandát
a	a	k8xC	a
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
též	též	k9	též
k	k	k7c3	k
odchodu	odchod	k1gInSc3	odchod
z	z	k7c2	z
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
rezignaci	rezignace	k1gFnSc4	rezignace
na	na	k7c4	na
místopředsednický	místopředsednický	k2eAgInSc4d1	místopředsednický
post	post	k1gInSc4	post
ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1997	[number]	k4	1997
odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
vlády	vláda	k1gFnSc2	vláda
také	také	k9	také
ministr	ministr	k1gMnSc1	ministr
vnitra	vnitro	k1gNnSc2	vnitro
Ruml	Ruml	k1gMnSc1	Ruml
a	a	k8xC	a
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc6	listopad
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
místopředsedou	místopředseda	k1gMnSc7	místopředseda
ODS	ODS	kA	ODS
Ivanem	Ivan	k1gMnSc7	Ivan
Pilipem	Pilip	k1gMnSc7	Pilip
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
Klause	Klaus	k1gMnSc4	Klaus
k	k	k7c3	k
odstoupení	odstoupení	k1gNnSc3	odstoupení
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
předsedy	předseda	k1gMnSc2	předseda
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Klaus	Klaus	k1gMnSc1	Klaus
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
v	v	k7c6	v
Sarajevu	Sarajevo	k1gNnSc6	Sarajevo
<g/>
,	,	kIx,	,
pročež	pročež	k6eAd1	pročež
později	pozdě	k6eAd2	pozdě
získal	získat	k5eAaPmAgInS	získat
tento	tento	k3xDgInSc4	tento
akt	akt	k1gInSc4	akt
přezdívku	přezdívka	k1gFnSc4	přezdívka
"	"	kIx"	"
<g/>
Sarajevský	sarajevský	k2eAgInSc1d1	sarajevský
atentát	atentát	k1gInSc1	atentát
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
podali	podat	k5eAaPmAgMnP	podat
své	svůj	k3xOyFgFnPc4	svůj
rezignace	rezignace	k1gFnPc4	rezignace
ministři	ministr	k1gMnPc1	ministr
za	za	k7c2	za
KDU-ČSL	KDU-ČSL	k1gFnSc2	KDU-ČSL
a	a	k8xC	a
vzápětí	vzápětí	k6eAd1	vzápětí
i	i	k9	i
ministři	ministr	k1gMnPc1	ministr
za	za	k7c2	za
ODA	ODA	kA	ODA
<g/>
.	.	kIx.	.
</s>
<s>
Strany	strana	k1gFnSc2	strana
to	ten	k3xDgNnSc1	ten
zdůvodnily	zdůvodnit	k5eAaPmAgFnP	zdůvodnit
zejména	zejména	k9	zejména
finančními	finanční	k2eAgInPc7d1	finanční
skandály	skandál	k1gInPc7	skandál
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
osobnostní	osobnostní	k2eAgFnSc4d1	osobnostní
rovinu	rovina	k1gFnSc4	rovina
však	však	k9	však
dokumentuje	dokumentovat	k5eAaBmIp3nS	dokumentovat
postoj	postoj	k1gInSc1	postoj
lidovců	lidovec	k1gMnPc2	lidovec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
uvažovali	uvažovat	k5eAaImAgMnP	uvažovat
o	o	k7c4	o
setrvání	setrvání	k1gNnSc4	setrvání
ve	v	k7c6	v
vládní	vládní	k2eAgFnSc6d1	vládní
koalici	koalice	k1gFnSc6	koalice
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
premiérem	premiér	k1gMnSc7	premiér
nebyl	být	k5eNaImAgMnS	být
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
.	.	kIx.	.
</s>
<s>
Premiér	premiér	k1gMnSc1	premiér
podal	podat	k5eAaPmAgMnS	podat
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc6	listopad
1997	[number]	k4	1997
demisi	demise	k1gFnSc4	demise
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
s	s	k7c7	s
dodatkem	dodatek	k1gInSc7	dodatek
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
demisi	demise	k1gFnSc4	demise
celé	celý	k2eAgFnSc2d1	celá
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Havel	Havel	k1gMnSc1	Havel
ji	on	k3xPp3gFnSc4	on
přijal	přijmout	k5eAaPmAgMnS	přijmout
a	a	k8xC	a
další	další	k2eAgNnPc4d1	další
politická	politický	k2eAgNnPc4d1	politické
jednání	jednání	k1gNnPc4	jednání
vedl	vést	k5eAaImAgInS	vést
předseda	předseda	k1gMnSc1	předseda
KDU-ČSL	KDU-ČSL	k1gFnPc2	KDU-ČSL
Josef	Josef	k1gMnSc1	Josef
Lux	Lux	k1gMnSc1	Lux
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1997	[number]	k4	1997
prezident	prezident	k1gMnSc1	prezident
pověřil	pověřit	k5eAaPmAgMnS	pověřit
Josefa	Josef	k1gMnSc4	Josef
Tošovského	Tošovského	k2eAgMnSc4d1	Tošovského
<g/>
,	,	kIx,	,
dosavadního	dosavadní	k2eAgMnSc4d1	dosavadní
guvernéra	guvernér	k1gMnSc4	guvernér
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
banky	banka	k1gFnSc2	banka
<g/>
,	,	kIx,	,
sestavením	sestavení	k1gNnSc7	sestavení
poloúřednické	poloúřednický	k2eAgFnSc2d1	poloúřednická
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
složená	složený	k2eAgFnSc1d1	složená
zčásti	zčásti	k6eAd1	zčásti
z	z	k7c2	z
členů	člen	k1gMnPc2	člen
koaličních	koaliční	k2eAgFnPc2d1	koaliční
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
zčásti	zčásti	k6eAd1	zčásti
z	z	k7c2	z
nestraníků	nestraník	k1gMnPc2	nestraník
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
jmenována	jmenovat	k5eAaImNgFnS	jmenovat
2	[number]	k4	2
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
obvinění	obvinění	k1gNnSc4	obvinění
z	z	k7c2	z
účasti	účast	k1gFnSc2	účast
na	na	k7c6	na
korupci	korupce	k1gFnSc6	korupce
odmítal	odmítat	k5eAaImAgMnS	odmítat
a	a	k8xC	a
na	na	k7c6	na
mimořádném	mimořádný	k2eAgInSc6d1	mimořádný
kongresu	kongres	k1gInSc6	kongres
své	svůj	k3xOyFgFnSc2	svůj
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
Poděbradech	Poděbrady	k1gInPc6	Poděbrady
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1997	[number]	k4	1997
získal	získat	k5eAaPmAgInS	získat
hlasy	hlas	k1gInPc4	hlas
většiny	většina	k1gFnSc2	většina
delegátů	delegát	k1gMnPc2	delegát
pro	pro	k7c4	pro
pokračování	pokračování	k1gNnSc4	pokračování
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
předsedy	předseda	k1gMnSc2	předseda
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
protikandidátem	protikandidát	k1gMnSc7	protikandidát
byl	být	k5eAaImAgMnS	být
Jan	Jan	k1gMnSc1	Jan
Ruml	Ruml	k1gMnSc1	Ruml
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
však	však	k9	však
ve	v	k7c6	v
volbě	volba	k1gFnSc6	volba
neuspěl	uspět	k5eNaPmAgMnS	uspět
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
obměně	obměna	k1gFnSc3	obměna
vedení	vedení	k1gNnSc2	vedení
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
noví	nový	k2eAgMnPc1d1	nový
místopředsedové	místopředseda	k1gMnPc1	místopředseda
byli	být	k5eAaImAgMnP	být
na	na	k7c6	na
Klausově	Klausův	k2eAgFnSc6d1	Klausova
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Vnitrostranická	vnitrostranický	k2eAgFnSc1d1	vnitrostranická
opozice	opozice	k1gFnSc1	opozice
proti	proti	k7c3	proti
Klausovi	Klaus	k1gMnSc3	Klaus
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
odštěpeneckou	odštěpenecký	k2eAgFnSc4d1	odštěpenecká
platformu	platforma	k1gFnSc4	platforma
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
další	další	k2eAgInSc4d1	další
měsíc	měsíc	k1gInSc4	měsíc
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
vznikající	vznikající	k2eAgFnSc2d1	vznikající
Unie	unie	k1gFnSc2	unie
svobody	svoboda	k1gFnSc2	svoboda
(	(	kIx(	(
<g/>
US	US	kA	US
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Události	událost	k1gFnPc1	událost
kolem	kolem	k7c2	kolem
pádu	pád	k1gInSc2	pád
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
rozštěpení	rozštěpení	k1gNnSc2	rozštěpení
ODS	ODS	kA	ODS
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
propadu	propad	k1gInSc3	propad
popularity	popularita	k1gFnSc2	popularita
ODS	ODS	kA	ODS
u	u	k7c2	u
voličů	volič	k1gMnPc2	volič
a	a	k8xC	a
určitému	určitý	k2eAgInSc3d1	určitý
odlivu	odliv	k1gInSc3	odliv
členů	člen	k1gMnPc2	člen
této	tento	k3xDgFnSc2	tento
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
se	se	k3xPyFc4	se
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
habilitoval	habilitovat	k5eAaBmAgMnS	habilitovat
na	na	k7c6	na
Fakultě	fakulta	k1gFnSc6	fakulta
sociálních	sociální	k2eAgFnPc2d1	sociální
věd	věda	k1gFnPc2	věda
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
jako	jako	k8xS	jako
docent	docent	k1gMnSc1	docent
ekonomie	ekonomie	k1gFnSc2	ekonomie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomtéž	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
založil	založit	k5eAaPmAgMnS	založit
Nadaci	nadace	k1gFnSc4	nadace
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
stal	stát	k5eAaPmAgInS	stát
Nadační	nadační	k2eAgInSc1d1	nadační
fond	fond	k1gInSc1	fond
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
<g/>
,	,	kIx,	,
podporující	podporující	k2eAgFnSc4d1	podporující
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
občanů	občan	k1gMnPc2	občan
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
politických	politický	k2eAgFnPc6d1	politická
<g/>
,	,	kIx,	,
sociálních	sociální	k2eAgFnPc6d1	sociální
a	a	k8xC	a
ekonomických	ekonomický	k2eAgFnPc6d1	ekonomická
vědách	věda	k1gFnPc6	věda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
byl	být	k5eAaImAgMnS	být
Klaus	Klaus	k1gMnSc1	Klaus
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
profesorem	profesor	k1gMnSc7	profesor
pro	pro	k7c4	pro
obor	obor	k1gInSc4	obor
financí	finance	k1gFnPc2	finance
na	na	k7c6	na
Vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgFnPc6d1	následující
předčasných	předčasný	k2eAgFnPc6d1	předčasná
volbách	volba	k1gFnPc6	volba
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
získala	získat	k5eAaPmAgFnS	získat
ODS	ODS	kA	ODS
pod	pod	k7c7	pod
Klausovým	Klausův	k2eAgNnSc7d1	Klausovo
vedením	vedení	k1gNnSc7	vedení
a	a	k8xC	a
po	po	k7c6	po
kampani	kampaň	k1gFnSc6	kampaň
<g/>
,	,	kIx,	,
založené	založený	k2eAgFnSc6d1	založená
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
na	na	k7c4	na
varování	varování	k1gNnSc4	varování
před	před	k7c7	před
nástupem	nástup	k1gInSc7	nástup
levice	levice	k1gFnSc2	levice
k	k	k7c3	k
vládě	vláda	k1gFnSc3	vláda
<g/>
,	,	kIx,	,
necelých	celý	k2eNgNnPc2d1	necelé
28	[number]	k4	28
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
neúspěšném	úspěšný	k2eNgNnSc6d1	neúspěšné
vyjednávání	vyjednávání	k1gNnSc6	vyjednávání
s	s	k7c7	s
předsedou	předseda	k1gMnSc7	předseda
Unie	unie	k1gFnSc2	unie
svobody	svoboda	k1gFnSc2	svoboda
Janem	Jan	k1gMnSc7	Jan
Rumlem	Ruml	k1gMnSc7	Ruml
sestavil	sestavit	k5eAaPmAgMnS	sestavit
vládu	vláda	k1gFnSc4	vláda
předseda	předseda	k1gMnSc1	předseda
vítězné	vítězný	k2eAgFnSc2d1	vítězná
ČSSD	ČSSD	kA	ČSSD
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
za	za	k7c2	za
podpory	podpora	k1gFnSc2	podpora
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
po	po	k7c6	po
sepsání	sepsání	k1gNnSc6	sepsání
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
opoziční	opoziční	k2eAgFnSc2d1	opoziční
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rozdělení	rozdělení	k1gNnSc2	rozdělení
funkcí	funkce	k1gFnPc2	funkce
podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
smlouvy	smlouva	k1gFnSc2	smlouva
byl	být	k5eAaImAgMnS	být
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
předsedy	předseda	k1gMnSc2	předseda
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Pragmatické	pragmatický	k2eAgNnSc1d1	pragmatické
řešení	řešení	k1gNnSc1	řešení
rozdělení	rozdělení	k1gNnSc2	rozdělení
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
kontrast	kontrast	k1gInSc1	kontrast
s	s	k7c7	s
předcházející	předcházející	k2eAgFnSc7d1	předcházející
výrazně	výrazně	k6eAd1	výrazně
protilevicovou	protilevicový	k2eAgFnSc7d1	protilevicová
kampaní	kampaň	k1gFnSc7	kampaň
ODS	ODS	kA	ODS
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
předmětem	předmět	k1gInSc7	předmět
ostré	ostrý	k2eAgFnSc2d1	ostrá
kritiky	kritika	k1gFnSc2	kritika
z	z	k7c2	z
nejrůznějších	různý	k2eAgFnPc2d3	nejrůznější
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
nespokojenosti	nespokojenost	k1gFnSc3	nespokojenost
části	část	k1gFnSc2	část
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
,	,	kIx,	,
vyjádřené	vyjádřený	k2eAgFnSc2d1	vyjádřená
například	například	k6eAd1	například
iniciativou	iniciativa	k1gFnSc7	iniciativa
"	"	kIx"	"
<g/>
Děkujeme	děkovat	k5eAaImIp1nP	děkovat
<g/>
,	,	kIx,	,
odejděte	odejít	k5eAaPmRp2nP	odejít
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
listopad	listopad	k1gInSc1	listopad
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
bezvýsledně	bezvýsledně	k6eAd1	bezvýsledně
požadovala	požadovat	k5eAaImAgFnS	požadovat
odchod	odchod	k1gInSc4	odchod
Klause	Klaus	k1gMnSc2	Klaus
a	a	k8xC	a
Zemana	Zeman	k1gMnSc2	Zeman
z	z	k7c2	z
politiky	politika	k1gFnSc2	politika
(	(	kIx(	(
<g/>
prosinec	prosinec	k1gInSc1	prosinec
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
Klaus	Klaus	k1gMnSc1	Klaus
založil	založit	k5eAaPmAgMnS	založit
občanské	občanský	k2eAgNnSc1d1	občanské
sdružení	sdružení	k1gNnSc1	sdružení
Centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
ekonomiku	ekonomika	k1gFnSc4	ekonomika
a	a	k8xC	a
politiku	politika	k1gFnSc4	politika
(	(	kIx(	(
<g/>
CEP	cep	k1gInSc4	cep
<g/>
)	)	kIx)	)
a	a	k8xC	a
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
předseda	předseda	k1gMnSc1	předseda
jeho	jeho	k3xOp3gFnSc2	jeho
správní	správní	k2eAgFnSc2d1	správní
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
krize	krize	k1gFnSc2	krize
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
televizi	televize	k1gFnSc6	televize
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nastala	nastat	k5eAaPmAgFnS	nastat
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
novým	nový	k2eAgMnSc7d1	nový
generálním	generální	k2eAgMnSc7d1	generální
ředitelem	ředitel	k1gMnSc7	ředitel
Jiří	Jiří	k1gMnSc1	Jiří
Hodač	Hodač	k1gMnSc1	Hodač
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Klaus	Klaus	k1gMnSc1	Klaus
postavil	postavit	k5eAaPmAgMnS	postavit
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
nového	nový	k2eAgNnSc2d1	nové
vedení	vedení	k1gNnSc2	vedení
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Hodačem	Hodač	k1gMnSc7	Hodač
a	a	k8xC	a
jím	on	k3xPp3gMnSc7	on
jmenované	jmenovaný	k2eAgFnPc1d1	jmenovaná
ředitelky	ředitelka	k1gFnSc2	ředitelka
zpravodajství	zpravodajství	k1gNnSc2	zpravodajství
Jany	Jana	k1gFnSc2	Jana
Bobošíkové	Bobošíková	k1gFnSc2	Bobošíková
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
na	na	k7c4	na
13	[number]	k4	13
<g/>
.	.	kIx.	.
kongresu	kongres	k1gInSc2	kongres
Občanské	občanský	k2eAgFnSc2d1	občanská
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
Václava	Václav	k1gMnSc4	Václav
Klause	Klaus	k1gMnSc4	Klaus
ve	v	k7c4	v
vedení	vedení	k1gNnSc4	vedení
ODS	ODS	kA	ODS
nově	nově	k6eAd1	nově
zvolený	zvolený	k2eAgMnSc1d1	zvolený
předseda	předseda	k1gMnSc1	předseda
Mirek	Mirek	k1gMnSc1	Mirek
Topolánek	Topolánek	k1gMnSc1	Topolánek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
nadsázce	nadsázka	k1gFnSc6	nadsázka
nazval	nazvat	k5eAaBmAgMnS	nazvat
ideologii	ideologie	k1gFnSc4	ideologie
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Klausova	Klausův	k2eAgNnSc2d1	Klausovo
předsednictví	předsednictví	k1gNnSc2	předsednictví
"	"	kIx"	"
<g/>
klausismus	klausismus	k1gInSc1	klausismus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
Klausovy	Klausův	k2eAgFnSc2d1	Klausova
biografie	biografie	k1gFnSc2	biografie
Lubomír	Lubomír	k1gMnSc1	Lubomír
Kopeček	Kopeček	k1gMnSc1	Kopeček
později	pozdě	k6eAd2	pozdě
popsal	popsat	k5eAaPmAgMnS	popsat
doktrínu	doktrína	k1gFnSc4	doktrína
klausismu	klausismus	k1gInSc2	klausismus
jako	jako	k8xC	jako
soubor	soubor	k1gInSc4	soubor
různých	různý	k2eAgFnPc2d1	různá
představ	představa	k1gFnPc2	představa
a	a	k8xC	a
myšlenek	myšlenka	k1gFnPc2	myšlenka
zahrnujících	zahrnující	k2eAgInPc2d1	zahrnující
národní	národní	k2eAgInSc4d1	národní
stát	stát	k1gInSc4	stát
jako	jako	k8xC	jako
jediný	jediný	k2eAgInSc4d1	jediný
myslitelný	myslitelný	k2eAgInSc4d1	myslitelný
rámec	rámec	k1gInSc4	rámec
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
,	,	kIx,	,
preferenci	preference	k1gFnSc3	preference
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
liberální	liberální	k2eAgInSc1d1	liberální
obhajobu	obhajoba	k1gFnSc4	obhajoba
trhu	trh	k1gInSc2	trh
bez	bez	k7c2	bez
deformací	deformace	k1gFnPc2	deformace
a	a	k8xC	a
redistribuce	redistribuce	k1gFnSc2	redistribuce
či	či	k8xC	či
konzervativní	konzervativní	k2eAgInSc4d1	konzervativní
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
se	se	k3xPyFc4	se
Klaus	Klaus	k1gMnSc1	Klaus
během	během	k7c2	během
letu	let	k1gInSc2	let
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Klárou	Klára	k1gFnSc7	Klára
Lohniskou	Lohniska	k1gFnSc7	Lohniska
<g/>
,	,	kIx,	,
letuškou	letuška	k1gFnSc7	letuška
ČSA	ČSA	kA	ČSA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2002	[number]	k4	2002
potom	potom	k6eAd1	potom
bulvární	bulvární	k2eAgInSc1d1	bulvární
deník	deník	k1gInSc1	deník
Blesk	blesk	k1gInSc1	blesk
uveřejnil	uveřejnit	k5eAaPmAgInS	uveřejnit
jejich	jejich	k3xOp3gFnSc4	jejich
společnou	společný	k2eAgFnSc4d1	společná
fotografii	fotografia	k1gFnSc4	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Aféra	aféra	k1gFnSc1	aféra
však	však	k9	však
Klausův	Klausův	k2eAgInSc4d1	Klausův
manželský	manželský	k2eAgInSc4d1	manželský
život	život	k1gInSc4	život
nerozvrátila	rozvrátit	k5eNaPmAgFnS	rozvrátit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2008	[number]	k4	2008
se	se	k3xPyFc4	se
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
objevila	objevit	k5eAaPmAgFnS	objevit
jména	jméno	k1gNnPc1	jméno
jiných	jiný	k1gMnPc2	jiný
dvou	dva	k4xCgFnPc2	dva
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yIgMnPc7	který
měl	mít	k5eAaImAgMnS	mít
Klaus	Klaus	k1gMnSc1	Klaus
údajně	údajně	k6eAd1	údajně
mít	mít	k5eAaImF	mít
poměr	poměr	k1gInSc4	poměr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
poslaneckých	poslanecký	k2eAgFnPc6d1	Poslanecká
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
opět	opět	k6eAd1	opět
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
ČSSD	ČSSD	kA	ČSSD
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
posílení	posílení	k1gNnSc3	posílení
levice	levice	k1gFnSc2	levice
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
vnímáno	vnímat	k5eAaImNgNnS	vnímat
jako	jako	k8xC	jako
neúspěch	neúspěch	k1gInSc1	neúspěch
ODS	ODS	kA	ODS
i	i	k8xC	i
Klause	Klaus	k1gMnSc2	Klaus
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
ODS	ODS	kA	ODS
dalo	dát	k5eAaPmAgNnS	dát
následně	následně	k6eAd1	následně
své	svůj	k3xOyFgFnPc4	svůj
funkce	funkce	k1gFnPc4	funkce
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
.	.	kIx.	.
</s>
<s>
Klaus	Klaus	k1gMnSc1	Klaus
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
nekandidovat	kandidovat	k5eNaImF	kandidovat
znovu	znovu	k6eAd1	znovu
na	na	k7c4	na
předsedu	předseda	k1gMnSc4	předseda
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
oznámil	oznámit	k5eAaPmAgInS	oznámit
úmysl	úmysl	k1gInSc1	úmysl
kandidovat	kandidovat	k5eAaImF	kandidovat
na	na	k7c4	na
funkci	funkce	k1gFnSc4	funkce
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
svého	svůj	k3xOyFgMnSc2	svůj
nástupce	nástupce	k1gMnSc2	nástupce
doporučil	doporučit	k5eAaPmAgMnS	doporučit
Petra	Petr	k1gMnSc4	Petr
Nečase	Nečas	k1gMnSc4	Nečas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
předsedou	předseda	k1gMnSc7	předseda
ODS	ODS	kA	ODS
byl	být	k5eAaImAgMnS	být
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Mirek	Mirek	k1gMnSc1	Mirek
Topolánek	Topolánek	k1gMnSc1	Topolánek
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
čestným	čestný	k2eAgMnSc7d1	čestný
předsedou	předseda	k1gMnSc7	předseda
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc2	tento
funkce	funkce	k1gFnSc2	funkce
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgMnS	vzdát
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Klausova	Klausův	k2eAgFnSc1d1	Klausova
prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
kandidatura	kandidatura	k1gFnSc1	kandidatura
byla	být	k5eAaImAgFnS	být
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2003	[number]	k4	2003
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
kole	kolo	k1gNnSc6	kolo
opakované	opakovaný	k2eAgFnSc2d1	opakovaná
volby	volba	k1gFnSc2	volba
zvolen	zvolit	k5eAaPmNgInS	zvolit
142	[number]	k4	142
hlasy	hlas	k1gInPc7	hlas
z	z	k7c2	z
281	[number]	k4	281
prezidentem	prezident	k1gMnSc7	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
společném	společný	k2eAgNnSc6d1	společné
hlasování	hlasování	k1gNnSc6	hlasování
Senátu	senát	k1gInSc2	senát
a	a	k8xC	a
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgInS	prosadit
nejen	nejen	k6eAd1	nejen
díky	díky	k7c3	díky
podpoře	podpora	k1gFnSc3	podpora
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
kvůli	kvůli	k7c3	kvůli
rozporům	rozpor	k1gInPc3	rozpor
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
vládní	vládní	k2eAgFnSc6d1	vládní
koalici	koalice	k1gFnSc6	koalice
<g/>
,	,	kIx,	,
vnitřnímu	vnitřní	k2eAgInSc3d1	vnitřní
boji	boj	k1gInSc3	boj
v	v	k7c6	v
ČSSD	ČSSD	kA	ČSSD
a	a	k8xC	a
podpoře	podpora	k1gFnSc3	podpora
části	část	k1gFnSc2	část
KSČM	KSČM	kA	KSČM
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
založil	založit	k5eAaPmAgMnS	založit
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Nadační	nadační	k2eAgInSc1d1	nadační
fond	fond	k1gInSc1	fond
manželů	manžel	k1gMnPc2	manžel
Livie	Livie	k1gFnSc2	Livie
a	a	k8xC	a
Václava	Václav	k1gMnSc2	Václav
Klausových	Klausových	k2eAgNnSc1d1	Klausových
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
posláním	poslání	k1gNnSc7	poslání
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
podpora	podpora	k1gFnSc1	podpora
humanitárních	humanitární	k2eAgFnPc2d1	humanitární
aktivit	aktivita	k1gFnPc2	aktivita
právnických	právnický	k2eAgFnPc2d1	právnická
a	a	k8xC	a
fyzických	fyzický	k2eAgFnPc2d1	fyzická
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
se	s	k7c7	s
zaměřením	zaměření	k1gNnSc7	zaměření
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
projektů	projekt	k1gInPc2	projekt
a	a	k8xC	a
aktivit	aktivita	k1gFnPc2	aktivita
zdravotně	zdravotně	k6eAd1	zdravotně
sociálního	sociální	k2eAgInSc2d1	sociální
charakteru	charakter	k1gInSc2	charakter
a	a	k8xC	a
projektů	projekt	k1gInPc2	projekt
a	a	k8xC	a
aktivit	aktivita	k1gFnPc2	aktivita
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Fond	fond	k1gInSc1	fond
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
šest	šest	k4xCc4	šest
projektů	projekt	k1gInPc2	projekt
<g/>
:	:	kIx,	:
Senioři	senior	k1gMnPc1	senior
komunikují	komunikovat	k5eAaImIp3nP	komunikovat
<g/>
,	,	kIx,	,
Jazykové	jazykový	k2eAgInPc1d1	jazykový
kurzy	kurz	k1gInPc1	kurz
<g/>
,	,	kIx,	,
Stipendia	stipendium	k1gNnPc1	stipendium
<g/>
,	,	kIx,	,
Kroužky	kroužek	k1gInPc1	kroužek
<g/>
,	,	kIx,	,
Podpora	podpora	k1gFnSc1	podpora
dětských	dětský	k2eAgFnPc2d1	dětská
obětí	oběť	k1gFnPc2	oběť
dopravních	dopravní	k2eAgFnPc2d1	dopravní
nehod	nehoda	k1gFnPc2	nehoda
a	a	k8xC	a
Řidičské	řidičský	k2eAgInPc4d1	řidičský
průkazy	průkaz	k1gInPc4	průkaz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
Klaus	Klaus	k1gMnSc1	Klaus
obdržel	obdržet	k5eAaPmAgMnS	obdržet
anticenu	anticen	k2eAgFnSc4d1	anticena
Zelená	zelený	k2eAgFnSc1d1	zelená
perla	perla	k1gFnSc1	perla
za	za	k7c4	za
výrok	výrok	k1gInSc4	výrok
"	"	kIx"	"
<g/>
Občanská	občanský	k2eAgFnSc1d1	občanská
společnost	společnost	k1gFnSc1	společnost
je	být	k5eAaImIp3nS	být
polemika	polemika	k1gFnSc1	polemika
se	s	k7c7	s
svobodnou	svobodný	k2eAgFnSc7d1	svobodná
společností	společnost	k1gFnSc7	společnost
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
je	být	k5eAaImIp3nS	být
povinností	povinnost	k1gFnSc7	povinnost
každého	každý	k3xTgMnSc2	každý
demokrata	demokrat	k1gMnSc2	demokrat
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
svých	svůj	k3xOyFgFnPc2	svůj
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
do	do	k7c2	do
konce	konec	k1gInSc2	konec
svých	svůj	k3xOyFgInPc2	svůj
věků	věk	k1gInPc2	věk
proti	proti	k7c3	proti
ní	on	k3xPp3gFnSc2	on
bojovat	bojovat	k5eAaImF	bojovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
podle	podle	k7c2	podle
deníku	deník	k1gInSc2	deník
Právo	právo	k1gNnSc4	právo
pronést	pronést	k5eAaPmF	pronést
12	[number]	k4	12
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
na	na	k7c6	na
semináři	seminář	k1gInSc6	seminář
CEP	cep	k1gInSc1	cep
s	s	k7c7	s
názvem	název	k1gInSc7	název
Postdemokracie	Postdemokracie	k1gFnSc2	Postdemokracie
<g/>
:	:	kIx,	:
hrozba	hrozba	k1gFnSc1	hrozba
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
naděje	naděje	k1gFnSc1	naděje
<g/>
?	?	kIx.	?
</s>
<s>
jako	jako	k9	jako
komentář	komentář	k1gInSc4	komentář
k	k	k7c3	k
příspěvku	příspěvek	k1gInSc3	příspěvek
ředitele	ředitel	k1gMnSc2	ředitel
české	český	k2eAgFnSc2d1	Česká
pobočky	pobočka	k1gFnSc2	pobočka
Greenpeace	Greenpeace	k1gFnSc2	Greenpeace
Jiřího	Jiří	k1gMnSc2	Jiří
Tuttera	Tutter	k1gMnSc2	Tutter
<g/>
.	.	kIx.	.
</s>
<s>
Klausovo	Klausův	k2eAgNnSc1d1	Klausovo
první	první	k4xOgNnSc1	první
funkční	funkční	k2eAgNnSc1d1	funkční
období	období	k1gNnSc1	období
vypršelo	vypršet	k5eAaPmAgNnS	vypršet
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
byl	být	k5eAaImAgInS	být
Klausovým	Klausův	k2eAgMnSc7d1	Klausův
soupeřem	soupeř	k1gMnSc7	soupeř
v	v	k7c6	v
první	první	k4xOgFnSc6	první
prezidentské	prezidentský	k2eAgFnSc6d1	prezidentská
volbě	volba	k1gFnSc6	volba
ekonom	ekonom	k1gMnSc1	ekonom
Jan	Jan	k1gMnSc1	Jan
Švejnar	Švejnar	k1gMnSc1	Švejnar
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
podporoval	podporovat	k5eAaImAgMnS	podporovat
i	i	k9	i
bývalý	bývalý	k2eAgMnSc1d1	bývalý
prezident	prezident	k1gMnSc1	prezident
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Volba	volba	k1gFnSc1	volba
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
8	[number]	k4	8
<g/>
.	.	kIx.	.
a	a	k8xC	a
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
a	a	k8xC	a
nepřinesla	přinést	k5eNaPmAgFnS	přinést
vítěze	vítěz	k1gMnPc4	vítěz
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
Václavu	Václav	k1gMnSc3	Václav
Klausovi	Klaus	k1gMnSc3	Klaus
stačilo	stačit	k5eAaBmAgNnS	stačit
ke	k	k7c3	k
zvolení	zvolení	k1gNnSc3	zvolení
ve	v	k7c6	v
třetím	třetí	k4xOgNnSc6	třetí
kole	kolo	k1gNnSc6	kolo
získat	získat	k5eAaPmF	získat
jediný	jediný	k2eAgInSc4d1	jediný
hlas	hlas	k1gInSc4	hlas
navíc	navíc	k6eAd1	navíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
volbě	volba	k1gFnSc6	volba
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2008	[number]	k4	2008
byl	být	k5eAaImAgMnS	být
Klaus	Klaus	k1gMnSc1	Klaus
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
kole	kolo	k1gNnSc6	kolo
prezidentem	prezident	k1gMnSc7	prezident
zvolen	zvolit	k5eAaPmNgMnS	zvolit
<g/>
,	,	kIx,	,
když	když	k8xS	když
získal	získat	k5eAaPmAgMnS	získat
141	[number]	k4	141
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
zvolení	zvolení	k1gNnSc3	zvolení
mu	on	k3xPp3gMnSc3	on
pomohly	pomoct	k5eAaPmAgInP	pomoct
hlasy	hlas	k1gInPc7	hlas
tří	tři	k4xCgMnPc2	tři
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
členů	člen	k1gMnPc2	člen
a	a	k8xC	a
poslanců	poslanec	k1gMnPc2	poslanec
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
Miloše	Miloš	k1gMnSc4	Miloš
Melčáka	Melčák	k1gMnSc4	Melčák
<g/>
,	,	kIx,	,
Michala	Michal	k1gMnSc2	Michal
Pohanky	Pohanka	k1gMnSc2	Pohanka
a	a	k8xC	a
Evžena	Evžen	k1gMnSc2	Evžen
Snítilého	Snítilý	k2eAgMnSc2d1	Snítilý
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
takzvanému	takzvaný	k2eAgMnSc3d1	takzvaný
"	"	kIx"	"
<g/>
chilskému	chilský	k2eAgInSc3d1	chilský
incidentu	incident	k1gInSc3	incident
<g/>
"	"	kIx"	"
došlo	dojít	k5eAaPmAgNnS	dojít
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
během	během	k7c2	během
státní	státní	k2eAgFnSc2d1	státní
návštěvy	návštěva	k1gFnSc2	návštěva
v	v	k7c6	v
Chile	Chile	k1gNnSc6	Chile
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Klaus	Klaus	k1gMnSc1	Klaus
si	se	k3xPyFc3	se
zde	zde	k6eAd1	zde
vzal	vzít	k5eAaPmAgMnS	vzít
a	a	k8xC	a
vložil	vložit	k5eAaPmAgMnS	vložit
do	do	k7c2	do
kapsy	kapsa	k1gFnSc2	kapsa
protokolární	protokolární	k2eAgNnSc4d1	protokolární
pero	pero	k1gNnSc4	pero
vyrobené	vyrobený	k2eAgNnSc4d1	vyrobené
z	z	k7c2	z
modrého	modrý	k2eAgInSc2d1	modrý
polodrahokamu	polodrahokam	k1gInSc2	polodrahokam
lapis	lapis	k1gInSc4	lapis
lazuli	lazout	k5eAaPmAgMnP	lazout
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
situaci	situace	k1gFnSc4	situace
natočila	natočit	k5eAaBmAgFnS	natočit
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
a	a	k8xC	a
záznam	záznam	k1gInSc4	záznam
také	také	k9	také
odvysílala	odvysílat	k5eAaPmAgFnS	odvysílat
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
měl	mít	k5eAaImAgMnS	mít
(	(	kIx(	(
<g/>
především	především	k9	především
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
<g/>
)	)	kIx)	)
několik	několik	k4yIc1	několik
milionů	milion	k4xCgInPc2	milion
zhlédnutí	zhlédnutí	k1gNnPc2	zhlédnutí
a	a	k8xC	a
vzbudil	vzbudit	k5eAaPmAgInS	vzbudit
značný	značný	k2eAgInSc1d1	značný
ohlas	ohlas	k1gInSc1	ohlas
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
i	i	k8xC	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2012	[number]	k4	2012
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
syny	syn	k1gMnPc7	syn
a	a	k8xC	a
kancléřem	kancléř	k1gMnSc7	kancléř
Jiřím	Jiří	k1gMnSc7	Jiří
Weiglem	Weigl	k1gMnSc7	Weigl
založil	založit	k5eAaPmAgMnS	založit
studijní	studijní	k2eAgFnSc4d1	studijní
a	a	k8xC	a
výzkumnou	výzkumný	k2eAgFnSc4d1	výzkumná
obecně	obecně	k6eAd1	obecně
prospěšnou	prospěšný	k2eAgFnSc4d1	prospěšná
společnost	společnost	k1gFnSc4	společnost
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Institut	institut	k1gInSc1	institut
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jejímiž	jejíž	k3xOyRp3gMnPc7	jejíž
cíly	cíl	k1gMnPc7	cíl
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
dokumentace	dokumentace	k1gFnSc1	dokumentace
<g/>
,	,	kIx,	,
výzkum	výzkum	k1gInSc1	výzkum
a	a	k8xC	a
propagace	propagace	k1gFnSc1	propagace
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
díla	dílo	k1gNnSc2	dílo
a	a	k8xC	a
myšlenek	myšlenka	k1gFnPc2	myšlenka
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
shromažďování	shromažďování	k1gNnSc1	shromažďování
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
propagace	propagace	k1gFnSc2	propagace
idejí	idea	k1gFnPc2	idea
a	a	k8xC	a
témat	téma	k1gNnPc2	téma
souvisejících	související	k2eAgFnPc2d1	související
s	s	k7c7	s
veřejným	veřejný	k2eAgNnSc7d1	veřejné
působením	působení	k1gNnSc7	působení
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
zakladatelů	zakladatel	k1gMnPc2	zakladatel
se	se	k3xPyFc4	se
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
činnosti	činnost	k1gFnSc6	činnost
účastní	účastnit	k5eAaImIp3nS	účastnit
např.	např.	kA	např.
Ladislav	Ladislav	k1gMnSc1	Ladislav
Jakl	Jakl	k1gMnSc1	Jakl
a	a	k8xC	a
europoslanec	europoslanec	k1gMnSc1	europoslanec
Ivo	Ivo	k1gMnSc1	Ivo
Strejček	Strejček	k1gMnSc1	Strejček
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
je	být	k5eAaImIp3nS	být
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
aktivním	aktivní	k2eAgMnSc7d1	aktivní
členem	člen	k1gMnSc7	člen
Montpelerinské	Montpelerinský	k2eAgFnSc2d1	Montpelerinská
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
sdružení	sdružení	k1gNnSc2	sdružení
liberálních	liberální	k2eAgMnPc2d1	liberální
ekonomů	ekonom	k1gMnPc2	ekonom
<g/>
.	.	kIx.	.
</s>
<s>
Vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
s	s	k7c7	s
projevy	projev	k1gInPc7	projev
na	na	k7c6	na
jejích	její	k3xOp3gFnPc6	její
konferencích	konference	k1gFnPc6	konference
a	a	k8xC	a
v	v	k7c6	v
září	září	k1gNnSc6	září
2012	[number]	k4	2012
se	se	k3xPyFc4	se
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
pozvání	pozvání	k1gNnSc6	pozvání
konalo	konat	k5eAaImAgNnS	konat
valné	valný	k2eAgNnSc1d1	Valné
shromáždění	shromáždění	k1gNnSc1	shromáždění
této	tento	k3xDgFnSc2	tento
společnosti	společnost	k1gFnSc2	společnost
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Klaus	Klaus	k1gMnSc1	Klaus
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
demokracii	demokracie	k1gFnSc4	demokracie
a	a	k8xC	a
tržní	tržní	k2eAgFnSc4d1	tržní
ekonomiku	ekonomika	k1gFnSc4	ekonomika
ohrožují	ohrožovat	k5eAaImIp3nP	ohrožovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
ztráta	ztráta	k1gFnSc1	ztráta
tradičních	tradiční	k2eAgFnPc2d1	tradiční
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
,	,	kIx,	,
populismus	populismus	k1gInSc1	populismus
demokratických	demokratický	k2eAgMnPc2d1	demokratický
politiků	politik	k1gMnPc2	politik
a	a	k8xC	a
rostoucí	rostoucí	k2eAgFnSc4d1	rostoucí
moc	moc	k1gFnSc4	moc
soudů	soud	k1gInPc2	soud
<g/>
,	,	kIx,	,
neziskových	ziskový	k2eNgFnPc2d1	nezisková
organizací	organizace	k1gFnPc2	organizace
a	a	k8xC	a
médií	médium	k1gNnPc2	médium
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2012	[number]	k4	2012
v	v	k7c6	v
Chrastavě	chrastavě	k6eAd1	chrastavě
na	na	k7c6	na
Liberecku	Liberecko	k1gNnSc6	Liberecko
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
útoku	útok	k1gInSc3	útok
na	na	k7c4	na
Václava	Václav	k1gMnSc4	Václav
Klause	Klaus	k1gMnSc4	Klaus
neškodnou	škodný	k2eNgFnSc4d1	neškodná
airsoftovou	airsoftová	k1gFnSc4	airsoftová
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Klaus	Klaus	k1gMnSc1	Klaus
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
oděrky	oděrka	k1gFnPc4	oděrka
na	na	k7c6	na
lokti	loket	k1gInSc6	loket
<g/>
.	.	kIx.	.
</s>
<s>
Útočník	útočník	k1gMnSc1	útočník
chtěl	chtít	k5eAaImAgMnS	chtít
upozornit	upozornit	k5eAaPmF	upozornit
na	na	k7c4	na
špatný	špatný	k2eAgInSc4d1	špatný
stav	stav	k1gInSc4	stav
politické	politický	k2eAgFnSc2d1	politická
scény	scéna	k1gFnSc2	scéna
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Za	za	k7c4	za
útok	útok	k1gInSc4	útok
byl	být	k5eAaImAgInS	být
potrestán	potrestat	k5eAaPmNgInS	potrestat
šesti	šest	k4xCc7	šest
měsíci	měsíc	k1gInPc7	měsíc
odnětí	odnětí	k1gNnSc4	odnětí
svobody	svoboda	k1gFnSc2	svoboda
podmíněně	podmíněně	k6eAd1	podmíněně
<g/>
.	.	kIx.	.
</s>
<s>
Senát	senát	k1gInSc1	senát
PČR	PČR	kA	PČR
dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
Klausova	Klausův	k2eAgNnSc2d1	Klausovo
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
schválil	schválit	k5eAaPmAgInS	schválit
podání	podání	k1gNnSc4	podání
ústavní	ústavní	k2eAgFnSc2d1	ústavní
žaloby	žaloba	k1gFnSc2	žaloba
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
pro	pro	k7c4	pro
velezradu	velezrada	k1gFnSc4	velezrada
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
první	první	k4xOgFnSc4	první
žalobu	žaloba	k1gFnSc4	žaloba
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Pro	pro	k7c4	pro
podání	podání	k1gNnSc4	podání
žaloby	žaloba	k1gFnSc2	žaloba
Ústavnímu	ústavní	k2eAgInSc3d1	ústavní
soudu	soud	k1gInSc3	soud
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
38	[number]	k4	38
přítomných	přítomný	k2eAgMnPc2d1	přítomný
senátorů	senátor	k1gMnPc2	senátor
a	a	k8xC	a
senátorek	senátorka	k1gFnPc2	senátorka
<g/>
,	,	kIx,	,
30	[number]	k4	30
bylo	být	k5eAaImAgNnS	být
proti	proti	k7c3	proti
a	a	k8xC	a
13	[number]	k4	13
se	se	k3xPyFc4	se
hlasování	hlasování	k1gNnSc2	hlasování
zdrželo	zdržet	k5eAaPmAgNnS	zdržet
nebo	nebo	k8xC	nebo
nezúčastnilo	zúčastnit	k5eNaPmAgNnS	zúčastnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ústavní	ústavní	k2eAgFnSc6d1	ústavní
žalobě	žaloba	k1gFnSc6	žaloba
bylo	být	k5eAaImAgNnS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
pět	pět	k4xCc1	pět
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
především	především	k9	především
Klausova	Klausův	k2eAgFnSc1d1	Klausova
kontroverzní	kontroverzní	k2eAgFnSc1d1	kontroverzní
novoroční	novoroční	k2eAgFnSc1d1	novoroční
amnestie	amnestie	k1gFnSc1	amnestie
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
otálení	otálení	k1gNnSc1	otálení
s	s	k7c7	s
podpisem	podpis	k1gInSc7	podpis
dodatku	dodatek	k1gInSc2	dodatek
k	k	k7c3	k
Evropské	evropský	k2eAgFnSc3d1	Evropská
sociální	sociální	k2eAgFnSc3d1	sociální
chartě	charta	k1gFnSc3	charta
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nerozhodl	rozhodnout	k5eNaPmAgMnS	rozhodnout
o	o	k7c6	o
jmenování	jmenování	k1gNnSc6	jmenování
Petra	Petr	k1gMnSc2	Petr
Langra	Langer	k1gMnSc2	Langer
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
ze	z	k7c2	z
soudních	soudní	k2eAgMnPc2d1	soudní
čekatelů	čekatel	k1gMnPc2	čekatel
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
podle	podle	k7c2	podle
žaloby	žaloba	k1gFnSc2	žaloba
uloženo	uložit	k5eAaPmNgNnS	uložit
soudem	soud	k1gInSc7	soud
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
bodem	bod	k1gInSc7	bod
bylo	být	k5eAaImAgNnS	být
nepodepsání	nepodepsání	k1gNnSc1	nepodepsání
doplňku	doplněk	k1gInSc2	doplněk
Lisabonské	lisabonský	k2eAgFnSc2d1	Lisabonská
smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c6	o
novém	nový	k2eAgInSc6d1	nový
záchranném	záchranný	k2eAgInSc6d1	záchranný
fondu	fond	k1gInSc6	fond
eurozóny	eurozóna	k1gFnSc2	eurozóna
i	i	k9	i
přes	přes	k7c4	přes
jeho	jeho	k3xOp3gNnSc4	jeho
schválení	schválení	k1gNnSc4	schválení
parlamentem	parlament	k1gInSc7	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Klaus	Klaus	k1gMnSc1	Klaus
podle	podle	k7c2	podle
žaloby	žaloba	k1gFnSc2	žaloba
také	také	k9	také
ohrozil	ohrozit	k5eAaPmAgInS	ohrozit
fungování	fungování	k1gNnSc4	fungování
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
téměř	téměř	k6eAd1	téměř
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
nejmenoval	jmenovat	k5eNaImAgMnS	jmenovat
žádného	žádný	k1gMnSc4	žádný
nového	nový	k2eAgMnSc4d1	nový
ústavního	ústavní	k2eAgMnSc4d1	ústavní
soudce	soudce	k1gMnSc4	soudce
a	a	k8xC	a
nenavrhl	navrhnout	k5eNaPmAgMnS	navrhnout
Senátu	senát	k1gInSc2	senát
nové	nový	k2eAgMnPc4d1	nový
kandidáty	kandidát	k1gMnPc4	kandidát
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
dvou	dva	k4xCgFnPc2	dva
-	-	kIx~	-
senátem	senát	k1gInSc7	senát
zamítnutých	zamítnutý	k2eAgInPc2d1	zamítnutý
-	-	kIx~	-
pokusů	pokus	k1gInPc2	pokus
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
premiéra	premiér	k1gMnSc2	premiér
Petra	Petr	k1gMnSc2	Petr
Nečase	Nečas	k1gMnSc2	Nečas
(	(	kIx(	(
<g/>
ODS	ODS	kA	ODS
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
schválení	schválení	k1gNnSc1	schválení
žaloby	žaloba	k1gFnSc2	žaloba
útokem	útok	k1gInSc7	útok
na	na	k7c4	na
pověst	pověst	k1gFnSc4	pověst
celé	celý	k2eAgFnSc2d1	celá
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
motivovaný	motivovaný	k2eAgMnSc1d1	motivovaný
"	"	kIx"	"
<g/>
osobní	osobní	k2eAgFnSc1d1	osobní
nesnášenlivostí	nesnášenlivost	k1gFnSc7	nesnášenlivost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
projednal	projednat	k5eAaPmAgInS	projednat
žalobu	žaloba	k1gFnSc4	žaloba
přednostně	přednostně	k6eAd1	přednostně
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
plénu	plénum	k1gNnSc6	plénum
pod	pod	k7c7	pod
sp	sp	k?	sp
<g/>
.	.	kIx.	.
zn.	zn.	kA	zn.
Pl.	Pl.	k1gFnSc2	Pl.
ÚS	ÚS	kA	ÚS
17	[number]	k4	17
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
Řízení	řízení	k1gNnSc1	řízení
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
bez	bez	k7c2	bez
věcného	věcný	k2eAgNnSc2d1	věcné
projednání	projednání	k1gNnSc2	projednání
zastaveno	zastaven	k2eAgNnSc1d1	zastaveno
se	s	k7c7	s
zdůvodněním	zdůvodnění	k1gNnSc7	zdůvodnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
již	již	k6eAd1	již
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nebyl	být	k5eNaImAgMnS	být
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
prezidentskými	prezidentský	k2eAgFnPc7d1	prezidentská
volbami	volba	k1gFnPc7	volba
2013	[number]	k4	2013
Klaus	Klaus	k1gMnSc1	Klaus
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
podporu	podpora	k1gFnSc4	podpora
kandidatury	kandidatura	k1gFnSc2	kandidatura
svého	svůj	k3xOyFgMnSc2	svůj
tajemníka	tajemník	k1gMnSc2	tajemník
Ladislava	Ladislav	k1gMnSc2	Ladislav
Jakla	Jakl	k1gMnSc2	Jakl
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
sám	sám	k3xTgMnSc1	sám
podepsal	podepsat	k5eAaPmAgMnS	podepsat
jeho	jeho	k3xOp3gFnSc4	jeho
nominační	nominační	k2eAgFnSc4d1	nominační
petici	petice	k1gFnSc4	petice
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
podpoře	podpora	k1gFnSc3	podpora
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
uvedl	uvést	k5eAaPmAgMnS	uvést
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
Lidové	lidový	k2eAgFnPc4d1	lidová
noviny	novina	k1gFnPc4	novina
<g/>
,	,	kIx,	,
že	že	k8xS	že
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
zájemců	zájemce	k1gMnPc2	zájemce
o	o	k7c4	o
prezidentský	prezidentský	k2eAgInSc4d1	prezidentský
post	post	k1gInSc4	post
je	být	k5eAaImIp3nS	být
Zeman	Zeman	k1gMnSc1	Zeman
jediným	jediný	k2eAgMnSc7d1	jediný
politikem	politik	k1gMnSc7	politik
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Tak	tak	k9	tak
proč	proč	k6eAd1	proč
bych	by	kYmCp1nS	by
ho	on	k3xPp3gMnSc4	on
nemohl	moct	k5eNaImAgMnS	moct
nepřímo	přímo	k6eNd1	přímo
podpořit	podpořit	k5eAaPmF	podpořit
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
prezidentských	prezidentský	k2eAgNnPc2d1	prezidentské
vet	veto	k1gNnPc2	veto
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
dříve	dříve	k6eAd2	dříve
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
Václava	Václav	k1gMnSc4	Václav
Havla	Havel	k1gMnSc4	Havel
za	za	k7c4	za
časté	častý	k2eAgNnSc4d1	časté
používání	používání	k1gNnSc4	používání
prezidentského	prezidentský	k2eAgNnSc2d1	prezidentské
veta	veto	k1gNnSc2	veto
<g/>
,	,	kIx,	,
sám	sám	k3xTgInSc1	sám
však	však	k9	však
jako	jako	k8xC	jako
prezident	prezident	k1gMnSc1	prezident
právo	právo	k1gNnSc4	právo
veta	veto	k1gNnSc2	veto
často	často	k6eAd1	často
používal	používat	k5eAaImAgInS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
prvního	první	k4xOgNnSc2	první
prezidentského	prezidentský	k2eAgNnSc2d1	prezidentské
období	období	k1gNnSc2	období
vetoval	vetovat	k5eAaBmAgInS	vetovat
celkem	celkem	k6eAd1	celkem
34	[number]	k4	34
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2006	[number]	k4	2006
například	například	k6eAd1	například
návrh	návrh	k1gInSc4	návrh
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
registrovaném	registrovaný	k2eAgNnSc6d1	registrované
partnerství	partnerství	k1gNnSc6	partnerství
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
špatný	špatný	k2eAgInSc1d1	špatný
a	a	k8xC	a
rozšiřoval	rozšiřovat	k5eAaImAgInS	rozšiřovat
oblast	oblast	k1gFnSc4	oblast
státních	státní	k2eAgInPc2d1	státní
zásahů	zásah	k1gInPc2	zásah
do	do	k7c2	do
lidských	lidský	k2eAgInPc2d1	lidský
životů	život	k1gInPc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
krok	krok	k1gInSc4	krok
byl	být	k5eAaImAgInS	být
kritizován	kritizován	k2eAgInSc1d1	kritizován
např.	např.	kA	např.
Jiřím	Jiří	k1gMnSc7	Jiří
Hromadou	Hromada	k1gMnSc7	Hromada
z	z	k7c2	z
Gay	gay	k1gMnSc1	gay
iniciativy	iniciativa	k1gFnSc2	iniciativa
a	a	k8xC	a
premiérem	premiér	k1gMnSc7	premiér
Jiřím	Jiří	k1gMnSc7	Jiří
Paroubkem	Paroubek	k1gMnSc7	Paroubek
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
veto	veto	k1gNnSc1	veto
nejtěsnější	těsný	k2eAgFnSc2d3	nejtěsnější
většinou	většinou	k6eAd1	většinou
přehlasovala	přehlasovat	k5eAaBmAgFnS	přehlasovat
a	a	k8xC	a
zákon	zákon	k1gInSc1	zákon
začal	začít	k5eAaPmAgInS	začít
platit	platit	k5eAaImF	platit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pak	pak	k6eAd1	pak
parlament	parlament	k1gInSc1	parlament
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2008	[number]	k4	2008
schválil	schválit	k5eAaPmAgInS	schválit
novelu	novela	k1gFnSc4	novela
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
Klaus	Klaus	k1gMnSc1	Klaus
ji	on	k3xPp3gFnSc4	on
nechal	nechat	k5eAaPmAgMnS	nechat
projít	projít	k5eAaPmF	projít
bez	bez	k7c2	bez
svého	svůj	k3xOyFgInSc2	svůj
podpisu	podpis	k1gInSc2	podpis
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhého	druhý	k4xOgNnSc2	druhý
prezidentského	prezidentský	k2eAgNnSc2d1	prezidentské
období	období	k1gNnSc2	období
využil	využít	k5eAaPmAgMnS	využít
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
prezidentské	prezidentský	k2eAgNnSc4d1	prezidentské
veto	veto	k1gNnSc4	veto
celkem	celkem	k6eAd1	celkem
29	[number]	k4	29
<g/>
krát	krát	k6eAd1	krát
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
vetoval	vetovat	k5eAaBmAgMnS	vetovat
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2008	[number]	k4	2008
tzv.	tzv.	kA	tzv.
antidiskriminační	antidiskriminační	k2eAgInSc4d1	antidiskriminační
zákon	zákon	k1gInSc4	zákon
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yIgInSc6	který
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
je	být	k5eAaImIp3nS	být
zbytečný	zbytečný	k2eAgMnSc1d1	zbytečný
a	a	k8xC	a
kontraproduktivní	kontraproduktivní	k2eAgMnSc1d1	kontraproduktivní
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Sněmovnou	sněmovna	k1gFnSc7	sněmovna
byl	být	k5eAaImAgInS	být
však	však	k9	však
přehlasován	přehlasován	k2eAgInSc1d1	přehlasován
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
praxí	praxe	k1gFnSc7	praxe
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
omezil	omezit	k5eAaPmAgMnS	omezit
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
udělování	udělování	k1gNnSc4	udělování
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
milosti	milost	k1gFnSc2	milost
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
udělenými	udělený	k2eAgFnPc7d1	udělená
milostmi	milost	k1gFnPc7	milost
je	být	k5eAaImIp3nS	být
však	však	k9	však
několik	několik	k4yIc4	několik
kontroverzních	kontroverzní	k2eAgInPc2d1	kontroverzní
případů	případ	k1gInPc2	případ
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc4d3	nejznámější
jsou	být	k5eAaImIp3nP	být
udělení	udělení	k1gNnSc4	udělení
milosti	milost	k1gFnSc2	milost
podnikateli	podnikatel	k1gMnPc7	podnikatel
a	a	k8xC	a
sponzorovi	sponzor	k1gMnSc3	sponzor
ODS	ODS	kA	ODS
Zdeňku	Zdeněk	k1gMnSc3	Zdeněk
Kratochvílovi	Kratochvíl	k1gMnSc3	Kratochvíl
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Emilovi	Emil	k1gMnSc3	Emil
Novotnému	Novotný	k1gMnSc3	Novotný
<g/>
,	,	kIx,	,
odsouzenému	odsouzený	k1gMnSc3	odsouzený
v	v	k7c6	v
Thajsku	Thajsko	k1gNnSc6	Thajsko
za	za	k7c4	za
pašování	pašování	k1gNnSc4	pašování
drog	droga	k1gFnPc2	droga
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
úplatné	úplatný	k2eAgFnSc3d1	úplatná
policistce	policistka	k1gFnSc3	policistka
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spolupracovníkům	spolupracovník	k1gMnPc3	spolupracovník
zavražděného	zavražděný	k2eAgMnSc2d1	zavražděný
podnikatele	podnikatel	k1gMnSc2	podnikatel
Františka	František	k1gMnSc2	František
Mrázka	Mrázek	k1gMnSc2	Mrázek
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jinému	jiný	k2eAgMnSc3d1	jiný
podnikateli	podnikatel	k1gMnSc3	podnikatel
<g/>
,	,	kIx,	,
odsouzenému	odsouzený	k1gMnSc3	odsouzený
za	za	k7c4	za
půlmiliardový	půlmiliardový	k2eAgInSc4d1	půlmiliardový
podvod	podvod	k1gInSc4	podvod
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
a	a	k8xC	a
bývalé	bývalý	k2eAgFnSc6d1	bývalá
ředitelce	ředitelka	k1gFnSc6	ředitelka
Metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
univerzity	univerzita	k1gFnSc2	univerzita
Praha	Praha	k1gFnSc1	Praha
Anně	Anna	k1gFnSc3	Anna
Benešové	Benešové	k2eAgMnPc2d1	Benešové
<g/>
,	,	kIx,	,
odsouzené	odsouzený	k2eAgMnPc4d1	odsouzený
za	za	k7c4	za
podplácení	podplácení	k1gNnSc4	podplácení
a	a	k8xC	a
zpronevěru	zpronevěra	k1gFnSc4	zpronevěra
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
byl	být	k5eAaImAgInS	být
odložen	odložen	k2eAgInSc1d1	odložen
nástup	nástup	k1gInSc1	nástup
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
dalšímu	další	k2eAgInSc3d1	další
spolupracovníkovi	spolupracovník	k1gMnSc3	spolupracovník
podnikatele	podnikatel	k1gMnSc4	podnikatel
Mrázka	Mrázek	k1gMnSc2	Mrázek
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Amnestie	amnestie	k1gFnSc2	amnestie
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
20	[number]	k4	20
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
vzniku	vznik	k1gInSc2	vznik
samostatné	samostatný	k2eAgFnSc2d1	samostatná
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
prezident	prezident	k1gMnSc1	prezident
Klaus	Klaus	k1gMnSc1	Klaus
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2013	[number]	k4	2013
dílčí	dílčí	k2eAgFnSc4d1	dílčí
amnestii	amnestie	k1gFnSc4	amnestie
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
prominul	prominout	k5eAaPmAgMnS	prominout
nebo	nebo	k8xC	nebo
zahladil	zahladit	k5eAaPmAgMnS	zahladit
roční	roční	k2eAgInSc4d1	roční
a	a	k8xC	a
kratší	krátký	k2eAgInPc4d2	kratší
nepodmíněné	podmíněný	k2eNgInPc4d1	nepodmíněný
tresty	trest	k1gInPc4	trest
všem	všecek	k3xTgFnPc3	všecek
odsouzeným	odsouzený	k2eAgFnPc3d1	odsouzená
a	a	k8xC	a
obviněným	obviněný	k2eAgFnPc3d1	obviněná
osobám	osoba	k1gFnPc3	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Amnestie	amnestie	k1gFnSc1	amnestie
se	se	k3xPyFc4	se
vztahovala	vztahovat	k5eAaImAgFnS	vztahovat
i	i	k9	i
na	na	k7c4	na
ty	ten	k3xDgFnPc4	ten
osoby	osoba	k1gFnPc4	osoba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
strávit	strávit	k5eAaPmF	strávit
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
nanejvýš	nanejvýš	k6eAd1	nanejvýš
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikoho	nikdo	k3yNnSc4	nikdo
nezabily	zabít	k5eNaPmAgInP	zabít
<g/>
,	,	kIx,	,
vážně	vážně	k6eAd1	vážně
nezranily	zranit	k5eNaPmAgFnP	zranit
a	a	k8xC	a
ani	ani	k8xC	ani
neznásilnily	znásilnit	k5eNaPmAgInP	znásilnit
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
téměř	téměř	k6eAd1	téměř
všem	všecek	k3xTgMnPc3	všecek
vězňům	vězeň	k1gMnPc3	vězeň
starším	starý	k2eAgNnPc3d2	starší
než	než	k8xS	než
75	[number]	k4	75
let	léto	k1gNnPc2	léto
byly	být	k5eAaImAgInP	být
jejich	jejich	k3xOp3gInPc1	jejich
tresty	trest	k1gInPc1	trest
prominuty	prominut	k2eAgInPc1d1	prominut
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
se	se	k3xPyFc4	se
také	také	k9	také
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
zastavit	zastavit	k5eAaPmF	zastavit
i	i	k9	i
vleklá	vleklý	k2eAgFnSc1d1	vleklá
trestní	trestní	k2eAgNnSc4d1	trestní
stíhání	stíhání	k1gNnSc4	stíhání
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
od	od	k7c2	od
jejich	jejich	k3xOp3gNnSc2	jejich
zahájení	zahájení	k1gNnSc2	zahájení
uplynulo	uplynout	k5eAaPmAgNnS	uplynout
přes	přes	k7c4	přes
osm	osm	k4xCc4	osm
let	léto	k1gNnPc2	léto
a	a	k8xC	a
týkaly	týkat	k5eAaImAgInP	týkat
se	se	k3xPyFc4	se
maximálně	maximálně	k6eAd1	maximálně
desetiletých	desetiletý	k2eAgInPc2d1	desetiletý
trestů	trest	k1gInPc2	trest
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
bod	bod	k1gInSc1	bod
amnestie	amnestie	k1gFnSc2	amnestie
se	se	k3xPyFc4	se
týkal	týkat	k5eAaImAgInS	týkat
například	například	k6eAd1	například
bývalého	bývalý	k2eAgMnSc2d1	bývalý
předsedy	předseda	k1gMnSc2	předseda
Českomoravského	českomoravský	k2eAgInSc2d1	českomoravský
fotbalového	fotbalový	k2eAgInSc2d1	fotbalový
svazu	svaz	k1gInSc2	svaz
Františka	František	k1gMnSc2	František
Chvalovského	Chvalovský	k2eAgMnSc2d1	Chvalovský
<g/>
,	,	kIx,	,
odsouzeného	odsouzený	k2eAgMnSc2d1	odsouzený
za	za	k7c4	za
úvěrový	úvěrový	k2eAgInSc4d1	úvěrový
podvod	podvod	k1gInSc4	podvod
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
manažerů	manažer	k1gMnPc2	manažer
vytunelovaného	vytunelovaný	k2eAgInSc2d1	vytunelovaný
H-Systemu	H-System	k1gInSc2	H-System
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
jediná	jediný	k2eAgFnSc1d1	jediná
amnestie	amnestie	k1gFnSc1	amnestie
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
úřadu	úřad	k1gInSc6	úřad
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
<g/>
.	.	kIx.	.
</s>
<s>
Amnestie	amnestie	k1gFnSc1	amnestie
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
jak	jak	k8xC	jak
laickou	laický	k2eAgFnSc7d1	laická
(	(	kIx(	(
<g/>
v	v	k7c6	v
průzkumu	průzkum	k1gInSc6	průzkum
společnosti	společnost	k1gFnSc2	společnost
Median	Mediany	k1gInPc2	Mediany
vyjádřilo	vyjádřit	k5eAaPmAgNnS	vyjádřit
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
s	s	k7c7	s
amnestií	amnestie	k1gFnSc7	amnestie
82,2	[number]	k4	82,2
%	%	kIx~	%
respondentů	respondent	k1gMnPc2	respondent
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
odbornou	odborný	k2eAgFnSc7d1	odborná
veřejností	veřejnost	k1gFnSc7	veřejnost
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
předseda	předseda	k1gMnSc1	předseda
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
Pavel	Pavel	k1gMnSc1	Pavel
Rychetský	Rychetský	k1gMnSc1	Rychetský
označil	označit	k5eAaPmAgMnS	označit
amnestii	amnestie	k1gFnSc4	amnestie
za	za	k7c4	za
"	"	kIx"	"
<g/>
neuvěřitelný	uvěřitelný	k2eNgInSc4d1	neuvěřitelný
diletantismus	diletantismus	k1gInSc4	diletantismus
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
skupiny	skupina	k1gFnSc2	skupina
senátorů	senátor	k1gMnPc2	senátor
na	na	k7c6	na
zrušení	zrušení	k1gNnSc6	zrušení
kontroverzní	kontroverzní	k2eAgFnSc2d1	kontroverzní
části	část	k1gFnSc2	část
o	o	k7c6	o
zastavení	zastavení	k1gNnSc6	zastavení
trestního	trestní	k2eAgNnSc2d1	trestní
stíhání	stíhání	k1gNnSc2	stíhání
však	však	k9	však
byly	být	k5eAaImAgInP	být
Ústavním	ústavní	k2eAgInSc7d1	ústavní
soudem	soud	k1gInSc7	soud
odmítnuty	odmítnout	k5eAaPmNgFnP	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Amnestie	amnestie	k1gFnSc1	amnestie
byla	být	k5eAaImAgFnS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
pěti	pět	k4xCc2	pět
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
Senát	senát	k1gInSc1	senát
podal	podat	k5eAaPmAgInS	podat
na	na	k7c4	na
Klause	Klaus	k1gMnSc4	Klaus
ústavní	ústavní	k2eAgFnSc2d1	ústavní
žaloby	žaloba	k1gFnSc2	žaloba
pro	pro	k7c4	pro
velezradu	velezrada	k1gFnSc4	velezrada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2005	[number]	k4	2005
Klaus	Klaus	k1gMnSc1	Klaus
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
jmenovat	jmenovat	k5eAaBmF	jmenovat
několik	několik	k4yIc4	několik
justičních	justiční	k2eAgMnPc2d1	justiční
čekatelů	čekatel	k1gMnPc2	čekatel
navržených	navržený	k2eAgMnPc2d1	navržený
ministrem	ministr	k1gMnSc7	ministr
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
soudce	soudce	k1gMnSc2	soudce
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odůvodnil	odůvodnit	k5eAaPmAgMnS	odůvodnit
jejich	jejich	k3xOp3gMnSc7	jejich
nízkým	nízký	k2eAgInSc7d1	nízký
věkem	věk	k1gInSc7	věk
(	(	kIx(	(
<g/>
méně	málo	k6eAd2	málo
než	než	k8xS	než
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
bránili	bránit	k5eAaImAgMnP	bránit
správní	správní	k2eAgFnSc7d1	správní
žalobou	žaloba	k1gFnSc7	žaloba
<g/>
,	,	kIx,	,
v	v	k7c6	v
soudním	soudní	k2eAgNnSc6d1	soudní
řízení	řízení	k1gNnSc6	řízení
však	však	k9	však
vytrval	vytrvat	k5eAaPmAgMnS	vytrvat
jen	jen	k9	jen
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Langer	Langer	k1gMnSc1	Langer
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
případě	případ	k1gInSc6	případ
Městský	městský	k2eAgInSc4d1	městský
soud	soud	k1gInSc4	soud
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
shledal	shledat	k5eAaPmAgMnS	shledat
Klausovo	Klausův	k2eAgNnSc4d1	Klausovo
jednání	jednání	k1gNnSc4	jednání
nezákonným	zákonný	k2eNgMnSc7d1	nezákonný
a	a	k8xC	a
uložil	uložit	k5eAaPmAgInS	uložit
mu	on	k3xPp3gMnSc3	on
justičního	justiční	k2eAgInSc2d1	justiční
čekatele	čekatel	k1gMnPc4	čekatel
soudcem	soudce	k1gMnSc7	soudce
jmenovat	jmenovat	k5eAaImF	jmenovat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gNnSc4	jeho
nejmenování	nejmenování	k1gNnSc4	nejmenování
řádně	řádně	k6eAd1	řádně
zdůvodnit	zdůvodnit	k5eAaPmF	zdůvodnit
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
i	i	k9	i
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
správní	správní	k2eAgInSc1d1	správní
soud	soud	k1gInSc1	soud
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dostupných	dostupný	k2eAgFnPc2d1	dostupná
informací	informace	k1gFnPc2	informace
však	však	k9	však
tak	tak	k6eAd1	tak
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
neučinil	učinit	k5eNaPmAgMnS	učinit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2005	[number]	k4	2005
Klaus	Klaus	k1gMnSc1	Klaus
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
jmenovat	jmenovat	k5eAaImF	jmenovat
prezidenta	prezident	k1gMnSc4	prezident
České	český	k2eAgFnSc2d1	Česká
lékařské	lékařský	k2eAgFnSc2d1	lékařská
komory	komora	k1gFnSc2	komora
(	(	kIx(	(
<g/>
ČLK	ČLK	kA	ČLK
<g/>
)	)	kIx)	)
Davida	David	k1gMnSc2	David
Ratha	Rath	k1gMnSc2	Rath
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
členem	člen	k1gMnSc7	člen
a	a	k8xC	a
poslancem	poslanec	k1gMnSc7	poslanec
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
ministra	ministr	k1gMnSc2	ministr
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
Jiřího	Jiří	k1gMnSc2	Jiří
Paroubka	Paroubek	k1gMnSc2	Paroubek
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Klause	Klaus	k1gMnSc2	Klaus
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
Rath	Rath	k1gMnSc1	Rath
díky	díky	k7c3	díky
svému	svůj	k3xOyFgNnSc3	svůj
předsednictví	předsednictví	k1gNnSc3	předsednictví
ČLK	ČLK	kA	ČLK
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
střetu	střet	k1gInSc2	střet
zájmů	zájem	k1gInPc2	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Rath	Rath	k1gInSc1	Rath
nakonec	nakonec	k6eAd1	nakonec
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2005	[number]	k4	2005
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
prezidenta	prezident	k1gMnSc2	prezident
ČLK	ČLK	kA	ČLK
abdikoval	abdikovat	k5eAaBmAgMnS	abdikovat
a	a	k8xC	a
Klaus	Klaus	k1gMnSc1	Klaus
jej	on	k3xPp3gNnSc2	on
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
ministra	ministr	k1gMnSc2	ministr
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
<g/>
.	.	kIx.	.
</s>
<s>
Přístupovou	přístupový	k2eAgFnSc4d1	přístupová
smlouvu	smlouva	k1gFnSc4	smlouva
k	k	k7c3	k
Evropské	evropský	k2eAgFnSc3d1	Evropská
unii	unie	k1gFnSc3	unie
podepsal	podepsat	k5eAaPmAgMnS	podepsat
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
tehdejším	tehdejší	k2eAgMnSc7d1	tehdejší
premiérem	premiér	k1gMnSc7	premiér
Vladimírem	Vladimír	k1gMnSc7	Vladimír
Špidlou	Špidla	k1gMnSc7	Špidla
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2003	[number]	k4	2003
v	v	k7c6	v
Aténách	Atény	k1gFnPc6	Atény
<g/>
.	.	kIx.	.
</s>
<s>
Smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c6	o
Ústavě	ústava	k1gFnSc6	ústava
pro	pro	k7c4	pro
Evropu	Evropa	k1gFnSc4	Evropa
jménem	jméno	k1gNnSc7	jméno
prezidenta	prezident	k1gMnSc2	prezident
Klause	Klaus	k1gMnSc2	Klaus
za	za	k7c4	za
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
podepsali	podepsat	k5eAaPmAgMnP	podepsat
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2004	[number]	k4	2004
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
premiér	premiér	k1gMnSc1	premiér
Stanislav	Stanislav	k1gMnSc1	Stanislav
Gross	Gross	k1gMnSc1	Gross
a	a	k8xC	a
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Cyril	Cyril	k1gMnSc1	Cyril
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
však	však	k9	však
poté	poté	k6eAd1	poté
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
jako	jako	k9	jako
kritik	kritik	k1gMnSc1	kritik
evropské	evropský	k2eAgFnSc2d1	Evropská
integrace	integrace	k1gFnSc2	integrace
a	a	k8xC	a
fungování	fungování	k1gNnSc2	fungování
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
ekonomické	ekonomický	k2eAgFnPc4d1	ekonomická
svobody	svoboda	k1gFnPc4	svoboda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odmítal	odmítat	k5eAaImAgMnS	odmítat
bruselský	bruselský	k2eAgInSc4d1	bruselský
politický	politický	k2eAgInSc4d1	politický
centralismus	centralismus	k1gInSc4	centralismus
i	i	k8xC	i
politickou	politický	k2eAgFnSc4d1	politická
integraci	integrace	k1gFnSc4	integrace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
podle	podle	k7c2	podle
Klause	Klaus	k1gMnSc2	Klaus
byla	být	k5eAaImAgFnS	být
možná	možná	k6eAd1	možná
jen	jen	k9	jen
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
kontrolovatelnost	kontrolovatelnost	k1gFnSc1	kontrolovatelnost
výkonné	výkonný	k2eAgFnSc2d1	výkonná
moci	moc	k1gFnSc2	moc
není	být	k5eNaImIp3nS	být
podle	podle	k7c2	podle
něho	on	k3xPp3gNnSc2	on
možná	možná	k9	možná
v	v	k7c6	v
ničem	nic	k3yNnSc6	nic
větším	veliký	k2eAgNnSc6d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
národním	národní	k2eAgInSc6d1	národní
státě	stát	k1gInSc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
na	na	k7c6	na
rozcestí	rozcestí	k1gNnSc6	rozcestí
-	-	kIx~	-
Čas	čas	k1gInSc1	čas
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
došel	dojít	k5eAaPmAgInS	dojít
až	až	k9	až
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejlépe	dobře	k6eAd3	dobře
by	by	kYmCp3nS	by
pro	pro	k7c4	pro
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
z	z	k7c2	z
"	"	kIx"	"
<g/>
antidemokratické	antidemokratický	k2eAgFnSc2d1	antidemokratická
<g/>
"	"	kIx"	"
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
<g/>
.	.	kIx.	.
</s>
<s>
Klausův	Klausův	k2eAgInSc1d1	Klausův
"	"	kIx"	"
<g/>
euroskepticismus	euroskepticismus	k1gInSc1	euroskepticismus
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
eurorealismus	eurorealismus	k1gInSc1	eurorealismus
<g/>
"	"	kIx"	"
bývá	bývat	k5eAaImIp3nS	bývat
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Daniel	Daniel	k1gMnSc1	Daniel
Esperza	Esperz	k1gMnSc2	Esperz
z	z	k7c2	z
Palackého	Palackého	k2eAgFnSc2d1	Palackého
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
považuje	považovat	k5eAaImIp3nS	považovat
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Klausův	Klausův	k2eAgInSc1d1	Klausův
euroskepticismus	euroskepticismus	k1gInSc1	euroskepticismus
ze	z	k7c2	z
hlavní	hlavní	k2eAgFnSc2d1	hlavní
příčinu	příčina	k1gFnSc4	příčina
náhlého	náhlý	k2eAgNnSc2d1	náhlé
odmítnutí	odmítnutí	k1gNnSc2	odmítnutí
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
scéně	scéna	k1gFnSc6	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jeho	jeho	k3xOp3gInSc2	jeho
názoru	názor	k1gInSc2	názor
se	se	k3xPyFc4	se
Klaus	Klaus	k1gMnSc1	Klaus
ideologicky	ideologicky	k6eAd1	ideologicky
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
britským	britský	k2eAgInSc7d1	britský
euroskepticismem	euroskepticismus	k1gInSc7	euroskepticismus
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
politickou	politický	k2eAgFnSc7d1	politická
realitou	realita	k1gFnSc7	realita
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
nemá	mít	k5eNaImIp3nS	mít
velmocenské	velmocenský	k2eAgNnSc4d1	velmocenské
postavení	postavení	k1gNnSc4	postavení
ani	ani	k8xC	ani
alternativu	alternativa	k1gFnSc4	alternativa
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
jako	jako	k8xS	jako
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Klausův	Klausův	k2eAgInSc1d1	Klausův
euroskepticismus	euroskepticismus	k1gInSc1	euroskepticismus
či	či	k8xC	či
eurorealismus	eurorealismus	k1gInSc1	eurorealismus
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
realitou	realita	k1gFnSc7	realita
a	a	k8xC	a
nelze	lze	k6eNd1	lze
jej	on	k3xPp3gMnSc4	on
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
jako	jako	k9	jako
v	v	k7c6	v
britském	britský	k2eAgInSc6d1	britský
případě	případ	k1gInSc6	případ
teorií	teorie	k1gFnPc2	teorie
racionální	racionální	k2eAgFnSc2d1	racionální
volby	volba	k1gFnSc2	volba
<g/>
,	,	kIx,	,
míní	mínit	k5eAaImIp3nS	mínit
Esperza	Esperza	k1gFnSc1	Esperza
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
ptá	ptat	k5eAaImIp3nS	ptat
<g/>
,	,	kIx,	,
nakolik	nakolik	k6eAd1	nakolik
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
z	z	k7c2	z
Klausova	Klausův	k2eAgInSc2d1	Klausův
euroskepticismu	euroskepticismus	k1gInSc2	euroskepticismus
zahraničně-politicky	zahraničněoliticky	k6eAd1	zahraničně-politicky
profitovala	profitovat	k5eAaBmAgFnS	profitovat
a	a	k8xC	a
co	co	k9	co
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
jeho	jeho	k3xOp3gInSc7	jeho
politickým	politický	k2eAgInSc7d1	politický
cílem	cíl	k1gInSc7	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Propast	propast	k1gFnSc1	propast
mezi	mezi	k7c7	mezi
ideologií	ideologie	k1gFnSc7	ideologie
a	a	k8xC	a
realitou	realita	k1gFnSc7	realita
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
dle	dle	k7c2	dle
autora	autor	k1gMnSc2	autor
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Klaus	Klaus	k1gMnSc1	Klaus
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
idealista	idealista	k1gMnSc1	idealista
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
slepě	slepě	k6eAd1	slepě
věří	věřit	k5eAaImIp3nS	věřit
ve	v	k7c4	v
svoji	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
ideologii	ideologie	k1gFnSc4	ideologie
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
prezentoval	prezentovat	k5eAaBmAgMnS	prezentovat
jako	jako	k9	jako
výjimečný	výjimečný	k2eAgInSc4d1	výjimečný
způsob	způsob	k1gInSc4	způsob
řešení	řešení	k1gNnSc2	řešení
současné	současný	k2eAgFnSc2d1	současná
politické	politický	k2eAgFnSc2d1	politická
krize	krize	k1gFnSc2	krize
EU	EU	kA	EU
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Esperza	Esperza	k1gFnSc1	Esperza
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
tento	tento	k3xDgInSc1	tento
euroskeptismus	euroskeptismus	k1gInSc1	euroskeptismus
Klausovým	Klausův	k2eAgNnSc7d1	Klausovo
srovnáním	srovnání	k1gNnSc7	srovnání
kritických	kritický	k2eAgInPc2d1	kritický
postojů	postoj	k1gInPc2	postoj
k	k	k7c3	k
EU	EU	kA	EU
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Husem	Hus	k1gMnSc7	Hus
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
kritikou	kritika	k1gFnSc7	kritika
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
Klausovou	Klausův	k2eAgFnSc7d1	Klausova
sebestylizací	sebestylizace	k1gFnSc7	sebestylizace
v	v	k7c4	v
disidenta	disident	k1gMnSc4	disident
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
EU	EU	kA	EU
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
odrážejí	odrážet	k5eAaImIp3nP	odrážet
v	v	k7c6	v
Esterzově	Esterzův	k2eAgFnSc6d1	Esterzův
interpretaci	interpretace	k1gFnSc6	interpretace
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
základních	základní	k2eAgInPc2d1	základní
národních	národní	k2eAgInPc2d1	národní
mýtů	mýtus	k1gInPc2	mýtus
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
-	-	kIx~	-
husitské	husitský	k2eAgNnSc4d1	husitské
stigma	stigma	k1gNnSc4	stigma
a	a	k8xC	a
představu	představa	k1gFnSc4	představa
o	o	k7c6	o
národní	národní	k2eAgFnSc6d1	národní
výjimečnosti	výjimečnost	k1gFnSc6	výjimečnost
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
byl	být	k5eAaImAgMnS	být
kritikem	kritik	k1gMnSc7	kritik
Lisabonské	lisabonský	k2eAgFnSc2d1	Lisabonská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nálezu	nález	k1gInSc6	nález
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ji	on	k3xPp3gFnSc4	on
neshledal	shledat	k5eNaPmAgMnS	shledat
protiústavní	protiústavní	k2eAgMnSc1d1	protiústavní
<g/>
,	,	kIx,	,
však	však	k9	však
smlouvu	smlouva	k1gFnSc4	smlouva
podepsal	podepsat	k5eAaPmAgInS	podepsat
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
prý	prý	k9	prý
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
zvažoval	zvažovat	k5eAaImAgMnS	zvažovat
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vzdá	vzdát	k5eAaPmIp3nS	vzdát
prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
evropskou	evropský	k2eAgFnSc7d1	Evropská
měnovou	měnový	k2eAgFnSc7d1	měnová
krizí	krize	k1gFnSc7	krize
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
Klaus	Klaus	k1gMnSc1	Klaus
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
se	s	k7c7	s
vstupem	vstup	k1gInSc7	vstup
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
do	do	k7c2	do
eurozóny	eurozóna	k1gFnSc2	eurozóna
(	(	kIx(	(
<g/>
s	s	k7c7	s
opuštěním	opuštění	k1gNnSc7	opuštění
koruny	koruna	k1gFnSc2	koruna
a	a	k8xC	a
přijetím	přijetí	k1gNnSc7	přijetí
eura	euro	k1gNnSc2	euro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vycházel	vycházet	k5eAaImAgInS	vycházet
přitom	přitom	k6eAd1	přitom
z	z	k7c2	z
přesvědčení	přesvědčení	k1gNnSc2	přesvědčení
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
že	že	k8xS	že
nemáme	mít	k5eNaImIp1nP	mít
takovou	takový	k3xDgFnSc4	takový
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
výkonnost	výkonnost	k1gFnSc4	výkonnost
jako	jako	k8xS	jako
Německo	Německo	k1gNnSc1	Německo
a	a	k8xC	a
že	že	k8xS	že
nepracujeme	pracovat	k5eNaImIp1nP	pracovat
tak	tak	k6eAd1	tak
tvrdě	tvrdě	k6eAd1	tvrdě
jako	jako	k8xC	jako
lidé	člověk	k1gMnPc1	člověk
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
Klaus	Klaus	k1gMnSc1	Klaus
za	za	k7c4	za
ČR	ČR	kA	ČR
podepsat	podepsat	k5eAaPmF	podepsat
doplněk	doplněk	k1gInSc4	doplněk
Lisabonské	lisabonský	k2eAgFnSc2d1	Lisabonská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
umožnil	umožnit	k5eAaPmAgInS	umožnit
vznik	vznik	k1gInSc4	vznik
nového	nový	k2eAgInSc2d1	nový
záchranného	záchranný	k2eAgInSc2d1	záchranný
fondu	fond	k1gInSc2	fond
eurozóny	eurozóna	k1gFnSc2	eurozóna
-	-	kIx~	-
Evropského	evropský	k2eAgInSc2d1	evropský
stabilizačního	stabilizační	k2eAgInSc2d1	stabilizační
mechanismu	mechanismus	k1gInSc2	mechanismus
(	(	kIx(	(
<g/>
ESM	ESM	kA	ESM
<g/>
,	,	kIx,	,
též	též	k9	též
Euroval	Euroval	k1gFnSc2	Euroval
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klaus	Klaus	k1gMnSc1	Klaus
tento	tento	k3xDgInSc4	tento
fond	fond	k1gInSc4	fond
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
zrůdnou	zrůdný	k2eAgFnSc4d1	zrůdná
<g/>
,	,	kIx,	,
hroznou	hrozný	k2eAgFnSc4d1	hrozná
věc	věc	k1gFnSc4	věc
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dodatek	dodatek	k1gInSc1	dodatek
nakonec	nakonec	k6eAd1	nakonec
podepsal	podepsat	k5eAaPmAgInS	podepsat
až	až	k9	až
Klausův	Klausův	k2eAgMnSc1d1	Klausův
nástupce	nástupce	k1gMnSc1	nástupce
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
opakovaně	opakovaně	k6eAd1	opakovaně
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
pochybnosti	pochybnost	k1gFnPc4	pochybnost
o	o	k7c6	o
smysluplnosti	smysluplnost	k1gFnSc6	smysluplnost
vojenského	vojenský	k2eAgInSc2d1	vojenský
zásahu	zásah	k1gInSc2	zásah
Severoatlantické	severoatlantický	k2eAgFnSc2d1	Severoatlantická
aliance	aliance	k1gFnSc2	aliance
(	(	kIx(	(
<g/>
NATO	NATO	kA	NATO
<g/>
)	)	kIx)	)
proti	proti	k7c3	proti
Jugoslávii	Jugoslávie	k1gFnSc3	Jugoslávie
vedené	vedený	k2eAgFnSc2d1	vedená
prezidentem	prezident	k1gMnSc7	prezident
Slobodanem	Slobodan	k1gMnSc7	Slobodan
Miloševičem	Miloševič	k1gMnSc7	Miloševič
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Operace	operace	k1gFnSc1	operace
Spojenecká	spojenecký	k2eAgFnSc1d1	spojenecká
síla	síla	k1gFnSc1	síla
<g/>
)	)	kIx)	)
a	a	k8xC	a
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
uznání	uznání	k1gNnSc3	uznání
samostatnosti	samostatnost	k1gFnSc2	samostatnost
státu	stát	k1gInSc2	stát
Kosovo	Kosův	k2eAgNnSc1d1	Kosovo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
hostil	hostit	k5eAaImAgMnS	hostit
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
schůzku	schůzka	k1gFnSc4	schůzka
mezi	mezi	k7c7	mezi
americkým	americký	k2eAgMnSc7d1	americký
prezidentem	prezident	k1gMnSc7	prezident
Barackem	Barack	k1gInSc7	Barack
Obamou	Obama	k1gFnSc7	Obama
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
ruským	ruský	k2eAgInSc7d1	ruský
protějškem	protějšek	k1gInSc7	protějšek
Dmitrijem	Dmitrij	k1gMnSc7	Dmitrij
Medveděvem	Medveděv	k1gInSc7	Medveděv
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
byla	být	k5eAaImAgFnS	být
podepsána	podepsán	k2eAgFnSc1d1	podepsána
smlouva	smlouva	k1gFnSc1	smlouva
START	start	k1gInSc1	start
o	o	k7c6	o
snižování	snižování	k1gNnSc6	snižování
počtu	počet	k1gInSc2	počet
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2011	[number]	k4	2011
se	se	k3xPyFc4	se
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
zastal	zastat	k5eAaPmAgMnS	zastat
svého	svůj	k3xOyFgMnSc4	svůj
mluvčího	mluvčí	k1gMnSc4	mluvčí
Petra	Petr	k1gMnSc4	Petr
Hájka	Hájek	k1gMnSc4	Hájek
a	a	k8xC	a
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
svůj	svůj	k3xOyFgInSc4	svůj
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
homosexualismu	homosexualismus	k1gInSc6	homosexualismus
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
Hájek	Hájek	k1gMnSc1	Hájek
označil	označit	k5eAaPmAgMnS	označit
homosexualitu	homosexualita	k1gFnSc4	homosexualita
za	za	k7c4	za
deviaci	deviace	k1gFnSc4	deviace
a	a	k8xC	a
odsoudil	odsoudit	k5eAaPmAgMnS	odsoudit
průvod	průvod	k1gInSc4	průvod
gay	gay	k1gMnSc1	gay
hrdosti	hrdost	k1gFnSc2	hrdost
Prague	Pragu	k1gFnSc2	Pragu
Pride	Prid	k1gInSc5	Prid
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Klaus	Klaus	k1gMnSc1	Klaus
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pražská	pražský	k2eAgFnSc1d1	Pražská
"	"	kIx"	"
<g/>
Prague	Praguus	k1gMnSc5	Praguus
Pride	Prid	k1gMnSc5	Prid
<g/>
"	"	kIx"	"
není	být	k5eNaImIp3nS	být
projevem	projev	k1gInSc7	projev
homosexuality	homosexualita	k1gFnSc2	homosexualita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
homosexualismu	homosexualismus	k1gInSc3	homosexualismus
<g/>
,	,	kIx,	,
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
-	-	kIx~	-
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
řady	řada	k1gFnSc2	řada
dalších	další	k2eAgInPc2d1	další
módních	módní	k2eAgInPc2d1	módní
"	"	kIx"	"
<g/>
ismů	ismus	k1gInPc2	ismus
<g/>
"	"	kIx"	"
-	-	kIx~	-
velmi	velmi	k6eAd1	velmi
obávám	obávat	k5eAaImIp1nS	obávat
<g/>
.	.	kIx.	.
</s>
<s>
Přesně	přesně	k6eAd1	přesně
toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
ostatně	ostatně	k6eAd1	ostatně
postoj	postoj	k1gInSc4	postoj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
jsem	být	k5eAaImIp1nS	být
veřejně	veřejně	k6eAd1	veřejně
zastával	zastávat	k5eAaImAgMnS	zastávat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
při	při	k7c6	při
svém	svůj	k3xOyFgNnSc6	svůj
vetu	veto	k1gNnSc6	veto
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
registrovaném	registrovaný	k2eAgNnSc6d1	registrované
partnerství	partnerství	k1gNnSc6	partnerství
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Dva	dva	k4xCgMnPc1	dva
Klausovi	Klausův	k2eAgMnPc1d1	Klausův
blízcí	blízký	k2eAgMnPc1d1	blízký
spolupracovníci	spolupracovník	k1gMnPc1	spolupracovník
v	v	k7c6	v
prezidentském	prezidentský	k2eAgNnSc6d1	prezidentské
období	období	k1gNnSc6	období
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
ideologičtí	ideologický	k2eAgMnPc1d1	ideologický
zastánci	zastánce	k1gMnPc1	zastánce
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
politického	politický	k2eAgInSc2d1	politický
odboru	odbor	k1gInSc2	odbor
Ladislav	Ladislav	k1gMnSc1	Ladislav
Jakl	Jakl	k1gMnSc1	Jakl
a	a	k8xC	a
zástupce	zástupce	k1gMnSc1	zástupce
vedoucího	vedoucí	k1gMnSc2	vedoucí
kanceláře	kancelář	k1gFnSc2	kancelář
pro	pro	k7c4	pro
oblast	oblast	k1gFnSc4	oblast
komunikace	komunikace	k1gFnSc2	komunikace
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
Petr	Petr	k1gMnSc1	Petr
Hájek	Hájek	k1gMnSc1	Hájek
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
signatáři	signatář	k1gMnPc1	signatář
petice	petice	k1gFnSc2	petice
a	a	k8xC	a
aktivními	aktivní	k2eAgMnPc7d1	aktivní
účastníky	účastník	k1gMnPc7	účastník
radikálně	radikálně	k6eAd1	radikálně
konzervativní	konzervativní	k2eAgFnSc2d1	konzervativní
iniciativy	iniciativa	k1gFnSc2	iniciativa
Akce	akce	k1gFnSc1	akce
D.	D.	kA	D.
<g/>
O.S.	O.S.	k1gFnSc2	O.S.
<g/>
T.	T.	kA	T.
Ta	ten	k3xDgFnSc1	ten
s	s	k7c7	s
Václavem	Václav	k1gMnSc7	Václav
Klausem	Klaus	k1gMnSc7	Klaus
sdílela	sdílet	k5eAaImAgFnS	sdílet
odpor	odpor	k1gInSc4	odpor
k	k	k7c3	k
antidiskriminačnímu	antidiskriminační	k2eAgInSc3d1	antidiskriminační
zákonu	zákon	k1gInSc3	zákon
<g/>
,	,	kIx,	,
multikulturalismu	multikulturalismus	k1gInSc3	multikulturalismus
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
homosexualismu	homosexualismus	k1gInSc3	homosexualismus
<g/>
"	"	kIx"	"
či	či	k8xC	či
Lisabonské	lisabonský	k2eAgFnSc3d1	Lisabonská
smlouvě	smlouva	k1gFnSc3	smlouva
a	a	k8xC	a
její	její	k3xOp3gFnSc4	její
představitelé	představitel	k1gMnPc1	představitel
spojovali	spojovat	k5eAaImAgMnP	spojovat
motivaci	motivace	k1gFnSc4	motivace
k	k	k7c3	k
vlastnímu	vlastní	k2eAgNnSc3d1	vlastní
angažmá	angažmá	k1gNnSc3	angažmá
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
nespokojeností	nespokojenost	k1gFnSc7	nespokojenost
s	s	k7c7	s
vývojem	vývoj	k1gInSc7	vývoj
ODS	ODS	kA	ODS
po	po	k7c6	po
Klausově	Klausův	k2eAgInSc6d1	Klausův
odchodu	odchod	k1gInSc6	odchod
<g/>
.	.	kIx.	.
</s>
<s>
Iniciativa	iniciativa	k1gFnSc1	iniciativa
uspořádala	uspořádat	k5eAaPmAgFnS	uspořádat
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2009	[number]	k4	2009
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
jeho	jeho	k3xOp3gNnPc2	jeho
odmítání	odmítání	k1gNnPc2	odmítání
ratifikace	ratifikace	k1gFnSc2	ratifikace
Lisabonské	lisabonský	k2eAgFnSc2d1	Lisabonská
smlouvy	smlouva	k1gFnSc2	smlouva
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Pochod	pochod	k1gInSc1	pochod
na	na	k7c4	na
Hrad	hrad	k1gInSc4	hrad
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
jí	jíst	k5eAaImIp3nS	jíst
posloužil	posloužit	k5eAaPmAgMnS	posloužit
jako	jako	k9	jako
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
aktivizačních	aktivizační	k2eAgInPc2d1	aktivizační
momentů	moment	k1gInPc2	moment
<g/>
.	.	kIx.	.
</s>
<s>
Klaus	Klaus	k1gMnSc1	Klaus
převzal	převzít	k5eAaPmAgMnS	převzít
petici	petice	k1gFnSc4	petice
a	a	k8xC	a
v	v	k7c6	v
proslovu	proslov	k1gInSc6	proslov
k	k	k7c3	k
účastníkům	účastník	k1gMnPc3	účastník
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Čtu	číst	k5eAaImIp1nS	číst
vaše	váš	k3xOp2gNnPc4	váš
hesla	heslo	k1gNnPc4	heslo
<g/>
,	,	kIx,	,
já	já	k3xPp1nSc1	já
jim	on	k3xPp3gMnPc3	on
rozumím	rozumět	k5eAaImIp1nS	rozumět
<g/>
.	.	kIx.	.
</s>
<s>
Já	já	k3xPp1nSc1	já
to	ten	k3xDgNnSc1	ten
cítím	cítit	k5eAaImIp1nS	cítit
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
vy	vy	k3xPp2nPc1	vy
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k8xS	jako
prezident	prezident	k1gMnSc1	prezident
to	ten	k3xDgNnSc4	ten
říkám	říkat	k5eAaImIp1nS	říkat
možná	možná	k9	možná
slabší	slabý	k2eAgFnSc7d2	slabší
terminologií	terminologie	k1gFnSc7	terminologie
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
prezidenta	prezident	k1gMnSc2	prezident
v	v	k7c6	v
protiakci	protiakce	k1gFnSc6	protiakce
při	při	k7c6	při
demonstraci	demonstrace	k1gFnSc6	demonstrace
Klausových	Klausových	k2eAgMnPc2d1	Klausových
odpůrců	odpůrce	k1gMnPc2	odpůrce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2011	[number]	k4	2011
zamýšlel	zamýšlet	k5eAaImAgMnS	zamýšlet
ministr	ministr	k1gMnSc1	ministr
školství	školství	k1gNnSc2	školství
Josef	Josef	k1gMnSc1	Josef
Dobeš	Dobeš	k1gMnSc1	Dobeš
(	(	kIx(	(
<g/>
VV	VV	kA	VV
<g/>
)	)	kIx)	)
jmenovat	jmenovat	k5eAaBmF	jmenovat
předsedu	předseda	k1gMnSc4	předseda
Akce	akce	k1gFnSc2	akce
D.	D.	kA	D.
<g/>
O.S.	O.S.	k1gFnSc2	O.S.
<g/>
T	T	kA	T
Ladislava	Ladislava	k1gFnSc1	Ladislava
Bátoru	Bátor	k1gInSc2	Bátor
svým	svůj	k3xOyFgMnSc7	svůj
náměstkem	náměstek	k1gMnSc7	náměstek
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
na	na	k7c4	na
doporučení	doporučení	k1gNnSc4	doporučení
"	"	kIx"	"
<g/>
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
prezidenta	prezident	k1gMnSc2	prezident
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Záměr	záměr	k1gInSc1	záměr
však	však	k9	však
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
značnou	značný	k2eAgFnSc4d1	značná
kritiku	kritika	k1gFnSc4	kritika
a	a	k8xC	a
protesty	protest	k1gInPc4	protest
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
s	s	k7c7	s
poukazy	poukaz	k1gInPc7	poukaz
na	na	k7c4	na
Bátorovy	Bátorův	k2eAgInPc4d1	Bátorův
údajně	údajně	k6eAd1	údajně
extremistické	extremistický	k2eAgInPc4d1	extremistický
názory	názor	k1gInPc4	názor
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
vedení	vedení	k1gNnSc4	vedení
kandidátky	kandidátka	k1gFnSc2	kandidátka
nacionalistické	nacionalistický	k2eAgFnSc2d1	nacionalistická
Národní	národní	k2eAgFnSc2d1	národní
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
Bátorovu	Bátorův	k2eAgNnSc3d1	Bátorovo
jmenování	jmenování	k1gNnSc3	jmenování
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
postavil	postavit	k5eAaPmAgMnS	postavit
i	i	k9	i
premiér	premiér	k1gMnSc1	premiér
Petr	Petr	k1gMnSc1	Petr
Nečas	Nečas	k1gMnSc1	Nečas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prezident	prezident	k1gMnSc1	prezident
Klaus	Klaus	k1gMnSc1	Klaus
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
hlavních	hlavní	k2eAgMnPc2d1	hlavní
obhájců	obhájce	k1gMnPc2	obhájce
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc2	on
v	v	k7c6	v
článku	článek	k1gInSc6	článek
pro	pro	k7c4	pro
Právo	právo	k1gNnSc4	právo
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
zastal	zastat	k5eAaPmAgMnS	zastat
<g/>
,	,	kIx,	,
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
se	se	k3xPyFc4	se
k	k	k7c3	k
řadě	řada	k1gFnSc3	řada
jeho	jeho	k3xOp3gInPc2	jeho
názorů	názor	k1gInPc2	názor
a	a	k8xC	a
protesty	protest	k1gInPc4	protest
přirovnal	přirovnat	k5eAaPmAgMnS	přirovnat
k	k	k7c3	k
hilsneriádě	hilsneriáda	k1gFnSc3	hilsneriáda
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
další	další	k2eAgFnPc4d1	další
bouřlivé	bouřlivý	k2eAgFnPc4d1	bouřlivá
reakce	reakce	k1gFnPc4	reakce
některých	některý	k3yIgFnPc2	některý
osobností	osobnost	k1gFnPc2	osobnost
a	a	k8xC	a
sdružení	sdružení	k1gNnPc2	sdružení
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2011	[number]	k4	2011
pak	pak	k6eAd1	pak
prezident	prezident	k1gMnSc1	prezident
přijal	přijmout	k5eAaPmAgMnS	přijmout
Bátoru	Bátora	k1gFnSc4	Bátora
a	a	k8xC	a
další	další	k2eAgMnPc4d1	další
představitele	představitel	k1gMnPc4	představitel
Akce	akce	k1gFnSc2	akce
D.	D.	kA	D.
<g/>
O.S.	O.S.	k1gFnSc2	O.S.
<g/>
T.	T.	kA	T.
na	na	k7c6	na
Hradě	hrad	k1gInSc6	hrad
při	při	k7c6	při
oficiálním	oficiální	k2eAgNnSc6d1	oficiální
setkání	setkání	k1gNnSc6	setkání
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
informovala	informovat	k5eAaBmAgFnS	informovat
o	o	k7c6	o
blízkosti	blízkost	k1gFnSc6	blízkost
některých	některý	k3yIgInPc2	některý
členů	člen	k1gInPc2	člen
ke	k	k7c3	k
krajní	krajní	k2eAgFnSc3d1	krajní
pravici	pravice	k1gFnSc3	pravice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
Klausova	Klausův	k2eAgNnSc2d1	Klausovo
prezidentství	prezidentství	k1gNnSc2	prezidentství
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
iniciativa	iniciativa	k1gFnSc1	iniciativa
postavila	postavit	k5eAaPmAgFnS	postavit
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
stranu	strana	k1gFnSc4	strana
ve	v	k7c6	v
sporech	spor	k1gInPc6	spor
o	o	k7c4	o
prezidentskou	prezidentský	k2eAgFnSc4d1	prezidentská
amnestii	amnestie	k1gFnSc4	amnestie
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
proti	proti	k7c3	proti
"	"	kIx"	"
<g/>
ideovému	ideový	k2eAgInSc3d1	ideový
táboru	tábor	k1gInSc3	tábor
kolem	kolem	k7c2	kolem
Karla	Karel	k1gMnSc4	Karel
Schwarzenberga	Schwarzenberg	k1gMnSc2	Schwarzenberg
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
byl	být	k5eAaImAgMnS	být
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
své	svůj	k3xOyFgFnSc2	svůj
politické	politický	k2eAgFnSc2d1	politická
dráhy	dráha	k1gFnSc2	dráha
kritikem	kritik	k1gMnSc7	kritik
environmentalismu	environmentalismus	k1gInSc2	environmentalismus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
levicovou	levicový	k2eAgFnSc4d1	levicová
ideologii	ideologie	k1gFnSc4	ideologie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
názory	názor	k1gInPc4	názor
obdržel	obdržet	k5eAaPmAgMnS	obdržet
třikrát	třikrát	k6eAd1	třikrát
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
a	a	k8xC	a
2007	[number]	k4	2007
<g/>
;	;	kIx,	;
výrok	výrok	k1gInSc1	výrok
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
netýkal	týkat	k5eNaImAgInS	týkat
ekologie	ekologie	k1gFnSc2	ekologie
<g/>
)	)	kIx)	)
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
"	"	kIx"	"
<g/>
antiekologický	antiekologický	k2eAgInSc1d1	antiekologický
výrok	výrok	k1gInSc1	výrok
roku	rok	k1gInSc2	rok
<g/>
"	"	kIx"	"
cenu	cena	k1gFnSc4	cena
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
"	"	kIx"	"
<g/>
Zelená	zelený	k2eAgFnSc1d1	zelená
perla	perla	k1gFnSc1	perla
roku	rok	k1gInSc2	rok
<g/>
"	"	kIx"	"
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
za	za	k7c4	za
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
dostavbě	dostavba	k1gFnSc3	dostavba
Temelína	Temelín	k1gInSc2	Temelín
také	také	k9	také
název	název	k1gInSc4	název
Ropák	ropák	k1gMnSc1	ropák
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Environmentalismus	Environmentalismus	k1gInSc1	Environmentalismus
označuje	označovat	k5eAaImIp3nS	označovat
za	za	k7c4	za
současné	současný	k2eAgNnSc4d1	současné
největší	veliký	k2eAgNnSc4d3	veliký
ohrožení	ohrožení	k1gNnSc4	ohrožení
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
odpovědi	odpověď	k1gFnPc4	odpověď
na	na	k7c4	na
otázky	otázka	k1gFnPc4	otázka
amerického	americký	k2eAgInSc2d1	americký
Kongresu	kongres	k1gInSc2	kongres
uvedl	uvést	k5eAaPmAgInS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Komunismus	komunismus	k1gInSc1	komunismus
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
ambiciózního	ambiciózní	k2eAgInSc2d1	ambiciózní
environmentalismu	environmentalismus	k1gInSc2	environmentalismus
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
ideologie	ideologie	k1gFnSc1	ideologie
vzývá	vzývat	k5eAaImIp3nS	vzývat
zemi	zem	k1gFnSc4	zem
a	a	k8xC	a
přírodu	příroda	k1gFnSc4	příroda
a	a	k8xC	a
pod	pod	k7c7	pod
hesly	heslo	k1gNnPc7	heslo
o	o	k7c6	o
jejich	jejich	k3xOp3gFnSc6	jejich
ochraně	ochrana	k1gFnSc6	ochrana
-	-	kIx~	-
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
staří	starý	k2eAgMnPc1d1	starý
marxisté	marxista	k1gMnPc1	marxista
-	-	kIx~	-
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
nahradit	nahradit	k5eAaPmF	nahradit
svobodný	svobodný	k2eAgInSc1d1	svobodný
spontánní	spontánní	k2eAgInSc1d1	spontánní
vývoj	vývoj	k1gInSc1	vývoj
lidstva	lidstvo	k1gNnSc2	lidstvo
jakýmsi	jakýsi	k3yIgNnSc7	jakýsi
centrálním	centrální	k2eAgMnSc7d1	centrální
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
globálním	globální	k2eAgMnSc7d1	globální
<g/>
)	)	kIx)	)
plánováním	plánování	k1gNnSc7	plánování
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
například	například	k6eAd1	například
<g/>
,	,	kIx,	,
že	že	k8xS	že
ekologičtí	ekologický	k2eAgMnPc1d1	ekologický
aktivisté	aktivista	k1gMnPc1	aktivista
představují	představovat	k5eAaImIp3nP	představovat
větší	veliký	k2eAgFnSc4d2	veliký
globální	globální	k2eAgFnSc4d1	globální
hrozbu	hrozba	k1gFnSc4	hrozba
než	než	k8xS	než
teroristé	terorista	k1gMnPc1	terorista
z	z	k7c2	z
Al-Káidy	Al-Káida	k1gFnSc2	Al-Káida
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
HN	HN	kA	HN
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
environmentalismus	environmentalismus	k1gInSc1	environmentalismus
je	být	k5eAaImIp3nS	být
inkarnace	inkarnace	k1gFnPc4	inkarnace
novodobého	novodobý	k2eAgNnSc2d1	novodobé
levičáctví	levičáctví	k1gNnSc2	levičáctví
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
názor	názor	k1gInSc1	názor
zopakoval	zopakovat	k5eAaPmAgInS	zopakovat
v	v	k7c6	v
diskuzi	diskuze	k1gFnSc6	diskuze
se	s	k7c7	s
čtenáři	čtenář	k1gMnPc7	čtenář
Financial	Financial	k1gMnSc1	Financial
Times	Times	k1gMnSc1	Times
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Environmentalismus	Environmentalismus	k1gInSc1	Environmentalismus
-	-	kIx~	-
nikoli	nikoli	k9	nikoli
ochrana	ochrana	k1gFnSc1	ochrana
přírody	příroda	k1gFnSc2	příroda
(	(	kIx(	(
<g/>
a	a	k8xC	a
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
)	)	kIx)	)
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
levicová	levicový	k2eAgFnSc1d1	levicová
ideologie	ideologie	k1gFnSc1	ideologie
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
hlasitým	hlasitý	k2eAgMnPc3d1	hlasitý
kritikům	kritik	k1gMnPc3	kritik
názoru	názor	k1gInSc2	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
globální	globální	k2eAgNnSc4d1	globální
oteplování	oteplování	k1gNnSc4	oteplování
je	být	k5eAaImIp3nS	být
způsobeno	způsoben	k2eAgNnSc1d1	způsobeno
člověkem	člověk	k1gMnSc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
názory	názor	k1gInPc4	názor
na	na	k7c4	na
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
globálnímu	globální	k2eAgNnSc3d1	globální
oteplování	oteplování	k1gNnSc3	oteplování
shrnul	shrnout	k5eAaPmAgMnS	shrnout
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Modrá	modrat	k5eAaImIp3nS	modrat
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
zelená	zelený	k2eAgFnSc1d1	zelená
planeta	planeta	k1gFnSc1	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Existenci	existence	k1gFnSc4	existence
globálních	globální	k2eAgFnPc2d1	globální
změn	změna	k1gFnPc2	změna
klimatu	klima	k1gNnSc2	klima
nezpochybňuje	zpochybňovat	k5eNaImIp3nS	zpochybňovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
hlasitým	hlasitý	k2eAgMnSc7d1	hlasitý
kritikem	kritik	k1gMnSc7	kritik
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
nim	on	k3xPp3gMnPc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Globální	globální	k2eAgFnPc1d1	globální
změny	změna	k1gFnPc1	změna
klimatu	klima	k1gNnSc2	klima
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gInSc2	jeho
názoru	názor	k1gInSc2	názor
přirozeného	přirozený	k2eAgInSc2d1	přirozený
původu	původ	k1gInSc2	původ
a	a	k8xC	a
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
nim	on	k3xPp3gInPc3	on
tak	tak	k9	tak
bezvýznamný	bezvýznamný	k2eAgMnSc1d1	bezvýznamný
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
argumentuje	argumentovat	k5eAaImIp3nS	argumentovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
oteplování	oteplování	k1gNnSc1	oteplování
nemusí	muset	k5eNaImIp3nS	muset
mít	mít	k5eAaImF	mít
jen	jen	k9	jen
negativní	negativní	k2eAgInPc4d1	negativní
dopady	dopad	k1gInPc4	dopad
<g/>
.	.	kIx.	.
</s>
<s>
Řešení	řešení	k1gNnSc1	řešení
problému	problém	k1gInSc2	problém
spatřuje	spatřovat	k5eAaImIp3nS	spatřovat
v	v	k7c6	v
adaptaci	adaptace	k1gFnSc6	adaptace
a	a	k8xC	a
technologickém	technologický	k2eAgInSc6d1	technologický
pokroku	pokrok	k1gInSc6	pokrok
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Namísto	namísto	k7c2	namísto
marných	marný	k2eAgInPc2d1	marný
pokusů	pokus	k1gInPc2	pokus
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
bojovat	bojovat	k5eAaImF	bojovat
bychom	by	kYmCp1nP	by
se	se	k3xPyFc4	se
měli	mít	k5eAaImAgMnP	mít
připravit	připravit	k5eAaPmF	připravit
na	na	k7c4	na
jeho	jeho	k3xOp3gInPc4	jeho
důsledky	důsledek	k1gInPc4	důsledek
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
kdyby	kdyby	kYmCp3nS	kdyby
se	se	k3xPyFc4	se
atmosféra	atmosféra	k1gFnSc1	atmosféra
oteplovala	oteplovat	k5eAaImAgFnS	oteplovat
<g/>
,	,	kIx,	,
negativní	negativní	k2eAgInPc4d1	negativní
dopady	dopad	k1gInPc4	dopad
nemusí	muset	k5eNaImIp3nS	muset
převážit	převážit	k5eAaPmF	převážit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Rovněž	rovněž	k9	rovněž
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
panel	panel	k1gInSc1	panel
IPCC	IPCC	kA	IPCC
jako	jako	k8xS	jako
zpolitizovaný	zpolitizovaný	k2eAgInSc1d1	zpolitizovaný
a	a	k8xC	a
jednostranně	jednostranně	k6eAd1	jednostranně
zaměřený	zaměřený	k2eAgInSc1d1	zaměřený
<g/>
.	.	kIx.	.
</s>
<s>
Staví	stavit	k5eAaPmIp3nS	stavit
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
termínu	termín	k1gInSc3	termín
vědecký	vědecký	k2eAgInSc1d1	vědecký
konsenzus	konsenzus	k1gInSc1	konsenzus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
něho	on	k3xPp3gMnSc2	on
"	"	kIx"	"
<g/>
vždy	vždy	k6eAd1	vždy
dosahován	dosahovat	k5eAaImNgInS	dosahovat
hlasitou	hlasitý	k2eAgFnSc7d1	hlasitá
menšinou	menšina	k1gFnSc7	menšina
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
tichou	tichý	k2eAgFnSc7d1	tichá
většinou	většina	k1gFnSc7	většina
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
odchodem	odchod	k1gInSc7	odchod
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
prezidenta	prezident	k1gMnSc2	prezident
zhodnotil	zhodnotit	k5eAaPmAgMnS	zhodnotit
americký	americký	k2eAgMnSc1d1	americký
novinář	novinář	k1gMnSc1	novinář
James	James	k1gMnSc1	James
Kirchick	Kirchick	k1gMnSc1	Kirchick
pro	pro	k7c4	pro
anglickou	anglický	k2eAgFnSc4d1	anglická
edici	edice	k1gFnSc4	edice
německého	německý	k2eAgInSc2d1	německý
časopisu	časopis	k1gInSc2	časopis
Der	drát	k5eAaImRp2nS	drát
Spiegel	Spiegel	k1gMnSc1	Spiegel
prezidentské	prezidentský	k2eAgNnSc4d1	prezidentské
období	období	k1gNnSc4	období
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jeho	on	k3xPp3gInSc2	on
názoru	názor	k1gInSc2	názor
Klaus	Klaus	k1gMnSc1	Klaus
měl	mít	k5eAaImAgMnS	mít
určitě	určitě	k6eAd1	určitě
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
"	"	kIx"	"
<g/>
jeho	on	k3xPp3gNnSc2	on
dědictví	dědictví	k1gNnSc2	dědictví
bude	být	k5eAaImBp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
poškozeno	poškodit	k5eAaPmNgNnS	poškodit
kontroverzními	kontroverzní	k2eAgInPc7d1	kontroverzní
postoji	postoj	k1gInPc7	postoj
k	k	k7c3	k
Evropské	evropský	k2eAgFnSc3d1	Evropská
unii	unie	k1gFnSc3	unie
<g/>
,	,	kIx,	,
klimatické	klimatický	k2eAgFnSc6d1	klimatická
změně	změna	k1gFnSc6	změna
a	a	k8xC	a
populismem	populismus	k1gInSc7	populismus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kirchick	Kirchick	k6eAd1	Kirchick
si	se	k3xPyFc3	se
všímá	všímat	k5eAaImIp3nS	všímat
Klausovy	Klausův	k2eAgFnPc4d1	Klausova
záliby	záliba	k1gFnPc4	záliba
v	v	k7c6	v
-ismech	sm	k1gInPc6	-ism
a	a	k8xC	a
zvláště	zvláště	k6eAd1	zvláště
potom	potom	k8xC	potom
jeho	on	k3xPp3gNnSc2	on
posledního	poslední	k2eAgNnSc2d1	poslední
užití	užití	k1gNnSc2	užití
pojmu	pojmout	k5eAaPmIp1nS	pojmout
havlismus	havlismus	k1gInSc1	havlismus
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
polský	polský	k2eAgInSc4d1	polský
týdeník	týdeník	k1gInSc4	týdeník
Rzeczy	Rzecza	k1gFnSc2	Rzecza
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
styl	styl	k1gInSc1	styl
považuje	považovat	k5eAaImIp3nS	považovat
u	u	k7c2	u
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
za	za	k7c4	za
rutinu	rutina	k1gFnSc4	rutina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
zlomyslná	zlomyslný	k2eAgFnSc1d1	zlomyslná
<g/>
,	,	kIx,	,
hysterická	hysterický	k2eAgFnSc1d1	hysterická
a	a	k8xC	a
přezíravá	přezíravý	k2eAgFnSc1d1	přezíravá
<g/>
...	...	k?	...
k	k	k7c3	k
V.	V.	kA	V.
Havlovi	Havlův	k2eAgMnPc1d1	Havlův
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Kirchicka	Kirchicko	k1gNnSc2	Kirchicko
byla	být	k5eAaImAgFnS	být
prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
dekáda	dekáda	k1gFnSc1	dekáda
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
charakteristická	charakteristický	k2eAgNnPc4d1	charakteristické
častým	častý	k2eAgNnSc7d1	časté
testováním	testování	k1gNnSc7	testování
ústavních	ústavní	k2eAgNnPc2d1	ústavní
omezení	omezení	k1gNnPc2	omezení
funkce	funkce	k1gFnSc2	funkce
a	a	k8xC	a
kontroverzemi	kontroverze	k1gFnPc7	kontroverze
v	v	k7c6	v
záležitostech	záležitost	k1gFnPc6	záležitost
počínaje	počínaje	k7c7	počínaje
homosexuály	homosexuál	k1gMnPc7	homosexuál
a	a	k8xC	a
konče	konče	k7c7	konče
globálním	globální	k2eAgNnSc7d1	globální
oteplením	oteplení	k1gNnSc7	oteplení
<g/>
.	.	kIx.	.
</s>
<s>
Klaus	Klaus	k1gMnSc1	Klaus
za	za	k7c7	za
sebou	se	k3xPyFc7	se
jako	jako	k8xC	jako
nejvýznamnější	významný	k2eAgMnSc1d3	nejvýznamnější
politik	politik	k1gMnSc1	politik
polistopadového	polistopadový	k2eAgNnSc2d1	polistopadové
období	období	k1gNnSc2	období
po	po	k7c6	po
V.	V.	kA	V.
Havlovi	Havlův	k2eAgMnPc1d1	Havlův
ponechává	ponechávat	k5eAaImIp3nS	ponechávat
"	"	kIx"	"
<g/>
diskutabilní	diskutabilní	k2eAgNnSc1d1	diskutabilní
dědictví	dědictví	k1gNnSc1	dědictví
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
míní	mínit	k5eAaImIp3nS	mínit
Kirchick	Kirchick	k1gMnSc1	Kirchick
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
si	se	k3xPyFc3	se
všímá	všímat	k5eAaImIp3nS	všímat
také	také	k9	také
Klausova	Klausův	k2eAgInSc2d1	Klausův
opožděného	opožděný	k2eAgInSc2d1	opožděný
podpisu	podpis	k1gInSc2	podpis
Lisabonské	lisabonský	k2eAgFnSc2d1	Lisabonská
smlouvy	smlouva	k1gFnSc2	smlouva
a	a	k8xC	a
kritiky	kritika	k1gFnSc2	kritika
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
připomíná	připomínat	k5eAaImIp3nS	připomínat
roli	role	k1gFnSc4	role
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
podporu	podpora	k1gFnSc4	podpora
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
podle	podle	k7c2	podle
Kirchickovy	Kirchickův	k2eAgFnSc2d1	Kirchickův
analýzy	analýza	k1gFnSc2	analýza
projevem	projev	k1gInSc7	projev
Klausových	Klausových	k2eAgInPc2d1	Klausových
sklonů	sklon	k1gInPc2	sklon
k	k	k7c3	k
populismu	populismus	k1gInSc3	populismus
<g/>
.	.	kIx.	.
</s>
<s>
Podpora	podpora	k1gFnSc1	podpora
Zemana	Zeman	k1gMnSc2	Zeman
proti	proti	k7c3	proti
Karlu	Karel	k1gMnSc3	Karel
Schwarzenbergovi	Schwarzenberg	k1gMnSc3	Schwarzenberg
odpovídala	odpovídat	k5eAaImAgFnS	odpovídat
podle	podle	k7c2	podle
Kirchicka	Kirchicko	k1gNnSc2	Kirchicko
Klausově	Klausův	k2eAgFnSc6d1	Klausova
osobnímu	osobní	k2eAgInSc3d1	osobní
politickému	politický	k2eAgInSc3d1	politický
stylu	styl	k1gInSc3	styl
<g/>
.	.	kIx.	.
</s>
<s>
Schwarzenberg	Schwarzenberg	k1gMnSc1	Schwarzenberg
představoval	představovat	k5eAaImAgMnS	představovat
v	v	k7c6	v
protikladu	protiklad	k1gInSc6	protiklad
ke	k	k7c3	k
Klausovi	Klaus	k1gMnSc3	Klaus
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
a	a	k8xC	a
kosmopolitní	kosmopolitní	k2eAgInSc4d1	kosmopolitní
názor	názor	k1gInSc4	názor
na	na	k7c4	na
politiku	politika	k1gFnSc4	politika
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
především	především	k9	především
přítelem	přítel	k1gMnSc7	přítel
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Kirchick	Kirchick	k6eAd1	Kirchick
si	se	k3xPyFc3	se
dále	daleko	k6eAd2	daleko
všiml	všimnout	k5eAaPmAgMnS	všimnout
pragmatického	pragmatický	k2eAgInSc2d1	pragmatický
vztahu	vztah	k1gInSc2	vztah
Václava	Václav	k1gMnSc4	Václav
Klause	Klaus	k1gMnSc4	Klaus
k	k	k7c3	k
Rusku	Rusko	k1gNnSc3	Rusko
a	a	k8xC	a
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Za	za	k7c2	za
jeho	jeho	k3xOp3gNnPc2	jeho
25	[number]	k4	25
let	léto	k1gNnPc2	léto
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
je	být	k5eAaImIp3nS	být
těžké	těžký	k2eAgNnSc1d1	těžké
najít	najít	k5eAaPmF	najít
jediný	jediný	k2eAgInSc4d1	jediný
geopolitický	geopolitický	k2eAgInSc4d1	geopolitický
problém	problém	k1gInSc4	problém
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yIgInSc4	který
by	by	kYmCp3nP	by
měl	mít	k5eAaImAgMnS	mít
Klaus	Klaus	k1gMnSc1	Klaus
jiný	jiný	k2eAgInSc1d1	jiný
názor	názor	k1gInSc1	názor
než	než	k8xS	než
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
svého	svůj	k3xOyFgInSc2	svůj
článku	článek	k1gInSc2	článek
Kirchick	Kirchick	k1gMnSc1	Kirchick
připomněl	připomnět	k5eAaPmAgMnS	připomnět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Klaus	Klaus	k1gMnSc1	Klaus
byl	být	k5eAaImAgMnS	být
i	i	k9	i
přes	přes	k7c4	přes
své	svůj	k3xOyFgInPc4	svůj
kontroverzní	kontroverzní	k2eAgInPc4d1	kontroverzní
výroky	výrok	k1gInPc4	výrok
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
populární	populární	k2eAgMnSc1d1	populární
a	a	k8xC	a
důvěryhodný	důvěryhodný	k2eAgMnSc1d1	důvěryhodný
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
se	se	k3xPyFc4	se
pozastavuje	pozastavovat	k5eAaImIp3nS	pozastavovat
nad	nad	k7c7	nad
jeho	jeho	k3xOp3gNnSc7	jeho
"	"	kIx"	"
<g/>
překvapivou	překvapivý	k2eAgFnSc7d1	překvapivá
<g/>
"	"	kIx"	"
amnestií	amnestie	k1gFnSc7	amnestie
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
"	"	kIx"	"
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nejtrvalejším	trvalý	k2eAgInSc7d3	nejtrvalejší
aspektem	aspekt	k1gInSc7	aspekt
Klausova	Klausův	k2eAgNnSc2d1	Klausovo
dědictví	dědictví	k1gNnSc2	dědictví
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
názorových	názorový	k2eAgMnPc2d1	názorový
oponentů	oponent	k1gMnPc2	oponent
i	i	k8xC	i
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
např.	např.	kA	např.
europoslance	europoslanec	k1gMnSc2	europoslanec
Othmara	Othmar	k1gMnSc2	Othmar
Karase	Karas	k1gMnSc2	Karas
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
kritizovány	kritizovat	k5eAaImNgFnP	kritizovat
i	i	k9	i
Klausovy	Klausův	k2eAgFnPc1d1	Klausova
osobní	osobní	k2eAgFnPc1d1	osobní
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
jeho	jeho	k3xOp3gFnSc1	jeho
údajná	údajný	k2eAgFnSc1d1	údajná
politická	politický	k2eAgFnSc1d1	politická
nevypočitatelnost	nevypočitatelnost	k1gFnSc1	nevypočitatelnost
<g/>
,	,	kIx,	,
ješitnost	ješitnost	k1gFnSc1	ješitnost
a	a	k8xC	a
sebestřednost	sebestřednost	k1gFnSc1	sebestřednost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Vladimíry	Vladimíra	k1gFnSc2	Vladimíra
Dvořákové	Dvořáková	k1gFnSc2	Dvořáková
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
popularitu	popularita	k1gFnSc4	popularita
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
snižují	snižovat	k5eAaImIp3nP	snižovat
také	také	k9	také
názory	názor	k1gInPc4	názor
a	a	k8xC	a
vystupování	vystupování	k1gNnSc4	vystupování
jeho	jeho	k3xOp3gMnSc2	jeho
poradce	poradce	k1gMnSc2	poradce
Ladislava	Ladislav	k1gMnSc2	Ladislav
Jakla	Jakl	k1gMnSc2	Jakl
a	a	k8xC	a
ředitele	ředitel	k1gMnSc2	ředitel
tiskového	tiskový	k2eAgInSc2d1	tiskový
odboru	odbor	k1gInSc2	odbor
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
kanceláře	kancelář	k1gFnSc2	kancelář
Hájka	hájka	k1gFnSc1	hájka
<g/>
.	.	kIx.	.
</s>
<s>
Britský	britský	k2eAgInSc1d1	britský
týdeník	týdeník	k1gInSc1	týdeník
The	The	k1gMnSc1	The
Economist	Economist	k1gMnSc1	Economist
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2011	[number]	k4	2011
zařadil	zařadit	k5eAaPmAgMnS	zařadit
Václava	Václav	k1gMnSc4	Václav
Klause	Klaus	k1gMnSc4	Klaus
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
způsobem	způsob	k1gInSc7	způsob
prezentace	prezentace	k1gFnPc4	prezentace
jeho	jeho	k3xOp3gInPc2	jeho
názorů	názor	k1gInPc2	názor
mezi	mezi	k7c4	mezi
kontroverzní	kontroverzní	k2eAgMnPc4d1	kontroverzní
východoevropské	východoevropský	k2eAgMnPc4d1	východoevropský
politiky	politik	k1gMnPc4	politik
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vyhledávají	vyhledávat	k5eAaImIp3nP	vyhledávat
špatnou	špatný	k2eAgFnSc4d1	špatná
publicitu	publicita	k1gFnSc4	publicita
a	a	k8xC	a
údajně	údajně	k6eAd1	údajně
tím	ten	k3xDgNnSc7	ten
škodí	škodit	k5eAaImIp3nP	škodit
vlastním	vlastní	k2eAgFnPc3d1	vlastní
zemím	zem	k1gFnPc3	zem
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
investice	investice	k1gFnPc4	investice
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
týdeníku	týdeník	k1gInSc2	týdeník
má	mít	k5eAaImIp3nS	mít
tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
sklon	sklon	k1gInSc4	sklon
opovrhovat	opovrhovat	k5eAaImF	opovrhovat
konvenční	konvenční	k2eAgFnSc7d1	konvenční
diplomacií	diplomacie	k1gFnSc7	diplomacie
a	a	k8xC	a
publicitou	publicita	k1gFnSc7	publicita
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Například	například	k6eAd1	například
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
obvykle	obvykle	k6eAd1	obvykle
odmítá	odmítat	k5eAaImIp3nS	odmítat
hovořit	hovořit	k5eAaImF	hovořit
se	s	k7c7	s
zahraničními	zahraniční	k2eAgMnPc7d1	zahraniční
novináři	novinář	k1gMnPc7	novinář
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
mu	on	k3xPp3gMnSc3	on
neslíbí	slíbit	k5eNaPmIp3nS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
otisknou	otisknout	k5eAaPmIp3nP	otisknout
celé	celý	k2eAgFnPc1d1	celá
jeho	jeho	k3xOp3gFnPc1	jeho
odpovědi	odpověď	k1gFnPc1	odpověď
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
citoval	citovat	k5eAaBmAgMnS	citovat
The	The	k1gMnSc1	The
Economist	Economist	k1gMnSc1	Economist
server	server	k1gInSc4	server
iHNed	ihned	k6eAd1	ihned
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
<s>
Uměleckou	umělecký	k2eAgFnSc7d1	umělecká
formou	forma	k1gFnSc7	forma
svůj	svůj	k3xOyFgInSc4	svůj
názor	názor	k1gInSc4	názor
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
Klause	Klaus	k1gMnSc4	Klaus
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
Michal	Michal	k1gMnSc1	Michal
Viewegh	Viewegh	k1gMnSc1	Viewegh
v	v	k7c6	v
knihách	kniha	k1gFnPc6	kniha
Báječná	báječný	k2eAgNnPc1d1	báječné
léta	léto	k1gNnPc1	léto
s	s	k7c7	s
Klausem	Klaus	k1gMnSc7	Klaus
a	a	k8xC	a
Mráz	mráz	k1gInSc4	mráz
přichází	přicházet	k5eAaImIp3nS	přicházet
z	z	k7c2	z
Hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2013	[number]	k4	2013
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
amerického	americký	k2eAgInSc2d1	americký
libertariánského	libertariánský	k2eAgInSc2d1	libertariánský
think	think	k1gMnSc1	think
tanku	tank	k1gInSc2	tank
Cato	Cato	k1gMnSc1	Cato
Institute	institut	k1gInSc5	institut
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
institut	institut	k1gInSc1	institut
spolupráci	spolupráce	k1gFnSc3	spolupráce
ukončil	ukončit	k5eAaPmAgInS	ukončit
kvůli	kvůli	k7c3	kvůli
Klausovým	Klausův	k2eAgInPc3d1	Klausův
názorům	názor	k1gInPc3	názor
na	na	k7c4	na
válku	válka	k1gFnSc4	válka
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
,	,	kIx,	,
a	a	k8xC	a
postoji	postoj	k1gInPc7	postoj
k	k	k7c3	k
sexuálním	sexuální	k2eAgFnPc3d1	sexuální
menšinám	menšina	k1gFnPc3	menšina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předčasných	předčasný	k2eAgFnPc6d1	předčasná
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
výslovně	výslovně	k6eAd1	výslovně
podpořil	podpořit	k5eAaPmAgMnS	podpořit
stranu	strana	k1gFnSc4	strana
Jany	Jana	k1gFnSc2	Jana
Bobošíkové	Bobošíková	k1gFnSc2	Bobošíková
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
soustředit	soustředit	k5eAaPmF	soustředit
"	"	kIx"	"
<g/>
klausovské	klausovský	k2eAgFnPc4d1	klausovská
síly	síla	k1gFnPc4	síla
<g/>
"	"	kIx"	"
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
Hlavu	hlava	k1gFnSc4	hlava
vzhůru	vzhůru	k6eAd1	vzhůru
-	-	kIx~	-
volební	volební	k2eAgInSc1d1	volební
blok	blok	k1gInSc1	blok
<g/>
.	.	kIx.	.
</s>
<s>
Osobně	osobně	k6eAd1	osobně
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
na	na	k7c6	na
několika	několik	k4yIc6	několik
mítincích	mítink	k1gInPc6	mítink
a	a	k8xC	a
objevil	objevit	k5eAaPmAgMnS	objevit
se	se	k3xPyFc4	se
i	i	k9	i
na	na	k7c6	na
předvolebních	předvolební	k2eAgInPc6d1	předvolební
billboardech	billboard	k1gInPc6	billboard
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
pak	pak	k6eAd1	pak
získala	získat	k5eAaPmAgFnS	získat
celkem	celkem	k6eAd1	celkem
21	[number]	k4	21
241	[number]	k4	241
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
činilo	činit	k5eAaImAgNnS	činit
0,42	[number]	k4	0,42
%	%	kIx~	%
<g/>
,	,	kIx,	,
a	a	k8xC	a
nezískala	získat	k5eNaPmAgFnS	získat
tak	tak	k8xS	tak
žádný	žádný	k3yNgInSc4	žádný
mandát	mandát	k1gInSc4	mandát
<g/>
.	.	kIx.	.
</s>
<s>
Klaus	Klaus	k1gMnSc1	Klaus
za	za	k7c2	za
hlavního	hlavní	k2eAgMnSc2d1	hlavní
viníka	viník	k1gMnSc2	viník
neúspěchu	neúspěch	k1gInSc2	neúspěch
označil	označit	k5eAaPmAgInS	označit
Stranu	strana	k1gFnSc4	strana
svobodných	svobodný	k2eAgMnPc2d1	svobodný
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
projektu	projekt	k1gInSc2	projekt
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
(	(	kIx(	(
<g/>
sama	sám	k3xTgFnSc1	sám
získala	získat	k5eAaPmAgFnS	získat
122	[number]	k4	122
564	[number]	k4	564
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
2,46	[number]	k4	2,46
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
nevyloučil	vyloučit	k5eNaPmAgMnS	vyloučit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
volby	volba	k1gFnSc2	volba
prezidenta	prezident	k1gMnSc2	prezident
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
post	post	k1gInSc4	post
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
kandidovat	kandidovat	k5eAaImF	kandidovat
kněz	kněz	k1gMnSc1	kněz
Tomáš	Tomáš	k1gMnSc1	Tomáš
Halík	Halík	k1gMnSc1	Halík
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
má	mít	k5eAaImIp3nS	mít
Klaus	Klaus	k1gMnSc1	Klaus
dlouhodobé	dlouhodobý	k2eAgInPc4d1	dlouhodobý
spory	spor	k1gInPc4	spor
<g/>
.	.	kIx.	.
</s>
<s>
Ministr	ministr	k1gMnSc1	ministr
školství	školství	k1gNnSc2	školství
Marcel	Marcel	k1gMnSc1	Marcel
Chládek	Chládek	k1gMnSc1	Chládek
(	(	kIx(	(
<g/>
ČSSD	ČSSD	kA	ČSSD
<g/>
)	)	kIx)	)
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2014	[number]	k4	2014
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
zasednout	zasednout	k5eAaPmF	zasednout
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
Národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
pro	pro	k7c4	pro
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
vznik	vznik	k1gInSc1	vznik
byl	být	k5eAaImAgInS	být
naplánován	naplánovat	k5eAaBmNgInS	naplánovat
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
novelou	novela	k1gFnSc7	novela
školského	školský	k2eAgInSc2d1	školský
zákona	zákon	k1gInSc2	zákon
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
ministra	ministr	k1gMnSc4	ministr
vyzvali	vyzvat	k5eAaPmAgMnP	vyzvat
představitelé	představitel	k1gMnPc1	představitel
protikorupčních	protikorupční	k2eAgFnPc2d1	protikorupční
iniciativ	iniciativa	k1gFnPc2	iniciativa
Vraťte	vrátit	k5eAaPmRp2nP	vrátit
nám	my	k3xPp1nPc3	my
stát	stát	k5eAaPmF	stát
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
Veřejnost	veřejnost	k1gFnSc1	veřejnost
proti	proti	k7c3	proti
korupci	korupce	k1gFnSc3	korupce
a	a	k8xC	a
Nadační	nadační	k2eAgInSc1d1	nadační
fond	fond	k1gInSc1	fond
proti	proti	k7c3	proti
korupci	korupce	k1gFnSc3	korupce
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gMnSc4	on
nejmenoval	jmenovat	k5eNaImAgInS	jmenovat
předsedou	předseda	k1gMnSc7	předseda
ani	ani	k8xC	ani
řadovým	řadový	k2eAgMnSc7d1	řadový
členem	člen	k1gMnSc7	člen
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
s	s	k7c7	s
poukazem	poukaz	k1gInSc7	poukaz
-	-	kIx~	-
krom	krom	k7c2	krom
jiného	jiné	k1gNnSc2	jiné
-	-	kIx~	-
na	na	k7c4	na
"	"	kIx"	"
<g/>
politické	politický	k2eAgInPc4d1	politický
a	a	k8xC	a
obchodní	obchodní	k2eAgInPc4d1	obchodní
zájmy	zájem	k1gInPc4	zájem
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
mladšího	mladý	k2eAgMnSc2d2	mladší
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Výběr	výběr	k1gInSc4	výběr
kritizovala	kritizovat	k5eAaImAgFnS	kritizovat
také	také	k9	také
společnost	společnost	k1gFnSc1	společnost
EDUin	EDUina	k1gFnPc2	EDUina
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgFnPc1d1	zabývající
se	s	k7c7	s
vzděláním	vzdělání	k1gNnSc7	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Ministr	ministr	k1gMnSc1	ministr
Chládek	Chládek	k1gMnSc1	Chládek
později	pozdě	k6eAd2	pozdě
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
Klaus	Klaus	k1gMnSc1	Klaus
neměl	mít	k5eNaImAgMnS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
samotné	samotný	k2eAgFnSc6d1	samotná
radě	rada	k1gFnSc6	rada
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
jen	jen	k9	jen
ve	v	k7c6	v
výboru	výbor	k1gInSc6	výbor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
připravit	připravit	k5eAaPmF	připravit
podklady	podklad	k1gInPc4	podklad
k	k	k7c3	k
ukotvení	ukotvení	k1gNnSc3	ukotvení
rady	rada	k1gFnSc2	rada
v	v	k7c6	v
zákoně	zákon	k1gInSc6	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
pak	pak	k9	pak
ministr	ministr	k1gMnSc1	ministr
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
radě	rada	k1gFnSc6	rada
nezasedne	zasednout	k5eNaPmIp3nS	zasednout
žádný	žádný	k3yNgInSc1	žádný
aktivní	aktivní	k2eAgInSc1d1	aktivní
ani	ani	k8xC	ani
bývalý	bývalý	k2eAgMnSc1d1	bývalý
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
že	že	k8xS	že
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
nezasedne	zasednout	k5eNaPmIp3nS	zasednout
ani	ani	k8xC	ani
v	v	k7c6	v
přípravném	přípravný	k2eAgInSc6d1	přípravný
výboru	výbor	k1gInSc6	výbor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2014	[number]	k4	2014
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
válku	válka	k1gFnSc4	válka
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
nesou	nést	k5eAaImIp3nP	nést
spoluzodpovědnost	spoluzodpovědnost	k1gFnSc4	spoluzodpovědnost
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
a	a	k8xC	a
USA	USA	kA	USA
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
podporovaly	podporovat	k5eAaImAgInP	podporovat
protestující	protestující	k2eAgInPc1d1	protestující
během	během	k7c2	během
Ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
krize	krize	k1gFnSc2	krize
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
bylo	být	k5eAaImAgNnS	být
podle	podle	k7c2	podle
Klause	Klaus	k1gMnSc2	Klaus
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
zavlečeno	zavlečen	k2eAgNnSc1d1	zavlečen
a	a	k8xC	a
ruský	ruský	k2eAgMnSc1d1	ruský
prezident	prezident	k1gMnSc1	prezident
Vladimir	Vladimir	k1gMnSc1	Vladimir
Putin	putin	k2eAgMnSc1d1	putin
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
chová	chovat	k5eAaImIp3nS	chovat
racionálně	racionálně	k6eAd1	racionálně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
pokračující	pokračující	k2eAgFnPc4d1	pokračující
devizové	devizový	k2eAgFnPc4d1	devizová
intervence	intervence	k1gFnPc4	intervence
ČNB	ČNB	kA	ČNB
jako	jako	k8xS	jako
neúčinné	účinný	k2eNgFnPc1d1	neúčinná
a	a	k8xC	a
procyklické	procyklický	k2eAgFnPc1d1	procyklická
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
České	český	k2eAgNnSc1d1	české
století	století	k1gNnSc1	století
<g/>
,	,	kIx,	,
mapujícím	mapující	k2eAgMnSc6d1	mapující
české	český	k2eAgFnPc4d1	Česká
dějiny	dějiny	k1gFnPc4	dějiny
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
postava	postava	k1gFnSc1	postava
Klause	Klaus	k1gMnSc2	Klaus
v	v	k7c6	v
dílech	díl	k1gInPc6	díl
věnovaných	věnovaný	k2eAgFnPc2d1	věnovaná
Sametové	sametový	k2eAgFnSc2d1	sametová
revoluci	revoluce	k1gFnSc3	revoluce
a	a	k8xC	a
rozdělení	rozdělení	k1gNnSc3	rozdělení
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Klause	Klaus	k1gMnSc4	Klaus
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
herec	herec	k1gMnSc1	herec
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Plesl	Plesl	k1gInSc4	Plesl
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2015	[number]	k4	2015
se	se	k3xPyFc4	se
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
zařadil	zařadit	k5eAaPmAgMnS	zařadit
mezi	mezi	k7c7	mezi
komentátory	komentátor	k1gMnPc7	komentátor
pořadu	pořad	k1gInSc2	pořad
Jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
vidí	vidět	k5eAaImIp3nS	vidět
na	na	k7c6	na
Dvojce	dvojka	k1gFnSc6	dvojka
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
podporoval	podporovat	k5eAaImAgInS	podporovat
vystoupení	vystoupení	k1gNnSc4	vystoupení
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
z	z	k7c2	z
EU	EU	kA	EU
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Brexit	Brexit	k1gInSc1	Brexit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2016	[number]	k4	2016
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
jako	jako	k9	jako
host	host	k1gMnSc1	host
na	na	k7c6	na
kongresu	kongres	k1gInSc6	kongres
německé	německý	k2eAgFnSc2d1	německá
strany	strana	k1gFnSc2	strana
Alternativa	alternativa	k1gFnSc1	alternativa
pro	pro	k7c4	pro
Německo	Německo	k1gNnSc4	Německo
(	(	kIx(	(
<g/>
AfD	AfD	k1gFnSc1	AfD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
za	za	k7c2	za
jejího	její	k3xOp3gInSc2	její
"	"	kIx"	"
<g/>
skutečného	skutečný	k2eAgMnSc2d1	skutečný
fanouška	fanoušek	k1gMnSc2	fanoušek
<g/>
"	"	kIx"	"
a	a	k8xC	a
pogratuloval	pogratulovat	k5eAaPmAgMnS	pogratulovat
straně	strana	k1gFnSc3	strana
k	k	k7c3	k
úspěchu	úspěch	k1gInSc3	úspěch
v	v	k7c6	v
zemských	zemský	k2eAgFnPc6d1	zemská
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
uvedl	uvést	k5eAaPmAgMnS	uvést
Petr	Petr	k1gMnSc1	Petr
Kellner	Kellner	k1gMnSc1	Kellner
<g/>
,	,	kIx,	,
že	že	k8xS	že
zaplatí	zaplatit	k5eAaPmIp3nS	zaplatit
(	(	kIx(	(
<g/>
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
své	svůj	k3xOyFgFnSc2	svůj
společnosti	společnost	k1gFnSc2	společnost
PPF	PPF	kA	PPF
<g/>
)	)	kIx)	)
desítky	desítka	k1gFnPc1	desítka
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
na	na	k7c4	na
vznik	vznik	k1gInSc4	vznik
a	a	k8xC	a
provoz	provoz	k1gInSc4	provoz
svého	svůj	k3xOyFgInSc2	svůj
institutu	institut	k1gInSc2	institut
s	s	k7c7	s
názvem	název	k1gInSc7	název
Institut	institut	k1gInSc1	institut
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
<g/>
,	,	kIx,	,
o.p.s.	o.p.s.	k?	o.p.s.
Nadační	nadační	k2eAgInSc1d1	nadační
fond	fond	k1gInSc1	fond
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
(	(	kIx(	(
<g/>
NFVK	NFVK	kA	NFVK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
před	před	k7c7	před
změnou	změna	k1gFnSc7	změna
statutu	statut	k1gInSc2	statut
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xS	jako
Nadace	nadace	k1gFnSc1	nadace
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
<g/>
,	,	kIx,	,
založili	založit	k5eAaPmAgMnP	založit
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1991	[number]	k4	1991
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
,	,	kIx,	,
Občanská	občanský	k2eAgFnSc1d1	občanská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
a	a	k8xC	a
společnost	společnost	k1gFnSc1	společnost
Telegraf	telegraf	k1gInSc1	telegraf
<g/>
.	.	kIx.	.
</s>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Macek	Macek	k1gMnSc1	Macek
a	a	k8xC	a
Jindřich	Jindřich	k1gMnSc1	Jindřich
Menzel	Menzel	k1gMnSc1	Menzel
<g/>
,	,	kIx,	,
spolumajitelé	spolumajitel	k1gMnPc1	spolumajitel
společnosti	společnost	k1gFnSc2	společnost
Telegraf	telegraf	k1gInSc1	telegraf
<g/>
,	,	kIx,	,
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
od	od	k7c2	od
státu	stát	k1gInSc2	stát
koupili	koupit	k5eAaPmAgMnP	koupit
Knižní	knižní	k2eAgInSc4d1	knižní
velkoobchod	velkoobchod	k1gInSc4	velkoobchod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
činily	činit	k5eAaImAgInP	činit
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
správu	správa	k1gFnSc4	správa
NFVK	NFVK	kA	NFVK
334	[number]	k4	334
tisíc	tisíc	k4xCgInSc1	tisíc
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
osobní	osobní	k2eAgInPc1d1	osobní
náklady	náklad	k1gInPc1	náklad
226	[number]	k4	226
tisíc	tisíc	k4xCgInPc2	tisíc
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
NFVK	NFVK	kA	NFVK
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
nadační	nadační	k2eAgInPc4d1	nadační
příspěvky	příspěvek	k1gInPc4	příspěvek
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
158	[number]	k4	158
tisíc	tisíc	k4xCgInPc2	tisíc
korun	koruna	k1gFnPc2	koruna
<g/>
:	:	kIx,	:
70	[number]	k4	70
tisíc	tisíc	k4xCgInSc4	tisíc
korun	koruna	k1gFnPc2	koruna
Centru	centr	k1gInSc2	centr
pro	pro	k7c4	pro
ekonomiku	ekonomika	k1gFnSc4	ekonomika
a	a	k8xC	a
politiku	politika	k1gFnSc4	politika
na	na	k7c4	na
seminář	seminář	k1gInSc4	seminář
a	a	k8xC	a
sborník	sborník	k1gInSc4	sborník
textů	text	k1gInPc2	text
o	o	k7c6	o
Thomasi	Thomas	k1gMnSc6	Thomas
Malthusovi	Malthus	k1gMnSc6	Malthus
<g/>
,	,	kIx,	,
40	[number]	k4	40
tisíc	tisíc	k4xCgInSc4	tisíc
korun	koruna	k1gFnPc2	koruna
Centru	centr	k1gInSc2	centr
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
demokracie	demokracie	k1gFnSc2	demokracie
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
na	na	k7c4	na
vydávání	vydávání	k1gNnSc4	vydávání
Revue	revue	k1gFnSc2	revue
Politika	politikum	k1gNnSc2	politikum
a	a	k8xC	a
48	[number]	k4	48
tisíc	tisíc	k4xCgInPc2	tisíc
korun	koruna	k1gFnPc2	koruna
na	na	k7c4	na
nákup	nákup	k1gInSc4	nákup
vybavení	vybavení	k1gNnSc4	vybavení
pro	pro	k7c4	pro
zdravotně	zdravotně	k6eAd1	zdravotně
postižené	postižený	k2eAgMnPc4d1	postižený
občany	občan	k1gMnPc4	občan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
činily	činit	k5eAaImAgInP	činit
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
správu	správa	k1gFnSc4	správa
NFVK	NFVK	kA	NFVK
336	[number]	k4	336
tisíc	tisíc	k4xCgInSc1	tisíc
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
osobní	osobní	k2eAgInPc1d1	osobní
náklady	náklad	k1gInPc1	náklad
226	[number]	k4	226
tisíc	tisíc	k4xCgInPc2	tisíc
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
NFVK	NFVK	kA	NFVK
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
nadační	nadační	k2eAgInPc4d1	nadační
příspěvky	příspěvek	k1gInPc4	příspěvek
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
278	[number]	k4	278
tisíc	tisíc	k4xCgInPc2	tisíc
korun	koruna	k1gFnPc2	koruna
<g/>
:	:	kIx,	:
130	[number]	k4	130
tisíc	tisíc	k4xCgInSc4	tisíc
korun	koruna	k1gFnPc2	koruna
Centru	centr	k1gInSc2	centr
pro	pro	k7c4	pro
ekonomiku	ekonomika	k1gFnSc4	ekonomika
a	a	k8xC	a
politiku	politika	k1gFnSc4	politika
a	a	k8xC	a
148	[number]	k4	148
tisíc	tisíc	k4xCgInPc2	tisíc
korun	koruna	k1gFnPc2	koruna
na	na	k7c4	na
nákup	nákup	k1gInSc4	nákup
vybavení	vybavení	k1gNnSc4	vybavení
pro	pro	k7c4	pro
zdravotně	zdravotně	k6eAd1	zdravotně
postižené	postižený	k2eAgMnPc4d1	postižený
občany	občan	k1gMnPc4	občan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
poklesly	poklesnout	k5eAaPmAgInP	poklesnout
příspěvky	příspěvek	k1gInPc1	příspěvek
poskytnuté	poskytnutý	k2eAgInPc1d1	poskytnutý
fondem	fond	k1gInSc7	fond
na	na	k7c4	na
48	[number]	k4	48
tisíc	tisíc	k4xCgInPc2	tisíc
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
správu	správa	k1gFnSc4	správa
NFVK	NFVK	kA	NFVK
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
výše	vysoce	k6eAd2	vysoce
325	[number]	k4	325
tisíc	tisíc	k4xCgInPc2	tisíc
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
osobní	osobní	k2eAgInPc1d1	osobní
náklady	náklad	k1gInPc1	náklad
225	[number]	k4	225
tisíc	tisíc	k4xCgInPc2	tisíc
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Předsedkyní	předsedkyně	k1gFnSc7	předsedkyně
správní	správní	k2eAgFnSc2d1	správní
rady	rada	k1gFnSc2	rada
fondu	fond	k1gInSc2	fond
je	být	k5eAaImIp3nS	být
bývalá	bývalý	k2eAgFnSc1d1	bývalá
předsedkyně	předsedkyně	k1gFnSc1	předsedkyně
Senátu	senát	k1gInSc2	senát
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
Libuše	Libuše	k1gFnSc1	Libuše
Benešová	Benešová	k1gFnSc1	Benešová
a	a	k8xC	a
členy	člen	k1gMnPc7	člen
Michaela	Michaela	k1gFnSc1	Michaela
Bartáková	Bartáková	k1gFnSc1	Bartáková
a	a	k8xC	a
Ladislav	Ladislav	k1gMnSc1	Ladislav
Jakl	Jakl	k1gMnSc1	Jakl
<g/>
.	.	kIx.	.
</s>
<s>
Předsedou	předseda	k1gMnSc7	předseda
dozorčí	dozorčí	k2eAgFnSc2d1	dozorčí
rady	rada	k1gFnSc2	rada
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
ministr	ministr	k1gMnSc1	ministr
Martin	Martin	k1gMnSc1	Martin
Kocourek	Kocourek	k1gMnSc1	Kocourek
<g/>
.	.	kIx.	.
a	a	k8xC	a
členy	člen	k1gInPc7	člen
Pavel	Pavel	k1gMnSc1	Pavel
Bém	Bém	k1gMnSc1	Bém
a	a	k8xC	a
Pavel	Pavel	k1gMnSc1	Pavel
Huťka	Huťka	k1gMnSc1	Huťka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
bylo	být	k5eAaImAgNnS	být
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
NFVK	NFVK	kA	NFVK
zanikne	zaniknout	k5eAaPmIp3nS	zaniknout
sloučením	sloučení	k1gNnSc7	sloučení
s	s	k7c7	s
Nadačním	nadační	k2eAgInSc7d1	nadační
fondem	fond	k1gInSc7	fond
manželů	manžel	k1gMnPc2	manžel
Livie	Livie	k1gFnSc2	Livie
a	a	k8xC	a
Václava	Václav	k1gMnSc2	Václav
Klausových	Klausová	k1gFnPc2	Klausová
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
ale	ale	k9	ale
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
politické	politický	k2eAgFnSc2d1	politická
kariéry	kariéra	k1gFnSc2	kariéra
byla	být	k5eAaImAgFnS	být
Václavu	Václava	k1gFnSc4	Václava
Klausovi	Klaus	k1gMnSc3	Klaus
udělena	udělit	k5eAaPmNgFnS	udělit
řada	řada	k1gFnSc1	řada
vyznamenání	vyznamenání	k1gNnSc2	vyznamenání
a	a	k8xC	a
čestných	čestný	k2eAgMnPc2d1	čestný
akademických	akademický	k2eAgMnPc2d1	akademický
titulů	titul	k1gInPc2	titul
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
i	i	k8xC	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
60	[number]	k4	60
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
úmrtí	úmrtí	k1gNnSc2	úmrtí
Tomáše	Tomáš	k1gMnSc2	Tomáš
Garrigua	Garriguus	k1gMnSc2	Garriguus
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
,	,	kIx,	,
obdržel	obdržet	k5eAaPmAgMnS	obdržet
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Petrem	Petr	k1gMnSc7	Petr
Pithartem	Pithart	k1gMnSc7	Pithart
a	a	k8xC	a
Milošem	Miloš	k1gMnSc7	Miloš
Zemanem	Zeman	k1gMnSc7	Zeman
v	v	k7c6	v
Rudolfinu	Rudolfinum	k1gNnSc6	Rudolfinum
Čestnou	čestný	k2eAgFnSc4d1	čestná
medaili	medaile	k1gFnSc4	medaile
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
uděluje	udělovat	k5eAaImIp3nS	udělovat
Masarykovo	Masarykův	k2eAgNnSc4d1	Masarykovo
demokratické	demokratický	k2eAgNnSc4d1	demokratické
hnutí	hnutí	k1gNnSc4	hnutí
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2007	[number]	k4	2007
<g />
.	.	kIx.	.
</s>
<s>
převzal	převzít	k5eAaPmAgMnS	převzít
od	od	k7c2	od
velvyslance	velvyslanec	k1gMnSc2	velvyslanec
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
Puškinovu	Puškinův	k2eAgFnSc4d1	Puškinova
medaili	medaile	k1gFnSc4	medaile
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
mu	on	k3xPp3gMnSc3	on
udělil	udělit	k5eAaPmAgMnS	udělit
ruský	ruský	k2eAgMnSc1d1	ruský
prezident	prezident	k1gMnSc1	prezident
Vladimir	Vladimir	k1gMnSc1	Vladimir
Putin	putin	k2eAgMnSc1d1	putin
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2009	[number]	k4	2009
obdržel	obdržet	k5eAaPmAgMnS	obdržet
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
bývalého	bývalý	k2eAgMnSc2d1	bývalý
prezidenta	prezident	k1gMnSc2	prezident
Spolkové	spolkový	k2eAgFnSc2d1	spolková
republiky	republika	k1gFnSc2	republika
Německo	Německo	k1gNnSc1	Německo
Romana	Roman	k1gMnSc2	Roman
Herzoga	Herzog	k1gMnSc2	Herzog
ve	v	k7c6	v
Freiburgu	Freiburg	k1gInSc6	Freiburg
v	v	k7c4	v
Breisgau	Breisga	k2eAgFnSc4d1	Breisga
Mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
cenu	cena	k1gFnSc4	cena
Nadace	nadace	k1gFnSc2	nadace
Friedricha	Friedrich	k1gMnSc2	Friedrich
Augusta	August	k1gMnSc2	August
von	von	k1gInSc4	von
Hayeka	Hayeko	k1gNnSc2	Hayeko
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
neúnavné	únavný	k2eNgNnSc4d1	neúnavné
angažmá	angažmá	k1gNnSc4	angažmá
pro	pro	k7c4	pro
věc	věc	k1gFnSc4	věc
svobodného	svobodný	k2eAgNnSc2d1	svobodné
tržního	tržní	k2eAgNnSc2d1	tržní
hospodářství	hospodářství	k1gNnSc2	hospodářství
a	a	k8xC	a
blízkost	blízkost	k1gFnSc4	blízkost
k	k	k7c3	k
principům	princip	k1gInPc3	princip
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
Hayek	Hayek	k6eAd1	Hayek
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
vědeckých	vědecký	k2eAgNnPc6d1	vědecké
dílech	dílo	k1gNnPc6	dílo
zastával	zastávat	k5eAaImAgMnS	zastávat
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
více	hodně	k6eAd2	hodně
než	než	k8xS	než
třiceti	třicet	k4xCc2	třicet
titulů	titul	k1gInPc2	titul
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
nejúspěšnější	úspěšný	k2eAgFnSc7d3	nejúspěšnější
knihou	kniha	k1gFnSc7	kniha
je	být	k5eAaImIp3nS	být
Modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
zelená	zelený	k2eAgFnSc1d1	zelená
planeta	planeta	k1gFnSc1	planeta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
přeložena	přeložit	k5eAaPmNgFnS	přeložit
do	do	k7c2	do
osmnácti	osmnáct	k4xCc2	osmnáct
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgNnPc4d1	další
známá	známý	k2eAgNnPc4d1	známé
díla	dílo	k1gNnPc4	dílo
patří	patřit	k5eAaImIp3nS	patřit
Evropská	evropský	k2eAgFnSc1d1	Evropská
integrace	integrace	k1gFnSc1	integrace
bez	bez	k7c2	bez
iluzí	iluze	k1gFnPc2	iluze
s	s	k7c7	s
pěti	pět	k4xCc2	pět
cizojazyčnými	cizojazyčný	k2eAgFnPc7d1	cizojazyčná
mutacemi	mutace	k1gFnPc7	mutace
a	a	k8xC	a
kniha	kniha	k1gFnSc1	kniha
Zápisky	zápiska	k1gFnSc2	zápiska
z	z	k7c2	z
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Politická	politický	k2eAgFnSc1d1	politická
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
ekonomie	ekonomie	k1gFnSc1	ekonomie
1969	[number]	k4	1969
<g/>
:	:	kIx,	:
Marxova	Marxův	k2eAgFnSc1d1	Marxova
politická	politický	k2eAgFnSc1d1	politická
ekonomie	ekonomie	k1gFnSc1	ekonomie
a	a	k8xC	a
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
teorie	teorie	k1gFnSc1	teorie
1991	[number]	k4	1991
<g/>
:	:	kIx,	:
A	a	k8xC	a
Road	Road	k1gMnSc1	Road
to	ten	k3xDgNnSc4	ten
Market	market	k1gInSc1	market
Economy	Econom	k1gInPc1	Econom
1991	[number]	k4	1991
<g/>
:	:	kIx,	:
Ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
teorie	teorie	k1gFnSc1	teorie
a	a	k8xC	a
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
reforma	reforma	k1gFnSc1	reforma
1992	[number]	k4	1992
<g/>
:	:	kIx,	:
Dismantling	Dismantling	k1gInSc1	Dismantling
Socialism	Socialism	k1gInSc1	Socialism
<g/>
:	:	kIx,	:
A	a	k8xC	a
Road	Road	k1gMnSc1	Road
to	ten	k3xDgNnSc4	ten
Market	market	k1gInSc1	market
Economy	Econom	k1gInPc7	Econom
II	II	kA	II
<g/>
.	.	kIx.	.
1994	[number]	k4	1994
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
cesta	cesta	k1gFnSc1	cesta
<g />
.	.	kIx.	.
</s>
<s>
2015	[number]	k4	2015
<g/>
:	:	kIx,	:
Velcí	velký	k2eAgMnPc1d1	velký
ekonomové	ekonom	k1gMnPc1	ekonom
jsou	být	k5eAaImIp3nP	být
mou	můj	k3xOp1gFnSc7	můj
inspirací	inspirace	k1gFnSc7	inspirace
Eseje	esej	k1gFnSc2	esej
a	a	k8xC	a
projevy	projev	k1gInPc7	projev
1998	[number]	k4	1998
<g/>
:	:	kIx,	:
Tak	tak	k6eAd1	tak
pravil	pravit	k5eAaBmAgMnS	pravit
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
1999	[number]	k4	1999
<g/>
:	:	kIx,	:
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
již	již	k6eAd1	již
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
nevládne	vládnout	k5eNaImIp3nS	vládnout
2004	[number]	k4	2004
<g/>
:	:	kIx,	:
Rok	rok	k1gInSc4	rok
první	první	k4xOgInSc4	první
-	-	kIx~	-
projevy	projev	k1gInPc1	projev
<g/>
,	,	kIx,	,
články	článek	k1gInPc1	článek
<g/>
,	,	kIx,	,
eseje	esej	k1gInPc1	esej
2005	[number]	k4	2005
<g/>
:	:	kIx,	:
Rok	rok	k1gInSc4	rok
druhý	druhý	k4xOgInSc4	druhý
-	-	kIx~	-
projevy	projev	k1gInPc4	projev
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
články	článek	k1gInPc1	článek
<g/>
,	,	kIx,	,
eseje	esej	k1gInPc1	esej
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
Rok	rok	k1gInSc4	rok
třetí	třetí	k4xOgInSc4	třetí
-	-	kIx~	-
projevy	projev	k1gInPc1	projev
<g/>
,	,	kIx,	,
články	článek	k1gInPc1	článek
<g/>
,	,	kIx,	,
eseje	esej	k1gInPc1	esej
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
Rok	rok	k1gInSc4	rok
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
-	-	kIx~	-
projevy	projev	k1gInPc1	projev
<g/>
,	,	kIx,	,
články	článek	k1gInPc1	článek
<g/>
,	,	kIx,	,
eseje	esej	k1gInPc1	esej
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
Rok	rok	k1gInSc4	rok
pátý	pátý	k4xOgInSc4	pátý
-	-	kIx~	-
projevy	projev	k1gInPc1	projev
<g/>
,	,	kIx,	,
články	článek	k1gInPc1	článek
<g/>
,	,	kIx,	,
eseje	esej	k1gInPc1	esej
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
Rok	rok	k1gInSc4	rok
šestý	šestý	k4xOgInSc4	šestý
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
projevy	projev	k1gInPc1	projev
<g/>
,	,	kIx,	,
články	článek	k1gInPc1	článek
<g/>
,	,	kIx,	,
eseje	esej	k1gInPc1	esej
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
Rok	rok	k1gInSc4	rok
sedmý	sedmý	k4xOgInSc4	sedmý
-	-	kIx~	-
projevy	projev	k1gInPc1	projev
<g/>
,	,	kIx,	,
články	článek	k1gInPc1	článek
<g/>
,	,	kIx,	,
eseje	esej	k1gInPc1	esej
2011	[number]	k4	2011
<g/>
:	:	kIx,	:
Rok	rok	k1gInSc4	rok
osmý	osmý	k4xOgInSc4	osmý
-	-	kIx~	-
projevy	projev	k1gInPc1	projev
<g/>
,	,	kIx,	,
články	článek	k1gInPc1	článek
<g/>
,	,	kIx,	,
eseje	esej	k1gInPc1	esej
2012	[number]	k4	2012
<g/>
:	:	kIx,	:
Rok	rok	k1gInSc4	rok
devátý	devátý	k4xOgInSc4	devátý
-	-	kIx~	-
projevy	projev	k1gInPc1	projev
<g/>
,	,	kIx,	,
články	článek	k1gInPc1	článek
<g/>
,	,	kIx,	,
eseje	esej	k1gInPc1	esej
2013	[number]	k4	2013
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
desátý	desátý	k4xOgInSc1	desátý
-	-	kIx~	-
projevy	projev	k1gInPc1	projev
<g/>
,	,	kIx,	,
články	článek	k1gInPc1	článek
<g/>
,	,	kIx,	,
eseje	esej	k1gInPc1	esej
Ostatní	ostatní	k2eAgInPc1d1	ostatní
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
Modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
zelená	zelený	k2eAgFnSc1d1	zelená
planeta	planeta	k1gFnSc1	planeta
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
Modrá	modrý	k2eAgFnSc1d1	modrá
planeta	planeta	k1gFnSc1	planeta
v	v	k7c6	v
ohrožení	ohrožení	k1gNnSc6	ohrožení
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
Kde	kde	k6eAd1	kde
začíná	začínat	k5eAaImIp3nS	začínat
zítřek	zítřek	k1gInSc4	zítřek
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
Zápisky	zápiska	k1gFnSc2	zápiska
z	z	k7c2	z
cest	cesta	k1gFnPc2	cesta
2011	[number]	k4	2011
<g/>
:	:	kIx,	:
Evropská	evropský	k2eAgFnSc1d1	Evropská
integrace	integrace	k1gFnSc1	integrace
bez	bez	k7c2	bez
iluzí	iluze	k1gFnPc2	iluze
2012	[number]	k4	2012
<g/>
:	:	kIx,	:
Zápisky	zápiska	k1gFnSc2	zápiska
<g />
.	.	kIx.	.
</s>
<s>
z	z	k7c2	z
nových	nový	k2eAgFnPc2d1	nová
cest	cesta	k1gFnPc2	cesta
2013	[number]	k4	2013
<g/>
:	:	kIx,	:
My	my	k3xPp1nPc1	my
<g/>
,	,	kIx,	,
Evropa	Evropa	k1gFnSc1	Evropa
a	a	k8xC	a
svět	svět	k1gInSc1	svět
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Pehe	Pehe	k1gFnSc1	Pehe
<g/>
,	,	kIx,	,
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
:	:	kIx,	:
portrét	portrét	k1gInSc1	portrét
politika	politik	k1gMnSc2	politik
ve	v	k7c6	v
dvaceti	dvacet	k4xCc6	dvacet
obrazech	obraz	k1gInPc6	obraz
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Prostor	prostor	k1gInSc1	prostor
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-7260-240-7	[number]	k4	978-80-7260-240-7
Kdo	kdo	k3yInSc1	kdo
byl	být	k5eAaImAgMnS	být
kdo	kdo	k3yRnSc1	kdo
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
dějinách	dějiny	k1gFnPc6	dějiny
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
I.	I.	kA	I.
<g/>
,	,	kIx,	,
A	A	kA	A
<g/>
-	-	kIx~	-
<g/>
M	M	kA	M
/	/	kIx~	/
Milan	Milan	k1gMnSc1	Milan
Churaň	Churaň	k1gMnSc1	Churaň
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
..	..	k?	..
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
467	[number]	k4	467
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85983	[number]	k4	85983
<g/>
-	-	kIx~	-
<g/>
44	[number]	k4	44
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
319	[number]	k4	319
<g/>
-	-	kIx~	-
<g/>
321	[number]	k4	321
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yRnSc1	kdo
je	být	k5eAaImIp3nS	být
kdo	kdo	k3yRnSc1	kdo
:	:	kIx,	:
91	[number]	k4	91
<g/>
/	/	kIx~	/
<g/>
92	[number]	k4	92
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
federální	federální	k2eAgInPc4d1	federální
orgány	orgán	k1gInPc4	orgán
ČSFR	ČSFR	kA	ČSFR
<g/>
.	.	kIx.	.
</s>
<s>
Díl	díl	k1gInSc1	díl
1	[number]	k4	1
<g/>
,	,	kIx,	,
A	A	kA	A
<g/>
-	-	kIx~	-
<g/>
M.	M.	kA	M.
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Kdo	kdo	k3yInSc1	kdo
je	být	k5eAaImIp3nS	být
kdo	kdo	k3yInSc1	kdo
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
636	[number]	k4	636
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
901103	[number]	k4	901103
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
420	[number]	k4	420
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yInSc1	kdo
je	být	k5eAaImIp3nS	být
kdo	kdo	k3yInSc1	kdo
=	=	kIx~	=
Who	Who	k1gMnPc1	Who
is	is	k?	is
who	who	k?	who
:	:	kIx,	:
osobnosti	osobnost	k1gFnPc4	osobnost
české	český	k2eAgFnSc2d1	Česká
současnosti	současnost	k1gFnSc2	současnost
:	:	kIx,	:
5000	[number]	k4	5000
životopisů	životopis	k1gInPc2	životopis
/	/	kIx~	/
(	(	kIx(	(
<g/>
Michael	Michael	k1gMnSc1	Michael
Třeštík	Třeštík	k1gMnSc1	Třeštík
editor	editor	k1gMnSc1	editor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Agentura	agentura	k1gFnSc1	agentura
Kdo	kdo	k3yRnSc1	kdo
je	být	k5eAaImIp3nS	být
kdo	kdo	k3yInSc1	kdo
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
775	[number]	k4	775
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902586	[number]	k4	902586
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
288	[number]	k4	288
<g/>
-	-	kIx~	-
<g/>
289	[number]	k4	289
<g/>
.	.	kIx.	.
</s>
<s>
KOSATÍK	KOSATÍK	kA	KOSATÍK
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
demokraté	demokrat	k1gMnPc1	demokrat
:	:	kIx,	:
50	[number]	k4	50
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
osobností	osobnost	k1gFnPc2	osobnost
veřejného	veřejný	k2eAgInSc2d1	veřejný
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
280	[number]	k4	280
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
2307	[number]	k4	2307
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Osobnosti	osobnost	k1gFnPc1	osobnost
-	-	kIx~	-
Česko	Česko	k1gNnSc1	Česko
:	:	kIx,	:
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
823	[number]	k4	823
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7360	[number]	k4	7360
<g/>
-	-	kIx~	-
<g/>
796	[number]	k4	796
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
323	[number]	k4	323
<g/>
.	.	kIx.	.
</s>
<s>
TOMEŠ	Tomeš	k1gMnSc1	Tomeš
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
XX	XX	kA	XX
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
:	:	kIx,	:
II	II	kA	II
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
:	:	kIx,	:
K	k	k7c3	k
<g/>
-	-	kIx~	-
<g/>
P.	P.	kA	P.
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
;	;	kIx,	;
Petr	Petr	k1gMnSc1	Petr
Meissner	Meissner	k1gMnSc1	Meissner
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
649	[number]	k4	649
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
246	[number]	k4	246
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
64	[number]	k4	64
<g/>
-	-	kIx~	-
<g/>
65	[number]	k4	65
<g/>
.	.	kIx.	.
</s>
<s>
Rozhovory	rozhovor	k1gInPc1	rozhovor
1997	[number]	k4	1997
<g/>
:	:	kIx,	:
NADORAZ	NADORAZ	k?	NADORAZ
-	-	kIx~	-
Rozhovor	rozhovor	k1gInSc1	rozhovor
s	s	k7c7	s
Janou	Jana	k1gFnSc7	Jana
Klusákovou	Klusáková	k1gFnSc7	Klusáková
2001	[number]	k4	2001
<g/>
:	:	kIx,	:
NAROVINU	NAROVINU	kA	NAROVINU
-	-	kIx~	-
Kniha	kniha	k1gFnSc1	kniha
hovorů	hovor	k1gInPc2	hovor
Petra	Petr	k1gMnSc2	Petr
Hájka	Hájek	k1gMnSc2	Hájek
s	s	k7c7	s
Václavem	Václav	k1gMnSc7	Václav
Klausem	Klaus	k1gMnSc7	Klaus
2003	[number]	k4	2003
<g/>
:	:	kIx,	:
Přerušený	přerušený	k2eAgInSc1d1	přerušený
rozhovor	rozhovor	k1gInSc1	rozhovor
-	-	kIx~	-
Knižní	knižní	k2eAgInSc1d1	knižní
rozhovor	rozhovor	k1gInSc1	rozhovor
s	s	k7c7	s
Jeronýmem	Jeroným	k1gMnSc7	Jeroným
Janíčkem	Janíček	k1gMnSc7	Janíček
</s>
