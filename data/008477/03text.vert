<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
florbale	florbal	k1gInSc5	florbal
do	do	k7c2	do
19	[number]	k4	19
let	léto	k1gNnPc2	léto
2015	[number]	k4	2015
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Helsingborgu	Helsingborg	k1gInSc2	Helsingborg
<g/>
.	.	kIx.	.
</s>
<s>
Obhájcem	obhájce	k1gMnSc7	obhájce
titulu	titul	k1gInSc2	titul
bylo	být	k5eAaImAgNnS	být
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
ve	v	k7c6	v
florbalu	florbal	k1gInSc6	florbal
odcházelo	odcházet	k5eAaImAgNnS	odcházet
bez	bez	k7c2	bez
medaile	medaile	k1gFnSc2	medaile
<g/>
,	,	kIx,	,
když	když	k8xS	když
prohrálo	prohrát	k5eAaPmAgNnS	prohrát
boj	boj	k1gInSc4	boj
o	o	k7c4	o
bronz	bronz	k1gInSc4	bronz
s	s	k7c7	s
Českem	Česko	k1gNnSc7	Česko
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Vítězem	vítěz	k1gMnSc7	vítěz
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
tým	tým	k1gInSc1	tým
Finska	Finsko	k1gNnSc2	Finsko
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
nad	nad	k7c7	nad
Švýcarskem	Švýcarsko	k1gNnSc7	Švýcarsko
13	[number]	k4	13
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Skupinová	skupinový	k2eAgFnSc1d1	skupinová
fáze	fáze	k1gFnSc1	fáze
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Skupina	skupina	k1gFnSc1	skupina
A	a	k9	a
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Zápasy	zápas	k1gInPc1	zápas
====	====	k?	====
</s>
</p>
<p>
<s>
===	===	k?	===
Skupina	skupina	k1gFnSc1	skupina
B	B	kA	B
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Zápasy	zápas	k1gInPc1	zápas
====	====	k?	====
</s>
</p>
<p>
<s>
==	==	k?	==
Play	play	k0	play
off	off	k?	off
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Pavouk	pavouk	k1gMnSc1	pavouk
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
O	o	k7c4	o
7	[number]	k4	7
<g/>
.	.	kIx.	.
<g/>
místo	místo	k1gNnSc4	místo
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
O	o	k7c4	o
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
místo	místo	k1gNnSc4	místo
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Semifinále	semifinále	k1gNnPc3	semifinále
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
O	o	k7c4	o
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Finále	finále	k1gNnSc1	finále
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Konečné	Konečné	k2eAgNnSc1d1	Konečné
pořadí	pořadí	k1gNnSc1	pořadí
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
florbale	florbal	k1gInSc5	florbal
do	do	k7c2	do
19	[number]	k4	19
let	léto	k1gNnPc2	léto
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInPc1d1	oficiální
výsledky	výsledek	k1gInPc1	výsledek
</s>
</p>
