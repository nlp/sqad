<s>
Mars	Mars	k1gInSc1	Mars
je	být	k5eAaImIp3nS	být
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
planeta	planeta	k1gFnSc1	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
nejmenší	malý	k2eAgFnSc1d3	nejmenší
planeta	planeta	k1gFnSc1	planeta
soustavy	soustava	k1gFnSc2	soustava
po	po	k7c6	po
Merkuru	Merkur	k1gInSc6	Merkur
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
po	po	k7c6	po
římském	římský	k2eAgMnSc6d1	římský
bohu	bůh	k1gMnSc6	bůh
války	válka	k1gFnSc2	válka
Martovi	Mars	k1gMnSc3	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
planetu	planeta	k1gFnSc4	planeta
terestrického	terestrický	k2eAgInSc2d1	terestrický
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
má	mít	k5eAaImIp3nS	mít
pevný	pevný	k2eAgInSc1d1	pevný
horninový	horninový	k2eAgInSc1d1	horninový
povrch	povrch	k1gInSc1	povrch
pokrytý	pokrytý	k2eAgInSc1d1	pokrytý
impaktními	impaktní	k2eAgInPc7d1	impaktní
krátery	kráter	k1gInPc7	kráter
<g/>
,	,	kIx,	,
vysokými	vysoký	k2eAgFnPc7d1	vysoká
sopkami	sopka	k1gFnPc7	sopka
<g/>
,	,	kIx,	,
hlubokými	hluboký	k2eAgInPc7d1	hluboký
kaňony	kaňon	k1gInPc7	kaňon
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
útvary	útvar	k1gInPc7	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
nepravidelného	pravidelný	k2eNgInSc2d1	nepravidelný
tvaru	tvar	k1gInSc2	tvar
pojmenované	pojmenovaný	k2eAgNnSc1d1	pojmenované
Phobos	Phobos	k1gInSc4	Phobos
a	a	k8xC	a
Deimos	Deimos	k1gInSc4	Deimos
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
Mars	Mars	k1gInSc1	Mars
v	v	k7c6	v
opozici	opozice	k1gFnSc6	opozice
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
a	a	k8xC	a
Země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nachází	nacházet	k5eAaImIp3nS	nacházet
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
dvěma	dva	k4xCgInPc7	dva
tělesy	těleso	k1gNnPc7	těleso
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Mars	Mars	k1gInSc1	Mars
pozorovatelný	pozorovatelný	k2eAgInSc1d1	pozorovatelný
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
noc	noc	k1gFnSc4	noc
<g/>
.	.	kIx.	.
</s>
<s>
Spolehlivé	spolehlivý	k2eAgFnPc1d1	spolehlivá
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
prvních	první	k4xOgNnPc6	první
pozorováních	pozorování	k1gNnPc6	pozorování
Marsu	Mars	k1gInSc2	Mars
jako	jako	k9	jako
planety	planeta	k1gFnPc1	planeta
neexistují	existovat	k5eNaImIp3nP	existovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
došlo	dojít	k5eAaPmAgNnS	dojít
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
3000	[number]	k4	3000
až	až	k9	až
4000	[number]	k4	4000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Všechny	všechen	k3xTgFnPc1	všechen
starověké	starověký	k2eAgFnPc1d1	starověká
civilizace	civilizace	k1gFnPc1	civilizace
<g/>
,	,	kIx,	,
Egypťané	Egypťan	k1gMnPc1	Egypťan
<g/>
,	,	kIx,	,
Babylóňané	Babylóňan	k1gMnPc1	Babylóňan
a	a	k8xC	a
Řekové	Řek	k1gMnPc1	Řek
<g/>
,	,	kIx,	,
znaly	znát	k5eAaImAgFnP	znát
tuto	tento	k3xDgFnSc4	tento
"	"	kIx"	"
<g/>
putující	putující	k2eAgFnSc4d1	putující
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
"	"	kIx"	"
a	a	k8xC	a
měly	mít	k5eAaImAgFnP	mít
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
svá	svůj	k3xOyFgNnPc4	svůj
pojmenování	pojmenování	k1gNnSc4	pojmenování
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
jejímu	její	k3xOp3gInSc3	její
načervenalému	načervenalý	k2eAgInSc3d1	načervenalý
nádechu	nádech	k1gInSc3	nádech
<g/>
,	,	kIx,	,
způsobenému	způsobený	k2eAgMnSc3d1	způsobený
červenou	červený	k2eAgFnSc7d1	červená
barvou	barva	k1gFnSc7	barva
zoxidované	zoxidovaný	k2eAgFnSc2d1	zoxidovaná
půdy	půda	k1gFnSc2	půda
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
považovaly	považovat	k5eAaImAgInP	považovat
staré	starý	k2eAgInPc1d1	starý
národy	národ	k1gInPc1	národ
Mars	Mars	k1gInSc4	Mars
většinou	většina	k1gFnSc7	většina
za	za	k7c4	za
symbol	symbol	k1gInSc4	symbol
ohně	oheň	k1gInSc2	oheň
<g/>
,	,	kIx,	,
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
zániku	zánik	k1gInSc2	zánik
<g/>
.	.	kIx.	.
</s>
<s>
Detailní	detailní	k2eAgNnSc1d1	detailní
zkoumání	zkoumání	k1gNnSc1	zkoumání
planety	planeta	k1gFnSc2	planeta
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
takřka	takřka	k6eAd1	takřka
20	[number]	k4	20
úspěšných	úspěšný	k2eAgFnPc2d1	úspěšná
automatických	automatický	k2eAgFnPc2d1	automatická
sond	sonda	k1gFnPc2	sonda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
kolem	kolem	k7c2	kolem
Marsu	Mars	k1gInSc2	Mars
pět	pět	k4xCc4	pět
funkčních	funkční	k2eAgFnPc2d1	funkční
sond	sonda	k1gFnPc2	sonda
(	(	kIx(	(
<g/>
Mars	Mars	k1gInSc1	Mars
Odyssey	Odyssea	k1gFnSc2	Odyssea
<g/>
,	,	kIx,	,
Mars	Mars	k1gInSc1	Mars
Express	express	k1gInSc1	express
<g/>
,	,	kIx,	,
Mars	Mars	k1gMnSc1	Mars
Reconnaissance	Reconnaissance	k1gFnSc2	Reconnaissance
Orbiter	Orbiter	k1gMnSc1	Orbiter
<g/>
,	,	kIx,	,
Mars	Mars	k1gMnSc1	Mars
Orbiter	Orbitra	k1gFnPc2	Orbitra
Mission	Mission	k1gInSc1	Mission
a	a	k8xC	a
MAVEN	MAVEN	kA	MAVEN
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
planety	planeta	k1gFnSc2	planeta
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
dvě	dva	k4xCgNnPc1	dva
vozítka	vozítko	k1gNnPc1	vozítko
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
jedno	jeden	k4xCgNnSc1	jeden
spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
mise	mise	k1gFnSc2	mise
Mars	Mars	k1gInSc1	Mars
Exploration	Exploration	k1gInSc1	Exploration
Rover	rover	k1gMnSc1	rover
(	(	kIx(	(
<g/>
Opportunity	Opportunit	k1gInPc1	Opportunit
<g/>
)	)	kIx)	)
a	a	k8xC	a
druhé	druhý	k4xOgInPc1	druhý
do	do	k7c2	do
mise	mise	k1gFnSc2	mise
Mars	Mars	k1gInSc1	Mars
Science	Science	k1gFnSc2	Science
Laboratory	Laborator	k1gInPc1	Laborator
(	(	kIx(	(
<g/>
Curiosity	Curiosita	k1gFnPc1	Curiosita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
sondám	sonda	k1gFnPc3	sonda
se	se	k3xPyFc4	se
povedlo	povést	k5eAaPmAgNnS	povést
zmapovat	zmapovat	k5eAaPmF	zmapovat
větší	veliký	k2eAgFnSc4d2	veliký
část	část	k1gFnSc4	část
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
definovat	definovat	k5eAaBmF	definovat
základní	základní	k2eAgNnPc4d1	základní
historická	historický	k2eAgNnPc4d1	historické
období	období	k1gNnPc4	období
či	či	k8xC	či
porozumět	porozumět	k5eAaPmF	porozumět
základním	základní	k2eAgInPc3d1	základní
jevům	jev	k1gInPc3	jev
odehrávajícím	odehrávající	k2eAgInPc3d1	odehrávající
se	se	k3xPyFc4	se
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Mars	Mars	k1gInSc1	Mars
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
ostatní	ostatní	k2eAgFnPc1d1	ostatní
planety	planeta	k1gFnPc1	planeta
našeho	náš	k3xOp1gInSc2	náš
systému	systém	k1gInSc2	systém
přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
4,5	[number]	k4	4,5
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
akrecí	akrece	k1gFnPc2	akrece
z	z	k7c2	z
pracho-plynného	pracholynný	k2eAgInSc2d1	pracho-plynný
disku	disk	k1gInSc2	disk
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
obíhal	obíhat	k5eAaImAgMnS	obíhat
kolem	kolem	k7c2	kolem
rodící	rodící	k2eAgFnSc2d1	rodící
se	se	k3xPyFc4	se
centrální	centrální	k2eAgFnSc2d1	centrální
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Srážkami	srážka	k1gFnPc7	srážka
prachových	prachový	k2eAgFnPc2d1	prachová
částic	částice	k1gFnPc2	částice
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgNnP	začít
formovat	formovat	k5eAaImF	formovat
malá	malý	k2eAgNnPc1d1	malé
tělesa	těleso	k1gNnPc1	těleso
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
svou	svůj	k3xOyFgFnSc7	svůj
gravitací	gravitace	k1gFnSc7	gravitace
přitahovala	přitahovat	k5eAaImAgFnS	přitahovat
další	další	k2eAgFnSc1d1	další
částice	částice	k1gFnSc1	částice
a	a	k8xC	a
okolní	okolní	k2eAgInSc1d1	okolní
plyn	plyn	k1gInSc1	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
tak	tak	k9	tak
první	první	k4xOgFnPc1	první
planetesimály	planetesimála	k1gFnPc1	planetesimála
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
vzájemně	vzájemně	k6eAd1	vzájemně
srážely	srážet	k5eAaImAgInP	srážet
a	a	k8xC	a
formovaly	formovat	k5eAaImAgInP	formovat
větší	veliký	k2eAgFnPc4d2	veliký
tělesa	těleso	k1gNnPc4	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
tohoto	tento	k3xDgInSc2	tento
procesu	proces	k1gInSc2	proces
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
čtyři	čtyři	k4xCgInPc1	čtyři
terestrické	terestrický	k2eAgInPc1d1	terestrický
protoplanety	protoplanet	k1gInPc1	protoplanet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
má	mít	k5eAaImIp3nS	mít
Mars	Mars	k1gInSc1	Mars
-	-	kIx~	-
nejvzdálenější	vzdálený	k2eAgInPc1d3	nejvzdálenější
z	z	k7c2	z
terestrických	terestrický	k2eAgFnPc2d1	terestrická
planet	planeta	k1gFnPc2	planeta
-	-	kIx~	-
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
zastoupení	zastoupení	k1gNnSc1	zastoupení
lehkých	lehký	k2eAgInPc2d1	lehký
prvků	prvek	k1gInPc2	prvek
jako	jako	k8xC	jako
křemík	křemík	k1gInSc1	křemík
<g/>
,	,	kIx,	,
hliník	hliník	k1gInSc1	hliník
či	či	k8xC	či
síra	síra	k1gFnSc1	síra
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zformování	zformování	k1gNnSc1	zformování
protoplanety	protoplanet	k1gInPc7	protoplanet
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
masivnímu	masivní	k2eAgNnSc3d1	masivní
bombardování	bombardování	k1gNnSc3	bombardování
povrchu	povrch	k1gInSc2	povrch
zbylým	zbylý	k2eAgInSc7d1	zbylý
materiálem	materiál	k1gInSc7	materiál
ze	z	k7c2	z
vzniku	vznik	k1gInSc2	vznik
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
jeho	on	k3xPp3gInSc2	on
neustálé	neustálý	k2eAgNnSc1d1	neustálé
přetváření	přetváření	k1gNnSc1	přetváření
a	a	k8xC	a
přetavování	přetavování	k1gNnSc1	přetavování
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dokonce	dokonce	k9	dokonce
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
celý	celý	k2eAgInSc1d1	celý
povrch	povrch	k1gInSc1	povrch
byl	být	k5eAaImAgInS	být
roztaven	roztavit	k5eAaPmNgInS	roztavit
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
tzv.	tzv.	kA	tzv.
magmatického	magmatický	k2eAgInSc2d1	magmatický
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
tepelná	tepelný	k2eAgFnSc1d1	tepelná
energie	energie	k1gFnSc1	energie
společně	společně	k6eAd1	společně
s	s	k7c7	s
teplem	teplo	k1gNnSc7	teplo
uvolněným	uvolněný	k2eAgNnSc7d1	uvolněné
diferenciací	diferenciace	k1gFnSc7	diferenciace
pláště	plášť	k1gInSc2	plášť
a	a	k8xC	a
jádra	jádro	k1gNnSc2	jádro
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
kumulována	kumulován	k2eAgFnSc1d1	kumulována
v	v	k7c6	v
nitru	nitro	k1gNnSc6	nitro
planety	planeta	k1gFnSc2	planeta
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
existenci	existence	k1gFnSc4	existence
vulkanismu	vulkanismus	k1gInSc2	vulkanismus
a	a	k8xC	a
tektonických	tektonický	k2eAgInPc2d1	tektonický
procesů	proces	k1gInPc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Mars	Mars	k1gInSc1	Mars
má	mít	k5eAaImIp3nS	mít
oproti	oproti	k7c3	oproti
Zemi	zem	k1gFnSc3	zem
zhruba	zhruba	k6eAd1	zhruba
čtvrtinovou	čtvrtinový	k2eAgFnSc4d1	čtvrtinová
plochu	plocha	k1gFnSc4	plocha
povrchu	povrch	k1gInSc2	povrch
a	a	k8xC	a
přibližně	přibližně	k6eAd1	přibližně
desetinovou	desetinový	k2eAgFnSc4d1	desetinová
hmotnost	hmotnost	k1gFnSc4	hmotnost
(	(	kIx(	(
<g/>
1,448	[number]	k4	1,448
<g/>
×	×	k?	×
<g/>
108	[number]	k4	108
km	km	kA	km
<g/>
2	[number]	k4	2
a	a	k8xC	a
6,418	[number]	k4	6,418
<g/>
5	[number]	k4	5
<g/>
×	×	k?	×
<g/>
1023	[number]	k4	1023
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnSc1d1	střední
hustota	hustota	k1gFnSc1	hustota
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
3933	[number]	k4	3933
kg	kg	kA	kg
<g/>
.	.	kIx.	.
<g/>
m	m	kA	m
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
Marsu	Mars	k1gInSc2	Mars
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
znatelně	znatelně	k6eAd1	znatelně
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
Země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
vyvíjel	vyvíjet	k5eAaImAgMnS	vyvíjet
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
více	hodně	k6eAd2	hodně
místa	místo	k1gNnPc1	místo
<g/>
,	,	kIx,	,
a	a	k8xC	a
mohl	moct	k5eAaImAgInS	moct
tak	tak	k6eAd1	tak
nasbírat	nasbírat	k5eAaPmF	nasbírat
více	hodně	k6eAd2	hodně
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
paradoxů	paradox	k1gInPc2	paradox
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
není	být	k5eNaImIp3nS	být
dobře	dobře	k6eAd1	dobře
vysvětlen	vysvětlen	k2eAgInSc1d1	vysvětlen
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
teorií	teorie	k1gFnPc2	teorie
by	by	kYmCp3nP	by
to	ten	k3xDgNnSc4	ten
mohlo	moct	k5eAaImAgNnS	moct
souviset	souviset	k5eAaImF	souviset
s	s	k7c7	s
dávnou	dávný	k2eAgFnSc7d1	dávná
migrací	migrace	k1gFnSc7	migrace
Jupiteru	Jupiter	k1gInSc2	Jupiter
sluneční	sluneční	k2eAgFnSc7d1	sluneční
soustavou	soustava	k1gFnSc7	soustava
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tato	tento	k3xDgFnSc1	tento
obří	obří	k2eAgFnSc1d1	obří
planeta	planeta	k1gFnSc1	planeta
mohla	moct	k5eAaImAgFnS	moct
krátce	krátce	k6eAd1	krátce
navštívit	navštívit	k5eAaPmF	navštívit
zónu	zóna	k1gFnSc4	zóna
terestrických	terestrický	k2eAgFnPc2d1	terestrická
planet	planeta	k1gFnPc2	planeta
a	a	k8xC	a
vymést	vymést	k5eAaPmF	vymést
odsud	odsud	k6eAd1	odsud
část	část	k1gFnSc4	část
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Sluneční	sluneční	k2eAgInSc1d1	sluneční
den	den	k1gInSc1	den
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
podobně	podobně	k6eAd1	podobně
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
jako	jako	k9	jako
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
(	(	kIx(	(
<g/>
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
39	[number]	k4	39
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
35,244	[number]	k4	35,244
sekund	sekunda	k1gFnPc2	sekunda
<g/>
)	)	kIx)	)
a	a	k8xC	a
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
Sol	sol	k1gNnSc2	sol
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geologie	geologie	k1gFnSc2	geologie
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgNnSc4d1	přesné
geologické	geologický	k2eAgNnSc4d1	geologické
složení	složení	k1gNnSc4	složení
planety	planeta	k1gFnSc2	planeta
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
základě	základ	k1gInSc6	základ
astronomických	astronomický	k2eAgNnPc2d1	astronomické
pozorování	pozorování	k1gNnPc2	pozorování
a	a	k8xC	a
průzkumu	průzkum	k1gInSc6	průzkum
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
meteoritů	meteorit	k1gInPc2	meteorit
z	z	k7c2	z
Marsu	Mars	k1gInSc2	Mars
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
nalezeny	naleznout	k5eAaPmNgInP	naleznout
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
soudí	soudit	k5eAaImIp3nS	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
povrch	povrch	k1gInSc1	povrch
Marsu	Mars	k1gInSc2	Mars
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
hornin	hornina	k1gFnPc2	hornina
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
čedičů	čedič	k1gInPc2	čedič
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
pozemským	pozemský	k2eAgInPc3d1	pozemský
čedičům	čedič	k1gInPc3	čedič
jsou	být	k5eAaImIp3nP	být
některé	některý	k3yIgFnSc3	některý
oblasti	oblast	k1gFnSc3	oblast
obohaceny	obohatit	k5eAaPmNgInP	obohatit
o	o	k7c4	o
křemičitanovou	křemičitanový	k2eAgFnSc4d1	křemičitanová
složku	složka	k1gFnSc4	složka
<g/>
,	,	kIx,	,
podobající	podobající	k2eAgFnSc4d1	podobající
se	se	k3xPyFc4	se
až	až	k9	až
pozemským	pozemský	k2eAgInPc3d1	pozemský
andezitům	andezit	k1gInPc3	andezit
(	(	kIx(	(
<g/>
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
i	i	k9	i
sopečným	sopečný	k2eAgNnSc7d1	sopečné
sklem	sklo	k1gNnSc7	sklo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pozorování	pozorování	k1gNnSc6	pozorování
je	být	k5eAaImIp3nS	být
planeta	planeta	k1gFnSc1	planeta
načervenalá	načervenalý	k2eAgFnSc1d1	načervenalá
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
pokrytím	pokrytí	k1gNnSc7	pokrytí
celého	celý	k2eAgInSc2d1	celý
povrchu	povrch	k1gInSc2	povrch
planety	planeta	k1gFnSc2	planeta
oxidem	oxid	k1gInSc7	oxid
železitým	železitý	k2eAgInSc7d1	železitý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Marsu	Mars	k1gInSc2	Mars
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
globální	globální	k2eAgNnSc4d1	globální
magnetické	magnetický	k2eAgNnSc4d1	magnetické
pole	pole	k1gNnSc4	pole
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
některé	některý	k3yIgFnPc1	některý
oblasti	oblast	k1gFnPc1	oblast
planety	planeta	k1gFnSc2	planeta
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
trvalou	trvalý	k2eAgFnSc4d1	trvalá
magnetizaci	magnetizace	k1gFnSc4	magnetizace
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
pro	pro	k7c4	pro
hypotézu	hypotéza	k1gFnSc4	hypotéza
<g/>
,	,	kIx,	,
že	že	k8xS	že
historické	historický	k2eAgNnSc1d1	historické
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
bylo	být	k5eAaImAgNnS	být
globálního	globální	k2eAgInSc2d1	globální
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
se	se	k3xPyFc4	se
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
tekutém	tekutý	k2eAgInSc6d1	tekutý
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
není	být	k5eNaImIp3nS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
desková	deskový	k2eAgFnSc1d1	desková
tektonika	tektonika	k1gFnSc1	tektonika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
vývoje	vývoj	k1gInSc2	vývoj
planety	planeta	k1gFnSc2	planeta
<g/>
)	)	kIx)	)
však	však	k9	však
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
část	část	k1gFnSc1	část
kůry	kůra	k1gFnSc2	kůra
mobilní	mobilní	k2eAgFnSc1d1	mobilní
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
by	by	kYmCp3nP	by
pozorované	pozorovaný	k2eAgFnPc1d1	pozorovaná
paleomagnetické	paleomagnetický	k2eAgFnPc1d1	paleomagnetický
anomálie	anomálie	k1gFnPc1	anomálie
mohly	moct	k5eAaImAgFnP	moct
souviset	souviset	k5eAaImF	souviset
s	s	k7c7	s
tvorbou	tvorba	k1gFnSc7	tvorba
nové	nový	k2eAgFnSc2d1	nová
kůry	kůra	k1gFnSc2	kůra
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
zemských	zemský	k2eAgInPc2d1	zemský
středooceánských	středooceánský	k2eAgInPc2d1	středooceánský
hřbetů	hřbet	k1gInPc2	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
faktu	fakt	k1gInSc3	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
nebyly	být	k5eNaImAgInP	být
prováděny	prováděn	k2eAgInPc1d1	prováděn
podrobné	podrobný	k2eAgInPc1d1	podrobný
geologické	geologický	k2eAgInPc1d1	geologický
průzkumy	průzkum	k1gInPc1	průzkum
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
současné	současný	k2eAgInPc4d1	současný
poznatky	poznatek	k1gInPc4	poznatek
o	o	k7c6	o
planetě	planeta	k1gFnSc6	planeta
a	a	k8xC	a
její	její	k3xOp3gFnSc3	její
vnitřní	vnitřní	k2eAgFnSc3d1	vnitřní
stavbě	stavba	k1gFnSc3	stavba
velmi	velmi	k6eAd1	velmi
slabé	slabý	k2eAgFnPc1d1	slabá
a	a	k8xC	a
založeny	založit	k5eAaPmNgFnP	založit
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
srovnáních	srovnání	k1gNnPc6	srovnání
se	s	k7c7	s
Zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
teoretických	teoretický	k2eAgInPc6d1	teoretický
modelech	model	k1gInPc6	model
založených	založený	k2eAgInPc2d1	založený
na	na	k7c6	na
nepřímých	přímý	k2eNgNnPc6d1	nepřímé
měřeních	měření	k1gNnPc6	měření
pořízených	pořízený	k2eAgFnPc2d1	pořízená
automatickými	automatický	k2eAgFnPc7d1	automatická
sondami	sonda	k1gFnPc7	sonda
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
kůrou	kůra	k1gFnSc7	kůra
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
nachází	nacházet	k5eAaImIp3nS	nacházet
plášť	plášť	k1gInSc4	plášť
primárně	primárně	k6eAd1	primárně
tvořený	tvořený	k2eAgInSc4d1	tvořený
olivínem	olivín	k1gInSc7	olivín
a	a	k8xC	a
spinelem	spinel	k1gInSc7	spinel
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
planeta	planeta	k1gFnSc1	planeta
má	mít	k5eAaImIp3nS	mít
žhavé	žhavý	k2eAgNnSc1d1	žhavé
<g/>
,	,	kIx,	,
zčásti	zčásti	k6eAd1	zčásti
tekuté	tekutý	k2eAgNnSc4d1	tekuté
jádro	jádro	k1gNnSc4	jádro
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
1480	[number]	k4	1480
km	km	kA	km
(	(	kIx(	(
<g/>
jiný	jiný	k2eAgInSc1d1	jiný
zdroj	zdroj	k1gInSc1	zdroj
uvádí	uvádět	k5eAaImIp3nS	uvádět
1300	[number]	k4	1300
až	až	k9	až
1700	[number]	k4	1700
km	km	kA	km
<g/>
)	)	kIx)	)
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
složené	složený	k2eAgNnSc1d1	složené
převážně	převážně	k6eAd1	převážně
ze	z	k7c2	z
železa	železo	k1gNnSc2	železo
s	s	k7c7	s
15	[number]	k4	15
-	-	kIx~	-
17	[number]	k4	17
váhových	váhový	k2eAgFnPc2d1	váhová
%	%	kIx~	%
příměsí	příměs	k1gFnPc2	příměs
síry	síra	k1gFnSc2	síra
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
až	až	k9	až
dvakrát	dvakrát	k6eAd1	dvakrát
více	hodně	k6eAd2	hodně
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
obsah	obsah	k1gInSc1	obsah
síry	síra	k1gFnSc2	síra
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
nepanuje	panovat	k5eNaImIp3nS	panovat
obecná	obecný	k2eAgFnSc1d1	obecná
shoda	shoda	k1gFnSc1	shoda
mezi	mezi	k7c7	mezi
vědci	vědec	k1gMnPc7	vědec
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
je	být	k5eAaImIp3nS	být
jádro	jádro	k1gNnSc1	jádro
částečně	částečně	k6eAd1	částečně
tekuté	tekutý	k2eAgNnSc4d1	tekuté
či	či	k8xC	či
pevné	pevný	k2eAgNnSc4d1	pevné
a	a	k8xC	a
obě	dva	k4xCgFnPc1	dva
dvě	dva	k4xCgFnPc1	dva
hypotézy	hypotéza	k1gFnPc1	hypotéza
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
zvažovány	zvažovat	k5eAaImNgInP	zvažovat
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
je	být	k5eAaImIp3nS	být
obklopeno	obklopit	k5eAaPmNgNnS	obklopit
pláštěm	plášť	k1gInSc7	plášť
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
aktivita	aktivita	k1gFnSc1	aktivita
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
tepelným	tepelný	k2eAgInSc7d1	tepelný
vývojem	vývoj	k1gInSc7	vývoj
dala	dát	k5eAaPmAgFnS	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
většině	většina	k1gFnSc3	většina
tektonických	tektonický	k2eAgInPc2d1	tektonický
a	a	k8xC	a
vulkanických	vulkanický	k2eAgInPc2d1	vulkanický
útvarů	útvar	k1gInPc2	útvar
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
aktivita	aktivita	k1gFnSc1	aktivita
minimální	minimální	k2eAgFnSc1d1	minimální
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
hlubších	hluboký	k2eAgFnPc6d2	hlubší
částech	část	k1gFnPc6	část
pláště	plášť	k1gInSc2	plášť
může	moct	k5eAaImIp3nS	moct
plášťová	plášťový	k2eAgFnSc1d1	plášťová
konvekce	konvekce	k1gFnSc1	konvekce
stále	stále	k6eAd1	stále
probíhat	probíhat	k5eAaImF	probíhat
<g/>
.	.	kIx.	.
</s>
<s>
Nejsvrchnější	svrchní	k2eAgFnSc1d3	nejsvrchnější
část	část	k1gFnSc1	část
pláště	plášť	k1gInSc2	plášť
tvoří	tvořit	k5eAaImIp3nS	tvořit
kůra	kůra	k1gFnSc1	kůra
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
průměrná	průměrný	k2eAgFnSc1d1	průměrná
mocnost	mocnost	k1gFnSc1	mocnost
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
38	[number]	k4	38
km	km	kA	km
až	až	k9	až
62	[number]	k4	62
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Povrch	povrch	k7c2wR	povrch
Marsu	Mars	k1gInSc2	Mars
a	a	k8xC	a
Vulkanismus	vulkanismus	k1gInSc1	vulkanismus
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
všeobecně	všeobecně	k6eAd1	všeobecně
věřilo	věřit	k5eAaImAgNnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
polární	polární	k2eAgFnPc1d1	polární
čepičky	čepička	k1gFnPc1	čepička
Marsu	Mars	k1gInSc2	Mars
jsou	být	k5eAaImIp3nP	být
složené	složený	k2eAgInPc1d1	složený
ze	z	k7c2	z
zmrzlé	zmrzlý	k2eAgFnSc2d1	zmrzlá
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
průzkumu	průzkum	k1gInSc2	průzkum
kosmickými	kosmický	k2eAgFnPc7d1	kosmická
sondami	sonda	k1gFnPc7	sonda
se	se	k3xPyFc4	se
ale	ale	k9	ale
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mars	Mars	k1gInSc1	Mars
má	mít	k5eAaImIp3nS	mít
slabou	slabý	k2eAgFnSc4d1	slabá
atmosféru	atmosféra	k1gFnSc4	atmosféra
složenou	složený	k2eAgFnSc4d1	složená
především	především	k9	především
z	z	k7c2	z
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
s	s	k7c7	s
pouze	pouze	k6eAd1	pouze
malou	malý	k2eAgFnSc7d1	malá
příměsí	příměs	k1gFnSc7	příměs
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
předpokládala	předpokládat	k5eAaImAgFnS	předpokládat
v	v	k7c6	v
polárních	polární	k2eAgFnPc6d1	polární
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Atmosférický	atmosférický	k2eAgInSc1d1	atmosférický
tlak	tlak	k1gInSc1	tlak
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
700	[number]	k4	700
Pa	Pa	kA	Pa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
tohoto	tento	k3xDgNnSc2	tento
zjištění	zjištění	k1gNnSc2	zjištění
byl	být	k5eAaImAgInS	být
následně	následně	k6eAd1	následně
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
model	model	k1gInSc1	model
atmosféry	atmosféra	k1gFnSc2	atmosféra
Marsu	Mars	k1gInSc2	Mars
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgNnSc2	který
vyplynulo	vyplynout	k5eAaPmAgNnS	vyplynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
dostatečně	dostatečně	k6eAd1	dostatečně
nízké	nízký	k2eAgFnPc1d1	nízká
teploty	teplota	k1gFnPc1	teplota
způsobily	způsobit	k5eAaPmAgFnP	způsobit
zkondenzování	zkondenzování	k1gNnSc4	zkondenzování
a	a	k8xC	a
zmrznutí	zmrznutí	k1gNnSc1	zmrznutí
samotného	samotný	k2eAgMnSc2d1	samotný
CO2	CO2	k1gMnSc2	CO2
na	na	k7c6	na
pólech	pól	k1gInPc6	pól
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomuto	tento	k3xDgInSc3	tento
periodickému	periodický	k2eAgInSc3d1	periodický
ději	děj	k1gInSc3	děj
(	(	kIx(	(
<g/>
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
se	se	k3xPyFc4	se
střídají	střídat	k5eAaImIp3nP	střídat
roční	roční	k2eAgNnSc4d1	roční
období	období	k1gNnSc4	období
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
)	)	kIx)	)
dochází	docházet	k5eAaImIp3nS	docházet
také	také	k9	také
k	k	k7c3	k
významné	významný	k2eAgFnSc3d1	významná
změně	změna	k1gFnSc3	změna
tlaku	tlak	k1gInSc2	tlak
během	během	k7c2	během
roku	rok	k1gInSc2	rok
až	až	k9	až
o	o	k7c4	o
20	[number]	k4	20
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc4d1	další
podrobné	podrobný	k2eAgNnSc4d1	podrobné
zkoumání	zkoumání	k1gNnSc4	zkoumání
nicméně	nicméně	k8xC	nicméně
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
póly	pól	k1gInPc7	pól
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
vodního	vodní	k2eAgInSc2d1	vodní
i	i	k8xC	i
suchého	suchý	k2eAgInSc2d1	suchý
ledu	led	k1gInSc2	led
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
i	i	k9	i
CO	co	k6eAd1	co
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pozorovatele	pozorovatel	k1gMnPc4	pozorovatel
mimo	mimo	k7c4	mimo
planetu	planeta	k1gFnSc4	planeta
má	mít	k5eAaImIp3nS	mít
Mars	Mars	k1gInSc1	Mars
oranžovočervenou	oranžovočervený	k2eAgFnSc4d1	oranžovočervená
barvu	barva	k1gFnSc4	barva
nebo	nebo	k8xC	nebo
růžovou	růžový	k2eAgFnSc4d1	růžová
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
bělavými	bělavý	k2eAgFnPc7d1	bělavá
oblastmi	oblast	k1gFnPc7	oblast
polárních	polární	k2eAgFnPc2d1	polární
čepiček	čepička	k1gFnPc2	čepička
<g/>
.	.	kIx.	.
</s>
<s>
Oblasti	oblast	k1gFnPc1	oblast
s	s	k7c7	s
nižším	nízký	k2eAgInSc7d2	nižší
albedem	albed	k1gInSc7	albed
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
při	při	k7c6	při
pozorování	pozorování	k1gNnSc6	pozorování
šedě	šedě	k6eAd1	šedě
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
červených	červený	k2eAgFnPc6d1	červená
oblastech	oblast	k1gFnPc6	oblast
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
rozličné	rozličný	k2eAgFnPc1d1	rozličná
světlé	světlý	k2eAgFnPc1d1	světlá
a	a	k8xC	a
tmavé	tmavý	k2eAgFnPc1d1	tmavá
plochy	plocha	k1gFnPc1	plocha
s	s	k7c7	s
nazelenalou	nazelenalý	k2eAgFnSc7d1	nazelenalá
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Tmavé	tmavý	k2eAgFnPc1d1	tmavá
plochy	plocha	k1gFnPc1	plocha
ovšem	ovšem	k9	ovšem
nejsou	být	k5eNaImIp3nP	být
oceány	oceán	k1gInPc1	oceán
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
nemůže	moct	k5eNaImIp3nS	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
v	v	k7c6	v
tekutém	tekutý	k2eAgInSc6d1	tekutý
stavu	stav	k1gInSc6	stav
kvůli	kvůli	k7c3	kvůli
nízkému	nízký	k2eAgInSc3d1	nízký
atmosférickému	atmosférický	k2eAgInSc3d1	atmosférický
tlaku	tlak	k1gInSc3	tlak
(	(	kIx(	(
<g/>
~	~	kIx~	~
<g/>
700	[number]	k4	700
Pa	Pa	kA	Pa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
jasnosti	jasnost	k1gFnSc6	jasnost
povrchu	povrch	k1gInSc2	povrch
jsou	být	k5eAaImIp3nP	být
způsobené	způsobený	k2eAgNnSc1d1	způsobené
rozdílným	rozdílný	k2eAgInSc7d1	rozdílný
druhem	druh	k1gInSc7	druh
povrchového	povrchový	k2eAgInSc2d1	povrchový
materiálu	materiál	k1gInSc2	materiál
<g/>
:	:	kIx,	:
světlejší	světlý	k2eAgFnPc1d2	světlejší
naoranžovělé	naoranžovělý	k2eAgFnPc1d1	naoranžovělá
oblasti	oblast	k1gFnPc1	oblast
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
prach	prach	k1gInSc4	prach
a	a	k8xC	a
písek	písek	k1gInSc4	písek
bohatý	bohatý	k2eAgInSc4d1	bohatý
na	na	k7c4	na
oxid	oxid	k1gInSc4	oxid
železitý	železitý	k2eAgInSc4d1	železitý
<g/>
;	;	kIx,	;
tmavší	tmavý	k2eAgFnPc1d2	tmavší
plochy	plocha	k1gFnPc1	plocha
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
kamenitější	kamenitý	k2eAgInPc1d2	kamenitější
a	a	k8xC	a
skalnatější	skalnatý	k2eAgInPc1d2	skalnatější
regiony	region	k1gInPc1	region
<g/>
.	.	kIx.	.
</s>
<s>
Tvary	tvar	k1gInPc1	tvar
a	a	k8xC	a
rozměry	rozměr	k1gInPc1	rozměr
těchto	tento	k3xDgFnPc2	tento
oblastí	oblast	k1gFnPc2	oblast
mění	měnit	k5eAaImIp3nS	měnit
se	s	k7c7	s
vlivem	vliv	k1gInSc7	vliv
občasných	občasný	k2eAgInPc2d1	občasný
silných	silný	k2eAgInPc2d1	silný
větrů	vítr	k1gInPc2	vítr
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
prach	prach	k1gInSc1	prach
přemisťují	přemisťovat	k5eAaImIp3nP	přemisťovat
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
Marsu	Mars	k1gInSc2	Mars
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
různorodý	různorodý	k2eAgInSc1d1	různorodý
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
polokoule	polokoule	k1gFnSc1	polokoule
s	s	k7c7	s
víceméně	víceméně	k9	víceméně
hornatou	hornatý	k2eAgFnSc7d1	hornatá
krajinou	krajina	k1gFnSc7	krajina
je	být	k5eAaImIp3nS	být
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
krátery	kráter	k1gInPc7	kráter
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
jsou	být	k5eAaImIp3nP	být
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
rovné	rovný	k2eAgFnPc4d1	rovná
pláně	pláň	k1gFnPc4	pláň
zalité	zalitý	k2eAgFnPc4d1	zalitá
lávou	láva	k1gFnSc7	láva
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
je	být	k5eAaImIp3nS	být
povrch	povrch	k1gInSc1	povrch
Marsu	Mars	k1gInSc2	Mars
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
skalnatými	skalnatý	k2eAgInPc7d1	skalnatý
a	a	k8xC	a
nebo	nebo	k8xC	nebo
kamenitými	kamenitý	k2eAgInPc7d1	kamenitý
útvary	útvar	k1gInPc7	útvar
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
místy	místy	k6eAd1	místy
překryty	překryt	k2eAgFnPc1d1	překryta
prachem	prach	k1gInSc7	prach
a	a	k8xC	a
písečnými	písečný	k2eAgFnPc7d1	písečná
dunami	duna	k1gFnPc7	duna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
kráterů	kráter	k1gInPc2	kráter
<g/>
,	,	kIx,	,
koryt	koryto	k1gNnPc2	koryto
<g/>
,	,	kIx,	,
kaňonů	kaňon	k1gInPc2	kaňon
a	a	k8xC	a
sopek	sopka	k1gFnPc2	sopka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
známá	známý	k2eAgFnSc1d1	známá
hora	hora	k1gFnSc1	hora
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
-	-	kIx~	-
štítová	štítový	k2eAgFnSc1d1	štítová
sopka	sopka	k1gFnSc1	sopka
Olympus	Olympus	k1gMnSc1	Olympus
Mons	Mons	k1gInSc1	Mons
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výšky	výška	k1gFnPc4	výška
přes	přes	k7c4	přes
21	[number]	k4	21
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rovníkové	rovníkový	k2eAgFnSc6d1	Rovníková
oblasti	oblast	k1gFnSc6	oblast
Marsu	Mars	k1gInSc2	Mars
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
obrovský	obrovský	k2eAgInSc1d1	obrovský
kaňon	kaňon	k1gInSc1	kaňon
Valles	Valles	k1gInSc1	Valles
Marineris	Marineris	k1gInSc4	Marineris
<g/>
,	,	kIx,	,
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
4	[number]	k4	4
500	[number]	k4	500
km	km	kA	km
a	a	k8xC	a
hluboký	hluboký	k2eAgInSc1d1	hluboký
7	[number]	k4	7
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Objevila	objevit	k5eAaPmAgFnS	objevit
ho	on	k3xPp3gNnSc4	on
sonda	sonda	k1gFnSc1	sonda
Mariner	Mariner	k1gInSc1	Mariner
9	[number]	k4	9
mapující	mapující	k2eAgInSc1d1	mapující
Mars	Mars	k1gInSc1	Mars
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1971	[number]	k4	1971
<g/>
-	-	kIx~	-
<g/>
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yQgFnSc2	který
byl	být	k5eAaImAgInS	být
kaňon	kaňon	k1gInSc1	kaňon
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
<g/>
.	.	kIx.	.
</s>
<s>
Průzkum	průzkum	k1gInSc1	průzkum
sondami	sonda	k1gFnPc7	sonda
Viking	Viking	k1gMnSc1	Viking
přinesl	přinést	k5eAaPmAgInS	přinést
i	i	k9	i
snímky	snímek	k1gInPc4	snímek
oblasti	oblast	k1gFnSc2	oblast
Cydonia	Cydonium	k1gNnSc2	Cydonium
Mensae	Mensa	k1gFnSc2	Mensa
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yQgInPc6	který
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
útvar	útvar	k1gInSc4	útvar
připomínající	připomínající	k2eAgInSc4d1	připomínající
lidskou	lidský	k2eAgFnSc4d1	lidská
tvář	tvář	k1gFnSc4	tvář
obrácenou	obrácený	k2eAgFnSc4d1	obrácená
k	k	k7c3	k
nebi	nebe	k1gNnSc3	nebe
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
skalní	skalní	k2eAgInSc1d1	skalní
útvar	útvar	k1gInSc1	útvar
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
začal	začít	k5eAaPmAgInS	začít
označovat	označovat	k5eAaImF	označovat
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
tvář	tvář	k1gFnSc1	tvář
z	z	k7c2	z
Marsu	Mars	k1gInSc2	Mars
<g/>
"	"	kIx"	"
a	a	k8xC	a
považoval	považovat	k5eAaImAgMnS	považovat
se	se	k3xPyFc4	se
za	za	k7c4	za
umělé	umělý	k2eAgNnSc4d1	umělé
dílo	dílo	k1gNnSc4	dílo
mimozemské	mimozemský	k2eAgFnSc2d1	mimozemská
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgInPc1d2	pozdější
kvalitnější	kvalitní	k2eAgInPc1d2	kvalitnější
snímky	snímek	k1gInPc1	snímek
ale	ale	k9	ale
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
hru	hra	k1gFnSc4	hra
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
stínu	stín	k1gInSc2	stín
na	na	k7c6	na
obyčejném	obyčejný	k2eAgInSc6d1	obyčejný
erodovaném	erodovaný	k2eAgInSc6d1	erodovaný
skalním	skalní	k2eAgInSc6d1	skalní
masívu	masív	k1gInSc6	masív
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
povrchových	povrchový	k2eAgInPc2d1	povrchový
útvarů	útvar	k1gInPc2	útvar
Marsu	Mars	k1gInSc2	Mars
je	být	k5eAaImIp3nS	být
složitější	složitý	k2eAgMnSc1d2	složitější
než	než	k8xS	než
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Merkuru	Merkur	k1gInSc2	Merkur
a	a	k8xC	a
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
názvosloví	názvosloví	k1gNnSc1	názvosloví
vznikalo	vznikat	k5eAaImAgNnS	vznikat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
již	již	k6eAd1	již
od	od	k7c2	od
prvních	první	k4xOgNnPc2	první
pozorování	pozorování	k1gNnPc2	pozorování
prováděných	prováděný	k2eAgNnPc2d1	prováděné
italským	italský	k2eAgMnSc7d1	italský
astronomem	astronom	k1gMnSc7	astronom
Giovannim	Giovannima	k1gFnPc2	Giovannima
Schiaparellim	Schiaparellim	k1gInSc1	Schiaparellim
roku	rok	k1gInSc2	rok
1877	[number]	k4	1877
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
během	během	k7c2	během
pozorování	pozorování	k1gNnSc2	pozorování
začal	začít	k5eAaPmAgInS	začít
pro	pro	k7c4	pro
útvary	útvar	k1gInPc4	útvar
používat	používat	k5eAaImF	používat
jména	jméno	k1gNnPc4	jméno
známé	známá	k1gFnSc2	známá
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc2	který
spojoval	spojovat	k5eAaImAgMnS	spojovat
s	s	k7c7	s
mytologickými	mytologický	k2eAgInPc7d1	mytologický
názvy	název	k1gInPc7	název
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
práci	práce	k1gFnSc6	práce
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Schiaparelli	Schiaparelle	k1gFnSc4	Schiaparelle
započal	započnout	k5eAaPmAgInS	započnout
<g/>
,	,	kIx,	,
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
i	i	k9	i
Eugene	Eugen	k1gInSc5	Eugen
Antoniadi	Antoniad	k1gMnPc5	Antoniad
<g/>
,	,	kIx,	,
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
byly	být	k5eAaImAgInP	být
pojmenovány	pojmenovat	k5eAaPmNgInP	pojmenovat
ale	ale	k8xC	ale
výrazné	výrazný	k2eAgInPc1d1	výrazný
albedové	albedový	k2eAgInPc1d1	albedový
útvary	útvar	k1gInPc1	útvar
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ne	ne	k9	ne
nutně	nutně	k6eAd1	nutně
odpovídaly	odpovídat	k5eAaImAgFnP	odpovídat
objektům	objekt	k1gInPc3	objekt
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
podrobnému	podrobný	k2eAgNnSc3d1	podrobné
zmapování	zmapování	k1gNnSc3	zmapování
povrchu	povrch	k1gInSc2	povrch
Marsu	Mars	k1gInSc2	Mars
pomocí	pomocí	k7c2	pomocí
sondy	sonda	k1gFnSc2	sonda
Mariner	Mariner	k1gInSc1	Mariner
9	[number]	k4	9
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
přineslo	přinést	k5eAaPmAgNnS	přinést
velkou	velký	k2eAgFnSc4d1	velká
revizi	revize	k1gFnSc4	revize
názvů	název	k1gInPc2	název
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
úpravu	úprava	k1gFnSc4	úprava
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
je	být	k5eAaImIp3nS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
současné	současný	k2eAgNnSc1d1	současné
názvosloví	názvosloví	k1gNnSc1	názvosloví
<g/>
.	.	kIx.	.
</s>
<s>
Nulová	nulový	k2eAgFnSc1d1	nulová
výška	výška	k1gFnSc1	výška
<g/>
:	:	kIx,	:
Protože	protože	k8xS	protože
Mars	Mars	k1gInSc1	Mars
nemá	mít	k5eNaImIp3nS	mít
žádné	žádný	k3yNgFnPc4	žádný
vodní	vodní	k2eAgFnPc4d1	vodní
plochy	plocha	k1gFnPc4	plocha
<g/>
,	,	kIx,	,
neexistuje	existovat	k5eNaImIp3nS	existovat
tedy	tedy	k9	tedy
ani	ani	k8xC	ani
žádná	žádný	k3yNgFnSc1	žádný
přirozená	přirozený	k2eAgFnSc1d1	přirozená
nulová	nulový	k2eAgFnSc1d1	nulová
výška	výška	k1gFnSc1	výška
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
Země	zem	k1gFnSc2	zem
hladina	hladina	k1gFnSc1	hladina
světového	světový	k2eAgInSc2d1	světový
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
od	od	k7c2	od
které	který	k3yQgFnSc2	který
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgFnP	moct
měřit	měřit	k5eAaImF	měřit
topografické	topografický	k2eAgFnSc2d1	topografická
výšky	výška	k1gFnSc2	výška
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
tedy	tedy	k9	tedy
zavedena	zaveden	k2eAgFnSc1d1	zavedena
umělá	umělý	k2eAgFnSc1d1	umělá
nulová	nulový	k2eAgFnSc1d1	nulová
výška	výška	k1gFnSc1	výška
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
do	do	k7c2	do
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc4	století
daná	daný	k2eAgNnPc4d1	dané
atmosferickým	atmosferický	k2eAgInSc7d1	atmosferický
tlakem	tlak	k1gInSc7	tlak
6,1	[number]	k4	6,1
mbar	mbara	k1gFnPc2	mbara
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
daná	daný	k2eAgFnSc1d1	daná
středním	střední	k2eAgInSc7d1	střední
gravitačním	gravitační	k2eAgInSc7d1	gravitační
potenciálem	potenciál	k1gInSc7	potenciál
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
rovníku	rovník	k1gInSc2	rovník
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Nultý	nultý	k4xOgInSc1	nultý
poledník	poledník	k1gInSc1	poledník
<g/>
:	:	kIx,	:
Rovník	rovník	k1gInSc1	rovník
Marsu	Mars	k1gInSc2	Mars
je	být	k5eAaImIp3nS	být
dán	dát	k5eAaPmNgInS	dát
rotací	rotace	k1gFnSc7	rotace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nultý	nultý	k4xOgInSc1	nultý
poledník	poledník	k1gInSc1	poledník
byl	být	k5eAaImAgInS	být
určen	určit	k5eAaPmNgInS	určit
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
dohodou	dohoda	k1gFnSc7	dohoda
<g/>
,	,	kIx,	,
že	že	k8xS	že
prochází	procházet	k5eAaImIp3nS	procházet
určitým	určitý	k2eAgInSc7d1	určitý
konkrétním	konkrétní	k2eAgInSc7d1	konkrétní
bodem	bod	k1gInSc7	bod
<g/>
.	.	kIx.	.
</s>
<s>
Astronomové	astronom	k1gMnPc1	astronom
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
si	se	k3xPyFc3	se
za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
bod	bod	k1gInSc4	bod
zvolili	zvolit	k5eAaPmAgMnP	zvolit
s	s	k7c7	s
poměrně	poměrně	k6eAd1	poměrně
velkou	velký	k2eAgFnSc7d1	velká
nepřesností	nepřesnost	k1gFnSc7	nepřesnost
kruhový	kruhový	k2eAgInSc4d1	kruhový
útvar	útvar	k1gInSc4	útvar
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
označovaný	označovaný	k2eAgMnSc1d1	označovaný
jako	jako	k8xS	jako
Sinus	sinus	k1gInSc1	sinus
Meridiani	Meridian	k1gMnPc1	Meridian
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
sonda	sonda	k1gFnSc1	sonda
Mariner	Mariner	k1gInSc1	Mariner
9	[number]	k4	9
získala	získat	k5eAaPmAgFnS	získat
první	první	k4xOgInPc4	první
podrobnější	podrobný	k2eAgInPc4d2	podrobnější
snímky	snímek	k1gInPc4	snímek
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
určeno	určit	k5eAaPmNgNnS	určit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nultý	nultý	k4xOgInSc1	nultý
poledník	poledník	k1gInSc1	poledník
prochází	procházet	k5eAaImIp3nS	procházet
malým	malý	k2eAgInSc7d1	malý
kráterem	kráter	k1gInSc7	kráter
Airy-	Airy-	k1gFnSc1	Airy-
<g/>
0	[number]	k4	0
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Sinus	sinus	k1gInSc1	sinus
Meridiani	Meridian	k1gMnPc1	Meridian
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Stratigrafie	stratigrafie	k1gFnSc2	stratigrafie
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Stratigrafie	stratigrafie	k1gFnSc1	stratigrafie
Marsu	Mars	k1gInSc2	Mars
je	být	k5eAaImIp3nS	být
vědní	vědní	k2eAgFnSc1d1	vědní
disciplína	disciplína	k1gFnSc1	disciplína
v	v	k7c6	v
planetologii	planetologie	k1gFnSc6	planetologie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
rozčlenit	rozčlenit	k5eAaPmF	rozčlenit
základní	základní	k2eAgFnPc4d1	základní
stratigrafické	stratigrafický	k2eAgFnPc4d1	stratigrafická
jednotky	jednotka	k1gFnPc4	jednotka
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
fotografií	fotografia	k1gFnPc2	fotografia
sondy	sonda	k1gFnSc2	sonda
Viking	Viking	k1gMnSc1	Viking
ze	z	k7c2	z
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
vyčleněny	vyčlenit	k5eAaPmNgFnP	vyčlenit
čtyři	čtyři	k4xCgFnPc1	čtyři
základní	základní	k2eAgFnPc1d1	základní
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
získávání	získávání	k1gNnSc3	získávání
stále	stále	k6eAd1	stále
nových	nový	k2eAgNnPc2d1	nové
dat	datum	k1gNnPc2	datum
ze	z	k7c2	z
sond	sonda	k1gFnPc2	sonda
z	z	k7c2	z
posledního	poslední	k2eAgNnSc2d1	poslední
desetiletí	desetiletí	k1gNnSc2	desetiletí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
kolem	kolem	k7c2	kolem
Marsu	Mars	k1gInSc2	Mars
obíhají	obíhat	k5eAaImIp3nP	obíhat
či	či	k8xC	či
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
jezdí	jezdit	k5eAaImIp3nP	jezdit
<g/>
,	,	kIx,	,
procházejí	procházet	k5eAaImIp3nP	procházet
podstatnou	podstatný	k2eAgFnSc7d1	podstatná
revizí	revize	k1gFnSc7	revize
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zatím	zatím	k6eAd1	zatím
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
získat	získat	k5eAaPmF	získat
geologické	geologický	k2eAgInPc4d1	geologický
vzorky	vzorek	k1gInPc4	vzorek
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
hornin	hornina	k1gFnPc2	hornina
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
celá	celý	k2eAgFnSc1d1	celá
stratigrafie	stratigrafie	k1gFnSc1	stratigrafie
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c4	na
pozorování	pozorování	k1gNnSc4	pozorování
svrchní	svrchní	k2eAgFnSc2d1	svrchní
vrstvy	vrstva	k1gFnSc2	vrstva
kůry	kůra	k1gFnSc2	kůra
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
na	na	k7c6	na
projevech	projev	k1gInPc6	projev
impaktů	impakt	k1gInPc2	impakt
cizích	cizí	k2eAgNnPc2d1	cizí
těles	těleso	k1gNnPc2	těleso
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Pozorováním	pozorování	k1gNnSc7	pozorování
kráterů	kráter	k1gInPc2	kráter
byly	být	k5eAaImAgFnP	být
vyčleněny	vyčlenit	k5eAaPmNgFnP	vyčlenit
čtyři	čtyři	k4xCgNnPc4	čtyři
základní	základní	k2eAgNnPc4d1	základní
historická	historický	k2eAgNnPc4d1	historické
období	období	k1gNnPc4	období
v	v	k7c6	v
geologické	geologický	k2eAgFnSc6d1	geologická
historii	historie	k1gFnSc6	historie
planety	planeta	k1gFnSc2	planeta
<g/>
:	:	kIx,	:
Pre-noachian	Preoachian	k1gMnSc1	Pre-noachian
<g/>
,	,	kIx,	,
noachian	noachian	k1gMnSc1	noachian
<g/>
,	,	kIx,	,
hesperian	hesperian	k1gMnSc1	hesperian
a	a	k8xC	a
amazonian	amazonian	k1gMnSc1	amazonian
<g/>
.	.	kIx.	.
</s>
<s>
Jednotka	jednotka	k1gFnSc1	jednotka
Noachian	Noachiana	k1gFnPc2	Noachiana
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
podle	podle	k7c2	podle
oblasti	oblast	k1gFnSc2	oblast
Noachis	Noachis	k1gFnSc1	Noachis
Terra	Terra	k1gFnSc1	Terra
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgNnPc4d1	charakteristické
vysokým	vysoký	k2eAgInSc7d1	vysoký
výskytem	výskyt	k1gInSc7	výskyt
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
různé	různý	k2eAgFnSc2d1	různá
velikosti	velikost	k1gFnSc2	velikost
podobající	podobající	k2eAgFnSc6d1	podobající
se	se	k3xPyFc4	se
měsíční	měsíční	k2eAgFnSc3d1	měsíční
krajině	krajina	k1gFnSc3	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
silným	silný	k2eAgNnSc7d1	silné
bombardováním	bombardování	k1gNnSc7	bombardování
povrchu	povrch	k1gInSc2	povrch
tělesy	těleso	k1gNnPc7	těleso
z	z	k7c2	z
vesmíru	vesmír	k1gInSc2	vesmír
a	a	k8xC	a
množstvím	množství	k1gNnSc7	množství
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Hesperian	Hesperian	k1gMnSc1	Hesperian
je	být	k5eAaImIp3nS	být
pojmenován	pojmenovat	k5eAaPmNgMnS	pojmenovat
dle	dle	k7c2	dle
oblasti	oblast	k1gFnSc2	oblast
Hesperia	Hesperium	k1gNnSc2	Hesperium
Planum	Planum	k1gInSc1	Planum
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
průměrné	průměrný	k2eAgNnSc1d1	průměrné
pokrytí	pokrytí	k1gNnSc1	pokrytí
impaktními	impaktní	k2eAgInPc7d1	impaktní
krátery	kráter	k1gInPc7	kráter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
významné	významný	k2eAgFnSc3d1	významná
sopečné	sopečný	k2eAgFnSc3d1	sopečná
činnosti	činnost	k1gFnSc3	činnost
a	a	k8xC	a
katastrofickým	katastrofický	k2eAgFnPc3d1	katastrofická
záplavám	záplava	k1gFnPc3	záplava
způsobených	způsobený	k2eAgFnPc2d1	způsobená
činností	činnost	k1gFnPc2	činnost
outflow	outflow	k?	outflow
channels	channelsa	k1gFnPc2	channelsa
<g/>
.	.	kIx.	.
</s>
<s>
Nejmladší	mladý	k2eAgFnSc1d3	nejmladší
jednotka	jednotka	k1gFnSc1	jednotka
Amazonian	Amazoniana	k1gFnPc2	Amazoniana
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
dle	dle	k7c2	dle
Amazonis	Amazonis	k1gFnSc2	Amazonis
Planitia	Planitium	k1gNnSc2	Planitium
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
období	období	k1gNnSc4	období
Amazonianu	Amazonian	k1gInSc2	Amazonian
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
nízký	nízký	k2eAgInSc1d1	nízký
počet	počet	k1gInSc4	počet
nových	nový	k2eAgInPc2d1	nový
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
a	a	k8xC	a
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
po	po	k7c6	po
projevech	projev	k1gInPc6	projev
ledovců	ledovec	k1gInPc2	ledovec
a	a	k8xC	a
výrazné	výrazný	k2eAgFnSc2d1	výrazná
sopečné	sopečný	k2eAgFnSc2d1	sopečná
činnosti	činnost	k1gFnSc2	činnost
například	například	k6eAd1	například
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Tharsis	Tharsis	k1gFnSc2	Tharsis
<g/>
.	.	kIx.	.
</s>
<s>
Marsovská	marsovský	k2eAgFnSc1d1	marsovská
historie	historie	k1gFnSc1	historie
(	(	kIx(	(
<g/>
V	v	k7c6	v
miliónech	milión	k4xCgInPc6	milión
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Atmosféra	atmosféra	k1gFnSc1	atmosféra
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Mars	Mars	k1gInSc1	Mars
má	mít	k5eAaImIp3nS	mít
dnes	dnes	k6eAd1	dnes
velmi	velmi	k6eAd1	velmi
řídkou	řídký	k2eAgFnSc4d1	řídká
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
není	být	k5eNaImIp3nS	být
schopná	schopný	k2eAgFnSc1d1	schopná
zadržovat	zadržovat	k5eAaImF	zadržovat
tepelnou	tepelný	k2eAgFnSc4d1	tepelná
výměnu	výměna	k1gFnSc4	výměna
mezi	mezi	k7c7	mezi
povrchem	povrch	k1gInSc7	povrch
a	a	k8xC	a
okolním	okolní	k2eAgInSc7d1	okolní
prostorem	prostor	k1gInSc7	prostor
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
velké	velký	k2eAgInPc4d1	velký
teplotní	teplotní	k2eAgInPc4d1	teplotní
rozdíly	rozdíl	k1gInPc4	rozdíl
během	během	k7c2	během
dne	den	k1gInSc2	den
a	a	k8xC	a
noci	noc	k1gFnSc2	noc
<g/>
.	.	kIx.	.
</s>
<s>
Tlak	tlak	k1gInSc1	tlak
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
600	[number]	k4	600
až	až	k9	až
1000	[number]	k4	1000
Pa	Pa	kA	Pa
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
až	až	k9	až
150	[number]	k4	150
<g/>
krát	krát	k6eAd1	krát
méně	málo	k6eAd2	málo
než	než	k8xS	než
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Země	zem	k1gFnSc2	zem
či	či	k8xC	či
jako	jako	k9	jako
přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c6	v
30	[number]	k4	30
km	km	kA	km
nad	nad	k7c7	nad
jejím	její	k3xOp3gInSc7	její
povrchem	povrch	k1gInSc7	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
ale	ale	k8xC	ale
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
sezónních	sezónní	k2eAgInPc6d1	sezónní
výkyvech	výkyv	k1gInPc6	výkyv
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
planeta	planeta	k1gFnSc1	planeta
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
a	a	k8xC	a
oddaluje	oddalovat	k5eAaImIp3nS	oddalovat
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
přibližně	přibližně	k6eAd1	přibližně
25	[number]	k4	25
%	%	kIx~	%
atmosférického	atmosférický	k2eAgInSc2d1	atmosférický
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
zmrzne	zmrznout	k5eAaPmIp3nS	zmrznout
na	na	k7c6	na
pólech	pól	k1gInPc6	pól
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
opět	opět	k6eAd1	opět
sublimuje	sublimovat	k5eAaBmIp3nS	sublimovat
a	a	k8xC	a
vrátí	vrátit	k5eAaPmIp3nS	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
(	(	kIx(	(
<g/>
95,32	[number]	k4	95,32
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
<g/>
:	:	kIx,	:
dusík	dusík	k1gInSc1	dusík
(	(	kIx(	(
<g/>
2,7	[number]	k4	2,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
argon	argon	k1gInSc1	argon
(	(	kIx(	(
<g/>
1,6	[number]	k4	1,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kyslík	kyslík	k1gInSc1	kyslík
(	(	kIx(	(
<g/>
0,13	[number]	k4	0,13
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oxid	oxid	k1gInSc1	oxid
uhelnatý	uhelnatý	k2eAgInSc1d1	uhelnatý
(	(	kIx(	(
<g/>
0,07	[number]	k4	0,07
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
vodní	vodní	k2eAgInPc1d1	vodní
páry	pár	k1gInPc1	pár
(	(	kIx(	(
<g/>
0,03	[number]	k4	0,03
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
sublimací	sublimace	k1gFnPc2	sublimace
z	z	k7c2	z
polárních	polární	k2eAgFnPc2d1	polární
čepiček	čepička	k1gFnPc2	čepička
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ostatní	ostatní	k2eAgInPc4d1	ostatní
plyny	plyn	k1gInPc4	plyn
vyskytující	vyskytující	k2eAgInPc4d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
ještě	ještě	k6eAd1	ještě
řadí	řadit	k5eAaImIp3nS	řadit
neon	neon	k1gInSc1	neon
<g/>
,	,	kIx,	,
krypton	krypton	k1gInSc1	krypton
<g/>
,	,	kIx,	,
xenon	xenon	k1gInSc1	xenon
<g/>
,	,	kIx,	,
ozón	ozón	k1gInSc1	ozón
a	a	k8xC	a
metan	metan	k1gInSc1	metan
(	(	kIx(	(
<g/>
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
možným	možný	k2eAgInSc7d1	možný
indikátorem	indikátor	k1gInSc7	indikátor
života	život	k1gInSc2	život
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
podléhá	podléhat	k5eAaImIp3nS	podléhat
rychlému	rychlý	k2eAgInSc3d1	rychlý
rozpadu	rozpad	k1gInSc3	rozpad
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
studie	studie	k1gFnSc1	studie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
metan	metan	k1gInSc1	metan
vzniká	vznikat	k5eAaImIp3nS	vznikat
jako	jako	k8xC	jako
výsledek	výsledek	k1gInSc1	výsledek
interakce	interakce	k1gFnSc1	interakce
UV	UV	kA	UV
záření	záření	k1gNnSc1	záření
se	s	k7c7	s
sloučeninami	sloučenina	k1gFnPc7	sloučenina
uhlíků	uhlík	k1gInPc2	uhlík
obsažených	obsažený	k2eAgInPc2d1	obsažený
v	v	k7c6	v
mikrometeoritech	mikrometeorit	k1gInPc6	mikrometeorit
a	a	k8xC	a
kosmickém	kosmický	k2eAgInSc6d1	kosmický
prachu	prach	k1gInSc6	prach
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
u	u	k7c2	u
povrchu	povrch	k1gInSc2	povrch
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
okolo	okolo	k7c2	okolo
210	[number]	k4	210
K	K	kA	K
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
63	[number]	k4	63
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Mars	Mars	k1gInSc4	Mars
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
velké	velký	k2eAgInPc1d1	velký
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
dnem	den	k1gInSc7	den
a	a	k8xC	a
nocí	noc	k1gFnSc7	noc
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rovníku	rovník	k1gInSc6	rovník
se	se	k3xPyFc4	se
teploty	teplota	k1gFnPc1	teplota
běžně	běžně	k6eAd1	běžně
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
od	od	k7c2	od
-	-	kIx~	-
do	do	k7c2	do
-	-	kIx~	-
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
a	a	k8xC	a
nad	nad	k7c4	nad
nulu	nula	k1gFnSc4	nula
se	se	k3xPyFc4	se
dostanou	dostat	k5eAaPmIp3nP	dostat
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
teplota	teplota	k1gFnSc1	teplota
povrchové	povrchový	k2eAgFnSc2d1	povrchová
vrstvy	vrstva	k1gFnSc2	vrstva
půdy	půda	k1gFnSc2	půda
může	moct	k5eAaImIp3nS	moct
někdy	někdy	k6eAd1	někdy
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
až	až	k9	až
+30	+30	k4	+30
°	°	k?	°
<g/>
C.	C.	kA	C.
I	i	k9	i
přes	přes	k7c4	přes
tyto	tento	k3xDgFnPc4	tento
občasně	občasně	k6eAd1	občasně
příznivé	příznivý	k2eAgFnPc4d1	příznivá
teploty	teplota	k1gFnPc4	teplota
nemůže	moct	k5eNaImIp3nS	moct
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
povrchu	povrch	k1gInSc2	povrch
existovat	existovat	k5eAaImF	existovat
kapalná	kapalný	k2eAgFnSc1d1	kapalná
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
okamžitě	okamžitě	k6eAd1	okamžitě
začala	začít	k5eAaPmAgFnS	začít
vypařovat	vypařovat	k5eAaImF	vypařovat
vlivem	vlivem	k7c2	vlivem
nízkého	nízký	k2eAgInSc2d1	nízký
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
okolo	okolo	k7c2	okolo
40	[number]	k4	40
až	až	k9	až
50	[number]	k4	50
km	km	kA	km
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
stálou	stálý	k2eAgFnSc4d1	stálá
teplotu	teplota	k1gFnSc4	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
přibližně	přibližně	k6eAd1	přibližně
130	[number]	k4	130
km	km	kA	km
začíná	začínat	k5eAaImIp3nS	začínat
ionosféra	ionosféra	k1gFnSc1	ionosféra
a	a	k8xC	a
vodíková	vodíkový	k2eAgFnSc1d1	vodíková
koróna	koróna	k1gFnSc1	koróna
planety	planeta	k1gFnSc2	planeta
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
až	až	k9	až
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
20	[number]	k4	20
000	[number]	k4	000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Podrobné	podrobný	k2eAgFnPc1d1	podrobná
znalosti	znalost	k1gFnPc1	znalost
o	o	k7c4	o
složení	složení	k1gNnSc4	složení
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
jejích	její	k3xOp3gFnPc6	její
změnách	změna	k1gFnPc6	změna
a	a	k8xC	a
o	o	k7c6	o
dlouhodobějším	dlouhodobý	k2eAgNnSc6d2	dlouhodobější
klimatu	klima	k1gNnSc6	klima
byly	být	k5eAaImAgFnP	být
získány	získat	k5eAaPmNgFnP	získat
díky	díky	k7c3	díky
několika	několik	k4yIc2	několik
sondám	sonda	k1gFnPc3	sonda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
přistály	přistát	k5eAaPmAgFnP	přistát
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Viking	Viking	k1gMnSc1	Viking
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
<g/>
,	,	kIx,	,
Spirit	Spirit	k1gInSc1	Spirit
<g/>
,	,	kIx,	,
Opportunity	Opportunit	k1gInPc1	Opportunit
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
které	který	k3yRgFnPc1	který
zkoumaly	zkoumat	k5eAaImAgFnP	zkoumat
atmosféru	atmosféra	k1gFnSc4	atmosféra
z	z	k7c2	z
orbity	orbita	k1gFnSc2	orbita
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
měření	měření	k1gNnSc2	měření
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
panuje	panovat	k5eAaImIp3nS	panovat
skleníkový	skleníkový	k2eAgInSc1d1	skleníkový
efekt	efekt	k1gInSc1	efekt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
otepluje	oteplovat	k5eAaImIp3nS	oteplovat
planetu	planeta	k1gFnSc4	planeta
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
5	[number]	k4	5
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
zadržuje	zadržovat	k5eAaImIp3nS	zadržovat
okolo	okolo	k7c2	okolo
30	[number]	k4	30
%	%	kIx~	%
tepelné	tepelný	k2eAgFnSc2d1	tepelná
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Výškově	výškově	k6eAd1	výškově
se	se	k3xPyFc4	se
atmosféra	atmosféra	k1gFnSc1	atmosféra
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
nižší	nízký	k2eAgFnPc4d2	nižší
(	(	kIx(	(
<g/>
do	do	k7c2	do
45	[number]	k4	45
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc1d1	střední
(	(	kIx(	(
<g/>
do	do	k7c2	do
110	[number]	k4	110
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
vyšší	vysoký	k2eAgFnSc1d2	vyšší
(	(	kIx(	(
<g/>
nad	nad	k7c7	nad
110	[number]	k4	110
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
byla	být	k5eAaImAgFnS	být
pozorována	pozorován	k2eAgFnSc1d1	pozorována
i	i	k9	i
oblačnost	oblačnost	k1gFnSc1	oblačnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
nejspíše	nejspíše	k9	nejspíše
tvořena	tvořen	k2eAgFnSc1d1	tvořena
krystalky	krystalka	k1gFnPc1	krystalka
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
vznikajících	vznikající	k2eAgFnPc2d1	vznikající
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
okolo	okolo	k7c2	okolo
zhruba	zhruba	k6eAd1	zhruba
patnácti	patnáct	k4xCc2	patnáct
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Vyjma	vyjma	k7c2	vyjma
oblačnosti	oblačnost	k1gFnSc2	oblačnost
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
projevují	projevovat	k5eAaImIp3nP	projevovat
i	i	k9	i
další	další	k2eAgInPc1d1	další
procesy	proces	k1gInPc1	proces
napovídající	napovídající	k2eAgInPc1d1	napovídající
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
panují	panovat	k5eAaImIp3nP	panovat
procesy	proces	k1gInPc1	proces
měnícího	měnící	k2eAgNnSc2d1	měnící
se	se	k3xPyFc4	se
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
počasí	počasí	k1gNnSc2	počasí
je	být	k5eAaImIp3nS	být
atmosféra	atmosféra	k1gFnSc1	atmosféra
planety	planeta	k1gFnSc2	planeta
také	také	k6eAd1	také
dějištěm	dějiště	k1gNnSc7	dějiště
častých	častý	k2eAgFnPc2d1	častá
prachových	prachový	k2eAgFnPc2d1	prachová
bouří	bouř	k1gFnPc2	bouř
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
občas	občas	k6eAd1	občas
dosáhnou	dosáhnout	k5eAaPmIp3nP	dosáhnout
celoplanetárního	celoplanetární	k2eAgInSc2d1	celoplanetární
charakteru	charakter	k1gInSc2	charakter
nebo	nebo	k8xC	nebo
i	i	k9	i
malé	malý	k2eAgFnSc2d1	malá
vzdušné	vzdušný	k2eAgFnSc2d1	vzdušná
víry	víra	k1gFnSc2	víra
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
prašných	prašný	k2eAgInPc2d1	prašný
vírů	vír	k1gInPc2	vír
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
bouří	bouř	k1gFnPc2	bouř
mohou	moct	k5eAaImIp3nP	moct
větry	vítr	k1gInPc1	vítr
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
planety	planeta	k1gFnSc2	planeta
dosahovat	dosahovat	k5eAaImF	dosahovat
až	až	k9	až
rychlostí	rychlost	k1gFnSc7	rychlost
okolo	okolo	k7c2	okolo
200	[number]	k4	200
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
vynášejíce	vynášet	k5eAaImSgFnP	vynášet
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
drobných	drobný	k2eAgFnPc2d1	drobná
prachových	prachový	k2eAgFnPc2d1	prachová
částic	částice	k1gFnPc2	částice
(	(	kIx(	(
<g/>
obsahujících	obsahující	k2eAgInPc2d1	obsahující
magnetit	magnetit	k1gInSc4	magnetit
<g/>
)	)	kIx)	)
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
0,1	[number]	k4	0,1
mikrometru	mikrometr	k1gInSc2	mikrometr
až	až	k9	až
0,01	[number]	k4	0,01
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
magnetit	magnetit	k1gInSc1	magnetit
má	mít	k5eAaImIp3nS	mít
větší	veliký	k2eAgFnSc4d2	veliký
schopnost	schopnost	k1gFnSc4	schopnost
pohlcovat	pohlcovat	k5eAaImF	pohlcovat
modré	modrý	k2eAgNnSc4d1	modré
světlo	světlo	k1gNnSc4	světlo
než	než	k8xS	než
červené	červené	k1gNnSc4	červené
<g/>
,	,	kIx,	,
atmosféra	atmosféra	k1gFnSc1	atmosféra
se	se	k3xPyFc4	se
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
z	z	k7c2	z
planety	planeta	k1gFnSc2	planeta
zdá	zdát	k5eAaPmIp3nS	zdát
žlutavá	žlutavý	k2eAgFnSc1d1	žlutavá
<g/>
,	,	kIx,	,
či	či	k8xC	či
při	při	k7c6	při
východu	východ	k1gInSc2	východ
<g/>
/	/	kIx~	/
<g/>
západu	západ	k1gInSc2	západ
Slunce	slunce	k1gNnSc2	slunce
červená	červenat	k5eAaImIp3nS	červenat
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
toto	tento	k3xDgNnSc4	tento
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
je	být	k5eAaImIp3nS	být
složitější	složitý	k2eAgFnSc1d2	složitější
než	než	k8xS	než
Rayleighův	Rayleighův	k2eAgInSc1d1	Rayleighův
rozptyl	rozptyl	k1gInSc1	rozptyl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
způsobující	způsobující	k2eAgMnSc1d1	způsobující
zde	zde	k6eAd1	zde
modrou	modrý	k2eAgFnSc4d1	modrá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
rychlosti	rychlost	k1gFnPc1	rychlost
větru	vítr	k1gInSc2	vítr
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
35	[number]	k4	35
až	až	k9	až
50	[number]	k4	50
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
hod	hod	k1gInSc1	hod
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
řidší	řídký	k2eAgFnSc3d2	řidší
atmosféře	atmosféra	k1gFnSc3	atmosféra
ale	ale	k8xC	ale
nemá	mít	k5eNaImIp3nS	mít
vítr	vítr	k1gInSc1	vítr
takovou	takový	k3xDgFnSc4	takový
sílu	síla	k1gFnSc4	síla
jako	jako	k8xS	jako
obdobný	obdobný	k2eAgInSc1d1	obdobný
vítr	vítr	k1gInSc1	vítr
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Voda	voda	k1gFnSc1	voda
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
kvůli	kvůli	k7c3	kvůli
nízkému	nízký	k2eAgInSc3d1	nízký
tlaku	tlak	k1gInSc3	tlak
nemůže	moct	k5eNaImIp3nS	moct
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Marsu	Mars	k1gInSc2	Mars
existovat	existovat	k5eAaImF	existovat
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
tekuté	tekutý	k2eAgFnSc6d1	tekutá
podobě	podoba	k1gFnSc6	podoba
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
-	-	kIx~	-
existuje	existovat	k5eAaImIp3nS	existovat
buď	buď	k8xC	buď
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
ledu	led	k1gInSc2	led
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
vodní	vodní	k2eAgFnSc1d1	vodní
pára	pára	k1gFnSc1	pára
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
sublimací	sublimace	k1gFnPc2	sublimace
při	při	k7c6	při
zvýšení	zvýšení	k1gNnSc6	zvýšení
teploty	teplota	k1gFnSc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
pozorování	pozorování	k1gNnSc2	pozorování
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
téměř	téměř	k6eAd1	téměř
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
planety	planeta	k1gFnSc2	planeta
tekoucí	tekoucí	k2eAgFnSc1d1	tekoucí
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
vyskytovala	vyskytovat	k5eAaImAgFnS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
spíše	spíše	k9	spíše
otázkou	otázka	k1gFnSc7	otázka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
a	a	k8xC	a
jak	jak	k6eAd1	jak
dlouho	dlouho	k6eAd1	dlouho
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
tekoucí	tekoucí	k2eAgFnSc1d1	tekoucí
voda	voda	k1gFnSc1	voda
nacházela	nacházet	k5eAaImAgFnS	nacházet
a	a	k8xC	a
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
poděla	poděla	k?	poděla
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
povrch	povrch	k1gInSc1	povrch
Marsu	Mars	k1gInSc2	Mars
byl	být	k5eAaImAgInS	být
zaplaven	zaplaven	k2eAgInSc1d1	zaplaven
oceánem	oceán	k1gInSc7	oceán
v	v	k7c4	v
období	období	k1gNnSc4	období
noachianu	noachian	k1gInSc2	noachian
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
ochlazování	ochlazování	k1gNnSc2	ochlazování
planety	planeta	k1gFnSc2	planeta
v	v	k7c6	v
hesperianu	hesperian	k1gInSc6	hesperian
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zmrznutí	zmrznutí	k1gNnSc3	zmrznutí
povrchové	povrchový	k2eAgFnSc2d1	povrchová
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
jí	jíst	k5eAaImIp3nS	jíst
zřejmě	zřejmě	k6eAd1	zřejmě
unikla	uniknout	k5eAaPmAgFnS	uniknout
do	do	k7c2	do
kosmického	kosmický	k2eAgInSc2d1	kosmický
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Následné	následný	k2eAgInPc1d1	následný
erozivní	erozivní	k2eAgInPc1d1	erozivní
procesy	proces	k1gInPc1	proces
pohřbily	pohřbít	k5eAaPmAgInP	pohřbít
část	část	k1gFnSc4	část
zmrzlého	zmrzlý	k2eAgInSc2d1	zmrzlý
ledu	led	k1gInSc2	led
pod	pod	k7c4	pod
povrch	povrch	k1gInSc4	povrch
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
těchto	tento	k3xDgInPc2	tento
zatím	zatím	k6eAd1	zatím
neprozkoumaných	prozkoumaný	k2eNgInPc2d1	neprozkoumaný
vodních	vodní	k2eAgInPc2d1	vodní
zdrojů	zdroj	k1gInPc2	zdroj
se	se	k3xPyFc4	se
na	na	k7c6	na
pólech	pól	k1gInPc6	pól
nacházejí	nacházet	k5eAaImIp3nP	nacházet
dvě	dva	k4xCgFnPc1	dva
polární	polární	k2eAgFnPc1d1	polární
čepičky	čepička	k1gFnPc1	čepička
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
částečně	částečně	k6eAd1	částečně
tvořeny	tvořit	k5eAaImNgInP	tvořit
vodním	vodní	k2eAgInSc7d1	vodní
ledem	led	k1gInSc7	led
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
suchým	suchý	k2eAgInSc7d1	suchý
ledem	led	k1gInSc7	led
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
voda	voda	k1gFnSc1	voda
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
permafrostu	permafrost	k1gInSc2	permafrost
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
zasahovat	zasahovat	k5eAaImF	zasahovat
až	až	k9	až
do	do	k7c2	do
oblastí	oblast	k1gFnPc2	oblast
kolem	kolem	k7c2	kolem
60	[number]	k4	60
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
NASA	NASA	kA	NASA
provedla	provést	k5eAaPmAgFnS	provést
odhad	odhad	k1gInSc4	odhad
množství	množství	k1gNnSc2	množství
vody	voda	k1gFnSc2	voda
zachycené	zachycený	k2eAgFnSc2d1	zachycená
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
polární	polární	k2eAgFnSc6d1	polární
čepičce	čepička	k1gFnSc6	čepička
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
modelu	model	k1gInSc2	model
by	by	kYmCp3nS	by
veškerá	veškerý	k3xTgFnSc1	veškerý
voda	voda	k1gFnSc1	voda
zaplavila	zaplavit	k5eAaPmAgFnS	zaplavit
celý	celý	k2eAgInSc4d1	celý
Mars	Mars	k1gInSc4	Mars
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
11	[number]	k4	11
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
novým	nový	k2eAgInPc3d1	nový
podrobným	podrobný	k2eAgInPc3d1	podrobný
snímkům	snímek	k1gInPc3	snímek
byly	být	k5eAaImAgInP	být
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Marsu	Mars	k1gInSc2	Mars
rozlišeny	rozlišen	k2eAgInPc1d1	rozlišen
geomorfologické	geomorfologický	k2eAgInPc1d1	geomorfologický
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
vodní	vodní	k2eAgFnSc2d1	vodní
činnosti	činnost	k1gFnSc2	činnost
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
říčních	říční	k2eAgNnPc2d1	říční
koryt	koryto	k1gNnPc2	koryto
<g/>
,	,	kIx,	,
sedimentů	sediment	k1gInPc2	sediment
<g/>
,	,	kIx,	,
pozůstatky	pozůstatek	k1gInPc4	pozůstatek
zaplavených	zaplavený	k2eAgFnPc2d1	zaplavená
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
či	či	k8xC	či
relikty	relikt	k1gInPc1	relikt
po	po	k7c6	po
rychlém	rychlý	k2eAgInSc6d1	rychlý
úniku	únik	k1gInSc6	únik
vody	voda	k1gFnSc2	voda
z	z	k7c2	z
kryosféry	kryosféra	k1gFnSc2	kryosféra
Marsu	Mars	k1gInSc2	Mars
vlivem	vlivem	k7c2	vlivem
vulkanické	vulkanický	k2eAgFnSc2d1	vulkanická
aktivity	aktivita	k1gFnSc2	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeden	jeden	k4xCgInSc1	jeden
podobný	podobný	k2eAgInSc1d1	podobný
obrovský	obrovský	k2eAgInSc1d1	obrovský
únik	únik	k1gInSc1	únik
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
i	i	k9	i
údolí	údolí	k1gNnSc4	údolí
Valles	Vallesa	k1gFnPc2	Vallesa
Marineris	Marineris	k1gFnPc2	Marineris
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
dávné	dávný	k2eAgFnSc6d1	dávná
historii	historie	k1gFnSc6	historie
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
Cerberus	Cerberus	k1gMnSc1	Cerberus
Fossae	Fossa	k1gInSc2	Fossa
<g/>
,	,	kIx,	,
u	u	k7c2	u
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
vznik	vznik	k1gInSc1	vznik
před	před	k7c7	před
5	[number]	k4	5
milióny	milión	k4xCgInPc7	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Prolomení	prolomení	k1gNnSc1	prolomení
vyvrhlo	vyvrhnout	k5eAaPmAgNnS	vyvrhnout
vodu	voda	k1gFnSc4	voda
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
Elysium	elysium	k1gNnSc1	elysium
Planitia	Planitium	k1gNnSc2	Planitium
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
ledové	ledový	k2eAgNnSc4d1	ledové
moře	moře	k1gNnSc4	moře
viditelné	viditelný	k2eAgFnSc2d1	viditelná
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2015	[number]	k4	2015
přišla	přijít	k5eAaPmAgFnS	přijít
NASA	NASA	kA	NASA
s	s	k7c7	s
důležitým	důležitý	k2eAgInSc7d1	důležitý
objevem	objev	k1gInSc7	objev
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
za	za	k7c2	za
příznivých	příznivý	k2eAgFnPc2d1	příznivá
podmínek	podmínka	k1gFnPc2	podmínka
občas	občas	k6eAd1	občas
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
tekoucí	tekoucí	k2eAgFnSc1d1	tekoucí
velmi	velmi	k6eAd1	velmi
slaná	slaný	k2eAgFnSc1d1	slaná
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Mars	Mars	k1gInSc1	Mars
má	mít	k5eAaImIp3nS	mít
slabé	slabý	k2eAgNnSc1d1	slabé
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
ochranná	ochranný	k2eAgFnSc1d1	ochranná
funkce	funkce	k1gFnSc1	funkce
je	být	k5eAaImIp3nS	být
neporovnatelně	porovnatelně	k6eNd1	porovnatelně
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
u	u	k7c2	u
zemského	zemský	k2eAgNnSc2d1	zemské
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Měření	měření	k1gNnSc1	měření
sondy	sonda	k1gFnSc2	sonda
Mars	Mars	k1gInSc1	Mars
Global	globat	k5eAaImAgMnS	globat
Surveyor	Surveyor	k1gMnSc1	Surveyor
přinesla	přinést	k5eAaPmAgFnS	přinést
důkazy	důkaz	k1gInPc4	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
planety	planeta	k1gFnSc2	planeta
měl	mít	k5eAaImAgInS	mít
Mars	Mars	k1gInSc1	Mars
dynamičtější	dynamický	k2eAgInSc4d2	dynamičtější
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
podobal	podobat	k5eAaImAgMnS	podobat
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Měření	měření	k1gNnSc1	měření
magnetometrem	magnetometr	k1gInSc7	magnetometr
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
magnetické	magnetický	k2eAgInPc4d1	magnetický
pruhy	pruh	k1gInPc4	pruh
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
silnějším	silný	k2eAgNnSc6d2	silnější
magnetickém	magnetický	k2eAgNnSc6d1	magnetické
dynamu	dynamo	k1gNnSc6	dynamo
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
pracovalo	pracovat	k5eAaImAgNnS	pracovat
několik	několik	k4yIc1	několik
miliónu	milión	k4xCgInSc2	milión
let	léto	k1gNnPc2	léto
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
<g/>
.	.	kIx.	.
</s>
<s>
Neznámá	známý	k2eNgFnSc1d1	neznámá
událost	událost	k1gFnSc1	událost
(	(	kIx(	(
<g/>
možný	možný	k2eAgInSc1d1	možný
dopad	dopad	k1gInSc1	dopad
asteroidu	asteroid	k1gInSc2	asteroid
<g/>
)	)	kIx)	)
však	však	k9	však
toto	tento	k3xDgNnSc4	tento
pole	pole	k1gNnSc4	pole
narušila	narušit	k5eAaPmAgFnS	narušit
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zjištění	zjištění	k1gNnSc2	zjištění
vědců	vědec	k1gMnPc2	vědec
z	z	k7c2	z
amerického	americký	k2eAgInSc2d1	americký
Úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
letectví	letectví	k1gNnSc4	letectví
a	a	k8xC	a
vesmír	vesmír	k1gInSc1	vesmír
(	(	kIx(	(
<g/>
NASA	NASA	kA	NASA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
analyzovali	analyzovat	k5eAaImAgMnP	analyzovat
získaná	získaný	k2eAgNnPc4d1	získané
data	datum	k1gNnPc4	datum
ze	z	k7c2	z
sondy	sonda	k1gFnSc2	sonda
Mars	Mars	k1gInSc1	Mars
Odyssey	Odyssea	k1gFnSc2	Odyssea
<g/>
,	,	kIx,	,
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
radiace	radiace	k1gFnSc1	radiace
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
Marsu	Mars	k1gInSc2	Mars
je	být	k5eAaImIp3nS	být
2,5	[number]	k4	2,5
<g/>
krát	krát	k6eAd1	krát
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
na	na	k7c6	na
Mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
vesmírné	vesmírný	k2eAgFnSc6d1	vesmírná
stanici	stanice	k1gFnSc6	stanice
a	a	k8xC	a
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
tak	tak	k6eAd1	tak
limitů	limit	k1gInPc2	limit
pro	pro	k7c4	pro
bezpečný	bezpečný	k2eAgInSc4d1	bezpečný
pobyt	pobyt	k1gInSc4	pobyt
<g/>
.	.	kIx.	.
</s>
<s>
NASA	NASA	kA	NASA
považuje	považovat	k5eAaImIp3nS	považovat
tento	tento	k3xDgInSc4	tento
problém	problém	k1gInSc4	problém
za	za	k7c4	za
zvládnutelný	zvládnutelný	k2eAgInSc4d1	zvládnutelný
pomocí	pomocí	k7c2	pomocí
stínítek	stínítko	k1gNnPc2	stínítko
a	a	k8xC	a
systémem	systém	k1gInSc7	systém
varování	varování	k1gNnSc2	varování
před	před	k7c7	před
vyšším	vysoký	k2eAgNnSc7d2	vyšší
zářením	záření	k1gNnSc7	záření
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Mars	Mars	k1gInSc1	Mars
obíhá	obíhat	k5eAaImIp3nS	obíhat
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
mezi	mezi	k7c7	mezi
206	[number]	k4	206
644	[number]	k4	644
545	[number]	k4	545
km	km	kA	km
(	(	kIx(	(
<g/>
1,381	[number]	k4	1,381
<g/>
5	[number]	k4	5
AU	au	k0	au
<g/>
)	)	kIx)	)
v	v	k7c6	v
perihelu	perihel	k1gInSc6	perihel
a	a	k8xC	a
249	[number]	k4	249
228	[number]	k4	228
730	[number]	k4	730
km	km	kA	km
(	(	kIx(	(
<g/>
1,666	[number]	k4	1,666
AU	au	k0	au
<g/>
)	)	kIx)	)
v	v	k7c6	v
afelu	afel	k1gInSc6	afel
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
jednoho	jeden	k4xCgInSc2	jeden
oběhu	oběh	k1gInSc2	oběh
kolem	kolem	k7c2	kolem
centrální	centrální	k2eAgFnSc2d1	centrální
hvězdy	hvězda	k1gFnSc2	hvězda
je	být	k5eAaImIp3nS	být
686,960	[number]	k4	686,960
<g/>
1	[number]	k4	1
pozemského	pozemský	k2eAgInSc2d1	pozemský
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
své	svůj	k3xOyFgFnSc2	svůj
osy	osa	k1gFnSc2	osa
se	se	k3xPyFc4	se
Mars	Mars	k1gInSc1	Mars
otočí	otočit	k5eAaPmIp3nS	otočit
za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
podobná	podobný	k2eAgFnSc1d1	podobná
délce	délka	k1gFnSc3	délka
pozemského	pozemský	k2eAgInSc2d1	pozemský
dne	den	k1gInSc2	den
-	-	kIx~	-
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
39	[number]	k4	39
minut	minuta	k1gFnPc2	minuta
35,244	[number]	k4	35,244
sekund	sekunda	k1gFnPc2	sekunda
(	(	kIx(	(
<g/>
Země	zem	k1gFnSc2	zem
23	[number]	k4	23
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
56	[number]	k4	56
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
4,091	[number]	k4	4,091
sekund	sekunda	k1gFnPc2	sekunda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úhlový	úhlový	k2eAgInSc1d1	úhlový
sklon	sklon	k1gInSc1	sklon
planetární	planetární	k2eAgFnSc2d1	planetární
osy	osa	k1gFnSc2	osa
25,19	[number]	k4	25,19
<g/>
°	°	k?	°
je	být	k5eAaImIp3nS	být
srovnatelný	srovnatelný	k2eAgInSc1d1	srovnatelný
se	s	k7c7	s
sklonem	sklon	k1gInSc7	sklon
23,44	[number]	k4	23,44
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
má	mít	k5eAaImIp3nS	mít
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomuto	tento	k3xDgNnSc3	tento
sklonění	sklonění	k1gNnSc3	sklonění
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
roční	roční	k2eAgNnSc4d1	roční
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
podobná	podobný	k2eAgNnPc1d1	podobné
těm	ten	k3xDgMnPc3	ten
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
dvakrát	dvakrát	k6eAd1	dvakrát
tak	tak	k6eAd1	tak
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
"	"	kIx"	"
<g/>
marsovský	marsovský	k2eAgInSc4d1	marsovský
rok	rok	k1gInSc4	rok
<g/>
"	"	kIx"	"
činí	činit	k5eAaImIp3nS	činit
1,88	[number]	k4	1,88
roku	rok	k1gInSc2	rok
pozemského	pozemský	k2eAgInSc2d1	pozemský
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
oběžné	oběžný	k2eAgFnSc2d1	oběžná
doby	doba	k1gFnSc2	doba
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
mezi	mezi	k7c7	mezi
56	[number]	k4	56
milióny	milión	k4xCgInPc7	milión
až	až	k8xS	až
400	[number]	k4	400
milióny	milión	k4xCgInPc1	milión
kilometrů	kilometr	k1gInPc2	kilometr
v	v	k7c6	v
pravidelném	pravidelný	k2eAgInSc6d1	pravidelný
cyklu	cyklus	k1gInSc6	cyklus
17	[number]	k4	17
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nastává	nastávat	k5eAaImIp3nS	nastávat
nejpříznivější	příznivý	k2eAgFnSc1d3	nejpříznivější
opozice	opozice	k1gFnSc1	opozice
planety	planeta	k1gFnSc2	planeta
pro	pro	k7c4	pro
pozorování	pozorování	k1gNnSc4	pozorování
a	a	k8xC	a
pro	pro	k7c4	pro
vysílání	vysílání	k1gNnSc4	vysílání
kosmických	kosmický	k2eAgFnPc2d1	kosmická
sond	sonda	k1gFnPc2	sonda
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Mars	Mars	k1gInSc1	Mars
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
<g/>
,	,	kIx,	,
či	či	k8xC	či
oddaluje	oddalovat	k5eAaImIp3nS	oddalovat
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
současně	současně	k6eAd1	současně
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
jeho	jeho	k3xOp3gFnSc3	jeho
hvězdné	hvězdný	k2eAgFnSc3d1	hvězdná
velikosti	velikost	k1gFnSc3	velikost
-	-	kIx~	-
pohyb	pohyb	k1gInSc1	pohyb
mezi	mezi	k7c7	mezi
1,6	[number]	k4	1,6
<g/>
m	m	kA	m
až	až	k6eAd1	až
-	-	kIx~	-
<g/>
2,8	[number]	k4	2,8
<g/>
m	m	kA	m
<g/>
,	,	kIx,	,
zdánlivý	zdánlivý	k2eAgInSc1d1	zdánlivý
průměr	průměr	k1gInSc1	průměr
4	[number]	k4	4
<g/>
"	"	kIx"	"
do	do	k7c2	do
25	[number]	k4	25
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nepravidelnost	nepravidelnost	k1gFnSc1	nepravidelnost
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
obdobích	období	k1gNnPc6	období
je	být	k5eAaImIp3nS	být
Mars	Mars	k1gInSc1	Mars
čtvrtým	čtvrtý	k4xOgInSc7	čtvrtý
nejjasnějším	jasný	k2eAgNnSc7d3	nejjasnější
tělesem	těleso	k1gNnSc7	těleso
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
po	po	k7c6	po
Slunci	slunce	k1gNnSc6	slunce
<g/>
,	,	kIx,	,
Měsíci	měsíc	k1gInSc6	měsíc
a	a	k8xC	a
Venuši	Venuše	k1gFnSc6	Venuše
a	a	k8xC	a
jindy	jindy	k6eAd1	jindy
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
jasný	jasný	k2eAgMnSc1d1	jasný
než	než	k8xS	než
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
byla	být	k5eAaImAgFnS	být
velká	velký	k2eAgFnSc1d1	velká
opozice	opozice	k1gFnSc1	opozice
Marsu	Mars	k1gInSc2	Mars
a	a	k8xC	a
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Mars	Mars	k1gInSc1	Mars
byl	být	k5eAaImAgInS	být
nejblíže	blízce	k6eAd3	blízce
55,757	[number]	k4	55,757
milionu	milion	k4xCgInSc2	milion
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
nastala	nastat	k5eAaPmAgFnS	nastat
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
Mars	Mars	k1gInSc1	Mars
při	při	k7c6	při
pozorování	pozorování	k1gNnSc6	pozorování
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
až	až	k9	až
55	[number]	k4	55
<g/>
°	°	k?	°
nad	nad	k7c7	nad
obzorem	obzor	k1gInSc7	obzor
<g/>
,	,	kIx,	,
další	další	k2eAgMnPc1d1	další
byla	být	k5eAaImAgFnS	být
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
měl	mít	k5eAaImAgInS	mít
Mars	Mars	k1gInSc1	Mars
magnitudu	magnitud	k1gInSc2	magnitud
-1,2	-1,2	k4	-1,2
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
opozice	opozice	k1gFnSc1	opozice
byla	být	k5eAaImAgFnS	být
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
měl	mít	k5eAaImAgInS	mít
Mars	Mars	k1gInSc1	Mars
magnitudu	magnitud	k1gInSc2	magnitud
-1,1	-1,1	k4	-1,1
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Měsíce	měsíc	k1gInSc2	měsíc
Marsu	Mars	k1gInSc2	Mars
<g/>
,	,	kIx,	,
Phobos	Phobos	k1gInSc1	Phobos
(	(	kIx(	(
<g/>
měsíc	měsíc	k1gInSc1	měsíc
<g/>
)	)	kIx)	)
a	a	k8xC	a
Deimos	Deimos	k1gInSc4	Deimos
(	(	kIx(	(
<g/>
měsíc	měsíc	k1gInSc4	měsíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
planety	planeta	k1gFnSc2	planeta
obíhají	obíhat	k5eAaImIp3nP	obíhat
dvě	dva	k4xCgFnPc1	dva
přirozené	přirozený	k2eAgFnPc1d1	přirozená
družice	družice	k1gFnPc1	družice
-	-	kIx~	-
Phobos	Phobos	k1gInSc1	Phobos
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
strach	strach	k1gInSc1	strach
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
Deimos	Deimos	k1gMnSc1	Deimos
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
hrůza	hrůza	k1gFnSc1	hrůza
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
byli	být	k5eAaImAgMnP	být
Fobos	Fobos	k1gMnSc1	Fobos
a	a	k8xC	a
Deimos	Deimos	k1gMnSc1	Deimos
synové	syn	k1gMnPc1	syn
boha	bůh	k1gMnSc2	bůh
války	válka	k1gFnSc2	válka
Area	area	k1gFnSc1	area
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ho	on	k3xPp3gNnSc4	on
jako	jako	k9	jako
vozatajové	vozataj	k1gMnPc1	vozataj
doprovázeli	doprovázet	k5eAaImAgMnP	doprovázet
do	do	k7c2	do
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Řecký	řecký	k2eAgInSc1d1	řecký
Arés	Arés	k1gInSc1	Arés
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
protějškem	protějšek	k1gInSc7	protějšek
římského	římský	k2eAgMnSc2d1	římský
Marta	Mars	k1gMnSc2	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
objevil	objevit	k5eAaPmAgMnS	objevit
Asaph	Asaph	k1gMnSc1	Asaph
Hall	Hall	k1gMnSc1	Hall
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1877	[number]	k4	1877
a	a	k8xC	a
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
synů	syn	k1gMnPc2	syn
boha	bůh	k1gMnSc4	bůh
Marta	Mars	k1gMnSc4	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
existence	existence	k1gFnSc1	existence
měsíců	měsíc	k1gInPc2	měsíc
byla	být	k5eAaImAgFnS	být
několikrát	několikrát	k6eAd1	několikrát
předpovězena	předpovězet	k5eAaImNgFnS	předpovězet
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
dlouho	dlouho	k6eAd1	dlouho
před	před	k7c7	před
jejich	jejich	k3xOp3gNnSc7	jejich
objevením	objevení	k1gNnSc7	objevení
<g/>
.	.	kIx.	.
</s>
<s>
Johannes	Johannes	k1gMnSc1	Johannes
Kepler	Kepler	k1gMnSc1	Kepler
byl	být	k5eAaImAgMnS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
Země	země	k1gFnSc1	země
jeden	jeden	k4xCgInSc4	jeden
měsíc	měsíc	k1gInSc4	měsíc
a	a	k8xC	a
Jupiter	Jupiter	k1gInSc4	Jupiter
4	[number]	k4	4
měsíce	měsíc	k1gInSc2	měsíc
(	(	kIx(	(
<g/>
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
době	doba	k1gFnSc6	doba
byly	být	k5eAaImAgFnP	být
známy	znám	k2eAgInPc1d1	znám
pouze	pouze	k6eAd1	pouze
Galileovy	Galileův	k2eAgInPc1d1	Galileův
měsíce	měsíc	k1gInPc1	měsíc
Jupitera	Jupiter	k1gMnSc2	Jupiter
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
Mars	Mars	k1gInSc1	Mars
kvůli	kvůli	k7c3	kvůli
harmonii	harmonie	k1gFnSc3	harmonie
kosmu	kosmos	k1gInSc2	kosmos
měsíce	měsíc	k1gInSc2	měsíc
dva	dva	k4xCgInPc1	dva
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
dvou	dva	k4xCgInPc6	dva
marsovských	marsovský	k2eAgInPc6d1	marsovský
měsících	měsíc	k1gInPc6	měsíc
psal	psát	k5eAaImAgMnS	psát
i	i	k9	i
Jonathan	Jonathan	k1gMnSc1	Jonathan
Swift	Swift	k1gMnSc1	Swift
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Gulliverovy	Gulliverův	k2eAgFnSc2d1	Gulliverova
cesty	cesta	k1gFnSc2	cesta
(	(	kIx(	(
<g/>
1726	[number]	k4	1726
<g/>
)	)	kIx)	)
či	či	k8xC	či
Voltaire	Voltair	k1gInSc5	Voltair
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Micromégas	Micromégasa	k1gFnPc2	Micromégasa
(	(	kIx(	(
<g/>
1752	[number]	k4	1752
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
dvě	dva	k4xCgNnPc1	dva
tělesa	těleso	k1gNnPc1	těleso
mají	mít	k5eAaImIp3nP	mít
vázanou	vázaný	k2eAgFnSc4d1	vázaná
rotaci	rotace	k1gFnSc4	rotace
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
Marsu	Mars	k1gInSc2	Mars
stále	stále	k6eAd1	stále
stejnou	stejný	k2eAgFnSc4d1	stejná
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
nápadně	nápadně	k6eAd1	nápadně
se	s	k7c7	s
chemickým	chemický	k2eAgNnSc7d1	chemické
složením	složení	k1gNnSc7	složení
a	a	k8xC	a
tvarově	tvarově	k6eAd1	tvarově
podobají	podobat	k5eAaImIp3nP	podobat
tělesům	těleso	k1gNnPc3	těleso
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
pás	pás	k1gInSc1	pás
planetek	planetka	k1gFnPc2	planetka
mezi	mezi	k7c7	mezi
Marsem	Mars	k1gInSc7	Mars
a	a	k8xC	a
Jupiterem	Jupiter	k1gInSc7	Jupiter
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
teorii	teorie	k1gFnSc3	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
asteroidy	asteroid	k1gInPc4	asteroid
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
Mars	Mars	k1gMnSc1	Mars
svojí	svůj	k3xOyFgFnSc7	svůj
gravitací	gravitace	k1gFnSc7	gravitace
zachytil	zachytit	k5eAaPmAgMnS	zachytit
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
teorií	teorie	k1gFnSc7	teorie
je	být	k5eAaImIp3nS	být
impaktní	impaktní	k2eAgFnSc1d1	impaktní
(	(	kIx(	(
<g/>
podobná	podobný	k2eAgFnSc1d1	podobná
dnes	dnes	k6eAd1	dnes
převládající	převládající	k2eAgFnSc4d1	převládající
teorii	teorie	k1gFnSc4	teorie
původu	původ	k1gInSc2	původ
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
počítající	počítající	k2eAgFnSc3d1	počítající
se	s	k7c7	s
srážkou	srážka	k1gFnSc7	srážka
velkého	velký	k2eAgNnSc2d1	velké
tělesa	těleso	k1gNnSc2	těleso
s	s	k7c7	s
Marsem	Mars	k1gInSc7	Mars
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyvrhla	vyvrhnout	k5eAaPmAgFnS	vyvrhnout
horniny	hornina	k1gFnPc4	hornina
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
obou	dva	k4xCgNnPc2	dva
těles	těleso	k1gNnPc2	těleso
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
Marsu	Mars	k1gInSc2	Mars
<g/>
,	,	kIx,	,
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
materiál	materiál	k1gInSc1	materiál
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
zformoval	zformovat	k5eAaPmAgMnS	zformovat
do	do	k7c2	do
měsíce	měsíc	k1gInSc2	měsíc
Phobos	Phobosa	k1gFnPc2	Phobosa
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
definitivní	definitivní	k2eAgNnSc4d1	definitivní
zodpovězení	zodpovězený	k2eAgMnPc1d1	zodpovězený
této	tento	k3xDgFnSc2	tento
otázky	otázka	k1gFnSc2	otázka
bude	být	k5eAaImBp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
odebrat	odebrat	k5eAaPmF	odebrat
vzorky	vzorek	k1gInPc4	vzorek
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
těchto	tento	k3xDgInPc2	tento
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Phobos	Phobos	k1gInSc1	Phobos
obíhá	obíhat	k5eAaImIp3nS	obíhat
planetu	planeta	k1gFnSc4	planeta
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
se	se	k3xPyFc4	se
ona	onen	k3xDgFnSc1	onen
sama	sám	k3xTgMnSc4	sám
otáčí	otáčet	k5eAaImIp3nS	otáčet
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
zpomalování	zpomalování	k1gNnSc4	zpomalování
jeho	jeho	k3xOp3gInSc2	jeho
oběhu	oběh	k1gInSc2	oběh
a	a	k8xC	a
snižování	snižování	k1gNnSc2	snižování
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
50	[number]	k4	50
000	[number]	k4	000
000	[number]	k4	000
let	léto	k1gNnPc2	léto
Phobos	Phobosa	k1gFnPc2	Phobosa
do	do	k7c2	do
planety	planeta	k1gFnSc2	planeta
narazí	narazit	k5eAaPmIp3nP	narazit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
Marsu	Mars	k1gInSc2	Mars
by	by	kYmCp3nS	by
Phobos	Phobos	k1gInSc4	Phobos
měl	mít	k5eAaImAgInS	mít
úhlový	úhlový	k2eAgInSc1d1	úhlový
průměr	průměr	k1gInSc1	průměr
12	[number]	k4	12
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Deimos	Deimos	k1gInSc1	Deimos
asi	asi	k9	asi
2	[number]	k4	2
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnPc1	první
pozorování	pozorování	k1gNnPc1	pozorování
planety	planeta	k1gFnSc2	planeta
jsou	být	k5eAaImIp3nP	být
známá	známý	k2eAgFnSc1d1	známá
již	již	k6eAd1	již
z	z	k7c2	z
období	období	k1gNnSc4	období
prvních	první	k4xOgFnPc2	první
civilizací	civilizace	k1gFnPc2	civilizace
(	(	kIx(	(
<g/>
Egypťané	Egypťan	k1gMnPc1	Egypťan
<g/>
,	,	kIx,	,
Babylóňané	Babylóňan	k1gMnPc1	Babylóňan
a	a	k8xC	a
Řekové	Řek	k1gMnPc1	Řek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
Mars	Mars	k1gInSc1	Mars
pozorován	pozorován	k2eAgInSc1d1	pozorován
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
využili	využít	k5eAaPmAgMnP	využít
astronomové	astronom	k1gMnPc1	astronom
první	první	k4xOgInPc4	první
konstruované	konstruovaný	k2eAgInPc4d1	konstruovaný
dalekohledy	dalekohled	k1gInPc4	dalekohled
pro	pro	k7c4	pro
pozorování	pozorování	k1gNnSc4	pozorování
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
jim	on	k3xPp3gMnPc3	on
umožnily	umožnit	k5eAaPmAgInP	umožnit
rozeznat	rozeznat	k5eAaPmF	rozeznat
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
planety	planeta	k1gFnSc2	planeta
tmavé	tmavý	k2eAgFnSc2d1	tmavá
a	a	k8xC	a
světlé	světlý	k2eAgFnSc2d1	světlá
plochy	plocha	k1gFnSc2	plocha
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
se	se	k3xPyFc4	se
usoudilo	usoudit	k5eAaPmAgNnS	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
jsou	být	k5eAaImIp3nP	být
polární	polární	k2eAgFnPc1d1	polární
čepičky	čepička	k1gFnPc1	čepička
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1877	[number]	k4	1877
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
mapách	mapa	k1gFnPc6	mapa
povrchu	povrch	k1gInSc2	povrch
Marsu	Mars	k1gInSc2	Mars
objevují	objevovat	k5eAaImIp3nP	objevovat
nové	nový	k2eAgInPc1d1	nový
útvary	útvar	k1gInPc1	útvar
tzv.	tzv.	kA	tzv.
kanály	kanál	k1gInPc1	kanál
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yRgMnPc2	který
si	se	k3xPyFc3	se
jejich	jejich	k3xOp3gInSc1	jejich
objevitel	objevitel	k1gMnSc1	objevitel
Giovanni	Giovanň	k1gMnSc3	Giovanň
Schiaparelli	Schiaparell	k1gMnSc3	Schiaparell
nebyl	být	k5eNaImAgMnS	být
jist	jist	k2eAgMnSc1d1	jist
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
znamenají	znamenat	k5eAaImIp3nP	znamenat
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byly	být	k5eAaImAgFnP	být
pouhým	pouhý	k2eAgInSc7d1	pouhý
optickým	optický	k2eAgInSc7d1	optický
klamem	klam	k1gInSc7	klam
zapříčiněným	zapříčiněný	k2eAgInSc7d1	zapříčiněný
špatnými	špatný	k2eAgFnPc7d1	špatná
rozlišovacími	rozlišovací	k2eAgFnPc7d1	rozlišovací
schopnostmi	schopnost	k1gFnPc7	schopnost
dalekohledu	dalekohled	k1gInSc2	dalekohled
a	a	k8xC	a
pohybem	pohyb	k1gInSc7	pohyb
prachu	prach	k1gInSc2	prach
po	po	k7c6	po
povrchu	povrch	k1gInSc6	povrch
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Částečně	částečně	k6eAd1	částečně
vlivem	vliv	k1gInSc7	vliv
špatného	špatný	k2eAgInSc2d1	špatný
překladu	překlad	k1gInSc2	překlad
italského	italský	k2eAgNnSc2d1	italské
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
canale	canale	k6eAd1	canale
<g/>
"	"	kIx"	"
znamenající	znamenající	k2eAgMnSc1d1	znamenající
vyjma	vyjma	k7c2	vyjma
umělého	umělý	k2eAgInSc2d1	umělý
kanálu	kanál	k1gInSc2	kanál
i	i	k8xC	i
přírodní	přírodní	k2eAgNnPc1d1	přírodní
"	"	kIx"	"
<g/>
koryto	koryto	k1gNnSc1	koryto
<g/>
"	"	kIx"	"
došlo	dojít	k5eAaPmAgNnS	dojít
překladem	překlad	k1gInSc7	překlad
k	k	k7c3	k
mýlce	mýlka	k1gFnSc3	mýlka
<g/>
,	,	kIx,	,
že	že	k8xS	že
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
umělého	umělý	k2eAgInSc2d1	umělý
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
pozorování	pozorování	k1gNnSc6	pozorování
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
roznesla	roznést	k5eAaPmAgFnS	roznést
a	a	k8xC	a
následně	následně	k6eAd1	následně
objev	objev	k1gInSc1	objev
začaly	začít	k5eAaPmAgInP	začít
potvrzovat	potvrzovat	k5eAaImF	potvrzovat
i	i	k9	i
další	další	k2eAgNnPc4d1	další
pozorovací	pozorovací	k2eAgNnPc4d1	pozorovací
místa	místo	k1gNnPc4	místo
a	a	k8xC	a
vytvářet	vytvářet	k5eAaImF	vytvářet
nepřeberné	přeberný	k2eNgNnSc4d1	nepřeberné
množství	množství	k1gNnSc4	množství
podrobných	podrobný	k2eAgFnPc2d1	podrobná
map	mapa	k1gFnPc2	mapa
neexistujících	existující	k2eNgInPc2d1	neexistující
kanálů	kanál	k1gInPc2	kanál
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
začaly	začít	k5eAaPmAgFnP	začít
vznikat	vznikat	k5eAaImF	vznikat
teorie	teorie	k1gFnSc1	teorie
o	o	k7c6	o
jejich	jejich	k3xOp3gInSc6	jejich
umělém	umělý	k2eAgInSc6d1	umělý
vzniku	vznik	k1gInSc6	vznik
a	a	k8xC	a
umírající	umírající	k2eAgFnSc3d1	umírající
civilizaci	civilizace	k1gFnSc3	civilizace
na	na	k7c6	na
vysychající	vysychající	k2eAgFnSc6d1	vysychající
planetě	planeta	k1gFnSc6	planeta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jsou	být	k5eAaImIp3nP	být
kanály	kanál	k1gInPc1	kanál
jen	jen	k9	jen
optický	optický	k2eAgInSc4d1	optický
klam	klam	k1gInSc4	klam
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
řetězcem	řetězec	k1gInSc7	řetězec
tmavých	tmavý	k2eAgFnPc2d1	tmavá
skvrn	skvrna	k1gFnPc2	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
existence	existence	k1gFnSc1	existence
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
50	[number]	k4	50
letech	léto	k1gNnPc6	léto
pozorováním	pozorování	k1gNnSc7	pozorování
vyvrácena	vyvrátit	k5eAaPmNgFnS	vyvrátit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
část	část	k1gFnSc1	část
veřejnosti	veřejnost	k1gFnSc2	veřejnost
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
existující	existující	k2eAgNnSc4d1	existující
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
fotografie	fotografia	k1gFnPc1	fotografia
z	z	k7c2	z
kosmických	kosmický	k2eAgFnPc2d1	kosmická
sond	sonda	k1gFnPc2	sonda
jednoznačně	jednoznačně	k6eAd1	jednoznačně
toto	tento	k3xDgNnSc4	tento
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
vyvrátily	vyvrátit	k5eAaPmAgFnP	vyvrátit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pozorování	pozorování	k1gNnSc6	pozorování
dalekohledem	dalekohled	k1gInSc7	dalekohled
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
nelze	lze	k6eNd1	lze
vidět	vidět	k5eAaImF	vidět
žádné	žádný	k3yNgInPc4	žádný
významné	významný	k2eAgInPc4d1	významný
detaily	detail	k1gInPc4	detail
povrchu	povrch	k1gInSc2	povrch
vyjma	vyjma	k7c2	vyjma
polárních	polární	k2eAgFnPc2d1	polární
čepiček	čepička	k1gFnPc2	čepička
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
podrobné	podrobný	k2eAgNnSc1d1	podrobné
prozkoumání	prozkoumání	k1gNnSc1	prozkoumání
Marsu	Mars	k1gInSc2	Mars
mohlo	moct	k5eAaImAgNnS	moct
proběhnout	proběhnout	k5eAaPmF	proběhnout
až	až	k9	až
po	po	k7c6	po
návštěvě	návštěva	k1gFnSc6	návštěva
sond	sonda	k1gFnPc2	sonda
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Výzkum	výzkum	k1gInSc1	výzkum
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Mars	Mars	k1gInSc1	Mars
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
zkoumána	zkoumat	k5eAaImNgFnS	zkoumat
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
průzkumu	průzkum	k1gInSc2	průzkum
<g/>
.	.	kIx.	.
</s>
<s>
Americké	americký	k2eAgFnPc1d1	americká
<g/>
,	,	kIx,	,
ruské	ruský	k2eAgFnPc1d1	ruská
<g/>
,	,	kIx,	,
evropské	evropský	k2eAgFnPc1d1	Evropská
a	a	k8xC	a
japonské	japonský	k2eAgFnPc1d1	japonská
sondy	sonda	k1gFnPc1	sonda
kolem	kolem	k7c2	kolem
této	tento	k3xDgFnSc2	tento
planety	planeta	k1gFnSc2	planeta
již	již	k6eAd1	již
obíhaly	obíhat	k5eAaImAgFnP	obíhat
<g/>
,	,	kIx,	,
dopadaly	dopadat	k5eAaImAgFnP	dopadat
na	na	k7c4	na
její	její	k3xOp3gInSc4	její
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
přistávaly	přistávat	k5eAaImAgInP	přistávat
a	a	k8xC	a
jezdily	jezdit	k5eAaImAgInP	jezdit
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
získaly	získat	k5eAaPmAgFnP	získat
data	datum	k1gNnSc2	datum
o	o	k7c6	o
jejím	její	k3xOp3gNnSc6	její
geologickém	geologický	k2eAgNnSc6d1	geologické
složení	složení	k1gNnSc6	složení
<g/>
,	,	kIx,	,
vlastnostech	vlastnost	k1gFnPc6	vlastnost
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
hledaly	hledat	k5eAaImAgFnP	hledat
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
zkoumaly	zkoumat	k5eAaImAgFnP	zkoumat
klima	klima	k1gNnSc4	klima
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
mise	mise	k1gFnSc1	mise
byla	být	k5eAaImAgFnS	být
americká	americký	k2eAgFnSc1d1	americká
Mariner	Marinra	k1gFnPc2	Marinra
4	[number]	k4	4
vypuštěná	vypuštěný	k2eAgFnSc1d1	vypuštěná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
<s>
Následoval	následovat	k5eAaImAgInS	následovat
symbolický	symbolický	k2eAgInSc1d1	symbolický
úspěch	úspěch	k1gInSc1	úspěch
dvou	dva	k4xCgFnPc2	dva
sovětských	sovětský	k2eAgFnPc2d1	sovětská
sond	sonda	k1gFnPc2	sonda
Mars	Mars	k1gMnSc1	Mars
2	[number]	k4	2
a	a	k8xC	a
Mars	Mars	k1gInSc4	Mars
3	[number]	k4	3
vypuštěných	vypuštěný	k2eAgMnPc2d1	vypuštěný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
přistály	přistát	k5eAaImAgFnP	přistát
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
byl	být	k5eAaImAgMnS	být
ztracen	ztratit	k5eAaPmNgMnS	ztratit
několik	několik	k4yIc1	několik
sekund	sekunda	k1gFnPc2	sekunda
po	po	k7c6	po
dosednutí	dosednutí	k1gNnSc6	dosednutí
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
událostí	událost	k1gFnSc7	událost
začátku	začátek	k1gInSc2	začátek
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
navedení	navedení	k1gNnSc1	navedení
americké	americký	k2eAgFnSc2d1	americká
sondy	sonda	k1gFnSc2	sonda
Mariner	Mariner	k1gInSc1	Mariner
9	[number]	k4	9
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pořídila	pořídit	k5eAaPmAgFnS	pořídit
první	první	k4xOgFnSc1	první
kvalitní	kvalitní	k2eAgFnSc1d1	kvalitní
fotografie	fotografie	k1gFnSc1	fotografie
povrchu	povrch	k1gInSc2	povrch
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
umožnily	umožnit	k5eAaPmAgFnP	umožnit
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
základní	základní	k2eAgFnPc4d1	základní
morfologické	morfologický	k2eAgFnPc4d1	morfologická
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Následoval	následovat	k5eAaImAgMnS	následovat
americký	americký	k2eAgInSc4d1	americký
program	program	k1gInSc4	program
Viking	Viking	k1gMnSc1	Viking
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
skládal	skládat	k5eAaImAgMnS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
orbitálních	orbitální	k2eAgFnPc2d1	orbitální
sond	sonda	k1gFnPc2	sonda
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
obsahující	obsahující	k2eAgFnSc1d1	obsahující
i	i	k8xC	i
povrchový	povrchový	k2eAgInSc1d1	povrchový
modul	modul	k1gInSc1	modul
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
povrchové	povrchový	k2eAgInPc1d1	povrchový
moduly	modul	k1gInPc1	modul
úspěšně	úspěšně	k6eAd1	úspěšně
přistály	přistát	k5eAaImAgInP	přistát
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
a	a	k8xC	a
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
6	[number]	k4	6
(	(	kIx(	(
<g/>
Viking	Viking	k1gMnSc1	Viking
1	[number]	k4	1
<g/>
)	)	kIx)	)
respektive	respektive	k9	respektive
3	[number]	k4	3
(	(	kIx(	(
<g/>
Viking	Viking	k1gMnSc1	Viking
2	[number]	k4	2
<g/>
)	)	kIx)	)
let	léto	k1gNnPc2	léto
prováděly	provádět	k5eAaImAgFnP	provádět
pozorování	pozorování	k1gNnSc2	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Přistávací	přistávací	k2eAgInPc1d1	přistávací
moduly	modul	k1gInPc1	modul
odvysílaly	odvysílat	k5eAaPmAgInP	odvysílat
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
také	také	k9	také
první	první	k4xOgFnSc4	první
barevnou	barevný	k2eAgFnSc4d1	barevná
fotografii	fotografia	k1gFnSc4	fotografia
povrchu	povrch	k1gInSc2	povrch
Marsu	Mars	k1gInSc2	Mars
a	a	k8xC	a
orbitální	orbitální	k2eAgFnSc2d1	orbitální
sekce	sekce	k1gFnSc2	sekce
pořídily	pořídit	k5eAaPmAgFnP	pořídit
detailní	detailní	k2eAgFnPc1d1	detailní
fotografie	fotografia	k1gFnPc1	fotografia
povrchu	povrch	k1gInSc2	povrch
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
rozlišení	rozlišení	k1gNnSc6	rozlišení
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
některé	některý	k3yIgFnPc1	některý
části	část	k1gFnPc1	část
používány	používat	k5eAaImNgFnP	používat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
byly	být	k5eAaImAgFnP	být
vyslány	vyslat	k5eAaPmNgFnP	vyslat
dvě	dva	k4xCgFnPc1	dva
sovětské	sovětský	k2eAgFnPc1d1	sovětská
sondy	sonda	k1gFnPc1	sonda
Fobos	Fobos	k1gMnSc1	Fobos
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
měly	mít	k5eAaImAgInP	mít
studovat	studovat	k5eAaImF	studovat
Mars	Mars	k1gInSc4	Mars
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc4	jeho
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Bohužel	bohužel	k6eAd1	bohužel
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
Fobos	Fobos	k1gInSc1	Fobos
1	[number]	k4	1
odmlčel	odmlčet	k5eAaPmAgInS	odmlčet
již	již	k6eAd1	již
po	po	k7c6	po
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
Marsu	Mars	k1gInSc3	Mars
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Fobos	Fobos	k1gInSc1	Fobos
2	[number]	k4	2
pořídil	pořídit	k5eAaPmAgInS	pořídit
úspěšně	úspěšně	k6eAd1	úspěšně
fotografie	fotografia	k1gFnSc2	fotografia
Marsu	Mars	k1gInSc2	Mars
i	i	k8xC	i
Phobosu	Phobos	k1gInSc2	Phobos
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
před	před	k7c7	před
vysláním	vyslání	k1gNnSc7	vyslání
dvou	dva	k4xCgInPc2	dva
přistávacích	přistávací	k2eAgMnPc2d1	přistávací
modulů	modul	k1gInPc2	modul
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
měsíce	měsíc	k1gInSc2	měsíc
se	se	k3xPyFc4	se
porouchal	porouchat	k5eAaPmAgMnS	porouchat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
selhání	selhání	k1gNnSc6	selhání
sondy	sonda	k1gFnSc2	sonda
Mars	Mars	k1gMnSc1	Mars
Observer	Observer	k1gMnSc1	Observer
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
se	se	k3xPyFc4	se
roku	rok	k1gInSc6	rok
1996	[number]	k4	1996
k	k	k7c3	k
Marsu	Mars	k1gInSc3	Mars
dostala	dostat	k5eAaPmAgFnS	dostat
sonda	sonda	k1gFnSc1	sonda
Mars	Mars	k1gInSc1	Mars
Global	globat	k5eAaImAgMnS	globat
Surveyor	Surveyor	k1gMnSc1	Surveyor
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
úspěšně	úspěšně	k6eAd1	úspěšně
mapovala	mapovat	k5eAaImAgFnS	mapovat
povrch	povrch	k1gInSc4	povrch
planety	planeta	k1gFnSc2	planeta
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
třetím	třetí	k4xOgInSc6	třetí
prodloužení	prodloužení	k1gNnSc6	prodloužení
mise	mise	k1gFnSc2	mise
se	s	k7c7	s
sondou	sonda	k1gFnSc7	sonda
ztraceno	ztracen	k2eAgNnSc4d1	ztraceno
spojení	spojení	k1gNnSc4	spojení
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
po	po	k7c6	po
vyslání	vyslání	k1gNnSc6	vyslání
sondy	sonda	k1gFnSc2	sonda
Surveyor	Surveyora	k1gFnPc2	Surveyora
byla	být	k5eAaImAgFnS	být
vyslána	vyslat	k5eAaPmNgFnS	vyslat
další	další	k2eAgFnSc1d1	další
sonda	sonda	k1gFnSc1	sonda
Mars	Mars	k1gMnSc1	Mars
Pathfinder	Pathfinder	k1gMnSc1	Pathfinder
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
vysadit	vysadit	k5eAaPmF	vysadit
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
malé	malý	k2eAgNnSc4d1	malé
pojízdné	pojízdný	k2eAgNnSc4d1	pojízdné
vozítko	vozítko	k1gNnSc4	vozítko
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
by	by	kYmCp3nS	by
zkoumalo	zkoumat	k5eAaImAgNnS	zkoumat
okolí	okolí	k1gNnSc4	okolí
přistávacího	přistávací	k2eAgInSc2d1	přistávací
modulu	modul	k1gInSc2	modul
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Ares	Ares	k1gMnSc1	Ares
Vallis	Vallis	k1gFnSc1	Vallis
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
mise	mise	k1gFnSc1	mise
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
NASA	NASA	kA	NASA
obrovským	obrovský	k2eAgInSc7d1	obrovský
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
přinesla	přinést	k5eAaPmAgFnS	přinést
velkou	velký	k2eAgFnSc4d1	velká
řadu	řada	k1gFnSc4	řada
snímků	snímek	k1gInPc2	snímek
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
kterým	který	k3yRgNnSc7	který
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
obrovská	obrovský	k2eAgFnSc1d1	obrovská
publicita	publicita	k1gFnSc1	publicita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
NASA	NASA	kA	NASA
vyslala	vyslat	k5eAaPmAgFnS	vyslat
úspěšně	úspěšně	k6eAd1	úspěšně
sondu	sonda	k1gFnSc4	sonda
Mars	Mars	k1gInSc1	Mars
Odyssey	Odyssea	k1gFnPc1	Odyssea
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
na	na	k7c6	na
orbitě	orbita	k1gFnSc6	orbita
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
gama	gama	k1gNnSc2	gama
spektrometru	spektrometr	k1gInSc2	spektrometr
objevila	objevit	k5eAaPmAgFnS	objevit
známky	známka	k1gFnPc4	známka
vodíku	vodík	k1gInSc2	vodík
ve	v	k7c6	v
svrchních	svrchní	k2eAgInPc6d1	svrchní
metrech	metr	k1gInPc6	metr
marsovského	marsovský	k2eAgInSc2d1	marsovský
regolitu	regolit	k1gInSc2	regolit
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
vodík	vodík	k1gInSc1	vodík
je	být	k5eAaImIp3nS	být
vázán	vázat	k5eAaImNgInS	vázat
ve	v	k7c6	v
vodním	vodní	k2eAgInSc6d1	vodní
ledu	led	k1gInSc6	led
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
se	se	k3xPyFc4	se
k	k	k7c3	k
planetě	planeta	k1gFnSc3	planeta
vydala	vydat	k5eAaPmAgFnS	vydat
evropská	evropský	k2eAgFnSc1d1	Evropská
sonda	sonda	k1gFnSc1	sonda
Mars	Mars	k1gInSc1	Mars
Express	express	k1gInSc1	express
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
skládala	skládat	k5eAaImAgFnS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
orbitálního	orbitální	k2eAgInSc2d1	orbitální
modulu	modul	k1gInSc2	modul
Mars	Mars	k1gInSc1	Mars
Express	express	k1gInSc1	express
a	a	k8xC	a
přistávacího	přistávací	k2eAgInSc2d1	přistávací
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
Beagle	Beagle	k1gFnSc2	Beagle
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
mise	mise	k1gFnSc1	mise
byla	být	k5eAaImAgFnS	být
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
jen	jen	k9	jen
částečně	částečně	k6eAd1	částečně
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
přistávací	přistávací	k2eAgInSc1d1	přistávací
modul	modul	k1gInSc1	modul
z	z	k7c2	z
nezjištěných	zjištěný	k2eNgFnPc2d1	nezjištěná
příčin	příčina	k1gFnPc2	příčina
selhal	selhat	k5eAaPmAgInS	selhat
během	během	k7c2	během
přistávacího	přistávací	k2eAgInSc2d1	přistávací
manévru	manévr	k1gInSc2	manévr
a	a	k8xC	a
následně	následně	k6eAd1	následně
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2004	[number]	k4	2004
byl	být	k5eAaImAgMnS	být
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
ztracený	ztracený	k2eAgInSc4d1	ztracený
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
byl	být	k5eAaImAgInS	být
pomocí	pomocí	k7c2	pomocí
planetárního	planetární	k2eAgInSc2d1	planetární
fourierovského	fourierovský	k2eAgInSc2d1	fourierovský
spektrometru	spektrometr	k1gInSc2	spektrometr
pracující	pracující	k2eAgMnSc1d1	pracující
s	s	k7c7	s
infračerveným	infračervený	k2eAgNnSc7d1	infračervené
světlem	světlo	k1gNnSc7	světlo
ohlášen	ohlášen	k2eAgInSc4d1	ohlášen
nález	nález	k1gInSc4	nález
metanu	metan	k1gInSc2	metan
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2006	[number]	k4	2006
Evropská	evropský	k2eAgFnSc1d1	Evropská
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
agentura	agentura	k1gFnSc1	agentura
vydala	vydat	k5eAaPmAgFnS	vydat
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
objevila	objevit	k5eAaPmAgFnS	objevit
polární	polární	k2eAgFnSc4d1	polární
záři	záře	k1gFnSc4	záře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
se	se	k3xPyFc4	se
k	k	k7c3	k
Marsu	Mars	k1gInSc2	Mars
vydala	vydat	k5eAaPmAgFnS	vydat
i	i	k9	i
dvě	dva	k4xCgNnPc1	dva
stejná	stejný	k2eAgNnPc1d1	stejné
vozítka	vozítko	k1gNnPc1	vozítko
NASA	NASA	kA	NASA
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
projektu	projekt	k1gInSc2	projekt
Mars	Mars	k1gInSc1	Mars
Exploration	Exploration	k1gInSc1	Exploration
Rover	rover	k1gMnSc1	rover
-	-	kIx~	-
Spirit	Spirit	k1gInSc1	Spirit
(	(	kIx(	(
<g/>
MER-A	MER-A	k1gFnSc1	MER-A
<g/>
)	)	kIx)	)
a	a	k8xC	a
Opportunity	Opportunit	k2eAgFnPc1d1	Opportunit
(	(	kIx(	(
<g/>
MER-B	MER-B	k1gFnPc1	MER-B
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
dvě	dva	k4xCgNnPc1	dva
vozítka	vozítko	k1gNnPc1	vozítko
úspěšně	úspěšně	k6eAd1	úspěšně
přistála	přistát	k5eAaImAgNnP	přistát
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2004	[number]	k4	2004
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
zkoumat	zkoumat	k5eAaImF	zkoumat
místa	místo	k1gNnPc4	místo
dopadu	dopad	k1gInSc2	dopad
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
mechanického	mechanický	k2eAgNnSc2d1	mechanické
ramena	rameno	k1gNnSc2	rameno
očišťovat	očišťovat	k5eAaImF	očišťovat
vzorky	vzorek	k1gInPc4	vzorek
a	a	k8xC	a
analyzovat	analyzovat	k5eAaImF	analyzovat
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
největší	veliký	k2eAgInPc4d3	veliký
objevy	objev	k1gInPc4	objev
patří	patřit	k5eAaImIp3nP	patřit
důkaz	důkaz	k1gInSc4	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
kdysi	kdysi	k6eAd1	kdysi
skutečně	skutečně	k6eAd1	skutečně
byla	být	k5eAaImAgFnS	být
tekutá	tekutý	k2eAgFnSc1d1	tekutá
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sondy	sonda	k1gFnPc1	sonda
přistály	přistát	k5eAaImAgFnP	přistát
<g/>
.	.	kIx.	.
</s>
<s>
Vozítka	vozítko	k1gNnSc2	vozítko
měla	mít	k5eAaImAgFnS	mít
hlavní	hlavní	k2eAgFnSc4d1	hlavní
misi	mise	k1gFnSc4	mise
naplánovánu	naplánován	k2eAgFnSc4d1	naplánována
na	na	k7c4	na
90	[number]	k4	90
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
díky	díky	k7c3	díky
silnému	silný	k2eAgInSc3d1	silný
větru	vítr	k1gInSc3	vítr
a	a	k8xC	a
prachovým	prachový	k2eAgInPc3d1	prachový
vírům	vír	k1gInPc3	vír
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
čistí	čistit	k5eAaImIp3nP	čistit
solární	solární	k2eAgInPc1d1	solární
panely	panel	k1gInPc1	panel
vozítek	vozítko	k1gNnPc2	vozítko
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
mnohonásobně	mnohonásobně	k6eAd1	mnohonásobně
déle	dlouho	k6eAd2	dlouho
funkční	funkční	k2eAgFnPc1d1	funkční
<g/>
.	.	kIx.	.
</s>
<s>
Vozítko	vozítko	k1gNnSc1	vozítko
Spirit	Spirita	k1gFnPc2	Spirita
přestalo	přestat	k5eAaPmAgNnS	přestat
fungovat	fungovat	k5eAaImF	fungovat
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2010	[number]	k4	2010
po	po	k7c6	po
ujetí	ujetí	k1gNnSc6	ujetí
7,73	[number]	k4	7,73
km	km	kA	km
namísto	namísto	k7c2	namísto
plánovaných	plánovaný	k2eAgInPc2d1	plánovaný
600	[number]	k4	600
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dvojče	dvojče	k1gNnSc1	dvojče
Opportunity	Opportunita	k1gFnSc2	Opportunita
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
pojízdné	pojízdný	k2eAgNnSc1d1	pojízdné
a	a	k8xC	a
přináší	přinášet	k5eAaImIp3nS	přinášet
vědecká	vědecký	k2eAgNnPc4d1	vědecké
data	datum	k1gNnPc4	datum
(	(	kIx(	(
<g/>
informace	informace	k1gFnSc1	informace
k	k	k7c3	k
březnu	březen	k1gInSc3	březen
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
ujelo	ujet	k5eAaPmAgNnS	ujet
již	již	k6eAd1	již
přes	přes	k7c4	přes
42	[number]	k4	42
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2005	[number]	k4	2005
byla	být	k5eAaImAgFnS	být
vyslána	vyslat	k5eAaPmNgFnS	vyslat
další	další	k2eAgFnSc1d1	další
americká	americký	k2eAgFnSc1d1	americká
sonda	sonda	k1gFnSc1	sonda
Mars	Mars	k1gInSc1	Mars
Reconnaissance	Reconnaissance	k1gFnSc2	Reconnaissance
Orbiter	Orbitra	k1gFnPc2	Orbitra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
planety	planeta	k1gFnSc2	planeta
dostala	dostat	k5eAaPmAgFnS	dostat
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
úkolem	úkol	k1gInSc7	úkol
plánované	plánovaný	k2eAgFnSc2d1	plánovaná
dvouleté	dvouletý	k2eAgFnSc2d1	dvouletá
vědecké	vědecký	k2eAgFnSc2d1	vědecká
mise	mise	k1gFnSc2	mise
je	být	k5eAaImIp3nS	být
zmapovat	zmapovat	k5eAaPmF	zmapovat
povrch	povrch	k1gInSc4	povrch
Marsu	Mars	k1gInSc2	Mars
a	a	k8xC	a
studovat	studovat	k5eAaImF	studovat
počasí	počasí	k1gNnSc4	počasí
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
vybrat	vybrat	k5eAaPmF	vybrat
vhodné	vhodný	k2eAgNnSc4d1	vhodné
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
další	další	k2eAgFnPc4d1	další
sondy	sonda	k1gFnPc4	sonda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
přistát	přistát	k5eAaPmF	přistát
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
telekomunikační	telekomunikační	k2eAgNnSc4d1	telekomunikační
zařízení	zařízení	k1gNnSc4	zařízení
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
přenosovou	přenosový	k2eAgFnSc7d1	přenosová
rychlostí	rychlost	k1gFnSc7	rychlost
než	než	k8xS	než
všechny	všechen	k3xTgFnPc4	všechen
předchozí	předchozí	k2eAgFnPc4d1	předchozí
sondy	sonda	k1gFnPc4	sonda
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
2008	[number]	k4	2008
úspěšně	úspěšně	k6eAd1	úspěšně
přistála	přistát	k5eAaImAgFnS	přistát
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
nepohyblivá	pohyblivý	k2eNgFnSc1d1	nepohyblivá
americká	americký	k2eAgFnSc1d1	americká
sonda	sonda	k1gFnSc1	sonda
Phoenix	Phoenix	k1gInSc1	Phoenix
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
svojí	svůj	k3xOyFgFnSc6	svůj
cestu	cesta	k1gFnSc4	cesta
vyslána	vyslán	k2eAgFnSc1d1	vyslána
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Přistála	přistát	k5eAaImAgFnS	přistát
poblíž	poblíž	k7c2	poblíž
severní	severní	k2eAgFnSc2d1	severní
polární	polární	k2eAgFnSc2d1	polární
čepičky	čepička	k1gFnSc2	čepička
<g/>
.	.	kIx.	.
</s>
<s>
Přistávací	přistávací	k2eAgInSc1d1	přistávací
modul	modul	k1gInSc1	modul
byl	být	k5eAaImAgInS	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
robotickou	robotický	k2eAgFnSc7d1	robotická
rukou	ruka	k1gFnSc7	ruka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
odebrat	odebrat	k5eAaPmF	odebrat
vzorky	vzorek	k1gInPc4	vzorek
až	až	k9	až
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
2,5	[number]	k4	2,5
metru	metr	k1gInSc2	metr
a	a	k8xC	a
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
až	až	k9	až
metr	metr	k1gInSc4	metr
pod	pod	k7c4	pod
marsovský	marsovský	k2eAgInSc4d1	marsovský
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
sonda	sonda	k1gFnSc1	sonda
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
přistání	přistání	k1gNnSc6	přistání
vodní	vodní	k2eAgInSc4d1	vodní
led	led	k1gInSc4	led
nehluboko	hluboko	k6eNd1	hluboko
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
ruská	ruský	k2eAgFnSc1d1	ruská
mise	mise	k1gFnSc1	mise
Fobos-Grunt	Fobos-Grunta	k1gFnPc2	Fobos-Grunta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
si	se	k3xPyFc3	se
kladla	klást	k5eAaImAgFnS	klást
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
dopravit	dopravit	k5eAaPmF	dopravit
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
Zem	zem	k1gFnSc4	zem
vzorky	vzorek	k1gInPc1	vzorek
z	z	k7c2	z
měsíce	měsíc	k1gInSc2	měsíc
Phobos	Phobosa	k1gFnPc2	Phobosa
<g/>
.	.	kIx.	.
</s>
<s>
Nosná	nosný	k2eAgFnSc1d1	nosná
raketa	raketa	k1gFnSc1	raketa
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
oddělení	oddělení	k1gNnSc6	oddělení
druhého	druhý	k4xOgInSc2	druhý
stupně	stupeň	k1gInSc2	stupeň
však	však	k9	však
nedošlo	dojít	k5eNaPmAgNnS	dojít
ke	k	k7c3	k
spuštění	spuštění	k1gNnSc3	spuštění
motoru	motor	k1gInSc2	motor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
sondu	sonda	k1gFnSc4	sonda
navést	navést	k5eAaPmF	navést
na	na	k7c4	na
dráhu	dráha	k1gFnSc4	dráha
k	k	k7c3	k
Marsu	Mars	k1gInSc3	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
zůstala	zůstat	k5eAaPmAgFnS	zůstat
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2011	[number]	k4	2011
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
start	start	k1gInSc4	start
nosné	nosný	k2eAgFnSc2d1	nosná
rakety	raketa	k1gFnSc2	raketa
Atlas	Atlas	k1gMnSc1	Atlas
V541	V541	k1gMnSc1	V541
se	s	k7c7	s
sondou	sonda	k1gFnSc7	sonda
Mars	Mars	k1gMnSc1	Mars
Science	Science	k1gFnSc2	Science
Laboratory	Laborator	k1gInPc4	Laborator
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
sofistikovanou	sofistikovaný	k2eAgFnSc4d1	sofistikovaná
pojízdnou	pojízdný	k2eAgFnSc4d1	pojízdná
laboratoř	laboratoř	k1gFnSc4	laboratoř
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
,	,	kIx,	,
hledat	hledat	k5eAaImF	hledat
organické	organický	k2eAgFnPc1d1	organická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
či	či	k8xC	či
stopy	stopa	k1gFnPc1	stopa
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
úspěšně	úspěšně	k6eAd1	úspěšně
přistála	přistát	k5eAaPmAgFnS	přistát
dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rok	rok	k1gInSc4	rok
2018	[number]	k4	2018
plánuje	plánovat	k5eAaImIp3nS	plánovat
Evropská	evropský	k2eAgFnSc1d1	Evropská
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
agentura	agentura	k1gFnSc1	agentura
svoje	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
vozítko	vozítko	k1gNnSc4	vozítko
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
ExoMars	ExoMars	k1gInSc1	ExoMars
<g/>
;	;	kIx,	;
měl	mít	k5eAaImAgInS	mít
by	by	kYmCp3nS	by
být	být	k5eAaImF	být
schopný	schopný	k2eAgMnSc1d1	schopný
kopat	kopat	k5eAaImF	kopat
až	až	k9	až
dva	dva	k4xCgInPc4	dva
metry	metr	k1gInPc4	metr
pod	pod	k7c4	pod
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
hledal	hledat	k5eAaImAgMnS	hledat
organické	organický	k2eAgFnPc4d1	organická
molekuly	molekula	k1gFnPc4	molekula
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
George	Georg	k1gMnSc2	Georg
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
dlouhodobý	dlouhodobý	k2eAgInSc4d1	dlouhodobý
plán	plán	k1gInSc4	plán
Vision	vision	k1gInSc4	vision
for	forum	k1gNnPc2	forum
Space	Space	k1gMnSc2	Space
Exploration	Exploration	k1gInSc4	Exploration
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
USA	USA	kA	USA
připravují	připravovat	k5eAaImIp3nP	připravovat
vyslat	vyslat	k5eAaPmF	vyslat
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
Marsu	Mars	k1gInSc2	Mars
pilotovanou	pilotovaný	k2eAgFnSc4d1	pilotovaná
loď	loď	k1gFnSc4	loď
a	a	k8xC	a
na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
povrch	povrch	k1gInSc4	povrch
vysadit	vysadit	k5eAaPmF	vysadit
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgInPc4d1	podobný
plány	plán	k1gInPc4	plán
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
Evropská	evropský	k2eAgFnSc1d1	Evropská
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
agentura	agentura	k1gFnSc1	agentura
sdružující	sdružující	k2eAgFnSc2d1	sdružující
evropské	evropský	k2eAgFnSc2d1	Evropská
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
chtěla	chtít	k5eAaImAgFnS	chtít
dostat	dostat	k5eAaPmF	dostat
člověka	člověk	k1gMnSc4	člověk
na	na	k7c4	na
Mars	Mars	k1gInSc4	Mars
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
2025	[number]	k4	2025
až	až	k9	až
2030	[number]	k4	2030
<g/>
.	.	kIx.	.
</s>
<s>
Obdobné	obdobný	k2eAgFnPc4d1	obdobná
ambice	ambice	k1gFnPc4	ambice
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Život	život	k1gInSc1	život
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgNnSc1d1	současné
poznání	poznání	k1gNnSc1	poznání
historie	historie	k1gFnSc2	historie
Marsu	Mars	k1gInSc2	Mars
nasvědčuje	nasvědčovat	k5eAaImIp3nS	nasvědčovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
vzniku	vznik	k1gInSc6	vznik
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
nacházela	nacházet	k5eAaImAgFnS	nacházet
hustá	hustý	k2eAgFnSc1d1	hustá
atmosféra	atmosféra	k1gFnSc1	atmosféra
a	a	k8xC	a
kapalná	kapalný	k2eAgFnSc1d1	kapalná
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
možná	možná	k9	možná
tvořila	tvořit	k5eAaImAgFnS	tvořit
i	i	k8xC	i
celoplanetární	celoplanetární	k2eAgInSc1d1	celoplanetární
oceán	oceán	k1gInSc1	oceán
pokrývající	pokrývající	k2eAgFnSc4d1	pokrývající
převážnou	převážný	k2eAgFnSc4d1	převážná
část	část	k1gFnSc4	část
severní	severní	k2eAgFnSc2d1	severní
polokoule	polokoule	k1gFnSc2	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
současné	současný	k2eAgFnSc2d1	současná
teorie	teorie	k1gFnSc2	teorie
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
života	život	k1gInSc2	život
tím	ten	k3xDgNnSc7	ten
byla	být	k5eAaImAgFnS	být
splněna	splnit	k5eAaPmNgFnS	splnit
základní	základní	k2eAgFnSc1d1	základní
podmínka	podmínka	k1gFnSc1	podmínka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mohla	moct	k5eAaImAgFnS	moct
vytvořit	vytvořit	k5eAaPmF	vytvořit
obyvatelnou	obyvatelný	k2eAgFnSc4d1	obyvatelná
zónu	zóna	k1gFnSc4	zóna
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
a	a	k8xC	a
umožnit	umožnit	k5eAaPmF	umožnit
tak	tak	k9	tak
vznik	vznik	k1gInSc4	vznik
primitivního	primitivní	k2eAgInSc2d1	primitivní
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
proti	proti	k7c3	proti
vzniku	vznik	k1gInSc3	vznik
života	život	k1gInSc2	život
hovoří	hovořit	k5eAaImIp3nS	hovořit
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
příznivé	příznivý	k2eAgFnPc1d1	příznivá
podmínky	podmínka	k1gFnPc1	podmínka
trvaly	trvat	k5eAaImAgFnP	trvat
pouze	pouze	k6eAd1	pouze
dočasně	dočasně	k6eAd1	dočasně
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
všechna	všechen	k3xTgFnSc1	všechen
voda	voda	k1gFnSc1	voda
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
zmrzlá	zmrzlý	k2eAgFnSc1d1	zmrzlá
<g/>
,	,	kIx,	,
a	a	k8xC	a
planeta	planeta	k1gFnSc1	planeta
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nachází	nacházet	k5eAaImIp3nS	nacházet
mimo	mimo	k7c4	mimo
obyvatelnou	obyvatelný	k2eAgFnSc4d1	obyvatelná
zónu	zóna	k1gFnSc4	zóna
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
pro	pro	k7c4	pro
případný	případný	k2eAgInSc4d1	případný
vznik	vznik	k1gInSc4	vznik
života	život	k1gInSc2	život
musely	muset	k5eAaImAgFnP	muset
být	být	k5eAaImF	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
jiné	jiný	k2eAgInPc4d1	jiný
energetické	energetický	k2eAgInPc4d1	energetický
zdroje	zdroj	k1gInPc4	zdroj
než	než	k8xS	než
energie	energie	k1gFnPc4	energie
Slunce	slunce	k1gNnSc2	slunce
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vulkanismus	vulkanismus	k1gInSc1	vulkanismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slabá	slabý	k2eAgFnSc1d1	slabá
magnetosféra	magnetosféra	k1gFnSc1	magnetosféra
a	a	k8xC	a
extrémně	extrémně	k6eAd1	extrémně
tenká	tenký	k2eAgFnSc1d1	tenká
atmosféra	atmosféra	k1gFnSc1	atmosféra
<g/>
,	,	kIx,	,
veliké	veliký	k2eAgInPc1d1	veliký
výkyvy	výkyv	k1gInPc1	výkyv
teplot	teplota	k1gFnPc2	teplota
<g/>
,	,	kIx,	,
ukončení	ukončení	k1gNnSc1	ukončení
současné	současný	k2eAgFnSc2d1	současná
vulkanické	vulkanický	k2eAgFnSc2d1	vulkanická
činnosti	činnost	k1gFnSc2	činnost
a	a	k8xC	a
bombardování	bombardování	k1gNnSc2	bombardování
povrchu	povrch	k1gInSc6	povrch
meteory	meteor	k1gInPc4	meteor
nedávají	dávat	k5eNaImIp3nP	dávat
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
příliš	příliš	k6eAd1	příliš
mnoho	mnoho	k4c1	mnoho
nadějí	naděje	k1gFnPc2	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
život	život	k1gInSc1	život
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
<g/>
)	)	kIx)	)
mohl	moct	k5eAaImAgInS	moct
přežít	přežít	k5eAaPmF	přežít
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
vědci	vědec	k1gMnPc1	vědec
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
jsou	být	k5eAaImIp3nP	být
neustále	neustále	k6eAd1	neustále
překvapování	překvapování	k1gNnSc4	překvapování
podmínkami	podmínka	k1gFnPc7	podmínka
<g/>
,	,	kIx,	,
za	za	k7c2	za
kterých	který	k3yIgInPc2	který
může	moct	k5eAaImIp3nS	moct
život	život	k1gInSc1	život
přežívat	přežívat	k5eAaImF	přežívat
(	(	kIx(	(
<g/>
radioaktivita	radioaktivita	k1gFnSc1	radioaktivita
<g/>
,	,	kIx,	,
život	život	k1gInSc1	život
v	v	k7c6	v
naprosté	naprostý	k2eAgFnSc6d1	naprostá
temnotě	temnota	k1gFnSc6	temnota
<g/>
,	,	kIx,	,
život	život	k1gInSc4	život
bez	bez	k7c2	bez
dýchatelného	dýchatelný	k2eAgInSc2d1	dýchatelný
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2014	[number]	k4	2014
<g/>
-	-	kIx~	-
<g/>
2015	[number]	k4	2015
probíhaly	probíhat	k5eAaImAgInP	probíhat
na	na	k7c6	na
Mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
vesmírné	vesmírný	k2eAgFnSc6d1	vesmírná
stanici	stanice	k1gFnSc6	stanice
experimenty	experiment	k1gInPc4	experiment
s	s	k7c7	s
antarktickými	antarktický	k2eAgFnPc7d1	antarktická
houbami	houba	k1gFnPc7	houba
rodu	rod	k1gInSc2	rod
Cryomyces	Cryomycesa	k1gFnPc2	Cryomycesa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vesmírné	vesmírný	k2eAgFnSc6d1	vesmírná
stanici	stanice	k1gFnSc6	stanice
byly	být	k5eAaImAgFnP	být
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
18	[number]	k4	18
měsíců	měsíc	k1gInPc2	měsíc
vystaveny	vystavit	k5eAaPmNgFnP	vystavit
stejné	stejný	k2eAgFnPc1d1	stejná
atmosféře	atmosféra	k1gFnSc6	atmosféra
i	i	k9	i
silnému	silný	k2eAgNnSc3d1	silné
ultrafialovému	ultrafialový	k2eAgNnSc3d1	ultrafialové
záření	záření	k1gNnSc3	záření
<g/>
,	,	kIx,	,
s	s	k7c7	s
jakým	jaký	k3yQgNnSc7	jaký
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
střetly	střetnout	k5eAaPmAgFnP	střetnout
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
testu	test	k1gInSc2	test
přežilo	přežít	k5eAaPmAgNnS	přežít
60	[number]	k4	60
%	%	kIx~	%
všech	všecek	k3xTgFnPc2	všecek
houbových	houbový	k2eAgFnPc2d1	houbová
buněk	buňka	k1gFnPc2	buňka
a	a	k8xC	a
každá	každý	k3xTgFnSc1	každý
desátá	desátý	k4xOgFnSc1	desátý
buňka	buňka	k1gFnSc1	buňka
byla	být	k5eAaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
se	se	k3xPyFc4	se
množit	množit	k5eAaImF	množit
a	a	k8xC	a
vytvářet	vytvářet	k5eAaImF	vytvářet
nové	nový	k2eAgFnPc4d1	nová
kolonie	kolonie	k1gFnPc4	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
potvrzení	potvrzení	k1gNnSc4	potvrzení
či	či	k8xC	či
vyvrácení	vyvrácení	k1gNnSc4	vyvrácení
teorie	teorie	k1gFnSc2	teorie
o	o	k7c6	o
životě	život	k1gInSc6	život
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
chybějí	chybět	k5eAaImIp3nP	chybět
zatím	zatím	k6eAd1	zatím
jasné	jasný	k2eAgInPc1d1	jasný
důkazy	důkaz	k1gInPc1	důkaz
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
sice	sice	k8xC	sice
některé	některý	k3yIgInPc1	některý
náznaky	náznak	k1gInPc1	náznak
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nasvědčují	nasvědčovat	k5eAaImIp3nP	nasvědčovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
život	život	k1gInSc1	život
skutečně	skutečně	k6eAd1	skutečně
byl	být	k5eAaImAgInS	být
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
struktury	struktura	k1gFnSc2	struktura
připomínající	připomínající	k2eAgInPc1d1	připomínající
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
činnosti	činnost	k1gFnSc2	činnost
organismů	organismus	k1gInPc2	organismus
v	v	k7c6	v
meteoritu	meteorit	k1gInSc6	meteorit
ALH	ALH	kA	ALH
84001	[number]	k4	84001
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
planety	planeta	k1gFnSc2	planeta
provedlo	provést	k5eAaPmAgNnS	provést
několik	několik	k4yIc1	několik
sond	sonda	k1gFnPc2	sonda
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Viking	Viking	k1gMnSc1	Viking
<g/>
)	)	kIx)	)
experimenty	experiment	k1gInPc1	experiment
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
měly	mít	k5eAaImAgInP	mít
objevit	objevit	k5eAaPmF	objevit
důkazy	důkaz	k1gInPc4	důkaz
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tyto	tento	k3xDgInPc1	tento
pokusy	pokus	k1gInPc1	pokus
nepřinesly	přinést	k5eNaPmAgFnP	přinést
žádný	žádný	k3yNgInSc4	žádný
důkaz	důkaz	k1gInSc4	důkaz
potvrzující	potvrzující	k2eAgInSc1d1	potvrzující
život	život	k1gInSc1	život
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
nyní	nyní	k6eAd1	nyní
ani	ani	k8xC	ani
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
zavlečení	zavlečení	k1gNnSc4	zavlečení
pozemského	pozemský	k2eAgInSc2d1	pozemský
života	život	k1gInSc2	život
na	na	k7c4	na
Mars	Mars	k1gInSc4	Mars
jsou	být	k5eAaImIp3nP	být
sondy	sonda	k1gFnPc1	sonda
určené	určený	k2eAgFnPc1d1	určená
pro	pro	k7c4	pro
přistání	přistání	k1gNnSc4	přistání
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
pečlivě	pečlivě	k6eAd1	pečlivě
sterilizovány	sterilizován	k2eAgInPc1d1	sterilizován
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
výzkumu	výzkum	k1gInSc2	výzkum
nebyly	být	k5eNaImAgFnP	být
všechny	všechen	k3xTgFnPc1	všechen
sondy	sonda	k1gFnPc1	sonda
sterilizovány	sterilizovat	k5eAaImNgFnP	sterilizovat
příliš	příliš	k6eAd1	příliš
pečlivě	pečlivě	k6eAd1	pečlivě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jasnou	jasný	k2eAgFnSc4d1	jasná
odpověď	odpověď	k1gFnSc4	odpověď
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
skutečně	skutečně	k6eAd1	skutečně
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
život	život	k1gInSc1	život
anebo	anebo	k8xC	anebo
jestli	jestli	k8xS	jestli
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
fikci	fikce	k1gFnSc4	fikce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
počkat	počkat	k5eAaPmF	počkat
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nebude	být	k5eNaImBp3nS	být
pečlivě	pečlivě	k6eAd1	pečlivě
prostudována	prostudován	k2eAgFnSc1d1	prostudována
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
povrchu	povrch	k1gInSc2	povrch
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kolonizace	kolonizace	k1gFnSc2	kolonizace
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Lidská	lidský	k2eAgFnSc1d1	lidská
kolonizace	kolonizace	k1gFnSc1	kolonizace
Marsu	Mars	k1gInSc2	Mars
je	být	k5eAaImIp3nS	být
cílem	cíl	k1gInSc7	cíl
mnoha	mnoho	k4c2	mnoho
spekulací	spekulace	k1gFnPc2	spekulace
i	i	k8xC	i
seriózních	seriózní	k2eAgFnPc2d1	seriózní
studií	studie	k1gFnPc2	studie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
výzkumu	výzkum	k1gInSc2	výzkum
této	tento	k3xDgFnSc2	tento
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Povrchové	povrchový	k2eAgFnPc1d1	povrchová
podmínky	podmínka	k1gFnPc1	podmínka
a	a	k8xC	a
snadná	snadný	k2eAgFnSc1d1	snadná
dostupnost	dostupnost	k1gFnSc1	dostupnost
vody	voda	k1gFnSc2	voda
dělají	dělat	k5eAaImIp3nP	dělat
z	z	k7c2	z
Marsu	Mars	k1gInSc2	Mars
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejlépe	dobře	k6eAd3	dobře
obyvatelných	obyvatelný	k2eAgFnPc2d1	obyvatelná
planet	planeta	k1gFnPc2	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
bude	být	k5eAaImBp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
dalším	další	k2eAgInSc7d1	další
cílem	cíl	k1gInSc7	cíl
lidské	lidský	k2eAgFnSc2d1	lidská
expanze	expanze	k1gFnSc2	expanze
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
nejnovějších	nový	k2eAgInPc2d3	nejnovější
záměrů	záměr	k1gInPc2	záměr
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgMnS	mít
člověk	člověk	k1gMnSc1	člověk
na	na	k7c4	na
Mars	Mars	k1gInSc4	Mars
vypravit	vypravit	k5eAaPmF	vypravit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2030	[number]	k4	2030
a	a	k8xC	a
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
zde	zde	k6eAd1	zde
začít	začít	k5eAaPmF	začít
budovat	budovat	k5eAaImF	budovat
stálou	stálý	k2eAgFnSc4d1	stálá
základnu	základna	k1gFnSc4	základna
<g/>
.	.	kIx.	.
</s>
<s>
Mars	Mars	k1gInSc1	Mars
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
méně	málo	k6eAd2	málo
energie	energie	k1gFnSc1	energie
na	na	k7c4	na
jednotku	jednotka	k1gFnSc4	jednotka
hmotnosti	hmotnost	k1gFnSc2	hmotnost
(	(	kIx(	(
<g/>
delta-v	delta	k1gInSc1	delta-v
<g/>
)	)	kIx)	)
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
dosažení	dosažení	k1gNnSc3	dosažení
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
než	než	k8xS	než
kterákoli	kterýkoli	k3yIgFnSc1	kterýkoli
jiná	jiný	k2eAgFnSc1d1	jiná
planeta	planeta	k1gFnSc1	planeta
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
využitím	využití	k1gNnSc7	využití
Hohmannovy	Hohmannův	k2eAgFnSc2d1	Hohmannova
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
trvá	trvat	k5eAaImIp3nS	trvat
let	let	k1gInSc4	let
k	k	k7c3	k
Marsu	Mars	k1gInSc2	Mars
přibližně	přibližně	k6eAd1	přibližně
9	[number]	k4	9
měsíců	měsíc	k1gInPc2	měsíc
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
druhu	druh	k1gInSc6	druh
použitého	použitý	k2eAgInSc2d1	použitý
pohonu	pohon	k1gInSc2	pohon
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterých	který	k3yIgFnPc2	který
bude	být	k5eAaImBp3nS	být
posádka	posádka	k1gFnSc1	posádka
vystavena	vystavit	k5eAaPmNgFnS	vystavit
stavu	stav	k1gInSc2	stav
beztíže	beztíže	k1gFnPc1	beztíže
<g/>
.	.	kIx.	.
</s>
<s>
Kratší	krátký	k2eAgFnSc1d2	kratší
doba	doba	k1gFnSc1	doba
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
6	[number]	k4	6
až	až	k9	až
7	[number]	k4	7
měsíců	měsíc	k1gInPc2	měsíc
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
došlo	dojít	k5eAaPmAgNnS	dojít
by	by	kYmCp3nS	by
ke	k	k7c3	k
spotřebě	spotřeba	k1gFnSc3	spotřeba
většího	veliký	k2eAgNnSc2d2	veliký
množství	množství	k1gNnSc2	množství
paliva	palivo	k1gNnSc2	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Otevřenou	otevřený	k2eAgFnSc7d1	otevřená
otázkou	otázka	k1gFnSc7	otázka
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
lidstvo	lidstvo	k1gNnSc1	lidstvo
bude	být	k5eAaImBp3nS	být
odsouzeno	odsoudit	k5eAaPmNgNnS	odsoudit
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
žít	žít	k5eAaImF	žít
v	v	k7c6	v
uzavřených	uzavřený	k2eAgFnPc6d1	uzavřená
základnách	základna	k1gFnPc6	základna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
uměle	uměle	k6eAd1	uměle
udržovat	udržovat	k5eAaImF	udržovat
atmosféra	atmosféra	k1gFnSc1	atmosféra
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nS	podařit
přeměnit	přeměnit	k5eAaPmF	přeměnit
povrch	povrch	k1gInSc4	povrch
planety	planeta	k1gFnSc2	planeta
na	na	k7c4	na
obyvatelný	obyvatelný	k2eAgInSc4d1	obyvatelný
pomocí	pomocí	k7c2	pomocí
terraformace	terraformace	k1gFnSc2	terraformace
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Terraformace	Terraformace	k1gFnSc2	Terraformace
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Terraformace	Terraformace	k1gFnSc1	Terraformace
Marsu	Mars	k1gInSc2	Mars
je	být	k5eAaImIp3nS	být
hypotetický	hypotetický	k2eAgInSc4d1	hypotetický
soubor	soubor	k1gInSc4	soubor
procesů	proces	k1gInPc2	proces
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
umožnit	umožnit	k5eAaPmF	umožnit
člověku	člověk	k1gMnSc3	člověk
život	život	k1gInSc1	život
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Marsu	Mars	k1gInSc2	Mars
bez	bez	k7c2	bez
nutnosti	nutnost	k1gFnSc2	nutnost
používat	používat	k5eAaImF	používat
ochranné	ochranný	k2eAgInPc4d1	ochranný
prostředky	prostředek	k1gInPc4	prostředek
před	před	k7c7	před
okolním	okolní	k2eAgNnSc7d1	okolní
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
výsledkem	výsledek	k1gInSc7	výsledek
by	by	kYmCp3nS	by
tak	tak	k9	tak
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
vznik	vznik	k1gInSc4	vznik
planety	planeta	k1gFnSc2	planeta
podobné	podobný	k2eAgFnSc2d1	podobná
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
teoreticky	teoreticky	k6eAd1	teoreticky
změnit	změnit	k5eAaPmF	změnit
celou	celý	k2eAgFnSc4d1	celá
planetu	planeta	k1gFnSc4	planeta
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
probíhal	probíhat	k5eAaImAgInS	probíhat
po	po	k7c4	po
desítky	desítka	k1gFnPc4	desítka
či	či	k8xC	či
stovky	stovka	k1gFnPc4	stovka
let	léto	k1gNnPc2	léto
od	od	k7c2	od
nejjednodušších	jednoduchý	k2eAgInPc2d3	nejjednodušší
organismů	organismus	k1gInPc2	organismus
přes	přes	k7c4	přes
rostliny	rostlina	k1gFnPc4	rostlina
až	až	k6eAd1	až
po	po	k7c4	po
první	první	k4xOgMnPc4	první
živočichy	živočich	k1gMnPc4	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
Mars	Mars	k1gInSc1	Mars
rozdílný	rozdílný	k2eAgInSc1d1	rozdílný
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
menší	malý	k2eAgFnSc4d2	menší
gravitaci	gravitace	k1gFnSc4	gravitace
<g/>
,	,	kIx,	,
podmínky	podmínka	k1gFnPc1	podmínka
nebudou	být	k5eNaImBp3nP	být
nikdy	nikdy	k6eAd1	nikdy
zcela	zcela	k6eAd1	zcela
shodné	shodný	k2eAgNnSc1d1	shodné
s	s	k7c7	s
těmi	ten	k3xDgMnPc7	ten
pozemskými	pozemský	k2eAgMnPc7d1	pozemský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
spíše	spíše	k9	spíše
o	o	k7c4	o
sci-fi	scii	k1gFnSc4	sci-fi
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
neexistuje	existovat	k5eNaImIp3nS	existovat
žádná	žádný	k3yNgFnSc1	žádný
dostupná	dostupný	k2eAgFnSc1d1	dostupná
technologie	technologie	k1gFnSc1	technologie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
tuto	tento	k3xDgFnSc4	tento
přeměnu	přeměna	k1gFnSc4	přeměna
zvládla	zvládnout	k5eAaPmAgFnS	zvládnout
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
již	již	k6eAd1	již
občas	občas	k6eAd1	občas
objevují	objevovat	k5eAaImIp3nP	objevovat
nápady	nápad	k1gInPc4	nápad
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
povrch	povrch	k7c2wR	povrch
Marsu	Mars	k1gInSc2	Mars
přeměnit	přeměnit	k5eAaPmF	přeměnit
<g/>
.	.	kIx.	.
</s>
<s>
Mars	Mars	k1gInSc1	Mars
je	být	k5eAaImIp3nS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
po	po	k7c6	po
římském	římský	k2eAgMnSc6d1	římský
bohu	bůh	k1gMnSc6	bůh
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Setkáváme	setkávat	k5eAaImIp1nP	setkávat
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
v	v	k7c6	v
římské	římský	k2eAgFnSc6d1	římská
mytologii	mytologie	k1gFnSc6	mytologie
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Mars	Mars	k1gInSc1	Mars
(	(	kIx(	(
<g/>
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
babylónské	babylónský	k2eAgFnSc6d1	Babylónská
astronomii	astronomie	k1gFnSc6	astronomie
byla	být	k5eAaImAgFnS	být
planeta	planeta	k1gFnSc1	planeta
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
po	po	k7c6	po
Nergalovi	Nergal	k1gMnSc6	Nergal
<g/>
,	,	kIx,	,
božstvu	božstvo	k1gNnSc6	božstvo
ohně	oheň	k1gInSc2	oheň
<g/>
,	,	kIx,	,
války	válka	k1gFnSc2	válka
a	a	k8xC	a
ničení	ničení	k1gNnSc4	ničení
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
díky	díky	k7c3	díky
rudé	rudý	k2eAgFnSc3d1	rudá
barvě	barva	k1gFnSc3	barva
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Řekové	Řek	k1gMnPc1	Řek
spojili	spojit	k5eAaPmAgMnP	spojit
Nergala	Nergal	k1gMnSc4	Nergal
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
bohem	bůh	k1gMnSc7	bůh
války	válka	k1gFnSc2	válka
Aréem	Aré	k1gMnSc7	Aré
<g/>
,	,	kIx,	,
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
planetu	planeta	k1gFnSc4	planeta
Ἄ	Ἄ	k?	Ἄ
ἀ	ἀ	k?	ἀ
(	(	kIx(	(
<g/>
Areos	Areos	k1gInSc1	Areos
aster	aster	k1gInSc1	aster
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
"	"	kIx"	"
<g/>
hvězda	hvězda	k1gFnSc1	hvězda
Areova	Areův	k2eAgFnSc1d1	Areův
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ztotožnili	ztotožnit	k5eAaPmAgMnP	ztotožnit
svého	svůj	k3xOyFgMnSc4	svůj
boha	bůh	k1gMnSc4	bůh
války	válka	k1gFnSc2	válka
Marta	Marta	k1gFnSc1	Marta
s	s	k7c7	s
řeckým	řecký	k2eAgInSc7d1	řecký
Aréem	Aré	k1gInSc7	Aré
<g/>
,	,	kIx,	,
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
také	také	k6eAd1	také
spojovali	spojovat	k5eAaImAgMnP	spojovat
planetu	planeta	k1gFnSc4	planeta
a	a	k8xC	a
říkali	říkat	k5eAaImAgMnP	říkat
jí	on	k3xPp3gFnSc3	on
"	"	kIx"	"
<g/>
Stella	Stella	k1gFnSc1	Stella
Martis	Martis	k1gFnSc2	Martis
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
"	"	kIx"	"
<g/>
Hvězda	hvězda	k1gFnSc1	hvězda
Martova	Martův	k2eAgFnSc1d1	Martova
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
"	"	kIx"	"
<g/>
Mars	Mars	k1gInSc1	Mars
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Řekové	Řek	k1gMnPc1	Řek
označovali	označovat	k5eAaImAgMnP	označovat
planetu	planeta	k1gFnSc4	planeta
i	i	k8xC	i
jako	jako	k9	jako
Π	Π	k?	Π
(	(	kIx(	(
<g/>
Pyroeis	Pyroeis	k1gFnSc1	Pyroeis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
přibližně	přibližně	k6eAd1	přibližně
hořící	hořící	k2eAgFnSc1d1	hořící
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hinduistické	hinduistický	k2eAgFnSc6d1	hinduistická
mytologii	mytologie	k1gFnSc6	mytologie
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
Mars	Mars	k1gInSc1	Mars
jako	jako	k8xC	jako
Mangala	Mangala	k1gFnSc1	Mangala
(	(	kIx(	(
<g/>
म	म	k?	म
<g/>
ं	ं	k?	ं
<g/>
ग	ग	k?	ग
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
spojován	spojovat	k5eAaImNgInS	spojovat
s	s	k7c7	s
bohem	bůh	k1gMnSc7	bůh
války	válka	k1gFnSc2	válka
v	v	k7c4	v
sanskrtu	sanskrta	k1gFnSc4	sanskrta
také	také	k9	také
jako	jako	k8xS	jako
Angaraka	Angarak	k1gMnSc2	Angarak
podle	podle	k7c2	podle
boha	bůh	k1gMnSc2	bůh
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnPc7	jehož
znameními	znamení	k1gNnPc7	znamení
byly	být	k5eAaImAgFnP	být
kozoroh	kozoroh	k1gMnSc1	kozoroh
a	a	k8xC	a
štír	štír	k1gMnSc1	štír
a	a	k8xC	a
který	který	k3yRgMnSc1	který
učil	učit	k5eAaImAgMnS	učit
okultním	okultní	k2eAgFnPc3d1	okultní
vědám	věda	k1gFnPc3	věda
<g/>
.	.	kIx.	.
</s>
<s>
Staří	starý	k2eAgMnPc1d1	starý
Egypťané	Egypťan	k1gMnPc1	Egypťan
tuto	tento	k3xDgFnSc4	tento
planetu	planeta	k1gFnSc4	planeta
nazývali	nazývat	k5eAaImAgMnP	nazývat
"	"	kIx"	"
<g/>
Ḥ	Ḥ	k?	Ḥ
Dšr	Dšr	k1gFnSc1	Dšr
<g/>
"	"	kIx"	"
-	-	kIx~	-
"	"	kIx"	"
<g/>
Rudý	rudý	k1gMnSc1	rudý
Hor	hora	k1gFnPc2	hora
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Hebrejci	Hebrejec	k1gMnPc1	Hebrejec
ji	on	k3xPp3gFnSc4	on
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
říkali	říkat	k5eAaImAgMnP	říkat
Ma	Ma	k1gMnPc1	Ma
<g/>
'	'	kIx"	'
<g/>
adim	adim	k1gMnSc1	adim
(	(	kIx(	(
<g/>
מ	מ	k?	מ
<g/>
)	)	kIx)	)
-	-	kIx~	-
"	"	kIx"	"
<g/>
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zardívá	zardívat	k5eAaImIp3nS	zardívat
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
také	také	k9	také
původ	původ	k1gInSc4	původ
jména	jméno	k1gNnSc2	jméno
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
kaňonů	kaňon	k1gInPc2	kaňon
Marsu	Mars	k1gInSc2	Mars
-	-	kIx~	-
Ma	Ma	k1gMnSc1	Ma
<g/>
'	'	kIx"	'
<g/>
adim	adim	k1gMnSc1	adim
Vallis	Vallis	k1gFnSc2	Vallis
<g/>
.	.	kIx.	.
</s>
<s>
Mars	Mars	k1gInSc1	Mars
je	být	k5eAaImIp3nS	být
al-Mirrikh	al-Mirrikh	k1gInSc4	al-Mirrikh
(	(	kIx(	(
<g/>
ا	ا	k?	ا
<g/>
)	)	kIx)	)
jak	jak	k8xS	jak
v	v	k7c6	v
arabštině	arabština	k1gFnSc6	arabština
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
perštině	perština	k1gFnSc6	perština
(	(	kIx(	(
<g/>
ب	ب	k?	ب
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
turečtině	turečtina	k1gFnSc6	turečtina
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
říká	říkat	k5eAaImIp3nS	říkat
Merih	Merih	k1gInSc1	Merih
<g/>
.	.	kIx.	.
</s>
<s>
Etymologie	etymologie	k1gFnSc1	etymologie
al-Mirrikh	al-Mirrikha	k1gFnPc2	al-Mirrikha
je	být	k5eAaImIp3nS	být
zatím	zatím	k6eAd1	zatím
neobjasněná	objasněný	k2eNgFnSc1d1	neobjasněná
<g/>
.	.	kIx.	.
</s>
<s>
Staří	starý	k2eAgMnPc1d1	starý
Peršané	Peršan	k1gMnPc1	Peršan
říkali	říkat	k5eAaImAgMnP	říkat
Marsu	Mars	k1gInSc2	Mars
Bahram	Bahram	k1gInSc1	Bahram
(	(	kIx(	(
<g/>
ب	ب	k?	ب
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
zoroastrijského	zoroastrijský	k2eAgMnSc2d1	zoroastrijský
boha	bůh	k1gMnSc2	bůh
osudu	osud	k1gInSc2	osud
<g/>
.	.	kIx.	.
</s>
<s>
Staří	starý	k2eAgMnPc1d1	starý
Turkové	Turek	k1gMnPc1	Turek
jej	on	k3xPp3gMnSc4	on
nazývali	nazývat	k5eAaImAgMnP	nazývat
Sakit	Sakit	k1gInSc4	Sakit
<g/>
.	.	kIx.	.
</s>
<s>
Číňané	Číňan	k1gMnPc1	Číňan
<g/>
,	,	kIx,	,
Japonci	Japonec	k1gMnPc1	Japonec
<g/>
,	,	kIx,	,
Korejci	Korejec	k1gMnPc1	Korejec
a	a	k8xC	a
Vietnamci	Vietnamec	k1gMnPc1	Vietnamec
planetu	planeta	k1gFnSc4	planeta
označovali	označovat	k5eAaImAgMnP	označovat
za	za	k7c4	za
Ohnivou	ohnivý	k2eAgFnSc4d1	ohnivá
hvězdu	hvězda	k1gFnSc4	hvězda
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
přepisu	přepis	k1gInSc6	přepis
Chuo-sing	Chuoing	k1gInSc4	Chuo-sing
<g/>
,	,	kIx,	,
pchin-jinem	pchinino	k1gNnSc7	pchin-jino
Huǒ	Huǒ	k1gFnSc2	Huǒ
<g/>
,	,	kIx,	,
znaky	znak	k1gInPc4	znak
火	火	k?	火
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jménem	jméno	k1gNnSc7	jméno
založeným	založený	k2eAgInPc3d1	založený
na	na	k7c6	na
starém	starý	k2eAgInSc6d1	starý
čínském	čínský	k2eAgInSc6d1	čínský
systému	systém	k1gInSc6	systém
pěti	pět	k4xCc2	pět
elementů	element	k1gInPc2	element
<g/>
.	.	kIx.	.
</s>
<s>
Symbolem	symbol	k1gInSc7	symbol
Marsu	Mars	k1gInSc2	Mars
je	být	k5eAaImIp3nS	být
malé	malý	k2eAgNnSc4d1	malé
kolečko	kolečko	k1gNnSc4	kolečko
s	s	k7c7	s
šipkou	šipka	k1gFnSc7	šipka
směrující	směrující	k2eAgInSc1d1	směrující
nahoru	nahoru	k6eAd1	nahoru
a	a	k8xC	a
ven	ven	k6eAd1	ven
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
stylizované	stylizovaný	k2eAgNnSc4d1	stylizované
znázornění	znázornění	k1gNnSc4	znázornění
štítu	štít	k1gInSc2	štít
a	a	k8xC	a
kopí	kopí	k1gNnSc2	kopí
<g/>
,	,	kIx,	,
používaných	používaný	k2eAgInPc2d1	používaný
římským	římský	k2eAgMnSc7d1	římský
bohem	bůh	k1gMnSc7	bůh
Marsem	Mars	k1gInSc7	Mars
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
nejen	nejen	k6eAd1	nejen
bohem	bůh	k1gMnSc7	bůh
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
patronem	patron	k1gMnSc7	patron
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Symbol	symbol	k1gInSc1	symbol
se	se	k3xPyFc4	se
také	také	k9	také
užívá	užívat	k5eAaImIp3nS	užívat
v	v	k7c6	v
biologii	biologie	k1gFnSc6	biologie
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
mužského	mužský	k2eAgNnSc2d1	mužské
pohlaví	pohlaví	k1gNnSc2	pohlaví
a	a	k8xC	a
v	v	k7c6	v
alchymii	alchymie	k1gFnSc6	alchymie
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
prvku	prvek	k1gInSc2	prvek
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
soudilo	soudit	k5eAaImAgNnS	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
ovládán	ovládat	k5eAaImNgMnS	ovládat
Marsem	Mars	k1gInSc7	Mars
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
charakteristicky	charakteristicky	k6eAd1	charakteristicky
červené	červený	k2eAgFnSc3d1	červená
barvě	barva	k1gFnSc3	barva
oxidu	oxid	k1gInSc2	oxid
železitého	železitý	k2eAgInSc2d1	železitý
<g/>
.	.	kIx.	.
♂	♂	k?	♂
označuje	označovat	k5eAaImIp3nS	označovat
znak	znak	k1gInSc1	znak
Unicode	Unicod	k1gInSc5	Unicod
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
2642	[number]	k4	2642
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Chaldejců	Chaldejec	k1gMnPc2	Chaldejec
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
významnému	významný	k2eAgInSc3d1	významný
rozvoji	rozvoj	k1gInSc3	rozvoj
astrologie	astrologie	k1gFnSc2	astrologie
a	a	k8xC	a
k	k	k7c3	k
zavedení	zavedení	k1gNnSc3	zavedení
systému	systém	k1gInSc2	systém
sedmi	sedm	k4xCc2	sedm
planet	planeta	k1gFnPc2	planeta
(	(	kIx(	(
<g/>
k	k	k7c3	k
vládnoucímu	vládnoucí	k2eAgInSc3d1	vládnoucí
páru	pár	k1gInSc3	pár
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
Měsíc	měsíc	k1gInSc1	měsíc
přidali	přidat	k5eAaPmAgMnP	přidat
ještě	ještě	k6eAd1	ještě
Merkur	Merkur	k1gInSc4	Merkur
<g/>
,	,	kIx,	,
Venuši	Venuše	k1gFnSc4	Venuše
<g/>
,	,	kIx,	,
Mars	Mars	k1gMnSc1	Mars
<g/>
,	,	kIx,	,
Jupiter	Jupiter	k1gMnSc1	Jupiter
a	a	k8xC	a
Saturn	Saturn	k1gMnSc1	Saturn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
byly	být	k5eAaImAgFnP	být
také	také	k9	také
přiřazeny	přiřazen	k2eAgInPc4d1	přiřazen
příslušné	příslušný	k2eAgInPc4d1	příslušný
božské	božský	k2eAgInPc4d1	božský
principy	princip	k1gInPc4	princip
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
Marsu	Mars	k1gInSc2	Mars
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgMnS	být
babylónský	babylónský	k2eAgMnSc1d1	babylónský
bůh	bůh	k1gMnSc1	bůh
moru	mor	k1gInSc2	mor
Nergal	Nergal	k1gMnSc1	Nergal
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
byly	být	k5eAaImAgFnP	být
později	pozdě	k6eAd2	pozdě
asociováni	asociovat	k5eAaBmNgMnP	asociovat
egyptský	egyptský	k2eAgInSc1d1	egyptský
Hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
hindský	hindský	k2eAgMnSc1d1	hindský
Mangal	Mangal	k1gMnSc1	Mangal
<g/>
,	,	kIx,	,
řecký	řecký	k2eAgMnSc1d1	řecký
Áres	Áres	k1gMnSc1	Áres
a	a	k8xC	a
římský	římský	k2eAgMnSc1d1	římský
bůh	bůh	k1gMnSc1	bůh
války	válka	k1gFnSc2	válka
Mars	Mars	k1gInSc1	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Tradičním	tradiční	k2eAgInSc7d1	tradiční
astrologickým	astrologický	k2eAgFnPc3d1	astrologická
7	[number]	k4	7
planetám	planeta	k1gFnPc3	planeta
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
7	[number]	k4	7
dnů	den	k1gInPc2	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
Mars	Mars	k1gInSc1	Mars
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
úterým	úterý	k1gNnSc7	úterý
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
také	také	k9	také
vychází	vycházet	k5eAaImIp3nS	vycházet
pojmenování	pojmenování	k1gNnSc4	pojmenování
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
v	v	k7c6	v
románských	románský	k2eAgInPc6d1	románský
jazycích	jazyk	k1gInPc6	jazyk
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ve	v	k7c6	v
španělštině	španělština	k1gFnSc6	španělština
martes	martesa	k1gFnPc2	martesa
<g/>
,	,	kIx,	,
v	v	k7c6	v
italštině	italština	k1gFnSc6	italština
martedì	martedì	k?	martedì
a	a	k8xC	a
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
mardi	mard	k1gMnPc1	mard
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Pythagora	Pythagoras	k1gMnSc2	Pythagoras
7	[number]	k4	7
planetárních	planetární	k2eAgFnPc2d1	planetární
sfér	sféra	k1gFnPc2	sféra
kolem	kolem	k7c2	kolem
Země	zem	k1gFnSc2	zem
svým	svůj	k3xOyFgNnPc3	svůj
otáčením	otáčení	k1gNnPc3	otáčení
vyluzuje	vyluzovat	k5eAaImIp3nS	vyluzovat
tzv.	tzv.	kA	tzv.
hudbu	hudba	k1gFnSc4	hudba
sfér	sféra	k1gFnPc2	sféra
-	-	kIx~	-
starší	starý	k2eAgFnPc1d2	starší
hudební	hudební	k2eAgFnPc1d1	hudební
stupnice	stupnice	k1gFnPc1	stupnice
proto	proto	k8xC	proto
byly	být	k5eAaImAgFnP	být
sedmitónové	sedmitónový	k2eAgFnPc1d1	sedmitónový
<g/>
.	.	kIx.	.
</s>
<s>
Marsu	Mars	k1gInSc3	Mars
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
číslo	číslo	k1gNnSc1	číslo
5	[number]	k4	5
a	a	k8xC	a
tón	tón	k1gInSc1	tón
G.	G.	kA	G.
Mars	Mars	k1gInSc1	Mars
ve	v	k7c6	v
zvěrokruhu	zvěrokruh	k1gInSc6	zvěrokruh
vládne	vládnout	k5eAaImIp3nS	vládnout
I.	I.	kA	I.
a	a	k8xC	a
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
nebeskému	nebeský	k2eAgInSc3d1	nebeský
domu	dům	k1gInSc3	dům
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
denní	denní	k2eAgInSc1d1	denní
dům	dům	k1gInSc1	dům
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gInSc4	on
Beran	beran	k1gInSc4	beran
a	a	k8xC	a
noční	noční	k2eAgMnSc1d1	noční
Štír	štír	k1gMnSc1	štír
<g/>
,	,	kIx,	,
povýšení	povýšení	k1gNnSc1	povýšení
pak	pak	k6eAd1	pak
zažívá	zažívat	k5eAaImIp3nS	zažívat
v	v	k7c6	v
Kozorohu	Kozoroh	k1gMnSc6	Kozoroh
<g/>
,	,	kIx,	,
pád	pád	k1gInSc4	pád
v	v	k7c6	v
Raku	rak	k1gMnSc6	rak
a	a	k8xC	a
zničení	zničení	k1gNnSc1	zničení
ve	v	k7c6	v
Váhách	Váhy	k1gFnPc6	Váhy
a	a	k8xC	a
v	v	k7c6	v
Býku	býk	k1gMnSc6	býk
<g/>
.	.	kIx.	.
</s>
<s>
Problémy	problém	k1gInPc1	problém
tomuto	tento	k3xDgInSc3	tento
systému	systém	k1gInSc6	systém
přinesl	přinést	k5eAaPmAgInS	přinést
objev	objev	k1gInSc1	objev
trpasličí	trpasličí	k2eAgFnSc2d1	trpasličí
planety	planeta	k1gFnSc2	planeta
Pluto	Pluto	k1gNnSc1	Pluto
<g/>
,	,	kIx,	,
v	v	k7c4	v
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
prospěch	prospěch	k1gInSc4	prospěch
někteří	některý	k3yIgMnPc1	některý
moderní	moderní	k2eAgMnSc1d1	moderní
astrologové	astrolog	k1gMnPc1	astrolog
odebírají	odebírat	k5eAaImIp3nP	odebírat
Marsu	Mars	k1gInSc3	Mars
znamení	znamení	k1gNnSc4	znamení
Štíra	štír	k1gMnSc2	štír
<g/>
.	.	kIx.	.
</s>
<s>
Konzervativní	konzervativní	k2eAgMnPc1d1	konzervativní
astrologové	astrolog	k1gMnPc1	astrolog
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
raději	rád	k6eAd2	rád
ponechávají	ponechávat	k5eAaImIp3nP	ponechávat
Pluto	Pluto	k1gNnSc4	Pluto
bez	bez	k7c2	bez
domicilu	domicil	k1gInSc2	domicil
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
představoval	představovat	k5eAaImAgInS	představovat
božský	božský	k2eAgInSc1d1	božský
princip	princip	k1gInSc1	princip
Marsu	Mars	k1gInSc2	Mars
(	(	kIx(	(
<g/>
muže	muž	k1gMnSc4	muž
<g/>
)	)	kIx)	)
harmonický	harmonický	k2eAgInSc4d1	harmonický
protiklad	protiklad	k1gInSc4	protiklad
k	k	k7c3	k
Venuši	Venuše	k1gFnSc3	Venuše
(	(	kIx(	(
<g/>
ženě	žena	k1gFnSc3	žena
<g/>
)	)	kIx)	)
a	a	k8xC	a
tomu	ten	k3xDgMnSc3	ten
odpovídal	odpovídat	k5eAaImAgInS	odpovídat
i	i	k9	i
jeho	jeho	k3xOp3gInSc1	jeho
tehdejší	tehdejší	k2eAgInSc1d1	tehdejší
symbol	symbol	k1gInSc1	symbol
♁	♁	k?	♁
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
symbol	symbol	k1gInSc4	symbol
Země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pozdějším	pozdní	k2eAgNnSc7d2	pozdější
zdůrazněním	zdůraznění	k1gNnSc7	zdůraznění
agresívních	agresívní	k2eAgMnPc2d1	agresívní
prvků	prvek	k1gInPc2	prvek
však	však	k9	však
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
deformaci	deformace	k1gFnSc3	deformace
kříže	kříž	k1gInSc2	kříž
do	do	k7c2	do
úhlopříčného	úhlopříčný	k2eAgInSc2d1	úhlopříčný
šípu	šíp	k1gInSc2	šíp
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
k	k	k7c3	k
přechodu	přechod	k1gInSc3	přechod
do	do	k7c2	do
dnešního	dnešní	k2eAgInSc2d1	dnešní
symbolu	symbol	k1gInSc2	symbol
♂	♂	k?	♂
<g/>
.	.	kIx.	.
</s>
<s>
Astrologická	astrologický	k2eAgFnSc1d1	astrologická
povaha	povaha	k1gFnSc1	povaha
Marsu	Mars	k1gInSc2	Mars
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
mytologie	mytologie	k1gFnSc2	mytologie
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
spojována	spojovat	k5eAaImNgFnS	spojovat
se	s	k7c7	s
sebejistotou	sebejistota	k1gFnSc7	sebejistota
a	a	k8xC	a
sebe-prosazováním	seberosazování	k1gNnSc7	sebe-prosazování
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
agresí	agrese	k1gFnSc7	agrese
<g/>
,	,	kIx,	,
sexualitou	sexualita	k1gFnSc7	sexualita
<g/>
,	,	kIx,	,
energií	energie	k1gFnSc7	energie
<g/>
,	,	kIx,	,
silou	síla	k1gFnSc7	síla
<g/>
,	,	kIx,	,
ambicemi	ambice	k1gFnPc7	ambice
a	a	k8xC	a
výbušností	výbušnost	k1gFnSc7	výbušnost
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
historicky	historicky	k6eAd1	historicky
chápanými	chápaný	k2eAgFnPc7d1	chápaná
samčími	samčí	k2eAgFnPc7d1	samčí
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
vlastnosti	vlastnost	k1gFnPc1	vlastnost
zároveň	zároveň	k6eAd1	zároveň
svědčí	svědčit	k5eAaImIp3nP	svědčit
o	o	k7c6	o
duchu	duch	k1gMnSc6	duch
ovládaném	ovládaný	k2eAgMnSc6d1	ovládaný
nižšími	nízký	k2eAgFnPc7d2	nižší
potencemi	potence	k1gFnPc7	potence
(	(	kIx(	(
<g/>
hmotou	hmota	k1gFnSc7	hmota
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
odráželo	odrážet	k5eAaImAgNnS	odrážet
i	i	k9	i
v	v	k7c6	v
již	již	k6eAd1	již
zmíněném	zmíněný	k2eAgInSc6d1	zmíněný
původním	původní	k2eAgInSc6d1	původní
symbolu	symbol	k1gInSc6	symbol
♁	♁	k?	♁
(	(	kIx(	(
<g/>
kříži	kříž	k1gInSc6	kříž
hmoty	hmota	k1gFnSc2	hmota
nad	nad	k7c7	nad
kruhem	kruh	k1gInSc7	kruh
ducha	duch	k1gMnSc2	duch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
astrologů	astrolog	k1gMnPc2	astrolog
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
vliv	vliv	k1gInSc1	vliv
Marsu	Mars	k1gInSc2	Mars
uplatňovat	uplatňovat	k5eAaImF	uplatňovat
v	v	k7c6	v
povoláních	povolání	k1gNnPc6	povolání
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
vojáci	voják	k1gMnPc1	voják
<g/>
,	,	kIx,	,
chirurgové	chirurg	k1gMnPc1	chirurg
či	či	k8xC	či
sportovci	sportovec	k1gMnPc1	sportovec
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgMnSc1d1	francouzský
psycholog	psycholog	k1gMnSc1	psycholog
a	a	k8xC	a
statistik	statistik	k1gMnSc1	statistik
Michel	Michel	k1gMnSc1	Michel
Gauquelin	Gauquelina	k1gFnPc2	Gauquelina
provedl	provést	k5eAaPmAgMnS	provést
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
velkou	velký	k2eAgFnSc4d1	velká
studii	studie	k1gFnSc4	studie
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
"	"	kIx"	"
<g/>
Mars	Mars	k1gMnSc1	Mars
Effect	Effect	k1gMnSc1	Effect
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dávala	dávat	k5eAaImAgFnS	dávat
významnou	významný	k2eAgFnSc4d1	významná
korelaci	korelace	k1gFnSc4	korelace
mezi	mezi	k7c7	mezi
datem	datum	k1gNnSc7	datum
narození	narození	k1gNnSc2	narození
sportovních	sportovní	k2eAgMnPc2d1	sportovní
šampiónů	šampión	k1gMnPc2	šampión
a	a	k8xC	a
dominantním	dominantní	k2eAgNnSc7d1	dominantní
postavením	postavení	k1gNnSc7	postavení
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Test	test	k1gInSc1	test
na	na	k7c6	na
jinak	jinak	k6eAd1	jinak
sestaveném	sestavený	k2eAgInSc6d1	sestavený
vzorku	vzorek	k1gInSc6	vzorek
šampiónů	šampión	k1gMnPc2	šampión
však	však	k9	však
přinesl	přinést	k5eAaPmAgInS	přinést
negativní	negativní	k2eAgInSc1d1	negativní
výsledek	výsledek	k1gInSc1	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
představa	představa	k1gFnSc1	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Mars	Mars	k1gInSc1	Mars
obydlen	obydlen	k2eAgInSc1d1	obydlen
inteligentními	inteligentní	k2eAgMnPc7d1	inteligentní
Marťany	Marťan	k1gMnPc7	Marťan
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
traduje	tradovat	k5eAaImIp3nS	tradovat
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
na	na	k7c6	na
plno	plno	k6eAd1	plno
rozeběhlo	rozeběhnout	k5eAaPmAgNnS	rozeběhnout
mapování	mapování	k1gNnSc1	mapování
marsovských	marsovský	k2eAgInPc2d1	marsovský
kanálů	kanál	k1gInPc2	kanál
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
propagoval	propagovat	k5eAaImAgMnS	propagovat
především	především	k9	především
italský	italský	k2eAgMnSc1d1	italský
astronom	astronom	k1gMnSc1	astronom
Giovanni	Giovanň	k1gMnSc3	Giovanň
Schiaparelli	Schiaparell	k1gMnSc3	Schiaparell
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
knihou	kniha	k1gFnSc7	kniha
od	od	k7c2	od
Percivala	Percivala	k1gFnSc2	Percivala
Lowella	Lowello	k1gNnSc2	Lowello
o	o	k7c6	o
postupně	postupně	k6eAd1	postupně
umírající	umírající	k2eAgFnSc6d1	umírající
planetě	planeta	k1gFnSc6	planeta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vysychá	vysychat	k5eAaImIp3nS	vysychat
a	a	k8xC	a
chladne	chladnout	k5eAaImIp3nS	chladnout
s	s	k7c7	s
prastarou	prastarý	k2eAgFnSc7d1	prastará
civilizací	civilizace	k1gFnSc7	civilizace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
vytvořit	vytvořit	k5eAaPmF	vytvořit
síť	síť	k1gFnSc4	síť
zavlažovacích	zavlažovací	k2eAgInPc2d1	zavlažovací
kanálů	kanál	k1gInPc2	kanál
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
začala	začít	k5eAaPmAgFnS	začít
šířit	šířit	k5eAaImF	šířit
myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
existuje	existovat	k5eAaImIp3nS	existovat
inteligentní	inteligentní	k2eAgInSc1d1	inteligentní
život	život	k1gInSc1	život
<g/>
.	.	kIx.	.
</s>
<s>
Pozorování	pozorování	k1gNnSc1	pozorování
neexistujících	existující	k2eNgInPc2d1	neexistující
kanálů	kanál	k1gInPc2	kanál
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
se	se	k3xPyFc4	se
šířilo	šířit	k5eAaImAgNnS	šířit
mezi	mezi	k7c7	mezi
tehdejšími	tehdejší	k2eAgMnPc7d1	tehdejší
astronomy	astronom	k1gMnPc7	astronom
jako	jako	k8xS	jako
Marsovská	marsovský	k2eAgFnSc1d1	marsovská
horečka	horečka	k1gFnSc1	horečka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přinášela	přinášet	k5eAaImAgFnS	přinášet
podrobnější	podrobný	k2eAgInPc4d2	podrobnější
a	a	k8xC	a
přesnější	přesný	k2eAgFnPc4d2	přesnější
mapy	mapa	k1gFnPc4	mapa
zavlažovacích	zavlažovací	k2eAgInPc2d1	zavlažovací
kanálů	kanál	k1gInPc2	kanál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
během	během	k7c2	během
průzkumu	průzkum	k1gInSc2	průzkum
atmosférického	atmosférický	k2eAgInSc2d1	atmosférický
rádiového	rádiový	k2eAgInSc2d1	rádiový
šumu	šum	k1gInSc2	šum
zachytil	zachytit	k5eAaPmAgMnS	zachytit
vynálezce	vynálezce	k1gMnSc1	vynálezce
Nikola	Nikola	k1gMnSc1	Nikola
Tesla	Tesla	k1gMnSc1	Tesla
opakující	opakující	k2eAgMnSc1d1	opakující
se	se	k3xPyFc4	se
signál	signál	k1gInSc1	signál
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yRgInSc6	který
později	pozdě	k6eAd2	pozdě
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
radiovou	radiový	k2eAgFnSc4d1	radiová
komunikaci	komunikace	k1gFnSc4	komunikace
z	z	k7c2	z
jiné	jiný	k2eAgFnSc2d1	jiná
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Teslově	Teslův	k2eAgFnSc3d1	Teslova
teorii	teorie	k1gFnSc3	teorie
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
dostalo	dostat	k5eAaPmAgNnS	dostat
podpory	podpora	k1gFnSc2	podpora
Lorda	lord	k1gMnSc2	lord
Kelvina	Kelvin	k2eAgMnSc2d1	Kelvin
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
americké	americký	k2eAgInPc4d1	americký
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
a	a	k8xC	a
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
měl	mít	k5eAaImAgMnS	mít
prohlásit	prohlásit	k5eAaPmF	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Tesla	Tesla	k1gMnSc1	Tesla
zachytil	zachytit	k5eAaPmAgMnS	zachytit
Marťanské	marťanský	k2eAgNnSc4d1	marťanské
radiové	radiový	k2eAgNnSc4d1	radiové
vysílání	vysílání	k1gNnSc4	vysílání
určené	určený	k2eAgNnSc4d1	určené
pro	pro	k7c4	pro
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
vyšel	vyjít	k5eAaPmAgInS	vyjít
článek	článek	k1gInSc1	článek
v	v	k7c6	v
New	New	k1gFnSc6	New
York	York	k1gInSc1	York
Times	Timesa	k1gFnPc2	Timesa
<g/>
,	,	kIx,	,
že	že	k8xS	že
ředitel	ředitel	k1gMnSc1	ředitel
Harvard	Harvard	k1gInSc4	Harvard
College	Colleg	k1gFnSc2	Colleg
Observatory	Observator	k1gMnPc4	Observator
Edward	Edward	k1gMnSc1	Edward
Charles	Charles	k1gMnSc1	Charles
Pickering	Pickering	k1gInSc4	Pickering
obdržel	obdržet	k5eAaPmAgMnS	obdržet
telegram	telegram	k1gInSc4	telegram
z	z	k7c2	z
Lowell	Lowella	k1gFnPc2	Lowella
Observatory	Observator	k1gInPc1	Observator
v	v	k7c6	v
Arizoně	Arizona	k1gFnSc6	Arizona
ohledně	ohledně	k7c2	ohledně
možného	možný	k2eAgInSc2d1	možný
pokusu	pokus	k1gInSc2	pokus
zachycené	zachycený	k2eAgFnSc2d1	zachycená
komunikace	komunikace	k1gFnSc2	komunikace
Marsu	Mars	k1gInSc2	Mars
se	s	k7c7	s
Zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
ale	ale	k8xC	ale
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
kosmické	kosmický	k2eAgFnPc4d1	kosmická
sondy	sonda	k1gFnPc4	sonda
ve	v	k7c4	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
inteligentní	inteligentní	k2eAgInSc1d1	inteligentní
život	život	k1gInSc1	život
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Mars	Mars	k1gInSc1	Mars
byl	být	k5eAaImAgInS	být
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
častým	častý	k2eAgInSc7d1	častý
předmětem	předmět	k1gInSc7	předmět
sci-fi	scii	k1gFnPc2	sci-fi
příběhů	příběh	k1gInPc2	příběh
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
ho	on	k3xPp3gNnSc4	on
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
popisovaly	popisovat	k5eAaImAgFnP	popisovat
jako	jako	k8xS	jako
živý	živý	k2eAgInSc1d1	živý
svět	svět	k1gInSc1	svět
inteligentních	inteligentní	k2eAgMnPc2d1	inteligentní
tvorů	tvor	k1gMnPc2	tvor
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jako	jako	k9	jako
vyprahlou	vyprahlý	k2eAgFnSc4d1	vyprahlá
planetu	planeta	k1gFnSc4	planeta
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
snaží	snažit	k5eAaImIp3nS	snažit
podmanit	podmanit	k5eAaPmF	podmanit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
magická	magický	k2eAgFnSc1d1	magická
rudá	rudý	k2eAgFnSc1d1	rudá
barva	barva	k1gFnSc1	barva
a	a	k8xC	a
chybná	chybný	k2eAgFnSc1d1	chybná
představa	představa	k1gFnSc1	představa
rozsáhlých	rozsáhlý	k2eAgInPc2d1	rozsáhlý
kanálů	kanál	k1gInPc2	kanál
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
povrchu	povrch	k1gInSc6	povrch
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
mnohé	mnohé	k1gNnSc4	mnohé
spisovatele	spisovatel	k1gMnSc2	spisovatel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
své	svůj	k3xOyFgInPc4	svůj
příběhy	příběh	k1gInPc4	příběh
zasadili	zasadit	k5eAaPmAgMnP	zasadit
do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
nejznámější	známý	k2eAgFnSc7d3	nejznámější
knihou	kniha	k1gFnSc7	kniha
z	z	k7c2	z
rané	raný	k2eAgFnSc2d1	raná
historie	historie	k1gFnSc2	historie
sci-fi	scii	k1gFnSc2	sci-fi
žánru	žánr	k1gInSc2	žánr
je	být	k5eAaImIp3nS	být
H.	H.	kA	H.
G.	G.	kA	G.
Wellsova	Wellsův	k2eAgFnSc1d1	Wellsova
kniha	kniha	k1gFnSc1	kniha
Válka	Válek	k1gMnSc2	Válek
světů	svět	k1gInPc2	svět
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
invazi	invaze	k1gFnSc4	invaze
Marťanů	Marťan	k1gMnPc2	Marťan
z	z	k7c2	z
umírající	umírající	k2eAgFnSc2d1	umírající
planety	planeta	k1gFnSc2	planeta
na	na	k7c4	na
Zem	zem	k1gFnSc4	zem
a	a	k8xC	a
následnou	následný	k2eAgFnSc4d1	následná
válku	válka	k1gFnSc4	válka
s	s	k7c7	s
lidstvem	lidstvo	k1gNnSc7	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
hitem	hit	k1gInSc7	hit
a	a	k8xC	a
dočkala	dočkat	k5eAaPmAgFnS	dočkat
se	se	k3xPyFc4	se
i	i	k9	i
rádiového	rádiový	k2eAgNnSc2d1	rádiové
vysílání	vysílání	k1gNnSc2	vysílání
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
<s>
Vysílání	vysílání	k1gNnSc1	vysílání
bylo	být	k5eAaImAgNnS	být
natolik	natolik	k6eAd1	natolik
přesvědčivé	přesvědčivý	k2eAgNnSc1d1	přesvědčivé
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnozí	mnohý	k2eAgMnPc1d1	mnohý
posluchači	posluchač	k1gMnPc1	posluchač
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
si	se	k3xPyFc3	se
zapnuli	zapnout	k5eAaPmAgMnP	zapnout
rádio	rádio	k1gNnSc4	rádio
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
domnívali	domnívat	k5eAaImAgMnP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
událost	událost	k1gFnSc4	událost
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
v	v	k7c4	v
paniku	panika	k1gFnSc4	panika
a	a	k8xC	a
chaos	chaos	k1gInSc4	chaos
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
slavným	slavný	k2eAgInSc7d1	slavný
dílem	díl	k1gInSc7	díl
je	být	k5eAaImIp3nS	být
Marťanská	marťanský	k2eAgFnSc1d1	Marťanská
kronika	kronika	k1gFnSc1	kronika
od	od	k7c2	od
amerického	americký	k2eAgMnSc2d1	americký
spisovatele	spisovatel	k1gMnSc2	spisovatel
Raye	Ray	k1gMnSc2	Ray
Bradburyho	Bradbury	k1gMnSc2	Bradbury
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
líčí	líčit	k5eAaImIp3nS	líčit
zkázu	zkáza	k1gFnSc4	zkáza
marťanské	marťanský	k2eAgFnSc2d1	Marťanská
civilizace	civilizace	k1gFnSc2	civilizace
nešťastnou	šťastný	k2eNgFnSc7d1	nešťastná
náhodou	náhoda	k1gFnSc7	náhoda
způsobenou	způsobený	k2eAgFnSc4d1	způsobená
lidmi	člověk	k1gMnPc7	člověk
a	a	k8xC	a
neschopnost	neschopnost	k1gFnSc1	neschopnost
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
chyby	chyba	k1gFnSc2	chyba
poučit	poučit	k5eAaPmF	poučit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
o	o	k7c6	o
Marsu	Mars	k1gInSc6	Mars
psali	psát	k5eAaImAgMnP	psát
Edgar	Edgar	k1gMnSc1	Edgar
Rice	Ric	k1gFnSc2	Ric
Burroughs	Burroughs	k1gInSc1	Burroughs
a	a	k8xC	a
Robert	Robert	k1gMnSc1	Robert
A.	A.	kA	A.
Heinlein	Heinlein	k1gMnSc1	Heinlein
<g/>
.	.	kIx.	.
</s>
<s>
Mars	Mars	k1gInSc1	Mars
se	se	k3xPyFc4	se
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
vyskytoval	vyskytovat	k5eAaImAgMnS	vyskytovat
již	již	k9	již
dříve	dříve	k6eAd2	dříve
před	před	k7c7	před
vznikem	vznik	k1gInSc7	vznik
moderní	moderní	k2eAgFnSc2d1	moderní
sci-fi	scii	k1gFnSc2	sci-fi
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
spisovatel	spisovatel	k1gMnSc1	spisovatel
Jonathan	Jonathan	k1gMnSc1	Jonathan
Swift	Swift	k1gMnSc1	Swift
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Gulliverovy	Gulliverův	k2eAgFnSc2d1	Gulliverova
cesty	cesta	k1gFnSc2	cesta
v	v	k7c6	v
devatenácté	devatenáctý	k4xOgFnSc6	devatenáctý
kapitole	kapitola	k1gFnSc6	kapitola
popisuje	popisovat	k5eAaImIp3nS	popisovat
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
Marsu	Mars	k1gInSc2	Mars
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
150	[number]	k4	150
let	léto	k1gNnPc2	léto
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
byly	být	k5eAaImAgInP	být
skutečně	skutečně	k6eAd1	skutečně
objeveny	objevit	k5eAaPmNgInP	objevit
astronomem	astronom	k1gMnSc7	astronom
Asaphem	Asaph	k1gInSc7	Asaph
Hallem	Hall	k1gMnSc7	Hall
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
vysláním	vyslání	k1gNnSc7	vyslání
sond	sonda	k1gFnPc2	sonda
Mariner	Mariner	k1gInSc4	Mariner
a	a	k8xC	a
Viking	Viking	k1gMnSc1	Viking
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
přinesly	přinést	k5eAaPmAgInP	přinést
první	první	k4xOgInPc1	první
podrobné	podrobný	k2eAgInPc1d1	podrobný
snímky	snímek	k1gInPc1	snímek
o	o	k7c6	o
skutečném	skutečný	k2eAgInSc6d1	skutečný
povrchu	povrch	k1gInSc6	povrch
Marsu	Mars	k1gInSc2	Mars
bez	bez	k7c2	bez
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
knih	kniha	k1gFnPc2	kniha
zabývala	zabývat	k5eAaImAgFnS	zabývat
tématem	téma	k1gNnSc7	téma
inteligentních	inteligentní	k2eAgMnPc2d1	inteligentní
Marťanů	Marťan	k1gMnPc2	Marťan
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc4	jejich
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
ale	ale	k9	ale
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
život	život	k1gInSc4	život
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
není	být	k5eNaImIp3nS	být
a	a	k8xC	a
že	že	k8xS	že
možná	možná	k9	možná
ani	ani	k8xC	ani
nikdy	nikdy	k6eAd1	nikdy
nebyl	být	k5eNaImAgMnS	být
<g/>
,	,	kIx,	,
téma	téma	k1gNnSc1	téma
knih	kniha	k1gFnPc2	kniha
se	se	k3xPyFc4	se
změnilo	změnit	k5eAaPmAgNnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Začalo	začít	k5eAaPmAgNnS	začít
se	se	k3xPyFc4	se
zaobírat	zaobírat	k5eAaImF	zaobírat
blízkou	blízký	k2eAgFnSc7d1	blízká
lidskou	lidský	k2eAgFnSc7d1	lidská
budoucností	budoucnost	k1gFnSc7	budoucnost
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
lidstvo	lidstvo	k1gNnSc1	lidstvo
pokusí	pokusit	k5eAaPmIp3nS	pokusit
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
přistát	přistát	k5eAaPmF	přistát
a	a	k8xC	a
vytvořit	vytvořit	k5eAaPmF	vytvořit
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
trvalou	trvalá	k1gFnSc4	trvalá
základnu	základna	k1gFnSc4	základna
a	a	k8xC	a
následně	následně	k6eAd1	následně
ho	on	k3xPp3gMnSc4	on
osídlit	osídlit	k5eAaPmF	osídlit
<g/>
.	.	kIx.	.
</s>
<s>
Ceněnou	ceněný	k2eAgFnSc7d1	ceněná
ságou	sága	k1gFnSc7	sága
o	o	k7c6	o
kolonizaci	kolonizace	k1gFnSc6	kolonizace
a	a	k8xC	a
boji	boj	k1gInSc6	boj
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
Marsu	Mars	k1gInSc2	Mars
je	být	k5eAaImIp3nS	být
Marťanská	marťanský	k2eAgFnSc1d1	Marťanská
trilogie	trilogie	k1gFnSc1	trilogie
od	od	k7c2	od
Kima	Kim	k1gInSc2	Kim
Stanleyho	Stanley	k1gMnSc4	Stanley
Robinsona	Robinson	k1gMnSc4	Robinson
<g/>
.	.	kIx.	.
</s>
<s>
Snímky	snímek	k1gInPc1	snímek
z	z	k7c2	z
Marsu	Mars	k1gInSc2	Mars
ale	ale	k8xC	ale
nevyvrátily	vyvrátit	k5eNaPmAgInP	vyvrátit
veškeré	veškerý	k3xTgFnPc4	veškerý
pochybnosti	pochybnost	k1gFnPc4	pochybnost
a	a	k8xC	a
některé	některý	k3yIgFnPc4	některý
paradoxně	paradoxně	k6eAd1	paradoxně
zájem	zájem	k1gInSc1	zájem
sci-fi	scii	k1gFnPc2	sci-fi
autorů	autor	k1gMnPc2	autor
ještě	ještě	k6eAd1	ještě
podpořily	podpořit	k5eAaPmAgInP	podpořit
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
útvarem	útvar	k1gInSc7	útvar
ze	z	k7c2	z
snímků	snímek	k1gInPc2	snímek
sond	sonda	k1gFnPc2	sonda
Viking	Viking	k1gMnSc1	Viking
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
tzv.	tzv.	kA	tzv.
Tvář	tvář	k1gFnSc1	tvář
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
-	-	kIx~	-
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
připomínající	připomínající	k2eAgFnSc4d1	připomínající
lidskou	lidský	k2eAgFnSc4d1	lidská
tvář	tvář	k1gFnSc4	tvář
<g/>
,	,	kIx,	,
obrácenou	obrácený	k2eAgFnSc4d1	obrácená
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
(	(	kIx(	(
<g/>
pozdější	pozdní	k2eAgNnSc1d2	pozdější
podrobné	podrobný	k2eAgNnSc1d1	podrobné
mapování	mapování	k1gNnSc1	mapování
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
přírodní	přírodní	k2eAgInSc4d1	přírodní
útvar	útvar	k1gInSc4	útvar
vzniklý	vzniklý	k2eAgInSc4d1	vzniklý
zvětráváním	zvětrávání	k1gNnSc7	zvětrávání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
a	a	k8xC	a
podobné	podobný	k2eAgInPc1d1	podobný
výjevy	výjev	k1gInPc1	výjev
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
měly	mít	k5eAaImAgInP	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Mars	Mars	k1gInSc1	Mars
i	i	k9	i
po	po	k7c4	po
zmapování	zmapování	k1gNnSc4	zmapování
povrchu	povrch	k1gInSc2	povrch
pro	pro	k7c4	pro
spisovatele	spisovatel	k1gMnPc4	spisovatel
vědeckofantastické	vědeckofantastický	k2eAgFnSc2d1	vědeckofantastická
literatury	literatura	k1gFnSc2	literatura
stále	stále	k6eAd1	stále
zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
námětem	námět	k1gInSc7	námět
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
boj	boj	k1gInSc1	boj
marťanské	marťanský	k2eAgFnSc2d1	Marťanská
kolonie	kolonie	k1gFnSc2	kolonie
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
dílech	díl	k1gInPc6	díl
Grega	Greg	k1gMnSc4	Greg
Beara	Bear	k1gMnSc4	Bear
a	a	k8xC	a
nebo	nebo	k8xC	nebo
již	již	k6eAd1	již
zmiňovaného	zmiňovaný	k2eAgMnSc4d1	zmiňovaný
Kima	Kimus	k1gMnSc4	Kimus
Stanleyho	Stanley	k1gMnSc4	Stanley
Robinsona	Robinson	k1gMnSc4	Robinson
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stejném	stejný	k2eAgInSc6d1	stejný
základě	základ	k1gInSc6	základ
staví	stavit	k5eAaImIp3nS	stavit
film	film	k1gInSc4	film
Total	totat	k5eAaImAgInS	totat
Recall	Recall	k1gInSc1	Recall
a	a	k8xC	a
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
Babylon	Babylon	k1gInSc1	Babylon
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nezávislé	závislý	k2eNgFnSc6d1	nezávislá
kolonii	kolonie	k1gFnSc6	kolonie
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
i	i	k9	i
děj	děj	k1gInSc1	děj
tetralogie	tetralogie	k1gFnSc2	tetralogie
Pán	pán	k1gMnSc1	pán
modrého	modrý	k2eAgInSc2d1	modrý
meče	meč	k1gInSc2	meč
českého	český	k2eAgMnSc2d1	český
spisovatele	spisovatel	k1gMnSc2	spisovatel
Ondřeje	Ondřej	k1gMnSc2	Ondřej
Neffa	Neff	k1gMnSc2	Neff
<g/>
.	.	kIx.	.
</s>
