<p>
<s>
Cimljanská	Cimljanský	k2eAgFnSc1d1	Cimljanský
přehradní	přehradní	k2eAgFnSc1d1	přehradní
nádrž	nádrž	k1gFnSc1	nádrž
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
Ц	Ц	k?	Ц
в	в	k?	в
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
přehradní	přehradní	k2eAgFnSc1d1	přehradní
nádrž	nádrž	k1gFnSc1	nádrž
na	na	k7c6	na
území	území	k1gNnSc6	území
Rostovské	Rostovský	k2eAgFnSc2d1	Rostovská
oblasti	oblast	k1gFnSc2	oblast
a	a	k8xC	a
Volgogradské	volgogradský	k2eAgFnSc2d1	Volgogradská
oblasti	oblast	k1gFnSc2	oblast
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
2700	[number]	k4	2700
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
260	[number]	k4	260
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
a	a	k8xC	a
maximálně	maximálně	k6eAd1	maximálně
38	[number]	k4	38
km	km	kA	km
široká	široký	k2eAgFnSc1d1	široká
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
hloubka	hloubka	k1gFnSc1	hloubka
je	být	k5eAaImIp3nS	být
8,8	[number]	k4	8,8
m.	m.	k?	m.
Má	mít	k5eAaImIp3nS	mít
objem	objem	k1gInSc4	objem
23,9	[number]	k4	23,9
km3	km3	k4	km3
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místech	místo	k1gNnPc6	místo
dolních	dolní	k2eAgFnPc2d1	dolní
toků	tok	k1gInPc2	tok
hlavních	hlavní	k2eAgInPc2d1	hlavní
přítoků	přítok	k1gInPc2	přítok
Donu	Don	k1gInSc2	Don
(	(	kIx(	(
<g/>
Cimla	Cimla	k1gMnSc1	Cimla
<g/>
,	,	kIx,	,
Čir	Čir	k1gMnSc1	Čir
<g/>
,	,	kIx,	,
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vytvořily	vytvořit	k5eAaPmAgInP	vytvořit
zálivy	záliv	k1gInPc1	záliv
široké	široký	k2eAgInPc1d1	široký
až	až	k9	až
5	[number]	k4	5
km	km	kA	km
a	a	k8xC	a
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
15	[number]	k4	15
až	až	k9	až
30	[number]	k4	30
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vodní	vodní	k2eAgInSc1d1	vodní
režim	režim	k1gInSc1	režim
==	==	k?	==
</s>
</p>
<p>
<s>
Nádrž	nádrž	k1gFnSc1	nádrž
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Donu	Don	k1gInSc2	Don
za	za	k7c7	za
přehradní	přehradní	k2eAgFnSc7d1	přehradní
hrází	hráz	k1gFnSc7	hráz
Cimljanské	Cimljanský	k2eAgFnSc2d1	Cimljanský
vodní	vodní	k2eAgFnSc2d1	vodní
elektrárny	elektrárna	k1gFnSc2	elektrárna
byla	být	k5eAaImAgFnS	být
naplněna	naplnit	k5eAaPmNgFnS	naplnit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1952	[number]	k4	1952
<g/>
-	-	kIx~	-
<g/>
55	[number]	k4	55
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
Volžsko-donské	volžskoonský	k2eAgFnSc2d1	volžsko-donský
vodní	vodní	k2eAgFnSc2d1	vodní
cesty	cesta	k1gFnSc2	cesta
a	a	k8xC	a
reguluje	regulovat	k5eAaImIp3nS	regulovat
dlouhodobé	dlouhodobý	k2eAgNnSc1d1	dlouhodobé
kolísání	kolísání	k1gNnSc1	kolísání
průtoku	průtok	k1gInSc2	průtok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přítoky	přítok	k1gInPc4	přítok
===	===	k?	===
</s>
</p>
<p>
<s>
Don	Don	k1gMnSc1	Don
</s>
</p>
<p>
<s>
Cimla	Cimla	k6eAd1	Cimla
</s>
</p>
<p>
<s>
Čir	Čir	k?	Čir
</s>
</p>
<p>
<s>
Jesaulovský	Jesaulovský	k2eAgInSc1d1	Jesaulovský
Aksaj	Aksaj	k1gInSc1	Aksaj
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Využívá	využívat	k5eAaPmIp3nS	využívat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
zavlažování	zavlažování	k1gNnSc4	zavlažování
(	(	kIx(	(
<g/>
6	[number]	k4	6
000	[number]	k4	000
km2	km2	k4	km2
<g/>
)	)	kIx)	)
a	a	k8xC	a
zavodňování	zavodňování	k1gNnSc1	zavodňování
(	(	kIx(	(
<g/>
20	[number]	k4	20
000	[number]	k4	000
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přehrada	přehrada	k1gFnSc1	přehrada
zvedla	zvednout	k5eAaPmAgFnS	zvednout
hladinu	hladina	k1gFnSc4	hladina
řeky	řeka	k1gFnSc2	řeka
na	na	k7c4	na
26	[number]	k4	26
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
zlepšila	zlepšit	k5eAaPmAgFnS	zlepšit
tak	tak	k9	tak
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
vodní	vodní	k2eAgFnSc4d1	vodní
dopravu	doprava	k1gFnSc4	doprava
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
Donu	Don	k1gInSc6	Don
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
rozvinuté	rozvinutý	k2eAgNnSc1d1	rozvinuté
rybářství	rybářství	k1gNnSc1	rybářství
(	(	kIx(	(
<g/>
cejni	cejn	k1gMnPc1	cejn
velcí	velký	k2eAgMnPc1d1	velký
i	i	k8xC	i
siní	siný	k2eAgMnPc1d1	siný
<g/>
,	,	kIx,	,
štiky	štika	k1gFnPc4	štika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
leží	ležet	k5eAaImIp3nS	ležet
města	město	k1gNnSc2	město
Kalač	kalač	k1gInSc4	kalač
na	na	k7c4	na
Donu	dona	k1gFnSc4	dona
<g/>
,	,	kIx,	,
Cimljansk	Cimljansk	k1gInSc4	Cimljansk
<g/>
,	,	kIx,	,
Volgodonsk	Volgodonsk	k1gInSc4	Volgodonsk
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
využití	využití	k1gNnSc3	využití
vodní	vodní	k2eAgFnSc2d1	vodní
energie	energie	k1gFnSc2	energie
slouží	sloužit	k5eAaImIp3nS	sloužit
Cimljanská	Cimljanský	k2eAgFnSc1d1	Cimljanský
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
s	s	k7c7	s
výkonem	výkon	k1gInSc7	výkon
160	[number]	k4	160
MW	MW	kA	MW
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
Ц	Ц	k?	Ц
в	в	k?	в
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
Д	Д	k?	Д
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Cimljanská	Cimljanský	k2eAgFnSc1d1	Cimljanský
přehradní	přehradní	k2eAgFnSc1d1	přehradní
nádrž	nádrž	k1gFnSc1	nádrž
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
