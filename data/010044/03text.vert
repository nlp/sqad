<p>
<s>
STS-85	STS-85	k4	STS-85
byla	být	k5eAaImAgFnS	být
mise	mise	k1gFnSc1	mise
raketoplánu	raketoplán	k1gInSc2	raketoplán
Discovery	Discovera	k1gFnSc2	Discovera
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
86	[number]	k4	86
<g/>
.	.	kIx.	.
misi	mise	k1gFnSc6	mise
raketoplánu	raketoplán	k1gInSc2	raketoplán
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
a	a	k8xC	a
23	[number]	k4	23
<g/>
.	.	kIx.	.
pro	pro	k7c4	pro
Discovery	Discover	k1gInPc4	Discover
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
mise	mise	k1gFnSc2	mise
bylo	být	k5eAaImAgNnS	být
vynesení	vynesení	k1gNnSc1	vynesení
kryogenního	kryogenní	k2eAgInSc2d1	kryogenní
infračerveného	infračervený	k2eAgInSc2d1	infračervený
spektrometru	spektrometr	k1gInSc2	spektrometr
a	a	k8xC	a
teleskopu	teleskop	k1gInSc2	teleskop
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Posádka	posádka	k1gFnSc1	posádka
==	==	k?	==
</s>
</p>
<p>
<s>
Curtis	Curtis	k1gFnSc1	Curtis
Brown	Brown	k1gMnSc1	Brown
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
velitel	velitel	k1gMnSc1	velitel
</s>
</p>
<p>
<s>
Kent	Kent	k1gMnSc1	Kent
Rominger	Rominger	k1gMnSc1	Rominger
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
pilot	pilot	k1gMnSc1	pilot
</s>
</p>
<p>
<s>
Nancy	Nancy	k1gFnSc1	Nancy
Davisová	Davisový	k2eAgFnSc1d1	Davisová
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
letový	letový	k2eAgMnSc1d1	letový
specialista	specialista	k1gMnSc1	specialista
1	[number]	k4	1
</s>
</p>
<p>
<s>
Stephen	Stephen	k2eAgMnSc1d1	Stephen
Robinson	Robinson	k1gMnSc1	Robinson
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
letový	letový	k2eAgMnSc1d1	letový
specialista	specialista	k1gMnSc1	specialista
2	[number]	k4	2
</s>
</p>
<p>
<s>
Robert	Robert	k1gMnSc1	Robert
Curbeam	Curbeam	k1gInSc1	Curbeam
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
letový	letový	k2eAgMnSc1d1	letový
specialista	specialista	k1gMnSc1	specialista
2	[number]	k4	2
</s>
</p>
<p>
<s>
Bjarni	Bjarnit	k5eAaPmRp2nS	Bjarnit
Tryggvason	Tryggvason	k1gNnSc4	Tryggvason
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
specialista	specialista	k1gMnSc1	specialista
pro	pro	k7c4	pro
užitečné	užitečný	k2eAgNnSc4d1	užitečné
zatížení	zatížení	k1gNnSc4	zatížení
1	[number]	k4	1
<g/>
,	,	kIx,	,
CSAV	CSAV	kA	CSAV
závorkách	závorka	k1gFnPc6	závorka
je	být	k5eAaImIp3nS	být
uvedený	uvedený	k2eAgInSc1d1	uvedený
dosavadní	dosavadní	k2eAgInSc1d1	dosavadní
počet	počet	k1gInSc1	počet
letů	let	k1gInPc2	let
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
včetně	včetně	k7c2	včetně
této	tento	k3xDgFnSc2	tento
mise	mise	k1gFnSc2	mise
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
STS-85	STS-85	k1gFnSc2	STS-85
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
STS-85	STS-85	k1gFnSc2	STS-85
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Stránka	stránka	k1gFnSc1	stránka
o	o	k7c6	o
letu	let	k1gInSc6	let
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
NASA	NASA	kA	NASA
</s>
</p>
