<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
rychlobruslení	rychlobruslení	k1gNnSc6
na	na	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
tratích	trať	k1gFnPc6
mužů	muž	k1gMnPc2
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
rychlobruslení	rychlobruslení	k1gNnSc6
na	na	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
tratích	trať	k1gFnPc6
mužů	muž	k1gMnPc2
je	být	k5eAaImIp3nS
pořádáno	pořádat	k5eAaImNgNnS
Mezinárodní	mezinárodní	k2eAgFnSc7d1
bruslařskou	bruslařský	k2eAgFnSc7d1
unií	unie	k1gFnSc7
každoročně	každoročně	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1996	#num#	k4
společně	společně	k6eAd1
s	s	k7c7
ženským	ženský	k2eAgInSc7d1
šampionátem	šampionát	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1999	#num#	k4
se	se	k3xPyFc4
mistrovství	mistrovství	k1gNnSc2
nekoná	konat	k5eNaImIp3nS
v	v	k7c6
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
uskutečňují	uskutečňovat	k5eAaImIp3nP
zimní	zimní	k2eAgFnPc4d1
olympijské	olympijský	k2eAgFnPc4d1
hry	hra	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muži	muž	k1gMnPc1
závodí	závodit	k5eAaImIp3nP
na	na	k7c6
tratích	trať	k1gFnPc6
500	#num#	k4
<g/>
,	,	kIx,
1000	#num#	k4
<g/>
,	,	kIx,
1500	#num#	k4
<g/>
,	,	kIx,
5000	#num#	k4
a	a	k8xC
10	#num#	k4
000	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
také	také	k6eAd1
ve	v	k7c6
stíhacím	stíhací	k2eAgInSc6d1
závodě	závod	k1gInSc6
družstev	družstvo	k1gNnPc2
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
2015	#num#	k4
v	v	k7c6
závodě	závod	k1gInSc6
s	s	k7c7
hromadným	hromadný	k2eAgInSc7d1
startem	start	k1gInSc7
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
2019	#num#	k4
i	i	k8xC
v	v	k7c6
týmovém	týmový	k2eAgInSc6d1
sprintu	sprint	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
rychlobruslení	rychlobruslení	k1gNnSc6
ve	v	k7c6
víceboji	víceboj	k1gInSc6
mužů	muž	k1gMnPc2
se	se	k3xPyFc4
konají	konat	k5eAaImIp3nP
již	již	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1893	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
zimních	zimní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
jsou	být	k5eAaImIp3nP
ale	ale	k9
pořádány	pořádán	k2eAgInPc1d1
závody	závod	k1gInPc1
pouze	pouze	k6eAd1
na	na	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
tratích	trať	k1gFnPc6
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
ve	v	k7c6
víceboji	víceboj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
závodníci	závodník	k1gMnPc1
postupně	postupně	k6eAd1
začali	začít	k5eAaPmAgMnP
specializovat	specializovat	k5eAaBmF
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vyústilo	vyústit	k5eAaPmAgNnS
v	v	k7c6
rozhodnutí	rozhodnutí	k1gNnSc6
Mezinárodní	mezinárodní	k2eAgFnSc2d1
bruslařské	bruslařský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
zorganizovat	zorganizovat	k5eAaPmF
mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
i	i	k8xC
na	na	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
tratích	trať	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
koná	konat	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1996	#num#	k4
nezávisle	závisle	k6eNd1
na	na	k7c6
mistrovství	mistrovství	k1gNnSc6
světa	svět	k1gInSc2
ve	v	k7c6
víceboji	víceboj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Medailisté	medailista	k1gMnPc1
na	na	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
tratích	trať	k1gFnPc6
</s>
<s>
500	#num#	k4
metrů	metr	k1gInPc2
</s>
<s>
RokMístoZlatoStříbroBronz	RokMístoZlatoStříbroBronz	k1gMnSc1
</s>
<s>
1996	#num#	k4
<g/>
Hamar	Hamar	k1gMnSc1
Hirojasu	Hirojas	k1gInSc2
Šimizu	Šimiz	k1gInSc2
Sergej	Sergej	k1gMnSc1
Klevčenja	Klevčenja	k1gMnSc1
Roger	Roger	k1gMnSc1
Strø	Strø	k1gMnSc1
</s>
<s>
1997	#num#	k4
<g/>
Varšava	Varšava	k1gFnSc1
Manabu	Manab	k1gInSc2
Horii	Horie	k1gFnSc4
Roger	Rogra	k1gFnPc2
Strø	Strø	k1gFnPc2
Hirojasu	Hirojas	k1gInSc3
Šimizu	Šimiza	k1gFnSc4
</s>
<s>
1998	#num#	k4
<g/>
Calgary	Calgary	k1gNnSc7
Hirojasu	Hirojas	k1gInSc2
Šimizu	Šimiz	k1gInSc2
Sylvain	Sylvaina	k1gFnPc2
Bouchard	Bouchard	k1gMnSc1
Jeremy	Jerema	k1gFnSc2
Wotherspoon	Wotherspoon	k1gMnSc1
</s>
<s>
1999	#num#	k4
<g/>
Heerenveen	Heerenveen	k1gInSc1
Hirojasu	Hirojas	k1gInSc2
Šimizu	Šimiz	k1gInSc2
Erben	Erben	k1gMnSc1
Wennemars	Wennemarsa	k1gFnPc2
Jakko	Jakko	k1gNnSc1
Jan	Jan	k1gMnSc1
Leeuwangh	Leeuwangh	k1gMnSc1
</s>
<s>
2000	#num#	k4
<g/>
Nagano	Nagano	k1gNnSc1
Hirojasu	Hirojas	k1gInSc2
Šimizu	Šimiz	k1gInSc2
Mike	Mik	k1gFnSc2
Ireland	Irelanda	k1gFnPc2
Jeremy	Jerema	k1gFnSc2
Wotherspoon	Wotherspoon	k1gMnSc1
</s>
<s>
2001	#num#	k4
<g/>
Salt	salto	k1gNnPc2
Lake	Lak	k1gFnSc2
City	city	k1gNnSc1
Hirojasu	Hirojas	k1gInSc2
Šimizu	Šimiz	k1gInSc2
Jeremy	Jerema	k1gFnSc2
Wotherspoon	Wotherspoon	k1gMnSc1
Casey	Casea	k1gFnSc2
FitzRandolph	FitzRandolph	k1gMnSc1
</s>
<s>
2002	#num#	k4
<g/>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
se	se	k3xPyFc4
nekonalo	konat	k5eNaImAgNnS
kvůli	kvůli	k7c3
pořádání	pořádání	k1gNnSc3
zimních	zimní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
</s>
<s>
2003	#num#	k4
<g/>
Berlín	Berlín	k1gInSc1
Jeremy	Jerema	k1gFnSc2
Wotherspoon	Wotherspoona	k1gFnPc2
Hirojasu	Hirojas	k1gInSc2
Šimizu	Šimiz	k1gInSc2
Erben	Erben	k1gMnSc1
Wennemars	Wennemarsa	k1gFnPc2
</s>
<s>
2004	#num#	k4
<g/>
Soul	Soul	k1gInSc1
Jeremy	Jerema	k1gFnSc2
Wotherspoon	Wotherspoona	k1gFnPc2
Dmitrij	Dmitrij	k1gFnSc2
Lobkov	Lobkov	k1gInSc1
Mike	Mike	k1gFnSc4
Ireland	Irelando	k1gNnPc2
</s>
<s>
2005	#num#	k4
<g/>
Inzell	Inzell	k1gInSc1
Džódži	Džódž	k1gFnSc6
Kató	Kató	k1gFnSc6
Hirojasu	Hirojas	k1gInSc2
Šimizu	Šimiz	k1gInSc2
Jeremy	Jerema	k1gFnSc2
Wotherspoon	Wotherspoona	k1gFnPc2
</s>
<s>
2006	#num#	k4
<g/>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
se	se	k3xPyFc4
nekonalo	konat	k5eNaImAgNnS
kvůli	kvůli	k7c3
pořádání	pořádání	k1gNnSc3
zimních	zimní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
</s>
<s>
2007	#num#	k4
<g/>
Salt	salto	k1gNnPc2
Lake	Lak	k1gFnSc2
City	city	k1gNnSc1
I	i	k8xC
Kang-sok	Kang-sok	k1gInSc1
Júja	Jújum	k1gNnSc2
Oikawa	Oikaw	k1gInSc2
Tucker	Tuckra	k1gFnPc2
Fredricks	Fredricksa	k1gFnPc2
</s>
<s>
2008	#num#	k4
<g/>
Nagano	Nagano	k1gNnSc1
Jeremy	Jerema	k1gFnSc2
Wotherspoon	Wotherspoon	k1gInSc1
I	i	k8xC
Kju-hjok	Kju-hjok	k1gInSc1
Džódži	Džódž	k1gFnSc3
Kató	Kató	k1gFnSc3
</s>
<s>
2009	#num#	k4
<g/>
Richmond	Richmond	k1gMnSc1
I	i	k9
Kang-sok	Kang-sok	k1gInSc4
I	i	k8xC
Kju-hjok	Kju-hjok	k1gInSc4
Jü	Jü	k1gFnSc2
Feng-tchung	Feng-tchunga	k1gFnPc2
</s>
<s>
2010	#num#	k4
<g/>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
se	se	k3xPyFc4
nekonalo	konat	k5eNaImAgNnS
kvůli	kvůli	k7c3
pořádání	pořádání	k1gNnSc3
zimních	zimní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
</s>
<s>
2011	#num#	k4
<g/>
Inzell	Inzella	k1gFnPc2
I	i	k8xC
Kju-hjok	Kju-hjok	k1gInSc1
Džódži	Džódž	k1gFnSc3
Kató	Kató	k1gFnSc1
Jan	Jan	k1gMnSc1
Smeekens	Smeekens	k1gInSc1
</s>
<s>
2012	#num#	k4
<g/>
Heerenveen	Heerenvena	k1gFnPc2
Mo	Mo	k1gFnPc2
Tche-pom	Tche-pom	k1gInSc1
Michel	Michel	k1gMnSc1
Mulder	Mulder	k1gMnSc1
Pekka	Pekka	k1gMnSc1
Koskela	Koskela	k1gFnSc1
</s>
<s>
2013	#num#	k4
<g/>
Soči	Soči	k1gNnSc7
Mo	Mo	k1gFnPc2
Tche-pom	Tche-pom	k1gInSc4
Džódži	Džódž	k1gFnSc3
Kató	Kató	k1gFnSc1
Jan	Jan	k1gMnSc1
Smeekens	Smeekens	k1gInSc1
</s>
<s>
2014	#num#	k4
<g/>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
se	se	k3xPyFc4
nekonalo	konat	k5eNaImAgNnS
kvůli	kvůli	k7c3
pořádání	pořádání	k1gNnSc3
zimních	zimní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
</s>
<s>
2015	#num#	k4
<g/>
Heerenveen	Heerenvena	k1gFnPc2
Pavel	Pavla	k1gFnPc2
Kuližnikov	Kuližnikov	k1gInSc1
Michel	Michel	k1gMnSc1
Mulder	Mulder	k1gMnSc1
Laurent	Laurent	k1gMnSc1
Dubreuil	Dubreuil	k1gMnSc1
</s>
<s>
2016	#num#	k4
<g/>
Kolomna	Kolomna	k1gFnSc1
Pavel	Pavel	k1gMnSc1
Kuližnikov	Kuližnikov	k1gInSc1
Ruslan	Ruslana	k1gFnPc2
Murašov	Murašov	k1gInSc1
Alex	Alex	k1gMnSc1
Boisvert-Lacroix	Boisvert-Lacroix	k1gInSc1
</s>
<s>
2017	#num#	k4
<g/>
Kangnung	Kangnung	k1gMnSc1
Jan	Jan	k1gMnSc1
Smeekens	Smeekensa	k1gFnPc2
Nico	Nico	k1gMnSc1
Ihle	Ihl	k1gFnSc2
Ruslan	Ruslan	k1gMnSc1
Murašov	Murašov	k1gInSc1
</s>
<s>
2018	#num#	k4
<g/>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
se	se	k3xPyFc4
nekonalo	konat	k5eNaImAgNnS
kvůli	kvůli	k7c3
pořádání	pořádání	k1gNnSc3
zimních	zimní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
</s>
<s>
2019	#num#	k4
<g/>
Inzell	Inzella	k1gFnPc2
Ruslan	Ruslana	k1gFnPc2
Murašov	Murašov	k1gInSc1
Hå	Hå	k1gMnSc1
Holmefjord	Holmefjord	k1gMnSc1
Lorentzen	Lorentzen	k2eAgMnSc1d1
Viktor	Viktor	k1gMnSc1
Muštakov	Muštakov	k1gInSc4
</s>
<s>
2020	#num#	k4
<g/>
Salt	salto	k1gNnPc2
Lake	Lak	k1gFnSc2
City	City	k1gFnSc1
Pavel	Pavel	k1gMnSc1
Kuližnikov	Kuližnikov	k1gInSc1
Ruslan	Ruslan	k1gInSc1
Murašov	Murašov	k1gInSc1
Tacuja	Tacujus	k1gMnSc2
Šinhama	Šinham	k1gMnSc2
</s>
<s>
2021	#num#	k4
<g/>
Heerenveen	Heerenveen	k2eAgInSc4d1
Laurent	Laurent	k1gInSc4
Dubreuil	Dubreuil	k1gMnSc1
Pavel	Pavel	k1gMnSc1
Kuližnikov	Kuližnikov	k1gInSc4
Dai	Dai	k1gFnSc2
Dai	Dai	k1gFnSc2
N	N	kA
<g/>
'	'	kIx"
<g/>
tab	tab	kA
</s>
<s>
1000	#num#	k4
metrů	metr	k1gInPc2
</s>
<s>
RokMístoZlatoStříbroBronz	RokMístoZlatoStříbroBronz	k1gMnSc1
</s>
<s>
1996	#num#	k4
<g/>
Hamar	Hamar	k1gMnSc1
Sergej	Sergej	k1gMnSc1
Klevčenja	Klevčenja	k1gMnSc1
Å	Å	k5eAaPmIp3nS
Sø	Sø	k1gMnSc1
Čegal	Čegal	k1gMnSc1
Song-jol	Song-jol	k1gInSc4
</s>
<s>
1997	#num#	k4
<g/>
Varšava	Varšava	k1gFnSc1
Å	Å	k5eAaPmIp3nS
Sø	Sø	k1gMnSc1
Jan	Jan	k1gMnSc1
Bos	bos	k1gMnSc1
Martin	Martin	k1gMnSc1
Hersman	Hersman	k1gMnSc1
</s>
<s>
1998	#num#	k4
<g/>
Calgary	Calgary	k1gNnSc1
Sylvain	Sylvaina	k1gFnPc2
Bouchard	Boucharda	k1gFnPc2
Jeremy	Jerema	k1gFnSc2
Wotherspoon	Wotherspoon	k1gNnSc1
Hirojasu	Hirojas	k1gInSc2
Šimizu	Šimiz	k1gInSc2
</s>
<s>
1999	#num#	k4
<g/>
Heerenveen	Heerenvena	k1gFnPc2
Jan	Jan	k1gMnSc1
Bos	bos	k1gMnSc1
Hirojasu	Hirojas	k1gInSc2
Šimizu	Šimiz	k1gInSc2
Jakko	Jakko	k1gNnSc1
Jan	Jan	k1gMnSc1
Leeuwangh	Leeuwangh	k1gMnSc1
</s>
<s>
2000	#num#	k4
<g/>
Nagano	Nagano	k1gNnSc1
Å	Å	k5eAaPmIp3nS
Sø	Sø	k1gMnSc1
Jan	Jan	k1gMnSc1
Bos	bos	k2eAgInSc4d1
Mike	Mike	k1gInSc4
Ireland	Irelanda	k1gFnPc2
</s>
<s>
2001	#num#	k4
<g/>
Salt	salto	k1gNnPc2
Lake	Lak	k1gFnSc2
City	city	k1gNnSc1
Jeremy	Jerema	k1gFnSc2
Wotherspoon	Wotherspoona	k1gFnPc2
Å	Å	k5eAaPmIp3nS
Sø	Sø	k1gMnSc1
Sergej	Sergej	k1gMnSc1
Klevčenja	Klevčenja	k1gMnSc1
</s>
<s>
2002	#num#	k4
<g/>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
se	se	k3xPyFc4
nekonalo	konat	k5eNaImAgNnS
kvůli	kvůli	k7c3
pořádání	pořádání	k1gNnSc3
zimních	zimní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
</s>
<s>
2003	#num#	k4
<g/>
Berlín	Berlín	k1gInSc1
Erben	Erben	k1gMnSc1
Wennemars	Wennemarsa	k1gFnPc2
Gerard	Gerarda	k1gFnPc2
van	van	k1gInSc1
Velde	Veld	k1gInSc5
Joey	Joea	k1gFnPc4
Cheek	Cheek	k1gMnSc1
</s>
<s>
2004	#num#	k4
<g/>
Soul	Soul	k1gInSc1
Erben	Erben	k1gMnSc1
Wennemars	Wennemarsa	k1gFnPc2
Jeremy	Jerema	k1gFnSc2
Wotherspoon	Wotherspoon	k1gInSc1
Masaaki	Masaak	k1gFnSc2
Kobajaši	Kobajaše	k1gFnSc3
</s>
<s>
2005	#num#	k4
<g/>
Inzell	Inzell	k1gMnSc1
Even	Even	k1gMnSc1
Wetten	Wetten	k2eAgMnSc1d1
Jan	Jan	k1gMnSc1
Bos	bos	k1gMnSc1
Petter	Petter	k1gMnSc1
Andersen	Andersen	k1gMnSc1
Pekka	Pekka	k1gMnSc1
Koskela	Koskela	k1gFnSc1
</s>
<s>
2006	#num#	k4
<g/>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
se	se	k3xPyFc4
nekonalo	konat	k5eNaImAgNnS
kvůli	kvůli	k7c3
pořádání	pořádání	k1gNnSc3
zimních	zimní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
</s>
<s>
2007	#num#	k4
<g/>
Salt	salto	k1gNnPc2
Lake	Lak	k1gFnSc2
City	city	k1gNnSc1
Shani	Shan	k1gMnPc1
Davis	Davis	k1gFnSc2
Denny	Denna	k1gFnSc2
Morrison	Morrison	k1gInSc1
I	i	k8xC
Kju-hjok	Kju-hjok	k1gInSc1
</s>
<s>
2008	#num#	k4
<g/>
Nagano	Nagano	k1gNnSc1
Shani	Shaeň	k1gFnSc3
Davis	Davis	k1gFnSc1
Jevgenij	Jevgenij	k1gFnSc1
Lalenkov	Lalenkov	k1gInSc4
Denny	Denna	k1gFnSc2
Morrison	Morrisona	k1gFnPc2
</s>
<s>
2009	#num#	k4
<g/>
Richmond	Richmond	k1gMnSc1
Trevor	Trevor	k1gMnSc1
Marsicano	Marsicana	k1gFnSc5
Denny	Denna	k1gFnPc1
Morrison	Morrison	k1gNnSc1
Shani	Shan	k1gMnPc1
Davis	Davis	k1gFnSc2
</s>
<s>
2010	#num#	k4
<g/>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
se	se	k3xPyFc4
nekonalo	konat	k5eNaImAgNnS
kvůli	kvůli	k7c3
pořádání	pořádání	k1gNnSc3
zimních	zimní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
</s>
<s>
2011	#num#	k4
<g/>
Inzell	Inzell	k1gInSc4
Shani	Shaeň	k1gFnSc3
Davis	Davis	k1gInSc1
Kjeld	Kjeld	k1gMnSc1
Nuis	Nuisa	k1gFnPc2
Stefan	Stefan	k1gMnSc1
Groothuis	Groothuis	k1gFnSc2
</s>
<s>
2012	#num#	k4
<g/>
Heerenveen	Heerenvena	k1gFnPc2
Stefan	Stefan	k1gMnSc1
Groothuis	Groothuis	k1gFnSc2
Kjeld	Kjeld	k1gMnSc1
Nuis	Nuis	k1gInSc4
Shani	Shaeň	k1gFnSc3
Davis	Davis	k1gInSc1
</s>
<s>
2013	#num#	k4
<g/>
Soči	Soči	k1gNnSc7
Denis	Denisa	k1gFnPc2
Kuzin	Kuzin	k1gInSc4
Mo	Mo	k1gFnSc4
Tche-pom	Tche-pom	k1gInSc4
Shani	Shaeň	k1gFnSc3
Davis	Davis	k1gInSc1
</s>
<s>
2014	#num#	k4
<g/>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
se	se	k3xPyFc4
nekonalo	konat	k5eNaImAgNnS
kvůli	kvůli	k7c3
pořádání	pořádání	k1gNnSc3
zimních	zimní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
</s>
<s>
2015	#num#	k4
<g/>
Heerenveen	Heerenveen	k2eAgInSc1d1
Shani	Shaeň	k1gFnSc3
Davis	Davis	k1gFnSc2
Pavel	Pavla	k1gFnPc2
Kuližnikov	Kuližnikov	k1gInSc1
Kjeld	Kjeld	k1gMnSc1
Nuis	Nuis	k1gInSc1
</s>
<s>
2016	#num#	k4
<g/>
Kolomna	Kolomna	k1gFnSc1
Pavel	Pavel	k1gMnSc1
Kuližnikov	Kuližnikov	k1gInSc1
Denis	Denisa	k1gFnPc2
Juskov	Juskov	k1gInSc1
Kjeld	Kjeld	k1gMnSc1
Nuis	Nuis	k1gInSc1
</s>
<s>
2017	#num#	k4
<g/>
Kangnung	Kangnung	k1gMnSc1
Kjeld	Kjeld	k1gMnSc1
Nuis	Nuisa	k1gFnPc2
Vincent	Vincent	k1gMnSc1
De	De	k?
Haître	Haîtr	k1gMnSc5
Kai	Kai	k1gMnSc5
Verbij	Verbij	k1gMnSc5
</s>
<s>
2018	#num#	k4
<g/>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
se	se	k3xPyFc4
nekonalo	konat	k5eNaImAgNnS
kvůli	kvůli	k7c3
pořádání	pořádání	k1gNnSc3
zimních	zimní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
</s>
<s>
2019	#num#	k4
<g/>
Inzell	Inzell	k1gMnSc1
Kai	Kai	k1gMnSc1
Verbij	Verbij	k1gMnSc1
Thomas	Thomas	k1gMnSc1
Krol	Krol	k1gMnSc1
Kjeld	Kjeld	k1gMnSc1
Nuis	Nuis	k1gInSc4
</s>
<s>
2020	#num#	k4
<g/>
Salt	salto	k1gNnPc2
Lake	Lak	k1gInSc2
City	City	k1gFnSc2
Pavel	Pavla	k1gFnPc2
Kuližnikov	Kuližnikov	k1gInSc1
Kjeld	Kjeld	k1gMnSc1
Nuis	Nuisa	k1gFnPc2
Laurent	Laurent	k1gMnSc1
Dubreuil	Dubreuil	k1gMnSc1
</s>
<s>
2021	#num#	k4
<g/>
Heerenveen	Heerenveen	k1gInSc1
Kai	Kai	k1gFnSc2
Verbij	Verbij	k1gFnSc2
Pavel	Pavla	k1gFnPc2
Kuližnikov	Kuližnikov	k1gInSc1
Laurent	Laurent	k1gMnSc1
Dubreuil	Dubreuil	k1gMnSc1
</s>
<s>
1500	#num#	k4
metrů	metr	k1gInPc2
</s>
<s>
RokMístoZlatoStříbroBronz	RokMístoZlatoStříbroBronz	k1gMnSc1
</s>
<s>
1996	#num#	k4
<g/>
Hamar	Hamar	k1gInSc1
Jeroen	Jeroen	k2eAgInSc4d1
Straathof	Straathof	k1gInSc4
Å	Å	k5eAaPmIp3nS
Sø	Sø	k1gMnSc1
Martin	Martin	k1gMnSc1
Hersman	Hersman	k1gMnSc1
</s>
<s>
1997	#num#	k4
<g/>
Varšava	Varšava	k1gFnSc1
Rintje	Rintje	k1gFnSc1
Ritsma	Ritsma	k1gFnSc1
Å	Å	k5eAaPmIp3nS
Sø	Sø	k1gMnSc1
Neal	Neal	k1gMnSc1
Marshall	Marshall	k1gMnSc1
</s>
<s>
1998	#num#	k4
<g/>
Calgary	Calgary	k1gNnSc6
Å	Å	k5eAaPmIp3nS
Sø	Sø	k1gInSc1
Ids	Ids	k1gMnSc2
Postma	Postm	k1gMnSc2
Roberto	Roberta	k1gFnSc5
Sighel	Sighel	k1gInSc1
</s>
<s>
1999	#num#	k4
<g/>
Heerenveen	Heerenveen	k1gInSc1
Ids	Ids	k1gFnSc2
Postma	Postmum	k1gNnSc2
Å	Å	k5eAaPmIp3nS
Sø	Sø	k1gInSc1
Rintje	Rintj	k1gMnSc2
Ritsma	Ritsm	k1gMnSc2
</s>
<s>
2000	#num#	k4
<g/>
Nagano	Nagano	k1gNnSc1
Ids	Ids	k1gFnPc2
Postma	Postmum	k1gNnSc2
Å	Å	k5eAaPmIp3nS
Sø	Sø	k1gMnSc1
Jan	Jan	k1gMnSc1
Bos	bos	k1gMnSc1
</s>
<s>
2001	#num#	k4
<g/>
Salt	salto	k1gNnPc2
Lake	Lake	k1gFnSc1
City	City	k1gFnPc7
Å	Å	k5eAaPmIp3nS
Sø	Sø	k1gMnSc1
Derek	Derek	k1gMnSc1
Parra	Parra	k1gMnSc1
Erben	Erben	k1gMnSc1
Wennemars	Wennemars	k1gInSc4
</s>
<s>
2002	#num#	k4
<g/>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
se	se	k3xPyFc4
nekonalo	konat	k5eNaImAgNnS
kvůli	kvůli	k7c3
pořádání	pořádání	k1gNnSc3
zimních	zimní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
</s>
<s>
2003	#num#	k4
<g/>
Berlín	Berlín	k1gInSc1
Erben	Erben	k1gMnSc1
Wennemars	Wennemarsa	k1gFnPc2
Ralf	Ralf	k1gMnSc1
van	van	k1gInSc1
der	drát	k5eAaImRp2nS
Rijst	Rijst	k1gInSc4
Joey	Joea	k1gMnSc2
Cheek	Cheek	k6eAd1
</s>
<s>
2004	#num#	k4
<g/>
Soul	Soul	k1gInSc1
Shani	Shaeň	k1gFnSc3
Davis	Davis	k1gFnSc1
Mark	Mark	k1gMnSc1
Tuitert	Tuitert	k1gMnSc1
Erben	Erben	k1gMnSc1
Wennemars	Wennemars	k1gInSc4
</s>
<s>
2005	#num#	k4
<g/>
Inzell	Inzell	k1gInSc1
Rune	run	k1gInSc5
Stordal	Stordal	k1gFnSc2
Mark	Mark	k1gMnSc1
Tuitert	Tuitert	k1gMnSc1
Even	Even	k1gMnSc1
Wetten	Wetten	k2eAgMnSc1d1
</s>
<s>
2006	#num#	k4
<g/>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
se	se	k3xPyFc4
nekonalo	konat	k5eNaImAgNnS
kvůli	kvůli	k7c3
pořádání	pořádání	k1gNnSc3
zimních	zimní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
</s>
<s>
2007	#num#	k4
<g/>
Salt	salto	k1gNnPc2
Lake	Lak	k1gFnSc2
City	city	k1gNnSc1
Shani	Shan	k1gMnPc1
Davis	Davis	k1gFnSc2
Erben	Erben	k1gMnSc1
Wennemars	Wennemarsa	k1gFnPc2
Denny	Denna	k1gFnSc2
Morrison	Morrison	k1gMnSc1
</s>
<s>
2008	#num#	k4
<g/>
Nagano	Nagano	k1gNnSc1
Denny	Denna	k1gFnPc1
Morrison	Morrison	k1gMnSc1
Sven	Sven	k1gMnSc1
Kramer	Kramer	k1gInSc4
Shani	Shaeň	k1gFnSc3
Davis	Davis	k1gInSc1
<g/>
—	—	k?
</s>
<s>
2009	#num#	k4
<g/>
Richmond	Richmond	k1gInSc1
Shani	Shaň	k1gMnPc7
Davis	Davis	k1gFnSc4
Trevor	Trevora	k1gFnPc2
Marsicano	Marsicana	k1gFnSc5
Denny	Denn	k1gInPc7
Morrison	Morrison	k1gMnSc1
</s>
<s>
2010	#num#	k4
<g/>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
se	se	k3xPyFc4
nekonalo	konat	k5eNaImAgNnS
kvůli	kvůli	k7c3
pořádání	pořádání	k1gNnSc3
zimních	zimní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
</s>
<s>
2011	#num#	k4
<g/>
Inzell	Inzell	k1gMnSc1
Hå	Hå	k1gMnSc1
Bø	Bø	k1gNnSc4
Shani	Shaeň	k1gFnSc3
Davis	Davis	k1gFnSc2
Lucas	Lucasa	k1gFnPc2
Makowsky	Makowska	k1gFnSc2
</s>
<s>
2012	#num#	k4
<g/>
Heerenveen	Heerenveen	k1gInSc1
Denny	Denna	k1gFnSc2
Morrison	Morrison	k1gMnSc1
Ivan	Ivan	k1gMnSc1
Skobrev	Skobrva	k1gFnPc2
Hå	Hå	k1gMnSc1
Bø	Bø	k1gNnSc1
</s>
<s>
2013	#num#	k4
<g/>
Soči	Soči	k1gNnSc7
Denis	Denisa	k1gFnPc2
Juskov	Juskov	k1gInSc4
Shani	Shaeň	k1gFnSc3
Davis	Davis	k1gInSc1
Ivan	Ivan	k1gMnSc1
Skobrev	Skobrev	k1gFnSc1
</s>
<s>
2014	#num#	k4
<g/>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
se	se	k3xPyFc4
nekonalo	konat	k5eNaImAgNnS
kvůli	kvůli	k7c3
pořádání	pořádání	k1gNnSc3
zimních	zimní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
</s>
<s>
2015	#num#	k4
<g/>
Heerenveen	Heerenvena	k1gFnPc2
Denis	Denisa	k1gFnPc2
Juskov	Juskov	k1gInSc1
Denny	Denna	k1gFnSc2
Morrison	Morrison	k1gMnSc1
Koen	Koen	k1gMnSc1
Verweij	Verweij	k1gMnSc1
</s>
<s>
2016	#num#	k4
<g/>
Kolomna	Kolomn	k1gInSc2
Denis	Denisa	k1gFnPc2
Juskov	Juskov	k1gInSc1
Kjeld	Kjeld	k1gMnSc1
Nuis	Nuisa	k1gFnPc2
Thomas	Thomas	k1gMnSc1
Krol	Krol	k1gMnSc1
</s>
<s>
2017	#num#	k4
<g/>
Kangnung	Kangnung	k1gMnSc1
Kjeld	Kjeld	k1gMnSc1
Nuis	Nuisa	k1gFnPc2
Denis	Denisa	k1gFnPc2
Juskov	Juskov	k1gInSc1
Sven	Sven	k1gMnSc1
Kramer	Kramer	k1gMnSc1
</s>
<s>
2018	#num#	k4
<g/>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
se	se	k3xPyFc4
nekonalo	konat	k5eNaImAgNnS
kvůli	kvůli	k7c3
pořádání	pořádání	k1gNnSc3
zimních	zimní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
</s>
<s>
2019	#num#	k4
<g/>
Inzell	Inzell	k1gMnSc1
Thomas	Thomas	k1gMnSc1
Krol	Krol	k1gMnSc1
Sverre	Sverr	k1gInSc5
Lunde	Lund	k1gInSc5
Pedersen	Pedersna	k1gFnPc2
Denis	Denisa	k1gFnPc2
Juskov	Juskov	k1gInSc1
</s>
<s>
2020	#num#	k4
<g/>
Salt	salto	k1gNnPc2
Lake	Lak	k1gInSc2
City	City	k1gFnSc2
Kjeld	Kjeld	k1gMnSc1
Nuis	Nuisa	k1gFnPc2
Thomas	Thomas	k1gMnSc1
Krol	Krol	k1gMnSc1
Joey	Joea	k1gMnSc2
Mantia	Mantium	k1gNnSc2
</s>
<s>
2021	#num#	k4
<g/>
Heerenveen	Heerenvena	k1gFnPc2
Thomas	Thomas	k1gMnSc1
Krol	Krol	k1gMnSc1
Kjeld	Kjeld	k1gMnSc1
Nuis	Nuisa	k1gFnPc2
Patrick	Patrick	k1gMnSc1
Roest	Roest	k1gMnSc1
</s>
<s>
5000	#num#	k4
metrů	metr	k1gInPc2
</s>
<s>
RokMístoZlatoStříbroBronz	RokMístoZlatoStříbroBronz	k1gMnSc1
</s>
<s>
1996	#num#	k4
<g/>
Hamar	Hamar	k1gMnSc1
Ids	Ids	k1gMnSc1
Postma	Postma	k1gNnSc1
Keidži	Keidž	k1gFnSc3
Širahata	Širahata	k1gFnSc1
Gianni	Gianň	k1gMnSc6
Romme	Romm	k1gInSc5
</s>
<s>
1997	#num#	k4
<g/>
Varšava	Varšava	k1gFnSc1
Rintje	Rintje	k1gFnSc1
Ritsma	Ritsma	k1gFnSc1
Gianni	Giann	k1gMnPc1
Romme	Romm	k1gInSc5
Frank	Frank	k1gMnSc1
Dittrich	Dittri	k1gFnPc6
</s>
<s>
1998	#num#	k4
<g/>
Calgary	Calgary	k1gNnPc7
Gianni	Giann	k1gMnPc1
Romme	Romm	k1gInSc5
Rintje	Rintj	k1gMnSc4
Ritsma	Ritsmum	k1gNnSc2
Bart	Bart	k2eAgInSc4d1
Veldkamp	Veldkamp	k1gInSc4
</s>
<s>
1999	#num#	k4
<g/>
Heerenveen	Heerenvena	k1gFnPc2
Gianni	Giann	k1gMnPc1
Romme	Romm	k1gInSc5
Bart	Barta	k1gFnPc2
Veldkamp	Veldkamp	k1gMnSc1
Bob	Bob	k1gMnSc1
de	de	k?
Jong	Jong	k1gMnSc1
</s>
<s>
2000	#num#	k4
<g/>
Nagano	Nagano	k1gNnSc1
Gianni	Giann	k1gMnPc1
Romme	Romm	k1gInSc5
Bob	Bob	k1gMnSc1
de	de	k?
Jong	Jong	k1gMnSc1
Keidži	Keidž	k1gFnSc3
Širahata	Širahat	k2eAgFnSc1d1
</s>
<s>
2001	#num#	k4
<g/>
Salt	salto	k1gNnPc2
Lake	Lak	k1gInSc2
City	City	k1gFnSc2
Bob	Bob	k1gMnSc1
de	de	k?
Jong	Jong	k1gMnSc1
Carl	Carl	k1gMnSc1
Verheijen	Verheijna	k1gFnPc2
Gianni	Gianeň	k1gFnSc3
Romme	Romm	k1gInSc5
</s>
<s>
2002	#num#	k4
<g/>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
se	se	k3xPyFc4
nekonalo	konat	k5eNaImAgNnS
kvůli	kvůli	k7c3
pořádání	pořádání	k1gNnSc3
zimních	zimní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
</s>
<s>
2003	#num#	k4
<g/>
Berlín	Berlín	k1gInSc1
Jochem	Jochem	k?
Uytdehaage	Uytdehaage	k1gInSc1
Bob	Bob	k1gMnSc1
de	de	k?
Jong	Jong	k1gMnSc1
Carl	Carl	k1gMnSc1
Verheijen	Verheijna	k1gFnPc2
</s>
<s>
2004	#num#	k4
<g/>
Soul	Soul	k1gInSc1
Chad	Chad	k1gMnSc1
Hedrick	Hedrick	k1gMnSc1
Carl	Carl	k1gMnSc1
Verheijen	Verheijna	k1gFnPc2
Gianni	Gianň	k1gMnPc7
Romme	Romm	k1gInSc5
</s>
<s>
2005	#num#	k4
<g/>
Inzell	Inzell	k1gMnSc1
Chad	Chad	k1gMnSc1
Hedrick	Hedrick	k1gMnSc1
Bob	Bob	k1gMnSc1
de	de	k?
Jong	Jong	k1gMnSc1
Carl	Carl	k1gMnSc1
Verheijen	Verheijna	k1gFnPc2
</s>
<s>
2006	#num#	k4
<g/>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
se	se	k3xPyFc4
nekonalo	konat	k5eNaImAgNnS
kvůli	kvůli	k7c3
pořádání	pořádání	k1gNnSc3
zimních	zimní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
</s>
<s>
2007	#num#	k4
<g/>
Salt	salto	k1gNnPc2
Lake	Lak	k1gFnSc2
City	city	k1gNnSc1
Sven	Sven	k1gInSc1
Kramer	Kramer	k1gMnSc1
Enrico	Enrico	k1gMnSc1
Fabris	Fabris	k1gFnSc2
Carl	Carl	k1gMnSc1
Verheijen	Verheijna	k1gFnPc2
</s>
<s>
2008	#num#	k4
<g/>
Nagano	Nagano	k1gNnSc1
Sven	Svena	k1gFnPc2
Kramer	Kramero	k1gNnPc2
Enrico	Enrico	k1gMnSc1
Fabris	Fabris	k1gFnSc2
Wouter	Wouter	k1gMnSc1
olde	olde	k1gFnPc2
Heuvel	Heuvel	k1gMnSc1
</s>
<s>
2009	#num#	k4
<g/>
Richmond	Richmond	k1gMnSc1
Sven	Sven	k1gMnSc1
Kramer	Kramer	k1gMnSc1
Hå	Hå	k1gMnSc1
Bø	Bø	k1gNnSc1
Trevor	Trevor	k1gMnSc1
Marsicano	Marsicana	k1gFnSc5
</s>
<s>
2010	#num#	k4
<g/>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
se	se	k3xPyFc4
nekonalo	konat	k5eNaImAgNnS
kvůli	kvůli	k7c3
pořádání	pořádání	k1gNnSc3
zimních	zimní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
</s>
<s>
2011	#num#	k4
<g/>
Inzell	Inzell	k1gMnSc1
Bob	Bob	k1gMnSc1
de	de	k?
Jong	Jong	k1gMnSc1
I	i	k8xC
Sung-hun	Sung-hun	k1gMnSc1
Ivan	Ivan	k1gMnSc1
Skobrev	Skobrev	k1gFnSc4
</s>
<s>
2012	#num#	k4
<g/>
Heerenveen	Heerenveen	k2eAgInSc4d1
Sven	Sven	k1gInSc4
Kramer	Kramer	k1gMnSc1
Bob	Bob	k1gMnSc1
de	de	k?
Jong	Jong	k1gMnSc1
Jonathan	Jonathan	k1gMnSc1
Kuck	Kuck	k1gMnSc1
</s>
<s>
2013	#num#	k4
<g/>
Soči	Soči	k1gNnSc7
Sven	Svena	k1gFnPc2
Kramer	Kramer	k1gInSc4
Jorrit	Jorrit	k1gInSc1
Bergsma	Bergsma	k1gNnSc1
Ivan	Ivana	k1gFnPc2
Skobrev	Skobrva	k1gFnPc2
</s>
<s>
2014	#num#	k4
<g/>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
se	se	k3xPyFc4
nekonalo	konat	k5eNaImAgNnS
kvůli	kvůli	k7c3
pořádání	pořádání	k1gNnSc3
zimních	zimní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
</s>
<s>
2015	#num#	k4
<g/>
Heerenveen	Heerenveen	k2eAgInSc1d1
Sven	Sven	k1gInSc1
Kramer	Kramra	k1gFnPc2
Jorrit	Jorrita	k1gFnPc2
Bergsma	Bergsm	k1gMnSc2
Douwe	Douw	k1gMnSc2
de	de	k?
Vries	Vries	k1gMnSc1
</s>
<s>
2016	#num#	k4
<g/>
Kolomna	Kolomn	k1gInSc2
Sven	Sven	k1gMnSc1
Kramer	Kramer	k1gMnSc1
Jorrit	Jorrit	k1gInSc4
Bergsma	Bergsmum	k1gNnSc2
Sverre	Sverr	k1gInSc5
Lunde	Lund	k1gInSc5
Pedersen	Pedersen	k1gInSc1
</s>
<s>
2017	#num#	k4
<g/>
Kangnung	Kangnung	k1gMnSc1
Sven	Sven	k1gMnSc1
Kramer	Kramra	k1gFnPc2
Jorrit	Jorrita	k1gFnPc2
Bergsma	Bergsmum	k1gNnSc2
Peter	Peter	k1gMnSc1
Michael	Michael	k1gMnSc1
</s>
<s>
2018	#num#	k4
<g/>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
se	se	k3xPyFc4
nekonalo	konat	k5eNaImAgNnS
kvůli	kvůli	k7c3
pořádání	pořádání	k1gNnSc3
zimních	zimní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
</s>
<s>
2019	#num#	k4
<g/>
Inzell	Inzell	k1gInSc1
Sverre	Sverr	k1gInSc5
Lunde	Lund	k1gInSc5
Pedersen	Pedersna	k1gFnPc2
Patrick	Patrick	k1gMnSc1
Roest	Roest	k1gMnSc1
Sven	Sven	k1gMnSc1
Kramer	Kramer	k1gMnSc1
</s>
<s>
2020	#num#	k4
<g/>
Salt	salto	k1gNnPc2
Lake	Lak	k1gInSc2
City	City	k1gFnSc2
Ted-Jan	Ted-Jana	k1gFnPc2
Bloemen	Bloemen	k1gInSc1
Sven	Sven	k1gMnSc1
Kramer	Kramer	k1gMnSc1
Graeme	Graem	k1gInSc5
Fish	Fish	k1gInSc1
</s>
<s>
2021	#num#	k4
<g/>
Heerenveen	Heerenveen	k2eAgInSc1d1
Nils	Nils	k1gInSc1
van	vana	k1gFnPc2
der	drát	k5eAaImRp2nS
Poel	Poel	k1gMnSc1
Patrick	Patrick	k1gMnSc1
Roest	Roest	k1gMnSc1
Sergej	Sergej	k1gMnSc1
Trofimov	Trofimov	k1gInSc4
</s>
<s>
10	#num#	k4
000	#num#	k4
metrů	metr	k1gInPc2
</s>
<s>
RokMístoZlatoStříbroBronz	RokMístoZlatoStříbroBronz	k1gMnSc1
</s>
<s>
1996	#num#	k4
<g/>
Hamar	Hamara	k1gFnPc2
Gianni	Giann	k1gMnPc1
Romme	Romm	k1gInSc5
Bart	Barta	k1gFnPc2
Veldkamp	Veldkamp	k1gMnSc1
Frank	Frank	k1gMnSc1
Dittrich	Dittrich	k1gMnSc1
</s>
<s>
1997	#num#	k4
<g/>
Varšava	Varšava	k1gFnSc1
Gianni	Giann	k1gMnPc1
Romme	Romm	k1gInSc5
Rintje	Rintj	k1gMnSc2
Ritsma	Ritsma	k1gNnSc1
Bob	Bob	k1gMnSc1
de	de	k?
Jong	Jong	k1gMnSc1
</s>
<s>
1998	#num#	k4
<g/>
Calgary	Calgary	k1gNnPc7
Gianni	Giann	k1gMnPc1
Romme	Romm	k1gInSc5
Bob	Bob	k1gMnSc1
de	de	k?
Jong	Jong	k1gMnSc1
Frank	Frank	k1gMnSc1
Dittrich	Dittrich	k1gMnSc1
</s>
<s>
1999	#num#	k4
<g/>
Heerenveen	Heerenvena	k1gFnPc2
Bob	Bob	k1gMnSc1
de	de	k?
Jong	Jong	k1gMnSc1
Gianni	Giann	k1gMnPc1
Romme	Romm	k1gInSc5
Frank	Frank	k1gMnSc1
Dittrich	Dittrich	k1gInSc4
</s>
<s>
2000	#num#	k4
<g/>
Nagano	Nagano	k1gNnSc1
Gianni	Giann	k1gMnPc1
Romme	Romm	k1gInSc5
Bob	Bob	k1gMnSc1
de	de	k?
Jong	Jong	k1gMnSc1
Frank	Frank	k1gMnSc1
Dittrich	Dittrich	k1gMnSc1
</s>
<s>
2001	#num#	k4
<g/>
Salt	salto	k1gNnPc2
Lake	Lak	k1gInSc2
City	City	k1gFnSc2
Carl	Carl	k1gMnSc1
Verheijen	Verheijna	k1gFnPc2
Bob	Bob	k1gMnSc1
de	de	k?
Jong	Jong	k1gMnSc1
Vadim	Vadim	k?
Sajutin	Sajutin	k1gMnSc1
</s>
<s>
2002	#num#	k4
<g/>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
se	se	k3xPyFc4
nekonalo	konat	k5eNaImAgNnS
kvůli	kvůli	k7c3
pořádání	pořádání	k1gNnSc3
zimních	zimní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
</s>
<s>
2003	#num#	k4
<g/>
Berlín	Berlín	k1gInSc1
Bob	bob	k1gInSc4
de	de	k?
Jong	Jong	k1gInSc1
Carl	Carl	k1gMnSc1
Verheijen	Verheijna	k1gFnPc2
Lasse	Lasse	k1gFnSc2
Sæ	Sæ	k1gMnSc5
</s>
<s>
2004	#num#	k4
<g/>
Soul	Soul	k1gInSc1
Carl	Carl	k1gMnSc1
Verheijen	Verheijna	k1gFnPc2
Bob	Bob	k1gMnSc1
de	de	k?
Jong	Jong	k1gMnSc1
Chad	Chad	k1gMnSc1
Hedrick	Hedrick	k1gMnSc1
</s>
<s>
2005	#num#	k4
<g/>
Inzell	Inzell	k1gMnSc1
Bob	Bob	k1gMnSc1
de	de	k?
Jong	Jong	k1gMnSc1
Carl	Carl	k1gMnSc1
Verheijen	Verheijna	k1gFnPc2
Chad	Chad	k1gMnSc1
Hedrick	Hedrick	k1gMnSc1
</s>
<s>
2006	#num#	k4
<g/>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
se	se	k3xPyFc4
nekonalo	konat	k5eNaImAgNnS
kvůli	kvůli	k7c3
pořádání	pořádání	k1gNnSc3
zimních	zimní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
</s>
<s>
2007	#num#	k4
<g/>
Salt	salto	k1gNnPc2
Lake	Lak	k1gFnSc2
City	city	k1gNnSc1
Sven	Sven	k1gInSc1
Kramer	Kramer	k1gMnSc1
Carl	Carl	k1gMnSc1
Verheijen	Verheijna	k1gFnPc2
Brigt	Brigt	k1gMnSc1
Rykkje	Rykkj	k1gFnSc2
</s>
<s>
2008	#num#	k4
<g/>
Nagano	Nagano	k1gNnSc1
Sven	Svena	k1gFnPc2
Kramer	Kramero	k1gNnPc2
Enrico	Enrico	k1gMnSc1
Fabris	Fabris	k1gFnSc2
Bob	Bob	k1gMnSc1
de	de	k?
Jong	Jong	k1gMnSc1
</s>
<s>
2009	#num#	k4
<g/>
Richmond	Richmond	k1gMnSc1
Sven	Sven	k1gMnSc1
Kramer	Kramer	k1gMnSc1
Hå	Hå	k1gMnSc1
Bø	Bø	k1gNnSc1
Bob	Bob	k1gMnSc1
de	de	k?
Jong	Jong	k1gMnSc1
</s>
<s>
2010	#num#	k4
<g/>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
se	se	k3xPyFc4
nekonalo	konat	k5eNaImAgNnS
kvůli	kvůli	k7c3
pořádání	pořádání	k1gNnSc3
zimních	zimní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
</s>
<s>
2011	#num#	k4
<g/>
Inzell	Inzell	k1gMnSc1
Bob	Bob	k1gMnSc1
de	de	k?
Jong	Jong	k1gMnSc1
Bob	Bob	k1gMnSc1
de	de	k?
Vries	Vries	k1gMnSc1
Ivan	Ivan	k1gMnSc1
Skobrev	Skobrev	k1gFnSc4
</s>
<s>
2012	#num#	k4
<g/>
Heerenveen	Heerenveen	k2eAgInSc1d1
Bob	bob	k1gInSc1
de	de	k?
Jong	Jong	k1gInSc1
Jorrit	Jorrita	k1gFnPc2
Bergsma	Bergsma	k1gNnSc1
Jonathan	Jonathan	k1gMnSc1
Kuck	Kuck	k1gMnSc1
</s>
<s>
2013	#num#	k4
<g/>
Soči	Soči	k1gNnSc1
Jorrit	Jorrita	k1gFnPc2
Bergsma	Bergsmum	k1gNnSc2
Sven	Sven	k1gMnSc1
Kramer	Kramer	k1gMnSc1
Bob	Bob	k1gMnSc1
de	de	k?
Jong	Jong	k1gMnSc1
</s>
<s>
2014	#num#	k4
<g/>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
se	se	k3xPyFc4
nekonalo	konat	k5eNaImAgNnS
kvůli	kvůli	k7c3
pořádání	pořádání	k1gNnSc3
zimních	zimní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
</s>
<s>
2015	#num#	k4
<g/>
Heerenveen	Heerenveen	k2eAgInSc1d1
Jorrit	Jorrit	k1gInSc1
Bergsma	Bergsma	k1gFnSc1
Erik	Erik	k1gMnSc1
Jan	Jan	k1gMnSc1
Kooiman	Kooiman	k1gMnSc1
Patrick	Patrick	k1gMnSc1
Beckert	Beckert	k1gMnSc1
</s>
<s>
2016	#num#	k4
<g/>
Kolomna	Kolomn	k1gInSc2
Sven	Sven	k1gMnSc1
Kramer	Kramer	k1gMnSc1
Ted-Jan	Ted-Jan	k1gMnSc1
Bloemen	Bloemen	k2eAgMnSc1d1
Erik	Erik	k1gMnSc1
Jan	Jan	k1gMnSc1
Kooiman	Kooiman	k1gMnSc1
</s>
<s>
2017	#num#	k4
<g/>
Kangnung	Kangnung	k1gMnSc1
Sven	Sven	k1gMnSc1
Kramer	Kramra	k1gFnPc2
Jorrit	Jorrita	k1gFnPc2
Bergsma	Bergsmum	k1gNnSc2
Patrick	Patrick	k1gMnSc1
Beckert	Beckert	k1gMnSc1
</s>
<s>
2018	#num#	k4
<g/>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
se	se	k3xPyFc4
nekonalo	konat	k5eNaImAgNnS
kvůli	kvůli	k7c3
pořádání	pořádání	k1gNnSc3
zimních	zimní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
</s>
<s>
2019	#num#	k4
<g/>
Inzell	Inzell	k1gInSc1
Jorrit	Jorrit	k1gInSc4
Bergsma	Bergsma	k1gFnSc1
Patrick	Patricka	k1gFnPc2
Roest	Roest	k1gFnSc1
Danila	danit	k5eAaImAgFnS
Semerikov	Semerikov	k1gInSc4
</s>
<s>
2020	#num#	k4
<g/>
Salt	salto	k1gNnPc2
Lake	Lak	k1gFnSc2
City	city	k1gNnSc1
Graeme	Graem	k1gInSc5
Fish	Fish	k1gInSc1
Ted-Jan	Ted-Jany	k1gInPc2
Bloemen	Bloemen	k2eAgMnSc1d1
Patrick	Patrick	k1gMnSc1
Beckert	Beckert	k1gMnSc1
</s>
<s>
2021	#num#	k4
<g/>
Heerenveen	Heerenveen	k2eAgInSc1d1
Nils	Nils	k1gInSc1
van	vana	k1gFnPc2
der	drát	k5eAaImRp2nS
Poel	Poel	k1gInSc4
Jorrit	Jorrit	k1gInSc1
Bergsma	Bergsma	k1gFnSc1
Alexandr	Alexandr	k1gMnSc1
Rumjancev	Rumjancev	k1gFnSc4
</s>
<s>
Závod	závod	k1gInSc1
s	s	k7c7
hromadným	hromadný	k2eAgInSc7d1
startem	start	k1gInSc7
</s>
<s>
RokMístoZlatoStříbroBronz	RokMístoZlatoStříbroBronz	k1gMnSc1
</s>
<s>
2015	#num#	k4
<g/>
Heerenveen	Heerenveen	k2eAgInSc1d1
Arjan	Arjan	k1gInSc1
Stroetinga	Stroeting	k1gMnSc2
Fabio	Fabio	k6eAd1
Francolini	Francolin	k2eAgMnPc1d1
Alexis	Alexis	k1gFnSc2
Contin	Contin	k1gMnSc1
</s>
<s>
2016	#num#	k4
<g/>
Kolomna	Kolomno	k1gNnPc1
I	i	k8xC
Sung-hun	Sung-hun	k1gNnSc1
Arjan	Arjan	k1gMnSc1
Stroetinga	Stroeting	k1gMnSc2
Alexis	Alexis	k1gFnSc2
Contin	Contin	k1gMnSc1
</s>
<s>
2017	#num#	k4
<g/>
Kangnung	Kangnung	k1gInSc1
Joey	Joea	k1gFnSc2
Mantia	Mantium	k1gNnSc2
Alexis	Alexis	k1gFnSc2
Contin	Contin	k1gMnSc1
Olivier	Olivier	k1gMnSc1
Jean	Jean	k1gMnSc1
</s>
<s>
2018	#num#	k4
<g/>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
se	se	k3xPyFc4
nekonalo	konat	k5eNaImAgNnS
kvůli	kvůli	k7c3
pořádání	pořádání	k1gNnSc3
zimních	zimní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
</s>
<s>
2019	#num#	k4
<g/>
Inzell	Inzell	k1gInSc1
Joey	Joea	k1gFnSc2
Mantia	Mantius	k1gMnSc2
Om	Om	k1gMnSc2
Čchon-ho	Čchon-	k1gMnSc2
Čong	Čong	k1gMnSc1
Če-won	Če-won	k1gMnSc1
</s>
<s>
2020	#num#	k4
<g/>
Salt	salto	k1gNnPc2
Lake	Lak	k1gFnSc2
City	city	k1gNnSc1
Jorrit	Jorrita	k1gFnPc2
Bergsma	Bergsmum	k1gNnSc2
Jordan	Jordan	k1gMnSc1
Belchos	Belchos	k1gMnSc1
Antoine	Antoin	k1gInSc5
Gélinas-Beaulieu	Gélinas-Beaulie	k1gMnSc6
</s>
<s>
2021	#num#	k4
<g/>
Heerenveen	Heerenveen	k1gInSc1
Joey	Joea	k1gFnSc2
Mantia	Mantius	k1gMnSc2
Arjan	Arjana	k1gFnPc2
Stroetinga	Stroeting	k1gMnSc2
Bart	Bart	k2eAgInSc4d1
Swings	Swings	k1gInSc4
</s>
<s>
Týmový	týmový	k2eAgInSc4d1
sprint	sprint	k1gInSc4
</s>
<s>
RokMístoZlatoStříbroBronz	RokMístoZlatoStříbroBronz	k1gMnSc1
</s>
<s>
2019	#num#	k4
<g/>
InzellNizozemsko	InzellNizozemsko	k1gNnSc1
NizozemskoMichel	NizozemskoMichela	k1gFnPc2
Mulder	Muldra	k1gFnPc2
<g/>
,	,	kIx,
Kjeld	Kjelda	k1gFnPc2
Nuis	Nuisa	k1gFnPc2
<g/>
,	,	kIx,
Kai	Kai	k1gFnPc2
VerbijJižní	VerbijJižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
Jižní	jižní	k2eAgFnSc2d1
KoreaKim	KoreaKim	k1gInSc1
Čun-ho	Čun-	k1gMnSc4
<g/>
,	,	kIx,
Kim	Kim	k1gMnSc1
Tche-jun	Tche-jun	k1gMnSc1
<g/>
,	,	kIx,
Čcha	Čcha	k1gMnSc1
Min-kjuRusko	Min-kjuRusko	k1gNnSc1
RuskoPavel	RuskoPavel	k1gMnSc1
Kuližnikov	Kuližnikov	k1gInSc1
<g/>
,	,	kIx,
Ruslan	Ruslan	k1gInSc1
Murašov	Murašov	k1gInSc1
<g/>
,	,	kIx,
Viktor	Viktor	k1gMnSc1
Muštakov	Muštakov	k1gInSc1
</s>
<s>
2020	#num#	k4
<g/>
Salt	salto	k1gNnPc2
Lake	Lak	k1gFnSc2
CityNizozemsko	CityNizozemsko	k1gNnSc1
NizozemskoDai	NizozemskoDai	k1gNnSc2
Dai	Dai	k1gFnPc2
N	N	kA
<g/>
'	'	kIx"
<g/>
tab	tab	kA
<g/>
,	,	kIx,
Kai	Kai	k1gMnSc1
Verbij	Verbij	k1gMnSc1
<g/>
,	,	kIx,
Thomas	Thomas	k1gMnSc1
KrolČína	KrolČín	k1gInSc2
ČínaKao	ČínaKao	k1gMnSc1
Tching-jü	Tching-jü	k1gMnSc1
<g/>
,	,	kIx,
Wang	Wang	k1gMnSc1
Š	Š	kA
<g/>
’	’	k?
<g/>
-wej	-wej	k1gInSc1
<g/>
,	,	kIx,
Ning	Ning	k1gInSc1
Čung-jenNorsko	Čung-jenNorsko	k1gNnSc1
NorskoBjø	NorskoBjø	k1gFnPc2
Magnussen	Magnussna	k1gFnPc2
<g/>
,	,	kIx,
Hå	Hå	k1gInSc1
Holmefjord	Holmefjord	k1gInSc1
Lorentzen	Lorentzen	k2eAgInSc1d1
<g/>
,	,	kIx,
Odin	Odin	k1gInSc1
By	by	k9
Farstad	Farstad	k1gInSc1
</s>
<s>
Stíhací	stíhací	k2eAgInSc1d1
závod	závod	k1gInSc1
družstev	družstvo	k1gNnPc2
</s>
<s>
RokMístoZlatoStříbroBronz	RokMístoZlatoStříbroBronz	k1gMnSc1
</s>
<s>
2005	#num#	k4
<g/>
InzellNizozemsko	InzellNizozemsko	k1gNnSc1
NizozemskoCarl	NizozemskoCarla	k1gFnPc2
Verheijen	Verheijna	k1gFnPc2
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
Tuitert	Tuitert	k1gMnSc1
<g/>
,	,	kIx,
Erben	Erben	k1gMnSc1
WennemarsItálie	WennemarsItálie	k1gFnSc2
ItálieEnrico	ItálieEnrico	k1gMnSc1
Fabris	Fabris	k1gFnSc1
<g/>
,	,	kIx,
Matteo	Matteo	k6eAd1
Anesi	Anese	k1gFnSc4
<g/>
,	,	kIx,
Ippolito	Ippolit	k2eAgNnSc4d1
SanfratelloNorsko	SanfratelloNorsko	k1gNnSc4
NorskoPetter	NorskoPetter	k1gMnSc1
Andersen	Andersen	k1gMnSc1
<g/>
,	,	kIx,
Eskil	Eskil	k1gMnSc1
Ervik	Ervik	k1gMnSc1
<g/>
,	,	kIx,
Odd	odd	kA
Bohlin	Bohlin	k2eAgInSc1d1
Borgersen	Borgersen	k1gInSc1
</s>
<s>
2006	#num#	k4
<g/>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
se	se	k3xPyFc4
nekonalo	konat	k5eNaImAgNnS
kvůli	kvůli	k7c3
pořádání	pořádání	k1gNnSc3
zimních	zimní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
</s>
<s>
2007	#num#	k4
<g/>
Salt	salto	k1gNnPc2
Lake	Lak	k1gFnSc2
CityNizozemsko	CityNizozemsko	k1gNnSc1
NizozemskoSven	NizozemskoSven	k2eAgMnSc1d1
Kramer	Kramer	k1gMnSc1
<g/>
,	,	kIx,
Erben	Erben	k1gMnSc1
Wennemars	Wennemars	k1gInSc1
<g/>
,	,	kIx,
Carl	Carl	k1gMnSc1
VerheijenKanada	VerheijenKanada	k1gFnSc1
KanadaArne	KanadaArn	k1gInSc5
Dankers	Dankersa	k1gFnPc2
<g/>
,	,	kIx,
Denny	Denna	k1gFnPc1
Morrison	Morrison	k1gMnSc1
<g/>
,	,	kIx,
Justin	Justin	k1gMnSc1
WarsylewiczRusko	WarsylewiczRusko	k1gNnSc4
RuskoJevgenij	RuskoJevgenij	k1gMnSc2
Lalenkov	Lalenkov	k1gInSc4
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
Skobrev	Skobrev	k1gFnSc1
<g/>
,	,	kIx,
Alexej	Alexej	k1gMnSc1
Junin	Junin	k1gMnSc1
</s>
<s>
2008	#num#	k4
<g/>
NaganoNizozemsko	NaganoNizozemsko	k1gNnSc1
NizozemskoSven	NizozemskoSvna	k1gFnPc2
Kramer	Kramra	k1gFnPc2
<g/>
,	,	kIx,
Erben	Erben	k1gMnSc1
Wennemars	Wennemars	k1gInSc1
<g/>
,	,	kIx,
Wouter	Wouter	k1gInSc1
olde	old	k1gFnSc2
HeuvelItálie	HeuvelItálie	k1gFnSc2
ItálieEnrico	ItálieEnrico	k1gMnSc1
Fabris	Fabris	k1gFnSc1
<g/>
,	,	kIx,
Luca	Luca	k1gMnSc1
Stefani	Stefan	k1gMnPc1
<g/>
,	,	kIx,
Matteo	Matteo	k1gMnSc1
AnesiNěmecko	AnesiNěmecko	k1gNnSc1
NěmeckoJörg	NěmeckoJörg	k1gMnSc1
Dallmann	Dallmann	k1gMnSc1
<g/>
,	,	kIx,
Stefan	Stefan	k1gMnSc1
Heythausen	Heythausen	k1gInSc1
<g/>
,	,	kIx,
Marco	Marco	k1gMnSc1
Weber	Weber	k1gMnSc1
</s>
<s>
2009	#num#	k4
<g/>
RichmondNizozemsko	RichmondNizozemsko	k1gNnSc1
NizozemskoCarl	NizozemskoCarla	k1gFnPc2
Verheijen	Verheijna	k1gFnPc2
<g/>
,	,	kIx,
Wouter	Wouter	k1gMnSc1
olde	old	k1gFnSc2
Heuvel	Heuvel	k1gMnSc1
<g/>
,	,	kIx,
Sven	Sven	k1gMnSc1
KramerŠvédsko	KramerŠvédsko	k1gNnSc1
ŠvédskoJohan	ŠvédskoJohan	k1gMnSc1
Röjler	Röjler	k1gMnSc1
<g/>
,	,	kIx,
Joel	Joel	k1gMnSc1
Eriksson	Eriksson	k1gMnSc1
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
FribergUSA	FribergUSA	k1gMnSc1
USATrevor	USATrevor	k1gMnSc1
Marsicano	Marsicana	k1gFnSc5
<g/>
,	,	kIx,
Ryan	Ryan	k1gMnSc1
Bedford	Bedford	k1gMnSc1
<g/>
,	,	kIx,
Brian	Brian	k1gMnSc1
Hansen	Hansna	k1gFnPc2
</s>
<s>
2010	#num#	k4
<g/>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
se	se	k3xPyFc4
nekonalo	konat	k5eNaImAgNnS
kvůli	kvůli	k7c3
pořádání	pořádání	k1gNnSc3
zimních	zimní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
</s>
<s>
2011	#num#	k4
<g/>
InzellUSA	InzellUSA	k1gFnSc1
USAShani	USAShaň	k1gFnSc3
Davis	Davis	k1gFnSc4
<g/>
,	,	kIx,
Trevor	Trevor	k1gInSc4
Marsicano	Marsicana	k1gFnSc5
<g/>
,	,	kIx,
Jonathan	Jonathan	k1gMnSc1
KuckKanada	KuckKanada	k1gFnSc1
KanadaDenny	KanadaDenna	k1gFnSc2
Morrison	Morrison	k1gMnSc1
<g/>
,	,	kIx,
Lucas	Lucas	k1gMnSc1
Makowsky	Makowska	k1gFnSc2
<g/>
,	,	kIx,
Mathieu	Mathie	k2eAgFnSc4d1
GirouxNizozemsko	GirouxNizozemsko	k1gNnSc1
NizozemskoBob	NizozemskoBoba	k1gFnPc2
de	de	k?
Vries	Vries	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Blokhuijsen	Blokhuijsen	k1gInSc1
<g/>
,	,	kIx,
Koen	Koen	k1gMnSc1
Verweij	Verweij	k1gMnSc1
</s>
<s>
2012	#num#	k4
<g/>
HeerenveenNizozemsko	HeerenveenNizozemsko	k1gNnSc1
NizozemskoSven	NizozemskoSvna	k1gFnPc2
Kramer	Kramra	k1gFnPc2
<g/>
,	,	kIx,
Koen	Koen	k1gMnSc1
Verweij	Verweij	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
BlokhuijsenUSA	BlokhuijsenUSA	k1gFnSc2
USAShani	USAShaň	k1gFnSc3
Davis	Davis	k1gFnSc2
<g/>
,	,	kIx,
Brian	Brian	k1gMnSc1
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
Jonathan	Jonathan	k1gMnSc1
KuckRusko	KuckRusko	k1gNnSc1
RuskoIvan	RuskoIvan	k1gMnSc1
Skobrev	Skobrva	k1gFnPc2
<g/>
,	,	kIx,
Denis	Denisa	k1gFnPc2
Juskov	Juskov	k1gInSc1
<g/>
,	,	kIx,
Jevgenij	Jevgenij	k1gMnSc1
Lalenkov	Lalenkov	k1gInSc1
</s>
<s>
2013	#num#	k4
<g/>
SočiNizozemsko	SočiNizozemsko	k1gNnSc1
NizozemskoJan	NizozemskoJana	k1gFnPc2
Blokhuijsen	Blokhuijsna	k1gFnPc2
<g/>
,	,	kIx,
Sven	Svena	k1gFnPc2
Kramer	Kramra	k1gFnPc2
<g/>
,	,	kIx,
Koen	Koena	k1gFnPc2
VerweijJižní	VerweijJižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
Jižní	jižní	k2eAgFnSc4d1
KoreaČu	KoreaČa	k1gFnSc4
Hjong-čun	Hjong-čun	k1gMnSc1
<g/>
,	,	kIx,
Kim	Kim	k1gMnSc1
Čchol-min	Čchol-min	k2eAgMnSc1d1
<g/>
,	,	kIx,
I	i	k9
Sung-hunPolsko	Sung-hunPolsko	k1gNnSc1
PolskoZbigniew	PolskoZbigniew	k1gMnSc2
Bródka	Bródek	k1gMnSc2
<g/>
,	,	kIx,
Konrad	Konrad	k1gInSc4
Niedźwiedzki	Niedźwiedzk	k1gFnSc2
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Szymański	Szymańsk	k1gFnSc2
</s>
<s>
2014	#num#	k4
<g/>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
se	se	k3xPyFc4
nekonalo	konat	k5eNaImAgNnS
kvůli	kvůli	k7c3
pořádání	pořádání	k1gNnSc3
zimních	zimní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
</s>
<s>
2015	#num#	k4
<g/>
HeerenveenNizozemsko	HeerenveenNizozemsko	k1gNnSc1
NizozemskoSven	NizozemskoSvna	k1gFnPc2
Kramer	Kramra	k1gFnPc2
<g/>
,	,	kIx,
Koen	Koen	k1gMnSc1
Verweij	Verweij	k1gMnSc1
<g/>
,	,	kIx,
Douwe	Douwe	k1gFnSc1
de	de	k?
VriesKanada	VriesKanada	k1gFnSc1
KanadaDenny	KanadaDenna	k1gFnSc2
Morrison	Morrison	k1gNnSc1
<g/>
,	,	kIx,
Ted-Jan	Ted-Jan	k1gInSc1
Bloemen	Bloemen	k1gInSc1
<g/>
,	,	kIx,
Jordan	Jordan	k1gMnSc1
BelchosJižní	BelchosJižní	k2eAgFnSc2d1
Korea	Korea	k1gFnSc1
Jižní	jižní	k2eAgFnSc2d1
KoreaI	KoreaI	k1gFnSc2
Sung-hun	Sung-hun	k1gMnSc1
<g/>
,	,	kIx,
Ko	Ko	k1gMnSc1
Pjong-uk	Pjong-uk	k1gMnSc1
<g/>
,	,	kIx,
Kim	Kim	k1gMnSc1
Čchol-min	Čchol-min	k1gInSc1
</s>
<s>
2016	#num#	k4
<g/>
KolomnaNizozemsko	KolomnaNizozemsko	k1gNnSc1
NizozemskoJan	NizozemskoJana	k1gFnPc2
Blokhuijsen	Blokhuijsna	k1gFnPc2
<g/>
,	,	kIx,
Douwe	Douwe	k1gFnPc2
de	de	k?
Vries	Vries	k1gInSc1
<g/>
,	,	kIx,
Arjan	Arjan	k1gInSc1
StroetingaNorsko	StroetingaNorsko	k1gNnSc1
NorskoHå	NorskoHå	k1gFnPc2
Bø	Bø	k1gNnSc1
<g/>
,	,	kIx,
Simen	Simen	k2eAgInSc1d1
Spieler	Spieler	k1gInSc1
Nilsen	Nilsen	k2eAgInSc1d1
<g/>
,	,	kIx,
Sverre	Sverr	k1gMnSc5
Lunde	Lund	k1gMnSc5
PedersenKanada	PedersenKanada	k1gFnSc1
KanadaJordan	KanadaJordan	k1gMnSc1
Belchos	Belchos	k1gMnSc1
<g/>
,	,	kIx,
Ted-Jan	Ted-Jan	k1gMnSc1
Bloemen	Bloemen	k1gInSc1
<g/>
,	,	kIx,
Benjamin	Benjamin	k1gMnSc1
Donnelly	Donnella	k1gFnSc2
</s>
<s>
2017	#num#	k4
<g/>
KangnungNizozemsko	KangnungNizozemsko	k1gNnSc1
NizozemskoJorrit	NizozemskoJorrita	k1gFnPc2
Bergsma	Bergsmum	k1gNnSc2
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Blokhuijsen	Blokhuijsen	k1gInSc1
<g/>
,	,	kIx,
Douwe	Douwe	k1gInSc1
de	de	k?
VriesNový	VriesNový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
Nový	nový	k2eAgInSc1d1
ZélandShane	ZélandShan	k1gMnSc5
Dobbin	Dobbin	k2eAgMnSc1d1
<g/>
,	,	kIx,
Reyon	Reyon	k1gMnSc1
Kay	Kay	k1gMnSc1
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
MichaelNorsko	MichaelNorska	k1gFnSc5
NorskoSindre	NorskoSindr	k1gInSc5
Henriksen	Henriksen	k2eAgInSc4d1
<g/>
,	,	kIx,
Simen	Simen	k2eAgInSc4d1
Spieler	Spieler	k1gInSc4
Nilsen	Nilsna	k1gFnPc2
<g/>
,	,	kIx,
Sverre	Sverr	k1gMnSc5
Lunde	Lund	k1gMnSc5
Pedersen	Pedersen	k1gInSc1
</s>
<s>
2018	#num#	k4
<g/>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
se	se	k3xPyFc4
nekonalo	konat	k5eNaImAgNnS
kvůli	kvůli	k7c3
pořádání	pořádání	k1gNnSc3
zimních	zimní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
</s>
<s>
2019	#num#	k4
<g/>
InzellNizozemsko	InzellNizozemsko	k1gNnSc1
NizozemskoSven	NizozemskoSvna	k1gFnPc2
Kramer	Kramra	k1gFnPc2
<g/>
,	,	kIx,
Douwe	Douwe	k1gFnPc2
de	de	k?
Vries	Vries	k1gMnSc1
<g/>
,	,	kIx,
Marcel	Marcel	k1gMnSc1
BoskerNorsko	BoskerNorsko	k1gNnSc1
NorskoHå	NorskoHå	k1gMnSc1
Bø	Bø	k1gNnSc1
<g/>
,	,	kIx,
Sverre	Sverr	k1gMnSc5
Lunde	Lund	k1gMnSc5
Pedersen	Pedersna	k1gFnPc2
<g/>
,	,	kIx,
Sindre	Sindr	k1gInSc5
HenriksenRusko	HenriksenRusko	k1gNnSc1
RuskoAlexandr	RuskoAlexandr	k1gInSc1
Rumjancev	Rumjancev	k1gFnSc1
<g/>
,	,	kIx,
Danila	danit	k5eAaImAgFnS
Semerikov	Semerikov	k1gInSc4
<g/>
,	,	kIx,
Sergej	Sergej	k1gMnSc1
Trofimov	Trofimov	k1gInSc1
</s>
<s>
2020	#num#	k4
<g/>
Salt	salto	k1gNnPc2
Lake	Lak	k1gFnSc2
CityNizozemsko	CityNizozemsko	k1gNnSc1
NizozemskoSven	NizozemskoSvna	k1gFnPc2
Kramer	Kramra	k1gFnPc2
<g/>
,	,	kIx,
Douwe	Douwe	k1gFnPc2
de	de	k?
Vries	Vries	k1gMnSc1
<g/>
,	,	kIx,
Marcel	Marcel	k1gMnSc1
BoskerJaponsko	BoskerJaponsko	k1gNnSc4
JaponskoSeitaró	JaponskoSeitaró	k1gMnSc2
Ičinohe	Ičinohe	k1gNnSc4
<g/>
,	,	kIx,
Riku	Rika	k1gFnSc4
Cučija	Cučij	k1gInSc2
<g/>
,	,	kIx,
Shane	Shan	k1gMnSc5
WilliamsonRusko	WilliamsonRuska	k1gMnSc5
RuskoSergej	RuskoSergej	k1gInSc1
Trofimov	Trofimov	k1gInSc1
<g/>
,	,	kIx,
Ruslan	Ruslan	k1gInSc1
Zacharov	Zacharov	k1gInSc1
<g/>
,	,	kIx,
Danila	danit	k5eAaImAgFnS
Semerikov	Semerikov	k1gInSc4
</s>
<s>
2021	#num#	k4
<g/>
Salt	salto	k1gNnPc2
Lake	Lake	k1gFnPc2
CityNizozemsko	CityNizozemsko	k1gNnSc1
NizozemskoPatrick	NizozemskoPatrick	k1gMnSc1
Roest	Roest	k1gMnSc1
<g/>
,	,	kIx,
Marcel	Marcel	k1gMnSc1
Bosker	Bosker	k1gMnSc1
<g/>
,	,	kIx,
Beau	Beaa	k1gFnSc4
SnellinkKanada	SnellinkKanada	k1gFnSc1
KanadaTed-Jan	KanadaTed-Jan	k1gMnSc1
Bloemen	Bloemen	k1gInSc1
<g/>
,	,	kIx,
Jordan	Jordan	k1gMnSc1
Belchos	Belchos	k1gMnSc1
<g/>
,	,	kIx,
Connor	Connor	k1gInSc4
HoweRuská	HoweRuský	k2eAgFnSc1d1
bruslařská	bruslařský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
Ruská	ruský	k2eAgFnSc1d1
bruslařská	bruslařský	k2eAgFnSc1d1
unieDanila	unieDanit	k5eAaImAgFnS,k5eAaPmAgFnS
Semerikov	Semerikov	k1gInSc4
<g/>
,	,	kIx,
Sergej	Sergej	k1gMnSc1
Trofimov	Trofimov	k1gInSc1
<g/>
,	,	kIx,
Ruslan	Ruslan	k1gInSc1
Zacharov	Zacharov	k1gInSc1
</s>
<s>
Medailové	medailový	k2eAgNnSc1d1
pořadí	pořadí	k1gNnSc1
závodníků	závodník	k1gMnPc2
</s>
<s>
Aktualizováno	aktualizován	k2eAgNnSc1d1
po	po	k7c6
MS	MS	kA
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Poznámka	poznámka	k1gFnSc1
<g/>
:	:	kIx,
V	v	k7c6
tabulce	tabulka	k1gFnSc6
jsou	být	k5eAaImIp3nP
zařazeni	zařazen	k2eAgMnPc1d1
pouze	pouze	k6eAd1
závodníci	závodník	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
získali	získat	k5eAaPmAgMnP
nejméně	málo	k6eAd3
tři	tři	k4xCgFnPc4
zlaté	zlatý	k2eAgFnPc4d1
medaile	medaile	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
PořadíJménoZlatoStříbroBronzCelkem	PořadíJménoZlatoStříbroBronzCelek	k1gInSc7
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sven	Sven	k1gMnSc1
Kramer	Kramer	k1gMnSc1
<g/>
213226	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Shani	Shaň	k1gMnPc7
Davis	Davis	k1gFnSc2
<g/>
84315	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bob	Bob	k1gMnSc1
de	de	k?
Jong	Jong	k1gMnSc1
<g/>
78520	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gianni	Giann	k1gMnPc1
Romme	Romm	k1gMnSc5
<g/>
72312	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Erben	Erben	k1gMnSc1
Wennemars	Wennemars	k1gInSc4
<g/>
62311	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jorrit	Jorrit	k1gInSc1
Bergsma	Bergsma	k1gNnSc1
<g/>
57012	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Carl	Carla	k1gFnPc2
Verheijen	Verheijna	k1gFnPc2
<g/>
55313	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hirojasu	Hirojas	k1gInSc2
Šimizu	Šimiz	k1gInSc2
<g/>
53210	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pavel	Pavel	k1gMnSc1
Kuližnikov	Kuližnikov	k1gInSc4
<g/>
5319	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Douwe	Douwe	k1gInSc1
de	de	k?
Vries	Vries	k1gInSc1
<g/>
5016	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
Å	Å	k5eAaPmIp3nS
Sø	Sø	k1gMnSc1
<g/>
46010	#num#	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kjeld	Kjeld	k1gMnSc1
Nuis	Nuis	k1gInSc4
<g/>
45312	#num#	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeremy	Jeremo	k1gNnPc7
Wotherspoon	Wotherspoona	k1gFnPc2
<g/>
43310	#num#	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jan	Jan	k1gMnSc1
Blokhuijsen	Blokhuijsen	k1gInSc4
<g/>
4015	#num#	k4
</s>
<s>
Kai	Kai	k?
Verbij	Verbij	k1gFnSc1
<g/>
4015	#num#	k4
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Denis	Denisa	k1gFnPc2
Juskov	Juskov	k1gInSc4
<g/>
3227	#num#	k4
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Thomas	Thomas	k1gMnSc1
Krol	Krol	k1gMnSc1
<g/>
3216	#num#	k4
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ids	Ids	k1gFnSc1
Postma	Postma	k1gFnSc1
<g/>
3104	#num#	k4
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koen	Koen	k1gInSc1
Verweij	Verweij	k1gFnSc2
<g/>
3025	#num#	k4
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Joey	Joea	k1gMnSc2
Mantia	Mantius	k1gMnSc2
<g/>
3014	#num#	k4
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marcel	Marcel	k1gMnSc1
Bosker	Bosker	k1gMnSc1
<g/>
3003	#num#	k4
</s>
<s>
Medailové	medailový	k2eAgNnSc1d1
pořadí	pořadí	k1gNnSc1
zemí	zem	k1gFnPc2
</s>
<s>
Aktualizováno	aktualizován	k2eAgNnSc1d1
po	po	k7c6
MS	MS	kA
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
PořadíZeměZlatoStříbroBronzCelkem	PořadíZeměZlatoStříbroBronzCelek	k1gInSc7
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
Nizozemsko	Nizozemsko	k1gNnSc4
Nizozemsko	Nizozemsko	k1gNnSc1
<g/>
685338159	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
USA	USA	kA
USA1461434	USA1461434	k1gMnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
Kanada	Kanada	k1gFnSc1
Kanada	Kanada	k1gFnSc1
<g/>
10161844	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
<g/>
Rusko	Rusko	k1gNnSc4
Rusko	Rusko	k1gNnSc1
<g/>
1091534	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
<g/>
Norsko	Norsko	k1gNnSc4
Norsko	Norsko	k1gNnSc1
<g/>
813930	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
<g/>
Japonsko	Japonsko	k1gNnSc4
Japonsko	Japonsko	k1gNnSc1
<g/>
78621	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
<g/>
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
<g/>
67417	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
<g/>
Švédsko	Švédsko	k1gNnSc4
Švédsko	Švédsko	k1gNnSc1
<g/>
2103	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
<g/>
Kazachstán	Kazachstán	k1gInSc1
Kazachstán	Kazachstán	k1gInSc1
<g/>
1001	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
<g/>
Itálie	Itálie	k1gFnSc2
Itálie	Itálie	k1gFnSc2
<g/>
0	#num#	k4
<g/>
617	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
<g/>
Ruská	ruský	k2eAgFnSc1d1
bruslařská	bruslařský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
Ruská	ruský	k2eAgFnSc1d1
bruslařská	bruslařský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
<g/>
0	#num#	k4
<g/>
235	#num#	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
<g/>
Belgie	Belgie	k1gFnSc1
Belgie	Belgie	k1gFnSc1
<g/>
0	#num#	k4
<g/>
224	#num#	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
<g/>
Německo	Německo	k1gNnSc4
Německo	Německo	k1gNnSc1
<g/>
0	#num#	k4
<g/>
1910	#num#	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
<g/>
Francie	Francie	k1gFnSc1
Francie	Francie	k1gFnSc2
<g/>
0	#num#	k4
<g/>
123	#num#	k4
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
Čína	Čína	k1gFnSc1
Čína	Čína	k1gFnSc1
<g/>
0	#num#	k4
<g/>
112	#num#	k4
</s>
<s>
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
<g/>
0	#num#	k4
<g/>
112	#num#	k4
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
<g/>
Finsko	Finsko	k1gNnSc4
Finsko	Finsko	k1gNnSc1
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
22	#num#	k4
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
<g/>
Polsko	Polsko	k1gNnSc4
Polsko	Polsko	k1gNnSc1
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
11	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Přehledy	přehled	k1gInPc1
medailistů	medailista	k1gMnPc2
z	z	k7c2
mistrovství	mistrovství	k1gNnSc2
světa	svět	k1gInSc2
na	na	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
tratích	trať	k1gFnPc6
<g/>
:	:	kIx,
500	#num#	k4
m	m	kA
<g/>
,	,	kIx,
1000	#num#	k4
m	m	kA
<g/>
,	,	kIx,
1500	#num#	k4
m	m	kA
<g/>
,	,	kIx,
5000	#num#	k4
m	m	kA
<g/>
,	,	kIx,
10	#num#	k4
000	#num#	k4
m	m	kA
<g/>
,	,	kIx,
závod	závod	k1gInSc1
s	s	k7c7
hromadným	hromadný	k2eAgInSc7d1
startem	start	k1gInSc7
<g/>
,	,	kIx,
stíhací	stíhací	k2eAgInSc1d1
závod	závod	k1gInSc1
družstev	družstvo	k1gNnPc2
<g/>
,	,	kIx,
schaatsstatistieken	schaatsstatistieken	k1gInSc1
<g/>
.	.	kIx.
<g/>
nl	nl	k?
(	(	kIx(
<g/>
nizozemsky	nizozemsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
rychlobruslení	rychlobruslení	k1gNnSc6
na	na	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
tratích	trať	k1gFnPc6
(	(	kIx(
<g/>
muži	muž	k1gMnSc6
<g/>
/	/	kIx~
<g/>
ženy	žena	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
1996	#num#	k4
Hamar	Hamar	k1gInSc1
•	•	k?
1997	#num#	k4
Varšava	Varšava	k1gFnSc1
•	•	k?
1998	#num#	k4
Calgary	Calgary	k1gNnSc6
•	•	k?
1999	#num#	k4
Heerenveen	Heerenveen	k1gInSc1
•	•	k?
2000	#num#	k4
Nagano	Nagano	k1gNnSc4
•	•	k?
2001	#num#	k4
Salt	salto	k1gNnPc2
Lake	Lak	k1gFnSc2
City	city	k1gNnSc1
•	•	k?
2002	#num#	k4
•	•	k?
2003	#num#	k4
Berlín	Berlín	k1gInSc1
•	•	k?
2004	#num#	k4
Soul	Soul	k1gInSc1
•	•	k?
2005	#num#	k4
Inzell	Inzell	k1gInSc1
•	•	k?
2006	#num#	k4
•	•	k?
2007	#num#	k4
Salt	salto	k1gNnPc2
Lake	Lak	k1gFnSc2
City	city	k1gNnSc1
•	•	k?
2008	#num#	k4
Nagano	Nagano	k1gNnSc4
•	•	k?
2009	#num#	k4
Richmond	Richmond	k1gInSc1
•	•	k?
2010	#num#	k4
•	•	k?
2011	#num#	k4
Inzell	Inzell	k1gInSc1
•	•	k?
2012	#num#	k4
Heerenveen	Heerenveen	k1gInSc1
•	•	k?
2013	#num#	k4
Soči	Soči	k1gNnSc4
•	•	k?
2014	#num#	k4
•	•	k?
2015	#num#	k4
Heerenveen	Heerenveen	k1gInSc1
•	•	k?
2016	#num#	k4
Kolomna	Kolomna	k1gFnSc1
•	•	k?
2017	#num#	k4
Kangnung	Kangnung	k1gInSc1
•	•	k?
2018	#num#	k4
•	•	k?
2019	#num#	k4
Inzell	Inzell	k1gInSc1
•	•	k?
2020	#num#	k4
Salt	salto	k1gNnPc2
Lake	Lak	k1gFnSc2
City	city	k1gNnSc1
•	•	k?
2021	#num#	k4
Heerenveen	Heerenvena	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sport	sport	k1gInSc1
</s>
