<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
rychlobruslení	rychlobruslení	k1gNnSc6
na	na	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
tratích	trať	k1gFnPc6
mužů	muž	k1gMnPc2
je	být	k5eAaImIp3nS
pořádáno	pořádat	k5eAaImNgNnS
Mezinárodní	mezinárodní	k2eAgFnSc7d1
bruslařskou	bruslařský	k2eAgFnSc7d1
unií	unie	k1gFnSc7
každoročně	každoročně	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1996	#num#	k4
společně	společně	k6eAd1
s	s	k7c7
ženským	ženský	k2eAgInSc7d1
šampionátem	šampionát	k1gInSc7
<g/>
.	.	kIx.
</s>