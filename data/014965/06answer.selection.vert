<s desamb="1">
Muži	muž	k1gMnPc1
závodí	závodit	k5eAaImIp3nP
na	na	k7c6
tratích	trať	k1gFnPc6
500	#num#	k4
<g/>
,	,	kIx,
1000	#num#	k4
<g/>
,	,	kIx,
1500	#num#	k4
<g/>
,	,	kIx,
5000	#num#	k4
a	a	k8xC
10	#num#	k4
000	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
také	také	k6eAd1
ve	v	k7c6
stíhacím	stíhací	k2eAgInSc6d1
závodě	závod	k1gInSc6
družstev	družstvo	k1gNnPc2
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
2015	#num#	k4
v	v	k7c6
závodě	závod	k1gInSc6
s	s	k7c7
hromadným	hromadný	k2eAgInSc7d1
startem	start	k1gInSc7
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
2019	#num#	k4
i	i	k8xC
v	v	k7c6
týmovém	týmový	k2eAgInSc6d1
sprintu	sprint	k1gInSc6
<g/>
.	.	kIx.
</s>