<s>
Jean-Paul	Jean-Paul	k1gInSc1	Jean-Paul
Sartre	Sartr	k1gInSc5	Sartr
[	[	kIx(	[
<g/>
ʒ	ʒ	k?	ʒ
<g/>
̃	̃	k?	̃
pɔ	pɔ	k?	pɔ
saʁ	saʁ	k?	saʁ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
[	[	kIx(	[
<g/>
žán	žán	k?	žán
pol	pola	k1gFnPc2	pola
sártr	sártr	k1gMnSc1	sártr
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1905	[number]	k4	1905
-	-	kIx~	-
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
filozof	filozof	k1gMnSc1	filozof
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
literární	literární	k2eAgMnSc1d1	literární
kritik	kritik	k1gMnSc1	kritik
a	a	k8xC	a
politický	politický	k2eAgMnSc1d1	politický
aktivista	aktivista	k1gMnSc1	aktivista
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgMnPc2d1	hlavní
představitelů	představitel	k1gMnPc2	představitel
existencialismu	existencialismus	k1gInSc2	existencialismus
a	a	k8xC	a
marxismu	marxismus	k1gInSc2	marxismus
ve	v	k7c6	v
francouzské	francouzský	k2eAgFnSc6d1	francouzská
filosofii	filosofie	k1gFnSc6	filosofie
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
postavy	postava	k1gFnPc4	postava
poválečné	poválečný	k2eAgFnSc2d1	poválečná
kultury	kultura	k1gFnSc2	kultura
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Sartre	Sartr	k1gMnSc5	Sartr
svým	svůj	k3xOyFgNnSc7	svůj
dílem	dílo	k1gNnSc7	dílo
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
nejen	nejen	k6eAd1	nejen
filozofii	filozofie	k1gFnSc4	filozofie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
sociologii	sociologie	k1gFnSc4	sociologie
<g/>
,	,	kIx,	,
literární	literární	k2eAgFnSc4d1	literární
kritiku	kritika	k1gFnSc4	kritika
či	či	k8xC	či
postkoloniální	postkoloniální	k2eAgNnPc4d1	postkoloniální
studia	studio	k1gNnPc4	studio
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
bývá	bývat	k5eAaImIp3nS	bývat
také	také	k9	také
zmiňován	zmiňován	k2eAgInSc1d1	zmiňován
pro	pro	k7c4	pro
nekonvenční	konvenční	k2eNgInSc4d1	nekonvenční
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
celoživotní	celoživotní	k2eAgNnSc1d1	celoživotní
<g/>
,	,	kIx,	,
vztah	vztah	k1gInSc1	vztah
se	s	k7c7	s
spisovatelkou	spisovatelka	k1gFnSc7	spisovatelka
a	a	k8xC	a
feministickou	feministický	k2eAgFnSc7d1	feministická
teoretičkou	teoretička	k1gFnSc7	teoretička
Simone	Simon	k1gMnSc5	Simon
de	de	k?	de
Beauvoir	Beauvoira	k1gFnPc2	Beauvoira
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
udělena	udělen	k2eAgFnSc1d1	udělena
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
s	s	k7c7	s
odůvodněním	odůvodnění	k1gNnSc7	odůvodnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
nikdy	nikdy	k6eAd1	nikdy
nepřijal	přijmout	k5eNaPmAgMnS	přijmout
žádné	žádný	k3yNgNnSc4	žádný
oficiální	oficiální	k2eAgNnSc4d1	oficiální
vyznamenání	vyznamenání	k1gNnSc4	vyznamenání
<g/>
,	,	kIx,	,
a	a	k8xC	a
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
spisovatel	spisovatel	k1gMnSc1	spisovatel
nesmí	smět	k5eNaImIp3nS	smět
dopustit	dopustit	k5eAaPmF	dopustit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
stala	stát	k5eAaPmAgFnS	stát
instituce	instituce	k1gFnSc1	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Jean-Paul	Jean-Paul	k1gInSc1	Jean-Paul
Sartre	Sartr	k1gInSc5	Sartr
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyrůstal	vyrůstat	k5eAaImAgInS	vyrůstat
v	v	k7c6	v
měšťanském	měšťanský	k2eAgNnSc6d1	měšťanské
prostředí	prostředí	k1gNnSc6	prostředí
intelektuálů	intelektuál	k1gMnPc2	intelektuál
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
když	když	k8xS	když
byly	být	k5eAaImAgFnP	být
Jean-Paulovi	Jean-Paulův	k2eAgMnPc1d1	Jean-Paulův
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
pak	pak	k6eAd1	pak
v	v	k7c6	v
domě	dům	k1gInSc6	dům
svého	svůj	k3xOyFgMnSc2	svůj
dědečka	dědeček	k1gMnSc2	dědeček
z	z	k7c2	z
matčiny	matčin	k2eAgFnSc2d1	matčina
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
Carla	Carl	k1gMnSc2	Carl
Schweitzera	Schweitzer	k1gMnSc2	Schweitzer
<g/>
,	,	kIx,	,
profesora	profesor	k1gMnSc2	profesor
němčiny	němčina	k1gFnSc2	němčina
na	na	k7c6	na
Sorbonně	Sorbonna	k1gFnSc6	Sorbonna
a	a	k8xC	a
strýce	strýc	k1gMnSc2	strýc
známého	známý	k1gMnSc2	známý
misionáře	misionář	k1gMnSc2	misionář
a	a	k8xC	a
lékaře	lékař	k1gMnSc2	lékař
Alberta	Albert	k1gMnSc2	Albert
Schweitzera	Schweitzer	k1gMnSc2	Schweitzer
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
útlém	útlý	k2eAgNnSc6d1	útlé
dětství	dětství	k1gNnSc6	dětství
se	se	k3xPyFc4	se
ponořil	ponořit	k5eAaPmAgInS	ponořit
do	do	k7c2	do
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
jako	jako	k8xC	jako
malého	malý	k2eAgMnSc4d1	malý
chlapce	chlapec	k1gMnSc4	chlapec
s	s	k7c7	s
šilhavýma	šilhavý	k2eAgNnPc7d1	šilhavé
očima	oko	k1gNnPc7	oko
stala	stát	k5eAaPmAgFnS	stát
útěchou	útěcha	k1gFnSc7	útěcha
před	před	k7c7	před
nepřátelským	přátelský	k2eNgInSc7d1	nepřátelský
světem	svět	k1gInSc7	svět
<g/>
.	.	kIx.	.
</s>
<s>
Navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
lyceum	lyceum	k1gNnSc4	lyceum
Jindřicha	Jindřich	k1gMnSc2	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Paulem	Paul	k1gMnSc7	Paul
Nizanem	Nizan	k1gMnSc7	Nizan
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
lyceum	lyceum	k1gNnSc1	lyceum
v	v	k7c6	v
La	la	k1gNnSc6	la
Rochelle	Rochelle	k1gFnSc2	Rochelle
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1922	[number]	k4	1922
<g/>
-	-	kIx~	-
<g/>
1924	[number]	k4	1924
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
přípravné	přípravný	k2eAgFnSc6d1	přípravná
třídě	třída	k1gFnSc6	třída
(	(	kIx(	(
<g/>
classe	class	k1gMnSc5	class
préparatoire	préparatoir	k1gMnSc5	préparatoir
<g/>
)	)	kIx)	)
na	na	k7c6	na
lyceu	lyceum	k1gNnSc6	lyceum
Louis-le-Grand	Louise-Granda	k1gFnPc2	Louis-le-Granda
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
na	na	k7c6	na
prestižní	prestižní	k2eAgFnSc6d1	prestižní
École	Écola	k1gFnSc6	Écola
normale	normal	k1gMnSc5	normal
supérieure	supérieur	k1gMnSc5	supérieur
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Raymondem	Raymond	k1gMnSc7	Raymond
Aronem	Aron	k1gMnSc7	Aron
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
také	také	k9	také
poznal	poznat	k5eAaPmAgMnS	poznat
Simone	Simon	k1gMnSc5	Simon
de	de	k?	de
Beauvoir	Beauvoir	k1gInSc4	Beauvoir
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
ho	on	k3xPp3gMnSc4	on
do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
pojil	pojit	k5eAaImAgMnS	pojit
otevřeně	otevřeně	k6eAd1	otevřeně
volný	volný	k2eAgMnSc1d1	volný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
trvalý	trvalý	k2eAgInSc4d1	trvalý
partnerský	partnerský	k2eAgInSc4d1	partnerský
vztah	vztah	k1gInSc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
poznal	poznat	k5eAaPmAgMnS	poznat
i	i	k8xC	i
mnohé	mnohý	k2eAgMnPc4d1	mnohý
další	další	k2eAgMnPc4d1	další
budoucí	budoucí	k2eAgMnPc4d1	budoucí
významné	významný	k2eAgMnPc4d1	významný
autory	autor	k1gMnPc4	autor
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgMnPc4	jenž
patřili	patřit	k5eAaImAgMnP	patřit
například	například	k6eAd1	například
Maurice	Maurika	k1gFnSc3	Maurika
Merleau-Ponty	Merleau-Ponta	k1gFnSc2	Merleau-Ponta
<g/>
,	,	kIx,	,
Simone	Simon	k1gMnSc5	Simon
Weil	Weil	k1gMnSc1	Weil
<g/>
,	,	kIx,	,
Emmanuel	Emmanuel	k1gMnSc1	Emmanuel
Mounier	Mounier	k1gMnSc1	Mounier
či	či	k8xC	či
Claude	Claud	k1gInSc5	Claud
Lévi-Strauss	Lévi-Strauss	k1gInSc4	Lévi-Strauss
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
ukončil	ukončit	k5eAaPmAgInS	ukončit
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
jako	jako	k8xC	jako
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
filosofické	filosofický	k2eAgFnSc2d1	filosofická
třídy	třída	k1gFnSc2	třída
a	a	k8xC	a
poté	poté	k6eAd1	poté
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
učitel	učitel	k1gMnSc1	učitel
na	na	k7c6	na
lyceích	lyceum	k1gNnPc6	lyceum
v	v	k7c6	v
Le	Le	k1gMnSc5	Le
Havre	Havr	k1gMnSc5	Havr
<g/>
,	,	kIx,	,
Laonu	Laono	k1gNnSc6	Laono
a	a	k8xC	a
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1933	[number]	k4	1933
<g/>
-	-	kIx~	-
<g/>
1934	[number]	k4	1934
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
pobyt	pobyt	k1gInSc1	pobyt
na	na	k7c6	na
Francouzském	francouzský	k2eAgInSc6d1	francouzský
institutu	institut	k1gInSc6	institut
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
fenomenologickou	fenomenologický	k2eAgFnSc7d1	fenomenologická
filozofií	filozofie	k1gFnSc7	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
Ovlivnili	ovlivnit	k5eAaPmAgMnP	ovlivnit
ho	on	k3xPp3gNnSc4	on
především	především	k6eAd1	především
Hegel	Hegel	k1gMnSc1	Hegel
<g/>
,	,	kIx,	,
Soren	Soren	k2eAgMnSc1d1	Soren
Kierkegaard	Kierkegaard	k1gMnSc1	Kierkegaard
<g/>
,	,	kIx,	,
Karl	Karl	k1gMnSc1	Karl
Marx	Marx	k1gMnSc1	Marx
<g/>
,	,	kIx,	,
Friedrich	Friedrich	k1gMnSc1	Friedrich
Nietzsche	Nietzsch	k1gFnSc2	Nietzsch
<g/>
,	,	kIx,	,
Edmund	Edmund	k1gMnSc1	Edmund
Husserl	Husserl	k1gMnSc1	Husserl
a	a	k8xC	a
René	René	k1gMnSc1	René
Descartes	Descartes	k1gMnSc1	Descartes
<g/>
,	,	kIx,	,
dalším	další	k2eAgInSc7d1	další
impulsem	impuls	k1gInSc7	impuls
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
tvorbě	tvorba	k1gFnSc3	tvorba
byli	být	k5eAaImAgMnP	být
francouzští	francouzský	k2eAgMnPc1d1	francouzský
moralisté	moralista	k1gMnPc1	moralista
(	(	kIx(	(
<g/>
filozofický	filozofický	k2eAgInSc4d1	filozofický
směr	směr	k1gInSc4	směr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
jako	jako	k8xS	jako
francouzský	francouzský	k2eAgMnSc1d1	francouzský
voják	voják	k1gMnSc1	voják
v	v	k7c6	v
Alsasku	Alsasko	k1gNnSc6	Alsasko
zajat	zajmout	k5eAaPmNgMnS	zajmout
a	a	k8xC	a
internován	internovat	k5eAaBmNgMnS	internovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
propuštění	propuštění	k1gNnSc6	propuštění
se	se	k3xPyFc4	se
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začal	začít	k5eAaPmAgMnS	začít
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
hnutím	hnutí	k1gNnSc7	hnutí
odporu	odpor	k1gInSc2	odpor
Résistance	Résistance	k1gFnSc2	Résistance
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
spisovatel	spisovatel	k1gMnSc1	spisovatel
na	na	k7c6	na
volné	volný	k2eAgFnSc6d1	volná
noze	noha	k1gFnSc6	noha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
zařadil	zařadit	k5eAaPmAgMnS	zařadit
papež	papež	k1gMnSc1	papež
Pius	Pius	k1gMnSc1	Pius
XII	XII	kA	XII
<g/>
.	.	kIx.	.
</s>
<s>
Sartrovy	Sartrův	k2eAgInPc4d1	Sartrův
spisy	spis	k1gInPc4	spis
na	na	k7c4	na
Index	index	k1gInSc4	index
zakázaných	zakázaný	k2eAgFnPc2d1	zakázaná
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc3	jeho
politické	politický	k2eAgInPc1d1	politický
postoje	postoj	k1gInPc1	postoj
přibližovaly	přibližovat	k5eAaImAgInP	přibližovat
komunistické	komunistický	k2eAgFnSc6d1	komunistická
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
jejím	její	k3xOp3gInSc7	její
členem	člen	k1gInSc7	člen
se	se	k3xPyFc4	se
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
nestal	stát	k5eNaPmAgInS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc4	jeho
vztah	vztah	k1gInSc4	vztah
ke	k	k7c3	k
KS	ks	kA	ks
poněkud	poněkud	k6eAd1	poněkud
ochladl	ochladnout	k5eAaPmAgMnS	ochladnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
po	po	k7c6	po
sovětské	sovětský	k2eAgFnSc6d1	sovětská
intervenci	intervence	k1gFnSc6	intervence
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
sovětským	sovětský	k2eAgInSc7d1	sovětský
komunismem	komunismus	k1gInSc7	komunismus
vždy	vždy	k6eAd1	vždy
upřednostňoval	upřednostňovat	k5eAaImAgMnS	upřednostňovat
spíše	spíše	k9	spíše
různé	různý	k2eAgFnPc4d1	různá
alternativní	alternativní	k2eAgFnPc4d1	alternativní
verze	verze	k1gFnPc4	verze
komunismu	komunismus	k1gInSc2	komunismus
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Titův	Titův	k2eAgInSc1d1	Titův
v	v	k7c4	v
Jugoslávii	Jugoslávie	k1gFnSc4	Jugoslávie
<g/>
,	,	kIx,	,
Castrův	Castrův	k2eAgInSc4d1	Castrův
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
a	a	k8xC	a
Maův	Maův	k1gInSc4	Maův
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
komunismus	komunismus	k1gInSc4	komunismus
zleva	zleva	k6eAd1	zleva
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
protektorem	protektor	k1gInSc7	protektor
anarchistických	anarchistický	k2eAgInPc2d1	anarchistický
<g/>
,	,	kIx,	,
ultralevicových	ultralevicový	k2eAgInPc2d1	ultralevicový
proudů	proud	k1gInPc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Hodně	hodně	k6eAd1	hodně
cestoval	cestovat	k5eAaImAgMnS	cestovat
<g/>
,	,	kIx,	,
navštívil	navštívit	k5eAaPmAgMnS	navštívit
mj.	mj.	kA	mj.
USA	USA	kA	USA
<g/>
,	,	kIx,	,
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
Čínu	Čína	k1gFnSc4	Čína
<g/>
,	,	kIx,	,
Egypt	Egypt	k1gInSc4	Egypt
a	a	k8xC	a
Kubu	Kuba	k1gFnSc4	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Přednášel	přednášet	k5eAaImAgMnS	přednášet
také	také	k9	také
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
druhé	druhý	k4xOgFnSc6	druhý
návštěvě	návštěva	k1gFnSc6	návštěva
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Čechoslováci	Čechoslovák	k1gMnPc1	Čechoslovák
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
řešit	řešit	k5eAaImF	řešit
svůj	svůj	k3xOyFgInSc4	svůj
odpor	odpor	k1gInSc4	odpor
střetnutím	střetnutí	k1gNnSc7	střetnutí
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
nadále	nadále	k6eAd1	nadále
bojovat	bojovat	k5eAaImF	bojovat
za	za	k7c4	za
skutečně	skutečně	k6eAd1	skutečně
demokratický	demokratický	k2eAgInSc4d1	demokratický
socialismus	socialismus	k1gInSc4	socialismus
i	i	k9	i
přes	přes	k7c4	přes
srpnovou	srpnový	k2eAgFnSc4d1	srpnová
přestávku	přestávka	k1gFnSc4	přestávka
Cesta	cesta	k1gFnSc1	cesta
nastoupená	nastoupený	k2eAgFnSc1d1	nastoupená
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
je	být	k5eAaImIp3nS	být
přísně	přísně	k6eAd1	přísně
marxistická	marxistický	k2eAgFnSc1d1	marxistická
a	a	k8xC	a
liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
od	od	k7c2	od
ostatních	ostatní	k2eAgNnPc2d1	ostatní
liberálních	liberální	k2eAgNnPc2d1	liberální
hnutí	hnutí	k1gNnPc2	hnutí
nebo	nebo	k8xC	nebo
buržoazního	buržoazní	k2eAgInSc2d1	buržoazní
individualismu	individualismus	k1gInSc2	individualismus
<g/>
.	.	kIx.	.
</s>
<s>
Představuje	představovat	k5eAaImIp3nS	představovat
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
vzorů	vzor	k1gInPc2	vzor
nejvyspělejší	vyspělý	k2eAgFnSc2d3	nejvyspělejší
formule	formule	k1gFnSc2	formule
socialismu	socialismus	k1gInSc2	socialismus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
odhaluje	odhalovat	k5eAaImIp3nS	odhalovat
svou	svůj	k3xOyFgFnSc4	svůj
demokratickou	demokratický	k2eAgFnSc4d1	demokratická
podstatu	podstata	k1gFnSc4	podstata
<g/>
...	...	k?	...
Dělníci	dělník	k1gMnPc1	dělník
představují	představovat	k5eAaImIp3nP	představovat
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
baštu	bašta	k1gFnSc4	bašta
boje	boj	k1gInSc2	boj
za	za	k7c4	za
socialismus	socialismus	k1gInSc4	socialismus
a	a	k8xC	a
svrchovanost	svrchovanost	k1gFnSc4	svrchovanost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
politických	politický	k2eAgNnPc6d1	politické
vystoupeních	vystoupení	k1gNnPc6	vystoupení
se	se	k3xPyFc4	se
angažoval	angažovat	k5eAaBmAgMnS	angažovat
prakticky	prakticky	k6eAd1	prakticky
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
tehdejších	tehdejší	k2eAgInPc6d1	tehdejší
problémech	problém	k1gInPc6	problém
(	(	kIx(	(
<g/>
např.	např.	kA	např.
nezávislost	nezávislost	k1gFnSc4	nezávislost
Alžírska	Alžírsko	k1gNnSc2	Alžírsko
<g/>
,	,	kIx,	,
emancipace	emancipace	k1gFnSc2	emancipace
bývalých	bývalý	k2eAgFnPc2d1	bývalá
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
,	,	kIx,	,
válka	válka	k1gFnSc1	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
též	též	k9	též
ve	v	k7c6	v
studentských	studentská	k1gFnPc6	studentská
hnutí	hnutí	k1gNnSc2	hnutí
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
neoženil	oženit	k5eNaPmAgMnS	oženit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
celoživotní	celoživotní	k2eAgFnSc7d1	celoživotní
družkou	družka	k1gFnSc7	družka
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgNnP	být
Simone	Simon	k1gMnSc5	Simon
de	de	k?	de
Beauvoir	Beauvoira	k1gFnPc2	Beauvoira
<g/>
.	.	kIx.	.
</s>
<s>
Jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
milenek	milenka	k1gFnPc2	milenka
<g/>
,	,	kIx,	,
Arlette	Arlett	k1gInSc5	Arlett
El	Ela	k1gFnPc2	Ela
Kaim	Kaim	k1gMnSc1	Kaim
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
Egypťanku	Egypťanka	k1gFnSc4	Egypťanka
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
adoptoval	adoptovat	k5eAaPmAgMnS	adoptovat
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
jako	jako	k8xC	jako
dceru	dcera	k1gFnSc4	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
parodované	parodovaný	k2eAgNnSc1d1	parodované
alter-ego	altergo	k1gNnSc1	alter-ego
Jean-Sol	Jean-Sola	k1gFnPc2	Jean-Sola
Partre	Partr	k1gInSc5	Partr
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
románu	román	k1gInSc6	román
Borise	Boris	k1gMnSc2	Boris
Viana	Vian	k1gInSc2	Vian
Pěna	pěna	k1gFnSc1	pěna
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
samém	samý	k3xTgInSc6	samý
závěru	závěr	k1gInSc6	závěr
života	život	k1gInSc2	život
relativizoval	relativizovat	k5eAaImAgMnS	relativizovat
některé	některý	k3yIgInPc4	některý
své	svůj	k3xOyFgInPc4	svůj
dřívější	dřívější	k2eAgInPc4d1	dřívější
postoje	postoj	k1gInPc4	postoj
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
otázkách	otázka	k1gFnPc6	otázka
humanismu	humanismus	k1gInSc2	humanismus
a	a	k8xC	a
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Přiznal	přiznat	k5eAaPmAgMnS	přiznat
svou	svůj	k3xOyFgFnSc4	svůj
fascinaci	fascinace	k1gFnSc4	fascinace
judaismem	judaismus	k1gInSc7	judaismus
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc7	jeho
mesianistickým	mesianistický	k2eAgInSc7d1	mesianistický
<g/>
,	,	kIx,	,
utopistickým	utopistický	k2eAgInSc7d1	utopistický
potenciálem	potenciál	k1gInSc7	potenciál
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc7	jeho
eschatologií	eschatologie	k1gFnSc7	eschatologie
a	a	k8xC	a
také	také	k9	také
jeho	jeho	k3xOp3gFnSc7	jeho
teologií	teologie	k1gFnSc7	teologie
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Bez	bez	k7c2	bez
Židů	Žid	k1gMnPc2	Žid
by	by	kYmCp3nS	by
filosofie	filosofie	k1gFnSc1	filosofie
dějin	dějiny	k1gFnPc2	dějiny
nebyla	být	k5eNaImAgFnS	být
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Espoir	Espoir	k1gMnSc1	Espoir
maintenant	maintenant	k1gMnSc1	maintenant
<g/>
,	,	kIx,	,
rozhovor	rozhovor	k1gInSc1	rozhovor
s	s	k7c7	s
Bennym	Bennym	k1gInSc4	Bennym
Lévym	Lévym	k1gInSc4	Lévym
<g/>
)	)	kIx)	)
Ústřední	ústřední	k2eAgFnSc7d1	ústřední
kategorií	kategorie	k1gFnSc7	kategorie
Sartrovy	Sartrův	k2eAgFnSc2d1	Sartrova
filosofie	filosofie	k1gFnSc2	filosofie
je	být	k5eAaImIp3nS	být
svoboda	svoboda	k1gFnSc1	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
díle	díl	k1gInSc6	díl
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
interpretovat	interpretovat	k5eAaBmF	interpretovat
ve	v	k7c6	v
fenomenologických	fenomenologický	k2eAgFnPc6d1	fenomenologická
a	a	k8xC	a
existencialistických	existencialistický	k2eAgFnPc6d1	existencialistická
souvislostech	souvislost	k1gFnPc6	souvislost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
pak	pak	k9	pak
posouvá	posouvat	k5eAaImIp3nS	posouvat
těžiště	těžiště	k1gNnSc4	těžiště
svého	svůj	k3xOyFgInSc2	svůj
výkladu	výklad	k1gInSc2	výklad
na	na	k7c4	na
svobodu	svoboda	k1gFnSc4	svoboda
v	v	k7c6	v
Dějinách	dějiny	k1gFnPc6	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Oporou	opora	k1gFnSc7	opora
mu	on	k3xPp3gInSc3	on
tu	tu	k6eAd1	tu
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
Hegel	Hegel	k1gMnSc1	Hegel
a	a	k8xC	a
Marx	Marx	k1gMnSc1	Marx
<g/>
.	.	kIx.	.
</s>
<s>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
imagination	imagination	k1gInSc1	imagination
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
La	la	k1gNnSc6	la
transcendance	transcendanec	k1gInSc2	transcendanec
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
égo	égo	k?	égo
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
Esquisse	Esquisse	k1gFnSc1	Esquisse
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
une	une	k?	une
théorie	théorie	k1gFnSc2	théorie
des	des	k1gNnSc2	des
émotions	émotionsa	k1gFnPc2	émotionsa
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
imaginaire	imaginair	k1gInSc5	imaginair
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
Bytí	bytí	k1gNnSc1	bytí
a	a	k8xC	a
nicota	nicota	k1gFnSc1	nicota
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
Existencialismus	existencialismus	k1gInSc1	existencialismus
je	být	k5eAaImIp3nS	být
humanismus	humanismus	k1gInSc4	humanismus
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
Cesty	cesta	k1gFnSc2	cesta
k	k	k7c3	k
svobodě	svoboda	k1gFnSc3	svoboda
<g/>
,	,	kIx,	,
I.	I.	kA	I.
Věk	věk	k1gInSc4	věk
rozumu	rozum	k1gInSc2	rozum
(	(	kIx(	(
<g/>
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
ELK	ELK	kA	ELK
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
Cesty	cesta	k1gFnSc2	cesta
k	k	k7c3	k
svobodě	svoboda	k1gFnSc3	svoboda
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Odklad	odklad	k1gInSc1	odklad
(	(	kIx(	(
<g/>
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
ELK	ELK	kA	ELK
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
Kritika	kritika	k1gFnSc1	kritika
dialektického	dialektický	k2eAgInSc2d1	dialektický
rozumu	rozum	k1gInSc2	rozum
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
Cahiers	Cahiers	k1gInSc1	Cahiers
pour	pour	k1gInSc1	pour
une	une	k?	une
morale	morale	k6eAd1	morale
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
Vérité	Véritý	k2eAgFnSc2d1	Véritý
et	et	k?	et
existence	existence	k1gFnSc2	existence
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Nevolnost	nevolnost	k1gFnSc1	nevolnost
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
syntézu	syntéza	k1gFnSc4	syntéza
eseje	esej	k1gInSc2	esej
a	a	k8xC	a
románu	román	k1gInSc2	román
s	s	k7c7	s
filosofickým	filosofický	k2eAgNnSc7d1	filosofické
zaměřením	zaměření	k1gNnSc7	zaměření
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
psána	psát	k5eAaImNgFnS	psát
jako	jako	k8xS	jako
fiktivní	fiktivní	k2eAgInSc4d1	fiktivní
deník	deník	k1gInSc4	deník
historika	historik	k1gMnSc2	historik
Antoina	Antoin	k1gMnSc2	Antoin
Roquentina	Roquentin	k1gMnSc2	Roquentin
<g/>
,	,	kIx,	,
třicátníka	třicátník	k1gMnSc2	třicátník
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
po	po	k7c6	po
četných	četný	k2eAgFnPc6d1	četná
cestách	cesta	k1gFnPc6	cesta
po	po	k7c6	po
světě	svět	k1gInSc6	svět
načas	načas	k6eAd1	načas
usadí	usadit	k5eAaPmIp3nP	usadit
v	v	k7c6	v
provinčním	provinční	k2eAgInSc6d1	provinční
přímořském	přímořský	k2eAgInSc6d1	přímořský
Bouville	Bouvill	k1gInSc6	Bouvill
(	(	kIx(	(
<g/>
smyšlené	smyšlený	k2eAgNnSc1d1	smyšlené
město	město	k1gNnSc1	město
evokující	evokující	k2eAgNnSc1d1	evokující
představu	představa	k1gFnSc4	představa
Le	Le	k1gMnSc5	Le
Havre	Havr	k1gMnSc5	Havr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zde	zde	k6eAd1	zde
dopsal	dopsat	k5eAaPmAgMnS	dopsat
monografii	monografie	k1gFnSc4	monografie
o	o	k7c6	o
markýzi	markýz	k1gMnSc6	markýz
de	de	k?	de
Rollebon	Rollebon	k1gNnSc4	Rollebon
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Postava	postava	k1gFnSc1	postava
Roquentina	Roquentina	k1gFnSc1	Roquentina
je	být	k5eAaImIp3nS	být
osamělá	osamělý	k2eAgFnSc1d1	osamělá
<g/>
,	,	kIx,	,
představuje	představovat	k5eAaImIp3nS	představovat
sartrovskou	sartrovský	k2eAgFnSc4d1	sartrovský
variaci	variace	k1gFnSc4	variace
na	na	k7c4	na
prototyp	prototyp	k1gInSc4	prototyp
"	"	kIx"	"
<g/>
zbytečného	zbytečný	k2eAgMnSc2d1	zbytečný
člověka	člověk	k1gMnSc2	člověk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
frekventovaný	frekventovaný	k2eAgInSc1d1	frekventovaný
v	v	k7c6	v
ruských	ruský	k2eAgInPc6d1	ruský
klasických	klasický	k2eAgInPc6d1	klasický
románech	román	k1gInPc6	román
<g/>
.	.	kIx.	.
</s>
<s>
Roquentin	Roquentin	k1gInSc1	Roquentin
<g/>
,	,	kIx,	,
finančně	finančně	k6eAd1	finančně
zajištěný	zajištěný	k2eAgInSc4d1	zajištěný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dobrovolně	dobrovolně	k6eAd1	dobrovolně
izolovaný	izolovaný	k2eAgMnSc1d1	izolovaný
intelektuál	intelektuál	k1gMnSc1	intelektuál
<g/>
,	,	kIx,	,
lhostejný	lhostejný	k2eAgInSc4d1	lhostejný
k	k	k7c3	k
historickým	historický	k2eAgFnPc3d1	historická
a	a	k8xC	a
společenským	společenský	k2eAgFnPc3d1	společenská
souvislostem	souvislost	k1gFnPc3	souvislost
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
po	po	k7c6	po
krizovém	krizový	k2eAgInSc6d1	krizový
prožitku	prožitek	k1gInSc6	prožitek
"	"	kIx"	"
<g/>
Nevolnosti	nevolnost	k1gFnSc6	nevolnost
<g/>
"	"	kIx"	"
oddává	oddávat	k5eAaImIp3nS	oddávat
sebepozorování	sebepozorování	k1gNnSc4	sebepozorování
<g/>
,	,	kIx,	,
analýze	analýza	k1gFnSc6	analýza
i	i	k8xC	i
záznamům	záznam	k1gInPc3	záznam
každodenních	každodenní	k2eAgMnPc2d1	každodenní
prožitků	prožitek	k1gInPc2	prožitek
<g/>
.	.	kIx.	.
</s>
<s>
Ironicky	ironicky	k6eAd1	ironicky
komentuje	komentovat	k5eAaBmIp3nS	komentovat
v	v	k7c6	v
deníkových	deníkový	k2eAgInPc6d1	deníkový
zápiscích	zápisek	k1gInPc6	zápisek
život	život	k1gInSc1	život
v	v	k7c4	v
Bouville	Bouville	k1gInSc4	Bouville
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
plný	plný	k2eAgInSc1d1	plný
omezenosti	omezenost	k1gFnSc3	omezenost
<g/>
,	,	kIx,	,
přetvářky	přetvářka	k1gFnSc2	přetvářka
a	a	k8xC	a
stereotypu	stereotyp	k1gInSc2	stereotyp
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
podstatu	podstata	k1gFnSc4	podstata
Nevolnosti	nevolnost	k1gFnSc2	nevolnost
postupně	postupně	k6eAd1	postupně
objevuje	objevovat	k5eAaImIp3nS	objevovat
"	"	kIx"	"
<g/>
přemíru	přemíra	k1gFnSc4	přemíra
bytí	bytí	k1gNnSc2	bytí
bez	bez	k7c2	bez
důvodu	důvod	k1gInSc2	důvod
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
absurditu	absurdita	k1gFnSc4	absurdita
a	a	k8xC	a
nahodilost	nahodilost	k1gFnSc4	nahodilost
existence	existence	k1gFnSc2	existence
sebe	sebe	k3xPyFc4	sebe
i	i	k9	i
všeho	všecek	k3xTgNnSc2	všecek
ostatního	ostatní	k1gNnSc2	ostatní
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Každý	každý	k3xTgInSc1	každý
existující	existující	k2eAgMnSc1d1	existující
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nS	rodit
bez	bez	k7c2	bez
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
přežívá	přežívat	k5eAaImIp3nS	přežívat
ze	z	k7c2	z
slabosti	slabost	k1gFnSc2	slabost
a	a	k8xC	a
umírá	umírat	k5eAaImIp3nS	umírat
na	na	k7c6	na
setkání	setkání	k1gNnSc6	setkání
<g/>
.	.	kIx.	.
</s>
<s>
Zvrátil	zvrátit	k5eAaPmAgMnS	zvrátit
jsem	být	k5eAaImIp1nS	být
hlavu	hlava	k1gFnSc4	hlava
dozadu	dozadu	k6eAd1	dozadu
a	a	k8xC	a
zavřel	zavřít	k5eAaPmAgMnS	zavřít
oči	oko	k1gNnPc4	oko
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
náhle	náhle	k6eAd1	náhle
probuzené	probuzený	k2eAgInPc1d1	probuzený
obrazy	obraz	k1gInPc1	obraz
vstaly	vstát	k5eAaPmAgInP	vstát
a	a	k8xC	a
zaplnily	zaplnit	k5eAaPmAgInP	zaplnit
mi	já	k3xPp1nSc3	já
oči	oko	k1gNnPc4	oko
existencemi	existence	k1gFnPc7	existence
<g/>
:	:	kIx,	:
Existence	existence	k1gFnSc1	existence
je	být	k5eAaImIp3nS	být
plnost	plnost	k1gFnSc4	plnost
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
nelze	lze	k6eNd1	lze
opustit	opustit	k5eAaPmF	opustit
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Věděl	vědět	k5eAaImAgMnS	vědět
jsem	být	k5eAaImIp1nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
náhle	náhle	k6eAd1	náhle
ukázal	ukázat	k5eAaPmAgMnS	ukázat
a	a	k8xC	a
dusil	dusit	k5eAaImAgMnS	dusit
jsem	být	k5eAaImIp1nS	být
se	s	k7c7	s
zlostí	zlost	k1gFnSc7	zlost
na	na	k7c4	na
tu	ten	k3xDgFnSc4	ten
velikánskou	velikánský	k2eAgFnSc4d1	velikánská
<g/>
,	,	kIx,	,
absurdní	absurdní	k2eAgFnSc4d1	absurdní
bytost	bytost	k1gFnSc4	bytost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
D.	D.	kA	D.
Steinová	Steinová	k1gFnSc1	Steinová
<g/>
)	)	kIx)	)
Roquentin	Roquentin	k1gInSc1	Roquentin
je	být	k5eAaImIp3nS	být
nesmyslností	nesmyslnost	k1gFnSc7	nesmyslnost
bytí	bytí	k1gNnSc2	bytí
zhnusen	zhnusen	k2eAgMnSc1d1	zhnusen
a	a	k8xC	a
uzavírá	uzavírat	k5eAaImIp3nS	uzavírat
se	se	k3xPyFc4	se
do	do	k7c2	do
sebe	se	k3xPyFc2	se
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
však	však	k9	však
zahlédá	zahlédat	k5eAaImIp3nS	zahlédat
jakési	jakýsi	k3yIgNnSc1	jakýsi
východisko	východisko	k1gNnSc1	východisko
v	v	k7c6	v
požitku	požitek	k1gInSc6	požitek
z	z	k7c2	z
jazzové	jazzový	k2eAgFnSc2d1	jazzová
hudby	hudba	k1gFnSc2	hudba
(	(	kIx(	(
<g/>
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
prodchnuto	prodchnout	k5eAaPmNgNnS	prodchnout
písní	píseň	k1gFnSc7	píseň
Some	Som	k1gFnSc2	Som
of	of	k?	of
these	these	k1gFnPc1	these
days	daysa	k1gFnPc2	daysa
<g/>
)	)	kIx)	)
a	a	k8xC	a
možnosti	možnost	k1gFnPc4	možnost
napsat	napsat	k5eAaPmF	napsat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
knihu	kniha	k1gFnSc4	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
román	román	k1gInSc4	román
byl	být	k5eAaImAgMnS	být
jakýmsi	jakýsi	k3yIgNnSc7	jakýsi
předznamenáním	předznamenání	k1gNnSc7	předznamenání
existencialismu	existencialismus	k1gInSc2	existencialismus
<g/>
.	.	kIx.	.
</s>
<s>
Zeď	zeď	k1gFnSc1	zeď
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc1	soubor
5	[number]	k4	5
novel	novela	k1gFnPc2	novela
<g/>
,	,	kIx,	,
nejznámější	známý	k2eAgFnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
novela	novela	k1gFnSc1	novela
Zeď	zeď	k1gFnSc1	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
za	za	k7c2	za
španělské	španělský	k2eAgFnSc2d1	španělská
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
provizorním	provizorní	k2eAgNnSc6d1	provizorní
vězení	vězení	k1gNnSc6	vězení
s	s	k7c7	s
odsouzenci	odsouzenec	k1gMnPc7	odsouzenec
na	na	k7c4	na
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Zeď	zeď	k1gFnSc1	zeď
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
věznice	věznice	k1gFnSc2	věznice
je	být	k5eAaImIp3nS	být
místem	místem	k6eAd1	místem
jejich	jejich	k3xOp3gFnPc4	jejich
popravy	poprava	k1gFnPc4	poprava
<g/>
.	.	kIx.	.
</s>
<s>
Pomyslná	pomyslný	k2eAgFnSc1d1	pomyslná
zeď	zeď	k1gFnSc1	zeď
<g/>
,	,	kIx,	,
vystavená	vystavený	k2eAgFnSc1d1	vystavená
ze	z	k7c2	z
ztráty	ztráta	k1gFnSc2	ztráta
iluze	iluze	k1gFnSc2	iluze
věčného	věčný	k2eAgNnSc2d1	věčné
žití	žití	k1gNnSc2	žití
a	a	k8xC	a
očekávání	očekávání	k1gNnSc2	očekávání
blízké	blízký	k2eAgFnSc2d1	blízká
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
vězně	vězně	k6eAd1	vězně
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
samotě	samota	k1gFnSc6	samota
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
,	,	kIx,	,
anarchista	anarchista	k1gMnSc1	anarchista
Pablo	Pablo	k1gNnSc4	Pablo
Ibbieta	Ibbiet	k1gMnSc2	Ibbiet
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
při	při	k7c6	při
vědomí	vědomí	k1gNnSc6	vědomí
nevyhnutelné	vyhnutelný	k2eNgFnSc2d1	nevyhnutelná
smrti	smrt	k1gFnSc2	smrt
uvědomí	uvědomit	k5eAaPmIp3nS	uvědomit
absurditu	absurdita	k1gFnSc4	absurdita
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
svého	svůj	k3xOyFgMnSc2	svůj
velitele	velitel	k1gMnSc2	velitel
neprozradit	prozradit	k5eNaPmF	prozradit
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
si	se	k3xPyFc3	se
udělá	udělat	k5eAaPmIp3nS	udělat
z	z	k7c2	z
falangistů	falangista	k1gMnPc2	falangista
žert	žert	k1gInSc4	žert
<g/>
.	.	kIx.	.
</s>
<s>
Vymyslí	vymyslet	k5eAaPmIp3nS	vymyslet
si	se	k3xPyFc3	se
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
údajně	údajně	k6eAd1	údajně
jeho	jeho	k3xOp3gMnSc1	jeho
velitel	velitel	k1gMnSc1	velitel
ukrývat	ukrývat	k5eAaImF	ukrývat
-	-	kIx~	-
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Těší	těšit	k5eAaImIp3nS	těšit
se	se	k3xPyFc4	se
na	na	k7c4	na
zklamané	zklamaný	k2eAgMnPc4d1	zklamaný
falangisty	falangista	k1gMnPc4	falangista
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
však	však	k9	však
popraven	popraven	k2eAgMnSc1d1	popraven
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
překvapivě	překvapivě	k6eAd1	překvapivě
odveden	odveden	k2eAgInSc1d1	odveden
na	na	k7c4	na
dvůr	dvůr	k1gInSc4	dvůr
a	a	k8xC	a
podle	podle	k7c2	podle
všeho	všecek	k3xTgNnSc2	všecek
bude	být	k5eAaImBp3nS	být
za	za	k7c4	za
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
propuštěn	propustit	k5eAaPmNgMnS	propustit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
vůdce	vůdce	k1gMnSc1	vůdce
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
změnit	změnit	k5eAaPmF	změnit
úkryt	úkryt	k1gInSc4	úkryt
a	a	k8xC	a
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
byl	být	k5eAaImAgInS	být
skutečně	skutečně	k6eAd1	skutečně
nalezen	nalézt	k5eAaBmNgInS	nalézt
<g/>
.	.	kIx.	.
</s>
<s>
Cesty	cesta	k1gFnPc4	cesta
svobody	svoboda	k1gFnSc2	svoboda
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
-	-	kIx~	-
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
-	-	kIx~	-
nedokončená	dokončený	k2eNgFnSc1d1	nedokončená
románová	románový	k2eAgFnSc1d1	románová
tetralogie	tetralogie	k1gFnSc1	tetralogie
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgInSc7d1	základní
motivem	motiv	k1gInSc7	motiv
je	být	k5eAaImIp3nS	být
lidská	lidský	k2eAgFnSc1d1	lidská
svoboda	svoboda	k1gFnSc1	svoboda
<g/>
,	,	kIx,	,
děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1938	[number]	k4	1938
-	-	kIx~	-
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
hrdinou	hrdina	k1gMnSc7	hrdina
je	být	k5eAaImIp3nS	být
středoškolský	středoškolský	k2eAgMnSc1d1	středoškolský
profesor	profesor	k1gMnSc1	profesor
filosofie	filosofie	k1gFnSc2	filosofie
Mathieu	Mathie	k2eAgFnSc4d1	Mathie
Delarue	Delarue	k1gFnSc4	Delarue
<g/>
,	,	kIx,	,
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
Sartrovo	Sartrův	k2eAgNnSc1d1	Sartrovo
alter	alter	k1gInSc4	alter
ego	ego	k1gNnSc3	ego
<g/>
.	.	kIx.	.
</s>
<s>
Věk	věk	k1gInSc1	věk
rozumu	rozum	k1gInSc2	rozum
<g/>
:	:	kIx,	:
Profesor	profesor	k1gMnSc1	profesor
Delarue	Delaru	k1gFnSc2	Delaru
se	se	k3xPyFc4	se
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
dlouholeté	dlouholetý	k2eAgFnSc2d1	dlouholetá
přítelkyně	přítelkyně	k1gFnSc2	přítelkyně
Marcely	Marcela	k1gFnSc2	Marcela
dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
o	o	k7c6	o
jejím	její	k3xOp3gNnSc6	její
těhotenství	těhotenství	k1gNnSc6	těhotenství
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
jsou	být	k5eAaImIp3nP	být
oba	dva	k4xCgMnPc1	dva
rozhodnuti	rozhodnut	k2eAgMnPc1d1	rozhodnut
pro	pro	k7c4	pro
pokoutní	pokoutní	k2eAgInSc4d1	pokoutní
potrat	potrat	k1gInSc4	potrat
<g/>
,	,	kIx,	,
problémem	problém	k1gInSc7	problém
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
jenom	jenom	k9	jenom
sehnání	sehnání	k1gNnSc1	sehnání
potřebné	potřebný	k2eAgFnSc2d1	potřebná
finanční	finanční	k2eAgFnSc2d1	finanční
částky	částka	k1gFnSc2	částka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
komplikovaného	komplikovaný	k2eAgNnSc2d1	komplikované
hledání	hledání	k1gNnSc2	hledání
pomoci	pomoc	k1gFnSc2	pomoc
se	se	k3xPyFc4	se
profesor	profesor	k1gMnSc1	profesor
Delarue	Delarue	k1gFnPc2	Delarue
setkává	setkávat	k5eAaImIp3nS	setkávat
se	s	k7c7	s
španělským	španělský	k2eAgMnSc7d1	španělský
malířem	malíř	k1gMnSc7	malíř
Gómezem	Gómez	k1gInSc7	Gómez
<g/>
,	,	kIx,	,
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
Lolou	Lola	k1gMnSc7	Lola
<g/>
,	,	kIx,	,
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Jacquesem	Jacques	k1gMnSc7	Jacques
a	a	k8xC	a
také	také	k9	také
s	s	k7c7	s
estétem	estét	k1gMnSc7	estét
Danielem	Daniel	k1gMnSc7	Daniel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
obtížně	obtížně	k6eAd1	obtížně
smiřuje	smiřovat	k5eAaImIp3nS	smiřovat
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
tajenou	tajený	k2eAgFnSc7d1	tajená
homosexualitou	homosexualita	k1gFnSc7	homosexualita
<g/>
.	.	kIx.	.
</s>
<s>
Marcela	Marcela	k1gFnSc1	Marcela
však	však	k9	však
překvapivě	překvapivě	k6eAd1	překvapivě
změní	změnit	k5eAaPmIp3nS	změnit
svůj	svůj	k3xOyFgInSc4	svůj
postoj	postoj	k1gInSc4	postoj
<g/>
,	,	kIx,	,
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
dítě	dítě	k1gNnSc4	dítě
donosit	donosit	k5eAaPmF	donosit
a	a	k8xC	a
vyhoví	vyhovit	k5eAaPmIp3nS	vyhovit
také	také	k9	také
Danielově	Danielův	k2eAgFnSc3d1	Danielova
nabídce	nabídka	k1gFnSc3	nabídka
k	k	k7c3	k
sňatku	sňatek	k1gInSc3	sňatek
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
co	co	k3yRnSc1	co
tuší	tušit	k5eAaImIp3nS	tušit
o	o	k7c6	o
jeho	jeho	k3xOp3gInPc6	jeho
motivech	motiv	k1gInPc6	motiv
a	a	k8xC	a
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
homosexualitě	homosexualita	k1gFnSc6	homosexualita
<g/>
.	.	kIx.	.
</s>
<s>
Odklad	odklad	k1gInSc1	odklad
<g/>
:	:	kIx,	:
Tématem	téma	k1gNnSc7	téma
jsou	být	k5eAaImIp3nP	být
poslední	poslední	k2eAgInPc4d1	poslední
dny	den	k1gInPc4	den
září	září	k1gNnSc2	září
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
<s>
Sartre	Sartr	k1gMnSc5	Sartr
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
Johna	John	k1gMnSc2	John
Dos	Dos	k1gMnSc1	Dos
Passose	Passosa	k1gFnSc3	Passosa
nabízí	nabízet	k5eAaImIp3nS	nabízet
fragmentárně	fragmentárně	k6eAd1	fragmentárně
roztříštěný	roztříštěný	k2eAgMnSc1d1	roztříštěný
<g/>
,	,	kIx,	,
filmovou	filmový	k2eAgFnSc7d1	filmová
technikou	technika	k1gFnSc7	technika
inspirovaný	inspirovaný	k2eAgInSc1d1	inspirovaný
text	text	k1gInSc1	text
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
hlavním	hlavní	k2eAgNnSc7d1	hlavní
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Mnichovská	mnichovský	k2eAgFnSc1d1	Mnichovská
dohoda	dohoda	k1gFnSc1	dohoda
a	a	k8xC	a
její	její	k3xOp3gInPc1	její
důsledky	důsledek	k1gInPc1	důsledek
pro	pro	k7c4	pro
život	život	k1gInSc4	život
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Epizody	epizoda	k1gFnPc1	epizoda
se	se	k3xPyFc4	se
náhle	náhle	k6eAd1	náhle
střídají	střídat	k5eAaImIp3nP	střídat
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
sledovány	sledovat	k5eAaImNgInP	sledovat
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
synchronicitě	synchronicita	k1gFnSc6	synchronicita
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
děje	děj	k1gInSc2	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
také	také	k9	také
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
v	v	k7c6	v
textu	text	k1gInSc6	text
Přísečnice	Přísečnice	k1gFnSc2	Přísečnice
v	v	k7c6	v
Krušných	krušný	k2eAgFnPc6d1	krušná
horách	hora	k1gFnPc6	hora
<g/>
,	,	kIx,	,
okr	okr	k1gInSc1	okr
<g/>
.	.	kIx.	.
</s>
<s>
Chomutov	Chomutov	k1gInSc1	Chomutov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
probíhá	probíhat	k5eAaImIp3nS	probíhat
guerillový	guerillový	k2eAgInSc4d1	guerillový
boj	boj	k1gInSc4	boj
mezi	mezi	k7c7	mezi
německým	německý	k2eAgNnSc7d1	německé
a	a	k8xC	a
českým	český	k2eAgNnSc7d1	české
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
významnější	významný	k2eAgFnSc6d2	významnější
míře	míra	k1gFnSc6	míra
jsou	být	k5eAaImIp3nP	být
sledovány	sledovat	k5eAaImNgInP	sledovat
osudy	osud	k1gInPc1	osud
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
hrály	hrát	k5eAaImAgInP	hrát
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
díle	dílo	k1gNnSc6	dílo
jen	jen	k9	jen
podružnou	podružný	k2eAgFnSc4d1	podružná
úlohu	úloha	k1gFnSc4	úloha
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
v	v	k7c6	v
duši	duše	k1gFnSc6	duše
<g/>
:	:	kIx,	:
Profesor	profesor	k1gMnSc1	profesor
Mathieu	Mathieus	k1gInSc2	Mathieus
Delarue	Delarue	k1gInSc1	Delarue
je	být	k5eAaImIp3nS	být
odveden	odvést	k5eAaPmNgInS	odvést
do	do	k7c2	do
francouzské	francouzský	k2eAgFnSc2d1	francouzská
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
prožívá	prožívat	k5eAaImIp3nS	prožívat
průběh	průběh	k1gInSc4	průběh
tzv.	tzv.	kA	tzv.
Podivné	podivný	k2eAgFnSc2d1	podivná
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
ofenzíva	ofenzíva	k1gFnSc1	ofenzíva
Francie	Francie	k1gFnSc2	Francie
proti	proti	k7c3	proti
Německu	Německo	k1gNnSc3	Německo
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1939	[number]	k4	1939
<g/>
/	/	kIx~	/
<g/>
40	[number]	k4	40
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
románu	román	k1gInSc2	román
v	v	k7c6	v
beznadějné	beznadějný	k2eAgFnSc6d1	beznadějná
situaci	situace	k1gFnSc6	situace
z	z	k7c2	z
kostelní	kostelní	k2eAgFnSc2d1	kostelní
věže	věž	k1gFnSc2	věž
střílí	střílet	k5eAaImIp3nS	střílet
do	do	k7c2	do
Němců	Němec	k1gMnPc2	Němec
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jimi	on	k3xPp3gInPc7	on
ostřelován	ostřelovat	k5eAaImNgInS	ostřelovat
<g/>
.	.	kIx.	.
</s>
<s>
Španělský	španělský	k2eAgMnSc1d1	španělský
malíř	malíř	k1gMnSc1	malíř
Gómez	Gómez	k1gMnSc1	Gómez
emigroval	emigrovat	k5eAaBmAgMnS	emigrovat
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
živoří	živořit	k5eAaImIp3nP	živořit
a	a	k8xC	a
kde	kde	k6eAd1	kde
nenachází	nacházet	k5eNaImIp3nS	nacházet
nikoho	nikdo	k3yNnSc4	nikdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
by	by	kYmCp3nS	by
ocenil	ocenit	k5eAaPmAgMnS	ocenit
jeho	jeho	k3xOp3gNnSc4	jeho
umění	umění	k1gNnSc4	umění
a	a	k8xC	a
pochopil	pochopit	k5eAaPmAgMnS	pochopit
jeho	jeho	k3xOp3gFnSc4	jeho
otřesnou	otřesný	k2eAgFnSc4d1	otřesná
evropskou	evropský	k2eAgFnSc4d1	Evropská
zkušenost	zkušenost	k1gFnSc4	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Gómezova	Gómezův	k2eAgFnSc1d1	Gómezova
žena	žena	k1gFnSc1	žena
i	i	k9	i
se	s	k7c7	s
synkem	synek	k1gMnSc7	synek
spáchá	spáchat	k5eAaPmIp3nS	spáchat
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Francie	Francie	k1gFnSc2	Francie
sebevraždu	sebevražda	k1gFnSc4	sebevražda
při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
utéci	utéct	k5eAaPmF	utéct
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Danielova	Danielův	k2eAgFnSc1d1	Danielova
snaha	snaha	k1gFnSc1	snaha
žít	žít	k5eAaImF	žít
konvenčním	konvenční	k2eAgInSc7d1	konvenční
heterosexuálním	heterosexuální	k2eAgInSc7d1	heterosexuální
životem	život	k1gInSc7	život
ztroskotává	ztroskotávat	k5eAaImIp3nS	ztroskotávat
<g/>
,	,	kIx,	,
nadále	nadále	k6eAd1	nadále
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
efemérní	efemérní	k2eAgNnSc4d1	efemérní
dobrodružství	dobrodružství	k1gNnSc4	dobrodružství
s	s	k7c7	s
mladíky	mladík	k1gMnPc7	mladík
<g/>
.	.	kIx.	.
</s>
<s>
Komunistu	komunista	k1gMnSc4	komunista
Bruneta	brunet	k1gMnSc4	brunet
uvede	uvést	k5eAaPmIp3nS	uvést
politický	politický	k2eAgInSc1d1	politický
vývoj	vývoj	k1gInSc1	vývoj
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
do	do	k7c2	do
zmatku	zmatek	k1gInSc2	zmatek
a	a	k8xC	a
krize	krize	k1gFnSc2	krize
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnPc1d1	poslední
šance	šance	k1gFnPc1	šance
(	(	kIx(	(
<g/>
zachováno	zachovat	k5eAaPmNgNnS	zachovat
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
fragmentu	fragment	k1gInSc2	fragment
<g/>
;	;	kIx,	;
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
mj.	mj.	kA	mj.
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mathieu	Mathiea	k1gFnSc4	Mathiea
Delarue	Delarue	k1gInSc1	Delarue
přežil	přežít	k5eAaPmAgInS	přežít
těžká	těžký	k2eAgNnPc4d1	těžké
zranění	zranění	k1gNnPc4	zranění
a	a	k8xC	a
upadl	upadnout	k5eAaPmAgMnS	upadnout
do	do	k7c2	do
německého	německý	k2eAgNnSc2d1	německé
zajetí	zajetí	k1gNnSc2	zajetí
<g/>
;	;	kIx,	;
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
zajateckém	zajatecký	k2eAgInSc6d1	zajatecký
táboře	tábor	k1gInSc6	tábor
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
Francouzi	Francouz	k1gMnPc7	Francouz
<g/>
)	)	kIx)	)
Uragán	uragán	k1gInSc1	uragán
nad	nad	k7c7	nad
cukrem	cukr	k1gInSc7	cukr
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
-	-	kIx~	-
kniha	kniha	k1gFnSc1	kniha
reportáží	reportáž	k1gFnPc2	reportáž
o	o	k7c6	o
kubánské	kubánský	k2eAgFnSc6d1	kubánská
revoluci	revoluce	k1gFnSc6	revoluce
<g/>
,	,	kIx,	,
značně	značně	k6eAd1	značně
idealizující	idealizující	k2eAgFnSc1d1	idealizující
Fidela	Fidel	k1gMnSc4	Fidel
Castra	Castrum	k1gNnSc2	Castrum
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc7	jeho
společníky	společník	k1gMnPc7	společník
i	i	k9	i
kubánskou	kubánský	k2eAgFnSc4d1	kubánská
verzi	verze	k1gFnSc4	verze
komunismu	komunismus	k1gInSc2	komunismus
<g/>
.	.	kIx.	.
</s>
<s>
Slova	slovo	k1gNnPc4	slovo
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
-	-	kIx~	-
autobiografická	autobiografický	k2eAgFnSc1d1	autobiografická
<g/>
,	,	kIx,	,
poněkud	poněkud	k6eAd1	poněkud
sebeironicky	sebeironicky	k?	sebeironicky
laděná	laděný	k2eAgFnSc1d1	laděná
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
zachycující	zachycující	k2eAgFnSc1d1	zachycující
Sartrovo	Sartrův	k2eAgNnSc4d1	Sartrovo
dětství	dětství	k1gNnSc4	dětství
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
dětské	dětský	k2eAgNnSc1d1	dětské
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
spisovatelem	spisovatel	k1gMnSc7	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
téměř	téměř	k6eAd1	téměř
teologickou	teologický	k2eAgFnSc4d1	teologická
rozpravu	rozprava	k1gFnSc4	rozprava
o	o	k7c6	o
hodnotě	hodnota	k1gFnSc6	hodnota
poslání	poslání	k1gNnSc2	poslání
spisovatele	spisovatel	k1gMnSc2	spisovatel
a	a	k8xC	a
umělce	umělec	k1gMnSc2	umělec
<g/>
,	,	kIx,	,
o	o	k7c6	o
pokusu	pokus	k1gInSc6	pokus
zastoupit	zastoupit	k5eAaPmF	zastoupit
uměleckým	umělecký	k2eAgNnSc7d1	umělecké
tvořením	tvoření	k1gNnSc7	tvoření
nepřítomného	přítomný	k2eNgMnSc2d1	nepřítomný
Boha	bůh	k1gMnSc2	bůh
a	a	k8xC	a
vykoupit	vykoupit	k5eAaPmF	vykoupit
se	s	k7c7	s
vlastními	vlastní	k2eAgFnPc7d1	vlastní
silami	síla	k1gFnPc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Stárnoucí	stárnoucí	k2eAgMnSc1d1	stárnoucí
autor	autor	k1gMnSc1	autor
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
mladistvé	mladistvý	k2eAgFnSc3d1	mladistvá
volbě	volba	k1gFnSc3	volba
shovívavě	shovívavě	k6eAd1	shovívavě
skeptický	skeptický	k2eAgInSc1d1	skeptický
<g/>
,	,	kIx,	,
vnímá	vnímat	k5eAaImIp3nS	vnímat
ho	on	k3xPp3gInSc4	on
jako	jako	k8xS	jako
projekt	projekt	k1gInSc4	projekt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
sub	sub	k7c4	sub
specie	specie	k1gFnSc4	specie
aeternitatis	aeternitatis	k1gFnSc2	aeternitatis
nezdařil	zdařit	k5eNaPmAgMnS	zdařit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
Sartrových	Sartrův	k2eAgNnPc6d1	Sartrovo
dramatech	drama	k1gNnPc6	drama
hraje	hrát	k5eAaImIp3nS	hrát
zřetelnou	zřetelný	k2eAgFnSc4d1	zřetelná
roli	role	k1gFnSc4	role
vliv	vliv	k1gInSc4	vliv
antického	antický	k2eAgNnSc2d1	antické
dramatu	drama	k1gNnSc2	drama
<g/>
,	,	kIx,	,
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
spíše	spíše	k9	spíše
dědictví	dědictví	k1gNnSc2	dědictví
osvícenství	osvícenství	k1gNnSc2	osvícenství
<g/>
,	,	kIx,	,
B.	B.	kA	B.
Brechta	Brecht	k1gMnSc4	Brecht
nebo	nebo	k8xC	nebo
A.	A.	kA	A.
P.	P.	kA	P.
Čechova	Čechov	k1gMnSc2	Čechov
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
hry	hra	k1gFnPc1	hra
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
(	(	kIx(	(
<g/>
antika	antika	k1gFnSc1	antika
<g/>
,	,	kIx,	,
německá	německý	k2eAgFnSc1d1	německá
reformace	reformace	k1gFnSc1	reformace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
(	(	kIx(	(
<g/>
druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
historické	historický	k2eAgFnSc6d1	historická
realitě	realita	k1gFnSc6	realita
Sartrovi	Sartr	k1gMnSc3	Sartr
záleželo	záležet	k5eAaImAgNnS	záležet
jen	jen	k6eAd1	jen
málo	málo	k6eAd1	málo
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
o	o	k7c4	o
modelové	modelový	k2eAgFnPc4d1	modelová
hry	hra	k1gFnPc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Mouchy	moucha	k1gFnPc1	moucha
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
mouches	mouchesa	k1gFnPc2	mouchesa
<g/>
,	,	kIx,	,
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
-	-	kIx~	-
dramatická	dramatický	k2eAgFnSc1d1	dramatická
parafráze	parafráze	k1gFnSc1	parafráze
Orestei	Oreste	k1gFnSc2	Oreste
(	(	kIx(	(
<g/>
Aischylos	Aischylos	k1gInSc1	Aischylos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
drama	drama	k1gNnSc1	drama
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
počátek	počátek	k1gInSc4	počátek
existencialistického	existencialistický	k2eAgNnSc2d1	existencialistické
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
města	město	k1gNnSc2	město
Argu	Argos	k1gInSc2	Argos
přichází	přicházet	k5eAaImIp3nS	přicházet
osmnáctiletý	osmnáctiletý	k2eAgInSc4d1	osmnáctiletý
Orestés	Orestés	k1gInSc4	Orestés
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
paidagogem	paidagog	k1gInSc7	paidagog
<g/>
.	.	kIx.	.
</s>
<s>
Nežil	žít	k5eNaImAgMnS	žít
tu	tu	k6eAd1	tu
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
šťastnou	šťastný	k2eAgFnSc7d1	šťastná
shodou	shoda	k1gFnSc7	shoda
okolností	okolnost	k1gFnPc2	okolnost
zachráněn	zachránit	k5eAaPmNgInS	zachránit
před	před	k7c7	před
spiklencem	spiklenec	k1gMnSc7	spiklenec
Aigisthem	Aigisth	k1gInSc7	Aigisth
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
milenec	milenec	k1gMnSc1	milenec
královny	královna	k1gFnSc2	královna
Klytaimnestry	Klytaimnestra	k1gFnSc2	Klytaimnestra
<g/>
,	,	kIx,	,
Orestovy	Orestův	k2eAgFnSc2d1	Orestova
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
zavraždil	zavraždit	k5eAaPmAgMnS	zavraždit
s	s	k7c7	s
jejím	její	k3xOp3gNnSc7	její
vědomím	vědomí	k1gNnSc7	vědomí
krále	král	k1gMnSc2	král
Agamemnóna	Agamemnón	k1gMnSc2	Agamemnón
při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
trójského	trójský	k2eAgNnSc2d1	trójský
tažení	tažení	k1gNnSc2	tažení
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
usedl	usednout	k5eAaPmAgMnS	usednout
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
stalo	stát	k5eAaPmAgNnS	stát
ponurým	ponurý	k2eAgInSc7d1	ponurý
<g/>
,	,	kIx,	,
totalitářským	totalitářský	k2eAgInSc7d1	totalitářský
státem	stát	k1gInSc7	stát
(	(	kIx(	(
<g/>
trefně	trefně	k6eAd1	trefně
parodujícím	parodující	k2eAgInSc7d1	parodující
poměry	poměr	k1gInPc4	poměr
v	v	k7c6	v
kolaborantské	kolaborantský	k2eAgFnSc6d1	kolaborantská
vichistické	vichistický	k2eAgFnSc6d1	vichistická
Francii	Francie	k1gFnSc6	Francie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Orestés	Orestés	k6eAd1	Orestés
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
spíše	spíše	k9	spíše
zvědavý	zvědavý	k2eAgMnSc1d1	zvědavý
mladý	mladý	k2eAgMnSc1d1	mladý
estét	estét	k1gMnSc1	estét
a	a	k8xC	a
dobrodruh	dobrodruh	k1gMnSc1	dobrodruh
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
pomstít	pomstít	k5eAaPmF	pomstít
bezpráví	bezpráví	k1gNnSc4	bezpráví
a	a	k8xC	a
vzít	vzít	k5eAaPmF	vzít
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
tíhu	tíha	k1gFnSc4	tíha
viny	vina	k1gFnSc2	vina
za	za	k7c4	za
zabití	zabití	k1gNnSc4	zabití
Aigistha	Aigisth	k1gMnSc2	Aigisth
i	i	k8xC	i
své	svůj	k3xOyFgFnSc2	svůj
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
Počestná	počestný	k2eAgFnSc1d1	počestná
děvka	děvka	k1gFnSc1	děvka
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
-	-	kIx~	-
hra	hra	k1gFnSc1	hra
ostře	ostro	k6eAd1	ostro
kritická	kritický	k2eAgNnPc4d1	kritické
k	k	k7c3	k
rasistickým	rasistický	k2eAgInPc3d1	rasistický
poměrům	poměr	k1gInPc3	poměr
na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
Jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
znásilnění	znásilnění	k1gNnSc2	znásilnění
a	a	k8xC	a
vraždy	vražda	k1gFnSc2	vražda
<g/>
,	,	kIx,	,
spáchané	spáchaný	k2eAgNnSc1d1	spáchané
bohatými	bohatý	k2eAgMnPc7d1	bohatý
bělošskými	bělošský	k2eAgMnPc7d1	bělošský
mládenci	mládenec	k1gMnPc7	mládenec
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
obviněn	obviněn	k2eAgMnSc1d1	obviněn
nevinný	vinný	k2eNgMnSc1d1	nevinný
černoch	černoch	k1gMnSc1	černoch
<g/>
.	.	kIx.	.
</s>
<s>
Klíčové	klíčový	k2eAgNnSc1d1	klíčové
je	být	k5eAaImIp3nS	být
svědectví	svědectví	k1gNnSc1	svědectví
bílé	bílý	k2eAgFnSc2d1	bílá
prostitutky	prostitutka	k1gFnSc2	prostitutka
Lizzie	Lizzie	k1gFnSc2	Lizzie
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
zastrašována	zastrašovat	k5eAaImNgFnS	zastrašovat
vlivnými	vlivný	k2eAgFnPc7d1	vlivná
příbuznými	příbuzná	k1gFnPc7	příbuzná
viníků	viník	k1gMnPc2	viník
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
rozpoutá	rozpoutat	k5eAaPmIp3nS	rozpoutat
lynč	lynč	k1gInSc1	lynč
na	na	k7c4	na
černochy	černoch	k1gMnPc4	černoch
<g/>
.	.	kIx.	.
</s>
<s>
Lizzie	Lizzie	k1gFnSc1	Lizzie
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
policii	policie	k1gFnSc3	policie
odhalit	odhalit	k5eAaPmF	odhalit
pravé	pravý	k2eAgMnPc4d1	pravý
viníky	viník	k1gMnPc4	viník
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
zavřenými	zavřený	k2eAgFnPc7d1	zavřená
dveřmi	dveře	k1gFnPc7	dveře
(	(	kIx(	(
<g/>
Huis-clos	Huislos	k1gInSc1	Huis-clos
<g/>
,	,	kIx,	,
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
i	i	k9	i
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
S	s	k7c7	s
vyloučením	vyloučení	k1gNnSc7	vyloučení
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
je	být	k5eAaImIp3nS	být
vybudovaná	vybudovaný	k2eAgFnSc1d1	vybudovaná
podle	podle	k7c2	podle
klasického	klasický	k2eAgInSc2d1	klasický
aristotelovského	aristotelovský	k2eAgInSc2d1	aristotelovský
modelu	model	k1gInSc2	model
-	-	kIx~	-
jednoty	jednota	k1gFnSc2	jednota
času	čas	k1gInSc2	čas
a	a	k8xC	a
děje	děj	k1gInSc2	děj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
místnosti	místnost	k1gFnSc6	místnost
spolu	spolu	k6eAd1	spolu
debatují	debatovat	k5eAaImIp3nP	debatovat
tři	tři	k4xCgMnPc1	tři
mrtví	mrtvý	k1gMnPc1	mrtvý
<g/>
:	:	kIx,	:
novinář	novinář	k1gMnSc1	novinář
Garcin	Garcin	k1gMnSc1	Garcin
-	-	kIx~	-
popravený	popravený	k2eAgMnSc1d1	popravený
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
jako	jako	k9	jako
dezertér	dezertér	k1gMnSc1	dezertér
<g/>
,	,	kIx,	,
lesbička	lesbička	k1gFnSc1	lesbička
Inè	Inè	k1gFnSc1	Inè
(	(	kIx(	(
<g/>
sebevražedkyně	sebevražedkyně	k1gFnSc1	sebevražedkyně
<g/>
)	)	kIx)	)
a	a	k8xC	a
egoistická	egoistický	k2eAgFnSc1d1	egoistická
erotomanka	erotomanka	k1gFnSc1	erotomanka
(	(	kIx(	(
<g/>
vražedkyně	vražedkyně	k1gFnSc1	vražedkyně
vlastního	vlastní	k2eAgNnSc2d1	vlastní
dítěte	dítě	k1gNnSc2	dítě
<g/>
)	)	kIx)	)
Estelle	Estelle	k1gNnSc2	Estelle
<g/>
.	.	kIx.	.
</s>
<s>
Žádná	žádný	k3yNgFnSc1	žádný
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
postav	postava	k1gFnPc2	postava
se	se	k3xPyFc4	se
není	být	k5eNaImIp3nS	být
ochotna	ochoten	k2eAgFnSc1d1	ochotna
vzdát	vzdát	k5eAaPmF	vzdát
svého	svůj	k3xOyFgNnSc2	svůj
mínění	mínění	k1gNnSc2	mínění
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
usvědčuje	usvědčovat	k5eAaImIp3nS	usvědčovat
vidění	vidění	k1gNnPc2	vidění
druhých	druhý	k4xOgInPc2	druhý
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
zrcadly	zrcadlo	k1gNnPc7	zrcadlo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mohou	moct	k5eAaImIp3nP	moct
vidět	vidět	k5eAaImF	vidět
pravdu	pravda	k1gFnSc4	pravda
a	a	k8xC	a
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
obraze	obraz	k1gInSc6	obraz
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
nic	nic	k6eAd1	nic
změnit	změnit	k5eAaPmF	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
pekle	peklo	k1gNnSc6	peklo
prokletí	prokletí	k1gNnSc2	prokletí
pohledem	pohled	k1gInSc7	pohled
těch	ten	k3xDgMnPc2	ten
druhých	druhý	k4xOgMnPc2	druhý
Mrtví	mrtvit	k5eAaImIp3nS	mrtvit
bez	bez	k1gInSc1	bez
pohřbu	pohřeb	k1gInSc2	pohřeb
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
-	-	kIx~	-
tato	tento	k3xDgFnSc1	tento
hra	hra	k1gFnSc1	hra
pobuřovala	pobuřovat	k5eAaImAgFnS	pobuřovat
především	především	k9	především
svou	svůj	k3xOyFgFnSc7	svůj
drastičností	drastičnost	k1gFnSc7	drastičnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
improvizovaném	improvizovaný	k2eAgNnSc6d1	improvizované
vězení	vězení	k1gNnSc6	vězení
na	na	k7c6	na
francouzském	francouzský	k2eAgInSc6d1	francouzský
venkově	venkov	k1gInSc6	venkov
(	(	kIx(	(
<g/>
zřejmě	zřejmě	k6eAd1	zřejmě
nedaleko	nedaleko	k7c2	nedaleko
švýcarských	švýcarský	k2eAgFnPc2d1	švýcarská
hranic	hranice	k1gFnPc2	hranice
<g/>
)	)	kIx)	)
čekají	čekat	k5eAaImIp3nP	čekat
na	na	k7c4	na
výslech	výslech	k1gInSc4	výslech
a	a	k8xC	a
jistou	jistý	k2eAgFnSc4d1	jistá
smrt	smrt	k1gFnSc4	smrt
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
gestapa	gestapo	k1gNnSc2	gestapo
francouzští	francouzštit	k5eAaImIp3nP	francouzštit
odbojáři	odbojář	k1gMnPc1	odbojář
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
nejmladší	mladý	k2eAgMnPc1d3	nejmladší
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
patnáctiletý	patnáctiletý	k2eAgMnSc1d1	patnáctiletý
Francois	Francois	k1gMnSc1	Francois
ze	z	k7c2	z
strachu	strach	k1gInSc2	strach
z	z	k7c2	z
mučení	mučení	k1gNnSc2	mučení
a	a	k8xC	a
smrti	smrt	k1gFnSc2	smrt
hrozí	hrozit	k5eAaImIp3nS	hrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
promluví	promluvit	k5eAaPmIp3nP	promluvit
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnPc1d1	ostatní
ho	on	k3xPp3gMnSc4	on
zardousí	zardousit	k5eAaPmIp3nP	zardousit
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
nečekaný	čekaný	k2eNgInSc4d1	nečekaný
obrat	obrat	k1gInSc4	obrat
v	v	k7c6	v
postoji	postoj	k1gInSc6	postoj
velitele	velitel	k1gMnSc4	velitel
kolaborantské	kolaborantský	k2eAgFnSc2d1	kolaborantská
milice	milice	k1gFnSc2	milice
dopadne	dopadnout	k5eAaPmIp3nS	dopadnout
jejich	jejich	k3xOp3gInSc1	jejich
osud	osud	k1gInSc1	osud
špatně	špatně	k6eAd1	špatně
-	-	kIx~	-
absurdní	absurdní	k2eAgFnSc7d1	absurdní
popravou	poprava	k1gFnSc7	poprava
na	na	k7c6	na
nádvoří	nádvoří	k1gNnSc6	nádvoří
<g/>
.	.	kIx.	.
</s>
<s>
Špinavé	špinavý	k2eAgFnPc1d1	špinavá
ruce	ruka	k1gFnPc1	ruka
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
-	-	kIx~	-
V	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
antikomunistickou	antikomunistický	k2eAgFnSc4d1	antikomunistická
hru	hra	k1gFnSc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Idealista	idealista	k1gMnSc1	idealista
Hugo	Hugo	k1gMnSc1	Hugo
<g/>
,	,	kIx,	,
buržoazní	buržoazní	k2eAgMnSc1d1	buržoazní
jedináček	jedináček	k1gMnSc1	jedináček
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
nejmenované	jmenovaný	k2eNgFnSc6d1	nejmenovaná
východoevropské	východoevropský	k2eAgFnSc6d1	východoevropská
zemi	zem	k1gFnSc6	zem
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
pokyn	pokyn	k1gInSc4	pokyn
tajné	tajný	k2eAgFnSc2d1	tajná
frakce	frakce	k1gFnSc2	frakce
zavraždit	zavraždit	k5eAaPmF	zavraždit
jejího	její	k3xOp3gMnSc4	její
generálního	generální	k2eAgMnSc4d1	generální
tajemníka	tajemník	k1gMnSc4	tajemník
Hoederera	Hoederer	k1gMnSc4	Hoederer
<g/>
.	.	kIx.	.
</s>
<s>
Končí	končit	k5eAaImIp3nS	končit
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
strana	strana	k1gFnSc1	strana
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
v	v	k7c6	v
ilegalitě	ilegalita	k1gFnSc6	ilegalita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
brzy	brzy	k6eAd1	brzy
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
má	mít	k5eAaImIp3nS	mít
vystoupit	vystoupit	k5eAaPmF	vystoupit
a	a	k8xC	a
ujmout	ujmout	k5eAaPmF	ujmout
se	se	k3xPyFc4	se
značné	značný	k2eAgFnSc2d1	značná
části	část	k1gFnSc2	část
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Hugovi	Hugo	k1gMnSc3	Hugo
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nS	podařit
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
Hoedererovým	Hoedererův	k2eAgMnSc7d1	Hoedererův
sekretářem	sekretář	k1gMnSc7	sekretář
a	a	k8xC	a
svého	svůj	k3xOyFgMnSc4	svůj
šéfa	šéf	k1gMnSc4	šéf
nakonec	nakonec	k6eAd1	nakonec
zavraždit	zavraždit	k5eAaPmF	zavraždit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pravou	pravý	k2eAgFnSc7d1	pravá
příčinou	příčina	k1gFnSc7	příčina
činu	čin	k1gInSc2	čin
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
žárlivost	žárlivost	k1gFnSc1	žárlivost
<g/>
.	.	kIx.	.
</s>
<s>
Ďábel	ďábel	k1gMnSc1	ďábel
a	a	k8xC	a
Pánbůh	Pánbůh	k1gMnSc1	Pánbůh
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
-	-	kIx~	-
Velmi	velmi	k6eAd1	velmi
rozsáhlé	rozsáhlý	k2eAgInPc1d1	rozsáhlý
<g/>
,	,	kIx,	,
filosoficky	filosoficky	k6eAd1	filosoficky
zřejmě	zřejmě	k6eAd1	zřejmě
nejpropracovanější	propracovaný	k2eAgNnSc4d3	nejpropracovanější
Sartrovo	Sartrův	k2eAgNnSc4d1	Sartrovo
drama	drama	k1gNnSc4	drama
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kulisách	kulisa	k1gFnPc6	kulisa
německé	německý	k2eAgFnSc2d1	německá
selské	selský	k2eAgFnSc2d1	selská
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
1525	[number]	k4	1525
<g/>
)	)	kIx)	)
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
příběh	příběh	k1gInSc1	příběh
vojevůdce	vojevůdce	k1gMnSc2	vojevůdce
Goetze	Goetze	k1gFnSc1	Goetze
von	von	k1gInSc1	von
Birlichingen	Birlichingen	k1gInSc1	Birlichingen
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
bastard	bastard	k1gMnSc1	bastard
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
šlechtičny	šlechtična	k1gFnSc2	šlechtična
a	a	k8xC	a
selského	selský	k2eAgMnSc2d1	selský
rebela	rebel	k1gMnSc2	rebel
nepatří	patřit	k5eNaImIp3nS	patřit
do	do	k7c2	do
žádné	žádný	k3yNgFnSc2	žádný
společenské	společenský	k2eAgFnSc2d1	společenská
kasty	kasta	k1gFnSc2	kasta
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
zavázán	zavázat	k5eAaPmNgMnS	zavázat
žádné	žádný	k3yNgFnSc3	žádný
ideji	idea	k1gFnSc3	idea
<g/>
.	.	kIx.	.
</s>
<s>
Považuje	považovat	k5eAaImIp3nS	považovat
se	se	k3xPyFc4	se
za	za	k7c4	za
osamělého	osamělý	k2eAgMnSc4d1	osamělý
metafyzického	metafyzický	k2eAgMnSc4d1	metafyzický
dobrodruha	dobrodruh	k1gMnSc4	dobrodruh
<g/>
,	,	kIx,	,
za	za	k7c4	za
titánského	titánský	k2eAgMnSc4d1	titánský
pána	pán	k1gMnSc4	pán
svého	svůj	k3xOyFgInSc2	svůj
osudu	osud	k1gInSc2	osud
a	a	k8xC	a
hledače	hledač	k1gInSc2	hledač
absolutna	absolutno	k1gNnSc2	absolutno
<g/>
.	.	kIx.	.
</s>
<s>
Dobytí	dobytí	k1gNnSc1	dobytí
města	město	k1gNnSc2	město
Wormsu	Worms	k1gInSc2	Worms
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
vzbouřilo	vzbouřit	k5eAaPmAgNnS	vzbouřit
proti	proti	k7c3	proti
svému	svůj	k3xOyFgMnSc3	svůj
biskupovi	biskup	k1gMnSc3	biskup
<g/>
,	,	kIx,	,
krutosti	krutost	k1gFnSc3	krutost
války	válka	k1gFnSc2	válka
a	a	k8xC	a
také	také	k6eAd1	také
láska	láska	k1gFnSc1	láska
ke	k	k7c3	k
krásné	krásný	k2eAgFnSc3d1	krásná
a	a	k8xC	a
obětavé	obětavý	k2eAgFnSc3d1	obětavá
Hildě	Hilda	k1gFnSc3	Hilda
Goetze	Goetze	k1gFnSc2	Goetze
přimějí	přimět	k5eAaPmIp3nP	přimět
ke	k	k7c3	k
snaze	snaha	k1gFnSc3	snaha
změnit	změnit	k5eAaPmF	změnit
se	se	k3xPyFc4	se
v	v	k7c4	v
mystika	mystik	k1gMnSc4	mystik
a	a	k8xC	a
hlasatele	hlasatel	k1gMnSc4	hlasatel
humanistické	humanistický	k2eAgFnSc2d1	humanistická
a	a	k8xC	a
altruistické	altruistický	k2eAgFnSc2d1	altruistická
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
trpké	trpký	k2eAgFnPc1d1	trpká
zkušenosti	zkušenost	k1gFnPc1	zkušenost
ho	on	k3xPp3gNnSc4	on
nakonec	nakonec	k6eAd1	nakonec
přimějí	přimět	k5eAaPmIp3nP	přimět
dále	daleko	k6eAd2	daleko
svůj	svůj	k3xOyFgInSc4	svůj
postoj	postoj	k1gInSc4	postoj
poopravit	poopravit	k5eAaPmF	poopravit
<g/>
,	,	kIx,	,
přiznat	přiznat	k5eAaPmF	přiznat
naivitu	naivita	k1gFnSc4	naivita
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
humanismu	humanismus	k1gInSc2	humanismus
a	a	k8xC	a
přijmout	přijmout	k5eAaPmF	přijmout
prvek	prvek	k1gInSc4	prvek
nezbytného	zbytný	k2eNgNnSc2d1	nezbytné
revolučního	revoluční	k2eAgNnSc2d1	revoluční
násilí	násilí	k1gNnSc2	násilí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
jedině	jedině	k6eAd1	jedině
prostředkuje	prostředkovat	k5eAaImIp3nS	prostředkovat
cestu	cesta	k1gFnSc4	cesta
ke	k	k7c3	k
spravedlivé	spravedlivý	k2eAgFnSc3d1	spravedlivá
společnosti	společnost	k1gFnSc3	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
představuje	představovat	k5eAaImIp3nS	představovat
mj.	mj.	kA	mj.
apologii	apologie	k1gFnSc4	apologie
Sartrova	Sartrův	k2eAgInSc2d1	Sartrův
vztahu	vztah	k1gInSc2	vztah
k	k	k7c3	k
marxismu	marxismus	k1gInSc3	marxismus
a	a	k8xC	a
politice	politika	k1gFnSc3	politika
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
však	však	k9	však
také	také	k9	také
jeho	jeho	k3xOp3gFnSc4	jeho
ateistickou	ateistický	k2eAgFnSc4d1	ateistická
filosofii	filosofie	k1gFnSc4	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Vězňové	vězeň	k1gMnPc1	vězeň
z	z	k7c2	z
Altony	Altona	k1gFnSc2	Altona
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
-	-	kIx~	-
Drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
poválečnou	poválečný	k2eAgFnSc4d1	poválečná
realitu	realita	k1gFnSc4	realita
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
nezvládnutou	zvládnutý	k2eNgFnSc4d1	nezvládnutá
nedávnou	dávný	k2eNgFnSc4d1	nedávná
minulost	minulost	k1gFnSc4	minulost
<g/>
;	;	kIx,	;
podle	podle	k7c2	podle
autorova	autorův	k2eAgNnSc2d1	autorovo
přiznání	přiznání	k1gNnSc2	přiznání
je	být	k5eAaImIp3nS	být
však	však	k9	však
tento	tento	k3xDgInSc4	tento
rámec	rámec	k1gInSc4	rámec
jen	jen	k6eAd1	jen
jinotajnou	jinotajný	k2eAgFnSc7d1	jinotajná
kritikou	kritika	k1gFnSc7	kritika
francouzského	francouzský	k2eAgNnSc2d1	francouzské
chování	chování	k1gNnSc2	chování
v	v	k7c6	v
Alžírsku	Alžírsko	k1gNnSc6	Alžírsko
(	(	kIx(	(
<g/>
srov.	srov.	kA	srov.
motiv	motiv	k1gInSc4	motiv
Franzova	Franzův	k2eAgNnSc2d1	Franzovo
mučení	mučení	k1gNnSc2	mučení
civilního	civilní	k2eAgNnSc2d1	civilní
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
frontě	fronta	k1gFnSc6	fronta
<g />
.	.	kIx.	.
</s>
<s>
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
-	-	kIx~	-
Smrtelně	smrtelně	k6eAd1	smrtelně
nemocný	mocný	k2eNgMnSc1d1	nemocný
lodní	lodní	k2eAgMnSc1d1	lodní
rejdař	rejdař	k1gMnSc1	rejdař
a	a	k8xC	a
milionář	milionář	k1gMnSc1	milionář
Gerlach	Gerlach	k1gMnSc1	Gerlach
svolává	svolávat	k5eAaImIp3nS	svolávat
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
vily	vila	k1gFnSc2	vila
na	na	k7c6	na
hamburském	hamburský	k2eAgNnSc6d1	hamburské
předměstí	předměstí	k1gNnSc6	předměstí
své	svůj	k3xOyFgFnSc2	svůj
blízké	blízký	k2eAgFnSc2d1	blízká
(	(	kIx(	(
<g/>
dcera	dcera	k1gFnSc1	dcera
Leni	Leni	k?	Leni
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Werner	Werner	k1gMnSc1	Werner
<g/>
,	,	kIx,	,
snacha	snacha	k1gFnSc1	snacha
Johanna	Johanna	k1gFnSc1	Johanna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jim	on	k3xPp3gMnPc3	on
kromě	kromě	k7c2	kromě
rozdělení	rozdělení	k1gNnSc2	rozdělení
dědictví	dědictví	k1gNnSc2	dědictví
svěřil	svěřit	k5eAaPmAgMnS	svěřit
rodinné	rodinný	k2eAgNnSc1d1	rodinné
tajemství	tajemství	k1gNnSc1	tajemství
<g/>
,	,	kIx,	,
známé	známý	k2eAgNnSc1d1	známé
ovšem	ovšem	k9	ovšem
Leni	Leni	k?	Leni
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
odlehlém	odlehlý	k2eAgInSc6d1	odlehlý
pokojíku	pokojík	k1gInSc6	pokojík
žije	žít	k5eAaImIp3nS	žít
izolovaný	izolovaný	k2eAgMnSc1d1	izolovaný
a	a	k8xC	a
pološílený	pološílený	k2eAgMnSc1d1	pološílený
Gerlachův	Gerlachův	k2eAgMnSc1d1	Gerlachův
syn	syn	k1gMnSc1	syn
Franz	Franz	k1gMnSc1	Franz
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
podle	podle	k7c2	podle
letité	letitý	k2eAgFnSc2d1	letitá
legendy	legenda	k1gFnSc2	legenda
padl	padnout	k5eAaPmAgInS	padnout
za	za	k7c2	za
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Franz	Franz	k1gMnSc1	Franz
nevychází	vycházet	k5eNaImIp3nS	vycházet
z	z	k7c2	z
pokoje	pokoj	k1gInSc2	pokoj
a	a	k8xC	a
třináct	třináct	k4xCc4	třináct
let	léto	k1gNnPc2	léto
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
neví	vědět	k5eNaImIp3nS	vědět
o	o	k7c6	o
světě	svět	k1gInSc6	svět
venku	venku	k6eAd1	venku
téměř	téměř	k6eAd1	téměř
nic	nic	k3yNnSc1	nic
<g/>
.	.	kIx.	.
</s>
<s>
Podmínkou	podmínka	k1gFnSc7	podmínka
umírajícího	umírající	k2eAgMnSc2d1	umírající
Gerlacha	Gerlach	k1gMnSc2	Gerlach
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
pozůstalí	pozůstalý	k1gMnPc1	pozůstalý
nechali	nechat	k5eAaPmAgMnP	nechat
Franze	Franze	k1gFnPc4	Franze
žít	žít	k5eAaImF	žít
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
pokoji	pokoj	k1gInSc6	pokoj
a	a	k8xC	a
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
sdíleli	sdílet	k5eAaImAgMnP	sdílet
jeho	jeho	k3xOp3gFnSc4	jeho
společenskou	společenský	k2eAgFnSc4d1	společenská
izolaci	izolace	k1gFnSc4	izolace
-	-	kIx~	-
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nejsou	být	k5eNaImIp3nP	být
zcela	zcela	k6eAd1	zcela
zřejmé	zřejmý	k2eAgFnPc1d1	zřejmá
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Johannou	Johanný	k2eAgFnSc7d1	Johanný
a	a	k8xC	a
Franze	Franze	k1gFnSc1	Franze
se	se	k3xPyFc4	se
rozvine	rozvinout	k5eAaPmIp3nS	rozvinout
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
náklonnost	náklonnost	k1gFnSc1	náklonnost
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
výkladu	výklad	k1gInSc2	výklad
starého	starý	k2eAgMnSc2d1	starý
Gerlacha	Gerlach	k1gMnSc2	Gerlach
a	a	k8xC	a
zmateného	zmatený	k2eAgNnSc2d1	zmatené
svěřování	svěřování	k1gNnSc2	svěřování
Franze	Franze	k1gFnSc2	Franze
začíná	začínat	k5eAaImIp3nS	začínat
vyplývat	vyplývat	k5eAaImF	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Franz	Franz	k1gMnSc1	Franz
byl	být	k5eAaImAgMnS	být
původně	původně	k6eAd1	původně
idealistickým	idealistický	k2eAgMnSc7d1	idealistický
antinacistou	antinacista	k1gMnSc7	antinacista
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
hnusil	hnusit	k5eAaImAgMnS	hnusit
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
i	i	k8xC	i
jeho	jeho	k3xOp3gNnSc4	jeho
pragmatické	pragmatický	k2eAgNnSc4d1	pragmatické
akceptování	akceptování	k1gNnSc4	akceptování
hitlerovského	hitlerovský	k2eAgInSc2d1	hitlerovský
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k6eAd1	jednou
v	v	k7c6	v
nočním	noční	k2eAgInSc6d1	noční
parku	park	k1gInSc6	park
objevil	objevit	k5eAaPmAgInS	objevit
uprchlého	uprchlý	k1gMnSc4	uprchlý
vězně	vězeň	k1gMnSc4	vězeň
z	z	k7c2	z
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
<g/>
,	,	kIx,	,
židovského	židovský	k2eAgMnSc2d1	židovský
rabína	rabín	k1gMnSc2	rabín
a	a	k8xC	a
pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
zachránit	zachránit	k5eAaPmF	zachránit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
nezdaří	zdařit	k5eNaPmIp3nS	zdařit
<g/>
,	,	kIx,	,
rabín	rabín	k1gMnSc1	rabín
je	být	k5eAaImIp3nS	být
před	před	k7c7	před
Franzovýma	Franzův	k2eAgNnPc7d1	Franzovo
očima	oko	k1gNnPc7	oko
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
gestapem	gestapo	k1gNnSc7	gestapo
a	a	k8xC	a
Franz	Franz	k1gInSc1	Franz
musí	muset	k5eAaImIp3nS	muset
za	za	k7c4	za
trest	trest	k1gInSc4	trest
narukovat	narukovat	k5eAaPmF	narukovat
na	na	k7c4	na
východní	východní	k2eAgFnSc4d1	východní
frontu	fronta	k1gFnSc4	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
přiznává	přiznávat	k5eAaImIp3nS	přiznávat
Johanně	Johanně	k1gFnSc4	Johanně
<g/>
,	,	kIx,	,
dopouštěl	dopouštět	k5eAaImAgMnS	dopouštět
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
krutostí	krutost	k1gFnSc7	krutost
na	na	k7c6	na
civilním	civilní	k2eAgNnSc6d1	civilní
obyvatelstvu	obyvatelstvo	k1gNnSc6	obyvatelstvo
a	a	k8xC	a
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
mučení	mučení	k1gNnSc2	mučení
<g/>
.	.	kIx.	.
</s>
<s>
Johannu	Johannout	k5eAaImIp1nS	Johannout
toto	tento	k3xDgNnSc4	tento
svěření	svěření	k1gNnSc4	svěření
od	od	k7c2	od
její	její	k3xOp3gFnSc2	její
zamilovanosti	zamilovanost	k1gFnSc2	zamilovanost
do	do	k7c2	do
Franze	Franze	k1gFnSc2	Franze
nadobro	nadobro	k6eAd1	nadobro
odradí	odradit	k5eAaPmIp3nP	odradit
<g/>
.	.	kIx.	.
</s>
<s>
Franz	Franz	k1gMnSc1	Franz
i	i	k9	i
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
proto	proto	k8xC	proto
volí	volit	k5eAaImIp3nS	volit
společnou	společný	k2eAgFnSc4d1	společná
dobrovolnou	dobrovolný	k2eAgFnSc4d1	dobrovolná
smrt	smrt	k1gFnSc4	smrt
v	v	k7c6	v
automobilu	automobil	k1gInSc6	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Jean-Paul	Jean-Paul	k1gInSc1	Jean-Paul
Sartre	Sartr	k1gMnSc5	Sartr
a	a	k8xC	a
Simone	Simon	k1gMnSc5	Simon
de	de	k?	de
Beauvoir	Beauvoir	k1gInSc4	Beauvoir
jsou	být	k5eAaImIp3nP	být
hlavními	hlavní	k2eAgFnPc7d1	hlavní
postavami	postava	k1gFnPc7	postava
rozhlasové	rozhlasový	k2eAgFnSc2d1	rozhlasová
hry	hra	k1gFnSc2	hra
Benjamina	Benjamin	k1gMnSc2	Benjamin
Kurase	Kuras	k1gInSc6	Kuras
Sebeklamy	sebeklam	k1gInPc1	sebeklam
:	:	kIx,	:
lehce	lehko	k6eAd1	lehko
absurdní	absurdní	k2eAgFnSc2d1	absurdní
rozhlasové	rozhlasový	k2eAgFnSc2d1	rozhlasová
komedie	komedie	k1gFnSc2	komedie
s	s	k7c7	s
francouzsko-levičáckými	francouzskoevičácký	k2eAgInPc7d1	francouzsko-levičácký
nápěvy	nápěv	k1gInPc7	nápěv
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Michal	Michal	k1gMnSc1	Michal
Bureš	Bureš	k1gMnSc1	Bureš
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
Hana	Hana	k1gFnSc1	Hana
Maciuchová	Maciuchová	k1gFnSc1	Maciuchová
a	a	k8xC	a
Igor	Igor	k1gMnSc1	Igor
Bareš	Bareš	k1gMnSc1	Bareš
<g/>
,	,	kIx,	,
zpracováno	zpracován	k2eAgNnSc1d1	zpracováno
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
rozhlasu	rozhlas	k1gInSc6	rozhlas
-	-	kIx~	-
Vltava	Vltava	k1gFnSc1	Vltava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g />
.	.	kIx.	.
</s>
<s>
Existencialismus	existencialismus	k1gInSc1	existencialismus
Fenomenologie	fenomenologie	k1gFnSc2	fenomenologie
Filosofie	filosofie	k1gFnSc2	filosofie
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Seznam	seznam	k1gInSc1	seznam
francouzských	francouzský	k2eAgInPc2d1	francouzský
spisovatelů	spisovatel	k1gMnPc2	spisovatel
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jean-Paul	Jean-Paula	k1gFnPc2	Jean-Paula
Sartre	Sartr	k1gInSc5	Sartr
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Jean-Paul	Jean-Paul	k1gInSc1	Jean-Paul
Sartre	Sartr	k1gInSc5	Sartr
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Jean-Paul	Jean-Paul	k1gInSc1	Jean-Paul
Sartre	Sartr	k1gInSc5	Sartr
SARTRE	SARTRE	kA	SARTRE
<g/>
:	:	kIx,	:
CITÁTY-Z	CITÁTY-Z	k1gMnSc1	CITÁTY-Z
DÍLA-UKÁZKA	DÍLA-UKÁZKA	k1gMnSc1	DÍLA-UKÁZKA
Jean-Paul	Jean-Paul	k1gInSc4	Jean-Paul
Sartre	Sartr	k1gInSc5	Sartr
-	-	kIx~	-
životopis	životopis	k1gInSc4	životopis
podrobnější	podrobný	k2eAgInSc4d2	podrobnější
životopis	životopis	k1gInSc4	životopis
A.	A.	kA	A.
J.	J.	kA	J.
Liehm	Liehm	k1gMnSc1	Liehm
<g/>
:	:	kIx,	:
Jean-Paul	Jean-Paul	k1gInSc1	Jean-Paul
Sartre	Sartr	k1gInSc5	Sartr
stoletý	stoletý	k2eAgMnSc1d1	stoletý
</s>
