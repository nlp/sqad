<p>
<s>
Silmarillion	Silmarillion	k1gInSc1	Silmarillion
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
trojice	trojice	k1gFnSc1	trojice
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
knih	kniha	k1gFnPc2	kniha
britského	britský	k2eAgMnSc2d1	britský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Johna	John	k1gMnSc2	John
Ronalda	Ronald	k1gMnSc2	Ronald
Reuela	Reuel	k1gMnSc2	Reuel
Tolkiena	Tolkien	k1gMnSc2	Tolkien
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
výňatky	výňatek	k1gInPc4	výňatek
z	z	k7c2	z
archetypálních	archetypální	k2eAgFnPc2d1	archetypální
dějin	dějiny	k1gFnPc2	dějiny
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
zasazených	zasazený	k2eAgFnPc2d1	zasazená
zde	zde	k6eAd1	zde
do	do	k7c2	do
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
světa	svět	k1gInSc2	svět
Ardy	Arda	k1gFnSc2	Arda
a	a	k8xC	a
zejména	zejména	k9	zejména
jeho	jeho	k3xOp3gInSc2	jeho
kontinentu	kontinent	k1gInSc2	kontinent
Středozemě	Středozem	k1gFnSc2	Středozem
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
základní	základní	k2eAgFnPc4d1	základní
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
světě	svět	k1gInSc6	svět
jeho	jeho	k3xOp3gInPc2	jeho
příběhů	příběh	k1gInPc2	příběh
a	a	k8xC	a
dějiny	dějiny	k1gFnPc4	dějiny
jeho	jeho	k3xOp3gNnPc2	jeho
nejstarších	starý	k2eAgNnPc2d3	nejstarší
období	období	k1gNnPc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
knihu	kniha	k1gFnSc4	kniha
obsahující	obsahující	k2eAgFnSc4d1	obsahující
spoustu	spousta	k1gFnSc4	spousta
jmen	jméno	k1gNnPc2	jméno
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
neklade	klást	k5eNaImIp3nS	klást
hlavní	hlavní	k2eAgInSc4d1	hlavní
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
příběh	příběh	k1gInSc4	příběh
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
spíše	spíše	k9	spíše
charakter	charakter	k1gInSc4	charakter
kroniky	kronika	k1gFnSc2	kronika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osudy	osud	k1gInPc1	osud
knihy	kniha	k1gFnSc2	kniha
==	==	k?	==
</s>
</p>
<p>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
čtenáři	čtenář	k1gMnPc1	čtenář
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
knihou	kniha	k1gFnSc7	kniha
seznámili	seznámit	k5eAaPmAgMnP	seznámit
až	až	k6eAd1	až
po	po	k7c6	po
autorově	autorův	k2eAgFnSc6d1	autorova
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
příběhy	příběh	k1gInPc4	příběh
a	a	k8xC	a
myšlenky	myšlenka	k1gFnPc4	myšlenka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
jsou	být	k5eAaImIp3nP	být
základem	základ	k1gInSc7	základ
zbývajících	zbývající	k2eAgFnPc2d1	zbývající
dvou	dva	k4xCgFnPc2	dva
jeho	jeho	k3xOp3gNnPc2	jeho
slavných	slavný	k2eAgNnPc2d1	slavné
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
Hobita	hobit	k1gMnSc4	hobit
a	a	k8xC	a
Pána	pán	k1gMnSc4	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
obrysy	obrys	k1gInPc1	obrys
světa	svět	k1gInSc2	svět
Středozemě	Středozem	k1gFnSc2	Středozem
začaly	začít	k5eAaPmAgFnP	začít
vznikat	vznikat	k5eAaImF	vznikat
v	v	k7c6	v
Tolkienově	Tolkienův	k2eAgFnSc6d1	Tolkienova
mysli	mysl	k1gFnSc6	mysl
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ležel	ležet	k5eAaImAgMnS	ležet
s	s	k7c7	s
onemocněním	onemocnění	k1gNnSc7	onemocnění
ve	v	k7c6	v
vojenské	vojenský	k2eAgFnSc6d1	vojenská
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
světa	svět	k1gInSc2	svět
poznali	poznat	k5eAaPmAgMnP	poznat
čtenáři	čtenář	k1gMnPc1	čtenář
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Hobit	hobit	k1gMnSc1	hobit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejím	její	k3xOp3gInSc6	její
velkém	velký	k2eAgInSc6d1	velký
úspěchu	úspěch	k1gInSc6	úspěch
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
Tolkien	Tolkien	k1gInSc4	Tolkien
vydavatelům	vydavatel	k1gMnPc3	vydavatel
Silmarillion	Silmarillion	k1gInSc4	Silmarillion
k	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
však	však	k9	však
odmítnut	odmítnout	k5eAaPmNgInS	odmítnout
a	a	k8xC	a
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
vyzván	vyzvat	k5eAaPmNgMnS	vyzvat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
napsal	napsat	k5eAaPmAgMnS	napsat
pokračování	pokračování	k1gNnSc4	pokračování
Hobita	hobit	k1gMnSc2	hobit
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
vznikl	vzniknout	k5eAaPmAgMnS	vzniknout
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
,	,	kIx,	,
do	do	k7c2	do
nějž	jenž	k3xRgMnSc4	jenž
Tolkien	Tolkien	k1gInSc4	Tolkien
opět	opět	k6eAd1	opět
umístil	umístit	k5eAaPmAgMnS	umístit
mnoho	mnoho	k6eAd1	mnoho
z	z	k7c2	z
příběhů	příběh	k1gInPc2	příběh
Silmarillionu	Silmarillion	k1gInSc2	Silmarillion
jako	jako	k8xS	jako
odkazy	odkaz	k1gInPc4	odkaz
na	na	k7c4	na
starší	starý	k2eAgFnSc4d2	starší
minulost	minulost	k1gFnSc4	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
Pána	pán	k1gMnSc2	pán
prstenů	prsten	k1gInPc2	prsten
se	se	k3xPyFc4	se
k	k	k7c3	k
Silmarillionu	Silmarillion	k1gInSc3	Silmarillion
opět	opět	k6eAd1	opět
vrátil	vrátit	k5eAaPmAgInS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Nesoustředil	soustředit	k5eNaPmAgInS	soustředit
se	se	k3xPyFc4	se
však	však	k9	však
tolik	tolik	k6eAd1	tolik
na	na	k7c4	na
příběhy	příběh	k1gInPc4	příběh
jako	jako	k8xC	jako
na	na	k7c4	na
podání	podání	k1gNnSc4	podání
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
pozadí	pozadí	k1gNnSc6	pozadí
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInPc1	jeho
příběhy	příběh	k1gInPc1	příběh
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
vytvoření	vytvoření	k1gNnSc1	vytvoření
jeho	jeho	k3xOp3gFnSc2	jeho
ucelené	ucelený	k2eAgFnSc2d1	ucelená
koncepce	koncepce	k1gFnSc2	koncepce
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
proto	proto	k8xC	proto
knihu	kniha	k1gFnSc4	kniha
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
do	do	k7c2	do
ucelené	ucelený	k2eAgFnSc2d1	ucelená
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Christopher	Christophra	k1gFnPc2	Christophra
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
ujal	ujmout	k5eAaPmAgInS	ujmout
nedokončeného	dokončený	k2eNgNnSc2d1	nedokončené
díla	dílo	k1gNnSc2	dílo
a	a	k8xC	a
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
otcových	otcův	k2eAgInPc2d1	otcův
zápisků	zápisek	k1gInPc2	zápisek
dal	dát	k5eAaPmAgMnS	dát
dohromady	dohromady	k6eAd1	dohromady
knihu	kniha	k1gFnSc4	kniha
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ji	on	k3xPp3gFnSc4	on
známe	znát	k5eAaImIp1nP	znát
dnes	dnes	k6eAd1	dnes
(	(	kIx(	(
<g/>
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příběh	příběh	k1gInSc1	příběh
==	==	k?	==
</s>
</p>
<p>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
celá	celý	k2eAgFnSc1d1	celá
kniha	kniha	k1gFnSc1	kniha
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Silmarillion	Silmarillion	k1gInSc4	Silmarillion
<g/>
,	,	kIx,	,
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
pěti	pět	k4xCc2	pět
oddílů	oddíl	k1gInPc2	oddíl
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
název	název	k1gInSc1	název
Silmarillion	Silmarillion	k1gInSc1	Silmarillion
nese	nést	k5eAaImIp3nS	nést
jen	jen	k9	jen
jeden	jeden	k4xCgInSc1	jeden
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jen	jen	k9	jen
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
přímo	přímo	k6eAd1	přímo
historií	historie	k1gFnSc7	historie
silmarilů	silmaril	k1gInPc2	silmaril
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
oddíly	oddíl	k1gInPc1	oddíl
popisují	popisovat	k5eAaImIp3nP	popisovat
dějiny	dějiny	k1gFnPc4	dějiny
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
do	do	k7c2	do
nějž	jenž	k3xRgInSc2	jenž
jsou	být	k5eAaImIp3nP	být
Tolkienovy	Tolkienův	k2eAgInPc1d1	Tolkienův
příběhy	příběh	k1gInPc1	příběh
zasazeny	zasazen	k2eAgInPc1d1	zasazen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ainulindalë	Ainulindalë	k1gMnPc5	Ainulindalë
===	===	k?	===
</s>
</p>
<p>
<s>
Ainulindale	Ainulindale	k6eAd1	Ainulindale
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
úvodem	úvod	k1gInSc7	úvod
do	do	k7c2	do
světa	svět	k1gInSc2	svět
Tolkienových	Tolkienův	k2eAgInPc2d1	Tolkienův
příběhů	příběh	k1gInPc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
počátcích	počátek	k1gInPc6	počátek
světa	svět	k1gInSc2	svět
a	a	k8xC	a
původu	původ	k1gInSc2	původ
bytostí	bytost	k1gFnPc2	bytost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jej	on	k3xPp3gMnSc4	on
obývají	obývat	k5eAaImIp3nP	obývat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
byl	být	k5eAaImAgMnS	být
Eru	Eru	k1gFnSc4	Eru
či	či	k8xC	či
Ilúvatar	Ilúvatar	k1gInSc4	Ilúvatar
(	(	kIx(	(
<g/>
jména	jméno	k1gNnSc2	jméno
jsou	být	k5eAaImIp3nP	být
elfská	elfskat	k5eAaPmIp3nS	elfskat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
přebýval	přebývat	k5eAaImAgMnS	přebývat
v	v	k7c6	v
Prázdnu	prázdno	k1gNnSc6	prázdno
<g/>
,	,	kIx,	,
z	z	k7c2	z
nějž	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
vynořil	vynořit	k5eAaPmAgMnS	vynořit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Ilúvatara	Ilúvatar	k1gMnSc2	Ilúvatar
vycházely	vycházet	k5eAaImAgFnP	vycházet
myšlenky	myšlenka	k1gFnPc1	myšlenka
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgFnPc3	jenž
dal	dát	k5eAaPmAgInS	dát
věčný	věčný	k2eAgInSc4d1	věčný
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
povstali	povstat	k5eAaPmAgMnP	povstat
Ainur	Ainur	k1gMnSc1	Ainur
<g/>
,	,	kIx,	,
Svatí	svatý	k1gMnPc1	svatý
<g/>
,	,	kIx,	,
duchovní	duchovní	k2eAgFnPc1d1	duchovní
bytosti	bytost	k1gFnPc1	bytost
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něž	jenž	k3xRgInPc4	jenž
vybudoval	vybudovat	k5eAaPmAgInS	vybudovat
Ilúvatar	Ilúvatar	k1gInSc1	Ilúvatar
Bezčasé	bezčasý	k2eAgFnSc2d1	bezčasá
síně	síň	k1gFnSc2	síň
<g/>
.	.	kIx.	.
</s>
<s>
Ainur	Ainur	k1gMnSc1	Ainur
byli	být	k5eAaImAgMnP	být
obdařeni	obdařit	k5eAaPmNgMnP	obdařit
mocným	mocný	k2eAgInSc7d1	mocný
hlasem	hlas	k1gInSc7	hlas
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
před	před	k7c7	před
Ilúvaterem	Ilúvater	k1gMnSc7	Ilúvater
zpívali	zpívat	k5eAaImAgMnP	zpívat
podle	podle	k7c2	podle
námětů	námět	k1gInPc2	námět
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jim	on	k3xPp3gMnPc3	on
předkládal	předkládat	k5eAaImAgMnS	předkládat
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
povstala	povstat	k5eAaPmAgFnS	povstat
Hudba	hudba	k1gFnSc1	hudba
Ainur	Ainura	k1gFnPc2	Ainura
<g/>
.	.	kIx.	.
</s>
<s>
Ilúvatar	Ilúvatar	k1gInSc1	Ilúvatar
tuto	tento	k3xDgFnSc4	tento
Hudbu	hudba	k1gFnSc4	hudba
vzal	vzít	k5eAaPmAgMnS	vzít
a	a	k8xC	a
zhmotnil	zhmotnit	k5eAaPmAgMnS	zhmotnit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
počátek	počátek	k1gInSc1	počátek
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
nesl	nést	k5eAaImAgMnS	nést
jméno	jméno	k1gNnSc4	jméno
Eä	Eä	k1gFnSc2	Eä
(	(	kIx(	(
<g/>
Jsoucí	jsoucí	k2eAgFnSc1d1	jsoucí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
elfy	elf	k1gMnPc4	elf
později	pozdě	k6eAd2	pozdě
pojmenovaný	pojmenovaný	k2eAgMnSc1d1	pojmenovaný
Arda	Arda	k1gMnSc1	Arda
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
Ainur	Ainur	k1gMnSc1	Ainur
si	se	k3xPyFc3	se
nový	nový	k2eAgInSc4d1	nový
svět	svět	k1gInSc4	svět
zamilovali	zamilovat	k5eAaPmAgMnP	zamilovat
a	a	k8xC	a
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
sestoupí	sestoupit	k5eAaPmIp3nP	sestoupit
a	a	k8xC	a
budou	být	k5eAaImBp3nP	být
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
přebývat	přebývat	k5eAaImF	přebývat
<g/>
;	;	kIx,	;
ti	ten	k3xDgMnPc1	ten
nejmocnější	mocný	k2eAgMnPc1d3	nejmocnější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byli	být	k5eAaImAgMnP	být
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
známi	znám	k2eAgMnPc1d1	znám
jako	jako	k8xS	jako
Valar	Valar	k1gMnSc1	Valar
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
méně	málo	k6eAd2	málo
mocní	mocnit	k5eAaImIp3nP	mocnit
jako	jako	k8xC	jako
Maiar	Maiar	k1gInSc4	Maiar
<g/>
.	.	kIx.	.
</s>
<s>
Moc	moc	k1gFnSc4	moc
jich	on	k3xPp3gFnPc2	on
všech	všecek	k3xTgFnPc2	všecek
byla	být	k5eAaImAgFnS	být
omezena	omezen	k2eAgFnSc1d1	omezena
podmínkami	podmínka	k1gFnPc7	podmínka
světa	svět	k1gInSc2	svět
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
vzali	vzít	k5eAaPmAgMnP	vzít
pozemskou	pozemský	k2eAgFnSc4d1	pozemská
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
předpovězeno	předpovězen	k2eAgNnSc1d1	předpovězeno
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
Konci	konec	k1gInSc6	konec
světa	svět	k1gInSc2	svět
se	se	k3xPyFc4	se
Valar	Valar	k1gInSc1	Valar
a	a	k8xC	a
Maiar	Maiar	k1gInSc1	Maiar
vrátí	vrátit	k5eAaPmIp3nS	vrátit
do	do	k7c2	do
Bezčasých	bezčasý	k2eAgFnPc2d1	bezčasá
síní	síň	k1gFnPc2	síň
a	a	k8xC	a
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
Ilúvatarovy	Ilúvatarův	k2eAgFnPc1d1	Ilúvatarův
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
myslící	myslící	k2eAgFnPc1d1	myslící
bytosti	bytost	k1gFnPc1	bytost
<g/>
,	,	kIx,	,
stvořené	stvořený	k2eAgNnSc1d1	stvořené
až	až	k9	až
na	na	k7c6	na
Ardě	Ard	k1gInSc6	Ard
<g/>
,	,	kIx,	,
a	a	k8xC	a
pak	pak	k6eAd1	pak
bude	být	k5eAaImBp3nS	být
opět	opět	k6eAd1	opět
znít	znít	k5eAaImF	znít
Hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
obohacená	obohacený	k2eAgFnSc1d1	obohacená
o	o	k7c4	o
osudy	osud	k1gInPc4	osud
světa	svět	k1gInSc2	svět
a	a	k8xC	a
bez	bez	k7c2	bez
vady	vada	k1gFnSc2	vada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Valaquenta	Valaquento	k1gNnSc2	Valaquento
===	===	k?	===
</s>
</p>
<p>
<s>
Valaquenta	Valaquenta	k1gFnSc1	Valaquenta
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
žádný	žádný	k3yNgInSc4	žádný
příběh	příběh	k1gInSc4	příběh
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vyjmenováním	vyjmenování	k1gNnSc7	vyjmenování
a	a	k8xC	a
popisem	popis	k1gInSc7	popis
těch	ten	k3xDgFnPc2	ten
Ainur	Ainura	k1gFnPc2	Ainura
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
sestoupili	sestoupit	k5eAaPmAgMnP	sestoupit
na	na	k7c4	na
Ardu	Arda	k1gFnSc4	Arda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejprve	nejprve	k6eAd1	nejprve
je	být	k5eAaImIp3nS	být
věnována	věnován	k2eAgFnSc1d1	věnována
pozornost	pozornost	k1gFnSc1	pozornost
sedmi	sedm	k4xCc2	sedm
Valar	Valara	k1gFnPc2	Valara
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
<g/>
:	:	kIx,	:
Manwë	Manwë	k1gMnSc1	Manwë
<g/>
,	,	kIx,	,
pán	pán	k1gMnSc1	pán
Ardy	Arda	k1gFnSc2	Arda
<g/>
,	,	kIx,	,
miluje	milovat	k5eAaImIp3nS	milovat
vzduch	vzduch	k1gInSc1	vzduch
(	(	kIx(	(
<g/>
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
všechny	všechen	k3xTgInPc4	všechen
zvuky	zvuk	k1gInPc4	zvuk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
též	též	k9	též
zván	zván	k2eAgMnSc1d1	zván
Pán	pán	k1gMnSc1	pán
větru	vítr	k1gInSc2	vítr
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nP	patřit
mu	on	k3xPp3gInSc3	on
všichni	všechen	k3xTgMnPc1	všechen
ptáci	pták	k1gMnPc1	pták
<g/>
;	;	kIx,	;
Ulmo	Ulmo	k1gMnSc1	Ulmo
<g/>
,	,	kIx,	,
pán	pán	k1gMnSc1	pán
vod	voda	k1gFnPc2	voda
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
přebývá	přebývat	k5eAaImIp3nS	přebývat
<g/>
,	,	kIx,	,
vody	voda	k1gFnSc2	voda
<g />
.	.	kIx.	.
</s>
<s>
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
donesou	donést	k5eAaPmIp3nP	donést
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
na	na	k7c6	na
světě	svět	k1gInSc6	svět
udá	udat	k5eAaPmIp3nS	udat
<g/>
;	;	kIx,	;
Aulë	Aulë	k1gMnSc1	Aulë
<g/>
,	,	kIx,	,
Tvůrce	tvůrce	k1gMnSc1	tvůrce
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
mistr	mistr	k1gMnSc1	mistr
všech	všecek	k3xTgNnPc2	všecek
řemesel	řemeslo	k1gNnPc2	řemeslo
<g/>
,	,	kIx,	,
vynálezce	vynálezce	k1gMnSc1	vynálezce
kovů	kov	k1gInPc2	kov
a	a	k8xC	a
drahokamů	drahokam	k1gInPc2	drahokam
<g/>
;	;	kIx,	;
Námo	Námo	k1gMnSc1	Námo
<g/>
,	,	kIx,	,
Hlasatel	hlasatel	k1gMnSc1	hlasatel
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
podle	podle	k7c2	podle
svého	svůj	k3xOyFgNnSc2	svůj
sídla	sídlo	k1gNnSc2	sídlo
zvaný	zvaný	k2eAgMnSc1d1	zvaný
Mandos	Mandos	k1gMnSc1	Mandos
<g/>
,	,	kIx,	,
pán	pán	k1gMnSc1	pán
duchů	duch	k1gMnPc2	duch
a	a	k8xC	a
sudby	sudba	k1gFnSc2	sudba
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
sídle	sídlo	k1gNnSc6	sídlo
v	v	k7c6	v
Síních	síň	k1gFnPc6	síň
čekání	čekání	k1gNnSc2	čekání
přebývají	přebývat	k5eAaImIp3nP	přebývat
duše	duše	k1gFnPc1	duše
zabitých	zabitý	k2eAgMnPc2d1	zabitý
elfů	elf	k1gMnPc2	elf
<g/>
;	;	kIx,	;
Oromë	Oromë	k1gMnSc1	Oromë
<g/>
,	,	kIx,	,
lovec	lovec	k1gMnSc1	lovec
a	a	k8xC	a
krotitel	krotitel	k1gMnSc1	krotitel
zvěře	zvěř	k1gFnSc2	zvěř
<g/>
,	,	kIx,	,
vyjíždí	vyjíždět	k5eAaImIp3nS	vyjíždět
na	na	k7c4	na
lov	lov	k1gInSc4	lov
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Středozemi	Středozem	k1gFnSc6	Středozem
<g/>
;	;	kIx,	;
Tulkas	Tulkas	k1gInSc1	Tulkas
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
přezdívku	přezdívka	k1gFnSc4	přezdívka
Zápasník	zápasník	k1gMnSc1	zápasník
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
silný	silný	k2eAgInSc1d1	silný
a	a	k8xC	a
rychlý	rychlý	k2eAgInSc1d1	rychlý
<g/>
;	;	kIx,	;
Irmo	Irma	k1gFnSc5	Irma
<g/>
,	,	kIx,	,
pán	pán	k1gMnSc1	pán
snů	sen	k1gInPc2	sen
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgMnSc1d1	zvaný
podle	podle	k7c2	podle
svého	svůj	k3xOyFgNnSc2	svůj
sídla	sídlo	k1gNnSc2	sídlo
též	též	k9	též
Lórien	Lórien	k1gInSc4	Lórien
<g/>
;	;	kIx,	;
mezi	mezi	k7c4	mezi
Valar	Valar	k1gInSc4	Valar
byl	být	k5eAaImAgMnS	být
původně	původně	k6eAd1	původně
i	i	k9	i
Melkor	Melkor	k1gMnSc1	Melkor
<g/>
,	,	kIx,	,
nejmocnější	mocný	k2eAgMnSc1d3	nejmocnější
z	z	k7c2	z
Ainur	Ainura	k1gFnPc2	Ainura
<g/>
,	,	kIx,	,
obrátil	obrátit	k5eAaPmAgMnS	obrátit
se	se	k3xPyFc4	se
však	však	k9	však
ke	k	k7c3	k
zlu	zlo	k1gNnSc3	zlo
a	a	k8xC	a
nebyl	být	k5eNaImAgInS	být
dále	daleko	k6eAd2	daleko
počítán	počítat	k5eAaImNgInS	počítat
mezi	mezi	k7c4	mezi
Valar	Valar	k1gInSc4	Valar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Valar	Valar	k1gInSc4	Valar
mají	mít	k5eAaImIp3nP	mít
manželky	manželka	k1gFnPc4	manželka
<g/>
,	,	kIx,	,
duchy	duch	k1gMnPc4	duch
ženského	ženský	k2eAgInSc2d1	ženský
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
kterých	který	k3yRgMnPc2	který
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
sedm	sedm	k4xCc1	sedm
<g/>
,	,	kIx,	,
nazývají	nazývat	k5eAaImIp3nP	nazývat
se	se	k3xPyFc4	se
Valier	Valier	k1gInSc4	Valier
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
<g/>
:	:	kIx,	:
Varda	Varda	k1gFnSc1	Varda
<g/>
,	,	kIx,	,
královna	královna	k1gFnSc1	královna
Valar	Valar	k1gInSc1	Valar
<g/>
,	,	kIx,	,
paní	paní	k1gFnSc1	paní
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
duch	duch	k1gMnSc1	duch
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
nejmilejší	milý	k2eAgMnSc1d3	nejmilejší
Valar	Valar	k1gMnSc1	Valar
elfů	elf	k1gMnPc2	elf
<g/>
;	;	kIx,	;
Yavanna	Yavanna	k1gFnSc1	Yavanna
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
Aulëho	Aulë	k1gMnSc2	Aulë
<g/>
,	,	kIx,	,
živitelka	živitelka	k1gFnSc1	živitelka
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
dárkyně	dárkyně	k1gFnSc1	dárkyně
plodů	plod	k1gInPc2	plod
<g/>
,	,	kIx,	,
tvůrkyně	tvůrkyně	k1gFnSc1	tvůrkyně
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
ochránkyně	ochránkyně	k1gFnSc2	ochránkyně
rostoucích	rostoucí	k2eAgFnPc2d1	rostoucí
věcí	věc	k1gFnPc2	věc
<g/>
;	;	kIx,	;
Nienna	Nienna	k1gFnSc1	Nienna
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
její	její	k3xOp3gFnSc7	její
doménou	doména	k1gFnSc7	doména
jsou	být	k5eAaImIp3nP	být
hoře	hoře	k6eAd1	hoře
<g/>
,	,	kIx,	,
slitování	slitování	k1gNnPc4	slitování
<g/>
,	,	kIx,	,
přináší	přinášet	k5eAaImIp3nS	přinášet
moudrost	moudrost	k1gFnSc1	moudrost
a	a	k8xC	a
trpělivost	trpělivost	k1gFnSc1	trpělivost
ke	k	k7c3	k
snášení	snášení	k1gNnSc3	snášení
života	život	k1gInSc2	život
<g/>
;	;	kIx,	;
Vána	ván	k2eAgFnSc1d1	vána
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
Oromëho	Oromë	k1gMnSc2	Oromë
<g/>
,	,	kIx,	,
paní	paní	k1gFnSc2	paní
věčného	věčný	k2eAgNnSc2d1	věčné
mládí	mládí	k1gNnSc2	mládí
<g/>
,	,	kIx,	,
miluje	milovat	k5eAaImIp3nS	milovat
ptačí	ptačí	k2eAgInSc4d1	ptačí
zpěv	zpěv	k1gInSc4	zpěv
a	a	k8xC	a
květy	květ	k1gInPc4	květ
<g/>
;	;	kIx,	;
Nessa	Nessa	k1gFnSc1	Nessa
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
Tulkasova	Tulkasův	k2eAgFnSc1d1	Tulkasův
<g/>
,	,	kIx,	,
miluje	milovat	k5eAaImIp3nS	milovat
lesní	lesní	k2eAgMnPc4d1	lesní
tvory	tvor	k1gMnPc4	tvor
a	a	k8xC	a
tanec	tanec	k1gInSc4	tanec
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
stále	stále	k6eAd1	stále
tančí	tančit	k5eAaImIp3nS	tančit
v	v	k7c6	v
sídle	sídlo	k1gNnSc6	sídlo
Valar	Valara	k1gFnPc2	Valara
<g/>
;	;	kIx,	;
Estë	Estë	k1gFnSc1	Estë
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
Irma	Irma	k1gFnSc1	Irma
<g/>
,	,	kIx,	,
léčitelka	léčitelka	k1gFnSc1	léčitelka
ran	rána	k1gFnPc2	rána
a	a	k8xC	a
únavy	únava	k1gFnSc2	únava
<g/>
,	,	kIx,	,
dává	dávat	k5eAaImIp3nS	dávat
odpočinek	odpočinek	k1gInSc4	odpočinek
a	a	k8xC	a
úlevu	úleva	k1gFnSc4	úleva
od	od	k7c2	od
břemen	břemeno	k1gNnPc2	břemeno
světa	svět	k1gInSc2	svět
<g/>
;	;	kIx,	;
Vairë	Vairë	k1gFnSc1	Vairë
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
Námova	Námův	k2eAgFnSc1d1	Námův
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Tkadlena	tkadlena	k1gFnSc1	tkadlena
<g/>
,	,	kIx,	,
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
stavu	stav	k1gInSc6	stav
tká	tkát	k5eAaImIp3nS	tkát
události	událost	k1gFnPc4	událost
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
nastanou	nastat	k5eAaPmIp3nP	nastat
v	v	k7c6	v
čase	čas	k1gInSc6	čas
a	a	k8xC	a
utkanými	utkaný	k2eAgFnPc7d1	utkaná
sítěmi	síť	k1gFnPc7	síť
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
síně	síň	k1gFnSc2	síň
Mandosu	Mandos	k1gInSc2	Mandos
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Duchové	duch	k1gMnPc1	duch
relativně	relativně	k6eAd1	relativně
nižší	nízký	k2eAgFnSc2d2	nižší
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
přišlí	přišlý	k2eAgMnPc1d1	přišlý
do	do	k7c2	do
Ardy	Arda	k1gFnSc2	Arda
<g/>
,	,	kIx,	,
nesou	nést	k5eAaImIp3nP	nést
jméno	jméno	k1gNnSc4	jméno
Maiar	Maiara	k1gFnPc2	Maiara
<g/>
,	,	kIx,	,
stejného	stejný	k2eAgInSc2d1	stejný
řádu	řád	k1gInSc2	řád
jako	jako	k8xS	jako
Valar	Valar	k1gMnSc1	Valar
(	(	kIx(	(
<g/>
Ainur	Ainur	k1gMnSc1	Ainur
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jen	jen	k9	jen
nižšího	nízký	k2eAgInSc2d2	nižší
stupně	stupeň	k1gInSc2	stupeň
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
mocí	moc	k1gFnSc7	moc
malí	malý	k2eAgMnPc1d1	malý
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
téměř	téměř	k6eAd1	téměř
tak	tak	k6eAd1	tak
velcí	velký	k2eAgMnPc1d1	velký
jako	jako	k8xS	jako
Valar	Valar	k1gMnSc1	Valar
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
sloužili	sloužit	k5eAaImAgMnP	sloužit
a	a	k8xC	a
pomáhali	pomáhat	k5eAaImAgMnP	pomáhat
Valar	Valar	k1gInSc4	Valar
<g/>
,	,	kIx,	,
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
obyvateli	obyvatel	k1gMnPc7	obyvatel
světa	svět	k1gInSc2	svět
měli	mít	k5eAaImAgMnP	mít
málo	málo	k6eAd1	málo
společného	společný	k2eAgNnSc2d1	společné
a	a	k8xC	a
jen	jen	k9	jen
málo	málo	k6eAd1	málo
jich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnSc4	on
například	například	k6eAd1	například
Eönwë	Eönwë	k1gMnSc1	Eönwë
<g/>
,	,	kIx,	,
hlasatel	hlasatel	k1gMnSc1	hlasatel
Manweho	Manwe	k1gMnSc2	Manwe
<g/>
,	,	kIx,	,
učitel	učitel	k1gMnSc1	učitel
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
silou	síla	k1gFnSc7	síla
zbraně	zbraň	k1gFnSc2	zbraň
největší	veliký	k2eAgFnSc2d3	veliký
na	na	k7c6	na
Ardě	Arda	k1gFnSc6	Arda
<g/>
,	,	kIx,	,
Arien	Arien	k1gInSc1	Arien
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
řídí	řídit	k5eAaImIp3nS	řídit
let	let	k1gInSc4	let
Slunce	slunce	k1gNnSc2	slunce
s	s	k7c7	s
žárem	žár	k1gInSc7	žár
očí	oko	k1gNnPc2	oko
příliš	příliš	k6eAd1	příliš
silným	silný	k2eAgInSc7d1	silný
i	i	k9	i
pro	pro	k7c4	pro
Eldar	Eldar	k1gInSc4	Eldar
<g/>
,	,	kIx,	,
Tilion	Tilion	k1gInSc4	Tilion
<g/>
,	,	kIx,	,
řídící	řídící	k2eAgInSc4d1	řídící
běh	běh	k1gInSc4	běh
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
stříbrný	stříbrný	k2eAgInSc4d1	stříbrný
luk	luk	k1gInSc4	luk
<g/>
,	,	kIx,	,
Ossë	Ossë	k1gFnSc4	Ossë
Uinen	Uinno	k1gNnPc2	Uinno
<g/>
,	,	kIx,	,
manželé	manžel	k1gMnPc1	manžel
a	a	k8xC	a
vládci	vládce	k1gMnPc1	vládce
okružních	okružní	k2eAgNnPc2d1	okružní
moří	moře	k1gNnPc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Melian	Meliany	k1gInPc2	Meliany
přišla	přijít	k5eAaPmAgFnS	přijít
do	do	k7c2	do
Středozemě	Středozem	k1gFnSc2	Středozem
a	a	k8xC	a
provdala	provdat	k5eAaPmAgFnS	provdat
se	se	k3xPyFc4	se
za	za	k7c4	za
elfa	elf	k1gMnSc4	elf
Thingola	Thingola	k1gFnSc1	Thingola
<g/>
,	,	kIx,	,
a	a	k8xC	a
mezi	mezi	k7c7	mezi
Maiar	Maiara	k1gFnPc2	Maiara
patřilo	patřit	k5eAaImAgNnS	patřit
i	i	k9	i
pět	pět	k4xCc1	pět
čarodějů	čaroděj	k1gMnPc2	čaroděj
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
přišli	přijít	k5eAaPmAgMnP	přijít
do	do	k7c2	do
Středozemě	Středozem	k1gFnSc2	Středozem
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
Maiar	Maiar	k1gInSc4	Maiar
byli	být	k5eAaImAgMnP	být
zkaženi	zkazit	k5eAaPmNgMnP	zkazit
Melkorem	Melkor	k1gMnSc7	Melkor
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
velcí	velký	k2eAgMnPc1d1	velký
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Mairon	Mairon	k1gInSc1	Mairon
<g/>
,	,	kIx,	,
Obdivuhodný	obdivuhodný	k2eAgInSc1d1	obdivuhodný
(	(	kIx(	(
<g/>
Sauron	Sauron	k1gInSc1	Sauron
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
velký	velký	k2eAgMnSc1d1	velký
řemeslník	řemeslník	k1gMnSc1	řemeslník
z	z	k7c2	z
Aulëho	Aulë	k1gMnSc2	Aulë
lidu	lid	k1gInSc2	lid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
měl	mít	k5eAaImAgMnS	mít
podíl	podíl	k1gInSc4	podíl
ve	v	k7c6	v
všem	všecek	k3xTgNnSc6	všecek
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
kdy	kdy	k6eAd1	kdy
Melkor	Melkor	k1gMnSc1	Melkor
udělal	udělat	k5eAaPmAgMnS	udělat
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
pádu	pád	k1gInSc6	pád
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
dosadil	dosadit	k5eAaPmAgMnS	dosadit
na	na	k7c4	na
stolec	stolec	k1gInSc4	stolec
Temného	temný	k2eAgMnSc2d1	temný
pána	pán	k1gMnSc2	pán
<g/>
,	,	kIx,	,
v	v	k7c6	v
počátku	počátek	k1gInSc6	počátek
menšího	malý	k2eAgMnSc2d2	menší
ducha	duch	k1gMnSc2	duch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
celkově	celkově	k6eAd1	celkově
efektivnějšího	efektivní	k2eAgNnSc2d2	efektivnější
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc4d1	další
<g/>
,	,	kIx,	,
o	o	k7c4	o
něco	něco	k3yInSc4	něco
slabší	slabý	k2eAgMnPc1d2	slabší
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
balrogové	balrog	k1gMnPc1	balrog
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
nejmenší	malý	k2eAgMnSc1d3	nejmenší
<g/>
,	,	kIx,	,
a	a	k8xC	a
ti	ten	k3xDgMnPc1	ten
si	se	k3xPyFc3	se
vzali	vzít	k5eAaPmAgMnP	vzít
podobu	podoba	k1gFnSc4	podoba
prvních	první	k4xOgMnPc2	první
skřetů	skřet	k1gMnPc2	skřet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Quenta	Quent	k1gInSc2	Quent
Silmarillion	Silmarillion	k1gInSc1	Silmarillion
===	===	k?	===
</s>
</p>
<p>
<s>
Quenta	Quenta	k1gFnSc1	Quenta
Silmarillion	Silmarillion	k1gInSc1	Silmarillion
(	(	kIx(	(
<g/>
Příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
Silmarilech	Silmaril	k1gInPc6	Silmaril
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nP	tvořit
jádro	jádro	k1gNnSc4	jádro
knihy	kniha	k1gFnSc2	kniha
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
nejrozsáhlejší	rozsáhlý	k2eAgFnSc1d3	nejrozsáhlejší
část	část	k1gFnSc1	část
<g/>
.	.	kIx.	.
</s>
<s>
Popisuje	popisovat	k5eAaImIp3nS	popisovat
dějiny	dějiny	k1gFnPc4	dějiny
Ardy	Ard	k2eAgFnPc4d1	Ard
po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
Valar	Valara	k1gFnPc2	Valara
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc4	svůj
pozornost	pozornost	k1gFnSc4	pozornost
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
především	především	k9	především
na	na	k7c4	na
elfy	elf	k1gMnPc4	elf
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc2	jejich
mnoho	mnoho	k4c1	mnoho
staletí	staletí	k1gNnPc2	staletí
trvající	trvající	k2eAgInSc1d1	trvající
boj	boj	k1gInSc1	boj
s	s	k7c7	s
Melkorem	Melkor	k1gInSc7	Melkor
o	o	k7c4	o
klenoty	klenot	k1gInPc4	klenot
silmarily	silmarila	k1gFnSc2	silmarila
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c4	o
stvoření	stvoření	k1gNnSc4	stvoření
světa	svět	k1gInSc2	svět
Ilúvatarem	Ilúvatar	k1gInSc7	Ilúvatar
<g/>
,	,	kIx,	,
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
správy	správa	k1gFnSc2	správa
světa	svět	k1gInSc2	svět
ujali	ujmout	k5eAaPmAgMnP	ujmout
Valar	Valar	k1gMnSc1	Valar
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
sestoupili	sestoupit	k5eAaPmAgMnP	sestoupit
z	z	k7c2	z
nebe	nebe	k1gNnSc2	nebe
<g/>
.	.	kIx.	.
</s>
<s>
Vypráví	vyprávět	k5eAaImIp3nS	vyprávět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
byly	být	k5eAaImAgFnP	být
stvořeny	stvořen	k2eAgFnPc1d1	stvořena
myslící	myslící	k2eAgFnPc1d1	myslící
bytosti	bytost	k1gFnPc1	bytost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jej	on	k3xPp3gMnSc4	on
obývaly	obývat	k5eAaImAgFnP	obývat
–	–	k?	–
elfové	elf	k1gMnPc1	elf
<g/>
,	,	kIx,	,
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
trpaslíci	trpaslík	k1gMnPc1	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
Valar	Valara	k1gFnPc2	Valara
<g/>
,	,	kIx,	,
Melkor	Melkora	k1gFnPc2	Melkora
<g/>
,	,	kIx,	,
zatoužil	zatoužit	k5eAaPmAgInS	zatoužit
mít	mít	k5eAaImF	mít
celou	celý	k2eAgFnSc4d1	celá
zemi	zem	k1gFnSc4	zem
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
obyvatele	obyvatel	k1gMnPc4	obyvatel
světa	svět	k1gInSc2	svět
si	se	k3xPyFc3	se
podrobit	podrobit	k5eAaPmF	podrobit
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
nejzručnější	zručný	k2eAgMnSc1d3	nejzručnější
z	z	k7c2	z
elfů	elf	k1gMnPc2	elf
Fëanor	Fëanor	k1gMnSc1	Fëanor
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
zářící	zářící	k2eAgInPc4d1	zářící
klenoty	klenot	k1gInPc4	klenot
silmarily	silmarila	k1gFnSc2	silmarila
a	a	k8xC	a
jak	jak	k6eAd1	jak
po	po	k7c6	po
nich	on	k3xPp3gFnPc6	on
Melkor	Melkor	k1gInSc4	Melkor
zatoužil	zatoužit	k5eAaPmAgMnS	zatoužit
a	a	k8xC	a
ukradl	ukradnout	k5eAaPmAgMnS	ukradnout
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Fëanor	Fëanor	k1gMnSc1	Fëanor
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
klenoty	klenot	k1gInPc4	klenot
dobýt	dobýt	k5eAaPmF	dobýt
zpět	zpět	k6eAd1	zpět
a	a	k8xC	a
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
elfů	elf	k1gMnPc2	elf
odvedl	odvést	k5eAaPmAgMnS	odvést
do	do	k7c2	do
spoustu	spoustu	k6eAd1	spoustu
věků	věk	k1gInPc2	věk
trvající	trvající	k2eAgFnSc2d1	trvající
války	válka	k1gFnSc2	válka
proti	proti	k7c3	proti
Melkorovi	Melkor	k1gMnSc3	Melkor
<g/>
.	.	kIx.	.
</s>
<s>
Vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
osudech	osud	k1gInPc6	osud
elfů	elf	k1gMnPc2	elf
i	i	k8xC	i
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
oněch	onen	k3xDgInPc6	onen
věcích	věk	k1gInPc6	věk
a	a	k8xC	a
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Valar	Valara	k1gFnPc2	Valara
a	a	k8xC	a
samotného	samotný	k2eAgMnSc2d1	samotný
Ilúvatara	Ilúvatar	k1gMnSc2	Ilúvatar
lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
elfy	elf	k1gMnPc7	elf
nakonec	nakonec	k6eAd1	nakonec
Melkora	Melkor	k1gMnSc4	Melkor
porazili	porazit	k5eAaPmAgMnP	porazit
a	a	k8xC	a
vyhnali	vyhnat	k5eAaPmAgMnP	vyhnat
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Akallabê	Akallabê	k1gMnPc5	Akallabê
===	===	k?	===
</s>
</p>
<p>
<s>
Akallabê	Akallabê	k?	Akallabê
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
Druhém	druhý	k4xOgInSc6	druhý
věku	věk	k1gInSc6	věk
Středozemě	Středozem	k1gFnSc2	Středozem
a	a	k8xC	a
zejména	zejména	k9	zejména
o	o	k7c4	o
Númenoru	Númenora	k1gFnSc4	Númenora
a	a	k8xC	a
Númenorejcích	Númenorejec	k1gMnPc6	Númenorejec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
závěrečných	závěrečný	k2eAgInPc6d1	závěrečný
bojích	boj	k1gInPc6	boj
války	válka	k1gFnSc2	válka
o	o	k7c4	o
silmarily	silmarila	k1gFnPc4	silmarila
stála	stát	k5eAaImAgFnS	stát
většina	většina	k1gFnSc1	většina
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
Melkorově	Melkorův	k2eAgFnSc6d1	Melkorova
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
jen	jen	k9	jen
nemnoho	nemnoho	k6eAd1	nemnoho
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
postavilo	postavit	k5eAaPmAgNnS	postavit
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
elfů	elf	k1gMnPc2	elf
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
lidé	člověk	k1gMnPc1	člověk
byli	být	k5eAaImAgMnP	být
po	po	k7c6	po
Melkorově	Melkorův	k2eAgInSc6d1	Melkorův
pádu	pád	k1gInSc6	pád
odměněni	odměnit	k5eAaPmNgMnP	odměnit
–	–	k?	–
Valar	Valar	k1gInSc1	Valar
jim	on	k3xPp3gMnPc3	on
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nebyla	být	k5eNaImAgFnS	být
součástí	součást	k1gFnSc7	součást
ani	ani	k8xC	ani
Středozemě	Středozem	k1gFnPc4	Středozem
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
Valinoru	Valinora	k1gFnSc4	Valinora
<g/>
;	;	kIx,	;
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
ostrov	ostrov	k1gInSc1	ostrov
uprostřed	uprostřed	k7c2	uprostřed
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Valar	Valara	k1gFnPc2	Valara
přeplavili	přeplavit	k5eAaPmAgMnP	přeplavit
<g/>
.	.	kIx.	.
</s>
<s>
Zemi	zem	k1gFnSc4	zem
nazvali	nazvat	k5eAaPmAgMnP	nazvat
Anadû	Anadû	k1gMnPc1	Anadû
<g/>
,	,	kIx,	,
v	v	k7c6	v
jazyku	jazyk	k1gInSc6	jazyk
vznešených	vznešený	k2eAgMnPc2d1	vznešený
elfů	elf	k1gMnPc2	elf
Númenorë	Númenorë	k1gFnSc2	Númenorë
<g/>
;	;	kIx,	;
odtud	odtud	k6eAd1	odtud
Númenor	Númenor	k1gMnSc1	Númenor
<g/>
,	,	kIx,	,
oni	onen	k3xDgMnPc1	onen
sami	sám	k3xTgMnPc1	sám
proto	proto	k8xC	proto
byli	být	k5eAaImAgMnP	být
zváni	zvát	k5eAaImNgMnP	zvát
Dúnadani	Dúnadan	k1gMnPc1	Dúnadan
nebo	nebo	k8xC	nebo
Númenorejci	Númenorejec	k1gMnPc1	Númenorejec
<g/>
.	.	kIx.	.
</s>
<s>
Valar	Valar	k1gInSc4	Valar
nemohli	moct	k5eNaImAgMnP	moct
lidem	člověk	k1gMnPc3	člověk
odebrat	odebrat	k5eAaPmF	odebrat
Ilúvatarův	Ilúvatarův	k2eAgInSc4d1	Ilúvatarův
dar	dar	k1gInSc4	dar
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gInSc4	jejich
život	život	k1gInSc4	život
prodloužili	prodloužit	k5eAaPmAgMnP	prodloužit
na	na	k7c4	na
několikanásobek	několikanásobek	k1gInSc4	několikanásobek
života	život	k1gInSc2	život
lidí	člověk	k1gMnPc2	člověk
ve	v	k7c6	v
Středozemi	Středozem	k1gFnSc6	Středozem
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
jejich	jejich	k3xOp3gMnSc7	jejich
králem	král	k1gMnSc7	král
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
volbou	volba	k1gFnSc7	volba
Valar	Valara	k1gFnPc2	Valara
Elros	Elrosa	k1gFnPc2	Elrosa
<g/>
,	,	kIx,	,
bratr	bratr	k1gMnSc1	bratr
Elrondův	Elrondův	k2eAgMnSc1d1	Elrondův
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
král	král	k1gMnSc1	král
si	se	k3xPyFc3	se
Elros	Elros	k1gMnSc1	Elros
dal	dát	k5eAaPmAgMnS	dát
elfské	elfský	k2eAgNnSc4d1	elfské
jméno	jméno	k1gNnSc4	jméno
a	a	k8xC	a
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
to	ten	k3xDgNnSc4	ten
opakovali	opakovat	k5eAaImAgMnP	opakovat
všichni	všechen	k3xTgMnPc1	všechen
králové	král	k1gMnPc1	král
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
elfská	elfský	k2eAgFnSc1d1	elfská
řeč	řeč	k1gFnSc1	řeč
se	se	k3xPyFc4	se
těšila	těšit	k5eAaImAgFnS	těšit
v	v	k7c4	v
Númenoru	Númenora	k1gFnSc4	Númenora
zvláštní	zvláštní	k2eAgFnSc3d1	zvláštní
přízni	přízeň	k1gFnSc3	přízeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pod	pod	k7c7	pod
ochranou	ochrana	k1gFnSc7	ochrana
a	a	k8xC	a
učením	učení	k1gNnSc7	učení
elfů	elf	k1gMnPc2	elf
si	se	k3xPyFc3	se
osvojili	osvojit	k5eAaPmAgMnP	osvojit
Númenorejci	Númenorejec	k1gMnPc1	Númenorejec
mnoho	mnoho	k6eAd1	mnoho
schopností	schopnost	k1gFnSc7	schopnost
<g/>
,	,	kIx,	,
nejvíce	nejvíce	k6eAd1	nejvíce
však	však	k9	však
vynikali	vynikat	k5eAaImAgMnP	vynikat
v	v	k7c6	v
mořeplavbě	mořeplavba	k1gFnSc6	mořeplavba
<g/>
.	.	kIx.	.
</s>
<s>
Plavili	plavit	k5eAaImAgMnP	plavit
se	se	k3xPyFc4	se
do	do	k7c2	do
všech	všecek	k3xTgFnPc2	všecek
světových	světový	k2eAgFnPc2d1	světová
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
nejméně	málo	k6eAd3	málo
však	však	k9	však
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Neboť	neboť	k8xC	neboť
Valar	Valar	k1gMnSc1	Valar
nechtěli	chtít	k5eNaImAgMnP	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
lidé	člověk	k1gMnPc1	člověk
poznali	poznat	k5eAaPmAgMnP	poznat
země	zem	k1gFnPc4	zem
neumírajících	umírající	k2eNgInPc2d1	neumírající
a	a	k8xC	a
zatoužili	zatoužit	k5eAaPmAgMnP	zatoužit
po	po	k7c6	po
nich	on	k3xPp3gInPc6	on
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
jim	on	k3xPp3gMnPc3	on
stanovili	stanovit	k5eAaPmAgMnP	stanovit
Zápověď	zápověď	k1gFnSc4	zápověď
plout	plout	k5eAaImF	plout
daleko	daleko	k6eAd1	daleko
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Númenorejci	Númenorejec	k1gMnPc1	Númenorejec
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
plavili	plavit	k5eAaImAgMnP	plavit
především	především	k9	především
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
přistávali	přistávat	k5eAaImAgMnP	přistávat
také	také	k9	také
často	často	k6eAd1	často
ve	v	k7c6	v
Středozemi	Středozem	k1gFnSc6	Středozem
a	a	k8xC	a
učili	učít	k5eAaPmAgMnP	učít
tamní	tamní	k2eAgMnPc4d1	tamní
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dlouhých	dlouhý	k2eAgNnPc6d1	dlouhé
dvou	dva	k4xCgNnPc6	dva
tisíciletích	tisíciletí	k1gNnPc6	tisíciletí
začalo	začít	k5eAaPmAgNnS	začít
opět	opět	k6eAd1	opět
působit	působit	k5eAaImF	působit
zlo	zlo	k1gNnSc4	zlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
kdysi	kdysi	k6eAd1	kdysi
zasel	zasít	k5eAaPmAgMnS	zasít
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
Melkor	Melkor	k1gInSc1	Melkor
<g/>
.	.	kIx.	.
</s>
<s>
Neboť	neboť	k8xC	neboť
moc	moc	k6eAd1	moc
a	a	k8xC	a
schopnosti	schopnost	k1gFnPc1	schopnost
Númenorejců	Númenorejec	k1gMnPc2	Númenorejec
stále	stále	k6eAd1	stále
rostly	růst	k5eAaImAgFnP	růst
a	a	k8xC	a
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
samých	samý	k3xTgMnPc2	samý
začala	začít	k5eAaPmAgFnS	začít
klíčit	klíčit	k5eAaImF	klíčit
touha	touha	k1gFnSc1	touha
po	po	k7c6	po
věčném	věčný	k2eAgInSc6d1	věčný
životě	život	k1gInSc6	život
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
Valar	Valar	k1gMnSc1	Valar
to	ten	k3xDgNnSc4	ten
zaznamenali	zaznamenat	k5eAaPmAgMnP	zaznamenat
a	a	k8xC	a
skrze	skrze	k?	skrze
posly	posel	k1gMnPc7	posel
varovali	varovat	k5eAaImAgMnP	varovat
númenorského	númenorský	k2eAgMnSc4d1	númenorský
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
nikdo	nikdo	k3yNnSc1	nikdo
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
nepokoušel	pokoušet	k5eNaImAgMnS	pokoušet
měnit	měnit	k5eAaImF	měnit
řád	řád	k1gInSc4	řád
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
však	však	k9	však
jejich	jejich	k3xOp3gNnSc4	jejich
poselství	poselství	k1gNnSc4	poselství
nedbal	nedbat	k5eAaImAgMnS	nedbat
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
Númenorejci	Númenorejec	k1gMnPc1	Númenorejec
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
skupiny	skupina	k1gFnPc4	skupina
–	–	k?	–
větší	veliký	k2eAgFnSc4d2	veliký
si	se	k3xPyFc3	se
říkala	říkat	k5eAaImAgFnS	říkat
Královští	královský	k2eAgMnPc1d1	královský
a	a	k8xC	a
odcizila	odcizit	k5eAaPmAgFnS	odcizit
se	se	k3xPyFc4	se
Valar	Valara	k1gFnPc2	Valara
a	a	k8xC	a
elfům	elf	k1gMnPc3	elf
<g/>
,	,	kIx,	,
menší	malý	k2eAgInPc4d2	menší
si	se	k3xPyFc3	se
říkala	říkat	k5eAaImAgFnS	říkat
Přátelé	přítel	k1gMnPc1	přítel
elfů	elf	k1gMnPc2	elf
a	a	k8xC	a
dál	daleko	k6eAd2	daleko
naslouchala	naslouchat	k5eAaImAgFnS	naslouchat
radám	rada	k1gFnPc3	rada
elfů	elf	k1gMnPc2	elf
a	a	k8xC	a
vyslanců	vyslanec	k1gMnPc2	vyslanec
Valar	Valara	k1gFnPc2	Valara
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Myšlenkám	myšlenka	k1gFnPc3	myšlenka
na	na	k7c4	na
smrt	smrt	k1gFnSc4	smrt
však	však	k9	však
podlehli	podlehnout	k5eAaPmAgMnP	podlehnout
všichni	všechen	k3xTgMnPc1	všechen
a	a	k8xC	a
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
neodvážili	odvážit	k5eNaPmAgMnP	odvážit
porušit	porušit	k5eAaPmF	porušit
Zápověď	zápověď	k1gFnSc4	zápověď
<g/>
,	,	kIx,	,
plavívali	plavívat	k5eAaImAgMnP	plavívat
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
na	na	k7c4	na
Východ	východ	k1gInSc4	východ
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
si	se	k3xPyFc3	se
stavět	stavět	k5eAaImF	stavět
trvalá	trvalý	k2eAgNnPc4d1	trvalé
sídla	sídlo	k1gNnPc4	sídlo
ve	v	k7c6	v
Středozemi	Středozem	k1gFnSc6	Středozem
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jí	on	k3xPp3gFnSc7	on
zatoužili	zatoužit	k5eAaPmAgMnP	zatoužit
panovat	panovat	k5eAaImF	panovat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
lidí	člověk	k1gMnPc2	člověk
ze	z	k7c2	z
Středozemě	Středozem	k1gFnSc2	Středozem
také	také	k9	také
začali	začít	k5eAaPmAgMnP	začít
vybírat	vybírat	k5eAaImF	vybírat
daně	daň	k1gFnPc4	daň
a	a	k8xC	a
stali	stát	k5eAaPmAgMnP	stát
se	se	k3xPyFc4	se
spíš	spíš	k9	spíš
jejich	jejich	k3xOp3gMnPc7	jejich
pány	pan	k1gMnPc7	pan
než	než	k8xS	než
učiteli	učitel	k1gMnPc7	učitel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
v	v	k7c6	v
příběhu	příběh	k1gInSc6	příběh
o	o	k7c6	o
prstenech	prsten	k1gInPc6	prsten
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
ve	v	k7c6	v
Středozemi	Středozem	k1gFnSc6	Středozem
povstal	povstat	k5eAaPmAgInS	povstat
Sauron	Sauron	k1gInSc1	Sauron
<g/>
,	,	kIx,	,
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
ke	k	k7c3	k
zlu	zlo	k1gNnSc3	zlo
a	a	k8xC	a
opevnil	opevnit	k5eAaPmAgInS	opevnit
zemi	zem	k1gFnSc3	zem
Mordor	Mordor	k1gInSc1	Mordor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Nenáviděl	návidět	k5eNaImAgMnS	návidět
Númenorejce	Númenorejec	k1gMnPc4	Númenorejec
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnPc4	jejich
spojenectví	spojenectví	k1gNnPc4	spojenectví
s	s	k7c7	s
elfy	elf	k1gMnPc7	elf
a	a	k8xC	a
protože	protože	k8xS	protože
i	i	k9	i
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
toužil	toužit	k5eAaImAgMnS	toužit
panovat	panovat	k5eAaImF	panovat
Středozemi	Středozem	k1gFnSc4	Středozem
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
však	však	k9	však
neodvážil	odvážit	k5eNaPmAgInS	odvážit
postavit	postavit	k5eAaPmF	postavit
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
je	být	k5eAaImIp3nS	být
ovládnout	ovládnout	k5eAaPmF	ovládnout
pobřežní	pobřežní	k2eAgFnPc4d1	pobřežní
země	zem	k1gFnPc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
však	však	k9	však
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
Prstenové	prstenový	k2eAgInPc4d1	prstenový
přízraky	přízrak	k1gInPc4	přízrak
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc1	jeho
nejstrašnější	strašný	k2eAgMnPc1d3	nejstrašnější
služebníci	služebník	k1gMnPc1	služebník
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
napadat	napadat	k5eAaImF	napadat
jejich	jejich	k3xOp3gNnPc4	jejich
sídla	sídlo	k1gNnPc4	sídlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
pýcha	pýcha	k1gFnSc1	pýcha
númenorejských	númenorejský	k2eAgMnPc2d1	númenorejský
králů	král	k1gMnPc2	král
vrcholu	vrchol	k1gInSc2	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Zakázali	zakázat	k5eAaPmAgMnP	zakázat
používat	používat	k5eAaImF	používat
elfštinu	elfština	k1gFnSc4	elfština
a	a	k8xC	a
zabránili	zabránit	k5eAaPmAgMnP	zabránit
jakýmkoli	jakýkoli	k3yIgInPc3	jakýkoli
vztahům	vztah	k1gInPc3	vztah
s	s	k7c7	s
elfy	elf	k1gMnPc7	elf
<g/>
.	.	kIx.	.
</s>
<s>
Dvacátý	dvacátý	k4xOgInSc1	dvacátý
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
král	král	k1gMnSc1	král
byl	být	k5eAaImAgMnS	být
z	z	k7c2	z
númenorských	númenorský	k2eAgMnPc2d1	númenorský
králů	král	k1gMnPc2	král
nejmocnější	mocný	k2eAgInPc1d3	nejmocnější
a	a	k8xC	a
nejpyšnější	pyšný	k2eAgInPc1d3	nejpyšnější
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sauron	Sauron	k1gMnSc1	Sauron
si	se	k3xPyFc3	se
dal	dát	k5eAaPmAgMnS	dát
jméno	jméno	k1gNnSc4	jméno
Pán	pán	k1gMnSc1	pán
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
přemýšlet	přemýšlet	k5eAaImF	přemýšlet
o	o	k7c6	o
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
toužil	toužit	k5eAaImAgMnS	toužit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
byl	být	k5eAaImAgMnS	být
neomezeným	omezený	k2eNgMnSc7d1	neomezený
pánem	pán	k1gMnSc7	pán
<g/>
.	.	kIx.	.
</s>
<s>
Přeplul	přeplout	k5eAaPmAgInS	přeplout
proto	proto	k8xC	proto
s	s	k7c7	s
vojskem	vojsko	k1gNnSc7	vojsko
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
přistál	přistát	k5eAaPmAgInS	přistát
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Středozemě	Středozem	k1gFnSc2	Středozem
a	a	k8xC	a
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
tam	tam	k6eAd1	tam
Saurona	Sauron	k1gMnSc4	Sauron
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
přišel	přijít	k5eAaPmAgMnS	přijít
a	a	k8xC	a
přísahal	přísahat	k5eAaImAgMnS	přísahat
mu	on	k3xPp3gMnSc3	on
věrnost	věrnost	k1gFnSc4	věrnost
<g/>
.	.	kIx.	.
</s>
<s>
Sauron	Sauron	k1gMnSc1	Sauron
skutečně	skutečně	k6eAd1	skutečně
přišel	přijít	k5eAaPmAgMnS	přijít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pochyboval	pochybovat	k5eAaImAgInS	pochybovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
jeho	jeho	k3xOp3gNnPc1	jeho
vojska	vojsko	k1gNnPc1	vojsko
byla	být	k5eAaImAgNnP	být
schopna	schopen	k2eAgFnSc1d1	schopna
Númenorejcům	Númenorejec	k1gMnPc3	Númenorejec
odolat	odolat	k5eAaPmF	odolat
<g/>
,	,	kIx,	,
a	a	k8xC	a
protože	protože	k8xS	protože
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
svého	svůj	k3xOyFgMnSc4	svůj
může	moct	k5eAaImIp3nS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
i	i	k9	i
lstí	lest	k1gFnSc7	lest
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
si	se	k3xPyFc3	se
chtěl	chtít	k5eAaImAgMnS	chtít
Sauronovu	Sauronův	k2eAgFnSc4d1	Sauronova
věrnost	věrnost	k1gFnSc4	věrnost
pojistit	pojistit	k5eAaPmF	pojistit
a	a	k8xC	a
proto	proto	k8xC	proto
ho	on	k3xPp3gNnSc4	on
s	s	k7c7	s
sebou	se	k3xPyFc7	se
převezl	převézt	k5eAaPmAgMnS	převézt
na	na	k7c4	na
Númenor	Númenor	k1gInSc4	Númenor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
Númenoru	Númenora	k1gFnSc4	Númenora
si	se	k3xPyFc3	se
Sauron	Sauron	k1gMnSc1	Sauron
získal	získat	k5eAaPmAgMnS	získat
přízeň	přízeň	k1gFnSc4	přízeň
krále	král	k1gMnSc2	král
a	a	k8xC	a
lidu	lid	k1gInSc2	lid
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
Númenorejce	Númenorejec	k1gMnSc4	Númenorejec
odvracet	odvracet	k5eAaImF	odvracet
od	od	k7c2	od
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
je	on	k3xPp3gInPc4	on
naučili	naučit	k5eAaPmAgMnP	naučit
Valar	Valar	k1gInSc4	Valar
<g/>
.	.	kIx.	.
</s>
<s>
Krále	Král	k1gMnSc4	Král
také	také	k9	také
navedl	navést	k5eAaPmAgInS	navést
k	k	k7c3	k
uctívání	uctívání	k1gNnSc3	uctívání
Melkora	Melkor	k1gMnSc2	Melkor
a	a	k8xC	a
Tmy	tma	k1gFnSc2	tma
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
lidu	lid	k1gInSc2	lid
se	se	k3xPyFc4	se
přidala	přidat	k5eAaPmAgFnS	přidat
<g/>
.	.	kIx.	.
</s>
<s>
Začaly	začít	k5eAaPmAgInP	začít
oběti	oběť	k1gFnPc4	oběť
Melkorovi	Melkor	k1gMnSc3	Melkor
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
byli	být	k5eAaImAgMnP	být
obětováni	obětován	k2eAgMnPc1d1	obětován
Přátelé	přítel	k1gMnPc1	přítel
elfů	elf	k1gMnPc2	elf
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
blížil	blížit	k5eAaImAgInS	blížit
konec	konec	k1gInSc1	konec
králova	králův	k2eAgInSc2d1	králův
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
Sauron	Sauron	k1gMnSc1	Sauron
mu	on	k3xPp3gMnSc3	on
namluvil	namluvit	k5eAaPmAgMnS	namluvit
dlouho	dlouho	k6eAd1	dlouho
připravovanou	připravovaný	k2eAgFnSc4d1	připravovaná
lež	lež	k1gFnSc4	lež
–	–	k?	–
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
mocný	mocný	k2eAgMnSc1d1	mocný
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nemusí	muset	k5eNaImIp3nP	muset
obávat	obávat	k5eAaImF	obávat
žádného	žádný	k3yNgInSc2	žádný
zákazu	zákaz	k1gInSc2	zákaz
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
zmocnit	zmocnit	k5eAaPmF	zmocnit
Valinoru	Valinora	k1gFnSc4	Valinora
a	a	k8xC	a
opatřit	opatřit	k5eAaPmF	opatřit
si	se	k3xPyFc3	se
věčný	věčný	k2eAgInSc4d1	věčný
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
jeho	jeho	k3xOp3gFnPc4	jeho
lži	lež	k1gFnPc4	lež
uvěřil	uvěřit	k5eAaPmAgMnS	uvěřit
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
postavit	postavit	k5eAaPmF	postavit
obrovské	obrovský	k2eAgNnSc4d1	obrovské
loďstvo	loďstvo	k1gNnSc4	loďstvo
<g/>
.	.	kIx.	.
</s>
<s>
Přátelé	přítel	k1gMnPc1	přítel
elfů	elf	k1gMnPc2	elf
se	se	k3xPyFc4	se
připravované	připravovaný	k2eAgFnSc2d1	připravovaná
války	válka	k1gFnSc2	válka
zhrozili	zhrozit	k5eAaPmAgMnP	zhrozit
a	a	k8xC	a
stanovili	stanovit	k5eAaPmAgMnP	stanovit
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Númenor	Númenor	k1gMnSc1	Númenor
opustí	opustit	k5eAaPmIp3nS	opustit
<g/>
,	,	kIx,	,
než	než	k8xS	než
bude	být	k5eAaImBp3nS	být
pozdě	pozdě	k6eAd1	pozdě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lodě	loď	k1gFnPc1	loď
Númenorejců	Númenorejec	k1gMnPc2	Númenorejec
skutečně	skutečně	k6eAd1	skutečně
opustily	opustit	k5eAaPmAgInP	opustit
přístavy	přístav	k1gInPc4	přístav
a	a	k8xC	a
vypluly	vyplout	k5eAaPmAgFnP	vyplout
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Loďstvo	loďstvo	k1gNnSc1	loďstvo
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
až	až	k9	až
blažené	blažený	k2eAgFnSc2d1	blažená
říše	říš	k1gFnSc2	říš
Amanu	Aman	k1gInSc2	Aman
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
sám	sám	k3xTgMnSc1	sám
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
na	na	k7c4	na
břeh	břeh	k1gInSc4	břeh
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
zemi	zem	k1gFnSc4	zem
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
<g/>
.	.	kIx.	.
</s>
<s>
Valar	Valar	k1gMnSc1	Valar
tehdy	tehdy	k6eAd1	tehdy
volali	volat	k5eAaImAgMnP	volat
Ilúvatara	Ilúvatar	k1gMnSc4	Ilúvatar
a	a	k8xC	a
přenechali	přenechat	k5eAaPmAgMnP	přenechat
mu	on	k3xPp3gMnSc3	on
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
Ardou	Arda	k1gFnSc7	Arda
<g/>
.	.	kIx.	.
</s>
<s>
Ilúvatar	Ilúvatar	k1gMnSc1	Ilúvatar
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
mezi	mezi	k7c7	mezi
Amanem	Aman	k1gInSc7	Aman
a	a	k8xC	a
Númenorem	Númenor	k1gInSc7	Númenor
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
propast	propast	k1gFnSc4	propast
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
všechny	všechen	k3xTgFnPc4	všechen
númenorejské	númenorejský	k2eAgFnPc4d1	númenorejský
lodě	loď	k1gFnPc4	loď
a	a	k8xC	a
král	král	k1gMnSc1	král
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
bojovníci	bojovník	k1gMnPc1	bojovník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
na	na	k7c4	na
amanský	amanský	k2eAgInSc4d1	amanský
břeh	břeh	k1gInSc4	břeh
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
pohřbeni	pohřben	k2eAgMnPc1d1	pohřben
zhroucenými	zhroucený	k2eAgFnPc7d1	zhroucená
horami	hora	k1gFnPc7	hora
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
sám	sám	k3xTgInSc1	sám
Númenor	Númenor	k1gInSc1	Númenor
byl	být	k5eAaImAgInS	být
stržen	strhnout	k5eAaPmNgInS	strhnout
do	do	k7c2	do
propasti	propast	k1gFnSc2	propast
a	a	k8xC	a
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Země	zem	k1gFnPc1	zem
neumírajících	umírající	k2eNgMnPc2d1	neumírající
byly	být	k5eAaImAgFnP	být
Ilúvatarem	Ilúvatar	k1gInSc7	Ilúvatar
vzaty	vzít	k5eAaPmNgFnP	vzít
od	od	k7c2	od
ostatního	ostatní	k2eAgInSc2d1	ostatní
světa	svět	k1gInSc2	svět
z	z	k7c2	z
dosahu	dosah	k1gInSc2	dosah
lidí	člověk	k1gMnPc2	člověk
do	do	k7c2	do
říše	říš	k1gFnSc2	říš
skrytých	skrytý	k2eAgFnPc2d1	skrytá
věcí	věc	k1gFnPc2	věc
<g/>
;	;	kIx,	;
země	zem	k1gFnPc1	zem
<g/>
,	,	kIx,	,
dosud	dosud	k6eAd1	dosud
plochá	plochý	k2eAgFnSc1d1	plochá
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
zakulacena	zakulatit	k5eAaPmNgFnS	zakulatit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
ze	z	k7c2	z
Středozemě	Středozem	k1gFnSc2	Středozem
vydal	vydat	k5eAaPmAgMnS	vydat
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
elfům	elf	k1gMnPc3	elf
bylo	být	k5eAaImAgNnS	být
dovoleno	dovolen	k2eAgNnSc1d1	dovoleno
nadále	nadále	k6eAd1	nadále
ze	z	k7c2	z
Středozemě	Středozem	k1gFnSc2	Středozem
plout	plout	k5eAaImF	plout
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
blažené	blažený	k2eAgFnSc2d1	blažená
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
opustily	opustit	k5eAaPmAgFnP	opustit
na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
lodě	loď	k1gFnSc2	loď
Přátel	přítel	k1gMnPc2	přítel
elfů	elf	k1gMnPc2	elf
Númenor	Númenora	k1gFnPc2	Númenora
a	a	k8xC	a
pronásledovány	pronásledován	k2eAgFnPc1d1	pronásledována
bouří	bouř	k1gFnPc2	bouř
dopluly	doplout	k5eAaPmAgInP	doplout
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Středozemě	Středozem	k1gFnSc2	Středozem
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Středozemi	Středozem	k1gFnSc6	Středozem
pak	pak	k6eAd1	pak
založili	založit	k5eAaPmAgMnP	založit
nová	nový	k2eAgNnPc4d1	nové
království	království	k1gNnPc4	království
<g/>
.	.	kIx.	.
</s>
<s>
Sauron	Sauron	k1gInSc1	Sauron
byl	být	k5eAaImAgInS	být
polapen	polapit	k5eAaPmNgInS	polapit
ve	v	k7c6	v
zkáze	zkáza	k1gFnSc6	zkáza
Númenoru	Númenor	k1gInSc2	Númenor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protože	protože	k8xS	protože
neměl	mít	k5eNaImAgMnS	mít
smrtelné	smrtelný	k2eAgNnSc4d1	smrtelné
tělo	tělo	k1gNnSc4	tělo
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
duch	duch	k1gMnSc1	duch
povstal	povstat	k5eAaPmAgMnS	povstat
z	z	k7c2	z
hlubin	hlubina	k1gFnPc2	hlubina
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
země	zem	k1gFnSc2	zem
Mordoru	Mordor	k1gInSc2	Mordor
<g/>
;	;	kIx,	;
tam	tam	k6eAd1	tam
přijal	přijmout	k5eAaPmAgMnS	přijmout
novou	nový	k2eAgFnSc4d1	nová
podobu	podoba	k1gFnSc4	podoba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
strašná	strašný	k2eAgFnSc1d1	strašná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
O	o	k7c6	o
prstenech	prsten	k1gInPc6	prsten
moci	moct	k5eAaImF	moct
a	a	k8xC	a
Třetím	třetí	k4xOgInSc6	třetí
věku	věk	k1gInSc6	věk
===	===	k?	===
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
poslední	poslední	k2eAgFnSc1d1	poslední
část	část	k1gFnSc1	část
Silmarillionu	Silmarillion	k1gInSc2	Silmarillion
popisuje	popisovat	k5eAaImIp3nS	popisovat
události	událost	k1gFnPc4	událost
Druhého	druhý	k4xOgInSc2	druhý
a	a	k8xC	a
Třetího	třetí	k4xOgInSc2	třetí
věku	věk	k1gInSc2	věk
ve	v	k7c6	v
Středozemi	Středozem	k1gFnSc6	Středozem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Melkora	Melkor	k1gMnSc2	Melkor
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
elfů	elf	k1gMnPc2	elf
vrátila	vrátit	k5eAaPmAgFnS	vrátit
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
však	však	k9	však
zůstali	zůstat	k5eAaPmAgMnP	zůstat
ve	v	k7c6	v
Středozemi	Středozem	k1gFnSc6	Středozem
–	–	k?	–
usadili	usadit	k5eAaPmAgMnP	usadit
se	se	k3xPyFc4	se
jednak	jednak	k8xC	jednak
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
hlouběji	hluboko	k6eAd2	hluboko
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
založili	založit	k5eAaPmAgMnP	založit
několik	několik	k4yIc4	několik
říší	říš	k1gFnPc2	říš
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
největšího	veliký	k2eAgInSc2d3	veliký
rozvoje	rozvoj	k1gInSc2	rozvoj
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
Eregion	Eregion	k1gInSc1	Eregion
v	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
podzemní	podzemní	k2eAgFnSc2d1	podzemní
trpasličí	trpasličí	k2eAgFnSc2d1	trpasličí
říše	říš	k1gFnSc2	říš
Khazad-dû	Khazadû	k1gFnSc2	Khazad-dû
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
Středozemi	Středozem	k1gFnSc6	Středozem
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Sauron	Sauron	k1gInSc1	Sauron
po	po	k7c6	po
čase	čas	k1gInSc6	čas
znovu	znovu	k6eAd1	znovu
povstal	povstat	k5eAaPmAgMnS	povstat
<g/>
.	.	kIx.	.
</s>
<s>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
stranu	strana	k1gFnSc4	strana
získat	získat	k5eAaPmF	získat
mnoho	mnoho	k6eAd1	mnoho
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
toužil	toužit	k5eAaImAgInS	toužit
však	však	k9	však
podrobit	podrobit	k5eAaPmF	podrobit
si	se	k3xPyFc3	se
hlavně	hlavně	k9	hlavně
elfy	elf	k1gMnPc7	elf
<g/>
.	.	kIx.	.
</s>
<s>
Nosil	nosit	k5eAaImAgInS	nosit
ještě	ještě	k9	ještě
pěknou	pěkný	k2eAgFnSc4d1	pěkná
podobu	podoba	k1gFnSc4	podoba
a	a	k8xC	a
chodil	chodit	k5eAaImAgMnS	chodit
mezi	mezi	k7c4	mezi
elfy	elf	k1gMnPc4	elf
pod	pod	k7c7	pod
záminkou	záminka	k1gFnSc7	záminka
<g/>
,	,	kIx,	,
že	že	k8xS	že
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
chce	chtít	k5eAaImIp3nS	chtít
zvelebovat	zvelebovat	k5eAaImF	zvelebovat
Středozem	Středozem	k1gFnSc4	Středozem
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
rady	rada	k1gFnPc1	rada
byly	být	k5eAaImAgFnP	být
nejochotněji	ochotně	k6eAd3	ochotně
přijímány	přijímat	k5eAaImNgInP	přijímat
v	v	k7c6	v
Eregionu	Eregion	k1gInSc6	Eregion
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
elfové	elf	k1gMnPc1	elf
nejvíce	hodně	k6eAd3	hodně
toužili	toužit	k5eAaImAgMnP	toužit
po	po	k7c4	po
zvětšování	zvětšování	k1gNnSc4	zvětšování
svých	svůj	k3xOyFgFnPc2	svůj
schopností	schopnost	k1gFnPc2	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Saurona	Saurona	k1gFnSc1	Saurona
eregionští	eregionský	k2eAgMnPc1d1	eregionský
kováři	kovář	k1gMnPc1	kovář
vyrobili	vyrobit	k5eAaPmAgMnP	vyrobit
Prsteny	prsten	k1gInPc4	prsten
moci	moc	k1gFnSc2	moc
<g/>
;	;	kIx,	;
Sauron	Sauron	k1gMnSc1	Sauron
si	se	k3xPyFc3	se
však	však	k9	však
bez	bez	k7c2	bez
jejich	jejich	k3xOp3gNnSc2	jejich
vědomí	vědomí	k1gNnSc2	vědomí
vyrobil	vyrobit	k5eAaPmAgMnS	vyrobit
Jeden	jeden	k4xCgInSc4	jeden
prsten	prsten	k1gInSc4	prsten
<g/>
,	,	kIx,	,
do	do	k7c2	do
nějž	jenž	k3xRgMnSc2	jenž
přesunul	přesunout	k5eAaPmAgMnS	přesunout
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
své	svůj	k3xOyFgFnSc2	svůj
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
pomocí	pomocí	k7c2	pomocí
něhož	jenž	k3xRgInSc2	jenž
mohl	moct	k5eAaImAgInS	moct
vnímat	vnímat	k5eAaImF	vnímat
myšlenky	myšlenka	k1gFnPc4	myšlenka
nositelů	nositel	k1gMnPc2	nositel
ostatních	ostatní	k2eAgInPc2d1	ostatní
prstenů	prsten	k1gInPc2	prsten
a	a	k8xC	a
ovládat	ovládat	k5eAaImF	ovládat
jejich	jejich	k3xOp3gFnSc4	jejich
mysl	mysl	k1gFnSc4	mysl
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
si	se	k3xPyFc3	se
však	však	k9	však
prsten	prsten	k1gInSc4	prsten
poprvé	poprvé	k6eAd1	poprvé
nasadil	nasadit	k5eAaPmAgMnS	nasadit
<g/>
,	,	kIx,	,
elfové	elf	k1gMnPc1	elf
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
uvědomili	uvědomit	k5eAaPmAgMnP	uvědomit
a	a	k8xC	a
své	svůj	k3xOyFgInPc4	svůj
prsteny	prsten	k1gInPc4	prsten
skryli	skrýt	k5eAaPmAgMnP	skrýt
<g/>
.	.	kIx.	.
</s>
<s>
Sauron	Sauron	k1gMnSc1	Sauron
se	se	k3xPyFc4	se
na	na	k7c4	na
ně	on	k3xPp3gInPc4	on
rozhněval	rozhněvat	k5eAaPmAgMnS	rozhněvat
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgInPc4	všechen
prsteny	prsten	k1gInPc4	prsten
jim	on	k3xPp3gMnPc3	on
sebral	sebrat	k5eAaPmAgInS	sebrat
<g/>
.	.	kIx.	.
</s>
<s>
Prsteny	prsten	k1gInPc1	prsten
potom	potom	k6eAd1	potom
znovu	znovu	k6eAd1	znovu
rozdělil	rozdělit	k5eAaPmAgInS	rozdělit
–	–	k?	–
část	část	k1gFnSc4	část
jich	on	k3xPp3gMnPc2	on
dal	dát	k5eAaPmAgMnS	dát
trpaslíkům	trpaslík	k1gMnPc3	trpaslík
<g/>
,	,	kIx,	,
devět	devět	k4xCc4	devět
lidem	člověk	k1gMnPc3	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
je	on	k3xPp3gNnSc4	on
nosili	nosit	k5eAaImAgMnP	nosit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
po	po	k7c6	po
čase	čas	k1gInSc6	čas
vůlí	vůle	k1gFnPc2	vůle
Jednoho	jeden	k4xCgInSc2	jeden
prstenu	prsten	k1gInSc2	prsten
propadli	propadnout	k5eAaPmAgMnP	propadnout
do	do	k7c2	do
říše	říš	k1gFnSc2	říš
stínů	stín	k1gInPc2	stín
a	a	k8xC	a
stali	stát	k5eAaPmAgMnP	stát
se	s	k7c7	s
Sauronovými	Sauronův	k2eAgMnPc7d1	Sauronův
služebníky	služebník	k1gMnPc7	služebník
<g/>
,	,	kIx,	,
Prstenovými	prstenový	k2eAgInPc7d1	prstenový
přízraky	přízrak	k1gInPc7	přízrak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Prstenových	prstenový	k2eAgInPc2d1	prstenový
přízraků	přízrak	k1gInPc2	přízrak
Sauronova	Sauronův	k2eAgFnSc1d1	Sauronova
moc	moc	k1gFnSc1	moc
velmi	velmi	k6eAd1	velmi
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
a	a	k8xC	a
elfové	elf	k1gMnPc1	elf
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
opouštěli	opouštět	k5eAaImAgMnP	opouštět
Středozem	Středozem	k1gFnSc4	Středozem
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
nemnoho	mnoho	k6eNd1	mnoho
elfských	elfských	k2eAgFnSc7d1	elfských
říší	říš	k1gFnSc7	říš
dokázalo	dokázat	k5eAaPmAgNnS	dokázat
vzdorovat	vzdorovat	k5eAaImF	vzdorovat
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
jinými	jiná	k1gFnPc7	jiná
nově	nově	k6eAd1	nově
založená	založený	k2eAgFnSc1d1	založená
Roklinka	roklinka	k1gFnSc1	roklinka
<g/>
,	,	kIx,	,
pevnost	pevnost	k1gFnSc1	pevnost
Půlelfa	Půlelf	k1gMnSc2	Půlelf
Elronda	Elrond	k1gMnSc2	Elrond
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc2	syn
Eärendila	Eärendil	k1gMnSc2	Eärendil
a	a	k8xC	a
Elwing	Elwing	k1gInSc4	Elwing
<g/>
.	.	kIx.	.
</s>
<s>
Vzrůstu	vzrůst	k1gInSc2	vzrůst
Sauronovy	Sauronův	k2eAgFnSc2d1	Sauronova
moci	moc	k1gFnSc2	moc
se	se	k3xPyFc4	se
však	však	k9	však
postavili	postavit	k5eAaPmAgMnP	postavit
Númenorejci	Númenorejec	k1gMnPc1	Númenorejec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
začali	začít	k5eAaPmAgMnP	začít
ve	v	k7c6	v
Středozemi	Středozem	k1gFnSc6	Středozem
budovat	budovat	k5eAaImF	budovat
svá	svůj	k3xOyFgNnPc4	svůj
sídla	sídlo	k1gNnPc4	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Sauron	Sauron	k1gInSc1	Sauron
proto	proto	k8xC	proto
na	na	k7c4	na
čas	čas	k1gInSc4	čas
opustil	opustit	k5eAaPmAgMnS	opustit
Středozem	Středozem	k1gFnSc4	Středozem
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Númenorejce	Númenorejec	k1gMnSc4	Númenorejec
porazil	porazit	k5eAaPmAgMnS	porazit
lstí	lest	k1gFnSc7	lest
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
v	v	k7c6	v
Akallabê	Akallabê	k1gFnSc6	Akallabê
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgInS	být
Númenor	Númenor	k1gInSc1	Númenor
zničen	zničit	k5eAaPmNgInS	zničit
<g/>
,	,	kIx,	,
Sauron	Sauron	k1gMnSc1	Sauron
se	se	k3xPyFc4	se
jako	jako	k9	jako
duch	duch	k1gMnSc1	duch
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Středozemě	Středozem	k1gFnSc2	Středozem
<g/>
,	,	kIx,	,
zjistil	zjistit	k5eAaPmAgMnS	zjistit
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
nepřítomnosti	nepřítomnost	k1gFnSc2	nepřítomnost
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
moc	moc	k1gFnSc1	moc
Gil-galada	Gilalada	k1gFnSc1	Gil-galada
<g/>
,	,	kIx,	,
krále	král	k1gMnSc2	král
středozemských	středozemský	k2eAgMnPc2d1	středozemský
elfů	elf	k1gMnPc2	elf
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Středozemi	Středozem	k1gFnSc6	Středozem
navíc	navíc	k6eAd1	navíc
přistáli	přistát	k5eAaPmAgMnP	přistát
Númenorejci	Númenorejec	k1gMnPc1	Númenorejec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
uprchli	uprchnout	k5eAaPmAgMnP	uprchnout
ze	z	k7c2	z
zkázy	zkáza	k1gFnSc2	zkáza
své	svůj	k3xOyFgFnSc2	svůj
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
vedeni	vést	k5eAaImNgMnP	vést
Elendilem	Elendil	k1gInSc7	Elendil
<g/>
,	,	kIx,	,
potomkem	potomek	k1gMnSc7	potomek
Elrose	Elrosa	k1gFnSc3	Elrosa
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc7	jeho
syny	syn	k1gMnPc7	syn
Isildurem	Isildur	k1gMnSc7	Isildur
a	a	k8xC	a
Anárionem	Anárion	k1gInSc7	Anárion
<g/>
,	,	kIx,	,
a	a	k8xC	a
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Středozemě	Středozem	k1gFnSc2	Středozem
dvě	dva	k4xCgNnPc4	dva
království	království	k1gNnPc4	království
<g/>
,	,	kIx,	,
Arnor	Arnor	k1gInSc4	Arnor
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
Gondor	Gondor	k1gMnSc1	Gondor
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Númenoru	Númenor	k1gInSc2	Númenor
si	se	k3xPyFc3	se
dovezli	dovézt	k5eAaPmAgMnP	dovézt
mnoho	mnoho	k4c4	mnoho
pokladů	poklad	k1gInPc2	poklad
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
vidoucí	vidoucí	k2eAgInPc4d1	vidoucí
kameny	kámen	k1gInPc4	kámen
<g/>
,	,	kIx,	,
palantíry	palantír	k1gInPc4	palantír
<g/>
,	,	kIx,	,
a	a	k8xC	a
Bílý	bílý	k2eAgInSc1d1	bílý
strom	strom	k1gInSc1	strom
<g/>
,	,	kIx,	,
potomek	potomek	k1gMnSc1	potomek
zářícího	zářící	k2eAgInSc2d1	zářící
stromu	strom	k1gInSc2	strom
Telperionu	Telperion	k1gInSc2	Telperion
z	z	k7c2	z
Amanu	Aman	k1gInSc2	Aman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Netrvalo	trvat	k5eNaImAgNnS	trvat
dlouho	dlouho	k6eAd1	dlouho
a	a	k8xC	a
Sauron	Sauron	k1gMnSc1	Sauron
seznal	seznat	k5eAaPmAgMnS	seznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
vhodný	vhodný	k2eAgInSc4d1	vhodný
čas	čas	k1gInSc4	čas
a	a	k8xC	a
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
na	na	k7c4	na
Gondor	Gondor	k1gInSc4	Gondor
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
sice	sice	k8xC	sice
byl	být	k5eAaImAgMnS	být
odražen	odrazit	k5eAaPmNgMnS	odrazit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
všechny	všechen	k3xTgMnPc4	všechen
postupně	postupně	k6eAd1	postupně
přemohl	přemoct	k5eAaPmAgInS	přemoct
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
mohl	moct	k5eAaImAgInS	moct
bojovat	bojovat	k5eAaImF	bojovat
proti	proti	k7c3	proti
každému	každý	k3xTgNnSc3	každý
zvlášť	zvlášť	k6eAd1	zvlášť
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
elfové	elf	k1gMnPc1	elf
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Gil-galada	Gilalada	k1gFnSc1	Gil-galada
a	a	k8xC	a
Númenorejci	Númenorejec	k1gMnPc1	Númenorejec
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Isildura	Isildur	k1gMnSc2	Isildur
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
Poslední	poslední	k2eAgNnSc4d1	poslední
spojenectví	spojenectví	k1gNnSc4	spojenectví
<g/>
,	,	kIx,	,
shromáždili	shromáždit	k5eAaPmAgMnP	shromáždit
vojska	vojsko	k1gNnPc4	vojsko
a	a	k8xC	a
společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
utkali	utkat	k5eAaPmAgMnP	utkat
se	s	k7c7	s
Sauronovými	Sauronův	k2eAgMnPc7d1	Sauronův
bojovníky	bojovník	k1gMnPc7	bojovník
na	na	k7c6	na
Bitevní	bitevní	k2eAgFnSc6d1	bitevní
pláni	pláň	k1gFnSc6	pláň
před	před	k7c7	před
Mordorem	Mordor	k1gInSc7	Mordor
<g/>
.	.	kIx.	.
</s>
<s>
Dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
vítězství	vítězství	k1gNnPc4	vítězství
<g/>
,	,	kIx,	,
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
do	do	k7c2	do
Mordoru	Mordor	k1gInSc2	Mordor
a	a	k8xC	a
obklíčili	obklíčit	k5eAaPmAgMnP	obklíčit
Barad-dû	Baradû	k1gFnSc4	Barad-dû
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
sedmi	sedm	k4xCc2	sedm
let	léto	k1gNnPc2	léto
obléhání	obléhání	k1gNnSc1	obléhání
zemřel	zemřít	k5eAaPmAgInS	zemřít
Gil-galad	Gilalad	k1gInSc4	Gil-galad
i	i	k9	i
Elendil	Elendil	k1gMnSc1	Elendil
<g/>
,	,	kIx,	,
Isildur	Isildur	k1gMnSc1	Isildur
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
osobním	osobní	k2eAgInSc6d1	osobní
souboji	souboj	k1gInSc6	souboj
uťal	utít	k5eAaPmAgInS	utít
ze	z	k7c2	z
Sauronovy	Sauronův	k2eAgFnSc2d1	Sauronova
ruky	ruka	k1gFnSc2	ruka
Jeden	jeden	k4xCgInSc1	jeden
prsten	prsten	k1gInSc1	prsten
a	a	k8xC	a
Sauron	Sauron	k1gMnSc1	Sauron
byl	být	k5eAaImAgMnS	být
poražen	porazit	k5eAaPmNgMnS	porazit
a	a	k8xC	a
jako	jako	k9	jako
duch	duch	k1gMnSc1	duch
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
do	do	k7c2	do
pustin	pustina	k1gFnPc2	pustina
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
porážkou	porážka	k1gFnSc7	porážka
Saurona	Saurona	k1gFnSc1	Saurona
skončil	skončit	k5eAaPmAgInS	skončit
Druhý	druhý	k4xOgInSc4	druhý
věk	věk	k1gInSc4	věk
Středozemě	Středozem	k1gFnSc2	Středozem
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
věk	věk	k1gInSc1	věk
Třetí	třetí	k4xOgInSc1	třetí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
Třetího	třetí	k4xOgInSc2	třetí
věku	věk	k1gInSc2	věk
zla	zlo	k1gNnSc2	zlo
ve	v	k7c6	v
Středozemi	Středozem	k1gFnSc6	Středozem
ubylo	ubýt	k5eAaPmAgNnS	ubýt
<g/>
;	;	kIx,	;
neboť	neboť	k8xC	neboť
Sauronovi	Sauronův	k2eAgMnPc1d1	Sauronův
služebníci	služebník	k1gMnPc1	služebník
byli	být	k5eAaImAgMnP	být
rozehnáni	rozehnat	k5eAaPmNgMnP	rozehnat
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
ne	ne	k9	ne
zničeni	zničen	k2eAgMnPc1d1	zničen
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
poddalo	poddat	k5eAaPmAgNnS	poddat
Númenorejcům	Númenorejec	k1gMnPc3	Númenorejec
<g/>
,	,	kIx,	,
Barad-dû	Baradû	k1gMnSc3	Barad-dû
byla	být	k5eAaImAgFnS	být
srovnána	srovnat	k5eAaPmNgFnS	srovnat
se	s	k7c7	s
zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
Mordor	Mordor	k1gMnSc1	Mordor
obehnán	obehnat	k5eAaPmNgMnS	obehnat
strážemi	stráž	k1gFnPc7	stráž
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
a	a	k8xC	a
elfové	elf	k1gMnPc1	elf
se	se	k3xPyFc4	se
však	však	k9	však
sobě	se	k3xPyFc3	se
odcizili	odcizit	k5eAaPmAgMnP	odcizit
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc4	jeden
prsten	prsten	k1gInSc4	prsten
zmizel	zmizet	k5eAaPmAgMnS	zmizet
a	a	k8xC	a
nikdo	nikdo	k3yNnSc1	nikdo
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
nevěděl	vědět	k5eNaImAgMnS	vědět
<g/>
.	.	kIx.	.
</s>
<s>
Elrond	Elrond	k1gInSc1	Elrond
sice	sice	k8xC	sice
bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Saurona	Sauron	k1gMnSc2	Sauron
radil	radit	k5eAaImAgInS	radit
prsten	prsten	k1gInSc4	prsten
vrhnout	vrhnout	k5eAaPmF	vrhnout
do	do	k7c2	do
ohnivé	ohnivý	k2eAgFnSc2d1	ohnivá
hory	hora	k1gFnSc2	hora
Orodruiny	Orodruina	k1gFnSc2	Orodruina
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
jej	on	k3xPp3gMnSc4	on
Sauron	Sauron	k1gMnSc1	Sauron
vykoval	vykovat	k5eAaPmAgMnS	vykovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Isildur	Isildur	k1gMnSc1	Isildur
tehdy	tehdy	k6eAd1	tehdy
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Zamýšlel	zamýšlet	k5eAaImAgMnS	zamýšlet
si	se	k3xPyFc3	se
jej	on	k3xPp3gInSc4	on
ponechat	ponechat	k5eAaPmF	ponechat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
však	však	k9	však
cestou	cesta	k1gFnSc7	cesta
na	na	k7c4	na
sever	sever	k1gInSc4	sever
přepaden	přepaden	k2eAgInSc1d1	přepaden
skřety	skřet	k1gMnPc7	skřet
a	a	k8xC	a
Jeden	jeden	k4xCgInSc1	jeden
prsten	prsten	k1gInSc1	prsten
se	se	k3xPyFc4	se
ztratil	ztratit	k5eAaPmAgInS	ztratit
v	v	k7c6	v
řece	řeka	k1gFnSc6	řeka
Anduině	Anduin	k2eAgFnSc6d1	Anduin
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následujících	následující	k2eAgNnPc2d1	následující
let	léto	k1gNnPc2	léto
severní	severní	k2eAgNnSc1d1	severní
království	království	k1gNnSc1	království
Arnor	Arnora	k1gFnPc2	Arnora
velmi	velmi	k6eAd1	velmi
zesláblo	zeslábnout	k5eAaPmAgNnS	zeslábnout
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
obyvatel	obyvatel	k1gMnPc2	obyvatel
ubývalo	ubývat	k5eAaImAgNnS	ubývat
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc1	jejich
země	zem	k1gFnPc1	zem
upadly	upadnout	k5eAaPmAgFnP	upadnout
pod	pod	k7c4	pod
moc	moc	k1gFnSc4	moc
nepřátel	nepřítel	k1gMnPc2	nepřítel
a	a	k8xC	a
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
tuláci	tulák	k1gMnPc1	tulák
po	po	k7c6	po
krajině	krajina	k1gFnSc6	krajina
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
původ	původ	k1gInSc4	původ
upadl	upadnout	k5eAaPmAgMnS	upadnout
do	do	k7c2	do
zapomnění	zapomnění	k1gNnSc2	zapomnění
<g/>
.	.	kIx.	.
</s>
<s>
Říše	říše	k1gFnSc1	říše
Gondor	Gondora	k1gFnPc2	Gondora
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
naopak	naopak	k6eAd1	naopak
vzkvétala	vzkvétat	k5eAaImAgFnS	vzkvétat
a	a	k8xC	a
jistou	jistý	k2eAgFnSc4d1	jistá
dobu	doba	k1gFnSc4	doba
měla	mít	k5eAaImAgFnS	mít
respekt	respekt	k1gInSc4	respekt
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
části	část	k1gFnSc6	část
Středozemě	Středozem	k1gFnSc2	Středozem
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
Gondorských	Gondorský	k2eAgInPc6d1	Gondorský
však	však	k9	však
ubylo	ubýt	k5eAaPmAgNnS	ubýt
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
stráže	stráž	k1gFnSc2	stráž
okolo	okolo	k7c2	okolo
Mordoru	Mordor	k1gInSc2	Mordor
polevily	polevit	k5eAaPmAgFnP	polevit
v	v	k7c6	v
pozornosti	pozornost	k1gFnSc6	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
země	zem	k1gFnSc2	zem
vrátily	vrátit	k5eAaPmAgInP	vrátit
Prstenové	prstenový	k2eAgInPc1d1	prstenový
přízraky	přízrak	k1gInPc1	přízrak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
připravily	připravit	k5eAaPmAgInP	připravit
příchod	příchod	k1gInSc4	příchod
svého	svůj	k3xOyFgMnSc2	svůj
pána	pán	k1gMnSc2	pán
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
gondorský	gondorský	k2eAgMnSc1d1	gondorský
král	král	k1gMnSc1	král
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
nim	on	k3xPp3gMnPc3	on
osobně	osobně	k6eAd1	osobně
vydal	vydat	k5eAaPmAgInS	vydat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
však	však	k9	však
zajat	zajat	k2eAgInSc1d1	zajat
<g/>
,	,	kIx,	,
odvlečen	odvlečen	k2eAgInSc1d1	odvlečen
a	a	k8xC	a
víckrát	víckrát	k6eAd1	víckrát
se	se	k3xPyFc4	se
neobjevil	objevit	k5eNaPmAgInS	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
nezanechal	zanechat	k5eNaPmAgInS	zanechat
potomky	potomek	k1gMnPc4	potomek
<g/>
,	,	kIx,	,
ujali	ujmout	k5eAaPmAgMnP	ujmout
se	se	k3xPyFc4	se
vlády	vláda	k1gFnSc2	vláda
nad	nad	k7c7	nad
Gondorem	Gondor	k1gInSc7	Gondor
správci	správce	k1gMnPc1	správce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
přišli	přijít	k5eAaPmAgMnP	přijít
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
Rohirové	Rohirová	k1gFnSc2	Rohirová
<g/>
,	,	kIx,	,
válečníci	válečník	k1gMnPc1	válečník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
usadili	usadit	k5eAaPmAgMnP	usadit
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
severně	severně	k6eAd1	severně
od	od	k7c2	od
Gondoru	Gondor	k1gInSc2	Gondor
a	a	k8xC	a
stali	stát	k5eAaPmAgMnP	stát
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnPc1	jeho
spojenci	spojenec	k1gMnPc1	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Elfové	elf	k1gMnPc1	elf
ve	v	k7c6	v
Třetím	třetí	k4xOgInSc6	třetí
věku	věk	k1gInSc6	věk
stále	stále	k6eAd1	stále
odcházeli	odcházet	k5eAaImAgMnP	odcházet
na	na	k7c4	na
západ	západ	k1gInSc4	západ
za	za	k7c4	za
moře	moře	k1gNnSc4	moře
a	a	k8xC	a
těch	ten	k3xDgMnPc2	ten
nemnoho	mnoho	k6eNd1	mnoho
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zbývali	zbývat	k5eAaImAgMnP	zbývat
<g/>
,	,	kIx,	,
obývalo	obývat	k5eAaImAgNnS	obývat
nemnohá	mnohý	k2eNgNnPc1d1	nemnohé
místa	místo	k1gNnPc1	místo
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
vynikala	vynikat	k5eAaImAgFnS	vynikat
Elrondova	Elrondův	k2eAgFnSc1d1	Elrondova
pevnost	pevnost	k1gFnSc1	pevnost
Roklinka	roklinka	k1gFnSc1	roklinka
<g/>
,	,	kIx,	,
říše	říše	k1gFnSc1	říše
Celeborna	Celeborna	k1gFnSc1	Celeborna
a	a	k8xC	a
Galadriel	Galadriel	k1gInSc1	Galadriel
Lothlórien	Lothlórina	k1gFnPc2	Lothlórina
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Anduině	Anduina	k1gFnSc6	Anduina
a	a	k8xC	a
Šedé	Šedé	k2eAgInPc1d1	Šedé
přístavy	přístav	k1gInPc1	přístav
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Závěr	závěr	k1gInSc1	závěr
Třetího	třetí	k4xOgInSc2	třetí
věku	věk	k1gInSc2	věk
se	se	k3xPyFc4	se
přiblížil	přiblížit	k5eAaPmAgMnS	přiblížit
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
Velkém	velký	k2eAgInSc6d1	velký
zeleném	zelený	k2eAgInSc6d1	zelený
hvozdu	hvozd	k1gInSc6	hvozd
na	na	k7c6	na
východě	východ	k1gInSc6	východ
znovu	znovu	k6eAd1	znovu
povstal	povstat	k5eAaPmAgInS	povstat
Sauron	Sauron	k1gInSc1	Sauron
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
nikdo	nikdo	k3yNnSc1	nikdo
zpočátku	zpočátku	k6eAd1	zpočátku
nevěděl	vědět	k5eNaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
on	on	k3xPp3gMnSc1	on
<g/>
.	.	kIx.	.
</s>
<s>
Les	les	k1gInSc1	les
nabyl	nabýt	k5eAaPmAgInS	nabýt
zlé	zlé	k1gNnSc4	zlé
pověsti	pověst	k1gFnSc2	pověst
a	a	k8xC	a
dostal	dostat	k5eAaPmAgInS	dostat
jméno	jméno	k1gNnSc4	jméno
Temný	temný	k2eAgInSc1d1	temný
hvozd	hvozd	k1gInSc1	hvozd
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
ve	v	k7c6	v
Středozemi	Středozem	k1gFnSc6	Středozem
objevili	objevit	k5eAaPmAgMnP	objevit
čarodějové	čaroděj	k1gMnPc1	čaroděj
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
to	ten	k3xDgNnSc1	ten
duchové	duchový	k2eAgNnSc1d1	duchové
z	z	k7c2	z
Maiar	Maiara	k1gFnPc2	Maiara
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
vyslali	vyslat	k5eAaPmAgMnP	vyslat
Valar	Valar	k1gMnSc1	Valar
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
pomohli	pomoct	k5eAaPmAgMnP	pomoct
elfům	elf	k1gMnPc3	elf
a	a	k8xC	a
lidem	člověk	k1gMnPc3	člověk
ve	v	k7c6	v
Středozemi	Středozem	k1gFnSc6	Středozem
vzdorovat	vzdorovat	k5eAaImF	vzdorovat
Sauronovi	Sauronův	k2eAgMnPc1d1	Sauronův
<g/>
.	.	kIx.	.
</s>
<s>
Přišlo	přijít	k5eAaPmAgNnS	přijít
jich	on	k3xPp3gMnPc2	on
pět	pět	k4xCc4	pět
<g/>
,	,	kIx,	,
nejmocnější	mocný	k2eAgFnSc1d3	nejmocnější
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
dva	dva	k4xCgMnPc1	dva
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
od	od	k7c2	od
lidí	člověk	k1gMnPc2	člověk
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
jména	jméno	k1gNnSc2	jméno
Gandalf	Gandalf	k1gMnSc1	Gandalf
a	a	k8xC	a
Saruman	Saruman	k1gMnSc1	Saruman
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
Gandalf	Gandalf	k1gMnSc1	Gandalf
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zlo	zlo	k1gNnSc1	zlo
v	v	k7c6	v
Temném	temný	k2eAgInSc6d1	temný
hvozdu	hvozd	k1gInSc6	hvozd
je	být	k5eAaImIp3nS	být
znovu	znovu	k6eAd1	znovu
povstávající	povstávající	k2eAgInSc1d1	povstávající
Sauron	Sauron	k1gInSc1	Sauron
<g/>
.	.	kIx.	.
</s>
<s>
Radil	radit	k5eAaImAgMnS	radit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
němu	on	k3xPp3gNnSc3	on
zasáhlo	zasáhnout	k5eAaPmAgNnS	zasáhnout
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
ještě	ještě	k6eAd1	ještě
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
mocný	mocný	k2eAgMnSc1d1	mocný
a	a	k8xC	a
nemá	mít	k5eNaImIp3nS	mít
zpět	zpět	k6eAd1	zpět
prsten	prsten	k1gInSc4	prsten
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Saruman	Saruman	k1gMnSc1	Saruman
byl	být	k5eAaImAgMnS	být
proti	proti	k7c3	proti
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
po	po	k7c6	po
prstenu	prsten	k1gInSc6	prsten
zatoužil	zatoužit	k5eAaPmAgMnS	zatoužit
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Zjistil	zjistit	k5eAaPmAgMnS	zjistit
místo	místo	k1gNnSc4	místo
Isildurovy	Isildurův	k2eAgFnSc2d1	Isildurova
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
se	se	k3xPyFc4	se
v	v	k7c6	v
těch	ten	k3xDgFnPc6	ten
končinách	končina	k1gFnPc6	končina
prsten	prsten	k1gInSc4	prsten
nalézt	nalézt	k5eAaPmF	nalézt
<g/>
.	.	kIx.	.
</s>
<s>
Totéž	týž	k3xTgNnSc4	týž
však	však	k9	však
zjistil	zjistit	k5eAaPmAgMnS	zjistit
i	i	k9	i
Sauron	Sauron	k1gMnSc1	Sauron
a	a	k8xC	a
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
Saruman	Saruman	k1gMnSc1	Saruman
nechtěl	chtít	k5eNaImAgMnS	chtít
nechat	nechat	k5eAaPmF	nechat
předběhnout	předběhnout	k5eAaPmF	předběhnout
<g/>
,	,	kIx,	,
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
s	s	k7c7	s
útokem	útok	k1gInSc7	útok
na	na	k7c4	na
Temný	temný	k2eAgInSc4d1	temný
hvozd	hvozd	k1gInSc4	hvozd
ležící	ležící	k2eAgFnSc2d1	ležící
poblíž	poblíž	k6eAd1	poblíž
<g/>
.	.	kIx.	.
</s>
<s>
Sauron	Sauron	k1gMnSc1	Sauron
však	však	k9	však
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
do	do	k7c2	do
Mordoru	Mordor	k1gInSc2	Mordor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
obnovil	obnovit	k5eAaPmAgMnS	obnovit
Barad-dû	Baradû	k1gMnSc1	Barad-dû
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeden	jeden	k4xCgInSc1	jeden
prsten	prsten	k1gInSc1	prsten
nalezl	naleznout	k5eAaPmAgInS	naleznout
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
někdo	někdo	k3yInSc1	někdo
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
obyvatel	obyvatel	k1gMnPc2	obyvatel
okolí	okolí	k1gNnSc2	okolí
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
Glum	Glum	k1gInSc4	Glum
<g/>
,	,	kIx,	,
a	a	k8xC	a
odnesl	odnést	k5eAaPmAgMnS	odnést
jej	on	k3xPp3gInSc4	on
do	do	k7c2	do
podzemí	podzemí	k1gNnSc2	podzemí
hor.	hor.	k?	hor.
Od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
jej	on	k3xPp3gMnSc4	on
získal	získat	k5eAaPmAgMnS	získat
hobit	hobit	k1gMnSc1	hobit
Bilbo	Bilba	k1gFnSc5	Bilba
na	na	k7c6	na
trpasličí	trpasličí	k2eAgFnSc6d1	trpasličí
výpravě	výprava	k1gFnSc6	výprava
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
se	se	k3xPyFc4	se
o	o	k7c6	o
znovuobjevení	znovuobjevení	k1gNnSc6	znovuobjevení
prstenu	prsten	k1gInSc2	prsten
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
Gandalf	Gandalf	k1gMnSc1	Gandalf
<g/>
,	,	kIx,	,
zvěst	zvěst	k1gMnSc1	zvěst
se	se	k3xPyFc4	se
však	však	k9	však
dostala	dostat	k5eAaPmAgFnS	dostat
i	i	k9	i
k	k	k7c3	k
Sauronovi	Sauron	k1gMnSc3	Sauron
a	a	k8xC	a
tak	tak	k6eAd1	tak
započala	započnout	k5eAaPmAgFnS	započnout
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
Jeden	jeden	k4xCgInSc1	jeden
prsten	prsten	k1gInSc1	prsten
zničen	zničit	k5eAaPmNgInS	zničit
a	a	k8xC	a
Sauron	Sauron	k1gInSc1	Sauron
navždy	navždy	k6eAd1	navždy
poražen	porazit	k5eAaPmNgMnS	porazit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
v	v	k7c6	v
Pánu	pán	k1gMnSc6	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Gondoru	Gondor	k1gInSc6	Gondor
bylo	být	k5eAaImAgNnS	být
obnoveno	obnoven	k2eAgNnSc1d1	obnoveno
království	království	k1gNnSc1	království
a	a	k8xC	a
pod	pod	k7c4	pod
jeho	jeho	k3xOp3gFnSc4	jeho
moc	moc	k1gFnSc4	moc
byla	být	k5eAaImAgFnS	být
zahrnuta	zahrnut	k2eAgFnSc1d1	zahrnuta
celá	celý	k2eAgFnSc1d1	celá
západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
Středozemě	Středozem	k1gFnSc2	Středozem
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
věk	věk	k1gInSc1	věk
skončil	skončit	k5eAaPmAgInS	skončit
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgMnPc1d1	poslední
elfové	elf	k1gMnPc1	elf
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
přišli	přijít	k5eAaPmAgMnP	přijít
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Fëanora	Fëanor	k1gMnSc2	Fëanor
do	do	k7c2	do
Středozemě	Středozem	k1gFnSc2	Středozem
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
Západ	západ	k1gInSc4	západ
do	do	k7c2	do
zemí	zem	k1gFnPc2	zem
neumírajících	umírající	k2eNgFnPc2d1	neumírající
a	a	k8xC	a
ve	v	k7c4	v
Středozemi	Středozem	k1gFnSc4	Středozem
začal	začít	k5eAaPmAgInS	začít
Čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
věk	věk	k1gInSc1	věk
<g/>
,	,	kIx,	,
panování	panování	k1gNnSc1	panování
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
TOLKIEN	TOLKIEN	kA	TOLKIEN
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Ronald	Ronald	k1gMnSc1	Ronald
Reuel	Reuel	k1gMnSc1	Reuel
<g/>
.	.	kIx.	.
</s>
<s>
Silmarillion	Silmarillion	k1gInSc1	Silmarillion
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Christopher	Christophra	k1gFnPc2	Christophra
Tolkien	Tolkina	k1gFnPc2	Tolkina
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
320	[number]	k4	320
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
336	[number]	k4	336
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DAY	DAY	kA	DAY
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
.	.	kIx.	.
</s>
<s>
Průvodce	průvodce	k1gMnSc1	průvodce
světem	svět	k1gInSc7	svět
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkiena	Tolkien	k1gMnSc2	Tolkien
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1	ilustrace
Victor	Victor	k1gMnSc1	Victor
Ambrus	Ambrus	k1gMnSc1	Ambrus
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
..	..	k?	..
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
271	[number]	k4	271
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
540	[number]	k4	540
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
Ardy	Arda	k1gFnSc2	Arda
</s>
</p>
<p>
<s>
Beleriand	Beleriand	k1gInSc1	Beleriand
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Silmarillion	Silmarillion	k1gInSc1	Silmarillion
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
DÍLO	dílo	k1gNnSc1	dílo
<g/>
:	:	kIx,	:
Silmarillion	Silmarillion	k1gInSc1	Silmarillion
-	-	kIx~	-
Tolkien	Tolkien	k2eAgMnSc1d1	Tolkien
filolog	filolog	k1gMnSc1	filolog
a	a	k8xC	a
mýtotvůrce	mýtotvůrce	k1gMnSc1	mýtotvůrce
na	na	k7c4	na
tolkien	tolkien	k1gInSc4	tolkien
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
