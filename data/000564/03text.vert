<s>
Petr	Petr	k1gMnSc1	Petr
Čech	Čech	k1gMnSc1	Čech
(	(	kIx(	(
<g/>
*	*	kIx~	*
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1982	[number]	k4	1982
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
brankář	brankář	k1gMnSc1	brankář
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
reprezentant	reprezentant	k1gMnSc1	reprezentant
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
brankářem	brankář	k1gMnSc7	brankář
anglického	anglický	k2eAgInSc2d1	anglický
Arsenalu	Arsenal	k1gInSc2	Arsenal
<g/>
.	.	kIx.	.
</s>
<s>
Mistr	mistr	k1gMnSc1	mistr
Evropy	Evropa	k1gFnSc2	Evropa
hráčů	hráč	k1gMnPc2	hráč
do	do	k7c2	do
21	[number]	k4	21
let	léto	k1gNnPc2	léto
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
týmech	tým	k1gInPc6	tým
Viktorie	Viktoria	k1gFnSc2	Viktoria
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
,	,	kIx,	,
Chmelu	chmel	k1gInSc3	chmel
Blšany	Blšana	k1gFnSc2	Blšana
<g/>
,	,	kIx,	,
Sparty	Sparta	k1gFnSc2	Sparta
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Rennes	Rennes	k1gMnSc1	Rennes
a	a	k8xC	a
Chelsea	Chelsea	k1gMnSc1	Chelsea
FC	FC	kA	FC
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
fotbalových	fotbalový	k2eAgMnPc2d1	fotbalový
brankářů	brankář	k1gMnPc2	brankář
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Milanu	Milan	k1gMnSc6	Milan
Barošovi	Baroš	k1gMnSc6	Baroš
<g/>
,	,	kIx,	,
Vladimíru	Vladimír	k1gMnSc6	Vladimír
Šmicerovi	Šmicer	k1gMnSc6	Šmicer
a	a	k8xC	a
Marku	Marek	k1gMnSc6	Marek
Jankulovském	Jankulovský	k2eAgInSc6d1	Jankulovský
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
čtvrtým	čtvrtý	k4xOgNnSc7	čtvrtý
českým	český	k2eAgMnSc7d1	český
vítězem	vítěz	k1gMnSc7	vítěz
prestižní	prestižní	k2eAgFnSc2d1	prestižní
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
<g/>
.	.	kIx.	.
</s>
<s>
Klubový	klubový	k2eAgInSc1d1	klubový
rekord	rekord	k1gInSc1	rekord
Chelsea	Chelse	k1gInSc2	Chelse
FC	FC	kA	FC
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
vychytaných	vychytaný	k2eAgFnPc2d1	vychytaná
nul	nula	k1gFnPc2	nula
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
soutěžích	soutěž	k1gFnPc6	soutěž
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
držel	držet	k5eAaImAgInS	držet
s	s	k7c7	s
208	[number]	k4	208
čistými	čistý	k2eAgInPc7d1	čistý
konty	konto	k1gNnPc7	konto
Peter	Peter	k1gMnSc1	Peter
Bonetti	Bonetť	k1gFnPc4	Bonetť
<g/>
,	,	kIx,	,
překonal	překonat	k5eAaPmAgMnS	překonat
11	[number]	k4	11
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
držitelem	držitel	k1gMnSc7	držitel
rekordu	rekord	k1gInSc2	rekord
anglické	anglický	k2eAgFnSc2d1	anglická
Premier	Premiero	k1gNnPc2	Premiero
League	League	k1gFnPc7	League
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
vychytaných	vychytaný	k2eAgNnPc2d1	vychytané
čistých	čistý	k2eAgNnPc2d1	čisté
kont	konto	k1gNnPc2	konto
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
fotbalistů	fotbalista	k1gMnPc2	fotbalista
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
kontě	konto	k1gNnSc6	konto
nejvíce	nejvíce	k6eAd1	nejvíce
zápasů	zápas	k1gInPc2	zápas
za	za	k7c4	za
českou	český	k2eAgFnSc4d1	Česká
fotbalovou	fotbalový	k2eAgFnSc4d1	fotbalová
reprezentaci	reprezentace	k1gFnSc4	reprezentace
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
124	[number]	k4	124
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2003	[number]	k4	2003
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
dlouholetou	dlouholetý	k2eAgFnSc7d1	dlouholetá
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
Martinou	Martina	k1gFnSc7	Martina
Dolejšovou	Dolejšová	k1gFnSc7	Dolejšová
<g/>
,	,	kIx,	,
spolužačkou	spolužačka	k1gFnSc7	spolužačka
ze	z	k7c2	z
Sportovního	sportovní	k2eAgNnSc2d1	sportovní
gymnázia	gymnázium	k1gNnSc2	gymnázium
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
má	mít	k5eAaImIp3nS	mít
dceru	dcera	k1gFnSc4	dcera
Adélu	Adéla	k1gFnSc4	Adéla
(	(	kIx(	(
<g/>
*	*	kIx~	*
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
a	a	k8xC	a
syna	syn	k1gMnSc4	syn
Damiána	Damián	k1gMnSc4	Damián
(	(	kIx(	(
<g/>
*	*	kIx~	*
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gInPc7	jeho
záliby	zálib	k1gInPc7	zálib
patří	patřit	k5eAaImIp3nS	patřit
hra	hra	k1gFnSc1	hra
na	na	k7c4	na
bicí	bicí	k2eAgInSc4d1	bicí
<g/>
,	,	kIx,	,
na	na	k7c4	na
YouTube	YouTub	k1gInSc5	YouTub
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
coververze	coververze	k1gFnPc4	coververze
jeho	jeho	k3xOp3gFnPc2	jeho
oblíbených	oblíbený	k2eAgFnPc2d1	oblíbená
kapel	kapela	k1gFnPc2	kapela
Nirvana	Nirvan	k1gMnSc2	Nirvan
<g/>
,	,	kIx,	,
Foo	Foo	k1gMnPc1	Foo
Fighters	Fightersa	k1gFnPc2	Fightersa
a	a	k8xC	a
System	Systo	k1gNnSc7	Systo
of	of	k?	of
a	a	k8xC	a
Down	Down	k1gInSc1	Down
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
žákovských	žákovský	k2eAgInPc6d1	žákovský
výběrech	výběr	k1gInPc6	výběr
začal	začít	k5eAaPmAgMnS	začít
jako	jako	k9	jako
útočník	útočník	k1gMnSc1	útočník
<g/>
,	,	kIx,	,
až	až	k9	až
ve	v	k7c4	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
třídě	třída	k1gFnSc6	třída
si	se	k3xPyFc3	se
jej	on	k3xPp3gNnSc4	on
vyhlédl	vyhlédnout	k5eAaPmAgMnS	vyhlédnout
trenér	trenér	k1gMnSc1	trenér
gólmanů	gólman	k1gMnPc2	gólman
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
pevně	pevně	k6eAd1	pevně
uchytil	uchytit	k5eAaPmAgMnS	uchytit
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
mezi	mezi	k7c7	mezi
tyčemi	tyč	k1gFnPc7	tyč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
kdy	kdy	k6eAd1	kdy
hrál	hrát	k5eAaImAgMnS	hrát
za	za	k7c4	za
dorost	dorost	k1gInSc4	dorost
Viktorky	Viktorka	k1gFnSc2	Viktorka
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
reprezentačním	reprezentační	k2eAgInSc6d1	reprezentační
výběru	výběr	k1gInSc6	výběr
(	(	kIx(	(
<g/>
do	do	k7c2	do
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
vystoupení	vystoupení	k1gNnSc1	vystoupení
na	na	k7c6	na
ME	ME	kA	ME
do	do	k7c2	do
16	[number]	k4	16
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
gólmanem	gólman	k1gMnSc7	gólman
turnaje	turnaj	k1gInSc2	turnaj
<g/>
,	,	kIx,	,
předznamenalo	předznamenat	k5eAaPmAgNnS	předznamenat
začátek	začátek	k1gInSc4	začátek
veleúspěšné	veleúspěšný	k2eAgFnSc2d1	veleúspěšná
kariéry	kariéra	k1gFnSc2	kariéra
.	.	kIx.	.
</s>
<s>
FC	FC	kA	FC
Viktoria	Viktoria	k1gFnSc1	Viktoria
Plzeň	Plzeň	k1gFnSc1	Plzeň
však	však	k9	však
nedokázala	dokázat	k5eNaPmAgFnS	dokázat
ocenit	ocenit	k5eAaPmF	ocenit
kvality	kvalita	k1gFnSc2	kvalita
mladého	mladý	k2eAgMnSc4d1	mladý
brankáře	brankář	k1gMnSc4	brankář
a	a	k8xC	a
za	za	k7c4	za
pouhých	pouhý	k2eAgInPc2d1	pouhý
350	[number]	k4	350
tisíc	tisíc	k4xCgInPc2	tisíc
Kč	Kč	kA	Kč
jej	on	k3xPp3gNnSc4	on
přepustila	přepustit	k5eAaPmAgFnS	přepustit
do	do	k7c2	do
klubu	klub	k1gInSc2	klub
FK	FK	kA	FK
Chmel	Chmel	k1gMnSc1	Chmel
Blšany	Blšana	k1gFnSc2	Blšana
.	.	kIx.	.
</s>
<s>
Přestup	přestup	k1gInSc1	přestup
do	do	k7c2	do
Blšan	Blšana	k1gFnPc2	Blšana
nicméně	nicméně	k8xC	nicméně
znamenal	znamenat	k5eAaImAgInS	znamenat
významný	významný	k2eAgInSc1d1	významný
zlom	zlom	k1gInSc1	zlom
v	v	k7c6	v
Čechově	Čechův	k2eAgFnSc6d1	Čechova
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
totiž	totiž	k9	totiž
dočkal	dočkat	k5eAaPmAgMnS	dočkat
prvoligového	prvoligový	k2eAgInSc2d1	prvoligový
debutu	debut	k1gInSc2	debut
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1999	[number]	k4	1999
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
17	[number]	k4	17
let	léto	k1gNnPc2	léto
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
k	k	k7c3	k
utkání	utkání	k1gNnSc3	utkání
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
Spartě	Sparta	k1gFnSc6	Sparta
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
FK	FK	kA	FK
Chmel	chmel	k1gInSc4	chmel
Blšany	Blšana	k1gFnSc2	Blšana
odehrál	odehrát	k5eAaPmAgMnS	odehrát
během	během	k7c2	během
dvou	dva	k4xCgFnPc2	dva
sezón	sezóna	k1gFnPc2	sezóna
celkem	celkem	k6eAd1	celkem
27	[number]	k4	27
zápasů	zápas	k1gInPc2	zápas
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
sezóně	sezóna	k1gFnSc6	sezóna
díky	díky	k7c3	díky
svým	svůj	k3xOyFgInPc3	svůj
výkonům	výkon	k1gInPc3	výkon
již	již	k6eAd1	již
zastával	zastávat	k5eAaImAgMnS	zastávat
pozici	pozice	k1gFnSc4	pozice
brankářské	brankářský	k2eAgFnSc2d1	brankářská
jedničky	jednička	k1gFnSc2	jednička
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
rovněž	rovněž	k9	rovněž
pravidelně	pravidelně	k6eAd1	pravidelně
reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
Česko	Česko	k1gNnSc4	Česko
v	v	k7c6	v
juniorských	juniorský	k2eAgInPc6d1	juniorský
výběrech	výběr	k1gInPc6	výběr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2001	[number]	k4	2001
jej	on	k3xPp3gMnSc4	on
za	za	k7c4	za
22	[number]	k4	22
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
získala	získat	k5eAaPmAgFnS	získat
AC	AC	kA	AC
Sparta	Sparta	k1gFnSc1	Sparta
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ponechala	ponechat	k5eAaPmAgFnS	ponechat
jej	on	k3xPp3gMnSc4	on
na	na	k7c6	na
hostování	hostování	k1gNnSc6	hostování
v	v	k7c6	v
Blšanech	Blšan	k1gInPc6	Blšan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
devatenácti	devatenáct	k4xCc6	devatenáct
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
tak	tak	k9	tak
stal	stát	k5eAaPmAgInS	stát
nejdražším	drahý	k2eAgMnSc7d3	nejdražší
českým	český	k2eAgMnSc7d1	český
brankářem	brankář	k1gMnSc7	brankář
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
jako	jako	k8xS	jako
brankářská	brankářský	k2eAgFnSc1d1	brankářská
jednička	jednička	k1gFnSc1	jednička
Mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
hráčů	hráč	k1gMnPc2	hráč
do	do	k7c2	do
20	[number]	k4	20
let	léto	k1gNnPc2	léto
v	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
<g/>
,	,	kIx,	,
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
2001	[number]	k4	2001
na	na	k7c4	na
Letnou	Letná	k1gFnSc4	Letná
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
zde	zde	k6eAd1	zde
český	český	k2eAgInSc4d1	český
ligový	ligový	k2eAgInSc4d1	ligový
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
neprůstřelnosti	neprůstřelnost	k1gFnSc6	neprůstřelnost
<g/>
,	,	kIx,	,
gól	gól	k1gInSc4	gól
neinkasoval	inkasovat	k5eNaBmAgMnS	inkasovat
rovných	rovný	k2eAgFnPc2d1	rovná
903	[number]	k4	903
minut	minuta	k1gFnPc2	minuta
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
v	v	k7c6	v
dresu	dres	k1gInSc6	dres
Sparty	Sparta	k1gFnSc2	Sparta
odchytal	odchytat	k5eAaPmAgInS	odchytat
27	[number]	k4	27
utkání	utkání	k1gNnPc2	utkání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lize	liga	k1gFnSc6	liga
však	však	k9	však
Sparta	Sparta	k1gFnSc1	Sparta
skončila	skončit	k5eAaPmAgFnS	skončit
druhá	druhý	k4xOgFnSc1	druhý
za	za	k7c7	za
Slovanem	Slovan	k1gInSc7	Slovan
Liberec	Liberec	k1gInSc1	Liberec
a	a	k8xC	a
český	český	k2eAgInSc1d1	český
titul	titul	k1gInSc1	titul
tak	tak	k6eAd1	tak
v	v	k7c6	v
Čechově	Čechův	k2eAgFnSc6d1	Čechova
sbírce	sbírka	k1gFnSc6	sbírka
chybí	chybit	k5eAaPmIp3nS	chybit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2002	[number]	k4	2002
zazářil	zazářit	k5eAaPmAgMnS	zazářit
na	na	k7c6	na
ME	ME	kA	ME
hráčů	hráč	k1gMnPc2	hráč
do	do	k7c2	do
21	[number]	k4	21
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
proti	proti	k7c3	proti
Francii	Francie	k1gFnSc3	Francie
v	v	k7c6	v
penaltovém	penaltový	k2eAgInSc6d1	penaltový
rozstřelu	rozstřel	k1gInSc6	rozstřel
pustil	pustit	k5eAaPmAgMnS	pustit
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
střel	střela	k1gFnPc2	střela
jediný	jediný	k2eAgInSc4d1	jediný
gól	gól	k1gInSc4	gól
a	a	k8xC	a
vychytal	vychytat	k5eAaPmAgMnS	vychytat
tak	tak	k6eAd1	tak
pro	pro	k7c4	pro
Česko	Česko	k1gNnSc4	Česko
historické	historický	k2eAgNnSc4d1	historické
vítězství	vítězství	k1gNnSc4	vítězství
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
skvělé	skvělý	k2eAgInPc1d1	skvělý
výkony	výkon	k1gInPc1	výkon
přitáhly	přitáhnout	k5eAaPmAgInP	přitáhnout
zájem	zájem	k1gInSc4	zájem
pozorovatelů	pozorovatel	k1gMnPc2	pozorovatel
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
velkoklubů	velkoklub	k1gInPc2	velkoklub
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2002	[number]	k4	2002
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
do	do	k7c2	do
francouzského	francouzský	k2eAgInSc2d1	francouzský
klubu	klub	k1gInSc2	klub
Stade	Stad	k1gInSc5	Stad
Rennes	Rennes	k1gInSc4	Rennes
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
velkou	velký	k2eAgFnSc7d1	velká
brankářskou	brankářský	k2eAgFnSc7d1	brankářská
oporou	opora	k1gFnSc7	opora
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
jeho	jeho	k3xOp3gInPc4	jeho
výborné	výborný	k2eAgInPc4d1	výborný
výkony	výkon	k1gInPc4	výkon
se	se	k3xPyFc4	se
však	však	k9	však
klub	klub	k1gInSc1	klub
pohyboval	pohybovat	k5eAaImAgInS	pohybovat
většinou	většinou	k6eAd1	většinou
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
polovině	polovina	k1gFnSc6	polovina
tabulky	tabulka	k1gFnSc2	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kontě	konto	k1gNnSc6	konto
má	mít	k5eAaImIp3nS	mít
70	[number]	k4	70
startů	start	k1gInPc2	start
ve	v	k7c6	v
francouzské	francouzský	k2eAgFnSc6d1	francouzská
lize	liga	k1gFnSc6	liga
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc2	jenž
vychytal	vychytat	k5eAaPmAgMnS	vychytat
22	[number]	k4	22
čistých	čistý	k2eAgNnPc2d1	čisté
kont	konto	k1gNnPc2	konto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
už	už	k6eAd1	už
byl	být	k5eAaImAgInS	být
oporou	opora	k1gFnSc7	opora
seniorské	seniorský	k2eAgFnSc2d1	seniorská
reprezentace	reprezentace	k1gFnSc2	reprezentace
Česka	Česko	k1gNnSc2	Česko
v	v	k7c6	v
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
na	na	k7c4	na
ME	ME	kA	ME
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
2004	[number]	k4	2004
představuje	představovat	k5eAaImIp3nS	představovat
zatím	zatím	k6eAd1	zatím
největší	veliký	k2eAgInSc1d3	veliký
úspěch	úspěch	k1gInSc1	úspěch
Čechovy	Čechův	k2eAgFnSc2d1	Čechova
reprezentační	reprezentační	k2eAgFnSc2d1	reprezentační
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
týmem	tým	k1gInSc7	tým
Česka	Česko	k1gNnSc2	Česko
došel	dojít	k5eAaPmAgInS	dojít
až	až	k6eAd1	až
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
prodloužení	prodloužení	k1gNnSc6	prodloužení
podlehli	podlehnout	k5eAaPmAgMnP	podlehnout
pozdějším	pozdní	k2eAgMnPc3d2	pozdější
vítězům	vítěz	k1gMnPc3	vítěz
turnaje	turnaj	k1gInSc2	turnaj
z	z	k7c2	z
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
mistrovstvím	mistrovství	k1gNnSc7	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
s	s	k7c7	s
letním	letní	k2eAgInSc7d1	letní
přestupem	přestup	k1gInSc7	přestup
do	do	k7c2	do
londýnské	londýnský	k2eAgFnSc2d1	londýnská
Chelsea	Chelseus	k1gMnSc2	Chelseus
F.	F.	kA	F.
<g/>
C.	C.	kA	C.
za	za	k7c4	za
téměř	téměř	k6eAd1	téměř
půl	půl	k1xP	půl
miliardy	miliarda	k4xCgFnSc2	miliarda
korun	koruna	k1gFnPc2	koruna
a	a	k8xC	a
podepsal	podepsat	k5eAaPmAgMnS	podepsat
pětiletý	pětiletý	k2eAgInSc4d1	pětiletý
kontrakt	kontrakt	k1gInSc4	kontrakt
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
startem	start	k1gInSc7	start
sezóny	sezóna	k1gFnSc2	sezóna
počítal	počítat	k5eAaImAgInS	počítat
nový	nový	k2eAgMnSc1d1	nový
kouč	kouč	k1gMnSc1	kouč
José	José	k1gNnSc2	José
Mourinho	Mourin	k1gMnSc2	Mourin
s	s	k7c7	s
Italem	Ital	k1gMnSc7	Ital
Carlem	Carl	k1gMnSc7	Carl
Cudicinim	Cudicinima	k1gFnPc2	Cudicinima
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
brankářské	brankářský	k2eAgFnSc2d1	brankářská
jedničky	jednička	k1gFnSc2	jednička
<g/>
,	,	kIx,	,
Čech	Čech	k1gMnSc1	Čech
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
náhradníkem	náhradník	k1gMnSc7	náhradník
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
Cudicini	Cudicin	k2eAgMnPc1d1	Cudicin
si	se	k3xPyFc3	se
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
startem	start	k1gInSc7	start
ligy	liga	k1gFnSc2	liga
poranil	poranit	k5eAaPmAgInS	poranit
loket	loket	k1gInSc1	loket
a	a	k8xC	a
Čech	Čech	k1gMnSc1	Čech
tak	tak	k6eAd1	tak
dostal	dostat	k5eAaPmAgMnS	dostat
šanci	šance	k1gFnSc4	šance
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
naplno	naplno	k6eAd1	naplno
využil	využít	k5eAaPmAgMnS	využít
<g/>
.	.	kIx.	.
</s>
<s>
Skvělými	skvělý	k2eAgInPc7d1	skvělý
výkony	výkon	k1gInPc7	výkon
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
kouče	kouč	k1gMnPc4	kouč
a	a	k8xC	a
převzal	převzít	k5eAaPmAgMnS	převzít
na	na	k7c4	na
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
postavení	postavení	k1gNnSc2	postavení
brankářské	brankářský	k2eAgFnSc2d1	brankářská
jedničky	jednička	k1gFnSc2	jednička
Chelsea	Chelsea	k1gMnSc1	Chelsea
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2005	[number]	k4	2005
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
nový	nový	k2eAgInSc1d1	nový
rekord	rekord	k1gInSc1	rekord
Premier	Premira	k1gFnPc2	Premira
League	Leagu	k1gInSc2	Leagu
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
minut	minuta	k1gFnPc2	minuta
(	(	kIx(	(
<g/>
1025	[number]	k4	1025
<g/>
)	)	kIx)	)
bez	bez	k7c2	bez
inkasovaného	inkasovaný	k2eAgInSc2d1	inkasovaný
gólu	gól	k1gInSc2	gól
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc1	tento
rekord	rekord	k1gInSc1	rekord
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
překonán	překonat	k5eAaPmNgInS	překonat
Edwinem	Edwin	k1gInSc7	Edwin
van	van	k1gInSc1	van
der	drát	k5eAaImRp2nS	drát
Sarem	Sar	k1gMnSc7	Sar
z	z	k7c2	z
Manchesteru	Manchester	k1gInSc2	Manchester
United	United	k1gInSc1	United
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čech	Čech	k1gMnSc1	Čech
udržel	udržet	k5eAaPmAgMnS	udržet
čisté	čistý	k2eAgNnSc4d1	čisté
konto	konto	k1gNnSc4	konto
od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dostal	dostat	k5eAaPmAgMnS	dostat
gól	gól	k1gInSc4	gól
od	od	k7c2	od
útočníka	útočník	k1gMnSc2	útočník
Arsenalu	Arsenal	k1gInSc2	Arsenal
Londýn	Londýn	k1gInSc1	Londýn
Thierry	Thierra	k1gMnSc2	Thierra
Henryho	Henry	k1gMnSc2	Henry
do	do	k7c2	do
zmíněného	zmíněný	k2eAgNnSc2d1	zmíněné
5	[number]	k4	5
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gMnSc4	on
překonal	překonat	k5eAaPmAgMnS	překonat
Leon	Leona	k1gFnPc2	Leona
McKenzie	McKenzie	k1gFnSc2	McKenzie
z	z	k7c2	z
Norwich	Norwicha	k1gFnPc2	Norwicha
City	City	k1gFnSc2	City
FC	FC	kA	FC
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
vychytal	vychytat	k5eAaPmAgMnS	vychytat
celkem	celkem	k6eAd1	celkem
21	[number]	k4	21
čistých	čistý	k2eAgNnPc2d1	čisté
kont	konto	k1gNnPc2	konto
a	a	k8xC	a
velkou	velký	k2eAgFnSc7d1	velká
měrou	míra	k1gFnSc7wR	míra
tak	tak	k6eAd1	tak
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
historicky	historicky	k6eAd1	historicky
druhému	druhý	k4xOgNnSc3	druhý
vítězství	vítězství	k1gNnSc3	vítězství
Chelsea	Chelse	k1gInSc2	Chelse
F.	F.	kA	F.
<g/>
C.	C.	kA	C.
v	v	k7c6	v
Premier	Premira	k1gFnPc2	Premira
League	Leagu	k1gInSc2	Leagu
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2005	[number]	k4	2005
stál	stát	k5eAaImAgMnS	stát
Čech	Čech	k1gMnSc1	Čech
v	v	k7c6	v
brance	branka	k1gFnSc6	branka
Chelsea	Chelseus	k1gMnSc2	Chelseus
F.	F.	kA	F.
<g/>
C.	C.	kA	C.
při	při	k7c6	při
vítězství	vítězství	k1gNnSc6	vítězství
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
Superpoháru	superpohár	k1gInSc6	superpohár
<g/>
.	.	kIx.	.
</s>
<s>
Chelsea	Chelsea	k1gFnSc1	Chelsea
porazila	porazit	k5eAaPmAgFnS	porazit
dvěma	dva	k4xCgFnPc7	dva
góly	gól	k1gInPc7	gól
Didiera	Didier	k1gMnSc2	Didier
Drogby	Drogba	k1gMnSc2	Drogba
Arsenal	Arsenal	k1gFnSc2	Arsenal
FC	FC	kA	FC
2-1	[number]	k4	2-1
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Premier	Premier	k1gInSc4	Premier
League	League	k1gFnPc2	League
Čech	Čech	k1gMnSc1	Čech
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
k	k	k7c3	k
celkem	celkem	k6eAd1	celkem
34	[number]	k4	34
ligovým	ligový	k2eAgInPc3d1	ligový
zápasům	zápas	k1gInPc3	zápas
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
velký	velký	k2eAgInSc1d1	velký
podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
obhajobě	obhajoba	k1gFnSc6	obhajoba
ligového	ligový	k2eAgInSc2d1	ligový
titulu	titul	k1gInSc2	titul
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2006	[number]	k4	2006
prodloužil	prodloužit	k5eAaPmAgInS	prodloužit
kontrakt	kontrakt	k1gInSc1	kontrakt
s	s	k7c7	s
Chelsea	Chelseum	k1gNnSc2	Chelseum
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2006	[number]	k4	2006
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
dresu	dres	k1gInSc6	dres
Česka	Česko	k1gNnSc2	Česko
na	na	k7c4	na
MS	MS	kA	MS
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
sice	sice	k8xC	sice
porazil	porazit	k5eAaPmAgMnS	porazit
USA	USA	kA	USA
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
po	po	k7c6	po
následných	následný	k2eAgFnPc6d1	následná
porážkách	porážka	k1gFnPc6	porážka
s	s	k7c7	s
Ghanou	Ghana	k1gFnSc7	Ghana
0-2	[number]	k4	0-2
a	a	k8xC	a
s	s	k7c7	s
Itálií	Itálie	k1gFnSc7	Itálie
(	(	kIx(	(
<g/>
také	také	k9	také
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
skončil	skončit	k5eAaPmAgInS	skončit
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
skupině	skupina	k1gFnSc6	skupina
třetí	třetí	k4xOgFnSc2	třetí
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
nepostoupil	postoupit	k5eNaPmAgInS	postoupit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
MS	MS	kA	MS
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
Čech	Čech	k1gMnSc1	Čech
prodělal	prodělat	k5eAaPmAgMnS	prodělat
artroskopickou	artroskopický	k2eAgFnSc4d1	artroskopická
operaci	operace	k1gFnSc4	operace
obou	dva	k4xCgInPc2	dva
ramen	rameno	k1gNnPc2	rameno
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yRgMnPc7	který
měl	mít	k5eAaImAgMnS	mít
dlouhodobé	dlouhodobý	k2eAgInPc4d1	dlouhodobý
problémy	problém	k1gInPc4	problém
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
hry	hra	k1gFnSc2	hra
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2006	[number]	k4	2006
během	během	k7c2	během
ligového	ligový	k2eAgNnSc2d1	ligové
utkání	utkání	k1gNnSc2	utkání
proti	proti	k7c3	proti
Readingu	Reading	k1gInSc3	Reading
FC	FC	kA	FC
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
český	český	k2eAgMnSc1d1	český
brankář	brankář	k1gMnSc1	brankář
Chelsea	Chelsea	k1gMnSc1	Chelsea
frakturu	fraktura	k1gFnSc4	fraktura
lebky	lebka	k1gFnSc2	lebka
při	při	k7c6	při
střetu	střet	k1gInSc6	střet
s	s	k7c7	s
irským	irský	k2eAgMnSc7d1	irský
záložníkem	záložník	k1gMnSc7	záložník
soupeře	soupeř	k1gMnSc2	soupeř
Stephenem	Stephen	k1gMnSc7	Stephen
Huntem	hunt	k1gInSc7	hunt
<g/>
.	.	kIx.	.
</s>
<s>
Hunt	hunt	k1gInSc1	hunt
dobíhal	dobíhat	k5eAaImAgInS	dobíhat
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
utkání	utkání	k1gNnSc4	utkání
míč	míč	k1gInSc1	míč
uvnitř	uvnitř	k7c2	uvnitř
pokutového	pokutový	k2eAgNnSc2d1	pokutové
území	území	k1gNnSc2	území
Chelsea	Chelse	k1gInSc2	Chelse
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Čech	Čech	k1gMnSc1	Čech
byl	být	k5eAaImAgMnS	být
u	u	k7c2	u
něj	on	k3xPp3gNnSc2	on
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Hunt	hunt	k1gInSc1	hunt
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
pravým	pravý	k2eAgNnSc7d1	pravé
kolenem	koleno	k1gNnSc7	koleno
hlavu	hlava	k1gFnSc4	hlava
gólmana	gólman	k1gMnSc4	gólman
Chelsea	Chelseus	k1gMnSc4	Chelseus
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
si	se	k3xPyFc3	se
vyžádalo	vyžádat	k5eAaPmAgNnS	vyžádat
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
ošetřování	ošetřování	k1gNnSc1	ošetřování
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byl	být	k5eAaImAgMnS	být
brankář	brankář	k1gMnSc1	brankář
odnesen	odnést	k5eAaPmNgMnS	odnést
na	na	k7c6	na
nosítkách	nosítka	k1gNnPc6	nosítka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
branky	branka	k1gFnSc2	branka
Chelsea	Chelse	k1gInSc2	Chelse
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
náhradní	náhradní	k2eAgMnSc1d1	náhradní
brankář	brankář	k1gMnSc1	brankář
Carlo	Carlo	k1gNnSc4	Carlo
Cudicini	Cudicin	k2eAgMnPc1d1	Cudicin
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
také	také	k9	také
v	v	k7c6	v
tomtéž	týž	k3xTgInSc6	týž
zápase	zápas	k1gInSc6	zápas
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
zranění	zranění	k1gNnSc4	zranění
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
dokonce	dokonce	k9	dokonce
chvíli	chvíle	k1gFnSc4	chvíle
v	v	k7c6	v
bezvědomí	bezvědomí	k1gNnSc6	bezvědomí
<g/>
.	.	kIx.	.
</s>
<s>
Zápas	zápas	k1gInSc1	zápas
musel	muset	k5eAaImAgInS	muset
dochytat	dochytat	k5eAaPmF	dochytat
hráč	hráč	k1gMnSc1	hráč
z	z	k7c2	z
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
John	John	k1gMnSc1	John
Terry	Terra	k1gFnSc2	Terra
<g/>
.	.	kIx.	.
</s>
<s>
Chelsea	Chelsea	k6eAd1	Chelsea
i	i	k9	i
přesto	přesto	k8xC	přesto
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
utkání	utkání	k1gNnSc6	utkání
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
1-0	[number]	k4	1-0
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
Čech	Čech	k1gMnSc1	Čech
skončil	skončit	k5eAaPmAgMnS	skončit
v	v	k7c6	v
péči	péče	k1gFnSc6	péče
neurochirurgů	neurochirurg	k1gMnPc2	neurochirurg
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
mimo	mimo	k7c4	mimo
hru	hra	k1gFnSc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Doktoři	doktor	k1gMnPc1	doktor
brankáře	brankář	k1gMnSc4	brankář
varovali	varovat	k5eAaImAgMnP	varovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
návrat	návrat	k1gInSc4	návrat
na	na	k7c4	na
trávník	trávník	k1gInSc4	trávník
uspíší	uspíšit	k5eAaPmIp3nP	uspíšit
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
následky	následek	k1gInPc1	následek
fatální	fatální	k2eAgInPc1d1	fatální
<g/>
.	.	kIx.	.
</s>
<s>
Trenér	trenér	k1gMnSc1	trenér
Chelsea	Chelseus	k1gMnSc2	Chelseus
José	Josá	k1gFnSc2	Josá
Mourinho	Mourin	k1gMnSc2	Mourin
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
Stephena	Stephen	k1gMnSc4	Stephen
Hunta	Hunt	k1gMnSc4	Hunt
a	a	k8xC	a
také	také	k9	také
zdravotnickou	zdravotnický	k2eAgFnSc4d1	zdravotnická
službu	služba	k1gFnSc4	služba
a	a	k8xC	a
rozhodčího	rozhodčí	k1gMnSc2	rozhodčí
Mika	Mik	k1gMnSc2	Mik
Rileye	Riley	k1gMnSc2	Riley
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
komentátoři	komentátor	k1gMnPc1	komentátor
a	a	k8xC	a
bývalí	bývalý	k2eAgMnPc1d1	bývalý
brankáři	brankář	k1gMnPc1	brankář
(	(	kIx(	(
<g/>
a	a	k8xC	a
také	také	k9	také
aktivní	aktivní	k2eAgMnPc1d1	aktivní
brankáři	brankář	k1gMnPc1	brankář
<g/>
)	)	kIx)	)
po	po	k7c4	po
zranění	zranění	k1gNnSc4	zranění
Čecha	Čech	k1gMnSc2	Čech
volali	volat	k5eAaImAgMnP	volat
po	po	k7c6	po
vyšší	vysoký	k2eAgFnSc6d2	vyšší
ochraně	ochrana	k1gFnSc6	ochrana
gólmanů	gólman	k1gMnPc2	gólman
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
Čech	Čech	k1gMnSc1	Čech
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
z	z	k7c2	z
nemocnice	nemocnice	k1gFnSc2	nemocnice
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2006	[number]	k4	2006
a	a	k8xC	a
následující	následující	k2eAgInSc1d1	následující
týden	týden	k1gInSc1	týden
začal	začít	k5eAaPmAgInS	začít
s	s	k7c7	s
lehkým	lehký	k2eAgInSc7d1	lehký
tréninkem	trénink	k1gInSc7	trénink
<g/>
.	.	kIx.	.
</s>
<s>
Chelsea	Chelsea	k1gFnSc1	Chelsea
avizovala	avizovat	k5eAaBmAgFnS	avizovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
lékařským	lékařský	k2eAgNnSc7d1	lékařské
doporučením	doporučení	k1gNnSc7	doporučení
nebude	být	k5eNaImBp3nS	být
brankář	brankář	k1gMnSc1	brankář
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
3	[number]	k4	3
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
interview	interview	k1gNnSc6	interview
pro	pro	k7c4	pro
Chelsea	Chelseus	k1gMnSc4	Chelseus
TV	TV	kA	TV
Petr	Petr	k1gMnSc1	Petr
Čech	Čech	k1gMnSc1	Čech
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
srážku	srážka	k1gFnSc4	srážka
s	s	k7c7	s
Huntem	hunt	k1gInSc7	hunt
nepamatuje	pamatovat	k5eNaImIp3nS	pamatovat
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Premier	Premira	k1gFnPc2	Premira
League	Leagu	k1gFnSc2	Leagu
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
v	v	k7c6	v
utkání	utkání	k1gNnSc6	utkání
proti	proti	k7c3	proti
Liverpoolu	Liverpool	k1gInSc3	Liverpool
<g/>
.	.	kIx.	.
</s>
<s>
Hlavu	hlava	k1gFnSc4	hlava
měl	mít	k5eAaImAgInS	mít
chráněnou	chráněný	k2eAgFnSc7d1	chráněná
koženou	kožený	k2eAgFnSc7d1	kožená
helmou	helma	k1gFnSc7	helma
připomínající	připomínající	k2eAgInPc1d1	připomínající
podobné	podobný	k2eAgInPc1d1	podobný
chrániče	chránič	k1gInPc1	chránič
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
nosí	nosit	k5eAaImIp3nP	nosit
ragbisté	ragbista	k1gMnPc1	ragbista
.	.	kIx.	.
</s>
<s>
Podobnou	podobný	k2eAgFnSc4d1	podobná
helmu	helma	k1gFnSc4	helma
nosí	nosit	k5eAaImIp3nS	nosit
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Helma	helma	k1gFnSc1	helma
je	být	k5eAaImIp3nS	být
Čechovi	Čech	k1gMnSc3	Čech
vytvarována	vytvarován	k2eAgFnSc1d1	vytvarována
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
míru	míra	k1gFnSc4	míra
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vyztužena	vyztužit	k5eAaPmNgFnS	vyztužit
speciální	speciální	k2eAgFnSc7d1	speciální
polyetylenovou	polyetylenový	k2eAgFnSc7d1	polyetylenová
pěnou	pěna	k1gFnSc7	pěna
o	o	k7c6	o
vysoké	vysoký	k2eAgFnSc6d1	vysoká
hustotě	hustota	k1gFnSc6	hustota
<g/>
,	,	kIx,	,
mající	mající	k2eAgInPc4d1	mající
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
chránit	chránit	k5eAaImF	chránit
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
Čechova	Čechův	k2eAgFnSc1d1	Čechova
lebka	lebka	k1gFnSc1	lebka
při	při	k7c6	při
srážce	srážka	k1gFnSc6	srážka
poškozena	poškodit	k5eAaPmNgFnS	poškodit
.	.	kIx.	.
</s>
<s>
Utkání	utkání	k1gNnSc1	utkání
s	s	k7c7	s
Liverpoolem	Liverpool	k1gInSc7	Liverpool
sice	sice	k8xC	sice
skončilo	skončit	k5eAaPmAgNnS	skončit
prohrou	prohra	k1gFnSc7	prohra
Chelsea	Chelseum	k1gNnSc2	Chelseum
0-2	[number]	k4	0-2
,	,	kIx,	,
následně	následně	k6eAd1	následně
ovšem	ovšem	k9	ovšem
Čech	Čech	k1gMnSc1	Čech
držel	držet	k5eAaImAgMnS	držet
čisté	čistý	k2eAgNnSc4d1	čisté
konto	konto	k1gNnSc4	konto
více	hodně	k6eAd2	hodně
než	než	k8xS	než
800	[number]	k4	800
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2007	[number]	k4	2007
získal	získat	k5eAaPmAgMnS	získat
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
brankář	brankář	k1gMnSc1	brankář
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
ocenění	ocenění	k1gNnPc2	ocenění
pro	pro	k7c4	pro
Hráče	hráč	k1gMnPc4	hráč
měsíce	měsíc	k1gInSc2	měsíc
Premier	Premier	k1gMnSc1	Premier
League	League	k1gFnSc1	League
<g/>
,	,	kIx,	,
za	za	k7c4	za
osm	osm	k4xCc4	osm
čistých	čistý	k2eAgNnPc2d1	čisté
kont	konto	k1gNnPc2	konto
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
udržel	udržet	k5eAaPmAgMnS	udržet
čisté	čistý	k2eAgNnSc4d1	čisté
konto	konto	k1gNnSc4	konto
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
Anglického	anglický	k2eAgInSc2d1	anglický
poháru	pohár	k1gInSc2	pohár
proti	proti	k7c3	proti
Manchesteru	Manchester	k1gInSc2	Manchester
United	United	k1gInSc1	United
(	(	kIx(	(
<g/>
Chelsea	Chelsea	k1gFnSc1	Chelsea
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
1-0	[number]	k4	1-0
po	po	k7c4	po
prodloužení	prodloužení	k1gNnSc4	prodloužení
<g/>
)	)	kIx)	)
a	a	k8xC	a
slavil	slavit	k5eAaImAgMnS	slavit
tak	tak	k9	tak
první	první	k4xOgNnSc4	první
vítězství	vítězství	k1gNnSc4	vítězství
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
prestižní	prestižní	k2eAgFnSc6d1	prestižní
soutěži	soutěž	k1gFnSc6	soutěž
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
Chelsea	Chelseus	k1gMnSc4	Chelseus
historicky	historicky	k6eAd1	historicky
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
<g/>
)	)	kIx)	)
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Premier	Premier	k1gInSc4	Premier
League	Leagu	k1gFnSc2	Leagu
skončila	skončit	k5eAaPmAgFnS	skončit
Chelsea	Chelsea	k1gFnSc1	Chelsea
tentokrát	tentokrát	k6eAd1	tentokrát
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
právě	právě	k6eAd1	právě
za	za	k7c7	za
suverénním	suverénní	k2eAgInSc7d1	suverénní
Manchesterem	Manchester	k1gInSc7	Manchester
United	United	k1gInSc1	United
<g/>
.	.	kIx.	.
</s>
<s>
Čech	Čech	k1gMnSc1	Čech
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
sezóně	sezóna	k1gFnSc6	sezóna
bojoval	bojovat	k5eAaImAgMnS	bojovat
se	s	k7c7	s
zraněními	zranění	k1gNnPc7	zranění
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
kterým	který	k3yQgMnPc3	který
vynechal	vynechat	k5eAaPmAgInS	vynechat
celkem	celkem	k6eAd1	celkem
22	[number]	k4	22
soutěžních	soutěžní	k2eAgInPc2d1	soutěžní
zápasů	zápas	k1gInPc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
si	se	k3xPyFc3	se
zranil	zranit	k5eAaPmAgMnS	zranit
lýtkový	lýtkový	k2eAgInSc4d1	lýtkový
sval	sval	k1gInSc4	sval
pravé	pravý	k2eAgFnSc2d1	pravá
nohy	noha	k1gFnSc2	noha
,	,	kIx,	,
o	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
pro	pro	k7c4	pro
změnu	změna	k1gFnSc4	změna
zranil	zranit	k5eAaPmAgMnS	zranit
kyčel	kyčel	k1gInSc4	kyčel
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březno	k1gNnSc6	březno
si	se	k3xPyFc3	se
Čech	Čech	k1gMnSc1	Čech
v	v	k7c6	v
tréninku	trénink	k1gInSc6	trénink
zranil	zranit	k5eAaPmAgMnS	zranit
kotník	kotník	k1gInSc4	kotník
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
podrobit	podrobit	k5eAaPmF	podrobit
šití	šití	k1gNnSc4	šití
tváře	tvář	k1gFnSc2	tvář
(	(	kIx(	(
<g/>
50	[number]	k4	50
stehů	steh	k1gInPc2	steh
<g/>
)	)	kIx)	)
po	po	k7c6	po
srážce	srážka	k1gFnSc6	srážka
se	s	k7c7	s
spoluhráčem	spoluhráč	k1gMnSc7	spoluhráč
během	během	k7c2	během
tréninku	trénink	k1gInSc2	trénink
.	.	kIx.	.
</s>
<s>
Chelsea	Chelsea	k6eAd1	Chelsea
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
sezóně	sezóna	k1gFnSc6	sezóna
žádnou	žádný	k3yNgFnSc4	žádný
trofej	trofej	k1gFnSc4	trofej
nevybojovala	vybojovat	k5eNaPmAgFnS	vybojovat
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
několikrát	několikrát	k6eAd1	několikrát
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
blízko	blízko	k6eAd1	blízko
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
prohrála	prohrát	k5eAaPmAgFnS	prohrát
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
ligového	ligový	k2eAgInSc2d1	ligový
poháru	pohár	k1gInSc2	pohár
s	s	k7c7	s
Tottenhamem	Tottenham	k1gInSc7	Tottenham
Hotspur	Hotspura	k1gFnPc2	Hotspura
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
když	když	k8xS	když
Čech	Čech	k1gMnSc1	Čech
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
přímý	přímý	k2eAgInSc4d1	přímý
kop	kop	k1gInSc4	kop
Jermaina	Jermaino	k1gNnSc2	Jermaino
Jenase	Jenasa	k1gFnSc3	Jenasa
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
Jonathana	Jonathan	k1gMnSc2	Jonathan
Woodgata	Woodgat	k1gMnSc2	Woodgat
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
o	o	k7c6	o
zisku	zisk	k1gInSc6	zisk
trofeje	trofej	k1gFnSc2	trofej
pro	pro	k7c4	pro
Tottenham	Tottenham	k1gInSc4	Tottenham
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Premier	Premier	k1gInSc4	Premier
League	Leagu	k1gFnSc2	Leagu
bojovala	bojovat	k5eAaImAgFnS	bojovat
Chelsea	Chelsea	k1gFnSc1	Chelsea
do	do	k7c2	do
posledního	poslední	k2eAgNnSc2d1	poslední
kola	kolo	k1gNnSc2	kolo
o	o	k7c4	o
titul	titul	k1gInSc4	titul
s	s	k7c7	s
Manchesterem	Manchester	k1gInSc7	Manchester
United	United	k1gMnSc1	United
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
však	však	k9	však
skončila	skončit	k5eAaPmAgFnS	skončit
se	s	k7c7	s
ztrátou	ztráta	k1gFnSc7	ztráta
2	[number]	k4	2
bodů	bod	k1gInPc2	bod
druhá	druhý	k4xOgFnSc1	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc4d1	poslední
hořkou	hořký	k2eAgFnSc4d1	hořká
pilulku	pilulka	k1gFnSc4	pilulka
musela	muset	k5eAaImAgFnS	muset
Chelsea	Chelsea	k1gFnSc1	Chelsea
spolknout	spolknout	k5eAaPmF	spolknout
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
nejprestižnější	prestižní	k2eAgFnSc2d3	nejprestižnější
klubové	klubový	k2eAgFnSc2d1	klubová
soutěže	soutěž	k1gFnSc2	soutěž
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
<g/>
.	.	kIx.	.
</s>
<s>
Chelsea	Chelsea	k6eAd1	Chelsea
se	se	k3xPyFc4	se
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
hraném	hraný	k2eAgNnSc6d1	hrané
na	na	k7c4	na
stadiónu	stadión	k1gInSc2	stadión
Lužniky	Lužnik	k1gInPc4	Lužnik
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
znovu	znovu	k6eAd1	znovu
střetla	střetnout	k5eAaPmAgFnS	střetnout
s	s	k7c7	s
Manchesterem	Manchester	k1gInSc7	Manchester
United	United	k1gMnSc1	United
<g/>
.	.	kIx.	.
</s>
<s>
Stav	stav	k1gInSc1	stav
utkání	utkání	k1gNnSc3	utkání
byl	být	k5eAaImAgInS	být
i	i	k9	i
po	po	k7c6	po
prodloužení	prodloužení	k1gNnSc6	prodloužení
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
přišly	přijít	k5eAaPmAgFnP	přijít
na	na	k7c4	na
řadu	řada	k1gFnSc4	řada
penalty	penalta	k1gFnSc2	penalta
<g/>
.	.	kIx.	.
</s>
<s>
Čech	Čech	k1gMnSc1	Čech
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
pokusů	pokus	k1gInPc2	pokus
Manchesteru	Manchester	k1gInSc2	Manchester
chytil	chytit	k5eAaPmAgMnS	chytit
pouze	pouze	k6eAd1	pouze
střelu	střel	k1gInSc2	střel
Cristiana	Cristian	k1gMnSc2	Cristian
Ronalda	Ronald	k1gMnSc2	Ronald
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
John	John	k1gMnSc1	John
Terry	Terra	k1gFnSc2	Terra
trefil	trefit	k5eAaPmAgMnS	trefit
tyč	tyč	k1gFnSc4	tyč
a	a	k8xC	a
Čechův	Čechův	k2eAgInSc4d1	Čechův
protějšek	protějšek	k1gInSc4	protějšek
Edwin	Edwin	k2eAgInSc1d1	Edwin
van	van	k1gInSc1	van
der	drát	k5eAaImRp2nS	drát
Sar	Sar	k1gFnPc7	Sar
chytil	chytit	k5eAaPmAgMnS	chytit
střelu	střela	k1gFnSc4	střela
Nicolase	Nicolasa	k1gFnSc3	Nicolasa
Anelky	Anelka	k1gMnSc2	Anelka
<g/>
.	.	kIx.	.
</s>
<s>
Výhru	výhra	k1gFnSc4	výhra
v	v	k7c6	v
prestižní	prestižní	k2eAgFnSc6d1	prestižní
soutěži	soutěž	k1gFnSc6	soutěž
tak	tak	k6eAd1	tak
slavil	slavit	k5eAaImAgInS	slavit
Manchester	Manchester	k1gInSc1	Manchester
United	United	k1gInSc1	United
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
v	v	k7c6	v
reprezentaci	reprezentace	k1gFnSc6	reprezentace
si	se	k3xPyFc3	se
Čech	Čech	k1gMnSc1	Čech
chuť	chuť	k1gFnSc4	chuť
nespravil	spravit	k5eNaPmAgMnS	spravit
<g/>
.	.	kIx.	.
</s>
<s>
Reprezentační	reprezentační	k2eAgNnSc1d1	reprezentační
vystoupení	vystoupení	k1gNnSc1	vystoupení
na	na	k7c6	na
mistrovsví	mistrovsví	k1gNnSc6	mistrovsví
Evropy	Evropa	k1gFnSc2	Evropa
2008	[number]	k4	2008
skončilo	skončit	k5eAaPmAgNnS	skončit
po	po	k7c6	po
nevydařeném	vydařený	k2eNgNnSc6d1	nevydařené
utkání	utkání	k1gNnSc6	utkání
základní	základní	k2eAgFnSc2d1	základní
skupiny	skupina	k1gFnSc2	skupina
s	s	k7c7	s
Tureckem	Turecko	k1gNnSc7	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Česko	Česko	k1gNnSc1	Česko
vedlo	vést	k5eAaImAgNnS	vést
ještě	ještě	k9	ještě
20	[number]	k4	20
minut	minuta	k1gFnPc2	minuta
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
však	však	k9	však
prohrálo	prohrát	k5eAaPmAgNnS	prohrát
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
Čech	Čech	k1gMnSc1	Čech
přispěl	přispět	k5eAaPmAgMnS	přispět
velkou	velký	k2eAgFnSc7d1	velká
hrubkou	hrubka	k1gFnSc7	hrubka
vedoucí	vedoucí	k1gFnSc2	vedoucí
k	k	k7c3	k
druhému	druhý	k4xOgInSc3	druhý
gólu	gól	k1gInSc3	gól
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
síti	síť	k1gFnSc6	síť
<g/>
.	.	kIx.	.
</s>
<s>
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
zápasech	zápas	k1gInPc6	zápas
základní	základní	k2eAgFnSc2d1	základní
skupiny	skupina	k1gFnSc2	skupina
se	s	k7c7	s
šampionátem	šampionát	k1gInSc7	šampionát
rozloučila	rozloučit	k5eAaPmAgFnS	rozloučit
<g/>
,	,	kIx,	,
účast	účast	k1gFnSc1	účast
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
zi	zi	k?	zi
zajistilo	zajistit	k5eAaPmAgNnS	zajistit
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
a	a	k8xC	a
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2008	[number]	k4	2008
podepsal	podepsat	k5eAaPmAgMnS	podepsat
Čech	Čech	k1gMnSc1	Čech
v	v	k7c4	v
Chelsea	Chelseum	k1gNnPc4	Chelseum
nový	nový	k2eAgInSc4d1	nový
pětiletý	pětiletý	k2eAgInSc4d1	pětiletý
kontrakt	kontrakt	k1gInSc4	kontrakt
<g/>
,	,	kIx,	,
platný	platný	k2eAgInSc4d1	platný
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2008	[number]	k4	2008
porazila	porazit	k5eAaPmAgFnS	porazit
Chelsea	Chelsea	k1gFnSc1	Chelsea
Sunderland	Sunderland	k1gInSc1	Sunderland
AFC	AFC	kA	AFC
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
Čecha	Čech	k1gMnSc4	Čech
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
jubilejní	jubilejní	k2eAgFnSc1d1	jubilejní
stá	stý	k4xOgFnSc1	stý
nula	nula	k1gFnSc1	nula
v	v	k7c6	v
dresu	dres	k1gInSc6	dres
Chelsea	Chelse	k1gInSc2	Chelse
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2009	[number]	k4	2009
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
proti	proti	k7c3	proti
Juventusu	Juventus	k1gInSc3	Juventus
Turín	Turín	k1gInSc1	Turín
oslavil	oslavit	k5eAaPmAgInS	oslavit
i	i	k9	i
200	[number]	k4	200
<g/>
.	.	kIx.	.
start	start	k1gInSc1	start
za	za	k7c2	za
Chelsea	Chelseus	k1gMnSc2	Chelseus
F.	F.	kA	F.
C.	C.	kA	C.
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Premier	Premier	k1gInSc4	Premier
League	Leagu	k1gFnSc2	Leagu
skončila	skončit	k5eAaPmAgFnS	skončit
Chelsea	Chelsea	k1gFnSc1	Chelsea
tentokrát	tentokrát	k6eAd1	tentokrát
třetí	třetí	k4xOgFnSc7	třetí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
alespoň	alespoň	k9	alespoň
vítězství	vítězství	k1gNnSc4	vítězství
v	v	k7c6	v
Anglickém	anglický	k2eAgInSc6d1	anglický
poháru	pohár	k1gInSc6	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finálovém	finálový	k2eAgNnSc6d1	finálové
utkání	utkání	k1gNnSc6	utkání
proti	proti	k7c3	proti
Evertonu	Everton	k1gInSc3	Everton
Čech	Čechy	k1gFnPc2	Čechy
sice	sice	k8xC	sice
inkasoval	inkasovat	k5eAaBmAgInS	inkasovat
již	již	k6eAd1	již
po	po	k7c6	po
25	[number]	k4	25
sekundách	sekunda	k1gFnPc6	sekunda
hry	hra	k1gFnSc2	hra
(	(	kIx(	(
<g/>
nejrychlejší	rychlý	k2eAgInSc4d3	nejrychlejší
finálový	finálový	k2eAgInSc4d1	finálový
gól	gól	k1gInSc4	gól
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
stoleté	stoletý	k2eAgFnSc6d1	stoletá
historii	historie	k1gFnSc6	historie
této	tento	k3xDgFnSc2	tento
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
zápasu	zápas	k1gInSc2	zápas
již	již	k9	již
gól	gól	k1gInSc4	gól
nedostal	dostat	k5eNaPmAgMnS	dostat
a	a	k8xC	a
Chelsea	Chelsea	k1gMnSc1	Chelsea
po	po	k7c6	po
gólech	gól	k1gInPc6	gól
Didiera	Didier	k1gMnSc2	Didier
Drogby	Drogba	k1gMnSc2	Drogba
a	a	k8xC	a
Franka	Frank	k1gMnSc2	Frank
Lamparda	Lampard	k1gMnSc2	Lampard
nakonec	nakonec	k6eAd1	nakonec
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
2-1	[number]	k4	2-1
.	.	kIx.	.
</s>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
pro	pro	k7c4	pro
Čecha	Čech	k1gMnSc4	Čech
začala	začít	k5eAaPmAgFnS	začít
vítězstvím	vítězství	k1gNnSc7	vítězství
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
Superpoháru	superpohár	k1gInSc6	superpohár
<g/>
.	.	kIx.	.
</s>
<s>
Utkání	utkání	k1gNnSc1	utkání
s	s	k7c7	s
Manchesterem	Manchester	k1gInSc7	Manchester
United	United	k1gInSc1	United
skončilo	skončit	k5eAaPmAgNnS	skončit
nerozhodně	rozhodně	k6eNd1	rozhodně
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
o	o	k7c6	o
vítězi	vítěz	k1gMnSc6	vítěz
tak	tak	k6eAd1	tak
rozhodovaly	rozhodovat	k5eAaImAgInP	rozhodovat
pokutové	pokutový	k2eAgInPc1d1	pokutový
kopy	kop	k1gInPc1	kop
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
hráči	hráč	k1gMnPc1	hráč
Chelsea	Chelse	k1gInSc2	Chelse
proměnili	proměnit	k5eAaPmAgMnP	proměnit
všechny	všechen	k3xTgMnPc4	všechen
čtyři	čtyři	k4xCgMnPc4	čtyři
své	svůj	k3xOyFgInPc4	svůj
pokusy	pokus	k1gInPc4	pokus
(	(	kIx(	(
<g/>
v	v	k7c6	v
brance	branka	k1gFnSc6	branka
Manchesteru	Manchester	k1gInSc2	Manchester
nestál	stát	k5eNaImAgInS	stát
Edwin	Edwin	k2eAgInSc1d1	Edwin
van	van	k1gInSc1	van
der	drát	k5eAaImRp2nS	drát
Sar	Sar	k1gMnSc5	Sar
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
náhradník	náhradník	k1gMnSc1	náhradník
Ben	Ben	k1gInSc4	Ben
Foster	Foster	k1gInSc1	Foster
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c4	za
Manchester	Manchester	k1gInSc4	Manchester
United	United	k1gInSc4	United
nedali	dát	k5eNaPmAgMnP	dát
Ryan	Ryan	k1gInSc4	Ryan
Giggs	Giggsa	k1gFnPc2	Giggsa
a	a	k8xC	a
Patrice	patrice	k1gFnSc2	patrice
Evra	Evr	k1gInSc2	Evr
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2010	[number]	k4	2010
v	v	k7c6	v
utkání	utkání	k1gNnSc6	utkání
proti	proti	k7c3	proti
Boltonu	Bolton	k1gInSc3	Bolton
vychytal	vychytat	k5eAaPmAgMnS	vychytat
svoji	svůj	k3xOyFgFnSc4	svůj
100	[number]	k4	100
<g/>
.	.	kIx.	.
nulu	nula	k1gFnSc4	nula
v	v	k7c6	v
anglické	anglický	k2eAgFnSc6d1	anglická
lize	liga	k1gFnSc6	liga
(	(	kIx(	(
<g/>
Chelsea	Chelsea	k1gFnSc1	Chelsea
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Potřeboval	potřebovat	k5eAaImAgInS	potřebovat
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
180	[number]	k4	180
(	(	kIx(	(
<g/>
ligových	ligový	k2eAgInPc2d1	ligový
<g/>
)	)	kIx)	)
zápasů	zápas	k1gInPc2	zápas
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Premier	Premier	k1gInSc4	Premier
League	Leagu	k1gFnSc2	Leagu
sváděla	svádět	k5eAaImAgFnS	svádět
Chelsea	Chelsea	k1gFnSc1	Chelsea
opět	opět	k6eAd1	opět
velký	velký	k2eAgInSc4d1	velký
boj	boj	k1gInSc4	boj
s	s	k7c7	s
Manchesterem	Manchester	k1gInSc7	Manchester
United	United	k1gMnSc1	United
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2007	[number]	k4	2007
<g/>
/	/	kIx~	/
<g/>
2008	[number]	k4	2008
rozhodovalo	rozhodovat	k5eAaImAgNnS	rozhodovat
o	o	k7c6	o
titulu	titul	k1gInSc6	titul
až	až	k6eAd1	až
poslední	poslední	k2eAgNnSc4d1	poslední
kolo	kolo	k1gNnSc4	kolo
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
radovala	radovat	k5eAaImAgFnS	radovat
Chelsea	Chelsea	k1gFnSc1	Chelsea
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
soutěž	soutěž	k1gFnSc1	soutěž
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
s	s	k7c7	s
jednobodovým	jednobodový	k2eAgInSc7d1	jednobodový
náskokem	náskok	k1gInSc7	náskok
<g/>
.	.	kIx.	.
</s>
<s>
Čech	Čech	k1gMnSc1	Čech
tak	tak	k6eAd1	tak
slavil	slavit	k5eAaImAgMnS	slavit
své	svůj	k3xOyFgNnSc4	svůj
třetí	třetí	k4xOgNnSc4	třetí
vítězství	vítězství	k1gNnSc4	vítězství
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Chelsea	Chelsea	k1gFnSc1	Chelsea
navíc	navíc	k6eAd1	navíc
obhájila	obhájit	k5eAaPmAgFnS	obhájit
i	i	k9	i
vítězství	vítězství	k1gNnSc4	vítězství
v	v	k7c6	v
Anglickém	anglický	k2eAgInSc6d1	anglický
poháru	pohár	k1gInSc6	pohár
a	a	k8xC	a
vybojovala	vybojovat	k5eAaPmAgNnP	vybojovat
tak	tak	k6eAd1	tak
historické	historický	k2eAgNnSc4d1	historické
"	"	kIx"	"
<g/>
double	double	k2eAgNnSc4d1	double
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
Anglického	anglický	k2eAgInSc2d1	anglický
poháru	pohár	k1gInSc2	pohár
porazila	porazit	k5eAaPmAgFnS	porazit
Chelsea	Chelsea	k1gFnSc1	Chelsea
Portsmouth	Portsmouth	k1gInSc1	Portsmouth
FC	FC	kA	FC
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Čech	Čech	k1gMnSc1	Čech
uhájil	uhájit	k5eAaPmAgMnS	uhájit
čisté	čistý	k2eAgNnSc4d1	čisté
konto	konto	k1gNnSc4	konto
<g/>
,	,	kIx,	,
když	když	k8xS	když
chytil	chytit	k5eAaPmAgMnS	chytit
penaltu	penalta	k1gFnSc4	penalta
Kevin-Princi	Kevin-Prinec	k1gMnSc3	Kevin-Prinec
Boatengovi	Boateng	k1gMnSc3	Boateng
.	.	kIx.	.
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
2010	[number]	k4	2010
se	se	k3xPyFc4	se
Čech	Čech	k1gMnSc1	Čech
nezúčastnil	zúčastnit	k5eNaPmAgMnS	zúčastnit
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
si	se	k3xPyFc3	se
nevybojovalo	vybojovat	k5eNaPmAgNnS	vybojovat
postup	postup	k1gInSc4	postup
z	z	k7c2	z
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
skupině	skupina	k1gFnSc6	skupina
skončilo	skončit	k5eAaPmAgNnS	skončit
až	až	k8xS	až
třetí	třetí	k4xOgFnSc4	třetí
za	za	k7c7	za
Slovenskem	Slovensko	k1gNnSc7	Slovensko
a	a	k8xC	a
Slovinskem	Slovinsko	k1gNnSc7	Slovinsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přípravě	příprava	k1gFnSc6	příprava
na	na	k7c4	na
sezónu	sezóna	k1gFnSc4	sezóna
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
Čech	Čech	k1gMnSc1	Čech
zranění	zranění	k1gNnSc2	zranění
lýtka	lýtko	k1gNnSc2	lýtko
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
kterému	který	k3yIgNnSc3	který
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
chyběl	chybět	k5eAaImAgMnS	chybět
v	v	k7c6	v
souboji	souboj	k1gInSc6	souboj
o	o	k7c4	o
anglický	anglický	k2eAgInSc4d1	anglický
Superpohár	superpohár	k1gInSc4	superpohár
(	(	kIx(	(
<g/>
Chelsea	Chelsea	k1gFnSc1	Chelsea
prohrála	prohrát	k5eAaPmAgFnS	prohrát
s	s	k7c7	s
Manchesterem	Manchester	k1gInSc7	Manchester
United	United	k1gInSc4	United
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2011	[number]	k4	2011
si	se	k3xPyFc3	se
Čech	Čech	k1gMnSc1	Čech
v	v	k7c6	v
utkání	utkání	k1gNnSc6	utkání
s	s	k7c7	s
Blackpoolem	Blackpool	k1gInSc7	Blackpool
připsal	připsat	k5eAaPmAgMnS	připsat
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
konto	konto	k1gNnSc4	konto
jubilejní	jubilejní	k2eAgNnSc4d1	jubilejní
300	[number]	k4	300
<g/>
.	.	kIx.	.
start	start	k1gInSc4	start
v	v	k7c6	v
dresu	dres	k1gInSc6	dres
Chelsea	Chelse	k1gInSc2	Chelse
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
titul	titul	k1gInSc4	titul
v	v	k7c4	v
Premier	Premier	k1gInSc4	Premier
League	Leagu	k1gFnSc2	Leagu
si	se	k3xPyFc3	se
tentokrát	tentokrát	k6eAd1	tentokrát
suverénně	suverénně	k6eAd1	suverénně
došel	dojít	k5eAaPmAgInS	dojít
Manchester	Manchester	k1gInSc1	Manchester
United	United	k1gInSc1	United
<g/>
,	,	kIx,	,
Chelsea	Chelsea	k1gFnSc1	Chelsea
skončila	skončit	k5eAaPmAgFnS	skončit
s	s	k7c7	s
devítibodovou	devítibodový	k2eAgFnSc7d1	devítibodová
ztrátou	ztráta	k1gFnSc7	ztráta
druhá	druhý	k4xOgNnPc4	druhý
<g/>
,	,	kIx,	,
jen	jen	k9	jen
o	o	k7c6	o
skóre	skóre	k1gNnSc6	skóre
před	před	k7c7	před
Manchesterem	Manchester	k1gInSc7	Manchester
City	city	k1gNnSc1	city
<g/>
.	.	kIx.	.
</s>
<s>
Chelsea	Chelsea	k1gFnSc1	Chelsea
tedy	tedy	k9	tedy
skončila	skončit	k5eAaPmAgFnS	skončit
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
sezóně	sezóna	k1gFnSc6	sezóna
bez	bez	k7c2	bez
trofeje	trofej	k1gFnSc2	trofej
<g/>
,	,	kIx,	,
Čech	Čech	k1gMnSc1	Čech
byl	být	k5eAaImAgMnS	být
ovšem	ovšem	k9	ovšem
fanoušky	fanoušek	k1gMnPc4	fanoušek
poprvé	poprvé	k6eAd1	poprvé
zvolen	zvolit	k5eAaPmNgMnS	zvolit
Hráčem	hráč	k1gMnSc7	hráč
roku	rok	k1gInSc2	rok
Chelsea	Chelse	k1gInSc2	Chelse
F.	F.	kA	F.
<g/>
C.	C.	kA	C.
(	(	kIx(	(
<g/>
získal	získat	k5eAaPmAgMnS	získat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
28	[number]	k4	28
000	[number]	k4	000
hlasů	hlas	k1gInPc2	hlas
<g/>
)	)	kIx)	)
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úvodu	úvod	k1gInSc6	úvod
sezóny	sezóna	k1gFnSc2	sezóna
vyřadilo	vyřadit	k5eAaPmAgNnS	vyřadit
Čecha	Čech	k1gMnSc4	Čech
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
týdny	týden	k1gInPc4	týden
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
zranění	zranění	k1gNnSc2	zranění
kolene	kolen	k1gInSc5	kolen
z	z	k7c2	z
tréninku	trénink	k1gInSc2	trénink
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2012	[number]	k4	2012
v	v	k7c6	v
utkání	utkání	k1gNnSc6	utkání
s	s	k7c7	s
Tottenhamem	Tottenham	k1gInSc7	Tottenham
Hotspur	Hotspur	k1gMnSc1	Hotspur
oslavil	oslavit	k5eAaPmAgMnS	oslavit
svůj	svůj	k3xOyFgMnSc1	svůj
250	[number]	k4	250
<g/>
.	.	kIx.	.
ligový	ligový	k2eAgInSc4d1	ligový
start	start	k1gInSc4	start
v	v	k7c6	v
dresu	dres	k1gInSc6	dres
Chelsea	Chelse	k1gInSc2	Chelse
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lize	liga	k1gFnSc6	liga
Chelsea	Chelse	k1gInSc2	Chelse
tuto	tento	k3xDgFnSc4	tento
sezónu	sezóna	k1gFnSc4	sezóna
zůstala	zůstat	k5eAaPmAgFnS	zůstat
výrazně	výrazně	k6eAd1	výrazně
za	za	k7c7	za
očekáváním	očekávání	k1gNnSc7	očekávání
<g/>
,	,	kIx,	,
musela	muset	k5eAaImAgFnS	muset
se	se	k3xPyFc4	se
spokojit	spokojit	k5eAaPmF	spokojit
s	s	k7c7	s
konečným	konečný	k2eAgMnSc7d1	konečný
6	[number]	k4	6
<g/>
.	.	kIx.	.
místem	místo	k1gNnSc7	místo
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
si	se	k3xPyFc3	se
však	však	k9	však
hráči	hráč	k1gMnPc1	hráč
Chelsea	Chelse	k1gInSc2	Chelse
plně	plně	k6eAd1	plně
vynahradili	vynahradit	k5eAaPmAgMnP	vynahradit
v	v	k7c6	v
pohárech	pohár	k1gInPc6	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
Anglického	anglický	k2eAgInSc2d1	anglický
poháru	pohár	k1gInSc2	pohár
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
porazili	porazit	k5eAaPmAgMnP	porazit
Liverpool	Liverpool	k1gInSc4	Liverpool
FC	FC	kA	FC
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Čech	Čech	k1gMnSc1	Čech
se	se	k3xPyFc4	se
vyznamenal	vyznamenat	k5eAaPmAgMnS	vyznamenat
především	především	k6eAd1	především
při	při	k7c6	při
střele	střela	k1gFnSc6	střela
Andyho	Andy	k1gMnSc2	Andy
Carrolla	Carroll	k1gMnSc2	Carroll
z	z	k7c2	z
83	[number]	k4	83
<g/>
.	.	kIx.	.
minuty	minuta	k1gFnSc2	minuta
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
na	na	k7c4	na
břevno	břevno	k1gNnSc4	břevno
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
této	tento	k3xDgFnSc6	tento
situaci	situace	k1gFnSc6	situace
se	se	k3xPyFc4	se
debatovalo	debatovat	k5eAaImAgNnS	debatovat
i	i	k9	i
dlouho	dlouho	k6eAd1	dlouho
po	po	k7c6	po
zápase	zápas	k1gInSc6	zápas
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
nebylo	být	k5eNaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
míč	míč	k1gInSc1	míč
nepřešel	přejít	k5eNaPmAgInS	přejít
celým	celý	k2eAgInSc7d1	celý
objemem	objem	k1gInSc7	objem
brankovou	brankový	k2eAgFnSc4d1	branková
čáru	čára	k1gFnSc4	čára
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
situaci	situace	k1gFnSc4	situace
jako	jako	k9	jako
gól	gól	k1gInSc4	gól
neposoudili	posoudit	k5eNaPmAgMnP	posoudit
a	a	k8xC	a
Chelsea	Chelsea	k1gFnSc1	Chelsea
tak	tak	k6eAd1	tak
slavila	slavit	k5eAaImAgFnS	slavit
svůj	svůj	k3xOyFgInSc4	svůj
sedmý	sedmý	k4xOgInSc4	sedmý
triumf	triumf	k1gInSc4	triumf
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
soutěži	soutěž	k1gFnSc6	soutěž
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
Čecha	Čech	k1gMnSc4	Čech
čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
úspěch	úspěch	k1gInSc1	úspěch
měl	mít	k5eAaImAgInS	mít
však	však	k9	však
teprve	teprve	k6eAd1	teprve
přijít	přijít	k5eAaPmF	přijít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmifinále	osmifinále	k1gNnSc6	osmifinále
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
stála	stát	k5eAaImAgFnS	stát
Chelsea	Chelsea	k1gFnSc1	Chelsea
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
vyřazení	vyřazení	k1gNnSc2	vyřazení
<g/>
,	,	kIx,	,
když	když	k8xS	když
prohrála	prohrát	k5eAaPmAgFnS	prohrát
úvodní	úvodní	k2eAgInSc4d1	úvodní
zápas	zápas	k1gInSc4	zápas
v	v	k7c6	v
Neapoli	Neapol	k1gFnSc6	Neapol
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
hráči	hráč	k1gMnPc1	hráč
Chelsea	Chelse	k1gInSc2	Chelse
dokázali	dokázat	k5eAaPmAgMnP	dokázat
zmobilizovat	zmobilizovat	k5eAaPmF	zmobilizovat
síly	síla	k1gFnPc4	síla
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
slavili	slavit	k5eAaImAgMnP	slavit
postup	postup	k1gInSc4	postup
po	po	k7c6	po
domácí	domácí	k2eAgFnSc6d1	domácí
výhře	výhra	k1gFnSc6	výhra
4-1	[number]	k4	4-1
v	v	k7c6	v
prodloužení	prodloužení	k1gNnSc6	prodloužení
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
si	se	k3xPyFc3	se
Chelsea	Chelsea	k1gFnSc1	Chelsea
poradila	poradit	k5eAaPmAgFnS	poradit
s	s	k7c7	s
Benficou	Benfica	k1gFnSc7	Benfica
Lisabon	Lisabon	k1gInSc1	Lisabon
po	po	k7c6	po
výsledcích	výsledek	k1gInPc6	výsledek
1-0	[number]	k4	1-0
(	(	kIx(	(
<g/>
venku	venku	k6eAd1	venku
<g/>
)	)	kIx)	)
a	a	k8xC	a
2-1	[number]	k4	2-1
(	(	kIx(	(
<g/>
doma	doma	k6eAd1	doma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
semifinále	semifinále	k1gNnSc4	semifinále
proti	proti	k7c3	proti
obhájcům	obhájce	k1gMnPc3	obhájce
trofeje	trofej	k1gFnSc2	trofej
z	z	k7c2	z
FC	FC	kA	FC
Barcelona	Barcelona	k1gFnSc1	Barcelona
byla	být	k5eAaImAgFnS	být
Chelsea	Chelse	k2eAgFnSc1d1	Chelsea
i	i	k9	i
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
ligovým	ligový	k2eAgInPc3d1	ligový
výsledkům	výsledek	k1gInPc3	výsledek
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c2	za
jasného	jasný	k2eAgMnSc2d1	jasný
outsidera	outsider	k1gMnSc2	outsider
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
doma	doma	k6eAd1	doma
dokázala	dokázat	k5eAaPmAgFnS	dokázat
Chelsea	Chelsea	k1gFnSc1	Chelsea
zvítězit	zvítězit	k5eAaPmF	zvítězit
1-0	[number]	k4	1-0
gólem	gól	k1gInSc7	gól
Drogby	Drogba	k1gFnSc2	Drogba
a	a	k8xC	a
na	na	k7c4	na
horkou	horký	k2eAgFnSc4d1	horká
španělskou	španělský	k2eAgFnSc4d1	španělská
půdu	půda	k1gFnSc4	půda
tak	tak	k9	tak
rozhodně	rozhodně	k6eAd1	rozhodně
neodjížděla	odjíždět	k5eNaImAgFnS	odjíždět
bez	bez	k7c2	bez
šancí	šance	k1gFnPc2	šance
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
odvetného	odvetný	k2eAgInSc2d1	odvetný
zápasu	zápas	k1gInSc2	zápas
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
Čech	Čech	k1gMnSc1	Čech
skvělý	skvělý	k2eAgInSc4d1	skvělý
zákrok	zákrok	k1gInSc4	zákrok
proti	proti	k7c3	proti
fenomenálnímu	fenomenální	k2eAgInSc3d1	fenomenální
Lionelu	Lionel	k1gInSc3	Lionel
Messimu	Messim	k1gInSc2	Messim
<g/>
.	.	kIx.	.
</s>
<s>
FC	FC	kA	FC
Barcelona	Barcelona	k1gFnSc1	Barcelona
se	se	k3xPyFc4	se
ujala	ujmout	k5eAaPmAgFnS	ujmout
vedení	vedení	k1gNnPc4	vedení
v	v	k7c6	v
35	[number]	k4	35
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
gólem	gól	k1gInSc7	gól
Busquetse	Busquetse	k1gFnSc2	Busquetse
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
37	[number]	k4	37
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
uviděl	uvidět	k5eAaPmAgMnS	uvidět
červenou	červený	k2eAgFnSc4d1	červená
kartu	karta	k1gFnSc4	karta
kapitán	kapitán	k1gMnSc1	kapitán
Chelsea	Chelse	k1gInSc2	Chelse
John	John	k1gMnSc1	John
Terry	Terra	k1gMnSc2	Terra
za	za	k7c4	za
nesmyslný	smyslný	k2eNgInSc4d1	nesmyslný
zákrok	zákrok	k1gInSc4	zákrok
zezadu	zezadu	k6eAd1	zezadu
na	na	k7c4	na
Sáncheze	Sáncheze	k1gFnPc4	Sáncheze
<g/>
.	.	kIx.	.
</s>
<s>
FC	FC	kA	FC
Barcelona	Barcelona	k1gFnSc1	Barcelona
využila	využít	k5eAaPmAgFnS	využít
převahu	převaha	k1gFnSc4	převaha
jednoho	jeden	k4xCgMnSc2	jeden
hráče	hráč	k1gMnSc2	hráč
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
již	již	k6eAd1	již
po	po	k7c6	po
šesti	šest	k4xCc6	šest
minutách	minuta	k1gFnPc6	minuta
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
trefil	trefit	k5eAaPmAgMnS	trefit
Iniesta	Iniesta	k1gMnSc1	Iniesta
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
Chelsea	Chelsea	k1gFnSc1	Chelsea
vypadala	vypadat	k5eAaPmAgFnS	vypadat
beznadějně	beznadějně	k6eAd1	beznadějně
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
nastaveném	nastavený	k2eAgInSc6d1	nastavený
čase	čas	k1gInSc6	čas
prvního	první	k4xOgInSc2	první
poločasu	poločas	k1gInSc2	poločas
snížil	snížit	k5eAaPmAgInS	snížit
Ramires	Ramires	k1gInSc1	Ramires
na	na	k7c4	na
1-2	[number]	k4	1-2
a	a	k8xC	a
postavil	postavit	k5eAaPmAgMnS	postavit
tak	tak	k6eAd1	tak
Barcelonu	Barcelona	k1gFnSc4	Barcelona
před	před	k7c4	před
nutnost	nutnost	k1gFnSc4	nutnost
vstřelit	vstřelit	k5eAaPmF	vstřelit
další	další	k2eAgInSc4d1	další
gól	gól	k1gInSc4	gól
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
49	[number]	k4	49
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
přišla	přijít	k5eAaPmAgFnS	přijít
další	další	k2eAgFnSc1d1	další
velká	velký	k2eAgFnSc1d1	velká
chvíle	chvíle	k1gFnSc1	chvíle
Petra	Petr	k1gMnSc2	Petr
Čecha	Čech	k1gMnSc2	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
faulu	faul	k1gInSc6	faul
Drogby	Drogba	k1gFnSc2	Drogba
byla	být	k5eAaImAgFnS	být
nařízena	nařízen	k2eAgFnSc1d1	nařízena
penalta	penalta	k1gFnSc1	penalta
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yQgFnSc3	který
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgInS	postavit
Lionel	Lionel	k1gInSc1	Lionel
Messi	Messe	k1gFnSc4	Messe
<g/>
.	.	kIx.	.
</s>
<s>
Nastřelil	nastřelit	k5eAaPmAgMnS	nastřelit
však	však	k9	však
jen	jen	k9	jen
břevno	břevno	k1gNnSc4	břevno
<g/>
!	!	kIx.	!
</s>
<s>
Zbytek	zbytek	k1gInSc4	zbytek
zápasu	zápas	k1gInSc2	zápas
Barcelona	Barcelona	k1gFnSc1	Barcelona
útočila	útočit	k5eAaImAgFnS	útočit
<g/>
,	,	kIx,	,
Chelsea	Chelsea	k1gFnSc1	Chelsea
úporně	úporně	k6eAd1	úporně
bránila	bránit	k5eAaImAgFnS	bránit
<g/>
,	,	kIx,	,
Čech	Čech	k1gMnSc1	Čech
však	však	k9	však
odolal	odolat	k5eAaPmAgMnS	odolat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nastavení	nastavení	k1gNnSc6	nastavení
nakonec	nakonec	k6eAd1	nakonec
obraně	obraně	k6eAd1	obraně
Barcelony	Barcelona	k1gFnSc2	Barcelona
unikl	uniknout	k5eAaPmAgMnS	uniknout
Torres	Torres	k1gMnSc1	Torres
a	a	k8xC	a
srovnal	srovnat	k5eAaPmAgMnS	srovnat
na	na	k7c4	na
konečných	konečný	k2eAgInPc2d1	konečný
2-2	[number]	k4	2-2
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
proti	proti	k7c3	proti
Bayernu	Bayern	k1gInSc3	Bayern
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
výhodu	výhoda	k1gFnSc4	výhoda
domácího	domácí	k2eAgNnSc2d1	domácí
hřiště	hřiště	k1gNnSc2	hřiště
(	(	kIx(	(
<g/>
finále	finále	k1gNnSc1	finále
se	se	k3xPyFc4	se
hrálo	hrát	k5eAaImAgNnS	hrát
v	v	k7c4	v
mnichovské	mnichovský	k2eAgInPc4d1	mnichovský
Allianz	Allianz	k1gInSc4	Allianz
Areně	Areeň	k1gFnSc2	Areeň
<g/>
)	)	kIx)	)
nebyla	být	k5eNaImAgFnS	být
Chelsea	Chelsea	k1gFnSc1	Chelsea
favoritem	favorit	k1gInSc7	favorit
<g/>
.	.	kIx.	.
</s>
<s>
Scházeli	scházet	k5eAaImAgMnP	scházet
jí	on	k3xPp3gFnSc3	on
totiž	totiž	k9	totiž
čtyři	čtyři	k4xCgMnPc1	čtyři
hráči	hráč	k1gMnPc1	hráč
základní	základní	k2eAgFnSc2d1	základní
sestavy	sestava	k1gFnSc2	sestava
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
vykartovali	vykartovat	k5eAaPmAgMnP	vykartovat
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
hraném	hraný	k2eAgNnSc6d1	hrané
19	[number]	k4	19
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
byli	být	k5eAaImAgMnP	být
Petr	Petr	k1gMnSc1	Petr
Čech	Čech	k1gMnSc1	Čech
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Didierem	Didier	k1gMnSc7	Didier
Drogbou	Drogba	k1gMnSc7	Drogba
hlavními	hlavní	k2eAgFnPc7d1	hlavní
hvězdami	hvězda	k1gFnPc7	hvězda
zápasu	zápas	k1gInSc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Bayern	Bayern	k1gInSc1	Bayern
byl	být	k5eAaImAgInS	být
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
zápasu	zápas	k1gInSc2	zápas
aktivnější	aktivní	k2eAgFnSc1d2	aktivnější
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
gólu	gól	k1gInSc2	gól
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgMnS	dočkat
až	až	k9	až
v	v	k7c6	v
83	[number]	k4	83
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
trefil	trefit	k5eAaPmAgMnS	trefit
Thomas	Thomas	k1gMnSc1	Thomas
Müller	Müller	k1gMnSc1	Müller
<g/>
.	.	kIx.	.
</s>
<s>
Domácí	domácí	k2eAgMnPc1d1	domácí
fanoušci	fanoušek	k1gMnPc1	fanoušek
už	už	k6eAd1	už
chystali	chystat	k5eAaImAgMnP	chystat
oslavy	oslava	k1gFnPc4	oslava
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
88	[number]	k4	88
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
<g/>
,	,	kIx,	,
po	po	k7c6	po
vůbec	vůbec	k9	vůbec
prvním	první	k4xOgInSc6	první
rohovém	rohový	k2eAgInSc6d1	rohový
kopu	kop	k1gInSc6	kop
pro	pro	k7c4	pro
Chelsea	Chelseus	k1gMnSc4	Chelseus
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
přesně	přesně	k6eAd1	přesně
hlavičkoval	hlavičkovat	k5eAaImAgMnS	hlavičkovat
Didier	Didier	k1gMnSc1	Didier
Drogba	Drogba	k1gMnSc1	Drogba
<g/>
,	,	kIx,	,
srovnal	srovnat	k5eAaPmAgMnS	srovnat
tak	tak	k9	tak
stav	stav	k1gInSc4	stav
zápasu	zápas	k1gInSc2	zápas
na	na	k7c4	na
1-1	[number]	k4	1-1
a	a	k8xC	a
muselo	muset	k5eAaImAgNnS	muset
se	se	k3xPyFc4	se
prodlužovat	prodlužovat	k5eAaImF	prodlužovat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
minutě	minuta	k1gFnSc6	minuta
nastavení	nastavení	k1gNnSc2	nastavení
byla	být	k5eAaImAgFnS	být
nařízena	nařídit	k5eAaPmNgFnS	nařídit
proti	proti	k7c3	proti
Chelsea	Chelseus	k1gMnSc2	Chelseus
penalta	penalta	k1gFnSc1	penalta
po	po	k7c6	po
faulu	faul	k1gInSc6	faul
Didiera	Didiero	k1gNnSc2	Didiero
Drogby	Drogba	k1gFnSc2	Drogba
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgInS	postavit
Arjen	Arjen	k1gInSc1	Arjen
Robben	Robben	k2eAgInSc1d1	Robben
<g/>
.	.	kIx.	.
</s>
<s>
Čech	Čech	k1gMnSc1	Čech
ovšem	ovšem	k9	ovšem
jeho	jeho	k3xOp3gFnSc4	jeho
přízemní	přízemní	k2eAgFnSc4d1	přízemní
střelu	střela	k1gFnSc4	střela
chytil	chytit	k5eAaPmAgMnS	chytit
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
ve	v	k7c6	v
zbytku	zbytek	k1gInSc6	zbytek
prodloužení	prodloužení	k1gNnSc2	prodloužení
gól	gól	k1gInSc4	gól
nepadl	padnout	k5eNaPmAgInS	padnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
rozhodovaly	rozhodovat	k5eAaImAgFnP	rozhodovat
penalty	penalta	k1gFnPc1	penalta
<g/>
.	.	kIx.	.
</s>
<s>
Brankář	brankář	k1gMnSc1	brankář
Bayernu	Bayern	k1gInSc2	Bayern
Manuel	Manuel	k1gMnSc1	Manuel
Neuer	Neuer	k1gMnSc1	Neuer
hned	hned	k6eAd1	hned
v	v	k7c6	v
první	první	k4xOgFnSc6	první
sérii	série	k1gFnSc6	série
chytil	chytit	k5eAaPmAgMnS	chytit
střelu	střel	k1gInSc2	střel
Maty	mat	k1gInPc4	mat
<g/>
,	,	kIx,	,
další	další	k2eAgMnPc1d1	další
střelci	střelec	k1gMnPc1	střelec
Chelsea	Chelse	k1gInSc2	Chelse
však	však	k9	však
byli	být	k5eAaImAgMnP	být
neomylní	omylný	k2eNgMnPc1d1	neomylný
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
Čech	Čech	k1gMnSc1	Čech
kapituloval	kapitulovat	k5eAaBmAgMnS	kapitulovat
v	v	k7c6	v
prvních	první	k4xOgFnPc6	první
třech	tři	k4xCgFnPc6	tři
sériích	série	k1gFnPc6	série
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
však	však	k9	však
přišly	přijít	k5eAaPmAgFnP	přijít
jeho	jeho	k3xOp3gFnPc1	jeho
velké	velký	k2eAgFnPc1d1	velká
chvíle	chvíle	k1gFnPc1	chvíle
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
sérii	série	k1gFnSc6	série
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
střelu	střel	k1gInSc2	střel
Ivicy	Ivica	k1gMnSc2	Ivica
Oliće	Olić	k1gMnSc2	Olić
a	a	k8xC	a
v	v	k7c6	v
páté	pátý	k4xOgFnSc6	pátý
sérii	série	k1gFnSc6	série
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
ránu	rána	k1gFnSc4	rána
Bastiana	Bastian	k1gMnSc2	Bastian
Schweinsteigera	Schweinsteiger	k1gMnSc2	Schweinsteiger
na	na	k7c4	na
tyč	tyč	k1gFnSc4	tyč
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
zpečetil	zpečetit	k5eAaPmAgMnS	zpečetit
historický	historický	k2eAgInSc4d1	historický
úspěch	úspěch	k1gInSc4	úspěch
Chelsea	Chelsea	k1gMnSc1	Chelsea
Didier	Didier	k1gMnSc1	Didier
Drogba	Drogba	k1gMnSc1	Drogba
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Petra	Petr	k1gMnSc4	Petr
Čecha	Čech	k1gMnSc2	Čech
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
nesporně	sporně	k6eNd1	sporně
největší	veliký	k2eAgInSc1d3	veliký
úspěch	úspěch	k1gInSc1	úspěch
jeho	jeho	k3xOp3gFnSc2	jeho
klubové	klubový	k2eAgFnSc2d1	klubová
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
28	[number]	k4	28
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
oznámil	oznámit	k5eAaPmAgInS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ještě	ještě	k9	ještě
před	před	k7c7	před
odjezdem	odjezd	k1gInSc7	odjezd
české	český	k2eAgFnSc2d1	Česká
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
reprezentace	reprezentace	k1gFnSc2	reprezentace
na	na	k7c4	na
soustředění	soustředění	k1gNnSc4	soustředění
do	do	k7c2	do
Rakouska	Rakousko	k1gNnSc2	Rakousko
podepsal	podepsat	k5eAaPmAgMnS	podepsat
s	s	k7c7	s
Chelsea	Chelseum	k1gNnPc1	Chelseum
FC	FC	kA	FC
novou	nový	k2eAgFnSc4d1	nová
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
platnost	platnost	k1gFnSc4	platnost
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2016	[number]	k4	2016
.	.	kIx.	.
</s>
<s>
Zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
Mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
2012	[number]	k4	2012
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
zápase	zápas	k1gInSc6	zápas
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
nezabránil	zabránit	k5eNaPmAgMnS	zabránit
vysoké	vysoký	k2eAgFnSc3d1	vysoká
prohře	prohra	k1gFnSc3	prohra
českého	český	k2eAgNnSc2d1	české
mužstva	mužstvo	k1gNnSc2	mužstvo
1-4	[number]	k4	1-4
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
zápase	zápas	k1gInSc6	zápas
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
proti	proti	k7c3	proti
Řecku	Řecko	k1gNnSc3	Řecko
vyrobil	vyrobit	k5eAaPmAgInS	vyrobit
v	v	k7c6	v
úvodu	úvod	k1gInSc6	úvod
druhého	druhý	k4xOgInSc2	druhý
poločasu	poločas	k1gInSc2	poločas
za	za	k7c2	za
stavu	stav	k1gInSc2	stav
2-0	[number]	k4	2-0
pro	pro	k7c4	pro
ČR	ČR	kA	ČR
fatální	fatální	k2eAgFnSc4d1	fatální
hrubku	hrubka	k1gFnSc4	hrubka
<g/>
,	,	kIx,	,
následkem	následkem	k7c2	následkem
čehož	což	k3yRnSc2	což
řecký	řecký	k2eAgMnSc1d1	řecký
hráč	hráč	k1gMnSc1	hráč
Theofanis	Theofanis	k1gFnPc2	Theofanis
Gekas	Gekas	k1gMnSc1	Gekas
snížil	snížit	k5eAaPmAgMnS	snížit
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc4d1	české
mužstvo	mužstvo	k1gNnSc4	mužstvo
však	však	k9	však
výsledek	výsledek	k1gInSc4	výsledek
2-1	[number]	k4	2-1
udrželo	udržet	k5eAaPmAgNnS	udržet
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
utkání	utkání	k1gNnSc2	utkání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěrečném	závěrečný	k2eAgInSc6d1	závěrečný
zápase	zápas	k1gInSc6	zápas
základní	základní	k2eAgFnSc2d1	základní
skupiny	skupina	k1gFnSc2	skupina
A	a	k9	a
udržel	udržet	k5eAaPmAgMnS	udržet
Petr	Petr	k1gMnSc1	Petr
Čech	Čech	k1gMnSc1	Čech
čisté	čistý	k2eAgNnSc4d1	čisté
konto	konto	k1gNnSc4	konto
proti	proti	k7c3	proti
Polsku	Polsko	k1gNnSc3	Polsko
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Jiráček	Jiráček	k1gMnSc1	Jiráček
gólem	gól	k1gInSc7	gól
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
o	o	k7c4	o
vítězství	vítězství	k1gNnSc4	vítězství
českého	český	k2eAgInSc2d1	český
týmu	tým	k1gInSc2	tým
1-0	[number]	k4	1-0
a	a	k8xC	a
postupu	postup	k1gInSc2	postup
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
místa	místo	k1gNnSc2	místo
skupiny	skupina	k1gFnPc1	skupina
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtfinálový	čtvrtfinálový	k2eAgInSc1d1	čtvrtfinálový
zápas	zápas	k1gInSc1	zápas
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
proti	proti	k7c3	proti
Portugalsku	Portugalsko	k1gNnSc3	Portugalsko
český	český	k2eAgInSc4d1	český
tým	tým	k1gInSc4	tým
prohrál	prohrát	k5eAaPmAgMnS	prohrát
0-1	[number]	k4	0-1
a	a	k8xC	a
s	s	k7c7	s
turnajem	turnaj	k1gInSc7	turnaj
se	se	k3xPyFc4	se
rozloučil	rozloučit	k5eAaPmAgInS	rozloučit
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
Čech	Čech	k1gMnSc1	Čech
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
ke	k	k7c3	k
všem	všecek	k3xTgInPc3	všecek
čtyřem	čtyři	k4xCgInPc3	čtyři
zápasům	zápas	k1gInPc3	zápas
české	český	k2eAgFnSc2d1	Česká
reprezentace	reprezentace	k1gFnSc2	reprezentace
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2012	[number]	k4	2012
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
Monaku	Monako	k1gNnSc6	Monako
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
sestavě	sestava	k1gFnSc6	sestava
Chelsea	Chelse	k1gInSc2	Chelse
k	k	k7c3	k
utkání	utkání	k1gNnSc3	utkání
o	o	k7c4	o
evropský	evropský	k2eAgInSc4d1	evropský
Superpohár	superpohár	k1gInSc4	superpohár
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tuto	tento	k3xDgFnSc4	tento
trofej	trofej	k1gFnSc4	trofej
se	se	k3xPyFc4	se
střetávají	střetávat	k5eAaImIp3nP	střetávat
vítěz	vítěz	k1gMnSc1	vítěz
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
Chelsea	Chelsea	k1gMnSc1	Chelsea
FC	FC	kA	FC
<g/>
)	)	kIx)	)
a	a	k8xC	a
Evropské	evropský	k2eAgFnSc2d1	Evropská
ligy	liga	k1gFnSc2	liga
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
Atlético	Atlético	k1gNnSc1	Atlético
Madrid	Madrid	k1gInSc1	Madrid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čech	Čech	k1gMnSc1	Čech
odchytal	odchytat	k5eAaPmAgMnS	odchytat
celý	celý	k2eAgInSc4d1	celý
zápas	zápas	k1gInSc4	zápas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
porážce	porážka	k1gFnSc6	porážka
1-4	[number]	k4	1-4
společně	společně	k6eAd1	společně
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
spoluhráči	spoluhráč	k1gMnPc7	spoluhráč
zabránit	zabránit	k5eAaPmF	zabránit
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
inkasoval	inkasovat	k5eAaBmAgMnS	inkasovat
tři	tři	k4xCgFnPc4	tři
branky	branka	k1gFnPc4	branka
v	v	k7c6	v
domácím	domácí	k2eAgNnSc6d1	domácí
ligovém	ligový	k2eAgNnSc6d1	ligové
utkání	utkání	k1gNnSc6	utkání
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
<g/>
)	)	kIx)	)
proti	proti	k7c3	proti
Manchesteru	Manchester	k1gInSc3	Manchester
United	United	k1gInSc1	United
(	(	kIx(	(
<g/>
prohra	prohra	k1gFnSc1	prohra
Chelsea	Chelsea	k1gFnSc1	Chelsea
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
když	když	k8xS	když
Robin	robin	k2eAgInSc1d1	robin
van	van	k1gInSc1	van
Persie	Persie	k1gFnSc2	Persie
orazítkoval	orazítkovat	k5eAaPmAgInS	orazítkovat
tvrdou	tvrdý	k2eAgFnSc7d1	tvrdá
střelou	střela	k1gFnSc7	střela
tyč	tyč	k1gFnSc1	tyč
a	a	k8xC	a
míč	míč	k1gInSc1	míč
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
odrazil	odrazit	k5eAaPmAgInS	odrazit
od	od	k7c2	od
zad	záda	k1gNnPc2	záda
Davida	David	k1gMnSc2	David
Luize	Luize	k1gFnSc2	Luize
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
odkryté	odkrytý	k2eAgFnSc2d1	odkrytá
brány	brána	k1gFnSc2	brána
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
přísně	přísně	k6eAd1	přísně
udělili	udělit	k5eAaPmAgMnP	udělit
dvě	dva	k4xCgFnPc4	dva
červené	červený	k2eAgFnPc4d1	červená
karty	karta	k1gFnPc4	karta
<g/>
,	,	kIx,	,
obránci	obránce	k1gMnSc3	obránce
Branislavu	Branislav	k1gMnSc3	Branislav
Ivanovićovi	Ivanovića	k1gMnSc3	Ivanovića
a	a	k8xC	a
útočníkovi	útočník	k1gMnSc3	útočník
Fernandu	Fernand	k1gInSc2	Fernand
Torresovi	Torres	k1gMnSc3	Torres
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
uznali	uznat	k5eAaPmAgMnP	uznat
rozdílový	rozdílový	k2eAgInSc4d1	rozdílový
gól	gól	k1gInSc4	gól
z	z	k7c2	z
ofsajdu	ofsajd	k1gInSc2	ofsajd
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
Javier	Javier	k1gMnSc1	Javier
Hernández	Hernández	k1gMnSc1	Hernández
Balcázar	Balcázar	k1gMnSc1	Balcázar
<g/>
.	.	kIx.	.
</s>
<s>
Chelsea	Chelsea	k6eAd1	Chelsea
tak	tak	k9	tak
poprvé	poprvé	k6eAd1	poprvé
prohrála	prohrát	k5eAaPmAgFnS	prohrát
v	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
v	v	k7c6	v
lize	liga	k1gFnSc6	liga
<g/>
.	.	kIx.	.
</s>
<s>
Duel	duel	k1gInSc1	duel
Chelsea-Manchester	Chelsea-Manchester	k1gMnSc1	Chelsea-Manchester
United	United	k1gMnSc1	United
se	se	k3xPyFc4	se
vzápětí	vzápětí	k6eAd1	vzápětí
opakoval	opakovat	k5eAaImAgInS	opakovat
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
tyto	tento	k3xDgInPc1	tento
týmy	tým	k1gInPc1	tým
se	se	k3xPyFc4	se
potkaly	potkat	k5eAaPmAgInP	potkat
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
anglického	anglický	k2eAgInSc2d1	anglický
ligového	ligový	k2eAgInSc2d1	ligový
poháru	pohár	k1gInSc2	pohár
(	(	kIx(	(
<g/>
Capital	Capital	k1gMnSc1	Capital
One	One	k1gFnSc2	One
Cup	cup	k1gInSc1	cup
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Chelsea	Chelsea	k1gMnSc1	Chelsea
v	v	k7c6	v
atraktivním	atraktivní	k2eAgNnSc6d1	atraktivní
utkání	utkání	k1gNnSc6	utkání
Manchesteru	Manchester	k1gInSc2	Manchester
ligovou	ligový	k2eAgFnSc4d1	ligová
prohru	prohra	k1gFnSc4	prohra
oplatila	oplatit	k5eAaPmAgFnS	oplatit
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
pouze	pouze	k6eAd1	pouze
dotahovala	dotahovat	k5eAaImAgFnS	dotahovat
soupeře	soupeř	k1gMnSc4	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
chybě	chyba	k1gFnSc3	chyba
Petra	Petr	k1gMnSc2	Petr
Čecha	Čech	k1gMnSc2	Čech
se	se	k3xPyFc4	se
před	před	k7c7	před
bránou	brána	k1gFnSc7	brána
ocitl	ocitnout	k5eAaPmAgMnS	ocitnout
veterán	veterán	k1gMnSc1	veterán
Manchesteru	Manchester	k1gInSc2	Manchester
Ryan	Ryan	k1gMnSc1	Ryan
Giggs	Giggsa	k1gFnPc2	Giggsa
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
střelou	střela	k1gFnSc7	střela
k	k	k7c3	k
tyči	tyč	k1gFnSc3	tyč
otevřel	otevřít	k5eAaPmAgMnS	otevřít
skóre	skóre	k1gNnPc4	skóre
<g/>
.	.	kIx.	.
</s>
<s>
Zápas	zápas	k1gInSc1	zápas
dospěl	dochvít	k5eAaPmAgInS	dochvít
do	do	k7c2	do
prodloužení	prodloužení	k1gNnSc2	prodloužení
a	a	k8xC	a
skončil	skončit	k5eAaPmAgMnS	skončit
výhrou	výhra	k1gFnSc7	výhra
Chelsea	Chelseum	k1gNnSc2	Chelseum
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
vyřazení	vyřazení	k1gNnSc6	vyřazení
Manchesteru	Manchester	k1gInSc2	Manchester
United	United	k1gInSc1	United
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
utkala	utkat	k5eAaPmAgFnS	utkat
s	s	k7c7	s
Leedsem	Leeds	k1gInSc7	Leeds
United	United	k1gMnSc1	United
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
opakovaném	opakovaný	k2eAgNnSc6d1	opakované
čtvrtfinálovém	čtvrtfinálový	k2eAgNnSc6d1	čtvrtfinálové
utkání	utkání	k1gNnSc6	utkání
FA	fa	kA	fa
Cupu	cup	k1gInSc2	cup
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
(	(	kIx(	(
<g/>
první	první	k4xOgInSc1	první
zápas	zápas	k1gInSc1	zápas
skončil	skončit	k5eAaPmAgInS	skončit
remízou	remíza	k1gFnSc7	remíza
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
opakoval	opakovat	k5eAaImAgInS	opakovat
<g/>
)	)	kIx)	)
opět	opět	k6eAd1	opět
proti	proti	k7c3	proti
Manchesteru	Manchester	k1gInSc2	Manchester
United	United	k1gInSc4	United
za	za	k7c2	za
stavu	stav	k1gInSc2	stav
1-0	[number]	k4	1-0
bravurně	bravurně	k6eAd1	bravurně
zlikvidoval	zlikvidovat	k5eAaPmAgInS	zlikvidovat
šanci	šance	k1gFnSc4	šance
Javiera	Javiero	k1gNnSc2	Javiero
Hernándeze	Hernándeze	k1gFnSc1	Hernándeze
Balcázara	Balcázara	k1gFnSc1	Balcázara
<g/>
.	.	kIx.	.
</s>
<s>
Pomohl	pomoct	k5eAaPmAgInS	pomoct
tak	tak	k9	tak
k	k	k7c3	k
vítězství	vítězství	k1gNnSc3	vítězství
Chelsea	Chelse	k1gInSc2	Chelse
1-0	[number]	k4	1-0
a	a	k8xC	a
postupu	postup	k1gInSc6	postup
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
proti	proti	k7c3	proti
Manchesteru	Manchester	k1gInSc3	Manchester
City	City	k1gFnSc2	City
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
inkasoval	inkasovat	k5eAaBmAgInS	inkasovat
dva	dva	k4xCgInPc4	dva
góly	gól	k1gInPc4	gól
a	a	k8xC	a
Manchester	Manchester	k1gInSc4	Manchester
City	City	k1gFnSc2	City
vyřadil	vyřadit	k5eAaPmAgMnS	vyřadit
Chelsea	Chelsea	k1gMnSc1	Chelsea
výsledkem	výsledek	k1gInSc7	výsledek
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Chelsea	Chelsea	k1gFnSc1	Chelsea
skončila	skončit	k5eAaPmAgFnS	skončit
v	v	k7c6	v
Lize	liga	k1gFnSc6	liga
mistrů	mistr	k1gMnPc2	mistr
na	na	k7c6	na
třetím	třetí	k4xOgNnSc6	třetí
místě	místo	k1gNnSc6	místo
základní	základní	k2eAgFnSc2d1	základní
skupiny	skupina	k1gFnSc2	skupina
E	E	kA	E
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
vyřazení	vyřazení	k1gNnSc1	vyřazení
a	a	k8xC	a
pokračování	pokračování	k1gNnSc1	pokračování
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
lize	liga	k1gFnSc6	liga
(	(	kIx(	(
<g/>
Chelsea	Chelsea	k1gFnSc1	Chelsea
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
prvním	první	k4xOgNnSc7	první
vítězem	vítěz	k1gMnSc7	vítěz
LM	LM	kA	LM
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
sezóně	sezóna	k1gFnSc6	sezóna
nepostoupil	postoupit	k5eNaPmAgInS	postoupit
ze	z	k7c2	z
základní	základní	k2eAgFnSc2d1	základní
skupiny	skupina	k1gFnSc2	skupina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jarního	jarní	k2eAgNnSc2d1	jarní
šestnáctifinále	šestnáctifinále	k1gNnSc2	šestnáctifinále
byl	být	k5eAaImAgInS	být
Chelsea	Chelseus	k1gMnSc2	Chelseus
přilosován	přilosován	k2eAgInSc1d1	přilosován
český	český	k2eAgInSc1d1	český
klub	klub	k1gInSc1	klub
AC	AC	kA	AC
Sparta	Sparta	k1gFnSc1	Sparta
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Čech	Čech	k1gMnSc1	Čech
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2013	[number]	k4	2013
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgInSc1d1	anglický
celek	celek	k1gInSc1	celek
zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
1-0	[number]	k4	1-0
gólem	gól	k1gInSc7	gól
mladého	mladý	k2eAgMnSc2d1	mladý
brazilského	brazilský	k2eAgMnSc2d1	brazilský
fotbalisty	fotbalista	k1gMnSc2	fotbalista
Oscara	Oscar	k1gMnSc2	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
týden	týden	k1gInSc4	týden
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
představil	představit	k5eAaPmAgInS	představit
i	i	k9	i
v	v	k7c6	v
odvetě	odveta	k1gFnSc6	odveta
na	na	k7c4	na
Stamford	Stamford	k1gInSc4	Stamford
Bridge	Bridg	k1gFnSc2	Bridg
a	a	k8xC	a
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
inkasoval	inkasovat	k5eAaBmAgMnS	inkasovat
gól	gól	k1gInSc4	gól
od	od	k7c2	od
Davida	David	k1gMnSc2	David
Lafaty	Lafata	k1gFnSc2	Lafata
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
branka	branka	k1gFnSc1	branka
Edena	Eden	k1gMnSc2	Eden
Hazarda	hazard	k1gMnSc2	hazard
na	na	k7c6	na
konečných	konečná	k1gFnPc6	konečná
1-1	[number]	k4	1-1
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
minutě	minuta	k1gFnSc6	minuta
nastaveného	nastavený	k2eAgInSc2d1	nastavený
času	čas	k1gInSc2	čas
znamenala	znamenat	k5eAaImAgFnS	znamenat
postup	postup	k1gInSc4	postup
Chelsea	Chelse	k1gInSc2	Chelse
do	do	k7c2	do
osmifinále	osmifinále	k1gNnSc2	osmifinále
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Chelsea	Chelsea	k1gMnSc1	Chelsea
nakonec	nakonec	k6eAd1	nakonec
postoupil	postoupit	k5eAaPmAgMnS	postoupit
až	až	k9	až
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c4	na
londýnský	londýnský	k2eAgInSc4d1	londýnský
tým	tým	k1gInSc4	tým
čekalo	čekat	k5eAaImAgNnS	čekat
portugalské	portugalský	k2eAgNnSc1d1	portugalské
mužstvo	mužstvo	k1gNnSc1	mužstvo
Benfica	Benfic	k1gInSc2	Benfic
Lisabon	Lisabon	k1gInSc1	Lisabon
<g/>
.	.	kIx.	.
</s>
<s>
Finále	finále	k1gNnSc1	finále
Evropské	evropský	k2eAgFnSc2d1	Evropská
ligy	liga	k1gFnSc2	liga
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2013	[number]	k4	2013
v	v	k7c6	v
Amsterdamu	Amsterdam	k1gInSc6	Amsterdam
<g/>
.	.	kIx.	.
</s>
<s>
Chelsea	Chelsea	k1gFnSc1	Chelsea
nakonec	nakonec	k6eAd1	nakonec
slavila	slavit	k5eAaImAgFnS	slavit
titul	titul	k1gInSc4	titul
po	po	k7c4	po
vítězství	vítězství	k1gNnSc4	vítězství
2-1	[number]	k4	2-1
gólem	gól	k1gInSc7	gól
Ivanoviće	Ivanović	k1gInSc2	Ivanović
v	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
minutě	minuta	k1gFnSc6	minuta
nastavení	nastavení	k1gNnSc2	nastavení
<g/>
.	.	kIx.	.
</s>
<s>
Čech	Čech	k1gMnSc1	Čech
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
inkasoval	inkasovat	k5eAaBmAgInS	inkasovat
gól	gól	k1gInSc1	gól
z	z	k7c2	z
pokutového	pokutový	k2eAgInSc2d1	pokutový
kopu	kop	k1gInSc2	kop
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
předtím	předtím	k6eAd1	předtím
založil	založit	k5eAaPmAgMnS	založit
akci	akce	k1gFnSc4	akce
vedoucí	vedoucí	k1gFnSc2	vedoucí
k	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
gólu	gól	k1gInSc3	gól
Chelsea	Chelse	k1gInSc2	Chelse
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
polovině	polovina	k1gFnSc6	polovina
hřiště	hřiště	k1gNnSc4	hřiště
si	se	k3xPyFc3	se
všiml	všimnout	k5eAaPmAgMnS	všimnout
volného	volný	k2eAgMnSc4d1	volný
Juana	Juan	k1gMnSc4	Juan
Maty	mat	k1gInPc4	mat
a	a	k8xC	a
hodil	hodit	k5eAaImAgMnS	hodit
mu	on	k3xPp3gMnSc3	on
rychle	rychle	k6eAd1	rychle
míč	míč	k1gInSc4	míč
<g/>
,	,	kIx,	,
Mata	mást	k5eAaImSgMnS	mást
jej	on	k3xPp3gMnSc4	on
ihned	ihned	k6eAd1	ihned
posunul	posunout	k5eAaPmAgMnS	posunout
na	na	k7c4	na
Fernanda	Fernando	k1gNnPc4	Fernando
Torrese	Torrese	k1gFnSc2	Torrese
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
běžel	běžet	k5eAaImAgMnS	běžet
sám	sám	k3xTgMnSc1	sám
na	na	k7c4	na
brankáře	brankář	k1gMnSc4	brankář
Artura	Artur	k1gMnSc4	Artur
a	a	k8xC	a
své	svůj	k3xOyFgNnSc4	svůj
sólo	sólo	k2eAgNnSc4d1	sólo
zakončil	zakončit	k5eAaPmAgMnS	zakončit
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
překonal	překonat	k5eAaPmAgInS	překonat
klubový	klubový	k2eAgInSc1d1	klubový
rekord	rekord	k1gInSc1	rekord
Chelsea	Chelse	k1gInSc2	Chelse
FC	FC	kA	FC
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
vychytaných	vychytaný	k2eAgFnPc2d1	vychytaná
nul	nula	k1gFnPc2	nula
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
soutěžích	soutěž	k1gFnPc6	soutěž
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
držel	držet	k5eAaImAgInS	držet
s	s	k7c7	s
208	[number]	k4	208
čistými	čistý	k2eAgInPc7d1	čistý
konty	konto	k1gNnPc7	konto
Peter	Peter	k1gMnSc1	Peter
Bonetti	Bonett	k2eAgMnPc1d1	Bonett
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
ligovém	ligový	k2eAgNnSc6d1	ligové
utkání	utkání	k1gNnSc6	utkání
s	s	k7c7	s
Hull	Hull	k1gMnSc1	Hull
City	City	k1gFnSc3	City
AFC	AFC	kA	AFC
(	(	kIx(	(
<g/>
výhra	výhra	k1gFnSc1	výhra
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
závěrečných	závěrečný	k2eAgFnPc2d1	závěrečná
kol	kola	k1gFnPc2	kola
bojoval	bojovat	k5eAaImAgInS	bojovat
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
o	o	k7c4	o
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nakonec	nakonec	k6eAd1	nakonec
získal	získat	k5eAaPmAgInS	získat
Manchester	Manchester	k1gInSc1	Manchester
City	City	k1gFnSc2	City
FC	FC	kA	FC
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
se	se	k3xPyFc4	se
do	do	k7c2	do
Chelsea	Chelseus	k1gMnSc2	Chelseus
vrátil	vrátit	k5eAaPmAgMnS	vrátit
z	z	k7c2	z
tříletého	tříletý	k2eAgNnSc2d1	tříleté
hostování	hostování	k1gNnSc2	hostování
v	v	k7c6	v
Atléticu	Atléticum	k1gNnSc6	Atléticum
Madrid	Madrid	k1gInSc4	Madrid
talentovaný	talentovaný	k2eAgMnSc1d1	talentovaný
belgický	belgický	k2eAgMnSc1d1	belgický
brankář	brankář	k1gMnSc1	brankář
Thibaut	Thibaut	k2eAgInSc4d1	Thibaut
Courtois	Courtois	k1gInSc4	Courtois
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
Petr	Petr	k1gMnSc1	Petr
Čech	Čech	k1gMnSc1	Čech
bojoval	bojovat	k5eAaImAgMnS	bojovat
v	v	k7c6	v
přípravě	příprava	k1gFnSc6	příprava
o	o	k7c4	o
místo	místo	k1gNnSc4	místo
brankářské	brankářský	k2eAgFnSc2d1	brankářská
jedničky	jednička	k1gFnSc2	jednička
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
sezony	sezona	k1gFnSc2	sezona
začal	začít	k5eAaPmAgMnS	začít
Čech	Čech	k1gMnSc1	Čech
na	na	k7c6	na
lavičce	lavička	k1gFnSc6	lavička
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
pro	pro	k7c4	pro
něj	on	k3xPp3gNnSc4	on
byla	být	k5eAaImAgFnS	být
nová	nový	k2eAgFnSc1d1	nová
role	role	k1gFnSc1	role
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
dostal	dostat	k5eAaPmAgMnS	dostat
český	český	k2eAgMnSc1d1	český
brankář	brankář	k1gMnSc1	brankář
příležitost	příležitost	k1gFnSc4	příležitost
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
ligovém	ligový	k2eAgInSc6d1	ligový
poháru	pohár	k1gInSc6	pohár
proti	proti	k7c3	proti
Boltonu	Bolton	k1gInSc3	Bolton
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Premier	Premier	k1gInSc4	Premier
League	Leagu	k1gFnSc2	Leagu
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
až	až	k6eAd1	až
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
proti	proti	k7c3	proti
Arsenalu	Arsenal	k1gInSc3	Arsenal
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ve	v	k7c6	v
24	[number]	k4	24
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
střídal	střídat	k5eAaImAgMnS	střídat
zraněného	zraněný	k1gMnSc2	zraněný
Courtoise	Courtoise	k1gFnSc2	Courtoise
za	za	k7c2	za
stavu	stav	k1gInSc2	stav
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
Chelsea	Chelsea	k1gFnSc1	Chelsea
zápas	zápas	k1gInSc4	zápas
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2014	[number]	k4	2014
vychytal	vychytat	k5eAaPmAgMnS	vychytat
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
<g />
.	.	kIx.	.
</s>
<s>
skupině	skupina	k1gFnSc3	skupina
G	G	kA	G
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
15	[number]	k4	15
výhru	výhra	k1gFnSc4	výhra
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
proti	proti	k7c3	proti
slovinskému	slovinský	k2eAgInSc3d1	slovinský
klubu	klub	k1gInSc3	klub
NK	NK	kA	NK
Maribor	Maribora	k1gFnPc2	Maribora
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2015	[number]	k4	2015
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
s	s	k7c7	s
Chelsea	Chelse	k1gInSc2	Chelse
potřetí	potřetí	k4xO	potřetí
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
kariéře	kariéra	k1gFnSc6	kariéra
Anglický	anglický	k2eAgInSc4d1	anglický
ligový	ligový	k2eAgInSc4d1	ligový
pohár	pohár	k1gInSc4	pohár
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2015	[number]	k4	2015
tři	tři	k4xCgFnPc4	tři
kola	kolo	k1gNnPc4	kolo
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
získal	získat	k5eAaPmAgMnS	získat
s	s	k7c7	s
Chelsea	Chelseum	k1gNnPc4	Chelseum
další	další	k2eAgInSc4d1	další
ligový	ligový	k2eAgInSc4d1	ligový
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2015	[number]	k4	2015
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
do	do	k7c2	do
Arsenalu	Arsenal	k1gInSc2	Arsenal
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podepsal	podepsat	k5eAaPmAgMnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
na	na	k7c4	na
4	[number]	k4	4
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Přestupová	přestupový	k2eAgFnSc1d1	přestupová
částka	částka	k1gFnSc1	částka
nebyla	být	k5eNaImAgFnS	být
zveřejněna	zveřejnit	k5eAaPmNgFnS	zveřejnit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podle	podle	k7c2	podle
spekulací	spekulace	k1gFnPc2	spekulace
médií	médium	k1gNnPc2	médium
byla	být	k5eAaImAgFnS	být
výše	výše	k1gFnSc1	výše
přestupu	přestup	k1gInSc2	přestup
stanovena	stanovit	k5eAaPmNgFnS	stanovit
na	na	k7c4	na
11	[number]	k4	11
milionů	milion	k4xCgInPc2	milion
liber	libra	k1gFnPc2	libra
<g/>
.	.	kIx.	.
</s>
<s>
Čechův	Čechův	k2eAgInSc4d1	Čechův
první	první	k4xOgInSc4	první
soutěžní	soutěžní	k2eAgInSc4d1	soutěžní
zápas	zápas	k1gInSc4	zápas
v	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
klubu	klub	k1gInSc6	klub
se	se	k3xPyFc4	se
paradoxně	paradoxně	k6eAd1	paradoxně
uskuteční	uskutečnit	k5eAaPmIp3nS	uskutečnit
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
proti	proti	k7c3	proti
jeho	jeho	k3xOp3gInSc3	jeho
bývalému	bývalý	k2eAgInSc3d1	bývalý
klubu	klub	k1gInSc3	klub
Chelsea	Chelseum	k1gNnSc2	Chelseum
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
Arsenal	Arsenal	k1gFnSc1	Arsenal
utká	utkat	k5eAaPmIp3nS	utkat
o	o	k7c4	o
anglický	anglický	k2eAgInSc4d1	anglický
Superpohár	superpohár	k1gInSc4	superpohár
(	(	kIx(	(
<g/>
Community	Communit	k1gInPc4	Communit
Shield	Shield	k1gInSc1	Shield
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
facebookovém	facebookový	k2eAgInSc6d1	facebookový
profilu	profil	k1gInSc6	profil
pak	pak	k6eAd1	pak
k	k	k7c3	k
přestupu	přestup	k1gInSc3	přestup
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Po	po	k7c6	po
11	[number]	k4	11
letech	léto	k1gNnPc6	léto
z	z	k7c2	z
Chelsea	Chelseum	k1gNnSc2	Chelseum
do	do	k7c2	do
Arsenalu	Arsenal	k1gInSc2	Arsenal
<g/>
.	.	kIx.	.
</s>
<s>
Mám	mít	k5eAaImIp1nS	mít
velkou	velký	k2eAgFnSc4d1	velká
radost	radost	k1gFnSc4	radost
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohu	moct	k5eAaImIp1nS	moct
spojit	spojit	k5eAaPmF	spojit
své	svůj	k3xOyFgFnPc4	svůj
síly	síla	k1gFnPc4	síla
s	s	k7c7	s
Arsenalem	Arsenal	k1gInSc7	Arsenal
<g/>
.	.	kIx.	.
</s>
<s>
Zůstal	zůstat	k5eAaPmAgMnS	zůstat
mi	já	k3xPp1nSc3	já
stejný	stejný	k2eAgInSc4d1	stejný
hlad	hlad	k1gInSc4	hlad
po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
jako	jako	k8xS	jako
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
mé	můj	k3xOp1gFnSc2	můj
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Miluji	milovat	k5eAaImIp1nS	milovat
výzvy	výzva	k1gFnPc4	výzva
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
stojí	stát	k5eAaImIp3nP	stát
před	před	k7c7	před
top	topit	k5eAaImRp2nS	topit
světovými	světový	k2eAgMnPc7d1	světový
hráči	hráč	k1gMnPc7	hráč
<g/>
,	,	kIx,	,
když	když	k8xS	když
nastupují	nastupovat	k5eAaImIp3nP	nastupovat
v	v	k7c4	v
Premier	Premier	k1gInSc4	Premier
League	Leagu	k1gFnSc2	Leagu
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
mi	já	k3xPp1nSc3	já
Arsè	Arsè	k1gMnSc1	Arsè
Wenger	Wengero	k1gNnPc2	Wengero
řekl	říct	k5eAaPmAgMnS	říct
o	o	k7c6	o
ambicích	ambice	k1gFnPc6	ambice
klubu	klub	k1gInSc2	klub
<g/>
,	,	kIx,	,
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
vidí	vidět	k5eAaImIp3nS	vidět
moji	můj	k3xOp1gFnSc4	můj
roli	role	k1gFnSc4	role
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
<g/>
,	,	kIx,	,
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
přípravě	příprava	k1gFnSc6	příprava
sbíral	sbírat	k5eAaImAgInS	sbírat
pouze	pouze	k6eAd1	pouze
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgNnSc6	první
soutěžním	soutěžní	k2eAgNnSc6d1	soutěžní
utkání	utkání	k1gNnSc6	utkání
nové	nový	k2eAgFnSc2d1	nová
sezóny	sezóna	k1gFnSc2	sezóna
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2015	[number]	k4	2015
(	(	kIx(	(
<g/>
Community	Communita	k1gFnSc2	Communita
Shield	Shield	k1gInSc1	Shield
2015	[number]	k4	2015
<g/>
-	-	kIx~	-
anglický	anglický	k2eAgInSc1d1	anglický
Superpohár	superpohár	k1gInSc1	superpohár
<g/>
)	)	kIx)	)
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
proti	proti	k7c3	proti
svému	svůj	k3xOyFgInSc3	svůj
bývalému	bývalý	k2eAgInSc3d1	bývalý
klubu	klub	k1gInSc3	klub
Chelsea	Chelseus	k1gMnSc2	Chelseus
a	a	k8xC	a
podílel	podílet	k5eAaImAgMnS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
výhře	výhra	k1gFnSc6	výhra
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
zápasu	zápas	k1gInSc3	zápas
za	za	k7c4	za
Arsenal	Arsenal	k1gFnSc4	Arsenal
v	v	k7c6	v
Lize	liga	k1gFnSc6	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
2015	[number]	k4	2015
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2015	[number]	k4	2015
na	na	k7c4	na
Emirates	Emirates	k1gInSc4	Emirates
Stadium	stadium	k1gNnSc4	stadium
proti	proti	k7c3	proti
favorizovanému	favorizovaný	k2eAgInSc3d1	favorizovaný
Bayernu	Bayern	k1gInSc3	Bayern
Mnichov	Mnichov	k1gInSc4	Mnichov
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tahal	tahat	k5eAaImAgInS	tahat
šňůru	šňůra	k1gFnSc4	šňůra
výher	výhra	k1gFnPc2	výhra
<g/>
.	.	kIx.	.
</s>
<s>
Vychytal	vychytat	k5eAaPmAgMnS	vychytat
čisté	čistý	k2eAgNnSc4d1	čisté
konto	konto	k1gNnSc4	konto
při	při	k7c6	při
výhře	výhra	k1gFnSc6	výhra
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
mužem	muž	k1gMnSc7	muž
utkání	utkání	k1gNnSc2	utkání
<g/>
..	..	k?	..
Proti	proti	k7c3	proti
Evertonu	Everton	k1gInSc2	Everton
si	se	k3xPyFc3	se
připsal	připsat	k5eAaPmAgMnS	připsat
výhru	výhra	k1gFnSc4	výhra
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
proti	proti	k7c3	proti
Swansea	Swanse	k1gInSc2	Swanse
City	city	k1gNnSc2	city
AFC	AFC	kA	AFC
si	se	k3xPyFc3	se
připsal	připsat	k5eAaPmAgInS	připsat
čisté	čistý	k2eAgNnSc4d1	čisté
konto	konto	k1gNnSc4	konto
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
<g />
.	.	kIx.	.
</s>
<s>
0	[number]	k4	0
<g/>
)	)	kIx)	)
a	a	k8xC	a
už	už	k6eAd1	už
jen	jen	k9	jen
jediná	jediný	k2eAgFnSc1d1	jediná
vychytaná	vychytaný	k2eAgFnSc1d1	vychytaná
nula	nula	k1gFnSc1	nula
jej	on	k3xPp3gMnSc4	on
dělila	dělit	k5eAaImAgFnS	dělit
od	od	k7c2	od
vyrovnání	vyrovnání	k1gNnSc2	vyrovnání
rekordu	rekord	k1gInSc2	rekord
Davida	David	k1gMnSc2	David
Jamese	Jamese	k1gFnSc2	Jamese
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
čistých	čistý	k2eAgNnPc2d1	čisté
kont	konto	k1gNnPc2	konto
v	v	k7c4	v
Premier	Premier	k1gInSc4	Premier
League	Leagu	k1gFnSc2	Leagu
<g/>
..	..	k?	..
V	v	k7c6	v
odvetě	odveta	k1gFnSc6	odveta
LM	LM	kA	LM
proti	proti	k7c3	proti
Bayernu	Bayern	k1gInSc3	Bayern
dostal	dostat	k5eAaPmAgMnS	dostat
Petr	Petr	k1gMnSc1	Petr
Čech	Čech	k1gMnSc1	Čech
přes	přes	k7c4	přes
dobrý	dobrý	k2eAgInSc4d1	dobrý
výkon	výkon	k1gInSc4	výkon
pět	pět	k4xCc1	pět
branek	branka	k1gFnPc2	branka
(	(	kIx(	(
<g/>
prohra	prohra	k1gFnSc1	prohra
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
Tottenhamu	Tottenham	k1gInSc3	Tottenham
Spurs	Spursa	k1gFnPc2	Spursa
měl	mít	k5eAaImAgInS	mít
Čech	Čechy	k1gFnPc2	Čechy
možnost	možnost	k1gFnSc1	možnost
rekord	rekord	k1gInSc1	rekord
vyrovnat	vyrovnat	k5eAaPmF	vyrovnat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
nepovedlo	povést	k5eNaPmAgNnS	povést
a	a	k8xC	a
zápas	zápas	k1gInSc4	zápas
skončil	skončit	k5eAaPmAgMnS	skončit
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2015	[number]	k4	2015
vychytal	vychytat	k5eAaPmAgInS	vychytat
169	[number]	k4	169
<g/>
.	.	kIx.	.
čisté	čistý	k2eAgNnSc1d1	čisté
konto	konto	k1gNnSc1	konto
v	v	k7c4	v
Premier	Premier	k1gInSc4	Premier
League	Leagu	k1gFnSc2	Leagu
(	(	kIx(	(
<g/>
v	v	k7c6	v
utkání	utkání	k1gNnSc6	utkání
proti	proti	k7c3	proti
Aston	Aston	k1gMnSc1	Aston
Villa	Vill	k1gMnSc2	Vill
FC	FC	kA	FC
<g/>
,	,	kIx,	,
výhra	výhra	k1gFnSc1	výhra
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
a	a	k8xC	a
vyrovnal	vyrovnat	k5eAaPmAgMnS	vyrovnat
tak	tak	k9	tak
rekord	rekord	k1gInSc4	rekord
Davida	David	k1gMnSc2	David
Jamese	Jamese	k1gFnSc2	Jamese
a	a	k8xC	a
celé	celý	k2eAgFnSc2d1	celá
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
170	[number]	k4	170
<g/>
.	.	kIx.	.
čisté	čistý	k2eAgNnSc1d1	čisté
konto	konto	k1gNnSc1	konto
v	v	k7c4	v
Premier	Premier	k1gInSc4	Premier
League	League	k1gNnSc4	League
vychytal	vychytat	k5eAaPmAgInS	vychytat
a	a	k8xC	a
rekord	rekord	k1gInSc1	rekord
Jamese	Jamese	k1gFnSc2	Jamese
překonal	překonat	k5eAaPmAgInS	překonat
28	[number]	k4	28
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
proti	proti	k7c3	proti
AFC	AFC	kA	AFC
Bournemouth	Bournemouth	k1gInSc1	Bournemouth
(	(	kIx(	(
<g/>
výhra	výhra	k1gFnSc1	výhra
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Účastnil	účastnit	k5eAaImAgInS	účastnit
se	se	k3xPyFc4	se
mistrovství	mistrovství	k1gNnSc2	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
do	do	k7c2	do
16	[number]	k4	16
let	léto	k1gNnPc2	léto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
skončil	skončit	k5eAaPmAgInS	skončit
český	český	k2eAgInSc1d1	český
tým	tým	k1gInSc1	tým
čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
do	do	k7c2	do
18	[number]	k4	18
let	léto	k1gNnPc2	léto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
hrálo	hrát	k5eAaImAgNnS	hrát
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
obsadila	obsadit	k5eAaPmAgFnS	obsadit
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
rovněž	rovněž	k9	rovněž
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
hráčů	hráč	k1gMnPc2	hráč
do	do	k7c2	do
20	[number]	k4	20
let	léto	k1gNnPc2	léto
v	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ČR	ČR	kA	ČR
prohrála	prohrát	k5eAaPmAgFnS	prohrát
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
s	s	k7c7	s
Paraguayí	Paraguay	k1gFnSc7	Paraguay
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
získal	získat	k5eAaPmAgMnS	získat
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
hráčů	hráč	k1gMnPc2	hráč
do	do	k7c2	do
21	[number]	k4	21
let	léto	k1gNnPc2	léto
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reprezentačním	reprezentační	k2eAgNnSc6d1	reprezentační
A-týmu	Aýmo	k1gNnSc6	A-týmo
debutoval	debutovat	k5eAaBmAgMnS	debutovat
12	[number]	k4	12
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2002	[number]	k4	2002
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
proti	proti	k7c3	proti
Maďarsku	Maďarsko	k1gNnSc3	Maďarsko
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
český	český	k2eAgInSc1d1	český
tým	tým	k1gInSc1	tým
zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
oporou	opora	k1gFnSc7	opora
reprezentačního	reprezentační	k2eAgNnSc2d1	reprezentační
družstva	družstvo	k1gNnSc2	družstvo
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
v	v	k7c6	v
kvalifikačním	kvalifikační	k2eAgNnSc6d1	kvalifikační
střetnutí	střetnutí	k1gNnSc6	střetnutí
v	v	k7c6	v
Arménii	Arménie	k1gFnSc6	Arménie
vychytal	vychytat	k5eAaPmAgMnS	vychytat
výhru	výhra	k1gFnSc4	výhra
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
jeho	jeho	k3xOp3gInSc1	jeho
100	[number]	k4	100
<g/>
.	.	kIx.	.
reprezentační	reprezentační	k2eAgInSc4d1	reprezentační
zápas	zápas	k1gInSc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
teprve	teprve	k6eAd1	teprve
druhým	druhý	k4xOgMnSc7	druhý
hráčem	hráč	k1gMnSc7	hráč
české	český	k2eAgFnSc2d1	Česká
reprezentace	reprezentace	k1gFnSc2	reprezentace
(	(	kIx(	(
<g/>
po	po	k7c6	po
Karlu	Karel	k1gMnSc6	Karel
Poborském	Poborský	k2eAgMnSc6d1	Poborský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
absolvovat	absolvovat	k5eAaPmF	absolvovat
100	[number]	k4	100
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
utkání	utkání	k1gNnSc1	utkání
v	v	k7c6	v
národním	národní	k2eAgInSc6d1	národní
dresu	dres	k1gInSc6	dres
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
září	září	k1gNnSc4	září
2015	[number]	k4	2015
český	český	k2eAgInSc1d1	český
národní	národní	k2eAgInSc1d1	národní
tým	tým	k1gInSc1	tým
s	s	k7c7	s
Čechem	Čech	k1gMnSc7	Čech
v	v	k7c6	v
brance	branka	k1gFnSc6	branka
postoupil	postoupit	k5eAaPmAgMnS	postoupit
na	na	k7c4	na
EURO	euro	k1gNnSc4	euro
2016	[number]	k4	2016
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jubilejní	jubilejní	k2eAgFnSc6d1	jubilejní
desáté	desátá	k1gFnSc6	desátá
výhře	výhra	k1gFnSc6	výhra
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Zlatý	zlatý	k2eAgInSc4d1	zlatý
míč	míč	k1gInSc4	míč
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2016	[number]	k4	2016
ohlásil	ohlásit	k5eAaPmAgInS	ohlásit
konec	konec	k1gInSc4	konec
své	svůj	k3xOyFgFnSc2	svůj
dlouholeté	dlouholetý	k2eAgFnSc2d1	dlouholetá
reprezentační	reprezentační	k2eAgFnSc2d1	reprezentační
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Účast	účast	k1gFnSc1	účast
Petra	Petr	k1gMnSc2	Petr
Čecha	Čech	k1gMnSc2	Čech
na	na	k7c6	na
vrcholových	vrcholový	k2eAgInPc6d1	vrcholový
turnajích	turnaj	k1gInPc6	turnaj
<g/>
:	:	kIx,	:
ME	ME	kA	ME
2004	[number]	k4	2004
v	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
MS	MS	kA	MS
2006	[number]	k4	2006
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
ME	ME	kA	ME
2008	[number]	k4	2008
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
a	a	k8xC	a
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
ME	ME	kA	ME
2012	[number]	k4	2012
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
ME	ME	kA	ME
2016	[number]	k4	2016
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
Zápasy	zápas	k1gInPc1	zápas
Petra	Petr	k1gMnSc2	Petr
Čecha	Čech	k1gMnSc2	Čech
v	v	k7c6	v
A-mužstvu	Aužstvo	k1gNnSc6	A-mužstvo
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
2002	[number]	k4	2002
Mistrovství	mistrovství	k1gNnPc2	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
U-21	U-21	k1gFnSc1	U-21
(	(	kIx(	(
<g/>
Česko	Česko	k1gNnSc1	Česko
U-	U-	k1gFnSc2	U-
<g/>
21	[number]	k4	21
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
2004	[number]	k4	2004
Mistrovství	mistrovství	k1gNnPc2	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
Česko	Česko	k1gNnSc1	Česko
<g/>
)	)	kIx)	)
2005	[number]	k4	2005
Anglický	anglický	k2eAgInSc1d1	anglický
ligový	ligový	k2eAgInSc1d1	ligový
pohár	pohár	k1gInSc1	pohár
(	(	kIx(	(
<g/>
Chelsea	Chelse	k2eAgFnSc1d1	Chelsea
FC	FC	kA	FC
<g/>
)	)	kIx)	)
2005	[number]	k4	2005
Premier	Premiero	k1gNnPc2	Premiero
League	League	k1gFnPc2	League
(	(	kIx(	(
<g/>
Chelsea	Chelse	k2eAgFnSc1d1	Chelsea
FC	FC	kA	FC
<g/>
)	)	kIx)	)
2005	[number]	k4	2005
Community	Communita	k1gFnSc2	Communita
Shield	Shielda	k1gFnPc2	Shielda
(	(	kIx(	(
<g/>
Chelsea	Chelse	k2eAgFnSc1d1	Chelsea
FC	FC	kA	FC
<g/>
)	)	kIx)	)
2006	[number]	k4	2006
Premier	Premiero	k1gNnPc2	Premiero
League	League	k1gFnPc2	League
(	(	kIx(	(
<g/>
Chelsea	Chelse	k2eAgFnSc1d1	Chelsea
FC	FC	kA	FC
<g/>
)	)	kIx)	)
2007	[number]	k4	2007
Anglický	anglický	k2eAgInSc1d1	anglický
ligový	ligový	k2eAgInSc1d1	ligový
pohár	pohár	k1gInSc1	pohár
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Chelsea	Chelseus	k1gMnSc4	Chelseus
FC	FC	kA	FC
<g/>
)	)	kIx)	)
2007	[number]	k4	2007
FA	fa	kA	fa
Cup	cup	k1gInSc1	cup
(	(	kIx(	(
<g/>
Chelsea	Chelse	k2eAgFnSc1d1	Chelsea
FC	FC	kA	FC
<g/>
)	)	kIx)	)
2008	[number]	k4	2008
Liga	liga	k1gFnSc1	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
(	(	kIx(	(
<g/>
Chelsea	Chelsea	k1gMnSc1	Chelsea
FC	FC	kA	FC
<g/>
)	)	kIx)	)
2009	[number]	k4	2009
FA	fa	kA	fa
Cup	cup	k1gInSc1	cup
(	(	kIx(	(
<g/>
Chelsea	Chelse	k2eAgFnSc1d1	Chelsea
FC	FC	kA	FC
<g/>
)	)	kIx)	)
2009	[number]	k4	2009
Community	Communita	k1gFnSc2	Communita
Shield	Shielda	k1gFnPc2	Shielda
(	(	kIx(	(
<g/>
Chelsea	Chelse	k2eAgFnSc1d1	Chelsea
FC	FC	kA	FC
<g/>
)	)	kIx)	)
2010	[number]	k4	2010
Premier	Premiero	k1gNnPc2	Premiero
League	League	k1gFnPc2	League
(	(	kIx(	(
<g/>
Chelsea	Chelsea	k1gFnSc1	Chelsea
FC	FC	kA	FC
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
2010	[number]	k4	2010
FA	fa	kA	fa
Cup	cup	k1gInSc1	cup
(	(	kIx(	(
<g/>
Chelsea	Chelse	k2eAgFnSc1d1	Chelsea
FC	FC	kA	FC
<g/>
)	)	kIx)	)
2012	[number]	k4	2012
FA	fa	kA	fa
Cup	cup	k1gInSc1	cup
(	(	kIx(	(
<g/>
Chelsea	Chelse	k2eAgFnSc1d1	Chelsea
FC	FC	kA	FC
<g/>
)	)	kIx)	)
2012	[number]	k4	2012
Liga	liga	k1gFnSc1	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
(	(	kIx(	(
<g/>
Chelsea	Chelsea	k1gMnSc1	Chelsea
FC	FC	kA	FC
<g/>
)	)	kIx)	)
2013	[number]	k4	2013
Evropská	evropský	k2eAgFnSc1d1	Evropská
liga	liga	k1gFnSc1	liga
UEFA	UEFA	kA	UEFA
(	(	kIx(	(
<g/>
Chelsea	Chelsea	k1gMnSc1	Chelsea
FC	FC	kA	FC
<g/>
)	)	kIx)	)
2015	[number]	k4	2015
Anglický	anglický	k2eAgInSc1d1	anglický
ligový	ligový	k2eAgInSc1d1	ligový
pohár	pohár	k1gInSc1	pohár
(	(	kIx(	(
<g/>
Chelsea	Chelse	k2eAgFnSc1d1	Chelsea
FC	FC	kA	FC
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
2015	[number]	k4	2015
Premier	Premier	k1gInSc1	Premier
League	League	k1gInSc4	League
(	(	kIx(	(
<g/>
Chelsea	Chelse	k2eAgFnSc1d1	Chelsea
FC	FC	kA	FC
<g/>
)	)	kIx)	)
2015	[number]	k4	2015
Community	Communita	k1gFnSc2	Communita
Shield	Shielda	k1gFnPc2	Shielda
(	(	kIx(	(
<g/>
Arsenal	Arsenal	k1gFnSc1	Arsenal
FC	FC	kA	FC
<g/>
)	)	kIx)	)
1999	[number]	k4	1999
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
český	český	k2eAgMnSc1d1	český
brankář	brankář	k1gMnSc1	brankář
2001	[number]	k4	2001
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
český	český	k2eAgMnSc1d1	český
brankář	brankář	k1gMnSc1	brankář
-	-	kIx~	-
Cena	cena	k1gFnSc1	cena
Františka	František	k1gMnSc2	František
Pláničky	plánička	k1gFnSc2	plánička
2002	[number]	k4	2002
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
český	český	k2eAgMnSc1d1	český
brankář	brankář	k1gMnSc1	brankář
-	-	kIx~	-
Cena	cena	k1gFnSc1	cena
Ivo	Ivo	k1gMnSc1	Ivo
Viktora	Viktora	k1gMnSc1	Viktora
2003	[number]	k4	2003
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
český	český	k2eAgMnSc1d1	český
brankář	brankář	k1gMnSc1	brankář
-	-	kIx~	-
Cena	cena	k1gFnSc1	cena
Ivo	Ivo	k1gMnSc1	Ivo
Viktora	Viktora	k1gMnSc1	Viktora
2004	[number]	k4	2004
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
brankář	brankář	k1gMnSc1	brankář
Ligue	Ligu	k1gFnSc2	Ligu
<g />
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
podle	podle	k7c2	podle
časopisu	časopis	k1gInSc2	časopis
France	Franc	k1gMnSc4	Franc
Football	Football	k1gInSc4	Football
2004	[number]	k4	2004
Zlatý	zlatý	k2eAgInSc4d1	zlatý
míč	míč	k1gInSc4	míč
mezi	mezi	k7c7	mezi
50	[number]	k4	50
nejlepšími	dobrý	k2eAgMnPc7d3	nejlepší
<g/>
;	;	kIx,	;
(	(	kIx(	(
<g/>
v	v	k7c6	v
celkovém	celkový	k2eAgNnSc6d1	celkové
hodnocení	hodnocení	k1gNnSc6	hodnocení
nezískal	získat	k5eNaPmAgMnS	získat
ani	ani	k9	ani
jeden	jeden	k4xCgInSc4	jeden
bod	bod	k1gInSc4	bod
<g/>
)	)	kIx)	)
2005	[number]	k4	2005
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
klubový	klubový	k2eAgMnSc1d1	klubový
brankář	brankář	k1gMnSc1	brankář
Evropy	Evropa	k1gFnSc2	Evropa
za	za	k7c4	za
sezonu	sezona	k1gFnSc4	sezona
2004	[number]	k4	2004
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
podle	podle	k7c2	podle
UEFA	UEFA	kA	UEFA
2005	[number]	k4	2005
Zlatý	zlatý	k2eAgInSc4d1	zlatý
míč	míč	k1gInSc4	míč
-	-	kIx~	-
nominace	nominace	k1gFnSc2	nominace
mezi	mezi	k7c7	mezi
50	[number]	k4	50
nejlepšími	dobrý	k2eAgMnPc7d3	nejlepší
<g/>
;	;	kIx,	;
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
2005	[number]	k4	2005
Zlatý	zlatý	k2eAgInSc4d1	zlatý
míč	míč	k1gInSc4	míč
ČR	ČR	kA	ČR
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
2005	[number]	k4	2005
Fotbalista	fotbalista	k1gMnSc1	fotbalista
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
Česko	Česko	k1gNnSc1	Česko
<g/>
)	)	kIx)	)
2005	[number]	k4	2005
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
2005	[number]	k4	2005
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
brankář	brankář	k1gMnSc1	brankář
světa	svět	k1gInSc2	svět
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2005	[number]	k4	2005
podle	podle	k7c2	podle
hlasování	hlasování	k1gNnSc2	hlasování
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
federace	federace	k1gFnSc2	federace
fotbalových	fotbalový	k2eAgMnPc2d1	fotbalový
historiků	historik	k1gMnPc2	historik
a	a	k8xC	a
statistiků	statistik	k1gMnPc2	statistik
(	(	kIx(	(
<g/>
IFFHS	IFFHS	kA	IFFHS
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
2006	[number]	k4	2006
Zlatý	zlatý	k2eAgInSc4d1	zlatý
míč	míč	k1gInSc4	míč
ČR	ČR	kA	ČR
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
2007	[number]	k4	2007
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
klubový	klubový	k2eAgMnSc1d1	klubový
brankář	brankář	k1gMnSc1	brankář
Evropy	Evropa	k1gFnSc2	Evropa
za	za	k7c4	za
sezonu	sezona	k1gFnSc4	sezona
2006	[number]	k4	2006
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
podle	podle	k7c2	podle
UEFA	UEFA	kA	UEFA
2007	[number]	k4	2007
Premier	Premier	k1gInSc1	Premier
League	League	k1gNnSc2	League
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
hráč	hráč	k1gMnSc1	hráč
měsíce	měsíc	k1gInSc2	měsíc
března	březen	k1gInSc2	březen
2007	[number]	k4	2007
2007	[number]	k4	2007
Zlatý	zlatý	k2eAgInSc4d1	zlatý
míč	míč	k1gInSc4	míč
ČR	ČR	kA	ČR
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
2008	[number]	k4	2008
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
klubový	klubový	k2eAgMnSc1d1	klubový
brankář	brankář	k1gMnSc1	brankář
Evropy	Evropa	k1gFnSc2	Evropa
za	za	k7c4	za
sezonu	sezona	k1gFnSc4	sezona
<g />
.	.	kIx.	.
</s>
<s>
2007	[number]	k4	2007
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
podle	podle	k7c2	podle
UEFA	UEFA	kA	UEFA
2008	[number]	k4	2008
Fotbalista	fotbalista	k1gMnSc1	fotbalista
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
Česko	Česko	k1gNnSc1	Česko
<g/>
)	)	kIx)	)
2008	[number]	k4	2008
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
2008	[number]	k4	2008
Zlatý	zlatý	k2eAgInSc1d1	zlatý
míč	míč	k1gInSc1	míč
ČR	ČR	kA	ČR
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
2009	[number]	k4	2009
Fotbalista	fotbalista	k1gMnSc1	fotbalista
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
Česko	Česko	k1gNnSc1	Česko
<g/>
)	)	kIx)	)
2009	[number]	k4	2009
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
2010	[number]	k4	2010
Zlatý	zlatý	k2eAgInSc1d1	zlatý
míč	míč	k1gInSc1	míč
ČR	ČR	kA	ČR
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
2010	[number]	k4	2010
Fotbalista	fotbalista	k1gMnSc1	fotbalista
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
Česko	Česko	k1gNnSc1	Česko
<g/>
)	)	kIx)	)
2010	[number]	k4	2010
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
2011	[number]	k4	2011
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
fotbalista	fotbalista	k1gMnSc1	fotbalista
Chelsea	Chelsea	k1gMnSc1	Chelsea
za	za	k7c4	za
sezónu	sezóna	k1gFnSc4	sezóna
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
2011	[number]	k4	2011
Zlatý	zlatý	k2eAgInSc4d1	zlatý
míč	míč	k1gInSc4	míč
ČR	ČR	kA	ČR
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
2011	[number]	k4	2011
Fotbalista	fotbalista	k1gMnSc1	fotbalista
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
Česko	Česko	k1gNnSc1	Česko
<g/>
)	)	kIx)	)
2011	[number]	k4	2011
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
2012	[number]	k4	2012
Zlatý	zlatý	k2eAgInSc1d1	zlatý
míč	míč	k1gInSc1	míč
ČR	ČR	kA	ČR
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
2013	[number]	k4	2013
Fotbalista	fotbalista	k1gMnSc1	fotbalista
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
Česko	Česko	k1gNnSc1	Česko
<g/>
)	)	kIx)	)
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2012	[number]	k4	2012
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
2014	[number]	k4	2014
Fotbalista	fotbalista	k1gMnSc1	fotbalista
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
Česko	Česko	k1gNnSc1	Česko
<g/>
)	)	kIx)	)
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2013	[number]	k4	2013
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
