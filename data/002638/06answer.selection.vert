<s>
Skupinu	skupina	k1gFnSc4	skupina
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
září	září	k1gNnSc6	září
1968	[number]	k4	1968
mladý	mladý	k1gMnSc1	mladý
baskytarista	baskytarista	k1gMnSc1	baskytarista
Milan	Milan	k1gMnSc1	Milan
"	"	kIx"	"
<g/>
Mejla	Mejla	k1gMnSc1	Mejla
<g/>
"	"	kIx"	"
Hlavsa	Hlavsa	k1gMnSc1	Hlavsa
<g/>
,	,	kIx,	,
dalšími	další	k2eAgInPc7d1	další
členy	člen	k1gInPc7	člen
byli	být	k5eAaImAgMnP	být
Michal	Michal	k1gMnSc1	Michal
Jernek	Jernek	k1gMnSc1	Jernek
(	(	kIx(	(
<g/>
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
klarinet	klarinet	k1gInSc1	klarinet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
"	"	kIx"	"
<g/>
Přemysl	Přemysl	k1gMnSc1	Přemysl
<g/>
"	"	kIx"	"
Števich	Števich	k1gMnSc1	Števich
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
Brabec	Brabec	k1gMnSc1	Brabec
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgMnSc1d1	bicí
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
o	o	k7c4	o
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgMnS	být
Brabec	Brabec	k1gMnSc1	Brabec
nahrazen	nahradit	k5eAaPmNgMnS	nahradit
Pavlem	Pavel	k1gMnSc7	Pavel
Zemanem	Zeman	k1gMnSc7	Zeman
a	a	k8xC	a
ke	k	k7c3	k
skupině	skupina	k1gFnSc3	skupina
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgMnS	přidat
kytarista	kytarista	k1gMnSc1	kytarista
a	a	k8xC	a
klávesista	klávesista	k1gMnSc1	klávesista
Josef	Josef	k1gMnSc1	Josef
Janíček	Janíček	k1gMnSc1	Janíček
<g/>
.	.	kIx.	.
</s>
