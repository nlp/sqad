<s>
The	The	k?	The
Plastic	Plastice	k1gFnPc2	Plastice
People	People	k1gFnSc1	People
of	of	k?	of
the	the	k?	the
Universe	Universe	k1gFnSc2	Universe
(	(	kIx(	(
<g/>
PPU	PPU	kA	PPU
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pražská	pražský	k2eAgFnSc1d1	Pražská
rocková	rockový	k2eAgFnSc1d1	rocková
kapela	kapela	k1gFnSc1	kapela
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
patřila	patřit	k5eAaImAgFnS	patřit
mezi	mezi	k7c7	mezi
hlavní	hlavní	k2eAgFnSc7d1	hlavní
protagonisty	protagonista	k1gMnPc7	protagonista
československého	československý	k2eAgNnSc2d1	Československé
hnutí	hnutí	k1gNnSc2	hnutí
undergroundu	underground	k1gInSc2	underground
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1968	[number]	k4	1968
až	až	k9	až
1988	[number]	k4	1988
a	a	k8xC	a
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
vznik	vznik	k1gInSc4	vznik
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgFnPc2d1	další
undergroundových	undergroundový	k2eAgFnPc2d1	undergroundová
kapel	kapela	k1gFnPc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
avantgardní	avantgardní	k2eAgFnSc1d1	avantgardní
skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
ocitala	ocitat	k5eAaImAgFnS	ocitat
nedobrovolně	dobrovolně	k6eNd1	dobrovolně
v	v	k7c6	v
opozici	opozice	k1gFnSc6	opozice
proti	proti	k7c3	proti
komunistickému	komunistický	k2eAgInSc3d1	komunistický
režimu	režim	k1gInSc3	režim
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
a	a	k8xC	a
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
nekonformnímu	konformní	k2eNgNnSc3d1	nekonformní
vystupování	vystupování	k1gNnSc3	vystupování
často	často	k6eAd1	často
zažívala	zažívat	k5eAaImAgFnS	zažívat
perzekuce	perzekuce	k1gFnSc1	perzekuce
a	a	k8xC	a
také	také	k9	také
věznění	věznění	k1gNnSc2	věznění
<g/>
.	.	kIx.	.
</s>
<s>
Skupinu	skupina	k1gFnSc4	skupina
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
září	září	k1gNnSc6	září
1968	[number]	k4	1968
mladý	mladý	k1gMnSc1	mladý
baskytarista	baskytarista	k1gMnSc1	baskytarista
Milan	Milan	k1gMnSc1	Milan
"	"	kIx"	"
<g/>
Mejla	Mejla	k1gMnSc1	Mejla
<g/>
"	"	kIx"	"
Hlavsa	Hlavsa	k1gMnSc1	Hlavsa
<g/>
,	,	kIx,	,
dalšími	další	k2eAgInPc7d1	další
členy	člen	k1gInPc7	člen
byli	být	k5eAaImAgMnP	být
Michal	Michal	k1gMnSc1	Michal
Jernek	Jernek	k1gMnSc1	Jernek
(	(	kIx(	(
<g/>
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
klarinet	klarinet	k1gInSc1	klarinet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
"	"	kIx"	"
<g/>
Přemysl	Přemysl	k1gMnSc1	Přemysl
<g/>
"	"	kIx"	"
Števich	Števich	k1gMnSc1	Števich
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
Brabec	Brabec	k1gMnSc1	Brabec
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgMnSc1d1	bicí
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
o	o	k7c4	o
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgMnS	být
Brabec	Brabec	k1gMnSc1	Brabec
nahrazen	nahradit	k5eAaPmNgMnS	nahradit
Pavlem	Pavel	k1gMnSc7	Pavel
Zemanem	Zeman	k1gMnSc7	Zeman
a	a	k8xC	a
ke	k	k7c3	k
skupině	skupina	k1gFnSc3	skupina
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgMnS	přidat
kytarista	kytarista	k1gMnSc1	kytarista
a	a	k8xC	a
klávesista	klávesista	k1gMnSc1	klávesista
Josef	Josef	k1gMnSc1	Josef
Janíček	Janíček	k1gMnSc1	Janíček
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
si	se	k3xPyFc3	se
dala	dát	k5eAaPmAgFnS	dát
jméno	jméno	k1gNnSc4	jméno
podle	podle	k7c2	podle
písně	píseň	k1gFnSc2	píseň
Franka	Frank	k1gMnSc2	Frank
Zappy	Zappa	k1gFnSc2	Zappa
Plastic	Plastice	k1gFnPc2	Plastice
People	People	k1gMnSc1	People
<g/>
.	.	kIx.	.
</s>
<s>
Tvorba	tvorba	k1gFnSc1	tvorba
kapely	kapela	k1gFnSc2	kapela
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
byla	být	k5eAaImAgFnS	být
ovlivněná	ovlivněný	k2eAgFnSc1d1	ovlivněná
například	například	k6eAd1	například
Frankem	Frank	k1gMnSc7	Frank
Zappou	Zappa	k1gMnSc7	Zappa
<g/>
,	,	kIx,	,
Captainem	Captain	k1gMnSc7	Captain
Beefheartem	Beefheart	k1gMnSc7	Beefheart
<g/>
,	,	kIx,	,
či	či	k8xC	či
skupinou	skupina	k1gFnSc7	skupina
The	The	k1gFnSc2	The
Velvet	Velvet	k1gInSc1	Velvet
Underground	underground	k1gInSc1	underground
<g/>
.	.	kIx.	.
</s>
<s>
Texty	text	k1gInPc1	text
jejich	jejich	k3xOp3gFnPc2	jejich
písní	píseň	k1gFnPc2	píseň
byly	být	k5eAaImAgFnP	být
zpočátku	zpočátku	k6eAd1	zpočátku
vlastní	vlastní	k2eAgFnPc1d1	vlastní
básně	báseň	k1gFnPc1	báseň
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
)	)	kIx)	)
či	či	k8xC	či
básně	báseň	k1gFnPc4	báseň
známých	známý	k2eAgMnPc2d1	známý
autorů	autor	k1gMnPc2	autor
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Jiřího	Jiří	k1gMnSc2	Jiří
Koláře	Kolář	k1gMnSc2	Kolář
či	či	k8xC	či
Williama	William	k1gMnSc2	William
Blakea	Blakeus	k1gMnSc2	Blakeus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hudbu	hudba	k1gFnSc4	hudba
většiny	většina	k1gFnSc2	většina
písní	píseň	k1gFnSc7	píseň
skládal	skládat	k5eAaImAgMnS	skládat
Hlavsa	Hlavsa	k1gMnSc1	Hlavsa
<g/>
.	.	kIx.	.
</s>
<s>
Vystoupení	vystoupení	k1gNnSc1	vystoupení
kapely	kapela	k1gFnSc2	kapela
bylo	být	k5eAaImAgNnS	být
často	často	k6eAd1	často
zvýrazněno	zvýraznit	k5eAaPmNgNnS	zvýraznit
vizuálními	vizuální	k2eAgInPc7d1	vizuální
prostředky	prostředek	k1gInPc7	prostředek
(	(	kIx(	(
<g/>
kostýmy	kostým	k1gInPc7	kostým
<g/>
,	,	kIx,	,
kulisy	kulisa	k1gFnPc4	kulisa
<g/>
,	,	kIx,	,
hořící	hořící	k2eAgInPc4d1	hořící
ohně	oheň	k1gInPc4	oheň
na	na	k7c6	na
pódiu	pódium	k1gNnSc6	pódium
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Manažerem	manažer	k1gMnSc7	manažer
a	a	k8xC	a
uměleckým	umělecký	k2eAgMnSc7d1	umělecký
vedoucím	vedoucí	k1gMnSc7	vedoucí
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
český	český	k2eAgMnSc1d1	český
kunsthistorik	kunsthistorik	k1gMnSc1	kunsthistorik
a	a	k8xC	a
kritik	kritik	k1gMnSc1	kritik
Ivan	Ivan	k1gMnSc1	Ivan
"	"	kIx"	"
<g/>
Magor	magor	k1gMnSc1	magor
<g/>
"	"	kIx"	"
Jirous	Jirous	k1gMnSc1	Jirous
a	a	k8xC	a
zastával	zastávat	k5eAaImAgMnS	zastávat
tak	tak	k6eAd1	tak
podobnou	podobný	k2eAgFnSc4d1	podobná
úlohu	úloha	k1gFnSc4	úloha
<g/>
,	,	kIx,	,
jako	jako	k9	jako
měl	mít	k5eAaImAgInS	mít
Andy	Anda	k1gFnSc2	Anda
Warhol	Warhol	k1gInSc1	Warhol
v	v	k7c6	v
případě	případ	k1gInSc6	případ
The	The	k1gFnSc1	The
Velvet	Velvet	k1gInSc1	Velvet
Underground	underground	k1gInSc1	underground
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
uvolnění	uvolnění	k1gNnSc3	uvolnění
totalitního	totalitní	k2eAgNnSc2d1	totalitní
sevření	sevření	k1gNnSc2	sevření
moci	moc	k1gFnSc2	moc
(	(	kIx(	(
<g/>
Pražské	pražský	k2eAgNnSc1d1	Pražské
jaro	jaro	k1gNnSc1	jaro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
ale	ale	k8xC	ale
sovětská	sovětský	k2eAgFnSc1d1	sovětská
armáda	armáda	k1gFnSc1	armáda
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
vojska	vojsko	k1gNnPc1	vojsko
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
obsadily	obsadit	k5eAaPmAgInP	obsadit
Československo	Československo	k1gNnSc4	Československo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nastolily	nastolit	k5eAaPmAgFnP	nastolit
opět	opět	k6eAd1	opět
tvrdou	tvrdý	k2eAgFnSc4d1	tvrdá
linii	linie	k1gFnSc4	linie
komunismu	komunismus	k1gInSc2	komunismus
-	-	kIx~	-
začal	začít	k5eAaPmAgInS	začít
proces	proces	k1gInSc1	proces
tzv.	tzv.	kA	tzv.
normalizace	normalizace	k1gFnSc2	normalizace
<g/>
.	.	kIx.	.
</s>
<s>
Normalizace	normalizace	k1gFnSc1	normalizace
se	se	k3xPyFc4	se
nevyhnula	vyhnout	k5eNaPmAgFnS	vyhnout
ani	ani	k8xC	ani
kultuře	kultura	k1gFnSc3	kultura
-	-	kIx~	-
jiná	jiný	k2eAgNnPc1d1	jiné
než	než	k8xS	než
režimem	režim	k1gInSc7	režim
podporovaná	podporovaný	k2eAgFnSc1d1	podporovaná
hudba	hudba	k1gFnSc1	hudba
byla	být	k5eAaImAgFnS	být
likvidována	likvidovat	k5eAaBmNgFnS	likvidovat
a	a	k8xC	a
hudební	hudební	k2eAgFnPc1d1	hudební
skupiny	skupina	k1gFnPc1	skupina
stály	stát	k5eAaImAgFnP	stát
před	před	k7c7	před
dilematem	dilema	k1gNnSc7	dilema
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
<g/>
,	,	kIx,	,
či	či	k8xC	či
riskovat	riskovat	k5eAaBmF	riskovat
problémy	problém	k1gInPc1	problém
se	se	k3xPyFc4	se
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Plastic	Plastice	k1gFnPc2	Plastice
People	People	k1gFnSc2	People
se	se	k3xPyFc4	se
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
podřídit	podřídit	k5eAaPmF	podřídit
požadavkům	požadavek	k1gInPc3	požadavek
na	na	k7c4	na
úpravu	úprava	k1gFnSc4	úprava
repertoáru	repertoár	k1gInSc2	repertoár
<g/>
,	,	kIx,	,
počeštění	počeštění	k1gNnSc1	počeštění
názvu	název	k1gInSc2	název
a	a	k8xC	a
zkrácení	zkrácení	k1gNnSc2	zkrácení
vlasů	vlas	k1gInPc2	vlas
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
dostávali	dostávat	k5eAaImAgMnP	dostávat
do	do	k7c2	do
stále	stále	k6eAd1	stále
větších	veliký	k2eAgInPc2d2	veliký
konfliktů	konflikt	k1gInPc2	konflikt
s	s	k7c7	s
představiteli	představitel	k1gMnPc7	představitel
moci	moct	k5eAaImF	moct
<g/>
.	.	kIx.	.
</s>
<s>
Sestava	sestava	k1gFnSc1	sestava
Plastic	Plastice	k1gFnPc2	Plastice
People	People	k1gMnSc1	People
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
několikrát	několikrát	k6eAd1	několikrát
měnila	měnit	k5eAaImAgFnS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Stabilní	stabilní	k2eAgNnSc4d1	stabilní
jádro	jádro	k1gNnSc4	jádro
tvořili	tvořit	k5eAaImAgMnP	tvořit
Milan	Milan	k1gMnSc1	Milan
Hlavsa	Hlavsa	k1gMnSc1	Hlavsa
(	(	kIx(	(
<g/>
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vratislav	Vratislav	k1gMnSc1	Vratislav
Brabenec	Brabenec	k1gMnSc1	Brabenec
(	(	kIx(	(
<g/>
saxofon	saxofon	k1gInSc1	saxofon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Janíček	Janíček	k1gMnSc1	Janíček
(	(	kIx(	(
<g/>
klávesy	kláves	k1gInPc1	kláves
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Kabeš	Kabeš	k1gFnSc2	Kabeš
(	(	kIx(	(
<g/>
housle	housle	k1gFnPc4	housle
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
za	za	k7c7	za
bicími	bicí	k2eAgFnPc7d1	bicí
se	se	k3xPyFc4	se
vystřídali	vystřídat	k5eAaPmAgMnP	vystřídat
postupně	postupně	k6eAd1	postupně
Pavel	Pavel	k1gMnSc1	Pavel
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Šula	Šula	k1gMnSc1	Šula
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vožniak	Vožniak	k1gMnSc1	Vožniak
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Brabec	Brabec	k1gMnSc1	Brabec
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
kanadský	kanadský	k2eAgMnSc1d1	kanadský
spisovatel	spisovatel	k1gMnSc1	spisovatel
Paul	Paul	k1gMnSc1	Paul
Wilson	Wilson	k1gMnSc1	Wilson
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
kapela	kapela	k1gFnSc1	kapela
hrála	hrát	k5eAaImAgFnS	hrát
převážně	převážně	k6eAd1	převážně
přejaté	přejatý	k2eAgFnPc4d1	přejatá
skladby	skladba	k1gFnPc4	skladba
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
interpretů	interpret	k1gMnPc2	interpret
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
začaly	začít	k5eAaPmAgFnP	začít
přibývat	přibývat	k5eAaImF	přibývat
vlastní	vlastní	k2eAgFnPc4d1	vlastní
skladby	skladba	k1gFnPc4	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
hudby	hudba	k1gFnSc2	hudba
byl	být	k5eAaImAgMnS	být
Hlavsa	Hlavsa	k1gMnSc1	Hlavsa
<g/>
,	,	kIx,	,
texty	text	k1gInPc1	text
byly	být	k5eAaImAgInP	být
nejčastěji	často	k6eAd3	často
básně	báseň	k1gFnSc2	báseň
nonkonformního	nonkonformní	k2eAgMnSc2d1	nonkonformní
filozofa	filozof	k1gMnSc2	filozof
a	a	k8xC	a
básníka	básník	k1gMnSc2	básník
Egona	Egon	k1gMnSc2	Egon
Bondyho	Bondy	k1gMnSc2	Bondy
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
konceptuálnějších	konceptuální	k2eAgNnPc6d2	konceptuální
dílech	dílo	k1gNnPc6	dílo
se	se	k3xPyFc4	se
jako	jako	k9	jako
textař	textař	k1gMnSc1	textař
autorsky	autorsky	k6eAd1	autorsky
zapsal	zapsat	k5eAaPmAgMnS	zapsat
saxofonista	saxofonista	k1gMnSc1	saxofonista
skupiny	skupina	k1gFnSc2	skupina
Vratislav	Vratislav	k1gMnSc1	Vratislav
Brabenec	Brabenec	k1gMnSc1	Brabenec
<g/>
.	.	kIx.	.
</s>
<s>
Skupině	skupina	k1gFnSc3	skupina
nebylo	být	k5eNaImAgNnS	být
umožněno	umožněn	k2eAgNnSc1d1	umožněno
nahrát	nahrát	k5eAaBmF	nahrát
žádné	žádný	k3yNgNnSc4	žádný
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
byli	být	k5eAaImAgMnP	být
fanoušci	fanoušek	k1gMnPc1	fanoušek
odkázáni	odkázán	k2eAgMnPc1d1	odkázán
na	na	k7c4	na
záznamy	záznam	k1gInPc4	záznam
z	z	k7c2	z
koncertů	koncert	k1gInPc2	koncert
<g/>
,	,	kIx,	,
mnohokrát	mnohokrát	k6eAd1	mnohokrát
kopírované	kopírovaný	k2eAgFnPc1d1	kopírovaná
a	a	k8xC	a
často	často	k6eAd1	často
velmi	velmi	k6eAd1	velmi
nekvalitní	kvalitní	k2eNgFnPc4d1	nekvalitní
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
záznamy	záznam	k1gInPc1	záznam
byly	být	k5eAaImAgInP	být
o	o	k7c4	o
desítky	desítka	k1gFnPc4	desítka
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
vydány	vydat	k5eAaPmNgInP	vydat
v	v	k7c6	v
remasterované	remasterovaný	k2eAgFnSc6d1	remasterovaná
podobě	podoba	k1gFnSc6	podoba
pod	pod	k7c4	pod
názvy	název	k1gInPc4	název
Muž	muž	k1gMnSc1	muž
bez	bez	k7c2	bez
uší	ucho	k1gNnPc2	ucho
<g/>
,	,	kIx,	,
Vožralej	Vožralej	k1gFnSc1	Vožralej
jak	jak	k8xC	jak
slíva	slíva	k1gFnSc1	slíva
<g/>
,	,	kIx,	,
Do	do	k7c2	do
lesíčka	lesíček	k1gInSc2	lesíček
na	na	k7c4	na
čekanou	čekaná	k1gFnSc4	čekaná
<g/>
,	,	kIx,	,
Trouble	Trouble	k1gMnSc1	Trouble
every	evera	k1gFnSc2	evera
day	day	k?	day
a	a	k8xC	a
Ach	ach	k0	ach
to	ten	k3xDgNnSc1	ten
státu	stát	k1gInSc3	stát
hanobení	hanobení	k1gNnSc2	hanobení
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
nebyl	být	k5eNaImAgMnS	být
Plastikům	plastik	k1gMnPc3	plastik
přiznán	přiznán	k2eAgInSc4d1	přiznán
profesionální	profesionální	k2eAgInSc4d1	profesionální
statut	statut	k1gInSc4	statut
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
prakticky	prakticky	k6eAd1	prakticky
ztratili	ztratit	k5eAaPmAgMnP	ztratit
možnost	možnost	k1gFnSc4	možnost
legálně	legálně	k6eAd1	legálně
veřejně	veřejně	k6eAd1	veřejně
vystupovat	vystupovat	k5eAaImF	vystupovat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
už	už	k6eAd1	už
hráli	hrát	k5eAaImAgMnP	hrát
jen	jen	k9	jen
na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
pro	pro	k7c4	pro
pozvané	pozvaný	k2eAgInPc4d1	pozvaný
-	-	kIx~	-
často	často	k6eAd1	často
pod	pod	k7c7	pod
záminkou	záminka	k1gFnSc7	záminka
svatby	svatba	k1gFnSc2	svatba
některého	některý	k3yIgMnSc2	některý
z	z	k7c2	z
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
koncert	koncert	k1gInSc4	koncert
do	do	k7c2	do
Rudolfova	Rudolfův	k2eAgMnSc2d1	Rudolfův
u	u	k7c2	u
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
přijely	přijet	k5eAaPmAgFnP	přijet
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1974	[number]	k4	1974
stovky	stovka	k1gFnPc4	stovka
příznivců	příznivec	k1gMnPc2	příznivec
kapely	kapela	k1gFnSc2	kapela
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
koncert	koncert	k1gInSc1	koncert
byl	být	k5eAaImAgInS	být
Veřejnou	veřejný	k2eAgFnSc7d1	veřejná
bezpečností	bezpečnost	k1gFnSc7	bezpečnost
násilně	násilně	k6eAd1	násilně
rozehnán	rozehnán	k2eAgMnSc1d1	rozehnán
a	a	k8xC	a
kapela	kapela	k1gFnSc1	kapela
vůbec	vůbec	k9	vůbec
nevystoupila	vystoupit	k5eNaPmAgFnS	vystoupit
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
mnoho	mnoho	k4c1	mnoho
soudních	soudní	k2eAgMnPc2d1	soudní
i	i	k8xC	i
mimosoudních	mimosoudní	k2eAgInPc2d1	mimosoudní
postihů	postih	k1gInPc2	postih
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
tentokrát	tentokrát	k6eAd1	tentokrát
ještě	ještě	k6eAd1	ještě
postižena	postihnout	k5eAaPmNgFnS	postihnout
nebyla	být	k5eNaImAgFnS	být
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1974	[number]	k4	1974
kapela	kapela	k1gFnSc1	kapela
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
na	na	k7c4	na
tzv.	tzv.	kA	tzv.
Prvním	první	k4xOgMnSc7	první
festivalu	festival	k1gInSc6	festival
druhé	druhý	k4xOgFnSc2	druhý
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
například	například	k6eAd1	například
DG	dg	kA	dg
307	[number]	k4	307
a	a	k8xC	a
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Karásek	Karásek	k1gMnSc1	Karásek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1974	[number]	k4	1974
a	a	k8xC	a
1975	[number]	k4	1975
nahráli	nahrát	k5eAaPmAgMnP	nahrát
Plastic	Plastice	k1gFnPc2	Plastice
People	People	k1gFnPc7	People
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Houska	houska	k1gFnSc1	houska
své	svůj	k3xOyFgFnSc6	svůj
první	první	k4xOgFnSc3	první
"	"	kIx"	"
<g/>
studiové	studiový	k2eAgNnSc4d1	studiové
<g/>
"	"	kIx"	"
album	album	k1gNnSc4	album
Egon	Egon	k1gMnSc1	Egon
Bondy	bond	k1gInPc4	bond
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Happy	Happ	k1gMnPc7	Happ
Hearts	Hearts	k1gInSc4	Hearts
Club	club	k1gInSc1	club
Banned	Banned	k1gInSc1	Banned
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgInPc1d1	obsahující
převážně	převážně	k6eAd1	převážně
Hlavsou	Hlavsa	k1gMnSc7	Hlavsa
zhudebněné	zhudebněný	k2eAgFnSc2d1	zhudebněná
texty	text	k1gInPc7	text
Egona	Egon	k1gMnSc2	Egon
Bondyho	Bondy	k1gMnSc2	Bondy
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
nemohlo	moct	k5eNaImAgNnS	moct
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
oficiálně	oficiálně	k6eAd1	oficiálně
vyjít	vyjít	k5eAaPmF	vyjít
a	a	k8xC	a
šířilo	šířit	k5eAaImAgNnS	šířit
se	se	k3xPyFc4	se
samizdatově	samizdatově	k6eAd1	samizdatově
mezi	mezi	k7c7	mezi
fanoušky	fanoušek	k1gMnPc7	fanoušek
<g/>
;	;	kIx,	;
oficiálního	oficiální	k2eAgNnSc2d1	oficiální
vydání	vydání	k1gNnSc2	vydání
se	se	k3xPyFc4	se
dočkalo	dočkat	k5eAaPmAgNnS	dočkat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
komunistická	komunistický	k2eAgFnSc1d1	komunistická
policie	policie	k1gFnSc1	policie
zatkla	zatknout	k5eAaPmAgFnS	zatknout
členy	člen	k1gMnPc4	člen
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
představiteli	představitel	k1gMnPc7	představitel
kulturního	kulturní	k2eAgInSc2d1	kulturní
undergroundu	underground	k1gInSc2	underground
je	být	k5eAaImIp3nS	být
postavila	postavit	k5eAaPmAgFnS	postavit
před	před	k7c4	před
soud	soud	k1gInSc4	soud
za	za	k7c4	za
údajné	údajný	k2eAgNnSc4d1	údajné
výtržnictví	výtržnictví	k1gNnSc4	výtržnictví
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
členů	člen	k1gInPc2	člen
kapely	kapela	k1gFnSc2	kapela
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
několika	několik	k4yIc6	několik
týdnech	týden	k1gInPc6	týden
pobytu	pobyt	k1gInSc2	pobyt
ve	v	k7c6	v
vazbě	vazba	k1gFnSc6	vazba
propuštěna	propustit	k5eAaPmNgFnS	propustit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
saxofonista	saxofonista	k1gMnSc1	saxofonista
Brabenec	Brabenec	k1gMnSc1	Brabenec
byl	být	k5eAaImAgMnS	být
shledán	shledat	k5eAaPmNgMnS	shledat
vinným	vinný	k1gMnSc7	vinný
a	a	k8xC	a
odsouzen	odsoudit	k5eAaPmNgInS	odsoudit
k	k	k7c3	k
nepodmíněnému	podmíněný	k2eNgInSc3d1	nepodmíněný
trestu	trest	k1gInSc2	trest
osmi	osm	k4xCc2	osm
měsíců	měsíc	k1gInPc2	měsíc
odnětí	odnětí	k1gNnSc2	odnětí
svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
a	a	k8xC	a
manažer	manažer	k1gMnSc1	manažer
kapely	kapela	k1gFnSc2	kapela
Jirous	Jirous	k1gMnSc1	Jirous
odešel	odejít	k5eAaPmAgMnS	odejít
od	od	k7c2	od
soudu	soud	k1gInSc2	soud
dokonce	dokonce	k9	dokonce
s	s	k7c7	s
osmnáctiměsíčním	osmnáctiměsíční	k2eAgInSc7d1	osmnáctiměsíční
trestem	trest	k1gInSc7	trest
<g/>
.	.	kIx.	.
</s>
<s>
Bývalý	bývalý	k2eAgMnSc1d1	bývalý
zpěvák	zpěvák	k1gMnSc1	zpěvák
skupiny	skupina	k1gFnSc2	skupina
Paul	Paul	k1gMnSc1	Paul
Wilson	Wilson	k1gMnSc1	Wilson
byl	být	k5eAaImAgMnS	být
později	pozdě	k6eAd2	pozdě
vyhoštěn	vyhostit	k5eAaPmNgMnS	vyhostit
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
dalšími	další	k2eAgMnPc7d1	další
odsouzenými	odsouzený	k2eAgMnPc7d1	odsouzený
muzikanty	muzikant	k1gMnPc7	muzikant
byli	být	k5eAaImAgMnP	být
například	například	k6eAd1	například
Pavel	Pavel	k1gMnSc1	Pavel
Zajíček	Zajíček	k1gMnSc1	Zajíček
<g/>
,	,	kIx,	,
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Karásek	Karásek	k1gMnSc1	Karásek
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
František	František	k1gMnSc1	František
"	"	kIx"	"
<g/>
Čuňas	Čuňas	k1gMnSc1	Čuňas
<g/>
"	"	kIx"	"
Stárek	Stárek	k1gMnSc1	Stárek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
protestů	protest	k1gInPc2	protest
proti	proti	k7c3	proti
uvěznění	uvěznění	k1gNnSc3	uvěznění
muzikantů	muzikant	k1gMnPc2	muzikant
posléze	posléze	k6eAd1	posléze
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
vzešla	vzejít	k5eAaPmAgFnS	vzejít
iniciativa	iniciativa	k1gFnSc1	iniciativa
Charty	charta	k1gFnSc2	charta
77	[number]	k4	77
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
sjednotila	sjednotit	k5eAaPmAgFnS	sjednotit
opozici	opozice	k1gFnSc4	opozice
proti	proti	k7c3	proti
komunistickému	komunistický	k2eAgInSc3d1	komunistický
režimu	režim	k1gInSc3	režim
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
představitelem	představitel	k1gMnSc7	představitel
Charty	charta	k1gFnSc2	charta
77	[number]	k4	77
byl	být	k5eAaImAgMnS	být
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
skupinou	skupina	k1gFnSc7	skupina
později	pozdě	k6eAd2	pozdě
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
na	na	k7c4	na
pořádání	pořádání	k1gNnSc4	pořádání
jejich	jejich	k3xOp3gInPc2	jejich
utajovaných	utajovaný	k2eAgInPc2d1	utajovaný
koncertů	koncert	k1gInPc2	koncert
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
usedlosti	usedlost	k1gFnSc6	usedlost
v	v	k7c6	v
Hrádečku	hrádeček	k1gInSc6	hrádeček
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
skupině	skupina	k1gFnSc3	skupina
vybíral	vybírat	k5eAaImAgMnS	vybírat
texty	text	k1gInPc4	text
jiných	jiný	k2eAgMnPc2d1	jiný
autorů	autor	k1gMnPc2	autor
ke	k	k7c3	k
zhudebnění	zhudebnění	k1gNnSc3	zhudebnění
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
na	na	k7c6	na
albech	album	k1gNnPc6	album
Hovězí	hovězí	k1gNnSc4	hovězí
porážka	porážka	k1gFnSc1	porážka
a	a	k8xC	a
Půlnoční	půlnoční	k2eAgFnSc1d1	půlnoční
myš	myš	k1gFnSc1	myš
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
propuštění	propuštění	k1gNnSc6	propuštění
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
Plastici	plastik	k1gMnPc1	plastik
v	v	k7c6	v
hudební	hudební	k2eAgFnSc6d1	hudební
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
svoji	svůj	k3xOyFgFnSc4	svůj
perzekuci	perzekuce	k1gFnSc4	perzekuce
nahráli	nahrát	k5eAaPmAgMnP	nahrát
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
kompozice	kompozice	k1gFnPc4	kompozice
100	[number]	k4	100
bodů	bod	k1gInPc2	bod
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
a	a	k8xC	a
Dopis	dopis	k1gInSc1	dopis
Magorovi	magor	k1gMnSc6	magor
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
jmenovaná	jmenovaný	k2eAgFnSc1d1	jmenovaná
skladba	skladba	k1gFnSc1	skladba
byla	být	k5eAaImAgFnS	být
zhudebněným	zhudebněný	k2eAgInSc7d1	zhudebněný
textem	text	k1gInSc7	text
českého	český	k2eAgMnSc2d1	český
novináře	novinář	k1gMnSc2	novinář
Františka	František	k1gMnSc2	František
Vaněčka	Vaněček	k1gMnSc2	Vaněček
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
shrnul	shrnout	k5eAaPmAgMnS	shrnout
<g/>
,	,	kIx,	,
čeho	co	k3yQnSc2	co
všeho	všecek	k3xTgNnSc2	všecek
se	se	k3xPyFc4	se
vládnoucí	vládnoucí	k2eAgFnSc1d1	vládnoucí
komunistická	komunistický	k2eAgFnSc1d1	komunistická
moc	moc	k1gFnSc1	moc
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
bojí	bát	k5eAaImIp3nP	bát
<g/>
.	.	kIx.	.
</s>
<s>
Dopis	dopis	k1gInSc1	dopis
Magorovi	magor	k1gMnSc3	magor
byl	být	k5eAaImAgInS	být
pozdravem	pozdrav	k1gInSc7	pozdrav
pro	pro	k7c4	pro
uměleckého	umělecký	k2eAgMnSc4d1	umělecký
vedoucího	vedoucí	k1gMnSc4	vedoucí
a	a	k8xC	a
manažera	manažer	k1gMnSc4	manažer
kapely	kapela	k1gFnSc2	kapela
Jirouse	Jirouse	k1gFnSc2	Jirouse
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
komunistickým	komunistický	k2eAgInSc7d1	komunistický
režimem	režim	k1gInSc7	režim
opakovaně	opakovaně	k6eAd1	opakovaně
zatýkán	zatýkán	k2eAgInSc4d1	zatýkán
a	a	k8xC	a
vězněn	vězněn	k2eAgInSc4d1	vězněn
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
členů	člen	k1gMnPc2	člen
Plastic	Plastice	k1gFnPc2	Plastice
People	People	k1gFnSc1	People
připojila	připojit	k5eAaPmAgFnS	připojit
svůj	svůj	k3xOyFgInSc4	svůj
pozdrav	pozdrav	k1gInSc4	pozdrav
i	i	k9	i
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgMnPc2d1	další
představitelů	představitel	k1gMnPc2	představitel
kulturního	kulturní	k2eAgInSc2d1	kulturní
undergroundu	underground	k1gInSc2	underground
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Pavel	Pavel	k1gMnSc1	Pavel
Zajíček	Zajíček	k1gMnSc1	Zajíček
<g/>
,	,	kIx,	,
Eugen	Eugen	k2eAgMnSc1d1	Eugen
Brikcius	Brikcius	k1gMnSc1	Brikcius
a	a	k8xC	a
také	také	k9	také
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
skladby	skladba	k1gFnPc1	skladba
později	pozdě	k6eAd2	pozdě
vyšly	vyjít	k5eAaPmAgFnP	vyjít
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
nahrávek	nahrávka	k1gFnPc2	nahrávka
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
Kolejnice	kolejnice	k1gFnSc2	kolejnice
duní	dunět	k5eAaImIp3nS	dunět
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
ale	ale	k9	ale
převážně	převážně	k6eAd1	převážně
věnovala	věnovat	k5eAaPmAgFnS	věnovat
ucelenějším	ucelený	k2eAgNnPc3d2	ucelenější
dílům	dílo	k1gNnPc3	dílo
bez	bez	k7c2	bez
politického	politický	k2eAgInSc2d1	politický
podtextu	podtext	k1gInSc2	podtext
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byla	být	k5eAaImAgFnS	být
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
Pašijové	pašijový	k2eAgFnSc2d1	pašijová
hry	hra	k1gFnSc2	hra
velikonoční	velikonoční	k2eAgInPc4d1	velikonoční
s	s	k7c7	s
texty	text	k1gInPc7	text
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Starého	Starého	k2eAgInSc2d1	Starého
a	a	k8xC	a
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
přibyl	přibýt	k5eAaPmAgInS	přibýt
projekt	projekt	k1gInSc1	projekt
Jak	jak	k6eAd1	jak
bude	být	k5eAaImBp3nS	být
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
-	-	kIx~	-
zhudebněné	zhudebněný	k2eAgInPc1d1	zhudebněný
texty	text	k1gInPc1	text
českého	český	k2eAgMnSc2d1	český
básníka	básník	k1gMnSc2	básník
a	a	k8xC	a
filosofa	filosof	k1gMnSc2	filosof
Ladislava	Ladislav	k1gMnSc2	Ladislav
Klímy	Klíma	k1gMnSc2	Klíma
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
spatřilo	spatřit	k5eAaPmAgNnS	spatřit
světlo	světlo	k1gNnSc1	světlo
světa	svět	k1gInSc2	svět
album	album	k1gNnSc1	album
Co	co	k9	co
znamená	znamenat	k5eAaImIp3nS	znamenat
vésti	vést	k5eAaImF	vést
koně	kůň	k1gMnSc4	kůň
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgMnSc3	který
napsal	napsat	k5eAaBmAgMnS	napsat
texty	text	k1gInPc7	text
Vratislav	Vratislav	k1gMnSc1	Vratislav
Brabenec	Brabenec	k1gMnSc1	Brabenec
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
projekt	projekt	k1gInSc1	projekt
Souostroví	souostroví	k1gNnSc1	souostroví
GULAG	gulag	k1gInSc1	gulag
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
knihy	kniha	k1gFnSc2	kniha
sovětského	sovětský	k2eAgMnSc2d1	sovětský
politického	politický	k2eAgMnSc2d1	politický
vězně	vězeň	k1gMnSc2	vězeň
Alexandra	Alexandr	k1gMnSc2	Alexandr
Solženicyna	Solženicyn	k1gMnSc2	Solženicyn
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
dokončit	dokončit	k5eAaPmF	dokončit
a	a	k8xC	a
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgFnPc4	tři
instrumentální	instrumentální	k2eAgFnPc4d1	instrumentální
skladby	skladba	k1gFnPc4	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Skladby	skladba	k1gFnPc1	skladba
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
lišily	lišit	k5eAaImAgFnP	lišit
od	od	k7c2	od
nahrávek	nahrávka	k1gFnPc2	nahrávka
kapely	kapela	k1gFnSc2	kapela
z	z	k7c2	z
konce	konec	k1gInSc2	konec
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgMnSc1d1	hudební
publicista	publicista	k1gMnSc1	publicista
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Riedel	Riedlo	k1gNnPc2	Riedlo
je	být	k5eAaImIp3nS	být
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Přes	přes	k7c4	přes
návrat	návrat	k1gInSc4	návrat
k	k	k7c3	k
písňové	písňový	k2eAgFnSc3d1	písňová
formě	forma	k1gFnSc3	forma
Plastic	Plastice	k1gFnPc2	Plastice
People	People	k1gMnPc1	People
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
v	v	k7c6	v
zadumaně	zadumaně	k6eAd1	zadumaně
artistní	artistní	k2eAgFnSc6d1	artistní
linii	linie	k1gFnSc6	linie
<g/>
,	,	kIx,	,
svým	svůj	k3xOyFgInSc7	svůj
chmurným	chmurný	k2eAgInSc7d1	chmurný
patosem	patos	k1gInSc7	patos
a	a	k8xC	a
využíváním	využívání	k1gNnSc7	využívání
postupů	postup	k1gInPc2	postup
církevní	církevní	k2eAgFnSc2d1	církevní
hudby	hudba	k1gFnSc2	hudba
se	se	k3xPyFc4	se
dosti	dosti	k6eAd1	dosti
vzdálili	vzdálit	k5eAaPmAgMnP	vzdálit
původním	původní	k2eAgFnPc3d1	původní
razantně	razantně	k6eAd1	razantně
rockovým	rockový	k2eAgNnPc3d1	rockové
východiskům	východisko	k1gNnPc3	východisko
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Počátkem	počátkem	k7c2	počátkem
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
byly	být	k5eAaImAgInP	být
vyvlastněny	vyvlastněn	k2eAgInPc1d1	vyvlastněn
domy	dům	k1gInPc1	dům
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
Plastic	Plastice	k1gFnPc2	Plastice
People	People	k1gMnPc1	People
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
dokonce	dokonce	k9	dokonce
někdo	někdo	k3yInSc1	někdo
zapálil	zapálit	k5eAaPmAgMnS	zapálit
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
Vratislav	Vratislav	k1gMnSc1	Vratislav
Brabenec	Brabenec	k1gMnSc1	Brabenec
donucen	donucen	k2eAgInSc1d1	donucen
státní	státní	k2eAgFnSc3d1	státní
bezpečnosti	bezpečnost	k1gFnSc3	bezpečnost
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tzv.	tzv.	kA	tzv.
akce	akce	k1gFnSc1	akce
Asanace	asanace	k1gFnSc2	asanace
emigrovat	emigrovat	k5eAaBmF	emigrovat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
už	už	k6eAd1	už
kapela	kapela	k1gFnSc1	kapela
nevystupovala	vystupovat	k5eNaImAgFnS	vystupovat
vůbec	vůbec	k9	vůbec
a	a	k9	a
tvořila	tvořit	k5eAaImAgFnS	tvořit
pouze	pouze	k6eAd1	pouze
"	"	kIx"	"
<g/>
studiový	studiový	k2eAgInSc1d1	studiový
<g/>
"	"	kIx"	"
materiál	materiál	k1gInSc1	materiál
-	-	kIx~	-
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
nahrávky	nahrávka	k1gFnPc4	nahrávka
Hovězí	hovězí	k2eAgFnSc1d1	hovězí
porážka	porážka	k1gFnSc1	porážka
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Půlnoční	půlnoční	k2eAgFnSc1d1	půlnoční
myš	myš	k1gFnSc1	myš
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
a	a	k8xC	a
hudba	hudba	k1gFnSc1	hudba
k	k	k7c3	k
Havlově	Havlův	k2eAgFnSc3d1	Havlova
hře	hra	k1gFnSc3	hra
Pokoušení	pokoušení	k1gNnPc2	pokoušení
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
ale	ale	k9	ale
nakonec	nakonec	k6eAd1	nakonec
nebyla	být	k5eNaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
<g/>
.	.	kIx.	.
</s>
<s>
Skupinu	skupina	k1gFnSc4	skupina
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
posílili	posílit	k5eAaPmAgMnP	posílit
klarinetista	klarinetista	k1gMnSc1	klarinetista
Petr	Petr	k1gMnSc1	Petr
Placák	placák	k1gInSc1	placák
<g/>
,	,	kIx,	,
violoncellista	violoncellista	k1gMnSc1	violoncellista
Tomáš	Tomáš	k1gMnSc1	Tomáš
Schilla	Schilla	k1gMnSc1	Schilla
<g/>
,	,	kIx,	,
kytarista	kytarista	k1gMnSc1	kytarista
Milan	Milan	k1gMnSc1	Milan
Schelinger	Schelinger	k1gMnSc1	Schelinger
(	(	kIx(	(
<g/>
bratr	bratr	k1gMnSc1	bratr
Jiřího	Jiří	k1gMnSc2	Jiří
Schelingera	Schelingero	k1gNnSc2	Schelingero
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
kapela	kapela	k1gFnSc1	kapela
působit	působit	k5eAaImF	působit
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Marta	Marta	k1gFnSc1	Marta
Kubišová	Kubišová	k1gFnSc1	Kubišová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
toto	tento	k3xDgNnSc4	tento
angažmá	angažmá	k1gNnSc4	angažmá
překazila	překazit	k5eAaPmAgFnS	překazit
StB	StB	k1gFnSc1	StB
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
ní	on	k3xPp3gFnSc2	on
se	s	k7c7	s
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
PPU	PPU	kA	PPU
stala	stát	k5eAaPmAgFnS	stát
Michaela	Michaela	k1gFnSc1	Michaela
Pohanková	Pohanková	k1gFnSc1	Pohanková
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1987	[number]	k4	1987
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
domluvit	domluvit	k5eAaPmF	domluvit
dva	dva	k4xCgInPc1	dva
legální	legální	k2eAgInPc1d1	legální
koncerty	koncert	k1gInPc1	koncert
Plastic	Plastice	k1gFnPc2	Plastice
People	People	k1gFnPc2	People
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obě	dva	k4xCgNnPc1	dva
dvě	dva	k4xCgNnPc1	dva
vystoupení	vystoupení	k1gNnPc1	vystoupení
byla	být	k5eAaImAgNnP	být
pořadateli	pořadatel	k1gMnPc7	pořadatel
na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
StB	StB	k1gFnPc2	StB
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
sporům	spor	k1gInPc3	spor
mezi	mezi	k7c7	mezi
hudebníky	hudebník	k1gMnPc7	hudebník
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
členů	člen	k1gMnPc2	člen
poté	poté	k6eAd1	poté
přešla	přejít	k5eAaPmAgFnS	přejít
do	do	k7c2	do
Hlavsovy	Hlavsův	k2eAgFnSc2d1	Hlavsova
nové	nový	k2eAgFnSc2d1	nová
kapely	kapela	k1gFnSc2	kapela
Půlnoc	půlnoc	k1gFnSc1	půlnoc
<g/>
.	.	kIx.	.
</s>
<s>
Půlnoc	půlnoc	k1gFnSc1	půlnoc
převzala	převzít	k5eAaPmAgFnS	převzít
některé	některý	k3yIgFnPc4	některý
skladby	skladba	k1gFnPc4	skladba
Plastiků	plastik	k1gMnPc2	plastik
<g/>
.	.	kIx.	.
</s>
<s>
Hrála	hrát	k5eAaImAgFnS	hrát
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
veřejně	veřejně	k6eAd1	veřejně
a	a	k8xC	a
bez	bez	k7c2	bez
větších	veliký	k2eAgInPc2d2	veliký
problémů	problém	k1gInPc2	problém
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
jí	on	k3xPp3gFnSc3	on
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
dovoleno	dovolit	k5eAaPmNgNnS	dovolit
vyjet	vyjet	k5eAaPmF	vyjet
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Hlavsa	Hlavsa	k1gMnSc1	Hlavsa
dále	daleko	k6eAd2	daleko
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
několika	několik	k4yIc6	několik
dalších	další	k2eAgFnPc6d1	další
hudebních	hudební	k2eAgFnPc6d1	hudební
skupinách	skupina	k1gFnPc6	skupina
(	(	kIx(	(
<g/>
Garáž	garáž	k1gFnSc1	garáž
<g/>
,	,	kIx,	,
Echt	Echt	k1gInSc1	Echt
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
Fiction	Fiction	k1gInSc1	Fiction
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
kapela	kapela	k1gFnSc1	kapela
nevystupovala	vystupovat	k5eNaImAgFnS	vystupovat
a	a	k8xC	a
členové	člen	k1gMnPc1	člen
se	se	k3xPyFc4	se
věnovali	věnovat	k5eAaPmAgMnP	věnovat
jiným	jiný	k2eAgInPc3d1	jiný
projektům	projekt	k1gInPc3	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
byl	být	k5eAaImAgInS	být
jeden	jeden	k4xCgInSc4	jeden
koncert	koncert	k1gInSc4	koncert
v	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
sestavě	sestava	k1gFnSc6	sestava
(	(	kIx(	(
<g/>
Hlavsa	Hlavsa	k1gMnSc1	Hlavsa
<g/>
,	,	kIx,	,
Janíček	Janíček	k1gMnSc1	Janíček
<g/>
,	,	kIx,	,
Števich	Števich	k1gMnSc1	Števich
<g/>
,	,	kIx,	,
Jernek	Jernek	k1gMnSc1	Jernek
<g/>
,	,	kIx,	,
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
zachycen	zachytit	k5eAaPmNgInS	zachytit
na	na	k7c6	na
albu	album	k1gNnSc6	album
Bez	bez	k7c2	bez
ohňů	oheň	k1gInPc2	oheň
je	být	k5eAaImIp3nS	být
underground	underground	k1gInSc1	underground
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
vydal	vydat	k5eAaPmAgMnS	vydat
Hlavsa	Hlavsa	k1gMnSc1	Hlavsa
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Vozárym	Vozárym	k1gInSc4	Vozárym
(	(	kIx(	(
<g/>
bývalým	bývalý	k2eAgInSc7d1	bývalý
členem	člen	k1gInSc7	člen
skupiny	skupina	k1gFnSc2	skupina
Oceán	oceán	k1gInSc1	oceán
<g/>
)	)	kIx)	)
album	album	k1gNnSc4	album
Magická	magický	k2eAgFnSc1d1	magická
noc	noc	k1gFnSc1	noc
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
starší	starý	k2eAgFnPc4d2	starší
písně	píseň	k1gFnPc4	píseň
Plastic	Plastice	k1gFnPc2	Plastice
People	People	k1gMnPc1	People
v	v	k7c6	v
moderním	moderní	k2eAgNnSc6d1	moderní
elektronickém	elektronický	k2eAgNnSc6d1	elektronické
aranžmá	aranžmá	k1gNnSc6	aranžmá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
zemřel	zemřít	k5eAaPmAgMnS	zemřít
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
skupiny	skupina	k1gFnSc2	skupina
Michal	Michal	k1gMnSc1	Michal
Jernek	Jernek	k1gMnSc1	Jernek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
sestavě	sestava	k1gFnSc6	sestava
(	(	kIx(	(
<g/>
Hlavsa	Hlavsa	k1gMnSc1	Hlavsa
<g/>
,	,	kIx,	,
Brabenec	Brabenec	k1gMnSc1	Brabenec
<g/>
,	,	kIx,	,
Janíček	Janíček	k1gMnSc1	Janíček
<g/>
,	,	kIx,	,
Kabeš	Kabeš	k1gMnSc1	Kabeš
<g/>
,	,	kIx,	,
Brabec	Brabec	k1gMnSc1	Brabec
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
s	s	k7c7	s
kytaristou	kytarista	k1gMnSc7	kytarista
Garáže	garáž	k1gFnSc2	garáž
Josefem	Josef	k1gMnSc7	Josef
Karafiátem	Karafiát	k1gMnSc7	Karafiát
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgInSc7	který
už	už	k6eAd1	už
dříve	dříve	k6eAd2	dříve
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
Brabenec	Brabenec	k1gMnSc1	Brabenec
na	na	k7c6	na
svých	svůj	k3xOyFgInPc6	svůj
sólových	sólový	k2eAgInPc6d1	sólový
projektech	projekt	k1gInPc6	projekt
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
Plastici	plastik	k1gMnPc1	plastik
sešli	sejít	k5eAaPmAgMnP	sejít
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
na	na	k7c4	na
přání	přání	k1gNnSc4	přání
Václav	Václava	k1gFnPc2	Václava
Havla	Havel	k1gMnSc2	Havel
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
výročí	výročí	k1gNnSc2	výročí
Charty	charta	k1gFnSc2	charta
77	[number]	k4	77
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
koncert	koncert	k1gInSc4	koncert
navázalo	navázat	k5eAaPmAgNnS	navázat
několik	několik	k4yIc1	několik
koncertních	koncertní	k2eAgNnPc2d1	koncertní
turné	turné	k1gNnPc2	turné
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
i	i	k8xC	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Koncertování	koncertování	k1gNnSc1	koncertování
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
přerušilo	přerušit	k5eAaPmAgNnS	přerušit
vážné	vážný	k2eAgNnSc4d1	vážné
onemocnění	onemocnění	k1gNnSc4	onemocnění
frontmana	frontman	k1gMnSc2	frontman
Milana	Milan	k1gMnSc2	Milan
Hlavsy	Hlavsa	k1gMnSc2	Hlavsa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
Hlavsa	Hlavsa	k1gMnSc1	Hlavsa
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
49	[number]	k4	49
let	léto	k1gNnPc2	léto
nemoci	nemoc	k1gFnSc2	nemoc
podlehl	podlehnout	k5eAaPmAgInS	podlehnout
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Hlavsově	Hlavsův	k2eAgFnSc6d1	Hlavsova
smrti	smrt	k1gFnSc6	smrt
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
zbývající	zbývající	k2eAgMnPc1d1	zbývající
muzikanti	muzikant	k1gMnPc1	muzikant
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
Hlavsovy	Hlavsův	k2eAgFnSc2d1	Hlavsova
vdovy	vdova	k1gFnSc2	vdova
na	na	k7c6	na
oslavě	oslava	k1gFnSc6	oslava
Hlavsových	Hlavsových	k2eAgNnPc2d1	Hlavsových
nedožitých	dožitý	k2eNgNnPc2d1	nedožité
padesátých	padesátý	k4xOgNnPc2	padesátý
narozenin	narozeniny	k1gFnPc2	narozeniny
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
koncertě	koncert	k1gInSc6	koncert
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
nahrát	nahrát	k5eAaPmF	nahrát
album	album	k1gNnSc4	album
jeho	jeho	k3xOp3gFnPc2	jeho
dosud	dosud	k6eAd1	dosud
nevydaných	vydaný	k2eNgFnPc2d1	nevydaná
skladeb	skladba	k1gFnPc2	skladba
(	(	kIx(	(
<g/>
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
jako	jako	k9	jako
Líně	líně	k6eAd1	líně
s	s	k7c7	s
tebou	ty	k3xPp2nSc7	ty
spím	spát	k5eAaImIp1nS	spát
<g/>
/	/	kIx~	/
<g/>
Lazy	Lazy	k?	Lazy
love	lov	k1gInSc5	lov
<g/>
)	)	kIx)	)
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
padlo	padnout	k5eAaPmAgNnS	padnout
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
hraní	hraní	k1gNnSc6	hraní
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
baskytary	baskytara	k1gFnSc2	baskytara
nahradila	nahradit	k5eAaPmAgFnS	nahradit
Hlavsu	Hlavsa	k1gMnSc4	Hlavsa
Eva	Eva	k1gFnSc1	Eva
Turnová	Turnová	k1gFnSc1	Turnová
<g/>
,	,	kIx,	,
za	za	k7c2	za
bicí	bicí	k2eAgFnSc2d1	bicí
zasedl	zasednout	k5eAaPmAgMnS	zasednout
bubeník	bubeník	k1gMnSc1	bubeník
Hudby	hudba	k1gFnSc2	hudba
Praha	Praha	k1gFnSc1	Praha
Ludvík	Ludvík	k1gMnSc1	Ludvík
Kandl	Kandl	k1gMnSc1	Kandl
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
jej	on	k3xPp3gMnSc4	on
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kvasnička	Kvasnička	k1gMnSc1	Kvasnička
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2001-2009	[number]	k4	2001-2009
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
kapely	kapela	k1gFnSc2	kapela
kontrabasista	kontrabasista	k1gMnSc1	kontrabasista
Ivan	Ivan	k1gMnSc1	Ivan
Bierhanzl	Bierhanzl	k1gMnSc1	Bierhanzl
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
krátce	krátce	k6eAd1	krátce
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
už	už	k6eAd1	už
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
nahrála	nahrát	k5eAaBmAgFnS	nahrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
první	první	k4xOgNnSc4	první
řádné	řádný	k2eAgNnSc4d1	řádné
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
Plastiků	plastik	k1gInPc2	plastik
Líně	líně	k6eAd1	líně
s	s	k7c7	s
tebou	ty	k3xPp2nSc7	ty
spím	spát	k5eAaImIp1nS	spát
<g/>
,	,	kIx,	,
věnované	věnovaný	k2eAgFnSc3d1	věnovaná
památce	památka	k1gFnSc3	památka
Milana	Milan	k1gMnSc2	Milan
Hlavsy	Hlavsa	k1gMnSc2	Hlavsa
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
písní	píseň	k1gFnPc2	píseň
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
jsou	být	k5eAaImIp3nP	být
Hlavsou	Hlavsa	k1gMnSc7	Hlavsa
zhudebněné	zhudebněný	k2eAgFnSc2d1	zhudebněná
básně	báseň	k1gFnSc2	báseň
J.	J.	kA	J.
H.	H.	kA	H.
Krchovského	Krchovský	k2eAgInSc2d1	Krchovský
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
orchestrem	orchestr	k1gInSc7	orchestr
Agon	agon	k1gInSc1	agon
Orchestra	orchestra	k1gFnSc1	orchestra
znovu	znovu	k6eAd1	znovu
nahráli	nahrát	k5eAaPmAgMnP	nahrát
dvě	dva	k4xCgNnPc4	dva
konceptuální	konceptuální	k2eAgNnPc4d1	konceptuální
alba	album	k1gNnPc4	album
z	z	k7c2	z
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
Pašijové	pašijový	k2eAgFnSc2d1	pašijová
hry	hra	k1gFnSc2	hra
velikonoční	velikonoční	k2eAgFnPc1d1	velikonoční
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
1978	[number]	k4	1978
<g/>
))	))	k?	))
a	a	k8xC	a
Obešel	obejít	k5eAaPmAgMnS	obejít
já	já	k3xPp1nSc1	já
polí	pole	k1gNnPc2	pole
pět	pět	k4xCc4	pět
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
2010	[number]	k4	2010
<g/>
;	;	kIx,	;
původně	původně	k6eAd1	původně
1979	[number]	k4	1979
jako	jako	k8xS	jako
Jak	jak	k6eAd1	jak
bude	být	k5eAaImBp3nS	být
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1997	[number]	k4	1997
<g/>
-	-	kIx~	-
<g/>
2006	[number]	k4	2006
postupně	postupně	k6eAd1	postupně
vyšly	vyjít	k5eAaPmAgFnP	vyjít
remasterované	remasterovaný	k2eAgFnPc1d1	remasterovaná
staré	starý	k2eAgFnPc1d1	stará
nahrávky	nahrávka	k1gFnPc1	nahrávka
skupiny	skupina	k1gFnSc2	skupina
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nemohla	moct	k5eNaImAgFnS	moct
svobodně	svobodně	k6eAd1	svobodně
natáčet	natáčet	k5eAaImF	natáčet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
zrušil	zrušit	k5eAaPmAgInS	zrušit
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
rozsudek	rozsudek	k1gInSc4	rozsudek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
vydala	vydat	k5eAaPmAgFnS	vydat
kapela	kapela	k1gFnSc1	kapela
nové	nový	k2eAgFnSc2d1	nová
studiové	studiový	k2eAgFnSc2d1	studiová
album	album	k1gNnSc4	album
Maska	maska	k1gFnSc1	maska
za	za	k7c7	za
maskou	maska	k1gFnSc7	maska
<g/>
.	.	kIx.	.
</s>
<s>
Autory	autor	k1gMnPc7	autor
hudby	hudba	k1gFnSc2	hudba
byli	být	k5eAaImAgMnP	být
především	především	k9	především
Josef	Josef	k1gMnSc1	Josef
(	(	kIx(	(
<g/>
Joe	Joe	k1gMnSc1	Joe
<g/>
)	)	kIx)	)
Karafiát	Karafiát	k1gMnSc1	Karafiát
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
Turnová	Turnová	k1gFnSc1	Turnová
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
Janíček	Janíček	k1gMnSc1	Janíček
<g/>
,	,	kIx,	,
většinu	většina	k1gFnSc4	většina
textů	text	k1gInPc2	text
napsal	napsat	k5eAaBmAgMnS	napsat
Vratislav	Vratislav	k1gMnSc1	Vratislav
Brabenec	Brabenec	k1gMnSc1	Brabenec
a	a	k8xC	a
J.	J.	kA	J.
H.	H.	kA	H.
Krchovský	Krchovský	k2eAgInSc4d1	Krchovský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
zemřel	zemřít	k5eAaPmAgMnS	zemřít
bývalý	bývalý	k2eAgMnSc1d1	bývalý
manažer	manažer	k1gMnSc1	manažer
skupiny	skupina	k1gFnSc2	skupina
Ivan	Ivan	k1gMnSc1	Ivan
Martin	Martin	k1gMnSc1	Martin
Jirous	Jirous	k1gMnSc1	Jirous
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
kapela	kapela	k1gFnSc1	kapela
každoročně	každoročně	k6eAd1	každoročně
odehrála	odehrát	k5eAaPmAgFnS	odehrát
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
koncertů	koncert	k1gInPc2	koncert
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
i	i	k8xC	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
přesunuto	přesunout	k5eAaPmNgNnS	přesunout
na	na	k7c4	na
Novou	nový	k2eAgFnSc4d1	nová
scénu	scéna	k1gFnSc4	scéna
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
)	)	kIx)	)
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
před	před	k7c7	před
a	a	k8xC	a
po	po	k7c6	po
představení	představení	k1gNnSc6	představení
hry	hra	k1gFnSc2	hra
Rock	rock	k1gInSc1	rock
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
roll	roll	k1gInSc1	roll
od	od	k7c2	od
Toma	Tom	k1gMnSc2	Tom
Stopparda	Stoppard	k1gMnSc2	Stoppard
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
o	o	k7c6	o
osudech	osud	k1gInPc6	osud
disentu	disent	k1gInSc2	disent
a	a	k8xC	a
kulturního	kulturní	k2eAgInSc2d1	kulturní
undergroundu	underground	k1gInSc2	underground
v	v	k7c6	v
totalitním	totalitní	k2eAgNnSc6d1	totalitní
Československu	Československo	k1gNnSc6	Československo
a	a	k8xC	a
kapela	kapela	k1gFnSc1	kapela
The	The	k1gFnSc2	The
Plastic	Plastice	k1gFnPc2	Plastice
People	People	k1gFnSc2	People
of	of	k?	of
the	the	k?	the
Universe	Universe	k1gFnSc1	Universe
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
několikrát	několikrát	k6eAd1	několikrát
zmiňována	zmiňovat	k5eAaImNgFnS	zmiňovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
byly	být	k5eAaImAgInP	být
realizovány	realizován	k2eAgInPc1d1	realizován
tři	tři	k4xCgInPc1	tři
koncerty	koncert	k1gInPc1	koncert
v	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
The	The	k1gFnSc2	The
Plastic	Plastice	k1gFnPc2	Plastice
People	People	k1gFnSc2	People
of	of	k?	of
the	the	k?	the
Universe	Universe	k1gFnSc2	Universe
a	a	k8xC	a
Filharmonie	filharmonie	k1gFnSc2	filharmonie
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
tak	tak	k6eAd1	tak
realizovali	realizovat	k5eAaBmAgMnP	realizovat
"	"	kIx"	"
<g/>
plastikovské	plastikovský	k2eAgNnSc4d1	plastikovský
<g/>
"	"	kIx"	"
dílo	dílo	k1gNnSc4	dílo
Co	co	k9	co
znamená	znamenat	k5eAaImIp3nS	znamenat
vésti	vést	k5eAaImF	vést
koně	kůň	k1gMnSc4	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
původně	původně	k6eAd1	původně
jednorázový	jednorázový	k2eAgInSc1d1	jednorázový
symfonický	symfonický	k2eAgInSc1d1	symfonický
projekt	projekt	k1gInSc1	projekt
byl	být	k5eAaImAgInS	být
iniciován	iniciovat	k5eAaBmNgInS	iniciovat
dramaturgem	dramaturg	k1gMnSc7	dramaturg
Filharmonie	filharmonie	k1gFnPc1	filharmonie
Brno	Brno	k1gNnSc1	Brno
Vítězslavem	Vítězslav	k1gMnSc7	Vítězslav
Mikešem	Mikeš	k1gMnSc7	Mikeš
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
oslovil	oslovit	k5eAaPmAgMnS	oslovit
k	k	k7c3	k
sepsání	sepsání	k1gNnSc3	sepsání
symfonických	symfonický	k2eAgFnPc2d1	symfonická
aranží	aranže	k1gFnPc2	aranže
hudebníka	hudebník	k1gMnSc2	hudebník
<g/>
,	,	kIx,	,
skladatele	skladatel	k1gMnSc2	skladatel
a	a	k8xC	a
aranžéra	aranžér	k1gMnSc2	aranžér
Michala	Michal	k1gMnSc2	Michal
Nejtka	Nejtek	k1gMnSc2	Nejtek
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
navíc	navíc	k6eAd1	navíc
k	k	k7c3	k
původnímu	původní	k2eAgNnSc3d1	původní
dílu	dílo	k1gNnSc3	dílo
připsal	připsat	k5eAaPmAgInS	připsat
zhruba	zhruba	k6eAd1	zhruba
20	[number]	k4	20
minut	minuta	k1gFnPc2	minuta
své	svůj	k3xOyFgFnSc2	svůj
původní	původní	k2eAgFnSc2d1	původní
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Koncerty	koncert	k1gInPc1	koncert
se	se	k3xPyFc4	se
uskutečnily	uskutečnit	k5eAaPmAgInP	uskutečnit
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2013	[number]	k4	2013
v	v	k7c6	v
brněnské	brněnský	k2eAgFnSc6d1	brněnská
Richard	Richard	k1gMnSc1	Richard
Adam	Adam	k1gMnSc1	Adam
Gallery	Galler	k1gInPc7	Galler
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
a	a	k8xC	a
poté	poté	k6eAd1	poté
ještě	ještě	k9	ještě
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
Divadle	divadlo	k1gNnSc6	divadlo
Archa	archa	k1gFnSc1	archa
<g/>
.	.	kIx.	.
</s>
<s>
Koncert	koncert	k1gInSc1	koncert
byl	být	k5eAaImAgInS	být
realizován	realizovat	k5eAaBmNgInS	realizovat
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
sestavě	sestava	k1gFnSc6	sestava
<g/>
:	:	kIx,	:
Vratislav	Vratislav	k1gMnSc1	Vratislav
Brabenec	Brabenec	k1gMnSc1	Brabenec
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Janíček	Janíček	k1gMnSc1	Janíček
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Kabeš	Kabeš	k1gMnSc1	Kabeš
<g/>
,	,	kIx,	,
Joe	Joe	k1gMnSc1	Joe
Karafiát	Karafiát	k1gMnSc1	Karafiát
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
Turnová	Turnová	k1gFnSc1	Turnová
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kvasnička	Kvasnička	k1gMnSc1	Kvasnička
s	s	k7c7	s
hostujícím	hostující	k2eAgMnSc7d1	hostující
Michalem	Michal	k1gMnSc7	Michal
Nejtkem	Nejtek	k1gMnSc7	Nejtek
<g/>
.	.	kIx.	.
</s>
<s>
Dirigentem	dirigent	k1gMnSc7	dirigent
byl	být	k5eAaImAgMnS	být
Marko	Marko	k1gMnSc1	Marko
Ivanović	Ivanović	k1gMnSc1	Ivanović
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2015	[number]	k4	2015
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
opětovnému	opětovný	k2eAgNnSc3d1	opětovné
spojení	spojení	k1gNnSc3	spojení
s	s	k7c7	s
Filharmonií	filharmonie	k1gFnSc7	filharmonie
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
brněnském	brněnský	k2eAgNnSc6d1	brněnské
SONO	SONO	kA	SONO
centru	centrum	k1gNnSc6	centrum
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
koncert	koncert	k1gInSc1	koncert
Co	co	k3yQnSc4	co
znamená	znamenat	k5eAaImIp3nS	znamenat
vésti	vést	k5eAaImF	vést
koně	kůň	k1gMnSc4	kůň
<g/>
,	,	kIx,	,
k	k	k7c3	k
vyvrcholení	vyvrcholení	k1gNnSc3	vyvrcholení
oslav	oslava	k1gFnPc2	oslava
státního	státní	k2eAgInSc2d1	státní
svátku	svátek	k1gInSc2	svátek
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
komponovaný	komponovaný	k2eAgInSc4d1	komponovaný
večer	večer	k1gInSc4	večer
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
přenášen	přenášet	k5eAaImNgInS	přenášet
Českou	český	k2eAgFnSc7d1	Česká
televizí	televize	k1gFnSc7	televize
a	a	k8xC	a
vysílán	vysílat	k5eAaImNgInS	vysílat
přímým	přímý	k2eAgInSc7d1	přímý
přenosem	přenos	k1gInSc7	přenos
na	na	k7c6	na
kanále	kanál	k1gInSc6	kanál
ČT	ČT	kA	ČT
Art	Art	k1gFnPc2	Art
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úvodu	úvod	k1gInSc6	úvod
večera	večer	k1gInSc2	večer
byl	být	k5eAaImAgInS	být
promítnut	promítnut	k2eAgInSc1d1	promítnut
dokument	dokument	k1gInSc1	dokument
Co	co	k9	co
znamenalo	znamenat	k5eAaImAgNnS	znamenat
vésti	vést	k5eAaImF	vést
koně	kůň	k1gMnPc4	kůň
<g/>
,	,	kIx,	,
natočený	natočený	k2eAgInSc4d1	natočený
speciálně	speciálně	k6eAd1	speciálně
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
příležitost	příležitost	k1gFnSc4	příležitost
<g/>
,	,	kIx,	,
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
vernisáž	vernisáž	k1gFnSc4	vernisáž
fotografií	fotografia	k1gFnPc2	fotografia
z	z	k7c2	z
Kerhartic	Kerhartice	k1gFnPc2	Kerhartice
(	(	kIx(	(
<g/>
místo	místo	k7c2	místo
konání	konání	k1gNnSc2	konání
prvního	první	k4xOgMnSc2	první
a	a	k8xC	a
jediného	jediný	k2eAgInSc2d1	jediný
koncertu	koncert	k1gInSc2	koncert
Co	co	k3yInSc1	co
znamená	znamenat	k5eAaImIp3nS	znamenat
vésti	vést	k5eAaImF	vést
koně	kůň	k1gMnSc4	kůň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
a	a	k8xC	a
poté	poté	k6eAd1	poté
zhlédli	zhlédnout	k5eAaPmAgMnP	zhlédnout
diváci	divák	k1gMnPc1	divák
v	v	k7c6	v
sále	sál	k1gInSc6	sál
i	i	k8xC	i
v	v	k7c6	v
ČT	ČT	kA	ČT
samotné	samotný	k2eAgFnSc6d1	samotná
symfonické	symfonický	k2eAgFnSc6d1	symfonická
provedení	provedení	k1gNnSc6	provedení
Co	co	k3yRnSc1	co
znamená	znamenat	k5eAaImIp3nS	znamenat
vésti	vést	k5eAaImF	vést
koně	kůň	k1gMnSc4	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Koncert	koncert	k1gInSc1	koncert
byl	být	k5eAaImAgInS	být
realizován	realizovat	k5eAaBmNgInS	realizovat
v	v	k7c6	v
sestavě	sestava	k1gFnSc6	sestava
<g/>
:	:	kIx,	:
Vratislav	Vratislav	k1gMnSc1	Vratislav
Brabenec	Brabenec	k1gMnSc1	Brabenec
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Janíček	Janíček	k1gMnSc1	Janíček
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
Turnová	Turnová	k1gFnSc1	Turnová
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kvasnička	Kvasnička	k1gMnSc1	Kvasnička
s	s	k7c7	s
hosty	host	k1gMnPc7	host
Michalem	Michal	k1gMnSc7	Michal
Nejtkem	Nejtek	k1gMnSc7	Nejtek
(	(	kIx(	(
<g/>
recitace	recitace	k1gFnSc1	recitace
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
teremin	teremin	k1gInSc1	teremin
<g/>
)	)	kIx)	)
Josefem	Josef	k1gMnSc7	Josef
Klíčem	klíč	k1gInSc7	klíč
(	(	kIx(	(
<g/>
violoncello	violoncello	k1gNnSc1	violoncello
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dirigentem	dirigent	k1gMnSc7	dirigent
byl	být	k5eAaImAgMnS	být
Pavel	Pavel	k1gMnSc1	Pavel
Šnajdr	Šnajdr	k1gMnSc1	Šnajdr
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Kabeš	Kabeš	k1gMnSc1	Kabeš
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
filharmonického	filharmonický	k2eAgInSc2d1	filharmonický
projektu	projekt	k1gInSc2	projekt
neúčastnit	účastnit	k5eNaImF	účastnit
a	a	k8xC	a
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
jeho	jeho	k3xOp3gFnSc7	jeho
realizací	realizace	k1gFnSc7	realizace
jej	on	k3xPp3gMnSc4	on
následoval	následovat	k5eAaImAgInS	následovat
i	i	k9	i
Joe	Joe	k1gMnSc1	Joe
Karafiát	Karafiát	k1gMnSc1	Karafiát
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
měl	mít	k5eAaImAgInS	mít
pokračování	pokračování	k1gNnPc4	pokračování
i	i	k8xC	i
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Plastic	Plastice	k1gFnPc2	Plastice
People	People	k1gFnSc1	People
of	of	k?	of
the	the	k?	the
Universe	Universe	k1gFnSc2	Universe
byli	být	k5eAaImAgMnP	být
pozváni	pozvat	k5eAaPmNgMnP	pozvat
Českým	český	k2eAgInSc7d1	český
centrem	centr	k1gInSc7	centr
v	v	k7c6	v
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
k	k	k7c3	k
provedení	provedení	k1gNnSc3	provedení
symfonického	symfonický	k2eAgInSc2d1	symfonický
koncertu	koncert	k1gInSc2	koncert
Co	co	k3yQnSc1	co
znamená	znamenat	k5eAaImIp3nS	znamenat
vésti	vést	k5eAaImF	vést
koně	kůň	k1gMnSc4	kůň
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
vyvrcholení	vyvrcholení	k1gNnSc2	vyvrcholení
oslav	oslava	k1gFnPc2	oslava
roku	rok	k1gInSc2	rok
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Koncert	koncert	k1gInSc1	koncert
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
ve	v	k7c6	v
funkcionalistické	funkcionalistický	k2eAgFnSc6d1	funkcionalistická
historické	historický	k2eAgFnSc6d1	historická
budově	budova	k1gFnSc6	budova
Starého	Starého	k2eAgInSc2d1	Starého
Cyrku	Cyrk	k1gInSc2	Cyrk
v	v	k7c6	v
ukrajinském	ukrajinský	k2eAgInSc6d1	ukrajinský
Charkově	Charkov	k1gInSc6	Charkov
dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
,	,	kIx,	,
<g/>
října	říjen	k1gInSc2	říjen
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Plastiky	plastika	k1gFnPc4	plastika
doprovodil	doprovodit	k5eAaPmAgInS	doprovodit
charkovský	charkovský	k2eAgInSc1d1	charkovský
Molodoznyj	Molodoznyj	k1gInSc1	Molodoznyj
akademiceskij	akademiceskít	k5eAaPmRp2nS	akademiceskít
simfoniceskij	simfoniceskít	k5eAaPmRp2nS	simfoniceskít
orkestr	orkestr	k1gInSc1	orkestr
"	"	kIx"	"
<g/>
Slobožanskij	Slobožanskij	k1gFnSc1	Slobožanskij
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Koncert	koncert	k1gInSc1	koncert
byl	být	k5eAaImAgInS	být
realizován	realizovat	k5eAaBmNgInS	realizovat
v	v	k7c6	v
sestavě	sestava	k1gFnSc6	sestava
<g/>
:	:	kIx,	:
Vratislav	Vratislav	k1gMnSc1	Vratislav
Brabenec	Brabenec	k1gMnSc1	Brabenec
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Janíček	Janíček	k1gMnSc1	Janíček
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kvasnička	Kvasnička	k1gMnSc1	Kvasnička
<g/>
,	,	kIx,	,
Johnny	Johnn	k1gInPc1	Johnn
Judl	Judl	k1gInSc1	Judl
se	s	k7c7	s
stálým	stálý	k2eAgMnSc7d1	stálý
hostem	host	k1gMnSc7	host
Josefem	Josef	k1gMnSc7	Josef
Klíčem	klíč	k1gInSc7	klíč
(	(	kIx(	(
<g/>
violoncello	violoncello	k1gNnSc1	violoncello
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dirigentem	dirigent	k1gMnSc7	dirigent
byl	být	k5eAaImAgMnS	být
opět	opět	k6eAd1	opět
Pavel	Pavel	k1gMnSc1	Pavel
Šnajdr	Šnajdr	k1gMnSc1	Šnajdr
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
kapela	kapela	k1gFnSc1	kapela
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
téměř	téměř	k6eAd1	téměř
40	[number]	k4	40
hudebníků	hudebník	k1gMnPc2	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
strávila	strávit	k5eAaPmAgFnS	strávit
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
pouze	pouze	k6eAd1	pouze
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Klasická	klasický	k2eAgFnSc1d1	klasická
sestava	sestava	k1gFnSc1	sestava
Milan	Milan	k1gMnSc1	Milan
Hlavsa	Hlavsa	k1gMnSc1	Hlavsa
alias	alias	k9	alias
Mejla	Mejla	k1gMnSc1	Mejla
(	(	kIx(	(
<g/>
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
)	)	kIx)	)
Vratislav	Vratislav	k1gMnSc1	Vratislav
Brabenec	Brabenec	k1gMnSc1	Brabenec
(	(	kIx(	(
<g/>
saxofon	saxofon	k1gInSc1	saxofon
<g/>
)	)	kIx)	)
Josef	Josef	k1gMnSc1	Josef
Janíček	Janíček	k1gMnSc1	Janíček
(	(	kIx(	(
<g/>
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
)	)	kIx)	)
Jiří	Jiří	k1gMnSc1	Jiří
Kabeš	Kabeš	k1gMnSc1	Kabeš
(	(	kIx(	(
<g/>
viola	viola	k1gFnSc1	viola
<g/>
)	)	kIx)	)
Pavel	Pavel	k1gMnSc1	Pavel
Zeman	Zeman	k1gMnSc1	Zeman
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Šula	Šula	k1gMnSc1	Šula
/	/	kIx~	/
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vožniak	Vožniak	k1gMnSc1	Vožniak
/	/	kIx~	/
Jan	Jan	k1gMnSc1	Jan
Brabec	Brabec	k1gMnSc1	Brabec
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgMnSc1d1	bicí
<g/>
)	)	kIx)	)
Ivan	Ivan	k1gMnSc1	Ivan
Martin	Martin	k1gMnSc1	Martin
<g />
.	.	kIx.	.
</s>
<s>
Jirous	Jirous	k1gMnSc1	Jirous
alias	alias	k9	alias
Magor	magor	k1gMnSc1	magor
(	(	kIx(	(
<g/>
manažer	manažer	k1gMnSc1	manažer
<g/>
)	)	kIx)	)
Původní	původní	k2eAgFnSc1d1	původní
sestava	sestava	k1gFnSc1	sestava
Milan	Milan	k1gMnSc1	Milan
"	"	kIx"	"
<g/>
Mejla	Mejla	k1gMnSc1	Mejla
<g/>
"	"	kIx"	"
Hlavsa	Hlavsa	k1gMnSc1	Hlavsa
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
-	-	kIx~	-
<g/>
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
-	-	kIx~	-
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Michal	Michal	k1gMnSc1	Michal
Jernek	Jernek	k1gMnSc1	Jernek
-	-	kIx~	-
saxofon	saxofon	k1gInSc1	saxofon
<g/>
,	,	kIx,	,
klarinet	klarinet	k1gInSc1	klarinet
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1968	[number]	k4	1968
<g/>
-	-	kIx~	-
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
Jiří	Jiří	k1gMnSc1	Jiří
Števich	Števich	k1gMnSc1	Števich
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
-	-	kIx~	-
<g/>
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
Josef	Josef	k1gMnSc1	Josef
Brabec	Brabec	k1gMnSc1	Brabec
-	-	kIx~	-
bicí	bicí	k2eAgMnSc1d1	bicí
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
-	-	kIx~	-
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
Současná	současný	k2eAgFnSc1d1	současná
sestava	sestava	k1gFnSc1	sestava
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
Vratislav	Vratislav	k1gMnSc1	Vratislav
Brabenec	Brabenec	k1gMnSc1	Brabenec
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
saxofon	saxofon	k1gInSc1	saxofon
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
<g/>
)	)	kIx)	)
Josef	Josef	k1gMnSc1	Josef
Janíček	Janíček	k1gMnSc1	Janíček
(	(	kIx(	(
<g/>
klávesy	klávesa	k1gFnPc1	klávesa
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
<g/>
)	)	kIx)	)
Johnny	Johnna	k1gFnSc2	Johnna
Jůdl	Jůdl	k1gMnSc1	Jůdl
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
)	)	kIx)	)
Jarolav	Jarolav	k1gMnSc1	Jarolav
Kvasnička	Kvasnička	k1gMnSc1	Kvasnička
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgMnSc1d1	bicí
<g/>
)	)	kIx)	)
David	David	k1gMnSc1	David
Babka	Babka	k1gMnSc1	Babka
(	(	kIx(	(
<g/>
kytary	kytara	k1gFnPc1	kytara
<g/>
)	)	kIx)	)
Další	další	k2eAgFnPc1d1	další
dřívější	dřívější	k2eAgFnPc1d1	dřívější
členové	člen	k1gMnPc1	člen
Eva	Eva	k1gFnSc1	Eva
Turnová	Turnová	k1gFnSc1	Turnová
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
do	do	k7c2	do
června	červen	k1gInSc2	červen
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
Jen	jen	k9	jen
u	u	k7c2	u
nahrávek	nahrávka	k1gFnPc2	nahrávka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
vydány	vydán	k2eAgInPc1d1	vydán
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
</s>
