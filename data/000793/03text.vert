<s>
University	universita	k1gFnPc1	universita
of	of	k?	of
Notre	Notr	k1gMnSc5	Notr
Dame	Damus	k1gMnSc5	Damus
du	du	k?	du
Lac	Lac	k1gMnSc6	Lac
je	být	k5eAaImIp3nS	být
prestižní	prestižní	k2eAgFnSc1d1	prestižní
katolická	katolický	k2eAgFnSc1d1	katolická
univerzita	univerzita	k1gFnSc1	univerzita
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Indiana	Indiana	k1gFnSc1	Indiana
u	u	k7c2	u
South	Southa	k1gFnPc2	Southa
Bendu	Benda	k1gMnSc4	Benda
<g/>
.	.	kIx.	.
</s>
<s>
Založena	založen	k2eAgFnSc1d1	založena
byla	být	k5eAaImAgFnS	být
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1842	[number]	k4	1842
sedmi	sedm	k4xCc2	sedm
bratry	bratr	k1gMnPc7	bratr
z	z	k7c2	z
Kongregace	kongregace	k1gFnSc2	kongregace
Svatého	svatý	k2eAgInSc2d1	svatý
kříže	kříž	k1gInSc2	kříž
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
čele	čelo	k1gNnSc6	čelo
stál	stát	k5eAaImAgMnS	stát
Edward	Edward	k1gMnSc1	Edward
Sorin	Sorin	k1gMnSc1	Sorin
<g/>
.	.	kIx.	.
</s>
<s>
Notre	Notr	k1gMnSc5	Notr
Dame	Damus	k1gMnSc5	Damus
du	du	k?	du
Lac	Lac	k1gFnPc6	Lac
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
přeložitelné	přeložitelný	k2eAgFnPc1d1	přeložitelná
jako	jako	k8xC	jako
Naše	náš	k3xOp1gFnSc1	náš
Paní	paní	k1gFnSc1	paní
na	na	k7c6	na
jezeře	jezero	k1gNnSc6	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Naše	náš	k3xOp1gFnSc1	náš
Paní	paní	k1gFnSc1	paní
je	být	k5eAaImIp3nS	být
jednoznačně	jednoznačně	k6eAd1	jednoznačně
srozumitelný	srozumitelný	k2eAgInSc1d1	srozumitelný
odkaz	odkaz	k1gInSc1	odkaz
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
titulů	titul	k1gInPc2	titul
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
na	na	k7c6	na
jezeře	jezero	k1gNnSc6	jezero
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
inspirováno	inspirován	k2eAgNnSc1d1	inspirováno
některým	některý	k3yIgNnPc3	některý
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
jezer	jezero	k1gNnPc2	jezero
v	v	k7c6	v
kampusu	kampus	k1gInSc6	kampus
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
zabírá	zabírat	k5eAaImIp3nS	zabírat
1	[number]	k4	1
250	[number]	k4	250
akrů	akr	k1gInPc2	akr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2009	[number]	k4	2009
vedení	vedení	k1gNnSc4	vedení
univerzity	univerzita	k1gFnSc2	univerzita
rozpoutalo	rozpoutat	k5eAaPmAgNnS	rozpoutat
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
skandál	skandál	k1gInSc1	skandál
<g/>
,	,	kIx,	,
když	když	k8xS	když
pozvalo	pozvat	k5eAaPmAgNnS	pozvat
jako	jako	k8xS	jako
slavnostního	slavnostní	k2eAgMnSc4d1	slavnostní
řečníka	řečník	k1gMnSc4	řečník
na	na	k7c4	na
slavnostní	slavnostní	k2eAgNnSc4d1	slavnostní
vyřazování	vyřazování	k1gNnSc4	vyřazování
absolventů	absolvent	k1gMnPc2	absolvent
Baracka	Baracka	k1gFnSc1	Baracka
Obamu	Obam	k1gInSc2	Obam
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
oznámilo	oznámit	k5eAaPmAgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
univerzita	univerzita	k1gFnSc1	univerzita
udělí	udělit	k5eAaPmIp3nS	udělit
čestný	čestný	k2eAgInSc4d1	čestný
doktorát	doktorát	k1gInSc4	doktorát
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
silné	silný	k2eAgNnSc1d1	silné
pobouření	pobouření	k1gNnSc1	pobouření
mezi	mezi	k7c7	mezi
katolíky	katolík	k1gMnPc7	katolík
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
extrémně	extrémně	k6eAd1	extrémně
propotratovým	propotratův	k2eAgInPc3d1	propotratův
postojům	postoj	k1gInPc3	postoj
a	a	k8xC	a
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
prezidenta	prezident	k1gMnSc2	prezident
Obamy	Obama	k1gFnSc2	Obama
<g/>
,	,	kIx,	,
protestní	protestní	k2eAgFnSc2d1	protestní
petice	petice	k1gFnSc2	petice
proti	proti	k7c3	proti
jeho	jeho	k3xOp3gNnSc3	jeho
pozvání	pozvánět	k5eAaImIp3nP	pozvánět
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
organizuje	organizovat	k5eAaBmIp3nS	organizovat
Cardinal	Cardinal	k1gMnSc1	Cardinal
Newman	Newman	k1gMnSc1	Newman
Society	societa	k1gFnSc2	societa
<g/>
,	,	kIx,	,
deklarovala	deklarovat	k5eAaBmAgFnS	deklarovat
k	k	k7c3	k
8	[number]	k4	8
<g/>
.	.	kIx.	.
květnu	květen	k1gInSc6	květen
2009	[number]	k4	2009
více	hodně	k6eAd2	hodně
než	než	k8xS	než
354	[number]	k4	354
tisíc	tisíc	k4xCgInPc2	tisíc
podpisů	podpis	k1gInPc2	podpis
(	(	kIx(	(
<g/>
zatímco	zatímco	k8xS	zatímco
její	její	k3xOp3gFnSc3	její
protipetice	protipetika	k1gFnSc3	protipetika
necelých	celý	k2eNgFnPc2d1	necelá
35	[number]	k4	35
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
otevřený	otevřený	k2eAgInSc4d1	otevřený
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
s	s	k7c7	s
pozváním	pozvání	k1gNnSc7	pozvání
a	a	k8xC	a
udělením	udělení	k1gNnSc7	udělení
doktorátu	doktorát	k1gInSc2	doktorát
vyjádřilo	vyjádřit	k5eAaPmAgNnS	vyjádřit
k	k	k7c3	k
7	[number]	k4	7
<g/>
.	.	kIx.	.
květnu	květen	k1gInSc3	květen
68	[number]	k4	68
amerických	americký	k2eAgMnPc2d1	americký
biskupů	biskup	k1gMnPc2	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Kardinál	kardinál	k1gMnSc1	kardinál
George	Georg	k1gInSc2	Georg
<g/>
,	,	kIx,	,
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Chicaga	Chicago	k1gNnSc2	Chicago
<g/>
,	,	kIx,	,
výslovně	výslovně	k6eAd1	výslovně
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc4	tento
pozvání	pozvání	k1gNnSc4	pozvání
je	být	k5eAaImIp3nS	být
ostudné	ostudný	k2eAgNnSc1d1	ostudné
a	a	k8xC	a
že	že	k8xS	že
University	universita	k1gFnPc1	universita
of	of	k?	of
Notre	Notr	k1gInSc5	Notr
Dame	Dame	k1gFnSc1	Dame
zjevně	zjevně	k6eAd1	zjevně
nechápe	chápat	k5eNaImIp3nS	chápat
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
to	ten	k3xDgNnSc4	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
být	být	k5eAaImF	být
katolickou	katolický	k2eAgFnSc7d1	katolická
univerzitou	univerzita	k1gFnSc7	univerzita
<g/>
,	,	kIx,	,
a	a	k8xC	a
John	John	k1gMnSc1	John
Michael	Michael	k1gMnSc1	Michael
D	D	kA	D
<g/>
'	'	kIx"	'
<g/>
Arcy	Arca	k1gFnSc2	Arca
<g/>
,	,	kIx,	,
biskup	biskup	k1gMnSc1	biskup
diecéze	diecéze	k1gFnSc2	diecéze
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
univerzita	univerzita	k1gFnSc1	univerzita
leží	ležet	k5eAaImIp3nS	ležet
<g/>
,	,	kIx,	,
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
slavnostního	slavnostní	k2eAgNnSc2d1	slavnostní
vyřazování	vyřazování	k1gNnSc2	vyřazování
absolventů	absolvent	k1gMnPc2	absolvent
tento	tento	k3xDgInSc1	tento
rok	rok	k1gInSc4	rok
nezúčastní	zúčastnit	k5eNaPmIp3nS	zúčastnit
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
biskupů	biskup	k1gMnPc2	biskup
upozornila	upozornit	k5eAaPmAgFnS	upozornit
<g/>
,	,	kIx,	,
že	že	k8xS	že
univerzita	univerzita	k1gFnSc1	univerzita
svým	svůj	k3xOyFgNnSc7	svůj
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
porušuje	porušovat	k5eAaImIp3nS	porušovat
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
biskupské	biskupský	k2eAgFnSc2d1	biskupská
konference	konference	k1gFnSc2	konference
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
katolickým	katolický	k2eAgFnPc3d1	katolická
institucím	instituce	k1gFnPc3	instituce
poctít	poctít	k5eAaPmF	poctít
osoby	osoba	k1gFnPc4	osoba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
oponují	oponovat	k5eAaImIp3nP	oponovat
zásadním	zásadní	k2eAgInSc7d1	zásadní
morálním	morální	k2eAgInSc7d1	morální
stanoviskům	stanovisko	k1gNnPc3	stanovisko
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnPc1	církev
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
dubna	duben	k1gInSc2	duben
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
Obamovu	Obamův	k2eAgNnSc3d1	Obamovo
pozvání	pozvání	k1gNnSc3	pozvání
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
Laetare	Laetar	k1gMnSc5	Laetar
Medal	Medal	k1gInSc4	Medal
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
odvolala	odvolat	k5eAaPmAgFnS	odvolat
svůj	svůj	k3xOyFgInSc4	svůj
slavnostní	slavnostní	k2eAgInSc4d1	slavnostní
projev	projev	k1gInSc4	projev
na	na	k7c4	na
University	universita	k1gFnPc4	universita
of	of	k?	of
Notre	Notr	k1gMnSc5	Notr
Dame	Damus	k1gMnSc5	Damus
profesorka	profesorka	k1gFnSc1	profesorka
práva	právo	k1gNnPc4	právo
na	na	k7c6	na
Harvadu	Harvad	k1gInSc6	Harvad
a	a	k8xC	a
bývalá	bývalý	k2eAgFnSc1d1	bývalá
velvyslankyně	velvyslankyně	k1gFnSc1	velvyslankyně
USA	USA	kA	USA
při	při	k7c6	při
Svatém	svatý	k2eAgInSc6d1	svatý
stolci	stolec	k1gInSc6	stolec
<g/>
,	,	kIx,	,
Mary	Mary	k1gFnSc1	Mary
Ann	Ann	k1gMnSc1	Ann
Glendon	Glendon	k1gMnSc1	Glendon
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgInSc1	první
případ	případ	k1gInSc1	případ
odmítnutí	odmítnutí	k1gNnSc4	odmítnutí
Laetare	Laetar	k1gMnSc5	Laetar
Medal	Medal	k1gInSc1	Medal
univerzity	univerzita	k1gFnSc2	univerzita
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1883	[number]	k4	1883
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
její	její	k3xOp3gNnSc1	její
udělování	udělování	k1gNnSc1	udělování
zavedeno	zavést	k5eAaPmNgNnS	zavést
<g/>
.	.	kIx.	.
</s>
<s>
Sportovní	sportovní	k2eAgInPc1d1	sportovní
týmy	tým	k1gInPc1	tým
Notre	Notr	k1gInSc5	Notr
Dame	Dame	k1gNnPc1	Dame
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
Fighting	Fighting	k1gInSc4	Fighting
Irish	Irisha	k1gFnPc2	Irisha
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
členem	člen	k1gMnSc7	člen
Národní	národní	k2eAgFnSc2d1	národní
vysokoškolské	vysokoškolský	k2eAgFnSc2d1	vysokoškolská
atletické	atletický	k2eAgFnSc2d1	atletická
asociace	asociace	k1gFnSc2	asociace
(	(	kIx(	(
<g/>
NCAA	NCAA	kA	NCAA
<g/>
)	)	kIx)	)
divize	divize	k1gFnSc1	divize
I.	I.	kA	I.
To	to	k9	to
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
úroveň	úroveň	k1gFnSc4	úroveň
univerzitních	univerzitní	k2eAgMnPc2d1	univerzitní
programů	program	k1gInPc2	program
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
nabízí	nabízet	k5eAaImIp3nS	nabízet
Americké	americký	k2eAgNnSc1d1	americké
školství	školství	k1gNnSc1	školství
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
kompetentní	kompetentní	k2eAgFnPc1d1	kompetentní
konference	konference	k1gFnPc1	konference
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
se	se	k3xPyFc4	se
sportovní	sportovní	k2eAgInSc1d1	sportovní
program	program	k1gInSc1	program
Fighting	Fighting	k1gInSc1	Fighting
Irish	Irish	k1gInSc4	Irish
nachází	nacházet	k5eAaImIp3nS	nacházet
je	být	k5eAaImIp3nS	být
Atlantic	Atlantice	k1gFnPc2	Atlantice
Coast	Coast	k1gMnSc1	Coast
Conference	Conference	k1gFnSc2	Conference
(	(	kIx(	(
<g/>
ACC	ACC	kA	ACC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
se	se	k3xPyFc4	se
sportovní	sportovní	k2eAgInSc1d1	sportovní
program	program	k1gInSc1	program
Fighting	Fighting	k1gInSc1	Fighting
Irish	Irisha	k1gFnPc2	Irisha
řadil	řadit	k5eAaImAgInS	řadit
do	do	k7c2	do
Horizon	Horizona	k1gFnPc2	Horizona
League	Leagu	k1gFnSc2	Leagu
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
-	-	kIx~	-
<g/>
83	[number]	k4	83
až	až	k9	až
1985	[number]	k4	1985
<g/>
-	-	kIx~	-
<g/>
86	[number]	k4	86
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jednoletou	jednoletý	k2eAgFnSc7d1	jednoletá
přestávkou	přestávka	k1gFnSc7	přestávka
se	se	k3xPyFc4	se
do	do	k7c2	do
Horizon	Horizona	k1gFnPc2	Horizona
League	League	k1gFnPc2	League
opět	opět	k6eAd1	opět
vrátil	vrátit	k5eAaPmAgMnS	vrátit
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
-	-	kIx~	-
<g/>
88	[number]	k4	88
až	až	k9	až
1994	[number]	k4	1994
<g/>
-	-	kIx~	-
<g/>
95	[number]	k4	95
<g/>
)	)	kIx)	)
a	a	k8xC	a
poté	poté	k6eAd1	poté
následovalo	následovat	k5eAaImAgNnS	následovat
zařazení	zařazení	k1gNnSc1	zařazení
do	do	k7c2	do
Big	Big	k1gMnSc2	Big
East	East	k2eAgInSc1d1	East
Conference	Conferenec	k1gInSc2	Conferenec
do	do	k7c2	do
konce	konec	k1gInSc2	konec
sezony	sezona	k1gFnSc2	sezona
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
Mužskou	mužský	k2eAgFnSc4d1	mužská
část	část	k1gFnSc4	část
sportovního	sportovní	k2eAgInSc2d1	sportovní
programu	program	k1gInSc2	program
nabízí	nabízet	k5eAaImIp3nS	nabízet
univerzita	univerzita	k1gFnSc1	univerzita
uplatnění	uplatnění	k1gNnSc2	uplatnění
v	v	k7c6	v
baseballu	baseball	k1gInSc6	baseball
<g/>
,	,	kIx,	,
basketbalu	basketbal	k1gInSc6	basketbal
<g/>
,	,	kIx,	,
veslování	veslování	k1gNnSc6	veslování
<g/>
,	,	kIx,	,
běhu	běh	k1gInSc6	běh
<g/>
,	,	kIx,	,
šermu	šerm	k1gInSc6	šerm
<g/>
,	,	kIx,	,
americkému	americký	k2eAgInSc3d1	americký
fotbalu	fotbal	k1gInSc3	fotbal
<g/>
,	,	kIx,	,
golfu	golf	k1gInSc3	golf
<g/>
,	,	kIx,	,
lednímu	lední	k2eAgInSc3d1	lední
hokeji	hokej	k1gInSc3	hokej
<g/>
,	,	kIx,	,
lakrosu	lakros	k1gInSc3	lakros
<g/>
,	,	kIx,	,
fotbalu	fotbal	k1gInSc3	fotbal
<g/>
,	,	kIx,	,
plavání	plavání	k1gNnSc2	plavání
a	a	k8xC	a
potápění	potápění	k1gNnSc2	potápění
<g/>
,	,	kIx,	,
tenisu	tenis	k1gInSc2	tenis
a	a	k8xC	a
atletiku	atletika	k1gFnSc4	atletika
<g/>
.	.	kIx.	.
</s>
<s>
Ženská	ženský	k2eAgFnSc1d1	ženská
část	část	k1gFnSc1	část
programu	program	k1gInSc2	program
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
basketbal	basketbal	k1gInSc1	basketbal
<g/>
,	,	kIx,	,
běh	běh	k1gInSc1	běh
<g/>
,	,	kIx,	,
šerm	šerm	k1gInSc1	šerm
<g/>
,	,	kIx,	,
golf	golf	k1gInSc1	golf
<g/>
,	,	kIx,	,
lakros	lakros	k1gInSc1	lakros
<g/>
,	,	kIx,	,
veslování	veslování	k1gNnSc1	veslování
<g/>
,	,	kIx,	,
fotbal	fotbal	k1gInSc1	fotbal
<g/>
,	,	kIx,	,
softball	softball	k1gInSc1	softball
<g/>
,	,	kIx,	,
plavání	plavání	k1gNnSc4	plavání
a	a	k8xC	a
potápění	potápění	k1gNnSc4	potápění
<g/>
,	,	kIx,	,
tenis	tenis	k1gInSc4	tenis
<g/>
,	,	kIx,	,
volejbal	volejbal	k1gInSc4	volejbal
a	a	k8xC	a
atletiku	atletika	k1gFnSc4	atletika
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
amerického	americký	k2eAgInSc2d1	americký
fotbalu	fotbal	k1gInSc2	fotbal
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
samovolný	samovolný	k2eAgMnSc1d1	samovolný
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jako	jako	k8xC	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mála	málo	k1gNnSc2	málo
Indenpendent	Indenpendent	k1gMnSc1	Indenpendent
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
má	mít	k5eAaImIp3nS	mít
podepsanou	podepsaný	k2eAgFnSc4d1	podepsaná
dohodu	dohoda	k1gFnSc4	dohoda
s	s	k7c7	s
konferencí	konference	k1gFnSc7	konference
ACC	ACC	kA	ACC
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
se	se	k3xPyFc4	se
v	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
utkat	utkat	k5eAaPmF	utkat
s	s	k7c7	s
jejich	jejich	k3xOp3gFnPc7	jejich
pěti	pět	k4xCc7	pět
členy	člen	k1gInPc7	člen
<g/>
.	.	kIx.	.
</s>
<s>
Šermířská	šermířský	k2eAgFnSc1d1	šermířská
část	část	k1gFnSc1	část
programu	program	k1gInSc2	program
soutěží	soutěž	k1gFnPc2	soutěž
v	v	k7c4	v
Midwest	Midwest	k1gFnSc4	Midwest
Fencing	Fencing	k1gInSc1	Fencing
Conference	Conference	k1gFnPc1	Conference
a	a	k8xC	a
hokejisté	hokejista	k1gMnPc1	hokejista
v	v	k7c4	v
Hockey	Hockea	k1gFnPc4	Hockea
East	Easta	k1gFnPc2	Easta
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
fotbal	fotbal	k1gInSc1	fotbal
v	v	k7c6	v
Notre	Notr	k1gInSc5	Notr
Dame	Dame	k1gFnSc1	Dame
má	mít	k5eAaImIp3nS	mít
dlouholetou	dlouholetý	k2eAgFnSc4d1	dlouholetá
tradici	tradice	k1gFnSc4	tradice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k6eAd1	až
do	do	k7c2	do
roka	rok	k1gInSc2	rok
1887	[number]	k4	1887
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
první	první	k4xOgNnPc1	první
utkání	utkání	k1gNnSc1	utkání
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Notre	Notr	k1gInSc5	Notr
Dame	Dame	k1gNnSc6	Dame
nastoupilo	nastoupit	k5eAaPmAgNnS	nastoupit
proti	proti	k7c3	proti
Michiganu	Michigan	k1gInSc3	Michigan
Wolverines	Wolverinesa	k1gFnPc2	Wolverinesa
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
utkání	utkání	k1gNnSc4	utkání
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
týmy	tým	k1gInPc7	tým
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejsledovanější	sledovaný	k2eAgFnSc1d3	nejsledovanější
událostí	událost	k1gFnSc7	událost
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
úplně	úplně	k6eAd1	úplně
největším	veliký	k2eAgMnSc7d3	veliký
rivalem	rival	k1gMnSc7	rival
Fighting	Fighting	k1gInSc1	Fighting
Irish	Irish	k1gInSc4	Irish
je	být	k5eAaImIp3nS	být
škola	škola	k1gFnSc1	škola
z	z	k7c2	z
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
-	-	kIx~	-
USC	USC	kA	USC
<g/>
.	.	kIx.	.
</s>
<s>
Fighting	Fighting	k1gInSc1	Fighting
Irish	Irisha	k1gFnPc2	Irisha
doposud	doposud	k6eAd1	doposud
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
13	[number]	k4	13
mistrovských	mistrovský	k2eAgInPc2d1	mistrovský
titulů	titul	k1gInPc2	titul
(	(	kIx(	(
<g/>
ačkoliv	ačkoliv	k8xS	ačkoliv
univerzity	univerzita	k1gFnSc2	univerzita
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
pouze	pouze	k6eAd1	pouze
11	[number]	k4	11
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
Notre	Notr	k1gInSc5	Notr
Dame	Dam	k1gFnPc1	Dam
má	mít	k5eAaImIp3nS	mít
nejvíce	hodně	k6eAd3	hodně
členů	člen	k1gMnPc2	člen
v	v	k7c6	v
College	College	k1gFnSc6	College
Football	Football	k1gMnSc1	Football
Hall	Hall	k1gMnSc1	Hall
of	of	k?	of
Fame	Fame	k1gInSc1	Fame
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Ohio	Ohio	k1gNnSc1	Ohio
State	status	k1gInSc5	status
University	universita	k1gFnPc1	universita
soupeří	soupeřit	k5eAaImIp3nP	soupeřit
o	o	k7c4	o
vítěze	vítěz	k1gMnPc4	vítěz
v	v	k7c6	v
Heisman	Heisman	k1gMnSc1	Heisman
Trophy	Tropha	k1gMnSc2	Tropha
(	(	kIx(	(
<g/>
trofej	trofej	k1gFnSc4	trofej
pro	pro	k7c4	pro
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
hráče	hráč	k1gMnSc4	hráč
amerického	americký	k2eAgInSc2d1	americký
fotbalu	fotbal	k1gInSc2	fotbal
na	na	k7c6	na
univerzitách	univerzita	k1gFnPc6	univerzita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
zmínit	zmínit	k5eAaPmF	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Notre	Notr	k1gInSc5	Notr
Dame	Dame	k1gFnSc1	Dame
Figting	Figting	k1gInSc4	Figting
Irish	Irish	k1gInSc1	Irish
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgNnSc4d3	veliký
procento	procento	k1gNnSc4	procento
vítězství	vítězství	k1gNnSc2	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
hrají	hrát	k5eAaImIp3nP	hrát
svá	svůj	k3xOyFgNnPc4	svůj
domácí	domácí	k2eAgNnPc4d1	domácí
utkání	utkání	k1gNnPc4	utkání
na	na	k7c6	na
legendárním	legendární	k2eAgInSc6d1	legendární
Notre	Notr	k1gInSc5	Notr
Dame	Dame	k1gNnPc1	Dame
Stadium	stadium	k1gNnSc1	stadium
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stánek	stánek	k1gInSc1	stánek
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
místě	místo	k1gNnSc6	místo
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2016-2019	[number]	k4	2016-2019
by	by	k9	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
kompletně	kompletně	k6eAd1	kompletně
zmodernizován	zmodernizován	k2eAgMnSc1d1	zmodernizován
<g/>
.	.	kIx.	.
</s>
<s>
Rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
stadionu	stadion	k1gInSc2	stadion
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
stát	stát	k5eAaPmF	stát
kolem	kolem	k7c2	kolem
$	$	kIx~	$
<g/>
400	[number]	k4	400
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
kapacita	kapacita	k1gFnSc1	kapacita
je	být	k5eAaImIp3nS	být
80,795	[number]	k4	80,795
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
pár	pár	k4xCyI	pár
tisíc	tisíc	k4xCgInPc2	tisíc
zvětšena	zvětšen	k2eAgNnPc4d1	zvětšeno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
vyčíslen	vyčíslen	k2eAgInSc1d1	vyčíslen
roční	roční	k2eAgInSc1d1	roční
zisk	zisk	k1gInSc1	zisk
týmu	tým	k1gInSc2	tým
na	na	k7c4	na
$	$	kIx~	$
<g/>
22	[number]	k4	22
milionů	milion	k4xCgInPc2	milion
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k6eAd1	především
díky	dík	k1gInPc4	dík
jeho	jeho	k3xOp3gInSc2	jeho
vlastního	vlastní	k2eAgInSc2d1	vlastní
fotbalového	fotbalový	k2eAgInSc2d1	fotbalový
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Forbes	forbes	k1gInSc4	forbes
jmenoval	jmenovat	k5eAaBmAgInS	jmenovat
tým	tým	k1gInSc1	tým
jako	jako	k8xS	jako
nejcennější	cenný	k2eAgNnSc1d3	nejcennější
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
,	,	kIx,	,
když	když	k8xS	když
vyčíslil	vyčíslit	k5eAaPmAgMnS	vyčíslit
hodnotu	hodnota	k1gFnSc4	hodnota
týmu	tým	k1gInSc2	tým
na	na	k7c4	na
$	$	kIx~	$
<g/>
101	[number]	k4	101
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
významných	významný	k2eAgMnPc2d1	významný
absolventů	absolvent	k1gMnPc2	absolvent
University	universita	k1gFnSc2	universita
of	of	k?	of
Notre	Notr	k1gMnSc5	Notr
Dame	Damus	k1gMnSc5	Damus
<g/>
.	.	kIx.	.
</s>
<s>
Bruce	Bruce	k1gMnSc1	Bruce
Babbitt	Babbitt	k1gMnSc1	Babbitt
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
guvernér	guvernér	k1gMnSc1	guvernér
Arizony	Arizona	k1gFnSc2	Arizona
John	John	k1gMnSc1	John
Bellairs	Bellairs	k1gInSc1	Bellairs
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
William	William	k1gInSc1	William
Borders	Borders	k1gInSc1	Borders
<g/>
,	,	kIx,	,
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
baltimorský	baltimorský	k2eAgMnSc1d1	baltimorský
Abraham	Abraham	k1gMnSc1	Abraham
González	González	k1gMnSc1	González
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
guvernér	guvernér	k1gMnSc1	guvernér
státu	stát	k1gInSc2	stát
Chihuahua	Chihuahua	k1gMnSc1	Chihuahua
Alexander	Alexandra	k1gFnPc2	Alexandra
Haig	Haig	k1gMnSc1	Haig
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
USA	USA	kA	USA
a	a	k8xC	a
šéf	šéf	k1gMnSc1	šéf
štábu	štáb	k1gInSc2	štáb
Bílého	bílý	k2eAgInSc2d1	bílý
domu	dům	k1gInSc2	dům
James	James	k1gMnSc1	James
Patrick	Patrick	k1gMnSc1	Patrick
Kelly	Kella	k1gFnSc2	Kella
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
William	William	k1gInSc1	William
E.	E.	kA	E.
<g />
.	.	kIx.	.
</s>
<s>
Miller	Miller	k1gMnSc1	Miller
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
kandidát	kandidát	k1gMnSc1	kandidát
na	na	k7c6	na
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
USA	USA	kA	USA
Julius	Julius	k1gMnSc1	Julius
Nieuwland	Nieuwland	k1gInSc1	Nieuwland
<g/>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
a	a	k8xC	a
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
Edwin	Edwin	k1gMnSc1	Edwin
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Connor	Connor	k1gMnSc1	Connor
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
novinář	novinář	k1gMnSc1	novinář
<g/>
,	,	kIx,	,
držitel	držitel	k1gMnSc1	držitel
Pulitzerovy	Pulitzerův	k2eAgFnSc2d1	Pulitzerova
ceny	cena	k1gFnSc2	cena
Condoleezza	Condoleezza	k1gFnSc1	Condoleezza
Rice	Rice	k1gFnSc1	Rice
<g/>
,	,	kIx,	,
politička	politička	k1gFnSc1	politička
<g/>
,	,	kIx,	,
ministryně	ministryně	k1gFnSc1	ministryně
zahraničí	zahraničí	k1gNnSc2	zahraničí
USA	USA	kA	USA
Nicholas	Nicholas	k1gMnSc1	Nicholas
Sparks	Sparksa	k1gFnPc2	Sparksa
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejúspěšnějších	úspěšný	k2eAgMnPc2d3	nejúspěšnější
spisovatelů	spisovatel	k1gMnPc2	spisovatel
současnosti	současnost	k1gFnSc2	současnost
Jim	on	k3xPp3gMnPc3	on
Wetherbee	Wetherbee	k1gFnSc1	Wetherbee
<g/>
,	,	kIx,	,
kosmonaut	kosmonaut	k1gMnSc1	kosmonaut
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
University	universita	k1gFnSc2	universita
of	of	k?	of
Notre	Notr	k1gMnSc5	Notr
Dame	Damus	k1gMnSc5	Damus
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
školy	škola	k1gFnSc2	škola
</s>
