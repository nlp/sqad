<s>
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Dzurinda	Dzurind	k1gMnSc2	Dzurind
(	(	kIx(	(
<g/>
*	*	kIx~	*
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1955	[number]	k4	1955
Spišský	spišský	k2eAgInSc1d1	spišský
Štvrtok	Štvrtok	k1gInSc1	Štvrtok
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
slovenský	slovenský	k2eAgMnSc1d1	slovenský
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
SDKÚ-DS	SDKÚ-DS	k1gFnSc2	SDKÚ-DS
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
Slovenska	Slovensko	k1gNnSc2	Slovensko
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
a	a	k8xC	a
jako	jako	k9	jako
ministr	ministr	k1gMnSc1	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
–	–	k?	–
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Dzurinda	Dzurind	k1gMnSc4	Dzurind
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
Vysokou	vysoká	k1gFnSc4	vysoká
školu	škola	k1gFnSc4	škola
dopravy	doprava	k1gFnSc2	doprava
a	a	k8xC	a
spojů	spoj	k1gInPc2	spoj
v	v	k7c6	v
Žilině	Žilina	k1gFnSc6	Žilina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
získal	získat	k5eAaPmAgInS	získat
titul	titul	k1gInSc1	titul
kandidáta	kandidát	k1gMnSc4	kandidát
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgInS	pracovat
ve	v	k7c6	v
Výzkumném	výzkumný	k2eAgInSc6d1	výzkumný
ústavu	ústav	k1gInSc6	ústav
dopravním	dopravní	k2eAgInSc6d1	dopravní
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
na	na	k7c6	na
oblastním	oblastní	k2eAgNnSc6d1	oblastní
ředitelství	ředitelství	k1gNnSc6	ředitelství
Československých	československý	k2eAgFnPc2d1	Československá
státních	státní	k2eAgFnPc2d1	státní
drah	draha	k1gFnPc2	draha
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
politiky	politika	k1gFnSc2	politika
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
jako	jako	k8xC	jako
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
zakládajících	zakládající	k2eAgMnPc2d1	zakládající
členů	člen	k1gMnPc2	člen
KDH	KDH	kA	KDH
(	(	kIx(	(
<g/>
Kresťanskodemokratické	Kresťanskodemokratický	k2eAgFnSc2d1	Kresťanskodemokratický
hnutie	hnutie	k1gFnSc2	hnutie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
náměstkem	náměstek	k1gMnSc7	náměstek
ministra	ministr	k1gMnSc2	ministr
dopravy	doprava	k1gFnSc2	doprava
a	a	k8xC	a
pošt	pošta	k1gFnPc2	pošta
SR	SR	kA	SR
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
poslancem	poslanec	k1gMnSc7	poslanec
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
byl	být	k5eAaImAgInS	být
slovenským	slovenský	k2eAgMnSc7d1	slovenský
ministrem	ministr	k1gMnSc7	ministr
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
pošt	pošta	k1gFnPc2	pošta
a	a	k8xC	a
veřejných	veřejný	k2eAgFnPc2d1	veřejná
prací	práce	k1gFnPc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
opozičním	opoziční	k2eAgMnSc7d1	opoziční
poslancem	poslanec	k1gMnSc7	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
se	se	k3xPyFc4	se
KDH	KDH	kA	KDH
stalo	stát	k5eAaPmAgNnS	stát
členem	člen	k1gInSc7	člen
opozičního	opoziční	k2eAgNnSc2d1	opoziční
koaličního	koaliční	k2eAgNnSc2d1	koaliční
sdružení	sdružení	k1gNnSc2	sdružení
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
koalice	koalice	k1gFnSc1	koalice
<g/>
,	,	kIx,	,
Dzurinda	Dzurinda	k1gFnSc1	Dzurinda
se	s	k7c7	s
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1998	[number]	k4	1998
stal	stát	k5eAaPmAgMnS	stát
jejím	její	k3xOp3gMnSc7	její
předsedou	předseda	k1gMnSc7	předseda
a	a	k8xC	a
po	po	k7c6	po
vítězných	vítězný	k2eAgFnPc6d1	vítězná
volbách	volba	k1gFnPc6	volba
také	také	k9	také
poprvé	poprvé	k6eAd1	poprvé
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
SR	SR	kA	SR
(	(	kIx(	(
<g/>
od	od	k7c2	od
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2000	[number]	k4	2000
pak	pak	k6eAd1	pak
založil	založit	k5eAaPmAgMnS	založit
novou	nový	k2eAgFnSc4d1	nová
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
SDKÚ	SDKÚ	kA	SDKÚ
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
také	také	k6eAd1	také
stal	stát	k5eAaPmAgMnS	stát
předsedou	předseda	k1gMnSc7	předseda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
sestavil	sestavit	k5eAaPmAgInS	sestavit
s	s	k7c7	s
koaličními	koaliční	k2eAgMnPc7d1	koaliční
partnery	partner	k1gMnPc7	partner
KDH	KDH	kA	KDH
<g/>
,	,	kIx,	,
SMK	SMK	kA	SMK
a	a	k8xC	a
ANO	ano	k9	ano
novou	nový	k2eAgFnSc4d1	nová
pravicovou	pravicový	k2eAgFnSc4d1	pravicová
vládu	vláda	k1gFnSc4	vláda
SR	SR	kA	SR
<g/>
.	.	kIx.	.
</s>
<s>
Premiérem	premiér	k1gMnSc7	premiér
SR	SR	kA	SR
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gMnSc4	on
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
Robert	Robert	k1gMnSc1	Robert
Fico	Fico	k1gMnSc1	Fico
<g/>
.	.	kIx.	.
</s>
<s>
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Dzurinda	Dzurinda	k1gFnSc1	Dzurinda
často	často	k6eAd1	často
sportuje	sportovat	k5eAaImIp3nS	sportovat
<g/>
,	,	kIx,	,
pravidelně	pravidelně	k6eAd1	pravidelně
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
maratónských	maratónský	k2eAgInPc2d1	maratónský
běhů	běh	k1gInPc2	běh
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
dcery	dcera	k1gFnPc4	dcera
<g/>
.	.	kIx.	.
1991	[number]	k4	1991
–	–	k?	–
náměstek	náměstek	k1gMnSc1	náměstek
ministra	ministr	k1gMnSc2	ministr
dopravy	doprava	k1gFnSc2	doprava
a	a	k8xC	a
pošt	pošta	k1gFnPc2	pošta
1992	[number]	k4	1992
–	–	k?	–
poslanec	poslanec	k1gMnSc1	poslanec
za	za	k7c4	za
KDH	KDH	kA	KDH
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
rozpočet	rozpočet	k1gInSc4	rozpočet
a	a	k8xC	a
finance	finance	k1gFnSc1	finance
březen	březen	k1gInSc1	březen
1994	[number]	k4	1994
–	–	k?	–
říjen	říjen	k1gInSc1	říjen
1994	[number]	k4	1994
-	-	kIx~	-
ministr	ministr	k1gMnSc1	ministr
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
pošt	pošta	k1gFnPc2	pošta
a	a	k8xC	a
veřejných	veřejný	k2eAgFnPc2d1	veřejná
prací	práce	k1gFnPc2	práce
1994	[number]	k4	1994
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
–	–	k?	–
poslanec	poslanec	k1gMnSc1	poslanec
<g />
.	.	kIx.	.
</s>
<s>
za	za	k7c4	za
KDH	KDH	kA	KDH
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
–	–	k?	–
poslanec	poslanec	k1gMnSc1	poslanec
za	za	k7c4	za
SDK	SDK	kA	SDK
(	(	kIx(	(
<g/>
mandát	mandát	k1gInSc1	mandát
neuplatnil	uplatnit	k5eNaPmAgInS	uplatnit
<g/>
)	)	kIx)	)
2002	[number]	k4	2002
<g/>
–	–	k?	–
<g/>
2006	[number]	k4	2006
–	–	k?	–
poslanec	poslanec	k1gMnSc1	poslanec
za	za	k7c4	za
SDKÚ	SDKÚ	kA	SDKÚ
(	(	kIx(	(
<g/>
mandát	mandát	k1gInSc1	mandát
neuplatnil	uplatnit	k5eNaPmAgInS	uplatnit
<g/>
)	)	kIx)	)
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
2006	[number]	k4	2006
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
2006	[number]	k4	2006
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
–	–	k?	–
poslanec	poslanec	k1gMnSc1	poslanec
za	za	k7c4	za
SDKÚ-DS	SDKÚ-DS	k1gFnSc4	SDKÚ-DS
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
výboru	výbor	k1gInSc2	výbor
NR	NR	kA	NR
SR	SR	kA	SR
2010	[number]	k4	2010
<g/>
–	–	k?	–
<g/>
2012	[number]	k4	2012
–	–	k?	–
ministr	ministr	k1gMnSc1	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
Ivety	Iveta	k1gFnSc2	Iveta
Radičové	radič	k1gMnPc1	radič
</s>
