<s>
Jaký	jaký	k3yRgInSc4	jaký
zákon	zákon	k1gInSc4	zákon
<g/>
,	,	kIx,	,
popři	popřít	k5eAaPmRp2nS	popřít
komutativním	komutativní	k2eAgMnPc3d1	komutativní
a	a	k8xC	a
asociativním	asociativní	k2eAgMnPc3d1	asociativní
<g/>
,	,	kIx,	,
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
absolutně	absolutně	k6eAd1	absolutně
konvergentní	konvergentní	k2eAgFnPc4d1	konvergentní
řady	řada	k1gFnPc4	řada
<g/>
?	?	kIx.	?
</s>
