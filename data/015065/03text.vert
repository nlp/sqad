<s>
Hiroši	Hiroš	k1gMnPc1
Saeki	Saek	k1gFnSc2
</s>
<s>
Hiroši	Hiroš	k1gMnPc1
SaekiOsobní	SaekiOsobní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Celé	celý	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Hiroši	Hiroš	k1gMnPc1
Saeki	Saek	k1gFnSc2
Datum	datum	k1gNnSc4
narození	narození	k1gNnSc2
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1936	#num#	k4
(	(	kIx(
<g/>
84	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Prefektura	prefektura	k1gFnSc1
Hirošima	Hirošima	k1gFnSc1
<g/>
,	,	kIx,
Japonsko	Japonsko	k1gNnSc1
Datum	datum	k1gNnSc1
úmrtí	úmrtí	k1gNnPc2
</s>
<s>
2010	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
73	#num#	k4
<g/>
–	–	k?
<g/>
74	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Klubové	klubový	k2eAgFnPc4d1
informace	informace	k1gFnPc4
</s>
<s>
Konec	konec	k1gInSc1
hráčské	hráčský	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
Pozice	pozice	k1gFnSc2
</s>
<s>
útočník	útočník	k1gMnSc1
Profesionální	profesionální	k2eAgFnSc2d1
kluby	klub	k1gInPc1
</s>
<s>
Roky	rok	k1gInPc1
</s>
<s>
Klub	klub	k1gInSc1
</s>
<s>
</s>
<s desamb="1">
<g/>
–	–	k?
<g/>
1966	#num#	k4
Yawata	Yawat	k1gMnSc2
Steel	Steel	k1gInSc4
</s>
<s>
Reprezentace	reprezentace	k1gFnSc1
<g/>
**	**	k?
</s>
<s>
Roky	rok	k1gInPc1
</s>
<s>
Reprezentace	reprezentace	k1gFnSc1
</s>
<s>
Záp	Záp	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
góly	gól	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
1958	#num#	k4
<g/>
–	–	k?
<g/>
1961	#num#	k4
Japonsko	Japonsko	k1gNnSc4
<g/>
4	#num#	k4
(	(	kIx(
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
</s>
<s>
→	→	k?
Šipka	šipka	k1gFnSc1
znamená	znamenat	k5eAaImIp3nS
hostování	hostování	k1gNnSc4
hráče	hráč	k1gMnSc2
v	v	k7c6
daném	daný	k2eAgInSc6d1
klubu	klub	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hiroši	Hiroši	k1gMnSc1
Saeki	Saeki	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
26	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
1936	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bývalý	bývalý	k2eAgMnSc1d1
japonský	japonský	k2eAgMnSc1d1
fotbalista	fotbalista	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Klubová	klubový	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
Hrával	hrávat	k5eAaImAgMnS
za	za	k7c4
Yawata	Yawat	k1gMnSc4
Steel	Steel	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Reprezentační	reprezentační	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
Hiroši	Hiroš	k1gMnPc7
Saeki	Saek	k1gFnSc2
odehrál	odehrát	k5eAaPmAgInS
za	za	k7c4
japonský	japonský	k2eAgInSc4d1
národní	národní	k2eAgInSc4d1
tým	tým	k1gInSc4
v	v	k7c6
letech	let	k1gInPc6
1958	#num#	k4
<g/>
–	–	k?
<g/>
1961	#num#	k4
celkem	celkem	k6eAd1
4	#num#	k4
reprezentačních	reprezentační	k2eAgNnPc2d1
utkání	utkání	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Statistiky	statistika	k1gFnPc1
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
RokyZáp	RokyZáp	k1gInSc1
<g/>
.	.	kIx.
<g/>
Góly	gól	k1gInPc1
</s>
<s>
195810	#num#	k4
</s>
<s>
195910	#num#	k4
</s>
<s>
196000	#num#	k4
</s>
<s>
196120	#num#	k4
</s>
<s>
Celkem	celkem	k6eAd1
<g/>
40	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
National	Nationat	k5eAaPmAgInS,k5eAaImAgInS
Football	Football	k1gMnSc1
Teams	Teamsa	k1gFnPc2
</s>
<s>
Japan	japan	k1gInSc1
National	National	k1gFnSc2
Football	Footballa	k1gFnPc2
Team	team	k1gInSc1
Database	Databasa	k1gFnSc3
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
</s>
