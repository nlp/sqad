<s>
Nogajština	Nogajština	k1gFnSc1	Nogajština
(	(	kIx(	(
<g/>
nogajsky	nogajsky	k6eAd1	nogajsky
<g/>
:	:	kIx,	:
Н	Н	k?	Н
т	т	k?	т
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
západoturkický	západoturkický	k2eAgInSc1d1	západoturkický
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
Kavkazu	Kavkaz	k1gInSc6	Kavkaz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
v	v	k7c6	v
blízkých	blízký	k2eAgInPc6d1	blízký
státech	stát	k1gInPc6	stát
jako	jako	k8xS	jako
<g/>
:	:	kIx,	:
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
a	a	k8xC	a
Uzbekistán	Uzbekistán	k1gInSc1	Uzbekistán
<g/>
.	.	kIx.	.
</s>
