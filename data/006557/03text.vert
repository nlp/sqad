<s>
DNA	DNA	kA	DNA
polymeráza	polymeráza	k1gFnSc1	polymeráza
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnPc4	označení
pro	pro	k7c4	pro
takový	takový	k3xDgInSc4	takový
enzym	enzym	k1gInSc4	enzym
účastnící	účastnící	k2eAgFnSc1d1	účastnící
se	se	k3xPyFc4	se
replikace	replikace	k1gFnSc1	replikace
DNA	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
katalyzuje	katalyzovat	k5eAaImIp3nS	katalyzovat
polymeraci	polymerace	k1gFnSc4	polymerace
řetězce	řetězec	k1gInSc2	řetězec
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
replikační	replikační	k2eAgFnSc6d1	replikační
vidlici	vidlice	k1gFnSc6	vidlice
vkládá	vkládat	k5eAaImIp3nS	vkládat
deoxyribonukleotidy	deoxyribonukleotid	k1gMnPc4	deoxyribonukleotid
(	(	kIx(	(
<g/>
typ	typ	k1gInSc1	typ
nukleotidů	nukleotid	k1gInPc2	nukleotid
obsahující	obsahující	k2eAgFnSc4d1	obsahující
deoxyribózu	deoxyribóza	k1gFnSc4	deoxyribóza
<g/>
)	)	kIx)	)
a	a	k8xC	a
prodlužuje	prodlužovat	k5eAaImIp3nS	prodlužovat
tak	tak	k9	tak
řetězec	řetězec	k1gInSc1	řetězec
<g/>
.	.	kIx.	.
</s>
<s>
DNA	DNA	kA	DNA
polymeráza	polymeráza	k1gFnSc1	polymeráza
umí	umět	k5eAaImIp3nS	umět
přidávat	přidávat	k5eAaImF	přidávat
nové	nový	k2eAgInPc4d1	nový
nukleotidy	nukleotid	k1gInPc4	nukleotid
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
3	[number]	k4	3
<g/>
'	'	kIx"	'
konec	konec	k1gInSc4	konec
nově	nově	k6eAd1	nově
vznikajícího	vznikající	k2eAgInSc2d1	vznikající
řetězce	řetězec	k1gInSc2	řetězec
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
řetězec	řetězec	k1gInSc1	řetězec
prodlužuje	prodlužovat	k5eAaImIp3nS	prodlužovat
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
5	[number]	k4	5
<g/>
'	'	kIx"	'
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
DNA	dna	k1gFnSc1	dna
polymeráza	polymeráza	k1gFnSc1	polymeráza
nikdy	nikdy	k6eAd1	nikdy
nesyntetizuje	syntetizovat	k5eNaImIp3nS	syntetizovat
nový	nový	k2eAgInSc1d1	nový
řetězec	řetězec	k1gInSc1	řetězec
DNA	DNA	kA	DNA
nanovo	nanovo	k6eAd1	nanovo
(	(	kIx(	(
<g/>
de	de	k?	de
novo	nova	k1gFnSc5	nova
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
zmíněný	zmíněný	k2eAgInSc1d1	zmíněný
3	[number]	k4	3
<g/>
'	'	kIx"	'
<g/>
-OH	-OH	k?	-OH
konec	konec	k1gInSc1	konec
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
DNA	dna	k1gFnSc1	dna
polymeráza	polymeráza	k1gFnSc1	polymeráza
musí	muset	k5eAaImIp3nS	muset
navázat	navázat	k5eAaPmF	navázat
na	na	k7c4	na
primer	primera	k1gFnPc2	primera
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tento	tento	k3xDgInSc1	tento
3	[number]	k4	3
<g/>
'	'	kIx"	'
konec	konec	k1gInSc1	konec
již	již	k6eAd1	již
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
(	(	kIx(	(
<g/>
tvorby	tvorba	k1gFnSc2	tvorba
primeru	primera	k1gFnSc4	primera
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
enzym	enzym	k1gInSc4	enzym
primáza	primáza	k1gFnSc1	primáza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
DNA	DNA	kA	DNA
polymerázy	polymeráza	k1gFnPc1	polymeráza
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
opravy	oprava	k1gFnPc1	oprava
chyb	chyba	k1gFnPc2	chyba
vzniklých	vzniklý	k2eAgFnPc2d1	vzniklá
při	při	k7c6	při
replikaci	replikace	k1gFnSc6	replikace
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
tyto	tento	k3xDgFnPc4	tento
polymerázy	polymeráza	k1gFnPc4	polymeráza
objeví	objevit	k5eAaPmIp3nS	objevit
chybný	chybný	k2eAgInSc1d1	chybný
pár	pár	k4xCyI	pár
bází	báze	k1gFnPc2	báze
<g/>
,	,	kIx,	,
vyjmou	vyjmout	k5eAaPmIp3nP	vyjmout
ho	on	k3xPp3gMnSc4	on
a	a	k8xC	a
nahradí	nahradit	k5eAaPmIp3nS	nahradit
správným	správný	k2eAgInSc7d1	správný
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
DNA	dno	k1gNnSc2	dno
polymeráz	polymeráza	k1gFnPc2	polymeráza
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
tzv.	tzv.	kA	tzv.
templát	templát	k1gInSc1	templát
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nějakou	nějaký	k3yIgFnSc4	nějaký
sekvenci	sekvence	k1gFnSc4	sekvence
nukleové	nukleový	k2eAgFnSc2d1	nukleová
kyseliny	kyselina	k1gFnSc2	kyselina
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jejíhož	jejíž	k3xOyRp3gInSc2	jejíž
vzoru	vzor	k1gInSc2	vzor
provádí	provádět	k5eAaImIp3nS	provádět
polymeraci	polymerace	k1gFnSc4	polymerace
(	(	kIx(	(
<g/>
řadí	řadit	k5eAaImIp3nP	řadit
nukleotidy	nukleotid	k1gInPc4	nukleotid
podle	podle	k7c2	podle
komplementarity	komplementarita	k1gFnSc2	komplementarita
bází	báze	k1gFnPc2	báze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
výjimkám	výjimka	k1gFnPc3	výjimka
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
terminální	terminální	k2eAgFnSc1d1	terminální
deoxynukleotidyltransferáza	deoxynukleotidyltransferáza	k1gFnSc1	deoxynukleotidyltransferáza
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
DNA-dependentní	DNAependentní	k2eAgNnPc4d1	DNA-dependentní
DNA	dno	k1gNnPc4	dno
polymerázy	polymeráza	k1gFnSc2	polymeráza
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
jako	jako	k9	jako
templát	templát	k1gInSc4	templát
využívají	využívat	k5eAaImIp3nP	využívat
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
a	a	k8xC	a
RNA-dependentní	RNAependentní	k2eAgFnSc1d1	RNA-dependentní
DNA	dna	k1gFnSc1	dna
polymerázy	polymeráza	k1gFnSc2	polymeráza
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
reverzní	reverzní	k2eAgFnPc4d1	reverzní
transkriptázy	transkriptáza	k1gFnPc4	transkriptáza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
využívají	využívat	k5eAaPmIp3nP	využívat
jako	jako	k9	jako
templát	templát	k1gInSc4	templát
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
bakterií	bakterie	k1gFnPc2	bakterie
známe	znát	k5eAaImIp1nP	znát
sice	sice	k8xC	sice
pět	pět	k4xCc4	pět
druhů	druh	k1gInPc2	druh
DNA	dno	k1gNnSc2	dno
polymeráz	polymeráza	k1gFnPc2	polymeráza
<g/>
,	,	kIx,	,
z	z	k7c2	z
E.	E.	kA	E.
coli	coli	k6eAd1	coli
však	však	k9	však
byly	být	k5eAaImAgFnP	být
izolovány	izolovat	k5eAaBmNgFnP	izolovat
jen	jen	k9	jen
Pol	pola	k1gFnPc2	pola
I	I	kA	I
<g/>
,	,	kIx,	,
II	II	kA	II
a	a	k8xC	a
III	III	kA	III
<g/>
:	:	kIx,	:
DNA	DNA	kA	DNA
polymeráza	polymeráza	k1gFnSc1	polymeráza
I	i	k9	i
-	-	kIx~	-
především	především	k6eAd1	především
opravuje	opravovat	k5eAaImIp3nS	opravovat
DNA	DNA	kA	DNA
DNA	DNA	kA	DNA
polymeráza	polymeráza	k1gFnSc1	polymeráza
II	II	kA	II
-	-	kIx~	-
především	především	k6eAd1	především
opravuje	opravovat	k5eAaImIp3nS	opravovat
DNA	DNA	kA	DNA
DNA	DNA	kA	DNA
polymeráza	polymeráza	k1gFnSc1	polymeráza
III	III	kA	III
-	-	kIx~	-
hlavní	hlavní	k2eAgFnSc1d1	hlavní
DNA	DNA	kA	DNA
polymeráza	polymeráza	k1gFnSc1	polymeráza
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
replikaci	replikace	k1gFnSc4	replikace
DNA	DNA	kA	DNA
DNA	DNA	kA	DNA
polymeráza	polymeráza	k1gFnSc1	polymeráza
IV	Iva	k1gFnPc2	Iva
(	(	kIx(	(
<g/>
méně	málo	k6eAd2	málo
významná	významný	k2eAgFnSc1d1	významná
<g/>
)	)	kIx)	)
DNA	dna	k1gFnSc1	dna
polymeráza	polymeráza	k1gFnSc1	polymeráza
V	v	k7c4	v
(	(	kIx(	(
<g/>
méně	málo	k6eAd2	málo
významná	významný	k2eAgFnSc1d1	významná
<g/>
)	)	kIx)	)
Příslušné	příslušný	k2eAgNnSc1d1	příslušné
DNA	dno	k1gNnPc1	dno
polymerázy	polymeráza	k1gFnSc2	polymeráza
jsou	být	k5eAaImIp3nP	být
podobné	podobný	k2eAgInPc1d1	podobný
spíše	spíše	k9	spíše
eukaryotním	eukaryotní	k2eAgInSc7d1	eukaryotní
DNA	DNA	kA	DNA
polymerázám	polymeráza	k1gFnPc3	polymeráza
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Eukaryotických	Eukaryotický	k2eAgInPc2d1	Eukaryotický
organizmů	organizmus	k1gInPc2	organizmus
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgFnPc3	jenž
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
nalezeno	naleznout	k5eAaPmNgNnS	naleznout
nejméně	málo	k6eAd3	málo
15	[number]	k4	15
různých	různý	k2eAgFnPc2d1	různá
DNA	dno	k1gNnPc4	dno
polymeráz	polymeráza	k1gFnPc2	polymeráza
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
replikaci	replikace	k1gFnSc6	replikace
chromozomů	chromozom	k1gInPc2	chromozom
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
opravě	oprava	k1gFnSc6	oprava
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
crossing-overu	crossingvera	k1gFnSc4	crossing-overa
chromozomových	chromozomový	k2eAgNnPc2d1	chromozomový
ramen	rameno	k1gNnPc2	rameno
a	a	k8xC	a
replikaci	replikace	k1gFnSc3	replikace
mitochondriální	mitochondriální	k2eAgFnSc4d1	mitochondriální
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
známé	známý	k1gMnPc4	známý
DNA	dna	k1gFnSc1	dna
polymerázy	polymeráza	k1gFnSc2	polymeráza
eukaryot	eukaryota	k1gFnPc2	eukaryota
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
DNA	dna	k1gFnSc1	dna
polymeráza	polymeráza	k1gFnSc1	polymeráza
α	α	k?	α
–	–	k?	–
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
primery	primera	k1gFnPc4	primera
DNA	DNA	kA	DNA
polymeráza	polymeráza	k1gFnSc1	polymeráza
δ	δ	k?	δ
–	–	k?	–
replikuje	replikovat	k5eAaImIp3nS	replikovat
DNA	DNA	kA	DNA
DNA	DNA	kA	DNA
polymeráza	polymeráza	k1gFnSc1	polymeráza
ε	ε	k?	ε
–	–	k?	–
replikuje	replikovat	k5eAaImIp3nS	replikovat
DNA	DNA	kA	DNA
V	v	k7c6	v
genetickém	genetický	k2eAgNnSc6d1	genetické
inženýrství	inženýrství	k1gNnSc6	inženýrství
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
využívají	využívat	k5eAaImIp3nP	využívat
zejména	zejména	k9	zejména
čtyři	čtyři	k4xCgInPc1	čtyři
druhy	druh	k1gInPc1	druh
DNA	dno	k1gNnSc2	dno
polymeráz	polymeráza	k1gFnPc2	polymeráza
<g/>
.	.	kIx.	.
</s>
<s>
DNA	DNA	kA	DNA
polymeráza	polymeráza	k1gFnSc1	polymeráza
I	i	k9	i
<g/>
,	,	kIx,	,
izolovaná	izolovaný	k2eAgFnSc1d1	izolovaná
obvykle	obvykle	k6eAd1	obvykle
z	z	k7c2	z
E.	E.	kA	E.
coli	col	k1gFnSc2	col
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
5	[number]	k4	5
<g/>
'	'	kIx"	'
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
'	'	kIx"	'
polymerizační	polymerizační	k2eAgFnSc4d1	polymerizační
a	a	k8xC	a
obousměrnou	obousměrný	k2eAgFnSc4d1	obousměrná
exonukleázovou	exonukleázový	k2eAgFnSc4d1	exonukleázový
aktivitu	aktivita	k1gFnSc4	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Klenowův	Klenowův	k2eAgInSc4d1	Klenowův
fragment	fragment	k1gInSc4	fragment
je	být	k5eAaImIp3nS	být
upravená	upravený	k2eAgFnSc1d1	upravená
DNA	dna	k1gFnSc1	dna
polymeráza	polymeráza	k1gFnSc1	polymeráza
I	i	k9	i
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
ztratila	ztratit	k5eAaPmAgFnS	ztratit
část	část	k1gFnSc1	část
svých	svůj	k3xOyFgFnPc2	svůj
exonukleázových	exonukleázův	k2eAgFnPc2d1	exonukleázův
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
hojně	hojně	k6eAd1	hojně
využívanou	využívaný	k2eAgFnSc7d1	využívaná
polymerázou	polymeráza	k1gFnSc7	polymeráza
je	být	k5eAaImIp3nS	být
Taq	Taq	k1gFnSc1	Taq
polymeráza	polymeráza	k1gFnSc1	polymeráza
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
odolává	odolávat	k5eAaImIp3nS	odolávat
vysokým	vysoký	k2eAgFnPc3d1	vysoká
teplotám	teplota	k1gFnPc3	teplota
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
např.	např.	kA	např.
při	při	k7c6	při
PCR	PCR	kA	PCR
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
různé	různý	k2eAgFnPc4d1	různá
reverzní	reverzní	k2eAgFnPc4d1	reverzní
transkriptázy	transkriptáza	k1gFnPc4	transkriptáza
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgFnSc4d1	umožňující
syntézu	syntéza	k1gFnSc4	syntéza
DNA	DNA	kA	DNA
podle	podle	k7c2	podle
RNA	RNA	kA	RNA
vzoru	vzor	k1gInSc2	vzor
<g/>
.	.	kIx.	.
</s>
