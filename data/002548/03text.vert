<s>
Obsedantně	obsedantně	k6eAd1	obsedantně
kompulzivní	kompulzivní	k2eAgFnSc1d1	kompulzivní
porucha	porucha	k1gFnSc1	porucha
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
OCD	OCD	kA	OCD
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
porucha	porucha	k1gFnSc1	porucha
chování	chování	k1gNnSc2	chování
<g/>
.	.	kIx.	.
</s>
<s>
Člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
trpí	trpět	k5eAaImIp3nS	trpět
touto	tento	k3xDgFnSc7	tento
poruchou	porucha	k1gFnSc7	porucha
<g/>
,	,	kIx,	,
trápí	trápit	k5eAaImIp3nS	trápit
nepříjemné	příjemný	k2eNgFnPc4d1	nepříjemná
myšlenky	myšlenka	k1gFnPc4	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Úlevu	úleva	k1gFnSc4	úleva
od	od	k7c2	od
nich	on	k3xPp3gMnPc2	on
nalézá	nalézat	k5eAaImIp3nS	nalézat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
ujišťuje	ujišťovat	k5eAaImIp3nS	ujišťovat
a	a	k8xC	a
opakovaně	opakovaně	k6eAd1	opakovaně
provádí	provádět	k5eAaImIp3nS	provádět
nějakou	nějaký	k3yIgFnSc4	nějaký
činnost	činnost	k1gFnSc4	činnost
<g/>
:	:	kIx,	:
musím	muset	k5eAaImIp1nS	muset
zkontrolovat	zkontrolovat	k5eAaPmF	zkontrolovat
zamknuté	zamknutý	k2eAgFnPc4d1	zamknutá
dveře	dveře	k1gFnPc4	dveře
3	[number]	k4	3
<g/>
krát	krát	k6eAd1	krát
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dvojka	dvojka	k1gFnSc1	dvojka
není	být	k5eNaImIp3nS	být
hezké	hezký	k2eAgNnSc4d1	hezké
číslo	číslo	k1gNnSc4	číslo
<g/>
;	;	kIx,	;
každé	každý	k3xTgNnSc1	každý
ráno	ráno	k6eAd1	ráno
<g/>
,	,	kIx,	,
když	když	k8xS	když
vstávám	vstávat	k5eAaImIp1nS	vstávat
<g/>
,	,	kIx,	,
musím	muset	k5eAaImIp1nS	muset
šlápnout	šlápnout	k5eAaPmF	šlápnout
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
nejprve	nejprve	k6eAd1	nejprve
levou	levý	k2eAgFnSc4d1	levá
a	a	k8xC	a
potom	potom	k6eAd1	potom
pravou	pravý	k2eAgFnSc7d1	pravá
nohou	noha	k1gFnSc7	noha
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
se	se	k3xPyFc4	se
něco	něco	k3yInSc4	něco
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
;	;	kIx,	;
obcházím	obcházet	k5eAaImIp1nS	obcházet
na	na	k7c6	na
chodníku	chodník	k1gInSc6	chodník
kachličky	kachlička	k1gFnSc2	kachlička
a	a	k8xC	a
listí	listí	k1gNnSc2	listí
<g/>
;	;	kIx,	;
pořád	pořád	k6eAd1	pořád
se	se	k3xPyFc4	se
myji	mýt	k5eAaImIp1nS	mýt
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mám	mít	k5eAaImIp1nS	mít
strach	strach	k1gInSc1	strach
z	z	k7c2	z
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
;	;	kIx,	;
další	další	k2eAgInPc4d1	další
podobné	podobný	k2eAgInPc4d1	podobný
případy	případ	k1gInPc4	případ
<g/>
.	.	kIx.	.
</s>
<s>
Osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
trpí	trpět	k5eAaImIp3nS	trpět
něčím	něco	k3yInSc7	něco
z	z	k7c2	z
výše	vysoce	k6eAd2	vysoce
uvedeného	uvedený	k2eAgNnSc2d1	uvedené
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
neměla	mít	k5eNaImAgFnS	mít
váhat	váhat	k5eAaImF	váhat
a	a	k8xC	a
vyhledat	vyhledat	k5eAaPmF	vyhledat
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
toto	tento	k3xDgNnSc4	tento
chování	chování	k1gNnSc4	chování
nenese	nést	k5eNaImIp3nS	nést
žádný	žádný	k1gMnSc1	žádný
člověk	člověk	k1gMnSc1	člověk
vinu	vina	k1gFnSc4	vina
<g/>
;	;	kIx,	;
většinou	většinou	k6eAd1	většinou
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
ani	ani	k8xC	ani
neuvědomuje	uvědomovat	k5eNaImIp3nS	uvědomovat
<g/>
.	.	kIx.	.
</s>
<s>
Obsedantně	obsedantně	k6eAd1	obsedantně
kompulzivní	kompulzivnět	k5eAaPmIp3nS	kompulzivnět
poruchu	porucha	k1gFnSc4	porucha
lze	lze	k6eAd1	lze
úspěšně	úspěšně	k6eAd1	úspěšně
léčit	léčit	k5eAaImF	léčit
<g/>
.	.	kIx.	.
</s>
<s>
Obsedantně	obsedantně	k6eAd1	obsedantně
kompulzivní	kompulzivní	k2eAgFnSc7d1	kompulzivní
poruchou	porucha	k1gFnSc7	porucha
trpí	trpět	k5eAaImIp3nS	trpět
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
každý	každý	k3xTgMnSc1	každý
42	[number]	k4	42
<g/>
.	.	kIx.	.
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
narůstající	narůstající	k2eAgFnSc3d1	narůstající
komunikaci	komunikace	k1gFnSc3	komunikace
na	na	k7c6	na
sociálních	sociální	k2eAgFnPc6d1	sociální
sítích	síť	k1gFnPc6	síť
<g/>
,	,	kIx,	,
postupné	postupný	k2eAgFnSc3d1	postupná
sociální	sociální	k2eAgFnSc3d1	sociální
izolaci	izolace	k1gFnSc3	izolace
a	a	k8xC	a
ke	k	k7c3	k
zvýšenému	zvýšený	k2eAgInSc3d1	zvýšený
dohledu	dohled	k1gInSc3	dohled
rodičů	rodič	k1gMnPc2	rodič
vůči	vůči	k7c3	vůči
dětem	dítě	k1gFnPc3	dítě
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
číslo	číslo	k1gNnSc1	číslo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
společnosti	společnost	k1gFnSc6	společnost
daleko	daleko	k6eAd1	daleko
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
ještě	ještě	k9	ještě
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
poruchu	porucha	k1gFnSc4	porucha
OCD	OCD	kA	OCD
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stydí	stydět	k5eAaImIp3nS	stydět
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
mluvit	mluvit	k5eAaImF	mluvit
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
chování	chování	k1gNnSc1	chování
je	být	k5eAaImIp3nS	být
nepřesně	přesně	k6eNd1	přesně
nazýváno	nazývat	k5eAaImNgNnS	nazývat
jako	jako	k8xS	jako
neurotická	neurotický	k2eAgFnSc1d1	neurotická
<g/>
,	,	kIx,	,
úzkostná	úzkostný	k2eAgFnSc1d1	úzkostná
duševní	duševní	k2eAgFnSc1d1	duševní
porucha	porucha	k1gFnSc1	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
chování	chování	k1gNnSc6	chování
mají	mít	k5eAaImIp3nP	mít
vliv	vliv	k1gInSc4	vliv
vrozené	vrozený	k2eAgFnSc2d1	vrozená
a	a	k8xC	a
výchovné	výchovný	k2eAgFnSc2d1	výchovná
dispozice	dispozice	k1gFnSc2	dispozice
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
trpí	trpět	k5eAaImIp3nS	trpět
tímto	tento	k3xDgNnSc7	tento
chováním	chování	k1gNnSc7	chování
<g/>
,	,	kIx,	,
neustále	neustále	k6eAd1	neustále
opakuje	opakovat	k5eAaImIp3nS	opakovat
věci	věc	k1gFnPc4	věc
do	do	k7c2	do
kola	kolo	k1gNnSc2	kolo
v	v	k7c6	v
nějakém	nějaký	k3yIgNnSc6	nějaký
pořadí	pořadí	k1gNnSc6	pořadí
a	a	k8xC	a
řádu	řád	k1gInSc6	řád
<g/>
:	:	kIx,	:
umývá	umývat	k5eAaImIp3nS	umývat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
koupe	koupat	k5eAaImIp3nS	koupat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
počítá	počítat	k5eAaImIp3nS	počítat
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
rovná	rovnat	k5eAaImIp3nS	rovnat
a	a	k8xC	a
uspořádává	uspořádávat	k5eAaImIp3nS	uspořádávat
různé	různý	k2eAgInPc4d1	různý
drobné	drobný	k2eAgInPc4d1	drobný
objekty	objekt	k1gInPc4	objekt
a	a	k8xC	a
myslí	myslet	k5eAaImIp3nP	myslet
pořád	pořád	k6eAd1	pořád
dokola	dokola	k6eAd1	dokola
na	na	k7c4	na
něco	něco	k3yInSc4	něco
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
věci	věc	k1gFnPc4	věc
opakuje	opakovat	k5eAaImIp3nS	opakovat
v	v	k7c6	v
přesně	přesně	k6eAd1	přesně
stanoveném	stanovený	k2eAgInSc6d1	stanovený
řádu	řád	k1gInSc6	řád
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nechce	chtít	k5eNaImIp3nS	chtít
dávat	dávat	k5eAaImF	dávat
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
myšlenek	myšlenka	k1gFnPc2	myšlenka
vstup	vstup	k1gInSc1	vstup
zkresleným	zkreslený	k2eAgFnPc3d1	zkreslená
domněnkám	domněnka	k1gFnPc3	domněnka
a	a	k8xC	a
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
věřit	věřit	k5eAaImF	věřit
sám	sám	k3xTgMnSc1	sám
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
trpí	trpět	k5eAaImIp3nP	trpět
silnou	silný	k2eAgFnSc7d1	silná
formou	forma	k1gFnSc7	forma
tohoto	tento	k3xDgNnSc2	tento
ujišťovacího	ujišťovací	k2eAgNnSc2d1	ujišťovací
chování	chování	k1gNnSc2	chování
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
neznají	neznat	k5eAaImIp3nP	neznat
žebříček	žebříček	k1gInSc4	žebříček
svých	svůj	k3xOyFgFnPc2	svůj
hodnot	hodnota	k1gFnPc2	hodnota
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
mezilidskou	mezilidský	k2eAgFnSc7d1	mezilidská
komunikací	komunikace	k1gFnSc7	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgMnSc1	takový
člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
společnosti	společnost	k1gFnSc2	společnost
později	pozdě	k6eAd2	pozdě
vyloučen	vyloučen	k2eAgMnSc1d1	vyloučen
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
se	se	k3xPyFc4	se
samovolně	samovolně	k6eAd1	samovolně
vyloučí	vyloučit	k5eAaPmIp3nS	vyloučit
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
takto	takto	k6eAd1	takto
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
ze	z	k7c2	z
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
naprosto	naprosto	k6eAd1	naprosto
fyzicky	fyzicky	k6eAd1	fyzicky
zdravý	zdravý	k2eAgMnSc1d1	zdravý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nerozumí	rozumět	k5eNaImIp3nS	rozumět
si	se	k3xPyFc3	se
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
má	mít	k5eAaImIp3nS	mít
jiné	jiný	k2eAgInPc4d1	jiný
zájmy	zájem	k1gInPc4	zájem
než	než	k8xS	než
ostatní	ostatní	k2eAgInPc4d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
trpí	trpět	k5eAaImIp3nP	trpět
OCD	OCD	kA	OCD
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
obrovské	obrovský	k2eAgFnPc4d1	obrovská
změny	změna	k1gFnPc4	změna
nálad	nálada	k1gFnPc2	nálada
<g/>
,	,	kIx,	,
strach	strach	k1gInSc4	strach
z	z	k7c2	z
každodenních	každodenní	k2eAgFnPc2d1	každodenní
situací	situace	k1gFnPc2	situace
<g/>
,	,	kIx,	,
strach	strach	k1gInSc1	strach
z	z	k7c2	z
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
různých	různý	k2eAgInPc2d1	různý
objektů	objekt	k1gInPc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
mezi	mezi	k7c7	mezi
námi	my	k3xPp1nPc7	my
lidé	člověk	k1gMnPc1	člověk
klasifikování	klasifikování	k1gNnPc2	klasifikování
s	s	k7c7	s
OCD	OCD	kA	OCD
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
tak	tak	k6eAd1	tak
špatně	špatně	k6eAd1	špatně
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c7	za
jedince	jedinko	k6eAd1	jedinko
trpící	trpící	k2eAgFnSc7d1	trpící
psychózou	psychóza	k1gFnSc7	psychóza
<g/>
.	.	kIx.	.
</s>
<s>
OCD	OCD	kA	OCD
je	být	k5eAaImIp3nS	být
projeveno	projeven	k2eAgNnSc1d1	projeveno
jako	jako	k8xS	jako
psychické	psychický	k2eAgNnSc1d1	psychické
vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
(	(	kIx(	(
<g/>
myšlení	myšlení	k1gNnSc1	myšlení
<g/>
)	)	kIx)	)
a	a	k8xC	a
vnější	vnější	k2eAgNnSc1d1	vnější
(	(	kIx(	(
<g/>
jednání-chování	jednáníhování	k1gNnSc1	jednání-chování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
bereme	brát	k5eAaImIp1nP	brát
OCD	OCD	kA	OCD
jako	jako	k8xC	jako
chování	chování	k1gNnSc4	chování
jedince	jedinec	k1gMnSc2	jedinec
<g/>
,	,	kIx,	,
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
tak	tak	k6eAd1	tak
trochu	trochu	k6eAd1	trochu
filozofický	filozofický	k2eAgInSc4d1	filozofický
výklad	výklad	k1gInSc4	výklad
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
o	o	k7c4	o
typ	typ	k1gInSc4	typ
chování	chování	k1gNnSc2	chování
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
má	mít	k5eAaImIp3nS	mít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
základy	základ	k1gInPc1	základ
už	už	k6eAd1	už
v	v	k7c6	v
dávné	dávný	k2eAgFnSc6d1	dávná
minulosti	minulost	k1gFnSc6	minulost
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
zde	zde	k6eAd1	zde
žádná	žádný	k3yNgFnSc1	žádný
ostrá	ostrý	k2eAgFnSc1d1	ostrá
hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
chováním	chování	k1gNnSc7	chování
a	a	k8xC	a
psychickou	psychický	k2eAgFnSc7d1	psychická
poruchou	porucha	k1gFnSc7	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
chování	chování	k1gNnSc1	chování
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
být	být	k5eAaImF	být
dosti	dosti	k6eAd1	dosti
promyšlené	promyšlený	k2eAgNnSc1d1	promyšlené
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
člověk	člověk	k1gMnSc1	člověk
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
jinak	jinak	k6eAd1	jinak
a	a	k8xC	a
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
jsou	být	k5eAaImIp3nP	být
moc	moc	k6eAd1	moc
velké	velký	k2eAgFnPc1d1	velká
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
léčby	léčba	k1gFnSc2	léčba
kritické	kritický	k2eAgFnSc2d1	kritická
OCD	OCD	kA	OCD
je	být	k5eAaImIp3nS	být
zamezit	zamezit	k5eAaPmF	zamezit
abnormálnímu	abnormální	k2eAgInSc3d1	abnormální
strachu	strach	k1gInSc3	strach
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
objektů	objekt	k1gInPc2	objekt
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
zlepšit	zlepšit	k5eAaPmF	zlepšit
sociální	sociální	k2eAgFnSc4d1	sociální
komunikaci	komunikace	k1gFnSc4	komunikace
a	a	k8xC	a
odbourat	odbourat	k5eAaPmF	odbourat
strach	strach	k1gInSc4	strach
z	z	k7c2	z
věci	věc	k1gFnSc2	věc
přirozených	přirozený	k2eAgFnPc2d1	přirozená
či	či	k8xC	či
nadpřirozených	nadpřirozený	k2eAgFnPc2d1	nadpřirozená
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
OCD	OCD	kA	OCD
není	být	k5eNaImIp3nS	být
vážná	vážný	k2eAgFnSc1d1	vážná
psychická	psychický	k2eAgFnSc1d1	psychická
porucha	porucha	k1gFnSc1	porucha
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
léčitelná	léčitelný	k2eAgFnSc1d1	léčitelná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
správné	správný	k2eAgFnSc2d1	správná
léčby	léčba	k1gFnSc2	léčba
pacientovi	pacient	k1gMnSc3	pacient
dostavuje	dostavovat	k5eAaImIp3nS	dostavovat
<g/>
.	.	kIx.	.
</s>
<s>
Nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
zaměněna	zaměnit	k5eAaPmNgFnS	zaměnit
za	za	k7c4	za
OCPD	OCPD	kA	OCPD
a	a	k8xC	a
autistické	autistický	k2eAgFnSc2d1	autistická
poruchy	porucha	k1gFnSc2	porucha
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
OCD	OCD	kA	OCD
má	mít	k5eAaImIp3nS	mít
mnohdy	mnohdy	k6eAd1	mnohdy
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
společné	společný	k2eAgInPc1d1	společný
znaky	znak	k1gInPc1	znak
<g/>
.	.	kIx.	.
</s>
<s>
Obsedantně-kompulzivní	Obsedantněompulzivní	k2eAgNnSc1d1	Obsedantně-kompulzivní
chování	chování	k1gNnSc1	chování
(	(	kIx(	(
<g/>
označováno	označován	k2eAgNnSc1d1	označováno
jako	jako	k8xS	jako
obsedantně	obsedantně	k6eAd1	obsedantně
kompulzivní	kompulzivní	k2eAgFnSc1d1	kompulzivní
porucha	porucha	k1gFnSc1	porucha
-	-	kIx~	-
dále	daleko	k6eAd2	daleko
jen	jen	k9	jen
OCD	OCD	kA	OCD
<g/>
)	)	kIx)	)
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
zaměňována	zaměňován	k2eAgFnSc1d1	zaměňována
s	s	k7c7	s
OCPD	OCPD	kA	OCPD
(	(	kIx(	(
<g/>
Obsedantně	obsedantně	k6eAd1	obsedantně
kompulzivní	kompulzivnět	k5eAaPmIp3nS	kompulzivnět
poruchou	porucha	k1gFnSc7	porucha
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
)	)	kIx)	)
a	a	k8xC	a
jinými	jiný	k2eAgFnPc7d1	jiná
úzkostnými	úzkostný	k2eAgFnPc7d1	úzkostná
poruchami	porucha	k1gFnPc7	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
poruchu	porucha	k1gFnSc4	porucha
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
logickým	logický	k2eAgNnSc7d1	logické
obsesivním	obsesivní	k2eAgNnSc7d1	obsesivní
a	a	k8xC	a
kompulzivním	kompulzivní	k2eAgNnSc7d1	kompulzivní
chováním	chování	k1gNnSc7	chování
trpí	trpět	k5eAaImIp3nS	trpět
úplně	úplně	k6eAd1	úplně
každý	každý	k3xTgMnSc1	každý
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
reakci	reakce	k1gFnSc4	reakce
na	na	k7c4	na
popud	popud	k1gInSc4	popud
(	(	kIx(	(
<g/>
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,	,
vjem	vjem	k1gInSc4	vjem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
OCD	OCD	kA	OCD
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
posuzovat	posuzovat	k5eAaImF	posuzovat
míru	míra	k1gFnSc4	míra
závažnosti	závažnost	k1gFnSc2	závažnost
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tomto	tento	k3xDgNnSc6	tento
chování	chování	k1gNnSc6	chování
se	se	k3xPyFc4	se
ví	vědět	k5eAaImIp3nS	vědět
jen	jen	k9	jen
málo	málo	k4c1	málo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
na	na	k7c4	na
něho	on	k3xPp3gMnSc4	on
dá	dát	k5eAaPmIp3nS	dát
těžko	těžko	k6eAd1	těžko
přijít	přijít	k5eAaPmF	přijít
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
naše	náš	k3xOp1gFnSc1	náš
populace	populace	k1gFnSc1	populace
má	mít	k5eAaImIp3nS	mít
málo	málo	k4c4	málo
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
tomto	tento	k3xDgNnSc6	tento
chování	chování	k1gNnSc6	chování
<g/>
.	.	kIx.	.
</s>
<s>
Silná	silný	k2eAgFnSc1d1	silná
forma	forma	k1gFnSc1	forma
OCD	OCD	kA	OCD
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
trpí	trpět	k5eAaImIp3nS	trpět
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
jedince	jedinec	k1gMnPc4	jedinec
ze	z	k7c2	z
sociální	sociální	k2eAgFnSc2d1	sociální
sféry	sféra	k1gFnSc2	sféra
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
společnost	společnost	k1gFnSc1	společnost
jej	on	k3xPp3gMnSc4	on
vyloučila	vyloučit	k5eAaPmAgFnS	vyloučit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
osoba	osoba	k1gFnSc1	osoba
má	mít	k5eAaImIp3nS	mít
slabé	slabý	k2eAgNnSc1d1	slabé
sebevědomí	sebevědomí	k1gNnSc1	sebevědomí
<g/>
,	,	kIx,	,
slabý	slabý	k2eAgInSc4d1	slabý
žebříček	žebříček	k1gInSc4	žebříček
hodnot	hodnota	k1gFnPc2	hodnota
a	a	k8xC	a
nezvládá	zvládat	k5eNaImIp3nS	zvládat
správné	správný	k2eAgInPc4d1	správný
komunikační	komunikační	k2eAgInPc4d1	komunikační
a	a	k8xC	a
emoční	emoční	k2eAgInPc4d1	emoční
návyky	návyk	k1gInPc4	návyk
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jdou	jít	k5eAaImIp3nP	jít
u	u	k7c2	u
nositelů	nositel	k1gMnPc2	nositel
OCD	OCD	kA	OCD
zlepšit	zlepšit	k5eAaPmF	zlepšit
do	do	k7c2	do
velmi	velmi	k6eAd1	velmi
vysoké	vysoký	k2eAgFnSc2d1	vysoká
úrovně	úroveň	k1gFnSc2	úroveň
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
potřeba	potřeba	k1gFnSc1	potřeba
spolupráce	spolupráce	k1gFnSc1	spolupráce
jak	jak	k8xC	jak
na	na	k7c4	na
lékařské	lékařský	k2eAgNnSc4d1	lékařské
<g/>
,	,	kIx,	,
tak	tak	k9	tak
na	na	k7c6	na
psychoterapeutické	psychoterapeutický	k2eAgFnSc6d1	psychoterapeutická
úrovni	úroveň	k1gFnSc6	úroveň
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
vůli	vůle	k1gFnSc4	vůle
klienta	klient	k1gMnSc2	klient
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nespolupráci	nespoluprák	k1gMnPc1	nespoluprák
pacienta	pacient	k1gMnSc2	pacient
může	moct	k5eAaImIp3nS	moct
tak	tak	k6eAd1	tak
být	být	k5eAaImF	být
situace	situace	k1gFnSc2	situace
díky	díky	k7c3	díky
samotnému	samotný	k2eAgNnSc3d1	samotné
OCD	OCD	kA	OCD
jen	jen	k9	jen
těžko	těžko	k6eAd1	těžko
řešitelná	řešitelný	k2eAgFnSc1d1	řešitelná
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
této	tento	k3xDgFnSc2	tento
bezvýchodné	bezvýchodný	k2eAgFnSc2d1	bezvýchodná
situace	situace	k1gFnSc2	situace
může	moct	k5eAaImIp3nS	moct
pomoct	pomoct	k5eAaPmF	pomoct
vyhledat	vyhledat	k5eAaPmF	vyhledat
jiného	jiný	k2eAgMnSc4d1	jiný
doktora	doktor	k1gMnSc4	doktor
na	na	k7c6	na
jiném	jiný	k2eAgNnSc6d1	jiné
místě	místo	k1gNnSc6	místo
nebo	nebo	k8xC	nebo
zájem	zájem	k1gInSc1	zájem
vyřešit	vyřešit	k5eAaPmF	vyřešit
OCD	OCD	kA	OCD
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Obsedantně	obsedantně	k6eAd1	obsedantně
kompulzivní	kompulzivní	k2eAgNnSc1d1	kompulzivní
chování	chování	k1gNnSc1	chování
je	být	k5eAaImIp3nS	být
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
typ	typ	k1gInSc4	typ
myšlení	myšlení	k1gNnSc1	myšlení
a	a	k8xC	a
jednání	jednání	k1gNnSc1	jednání
zdravého	zdravý	k2eAgMnSc2d1	zdravý
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgNnSc7d1	základní
jednáním	jednání	k1gNnSc7	jednání
je	být	k5eAaImIp3nS	být
kompulze	kompulze	k1gFnSc1	kompulze
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
při	při	k7c6	při
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
chceme	chtít	k5eAaImIp1nP	chtít
zabránit	zabránit	k5eAaPmF	zabránit
myšlence	myšlenka	k1gFnSc3	myšlenka
(	(	kIx(	(
<g/>
obsesi	obsese	k1gFnSc3	obsese
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
nepropadli	propadnout	k5eNaPmAgMnP	propadnout
v	v	k7c4	v
úzkost	úzkost	k1gFnSc4	úzkost
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
představení	představení	k1gNnSc4	představení
si	se	k3xPyFc3	se
můžeme	moct	k5eAaImIp1nP	moct
dát	dát	k5eAaPmF	dát
za	za	k7c4	za
příklad	příklad	k1gInSc4	příklad
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
chceme	chtít	k5eAaImIp1nP	chtít
zkontrolovat	zkontrolovat	k5eAaPmF	zkontrolovat
zamknuté	zamknutý	k2eAgFnPc4d1	zamknutá
dveře	dveře	k1gFnPc4	dveře
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
máme	mít	k5eAaImIp1nP	mít
myšlenku	myšlenka	k1gFnSc4	myšlenka
(	(	kIx(	(
<g/>
obsesi	obsese	k1gFnSc4	obsese
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
náš	náš	k3xOp1gInSc1	náš
dům	dům	k1gInSc1	dům
může	moct	k5eAaImIp3nS	moct
někdo	někdo	k3yInSc1	někdo
vykrást	vykrást	k5eAaPmF	vykrást
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
nezajdeme	zajít	k5eNaPmIp1nP	zajít
dveře	dveře	k1gFnPc4	dveře
zkontrolovat	zkontrolovat	k5eAaPmF	zkontrolovat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
jednání	jednání	k1gNnSc1	jednání
označované	označovaný	k2eAgFnSc2d1	označovaná
jako	jako	k8xC	jako
kompulze	kompulze	k1gFnSc2	kompulze
<g/>
,	,	kIx,	,
dostaví	dostavit	k5eAaPmIp3nS	dostavit
se	se	k3xPyFc4	se
zřetelná	zřetelný	k2eAgFnSc1d1	zřetelná
úzkost	úzkost	k1gFnSc1	úzkost
<g/>
.	.	kIx.	.
</s>
<s>
Zní	znět	k5eAaImIp3nS	znět
to	ten	k3xDgNnSc1	ten
poměrně	poměrně	k6eAd1	poměrně
jednoduše	jednoduše	k6eAd1	jednoduše
a	a	k8xC	a
potkává	potkávat	k5eAaImIp3nS	potkávat
to	ten	k3xDgNnSc1	ten
každého	každý	k3xTgMnSc2	každý
z	z	k7c2	z
nás	my	k3xPp1nPc2	my
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
nezajdou	zajít	k5eNaPmIp3nP	zajít
zkontrolovat	zkontrolovat	k5eAaPmF	zkontrolovat
zmíněné	zmíněný	k2eAgFnPc4d1	zmíněná
dveře	dveře	k1gFnPc4	dveře
<g/>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
2	[number]	k4	2
<g/>
×	×	k?	×
a	a	k8xC	a
neotočí	otočit	k5eNaPmIp3nS	otočit
se	se	k3xPyFc4	se
ve	v	k7c6	v
dveřích	dveře	k1gFnPc6	dveře
třikrát	třikrát	k6eAd1	třikrát
<g/>
,	,	kIx,	,
stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
něco	něco	k3yInSc1	něco
nepříjemného	příjemný	k2eNgNnSc2d1	nepříjemné
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
zase	zase	k9	zase
trpí	trpět	k5eAaImIp3nP	trpět
tak	tak	k6eAd1	tak
silnou	silný	k2eAgFnSc7d1	silná
a	a	k8xC	a
komplikovanou	komplikovaný	k2eAgFnSc7d1	komplikovaná
formou	forma	k1gFnSc7	forma
OCD	OCD	kA	OCD
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
životě	život	k1gInSc6	život
problémy	problém	k1gInPc1	problém
se	s	k7c7	s
sociální	sociální	k2eAgFnSc7d1	sociální
interakcí	interakce	k1gFnSc7	interakce
<g/>
,	,	kIx,	,
životním	životní	k2eAgInSc7d1	životní
řádem	řád	k1gInSc7	řád
a	a	k8xC	a
citlivostí	citlivost	k1gFnSc7	citlivost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
příčinou	příčina	k1gFnSc7	příčina
nebo	nebo	k8xC	nebo
následkem	následek	k1gInSc7	následek
mohutné	mohutný	k2eAgFnSc2d1	mohutná
OCD	OCD	kA	OCD
<g/>
.	.	kIx.	.
</s>
<s>
Jedinec	jedinec	k1gMnSc1	jedinec
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
zvýšené	zvýšený	k2eAgNnSc4d1	zvýšené
OCD	OCD	kA	OCD
<g/>
,	,	kIx,	,
provádí	provádět	k5eAaImIp3nS	provádět
veškeré	veškerý	k3xTgNnSc4	veškerý
jednání	jednání	k1gNnSc4	jednání
obvykle	obvykle	k6eAd1	obvykle
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gInSc4	on
neviděli	vidět	k5eNaImAgMnP	vidět
ostatní	ostatní	k2eAgMnPc1d1	ostatní
a	a	k8xC	a
nepovažovali	považovat	k5eNaImAgMnP	považovat
ho	on	k3xPp3gNnSc4	on
za	za	k7c4	za
psychicky	psychicky	k6eAd1	psychicky
nemocného	nemocný	k2eAgMnSc4d1	nemocný
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
plyne	plynout	k5eAaImIp3nS	plynout
logická	logický	k2eAgFnSc1d1	logická
věc	věc	k1gFnSc1	věc
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
jistý	jistý	k2eAgMnSc1d1	jistý
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
dělá	dělat	k5eAaImIp3nS	dělat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nenormální	normální	k2eNgNnSc1d1	nenormální
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
závažnou	závažný	k2eAgFnSc4d1	závažná
nevyléčitelnou	vyléčitelný	k2eNgFnSc4d1	nevyléčitelná
formu	forma	k1gFnSc4	forma
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
tímto	tento	k3xDgNnSc7	tento
chováním	chování	k1gNnSc7	chování
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
dotyčný	dotyčný	k2eAgMnSc1d1	dotyčný
provozuje	provozovat	k5eAaImIp3nS	provozovat
a	a	k8xC	a
neustále	neustále	k6eAd1	neustále
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
myslí	myslet	k5eAaImIp3nS	myslet
stereotypní	stereotypní	k2eAgNnSc4d1	stereotypní
myšlení	myšlení	k1gNnSc4	myšlení
<g/>
,	,	kIx,	,
vyhýbání	vyhýbání	k1gNnSc4	vyhýbání
<g/>
,	,	kIx,	,
čištění	čištění	k1gNnSc4	čištění
<g/>
,	,	kIx,	,
počítání	počítání	k1gNnSc4	počítání
<g/>
,	,	kIx,	,
mimické	mimický	k2eAgNnSc4d1	mimické
gesta	gesto	k1gNnPc4	gesto
<g/>
,	,	kIx,	,
mytí	mytý	k2eAgMnPc1d1	mytý
si	se	k3xPyFc3	se
rukou	ruka	k1gFnPc2	ruka
<g/>
,	,	kIx,	,
ujišťování	ujišťování	k1gNnSc2	ujišťování
atd.	atd.	kA	atd.
Pokud	pokud	k8xS	pokud
toto	tento	k3xDgNnSc4	tento
jednání	jednání	k1gNnSc4	jednání
osoba	osoba	k1gFnSc1	osoba
nedodrží	dodržet	k5eNaPmIp3nS	dodržet
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
automaticky	automaticky	k6eAd1	automaticky
vystavena	vystavit	k5eAaPmNgFnS	vystavit
obrovským	obrovský	k2eAgFnPc3d1	obrovská
hodnotám	hodnota	k1gFnPc3	hodnota
úzkosti	úzkost	k1gFnSc2	úzkost
a	a	k8xC	a
strachu	strach	k1gInSc2	strach
z	z	k7c2	z
věcí	věc	k1gFnPc2	věc
přirozených	přirozený	k2eAgNnPc2d1	přirozené
či	či	k8xC	či
nadpřirozených	nadpřirozený	k2eAgNnPc2d1	nadpřirozené
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dalších	další	k2eAgInPc6d1	další
krocích	krok	k1gInPc6	krok
jedince	jedinec	k1gMnSc4	jedinec
OCD	OCD	kA	OCD
mizí	mizet	k5eAaImIp3nS	mizet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
faktu	fakt	k1gInSc6	fakt
je	být	k5eAaImIp3nS	být
založena	založen	k2eAgFnSc1d1	založena
psychoterapeutická	psychoterapeutický	k2eAgFnSc1d1	psychoterapeutická
léčba	léčba	k1gFnSc1	léčba
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
lidská	lidský	k2eAgFnSc1d1	lidská
psychika	psychika	k1gFnSc1	psychika
si	se	k3xPyFc3	se
může	moct	k5eAaImIp3nS	moct
najít	najít	k5eAaPmF	najít
znovu	znovu	k6eAd1	znovu
svůj	svůj	k3xOyFgInSc4	svůj
pravidelný	pravidelný	k2eAgInSc4d1	pravidelný
řád	řád	k1gInSc4	řád
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
mohutné	mohutný	k2eAgFnPc4d1	mohutná
obsese	obsese	k1gFnPc4	obsese
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
úzkost	úzkost	k1gFnSc4	úzkost
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
likvidovány	likvidován	k2eAgFnPc1d1	likvidována
kompulzemi	kompulze	k1gFnPc7	kompulze
<g/>
.	.	kIx.	.
</s>
<s>
Strach	strach	k1gInSc1	strach
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
úzkost	úzkost	k1gFnSc1	úzkost
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
vyvolaná	vyvolaný	k2eAgFnSc1d1	vyvolaná
pouze	pouze	k6eAd1	pouze
momentální	momentální	k2eAgFnSc1d1	momentální
špatnou	špatný	k2eAgFnSc7d1	špatná
myšlenkou	myšlenka	k1gFnSc7	myšlenka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
náhlým	náhlý	k2eAgInSc7d1	náhlý
stresujícím	stresující	k2eAgInSc7d1	stresující
faktorem	faktor	k1gInSc7	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vyvolána	vyvolat	k5eAaPmNgFnS	vyvolat
nejrůznějšími	různý	k2eAgInPc7d3	nejrůznější
podněty	podnět	k1gInPc7	podnět
<g/>
,	,	kIx,	,
ať	ať	k9	ať
už	už	k9	už
náhlými	náhlý	k2eAgInPc7d1	náhlý
vnějšími	vnější	k2eAgInPc7d1	vnější
psychickými	psychický	k2eAgInPc7d1	psychický
jevy	jev	k1gInPc7	jev
(	(	kIx(	(
<g/>
vjemy	vjem	k1gInPc7	vjem
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
vzdálenými	vzdálený	k2eAgInPc7d1	vzdálený
vnitřními	vnitřní	k2eAgInPc7d1	vnitřní
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
uloženy	uložit	k5eAaPmNgFnP	uložit
v	v	k7c6	v
paměti	paměť	k1gFnSc6	paměť
jedince	jedinec	k1gMnPc4	jedinec
a	a	k8xC	a
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
problémem	problém	k1gInSc7	problém
nejdůležitějšího	důležitý	k2eAgInSc2d3	nejdůležitější
psychického	psychický	k2eAgInSc2d1	psychický
procesu	proces	k1gInSc2	proces
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
poznáváním	poznávání	k1gNnSc7	poznávání
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
podnětu	podnět	k1gInSc6	podnět
donucen	donucen	k2eAgMnSc1d1	donucen
odpovědět	odpovědět	k5eAaPmF	odpovědět
iracionálně	iracionálně	k6eAd1	iracionálně
a	a	k8xC	a
nepřiměřeně	přiměřeně	k6eNd1	přiměřeně
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
si	se	k3xPyFc3	se
obvykle	obvykle	k6eAd1	obvykle
uvědomuje	uvědomovat	k5eAaImIp3nS	uvědomovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pozoruhodné	pozoruhodný	k2eAgNnSc1d1	pozoruhodné
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedinec	jedinec	k1gMnSc1	jedinec
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
jistý	jistý	k2eAgMnSc1d1	jistý
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedná	jednat	k5eAaImIp3nS	jednat
zcela	zcela	k6eAd1	zcela
správně	správně	k6eAd1	správně
a	a	k8xC	a
ostatní	ostatní	k2eAgMnPc1d1	ostatní
jednají	jednat	k5eAaImIp3nP	jednat
odolatelně	odolatelně	k6eAd1	odolatelně
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
trpí	trpět	k5eAaImIp3nP	trpět
tímto	tento	k3xDgNnSc7	tento
chováním	chování	k1gNnSc7	chování
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
vědí	vědět	k5eAaImIp3nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nepřiměřené	přiměřený	k2eNgNnSc1d1	nepřiměřené
a	a	k8xC	a
nadhodnocené	nadhodnocený	k2eAgNnSc1d1	nadhodnocené
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedokážou	dokázat	k5eNaPmIp3nP	dokázat
mu	on	k3xPp3gMnSc3	on
čelit	čelit	k5eAaImF	čelit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
stanovují	stanovovat	k5eAaImIp3nP	stanovovat
svůj	svůj	k3xOyFgInSc4	svůj
myšlenkový	myšlenkový	k2eAgInSc4d1	myšlenkový
řád	řád	k1gInSc4	řád
a	a	k8xC	a
pravidelnost	pravidelnost	k1gFnSc4	pravidelnost
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
není	být	k5eNaImIp3nS	být
jasný	jasný	k2eAgInSc4d1	jasný
přesný	přesný	k2eAgInSc4d1	přesný
průběh	průběh	k1gInSc4	průběh
tohoto	tento	k3xDgNnSc2	tento
chování	chování	k1gNnSc2	chování
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zkoumáno	zkoumán	k2eAgNnSc1d1	zkoumáno
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
OCD	OCD	kA	OCD
je	být	k5eAaImIp3nS	být
pravidelný	pravidelný	k2eAgInSc4d1	pravidelný
opakující	opakující	k2eAgInSc4d1	opakující
se	se	k3xPyFc4	se
cyklus	cyklus	k1gInSc4	cyklus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
různou	různý	k2eAgFnSc4d1	různá
míru	míra	k1gFnSc4	míra
závažnosti	závažnost	k1gFnSc2	závažnost
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
trpí	trpět	k5eAaImIp3nP	trpět
kompulzivním	kompulzivní	k2eAgNnSc7d1	kompulzivní
chováním	chování	k1gNnSc7	chování
třeba	třeba	k6eAd1	třeba
dvě	dva	k4xCgFnPc4	dva
hodiny	hodina	k1gFnPc4	hodina
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
mají	mít	k5eAaImIp3nP	mít
OCD	OCD	kA	OCD
tak	tak	k6eAd1	tak
silné	silný	k2eAgNnSc4d1	silné
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
těžko	těžko	k6eAd1	těžko
rozlišitelné	rozlišitelný	k2eAgInPc4d1	rozlišitelný
od	od	k7c2	od
psychózy	psychóza	k1gFnSc2	psychóza
<g/>
.	.	kIx.	.
</s>
<s>
OCD	OCD	kA	OCD
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
léčit	léčit	k5eAaImF	léčit
s	s	k7c7	s
pozoruhodnými	pozoruhodný	k2eAgInPc7d1	pozoruhodný
výsledky	výsledek	k1gInPc7	výsledek
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
psychoterapeuticky	psychoterapeuticky	k6eAd1	psychoterapeuticky
<g/>
,	,	kIx,	,
farmakologicky	farmakologicky	k6eAd1	farmakologicky
nebo	nebo	k8xC	nebo
v	v	k7c6	v
závažnějších	závažný	k2eAgInPc6d2	závažnější
případech	případ	k1gInPc6	případ
neurologicky	neurologicky	k6eAd1	neurologicky
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
dva	dva	k4xCgInPc4	dva
jmenované	jmenovaný	k2eAgInPc4d1	jmenovaný
přístupy	přístup	k1gInPc4	přístup
jsou	být	k5eAaImIp3nP	být
nejčastější	častý	k2eAgMnPc1d3	nejčastější
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
se	se	k3xPyFc4	se
provádět	provádět	k5eAaImF	provádět
najednou	najednou	k6eAd1	najednou
pro	pro	k7c4	pro
větší	veliký	k2eAgFnSc4d2	veliký
účinnost	účinnost	k1gFnSc4	účinnost
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
přístup	přístup	k1gInSc1	přístup
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
po	po	k7c6	po
selhání	selhání	k1gNnSc6	selhání
prvních	první	k4xOgInPc2	první
dvou	dva	k4xCgNnPc2	dva
<g/>
.	.	kIx.	.
</s>
<s>
OCD	OCD	kA	OCD
zvýšené	zvýšený	k2eAgNnSc1d1	zvýšené
chování	chování	k1gNnSc1	chování
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
přesněji	přesně	k6eAd2	přesně
úzkostná	úzkostný	k2eAgFnSc1d1	úzkostná
neurotická	neurotický	k2eAgFnSc1d1	neurotická
porucha	porucha	k1gFnSc1	porucha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
strukturu	struktura	k1gFnSc4	struktura
často	často	k6eAd1	často
zaměňována	zaměňovat	k5eAaImNgFnS	zaměňovat
se	s	k7c7	s
závislostí	závislost	k1gFnSc7	závislost
<g/>
.	.	kIx.	.
</s>
<s>
OCD	OCD	kA	OCD
je	být	k5eAaImIp3nS	být
podmíněna	podmínit	k5eAaPmNgFnS	podmínit
mnoha	mnoho	k4c7	mnoho
faktory	faktor	k1gInPc7	faktor
biologickými	biologický	k2eAgInPc7d1	biologický
i	i	k8xC	i
psychologickými	psychologický	k2eAgInPc7d1	psychologický
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
léčbě	léčba	k1gFnSc3	léčba
obsedantně	obsedantně	k6eAd1	obsedantně
kompulzivní	kompulzivní	k2eAgFnSc2d1	kompulzivní
poruchy	porucha	k1gFnSc2	porucha
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jak	jak	k6eAd1	jak
farmakoterapie	farmakoterapie	k1gFnSc2	farmakoterapie
(	(	kIx(	(
<g/>
antidepresiva	antidepresivum	k1gNnSc2	antidepresivum
<g/>
)	)	kIx)	)
tak	tak	k9	tak
psychoterapie	psychoterapie	k1gFnSc1	psychoterapie
(	(	kIx(	(
<g/>
kognitivně	kognitivně	k6eAd1	kognitivně
behaviorální	behaviorální	k2eAgFnPc1d1	behaviorální
terapie	terapie	k1gFnPc1	terapie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejúčinnější	účinný	k2eAgFnSc1d3	nejúčinnější
je	být	k5eAaImIp3nS	být
však	však	k9	však
kombinace	kombinace	k1gFnSc1	kombinace
obojího	obojí	k4xRgMnSc2	obojí
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
psychoterapii	psychoterapie	k1gFnSc6	psychoterapie
je	být	k5eAaImIp3nS	být
pacient	pacient	k1gMnSc1	pacient
postupně	postupně	k6eAd1	postupně
vystavován	vystavovat	k5eAaImNgInS	vystavovat
situacím	situace	k1gFnPc3	situace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mu	on	k3xPp3gInSc3	on
působí	působit	k5eAaImIp3nS	působit
úzkost	úzkost	k1gFnSc4	úzkost
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tak	tak	k9	tak
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nedojde	dojít	k5eNaPmIp3nS	dojít
k	k	k7c3	k
jejímu	její	k3xOp3gInSc3	její
poklesu	pokles	k1gInSc3	pokles
<g/>
.	.	kIx.	.
</s>
<s>
Těmto	tento	k3xDgFnPc3	tento
situacím	situace	k1gFnPc3	situace
musí	muset	k5eAaImIp3nS	muset
čelit	čelit	k5eAaImF	čelit
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
vykonal	vykonat	k5eAaPmAgMnS	vykonat
kompulzi	kompulze	k1gFnSc4	kompulze
<g/>
.	.	kIx.	.
</s>
<s>
Kompulzí	Kompulzit	k5eAaPmIp3nS	Kompulzit
se	se	k3xPyFc4	se
pacient	pacient	k1gMnSc1	pacient
zbaví	zbavit	k5eAaPmIp3nS	zbavit
až	až	k9	až
po	po	k7c6	po
vyléčení	vyléčení	k1gNnSc6	vyléčení
obsesí	obsese	k1gFnPc2	obsese
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
právě	právě	k9	právě
ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
jejich	jejich	k3xOp3gFnSc7	jejich
příčinou	příčina	k1gFnSc7	příčina
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
psychoterapeut	psychoterapeut	k1gMnSc1	psychoterapeut
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
osobnost	osobnost	k1gFnSc4	osobnost
vzbuzující	vzbuzující	k2eAgFnSc4d1	vzbuzující
důvěru	důvěra	k1gFnSc4	důvěra
a	a	k8xC	a
respekt	respekt	k1gInSc4	respekt
<g/>
.	.	kIx.	.
</s>
<s>
Pojišťovna	pojišťovna	k1gFnSc1	pojišťovna
léčbu	léčba	k1gFnSc4	léčba
pacientům	pacient	k1gMnPc3	pacient
proplácí	proplácet	k5eAaImIp3nS	proplácet
a	a	k8xC	a
návštěva	návštěva	k1gFnSc1	návštěva
psychologa	psycholog	k1gMnSc2	psycholog
1	[number]	k4	1
<g/>
×	×	k?	×
za	za	k7c4	za
měsíc	měsíc	k1gInSc4	měsíc
je	být	k5eAaImIp3nS	být
neúčinná	účinný	k2eNgFnSc1d1	neúčinná
<g/>
.	.	kIx.	.
</s>
<s>
Psychologů	psycholog	k1gMnPc2	psycholog
je	být	k5eAaImIp3nS	být
málo	málo	k6eAd1	málo
a	a	k8xC	a
dobrých	dobrý	k2eAgFnPc2d1	dobrá
ještě	ještě	k9	ještě
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
<s>
Pacienti	pacient	k1gMnPc1	pacient
jsou	být	k5eAaImIp3nP	být
nezaměstnávatelní	zaměstnávatelný	k2eNgMnPc1d1	zaměstnávatelný
a	a	k8xC	a
finance	finance	k1gFnPc1	finance
na	na	k7c6	na
terapii	terapie	k1gFnSc6	terapie
nemají	mít	k5eNaImIp3nP	mít
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgInPc1d3	nejčastější
typy	typ	k1gInPc1	typ
obsedantně	obsedantně	k6eAd1	obsedantně
kompulzivní	kompulzivní	k2eAgInPc1d1	kompulzivní
poruchy	poruch	k1gInPc1	poruch
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
obsese	obsese	k1gFnPc4	obsese
bez	bez	k7c2	bez
zjevných	zjevný	k2eAgFnPc2d1	zjevná
kompulzí	kompulze	k1gFnPc2	kompulze
(	(	kIx(	(
<g/>
10	[number]	k4	10
%	%	kIx~	%
<g/>
)	)	kIx)	)
nadměrný	nadměrný	k2eAgInSc1d1	nadměrný
strach	strach	k1gInSc1	strach
ze	z	k7c2	z
špíny	špína	k1gFnSc2	špína
a	a	k8xC	a
infekce	infekce	k1gFnSc2	infekce
(	(	kIx(	(
<g/>
50	[number]	k4	50
%	%	kIx~	%
<g/>
)	)	kIx)	)
kontrolování	kontrolování	k1gNnSc1	kontrolování
(	(	kIx(	(
<g/>
strach	strach	k1gInSc1	strach
z	z	k7c2	z
opomenutí	opomenutí	k1gNnSc2	opomenutí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
by	by	kYmCp3nS	by
ohrozilo	ohrozit	k5eAaPmAgNnS	ohrozit
je	on	k3xPp3gNnSc4	on
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgMnPc4d1	jiný
lidi	člověk	k1gMnPc4	člověk
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g />
.	.	kIx.	.
</s>
<s>
%	%	kIx~	%
<g/>
)	)	kIx)	)
jiné	jiný	k2eAgFnPc1d1	jiná
kompulze	kompulze	k1gFnPc1	kompulze
(	(	kIx(	(
<g/>
opakování	opakování	k1gNnSc1	opakování
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
dotykové	dotykový	k2eAgFnPc1d1	dotyková
kompulze	kompulze	k1gFnPc1	kompulze
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
10	[number]	k4	10
%	%	kIx~	%
<g/>
)	)	kIx)	)
chorobný	chorobný	k2eAgInSc4d1	chorobný
strach	strach	k1gInSc4	strach
z	z	k7c2	z
neekologicky	ekologicky	k6eNd1	ekologicky
používaných	používaný	k2eAgInPc2d1	používaný
aromatických	aromatický	k2eAgInPc2d1	aromatický
přípravků	přípravek	k1gInPc2	přípravek
<g/>
,	,	kIx,	,
deodorantů	deodorant	k1gInPc2	deodorant
<g/>
,	,	kIx,	,
pracích	prací	k2eAgInPc2d1	prací
a	a	k8xC	a
čisticích	čisticí	k2eAgInPc2d1	čisticí
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
ftalátů	ftalát	k1gInPc2	ftalát
z	z	k7c2	z
nábytku	nábytek	k1gInSc2	nábytek
<g/>
...	...	k?	...
</s>
<s>
Obsese	obsese	k1gFnSc1	obsese
je	být	k5eAaImIp3nS	být
chorobně	chorobně	k6eAd1	chorobně
utkvělá	utkvělý	k2eAgFnSc1d1	utkvělá
představa	představa	k1gFnSc1	představa
<g/>
,	,	kIx,	,
myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
medicínského	medicínský	k2eAgNnSc2d1	medicínské
hlediska	hledisko	k1gNnSc2	hledisko
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
nutkavé	nutkavý	k2eAgInPc1d1	nutkavý
jevy	jev	k1gInPc1	jev
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
myšlenky	myšlenka	k1gFnPc4	myšlenka
<g/>
,	,	kIx,	,
popudy	popud	k1gInPc4	popud
k	k	k7c3	k
jednání	jednání	k1gNnSc3	jednání
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
bezdůvodně	bezdůvodně	k6eAd1	bezdůvodně
ovládají	ovládat	k5eAaImIp3nP	ovládat
mysl	mysl	k1gFnSc4	mysl
pacienta	pacient	k1gMnSc2	pacient
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
je	on	k3xPp3gMnPc4	on
snaží	snažit	k5eAaImIp3nP	snažit
potlačit	potlačit	k5eAaPmF	potlačit
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
automatické	automatický	k2eAgFnPc1d1	automatická
<g/>
,	,	kIx,	,
časté	častý	k2eAgFnPc1d1	častá
<g/>
,	,	kIx,	,
úzkostné	úzkostný	k2eAgFnPc1d1	úzkostná
<g/>
,	,	kIx,	,
těžko	těžko	k6eAd1	těžko
kontrolovatelné	kontrolovatelný	k2eAgFnPc1d1	kontrolovatelná
<g/>
.	.	kIx.	.
</s>
<s>
Paradoxně	paradoxně	k6eAd1	paradoxně
čím	co	k3yRnSc7	co
více	hodně	k6eAd2	hodně
se	se	k3xPyFc4	se
pacient	pacient	k1gMnSc1	pacient
snaží	snažit	k5eAaImIp3nS	snažit
přestat	přestat	k5eAaPmF	přestat
na	na	k7c4	na
určitou	určitý	k2eAgFnSc4d1	určitá
věc	věc	k1gFnSc4	věc
myslet	myslet	k5eAaImF	myslet
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
myšlenka	myšlenka	k1gFnSc1	myšlenka
vtíravější	vtíravý	k2eAgFnSc1d2	vtíravější
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
fenomén	fenomén	k1gInSc4	fenomén
nemyslete	myslet	k5eNaImRp2nP	myslet
na	na	k7c4	na
slona	slon	k1gMnSc4	slon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Typickými	typický	k2eAgFnPc7d1	typická
obsesemi	obsese	k1gFnPc7	obsese
jsou	být	k5eAaImIp3nP	být
strach	strach	k1gInSc4	strach
<g/>
,	,	kIx,	,
že	že	k8xS	že
pacient	pacient	k1gMnSc1	pacient
vážně	vážně	k6eAd1	vážně
onemocní	onemocnět	k5eAaPmIp3nS	onemocnět
<g/>
,	,	kIx,	,
zraní	zranit	k5eAaPmIp3nS	zranit
se	se	k3xPyFc4	se
nebo	nebo	k8xC	nebo
ublíží	ublížit	k5eAaPmIp3nS	ublížit
sobě	se	k3xPyFc3	se
nebo	nebo	k8xC	nebo
ostatním	ostatní	k2eAgNnPc3d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
trpících	trpící	k2eAgFnPc2d1	trpící
obsesemi	obsese	k1gFnPc7	obsese
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
týkají	týkat	k5eAaImIp3nP	týkat
ublížení	ublížení	k1gNnSc2	ublížení
sobě	se	k3xPyFc3	se
nebo	nebo	k8xC	nebo
druhým	druhý	k4xOgNnSc7	druhý
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
daleko	daleko	k6eAd1	daleko
menší	malý	k2eAgFnSc1d2	menší
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc4	ten
opravdu	opravdu	k6eAd1	opravdu
udělali	udělat	k5eAaPmAgMnP	udělat
<g/>
,	,	kIx,	,
než	než	k8xS	než
u	u	k7c2	u
průměrné	průměrný	k2eAgFnSc2d1	průměrná
osoby	osoba	k1gFnSc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Kompulze	Kompulze	k6eAd1	Kompulze
je	být	k5eAaImIp3nS	být
jednání	jednání	k1gNnSc1	jednání
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
osoba	osoba	k1gFnSc1	osoba
trpící	trpící	k2eAgFnSc2d1	trpící
OCD	OCD	kA	OCD
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
opakovaně	opakovaně	k6eAd1	opakovaně
<g/>
,	,	kIx,	,
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zbavila	zbavit	k5eAaPmAgFnS	zbavit
obsese	obsese	k1gFnSc1	obsese
a	a	k8xC	a
úzkostí	úzkost	k1gFnSc7	úzkost
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
obsese	obsese	k1gFnPc1	obsese
se	se	k3xPyFc4	se
týkají	týkat	k5eAaImIp3nP	týkat
bakterií	bakterie	k1gFnPc2	bakterie
nebo	nebo	k8xC	nebo
kontaminace	kontaminace	k1gFnSc2	kontaminace
<g/>
,	,	kIx,	,
kompulze	kompulze	k6eAd1	kompulze
obvykle	obvykle	k6eAd1	obvykle
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
opakované	opakovaný	k2eAgNnSc1d1	opakované
čištění	čištění	k1gNnSc1	čištění
a	a	k8xC	a
mytí	mytí	k1gNnSc1	mytí
nebo	nebo	k8xC	nebo
úzkostlivé	úzkostlivý	k2eAgNnSc1d1	úzkostlivé
vyhýbání	vyhýbání	k1gNnSc1	vyhýbání
se	se	k3xPyFc4	se
odpadkům	odpadek	k1gInPc3	odpadek
a	a	k8xC	a
nepořádku	nepořádek	k1gInSc3	nepořádek
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklými	obvyklý	k2eAgFnPc7d1	obvyklá
kompulzemi	kompulze	k1gFnPc7	kompulze
jsou	být	k5eAaImIp3nP	být
kromě	kromě	k7c2	kromě
nadměrného	nadměrný	k2eAgNnSc2d1	nadměrné
mytí	mytí	k1gNnSc2	mytí
a	a	k8xC	a
čistění	čistění	k1gNnSc2	čistění
také	také	k9	také
shromažďování	shromažďování	k1gNnSc1	shromažďování
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
opakované	opakovaný	k2eAgNnSc1d1	opakované
dotýkání	dotýkání	k1gNnSc1	dotýkání
se	se	k3xPyFc4	se
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
počítání	počítání	k1gNnSc3	počítání
<g/>
,	,	kIx,	,
opakování	opakování	k1gNnSc3	opakování
slov	slovo	k1gNnPc2	slovo
a	a	k8xC	a
frází	fráze	k1gFnPc2	fráze
<g/>
,	,	kIx,	,
urovnávání	urovnávání	k1gNnSc1	urovnávání
věcí	věc	k1gFnPc2	věc
<g/>
...	...	k?	...
a	a	k8xC	a
další	další	k2eAgNnSc4d1	další
rituální	rituální	k2eAgNnSc4d1	rituální
chování	chování	k1gNnSc4	chování
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
pacient	pacient	k1gMnSc1	pacient
zbavuje	zbavovat	k5eAaImIp3nS	zbavovat
obsesivních	obsesivní	k2eAgFnPc2d1	obsesivní
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
.	.	kIx.	.
přítomnost	přítomnost	k1gFnSc1	přítomnost
obsesí	obsese	k1gFnPc2	obsese
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
kompulzí	kompulzí	k1gNnSc2	kompulzí
nebo	nebo	k8xC	nebo
obojího	obojí	k4xRgMnSc2	obojí
obsese	obsese	k1gFnSc1	obsese
pacient	pacient	k1gMnSc1	pacient
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
svoje	své	k1gNnSc4	své
vlastní	vlastní	k2eAgFnSc2d1	vlastní
myšlenky	myšlenka	k1gFnSc2	myšlenka
pacient	pacient	k1gMnSc1	pacient
považuje	považovat	k5eAaImIp3nS	považovat
obsese	obsese	k1gFnPc4	obsese
nebo	nebo	k8xC	nebo
kompulze	kompulze	k1gFnPc4	kompulze
za	za	k7c4	za
přehnané	přehnaný	k2eAgNnSc4d1	přehnané
a	a	k8xC	a
nesmyslné	smyslný	k2eNgNnSc4d1	nesmyslné
(	(	kIx(	(
<g/>
toto	tento	k3xDgNnSc1	tento
neplatí	platit	k5eNaImIp3nS	platit
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
<g/>
)	)	kIx)	)
obsese	obsese	k1gFnPc1	obsese
nebo	nebo	k8xC	nebo
kompulze	kompulze	k1gFnPc1	kompulze
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
nadměrnou	nadměrný	k2eAgFnSc4d1	nadměrná
úzkost	úzkost	k1gFnSc4	úzkost
<g/>
,	,	kIx,	,
zabírají	zabírat	k5eAaImIp3nP	zabírat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
hodinu	hodina	k1gFnSc4	hodina
denně	denně	k6eAd1	denně
nebo	nebo	k8xC	nebo
významně	významně	k6eAd1	významně
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
každodenní	každodenní	k2eAgInSc4d1	každodenní
život	život	k1gInSc4	život
a	a	k8xC	a
pracovní	pracovní	k2eAgNnSc4d1	pracovní
nebo	nebo	k8xC	nebo
sociální	sociální	k2eAgNnSc4d1	sociální
<g />
.	.	kIx.	.
</s>
<s>
aktivity	aktivita	k1gFnPc1	aktivita
a	a	k8xC	a
vztahy	vztah	k1gInPc1	vztah
obsese	obsese	k1gFnSc2	obsese
nebo	nebo	k8xC	nebo
kompulze	kompulze	k1gFnSc2	kompulze
nejsou	být	k5eNaImIp3nP	být
důsledkem	důsledek	k1gInSc7	důsledek
schizofrenie	schizofrenie	k1gFnSc2	schizofrenie
nebo	nebo	k8xC	nebo
poruchy	porucha	k1gFnSc2	porucha
nálady	nálada	k1gFnSc2	nálada
obsese	obsese	k1gFnSc2	obsese
nebo	nebo	k8xC	nebo
kompulze	kompulze	k1gFnSc2	kompulze
nejsou	být	k5eNaImIp3nP	být
důsledkem	důsledek	k1gInSc7	důsledek
jiné	jiný	k2eAgFnSc2d1	jiná
poruchy	porucha	k1gFnSc2	porucha
(	(	kIx(	(
<g/>
trichotilomanie	trichotilomanie	k1gFnPc1	trichotilomanie
<g/>
,	,	kIx,	,
poruchy	porucha	k1gFnPc1	porucha
příjmu	příjem	k1gInSc2	příjem
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
hypochondrie	hypochondrie	k1gFnSc1	hypochondrie
<g/>
)	)	kIx)	)
obsese	obsese	k1gFnSc1	obsese
nebo	nebo	k8xC	nebo
kompulze	kompulze	k1gFnSc1	kompulze
nejsou	být	k5eNaImIp3nP	být
důsledkem	důsledek	k1gInSc7	důsledek
užívání	užívání	k1gNnSc2	užívání
drog	droga	k1gFnPc2	droga
nebo	nebo	k8xC	nebo
léků	lék	k1gInPc2	lék
celoživotní	celoživotní	k2eAgFnSc2d1	celoživotní
prevalence	prevalence	k1gFnSc2	prevalence
<g/>
:	:	kIx,	:
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
%	%	kIx~	%
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
poměru	poměr	k1gInSc6	poměr
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
i	i	k9	i
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
Stephen	Stephen	k2eAgMnSc1d1	Stephen
King	King	k1gMnSc1	King
napsal	napsat	k5eAaBmAgInS	napsat
povídku	povídka	k1gFnSc4	povídka
N.	N.	kA	N.
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
publikována	publikovat	k5eAaBmNgFnS	publikovat
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
povídek	povídka	k1gFnPc2	povídka
Za	za	k7c2	za
Soumraku	soumrak	k1gInSc2	soumrak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
povídce	povídka	k1gFnSc6	povídka
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
nositelem	nositel	k1gMnSc7	nositel
OCD	OCD	kA	OCD
díky	díky	k7c3	díky
místu	místo	k1gNnSc3	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
údajně	údajně	k6eAd1	údajně
prolínají	prolínat	k5eAaImIp3nP	prolínat
paralelní	paralelní	k2eAgInPc4d1	paralelní
vesmíry	vesmír	k1gInPc4	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
OCD	OCD	kA	OCD
objevila	objevit	k5eAaPmAgFnS	objevit
např.	např.	kA	např.
ve	v	k7c6	v
filmech	film	k1gInPc6	film
Lepší	lepšit	k5eAaImIp3nS	lepšit
už	už	k6eAd1	už
to	ten	k3xDgNnSc1	ten
nebude	být	k5eNaImBp3nS	být
či	či	k8xC	či
Letec	letec	k1gMnSc1	letec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
jí	jíst	k5eAaImIp3nS	jíst
trpěl	trpět	k5eAaImAgMnS	trpět
hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
v	v	k7c6	v
podání	podání	k1gNnSc6	podání
Jacka	Jacek	k1gMnSc2	Jacek
Nicholsona	Nicholson	k1gMnSc2	Nicholson
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
Leonarda	Leonardo	k1gMnSc2	Leonardo
DiCapria	DiCaprium	k1gNnSc2	DiCaprium
<g/>
.	.	kIx.	.
</s>
<s>
Osoby	osoba	k1gFnPc1	osoba
s	s	k7c7	s
poruchou	porucha	k1gFnSc7	porucha
OCD	OCD	kA	OCD
můžeme	moct	k5eAaImIp1nP	moct
vidět	vidět	k5eAaImF	vidět
i	i	k9	i
v	v	k7c6	v
seriálech	seriál	k1gInPc6	seriál
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Můj	můj	k3xOp1gMnSc1	můj
přítel	přítel	k1gMnSc1	přítel
Monk	Monk	k1gMnSc1	Monk
v	v	k7c6	v
podání	podání	k1gNnSc6	podání
herce	herec	k1gMnSc2	herec
Tonyho	Tony	k1gMnSc2	Tony
Shalhouba	Shalhoub	k1gMnSc2	Shalhoub
nebo	nebo	k8xC	nebo
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Myšlenky	myšlenka	k1gFnSc2	myšlenka
zločince	zločinec	k1gMnPc4	zločinec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
vraha	vrah	k1gMnSc4	vrah
trpícího	trpící	k2eAgMnSc4d1	trpící
OCD	OCD	kA	OCD
zahrál	zahrát	k5eAaPmAgMnS	zahrát
Alex	Alex	k1gMnSc1	Alex
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Loughlin	Loughlina	k1gFnPc2	Loughlina
<g/>
.	.	kIx.	.
</s>
<s>
Postava	postava	k1gFnSc1	postava
trpící	trpící	k2eAgFnSc2d1	trpící
OCD	OCD	kA	OCD
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
seriálu	seriál	k1gInSc6	seriál
Glee	Gle	k1gFnSc2	Gle
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Emma	Emma	k1gFnSc1	Emma
Pillsbury	Pillsbura	k1gFnSc2	Pillsbura
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
Jayma	Jayma	k1gNnSc4	Jayma
Mays	Maysa	k1gFnPc2	Maysa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Teorie	teorie	k1gFnSc1	teorie
velkého	velký	k2eAgInSc2d1	velký
třesku	třesk	k1gInSc2	třesk
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
ústředních	ústřední	k2eAgFnPc2d1	ústřední
postav	postava	k1gFnPc2	postava
Sheldon	Sheldon	k1gMnSc1	Sheldon
Cooper	Cooper	k1gMnSc1	Cooper
opakuje	opakovat	k5eAaImIp3nS	opakovat
rituály	rituál	k1gInPc4	rituál
<g/>
,	,	kIx,	,
chová	chovat	k5eAaImIp3nS	chovat
se	se	k3xPyFc4	se
přehnaně	přehnaně	k6eAd1	přehnaně
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
situacích	situace	k1gFnPc6	situace
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
poruchou	porucha	k1gFnSc7	porucha
OCD	OCD	kA	OCD
označen	označen	k2eAgMnSc1d1	označen
svou	svůj	k3xOyFgFnSc7	svůj
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
Amy	Amy	k1gFnSc2	Amy
Farrah	Farrah	k1gInSc1	Farrah
Fowlerovou	Fowlerová	k1gFnSc4	Fowlerová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Kriminálka	kriminálka	k1gFnSc1	kriminálka
Miami	Miami	k1gNnSc2	Miami
trpí	trpět	k5eAaImIp3nS	trpět
OCD	OCD	kA	OCD
chemik	chemik	k1gMnSc1	chemik
Ryan	Ryan	k1gMnSc1	Ryan
Wolfe	Wolf	k1gMnSc5	Wolf
<g/>
.	.	kIx.	.
</s>
<s>
Projevuje	projevovat	k5eAaImIp3nS	projevovat
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
u	u	k7c2	u
něj	on	k3xPp3gInSc2	on
častým	častý	k2eAgNnSc7d1	časté
čištěním	čištění	k1gNnSc7	čištění
služebních	služební	k2eAgFnPc2d1	služební
zbraní	zbraň	k1gFnPc2	zbraň
a	a	k8xC	a
chová	chovat	k5eAaImIp3nS	chovat
se	se	k3xPyFc4	se
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
přehnaně	přehnaně	k6eAd1	přehnaně
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
postavou	postava	k1gFnSc7	postava
s	s	k7c7	s
OCD	OCD	kA	OCD
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Anger	Anger	k1gInSc1	Anger
Management	management	k1gInSc1	management
dcera	dcera	k1gFnSc1	dcera
Charlieho	Charlie	k1gMnSc2	Charlie
Goodsona	Goodsona	k1gFnSc1	Goodsona
<g/>
,	,	kIx,	,
Sam	Sam	k1gMnSc1	Sam
Goodsonová	Goodsonová	k1gFnSc1	Goodsonová
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
Daniela	Daniela	k1gFnSc1	Daniela
Bobadilla	Bobadilla	k1gFnSc1	Bobadilla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Chirurgové	chirurg	k1gMnPc1	chirurg
trpí	trpět	k5eAaImIp3nP	trpět
OCD	OCD	kA	OCD
doktorka	doktorka	k1gFnSc1	doktorka
Miranda	Miranda	k1gFnSc1	Miranda
Baileyová	Baileyová	k1gFnSc1	Baileyová
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Obsedantně	obsedantně	k6eAd1	obsedantně
kompulzivní	kompulzivnět	k5eAaPmIp3nS	kompulzivnět
porucha	porucha	k1gFnSc1	porucha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Zápisník	zápisník	k1gInSc1	zápisník
kluka	kluk	k1gMnSc2	kluk
s	s	k7c7	s
obsedantně	obsedantně	k6eAd1	obsedantně
kompulzivní	kompulzivní	k2eAgFnSc7d1	kompulzivní
poruchou	porucha	k1gFnSc7	porucha
</s>
