<s>
Ehrlichióza	Ehrlichióza	k1gFnSc1	Ehrlichióza
je	být	k5eAaImIp3nS	být
bakteriální	bakteriální	k2eAgNnSc4d1	bakteriální
onemocnění	onemocnění	k1gNnSc4	onemocnění
přenášené	přenášený	k2eAgFnSc2d1	přenášená
klíšťaty	klíště	k1gNnPc7	klíště
<g/>
.	.	kIx.	.
</s>
<s>
Původcem	původce	k1gMnSc7	původce
jsou	být	k5eAaImIp3nP	být
bakterie	bakterie	k1gFnSc1	bakterie
rodu	rod	k1gInSc2	rod
Ehrlichia	Ehrlichium	k1gNnSc2	Ehrlichium
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nové	nový	k2eAgFnSc2d1	nová
nomenklatury	nomenklatura	k1gFnSc2	nomenklatura
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
mikroorganizmus	mikroorganizmus	k1gInSc1	mikroorganizmus
správně	správně	k6eAd1	správně
nazývá	nazývat	k5eAaImIp3nS	nazývat
Anaplasma	Anaplasma	k1gFnSc1	Anaplasma
phagocytophila	phagocytophila	k1gFnSc1	phagocytophila
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
byly	být	k5eAaImAgInP	být
jako	jako	k8xC	jako
lidské	lidský	k2eAgInPc1d1	lidský
patogeny	patogen	k1gInPc1	patogen
identifikovány	identifikován	k2eAgInPc1d1	identifikován
tři	tři	k4xCgInPc1	tři
druhy	druh	k1gInPc1	druh
(	(	kIx(	(
<g/>
A.	A.	kA	A.
sennetsu	sennetsa	k1gFnSc4	sennetsa
<g/>
,	,	kIx,	,
A.	A.	kA	A.
chaffennsis	chaffennsis	k1gInSc1	chaffennsis
<g/>
,	,	kIx,	,
A.	A.	kA	A.
phagocytophila	phagocytophila	k1gFnSc1	phagocytophila
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ehrlichiemi	Ehrlichie	k1gFnPc7	Ehrlichie
jsou	být	k5eAaImIp3nP	být
napadeny	napaden	k2eAgInPc4d1	napaden
granulocyty	granulocyt	k1gInPc4	granulocyt
(	(	kIx(	(
<g/>
granulocytární	granulocytární	k2eAgFnSc1d1	granulocytární
ehrlichióza	ehrlichióza	k1gFnSc1	ehrlichióza
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
monocyty	monocyt	k1gInPc4	monocyt
(	(	kIx(	(
<g/>
monocytární	monocytární	k2eAgFnSc1d1	monocytární
ehrlichióza	ehrlichióza	k1gFnSc1	ehrlichióza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klinickými	klinický	k2eAgInPc7d1	klinický
příznaky	příznak	k1gInPc7	příznak
jsou	být	k5eAaImIp3nP	být
horečka	horečka	k1gFnSc1	horečka
<g/>
,	,	kIx,	,
bolest	bolest	k1gFnSc1	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
svalů	sval	k1gInPc2	sval
<g/>
,	,	kIx,	,
nauzea	nauzea	k1gFnSc1	nauzea
<g/>
,	,	kIx,	,
u	u	k7c2	u
monocytární	monocytární	k2eAgFnSc2d1	monocytární
ehrlichiózy	ehrlichióza	k1gFnSc2	ehrlichióza
bývá	bývat	k5eAaImIp3nS	bývat
přítomen	přítomen	k2eAgInSc1d1	přítomen
makulopapulózní	makulopapulózní	k2eAgInSc1d1	makulopapulózní
exantém	exantém	k1gInSc1	exantém
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
postiženy	postižen	k2eAgInPc1d1	postižen
vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
orgány	orgán	k1gInPc1	orgán
-	-	kIx~	-
nejčastěji	často	k6eAd3	často
plíce	plíce	k1gFnPc1	plíce
<g/>
,	,	kIx,	,
játra	játra	k1gNnPc1	játra
<g/>
,	,	kIx,	,
centrální	centrální	k2eAgFnSc1d1	centrální
nervová	nervový	k2eAgFnSc1d1	nervová
soustava	soustava	k1gFnSc1	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Diagnostika	diagnostika	k1gFnSc1	diagnostika
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
krevního	krevní	k2eAgInSc2d1	krevní
obrazu	obraz	k1gInSc2	obraz
(	(	kIx(	(
<g/>
leukopenie	leukopenie	k1gFnSc1	leukopenie
<g/>
,	,	kIx,	,
trombocytopenie	trombocytopenie	k1gFnSc1	trombocytopenie
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
anémie	anémie	k1gFnSc1	anémie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sérologického	sérologický	k2eAgNnSc2d1	sérologické
vyšetření	vyšetření	k1gNnSc2	vyšetření
<g/>
,	,	kIx,	,
eventuálně	eventuálně	k6eAd1	eventuálně
průkazem	průkaz	k1gInSc7	průkaz
DNA	dno	k1gNnSc2	dno
pomocí	pomoc	k1gFnPc2	pomoc
PCR	PCR	kA	PCR
<g/>
.	.	kIx.	.
</s>
<s>
Prozatím	prozatím	k6eAd1	prozatím
nejvíce	hodně	k6eAd3	hodně
případů	případ	k1gInPc2	případ
tohoto	tento	k3xDgNnSc2	tento
nově	nově	k6eAd1	nově
rozpoznaného	rozpoznaný	k2eAgNnSc2d1	rozpoznané
onemocnění	onemocnění	k1gNnSc2	onemocnění
bylo	být	k5eAaImAgNnS	být
diagnostikováno	diagnostikovat	k5eAaBmNgNnS	diagnostikovat
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
evropských	evropský	k2eAgFnPc6d1	Evropská
zemích	zem	k1gFnPc6	zem
jsou	být	k5eAaImIp3nP	být
dosud	dosud	k6eAd1	dosud
lidská	lidský	k2eAgNnPc1d1	lidské
onemocnění	onemocnění	k1gNnPc1	onemocnění
evidována	evidován	k2eAgFnSc1d1	evidována
spíše	spíše	k9	spíše
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobný	pravděpodobný	k2eAgInSc1d1	pravděpodobný
je	být	k5eAaImIp3nS	být
přenos	přenos	k1gInSc1	přenos
klíšťaty	klíště	k1gNnPc7	klíště
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
mezilidský	mezilidský	k2eAgInSc1d1	mezilidský
přenos	přenos	k1gInSc1	přenos
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
činí	činit	k5eAaImIp3nS	činit
14	[number]	k4	14
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
s	s	k7c7	s
rozpětím	rozpětí	k1gNnSc7	rozpětí
7	[number]	k4	7
až	až	k8xS	až
21	[number]	k4	21
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
imunologicky	imunologicky	k6eAd1	imunologicky
oslabených	oslabený	k2eAgFnPc2d1	oslabená
osob	osoba	k1gFnPc2	osoba
je	být	k5eAaImIp3nS	být
pozorován	pozorovat	k5eAaImNgInS	pozorovat
vážnější	vážní	k2eAgInSc1d2	vážnější
průběh	průběh	k1gInSc1	průběh
<g/>
.	.	kIx.	.
</s>
<s>
Vhodnou	vhodný	k2eAgFnSc7d1	vhodná
prevencí	prevence	k1gFnSc7	prevence
je	být	k5eAaImIp3nS	být
ochrana	ochrana	k1gFnSc1	ochrana
před	před	k7c7	před
klíšťaty	klíště	k1gNnPc7	klíště
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
včasné	včasný	k2eAgNnSc4d1	včasné
a	a	k8xC	a
správné	správný	k2eAgNnSc4d1	správné
odstranění	odstranění	k1gNnSc4	odstranění
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ehrlichióza	Ehrlichióza	k1gFnSc1	Ehrlichióza
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
-kliste	list	k1gMnSc5	-klist
<g/>
.	.	kIx.	.
<g/>
cz-	cz-	k?	cz-
Informační	informační	k2eAgInSc1d1	informační
web	web	k1gInSc1	web
o	o	k7c6	o
klíšťatech	klíště	k1gNnPc6	klíště
a	a	k8xC	a
nemocí	nemoc	k1gFnPc2	nemoc
jimi	on	k3xPp3gMnPc7	on
přenášených	přenášený	k2eAgNnPc6d1	přenášené
</s>
