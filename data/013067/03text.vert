<p>
<s>
Hearthstone	Hearthston	k1gInSc5	Hearthston
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Hearthstone	Hearthston	k1gInSc5	Hearthston
<g/>
:	:	kIx,	:
Heroes	Heroes	k1gInSc1	Heroes
of	of	k?	of
Warcraft	Warcraft	k1gInSc1	Warcraft
<g/>
)	)	kIx)	)
je	on	k3xPp3gNnSc4	on
online	onlinout	k5eAaPmIp3nS	onlinout
virtuální	virtuální	k2eAgFnSc1d1	virtuální
karetní	karetní	k2eAgFnSc1d1	karetní
hra	hra	k1gFnSc1	hra
vyvinutá	vyvinutý	k2eAgFnSc1d1	vyvinutá
společností	společnost	k1gFnSc7	společnost
Blizzard	Blizzard	k1gMnSc1	Blizzard
Entertainment	Entertainment	k1gMnSc1	Entertainment
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zdarma	zdarma	k6eAd1	zdarma
dostupná	dostupný	k2eAgFnSc1d1	dostupná
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
free-to-play	freeolaa	k1gFnSc2	free-to-plaa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
koupě	koupě	k1gFnSc2	koupě
karet	kareta	k1gFnPc2	kareta
pro	pro	k7c4	pro
rychlejší	rychlý	k2eAgInSc4d2	rychlejší
postup	postup	k1gInSc4	postup
hrou	hra	k1gFnSc7	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hra	hra	k1gFnSc1	hra
byla	být	k5eAaImAgFnS	být
oznámena	oznámit	k5eAaPmNgFnS	oznámit
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2013	[number]	k4	2013
a	a	k8xC	a
spuštěna	spuštěn	k2eAgFnSc1d1	spuštěna
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Hearthstone	Hearthston	k1gInSc5	Hearthston
je	být	k5eAaImIp3nS	být
dostupný	dostupný	k2eAgInSc1d1	dostupný
na	na	k7c6	na
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
a	a	k8xC	a
OS	OS	kA	OS
X	X	kA	X
systémech	systém	k1gInPc6	systém
iOS	iOS	k?	iOS
a	a	k8xC	a
Androidových	Androidový	k2eAgNnPc2d1	Androidový
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2018	[number]	k4	2018
překročil	překročit	k5eAaPmAgInS	překročit
počet	počet	k1gInSc1	počet
registrovaných	registrovaný	k2eAgMnPc2d1	registrovaný
hráčů	hráč	k1gMnPc2	hráč
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Herní	herní	k2eAgInSc1d1	herní
princip	princip	k1gInSc1	princip
==	==	k?	==
</s>
</p>
<p>
<s>
Hráč	hráč	k1gMnSc1	hráč
si	se	k3xPyFc3	se
sestaví	sestavit	k5eAaPmIp3nS	sestavit
balíček	balíček	k1gInSc4	balíček
30	[number]	k4	30
karet	kareta	k1gFnPc2	kareta
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yQgInPc7	který
hraje	hrát	k5eAaImIp3nS	hrát
online	onlinout	k5eAaPmIp3nS	onlinout
proti	proti	k7c3	proti
protihráči	protihráč	k1gMnSc3	protihráč
podobné	podobný	k2eAgFnSc2d1	podobná
úrovně	úroveň	k1gFnSc2	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
si	se	k3xPyFc3	se
může	moct	k5eAaImIp3nS	moct
vybrat	vybrat	k5eAaPmF	vybrat
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
9	[number]	k4	9
hrdinů	hrdina	k1gMnPc2	hrdina
(	(	kIx(	(
<g/>
Druid	Druid	k1gMnSc1	Druid
<g/>
,	,	kIx,	,
Shaman	Shaman	k1gMnSc1	Shaman
<g/>
,	,	kIx,	,
Mage	Mage	k1gFnSc1	Mage
<g/>
,	,	kIx,	,
Warrior	Warrior	k1gMnSc1	Warrior
<g/>
,	,	kIx,	,
Paladin	paladin	k1gMnSc1	paladin
<g/>
,	,	kIx,	,
Rogue	Rogue	k1gFnSc1	Rogue
<g/>
,	,	kIx,	,
Hunter	Hunter	k1gMnSc1	Hunter
<g/>
,	,	kIx,	,
Priest	Priest	k1gMnSc1	Priest
<g/>
,	,	kIx,	,
Warlock	Warlock	k1gMnSc1	Warlock
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
má	mít	k5eAaImIp3nS	mít
každý	každý	k3xTgMnSc1	každý
své	svůj	k3xOyFgFnPc4	svůj
speciální	speciální	k2eAgFnPc4d1	speciální
karty	karta	k1gFnPc4	karta
a	a	k8xC	a
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
hry	hra	k1gFnSc2	hra
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
náhodným	náhodný	k2eAgInSc7d1	náhodný
losem	los	k1gInSc7	los
začínající	začínající	k2eAgMnSc1d1	začínající
hráč	hráč	k1gMnSc1	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgMnSc1	druhý
hráč	hráč	k1gMnSc1	hráč
dostane	dostat	k5eAaPmIp3nS	dostat
jako	jako	k8xC	jako
kompenzaci	kompenzace	k1gFnSc4	kompenzace
kartu	karta	k1gFnSc4	karta
navíc	navíc	k6eAd1	navíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
hry	hra	k1gFnSc2	hra
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vykládat	vykládat	k5eAaImF	vykládat
pouze	pouze	k6eAd1	pouze
karty	karta	k1gFnPc4	karta
s	s	k7c7	s
cenou	cena	k1gFnSc7	cena
1	[number]	k4	1
many	mana	k1gFnPc4	mana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgNnSc6	každý
dalším	další	k2eAgNnSc6d1	další
kole	kolo	k1gNnSc6	kolo
je	být	k5eAaImIp3nS	být
maximální	maximální	k2eAgFnSc1d1	maximální
hodnota	hodnota	k1gFnSc1	hodnota
"	"	kIx"	"
<g/>
hratelné	hratelný	k2eAgFnSc2d1	hratelná
<g/>
"	"	kIx"	"
many	mana	k1gFnSc2	mana
navýšena	navýšit	k5eAaPmNgFnS	navýšit
vždy	vždy	k6eAd1	vždy
o	o	k7c4	o
1	[number]	k4	1
(	(	kIx(	(
<g/>
druhé	druhý	k4xOgNnSc1	druhý
kolo	kolo	k1gNnSc1	kolo
má	mít	k5eAaImIp3nS	mít
tedy	tedy	k8xC	tedy
hráč	hráč	k1gMnSc1	hráč
2	[number]	k4	2
many	mana	k1gFnSc2	mana
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgNnSc1	třetí
kolo	kolo	k1gNnSc1	kolo
3	[number]	k4	3
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
dále	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
maxima	maximum	k1gNnSc2	maximum
10	[number]	k4	10
many	mana	k1gFnSc2	mana
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
růst	růst	k1gInSc1	růst
zastaví	zastavit	k5eAaPmIp3nS	zastavit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hráči	hráč	k1gMnPc1	hráč
se	se	k3xPyFc4	se
střídají	střídat	k5eAaImIp3nP	střídat
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
tazích	tag	k1gInPc6	tag
a	a	k8xC	a
vykládají	vykládat	k5eAaImIp3nP	vykládat
vždy	vždy	k6eAd1	vždy
karty	karta	k1gFnPc1	karta
s	s	k7c7	s
cenou	cena	k1gFnSc7	cena
many	mana	k1gFnSc2	mana
nepřevyšující	převyšující	k2eNgFnSc4d1	nepřevyšující
maximální	maximální	k2eAgFnSc4d1	maximální
možnou	možný	k2eAgFnSc4d1	možná
hodnotu	hodnota	k1gFnSc4	hodnota
v	v	k7c6	v
daném	daný	k2eAgNnSc6d1	dané
kole	kolo	k1gNnSc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
je	být	k5eAaImIp3nS	být
umožněno	umožnit	k5eAaPmNgNnS	umožnit
hrát	hrát	k5eAaImF	hrát
v	v	k7c6	v
každém	každý	k3xTgNnSc6	každý
postupujícím	postupující	k2eAgNnSc6d1	postupující
kole	kolo	k1gNnSc6	kolo
silnější	silný	k2eAgFnSc2d2	silnější
karty	karta	k1gFnSc2	karta
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
karet	kareta	k1gFnPc2	kareta
s	s	k7c7	s
nižší	nízký	k2eAgFnSc7d2	nižší
cenou	cena	k1gFnSc7	cena
many	mana	k1gFnSc2	mana
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
nového	nový	k2eAgNnSc2d1	nové
kola	kolo	k1gNnSc2	kolo
obdrží	obdržet	k5eAaPmIp3nS	obdržet
každý	každý	k3xTgMnSc1	každý
hráč	hráč	k1gMnSc1	hráč
po	po	k7c6	po
jedné	jeden	k4xCgFnSc6	jeden
kartě	karta	k1gFnSc6	karta
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
balíku	balík	k1gInSc2	balík
<g/>
.	.	kIx.	.
</s>
<s>
Hraním	hranit	k5eAaImIp1nS	hranit
karet	kareta	k1gFnPc2	kareta
si	se	k3xPyFc3	se
hráči	hráč	k1gMnPc1	hráč
staví	stavit	k5eAaPmIp3nP	stavit
linii	linie	k1gFnSc4	linie
bojovníků	bojovník	k1gMnPc2	bojovník
a	a	k8xC	a
monster	monstrum	k1gNnPc2	monstrum
<g/>
,	,	kIx,	,
kterými	který	k3yQgFnPc7	který
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
kolech	kolo	k1gNnPc6	kolo
útočit	útočit	k5eAaImF	útočit
na	na	k7c4	na
soupeře	soupeř	k1gMnSc4	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
hrát	hrát	k5eAaImF	hrát
karty	karta	k1gFnPc4	karta
s	s	k7c7	s
kouzly	kouzlo	k1gNnPc7	kouzlo
-	-	kIx~	-
těmi	ten	k3xDgMnPc7	ten
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
útočí	útočit	k5eAaImIp3nS	útočit
(	(	kIx(	(
<g/>
nevykládají	vykládat	k5eNaImIp3nP	vykládat
se	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Útok	útok	k1gInSc1	útok
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
buď	buď	k8xC	buď
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
soupeřovu	soupeřův	k2eAgFnSc4d1	soupeřova
vyloženou	vyložený	k2eAgFnSc4d1	vyložená
kartu	karta	k1gFnSc4	karta
(	(	kIx(	(
<g/>
bojovníka	bojovník	k1gMnSc2	bojovník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
hlavního	hlavní	k2eAgMnSc4d1	hlavní
hrdinu	hrdina	k1gMnSc4	hrdina
soupeře	soupeř	k1gMnSc4	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrává	vyhrávat	k5eAaImIp3nS	vyhrávat
ten	ten	k3xDgMnSc1	ten
hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dříve	dříve	k6eAd2	dříve
zničí	zničit	k5eAaPmIp3nS	zničit
nepřátelského	přátelský	k2eNgMnSc4d1	nepřátelský
hrdinu	hrdina	k1gMnSc4	hrdina
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
udělí	udělit	k5eAaPmIp3nS	udělit
poškození	poškození	k1gNnSc4	poškození
30	[number]	k4	30
životů	život	k1gInPc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Herní	herní	k2eAgFnSc2d1	herní
módy	móda	k1gFnSc2	móda
==	==	k?	==
</s>
</p>
<p>
<s>
Hra	hra	k1gFnSc1	hra
má	mít	k5eAaImIp3nS	mít
4	[number]	k4	4
herní	herní	k2eAgFnSc2d1	herní
módy	móda	k1gFnSc2	móda
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Play	play	k0	play
-	-	kIx~	-
Běžná	běžný	k2eAgFnSc1d1	běžná
hra	hra	k1gFnSc1	hra
proti	proti	k7c3	proti
hráči	hráč	k1gMnSc3	hráč
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
Casual	Casual	k1gInSc4	Casual
(	(	kIx(	(
<g/>
běžná	běžný	k2eAgFnSc1d1	běžná
hra	hra	k1gFnSc1	hra
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ranked	Ranked	k1gMnSc1	Ranked
(	(	kIx(	(
<g/>
hráč	hráč	k1gMnSc1	hráč
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
o	o	k7c4	o
co	co	k9	co
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
výsledky	výsledek	k1gInPc4	výsledek
<g/>
,	,	kIx,	,
za	za	k7c4	za
které	který	k3yIgFnPc4	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
měsíce	měsíc	k1gInSc2	měsíc
odměněn	odměnit	k5eAaPmNgInS	odměnit
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Wild	Wild	k1gInSc1	Wild
-	-	kIx~	-
Dají	dát	k5eAaPmIp3nP	dát
se	se	k3xPyFc4	se
používat	používat	k5eAaImF	používat
všechny	všechen	k3xTgFnPc4	všechen
karty	karta	k1gFnPc4	karta
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
kdy	kdy	k6eAd1	kdy
vyšly	vyjít	k5eAaPmAgFnP	vyjít
</s>
</p>
<p>
<s>
Standard	standard	k1gInSc1	standard
-	-	kIx~	-
Jdou	jít	k5eAaImIp3nP	jít
používat	používat	k5eAaImF	používat
pouze	pouze	k6eAd1	pouze
karty	karta	k1gFnPc4	karta
mladší	mladý	k2eAgFnPc4d2	mladší
zhruba	zhruba	k6eAd1	zhruba
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
</s>
</p>
<p>
<s>
Solo	Solo	k1gMnSc1	Solo
Adventures	Adventures	k1gMnSc1	Adventures
-	-	kIx~	-
Hráč	hráč	k1gMnSc1	hráč
hraje	hrát	k5eAaImIp3nS	hrát
sám	sám	k3xTgMnSc1	sám
proti	proti	k7c3	proti
NPC	NPC	kA	NPC
<g/>
,	,	kIx,	,
at	at	k?	at
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
Boss	boss	k1gMnSc1	boss
(	(	kIx(	(
<g/>
který	který	k3yRgMnSc1	který
má	mít	k5eAaImIp3nS	mít
speciální	speciální	k2eAgFnPc4d1	speciální
karty	karta	k1gFnPc4	karta
<g/>
,	,	kIx,	,
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
porážku	porážka	k1gFnSc4	porážka
dostává	dostávat	k5eAaImIp3nS	dostávat
hráč	hráč	k1gMnSc1	hráč
většinou	většinou	k6eAd1	většinou
odměnu	odměna	k1gFnSc4	odměna
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
normální	normální	k2eAgMnSc1d1	normální
hrdina	hrdina	k1gMnSc1	hrdina
(	(	kIx(	(
<g/>
trénink	trénink	k1gInSc1	trénink
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Aréna	aréna	k1gFnSc1	aréna
(	(	kIx(	(
<g/>
zpoplatněna	zpoplatněn	k2eAgFnSc1d1	zpoplatněna
150	[number]	k4	150
goldy	goldy	k6eAd1	goldy
nebo	nebo	k8xC	nebo
dvěma	dva	k4xCgNnPc7	dva
Eury	euro	k1gNnPc7	euro
za	za	k7c4	za
vstup	vstup	k1gInSc4	vstup
<g/>
)	)	kIx)	)
-	-	kIx~	-
Hráč	hráč	k1gMnSc1	hráč
si	se	k3xPyFc3	se
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
balíček	balíček	k1gInSc4	balíček
z	z	k7c2	z
nabídnutých	nabídnutý	k2eAgFnPc2d1	nabídnutá
karet	kareta	k1gFnPc2	kareta
a	a	k8xC	a
hraje	hrát	k5eAaImIp3nS	hrát
proti	proti	k7c3	proti
nepříteli	nepřítel	k1gMnSc3	nepřítel
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
více	hodně	k6eAd2	hodně
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
schopnostech	schopnost	k1gFnPc6	schopnost
hráče	hráč	k1gMnSc2	hráč
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
hra	hra	k1gFnSc1	hra
výše	vysoce	k6eAd2	vysoce
ohodnocena	ohodnocen	k2eAgFnSc1d1	ohodnocena
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tavern	Tavern	k1gMnSc1	Tavern
Brawl	Brawl	k1gMnSc1	Brawl
(	(	kIx(	(
<g/>
Je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
mít	mít	k5eAaImF	mít
alespoň	alespoň	k9	alespoň
jednoho	jeden	k4xCgMnSc4	jeden
hrdinu	hrdina	k1gMnSc4	hrdina
na	na	k7c6	na
levelu	level	k1gInSc6	level
20	[number]	k4	20
nebo	nebo	k8xC	nebo
výše	vysoce	k6eAd2	vysoce
<g/>
)	)	kIx)	)
-	-	kIx~	-
Hráči	hráč	k1gMnPc1	hráč
mají	mít	k5eAaImIp3nP	mít
předvoleného	předvolený	k2eAgMnSc4d1	předvolený
hrdinu	hrdina	k1gMnSc4	hrdina
a	a	k8xC	a
balíček	balíček	k1gInSc4	balíček
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
si	se	k3xPyFc3	se
musí	muset	k5eAaImIp3nS	muset
sestavit	sestavit	k5eAaPmF	sestavit
balíček	balíček	k1gInSc1	balíček
podle	podle	k7c2	podle
kritérií	kritérion	k1gNnPc2	kritérion
určených	určený	k2eAgInPc2d1	určený
samotnými	samotný	k2eAgMnPc7d1	samotný
tvůrci	tvůrce	k1gMnPc7	tvůrce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Tavern	Tavern	k1gNnSc4	Tavern
Brawl	Brawla	k1gFnPc2	Brawla
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
zcela	zcela	k6eAd1	zcela
měnit	měnit	k5eAaImF	měnit
pravidla	pravidlo	k1gNnPc4	pravidlo
her	hra	k1gFnPc2	hra
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
o	o	k7c4	o
mód	mód	k1gInSc4	mód
pro	pro	k7c4	pro
zábavu	zábava	k1gFnSc4	zábava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Herní	herní	k2eAgNnSc4d1	herní
rozšíření	rozšíření	k1gNnSc4	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Hra	hra	k1gFnSc1	hra
už	už	k6eAd1	už
byla	být	k5eAaImAgFnS	být
několikrát	několikrát	k6eAd1	několikrát
vylepšena	vylepšit	k5eAaPmNgFnS	vylepšit
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
obsahová	obsahový	k2eAgNnPc4d1	obsahové
rozšíření	rozšíření	k1gNnPc4	rozšíření
patří	patřit	k5eAaImIp3nS	patřit
Curse	Curse	k1gFnSc1	Curse
of	of	k?	of
Naxramas	Naxramas	k1gMnSc1	Naxramas
<g/>
,	,	kIx,	,
Blackrock	Blackrock	k1gMnSc1	Blackrock
Moutain	Moutain	k1gMnSc1	Moutain
<g/>
,	,	kIx,	,
League	League	k1gFnSc1	League
of	of	k?	of
Explorers	Explorers	k1gInSc1	Explorers
<g/>
,	,	kIx,	,
One	One	k1gMnSc1	One
night	night	k1gMnSc1	night
in	in	k?	in
Karazhan	Karazhan	k1gInSc1	Karazhan
(	(	kIx(	(
<g/>
soupeření	soupeření	k1gNnSc1	soupeření
s	s	k7c7	s
NPC	NPC	kA	NPC
bossy	boss	k1gMnPc7	boss
<g/>
)	)	kIx)	)
a	a	k8xC	a
Goblins	Goblins	k1gInSc1	Goblins
vs	vs	k?	vs
Gnomes	Gnomes	k1gMnSc1	Gnomes
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Grand	grand	k1gMnSc1	grand
Tournament	Tournament	k1gMnSc1	Tournament
<g/>
,	,	kIx,	,
Whispers	Whispers	k1gInSc1	Whispers
of	of	k?	of
the	the	k?	the
Old	Olda	k1gFnPc2	Olda
Gods	Gods	k1gInSc1	Gods
<g/>
,	,	kIx,	,
Mean	Mean	k1gInSc1	Mean
Streets	Streets	k1gInSc1	Streets
of	of	k?	of
Gadzetan	Gadzetan	k1gInSc1	Gadzetan
<g/>
,	,	kIx,	,
Journey	Journe	k2eAgMnPc4d1	Journe
to	ten	k3xDgNnSc1	ten
Un	Un	k1gFnPc1	Un
<g/>
'	'	kIx"	'
<g/>
Goro	Goro	k1gNnSc1	Goro
<g/>
,	,	kIx,	,
Knights	Knights	k1gInSc1	Knights
of	of	k?	of
the	the	k?	the
Frozen	Frozna	k1gFnPc2	Frozna
Throne	Thron	k1gInSc5	Thron
<g/>
,	,	kIx,	,
Kobolds	Kobolds	k1gInSc1	Kobolds
and	and	k?	and
Catacombs	Catacombs	k1gInSc1	Catacombs
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Witchwood	Witchwood	k1gInSc1	Witchwood
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
The	The	k?	The
Boomsday	Boomsday	k1gInPc1	Boomsday
Project	Projectum	k1gNnPc2	Projectum
<g/>
,	,	kIx,	,
Rastakhan	Rastakhan	k1gInSc1	Rastakhan
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Rumble	Rumble	k1gFnSc7	Rumble
a	a	k8xC	a
Rise	Rise	k1gFnSc7	Rise
of	of	k?	of
Shadows	Shadowsa	k1gFnPc2	Shadowsa
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
