<s>
Hospodářský	hospodářský	k2eAgInSc1d1
cyklus	cyklus	k1gInSc1
</s>
<s>
Hospodářský	hospodářský	k2eAgInSc1d1
cyklus	cyklus	k1gInSc1
označuje	označovat	k5eAaImIp3nS
kolísání	kolísání	k1gNnSc4
ekonomické	ekonomický	k2eAgFnSc2d1
aktivity	aktivita	k1gFnSc2
na	na	k7c6
úrovni	úroveň	k1gFnSc6
celé	celý	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
nebo	nebo	k8xC
její	její	k3xOp3gFnSc2
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
(	(	kIx(
<g/>
nikoliv	nikoliv	k9
pouze	pouze	k6eAd1
v	v	k7c6
určitém	určitý	k2eAgInSc6d1
sektoru	sektor	k1gInSc6
<g/>
)	)	kIx)
okolo	okolo	k7c2
dlouhodobého	dlouhodobý	k2eAgInSc2d1
trendu	trend	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Střídání	střídání	k1gNnSc1
relativního	relativní	k2eAgInSc2d1
ekonomického	ekonomický	k2eAgInSc2d1
růstu	růst	k1gInSc2
a	a	k8xC
relativního	relativní	k2eAgInSc2d1
poklesu	pokles	k1gInSc2
je	být	k5eAaImIp3nS
nazýváno	nazývat	k5eAaImNgNnS
cyklem	cyklus	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
kolísání	kolísání	k1gNnSc1
je	být	k5eAaImIp3nS
často	často	k6eAd1
měřeno	měřit	k5eAaImNgNnS
pomocí	pomocí	k7c2
hrubého	hrubý	k2eAgInSc2d1
domácího	domácí	k2eAgInSc2d1
produktu	produkt	k1gInSc2
a	a	k8xC
dalších	další	k2eAgInPc2d1
makroekonomických	makroekonomický	k2eAgInPc2d1
indikátorů	indikátor	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Délka	délka	k1gFnSc1
hospodářského	hospodářský	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
se	se	k3xPyFc4
při	při	k7c6
různých	různý	k2eAgFnPc6d1
příležitostech	příležitost	k1gFnPc6
liší	lišit	k5eAaImIp3nP
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
jeho	jeho	k3xOp3gInSc6
původu	původ	k1gInSc6
<g/>
,	,	kIx,
ekonomice	ekonomika	k1gFnSc6
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
k	k	k7c3
němu	on	k3xPp3gInSc3
dochází	docházet	k5eAaImIp3nS
<g/>
,	,	kIx,
a	a	k8xC
dalších	další	k2eAgInPc6d1
četných	četný	k2eAgInPc6d1
faktorech	faktor	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
ukončení	ukončení	k1gNnSc6
jednoho	jeden	k4xCgInSc2
cyklu	cyklus	k1gInSc2
ekonomika	ekonomik	k1gMnSc2
plynule	plynule	k6eAd1
přechází	přecházet	k5eAaImIp3nS
do	do	k7c2
druhého	druhý	k4xOgNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
zkoumání	zkoumání	k1gNnSc6
hospodářského	hospodářský	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
rozlišovat	rozlišovat	k5eAaImF
mezi	mezi	k7c7
cyklickými	cyklický	k2eAgInPc7d1
jevy	jev	k1gInPc7
a	a	k8xC
běžnými	běžný	k2eAgFnPc7d1
fluktuacemi	fluktuace	k1gFnPc7
ekonomiky	ekonomika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
lze	lze	k6eAd1
identifikovat	identifikovat	k5eAaBmF
alespoň	alespoň	k9
dva	dva	k4xCgMnPc1
po	po	k7c6
sobě	sebe	k3xPyFc6
jdoucí	jdoucí	k2eAgFnPc1d1
cykly	cyklus	k1gInPc4
<g/>
,	,	kIx,
lze	lze	k6eAd1
hovořit	hovořit	k5eAaImF
o	o	k7c6
cyklickém	cyklický	k2eAgInSc6d1
vývoji	vývoj	k1gInSc6
dané	daný	k2eAgFnSc2d1
veličiny	veličina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Cyklus	cyklus	k1gInSc1
vs	vs	k?
fluktuace	fluktuace	k1gFnSc1
</s>
<s>
Při	při	k7c6
sledování	sledování	k1gNnSc6
výkyvů	výkyv	k1gInPc2
ekonomiky	ekonomika	k1gFnSc2
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
rozlišovat	rozlišovat	k5eAaImF
mezi	mezi	k7c7
hospodářským	hospodářský	k2eAgInSc7d1
cyklem	cyklus	k1gInSc7
(	(	kIx(
<g/>
tj.	tj.	kA
cyklicky	cyklicky	k6eAd1
se	se	k3xPyFc4
opakujícími	opakující	k2eAgInPc7d1
jevy	jev	k1gInPc7
<g/>
)	)	kIx)
a	a	k8xC
izolovanými	izolovaný	k2eAgFnPc7d1
hospodářskými	hospodářský	k2eAgFnPc7d1
fluktuacemi	fluktuace	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fluktuace	fluktuace	k1gFnSc1
se	se	k3xPyFc4
objevují	objevovat	k5eAaImIp3nP
jednotlivě	jednotlivě	k6eAd1
a	a	k8xC
náhodně	náhodně	k6eAd1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
cyklus	cyklus	k1gInSc1
přichází	přicházet	k5eAaImIp3nS
opakovaně	opakovaně	k6eAd1
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
někdy	někdy	k6eAd1
v	v	k7c6
nepravidelných	pravidelný	k2eNgInPc6d1
intervalech	interval	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Cyklus	cyklus	k1gInSc1
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1
znakem	znak	k1gInSc7
hospodářského	hospodářský	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
je	být	k5eAaImIp3nS
pravidelné	pravidelný	k2eAgNnSc4d1
opakování	opakování	k1gNnSc4
ekonomického	ekonomický	k2eAgInSc2d1
růstu	růst	k1gInSc2
a	a	k8xC
následného	následný	k2eAgInSc2d1
poklesu	pokles	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
dáno	dán	k2eAgNnSc1d1
přirozenou	přirozený	k2eAgFnSc7d1
nebo	nebo	k8xC
umělou	umělý	k2eAgFnSc7d1
periodicitou	periodicita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Přirozená	přirozený	k2eAgFnSc1d1
periodicita	periodicita	k1gFnSc1
</s>
<s>
Typický	typický	k2eAgInSc1d1
je	být	k5eAaImIp3nS
vliv	vliv	k1gInSc1
ročního	roční	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
<g/>
:	:	kIx,
Jednoznačně	jednoznačně	k6eAd1
je	být	k5eAaImIp3nS
patrný	patrný	k2eAgInSc1d1
například	například	k6eAd1
na	na	k7c6
stavebnictví	stavebnictví	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čím	co	k3yRnSc7,k3yInSc7,k3yQnSc7
kratší	krátký	k2eAgInPc1d2
cykly	cyklus	k1gInPc1
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
lokálnější	lokální	k2eAgInPc1d2
mají	mít	k5eAaImIp3nP
charakter	charakter	k1gInSc4
<g/>
:	:	kIx,
Globální	globální	k2eAgInSc4d1
systém	systém	k1gInSc4
má	mít	k5eAaImIp3nS
příliš	příliš	k6eAd1
velkou	velký	k2eAgFnSc4d1
setrvačnost	setrvačnost	k1gFnSc4
<g/>
,	,	kIx,
než	než	k8xS
aby	aby	kYmCp3nS
se	se	k3xPyFc4
na	na	k7c6
něm	on	k3xPp3gMnSc6
projevily	projevit	k5eAaPmAgFnP
<g/>
,	,	kIx,
spíše	spíše	k9
se	se	k3xPyFc4
vyruší	vyrušit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
místně	místně	k6eAd1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
právě	právě	k9
krátké	krátký	k2eAgInPc4d1
vlivy	vliv	k1gInPc4
značné	značný	k2eAgInPc4d1
<g/>
:	:	kIx,
Hospodský	hospodský	k1gMnSc1
snadno	snadno	k6eAd1
pozná	poznat	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
ve	v	k7c6
vedlejší	vedlejší	k2eAgFnSc6d1
továrně	továrna	k1gFnSc6
opět	opět	k6eAd1
měli	mít	k5eAaImAgMnP
výplatní	výplatní	k2eAgInSc4d1
den	den	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
celkové	celkový	k2eAgNnSc4d1
zkoumání	zkoumání	k1gNnSc4
tedy	tedy	k9
stojí	stát	k5eAaImIp3nS
pouze	pouze	k6eAd1
dlouhodobé	dlouhodobý	k2eAgInPc4d1
cykly	cyklus	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ekonomika	ekonomika	k1gFnSc1
se	se	k3xPyFc4
typicky	typicky	k6eAd1
zkoumá	zkoumat	k5eAaImIp3nS
zvnějšku	zvnějšku	k6eAd1
<g/>
,	,	kIx,
pozorováním	pozorování	k1gNnSc7
jejího	její	k3xOp3gNnSc2
chování	chování	k1gNnSc2
<g/>
,	,	kIx,
její	její	k3xOp3gInSc1
vnitřní	vnitřní	k2eAgInSc1d1
model	model	k1gInSc1
není	být	k5eNaImIp3nS
znám	znám	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
postupovat	postupovat	k5eAaImF
buď	buď	k8xC
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
k	k	k7c3
pozorovanému	pozorovaný	k2eAgNnSc3d1
chování	chování	k1gNnSc3
zpětně	zpětně	k6eAd1
dohledávají	dohledávat	k5eAaImIp3nP
příčiny	příčina	k1gFnPc4
(	(	kIx(
<g/>
neočekávaný	očekávaný	k2eNgInSc1d1
pohyb	pohyb	k1gInSc1
cen	cena	k1gFnPc2
komodit	komodita	k1gFnPc2
<g/>
,	,	kIx,
zdánlivě	zdánlivě	k6eAd1
bez	bez	k7c2
příčiny	příčina	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
nejdříve	dříve	k6eAd3
formuluje	formulovat	k5eAaImIp3nS
hypotéza	hypotéza	k1gFnSc1
o	o	k7c6
budoucích	budoucí	k2eAgInPc6d1
důsledcích	důsledek	k1gInPc6
známé	známý	k2eAgFnSc2d1
změny	změna	k1gFnSc2
a	a	k8xC
následně	následně	k6eAd1
se	se	k3xPyFc4
tato	tento	k3xDgFnSc1
empiricky	empiricky	k6eAd1
ověřuje	ověřovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
zjištění	zjištění	k1gNnSc4
citlivosti	citlivost	k1gFnSc2
konkrétních	konkrétní	k2eAgInPc2d1
parametrů	parametr	k1gInPc2
ekonomiky	ekonomika	k1gFnSc2
na	na	k7c4
konkrétní	konkrétní	k2eAgInPc4d1
jevy	jev	k1gInPc4
a	a	k8xC
na	na	k7c6
základě	základ	k1gInSc6
toho	ten	k3xDgNnSc2
předvídat	předvídat	k5eAaImF
výsledek	výsledek	k1gInSc4
pro	pro	k7c4
příští	příští	k2eAgFnPc4d1
podobné	podobný	k2eAgFnPc4d1
situace	situace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Jedním	jeden	k4xCgInSc7
z	z	k7c2
předpokládaných	předpokládaný	k2eAgInPc2d1
vlivů	vliv	k1gInPc2
je	být	k5eAaImIp3nS
jedenáctiletý	jedenáctiletý	k2eAgInSc1d1
Sluneční	sluneční	k2eAgInSc1d1
cyklus	cyklus	k1gInSc1
<g/>
:	:	kIx,
Vliv	vliv	k1gInSc1
na	na	k7c4
ekonomiku	ekonomika	k1gFnSc4
má	mít	k5eAaImIp3nS
přicházet	přicházet	k5eAaImF
na	na	k7c6
jedné	jeden	k4xCgFnSc6
straně	strana	k1gFnSc6
přes	přes	k7c4
zemědělství	zemědělství	k1gNnSc4
<g/>
,	,	kIx,
na	na	k7c6
druhé	druhý	k4xOgFnSc6
straně	strana	k1gFnSc6
přes	přes	k7c4
vliv	vliv	k1gInSc4
slunečního	sluneční	k2eAgInSc2d1
větru	vítr	k1gInSc2
na	na	k7c4
elektrická	elektrický	k2eAgNnPc4d1
zařízení	zařízení	k1gNnPc4
na	na	k7c4
Zemi	zem	k1gFnSc4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
např.	např.	kA
kolapsy	kolaps	k1gInPc4
rozvodných	rozvodný	k2eAgFnPc2d1
sítí	síť	k1gFnPc2
v	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
<g/>
,	,	kIx,
Itálii	Itálie	k1gFnSc6
a	a	k8xC
Německu	Německo	k1gNnSc6
během	během	k7c2
první	první	k4xOgFnSc2
dekády	dekáda	k1gFnSc2
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgFnSc1d3
pozornost	pozornost	k1gFnSc1
národohospodářů	národohospodář	k1gMnPc2
a	a	k8xC
investorů	investor	k1gMnPc2
se	se	k3xPyFc4
zaměřuje	zaměřovat	k5eAaImIp3nS
na	na	k7c6
střednědobé	střednědobý	k2eAgFnSc6d1
t.	t.	k?
<g/>
zv	zv	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Juglarovy	Juglarův	k2eAgInPc1d1
cykly	cyklus	k1gInPc1
(	(	kIx(
<g/>
business	business	k1gInSc1
cycle	cycle	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
nejvýrazněji	výrazně	k6eAd3
ovlivňují	ovlivňovat	k5eAaImIp3nP
konjunkturu	konjunktura	k1gFnSc4
a	a	k8xC
mají	mít	k5eAaImIp3nP
celou	celý	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
projevů	projev	k1gInPc2
<g/>
.	.	kIx.
<g/>
Nejzávažnĕ	Nejzávažnĕ	k2eAgMnSc1d1
z	z	k7c2
nich	on	k3xPp3gFnPc2
jsou	být	k5eAaImIp3nP
cylický	cylický	k2eAgInSc4d1
pokles	pokles	k1gInSc4
výroby	výroba	k1gFnSc2
a	a	k8xC
cyklická	cyklický	k2eAgFnSc1d1
nezaměstnanost	nezaměstnanost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
závažné	závažný	k2eAgInPc4d1
projevy	projev	k1gInPc4
cyklického	cyklický	k2eAgInSc2d1
hospodářského	hospodářský	k2eAgInSc2d1
poklesu	pokles	k1gInSc2
patří	patřit	k5eAaImIp3nS
též	též	k9
finanční	finanční	k2eAgFnSc1d1
krize	krize	k1gFnSc1
<g/>
,	,	kIx,
prudký	prudký	k2eAgInSc1d1
pokles	pokles	k1gInSc1
akciových	akciový	k2eAgInPc2d1
kurzů	kurz	k1gInPc2
a	a	k8xC
úpadky	úpadek	k1gInPc1
firem	firma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
dob	doba	k1gFnPc2
průmyslové	průmyslový	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
jsou	být	k5eAaImIp3nP
hospodářské	hospodářský	k2eAgInPc4d1
cykly	cyklus	k1gInPc4
a	a	k8xC
cyklické	cyklický	k2eAgFnSc2d1
krize	krize	k1gFnSc2
považovány	považován	k2eAgFnPc1d1
za	za	k7c4
přirozené	přirozený	k2eAgInPc4d1
jevy	jev	k1gInPc4
resp.	resp.	kA
za	za	k7c4
zákonitosti	zákonitost	k1gFnPc4
růstu	růst	k1gInSc2
tržní	tržní	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
existuje	existovat	k5eAaImIp3nS
řada	řada	k1gFnSc1
názorů	názor	k1gInPc2
a	a	k8xC
škol	škola	k1gFnPc2
/	/	kIx~
<g/>
smĕ	smĕ	k1gInPc2
<g/>
/	/	kIx~
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
je	být	k5eAaImIp3nS
zmírňovat	zmírňovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současní	současný	k2eAgMnPc1d1
libertariáni	libertarián	k1gMnPc1
vĕ	vĕ	k1gFnSc7
odmítají	odmítat	k5eAaImIp3nP
zásahy	zásah	k1gInPc4
státu	stát	k1gInSc2
do	do	k7c2
cyklického	cyklický	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
ekonomiky	ekonomika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Umělá	umělý	k2eAgFnSc1d1
periodicita	periodicita	k1gFnSc1
</s>
<s>
Ekonomické	ekonomický	k2eAgInPc1d1
cykly	cyklus	k1gInPc1
se	se	k3xPyFc4
však	však	k9
mohou	moct	k5eAaImIp3nP
vyskytovat	vyskytovat	k5eAaImF
i	i	k9
bez	bez	k7c2
jakýchkoli	jakýkoli	k3yIgInPc2
vnějších	vnější	k2eAgInPc2d1
vlivů	vliv	k1gInPc2
<g/>
:	:	kIx,
Pak	pak	k6eAd1
je	být	k5eAaImIp3nS
periodické	periodický	k2eAgNnSc1d1
(	(	kIx(
<g/>
harmonické	harmonický	k2eAgNnSc4d1
<g/>
)	)	kIx)
kmitání	kmitání	k1gNnSc4
přímo	přímo	k6eAd1
vlastností	vlastnost	k1gFnSc7
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základním	základní	k2eAgInSc7d1
axiomem	axiom	k1gInSc7
ekonomie	ekonomie	k1gFnSc2
však	však	k9
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
trh	trh	k1gInSc1
vždy	vždy	k6eAd1
najde	najít	k5eAaPmIp3nS
rovnovážný	rovnovážný	k2eAgInSc4d1
stav	stav	k1gInSc4
<g/>
,	,	kIx,
tedy	tedy	k9
ustálenou	ustálený	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
může	moct	k5eAaImIp3nS
docházet	docházet	k5eAaImF
k	k	k7c3
překmitu	překmit	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
v	v	k7c6
praxi	praxe	k1gFnSc6
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
dané	daný	k2eAgFnSc6d1
ekonomice	ekonomika	k1gFnSc6
existují	existovat	k5eAaImIp3nP
vlivy	vliv	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
mají	mít	k5eAaImIp3nP
tendenci	tendence	k1gFnSc4
ji	on	k3xPp3gFnSc4
pravidelně	pravidelně	k6eAd1
vychylovat	vychylovat	k5eAaImF
z	z	k7c2
rovnováhy	rovnováha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Působením	působení	k1gNnSc7
volného	volný	k2eAgInSc2d1
trhu	trh	k1gInSc2
následně	následně	k6eAd1
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
návratu	návrat	k1gInSc3
do	do	k7c2
nového	nový	k2eAgInSc2d1
rovnovážného	rovnovážný	k2eAgInSc2d1
bodu	bod	k1gInSc2
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
je	být	k5eAaImIp3nS
následně	následně	k6eAd1
opět	opět	k6eAd1
vychýlena	vychýlit	k5eAaPmNgFnS
atd.	atd.	kA
</s>
<s>
Na	na	k7c6
základě	základ	k1gInSc6
takové	takový	k3xDgFnSc2
úvahy	úvaha	k1gFnSc2
pak	pak	k6eAd1
např.	např.	kA
zastánci	zastánce	k1gMnPc1
liberalismu	liberalismus	k1gInSc2
odmítají	odmítat	k5eAaImIp3nP
zásahy	zásah	k1gInPc1
státu	stát	k1gInSc2
do	do	k7c2
ekonomiky	ekonomika	k1gFnSc2
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
místo	místo	k1gNnSc1
aby	aby	kYmCp3nP
se	se	k3xPyFc4
tak	tak	k9
problém	problém	k1gInSc1
řešil	řešit	k5eAaImAgInS
<g/>
,	,	kIx,
naopak	naopak	k6eAd1
se	se	k3xPyFc4
ještě	ještě	k9
zhoršuje	zhoršovat	k5eAaImIp3nS
(	(	kIx(
<g/>
přilévání	přilévání	k1gNnSc1
oleje	olej	k1gInSc2
do	do	k7c2
ohně	oheň	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Argumentem	argument	k1gInSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
byly	být	k5eAaImAgFnP
pravě	pravě	k6eAd1
intervence	intervence	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
ekonomiku	ekonomika	k1gFnSc4
vychýlily	vychýlit	k5eAaPmAgInP
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
ji	on	k3xPp3gFnSc4
další	další	k2eAgFnSc1d1
intervence	intervence	k1gFnSc1
v	v	k7c6
podobném	podobný	k2eAgMnSc6d1
duchu	duch	k1gMnSc6
zpět	zpět	k6eAd1
neuklidní	uklidnit	k5eNaPmIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
naopak	naopak	k6eAd1
pokřiví	pokřivit	k5eAaPmIp3nS
ještě	ještě	k9
více	hodně	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nastává	nastávat	k5eAaImIp3nS
paradox	paradox	k1gInSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
stát	stát	k1gInSc1
bere	brát	k5eAaImIp3nS
obyvatelům	obyvatel	k1gMnPc3
prostředky	prostředek	k1gInPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
je	on	k3xPp3gFnPc4
mohl	moct	k5eAaImAgMnS
utratit	utratit	k5eAaPmF
<g/>
;	;	kIx,
a	a	k8xC
utrácí	utrácet	k5eAaImIp3nP
je	on	k3xPp3gFnPc4
právě	právě	k9
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
dále	daleko	k6eAd2
pokřivuje	pokřivovat	k5eAaImIp3nS
hospodářské	hospodářský	k2eAgInPc4d1
poměry	poměr	k1gInPc4
a	a	k8xC
tím	ten	k3xDgNnSc7
lidé	člověk	k1gMnPc1
trpí	trpět	k5eAaImIp3nP
znovu	znovu	k6eAd1
a	a	k8xC
ve	v	k7c6
větším	veliký	k2eAgInSc6d2
rozsahu	rozsah	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odpůrci	odpůrce	k1gMnPc1
tzv.	tzv.	kA
"	"	kIx"
<g/>
velkého	velký	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
minarchisté	minarchista	k1gMnPc1
<g/>
,	,	kIx,
dále	daleko	k6eAd2
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
bez	bez	k7c2
vlivů	vliv	k1gInPc2
státu	stát	k1gInSc2
by	by	kYmCp3nP
žádné	žádný	k3yNgNnSc1
vnitřní	vnitřní	k2eAgNnSc1d1
oscilování	oscilování	k1gNnSc1
v	v	k7c6
ekonomice	ekonomika	k1gFnSc6
vůbec	vůbec	k9
nevznikalo	vznikat	k5eNaImAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fluktuace	fluktuace	k1gFnSc1
</s>
<s>
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
pravidelného	pravidelný	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
fluktuace	fluktuace	k1gFnPc1
představují	představovat	k5eAaImIp3nP
přizpůsobení	přizpůsobení	k1gNnSc2
se	se	k3xPyFc4
trhu	trh	k1gInSc6
po	po	k7c6
jednorázových	jednorázový	k2eAgFnPc6d1
událostech	událost	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nelze	lze	k6eNd1
tedy	tedy	k9
hovořit	hovořit	k5eAaImF
o	o	k7c6
hospodářském	hospodářský	k2eAgInSc6d1
cyklu	cyklus	k1gInSc6
v	v	k7c6
pravém	pravý	k2eAgInSc6d1
slova	slovo	k1gNnSc2
smyslu	smysl	k1gInSc6
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
tyto	tento	k3xDgFnPc1
jednorázové	jednorázový	k2eAgFnPc1d1
události	událost	k1gFnPc1
mají	mít	k5eAaImIp3nP
tendenci	tendence	k1gFnSc4
se	se	k3xPyFc4
opakovat	opakovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Nepravidelné	pravidelný	k2eNgInPc1d1
vlivy	vliv	k1gInPc1
na	na	k7c4
ekonomiku	ekonomika	k1gFnSc4
jsou	být	k5eAaImIp3nP
buď	buď	k8xC
impulsní	impulsní	k2eAgInPc1d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
po	po	k7c6
jednorázovém	jednorázový	k2eAgInSc6d1
přechodu	přechod	k1gInSc6
už	už	k6eAd1
trvalé	trvalý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záleží	záležet	k5eAaImIp3nS
na	na	k7c6
flexibilitě	flexibilita	k1gFnSc6
ekonomiky	ekonomika	k1gFnSc2
do	do	k7c2
jaké	jaký	k3yRgFnSc2,k3yQgFnSc2,k3yIgFnSc2
míry	míra	k1gFnSc2
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
těmito	tento	k3xDgInPc7
vlivy	vliv	k1gInPc7
ovlivnit	ovlivnit	k5eAaPmF
<g/>
,	,	kIx,
v	v	k7c6
jakém	jaký	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
územním	územní	k2eAgInSc6d1
rozsahu	rozsah	k1gInSc6
a	a	k8xC
s	s	k7c7
jakou	jaký	k3yQgFnSc7,k3yRgFnSc7,k3yIgFnSc7
svou	svůj	k3xOyFgFnSc7
dynamikou	dynamika	k1gFnSc7
dosáhne	dosáhnout	k5eAaPmIp3nS
nového	nový	k2eAgInSc2d1
rovnovážného	rovnovážný	k2eAgInSc2d1
stavu	stav	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příkladem	příklad	k1gInSc7
takových	takový	k3xDgFnPc2
změn	změna	k1gFnPc2
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
přírodní	přírodní	k2eAgFnPc4d1
katastrofy	katastrofa	k1gFnPc4
(	(	kIx(
<g/>
jako	jako	k9
například	například	k6eAd1
vlny	vlna	k1gFnSc2
tsunami	tsunami	k1gNnSc1
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
v	v	k7c6
Indickém	indický	k2eAgInSc6d1
oceánu	oceán	k1gInSc6
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
války	válka	k1gFnSc2
(	(	kIx(
<g/>
v	v	k7c6
Iráku	Irák	k1gInSc6
nebo	nebo	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
sled	sled	k1gInSc1
revolucí	revoluce	k1gFnPc2
v	v	k7c6
severní	severní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
a	a	k8xC
občanská	občanský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
v	v	k7c6
Libyi	Libye	k1gFnSc6
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
a	a	k8xC
také	také	k9
průlomové	průlomový	k2eAgInPc4d1
technologické	technologický	k2eAgInPc4d1
objevy	objev	k1gInPc4
(	(	kIx(
<g/>
oheň	oheň	k1gInSc1
<g/>
,	,	kIx,
kolo	kolo	k1gNnSc1
<g/>
,	,	kIx,
kov	kov	k1gInSc1
<g/>
,	,	kIx,
uhlí	uhlí	k1gNnSc1
<g/>
,	,	kIx,
pára	pára	k1gFnSc1
<g/>
,	,	kIx,
elektřina	elektřina	k1gFnSc1
<g/>
,	,	kIx,
ropa	ropa	k1gFnSc1
<g/>
,	,	kIx,
tranzistor	tranzistor	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
všechny	všechen	k3xTgFnPc1
nejprve	nejprve	k6eAd1
ovlivnily	ovlivnit	k5eAaPmAgFnP
jen	jen	k6eAd1
určitou	určitý	k2eAgFnSc4d1
část	část	k1gFnSc4
ekonomiky	ekonomika	k1gFnSc2
a	a	k8xC
jejich	jejich	k3xOp3gInSc1
vliv	vliv	k1gInSc1
se	se	k3xPyFc4
postupně	postupně	k6eAd1
přeléval	přelévat	k5eAaImAgMnS
mezi	mezi	k7c4
obory	obora	k1gFnPc4
k	k	k7c3
novému	nový	k2eAgInSc3d1
ustálenému	ustálený	k2eAgInSc3d1
stavu	stav	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
nepravidelné	pravidelný	k2eNgInPc4d1
jevy	jev	k1gInPc4
(	(	kIx(
<g/>
jako	jako	k8xS,k8xC
např.	např.	kA
přechodové	přechodový	k2eAgInPc4d1
jevy	jev	k1gInPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
lze	lze	k6eAd1
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
bránit	bránit	k5eAaImF
jen	jen	k6eAd1
těžko	těžko	k6eAd1
a	a	k8xC
spíše	spíše	k9
plošnými	plošný	k2eAgFnPc7d1
metodami	metoda	k1gFnPc7
založenými	založený	k2eAgFnPc7d1
na	na	k7c6
statistice	statistika	k1gFnSc6
a	a	k8xC
řízení	řízení	k1gNnSc6
rizik	riziko	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Průběh	průběh	k1gInSc1
hospodářského	hospodářský	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
</s>
<s>
Každá	každý	k3xTgFnSc1
ekonomika	ekonomika	k1gFnSc1
prochází	procházet	k5eAaImIp3nS
všemi	všecek	k3xTgFnPc7
částmi	část	k1gFnPc7
cyklu	cyklus	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
mnohdy	mnohdy	k6eAd1
nevyrovnané	vyrovnaný	k2eNgInPc1d1
<g/>
.	.	kIx.
</s>
<s>
Učebnicový	učebnicový	k2eAgInSc1d1
hospodářský	hospodářský	k2eAgInSc1d1
cyklus	cyklus	k1gInSc1
obvykle	obvykle	k6eAd1
začíná	začínat	k5eAaImIp3nS
velkým	velký	k2eAgInSc7d1
hospodářským	hospodářský	k2eAgInSc7d1
růstem	růst	k1gInSc7
a	a	k8xC
rozvojem	rozvoj	k1gInSc7
<g/>
,	,	kIx,
nazýváme	nazývat	k5eAaImIp1nP
jej	on	k3xPp3gNnSc4
expanze	expanze	k1gFnSc1
(	(	kIx(
<g/>
konjunktura	konjunktura	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgInSc1
stupeň	stupeň	k1gInSc1
hospodářského	hospodářský	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
nazýváme	nazývat	k5eAaImIp1nP
vrchol	vrchol	k1gInSc4
<g/>
,	,	kIx,
zde	zde	k6eAd1
dochází	docházet	k5eAaImIp3nS
ke	k	k7c3
zpomalení	zpomalení	k1gNnSc3
růstu	růst	k1gInSc2
HDP	HDP	kA
<g/>
,	,	kIx,
tempo	tempo	k1gNnSc1
růstu	růst	k1gInSc2
výroby	výroba	k1gFnSc2
se	se	k3xPyFc4
zpomaluje	zpomalovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
se	se	k3xPyFc4
trh	trh	k1gInSc1
nasytí	nasytit	k5eAaPmIp3nS
a	a	k8xC
postupně	postupně	k6eAd1
začne	začít	k5eAaPmIp3nS
klesat	klesat	k5eAaImF
poptávka	poptávka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následkem	následkem	k7c2
toho	ten	k3xDgInSc2
se	se	k3xPyFc4
sníží	snížit	k5eAaPmIp3nS
výroba	výroba	k1gFnSc1
<g/>
;	;	kIx,
neefektivní	efektivní	k2eNgMnPc1d1
producenti	producent	k1gMnPc1
zboží	zboží	k1gNnSc2
a	a	k8xC
poskytovatelé	poskytovatel	k1gMnPc1
služeb	služba	k1gFnPc2
zkrachují	zkrachovat	k5eAaPmIp3nP
<g/>
,	,	kIx,
nastane	nastat	k5eAaPmIp3nS
třetí	třetí	k4xOgFnSc1
fáze	fáze	k1gFnSc1
cyklu	cyklus	k1gInSc2
<g/>
,	,	kIx,
recese	recese	k1gFnSc2
<g/>
,	,	kIx,
později	pozdě	k6eAd2
deprese	deprese	k1gFnSc1
a	a	k8xC
nakonec	nakonec	k6eAd1
krize	krize	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
se	se	k3xPyFc4
celý	celý	k2eAgInSc1d1
koloběh	koloběh	k1gInSc1
opět	opět	k6eAd1
opakuje	opakovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Fáze	fáze	k1gFnSc1
hospodářského	hospodářský	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
dno	dno	k1gNnSc1
(	(	kIx(
<g/>
sedlo	sedlo	k1gNnSc1
<g/>
,	,	kIx,
deprese	deprese	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
expanze	expanze	k1gFnSc1
(	(	kIx(
<g/>
zotavení	zotavení	k1gNnSc1
<g/>
,	,	kIx,
konjunktura	konjunktura	k1gFnSc1
<g/>
,	,	kIx,
boom	boom	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
vrchol	vrchol	k1gInSc1
(	(	kIx(
<g/>
a	a	k8xC
následné	následný	k2eAgNnSc1d1
prasknutí	prasknutí	k1gNnSc1
bubliny	bublina	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
kontrakce	kontrakce	k1gFnSc1
(	(	kIx(
<g/>
pokles	pokles	k1gInSc1
<g/>
,	,	kIx,
recese	recese	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
předpokládanému	předpokládaný	k2eAgNnSc3d1
opakování	opakování	k1gNnSc3
cyklu	cyklus	k1gInSc2
nelze	lze	k6eNd1
jednoznačně	jednoznačně	k6eAd1
určit	určit	k5eAaPmF
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
fáze	fáze	k1gFnSc1
je	být	k5eAaImIp3nS
počínající	počínající	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dno	dno	k1gNnSc1
a	a	k8xC
vrchol	vrchol	k1gInSc1
však	však	k9
představují	představovat	k5eAaImIp3nP
body	bod	k1gInPc4
obratu	obrat	k1gInSc2
a	a	k8xC
konjunktura	konjunktura	k1gFnSc1
s	s	k7c7
recesí	recese	k1gFnSc7
jsou	být	k5eAaImIp3nP
hlavními	hlavní	k2eAgFnPc7d1
fázemi	fáze	k1gFnPc7
cyklu	cyklus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Zatímco	zatímco	k8xS
standardní	standardní	k2eAgNnSc1d1
vysvětlení	vysvětlení	k1gNnSc1
hospodářského	hospodářský	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
se	se	k3xPyFc4
soustředí	soustředit	k5eAaPmIp3nS
na	na	k7c4
problémy	problém	k1gInPc4
během	během	k7c2
klesající	klesající	k2eAgFnSc2d1
fáze	fáze	k1gFnSc2
hospodářského	hospodářský	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
<g/>
,	,	kIx,
Rakouská	rakouský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
naopak	naopak	k6eAd1
vidí	vidět	k5eAaImIp3nS
problém	problém	k1gInSc1
již	již	k6eAd1
v	v	k7c6
nadměrném	nadměrný	k2eAgInSc6d1
růstu	růst	k1gInSc6
v	v	k7c4
první	první	k4xOgFnSc4
fázi	fáze	k1gFnSc4
a	a	k8xC
nevyhnutelnou	vyhnutelný	k2eNgFnSc4d1
krizi	krize	k1gFnSc4
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
očistnou	očistný	k2eAgFnSc4d1
fázi	fáze	k1gFnSc4
hospodářského	hospodářský	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
<g/>
,	,	kIx,
po	po	k7c6
níž	jenž	k3xRgFnSc6
přežijí	přežít	k5eAaPmIp3nP
pouze	pouze	k6eAd1
ti	ten	k3xDgMnPc1
efektivnější	efektivní	k2eAgMnSc1d2
výrobci	výrobce	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Hospodářské	hospodářský	k2eAgInPc1d1
cykly	cyklus	k1gInPc1
dle	dle	k7c2
délky	délka	k1gFnSc2
</s>
<s>
Podle	podle	k7c2
délky	délka	k1gFnSc2
hospodářských	hospodářský	k2eAgInPc2d1
cyklů	cyklus	k1gInPc2
<g/>
,	,	kIx,
měřené	měřený	k2eAgFnSc6d1
od	od	k7c2
bodu	bod	k1gInSc2
obratu	obrat	k1gInSc2
k	k	k7c3
jeho	jeho	k3xOp3gNnSc3
zopakování	zopakování	k1gNnSc3
<g/>
,	,	kIx,
se	se	k3xPyFc4
člení	členit	k5eAaImIp3nS
do	do	k7c2
tří	tři	k4xCgInPc2
základních	základní	k2eAgInPc2d1
typů	typ	k1gInPc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
krátkodobé	krátkodobý	k2eAgInPc1d1
=	=	kIx~
tzv.	tzv.	kA
Kitchinovy	Kitchinův	k2eAgInPc1d1
cykly	cyklus	k1gInPc1
v	v	k7c6
délce	délka	k1gFnSc6
18	#num#	k4
<g/>
–	–	k?
<g/>
40	#num#	k4
měsíců	měsíc	k1gInPc2
představují	představovat	k5eAaImIp3nP
krátkodobé	krátkodobý	k2eAgNnSc4d1
kolísání	kolísání	k1gNnSc4
reálného	reálný	k2eAgInSc2d1
produktu	produkt	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
je	být	k5eAaImIp3nS
způsobeno	způsobit	k5eAaPmNgNnS
výkyvy	výkyv	k1gInPc7
v	v	k7c6
zásobách	zásoba	k1gFnPc6
<g/>
,	,	kIx,
příčiny	příčina	k1gFnPc1
jsou	být	k5eAaImIp3nP
obtížně	obtížně	k6eAd1
prokazatelné	prokazatelný	k2eAgFnPc1d1
<g/>
,	,	kIx,
</s>
<s>
střednědobé	střednědobý	k2eAgNnSc1d1
=	=	kIx~
tzv.	tzv.	kA
Juglarovy	Juglarův	k2eAgInPc1d1
cykly	cyklus	k1gInPc1
v	v	k7c6
délce	délka	k1gFnSc6
8	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
let	léto	k1gNnPc2
jsou	být	k5eAaImIp3nP
spojovány	spojován	k2eAgInPc1d1
s	s	k7c7
investicemi	investice	k1gFnPc7
do	do	k7c2
fixního	fixní	k2eAgInSc2d1
kapitálu	kapitál	k1gInSc2
<g/>
,	,	kIx,
střídá	střídat	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
nich	on	k3xPp3gMnPc6
období	období	k1gNnSc6
zvýšeného	zvýšený	k2eAgNnSc2d1
opotřebení	opotřebení	k1gNnSc2
a	a	k8xC
zvýšeného	zvýšený	k2eAgNnSc2d1
investování	investování	k1gNnSc2
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
jít	jít	k5eAaImF
také	také	k9
o	o	k7c4
střídání	střídání	k1gNnSc4
generací	generace	k1gFnPc2
komodit	komodita	k1gFnPc2
<g/>
,	,	kIx,
zemědělské	zemědělský	k2eAgInPc4d1
výkyvy	výkyv	k1gInPc4
apod.	apod.	kA
</s>
<s>
dlouhodobé	dlouhodobý	k2eAgNnSc1d1
=	=	kIx~
tzv.	tzv.	kA
Kuznetsovy	Kuznetsův	k2eAgInPc1d1
cykly	cyklus	k1gInPc1
(	(	kIx(
<g/>
též	též	k9
Schumpeterovy	Schumpeterův	k2eAgFnPc1d1
<g/>
,	,	kIx,
Kondratěvovy	Kondratěvův	k2eAgFnPc1d1
vlny	vlna	k1gFnPc1
<g/>
)	)	kIx)
v	v	k7c6
délce	délka	k1gFnSc6
20	#num#	k4
<g/>
–	–	k?
<g/>
50	#num#	k4
let	léto	k1gNnPc2
jsou	být	k5eAaImIp3nP
vysvětlovány	vysvětlovat	k5eAaImNgFnP
válkami	válka	k1gFnPc7
<g/>
,	,	kIx,
vědeckými	vědecký	k2eAgInPc7d1
objevy	objev	k1gInPc7
<g/>
,	,	kIx,
značnými	značný	k2eAgFnPc7d1
investicemi	investice	k1gFnPc7
do	do	k7c2
infrastruktury	infrastruktura	k1gFnSc2
<g/>
,	,	kIx,
inovačními	inovační	k2eAgFnPc7d1
vlnami	vlna	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Vysvětlení	vysvětlení	k1gNnSc1
hospodářského	hospodářský	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
</s>
<s>
Ekonomové	ekonom	k1gMnPc1
různých	různý	k2eAgFnPc2d1
škol	škola	k1gFnPc2
vidí	vidět	k5eAaImIp3nS
dvě	dva	k4xCgFnPc4
hlavní	hlavní	k2eAgFnPc4d1
příčiny	příčina	k1gFnPc4
hospodářského	hospodářský	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
–	–	k?
monetární	monetární	k2eAgNnSc1d1
a	a	k8xC
reálné	reálný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zastánci	zastánce	k1gMnPc1
monetárních	monetární	k2eAgFnPc2d1
příčin	příčina	k1gFnPc2
(	(	kIx(
<g/>
monetaristé	monetarista	k1gMnPc1
a	a	k8xC
Rakouská	rakouský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
)	)	kIx)
hospodářského	hospodářský	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
spatřují	spatřovat	k5eAaImIp3nP
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
původ	původ	k1gInSc4
v	v	k7c6
neodpovídající	odpovídající	k2eNgFnSc6d1
nabídce	nabídka	k1gFnSc6
peněz	peníze	k1gInPc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
zastánci	zastánce	k1gMnPc1
reálného	reálný	k2eAgNnSc2d1
vysvětlení	vysvětlení	k1gNnSc2
hospodářského	hospodářský	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
(	(	kIx(
<g/>
keynesiánci	keynesiánec	k1gMnPc1
a	a	k8xC
zastánci	zastánce	k1gMnPc1
Reálné	reálný	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
hospodářského	hospodářský	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
<g/>
)	)	kIx)
spatřují	spatřovat	k5eAaImIp3nP
původ	původ	k1gInSc4
hospodářského	hospodářský	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
v	v	k7c6
reálných	reálný	k2eAgInPc6d1
(	(	kIx(
<g/>
nepeněžních	peněžní	k2eNgInPc6d1
<g/>
)	)	kIx)
jevech	jev	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Monetaristé	Monetarista	k1gMnPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Monetarismus	Monetarismus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Monetaristé	Monetarista	k1gMnPc1
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
Miltonem	Milton	k1gInSc7
Friedmanem	Friedman	k1gMnSc7
jsou	být	k5eAaImIp3nP
zastánci	zastánce	k1gMnPc1
monetárního	monetární	k2eAgInSc2d1
(	(	kIx(
<g/>
peněžního	peněžní	k2eAgInSc2d1
<g/>
)	)	kIx)
původu	původ	k1gInSc2
hospodářského	hospodářský	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Růst	růst	k1gInSc1
nabídky	nabídka	k1gFnSc2
peněz	peníze	k1gInPc2
je	být	k5eAaImIp3nS
určujícím	určující	k2eAgInSc7d1
faktorem	faktor	k1gInSc7
hospodářského	hospodářský	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
monetární	monetární	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
stabilní	stabilní	k2eAgInSc4d1
růst	růst	k1gInSc4
peněžní	peněžní	k2eAgFnSc2d1
nabídky	nabídka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
nabídka	nabídka	k1gFnSc1
peněz	peníze	k1gInPc2
roste	růst	k5eAaImIp3nS
příliš	příliš	k6eAd1
rychle	rychle	k6eAd1
<g/>
,	,	kIx,
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
inflaci	inflace	k1gFnSc3
a	a	k8xC
pokud	pokud	k8xS
naopak	naopak	k6eAd1
roste	růst	k5eAaImIp3nS
pomalu	pomalu	k6eAd1
<g/>
,	,	kIx,
v	v	k7c6
ekonomice	ekonomika	k1gFnSc6
se	se	k3xPyFc4
objevují	objevovat	k5eAaImIp3nP
deflační	deflační	k2eAgInPc1d1
tlaky	tlak	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rigidní	rigidní	k2eAgFnPc4d1
mzdy	mzda	k1gFnPc4
a	a	k8xC
ceny	cena	k1gFnPc4
při	při	k7c6
pomalém	pomalý	k2eAgInSc6d1
růstu	růst	k1gInSc6
peněžní	peněžní	k2eAgFnPc1d1
nabídky	nabídka	k1gFnPc1
vedou	vést	k5eAaImIp3nP
k	k	k7c3
hospodářskému	hospodářský	k2eAgInSc3d1
cyklu	cyklus	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Rakouská	rakouský	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
hospodářského	hospodářský	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Rakouská	rakouský	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
hospodářského	hospodářský	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Rakouská	rakouský	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
hospodářského	hospodářský	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
se	se	k3xPyFc4
řadí	řadit	k5eAaImIp3nS
mezi	mezi	k7c4
monetární	monetární	k2eAgFnPc4d1
teorie	teorie	k1gFnPc4
hospodářských	hospodářský	k2eAgInPc2d1
cyklů	cyklus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dle	dle	k7c2
této	tento	k3xDgFnSc2
teorie	teorie	k1gFnSc2
je	být	k5eAaImIp3nS
příčinou	příčina	k1gFnSc7
hospodářského	hospodářský	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
činnost	činnost	k1gFnSc4
centrální	centrální	k2eAgFnSc2d1
banky	banka	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
v	v	k7c6
systému	systém	k1gInSc6
bankovnictví	bankovnictví	k1gNnSc2
částečných	částečný	k2eAgFnPc2d1
rezerv	rezerva	k1gFnPc2
umožňuje	umožňovat	k5eAaImIp3nS
úvěrovou	úvěrový	k2eAgFnSc4d1
expanzi	expanze	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komerční	komerční	k2eAgFnPc4d1
banky	banka	k1gFnPc4
mohou	moct	k5eAaImIp3nP
tedy	tedy	k9
vytvářet	vytvářet	k5eAaImF
úvěry	úvěr	k1gInPc4
nad	nad	k7c4
rámec	rámec	k1gInSc4
svých	svůj	k3xOyFgFnPc2
rezerv	rezerva	k1gFnPc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
efektivně	efektivně	k6eAd1
vznikají	vznikat	k5eAaImIp3nP
nové	nový	k2eAgInPc4d1
peníze	peníz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
peníze	peníz	k1gInPc4
směřují	směřovat	k5eAaImIp3nP
do	do	k7c2
nových	nový	k2eAgFnPc2d1
investic	investice	k1gFnPc2
a	a	k8xC
ekonomika	ekonomika	k1gFnSc1
roste	růst	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Růst	růst	k1gInSc1
peněžní	peněžní	k2eAgFnSc2d1
nabídky	nabídka	k1gFnSc2
se	se	k3xPyFc4
ale	ale	k9
následně	následně	k6eAd1
projeví	projevit	k5eAaPmIp3nS
všeobecným	všeobecný	k2eAgInSc7d1
růstem	růst	k1gInSc7
cen	cena	k1gFnPc2
neboli	neboli	k8xC
inflací	inflace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inflace	inflace	k1gFnSc1
způsobí	způsobit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
projekty	projekt	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
jevily	jevit	k5eAaImAgFnP
jako	jako	k9
ziskové	ziskový	k2eAgFnPc1d1
<g/>
,	,	kIx,
se	se	k3xPyFc4
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
stanou	stanout	k5eAaPmIp3nP
ztrátovými	ztrátový	k2eAgFnPc7d1
a	a	k8xC
krachují	krachovat	k5eAaBmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nastává	nastávat	k5eAaImIp3nS
krize	krize	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
očistnou	očistný	k2eAgFnSc7d1
fází	fáze	k1gFnSc7
hospodářského	hospodářský	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Keynesiánci	keynesiánec	k1gMnPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Keynesiánství	Keynesiánství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Zastánci	zastánce	k1gMnPc1
keynesiánství	keynesiánství	k1gNnSc2
se	se	k3xPyFc4
při	při	k7c6
vysvětlování	vysvětlování	k1gNnSc6
hospodářského	hospodářský	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
soustředí	soustředit	k5eAaPmIp3nS
na	na	k7c4
agregátní	agregátní	k2eAgFnSc4d1
poptávku	poptávka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokles	pokles	k1gInSc1
agregátní	agregátní	k2eAgFnSc2d1
poptávky	poptávka	k1gFnSc2
vede	vést	k5eAaImIp3nS
k	k	k7c3
poklesu	pokles	k1gInSc3
příjmů	příjem	k1gInPc2
výrobců	výrobce	k1gMnPc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
ceny	cena	k1gFnPc1
a	a	k8xC
mzdy	mzda	k1gFnPc1
se	se	k3xPyFc4
přizpůsobují	přizpůsobovat	k5eAaImIp3nP
pomaleji	pomale	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reálné	reálný	k2eAgInPc1d1
náklady	náklad	k1gInPc1
výrobců	výrobce	k1gMnPc2
tedy	tedy	k8xC
rostou	růst	k5eAaImIp3nP
a	a	k8xC
ti	ten	k3xDgMnPc1
jsou	být	k5eAaImIp3nP
nuceni	nutit	k5eAaImNgMnP
propustit	propustit	k5eAaPmF
část	část	k1gFnSc4
zaměstnanců	zaměstnanec	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vede	vést	k5eAaImIp3nS
k	k	k7c3
růstu	růst	k1gInSc3
nezaměstnanosti	nezaměstnanost	k1gFnSc2
a	a	k8xC
dalšímu	další	k2eAgInSc3d1
poklesu	pokles	k1gInSc3
agregátní	agregátní	k2eAgFnSc2d1
poptávky	poptávka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tuto	tento	k3xDgFnSc4
chvíli	chvíle	k1gFnSc4
by	by	kYmCp3nS
podle	podle	k7c2
této	tento	k3xDgFnSc2
teorie	teorie	k1gFnSc2
měla	mít	k5eAaImAgFnS
zasáhnout	zasáhnout	k5eAaPmF
vláda	vláda	k1gFnSc1
a	a	k8xC
nahradit	nahradit	k5eAaPmF
výpadek	výpadek	k1gInSc4
agregátní	agregátní	k2eAgFnSc2d1
poptávky	poptávka	k1gFnSc2
růstem	růst	k1gInSc7
státních	státní	k2eAgInPc2d1
výdajů	výdaj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Teorie	teorie	k1gFnSc1
reálného	reálný	k2eAgInSc2d1
hospodářského	hospodářský	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
</s>
<s>
Teorie	teorie	k1gFnSc1
reálného	reálný	k2eAgInSc2d1
hospodářského	hospodářský	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
se	se	k3xPyFc4
soustředí	soustředit	k5eAaPmIp3nS
na	na	k7c4
negativní	negativní	k2eAgInPc4d1
šoky	šok	k1gInPc4
v	v	k7c6
ekonomice	ekonomika	k1gFnSc6
(	(	kIx(
<g/>
např.	např.	kA
růst	růst	k1gInSc4
ceny	cena	k1gFnSc2
ropy	ropa	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
úvodním	úvodní	k2eAgInSc6d1
šoku	šok	k1gInSc6
dojde	dojít	k5eAaPmIp3nS
k	k	k7c3
poklesu	pokles	k1gInSc3
ekonomické	ekonomický	k2eAgFnSc2d1
aktivity	aktivita	k1gFnSc2
v	v	k7c6
určitém	určitý	k2eAgInSc6d1
sektoru	sektor	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
postupně	postupně	k6eAd1
přelívá	přelívat	k5eAaImIp3nS
do	do	k7c2
dalších	další	k2eAgFnPc2d1
částí	část	k1gFnPc2
ekonomiky	ekonomika	k1gFnSc2
v	v	k7c6
důsledku	důsledek	k1gInSc6
čehož	což	k3yRnSc2,k3yQnSc2
dochází	docházet	k5eAaImIp3nS
ke	k	k7c3
krizi	krize	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
významných	významný	k2eAgFnPc2d1
krizí	krize	k1gFnPc2
zaviněných	zaviněný	k2eAgFnPc2d1
hospodářským	hospodářský	k2eAgInSc7d1
cyklem	cyklus	k1gInSc7
</s>
<s>
Velká	velký	k2eAgFnSc1d1
deprese	deprese	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Velká	velký	k2eAgFnSc1d1
deprese	deprese	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Kolosálním	kolosální	k2eAgInSc7d1
příkladem	příklad	k1gInSc7
státních	státní	k2eAgInPc2d1
zásahů	zásah	k1gInPc2
do	do	k7c2
průběhu	průběh	k1gInSc2
hospodářského	hospodářský	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
byl	být	k5eAaImAgMnS
tzv.	tzv.	kA
New	New	k1gMnSc1
Deal	Deal	k1gMnSc1
v	v	k7c6
USA	USA	kA
v	v	k7c6
polovině	polovina	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
nesl	nést	k5eAaImAgMnS
v	v	k7c6
duchu	duch	k1gMnSc6
keynesiánské	keynesiánský	k2eAgFnSc2d1
myšlenky	myšlenka	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
"	"	kIx"
<g/>
národ	národ	k1gInSc1
obohatíme	obohatit	k5eAaPmIp1nP
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
budeme	být	k5eAaImBp1nP
rozbíjet	rozbíjet	k5eAaImF
okna	okno	k1gNnPc4
<g/>
"	"	kIx"
<g/>
:	:	kIx,
Nezaměstnaný	nezaměstnaný	k1gMnSc1
totiž	totiž	k9
dostane	dostat	k5eAaPmIp3nS
práci	práce	k1gFnSc4
a	a	k8xC
bude	být	k5eAaImBp3nS
bohatší	bohatý	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
to	ten	k3xDgNnSc1
vše	všechen	k3xTgNnSc1
za	za	k7c4
peníze	peníz	k1gInPc4
od	od	k7c2
občanů	občan	k1gMnPc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
na	na	k7c4
dluh	dluh	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
pak	pak	k6eAd1
tedy	tedy	k9
opět	opět	k6eAd1
od	od	k7c2
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Internetová	internetový	k2eAgFnSc1d1
bublina	bublina	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Internetová	internetový	k2eAgFnSc1d1
horečka	horečka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Realitní	realitní	k2eAgFnSc1d1
bublina	bublina	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Americká	americký	k2eAgFnSc1d1
hypoteční	hypoteční	k2eAgFnSc1d1
krize	krize	k1gFnSc1
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Korotayev	Korotayev	k1gFnSc1
<g/>
,	,	kIx,
Andrey	Andrea	k1gFnPc1
V.	V.	kA
<g/>
,	,	kIx,
&	&	k?
Tsirel	Tsirel	k1gInSc1
<g/>
,	,	kIx,
Sergey	Sergea	k1gFnPc1
V.	V.	kA
A	a	k8xC
Spectral	Spectral	k1gMnSc1
Analysis	Analysis	k1gFnSc2
of	of	k?
World	World	k1gInSc1
GDP	GDP	kA
Dynamics	Dynamics	k1gInSc1
<g/>
:	:	kIx,
Kondratieff	Kondratieff	k1gMnSc1
Waves	Waves	k1gMnSc1
<g/>
,	,	kIx,
Kuznets	Kuznets	k1gInSc1
Swings	Swings	k1gInSc1
<g/>
,	,	kIx,
Juglar	Juglar	k1gMnSc1
and	and	k?
Kitchin	Kitchin	k1gMnSc1
Cycles	Cycles	k1gMnSc1
in	in	k?
Global	globat	k5eAaImAgMnS
Economic	Economic	k1gMnSc1
Development	Development	k1gMnSc1
<g/>
,	,	kIx,
and	and	k?
the	the	k?
2008	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
Economic	Economice	k1gInPc2
Crisis	Crisis	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Structure	Structur	k1gMnSc5
and	and	k?
Dynamics	Dynamics	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vol	vol	k6eAd1
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
no	no	k9
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
pp	pp	k?
<g/>
.	.	kIx.
3	#num#	k4
<g/>
–	–	k?
<g/>
57.1	57.1	k4
2	#num#	k4
HUERTA	HUERTA	kA
DE	DE	k?
SOTO	SOTO	kA
<g/>
,	,	kIx,
Jesús	Jesús	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peníze	peníz	k1gInSc2
<g/>
,	,	kIx,
banky	banka	k1gFnSc2
a	a	k8xC
hospodářské	hospodářský	k2eAgFnSc2d1
krize	krize	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Liberální	liberální	k2eAgInSc1d1
institut	institut	k1gInSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
865	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7357	#num#	k4
<g/>
-	-	kIx~
<g/>
411	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Cahlík	Cahlík	k1gInSc1
<g/>
,	,	kIx,
T.	T.	kA
Makroekonomie	makroekonomie	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc1
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Karolinum	Karolinum	k1gNnSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vít	vít	k5eAaImF
Pošta	pošta	k1gFnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
Sirůček	Sirůček	k1gMnSc1
<g/>
:	:	kIx,
Makroekonomie	makroekonomie	k1gFnSc1
základní	základní	k2eAgFnSc1d1
kurz	kurz	k1gInSc1
<g/>
,	,	kIx,
cvičebnice	cvičebnice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Melandrium	Melandrium	k1gNnSc1
Slaný	Slaný	k1gInSc1
2008	#num#	k4
<g/>
.	.	kIx.
s.	s.	k?
114	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Pokračující	pokračující	k2eAgInSc1d1
růst	růst	k1gInSc1
státu	stát	k1gInSc2
v	v	k7c6
hospodářsky	hospodářsky	k6eAd1
vyspělých	vyspělý	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
Higgs	Higgs	k1gInSc1
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
<g/>
:	:	kIx,
Politická	politický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
strachu	strach	k1gInSc2
<g/>
,	,	kIx,
překlad	překlad	k1gInSc1
z	z	k7c2
angličtiny	angličtina	k1gFnSc2
<g/>
:	:	kIx,
Josef	Josef	k1gMnSc1
Šíma	Šíma	k1gMnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
,	,	kIx,
společně	společně	k6eAd1
Alfa	alfa	k1gFnSc1
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86851	#num#	k4
<g/>
-	-	kIx~
<g/>
33	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
Liberální	liberální	k2eAgInSc1d1
institut	institut	k1gInSc1
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86389	#num#	k4
<g/>
-	-	kIx~
<g/>
43	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Kitchinovy	Kitchinův	k2eAgInPc1d1
cykly	cyklus	k1gInPc1
</s>
<s>
Rakouská	rakouský	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
hospodářského	hospodářský	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
</s>
<s>
Monetarismus	Monetarismus	k1gInSc1
</s>
<s>
Keynesiánství	Keynesiánství	k1gNnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Makroekonomie	makroekonomie	k1gFnPc1
Hlavní	hlavní	k2eAgFnPc1d1
koncepty	koncept	k1gInPc4
</s>
<s>
Agregátní	agregátní	k2eAgFnSc1d1
poptávka	poptávka	k1gFnSc1
•	•	k?
Agregátní	agregátní	k2eAgFnSc1d1
nabídka	nabídka	k1gFnSc1
•	•	k?
Hospodářský	hospodářský	k2eAgInSc1d1
cyklus	cyklus	k1gInSc1
•	•	k?
Deflace	deflace	k1gFnSc2
•	•	k?
Poptávkový	poptávkový	k2eAgInSc1d1
šok	šok	k1gInSc1
•	•	k?
Nabídkový	nabídkový	k2eAgInSc1d1
šok	šok	k1gInSc1
<g/>
•	•	k?
Dezinflace	Dezinflace	k1gFnSc2
•	•	k?
Efektivní	efektivní	k2eAgFnSc1d1
poptávka	poptávka	k1gFnSc1
•	•	k?
Očekávání	očekávání	k1gNnSc1
<g/>
(	(	kIx(
<g/>
Adaptivní	adaptivní	k2eAgFnPc1d1
•	•	k?
Racionální	racionální	k2eAgFnPc1d1
<g/>
)	)	kIx)
•	•	k?
Finanční	finanční	k2eAgFnSc2d1
krize	krize	k1gFnSc2
•	•	k?
Hospodářský	hospodářský	k2eAgInSc1d1
růst	růst	k1gInSc1
•	•	k?
Inflace	inflace	k1gFnSc1
<g/>
(	(	kIx(
<g/>
Tahem	tah	k1gInSc7
poptávky	poptávka	k1gFnSc2
•	•	k?
Nákladová	nákladový	k2eAgFnSc1d1
)	)	kIx)
•	•	k?
Úroková	úrokový	k2eAgFnSc1d1
sazba	sazba	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Investice	investice	k1gFnSc1
•	•	k?
Past	pasta	k1gFnPc2
na	na	k7c4
likviditu	likvidita	k1gFnSc4
•	•	k?
Národní	národní	k2eAgInSc1d1
důchod	důchod	k1gInSc1
<g/>
(	(	kIx(
<g/>
HDP	HDP	kA
•	•	k?
HNP	HNP	kA
•	•	k?
ČND	ČND	kA
<g/>
)	)	kIx)
•	•	k?
Microfoundations	Microfoundations	k1gInSc4
•	•	k?
Peníze	peníz	k1gInPc4
<g/>
(	(	kIx(
<g/>
Endogenní	endogenní	k2eAgInSc4d1
<g/>
)	)	kIx)
•	•	k?
Tvorba	tvorba	k1gFnSc1
peněz	peníze	k1gInPc2
•	•	k?
Poptávka	poptávka	k1gFnSc1
po	po	k7c6
penězích	peníze	k1gInPc6
•	•	k?
Preference	preference	k1gFnPc1
likvidity	likvidita	k1gFnSc2
•	•	k?
Peněžní	peněžní	k2eAgFnSc1d1
zásoba	zásoba	k1gFnSc1
•	•	k?
Národní	národní	k2eAgInPc4d1
účty	účet	k1gInPc4
<g/>
(	(	kIx(
<g/>
Systém	systém	k1gInSc1
národních	národní	k2eAgInPc2d1
účtů	účet	k1gInPc2
<g/>
)	)	kIx)
•	•	k?
Nominal	Nominal	k1gMnSc1
rigidity	rigidita	k1gFnSc2
•	•	k?
Cenová	cenový	k2eAgFnSc1d1
hladina	hladina	k1gFnSc1
•	•	k?
Recese	recese	k1gFnSc1
•	•	k?
Shrinkflation	Shrinkflation	k1gInSc1
•	•	k?
Stagflace	Stagflace	k1gFnSc2
•	•	k?
Úspory	úspora	k1gFnSc2
•	•	k?
Nezaměstnanost	nezaměstnanost	k1gFnSc1
Politiky	politika	k1gFnSc2
</s>
<s>
Fiskální	fiskální	k2eAgFnSc1d1
•	•	k?
Monetární	monetární	k2eAgFnSc1d1
•	•	k?
Obchodní	obchodní	k2eAgFnSc1d1
•	•	k?
Centrální	centrální	k2eAgFnSc1d1
banka	banka	k1gFnSc1
Modely	model	k1gInPc1
</s>
<s>
IS-LM	IS-LM	k?
•	•	k?
AD	ad	k7c4
<g/>
–	–	k?
<g/>
AS	as	k1gNnSc4
•	•	k?
Keynesiánský	keynesiánský	k2eAgInSc4d1
kříž	kříž	k1gInSc4
•	•	k?
Multiplikátor	multiplikátor	k1gInSc1
•	•	k?
Akcelerátorový	Akcelerátorový	k2eAgInSc1d1
efekt	efekt	k1gInSc1
•	•	k?
Phillipsova	Phillipsův	k2eAgFnSc1d1
křivka	křivka	k1gFnSc1
•	•	k?
Arrow	Arrow	k1gFnSc2
<g/>
–	–	k?
<g/>
Debreu	Debreus	k1gInSc2
•	•	k?
Harrod	Harroda	k1gFnPc2
<g/>
–	–	k?
<g/>
Domar	Domar	k1gMnSc1
•	•	k?
Solow	Solow	k1gMnSc1
<g/>
–	–	k?
<g/>
Swan	Swan	k1gMnSc1
•	•	k?
Ramsey	Ramsea	k1gFnSc2
<g/>
–	–	k?
<g/>
Cass	Cass	k1gInSc1
<g/>
–	–	k?
<g/>
Koopmans	Koopmans	k1gInSc1
•	•	k?
Model	modla	k1gFnPc2
překrývajících	překrývající	k2eAgFnPc2d1
se	se	k3xPyFc4
generací	generace	k1gFnPc2
•	•	k?
Obecná	obecný	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
rovnováhy	rovnováha	k1gFnSc2
•	•	k?
Teorie	teorie	k1gFnSc1
endogenního	endogenní	k2eAgInSc2d1
růstu	růst	k1gInSc2
•	•	k?
Teorie	teorie	k1gFnSc1
shody	shoda	k1gFnSc2
•	•	k?
Mundell	Mundell	k1gInSc1
<g/>
–	–	k?
<g/>
Fleming	Fleming	k1gInSc1
•	•	k?
Model	model	k1gInSc1
překročení	překročení	k1gNnSc2
•	•	k?
NAIRU	NAIRU	kA
Související	související	k2eAgFnSc1d1
</s>
<s>
Ekonometrie	Ekonometrie	k1gFnSc1
•	•	k?
Ekonomická	ekonomický	k2eAgFnSc1d1
statistika	statistika	k1gFnSc1
•	•	k?
Monetární	monetární	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Rozvojová	rozvojový	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Mezinárodní	mezinárodní	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
Školy	škola	k1gFnSc2
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1
proud	proud	k1gInSc1
</s>
<s>
Keynesiánství	Keynesiánství	k1gNnSc1
(	(	kIx(
<g/>
Neo-Nová	Neo-Nová	k1gFnSc1
<g/>
)	)	kIx)
<g/>
•	•	k?
Monetarismus	Monetarismus	k1gInSc1
•	•	k?
Nová	nový	k2eAgFnSc1d1
klasická	klasický	k2eAgFnSc1d1
makroekonomie	makroekonomie	k1gFnSc1
(	(	kIx(
<g/>
Teorie	teorie	k1gFnSc1
reálného	reálný	k2eAgInSc2d1
obchdního	obchdní	k2eAgInSc2d1
cykklu	cykkl	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Stockholmská	stockholmský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
•	•	k?
Ekonomie	ekonomie	k1gFnSc2
strany	strana	k1gFnSc2
nabídky	nabídka	k1gFnSc2
•	•	k?
Nová	nový	k2eAgFnSc1d1
neoklasická	neoklasický	k2eAgFnSc1d1
syntéza	syntéza	k1gFnSc1
•	•	k?
Slano	slano	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Sladko	sladko	k6eAd1
vodní	vodní	k2eAgFnSc2d1
ekonomie	ekonomie	k1gFnSc2
Heterodox	Heterodox	k1gInSc1
</s>
<s>
Rakouská	rakouský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
•	•	k?
Chartalism	Chartalism	k1gInSc1
(	(	kIx(
<g/>
Moderní	moderní	k2eAgFnSc1d1
monetární	monetární	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Rovnovážná	rovnovážný	k2eAgFnSc1d1
makroekonomie	makroekonomie	k1gFnSc1
•	•	k?
Marxistická	marxistický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Postkeynesiánství	Postkeynesiánství	k1gNnSc1
<g/>
(	(	kIx(
<g/>
Cirkulační	cirkulační	k2eAgFnSc1d1
<g/>
)	)	kIx)
•	•	k?
Tržní	tržní	k2eAgInSc1d1
monetarismus	monetarismus	k1gInSc1
</s>
<s>
Významní	významný	k2eAgMnPc1d1
makroekonomové	makroekonom	k1gMnPc1
</s>
<s>
François	François	k1gFnSc1
Quesnay	Quesnaa	k1gFnSc2
•	•	k?
Adam	Adam	k1gMnSc1
Smith	Smith	k1gMnSc1
•	•	k?
Thomas	Thomas	k1gMnSc1
Robert	Robert	k1gMnSc1
Malthus	Malthus	k1gMnSc1
•	•	k?
Karl	Karl	k1gMnSc1
Marx	Marx	k1gMnSc1
•	•	k?
Léon	Léon	k1gMnSc1
Walras	Walras	k1gMnSc1
•	•	k?
Georg	Georg	k1gMnSc1
Friedrich	Friedrich	k1gMnSc1
Knapp	Knapp	k1gMnSc1
•	•	k?
Knut	knuta	k1gFnPc2
Wicksell	Wicksell	k1gMnSc1
•	•	k?
Irving	Irving	k1gInSc1
Fisher	Fishra	k1gFnPc2
•	•	k?
Wesley	Weslea	k1gFnSc2
Clair	Clair	k1gMnSc1
Mitchell	Mitchell	k1gMnSc1
•	•	k?
John	John	k1gMnSc1
Maynard	Maynard	k1gMnSc1
Keynes	Keynes	k1gMnSc1
•	•	k?
Alvin	Alvin	k1gMnSc1
Hansen	Hansen	k2eAgMnSc1d1
•	•	k?
Michał	Michał	k1gMnSc1
Kalecki	Kaleck	k1gFnSc2
•	•	k?
Gunnar	Gunnar	k1gMnSc1
Myrdal	Myrdal	k1gMnSc1
•	•	k?
Simon	Simon	k1gMnSc1
Kuznets	Kuznetsa	k1gFnPc2
•	•	k?
Joan	Joan	k1gMnSc1
Robinson	Robinson	k1gMnSc1
•	•	k?
Friedrich	Friedrich	k1gMnSc1
Hayek	Hayek	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
John	John	k1gMnSc1
Hicks	Hicksa	k1gFnPc2
•	•	k?
Richard	Richard	k1gMnSc1
Stone	ston	k1gInSc5
•	•	k?
Hyman	Hyman	k1gMnSc1
Minsky	minsky	k6eAd1
•	•	k?
Milton	Milton	k1gInSc1
Friedman	Friedman	k1gMnSc1
•	•	k?
Paul	Paul	k1gMnSc1
Samuelson	Samuelson	k1gMnSc1
•	•	k?
Lawrence	Lawrence	k1gFnSc2
Klein	Klein	k1gMnSc1
•	•	k?
Edmund	Edmund	k1gMnSc1
Phelps	Phelpsa	k1gFnPc2
•	•	k?
Robert	Robert	k1gMnSc1
Lucas	Lucas	k1gMnSc1
Jr	Jr	k1gMnSc1
<g/>
.	.	kIx.
•	•	k?
Edward	Edward	k1gMnSc1
C.	C.	kA
Prescott	Prescott	k1gMnSc1
•	•	k?
Peter	Peter	k1gMnSc1
Diamond	Diamond	k1gMnSc1
•	•	k?
William	William	k1gInSc1
Nordhaus	Nordhaus	k1gMnSc1
•	•	k?
Joseph	Joseph	k1gMnSc1
Stiglitz	Stiglitz	k1gMnSc1
•	•	k?
Thomas	Thomas	k1gMnSc1
J.	J.	kA
Sargent	Sargent	k1gMnSc1
•	•	k?
Paul	Paul	k1gMnSc1
Krugman	Krugman	k1gMnSc1
•	•	k?
Gregory	Gregor	k1gMnPc7
Mankiw	Mankiw	k1gFnPc7
K	k	k7c3
vidění	vidění	k1gNnSc3
</s>
<s>
Ekonomie	ekonomie	k1gFnSc1
•	•	k?
Ekonomika	ekonomik	k1gMnSc2
•	•	k?
Makroekonomický	makroekonomický	k2eAgInSc4d1
model	model	k1gInSc4
•	•	k?
Seznam	seznam	k1gInSc1
významných	významný	k2eAgFnPc2d1
ekonomických	ekonomický	k2eAgFnPc2d1
publikací	publikace	k1gFnPc2
•	•	k?
Mikroekonomie	mikroekonomie	k1gFnSc2
•	•	k?
Politická	politický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Matematická	matematický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4032125-3	4032125-3	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
1456	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85018278	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85018278	#num#	k4
</s>
