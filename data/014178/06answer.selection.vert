<s>
Hospodářský	hospodářský	k2eAgInSc1d1
cyklus	cyklus	k1gInSc1
označuje	označovat	k5eAaImIp3nS
kolísání	kolísání	k1gNnSc4
ekonomické	ekonomický	k2eAgFnSc2d1
aktivity	aktivita	k1gFnSc2
na	na	k7c6
úrovni	úroveň	k1gFnSc6
celé	celý	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
nebo	nebo	k8xC
její	její	k3xOp3gFnSc2
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
(	(	kIx(
<g/>
nikoliv	nikoliv	k9
pouze	pouze	k6eAd1
v	v	k7c6
určitém	určitý	k2eAgInSc6d1
sektoru	sektor	k1gInSc6
<g/>
)	)	kIx)
okolo	okolo	k7c2
dlouhodobého	dlouhodobý	k2eAgInSc2d1
trendu	trend	k1gInSc2
<g/>
.	.	kIx.
</s>