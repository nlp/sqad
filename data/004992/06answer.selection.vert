<s>
Slabiky	slabika	k1gFnPc1	slabika
ainu	ainu	k6eAd1	ainu
jsou	být	k5eAaImIp3nP	být
typu	typa	k1gFnSc4	typa
CV	CV	kA	CV
<g/>
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
tvořené	tvořený	k2eAgNnSc1d1	tvořené
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
souhláskou	souhláska	k1gFnSc7	souhláska
<g/>
,	,	kIx,	,
samohláskou	samohláska	k1gFnSc7	samohláska
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
další	další	k2eAgFnSc7d1	další
souhláskou	souhláska	k1gFnSc7	souhláska
<g/>
)	)	kIx)	)
a	a	k8xC	a
proto	proto	k8xC	proto
zde	zde	k6eAd1	zde
vzniká	vznikat	k5eAaImIp3nS	vznikat
málo	málo	k4c1	málo
skupin	skupina	k1gFnPc2	skupina
souhlásek	souhláska	k1gFnPc2	souhláska
<g/>
.	.	kIx.	.
</s>
