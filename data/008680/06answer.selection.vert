<s>
Mumlavský	Mumlavský	k2eAgInSc1d1	Mumlavský
vodopád	vodopád	k1gInSc1	vodopád
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Mumlava	Mumlava	k1gFnSc1	Mumlava
Waterfall	Waterfall	k1gMnSc1	Waterfall
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
necelých	celý	k2eNgInPc2d1	necelý
10	[number]	k4	10
metrů	metr	k1gInPc2	metr
vysoký	vysoký	k2eAgInSc4d1	vysoký
vodopád	vodopád	k1gInSc4	vodopád
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Libereckém	liberecký	k2eAgInSc6d1	liberecký
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
okrese	okres	k1gInSc6	okres
Semily	Semily	k1gInPc1	Semily
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
asi	asi	k9	asi
2	[number]	k4	2
km	km	kA	km
od	od	k7c2	od
Harrachova	Harrachov	k1gInSc2	Harrachov
<g/>
.	.	kIx.	.
</s>
