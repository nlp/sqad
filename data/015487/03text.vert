<s>
Vladimír	Vladimír	k1gMnSc1
Kubenko	Kubenka	k1gFnSc5
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Kubenko	Kubenka	k1gFnSc5
Narození	narození	k1gNnPc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1924	#num#	k4
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1993	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
68	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Povolání	povolání	k1gNnPc2
</s>
<s>
filmový	filmový	k2eAgMnSc1d1
režisér	režisér	k1gMnSc1
a	a	k8xC
fotograf	fotograf	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
(	(	kIx(
<g/>
Vlado	Vlado	k1gNnSc1
<g/>
)	)	kIx)
Kubenko	Kubenka	k1gFnSc5
(	(	kIx(
<g/>
10	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1924	#num#	k4
–	–	k?
26	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1993	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
slovenský	slovenský	k2eAgMnSc1d1
průkopník	průkopník	k1gMnSc1
slovenského	slovenský	k2eAgInSc2d1
dokumentárního	dokumentární	k2eAgInSc2d1
filmu	film	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
režírování	režírování	k1gNnSc2
pracoval	pracovat	k5eAaImAgMnS
i	i	k9
na	na	k7c6
několika	několik	k4yIc6
scénářích	scénář	k1gInPc6
k	k	k7c3
mnoha	mnoho	k4c3
slovenským	slovenský	k2eAgInPc3d1
filmům	film	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
začátku	začátek	k1gInSc6
své	svůj	k3xOyFgFnSc2
kariéry	kariéra	k1gFnSc2
spolupracoval	spolupracovat	k5eAaImAgMnS
s	s	k7c7
režisérem	režisér	k1gMnSc7
animovaných	animovaný	k2eAgInPc2d1
filmů	film	k1gInPc2
Viktorem	Viktor	k1gMnSc7
Kubalem	Kubalem	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Život	život	k1gInSc1
a	a	k8xC
kariéra	kariéra	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1958	#num#	k4
spolupracoval	spolupracovat	k5eAaImAgInS
se	s	k7c7
Štefanem	Štefan	k1gMnSc7
Kamenickým	Kamenický	k1gMnSc7
na	na	k7c6
reportáži	reportáž	k1gFnSc6
Zaslúžilí	Zaslúžilý	k2eAgMnPc1d1
umelci	umelec	k1gMnPc1
(	(	kIx(
<g/>
Zasloužilí	zasloužilý	k2eAgMnPc1d1
umělci	umělec	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reportáž	reportáž	k1gFnSc1
pojednávala	pojednávat	k5eAaImAgFnS
o	o	k7c6
Miloši	Miloš	k1gMnSc6
Bazovském	Bazovský	k1gMnSc6
a	a	k8xC
Jaroslavu	Jaroslav	k1gMnSc6
Augustovi	August	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1963	#num#	k4
až	až	k9
1969	#num#	k4
realizoval	realizovat	k5eAaBmAgInS
Vlado	Vlado	k1gNnSc4
Kubenko	Kubenka	k1gFnSc5
své	svůj	k3xOyFgInPc4
vrcholné	vrcholný	k2eAgInPc4d1
opusy	opus	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společně	společně	k6eAd1
s	s	k7c7
kolektivem	kolektiv	k1gInSc7
autorů	autor	k1gMnPc2
vytvořil	vytvořit	k5eAaPmAgInS
celovečerní	celovečerní	k2eAgInSc1d1
dokument	dokument	k1gInSc1
Čas	čas	k1gInSc1
<g/>
,	,	kIx,
ktorý	ktorý	k2eAgInSc1d1
žijeme	žít	k5eAaImIp1nP
(	(	kIx(
<g/>
Čas	čas	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
žijeme	žít	k5eAaImIp1nP
<g/>
,	,	kIx,
1968	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokument	dokument	k1gInSc1
reaguje	reagovat	k5eAaBmIp3nS
na	na	k7c4
události	událost	k1gFnPc4
pražského	pražský	k2eAgNnSc2d1
jara	jaro	k1gNnSc2
1968	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
spolu	spolu	k6eAd1
s	s	k7c7
Petrem	Petr	k1gMnSc7
Mihálikem	Mihálik	k1gMnSc7
a	a	k8xC
Dušanem	Dušan	k1gMnSc7
Trančíkem	Trančík	k1gMnSc7
vytvořili	vytvořit	k5eAaPmAgMnP
snímek	snímek	k1gInSc4
Tryzna	tryzna	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
podává	podávat	k5eAaImIp3nS
sugestivní	sugestivní	k2eAgFnSc4d1
zprávu	zpráva	k1gFnSc4
o	o	k7c6
šoku	šok	k1gInSc6
a	a	k8xC
smutku	smutek	k1gInSc6
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
plynuly	plynout	k5eAaImAgFnP
z	z	k7c2
Palachova	Palachův	k2eAgNnSc2d1
sebeupálení	sebeupálení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Film	film	k1gInSc1
byl	být	k5eAaImAgInS
po	po	k7c6
premiéře	premiéra	k1gFnSc6
stažen	stažen	k2eAgInSc4d1
z	z	k7c2
oběhu	oběh	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Kubenko	Kubenka	k1gFnSc5
ve	v	k7c6
svých	svůj	k3xOyFgInPc6
filmech	film	k1gInPc6
upřednostňoval	upřednostňovat	k5eAaImAgMnS
rekonstrukci	rekonstrukce	k1gFnSc4
událostí	událost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
jeho	jeho	k3xOp3gFnSc4
tvorbu	tvorba	k1gFnSc4
v	v	k7c6
šedesátých	šedesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
jsou	být	k5eAaImIp3nP
příznačné	příznačný	k2eAgInPc1d1
tři	tři	k4xCgInPc1
filmy	film	k1gInPc1
<g/>
:	:	kIx,
Návraty	návrat	k1gInPc1
za	za	k7c7
šťastím	šťastí	k1gNnSc7
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Žehra	Žehra	k1gMnSc1
(	(	kIx(
<g/>
1968	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Peklo	peklo	k1gNnSc4
(	(	kIx(
<g/>
1967	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
se	se	k3xPyFc4
Kubenka	Kubenka	k1gFnSc1
soustředil	soustředit	k5eAaPmAgInS
opět	opět	k6eAd1
na	na	k7c4
filmy	film	k1gInPc4
o	o	k7c4
umění	umění	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
těchto	tento	k3xDgInPc6
filmech	film	k1gInPc6
vládne	vládnout	k5eAaImIp3nS
precizní	precizní	k2eAgFnSc1d1
připravenost	připravenost	k1gFnSc1
každého	každý	k3xTgInSc2
záběru	záběr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dílech	díl	k1gInPc6
není	být	k5eNaImIp3nS
reportážní	reportážní	k2eAgFnSc1d1
dynamika	dynamika	k1gFnSc1
<g/>
,	,	kIx,
dominuje	dominovat	k5eAaImIp3nS
lyrizace	lyrizace	k1gFnSc1
a	a	k8xC
poetizace	poetizace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlado	Vlado	k1gNnSc1
Kubenka	Kubenka	k1gFnSc1
zemřel	zemřít	k5eAaPmAgMnS
26	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Filmografie	filmografie	k1gFnSc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
:	:	kIx,
</s>
<s>
Režie	režie	k1gFnSc1
</s>
<s>
1946	#num#	k4
Hurá	hurá	k0
na	na	k7c4
nich	on	k3xPp3gMnPc6
</s>
<s>
1949	#num#	k4
Export-import	Export-import	k1gInSc1
<g/>
,	,	kIx,
Záleží	záležet	k5eAaImIp3nS
na	na	k7c4
nás	my	k3xPp1nPc4
</s>
<s>
1951	#num#	k4
Hudobné	Hudobný	k2eAgFnPc4d1
leto	leto	k6eAd1
<g/>
,	,	kIx,
Prvý	prvý	k4xOgInSc1
máj	máj	k1gInSc1
1951	#num#	k4
<g/>
,	,	kIx,
Za	za	k7c4
300	#num#	k4
000	#num#	k4
výtlačkov	výtlačkov	k1gInSc1
straníckej	straníckat	k5eAaPmRp2nS,k5eAaImRp2nS,k5eAaBmRp2nS
tlače	tlačit	k5eAaImSgInS
</s>
<s>
1952	#num#	k4
N.	N.	kA
S.	S.	kA
Dombrovskij	Dombrovskij	k1gMnSc1
<g/>
,	,	kIx,
Príklad	Príklad	k1gInSc1
trnavského	trnavský	k2eAgInSc2d1
kovos	kovos	k1gInSc4
altu	alt	k1gInSc2
<g/>
,	,	kIx,
Triedny	Triedna	k1gFnSc2
nepriateľ	nepriateľ	k?
</s>
<s>
1954	#num#	k4
Deň	Deň	k1gFnSc1
budúcej	budúcat	k5eAaPmRp2nS,k5eAaImRp2nS
vlasti	vlast	k1gFnSc2
<g/>
,	,	kIx,
P.	P.	kA
O.	O.	kA
Hviezdoslav	Hviezdoslav	k1gMnSc1
</s>
<s>
1955	#num#	k4
V	v	k7c6
službách	služba	k1gFnPc6
divadla	divadlo	k1gNnSc2
<g/>
,	,	kIx,
Mišo	Mišo	k6eAd1
sebepán	sebepán	k2eAgInSc1d1
</s>
<s>
1956	#num#	k4
Ich	Ich	k1gFnPc2
veľký	veľký	k2eAgInSc1d1
deň	deň	k?
<g/>
,	,	kIx,
Deň	Deň	k1gFnSc1
v	v	k7c6
Kodani	Kodaň	k1gFnSc6
<g/>
,	,	kIx,
Vás	vy	k3xPp2nPc6
sa	sa	k?
to	ten	k3xDgNnSc4
netýka	týka	k6eNd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
S	s	k7c7
našimi	náš	k3xOp1gFnPc7
futbalistami	futbalista	k1gFnPc7
v	v	k7c6
Južnej	Južnej	k1gFnSc6
Afrike	Afrik	k1gFnSc2
</s>
<s>
1957	#num#	k4
Kým	kdo	k3yInSc7,k3yRnSc7,k3yQnSc7
vykročia	vykročia	k1gFnSc1
</s>
<s>
1958	#num#	k4
Kambodžská	kambodžský	k2eAgFnSc1d1
vládna	vládnout	k5eAaImSgInS
delegácia	delegácius	k1gMnSc4
<g/>
,	,	kIx,
Na	na	k7c4
hornom	hornom	k1gInSc4
toku	tok	k1gInSc2
<g/>
,	,	kIx,
V	v	k7c4
rodine	rodinout	k5eAaPmIp3nS
Tatrovákov	Tatrovákov	k1gInSc1
</s>
<s>
1959	#num#	k4
Angkor	Angkor	k1gInSc1
Vat	vat	k1gInSc4
<g/>
,	,	kIx,
Bajon	Bajon	k1gInSc4
<g/>
,	,	kIx,
Deň	Deň	k1gFnSc4
v	v	k7c4
Phnom	Phnom	k1gInSc4
Phene	Phen	k1gInSc5
<g/>
,	,	kIx,
Úsmevy	Úsmev	k1gInPc7
Angkoru	Angkor	k1gInSc2
</s>
<s>
1960	#num#	k4
Poddukelská	Poddukelský	k2eAgFnSc1d1
mozaika	mozaika	k1gFnSc1
<g/>
,	,	kIx,
Radostná	radostný	k2eAgFnSc1d1
mládež	mládež	k1gFnSc1
</s>
<s>
1961	#num#	k4
Činohra	činohra	k1gFnSc1
národného	národný	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
<g/>
,	,	kIx,
Zaslúžilí	Zaslúžilý	k2eAgMnPc1d1
umelci	umelec	k1gMnPc1
<g/>
,	,	kIx,
ČSSR	ČSSR	kA
<g/>
,	,	kIx,
Škótsko	Škótsko	k1gNnSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
máj	máj	k1gInSc1
1961	#num#	k4
</s>
<s>
1962	#num#	k4
Tokajík	Tokajík	k1gInSc1
<g/>
,	,	kIx,
To	ten	k3xDgNnSc1
bolo	bola	k1gFnSc5
včera	včera	k6eAd1
</s>
<s>
1963	#num#	k4
Objektívne	Objektívne	k1gFnSc1
s	s	k7c7
objektívom	objektívom	k1gInSc1
</s>
<s>
1965	#num#	k4
Stretnutie	Stretnutie	k1gFnSc1
<g/>
,	,	kIx,
Isyk	Isyk	k1gInSc1
–	–	k?
kuľ	kuľ	k?
</s>
<s>
1966	#num#	k4
Slovenský	slovenský	k2eAgInSc1d1
raj	raj	k?
</s>
<s>
1967	#num#	k4
Peklo	peklo	k1gNnSc1
<g/>
,	,	kIx,
Stretnutie	Stretnutie	k1gFnSc1
s	s	k7c7
Tunisom	Tunisom	k1gInSc1
<g/>
,	,	kIx,
Bienále	bienále	k1gNnSc1
ilustrácii	ilustrácie	k1gFnSc3
Bratislava	Bratislava	k1gFnSc1
<g/>
,	,	kIx,
Insitné	Insitný	k2eAgFnPc1d1
umenie	umenie	k1gFnPc1
<g/>
,	,	kIx,
črty	črta	k1gFnPc1
z	z	k7c2
Indie	Indie	k1gFnSc2
<g/>
,	,	kIx,
Karla	Karel	k1gMnSc2
<g/>
,	,	kIx,
Moderný	Moderný	k2eAgMnSc1d1
indickí	indickí	k1gMnSc1
maliari	maliar	k1gFnSc2
</s>
<s>
1968	#num#	k4
Zasľúbená	Zasľúbený	k2eAgFnSc1d1
zem	zem	k1gFnSc1
<g/>
,	,	kIx,
Žehra	Žehra	k1gFnSc1
<g/>
,	,	kIx,
Čas	čas	k1gInSc1
<g/>
,	,	kIx,
ktorý	ktorý	k2eAgInSc1d1
žijeme	žít	k5eAaImIp1nP
</s>
<s>
1969	#num#	k4
Tryzna	tryzna	k1gFnSc1
<g/>
,	,	kIx,
Návraty	návrat	k1gInPc1
zo	zo	k?
šťastím	šťastit	k5eAaPmIp1nS,k5eAaImIp1nS
<g/>
,	,	kIx,
Bienále	bienále	k1gNnSc1
ilustrácii	ilustrácie	k1gFnSc3
Bratislava	Bratislava	k1gFnSc1
1969	#num#	k4
</s>
<s>
1970	#num#	k4
Svedectvá	Svedectvá	k1gFnSc1
<g/>
,	,	kIx,
Vyznávač	vyznávač	k1gMnSc1
snov	snov	k1gMnSc1
<g/>
,	,	kIx,
Tália	Tália	k1gFnSc1
rokmi	rok	k1gFnPc7
zrejúca	zrejúca	k1gFnSc1
</s>
<s>
1971	#num#	k4
Mimoriadne	Mimoriadne	k1gFnSc1
cvičenie	cvičenie	k1gFnSc1
<g/>
,	,	kIx,
Galanda	Galanda	k1gMnSc1
</s>
<s>
1972	#num#	k4
Kambodža	Kambodža	k1gFnSc1
<g/>
,	,	kIx,
Slovo	slovo	k1gNnSc1
o	o	k7c4
strojoch	strojoch	k1gInSc4
<g/>
,	,	kIx,
Národná	Národný	k2eAgFnSc1d1
umelkyňa	umelkyň	k2eAgFnSc1d1
Ľudmila	Ľudmila	k1gFnSc1
Riznerová-Podjavorinská	Riznerová-Podjavorinský	k2eAgFnSc1d1
<g/>
,	,	kIx,
Bienále	bienále	k1gNnSc1
ilustrácii	ilustrácie	k1gFnSc3
Bratislava	Bratislava	k1gFnSc1
1971	#num#	k4
</s>
<s>
1973	#num#	k4
3	#num#	k4
<g/>
.	.	kIx.
trienále	trienále	k1gNnSc2
insitného	insitný	k2eAgNnSc2d1
umenia	umenium	k1gNnSc2
v	v	k7c6
Bratislave	Bratislav	k1gMnSc5
1972	#num#	k4
<g/>
,	,	kIx,
Vystrúhať	Vystrúhať	k1gFnSc1
znovu	znovu	k6eAd1
dreveného	drevený	k2eAgMnSc2d1
koníka	koník	k1gMnSc2
detstva	detstvo	k1gNnSc2
</s>
<s>
1974	#num#	k4
BIB	BIB	kA
73	#num#	k4
<g/>
,	,	kIx,
Jarné	Jarné	k2eAgInSc3d1
dni	den	k1gInSc3
Pariža	Parižum	k1gNnSc2
<g/>
,	,	kIx,
Kaliste	Kalist	k1gMnSc5
<g/>
,	,	kIx,
So	So	kA
SLUKom	SLUKom	k1gInSc1
o	o	k7c4
SLUKu	sluka	k1gFnSc4
<g/>
,	,	kIx,
Tri	Tri	k1gFnSc4
barančeky	baranček	k1gInPc5
</s>
<s>
1975	#num#	k4
BIB	BIB	kA
75	#num#	k4
<g/>
,	,	kIx,
Kde	kde	k6eAd1
vtáci	vtáce	k1gMnPc1
nelietajú	nelietajú	k?
<g/>
,	,	kIx,
Svet	Svet	k2eAgInSc1d1
rozprávok	rozprávok	k1gInSc1
</s>
<s>
1976	#num#	k4
Nemčík	Nemčík	k1gInSc1
</s>
<s>
1977	#num#	k4
Jar	jar	k1gInSc1
nášho	nášze	k6eAd1
veku	veka	k1gFnSc4
<g/>
,	,	kIx,
Za	za	k7c4
hrsť	hrsť	k1gFnSc4
rodnej	rodnej	k?
zeme	zem	k1gInSc2
<g/>
,	,	kIx,
Na	na	k7c6
prelome	prelom	k1gInSc5
vekov	vekov	k1gInSc4
</s>
<s>
1978	#num#	k4
Bienále	bienále	k1gNnSc1
ilustrácii	ilustrácie	k1gFnSc3
Bratislava	Bratislava	k1gFnSc1
77	#num#	k4
</s>
<s>
1979	#num#	k4
Chladná	chladný	k2eAgFnSc1d1
rána	rána	k1gFnSc1
<g/>
,	,	kIx,
Povesť	Povesť	k1gFnSc1
o	o	k7c4
láske	láske	k1gFnSc4
</s>
<s>
1980	#num#	k4
Bienále	bienále	k1gNnSc1
ilustrácii	ilustrácie	k1gFnSc3
Bratislava	Bratislava	k1gFnSc1
79	#num#	k4
<g/>
,	,	kIx,
Dr	dr	kA
<g/>
.	.	kIx.
Dušan	Dušan	k1gMnSc1
Makovický	Makovický	k2eAgMnSc1d1
</s>
<s>
1981	#num#	k4
Rudolf	Rudolf	k1gMnSc1
Fabry	Fabra	k1gFnPc4
<g/>
,	,	kIx,
Cyprián	Cyprián	k1gMnSc1
Majerník	Majerník	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Hála	Hála	k1gMnSc1
</s>
<s>
1982	#num#	k4
Prečerpávacia	Prečerpávacium	k1gNnPc1
vodná	vodný	k2eAgFnSc1d1
elektráreň	elektráreň	k1gFnSc1
<g/>
,	,	kIx,
Čierny	Čierna	k1gFnPc1
Váh	Váh	k1gInSc1
–	–	k?
horná	hornat	k5eAaPmIp3nS
nádrž	nádrž	k1gFnSc1
<g/>
,	,	kIx,
Prečerpávacia	Prečerpávacia	k1gFnSc1
vodná	vodný	k2eAgFnSc1d1
elektráreň	elektráreň	k1gFnSc1
Čierny	Čierna	k1gFnSc2
Váh	Váh	k1gInSc1
–	–	k?
privádzače	privádzač	k1gInSc2
</s>
<s>
1983	#num#	k4
Obraz	obraz	k1gInSc1
mesta	mesto	k1gNnSc2
<g/>
,	,	kIx,
Prečerpávacia	Prečerpávacia	k1gFnSc1
vodná	vodný	k2eAgFnSc1d1
elektráreň	elektráreň	k1gFnSc1
Čierny	Čierna	k1gFnSc2
Váh	Váh	k1gInSc1
–	–	k?
technológia	technológia	k1gFnSc1
elektrárne	elektrárnout	k5eAaPmIp3nS
<g/>
,	,	kIx,
Prečerpávacia	Prečerpávacia	k1gFnSc1
vodná	vodný	k2eAgFnSc1d1
elektráreň	elektráreň	k1gFnSc1
Čierny	Čierna	k1gFnSc2
Váh	Váh	k1gInSc1
–	–	k?
výstavba	výstavba	k1gFnSc1
komplexu	komplex	k1gInSc2
dolnej	dolnat	k5eAaPmRp2nS,k5eAaImRp2nS
nádrže	nádrž	k1gFnPc1
s	s	k7c7
el.	el.	k?
<g/>
,	,	kIx,
Rudolf	Rudolf	k1gMnSc1
Uher	Uher	k1gMnSc1
</s>
<s>
1984	#num#	k4
Bienále	bienále	k1gNnSc1
ilustrácii	ilustrácie	k1gFnSc3
Bratislava	Bratislava	k1gFnSc1
–	–	k?
BIB	BIB	kA
83	#num#	k4
<g/>
,	,	kIx,
Národný	Národný	k2eAgMnSc1d1
umelec	umelec	k1gMnSc1
Vladimír	Vladimír	k1gMnSc1
Mináč	Mináč	k1gMnSc1
<g/>
,	,	kIx,
Prečerpávacia	Prečerpávacia	k1gFnSc1
vodná	vodný	k2eAgFnSc1d1
elektráreň	elektráreň	k1gFnSc1
Čierny	Čierna	k1gFnSc2
Váh	Váh	k1gInSc1
</s>
<s>
1985	#num#	k4
Dlhá	Dlhá	k1gFnSc1
cesta	cesta	k1gFnSc1
<g/>
,	,	kIx,
Keramika	keramika	k1gFnSc1
z	z	k7c2
Modry	modro	k1gNnPc7
<g/>
,	,	kIx,
Veľká	Veľká	k1gFnSc1
vlastenecká	vlastenecký	k2eAgFnSc1d1
vojna	vojna	k1gFnSc1
</s>
<s>
1987	#num#	k4
Auto-vegetatívne	Auto-vegetatívne	k1gFnSc1
rozmnožovanie	rozmnožovanie	k1gFnSc2
lesných	lesný	k2eAgFnPc2d1
drevín	drevína	k1gFnPc2
–	–	k?
smreka	smreka	k1gMnSc1
<g/>
,	,	kIx,
Jozef	Jozef	k1gMnSc1
Kostka	Kostka	k1gMnSc1
<g/>
,	,	kIx,
Pamäť	Pamäť	k1gFnSc1
kultúry	kultúra	k1gMnSc2
<g/>
,	,	kIx,
Zo	Zo	k1gMnSc2
života	život	k1gInSc2
Zväzu	Zväz	k1gInSc2
družstevných	družstevný	k2eAgMnPc2d1
roľníkov	roľníkov	k1gInSc4
na	na	k7c4
Slovensku	Slovensko	k1gNnSc6
</s>
<s>
1989	#num#	k4
Prečerpávajúca	Prečerpávajúc	k2eAgFnSc1d1
vodná	vodný	k2eAgFnSc1d1
elektráreň	elektráreň	k1gFnSc1
a	a	k8xC
životné	životný	k2eAgFnPc1d1
prostredie	prostredie	k1gFnPc1
<g/>
,	,	kIx,
Rozvoj	rozvoj	k1gInSc1
lesného	lesný	k2eAgNnSc2d1
hospodárstva	hospodárstvo	k1gNnSc2
<g/>
,	,	kIx,
Bienále	bienále	k1gNnSc1
ilustrácii	ilustrácie	k1gFnSc3
Bratislava	Bratislava	k1gFnSc1
1989	#num#	k4
</s>
<s>
1990	#num#	k4
PVE	PVE	kA
I	i	k8xC
peľ	peľ	k?
–	–	k?
príprava	príprava	k1gFnSc1
výstavby	výstavba	k1gFnSc2
</s>
<s>
1993	#num#	k4
Rok	rok	k1gInSc1
nádejí	nádet	k5eAaPmIp3nP,k5eAaImIp3nP
a	a	k8xC
roly	rola	k1gFnPc1
normalizácie	normalizácie	k1gFnSc2
</s>
<s>
Pomocná	pomocný	k2eAgFnSc1d1
režie	režie	k1gFnSc1
</s>
<s>
1948	#num#	k4
Vlčie	Vlčie	k1gFnSc1
diery	diera	k1gFnSc2
</s>
<s>
1950	#num#	k4
Katka	Katka	k1gFnSc1
<g/>
,	,	kIx,
Priehrada	Priehrada	k1gFnSc1
</s>
<s>
Autor	autor	k1gMnSc1
námětu	námět	k1gInSc2
</s>
<s>
1965	#num#	k4
Stretnutie	Stretnutie	k1gFnSc1
</s>
<s>
1968	#num#	k4
Čas	čas	k1gInSc1
<g/>
,	,	kIx,
ktorý	ktorý	k2eAgInSc1d1
žijeme	žít	k5eAaImIp1nP
</s>
<s>
1971	#num#	k4
Mimoriadne	Mimoriadne	k1gFnSc1
cvičenie	cvičenie	k1gFnSc1
</s>
<s>
Scenárista	scenárista	k1gMnSc1
</s>
<s>
1965	#num#	k4
Stretnutie	Stretnutie	k1gFnSc1
</s>
<s>
1971	#num#	k4
Mimoriadne	Mimoriadne	k1gFnSc1
cvičenie	cvičenie	k1gFnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Vladimír	Vladimír	k1gMnSc1
Kubenko	Kubenka	k1gFnSc5
na	na	k7c6
slovenské	slovenský	k2eAgFnSc3d1
Wikipedii	Wikipedie	k1gFnSc3
<g/>
.	.	kIx.
<g/>
↑	↑	k?
http://www.fdb.cz/lidi/20070-vlado-kubenko.html1	http://www.fdb.cz/lidi/20070-vlado-kubenko.html1	k4
2	#num#	k4
(	(	kIx(
<g/>
Dejiny	Dejina	k1gFnPc1
slovenskej	slovenskej	k?
kinematografie	kinematografie	k1gFnSc1
<g/>
,	,	kIx,
Osveta	Osveta	k1gFnSc1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
fdb	fdb	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
Urc	Urc	k1gMnSc1
<g/>
:	:	kIx,
Traja	Traja	k1gMnSc1
veteráni	veterán	k1gMnPc1
za	za	k7c7
kamerou	kamera	k1gFnSc7
<g/>
:	:	kIx,
Traja	Traja	k1gMnSc1
veteráni	veterán	k1gMnPc1
za	za	k7c7
kamerou	kamera	k1gFnSc7
<g/>
:	:	kIx,
Viktor	Viktor	k1gMnSc1
Kubal	Kubal	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
Kubenko	Kubenka	k1gFnSc5
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
Kudelka	Kudelka	k1gMnSc1
<g/>
,	,	kIx,
Bratislava	Bratislava	k1gFnSc1
:	:	kIx,
Národné	Národný	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
pre	pre	k?
audiovizuálne	audiovizuálnout	k5eAaImIp3nS,k5eAaPmIp3nS
umenie	umenie	k1gFnSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-85187-13-2	80-85187-13-2	k4
(	(	kIx(
<g/>
brož	brož	k1gFnSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Kubenko	Kubenka	k1gFnSc5
v	v	k7c6
Internet	Internet	k1gInSc1
Movie	Movie	k1gFnSc2
Database	Databasa	k1gFnSc3
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Kubenko	Kubenka	k1gFnSc5
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc3d1
filmové	filmový	k2eAgFnSc3d1
databázi	databáze	k1gFnSc3
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Kubenko	Kubenka	k1gFnSc5
ve	v	k7c6
Filmové	filmový	k2eAgFnSc3d1
databázi	databáze	k1gFnSc3
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
20000703167	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0815	#num#	k4
3018	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
84038410	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Film	film	k1gInSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Slovensko	Slovensko	k1gNnSc1
</s>
