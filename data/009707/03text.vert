<p>
<s>
Dna	dna	k1gFnSc1	dna
neboli	neboli	k8xC	neboli
pakostnice	pakostnice	k1gFnSc1	pakostnice
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
postihuje	postihovat	k5eAaImIp3nS	postihovat
palec	palec	k1gInSc1	palec
u	u	k7c2	u
nohy	noha	k1gFnSc2	noha
<g/>
,	,	kIx,	,
označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
též	též	k9	též
jako	jako	k9	jako
podagra	podagra	k?	podagra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odborně	odborně	k6eAd1	odborně
arthritis	arthritis	k1gFnSc1	arthritis
urica	urica	k1gFnSc1	urica
(	(	kIx(	(
<g/>
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
pramenech	pramen	k1gInPc6	pramen
arthritis	arthritis	k1gFnSc1	arthritis
uratica	uratic	k2eAgFnSc1d1	uratic
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
onemocnění	onemocnění	k1gNnSc1	onemocnění
obvykle	obvykle	k6eAd1	obvykle
charakterizované	charakterizovaný	k2eAgNnSc1d1	charakterizované
opakujícími	opakující	k2eAgFnPc7d1	opakující
se	se	k3xPyFc4	se
záchvaty	záchvat	k1gInPc1	záchvat
akutní	akutní	k2eAgFnSc2d1	akutní
zánětlivé	zánětlivý	k2eAgFnSc2d1	zánětlivá
artritidy	artritida	k1gFnSc2	artritida
<g/>
,	,	kIx,	,
projevující	projevující	k2eAgFnSc2d1	projevující
se	s	k7c7	s
zarudlým	zarudlý	k2eAgInSc7d1	zarudlý
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
citlivým	citlivý	k2eAgInSc7d1	citlivý
a	a	k8xC	a
horkým	horký	k2eAgInSc7d1	horký
otokem	otok	k1gInSc7	otok
kloubu	kloub	k1gInSc2	kloub
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
je	být	k5eAaImIp3nS	být
postižen	postihnout	k5eAaPmNgInS	postihnout
metatarzofalangeální	metatarzofalangeální	k2eAgInSc1d1	metatarzofalangeální
kloub	kloub	k1gInSc1	kloub
u	u	k7c2	u
kořene	kořen	k1gInSc2	kořen
palce	palec	k1gInSc2	palec
u	u	k7c2	u
nohy	noha	k1gFnSc2	noha
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
50	[number]	k4	50
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nemoc	nemoc	k1gFnSc1	nemoc
se	se	k3xPyFc4	se
ale	ale	k9	ale
může	moct	k5eAaImIp3nS	moct
projevit	projevit	k5eAaPmF	projevit
i	i	k9	i
jako	jako	k9	jako
tofus	tofus	k1gInSc1	tofus
<g/>
,	,	kIx,	,
ledvinové	ledvinový	k2eAgInPc1d1	ledvinový
kameny	kámen	k1gInPc1	kámen
nebo	nebo	k8xC	nebo
urátová	urátový	k2eAgFnSc1d1	urátová
nefropatie	nefropatie	k1gFnSc1	nefropatie
<g/>
.	.	kIx.	.
</s>
<s>
Způsobena	způsoben	k2eAgFnSc1d1	způsobena
je	být	k5eAaImIp3nS	být
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
hladinou	hladina	k1gFnSc7	hladina
kyseliny	kyselina	k1gFnSc2	kyselina
močové	močový	k2eAgFnSc2d1	močová
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
.	.	kIx.	.
</s>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
močová	močový	k2eAgFnSc1d1	močová
krystalizuje	krystalizovat	k5eAaImIp3nS	krystalizovat
a	a	k8xC	a
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
krystalky	krystalek	k1gInPc1	krystalek
se	se	k3xPyFc4	se
ukládají	ukládat	k5eAaImIp3nP	ukládat
v	v	k7c6	v
kloubech	kloub	k1gInPc6	kloub
<g/>
,	,	kIx,	,
šlachách	šlacha	k1gFnPc6	šlacha
a	a	k8xC	a
okolních	okolní	k2eAgFnPc6d1	okolní
tkáních	tkáň	k1gFnPc6	tkáň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Potvrzením	potvrzení	k1gNnSc7	potvrzení
klinické	klinický	k2eAgFnSc2d1	klinická
diagnózy	diagnóza	k1gFnSc2	diagnóza
je	být	k5eAaImIp3nS	být
přítomnost	přítomnost	k1gFnSc1	přítomnost
charakteristických	charakteristický	k2eAgInPc2d1	charakteristický
krystalů	krystal	k1gInPc2	krystal
v	v	k7c6	v
kloubní	kloubní	k2eAgFnSc6d1	kloubní
tekutině	tekutina	k1gFnSc6	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
Symptomy	symptom	k1gInPc1	symptom
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
zlepší	zlepšit	k5eAaPmIp3nS	zlepšit
po	po	k7c6	po
nasazení	nasazení	k1gNnSc6	nasazení
nesteroidních	steroidní	k2eNgInPc2d1	nesteroidní
protizánětlivých	protizánětlivý	k2eAgInPc2d1	protizánětlivý
léků	lék	k1gInPc2	lék
(	(	kIx(	(
<g/>
NSAID	NSAID	kA	NSAID
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
steroidů	steroid	k1gInPc2	steroid
nebo	nebo	k8xC	nebo
kolchicinu	kolchicin	k1gInSc2	kolchicin
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
akutní	akutní	k2eAgInSc1d1	akutní
záchvat	záchvat	k1gInSc1	záchvat
odezní	odeznět	k5eAaPmIp3nS	odeznět
<g/>
,	,	kIx,	,
hladinu	hladina	k1gFnSc4	hladina
kyseliny	kyselina	k1gFnSc2	kyselina
močové	močový	k2eAgFnPc1d1	močová
lze	lze	k6eAd1	lze
obvykle	obvykle	k6eAd1	obvykle
udržovat	udržovat	k5eAaImF	udržovat
na	na	k7c6	na
nízké	nízký	k2eAgFnSc6d1	nízká
hodnotě	hodnota	k1gFnSc6	hodnota
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
změn	změna	k1gFnPc2	změna
životního	životní	k2eAgInSc2d1	životní
stylu	styl	k1gInSc2	styl
<g/>
;	;	kIx,	;
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
častými	častý	k2eAgInPc7d1	častý
záchvaty	záchvat	k1gInPc7	záchvat
přispívá	přispívat	k5eAaImIp3nS	přispívat
podávání	podávání	k1gNnSc1	podávání
allopurinolu	allopurinol	k1gInSc2	allopurinol
či	či	k8xC	či
probenecidu	probenecid	k1gInSc2	probenecid
k	k	k7c3	k
dlouhodobé	dlouhodobý	k2eAgFnSc3d1	dlouhodobá
prevenci	prevence	k1gFnSc3	prevence
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
dny	dna	k1gFnSc2	dna
se	se	k3xPyFc4	se
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
–	–	k?	–
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
okamžiku	okamžik	k1gInSc6	okamžik
života	život	k1gInSc2	život
postihuje	postihovat	k5eAaImIp3nS	postihovat
tato	tento	k3xDgFnSc1	tento
nemoc	nemoc	k1gFnSc1	nemoc
kolem	kolem	k7c2	kolem
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
západního	západní	k2eAgInSc2d1	západní
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
se	se	k3xPyFc4	se
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
nárůstu	nárůst	k1gInSc3	nárůst
dochází	docházet	k5eAaImIp3nP	docházet
rozvojem	rozvoj	k1gInSc7	rozvoj
rizikových	rizikový	k2eAgInPc2d1	rizikový
faktorů	faktor	k1gInPc2	faktor
u	u	k7c2	u
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgMnPc3	jenž
patří	patřit	k5eAaImIp3nS	patřit
metabolický	metabolický	k2eAgInSc1d1	metabolický
syndrom	syndrom	k1gInSc1	syndrom
<g/>
,	,	kIx,	,
prodloužení	prodloužení	k1gNnSc4	prodloužení
očekávané	očekávaný	k2eAgFnSc2d1	očekávaná
délky	délka	k1gFnSc2	délka
života	život	k1gInSc2	život
a	a	k8xC	a
změny	změna	k1gFnSc2	změna
ve	v	k7c6	v
způsobu	způsob	k1gInSc6	způsob
stravování	stravování	k1gNnSc2	stravování
<g/>
.	.	kIx.	.
</s>
<s>
Dna	dna	k1gFnSc1	dna
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
známa	známo	k1gNnSc2	známo
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
nemoc	nemoc	k1gFnSc1	nemoc
králů	král	k1gMnPc2	král
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
nemoc	nemoc	k1gFnSc1	nemoc
bohatých	bohatý	k2eAgMnPc2d1	bohatý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
lékařství	lékařství	k1gNnSc6	lékařství
se	se	k3xPyFc4	se
určitá	určitý	k2eAgFnSc1d1	určitá
forma	forma	k1gFnSc1	forma
dny	den	k1gInPc1	den
označuje	označovat	k5eAaImIp3nS	označovat
podle	podle	k7c2	podle
místa	místo	k1gNnSc2	místo
uložení	uložení	k1gNnSc2	uložení
krystalických	krystalický	k2eAgFnPc2d1	krystalická
hmot	hmota	k1gFnPc2	hmota
kyseliny	kyselina	k1gFnSc2	kyselina
močové	močový	k2eAgFnSc2d1	močová
nebo	nebo	k8xC	nebo
podle	podle	k7c2	podle
místa	místo	k1gNnSc2	místo
vzniku	vznik	k1gInSc2	vznik
záchvatů	záchvat	k1gInPc2	záchvat
<g/>
:	:	kIx,	:
noha	noha	k1gFnSc1	noha
–	–	k?	–
podagra	podagra	k?	podagra
<g/>
,	,	kIx,	,
ruka	ruka	k1gFnSc1	ruka
–	–	k?	–
chiragra	chiragra	k1gFnSc1	chiragra
<g/>
,	,	kIx,	,
rameno	rameno	k1gNnSc1	rameno
–	–	k?	–
omagra	omagr	k1gInSc2	omagr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příznaky	příznak	k1gInPc1	příznak
a	a	k8xC	a
symptomy	symptom	k1gInPc1	symptom
==	==	k?	==
</s>
</p>
<p>
<s>
Dna	dna	k1gFnSc1	dna
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
nejrůznějšími	různý	k2eAgInPc7d3	nejrůznější
způsoby	způsob	k1gInPc7	způsob
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejobvyklejší	obvyklý	k2eAgFnSc1d3	nejobvyklejší
jsou	být	k5eAaImIp3nP	být
opakující	opakující	k2eAgInPc1d1	opakující
se	se	k3xPyFc4	se
záchvaty	záchvat	k1gInPc7	záchvat
akutní	akutní	k2eAgFnSc2d1	akutní
zánětlivé	zánětlivý	k2eAgFnSc2d1	zánětlivá
artritidy	artritida	k1gFnSc2	artritida
(	(	kIx(	(
<g/>
zarudlý	zarudlý	k2eAgInSc4d1	zarudlý
<g/>
,	,	kIx,	,
citlivý	citlivý	k2eAgInSc4d1	citlivý
<g/>
,	,	kIx,	,
horký	horký	k2eAgInSc4d1	horký
a	a	k8xC	a
oteklý	oteklý	k2eAgInSc4d1	oteklý
kloub	kloub	k1gInSc4	kloub
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
je	být	k5eAaImIp3nS	být
postižen	postihnout	k5eAaPmNgInS	postihnout
metatarzofalangeální	metatarzofalangeální	k2eAgInSc1d1	metatarzofalangeální
kloub	kloub	k1gInSc1	kloub
u	u	k7c2	u
kořene	kořen	k1gInSc2	kořen
palce	palec	k1gInSc2	palec
u	u	k7c2	u
nohy	noha	k1gFnSc2	noha
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
všech	všecek	k3xTgInPc2	všecek
případů	případ	k1gInPc2	případ
dny	den	k1gInPc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Zasaženy	zasažen	k2eAgInPc1d1	zasažen
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
ovšem	ovšem	k9	ovšem
i	i	k9	i
jiné	jiný	k2eAgInPc1d1	jiný
klouby	kloub	k1gInPc1	kloub
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
paty	pat	k1gInPc1	pat
<g/>
,	,	kIx,	,
kolena	koleno	k1gNnPc1	koleno
<g/>
,	,	kIx,	,
zápěstí	zápěstí	k1gNnPc1	zápěstí
a	a	k8xC	a
klouby	kloub	k1gInPc1	kloub
prstů	prst	k1gInPc2	prst
na	na	k7c6	na
rukou	ruka	k1gFnPc6	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Bolesti	bolest	k1gFnPc4	bolest
kloubů	kloub	k1gInPc2	kloub
obvykle	obvykle	k6eAd1	obvykle
začínají	začínat	k5eAaImIp3nP	začínat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
je	být	k5eAaImIp3nS	být
snížená	snížený	k2eAgFnSc1d1	snížená
tělesná	tělesný	k2eAgFnSc1d1	tělesná
teplota	teplota	k1gFnSc1	teplota
po	po	k7c6	po
ulehnutí	ulehnutí	k1gNnSc6	ulehnutí
ke	k	k7c3	k
spánku	spánek	k1gInSc3	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Bolest	bolest	k1gFnSc4	bolest
kloubů	kloub	k1gInPc2	kloub
mohou	moct	k5eAaImIp3nP	moct
občas	občas	k6eAd1	občas
doprovázet	doprovázet	k5eAaImF	doprovázet
i	i	k9	i
jiné	jiný	k2eAgInPc1d1	jiný
symptomy	symptom	k1gInPc1	symptom
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
vyčerpanost	vyčerpanost	k1gFnSc1	vyčerpanost
a	a	k8xC	a
horečka	horečka	k1gFnSc1	horečka
<g/>
.	.	kIx.	.
<g/>
Dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
hladina	hladina	k1gFnSc1	hladina
kyseliny	kyselina	k1gFnSc2	kyselina
močové	močový	k2eAgFnSc2d1	močová
(	(	kIx(	(
<g/>
hyperurikemie	hyperurikemie	k1gFnSc2	hyperurikemie
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
dalším	další	k2eAgInPc3d1	další
symptomům	symptom	k1gInPc3	symptom
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
tvrdých	tvrdý	k2eAgInPc2d1	tvrdý
<g/>
,	,	kIx,	,
nebolestivých	bolestivý	k2eNgNnPc2d1	nebolestivé
ložisek	ložisko	k1gNnPc2	ložisko
krystalů	krystal	k1gInPc2	krystal
kyseliny	kyselina	k1gFnSc2	kyselina
močové	močový	k2eAgFnPc1d1	močová
<g/>
,	,	kIx,	,
označovaných	označovaný	k2eAgFnPc2d1	označovaná
jako	jako	k9	jako
tofy	tofa	k1gFnPc1	tofa
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
výskyt	výskyt	k1gInSc1	výskyt
tofů	tof	k1gInPc2	tof
může	moct	k5eAaImIp3nS	moct
pak	pak	k6eAd1	pak
vést	vést	k5eAaImF	vést
k	k	k7c3	k
chronické	chronický	k2eAgFnSc3d1	chronická
artritidě	artritida	k1gFnSc3	artritida
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
eroze	eroze	k1gFnSc2	eroze
kostí	kost	k1gFnPc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
hladina	hladina	k1gFnSc1	hladina
kyseliny	kyselina	k1gFnSc2	kyselina
močové	močový	k2eAgFnSc2d1	močová
může	moct	k5eAaImIp3nS	moct
rovněž	rovněž	k9	rovněž
způsobit	způsobit	k5eAaPmF	způsobit
ukládání	ukládání	k1gNnSc4	ukládání
jejích	její	k3xOp3gInPc2	její
krystalů	krystal	k1gInPc2	krystal
v	v	k7c6	v
ledvinách	ledvina	k1gFnPc6	ledvina
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
kamenů	kámen	k1gInPc2	kámen
a	a	k8xC	a
následné	následný	k2eAgFnSc6d1	následná
urátové	urátový	k2eAgFnSc6d1	urátová
nefropatii	nefropatie	k1gFnSc6	nefropatie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příčina	příčina	k1gFnSc1	příčina
==	==	k?	==
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnSc7d1	základní
příčinou	příčina	k1gFnSc7	příčina
dny	dna	k1gFnSc2	dna
je	být	k5eAaImIp3nS	být
hyperurikémie	hyperurikémie	k1gFnSc1	hyperurikémie
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vyvolána	vyvolat	k5eAaPmNgFnS	vyvolat
celou	celý	k2eAgFnSc7d1	celá
řadou	řada	k1gFnSc7	řada
faktorů	faktor	k1gInPc2	faktor
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
skladby	skladba	k1gFnSc2	skladba
jídelníčku	jídelníček	k1gInSc2	jídelníček
<g/>
,	,	kIx,	,
genetických	genetický	k2eAgFnPc2d1	genetická
dispozic	dispozice	k1gFnPc2	dispozice
nebo	nebo	k8xC	nebo
snížené	snížený	k2eAgFnPc1d1	snížená
sekrece	sekrece	k1gFnPc1	sekrece
urátů	urát	k1gInPc2	urát
<g/>
,	,	kIx,	,
solí	sůl	k1gFnPc2	sůl
kyseliny	kyselina	k1gFnSc2	kyselina
močové	močový	k2eAgFnSc2d1	močová
<g/>
.	.	kIx.	.
</s>
<s>
Snížené	snížený	k2eAgNnSc1d1	snížené
vylučování	vylučování	k1gNnSc1	vylučování
kyseliny	kyselina	k1gFnSc2	kyselina
močové	močový	k2eAgFnSc2d1	močová
ledvinami	ledvina	k1gFnPc7	ledvina
je	být	k5eAaImIp3nS	být
primární	primární	k2eAgFnSc7d1	primární
příčinou	příčina	k1gFnSc7	příčina
hyperurikémie	hyperurikémie	k1gFnSc2	hyperurikémie
v	v	k7c6	v
přibližně	přibližně	k6eAd1	přibližně
90	[number]	k4	90
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
nadměrná	nadměrný	k2eAgFnSc1d1	nadměrná
produkce	produkce	k1gFnSc1	produkce
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c7	za
pouhými	pouhý	k2eAgNnPc7d1	pouhé
10	[number]	k4	10
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jistém	jistý	k2eAgNnSc6d1	jisté
období	období	k1gNnSc6	období
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
dna	dna	k1gFnSc1	dna
projeví	projevit	k5eAaPmIp3nS	projevit
u	u	k7c2	u
asi	asi	k9	asi
10	[number]	k4	10
%	%	kIx~	%
osob	osoba	k1gFnPc2	osoba
trpících	trpící	k2eAgFnPc2d1	trpící
hyperurikémií	hyperurikémie	k1gFnPc2	hyperurikémie
<g/>
.	.	kIx.	.
</s>
<s>
Riziko	riziko	k1gNnSc1	riziko
rozvinutí	rozvinutí	k1gNnSc2	rozvinutí
dny	dna	k1gFnSc2	dna
se	se	k3xPyFc4	se
však	však	k9	však
liší	lišit	k5eAaImIp3nP	lišit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
stupni	stupeň	k1gInSc6	stupeň
hyperurikémie	hyperurikémie	k1gFnSc2	hyperurikémie
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
hodnoty	hodnota	k1gFnSc2	hodnota
hladiny	hladina	k1gFnSc2	hladina
kyseliny	kyselina	k1gFnSc2	kyselina
močové	močový	k2eAgFnSc2d1	močová
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
mezi	mezi	k7c4	mezi
415	[number]	k4	415
a	a	k8xC	a
530	[number]	k4	530
μ	μ	k?	μ
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
(	(	kIx(	(
<g/>
7	[number]	k4	7
a	a	k8xC	a
8,9	[number]	k4	8,9
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
dl	dl	k?	dl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
riziko	riziko	k1gNnSc1	riziko
je	být	k5eAaImIp3nS	být
0,5	[number]	k4	0,5
%	%	kIx~	%
ročně	ročně	k6eAd1	ročně
<g/>
;	;	kIx,	;
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
hodnotou	hodnota	k1gFnSc7	hodnota
vyšší	vysoký	k2eAgFnSc7d2	vyšší
než	než	k8xS	než
535	[number]	k4	535
μ	μ	k?	μ
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
(	(	kIx(	(
<g/>
9	[number]	k4	9
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
dl	dl	k?	dl
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
riziko	riziko	k1gNnSc1	riziko
4,5	[number]	k4	4,5
%	%	kIx~	%
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Životní	životní	k2eAgInSc4d1	životní
styl	styl	k1gInSc4	styl
===	===	k?	===
</s>
</p>
<p>
<s>
Složení	složení	k1gNnSc1	složení
jídelníčku	jídelníček	k1gInSc2	jídelníček
je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
asi	asi	k9	asi
12	[number]	k4	12
%	%	kIx~	%
případů	případ	k1gInPc2	případ
dny	dna	k1gFnSc2	dna
a	a	k8xC	a
souvisí	souviset	k5eAaImIp3nS	souviset
se	s	k7c7	s
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
konzumací	konzumace	k1gFnSc7	konzumace
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
,	,	kIx,	,
fruktózy	fruktóza	k1gFnPc1	fruktóza
ze	z	k7c2	z
slazených	slazený	k2eAgInPc2d1	slazený
nápojů	nápoj	k1gInPc2	nápoj
<g/>
,	,	kIx,	,
masa	maso	k1gNnSc2	maso
a	a	k8xC	a
mořských	mořský	k2eAgInPc2d1	mořský
plodů	plod	k1gInPc2	plod
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgMnPc3d1	další
spouštěcím	spouštěcí	k2eAgMnPc3d1	spouštěcí
faktorům	faktor	k1gMnPc3	faktor
patří	patřit	k5eAaImIp3nS	patřit
tělesné	tělesný	k2eAgNnSc1d1	tělesné
poranění	poranění	k1gNnSc1	poranění
a	a	k8xC	a
prodělaný	prodělaný	k2eAgInSc1d1	prodělaný
chirurgický	chirurgický	k2eAgInSc1d1	chirurgický
zákrok	zákrok	k1gInSc1	zákrok
<g/>
.	.	kIx.	.
</s>
<s>
Nedávno	nedávno	k6eAd1	nedávno
provedené	provedený	k2eAgFnPc1d1	provedená
studie	studie	k1gFnPc1	studie
prokázaly	prokázat	k5eAaPmAgFnP	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jisté	jistý	k2eAgFnPc4d1	jistá
složky	složka	k1gFnPc4	složka
jídelníčku	jídelníček	k1gInSc2	jídelníček
<g/>
,	,	kIx,	,
kdysi	kdysi	k6eAd1	kdysi
považované	považovaný	k2eAgInPc4d1	považovaný
za	za	k7c4	za
faktory	faktor	k1gInPc4	faktor
se	s	k7c7	s
dnou	dna	k1gFnSc7	dna
související	související	k2eAgMnPc1d1	související
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
neovlivňují	ovlivňovat	k5eNaImIp3nP	ovlivňovat
–	–	k?	–
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
včetně	včetně	k7c2	včetně
konzumace	konzumace	k1gFnSc2	konzumace
na	na	k7c4	na
purin	purin	k1gInSc4	purin
bohaté	bohatý	k2eAgFnSc2d1	bohatá
zeleniny	zelenina	k1gFnSc2	zelenina
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
fazolí	fazole	k1gFnPc2	fazole
<g/>
,	,	kIx,	,
hrachu	hrách	k1gInSc2	hrách
<g/>
,	,	kIx,	,
čočky	čočka	k1gFnSc2	čočka
a	a	k8xC	a
špenátu	špenát	k1gInSc2	špenát
<g/>
)	)	kIx)	)
a	a	k8xC	a
celkového	celkový	k2eAgNnSc2d1	celkové
množství	množství	k1gNnSc2	množství
bílkovin	bílkovina	k1gFnPc2	bílkovina
přijatých	přijatý	k2eAgFnPc2d1	přijatá
ve	v	k7c6	v
stravě	strava	k1gFnSc6	strava
<g/>
.	.	kIx.	.
</s>
<s>
Konzumace	konzumace	k1gFnSc1	konzumace
kávy	káva	k1gFnSc2	káva
<g/>
,	,	kIx,	,
vitamínu	vitamín	k1gInSc2	vitamín
C	C	kA	C
a	a	k8xC	a
mléčných	mléčný	k2eAgInPc2d1	mléčný
výrobků	výrobek	k1gInPc2	výrobek
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
tělesná	tělesný	k2eAgFnSc1d1	tělesná
zdatnost	zdatnost	k1gFnSc1	zdatnost
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zdají	zdát	k5eAaImIp3nP	zdát
riziko	riziko	k1gNnSc4	riziko
nemoci	nemoc	k1gFnSc2	nemoc
snižovat	snižovat	k5eAaImF	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
všeho	všecek	k3xTgNnSc2	všecek
částečně	částečně	k6eAd1	částečně
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
jejich	jejich	k3xOp3gNnSc1	jejich
vlivem	vliv	k1gInSc7	vliv
na	na	k7c6	na
snižování	snižování	k1gNnSc6	snižování
inzulinové	inzulinový	k2eAgFnSc2d1	inzulinová
rezistence	rezistence	k1gFnSc2	rezistence
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Genetika	genetika	k1gFnSc1	genetika
===	===	k?	===
</s>
</p>
<p>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
dny	dna	k1gFnSc2	dna
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
genetickou	genetický	k2eAgFnSc7d1	genetická
záležitostí	záležitost	k1gFnSc7	záležitost
-	-	kIx~	-
genetické	genetický	k2eAgInPc1d1	genetický
faktory	faktor	k1gInPc1	faktor
přispívají	přispívat	k5eAaImIp3nP	přispívat
k	k	k7c3	k
přibližně	přibližně	k6eAd1	přibližně
60	[number]	k4	60
%	%	kIx~	%
variability	variabilita	k1gFnPc1	variabilita
hladiny	hladina	k1gFnSc2	hladina
kyseliny	kyselina	k1gFnSc2	kyselina
močové	močový	k2eAgFnSc2d1	močová
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výskytem	výskyt	k1gInSc7	výskyt
dny	dna	k1gFnSc2	dna
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
spojovány	spojovat	k5eAaImNgInP	spojovat
tři	tři	k4xCgInPc1	tři
geny	gen	k1gInPc1	gen
<g/>
,	,	kIx,	,
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
SLC	SLC	kA	SLC
<g/>
2	[number]	k4	2
<g/>
A	a	k9	a
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
SLC22A12	SLC22A12	k1gFnSc1	SLC22A12
a	a	k8xC	a
ABCG	ABCG	kA	ABCG
<g/>
2	[number]	k4	2
<g/>
;	;	kIx,	;
variace	variace	k1gFnPc1	variace
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
skladbě	skladba	k1gFnSc6	skladba
mohou	moct	k5eAaImIp3nP	moct
riziko	riziko	k1gNnSc4	riziko
onemocnění	onemocnění	k1gNnSc2	onemocnění
přibližně	přibližně	k6eAd1	přibližně
zdvojnásobit	zdvojnásobit	k5eAaPmF	zdvojnásobit
<g/>
.	.	kIx.	.
</s>
<s>
Mutace	mutace	k1gFnSc1	mutace
vedoucí	vedoucí	k1gFnSc1	vedoucí
ke	k	k7c3	k
ztrátě	ztráta	k1gFnSc3	ztráta
funkce	funkce	k1gFnSc2	funkce
u	u	k7c2	u
genů	gen	k1gInPc2	gen
SLC2A9	SLC2A9	k1gFnSc2	SLC2A9
a	a	k8xC	a
SLC22A12	SLC22A12	k1gFnSc2	SLC22A12
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
dědičnou	dědičný	k2eAgFnSc4d1	dědičná
hypourikémii	hypourikémie	k1gFnSc4	hypourikémie
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
snižují	snižovat	k5eAaImIp3nP	snižovat
absorpci	absorpce	k1gFnSc4	absorpce
urátů	urát	k1gInPc2	urát
a	a	k8xC	a
zapříčiňují	zapříčiňovat	k5eAaImIp3nP	zapříčiňovat
jejich	jejich	k3xOp3gFnSc4	jejich
neomezenou	omezený	k2eNgFnSc4d1	neomezená
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Dna	dna	k1gFnSc1	dna
je	být	k5eAaImIp3nS	být
také	také	k9	také
komplikací	komplikace	k1gFnPc2	komplikace
několika	několik	k4yIc2	několik
vzácných	vzácný	k2eAgFnPc2d1	vzácná
genetických	genetický	k2eAgFnPc2d1	genetická
poruch	porucha	k1gFnPc2	porucha
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgFnPc3	jenž
patří	patřit	k5eAaImIp3nS	patřit
familiární	familiární	k2eAgFnSc1d1	familiární
juvenilní	juvenilní	k2eAgFnSc1d1	juvenilní
hyperurikemická	hyperurikemický	k2eAgFnSc1d1	hyperurikemický
nefropatie	nefropatie	k1gFnSc1	nefropatie
<g/>
,	,	kIx,	,
medulární	medulární	k2eAgFnSc1d1	medulární
cystická	cystický	k2eAgFnSc1d1	cystická
nemoc	nemoc	k1gFnSc1	nemoc
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
,	,	kIx,	,
nadměrná	nadměrný	k2eAgFnSc1d1	nadměrná
aktivita	aktivita	k1gFnSc1	aktivita
fosforibosyl-pyrofosfát	fosforibosylyrofosfát	k5eAaPmF	fosforibosyl-pyrofosfát
syntetázy	syntetáza	k1gFnPc4	syntetáza
a	a	k8xC	a
nedostatek	nedostatek	k1gInSc4	nedostatek
hypoxantin-guanin	hypoxantinuanina	k1gFnPc2	hypoxantin-guanina
fosforibosyltransferázy	fosforibosyltransferáza	k1gFnSc2	fosforibosyltransferáza
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
provází	provázet	k5eAaImIp3nS	provázet
Lesch-Nyhanův	Lesch-Nyhanův	k2eAgInSc4d1	Lesch-Nyhanův
syndrom	syndrom	k1gInSc4	syndrom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgNnPc1d1	další
onemocnění	onemocnění	k1gNnPc1	onemocnění
===	===	k?	===
</s>
</p>
<p>
<s>
Dna	dna	k1gFnSc1	dna
se	se	k3xPyFc4	se
často	často	k6eAd1	často
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c4	v
kombinaci	kombinace	k1gFnSc4	kombinace
s	s	k7c7	s
jinými	jiný	k2eAgNnPc7d1	jiné
onemocněními	onemocnění	k1gNnPc7	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téměř	téměř	k6eAd1	téměř
75	[number]	k4	75
%	%	kIx~	%
případů	případ	k1gInPc2	případ
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
metabolický	metabolický	k2eAgInSc1d1	metabolický
syndrom	syndrom	k1gInSc1	syndrom
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
kombinace	kombinace	k1gFnSc1	kombinace
abdominální	abdominální	k2eAgFnSc2d1	abdominální
obezity	obezita	k1gFnSc2	obezita
<g/>
,	,	kIx,	,
vysokého	vysoký	k2eAgInSc2d1	vysoký
krevního	krevní	k2eAgInSc2d1	krevní
tlaku	tlak	k1gInSc2	tlak
<g/>
,	,	kIx,	,
inzulinové	inzulinový	k2eAgFnSc2d1	inzulinová
rezistence	rezistence	k1gFnSc2	rezistence
a	a	k8xC	a
abnormální	abnormální	k2eAgFnSc2d1	abnormální
hladiny	hladina	k1gFnSc2	hladina
lipidů	lipid	k1gInPc2	lipid
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgNnPc3d1	další
onemocněním	onemocnění	k1gNnPc3	onemocnění
běžně	běžně	k6eAd1	běžně
komplikovaným	komplikovaný	k2eAgNnSc7d1	komplikované
dnou	dna	k1gFnSc7	dna
patří	patřit	k5eAaImIp3nS	patřit
polycytémie	polycytémie	k1gFnSc1	polycytémie
<g/>
,	,	kIx,	,
otrava	otrava	k1gFnSc1	otrava
olovem	olovo	k1gNnSc7	olovo
<g/>
,	,	kIx,	,
selhání	selhání	k1gNnSc4	selhání
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
,	,	kIx,	,
hemolytická	hemolytický	k2eAgFnSc1d1	hemolytická
anémie	anémie	k1gFnSc1	anémie
<g/>
,	,	kIx,	,
lupénka	lupénka	k1gFnSc1	lupénka
a	a	k8xC	a
transplantace	transplantace	k1gFnSc1	transplantace
solidních	solidní	k2eAgInPc2d1	solidní
orgánů	orgán	k1gInPc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Index	index	k1gInSc1	index
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
vyšší	vysoký	k2eAgMnSc1d2	vyšší
nebo	nebo	k8xC	nebo
roven	roven	k2eAgInSc1d1	roven
35	[number]	k4	35
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
riziko	riziko	k1gNnSc4	riziko
dny	den	k1gInPc4	den
trojnásobně	trojnásobně	k6eAd1	trojnásobně
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobé	dlouhodobý	k2eAgNnSc1d1	dlouhodobé
vystavení	vystavení	k1gNnSc1	vystavení
působení	působení	k1gNnSc2	působení
olova	olovo	k1gNnSc2	olovo
a	a	k8xC	a
alkoholu	alkohol	k1gInSc2	alkohol
obsahujícího	obsahující	k2eAgNnSc2d1	obsahující
olovo	olovo	k1gNnSc1	olovo
jsou	být	k5eAaImIp3nP	být
rizikovými	rizikový	k2eAgInPc7d1	rizikový
faktory	faktor	k1gInPc7	faktor
kvůli	kvůli	k7c3	kvůli
škodlivému	škodlivý	k2eAgInSc3d1	škodlivý
účinku	účinek	k1gInSc3	účinek
tohoto	tento	k3xDgInSc2	tento
prvku	prvek	k1gInSc2	prvek
na	na	k7c4	na
funkci	funkce	k1gFnSc4	funkce
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
.	.	kIx.	.
</s>
<s>
Lesch-Nyhanův	Lesch-Nyhanův	k2eAgInSc1d1	Lesch-Nyhanův
syndrom	syndrom	k1gInSc1	syndrom
často	často	k6eAd1	často
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
dnavou	dnavý	k2eAgFnSc7d1	dnavá
artritidou	artritida	k1gFnSc7	artritida
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Léky	lék	k1gInPc1	lék
===	===	k?	===
</s>
</p>
<p>
<s>
Se	s	k7c7	s
záchvaty	záchvat	k1gInPc7	záchvat
dny	dna	k1gFnSc2	dna
bývají	bývat	k5eAaImIp3nP	bývat
často	často	k6eAd1	často
dávána	dávat	k5eAaImNgFnS	dávat
do	do	k7c2	do
souvislosti	souvislost	k1gFnSc2	souvislost
diuretika	diuretikum	k1gNnSc2	diuretikum
<g/>
.	.	kIx.	.
</s>
<s>
Nízké	nízký	k2eAgFnPc1d1	nízká
dávky	dávka	k1gFnPc1	dávka
hydrochlorothiazidu	hydrochlorothiazida	k1gFnSc4	hydrochlorothiazida
však	však	k9	však
podle	podle	k7c2	podle
všeho	všecek	k3xTgNnSc2	všecek
riziko	riziko	k1gNnSc1	riziko
nezvyšují	zvyšovat	k5eNaImIp3nP	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
léky	lék	k1gInPc1	lék
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
na	na	k7c6	na
dnu	den	k1gInSc6	den
vliv	vliv	k1gInSc4	vliv
<g/>
,	,	kIx,	,
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
niacin	niacin	k1gInSc1	niacin
a	a	k8xC	a
aspirin	aspirin	k1gInSc1	aspirin
(	(	kIx(	(
<g/>
kyselina	kyselina	k1gFnSc1	kyselina
acetylsalicylová	acetylsalicylový	k2eAgFnSc1d1	acetylsalicylová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Imunosupresivní	Imunosupresivní	k2eAgInPc1d1	Imunosupresivní
léky	lék	k1gInPc1	lék
cyklosporin	cyklosporin	k1gInSc1	cyklosporin
a	a	k8xC	a
takrolimus	takrolimus	k1gInSc1	takrolimus
mohou	moct	k5eAaImIp3nP	moct
rovněž	rovněž	k9	rovněž
dnu	dna	k1gFnSc4	dna
vyvolat	vyvolat	k5eAaPmF	vyvolat
<g/>
;	;	kIx,	;
cyklosporin	cyklosporin	k1gInSc1	cyklosporin
pak	pak	k6eAd1	pak
zejména	zejména	k9	zejména
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
hydrochlorothiazidem	hydrochlorothiazid	k1gInSc7	hydrochlorothiazid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Patofyziologie	patofyziologie	k1gFnSc2	patofyziologie
==	==	k?	==
</s>
</p>
<p>
<s>
Dna	dna	k1gFnSc1	dna
je	být	k5eAaImIp3nS	být
poruchou	porucha	k1gFnSc7	porucha
metabolismu	metabolismus	k1gInSc2	metabolismus
purinů	purin	k1gInPc2	purin
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
<g/>
,	,	kIx,	,
když	když	k8xS	když
jeho	jeho	k3xOp3gInSc1	jeho
výsledný	výsledný	k2eAgInSc1d1	výsledný
produkt	produkt	k1gInSc1	produkt
<g/>
,	,	kIx,	,
kyselina	kyselina	k1gFnSc1	kyselina
močová	močový	k2eAgFnSc1d1	močová
<g/>
,	,	kIx,	,
krystalizuje	krystalizovat	k5eAaImIp3nS	krystalizovat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
monosodium	monosodium	k1gNnSc1	monosodium
urátu	urát	k1gInSc2	urát
a	a	k8xC	a
ukládá	ukládat	k5eAaImIp3nS	ukládat
se	se	k3xPyFc4	se
v	v	k7c6	v
kloubech	kloub	k1gInPc6	kloub
<g/>
,	,	kIx,	,
na	na	k7c6	na
šlachách	šlacha	k1gFnPc6	šlacha
a	a	k8xC	a
v	v	k7c6	v
okolních	okolní	k2eAgFnPc6d1	okolní
tkáních	tkáň	k1gFnPc6	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Krystaly	krystal	k1gInPc1	krystal
monosodium	monosodium	k1gNnSc1	monosodium
urátu	urát	k1gInSc2	urát
pak	pak	k6eAd1	pak
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
místní	místní	k2eAgFnSc4d1	místní
imunitně	imunitně	k6eAd1	imunitně
zprostředkovanou	zprostředkovaný	k2eAgFnSc4d1	zprostředkovaná
zánětlivou	zánětlivý	k2eAgFnSc4d1	zánětlivá
reakci	reakce	k1gFnSc4	reakce
<g/>
;	;	kIx,	;
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
klíčových	klíčový	k2eAgInPc2d1	klíčový
proteinů	protein	k1gInPc2	protein
zánětlivé	zánětlivý	k2eAgFnSc2d1	zánětlivá
kaskády	kaskáda	k1gFnSc2	kaskáda
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
interleukin	interleukin	k1gInSc1	interleukin
1	[number]	k4	1
<g/>
β	β	k?	β
Absence	absence	k1gFnSc1	absence
urikázy	urikáza	k1gFnSc2	urikáza
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
evoluce	evoluce	k1gFnSc2	evoluce
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
vyšších	vysoký	k2eAgMnPc2d2	vyšší
primátů	primát	k1gMnPc2	primát
a	a	k8xC	a
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
rozkládat	rozkládat	k5eAaImF	rozkládat
kyselinu	kyselina	k1gFnSc4	kyselina
močovou	močový	k2eAgFnSc4d1	močová
<g/>
,	,	kIx,	,
způsobila	způsobit	k5eAaPmAgFnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
onemocnění	onemocnění	k1gNnSc1	onemocnění
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
.	.	kIx.	.
<g/>
Spouštěcí	spouštěcí	k2eAgInPc1d1	spouštěcí
faktory	faktor	k1gInPc1	faktor
pro	pro	k7c4	pro
ukládání	ukládání	k1gNnSc4	ukládání
kyseliny	kyselina	k1gFnSc2	kyselina
močové	močový	k2eAgFnPc1d1	močová
nebyly	být	k5eNaImAgFnP	být
doposud	doposud	k6eAd1	doposud
zcela	zcela	k6eAd1	zcela
objasněny	objasněn	k2eAgFnPc1d1	objasněna
<g/>
.	.	kIx.	.
</s>
<s>
Krystalizovat	krystalizovat	k5eAaImF	krystalizovat
může	moct	k5eAaImIp3nS	moct
tato	tento	k3xDgFnSc1	tento
látka	látka	k1gFnSc1	látka
i	i	k9	i
při	při	k7c6	při
běžné	běžný	k2eAgFnSc6d1	běžná
hladině	hladina	k1gFnSc6	hladina
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
<g/>
,	,	kIx,	,
mnohem	mnohem	k6eAd1	mnohem
pravděpodobnější	pravděpodobný	k2eAgFnSc1d2	pravděpodobnější
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
při	při	k7c6	při
hladině	hladina	k1gFnSc6	hladina
zvýšené	zvýšený	k2eAgFnSc6d1	zvýšená
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
faktory	faktor	k1gInPc1	faktor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
všeho	všecek	k3xTgNnSc2	všecek
při	při	k7c6	při
spuštění	spuštění	k1gNnSc6	spuštění
epizody	epizoda	k1gFnSc2	epizoda
akutní	akutní	k2eAgFnSc2d1	akutní
artritidy	artritida	k1gFnSc2	artritida
důležité	důležitý	k2eAgFnPc1d1	důležitá
<g/>
,	,	kIx,	,
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
nízkou	nízký	k2eAgFnSc4d1	nízká
teplotu	teplota	k1gFnSc4	teplota
<g/>
,	,	kIx,	,
náhlé	náhlý	k2eAgFnPc1d1	náhlá
změny	změna	k1gFnPc1	změna
hladiny	hladina	k1gFnSc2	hladina
kyseliny	kyselina	k1gFnSc2	kyselina
močové	močový	k2eAgFnSc2d1	močová
<g/>
,	,	kIx,	,
acidózu	acidóza	k1gFnSc4	acidóza
<g/>
,	,	kIx,	,
výpotek	výpotek	k1gInSc4	výpotek
v	v	k7c6	v
kloubech	kloub	k1gInPc6	kloub
a	a	k8xC	a
výskyt	výskyt	k1gInSc1	výskyt
proteinů	protein	k1gInPc2	protein
mezibuněčné	mezibuněčný	k2eAgFnSc2d1	mezibuněčná
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
proteoglykanů	proteoglykan	k1gInPc2	proteoglykan
<g/>
,	,	kIx,	,
kolagenu	kolagen	k1gInSc2	kolagen
a	a	k8xC	a
chondroitin	chondroitin	k1gInSc4	chondroitin
sulfátu	sulfát	k1gInSc2	sulfát
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšené	zvýšený	k2eAgNnSc1d1	zvýšené
ukládání	ukládání	k1gNnSc1	ukládání
při	při	k7c6	při
nízkých	nízký	k2eAgFnPc6d1	nízká
teplotách	teplota	k1gFnPc6	teplota
částečně	částečně	k6eAd1	částečně
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
jsou	být	k5eAaImIp3nP	být
nejčastěji	často	k6eAd3	často
ovlivněny	ovlivněn	k2eAgInPc4d1	ovlivněn
klouby	kloub	k1gInPc4	kloub
chodidel	chodidlo	k1gNnPc2	chodidlo
<g/>
.	.	kIx.	.
</s>
<s>
Náhlé	náhlý	k2eAgFnPc1d1	náhlá
změny	změna	k1gFnPc1	změna
hladiny	hladina	k1gFnSc2	hladina
kyseliny	kyselina	k1gFnSc2	kyselina
močové	močový	k2eAgFnSc2d1	močová
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
způsobeny	způsobit	k5eAaPmNgInP	způsobit
řadou	řada	k1gFnSc7	řada
faktorů	faktor	k1gInPc2	faktor
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
poranění	poranění	k1gNnSc2	poranění
<g/>
,	,	kIx,	,
chirurgického	chirurgický	k2eAgInSc2d1	chirurgický
zákroku	zákrok	k1gInSc2	zákrok
<g/>
,	,	kIx,	,
chemoterapie	chemoterapie	k1gFnSc2	chemoterapie
<g/>
,	,	kIx,	,
diuretik	diuretikum	k1gNnPc2	diuretikum
a	a	k8xC	a
zahájení	zahájení	k1gNnSc6	zahájení
nebo	nebo	k8xC	nebo
ukončení	ukončení	k1gNnSc6	ukončení
užívání	užívání	k1gNnSc2	užívání
allopurinolu	allopurinol	k1gInSc2	allopurinol
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
přípravky	přípravek	k1gInPc7	přípravek
na	na	k7c4	na
léčbu	léčba	k1gFnSc4	léčba
vysokého	vysoký	k2eAgInSc2d1	vysoký
krevního	krevní	k2eAgInSc2d1	krevní
tlaku	tlak	k1gInSc2	tlak
znamenají	znamenat	k5eAaImIp3nP	znamenat
blokátory	blokátor	k1gInPc1	blokátor
vápníkových	vápníkový	k2eAgInPc2d1	vápníkový
kanálů	kanál	k1gInPc2	kanál
a	a	k8xC	a
losartan	losartan	k1gInSc4	losartan
nižší	nízký	k2eAgNnSc4d2	nižší
riziko	riziko	k1gNnSc4	riziko
dny	dna	k1gFnSc2	dna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diagnóza	diagnóza	k1gFnSc1	diagnóza
==	==	k?	==
</s>
</p>
<p>
<s>
Dnu	dna	k1gFnSc4	dna
lze	lze	k6eAd1	lze
diagnostikovat	diagnostikovat	k5eAaBmF	diagnostikovat
a	a	k8xC	a
léčit	léčit	k5eAaImF	léčit
bez	bez	k7c2	bez
dalších	další	k2eAgNnPc2d1	další
vyšetření	vyšetření	k1gNnPc2	vyšetření
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
trpících	trpící	k2eAgFnPc2d1	trpící
hyperurikémií	hyperurikémie	k1gFnPc2	hyperurikémie
a	a	k8xC	a
klasickou	klasický	k2eAgFnSc7d1	klasická
podagrou	podagrou	k?	podagrou
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
však	však	k9	však
jsou	být	k5eAaImIp3nP	být
o	o	k7c6	o
diagnóze	diagnóza	k1gFnSc6	diagnóza
pochybnosti	pochybnost	k1gFnPc4	pochybnost
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
by	by	kYmCp3nS	by
být	být	k5eAaImF	být
provedena	provést	k5eAaPmNgFnS	provést
analýza	analýza	k1gFnSc1	analýza
synoviální	synoviální	k2eAgFnSc2d1	synoviální
tekutiny	tekutina	k1gFnSc2	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
Rentgenové	rentgenový	k2eAgNnSc1d1	rentgenové
snímkování	snímkování	k1gNnSc1	snímkování
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
stanovení	stanovení	k1gNnSc4	stanovení
diagnózy	diagnóza	k1gFnSc2	diagnóza
chronické	chronický	k2eAgInPc1d1	chronický
dny	den	k1gInPc1	den
velmi	velmi	k6eAd1	velmi
užitečné	užitečný	k2eAgInPc1d1	užitečný
<g/>
,	,	kIx,	,
u	u	k7c2	u
akutních	akutní	k2eAgInPc2d1	akutní
záchvatů	záchvat	k1gInPc2	záchvat
je	být	k5eAaImIp3nS	být
však	však	k9	však
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
jen	jen	k9	jen
minimálně	minimálně	k6eAd1	minimálně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Synoviální	synoviální	k2eAgFnSc1d1	synoviální
tekutina	tekutina	k1gFnSc1	tekutina
===	===	k?	===
</s>
</p>
<p>
<s>
Konečná	konečný	k2eAgFnSc1d1	konečná
diagnóza	diagnóza	k1gFnSc1	diagnóza
dny	den	k1gInPc4	den
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c4	na
identifikaci	identifikace	k1gFnSc4	identifikace
krystalů	krystal	k1gInPc2	krystal
monosodium	monosodium	k1gNnSc4	monosodium
urátu	urát	k1gInSc2	urát
v	v	k7c6	v
synoviální	synoviální	k2eAgFnSc6d1	synoviální
tekutině	tekutina	k1gFnSc6	tekutina
(	(	kIx(	(
<g/>
tekutině	tekutina	k1gFnSc6	tekutina
obklopující	obklopující	k2eAgInPc1d1	obklopující
klouby	kloub	k1gInPc1	kloub
<g/>
)	)	kIx)	)
či	či	k8xC	či
na	na	k7c6	na
výskytu	výskyt	k1gInSc6	výskyt
tofu	tofu	k1gNnSc2	tofu
<g/>
.	.	kIx.	.
</s>
<s>
Veškeré	veškerý	k3xTgInPc1	veškerý
vzorky	vzorek	k1gInPc1	vzorek
synoviální	synoviální	k2eAgFnSc2d1	synoviální
tekutiny	tekutina	k1gFnSc2	tekutina
získané	získaný	k2eAgFnSc2d1	získaná
z	z	k7c2	z
nediagnostikovaných	diagnostikovaný	k2eNgInPc2d1	nediagnostikovaný
zanícených	zanícený	k2eAgInPc2d1	zanícený
kloubů	kloub	k1gInPc2	kloub
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
analýze	analýza	k1gFnSc3	analýza
na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
krystaly	krystal	k1gInPc4	krystal
podrobeny	podroben	k2eAgInPc4d1	podroben
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zkoumání	zkoumání	k1gNnSc6	zkoumání
mikroskopem	mikroskop	k1gInSc7	mikroskop
využívajícím	využívající	k2eAgFnPc3d1	využívající
polarizovaného	polarizovaný	k2eAgNnSc2d1	polarizované
světla	světlo	k1gNnSc2	světlo
mají	mít	k5eAaImIp3nP	mít
tyto	tento	k3xDgInPc1	tento
krystaly	krystal	k1gInPc1	krystal
strukturu	struktura	k1gFnSc4	struktura
podobnou	podobný	k2eAgFnSc4d1	podobná
jehlám	jehla	k1gFnPc3	jehla
a	a	k8xC	a
silný	silný	k2eAgInSc4d1	silný
negativní	negativní	k2eAgInSc4d1	negativní
dvojlom	dvojlom	k1gInSc4	dvojlom
<g/>
.	.	kIx.	.
</s>
<s>
Provedení	provedení	k1gNnSc1	provedení
uvedeného	uvedený	k2eAgInSc2d1	uvedený
testu	test	k1gInSc2	test
je	být	k5eAaImIp3nS	být
složité	složitý	k2eAgNnSc1d1	složité
a	a	k8xC	a
často	často	k6eAd1	často
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
zkušeného	zkušený	k2eAgMnSc4d1	zkušený
odborníka	odborník	k1gMnSc4	odborník
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
tekutina	tekutina	k1gFnSc1	tekutina
zkoumána	zkoumán	k2eAgFnSc1d1	zkoumána
relativně	relativně	k6eAd1	relativně
rychle	rychle	k6eAd1	rychle
po	po	k7c6	po
odběru	odběr	k1gInSc6	odběr
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
teplota	teplota	k1gFnSc1	teplota
a	a	k8xC	a
pH	ph	kA	ph
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
její	její	k3xOp3gFnSc4	její
rozpustnost	rozpustnost	k1gFnSc4	rozpustnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Krevní	krevní	k2eAgInPc4d1	krevní
testy	test	k1gInPc4	test
===	===	k?	===
</s>
</p>
<p>
<s>
Hyperurikémie	Hyperurikémie	k1gFnSc1	Hyperurikémie
je	být	k5eAaImIp3nS	být
klasickým	klasický	k2eAgInSc7d1	klasický
znakem	znak	k1gInSc7	znak
dny	dna	k1gFnSc2	dna
<g/>
;	;	kIx,	;
v	v	k7c6	v
téměř	téměř	k6eAd1	téměř
polovině	polovina	k1gFnSc6	polovina
případů	případ	k1gInPc2	případ
dny	dna	k1gFnSc2	dna
se	se	k3xPyFc4	se
však	však	k9	však
neobjevuje	objevovat	k5eNaImIp3nS	objevovat
a	a	k8xC	a
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
osob	osoba	k1gFnPc2	osoba
se	s	k7c7	s
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
hladinou	hladina	k1gFnSc7	hladina
kyseliny	kyselina	k1gFnSc2	kyselina
močové	močový	k2eAgFnSc2d1	močová
se	se	k3xPyFc4	se
dna	dno	k1gNnSc2	dno
nikdy	nikdy	k6eAd1	nikdy
nerozvine	rozvinout	k5eNaPmIp3nS	rozvinout
<g/>
.	.	kIx.	.
</s>
<s>
Diagnostická	diagnostický	k2eAgFnSc1d1	diagnostická
hodnota	hodnota	k1gFnSc1	hodnota
měření	měření	k1gNnSc2	měření
hladiny	hladina	k1gFnSc2	hladina
kyseliny	kyselina	k1gFnSc2	kyselina
močové	močový	k2eAgFnSc2d1	močová
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
omezená	omezený	k2eAgFnSc1d1	omezená
<g/>
.	.	kIx.	.
</s>
<s>
Hyperurikémie	Hyperurikémie	k1gFnSc1	Hyperurikémie
je	být	k5eAaImIp3nS	být
definována	definovat	k5eAaBmNgFnS	definovat
jako	jako	k9	jako
hladina	hladina	k1gFnSc1	hladina
urátů	urát	k1gInPc2	urát
v	v	k7c6	v
plazmě	plazma	k1gFnSc6	plazma
vyšší	vysoký	k2eAgFnSc4d2	vyšší
než	než	k8xS	než
420	[number]	k4	420
μ	μ	k?	μ
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
(	(	kIx(	(
<g/>
7,0	[number]	k4	7,0
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
dl	dl	k?	dl
<g/>
)	)	kIx)	)
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
360	[number]	k4	360
μ	μ	k?	μ
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
(	(	kIx(	(
<g/>
6,0	[number]	k4	6,0
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
dl	dl	k?	dl
<g/>
)	)	kIx)	)
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
často	často	k6eAd1	často
prováděným	prováděný	k2eAgInPc3d1	prováděný
krevním	krevní	k2eAgInPc3d1	krevní
testům	test	k1gInPc3	test
patří	patřit	k5eAaImIp3nS	patřit
zjišťování	zjišťování	k1gNnSc1	zjišťování
počtu	počet	k1gInSc2	počet
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
<g/>
,	,	kIx,	,
hladiny	hladina	k1gFnPc1	hladina
elektrolytů	elektrolyt	k1gInPc2	elektrolyt
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
,	,	kIx,	,
funkce	funkce	k1gFnSc1	funkce
ledvin	ledvina	k1gFnPc2	ledvina
a	a	k8xC	a
sedimentace	sedimentace	k1gFnSc2	sedimentace
červených	červený	k2eAgFnPc2d1	červená
krvinek	krvinka	k1gFnPc2	krvinka
(	(	kIx(	(
<g/>
ESR	ESR	kA	ESR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
počtu	počet	k1gInSc2	počet
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
a	a	k8xC	a
ESR	ESR	kA	ESR
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
u	u	k7c2	u
dny	dna	k1gFnSc2	dna
dojít	dojít	k5eAaPmF	dojít
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
organismus	organismus	k1gInSc4	organismus
napaden	napaden	k2eAgInSc4d1	napaden
infekcí	infekce	k1gFnSc7	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
dokonce	dokonce	k9	dokonce
zaznamenán	zaznamenán	k2eAgInSc1d1	zaznamenán
počet	počet	k1gInSc1	počet
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
o	o	k7c6	o
hodnotě	hodnota	k1gFnSc6	hodnota
40,0	[number]	k4	40,0
<g/>
×	×	k?	×
<g/>
109	[number]	k4	109
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
(	(	kIx(	(
<g/>
40	[number]	k4	40
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
/	/	kIx~	/
<g/>
mm	mm	kA	mm
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Diferenciální	diferenciální	k2eAgFnSc1d1	diferenciální
diagnostika	diagnostika	k1gFnSc1	diagnostika
===	===	k?	===
</s>
</p>
<p>
<s>
Nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
prvkem	prvek	k1gInSc7	prvek
diferenciální	diferenciální	k2eAgFnSc2d1	diferenciální
diagnostiky	diagnostika	k1gFnSc2	diagnostika
u	u	k7c2	u
dny	dna	k1gFnSc2	dna
je	být	k5eAaImIp3nS	být
vyloučení	vyloučení	k1gNnSc1	vyloučení
septické	septický	k2eAgFnSc2d1	septická
artritidy	artritida	k1gFnSc2	artritida
<g/>
.	.	kIx.	.
</s>
<s>
Tu	ten	k3xDgFnSc4	ten
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
uvážit	uvážit	k5eAaPmF	uvážit
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
se	s	k7c7	s
znaky	znak	k1gInPc7	znak
infekce	infekce	k1gFnSc2	infekce
nebo	nebo	k8xC	nebo
u	u	k7c2	u
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
po	po	k7c6	po
léčbě	léčba	k1gFnSc6	léčba
nelepší	lepšit	k5eNaImIp3nS	lepšit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přesnému	přesný	k2eAgNnSc3d1	přesné
stanovení	stanovení	k1gNnSc3	stanovení
diagnózy	diagnóza	k1gFnSc2	diagnóza
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
provést	provést	k5eAaPmF	provést
Gramovo	Gramův	k2eAgNnSc4d1	Gramovo
barvení	barvení	k1gNnSc4	barvení
synoviální	synoviální	k2eAgFnSc2d1	synoviální
tekutiny	tekutina	k1gFnSc2	tekutina
a	a	k8xC	a
bakteriologický	bakteriologický	k2eAgInSc4d1	bakteriologický
rozbor	rozbor	k1gInSc4	rozbor
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgNnPc3d1	další
onemocněním	onemocnění	k1gNnPc3	onemocnění
s	s	k7c7	s
podobnými	podobný	k2eAgInPc7d1	podobný
příznaky	příznak	k1gInPc7	příznak
jako	jako	k8xS	jako
dna	dna	k1gFnSc1	dna
patří	patřit	k5eAaImIp3nS	patřit
pseudodna	pseudodna	k1gFnSc1	pseudodna
a	a	k8xC	a
revmatoidní	revmatoidní	k2eAgFnSc1d1	revmatoidní
artritida	artritida	k1gFnSc1	artritida
<g/>
.	.	kIx.	.
</s>
<s>
Dnavý	dnavý	k2eAgInSc1d1	dnavý
tofus	tofus	k1gInSc1	tofus
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
kloubu	kloub	k1gInSc2	kloub
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
mylně	mylně	k6eAd1	mylně
pokládán	pokládat	k5eAaImNgInS	pokládat
za	za	k7c4	za
karcinom	karcinom	k1gInSc4	karcinom
bazálních	bazální	k2eAgFnPc2d1	bazální
buněk	buňka	k1gFnPc2	buňka
nebo	nebo	k8xC	nebo
jiný	jiný	k2eAgInSc4d1	jiný
druh	druh	k1gInSc4	druh
nádoru	nádor	k1gInSc2	nádor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Prevence	prevence	k1gFnSc1	prevence
==	==	k?	==
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
hladiny	hladina	k1gFnSc2	hladina
kyseliny	kyselina	k1gFnSc2	kyselina
močové	močový	k2eAgFnSc2d1	močová
přispívají	přispívat	k5eAaImIp3nP	přispívat
jak	jak	k6eAd1	jak
změny	změna	k1gFnPc1	změna
životního	životní	k2eAgInSc2d1	životní
stylu	styl	k1gInSc2	styl
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
léky	lék	k1gInPc1	lék
<g/>
.	.	kIx.	.
</s>
<s>
Účinné	účinný	k2eAgFnPc1d1	účinná
změny	změna	k1gFnPc1	změna
životního	životní	k2eAgInSc2d1	životní
stylu	styl	k1gInSc2	styl
a	a	k8xC	a
jídelníčku	jídelníček	k1gInSc3	jídelníček
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
sníženou	snížený	k2eAgFnSc4d1	snížená
konzumaci	konzumace	k1gFnSc4	konzumace
masa	maso	k1gNnSc2	maso
a	a	k8xC	a
mořských	mořský	k2eAgInPc2d1	mořský
plodů	plod	k1gInPc2	plod
<g/>
,	,	kIx,	,
adekvátní	adekvátní	k2eAgInSc1d1	adekvátní
příjem	příjem	k1gInSc1	příjem
vitamínu	vitamín	k1gInSc2	vitamín
C	C	kA	C
<g/>
,	,	kIx,	,
omezení	omezení	k1gNnSc1	omezení
konzumace	konzumace	k1gFnSc2	konzumace
alkoholu	alkohol	k1gInSc2	alkohol
a	a	k8xC	a
fruktózy	fruktóza	k1gFnSc2	fruktóza
a	a	k8xC	a
předcházení	předcházení	k1gNnSc2	předcházení
obezitě	obezita	k1gFnSc3	obezita
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
obézních	obézní	k2eAgMnPc2d1	obézní
mužů	muž	k1gMnPc2	muž
snížila	snížit	k5eAaPmAgFnS	snížit
nízkokalorická	nízkokalorický	k2eAgFnSc1d1	nízkokalorická
dieta	dieta	k1gFnSc1	dieta
hladinu	hladina	k1gFnSc4	hladina
kyseliny	kyselina	k1gFnSc2	kyselina
močové	močový	k2eAgFnSc2d1	močová
o	o	k7c6	o
100	[number]	k4	100
μ	μ	k?	μ
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
(	(	kIx(	(
<g/>
1,7	[number]	k4	1,7
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
dl	dl	k?	dl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Konzumace	konzumace	k1gFnSc1	konzumace
vitamínu	vitamín	k1gInSc2	vitamín
C	C	kA	C
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
1500	[number]	k4	1500
mg	mg	kA	mg
za	za	k7c4	za
den	den	k1gInSc4	den
snižuje	snižovat	k5eAaImIp3nS	snižovat
riziko	riziko	k1gNnSc1	riziko
dny	dna	k1gFnSc2	dna
o	o	k7c4	o
45	[number]	k4	45
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Riziko	riziko	k1gNnSc1	riziko
dny	dna	k1gFnSc2	dna
snižuje	snižovat	k5eAaImIp3nS	snižovat
také	také	k9	také
pití	pití	k1gNnSc1	pití
černé	černý	k2eAgFnSc2d1	černá
kávy	káva	k1gFnSc2	káva
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
však	však	k9	však
čaje	čaj	k1gInSc2	čaj
<g/>
.	.	kIx.	.
</s>
<s>
Dna	dna	k1gFnSc1	dna
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
rozvinout	rozvinout	k5eAaPmF	rozvinout
i	i	k9	i
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
spánkové	spánkový	k2eAgFnSc2d1	spánková
apnoe	apno	k1gFnSc2	apno
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
uvolňování	uvolňování	k1gNnSc3	uvolňování
purinů	purin	k1gInPc2	purin
z	z	k7c2	z
buněk	buňka	k1gFnPc2	buňka
nedostatečně	dostatečně	k6eNd1	dostatečně
zásobených	zásobený	k2eAgMnPc2d1	zásobený
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
spánkové	spánkový	k2eAgFnSc2d1	spánková
apnoe	apno	k1gFnSc2	apno
pak	pak	k6eAd1	pak
snižuje	snižovat	k5eAaImIp3nS	snižovat
výskyt	výskyt	k1gInSc1	výskyt
záchvatů	záchvat	k1gInPc2	záchvat
dny	den	k1gInPc4	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Léčba	léčba	k1gFnSc1	léčba
==	==	k?	==
</s>
</p>
<p>
<s>
Prvotním	prvotní	k2eAgInSc7d1	prvotní
cílem	cíl	k1gInSc7	cíl
léčby	léčba	k1gFnSc2	léčba
je	být	k5eAaImIp3nS	být
ustálit	ustálit	k5eAaPmF	ustálit
symptomy	symptom	k1gInPc4	symptom
akutního	akutní	k2eAgInSc2d1	akutní
záchvatu	záchvat	k1gInSc2	záchvat
dny	den	k1gInPc1	den
<g/>
.	.	kIx.	.
</s>
<s>
Opakovaným	opakovaný	k2eAgInPc3d1	opakovaný
záchvatům	záchvat	k1gInPc3	záchvat
lze	lze	k6eAd1	lze
předcházet	předcházet	k5eAaImF	předcházet
různými	různý	k2eAgInPc7d1	různý
léky	lék	k1gInPc7	lék
používanými	používaný	k2eAgInPc7d1	používaný
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
hladiny	hladina	k1gFnSc2	hladina
kyseliny	kyselina	k1gFnSc2	kyselina
močové	močový	k2eAgFnSc2d1	močová
v	v	k7c6	v
krevním	krevní	k2eAgNnSc6d1	krevní
séru	sérum	k1gNnSc6	sérum
<g/>
.	.	kIx.	.
</s>
<s>
Bolest	bolest	k1gFnSc4	bolest
snižují	snižovat	k5eAaImIp3nP	snižovat
ledové	ledový	k2eAgInPc1d1	ledový
obklady	obklad	k1gInPc1	obklad
aplikované	aplikovaný	k2eAgInPc1d1	aplikovaný
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
20	[number]	k4	20
až	až	k9	až
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
několikrát	několikrát	k6eAd1	několikrát
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Léčbu	léčba	k1gFnSc4	léčba
akutních	akutní	k2eAgInPc2d1	akutní
případů	případ	k1gInPc2	případ
lze	lze	k6eAd1	lze
provádět	provádět	k5eAaImF	provádět
pomocí	pomocí	k7c2	pomocí
nesteroidních	steroidní	k2eNgInPc2d1	nesteroidní
protizánětlivých	protizánětlivý	k2eAgInPc2d1	protizánětlivý
léků	lék	k1gInPc2	lék
(	(	kIx(	(
<g/>
NSAID	NSAID	kA	NSAID
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kolchicinu	kolchicin	k1gInSc2	kolchicin
a	a	k8xC	a
steroidů	steroid	k1gInPc2	steroid
<g/>
,	,	kIx,	,
prevencí	prevence	k1gFnSc7	prevence
je	být	k5eAaImIp3nS	být
podávání	podávání	k1gNnSc1	podávání
allopurinolu	allopurinol	k1gInSc2	allopurinol
<g/>
,	,	kIx,	,
febuxostatu	febuxostat	k1gInSc2	febuxostat
a	a	k8xC	a
probenecidu	probenecid	k1gInSc2	probenecid
<g/>
.	.	kIx.	.
</s>
<s>
Snížení	snížení	k1gNnSc1	snížení
hladiny	hladina	k1gFnSc2	hladina
kyseliny	kyselina	k1gFnSc2	kyselina
močové	močový	k2eAgNnSc1d1	močové
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
vyléčení	vyléčení	k1gNnSc3	vyléčení
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
také	také	k9	také
léčba	léčba	k1gFnSc1	léčba
souběžných	souběžný	k2eAgNnPc2d1	souběžné
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Léky	lék	k1gInPc1	lék
NSAID	NSAID	kA	NSAID
===	===	k?	===
</s>
</p>
<p>
<s>
Léky	lék	k1gInPc1	lék
NSAID	NSAID	kA	NSAID
jsou	být	k5eAaImIp3nP	být
obvyklým	obvyklý	k2eAgInSc7d1	obvyklý
prvotním	prvotní	k2eAgInSc7d1	prvotní
způsobem	způsob	k1gInSc7	způsob
léčby	léčba	k1gFnSc2	léčba
dny	dna	k1gFnSc2	dna
<g/>
;	;	kIx,	;
žádný	žádný	k3yNgMnSc1	žádný
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
není	být	k5eNaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
méně	málo	k6eAd2	málo
či	či	k8xC	či
více	hodně	k6eAd2	hodně
efektivní	efektivní	k2eAgFnPc1d1	efektivní
než	než	k8xS	než
ostatní	ostatní	k2eAgFnPc1d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Zlepšení	zlepšení	k1gNnSc1	zlepšení
bývá	bývat	k5eAaImIp3nS	bývat
patrné	patrný	k2eAgNnSc1d1	patrné
po	po	k7c6	po
čtyřech	čtyři	k4xCgFnPc6	čtyři
hodinách	hodina	k1gFnPc6	hodina
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
léčba	léčba	k1gFnSc1	léčba
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
jednoho	jeden	k4xCgInSc2	jeden
až	až	k9	až
dvou	dva	k4xCgInPc2	dva
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
léků	lék	k1gInPc2	lék
se	se	k3xPyFc4	se
však	však	k9	však
nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
jistými	jistý	k2eAgInPc7d1	jistý
zdravotními	zdravotní	k2eAgInPc7d1	zdravotní
problémy	problém	k1gInPc7	problém
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
gastrointestinální	gastrointestinální	k2eAgNnSc4d1	gastrointestinální
krvácení	krvácení	k1gNnSc4	krvácení
<g/>
,	,	kIx,	,
selhání	selhání	k1gNnSc4	selhání
ledvin	ledvina	k1gFnPc2	ledvina
nebo	nebo	k8xC	nebo
selhání	selhání	k1gNnSc4	selhání
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Indomethacin	Indomethacin	k1gInSc1	Indomethacin
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
případech	případ	k1gInPc6	případ
dny	den	k1gInPc1	den
historicky	historicky	k6eAd1	historicky
nejvyužívanějším	využívaný	k2eAgInSc7d3	nejvyužívanější
lékem	lék	k1gInSc7	lék
NSAID	NSAID	kA	NSAID
<g/>
;	;	kIx,	;
některým	některý	k3yIgMnPc3	některý
nemocným	nemocný	k1gMnPc3	nemocný
však	však	k9	však
lze	lze	k6eAd1	lze
přednostně	přednostně	k6eAd1	přednostně
podávat	podávat	k5eAaImF	podávat
alternativní	alternativní	k2eAgInPc4d1	alternativní
léky	lék	k1gInPc4	lék
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
ibuprofen	ibuprofen	k1gInSc1	ibuprofen
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
sice	sice	k8xC	sice
nejsou	být	k5eNaImIp3nP	být
tak	tak	k6eAd1	tak
efektivní	efektivní	k2eAgFnPc1d1	efektivní
<g/>
,	,	kIx,	,
nezpůsobují	způsobovat	k5eNaImIp3nP	způsobovat
však	však	k9	však
tolik	tolik	k4yIc4	tolik
nežádoucích	žádoucí	k2eNgInPc2d1	nežádoucí
účinků	účinek	k1gInPc2	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Osobám	osoba	k1gFnPc3	osoba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
po	po	k7c4	po
užití	užití	k1gNnSc4	užití
léků	lék	k1gInPc2	lék
NSAID	NSAID	kA	NSAID
vystaveny	vystavit	k5eAaPmNgInP	vystavit
nežádoucím	žádoucí	k2eNgInPc3d1	nežádoucí
gastrointestinálním	gastrointestinální	k2eAgInPc3d1	gastrointestinální
účinkům	účinek	k1gInPc3	účinek
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
předepsat	předepsat	k5eAaPmF	předepsat
dodatečný	dodatečný	k2eAgInSc4d1	dodatečný
inhibitor	inhibitor	k1gInSc4	inhibitor
protonové	protonový	k2eAgFnSc2d1	protonová
pumpy	pumpa	k1gFnSc2	pumpa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kolchicin	kolchicin	k1gInSc4	kolchicin
===	===	k?	===
</s>
</p>
<p>
<s>
Kolchicin	kolchicin	k1gInSc1	kolchicin
je	být	k5eAaImIp3nS	být
alternativním	alternativní	k2eAgInSc7d1	alternativní
přípravkem	přípravek	k1gInSc7	přípravek
pro	pro	k7c4	pro
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
léky	lék	k1gInPc4	lék
NSAID	NSAID	kA	NSAID
nesnášejí	snášet	k5eNaImIp3nP	snášet
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
nežádoucí	žádoucí	k2eNgInPc1d1	nežádoucí
účinky	účinek	k1gInPc1	účinek
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
gastrointestinální	gastrointestinální	k2eAgFnPc1d1	gastrointestinální
obtíže	obtíž	k1gFnPc1	obtíž
<g/>
)	)	kIx)	)
omezují	omezovat	k5eAaImIp3nP	omezovat
jeho	jeho	k3xOp3gNnSc4	jeho
širší	široký	k2eAgNnSc4d2	širší
využití	využití	k1gNnSc4	využití
<g/>
.	.	kIx.	.
</s>
<s>
Zažívací	zažívací	k2eAgFnPc1d1	zažívací
potíže	potíž	k1gFnPc1	potíž
však	však	k9	však
závisí	záviset	k5eAaImIp3nP	záviset
na	na	k7c6	na
podané	podaný	k2eAgFnSc6d1	podaná
dávce	dávka	k1gFnSc6	dávka
a	a	k8xC	a
riziko	riziko	k1gNnSc4	riziko
lze	lze	k6eAd1	lze
tedy	tedy	k9	tedy
snížit	snížit	k5eAaPmF	snížit
podáním	podání	k1gNnSc7	podání
nižších	nízký	k2eAgInPc2d2	nižší
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
však	však	k9	však
stále	stále	k6eAd1	stále
efektivních	efektivní	k2eAgFnPc2d1	efektivní
dávek	dávka	k1gFnPc2	dávka
<g/>
.	.	kIx.	.
</s>
<s>
Kolchicin	kolchicin	k1gInSc1	kolchicin
může	moct	k5eAaImIp3nS	moct
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
působení	působení	k1gNnSc1	působení
jiných	jiný	k2eAgInPc2d1	jiný
běžně	běžně	k6eAd1	běžně
předepisovaných	předepisovaný	k2eAgInPc2d1	předepisovaný
léků	lék	k1gInPc2	lék
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
atorvastatin	atorvastatina	k1gFnPc2	atorvastatina
a	a	k8xC	a
erytromycin	erytromycina	k1gFnPc2	erytromycina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Steroidy	steroid	k1gInPc4	steroid
===	===	k?	===
</s>
</p>
<p>
<s>
Glukokortikoidy	glukokortikoid	k1gInPc1	glukokortikoid
se	se	k3xPyFc4	se
zdají	zdát	k5eAaPmIp3nP	zdát
být	být	k5eAaImF	být
stejně	stejně	k6eAd1	stejně
účinné	účinný	k2eAgInPc1d1	účinný
jako	jako	k8xC	jako
léky	lék	k1gInPc1	lék
NSAID	NSAID	kA	NSAID
a	a	k8xC	a
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
použít	použít	k5eAaPmF	použít
v	v	k7c6	v
případě	případ	k1gInSc6	případ
kontraindikace	kontraindikace	k1gFnSc2	kontraindikace
na	na	k7c6	na
NSAID	NSAID	kA	NSAID
<g/>
.	.	kIx.	.
</s>
<s>
Vedou	vést	k5eAaImIp3nP	vést
rovněž	rovněž	k9	rovněž
ke	k	k7c3	k
zlepšení	zlepšení	k1gNnSc3	zlepšení
stavu	stav	k1gInSc2	stav
po	po	k7c6	po
injekci	injekce	k1gFnSc6	injekce
do	do	k7c2	do
kloubu	kloub	k1gInSc2	kloub
<g/>
;	;	kIx,	;
předtím	předtím	k6eAd1	předtím
je	být	k5eAaImIp3nS	být
však	však	k9	však
nutno	nutno	k6eAd1	nutno
vyloučit	vyloučit	k5eAaPmF	vyloučit
zánět	zánět	k1gInSc4	zánět
kloubu	kloub	k1gInSc2	kloub
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
steroidy	steroid	k1gInPc1	steroid
toto	tento	k3xDgNnSc4	tento
onemocnění	onemocnění	k1gNnSc4	onemocnění
zhoršují	zhoršovat	k5eAaImIp3nP	zhoršovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Peglotikáza	Peglotikáza	k1gFnSc1	Peglotikáza
===	===	k?	===
</s>
</p>
<p>
<s>
Přípravek	přípravek	k1gInSc1	přípravek
peglotikáza	peglotikáza	k1gFnSc1	peglotikáza
(	(	kIx(	(
<g/>
krystexxa	krystexxa	k1gMnSc1	krystexxa
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
léčbu	léčba	k1gFnSc4	léčba
dny	dna	k1gFnSc2	dna
schválen	schválit	k5eAaPmNgInS	schválit
v	v	k7c6	v
USA	USA	kA	USA
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Nabízí	nabízet	k5eAaImIp3nS	nabízet
možnost	možnost	k1gFnSc4	možnost
léčby	léčba	k1gFnSc2	léčba
3	[number]	k4	3
%	%	kIx~	%
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
jiné	jiný	k2eAgInPc1d1	jiný
léky	lék	k1gInPc1	lék
netolerují	tolerovat	k5eNaImIp3nP	tolerovat
<g/>
.	.	kIx.	.
</s>
<s>
Peglotikáza	Peglotikáza	k1gFnSc1	Peglotikáza
se	se	k3xPyFc4	se
podává	podávat	k5eAaImIp3nS	podávat
jako	jako	k9	jako
nitrožilní	nitrožilní	k2eAgFnSc1d1	nitrožilní
infúze	infúze	k1gFnSc1	infúze
každé	každý	k3xTgInPc4	každý
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
a	a	k8xC	a
podle	podle	k7c2	podle
dosavadních	dosavadní	k2eAgInPc2d1	dosavadní
údajů	údaj	k1gInPc2	údaj
snižuje	snižovat	k5eAaImIp3nS	snižovat
hladinu	hladina	k1gFnSc4	hladina
kyseliny	kyselina	k1gFnSc2	kyselina
močové	močový	k2eAgFnSc2d1	močová
u	u	k7c2	u
výše	výše	k1gFnSc2	výše
uvedené	uvedený	k2eAgFnSc2d1	uvedená
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Profylaxe	profylaxe	k1gFnSc2	profylaxe
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
prevenci	prevence	k1gFnSc4	prevence
dalších	další	k2eAgInPc2d1	další
záchvatů	záchvat	k1gInPc2	záchvat
dny	dna	k1gFnSc2	dna
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
řada	řada	k1gFnSc1	řada
jiných	jiný	k2eAgInPc2d1	jiný
léků	lék	k1gInPc2	lék
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
také	také	k9	také
inhibitory	inhibitor	k1gInPc4	inhibitor
xantinoxidázy	xantinoxidáza	k1gFnSc2	xantinoxidáza
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
allopurinol	allopurinol	k1gInSc4	allopurinol
a	a	k8xC	a
febuxostat	febuxostat	k1gInSc4	febuxostat
<g/>
)	)	kIx)	)
a	a	k8xC	a
urikosurika	urikosurika	k1gFnSc1	urikosurika
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
probenecidu	probenecid	k1gInSc2	probenecid
a	a	k8xC	a
sulfinpyrazonu	sulfinpyrazon	k1gInSc2	sulfinpyrazon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
jsou	být	k5eAaImIp3nP	být
nasazeny	nasadit	k5eAaPmNgFnP	nasadit
jeden	jeden	k4xCgMnSc1	jeden
až	až	k9	až
dva	dva	k4xCgInPc1	dva
týdny	týden	k1gInPc1	týden
po	po	k7c6	po
odeznění	odeznění	k1gNnSc6	odeznění
akutního	akutní	k2eAgInSc2d1	akutní
záchvatu	záchvat	k1gInSc2	záchvat
(	(	kIx(	(
<g/>
existují	existovat	k5eAaImIp3nP	existovat
totiž	totiž	k9	totiž
teoretické	teoretický	k2eAgFnPc1d1	teoretická
obavy	obava	k1gFnPc1	obava
ze	z	k7c2	z
zhoršení	zhoršení	k1gNnSc2	zhoršení
stavu	stav	k1gInSc2	stav
<g/>
)	)	kIx)	)
a	a	k8xC	a
často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
kombinovány	kombinován	k2eAgInPc1d1	kombinován
buď	buď	k8xC	buď
s	s	k7c7	s
léky	lék	k1gInPc7	lék
NSAID	NSAID	kA	NSAID
nebo	nebo	k8xC	nebo
kolchicinem	kolchicin	k1gInSc7	kolchicin
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
prvních	první	k4xOgNnPc2	první
tří	tři	k4xCgNnPc2	tři
až	až	k9	až
šesti	šest	k4xCc2	šest
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
podávání	podávání	k1gNnSc1	podávání
se	se	k3xPyFc4	se
nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nemocný	nemocný	k2eAgMnSc1d1	nemocný
neprodělal	prodělat	k5eNaPmAgMnS	prodělat
dva	dva	k4xCgInPc4	dva
záchvaty	záchvat	k1gInPc4	záchvat
dny	dna	k1gFnSc2	dna
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
poškození	poškození	k1gNnSc3	poškození
kloubu	kloub	k1gInSc2	kloub
a	a	k8xC	a
neobjeví	objevit	k5eNaPmIp3nP	objevit
se	se	k3xPyFc4	se
tofy	tofa	k1gFnSc2	tofa
nebo	nebo	k8xC	nebo
urátová	urátový	k2eAgFnSc1d1	urátová
nefropatie	nefropatie	k1gFnSc1	nefropatie
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
okamžiku	okamžik	k1gInSc2	okamžik
se	se	k3xPyFc4	se
léky	lék	k1gInPc1	lék
zdají	zdát	k5eAaPmIp3nP	zdát
být	být	k5eAaImF	být
cenově	cenově	k6eAd1	cenově
neefektivní	efektivní	k2eNgFnPc4d1	neefektivní
<g/>
.	.	kIx.	.
</s>
<s>
Opatření	opatření	k1gNnPc1	opatření
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
hladiny	hladina	k1gFnSc2	hladina
urátů	urát	k1gInPc2	urát
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
posílena	posílit	k5eAaPmNgFnS	posílit
až	až	k6eAd1	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
hladina	hladina	k1gFnSc1	hladina
kyseliny	kyselina	k1gFnSc2	kyselina
močové	močový	k2eAgFnSc2d1	močová
v	v	k7c6	v
krevním	krevní	k2eAgNnSc6d1	krevní
séru	sérum	k1gNnSc6	sérum
klesne	klesnout	k5eAaPmIp3nS	klesnout
pod	pod	k7c7	pod
300	[number]	k4	300
<g/>
–	–	k?	–
<g/>
360	[number]	k4	360
μ	μ	k?	μ
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
(	(	kIx(	(
<g/>
5,0	[number]	k4	5,0
<g/>
-	-	kIx~	-
<g/>
6,0	[number]	k4	6,0
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
dl	dl	k?	dl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
mělo	mít	k5eAaImAgNnS	mít
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gFnPc2	on
pokračovat	pokračovat	k5eAaImF	pokračovat
po	po	k7c4	po
neomezenou	omezený	k2eNgFnSc4d1	neomezená
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc4	tento
léčivé	léčivý	k2eAgInPc4d1	léčivý
přípravky	přípravek	k1gInPc4	přípravek
v	v	k7c6	v
době	doba	k1gFnSc6	doba
záchvatu	záchvat	k1gInSc2	záchvat
podávány	podávat	k5eAaImNgInP	podávat
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
<g/>
,	,	kIx,	,
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gNnSc1	jejich
užívání	užívání	k1gNnSc1	užívání
ukončit	ukončit	k5eAaPmF	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
se	se	k3xPyFc4	se
hladinu	hladina	k1gFnSc4	hladina
kyseliny	kyselina	k1gFnSc2	kyselina
močové	močový	k2eAgNnSc1d1	močové
nedaří	dařit	k5eNaImIp3nS	dařit
dostat	dostat	k5eAaPmF	dostat
pod	pod	k7c4	pod
hodnotu	hodnota	k1gFnSc4	hodnota
6,0	[number]	k4	6,0
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
dl	dl	k?	dl
a	a	k8xC	a
záchvaty	záchvat	k1gInPc1	záchvat
stále	stále	k6eAd1	stále
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
selhání	selhání	k1gNnSc4	selhání
léčby	léčba	k1gFnSc2	léčba
a	a	k8xC	a
dna	dno	k1gNnSc2	dno
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
označena	označen	k2eAgFnSc1d1	označena
jako	jako	k8xC	jako
refrakterní	refrakterní	k2eAgFnSc1d1	refrakterní
(	(	kIx(	(
<g/>
nereagující	reagující	k2eNgFnSc1d1	nereagující
na	na	k7c4	na
léčbu	léčba	k1gFnSc4	léčba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
probenecid	probenecid	k1gInSc1	probenecid
zdá	zdát	k5eAaPmIp3nS	zdát
méně	málo	k6eAd2	málo
účinný	účinný	k2eAgInSc1d1	účinný
než	než	k8xS	než
allopurinol	allopurinol	k1gInSc1	allopurinol
<g/>
.	.	kIx.	.
<g/>
Urikosurika	Urikosurika	k1gFnSc1	Urikosurika
se	se	k3xPyFc4	se
typicky	typicky	k6eAd1	typicky
předepisují	předepisovat	k5eAaImIp3nP	předepisovat
v	v	k7c6	v
případech	případ	k1gInPc6	případ
snížené	snížený	k2eAgFnSc2d1	snížená
sekrece	sekrece	k1gFnSc2	sekrece
kyseliny	kyselina	k1gFnSc2	kyselina
močové	močový	k2eAgFnSc2d1	močová
<g/>
,	,	kIx,	,
zjištěné	zjištěný	k2eAgFnSc2d1	zjištěná
na	na	k7c6	na
základě	základ	k1gInSc6	základ
výsledků	výsledek	k1gInPc2	výsledek
24	[number]	k4	24
<g/>
hodinového	hodinový	k2eAgInSc2d1	hodinový
sběru	sběr	k1gInSc2	sběr
moči	moč	k1gFnSc2	moč
při	při	k7c6	při
hladině	hladina	k1gFnSc6	hladina
kyseliny	kyselina	k1gFnSc2	kyselina
močové	močový	k2eAgFnSc2d1	močová
nižší	nízký	k2eAgFnSc2d2	nižší
než	než	k8xS	než
800	[number]	k4	800
mg	mg	kA	mg
<g/>
.	.	kIx.	.
</s>
<s>
Urikosurika	Urikosurika	k1gFnSc1	Urikosurika
se	se	k3xPyFc4	se
ale	ale	k9	ale
nedoporučují	doporučovat	k5eNaImIp3nP	doporučovat
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
anamnézou	anamnéza	k1gFnSc7	anamnéza
ledvinových	ledvinový	k2eAgInPc2d1	ledvinový
kamenů	kámen	k1gInPc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
množství	množství	k1gNnSc1	množství
vyloučené	vyloučený	k2eAgFnSc2d1	vyloučená
moči	moč	k1gFnSc2	moč
během	během	k7c2	během
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
přesáhne	přesáhnout	k5eAaPmIp3nS	přesáhnout
800	[number]	k4	800
mg	mg	kA	mg
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
její	její	k3xOp3gFnSc6	její
nadměrné	nadměrný	k2eAgFnSc6d1	nadměrná
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
,	,	kIx,	,
preferuje	preferovat	k5eAaImIp3nS	preferovat
se	se	k3xPyFc4	se
podání	podání	k1gNnSc1	podání
inhibitoru	inhibitor	k1gInSc2	inhibitor
xantinoxidázy	xantinoxidáza	k1gFnSc2	xantinoxidáza
<g/>
.	.	kIx.	.
<g/>
Inhibitory	inhibitor	k1gInPc1	inhibitor
xantinoxidázy	xantinoxidáza	k1gFnSc2	xantinoxidáza
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
allopurinolu	allopurinol	k1gInSc2	allopurinol
a	a	k8xC	a
febuxostatu	febuxostat	k1gInSc2	febuxostat
<g/>
)	)	kIx)	)
blokují	blokovat	k5eAaImIp3nP	blokovat
produkci	produkce	k1gFnSc4	produkce
kyseliny	kyselina	k1gFnSc2	kyselina
močové	močový	k2eAgNnSc1d1	močové
<g/>
;	;	kIx,	;
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
léčba	léčba	k1gFnSc1	léčba
pomocí	pomocí	k7c2	pomocí
těchto	tento	k3xDgInPc2	tento
přípravků	přípravek	k1gInPc2	přípravek
je	být	k5eAaImIp3nS	být
bezpečná	bezpečný	k2eAgFnSc1d1	bezpečná
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
snášena	snášen	k2eAgFnSc1d1	snášena
a	a	k8xC	a
lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
použít	použít	k5eAaPmF	použít
i	i	k9	i
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
ledvinovými	ledvinový	k2eAgFnPc7d1	ledvinová
potížemi	potíž	k1gFnPc7	potíž
nebo	nebo	k8xC	nebo
močovými	močový	k2eAgInPc7d1	močový
kameny	kámen	k1gInPc7	kámen
(	(	kIx(	(
<g/>
i	i	k8xC	i
když	když	k8xS	když
allopurinol	allopurinol	k1gInSc1	allopurinol
může	moct	k5eAaImIp3nS	moct
u	u	k7c2	u
malého	malý	k2eAgInSc2d1	malý
počtu	počet	k1gInSc2	počet
jedinců	jedinec	k1gMnPc2	jedinec
způsobit	způsobit	k5eAaPmF	způsobit
přecitlivělost	přecitlivělost	k1gFnSc4	přecitlivělost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takových	takový	k3xDgInPc6	takový
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
alternativní	alternativní	k2eAgInSc4d1	alternativní
léčivý	léčivý	k2eAgInSc4d1	léčivý
přípravek	přípravek	k1gInSc4	přípravek
febuxostat	febuxostat	k5eAaImF	febuxostat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Prognóza	prognóza	k1gFnSc1	prognóza
==	==	k?	==
</s>
</p>
<p>
<s>
Neléčený	léčený	k2eNgInSc1d1	neléčený
akutní	akutní	k2eAgInSc1d1	akutní
záchvat	záchvat	k1gInSc1	záchvat
dny	dna	k1gFnSc2	dna
obvykle	obvykle	k6eAd1	obvykle
odezní	odeznět	k5eAaPmIp3nS	odeznět
během	během	k7c2	během
pěti	pět	k4xCc2	pět
až	až	k9	až
sedmi	sedm	k4xCc2	sedm
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
60	[number]	k4	60
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
ale	ale	k8xC	ale
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
záchvatu	záchvat	k1gInSc3	záchvat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
trpících	trpící	k2eAgFnPc2d1	trpící
dnou	dna	k1gFnSc7	dna
existuje	existovat	k5eAaImIp3nS	existovat
zvýšené	zvýšený	k2eAgNnSc4d1	zvýšené
riziko	riziko	k1gNnSc4	riziko
vysokého	vysoký	k2eAgInSc2d1	vysoký
krevního	krevní	k2eAgInSc2d1	krevní
tlaku	tlak	k1gInSc2	tlak
<g/>
,	,	kIx,	,
cukrovky	cukrovka	k1gFnSc2	cukrovka
<g/>
,	,	kIx,	,
metabolického	metabolický	k2eAgInSc2d1	metabolický
syndromu	syndrom	k1gInSc2	syndrom
<g/>
,	,	kIx,	,
onemocnění	onemocnění	k1gNnSc1	onemocnění
ledvin	ledvina	k1gFnPc2	ledvina
a	a	k8xC	a
kardiovaskulárních	kardiovaskulární	k2eAgFnPc2d1	kardiovaskulární
chorob	choroba	k1gFnPc2	choroba
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
vyšší	vysoký	k2eAgNnSc4d2	vyšší
riziko	riziko	k1gNnSc4	riziko
úmrtí	úmrtí	k1gNnSc2	úmrtí
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
částečně	částečně	k6eAd1	částečně
souviset	souviset	k5eAaImF	souviset
s	s	k7c7	s
inzulinovou	inzulinový	k2eAgFnSc7d1	inzulinová
rezistencí	rezistence	k1gFnSc7	rezistence
a	a	k8xC	a
obezitou	obezita	k1gFnSc7	obezita
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
některé	některý	k3yIgInPc1	některý
faktory	faktor	k1gInPc1	faktor
zvýšeného	zvýšený	k2eAgNnSc2d1	zvýšené
rizika	riziko	k1gNnSc2	riziko
působí	působit	k5eAaImIp3nS	působit
zřejmě	zřejmě	k6eAd1	zřejmě
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
<g/>
.	.	kIx.	.
<g/>
Bez	bez	k7c2	bez
léčby	léčba	k1gFnSc2	léčba
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
akutní	akutní	k2eAgFnSc1d1	akutní
dna	dna	k1gFnSc1	dna
rozvinout	rozvinout	k5eAaPmF	rozvinout
v	v	k7c4	v
chronickou	chronický	k2eAgFnSc4d1	chronická
dnu	dna	k1gFnSc4	dna
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
k	k	k7c3	k
poškození	poškození	k1gNnSc3	poškození
kloubního	kloubní	k2eAgInSc2d1	kloubní
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
deformaci	deformace	k1gFnSc3	deformace
kloubů	kloub	k1gInPc2	kloub
a	a	k8xC	a
vzniku	vznik	k1gInSc2	vznik
nebolestivých	bolestivý	k2eNgInPc2d1	nebolestivý
tofů	tof	k1gInPc2	tof
<g/>
.	.	kIx.	.
</s>
<s>
Tofy	Tofa	k1gFnPc1	Tofa
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
u	u	k7c2	u
30	[number]	k4	30
%	%	kIx~	%
osob	osoba	k1gFnPc2	osoba
po	po	k7c6	po
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
neléčené	léčený	k2eNgFnSc2d1	neléčená
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
se	se	k3xPyFc4	se
často	často	k6eAd1	často
na	na	k7c6	na
helixu	helix	k1gInSc6	helix
(	(	kIx(	(
<g/>
zevním	zevní	k2eAgInSc6d1	zevní
přehnutém	přehnutý	k2eAgInSc6d1	přehnutý
okraji	okraj	k1gInSc6	okraj
ušního	ušní	k2eAgInSc2d1	ušní
boltce	boltec	k1gInSc2	boltec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
výběžkem	výběžek	k1gInSc7	výběžek
kosti	kost	k1gFnSc2	kost
loketní	loketní	k2eAgFnSc2d1	loketní
nebo	nebo	k8xC	nebo
na	na	k7c6	na
Achillových	Achillův	k2eAgFnPc6d1	Achillova
šlachách	šlacha	k1gFnPc6	šlacha
<g/>
.	.	kIx.	.
</s>
<s>
Agresivní	agresivní	k2eAgFnSc7d1	agresivní
léčbou	léčba	k1gFnSc7	léčba
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vstřebat	vstřebat	k5eAaPmF	vstřebat
<g/>
.	.	kIx.	.
</s>
<s>
Častou	častý	k2eAgFnSc7d1	častá
komplikací	komplikace	k1gFnSc7	komplikace
dny	dna	k1gFnSc2	dna
bývají	bývat	k5eAaImIp3nP	bývat
ledvinové	ledvinový	k2eAgInPc1d1	ledvinový
kameny	kámen	k1gInPc1	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
komplikace	komplikace	k1gFnSc1	komplikace
postihuje	postihovat	k5eAaImIp3nS	postihovat
10	[number]	k4	10
až	až	k9	až
40	[number]	k4	40
%	%	kIx~	%
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
kvůli	kvůli	k7c3	kvůli
nízké	nízký	k2eAgFnSc3d1	nízká
hodnotě	hodnota	k1gFnSc3	hodnota
pH	ph	kA	ph
moči	moč	k1gFnSc6	moč
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
podporuje	podporovat	k5eAaImIp3nS	podporovat
srážení	srážení	k1gNnSc4	srážení
kyseliny	kyselina	k1gFnSc2	kyselina
močové	močový	k2eAgFnSc2d1	močová
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytovat	vyskytovat	k5eAaImF	vyskytovat
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
i	i	k9	i
jiné	jiný	k2eAgFnPc1d1	jiná
formy	forma	k1gFnPc1	forma
chronické	chronický	k2eAgFnSc2d1	chronická
poruchy	porucha	k1gFnSc2	porucha
funkce	funkce	k1gFnSc2	funkce
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Epidemiologie	epidemiologie	k1gFnSc2	epidemiologie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
jistém	jistý	k2eAgInSc6d1	jistý
momentu	moment	k1gInSc6	moment
života	život	k1gInSc2	život
postihuje	postihovat	k5eAaImIp3nS	postihovat
dna	dno	k1gNnPc1	dno
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
až	až	k9	až
2	[number]	k4	2
%	%	kIx~	%
západní	západní	k2eAgFnSc2d1	západní
populace	populace	k1gFnSc2	populace
a	a	k8xC	a
četnost	četnost	k1gFnSc4	četnost
tohoto	tento	k3xDgNnSc2	tento
onemocnění	onemocnění	k1gNnSc2	onemocnění
stále	stále	k6eAd1	stále
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1990	[number]	k4	1990
až	až	k8xS	až
2010	[number]	k4	2010
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc1	jeho
výskyt	výskyt	k1gInSc1	výskyt
přibližně	přibližně	k6eAd1	přibližně
zdvojnásobil	zdvojnásobit	k5eAaPmAgInS	zdvojnásobit
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
příčinou	příčina	k1gFnSc7	příčina
zvýšeného	zvýšený	k2eAgInSc2d1	zvýšený
výskytu	výskyt	k1gInSc2	výskyt
dny	dna	k1gFnSc2	dna
je	být	k5eAaImIp3nS	být
prodloužení	prodloužení	k1gNnSc4	prodloužení
očekávané	očekávaný	k2eAgFnSc2d1	očekávaná
délky	délka	k1gFnSc2	délka
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
změna	změna	k1gFnSc1	změna
stravovacích	stravovací	k2eAgInPc2d1	stravovací
návyků	návyk	k1gInPc2	návyk
a	a	k8xC	a
také	také	k9	také
vyšší	vysoký	k2eAgInSc1d2	vyšší
výskyt	výskyt	k1gInSc1	výskyt
onemocnění	onemocnění	k1gNnSc2	onemocnění
souvisejících	související	k2eAgInPc2d1	související
se	s	k7c7	s
dnou	dna	k1gFnSc7	dna
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
metabolického	metabolický	k2eAgInSc2d1	metabolický
syndromu	syndrom	k1gInSc2	syndrom
nebo	nebo	k8xC	nebo
vysokého	vysoký	k2eAgInSc2d1	vysoký
krevního	krevní	k2eAgInSc2d1	krevní
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
četnost	četnost	k1gFnSc4	četnost
výskytu	výskyt	k1gInSc2	výskyt
dny	dna	k1gFnSc2	dna
má	mít	k5eAaImIp3nS	mít
vliv	vliv	k1gInSc1	vliv
několik	několik	k4yIc1	několik
faktorů	faktor	k1gInPc2	faktor
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
věk	věk	k1gInSc1	věk
<g/>
,	,	kIx,	,
rasa	rasa	k1gFnSc1	rasa
nebo	nebo	k8xC	nebo
roční	roční	k2eAgNnSc1d1	roční
období	období	k1gNnSc1	období
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
onemocnění	onemocnění	k1gNnSc1	onemocnění
postihuje	postihovat	k5eAaImIp3nS	postihovat
2	[number]	k4	2
%	%	kIx~	%
mužů	muž	k1gMnPc2	muž
starších	starý	k2eAgMnPc2d2	starší
30	[number]	k4	30
let	léto	k1gNnPc2	léto
a	a	k8xC	a
žen	žena	k1gFnPc2	žena
starších	starší	k1gMnPc2	starší
50	[number]	k4	50
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
afroamerického	afroamerický	k2eAgInSc2d1	afroamerický
původu	původ	k1gInSc2	původ
dvakrát	dvakrát	k6eAd1	dvakrát
vyšší	vysoký	k2eAgFnSc1d2	vyšší
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
dnou	dna	k1gFnSc7	dna
onemocní	onemocnět	k5eAaPmIp3nS	onemocnět
<g/>
,	,	kIx,	,
než	než	k8xS	než
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
rasy	rasa	k1gFnSc2	rasa
europoidní	europoidní	k2eAgFnSc2d1	europoidní
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
tohoto	tento	k3xDgNnSc2	tento
onemocnění	onemocnění	k1gNnSc2	onemocnění
je	být	k5eAaImIp3nS	být
vysoký	vysoký	k2eAgInSc1d1	vysoký
mezi	mezi	k7c7	mezi
národy	národ	k1gInPc7	národ
pacifických	pacifický	k2eAgInPc2d1	pacifický
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
u	u	k7c2	u
Maorů	Maor	k1gMnPc2	Maor
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
u	u	k7c2	u
australských	australský	k2eAgMnPc2d1	australský
domorodců	domorodec	k1gMnPc2	domorodec
(	(	kIx(	(
<g/>
Aboridžinců	Aboridžinec	k1gMnPc2	Aboridžinec
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
navzdory	navzdory	k7c3	navzdory
vysoké	vysoký	k2eAgFnSc3d1	vysoká
průměrné	průměrný	k2eAgFnSc3d1	průměrná
koncentraci	koncentrace	k1gFnSc3	koncentrace
kyseliny	kyselina	k1gFnSc2	kyselina
močové	močový	k2eAgFnSc2d1	močová
v	v	k7c6	v
séru	sérum	k1gNnSc6	sérum
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
vzácně	vzácně	k6eAd1	vzácně
<g/>
.	.	kIx.	.
</s>
<s>
Běžným	běžný	k2eAgMnPc3d1	běžný
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
onemocnění	onemocnění	k1gNnSc1	onemocnění
stalo	stát	k5eAaPmAgNnS	stát
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
Polynésii	Polynésie	k1gFnSc6	Polynésie
a	a	k8xC	a
v	v	k7c6	v
městských	městský	k2eAgFnPc6d1	městská
oblastech	oblast	k1gFnPc6	oblast
subsaharské	subsaharský	k2eAgFnSc2d1	subsaharská
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
studie	studie	k1gFnPc1	studie
zjistily	zjistit	k5eAaPmAgFnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
záchvatům	záchvat	k1gInPc3	záchvat
dny	dna	k1gFnSc2	dna
dochází	docházet	k5eAaImIp3nS	docházet
častěji	často	k6eAd2	často
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
spojováno	spojovat	k5eAaImNgNnS	spojovat
se	s	k7c7	s
sezónními	sezónní	k2eAgFnPc7d1	sezónní
změnami	změna	k1gFnPc7	změna
stravy	strava	k1gFnSc2	strava
<g/>
,	,	kIx,	,
konzumací	konzumace	k1gFnPc2	konzumace
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
,	,	kIx,	,
fyzickou	fyzický	k2eAgFnSc7d1	fyzická
aktivitou	aktivita	k1gFnSc7	aktivita
a	a	k8xC	a
teplotou	teplota	k1gFnSc7	teplota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Anglický	anglický	k2eAgInSc1d1	anglický
výraz	výraz	k1gInSc1	výraz
pro	pro	k7c4	pro
dnu	dna	k1gFnSc4	dna
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
gout	gout	k5eAaPmF	gout
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
použil	použít	k5eAaPmAgMnS	použít
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
mnich	mnich	k1gMnSc1	mnich
Randolf	Randolf	k1gMnSc1	Randolf
z	z	k7c2	z
Bockingu	Bocking	k1gInSc2	Bocking
někdy	někdy	k6eAd1	někdy
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1200	[number]	k4	1200
po	po	k7c6	po
Kr.	Kr.	k1gFnSc6	Kr.
Odvodil	odvodit	k5eAaPmAgMnS	odvodit
ho	on	k3xPp3gInSc4	on
z	z	k7c2	z
latinského	latinský	k2eAgNnSc2d1	latinské
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
gutta	gutta	k1gFnSc1	gutta
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
znamená	znamenat	k5eAaImIp3nS	znamenat
kapku	kapka	k1gFnSc4	kapka
(	(	kIx(	(
<g/>
tekutiny	tekutina	k1gFnSc2	tekutina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
slovníku	slovník	k1gInSc2	slovník
Oxford	Oxford	k1gInSc4	Oxford
English	English	k1gInSc1	English
Dictionary	Dictionara	k1gFnSc2	Dictionara
tento	tento	k3xDgInSc1	tento
výraz	výraz	k1gInSc1	výraz
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
humorální	humorální	k2eAgFnSc2d1	humorální
teorie	teorie	k1gFnSc2	teorie
a	a	k8xC	a
z	z	k7c2	z
"	"	kIx"	"
<g/>
představy	představa	k1gFnSc2	představa
chorobné	chorobný	k2eAgFnSc2d1	chorobná
látky	látka	k1gFnSc2	látka
odkapávající	odkapávající	k2eAgFnSc2d1	odkapávající
z	z	k7c2	z
krve	krev	k1gFnSc2	krev
na	na	k7c4	na
klouby	kloub	k1gInPc4	kloub
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
okolí	okolí	k1gNnSc4	okolí
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Dna	dno	k1gNnPc1	dno
byla	být	k5eAaImAgNnP	být
ale	ale	k8xC	ale
známa	znám	k2eAgNnPc1d1	známo
už	už	k6eAd1	už
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
říkalo	říkat	k5eAaImAgNnS	říkat
"	"	kIx"	"
<g/>
král	král	k1gMnSc1	král
všech	všecek	k3xTgFnPc2	všecek
nemocí	nemoc	k1gFnPc2	nemoc
a	a	k8xC	a
nemoc	nemoc	k1gFnSc1	nemoc
králů	král	k1gMnPc2	král
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
nemoc	nemoc	k1gFnSc1	nemoc
bohatých	bohatý	k2eAgMnPc2d1	bohatý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
dně	dno	k1gNnSc6	dno
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
2600	[number]	k4	2600
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
Kr.	Kr.	k1gMnSc2	Kr.
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
popisu	popis	k1gInSc2	popis
artritidy	artritida	k1gFnSc2	artritida
palce	palec	k1gInSc2	palec
na	na	k7c6	na
noze	noha	k1gFnSc6	noha
<g/>
.	.	kIx.	.
</s>
<s>
Řecký	řecký	k2eAgMnSc1d1	řecký
lékař	lékař	k1gMnSc1	lékař
Hippokratés	Hippokratésa	k1gFnPc2	Hippokratésa
se	se	k3xPyFc4	se
o	o	k7c6	o
tomto	tento	k3xDgNnSc6	tento
onemocnění	onemocnění	k1gNnSc6	onemocnění
zmínil	zmínit	k5eAaPmAgMnS	zmínit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
400	[number]	k4	400
př	př	kA	př
<g/>
.	.	kIx.	.
Kr.	Kr.	k1gMnSc1	Kr.
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
Aforismech	aforismus	k1gInPc6	aforismus
a	a	k8xC	a
povšiml	povšimnout	k5eAaPmAgInS	povšimnout
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
u	u	k7c2	u
eunuchů	eunuch	k1gMnPc2	eunuch
ani	ani	k8xC	ani
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
před	před	k7c7	před
menopauzou	menopauza	k1gFnSc7	menopauza
<g/>
.	.	kIx.	.
</s>
<s>
Aulus	Aulus	k1gMnSc1	Aulus
Cornelius	Cornelius	k1gMnSc1	Cornelius
Celsus	Celsus	k1gMnSc1	Celsus
(	(	kIx(	(
<g/>
30	[number]	k4	30
po	po	k7c6	po
Kr.	Kr.	k1gFnSc6	Kr.
<g/>
)	)	kIx)	)
zase	zase	k9	zase
popsal	popsat	k5eAaPmAgMnS	popsat
souvislost	souvislost	k1gFnSc4	souvislost
s	s	k7c7	s
alkoholem	alkohol	k1gInSc7	alkohol
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgInSc1d2	pozdější
výskyt	výskyt	k1gInSc1	výskyt
onemocnění	onemocnění	k1gNnSc2	onemocnění
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
související	související	k2eAgInPc4d1	související
ledvinové	ledvinový	k2eAgInPc4d1	ledvinový
problémy	problém	k1gInPc4	problém
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Také	také	k9	také
hustá	hustý	k2eAgFnSc1d1	hustá
moč	moč	k1gFnSc1	moč
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
bílý	bílý	k2eAgInSc1d1	bílý
sediment	sediment	k1gInSc1	sediment
<g/>
,	,	kIx,	,
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
obávat	obávat	k5eAaImF	obávat
bolesti	bolest	k1gFnPc4	bolest
a	a	k8xC	a
choroby	choroba	k1gFnPc4	choroba
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
kloubů	kloub	k1gInPc2	kloub
nebo	nebo	k8xC	nebo
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
orgánů	orgán	k1gInPc2	orgán
<g/>
...	...	k?	...
Potíže	potíž	k1gFnPc1	potíž
s	s	k7c7	s
klouby	kloub	k1gInPc7	kloub
na	na	k7c6	na
rukou	ruka	k1gFnPc6	ruka
a	a	k8xC	a
nohou	noha	k1gFnPc6	noha
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
časté	častý	k2eAgFnPc1d1	častá
a	a	k8xC	a
úporné	úporný	k2eAgFnPc1d1	úporná
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
v	v	k7c6	v
případě	případ	k1gInSc6	případ
podagry	podagry	k?	podagry
a	a	k8xC	a
cheiragry	cheiragra	k1gFnSc2	cheiragra
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
jen	jen	k9	jen
vzácně	vzácně	k6eAd1	vzácně
postihují	postihovat	k5eAaImIp3nP	postihovat
eunuchy	eunuch	k1gMnPc4	eunuch
a	a	k8xC	a
chlapce	chlapec	k1gMnPc4	chlapec
před	před	k7c7	před
prvním	první	k4xOgInSc7	první
pohlavním	pohlavní	k2eAgInSc7d1	pohlavní
stykem	styk	k1gInSc7	styk
se	se	k3xPyFc4	se
ženou	hnát	k5eAaImIp3nP	hnát
nebo	nebo	k8xC	nebo
ženy	žena	k1gFnPc1	žena
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgMnPc2	jenž
byl	být	k5eAaImAgInS	být
potlačen	potlačit	k5eAaPmNgInS	potlačit
menses	menses	k1gInSc1	menses
<g/>
...	...	k?	...
někdo	někdo	k3yInSc1	někdo
získá	získat	k5eAaPmIp3nS	získat
celoživotní	celoživotní	k2eAgFnSc4d1	celoživotní
odolnost	odolnost	k1gFnSc4	odolnost
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zdrží	zdržet	k5eAaPmIp3nS	zdržet
pití	pití	k1gNnSc1	pití
vína	víno	k1gNnSc2	víno
a	a	k8xC	a
medoviny	medovina	k1gFnSc2	medovina
a	a	k8xC	a
vyvaruje	vyvarovat	k5eAaPmIp3nS	vyvarovat
se	se	k3xPyFc4	se
vyhledávání	vyhledávání	k1gNnSc1	vyhledávání
sexuálních	sexuální	k2eAgFnPc2d1	sexuální
rozkoší	rozkoš	k1gFnPc2	rozkoš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1683	[number]	k4	1683
popsal	popsat	k5eAaPmAgMnS	popsat
anglický	anglický	k2eAgMnSc1d1	anglický
lékař	lékař	k1gMnSc1	lékař
Thomas	Thomas	k1gMnSc1	Thomas
Sydenham	Sydenham	k1gInSc4	Sydenham
výskyt	výskyt	k1gInSc4	výskyt
onemocnění	onemocnění	k1gNnPc2	onemocnění
v	v	k7c6	v
časných	časný	k2eAgFnPc6d1	časná
ranních	ranní	k2eAgFnPc6d1	ranní
hodinách	hodina	k1gFnPc6	hodina
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
nejčastěji	často	k6eAd3	často
vybírá	vybírat	k5eAaImIp3nS	vybírat
starší	starý	k2eAgMnPc4d2	starší
muže	muž	k1gMnPc4	muž
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Pacienti	pacient	k1gMnPc1	pacient
se	se	k3xPyFc4	se
dnou	dna	k1gFnSc7	dna
bývají	bývat	k5eAaImIp3nP	bývat
obvykle	obvykle	k6eAd1	obvykle
starší	starý	k2eAgMnPc1d2	starší
muži	muž	k1gMnPc1	muž
nebo	nebo	k8xC	nebo
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
natolik	natolik	k6eAd1	natolik
vyčerpali	vyčerpat	k5eAaPmAgMnP	vyčerpat
<g/>
,	,	kIx,	,
až	až	k9	až
předčasně	předčasně	k6eAd1	předčasně
zestárli	zestárnout	k5eAaPmAgMnP	zestárnout
–	–	k?	–
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
neřestí	neřest	k1gFnPc2	neřest
přitom	přitom	k6eAd1	přitom
tou	ten	k3xDgFnSc7	ten
nejčastější	častý	k2eAgFnSc7d3	nejčastější
bývá	bývat	k5eAaImIp3nS	bývat
předčasné	předčasný	k2eAgNnSc4d1	předčasné
a	a	k8xC	a
nadměrné	nadměrný	k2eAgNnSc4d1	nadměrné
holdování	holdování	k1gNnSc4	holdování
sexuálním	sexuální	k2eAgFnPc3d1	sexuální
rozkoším	rozkoš	k1gFnPc3	rozkoš
a	a	k8xC	a
podobným	podobný	k2eAgFnPc3d1	podobná
vyčerpávajícím	vyčerpávající	k2eAgFnPc3d1	vyčerpávající
vášním	vášeň	k1gFnPc3	vášeň
<g/>
.	.	kIx.	.
</s>
<s>
Oběť	oběť	k1gFnSc1	oběť
jde	jít	k5eAaImIp3nS	jít
spát	spát	k5eAaImF	spát
a	a	k8xC	a
když	když	k8xS	když
usíná	usínat	k5eAaImIp3nS	usínat
<g/>
,	,	kIx,	,
těší	těšit	k5eAaImIp3nS	těšit
se	se	k3xPyFc4	se
dobrému	dobrý	k2eAgNnSc3d1	dobré
zdraví	zdraví	k1gNnSc3	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
druhé	druhý	k4xOgFnSc2	druhý
hodiny	hodina	k1gFnSc2	hodina
ranní	ranní	k2eAgFnSc4d1	ranní
ji	on	k3xPp3gFnSc4	on
vzbudí	vzbudit	k5eAaPmIp3nP	vzbudit
krutá	krutý	k2eAgFnSc1d1	krutá
bolest	bolest	k1gFnSc1	bolest
v	v	k7c6	v
palci	palec	k1gInSc6	palec
na	na	k7c6	na
noze	noha	k1gFnSc6	noha
<g/>
,	,	kIx,	,
vzácněji	vzácně	k6eAd2	vzácně
v	v	k7c6	v
patě	pata	k1gFnSc6	pata
<g/>
,	,	kIx,	,
kotníku	kotník	k1gInSc6	kotník
nebo	nebo	k8xC	nebo
nártu	nárt	k1gInSc6	nárt
<g/>
.	.	kIx.	.
</s>
<s>
Bolí	bolet	k5eAaImIp3nS	bolet
to	ten	k3xDgNnSc1	ten
jako	jako	k9	jako
při	při	k7c6	při
vykloubení	vykloubení	k1gNnSc6	vykloubení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
postižený	postižený	k2eAgMnSc1d1	postižený
má	mít	k5eAaImIp3nS	mít
pocit	pocit	k1gInSc1	pocit
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
kdyby	kdyby	kYmCp3nP	kdyby
mu	on	k3xPp3gMnSc3	on
bolavá	bolavý	k2eAgNnPc4d1	bolavé
místa	místo	k1gNnPc4	místo
polévali	polévat	k5eAaImAgMnP	polévat
studenou	studený	k2eAgFnSc7d1	studená
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
přichází	přicházet	k5eAaImIp3nS	přicházet
zimnice	zimnice	k1gFnSc1	zimnice
<g/>
,	,	kIx,	,
třesavka	třesavka	k1gFnSc1	třesavka
a	a	k8xC	a
nízká	nízký	k2eAgFnSc1d1	nízká
horečka	horečka	k1gFnSc1	horečka
<g/>
...	...	k?	...
Noc	noc	k1gFnSc1	noc
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
mukách	muka	k1gFnPc6	muka
<g/>
,	,	kIx,	,
beze	beze	k7c2	beze
spánku	spánek	k1gInSc2	spánek
<g/>
,	,	kIx,	,
nemocný	nemocný	k1gMnSc1	nemocný
obrací	obracet	k5eAaImIp3nS	obracet
postiženou	postižený	k2eAgFnSc7d1	postižená
částí	část	k1gFnSc7	část
a	a	k8xC	a
neustále	neustále	k6eAd1	neustále
mění	měnit	k5eAaImIp3nS	měnit
polohu	poloha	k1gFnSc4	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Převalování	převalování	k1gNnSc1	převalování
na	na	k7c6	na
lůžku	lůžko	k1gNnSc6	lůžko
neustává	ustávat	k5eNaImIp3nS	ustávat
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
neustává	ustávat	k5eNaImIp3nS	ustávat
ani	ani	k8xC	ani
bolest	bolest	k1gFnSc4	bolest
mučeného	mučený	k2eAgInSc2d1	mučený
kloubu	kloub	k1gInSc2	kloub
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
záchvat	záchvat	k1gInSc1	záchvat
postupuje	postupovat	k5eAaImIp3nS	postupovat
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
zhoršuje	zhoršovat	k5eAaImIp3nS	zhoršovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Holandský	holandský	k2eAgMnSc1d1	holandský
vědec	vědec	k1gMnSc1	vědec
Anthony	Anthona	k1gFnSc2	Anthona
van	van	k1gInSc1	van
Leeuwenhoek	Leeuwenhoek	k1gMnSc1	Leeuwenhoek
popsal	popsat	k5eAaPmAgMnS	popsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1679	[number]	k4	1679
jako	jako	k8xC	jako
první	první	k4xOgInSc1	první
vzhled	vzhled	k1gInSc1	vzhled
krystalků	krystalek	k1gInPc2	krystalek
soli	sůl	k1gFnSc2	sůl
kyseliny	kyselina	k1gFnSc2	kyselina
močové	močový	k2eAgFnPc4d1	močová
pod	pod	k7c7	pod
mikroskopem	mikroskop	k1gInSc7	mikroskop
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
si	se	k3xPyFc3	se
anglický	anglický	k2eAgMnSc1d1	anglický
lékař	lékař	k1gMnSc1	lékař
Alfred	Alfred	k1gMnSc1	Alfred
Baring	Baring	k1gInSc4	Baring
Garrod	Garroda	k1gFnPc2	Garroda
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
příčinou	příčina	k1gFnSc7	příčina
dny	dna	k1gFnSc2	dna
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
nadměrné	nadměrný	k2eAgNnSc4d1	nadměrné
množství	množství	k1gNnSc4	množství
kyseliny	kyselina	k1gFnSc2	kyselina
močové	močový	k2eAgFnSc2d1	močová
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jiní	jiný	k2eAgMnPc1d1	jiný
živočichové	živočich	k1gMnPc1	živočich
==	==	k?	==
</s>
</p>
<p>
<s>
U	u	k7c2	u
většiny	většina	k1gFnSc2	většina
ostatních	ostatní	k2eAgMnPc2d1	ostatní
živočichů	živočich	k1gMnPc2	živočich
se	se	k3xPyFc4	se
dna	dna	k1gFnSc1	dna
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
vzácně	vzácně	k6eAd1	vzácně
díky	díky	k7c3	díky
jejich	jejich	k3xOp3gFnSc3	jejich
schopnosti	schopnost	k1gFnSc3	schopnost
produkovat	produkovat	k5eAaImF	produkovat
enzym	enzym	k1gInSc4	enzym
urikázu	urikáza	k1gFnSc4	urikáza
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
štěpí	štěpit	k5eAaImIp3nS	štěpit
kyselinu	kyselina	k1gFnSc4	kyselina
močovou	močový	k2eAgFnSc4d1	močová
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
ani	ani	k8xC	ani
lidoopi	lidoop	k1gMnPc1	lidoop
ale	ale	k8xC	ale
tuto	tento	k3xDgFnSc4	tento
schopnost	schopnost	k1gFnSc4	schopnost
nemají	mít	k5eNaImIp3nP	mít
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
nich	on	k3xPp3gNnPc2	on
dna	dno	k1gNnSc2	dno
běžným	běžný	k2eAgNnSc7d1	běžné
onemocněním	onemocnění	k1gNnSc7	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dnou	dna	k1gFnSc7	dna
trpěl	trpět	k5eAaImAgMnS	trpět
i	i	k8xC	i
jedinec	jedinec	k1gMnSc1	jedinec
druhu	druh	k1gInSc2	druh
Tyrannosaurus	Tyrannosaurus	k1gMnSc1	Tyrannosaurus
rex	rex	k?	rex
známý	známý	k1gMnSc1	známý
jako	jako	k8xS	jako
Sue	Sue	k1gMnSc1	Sue
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výzkum	výzkum	k1gInSc1	výzkum
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
několik	několik	k4yIc1	několik
nových	nový	k2eAgInPc2d1	nový
léků	lék	k1gInPc2	lék
k	k	k7c3	k
léčbě	léčba	k1gFnSc3	léčba
dny	dna	k1gFnSc2	dna
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
anakinra	anakinra	k1gMnSc1	anakinra
<g/>
,	,	kIx,	,
kanakinumab	kanakinumab	k1gMnSc1	kanakinumab
nebo	nebo	k8xC	nebo
rilonacept	rilonacept	k1gMnSc1	rilonacept
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
je	být	k5eAaImIp3nS	být
také	také	k9	také
rekombinantní	rekombinantní	k2eAgFnSc1d1	rekombinantní
urikáza	urikáza	k1gFnSc1	urikáza
(	(	kIx(	(
<g/>
rasburikáza	rasburikáza	k1gFnSc1	rasburikáza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
možnosti	možnost	k1gFnSc3	možnost
podávání	podávání	k1gNnSc2	podávání
tohoto	tento	k3xDgInSc2	tento
enzymu	enzym	k1gInSc2	enzym
jsou	být	k5eAaImIp3nP	být
omezené	omezený	k2eAgFnPc1d1	omezená
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
autoimunitní	autoimunitní	k2eAgFnSc4d1	autoimunitní
odpověď	odpověď	k1gFnSc4	odpověď
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
vývoji	vývoj	k1gInSc6	vývoj
verze	verze	k1gFnSc2	verze
s	s	k7c7	s
méně	málo	k6eAd2	málo
antigenními	antigenní	k2eAgInPc7d1	antigenní
účinky	účinek	k1gInPc7	účinek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
dna	dno	k1gNnSc2	dno
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
dna	dno	k1gNnSc2	dno
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Pakostnice	pakostnice	k1gFnSc2	pakostnice
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
