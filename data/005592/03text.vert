<s>
Chemie	chemie	k1gFnSc1	chemie
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
χ	χ	k?	χ
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
lučba	lučba	k?	lučba
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
,	,	kIx,	,
složením	složení	k1gNnSc7	složení
<g/>
,	,	kIx,	,
přípravou	příprava	k1gFnSc7	příprava
<g/>
,	,	kIx,	,
strukturou	struktura	k1gFnSc7	struktura
anorganických	anorganický	k2eAgInPc2d1	anorganický
a	a	k8xC	a
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc7	jejich
vzájemnými	vzájemný	k2eAgFnPc7d1	vzájemná
interakcemi	interakce	k1gFnPc7	interakce
<g/>
.	.	kIx.	.
</s>
<s>
Látky	látka	k1gFnPc1	látka
jsou	být	k5eAaImIp3nP	být
složeny	složit	k5eAaPmNgFnP	složit
z	z	k7c2	z
atomů	atom	k1gInPc2	atom
a	a	k8xC	a
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
složených	složený	k2eAgFnPc2d1	složená
molekul	molekula	k1gFnPc2	molekula
nebo	nebo	k8xC	nebo
z	z	k7c2	z
iontů	ion	k1gInPc2	ion
(	(	kIx(	(
<g/>
iontové	iontový	k2eAgFnPc4d1	iontová
soli	sůl	k1gFnPc4	sůl
a	a	k8xC	a
taveniny	tavenina	k1gFnPc4	tavenina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chemie	chemie	k1gFnSc1	chemie
popisuje	popisovat	k5eAaImIp3nS	popisovat
svým	svůj	k3xOyFgInSc7	svůj
vlastním	vlastní	k2eAgInSc7d1	vlastní
metodickým	metodický	k2eAgInSc7d1	metodický
a	a	k8xC	a
teoretickým	teoretický	k2eAgInSc7d1	teoretický
přístupem	přístup	k1gInSc7	přístup
hmotu	hmota	k1gFnSc4	hmota
a	a	k8xC	a
navazuje	navazovat	k5eAaImIp3nS	navazovat
tak	tak	k9	tak
na	na	k7c4	na
studia	studio	k1gNnPc4	studio
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
a	a	k8xC	a
předchází	předcházet	k5eAaImIp3nS	předcházet
studiu	studio	k1gNnSc3	studio
hmoty	hmota	k1gFnSc2	hmota
z	z	k7c2	z
biologického	biologický	k2eAgInSc2d1	biologický
nebo	nebo	k8xC	nebo
např.	např.	kA	např.
geologického	geologický	k2eAgNnSc2d1	geologické
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
rozličnosti	rozličnost	k1gFnSc3	rozličnost
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
kombinací	kombinace	k1gFnPc2	kombinace
atomů	atom	k1gInPc2	atom
<g/>
,	,	kIx,	,
chemici	chemik	k1gMnPc1	chemik
často	často	k6eAd1	často
studují	studovat	k5eAaImIp3nP	studovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
atomy	atom	k1gInPc1	atom
odlišných	odlišný	k2eAgInPc2d1	odlišný
chemických	chemický	k2eAgInPc2d1	chemický
prvků	prvek	k1gInPc2	prvek
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
interagují	interagovat	k5eAaBmIp3nP	interagovat
a	a	k8xC	a
jaké	jaký	k3yQgFnPc1	jaký
molekuly	molekula	k1gFnPc1	molekula
a	a	k8xC	a
jakým	jaký	k3yRgInSc7	jaký
průběhem	průběh	k1gInSc7	průběh
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
interakcí	interakce	k1gFnPc2	interakce
vznikají	vznikat	k5eAaImIp3nP	vznikat
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
tohoto	tento	k3xDgInSc2	tento
vědního	vědní	k2eAgInSc2d1	vědní
oboru	obor	k1gInSc2	obor
chemie	chemie	k1gFnSc2	chemie
(	(	kIx(	(
<g/>
řec.	řec.	k?	řec.
χ	χ	k?	χ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
původně	původně	k6eAd1	původně
chymie	chymie	k1gFnSc1	chymie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
teprve	teprve	k6eAd1	teprve
ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
ve	v	k7c6	v
spisech	spis	k1gInPc6	spis
byzantských	byzantský	k2eAgMnPc2d1	byzantský
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
prapůvod	prapůvod	k1gInSc1	prapůvod
však	však	k9	však
není	být	k5eNaImIp3nS	být
dobře	dobře	k6eAd1	dobře
vysvětlen	vysvětlen	k2eAgInSc1d1	vysvětlen
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
bývá	bývat	k5eAaImIp3nS	bývat
s	s	k7c7	s
odvoláním	odvolání	k1gNnSc7	odvolání
na	na	k7c4	na
alexandrijského	alexandrijský	k2eAgMnSc4d1	alexandrijský
encyklopedického	encyklopedický	k2eAgMnSc4d1	encyklopedický
autora	autor	k1gMnSc4	autor
Zosima	Zosim	k1gMnSc4	Zosim
Panopolidského	Panopolidský	k2eAgInSc2d1	Panopolidský
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
odvozován	odvozovat	k5eAaImNgMnS	odvozovat
od	od	k7c2	od
názvu	název	k1gInSc2	název
jakési	jakýsi	k3yIgFnSc2	jakýsi
bájné	bájný	k2eAgFnSc2d1	bájná
knihy	kniha	k1gFnSc2	kniha
Χ	Χ	k?	Χ
(	(	kIx(	(
<g/>
Chémey	Chémea	k1gFnSc2	Chémea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
popisovala	popisovat	k5eAaImAgFnS	popisovat
návod	návod	k1gInSc4	návod
na	na	k7c4	na
zpracování	zpracování	k1gNnSc4	zpracování
kovů	kov	k1gInPc2	kov
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
prý	prý	k9	prý
božského	božský	k2eAgInSc2d1	božský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiných	jiný	k1gMnPc2	jiný
může	moct	k5eAaImIp3nS	moct
slovo	slovo	k1gNnSc4	slovo
chemie	chemie	k1gFnSc2	chemie
souviset	souviset	k5eAaImF	souviset
se	se	k3xPyFc4	se
starým	starý	k2eAgInSc7d1	starý
hebrejským	hebrejský	k2eAgInSc7d1	hebrejský
názvem	název	k1gInSc7	název
pro	pro	k7c4	pro
Egypt	Egypt	k1gInSc4	Egypt
<g/>
,	,	kIx,	,
Chemia	Chemia	k1gFnSc1	Chemia
(	(	kIx(	(
<g/>
země	země	k1gFnSc1	země
Chámova	chámův	k2eAgFnSc1d1	chámův
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
uvádí	uvádět	k5eAaImIp3nS	uvádět
již	již	k9	již
Plutarchos	Plutarchos	k1gInSc1	Plutarchos
<g/>
;	;	kIx,	;
tento	tento	k3xDgInSc1	tento
výklad	výklad	k1gInSc1	výklad
podporuje	podporovat	k5eAaImIp3nS	podporovat
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
starověcí	starověký	k2eAgMnPc1d1	starověký
Egypťané	Egypťan	k1gMnPc1	Egypťan
ovládali	ovládat	k5eAaImAgMnP	ovládat
řadu	řada	k1gFnSc4	řada
chemických	chemický	k2eAgFnPc2d1	chemická
znalostí	znalost	k1gFnPc2	znalost
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
etymologická	etymologický	k2eAgFnSc1d1	etymologická
teorie	teorie	k1gFnSc1	teorie
spojuje	spojovat	k5eAaImIp3nS	spojovat
chemii	chemie	k1gFnSc4	chemie
s	s	k7c7	s
řeckým	řecký	k2eAgNnSc7d1	řecké
slovem	slovo	k1gNnSc7	slovo
χ	χ	k?	χ
(	(	kIx(	(
<g/>
chymós	chymós	k1gInSc1	chymós
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
kapalina	kapalina	k1gFnSc1	kapalina
nebo	nebo	k8xC	nebo
šťáva	šťáva	k1gFnSc1	šťáva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
míněna	mínit	k5eAaImNgFnS	mínit
tinktura	tinktura	k1gFnSc1	tinktura
potřebná	potřebný	k2eAgFnSc1d1	potřebná
pro	pro	k7c4	pro
transmutaci	transmutace	k1gFnSc4	transmutace
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
hledaná	hledaná	k1gFnSc1	hledaná
alchymisty	alchymista	k1gMnSc2	alchymista
<g/>
.	.	kIx.	.
</s>
<s>
Látkové	látkový	k2eAgFnPc1d1	látková
přeměny	přeměna	k1gFnPc1	přeměna
jsou	být	k5eAaImIp3nP	být
základem	základ	k1gInSc7	základ
velké	velký	k2eAgFnSc2d1	velká
řady	řada	k1gFnSc2	řada
technologických	technologický	k2eAgInPc2d1	technologický
postupů	postup	k1gInPc2	postup
spojených	spojený	k2eAgInPc2d1	spojený
s	s	k7c7	s
osvojením	osvojení	k1gNnSc7	osvojení
ohně	oheň	k1gInSc2	oheň
(	(	kIx(	(
<g/>
výroba	výroba	k1gFnSc1	výroba
keramiky	keramika	k1gFnSc2	keramika
a	a	k8xC	a
vápna	vápno	k1gNnSc2	vápno
či	či	k8xC	či
úprava	úprava	k1gFnSc1	úprava
potravy	potrava	k1gFnSc2	potrava
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
zpracováním	zpracování	k1gNnSc7	zpracování
kůží	kůže	k1gFnPc2	kůže
a	a	k8xC	a
tkanin	tkanina	k1gFnPc2	tkanina
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
říct	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
chemie	chemie	k1gFnSc1	chemie
jako	jako	k8xC	jako
obor	obor	k1gInSc1	obor
sledující	sledující	k2eAgFnSc2d1	sledující
zákonitosti	zákonitost	k1gFnSc2	zákonitost
látkových	látkový	k2eAgFnPc2d1	látková
přeměn	přeměna	k1gFnPc2	přeměna
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
snad	snad	k9	snad
už	už	k6eAd1	už
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kamenné	kamenný	k2eAgInPc1d1	kamenný
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
navazovaly	navazovat	k5eAaImAgFnP	navazovat
technologie	technologie	k1gFnPc4	technologie
zpracování	zpracování	k1gNnSc2	zpracování
kovů	kov	k1gInPc2	kov
(	(	kIx(	(
<g/>
stříbro	stříbro	k1gNnSc1	stříbro
<g/>
,	,	kIx,	,
zlato	zlato	k1gNnSc1	zlato
<g/>
,	,	kIx,	,
měď	měď	k1gFnSc1	měď
<g/>
,	,	kIx,	,
bronz	bronz	k1gInSc1	bronz
<g/>
,	,	kIx,	,
železo	železo	k1gNnSc1	železo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
skla	sklo	k1gNnSc2	sklo
a	a	k8xC	a
také	také	k9	také
výroba	výroba	k1gFnSc1	výroba
kvasu	kvas	k1gInSc2	kvas
či	či	k8xC	či
piva	pivo	k1gNnSc2	pivo
ve	v	k7c6	v
starověkých	starověký	k2eAgFnPc6d1	starověká
říších	říš	k1gFnPc6	říš
(	(	kIx(	(
<g/>
Sumerové	Sumer	k1gMnPc1	Sumer
<g/>
,	,	kIx,	,
Egypt	Egypt	k1gInSc1	Egypt
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chemie	chemie	k1gFnSc1	chemie
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
několika	několik	k4yIc2	několik
oborů	obor	k1gInPc2	obor
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
častokrát	častokrát	k6eAd1	častokrát
provázány	provázán	k2eAgInPc4d1	provázán
interdisciplinárními	interdisciplinární	k2eAgInPc7d1	interdisciplinární
obory	obor	k1gInPc7	obor
a	a	k8xC	a
jinými	jiný	k2eAgFnPc7d1	jiná
specializacemi	specializace	k1gFnPc7	specializace
<g/>
.	.	kIx.	.
</s>
<s>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
analýzou	analýza	k1gFnSc7	analýza
vzorků	vzorek	k1gInPc2	vzorek
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
porozumět	porozumět	k5eAaPmF	porozumět
jejich	jejich	k3xOp3gNnSc3	jejich
chemickému	chemický	k2eAgNnSc3d1	chemické
složení	složení	k1gNnSc3	složení
a	a	k8xC	a
struktuře	struktura	k1gFnSc3	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Biochemie	biochemie	k1gFnSc1	biochemie
Biochemie	biochemie	k1gFnSc2	biochemie
studuje	studovat	k5eAaImIp3nS	studovat
chemické	chemický	k2eAgNnSc1d1	chemické
složení	složení	k1gNnSc1	složení
<g/>
,	,	kIx,	,
chemické	chemický	k2eAgFnPc1d1	chemická
reakce	reakce	k1gFnPc1	reakce
a	a	k8xC	a
vzájemné	vzájemný	k2eAgInPc1d1	vzájemný
vztahy	vztah	k1gInPc1	vztah
chemických	chemický	k2eAgNnPc2d1	chemické
individuí	individuum	k1gNnPc2	individuum
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
v	v	k7c6	v
živém	živý	k2eAgInSc6d1	živý
organismu	organismus	k1gInSc6	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
</s>
<s>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
studuje	studovat	k5eAaImIp3nS	studovat
vlastnosti	vlastnost	k1gFnPc4	vlastnost
a	a	k8xC	a
reakce	reakce	k1gFnPc4	reakce
anorganických	anorganický	k2eAgFnPc2d1	anorganická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Organická	organický	k2eAgFnSc1d1	organická
chemie	chemie	k1gFnSc1	chemie
Organická	organický	k2eAgFnSc1d1	organická
chemie	chemie	k1gFnSc1	chemie
studuje	studovat	k5eAaImIp3nS	studovat
strukturu	struktura	k1gFnSc4	struktura
<g/>
,	,	kIx,	,
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
složení	složení	k1gNnPc4	složení
a	a	k8xC	a
reakce	reakce	k1gFnPc4	reakce
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
organickou	organický	k2eAgFnSc7d1	organická
a	a	k8xC	a
anorganickou	anorganický	k2eAgFnSc7d1	anorganická
chemií	chemie	k1gFnSc7	chemie
není	být	k5eNaImIp3nS	být
vždy	vždy	k6eAd1	vždy
zřetelný	zřetelný	k2eAgInSc1d1	zřetelný
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
zde	zde	k6eAd1	zde
k	k	k7c3	k
mnoha	mnoho	k4c3	mnoho
překryvům	překryv	k1gInPc3	překryv
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
organokovové	organokovový	k2eAgFnSc6d1	organokovová
chemii	chemie	k1gFnSc6	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
chemie	chemie	k1gFnSc1	chemie
Fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
chemie	chemie	k1gFnSc2	chemie
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
fyzikálním	fyzikální	k2eAgInSc7d1	fyzikální
popisem	popis	k1gInSc7	popis
chemických	chemický	k2eAgInPc2d1	chemický
systémů	systém	k1gInPc2	systém
a	a	k8xC	a
procesů	proces	k1gInPc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
energetickým	energetický	k2eAgInSc7d1	energetický
popisem	popis	k1gInSc7	popis
rozdílných	rozdílný	k2eAgFnPc2d1	rozdílná
chemických	chemický	k2eAgFnPc2d1	chemická
přeměn	přeměna	k1gFnPc2	přeměna
<g/>
.	.	kIx.	.
</s>
<s>
Studuje	studovat	k5eAaImIp3nS	studovat
chemickou	chemický	k2eAgFnSc4d1	chemická
termodynamiku	termodynamika	k1gFnSc4	termodynamika
<g/>
,	,	kIx,	,
chemickou	chemický	k2eAgFnSc4d1	chemická
kinetiku	kinetika	k1gFnSc4	kinetika
<g/>
,	,	kIx,	,
elektrochemii	elektrochemie	k1gFnSc4	elektrochemie
<g/>
,	,	kIx,	,
kvantovou	kvantový	k2eAgFnSc4d1	kvantová
chemii	chemie	k1gFnSc4	chemie
<g/>
,	,	kIx,	,
statistickou	statistický	k2eAgFnSc4d1	statistická
termodynamiku	termodynamika	k1gFnSc4	termodynamika
a	a	k8xC	a
spektroskopii	spektroskopie	k1gFnSc4	spektroskopie
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
existují	existovat	k5eAaImIp3nP	existovat
Biofyzikální	biofyzikální	k2eAgFnSc2d1	biofyzikální
chemie	chemie	k1gFnSc2	chemie
<g/>
,	,	kIx,	,
Farmaceutická	farmaceutický	k2eAgFnSc1d1	farmaceutická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
Geochemie	geochemie	k1gFnSc1	geochemie
<g/>
,	,	kIx,	,
Petrochemie	petrochemie	k1gFnSc1	petrochemie
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
silikátů	silikát	k1gInPc2	silikát
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc2	chemie
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
Agrochemie	agrochemie	k1gFnSc1	agrochemie
<g/>
,	,	kIx,	,
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
Kvasná	kvasný	k2eAgFnSc1d1	kvasná
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
Lékařská	lékařský	k2eAgFnSc1d1	lékařská
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
Makromolekulární	makromolekulární	k2eAgFnSc1d1	makromolekulární
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
Supramolekulární	Supramolekulární	k2eAgFnSc1d1	Supramolekulární
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
Termochemie	termochemie	k1gFnSc1	termochemie
a	a	k8xC	a
Výpočetní	výpočetní	k2eAgFnSc1d1	výpočetní
chemie	chemie	k1gFnSc1	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Chemik	chemik	k1gMnSc1	chemik
Chemické	chemický	k2eAgInPc4d1	chemický
sjezdy	sjezd	k1gInPc4	sjezd
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
chemie	chemie	k1gFnSc2	chemie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
chemie	chemie	k1gFnSc2	chemie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Charakteristiky	charakteristika	k1gFnSc2	charakteristika
chemických	chemický	k2eAgInPc2d1	chemický
prvků	prvek	k1gInPc2	prvek
Chemický	chemický	k2eAgMnSc1d1	chemický
a	a	k8xC	a
potravinářský	potravinářský	k2eAgInSc1d1	potravinářský
portál	portál	k1gInSc1	portál
EuroChem	EuroCh	k1gInSc7	EuroCh
-	-	kIx~	-
chemický	chemický	k2eAgInSc1d1	chemický
portál	portál	k1gInSc1	portál
DMOZ	DMOZ	kA	DMOZ
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
