<s>
Michaela	Michaela	k1gFnSc1	Michaela
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
Michala	Michala	k1gFnSc1	Michala
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ženské	ženský	k2eAgNnSc4d1	ženské
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
českého	český	k2eAgInSc2d1	český
kalendáře	kalendář	k1gInSc2	kalendář
má	mít	k5eAaImIp3nS	mít
svátek	svátek	k1gInSc1	svátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
jména	jméno	k1gNnSc2	jméno
je	být	k5eAaImIp3nS	být
hebrejský	hebrejský	k2eAgInSc1d1	hebrejský
<g/>
,	,	kIx,	,
מ	מ	k?	מ
<g/>
ִ	ִ	k?	ִ
<g/>
י	י	k?	י
<g/>
ָ	ָ	k?	ָ
<g/>
א	א	k?	א
<g/>
ֵ	ֵ	k?	ֵ
<g/>
ל	ל	k?	ל
(	(	kIx(	(
<g/>
Mika	Mik	k1gMnSc2	Mik
<g/>
'	'	kIx"	'
<g/>
el	ela	k1gFnPc2	ela
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
podobný	podobný	k2eAgInSc1d1	podobný
Bohu	bůh	k1gMnSc6	bůh
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zdrobnělina	zdrobnělina	k1gFnSc1	zdrobnělina
Mika	Mik	k1gMnSc2	Mik
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
japonském	japonský	k2eAgInSc6d1	japonský
přepise	přepis	k1gInSc6	přepis
krásná	krásný	k2eAgFnSc1d1	krásná
vůně	vůně	k1gFnSc1	vůně
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
četnost	četnost	k1gFnSc4	četnost
jména	jméno	k1gNnSc2	jméno
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
pořadí	pořadí	k1gNnSc2	pořadí
mezi	mezi	k7c7	mezi
ženskými	ženský	k2eAgNnPc7d1	ženské
jmény	jméno	k1gNnPc7	jméno
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
dvou	dva	k4xCgInPc2	dva
roků	rok	k1gInPc2	rok
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yQgInPc4	který
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
údaje	údaj	k1gInPc1	údaj
MV	MV	kA	MV
ČR	ČR	kA	ČR
–	–	k?	–
lze	lze	k6eAd1	lze
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
tedy	tedy	k9	tedy
vysledovat	vysledovat	k5eAaPmF	vysledovat
trend	trend	k1gInSc4	trend
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
Velmi	velmi	k6eAd1	velmi
častý	častý	k2eAgInSc1d1	častý
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
procentního	procentní	k2eAgNnSc2d1	procentní
zastoupení	zastoupení	k1gNnSc2	zastoupení
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
mezi	mezi	k7c7	mezi
žijícími	žijící	k2eAgFnPc7d1	žijící
ženami	žena	k1gFnPc7	žena
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
procentní	procentní	k2eAgFnSc1d1	procentní
změna	změna	k1gFnSc1	změna
se	se	k3xPyFc4	se
započítáním	započítání	k1gNnSc7	započítání
celkového	celkový	k2eAgInSc2d1	celkový
úbytku	úbytek	k1gInSc2	úbytek
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
ČR	ČR	kA	ČR
za	za	k7c4	za
sledované	sledovaný	k2eAgInPc4d1	sledovaný
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
+4,4	+4,4	k4	+4,4
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
poměrně	poměrně	k6eAd1	poměrně
strmém	strmý	k2eAgInSc6d1	strmý
nárůstu	nárůst	k1gInSc6	nárůst
obliby	obliba	k1gFnSc2	obliba
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2006	[number]	k4	2006
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
ČSÚ	ČSÚ	kA	ČSÚ
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
12	[number]	k4	12
<g/>
.	.	kIx.	.
nejčastější	častý	k2eAgNnSc4d3	nejčastější
ženské	ženský	k2eAgNnSc4d1	ženské
jméno	jméno	k1gNnSc4	jméno
mezi	mezi	k7c7	mezi
novorozenci	novorozenec	k1gMnPc7	novorozenec
<g/>
.	.	kIx.	.
</s>
<s>
Míša	Míša	k1gFnSc1	Míša
<g/>
,	,	kIx,	,
Michalka	Michalka	k1gFnSc1	Michalka
<g/>
,	,	kIx,	,
Miška	Miška	k1gFnSc1	Miška
<g/>
,	,	kIx,	,
Mišička	Mišička	k1gFnSc1	Mišička
<g/>
,	,	kIx,	,
Mišulka	Mišulka	k1gFnSc1	Mišulka
<g/>
,	,	kIx,	,
Mišutka	Mišutka	k1gFnSc1	Mišutka
<g/>
,	,	kIx,	,
Míšenka	míšenka	k1gFnSc1	míšenka
<g/>
,	,	kIx,	,
Elka	Elka	k1gFnSc1	Elka
<g/>
,	,	kIx,	,
Mušina	mušina	k1gFnSc1	mušina
slovensky	slovensky	k6eAd1	slovensky
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Michaela	Michaela	k1gFnSc1	Michaela
polsky	polsky	k6eAd1	polsky
<g/>
:	:	kIx,	:
Michalina	Michalin	k2eAgFnSc1d1	Michalina
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
Michajlina	Michajlina	k1gFnSc1	Michajlina
srbochorvatsky	srbochorvatsky	k6eAd1	srbochorvatsky
<g/>
,	,	kIx,	,
bulharsky	bulharsky	k6eAd1	bulharsky
<g/>
:	:	kIx,	:
Michaila	Michaila	k1gFnSc1	Michaila
<g />
.	.	kIx.	.
</s>
<s>
maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
:	:	kIx,	:
Michaéla	Michaéla	k1gFnSc1	Michaéla
italsky	italsky	k6eAd1	italsky
<g/>
:	:	kIx,	:
Micaela	Micaela	k1gFnSc1	Micaela
španělsky	španělsky	k6eAd1	španělsky
<g/>
:	:	kIx,	:
Miguela	Miguela	k1gFnSc1	Miguela
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
:	:	kIx,	:
Michéle	Michéle	k1gNnSc4	Michéle
nizozemsky	nizozemsky	k6eAd1	nizozemsky
<g/>
:	:	kIx,	:
Michelina	Michelina	k1gFnSc1	Michelina
nebo	nebo	k8xC	nebo
Michaëla	Michaëla	k1gFnSc1	Michaëla
dánsky	dánsky	k6eAd1	dánsky
<g/>
:	:	kIx,	:
Michala	Michala	k1gFnSc1	Michala
norsky	norsky	k6eAd1	norsky
<g/>
:	:	kIx,	:
Michelle	Michelle	k1gNnSc4	Michelle
švédsky	švédsky	k6eAd1	švédsky
<g/>
:	:	kIx,	:
Mikaela	Mikaela	k1gFnSc1	Mikaela
Změna	změna	k1gFnSc1	změna
procentního	procentní	k2eAgNnSc2d1	procentní
zastoupení	zastoupení	k1gNnSc2	zastoupení
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
mezi	mezi	k7c7	mezi
žijícími	žijící	k2eAgFnPc7d1	žijící
ženami	žena	k1gFnPc7	žena
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
procentní	procentní	k2eAgFnSc1d1	procentní
změna	změna	k1gFnSc1	změna
se	se	k3xPyFc4	se
započítáním	započítání	k1gNnSc7	započítání
celkového	celkový	k2eAgInSc2d1	celkový
úbytku	úbytek	k1gInSc2	úbytek
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
ČR	ČR	kA	ČR
za	za	k7c4	za
sledované	sledovaný	k2eAgInPc4d1	sledovaný
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
+4,8	+4,8	k4	+4,8
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
poměrně	poměrně	k6eAd1	poměrně
strmém	strmý	k2eAgInSc6d1	strmý
nárůstu	nárůst	k1gInSc6	nárůst
obliby	obliba	k1gFnSc2	obliba
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Michaela	Michaela	k1gFnSc1	Michaela
Badinková	Badinkový	k2eAgFnSc1d1	Badinková
–	–	k?	–
slovenská	slovenský	k2eAgFnSc1d1	slovenská
herečka	herečka	k1gFnSc1	herečka
Michaela	Michaela	k1gFnSc1	Michaela
Dorfmeisterová	Dorfmeisterová	k1gFnSc1	Dorfmeisterová
–	–	k?	–
rakouská	rakouský	k2eAgFnSc1d1	rakouská
lyžařka	lyžařka	k1gFnSc1	lyžařka
Michaela	Michaela	k1gFnSc1	Michaela
Horká	Horká	k1gFnSc1	Horká
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Michaela	Michaela	k1gFnSc1	Michaela
Klimková	Klimková	k1gFnSc1	Klimková
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
lékařka	lékařka	k1gFnSc1	lékařka
a	a	k8xC	a
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Míša	Míša	k1gFnSc1	Míša
Michaela	Michaela	k1gFnSc1	Michaela
Kociánová	Kociánová	k1gFnSc1	Kociánová
–	–	k?	–
slovenská	slovenský	k2eAgFnSc1d1	slovenská
topmodelka	topmodelka	k1gFnSc1	topmodelka
Michaela	Michaela	k1gFnSc1	Michaela
Krutská	Krutský	k2eAgFnSc1d1	Krutská
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
krasobruslařka	krasobruslařka	k1gFnSc1	krasobruslařka
Michaela	Michaela	k1gFnSc1	Michaela
Kuklová	Kuklová	k1gFnSc1	Kuklová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
Michaela	Michaela	k1gFnSc1	Michaela
Kožíšková	Kožíšková	k1gFnSc1	Kožíšková
(	(	kIx(	(
<g/>
dívčím	dívčí	k2eAgNnSc7d1	dívčí
příjmením	příjmení	k1gNnSc7	příjmení
Maurerová	Maurerová	k1gFnSc1	Maurerová
<g/>
)	)	kIx)	)
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
Michaela	Michaela	k1gFnSc1	Michaela
May	May	k1gMnSc1	May
–	–	k?	–
německá	německý	k2eAgFnSc1d1	německá
herečka	herečka	k1gFnSc1	herečka
Michaela	Michaela	k1gFnSc1	Michaela
McManus	McManus	k1gInSc4	McManus
–	–	k?	–
americká	americký	k2eAgFnSc1d1	americká
herečka	herečka	k1gFnSc1	herečka
Michaela	Michaela	k1gFnSc1	Michaela
Musilová	Musilová	k1gFnSc1	Musilová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
střelkyně	střelkyně	k1gFnSc1	střelkyně
Misha	Misha	k1gFnSc1	Misha
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Michaela	Michael	k1gMnSc2	Michael
Paľová	Paľový	k2eAgFnSc1d1	Paľový
–	–	k?	–
slovenská	slovenský	k2eAgFnSc1d1	slovenská
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Michaela	Michaela	k1gFnSc1	Michaela
Paštěková	Paštěková	k1gFnSc1	Paštěková
<g/>
,	,	kIx,	,
uměleckým	umělecký	k2eAgNnSc7d1	umělecké
jménem	jméno	k1gNnSc7	jméno
Michaela	Michaela	k1gFnSc1	Michaela
–	–	k?	–
slovenská	slovenský	k2eAgFnSc1d1	slovenská
raperka	raperka	k1gFnSc1	raperka
Michaela	Michaela	k1gFnSc1	Michaela
Salačová	Salačová	k1gFnSc1	Salačová
–	–	k?	–
česká	český	k2eAgNnPc4d1	české
DJ	DJ	kA	DJ
<g/>
,	,	kIx,	,
modelka	modelka	k1gFnSc1	modelka
<g/>
,	,	kIx,	,
Miss	miss	k1gFnSc1	miss
Michaela	Michael	k1gMnSc2	Michael
Škultéty	Škultéta	k1gFnSc2	Škultéta
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
překladatelka	překladatelka	k1gFnSc1	překladatelka
Michaela	Michaela	k1gFnSc1	Michaela
Šojdrová	Šojdrová	k1gFnSc1	Šojdrová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
politička	politička	k1gFnSc1	politička
</s>
