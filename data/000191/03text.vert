<s>
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
a	a	k8xC	a
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
dalších	další	k2eAgInPc6d1	další
jazycích	jazyk	k1gInPc6	jazyk
Wikipedia	Wikipedium	k1gNnSc2	Wikipedium
<g/>
;	;	kIx,	;
název	název	k1gInSc1	název
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
ze	z	k7c2	z
slov	slovo	k1gNnPc2	slovo
wiki	wik	k1gFnSc2	wik
a	a	k8xC	a
encyclopedia	encyclopedium	k1gNnSc2	encyclopedium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mnohojazyčná	mnohojazyčný	k2eAgFnSc1d1	mnohojazyčná
webová	webový	k2eAgFnSc1d1	webová
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
se	s	k7c7	s
svobodným	svobodný	k2eAgInSc7d1	svobodný
(	(	kIx(	(
<g/>
otevřeným	otevřený	k2eAgInSc7d1	otevřený
<g/>
)	)	kIx)	)
obsahem	obsah	k1gInSc7	obsah
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
tvorbě	tvorba	k1gFnSc6	tvorba
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
dobrovolní	dobrovolný	k2eAgMnPc1d1	dobrovolný
přispěvatelé	přispěvatel	k1gMnPc1	přispěvatel
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
tvorba	tvorba	k1gFnSc1	tvorba
a	a	k8xC	a
celosvětové	celosvětový	k2eAgNnSc1d1	celosvětové
šíření	šíření	k1gNnSc1	šíření
volně	volně	k6eAd1	volně
přístupných	přístupný	k2eAgFnPc2d1	přístupná
encyklopedických	encyklopedický	k2eAgFnPc2d1	encyklopedická
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
existuje	existovat	k5eAaImIp3nS	existovat
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
270	[number]	k4	270
jazykových	jazykový	k2eAgFnPc6d1	jazyková
verzích	verze	k1gFnPc6	verze
různého	různý	k2eAgInSc2d1	různý
rozsahu	rozsah	k1gInSc2	rozsah
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
rozsah	rozsah	k1gInSc4	rozsah
zhruba	zhruba	k6eAd1	zhruba
třetiny	třetina	k1gFnPc1	třetina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
symbolický	symbolický	k2eAgInSc1d1	symbolický
<g/>
.	.	kIx.	.
</s>
<s>
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
projektů	projekt	k1gInPc2	projekt
Nadace	nadace	k1gFnSc2	nadace
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgMnPc7	jenž
je	být	k5eAaImIp3nS	být
vzájemně	vzájemně	k6eAd1	vzájemně
provázána	provázat	k5eAaPmNgFnS	provázat
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
koordinaci	koordinace	k1gFnSc3	koordinace
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
"	"	kIx"	"
<g/>
wikiprojektů	wikiprojekt	k1gInPc2	wikiprojekt
<g/>
"	"	kIx"	"
slouží	sloužit	k5eAaImIp3nS	sloužit
projekt	projekt	k1gInSc4	projekt
Meta-Wiki	Meta-Wik	k1gFnSc2	Meta-Wik
<g/>
.	.	kIx.	.
</s>
<s>
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
funguje	fungovat	k5eAaImIp3nS	fungovat
na	na	k7c6	na
principu	princip	k1gInSc6	princip
tzv.	tzv.	kA	tzv.
wiki	wiki	k6eAd1	wiki
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nový	nový	k2eAgInSc1d1	nový
článek	článek	k1gInSc1	článek
může	moct	k5eAaImIp3nS	moct
vložit	vložit	k5eAaPmF	vložit
a	a	k8xC	a
libovolný	libovolný	k2eAgInSc4d1	libovolný
článek	článek	k1gInSc4	článek
může	moct	k5eAaImIp3nS	moct
změnit	změnit	k5eAaPmF	změnit
(	(	kIx(	(
<g/>
ať	ať	k9	ať
už	už	k6eAd1	už
pouze	pouze	k6eAd1	pouze
opravit	opravit	k5eAaPmF	opravit
překlep	překlep	k1gInSc4	překlep
<g/>
,	,	kIx,	,
zkorigovat	zkorigovat	k5eAaPmF	zkorigovat
věcnou	věcný	k2eAgFnSc4d1	věcná
chybu	chyba	k1gFnSc4	chyba
<g/>
,	,	kIx,	,
článek	článek	k1gInSc4	článek
výrazně	výrazně	k6eAd1	výrazně
rozšířit	rozšířit	k5eAaPmF	rozšířit
nebo	nebo	k8xC	nebo
zcela	zcela	k6eAd1	zcela
přepsat	přepsat	k5eAaPmF	přepsat
<g/>
)	)	kIx)	)
takřka	takřka	k6eAd1	takřka
kdokoli	kdokoli	k3yInSc1	kdokoli
s	s	k7c7	s
přístupem	přístup	k1gInSc7	přístup
na	na	k7c4	na
web	web	k1gInSc4	web
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
otevřenost	otevřenost	k1gFnSc1	otevřenost
ovšem	ovšem	k9	ovšem
s	s	k7c7	s
sebou	se	k3xPyFc7	se
nese	nést	k5eAaImIp3nS	nést
také	také	k9	také
riziko	riziko	k1gNnSc1	riziko
nepřesností	nepřesnost	k1gFnPc2	nepřesnost
či	či	k8xC	či
tzv.	tzv.	kA	tzv.
vandalismu	vandalismus	k1gInSc2	vandalismus
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgNnSc7	jeden
ze	z	k7c2	z
základních	základní	k2eAgNnPc2d1	základní
pravidel	pravidlo	k1gNnPc2	pravidlo
prosazovaných	prosazovaný	k2eAgNnPc2d1	prosazované
uživateli	uživatel	k1gMnSc3	uživatel
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
nezaujatý	zaujatý	k2eNgInSc4d1	nezaujatý
úhel	úhel	k1gInSc4	úhel
pohledu	pohled	k1gInSc2	pohled
-	-	kIx~	-
články	článek	k1gInPc1	článek
musí	muset	k5eAaImIp3nP	muset
nezaujatě	zaujatě	k6eNd1	zaujatě
prezentovat	prezentovat	k5eAaBmF	prezentovat
všechny	všechen	k3xTgInPc4	všechen
podstatné	podstatný	k2eAgInPc4d1	podstatný
názory	názor	k1gInPc4	názor
na	na	k7c4	na
příslušné	příslušný	k2eAgNnSc4d1	příslušné
téma	téma	k1gNnSc4	téma
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nP	by
některý	některý	k3yIgInSc4	některý
vydávaly	vydávat	k5eAaImAgFnP	vydávat
za	za	k7c4	za
jedinou	jediný	k2eAgFnSc4d1	jediná
objektivní	objektivní	k2eAgFnSc4d1	objektivní
pravdu	pravda	k1gFnSc4	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Požadavek	požadavek	k1gInSc1	požadavek
na	na	k7c4	na
ověřitelnost	ověřitelnost	k1gFnSc4	ověřitelnost
údajů	údaj	k1gInPc2	údaj
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
je	být	k5eAaImIp3nS	být
uplatňován	uplatňovat	k5eAaImNgInS	uplatňovat
zejména	zejména	k9	zejména
u	u	k7c2	u
kontroverzních	kontroverzní	k2eAgInPc2d1	kontroverzní
údajů	údaj	k1gInPc2	údaj
a	a	k8xC	a
témat	téma	k1gNnPc2	téma
<g/>
.	.	kIx.	.
</s>
<s>
Zárodek	zárodek	k1gInSc1	zárodek
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
programátor	programátor	k1gInSc1	programátor
Ward	Ward	k1gInSc1	Ward
Cunningham	Cunningham	k1gInSc4	Cunningham
založil	založit	k5eAaPmAgInS	založit
webové	webový	k2eAgFnPc4d1	webová
stránky	stránka	k1gFnPc4	stránka
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
WikiWikiWeb	WikiWikiWba	k1gFnPc2	WikiWikiWba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
mohl	moct	k5eAaImAgMnS	moct
upravovat	upravovat	k5eAaImF	upravovat
každý	každý	k3xTgMnSc1	každý
jejich	jejich	k3xOp3gMnSc1	jejich
návštěvník	návštěvník	k1gMnSc1	návštěvník
-	-	kIx~	-
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
podobných	podobný	k2eAgInPc2d1	podobný
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
označovaných	označovaný	k2eAgNnPc2d1	označované
jako	jako	k8xS	jako
wiki	wiki	k1gNnSc4	wiki
(	(	kIx(	(
<g/>
havajské	havajský	k2eAgNnSc4d1	havajské
slovo	slovo	k1gNnSc4	slovo
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
rychlý	rychlý	k2eAgInSc4d1	rychlý
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
míněno	míněn	k2eAgNnSc4d1	míněno
stránky	stránka	k1gFnPc4	stránka
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgInPc6	který
může	moct	k5eAaImIp3nS	moct
návštěvník	návštěvník	k1gMnSc1	návštěvník
cokoliv	cokoliv	k3yInSc4	cokoliv
rychle	rychle	k6eAd1	rychle
opravit	opravit	k5eAaPmF	opravit
<g/>
)	)	kIx)	)
objevilo	objevit	k5eAaPmAgNnS	objevit
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
používány	používán	k2eAgInPc1d1	používán
např.	např.	kA	např.
jako	jako	k8xS	jako
vnitrofiremní	vnitrofiremní	k2eAgFnSc2d1	vnitrofiremní
báze	báze	k1gFnSc2	báze
znalostí	znalost	k1gFnPc2	znalost
<g/>
.	.	kIx.	.
</s>
<s>
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
jako	jako	k8xS	jako
taková	takový	k3xDgFnSc1	takový
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2001	[number]	k4	2001
jako	jako	k8xS	jako
doplňkový	doplňkový	k2eAgInSc1d1	doplňkový
projekt	projekt	k1gInSc1	projekt
k	k	k7c3	k
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
neexistující	existující	k2eNgFnSc6d1	neexistující
encyklopedii	encyklopedie	k1gFnSc6	encyklopedie
Nupedia	Nupedium	k1gNnSc2	Nupedium
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
mohli	moct	k5eAaImAgMnP	moct
přispívat	přispívat	k5eAaImF	přispívat
jen	jen	k9	jen
odborníci	odborník	k1gMnPc1	odborník
<g/>
.	.	kIx.	.
</s>
<s>
Zakladateli	zakladatel	k1gMnSc3	zakladatel
obou	dva	k4xCgInPc2	dva
projektů	projekt	k1gInPc2	projekt
byli	být	k5eAaImAgMnP	být
Američané	Američan	k1gMnPc1	Američan
Jimmy	Jimma	k1gFnSc2	Jimma
Wales	Wales	k1gInSc1	Wales
(	(	kIx(	(
<g/>
přezdívaný	přezdívaný	k2eAgMnSc1d1	přezdívaný
Jimbo	Jimba	k1gMnSc5	Jimba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
internetový	internetový	k2eAgMnSc1d1	internetový
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g/>
,	,	kIx,	,
a	a	k8xC	a
Larry	Larra	k1gFnPc1	Larra
Sanger	Sanger	k1gInSc1	Sanger
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
a	a	k8xC	a
informatik	informatik	k1gMnSc1	informatik
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
brzy	brzy	k6eAd1	brzy
z	z	k7c2	z
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
odešel	odejít	k5eAaPmAgMnS	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Wales	Wales	k1gInSc1	Wales
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2003	[number]	k4	2003
založil	založit	k5eAaPmAgMnS	založit
podle	podle	k7c2	podle
zákonů	zákon	k1gInPc2	zákon
státu	stát	k1gInSc2	stát
Florida	Florida	k1gFnSc1	Florida
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
neziskovou	ziskový	k2eNgFnSc4d1	nezisková
nadaci	nadace	k1gFnSc4	nadace
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yIgFnSc4	který
převedl	převést	k5eAaPmAgMnS	převést
autorská	autorský	k2eAgNnPc4d1	autorské
a	a	k8xC	a
vlastnická	vlastnický	k2eAgNnPc4d1	vlastnické
práva	právo	k1gNnPc4	právo
související	související	k2eAgFnSc1d1	související
s	s	k7c7	s
Wikipedií	Wikipedie	k1gFnSc7	Wikipedie
i	i	k8xC	i
sesterskými	sesterský	k2eAgInPc7d1	sesterský
projekty	projekt	k1gInPc7	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Sanger	Sanger	k1gInSc1	Sanger
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
rozpoutal	rozpoutat	k5eAaPmAgMnS	rozpoutat
diskusi	diskuse	k1gFnSc4	diskuse
článkem	článek	k1gInSc7	článek
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
anti-elitářství	antilitářství	k1gNnSc2	anti-elitářství
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
od	od	k7c2	od
spolupráce	spolupráce	k1gFnSc2	spolupráce
odrazuje	odrazovat	k5eAaImIp3nS	odrazovat
experty	expert	k1gMnPc4	expert
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
založil	založit	k5eAaPmAgInS	založit
vlastní	vlastní	k2eAgInSc1d1	vlastní
projekt	projekt	k1gInSc1	projekt
Citizendium	Citizendium	k1gNnSc4	Citizendium
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
tento	tento	k3xDgInSc4	tento
nedostatek	nedostatek	k1gInSc4	nedostatek
napravovat	napravovat	k5eAaImF	napravovat
<g/>
.	.	kIx.	.
</s>
<s>
Popularita	popularita	k1gFnSc1	popularita
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
vytrvale	vytrvale	k6eAd1	vytrvale
roste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
vedle	vedle	k7c2	vedle
ní	on	k3xPp3gFnSc2	on
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
také	také	k6eAd1	také
několik	několik	k4yIc1	několik
sesterských	sesterský	k2eAgInPc2d1	sesterský
projektů	projekt	k1gInPc2	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejpopulárnější	populární	k2eAgFnPc4d3	nejpopulárnější
referenční	referenční	k2eAgFnPc4d1	referenční
stránky	stránka	k1gFnPc4	stránka
na	na	k7c6	na
webu	web	k1gInSc6	web
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
den	den	k1gInSc1	den
obslouží	obsloužit	k5eAaPmIp3nS	obsloužit
přibližně	přibližně	k6eAd1	přibližně
60	[number]	k4	60
milionů	milion	k4xCgInPc2	milion
požadavků	požadavek	k1gInPc2	požadavek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počtu	počet	k1gInSc2	počet
návštěv	návštěva	k1gFnPc2	návštěva
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2013	[number]	k4	2013
na	na	k7c4	na
7	[number]	k4	7
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgFnPc7	všecek
internetovými	internetový	k2eAgFnPc7d1	internetová
stránkami	stránka	k1gFnPc7	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
verze	verze	k1gFnSc1	verze
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
na	na	k7c6	na
CD	CD	kA	CD
a	a	k8xC	a
tisícistránkový	tisícistránkový	k2eAgInSc4d1	tisícistránkový
výběr	výběr	k1gInSc4	výběr
asi	asi	k9	asi
20	[number]	k4	20
000	[number]	k4	000
článků	článek	k1gInPc2	článek
německé	německý	k2eAgFnSc2d1	německá
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
respektovanou	respektovaný	k2eAgFnSc7d1	respektovaná
vydavatelskou	vydavatelský	k2eAgFnSc7d1	vydavatelská
skupinou	skupina	k1gFnSc7	skupina
Bertelsmann	Bertelsmanna	k1gFnPc2	Bertelsmanna
v	v	k7c6	v
září	září	k1gNnSc6	září
2008	[number]	k4	2008
vydán	vydat	k5eAaPmNgInS	vydat
také	také	k9	také
knižně	knižně	k6eAd1	knižně
Das	Das	k1gMnSc1	Das
Wikipedia-Lexikon	Wikipedia-Lexikon	k1gMnSc1	Wikipedia-Lexikon
in	in	k?	in
einem	einem	k1gInSc1	einem
Band	banda	k1gFnPc2	banda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Internetu	Internet	k1gInSc6	Internet
existuje	existovat	k5eAaImIp3nS	existovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
serverů	server	k1gInPc2	server
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
kopii	kopie	k1gFnSc4	kopie
obsahu	obsah	k1gInSc2	obsah
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
nabízejí	nabízet	k5eAaImIp3nP	nabízet
za	za	k7c2	za
podmínek	podmínka	k1gFnPc2	podmínka
licence	licence	k1gFnSc2	licence
GFDL	GFDL	kA	GFDL
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2005	[number]	k4	2005
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
dohromady	dohromady	k6eAd1	dohromady
přibližně	přibližně	k6eAd1	přibližně
1,5	[number]	k4	1,5
milionu	milion	k4xCgInSc2	milion
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2006	[number]	k4	2006
přes	přes	k7c4	přes
5,6	[number]	k4	5,6
milionů	milion	k4xCgInPc2	milion
a	a	k8xC	a
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2008	[number]	k4	2008
již	již	k6eAd1	již
přes	přes	k7c4	přes
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
článků	článek	k1gInPc2	článek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2016	[number]	k4	2016
měla	mít	k5eAaImAgFnS	mít
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
přes	přes	k7c4	přes
39,3	[number]	k4	39,3
milionů	milion	k4xCgInPc2	milion
článků	článek	k1gInPc2	článek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
fundraising	fundraising	k1gInSc1	fundraising
a	a	k8xC	a
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
tak	tak	k6eAd1	tak
získala	získat	k5eAaPmAgFnS	získat
20	[number]	k4	20
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
provoz	provoz	k1gInSc4	provoz
a	a	k8xC	a
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
tak	tak	k6eAd1	tak
dále	daleko	k6eAd2	daleko
bez	bez	k7c2	bez
reklamních	reklamní	k2eAgInPc2d1	reklamní
prostorů	prostor	k1gInPc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2016	[number]	k4	2016
existovala	existovat	k5eAaImAgFnS	existovat
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
ve	v	k7c6	v
292	[number]	k4	292
jazykových	jazykový	k2eAgFnPc6d1	jazyková
verzích	verze	k1gFnPc6	verze
<g/>
.	.	kIx.	.
</s>
<s>
Anglická	anglický	k2eAgFnSc1d1	anglická
verze	verze	k1gFnSc1	verze
měla	mít	k5eAaImAgFnS	mít
přes	přes	k7c4	přes
5	[number]	k4	5
milionu	milion	k4xCgInSc2	milion
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
dalších	další	k2eAgFnPc2d1	další
12	[number]	k4	12
verzí	verze	k1gFnPc2	verze
mělo	mít	k5eAaImAgNnS	mít
přes	přes	k7c4	přes
milion	milion	k4xCgInSc4	milion
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
45	[number]	k4	45
přes	přes	k7c4	přes
100	[number]	k4	100
000	[number]	k4	000
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
75	[number]	k4	75
verzí	verze	k1gFnPc2	verze
více	hodně	k6eAd2	hodně
než	než	k8xS	než
10	[number]	k4	10
000	[number]	k4	000
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
110	[number]	k4	110
verzí	verze	k1gFnPc2	verze
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1000	[number]	k4	1000
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
39	[number]	k4	39
verzí	verze	k1gFnPc2	verze
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
článků	článek	k1gInPc2	článek
a	a	k8xC	a
10	[number]	k4	10
verzí	verze	k1gFnPc2	verze
méně	málo	k6eAd2	málo
než	než	k8xS	než
100	[number]	k4	100
článků	článek	k1gInPc2	článek
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
odlišení	odlišení	k1gNnSc4	odlišení
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
jazykových	jazykový	k2eAgFnPc2d1	jazyková
verzí	verze	k1gFnPc2	verze
se	se	k3xPyFc4	se
v	v	k7c6	v
URL	URL	kA	URL
používá	používat	k5eAaImIp3nS	používat
standard	standard	k1gInSc1	standard
ISO	ISO	kA	ISO
639-1	[number]	k4	639-1
-	-	kIx~	-
např.	např.	kA	např.
en	en	k?	en
pro	pro	k7c4	pro
angličtinu	angličtina	k1gFnSc4	angličtina
<g/>
,	,	kIx,	,
cs	cs	k?	cs
pro	pro	k7c4	pro
češtinu	čeština	k1gFnSc4	čeština
atd.	atd.	kA	atd.
Nejrozsáhlejšími	rozsáhlý	k2eAgFnPc7d3	nejrozsáhlejší
verzemi	verze	k1gFnPc7	verze
jsou	být	k5eAaImIp3nP	být
anglická	anglický	k2eAgFnSc1d1	anglická
(	(	kIx(	(
<g/>
13	[number]	k4	13
%	%	kIx~	%
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
článků	článek	k1gInPc2	článek
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
švédská	švédský	k2eAgFnSc1d1	švédská
(	(	kIx(	(
<g/>
7,5	[number]	k4	7,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cebuánská	cebuánský	k2eAgFnSc1d1	cebuánský
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
5,5	[number]	k4	5,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německá	německý	k2eAgFnSc1d1	německá
(	(	kIx(	(
<g/>
4,9	[number]	k4	4,9
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nizozemská	nizozemský	k2eAgFnSc1d1	nizozemská
(	(	kIx(	(
<g/>
4,7	[number]	k4	4,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
francouzská	francouzský	k2eAgFnSc1d1	francouzská
(	(	kIx(	(
<g/>
4,4	[number]	k4	4,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
53	[number]	k4	53
%	%	kIx~	%
článků	článek	k1gInPc2	článek
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
je	být	k5eAaImIp3nS	být
napsáno	napsán	k2eAgNnSc1d1	napsáno
v	v	k7c6	v
10	[number]	k4	10
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
72	[number]	k4	72
%	%	kIx~	%
ve	v	k7c6	v
20	[number]	k4	20
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
byla	být	k5eAaImAgFnS	být
spuštěna	spuštěn	k2eAgFnSc1d1	spuštěna
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2002	[number]	k4	2002
a	a	k8xC	a
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2016	[number]	k4	2016
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
přes	přes	k7c4	přes
351	[number]	k4	351
000	[number]	k4	000
článků	článek	k1gInPc2	článek
jako	jako	k8xS	jako
26	[number]	k4	26
<g/>
.	.	kIx.	.
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
články	článek	k1gInPc1	článek
představovaly	představovat	k5eAaImAgInP	představovat
necelé	celý	k2eNgInPc4d1	necelý
1	[number]	k4	1
%	%	kIx~	%
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
článků	článek	k1gInPc2	článek
na	na	k7c4	na
Wikipedii	Wikipedie	k1gFnSc4	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
měla	mít	k5eAaImAgFnS	mít
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
přes	přes	k7c4	přes
209	[number]	k4	209
000	[number]	k4	000
článků	článek	k1gInPc2	článek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2016	[number]	k4	2016
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
jazykových	jazykový	k2eAgFnPc6d1	jazyková
verzích	verze	k1gFnPc6	verze
registrováno	registrovat	k5eAaBmNgNnS	registrovat
celkem	celkem	k6eAd1	celkem
přes	přes	k7c4	přes
61,5	[number]	k4	61,5
milionů	milion	k4xCgInPc2	milion
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
na	na	k7c4	na
anglické	anglický	k2eAgFnPc4d1	anglická
45,5	[number]	k4	45,5
%	%	kIx~	%
<g/>
,	,	kIx,	,
španělské	španělský	k2eAgFnSc2d1	španělská
6,8	[number]	k4	6,8
%	%	kIx~	%
<g/>
,	,	kIx,	,
francouzské	francouzský	k2eAgFnSc2d1	francouzská
4,1	[number]	k4	4,1
%	%	kIx~	%
a	a	k8xC	a
německé	německý	k2eAgInPc1d1	německý
3,9	[number]	k4	3,9
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
295	[number]	k4	295
300	[number]	k4	300
uživatelů	uživatel	k1gMnPc2	uživatel
je	být	k5eAaImIp3nS	být
aktivních	aktivní	k2eAgMnPc2d1	aktivní
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
články	článek	k1gInPc4	článek
editují	editovat	k5eAaImIp3nP	editovat
častěji	často	k6eAd2	často
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těch	ten	k3xDgMnPc2	ten
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
nejvíce	nejvíce	k6eAd1	nejvíce
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
(	(	kIx(	(
<g/>
44,4	[number]	k4	44,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německé	německý	k2eAgFnSc2d1	německá
(	(	kIx(	(
<g/>
6,8	[number]	k4	6,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzské	francouzský	k2eAgFnSc2d1	francouzská
(	(	kIx(	(
<g/>
5,9	[number]	k4	5,9
%	%	kIx~	%
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
španělské	španělský	k2eAgNnSc1d1	španělské
(	(	kIx(	(
<g/>
5,6	[number]	k4	5,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
bylo	být	k5eAaImAgNnS	být
evidováno	evidovat	k5eAaImNgNnS	evidovat
4	[number]	k4	4
020	[number]	k4	020
správců	správce	k1gMnPc2	správce
<g/>
.	.	kIx.	.
</s>
<s>
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
<g/>
:	:	kIx,	:
<g/>
Seznam	seznam	k1gInSc1	seznam
jazyků	jazyk	k1gInPc2	jazyk
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
Seznam	seznam	k1gInSc1	seznam
jazykových	jazykový	k2eAgFnPc2d1	jazyková
verzí	verze	k1gFnPc2	verze
s	s	k7c7	s
podrobnějšími	podrobný	k2eAgInPc7d2	podrobnější
údaji	údaj	k1gInPc7	údaj
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
k	k	k7c3	k
počtu	počet	k1gInSc3	počet
uživatelů	uživatel	k1gMnPc2	uživatel
jazyka	jazyk	k1gInSc2	jazyk
(	(	kIx(	(
<g/>
údaje	údaj	k1gInPc4	údaj
z	z	k7c2	z
dubna	duben	k1gInSc2	duben
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
tabulku	tabulka	k1gFnSc4	tabulka
s	s	k7c7	s
náskokem	náskok	k1gInSc7	náskok
vedou	vést	k5eAaImIp3nP	vést
umělé	umělý	k2eAgInPc1d1	umělý
jazyky	jazyk	k1gInPc1	jazyk
volapük	volapük	k1gInSc1	volapük
<g/>
,	,	kIx,	,
ido	ido	k1gNnSc1	ido
a	a	k8xC	a
interlingua	interlingua	k1gFnSc1	interlingua
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgFnPc2	jenž
počet	počet	k1gInSc1	počet
článků	článek	k1gInPc2	článek
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
velmi	velmi	k6eAd1	velmi
malý	malý	k2eAgInSc1d1	malý
počet	počet	k1gInSc1	počet
jejich	jejich	k3xOp3gMnPc2	jejich
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
z	z	k7c2	z
umělých	umělý	k2eAgInPc2d1	umělý
jazyků	jazyk	k1gInPc2	jazyk
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
uživatelskou	uživatelský	k2eAgFnSc7d1	Uživatelská
základnou	základna	k1gFnSc7	základna
je	být	k5eAaImIp3nS	být
esperanto	esperanto	k1gNnSc1	esperanto
(	(	kIx(	(
<g/>
se	s	k7c7	s
64	[number]	k4	64
články	článek	k1gInPc7	článek
na	na	k7c4	na
1000	[number]	k4	1000
mluvčích	mluvčí	k1gMnPc2	mluvčí
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
14	[number]	k4	14
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
národních	národní	k2eAgInPc2d1	národní
jazyků	jazyk	k1gInPc2	jazyk
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
čelních	čelní	k2eAgNnPc6d1	čelní
místech	místo	k1gNnPc6	místo
aragonština	aragonština	k1gFnSc1	aragonština
a	a	k8xC	a
kornština	kornština	k1gFnSc1	kornština
(	(	kIx(	(
<g/>
stovky	stovka	k1gFnPc1	stovka
článků	článek	k1gInPc2	článek
na	na	k7c4	na
tisíc	tisíc	k4xCgInSc4	tisíc
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
články	článek	k1gInPc1	článek
na	na	k7c4	na
tisíc	tisíc	k4xCgInSc4	tisíc
mluvčích	mluvčí	k1gFnPc2	mluvčí
pak	pak	k6eAd1	pak
například	například	k6eAd1	například
normanština	normanština	k1gFnSc1	normanština
<g/>
,	,	kIx,	,
skotská	skotský	k2eAgFnSc1d1	skotská
gaelština	gaelština	k1gFnSc1	gaelština
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc1d1	severní
laponština	laponština	k1gFnSc1	laponština
<g/>
.	.	kIx.	.
</s>
<s>
Hranici	hranice	k1gFnSc4	hranice
100	[number]	k4	100
článků	článek	k1gInPc2	článek
na	na	k7c4	na
1000	[number]	k4	1000
mluvčích	mluvčí	k1gMnPc2	mluvčí
dále	daleko	k6eAd2	daleko
překročily	překročit	k5eAaPmAgInP	překročit
norská	norský	k2eAgFnSc1d1	norská
<g/>
,	,	kIx,	,
bretonská	bretonský	k2eAgFnSc1d1	bretonská
a	a	k8xC	a
irské	irský	k2eAgFnPc1d1	irská
verze	verze	k1gFnPc1	verze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
verzi	verze	k1gFnSc6	verze
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
40	[number]	k4	40
<g/>
.	.	kIx.	.
místa	místo	k1gNnPc4	místo
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
)	)	kIx)	)
na	na	k7c4	na
tisíc	tisíc	k4xCgInSc4	tisíc
mluvčích	mluvčí	k1gMnPc2	mluvčí
připadá	připadat	k5eAaImIp3nS	připadat
přibližně	přibližně	k6eAd1	přibližně
třináct	třináct	k4xCc1	třináct
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
srovnatelná	srovnatelný	k2eAgFnSc1d1	srovnatelná
je	být	k5eAaImIp3nS	být
osetština	osetština	k1gFnSc1	osetština
<g/>
;	;	kIx,	;
u	u	k7c2	u
angličtiny	angličtina	k1gFnSc2	angličtina
cca	cca	kA	cca
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
závěru	závěr	k1gInSc2	závěr
tabulky	tabulka	k1gFnSc2	tabulka
v	v	k7c6	v
nepálském	nepálský	k2eAgNnSc6d1	nepálské
<g/>
,	,	kIx,	,
kašmírském	kašmírský	k2eAgNnSc6d1	Kašmírské
<g/>
,	,	kIx,	,
paštském	paštský	k2eAgMnSc6d1	paštský
a	a	k8xC	a
paňdžábském	paňdžábský	k2eAgInSc6d1	paňdžábský
jazyce	jazyk	k1gInSc6	jazyk
připadá	připadat	k5eAaPmIp3nS	připadat
méně	málo	k6eAd2	málo
než	než	k8xS	než
100	[number]	k4	100
článků	článek	k1gInPc2	článek
na	na	k7c4	na
milión	milión	k4xCgInSc4	milión
lidí	člověk	k1gMnPc2	člověk
užívajících	užívající	k2eAgFnPc2d1	užívající
tyto	tento	k3xDgInPc4	tento
jazyky	jazyk	k1gInPc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Metodika	metodika	k1gFnSc1	metodika
určování	určování	k1gNnSc2	určování
počtu	počet	k1gInSc2	počet
uživatelů	uživatel	k1gMnPc2	uživatel
jazyků	jazyk	k1gInPc2	jazyk
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
sporná	sporný	k2eAgFnSc1d1	sporná
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
u	u	k7c2	u
umělých	umělý	k2eAgFnPc2d1	umělá
<g/>
,	,	kIx,	,
menšinových	menšinový	k2eAgFnPc2d1	menšinová
a	a	k8xC	a
mezinárodně	mezinárodně	k6eAd1	mezinárodně
užívaných	užívaný	k2eAgInPc2d1	užívaný
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Encyklopedický	encyklopedický	k2eAgInSc1d1	encyklopedický
obsah	obsah	k1gInSc1	obsah
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
je	být	k5eAaImIp3nS	být
zveřejňován	zveřejňován	k2eAgInSc1d1	zveřejňován
pod	pod	k7c7	pod
svobodnými	svobodný	k2eAgFnPc7d1	svobodná
licencemi	licence	k1gFnPc7	licence
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
dodržení	dodržení	k1gNnSc6	dodržení
jistých	jistý	k2eAgNnPc2d1	jisté
pravidel	pravidlo	k1gNnPc2	pravidlo
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
jej	on	k3xPp3gInSc4	on
volně	volně	k6eAd1	volně
kopírovat	kopírovat	k5eAaImF	kopírovat
a	a	k8xC	a
upravovat	upravovat	k5eAaImF	upravovat
<g/>
.	.	kIx.	.
</s>
<s>
Texty	text	k1gInPc1	text
ve	v	k7c6	v
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
byly	být	k5eAaImAgFnP	být
zveřejňovány	zveřejňovat	k5eAaImNgInP	zveřejňovat
pod	pod	k7c7	pod
svobodnou	svobodný	k2eAgFnSc7d1	svobodná
licencí	licence	k1gFnSc7	licence
GNU	gnu	k1gNnSc2	gnu
FDL	FDL	kA	FDL
<g/>
,	,	kIx,	,
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2009	[number]	k4	2009
však	však	k9	však
veškerý	veškerý	k3xTgInSc4	veškerý
textový	textový	k2eAgInSc4d1	textový
obsah	obsah	k1gInSc4	obsah
přešel	přejít	k5eAaPmAgMnS	přejít
pod	pod	k7c4	pod
licenci	licence	k1gFnSc4	licence
CC-BY-SA	CC-BY-SA	k1gFnSc2	CC-BY-SA
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
dalšího	další	k2eAgInSc2d1	další
obsahu	obsah	k1gInSc2	obsah
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
obrázky	obrázek	k1gInPc4	obrázek
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
vložené	vložený	k2eAgInPc4d1	vložený
soubory	soubor	k1gInPc4	soubor
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
je	být	k5eAaImIp3nS	být
zveřejněna	zveřejněn	k2eAgFnSc1d1	zveřejněna
pod	pod	k7c7	pod
svobodnými	svobodný	k2eAgFnPc7d1	svobodná
licencemi	licence	k1gFnPc7	licence
různých	různý	k2eAgInPc2d1	různý
typů	typ	k1gInPc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
licence	licence	k1gFnSc1	licence
byla	být	k5eAaImAgFnS	být
příliš	příliš	k6eAd1	příliš
nesvobodná	svobodný	k2eNgFnSc1d1	nesvobodná
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
vylučovala	vylučovat	k5eAaImAgFnS	vylučovat
komerční	komerční	k2eAgNnSc4d1	komerční
použití	použití	k1gNnSc4	použití
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
postupně	postupně	k6eAd1	postupně
odstraňovány	odstraňován	k2eAgFnPc1d1	odstraňována
a	a	k8xC	a
pokud	pokud	k8xS	pokud
možno	možno	k6eAd1	možno
nahrazovány	nahrazovat	k5eAaImNgInP	nahrazovat
svobodnými	svobodný	k2eAgFnPc7d1	svobodná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
se	se	k3xPyFc4	se
dbá	dbát	k5eAaImIp3nS	dbát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zveřejnění	zveřejnění	k1gNnSc1	zveřejnění
textů	text	k1gInPc2	text
<g/>
,	,	kIx,	,
obrázků	obrázek	k1gInPc2	obrázek
a	a	k8xC	a
dalšího	další	k2eAgInSc2d1	další
obsahu	obsah	k1gInSc2	obsah
pod	pod	k7c7	pod
svobodnou	svobodný	k2eAgFnSc7d1	svobodná
licencí	licence	k1gFnSc7	licence
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
zákonnou	zákonný	k2eAgFnSc7d1	zákonná
ochranou	ochrana	k1gFnSc7	ochrana
autorského	autorský	k2eAgNnSc2d1	autorské
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
na	na	k7c6	na
webové	webový	k2eAgFnSc6d1	webová
síti	síť	k1gFnSc6	síť
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
na	na	k7c6	na
anonymních	anonymní	k2eAgInPc6d1	anonymní
webech	web	k1gInPc6	web
užívajících	užívající	k2eAgFnPc2d1	užívající
bezplatného	bezplatný	k2eAgInSc2d1	bezplatný
hostingu	hosting	k1gInSc2	hosting
<g/>
,	,	kIx,	,
nebývá	bývat	k5eNaImIp3nS	bývat
vždy	vždy	k6eAd1	vždy
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
<g/>
.	.	kIx.	.
</s>
<s>
Odlišně	odlišně	k6eAd1	odlišně
jsou	být	k5eAaImIp3nP	být
podmínky	podmínka	k1gFnPc1	podmínka
stanoveny	stanovit	k5eAaPmNgFnP	stanovit
pro	pro	k7c4	pro
loga	logo	k1gNnPc4	logo
projektů	projekt	k1gInPc2	projekt
nadace	nadace	k1gFnSc2	nadace
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
režimu	režim	k1gInSc2	režim
svobodných	svobodný	k2eAgFnPc2d1	svobodná
licencí	licence	k1gFnPc2	licence
vyjmuta	vyjmout	k5eAaPmNgFnS	vyjmout
<g/>
.	.	kIx.	.
</s>
<s>
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
běží	běžet	k5eAaImIp3nS	běžet
na	na	k7c6	na
svobodném	svobodný	k2eAgInSc6d1	svobodný
software	software	k1gInSc1	software
MediaWiki	MediaWike	k1gFnSc4	MediaWike
na	na	k7c6	na
clusteru	cluster	k1gInSc6	cluster
vyhrazených	vyhrazený	k2eAgInPc2d1	vyhrazený
serverů	server	k1gInPc2	server
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
dalších	další	k2eAgNnPc6d1	další
třech	tři	k4xCgNnPc6	tři
místech	místo	k1gNnPc6	místo
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
MediaWiki	MediaWiki	k6eAd1	MediaWiki
je	být	k5eAaImIp3nS	být
už	už	k6eAd1	už
třetí	třetí	k4xOgFnSc7	třetí
etapou	etapa	k1gFnSc7	etapa
v	v	k7c6	v
softwarovém	softwarový	k2eAgInSc6d1	softwarový
vývoji	vývoj	k1gInSc6	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
<g/>
,	,	kIx,	,
v	v	k7c6	v
první	první	k4xOgFnSc6	první
etapě	etapa	k1gFnSc6	etapa
<g/>
,	,	kIx,	,
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
fungovala	fungovat	k5eAaImAgFnS	fungovat
na	na	k7c4	na
UseModWiki	UseModWike	k1gFnSc4	UseModWike
od	od	k7c2	od
Clifforda	Cliffordo	k1gNnSc2	Cliffordo
Adamse	Adamse	k1gFnSc2	Adamse
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
pro	pro	k7c4	pro
odkazy	odkaz	k1gInPc4	odkaz
používat	používat	k5eAaImF	používat
zápis	zápis	k1gInSc4	zápis
slov	slovo	k1gNnPc2	slovo
bez	bez	k7c2	bez
mezer	mezera	k1gFnPc2	mezera
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
CamelCase	CamelCasa	k1gFnSc6	CamelCasa
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
používat	používat	k5eAaImF	používat
dvojité	dvojitý	k2eAgFnSc2d1	dvojitá
lomené	lomený	k2eAgFnSc2d1	lomená
závorky	závorka	k1gFnSc2	závorka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
etapy	etapa	k1gFnSc2	etapa
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
když	když	k8xS	když
začala	začít	k5eAaPmAgFnS	začít
používat	používat	k5eAaImF	používat
wiki	wike	k1gFnSc4	wike
engine	enginout	k5eAaPmIp3nS	enginout
napsaný	napsaný	k2eAgInSc1d1	napsaný
v	v	k7c6	v
PHP	PHP	kA	PHP
s	s	k7c7	s
databází	databáze	k1gFnSc7	databáze
MySQL	MySQL	k1gMnSc1	MySQL
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
speciálně	speciálně	k6eAd1	speciálně
pro	pro	k7c4	pro
Wikipedii	Wikipedie	k1gFnSc4	Wikipedie
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Magnus	Magnus	k1gMnSc1	Magnus
Manske	Mansk	k1gInSc2	Mansk
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byly	být	k5eAaImAgFnP	být
několikrát	několikrát	k6eAd1	několikrát
provedeny	proveden	k2eAgFnPc1d1	provedena
úpravy	úprava	k1gFnPc1	úprava
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
zvýšení	zvýšení	k1gNnSc1	zvýšení
výkonu	výkon	k1gInSc2	výkon
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
zvládnout	zvládnout	k5eAaPmF	zvládnout
narůstající	narůstající	k2eAgInSc4d1	narůstající
provoz	provoz	k1gInSc4	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
bylo	být	k5eAaImAgNnS	být
programové	programový	k2eAgNnSc1d1	programové
vybavení	vybavení	k1gNnSc1	vybavení
znovu	znovu	k6eAd1	znovu
přepsáno	přepsat	k5eAaPmNgNnS	přepsat
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
Lee	Lea	k1gFnSc6	Lea
Danielem	Daniel	k1gMnSc7	Daniel
Crockerem	Crocker	k1gMnSc7	Crocker
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
třetí	třetí	k4xOgFnSc1	třetí
vývojová	vývojový	k2eAgFnSc1d1	vývojová
etapa	etapa	k1gFnSc1	etapa
byla	být	k5eAaImAgFnS	být
nazvána	nazván	k2eAgFnSc1d1	nazvána
MediaWiki	MediaWik	k1gInPc7	MediaWik
a	a	k8xC	a
do	do	k7c2	do
ostrého	ostrý	k2eAgInSc2d1	ostrý
provozu	provoz	k1gInSc2	provoz
byla	být	k5eAaImAgNnP	být
nasazena	nasadit	k5eAaPmNgNnP	nasadit
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
svobodnou	svobodný	k2eAgFnSc7d1	svobodná
licencí	licence	k1gFnSc7	licence
GNU	gnu	k1gNnSc2	gnu
GPL	GPL	kA	GPL
a	a	k8xC	a
používají	používat	k5eAaImIp3nP	používat
ji	on	k3xPp3gFnSc4	on
všechny	všechen	k3xTgInPc4	všechen
projekty	projekt	k1gInPc4	projekt
nadace	nadace	k1gFnSc2	nadace
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
byl	být	k5eAaImAgInS	být
jediný	jediný	k2eAgInSc1d1	jediný
server	server	k1gInSc1	server
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
obsluhoval	obsluhovat	k5eAaImAgMnS	obsluhovat
Wikipedii	Wikipedie	k1gFnSc4	Wikipedie
<g/>
,	,	kIx,	,
rozšířen	rozšířen	k2eAgInSc1d1	rozšířen
na	na	k7c4	na
distribuovanou	distribuovaný	k2eAgFnSc4d1	distribuovaná
architekturu	architektura	k1gFnSc4	architektura
n-tier	nira	k1gFnPc2	n-tira
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2005	[number]	k4	2005
celý	celý	k2eAgInSc1d1	celý
projekt	projekt	k1gInSc1	projekt
fungoval	fungovat	k5eAaImAgInS	fungovat
na	na	k7c4	na
39	[number]	k4	39
vyhrazených	vyhrazený	k2eAgFnPc2d1	vyhrazená
serverech	server	k1gInPc6	server
umístěných	umístěný	k2eAgFnPc2d1	umístěná
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
<g/>
.	.	kIx.	.
</s>
<s>
Konfigurace	konfigurace	k1gFnSc1	konfigurace
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
jeden	jeden	k4xCgInSc4	jeden
hlavní	hlavní	k2eAgInSc4d1	hlavní
databázový	databázový	k2eAgInSc4d1	databázový
server	server	k1gInSc4	server
s	s	k7c7	s
MySQL	MySQL	k1gFnSc7	MySQL
<g/>
,	,	kIx,	,
několik	několik	k4yIc1	několik
podřízených	podřízený	k1gMnPc2	podřízený
databázových	databázový	k2eAgInPc2d1	databázový
serverů	server	k1gInPc2	server
<g/>
,	,	kIx,	,
21	[number]	k4	21
webových	webový	k2eAgInPc2d1	webový
serverů	server	k1gInPc2	server
s	s	k7c7	s
Apache	Apache	k1gNnSc7	Apache
a	a	k8xC	a
7	[number]	k4	7
serverů	server	k1gInPc2	server
se	se	k3xPyFc4	se
Squid	Squid	k1gInSc1	Squid
cache	cachat	k5eAaPmIp3nS	cachat
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
ledna	leden	k1gInSc2	leden
2006	[number]	k4	2006
se	se	k3xPyFc4	se
serverový	serverový	k2eAgInSc1d1	serverový
park	park	k1gInSc1	park
rozrostl	rozrůst	k5eAaPmAgInS	rozrůst
na	na	k7c4	na
171	[number]	k4	171
strojů	stroj	k1gInPc2	stroj
na	na	k7c6	na
čtyřech	čtyři	k4xCgNnPc6	čtyři
místech	místo	k1gNnPc6	místo
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
většině	většina	k1gFnSc6	většina
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
nainstalován	nainstalován	k2eAgInSc1d1	nainstalován
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
Linux	Linux	kA	Linux
<g/>
,	,	kIx,	,
převládá	převládat	k5eAaImIp3nS	převládat
Fedora	Fedora	k1gFnSc1	Fedora
Core	Core	k1gFnSc1	Core
<g/>
,	,	kIx,	,
po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
serveru	server	k1gInSc6	server
obsazují	obsazovat	k5eAaImIp3nP	obsazovat
FreeBSD	FreeBSD	k1gFnPc1	FreeBSD
a	a	k8xC	a
Solaris	Solaris	k1gFnPc1	Solaris
<g/>
.	.	kIx.	.
</s>
<s>
Požadavky	požadavek	k1gInPc1	požadavek
na	na	k7c4	na
stránky	stránka	k1gFnPc4	stránka
jsou	být	k5eAaImIp3nP	být
nejprve	nejprve	k6eAd1	nejprve
vyřizovány	vyřizovat	k5eAaImNgInP	vyřizovat
skupinou	skupina	k1gFnSc7	skupina
nárazových	nárazový	k2eAgFnPc2d1	nárazová
(	(	kIx(	(
<g/>
front-end	frontnda	k1gFnPc2	front-enda
<g/>
)	)	kIx)	)
cachovacích	cachovací	k2eAgInPc2d1	cachovací
serverů	server	k1gInPc2	server
se	se	k3xPyFc4	se
Squid	Squid	k1gInSc1	Squid
cache	cachat	k5eAaPmIp3nS	cachat
<g/>
.	.	kIx.	.
</s>
<s>
Požadavky	požadavek	k1gInPc1	požadavek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nemohou	moct	k5eNaImIp3nP	moct
být	být	k5eAaImF	být
vyřízeny	vyřídit	k5eAaPmNgFnP	vyřídit
z	z	k7c2	z
cache	cache	k1gNnSc4	cache
(	(	kIx(	(
<g/>
vyrovnávací	vyrovnávací	k2eAgFnSc2d1	vyrovnávací
paměti	paměť	k1gFnSc2	paměť
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
posílají	posílat	k5eAaImIp3nP	posílat
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
servery	server	k1gInPc4	server
pro	pro	k7c4	pro
rozložení	rozložení	k1gNnSc4	rozložení
zátěže	zátěž	k1gFnSc2	zátěž
(	(	kIx(	(
<g/>
load-balancing	loadalancing	k1gInSc1	load-balancing
<g/>
)	)	kIx)	)
běžící	běžící	k2eAgMnSc1d1	běžící
na	na	k7c4	na
software	software	k1gInSc4	software
Perlbal	Perlbal	k1gInSc4	Perlbal
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
požadavky	požadavek	k1gInPc1	požadavek
přepošlou	přepošlat	k5eAaPmIp3nP	přepošlat
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
webových	webový	k2eAgInPc2d1	webový
serverů	server	k1gInPc2	server
s	s	k7c7	s
Apache	Apache	k1gFnSc7	Apache
<g/>
.	.	kIx.	.
</s>
<s>
Webservery	Webserver	k1gInPc1	Webserver
sestavují	sestavovat	k5eAaImIp3nP	sestavovat
stránky	stránka	k1gFnPc4	stránka
z	z	k7c2	z
databáze	databáze	k1gFnSc2	databáze
a	a	k8xC	a
dodávají	dodávat	k5eAaImIp3nP	dodávat
je	být	k5eAaImIp3nS	být
všem	všecek	k3xTgNnPc3	všecek
Wikipediím	Wikipedium	k1gNnPc3	Wikipedium
<g/>
.	.	kIx.	.
</s>
<s>
Dalšího	další	k2eAgNnSc2d1	další
zrychlení	zrychlení	k1gNnSc3	zrychlení
sestavování	sestavování	k1gNnSc1	sestavování
stránek	stránka	k1gFnPc2	stránka
pro	pro	k7c4	pro
anonymní	anonymní	k2eAgMnPc4d1	anonymní
uživatele	uživatel	k1gMnPc4	uživatel
je	být	k5eAaImIp3nS	být
dosahováno	dosahovat	k5eAaImNgNnS	dosahovat
dočasným	dočasný	k2eAgNnSc7d1	dočasné
ukládáním	ukládání	k1gNnSc7	ukládání
v	v	k7c6	v
souborovém	souborový	k2eAgInSc6d1	souborový
systému	systém	k1gInSc6	systém
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nevyprší	vypršet	k5eNaPmIp3nS	vypršet
jejich	jejich	k3xOp3gFnSc1	jejich
časová	časový	k2eAgFnSc1d1	časová
platnost	platnost	k1gFnSc1	platnost
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
běžných	běžný	k2eAgInPc2d1	běžný
přístupů	přístup	k1gInPc2	přístup
ke	k	k7c3	k
stránce	stránka	k1gFnSc3	stránka
vynechat	vynechat	k5eAaPmF	vynechat
opětovné	opětovný	k2eAgNnSc4d1	opětovné
sestavování	sestavování	k1gNnSc4	sestavování
<g/>
.	.	kIx.	.
</s>
<s>
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
začala	začít	k5eAaPmAgFnS	začít
budovat	budovat	k5eAaImF	budovat
globální	globální	k2eAgFnSc4d1	globální
síť	síť	k1gFnSc4	síť
cachovacích	cachovací	k2eAgFnPc2d1	cachovací
Squid	Squida	k1gFnPc2	Squida
serverů	server	k1gInPc2	server
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2004	[number]	k4	2004
umístěním	umístění	k1gNnSc7	umístění
tří	tři	k4xCgInPc2	tři
strojů	stroj	k1gInPc2	stroj
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
přibyl	přibýt	k5eAaPmAgInS	přibýt
nizozemský	nizozemský	k2eAgInSc1d1	nizozemský
cluster	cluster	k1gInSc1	cluster
s	s	k7c7	s
11	[number]	k4	11
servery	server	k1gInPc4	server
a	a	k8xC	a
23	[number]	k4	23
serverů	server	k1gInPc2	server
v	v	k7c6	v
Koreji	Korea	k1gFnSc6	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
tato	tento	k3xDgNnPc4	tento
opatření	opatření	k1gNnPc4	opatření
se	se	k3xPyFc4	se
nedaří	dařit	k5eNaImIp3nS	dařit
stále	stále	k6eAd1	stále
vzrůstající	vzrůstající	k2eAgInSc4d1	vzrůstající
provoz	provoz	k1gInSc4	provoz
zvládnout	zvládnout	k5eAaPmF	zvládnout
a	a	k8xC	a
rychlost	rychlost	k1gFnSc4	rychlost
zobrazování	zobrazování	k1gNnSc2	zobrazování
stránek	stránka	k1gFnPc2	stránka
poměrně	poměrně	k6eAd1	poměrně
kolísá	kolísat	k5eAaImIp3nS	kolísat
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Stav	stav	k1gInSc1	stav
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
uživatelé	uživatel	k1gMnPc1	uživatel
hlásí	hlásit	k5eAaImIp3nP	hlásit
na	na	k7c4	na
stránku	stránka	k1gFnSc4	stránka
"	"	kIx"	"
<g/>
Status	status	k1gInSc1	status
page	pag	k1gInSc2	pag
<g/>
"	"	kIx"	"
na	na	k7c6	na
serveru	server	k1gInSc6	server
OpenFacts	OpenFacts	k1gInSc1	OpenFacts
<g/>
.	.	kIx.	.
</s>
<s>
Podrobné	podrobný	k2eAgFnPc1d1	podrobná
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
serverech	server	k1gInPc6	server
a	a	k8xC	a
software	software	k1gInSc4	software
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
anglických	anglický	k2eAgFnPc6d1	anglická
stránkách	stránka	k1gFnPc6	stránka
"	"	kIx"	"
<g/>
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
servers	serversa	k1gFnPc2	serversa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
vyvinuty	vyvinout	k5eAaPmNgFnP	vyvinout
různé	různý	k2eAgFnPc1d1	různá
softwarové	softwarový	k2eAgFnPc1d1	softwarová
pomůcky	pomůcka	k1gFnPc1	pomůcka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
usnadňují	usnadňovat	k5eAaImIp3nP	usnadňovat
prohlížení	prohlížení	k1gNnSc4	prohlížení
a	a	k8xC	a
editaci	editace	k1gFnSc4	editace
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
pro	pro	k7c4	pro
snadnější	snadný	k2eAgNnSc4d2	snazší
užívání	užívání	k1gNnSc4	užívání
a	a	k8xC	a
editaci	editace	k1gFnSc4	editace
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
v	v	k7c6	v
prohlížeči	prohlížeč	k1gInSc6	prohlížeč
Mozilla	Mozilla	k1gFnSc1	Mozilla
Firefox	Firefox	k1gInSc1	Firefox
existuje	existovat	k5eAaImIp3nS	existovat
rozšíření	rozšíření	k1gNnSc4	rozšíření
Wikipedia	Wikipedium	k1gNnSc2	Wikipedium
Extension	Extension	k1gInSc1	Extension
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgInS	představit
online	onlinout	k5eAaPmIp3nS	onlinout
nástroj	nástroj	k1gInSc1	nástroj
WikiBhasha	WikiBhash	k1gMnSc4	WikiBhash
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
snadnější	snadný	k2eAgFnSc4d2	snazší
tvorbu	tvorba	k1gFnSc4	tvorba
článků	článek	k1gInPc2	článek
pomocí	pomocí	k7c2	pomocí
překladu	překlad	k1gInSc2	překlad
z	z	k7c2	z
anglické	anglický	k2eAgFnSc2d1	anglická
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
do	do	k7c2	do
některých	některý	k3yIgFnPc2	některý
jiných	jiný	k2eAgFnPc2d1	jiná
jazykových	jazykový	k2eAgFnPc2d1	jazyková
verzí	verze	k1gFnPc2	verze
<g/>
.	.	kIx.	.
</s>
<s>
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
stává	stávat	k5eAaImIp3nS	stávat
objektem	objekt	k1gInSc7	objekt
zájmu	zájem	k1gInSc2	zájem
médií	médium	k1gNnPc2	médium
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
Jimbo	Jimba	k1gFnSc5	Jimba
Wales	Wales	k1gInSc1	Wales
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
100	[number]	k4	100
nejvlivnějších	vlivný	k2eAgMnPc2d3	nejvlivnější
lidí	člověk	k1gMnPc2	člověk
světa	svět	k1gInSc2	svět
časopisu	časopis	k1gInSc2	časopis
Time	Tim	k1gFnSc2	Tim
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
se	se	k3xPyFc4	se
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
stala	stát	k5eAaPmAgFnS	stát
objektem	objekt	k1gInSc7	objekt
řady	řada	k1gFnPc1	řada
studií	studio	k1gNnPc2	studio
<g/>
,	,	kIx,	,
výzkumů	výzkum	k1gInPc2	výzkum
a	a	k8xC	a
srovnání	srovnání	k1gNnSc4	srovnání
kvality	kvalita	k1gFnSc2	kvalita
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
dosud	dosud	k6eAd1	dosud
zpravidla	zpravidla	k6eAd1	zpravidla
zaměřují	zaměřovat	k5eAaImIp3nP	zaměřovat
na	na	k7c4	na
největší	veliký	k2eAgFnPc4d3	veliký
jazykové	jazykový	k2eAgFnPc4d1	jazyková
verze	verze	k1gFnPc4	verze
<g/>
,	,	kIx,	,
anglickou	anglický	k2eAgFnSc4d1	anglická
a	a	k8xC	a
německou	německý	k2eAgFnSc4d1	německá
<g/>
.	.	kIx.	.
</s>
<s>
Značnou	značný	k2eAgFnSc4d1	značná
pozornost	pozornost	k1gFnSc4	pozornost
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
článek	článek	k1gInSc1	článek
časopisu	časopis	k1gInSc2	časopis
Nature	Natur	k1gMnSc5	Natur
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
byly	být	k5eAaImAgInP	být
porovnávány	porovnávat	k5eAaImNgInP	porovnávat
anglická	anglický	k2eAgFnSc1d1	anglická
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
a	a	k8xC	a
Encyclopæ	Encyclopæ	k1gFnSc1	Encyclopæ
Britannica	Britannic	k1gInSc2	Britannic
se	s	k7c7	s
závěrem	závěr	k1gInSc7	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
srovnatelný	srovnatelný	k2eAgInSc4d1	srovnatelný
počet	počet	k1gInSc4	počet
chyb	chyba	k1gFnPc2	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Britannica	Britannica	k6eAd1	Britannica
metodiku	metodika	k1gFnSc4	metodika
a	a	k8xC	a
výsledky	výsledek	k1gInPc4	výsledek
srovnání	srovnání	k1gNnSc2	srovnání
ostře	ostro	k6eAd1	ostro
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
zdrojem	zdroj	k1gInSc7	zdroj
referátů	referát	k1gInPc2	referát
pro	pro	k7c4	pro
studenty	student	k1gMnPc4	student
všech	všecek	k3xTgInPc2	všecek
typů	typ	k1gInPc2	typ
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
Cambridge	Cambridge	k1gFnSc2	Cambridge
ji	on	k3xPp3gFnSc4	on
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
využívá	využívat	k5eAaImIp3nS	využívat
celkem	celkem	k6eAd1	celkem
82	[number]	k4	82
procent	procento	k1gNnPc2	procento
vysokoškoláků	vysokoškolák	k1gMnPc2	vysokoškolák
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
sledovaným	sledovaný	k2eAgNnPc3d1	sledované
tématům	téma	k1gNnPc3	téma
patří	patřit	k5eAaImIp3nS	patřit
spory	spor	k1gInPc7	spor
o	o	k7c4	o
podobu	podoba	k1gFnSc4	podoba
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgFnPc6	jenž
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
<g/>
)	)	kIx)	)
dva	dva	k4xCgMnPc1	dva
uživatelé	uživatel	k1gMnPc1	uživatel
neustále	neustále	k6eAd1	neustále
mění	měnit	k5eAaImIp3nP	měnit
editace	editace	k1gFnPc4	editace
toho	ten	k3xDgInSc2	ten
druhého	druhý	k4xOgMnSc4	druhý
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
původní	původní	k2eAgFnSc2d1	původní
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
těchto	tento	k3xDgInPc2	tento
tzv.	tzv.	kA	tzv.
revertovacích	revertovací	k2eAgFnPc2d1	revertovací
válek	válka	k1gFnPc2	válka
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
edit	edit	k1gInSc1	edit
wars	wars	k1gInSc1	wars
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
malého	malý	k2eAgNnSc2d1	malé
procenta	procento	k1gNnSc2	procento
kontroverzních	kontroverzní	k2eAgInPc2d1	kontroverzní
článků	článek	k1gInPc2	článek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
k	k	k7c3	k
vysoce	vysoce	k6eAd1	vysoce
kontroverzním	kontroverzní	k2eAgNnPc3d1	kontroverzní
tématům	téma	k1gNnPc3	téma
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
George	Georg	k1gFnSc2	Georg
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
<g/>
,	,	kIx,	,
anarchismus	anarchismus	k1gInSc1	anarchismus
a	a	k8xC	a
Mohamed	Mohamed	k1gMnSc1	Mohamed
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zabránění	zabránění	k1gNnSc3	zabránění
revertovacích	revertovací	k2eAgFnPc2d1	revertovací
válek	válka	k1gFnPc2	válka
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
Wikipedii	Wikipedie	k1gFnSc4	Wikipedie
přijata	přijat	k2eAgFnSc1d1	přijata
řada	řada	k1gFnSc1	řada
opatření	opatření	k1gNnPc2	opatření
<g/>
,	,	kIx,	,
např.	např.	kA	např.
pravidlo	pravidlo	k1gNnSc1	pravidlo
tří	tři	k4xCgMnPc2	tři
revertů	revert	k1gMnPc2	revert
nebo	nebo	k8xC	nebo
úvodní	úvodní	k2eAgNnPc4d1	úvodní
upozornění	upozornění	k1gNnPc4	upozornění
vztahující	vztahující	k2eAgNnPc4d1	vztahující
se	se	k3xPyFc4	se
ke	k	k7c3	k
kontroverzním	kontroverzní	k2eAgInPc3d1	kontroverzní
článkům	článek	k1gInPc3	článek
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
metody	metoda	k1gFnPc1	metoda
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
revertovací	revertovací	k2eAgFnSc2d1	revertovací
války	válka	k1gFnSc2	válka
na	na	k7c4	na
Wikipedii	Wikipedie	k1gFnSc4	Wikipedie
automaticky	automaticky	k6eAd1	automaticky
detekovat	detekovat	k5eAaImF	detekovat
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
už	už	k6eAd1	už
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
několik	několik	k4yIc1	několik
skandálů	skandál	k1gInPc2	skandál
<g/>
,	,	kIx,	,
vyvolaných	vyvolaný	k2eAgInPc2d1	vyvolaný
nepravdivým	pravdivý	k2eNgInSc7d1	nepravdivý
obsahem	obsah	k1gInSc7	obsah
článků	článek	k1gInPc2	článek
-	-	kIx~	-
například	například	k6eAd1	například
v	v	k7c6	v
polské	polský	k2eAgFnSc6d1	polská
verzi	verze	k1gFnSc6	verze
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
dlouho	dlouho	k6eAd1	dlouho
existoval	existovat	k5eAaImAgInS	existovat
článek	článek	k1gInSc1	článek
o	o	k7c4	o
Henryku	Henryka	k1gFnSc4	Henryka
Batutovi	Batutův	k2eAgMnPc1d1	Batutův
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
zcela	zcela	k6eAd1	zcela
fiktivní	fiktivní	k2eAgFnSc1d1	fiktivní
osoba	osoba	k1gFnSc1	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Trvale	trvale	k6eAd1	trvale
kriticky	kriticky	k6eAd1	kriticky
se	se	k3xPyFc4	se
k	k	k7c3	k
Wikipedii	Wikipedie	k1gFnSc3	Wikipedie
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
zástupci	zástupce	k1gMnPc7	zástupce
Encyclopedie	Encyclopedie	k1gFnSc2	Encyclopedie
Britanniky	Britannik	k1gInPc1	Britannik
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Nejčastěji	často	k6eAd3	často
uváděným	uváděný	k2eAgInSc7d1	uváděný
nedostatkem	nedostatek	k1gInSc7	nedostatek
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádná	žádný	k3yNgFnSc1	žádný
vydavatelská	vydavatelský	k2eAgFnSc1d1	vydavatelská
autorita	autorita	k1gFnSc1	autorita
neručí	ručit	k5eNaImIp3nS	ručit
za	za	k7c4	za
správnost	správnost	k1gFnSc4	správnost
obsahu	obsah	k1gInSc2	obsah
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
kritika	kritika	k1gFnSc1	kritika
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
fungování	fungování	k1gNnSc4	fungování
komunity	komunita	k1gFnSc2	komunita
přispěvatelů	přispěvatel	k1gMnPc2	přispěvatel
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
na	na	k7c4	na
konkrétní	konkrétní	k2eAgFnPc4d1	konkrétní
čelné	čelný	k2eAgFnPc4d1	čelná
osoby	osoba	k1gFnPc4	osoba
projektu	projekt	k1gInSc2	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
pochází	pocházet	k5eAaImIp3nS	pocházet
zevnitř	zevnitř	k6eAd1	zevnitř
komunity	komunita	k1gFnPc4	komunita
existující	existující	k2eAgFnPc4d1	existující
kolem	kolem	k7c2	kolem
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
<g/>
,	,	kIx,	,
např.	např.	kA	např.
od	od	k7c2	od
přispěvatelů	přispěvatel	k1gMnPc2	přispěvatel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
s	s	k7c7	s
komunitou	komunita	k1gFnSc7	komunita
rozešli	rozejít	k5eAaPmAgMnP	rozejít
ve	v	k7c6	v
zlém	zlé	k1gNnSc6	zlé
<g/>
,	,	kIx,	,
účast	účast	k1gFnSc1	účast
na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
jim	on	k3xPp3gMnPc3	on
byla	být	k5eAaImAgFnS	být
zakázána	zakázat	k5eAaPmNgFnS	zakázat
apod.	apod.	kA	apod.
Například	například	k6eAd1	například
stránka	stránka	k1gFnSc1	stránka
wikitruth	wikitrutha	k1gFnPc2	wikitrutha
<g/>
.	.	kIx.	.
<g/>
info	info	k6eAd1	info
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
široké	široký	k2eAgNnSc4d1	široké
spektrum	spektrum	k1gNnSc4	spektrum
negativních	negativní	k2eAgNnPc2d1	negativní
hodnocení	hodnocení	k1gNnPc2	hodnocení
anglické	anglický	k2eAgFnSc2d1	anglická
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
až	až	k9	až
po	po	k7c4	po
zesměšňování	zesměšňování	k1gNnPc4	zesměšňování
a	a	k8xC	a
urážky	urážka	k1gFnPc4	urážka
konkrétních	konkrétní	k2eAgFnPc2d1	konkrétní
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Dočekal	dočekat	k5eAaPmAgMnS	dočekat
o	o	k7c4	o
Wikipedii	Wikipedie	k1gFnSc4	Wikipedie
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Přínosem	přínos	k1gInSc7	přínos
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
je	být	k5eAaImIp3nS	být
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
volně	volně	k6eAd1	volně
dostupných	dostupný	k2eAgFnPc2d1	dostupná
informací	informace	k1gFnPc2	informace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
navíc	navíc	k6eAd1	navíc
poměrně	poměrně	k6eAd1	poměrně
dobře	dobře	k6eAd1	dobře
vzájemně	vzájemně	k6eAd1	vzájemně
propojeny	propojit	k5eAaPmNgFnP	propojit
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
obsah	obsah	k1gInSc1	obsah
brán	brán	k2eAgInSc1d1	brán
s	s	k7c7	s
nutnou	nutný	k2eAgFnSc7d1	nutná
rezervou	rezerva	k1gFnSc7	rezerva
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
vytvářen	vytvářet	k5eAaImNgMnS	vytvářet
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
tak	tak	k9	tak
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
užitečný	užitečný	k2eAgInSc1d1	užitečný
při	při	k7c6	při
rešerších	rešerše	k1gFnPc6	rešerše
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
planetka	planetka	k1gFnSc1	planetka
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
kritizují	kritizovat	k5eAaImIp3nP	kritizovat
pro	pro	k7c4	pro
její	její	k3xOp3gFnSc4	její
systematickou	systematický	k2eAgFnSc4d1	systematická
liberální	liberální	k2eAgFnSc4d1	liberální
(	(	kIx(	(
<g/>
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
smyslu	smysl	k1gInSc6	smysl
slova	slovo	k1gNnSc2	slovo
liberalismus	liberalismus	k1gInSc1	liberalismus
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
liberálně-levicovou	liberálněevicový	k2eAgFnSc4d1	liberálně-levicový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
levicovou	levicový	k2eAgFnSc4d1	levicová
a	a	k8xC	a
protikonzervativní	protikonzervativní	k2eAgFnSc4d1	protikonzervativní
zaujatost	zaujatost	k1gFnSc4	zaujatost
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
zakladatel	zakladatel	k1gMnSc1	zakladatel
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
Jimmy	Jimma	k1gFnSc2	Jimma
Wales	Wales	k1gInSc1	Wales
uznává	uznávat	k5eAaImIp3nS	uznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
obsah	obsah	k1gInSc4	obsah
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
liberálnější	liberální	k2eAgInPc1d2	liberálnější
nežli	nežli	k8xS	nežli
názory	názor	k1gInPc1	názor
průměru	průměr	k1gInSc2	průměr
americké	americký	k2eAgFnSc2d1	americká
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
to	ten	k3xDgNnSc1	ten
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
anglickojazyčná	anglickojazyčný	k2eAgFnSc1d1	anglickojazyčná
komunita	komunita	k1gFnSc1	komunita
je	být	k5eAaImIp3nS	být
liberálnější	liberální	k2eAgFnSc1d2	liberálnější
než	než	k8xS	než
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Lawrence	Lawrence	k1gFnSc1	Lawrence
Solomon	Solomona	k1gFnPc2	Solomona
v	v	k7c6	v
National	National	k1gFnSc6	National
Review	Review	k1gFnSc2	Review
dochází	docházet	k5eAaImIp3nS	docházet
ze	z	k7c2	z
zjištění	zjištění	k1gNnSc2	zjištění
<g/>
,	,	kIx,	,
že	že	k8xS	že
články	článek	k1gInPc1	článek
o	o	k7c6	o
tématech	téma	k1gNnPc6	téma
jako	jako	k8xS	jako
globální	globální	k2eAgNnSc1d1	globální
oteplování	oteplování	k1gNnSc1	oteplování
<g/>
,	,	kIx,	,
inteligentní	inteligentní	k2eAgInSc1d1	inteligentní
design	design	k1gInSc1	design
či	či	k8xC	či
Roe	Roe	k1gFnSc1	Roe
vs	vs	k?	vs
<g/>
.	.	kIx.	.
</s>
<s>
Wade	Wade	k?	Wade
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
systematickou	systematický	k2eAgFnSc4d1	systematická
zaujatost	zaujatost	k1gFnSc4	zaujatost
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
liberálů	liberál	k1gMnPc2	liberál
<g/>
.	.	kIx.	.
</s>
<s>
Zaujatý	zaujatý	k2eAgInSc4d1	zaujatý
a	a	k8xC	a
hyperkritický	hyperkritický	k2eAgInSc4d1	hyperkritický
pohled	pohled	k1gInSc4	pohled
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
na	na	k7c4	na
inteligentní	inteligentní	k2eAgInSc4d1	inteligentní
design	design	k1gInSc4	design
uvádí	uvádět	k5eAaImIp3nS	uvádět
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
článku	článek	k1gInSc6	článek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
i	i	k8xC	i
The	The	k1gMnSc1	The
Christian	Christian	k1gMnSc1	Christian
Post	posta	k1gFnPc2	posta
<g/>
.	.	kIx.	.
</s>
<s>
Nicholas	Nicholas	k1gInSc1	Nicholas
Stix	Stix	k1gInSc1	Stix
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
American	Americana	k1gFnPc2	Americana
Renaissance	Renaissanec	k1gInSc2	Renaissanec
dokládá	dokládat	k5eAaImIp3nS	dokládat
systematickou	systematický	k2eAgFnSc4d1	systematická
levicovou	levicový	k2eAgFnSc4d1	levicová
zaujatost	zaujatost	k1gFnSc4	zaujatost
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
v	v	k7c6	v
rasových	rasový	k2eAgFnPc6d1	rasová
otázkách	otázka	k1gFnPc6	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
se	se	k3xPyFc4	se
staví	stavit	k5eAaPmIp3nS	stavit
pozitivně	pozitivně	k6eAd1	pozitivně
k	k	k7c3	k
rasové	rasový	k2eAgFnSc3d1	rasová
desegregaci	desegregace	k1gFnSc3	desegregace
<g/>
,	,	kIx,	,
afirmativní	afirmativní	k2eAgFnSc3d1	afirmativní
akci	akce	k1gFnSc3	akce
a	a	k8xC	a
mezirasovému	mezirasový	k2eAgNnSc3d1	mezirasový
manželství	manželství	k1gNnSc3	manželství
<g/>
.	.	kIx.	.
</s>
<s>
Informuje	informovat	k5eAaBmIp3nS	informovat
o	o	k7c6	o
bělošském	bělošský	k2eAgNnSc6d1	bělošské
násilí	násilí	k1gNnSc6	násilí
vůči	vůči	k7c3	vůči
jiným	jiný	k2eAgFnPc3d1	jiná
rasovým	rasový	k2eAgFnPc3d1	rasová
skupinám	skupina	k1gFnPc3	skupina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
naopak	naopak	k6eAd1	naopak
zamlčuje	zamlčovat	k5eAaImIp3nS	zamlčovat
události	událost	k1gFnSc2	událost
opačného	opačný	k2eAgInSc2d1	opačný
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Pozitivně	pozitivně	k6eAd1	pozitivně
se	se	k3xPyFc4	se
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
o	o	k7c6	o
antirasistických	antirasistický	k2eAgFnPc6d1	antirasistická
i	i	k8xC	i
radikálních	radikální	k2eAgFnPc6d1	radikální
černošských	černošský	k2eAgMnPc6d1	černošský
aktivistech	aktivista	k1gMnPc6	aktivista
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
naopak	naopak	k6eAd1	naopak
pomlouvá	pomlouvat	k5eAaImIp3nS	pomlouvat
a	a	k8xC	a
negativně	negativně	k6eAd1	negativně
nálepkuje	nálepkovat	k5eAaImIp3nS	nálepkovat
členy	člen	k1gInPc4	člen
probělošských	probělošský	k2eAgFnPc2d1	probělošský
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
přijde	přijít	k5eAaPmIp3nS	přijít
na	na	k7c4	na
"	"	kIx"	"
<g/>
věrohodného	věrohodný	k2eAgInSc2d1	věrohodný
zdroje	zdroj	k1gInSc2	zdroj
<g/>
"	"	kIx"	"
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
rasy	rasa	k1gFnSc2	rasa
a	a	k8xC	a
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
jimi	on	k3xPp3gFnPc7	on
být	být	k5eAaImF	být
jen	jen	k9	jen
cokoliv	cokoliv	k3yInSc1	cokoliv
"	"	kIx"	"
<g/>
věrohodně	věrohodně	k6eAd1	věrohodně
levicového	levicový	k2eAgInSc2d1	levicový
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zdroji	zdroj	k1gInPc7	zdroj
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
liberální	liberální	k2eAgFnPc1d1	liberální
vysokoškolské	vysokoškolský	k2eAgFnPc1d1	vysokoškolská
noviny	novina	k1gFnPc1	novina
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
fundraisingové	fundraisingový	k2eAgInPc1d1	fundraisingový
dopisy	dopis	k1gInPc1	dopis
organizací	organizace	k1gFnPc2	organizace
jako	jako	k8xS	jako
Southern	Southerna	k1gFnPc2	Southerna
Poverty	Povert	k1gInPc1	Povert
Law	Law	k1gFnSc2	Law
Center	centrum	k1gNnPc2	centrum
či	či	k8xC	či
Anti-Defamation	Anti-Defamation	k1gInSc4	Anti-Defamation
League	Leagu	k1gFnSc2	Leagu
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
nepohodlné	pohodlný	k2eNgFnPc1d1	nepohodlná
stránky	stránka	k1gFnPc1	stránka
jsou	být	k5eAaImIp3nP	být
odstraňovány	odstraňován	k2eAgFnPc1d1	odstraňována
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2010	[number]	k4	2010
srovnal	srovnat	k5eAaPmAgMnS	srovnat
Rowan	Rowan	k1gMnSc1	Rowan
Scarborough	Scarborough	k1gMnSc1	Scarborough
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Human	Human	k1gInSc1	Human
Events	Events	k1gInSc1	Events
přístup	přístup	k1gInSc4	přístup
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
k	k	k7c3	k
biografickým	biografický	k2eAgInPc3d1	biografický
článkům	článek	k1gInPc3	článek
o	o	k7c6	o
kandidátech	kandidát	k1gMnPc6	kandidát
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
konaných	konaný	k2eAgFnPc2d1	konaná
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
a	a	k8xC	a
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
systematicky	systematicky	k6eAd1	systematicky
výrazně	výrazně	k6eAd1	výrazně
negativněji	negativně	k6eAd2	negativně
informuje	informovat	k5eAaBmIp3nS	informovat
o	o	k7c6	o
konzervativcích	konzervativec	k1gMnPc6	konzervativec
a	a	k8xC	a
kandidátech	kandidát	k1gMnPc6	kandidát
podporovaných	podporovaný	k2eAgInPc2d1	podporovaný
hnutím	hnutí	k1gNnPc3	hnutí
Tea	Tea	k1gFnSc1	Tea
Party	party	k1gFnSc3	party
nežli	nežli	k8xS	nežli
o	o	k7c6	o
liberálech	liberál	k1gMnPc6	liberál
a	a	k8xC	a
středově	středově	k6eAd1	středově
orientovaných	orientovaný	k2eAgMnPc6d1	orientovaný
kandidátech	kandidát	k1gMnPc6	kandidát
<g/>
.	.	kIx.	.
</s>
<s>
Tim	Tim	k?	Tim
Anderson	Anderson	k1gMnSc1	Anderson
<g/>
,	,	kIx,	,
vyučující	vyučující	k2eAgFnSc4d1	vyučující
na	na	k7c4	na
University	universita	k1gFnPc4	universita
of	of	k?	of
Sydney	Sydney	k1gNnSc1	Sydney
<g/>
,	,	kIx,	,
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
podporuje	podporovat	k5eAaImIp3nS	podporovat
politiku	politika	k1gFnSc4	politika
americké	americký	k2eAgFnSc2d1	americká
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
média	médium	k1gNnPc1	médium
vlastněná	vlastněný	k2eAgFnSc1d1	vlastněná
velkými	velký	k2eAgFnPc7d1	velká
korporacemi	korporace	k1gFnPc7	korporace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
článku	článek	k1gInSc6	článek
o	o	k7c4	o
Hugo	Hugo	k1gMnSc1	Hugo
Chávezovi	Chávez	k1gMnSc6	Chávez
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgInP	používat
vůči	vůči	k7c3	vůči
němu	on	k3xPp3gMnSc3	on
kritické	kritický	k2eAgInPc1d1	kritický
zdroje	zdroj	k1gInPc1	zdroj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zdroje	zdroj	k1gInPc1	zdroj
jako	jako	k8xS	jako
Venezuela	Venezuela	k1gFnSc1	Venezuela
Analysis	Analysis	k1gFnSc2	Analysis
a	a	k8xC	a
Z	z	k7c2	z
Magazine	Magazin	k1gMnSc5	Magazin
označují	označovat	k5eAaImIp3nP	označovat
správci	správce	k1gMnPc7	správce
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
za	za	k7c4	za
"	"	kIx"	"
<g/>
nepoužitelné	použitelný	k2eNgNnSc4d1	nepoužitelné
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
provozuje	provozovat	k5eAaImIp3nS	provozovat
nadace	nadace	k1gFnSc1	nadace
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
několik	několik	k4yIc1	několik
dalších	další	k2eAgInPc2d1	další
otevřených	otevřený	k2eAgInPc2d1	otevřený
projektů	projekt	k1gInPc2	projekt
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
plní	plnit	k5eAaImIp3nP	plnit
jiné	jiný	k2eAgInPc1d1	jiný
než	než	k8xS	než
encyklopedické	encyklopedický	k2eAgInPc1d1	encyklopedický
cíle	cíl	k1gInPc1	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejdůležitější	důležitý	k2eAgNnSc4d3	nejdůležitější
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Wikislovník	Wikislovník	k1gInSc1	Wikislovník
-	-	kIx~	-
výkladový	výkladový	k2eAgInSc1d1	výkladový
slovník	slovník	k1gInSc1	slovník
a	a	k8xC	a
tezaurus	tezaurus	k1gInSc1	tezaurus
<g/>
,	,	kIx,	,
Wikizdroje	Wikizdroj	k1gInPc1	Wikizdroj
-	-	kIx~	-
úložiště	úložiště	k1gNnSc1	úložiště
volných	volný	k2eAgInPc2d1	volný
dokumentů	dokument	k1gInPc2	dokument
<g/>
,	,	kIx,	,
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
-	-	kIx~	-
sdílená	sdílený	k2eAgFnSc1d1	sdílená
sbírka	sbírka	k1gFnSc1	sbírka
multimediálních	multimediální	k2eAgInPc2d1	multimediální
souborů	soubor	k1gInPc2	soubor
<g/>
,	,	kIx,	,
Wikizprávy	Wikizpráv	k1gInPc1	Wikizpráv
-	-	kIx~	-
otevřené	otevřený	k2eAgNnSc1d1	otevřené
zpravodajství	zpravodajství	k1gNnSc1	zpravodajství
<g/>
,	,	kIx,	,
Wikicitáty	Wikicitát	k1gInPc1	Wikicitát
-	-	kIx~	-
sbírka	sbírka	k1gFnSc1	sbírka
citátů	citát	k1gInPc2	citát
<g/>
,	,	kIx,	,
Wikiknihy	Wikiknih	k1gInPc1	Wikiknih
-	-	kIx~	-
volně	volně	k6eAd1	volně
dostupné	dostupný	k2eAgFnPc1d1	dostupná
učebnice	učebnice	k1gFnPc1	učebnice
a	a	k8xC	a
příručky	příručka	k1gFnPc1	příručka
<g/>
,	,	kIx,	,
Wikidruhy	Wikidruh	k1gInPc1	Wikidruh
-	-	kIx~	-
adresář	adresář	k1gInSc1	adresář
biologických	biologický	k2eAgInPc2d1	biologický
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
-	-	kIx~	-
repozitář	repozitář	k1gInSc1	repozitář
svobodných	svobodný	k2eAgFnPc2d1	svobodná
multimediálních	multimediální	k2eAgFnPc2d1	multimediální
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
Wikidata	Wikidata	k1gFnSc1	Wikidata
-	-	kIx~	-
svobodná	svobodný	k2eAgFnSc1d1	svobodná
databáze	databáze	k1gFnSc1	databáze
znalostí	znalost	k1gFnPc2	znalost
a	a	k8xC	a
Wikicesty	Wikicest	k1gInPc1	Wikicest
-	-	kIx~	-
cestovní	cestovní	k2eAgMnSc1d1	cestovní
průvodce	průvodce	k1gMnSc1	průvodce
<g/>
.	.	kIx.	.
</s>
