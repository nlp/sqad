<s>
Braniborská	braniborský	k2eAgFnSc1d1	Braniborská
brána	brána	k1gFnSc1	brána
je	být	k5eAaImIp3nS	být
26	[number]	k4	26
metrů	metr	k1gInPc2	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
65,5	[number]	k4	65,5
metrů	metr	k1gInPc2	metr
široká	široký	k2eAgFnSc1d1	široká
a	a	k8xC	a
11	[number]	k4	11
metrů	metr	k1gInPc2	metr
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
(	(	kIx(	(
<g/>
hluboká	hluboký	k2eAgFnSc1d1	hluboká
<g/>
)	)	kIx)	)
stavba	stavba	k1gFnSc1	stavba
z	z	k7c2	z
pískovce	pískovec	k1gInSc2	pískovec
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
raného	raný	k2eAgInSc2d1	raný
klasicismu	klasicismus	k1gInSc2	klasicismus
<g/>
.	.	kIx.	.
</s>
