<s>
George	Georg	k1gMnSc2	Georg
Walker	Walker	k1gMnSc1	Walker
Bush	Bush	k1gMnSc1	Bush
(	(	kIx(	(
<g/>
*	*	kIx~	*
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
43	[number]	k4	43
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Republikánské	republikánský	k2eAgFnSc2d1	republikánská
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgFnSc2d1	pocházející
z	z	k7c2	z
prominentní	prominentní	k2eAgFnSc2d1	prominentní
rodiny	rodina	k1gFnSc2	rodina
Bushů	Bush	k1gMnPc2	Bush
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
dalšími	další	k2eAgInPc7d1	další
vlivnými	vlivný	k2eAgInPc7d1	vlivný
členy	člen	k1gInPc7	člen
této	tento	k3xDgFnSc2	tento
rodiny	rodina	k1gFnSc2	rodina
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gMnSc4	jeho
děd	děd	k1gMnSc1	děd
<g/>
,	,	kIx,	,
senátor	senátor	k1gMnSc1	senátor
Prescott	Prescott	k1gMnSc1	Prescott
Bush	Bush	k1gMnSc1	Bush
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
otec	otec	k1gMnSc1	otec
George	Georg	k1gMnSc2	Georg
H.	H.	kA	H.
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
<g/>
,	,	kIx,	,
41	[number]	k4	41
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Jeb	Jeb	k1gMnSc1	Jeb
Bush	Bush	k1gMnSc1	Bush
<g/>
,	,	kIx,	,
exguvernér	exguvernér	k1gMnSc1	exguvernér
státu	stát	k1gInSc2	stát
Florida	Florida	k1gFnSc1	Florida
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
prezidentskou	prezidentský	k2eAgFnSc7d1	prezidentská
kandidaturou	kandidatura	k1gFnSc7	kandidatura
podnikal	podnikat	k5eAaImAgInS	podnikat
George	George	k1gInSc1	George
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
v	v	k7c6	v
ropném	ropný	k2eAgInSc6d1	ropný
průmyslu	průmysl	k1gInSc6	průmysl
a	a	k8xC	a
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
profesionální	profesionální	k2eAgNnSc4d1	profesionální
baseballové	baseballový	k2eAgNnSc4d1	baseballové
mužstvo	mužstvo	k1gNnSc4	mužstvo
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
46	[number]	k4	46
<g/>
.	.	kIx.	.
guvernérem	guvernér	k1gMnSc7	guvernér
státu	stát	k1gInSc2	stát
Texas	Texas	kA	Texas
a	a	k8xC	a
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
nominaci	nominace	k1gFnSc4	nominace
Republikánské	republikánský	k2eAgFnSc2d1	republikánská
strany	strana	k1gFnSc2	strana
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentem	prezident	k1gMnSc7	prezident
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
kontroverzních	kontroverzní	k2eAgFnPc6d1	kontroverzní
volbách	volba	k1gFnPc6	volba
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jeho	jeho	k3xOp3gMnSc7	jeho
hlavním	hlavní	k2eAgMnSc7d1	hlavní
soupeřem	soupeř	k1gMnSc7	soupeř
byl	být	k5eAaImAgMnS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
viceprezident	viceprezident	k1gMnSc1	viceprezident
Al	ala	k1gFnPc2	ala
Gore	Gore	k1gInSc4	Gore
z	z	k7c2	z
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Uspěl	uspět	k5eAaPmAgMnS	uspět
i	i	k9	i
při	při	k7c6	při
kandidatuře	kandidatura	k1gFnSc6	kandidatura
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
porazil	porazit	k5eAaPmAgMnS	porazit
Massachusettského	massachusettský	k2eAgMnSc4d1	massachusettský
senátora	senátor	k1gMnSc4	senátor
Johna	John	k1gMnSc4	John
Kerryho	Kerry	k1gMnSc4	Kerry
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2000	[number]	k4	2000
a	a	k8xC	a
2004	[number]	k4	2004
jej	on	k3xPp3gMnSc4	on
časopis	časopis	k1gInSc1	časopis
Time	Tim	k1gFnSc2	Tim
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
Osobností	osobnost	k1gFnSc7	osobnost
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
George	George	k1gFnSc7	George
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
ve	v	k7c6	v
městě	město	k1gNnSc6	město
New	New	k1gFnPc2	New
Haven	Havno	k1gNnPc2	Havno
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Connecticut	Connecticut	k1gInSc1	Connecticut
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
v	v	k7c6	v
Midlandu	Midland	k1gInSc6	Midland
a	a	k8xC	a
Houstonu	Houston	k1gInSc6	Houston
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
léta	léto	k1gNnPc4	léto
a	a	k8xC	a
většinu	většina	k1gFnSc4	většina
prázdnin	prázdniny	k1gFnPc2	prázdniny
trávila	trávit	k5eAaImAgFnS	trávit
rodina	rodina	k1gFnSc1	rodina
v	v	k7c6	v
Bush	Bush	k1gMnSc1	Bush
Compound	Compound	k1gInSc1	Compound
v	v	k7c6	v
Maine	Main	k1gInSc5	Main
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
otec	otec	k1gMnSc1	otec
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
G.	G.	kA	G.
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
Phillips	Phillips	k1gInSc4	Phillips
Academy	Academa	k1gFnSc2	Academa
(	(	kIx(	(
<g/>
září	září	k1gNnSc2	září
1961	[number]	k4	1961
-	-	kIx~	-
červen	červen	k1gInSc1	červen
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
univerzitu	univerzita	k1gFnSc4	univerzita
Yale	Yal	k1gFnSc2	Yal
(	(	kIx(	(
<g/>
září	září	k1gNnSc2	září
1964	[number]	k4	1964
-	-	kIx~	-
květen	květen	k1gInSc4	květen
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Yale	Yale	k1gFnSc6	Yale
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgMnS	přidat
k	k	k7c3	k
studentské	studentský	k2eAgFnSc3d1	studentská
skupině	skupina	k1gFnSc3	skupina
Delta	delta	k1gFnSc1	delta
Kappa	kappa	k1gNnSc7	kappa
Epsilon	epsilon	k1gNnSc1	epsilon
(	(	kIx(	(
<g/>
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
prezidentem	prezident	k1gMnSc7	prezident
byl	být	k5eAaImAgMnS	být
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
1965	[number]	k4	1965
do	do	k7c2	do
svojí	svůj	k3xOyFgFnSc2	svůj
promoce	promoce	k1gFnSc2	promoce
<g/>
)	)	kIx)	)
a	a	k8xC	a
tajné	tajný	k2eAgFnSc2d1	tajná
společnosti	společnost	k1gFnSc2	společnost
Skull	Skull	k1gMnSc1	Skull
and	and	k?	and
Bones	Bones	k1gMnSc1	Bones
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
studentem	student	k1gMnSc7	student
kategorie	kategorie	k1gFnSc2	kategorie
"	"	kIx"	"
<g/>
C	C	kA	C
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
skóre	skóre	k1gNnSc4	skóre
77	[number]	k4	77
procent	procento	k1gNnPc2	procento
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
"	"	kIx"	"
<g/>
A	a	k9	a
<g/>
"	"	kIx"	"
hodnocení	hodnocení	k1gNnSc1	hodnocení
a	a	k8xC	a
s	s	k7c7	s
jedním	jeden	k4xCgNnSc7	jeden
hodnocením	hodnocení	k1gNnSc7	hodnocení
"	"	kIx"	"
<g/>
D	D	kA	D
<g/>
"	"	kIx"	"
z	z	k7c2	z
astronomie	astronomie	k1gFnSc2	astronomie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
průměrným	průměrný	k2eAgNnSc7d1	průměrné
ročním	roční	k2eAgNnSc7d1	roční
hodnocením	hodnocení	k1gNnSc7	hodnocení
2,35	[number]	k4	2,35
ze	z	k7c2	z
4,00	[number]	k4	4,00
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
roztleskávač	roztleskávač	k1gMnSc1	roztleskávač
(	(	kIx(	(
<g/>
cheerleader	cheerleader	k1gMnSc1	cheerleader
<g/>
)	)	kIx)	)
u	u	k7c2	u
univerzitního	univerzitní	k2eAgInSc2d1	univerzitní
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
titul	titul	k1gInSc1	titul
"	"	kIx"	"
<g/>
Bakalář	bakalář	k1gMnSc1	bakalář
filosofických	filosofický	k2eAgFnPc2d1	filosofická
věd	věda	k1gFnPc2	věda
<g/>
"	"	kIx"	"
z	z	k7c2	z
historie	historie	k1gFnSc2	historie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
promoci	promoce	k1gFnSc6	promoce
z	z	k7c2	z
Yale	Yal	k1gInSc2	Yal
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
službu	služba	k1gFnSc4	služba
u	u	k7c2	u
texaské	texaský	k2eAgFnSc2d1	texaská
Národní	národní	k2eAgFnSc2d1	národní
letecké	letecký	k2eAgFnSc2d1	letecká
gardy	garda	k1gFnSc2	garda
jako	jako	k8xS	jako
pilot	pilot	k1gInSc1	pilot
vojenských	vojenský	k2eAgNnPc2d1	vojenské
letadel	letadlo	k1gNnPc2	letadlo
F-	F-	k1gFnSc1	F-
<g/>
102	[number]	k4	102
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
nezúčastnil	zúčastnit	k5eNaPmAgMnS	zúčastnit
války	válka	k1gFnSc2	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jednou	jednou	k6eAd1	jednou
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
prvního	první	k4xOgMnSc4	první
důstojníka	důstojník	k1gMnSc4	důstojník
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
deníku	deník	k1gInSc2	deník
Boston	Boston	k1gInSc1	Boston
Globe	globus	k1gInSc5	globus
v	v	k7c6	v
záznamech	záznam	k1gInPc6	záznam
Národní	národní	k2eAgFnSc2d1	národní
letecké	letecký	k2eAgFnSc2d1	letecká
gardy	garda	k1gFnSc2	garda
je	být	k5eAaImIp3nS	být
rok	rok	k1gInSc4	rok
a	a	k8xC	a
půl	půl	k6eAd1	půl
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
"	"	kIx"	"
<g/>
bílé	bílý	k2eAgNnSc4d1	bílé
místo	místo	k1gNnSc4	místo
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1972	[number]	k4	1972
a	a	k8xC	a
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
není	být	k5eNaImIp3nS	být
o	o	k7c6	o
Georgi	Georg	k1gFnSc6	Georg
W.	W.	kA	W.
Bushovi	Bush	k1gMnSc3	Bush
žádná	žádný	k3yNgFnSc1	žádný
zmínka	zmínka	k1gFnSc1	zmínka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1973	[number]	k4	1973
dostal	dostat	k5eAaPmAgMnS	dostat
povolení	povolení	k1gNnSc4	povolení
ukončit	ukončit	k5eAaPmF	ukončit
svoji	svůj	k3xOyFgFnSc4	svůj
šestiletou	šestiletý	k2eAgFnSc4d1	šestiletá
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
službu	služba	k1gFnSc4	služba
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
před	před	k7c7	před
termínem	termín	k1gInSc7	termín
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
začít	začít	k5eAaPmF	začít
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
Harvardskou	harvardský	k2eAgFnSc4d1	Harvardská
obchodní	obchodní	k2eAgFnSc4d1	obchodní
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Dvouleté	dvouletý	k2eAgNnSc4d1	dvouleté
studium	studium	k1gNnSc4	studium
na	na	k7c6	na
Harvardské	harvardský	k2eAgFnSc6d1	Harvardská
obchodní	obchodní	k2eAgFnSc6d1	obchodní
škole	škola	k1gFnSc6	škola
zakončil	zakončit	k5eAaPmAgMnS	zakončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
diplomem	diplom	k1gInSc7	diplom
MBA	MBA	kA	MBA
(	(	kIx(	(
<g/>
Masters	Masters	k1gInSc1	Masters
of	of	k?	of
Business	business	k1gInSc1	business
Administration	Administration	k1gInSc1	Administration
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
USA	USA	kA	USA
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
tento	tento	k3xDgInSc4	tento
diplom	diplom	k1gInSc4	diplom
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1976	[number]	k4	1976
zastavila	zastavit	k5eAaPmAgFnS	zastavit
Bushe	Bush	k1gMnSc4	Bush
policie	policie	k1gFnSc2	policie
blízko	blízko	k7c2	blízko
letoviska	letovisko	k1gNnSc2	letovisko
jeho	jeho	k3xOp3gFnSc2	jeho
rodiny	rodina	k1gFnSc2	rodina
v	v	k7c6	v
Maine	Main	k1gInSc5	Main
a	a	k8xC	a
zatkla	zatknout	k5eAaPmAgFnS	zatknout
jej	on	k3xPp3gInSc4	on
za	za	k7c2	za
řízení	řízení	k1gNnSc2	řízení
auta	auto	k1gNnSc2	auto
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Bush	Bush	k1gMnSc1	Bush
přiznal	přiznat	k5eAaPmAgMnS	přiznat
svoji	svůj	k3xOyFgFnSc4	svůj
vinu	vina	k1gFnSc4	vina
<g/>
,	,	kIx,	,
zaplatil	zaplatit	k5eAaPmAgMnS	zaplatit
pokutu	pokuta	k1gFnSc4	pokuta
150	[number]	k4	150
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
po	po	k7c4	po
30	[number]	k4	30
dnů	den	k1gInPc2	den
nesměl	smět	k5eNaImAgMnS	smět
řídit	řídit	k5eAaImF	řídit
<g/>
.	.	kIx.	.
</s>
<s>
Zprávy	zpráva	k1gFnPc1	zpráva
týkající	týkající	k2eAgFnPc1d1	týkající
se	se	k3xPyFc4	se
tohoto	tento	k3xDgNnSc2	tento
zatčení	zatčení	k1gNnSc2	zatčení
objevil	objevit	k5eAaPmAgInS	objevit
tisk	tisk	k1gInSc1	tisk
5	[number]	k4	5
dní	den	k1gInPc2	den
před	před	k7c7	před
prezidentskými	prezidentský	k2eAgFnPc7d1	prezidentská
volbami	volba	k1gFnPc7	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
byl	být	k5eAaImAgMnS	být
Bush	Bush	k1gMnSc1	Bush
zatčen	zatknout	k5eAaPmNgMnS	zatknout
třikrát	třikrát	k6eAd1	třikrát
-	-	kIx~	-
krom	krom	k7c2	krom
jízdy	jízda	k1gFnSc2	jízda
v	v	k7c6	v
opilosti	opilost	k1gFnSc6	opilost
ještě	ještě	k9	ještě
za	za	k7c4	za
výtržnictví	výtržnictví	k1gNnSc4	výtržnictví
a	a	k8xC	a
krádež	krádež	k1gFnSc4	krádež
<g/>
.	.	kIx.	.
</s>
<s>
Bush	Bush	k1gMnSc1	Bush
označil	označit	k5eAaPmAgMnS	označit
dny	den	k1gInPc4	den
předcházející	předcházející	k2eAgFnPc1d1	předcházející
jeho	jeho	k3xOp3gFnSc3	jeho
náboženské	náboženský	k2eAgFnSc3d1	náboženská
konverzi	konverze	k1gFnSc3	konverze
jako	jako	k8xS	jako
svoje	své	k1gNnSc1	své
"	"	kIx"	"
<g/>
nomádské	nomádský	k2eAgNnSc1d1	nomádské
<g/>
"	"	kIx"	"
období	období	k1gNnSc1	období
a	a	k8xC	a
"	"	kIx"	"
<g/>
nezodpovědné	zodpovědný	k2eNgNnSc1d1	nezodpovědné
mládí	mládí	k1gNnSc1	mládí
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
i	i	k9	i
přiznal	přiznat	k5eAaPmAgMnS	přiznat
že	že	k8xS	že
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
pil	pít	k5eAaImAgMnS	pít
na	na	k7c4	na
zdejší	zdejší	k2eAgInSc4d1	zdejší
poměry	poměr	k1gInPc4	poměr
až	až	k9	až
"	"	kIx"	"
<g/>
příliš	příliš	k6eAd1	příliš
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Přestal	přestat	k5eAaPmAgMnS	přestat
jsem	být	k5eAaImIp1nS	být
pít	pít	k5eAaImF	pít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jsem	být	k5eAaImIp1nS	být
nevypil	vypít	k5eNaPmAgMnS	vypít
ani	ani	k8xC	ani
kapku	kapka	k1gFnSc4	kapka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
proměnu	proměna	k1gFnSc4	proměna
zčásti	zčásti	k6eAd1	zčásti
připsal	připsat	k5eAaPmAgMnS	připsat
svému	svůj	k3xOyFgMnSc3	svůj
setkání	setkání	k1gNnSc4	setkání
s	s	k7c7	s
reverendem	reverend	k1gMnSc7	reverend
Billy	Bill	k1gMnPc4	Bill
Grahamem	Graham	k1gMnSc7	Graham
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
George	George	k1gFnSc1	George
Walker	Walkra	k1gFnPc2	Walkra
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc1d3	nejstarší
z	z	k7c2	z
dětí	dítě	k1gFnPc2	dítě
George	Georg	k1gMnSc2	Georg
H.	H.	kA	H.
W.	W.	kA	W.
Bushe	Bush	k1gMnSc2	Bush
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
ženy	žena	k1gFnSc2	žena
Barbary	Barbara	k1gFnSc2	Barbara
Piercové-Bushové	Piercové-Bushová	k1gFnSc2	Piercové-Bushová
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
sourozenci	sourozenec	k1gMnPc1	sourozenec
jsou	být	k5eAaImIp3nP	být
Dorothy	Doroth	k1gMnPc4	Doroth
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
(	(	kIx(	(
<g/>
Jeb	Jeb	k1gMnSc1	Jeb
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Marvin	Marvina	k1gFnPc2	Marvina
Pierce	Pierec	k1gInSc2	Pierec
<g/>
,	,	kIx,	,
Neil	Neil	k1gMnSc1	Neil
a	a	k8xC	a
Robin	robin	k2eAgMnSc1d1	robin
Bushovi	Bush	k1gMnSc3	Bush
(	(	kIx(	(
<g/>
Robin	Robina	k1gFnPc2	Robina
zemřela	zemřít	k5eAaPmAgFnS	zemřít
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
na	na	k7c6	na
leukémii	leukémie	k1gFnSc6	leukémie
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeb	Jeb	k?	Jeb
Bush	Bush	k1gMnSc1	Bush
byl	být	k5eAaImAgMnS	být
guvernérem	guvernér	k1gMnSc7	guvernér
státu	stát	k1gInSc2	stát
Florida	Florida	k1gFnSc1	Florida
<g/>
.	.	kIx.	.
</s>
<s>
Bratr	bratr	k1gMnSc1	bratr
Marvin	Marvina	k1gFnPc2	Marvina
podniká	podnikat	k5eAaImIp3nS	podnikat
v	v	k7c6	v
investiční	investiční	k2eAgFnSc6d1	investiční
firmě	firma	k1gFnSc6	firma
a	a	k8xC	a
na	na	k7c6	na
burze	burza	k1gFnSc6	burza
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
ředitelem	ředitel	k1gMnSc7	ředitel
společnosti	společnost	k1gFnSc2	společnost
mající	mající	k2eAgMnPc1d1	mající
na	na	k7c4	na
starost	starost	k1gFnSc4	starost
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
Světového	světový	k2eAgNnSc2d1	světové
obchodního	obchodní	k2eAgNnSc2d1	obchodní
centra	centrum	k1gNnSc2	centrum
v	v	k7c6	v
době	doba	k1gFnSc6	doba
útoků	útok	k1gInPc2	útok
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Dědeček	dědeček	k1gMnSc1	dědeček
Prescott	Prescott	k1gMnSc1	Prescott
byl	být	k5eAaImAgMnS	být
koncem	koncem	k7c2	koncem
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
ředitelem	ředitel	k1gMnSc7	ředitel
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
bank	banka	k1gFnPc2	banka
Union	union	k1gInSc1	union
Banking	Banking	k1gInSc1	Banking
Corporation	Corporation	k1gInSc4	Corporation
<g/>
,	,	kIx,	,
vlastněnou	vlastněný	k2eAgFnSc4d1	vlastněná
průmyslníky	průmyslník	k1gMnPc7	průmyslník
(	(	kIx(	(
<g/>
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
<g/>
)	)	kIx)	)
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
profiltrovali	profiltrovat	k5eAaPmAgMnP	profiltrovat
přes	přes	k7c4	přes
jinou	jiný	k2eAgFnSc4d1	jiná
holandskou	holandský	k2eAgFnSc4d1	holandská
banku	banka	k1gFnSc4	banka
asi	asi	k9	asi
3	[number]	k4	3
miliony	milion	k4xCgInPc1	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
nakonec	nakonec	k6eAd1	nakonec
banku	banka	k1gFnSc4	banka
zrušila	zrušit	k5eAaPmAgFnS	zrušit
a	a	k8xC	a
její	její	k3xOp3gInSc4	její
majetek	majetek	k1gInSc4	majetek
zabavila	zabavit	k5eAaPmAgFnS	zabavit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
Prescottu	Prescott	k1gMnSc3	Prescott
Bushovi	Bush	k1gMnSc3	Bush
vyneslo	vynést	k5eAaPmAgNnS	vynést
1,5	[number]	k4	1,5
milionu	milion	k4xCgInSc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
se	se	k3xPyFc4	se
George	George	k1gNnSc2	George
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Laurou	Laura	k1gFnSc7	Laura
Welchovou	Welchová	k1gFnSc7	Welchová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodila	narodit	k5eAaPmAgNnP	narodit
dvojčata	dvojče	k1gNnPc1	dvojče
Barbara	barbar	k1gMnSc2	barbar
a	a	k8xC	a
Jenna	Jenn	k1gMnSc2	Jenn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
opustil	opustit	k5eAaPmAgMnS	opustit
Bush	Bush	k1gMnSc1	Bush
Episkopální	episkopální	k2eAgFnSc4d1	episkopální
církev	církev	k1gFnSc4	církev
a	a	k8xC	a
připojil	připojit	k5eAaPmAgMnS	připojit
se	se	k3xPyFc4	se
k	k	k7c3	k
církvi	církev	k1gFnSc3	církev
své	svůj	k3xOyFgFnSc2	svůj
manželky	manželka	k1gFnSc2	manželka
<g/>
,	,	kIx,	,
Sjednocené	sjednocený	k2eAgFnSc3d1	sjednocená
metodistické	metodistický	k2eAgFnSc3d1	metodistická
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
se	se	k3xPyFc4	se
Bush	Bush	k1gMnSc1	Bush
ucházel	ucházet	k5eAaImAgMnS	ucházet
o	o	k7c4	o
post	post	k1gInSc4	post
ve	v	k7c6	v
Sněmovně	sněmovna	k1gFnSc6	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prohrál	prohrát	k5eAaPmAgMnS	prohrát
v	v	k7c6	v
souboji	souboj	k1gInSc6	souboj
s	s	k7c7	s
demokratickým	demokratický	k2eAgMnSc7d1	demokratický
senátorem	senátor	k1gMnSc7	senátor
Kentem	Kent	k1gMnSc7	Kent
Hancem	Hance	k1gMnSc7	Hance
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
tedy	tedy	k9	tedy
svoji	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
ropného	ropný	k2eAgInSc2d1	ropný
průmyslu	průmysl	k1gInSc2	průmysl
založením	založení	k1gNnSc7	založení
podniku	podnik	k1gInSc2	podnik
Arbusto	Arbusta	k1gMnSc5	Arbusta
Energy	Energ	k1gMnPc7	Energ
(	(	kIx(	(
<g/>
arbusto	arbusto	k6eAd1	arbusto
je	být	k5eAaImIp3nS	být
španělsky	španělsky	k6eAd1	španělsky
keř	keř	k1gInSc1	keř
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
bush	bush	k1gInSc1	bush
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zaměřeným	zaměřený	k2eAgNnSc7d1	zaměřené
na	na	k7c4	na
hledání	hledání	k1gNnSc4	hledání
a	a	k8xC	a
těžbu	těžba	k1gFnSc4	těžba
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
podnik	podnik	k1gInSc1	podnik
musel	muset	k5eAaImAgInS	muset
čelit	čelit	k5eAaImF	čelit
energetické	energetický	k2eAgFnSc3d1	energetická
krizi	krize	k1gFnSc3	krize
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
Bush	Bush	k1gMnSc1	Bush
podnik	podnik	k1gInSc4	podnik
přejmenoval	přejmenovat	k5eAaPmAgMnS	přejmenovat
na	na	k7c4	na
Bush	Bush	k1gMnSc1	Bush
Exploration	Exploration	k1gInSc1	Exploration
<g/>
,	,	kIx,	,
ho	on	k3xPp3gInSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
prodal	prodat	k5eAaPmAgInS	prodat
konkurenční	konkurenční	k2eAgInSc1d1	konkurenční
firmě	firma	k1gFnSc6	firma
Spektrum	spektrum	k1gNnSc1	spektrum
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
podmínek	podmínka	k1gFnPc2	podmínka
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Bush	Bush	k1gMnSc1	Bush
stane	stanout	k5eAaPmIp3nS	stanout
generálním	generální	k2eAgMnSc7d1	generální
ředitelem	ředitel	k1gMnSc7	ředitel
(	(	kIx(	(
<g/>
CEO	CEO	kA	CEO
<g/>
)	)	kIx)	)
této	tento	k3xDgFnSc2	tento
firmy	firma	k1gFnSc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
podobným	podobný	k2eAgInSc7d1	podobný
scénářem	scénář	k1gInSc7	scénář
se	se	k3xPyFc4	se
firma	firma	k1gFnSc1	firma
sloučila	sloučit	k5eAaPmAgFnS	sloučit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
s	s	k7c7	s
Harken	Harken	k1gInSc1	Harken
Energy	Energ	k1gMnPc4	Energ
Corporation	Corporation	k1gInSc4	Corporation
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
obviňují	obviňovat	k5eAaImIp3nP	obviňovat
jeho	jeho	k3xOp3gMnSc4	jeho
otce	otec	k1gMnSc4	otec
(	(	kIx(	(
<g/>
toho	ten	k3xDgNnSc2	ten
času	čas	k1gInSc2	čas
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
nátlak	nátlak	k1gInSc4	nátlak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
některé	některý	k3yIgNnSc1	některý
údajně	údajně	k6eAd1	údajně
podezřelé	podezřelý	k2eAgInPc1d1	podezřelý
prvky	prvek	k1gInPc1	prvek
tohoto	tento	k3xDgInSc2	tento
prodeje	prodej	k1gInSc2	prodej
nestaly	stát	k5eNaPmAgFnP	stát
předmětem	předmět	k1gInSc7	předmět
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
kandidatury	kandidatura	k1gFnSc2	kandidatura
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
zapojit	zapojit	k5eAaPmF	zapojit
do	do	k7c2	do
volební	volební	k2eAgFnSc2d1	volební
kampaně	kampaň	k1gFnSc2	kampaň
<g/>
.	.	kIx.	.
</s>
<s>
Pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
navazovat	navazovat	k5eAaImF	navazovat
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
organizacemi	organizace	k1gFnPc7	organizace
a	a	k8xC	a
hnutími	hnutí	k1gNnPc7	hnutí
náboženské	náboženský	k2eAgFnSc2d1	náboženská
pravice	pravice	k1gFnSc2	pravice
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
křesťanské	křesťanský	k2eAgFnPc1d1	křesťanská
koalice	koalice	k1gFnPc1	koalice
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yQgMnPc3	který
jeho	on	k3xPp3gInSc4	on
otec	otec	k1gMnSc1	otec
nevěděl	vědět	k5eNaImAgMnS	vědět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
přistupovat	přistupovat	k5eAaImF	přistupovat
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
spojil	spojit	k5eAaPmAgMnS	spojit
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
přáteli	přítel	k1gMnPc7	přítel
a	a	k8xC	a
společně	společně	k6eAd1	společně
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
koupili	koupit	k5eAaPmAgMnP	koupit
86	[number]	k4	86
<g/>
%	%	kIx~	%
podíl	podíl	k1gInSc1	podíl
v	v	k7c6	v
baseballovém	baseballový	k2eAgNnSc6d1	baseballové
mužstvu	mužstvo	k1gNnSc6	mužstvo
Texas	Texas	k1gInSc1	Texas
Rangers	Rangersa	k1gFnPc2	Rangersa
za	za	k7c4	za
75	[number]	k4	75
miliónů	milión	k4xCgInPc2	milión
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Bush	Bush	k1gMnSc1	Bush
získal	získat	k5eAaPmAgMnS	získat
2	[number]	k4	2
<g/>
%	%	kIx~	%
podíl	podíl	k1gInSc1	podíl
<g/>
,	,	kIx,	,
když	když	k8xS	když
investoval	investovat	k5eAaBmAgMnS	investovat
606	[number]	k4	606
302	[number]	k4	302
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterých	který	k3yRgFnPc2	který
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
pocházelo	pocházet	k5eAaImAgNnS	pocházet
z	z	k7c2	z
bankovní	bankovní	k2eAgFnSc2d1	bankovní
půjčky	půjčka	k1gFnSc2	půjčka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
půjčku	půjčka	k1gFnSc4	půjčka
splatil	splatit	k5eAaPmAgInS	splatit
prodejem	prodej	k1gInSc7	prodej
akcií	akcie	k1gFnPc2	akcie
Harken	Harkna	k1gFnPc2	Harkna
Energy	Energ	k1gMnPc4	Energ
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
výkonný	výkonný	k2eAgMnSc1d1	výkonný
generální	generální	k2eAgMnSc1d1	generální
partner	partner	k1gMnSc1	partner
Texas	Texas	kA	Texas
Rangers	Rangers	k1gInSc1	Rangers
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
aktivní	aktivní	k2eAgMnSc1d1	aktivní
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
mediálních	mediální	k2eAgInPc2d1	mediální
vztahů	vztah	k1gInPc2	vztah
družstva	družstvo	k1gNnSc2	družstvo
a	a	k8xC	a
v	v	k7c6	v
zajišťování	zajišťování	k1gNnSc6	zajišťování
výstavby	výstavba	k1gFnSc2	výstavba
nového	nový	k2eAgInSc2d1	nový
stadionu	stadion	k1gInSc2	stadion
The	The	k1gFnSc2	The
Ballpark	Ballpark	k1gInSc1	Ballpark
Arlington	Arlington	k1gInSc1	Arlington
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Bushova	Bushův	k2eAgFnSc1d1	Bushova
prominentní	prominentní	k2eAgFnSc1d1	prominentní
úloha	úloha	k1gFnSc1	úloha
u	u	k7c2	u
Texas	Texas	kA	Texas
Rangers	Rangers	k1gInSc4	Rangers
mu	on	k3xPp3gMnSc3	on
vynesla	vynést	k5eAaPmAgFnS	vynést
popularitu	popularita	k1gFnSc4	popularita
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
Texasu	Texas	k1gInSc6	Texas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
mužstvo	mužstvo	k1gNnSc4	mužstvo
prodal	prodat	k5eAaPmAgMnS	prodat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
kandidovat	kandidovat	k5eAaImF	kandidovat
na	na	k7c4	na
post	post	k1gInSc4	post
guvernéra	guvernér	k1gMnSc2	guvernér
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tento	tento	k3xDgInSc4	tento
post	post	k1gInSc4	post
se	se	k3xPyFc4	se
střetl	střetnout	k5eAaPmAgMnS	střetnout
s	s	k7c7	s
populární	populární	k2eAgFnSc7d1	populární
demokratickou	demokratický	k2eAgFnSc7d1	demokratická
kandidátkou	kandidátka	k1gFnSc7	kandidátka
Ann	Ann	k1gFnSc2	Ann
Richards	Richardsa	k1gFnPc2	Richardsa
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
porazil	porazit	k5eAaPmAgInS	porazit
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
53	[number]	k4	53
%	%	kIx~	%
ku	k	k7c3	k
46	[number]	k4	46
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
znovu	znovu	k6eAd1	znovu
<g/>
.	.	kIx.	.
</s>
<s>
Zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
dokázal	dokázat	k5eAaPmAgMnS	dokázat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
stranu	strana	k1gFnSc4	strana
získat	získat	k5eAaPmF	získat
konzervativní	konzervativní	k2eAgMnPc4d1	konzervativní
křesťany	křesťan	k1gMnPc4	křesťan
a	a	k8xC	a
evangelisty	evangelista	k1gMnPc4	evangelista
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
tradičně	tradičně	k6eAd1	tradičně
tvoří	tvořit	k5eAaImIp3nP	tvořit
nezanedbatelnou	zanedbatelný	k2eNgFnSc4d1	nezanedbatelná
část	část	k1gFnSc4	část
obyvatel	obyvatel	k1gMnPc2	obyvatel
tohoto	tento	k3xDgInSc2	tento
jižanského	jižanský	k2eAgInSc2d1	jižanský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
guvernér	guvernér	k1gMnSc1	guvernér
Bush	Bush	k1gMnSc1	Bush
prosadil	prosadit	k5eAaPmAgMnS	prosadit
několik	několik	k4yIc4	několik
významných	významný	k2eAgFnPc2d1	významná
legislativních	legislativní	k2eAgFnPc2d1	legislativní
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
trestního	trestní	k2eAgNnSc2d1	trestní
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
financování	financování	k1gNnSc2	financování
školství	školství	k1gNnSc2	školství
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
pozornost	pozornost	k1gFnSc4	pozornost
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
jeho	jeho	k3xOp3gNnSc4	jeho
nekompromisní	kompromisní	k2eNgNnSc1d1	nekompromisní
využívání	využívání	k1gNnSc1	využívání
trestu	trest	k1gInSc2	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
:	:	kIx,	:
podepsal	podepsat	k5eAaPmAgMnS	podepsat
příkaz	příkaz	k1gInSc4	příkaz
pro	pro	k7c4	pro
vykonání	vykonání	k1gNnSc4	vykonání
rozsudku	rozsudek	k1gInSc2	rozsudek
pro	pro	k7c4	pro
152	[number]	k4	152
vězňů	vězeň	k1gMnPc2	vězeň
odsouzených	odsouzený	k2eAgMnPc2d1	odsouzený
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
ve	v	k7c6	v
funkčních	funkční	k2eAgNnPc6d1	funkční
obdobích	období	k1gNnPc6	období
jiných	jiný	k2eAgMnPc2d1	jiný
guvernérů	guvernér	k1gMnPc2	guvernér
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
vysoký	vysoký	k2eAgInSc1d1	vysoký
počet	počet	k1gInSc1	počet
poprav	poprava	k1gFnPc2	poprava
nebyl	být	k5eNaImAgInS	být
pro	pro	k7c4	pro
Texas	Texas	k1gInSc4	Texas
neobvyklý	obvyklý	k2eNgInSc4d1	neobvyklý
<g/>
.	.	kIx.	.
</s>
<s>
Bush	Bush	k1gMnSc1	Bush
uskutečnil	uskutečnit	k5eAaPmAgMnS	uskutečnit
významné	významný	k2eAgFnPc4d1	významná
změny	změna	k1gFnPc4	změna
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
o	o	k7c4	o
jejich	jejich	k3xOp3gInSc4	jejich
přínos	přínos	k1gInSc4	přínos
se	se	k3xPyFc4	se
vedou	vést	k5eAaImIp3nP	vést
urputné	urputný	k2eAgInPc1d1	urputný
spory	spor	k1gInPc1	spor
<g/>
.	.	kIx.	.
</s>
<s>
Každopádně	každopádně	k6eAd1	každopádně
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
ochotě	ochota	k1gFnSc3	ochota
k	k	k7c3	k
rázným	rázný	k2eAgFnPc3d1	rázná
reformám	reforma	k1gFnPc3	reforma
a	a	k8xC	a
rodinnému	rodinný	k2eAgNnSc3d1	rodinné
zázemí	zázemí	k1gNnSc3	zázemí
vyšvihl	vyšvihnout	k5eAaPmAgInS	vyšvihnout
mezi	mezi	k7c4	mezi
nejpopulárnější	populární	k2eAgMnPc4d3	nejpopulárnější
republikánské	republikánský	k2eAgMnPc4d1	republikánský
politiky	politik	k1gMnPc4	politik
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgMnPc2d1	hlavní
kandidátů	kandidát	k1gMnPc2	kandidát
strany	strana	k1gFnSc2	strana
na	na	k7c4	na
potenciálního	potenciální	k2eAgMnSc4d1	potenciální
nástupce	nástupce	k1gMnSc4	nástupce
Billa	Bill	k1gMnSc4	Bill
Clintona	Clinton	k1gMnSc4	Clinton
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Volby	volba	k1gFnSc2	volba
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
George	Georg	k1gMnSc2	Georg
Bush	Bush	k1gMnSc1	Bush
před	před	k7c7	před
volbami	volba	k1gFnPc7	volba
porazil	porazit	k5eAaPmAgMnS	porazit
své	svůj	k3xOyFgMnPc4	svůj
protikandidáty	protikandidát	k1gMnPc4	protikandidát
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
nominaci	nominace	k1gFnSc4	nominace
republikánské	republikánský	k2eAgFnSc2d1	republikánská
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Kandidátem	kandidát	k1gMnSc7	kandidát
demokratů	demokrat	k1gMnPc2	demokrat
a	a	k8xC	a
Bushovým	Bushův	k2eAgMnSc7d1	Bushův
protivníkem	protivník	k1gMnSc7	protivník
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
(	(	kIx(	(
<g/>
rovněž	rovněž	k9	rovněž
bez	bez	k7c2	bez
větších	veliký	k2eAgInPc2d2	veliký
problémů	problém	k1gInPc2	problém
<g/>
)	)	kIx)	)
viceprezident	viceprezident	k1gMnSc1	viceprezident
Billa	Bill	k1gMnSc2	Bill
Clintona	Clinton	k1gMnSc2	Clinton
Al	ala	k1gFnPc2	ala
Gore	Gor	k1gMnSc2	Gor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kampani	kampaň	k1gFnSc6	kampaň
Bush	Bush	k1gMnSc1	Bush
vyzdvihl	vyzdvihnout	k5eAaPmAgMnS	vyzdvihnout
svoji	svůj	k3xOyFgFnSc4	svůj
vůli	vůle	k1gFnSc4	vůle
zabývat	zabývat	k5eAaImF	zabývat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
vnitřními	vnitřní	k2eAgFnPc7d1	vnitřní
záležitostmi	záležitost	k1gFnPc7	záležitost
země	zem	k1gFnSc2	zem
a	a	k8xC	a
snížit	snížit	k5eAaPmF	snížit
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
angažovanost	angažovanost	k1gFnSc4	angažovanost
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
izolacionistickou	izolacionistický	k2eAgFnSc7d1	izolacionistická
tradicí	tradice	k1gFnSc7	tradice
Republikánské	republikánský	k2eAgFnSc2d1	republikánská
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Opakovaně	opakovaně	k6eAd1	opakovaně
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
zahraničním	zahraniční	k2eAgMnPc3d1	zahraniční
národně-budovatelským	národněudovatelský	k2eAgMnPc3d1	národně-budovatelský
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
nation-building	nationuilding	k1gInSc1	nation-building
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
pokusům	pokus	k1gInPc3	pokus
(	(	kIx(	(
<g/>
rozuměj	rozumět	k5eAaImRp2nS	rozumět
zásahům	zásah	k1gInPc3	zásah
do	do	k7c2	do
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
záležitostí	záležitost	k1gFnPc2	záležitost
jiných	jiný	k2eAgInPc2d1	jiný
států	stát	k1gInPc2	stát
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
státního	státní	k2eAgNnSc2d1	státní
zřízení	zřízení	k1gNnSc2	zřízení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Názor	názor	k1gInSc1	názor
změnil	změnit	k5eAaPmAgInS	změnit
po	po	k7c6	po
útocích	útok	k1gInPc6	útok
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
kampaně	kampaň	k1gFnSc2	kampaň
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
povolení	povolení	k1gNnPc4	povolení
náboženským	náboženský	k2eAgInPc3d1	náboženský
charitativním	charitativní	k2eAgInPc3d1	charitativní
spolkům	spolek	k1gInPc3	spolek
zúčastňovat	zúčastňovat	k5eAaImF	zúčastňovat
se	se	k3xPyFc4	se
federálně	federálně	k6eAd1	federálně
podporovaných	podporovaný	k2eAgInPc2d1	podporovaný
programů	program	k1gInPc2	program
<g/>
,	,	kIx,	,
používání	používání	k1gNnSc3	používání
vzdělávacích	vzdělávací	k2eAgInPc2d1	vzdělávací
lístků	lístek	k1gInPc2	lístek
<g/>
,	,	kIx,	,
těžbu	těžba	k1gFnSc4	těžba
ropy	ropa	k1gFnSc2	ropa
v	v	k7c6	v
národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
Arctic	Arctice	k1gFnPc2	Arctice
National	National	k1gMnSc1	National
Wildlife	Wildlif	k1gInSc5	Wildlif
Refuge	Refuge	k1gNnPc7	Refuge
na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
<g/>
,	,	kIx,	,
restrukturalizaci	restrukturalizace	k1gFnSc3	restrukturalizace
armády	armáda	k1gFnSc2	armáda
USA	USA	kA	USA
atd.	atd.	kA	atd.
Průběh	průběh	k1gInSc4	průběh
voleb	volba	k1gFnPc2	volba
a	a	k8xC	a
zejména	zejména	k9	zejména
jejich	jejich	k3xOp3gNnSc1	jejich
vyhodnocení	vyhodnocení	k1gNnSc1	vyhodnocení
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
být	být	k5eAaImF	být
velice	velice	k6eAd1	velice
kontroverzní	kontroverzní	k2eAgMnSc1d1	kontroverzní
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
státech	stát	k1gInPc6	stát
velmi	velmi	k6eAd1	velmi
těsné	těsný	k2eAgNnSc1d1	těsné
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Mexiku	Mexiko	k1gNnSc6	Mexiko
a	a	k8xC	a
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
<g/>
.	.	kIx.	.
</s>
<s>
Nemalé	malý	k2eNgFnPc1d1	nemalá
emoce	emoce	k1gFnPc1	emoce
vzbudily	vzbudit	k5eAaPmAgFnP	vzbudit
i	i	k9	i
deklarace	deklarace	k1gFnSc1	deklarace
některých	některý	k3yIgFnPc2	některý
televizních	televizní	k2eAgFnPc2d1	televizní
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
první	první	k4xOgFnSc6	první
přinést	přinést	k5eAaPmF	přinést
výsledky	výsledek	k1gInPc1	výsledek
voleb	volba	k1gFnPc2	volba
oznámily	oznámit	k5eAaPmAgInP	oznámit
Gorovo	Gorovo	k1gNnSc4	Gorovo
vítězství	vítězství	k1gNnSc2	vítězství
již	již	k6eAd1	již
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
ještě	ještě	k9	ještě
volilo	volit	k5eAaImAgNnS	volit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
demokratů	demokrat	k1gMnPc2	demokrat
na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
vybudilo	vybudit	k5eAaPmAgNnS	vybudit
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
republikánů	republikán	k1gMnPc2	republikán
k	k	k7c3	k
účasti	účast	k1gFnSc3	účast
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
</s>
<s>
Zásadní	zásadní	k2eAgInSc4d1	zásadní
problém	problém	k1gInSc4	problém
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Floridy	Florida	k1gFnSc2	Florida
se	se	k3xPyFc4	se
však	však	k9	však
týkal	týkat	k5eAaImAgInS	týkat
hlasovacích	hlasovací	k2eAgInPc2d1	hlasovací
lístků	lístek	k1gInPc2	lístek
a	a	k8xC	a
nedostatků	nedostatek	k1gInPc2	nedostatek
ve	v	k7c4	v
sčítání	sčítání	k1gNnSc4	sčítání
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatky	nedostatek	k1gInPc4	nedostatek
a	a	k8xC	a
nejasnosti	nejasnost	k1gFnPc4	nejasnost
některých	některý	k3yIgInPc2	některý
volebních	volební	k2eAgInPc2d1	volební
formulářů	formulář	k1gInPc2	formulář
vedly	vést	k5eAaImAgInP	vést
ke	k	k7c3	k
sporům	spor	k1gInPc3	spor
na	na	k7c6	na
některých	některý	k3yIgInPc6	některý
volebních	volební	k2eAgInPc6d1	volební
úřadech	úřad	k1gInPc6	úřad
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
rozdíl	rozdíl	k1gInSc4	rozdíl
jen	jen	k9	jen
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
stovek	stovka	k1gFnPc2	stovka
hlasů	hlas	k1gInPc2	hlas
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
oficiálních	oficiální	k2eAgInPc2d1	oficiální
výsledků	výsledek	k1gInPc2	výsledek
Bush	Bush	k1gMnSc1	Bush
získal	získat	k5eAaPmAgMnS	získat
2	[number]	k4	2
912	[number]	k4	912
790	[number]	k4	790
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
Al	ala	k1gFnPc2	ala
Gore	Gore	k1gNnSc1	Gore
2	[number]	k4	2
912	[number]	k4	912
253	[number]	k4	253
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Advokáti	advokát	k1gMnPc1	advokát
Ala	ala	k1gFnSc1	ala
Gorea	Gorea	k1gFnSc1	Gorea
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
u	u	k7c2	u
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
Floridy	Florida	k1gFnSc2	Florida
(	(	kIx(	(
<g/>
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
6	[number]	k4	6
soudců	soudce	k1gMnPc2	soudce
ze	z	k7c2	z
7	[number]	k4	7
bylo	být	k5eAaImAgNnS	být
nominováno	nominovat	k5eAaBmNgNnS	nominovat
demokratickou	demokratický	k2eAgFnSc7d1	demokratická
stranou	strana	k1gFnSc7	strana
<g/>
)	)	kIx)	)
nového	nový	k2eAgNnSc2d1	nové
manuálního	manuální	k2eAgNnSc2d1	manuální
sčítání	sčítání	k1gNnSc2	sčítání
hlasů	hlas	k1gInPc2	hlas
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
obvodech	obvod	k1gInPc6	obvod
<g/>
:	:	kIx,	:
Miami	Miami	k1gNnPc2	Miami
Dade	Dade	k1gNnSc2	Dade
<g/>
,	,	kIx,	,
Palm	Palm	k1gMnSc1	Palm
Beach	Beach	k1gMnSc1	Beach
a	a	k8xC	a
Broward	Broward	k1gMnSc1	Broward
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
je	být	k5eAaImIp3nS	být
voličská	voličský	k2eAgFnSc1d1	voličská
základna	základna	k1gFnSc1	základna
výrazně	výrazně	k6eAd1	výrazně
demokratická	demokratický	k2eAgFnSc1d1	demokratická
<g/>
.	.	kIx.	.
</s>
<s>
Právníci	právník	k1gMnPc1	právník
G.	G.	kA	G.
W.	W.	kA	W.
Bushe	Bush	k1gMnSc2	Bush
napadli	napadnout	k5eAaPmAgMnP	napadnout
toto	tento	k3xDgNnSc4	tento
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
s	s	k7c7	s
odůvodněním	odůvodnění	k1gNnSc7	odůvodnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
tím	ten	k3xDgNnSc7	ten
nejvyšší	vysoký	k2eAgInPc4d3	Nejvyšší
soud	soud	k1gInSc4	soud
Floridy	Florida	k1gFnSc2	Florida
překročil	překročit	k5eAaPmAgMnS	překročit
své	svůj	k3xOyFgFnPc4	svůj
pravomoce	pravomoc	k1gFnPc4	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
USA	USA	kA	USA
(	(	kIx(	(
<g/>
jehož	jehož	k3xOyRp3gMnPc2	jehož
7	[number]	k4	7
soudců	soudce	k1gMnPc2	soudce
z	z	k7c2	z
9	[number]	k4	9
jmenovali	jmenovat	k5eAaBmAgMnP	jmenovat
republikánští	republikánský	k2eAgMnPc1d1	republikánský
prezidenti	prezident	k1gMnPc1	prezident
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
4	[number]	k4	4
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byli	být	k5eAaImAgMnP	být
obecně	obecně	k6eAd1	obecně
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
konzervativce	konzervativec	k1gMnPc4	konzervativec
<g/>
,	,	kIx,	,
4	[number]	k4	4
za	za	k7c4	za
liberály	liberál	k1gMnPc4	liberál
a	a	k8xC	a
poslední	poslední	k2eAgFnSc1d1	poslední
soudkyně	soudkyně	k1gFnSc1	soudkyně
za	za	k7c4	za
umírněnou	umírněný	k2eAgFnSc4d1	umírněná
<g/>
)	)	kIx)	)
jim	on	k3xPp3gMnPc3	on
dal	dát	k5eAaPmAgInS	dát
za	za	k7c4	za
pravdu	pravda	k1gFnSc4	pravda
a	a	k8xC	a
přepočítávání	přepočítávání	k1gNnSc6	přepočítávání
hlasů	hlas	k1gInPc2	hlas
zrušil	zrušit	k5eAaPmAgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
těsné	těsný	k2eAgInPc1d1	těsný
rozdíly	rozdíl	k1gInPc1	rozdíl
výsledků	výsledek	k1gInPc2	výsledek
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Mexiku	Mexiko	k1gNnSc6	Mexiko
(	(	kIx(	(
<g/>
0,06	[number]	k4	0,06
%	%	kIx~	%
pro	pro	k7c4	pro
Ala	ala	k1gFnSc1	ala
Gora	Gora	k1gMnSc1	Gora
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Wisconsinu	Wisconsina	k1gFnSc4	Wisconsina
(	(	kIx(	(
<g/>
0,22	[number]	k4	0,22
%	%	kIx~	%
pro	pro	k7c4	pro
Ala	ala	k1gFnSc1	ala
Gora	Gora	k1gMnSc1	Gora
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Iowě	Iowě	k1gMnSc2	Iowě
(	(	kIx(	(
<g/>
0,31	[number]	k4	0,31
%	%	kIx~	%
pro	pro	k7c4	pro
Ala	ala	k1gFnSc1	ala
Gora	Gora	k1gMnSc1	Gora
<g/>
)	)	kIx)	)
a	a	k8xC	a
Oregonu	Oregona	k1gFnSc4	Oregona
(	(	kIx(	(
<g/>
0,44	[number]	k4	0,44
%	%	kIx~	%
pro	pro	k7c4	pro
Ala	ala	k1gFnSc1	ala
Gora	Gora	k1gMnSc1	Gora
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
rozdíly	rozdíl	k1gInPc1	rozdíl
už	už	k9	už
překračovaly	překračovat	k5eAaImAgInP	překračovat
alespoň	alespoň	k9	alespoň
hranici	hranice	k1gFnSc4	hranice
jednoho	jeden	k4xCgInSc2	jeden
procenta	procent	k1gInSc2	procent
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
byl	být	k5eAaImAgInS	být
George	George	k1gFnSc7	George
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
zvolen	zvolen	k2eAgMnSc1d1	zvolen
prezidentem	prezident	k1gMnSc7	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
po	po	k7c6	po
rozhodnutí	rozhodnutí	k1gNnSc6	rozhodnutí
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
díky	díky	k7c3	díky
hlasům	hlas	k1gInPc3	hlas
volitelů	volitel	k1gMnPc2	volitel
z	z	k7c2	z
Floridy	Florida	k1gFnSc2	Florida
<g/>
.	.	kIx.	.
</s>
<s>
Bush	Bush	k1gMnSc1	Bush
získal	získat	k5eAaPmAgMnS	získat
celkem	celkem	k6eAd1	celkem
271	[number]	k4	271
hlasů	hlas	k1gInPc2	hlas
volitelů	volitel	k1gMnPc2	volitel
proti	proti	k7c3	proti
266	[number]	k4	266
hlasům	hlas	k1gInPc3	hlas
Ala	ala	k0	ala
Gorea	Gore	k2eAgFnSc1d1	Gorea
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
Al	ala	k1gFnPc2	ala
Gore	Gore	k1gNnSc4	Gore
získal	získat	k5eAaPmAgInS	získat
více	hodně	k6eAd2	hodně
hlasů	hlas	k1gInPc2	hlas
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
48,4	[number]	k4	48,4
%	%	kIx~	%
proti	proti	k7c3	proti
47,9	[number]	k4	47,9
%	%	kIx~	%
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
o	o	k7c4	o
540	[number]	k4	540
000	[number]	k4	000
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
2000	[number]	k4	2000
tak	tak	k9	tak
byly	být	k5eAaImAgInP	být
první	první	k4xOgFnSc7	první
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vítěz	vítěz	k1gMnSc1	vítěz
voleb	volba	k1gFnPc2	volba
získal	získat	k5eAaPmAgMnS	získat
méně	málo	k6eAd2	málo
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
než	než	k8xS	než
jeho	jeho	k3xOp3gMnSc1	jeho
oponent	oponent	k1gMnSc1	oponent
<g/>
,	,	kIx,	,
a	a	k8xC	a
první	první	k4xOgFnSc1	první
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1876	[number]	k4	1876
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
konečná	konečný	k2eAgFnSc1d1	konečná
volba	volba	k1gFnSc1	volba
prezidenta	prezident	k1gMnSc2	prezident
závisela	záviset	k5eAaImAgFnS	záviset
na	na	k7c4	na
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Bushova	Bushův	k2eAgFnSc1d1	Bushova
předvolební	předvolební	k2eAgFnSc1d1	předvolební
kampaň	kampaň	k1gFnSc1	kampaň
stála	stát	k5eAaImAgFnS	stát
191	[number]	k4	191
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Bush	Bush	k1gMnSc1	Bush
začínal	začínat	k5eAaImAgMnS	začínat
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
v	v	k7c6	v
komplikované	komplikovaný	k2eAgFnSc6d1	komplikovaná
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
situaci	situace	k1gFnSc6	situace
po	po	k7c6	po
zhroucení	zhroucení	k1gNnSc6	zhroucení
"	"	kIx"	"
<g/>
internetové	internetový	k2eAgFnSc2d1	internetová
bubliny	bublina	k1gFnSc2	bublina
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
USA	USA	kA	USA
vážně	vážně	k6eAd1	vážně
hrozila	hrozit	k5eAaImAgFnS	hrozit
recese	recese	k1gFnSc1	recese
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
úmyslem	úmysl	k1gInSc7	úmysl
oživit	oživit	k5eAaPmF	oživit
spotřebu	spotřeba	k1gFnSc4	spotřeba
a	a	k8xC	a
splnit	splnit	k5eAaPmF	splnit
své	svůj	k3xOyFgInPc4	svůj
předvolební	předvolební	k2eAgInPc4d1	předvolební
sliby	slib	k1gInPc4	slib
prosadil	prosadit	k5eAaPmAgMnS	prosadit
v	v	k7c6	v
Kongresu	kongres	k1gInSc6	kongres
sérii	série	k1gFnSc4	série
zákonů	zákon	k1gInPc2	zákon
snižujících	snižující	k2eAgMnPc2d1	snižující
daně	daň	k1gFnSc2	daň
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
o	o	k7c4	o
tyto	tento	k3xDgFnPc4	tento
tři	tři	k4xCgFnPc4	tři
věci	věc	k1gFnPc4	věc
<g/>
:	:	kIx,	:
snížení	snížení	k1gNnSc4	snížení
Daně	daň	k1gFnSc2	daň
z	z	k7c2	z
příjmu	příjem	k1gInSc2	příjem
pro	pro	k7c4	pro
sezdané	sezdaný	k2eAgFnPc4d1	sezdaná
dvojice	dvojice	k1gFnPc4	dvojice
<g/>
,	,	kIx,	,
zrušení	zrušení	k1gNnSc4	zrušení
Daně	daň	k1gFnSc2	daň
z	z	k7c2	z
nemovitostí	nemovitost	k1gFnPc2	nemovitost
a	a	k8xC	a
snížení	snížení	k1gNnSc2	snížení
Hlavní	hlavní	k2eAgFnSc2d1	hlavní
daně	daň	k1gFnSc2	daň
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Kongresového	kongresový	k2eAgNnSc2d1	Kongresové
rozpočtového	rozpočtový	k2eAgNnSc2d1	rozpočtové
centra	centrum	k1gNnSc2	centrum
tato	tento	k3xDgFnSc1	tento
snížení	snížení	k1gNnSc4	snížení
daní	daň	k1gFnPc2	daň
snížily	snížit	k5eAaPmAgFnP	snížit
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
celkový	celkový	k2eAgInSc1d1	celkový
federální	federální	k2eAgInSc1d1	federální
příjem	příjem	k1gInSc1	příjem
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
podíl	podíl	k1gInSc1	podíl
z	z	k7c2	z
hrubého	hrubý	k2eAgInSc2d1	hrubý
domácího	domácí	k2eAgInSc2d1	domácí
produktu	produkt	k1gInSc2	produkt
(	(	kIx(	(
<g/>
HDP	HDP	kA	HDP
<g/>
)	)	kIx)	)
na	na	k7c4	na
nejnižší	nízký	k2eAgFnSc4d3	nejnižší
míru	míra	k1gFnSc4	míra
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
snížení	snížení	k1gNnSc1	snížení
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
z	z	k7c2	z
43	[number]	k4	43
procent	procento	k1gNnPc2	procento
týká	týkat	k5eAaImIp3nS	týkat
nejbohatšího	bohatý	k2eAgMnSc4d3	nejbohatší
jednoho	jeden	k4xCgNnSc2	jeden
procenta	procento	k1gNnSc2	procento
Američanů	Američan	k1gMnPc2	Američan
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následek	k1gInSc7	následek
byl	být	k5eAaImAgInS	být
rekordní	rekordní	k2eAgInSc1d1	rekordní
deficit	deficit	k1gInSc1	deficit
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
(	(	kIx(	(
<g/>
ovšem	ovšem	k9	ovšem
jen	jen	k9	jen
v	v	k7c6	v
absolutních	absolutní	k2eAgNnPc6d1	absolutní
číslech	číslo	k1gNnPc6	číslo
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
v	v	k7c6	v
procentech	procento	k1gNnPc6	procento
HDP	HDP	kA	HDP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
roce	rok	k1gInSc6	rok
vlády	vláda	k1gFnSc2	vláda
Billa	Bill	k1gMnSc2	Bill
Clintona	Clinton	k1gMnSc2	Clinton
vykazoval	vykazovat	k5eAaImAgInS	vykazovat
federální	federální	k2eAgInSc1d1	federální
rozpočet	rozpočet	k1gInSc1	rozpočet
roční	roční	k2eAgInSc4d1	roční
přebytek	přebytek	k1gInSc4	přebytek
230	[number]	k4	230
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
za	za	k7c2	za
Bushovy	Bushův	k2eAgFnSc2d1	Bushova
vlády	vláda	k1gFnSc2	vláda
přešlo	přejít	k5eAaPmAgNnS	přejít
hospodaření	hospodaření	k1gNnSc1	hospodaření
do	do	k7c2	do
deficitu	deficit	k1gInSc2	deficit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
roční	roční	k2eAgInSc1d1	roční
deficit	deficit	k1gInSc1	deficit
rekordních	rekordní	k2eAgFnPc2d1	rekordní
374	[number]	k4	374
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
představovalo	představovat	k5eAaImAgNnS	představovat
3,5	[number]	k4	3,5
%	%	kIx~	%
HDP	HDP	kA	HDP
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
procentech	procento	k1gNnPc6	procento
domácího	domácí	k2eAgInSc2d1	domácí
produktu	produkt	k1gInSc2	produkt
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
než	než	k8xS	než
některé	některý	k3yIgInPc1	některý
předcházející	předcházející	k2eAgInPc1d1	předcházející
deficity	deficit	k1gInPc1	deficit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
ekonomů	ekonom	k1gMnPc2	ekonom
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
takovéto	takovýto	k3xDgNnSc4	takovýto
porovnávání	porovnávání	k1gNnSc4	porovnávání
význam	význam	k1gInSc4	význam
-	-	kIx~	-
protože	protože	k8xS	protože
porovnávání	porovnávání	k1gNnSc4	porovnávání
absolutních	absolutní	k2eAgNnPc2d1	absolutní
čísel	číslo	k1gNnPc2	číslo
je	být	k5eAaImIp3nS	být
zkresleno	zkreslen	k2eAgNnSc1d1	zkresleno
růstem	růst	k1gInSc7	růst
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Bush	Bush	k1gMnSc1	Bush
navýšil	navýšit	k5eAaPmAgMnS	navýšit
rozpočty	rozpočet	k1gInPc4	rozpočet
pro	pro	k7c4	pro
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
a	a	k8xC	a
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
snížil	snížit	k5eAaPmAgInS	snížit
výdaje	výdaj	k1gInPc4	výdaj
na	na	k7c4	na
nevojenské	vojenský	k2eNgInPc4d1	nevojenský
programy	program	k1gInPc4	program
<g/>
.	.	kIx.	.
</s>
<s>
Výdaje	výdaj	k1gInPc1	výdaj
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
zvýšily	zvýšit	k5eAaPmAgFnP	zvýšit
za	za	k7c4	za
4	[number]	k4	4
roky	rok	k1gInPc4	rok
o	o	k7c4	o
20	[number]	k4	20
%	%	kIx~	%
(	(	kIx(	(
<g/>
opět	opět	k6eAd1	opět
ale	ale	k8xC	ale
jen	jen	k9	jen
v	v	k7c6	v
absolutních	absolutní	k2eAgNnPc6d1	absolutní
číslech	číslo	k1gNnPc6	číslo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
ekonomové	ekonom	k1gMnPc1	ekonom
mluví	mluvit	k5eAaImIp3nP	mluvit
o	o	k7c6	o
hospodářském	hospodářský	k2eAgInSc6d1	hospodářský
zázraku	zázrak	k1gInSc6	zázrak
během	během	k7c2	během
Bushova	Bushův	k2eAgInSc2d1	Bushův
prvního	první	k4xOgNnSc2	první
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářství	hospodářství	k1gNnSc1	hospodářství
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
stála	stát	k5eAaImAgFnS	stát
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
recese	recese	k1gFnSc2	recese
<g/>
,	,	kIx,	,
začalo	začít	k5eAaPmAgNnS	začít
pomalu	pomalu	k6eAd1	pomalu
nabírat	nabírat	k5eAaImF	nabírat
na	na	k7c6	na
síle	síla	k1gFnSc6	síla
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Nemalý	malý	k2eNgInSc4d1	nemalý
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
měly	mít	k5eAaImAgFnP	mít
i	i	k9	i
události	událost	k1gFnPc1	událost
ze	z	k7c2	z
září	září	k1gNnSc2	září
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
růst	růst	k1gInSc1	růst
o	o	k7c4	o
8,2	[number]	k4	8,2
a	a	k8xC	a
4,0	[number]	k4	4,0
%	%	kIx~	%
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zaznamenaly	zaznamenat	k5eAaPmAgFnP	zaznamenat
USA	USA	kA	USA
v	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
čtvrtletí	čtvrtletí	k1gNnSc6	čtvrtletí
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
nejvýraznější	výrazný	k2eAgInSc1d3	nejvýraznější
růst	růst	k1gInSc1	růst
ekonomiky	ekonomika	k1gFnSc2	ekonomika
USA	USA	kA	USA
za	za	k7c2	za
posledních	poslední	k2eAgNnPc2d1	poslední
20	[number]	k4	20
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
čísla	číslo	k1gNnPc1	číslo
jsou	být	k5eAaImIp3nP	být
přepočítaná	přepočítaný	k2eAgNnPc1d1	přepočítané
na	na	k7c4	na
roky	rok	k1gInPc4	rok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
převzetí	převzetí	k1gNnSc6	převzetí
moci	moc	k1gFnSc2	moc
ekonomika	ekonomika	k1gFnSc1	ekonomika
rostla	růst	k5eAaImAgFnS	růst
ročním	roční	k2eAgNnSc7d1	roční
tempem	tempo	k1gNnSc7	tempo
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
0,8	[number]	k4	0,8
%	%	kIx~	%
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
už	už	k9	už
1,9	[number]	k4	1,9
%	%	kIx~	%
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
3,0	[number]	k4	3,0
%	%	kIx~	%
a	a	k8xC	a
ve	v	k7c6	v
volebním	volební	k2eAgInSc6d1	volební
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
dokonce	dokonce	k9	dokonce
4,2	[number]	k4	4,2
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
důvěra	důvěra	k1gFnSc1	důvěra
investorů	investor	k1gMnPc2	investor
v	v	k7c4	v
dobré	dobrý	k2eAgFnPc4d1	dobrá
vyhlídky	vyhlídka	k1gFnPc4	vyhlídka
ekonomiky	ekonomika	k1gFnSc2	ekonomika
USA	USA	kA	USA
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
růst	růst	k1gInSc1	růst
export	export	k1gInSc4	export
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
díky	díky	k7c3	díky
devalvaci	devalvace	k1gFnSc3	devalvace
dolaru	dolar	k1gInSc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
části	část	k1gFnSc6	část
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
byly	být	k5eAaImAgInP	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
tahounem	tahoun	k1gInSc7	tahoun
růstu	růst	k1gInSc2	růst
HDP	HDP	kA	HDP
vládní	vládní	k2eAgInPc1d1	vládní
výdaje	výdaj	k1gInPc1	výdaj
a	a	k8xC	a
spotřeba	spotřeba	k1gFnSc1	spotřeba
domácností	domácnost	k1gFnPc2	domácnost
(	(	kIx(	(
<g/>
hrazená	hrazený	k2eAgFnSc1d1	hrazená
na	na	k7c4	na
dluh	dluh	k1gInSc4	dluh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
začala	začít	k5eAaPmAgFnS	začít
převažovat	převažovat	k5eAaImF	převažovat
spotřeba	spotřeba	k1gFnSc1	spotřeba
domácností	domácnost	k1gFnPc2	domácnost
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
fixními	fixní	k2eAgFnPc7d1	fixní
investicemi	investice	k1gFnPc7	investice
firem	firma	k1gFnPc2	firma
a	a	k8xC	a
růstem	růst	k1gInSc7	růst
exportu	export	k1gInSc2	export
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
převyšoval	převyšovat	k5eAaImAgInS	převyšovat
růst	růst	k1gInSc4	růst
importu	import	k1gInSc2	import
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
celosvětového	celosvětový	k2eAgInSc2d1	celosvětový
růstu	růst	k1gInSc2	růst
a	a	k8xC	a
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
vývoje	vývoj	k1gInSc2	vývoj
evropských	evropský	k2eAgFnPc2d1	Evropská
ekonomik	ekonomika	k1gFnPc2	ekonomika
staly	stát	k5eAaPmAgFnP	stát
"	"	kIx"	"
<g/>
hospodářským	hospodářský	k2eAgMnSc7d1	hospodářský
skokanem	skokan	k1gMnSc7	skokan
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Rostly	růst	k5eAaImAgInP	růst
totiž	totiž	k9	totiž
osmkrát	osmkrát	k6eAd1	osmkrát
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
Evropská	evropský	k2eAgFnSc1d1	Evropská
měnová	měnový	k2eAgFnSc1d1	měnová
unie	unie	k1gFnSc1	unie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
růst	růst	k1gInSc1	růst
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
jen	jen	k9	jen
0,4	[number]	k4	0,4
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc1d1	podobný
rozdíl	rozdíl	k1gInSc1	rozdíl
byl	být	k5eAaImAgInS	být
zachovaný	zachovaný	k2eAgMnSc1d1	zachovaný
i	i	k9	i
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
pozitivnímu	pozitivní	k2eAgInSc3d1	pozitivní
vývoji	vývoj	k1gInSc3	vývoj
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
USA	USA	kA	USA
i	i	k9	i
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
dalších	další	k2eAgFnPc6d1	další
oblastech	oblast	k1gFnPc6	oblast
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2001	[number]	k4	2001
<g/>
-	-	kIx~	-
<g/>
2003	[number]	k4	2003
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
produktivita	produktivita	k1gFnSc1	produktivita
práce	práce	k1gFnSc2	práce
nejvíc	hodně	k6eAd3	hodně
za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
50	[number]	k4	50
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
akciová	akciový	k2eAgFnSc1d1	akciová
burza	burza	k1gFnSc1	burza
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
vzpamatovala	vzpamatovat	k5eAaPmAgFnS	vzpamatovat
ze	z	k7c2	z
ztrát	ztráta	k1gFnPc2	ztráta
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
(	(	kIx(	(
<g/>
Dow	Dow	k1gFnSc1	Dow
Jones	Jones	k1gInSc4	Jones
Industrial	Industrial	k1gInSc1	Industrial
Index	index	k1gInSc1	index
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
o	o	k7c4	o
40	[number]	k4	40
%	%	kIx~	%
a	a	k8xC	a
technologický	technologický	k2eAgInSc1d1	technologický
index	index	k1gInSc1	index
Nasdaq	Nasdaq	k1gFnSc2	Nasdaq
Composite	Composit	k1gInSc5	Composit
se	se	k3xPyFc4	se
zhodnotil	zhodnotit	k5eAaPmAgMnS	zhodnotit
dokonce	dokonce	k9	dokonce
o	o	k7c4	o
70	[number]	k4	70
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slušným	slušný	k2eAgNnSc7d1	slušné
tempem	tempo	k1gNnSc7	tempo
rostly	růst	k5eAaImAgInP	růst
zisky	zisk	k1gInPc1	zisk
amerických	americký	k2eAgFnPc2d1	americká
společností	společnost	k1gFnPc2	společnost
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
otočit	otočit	k5eAaPmF	otočit
i	i	k9	i
vývoj	vývoj	k1gInSc4	vývoj
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
(	(	kIx(	(
<g/>
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
stále	stále	k6eAd1	stále
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
,	,	kIx,	,
než	než	k8xS	než
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
<g/>
)	)	kIx)	)
a	a	k8xC	a
míra	míra	k1gFnSc1	míra
inflace	inflace	k1gFnSc1	inflace
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
přijatelnou	přijatelný	k2eAgFnSc4d1	přijatelná
úroveň	úroveň	k1gFnSc4	úroveň
přibližně	přibližně	k6eAd1	přibližně
dvou	dva	k4xCgNnPc2	dva
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
převzetí	převzetí	k1gNnSc6	převzetí
moci	moc	k1gFnSc2	moc
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
úrovně	úroveň	k1gFnPc4	úroveň
4,2	[number]	k4	4,2
%	%	kIx~	%
(	(	kIx(	(
<g/>
leden	leden	k1gInSc1	leden
a	a	k8xC	a
únor	únor	k1gInSc1	únor
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
tehdy	tehdy	k6eAd1	tehdy
měla	mít	k5eAaImAgFnS	mít
silně	silně	k6eAd1	silně
stoupající	stoupající	k2eAgFnSc4d1	stoupající
tendenci	tendence	k1gFnSc4	tendence
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
zdědila	zdědit	k5eAaPmAgFnS	zdědit
po	po	k7c6	po
předcházející	předcházející	k2eAgFnSc6d1	předcházející
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
před	před	k7c7	před
nástupem	nástup	k1gInSc7	nástup
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
jen	jen	k9	jen
3,9	[number]	k4	3,9
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
posledního	poslední	k2eAgInSc2d1	poslední
měsíce	měsíc	k1gInSc2	měsíc
vlády	vláda	k1gFnSc2	vláda
Billa	Bill	k1gMnSc2	Bill
Clintona	Clinton	k1gMnSc2	Clinton
narostla	narůst	k5eAaPmAgFnS	narůst
o	o	k7c4	o
0,3	[number]	k4	0,3
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Růst	růst	k1gInSc1	růst
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
měsících	měsíc	k1gInPc6	měsíc
<g/>
,	,	kIx,	,
v	v	k7c6	v
září	září	k1gNnSc6	září
2001	[number]	k4	2001
už	už	k6eAd1	už
nezaměstnanost	nezaměstnanost	k1gFnSc4	nezaměstnanost
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
hranice	hranice	k1gFnSc1	hranice
5	[number]	k4	5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
útocích	útok	k1gInPc6	útok
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2001	[number]	k4	2001
přišel	přijít	k5eAaPmAgInS	přijít
další	další	k2eAgInSc1d1	další
prudký	prudký	k2eAgInSc1d1	prudký
nárůst	nárůst	k1gInSc1	nárůst
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2002	[number]	k4	2002
už	už	k6eAd1	už
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
5,9	[number]	k4	5,9
%	%	kIx~	%
<g/>
,	,	kIx,	,
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
6	[number]	k4	6
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2003	[number]	k4	2003
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
svůj	svůj	k3xOyFgInSc4	svůj
vrchol	vrchol	k1gInSc4	vrchol
-	-	kIx~	-
6,3	[number]	k4	6,3
%	%	kIx~	%
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
už	už	k6eAd1	už
začala	začít	k5eAaPmAgFnS	začít
klesat	klesat	k5eAaImF	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
klesla	klesnout	k5eAaPmAgFnS	klesnout
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
5,7	[number]	k4	5,7
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volebním	volební	k2eAgInSc6d1	volební
měsíci	měsíc	k1gInSc6	měsíc
(	(	kIx(	(
<g/>
listopad	listopad	k1gInSc1	listopad
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
5,4	[number]	k4	5,4
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Pokles	pokles	k1gInSc1	pokles
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
pak	pak	k6eAd1	pak
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
i	i	k9	i
v	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
volebním	volební	k2eAgNnSc6d1	volební
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
však	však	k9	však
během	během	k7c2	během
jeho	jeho	k3xOp3gInSc2	jeho
prvního	první	k4xOgInSc2	první
mandátu	mandát	k1gInSc2	mandát
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
recese	recese	k1gFnSc2	recese
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
byl	být	k5eAaImAgMnS	být
Bush	Bush	k1gMnSc1	Bush
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
stoupenci	stoupenec	k1gMnPc7	stoupenec
Johna	John	k1gMnSc2	John
Kerryho	Kerry	k1gMnSc2	Kerry
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
po	po	k7c6	po
Herbertu	Herbert	k1gInSc6	Herbert
Hooverovi	Hoover	k1gMnSc3	Hoover
<g/>
,	,	kIx,	,
během	během	k7c2	během
jehož	jehož	k3xOyRp3gFnSc2	jehož
vlády	vláda	k1gFnSc2	vláda
byla	být	k5eAaImAgFnS	být
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
jasná	jasný	k2eAgFnSc1d1	jasná
ztráta	ztráta	k1gFnSc1	ztráta
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
makroekonomických	makroekonomický	k2eAgInPc2d1	makroekonomický
ukazatelů	ukazatel	k1gInPc2	ukazatel
amerického	americký	k2eAgNnSc2d1	americké
hospodářství	hospodářství	k1gNnSc2	hospodářství
za	za	k7c2	za
Bushovy	Bushův	k2eAgFnSc2d1	Bushova
vlády	vláda	k1gFnSc2	vláda
je	být	k5eAaImIp3nS	být
bezesporu	bezesporu	k9	bezesporu
dvojitý	dvojitý	k2eAgInSc1d1	dvojitý
deficit	deficit	k1gInSc1	deficit
<g/>
:	:	kIx,	:
deficit	deficit	k1gInSc1	deficit
federálního	federální	k2eAgInSc2d1	federální
rozpočtu	rozpočet	k1gInSc2	rozpočet
a	a	k8xC	a
deficit	deficit	k1gInSc4	deficit
běžného	běžný	k2eAgInSc2d1	běžný
účtu	účet	k1gInSc2	účet
platební	platební	k2eAgFnSc2d1	platební
bilance	bilance	k1gFnSc2	bilance
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
řešit	řešit	k5eAaImF	řešit
deficit	deficit	k1gInSc4	deficit
platební	platební	k2eAgFnSc2d1	platební
bilance	bilance	k1gFnSc2	bilance
se	se	k3xPyFc4	se
Bush	Bush	k1gMnSc1	Bush
snažil	snažit	k5eAaImAgMnS	snažit
zavést	zavést	k5eAaPmF	zavést
vysoká	vysoký	k2eAgNnPc1d1	vysoké
cla	clo	k1gNnPc1	clo
na	na	k7c4	na
některé	některý	k3yIgInPc4	některý
produkty	produkt	k1gInPc4	produkt
upadajícího	upadající	k2eAgInSc2d1	upadající
primárního	primární	k2eAgInSc2d1	primární
sektoru	sektor	k1gInSc2	sektor
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
zejména	zejména	k9	zejména
v	v	k7c6	v
hutnictví	hutnictví	k1gNnSc6	hutnictví
a	a	k8xC	a
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
velkou	velký	k2eAgFnSc4d1	velká
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
konkurenci	konkurence	k1gFnSc4	konkurence
(	(	kIx(	(
<g/>
v	v	k7c6	v
první	první	k4xOgFnSc6	první
části	část	k1gFnSc6	část
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bushovo	Bushův	k2eAgNnSc1d1	Bushovo
zavedení	zavedení	k1gNnSc1	zavedení
poplatků	poplatek	k1gInPc2	poplatek
za	za	k7c4	za
import	import	k1gInSc4	import
oceli	ocel	k1gFnSc2	ocel
a	a	k8xC	a
kanadských	kanadský	k2eAgInPc2d1	kanadský
dřevařských	dřevařský	k2eAgInPc2d1	dřevařský
a	a	k8xC	a
tesařských	tesařský	k2eAgInPc2d1	tesařský
výrobků	výrobek	k1gInPc2	výrobek
bylo	být	k5eAaImAgNnS	být
kontroverzní	kontroverzní	k2eAgNnSc1d1	kontroverzní
a	a	k8xC	a
ve	v	k7c6	v
světle	světlo	k1gNnSc6	světlo
jeho	on	k3xPp3gNnSc2	on
ostentativního	ostentativní	k2eAgNnSc2d1	ostentativní
vyzdvihování	vyzdvihování	k1gNnSc2	vyzdvihování
principů	princip	k1gInPc2	princip
volného	volný	k2eAgInSc2d1	volný
trhu	trh	k1gInSc2	trh
vypadalo	vypadat	k5eAaPmAgNnS	vypadat
obzvlášť	obzvlášť	k6eAd1	obzvlášť
pikantně	pikantně	k6eAd1	pikantně
<g/>
.	.	kIx.	.
</s>
<s>
Vysloužilo	vysloužit	k5eAaPmAgNnS	vysloužit
si	se	k3xPyFc3	se
kritiku	kritika	k1gFnSc4	kritika
z	z	k7c2	z
více	hodně	k6eAd2	hodně
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
vlastního	vlastní	k2eAgInSc2d1	vlastní
tábora	tábor	k1gInSc2	tábor
konzervativců	konzervativec	k1gMnPc2	konzervativec
a	a	k8xC	a
ekonomických	ekonomický	k2eAgMnPc2d1	ekonomický
liberálů	liberál	k1gMnPc2	liberál
<g/>
.	.	kIx.	.
</s>
<s>
Poplatky	poplatek	k1gInPc1	poplatek
na	na	k7c4	na
dovoz	dovoz	k1gInSc4	dovoz
oceli	ocel	k1gFnSc2	ocel
byly	být	k5eAaImAgInP	být
posléze	posléze	k6eAd1	posléze
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
Světové	světový	k2eAgFnSc2d1	světová
obchodní	obchodní	k2eAgFnSc2d1	obchodní
organizace	organizace	k1gFnSc2	organizace
(	(	kIx(	(
<g/>
World	World	k1gInSc1	World
Trade	Trad	k1gInSc5	Trad
Organization	Organization	k1gInSc1	Organization
<g/>
)	)	kIx)	)
zrušeny	zrušit	k5eAaPmNgInP	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
bodů	bod	k1gInPc2	bod
v	v	k7c6	v
Bushově	Bushův	k2eAgInSc6d1	Bushův
předvolebním	předvolební	k2eAgInSc6d1	předvolební
programu	program	k1gInSc6	program
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
věnuje	věnovat	k5eAaImIp3nS	věnovat
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
deštných	deštný	k2eAgInPc2d1	deštný
pralesů	prales	k1gInPc2	prales
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
jeho	jeho	k3xOp3gInSc7	jeho
slibem	slib	k1gInSc7	slib
byl	být	k5eAaImAgInS	být
závazek	závazek	k1gInSc1	závazek
snížení	snížení	k1gNnSc1	snížení
emisí	emise	k1gFnPc2	emise
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
slibů	slib	k1gInPc2	slib
však	však	k9	však
nedodržel	dodržet	k5eNaPmAgMnS	dodržet
-	-	kIx~	-
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
prvních	první	k4xOgNnPc2	první
rozhodnutí	rozhodnutí	k1gNnPc2	rozhodnutí
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
bylo	být	k5eAaImAgNnS	být
odmítnutí	odmítnutí	k1gNnSc4	odmítnutí
postoupit	postoupit	k5eAaPmF	postoupit
Kjótský	Kjótský	k2eAgInSc4d1	Kjótský
protokol	protokol	k1gInSc4	protokol
k	k	k7c3	k
ratifikaci	ratifikace	k1gFnSc3	ratifikace
Senátu	senát	k1gInSc2	senát
(	(	kIx(	(
<g/>
ze	z	k7c2	z
178	[number]	k4	178
států	stát	k1gInPc2	stát
nepodepsaly	podepsat	k5eNaPmAgInP	podepsat
pouze	pouze	k6eAd1	pouze
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
a	a	k8xC	a
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
protokolu	protokol	k1gInSc2	protokol
je	být	k5eAaImIp3nS	být
omezení	omezení	k1gNnSc1	omezení
emisí	emise	k1gFnPc2	emise
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
.	.	kIx.	.
</s>
<s>
Bush	Bush	k1gMnSc1	Bush
argumentoval	argumentovat	k5eAaImAgMnS	argumentovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
podepsání	podepsání	k1gNnSc4	podepsání
Kjótského	Kjótský	k2eAgInSc2d1	Kjótský
protokolu	protokol	k1gInSc2	protokol
by	by	kYmCp3nS	by
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
ohrožení	ohrožení	k1gNnSc3	ohrožení
ekonomiky	ekonomika	k1gFnSc2	ekonomika
USA	USA	kA	USA
a	a	k8xC	a
nadnesl	nadnést	k5eAaPmAgInS	nadnést
jiný	jiný	k2eAgInSc1d1	jiný
způsob	způsob	k1gInSc1	způsob
řešení	řešení	k1gNnSc2	řešení
ekologických	ekologický	k2eAgInPc2d1	ekologický
problémů	problém	k1gInPc2	problém
<g/>
,	,	kIx,	,
spočívající	spočívající	k2eAgFnSc2d1	spočívající
zejména	zejména	k9	zejména
v	v	k7c6	v
technických	technický	k2eAgFnPc6d1	technická
inovacích	inovace	k1gFnPc6	inovace
šetřících	šetřící	k2eAgInPc2d1	šetřící
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
výhradou	výhrada	k1gFnSc7	výhrada
je	být	k5eAaImIp3nS	být
nezahrnutí	nezahrnutí	k1gNnSc1	nezahrnutí
rozvojových	rozvojový	k2eAgFnPc2d1	rozvojová
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
zejména	zejména	k9	zejména
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
Indie	Indie	k1gFnSc2	Indie
<g/>
)	)	kIx)	)
do	do	k7c2	do
protokolu	protokol	k1gInSc2	protokol
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
silně	silně	k6eAd1	silně
znevýhodňuje	znevýhodňovat	k5eAaImIp3nS	znevýhodňovat
vyspělé	vyspělý	k2eAgFnPc4d1	vyspělá
země	zem	k1gFnPc4	zem
oproti	oproti	k7c3	oproti
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
rozvíjejícím	rozvíjející	k2eAgFnPc3d1	rozvíjející
asijským	asijský	k2eAgFnPc3d1	asijská
ekonomikám	ekonomika	k1gFnPc3	ekonomika
zmíněných	zmíněný	k2eAgFnPc2d1	zmíněná
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
svého	svůj	k3xOyFgInSc2	svůj
ekologického	ekologický	k2eAgInSc2d1	ekologický
programu	program	k1gInSc2	program
výrazně	výrazně	k6eAd1	výrazně
zvýšil	zvýšit	k5eAaPmAgMnS	zvýšit
příspěvky	příspěvek	k1gInPc4	příspěvek
vlády	vláda	k1gFnSc2	vláda
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
nových	nový	k2eAgFnPc2d1	nová
technologií	technologie	k1gFnPc2	technologie
šetřících	šetřící	k2eAgInPc2d1	šetřící
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zástupci	zástupce	k1gMnPc1	zástupce
USA	USA	kA	USA
a	a	k8xC	a
ostatních	ostatní	k2eAgFnPc2d1	ostatní
zemí	zem	k1gFnPc2	zem
jednali	jednat	k5eAaImAgMnP	jednat
o	o	k7c6	o
Kjótském	Kjótský	k2eAgInSc6d1	Kjótský
protokolu	protokol	k1gInSc6	protokol
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Senát	senát	k1gInSc1	senát
USA	USA	kA	USA
hlasováním	hlasování	k1gNnSc7	hlasování
95	[number]	k4	95
ku	k	k7c3	k
0	[number]	k4	0
postavil	postavit	k5eAaPmAgMnS	postavit
proti	proti	k7c3	proti
jakékoliv	jakýkoliv	k3yIgFnSc3	jakýkoliv
dohodě	dohoda	k1gFnSc3	dohoda
o	o	k7c6	o
globálním	globální	k2eAgNnSc6d1	globální
oteplování	oteplování	k1gNnSc6	oteplování
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nevyžaduje	vyžadovat	k5eNaImIp3nS	vyžadovat
závazky	závazek	k1gInPc4	závazek
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
ekonomicky	ekonomicky	k6eAd1	ekonomicky
se	se	k3xPyFc4	se
rozvíjejících	rozvíjející	k2eAgFnPc2d1	rozvíjející
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
byl	být	k5eAaImAgMnS	být
Kjótský	Kjótský	k2eAgInSc4d1	Kjótský
protokol	protokol	k1gInSc4	protokol
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
symbolicky	symbolicky	k6eAd1	symbolicky
podepsaný	podepsaný	k2eAgInSc1d1	podepsaný
Petrem	Petr	k1gMnSc7	Petr
Burleighem	Burleigh	k1gInSc7	Burleigh
<g/>
,	,	kIx,	,
výkonným	výkonný	k2eAgMnSc7d1	výkonný
velvyslancem	velvyslanec	k1gMnSc7	velvyslanec
USA	USA	kA	USA
při	při	k7c6	při
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
vláda	vláda	k1gFnSc1	vláda
Billa	Bill	k1gMnSc2	Bill
Clintona	Clinton	k1gMnSc2	Clinton
jej	on	k3xPp3gInSc4	on
nikdy	nikdy	k6eAd1	nikdy
nepředložila	předložit	k5eNaPmAgFnS	předložit
Senátu	senát	k1gInSc3	senát
k	k	k7c3	k
ratifikaci	ratifikace	k1gFnSc3	ratifikace
<g/>
.	.	kIx.	.
</s>
<s>
Bush	Bush	k1gMnSc1	Bush
se	se	k3xPyFc4	se
obrátil	obrátit	k5eAaPmAgMnS	obrátit
na	na	k7c4	na
výzkumné	výzkumný	k2eAgFnPc4d1	výzkumná
organizace	organizace	k1gFnPc4	organizace
kvůli	kvůli	k7c3	kvůli
fenoménu	fenomén	k1gInSc3	fenomén
globálního	globální	k2eAgNnSc2d1	globální
oteplování	oteplování	k1gNnSc2	oteplování
s	s	k7c7	s
požadavkem	požadavek	k1gInSc7	požadavek
uskutečnění	uskutečnění	k1gNnSc2	uskutečnění
důsledného	důsledný	k2eAgInSc2d1	důsledný
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
souvislost	souvislost	k1gFnSc4	souvislost
mezi	mezi	k7c7	mezi
globálním	globální	k2eAgNnSc7d1	globální
oteplováním	oteplování	k1gNnSc7	oteplování
a	a	k8xC	a
lidskou	lidský	k2eAgFnSc7d1	lidská
činností	činnost	k1gFnSc7	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Svoje	svůj	k3xOyFgInPc4	svůj
názory	názor	k1gInPc4	názor
Bushova	Bushův	k2eAgFnSc1d1	Bushova
vláda	vláda	k1gFnSc1	vláda
obhajovala	obhajovat	k5eAaImAgFnS	obhajovat
i	i	k9	i
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
neexistuje	existovat	k5eNaImIp3nS	existovat
konsenzus	konsenzus	k1gInSc1	konsenzus
vědců	vědec	k1gMnPc2	vědec
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
globální	globální	k2eAgNnSc4d1	globální
oteplování	oteplování	k1gNnSc4	oteplování
<g/>
,	,	kIx,	,
když	když	k8xS	když
poukazovala	poukazovat	k5eAaImAgFnS	poukazovat
například	například	k6eAd1	například
na	na	k7c4	na
Oregonskou	Oregonský	k2eAgFnSc4d1	Oregonská
petici	petice	k1gFnSc4	petice
[	[	kIx(	[
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
19	[number]	k4	19
600	[number]	k4	600
vědců	vědec	k1gMnPc2	vědec
vyjádřilo	vyjádřit	k5eAaPmAgNnS	vyjádřit
svůj	svůj	k3xOyFgInSc4	svůj
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
s	s	k7c7	s
tvrzením	tvrzení	k1gNnSc7	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
globální	globální	k2eAgNnSc4d1	globální
oteplování	oteplování	k1gNnSc4	oteplování
je	být	k5eAaImIp3nS	být
zapříčiněno	zapříčiněn	k2eAgNnSc1d1	zapříčiněno
lidskou	lidský	k2eAgFnSc7d1	lidská
činností	činnost	k1gFnSc7	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
tato	tento	k3xDgFnSc1	tento
vláda	vláda	k1gFnSc1	vláda
systematicky	systematicky	k6eAd1	systematicky
ignorovala	ignorovat	k5eAaImAgFnS	ignorovat
závěry	závěr	k1gInPc4	závěr
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
Mezivládní	mezivládní	k2eAgFnSc2d1	mezivládní
skupiny	skupina	k1gFnSc2	skupina
klimatických	klimatický	k2eAgFnPc2d1	klimatická
změn	změna	k1gFnPc2	změna
při	při	k7c6	při
OSN	OSN	kA	OSN
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
institucí	instituce	k1gFnPc2	instituce
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterých	který	k3yQgInPc2	který
je	být	k5eAaImIp3nS	být
oteplování	oteplování	k1gNnSc1	oteplování
planety	planeta	k1gFnSc2	planeta
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
zejména	zejména	k6eAd1	zejména
lidskou	lidský	k2eAgFnSc7d1	lidská
činností	činnost	k1gFnSc7	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Administrativa	administrativa	k1gFnSc1	administrativa
taktéž	taktéž	k?	taktéž
důsledně	důsledně	k6eAd1	důsledně
používá	používat	k5eAaImIp3nS	používat
termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
změna	změna	k1gFnSc1	změna
klimatu	klima	k1gNnSc2	klima
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vyhnula	vyhnout	k5eAaPmAgFnS	vyhnout
negativním	negativní	k2eAgFnPc3d1	negativní
konotacím	konotace	k1gFnPc3	konotace
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
s	s	k7c7	s
sebou	se	k3xPyFc7	se
nese	nést	k5eAaImIp3nS	nést
běžně	běžně	k6eAd1	běžně
používané	používaný	k2eAgNnSc1d1	používané
"	"	kIx"	"
<g/>
globální	globální	k2eAgNnSc4d1	globální
oteplování	oteplování	k1gNnSc4	oteplování
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
počinem	počin	k1gInSc7	počin
Bushe	Bush	k1gMnSc2	Bush
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
byla	být	k5eAaImAgFnS	být
Iniciativa	iniciativa	k1gFnSc1	iniciativa
za	za	k7c4	za
čistou	čistý	k2eAgFnSc4d1	čistá
oblohu	obloha	k1gFnSc4	obloha
(	(	kIx(	(
<g/>
en	en	k?	en
<g/>
:	:	kIx,	:
<g/>
Clear	Clear	k1gMnSc1	Clear
Skies	Skies	k1gMnSc1	Skies
Initiative	Initiativ	k1gInSc5	Initiativ
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
projevu	projev	k1gInSc6	projev
uvádějícím	uvádějící	k2eAgInSc6d1	uvádějící
tuto	tento	k3xDgFnSc4	tento
iniciativu	iniciativa	k1gFnSc4	iniciativa
Bush	Bush	k1gMnSc1	Bush
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
růst	růst	k1gInSc1	růst
je	být	k5eAaImIp3nS	být
klíčem	klíč	k1gInSc7	klíč
k	k	k7c3	k
pokroku	pokrok	k1gInSc3	pokrok
v	v	k7c6	v
environmentální	environmentální	k2eAgFnSc6d1	environmentální
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
růst	růst	k1gInSc1	růst
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
zdroje	zdroj	k1gInPc4	zdroj
pro	pro	k7c4	pro
investice	investice	k1gFnPc4	investice
do	do	k7c2	do
čistých	čistý	k2eAgFnPc2d1	čistá
technologií	technologie	k1gFnPc2	technologie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
projevu	projev	k1gInSc6	projev
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
naplněním	naplnění	k1gNnSc7	naplnění
tohoto	tento	k3xDgInSc2	tento
programu	program	k1gInSc2	program
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
následnému	následný	k2eAgInSc3d1	následný
poklesu	pokles	k1gInSc2	pokles
emisí	emise	k1gFnPc2	emise
<g/>
:	:	kIx,	:
SO2	SO2	k1gFnPc2	SO2
o	o	k7c4	o
73	[number]	k4	73
%	%	kIx~	%
NOx	noxa	k1gFnPc2	noxa
o	o	k7c4	o
67	[number]	k4	67
%	%	kIx~	%
rtuti	rtuť	k1gFnSc2	rtuť
o	o	k7c4	o
69	[number]	k4	69
%	%	kIx~	%
Snížení	snížení	k1gNnSc6	snížení
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
zrealizovat	zrealizovat	k5eAaPmF	zrealizovat
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
fázích	fáze	k1gFnPc6	fáze
<g/>
,	,	kIx,	,
první	první	k4xOgInPc1	první
skončí	skončit	k5eAaPmIp3nP	skončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
v	v	k7c6	v
roku	rok	k1gInSc6	rok
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
každá	každý	k3xTgFnSc1	každý
elektrárna	elektrárna	k1gFnSc1	elektrárna
musí	muset	k5eAaImIp3nS	muset
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
získat	získat	k5eAaPmF	získat
povolení	povolení	k1gNnSc4	povolení
na	na	k7c4	na
vypuštění	vypuštění	k1gNnSc4	vypuštění
každé	každý	k3xTgFnSc2	každý
tuny	tuna	k1gFnSc2	tuna
uvedených	uvedený	k2eAgFnPc2d1	uvedená
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
je	být	k5eAaImIp3nS	být
založený	založený	k2eAgInSc1d1	založený
minimálně	minimálně	k6eAd1	minimálně
na	na	k7c6	na
tržních	tržní	k2eAgInPc6d1	tržní
prostředcích	prostředek	k1gInPc6	prostředek
<g/>
,	,	kIx,	,
když	když	k8xS	když
umožnil	umožnit	k5eAaPmAgInS	umožnit
obchodovat	obchodovat	k5eAaImF	obchodovat
s	s	k7c7	s
povoleními	povolení	k1gNnPc7	povolení
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
společnosti	společnost	k1gFnSc3	společnost
používající	používající	k2eAgFnPc4d1	používající
nové	nový	k2eAgFnPc4d1	nová
technologie	technologie	k1gFnPc4	technologie
mohou	moct	k5eAaImIp3nP	moct
ušetřit	ušetřit	k5eAaPmF	ušetřit
svoje	svůj	k3xOyFgFnPc4	svůj
kvóty	kvóta	k1gFnPc4	kvóta
a	a	k8xC	a
prodat	prodat	k5eAaPmF	prodat
je	být	k5eAaImIp3nS	být
dalším	další	k2eAgFnPc3d1	další
společnostem	společnost	k1gFnPc3	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
této	tento	k3xDgFnSc2	tento
iniciativy	iniciativa	k1gFnSc2	iniciativa
Bush	Bush	k1gMnSc1	Bush
zmiňoval	zmiňovat	k5eAaImAgMnS	zmiňovat
úspěch	úspěch	k1gInSc4	úspěch
programu	program	k1gInSc2	program
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
podobně	podobně	k6eAd1	podobně
přistupoval	přistupovat	k5eAaImAgMnS	přistupovat
k	k	k7c3	k
emisím	emise	k1gFnPc3	emise
SO	So	kA	So
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
oponenti	oponent	k1gMnPc1	oponent
k	k	k7c3	k
iniciativě	iniciativa	k1gFnSc3	iniciativa
vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
uplatňování	uplatňování	k1gNnSc1	uplatňování
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
tato	tento	k3xDgFnSc1	tento
iniciativa	iniciativa	k1gFnSc1	iniciativa
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
<g/>
,	,	kIx,	,
povede	povést	k5eAaPmIp3nS	povést
v	v	k7c6	v
konečném	konečný	k2eAgInSc6d1	konečný
důsledku	důsledek	k1gInSc6	důsledek
k	k	k7c3	k
růstu	růst	k1gInSc3	růst
znečištění	znečištění	k1gNnSc2	znečištění
<g/>
.	.	kIx.	.
</s>
<s>
Bush	Bush	k1gMnSc1	Bush
též	též	k9	též
podepsal	podepsat	k5eAaPmAgMnS	podepsat
Great	Great	k2eAgMnSc1d1	Great
Lakes	Lakes	k1gMnSc1	Lakes
Legacy	Legaca	k1gFnSc2	Legaca
Act	Act	k1gMnSc1	Act
of	of	k?	of
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
povoluje	povolovat	k5eAaImIp3nS	povolovat
federální	federální	k2eAgInSc1d1	federální
vládě	vláda	k1gFnSc3	vláda
začít	začít	k5eAaPmF	začít
s	s	k7c7	s
čistěním	čistění	k1gNnSc7	čistění
kontaminovaných	kontaminovaný	k2eAgInPc2d1	kontaminovaný
sedimentů	sediment	k1gInPc2	sediment
Velkých	velký	k2eAgNnPc2d1	velké
jezer	jezero	k1gNnPc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
podepsal	podepsat	k5eAaPmAgMnS	podepsat
i	i	k9	i
tzv.	tzv.	kA	tzv.
Brownfieldův	Brownfieldův	k2eAgInSc1d1	Brownfieldův
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
urychluje	urychlovat	k5eAaImIp3nS	urychlovat
proces	proces	k1gInSc1	proces
čištění	čištění	k1gNnSc2	čištění
opuštěných	opuštěný	k2eAgFnPc2d1	opuštěná
průmyslových	průmyslový	k2eAgFnPc2d1	průmyslová
zón	zóna	k1gFnPc2	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pozici	pozice	k1gFnSc6	pozice
náměstka	náměstka	k1gFnSc1	náměstka
ministryně	ministryně	k1gFnSc1	ministryně
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
pro	pro	k7c4	pro
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
vědu	věda	k1gFnSc4	věda
Bush	Bush	k1gMnSc1	Bush
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
Bennetta	Bennetta	k1gMnSc1	Bennetta
Raleyho	Raley	k1gMnSc2	Raley
<g/>
,	,	kIx,	,
známého	známý	k2eAgMnSc2d1	známý
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
snahu	snaha	k1gFnSc4	snaha
zrušit	zrušit	k5eAaPmF	zrušit
zákon	zákon	k1gInSc4	zákon
o	o	k7c6	o
ohrožených	ohrožený	k2eAgInPc6d1	ohrožený
druzích	druh	k1gInPc6	druh
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
kontroverzní	kontroverzní	k2eAgFnPc4d1	kontroverzní
náměstky	náměstka	k1gFnPc4	náměstka
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
patří	patřit	k5eAaImIp3nP	patřit
Lynn	Lynn	k1gInSc4	Lynn
Scarlettová	Scarlettová	k1gFnSc1	Scarlettová
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
svým	svůj	k3xOyFgInSc7	svůj
skeptickým	skeptický	k2eAgInSc7d1	skeptický
názorem	názor	k1gInSc7	názor
na	na	k7c4	na
globální	globální	k2eAgNnSc4d1	globální
oteplování	oteplování	k1gNnSc4	oteplování
a	a	k8xC	a
nesouhlasící	souhlasící	k2eNgNnSc4d1	nesouhlasící
se	s	k7c7	s
zpřísňováním	zpřísňování	k1gNnSc7	zpřísňování
ekologických	ekologický	k2eAgInPc2d1	ekologický
předpisů	předpis	k1gInPc2	předpis
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
zástupkyni	zástupkyně	k1gFnSc4	zástupkyně
ředitele	ředitel	k1gMnSc2	ředitel
Úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
Bush	Bush	k1gMnSc1	Bush
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
Lindy	Linda	k1gFnPc4	Linda
Fisherovou	Fisherová	k1gFnSc4	Fisherová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
předtím	předtím	k6eAd1	předtím
působila	působit	k5eAaImAgFnS	působit
jako	jako	k8xS	jako
ředitelka	ředitelka	k1gFnSc1	ředitelka
firmy	firma	k1gFnSc2	firma
Monsanto	Monsanta	k1gFnSc5	Monsanta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
nechvalně	chvalně	k6eNd1	chvalně
proslavila	proslavit	k5eAaPmAgFnS	proslavit
negativním	negativní	k2eAgInSc7d1	negativní
dopadem	dopad	k1gInSc7	dopad
svého	svůj	k3xOyFgInSc2	svůj
geneticky	geneticky	k6eAd1	geneticky
modifikovaného	modifikovaný	k2eAgInSc2d1	modifikovaný
růstového	růstový	k2eAgInSc2d1	růstový
hormonu	hormon	k1gInSc2	hormon
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
krmila	krmit	k5eAaImAgFnS	krmit
krávy	kráva	k1gFnSc2	kráva
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc4	jejichž
mléko	mléko	k1gNnSc4	mléko
prodávala	prodávat	k5eAaImAgFnS	prodávat
<g/>
,	,	kIx,	,
s	s	k7c7	s
dopadem	dopad	k1gInSc7	dopad
na	na	k7c4	na
zdraví	zdraví	k1gNnSc4	zdraví
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
ministryni	ministryně	k1gFnSc3	ministryně
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
Gale	Gale	k1gMnSc1	Gale
Nortonovou	Nortonový	k2eAgFnSc4d1	Nortonová
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
začala	začít	k5eAaPmAgFnS	začít
své	svůj	k3xOyFgNnSc4	svůj
působení	působení	k1gNnSc4	působení
předložením	předložení	k1gNnSc7	předložení
návrhu	návrh	k1gInSc2	návrh
na	na	k7c4	na
těžbu	těžba	k1gFnSc4	těžba
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
,	,	kIx,	,
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
plynu	plyn	k1gInSc2	plyn
v	v	k7c6	v
národních	národní	k2eAgInPc6d1	národní
parcích	park	k1gInPc6	park
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
návrh	návrh	k1gInSc1	návrh
ministryně	ministryně	k1gFnSc1	ministryně
-	-	kIx~	-
vydražení	vydražení	k1gNnSc1	vydražení
oblastí	oblast	k1gFnPc2	oblast
u	u	k7c2	u
východního	východní	k2eAgNnSc2d1	východní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Floridy	Florida	k1gFnSc2	Florida
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
těžit	těžit	k5eAaImF	těžit
tam	tam	k6eAd1	tam
ropu	ropa	k1gFnSc4	ropa
a	a	k8xC	a
zemní	zemní	k2eAgInSc4d1	zemní
plyn	plyn	k1gInSc4	plyn
Bush	Bush	k1gMnSc1	Bush
bez	bez	k7c2	bez
námitek	námitka	k1gFnPc2	námitka
schválil	schválit	k5eAaPmAgInS	schválit
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Bush	Bush	k1gMnSc1	Bush
podal	podat	k5eAaPmAgMnS	podat
podobný	podobný	k2eAgInSc4d1	podobný
návrh	návrh	k1gInSc4	návrh
-	-	kIx~	-
prodat	prodat	k5eAaPmF	prodat
pozemky	pozemek	k1gInPc4	pozemek
s	s	k7c7	s
ropou	ropa	k1gFnSc7	ropa
a	a	k8xC	a
plynem	plyn	k1gInSc7	plyn
v	v	k7c6	v
Aljašské	aljašský	k2eAgFnSc6d1	aljašská
přírodní	přírodní	k2eAgFnSc6d1	přírodní
rezervaci	rezervace	k1gFnSc6	rezervace
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
soukromých	soukromý	k2eAgFnPc2d1	soukromá
firem	firma	k1gFnPc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
povolit	povolit	k5eAaPmF	povolit
těžbu	těžba	k1gFnSc4	těžba
ropy	ropa	k1gFnSc2	ropa
v	v	k7c6	v
Lewisově	Lewisův	k2eAgInSc6d1	Lewisův
a	a	k8xC	a
Clarkově	Clarkův	k2eAgInSc6d1	Clarkův
národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
v	v	k7c6	v
Montaně	Montana	k1gFnSc6	Montana
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gInPc4	jeho
další	další	k2eAgInPc4d1	další
návrhy	návrh	k1gInPc4	návrh
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
podal	podat	k5eAaPmAgMnS	podat
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
převzetí	převzetí	k1gNnSc6	převzetí
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zmírnění	zmírnění	k1gNnSc1	zmírnění
kritérií	kritérion	k1gNnPc2	kritérion
povolovacího	povolovací	k2eAgNnSc2d1	povolovací
řízení	řízení	k1gNnSc2	řízení
pro	pro	k7c4	pro
výstavbu	výstavba	k1gFnSc4	výstavba
ropných	ropný	k2eAgFnPc2d1	ropná
rafinérií	rafinérie	k1gFnPc2	rafinérie
a	a	k8xC	a
přehrad	přehrada	k1gFnPc2	přehrada
s	s	k7c7	s
jadernými	jaderný	k2eAgFnPc7d1	jaderná
a	a	k8xC	a
vodními	vodní	k2eAgFnPc7d1	vodní
elektrárnami	elektrárna	k1gFnPc7	elektrárna
a	a	k8xC	a
snížení	snížení	k1gNnSc1	snížení
ekologických	ekologický	k2eAgFnPc2d1	ekologická
norem	norma	k1gFnPc2	norma
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
spjatých	spjatý	k2eAgFnPc2d1	spjatá
<g/>
.	.	kIx.	.
</s>
<s>
Dosavadní	dosavadní	k2eAgInPc1d1	dosavadní
předpisy	předpis	k1gInPc1	předpis
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
posilují	posilovat	k5eAaImIp3nP	posilovat
vládní	vládní	k2eAgFnSc4d1	vládní
pravomoc	pravomoc	k1gFnSc4	pravomoc
odmítnout	odmítnout	k5eAaPmF	odmítnout
přidělení	přidělení	k1gNnSc4	přidělení
zakázky	zakázka	k1gFnSc2	zakázka
firmě	firma	k1gFnSc3	firma
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
porušuje	porušovat	k5eAaImIp3nS	porušovat
federální	federální	k2eAgInPc4d1	federální
a	a	k8xC	a
ekologické	ekologický	k2eAgInPc4d1	ekologický
zákony	zákon	k1gInPc4	zákon
<g/>
,	,	kIx,	,
Bush	Bush	k1gMnSc1	Bush
zrušil	zrušit	k5eAaPmAgMnS	zrušit
během	během	k7c2	během
svých	svůj	k3xOyFgInPc2	svůj
prvních	první	k4xOgInPc2	první
100	[number]	k4	100
dní	den	k1gInPc2	den
v	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Bushe	Bush	k1gMnSc2	Bush
se	se	k3xPyFc4	se
ochrana	ochrana	k1gFnSc1	ochrana
přírody	příroda	k1gFnSc2	příroda
úzce	úzko	k6eAd1	úzko
prolíná	prolínat	k5eAaImIp3nS	prolínat
s	s	k7c7	s
výzkumem	výzkum	k1gInSc7	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokroku	pokrok	k1gInSc2	pokrok
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
jen	jen	k9	jen
novými	nový	k2eAgFnPc7d1	nová
čistějšími	čistý	k2eAgFnPc7d2	čistší
technologiemi	technologie	k1gFnPc7	technologie
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
zákonnými	zákonný	k2eAgInPc7d1	zákonný
příkazy	příkaz	k1gInPc7	příkaz
<g/>
.	.	kIx.	.
</s>
<s>
Snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
pomoci	pomoct	k5eAaPmF	pomoct
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
ekologické	ekologický	k2eAgFnSc2d1	ekologická
náhrady	náhrada	k1gFnSc2	náhrada
za	za	k7c4	za
ropné	ropný	k2eAgInPc4d1	ropný
produkty	produkt	k1gInPc4	produkt
v	v	k7c6	v
dopravě	doprava	k1gFnSc6	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Vyčlenil	vyčlenit	k5eAaPmAgInS	vyčlenit
proto	proto	k6eAd1	proto
1,2	[number]	k4	1,2
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
z	z	k7c2	z
federálního	federální	k2eAgInSc2d1	federální
rozpočtu	rozpočet	k1gInSc2	rozpočet
na	na	k7c6	na
období	období	k1gNnSc6	období
pěti	pět	k4xCc2	pět
let	léto	k1gNnPc2	léto
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
vodíkem	vodík	k1gInSc7	vodík
poháněných	poháněný	k2eAgNnPc2d1	poháněné
vozidel	vozidlo	k1gNnPc2	vozidlo
bez	bez	k7c2	bez
emisí	emise	k1gFnPc2	emise
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
podpořil	podpořit	k5eAaPmAgMnS	podpořit
výzkum	výzkum	k1gInSc4	výzkum
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
využití	využití	k1gNnSc2	využití
bionafty	bionafta	k1gFnSc2	bionafta
vznikající	vznikající	k2eAgFnSc2d1	vznikající
z	z	k7c2	z
odpadových	odpadový	k2eAgInPc2d1	odpadový
materiálů	materiál	k1gInPc2	materiál
a	a	k8xC	a
vývoj	vývoj	k1gInSc1	vývoj
kontroverzního	kontroverzní	k2eAgMnSc2d1	kontroverzní
"	"	kIx"	"
<g/>
zeleného	zelené	k1gNnSc2	zelené
benzínu	benzín	k1gInSc2	benzín
<g/>
"	"	kIx"	"
z	z	k7c2	z
kukuřice	kukuřice	k1gFnSc2	kukuřice
<g/>
,	,	kIx,	,
o	o	k7c4	o
které	který	k3yRgFnPc4	který
ekologové	ekolog	k1gMnPc1	ekolog
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
její	její	k3xOp3gNnSc4	její
vypěstování	vypěstování	k1gNnSc4	vypěstování
se	se	k3xPyFc4	se
spotřebují	spotřebovat	k5eAaPmIp3nP	spotřebovat
prostředky	prostředek	k1gInPc1	prostředek
vyžadující	vyžadující	k2eAgInPc1d1	vyžadující
více	hodně	k6eAd2	hodně
ropy	ropa	k1gFnPc4	ropa
<g/>
,	,	kIx,	,
než	než	k8xS	než
tato	tento	k3xDgFnSc1	tento
technologie	technologie	k1gFnSc1	technologie
dodá	dodat	k5eAaPmIp3nS	dodat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tohoto	tento	k3xDgInSc2	tento
programu	program	k1gInSc2	program
prosadil	prosadit	k5eAaPmAgInS	prosadit
i	i	k9	i
snížení	snížení	k1gNnSc4	snížení
daňového	daňový	k2eAgNnSc2d1	daňové
zatížení	zatížení	k1gNnSc2	zatížení
pro	pro	k7c4	pro
lidi	člověk	k1gMnPc4	člověk
používající	používající	k2eAgInPc1d1	používající
automobily	automobil	k1gInPc1	automobil
s	s	k7c7	s
nižšími	nízký	k2eAgFnPc7d2	nižší
emisemi	emise	k1gFnPc7	emise
<g/>
.	.	kIx.	.
</s>
<s>
Částku	částka	k1gFnSc4	částka
na	na	k7c4	na
výzkum	výzkum	k1gInSc4	výzkum
ekologičtějších	ekologický	k2eAgInPc2d2	ekologičtější
a	a	k8xC	a
účinnějších	účinný	k2eAgInPc2d2	účinnější
automobilů	automobil	k1gInPc2	automobil
a	a	k8xC	a
kamionů	kamion	k1gInPc2	kamion
Bush	Bush	k1gMnSc1	Bush
nicméně	nicméně	k8xC	nicméně
snížil	snížit	k5eAaPmAgInS	snížit
o	o	k7c4	o
28	[number]	k4	28
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
zrušil	zrušit	k5eAaPmAgInS	zrušit
termín	termín	k1gInSc1	termín
(	(	kIx(	(
<g/>
stanovený	stanovený	k2eAgInSc1d1	stanovený
na	na	k7c4	na
rok	rok	k1gInSc4	rok
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yRgInSc2	který
měly	mít	k5eAaImAgFnP	mít
automobilky	automobilka	k1gFnPc1	automobilka
vyrobit	vyrobit	k5eAaPmF	vyrobit
prototyp	prototyp	k1gInSc4	prototyp
úsporného	úsporný	k2eAgNnSc2d1	úsporné
vozidla	vozidlo	k1gNnSc2	vozidlo
s	s	k7c7	s
nízkou	nízký	k2eAgFnSc7d1	nízká
spotřebou	spotřeba	k1gFnSc7	spotřeba
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgNnPc4d1	další
Bushova	Bushův	k2eAgNnPc4d1	Bushovo
politická	politický	k2eAgNnPc4d1	politické
rozhodnutí	rozhodnutí	k1gNnPc4	rozhodnutí
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
stornování	stornování	k1gNnSc1	stornování
návrhu	návrh	k1gInSc2	návrh
na	na	k7c4	na
zlepšení	zlepšení	k1gNnSc4	zlepšení
přístupu	přístup	k1gInSc2	přístup
veřejnosti	veřejnost	k1gFnSc2	veřejnost
k	k	k7c3	k
informacím	informace	k1gFnPc3	informace
o	o	k7c6	o
potenciálním	potenciální	k2eAgInSc6d1	potenciální
dopadu	dopad	k1gInSc6	dopad
havárií	havárie	k1gFnPc2	havárie
chemických	chemický	k2eAgInPc2d1	chemický
provozů	provoz	k1gInPc2	provoz
<g/>
,	,	kIx,	,
pozdržení	pozdržení	k1gNnSc1	pozdržení
platnosti	platnost	k1gFnSc2	platnost
předpisů	předpis	k1gInPc2	předpis
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
snížit	snížit	k5eAaPmF	snížit
mezní	mezní	k2eAgFnSc4d1	mezní
úroveň	úroveň	k1gFnSc4	úroveň
arsenu	arsen	k1gInSc2	arsen
v	v	k7c6	v
pitné	pitný	k2eAgFnSc6d1	pitná
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
a	a	k8xC	a
snížení	snížení	k1gNnSc1	snížení
částky	částka	k1gFnSc2	částka
na	na	k7c4	na
výzkum	výzkum	k1gInSc4	výzkum
obnovitelných	obnovitelný	k2eAgInPc2d1	obnovitelný
zdrojů	zdroj	k1gInPc2	zdroj
na	na	k7c4	na
polovinu	polovina	k1gFnSc4	polovina
<g/>
.	.	kIx.	.
</s>
<s>
Rozpočet	rozpočet	k1gInSc1	rozpočet
Úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
snížil	snížit	k5eAaPmAgInS	snížit
o	o	k7c4	o
půl	půl	k1xP	půl
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Bush	Bush	k1gMnSc1	Bush
je	být	k5eAaImIp3nS	být
příznivcem	příznivec	k1gMnSc7	příznivec
"	"	kIx"	"
<g/>
hnutí	hnutí	k1gNnSc3	hnutí
pro-life	proif	k1gInSc5	pro-lif
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
ministra	ministr	k1gMnSc2	ministr
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
Johna	John	k1gMnSc4	John
Ashcrofta	Ashcroft	k1gMnSc4	Ashcroft
známého	známý	k1gMnSc4	známý
svým	svůj	k3xOyFgInSc7	svůj
nepřátelským	přátelský	k2eNgInSc7d1	nepřátelský
postojem	postoj	k1gInSc7	postoj
vůči	vůči	k7c3	vůči
interrupcím	interrupce	k1gFnPc3	interrupce
<g/>
.	.	kIx.	.
</s>
<s>
Zrušil	zrušit	k5eAaPmAgInS	zrušit
federální	federální	k2eAgFnSc4d1	federální
pomoc	pomoc	k1gFnSc4	pomoc
zahraničním	zahraniční	k2eAgNnSc7d1	zahraniční
sdružením	sdružení	k1gNnSc7	sdružení
nakloněným	nakloněný	k2eAgFnPc3d1	nakloněná
interrupcím	interrupce	k1gFnPc3	interrupce
a	a	k8xC	a
umělé	umělý	k2eAgFnSc3d1	umělá
antikoncepci	antikoncepce	k1gFnSc3	antikoncepce
<g/>
.	.	kIx.	.
</s>
<s>
Bush	Bush	k1gMnSc1	Bush
dále	daleko	k6eAd2	daleko
zrušil	zrušit	k5eAaPmAgMnS	zrušit
pomoc	pomoc	k1gFnSc4	pomoc
fondům	fond	k1gInPc3	fond
nabádajícím	nabádající	k2eAgFnPc3d1	nabádající
k	k	k7c3	k
používání	používání	k1gNnSc3	používání
kondomů	kondom	k1gInPc2	kondom
a	a	k8xC	a
pomáhajícím	pomáhající	k2eAgFnPc3d1	pomáhající
prostitutkám	prostitutka	k1gFnPc3	prostitutka
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
fondů	fond	k1gInPc2	fond
propagujících	propagující	k2eAgFnPc2d1	propagující
sexuální	sexuální	k2eAgFnSc4d1	sexuální
abstinenci	abstinence	k1gFnSc4	abstinence
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2002	[number]	k4	2002
Bush	Bush	k1gMnSc1	Bush
zrušil	zrušit	k5eAaPmAgMnS	zrušit
podporu	podpora	k1gFnSc4	podpora
Populačnímu	populační	k2eAgInSc3d1	populační
fondu	fond	k1gInSc3	fond
OSN	OSN	kA	OSN
(	(	kIx(	(
<g/>
UNFPA	UNFPA	kA	UNFPA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odůvodnil	odůvodnit	k5eAaPmAgInS	odůvodnit
to	ten	k3xDgNnSc1	ten
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
UNFPA	UNFPA	kA	UNFPA
podporuje	podporovat	k5eAaImIp3nS	podporovat
násilné	násilný	k2eAgInPc4d1	násilný
potraty	potrat	k1gInPc4	potrat
a	a	k8xC	a
sterilizace	sterilizace	k1gFnPc4	sterilizace
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Bush	Bush	k1gMnSc1	Bush
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
proti	proti	k7c3	proti
legálnímu	legální	k2eAgNnSc3d1	legální
uznání	uznání	k1gNnSc3	uznání
manželství	manželství	k1gNnSc2	manželství
osob	osoba	k1gFnPc2	osoba
stejného	stejný	k2eAgNnSc2d1	stejné
pohlaví	pohlaví	k1gNnSc2	pohlaví
a	a	k8xC	a
podporuje	podporovat	k5eAaImIp3nS	podporovat
zavedení	zavedení	k1gNnSc1	zavedení
civilního	civilní	k2eAgInSc2d1	civilní
svazku	svazek	k1gInSc2	svazek
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Nemyslím	myslet	k5eNaImIp1nS	myslet
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
bychom	by	kYmCp1nP	by
měli	mít	k5eAaImAgMnP	mít
popírat	popírat	k5eAaImF	popírat
lidské	lidský	k2eAgNnSc4d1	lidské
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
civilní	civilní	k2eAgInSc4d1	civilní
svazek	svazek	k1gInSc4	svazek
<g/>
,	,	kIx,	,
legálně	legálně	k6eAd1	legálně
ustanovený	ustanovený	k2eAgMnSc1d1	ustanovený
<g/>
"	"	kIx"	"
-	-	kIx~	-
ABC	ABC	kA	ABC
News	News	k1gInSc1	News
<g/>
,	,	kIx,	,
říjen	říjen	k1gInSc1	říjen
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podporuje	podporovat	k5eAaImIp3nS	podporovat
doplněk	doplněk	k1gInSc1	doplněk
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
federálním	federální	k2eAgNnSc6d1	federální
manželství	manželství	k1gNnSc6	manželství
(	(	kIx(	(
<g/>
Federal	Federal	k1gMnSc1	Federal
Marriage	Marriag	k1gFnSc2	Marriag
Amendment	Amendment	k1gMnSc1	Amendment
<g/>
)	)	kIx)	)
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
doplnit	doplnit	k5eAaPmF	doplnit
ústavu	ústava	k1gFnSc4	ústava
o	o	k7c4	o
tvrzení	tvrzení	k1gNnSc4	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
manželství	manželství	k1gNnSc1	manželství
je	být	k5eAaImIp3nS	být
svazek	svazek	k1gInSc4	svazek
jednoho	jeden	k4xCgMnSc2	jeden
muže	muž	k1gMnSc2	muž
a	a	k8xC	a
jedné	jeden	k4xCgFnSc2	jeden
ženy	žena	k1gFnSc2	žena
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Bushova	Bushův	k2eAgInSc2d1	Bushův
prvního	první	k4xOgNnSc2	první
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
se	se	k3xPyFc4	se
Michael	Michael	k1gMnSc1	Michael
E.	E.	kA	E.
Guest	Guest	k1gMnSc1	Guest
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
otevřeným	otevřený	k2eAgMnSc7d1	otevřený
homosexuálem	homosexuál	k1gMnSc7	homosexuál
<g/>
,	,	kIx,	,
kterého	který	k3yRgNnSc2	který
Senát	senát	k1gInSc1	senát
schválil	schválit	k5eAaPmAgInS	schválit
na	na	k7c4	na
post	post	k1gInSc4	post
velvyslance	velvyslanec	k1gMnSc2	velvyslanec
USA	USA	kA	USA
pro	pro	k7c4	pro
Rumunsko	Rumunsko	k1gNnSc4	Rumunsko
(	(	kIx(	(
<g/>
prvním	první	k4xOgMnSc6	první
otevřeným	otevřený	k2eAgNnSc7d1	otevřené
homosexuálem	homosexuál	k1gMnSc7	homosexuál
nominovaným	nominovaný	k2eAgMnSc7d1	nominovaný
na	na	k7c4	na
post	post	k1gInSc4	post
velvyslance	velvyslanec	k1gMnSc2	velvyslanec
byl	být	k5eAaImAgMnS	být
James	James	k1gMnSc1	James
Hormel	Hormel	k1gMnSc1	Hormel
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
Senát	senát	k1gInSc1	senát
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
George	George	k1gFnSc7	George
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
"	"	kIx"	"
<g/>
znovuzrozeného	znovuzrozený	k2eAgMnSc4d1	znovuzrozený
křesťana	křesťan	k1gMnSc4	křesťan
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
konverze	konverze	k1gFnSc1	konverze
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
reverenda	reverend	k1gMnSc2	reverend
Billyho	Billy	k1gMnSc2	Billy
Grahama	Graham	k1gMnSc2	Graham
<g/>
,	,	kIx,	,
neoficiálně	oficiálně	k6eNd1	oficiálně
nazývaného	nazývaný	k2eAgInSc2d1	nazývaný
"	"	kIx"	"
<g/>
evangelikální	evangelikální	k2eAgMnSc1d1	evangelikální
papež	papež	k1gMnSc1	papež
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
k	k	k7c3	k
"	"	kIx"	"
<g/>
modernímu	moderní	k2eAgInSc3d1	moderní
evangelismu	evangelismus	k1gInSc3	evangelismus
<g/>
"	"	kIx"	"
má	mít	k5eAaImIp3nS	mít
George	George	k1gFnPc2	George
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
asi	asi	k9	asi
nejblíž	blízce	k6eAd3	blízce
<g/>
.	.	kIx.	.
</s>
<s>
Bush	Bush	k1gMnSc1	Bush
se	se	k3xPyFc4	se
netají	tajit	k5eNaImIp3nS	tajit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vícekrát	vícekrát	k6eAd1	vícekrát
denně	denně	k6eAd1	denně
modlí	modlit	k5eAaImIp3nS	modlit
a	a	k8xC	a
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
projevech	projev	k1gInPc6	projev
cituje	citovat	k5eAaBmIp3nS	citovat
verše	verš	k1gInPc4	verš
z	z	k7c2	z
Bible	bible	k1gFnSc2	bible
nebo	nebo	k8xC	nebo
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
bible	bible	k1gFnSc2	bible
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc7	jeho
velkou	velký	k2eAgFnSc7d1	velká
vášní	vášeň	k1gFnSc7	vášeň
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
začal	začít	k5eAaPmAgInS	začít
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
návštěvami	návštěva	k1gFnPc7	návštěva
komunitní	komunitní	k2eAgFnSc2d1	komunitní
skupiny	skupina	k1gFnSc2	skupina
(	(	kIx(	(
<g/>
Community	Communita	k1gFnSc2	Communita
Bible	bible	k1gFnSc2	bible
Study	stud	k1gInPc1	stud
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
věnující	věnující	k2eAgFnSc4d1	věnující
se	se	k3xPyFc4	se
katechezi	katecheze	k1gFnSc4	katecheze
a	a	k8xC	a
výkladem	výklad	k1gInSc7	výklad
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
Bible	bible	k1gFnSc2	bible
zavedl	zavést	k5eAaPmAgInS	zavést
i	i	k9	i
v	v	k7c6	v
Bílém	bílý	k2eAgInSc6d1	bílý
domě	dům	k1gInSc6	dům
<g/>
.	.	kIx.	.
</s>
<s>
Několikrát	několikrát	k6eAd1	několikrát
se	se	k3xPyFc4	se
v	v	k7c6	v
projevech	projev	k1gInPc6	projev
zmiňoval	zmiňovat	k5eAaImAgMnS	zmiňovat
i	i	k9	i
o	o	k7c6	o
Koránu	korán	k1gInSc6	korán
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
z	z	k7c2	z
něj	on	k3xPp3gMnSc4	on
citoval	citovat	k5eAaBmAgMnS	citovat
nebo	nebo	k8xC	nebo
ho	on	k3xPp3gMnSc4	on
dokonce	dokonce	k9	dokonce
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c2	za
slova	slovo	k1gNnSc2	slovo
pocházející	pocházející	k2eAgFnSc1d1	pocházející
od	od	k7c2	od
Boha	bůh	k1gMnSc2	bůh
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
[	[	kIx(	[
<g/>
7	[number]	k4	7
<g/>
]	]	kIx)	]
K	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
vítězství	vítězství	k1gNnSc3	vítězství
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
přispělo	přispět	k5eAaPmAgNnS	přispět
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokázal	dokázat	k5eAaPmAgMnS	dokázat
na	na	k7c4	na
svoji	svůj	k3xOyFgFnSc4	svůj
stranu	strana	k1gFnSc4	strana
přivést	přivést	k5eAaPmF	přivést
široké	široký	k2eAgFnPc4d1	široká
masy	masa	k1gFnPc4	masa
evangelikálů	evangelikál	k1gMnPc2	evangelikál
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
silně	silně	k6eAd1	silně
křesťansky	křesťansky	k6eAd1	křesťansky
orientovaných	orientovaný	k2eAgMnPc2d1	orientovaný
voličů	volič	k1gMnPc2	volič
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
podle	podle	k7c2	podle
průzkumů	průzkum	k1gInPc2	průzkum
získal	získat	k5eAaPmAgMnS	získat
dokonce	dokonce	k9	dokonce
většinu	většina	k1gFnSc4	většina
hlasů	hlas	k1gInPc2	hlas
katolických	katolický	k2eAgMnPc2d1	katolický
voličů	volič	k1gMnPc2	volič
<g/>
,	,	kIx,	,
když	když	k8xS	když
získal	získat	k5eAaPmAgMnS	získat
mírně	mírně	k6eAd1	mírně
přes	přes	k7c4	přes
50	[number]	k4	50
%	%	kIx~	%
"	"	kIx"	"
<g/>
katolických	katolický	k2eAgInPc2d1	katolický
hlasů	hlas	k1gInPc2	hlas
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
jeho	jeho	k3xOp3gMnSc7	jeho
protivníkem	protivník	k1gMnSc7	protivník
byl	být	k5eAaImAgMnS	být
katolík	katolík	k1gMnSc1	katolík
John	John	k1gMnSc1	John
Kerry	Kerra	k1gFnSc2	Kerra
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
oslovit	oslovit	k5eAaPmF	oslovit
své	svůj	k3xOyFgMnPc4	svůj
souvěrce	souvěrec	k1gMnPc4	souvěrec
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
pozice	pozice	k1gFnSc2	pozice
(	(	kIx(	(
<g/>
Kerry	Kerra	k1gFnPc1	Kerra
se	se	k3xPyFc4	se
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
mnohých	mnohé	k1gNnPc2	mnohé
svých	svůj	k3xOyFgMnPc2	svůj
souvěrců	souvěrec	k1gMnPc2	souvěrec
zdiskreditoval	zdiskreditovat	k5eAaPmAgMnS	zdiskreditovat
svou	svůj	k3xOyFgFnSc7	svůj
otevřenou	otevřený	k2eAgFnSc7d1	otevřená
podporou	podpora	k1gFnSc7	podpora
práva	právo	k1gNnSc2	právo
ženy	žena	k1gFnSc2	žena
na	na	k7c4	na
potrat	potrat	k1gInSc4	potrat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
náboženských	náboženský	k2eAgInPc2d1	náboženský
důvodů	důvod	k1gInPc2	důvod
se	se	k3xPyFc4	se
Georg	Georg	k1gInSc1	Georg
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
staví	stavit	k5eAaImIp3nS	stavit
proti	proti	k7c3	proti
eutanázii	eutanázie	k1gFnSc3	eutanázie
<g/>
,	,	kIx,	,
výzkumu	výzkum	k1gInSc3	výzkum
buněk	buňka	k1gFnPc2	buňka
z	z	k7c2	z
lidských	lidský	k2eAgNnPc2d1	lidské
embryí	embryo	k1gNnPc2	embryo
<g/>
,	,	kIx,	,
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
nepřátelský	přátelský	k2eNgInSc4d1	nepřátelský
postoj	postoj	k1gInSc4	postoj
proti	proti	k7c3	proti
interrupcím	interrupce	k1gFnPc3	interrupce
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
formálně	formálně	k6eAd1	formálně
proti	proti	k7c3	proti
manželství	manželství	k1gNnSc1	manželství
dvou	dva	k4xCgFnPc2	dva
osob	osoba	k1gFnPc2	osoba
téhož	týž	k3xTgInSc2	týž
pohlaví	pohlaví	k1gNnPc4	pohlaví
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2005	[number]	k4	2005
se	se	k3xPyFc4	se
George	George	k1gNnSc2	George
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
úřadujícím	úřadující	k2eAgMnSc7d1	úřadující
americkým	americký	k2eAgMnSc7d1	americký
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
osobně	osobně	k6eAd1	osobně
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
pohřbu	pohřeb	k1gInSc2	pohřeb
papeže	papež	k1gMnSc2	papež
<g/>
.	.	kIx.	.
</s>
<s>
Doprovázeli	doprovázet	k5eAaImAgMnP	doprovázet
jej	on	k3xPp3gNnSc4	on
jeho	jeho	k3xOp3gMnPc1	jeho
předchůdci	předchůdce	k1gMnPc1	předchůdce
Bill	Bill	k1gMnSc1	Bill
Clinton	Clinton	k1gMnSc1	Clinton
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
George	Georg	k1gMnSc2	Georg
H.	H.	kA	H.
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
začal	začít	k5eAaPmAgMnS	začít
Bush	Bush	k1gMnSc1	Bush
pracovat	pracovat	k5eAaImF	pracovat
s	s	k7c7	s
republikány	republikán	k1gMnPc7	republikán
v	v	k7c6	v
Kongresu	kongres	k1gInSc6	kongres
na	na	k7c6	na
schválení	schválení	k1gNnSc6	schválení
legislativy	legislativa	k1gFnSc2	legislativa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
změnila	změnit	k5eAaPmAgFnS	změnit
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInSc7	jaký
federální	federální	k2eAgFnSc1d1	federální
vláda	vláda	k1gFnSc1	vláda
regulovala	regulovat	k5eAaImAgFnS	regulovat
<g/>
,	,	kIx,	,
zdaňovala	zdaňovat	k5eAaImAgFnS	zdaňovat
a	a	k8xC	a
podporovala	podporovat	k5eAaImAgFnS	podporovat
neziskové	ziskový	k2eNgFnPc4d1	nezisková
organizace	organizace	k1gFnPc4	organizace
řízené	řízený	k2eAgInPc1d1	řízený
náboženskými	náboženský	k2eAgFnPc7d1	náboženská
organizacemi	organizace	k1gFnPc7	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Byť	byť	k8xS	byť
prvořadou	prvořadý	k2eAgFnSc7d1	prvořadá
snahou	snaha	k1gFnSc7	snaha
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
této	tento	k3xDgFnSc2	tento
legislativy	legislativa	k1gFnSc2	legislativa
byla	být	k5eAaImAgFnS	být
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
tyto	tento	k3xDgFnPc1	tento
organizace	organizace	k1gFnPc1	organizace
mohly	moct	k5eAaImAgFnP	moct
čerpat	čerpat	k5eAaImF	čerpat
federální	federální	k2eAgFnSc4d1	federální
podporu	podpora	k1gFnSc4	podpora
<g/>
,	,	kIx,	,
další	další	k2eAgInSc4d1	další
zákon	zákon	k1gInSc4	zákon
přinesl	přinést	k5eAaPmAgInS	přinést
požadavek	požadavek	k1gInSc1	požadavek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
oddělily	oddělit	k5eAaPmAgInP	oddělit
svoje	svůj	k3xOyFgFnPc4	svůj
charitativní	charitativní	k2eAgFnPc4d1	charitativní
funkce	funkce	k1gFnPc4	funkce
od	od	k7c2	od
náboženských	náboženský	k2eAgFnPc2d1	náboženská
<g/>
.	.	kIx.	.
</s>
<s>
Bush	Bush	k1gMnSc1	Bush
též	též	k9	též
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
Úřad	úřad	k1gInSc1	úřad
Bílého	bílý	k2eAgInSc2d1	bílý
domu	dům	k1gInSc2	dům
pro	pro	k7c4	pro
komunitní	komunitní	k2eAgNnPc4d1	komunitní
a	a	k8xC	a
na	na	k7c6	na
víře	víra	k1gFnSc6	víra
založené	založený	k2eAgFnSc2d1	založená
iniciativy	iniciativa	k1gFnSc2	iniciativa
(	(	kIx(	(
<g/>
en	en	k?	en
<g/>
:	:	kIx,	:
<g/>
White	Whit	k1gInSc5	Whit
House	house	k1gNnSc1	house
Office	Office	kA	Office
of	of	k?	of
Faith-Based	Faith-Based	k1gInSc1	Faith-Based
and	and	k?	and
Community	Communita	k1gFnSc2	Communita
Initiatives	Initiativesa	k1gFnPc2	Initiativesa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
organizace	organizace	k1gFnPc1	organizace
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
Unie	unie	k1gFnSc1	unie
amerických	americký	k2eAgFnPc2d1	americká
občanských	občanský	k2eAgFnPc2d1	občanská
svobod	svoboda	k1gFnPc2	svoboda
(	(	kIx(	(
<g/>
en	en	k?	en
<g/>
:	:	kIx,	:
<g/>
American	American	k1gMnSc1	American
Civil	civil	k1gMnSc1	civil
Liberties	Liberties	k1gMnSc1	Liberties
Union	union	k1gInSc1	union
<g/>
)	)	kIx)	)
kritizovaly	kritizovat	k5eAaImAgFnP	kritizovat
tento	tento	k3xDgInSc4	tento
Bushův	Bushův	k2eAgInSc4d1	Bushův
program	program	k1gInSc4	program
<g/>
.	.	kIx.	.
</s>
<s>
Bush	Bush	k1gMnSc1	Bush
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
imigrační	imigrační	k2eAgInSc4d1	imigrační
zákon	zákon	k1gInSc4	zákon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
podstatně	podstatně	k6eAd1	podstatně
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
použití	použití	k1gNnSc4	použití
cizineckých	cizinecký	k2eAgNnPc2d1	cizinecké
pracovních	pracovní	k2eAgNnPc2d1	pracovní
víz	vízo	k1gNnPc2	vízo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
návrh	návrh	k1gInSc1	návrh
by	by	kYmCp3nS	by
zrovnoprávnil	zrovnoprávnit	k5eAaPmAgInS	zrovnoprávnit
zahraniční	zahraniční	k2eAgMnPc4d1	zahraniční
pracovníky	pracovník	k1gMnPc4	pracovník
<g/>
,	,	kIx,	,
i	i	k8xC	i
kdyby	kdyby	kYmCp3nP	kdyby
tito	tento	k3xDgMnPc1	tento
neměli	mít	k5eNaImAgMnP	mít
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
trvalý	trvalý	k2eAgInSc4d1	trvalý
pobyt	pobyt	k1gInSc4	pobyt
nebo	nebo	k8xC	nebo
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
proti	proti	k7c3	proti
amnestii	amnestie	k1gFnSc3	amnestie
pro	pro	k7c4	pro
ilegální	ilegální	k2eAgMnPc4d1	ilegální
přistěhovalce	přistěhovalec	k1gMnPc4	přistěhovalec
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
počet	počet	k1gInSc1	počet
se	se	k3xPyFc4	se
v	v	k7c6	v
USA	USA	kA	USA
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
15	[number]	k4	15
miliónů	milión	k4xCgInPc2	milión
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
Bush	Bush	k1gMnSc1	Bush
tlačil	tlačit	k5eAaImAgMnS	tlačit
na	na	k7c4	na
Trenta	Trent	k1gMnSc4	Trent
Lotta	Lott	k1gMnSc4	Lott
<g/>
,	,	kIx,	,
republikánského	republikánský	k2eAgMnSc4d1	republikánský
vůdce	vůdce	k1gMnSc4	vůdce
Senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
podal	podat	k5eAaPmAgMnS	podat
demisi	demise	k1gFnSc4	demise
kvůli	kvůli	k7c3	kvůli
nostalgickým	nostalgický	k2eAgFnPc3d1	nostalgická
poznámkám	poznámka	k1gFnPc3	poznámka
týkajícím	týkající	k2eAgFnPc3d1	týkající
se	se	k3xPyFc4	se
národnostní	národnostní	k2eAgFnSc2d1	národnostní
segregace	segregace	k1gFnSc2	segregace
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
pronesl	pronést	k5eAaPmAgMnS	pronést
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
vzpomínky	vzpomínka	k1gFnSc2	vzpomínka
na	na	k7c4	na
bývalého	bývalý	k2eAgMnSc4d1	bývalý
kandidáta	kandidát	k1gMnSc4	kandidát
prezidentských	prezidentský	k2eAgFnPc2d1	prezidentská
voleb	volba	k1gFnPc2	volba
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
senátora	senátor	k1gMnSc2	senátor
Stroma	stroma	k1gNnSc2	stroma
Thurmonda	Thurmond	k1gMnSc2	Thurmond
<g/>
.	.	kIx.	.
</s>
<s>
Bush	Bush	k1gMnSc1	Bush
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
i	i	k9	i
ve	v	k7c4	v
zdravotnictví	zdravotnictví	k1gNnSc4	zdravotnictví
přístupy	přístup	k1gInPc4	přístup
založené	založený	k2eAgInPc4d1	založený
na	na	k7c6	na
konkurenci	konkurence	k1gFnSc6	konkurence
a	a	k8xC	a
volném	volný	k2eAgInSc6d1	volný
trhu	trh	k1gInSc6	trh
(	(	kIx(	(
<g/>
osobní	osobní	k2eAgInPc4d1	osobní
účty	účet	k1gInPc4	účet
na	na	k7c4	na
zdravotní	zdravotní	k2eAgNnSc4d1	zdravotní
spoření	spoření	k1gNnSc4	spoření
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
zákonem	zákon	k1gInSc7	zákon
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yIgInSc4	který
se	se	k3xPyFc4	se
zaměřil	zaměřit	k5eAaPmAgMnS	zaměřit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Medicare	Medicar	k1gMnSc5	Medicar
Act	Act	k1gFnSc4	Act
of	of	k?	of
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
8	[number]	k4	8
<g/>
]	]	kIx)	]
Tento	tento	k3xDgInSc1	tento
zákon	zákon	k1gInSc1	zákon
představuje	představovat	k5eAaImIp3nS	představovat
největší	veliký	k2eAgFnSc4d3	veliký
změnu	změna	k1gFnSc4	změna
programu	program	k1gInSc2	program
Medicare	Medicar	k1gMnSc5	Medicar
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
existenci	existence	k1gFnSc4	existence
(	(	kIx(	(
<g/>
běží	běžet	k5eAaImIp3nS	běžet
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgNnSc6	první
desetileti	desetile	k1gNnSc6	desetile
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
změnám	změna	k1gFnPc3	změna
do	do	k7c2	do
programu	program	k1gInSc2	program
vloží	vložit	k5eAaPmIp3nS	vložit
o	o	k7c4	o
395	[number]	k4	395
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
bylo	být	k5eAaImAgNnS	být
plánováno	plánovat	k5eAaImNgNnS	plánovat
<g/>
.	.	kIx.	.
</s>
<s>
Schválení	schválení	k1gNnSc1	schválení
tohoto	tento	k3xDgInSc2	tento
zákona	zákon	k1gInSc2	zákon
předcházely	předcházet	k5eAaImAgFnP	předcházet
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
diskuse	diskuse	k1gFnPc1	diskuse
a	a	k8xC	a
Kongresem	kongres	k1gInSc7	kongres
prošel	projít	k5eAaPmAgInS	projít
až	až	k9	až
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
souhlasila	souhlasit	k5eAaImAgFnS	souhlasit
i	i	k9	i
Americká	americký	k2eAgFnSc1d1	americká
asociace	asociace	k1gFnSc1	asociace
důchodců	důchodce	k1gMnPc2	důchodce
(	(	kIx(	(
<g/>
American	American	k1gInSc1	American
Association	Association	k1gInSc1	Association
of	of	k?	of
Retired	Retired	k1gInSc1	Retired
Persons	Persons	k1gInSc1	Persons
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejdůležitější	důležitý	k2eAgFnSc4d3	nejdůležitější
změnu	změna	k1gFnSc4	změna
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
zavedení	zavedení	k1gNnSc4	zavedení
speciálních	speciální	k2eAgInPc2d1	speciální
nezdaňovaných	zdaňovaný	k2eNgInPc2d1	nezdaňovaný
osobních	osobní	k2eAgInPc2d1	osobní
účtů	účet	k1gInPc2	účet
pro	pro	k7c4	pro
spoření	spoření	k1gNnSc4	spoření
na	na	k7c4	na
zdravotní	zdravotní	k2eAgInPc4d1	zdravotní
výdaje	výdaj	k1gInPc4	výdaj
pro	pro	k7c4	pro
pracující	pracující	k2eAgFnSc4d1	pracující
populaci	populace	k1gFnSc4	populace
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Health	Health	k1gInSc1	Health
Savings	Savings	k1gInSc1	Savings
Accounts	Accounts	k1gInSc1	Accounts
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
dále	daleko	k6eAd2	daleko
přisuzuje	přisuzovat	k5eAaImIp3nS	přisuzovat
příspěvky	příspěvek	k1gInPc4	příspěvek
a	a	k8xC	a
daňové	daňový	k2eAgFnPc4d1	daňová
úlevy	úleva	k1gFnPc4	úleva
zaměstnavatelům	zaměstnavatel	k1gMnPc3	zaměstnavatel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
přispívat	přispívat	k5eAaImF	přispívat
svým	svůj	k3xOyFgMnPc3	svůj
zaměstnancům	zaměstnanec	k1gMnPc3	zaměstnanec
na	na	k7c4	na
zdravotní	zdravotní	k2eAgNnSc4d1	zdravotní
pojištění	pojištění	k1gNnSc4	pojištění
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
bude	být	k5eAaImBp3nS	být
možná	možná	k6eAd1	možná
částečná	částečný	k2eAgFnSc1d1	částečná
privatizace	privatizace	k1gFnSc1	privatizace
tohoto	tento	k3xDgInSc2	tento
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
již	již	k6eAd1	již
nepracující	pracující	k2eNgMnSc1d1	nepracující
(	(	kIx(	(
<g/>
důchodci	důchodce	k1gMnPc1	důchodce
<g/>
,	,	kIx,	,
invalidé	invalid	k1gMnPc1	invalid
<g/>
)	)	kIx)	)
zákon	zákon	k1gInSc1	zákon
finančně	finančně	k6eAd1	finančně
zpřístupnil	zpřístupnit	k5eAaPmAgInS	zpřístupnit
léky	lék	k1gInPc4	lék
na	na	k7c4	na
předpis	předpis	k1gInSc4	předpis
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
se	se	k3xPyFc4	se
speciálně	speciálně	k6eAd1	speciálně
zabývá	zabývat	k5eAaImIp3nS	zabývat
zdravotnictvím	zdravotnictví	k1gNnSc7	zdravotnictví
mimo	mimo	k7c4	mimo
města	město	k1gNnPc4	město
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zvýšit	zvýšit	k5eAaPmF	zvýšit
zdravotnickou	zdravotnický	k2eAgFnSc4d1	zdravotnická
dostupnost	dostupnost	k1gFnSc4	dostupnost
<g/>
.	.	kIx.	.
</s>
<s>
Stanoví	stanovit	k5eAaPmIp3nS	stanovit
speciální	speciální	k2eAgInSc4d1	speciální
bonus	bonus	k1gInSc4	bonus
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
25	[number]	k4	25
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
rozdělit	rozdělit	k5eAaPmF	rozdělit
mezi	mezi	k7c4	mezi
nemocnice	nemocnice	k1gFnPc4	nemocnice
v	v	k7c6	v
menších	malý	k2eAgNnPc6d2	menší
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
propočtů	propočet	k1gInPc2	propočet
zákon	zákon	k1gInSc4	zákon
pomocí	pomocí	k7c2	pomocí
nových	nový	k2eAgNnPc2d1	nové
komunitních	komunitní	k2eAgNnPc2d1	komunitní
zdravotních	zdravotní	k2eAgNnPc2d1	zdravotní
center	centrum	k1gNnPc2	centrum
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
lepší	dobrý	k2eAgFnPc4d2	lepší
zdravotní	zdravotní	k2eAgFnPc4d1	zdravotní
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
asi	asi	k9	asi
3	[number]	k4	3
miliony	milion	k4xCgInPc1	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
příslušníků	příslušník	k1gMnPc2	příslušník
bohatších	bohatý	k2eAgFnPc2d2	bohatší
vrstev	vrstva	k1gFnPc2	vrstva
požaduje	požadovat	k5eAaImIp3nS	požadovat
mírně	mírně	k6eAd1	mírně
vyšší	vysoký	k2eAgInPc4d2	vyšší
příspěvky	příspěvek	k1gInPc4	příspěvek
<g/>
,	,	kIx,	,
než	než	k8xS	než
byly	být	k5eAaImAgInP	být
před	před	k7c7	před
jeho	jeho	k3xOp3gNnSc7	jeho
zavedením	zavedení	k1gNnSc7	zavedení
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc4d1	velký
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
soukromými	soukromý	k2eAgFnPc7d1	soukromá
a	a	k8xC	a
státními	státní	k2eAgFnPc7d1	státní
školami	škola	k1gFnPc7	škola
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
například	například	k6eAd1	například
nejsou	být	k5eNaImIp3nP	být
státní	státní	k2eAgFnPc4d1	státní
školní	školní	k2eAgFnPc4d1	školní
knihovny	knihovna	k1gFnPc4	knihovna
financovány	financován	k2eAgFnPc4d1	financována
přímo	přímo	k6eAd1	přímo
ale	ale	k8xC	ale
přes	přes	k7c4	přes
tzv.	tzv.	kA	tzv.
grantové	grantový	k2eAgInPc4d1	grantový
balíčky	balíček	k1gInPc4	balíček
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
státy	stát	k1gInPc1	stát
mohou	moct	k5eAaImIp3nP	moct
rozdělit	rozdělit	k5eAaPmF	rozdělit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
uznají	uznat	k5eAaPmIp3nP	uznat
za	za	k7c4	za
vhodné	vhodný	k2eAgNnSc4d1	vhodné
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
nikdy	nikdy	k6eAd1	nikdy
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
do	do	k7c2	do
knihoven	knihovna	k1gFnPc2	knihovna
investovány	investován	k2eAgFnPc4d1	investována
v	v	k7c6	v
plné	plný	k2eAgFnSc6d1	plná
výši	výše	k1gFnSc6	výše
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
stav	stav	k1gInSc4	stav
učebnic	učebnice	k1gFnPc2	učebnice
a	a	k8xC	a
ostatních	ostatní	k2eAgFnPc2d1	ostatní
pomůcek	pomůcka	k1gFnPc2	pomůcka
tristní	tristní	k2eAgFnPc1d1	tristní
-	-	kIx~	-
nezřídka	nezřídka	k6eAd1	nezřídka
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
konce	konec	k1gInSc2	konec
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
prvních	první	k4xOgInPc2	první
100	[number]	k4	100
dnů	den	k1gInPc2	den
v	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
George	Georg	k1gFnSc2	Georg
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
výdaje	výdaj	k1gInSc2	výdaj
na	na	k7c4	na
státní	státní	k2eAgFnPc4d1	státní
knihovny	knihovna	k1gFnPc4	knihovna
snížil	snížit	k5eAaPmAgMnS	snížit
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
o	o	k7c4	o
39	[number]	k4	39
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dalších	další	k2eAgFnPc2d1	další
35	[number]	k4	35
milionů	milion	k4xCgInPc2	milion
například	například	k6eAd1	například
zkrátil	zkrátit	k5eAaPmAgMnS	zkrátit
částku	částka	k1gFnSc4	částka
na	na	k7c4	na
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
pediatrů	pediatr	k1gMnPc2	pediatr
<g/>
.	.	kIx.	.
</s>
<s>
Odepřel	odepřít	k5eAaPmAgInS	odepřít
vysokoškolskou	vysokoškolský	k2eAgFnSc4d1	vysokoškolská
finanční	finanční	k2eAgFnSc4d1	finanční
pomoc	pomoc	k1gFnSc4	pomoc
studentům	student	k1gMnPc3	student
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
odsouzeni	odsouzet	k5eAaImNgMnP	odsouzet
za	za	k7c4	za
přestupky	přestupek	k1gInPc4	přestupek
související	související	k2eAgInPc4d1	související
s	s	k7c7	s
drogami	droga	k1gFnPc7	droga
(	(	kIx(	(
<g/>
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
odsouzení	odsouzený	k2eAgMnPc1d1	odsouzený
vrahové	vrah	k1gMnPc1	vrah
stipendia	stipendium	k1gNnSc2	stipendium
čerpat	čerpat	k5eAaImF	čerpat
mohou	moct	k5eAaImIp3nP	moct
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
zrušení	zrušení	k1gNnSc4	zrušení
programu	program	k1gInSc2	program
"	"	kIx"	"
<g/>
Četba	četba	k1gFnSc1	četba
-	-	kIx~	-
zásadní	zásadní	k2eAgFnSc1d1	zásadní
nutnost	nutnost	k1gFnSc1	nutnost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
přiděloval	přidělovat	k5eAaImAgMnS	přidělovat
knihy	kniha	k1gFnSc2	kniha
zdarma	zdarma	k6eAd1	zdarma
chudým	chudý	k2eAgFnPc3d1	chudá
dětem	dítě	k1gFnPc3	dítě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2002	[number]	k4	2002
Bush	Bush	k1gMnSc1	Bush
podepsal	podepsat	k5eAaPmAgMnS	podepsat
program	program	k1gInSc4	program
No	no	k9	no
Child	Child	k1gMnSc1	Child
Left	Left	k1gMnSc1	Left
Behind	Behind	k1gMnSc1	Behind
(	(	kIx(	(
<g/>
Nezapomenout	zapomenout	k5eNaPmF	zapomenout
na	na	k7c4	na
žádné	žádný	k3yNgNnSc4	žádný
dítě	dítě	k1gNnSc4	dítě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
je	být	k5eAaImIp3nS	být
zaměřený	zaměřený	k2eAgInSc1d1	zaměřený
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
nejnižšího	nízký	k2eAgNnSc2d3	nejnižší
školství	školství	k1gNnSc2	školství
a	a	k8xC	a
zabezpečuje	zabezpečovat	k5eAaImIp3nS	zabezpečovat
více	hodně	k6eAd2	hodně
zdrojů	zdroj	k1gInPc2	zdroj
pro	pro	k7c4	pro
školy	škola	k1gFnPc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Kritici	kritik	k1gMnPc1	kritik
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
školy	škola	k1gFnPc1	škola
nedostaly	dostat	k5eNaPmAgFnP	dostat
dost	dost	k6eAd1	dost
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohly	moct	k5eAaImAgInP	moct
splnit	splnit	k5eAaPmF	splnit
cíle	cíl	k1gInPc1	cíl
stanovené	stanovený	k2eAgInPc1d1	stanovený
programem	program	k1gInSc7	program
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Výboru	výbor	k1gInSc2	výbor
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
pro	pro	k7c4	pro
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
a	a	k8xC	a
práci	práce	k1gFnSc4	práce
(	(	kIx(	(
<g/>
House	house	k1gNnSc1	house
Committee	Committe	k1gFnSc2	Committe
on	on	k3xPp3gInSc1	on
Education	Education	k1gInSc1	Education
and	and	k?	and
the	the	k?	the
Workforce	Workforka	k1gFnSc6	Workforka
<g/>
)	)	kIx)	)
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2003	[number]	k4	2003
se	se	k3xPyFc4	se
během	během	k7c2	během
prvních	první	k4xOgInPc2	první
tří	tři	k4xCgInPc2	tři
let	léto	k1gNnPc2	léto
Bushovy	Bushův	k2eAgFnSc2d1	Bushova
vlády	vláda	k1gFnSc2	vláda
rozpočet	rozpočet	k1gInSc4	rozpočet
pro	pro	k7c4	pro
školství	školství	k1gNnPc4	školství
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
o	o	k7c4	o
13,2	[number]	k4	13,2
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Bush	Bush	k1gMnSc1	Bush
stanovil	stanovit	k5eAaPmAgMnS	stanovit
omezení	omezení	k1gNnSc4	omezení
podpory	podpora	k1gFnSc2	podpora
výzkumu	výzkum	k1gInSc2	výzkum
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Financování	financování	k1gNnSc1	financování
tohoto	tento	k3xDgInSc2	tento
výzkumu	výzkum	k1gInSc2	výzkum
federální	federální	k2eAgFnSc7d1	federální
vládou	vláda	k1gFnSc7	vláda
schválil	schválit	k5eAaPmAgMnS	schválit
jeho	jeho	k3xOp3gMnSc1	jeho
předchůdce	předchůdce	k1gMnSc1	předchůdce
Clinton	Clinton	k1gMnSc1	Clinton
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
peníze	peníz	k1gInPc1	peníz
nebyly	být	k5eNaImAgInP	být
přiděleny	přidělit	k5eAaPmNgInP	přidělit
žádnému	žádný	k3yNgInSc3	žádný
programu	program	k1gInSc3	program
až	až	k6eAd1	až
do	do	k7c2	do
vytvoření	vytvoření	k1gNnSc2	vytvoření
rozpočtových	rozpočtový	k2eAgNnPc2d1	rozpočtové
pravidel	pravidlo	k1gNnPc2	pravidlo
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
již	již	k6eAd1	již
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
prvním	první	k4xOgNnSc7	první
rozdělením	rozdělení	k1gNnSc7	rozdělení
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
Bush	Bush	k1gMnSc1	Bush
ohlásil	ohlásit	k5eAaPmAgMnS	ohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozpočtová	rozpočtový	k2eAgNnPc1d1	rozpočtové
pravidla	pravidlo	k1gNnPc1	pravidlo
budou	být	k5eAaImBp3nP	být
změněna	změnit	k5eAaPmNgNnP	změnit
tak	tak	k9	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
umožnila	umožnit	k5eAaPmAgFnS	umožnit
podporovat	podporovat	k5eAaImF	podporovat
jen	jen	k9	jen
výzkum	výzkum	k1gInSc4	výzkum
na	na	k7c6	na
již	již	k6eAd1	již
existujících	existující	k2eAgFnPc6d1	existující
kmenových	kmenový	k2eAgFnPc6d1	kmenová
buňkách	buňka	k1gFnPc6	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
Bush	Bush	k1gMnSc1	Bush
podepsal	podepsat	k5eAaPmAgMnS	podepsat
zákon	zákon	k1gInSc4	zákon
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zabezpečil	zabezpečit	k5eAaPmAgMnS	zabezpečit
zdvojnásobení	zdvojnásobení	k1gNnSc4	zdvojnásobení
finančních	finanční	k2eAgInPc2d1	finanční
prostředků	prostředek	k1gInPc2	prostředek
pro	pro	k7c4	pro
National	National	k1gFnSc4	National
Science	Science	k1gFnSc2	Science
Foundation	Foundation	k1gInSc1	Foundation
(	(	kIx(	(
<g/>
Nadace	nadace	k1gFnSc1	nadace
národní	národní	k2eAgFnSc2d1	národní
vědy	věda	k1gFnSc2	věda
<g/>
)	)	kIx)	)
v	v	k7c6	v
příštích	příští	k2eAgNnPc6d1	příští
5	[number]	k4	5
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
vytvořit	vytvořit	k5eAaPmF	vytvořit
nové	nový	k2eAgInPc4d1	nový
vzdělávací	vzdělávací	k2eAgInPc4d1	vzdělávací
postupy	postup	k1gInPc4	postup
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
a	a	k8xC	a
vědě	věda	k1gFnSc6	věda
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
na	na	k7c6	na
základních	základní	k2eAgFnPc6d1	základní
a	a	k8xC	a
středních	střední	k2eAgFnPc6d1	střední
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2004	[number]	k4	2004
Bush	Bush	k1gMnSc1	Bush
oznámil	oznámit	k5eAaPmAgMnS	oznámit
založení	založení	k1gNnSc4	založení
Vize	vize	k1gFnSc2	vize
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
výzkumu	výzkum	k1gInSc2	výzkum
(	(	kIx(	(
<g/>
en	en	k?	en
<g/>
:	:	kIx,	:
<g/>
Vision	vision	k1gInSc1	vision
for	forum	k1gNnPc2	forum
Space	Space	k1gFnSc2	Space
Exploration	Exploration	k1gInSc1	Exploration
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
plánovaný	plánovaný	k2eAgInSc4d1	plánovaný
návrat	návrat	k1gInSc4	návrat
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2020	[number]	k4	2020
<g/>
,	,	kIx,	,
realizace	realizace	k1gFnSc1	realizace
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
vesmírné	vesmírný	k2eAgFnSc2d1	vesmírná
stanice	stanice	k1gFnSc2	stanice
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
a	a	k8xC	a
případně	případně	k6eAd1	případně
vyslání	vyslání	k1gNnSc1	vyslání
astronautů	astronaut	k1gMnPc2	astronaut
na	na	k7c4	na
Mars	Mars	k1gInSc4	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Byť	byť	k8xS	byť
se	se	k3xPyFc4	se
plán	plán	k1gInSc1	plán
střetl	střetnout	k5eAaPmAgInS	střetnout
s	s	k7c7	s
rozporuplnými	rozporuplný	k2eAgFnPc7d1	rozporuplná
reakcemi	reakce	k1gFnPc7	reakce
<g/>
,	,	kIx,	,
schválení	schválení	k1gNnSc4	schválení
jeho	jeho	k3xOp3gInSc2	jeho
rozpočtu	rozpočet	k1gInSc2	rozpočet
s	s	k7c7	s
malými	malý	k2eAgFnPc7d1	malá
změnami	změna	k1gFnPc7	změna
prošlo	projít	k5eAaPmAgNnS	projít
hned	hned	k6eAd1	hned
po	po	k7c6	po
listopadových	listopadový	k2eAgFnPc6d1	listopadová
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
</s>
<s>
Bush	Bush	k1gMnSc1	Bush
vyčlenil	vyčlenit	k5eAaPmAgMnS	vyčlenit
1,2	[number]	k4	1,2
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
z	z	k7c2	z
federálního	federální	k2eAgInSc2d1	federální
rozpočtu	rozpočet	k1gInSc2	rozpočet
na	na	k7c6	na
období	období	k1gNnSc6	období
5	[number]	k4	5
let	léto	k1gNnPc2	léto
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
vodíkem	vodík	k1gInSc7	vodík
poháněných	poháněný	k2eAgNnPc2d1	poháněné
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
získat	získat	k5eAaPmF	získat
a	a	k8xC	a
nabídnout	nabídnout	k5eAaPmF	nabídnout
ekologickou	ekologický	k2eAgFnSc4d1	ekologická
náhradu	náhrada	k1gFnSc4	náhrada
za	za	k7c4	za
ropné	ropný	k2eAgInPc4d1	ropný
produkty	produkt	k1gInPc4	produkt
v	v	k7c6	v
dopravě	doprava	k1gFnSc6	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
podpořil	podpořit	k5eAaPmAgMnS	podpořit
vývoj	vývoj	k1gInSc4	vývoj
v	v	k7c6	v
tvorbě	tvorba	k1gFnSc6	tvorba
"	"	kIx"	"
<g/>
zeleného	zelený	k2eAgInSc2d1	zelený
benzínu	benzín	k1gInSc2	benzín
<g/>
"	"	kIx"	"
z	z	k7c2	z
kukuřice	kukuřice	k1gFnSc2	kukuřice
a	a	k8xC	a
výzkum	výzkum	k1gInSc4	výzkum
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
využití	využití	k1gNnSc2	využití
bionafty	bionafta	k1gFnSc2	bionafta
vznikající	vznikající	k2eAgFnSc2d1	vznikající
z	z	k7c2	z
odpadových	odpadový	k2eAgInPc2d1	odpadový
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tohoto	tento	k3xDgInSc2	tento
programu	program	k1gInSc2	program
prosadil	prosadit	k5eAaPmAgInS	prosadit
i	i	k9	i
snížení	snížení	k1gNnSc4	snížení
daňového	daňový	k2eAgNnSc2d1	daňové
zatížení	zatížení	k1gNnSc2	zatížení
pro	pro	k7c4	pro
lidi	člověk	k1gMnPc4	člověk
používající	používající	k2eAgInPc1d1	používající
automobily	automobil	k1gInPc1	automobil
s	s	k7c7	s
nižšími	nízký	k2eAgFnPc7d2	nižší
emisemi	emise	k1gFnPc7	emise
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volební	volební	k2eAgFnSc6d1	volební
kampani	kampaň	k1gFnSc6	kampaň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
definoval	definovat	k5eAaBmAgInS	definovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
soucitný	soucitný	k2eAgMnSc1d1	soucitný
konzervativec	konzervativec	k1gMnSc1	konzervativec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
politiku	politika	k1gFnSc4	politika
jeho	jeho	k3xOp3gFnSc2	jeho
administrativy	administrativa	k1gFnSc2	administrativa
má	mít	k5eAaImIp3nS	mít
neokonzervativní	okonzervativní	k2eNgMnSc1d1	neokonzervativní
think-tank	thinkank	k1gMnSc1	think-tank
Project	Project	k1gMnSc1	Project
for	forum	k1gNnPc2	forum
the	the	k?	the
New	New	k1gFnSc2	New
American	Americana	k1gFnPc2	Americana
Century	Centura	k1gFnSc2	Centura
(	(	kIx(	(
<g/>
Projekt	projekt	k1gInSc1	projekt
pro	pro	k7c4	pro
nové	nový	k2eAgNnSc4d1	nové
americké	americký	k2eAgNnSc4d1	americké
století	století	k1gNnSc4	století
<g/>
,	,	kIx,	,
PNAC	PNAC	kA	PNAC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
proponenti	proponent	k1gMnPc1	proponent
PNAC	PNAC	kA	PNAC
(	(	kIx(	(
<g/>
Dick	Dick	k1gMnSc1	Dick
Cheney	Chenea	k1gFnSc2	Chenea
<g/>
,	,	kIx,	,
Donald	Donald	k1gMnSc1	Donald
Rumsfeld	Rumsfeld	k1gMnSc1	Rumsfeld
a	a	k8xC	a
Paul	Paul	k1gMnSc1	Paul
Wolfovitz	Wolfovitz	k1gMnSc1	Wolfovitz
<g/>
)	)	kIx)	)
zastávají	zastávat	k5eAaImIp3nP	zastávat
nebo	nebo	k8xC	nebo
zastávali	zastávat	k5eAaImAgMnP	zastávat
vysoké	vysoký	k2eAgInPc4d1	vysoký
posty	post	k1gInPc4	post
v	v	k7c6	v
Bushově	Bushův	k2eAgFnSc6d1	Bushova
administrativě	administrativa	k1gFnSc6	administrativa
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
volební	volební	k2eAgFnSc2d1	volební
kampaně	kampaň	k1gFnSc2	kampaň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
Bush	Bush	k1gMnSc1	Bush
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
větší	veliký	k2eAgFnSc4d2	veliký
míru	míra	k1gFnSc4	míra
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
spolupráce	spolupráce	k1gFnSc2	spolupráce
a	a	k8xC	a
lepší	dobrý	k2eAgInPc4d2	lepší
politické	politický	k2eAgInPc4d1	politický
vztahy	vztah	k1gInPc4	vztah
se	s	k7c7	s
zeměmi	zem	k1gFnPc7	zem
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
s	s	k7c7	s
Mexikem	Mexiko	k1gNnSc7	Mexiko
<g/>
,	,	kIx,	,
a	a	k8xC	a
omezení	omezení	k1gNnSc1	omezení
snah	snaha	k1gFnPc2	snaha
o	o	k7c4	o
"	"	kIx"	"
<g/>
budování	budování	k1gNnSc4	budování
národů	národ	k1gInPc2	národ
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Názor	názor	k1gInSc1	názor
změnil	změnit	k5eAaPmAgInS	změnit
po	po	k7c6	po
teroristických	teroristický	k2eAgInPc6d1	teroristický
útocích	útok	k1gInPc6	útok
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
svoji	svůj	k3xOyFgFnSc4	svůj
pozornost	pozornost	k1gFnSc4	pozornost
přenesl	přenést	k5eAaPmAgInS	přenést
na	na	k7c4	na
Blízký	blízký	k2eAgInSc4d1	blízký
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Necelý	celý	k2eNgInSc4d1	necelý
měsíc	měsíc	k1gInSc4	měsíc
po	po	k7c6	po
útocích	útok	k1gInPc6	útok
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
USA	USA	kA	USA
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
širokou	široký	k2eAgFnSc4d1	široká
koalici	koalice	k1gFnSc4	koalice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2001	[number]	k4	2001
zahájila	zahájit	k5eAaPmAgFnS	zahájit
operaci	operace	k1gFnSc4	operace
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
odstranit	odstranit	k5eAaPmF	odstranit
vládu	vláda	k1gFnSc4	vláda
Talibanu	Taliban	k1gInSc2	Taliban
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
poskytoval	poskytovat	k5eAaImAgInS	poskytovat
podporu	podpora	k1gFnSc4	podpora
a	a	k8xC	a
úkryt	úkryt	k1gInSc4	úkryt
vedení	vedení	k1gNnSc2	vedení
teroristické	teroristický	k2eAgFnSc2d1	teroristická
organizace	organizace	k1gFnSc2	organizace
Al-Káida	Al-Káida	k1gFnSc1	Al-Káida
<g/>
.	.	kIx.	.
</s>
<s>
Koalice	koalice	k1gFnSc1	koalice
se	se	k3xPyFc4	se
během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
války	válka	k1gFnSc2	válka
soustředila	soustředit	k5eAaPmAgFnS	soustředit
zejména	zejména	k9	zejména
na	na	k7c4	na
nálety	nálet	k1gInPc4	nálet
a	a	k8xC	a
vzdušnou	vzdušný	k2eAgFnSc4d1	vzdušná
podporu	podpora	k1gFnSc4	podpora
opozičních	opoziční	k2eAgFnPc2d1	opoziční
sil	síla	k1gFnPc2	síla
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
zejména	zejména	k9	zejména
tzv.	tzv.	kA	tzv.
Severní	severní	k2eAgFnSc1d1	severní
aliance	aliance	k1gFnSc1	aliance
Talibanem	Taliban	k1gInSc7	Taliban
vyhnaného	vyhnaný	k2eAgMnSc2d1	vyhnaný
prezidenta	prezident	k1gMnSc2	prezident
Rabbáního	Rabbání	k1gMnSc2	Rabbání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
země	zem	k1gFnSc2	zem
vstoupily	vstoupit	k5eAaPmAgInP	vstoupit
jen	jen	k9	jen
malé	malý	k2eAgFnPc1d1	malá
pozemní	pozemní	k2eAgFnPc1d1	pozemní
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svržení	svržení	k1gNnSc6	svržení
vlády	vláda	k1gFnSc2	vláda
Talibanu	Taliban	k1gInSc2	Taliban
(	(	kIx(	(
<g/>
Taliban	Taliban	k1gInSc1	Taliban
se	se	k3xPyFc4	se
zcela	zcela	k6eAd1	zcela
zničit	zničit	k5eAaPmF	zničit
nepodařilo	podařit	k5eNaPmAgNnS	podařit
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
bojůvky	bojůvka	k1gFnSc2	bojůvka
vedou	vést	k5eAaImIp3nP	vést
partyzánský	partyzánský	k2eAgInSc4d1	partyzánský
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
nové	nový	k2eAgFnSc3d1	nová
vládě	vláda	k1gFnSc3	vláda
dodnes	dodnes	k6eAd1	dodnes
<g/>
)	)	kIx)	)
následovaly	následovat	k5eAaImAgFnP	následovat
snahy	snaha	k1gFnPc4	snaha
vytvořit	vytvořit	k5eAaPmF	vytvořit
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
OSN	OSN	kA	OSN
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
novou	nový	k2eAgFnSc4d1	nová
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
čele	čelo	k1gNnSc6	čelo
nakonec	nakonec	k6eAd1	nakonec
stanul	stanout	k5eAaPmAgMnS	stanout
nový	nový	k2eAgMnSc1d1	nový
prezident	prezident	k1gMnSc1	prezident
Hamid	Hamida	k1gFnPc2	Hamida
Karzáí	Karzáí	k1gMnSc1	Karzáí
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
úspěch	úspěch	k1gInSc4	úspěch
při	při	k7c6	při
porážce	porážka	k1gFnSc6	porážka
Talibanu	Taliban	k1gInSc2	Taliban
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nepodařilo	podařit	k5eNaPmAgNnS	podařit
dopadnout	dopadnout	k5eAaPmF	dopadnout
ani	ani	k8xC	ani
zabít	zabít	k5eAaPmF	zabít
Usámu	Usáma	k1gFnSc4	Usáma
bin	bin	k?	bin
Ládina	Ládin	k2eAgFnSc1d1	Ládina
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
spojeneckých	spojenecký	k2eAgMnPc2d1	spojenecký
vojáků	voják	k1gMnPc2	voják
zůstala	zůstat	k5eAaPmAgFnS	zůstat
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
měsících	měsíc	k1gInPc6	měsíc
opět	opět	k6eAd1	opět
zesiluje	zesilovat	k5eAaImIp3nS	zesilovat
vliv	vliv	k1gInSc4	vliv
Talibanu	Taliban	k1gInSc2	Taliban
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
se	s	k7c7	s
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2004	[number]	k4	2004
uskutečnily	uskutečnit	k5eAaPmAgFnP	uskutečnit
prezidentské	prezidentský	k2eAgFnPc1d1	prezidentská
volby	volba	k1gFnPc1	volba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
zahraniční	zahraniční	k2eAgMnPc1d1	zahraniční
pozorovatelé	pozorovatel	k1gMnPc1	pozorovatel
označili	označit	k5eAaPmAgMnP	označit
za	za	k7c4	za
demokratické	demokratický	k2eAgNnSc4d1	demokratické
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
Hamid	Hamid	k1gInSc4	Hamid
Karzáí	Karzáí	k1gFnSc2	Karzáí
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
důležitým	důležitý	k2eAgNnSc7d1	důležité
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
byla	být	k5eAaImAgFnS	být
změna	změna	k1gFnSc1	změna
poměrů	poměr	k1gInPc2	poměr
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
Koalice	koalice	k1gFnSc1	koalice
"	"	kIx"	"
<g/>
ochotných	ochotný	k2eAgMnPc2d1	ochotný
<g/>
"	"	kIx"	"
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
po	po	k7c6	po
několikadenním	několikadenní	k2eAgNnSc6d1	několikadenní
ultimátu	ultimátum	k1gNnSc6	ultimátum
do	do	k7c2	do
Iráku	Irák	k1gInSc2	Irák
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2003	[number]	k4	2003
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zbavit	zbavit	k5eAaPmF	zbavit
diktátora	diktátor	k1gMnSc4	diktátor
moci	moct	k5eAaImF	moct
a	a	k8xC	a
nahradit	nahradit	k5eAaPmF	nahradit
ho	on	k3xPp3gMnSc4	on
novou	nový	k2eAgFnSc7d1	nová
<g/>
,	,	kIx,	,
demokraticky	demokraticky	k6eAd1	demokraticky
zvolenou	zvolený	k2eAgFnSc7d1	zvolená
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
toho	ten	k3xDgInSc2	ten
se	se	k3xPyFc4	se
Bush	Bush	k1gMnSc1	Bush
snažil	snažit	k5eAaImAgMnS	snažit
vyřešit	vyřešit	k5eAaPmF	vyřešit
konflikt	konflikt	k1gInSc4	konflikt
mezi	mezi	k7c7	mezi
Izraelem	Izrael	k1gInSc7	Izrael
a	a	k8xC	a
Palestinci	Palestinec	k1gMnPc1	Palestinec
obývajícími	obývající	k2eAgFnPc7d1	obývající
okupovaná	okupovaný	k2eAgNnPc4d1	okupované
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Palestincům	Palestinec	k1gMnPc3	Palestinec
slíbil	slíbit	k5eAaPmAgMnS	slíbit
podporu	podpora	k1gFnSc4	podpora
při	při	k7c6	při
vytváření	vytváření	k1gNnSc6	vytváření
vlastního	vlastní	k2eAgInSc2d1	vlastní
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
,	,	kIx,	,
některými	některý	k3yIgFnPc7	některý
evropskými	evropský	k2eAgFnPc7d1	Evropská
zeměmi	zem	k1gFnPc7	zem
a	a	k8xC	a
OSN	OSN	kA	OSN
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
takzvanou	takzvaný	k2eAgFnSc4d1	takzvaná
Cestovní	cestovní	k2eAgFnSc4d1	cestovní
mapu	mapa	k1gFnSc4	mapa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
položit	položit	k5eAaPmF	položit
základ	základ	k1gInSc4	základ
urovnání	urovnání	k1gNnSc2	urovnání
skrze	skrze	k?	skrze
ústupky	ústupek	k1gInPc1	ústupek
obou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
specifikovat	specifikovat	k5eAaBmF	specifikovat
rozvrh	rozvrh	k1gInSc1	rozvrh
stahování	stahování	k1gNnSc2	stahování
izraelských	izraelský	k2eAgNnPc2d1	izraelské
vojsk	vojsko	k1gNnPc2	vojsko
z	z	k7c2	z
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2001	[number]	k4	2001
Bush	Bush	k1gMnSc1	Bush
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
od	od	k7c2	od
smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c6	o
antibalistických	antibalistický	k2eAgFnPc6d1	antibalistický
střelách	střela	k1gFnPc6	střela
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
(	(	kIx(	(
<g/>
Anti-Ballistic	Anti-Ballistice	k1gFnPc2	Anti-Ballistice
Missile	Missila	k1gFnSc6	Missila
Treaty	Treata	k1gFnPc4	Treata
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
pro	pro	k7c4	pro
Americko-sovětskou	americkoovětský	k2eAgFnSc4d1	americko-sovětská
jadernou	jaderný	k2eAgFnSc4d1	jaderná
rovnováhu	rovnováha	k1gFnSc4	rovnováha
během	během	k7c2	během
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
důvod	důvod	k1gInSc4	důvod
bylo	být	k5eAaImAgNnS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
relevantní	relevantní	k2eAgFnSc1d1	relevantní
<g/>
.	.	kIx.	.
</s>
<s>
Smlouva	smlouva	k1gFnSc1	smlouva
zakazovala	zakazovat	k5eAaImAgFnS	zakazovat
vybudovat	vybudovat	k5eAaPmF	vybudovat
rozsáhlejší	rozsáhlý	k2eAgInSc4d2	rozsáhlejší
systém	systém	k1gInSc4	systém
chránící	chránící	k2eAgInSc4d1	chránící
před	před	k7c7	před
strategickými	strategický	k2eAgFnPc7d1	strategická
střelami	střela	k1gFnPc7	střela
protivníka	protivník	k1gMnSc2	protivník
a	a	k8xC	a
ponechávala	ponechávat	k5eAaImAgFnS	ponechávat
tak	tak	k9	tak
zemi	zem	k1gFnSc4	zem
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
útoku	útok	k1gInSc2	útok
druhé	druhý	k4xOgFnSc2	druhý
strany	strana	k1gFnSc2	strana
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ani	ani	k8xC	ani
druhá	druhý	k4xOgFnSc1	druhý
strana	strana	k1gFnSc1	strana
nebude	být	k5eNaImBp3nS	být
ochotná	ochotný	k2eAgFnSc1d1	ochotná
riskovat	riskovat	k5eAaBmF	riskovat
odvetu	odveta	k1gFnSc4	odveta
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
přístup	přístup	k1gInSc1	přístup
se	se	k3xPyFc4	se
označoval	označovat	k5eAaImAgInS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
vzájemně	vzájemně	k6eAd1	vzájemně
zaručené	zaručený	k2eAgNnSc1d1	zaručené
zničení	zničení	k1gNnSc1	zničení
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
mutual	mutuat	k5eAaImAgInS	mutuat
assured	assured	k1gInSc1	assured
destruction	destruction	k1gInSc1	destruction
<g/>
.	.	kIx.	.
</s>
<s>
Kritici	kritik	k1gMnPc1	kritik
tento	tento	k3xDgInSc4	tento
systém	systém	k1gInSc4	systém
označovali	označovat	k5eAaImAgMnP	označovat
zkratkou	zkratka	k1gFnSc7	zkratka
MAD	MAD	kA	MAD
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
znamená	znamenat	k5eAaImIp3nS	znamenat
šílený	šílený	k2eAgMnSc1d1	šílený
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vypovězení	vypovězení	k1gNnSc6	vypovězení
smlouvy	smlouva	k1gFnSc2	smlouva
(	(	kIx(	(
<g/>
které	který	k3yQgNnSc1	který
zpočátku	zpočátku	k6eAd1	zpočátku
znervóznilo	znervóznit	k5eAaPmAgNnS	znervóznit
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
mohly	moct	k5eAaImAgInP	moct
USA	USA	kA	USA
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
svého	svůj	k3xOyFgInSc2	svůj
obranného	obranný	k2eAgInSc2d1	obranný
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
známého	známý	k1gMnSc4	známý
jako	jako	k8xS	jako
raketový	raketový	k2eAgInSc4d1	raketový
deštník	deštník	k1gInSc4	deštník
<g/>
.	.	kIx.	.
</s>
<s>
Dosavadní	dosavadní	k2eAgInPc1d1	dosavadní
testy	test	k1gInPc1	test
však	však	k8xC	však
funkčnost	funkčnost	k1gFnSc1	funkčnost
tohoto	tento	k3xDgInSc2	tento
systému	systém	k1gInSc2	systém
zpochybňují	zpochybňovat	k5eAaImIp3nP	zpochybňovat
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
trestní	trestní	k2eAgInSc1d1	trestní
soud	soud	k1gInSc1	soud
(	(	kIx(	(
<g/>
International	International	k1gFnSc1	International
Criminal	Criminal	k1gMnSc1	Criminal
Court	Court	k1gInSc1	Court
<g/>
,	,	kIx,	,
ICC	ICC	kA	ICC
<g/>
)	)	kIx)	)
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2002	[number]	k4	2002
a	a	k8xC	a
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
nizozemském	nizozemský	k2eAgInSc6d1	nizozemský
Haagu	Haag	k1gInSc6	Haag
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
ustanoven	ustanovit	k5eAaPmNgInS	ustanovit
jako	jako	k8xS	jako
stálý	stálý	k2eAgInSc1d1	stálý
trestní	trestní	k2eAgInSc1d1	trestní
soud	soud	k1gInSc1	soud
pro	pro	k7c4	pro
stíhání	stíhání	k1gNnSc4	stíhání
pachatelů	pachatel	k1gMnPc2	pachatel
na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Bushova	Bushův	k2eAgFnSc1d1	Bushova
administrativa	administrativa	k1gFnSc1	administrativa
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
obavy	obava	k1gFnPc4	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
soud	soud	k1gInSc1	soud
zvrhne	zvrhnout	k5eAaPmIp3nS	zvrhnout
v	v	k7c4	v
nástroj	nástroj	k1gInSc4	nástroj
na	na	k7c4	na
obviňování	obviňování	k1gNnSc4	obviňování
a	a	k8xC	a
souzení	souzení	k1gNnSc4	souzení
amerických	americký	k2eAgMnPc2d1	americký
diplomatů	diplomat	k1gMnPc2	diplomat
<g/>
,	,	kIx,	,
politiků	politik	k1gMnPc2	politik
a	a	k8xC	a
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
budou	být	k5eAaImBp3nP	být
mít	mít	k5eAaImF	mít
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
v	v	k7c6	v
OSN	OSN	kA	OSN
<g/>
)	)	kIx)	)
moc	moc	k6eAd1	moc
nezodpovědné	zodpovědný	k2eNgFnSc2d1	nezodpovědná
země	zem	k1gFnSc2	zem
porušující	porušující	k2eAgNnPc4d1	porušující
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
příklad	příklad	k1gInSc4	příklad
je	být	k5eAaImIp3nS	být
uváděna	uváděn	k2eAgFnSc1d1	uváděna
Libye	Libye	k1gFnSc1	Libye
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2003	[number]	k4	2003
zvolena	zvolit	k5eAaPmNgFnS	zvolit
předsedající	předsedající	k2eAgFnSc7d1	předsedající
zemí	zem	k1gFnSc7	zem
Komise	komise	k1gFnSc2	komise
pro	pro	k7c4	pro
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
OSN	OSN	kA	OSN
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Teroristické	teroristický	k2eAgInPc4d1	teroristický
útoky	útok	k1gInPc4	útok
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Operace	operace	k1gFnSc1	operace
Trvalá	trvalá	k1gFnSc1	trvalá
svoboda	svoboda	k1gFnSc1	svoboda
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2001	[number]	k4	2001
byly	být	k5eAaImAgInP	být
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
šokovány	šokován	k2eAgInPc1d1	šokován
svou	svůj	k3xOyFgFnSc7	svůj
zranitelností	zranitelnost	k1gFnSc7	zranitelnost
<g/>
.	.	kIx.	.
</s>
<s>
Zahynulo	zahynout	k5eAaPmAgNnS	zahynout
přes	přes	k7c4	přes
3000	[number]	k4	3000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Bush	Bush	k1gMnSc1	Bush
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
okamžik	okamžik	k1gInSc1	okamžik
prvního	první	k4xOgInSc2	první
útoku	útok	k1gInSc2	útok
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
na	na	k7c4	na
návštěvu	návštěva	k1gFnSc4	návštěva
floridské	floridský	k2eAgFnSc2d1	floridská
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
Emmy	Emma	k1gFnSc2	Emma
E.	E.	kA	E.
Bookerové	Bookerová	k1gFnSc2	Bookerová
<g/>
,	,	kIx,	,
o	o	k7c6	o
tom	ten	k3xDgInSc6	ten
druhém	druhý	k4xOgInSc6	druhý
se	se	k3xPyFc4	se
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
od	od	k7c2	od
Andy	Anda	k1gFnSc2	Anda
Carda	Carda	k1gMnSc1	Carda
již	již	k6eAd1	již
ve	v	k7c6	v
třídě	třída	k1gFnSc6	třída
a	a	k8xC	a
až	až	k9	až
po	po	k7c6	po
sedmi	sedm	k4xCc6	sedm
minutách	minuta	k1gFnPc6	minuta
poté	poté	k6eAd1	poté
ji	on	k3xPp3gFnSc4	on
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
,	,	kIx,	,
aby	aby	k9	aby
improvizovaným	improvizovaný	k2eAgNnSc7d1	improvizované
prohlášením	prohlášení	k1gNnSc7	prohlášení
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
útokům	útok	k1gInPc3	útok
na	na	k7c6	na
americké	americký	k2eAgFnSc6d1	americká
půdě	půda	k1gFnSc6	půda
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
toho	ten	k3xDgInSc2	ten
dne	den	k1gInSc2	den
strávil	strávit	k5eAaPmAgMnS	strávit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
viceprezidentem	viceprezident	k1gMnSc7	viceprezident
Dickem	Dicek	k1gInSc7	Dicek
Cheneym	Cheneymum	k1gNnPc2	Cheneymum
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
v	v	k7c6	v
Air	Air	k1gFnPc6	Air
Force	force	k1gFnSc2	force
One	One	k1gFnSc2	One
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
k	k	k7c3	k
večeru	večer	k1gInSc3	večer
promluvil	promluvit	k5eAaPmAgMnS	promluvit
na	na	k7c6	na
celostátních	celostátní	k2eAgInPc6d1	celostátní
televizních	televizní	k2eAgInPc6d1	televizní
kanálech	kanál	k1gInPc6	kanál
k	k	k7c3	k
národu	národ	k1gInSc3	národ
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Bush	Bush	k1gMnSc1	Bush
svou	svůj	k3xOyFgFnSc4	svůj
předvolební	předvolební	k2eAgFnSc4d1	předvolební
kampaň	kampaň	k1gFnSc4	kampaň
(	(	kIx(	(
<g/>
její	její	k3xOp3gFnSc4	její
zahraničně-politickou	zahraničněolitický	k2eAgFnSc4d1	zahraničně-politická
část	část	k1gFnSc4	část
<g/>
)	)	kIx)	)
stavěl	stavět	k5eAaImAgMnS	stavět
proti	proti	k7c3	proti
intervencionalismu	intervencionalismus	k1gInSc3	intervencionalismus
<g/>
,	,	kIx,	,
po	po	k7c6	po
útocích	útok	k1gInPc6	útok
zaměření	zaměření	k1gNnSc2	zaměření
americké	americký	k2eAgFnSc2d1	americká
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
změnil	změnit	k5eAaPmAgMnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
další	další	k2eAgFnSc1d1	další
politika	politika	k1gFnSc1	politika
Bushovy	Bushův	k2eAgFnSc2d1	Bushova
vlády	vláda	k1gFnSc2	vláda
byla	být	k5eAaImAgFnS	být
založená	založený	k2eAgFnSc1d1	založená
nebo	nebo	k8xC	nebo
ovlivněná	ovlivněný	k2eAgFnSc1d1	ovlivněná
Válkou	válka	k1gFnSc7	válka
proti	proti	k7c3	proti
terorismu	terorismus	k1gInSc3	terorismus
a	a	k8xC	a
týkala	týkat	k5eAaImAgFnS	týkat
se	se	k3xPyFc4	se
nejen	nejen	k6eAd1	nejen
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
ale	ale	k8xC	ale
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
i	i	k9	i
dění	dění	k1gNnSc4	dění
uvnitř	uvnitř	k7c2	uvnitř
samotných	samotný	k2eAgInPc2d1	samotný
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
přijata	přijat	k2eAgFnSc1d1	přijata
legislativa	legislativa	k1gFnSc1	legislativa
jako	jako	k8xC	jako
USA	USA	kA	USA
PATRIOT	patriot	k1gMnSc1	patriot
Act	Act	k1gMnSc1	Act
of	of	k?	of
2001	[number]	k4	2001
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
výrazně	výrazně	k6eAd1	výrazně
omezily	omezit	k5eAaPmAgFnP	omezit
občanské	občanský	k2eAgFnPc1d1	občanská
svobody	svoboda	k1gFnPc1	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Změnu	změna	k1gFnSc4	změna
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
Bush	Bush	k1gMnSc1	Bush
jasně	jasně	k6eAd1	jasně
deklaroval	deklarovat	k5eAaBmAgMnS	deklarovat
ve	v	k7c6	v
Zprávě	zpráva	k1gFnSc6	zpráva
o	o	k7c6	o
stavu	stav	k1gInSc6	stav
Unie	unie	k1gFnSc2	unie
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Útoky	útok	k1gInPc1	útok
zvýraznily	zvýraznit	k5eAaPmAgInP	zvýraznit
ohrožení	ohrožení	k1gNnSc4	ohrožení
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
teroristických	teroristický	k2eAgFnPc2d1	teroristická
skupin	skupina	k1gFnPc2	skupina
oproti	oproti	k7c3	oproti
ohrožení	ohrožení	k1gNnSc3	ohrožení
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Bush	Bush	k1gMnSc1	Bush
použil	použít	k5eAaPmAgMnS	použít
termín	termín	k1gInSc4	termín
"	"	kIx"	"
<g/>
Osa	osa	k1gFnSc1	osa
zla	zlo	k1gNnSc2	zlo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yQgFnSc2	který
zahrnul	zahrnout	k5eAaPmAgInS	zahrnout
Írán	Írán	k1gInSc1	Írán
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc4d1	severní
Koreu	Korea	k1gFnSc4	Korea
a	a	k8xC	a
Irák	Irák	k1gInSc1	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Koncept	koncept	k1gInSc1	koncept
jeho	jeho	k3xOp3gInSc2	jeho
přístupu	přístup	k1gInSc2	přístup
v	v	k7c6	v
"	"	kIx"	"
<g/>
pozářiové	pozářiový	k2eAgFnSc3d1	pozářiový
<g/>
"	"	kIx"	"
americké	americký	k2eAgFnSc3d1	americká
zahraniční	zahraniční	k2eAgFnSc3d1	zahraniční
politice	politika	k1gFnSc3	politika
(	(	kIx(	(
<g/>
a	a	k8xC	a
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
<g/>
)	)	kIx)	)
dostal	dostat	k5eAaPmAgInS	dostat
název	název	k1gInSc1	název
Bushova	Bushův	k2eAgFnSc1d1	Bushova
doktrína	doktrína	k1gFnSc1	doktrína
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
původce	původce	k1gMnSc2	původce
útoků	útok	k1gInPc2	útok
označila	označit	k5eAaPmAgFnS	označit
Bushova	Bushův	k2eAgFnSc1d1	Bushova
administrativa	administrativa	k1gFnSc1	administrativa
Usámu	Usám	k1gInSc2	Usám
bin	bin	k?	bin
Ládina	Ládin	k2eAgFnSc1d1	Ládina
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
organizaci	organizace	k1gFnSc4	organizace
Al-Kajda	Al-Kajda	k1gFnSc1	Al-Kajda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
své	svůj	k3xOyFgFnPc4	svůj
hlavní	hlavní	k2eAgFnPc4d1	hlavní
základny	základna	k1gFnPc4	základna
v	v	k7c6	v
Talibanem	Taliban	k1gInSc7	Taliban
ovládaném	ovládaný	k2eAgInSc6d1	ovládaný
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
<g/>
.	.	kIx.	.
</s>
<s>
Bush	Bush	k1gMnSc1	Bush
dal	dát	k5eAaPmAgMnS	dát
afghánské	afghánský	k2eAgFnSc3d1	afghánská
vládě	vláda	k1gFnSc3	vláda
ultimátum	ultimátum	k1gNnSc1	ultimátum
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vydala	vydat	k5eAaPmAgFnS	vydat
bin	bin	k?	bin
Ládina	Ládin	k2eAgFnSc1d1	Ládina
Spojeným	spojený	k2eAgInPc3d1	spojený
státům	stát	k1gInPc3	stát
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
požadavky	požadavek	k1gInPc7	požadavek
<g/>
,	,	kIx,	,
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
zejména	zejména	k6eAd1	zejména
teroristických	teroristický	k2eAgInPc2d1	teroristický
výcvikových	výcvikový	k2eAgInPc2d1	výcvikový
táborů	tábor	k1gInPc2	tábor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Taliban	Taliban	k1gMnSc1	Taliban
žádal	žádat	k5eAaImAgMnS	žádat
předložení	předložení	k1gNnSc4	předložení
důkazů	důkaz	k1gInPc2	důkaz
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
bin	bin	k?	bin
Ládin	Ládin	k2eAgMnSc1d1	Ládin
skutečně	skutečně	k6eAd1	skutečně
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
útoky	útok	k1gInPc4	útok
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
s	s	k7c7	s
blížícím	blížící	k2eAgMnSc7d1	blížící
se	se	k3xPyFc4	se
útokem	útok	k1gInSc7	útok
<g/>
,	,	kIx,	,
Taliban	Taliban	k1gMnSc1	Taliban
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
vydání	vydání	k1gNnSc4	vydání
bin	bin	k?	bin
Ládina	Ládin	k2eAgNnPc4d1	Ládin
do	do	k7c2	do
Pákistánu	Pákistán	k1gInSc2	Pákistán
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
souzený	souzený	k2eAgInSc1d1	souzený
podle	podle	k7c2	podle
islámského	islámský	k2eAgNnSc2d1	islámské
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
vůbec	vůbec	k9	vůbec
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
moci	moc	k1gFnSc6	moc
vlády	vláda	k1gFnSc2	vláda
bin	bin	k?	bin
Ládina	Ládin	k2eAgFnSc1d1	Ládina
vydat	vydat	k5eAaPmF	vydat
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
týden	týden	k1gInSc4	týden
po	po	k7c6	po
zahájení	zahájení	k1gNnSc6	zahájení
operace	operace	k1gFnSc2	operace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Tálibán	Tálibán	k2eAgInSc1d1	Tálibán
vydání	vydání	k1gNnSc1	vydání
bin	bin	k?	bin
Ládina	Ládin	k2eAgInSc2d1	Ládin
Americe	Amerika	k1gFnSc6	Amerika
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
<g/>
,	,	kIx,	,
Bush	Bush	k1gMnSc1	Bush
osobně	osobně	k6eAd1	osobně
tuto	tento	k3xDgFnSc4	tento
nabídku	nabídka	k1gFnSc4	nabídka
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
se	s	k7c7	s
slovy	slovo	k1gNnPc7	slovo
"	"	kIx"	"
<g/>
Když	když	k8xS	když
říkám	říkat	k5eAaImIp1nS	říkat
'	'	kIx"	'
<g/>
žádné	žádný	k3yNgNnSc1	žádný
vyjednávání	vyjednávání	k1gNnSc1	vyjednávání
<g/>
'	'	kIx"	'
tak	tak	k6eAd1	tak
míním	mínit	k5eAaImIp1nS	mínit
'	'	kIx"	'
<g/>
žádné	žádný	k3yNgNnSc1	žádný
vyjednávání	vyjednávání	k1gNnSc1	vyjednávání
<g/>
'	'	kIx"	'
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
USA	USA	kA	USA
zahájily	zahájit	k5eAaPmAgFnP	zahájit
vojenské	vojenský	k2eAgFnPc4d1	vojenská
operace	operace	k1gFnPc4	operace
proti	proti	k7c3	proti
Talibanu	Taliban	k1gInSc3	Taliban
bombardováním	bombardování	k1gNnSc7	bombardování
vybraných	vybraný	k2eAgFnPc2d1	vybraná
oblastí	oblast	k1gFnPc2	oblast
<g/>
;	;	kIx,	;
19	[number]	k4	19
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
pak	pak	k6eAd1	pak
pozemními	pozemní	k2eAgInPc7d1	pozemní
útoky	útok	k1gInPc7	útok
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
letecké	letecký	k2eAgInPc1d1	letecký
údery	úder	k1gInPc1	úder
podporované	podporovaný	k2eAgInPc1d1	podporovaný
akcemi	akce	k1gFnPc7	akce
zejména	zejména	k9	zejména
amerických	americký	k2eAgFnPc2d1	americká
a	a	k8xC	a
britských	britský	k2eAgFnPc2d1	britská
zvláštních	zvláštní	k2eAgFnPc2d1	zvláštní
jednotek	jednotka	k1gFnPc2	jednotka
a	a	k8xC	a
elitních	elitní	k2eAgInPc2d1	elitní
oddílů	oddíl	k1gInPc2	oddíl
umožnily	umožnit	k5eAaPmAgInP	umožnit
Severní	severní	k2eAgFnSc4d1	severní
alianci	aliance	k1gFnSc4	aliance
13	[number]	k4	13
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2001	[number]	k4	2001
dobýt	dobýt	k5eAaPmF	dobýt
Kábul	Kábul	k1gInSc4	Kábul
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
kontrolu	kontrola	k1gFnSc4	kontrola
dělila	dělit	k5eAaImAgFnS	dělit
s	s	k7c7	s
americkými	americký	k2eAgFnPc7d1	americká
jednotkami	jednotka	k1gFnPc7	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
Talibanu	Taliban	k1gInSc2	Taliban
na	na	k7c6	na
celostátní	celostátní	k2eAgFnSc6d1	celostátní
úrovní	úroveň	k1gFnPc2	úroveň
padla	padnout	k5eAaImAgNnP	padnout
<g/>
,	,	kIx,	,
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
však	však	k9	však
ještě	ještě	k6eAd1	ještě
držela	držet	k5eAaImAgFnS	držet
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
provinciích	provincie	k1gFnPc6	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Partyzánské	partyzánský	k2eAgInPc1d1	partyzánský
oddíly	oddíl	k1gInPc1	oddíl
složené	složený	k2eAgInPc1d1	složený
z	z	k7c2	z
věrných	věrný	k2eAgInPc2d1	věrný
Talibanu	Taliban	k1gInSc2	Taliban
se	se	k3xPyFc4	se
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Talibanem	Taliban	k1gInSc7	Taliban
vyhnaný	vyhnaný	k2eAgMnSc1d1	vyhnaný
prezident	prezident	k1gMnSc1	prezident
Burhanuddín	Burhanuddín	k1gInSc4	Burhanuddín
Rabbání	Rabbání	k1gNnSc2	Rabbání
se	s	k7c7	s
(	(	kIx(	(
<g/>
dočasně	dočasně	k6eAd1	dočasně
<g/>
)	)	kIx)	)
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
byla	být	k5eAaImAgFnS	být
sestavena	sestavit	k5eAaPmNgFnS	sestavit
vláda	vláda	k1gFnSc1	vláda
vedená	vedený	k2eAgFnSc1d1	vedená
Hamídem	Hamíd	k1gMnSc7	Hamíd
Karzáím	Karzáí	k1gMnSc7	Karzáí
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
nakonec	nakonec	k6eAd1	nakonec
převzala	převzít	k5eAaPmAgFnS	převzít
moc	moc	k6eAd1	moc
nad	nad	k7c7	nad
většinou	většina	k1gFnSc7	většina
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc4	některý
oblasti	oblast	k1gFnPc4	oblast
však	však	k9	však
stále	stále	k6eAd1	stále
neovládá	ovládat	k5eNaImIp3nS	ovládat
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
je	být	k5eAaImIp3nS	být
ovládá	ovládat	k5eAaImIp3nS	ovládat
víceméně	víceméně	k9	víceméně
formálně	formálně	k6eAd1	formálně
na	na	k7c6	na
základě	základ	k1gInSc6	základ
dohod	dohoda	k1gFnPc2	dohoda
s	s	k7c7	s
místními	místní	k2eAgMnPc7d1	místní
vůdci	vůdce	k1gMnPc7	vůdce
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
mají	mít	k5eAaImIp3nP	mít
nepokoje	nepokoj	k1gInPc1	nepokoj
a	a	k8xC	a
bombové	bombový	k2eAgInPc1d1	bombový
útoky	útok	k1gInPc1	útok
vzrůstající	vzrůstající	k2eAgFnSc4d1	vzrůstající
tendenci	tendence	k1gFnSc4	tendence
<g/>
.	.	kIx.	.
</s>
<s>
Síly	síla	k1gFnPc1	síla
OSN	OSN	kA	OSN
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
udržet	udržet	k5eAaPmF	udržet
pořádek	pořádek	k1gInSc4	pořádek
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Kábulu	Kábul	k1gInSc2	Kábul
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
významných	významný	k2eAgNnPc2d1	významné
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
již	již	k9	již
prezident	prezident	k1gMnSc1	prezident
Karzáí	Karzáí	k1gMnSc1	Karzáí
jsou	být	k5eAaImIp3nP	být
blízkými	blízký	k2eAgMnPc7d1	blízký
spojenci	spojenec	k1gMnPc7	spojenec
Washingtonu	Washington	k1gInSc2	Washington
v	v	k7c6	v
pokračujícím	pokračující	k2eAgInSc6d1	pokračující
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
terorismu	terorismus	k1gInSc3	terorismus
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Samotný	samotný	k2eAgMnSc1d1	samotný
Hamíd	Hamíd	k1gMnSc1	Hamíd
Karzáí	Karzáí	k1gMnSc1	Karzáí
studoval	studovat	k5eAaImAgMnS	studovat
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
poradcem	poradce	k1gMnSc7	poradce
nadnárodního	nadnárodní	k2eAgInSc2d1	nadnárodní
naftařského	naftařský	k2eAgInSc2d1	naftařský
koncernu	koncern	k1gInSc2	koncern
Unocal	Unocal	k1gMnSc2	Unocal
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
budoval	budovat	k5eAaImAgMnS	budovat
Transafghánský	Transafghánský	k2eAgInSc4d1	Transafghánský
plynovod	plynovod	k1gInSc4	plynovod
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Bushova	Bushův	k2eAgFnSc1d1	Bushova
administrativa	administrativa	k1gFnSc1	administrativa
je	být	k5eAaImIp3nS	být
kritizovaná	kritizovaný	k2eAgFnSc1d1	kritizovaná
pro	pro	k7c4	pro
zadržování	zadržování	k1gNnSc4	zadržování
několika	několik	k4yIc2	několik
stovek	stovka	k1gFnPc2	stovka
vězňů	vězeň	k1gMnPc2	vězeň
v	v	k7c6	v
táboře	tábor	k1gInSc6	tábor
Camp	camp	k1gInSc1	camp
X-Ray	X-Raa	k1gFnSc2	X-Raa
v	v	k7c6	v
Zátoce	zátoka	k1gFnSc6	zátoka
Guantanámo	Guantanáma	k1gFnSc5	Guantanáma
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
bez	bez	k7c2	bez
řádného	řádný	k2eAgInSc2d1	řádný
soudu	soud	k1gInSc2	soud
(	(	kIx(	(
<g/>
věznice	věznice	k1gFnSc2	věznice
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
vojenské	vojenský	k2eAgFnSc6d1	vojenská
základně	základna	k1gFnSc6	základna
byla	být	k5eAaImAgFnS	být
zbudována	zbudovat	k5eAaPmNgFnS	zbudovat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
zadržených	zadržený	k2eAgInPc2d1	zadržený
je	být	k5eAaImIp3nS	být
podezřelá	podezřelý	k2eAgFnSc1d1	podezřelá
ze	z	k7c2	z
spolupráce	spolupráce	k1gFnSc2	spolupráce
se	s	k7c7	s
sítí	síť	k1gFnSc7	síť
Al-Káida	Al-Káida	k1gFnSc1	Al-Káida
<g/>
,	,	kIx,	,
či	či	k8xC	či
Talibanem	Taliban	k1gInSc7	Taliban
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c4	mnoho
organizací	organizace	k1gFnPc2	organizace
zaměřených	zaměřený	k2eAgFnPc2d1	zaměřená
na	na	k7c4	na
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
hovoří	hovořit	k5eAaImIp3nP	hovořit
o	o	k7c6	o
nelidských	lidský	k2eNgFnPc6d1	nelidská
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
,	,	kIx,	,
ponižování	ponižování	k1gNnSc6	ponižování
a	a	k8xC	a
týrání	týrání	k1gNnSc6	týrání
<g/>
.	.	kIx.	.
</s>
<s>
Americké	americký	k2eAgFnPc1d1	americká
oficiální	oficiální	k2eAgFnPc1d1	oficiální
kontroly	kontrola	k1gFnPc1	kontrola
ale	ale	k8xC	ale
nic	nic	k3yNnSc1	nic
takového	takový	k3xDgNnSc2	takový
nepotvrdily	potvrdit	k5eNaPmAgFnP	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
schválení	schválení	k1gNnSc2	schválení
Zákona	zákon	k1gInSc2	zákon
o	o	k7c4	o
osvobození	osvobození	k1gNnSc4	osvobození
Iráku	Irák	k1gInSc2	Irák
(	(	kIx(	(
<g/>
en	en	k?	en
<g/>
:	:	kIx,	:
<g/>
Iraq	Iraq	k1gFnSc1	Iraq
Liberation	Liberation	k1gInSc1	Liberation
Act	Act	k1gFnSc1	Act
<g/>
)	)	kIx)	)
z	z	k7c2	z
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1998	[number]	k4	1998
podepsaného	podepsaný	k2eAgInSc2d1	podepsaný
ještě	ještě	k6eAd1	ještě
Billem	Bill	k1gMnSc7	Bill
Clintonem	Clinton	k1gMnSc7	Clinton
bylo	být	k5eAaImAgNnS	být
oficiálním	oficiální	k2eAgInSc7d1	oficiální
cílem	cíl	k1gInSc7	cíl
americké	americký	k2eAgFnSc2d1	americká
politiky	politika	k1gFnSc2	politika
zbavit	zbavit	k5eAaPmF	zbavit
Saddáma	Saddám	k1gMnSc4	Saddám
Husajna	Husajn	k1gMnSc4	Husajn
moci	moct	k5eAaImF	moct
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
útocích	útok	k1gInPc6	útok
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2001	[number]	k4	2001
hledala	hledat	k5eAaImAgFnS	hledat
Bushova	Bushův	k2eAgFnSc1d1	Bushova
administrativa	administrativa	k1gFnSc1	administrativa
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
zabránit	zabránit	k5eAaPmF	zabránit
dalším	další	k2eAgInPc3d1	další
možným	možný	k2eAgInPc3d1	možný
útokům	útok	k1gInPc3	útok
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
koncepce	koncepce	k1gFnSc1	koncepce
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
snahu	snaha	k1gFnSc4	snaha
omezit	omezit	k5eAaPmF	omezit
"	"	kIx"	"
<g/>
manévrovací	manévrovací	k2eAgInSc4d1	manévrovací
prostor	prostor	k1gInSc4	prostor
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
teroristické	teroristický	k2eAgFnPc4d1	teroristická
skupiny	skupina	k1gFnPc4	skupina
(	(	kIx(	(
<g/>
zmrazení	zmrazení	k1gNnSc1	zmrazení
účtů	účet	k1gInPc2	účet
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tlak	tlak	k1gInSc1	tlak
na	na	k7c4	na
všechny	všechen	k3xTgFnPc4	všechen
země	zem	k1gFnPc4	zem
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
aktivně	aktivně	k6eAd1	aktivně
vyhledávaly	vyhledávat	k5eAaImAgFnP	vyhledávat
teroristy	terorista	k1gMnPc7	terorista
a	a	k8xC	a
snaha	snaha	k1gFnSc1	snaha
zamezit	zamezit	k5eAaPmF	zamezit
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
takzvané	takzvaný	k2eAgFnPc1d1	takzvaná
"	"	kIx"	"
<g/>
neposlušné	poslušný	k2eNgInPc1d1	neposlušný
<g/>
"	"	kIx"	"
státy	stát	k1gInPc1	stát
mohly	moct	k5eAaImAgInP	moct
podporovat	podporovat	k5eAaImF	podporovat
terorismus	terorismus	k1gInSc4	terorismus
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
aby	aby	kYmCp3nP	aby
teroristé	terorista	k1gMnPc1	terorista
mohli	moct	k5eAaImAgMnP	moct
získat	získat	k5eAaPmF	získat
nebezpečné	bezpečný	k2eNgFnPc1d1	nebezpečná
zbraně	zbraň	k1gFnPc1	zbraň
od	od	k7c2	od
těchto	tento	k3xDgInPc2	tento
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
z	z	k7c2	z
vybraných	vybraný	k2eAgMnPc2d1	vybraný
směrů	směr	k1gInPc2	směr
byla	být	k5eAaImAgFnS	být
i	i	k9	i
změna	změna	k1gFnSc1	změna
poměrů	poměr	k1gInPc2	poměr
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
pocházeli	pocházet	k5eAaImAgMnP	pocházet
všichni	všechen	k3xTgMnPc1	všechen
útočníci	útočník	k1gMnPc1	útočník
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
vláda	vláda	k1gFnSc1	vláda
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
situace	situace	k1gFnSc1	situace
ve	v	k7c6	v
vztazích	vztah	k1gInPc6	vztah
k	k	k7c3	k
Iráku	Irák	k1gInSc3	Irák
je	být	k5eAaImIp3nS	být
kvůli	kvůli	k7c3	kvůli
těmto	tento	k3xDgInPc3	tento
důvodům	důvod	k1gInPc3	důvod
vážná	vážný	k2eAgNnPc4d1	vážné
<g/>
.	.	kIx.	.
</s>
<s>
Irák	Irák	k1gInSc1	Irák
se	se	k3xPyFc4	se
údajně	údajně	k6eAd1	údajně
snažil	snažit	k5eAaImAgMnS	snažit
získat	získat	k5eAaPmF	získat
jaderný	jaderný	k2eAgInSc4d1	jaderný
materiál	materiál	k1gInSc4	materiál
a	a	k8xC	a
neuvedl	uvést	k5eNaPmAgMnS	uvést
přesnou	přesný	k2eAgFnSc4d1	přesná
dokumentaci	dokumentace	k1gFnSc4	dokumentace
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
zbrojním	zbrojní	k2eAgInSc6d1	zbrojní
programu	program	k1gInSc6	program
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
biologických	biologický	k2eAgFnPc2d1	biologická
a	a	k8xC	a
chemických	chemický	k2eAgFnPc2d1	chemická
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
využít	využít	k5eAaPmF	využít
jako	jako	k9	jako
základ	základ	k1gInSc4	základ
pro	pro	k7c4	pro
zbraně	zbraň	k1gFnPc4	zbraň
hromadného	hromadný	k2eAgNnSc2d1	hromadné
ničení	ničení	k1gNnSc2	ničení
(	(	kIx(	(
<g/>
dále	daleko	k6eAd2	daleko
jen	jen	k9	jen
ZHN	ZHN	kA	ZHN
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
měl	mít	k5eAaImAgMnS	mít
dle	dle	k7c2	dle
rezolucí	rezoluce	k1gFnPc2	rezoluce
OSN	OSN	kA	OSN
vydaných	vydaný	k2eAgInPc2d1	vydaný
po	po	k7c4	po
invazi	invaze	k1gFnSc4	invaze
do	do	k7c2	do
Kuvajtu	Kuvajt	k1gInSc2	Kuvajt
zakázané	zakázaný	k2eAgNnSc1d1	zakázané
vyrábět	vyrábět	k5eAaImF	vyrábět
i	i	k8xC	i
vlastnit	vlastnit	k5eAaImF	vlastnit
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
uváděné	uváděný	k2eAgInPc4d1	uváděný
důvody	důvod	k1gInPc4	důvod
patřilo	patřit	k5eAaImAgNnS	patřit
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Irák	Irák	k1gInSc1	Irák
nedodržoval	dodržovat	k5eNaImAgInS	dodržovat
podmínky	podmínka	k1gFnPc4	podmínka
dohodnuté	dohodnutý	k2eAgFnPc4d1	dohodnutá
v	v	k7c6	v
předcházejících	předcházející	k2eAgFnPc6d1	předcházející
rezolucích	rezoluce	k1gFnPc6	rezoluce
RB	RB	kA	RB
OSN	OSN	kA	OSN
vydaných	vydaný	k2eAgInPc2d1	vydaný
po	po	k7c6	po
první	první	k4xOgFnSc6	první
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
Zálivu	záliv	k1gInSc6	záliv
(	(	kIx(	(
<g/>
uváděny	uváděn	k2eAgFnPc1d1	uváděna
byly	být	k5eAaImAgFnP	být
např.	např.	kA	např.
rezoluce	rezoluce	k1gFnSc2	rezoluce
RB	RB	kA	RB
OSN	OSN	kA	OSN
číslo	číslo	k1gNnSc4	číslo
1060	[number]	k4	1060
<g/>
,	,	kIx,	,
949	[number]	k4	949
<g/>
,	,	kIx,	,
778	[number]	k4	778
<g/>
,	,	kIx,	,
715	[number]	k4	715
<g/>
,	,	kIx,	,
660	[number]	k4	660
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
stoupenci	stoupenec	k1gMnPc7	stoupenec
a	a	k8xC	a
oponenty	oponent	k1gMnPc7	oponent
vojenského	vojenský	k2eAgInSc2d1	vojenský
zásahu	zásah	k1gInSc2	zásah
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
probíhá	probíhat	k5eAaImIp3nS	probíhat
diskuze	diskuze	k1gFnSc1	diskuze
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
<g/>
-li	i	k?	-li
vláda	vláda	k1gFnSc1	vláda
USA	USA	kA	USA
důkazy	důkaz	k1gInPc4	důkaz
o	o	k7c4	o
vlastnictví	vlastnictví	k1gNnSc4	vlastnictví
zbraní	zbraň	k1gFnPc2	zbraň
hromadného	hromadný	k2eAgNnSc2d1	hromadné
ničení	ničení	k1gNnSc2	ničení
Irákem	Irák	k1gInSc7	Irák
a	a	k8xC	a
existenci	existence	k1gFnSc4	existence
blízkých	blízký	k2eAgFnPc2d1	blízká
vazeb	vazba	k1gFnPc2	vazba
mezi	mezi	k7c7	mezi
Irákem	Irák	k1gInSc7	Irák
a	a	k8xC	a
Al-Kajdou	Al-Kajda	k1gFnSc7	Al-Kajda
<g/>
.	.	kIx.	.
</s>
<s>
Jisté	jistý	k2eAgNnSc1d1	jisté
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
<g/>
,	,	kIx,	,
že	že	k8xS	že
USA	USA	kA	USA
měly	mít	k5eAaImAgFnP	mít
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
OSN	OSN	kA	OSN
důkazy	důkaz	k1gInPc1	důkaz
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Irák	Irák	k1gInSc1	Irák
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
ZHN	ZHN	kA	ZHN
dříve	dříve	k6eAd2	dříve
a	a	k8xC	a
že	že	k8xS	že
nebyl	být	k5eNaImAgMnS	být
schopen	schopen	k2eAgMnSc1d1	schopen
doložit	doložit	k5eAaPmF	doložit
jejich	jejich	k3xOp3gNnSc4	jejich
řádné	řádný	k2eAgNnSc4d1	řádné
zničení	zničení	k1gNnSc4	zničení
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
ukládaly	ukládat	k5eAaImAgFnP	ukládat
rezoluce	rezoluce	k1gFnPc1	rezoluce
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
terorismus	terorismus	k1gInSc4	terorismus
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
zřetelné	zřetelný	k2eAgFnSc2d1	zřetelná
podpory	podpora	k1gFnSc2	podpora
palestinského	palestinský	k2eAgInSc2d1	palestinský
terorismu	terorismus	k1gInSc2	terorismus
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
se	se	k3xPyFc4	se
spojení	spojení	k1gNnSc1	spojení
mezi	mezi	k7c7	mezi
Al-Kajdou	Al-Kajda	k1gFnSc7	Al-Kajda
a	a	k8xC	a
Irákem	Irák	k1gInSc7	Irák
nepodařilo	podařit	k5eNaPmAgNnS	podařit
prokázat	prokázat	k5eAaPmF	prokázat
a	a	k8xC	a
odborníci	odborník	k1gMnPc1	odborník
se	se	k3xPyFc4	se
kloní	klonit	k5eAaImIp3nP	klonit
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nikdy	nikdy	k6eAd1	nikdy
žádné	žádný	k3yNgNnSc1	žádný
neexistovalo	existovat	k5eNaImAgNnS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Iraq	Iraq	k1gFnSc1	Iraq
Survey	Survea	k1gFnSc2	Survea
Group	Group	k1gMnSc1	Group
(	(	kIx(	(
<g/>
ISG	ISG	kA	ISG
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
najít	najít	k5eAaPmF	najít
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
zbraně	zbraň	k1gFnSc2	zbraň
hromadného	hromadný	k2eAgNnSc2d1	hromadné
ničení	ničení	k1gNnSc2	ničení
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
Husajnův	Husajnův	k2eAgInSc4d1	Husajnův
režim	režim	k1gInSc4	režim
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
a	a	k8xC	a
vyvíjel	vyvíjet	k5eAaImAgMnS	vyvíjet
<g/>
,	,	kIx,	,
napsala	napsat	k5eAaPmAgFnS	napsat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
závěrečné	závěrečný	k2eAgFnSc6d1	závěrečná
zprávě	zpráva	k1gFnSc6	zpráva
(	(	kIx(	(
<g/>
známé	známý	k2eAgNnSc1d1	známé
také	také	k9	také
jako	jako	k8xC	jako
Duelfer	Duelfer	k1gMnSc1	Duelfer
Report	report	k1gInSc1	report
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
byly	být	k5eAaImAgFnP	být
nalezeny	nalézt	k5eAaBmNgFnP	nalézt
chemické	chemický	k2eAgFnPc1d1	chemická
ZHN	ZHN	kA	ZHN
vyrobené	vyrobený	k2eAgInPc4d1	vyrobený
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1996	[number]	k4	1996
a	a	k8xC	a
výrobní	výrobní	k2eAgNnSc4d1	výrobní
zařízení	zařízení	k1gNnSc4	zařízení
a	a	k8xC	a
množství	množství	k1gNnSc4	množství
chemikálií	chemikálie	k1gFnPc2	chemikálie
dvojího	dvojí	k4xRgNnSc2	dvojí
určení	určení	k1gNnSc2	určení
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgMnPc2	který
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
daly	dát	k5eAaPmAgFnP	dát
zbraně	zbraň	k1gFnPc1	zbraň
vytvořit	vytvořit	k5eAaPmF	vytvořit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Loftus	Loftus	k1gInSc4	Loftus
Report	report	k1gInSc1	report
vláda	vláda	k1gFnSc1	vláda
USA	USA	kA	USA
tajila	tajit	k5eAaImAgFnS	tajit
pochybení	pochybení	k1gNnSc4	pochybení
Iraq	Iraq	k1gMnSc2	Iraq
Survey	Survea	k1gMnSc2	Survea
Group	Group	k1gMnSc1	Group
(	(	kIx(	(
<g/>
ISG	ISG	kA	ISG
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
když	když	k8xS	když
objevili	objevit	k5eAaPmAgMnP	objevit
sklad	sklad	k1gInSc4	sklad
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zbraní	zbraň	k1gFnPc2	zbraň
a	a	k8xC	a
sklad	sklad	k1gInSc4	sklad
koncentrovaného	koncentrovaný	k2eAgInSc2d1	koncentrovaný
přirodního	přirodní	k2eAgInSc2d1	přirodní
uranu	uran	k1gInSc2	uran
až	až	k9	až
poté	poté	k6eAd1	poté
co	co	k9	co
byly	být	k5eAaImAgFnP	být
jaderné	jaderný	k2eAgFnPc1d1	jaderná
zbraně	zbraň	k1gFnPc1	zbraň
odvezeny	odvezen	k2eAgFnPc1d1	odvezena
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
izrealské	izrealský	k2eAgFnSc2d1	izrealský
zpravodajské	zpravodajský	k2eAgFnSc2d1	zpravodajská
služby	služba	k1gFnSc2	služba
do	do	k7c2	do
Sýrie	Sýrie	k1gFnSc2	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skladu	sklad	k1gInSc6	sklad
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
už	už	k6eAd1	už
jen	jen	k9	jen
koncetrovaný	koncetrovaný	k2eAgInSc4d1	koncetrovaný
přírodní	přírodní	k2eAgInSc4d1	přírodní
uran	uran	k1gInSc4	uran
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
rezoluce	rezoluce	k1gFnSc2	rezoluce
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
č.	č.	k?	č.
678	[number]	k4	678
a	a	k8xC	a
č.	č.	k?	č.
1441	[number]	k4	1441
<g/>
,	,	kIx,	,
nesměl	smět	k5eNaImAgInS	smět
Irák	Irák	k1gInSc1	Irák
vlastnit	vlastnit	k5eAaImF	vlastnit
žádné	žádný	k3yNgNnSc1	žádný
ZHN	ZHN	kA	ZHN
ani	ani	k8xC	ani
zařízení	zařízení	k1gNnSc2	zařízení
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
výrobu	výroba	k1gFnSc4	výroba
ani	ani	k8xC	ani
vést	vést	k5eAaImF	vést
žádné	žádný	k3yNgInPc4	žádný
vědecké	vědecký	k2eAgInPc4d1	vědecký
programy	program	k1gInPc4	program
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
by	by	kYmCp3nP	by
k	k	k7c3	k
držení	držení	k1gNnSc6	držení
ZHN	ZHN	kA	ZHN
směřovaly	směřovat	k5eAaImAgFnP	směřovat
<g/>
.	.	kIx.	.
</s>
<s>
Bush	Bush	k1gMnSc1	Bush
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Saddám	Saddám	k1gMnSc1	Saddám
může	moct	k5eAaImIp3nS	moct
dodat	dodat	k5eAaPmF	dodat
ZHN	ZHN	kA	ZHN
teroristům	terorista	k1gMnPc3	terorista
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Al-Kajdě	Al-Kajd	k1gInSc6	Al-Kajd
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
tlačil	tlačit	k5eAaImAgInS	tlačit
na	na	k7c6	na
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
důrazněji	důrazně	k6eAd2	důrazně
prosazovalo	prosazovat	k5eAaImAgNnS	prosazovat
odzbrojení	odzbrojení	k1gNnSc1	odzbrojení
Iráku	Irák	k1gInSc2	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
USA	USA	kA	USA
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
v	v	k7c6	v
OSN	OSN	kA	OSN
prosadit	prosadit	k5eAaPmF	prosadit
novou	nový	k2eAgFnSc4d1	nová
rezoluci	rezoluce	k1gFnSc4	rezoluce
RB	RB	kA	RB
OSN	OSN	kA	OSN
o	o	k7c6	o
Iráku	Irák	k1gInSc6	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Hans	Hans	k1gMnSc1	Hans
Blix	Blix	k1gInSc1	Blix
a	a	k8xC	a
Mohamed	Mohamed	k1gMnSc1	Mohamed
El	Ela	k1gFnPc2	Ela
Baradei	Barade	k1gFnSc2	Barade
vedli	vést	k5eAaImAgMnP	vést
obnovené	obnovený	k2eAgFnPc4d1	obnovená
inspekce	inspekce	k1gFnPc4	inspekce
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
vyskytlo	vyskytnout	k5eAaPmAgNnS	vyskytnout
několik	několik	k4yIc1	několik
problémů	problém	k1gInPc2	problém
a	a	k8xC	a
omezení	omezení	k1gNnSc4	omezení
pohybu	pohyb	k1gInSc2	pohyb
inspektorů	inspektor	k1gMnPc2	inspektor
iráckou	irácký	k2eAgFnSc7d1	irácká
vládou	vláda	k1gFnSc7	vláda
<g/>
,	,	kIx,	,
když	když	k8xS	když
jim	on	k3xPp3gInPc3	on
nebyla	být	k5eNaImAgFnS	být
zpřístupněna	zpřístupněn	k2eAgFnSc1d1	zpřístupněna
některá	některý	k3yIgNnPc4	některý
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Problémy	problém	k1gInPc1	problém
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
diskuzi	diskuze	k1gFnSc3	diskuze
o	o	k7c6	o
účinnosti	účinnost	k1gFnSc6	účinnost
inspekcí	inspekce	k1gFnPc2	inspekce
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
Saddám	Saddám	k1gMnSc1	Saddám
dopaden	dopadnout	k5eAaPmNgMnS	dopadnout
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
vyšetřovatelé	vyšetřovatel	k1gMnPc1	vyšetřovatel
ptali	ptat	k5eAaImAgMnP	ptat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Když	když	k8xS	když
jste	být	k5eAaImIp2nP	být
neměli	mít	k5eNaImAgMnP	mít
ZHN	ZHN	kA	ZHN
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
jste	být	k5eAaImIp2nP	být
nevpustili	vpustit	k5eNaPmAgMnP	vpustit
inspektory	inspektor	k1gMnPc4	inspektor
OSN	OSN	kA	OSN
do	do	k7c2	do
některých	některý	k3yIgNnPc2	některý
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
?	?	kIx.	?
</s>
<s>
"	"	kIx"	"
Husajn	Husajn	k1gMnSc1	Husajn
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nechtěli	chtít	k5eNaImAgMnP	chtít
jsme	být	k5eAaImIp1nP	být
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
do	do	k7c2	do
našich	náš	k3xOp1gInPc2	náš
prezidentských	prezidentský	k2eAgInPc2d1	prezidentský
prostorů	prostor	k1gInPc2	prostor
a	a	k8xC	a
narušovali	narušovat	k5eAaImAgMnP	narušovat
naše	náš	k3xOp1gNnPc4	náš
soukromí	soukromí	k1gNnPc4	soukromí
<g/>
.	.	kIx.	.
"	"	kIx"	"
Ministr	ministr	k1gMnSc1	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
Colin	Colin	k1gMnSc1	Colin
Powell	Powell	k1gMnSc1	Powell
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
zabezpečit	zabezpečit	k5eAaPmF	zabezpečit
pro	pro	k7c4	pro
operaci	operace	k1gFnSc4	operace
podporu	podpor	k1gInSc2	podpor
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
se	se	k3xPyFc4	se
pokoušely	pokoušet	k5eAaImAgInP	pokoušet
prosadit	prosadit	k5eAaPmF	prosadit
další	další	k2eAgFnSc4d1	další
rezoluci	rezoluce	k1gFnSc4	rezoluce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
povolila	povolit	k5eAaPmAgFnS	povolit
použití	použití	k1gNnSc3	použití
vojenské	vojenský	k2eAgFnSc2d1	vojenská
síly	síla	k1gFnSc2	síla
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
Kapitoly	kapitola	k1gFnSc2	kapitola
VII	VII	kA	VII
Charty	charta	k1gFnSc2	charta
OSN	OSN	kA	OSN
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
ideu	idea	k1gFnSc4	idea
posléze	posléze	k6eAd1	posléze
zavrhly	zavrhnout	k5eAaPmAgInP	zavrhnout
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
chtěly	chtít	k5eAaImAgFnP	chtít
vyhnout	vyhnout	k5eAaPmF	vyhnout
zápornému	záporný	k2eAgNnSc3d1	záporné
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
(	(	kIx(	(
<g/>
navíc	navíc	k6eAd1	navíc
Francie	Francie	k1gFnSc1	Francie
hrozila	hrozit	k5eAaImAgFnS	hrozit
použitím	použití	k1gNnSc7	použití
svého	svůj	k3xOyFgNnSc2	svůj
práva	právo	k1gNnSc2	právo
veta	veta	k6eAd1	veta
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
vytvořily	vytvořit	k5eAaPmAgInP	vytvořit
skupinu	skupina	k1gFnSc4	skupina
přibližně	přibližně	k6eAd1	přibližně
40	[number]	k4	40
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
Polska	Polsko	k1gNnSc2	Polsko
či	či	k8xC	či
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
Bush	Bush	k1gMnSc1	Bush
nazval	nazvat	k5eAaBmAgMnS	nazvat
"	"	kIx"	"
<g/>
koalicí	koalice	k1gFnSc7	koalice
ochotných	ochotný	k2eAgFnPc2d1	ochotná
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
koalice	koalice	k1gFnSc1	koalice
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
po	po	k7c6	po
několikadenním	několikadenní	k2eAgNnSc6d1	několikadenní
ultimátu	ultimátum	k1gNnSc6	ultimátum
do	do	k7c2	do
Iráku	Irák	k1gInSc2	Irák
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
odvolávajíc	odvolávat	k5eAaImSgFnS	odvolávat
se	se	k3xPyFc4	se
na	na	k7c4	na
několik	několik	k4yIc4	několik
rezolucí	rezoluce	k1gFnPc2	rezoluce
OSN	OSN	kA	OSN
(	(	kIx(	(
<g/>
1441	[number]	k4	1441
<g/>
,	,	kIx,	,
1205	[number]	k4	1205
<g/>
,	,	kIx,	,
1137	[number]	k4	1137
<g/>
,	,	kIx,	,
1134	[number]	k4	1134
<g/>
,	,	kIx,	,
1115	[number]	k4	1115
<g/>
,	,	kIx,	,
1060	[number]	k4	1060
<g/>
,	,	kIx,	,
949	[number]	k4	949
<g/>
,	,	kIx,	,
778	[number]	k4	778
<g/>
,	,	kIx,	,
715	[number]	k4	715
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
nespolupráci	nespoluprák	k1gMnPc1	nespoluprák
Iráku	Irák	k1gInSc2	Irák
při	při	k7c6	při
jejich	jejich	k3xOp3gInSc6	jejich
výkonu	výkon	k1gInSc6	výkon
<g/>
,	,	kIx,	,
odmítání	odmítání	k1gNnSc3	odmítání
spolupráce	spolupráce	k1gFnSc2	spolupráce
s	s	k7c7	s
inspektory	inspektor	k1gMnPc7	inspektor
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
Saddámův	Saddámův	k2eAgInSc1d1	Saddámův
údajný	údajný	k2eAgInSc1d1	údajný
pokus	pokus	k1gInSc1	pokus
zavraždit	zavraždit	k5eAaPmF	zavraždit
bývalého	bývalý	k2eAgMnSc4d1	bývalý
prezidenta	prezident	k1gMnSc4	prezident
George	Georg	k1gMnSc2	Georg
H.	H.	kA	H.
W.	W.	kA	W.
Bushe	Bush	k1gMnSc2	Bush
v	v	k7c6	v
Kuvajtu	Kuvajt	k1gInSc6	Kuvajt
a	a	k8xC	a
zneužívání	zneužívání	k1gNnSc1	zneužívání
a	a	k8xC	a
nedodržování	nedodržování	k1gNnSc1	nedodržování
příměří	příměří	k1gNnSc2	příměří
uzavřeného	uzavřený	k2eAgNnSc2d1	uzavřené
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
koalice	koalice	k1gFnSc2	koalice
tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
tyto	tento	k3xDgFnPc4	tento
okolnosti	okolnost	k1gFnPc4	okolnost
opravňují	opravňovat	k5eAaImIp3nP	opravňovat
k	k	k7c3	k
legálnímu	legální	k2eAgNnSc3d1	legální
použití	použití	k1gNnSc3	použití
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
světoví	světový	k2eAgMnPc1d1	světový
vůdci	vůdce	k1gMnPc1	vůdce
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Kofi	Kofi	k1gNnSc1	Kofi
Annan	Annan	k1gMnSc1	Annan
<g/>
,	,	kIx,	,
Jacques	Jacques	k1gMnSc1	Jacques
Chirac	Chirac	k1gMnSc1	Chirac
či	či	k8xC	či
Gerhard	Gerhard	k1gMnSc1	Gerhard
Schröder	Schröder	k1gMnSc1	Schröder
<g/>
)	)	kIx)	)
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
tvrzením	tvrzení	k1gNnSc7	tvrzení
nesouhlasili	souhlasit	k5eNaImAgMnP	souhlasit
a	a	k8xC	a
označili	označit	k5eAaPmAgMnP	označit
válku	válka	k1gFnSc4	válka
za	za	k7c4	za
ilegální	ilegální	k2eAgMnPc4d1	ilegální
<g/>
.	.	kIx.	.
</s>
<s>
Koalice	koalice	k1gFnSc1	koalice
snadno	snadno	k6eAd1	snadno
porazila	porazit	k5eAaPmAgFnS	porazit
regulérní	regulérní	k2eAgFnSc4d1	regulérní
Iráckou	irácký	k2eAgFnSc4d1	irácká
armádu	armáda	k1gFnSc4	armáda
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2003	[number]	k4	2003
Bush	Bush	k1gMnSc1	Bush
ohlásil	ohlásit	k5eAaPmAgMnS	ohlásit
konec	konec	k1gInSc4	konec
hlavních	hlavní	k2eAgFnPc2d1	hlavní
vojenských	vojenský	k2eAgFnPc2d1	vojenská
operací	operace	k1gFnPc2	operace
<g/>
.	.	kIx.	.
</s>
<s>
Objevily	objevit	k5eAaPmAgInP	objevit
se	se	k3xPyFc4	se
však	však	k9	však
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
gerilovými	gerilový	k2eAgFnPc7d1	gerilová
jednotkami	jednotka	k1gFnPc7	jednotka
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
vážnost	vážnost	k1gFnSc4	vážnost
s	s	k7c7	s
časem	čas	k1gInSc7	čas
spíše	spíše	k9	spíše
stoupala	stoupat	k5eAaImAgFnS	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
Veřejné	veřejný	k2eAgNnSc1d1	veřejné
mínění	mínění	k1gNnSc1	mínění
v	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
s	s	k7c7	s
narůstajícím	narůstající	k2eAgInSc7d1	narůstající
odporem	odpor	k1gInSc7	odpor
partyzánských	partyzánský	k2eAgInPc2d1	partyzánský
a	a	k8xC	a
teroristických	teroristický	k2eAgFnPc2d1	teroristická
bojůvek	bojůvka	k1gFnPc2	bojůvka
odkláněla	odklánět	k5eAaImAgFnS	odklánět
od	od	k7c2	od
přesvědčení	přesvědčení	k1gNnSc2	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
prezident	prezident	k1gMnSc1	prezident
Bush	Bush	k1gMnSc1	Bush
zvládá	zvládat	k5eAaImIp3nS	zvládat
situaci	situace	k1gFnSc4	situace
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
dobře	dobře	k6eAd1	dobře
<g/>
.	.	kIx.	.
</s>
<s>
Vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
po	po	k7c6	po
bojích	boj	k1gInPc6	boj
neprokázalo	prokázat	k5eNaPmAgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
Saddám	Saddám	k1gMnSc1	Saddám
Hussajn	Hussajn	k1gMnSc1	Hussajn
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
ZHN	ZHN	kA	ZHN
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
ohrozit	ohrozit	k5eAaPmF	ohrozit
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Saddám	Saddám	k1gMnSc1	Saddám
Husssajn	Husssajn	k1gMnSc1	Husssajn
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
jen	jen	k9	jen
rakety	raketa	k1gFnPc1	raketa
Scud	Scuda	k1gFnPc2	Scuda
s	s	k7c7	s
doletem	dolet	k1gInSc7	dolet
do	do	k7c2	do
900	[number]	k4	900
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
závěry	závěr	k1gInPc7	závěr
vyšetřovatelů	vyšetřovatel	k1gMnPc2	vyšetřovatel
však	však	k9	však
byly	být	k5eAaImAgInP	být
i	i	k9	i
údaje	údaj	k1gInPc1	údaj
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Husajnova	Husajnův	k2eAgFnSc1d1	Husajnova
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
snažila	snažit	k5eAaImAgFnS	snažit
získat	získat	k5eAaPmF	získat
technologie	technologie	k1gFnPc4	technologie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
umožnily	umožnit	k5eAaPmAgFnP	umožnit
Iráku	Irák	k1gInSc3	Irák
produkovat	produkovat	k5eAaImF	produkovat
ZHN	ZHN	kA	ZHN
bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c4	po
uvolnění	uvolnění	k1gNnSc4	uvolnění
sankcí	sankce	k1gFnPc2	sankce
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
závěry	závěr	k1gInPc4	závěr
patřilo	patřit	k5eAaImAgNnS	patřit
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Saddám	Saddám	k1gMnSc1	Saddám
měl	mít	k5eAaImAgMnS	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
i	i	k9	i
střely	střel	k1gInPc4	střel
s	s	k7c7	s
větším	veliký	k2eAgInSc7d2	veliký
doletem	dolet	k1gInSc7	dolet
<g/>
,	,	kIx,	,
než	než	k8xS	než
mu	on	k3xPp3gMnSc3	on
povolovaly	povolovat	k5eAaImAgFnP	povolovat
sankce	sankce	k1gFnPc1	sankce
(	(	kIx(	(
<g/>
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
války	válka	k1gFnSc2	válka
je	on	k3xPp3gInPc4	on
použil	použít	k5eAaPmAgMnS	použít
k	k	k7c3	k
ostřelování	ostřelování	k1gNnSc3	ostřelování
Kuvajtu	Kuvajt	k1gInSc2	Kuvajt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působila	působit	k5eAaImAgFnS	působit
česko-slovenská	českolovenský	k2eAgFnSc1d1	česko-slovenská
jednotka	jednotka	k1gFnSc1	jednotka
chemické	chemický	k2eAgFnSc2d1	chemická
<g/>
,	,	kIx,	,
biologické	biologický	k2eAgFnSc2d1	biologická
a	a	k8xC	a
radiační	radiační	k2eAgFnSc2d1	radiační
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bush	Bush	k1gMnSc1	Bush
později	pozdě	k6eAd2	pozdě
bránil	bránit	k5eAaImAgMnS	bránit
své	svůj	k3xOyFgNnSc4	svůj
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
jít	jít	k5eAaImF	jít
do	do	k7c2	do
války	válka	k1gFnSc2	válka
tvrzením	tvrzení	k1gNnSc7	tvrzení
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Svět	svět	k1gInSc1	svět
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
bezpečnější	bezpečný	k2eAgNnSc1d2	bezpečnější
<g/>
.	.	kIx.	.
"	"	kIx"	"
Vyvstaly	vyvstat	k5eAaPmAgFnP	vyvstat
i	i	k9	i
další	další	k2eAgFnPc1d1	další
otázky	otázka	k1gFnPc1	otázka
-	-	kIx~	-
zejména	zejména	k9	zejména
ohledně	ohledně	k7c2	ohledně
možné	možný	k2eAgFnSc2d1	možná
zaujatosti	zaujatost	k1gFnSc2	zaujatost
či	či	k8xC	či
upravenosti	upravenost	k1gFnSc2	upravenost
informací	informace	k1gFnPc2	informace
a	a	k8xC	a
zpráv	zpráva	k1gFnPc2	zpráva
tajných	tajný	k2eAgFnPc2d1	tajná
služeb	služba	k1gFnPc2	služba
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
pokud	pokud	k8xS	pokud
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
ZHN	ZHN	kA	ZHN
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
oficiálního	oficiální	k2eAgNnSc2d1	oficiální
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
vedeného	vedený	k2eAgNnSc2d1	vedené
v	v	k7c6	v
USA	USA	kA	USA
vláda	vláda	k1gFnSc1	vláda
nezasahovala	zasahovat	k5eNaImAgFnS	zasahovat
do	do	k7c2	do
činnosti	činnost	k1gFnSc2	činnost
tajné	tajný	k2eAgFnSc2d1	tajná
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
témuž	týž	k3xTgInSc3	týž
závěru	závěr	k1gInSc3	závěr
dospěla	dochvít	k5eAaPmAgFnS	dochvít
i	i	k9	i
komise	komise	k1gFnSc1	komise
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
analytici	analytik	k1gMnPc1	analytik
hodnotí	hodnotit	k5eAaImIp3nP	hodnotit
toto	tento	k3xDgNnSc4	tento
selhání	selhání	k1gNnSc4	selhání
jako	jako	k8xC	jako
přehnanou	přehnaný	k2eAgFnSc4d1	přehnaná
snahu	snaha	k1gFnSc4	snaha
tajných	tajný	k2eAgFnPc2d1	tajná
služeb	služba	k1gFnPc2	služba
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
nedokázaly	dokázat	k5eNaPmAgFnP	dokázat
zabránit	zabránit	k5eAaPmF	zabránit
útokům	útok	k1gInPc3	útok
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
otázky	otázka	k1gFnPc4	otázka
patřily	patřit	k5eAaImAgInP	patřit
vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
Evropou	Evropa	k1gFnSc7	Evropa
<g/>
,	,	kIx,	,
úloha	úloha	k1gFnSc1	úloha
a	a	k8xC	a
funkce	funkce	k1gFnSc1	funkce
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
a	a	k8xC	a
vliv	vliv	k1gInSc4	vliv
války	válka	k1gFnSc2	válka
na	na	k7c4	na
okolní	okolní	k2eAgFnPc4d1	okolní
země	zem	k1gFnPc4	zem
<g/>
:	:	kIx,	:
Írán	Írán	k1gInSc1	Írán
<g/>
,	,	kIx,	,
Sýrii	Sýrie	k1gFnSc4	Sýrie
<g/>
,	,	kIx,	,
Libanon	Libanon	k1gInSc4	Libanon
a	a	k8xC	a
Turecko	Turecko	k1gNnSc4	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
George	George	k1gFnSc7	George
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
pracoval	pracovat	k5eAaImAgMnS	pracovat
původně	původně	k6eAd1	původně
v	v	k7c6	v
ropném	ropný	k2eAgInSc6d1	ropný
průmyslu	průmysl	k1gInSc6	průmysl
a	a	k8xC	a
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
prostředí	prostředí	k1gNnSc2	prostředí
udržuje	udržovat	k5eAaImIp3nS	udržovat
vztahy	vztah	k1gInPc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
kritici	kritik	k1gMnPc1	kritik
ho	on	k3xPp3gNnSc4	on
proto	proto	k8xC	proto
často	často	k6eAd1	často
obviňují	obviňovat	k5eAaImIp3nP	obviňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgNnSc1	některý
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
vydává	vydávat	k5eAaPmIp3nS	vydávat
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
ropné	ropný	k2eAgFnSc2d1	ropná
lobby	lobby	k1gFnSc2	lobby
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
komentátoři	komentátor	k1gMnPc1	komentátor
považují	považovat	k5eAaImIp3nP	považovat
ropné	ropný	k2eAgInPc4d1	ropný
zájmy	zájem	k1gInPc4	zájem
USA	USA	kA	USA
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
motivací	motivace	k1gFnPc2	motivace
při	při	k7c6	při
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Mickeyho	Mickey	k1gMnSc2	Mickey
Herskowitze	Herskowitze	k1gFnSc2	Herskowitze
mu	on	k3xPp3gMnSc3	on
Bush	Bush	k1gMnSc1	Bush
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
klíčů	klíč	k1gInPc2	klíč
od	od	k7c2	od
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
být	být	k5eAaImF	být
lidmi	člověk	k1gMnPc7	člověk
viděn	vidět	k5eAaImNgMnS	vidět
jako	jako	k8xC	jako
velký	velký	k2eAgMnSc1d1	velký
vůdce	vůdce	k1gMnSc1	vůdce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
velitelem	velitel	k1gMnSc7	velitel
<g/>
.	.	kIx.	.
</s>
<s>
Můj	můj	k3xOp1gMnSc1	můj
otec	otec	k1gMnSc1	otec
měl	mít	k5eAaImAgMnS	mít
celý	celý	k2eAgInSc4d1	celý
tento	tento	k3xDgInSc4	tento
politický	politický	k2eAgInSc4d1	politický
kapitál	kapitál	k1gInSc4	kapitál
<g/>
,	,	kIx,	,
když	když	k8xS	když
vyhnal	vyhnat	k5eAaPmAgInS	vyhnat
Iráčany	Iráčan	k1gMnPc4	Iráčan
z	z	k7c2	z
Kuvajtu	Kuvajt	k1gInSc2	Kuvajt
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
ho	on	k3xPp3gInSc4	on
promrhal	promrhat	k5eAaPmAgMnS	promrhat
<g/>
.	.	kIx.	.
</s>
<s>
Kdybych	kdyby	kYmCp1nS	kdyby
já	já	k3xPp1nSc1	já
měl	mít	k5eAaImAgInS	mít
podobnou	podobný	k2eAgFnSc4d1	podobná
šanci	šance	k1gFnSc4	šance
<g/>
,	,	kIx,	,
kdybych	kdyby	kYmCp1nS	kdyby
já	já	k3xPp1nSc1	já
měl	mít	k5eAaImAgMnS	mít
tolik	tolik	k6eAd1	tolik
kapitálu	kapitál	k1gInSc2	kapitál
<g/>
,	,	kIx,	,
já	já	k3xPp1nSc1	já
ho	on	k3xPp3gMnSc4	on
nepromrhám	promrhat	k5eNaPmIp1nS	promrhat
<g/>
.	.	kIx.	.
</s>
<s>
Překonám	překonat	k5eAaPmIp1nS	překonat
všechno	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
překonat	překonat	k5eAaPmF	překonat
chci	chtít	k5eAaImIp1nS	chtít
a	a	k8xC	a
moje	můj	k3xOp1gInPc4	můj
období	období	k1gNnSc4	období
v	v	k7c6	v
prezidentském	prezidentský	k2eAgInSc6d1	prezidentský
úřadě	úřad	k1gInSc6	úřad
bude	být	k5eAaImBp3nS	být
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
.	.	kIx.	.
"	"	kIx"	"
Informaci	informace	k1gFnSc4	informace
přinesl	přinést	k5eAaPmAgInS	přinést
internetový	internetový	k2eAgInSc1d1	internetový
server	server	k1gInSc1	server
"	"	kIx"	"
<g/>
Guerrilla	Guerrilla	k1gFnSc1	Guerrilla
News	News	k1gInSc1	News
Network	network	k1gInSc1	network
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Žádné	žádný	k3yNgNnSc1	žádný
významnejší	významný	k2eAgNnSc1d2	významnější
médium	médium	k1gNnSc1	médium
však	však	k9	však
tento	tento	k3xDgInSc4	tento
výrok	výrok	k1gInSc4	výrok
nezveřejnilo	zveřejnit	k5eNaPmAgNnS	zveřejnit
a	a	k8xC	a
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
autenticita	autenticita	k1gFnSc1	autenticita
je	být	k5eAaImIp3nS	být
pochybná	pochybný	k2eAgFnSc1d1	pochybná
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
kritiků	kritik	k1gMnPc2	kritik
Bushovi	Bushův	k2eAgMnPc1d1	Bushův
poradci	poradce	k1gMnPc1	poradce
zastávali	zastávat	k5eAaImAgMnP	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
shrnout	shrnout	k5eAaPmF	shrnout
do	do	k7c2	do
věty	věta	k1gFnSc2	věta
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Vyber	vybrat	k5eAaPmRp2nS	vybrat
si	se	k3xPyFc3	se
malou	malý	k2eAgFnSc4d1	malá
zem	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
oprávněné	oprávněný	k2eAgNnSc1d1	oprávněné
z	z	k7c2	z
nějakého	nějaký	k3yIgInSc2	nějaký
důvodu	důvod	k1gInSc2	důvod
vstoupit	vstoupit	k5eAaPmF	vstoupit
a	a	k8xC	a
proveď	provést	k5eAaPmRp2nS	provést
invazi	invaze	k1gFnSc3	invaze
<g/>
.	.	kIx.	.
</s>
<s>
Vojáci	voják	k1gMnPc1	voják
vracející	vracející	k2eAgFnSc2d1	vracející
se	se	k3xPyFc4	se
z	z	k7c2	z
války	válka	k1gFnSc2	válka
budou	být	k5eAaImBp3nP	být
pro	pro	k7c4	pro
lid	lid	k1gInSc4	lid
hrdinové	hrdina	k1gMnPc1	hrdina
a	a	k8xC	a
prezident	prezident	k1gMnSc1	prezident
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
Margaret	Margareta	k1gFnPc2	Margareta
Thatcherová	Thatcherový	k2eAgFnSc1d1	Thatcherová
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
o	o	k7c4	o
Falklandy	Falkland	k1gInPc4	Falkland
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Příznivci	příznivec	k1gMnPc1	příznivec
však	však	k9	však
upozorňují	upozorňovat	k5eAaImIp3nP	upozorňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
si	se	k3xPyFc3	se
Bush	Bush	k1gMnSc1	Bush
mohl	moct	k5eAaImAgMnS	moct
"	"	kIx"	"
<g/>
vybrat	vybrat	k5eAaPmF	vybrat
<g/>
"	"	kIx"	"
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
menších	malý	k2eAgFnPc2d2	menší
a	a	k8xC	a
snadněji	snadno	k6eAd2	snadno
zvládnutelných	zvládnutelný	k2eAgFnPc2d1	zvládnutelná
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
Irák	Irák	k1gInSc1	Irák
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
mu	on	k3xPp3gMnSc3	on
spíše	spíše	k9	spíše
hlasy	hlas	k1gInPc7	hlas
ubral	ubrat	k5eAaPmAgMnS	ubrat
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
"	"	kIx"	"
<g/>
exit	exit	k1gInSc1	exit
polls	polls	k1gInSc1	polls
<g/>
"	"	kIx"	"
uváděných	uváděný	k2eAgInPc2d1	uváděný
CNN	CNN	kA	CNN
od	od	k7c2	od
Gallupu	Gallup	k1gInSc2	Gallup
po	po	k7c6	po
<g />
.	.	kIx.	.
</s>
<s>
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
Kerry	Kerra	k1gFnSc2	Kerra
získal	získat	k5eAaPmAgInS	získat
73	[number]	k4	73
<g/>
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
respondentů	respondent	k1gMnPc2	respondent
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
považovali	považovat	k5eAaImAgMnP	považovat
Irák	Irák	k1gInSc4	Irák
za	za	k7c4	za
klíčové	klíčový	k2eAgNnSc4d1	klíčové
téma	téma	k1gNnSc4	téma
<g/>
;	;	kIx,	;
též	též	k9	též
mnoho	mnoho	k4c4	mnoho
respondentů	respondent	k1gMnPc2	respondent
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
označili	označit	k5eAaPmAgMnP	označit
jako	jako	k9	jako
Bushovi	Bushův	k2eAgMnPc1d1	Bushův
voliči	volič	k1gMnPc1	volič
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bushe	Bush	k1gMnSc4	Bush
již	již	k6eAd1	již
nevolí	volit	k5eNaImIp3nS	volit
kvůli	kvůli	k7c3	kvůli
Iráku	Irák	k1gInSc3	Irák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bush	Bush	k1gMnSc1	Bush
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
o	o	k7c4	o
obnovení	obnovení	k1gNnSc4	obnovení
mírového	mírový	k2eAgInSc2d1	mírový
procesu	proces	k1gInSc2	proces
mezi	mezi	k7c7	mezi
Izraelem	Izrael	k1gInSc7	Izrael
a	a	k8xC	a
Palestinci	Palestinec	k1gMnPc1	Palestinec
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
vytvořit	vytvořit	k5eAaPmF	vytvořit
samostatný	samostatný	k2eAgInSc4d1	samostatný
palestinský	palestinský	k2eAgInSc4d1	palestinský
stát	stát	k1gInSc4	stát
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
,	,	kIx,	,
některými	některý	k3yIgFnPc7	některý
evropskými	evropský	k2eAgFnPc7d1	Evropská
zeměmi	zem	k1gFnPc7	zem
a	a	k8xC	a
OSN	OSN	kA	OSN
plán	plán	k1gInSc4	plán
<g/>
,	,	kIx,	,
takzvanou	takzvaný	k2eAgFnSc4d1	takzvaná
Cestovní	cestovní	k2eAgFnSc4d1	cestovní
mapu	mapa	k1gFnSc4	mapa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
ústupky	ústupek	k1gInPc4	ústupek
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
časový	časový	k2eAgInSc1d1	časový
harmonogram	harmonogram	k1gInSc1	harmonogram
kroků	krok	k1gInPc2	krok
vedoucích	vedoucí	k1gFnPc2	vedoucí
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
palestinského	palestinský	k2eAgInSc2d1	palestinský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
10	[number]	k4	10
<g/>
]	]	kIx)	]
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2003	[number]	k4	2003
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Palestině	Palestina	k1gFnSc6	Palestina
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
nový	nový	k2eAgMnSc1d1	nový
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
Mahmúd	Mahmúd	k1gMnSc1	Mahmúd
Abbás	Abbás	k1gInSc1	Abbás
<g/>
.	.	kIx.	.
</s>
<s>
Cestovní	cestovní	k2eAgFnSc1d1	cestovní
mapa	mapa	k1gFnSc1	mapa
však	však	k9	však
upadla	upadnout	k5eAaPmAgFnS	upadnout
v	v	k7c4	v
zapomenutí	zapomenutí	k1gNnSc4	zapomenutí
po	po	k7c6	po
několika	několik	k4yIc6	několik
měsících	měsíc	k1gInPc6	měsíc
násilí	násilí	k1gNnSc2	násilí
<g/>
.	.	kIx.	.
</s>
<s>
Abbás	Abbás	k6eAd1	Abbás
poté	poté	k6eAd1	poté
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemá	mít	k5eNaImIp3nS	mít
dostatek	dostatek	k1gInSc4	dostatek
pravomocí	pravomoc	k1gFnPc2	pravomoc
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
násilí	násilí	k1gNnSc4	násilí
zamezil	zamezit	k5eAaPmAgMnS	zamezit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
ani	ani	k9	ani
jedna	jeden	k4xCgFnSc1	jeden
strana	strana	k1gFnSc1	strana
nevykonala	vykonat	k5eNaPmAgFnS	vykonat
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
bylo	být	k5eAaImAgNnS	být
požadované	požadovaný	k2eAgNnSc1d1	požadované
v	v	k7c6	v
plánu	plán	k1gInSc6	plán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2004	[number]	k4	2004
Bush	Bush	k1gMnSc1	Bush
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
schvaluje	schvalovat	k5eAaImIp3nS	schvalovat
plán	plán	k1gInSc1	plán
izraelského	izraelský	k2eAgMnSc2d1	izraelský
premiéra	premiér	k1gMnSc2	premiér
Ariela	Ariel	k1gMnSc2	Ariel
Šarona	Šaron	k1gMnSc2	Šaron
na	na	k7c4	na
stažení	stažení	k1gNnSc4	stažení
izraelských	izraelský	k2eAgFnPc2d1	izraelská
jednotek	jednotka	k1gFnPc2	jednotka
i	i	k9	i
osadníků	osadník	k1gMnPc2	osadník
z	z	k7c2	z
pásma	pásmo	k1gNnSc2	pásmo
Gazy	Gaza	k1gFnSc2	Gaza
a	a	k8xC	a
zachování	zachování	k1gNnSc4	zachování
židovských	židovský	k2eAgFnPc2d1	židovská
osad	osada	k1gFnPc2	osada
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
břehu	břeh	k1gInSc6	břeh
Jordánu	Jordán	k1gInSc2	Jordán
<g/>
.	.	kIx.	.
</s>
<s>
Též	též	k6eAd1	též
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
se	s	k7c7	s
Šaronovou	Šaronový	k2eAgFnSc7d1	Šaronový
politikou	politika	k1gFnSc7	politika
odmítání	odmítání	k1gNnSc2	odmítání
práva	právo	k1gNnSc2	právo
na	na	k7c4	na
návrat	návrat	k1gInSc4	návrat
tzv.	tzv.	kA	tzv.
arabských	arabský	k2eAgMnPc2d1	arabský
uprchlíků	uprchlík	k1gMnPc2	uprchlík
z	z	k7c2	z
Izraele	Izrael	k1gInSc2	Izrael
a	a	k8xC	a
přilehlých	přilehlý	k2eAgFnPc2d1	přilehlá
území	území	k1gNnSc4	území
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
odsouzení	odsouzení	k1gNnSc3	odsouzení
plánu	plán	k1gInSc2	plán
palestinským	palestinský	k2eAgMnSc7d1	palestinský
prezidentem	prezident	k1gMnSc7	prezident
Jásirem	Jásir	k1gMnSc7	Jásir
Arafatem	Arafat	k1gMnSc7	Arafat
<g/>
,	,	kIx,	,
arabskými	arabský	k2eAgInPc7d1	arabský
státy	stát	k1gInPc7	stát
a	a	k8xC	a
částí	část	k1gFnSc7	část
evropských	evropský	k2eAgFnPc2d1	Evropská
vlád	vláda	k1gFnPc2	vláda
[	[	kIx(	[
<g/>
11	[number]	k4	11
<g/>
]	]	kIx)	]
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
změnou	změna	k1gFnSc7	změna
politiky	politika	k1gFnSc2	politika
USA	USA	kA	USA
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
poprvé	poprvé	k6eAd1	poprvé
Bush	Bush	k1gMnSc1	Bush
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Evropu	Evropa	k1gFnSc4	Evropa
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
tvrdou	tvrdý	k2eAgFnSc7d1	tvrdá
kritikou	kritika	k1gFnSc7	kritika
svého	svůj	k3xOyFgInSc2	svůj
odmítavého	odmítavý	k2eAgInSc2d1	odmítavý
postoje	postoj	k1gInSc2	postoj
ke	k	k7c3	k
Kjótskému	Kjótský	k2eAgInSc3d1	Kjótský
protokolu	protokol	k1gInSc3	protokol
<g/>
.	.	kIx.	.
</s>
<s>
Vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
USA	USA	kA	USA
a	a	k8xC	a
podstatnou	podstatný	k2eAgFnSc7d1	podstatná
částí	část	k1gFnSc7	část
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
se	se	k3xPyFc4	se
i	i	k9	i
nadále	nadále	k6eAd1	nadále
zhoršovaly	zhoršovat	k5eAaImAgFnP	zhoršovat
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
po	po	k7c6	po
projevech	projev	k1gInPc6	projev
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgNnPc6	který
Bush	Bush	k1gMnSc1	Bush
hovořil	hovořit	k5eAaImAgMnS	hovořit
o	o	k7c4	o
"	"	kIx"	"
<g/>
Ose	osa	k1gFnSc3	osa
zla	zlo	k1gNnSc2	zlo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
nejhorší	zlý	k2eAgFnSc4d3	nejhorší
úroveň	úroveň	k1gFnSc4	úroveň
klesly	klesnout	k5eAaPmAgFnP	klesnout
<g/>
,	,	kIx,	,
když	když	k8xS	když
USA	USA	kA	USA
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
spojenci	spojenec	k1gMnPc1	spojenec
zasáhli	zasáhnout	k5eAaPmAgMnP	zasáhnout
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
ministr	ministr	k1gMnSc1	ministr
obrany	obrana	k1gFnSc2	obrana
Donald	Donald	k1gMnSc1	Donald
Rumsfeld	Rumsfeld	k1gMnSc1	Rumsfeld
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
projevu	projev	k1gInSc6	projev
rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
Evropu	Evropa	k1gFnSc4	Evropa
na	na	k7c4	na
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
starou	starý	k2eAgFnSc4d1	stará
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tvořenou	tvořený	k2eAgFnSc4d1	tvořená
"	"	kIx"	"
<g/>
protiamerickou	protiamerický	k2eAgFnSc4d1	protiamerická
<g/>
"	"	kIx"	"
skupinu	skupina	k1gFnSc4	skupina
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
<g/>
,	,	kIx,	,
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
Belgií	Belgie	k1gFnSc7	Belgie
a	a	k8xC	a
na	na	k7c4	na
"	"	kIx"	"
<g/>
novou	nový	k2eAgFnSc4d1	nová
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
proamerickou	proamerický	k2eAgFnSc4d1	proamerická
<g/>
)	)	kIx)	)
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
tvořily	tvořit	k5eAaImAgFnP	tvořit
zejména	zejména	k9	zejména
země	zem	k1gFnPc1	zem
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
další	další	k2eAgFnPc1d1	další
země	zem	k1gFnPc1	zem
(	(	kIx(	(
<g/>
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc1	Dánsko
a	a	k8xC	a
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
zhoršily	zhoršit	k5eAaPmAgInP	zhoršit
vztahy	vztah	k1gInPc1	vztah
se	s	k7c7	s
Španělskem	Španělsko	k1gNnSc7	Španělsko
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
socialisté	socialist	k1gMnPc1	socialist
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gInSc6	jejichž
čele	čelo	k1gNnSc6	čelo
stál	stát	k5eAaImAgInS	stát
José	José	k1gNnSc4	José
Luis	Luisa	k1gFnPc2	Luisa
Rodríguez	Rodrígueza	k1gFnPc2	Rodrígueza
Zapatero	Zapatero	k1gNnSc1	Zapatero
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
stáhl	stáhnout	k5eAaPmAgInS	stáhnout
španělské	španělský	k2eAgFnPc4d1	španělská
jednotky	jednotka	k1gFnPc4	jednotka
z	z	k7c2	z
Iráku	Irák	k1gInSc2	Irák
a	a	k8xC	a
mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
veřejně	veřejně	k6eAd1	veřejně
a	a	k8xC	a
nepředvídavě	předvídavě	k6eNd1	předvídavě
popřál	popřát	k5eAaPmAgInS	popřát
vítězství	vítězství	k1gNnSc4	vítězství
v	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
2004	[number]	k4	2004
Johnu	John	k1gMnSc3	John
Kerrymu	Kerrym	k1gInSc2	Kerrym
<g/>
,	,	kIx,	,
tlumočíc	tlumočit	k5eAaImSgNnS	tlumočit
tak	tak	k6eAd1	tak
přání	přání	k1gNnSc1	přání
vůdců	vůdce	k1gMnPc2	vůdce
některých	některý	k3yIgFnPc2	některý
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Volby	volba	k1gFnSc2	volba
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
soupeřem	soupeř	k1gMnSc7	soupeř
Bushe	Bush	k1gMnSc2	Bush
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
byl	být	k5eAaImAgMnS	být
kandidát	kandidát	k1gMnSc1	kandidát
demokratů	demokrat	k1gMnPc2	demokrat
John	John	k1gMnSc1	John
Kerry	Kerra	k1gFnSc2	Kerra
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
senátor	senátor	k1gMnSc1	senátor
za	za	k7c4	za
stát	stát	k1gInSc4	stát
Massachusetts	Massachusetts	k1gNnSc2	Massachusetts
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
prioritní	prioritní	k2eAgNnPc4d1	prioritní
témata	téma	k1gNnPc4	téma
kampaně	kampaň	k1gFnSc2	kampaň
patřila	patřit	k5eAaImAgFnS	patřit
Válka	válka	k1gFnSc1	válka
proti	proti	k7c3	proti
terorismu	terorismus	k1gInSc3	terorismus
a	a	k8xC	a
Válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Bush	Bush	k1gMnSc1	Bush
obhajoval	obhajovat	k5eAaImAgMnS	obhajovat
svá	svůj	k3xOyFgNnPc4	svůj
rozhodnutí	rozhodnutí	k1gNnPc4	rozhodnutí
a	a	k8xC	a
Kerry	Kerra	k1gFnPc4	Kerra
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
jako	jako	k9	jako
nekompetentní	kompetentní	k2eNgMnSc1d1	nekompetentní
<g/>
.	.	kIx.	.
</s>
<s>
Republikáni	republikán	k1gMnPc1	republikán
toho	ten	k3xDgMnSc4	ten
využili	využít	k5eAaPmAgMnP	využít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vykreslili	vykreslit	k5eAaPmAgMnP	vykreslit
Kerryho	Kerry	k1gMnSc4	Kerry
jako	jako	k8xS	jako
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
mění	měnit	k5eAaImIp3nS	měnit
své	svůj	k3xOyFgInPc4	svůj
názory	názor	k1gInPc4	názor
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
hodí	hodit	k5eAaImIp3nS	hodit
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
upozorňovali	upozorňovat	k5eAaImAgMnP	upozorňovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
hlasoval	hlasovat	k5eAaImAgMnS	hlasovat
proti	proti	k7c3	proti
Válce	válka	k1gFnSc3	válka
v	v	k7c6	v
zálivu	záliv	k1gInSc6	záliv
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
podporoval	podporovat	k5eAaImAgInS	podporovat
válku	válka	k1gFnSc4	válka
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
a	a	k8xC	a
poté	poté	k6eAd1	poté
ve	v	k7c6	v
volební	volební	k2eAgFnSc6d1	volební
kampani	kampaň	k1gFnSc6	kampaň
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
války	válka	k1gFnSc2	válka
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
důležitým	důležitý	k2eAgInSc7d1	důležitý
tématem	téma	k1gNnSc7	téma
byly	být	k5eAaImAgFnP	být
tzv.	tzv.	kA	tzv.
morální	morální	k2eAgFnPc4d1	morální
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pokud	pokud	k8xS	pokud
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
interrupce	interrupce	k1gFnPc4	interrupce
<g/>
,	,	kIx,	,
sňatky	sňatek	k1gInPc4	sňatek
homosexuálů	homosexuál	k1gMnPc2	homosexuál
etc	etc	k?	etc
<g/>
.	.	kIx.	.
</s>
<s>
Kampaň	kampaň	k1gFnSc1	kampaň
byla	být	k5eAaImAgFnS	být
neobvykle	obvykle	k6eNd1	obvykle
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
a	a	k8xC	a
poměrně	poměrně	k6eAd1	poměrně
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
se	se	k3xPyFc4	se
uskutečnily	uskutečnit	k5eAaPmAgFnP	uskutečnit
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
byly	být	k5eAaImAgInP	být
opět	opět	k6eAd1	opět
těsné	těsný	k2eAgInPc1d1	těsný
a	a	k8xC	a
definitivní	definitivní	k2eAgInSc1d1	definitivní
výsledek	výsledek	k1gInSc1	výsledek
bylo	být	k5eAaImAgNnS	být
dlouho	dlouho	k6eAd1	dlouho
těžké	těžký	k2eAgNnSc1d1	těžké
určit	určit	k5eAaPmF	určit
díky	díky	k7c3	díky
sčítání	sčítání	k1gNnSc3	sčítání
hlasů	hlas	k1gInPc2	hlas
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Ohio	Ohio	k1gNnSc1	Ohio
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
rozdíl	rozdíl	k1gInSc1	rozdíl
natolik	natolik	k6eAd1	natolik
těsný	těsný	k2eAgInSc1d1	těsný
<g/>
,	,	kIx,	,
že	že	k8xS	že
dlouho	dlouho	k6eAd1	dlouho
nebylo	být	k5eNaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
vlastně	vlastně	k9	vlastně
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
však	však	k9	však
Kerry	Kerra	k1gFnPc4	Kerra
musel	muset	k5eAaImAgMnS	muset
uznat	uznat	k5eAaPmF	uznat
svoji	svůj	k3xOyFgFnSc4	svůj
porážku	porážka	k1gFnSc4	porážka
<g/>
.	.	kIx.	.
</s>
<s>
Volitelé	volitel	k1gMnPc1	volitel
se	se	k3xPyFc4	se
střetli	střetnout	k5eAaPmAgMnP	střetnout
13	[number]	k4	13
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
a	a	k8xC	a
v	v	k7c6	v
oficiálním	oficiální	k2eAgNnSc6d1	oficiální
hlasování	hlasování	k1gNnSc6	hlasování
získal	získat	k5eAaPmAgMnS	získat
Bush	Bush	k1gMnSc1	Bush
286	[number]	k4	286
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
Kerry	Kerra	k1gFnSc2	Kerra
251	[number]	k4	251
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
spolukandidát	spolukandidát	k1gMnSc1	spolukandidát
Edwards	Edwards	k1gInSc4	Edwards
1	[number]	k4	1
hlas	hlas	k1gInSc1	hlas
(	(	kIx(	(
<g/>
takzvaný	takzvaný	k2eAgInSc1d1	takzvaný
"	"	kIx"	"
<g/>
volitel	volitel	k1gMnSc1	volitel
bez	bez	k7c2	bez
víry	víra	k1gFnSc2	víra
<g/>
"	"	kIx"	"
z	z	k7c2	z
Minnesoty	Minnesota	k1gFnSc2	Minnesota
hlasoval	hlasovat	k5eAaImAgMnS	hlasovat
za	za	k7c2	za
Edwardsa	Edwards	k1gMnSc2	Edwards
jako	jako	k8xC	jako
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
i	i	k8xC	i
prezidenta	prezident	k1gMnSc2	prezident
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bush	Bush	k1gMnSc1	Bush
získal	získat	k5eAaPmAgMnS	získat
62	[number]	k4	62
040	[number]	k4	040
606	[number]	k4	606
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
představovalo	představovat	k5eAaImAgNnS	představovat
50,77	[number]	k4	50,77
%	%	kIx~	%
všech	všecek	k3xTgInPc2	všecek
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
a	a	k8xC	a
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
v	v	k7c6	v
31	[number]	k4	31
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Kerry	Kerra	k1gFnSc2	Kerra
získal	získat	k5eAaPmAgMnS	získat
59	[number]	k4	59
028	[number]	k4	028
109	[number]	k4	109
hlasů	hlas	k1gInPc2	hlas
(	(	kIx(	(
<g/>
48,31	[number]	k4	48,31
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
v	v	k7c6	v
19	[number]	k4	19
státech	stát	k1gInPc6	stát
a	a	k8xC	a
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
DC	DC	kA	DC
<g/>
.	.	kIx.	.
</s>
<s>
Bush	Bush	k1gMnSc1	Bush
byl	být	k5eAaImAgMnS	být
inaugurován	inaugurovat	k5eAaBmNgMnS	inaugurovat
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
však	však	k9	však
jsou	být	k5eAaImIp3nP	být
navzdory	navzdory	k7c3	navzdory
těsnému	těsný	k2eAgInSc3d1	těsný
výsledku	výsledek	k1gInSc3	výsledek
označované	označovaný	k2eAgFnSc2d1	označovaná
za	za	k7c4	za
významnou	významný	k2eAgFnSc4d1	významná
výhru	výhra	k1gFnSc4	výhra
republikánů	republikán	k1gMnPc2	republikán
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
z	z	k7c2	z
těchto	tento	k3xDgMnPc2	tento
důvodů	důvod	k1gInPc2	důvod
<g/>
:	:	kIx,	:
Poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
voleb	volba	k1gFnPc2	volba
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
dokázal	dokázat	k5eAaPmAgMnS	dokázat
znovuzvolený	znovuzvolený	k2eAgMnSc1d1	znovuzvolený
prezident	prezident	k1gMnSc1	prezident
posílit	posílit	k5eAaPmF	posílit
pozici	pozice	k1gFnSc4	pozice
své	svůj	k3xOyFgFnSc2	svůj
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
částech	část	k1gFnPc6	část
Kongresu	kongres	k1gInSc2	kongres
<g/>
.	.	kIx.	.
</s>
<s>
Poslednímu	poslední	k2eAgMnSc3d1	poslední
republikánskému	republikánský	k2eAgMnSc3d1	republikánský
kandidátovi	kandidát	k1gMnSc3	kandidát
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
podařilo	podařit	k5eAaPmAgNnS	podařit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
<g/>
.	.	kIx.	.
</s>
<s>
George	George	k1gFnSc7	George
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
byl	být	k5eAaImAgMnS	být
první	první	k4xOgMnSc1	první
kandidát	kandidát	k1gMnSc1	kandidát
od	od	k7c2	od
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
získal	získat	k5eAaPmAgInS	získat
nadpoloviční	nadpoloviční	k2eAgFnSc4d1	nadpoloviční
většinu	většina	k1gFnSc4	většina
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
sedmé	sedmý	k4xOgFnSc2	sedmý
volby	volba	k1gFnSc2	volba
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
se	se	k3xPyFc4	se
demokratickému	demokratický	k2eAgMnSc3d1	demokratický
kandidátovi	kandidát	k1gMnSc3	kandidát
podobný	podobný	k2eAgInSc4d1	podobný
úspěch	úspěch	k1gInSc4	úspěch
nepodařil	podařit	k5eNaPmAgMnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
volbám	volba	k1gFnPc3	volba
se	se	k3xPyFc4	se
dostavilo	dostavit	k5eAaPmAgNnS	dostavit
o	o	k7c4	o
12	[number]	k4	12
miliónů	milión	k4xCgInPc2	milión
lidí	člověk	k1gMnPc2	člověk
více	hodně	k6eAd2	hodně
než	než	k8xS	než
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Účast	účast	k1gFnSc1	účast
byla	být	k5eAaImAgFnS	být
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volebním	volební	k2eAgNnSc6d1	volební
období	období	k1gNnSc6	období
2005	[number]	k4	2005
<g/>
-	-	kIx~	-
<g/>
2009	[number]	k4	2009
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nastane	nastat	k5eAaPmIp3nS	nastat
výrazná	výrazný	k2eAgFnSc1d1	výrazná
změna	změna	k1gFnSc1	změna
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
USA	USA	kA	USA
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
by	by	kYmCp3nP	by
měli	mít	k5eAaImAgMnP	mít
kvůli	kvůli	k7c3	kvůli
věku	věk	k1gInSc3	věk
odejít	odejít	k5eAaPmF	odejít
někteří	některý	k3yIgMnPc1	některý
soudci	soudce	k1gMnPc1	soudce
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
prezidentské	prezidentský	k2eAgFnPc4d1	prezidentská
pravomoci	pravomoc	k1gFnPc4	pravomoc
spadá	spadat	k5eAaPmIp3nS	spadat
i	i	k9	i
jmenování	jmenování	k1gNnSc1	jmenování
soudců	soudce	k1gMnPc2	soudce
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
3	[number]	k4	3
státy	stát	k1gInPc7	stát
zvolily	zvolit	k5eAaPmAgInP	zvolit
kandidáta	kandidát	k1gMnSc4	kandidát
jiné	jiný	k2eAgFnSc2d1	jiná
strany	strana	k1gFnSc2	strana
než	než	k8xS	než
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Bush	Bush	k1gMnSc1	Bush
získal	získat	k5eAaPmAgMnS	získat
Iowu	Iowa	k1gFnSc4	Iowa
a	a	k8xC	a
Nové	Nové	k2eAgNnSc4d1	Nové
Mexiko	Mexiko	k1gNnSc4	Mexiko
(	(	kIx(	(
<g/>
celkově	celkově	k6eAd1	celkově
12	[number]	k4	12
hlasů	hlas	k1gInPc2	hlas
volitelů	volitel	k1gMnPc2	volitel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kerry	Kerr	k1gInPc1	Kerr
New	New	k1gMnSc5	New
Hampshire	Hampshir	k1gMnSc5	Hampshir
(	(	kIx(	(
<g/>
4	[number]	k4	4
hlasy	hlas	k1gInPc7	hlas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
sledovali	sledovat	k5eAaImAgMnP	sledovat
zahraniční	zahraniční	k2eAgMnPc1d1	zahraniční
pozorovatelé	pozorovatel	k1gMnPc1	pozorovatel
z	z	k7c2	z
OSCE	OSCE	kA	OSCE
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
přišli	přijít	k5eAaPmAgMnP	přijít
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
americké	americký	k2eAgFnSc2d1	americká
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Prognózy	prognóza	k1gFnPc1	prognóza
Kongresového	kongresový	k2eAgInSc2d1	kongresový
rozpočtového	rozpočtový	k2eAgInSc2d1	rozpočtový
úřadu	úřad	k1gInSc2	úřad
z	z	k7c2	z
ledna	leden	k1gInSc2	leden
2005	[number]	k4	2005
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
trend	trend	k1gInSc1	trend
narůstajícího	narůstající	k2eAgInSc2d1	narůstající
deficitu	deficit	k1gInSc2	deficit
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
Bushova	Bushův	k2eAgNnSc2d1	Bushovo
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
bude	být	k5eAaImBp3nS	být
vystřídaný	vystřídaný	k2eAgInSc1d1	vystřídaný
klesajícím	klesající	k2eAgInSc7d1	klesající
deficitem	deficit	k1gInSc7	deficit
v	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
předpovědi	předpověď	k1gFnSc2	předpověď
klesne	klesnout	k5eAaPmIp3nS	klesnout
deficit	deficit	k1gInSc1	deficit
na	na	k7c4	na
368	[number]	k4	368
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
261	[number]	k4	261
miliard	miliarda	k4xCgFnPc2	miliarda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
a	a	k8xC	a
207	[number]	k4	207
miliard	miliarda	k4xCgFnPc2	miliarda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
s	s	k7c7	s
malým	malý	k2eAgInSc7d1	malý
nárůstem	nárůst	k1gInSc7	nárůst
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
(	(	kIx(	(
<g/>
tyto	tento	k3xDgInPc1	tento
odhady	odhad	k1gInPc1	odhad
ovšem	ovšem	k9	ovšem
nezohlednily	zohlednit	k5eNaPmAgInP	zohlednit
následky	následek	k1gInPc1	následek
hurikánu	hurikán	k1gInSc2	hurikán
Katrina	Katrin	k2eAgInSc2d1	Katrin
<g/>
.	.	kIx.	.
</s>
<s>
Naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
též	též	k9	též
<g/>
,	,	kIx,	,
že	že	k8xS	že
výdaje	výdaj	k1gInPc1	výdaj
spojené	spojený	k2eAgInPc1d1	spojený
s	s	k7c7	s
vojenskými	vojenský	k2eAgFnPc7d1	vojenská
operacemi	operace	k1gFnPc7	operace
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
a	a	k8xC	a
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
a	a	k8xC	a
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
aktivitami	aktivita	k1gFnPc7	aktivita
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Války	válka	k1gFnSc2	válka
proti	proti	k7c3	proti
terorismu	terorismus	k1gInSc3	terorismus
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
navzdory	navzdory	k7c3	navzdory
velkým	velký	k2eAgFnPc3d1	velká
rezervám	rezerva	k1gFnPc3	rezerva
v	v	k7c6	v
prognózách	prognóza	k1gFnPc6	prognóza
podílet	podílet	k5eAaImF	podílet
na	na	k7c6	na
dalším	další	k2eAgNnSc6d1	další
zvýšení	zvýšení	k1gNnSc6	zvýšení
<g/>
.	.	kIx.	.
</s>
<s>
Prognóza	prognóza	k1gFnSc1	prognóza
brala	brát	k5eAaImAgFnS	brát
též	též	k9	též
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bushem	Bush	k1gMnSc7	Bush
zavedené	zavedený	k2eAgFnSc2d1	zavedená
snížení	snížení	k1gNnSc1	snížení
daní	daň	k1gFnSc7	daň
skončí	skončit	k5eAaPmIp3nS	skončit
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
nebylo	být	k5eNaImAgNnS	být
<g/>
,	,	kIx,	,
rozpočtový	rozpočtový	k2eAgInSc4d1	rozpočtový
výhled	výhled	k1gInSc4	výhled
na	na	k7c4	na
rok	rok	k1gInSc4	rok
2015	[number]	k4	2015
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgInS	změnit
ze	z	k7c2	z
zisku	zisk	k1gInSc2	zisk
141	[number]	k4	141
miliard	miliarda	k4xCgFnPc2	miliarda
na	na	k7c4	na
deficit	deficit	k1gInSc1	deficit
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
282	[number]	k4	282
miliard	miliarda	k4xCgFnPc2	miliarda
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2005	[number]	k4	2005
americká	americký	k2eAgFnSc1d1	americká
administrativa	administrativa	k1gFnSc1	administrativa
snížila	snížit	k5eAaPmAgFnS	snížit
odhad	odhad	k1gInSc4	odhad
rozpočtového	rozpočtový	k2eAgInSc2d1	rozpočtový
deficitu	deficit	k1gInSc2	deficit
za	za	k7c4	za
rozpočtový	rozpočtový	k2eAgInSc4d1	rozpočtový
rok	rok	k1gInSc4	rok
2005	[number]	k4	2005
téměř	téměř	k6eAd1	téměř
o	o	k7c4	o
100	[number]	k4	100
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
neočekávaně	očekávaně	k6eNd1	očekávaně
vysoké	vysoký	k2eAgInPc4d1	vysoký
daňové	daňový	k2eAgInPc4d1	daňový
výnosy	výnos	k1gInPc4	výnos
<g/>
.	.	kIx.	.
</s>
<s>
Růst	růst	k1gInSc1	růst
HDP	HDP	kA	HDP
si	se	k3xPyFc3	se
zachoval	zachovat	k5eAaPmAgMnS	zachovat
tempo	tempo	k1gNnSc4	tempo
z	z	k7c2	z
posledního	poslední	k2eAgNnSc2d1	poslední
čtvrtletí	čtvrtletí	k1gNnSc2	čtvrtletí
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvém	prvý	k4xOgInSc6	prvý
čtvrtletí	čtvrtletí	k1gNnSc6	čtvrtletí
2005	[number]	k4	2005
rostl	růst	k5eAaImAgInS	růst
o	o	k7c4	o
3,8	[number]	k4	3,8
<g/>
%	%	kIx~	%
(	(	kIx(	(
<g/>
přepočteno	přepočíst	k5eAaPmNgNnS	přepočíst
na	na	k7c4	na
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
v	v	k7c6	v
Eurozóně	Eurozón	k1gInSc6	Eurozón
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
stejném	stejný	k2eAgNnSc6d1	stejné
období	období	k1gNnSc6	období
pouze	pouze	k6eAd1	pouze
0,9	[number]	k4	0,9
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
se	s	k7c7	s
stejným	stejný	k2eAgNnSc7d1	stejné
obdobím	období	k1gNnSc7	období
minulého	minulý	k2eAgInSc2d1	minulý
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ekonomika	ekonomika	k1gFnSc1	ekonomika
rostla	růst	k5eAaImAgFnS	růst
tempem	tempo	k1gNnSc7	tempo
4,5	[number]	k4	4,5
<g/>
%	%	kIx~	%
lze	lze	k6eAd1	lze
vidět	vidět	k5eAaImF	vidět
malé	malý	k2eAgNnSc4d1	malé
zpomalení	zpomalení	k1gNnSc4	zpomalení
<g/>
.	.	kIx.	.
</s>
<s>
Zásadní	zásadní	k2eAgInSc4d1	zásadní
zvrat	zvrat	k1gInSc4	zvrat
přišel	přijít	k5eAaPmAgInS	přijít
s	s	k7c7	s
hypoteční	hypoteční	k2eAgFnSc7d1	hypoteční
krizí	krize	k1gFnSc7	krize
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
způsobila	způsobit	k5eAaPmAgFnS	způsobit
historický	historický	k2eAgInSc4d1	historický
propad	propad	k1gInSc4	propad
hodnoty	hodnota	k1gFnSc2	hodnota
dolaru	dolar	k1gInSc2	dolar
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
druhé	druhý	k4xOgFnSc6	druhý
inauguraci	inaugurace	k1gFnSc6	inaugurace
se	se	k3xPyFc4	se
Bush	Bush	k1gMnSc1	Bush
obrátil	obrátit	k5eAaPmAgMnS	obrátit
k	k	k7c3	k
národu	národ	k1gInSc3	národ
s	s	k7c7	s
návrhem	návrh	k1gInSc7	návrh
osobních	osobní	k2eAgInPc2d1	osobní
účtů	účet	k1gInPc2	účet
sociálního	sociální	k2eAgNnSc2d1	sociální
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
<g/>
.	.	kIx.	.
</s>
<s>
Bush	Bush	k1gMnSc1	Bush
vyzýval	vyzývat	k5eAaImAgMnS	vyzývat
k	k	k7c3	k
principiálním	principiální	k2eAgFnPc3d1	principiální
změnám	změna	k1gFnPc3	změna
v	v	k7c6	v
sociálním	sociální	k2eAgNnSc6d1	sociální
zabezpečení	zabezpečení	k1gNnSc6	zabezpečení
<g/>
,	,	kIx,	,
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
problém	problém	k1gInSc1	problém
stanovil	stanovit	k5eAaPmAgInS	stanovit
za	za	k7c4	za
prioritu	priorita	k1gFnSc4	priorita
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
druhém	druhý	k4xOgNnSc6	druhý
volebním	volební	k2eAgNnSc6d1	volební
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ledna	leden	k1gInSc2	leden
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
2005	[number]	k4	2005
cestoval	cestovat	k5eAaImAgMnS	cestovat
po	po	k7c6	po
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
zastavil	zastavit	k5eAaPmAgInS	zastavit
se	se	k3xPyFc4	se
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
podpořil	podpořit	k5eAaPmAgMnS	podpořit
svůj	svůj	k3xOyFgInSc4	svůj
názor	názor	k1gInSc4	názor
na	na	k7c4	na
důležitost	důležitost	k1gFnSc4	důležitost
sociálního	sociální	k2eAgNnSc2d1	sociální
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
jako	jako	k8xC	jako
instituce	instituce	k1gFnSc2	instituce
a	a	k8xC	a
potřebu	potřeba	k1gFnSc4	potřeba
ustanovit	ustanovit	k5eAaPmF	ustanovit
systém	systém	k1gInSc4	systém
sociálního	sociální	k2eAgNnSc2d1	sociální
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
solventní	solventní	k2eAgInSc1d1	solventní
pro	pro	k7c4	pro
další	další	k2eAgFnPc4d1	další
generace	generace	k1gFnPc4	generace
Američanů	Američan	k1gMnPc2	Američan
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
reformy	reforma	k1gFnSc2	reforma
sociálního	sociální	k2eAgInSc2d1	sociální
systému	systém	k1gInSc2	systém
[	[	kIx(	[
<g/>
12	[number]	k4	12
<g/>
]	]	kIx)	]
je	být	k5eAaImIp3nS	být
především	především	k9	především
vývoj	vývoj	k1gInSc4	vývoj
ve	v	k7c6	v
struktuře	struktura	k1gFnSc6	struktura
populace	populace	k1gFnSc2	populace
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
stárnutí	stárnutí	k1gNnSc4	stárnutí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
pracovalo	pracovat	k5eAaImAgNnS	pracovat
16	[number]	k4	16
lidí	člověk	k1gMnPc2	člověk
na	na	k7c4	na
jednoho	jeden	k4xCgMnSc4	jeden
příjemce	příjemce	k1gMnSc4	příjemce
podpory	podpora	k1gFnSc2	podpora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
tento	tento	k3xDgInSc1	tento
poměr	poměr	k1gInSc1	poměr
klesl	klesnout	k5eAaPmAgInS	klesnout
na	na	k7c4	na
3,3	[number]	k4	3,3
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
očekává	očekávat	k5eAaImIp3nS	očekávat
další	další	k2eAgInSc1d1	další
výrazný	výrazný	k2eAgInSc1d1	výrazný
pokles	pokles	k1gInSc1	pokles
této	tento	k3xDgFnSc2	tento
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
generace	generace	k1gFnSc1	generace
Baby	baba	k1gFnSc2	baba
boomers	boomers	k6eAd1	boomers
začne	začít	k5eAaPmIp3nS	začít
odcházet	odcházet	k5eAaImF	odcházet
do	do	k7c2	do
důchodu	důchod	k1gInSc2	důchod
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vládních	vládní	k2eAgInPc2d1	vládní
odhadů	odhad	k1gInPc2	odhad
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
reforma	reforma	k1gFnSc1	reforma
nevykonala	vykonat	k5eNaPmAgFnS	vykonat
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2040	[number]	k4	2040
by	by	k9	by
systém	systém	k1gInSc1	systém
sociálního	sociální	k2eAgNnSc2d1	sociální
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
zkrachoval	zkrachovat	k5eAaPmAgMnS	zkrachovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2005	[number]	k4	2005
vydal	vydat	k5eAaPmAgInS	vydat
Bílý	bílý	k2eAgInSc1d1	bílý
dům	dům	k1gInSc1	dům
novou	nový	k2eAgFnSc4d1	nová
Zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
vesmírné	vesmírný	k2eAgFnSc6d1	vesmírná
dopravní	dopravní	k2eAgFnSc6d1	dopravní
politice	politika	k1gFnSc6	politika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zdůrazňovala	zdůrazňovat	k5eAaImAgFnS	zdůrazňovat
význam	význam	k1gInSc4	význam
všeobecné	všeobecný	k2eAgFnSc2d1	všeobecná
vládní	vládní	k2eAgFnSc2d1	vládní
vesmírné	vesmírný	k2eAgFnSc2d1	vesmírná
politiky	politika	k1gFnSc2	politika
a	a	k8xC	a
propojila	propojit	k5eAaPmAgFnS	propojit
rozvoj	rozvoj	k1gInSc4	rozvoj
vesmírné	vesmírný	k2eAgFnSc2d1	vesmírná
dopravy	doprava	k1gFnSc2	doprava
s	s	k7c7	s
národními	národní	k2eAgInPc7d1	národní
bezpečnostními	bezpečnostní	k2eAgInPc7d1	bezpečnostní
požadavky	požadavek	k1gInPc7	požadavek
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2005	[number]	k4	2005
časopis	časopis	k1gInSc1	časopis
The	The	k1gFnSc2	The
New	New	k1gFnPc2	New
York	York	k1gInSc1	York
Times	Times	k1gMnSc1	Times
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
získal	získat	k5eAaPmAgInS	získat
interní	interní	k2eAgInPc4d1	interní
dokumenty	dokument	k1gInPc4	dokument
Bílého	bílý	k2eAgInSc2d1	bílý
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
dokazovaly	dokazovat	k5eAaImAgInP	dokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Philip	Philip	k1gMnSc1	Philip
A.	A.	kA	A.
Cooney	Coonea	k1gFnPc1	Coonea
<g/>
,	,	kIx,	,
úředník	úředník	k1gMnSc1	úředník
Bushovy	Bushův	k2eAgFnSc2d1	Bushova
vlády	vláda	k1gFnSc2	vláda
s	s	k7c7	s
kontakty	kontakt	k1gInPc7	kontakt
na	na	k7c4	na
energetický	energetický	k2eAgInSc4d1	energetický
průmysl	průmysl	k1gInSc4	průmysl
změnil	změnit	k5eAaPmAgInS	změnit
národní	národní	k2eAgFnPc4d1	národní
zprávy	zpráva	k1gFnPc4	zpráva
o	o	k7c6	o
klimatických	klimatický	k2eAgFnPc6d1	klimatická
změnách	změna	k1gFnPc6	změna
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2002	[number]	k4	2002
a	a	k8xC	a
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zlehčil	zlehčit	k5eAaPmAgMnS	zlehčit
výsledky	výsledek	k1gInPc4	výsledek
o	o	k7c6	o
emisích	emise	k1gFnPc6	emise
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
globálním	globální	k2eAgNnSc7d1	globální
oteplováním	oteplování	k1gNnSc7	oteplování
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
po	po	k7c6	po
uveřejnění	uveřejnění	k1gNnSc6	uveřejnění
článku	článek	k1gInSc2	článek
se	se	k3xPyFc4	se
Cooney	Coonea	k1gFnSc2	Coonea
vzdal	vzdát	k5eAaPmAgMnS	vzdát
svého	svůj	k3xOyFgInSc2	svůj
vedoucího	vedoucí	k2eAgInSc2d1	vedoucí
postu	post	k1gInSc2	post
v	v	k7c6	v
Radě	rada	k1gFnSc6	rada
pro	pro	k7c4	pro
environmentální	environmentální	k2eAgFnSc4d1	environmentální
kvalitu	kvalita	k1gFnSc4	kvalita
Bílého	bílý	k2eAgInSc2d1	bílý
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
a	a	k8xC	a
od	od	k7c2	od
podzimu	podzim	k1gInSc2	podzim
2005	[number]	k4	2005
má	mít	k5eAaImIp3nS	mít
začít	začít	k5eAaPmF	začít
pracovat	pracovat	k5eAaImF	pracovat
pro	pro	k7c4	pro
firmu	firma	k1gFnSc4	firma
ExxonMobil	ExxonMobil	k1gFnSc2	ExxonMobil
<g/>
.	.	kIx.	.
</s>
<s>
Cooney	Cooney	k1gInPc1	Cooney
<g/>
,	,	kIx,	,
důležitý	důležitý	k2eAgMnSc1d1	důležitý
pracovník	pracovník	k1gMnSc1	pracovník
Bushovy	Bushův	k2eAgFnSc2d1	Bushova
vlády	vláda	k1gFnSc2	vláda
byl	být	k5eAaImAgInS	být
právníkem	právník	k1gMnSc7	právník
a	a	k8xC	a
lobbistou	lobbista	k1gMnSc7	lobbista
pro	pro	k7c4	pro
Americký	americký	k2eAgInSc4d1	americký
ropný	ropný	k2eAgInSc4d1	ropný
institut	institut	k1gInSc4	institut
<g/>
,	,	kIx,	,
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
lobistickou	lobistický	k2eAgFnSc4d1	lobistická
organizaci	organizace	k1gFnSc4	organizace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
stavěla	stavět	k5eAaImAgFnS	stavět
proti	proti	k7c3	proti
omezením	omezení	k1gNnSc7	omezení
emisí	emise	k1gFnPc2	emise
vyhlašováním	vyhlašování	k1gNnSc7	vyhlašování
<g/>
,	,	kIx,	,
že	že	k8xS	že
existuje	existovat	k5eAaImIp3nS	existovat
příliš	příliš	k6eAd1	příliš
mnoho	mnoho	k4c1	mnoho
nejasností	nejasnost	k1gFnPc2	nejasnost
ve	v	k7c6	v
výzkumu	výzkum	k1gInSc6	výzkum
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2005	[number]	k4	2005
na	na	k7c4	na
31	[number]	k4	31
<g/>
.	.	kIx.	.
summitu	summit	k1gInSc2	summit
průmyslově	průmyslově	k6eAd1	průmyslově
nejvyspělejších	vyspělý	k2eAgFnPc2d3	nejvyspělejší
demokratických	demokratický	k2eAgFnPc2d1	demokratická
zemí	zem	k1gFnPc2	zem
G8	G8	k1gMnSc1	G8
Bush	Bush	k1gMnSc1	Bush
uznal	uznat	k5eAaPmAgMnS	uznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
povrch	povrch	k1gInSc1	povrch
Země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
otepluje	oteplovat	k5eAaImIp3nS	oteplovat
a	a	k8xC	a
že	že	k8xS	že
zvyšující	zvyšující	k2eAgNnSc1d1	zvyšující
se	se	k3xPyFc4	se
množství	množství	k1gNnSc1	množství
emisí	emise	k1gFnPc2	emise
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
problému	problém	k1gInSc3	problém
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
také	také	k9	také
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kjótský	Kjótský	k2eAgInSc1d1	Kjótský
protokol	protokol	k1gInSc1	protokol
není	být	k5eNaImIp3nS	být
řešením	řešení	k1gNnSc7	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Chceme	chtít	k5eAaImIp1nP	chtít
redukovat	redukovat	k5eAaBmF	redukovat
skleníkové	skleníkový	k2eAgInPc1d1	skleníkový
plyny	plyn	k1gInPc1	plyn
<g/>
...	...	k?	...
Ale	ale	k8xC	ale
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
mého	můj	k3xOp1gInSc2	můj
názoru	názor	k1gInSc2	názor
týká	týkat	k5eAaImIp3nS	týkat
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
věc	věc	k1gFnSc1	věc
za	za	k7c7	za
druhou	druhý	k4xOgFnSc7	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Naše	náš	k3xOp1gFnSc1	náš
strategie	strategie	k1gFnSc1	strategie
musí	muset	k5eAaImIp3nS	muset
zabezpečit	zabezpečit	k5eAaPmF	zabezpečit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
pracující	pracující	k2eAgMnPc1d1	pracující
lidé	člověk	k1gMnPc1	člověk
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
nepřišli	přijít	k5eNaPmAgMnP	přijít
o	o	k7c4	o
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
"	"	kIx"	"
Snahou	snaha	k1gFnSc7	snaha
hostitelského	hostitelský	k2eAgMnSc2d1	hostitelský
britského	britský	k2eAgMnSc2d1	britský
premiéra	premiér	k1gMnSc2	premiér
Tonyho	Tony	k1gMnSc2	Tony
Blaira	Blair	k1gMnSc2	Blair
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
environmentálním	environmentální	k2eAgMnSc7d1	environmentální
poradcem	poradce	k1gMnSc7	poradce
je	být	k5eAaImIp3nS	být
významný	významný	k2eAgMnSc1d1	významný
klimatolog	klimatolog	k1gMnSc1	klimatolog
David	David	k1gMnSc1	David
King	King	k1gMnSc1	King
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
globálního	globální	k2eAgNnSc2d1	globální
oteplování	oteplování	k1gNnSc2	oteplování
na	na	k7c6	na
summitu	summit	k1gInSc6	summit
dospět	dospět	k5eAaPmF	dospět
k	k	k7c3	k
podpisu	podpis	k1gInSc3	podpis
konkrékní	konkrékní	k2eAgFnSc2d1	konkrékní
dohody	dohoda	k1gFnSc2	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
odmítavému	odmítavý	k2eAgInSc3d1	odmítavý
postoji	postoj	k1gInSc3	postoj
USA	USA	kA	USA
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
ostatní	ostatní	k2eAgInPc1d1	ostatní
státy	stát	k1gInPc1	stát
G8	G8	k1gMnPc2	G8
se	se	k3xPyFc4	se
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Kjótského	Kjótský	k2eAgInSc2d1	Kjótský
protokolu	protokol	k1gInSc2	protokol
zavázaly	zavázat	k5eAaPmAgFnP	zavázat
snížit	snížit	k5eAaPmF	snížit
emise	emise	k1gFnPc1	emise
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
však	však	k9	však
souhlasily	souhlasit	k5eAaImAgFnP	souhlasit
s	s	k7c7	s
připojením	připojení	k1gNnSc7	připojení
se	se	k3xPyFc4	se
k	k	k7c3	k
závěrečnému	závěrečný	k2eAgNnSc3d1	závěrečné
komuniké	komuniké	k1gNnSc3	komuniké
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
uvádělo	uvádět	k5eAaImAgNnS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
globální	globální	k2eAgNnSc1d1	globální
oteplování	oteplování	k1gNnSc1	oteplování
existuje	existovat	k5eAaImIp3nS	existovat
a	a	k8xC	a
že	že	k8xS	že
lidské	lidský	k2eAgFnPc1d1	lidská
aktivity	aktivita	k1gFnPc1	aktivita
mohou	moct	k5eAaImIp3nP	moct
přinejmenším	přinejmenším	k6eAd1	přinejmenším
částečně	částečně	k6eAd1	částečně
být	být	k5eAaImF	být
jeho	jeho	k3xOp3gFnSc7	jeho
příčinou	příčina	k1gFnSc7	příčina
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
také	také	k9	také
omezily	omezit	k5eAaPmAgFnP	omezit
finanční	finanční	k2eAgFnPc1d1	finanční
zálohy	záloha	k1gFnPc1	záloha
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
sítí	síť	k1gFnPc2	síť
regionálních	regionální	k2eAgNnPc2d1	regionální
klimatických	klimatický	k2eAgNnPc2d1	klimatické
center	centrum	k1gNnPc2	centrum
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
účelem	účel	k1gInSc7	účel
mělo	mít	k5eAaImAgNnS	mít
byt	byt	k1gInSc4	byt
monitorování	monitorování	k1gNnSc2	monitorování
rozvíjejících	rozvíjející	k2eAgInPc2d1	rozvíjející
se	se	k3xPyFc4	se
následků	následek	k1gInPc2	následek
globálního	globální	k2eAgNnSc2d1	globální
oteplování	oteplování	k1gNnSc2	oteplování
<g/>
.	.	kIx.	.
</s>
<s>
Bush	Bush	k1gMnSc1	Bush
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
Írán	Írán	k1gInSc4	Írán
a	a	k8xC	a
označil	označit	k5eAaPmAgMnS	označit
ho	on	k3xPp3gMnSc4	on
za	za	k7c4	za
"	"	kIx"	"
<g/>
hlavního	hlavní	k2eAgMnSc4d1	hlavní
státního	státní	k2eAgMnSc4d1	státní
sponzora	sponzor	k1gMnSc4	sponzor
teroru	teror	k1gInSc2	teror
<g/>
"	"	kIx"	"
a	a	k8xC	a
přislíbil	přislíbit	k5eAaPmAgMnS	přislíbit
íránským	íránský	k2eAgMnSc7d1	íránský
reformátorům	reformátor	k1gMnPc3	reformátor
podporu	podpora	k1gFnSc4	podpora
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
během	během	k7c2	během
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
Mahmúd	Mahmúda	k1gFnPc2	Mahmúda
Ahmadínedžád	Ahmadínedžáda	k1gFnPc2	Ahmadínedžáda
a	a	k8xC	a
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
nástupu	nástup	k1gInSc6	nástup
se	se	k3xPyFc4	se
vyostřil	vyostřit	k5eAaPmAgInS	vyostřit
spor	spor	k1gInSc1	spor
ohledně	ohledně	k7c2	ohledně
íránského	íránský	k2eAgInSc2d1	íránský
atomového	atomový	k2eAgInSc2d1	atomový
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Varoval	varovat	k5eAaImAgMnS	varovat
Sýrii	Sýrie	k1gFnSc4	Sýrie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
přestala	přestat	k5eAaPmAgFnS	přestat
podporovat	podporovat	k5eAaImF	podporovat
teroristy	terorista	k1gMnPc7	terorista
<g/>
.	.	kIx.	.
</s>
<s>
Vyzval	vyzvat	k5eAaPmAgInS	vyzvat
Egypt	Egypt	k1gInSc1	Egypt
a	a	k8xC	a
Saúdskou	saúdský	k2eAgFnSc4d1	Saúdská
Arábii	Arábie	k1gFnSc4	Arábie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
pracovaly	pracovat	k5eAaImAgFnP	pracovat
na	na	k7c6	na
silnější	silný	k2eAgFnSc6d2	silnější
demokratizaci	demokratizace	k1gFnSc6	demokratizace
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
nominaci	nominace	k1gFnSc4	nominace
Johna	John	k1gMnSc2	John
Boltona	Bolton	k1gMnSc2	Bolton
na	na	k7c4	na
velvyslance	velvyslanec	k1gMnSc4	velvyslanec
USA	USA	kA	USA
v	v	k7c6	v
OSN	OSN	kA	OSN
úspěšně	úspěšně	k6eAd1	úspěšně
zablokovali	zablokovat	k5eAaPmAgMnP	zablokovat
demokraté	demokrat	k1gMnPc1	demokrat
v	v	k7c6	v
Kongresu	kongres	k1gInSc6	kongres
na	na	k7c4	na
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2005	[number]	k4	2005
se	se	k3xPyFc4	se
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
uskutečnily	uskutečnit	k5eAaPmAgFnP	uskutečnit
svobodné	svobodný	k2eAgFnPc1d1	svobodná
volby	volba	k1gFnPc1	volba
do	do	k7c2	do
dočasného	dočasný	k2eAgInSc2d1	dočasný
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
vytvořit	vytvořit	k5eAaPmF	vytvořit
novou	nový	k2eAgFnSc4d1	nová
regulérní	regulérní	k2eAgFnSc4d1	regulérní
ústavu	ústava	k1gFnSc4	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
volby	volba	k1gFnSc2	volba
poznamenalo	poznamenat	k5eAaPmAgNnS	poznamenat
drobné	drobná	k1gFnPc4	drobná
násilí	násilí	k1gNnSc2	násilí
a	a	k8xC	a
slabá	slabý	k2eAgFnSc1d1	slabá
účast	účast	k1gFnSc1	účast
Sunnitů	sunnita	k1gMnPc2	sunnita
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
Kurdů	Kurd	k1gMnPc2	Kurd
a	a	k8xC	a
Šíitů	šíita	k1gMnPc2	šíita
projevila	projevit	k5eAaPmAgFnS	projevit
silný	silný	k2eAgInSc4d1	silný
zájem	zájem	k1gInSc4	zájem
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
Paul	Paul	k1gMnSc1	Paul
Wolfowitz	Wolfowitz	k1gMnSc1	Wolfowitz
ohlásil	ohlásit	k5eAaPmAgMnS	ohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
15	[number]	k4	15
000	[number]	k4	000
amerických	americký	k2eAgMnPc2d1	americký
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
služba	služba	k1gFnSc1	služba
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
byla	být	k5eAaImAgFnS	být
prodloužena	prodloužen	k2eAgFnSc1d1	prodloužena
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
pořádku	pořádek	k1gInSc2	pořádek
během	během	k7c2	během
voleb	volba	k1gFnPc2	volba
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
stáhnuto	stáhnut	k2eAgNnSc1d1	stáhnuto
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
13	[number]	k4	13
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
začátek	začátek	k1gInSc4	začátek
postupného	postupný	k2eAgNnSc2d1	postupné
snižování	snižování	k1gNnSc2	snižování
amerických	americký	k2eAgInPc2d1	americký
stavů	stav	k1gInPc2	stav
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
stále	stále	k6eAd1	stále
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
velmi	velmi	k6eAd1	velmi
pomalu	pomalu	k6eAd1	pomalu
<g/>
.	.	kIx.	.
</s>
<s>
Únor	únor	k1gInSc1	únor
<g/>
,	,	kIx,	,
březen	březen	k1gInSc1	březen
<g/>
,	,	kIx,	,
a	a	k8xC	a
duben	duben	k1gInSc1	duben
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
relativně	relativně	k6eAd1	relativně
pokojné	pokojný	k2eAgInPc4d1	pokojný
měsíce	měsíc	k1gInPc4	měsíc
v	v	k7c6	v
porovnaní	porovnaný	k2eAgMnPc1d1	porovnaný
s	s	k7c7	s
událostmi	událost	k1gFnPc7	událost
z	z	k7c2	z
listopadu	listopad	k1gInSc2	listopad
2004	[number]	k4	2004
a	a	k8xC	a
ledna	leden	k1gInSc2	leden
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
počet	počet	k1gInSc1	počet
útoků	útok	k1gInPc2	útok
klesl	klesnout	k5eAaPmAgInS	klesnout
na	na	k7c4	na
30	[number]	k4	30
denně	denně	k6eAd1	denně
z	z	k7c2	z
předešlého	předešlý	k2eAgInSc2d1	předešlý
průměru	průměr	k1gInSc2	průměr
70	[number]	k4	70
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2005	[number]	k4	2005
však	však	k9	však
americká	americký	k2eAgFnSc1d1	americká
armáda	armáda	k1gFnSc1	armáda
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
Iráku	Irák	k1gInSc2	Irák
pošle	poslat	k5eAaPmIp3nS	poslat
dalších	další	k2eAgInPc2d1	další
1	[number]	k4	1
500	[number]	k4	500
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
pomohli	pomoct	k5eAaPmAgMnP	pomoct
zabezpečit	zabezpečit	k5eAaPmF	zabezpečit
volby	volba	k1gFnPc4	volba
a	a	k8xC	a
referendum	referendum	k1gNnSc4	referendum
týkajíce	týkat	k5eAaImSgFnP	týkat
se	se	k3xPyFc4	se
nové	nový	k2eAgInPc1d1	nový
ústavy	ústav	k1gInPc1	ústav
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
měl	mít	k5eAaImAgMnS	mít
Irák	Irák	k1gInSc4	Irák
podle	podle	k7c2	podle
původního	původní	k2eAgInSc2d1	původní
plánu	plán	k1gInSc2	plán
přijmout	přijmout	k5eAaPmF	přijmout
novou	nový	k2eAgFnSc4d1	nová
ústavu	ústava	k1gFnSc4	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
se	se	k3xPyFc4	se
však	však	k9	však
nepodařilo	podařit	k5eNaPmAgNnS	podařit
dodržet	dodržet	k5eAaPmF	dodržet
a	a	k8xC	a
po	po	k7c6	po
dohodě	dohoda	k1gFnSc6	dohoda
byl	být	k5eAaImAgInS	být
několikrát	několikrát	k6eAd1	několikrát
odložen	odložit	k5eAaPmNgInS	odložit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
případě	případ	k1gInSc6	případ
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
celý	celý	k2eAgInSc1d1	celý
proces	proces	k1gInSc1	proces
včetně	včetně	k7c2	včetně
voleb	volba	k1gFnPc2	volba
musel	muset	k5eAaImAgMnS	muset
zopakovat	zopakovat	k5eAaPmF	zopakovat
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
odložením	odložení	k1gNnSc7	odložení
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
i	i	k8xC	i
Bush	Bush	k1gMnSc1	Bush
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2005	[number]	k4	2005
Bush	Bush	k1gMnSc1	Bush
zahájil	zahájit	k5eAaPmAgMnS	zahájit
své	svůj	k3xOyFgNnSc4	svůj
"	"	kIx"	"
<g/>
turné	turné	k1gNnSc4	turné
<g/>
"	"	kIx"	"
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
napravit	napravit	k5eAaPmF	napravit
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
některými	některý	k3yIgInPc7	některý
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
zhoršily	zhoršit	k5eAaPmAgInP	zhoršit
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
5	[number]	k4	5
dní	den	k1gInPc2	den
trvající	trvající	k2eAgFnSc1d1	trvající
cesta	cesta	k1gFnSc1	cesta
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
a	a	k8xC	a
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Setkal	setkat	k5eAaPmAgInS	setkat
se	se	k3xPyFc4	se
takřka	takřka	k6eAd1	takřka
se	s	k7c7	s
všemi	všecek	k3xTgMnPc7	všecek
důležitými	důležitý	k2eAgMnPc7d1	důležitý
politiky	politik	k1gMnPc7	politik
<g/>
.	.	kIx.	.
</s>
<s>
Patřili	patřit	k5eAaImAgMnP	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnSc4	on
Jacques	Jacques	k1gMnSc1	Jacques
Chirac	Chirac	k1gMnSc1	Chirac
<g/>
,	,	kIx,	,
Gerhard	Gerhard	k1gMnSc1	Gerhard
Schröder	Schröder	k1gMnSc1	Schröder
<g/>
,	,	kIx,	,
Viktor	Viktor	k1gMnSc1	Viktor
Juščenko	Juščenka	k1gFnSc5	Juščenka
<g/>
,	,	kIx,	,
Tony	Tony	k1gMnSc1	Tony
Blair	Blair	k1gMnSc1	Blair
<g/>
,	,	kIx,	,
Silvio	Silvio	k6eAd1	Silvio
Berlusconi	Berluscon	k1gMnPc1	Berluscon
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
střetl	střetnout	k5eAaPmAgInS	střetnout
s	s	k7c7	s
členy	člen	k1gMnPc7	člen
Evropské	evropský	k2eAgFnSc2d1	Evropská
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
vedením	vedení	k1gNnSc7	vedení
NATO	nato	k6eAd1	nato
a	a	k8xC	a
s	s	k7c7	s
Vladimírem	Vladimír	k1gMnSc7	Vladimír
Putinem	Putin	k1gMnSc7	Putin
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Bruselu	Brusel	k1gInSc6	Brusel
se	se	k3xPyFc4	se
prezident	prezident	k1gMnSc1	prezident
Bush	Bush	k1gMnSc1	Bush
zastavil	zastavit	k5eAaPmAgMnS	zastavit
v	v	k7c6	v
Mohuči	Mohuč	k1gFnSc6	Mohuč
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
navštívil	navštívit	k5eAaPmAgMnS	navštívit
i	i	k9	i
americkou	americký	k2eAgFnSc4d1	americká
základnu	základna	k1gFnSc4	základna
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
cestoval	cestovat	k5eAaImAgMnS	cestovat
na	na	k7c4	na
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
ruským	ruský	k2eAgMnSc7d1	ruský
prezidentem	prezident	k1gMnSc7	prezident
a	a	k8xC	a
představiteli	představitel	k1gMnPc7	představitel
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Bushova	Bushův	k2eAgFnSc1d1	Bushova
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
v	v	k7c6	v
prvním	první	k4xOgMnSc6	první
i	i	k8xC	i
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
volebním	volební	k2eAgNnSc6d1	volební
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejotevřenější	otevřený	k2eAgFnSc7d3	nejotevřenější
vládou	vláda	k1gFnSc7	vláda
vůči	vůči	k7c3	vůči
národnostním	národnostní	k2eAgFnPc3d1	národnostní
menšinám	menšina	k1gFnPc3	menšina
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Národnostní	národnostní	k2eAgFnSc1d1	národnostní
různorodost	různorodost	k1gFnSc1	různorodost
vládního	vládní	k2eAgInSc2d1	vládní
kabinetu	kabinet	k1gInSc2	kabinet
ho	on	k3xPp3gMnSc4	on
zařadila	zařadit	k5eAaPmAgFnS	zařadit
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
Guinessovy	Guinessův	k2eAgFnSc2d1	Guinessova
knihy	kniha	k1gFnSc2	kniha
rekordů	rekord	k1gInPc2	rekord
<g/>
.	.	kIx.	.
</s>
<s>
Colin	Colin	k1gMnSc1	Colin
Powell	Powell	k1gMnSc1	Powell
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
americkým	americký	k2eAgMnSc7d1	americký
černochem	černoch	k1gMnSc7	černoch
zastávajícím	zastávající	k2eAgFnPc3d1	zastávající
post	post	k1gInSc4	post
ministra	ministr	k1gMnSc2	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
(	(	kIx(	(
<g/>
ve	v	k7c6	v
vládním	vládní	k2eAgNnSc6d1	vládní
období	období	k1gNnSc6	období
2001	[number]	k4	2001
<g/>
-	-	kIx~	-
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Condoleezza	Condoleezza	k1gFnSc1	Condoleezza
Riceová	Riceová	k1gFnSc1	Riceová
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
první	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
černošského	černošský	k2eAgInSc2d1	černošský
původu	původ	k1gInSc2	původ
zastávající	zastávající	k2eAgNnSc1d1	zastávající
tento	tento	k3xDgInSc4	tento
post	post	k1gInSc4	post
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
a	a	k8xC	a
též	též	k6eAd1	též
první	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
černošského	černošský	k2eAgInSc2d1	černošský
původu	původ	k1gInSc2	původ
zastávající	zastávající	k2eAgInSc4d1	zastávající
post	post	k1gInSc4	post
Poradce	poradce	k1gMnSc1	poradce
<g />
.	.	kIx.	.
</s>
<s>
národní	národní	k2eAgFnSc2d1	národní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
(	(	kIx(	(
<g/>
vládní	vládní	k2eAgNnSc1d1	vládní
období	období	k1gNnSc1	období
2001	[number]	k4	2001
<g/>
-	-	kIx~	-
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Elaine	elain	k1gInSc5	elain
Chaová	Chaová	k1gFnSc1	Chaová
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
asijského	asijský	k2eAgInSc2d1	asijský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
členem	člen	k1gMnSc7	člen
americké	americký	k2eAgFnSc2d1	americká
vlády	vláda	k1gFnSc2	vláda
Mel	mlít	k5eAaImRp2nS	mlít
Martinez	Martinez	k1gMnSc1	Martinez
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
prvním	první	k4xOgMnSc7	první
politikem	politik	k1gMnSc7	politik
hispánského	hispánský	k2eAgInSc2d1	hispánský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
americké	americký	k2eAgFnSc2d1	americká
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
Alberto	Alberta	k1gFnSc5	Alberta
Gonzales	Gonzalesa	k1gFnPc2	Gonzalesa
se	se	k3xPyFc4	se
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
politikem	politik	k1gMnSc7	politik
hispánského	hispánský	k2eAgInSc2d1	hispánský
původu	původ	k1gInSc2	původ
na	na	k7c6	na
tak	tak	k6eAd1	tak
důležitém	důležitý	k2eAgInSc6d1	důležitý
postě	post	k1gInSc6	post
<g/>
,	,	kIx,	,
jakým	jaký	k3yQgInSc7	jaký
je	být	k5eAaImIp3nS	být
post	post	k1gInSc1	post
ministra	ministr	k1gMnSc4	ministr
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
Z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
osobností	osobnost	k1gFnPc2	osobnost
je	být	k5eAaImIp3nS	být
potřebné	potřebný	k2eAgNnSc1d1	potřebné
vzpomenout	vzpomenout	k5eAaPmF	vzpomenout
přítomnost	přítomnost	k1gFnSc4	přítomnost
Normana	Norman	k1gMnSc2	Norman
Mineta	Minet	k1gMnSc2	Minet
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
<g/>
,	,	kIx,	,
demokrata	demokrat	k1gMnSc2	demokrat
asijského	asijský	k2eAgInSc2d1	asijský
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
působil	působit	k5eAaImAgInS	působit
i	i	k9	i
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
Billa	Bill	k1gMnSc2	Bill
Clintona	Clinton	k1gMnSc2	Clinton
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
i	i	k9	i
Roda	Rodus	k1gMnSc4	Rodus
Paigea	Paigeus	k1gMnSc4	Paigeus
<g/>
,	,	kIx,	,
černocha	černoch	k1gMnSc4	černoch
<g/>
,	,	kIx,	,
ministra	ministr	k1gMnSc2	ministr
školství	školství	k1gNnSc2	školství
Podle	podle	k7c2	podle
Guinessovy	Guinessův	k2eAgFnSc2d1	Guinessova
knihy	kniha	k1gFnSc2	kniha
rekordů	rekord	k1gInPc2	rekord
je	být	k5eAaImIp3nS	být
Bushova	Bushův	k2eAgFnSc1d1	Bushova
vláda	vláda	k1gFnSc1	vláda
i	i	k9	i
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
nejbohatší	bohatý	k2eAgFnSc2d3	nejbohatší
vládou	vláda	k1gFnSc7	vláda
(	(	kIx(	(
<g/>
v	v	k7c6	v
nominálních	nominální	k2eAgNnPc6d1	nominální
číslech	číslo	k1gNnPc6	číslo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
je	být	k5eAaImIp3nS	být
i	i	k9	i
jeden	jeden	k4xCgMnSc1	jeden
ne-republikán	neepublikán	k1gMnSc1	ne-republikán
<g/>
:	:	kIx,	:
ministr	ministr	k1gMnSc1	ministr
dopravy	doprava	k1gFnSc2	doprava
Norman	Norman	k1gMnSc1	Norman
Mineta	Mineta	k1gFnSc1	Mineta
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
ministr	ministr	k1gMnSc1	ministr
asijského	asijský	k2eAgInSc2d1	asijský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
působil	působit	k5eAaImAgMnS	působit
na	na	k7c6	na
ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
hospodářství	hospodářství	k1gNnSc2	hospodářství
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
demokrata	demokrat	k1gMnSc2	demokrat
Billa	Bill	k1gMnSc2	Bill
Clintona	Clinton	k1gMnSc2	Clinton
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bushove	Bushov	k1gInSc5	Bushov
vládě	vláda	k1gFnSc3	vláda
působilo	působit	k5eAaImAgNnS	působit
i	i	k9	i
několik	několik	k4yIc1	několik
prominentů	prominent	k1gMnPc2	prominent
z	z	k7c2	z
předchozích	předchozí	k2eAgFnPc2d1	předchozí
vlád	vláda	k1gFnPc2	vláda
<g/>
:	:	kIx,	:
Colin	Colin	k1gMnSc1	Colin
Powell	Powell	k1gMnSc1	Powell
předtím	předtím	k6eAd1	předtím
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
poradce	poradce	k1gMnSc1	poradce
pro	pro	k7c4	pro
národní	národní	k2eAgFnSc4d1	národní
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
USA	USA	kA	USA
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
Ronalda	Ronald	k1gMnSc2	Ronald
Reagana	Reagan	k1gMnSc2	Reagan
a	a	k8xC	a
náčelník	náčelník	k1gMnSc1	náčelník
generálního	generální	k2eAgInSc2d1	generální
štábu	štáb	k1gInSc2	štáb
(	(	kIx(	(
<g/>
Chairman	Chairman	k1gMnSc1	Chairman
of	of	k?	of
the	the	k?	the
Joint	Joint	k1gInSc1	Joint
Chiefs	Chiefs	k1gInSc1	Chiefs
of	of	k?	of
Staff	Staff	k1gInSc1	Staff
<g/>
)	)	kIx)	)
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
George	Georg	k1gMnSc2	Georg
H.	H.	kA	H.
W.	W.	kA	W.
Bushe	Bush	k1gMnSc4	Bush
a	a	k8xC	a
Clintona	Clinton	k1gMnSc4	Clinton
<g/>
.	.	kIx.	.
</s>
<s>
Ministr	ministr	k1gMnSc1	ministr
obrany	obrana	k1gFnSc2	obrana
Donald	Donald	k1gMnSc1	Donald
Rumsfeld	Rumsfeld	k1gMnSc1	Rumsfeld
působil	působit	k5eAaImAgMnS	působit
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
funkci	funkce	k1gFnSc6	funkce
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
Geralda	Gerald	k1gMnSc2	Gerald
Forda	ford	k1gMnSc2	ford
<g/>
.	.	kIx.	.
</s>
<s>
Viceprezident	viceprezident	k1gMnSc1	viceprezident
Richard	Richard	k1gMnSc1	Richard
Cheney	Cheney	k1gInPc4	Cheney
pracoval	pracovat	k5eAaImAgMnS	pracovat
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
George	Georg	k1gMnSc2	Georg
H.	H.	kA	H.
W.	W.	kA	W.
Bushe	Bush	k1gMnSc4	Bush
jako	jako	k8xC	jako
ministr	ministr	k1gMnSc1	ministr
obrany	obrana	k1gFnSc2	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
funkce	funkce	k1gFnSc2	funkce
Bush	Bush	k1gMnSc1	Bush
veřejně	veřejně	k6eAd1	veřejně
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
před	před	k7c7	před
různými	různý	k2eAgFnPc7d1	různá
skupinami	skupina	k1gFnPc7	skupina
<g/>
,	,	kIx,	,
před	před	k7c7	před
nimiž	jenž	k3xRgInPc7	jenž
přednáší	přednášet	k5eAaImIp3nS	přednášet
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
typické	typický	k2eAgFnSc2d1	typická
hodinové	hodinový	k2eAgFnSc2d1	hodinová
přednášky	přednáška	k1gFnSc2	přednáška
začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c4	na
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
dolarech	dolar	k1gInPc6	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Přednášení	přednášení	k1gNnSc1	přednášení
mu	on	k3xPp3gMnSc3	on
vydělalo	vydělat	k5eAaPmAgNnS	vydělat
15	[number]	k4	15
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Popularita	popularita	k1gFnSc1	popularita
prezidenta	prezident	k1gMnSc2	prezident
Bushe	Bush	k1gMnSc2	Bush
se	se	k3xPyFc4	se
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
jeho	jeho	k3xOp3gFnSc2	jeho
funkce	funkce	k1gFnSc2	funkce
dostala	dostat	k5eAaPmAgFnS	dostat
jak	jak	k6eAd1	jak
na	na	k7c4	na
historická	historický	k2eAgNnPc4d1	historické
maxima	maximum	k1gNnPc4	maximum
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
i	i	k9	i
minima	minimum	k1gNnSc2	minimum
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
až	až	k9	až
do	do	k7c2	do
září	září	k1gNnSc2	září
pozvolna	pozvolna	k6eAd1	pozvolna
upadala	upadat	k5eAaImAgFnS	upadat
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
z	z	k7c2	z
50	[number]	k4	50
na	na	k7c4	na
44	[number]	k4	44
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
útocích	útok	k1gInPc6	útok
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
ale	ale	k8xC	ale
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
semknutí	semknutí	k1gNnSc3	semknutí
amerických	americký	k2eAgMnPc2d1	americký
občanů	občan	k1gMnPc2	občan
a	a	k8xC	a
podpoře	podpora	k1gFnSc3	podpora
jejich	jejich	k3xOp3gFnSc2	jejich
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
preference	preference	k1gFnSc1	preference
na	na	k7c4	na
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
zdvojnásobily	zdvojnásobit	k5eAaPmAgFnP	zdvojnásobit
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
byl	být	k5eAaImAgMnS	být
G.	G.	kA	G.
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
převážně	převážně	k6eAd1	převážně
předmětem	předmět	k1gInSc7	předmět
chvály	chvála	k1gFnSc2	chvála
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
image	image	k1gInSc4	image
rozhodného	rozhodný	k2eAgMnSc2d1	rozhodný
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
odhodlaného	odhodlaný	k2eAgMnSc2d1	odhodlaný
vyvést	vyvést	k5eAaPmF	vyvést
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
za	za	k7c7	za
útoky	útok	k1gInPc7	útok
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
zemi	zem	k1gFnSc6	zem
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
postupem	postup	k1gInSc7	postup
dalších	další	k2eAgInPc2d1	další
měsíců	měsíc	k1gInPc2	měsíc
ale	ale	k8xC	ale
začal	začít	k5eAaPmAgMnS	začít
být	být	k5eAaImF	být
spíše	spíše	k9	spíše
kritizován	kritizován	k2eAgMnSc1d1	kritizován
(	(	kIx(	(
<g/>
nejvíce	nejvíce	k6eAd1	nejvíce
asi	asi	k9	asi
za	za	k7c4	za
Válku	válka	k1gFnSc4	válka
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
resp.	resp.	kA	resp.
deziluzi	deziluze	k1gFnSc4	deziluze
výhledu	výhled	k1gInSc2	výhled
na	na	k7c4	na
její	její	k3xOp3gNnSc4	její
zdárné	zdárný	k2eAgNnSc4d1	zdárné
dokončení	dokončení	k1gNnSc4	dokončení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sympatizanté	Sympatizanta	k1gMnPc1	Sympatizanta
se	se	k3xPyFc4	se
zaměřili	zaměřit	k5eAaPmAgMnP	zaměřit
na	na	k7c4	na
domácí	domácí	k2eAgInPc4d1	domácí
úspěchy	úspěch	k1gInPc4	úspěch
v	v	k7c6	v
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
,	,	kIx,	,
pomoc	pomoc	k1gFnSc4	pomoc
Africe	Afrika	k1gFnSc3	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Odpůrci	odpůrce	k1gMnPc1	odpůrce
zejména	zejména	k9	zejména
kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
nejen	nejen	k6eAd1	nejen
některé	některý	k3yIgFnPc4	některý
části	část	k1gFnPc4	část
protiústavního	protiústavní	k2eAgInSc2d1	protiústavní
legislativního	legislativní	k2eAgInSc2d1	legislativní
výnosu	výnos	k1gInSc2	výnos
USA	USA	kA	USA
PATRIOT	patriot	k1gMnSc1	patriot
Act	Act	k1gMnSc1	Act
<g/>
,	,	kIx,	,
zaměřili	zaměřit	k5eAaPmAgMnP	zaměřit
se	se	k3xPyFc4	se
i	i	k9	i
na	na	k7c4	na
kontroverzní	kontroverzní	k2eAgFnPc4d1	kontroverzní
volby	volba	k1gFnPc4	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
a	a	k8xC	a
válku	válka	k1gFnSc4	válka
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
,	,	kIx,	,
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
byl	být	k5eAaImAgMnS	být
rovněž	rovněž	k9	rovněž
za	za	k7c4	za
pomalé	pomalý	k2eAgNnSc4d1	pomalé
a	a	k8xC	a
neefektivní	efektivní	k2eNgNnSc4d1	neefektivní
řešení	řešení	k1gNnSc4	řešení
následků	následek	k1gInPc2	následek
hurikánu	hurikán	k1gInSc2	hurikán
Katrina	Katrin	k2eAgInSc2d1	Katrin
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
za	za	k7c4	za
nelegální	legální	k2eNgFnSc4d1	nelegální
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
bez	bez	k7c2	bez
povolení	povolení	k1gNnSc2	povolení
soudu	soud	k1gInSc2	soud
<g/>
)	)	kIx)	)
odposlouchávání	odposlouchávání	k1gNnSc2	odposlouchávání
telefonních	telefonní	k2eAgInPc2d1	telefonní
hovorů	hovor	k1gInPc2	hovor
amerických	americký	k2eAgMnPc2d1	americký
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Vysloužil	Vysloužil	k1gMnSc1	Vysloužil
si	se	k3xPyFc3	se
také	také	k9	také
přezdívku	přezdívka	k1gFnSc4	přezdívka
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Leaker	Leaker	k1gMnSc1	Leaker
in	in	k?	in
Chief	Chief	k1gMnSc1	Chief
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Vrchní	vrchní	k1gMnSc1	vrchní
vynašeč	vynašeč	k1gMnSc1	vynašeč
<g/>
)	)	kIx)	)
za	za	k7c4	za
vyzrazování	vyzrazování	k1gNnSc4	vyzrazování
tajných	tajný	k2eAgFnPc2d1	tajná
informací	informace	k1gFnPc2	informace
mediím	medium	k1gNnPc3	medium
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
osobního	osobní	k2eAgInSc2d1	osobní
politického	politický	k2eAgInSc2d1	politický
zisku	zisk	k1gInSc2	zisk
<g/>
.	.	kIx.	.
</s>
<s>
Magazín	magazín	k1gInSc1	magazín
TIME	TIME	kA	TIME
označil	označit	k5eAaPmAgInS	označit
Bushe	Bush	k1gMnSc4	Bush
za	za	k7c4	za
"	"	kIx"	"
<g/>
Osobnost	osobnost	k1gFnSc4	osobnost
roku	rok	k1gInSc2	rok
<g/>
"	"	kIx"	"
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
:	:	kIx,	:
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
měsících	měsíc	k1gInPc6	měsíc
následujících	následující	k2eAgInPc6d1	následující
po	po	k7c6	po
teroristických	teroristický	k2eAgInPc6d1	teroristický
útocích	útok	k1gInPc6	útok
na	na	k7c6	na
New	New	k1gFnSc6	New
York	York	k1gInSc1	York
v	v	k7c6	v
září	září	k1gNnSc6	září
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
občané	občan	k1gMnPc1	občan
USA	USA	kA	USA
pravidelně	pravidelně	k6eAd1	pravidelně
vyjadřovali	vyjadřovat	k5eAaImAgMnP	vyjadřovat
vysokou	vysoký	k2eAgFnSc4d1	vysoká
podporu	podpora	k1gFnSc4	podpora
prezidentu	prezident	k1gMnSc3	prezident
Bushovi	Bush	k1gMnSc3	Bush
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původních	původní	k2eAgMnPc2d1	původní
44	[number]	k4	44
ihned	ihned	k6eAd1	ihned
stoupla	stoupnout	k5eAaPmAgFnS	stoupnout
nad	nad	k7c7	nad
80	[number]	k4	80
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Žádnému	žádný	k3yNgMnSc3	žádný
americkému	americký	k2eAgMnSc3d1	americký
prezidentovi	prezident	k1gMnSc3	prezident
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
tak	tak	k9	tak
silnou	silný	k2eAgFnSc4d1	silná
podporu	podpora	k1gFnSc4	podpora
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
začaly	začít	k5eAaPmAgInP	začít
dělat	dělat	k5eAaImF	dělat
průzkumy	průzkum	k1gInPc1	průzkum
<g/>
.	.	kIx.	.
</s>
<s>
Popularitu	popularita	k1gFnSc4	popularita
si	se	k3xPyFc3	se
udržel	udržet	k5eAaPmAgInS	udržet
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
útocích	útok	k1gInPc6	útok
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
klesla	klesnout	k5eAaPmAgFnS	klesnout
na	na	k7c4	na
nižší	nízký	k2eAgFnSc4d2	nižší
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
překračovala	překračovat	k5eAaImAgFnS	překračovat
hranici	hranice	k1gFnSc4	hranice
50	[number]	k4	50
<g/>
%	%	kIx~	%
dva	dva	k4xCgInPc4	dva
a	a	k8xC	a
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c4	na
(	(	kIx(	(
<g/>
taktéž	taktéž	k?	taktéž
historických	historický	k2eAgInPc2d1	historický
<g/>
)	)	kIx)	)
22	[number]	k4	22
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
května	květen	k1gInSc2	květen
CNN	CNN	kA	CNN
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
George	Georg	k1gMnSc2	Georg
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
stal	stát	k5eAaPmAgMnS	stát
nejneoblíbenějším	oblíbený	k2eNgMnSc7d3	nejneoblíbenější
americkým	americký	k2eAgMnSc7d1	americký
prezidentem	prezident	k1gMnSc7	prezident
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
když	když	k8xS	když
překonal	překonat	k5eAaPmAgMnS	překonat
"	"	kIx"	"
<g/>
rekord	rekord	k1gInSc4	rekord
<g/>
"	"	kIx"	"
Richarda	Richard	k1gMnSc2	Richard
Nixona	Nixon	k1gMnSc2	Nixon
<g/>
,	,	kIx,	,
vyšetřovanému	vyšetřovaný	k1gMnSc3	vyšetřovaný
kvůli	kvůli	k7c3	kvůli
aféře	aféra	k1gFnSc3	aféra
Watergate	Watergat	k1gMnSc5	Watergat
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
politikou	politika	k1gFnSc7	politika
je	být	k5eAaImIp3nS	být
nespokojeno	spokojen	k2eNgNnSc1d1	nespokojeno
více	hodně	k6eAd2	hodně
než	než	k8xS	než
70	[number]	k4	70
%	%	kIx~	%
Američanů	Američan	k1gMnPc2	Američan
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavým	zajímavý	k2eAgNnSc7d1	zajímavé
oceněním	ocenění	k1gNnSc7	ocenění
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
Bush	Bush	k1gMnSc1	Bush
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
roku	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Kelly	Kella	k1gMnSc2	Kella
Miller	Miller	k1gMnSc1	Miller
a	a	k8xC	a
Quentin	Quentin	k1gMnSc1	Quentin
Wheeler	Wheeler	k1gMnSc1	Wheeler
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
za	za	k7c4	za
konzervativce	konzervativec	k1gMnPc4	konzervativec
a	a	k8xC	a
obdivují	obdivovat	k5eAaImIp3nP	obdivovat
prezidenta	prezident	k1gMnSc4	prezident
Bushe	Bush	k1gMnSc4	Bush
<g/>
,	,	kIx,	,
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
nově	nově	k6eAd1	nově
objeveného	objevený	k2eAgMnSc4d1	objevený
brouka	brouk	k1gMnSc4	brouk
<g/>
,	,	kIx,	,
Agathidium	Agathidium	k1gNnSc4	Agathidium
bushi	bush	k1gFnSc2	bush
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
jim	on	k3xPp3gMnPc3	on
telefonicky	telefonicky	k6eAd1	telefonicky
poděkoval	poděkovat	k5eAaPmAgMnS	poděkovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Cardinal	Cardinal	k1gMnSc1	Cardinal
John	John	k1gMnSc1	John
J.	J.	kA	J.
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Connor	Connor	k1gMnSc1	Connor
Pro-Life	Pro-Lif	k1gInSc5	Pro-Lif
Award	Award	k1gMnSc1	Award
od	od	k7c2	od
katolické	katolický	k2eAgFnSc2d1	katolická
organizace	organizace	k1gFnSc2	organizace
Legatus	Legatus	k1gMnSc1	Legatus
<g/>
.	.	kIx.	.
</s>
<s>
Bushova	Bushův	k2eAgFnSc1d1	Bushova
popularita	popularita	k1gFnSc1	popularita
mimo	mimo	k7c4	mimo
USA	USA	kA	USA
výrazně	výrazně	k6eAd1	výrazně
klesla	klesnout	k5eAaPmAgFnS	klesnout
po	po	k7c6	po
změně	změna	k1gFnSc6	změna
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
nastolení	nastolení	k1gNnSc4	nastolení
"	"	kIx"	"
<g/>
Bushovy	Bushův	k2eAgFnSc2d1	Bushova
doktríny	doktrína	k1gFnSc2	doktrína
<g/>
"	"	kIx"	"
a	a	k8xC	a
invazi	invaze	k1gFnSc4	invaze
do	do	k7c2	do
Iráku	Irák	k1gInSc2	Irák
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
mnoho	mnoho	k6eAd1	mnoho
lidí	člověk	k1gMnPc2	člověk
považovalo	považovat	k5eAaImAgNnS	považovat
za	za	k7c4	za
akt	akt	k1gInSc4	akt
unilateralismu	unilateralismus	k1gInSc2	unilateralismus
<g/>
.	.	kIx.	.
</s>
<s>
Průzkumy	průzkum	k1gInPc1	průzkum
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
ukazovaly	ukazovat	k5eAaImAgInP	ukazovat
"	"	kIx"	"
<g/>
transatlantické	transatlantický	k2eAgNnSc1d1	transatlantické
rozdělení	rozdělení	k1gNnSc1	rozdělení
kvůli	kvůli	k7c3	kvůli
válce	válka	k1gFnSc3	válka
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Průzkum	průzkum	k1gInSc1	průzkum
uskutečněný	uskutečněný	k2eAgInSc1d1	uskutečněný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
ukázal	ukázat	k5eAaPmAgInS	ukázat
jeho	jeho	k3xOp3gInSc1	jeho
negativní	negativní	k2eAgInSc1d1	negativní
obraz	obraz	k1gInSc1	obraz
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
občanů	občan	k1gMnPc2	občan
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
v	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
(	(	kIx(	(
<g/>
tam	tam	k6eAd1	tam
jeho	jeho	k3xOp3gFnSc1	jeho
popularita	popularita	k1gFnSc1	popularita
klesla	klesnout	k5eAaPmAgFnS	klesnout
z	z	k7c2	z
původních	původní	k2eAgInPc2d1	původní
83	[number]	k4	83
%	%	kIx~	%
na	na	k7c4	na
56	[number]	k4	56
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
(	(	kIx(	(
<g/>
z	z	k7c2	z
78	[number]	k4	78
%	%	kIx~	%
klesla	klesnout	k5eAaPmAgFnS	klesnout
na	na	k7c4	na
37	[number]	k4	37
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Turecku	Turecko	k1gNnSc6	Turecko
(	(	kIx(	(
<g/>
z	z	k7c2	z
52	[number]	k4	52
%	%	kIx~	%
na	na	k7c4	na
13	[number]	k4	13
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
muslimských	muslimský	k2eAgFnPc6d1	muslimská
zemích	zem	k1gFnPc6	zem
neoblíbenost	neoblíbenost	k1gFnSc1	neoblíbenost
amerického	americký	k2eAgMnSc2d1	americký
prezidenta	prezident	k1gMnSc2	prezident
často	často	k6eAd1	často
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
90	[number]	k4	90
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnPc1d3	nejvyšší
popularity	popularita	k1gFnPc1	popularita
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
USA	USA	kA	USA
<g/>
,	,	kIx,	,
dosahoval	dosahovat	k5eAaImAgMnS	dosahovat
Bush	Bush	k1gMnSc1	Bush
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gMnSc4	on
podporovalo	podporovat	k5eAaImAgNnS	podporovat
62	[number]	k4	62
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ankety	anketa	k1gFnSc2	anketa
Pew	Pew	k1gMnPc1	Pew
Research	Researcha	k1gFnPc2	Researcha
Center	centrum	k1gNnPc2	centrum
je	být	k5eAaImIp3nS	být
George	Georg	k1gMnPc4	Georg
Walker	Walker	k1gMnSc1	Walker
Bush	Bush	k1gMnSc1	Bush
mezi	mezi	k7c7	mezi
61	[number]	k4	61
%	%	kIx~	%
profesionálních	profesionální	k2eAgMnPc2d1	profesionální
historiků	historik	k1gMnPc2	historik
hodnocen	hodnotit	k5eAaImNgMnS	hodnotit
jako	jako	k9	jako
nejhorší	zlý	k2eAgMnSc1d3	nejhorší
prezident	prezident	k1gMnSc1	prezident
amerických	americký	k2eAgFnPc2d1	americká
dějin	dějiny	k1gFnPc2	dějiny
<g/>
;	;	kIx,	;
za	za	k7c4	za
selhání	selhání	k1gNnSc4	selhání
jeho	jeho	k3xOp3gNnSc2	jeho
prezidenství	prezidenství	k1gNnSc2	prezidenství
považuje	považovat	k5eAaImIp3nS	považovat
dokonce	dokonce	k9	dokonce
98	[number]	k4	98
%	%	kIx~	%
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2005	[number]	k4	2005
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Tbilisi	Tbilisi	k1gNnSc6	Tbilisi
při	při	k7c6	při
návštěvě	návštěva	k1gFnSc6	návštěva
Gruzie	Gruzie	k1gFnSc2	Gruzie
spáchán	spáchán	k2eAgInSc4d1	spáchán
neúspěšný	úspěšný	k2eNgInSc4d1	neúspěšný
atentát	atentát	k1gInSc4	atentát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zasedání	zasedání	k1gNnSc6	zasedání
Tribunálu	tribunál	k1gInSc2	tribunál
pro	pro	k7c4	pro
válečné	válečný	k2eAgInPc4d1	válečný
zločiny	zločin	k1gInPc4	zločin
v	v	k7c4	v
Kuala	Kualo	k1gNnPc4	Kualo
Lumpur	Lumpura	k1gFnPc2	Lumpura
(	(	kIx(	(
<g/>
soukromé	soukromý	k2eAgFnSc2d1	soukromá
iniciativy	iniciativa	k1gFnSc2	iniciativa
založené	založený	k2eAgFnSc2d1	založená
bývalým	bývalý	k2eAgMnSc7d1	bývalý
malajským	malajský	k2eAgMnSc7d1	malajský
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
Mahathirem	Mahathir	k1gMnSc7	Mahathir
Mohamadem	Mohamad	k1gInSc7	Mohamad
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
několika	několik	k4yIc7	několik
členy	člen	k1gInPc7	člen
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
mj.	mj.	kA	mj.
Tonym	Tony	k1gMnSc7	Tony
Blairem	Blair	k1gMnSc7	Blair
označen	označit	k5eAaPmNgMnS	označit
"	"	kIx"	"
<g/>
vinným	vinný	k2eAgMnSc7d1	vinný
v	v	k7c6	v
plném	plný	k2eAgInSc6d1	plný
rozsahu	rozsah	k1gInSc6	rozsah
<g/>
"	"	kIx"	"
z	z	k7c2	z
"	"	kIx"	"
<g/>
genocidy	genocida	k1gFnSc2	genocida
a	a	k8xC	a
zločinů	zločin	k1gInPc2	zločin
proti	proti	k7c3	proti
lidskosti	lidskost	k1gFnSc3	lidskost
<g/>
"	"	kIx"	"
během	během	k7c2	během
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2012	[number]	k4	2012
se	se	k3xPyFc4	se
malajský	malajský	k2eAgInSc1d1	malajský
tribunál	tribunál	k1gInSc1	tribunál
jednomyslně	jednomyslně	k6eAd1	jednomyslně
shodl	shodnout	k5eAaBmAgInS	shodnout
podle	podle	k7c2	podle
článku	článek	k1gInSc2	článek
6	[number]	k4	6
Norimberské	norimberský	k2eAgFnSc2d1	Norimberská
charty	charta	k1gFnSc2	charta
o	o	k7c6	o
jejich	jejich	k3xOp3gFnSc6	jejich
vině	vina	k1gFnSc6	vina
na	na	k7c6	na
mučení	mučení	k1gNnSc6	mučení
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
důkazy	důkaz	k1gInPc1	důkaz
byly	být	k5eAaImAgFnP	být
spolehlivě	spolehlivě	k6eAd1	spolehlivě
prokázány	prokázán	k2eAgFnPc1d1	prokázána
a	a	k8xC	a
že	že	k8xS	že
Bush	Bush	k1gMnSc1	Bush
a	a	k8xC	a
členové	člen	k1gMnPc1	člen
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
si	se	k3xPyFc3	se
byli	být	k5eAaImAgMnP	být
vědomi	vědom	k2eAgMnPc1d1	vědom
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
porušují	porušovat	k5eAaImIp3nP	porušovat
konvenci	konvence	k1gFnSc4	konvence
o	o	k7c4	o
mučení	mučení	k1gNnSc4	mučení
z	z	k7c2	z
r.	r.	kA	r.
1984	[number]	k4	1984
a	a	k8xC	a
Ženevské	ženevský	k2eAgFnSc2d1	Ženevská
úmluvy	úmluva	k1gFnSc2	úmluva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nezasáhli	zasáhnout	k5eNaPmAgMnP	zasáhnout
proti	proti	k7c3	proti
jejich	jejich	k3xOp3gNnSc3	jejich
porušování	porušování	k1gNnSc3	porušování
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výkonného	výkonný	k2eAgMnSc2d1	výkonný
ředitele	ředitel	k1gMnSc2	ředitel
hnutí	hnutí	k1gNnSc2	hnutí
Human	Human	k1gMnSc1	Human
Rights	Rightsa	k1gFnPc2	Rightsa
Watch	Watch	k1gMnSc1	Watch
<g/>
,	,	kIx,	,
Kennetha	Kennetha	k1gMnSc1	Kennetha
Rotha	Roth	k1gMnSc2	Roth
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
právní	právní	k2eAgFnSc4d1	právní
povinnost	povinnost	k1gFnSc4	povinnost
Bushe	Bush	k1gMnSc2	Bush
zadržet	zadržet	k5eAaPmF	zadržet
a	a	k8xC	a
vydat	vydat	k5eAaPmF	vydat
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
ukáže	ukázat	k5eAaPmIp3nS	ukázat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnSc4	jejich
nečinnost	nečinnost	k1gFnSc4	nečinnost
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
tak	tak	k6eAd1	tak
učinit	učinit	k5eAaPmF	učinit
jiné	jiný	k2eAgInPc4d1	jiný
státy	stát	k1gInPc4	stát
<g/>
.	.	kIx.	.
</s>
