<s>
Opavice	Opavice	k1gFnSc1	Opavice
(	(	kIx(	(
<g/>
lidově	lidově	k6eAd1	lidově
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
Opavice	Opavice	k1gFnSc1	Opavice
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Goldoppa	Goldoppa	k1gFnSc1	Goldoppa
<g/>
,	,	kIx,	,
polsky	polsky	k6eAd1	polsky
Opawica	Opawic	k2eAgFnSc1d1	Opawic
<g/>
,	,	kIx,	,
slezsky	slezsky	k6eAd1	slezsky
Złota	Złota	k1gMnSc1	Złota
Opawica	Opawica	k1gMnSc1	Opawica
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
levostranný	levostranný	k2eAgInSc1d1	levostranný
přítok	přítok	k1gInSc1	přítok
řeky	řeka	k1gFnSc2	řeka
Opavy	Opava	k1gFnSc2	Opava
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Bruntál	Bruntál	k1gInSc1	Bruntál
v	v	k7c6	v
Moravskoslezském	moravskoslezský	k2eAgInSc6d1	moravskoslezský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
toku	tok	k1gInSc2	tok
činí	činit	k5eAaImIp3nS	činit
35,7	[number]	k4	35,7
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Plocha	plocha	k1gFnSc1	plocha
povodí	povodí	k1gNnSc2	povodí
měří	měřit	k5eAaImIp3nS	měřit
194,2	[number]	k4	194,2
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
pramení	pramenit	k5eAaImIp3nS	pramenit
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Heřmanovic	Heřmanovice	k1gFnPc2	Heřmanovice
na	na	k7c6	na
jihozápadních	jihozápadní	k2eAgInPc6d1	jihozápadní
svazích	svah	k1gInPc6	svah
Příčného	příčný	k2eAgInSc2d1	příčný
vrchu	vrch	k1gInSc2	vrch
(	(	kIx(	(
<g/>
974	[number]	k4	974
m	m	kA	m
<g/>
)	)	kIx)	)
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
850	[number]	k4	850
m.	m.	k?	m.
Nejprve	nejprve	k6eAd1	nejprve
její	její	k3xOp3gInSc4	její
tok	tok	k1gInSc4	tok
směřuje	směřovat	k5eAaImIp3nS	směřovat
na	na	k7c4	na
jihovýchod	jihovýchod	k1gInSc4	jihovýchod
<g/>
,	,	kIx,	,
protéká	protékat	k5eAaImIp3nS	protékat
výše	vysoce	k6eAd2	vysoce
zmíněnou	zmíněný	k2eAgFnSc7d1	zmíněná
obcí	obec	k1gFnSc7	obec
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
ústím	ústí	k1gNnSc7	ústí
Tisové	tisový	k2eAgFnSc2d1	Tisová
se	se	k3xPyFc4	se
říčka	říčka	k1gFnSc1	říčka
stáčí	stáčet	k5eAaImIp3nS	stáčet
více	hodně	k6eAd2	hodně
na	na	k7c4	na
východ	východ	k1gInSc4	východ
k	k	k7c3	k
obci	obec	k1gFnSc3	obec
Holčovice	Holčovice	k1gFnSc2	Holčovice
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
zhruba	zhruba	k6eAd1	zhruba
3	[number]	k4	3
km	km	kA	km
teče	téct	k5eAaImIp3nS	téct
severovýchodním	severovýchodní	k2eAgInSc7d1	severovýchodní
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
osadě	osada	k1gFnSc3	osada
Hynčice	Hynčice	k1gFnSc2	Hynčice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
obrací	obracet	k5eAaImIp3nS	obracet
opět	opět	k6eAd1	opět
k	k	k7c3	k
jihovýchodu	jihovýchod	k1gInSc3	jihovýchod
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dalších	další	k2eAgInPc6d1	další
čtyřech	čtyři	k4xCgInPc6	čtyři
kilometrech	kilometr	k1gInPc6	kilometr
protéká	protékat	k5eAaImIp3nS	protékat
Opavice	Opavice	k1gFnSc1	Opavice
Městem	město	k1gNnSc7	město
Albrechtice	Albrechtice	k1gFnPc1	Albrechtice
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
nímž	jenž	k3xRgNnSc7	jenž
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
cca	cca	kA	cca
10	[number]	k4	10
km	km	kA	km
tvoří	tvořit	k5eAaImIp3nP	tvořit
státní	státní	k2eAgFnSc4d1	státní
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
(	(	kIx(	(
<g/>
Opolské	opolský	k2eAgNnSc1d1	Opolské
vojvodství	vojvodství	k1gNnSc1	vojvodství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
zhruba	zhruba	k6eAd1	zhruba
3,0	[number]	k4	3,0
říčního	říční	k2eAgInSc2d1	říční
kilometru	kilometr	k1gInSc2	kilometr
až	až	k9	až
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
ústí	ústí	k1gNnSc3	ústí
řeka	řeka	k1gFnSc1	řeka
protéká	protékat	k5eAaImIp3nS	protékat
okrajem	okraj	k1gInSc7	okraj
Krnova	Krnov	k1gInSc2	Krnov
<g/>
.	.	kIx.	.
levé	levá	k1gFnSc2	levá
–	–	k?	–
Komorský	Komorský	k2eAgInSc1d1	Komorský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Solný	solný	k2eAgInSc1d1	solný
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Valštejnský	Valštejnský	k2eAgInSc1d1	Valštejnský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Láč	Láč	k1gFnSc1	Láč
(	(	kIx(	(
<g/>
Láčský	Láčský	k2eAgInSc1d1	Láčský
potok	potok	k1gInSc1	potok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mohla	moct	k5eAaImAgFnS	moct
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Potok	potok	k1gInSc1	potok
Mokry	mokro	k1gNnPc7	mokro
nebo	nebo	k8xC	nebo
Radynka	Radynka	k1gFnSc1	Radynka
<g/>
)	)	kIx)	)
pravé	pravý	k2eAgNnSc1d1	pravé
–	–	k?	–
Tisová	tisový	k2eAgFnSc1d1	Tisová
<g/>
,	,	kIx,	,
Burkvízský	Burkvízský	k2eAgInSc1d1	Burkvízský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Kobylí	kobylí	k2eAgInSc1d1	kobylí
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Hůrka	hůrka	k1gFnSc1	hůrka
<g/>
,	,	kIx,	,
Ježnický	Ježnický	k2eAgInSc1d1	Ježnický
potok	potok	k1gInSc1	potok
Průměrný	průměrný	k2eAgInSc1d1	průměrný
průtok	průtok	k1gInSc1	průtok
v	v	k7c6	v
Krnově	Krnov	k1gInSc6	Krnov
činí	činit	k5eAaImIp3nS	činit
1,5	[number]	k4	1,5
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Hlásný	hlásný	k2eAgInSc4d1	hlásný
profil	profil	k1gInSc4	profil
<g/>
:	:	kIx,	:
V	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
Opawica	Opawic	k1gInSc2	Opawic
–	–	k?	–
Chomiąża	Chomiążus	k1gMnSc2	Chomiążus
tvoří	tvořit	k5eAaImIp3nS	tvořit
řeka	řeka	k1gFnSc1	řeka
Opavice	Opavice	k1gFnSc2	Opavice
hranici	hranice	k1gFnSc4	hranice
polského	polský	k2eAgInSc2d1	polský
Přírodního	přírodní	k2eAgInSc2d1	přírodní
parku	park	k1gInSc2	park
Rajón	rajón	k1gInSc1	rajón
Mokre	Mokr	k1gInSc5	Mokr
-	-	kIx~	-
Lewice	Lewice	k1gFnSc2	Lewice
<g/>
.	.	kIx.	.
</s>
