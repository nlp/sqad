<p>
<s>
Prase	prase	k1gNnSc1	prase
divoké	divoký	k2eAgFnSc2d1	divoká
(	(	kIx(	(
<g/>
Sus	Sus	k1gFnSc2	Sus
scrofa	scrof	k1gMnSc2	scrof
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgMnSc1d1	velký
sudokopytník	sudokopytník	k1gMnSc1	sudokopytník
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
prasatovití	prasatovitý	k2eAgMnPc1d1	prasatovitý
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
domovinou	domovina	k1gFnSc7	domovina
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
člověkem	člověk	k1gMnSc7	člověk
byl	být	k5eAaImAgInS	být
zavlečen	zavlečen	k2eAgInSc1d1	zavlečen
i	i	k9	i
na	na	k7c4	na
jiné	jiný	k2eAgInPc4d1	jiný
světové	světový	k2eAgInPc4d1	světový
kontinenty	kontinent	k1gInPc4	kontinent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
typického	typický	k2eAgMnSc4d1	typický
všežravce	všežravec	k1gMnSc4	všežravec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
k	k	k7c3	k
životu	život	k1gInSc3	život
preferuje	preferovat	k5eAaImIp3nS	preferovat
staré	starý	k2eAgInPc4d1	starý
lesní	lesní	k2eAgInPc4d1	lesní
porosty	porost	k1gInPc4	porost
<g/>
.	.	kIx.	.
</s>
<s>
Prase	prase	k1gNnSc1	prase
divoké	divoký	k2eAgNnSc1d1	divoké
bylo	být	k5eAaImAgNnS	být
vždy	vždy	k6eAd1	vždy
významnou	významný	k2eAgFnSc7d1	významná
lovnou	lovný	k2eAgFnSc7d1	lovná
zvěří	zvěř	k1gFnSc7	zvěř
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
objevuje	objevovat	k5eAaImIp3nS	objevovat
ve	v	k7c6	v
folkloru	folklor	k1gInSc6	folklor
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
původcem	původce	k1gMnSc7	původce
prasete	prase	k1gNnSc2	prase
domácího	domácí	k2eAgNnSc2d1	domácí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
pro	pro	k7c4	pro
maso	maso	k1gNnSc4	maso
a	a	k8xC	a
tuk	tuk	k1gInSc4	tuk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prasata	prase	k1gNnPc4	prase
divoká	divoký	k2eAgFnSc1d1	divoká
byla	být	k5eAaImAgFnS	být
odpradávna	odpradávna	k6eAd1	odpradávna
vnímána	vnímat	k5eAaImNgFnS	vnímat
jako	jako	k8xS	jako
škodná	škodný	k2eAgFnSc1d1	škodná
zvěř	zvěř	k1gFnSc1	zvěř
a	a	k8xC	a
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
důvodů	důvod	k1gInPc2	důvod
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
omezen	omezit	k5eAaPmNgMnS	omezit
i	i	k9	i
jejich	jejich	k3xOp3gInSc1	jejich
výskyt	výskyt	k1gInSc1	výskyt
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
prakticky	prakticky	k6eAd1	prakticky
na	na	k7c4	na
nulu	nula	k1gFnSc4	nula
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Prase	prase	k1gNnSc1	prase
divoké	divoký	k2eAgNnSc1d1	divoké
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgMnSc1d1	velký
sudokopytník	sudokopytník	k1gMnSc1	sudokopytník
<g/>
.	.	kIx.	.
</s>
<s>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
samci	samec	k1gMnPc1	samec
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
délky	délka	k1gFnSc2	délka
120	[number]	k4	120
<g/>
–	–	k?	–
<g/>
180	[number]	k4	180
cm	cm	kA	cm
a	a	k8xC	a
v	v	k7c6	v
kohoutku	kohoutek	k1gInSc6	kohoutek
měří	měřit	k5eAaImIp3nS	měřit
55	[number]	k4	55
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
hmotnost	hmotnost	k1gFnSc1	hmotnost
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
různorodá	různorodý	k2eAgFnSc1d1	různorodá
a	a	k8xC	a
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
oblastech	oblast	k1gFnPc6	oblast
se	se	k3xPyFc4	se
viditelně	viditelně	k6eAd1	viditelně
liší	lišit	k5eAaImIp3nP	lišit
<g/>
,	,	kIx,	,
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
však	však	k9	však
činí	činit	k5eAaImIp3nS	činit
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
90	[number]	k4	90
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
zastřelen	zastřelen	k2eAgInSc4d1	zastřelen
samec	samec	k1gInSc4	samec
vážící	vážící	k2eAgInSc4d1	vážící
celých	celý	k2eAgInPc2d1	celý
277	[number]	k4	277
kg	kg	kA	kg
<g/>
,	,	kIx,	,
v	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
a	a	k8xC	a
Rusku	Rusko	k1gNnSc6	Rusko
byli	být	k5eAaImAgMnP	být
několikrát	několikrát	k6eAd1	několikrát
zaznamenáni	zaznamenán	k2eAgMnPc1d1	zaznamenán
i	i	k9	i
samci	samec	k1gMnPc1	samec
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
téměř	téměř	k6eAd1	téměř
300	[number]	k4	300
kg	kg	kA	kg
<g/>
.	.	kIx.	.
<g/>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
poměru	poměr	k1gInSc3	poměr
těla	tělo	k1gNnSc2	tělo
má	mít	k5eAaImIp3nS	mít
prase	prase	k1gNnSc1	prase
divoké	divoký	k2eAgNnSc1d1	divoké
nápadně	nápadně	k6eAd1	nápadně
velkou	velký	k2eAgFnSc4d1	velká
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
krátké	krátký	k2eAgFnPc4d1	krátká
končetiny	končetina	k1gFnPc4	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
srst	srst	k1gFnSc1	srst
je	být	k5eAaImIp3nS	být
tvořená	tvořený	k2eAgFnSc1d1	tvořená
hustými	hustý	k2eAgFnPc7d1	hustá
štětinami	štětina	k1gFnPc7	štětina
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
ho	on	k3xPp3gMnSc4	on
chrání	chránit	k5eAaImIp3nS	chránit
před	před	k7c7	před
nepříznivými	příznivý	k2eNgInPc7d1	nepříznivý
vlivy	vliv	k1gInPc7	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
zbarvení	zbarvení	k1gNnSc1	zbarvení
kolísá	kolísat	k5eAaImIp3nS	kolísat
mezi	mezi	k7c7	mezi
tmavošedou	tmavošedý	k2eAgFnSc7d1	tmavošedá
přes	přes	k7c4	přes
hnědou	hnědý	k2eAgFnSc4d1	hnědá
až	až	k9	až
k	k	k7c3	k
černé	černá	k1gFnSc3	černá
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
zimy	zima	k1gFnSc2	zima
je	být	k5eAaImIp3nS	být
srst	srst	k1gFnSc1	srst
výrazněji	výrazně	k6eAd2	výrazně
hustší	hustý	k2eAgFnSc1d2	hustší
a	a	k8xC	a
tmavší	tmavý	k2eAgFnSc1d2	tmavší
<g/>
.	.	kIx.	.
</s>
<s>
Selata	sele	k1gNnPc1	sele
jsou	být	k5eAaImIp3nP	být
zpočátku	zpočátku	k6eAd1	zpočátku
hnědá	hnědý	k2eAgNnPc1d1	hnědé
a	a	k8xC	a
světle	světle	k6eAd1	světle
pruhovaná	pruhovaný	k2eAgFnSc1d1	pruhovaná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
selata	sele	k1gNnPc1	sele
nazývají	nazývat	k5eAaImIp3nP	nazývat
markazíni	markazín	k1gMnPc1	markazín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
nápadné	nápadný	k2eAgInPc4d1	nápadný
znaky	znak	k1gInPc4	znak
prasete	prase	k1gNnSc2	prase
divokého	divoký	k2eAgNnSc2d1	divoké
patří	patřit	k5eAaImIp3nP	patřit
jeho	jeho	k3xOp3gInPc1	jeho
čtyři	čtyři	k4xCgInPc1	čtyři
výrazné	výrazný	k2eAgInPc1d1	výrazný
trojhranné	trojhranný	k2eAgInPc1d1	trojhranný
špičáky	špičák	k1gInPc1	špičák
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
se	se	k3xPyFc4	se
při	při	k7c6	při
dorůstání	dorůstání	k1gNnSc6	dorůstání
zahýbají	zahýbat	k5eAaPmIp3nP	zahýbat
vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
hlavně	hlavně	k9	hlavně
jako	jako	k9	jako
účinná	účinný	k2eAgFnSc1d1	účinná
zbraň	zbraň	k1gFnSc1	zbraň
při	při	k7c6	při
soubojích	souboj	k1gInPc6	souboj
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
samců	samec	k1gMnPc2	samec
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
obvykle	obvykle	k6eAd1	obvykle
20	[number]	k4	20
cm	cm	kA	cm
<g/>
,	,	kIx,	,
ve	v	k7c6	v
výjimečných	výjimečný	k2eAgInPc6d1	výjimečný
případech	případ	k1gInPc6	případ
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
dorůst	dorůst	k5eAaPmF	dorůst
až	až	k9	až
do	do	k7c2	do
délky	délka	k1gFnSc2	délka
30	[number]	k4	30
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgInPc4d1	spodní
špičáky	špičák	k1gInPc4	špičák
kňourů	kňour	k1gMnPc2	kňour
myslivci	myslivec	k1gMnPc1	myslivec
nazývají	nazývat	k5eAaImIp3nP	nazývat
páráky	párák	k1gMnPc4	párák
a	a	k8xC	a
menší	malý	k2eAgMnPc4d2	menší
horní	horní	k2eAgMnPc4d1	horní
špičáky	špičák	k1gMnPc4	špičák
klektáky	klekták	k1gInPc7	klekták
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
samic	samice	k1gFnPc2	samice
jsou	být	k5eAaImIp3nP	být
viditelně	viditelně	k6eAd1	viditelně
menší	malý	k2eAgFnPc1d2	menší
a	a	k8xC	a
směřují	směřovat	k5eAaImIp3nP	směřovat
směrem	směr	k1gInSc7	směr
nahoru	nahoru	k6eAd1	nahoru
výrazně	výrazně	k6eAd1	výrazně
mírněji	mírně	k6eAd2	mírně
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
starších	starý	k2eAgMnPc2d2	starší
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Prase	prase	k1gNnSc1	prase
divoké	divoký	k2eAgNnSc1d1	divoké
má	mít	k5eAaImIp3nS	mít
skvěle	skvěle	k6eAd1	skvěle
vyvinutý	vyvinutý	k2eAgInSc4d1	vyvinutý
čich	čich	k1gInSc4	čich
a	a	k8xC	a
sluch	sluch	k1gInSc4	sluch
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc7	jeho
nejhůře	zle	k6eAd3	zle
vyvinutým	vyvinutý	k2eAgInSc7d1	vyvinutý
smyslem	smysl	k1gInSc7	smysl
je	být	k5eAaImIp3nS	být
zrak	zrak	k1gInSc1	zrak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Nejenže	nejenže	k6eAd1	nejenže
je	být	k5eAaImIp3nS	být
prase	prase	k1gNnSc1	prase
divoké	divoký	k2eAgFnSc2d1	divoká
daleko	daleko	k6eAd1	daleko
nejrozšířenějším	rozšířený	k2eAgMnSc7d3	nejrozšířenější
zástupcem	zástupce	k1gMnSc7	zástupce
celé	celá	k1gFnSc2	celá
své	svůj	k3xOyFgFnSc2	svůj
čeledi	čeleď	k1gFnSc2	čeleď
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
navíc	navíc	k6eAd1	navíc
patří	patřit	k5eAaImIp3nS	patřit
–	–	k?	–
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
díky	díky	k7c3	díky
zásahu	zásah	k1gInSc3	zásah
člověka	člověk	k1gMnSc2	člověk
–	–	k?	–
i	i	k9	i
mezi	mezi	k7c4	mezi
nejrozšířenější	rozšířený	k2eAgMnPc4d3	nejrozšířenější
pozemské	pozemský	k2eAgMnPc4d1	pozemský
savce	savec	k1gMnPc4	savec
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
areál	areál	k1gInSc1	areál
rozšíření	rozšíření	k1gNnSc2	rozšíření
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
staletí	staletí	k1gNnPc2	staletí
značně	značně	k6eAd1	značně
měnil	měnit	k5eAaImAgInS	měnit
a	a	k8xC	a
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
dříve	dříve	k6eAd2	dříve
běžný	běžný	k2eAgInSc1d1	běžný
(	(	kIx(	(
<g/>
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
,	,	kIx,	,
Egypt	Egypt	k1gInSc1	Egypt
<g/>
,	,	kIx,	,
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
,	,	kIx,	,
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vyhuben	vyhuben	k2eAgInSc1d1	vyhuben
<g/>
,	,	kIx,	,
do	do	k7c2	do
některých	některý	k3yIgFnPc2	některý
byl	být	k5eAaImAgMnS	být
naopak	naopak	k6eAd1	naopak
zavlečen	zavlečen	k2eAgMnSc1d1	zavlečen
a	a	k8xC	a
hojně	hojně	k6eAd1	hojně
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
však	však	k9	však
prase	prase	k1gNnSc1	prase
většinu	většina	k1gFnSc4	většina
svého	svůj	k3xOyFgInSc2	svůj
původního	původní	k2eAgInSc2d1	původní
areálu	areál	k1gInSc2	areál
znovu	znovu	k6eAd1	znovu
osídlilo	osídlit	k5eAaPmAgNnS	osídlit
a	a	k8xC	a
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
přemnoženým	přemnožený	k2eAgInSc7d1	přemnožený
druhem	druh	k1gInSc7	druh
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
domovinou	domovina	k1gFnSc7	domovina
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
celá	celý	k2eAgFnSc1d1	celá
Evropa	Evropa	k1gFnSc1	Evropa
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
části	část	k1gFnSc2	část
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
a	a	k8xC	a
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
vyhuben	vyhubit	k5eAaPmNgInS	vyhubit
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
reintrodukován	reintrodukován	k2eAgInSc1d1	reintrodukován
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
volné	volný	k2eAgFnSc2d1	volná
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
však	však	k9	však
již	již	k6eAd1	již
obývá	obývat	k5eAaImIp3nS	obývat
všechny	všechen	k3xTgInPc4	všechen
světové	světový	k2eAgInPc4d1	světový
kontinenty	kontinent	k1gInPc4	kontinent
kromě	kromě	k7c2	kromě
Antarktidy	Antarktida	k1gFnSc2	Antarktida
<g/>
.	.	kIx.	.
<g/>
Ačkoli	ačkoli	k8xS	ačkoli
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
jeho	jeho	k3xOp3gInSc1	jeho
populační	populační	k2eAgInSc1d1	populační
trend	trend	k1gInSc1	trend
nejistý	jistý	k2eNgInSc1d1	nejistý
<g/>
,	,	kIx,	,
na	na	k7c6	na
území	území	k1gNnSc6	území
mnoha	mnoho	k4c2	mnoho
států	stát	k1gInPc2	stát
jeho	jeho	k3xOp3gFnSc1	jeho
početnost	početnost	k1gFnSc1	početnost
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
rapidně	rapidně	k6eAd1	rapidně
stoupla	stoupnout	k5eAaPmAgFnS	stoupnout
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
vážného	vážný	k2eAgMnSc4d1	vážný
škůdce	škůdce	k1gMnSc4	škůdce
<g/>
.	.	kIx.	.
</s>
<s>
Výrazné	výrazný	k2eAgNnSc1d1	výrazné
navýšení	navýšení	k1gNnSc1	navýšení
početnosti	početnost	k1gFnSc2	početnost
prasat	prase	k1gNnPc2	prase
divokých	divoký	k2eAgMnPc2d1	divoký
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
i	i	k9	i
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
byla	být	k5eAaImAgNnP	být
prasata	prase	k1gNnPc1	prase
divoká	divoký	k2eAgNnPc1d1	divoké
do	do	k7c2	do
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
prakticky	prakticky	k6eAd1	prakticky
vyhubena	vyhubit	k5eAaPmNgFnS	vyhubit
a	a	k8xC	a
chovala	chovat	k5eAaImAgFnS	chovat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
oborách	obora	k1gFnPc6	obora
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
trend	trend	k1gInSc1	trend
přetrval	přetrvat	k5eAaPmAgInS	přetrvat
celé	celá	k1gFnSc2	celá
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
první	první	k4xOgFnSc4	první
polovinu	polovina	k1gFnSc4	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgNnP	objevit
volně	volně	k6eAd1	volně
žijící	žijící	k2eAgNnPc1d1	žijící
divoká	divoký	k2eAgNnPc1d1	divoké
prasata	prase	k1gNnPc1	prase
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
zvířata	zvíře	k1gNnPc4	zvíře
uprchlá	uprchlý	k2eAgNnPc4d1	uprchlé
ze	z	k7c2	z
zrušených	zrušený	k2eAgFnPc2d1	zrušená
obor	obora	k1gFnPc2	obora
i	i	k9	i
o	o	k7c4	o
migranty	migrant	k1gMnPc4	migrant
z	z	k7c2	z
území	území	k1gNnSc2	území
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
a	a	k8xC	a
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Divoká	divoký	k2eAgNnPc1d1	divoké
prasata	prase	k1gNnPc1	prase
se	se	k3xPyFc4	se
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
přírodě	příroda	k1gFnSc6	příroda
dokázala	dokázat	k5eAaPmAgFnS	dokázat
výborně	výborně	k6eAd1	výborně
adaptovat	adaptovat	k5eAaBmF	adaptovat
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
počet	počet	k1gInSc1	počet
divokých	divoký	k2eAgNnPc2d1	divoké
prasat	prase	k1gNnPc2	prase
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
rapidně	rapidně	k6eAd1	rapidně
vzrůstá	vzrůstat	k5eAaImIp3nS	vzrůstat
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
oblastech	oblast	k1gFnPc6	oblast
již	již	k6eAd1	již
přemnoženým	přemnožený	k2eAgInSc7d1	přemnožený
druhem	druh	k1gInSc7	druh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
optimálním	optimální	k2eAgNnSc6d1	optimální
prostředí	prostředí	k1gNnSc6	prostředí
přitom	přitom	k6eAd1	přitom
připadají	připadat	k5eAaImIp3nP	připadat
zhruba	zhruba	k6eAd1	zhruba
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
kusy	kus	k1gInPc4	kus
<g/>
/	/	kIx~	/
<g/>
100	[number]	k4	100
ha	ha	kA	ha
lesa	les	k1gInSc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Předpokladem	předpoklad	k1gInSc7	předpoklad
jejich	jejich	k3xOp3gNnSc2	jejich
šíření	šíření	k1gNnSc2	šíření
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
plodnost	plodnost	k1gFnSc1	plodnost
<g/>
,	,	kIx,	,
absence	absence	k1gFnSc1	absence
přirozených	přirozený	k2eAgMnPc2d1	přirozený
predátorů	predátor	k1gMnPc2	predátor
a	a	k8xC	a
schopnost	schopnost	k1gFnSc4	schopnost
využívat	využívat	k5eAaImF	využívat
téměř	téměř	k6eAd1	téměř
jakékoli	jakýkoli	k3yIgInPc4	jakýkoli
dostupné	dostupný	k2eAgInPc4d1	dostupný
potravní	potravní	k2eAgInPc4d1	potravní
zdroje	zdroj	k1gInPc4	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc4d1	velké
přemnožení	přemnožení	k1gNnSc4	přemnožení
a	a	k8xC	a
stížnosti	stížnost	k1gFnPc1	stížnost
obyvatel	obyvatel	k1gMnPc2	obyvatel
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
organizování	organizování	k1gNnSc3	organizování
hromadných	hromadný	k2eAgInPc2d1	hromadný
odstřelů	odstřel	k1gInPc2	odstřel
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
Brdech	Brdy	k1gInPc6	Brdy
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
počátkem	počátkem	k7c2	počátkem
prosince	prosinec	k1gInSc2	prosinec
2013	[number]	k4	2013
bylo	být	k5eAaImAgNnS	být
zastřeleno	zastřelit	k5eAaPmNgNnS	zastřelit
140	[number]	k4	140
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
<g/>
Prase	prase	k1gNnSc1	prase
divoké	divoký	k2eAgNnSc1d1	divoké
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
životu	život	k1gInSc3	život
preferuje	preferovat	k5eAaImIp3nS	preferovat
zejména	zejména	k9	zejména
staré	starý	k2eAgInPc4d1	starý
listnaté	listnatý	k2eAgInPc4d1	listnatý
lesy	les	k1gInPc4	les
s	s	k7c7	s
bohatým	bohatý	k2eAgInSc7d1	bohatý
podrostem	podrost	k1gInSc7	podrost
a	a	k8xC	a
s	s	k7c7	s
dostatkem	dostatek	k1gInSc7	dostatek
příležitostí	příležitost	k1gFnPc2	příležitost
k	k	k7c3	k
bahenním	bahenní	k2eAgFnPc3d1	bahenní
"	"	kIx"	"
<g/>
koupelím	koupel	k1gFnPc3	koupel
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etologie	etologie	k1gFnSc2	etologie
==	==	k?	==
</s>
</p>
<p>
<s>
Prase	prase	k1gNnSc1	prase
divoké	divoký	k2eAgNnSc1d1	divoké
je	být	k5eAaImIp3nS	být
aktivní	aktivní	k2eAgNnSc1d1	aktivní
zejména	zejména	k9	zejména
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
den	den	k1gInSc4	den
tráví	trávit	k5eAaImIp3nP	trávit
většinou	většinou	k6eAd1	většinou
odpočinkem	odpočinek	k1gInSc7	odpočinek
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
skrytě	skrytě	k6eAd1	skrytě
žijící	žijící	k2eAgMnPc4d1	žijící
tvory	tvor	k1gMnPc4	tvor
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
svým	svůj	k3xOyFgMnPc3	svůj
dokonale	dokonale	k6eAd1	dokonale
vyvinutým	vyvinutý	k2eAgInPc3d1	vyvinutý
smyslům	smysl	k1gInPc3	smysl
většinou	většinou	k6eAd1	většinou
velmi	velmi	k6eAd1	velmi
účinně	účinně	k6eAd1	účinně
vyhýbají	vyhýbat	k5eAaImIp3nP	vyhýbat
přítomnosti	přítomnost	k1gFnPc4	přítomnost
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
přítomnost	přítomnost	k1gFnSc1	přítomnost
v	v	k7c6	v
lese	les	k1gInSc6	les
však	však	k9	však
často	často	k6eAd1	často
můžeme	moct	k5eAaImIp1nP	moct
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
díky	díky	k7c3	díky
jejich	jejich	k3xOp3gFnPc3	jejich
bahenním	bahenní	k2eAgFnPc3d1	bahenní
koupelím	koupel	k1gFnPc3	koupel
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
je	být	k5eAaImIp3nS	být
bahno	bahno	k1gNnSc4	bahno
v	v	k7c6	v
loužích	louž	k1gFnPc6	louž
rozválené	rozválený	k2eAgFnSc2d1	rozválená
na	na	k7c6	na
velké	velký	k2eAgFnSc6d1	velká
ploše	plocha	k1gFnSc6	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Žijí	žít	k5eAaImIp3nP	žít
obvykle	obvykle	k6eAd1	obvykle
ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
průměrně	průměrně	k6eAd1	průměrně
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
20	[number]	k4	20
<g/>
,	,	kIx,	,
vzácně	vzácně	k6eAd1	vzácně
až	až	k9	až
50	[number]	k4	50
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
staří	starý	k2eAgMnPc1d1	starý
samci	samec	k1gMnPc1	samec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
žijí	žít	k5eAaImIp3nP	žít
samotářsky	samotářsky	k6eAd1	samotářsky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jsou	být	k5eAaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
vyvinout	vyvinout	k5eAaPmF	vyvinout
rychlost	rychlost	k1gFnSc4	rychlost
48	[number]	k4	48
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
</s>
</p>
<p>
<s>
===	===	k?	===
Potrava	potrava	k1gFnSc1	potrava
===	===	k?	===
</s>
</p>
<p>
<s>
Prase	prase	k1gNnSc1	prase
divoké	divoký	k2eAgNnSc1d1	divoké
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgMnSc1d1	typický
všežravec	všežravec	k1gMnSc1	všežravec
<g/>
.	.	kIx.	.
</s>
<s>
Požírá	požírat	k5eAaImIp3nS	požírat
trávu	tráva	k1gFnSc4	tráva
<g/>
,	,	kIx,	,
hlízy	hlíza	k1gFnPc4	hlíza
<g/>
,	,	kIx,	,
ořechy	ořech	k1gInPc4	ořech
<g/>
,	,	kIx,	,
různé	různý	k2eAgFnPc4d1	různá
bobule	bobule	k1gFnPc4	bobule
<g/>
,	,	kIx,	,
kořeny	kořen	k1gInPc4	kořen
<g/>
,	,	kIx,	,
odpadky	odpadek	k1gInPc4	odpadek
<g/>
,	,	kIx,	,
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
malé	malý	k2eAgInPc4d1	malý
plazy	plaz	k1gInPc4	plaz
a	a	k8xC	a
mršiny	mršina	k1gFnPc4	mršina
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
pole	pole	k1gNnSc4	pole
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
kukuřičná	kukuřičný	k2eAgFnSc1d1	kukuřičná
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dokáže	dokázat	k5eAaPmIp3nS	dokázat
napáchat	napáchat	k5eAaBmF	napáchat
značné	značný	k2eAgFnPc4d1	značná
škody	škoda	k1gFnPc4	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
také	také	k9	také
na	na	k7c6	na
smetištích	smetiště	k1gNnPc6	smetiště
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
v	v	k7c6	v
obcích	obec	k1gFnPc6	obec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
hledá	hledat	k5eAaImIp3nS	hledat
potravu	potrava	k1gFnSc4	potrava
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
popelnic	popelnice	k1gFnPc2	popelnice
a	a	k8xC	a
kontejnerů	kontejner	k1gInPc2	kontejner
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgNnP	být
pozorována	pozorován	k2eAgNnPc1d1	pozorováno
divoká	divoký	k2eAgNnPc1d1	divoké
prasata	prase	k1gNnPc1	prase
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
převracejí	převracet	k5eAaImIp3nP	převracet
popelnice	popelnice	k1gFnPc4	popelnice
a	a	k8xC	a
valí	valit	k5eAaImIp3nP	valit
je	on	k3xPp3gNnSc4	on
před	před	k7c7	před
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
vysypala	vysypat	k5eAaPmAgFnS	vysypat
odpadky	odpadek	k1gInPc4	odpadek
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
všežravost	všežravost	k1gFnSc1	všežravost
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
předpokladů	předpoklad	k1gInPc2	předpoklad
úspěšného	úspěšný	k2eAgNnSc2d1	úspěšné
šíření	šíření	k1gNnSc2	šíření
divokých	divoký	k2eAgNnPc2d1	divoké
prasat	prase	k1gNnPc2	prase
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Predátoři	predátor	k1gMnPc5	predátor
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
dospělá	dospělý	k2eAgNnPc4d1	dospělé
prasata	prase	k1gNnPc4	prase
představuje	představovat	k5eAaImIp3nS	představovat
největší	veliký	k2eAgNnSc4d3	veliký
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
zejména	zejména	k9	zejména
vlk	vlk	k1gMnSc1	vlk
nebo	nebo	k8xC	nebo
medvěd	medvěd	k1gMnSc1	medvěd
<g/>
,	,	kIx,	,
selata	sele	k1gNnPc1	sele
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
zase	zase	k9	zase
stát	stát	k5eAaPmF	stát
relativně	relativně	k6eAd1	relativně
snadnou	snadný	k2eAgFnSc7d1	snadná
kořistí	kořist	k1gFnSc7	kořist
rysů	rys	k1gInPc2	rys
<g/>
,	,	kIx,	,
lišek	liška	k1gFnPc2	liška
<g/>
,	,	kIx,	,
koček	kočka	k1gFnPc2	kočka
divokých	divoký	k2eAgMnPc2d1	divoký
nebo	nebo	k8xC	nebo
různých	různý	k2eAgMnPc2d1	různý
dravců	dravec	k1gMnPc2	dravec
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
výrů	výr	k1gMnPc2	výr
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
úbytek	úbytek	k1gInSc1	úbytek
predátorů	predátor	k1gMnPc2	predátor
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
oblastí	oblast	k1gFnPc2	oblast
hlavní	hlavní	k2eAgFnSc7d1	hlavní
příčinou	příčina	k1gFnSc7	příčina
jejich	jejich	k3xOp3gNnSc2	jejich
rychlého	rychlý	k2eAgNnSc2d1	rychlé
přemnožení	přemnožení	k1gNnSc2	přemnožení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
tvoří	tvořit	k5eAaImIp3nS	tvořit
prasata	prase	k1gNnPc1	prase
divoká	divoký	k2eAgFnSc1d1	divoká
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
složek	složka	k1gFnPc2	složka
potravy	potrava	k1gFnSc2	potrava
tygra	tygr	k1gMnSc4	tygr
i	i	k9	i
větších	veliký	k2eAgMnPc2d2	veliký
jedinců	jedinec	k1gMnPc2	jedinec
varana	varan	k1gMnSc2	varan
komodského	komodský	k2eAgMnSc2d1	komodský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
===	===	k?	===
</s>
</p>
<p>
<s>
Samice	samice	k1gFnPc1	samice
(	(	kIx(	(
<g/>
bachyně	bachyně	k1gFnSc1	bachyně
<g/>
)	)	kIx)	)
prasat	prase	k1gNnPc2	prase
divokých	divoký	k2eAgMnPc2d1	divoký
pohlavně	pohlavně	k6eAd1	pohlavně
dospívají	dospívat	k5eAaImIp3nP	dospívat
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
až	až	k9	až
10	[number]	k4	10
<g/>
.	.	kIx.	.
měsíci	měsíc	k1gInSc3	měsíc
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
samci	samec	k1gMnPc1	samec
přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
2	[number]	k4	2
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
páření	páření	k1gNnSc2	páření
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
klimatickými	klimatický	k2eAgFnPc7d1	klimatická
podmínkami	podmínka	k1gFnPc7	podmínka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
však	však	k9	však
většinou	většinou	k6eAd1	většinou
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
do	do	k7c2	do
ledna	leden	k1gInSc2	leden
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
je	být	k5eAaImIp3nS	být
březí	březí	k2eAgFnSc1d1	březí
114	[number]	k4	114
<g/>
–	–	k?	–
<g/>
118	[number]	k4	118
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
vychází	vycházet	k5eAaImIp3nS	vycházet
většina	většina	k1gFnSc1	většina
vrhů	vrh	k1gInPc2	vrh
na	na	k7c4	na
období	období	k1gNnSc4	období
od	od	k7c2	od
března	březen	k1gInSc2	březen
do	do	k7c2	do
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
mláďat	mládě	k1gNnPc2	mládě
ve	v	k7c6	v
vrhu	vrh	k1gInSc6	vrh
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
3	[number]	k4	3
do	do	k7c2	do
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
vrhem	vrh	k1gInSc7	vrh
se	se	k3xPyFc4	se
samice	samice	k1gFnSc1	samice
odpojuje	odpojovat	k5eAaImIp3nS	odpojovat
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
připojuje	připojovat	k5eAaImIp3nS	připojovat
až	až	k9	až
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
již	již	k9	již
selata	sele	k1gNnPc1	sele
odrostlá	odrostlý	k2eAgNnPc1d1	odrostlé
<g/>
.	.	kIx.	.
</s>
<s>
Selata	sele	k1gNnPc1	sele
neboli	neboli	k8xC	neboli
markazíni	markazín	k1gMnPc1	markazín
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
podélně	podélně	k6eAd1	podélně
pruhovaným	pruhovaný	k2eAgNnSc7d1	pruhované
ochranným	ochranný	k2eAgNnSc7d1	ochranné
zbarvením	zbarvení	k1gNnSc7	zbarvení
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ztrácejí	ztrácet	k5eAaImIp3nP	ztrácet
koncem	koncem	k7c2	koncem
prvního	první	k4xOgInSc2	první
roku	rok	k1gInSc2	rok
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
péče	péče	k1gFnSc2	péče
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
velmi	velmi	k6eAd1	velmi
ostražitá	ostražitý	k2eAgFnSc1d1	ostražitá
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
bezprostředního	bezprostřední	k2eAgNnSc2d1	bezprostřední
ohrožení	ohrožení	k1gNnSc2	ohrožení
může	moct	k5eAaImIp3nS	moct
potenciálního	potenciální	k2eAgMnSc4d1	potenciální
predátora	predátor	k1gMnSc4	predátor
napadnout	napadnout	k5eAaPmF	napadnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Prase	prase	k1gNnSc1	prase
divoké	divoký	k2eAgNnSc1d1	divoké
a	a	k8xC	a
člověk	člověk	k1gMnSc1	člověk
==	==	k?	==
</s>
</p>
<p>
<s>
Prase	prase	k1gNnSc4	prase
divoké	divoký	k2eAgFnSc2d1	divoká
se	se	k3xPyFc4	se
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
kultuře	kultura	k1gFnSc6	kultura
objevuje	objevovat	k5eAaImIp3nS	objevovat
již	již	k6eAd1	již
od	od	k7c2	od
pravěku	pravěk	k1gInSc2	pravěk
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
jako	jako	k8xC	jako
významný	významný	k2eAgInSc1d1	významný
zdroj	zdroj	k1gInSc1	zdroj
masa	maso	k1gNnSc2	maso
a	a	k8xC	a
tuku	tuk	k1gInSc2	tuk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
jako	jako	k9	jako
symbol	symbol	k1gInSc4	symbol
plodivé	plodivý	k2eAgFnSc2d1	plodivá
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
mužnosti	mužnost	k1gFnSc2	mužnost
a	a	k8xC	a
odvahy	odvaha	k1gFnSc2	odvaha
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
na	na	k7c6	na
paleolitických	paleolitický	k2eAgFnPc6d1	paleolitická
jeskyních	jeskyně	k1gFnPc6	jeskyně
malbách	malba	k1gFnPc6	malba
ze	z	k7c2	z
španělské	španělský	k2eAgFnSc2d1	španělská
Altamiry	Altamira	k1gFnSc2	Altamira
můžeme	moct	k5eAaImIp1nP	moct
spatřit	spatřit	k5eAaPmF	spatřit
kance	kanec	k1gMnSc4	kanec
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
lov	lov	k1gInSc1	lov
<g/>
.	.	kIx.	.
</s>
<s>
Lov	lov	k1gInSc1	lov
na	na	k7c4	na
kance	kanec	k1gMnPc4	kanec
nebyl	být	k5eNaImAgMnS	být
pro	pro	k7c4	pro
pravěké	pravěký	k2eAgMnPc4d1	pravěký
a	a	k8xC	a
starověké	starověký	k2eAgMnPc4d1	starověký
lovce	lovec	k1gMnPc4	lovec
jen	jen	k6eAd1	jen
cestou	cesta	k1gFnSc7	cesta
k	k	k7c3	k
získávání	získávání	k1gNnSc3	získávání
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
důkazem	důkaz	k1gInSc7	důkaz
mužnosti	mužnost	k1gFnSc2	mužnost
a	a	k8xC	a
odvahy	odvaha	k1gFnSc2	odvaha
<g/>
.	.	kIx.	.
</s>
<s>
Soubojem	souboj	k1gInSc7	souboj
s	s	k7c7	s
nebezpečným	bezpečný	k2eNgMnSc7d1	nebezpečný
kancem	kanec	k1gMnSc7	kanec
proto	proto	k8xC	proto
později	pozdě	k6eAd2	pozdě
prokázali	prokázat	k5eAaPmAgMnP	prokázat
svou	svůj	k3xOyFgFnSc4	svůj
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
statečnost	statečnost	k1gFnSc4	statečnost
mnozí	mnohý	k2eAgMnPc1d1	mnohý
hrdinové	hrdina	k1gMnPc1	hrdina
starověkých	starověký	k2eAgInPc2d1	starověký
mýtů	mýtus	k1gInPc2	mýtus
<g/>
,	,	kIx,	,
eposů	epos	k1gInPc2	epos
a	a	k8xC	a
pověstí	pověst	k1gFnPc2	pověst
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
hrdina	hrdina	k1gMnSc1	hrdina
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
Héraklés	Héraklés	k1gInSc1	Héraklés
měl	mít	k5eAaImAgInS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
chytit	chytit	k5eAaPmF	chytit
kance	kanec	k1gMnPc4	kanec
z	z	k7c2	z
hory	hora	k1gFnSc2	hora
Erymanthu	Erymanth	k1gInSc2	Erymanth
a	a	k8xC	a
přinesl	přinést	k5eAaPmAgInS	přinést
ho	on	k3xPp3gMnSc4	on
živého	živý	k1gMnSc4	živý
do	do	k7c2	do
Mykén	Mykény	k1gFnPc2	Mykény
<g/>
,	,	kIx,	,
Théseus	Théseus	k1gMnSc1	Théseus
zabil	zabít	k5eAaPmAgMnS	zabít
cestou	cesta	k1gFnSc7	cesta
do	do	k7c2	do
Athén	Athéna	k1gFnPc2	Athéna
divokou	divoký	k2eAgFnSc4d1	divoká
svini	svině	k1gFnSc4	svině
z	z	k7c2	z
Krommyónu	Krommyón	k1gInSc2	Krommyón
a	a	k8xC	a
princ	princ	k1gMnSc1	princ
Meleagros	Meleagrosa	k1gFnPc2	Meleagrosa
s	s	k7c7	s
lovkyní	lovkyně	k1gFnSc7	lovkyně
Atalanté	Atalanta	k1gMnPc1	Atalanta
usmrtili	usmrtit	k5eAaPmAgMnP	usmrtit
staršlivého	staršlivý	k2eAgMnSc4d1	staršlivý
kance	kanec	k1gMnSc4	kanec
z	z	k7c2	z
Kalydónu	Kalydón	k1gInSc2	Kalydón
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
zasvěcen	zasvětit	k5eAaPmNgInS	zasvětit
bohyni	bohyně	k1gFnSc6	bohyně
Artemidě	Artemida	k1gFnSc6	Artemida
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
českých	český	k2eAgFnPc2d1	Česká
pověstí	pověst	k1gFnPc2	pověst
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
Bivojovi	Bivoj	k1gMnSc6	Bivoj
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
chytil	chytit	k5eAaPmAgMnS	chytit
kance	kanec	k1gMnPc4	kanec
z	z	k7c2	z
Kavčích	kavčí	k2eAgFnPc2d1	kavčí
hor	hora	k1gFnPc2	hora
a	a	k8xC	a
přinesl	přinést	k5eAaPmAgInS	přinést
ho	on	k3xPp3gMnSc4	on
kněžně	kněžna	k1gFnSc6	kněžna
Kazi	Kazi	k1gFnSc4	Kazi
na	na	k7c4	na
Vyšehrad	Vyšehrad	k1gInSc4	Vyšehrad
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
všichni	všechen	k3xTgMnPc1	všechen
hrdinové	hrdina	k1gMnPc1	hrdina
starověkých	starověký	k2eAgInPc2d1	starověký
mýtů	mýtus	k1gInPc2	mýtus
však	však	k9	však
v	v	k7c6	v
boji	boj	k1gInSc6	boj
s	s	k7c7	s
kancem	kanec	k1gMnSc7	kanec
uspěli	uspět	k5eAaPmAgMnP	uspět
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
milenec	milenec	k1gMnSc1	milenec
bohyně	bohyně	k1gFnSc2	bohyně
Afrodíty	Afrodíta	k1gFnSc2	Afrodíta
jménem	jméno	k1gNnSc7	jméno
Adónis	Adónis	k1gMnSc1	Adónis
nebo	nebo	k8xC	nebo
keltský	keltský	k2eAgMnSc1d1	keltský
hrdina	hrdina	k1gMnSc1	hrdina
Diarmuid	Diarmuida	k1gFnPc2	Diarmuida
v	v	k7c6	v
souboji	souboj	k1gInSc6	souboj
s	s	k7c7	s
kancem	kanec	k1gMnSc7	kanec
přišli	přijít	k5eAaPmAgMnP	přijít
o	o	k7c4	o
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
mytologii	mytologie	k1gFnSc6	mytologie
kanec	kanec	k1gMnSc1	kanec
také	také	k9	také
často	často	k6eAd1	často
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
bohy	bůh	k1gMnPc4	bůh
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
galskou	galský	k2eAgFnSc4d1	galská
bohyni	bohyně	k1gFnSc4	bohyně
Arduinnu	Arduinn	k1gInSc2	Arduinn
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
na	na	k7c6	na
kanci	kanec	k1gMnSc6	kanec
jezdila	jezdit	k5eAaImAgFnS	jezdit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
římského	římský	k2eAgMnSc4d1	římský
Jupitera	Jupiter	k1gMnSc4	Jupiter
či	či	k8xC	či
řeckou	řecký	k2eAgFnSc4d1	řecká
Artemidu	Artemida	k1gFnSc4	Artemida
<g/>
.	.	kIx.	.
vikinského	vikinský	k2eAgMnSc2d1	vikinský
boha	bůh	k1gMnSc2	bůh
Freye	Frey	k1gMnSc2	Frey
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
jako	jako	k9	jako
jízdní	jízdní	k2eAgNnSc1d1	jízdní
zvíře	zvíře	k1gNnSc1	zvíře
kanec	kanec	k1gMnSc1	kanec
Gullinbursti	Gullinburst	k1gFnSc3	Gullinburst
se	s	k7c7	s
zlatými	zlatý	k2eAgFnPc7d1	zlatá
štětinami	štětina	k1gFnPc7	štětina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
keltské	keltský	k2eAgFnSc6d1	keltská
mytologii	mytologie	k1gFnSc6	mytologie
zase	zase	k9	zase
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
teriantropicky	teriantropicky	k6eAd1	teriantropicky
proměňovali	proměňovat	k5eAaImAgMnP	proměňovat
v	v	k7c4	v
kance	kanec	k1gMnPc4	kanec
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
irský	irský	k2eAgMnSc1d1	irský
legendární	legendární	k2eAgMnSc1d1	legendární
hrdina	hrdina	k1gMnSc1	hrdina
Cian	Cian	k1gMnSc1	Cian
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
podobě	podoba	k1gFnSc6	podoba
ubit	ubit	k2eAgInSc4d1	ubit
syny	syn	k1gMnPc7	syn
svého	svůj	k1gMnSc2	svůj
protivníka	protivník	k1gMnSc2	protivník
Tuireana	Tuirean	k1gMnSc2	Tuirean
<g/>
.	.	kIx.	.
</s>
<s>
Keltové	Kelt	k1gMnPc1	Kelt
i	i	k8xC	i
Germáni	Germán	k1gMnPc1	Germán
však	však	k9	však
navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgMnSc3	ten
s	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
jedli	jíst	k5eAaImAgMnP	jíst
kančí	kančí	k2eAgNnSc4d1	kančí
maso	maso	k1gNnSc4	maso
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
populárním	populární	k2eAgMnSc6d1	populární
způsobem	způsob	k1gInSc7	způsob
zpracováno	zpracován	k2eAgNnSc1d1	zpracováno
v	v	k7c6	v
komiksech	komiks	k1gInPc6	komiks
a	a	k8xC	a
animovaných	animovaný	k2eAgInPc6d1	animovaný
filmech	film	k1gInPc6	film
o	o	k7c6	o
Asterixovi	Asterixa	k1gMnSc6	Asterixa
a	a	k8xC	a
Obelixovi	Obelixa	k1gMnSc6	Obelixa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kanec	kanec	k1gMnSc1	kanec
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gFnSc1	jeho
hlava	hlava	k1gFnSc1	hlava
vždy	vždy	k6eAd1	vždy
byl	být	k5eAaImAgInS	být
i	i	k9	i
častým	častý	k2eAgInSc7d1	častý
symbolem	symbol	k1gInSc7	symbol
bojovníků	bojovník	k1gMnPc2	bojovník
a	a	k8xC	a
vládců	vládce	k1gMnPc2	vládce
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
představitel	představitel	k1gMnSc1	představitel
plodnosti	plodnost	k1gFnSc2	plodnost
<g/>
,	,	kIx,	,
síly	síla	k1gFnSc2	síla
a	a	k8xC	a
statečnosti	statečnost	k1gFnSc2	statečnost
<g/>
.	.	kIx.	.
</s>
<s>
Kančími	kančí	k2eAgInPc7d1	kančí
kly	kel	k1gInPc7	kel
zastrčenými	zastrčený	k2eAgInPc7d1	zastrčený
v	v	k7c6	v
uších	ucho	k1gNnPc6	ucho
či	či	k8xC	či
nosních	nosní	k2eAgInPc6d1	nosní
otvorech	otvor	k1gInPc6	otvor
se	se	k3xPyFc4	se
odedávna	odedávna	k6eAd1	odedávna
zdobí	zdobit	k5eAaImIp3nP	zdobit
papuánští	papuánský	k2eAgMnPc1d1	papuánský
bojovníci	bojovník	k1gMnPc1	bojovník
z	z	k7c2	z
Nové	Nové	k2eAgFnSc2d1	Nové
Guiney	Guinea	k1gFnSc2	Guinea
<g/>
,	,	kIx,	,
Dajakové	Dajakový	k2eAgNnSc1d1	Dajakové
z	z	k7c2	z
Bornea	Borneo	k1gNnSc2	Borneo
nebo	nebo	k8xC	nebo
Nágové	Nágová	k1gFnSc2	Nágová
ze	z	k7c2	z
severovýchodní	severovýchodní	k2eAgFnSc2d1	severovýchodní
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
si	se	k3xPyFc3	se
také	také	k9	také
zdobí	zdobit	k5eAaImIp3nS	zdobit
kančími	kančí	k2eAgInPc7d1	kančí
kly	kel	k1gInPc7	kel
čepice	čepice	k1gFnSc2	čepice
nebo	nebo	k8xC	nebo
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
náramky	náramek	k1gInPc4	náramek
<g/>
.	.	kIx.	.
</s>
<s>
Obliba	obliba	k1gFnSc1	obliba
kančích	kančí	k2eAgInPc2d1	kančí
klů	kel	k1gInPc2	kel
se	se	k3xPyFc4	se
netýká	týkat	k5eNaImIp3nS	týkat
jen	jen	k9	jen
přírodních	přírodní	k2eAgInPc2d1	přírodní
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Homérské	homérský	k2eAgInPc1d1	homérský
eposy	epos	k1gInPc1	epos
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
Ilias	Ilias	k1gFnSc1	Ilias
<g/>
,	,	kIx,	,
i	i	k8xC	i
archeologické	archeologický	k2eAgInPc1d1	archeologický
nálezy	nález	k1gInPc1	nález
dokládají	dokládat	k5eAaImIp3nP	dokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
staří	starý	k2eAgMnPc1d1	starý
Řekové	Řek	k1gMnPc1	Řek
v	v	k7c6	v
mykénském	mykénský	k2eAgNnSc6d1	mykénské
období	období	k1gNnSc6	období
často	často	k6eAd1	často
zdobili	zdobit	k5eAaImAgMnP	zdobit
kančími	kančí	k2eAgInPc7d1	kančí
kly	kel	k1gInPc7	kel
přílby	přílba	k1gFnSc2	přílba
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
figurovalo	figurovat	k5eAaImAgNnS	figurovat
zobrazení	zobrazení	k1gNnSc1	zobrazení
kance	kanec	k1gMnSc2	kanec
na	na	k7c6	na
štítech	štít	k1gInPc6	štít
řeckých	řecký	k2eAgMnPc2d1	řecký
hoplítů	hoplíta	k1gMnPc2	hoplíta
i	i	k8xC	i
římských	římský	k2eAgMnPc2d1	římský
legionářů	legionář	k1gMnPc2	legionář
<g/>
.	.	kIx.	.
</s>
<s>
Kance	kanec	k1gMnPc4	kanec
si	se	k3xPyFc3	se
zvláště	zvláště	k6eAd1	zvláště
vážili	vážit	k5eAaImAgMnP	vážit
Galové	Galová	k1gFnPc4	Galová
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc1	jejich
korouhve	korouhev	k1gFnPc1	korouhev
byly	být	k5eAaImAgFnP	být
ukončeny	ukončit	k5eAaPmNgFnP	ukončit
malou	malý	k2eAgFnSc7d1	malá
figurkou	figurka	k1gFnSc7	figurka
kance	kanec	k1gMnSc2	kanec
a	a	k8xC	a
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
válečnické	válečnický	k2eAgFnPc1d1	válečnická
trumpety	trumpeta	k1gFnPc1	trumpeta
zvané	zvaný	k2eAgFnPc1d1	zvaná
karnyx	karnyx	k1gInSc4	karnyx
mívaly	mívat	k5eAaImAgInP	mívat
často	často	k6eAd1	často
tvar	tvar	k1gInSc4	tvar
kance	kanec	k1gMnSc2	kanec
s	s	k7c7	s
otevřenou	otevřený	k2eAgFnSc7d1	otevřená
tlamou	tlama	k1gFnSc7	tlama
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgFnPc1d1	Malé
sošky	soška	k1gFnPc1	soška
kance	kanec	k1gMnSc2	kanec
se	se	k3xPyFc4	se
na	na	k7c6	na
konci	konec	k1gInSc6	konec
starověku	starověk	k1gInSc2	starověk
objevují	objevovat	k5eAaImIp3nP	objevovat
na	na	k7c6	na
přílbách	přílba	k1gFnPc6	přílba
Anglosasů	Anglosas	k1gMnPc2	Anglosas
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
kanec	kanec	k1gMnSc1	kanec
či	či	k8xC	či
jeho	jeho	k3xOp3gFnSc1	jeho
hlava	hlava	k1gFnSc1	hlava
součástí	součást	k1gFnPc2	součást
šlechtických	šlechtický	k2eAgInPc2d1	šlechtický
erbů	erb	k1gInPc2	erb
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
i	i	k9	i
znaků	znak	k1gInPc2	znak
několika	několik	k4yIc3	několik
měst	město	k1gNnPc2	město
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
Belgii	Belgie	k1gFnSc6	Belgie
a	a	k8xC	a
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgMnSc1d1	anglický
král	král	k1gMnSc1	král
Richard	Richard	k1gMnSc1	Richard
III	III	kA	III
<g/>
.	.	kIx.	.
měl	mít	k5eAaImAgMnS	mít
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
erbu	erb	k1gInSc6	erb
dva	dva	k4xCgInPc4	dva
bílé	bílý	k1gMnPc4	bílý
kance	kanec	k1gMnSc4	kanec
jako	jako	k8xS	jako
štítonoše	štítonoš	k1gMnSc4	štítonoš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kanci	kanec	k1gMnPc1	kanec
hrají	hrát	k5eAaImIp3nP	hrát
velkou	velký	k2eAgFnSc4d1	velká
roli	role	k1gFnSc4	role
i	i	k8xC	i
v	v	k7c6	v
asijské	asijský	k2eAgFnSc3d1	asijská
mytologii	mytologie	k1gFnSc3	mytologie
<g/>
.	.	kIx.	.
hinduistický	hinduistický	k2eAgInSc4d1	hinduistický
Višnu	Višna	k1gFnSc4	Višna
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
třetím	třetí	k4xOgInSc6	třetí
avatáru	avatár	k1gInSc6	avatár
vzal	vzít	k5eAaPmAgMnS	vzít
podobu	podoba	k1gFnSc4	podoba
kance	kanec	k1gMnSc2	kanec
Várahy	Váraha	k1gFnSc2	Váraha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
přemohl	přemoct	k5eAaPmAgInS	přemoct
zlého	zlý	k1gMnSc4	zlý
obra	obr	k1gMnSc4	obr
Hirnajákšu	Hirnajákšu	k1gMnSc4	Hirnajákšu
a	a	k8xC	a
vysvobodil	vysvobodit	k5eAaPmAgMnS	vysvobodit
z	z	k7c2	z
jeho	jeho	k3xOp3gNnSc2	jeho
zajetí	zajetí	k1gNnSc2	zajetí
bohyni	bohyně	k1gFnSc4	bohyně
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mýtech	mýtus	k1gInPc6	mýtus
Kyrgyzů	Kyrgyz	k1gInPc2	Kyrgyz
a	a	k8xC	a
Burjatůse	Burjatůse	k1gFnSc1	Burjatůse
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
kancích	kanec	k1gMnPc6	kanec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zachránili	zachránit	k5eAaPmAgMnP	zachránit
legendární	legendární	k2eAgInPc4d1	legendární
předky	předek	k1gInPc4	předek
těchto	tento	k3xDgNnPc2	tento
etnik	etnikum	k1gNnPc2	etnikum
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
král	král	k1gMnSc1	král
Mandžuů	Mandžu	k1gMnPc2	Mandžu
měl	mít	k5eAaImAgMnS	mít
podle	podle	k7c2	podle
pověsti	pověst	k1gFnSc2	pověst
kančí	kančí	k2eAgFnSc4d1	kančí
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
Mandžuové	Mandžu	k1gMnPc1	Mandžu
proto	proto	k8xC	proto
nejedli	jíst	k5eNaImAgMnP	jíst
kančí	kančí	k2eAgNnSc4d1	kančí
maso	maso	k1gNnSc4	maso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Lov	lov	k1gInSc1	lov
===	===	k?	===
</s>
</p>
<p>
<s>
Černá	černý	k2eAgFnSc1d1	černá
neboli	neboli	k8xC	neboli
rytířská	rytířský	k2eAgFnSc1d1	rytířská
zvěř	zvěř	k1gFnSc1	zvěř
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
prase	prase	k1gNnSc1	prase
divoké	divoký	k2eAgFnSc2d1	divoká
nazývají	nazývat	k5eAaImIp3nP	nazývat
myslivci	myslivec	k1gMnPc1	myslivec
<g/>
,	,	kIx,	,
patřilo	patřit	k5eAaImAgNnS	patřit
odedávna	odedávna	k6eAd1	odedávna
k	k	k7c3	k
oblíbené	oblíbený	k2eAgFnSc3d1	oblíbená
lovné	lovný	k2eAgFnSc3d1	lovná
zvěři	zvěř	k1gFnSc3	zvěř
<g/>
.	.	kIx.	.
</s>
<s>
Doklady	doklad	k1gInPc4	doklad
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
lovu	lov	k1gInSc6	lov
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
jeskynních	jeskynní	k2eAgFnPc2d1	jeskynní
maleb	malba	k1gFnPc2	malba
i	i	k9	i
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
zbytků	zbytek	k1gInPc2	zbytek
kostí	kost	k1gFnPc2	kost
z	z	k7c2	z
odpadních	odpadní	k2eAgFnPc2d1	odpadní
jam	jáma	k1gFnPc2	jáma
<g/>
,	,	kIx,	,
známe	znát	k5eAaImIp1nP	znát
již	již	k6eAd1	již
z	z	k7c2	z
paleolitu	paleolit	k1gInSc2	paleolit
<g/>
.	.	kIx.	.
</s>
<s>
Kanec	kanec	k1gMnSc1	kanec
byl	být	k5eAaImAgMnS	být
hojně	hojně	k6eAd1	hojně
loven	lovit	k5eAaImNgMnS	lovit
v	v	k7c6	v
antickém	antický	k2eAgNnSc6d1	antické
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yRnSc6	což
svědčí	svědčit	k5eAaImIp3nP	svědčit
vázové	vázový	k2eAgFnPc1d1	vázový
kresby	kresba	k1gFnPc1	kresba
<g/>
,	,	kIx,	,
mýty	mýtus	k1gInPc1	mýtus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
Xenofónův	Xenofónův	k2eAgInSc4d1	Xenofónův
spis	spis	k1gInSc4	spis
Kynagetikos	Kynagetikos	k1gMnSc1	Kynagetikos
(	(	kIx(	(
<g/>
Psovod	psovod	k1gMnSc1	psovod
či	či	k8xC	či
O	o	k7c6	o
lovu	lov	k1gInSc6	lov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
popisuje	popisovat	k5eAaImIp3nS	popisovat
hon	hon	k1gInSc1	hon
na	na	k7c4	na
kance	kanec	k1gMnPc4	kanec
se	se	k3xPyFc4	se
psy	pes	k1gMnPc7	pes
i	i	k8xC	i
jejich	jejich	k3xOp3gNnSc2	jejich
chytání	chytání	k1gNnSc2	chytání
do	do	k7c2	do
pasti	past	k1gFnSc2	past
zvané	zvaný	k2eAgFnPc1d1	zvaná
stupka	stupka	k1gFnSc1	stupka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgMnS	být
proutěný	proutěný	k2eAgInSc4d1	proutěný
věnec	věnec	k1gInSc4	věnec
s	s	k7c7	s
ostrými	ostrý	k2eAgInPc7d1	ostrý
<g/>
,	,	kIx,	,
dovnitř	dovnitř	k6eAd1	dovnitř
směťujícími	směťující	k2eAgInPc7d1	směťující
hroty	hrot	k1gInPc7	hrot
<g/>
,	,	kIx,	,
přivázaný	přivázaný	k2eAgInSc1d1	přivázaný
ke	k	k7c3	k
špalku	špalek	k1gInSc3	špalek
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
kanec	kanec	k1gMnSc1	kanec
do	do	k7c2	do
pasti	past	k1gFnSc2	past
šlápl	šlápnout	k5eAaPmAgInS	šlápnout
<g/>
,	,	kIx,	,
hroty	hrot	k1gInPc1	hrot
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zachytily	zachytit	k5eAaPmAgFnP	zachytit
na	na	k7c6	na
noze	noha	k1gFnSc6	noha
a	a	k8xC	a
špalek	špalek	k1gInSc4	špalek
mu	on	k3xPp3gMnSc3	on
bránil	bránit	k5eAaImAgMnS	bránit
v	v	k7c6	v
útěku	útěk	k1gInSc6	útěk
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
velmi	velmi	k6eAd1	velmi
nehumánní	humánní	k2eNgFnSc4d1	nehumánní
past	past	k1gFnSc4	past
dodnes	dodnes	k6eAd1	dodnes
uzívají	uzívat	k5eAaPmIp3nP	uzívat
lovci	lovec	k1gMnPc1	lovec
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
při	při	k7c6	při
chytání	chytání	k1gNnSc6	chytání
zeber	zebra	k1gFnPc2	zebra
<g/>
,	,	kIx,	,
antilop	antilopa	k1gFnPc2	antilopa
či	či	k8xC	či
prasat	prase	k1gNnPc2	prase
bradavičnatých	bradavičnatý	k2eAgMnPc2d1	bradavičnatý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
starém	starý	k2eAgInSc6d1	starý
Římě	Řím	k1gInSc6	Řím
bylo	být	k5eAaImAgNnS	být
kančí	kančí	k2eAgNnSc1d1	kančí
maso	maso	k1gNnSc1	maso
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
pokrmem	pokrm	k1gInSc7	pokrm
na	na	k7c6	na
hostinách	hostina	k1gFnPc6	hostina
<g/>
.	.	kIx.	.
</s>
<s>
Divoká	divoký	k2eAgNnPc4d1	divoké
prasata	prase	k1gNnPc4	prase
zde	zde	k6eAd1	zde
však	však	k9	však
byla	být	k5eAaImAgFnS	být
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
jiná	jiný	k2eAgNnPc1d1	jiné
divoká	divoký	k2eAgNnPc1d1	divoké
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
zneužívána	zneužíván	k2eAgFnSc1d1	zneužívána
při	při	k7c6	při
surových	surový	k2eAgFnPc6d1	surová
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
cirku	cirk	k1gInSc6	cirk
<g/>
.	.	kIx.	.
</s>
<s>
Kanci	kanec	k1gMnPc1	kanec
při	při	k7c6	při
nich	on	k3xPp3gInPc6	on
byli	být	k5eAaImAgMnP	být
zabíjeni	zabíjen	k2eAgMnPc1d1	zabíjen
venatiory	venatiora	k1gFnSc2	venatiora
<g/>
,	,	kIx,	,
štváni	štván	k2eAgMnPc1d1	štván
proti	proti	k7c3	proti
jiným	jiný	k2eAgNnPc3d1	jiné
zvířatům	zvíře	k1gNnPc3	zvíře
nebo	nebo	k8xC	nebo
i	i	k8xC	i
odsouzeným	odsouzený	k2eAgMnPc3d1	odsouzený
zločincům	zločinec	k1gMnPc3	zločinec
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
měli	mít	k5eAaImAgMnP	mít
roztrhat	roztrhat	k5eAaPmF	roztrhat
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
dokonce	dokonce	k9	dokonce
i	i	k9	i
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
divadelního	divadelní	k2eAgNnSc2d1	divadelní
představení	představení	k1gNnSc2	představení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lov	lov	k1gInSc1	lov
na	na	k7c4	na
černou	černý	k2eAgFnSc4d1	černá
a	a	k8xC	a
jelení	jelení	k2eAgFnSc4d1	jelení
zvěř	zvěř	k1gFnSc4	zvěř
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
jezdecké	jezdecký	k2eAgFnSc2d1	jezdecká
štvanice	štvanice	k1gFnSc2	štvanice
se	s	k7c7	s
smečkou	smečka	k1gFnSc7	smečka
psů	pes	k1gMnPc2	pes
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
zábavou	zábava	k1gFnSc7	zábava
šlechty	šlechta	k1gFnSc2	šlechta
v	v	k7c6	v
evropském	evropský	k2eAgInSc6d1	evropský
středověku	středověk	k1gInSc6	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Náruživě	náruživě	k6eAd1	náruživě
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
věnovali	věnovat	k5eAaPmAgMnP	věnovat
i	i	k9	i
čeští	český	k2eAgMnPc1d1	český
králové	král	k1gMnPc1	král
Václav	Václava	k1gFnPc2	Václava
I.	I.	kA	I.
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
a	a	k8xC	a
raném	raný	k2eAgInSc6d1	raný
novověku	novověk	k1gInSc6	novověk
byl	být	k5eAaImAgInS	být
lov	lov	k1gInSc1	lov
na	na	k7c4	na
kance	kanec	k1gMnPc4	kanec
vyhrazen	vyhrazen	k2eAgInSc4d1	vyhrazen
jen	jen	k8xS	jen
šlechtě	šlechta	k1gFnSc6	šlechta
<g/>
,	,	kIx,	,
poddaný	poddaný	k1gMnSc1	poddaný
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
by	by	kYmCp3nS	by
kance	kanec	k1gMnPc4	kanec
zabil	zabít	k5eAaPmAgInS	zabít
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
přísně	přísně	k6eAd1	přísně
potrestán	potrestat	k5eAaPmNgMnS	potrestat
jako	jako	k8xS	jako
pytlák	pytlák	k1gMnSc1	pytlák
<g/>
.	.	kIx.	.
</s>
<s>
Prasata	prase	k1gNnPc1	prase
však	však	k9	však
páchala	páchat	k5eAaImAgFnS	páchat
škody	škoda	k1gFnPc4	škoda
na	na	k7c6	na
polích	pole	k1gNnPc6	pole
poddaných	poddaný	k1gMnPc2	poddaný
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
je	on	k3xPp3gMnPc4	on
ilegálně	ilegálně	k6eAd1	ilegálně
zabíjeli	zabíjet	k5eAaImAgMnP	zabíjet
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
docházelo	docházet	k5eAaImAgNnS	docházet
ke	k	k7c3	k
konfliktům	konflikt	k1gInPc3	konflikt
mezi	mezi	k7c7	mezi
šlechtou	šlechta	k1gFnSc7	šlechta
a	a	k8xC	a
poddanými	poddaná	k1gFnPc7	poddaná
<g/>
.	.	kIx.	.
</s>
<s>
Lov	lov	k1gInSc1	lov
na	na	k7c4	na
kance	kanec	k1gMnSc4	kanec
byl	být	k5eAaImAgInS	být
nebezpečný	bezpečný	k2eNgInSc1d1	nebezpečný
<g/>
,	,	kIx,	,
výjimkou	výjimka	k1gFnSc7	výjimka
při	při	k7c6	při
něm	on	k3xPp3gNnSc6	on
nebylo	být	k5eNaImAgNnS	být
ani	ani	k8xC	ani
usmrcení	usmrcení	k1gNnSc1	usmrcení
lovce	lovec	k1gMnSc2	lovec
poraněným	poraněný	k2eAgNnSc7d1	poraněné
zvířetem	zvíře	k1gNnSc7	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
zahynul	zahynout	k5eAaPmAgMnS	zahynout
například	například	k6eAd1	například
princ	princ	k1gMnSc1	princ
Imrich	Imrich	k1gMnSc1	Imrich
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
uherského	uherský	k2eAgMnSc2d1	uherský
krále	král	k1gMnSc2	král
Štěpána	Štěpán	k1gMnSc2	Štěpán
I.	I.	kA	I.
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
chorvatský	chorvatský	k2eAgMnSc1d1	chorvatský
šlechtic	šlechtic	k1gMnSc1	šlechtic
Nikola	Nikola	k1gMnSc1	Nikola
Zrinski	Zrinski	k1gNnPc2	Zrinski
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
renesance	renesance	k1gFnSc2	renesance
a	a	k8xC	a
baroka	baroko	k1gNnSc2	baroko
byl	být	k5eAaImAgInS	být
oblíben	oblíbit	k5eAaPmNgInS	oblíbit
hlavně	hlavně	k9	hlavně
lov	lov	k1gInSc1	lov
v	v	k7c6	v
leči	leč	k1gFnSc6	leč
obestavěné	obestavěný	k2eAgFnSc6d1	obestavěná
plátny	plátno	k1gNnPc7	plátno
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
těchto	tento	k3xDgInPc6	tento
lovech	lov	k1gInPc6	lov
byla	být	k5eAaImAgFnS	být
zvěř	zvěř	k1gFnSc1	zvěř
nahnána	nahnán	k2eAgFnSc1d1	nahnána
do	do	k7c2	do
velkých	velký	k2eAgFnPc2d1	velká
ohrad	ohrada	k1gFnPc2	ohrada
z	z	k7c2	z
pláten	plátno	k1gNnPc2	plátno
či	či	k8xC	či
sítí	síť	k1gFnPc2	síť
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
z	z	k7c2	z
altánů	altán	k1gInPc2	altán
stříleli	střílet	k5eAaImAgMnP	střílet
urození	urozený	k2eAgMnPc1d1	urozený
lovci	lovec	k1gMnPc1	lovec
nebo	nebo	k8xC	nebo
i	i	k8xC	i
dámy	dáma	k1gFnPc1	dáma
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
způsob	způsob	k1gInSc4	způsob
lovu	lov	k1gInSc2	lov
si	se	k3xPyFc3	se
oblíbil	oblíbit	k5eAaPmAgMnS	oblíbit
například	například	k6eAd1	například
císař	císař	k1gMnSc1	císař
Karel	Karel	k1gMnSc1	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
Alžběta	Alžběta	k1gFnSc1	Alžběta
Kristina	Kristina	k1gFnSc1	Kristina
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
však	však	k9	však
takto	takto	k6eAd1	takto
divočáky	divočák	k1gMnPc7	divočák
lovil	lovit	k5eAaImAgMnS	lovit
František	František	k1gMnSc1	František
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Este	Est	k1gMnSc5	Est
<g/>
.	.	kIx.	.
</s>
<s>
Jindy	jindy	k6eAd1	jindy
urození	urozený	k2eAgMnPc1d1	urozený
lovci	lovec	k1gMnPc1	lovec
nebo	nebo	k8xC	nebo
lovecký	lovecký	k2eAgInSc1d1	lovecký
personál	personál	k1gInSc1	personál
černou	černý	k2eAgFnSc4d1	černá
zvěř	zvěř	k1gFnSc4	zvěř
<g/>
,	,	kIx,	,
uzavřenou	uzavřený	k2eAgFnSc4d1	uzavřená
v	v	k7c6	v
ohradách	ohrada	k1gFnPc6	ohrada
z	z	k7c2	z
pláten	plátno	k1gNnPc2	plátno
<g/>
,	,	kIx,	,
zabíjeli	zabíjet	k5eAaImAgMnP	zabíjet
oštěpy	oštěp	k1gInPc4	oštěp
či	či	k8xC	či
zvláštními	zvláštní	k2eAgInPc7d1	zvláštní
kančími	kančí	k2eAgInPc7d1	kančí
meči	meč	k1gInPc7	meč
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
byla	být	k5eAaImAgFnS	být
surová	surový	k2eAgFnSc1d1	surová
lovecká	lovecký	k2eAgFnSc1d1	lovecká
zábava	zábava	k1gFnSc1	zábava
<g/>
,	,	kIx,	,
podobná	podobný	k2eAgFnSc1d1	podobná
spíše	spíše	k9	spíše
býčím	býčí	k2eAgMnPc3d1	býčí
zápasům	zápas	k1gInPc3	zápas
<g/>
,	,	kIx,	,
než	než	k8xS	než
skutečné	skutečný	k2eAgFnPc1d1	skutečná
myslivosti	myslivost	k1gFnPc1	myslivost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
viktoriánském	viktoriánský	k2eAgNnSc6d1	viktoriánské
období	období	k1gNnSc6	období
byl	být	k5eAaImAgInS	být
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
sportem	sport	k1gInSc7	sport
britských	britský	k2eAgMnPc2d1	britský
koloniálních	koloniální	k2eAgMnPc2d1	koloniální
důstojníků	důstojník	k1gMnPc2	důstojník
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
tzv.	tzv.	kA	tzv.
pig	pig	k?	pig
sticking	sticking	k1gInSc1	sticking
neboli	neboli	k8xC	neboli
jezdecká	jezdecký	k2eAgFnSc1d1	jezdecká
štvanice	štvanice	k1gFnSc1	štvanice
s	s	k7c7	s
oštěpy	oštěp	k1gInPc7	oštěp
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
plném	plný	k2eAgInSc6d1	plný
trysku	trysk	k1gInSc6	trysk
dohonit	dohonit	k5eAaPmF	dohonit
klikatě	klikatě	k6eAd1	klikatě
běžící	běžící	k2eAgNnSc4d1	běžící
prase	prase	k1gNnSc4	prase
a	a	k8xC	a
skolit	skolit	k5eAaImF	skolit
ho	on	k3xPp3gNnSc2	on
oštěpem	oštěp	k1gInSc7	oštěp
<g/>
.	.	kIx.	.
</s>
<s>
Důstojník	důstojník	k1gMnSc1	důstojník
Michael	Michael	k1gMnSc1	Michael
Rosenthal	Rosenthal	k1gMnSc1	Rosenthal
<g/>
,	,	kIx,	,
kritizovaný	kritizovaný	k2eAgMnSc1d1	kritizovaný
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vášnivě	vášnivě	k6eAd1	vášnivě
oddával	oddávat	k5eAaImAgMnS	oddávat
tomuto	tento	k3xDgInSc3	tento
surovému	surový	k2eAgInSc3d1	surový
sportu	sport	k1gInSc3	sport
<g/>
,	,	kIx,	,
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
a	a	k8xC	a
nejvíce	hodně	k6eAd3	hodně
vzrušující	vzrušující	k2eAgFnSc1d1	vzrušující
zábava	zábava	k1gFnSc1	zábava
jak	jak	k8xC	jak
pro	pro	k7c4	pro
jezdce	jezdec	k1gMnSc4	jezdec
<g/>
,	,	kIx,	,
tak	tak	k9	tak
pro	pro	k7c4	pro
jeho	on	k3xPp3gMnSc4	on
koně	kůň	k1gMnSc4	kůň
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
baví	bavit	k5eAaImIp3nS	bavit
i	i	k9	i
to	ten	k3xDgNnSc1	ten
prase	prase	k1gNnSc1	prase
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Tento	tento	k3xDgInSc1	tento
sport	sport	k1gInSc1	sport
byl	být	k5eAaImAgInS	být
Angličany	Angličan	k1gMnPc4	Angličan
zaveden	zavést	k5eAaPmNgInS	zavést
i	i	k9	i
ve	v	k7c6	v
Východní	východní	k2eAgFnSc6d1	východní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
loveným	lovený	k2eAgInPc3d1	lovený
druhem	druh	k1gInSc7	druh
prase	prase	k1gNnSc1	prase
bradavičnaté	bradavičnatý	k2eAgNnSc1d1	bradavičnaté
<g/>
.	.	kIx.	.
</s>
<s>
Známým	známý	k2eAgMnSc7d1	známý
milovníkem	milovník	k1gMnSc7	milovník
pig-stickingu	pigticking	k1gInSc2	pig-sticking
byl	být	k5eAaImAgMnS	být
zakladatel	zakladatel	k1gMnSc1	zakladatel
skautingu	skauting	k1gInSc2	skauting
Robert	Robert	k1gMnSc1	Robert
Baden-Powell	Baden-Powell	k1gMnSc1	Baden-Powell
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
prase	prase	k1gNnSc1	prase
divoké	divoký	k2eAgNnSc1d1	divoké
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
druhý	druhý	k4xOgInSc4	druhý
nejvíce	hodně	k6eAd3	hodně
lovený	lovený	k2eAgInSc4d1	lovený
druh	druh	k1gInSc4	druh
srstnaté	srstnatý	k2eAgFnSc2d1	srstnatá
zvěře	zvěř	k1gFnSc2	zvěř
<g/>
,	,	kIx,	,
roční	roční	k2eAgInSc1d1	roční
odstřel	odstřel	k1gInSc1	odstřel
činil	činit	k5eAaImAgInS	činit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
rekordních	rekordní	k2eAgInPc2d1	rekordní
185	[number]	k4	185
496	[number]	k4	496
jedinců	jedinec	k1gMnPc2	jedinec
a	a	k8xC	a
každoročně	každoročně	k6eAd1	každoročně
mírně	mírně	k6eAd1	mírně
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
Loví	lovit	k5eAaImIp3nS	lovit
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
na	na	k7c6	na
čekané	čekaná	k1gFnSc6	čekaná
nebo	nebo	k8xC	nebo
při	při	k7c6	při
společných	společný	k2eAgFnPc6d1	společná
naháňkách	naháňka	k1gFnPc6	naháňka
<g/>
,	,	kIx,	,
zřídka	zřídka	k6eAd1	zřídka
vábením	vábení	k1gNnSc7	vábení
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
selata	sele	k1gNnPc1	sele
lze	lze	k6eAd1	lze
přivábit	přivábit	k5eAaPmF	přivábit
napodobením	napodobení	k1gNnSc7	napodobení
chrochtání	chrochtání	k1gNnSc4	chrochtání
bachyně	bachyně	k1gFnSc1	bachyně
pomocí	pomocí	k7c2	pomocí
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
rourovité	rourovitý	k2eAgFnSc2d1	rourovitá
vábničky	vábnička	k1gFnSc2	vábnička
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
lovu	lov	k1gInSc6	lov
černé	černý	k2eAgFnSc2d1	černá
zvěře	zvěř	k1gFnSc2	zvěř
dělají	dělat	k5eAaImIp3nP	dělat
dobré	dobrý	k2eAgFnPc4d1	dobrá
služby	služba	k1gFnPc4	služba
lovečtí	lovecký	k2eAgMnPc1d1	lovecký
psi	pes	k1gMnPc1	pes
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
border	border	k1gMnSc1	border
teriér	teriér	k1gMnSc1	teriér
<g/>
,	,	kIx,	,
bulteriér	bulteriér	k1gMnSc1	bulteriér
<g/>
,	,	kIx,	,
slovenský	slovenský	k2eAgInSc1d1	slovenský
kopov	kopov	k1gInSc1	kopov
ale	ale	k8xC	ale
i	i	k9	i
jezevčíci	jezevčík	k1gMnPc1	jezevčík
<g/>
.	.	kIx.	.
</s>
<s>
Poraněný	poraněný	k2eAgMnSc1d1	poraněný
kňour	kňour	k1gMnSc1	kňour
nebo	nebo	k8xC	nebo
vodící	vodící	k2eAgFnSc1d1	vodící
bachyně	bachyně	k1gFnSc1	bachyně
nezřídka	nezřídka	k6eAd1	nezřídka
útočí	útočit	k5eAaImIp3nS	útočit
i	i	k9	i
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
lovce	lovec	k1gMnPc4	lovec
nebezpečně	bezpečně	k6eNd1	bezpečně
zranit	zranit	k5eAaPmF	zranit
svými	svůj	k3xOyFgInPc7	svůj
špičáky	špičák	k1gInPc7	špičák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemědělských	zemědělský	k2eAgFnPc6d1	zemědělská
oblastech	oblast	k1gFnPc6	oblast
působí	působit	k5eAaImIp3nP	působit
divoká	divoký	k2eAgNnPc4d1	divoké
prasata	prase	k1gNnPc4	prase
značné	značný	k2eAgFnPc4d1	značná
škody	škoda	k1gFnPc4	škoda
na	na	k7c6	na
polích	pole	k1gNnPc6	pole
<g/>
,	,	kIx,	,
s	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
žerou	žrát	k5eAaImIp3nP	žrát
mladou	mladý	k2eAgFnSc4d1	mladá
kukuřici	kukuřice	k1gFnSc4	kukuřice
<g/>
,	,	kIx,	,
brambory	brambor	k1gInPc4	brambor
<g/>
,	,	kIx,	,
dýně	dýně	k1gFnPc4	dýně
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
plodiny	plodina	k1gFnPc4	plodina
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
dospělá	dospělý	k2eAgNnPc4d1	dospělé
prasata	prase	k1gNnPc4	prase
je	být	k5eAaImIp3nS	být
povoleno	povolen	k2eAgNnSc1d1	povoleno
lovit	lovit	k5eAaImF	lovit
pouze	pouze	k6eAd1	pouze
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
lov	lov	k1gInSc4	lov
selat	sele	k1gNnPc2	sele
a	a	k8xC	a
lončáků	lončák	k1gMnPc2	lončák
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
provádět	provádět	k5eAaImF	provádět
celoročně	celoročně	k6eAd1	celoročně
<g/>
.	.	kIx.	.
</s>
<s>
Živá	živý	k2eAgFnSc1d1	živá
černá	černý	k2eAgFnSc1d1	černá
zvěř	zvěř	k1gFnSc1	zvěř
se	se	k3xPyFc4	se
odchytává	odchytávat	k5eAaImIp3nS	odchytávat
pomocí	pomocí	k7c2	pomocí
lapáků	lapák	k1gInPc2	lapák
<g/>
,	,	kIx,	,
konstruovaných	konstruovaný	k2eAgInPc2d1	konstruovaný
na	na	k7c6	na
principu	princip	k1gInSc6	princip
sklopce	sklopec	k1gInSc2	sklopec
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
klec	klec	k1gFnSc4	klec
s	s	k7c7	s
padacími	padací	k2eAgNnPc7d1	padací
dvířky	dvířka	k1gNnPc7	dvířka
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
prase	prase	k1gNnSc1	prase
vstoupí	vstoupit	k5eAaPmIp3nS	vstoupit
do	do	k7c2	do
klece	klec	k1gFnSc2	klec
<g/>
,	,	kIx,	,
dvířka	dvířka	k1gNnPc4	dvířka
za	za	k7c7	za
ním	on	k3xPp3gInSc7	on
zapadnou	zapadnout	k5eAaPmIp3nP	zapadnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Myslivci	Myslivec	k1gMnPc1	Myslivec
označují	označovat	k5eAaImIp3nP	označovat
samce	samec	k1gMnPc4	samec
prasete	prase	k1gNnSc2	prase
divokého	divoký	k2eAgInSc2d1	divoký
slovem	slovo	k1gNnSc7	slovo
kňour	kňoura	k1gFnPc2	kňoura
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
je	být	k5eAaImIp3nS	být
bachyně	bachyně	k1gFnPc4	bachyně
<g/>
.	.	kIx.	.
</s>
<s>
Mladá	mladý	k2eAgFnSc1d1	mladá
zvěř	zvěř	k1gFnSc1	zvěř
obojího	obojí	k4xRgMnSc2	obojí
pohlaví	pohlaví	k1gNnSc3	pohlaví
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
roce	rok	k1gInSc6	rok
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
jako	jako	k8xS	jako
markazíni	markazín	k1gMnPc1	markazín
<g/>
,	,	kIx,	,
letošáci	letošák	k1gMnPc1	letošák
či	či	k8xC	či
selata	sele	k1gNnPc1	sele
<g/>
,	,	kIx,	,
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
roce	rok	k1gInSc6	rok
života	život	k1gInSc2	život
jako	jako	k8xS	jako
lončáci	lončák	k1gMnPc1	lončák
<g/>
.	.	kIx.	.
</s>
<s>
Trofejí	trofej	k1gFnSc7	trofej
z	z	k7c2	z
prasete	prase	k1gNnSc2	prase
divokého	divoký	k2eAgNnSc2d1	divoké
je	být	k5eAaImIp3nS	být
kůže	kůže	k1gFnSc2	kůže
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
kly	kel	k1gInPc1	kel
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
myslivci	myslivec	k1gMnPc7	myslivec
označované	označovaný	k2eAgFnSc2d1	označovaná
jako	jako	k8xC	jako
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
delší	dlouhý	k2eAgInPc1d2	delší
spodní	spodní	k2eAgInPc1d1	spodní
páráky	párák	k1gInPc1	párák
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
kratší	krátký	k2eAgInPc1d2	kratší
horní	horní	k2eAgInPc1d1	horní
klektáky	klekták	k1gInPc1	klekták
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
rozzuřený	rozzuřený	k2eAgMnSc1d1	rozzuřený
kanec	kanec	k1gMnSc1	kanec
klektá	klektat	k5eAaImIp3nS	klektat
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
vydává	vydávat	k5eAaImIp3nS	vydávat
cvakavé	cvakavý	k2eAgInPc4d1	cvakavý
zvuky	zvuk	k1gInPc4	zvuk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kančí	kančí	k2eAgNnSc1d1	kančí
maso	maso	k1gNnSc1	maso
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
chutné	chutný	k2eAgNnSc1d1	chutné
<g/>
,	,	kIx,	,
chutí	chuť	k1gFnSc7	chuť
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
vepřovému	vepřové	k1gNnSc3	vepřové
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
méně	málo	k6eAd2	málo
tučné	tučný	k2eAgNnSc1d1	tučné
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
příchuť	příchuť	k1gFnSc4	příchuť
zvěřiny	zvěřina	k1gFnSc2	zvěřina
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
více	hodně	k6eAd2	hodně
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
štětin	štětina	k1gFnPc2	štětina
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
vyráběly	vyrábět	k5eAaImAgInP	vyrábět
štětce	štětec	k1gInPc1	štětec
a	a	k8xC	a
kartáče	kartáč	k1gInPc1	kartáč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Onemocnění	onemocnění	k1gNnSc1	onemocnění
==	==	k?	==
</s>
</p>
<p>
<s>
Divoká	divoký	k2eAgNnPc1d1	divoké
prasata	prase	k1gNnPc1	prase
mohou	moct	k5eAaImIp3nP	moct
býti	být	k5eAaImF	být
také	také	k9	také
nakažena	nakažen	k2eAgFnSc1d1	nakažena
tzv.	tzv.	kA	tzv.
trichinelózou	trichinelóza	k1gFnSc7	trichinelóza
<g/>
,	,	kIx,	,
nebezpečným	bezpečný	k2eNgNnSc7d1	nebezpečné
parazitárním	parazitární	k2eAgNnSc7d1	parazitární
onemocněním	onemocnění	k1gNnSc7	onemocnění
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
svalovec	svalovec	k1gMnSc1	svalovec
stočený	stočený	k2eAgMnSc1d1	stočený
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
také	také	k9	také
vnímavé	vnímavý	k2eAgNnSc1d1	vnímavé
vůči	vůči	k7c3	vůči
africkému	africký	k2eAgInSc3d1	africký
moru	mor	k1gInSc3	mor
prasat	prase	k1gNnPc2	prase
(	(	kIx(	(
<g/>
AMP	AMP	kA	AMP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
výskyt	výskyt	k1gInSc1	výskyt
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
ve	v	k7c6	v
zlínském	zlínský	k2eAgInSc6d1	zlínský
okrese	okres	k1gInSc6	okres
<g/>
.	.	kIx.	.
</s>
<s>
Zasažená	zasažený	k2eAgFnSc1d1	zasažená
oblast	oblast	k1gFnSc1	oblast
byla	být	k5eAaImAgFnS	být
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
pachovými	pachový	k2eAgInPc7d1	pachový
ohradníky	ohradník	k1gInPc7	ohradník
a	a	k8xC	a
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
hromadný	hromadný	k2eAgInSc4d1	hromadný
odlov	odlov	k1gInSc4	odlov
divočáků	divočák	k1gMnPc2	divočák
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
nákazu	nákaz	k1gInSc2	nákaz
eliminovat	eliminovat	k5eAaBmF	eliminovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Wild	Wild	k1gMnSc1	Wild
Boar	Boar	k1gMnSc1	Boar
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Wildschwein	Wildschwein	k1gInSc4	Wildschwein
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Andrej	Andrej	k1gMnSc1	Andrej
Soukup	Soukup	k1gMnSc1	Soukup
a	a	k8xC	a
Jiřina	Jiřina	k1gFnSc1	Jiřina
Soukupová	Soukupová	k1gFnSc1	Soukupová
-	-	kIx~	-
Atlas	Atlas	k1gInSc1	Atlas
savců	savec	k1gMnPc2	savec
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
Slovenska	Slovensko	k1gNnSc2	Slovensko
</s>
</p>
<p>
<s>
Leoš	Leoš	k1gMnSc1	Leoš
Macek	Macek	k1gMnSc1	Macek
-	-	kIx~	-
Největší	veliký	k2eAgMnPc1d3	veliký
savci	savec	k1gMnPc1	savec
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Andreska	Andreska	k1gFnSc1	Andreska
a	a	k8xC	a
Erika	Erika	k1gFnSc1	Erika
Andresková	Andresková	k1gFnSc1	Andresková
-	-	kIx~	-
1000	[number]	k4	1000
let	léto	k1gNnPc2	léto
myslivosti	myslivost	k1gFnSc2	myslivost
-	-	kIx~	-
Prase	prase	k1gNnSc1	prase
divoké	divoký	k2eAgFnSc2d1	divoká
</s>
</p>
<p>
<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
Rutynová	Rutynová	k1gFnSc1	Rutynová
a	a	k8xC	a
Miroslav	Miroslav	k1gMnSc1	Miroslav
Zahradní	zahradní	k2eAgMnSc1d1	zahradní
-	-	kIx~	-
Myslivost	myslivost	k1gFnSc4	myslivost
-	-	kIx~	-
Prase	prase	k1gNnSc4	prase
divoké	divoký	k2eAgFnSc2d1	divoká
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Zvěřina	zvěřina	k1gFnSc1	zvěřina
(	(	kIx(	(
<g/>
maso	maso	k1gNnSc1	maso
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
prase	prase	k1gNnSc1	prase
divoké	divoký	k2eAgNnSc1d1	divoké
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
prase	prase	k1gNnSc1	prase
divoké	divoký	k2eAgFnPc4d1	divoká
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
prase	prase	k1gNnSc1	prase
divoké	divoký	k2eAgFnPc4d1	divoká
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Sus	Sus	k1gMnSc2	Sus
scrofa	scrof	k1gMnSc2	scrof
ve	v	k7c6	v
WikidruzíchPrase	WikidruzíchPrasa	k1gFnSc6	WikidruzíchPrasa
divoké	divoký	k2eAgNnSc1d1	divoké
-	-	kIx~	-
AFRIKAonline	AFRIKAonlin	k1gInSc5	AFRIKAonlin
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Příroda	příroda	k1gFnSc1	příroda
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
