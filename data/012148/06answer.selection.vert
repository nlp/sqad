<s>
Steven	Steven	k2eAgMnSc1d1	Steven
Paul	Paul	k1gMnSc1	Paul
Jobs	Jobsa	k1gFnPc2	Jobsa
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
pouze	pouze	k6eAd1	pouze
Steve	Steve	k1gMnSc1	Steve
Jobs	Jobsa	k1gFnPc2	Jobsa
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1955	[number]	k4	1955
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
<g/>
,	,	kIx,	,
USA	USA	kA	USA
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
Palo	Pala	k1gMnSc5	Pala
Alto	Alta	k1gMnSc5	Alta
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
zakladatel	zakladatel	k1gMnSc1	zakladatel
<g/>
,	,	kIx,	,
výkonný	výkonný	k2eAgMnSc1d1	výkonný
ředitel	ředitel	k1gMnSc1	ředitel
(	(	kIx(	(
<g/>
CEO	CEO	kA	CEO
<g/>
)	)	kIx)	)
a	a	k8xC	a
předseda	předseda	k1gMnSc1	předseda
představenstva	představenstvo	k1gNnSc2	představenstvo
firmy	firma	k1gFnSc2	firma
Apple	Apple	kA	Apple
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejvýraznějších	výrazný	k2eAgFnPc2d3	nejvýraznější
osobností	osobnost	k1gFnPc2	osobnost
počítačového	počítačový	k2eAgInSc2d1	počítačový
průmyslu	průmysl	k1gInSc2	průmysl
posledních	poslední	k2eAgNnPc2d1	poslední
čtyřiceti	čtyřicet	k4xCc2	čtyřicet
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
