<p>
<s>
Neutronové	neutronový	k2eAgNnSc1d1	neutronové
záření	záření	k1gNnSc1	záření
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
ionizujícího	ionizující	k2eAgNnSc2d1	ionizující
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
tvořeného	tvořený	k2eAgInSc2d1	tvořený
proudem	proud	k1gInSc7	proud
volných	volný	k2eAgInPc2d1	volný
neutronů	neutron	k1gInPc2	neutron
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
neutrony	neutron	k1gInPc1	neutron
se	se	k3xPyFc4	se
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
při	při	k7c6	při
jaderném	jaderný	k2eAgNnSc6d1	jaderné
štěpení	štěpení	k1gNnSc6	štěpení
nebo	nebo	k8xC	nebo
fúzi	fúze	k1gFnSc6	fúze
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
interagují	interagovat	k5eAaBmIp3nP	interagovat
s	s	k7c7	s
jádry	jádro	k1gNnPc7	jádro
dalších	další	k2eAgMnPc2d1	další
atomů	atom	k1gInPc2	atom
a	a	k8xC	a
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
tak	tak	k6eAd1	tak
nové	nový	k2eAgInPc1d1	nový
izotopy	izotop	k1gInPc1	izotop
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zdroje	zdroj	k1gInPc1	zdroj
==	==	k?	==
</s>
</p>
<p>
<s>
Neutrony	neutron	k1gInPc1	neutron
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
emitovány	emitovat	k5eAaBmNgInP	emitovat
i	i	k9	i
při	při	k7c6	při
dalších	další	k2eAgFnPc6d1	další
jaderných	jaderný	k2eAgFnPc6d1	jaderná
reakcích	reakce	k1gFnPc6	reakce
<g/>
,	,	kIx,	,
např.	např.	kA	např.
při	při	k7c6	při
jaderném	jaderný	k2eAgInSc6d1	jaderný
rozpadu	rozpad	k1gInSc6	rozpad
nebo	nebo	k8xC	nebo
při	při	k7c6	při
reakcích	reakce	k1gFnPc6	reakce
vzniklých	vzniklý	k2eAgFnPc2d1	vzniklá
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
interakcí	interakce	k1gFnPc2	interakce
částic	částice	k1gFnPc2	částice
(	(	kIx(	(
<g/>
např	např	kA	např
z	z	k7c2	z
kosmického	kosmický	k2eAgNnSc2d1	kosmické
záření	záření	k1gNnSc2	záření
nebo	nebo	k8xC	nebo
v	v	k7c6	v
urychlovačích	urychlovač	k1gInPc6	urychlovač
částic	částice	k1gFnPc2	částice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Významných	významný	k2eAgInPc2d1	významný
zdrojů	zdroj	k1gInPc2	zdroj
neutronů	neutron	k1gInPc2	neutron
není	být	k5eNaImIp3nS	být
mnoho	mnoho	k6eAd1	mnoho
a	a	k8xC	a
taková	takový	k3xDgNnPc1	takový
zařízení	zařízení	k1gNnPc1	zařízení
obvykle	obvykle	k6eAd1	obvykle
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
velkých	velký	k2eAgInPc2d1	velký
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
v	v	k7c6	v
případě	případ	k1gInSc6	případ
jaderných	jaderný	k2eAgInPc2d1	jaderný
reaktorů	reaktor	k1gInPc2	reaktor
nebo	nebo	k8xC	nebo
urychlovačů	urychlovač	k1gInPc2	urychlovač
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Neutronové	neutronový	k2eAgNnSc1d1	neutronové
záření	záření	k1gNnSc1	záření
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
jako	jako	k8xS	jako
výsledek	výsledek	k1gInSc1	výsledek
pozorovaní	pozorovaný	k2eAgMnPc1d1	pozorovaný
jader	jádro	k1gNnPc2	jádro
berylia	berylium	k1gNnSc2	berylium
při	při	k7c6	při
reakci	reakce	k1gFnSc6	reakce
s	s	k7c7	s
alfa	alfa	k1gNnSc7	alfa
částicemi	částice	k1gFnPc7	částice
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
přeměňují	přeměňovat	k5eAaImIp3nP	přeměňovat
v	v	k7c4	v
jádra	jádro	k1gNnPc4	jádro
uhlíku	uhlík	k1gInSc2	uhlík
a	a	k8xC	a
produkují	produkovat	k5eAaImIp3nP	produkovat
neutrony	neutron	k1gInPc1	neutron
<g/>
,	,	kIx,	,
Be	Be	k1gFnPc1	Be
<g/>
(	(	kIx(	(
<g/>
α	α	k?	α
<g/>
,	,	kIx,	,
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
C.	C.	kA	C.
Kombinace	kombinace	k1gFnSc1	kombinace
emitoru	emitor	k1gInSc2	emitor
alfa	alfa	k1gNnSc2	alfa
částic	částice	k1gFnPc2	částice
a	a	k8xC	a
izotopu	izotop	k1gInSc2	izotop
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
(	(	kIx(	(
<g/>
α	α	k?	α
<g/>
,	,	kIx,	,
n	n	k0	n
<g/>
)	)	kIx)	)
reakce	reakce	k1gFnPc4	reakce
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
běžným	běžný	k2eAgInSc7d1	běžný
zdrojem	zdroj	k1gInSc7	zdroj
neutronů	neutron	k1gInPc2	neutron
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Užití	užití	k1gNnSc2	užití
==	==	k?	==
</s>
</p>
<p>
<s>
Studené	Studená	k1gFnPc1	Studená
<g/>
,	,	kIx,	,
teplé	teplý	k2eAgInPc1d1	teplý
a	a	k8xC	a
horké	horký	k2eAgInPc1d1	horký
neutrony	neutron	k1gInPc1	neutron
jsou	být	k5eAaImIp3nP	být
nejčastěji	často	k6eAd3	často
používány	používat	k5eAaImNgInP	používat
při	při	k7c6	při
experimentech	experiment	k1gInPc6	experiment
rozptylu	rozptyl	k1gInSc2	rozptyl
a	a	k8xC	a
ohybu	ohyb	k1gInSc2	ohyb
pro	pro	k7c4	pro
zjištění	zjištění	k1gNnSc4	zjištění
vlastností	vlastnost	k1gFnPc2	vlastnost
a	a	k8xC	a
struktury	struktura	k1gFnSc2	struktura
materiálu	materiál	k1gInSc2	materiál
v	v	k7c6	v
krystalografii	krystalografie	k1gFnSc6	krystalografie
<g/>
,	,	kIx,	,
fyzice	fyzika	k1gFnSc3	fyzika
kondenzovaných	kondenzovaný	k2eAgFnPc2d1	kondenzovaná
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
biologii	biologie	k1gFnSc4	biologie
<g/>
,	,	kIx,	,
chemii	chemie	k1gFnSc4	chemie
pevných	pevný	k2eAgFnPc2d1	pevná
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
materiálové	materiálový	k2eAgFnSc3d1	materiálová
vědě	věda	k1gFnSc3	věda
<g/>
,	,	kIx,	,
geologii	geologie	k1gFnSc6	geologie
<g/>
,	,	kIx,	,
mineralogii	mineralogie	k1gFnSc6	mineralogie
a	a	k8xC	a
příbuzných	příbuzný	k2eAgFnPc6d1	příbuzná
vědách	věda	k1gFnPc6	věda
<g/>
.	.	kIx.	.
</s>
<s>
Neutronové	neutronový	k2eAgNnSc1d1	neutronové
záření	záření	k1gNnSc1	záření
je	být	k5eAaImIp3nS	být
taktéž	taktéž	k?	taktéž
využíváno	využíván	k2eAgNnSc4d1	využíváno
ve	v	k7c6	v
vybraných	vybraný	k2eAgNnPc6d1	vybrané
zařízeních	zařízení	k1gNnPc6	zařízení
k	k	k7c3	k
léčení	léčení	k1gNnSc3	léčení
rakovinových	rakovinový	k2eAgInPc2d1	rakovinový
nádorů	nádor	k1gInPc2	nádor
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnPc4	jeho
pronikavé	pronikavý	k2eAgFnPc4d1	pronikavá
a	a	k8xC	a
ničivé	ničivý	k2eAgFnPc4d1	ničivá
vlastnosti	vlastnost	k1gFnPc4	vlastnost
u	u	k7c2	u
buněčné	buněčný	k2eAgFnSc2d1	buněčná
struktury	struktura	k1gFnSc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Neutrony	neutron	k1gInPc1	neutron
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
také	také	k9	také
použity	použít	k5eAaPmNgInP	použít
pro	pro	k7c4	pro
zobrazení	zobrazení	k1gNnSc4	zobrazení
průmyslových	průmyslový	k2eAgFnPc2d1	průmyslová
součástek	součástka	k1gFnPc2	součástka
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
neutronová	neutronový	k2eAgFnSc1d1	neutronová
radiografie	radiografie	k1gFnPc1	radiografie
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
fólie	fólie	k1gFnSc2	fólie
<g/>
,	,	kIx,	,
neutronová	neutronový	k2eAgFnSc1d1	neutronová
radioskopie	radioskopie	k1gFnSc1	radioskopie
při	při	k7c6	při
pořízení	pořízení	k1gNnSc6	pořízení
digitálních	digitální	k2eAgInPc2d1	digitální
snímků	snímek	k1gInPc2	snímek
a	a	k8xC	a
neutronová	neutronový	k2eAgFnSc1d1	neutronová
tomografie	tomografie	k1gFnSc1	tomografie
pro	pro	k7c4	pro
trojrozměrné	trojrozměrný	k2eAgNnSc4d1	trojrozměrné
zobrazení	zobrazení	k1gNnSc4	zobrazení
<g/>
.	.	kIx.	.
</s>
<s>
Neutronové	neutronový	k2eAgNnSc1d1	neutronové
zobrazování	zobrazování	k1gNnSc1	zobrazování
je	být	k5eAaImIp3nS	být
běžně	běžně	k6eAd1	běžně
užíváno	užívat	k5eAaImNgNnS	užívat
v	v	k7c6	v
jaderném	jaderný	k2eAgInSc6d1	jaderný
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vesmírném	vesmírný	k2eAgInSc6d1	vesmírný
a	a	k8xC	a
kosmickém	kosmický	k2eAgInSc6d1	kosmický
průmyslu	průmysl	k1gInSc6	průmysl
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
výbušnin	výbušnina	k1gFnPc2	výbušnina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
A	a	k9	a
také	také	k9	také
ho	on	k3xPp3gMnSc4	on
využívají	využívat	k5eAaPmIp3nP	využívat
jaderné	jaderný	k2eAgFnPc4d1	jaderná
elektrárny	elektrárna	k1gFnPc4	elektrárna
k	k	k7c3	k
začátku	začátek	k1gInSc3	začátek
řetězové	řetězový	k2eAgFnSc2d1	řetězová
reakce	reakce	k1gFnSc2	reakce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ionizační	ionizační	k2eAgInPc4d1	ionizační
mechanismy	mechanismus	k1gInPc4	mechanismus
a	a	k8xC	a
vlastnosti	vlastnost	k1gFnPc4	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Neutronové	neutronový	k2eAgNnSc1d1	neutronové
záření	záření	k1gNnSc1	záření
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
nazýváno	nazývat	k5eAaImNgNnS	nazývat
nepřímým	přímý	k2eNgNnSc7d1	nepřímé
ionizačním	ionizační	k2eAgNnSc7d1	ionizační
zářením	záření	k1gNnSc7	záření
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
neionizuje	ionizovat	k5eNaBmIp3nS	ionizovat
atomy	atom	k1gInPc4	atom
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
jako	jako	k8xC	jako
nabité	nabitý	k2eAgFnPc1d1	nabitá
částice	částice	k1gFnPc1	částice
(	(	kIx(	(
<g/>
protony	proton	k1gInPc1	proton
a	a	k8xC	a
elektrony	elektron	k1gInPc1	elektron
excitací	excitace	k1gFnPc2	excitace
elektronu	elektron	k1gInSc2	elektron
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nemají	mít	k5eNaImIp3nP	mít
náboj	náboj	k1gInSc4	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
neutronové	neutronový	k2eAgFnPc1d1	neutronová
interakce	interakce	k1gFnPc1	interakce
jsou	být	k5eAaImIp3nP	být
vysoce	vysoce	k6eAd1	vysoce
ionizující	ionizující	k2eAgMnPc1d1	ionizující
<g/>
,	,	kIx,	,
např.	např.	kA	např.
při	při	k7c6	při
absorpci	absorpce	k1gFnSc6	absorpce
neutronu	neutron	k1gInSc2	neutron
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
emisi	emise	k1gFnSc3	emise
gama	gama	k1gNnSc2	gama
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
následně	následně	k6eAd1	následně
odstraní	odstranit	k5eAaPmIp3nS	odstranit
elektron	elektron	k1gInSc1	elektron
z	z	k7c2	z
atomového	atomový	k2eAgInSc2d1	atomový
obalu	obal	k1gInSc2	obal
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jádro	jádro	k1gNnSc1	jádro
poodražené	poodražený	k2eAgNnSc1d1	poodražený
po	po	k7c6	po
interakci	interakce	k1gFnSc6	interakce
s	s	k7c7	s
neutronem	neutron	k1gInSc7	neutron
je	být	k5eAaImIp3nS	být
ionizováno	ionizován	k2eAgNnSc1d1	ionizováno
a	a	k8xC	a
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
více	hodně	k6eAd2	hodně
tradičních	tradiční	k2eAgFnPc2d1	tradiční
následných	následný	k2eAgFnPc2d1	následná
ionizací	ionizace	k1gFnPc2	ionizace
i	i	k8xC	i
dalších	další	k2eAgInPc6d1	další
atomech	atom	k1gInPc6	atom
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
neutrony	neutron	k1gInPc1	neutron
jsou	být	k5eAaImIp3nP	být
nenabité	nabitý	k2eNgInPc1d1	nenabitý
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
mají	mít	k5eAaImIp3nP	mít
větší	veliký	k2eAgFnSc4d2	veliký
pronikavost	pronikavost	k1gFnSc4	pronikavost
nežli	nežli	k8xS	nežli
částice	částice	k1gFnSc1	částice
alfa	alfa	k1gNnSc1	alfa
nebo	nebo	k8xC	nebo
záření	záření	k1gNnSc1	záření
beta	beta	k1gNnSc1	beta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
mají	mít	k5eAaImIp3nP	mít
větší	veliký	k2eAgFnSc4d2	veliký
pronikavost	pronikavost	k1gFnSc4	pronikavost
než	než	k8xS	než
záření	záření	k1gNnSc4	záření
gama	gama	k1gNnSc2	gama
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
zpomaleno	zpomalit	k5eAaPmNgNnS	zpomalit
v	v	k7c6	v
atomech	atom	k1gInPc6	atom
s	s	k7c7	s
vysokým	vysoký	k2eAgNnSc7d1	vysoké
atomovým	atomový	k2eAgNnSc7d1	atomové
číslem	číslo	k1gNnSc7	číslo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
atomech	atom	k1gInPc6	atom
s	s	k7c7	s
malým	malý	k2eAgNnSc7d1	malé
atomovým	atomový	k2eAgNnSc7d1	atomové
číslem	číslo	k1gNnSc7	číslo
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
vodík	vodík	k1gInSc4	vodík
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nízkoenergetické	nízkoenergetický	k2eAgNnSc1d1	nízkoenergetické
gama	gama	k1gNnSc1	gama
záření	záření	k1gNnSc2	záření
pronikavější	pronikavý	k2eAgInPc4d2	pronikavější
než	než	k8xS	než
neutrony	neutron	k1gInPc1	neutron
o	o	k7c6	o
vysoké	vysoký	k2eAgFnSc6d1	vysoká
energii	energie	k1gFnSc6	energie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zdravotní	zdravotní	k2eAgNnPc4d1	zdravotní
rizika	riziko	k1gNnPc4	riziko
a	a	k8xC	a
ochrana	ochrana	k1gFnSc1	ochrana
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
lékařské	lékařský	k2eAgFnSc6d1	lékařská
fyzice	fyzika	k1gFnSc6	fyzika
je	být	k5eAaImIp3nS	být
o	o	k7c6	o
neutronovém	neutronový	k2eAgInSc6d1	neutronový
záření	záření	k1gNnSc2	záření
uvažováno	uvažován	k2eAgNnSc1d1	uvažováno
jako	jako	k8xC	jako
o	o	k7c6	o
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
radiační	radiační	k2eAgNnPc4d1	radiační
nebezpečí	nebezpečí	k1gNnPc4	nebezpečí
napříč	napříč	k7c3	napříč
dalším	další	k2eAgInPc3d1	další
druhům	druh	k1gInPc3	druh
radiace	radiace	k1gFnSc2	radiace
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
větším	veliký	k2eAgNnSc7d2	veliký
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
neutronového	neutronový	k2eAgNnSc2d1	neutronové
záření	záření	k1gNnSc2	záření
tzv.	tzv.	kA	tzv.
neutronová	neutronový	k2eAgFnSc1d1	neutronová
aktivace	aktivace	k1gFnSc1	aktivace
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
schopnost	schopnost	k1gFnSc4	schopnost
neutronového	neutronový	k2eAgNnSc2d1	neutronové
záření	záření	k1gNnSc2	záření
vyvolat	vyvolat	k5eAaPmF	vyvolat
radioaktivitu	radioaktivita	k1gFnSc4	radioaktivita
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
na	na	k7c4	na
něž	jenž	k3xRgFnPc4	jenž
dopadá	dopadat	k5eAaImIp3nS	dopadat
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
tělesné	tělesný	k2eAgFnSc2d1	tělesná
tkáně	tkáň	k1gFnSc2	tkáň
samotných	samotný	k2eAgMnPc2d1	samotný
pracovníků	pracovník	k1gMnPc2	pracovník
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
dochází	docházet	k5eAaImIp3nS	docházet
při	při	k7c6	při
zachycení	zachycení	k1gNnSc6	zachycení
neutronů	neutron	k1gInPc2	neutron
atomovým	atomový	k2eAgNnSc7d1	atomové
jádrem	jádro	k1gNnSc7	jádro
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
transformuje	transformovat	k5eAaBmIp3nS	transformovat
na	na	k7c4	na
jiný	jiný	k2eAgInSc4d1	jiný
nuklid	nuklid	k1gInSc4	nuklid
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
na	na	k7c4	na
radionuklid	radionuklid	k1gInSc4	radionuklid
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
je	být	k5eAaImIp3nS	být
důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
radioaktivních	radioaktivní	k2eAgInPc2d1	radioaktivní
materiálů	materiál	k1gInPc2	materiál
uvolněných	uvolněný	k2eAgInPc2d1	uvolněný
při	při	k7c6	při
výbuchu	výbuch	k1gInSc6	výbuch
jaderné	jaderný	k2eAgFnSc2d1	jaderná
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
problém	problém	k1gInSc1	problém
vzniká	vznikat	k5eAaImIp3nS	vznikat
taktéž	taktéž	k?	taktéž
u	u	k7c2	u
zařízení	zařízení	k1gNnSc2	zařízení
pro	pro	k7c4	pro
jaderné	jaderný	k2eAgNnSc4d1	jaderné
štěpení	štěpení	k1gNnSc4	štěpení
a	a	k8xC	a
fúzi	fúze	k1gFnSc4	fúze
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
že	že	k9	že
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
vybavení	vybavení	k1gNnSc1	vybavení
stává	stávat	k5eAaImIp3nS	stávat
radioaktivním	radioaktivní	k2eAgNnSc7d1	radioaktivní
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
technické	technický	k2eAgNnSc4d1	technické
vybavení	vybavení	k1gNnSc4	vybavení
odstraněno	odstranit	k5eAaPmNgNnS	odstranit
a	a	k8xC	a
zlikvidováno	zlikvidován	k2eAgNnSc1d1	zlikvidováno
jako	jako	k8xS	jako
radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
odpad	odpad	k1gInSc1	odpad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ochrana	ochrana	k1gFnSc1	ochrana
před	před	k7c7	před
neutronovým	neutronový	k2eAgNnSc7d1	neutronové
zářením	záření	k1gNnSc7	záření
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
radiačním	radiační	k2eAgNnSc6d1	radiační
stínění	stínění	k1gNnSc6	stínění
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
vysoké	vysoký	k2eAgFnSc2d1	vysoká
kinetické	kinetický	k2eAgFnSc2d1	kinetická
energie	energie	k1gFnSc2	energie
neutronů	neutron	k1gInPc2	neutron
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
radiace	radiace	k1gFnSc1	radiace
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
nejtvrdší	tvrdý	k2eAgFnSc4d3	nejtvrdší
a	a	k8xC	a
nejnebezpečnější	bezpečný	k2eNgFnSc4d3	nejnebezpečnější
radiaci	radiace	k1gFnSc4	radiace
pro	pro	k7c4	pro
celé	celý	k2eAgNnSc4d1	celé
tělo	tělo	k1gNnSc4	tělo
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
vystaveno	vystaven	k2eAgNnSc1d1	vystaveno
externím	externí	k2eAgMnSc7d1	externí
zdrojům	zdroj	k1gInPc3	zdroj
této	tento	k3xDgFnSc2	tento
radiace	radiace	k1gFnSc2	radiace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
běžným	běžný	k2eAgNnSc7d1	běžné
ionizujícím	ionizující	k2eAgNnSc7d1	ionizující
zářením	záření	k1gNnSc7	záření
založeným	založený	k2eAgNnSc7d1	založené
na	na	k7c6	na
fotonech	foton	k1gInPc6	foton
nebo	nebo	k8xC	nebo
nabitých	nabitý	k2eAgFnPc6d1	nabitá
částicích	částice	k1gFnPc6	částice
jsou	být	k5eAaImIp3nP	být
neutrony	neutron	k1gInPc1	neutron
opakovaně	opakovaně	k6eAd1	opakovaně
odráženy	odrážet	k5eAaImNgInP	odrážet
a	a	k8xC	a
zpomalovány	zpomalovat	k5eAaImNgInP	zpomalovat
(	(	kIx(	(
<g/>
absorbovány	absorbován	k2eAgInPc1d1	absorbován
<g/>
)	)	kIx)	)
lehkými	lehký	k2eAgNnPc7d1	lehké
jádry	jádro	k1gNnPc7	jádro
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
materiály	materiál	k1gInPc1	materiál
bohaté	bohatý	k2eAgInPc1d1	bohatý
na	na	k7c4	na
vodík	vodík	k1gInSc4	vodík
jsou	být	k5eAaImIp3nP	být
efektivnější	efektivní	k2eAgNnPc1d2	efektivnější
než	než	k8xS	než
jádra	jádro	k1gNnPc1	jádro
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Lehké	Lehké	k2eAgInPc1d1	Lehké
atomy	atom	k1gInPc1	atom
zpomalují	zpomalovat	k5eAaImIp3nP	zpomalovat
neutrony	neutron	k1gInPc4	neutron
elastickým	elastický	k2eAgInSc7d1	elastický
rozptylem	rozptyl	k1gInSc7	rozptyl
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
podílet	podílet	k5eAaImF	podílet
na	na	k7c6	na
jaderných	jaderný	k2eAgFnPc6d1	jaderná
reakcích	reakce	k1gFnPc6	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
reakce	reakce	k1gFnPc1	reakce
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
zdrojem	zdroj	k1gInSc7	zdroj
záření	záření	k1gNnSc2	záření
gama	gama	k1gNnSc1	gama
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
provedeno	proveden	k2eAgNnSc4d1	provedeno
přídavné	přídavný	k2eAgNnSc4d1	přídavné
stínění	stínění	k1gNnSc4	stínění
<g/>
.	.	kIx.	.
</s>
<s>
Zřetel	zřetel	k1gInSc1	zřetel
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
brán	brát	k5eAaImNgMnS	brát
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
se	se	k3xPyFc4	se
vyhnuli	vyhnout	k5eAaPmAgMnP	vyhnout
použití	použití	k1gNnSc2	použití
jader	jádro	k1gNnPc2	jádro
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
podléhají	podléhat	k5eAaImIp3nP	podléhat
štěpení	štěpení	k1gNnSc4	štěpení
nebo	nebo	k8xC	nebo
zachytávání	zachytávání	k1gNnSc4	zachytávání
neutronů	neutron	k1gInPc2	neutron
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
radioaktivnímu	radioaktivní	k2eAgInSc3d1	radioaktivní
rozpadu	rozpad	k1gInSc3	rozpad
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
provází	provázet	k5eAaImIp3nS	provázet
produkce	produkce	k1gFnSc2	produkce
paprsků	paprsek	k1gInPc2	paprsek
gama	gama	k1gNnSc1	gama
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neutrony	neutron	k1gInPc1	neutron
snadno	snadno	k6eAd1	snadno
procházejí	procházet	k5eAaImIp3nP	procházet
přes	přes	k7c4	přes
většinu	většina	k1gFnSc4	většina
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dosti	dosti	k6eAd1	dosti
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
interagují	interagovat	k5eAaPmIp3nP	interagovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
biologické	biologický	k2eAgFnPc4d1	biologická
škody	škoda	k1gFnPc4	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Nejefektivněji	efektivně	k6eAd3	efektivně
stínící	stínící	k2eAgInPc1d1	stínící
materiály	materiál	k1gInPc1	materiál
jsou	být	k5eAaImIp3nP	být
uhlovodíky	uhlovodík	k1gInPc4	uhlovodík
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
polyethylen	polyethylen	k1gInSc1	polyethylen
<g/>
,	,	kIx,	,
tuhý	tuhý	k2eAgInSc1d1	tuhý
parafín	parafín	k1gInSc1	parafín
nebo	nebo	k8xC	nebo
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Beton	beton	k1gInSc1	beton
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
molekul	molekula	k1gFnPc2	molekula
vody	voda	k1gFnSc2	voda
chemicky	chemicky	k6eAd1	chemicky
svázáno	svázat	k5eAaPmNgNnS	svázat
s	s	k7c7	s
cementem	cement	k1gInSc7	cement
<g/>
)	)	kIx)	)
a	a	k8xC	a
štěrk	štěrk	k1gInSc4	štěrk
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
jako	jako	k9	jako
levné	levný	k2eAgFnPc1d1	levná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
efektivní	efektivní	k2eAgNnSc4d1	efektivní
biologické	biologický	k2eAgNnSc4d1	biologické
stínění	stínění	k1gNnSc4	stínění
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnSc4	jejich
kombinované	kombinovaný	k2eAgNnSc4d1	kombinované
stínění	stínění	k1gNnSc4	stínění
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
paprsků	paprsek	k1gInPc2	paprsek
gama	gama	k1gNnSc2	gama
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
neutronů	neutron	k1gInPc2	neutron
<g/>
.	.	kIx.	.
</s>
<s>
Vynikajícím	vynikající	k2eAgInSc7d1	vynikající
absorbérem	absorbér	k1gInSc7	absorbér
neutronů	neutron	k1gInPc2	neutron
je	být	k5eAaImIp3nS	být
bór	bór	k1gInSc1	bór
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
určitého	určitý	k2eAgInSc2d1	určitý
neutronového	neutronový	k2eAgInSc2d1	neutronový
rozptylu	rozptyl	k1gInSc2	rozptyl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
rozpadá	rozpadat	k5eAaImIp3nS	rozpadat
na	na	k7c4	na
lithium	lithium	k1gNnSc4	lithium
a	a	k8xC	a
hélium	hélium	k1gNnSc4	hélium
a	a	k8xC	a
neprodukuje	produkovat	k5eNaImIp3nS	produkovat
prakticky	prakticky	k6eAd1	prakticky
žádné	žádný	k3yNgNnSc1	žádný
gama	gama	k1gNnSc1	gama
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Karbid	karbid	k1gInSc1	karbid
bóru	bór	k1gInSc2	bór
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
použije	použít	k5eAaPmIp3nS	použít
ke	k	k7c3	k
stínění	stínění	k1gNnSc3	stínění
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
betonové	betonový	k2eAgNnSc1d1	betonové
stínění	stínění	k1gNnSc1	stínění
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
nákladné	nákladný	k2eAgNnSc1d1	nákladné
<g/>
.	.	kIx.	.
</s>
<s>
Komerčně	komerčně	k6eAd1	komerčně
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
vodě	voda	k1gFnSc3	voda
<g/>
,	,	kIx,	,
naftě	nafta	k1gFnSc6	nafta
<g/>
,	,	kIx,	,
betonu	beton	k1gInSc6	beton
<g/>
,	,	kIx,	,
štěrku	štěrk	k1gInSc6	štěrk
a	a	k8xC	a
B4C	B4C	k1gFnPc1	B4C
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
běžná	běžný	k2eAgNnPc1d1	běžné
stínění	stínění	k1gNnPc1	stínění
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
obklopují	obklopovat	k5eAaImIp3nP	obklopovat
plochy	plocha	k1gFnPc4	plocha
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
neutronovým	neutronový	k2eAgInSc7d1	neutronový
tokem	tok	k1gInSc7	tok
<g/>
.	.	kIx.	.
</s>
<s>
Bórem	bór	k1gInSc7	bór
impregnované	impregnovaný	k2eAgNnSc1d1	impregnované
silikonové	silikonový	k2eAgNnSc1d1	silikonové
sklo	sklo	k1gNnSc1	sklo
<g/>
,	,	kIx,	,
vysoce	vysoce	k6eAd1	vysoce
bórová	bórový	k2eAgFnSc1d1	bórová
ocel	ocel	k1gFnSc1	ocel
<g/>
,	,	kIx,	,
parafín	parafín	k1gInSc1	parafín
a	a	k8xC	a
plexisklo	plexisklo	k1gNnSc1	plexisklo
mají	mít	k5eAaImIp3nP	mít
ochranné	ochranný	k2eAgNnSc4d1	ochranné
užití	užití	k1gNnSc4	užití
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
neutrony	neutron	k1gInPc1	neutron
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
zasáhnou	zasáhnout	k5eAaPmIp3nP	zasáhnout
vodíková	vodíkový	k2eAgNnPc1d1	vodíkové
jádra	jádro	k1gNnPc1	jádro
(	(	kIx(	(
<g/>
protony	proton	k1gInPc1	proton
nebo	nebo	k8xC	nebo
deuterony	deuteron	k1gInPc1	deuteron
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
předají	předat	k5eAaPmIp3nP	předat
těmto	tento	k3xDgMnPc3	tento
jádrům	jádro	k1gNnPc3	jádro
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
tato	tento	k3xDgNnPc1	tento
jádra	jádro	k1gNnPc1	jádro
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
oddělí	oddělit	k5eAaPmIp3nP	oddělit
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
chemických	chemický	k2eAgFnPc2d1	chemická
vazeb	vazba	k1gFnPc2	vazba
a	a	k8xC	a
putují	putovat	k5eAaImIp3nP	putovat
na	na	k7c4	na
malou	malý	k2eAgFnSc4d1	malá
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
než	než	k8xS	než
se	se	k3xPyFc4	se
zastaví	zastavit	k5eAaPmIp3nS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
vodíková	vodíkový	k2eAgNnPc1d1	vodíkové
jádra	jádro	k1gNnPc1	jádro
jsou	být	k5eAaImIp3nP	být
vysoce	vysoce	k6eAd1	vysoce
urychlenými	urychlený	k2eAgFnPc7d1	urychlená
částicemi	částice	k1gFnPc7	částice
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
zastavena	zastavit	k5eAaPmNgFnS	zastavit
ionizací	ionizace	k1gFnSc7	ionizace
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
procházejí	procházet	k5eAaImIp3nP	procházet
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
v	v	k7c6	v
živé	živý	k2eAgFnSc6d1	živá
tkáni	tkáň	k1gFnSc6	tkáň
mají	mít	k5eAaImIp3nP	mít
neutrony	neutron	k1gInPc1	neutron
poměrně	poměrně	k6eAd1	poměrně
vysokou	vysoký	k2eAgFnSc4d1	vysoká
relativní	relativní	k2eAgFnSc4d1	relativní
účinnost	účinnost	k1gFnSc4	účinnost
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
přibližně	přibližně	k6eAd1	přibližně
desetkrát	desetkrát	k6eAd1	desetkrát
účinnější	účinný	k2eAgMnSc1d2	účinnější
v	v	k7c6	v
biologickém	biologický	k2eAgNnSc6d1	biologické
poškození	poškození	k1gNnSc6	poškození
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
gama	gama	k1gNnSc7	gama
zářením	záření	k1gNnSc7	záření
nebo	nebo	k8xC	nebo
beta	beta	k1gNnSc7	beta
zářením	záření	k1gNnSc7	záření
při	při	k7c6	při
stejném	stejný	k2eAgNnSc6d1	stejné
ozáření	ozáření	k1gNnSc6	ozáření
<g/>
.	.	kIx.	.
</s>
<s>
Neutrony	neutron	k1gInPc1	neutron
jsou	být	k5eAaImIp3nP	být
zvlášť	zvlášť	k6eAd1	zvlášť
nebezpečné	bezpečný	k2eNgInPc1d1	nebezpečný
pro	pro	k7c4	pro
měkkou	měkký	k2eAgFnSc4d1	měkká
tkáň	tkáň	k1gFnSc4	tkáň
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
koutky	koutek	k1gInPc4	koutek
očí	oko	k1gNnPc2	oko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
materiály	materiál	k1gInPc4	materiál
==	==	k?	==
</s>
</p>
<p>
<s>
Neutrony	neutron	k1gInPc1	neutron
také	také	k9	také
znehodnocují	znehodnocovat	k5eAaImIp3nP	znehodnocovat
materiály	materiál	k1gInPc1	materiál
<g/>
;	;	kIx,	;
bombardování	bombardování	k1gNnSc1	bombardování
materiálů	materiál	k1gInPc2	materiál
neutrony	neutron	k1gInPc4	neutron
vytváří	vytvářit	k5eAaPmIp3nP	vytvářit
laviny	lavina	k1gFnPc1	lavina
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
vytvářet	vytvářet	k5eAaImF	vytvářet
bodové	bodový	k2eAgInPc4d1	bodový
defekty	defekt	k1gInPc4	defekt
a	a	k8xC	a
rozrušení	rozrušení	k1gNnSc4	rozrušení
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vysokém	vysoký	k2eAgNnSc6d1	vysoké
neutronovém	neutronový	k2eAgNnSc6d1	neutronové
ozáření	ozáření	k1gNnSc6	ozáření
to	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
ke	k	k7c3	k
zkřehnutí	zkřehnutí	k1gNnSc3	zkřehnutí
kovů	kov	k1gInPc2	kov
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
materiálů	materiál	k1gInPc2	materiál
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
u	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jejich	jejich	k3xOp3gNnSc4	jejich
nabobtnání	nabobtnání	k1gNnSc4	nabobtnání
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
znamená	znamenat	k5eAaImIp3nS	znamenat
problém	problém	k1gInSc1	problém
pro	pro	k7c4	pro
jaderné	jaderný	k2eAgFnPc4d1	jaderná
reaktorové	reaktorový	k2eAgFnPc4d1	reaktorová
nádoby	nádoba	k1gFnPc4	nádoba
a	a	k8xC	a
význačně	význačně	k6eAd1	význačně
omezuje	omezovat	k5eAaImIp3nS	omezovat
jejich	jejich	k3xOp3gFnSc4	jejich
životnost	životnost	k1gFnSc4	životnost
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
určitým	určitý	k2eAgInSc7d1	určitý
způsobem	způsob	k1gInSc7	způsob
prodloužena	prodloužit	k5eAaPmNgFnS	prodloužit
jejím	její	k3xOp3gNnSc7	její
žíháním	žíhání	k1gNnSc7	žíhání
omezujícím	omezující	k2eAgInSc7d1	omezující
počet	počet	k1gInSc4	počet
vzniku	vznik	k1gInSc2	vznik
materiálového	materiálový	k2eAgNnSc2d1	materiálové
rozrušení	rozrušení	k1gNnSc2	rozrušení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Grafitové	grafitový	k2eAgInPc1d1	grafitový
moderátorové	moderátorové	k?	moderátorové
bloky	blok	k1gInPc1	blok
jsou	být	k5eAaImIp3nP	být
obzvláště	obzvláště	k6eAd1	obzvláště
citlivé	citlivý	k2eAgFnPc1d1	citlivá
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
efekt	efekt	k1gInSc4	efekt
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
Winger	Winger	k1gMnSc1	Winger
efekt	efekt	k1gInSc4	efekt
<g/>
,	,	kIx,	,
a	a	k8xC	a
musejí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
žíhány	žíhat	k5eAaImNgInP	žíhat
periodicky	periodicky	k6eAd1	periodicky
<g/>
;	;	kIx,	;
dobře	dobře	k6eAd1	dobře
známý	známý	k2eAgInSc1d1	známý
Windscale	Windscala	k1gFnSc3	Windscala
fire	fir	k1gFnSc2	fir
byl	být	k5eAaImAgInS	být
způsoben	způsobit	k5eAaPmNgInS	způsobit
nehodou	nehoda	k1gFnSc7	nehoda
během	během	k7c2	během
takovéto	takovýto	k3xDgFnSc2	takovýto
operace	operace	k1gFnSc2	operace
žíhání	žíhání	k1gNnSc2	žíhání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Neutronové	neutronový	k2eAgNnSc4d1	neutronové
záření	záření	k1gNnSc4	záření
a	a	k8xC	a
jaderné	jaderný	k2eAgNnSc4d1	jaderné
štěpení	štěpení	k1gNnSc4	štěpení
==	==	k?	==
</s>
</p>
<p>
<s>
Neutronové	neutronový	k2eAgInPc1d1	neutronový
reaktory	reaktor	k1gInPc1	reaktor
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
děleny	dělen	k2eAgInPc1d1	dělen
na	na	k7c4	na
pomalé	pomalý	k2eAgInPc4d1	pomalý
(	(	kIx(	(
<g/>
teplé	teplý	k2eAgInPc4d1	teplý
<g/>
)	)	kIx)	)
neutrony	neutron	k1gInPc4	neutron
a	a	k8xC	a
rychlé	rychlý	k2eAgInPc4d1	rychlý
(	(	kIx(	(
<g/>
horké	horký	k2eAgInPc4d1	horký
<g/>
)	)	kIx)	)
neutrony	neutron	k1gInPc4	neutron
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gFnSc2	jejich
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Teplé	Teplé	k2eAgInPc1d1	Teplé
neutrony	neutron	k1gInPc1	neutron
jsou	být	k5eAaImIp3nP	být
podobné	podobný	k2eAgInPc1d1	podobný
plynu	plyn	k1gInSc2	plyn
v	v	k7c6	v
termodynamické	termodynamický	k2eAgFnSc6d1	termodynamická
rovnováze	rovnováha	k1gFnSc6	rovnováha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
lehce	lehko	k6eAd1	lehko
zachycovány	zachycovat	k5eAaImNgInP	zachycovat
atomovými	atomový	k2eAgFnPc7d1	atomová
jádry	jádro	k1gNnPc7	jádro
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
primárními	primární	k2eAgInPc7d1	primární
prostředky	prostředek	k1gInPc7	prostředek
pro	pro	k7c4	pro
atomové	atomový	k2eAgFnPc4d1	atomová
přeměny	přeměna	k1gFnPc4	přeměna
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
dosažení	dosažení	k1gNnSc4	dosažení
efektivní	efektivní	k2eAgNnSc4d1	efektivní
štěpné	štěpný	k2eAgFnPc4d1	štěpná
jaderné	jaderný	k2eAgFnPc4d1	jaderná
reakce	reakce	k1gFnPc4	reakce
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
neutrony	neutron	k1gInPc1	neutron
zachyceny	zachycen	k2eAgInPc1d1	zachycen
štěpení	štěpení	k1gNnSc4	štěpení
schopným	schopný	k2eAgNnSc7d1	schopné
jádrem	jádro	k1gNnSc7	jádro
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
rozdělí	rozdělit	k5eAaPmIp3nS	rozdělit
a	a	k8xC	a
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
další	další	k2eAgInPc4d1	další
neutrony	neutron	k1gInPc4	neutron
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
většiny	většina	k1gFnSc2	většina
jaderných	jaderný	k2eAgInPc2d1	jaderný
reaktorů	reaktor	k1gInPc2	reaktor
není	být	k5eNaImIp3nS	být
jaderné	jaderný	k2eAgNnSc4d1	jaderné
palivo	palivo	k1gNnSc4	palivo
dostatečně	dostatečně	k6eAd1	dostatečně
čisté	čistý	k2eAgFnPc1d1	čistá
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
schopno	schopen	k2eAgNnSc1d1	schopno
zachytit	zachytit	k5eAaPmF	zachytit
rychlé	rychlý	k2eAgInPc4d1	rychlý
neutrony	neutron	k1gInPc4	neutron
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
řetězové	řetězový	k2eAgFnSc2d1	řetězová
reakce	reakce	k1gFnSc2	reakce
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
kvůli	kvůli	k7c3	kvůli
malému	malý	k2eAgInSc3d1	malý
účinnému	účinný	k2eAgInSc3d1	účinný
průřezu	průřez	k1gInSc3	průřez
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
použít	použít	k5eAaPmF	použít
neutronový	neutronový	k2eAgInSc4d1	neutronový
moderátor	moderátor	k1gInSc4	moderátor
pro	pro	k7c4	pro
zpomalení	zpomalení	k1gNnSc4	zpomalení
rychlých	rychlý	k2eAgInPc2d1	rychlý
neutronů	neutron	k1gInPc2	neutron
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
neutronů	neutron	k1gInPc2	neutron
teplých	teplý	k2eAgInPc2d1	teplý
pro	pro	k7c4	pro
dosažení	dosažení	k1gNnSc4	dosažení
vhodné	vhodný	k2eAgFnSc2d1	vhodná
absorpce	absorpce	k1gFnSc2	absorpce
<g/>
.	.	kIx.	.
</s>
<s>
Běžné	běžný	k2eAgInPc1d1	běžný
neutronové	neutronový	k2eAgInPc1d1	neutronový
moderátory	moderátor	k1gInPc1	moderátor
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
grafit	grafit	k1gInSc4	grafit
<g/>
,	,	kIx,	,
obyčejnou	obyčejný	k2eAgFnSc4d1	obyčejná
(	(	kIx(	(
<g/>
lehkou	lehký	k2eAgFnSc4d1	lehká
<g/>
)	)	kIx)	)
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
těžkou	těžký	k2eAgFnSc4d1	těžká
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
málo	málo	k4c1	málo
reaktorů	reaktor	k1gInPc2	reaktor
a	a	k8xC	a
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zbraní	zbraň	k1gFnPc2	zbraň
pracuje	pracovat	k5eAaImIp3nS	pracovat
s	s	k7c7	s
rychlými	rychlý	k2eAgInPc7d1	rychlý
neutrony	neutron	k1gInPc7	neutron
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
určité	určitý	k2eAgFnPc4d1	určitá
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
návrhu	návrh	k1gInSc6	návrh
a	a	k8xC	a
použitém	použitý	k2eAgNnSc6d1	Použité
palivu	palivo	k1gNnSc6	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Beryllium	Beryllium	k1gNnSc1	Beryllium
je	být	k5eAaImIp3nS	být
specifický	specifický	k2eAgInSc1d1	specifický
prvek	prvek	k1gInSc1	prvek
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
schopnost	schopnost	k1gFnSc4	schopnost
působit	působit	k5eAaImF	působit
na	na	k7c4	na
neutrony	neutron	k1gInPc4	neutron
jako	jako	k8xS	jako
reflektor	reflektor	k1gInSc1	reflektor
nebo	nebo	k8xC	nebo
čočka	čočka	k1gFnSc1	čočka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
použití	použití	k1gNnSc4	použití
menšího	malý	k2eAgNnSc2d2	menší
množství	množství	k1gNnSc2	množství
štěpného	štěpný	k2eAgInSc2d1	štěpný
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
primární	primární	k2eAgInSc4d1	primární
technický	technický	k2eAgInSc4d1	technický
vývoj	vývoj	k1gInSc4	vývoj
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
neutronových	neutronový	k2eAgFnPc2d1	neutronová
bomb	bomba	k1gFnPc2	bomba
<g/>
.	.	kIx.	.
</s>
</p>
