<s>
Zelená	zelený	k2eAgFnSc1d1	zelená
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
většina	většina	k1gFnSc1	většina
rostlin	rostlina	k1gFnPc2	rostlina
má	mít	k5eAaImIp3nS	mít
zelenou	zelený	k2eAgFnSc4d1	zelená
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
přítomností	přítomnost	k1gFnSc7	přítomnost
chlorofylu	chlorofyl	k1gInSc2	chlorofyl
<g/>
.	.	kIx.	.
</s>
