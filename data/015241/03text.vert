<s>
Strana	strana	k1gFnSc1
evropské	evropský	k2eAgFnSc2d1
levice	levice	k1gFnSc2
</s>
<s>
Party	party	k1gFnSc1
of	of	k?
the	the	k?
European	European	k1gMnSc1
LeftParti	LeftPart	k1gMnPc1
de	de	k?
la	la	k1gNnSc2
Gauche	Gauche	k1gFnPc2
EuropéennePartei	EuropéenneParte	k1gFnSc2
der	drát	k5eAaImRp2nS
Europäischen	Europäischen	k1gInSc4
Linken	Linken	k1gInSc1
Zkratka	zkratka	k1gFnSc1
</s>
<s>
PEL	pel	k1gInSc4
Datum	datum	k1gInSc1
založení	založení	k1gNnSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2004	#num#	k4
Předseda	předseda	k1gMnSc1
</s>
<s>
Heinz	Heinz	k1gInSc4
Bierbaum	Bierbaum	k1gInSc1
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Square	square	k1gInSc1
de	de	k?
Meeû	Meeû	k1gInSc4
251000	#num#	k4
Brusel	Brusel	k1gInSc1
<g/>
,	,	kIx,
Belgie	Belgie	k1gFnSc1
Ideologie	ideologie	k1gFnSc1
</s>
<s>
demokratický	demokratický	k2eAgInSc1d1
socialismus	socialismus	k1gInSc1
(	(	kIx(
<g/>
majoritní	majoritní	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
eurokomunismusekosocialismusantikapitalismus	eurokomunismusekosocialismusantikapitalismus	k1gInSc1
Politická	politický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
EP	EP	kA
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
sjednocená	sjednocený	k2eAgFnSc1d1
levice	levice	k1gFnSc1
a	a	k8xC
Severská	severský	k2eAgFnSc1d1
zelená	zelený	k2eAgFnSc1d1
levice	levice	k1gFnSc1
Barvy	barva	k1gFnSc2
</s>
<s>
rudá	rudý	k2eAgFnSc1d1
Oficiální	oficiální	k2eAgFnSc1d1
web	web	k1gInSc4
</s>
<s>
european-left	european-left	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
</s>
<s>
Strana	strana	k1gFnSc1
evropské	evropský	k2eAgFnSc2d1
levice	levice	k1gFnSc2
(	(	kIx(
<g/>
PEL	pel	k1gInSc1
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
Party	party	k1gFnSc1
of	of	k?
the	the	k?
European	European	k1gMnSc1
Left	Left	k1gMnSc1
<g/>
,	,	kIx,
německy	německy	k6eAd1
Partei	Parte	k1gMnPc1
der	drát	k5eAaImRp2nS
Europäischen	Europäischna	k1gFnPc2
Linken	Linken	k1gInSc1
<g/>
,	,	kIx,
francouzsky	francouzsky	k6eAd1
Parti	Parť	k1gFnPc1
de	de	k?
la	la	k1gNnSc1
Gauche	Gauchus	k1gMnSc5
Européenne	Européenn	k1gMnSc5
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
evropská	evropský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
sdružující	sdružující	k2eAgFnSc1d1
převážně	převážně	k6eAd1
rudo-zelené	rudo-zelený	k2eAgFnPc4d1
a	a	k8xC
eurokomunistické	eurokomunistický	k2eAgFnPc4d1
strany	strana	k1gFnPc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
deklaruje	deklarovat	k5eAaBmIp3nS
jak	jak	k6eAd1
odpor	odpor	k1gInSc4
ke	k	k7c3
kapitalismu	kapitalismus	k1gInSc3
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
i	i	k9
ke	k	k7c3
stalinismu	stalinismus	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strana	strana	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
v	v	k7c6
Římě	Řím	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řádným	řádný	k2eAgMnSc7d1
českým	český	k2eAgMnSc7d1
zástupcem	zástupce	k1gMnSc7
je	být	k5eAaImIp3nS
Levice	levice	k1gFnSc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
KSČM	KSČM	kA
zastává	zastávat	k5eAaImIp3nS
pouze	pouze	k6eAd1
status	status	k1gInSc4
pozorovatele	pozorovatel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Členové	člen	k1gMnPc1
</s>
<s>
ZeměČeský	ZeměČeský	k2eAgInSc1d1
názevOriginální	názevOriginální	k2eAgInSc1d1
názevZkratkaWeb	názevZkratkaWba	k1gFnPc2
</s>
<s>
Belgie	Belgie	k1gFnSc1
BelgieKomunistická	BelgieKomunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
Flandry	Flandry	k1gInPc1
<g/>
)	)	kIx)
<g/>
Parti	Parť	k1gFnPc1
CommunistePCparticommuniste	CommunistePCparticommunist	k1gMnSc5
<g/>
.	.	kIx.
<g/>
be	be	k?
</s>
<s>
Belgie	Belgie	k1gFnSc1
BelgieKomunistická	BelgieKomunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
Valonsko	Valonsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
Kommunistische	Kommunistische	k1gNnSc1
PartijKPkp-online	PartijKPkp-onlin	k1gInSc5
<g/>
.	.	kIx.
<g/>
be	be	k?
</s>
<s>
Bělorusko	Bělorusko	k1gNnSc4
BěloruskoBěloruská	běloruskoběloruský	k2eAgFnSc1d1
sjednocená	sjednocený	k2eAgFnSc1d1
levicová	levicový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
"	"	kIx"
<g/>
Spravedlivý	spravedlivý	k2eAgInSc1d1
svět	svět	k1gInSc1
<g/>
"	"	kIx"
<g/>
Б	Б	k?
п	п	k?
а	а	k?
<g/>
’	’	k?
<g/>
я	я	k?
л	л	k?
«	«	k?
<g/>
С	С	k?
с	с	k?
<g/>
»	»	k?
<g/>
Б	Б	k?
п	п	k?
л	л	k?
«	«	k?
<g/>
С	С	k?
м	м	k?
<g/>
»	»	k?
<g/>
spravmir	spravmir	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1
BulharskoBulharská	bulharskobulharský	k2eAgNnPc4d1
leviceБ	leviceБ	k?
л	л	k1gNnPc4
<g/>
.	.	kIx.
<g/>
org	org	k?
</s>
<s>
Česko	Česko	k1gNnSc1
ČeskoLeviceLeviceLevicejsmelevice	ČeskoLeviceLeviceLevicejsmelevice	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Dánsko	Dánsko	k1gNnSc4
DánskoJednotná	dánskojednotný	k2eAgFnSc1d1
kandidátka	kandidátka	k1gFnSc1
–	–	k?
Rudo-zeleníEnhedslisten	Rudo-zeleníEnhedslisten	k2eAgInSc4d1
–	–	k?
De	De	k?
Rø	Rø	k2eAgInSc4d1
<g/>
.	.	kIx.
<g/>
dk	dk	k?
</s>
<s>
Estonsko	Estonsko	k1gNnSc4
EstonskoEstonská	estonskoestonský	k2eAgFnSc1d1
sjednocená	sjednocený	k2eAgFnSc1d1
levicová	levicový	k2eAgFnSc1d1
stranaEestimaa	stranaEestimaa	k1gFnSc1
Ühendatud	Ühendatuda	k1gFnPc2
VasakparteiEVPvasakpartei	VasakparteiEVPvasakparte	k1gFnSc2
<g/>
.	.	kIx.
<g/>
ee	ee	k?
</s>
<s>
Finsko	Finsko	k1gNnSc4
FinskoKomunistická	finskokomunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
FinskaSuomen	FinskaSuomen	k2eAgInSc1d1
Kommunistinen	Kommunistinen	k2eAgInSc1d1
PuolueSKPskp	PuolueSKPskp	k1gInSc1
<g/>
.	.	kIx.
<g/>
fi	fi	k0
</s>
<s>
Finsko	Finsko	k1gNnSc1
FinskoSvaz	FinskoSvaz	k1gInSc1
leviceVasemmistoliittoVas	leviceVasemmistoliittoVas	k1gMnSc1
<g/>
.	.	kIx.
<g/>
vasemmisto	vasemmista	k1gMnSc5
<g/>
.	.	kIx.
<g/>
fi	fi	k0
</s>
<s>
Francie	Francie	k1gFnSc1
FrancieFrancouzská	FrancieFrancouzský	k2eAgFnSc1d1
komunistická	komunistický	k2eAgFnSc1d1
stranaParti	stranaPart	k1gMnPc1
communiste	communist	k1gMnSc5
françaisPCFpcf	françaisPCFpcf	k1gInSc1
<g/>
.	.	kIx.
<g/>
fr	fr	k0
</s>
<s>
Itálie	Itálie	k1gFnSc1
ItálieStrana	ItálieStrana	k1gFnSc1
komunistické	komunistický	k2eAgFnSc2d1
obnovyPartito	obnovyPartita	k1gMnSc5
della	dell	k1gMnSc2
Rifondazione	Rifondazion	k1gInSc5
ComunistaPRCrifondazione	ComunistaPRCrifondazion	k1gInSc5
<g/>
.	.	kIx.
<g/>
it	it	k?
</s>
<s>
Lucembursko	Lucembursko	k1gNnSc1
LucemburskoLeviceDéi	LucemburskoLeviceDé	k1gFnSc2
Lénk	Lénka	k1gFnPc2
/	/	kIx~
La	la	k1gNnSc1
Gauche	Gauch	k1gFnSc2
/	/	kIx~
Die	Die	k1gMnSc1
Linkendei-lenk	Linkendei-lenk	k1gMnSc1
<g/>
.	.	kIx.
<g/>
lu	lu	k?
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc4
MaďarskoDělnická	maďarskodělnický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Maďarska	Maďarsko	k1gNnSc2
2006	#num#	k4
<g/>
Magyarországi	Magyarországe	k1gFnSc4
Munkáspárt	Munkáspárta	k1gFnPc2
2006	#num#	k4
<g/>
munkaspart-	munkaspart-	k?
<g/>
2006	#num#	k4
<g/>
.	.	kIx.
<g/>
hu	hu	k0
</s>
<s>
Moldavsko	Moldavsko	k1gNnSc1
MoldavskoStrana	MoldavskoStrana	k1gFnSc1
komunistů	komunista	k1gMnPc2
Moldavské	moldavský	k2eAgFnSc2d1
republikyPartidul	republikyPartidout	k5eAaPmAgMnS
Comuniș	Comuniș	k1gMnSc1
din	din	k1gInSc4
Republica	Republic	k1gInSc2
MoldovaPCRMpcrm	MoldovaPCRMpcrm	k1gInSc1
<g/>
.	.	kIx.
<g/>
md	md	k?
</s>
<s>
Německo	Německo	k1gNnSc1
NěmeckoLeviceDie	NěmeckoLeviceDie	k1gFnSc2
Linkedie-linke	Linkedie-link	k1gFnSc2
<g/>
.	.	kIx.
<g/>
de	de	k?
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
PortugalskoLevý	portugalskolevý	k2eAgMnSc1d1
blokBloco	blokBloco	k1gMnSc1
de	de	k?
EsquerdaBEbloco	EsquerdaBEbloco	k1gMnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
</s>
<s>
Rakousko	Rakousko	k1gNnSc4
RakouskoKomunistická	rakouskokomunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
RakouskaKommunistische	RakouskaKommunistisch	k1gFnSc2
Partei	Parte	k1gFnSc2
ÖsterreichsKPÖkpoe	ÖsterreichsKPÖkpo	k1gFnSc2
<g/>
.	.	kIx.
<g/>
at	at	k?
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc4
RumunskoRumunská	rumunskorumunský	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
stranaPartidul	stranaPartidula	k1gFnPc2
Socialist	socialist	k1gMnSc1
RomânPSRpsr	RomânPSRpsr	k1gMnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
.	.	kIx.
<g/>
ro	ro	k?
</s>
<s>
Řecko	Řecko	k1gNnSc1
ŘeckoSYRIZA	ŘeckoSYRIZA	k1gFnSc2
-	-	kIx~
Koalice	koalice	k1gFnSc1
radikální	radikální	k2eAgFnSc1d1
leviceΣ	leviceΣ	k?
Ρ	Ρ	k?
Α	Α	k?
<g/>
.	.	kIx.
<g/>
Ρ	Ρ	k?
<g/>
.	.	kIx.
<g/>
Α	Α	k?
<g/>
syriza	syriza	k1gFnSc1
<g/>
.	.	kIx.
<g/>
gr	gr	k?
</s>
<s>
Španělsko	Španělsko	k1gNnSc4
ŠpanělskoKomunistická	španělskokomunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
ŠpanělskaPartido	ŠpanělskaPartida	k1gFnSc5
Comunista	Comunista	k1gMnSc1
de	de	k?
Españ	Españ	k1gFnSc1
<g/>
.	.	kIx.
<g/>
es	es	k1gNnSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc4
ŠpanělskoSjednocená	španělskosjednocený	k2eAgFnSc1d1
a	a	k8xC
alternativní	alternativní	k2eAgFnSc1d1
leviceEsquerra	leviceEsquerra	k1gFnSc1
Unida	Unida	k1gFnSc1
i	i	k8xC
AlternativaEUiAeuia	AlternativaEUiAeuia	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cat	cat	k?
</s>
<s>
Španělsko	Španělsko	k1gNnSc4
ŠpanělskoSpojená	španělskospojený	k2eAgFnSc1d1
leviceIzquierda	leviceIzquierda	k1gFnSc1
UnidaIUizquierda-unida	UnidaIUizquierda-unida	k1gFnSc1
<g/>
.	.	kIx.
<g/>
es	es	k1gNnSc1
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc4
ŠvýcarskoŠvýcarská	švýcarskošvýcarský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
prácePartei	práceParte	k1gFnSc2
der	drát	k5eAaImRp2nS
Arbeit	Arbeit	k1gInSc1
der	drát	k5eAaImRp2nS
Schweiz	Schweiz	k1gInSc1
/	/	kIx~
Parti	Parti	k1gNnSc1
Suisse	Suisse	k1gFnSc2
du	du	k?
TravailPdAPST	TravailPdAPST	k1gMnSc1
<g/>
‑	‑	k?
<g/>
POPpda	POPpda	k1gMnSc1
<g/>
.	.	kIx.
<g/>
chpst	chpst	k1gMnSc1
<g/>
.	.	kIx.
<g/>
ch	ch	k0
</s>
<s>
Turecko	Turecko	k1gNnSc1
TureckoLevicová	tureckolevicový	k2eAgFnSc1d1
stranaSol	stranaSol	k1gInSc4
PartiÖDPsolparti	PartiÖDPsolpart	k1gMnPc1
<g/>
.	.	kIx.
<g/>
org	org	k?
</s>
<s>
Pozorovatelé	pozorovatel	k1gMnPc1
</s>
<s>
ZeměČeský	ZeměČeský	k2eAgInSc1d1
názevOriginální	názevOriginální	k2eAgInSc1d1
názevZkratkaWeb	názevZkratkaWba	k1gFnPc2
</s>
<s>
Belgie	Belgie	k1gFnSc1
BelgieJiná	BelgieJiná	k1gFnSc1
leviceUne	leviceUn	k1gMnSc5
Autre	Autr	k1gMnSc5
GaucheUAGuneautregauche	GaucheUAGuneautregauchus	k1gMnSc5
<g/>
.	.	kIx.
<g/>
be	be	k?
</s>
<s>
Česko	Česko	k1gNnSc4
ČeskoKomunistická	českokomunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Čech	Čechy	k1gFnPc2
a	a	k8xC
MoravyKomunistická	MoravyKomunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Čech	Čechy	k1gFnPc2
a	a	k8xC
MoravyKSČMkscm	MoravyKSČMkscma	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Itálie	Itálie	k1gFnSc1
ItálieItalská	ItálieItalský	k2eAgFnSc1d1
leviceSinistra	leviceSinistra	k1gFnSc1
ItalianaSIsinistraitaliana	ItalianaSIsinistraitaliana	k1gFnSc1
<g/>
.	.	kIx.
<g/>
si	se	k3xPyFc3
</s>
<s>
Kypr	Kypr	k1gInSc1
KyprPokroková	KyprPokrokový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
pracujícího	pracující	k2eAgInSc2d1
liduΑ	liduΑ	k?
Κ	Κ	k?
Ε	Ε	k?
Λ	Λ	k?
<g/>
.	.	kIx.
<g/>
Κ	Κ	k?
<g/>
akel	akel	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
.	.	kIx.
<g/>
cy	cy	k?
</s>
<s>
Severní	severní	k2eAgInSc1d1
Kypr	Kypr	k1gInSc1
Severní	severní	k2eAgFnSc1d1
KyprNová	KyprNová	k1gFnSc1
kyperská	kyperský	k2eAgFnSc1d1
stranaYeni	stranaYit	k5eAaBmNgMnP,k5eAaPmNgMnP,k5eAaImNgMnP
Kı	Kı	k1gInSc4
PartisiYKPykp	PartisiYKPykp	k1gInSc4
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
.	.	kIx.
<g/>
cy	cy	k?
</s>
<s>
Severní	severní	k2eAgInSc1d1
Kypr	Kypr	k1gInSc1
Severní	severní	k2eAgFnSc1d1
KyprSjednocená	KyprSjednocený	k2eAgFnSc1d1
kyperská	kyperský	k2eAgFnSc1d1
stranaBirleşik	stranaBirleşika	k1gFnPc2
Kı	Kı	k1gInSc4
PartisiBKPbirlesikkibrispartisi	PartisiBKPbirlesikkibrispartise	k1gFnSc4
<g/>
.	.	kIx.
<g/>
org	org	k?
</s>
<s>
Slovensko	Slovensko	k1gNnSc4
SlovenskoKomunistická	slovenskokomunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
SlovenskaKomunistická	SlovenskaKomunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
SlovenskaKSSkss	SlovenskaKSSkssa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
sk	sk	k?
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Propagační	propagační	k2eAgInPc1d1
nálepky	nálepek	k1gInPc1
Strany	strana	k1gFnSc2
evropské	evropský	k2eAgFnSc2d1
levice	levice	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
www.european-left.org	www.european-left.org	k1gMnSc1
</s>
<s>
Dopis	dopis	k1gInSc1
KS	ks	kA
Řecka	Řecko	k1gNnSc2
komunistickým	komunistický	k2eAgFnPc3d1
a	a	k8xC
dělnickým	dělnický	k2eAgFnPc3d1
stranám	strana	k1gFnPc3
Evropy	Evropa	k1gFnSc2
ke	k	k7c3
sjezdu	sjezd	k1gInSc3
tzv.	tzv.	kA
Strany	strana	k1gFnSc2
evropské	evropský	k2eAgFnSc2d1
levice	levice	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Politika	politika	k1gFnSc1
EU	EU	kA
–	–	k?
politické	politický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
EU	EU	kA
<g/>
,	,	kIx,
skupiny	skupina	k1gFnPc1
v	v	k7c6
EP	EP	kA
a	a	k8xC
svazy	svaz	k1gInPc1
stran	strana	k1gFnPc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
Politické	politický	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
EP	EP	kA
</s>
<s>
Současné	současný	k2eAgNnSc1d1
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
EPP	EPP	kA
(	(	kIx(
<g/>
182	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pokrokové	pokrokový	k2eAgNnSc4d1
spojenectví	spojenectví	k1gNnSc4
socialistů	socialist	k1gMnPc2
a	a	k8xC
demokratů	demokrat	k1gMnPc2
S	s	k7c7
<g/>
&	&	k?
<g/>
D	D	kA
(	(	kIx(
<g/>
153	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Obnova	obnova	k1gFnSc1
Evropy	Evropa	k1gFnSc2
Renew	Renew	k1gFnSc2
(	(	kIx(
<g/>
108	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Zelení	Zelený	k1gMnPc1
/	/	kIx~
Evropská	evropský	k2eAgFnSc1d1
svobodná	svobodný	k2eAgFnSc1d1
aliance	aliance	k1gFnSc1
Greens	Greens	k1gInSc1
<g/>
–	–	k?
<g/>
EFA	EFA	kA
(	(	kIx(
<g/>
74	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Identita	identita	k1gFnSc1
a	a	k8xC
demokracie	demokracie	k1gFnSc1
ID	Ida	k1gFnPc2
(	(	kIx(
<g/>
73	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Evropští	evropský	k2eAgMnPc1d1
konzervativci	konzervativec	k1gMnPc1
a	a	k8xC
reformisté	reformista	k1gMnPc1
ECR	ECR	kA
(	(	kIx(
<g/>
62	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Evropská	evropský	k2eAgFnSc1d1
sjednocená	sjednocený	k2eAgFnSc1d1
levice	levice	k1gFnSc1
a	a	k8xC
Severská	severský	k2eAgFnSc1d1
zelená	zelený	k2eAgFnSc1d1
levice	levice	k1gFnSc1
GUE-NGL	GUE-NGL	k1gFnSc2
(	(	kIx(
<g/>
41	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Nezařazení	nezařazení	k1gNnSc1
NI	on	k3xPp3gFnSc4
(	(	kIx(
<g/>
55	#num#	k4
<g/>
)	)	kIx)
Bývalé	bývalý	k2eAgNnSc1d1
</s>
<s>
Evropa	Evropa	k1gFnSc1
svobody	svoboda	k1gFnSc2
a	a	k8xC
přímé	přímý	k2eAgFnSc2d1
demokracie	demokracie	k1gFnSc2
EFDD	EFDD	kA
•	•	k?
Evropa	Evropa	k1gFnSc1
národů	národ	k1gInPc2
a	a	k8xC
svobody	svoboda	k1gFnSc2
ENF	ENF	kA
•	•	k?
Aliance	aliance	k1gFnSc1
liberálů	liberál	k1gMnPc2
a	a	k8xC
demokratů	demokrat	k1gMnPc2
pro	pro	k7c4
Evropu	Evropa	k1gFnSc4
ALDE	ALDE	kA
•	•	k?
Identita	identita	k1gFnSc1
<g/>
,	,	kIx,
tradice	tradice	k1gFnSc1
<g/>
,	,	kIx,
suverenita	suverenita	k1gFnSc1
•	•	k?
Nezávislost	nezávislost	k1gFnSc1
/	/	kIx~
Demokracie	demokracie	k1gFnSc1
•	•	k?
Unie	unie	k1gFnSc2
pro	pro	k7c4
Evropu	Evropa	k1gFnSc4
národů	národ	k1gInPc2
</s>
<s>
Politické	politický	k2eAgFnPc1d1
strany	strana	k1gFnPc1
EU	EU	kA
</s>
<s>
Uznané	uznaný	k2eAgNnSc1d1
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
EDP	EDP	kA
•	•	k?
Aliance	aliance	k1gFnSc1
liberálů	liberál	k1gMnPc2
a	a	k8xC
demokratů	demokrat	k1gMnPc2
pro	pro	k7c4
Evropu	Evropa	k1gFnSc4
ALDE	ALDE	kA
•	•	k?
Evropská	evropský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
EPP	EPP	kA
•	•	k?
Evropská	evropský	k2eAgFnSc1d1
svobodná	svobodný	k2eAgFnSc1d1
aliance	aliance	k1gFnSc1
EFA	EFA	kA
•	•	k?
Strana	strana	k1gFnSc1
evropské	evropský	k2eAgFnSc2d1
levice	levice	k1gFnSc2
PEL	pel	k1gInSc1
•	•	k?
Strana	strana	k1gFnSc1
evropských	evropský	k2eAgMnPc2d1
socialistů	socialist	k1gMnPc2
PES	pes	k1gMnSc1
•	•	k?
Evropská	evropský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
zelených	zelená	k1gFnPc2
EGP	EGP	kA
•	•	k?
Strana	strana	k1gFnSc1
evropských	evropský	k2eAgMnPc2d1
konzervativců	konzervativec	k1gMnPc2
a	a	k8xC
reformistů	reformista	k1gMnPc2
ECR	ECR	kA
Party	party	k1gFnSc1
•	•	k?
Strana	strana	k1gFnSc1
identity	identita	k1gFnSc2
a	a	k8xC
demokracie	demokracie	k1gFnSc2
ID	Ida	k1gFnPc2
Party	parta	k1gFnSc2
•	•	k?
Evropské	evropský	k2eAgNnSc1d1
křesťanské	křesťanský	k2eAgNnSc1d1
politické	politický	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
ECPM	ECPM	kA
Ostatní	ostatní	k2eAgInPc1d1
a	a	k8xC
zaniklé	zaniklý	k2eAgInPc1d1
</s>
<s>
Aliance	aliance	k1gFnSc1
za	za	k7c4
mír	mír	k1gInSc4
a	a	k8xC
svobodu	svoboda	k1gFnSc4
APF	APF	kA
•	•	k?
Aliance	aliance	k1gFnSc2
severské	severský	k2eAgFnSc2d1
zelené	zelený	k2eAgFnSc2d1
levice	levice	k1gFnSc2
NGLA	NGLA	kA
•	•	k?
Evropská	evropský	k2eAgFnSc1d1
antikapitalistická	antikapitalistický	k2eAgFnSc1d1
levice	levice	k1gFnSc1
EACL	EACL	kA
•	•	k?
Iniciativa	iniciativa	k1gFnSc1
komunistických	komunistický	k2eAgFnPc2d1
a	a	k8xC
dělnických	dělnický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
INITIATIVE	INITIATIVE	kA
•	•	k?
Evropská	evropský	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
PPEU	PPEU	kA
•	•	k?
Aliance	aliance	k1gFnSc2
evropských	evropský	k2eAgNnPc2d1
národních	národní	k2eAgNnPc2d1
hnutí	hnutí	k1gNnPc2
•	•	k?
Aliance	aliance	k1gFnSc2
pro	pro	k7c4
Evropu	Evropa	k1gFnSc4
národů	národ	k1gInPc2
•	•	k?
Aliance	aliance	k1gFnSc1
za	za	k7c4
přímou	přímý	k2eAgFnSc4d1
demokracii	demokracie	k1gFnSc4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
•	•	k?
Euronat	Euronat	k1gFnSc6
•	•	k?
Evropská	evropský	k2eAgFnSc1d1
federalistická	federalistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
•	•	k?
Evropská	evropský	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
•	•	k?
Hnutí	hnutí	k1gNnSc1
za	za	k7c4
evropskou	evropský	k2eAgFnSc4d1
reformu	reforma	k1gFnSc4
•	•	k?
Volt	Volta	k1gFnPc2
Europa	Europa	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
2009059244	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
150330054	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
2009059244	#num#	k4
</s>
