<s>
Strana	strana	k1gFnSc1
evropské	evropský	k2eAgFnSc2d1
levice	levice	k1gFnSc2
(	(	kIx(
<g/>
PEL	pel	k1gInSc1
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
Party	party	k1gFnSc1
of	of	k?
the	the	k?
European	European	k1gMnSc1
Left	Left	k1gMnSc1
<g/>
,	,	kIx,
německy	německy	k6eAd1
Partei	Parte	k1gMnPc1
der	drát	k5eAaImRp2nS
Europäischen	Europäischna	k1gFnPc2
Linken	Linken	k1gInSc1
<g/>
,	,	kIx,
francouzsky	francouzsky	k6eAd1
Parti	Parť	k1gFnPc1
de	de	k?
la	la	k1gNnSc1
Gauche	Gauchus	k1gMnSc5
Européenne	Européenn	k1gMnSc5
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
evropská	evropský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
sdružující	sdružující	k2eAgFnSc1d1
převážně	převážně	k6eAd1
rudo-zelené	rudo-zelený	k2eAgFnPc4d1
a	a	k8xC
eurokomunistické	eurokomunistický	k2eAgFnPc4d1
strany	strana	k1gFnPc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
deklaruje	deklarovat	k5eAaBmIp3nS
jak	jak	k6eAd1
odpor	odpor	k1gInSc4
ke	k	k7c3
kapitalismu	kapitalismus	k1gInSc3
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
i	i	k9
ke	k	k7c3
stalinismu	stalinismus	k1gInSc3
<g/>
.	.	kIx.
</s>