<p>
<s>
(	(	kIx(	(
<g/>
951	[number]	k4	951
<g/>
)	)	kIx)	)
Gaspra	Gaspr	k1gInSc2	Gaspr
je	být	k5eAaImIp3nS	být
planetka	planetka	k1gFnSc1	planetka
hlavního	hlavní	k2eAgInSc2d1	hlavní
pásu	pás	k1gInSc2	pás
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
materiálu	materiál	k1gInSc2	materiál
je	být	k5eAaImIp3nS	být
řazena	řadit	k5eAaImNgFnS	řadit
do	do	k7c2	do
typu	typ	k1gInSc2	typ
S	s	k7c7	s
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
podle	podle	k7c2	podle
ukrajinského	ukrajinský	k2eAgNnSc2d1	ukrajinské
města	město	k1gNnSc2	město
Gaspra	Gaspr	k1gInSc2	Gaspr
na	na	k7c6	na
Krymu	Krym	k1gInSc6	Krym
<g/>
.	.	kIx.	.
</s>
<s>
Krátery	kráter	k1gInPc1	kráter
na	na	k7c4	na
Gaspře	Gaspř	k1gFnPc4	Gaspř
jsou	být	k5eAaImIp3nP	být
pojmenovány	pojmenovat	k5eAaPmNgFnP	pojmenovat
podle	podle	k7c2	podle
lázeňských	lázeňský	k2eAgNnPc2d1	lázeňské
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
nepravidelný	pravidelný	k2eNgInSc1d1	nepravidelný
tvar	tvar	k1gInSc1	tvar
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
18	[number]	k4	18
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
×	×	k?	×
<g/>
9	[number]	k4	9
km	km	kA	km
a	a	k8xC	a
dobu	doba	k1gFnSc4	doba
rotace	rotace	k1gFnSc1	rotace
7	[number]	k4	7
h.	h.	k?	h.
Tepelná	tepelný	k2eAgFnSc1d1	tepelná
setrvačnost	setrvačnost	k1gFnSc1	setrvačnost
povrchu	povrch	k1gInSc2	povrch
naměřená	naměřený	k2eAgFnSc1d1	naměřená
sondou	sonda	k1gFnSc7	sonda
Galileo	Galilea	k1gFnSc5	Galilea
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
slabou	slabý	k2eAgFnSc7d1	slabá
<g/>
,	,	kIx,	,
asi	asi	k9	asi
1	[number]	k4	1
metrů	metr	k1gInPc2	metr
silné	silný	k2eAgFnPc4d1	silná
vrstvu	vrstva	k1gFnSc4	vrstva
regolitu	regolit	k1gInSc2	regolit
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgInS	objevit
ji	on	k3xPp3gFnSc4	on
dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1916	[number]	k4	1916
G.	G.	kA	G.
<g/>
N.	N.	kA	N.
Neujmin	Neujmin	k1gInSc1	Neujmin
<g/>
.	.	kIx.	.
</s>
<s>
Gaspra	Gaspra	k1gFnSc1	Gaspra
má	mít	k5eAaImIp3nS	mít
600	[number]	k4	600
pozorovaných	pozorovaný	k2eAgInPc2d1	pozorovaný
kráterů	kráter	k1gInPc2	kráter
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
největší	veliký	k2eAgInSc4d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc1	průměr
1,5	[number]	k4	1,5
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1991	[number]	k4	1991
ve	v	k7c4	v
22	[number]	k4	22
<g/>
:	:	kIx,	:
<g/>
36	[number]	k4	36
<g/>
:	:	kIx,	:
<g/>
40	[number]	k4	40
UT	UT	kA	UT
proletěla	proletět	k5eAaPmAgFnS	proletět
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
1600	[number]	k4	1600
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
Gaspry	Gaspra	k1gFnSc2	Gaspra
sonda	sonda	k1gFnSc1	sonda
Galileo	Galilea	k1gFnSc5	Galilea
<g/>
.	.	kIx.	.
</s>
<s>
Gaspra	Gaspra	k1gMnSc1	Gaspra
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
Flora	Flor	k1gInSc2	Flor
rodiny	rodina	k1gFnSc2	rodina
asteroidů	asteroid	k1gInPc2	asteroid
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
ze	z	k7c2	z
směsi	směs	k1gFnSc2	směs
kamene	kámen	k1gInSc2	kámen
a	a	k8xC	a
kovových	kovový	k2eAgInPc2d1	kovový
minerálů	minerál	k1gInPc2	minerál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc4	seznam
planetek	planetka	k1gFnPc2	planetka
751-1000	[number]	k4	751-1000
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
951	[number]	k4	951
Gaspra	Gaspr	k1gInSc2	Gaspr
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
951	[number]	k4	951
<g/>
)	)	kIx)	)
Gaspra	Gaspr	k1gInSc2	Gaspr
na	na	k7c6	na
webu	web	k1gInSc6	web
České	český	k2eAgFnSc2d1	Česká
astronomické	astronomický	k2eAgFnSc2d1	astronomická
společnosti	společnost	k1gFnSc2	společnost
</s>
</p>
