<s>
Archeologické	archeologický	k2eAgInPc1d1	archeologický
výzkumy	výzkum	k1gInPc1	výzkum
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Puerto	Puerta	k1gFnSc5	Puerta
Vallarta	Vallart	k1gMnSc2	Vallart
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
oblast	oblast	k1gFnSc1	oblast
byla	být	k5eAaImAgFnS	být
osídlena	osídlit	k5eAaPmNgFnS	osídlit
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
560	[number]	k4	560
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
.	.	kIx.	.
</s>
