<p>
<s>
Puerto	Puerta	k1gFnSc5	Puerta
Vallarta	Vallarta	k1gFnSc1	Vallarta
je	on	k3xPp3gNnSc4	on
město	město	k1gNnSc4	město
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Jalisco	Jalisco	k1gNnSc1	Jalisco
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc1d1	ležící
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
příjmení	příjmení	k1gNnSc2	příjmení
právníka	právník	k1gMnSc2	právník
a	a	k8xC	a
guvernéra	guvernér	k1gMnSc2	guvernér
státu	stát	k1gInSc2	stát
Jalisco	Jalisco	k1gMnSc1	Jalisco
Ignacio	Ignacio	k1gMnSc1	Ignacio
Vallarty	Vallarta	k1gFnSc2	Vallarta
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
spolubojovníkem	spolubojovník	k1gMnSc7	spolubojovník
mexického	mexický	k2eAgMnSc2d1	mexický
prezidenta	prezident	k1gMnSc2	prezident
Benito	Benit	k2eAgNnSc4d1	Benito
Juáreze	Juárez	k1gInSc5	Juárez
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc4	město
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gNnSc3	jeho
umístění	umístění	k1gNnSc1	umístění
u	u	k7c2	u
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
a	a	k8xC	a
řeky	řeka	k1gFnSc2	řeka
Ameca	Amec	k1gInSc2	Amec
a	a	k8xC	a
hezkému	hezký	k2eAgNnSc3d1	hezké
okolí	okolí	k1gNnSc3	okolí
je	být	k5eAaImIp3nS	být
populární	populární	k2eAgFnSc7d1	populární
turistickou	turistický	k2eAgFnSc7d1	turistická
destinací	destinace	k1gFnSc7	destinace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Archeologické	archeologický	k2eAgInPc1d1	archeologický
výzkumy	výzkum	k1gInPc1	výzkum
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Puerto	Puerta	k1gFnSc5	Puerta
Vallarta	Vallart	k1gMnSc2	Vallart
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
oblast	oblast	k1gFnSc1	oblast
byla	být	k5eAaImAgFnS	být
osídlena	osídlit	k5eAaPmNgFnS	osídlit
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
560	[number]	k4	560
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
900	[number]	k4	900
až	až	k6eAd1	až
1200	[number]	k4	1200
n.	n.	k?	n.
l.	l.	k?	l.
tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
většina	většina	k1gFnSc1	většina
území	území	k1gNnSc2	území
států	stát	k1gInPc2	stát
Nayarit	Nayarita	k1gFnPc2	Nayarita
a	a	k8xC	a
Michoacán	Michoacán	k2eAgInSc1d1	Michoacán
<g/>
,	,	kIx,	,
patřila	patřit	k5eAaImAgFnS	patřit
kultuře	kultura	k1gFnSc3	kultura
Aztatlán	Aztatlán	k2eAgInSc1d1	Aztatlán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
misijních	misijní	k2eAgFnPc6d1	misijní
kronikách	kronika	k1gFnPc6	kronika
a	a	k8xC	a
záznamech	záznam	k1gInPc6	záznam
dobyvatelů	dobyvatel	k1gMnPc2	dobyvatel
je	být	k5eAaImIp3nS	být
zdokumentováno	zdokumentovat	k5eAaPmNgNnS	zdokumentovat
mnoho	mnoho	k4c1	mnoho
bitev	bitva	k1gFnPc2	bitva
mezi	mezi	k7c7	mezi
místními	místní	k2eAgMnPc7d1	místní
obyvateli	obyvatel	k1gMnPc7	obyvatel
a	a	k8xC	a
přistěhovalci	přistěhovalec	k1gMnPc7	přistěhovalec
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1524	[number]	k4	1524
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
velká	velký	k2eAgFnSc1d1	velká
bitva	bitva	k1gFnSc1	bitva
mezi	mezi	k7c7	mezi
armádou	armáda	k1gFnSc7	armáda
Hernána	Hernán	k2eAgFnSc1d1	Hernána
Cortése	Cortése	k1gFnSc1	Cortése
a	a	k8xC	a
armádou	armáda	k1gFnSc7	armáda
10	[number]	k4	10
000	[number]	k4	000
až	až	k9	až
20	[number]	k4	20
000	[number]	k4	000
indiánů	indián	k1gMnPc2	indián
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
Cortés	Cortés	k1gInSc1	Cortés
získal	získat	k5eAaPmAgInS	získat
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
většinou	většina	k1gFnSc7	většina
údolí	údolí	k1gNnSc2	údolí
Ameca	Amec	k1gInSc2	Amec
<g/>
.	.	kIx.	.
</s>
<s>
Údolí	údolí	k1gNnSc1	údolí
bylo	být	k5eAaImAgNnS	být
nazýváno	nazývat	k5eAaImNgNnS	nazývat
Údolí	údolí	k1gNnSc4	údolí
vlajek	vlajka	k1gFnPc2	vlajka
(	(	kIx(	(
<g/>
Banderas	Banderas	k1gInSc1	Banderas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
barevných	barevný	k2eAgFnPc2d1	barevná
vlajek	vlajka	k1gFnPc2	vlajka
domorodců	domorodec	k1gMnPc2	domorodec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
však	však	k9	však
zůstali	zůstat	k5eAaPmAgMnP	zůstat
nepokořeni	pokořen	k2eNgMnPc1d1	nepokořen
místní	místní	k2eAgMnPc1d1	místní
indiáni	indián	k1gMnPc1	indián
Huicholi	Huichole	k1gFnSc4	Huichole
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgFnSc4d1	žijící
v	v	k7c6	v
nepřístupných	přístupný	k2eNgNnPc6d1	nepřístupné
údolích	údolí	k1gNnPc6	údolí
a	a	k8xC	a
na	na	k7c6	na
svazích	svah	k1gInPc6	svah
pohoří	pohořet	k5eAaPmIp3nS	pohořet
Sierra	Sierra	k1gFnSc1	Sierra
Madre	Madr	k1gInSc5	Madr
Occidental	Occidental	k1gMnSc1	Occidental
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
dnešních	dnešní	k2eAgInPc2d1	dnešní
států	stát	k1gInPc2	stát
Jalisco	Jalisco	k6eAd1	Jalisco
a	a	k8xC	a
Nayarit	Nayarit	k1gInSc4	Nayarit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zhruba	zhruba	k6eAd1	zhruba
200	[number]	k4	200
let	léto	k1gNnPc2	léto
po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
Cortése	Cortése	k1gFnSc2	Cortése
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1772	[number]	k4	1772
<g/>
)	)	kIx)	)
se	s	k7c7	s
františkánským	františkánský	k2eAgMnSc7d1	františkánský
misonářům	misonář	k1gMnPc3	misonář
podařilo	podařit	k5eAaPmAgNnS	podařit
vybudovat	vybudovat	k5eAaPmF	vybudovat
zde	zde	k6eAd1	zde
první	první	k4xOgFnPc1	první
katolické	katolický	k2eAgFnPc1d1	katolická
kaple	kaple	k1gFnPc1	kaple
a	a	k8xC	a
kostely	kostel	k1gInPc1	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
misionářská	misionářský	k2eAgFnSc1d1	misionářská
práce	práce	k1gFnSc1	práce
neměla	mít	k5eNaImAgFnS	mít
úspěch	úspěch	k1gInSc4	úspěch
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
o	o	k7c4	o
100	[number]	k4	100
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
františkáni	františkán	k1gMnPc1	františkán
opustili	opustit	k5eAaPmAgMnP	opustit
oblast	oblast	k1gFnSc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Norský	norský	k2eAgMnSc1d1	norský
badatel	badatel	k1gMnSc1	badatel
Carl	Carl	k1gMnSc1	Carl
Lumholtz	Lumholtz	k1gMnSc1	Lumholtz
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
navštívil	navštívit	k5eAaPmAgMnS	navštívit
indiány	indián	k1gMnPc4	indián
Huicholi	Huichole	k1gFnSc6	Huichole
roku	rok	k1gInSc2	rok
1890	[number]	k4	1890
poznamenal	poznamenat	k5eAaPmAgInS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gFnSc1	jejich
původní	původní	k2eAgFnSc1d1	původní
víra	víra	k1gFnSc1	víra
byla	být	k5eAaImAgFnS	být
zachována	zachovat	k5eAaPmNgFnS	zachovat
a	a	k8xC	a
nenašel	najít	k5eNaPmAgMnS	najít
žádnou	žádný	k3yNgFnSc4	žádný
stopu	stopa	k1gFnSc4	stopa
křesťanských	křesťanský	k2eAgInPc2d1	křesťanský
obřadů	obřad	k1gInPc2	obřad
<g/>
.	.	kIx.	.
</s>
<s>
Zavedení	zavedení	k1gNnSc1	zavedení
chovu	chov	k1gInSc2	chov
ovcí	ovce	k1gFnPc2	ovce
<g/>
,	,	kIx,	,
skotu	skot	k1gInSc2	skot
a	a	k8xC	a
železné	železný	k2eAgInPc4d1	železný
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
jen	jen	k9	jen
mírně	mírně	k6eAd1	mírně
zasáhlo	zasáhnout	k5eAaPmAgNnS	zasáhnout
do	do	k7c2	do
života	život	k1gInSc2	život
domorodců	domorodec	k1gMnPc2	domorodec
<g/>
.	.	kIx.	.
</s>
<s>
Jasnou	jasný	k2eAgFnSc7d1	jasná
stopou	stopa	k1gFnSc7	stopa
františkánů	františkán	k1gMnPc2	františkán
byl	být	k5eAaImAgMnS	být
tehdy	tehdy	k6eAd1	tehdy
rozšířený	rozšířený	k2eAgInSc4d1	rozšířený
zvyk	zvyk	k1gInSc4	zvyk
používat	používat	k5eAaImF	používat
barevné	barevný	k2eAgInPc4d1	barevný
skleněné	skleněný	k2eAgInPc4d1	skleněný
korálky	korálek	k1gInPc4	korálek
jako	jako	k8xC	jako
ozdoby	ozdoba	k1gFnPc4	ozdoba
a	a	k8xC	a
předměty	předmět	k1gInPc4	předmět
pro	pro	k7c4	pro
každodenní	každodenní	k2eAgNnSc4d1	každodenní
použití	použití	k1gNnSc4	použití
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přístav	přístav	k1gInSc1	přístav
sloužil	sloužit	k5eAaImAgInS	sloužit
pro	pro	k7c4	pro
galeony	galeona	k1gFnPc4	galeona
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mířily	mířit	k5eAaImAgFnP	mířit
z	z	k7c2	z
Manily	Manila	k1gFnSc2	Manila
přes	přes	k7c4	přes
Tichý	tichý	k2eAgInSc4d1	tichý
oceán	oceán	k1gInSc4	oceán
do	do	k7c2	do
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
obec	obec	k1gFnSc1	obec
Puerto	Puerta	k1gFnSc5	Puerta
Vallarta	Vallart	k1gMnSc4	Vallart
získala	získat	k5eAaPmAgFnS	získat
statut	statut	k1gInSc4	statut
obce	obec	k1gFnSc2	obec
a	a	k8xC	a
současné	současný	k2eAgNnSc4d1	současné
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgNnPc6	první
letech	léto	k1gNnPc6	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc6	století
většinu	většina	k1gFnSc4	většina
pozemků	pozemek	k1gInPc2	pozemek
v	v	k7c6	v
Puerto	Puerta	k1gFnSc5	Puerta
Vallarta	Vallart	k1gMnSc4	Vallart
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
americká	americký	k2eAgFnSc1d1	americká
těžební	těžební	k2eAgFnSc1d1	těžební
společnost	společnost	k1gFnSc1	společnost
Union	union	k1gInSc1	union
en	en	k?	en
Cuale	Cuale	k1gInSc1	Cuale
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
hospodářství	hospodářství	k1gNnSc2	hospodářství
==	==	k?	==
</s>
</p>
<p>
<s>
Lidé	člověk	k1gMnPc1	člověk
pracují	pracovat	k5eAaImIp3nP	pracovat
zejména	zejména	k9	zejména
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
služeb	služba	k1gFnPc2	služba
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
,	,	kIx,	,
stavebnictví	stavebnictví	k1gNnSc6	stavebnictví
<g/>
,	,	kIx,	,
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
oblastech	oblast	k1gFnPc6	oblast
výroby	výroba	k1gFnSc2	výroba
a	a	k8xC	a
věnují	věnovat	k5eAaPmIp3nP	věnovat
se	se	k3xPyFc4	se
rybolovu	rybolov	k1gInSc2	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
Pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
kukuřice	kukuřice	k1gFnSc1	kukuřice
<g/>
,	,	kIx,	,
čirok	čirok	k1gInSc1	čirok
<g/>
,	,	kIx,	,
fazole	fazole	k1gFnSc1	fazole
<g/>
,	,	kIx,	,
chilli	chilli	k1gNnSc1	chilli
<g/>
,	,	kIx,	,
meloun	meloun	k1gInSc1	meloun
<g/>
,	,	kIx,	,
tabák	tabák	k1gInSc1	tabák
<g/>
,	,	kIx,	,
mango	mango	k1gNnSc1	mango
<g/>
,	,	kIx,	,
banán	banán	k1gInSc1	banán
a	a	k8xC	a
avokádo	avokádo	k1gNnSc1	avokádo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Natáčení	natáčení	k1gNnPc2	natáčení
filmů	film	k1gInPc2	film
==	==	k?	==
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
Puerto	Puerta	k1gFnSc5	Puerta
Vallarta	Vallarta	k1gFnSc1	Vallarta
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
populární	populární	k2eAgFnSc1d1	populární
turistická	turistický	k2eAgFnSc1d1	turistická
destinace	destinace	k1gFnSc1	destinace
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
využito	využít	k5eAaPmNgNnS	využít
ve	v	k7c6	v
filmu	film	k1gInSc6	film
The	The	k1gMnSc1	The
Night	Night	k1gMnSc1	Night
of	of	k?	of
the	the	k?	the
Iguana	Iguana	k1gFnSc1	Iguana
s	s	k7c7	s
Richardem	Richard	k1gMnSc7	Richard
Burtonem	Burton	k1gInSc7	Burton
<g/>
.	.	kIx.	.
</s>
<s>
Burton	Burton	k1gInSc1	Burton
a	a	k8xC	a
Liz	liz	k1gInSc1	liz
Taylor	Taylora	k1gFnPc2	Taylora
dokonce	dokonce	k9	dokonce
koupili	koupit	k5eAaPmAgMnP	koupit
ve	v	k7c6	v
městě	město	k1gNnSc6	město
dům	dům	k1gInSc1	dům
<g/>
.	.	kIx.	.
</s>
<s>
Filmový	filmový	k2eAgInSc1d1	filmový
štáb	štáb	k1gInSc1	štáb
<g/>
,	,	kIx,	,
hvězdy	hvězda	k1gFnPc1	hvězda
Hollywoodu	Hollywood	k1gInSc2	Hollywood
a	a	k8xC	a
davy	dav	k1gInPc1	dav
novinářů	novinář	k1gMnPc2	novinář
přispěly	přispět	k5eAaPmAgInP	přispět
k	k	k7c3	k
popularizaci	popularizace	k1gFnSc3	popularizace
města	město	k1gNnSc2	město
jako	jako	k8xS	jako
turistické	turistický	k2eAgFnSc2d1	turistická
atrakce	atrakce	k1gFnSc2	atrakce
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
též	též	k9	též
místem	místem	k6eAd1	místem
natáčení	natáčení	k1gNnSc4	natáčení
filmu	film	k1gInSc2	film
Predátor	predátor	k1gMnSc1	predátor
s	s	k7c7	s
Arnoldem	Arnold	k1gMnSc7	Arnold
Schwarzeneggerem	Schwarzenegger	k1gMnSc7	Schwarzenegger
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Turistické	turistický	k2eAgNnSc1d1	turistické
centrum	centrum	k1gNnSc1	centrum
==	==	k?	==
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
umístěné	umístěný	k2eAgFnSc2d1	umístěná
podél	podél	k6eAd1	podél
písečné	písečný	k2eAgFnSc2d1	písečná
pláže	pláž	k1gFnSc2	pláž
Banderas	Banderas	k1gMnSc1	Banderas
Bay	Bay	k1gMnSc1	Bay
<g/>
,	,	kIx,	,
v	v	k7c6	v
horkém	horký	k2eAgNnSc6d1	horké
tropickém	tropický	k2eAgNnSc6d1	tropické
podnebí	podnebí	k1gNnSc6	podnebí
<g/>
,	,	kIx,	,
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
džungle	džungle	k1gFnSc2	džungle
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
magnetem	magnet	k1gInSc7	magnet
pro	pro	k7c4	pro
turistiku	turistika	k1gFnSc4	turistika
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
velké	velký	k2eAgFnPc4d1	velká
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
s	s	k7c7	s
hotely	hotel	k1gInPc7	hotel
<g/>
,	,	kIx,	,
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
Old	Olda	k1gFnPc2	Olda
Vallarta	Vallarta	k1gFnSc1	Vallarta
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
Nuova	Nuova	k1gFnSc1	Nuova
Vallarta	Vallarta	k1gFnSc1	Vallarta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
hotelů	hotel	k1gInPc2	hotel
a	a	k8xC	a
turistických	turistický	k2eAgFnPc2d1	turistická
atrakcí	atrakce	k1gFnPc2	atrakce
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Noční	noční	k2eAgInSc1d1	noční
život	život	k1gInSc1	život
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
restauracích	restaurace	k1gFnPc6	restaurace
a	a	k8xC	a
klubech	klub	k1gInPc6	klub
podél	podél	k7c2	podél
pobřežní	pobřežní	k2eAgFnSc2d1	pobřežní
promenády	promenáda	k1gFnSc2	promenáda
Malecon	Malecona	k1gFnPc2	Malecona
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgMnPc1d1	místní
řemeslníci	řemeslník	k1gMnPc1	řemeslník
nabízí	nabízet	k5eAaImIp3nP	nabízet
turistům	turist	k1gMnPc3	turist
šperky	šperk	k1gInPc1	šperk
<g/>
,	,	kIx,	,
textil	textil	k1gInSc1	textil
<g/>
,	,	kIx,	,
sochy	socha	k1gFnPc1	socha
místních	místní	k2eAgMnPc2d1	místní
umělců	umělec	k1gMnPc2	umělec
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
suvenýry	suvenýr	k1gInPc4	suvenýr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Guadalajara	Guadalajara	k1gFnSc1	Guadalajara
a	a	k8xC	a
Acapulco	Acapulco	k1gNnSc1	Acapulco
byly	být	k5eAaImAgInP	být
typickým	typický	k2eAgInSc7d1	typický
cílem	cíl	k1gInSc7	cíl
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
gayů	gay	k1gMnPc2	gay
a	a	k8xC	a
lesbiček	lesbička	k1gFnPc2	lesbička
z	z	k7c2	z
Mexico	Mexico	k6eAd1	Mexico
City	city	k1gNnSc2	city
a	a	k8xC	a
především	především	k9	především
ze	z	k7c2	z
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
Kanady	Kanada	k1gFnSc2	Kanada
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
a	a	k8xC	a
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
Puerto	Puerta	k1gFnSc5	Puerta
Vallarta	Vallarta	k1gFnSc1	Vallarta
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
jako	jako	k9	jako
satelit	satelit	k1gInSc4	satelit
hlavního	hlavní	k2eAgNnSc2d1	hlavní
turistického	turistický	k2eAgNnSc2d1	turistické
rekreačního	rekreační	k2eAgNnSc2d1	rekreační
centra	centrum	k1gNnSc2	centrum
Guadalajara	Guadalajara	k1gFnSc1	Guadalajara
,	,	kIx,	,
určený	určený	k2eAgInSc1d1	určený
pro	pro	k7c4	pro
gaye	gay	k1gMnPc4	gay
a	a	k8xC	a
lesbičky	lesbička	k1gFnPc4	lesbička
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
např.	např.	kA	např.
Fire	Fire	k1gNnSc4	Fire
Island	Island	k1gInSc1	Island
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
New	New	k1gFnSc3	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
Palm	Palm	k1gInSc1	Palm
Springs	Springsa	k1gFnPc2	Springsa
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Puerto	Puerta	k1gFnSc5	Puerta
Vallarta	Vallart	k1gMnSc4	Vallart
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
největší	veliký	k2eAgFnSc4d3	veliký
turistickou	turistický	k2eAgFnSc4d1	turistická
destinaci	destinace	k1gFnSc4	destinace
gayů	gay	k1gMnPc2	gay
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
,	,	kIx,	,
nazývanou	nazývaný	k2eAgFnSc4d1	nazývaná
San	San	k1gFnSc4	San
Francisco	Francisco	k1gNnSc1	Francisco
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Puerto	Puerta	k1gFnSc5	Puerta
Vallarta	Vallart	k1gMnSc2	Vallart
bylo	být	k5eAaImAgNnS	být
označeno	označit	k5eAaPmNgNnS	označit
jako	jako	k8xC	jako
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
turistická	turistický	k2eAgFnSc1d1	turistická
destinace	destinace	k1gFnSc1	destinace
pro	pro	k7c4	pro
gaye	gay	k1gMnPc4	gay
v	v	k7c6	v
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Santa	Santa	k1gFnSc1	Santa
Barbara	Barbara	k1gFnSc1	Barbara
<g/>
,	,	kIx,	,
USA	USA	kA	USA
</s>
</p>
<p>
<s>
Gijón	Gijón	k1gInSc1	Gijón
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
</s>
</p>
<p>
<s>
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
</s>
</p>
<p>
<s>
Toronto	Toronto	k1gNnSc1	Toronto
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Puerto	Puerta	k1gFnSc5	Puerta
Vallarta	Vallart	k1gMnSc4	Vallart
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Průvodce	průvodce	k1gMnSc1	průvodce
pro	pro	k7c4	pro
dovolenou	dovolená	k1gFnSc4	dovolená
</s>
</p>
<p>
<s>
Ekoturistika	Ekoturistika	k1gFnSc1	Ekoturistika
Průvodce	průvodce	k1gMnSc1	průvodce
</s>
</p>
<p>
<s>
LGBT	LGBT	kA	LGBT
LGBT	LGBT	kA	LGBT
Friendly	Friendly	k1gMnSc1	Friendly
Průvodce	průvodce	k1gMnSc1	průvodce
</s>
</p>
<p>
<s>
Hotely	hotel	k1gInPc1	hotel
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
Puerto	Puerta	k1gFnSc5	Puerta
Vallarta	Vallart	k1gMnSc2	Vallart
</s>
</p>
<p>
<s>
All	All	k?	All
inclusive	inclusivat	k5eAaPmIp3nS	inclusivat
hotely	hotel	k1gInPc4	hotel
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Puerto	Puerta	k1gFnSc5	Puerta
Vallarta	Vallarta	k1gFnSc1	Vallarta
</s>
</p>
<p>
<s>
Plánu	plán	k1gInSc2	plán
evropské	evropský	k2eAgInPc4d1	evropský
hotely	hotel	k1gInPc4	hotel
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Puerto	Puerta	k1gFnSc5	Puerta
Vallarta	Vallarta	k1gFnSc1	Vallarta
</s>
</p>
<p>
<s>
==	==	k?	==
Referencias	Referencias	k1gInSc4	Referencias
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Puerto	Puerta	k1gFnSc5	Puerta
Vallarta	Vallart	k1gMnSc4	Vallart
na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
