<p>
<s>
Karel	Karel	k1gMnSc1	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1685	[number]	k4	1685
Vídeň	Vídeň	k1gFnSc1	Vídeň
–	–	k?	–
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1740	[number]	k4	1740
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
syn	syn	k1gMnSc1	syn
císaře	císař	k1gMnSc2	císař
Leopolda	Leopold	k1gMnSc2	Leopold
I.	I.	kA	I.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc4	jeho
manželky	manželka	k1gFnPc4	manželka
Eleonory	Eleonora	k1gFnSc2	Eleonora
Falcko-Neuburské	Falcko-Neuburský	k2eAgFnSc2d1	Falcko-Neuburský
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
jako	jako	k9	jako
Karel	Karel	k1gMnSc1	Karel
III	III	kA	III
<g/>
.	.	kIx.	.
králem	král	k1gMnSc7	král
španělským	španělský	k2eAgInSc7d1	španělský
(	(	kIx(	(
<g/>
1703	[number]	k4	1703
<g/>
–	–	k?	–
<g/>
1711	[number]	k4	1711
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
jako	jako	k9	jako
Karel	Karel	k1gMnSc1	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
císařem	císař	k1gMnSc7	císař
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
(	(	kIx(	(
<g/>
1711	[number]	k4	1711
<g/>
–	–	k?	–
<g/>
1740	[number]	k4	1740
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
jako	jako	k9	jako
Karel	Karel	k1gMnSc1	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
králem	král	k1gMnSc7	král
českým	český	k2eAgMnSc7d1	český
<g/>
,	,	kIx,	,
markrabětem	markrabě	k1gMnSc7	markrabě
moravským	moravský	k2eAgMnSc7d1	moravský
a	a	k8xC	a
arcivévodou	arcivévoda	k1gMnSc7	arcivévoda
rakouským	rakouský	k2eAgInSc7d1	rakouský
(	(	kIx(	(
<g/>
1711	[number]	k4	1711
<g/>
–	–	k?	–
<g/>
1740	[number]	k4	1740
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
jako	jako	k9	jako
Karel	Karel	k1gMnSc1	Karel
III	III	kA	III
<g/>
.	.	kIx.	.
králem	král	k1gMnSc7	král
uherským	uherský	k2eAgInSc7d1	uherský
(	(	kIx(	(
<g/>
1711	[number]	k4	1711
<g/>
–	–	k?	–
<g/>
1740	[number]	k4	1740
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nástup	nástup	k1gInSc1	nástup
do	do	k7c2	do
politiky	politika	k1gFnSc2	politika
==	==	k?	==
</s>
</p>
<p>
<s>
Dětství	dětství	k1gNnSc4	dětství
a	a	k8xC	a
mládí	mládí	k1gNnSc4	mládí
prožil	prožít	k5eAaPmAgMnS	prožít
na	na	k7c6	na
vídeňském	vídeňský	k2eAgInSc6d1	vídeňský
dvoře	dvůr	k1gInSc6	dvůr
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
Leopolda	Leopold	k1gMnSc2	Leopold
I.	I.	kA	I.
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
si	se	k3xPyFc3	se
vážil	vážit	k5eAaImAgMnS	vážit
jako	jako	k8xS	jako
vévody	vévoda	k1gMnPc4	vévoda
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
jazykové	jazykový	k2eAgInPc4d1	jazykový
a	a	k8xC	a
hudební	hudební	k2eAgNnSc4d1	hudební
nadání	nadání	k1gNnSc4	nadání
(	(	kIx(	(
<g/>
dokonce	dokonce	k9	dokonce
složil	složit	k5eAaPmAgMnS	složit
několik	několik	k4yIc4	několik
oper	opera	k1gFnPc2	opera
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyslanci	vyslanec	k1gMnPc1	vyslanec
ho	on	k3xPp3gMnSc4	on
později	pozdě	k6eAd2	pozdě
popisovali	popisovat	k5eAaImAgMnP	popisovat
jako	jako	k8xC	jako
nepříliš	příliš	k6eNd1	příliš
pohledného	pohledný	k2eAgMnSc4d1	pohledný
muže	muž	k1gMnSc4	muž
s	s	k7c7	s
nevýrazným	výrazný	k2eNgInSc7d1	nevýrazný
hlasem	hlas	k1gInSc7	hlas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
posledního	poslední	k2eAgInSc2d1	poslední
španělského	španělský	k2eAgInSc2d1	španělský
Habsburka	Habsburk	k1gInSc2	Habsburk
Karla	Karel	k1gMnSc2	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgMnS	mít
stát	stát	k5eAaPmF	stát
jeho	jeho	k3xOp3gMnSc7	jeho
nástupcem	nástupce	k1gMnSc7	nástupce
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
protikandidát	protikandidát	k1gMnSc1	protikandidát
však	však	k9	však
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
Filip	Filip	k1gMnSc1	Filip
z	z	k7c2	z
Anjou	Anja	k1gMnSc7	Anja
<g/>
,	,	kIx,	,
vnuk	vnuk	k1gMnSc1	vnuk
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Soustředění	soustředění	k1gNnSc1	soustředění
španělského	španělský	k2eAgNnSc2d1	španělské
dědictví	dědictví	k1gNnSc2	dědictví
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
vlastního	vlastní	k2eAgNnSc2d1	vlastní
Španělska	Španělsko	k1gNnSc2	Španělsko
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
území	území	k1gNnSc4	území
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
šlo	jít	k5eAaImAgNnS	jít
také	také	k9	také
o	o	k7c4	o
obrovské	obrovský	k2eAgNnSc4d1	obrovské
koloniální	koloniální	k2eAgNnSc4d1	koloniální
panství	panství	k1gNnSc4	panství
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
)	)	kIx)	)
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
bourbonského	bourbonský	k2eAgInSc2d1	bourbonský
rodu	rod	k1gInSc2	rod
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
nepřijatelné	přijatelný	k2eNgNnSc1d1	nepřijatelné
pro	pro	k7c4	pro
Anglii	Anglie	k1gFnSc4	Anglie
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc4	Nizozemsko
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
evropské	evropský	k2eAgInPc4d1	evropský
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
habsburskou	habsburský	k2eAgFnSc7d1	habsburská
monarchií	monarchie	k1gFnSc7	monarchie
uzavřely	uzavřít	k5eAaPmAgFnP	uzavřít
protifrancouzskou	protifrancouzský	k2eAgFnSc4d1	protifrancouzská
koalici	koalice	k1gFnSc4	koalice
<g/>
.	.	kIx.	.
</s>
<s>
Vojenské	vojenský	k2eAgInPc1d1	vojenský
konflikty	konflikt	k1gInPc1	konflikt
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
nenechaly	nechat	k5eNaPmAgFnP	nechat
dlouho	dlouho	k6eAd1	dlouho
čekat	čekat	k5eAaImF	čekat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
rozhořely	rozhořet	k5eAaPmAgFnP	rozhořet
na	na	k7c6	na
několika	několik	k4yIc6	několik
frontách	fronta	k1gFnPc6	fronta
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
anglo-francouzské	anglorancouzský	k2eAgNnSc1d1	anglo-francouzské
zápolení	zápolení	k1gNnSc1	zápolení
se	se	k3xPyFc4	se
přeneslo	přenést	k5eAaPmAgNnS	přenést
i	i	k9	i
do	do	k7c2	do
kolonií	kolonie	k1gFnPc2	kolonie
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1704	[number]	k4	1704
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
spojenci	spojenec	k1gMnPc1	spojenec
porazili	porazit	k5eAaPmAgMnP	porazit
francouzské	francouzský	k2eAgNnSc4d1	francouzské
vojsko	vojsko	k1gNnSc4	vojsko
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Hochstädtu	Hochstädt	k1gInSc2	Hochstädt
<g/>
,	,	kIx,	,
nastal	nastat	k5eAaPmAgInS	nastat
ve	v	k7c6	v
vzájemném	vzájemný	k2eAgInSc6d1	vzájemný
poměru	poměr	k1gInSc6	poměr
sil	síla	k1gFnPc2	síla
radikální	radikální	k2eAgInSc4d1	radikální
obrat	obrat	k1gInSc4	obrat
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
Karel	Karel	k1gMnSc1	Karel
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
španělský	španělský	k2eAgMnSc1d1	španělský
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
silné	silný	k2eAgFnSc2d1	silná
armády	armáda	k1gFnSc2	armáda
na	na	k7c4	na
Pyrenejský	pyrenejský	k2eAgInSc4d1	pyrenejský
poloostrov	poloostrov	k1gInSc4	poloostrov
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
získával	získávat	k5eAaImAgMnS	získávat
jedno	jeden	k4xCgNnSc4	jeden
vítězství	vítězství	k1gNnSc4	vítězství
za	za	k7c7	za
druhým	druhý	k4xOgNnSc7	druhý
a	a	k8xC	a
císařské	císařský	k2eAgFnSc2d1	císařská
armády	armáda	k1gFnSc2	armáda
vítězily	vítězit	k5eAaImAgInP	vítězit
i	i	k9	i
v	v	k7c6	v
španělských	španělský	k2eAgFnPc6d1	španělská
državách	država	k1gFnPc6	država
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
síly	síla	k1gFnPc1	síla
vyrovnaly	vyrovnat	k5eAaPmAgFnP	vyrovnat
<g/>
;	;	kIx,	;
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
obrat	obrat	k1gInSc4	obrat
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
válce	válka	k1gFnSc6	válka
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
smrt	smrt	k1gFnSc1	smrt
římskoněmeckého	římskoněmecký	k2eAgMnSc2d1	římskoněmecký
císaře	císař	k1gMnSc2	císař
a	a	k8xC	a
Karlova	Karlův	k2eAgMnSc2d1	Karlův
bratra	bratr	k1gMnSc2	bratr
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
</s>
</p>
<p>
<s>
==	==	k?	==
Hlava	hlava	k1gFnSc1	hlava
habsburské	habsburský	k2eAgFnSc2d1	habsburská
monarchie	monarchie	k1gFnSc2	monarchie
==	==	k?	==
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
se	se	k3xPyFc4	se
jako	jako	k9	jako
jeho	jeho	k3xOp3gMnSc1	jeho
nástupce	nástupce	k1gMnSc1	nástupce
stal	stát	k5eAaPmAgMnS	stát
hlavou	hlava	k1gFnSc7	hlava
habsburské	habsburský	k2eAgFnSc2d1	habsburská
monarchie	monarchie	k1gFnSc2	monarchie
(	(	kIx(	(
<g/>
uherským	uherský	k2eAgMnSc7d1	uherský
králem	král	k1gMnSc7	král
byl	být	k5eAaImAgMnS	být
korunován	korunován	k2eAgMnSc1d1	korunován
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1712	[number]	k4	1712
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
korunovace	korunovace	k1gFnSc1	korunovace
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1723	[number]	k4	1723
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Císařem	Císař	k1gMnSc7	Císař
římskoněmecké	římskoněmecký	k2eAgFnSc2d1	římskoněmecká
říše	říš	k1gFnSc2	říš
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
12	[number]	k4	12
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1711	[number]	k4	1711
<g/>
,	,	kIx,	,
korunován	korunován	k2eAgInSc4d1	korunován
tamtéž	tamtéž	k6eAd1	tamtéž
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1711	[number]	k4	1711
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tímto	tento	k3xDgInSc7	tento
aktem	akt	k1gInSc7	akt
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
pro	pro	k7c4	pro
členy	člen	k1gMnPc4	člen
protifrancouzské	protifrancouzský	k2eAgFnSc2d1	protifrancouzská
koalice	koalice	k1gFnSc2	koalice
nepřijatelný	přijatelný	k2eNgMnSc1d1	nepřijatelný
jako	jako	k8xS	jako
španělský	španělský	k2eAgMnSc1d1	španělský
panovník	panovník	k1gMnSc1	panovník
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Karel	Karel	k1gMnSc1	Karel
by	by	kYmCp3nS	by
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
rukou	ruka	k1gFnPc2	ruka
soustředil	soustředit	k5eAaPmAgInS	soustředit
příliš	příliš	k6eAd1	příliš
velkou	velký	k2eAgFnSc4d1	velká
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1713	[number]	k4	1713
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
Anglie	Anglie	k1gFnSc1	Anglie
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
dosavadní	dosavadní	k2eAgMnPc1d1	dosavadní
spojenci	spojenec	k1gMnPc1	spojenec
Habsburků	Habsburk	k1gMnPc2	Habsburk
s	s	k7c7	s
válkou	válka	k1gFnSc7	válka
vyčerpanou	vyčerpaný	k2eAgFnSc7d1	vyčerpaná
Francií	Francie	k1gFnSc7	Francie
mír	mír	k1gInSc1	mír
v	v	k7c6	v
Utrechtu	Utrecht	k1gInSc6	Utrecht
<g/>
,	,	kIx,	,
kterým	který	k3yQgNnSc7	který
bylo	být	k5eAaImAgNnS	být
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
nástupnictví	nástupnictví	k1gNnSc1	nástupnictví
Filipa	Filip	k1gMnSc2	Filip
V.	V.	kA	V.
(	(	kIx(	(
<g/>
z	z	k7c2	z
Anjou	Anja	k1gMnSc7	Anja
<g/>
)	)	kIx)	)
na	na	k7c6	na
španělském	španělský	k2eAgInSc6d1	španělský
trůnu	trůn	k1gInSc6	trůn
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
podmínkou	podmínka	k1gFnSc7	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
španělské	španělský	k2eAgFnPc1d1	španělská
državy	država	k1gFnPc1	država
nespojí	spojit	k5eNaPmIp3nP	spojit
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
mírovou	mírový	k2eAgFnSc7d1	mírová
smlouvou	smlouva	k1gFnSc7	smlouva
uzavřenou	uzavřený	k2eAgFnSc7d1	uzavřená
v	v	k7c6	v
Rastatte	Rastatt	k1gInSc5	Rastatt
roku	rok	k1gInSc2	rok
1714	[number]	k4	1714
ukončil	ukončit	k5eAaPmAgInS	ukončit
svůj	svůj	k3xOyFgInSc4	svůj
boj	boj	k1gInSc4	boj
i	i	k9	i
Karel	Karel	k1gMnSc1	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jako	jako	k9	jako
kompenzaci	kompenzace	k1gFnSc4	kompenzace
rezignace	rezignace	k1gFnSc2	rezignace
na	na	k7c4	na
španělský	španělský	k2eAgInSc4d1	španělský
trůn	trůn	k1gInSc4	trůn
dostal	dostat	k5eAaPmAgInS	dostat
část	část	k1gFnSc4	část
důležitých	důležitý	k2eAgFnPc2d1	důležitá
držav	država	k1gFnPc2	država
Španělska	Španělsko	k1gNnSc2	Španělsko
–	–	k?	–
Španělské	španělský	k2eAgNnSc1d1	španělské
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc6d1	dnešní
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
)	)	kIx)	)
a	a	k8xC	a
bývalé	bývalý	k2eAgFnSc2d1	bývalá
španělské	španělský	k2eAgFnSc2d1	španělská
državy	država	k1gFnSc2	država
na	na	k7c6	na
Apeninském	apeninský	k2eAgInSc6d1	apeninský
poloostrově	poloostrov	k1gInSc6	poloostrov
(	(	kIx(	(
<g/>
Neapolsko	Neapolsko	k1gNnSc1	Neapolsko
<g/>
,	,	kIx,	,
Milánsko	Milánsko	k1gNnSc1	Milánsko
a	a	k8xC	a
Sardinie	Sardinie	k1gFnSc1	Sardinie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
šestadvacetileté	šestadvacetiletý	k2eAgFnSc2d1	šestadvacetiletá
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
projevovat	projevovat	k5eAaImF	projevovat
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
slabost	slabost	k1gFnSc1	slabost
habsburské	habsburský	k2eAgFnSc2d1	habsburská
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Územní	územní	k2eAgInPc1d1	územní
zisky	zisk	k1gInPc1	zisk
z	z	k7c2	z
úspěšné	úspěšný	k2eAgFnSc2d1	úspěšná
války	válka	k1gFnSc2	válka
s	s	k7c7	s
Tureckem	Turecko	k1gNnSc7	Turecko
v	v	k7c6	v
letech	let	k1gInPc6	let
1716	[number]	k4	1716
<g/>
–	–	k?	–
<g/>
1718	[number]	k4	1718
ztratila	ztratit	k5eAaPmAgFnS	ztratit
v	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
střetu	střet	k1gInSc6	střet
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
o	o	k7c4	o
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
1737	[number]	k4	1737
<g/>
–	–	k?	–
<g/>
1738	[number]	k4	1738
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
přišla	přijít	k5eAaPmAgFnS	přijít
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
neúspěšného	úspěšný	k2eNgInSc2d1	neúspěšný
střetu	střet	k1gInSc2	střet
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
o	o	k7c4	o
jihoitalské	jihoitalský	k2eAgFnPc4d1	jihoitalská
državy	država	k1gFnPc4	država
Neapolsko	Neapolsko	k1gNnSc1	Neapolsko
a	a	k8xC	a
Sicílii	Sicílie	k1gFnSc6	Sicílie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Karel	Karel	k1gMnSc1	Karel
a	a	k8xC	a
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
nástupu	nástup	k1gInSc6	nástup
na	na	k7c4	na
císařský	císařský	k2eAgInSc4d1	císařský
trůn	trůn	k1gInSc4	trůn
a	a	k8xC	a
uzavřením	uzavření	k1gNnSc7	uzavření
Satmárského	Satmárský	k2eAgInSc2d1	Satmárský
míru	mír	k1gInSc2	mír
zřídil	zřídit	k5eAaPmAgMnS	zřídit
se	s	k7c7	s
schválením	schválení	k1gNnSc7	schválení
uherského	uherský	k2eAgInSc2d1	uherský
sněmu	sněm	k1gInSc2	sněm
stálou	stálý	k2eAgFnSc4d1	stálá
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
zlikvidovat	zlikvidovat	k5eAaPmF	zlikvidovat
poslední	poslední	k2eAgInPc4d1	poslední
zbytky	zbytek	k1gInPc4	zbytek
osmanského	osmanský	k2eAgNnSc2d1	osmanské
panství	panství	k1gNnSc2	panství
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
definitivním	definitivní	k2eAgNnSc6d1	definitivní
vytlačení	vytlačení	k1gNnSc6	vytlačení
Turků	Turek	k1gMnPc2	Turek
z	z	k7c2	z
Uher	Uhry	k1gFnPc2	Uhry
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
kurz	kurz	k1gInSc1	kurz
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
a	a	k8xC	a
politické	politický	k2eAgFnSc2d1	politická
konsolidace	konsolidace	k1gFnSc2	konsolidace
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
politice	politika	k1gFnSc6	politika
se	se	k3xPyFc4	se
neúspěšně	úspěšně	k6eNd1	úspěšně
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
omezit	omezit	k5eAaPmF	omezit
privilegia	privilegium	k1gNnPc4	privilegium
uherské	uherský	k2eAgFnSc2d1	uherská
šlechty	šlechta	k1gFnSc2	šlechta
a	a	k8xC	a
zavést	zavést	k5eAaPmF	zavést
do	do	k7c2	do
praxe	praxe	k1gFnSc2	praxe
starší	starý	k2eAgInSc4d2	starší
plán	plán	k1gInSc4	plán
centralizace	centralizace	k1gFnSc2	centralizace
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Řadou	řada	k1gFnSc7	řada
reforem	reforma	k1gFnPc2	reforma
schválených	schválený	k2eAgFnPc2d1	schválená
uherským	uherský	k2eAgInSc7d1	uherský
sněmem	sněm	k1gInSc7	sněm
v	v	k7c6	v
r.	r.	kA	r.
1723	[number]	k4	1723
upravil	upravit	k5eAaPmAgMnS	upravit
hospodářské	hospodářský	k2eAgNnSc4d1	hospodářské
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgNnSc4d1	soudní
i	i	k8xC	i
správní	správní	k2eAgNnSc4d1	správní
poměry	poměra	k1gFnPc4	poměra
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Územní	územní	k2eAgFnPc1d1	územní
<g/>
,	,	kIx,	,
politické	politický	k2eAgFnPc1d1	politická
a	a	k8xC	a
náboženské	náboženský	k2eAgFnPc1d1	náboženská
otázky	otázka	k1gFnPc1	otázka
upravil	upravit	k5eAaPmAgInS	upravit
v	v	k7c6	v
nařízení	nařízení	k1gNnSc6	nařízení
Resolutio	Resolutio	k6eAd1	Resolutio
Carolina	Carolin	k2eAgFnSc1d1	Carolina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pragmatická	pragmatický	k2eAgFnSc1d1	pragmatická
sankce	sankce	k1gFnSc1	sankce
==	==	k?	==
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1713	[number]	k4	1713
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
Karel	Karel	k1gMnSc1	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
vydal	vydat	k5eAaPmAgMnS	vydat
Pragmatickou	pragmatický	k2eAgFnSc4d1	pragmatická
sankci	sankce	k1gFnSc4	sankce
<g/>
,	,	kIx,	,
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Pragmatickou	pragmatický	k2eAgFnSc4d1	pragmatická
sankci	sankce	k1gFnSc4	sankce
o	o	k7c6	o
posloupnosti	posloupnost	k1gFnSc6	posloupnost
nejjasnějšího	jasný	k2eAgInSc2d3	nejjasnější
arcidomu	arcidom	k1gInSc2	arcidom
rakouského	rakouský	k2eAgInSc2d1	rakouský
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
byla	být	k5eAaImAgFnS	být
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
nedělitelnost	nedělitelnost	k1gFnSc1	nedělitelnost
habsburských	habsburský	k2eAgFnPc2d1	habsburská
držav	država	k1gFnPc2	država
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
vymření	vymření	k1gNnSc2	vymření
mužské	mužský	k2eAgFnSc2d1	mužská
linie	linie	k1gFnSc2	linie
Habsburků	Habsburk	k1gMnPc2	Habsburk
nástupnictví	nástupnictví	k1gNnSc2	nástupnictví
linie	linie	k1gFnSc2	linie
ženské	ženský	k2eAgFnSc2d1	ženská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cílem	cíl	k1gInSc7	cíl
pragmatické	pragmatický	k2eAgFnSc2d1	pragmatická
sankce	sankce	k1gFnSc2	sankce
bylo	být	k5eAaImAgNnS	být
zajištění	zajištění	k1gNnSc1	zajištění
nástupnictví	nástupnictví	k1gNnSc2	nástupnictví
po	po	k7c4	po
Karlu	Karla	k1gFnSc4	Karla
VI	VI	kA	VI
<g/>
.	.	kIx.	.
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnPc4	jeho
vlastní	vlastní	k2eAgFnPc4d1	vlastní
děti	dítě	k1gFnPc4	dítě
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
i	i	k9	i
dcery	dcera	k1gFnSc2	dcera
<g/>
)	)	kIx)	)
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
nezanechal	zanechat	k5eNaPmAgInS	zanechat
mužské	mužský	k2eAgMnPc4d1	mužský
potomky	potomek	k1gMnPc4	potomek
(	(	kIx(	(
<g/>
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ovšem	ovšem	k9	ovšem
Karel	Karel	k1gMnSc1	Karel
s	s	k7c7	s
mužským	mužský	k2eAgMnSc7d1	mužský
dědicem	dědic	k1gMnSc7	dědic
ještě	ještě	k6eAd1	ještě
počítal	počítat	k5eAaImAgMnS	počítat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
měly	mít	k5eAaImAgInP	mít
mít	mít	k5eAaImF	mít
při	při	k7c6	při
nástupnictví	nástupnictví	k1gNnSc6	nástupnictví
přednost	přednost	k1gFnSc4	přednost
dcery	dcera	k1gFnSc2	dcera
Karla	Karel	k1gMnSc2	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
před	před	k7c7	před
dcerami	dcera	k1gFnPc7	dcera
jeho	jeho	k3xOp3gMnPc2	jeho
předchůdců	předchůdce	k1gMnPc2	předchůdce
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
a	a	k8xC	a
Leopolda	Leopolda	k1gFnSc1	Leopolda
I.	I.	kA	I.
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1723	[number]	k4	1723
uznaly	uznat	k5eAaPmAgInP	uznat
pragmatickou	pragmatický	k2eAgFnSc4d1	pragmatická
sankci	sankce	k1gFnSc4	sankce
sněmy	sněm	k1gInPc4	sněm
všech	všecek	k3xTgFnPc2	všecek
zemí	zem	k1gFnPc2	zem
habsburské	habsburský	k2eAgFnSc2d1	habsburská
monarchie	monarchie	k1gFnSc2	monarchie
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
sněm	sněm	k1gInSc1	sněm
12	[number]	k4	12
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1720	[number]	k4	1720
<g/>
,	,	kIx,	,
moravský	moravský	k2eAgInSc1d1	moravský
sněm	sněm	k1gInSc1	sněm
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1720	[number]	k4	1720
<g/>
,	,	kIx,	,
slezský	slezský	k2eAgInSc1d1	slezský
sněm	sněm	k1gInSc1	sněm
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1720	[number]	k4	1720
<g/>
;	;	kIx,	;
Chebsko	Chebsko	k1gNnSc1	Chebsko
pak	pak	k6eAd1	pak
přijalo	přijmout	k5eAaPmAgNnS	přijmout
pragmatickou	pragmatický	k2eAgFnSc4d1	pragmatická
sankci	sankce	k1gFnSc4	sankce
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1721	[number]	k4	1721
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1725	[number]	k4	1725
pragmatickou	pragmatický	k2eAgFnSc4d1	pragmatická
sankci	sankce	k1gFnSc4	sankce
uznal	uznat	k5eAaPmAgMnS	uznat
španělský	španělský	k2eAgMnSc1d1	španělský
král	král	k1gMnSc1	král
Filip	Filip	k1gMnSc1	Filip
V.	V.	kA	V.
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
Karel	Karel	k1gMnSc1	Karel
vzdal	vzdát	k5eAaPmAgMnS	vzdát
nároku	nárok	k1gInSc3	nárok
na	na	k7c4	na
španělský	španělský	k2eAgInSc4d1	španělský
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1726	[number]	k4	1726
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
společného	společný	k2eAgInSc2d1	společný
obranného	obranný	k2eAgInSc2d1	obranný
spolku	spolek	k1gInSc2	spolek
proti	proti	k7c3	proti
Anglii	Anglie	k1gFnSc3	Anglie
a	a	k8xC	a
Francii	Francie	k1gFnSc3	Francie
sankci	sankce	k1gFnSc3	sankce
uznalo	uznat	k5eAaPmAgNnS	uznat
Rusko	Rusko	k1gNnSc1	Rusko
a	a	k8xC	a
Prusko	Prusko	k1gNnSc1	Prusko
</s>
</p>
<p>
<s>
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1731	[number]	k4	1731
a	a	k8xC	a
1732	[number]	k4	1732
sankci	sankce	k1gFnSc4	sankce
uznaly	uznat	k5eAaPmAgFnP	uznat
Anglie	Anglie	k1gFnPc1	Anglie
a	a	k8xC	a
Nizozemí	Nizozemí	k1gNnSc1	Nizozemí
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
zhroucení	zhroucení	k1gNnSc3	zhroucení
anglicko-francouzského	anglickorancouzský	k2eAgNnSc2d1	anglicko-francouzský
spojenectví	spojenectví	k1gNnSc2	spojenectví
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1733	[number]	k4	1733
saský	saský	k2eAgMnSc1d1	saský
kurfiřt	kurfiřt	k1gMnSc1	kurfiřt
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
pragmatickou	pragmatický	k2eAgFnSc4d1	pragmatická
sankci	sankce	k1gFnSc4	sankce
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ho	on	k3xPp3gNnSc4	on
Rakousko	Rakousko	k1gNnSc1	Rakousko
podporovalo	podporovat	k5eAaImAgNnS	podporovat
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
úsilí	úsilí	k1gNnSc6	úsilí
o	o	k7c4	o
polský	polský	k2eAgInSc4d1	polský
trůn	trůn	k1gInSc4	trůn
</s>
</p>
<p>
<s>
v	v	k7c6	v
následné	následný	k2eAgFnSc6d1	následná
válce	válka	k1gFnSc6	válka
o	o	k7c4	o
polské	polský	k2eAgNnSc4d1	polské
nástupnictví	nástupnictví	k1gNnSc4	nástupnictví
zpochybňovaly	zpochybňovat	k5eAaImAgFnP	zpochybňovat
sankci	sankce	k1gFnSc4	sankce
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
a	a	k8xC	a
Piemont	Piemont	k1gInSc1	Piemont
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
Francie	Francie	k1gFnSc1	Francie
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
vídeňského	vídeňský	k2eAgInSc2d1	vídeňský
předjarního	předjarní	k2eAgInSc2d1	předjarní
míru	mír	k1gInSc2	mír
1735	[number]	k4	1735
rovněž	rovněž	k9	rovněž
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1740	[number]	k4	1740
po	po	k7c6	po
nastoupení	nastoupení	k1gNnSc6	nastoupení
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
začaly	začít	k5eAaPmAgFnP	začít
Války	válek	k1gInPc4	válek
o	o	k7c4	o
rakouské	rakouský	k2eAgNnSc4d1	rakouské
dědictví	dědictví	k1gNnSc4	dědictví
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Prusko	Prusko	k1gNnSc1	Prusko
<g/>
,	,	kIx,	,
Bavorsko	Bavorsko	k1gNnSc1	Bavorsko
<g/>
,	,	kIx,	,
Sasko	Sasko	k1gNnSc1	Sasko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
Španělsko	Španělsko	k1gNnSc4	Španělsko
přestaly	přestat	k5eAaPmAgFnP	přestat
uznávat	uznávat	k5eAaImF	uznávat
pragmatickou	pragmatický	k2eAgFnSc4d1	pragmatická
sankci	sankce	k1gFnSc4	sankce
a	a	k8xC	a
dohodly	dohodnout	k5eAaPmAgFnP	dohodnout
se	se	k3xPyFc4	se
na	na	k7c4	na
rozdělení	rozdělení	k1gNnSc4	rozdělení
habsburských	habsburský	k2eAgFnPc2d1	habsburská
držav	država	k1gFnPc2	država
</s>
</p>
<p>
<s>
S	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
Anglie	Anglie	k1gFnSc2	Anglie
uhájila	uhájit	k5eAaPmAgFnS	uhájit
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
své	svůj	k3xOyFgNnSc4	svůj
dědictví	dědictví	k1gNnSc4	dědictví
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
,	,	kIx,	,
Parmy	Parma	k1gFnSc2	Parma
a	a	k8xC	a
Piacenzy	Piacenza	k1gFnSc2	Piacenza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1748	[number]	k4	1748
v	v	k7c6	v
míru	mír	k1gInSc6	mír
z	z	k7c2	z
Aachen	Aachna	k1gFnPc2	Aachna
(	(	kIx(	(
<g/>
Cách	Cáchy	k1gFnPc2	Cáchy
<g/>
)	)	kIx)	)
Pragmatickou	pragmatický	k2eAgFnSc4d1	pragmatická
sankci	sankce	k1gFnSc4	sankce
potvrdily	potvrdit	k5eAaPmAgFnP	potvrdit
všechny	všechen	k3xTgFnPc1	všechen
zúčastněné	zúčastněný	k2eAgFnPc1d1	zúčastněná
evropské	evropský	k2eAgFnPc1d1	Evropská
mocnosti	mocnost	k1gFnPc1	mocnost
</s>
</p>
<p>
<s>
==	==	k?	==
Postřelení	postřelení	k1gNnSc4	postřelení
krumlovského	krumlovský	k2eAgMnSc2d1	krumlovský
vévody	vévoda	k1gMnSc2	vévoda
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1732	[number]	k4	1732
se	se	k3xPyFc4	se
císařský	císařský	k2eAgInSc1d1	císařský
pár	pár	k1gInSc1	pár
vypravil	vypravit	k5eAaPmAgInS	vypravit
z	z	k7c2	z
lázeňského	lázeňský	k2eAgInSc2d1	lázeňský
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
Čechách	Čechy	k1gFnPc6	Čechy
přes	přes	k7c4	přes
Prahu	Praha	k1gFnSc4	Praha
na	na	k7c4	na
návštěvu	návštěva	k1gFnSc4	návštěva
knížecího	knížecí	k2eAgInSc2d1	knížecí
dvora	dvůr	k1gInSc2	dvůr
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Krumlově	Krumlov	k1gInSc6	Krumlov
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
jej	on	k3xPp3gMnSc4	on
jako	jako	k9	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejbližších	blízký	k2eAgMnPc2d3	nejbližší
šlechticů	šlechtic	k1gMnPc2	šlechtic
a	a	k8xC	a
nejvyšší	vysoký	k2eAgMnPc4d3	nejvyšší
podkoní	podkoní	k1gMnPc4	podkoní
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
Adam	Adam	k1gMnSc1	Adam
František	František	k1gMnSc1	František
Schwarzenberg	Schwarzenberg	k1gMnSc1	Schwarzenberg
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
cesty	cesta	k1gFnSc2	cesta
se	se	k3xPyFc4	se
celý	celý	k2eAgInSc1d1	celý
dvůr	dvůr	k1gInSc1	dvůr
odebral	odebrat	k5eAaPmAgInS	odebrat
do	do	k7c2	do
Brandýsa	Brandýs	k1gInSc2	Brandýs
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
na	na	k7c4	na
lov	lov	k1gInSc4	lov
jelenů	jelen	k1gMnPc2	jelen
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1732	[number]	k4	1732
se	se	k3xPyFc4	se
císař	císař	k1gMnSc1	císař
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
krumlovským	krumlovský	k2eAgMnSc7d1	krumlovský
vévodou	vévoda	k1gMnSc7	vévoda
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
nadháňky	nadháňka	k1gFnPc4	nadháňka
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
kvůli	kvůli	k7c3	kvůli
závažné	závažný	k2eAgFnSc3d1	závažná
chybě	chyba	k1gFnSc3	chyba
v	v	k7c6	v
rozestavení	rozestavení	k1gNnSc6	rozestavení
střelců	střelec	k1gMnPc2	střelec
a	a	k8xC	a
císařově	císařův	k2eAgFnSc3d1	císařova
krátkozrakosti	krátkozrakost	k1gFnSc3	krátkozrakost
byl	být	k5eAaImAgMnS	být
Adam	Adam	k1gMnSc1	Adam
František	František	k1gMnSc1	František
Schwarzenberg	Schwarzenberg	k1gMnSc1	Schwarzenberg
smrtelně	smrtelně	k6eAd1	smrtelně
zraněn	zranit	k5eAaPmNgMnS	zranit
císařovou	císařův	k2eAgFnSc7d1	císařova
ranou	rána	k1gFnSc7	rána
do	do	k7c2	do
břicha	břicho	k1gNnSc2	břicho
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hodnocení	hodnocení	k1gNnSc1	hodnocení
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
vlády	vláda	k1gFnSc2	vláda
==	==	k?	==
</s>
</p>
<p>
<s>
Osobnost	osobnost	k1gFnSc4	osobnost
a	a	k8xC	a
povahu	povaha	k1gFnSc4	povaha
Karla	Karel	k1gMnSc2	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
značně	značně	k6eAd1	značně
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
jeho	on	k3xPp3gInSc4	on
pobyt	pobyt	k1gInSc4	pobyt
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Zvykl	zvyknout	k5eAaPmAgMnS	zvyknout
si	se	k3xPyFc3	se
na	na	k7c4	na
španělský	španělský	k2eAgInSc4d1	španělský
dvorský	dvorský	k2eAgInSc4d1	dvorský
rituál	rituál	k1gInSc4	rituál
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
mu	on	k3xPp3gMnSc3	on
velmi	velmi	k6eAd1	velmi
vyhovoval	vyhovovat	k5eAaImAgInS	vyhovovat
<g/>
,	,	kIx,	,
vídeňský	vídeňský	k2eAgInSc1d1	vídeňský
dvůr	dvůr	k1gInSc1	dvůr
však	však	k9	však
svazoval	svazovat	k5eAaImAgInS	svazovat
do	do	k7c2	do
strnulosti	strnulost	k1gFnSc2	strnulost
<g/>
.	.	kIx.	.
</s>
<s>
Zajímal	zajímat	k5eAaImAgMnS	zajímat
se	se	k3xPyFc4	se
o	o	k7c4	o
ekonomiku	ekonomika	k1gFnSc4	ekonomika
a	a	k8xC	a
chtěl	chtít	k5eAaImAgInS	chtít
odstranit	odstranit	k5eAaPmF	odstranit
cla	clo	k1gNnPc4	clo
uvnitř	uvnitř	k7c2	uvnitř
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
vládou	vláda	k1gFnSc7	vláda
Karla	Karel	k1gMnSc2	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
spojován	spojován	k2eAgInSc4d1	spojován
počátek	počátek	k1gInSc4	počátek
budování	budování	k1gNnSc2	budování
silniční	silniční	k2eAgFnSc2d1	silniční
sítě	síť	k1gFnSc2	síť
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
<g/>
Karel	Karel	k1gMnSc1	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgMnS	být
velmi	velmi	k6eAd1	velmi
zbožným	zbožný	k2eAgMnSc7d1	zbožný
katolíkem	katolík	k1gMnSc7	katolík
<g/>
,	,	kIx,	,
za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
rozpoutala	rozpoutat	k5eAaPmAgFnS	rozpoutat
v	v	k7c6	v
monarchii	monarchie	k1gFnSc6	monarchie
další	další	k2eAgFnSc1d1	další
vlna	vlna	k1gFnSc1	vlna
rekatolizace	rekatolizace	k1gFnSc2	rekatolizace
–	–	k?	–
především	především	k6eAd1	především
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
proti	proti	k7c3	proti
nekatolickým	katolický	k2eNgInPc3d1	nekatolický
vlivům	vliv	k1gInPc3	vliv
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
posledním	poslední	k2eAgMnSc7d1	poslední
mužským	mužský	k2eAgMnSc7d1	mužský
příslušníkem	příslušník	k1gMnSc7	příslušník
habsburské	habsburský	k2eAgFnSc2d1	habsburská
dynastie	dynastie	k1gFnSc2	dynastie
(	(	kIx(	(
<g/>
manželstvím	manželství	k1gNnSc7	manželství
jeho	jeho	k3xOp3gFnSc2	jeho
dcery	dcera	k1gFnSc2	dcera
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
s	s	k7c7	s
Františkem	František	k1gMnSc7	František
I.	I.	kA	I.
Lotrinským	lotrinský	k2eAgFnPc3d1	lotrinská
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
rod	rod	k1gInSc1	rod
jako	jako	k8xS	jako
habsbursko-lotrinský	habsburskootrinský	k2eAgInSc1d1	habsbursko-lotrinský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
otravu	otrava	k1gFnSc4	otrava
houbami	houba	k1gFnPc7	houba
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
muchomůrkou	muchomůrkou	k?	muchomůrkou
zelenou	zelená	k1gFnSc4	zelená
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pochován	pochován	k2eAgMnSc1d1	pochován
v	v	k7c6	v
kapucínské	kapucínský	k2eAgFnSc6d1	Kapucínská
hrobce	hrobka	k1gFnSc6	hrobka
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
a	a	k8xC	a
rodiče	rodič	k1gMnPc1	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Nápis	nápis	k1gInSc1	nápis
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
sarkofágu	sarkofág	k1gInSc6	sarkofág
zní	znět	k5eAaImIp3nS	znět
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Potomci	potomek	k1gMnPc5	potomek
==	==	k?	==
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1708	[number]	k4	1708
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
s	s	k7c7	s
princeznou	princezna	k1gFnSc7	princezna
Alžbětou	Alžběta	k1gFnSc7	Alžběta
Kristýnou	Kristýna	k1gFnSc7	Kristýna
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
vévody	vévoda	k1gMnSc2	vévoda
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Brunšvicko-Wolfenbüttelského	Brunšvicko-Wolfenbüttelský	k2eAgMnSc2d1	Brunšvicko-Wolfenbüttelský
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
Kristýny	Kristýna	k1gFnSc2	Kristýna
Luisy	Luisa	k1gFnSc2	Luisa
Öttingenské	Öttingenský	k2eAgFnSc2d1	Öttingenský
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
manželství	manželství	k1gNnSc2	manželství
se	se	k3xPyFc4	se
narodily	narodit	k5eAaPmAgFnP	narodit
čtyři	čtyři	k4xCgFnPc1	čtyři
děti	dítě	k1gFnPc1	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývod	vývod	k1gInSc1	vývod
z	z	k7c2	z
předků	předek	k1gInPc2	předek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ČORNEJOVÁ	ČORNEJOVÁ	kA	ČORNEJOVÁ
<g/>
,	,	kIx,	,
Ivana	Ivana	k1gFnSc1	Ivana
<g/>
;	;	kIx,	;
RAK	rak	k1gMnSc1	rak
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
VLNAS	VLNAS	kA	VLNAS
<g/>
,	,	kIx,	,
Vít	Vít	k1gMnSc1	Vít
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
tvých	tvůj	k3xOp2gNnPc2	tvůj
křídel	křídlo	k1gNnPc2	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Habsburkové	Habsburk	k1gMnPc1	Habsburk
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grafoprint-Neubert	Grafoprint-Neubert	k1gMnSc1	Grafoprint-Neubert
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
289	[number]	k4	289
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85785	[number]	k4	85785
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
HAMANNOVÁ	HAMANNOVÁ	kA	HAMANNOVÁ
<g/>
,	,	kIx,	,
Brigitte	Brigitte	k1gFnSc1	Brigitte
<g/>
.	.	kIx.	.
</s>
<s>
Habsburkové	Habsburk	k1gMnPc1	Habsburk
<g/>
.	.	kIx.	.
</s>
<s>
Životopisná	životopisný	k2eAgFnSc1d1	životopisná
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Brána	brána	k1gFnSc1	brána
;	;	kIx,	;
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
408	[number]	k4	408
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85946	[number]	k4	85946
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
MIKULEC	Mikulec	k1gMnSc1	Mikulec
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
BĚLINA	Bělina	k1gMnSc1	Bělina
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
;	;	kIx,	;
VLNAS	VLNAS	kA	VLNAS
<g/>
,	,	kIx,	,
Vít	Vít	k1gMnSc1	Vít
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
IX	IX	kA	IX
<g/>
.	.	kIx.	.
1683	[number]	k4	1683
<g/>
-	-	kIx~	-
<g/>
1740	[number]	k4	1740
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
860	[number]	k4	860
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7432	[number]	k4	7432
<g/>
-	-	kIx~	-
<g/>
105	[number]	k4	105
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RILL	RILL	kA	RILL
<g/>
,	,	kIx,	,
Bernd	Bernd	k1gInSc1	Bernd
<g/>
.	.	kIx.	.
</s>
<s>
Karl	Karl	k1gMnSc1	Karl
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Habsburg	Habsburg	k1gInSc1	Habsburg
als	als	k?	als
barocke	barockat	k5eAaPmIp3nS	barockat
Großmacht	Großmacht	k1gInSc1	Großmacht
<g/>
.	.	kIx.	.
</s>
<s>
Graz	Graz	k1gMnSc1	Graz
<g/>
;	;	kIx,	;
Wien	Wien	k1gMnSc1	Wien
<g/>
;	;	kIx,	;
Köln	Köln	k1gMnSc1	Köln
<g/>
:	:	kIx,	:
Styria	Styrium	k1gNnPc1	Styrium
Verlag	Verlaga	k1gFnPc2	Verlaga
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
359	[number]	k4	359
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
222	[number]	k4	222
<g/>
-	-	kIx~	-
<g/>
12148	[number]	k4	12148
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
VÁCHA	Vácha	k1gMnSc1	Vácha
<g/>
,	,	kIx,	,
Štěpán	Štěpán	k1gMnSc1	Štěpán
<g/>
;	;	kIx,	;
VESELÁ	Veselá	k1gFnSc1	Veselá
<g/>
,	,	kIx,	,
Irena	Irena	k1gFnSc1	Irena
<g/>
;	;	kIx,	;
VLNAS	VLNAS	kA	VLNAS
<g/>
,	,	kIx,	,
Vít	Vít	k1gMnSc1	Vít
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
a	a	k8xC	a
Alžběta	Alžběta	k1gFnSc1	Alžběta
Kristýna	Kristýna	k1gFnSc1	Kristýna
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
korunovace	korunovace	k1gFnSc1	korunovace
1723	[number]	k4	1723
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
520	[number]	k4	520
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7432	[number]	k4	7432
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VOKÁČOVÁ	Vokáčová	k1gFnSc1	Vokáčová
<g/>
,	,	kIx,	,
Petra	Petra	k1gFnSc1	Petra
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
RYANTOVÁ	RYANTOVÁ	kA	RYANTOVÁ
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
;	;	kIx,	;
VOREL	Vorel	k1gMnSc1	Vorel
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
králové	král	k1gMnPc1	král
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
940	[number]	k4	940
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
433	[number]	k4	433
<g/>
-	-	kIx~	-
<g/>
449	[number]	k4	449
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VONDRA	Vondra	k1gMnSc1	Vondra
<g/>
,	,	kIx,	,
Roman	Roman	k1gMnSc1	Roman
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
země	zem	k1gFnPc1	zem
v	v	k7c6	v
letech	let	k1gInPc6	let
1705	[number]	k4	1705
<g/>
–	–	k?	–
<g/>
1792	[number]	k4	1792
:	:	kIx,	:
doba	doba	k1gFnSc1	doba
absolutismu	absolutismus	k1gInSc2	absolutismus
<g/>
,	,	kIx,	,
osvícenství	osvícenství	k1gNnSc2	osvícenství
<g/>
,	,	kIx,	,
paruk	paruka	k1gFnPc2	paruka
a	a	k8xC	a
třírohých	třírohý	k2eAgInPc2d1	třírohý
klobouků	klobouk	k1gInPc2	klobouk
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
384	[number]	k4	384
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
448	[number]	k4	448
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VONDRA	Vondra	k1gMnSc1	Vondra
<g/>
,	,	kIx,	,
Roman	Roman	k1gMnSc1	Roman
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
1685	[number]	k4	1685
<g/>
–	–	k?	–
<g/>
1740	[number]	k4	1740
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
obzor	obzor	k1gInSc1	obzor
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
21	[number]	k4	21
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
275	[number]	k4	275
<g/>
–	–	k?	–
<g/>
278	[number]	k4	278
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1210	[number]	k4	1210
<g/>
-	-	kIx~	-
<g/>
6097	[number]	k4	6097
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
León	León	k1gNnSc1	León
Sanz	Sanza	k1gFnPc2	Sanza
<g/>
,	,	kIx,	,
Virginia	Virginium	k1gNnSc2	Virginium
<g/>
.	.	kIx.	.
</s>
<s>
Carlos	Carlos	k1gMnSc1	Carlos
VI	VI	kA	VI
<g/>
:	:	kIx,	:
el	ela	k1gFnPc2	ela
emperador	emperadora	k1gFnPc2	emperadora
que	que	k?	que
no	no	k9	no
pudo	pudo	k6eAd1	pudo
ser	srát	k5eAaImRp2nS	srát
rey	rey	k?	rey
de	de	k?	de
Españ	Españ	k1gFnSc2	Españ
<g/>
.	.	kIx.	.
</s>
<s>
Madrid	Madrid	k1gInSc1	Madrid
:	:	kIx,	:
Aguilar	Aguilar	k1gInSc1	Aguilar
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
84	[number]	k4	84
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9409	[number]	k4	9409
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WANNER	WANNER	kA	WANNER
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Ostend	Ostend	k1gInSc1	Ostend
Company	Compana	k1gFnSc2	Compana
as	as	k1gNnSc2	as
Phenomenon	Phenomenon	k1gMnSc1	Phenomenon
of	of	k?	of
International	International	k1gFnSc1	International
Politics	Politics	k1gInSc1	Politics
in	in	k?	in
1722	[number]	k4	1722
<g/>
–	–	k?	–
<g/>
1731	[number]	k4	1731
<g/>
.	.	kIx.	.
</s>
<s>
Prague	Prague	k1gFnSc1	Prague
Papers	Papersa	k1gFnPc2	Papersa
on	on	k3xPp3gMnSc1	on
the	the	k?	the
History	Histor	k1gInPc4	Histor
of	of	k?	of
International	International	k1gFnSc2	International
Relations	Relationsa	k1gFnPc2	Relationsa
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
,	,	kIx,	,
s.	s.	k?	s.
29	[number]	k4	29
<g/>
-	-	kIx~	-
<g/>
63	[number]	k4	63
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
[	[	kIx(	[
<g/>
PDF	PDF	kA	PDF
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7308	[number]	k4	7308
<g/>
-	-	kIx~	-
<g/>
161	[number]	k4	161
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
WANNER	WANNER	kA	WANNER
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Establishment	establishment	k1gInSc1	establishment
of	of	k?	of
the	the	k?	the
General	General	k1gMnSc1	General
Company	Compana	k1gFnSc2	Compana
in	in	k?	in
Ostend	Ostend	k1gMnSc1	Ostend
in	in	k?	in
the	the	k?	the
Context	Context	k1gInSc1	Context
of	of	k?	of
the	the	k?	the
Habsburg	Habsburg	k1gInSc1	Habsburg
Maritime	Maritim	k1gInSc5	Maritim
Plans	Plansa	k1gFnPc2	Plansa
1714	[number]	k4	1714
<g/>
–	–	k?	–
<g/>
1723	[number]	k4	1723
<g/>
.	.	kIx.	.
</s>
<s>
Prague	Prague	k1gFnSc1	Prague
Papers	Papersa	k1gFnPc2	Papersa
on	on	k3xPp3gMnSc1	on
the	the	k?	the
History	Histor	k1gInPc4	Histor
of	of	k?	of
International	International	k1gFnSc2	International
Relations	Relationsa	k1gFnPc2	Relationsa
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
,	,	kIx,	,
s.	s.	k?	s.
33	[number]	k4	33
<g/>
-	-	kIx~	-
<g/>
62	[number]	k4	62
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
[	[	kIx(	[
<g/>
PDF	PDF	kA	PDF
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7308	[number]	k4	7308
<g/>
-	-	kIx~	-
<g/>
208	[number]	k4	208
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
hlav	hlava	k1gFnPc2	hlava
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
panovníků	panovník	k1gMnPc2	panovník
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Karel	Karla	k1gFnPc2	Karla
VI	VI	kA	VI
<g/>
.	.	kIx.	.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
