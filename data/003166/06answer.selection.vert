<s>
Klec	klec	k1gFnSc1	klec
je	být	k5eAaImIp3nS	být
železná	železný	k2eAgFnSc1d1	železná
nebo	nebo	k8xC	nebo
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
konstrukce	konstrukce	k1gFnSc1	konstrukce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
zvířata	zvíře	k1gNnPc4	zvíře
nebo	nebo	k8xC	nebo
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
oddělení	oddělení	k1gNnSc3	oddělení
od	od	k7c2	od
vnějšího	vnější	k2eAgInSc2d1	vnější
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
