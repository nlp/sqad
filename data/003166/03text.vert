<s>
Klec	klec	k1gFnSc1	klec
je	být	k5eAaImIp3nS	být
železná	železný	k2eAgFnSc1d1	železná
nebo	nebo	k8xC	nebo
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
konstrukce	konstrukce	k1gFnSc1	konstrukce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
zvířata	zvíře	k1gNnPc4	zvíře
nebo	nebo	k8xC	nebo
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
oddělení	oddělení	k1gNnSc3	oddělení
od	od	k7c2	od
vnějšího	vnější	k2eAgInSc2d1	vnější
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Klec	klec	k1gFnSc1	klec
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
5-6	[number]	k4	5-6
stěnami	stěna	k1gFnPc7	stěna
tvořenými	tvořený	k2eAgFnPc7d1	tvořená
mříží	mříž	k1gFnSc7	mříž
nebo	nebo	k8xC	nebo
sítí	síť	k1gFnSc7	síť
<g/>
.	.	kIx.	.
</s>
<s>
Mívá	mívat	k5eAaImIp3nS	mívat
též	též	k9	též
vstupní	vstupní	k2eAgNnPc4d1	vstupní
dvířka	dvířka	k1gNnPc4	dvířka
<g/>
,	,	kIx,	,
kterými	který	k3yQgInPc7	který
se	se	k3xPyFc4	se
daná	daný	k2eAgFnSc1d1	daná
bytost	bytost	k1gFnSc1	bytost
vkládá	vkládat	k5eAaImIp3nS	vkládat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vlézá	vlézat	k5eAaImIp3nS	vlézat
nebo	nebo	k8xC	nebo
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
dovnitř	dovnitř	k6eAd1	dovnitř
<g/>
.	.	kIx.	.
</s>
<s>
Klece	klec	k1gFnPc1	klec
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
<g/>
,	,	kIx,	,
cirkusech	cirkus	k1gInPc6	cirkus
a	a	k8xC	a
jiných	jiný	k2eAgNnPc6d1	jiné
zařízeních	zařízení	k1gNnPc6	zařízení
na	na	k7c6	na
člověku	člověk	k1gMnSc6	člověk
nebezpečná	bezpečný	k2eNgNnPc4d1	nebezpečné
zvířata	zvíře	k1gNnPc4	zvíře
či	či	k8xC	či
ptáky	pták	k1gMnPc4	pták
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
by	by	kYmCp3nP	by
mohli	moct	k5eAaImAgMnP	moct
ulétnout	ulétnout	k5eAaPmF	ulétnout
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
též	tenž	k3xDgFnSc2	tenž
klece	klec	k1gFnSc2	klec
používali	používat	k5eAaImAgMnP	používat
i	i	k9	i
pro	pro	k7c4	pro
lidi	člověk	k1gMnPc4	člověk
(	(	kIx(	(
<g/>
např.	např.	kA	např.
otroky	otrok	k1gMnPc4	otrok
<g/>
,	,	kIx,	,
zloděje	zloděj	k1gMnPc4	zloděj
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Časté	častý	k2eAgFnPc1d1	častá
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
klícky	klícka	k1gFnPc1	klícka
na	na	k7c4	na
drobné	drobný	k2eAgNnSc4d1	drobné
ptactvo	ptactvo	k1gNnSc4	ptactvo
v	v	k7c6	v
lidských	lidský	k2eAgInPc6d1	lidský
příbytcích	příbytek	k1gInPc6	příbytek
<g/>
.	.	kIx.	.
</s>
<s>
Klec	klec	k1gFnSc1	klec
nemusí	muset	k5eNaImIp3nS	muset
mít	mít	k5eAaImF	mít
pouze	pouze	k6eAd1	pouze
funkci	funkce	k1gFnSc4	funkce
zamezovací	zamezovací	k2eAgFnSc4d1	zamezovací
a	a	k8xC	a
ochrannou	ochranný	k2eAgFnSc4d1	ochranná
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
dolech	dol	k1gInPc6	dol
horníci	horník	k1gMnPc1	horník
při	při	k7c6	při
fárání	fárání	k1gNnSc6	fárání
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
dopravě	doprava	k1gFnSc3	doprava
také	také	k9	také
klec	klec	k1gFnSc1	klec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
klec	klec	k1gFnSc1	klec
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
k	k	k7c3	k
máčení	máčení	k1gNnSc3	máčení
nepoctivců	nepoctivec	k1gMnPc2	nepoctivec
a	a	k8xC	a
drobných	drobný	k2eAgMnPc2d1	drobný
zlodějíčků	zlodějíček	k1gMnPc2	zlodějíček
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
klecemi	klec	k1gFnPc7	klec
se	se	k3xPyFc4	se
setkáme	setkat	k5eAaPmIp1nP	setkat
i	i	k9	i
v	v	k7c6	v
pohádkách	pohádka	k1gFnPc6	pohádka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
zlatá	zlatý	k2eAgNnPc4d1	Zlaté
klícka	klícka	k1gFnSc1	klícka
<g/>
)	)	kIx)	)
a	a	k8xC	a
vědeckofantastické	vědeckofantastický	k2eAgFnSc3d1	vědeckofantastická
literatuře	literatura	k1gFnSc3	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Klecové	klecový	k2eAgNnSc4d1	klecové
lůžko	lůžko	k1gNnSc4	lůžko
Faradayova	Faradayův	k2eAgFnSc1d1	Faradayova
klec	klec	k1gFnSc1	klec
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
klec	klec	k1gFnSc1	klec
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
klec	klec	k1gFnSc1	klec
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
