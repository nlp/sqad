<s>
Prof.	prof.	kA	prof.
MUDr.	MUDr.	kA	MUDr.
Mojmír	Mojmír	k1gMnSc1	Mojmír
Petráň	Petráň	k1gMnSc1	Petráň
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
českých	český	k2eAgMnPc2d1	český
vědců	vědec	k1gMnPc2	vědec
a	a	k8xC	a
vynálezců	vynálezce	k1gMnPc2	vynálezce
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
historie	historie	k1gFnSc2	historie
světové	světový	k2eAgFnSc2d1	světová
vědy	věda	k1gFnSc2	věda
se	se	k3xPyFc4	se
zapsal	zapsat	k5eAaPmAgMnS	zapsat
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
vynálezem	vynález	k1gInSc7	vynález
tzv.	tzv.	kA	tzv.
konfokálního	konfokální	k2eAgInSc2d1	konfokální
mikroskopu	mikroskop	k1gInSc2	mikroskop
s	s	k7c7	s
dvojitým	dvojitý	k2eAgNnSc7d1	dvojité
řádkováním	řádkování	k1gNnSc7	řádkování
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
znamenal	znamenat	k5eAaImAgInS	znamenat
převrat	převrat	k1gInSc1	převrat
v	v	k7c6	v
mikroskopických	mikroskopický	k2eAgFnPc6d1	mikroskopická
technikách	technika	k1gFnPc6	technika
přispěl	přispět	k5eAaPmAgInS	přispět
k	k	k7c3	k
bouřlivému	bouřlivý	k2eAgInSc3d1	bouřlivý
rozvoji	rozvoj	k1gInSc3	rozvoj
molekulární	molekulární	k2eAgFnSc2d1	molekulární
buněčné	buněčný	k2eAgFnSc2d1	buněčná
biologie	biologie	k1gFnSc2	biologie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
odboje	odboj	k1gInSc2	odboj
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
byl	být	k5eAaImAgInS	být
vyznamenán	vyznamenat	k5eAaPmNgInS	vyznamenat
Československou	československý	k2eAgFnSc7d1	Československá
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
medailí	medaile	k1gFnSc7	medaile
Za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
II	II	kA	II
<g/>
.	.	kIx.	.
stupně	stupeň	k1gInSc2	stupeň
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
Lékařskou	lékařský	k2eAgFnSc4d1	lékařská
fakultu	fakulta	k1gFnSc4	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
na	na	k7c6	na
Fyziologickém	fyziologický	k2eAgInSc6d1	fyziologický
ústavu	ústav	k1gInSc6	ústav
ČSAV	ČSAV	kA	ČSAV
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
elektrofyziologií	elektrofyziologie	k1gFnSc7	elektrofyziologie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
se	se	k3xPyFc4	se
habilitoval	habilitovat	k5eAaBmAgInS	habilitovat
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
lékařské	lékařský	k2eAgFnSc2d1	lékařská
fyziky	fyzika	k1gFnSc2	fyzika
na	na	k7c6	na
Lékařské	lékařský	k2eAgFnSc6d1	lékařská
fakultě	fakulta	k1gFnSc6	fakulta
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
prací	práce	k1gFnPc2	práce
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
hledání	hledání	k1gNnSc2	hledání
jiných	jiný	k2eAgInPc2d1	jiný
než	než	k8xS	než
elektrických	elektrický	k2eAgInPc2d1	elektrický
projevů	projev	k1gInPc2	projev
nervového	nervový	k2eAgInSc2d1	nervový
a	a	k8xC	a
svalového	svalový	k2eAgInSc2d1	svalový
vzruchu	vzruch	k1gInSc2	vzruch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
řádným	řádný	k2eAgMnSc7d1	řádný
profesorem	profesor	k1gMnSc7	profesor
pro	pro	k7c4	pro
obor	obor	k1gInSc4	obor
biofyzika	biofyzika	k1gFnSc1	biofyzika
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Je	být	k5eAaImIp3nS	být
spoluautorem	spoluautor	k1gMnSc7	spoluautor
knihy	kniha	k1gFnSc2	kniha
Electrophysiological	Electrophysiological	k1gMnSc1	Electrophysiological
Methods	Methodsa	k1gFnPc2	Methodsa
in	in	k?	in
Biological	Biological	k1gMnSc1	Biological
Research	Research	k1gMnSc1	Research
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jej	on	k3xPp3gMnSc4	on
proslavila	proslavit	k5eAaPmAgFnS	proslavit
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
čestným	čestný	k2eAgMnSc7d1	čestný
členem	člen	k1gMnSc7	člen
britské	britský	k2eAgFnSc2d1	britská
Královské	královský	k2eAgFnSc2d1	královská
mikroskopické	mikroskopický	k2eAgFnSc2d1	mikroskopická
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2005	[number]	k4	2005
mu	on	k3xPp3gNnSc3	on
prezident	prezident	k1gMnSc1	prezident
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
udělil	udělit	k5eAaPmAgMnS	udělit
Medaili	medaile	k1gFnSc4	medaile
za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
o	o	k7c4	o
stát	stát	k1gInSc4	stát
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vědy	věda	k1gFnSc2	věda
<g/>
.	.	kIx.	.
</s>
