<s>
Angličtina	angličtina	k1gFnSc1	angličtina
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
západogermánských	západogermánský	k2eAgInPc2d1	západogermánský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
se	se	k3xPyFc4	se
z	z	k7c2	z
anglo	anglo	k1gNnSc4	anglo
<g/>
-	-	kIx~	-
<g/>
saského	saský	k2eAgInSc2d1	saský
dialektu	dialekt	k1gInSc2	dialekt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1066	[number]	k4	1066
po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
Viléma	Vilém	k1gMnSc2	Vilém
Dobyvatele	dobyvatel	k1gMnSc2	dobyvatel
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Hastings	Hastingsa	k1gFnPc2	Hastingsa
ovlivněn	ovlivněn	k2eAgInSc4d1	ovlivněn
francouzštinou	francouzština	k1gFnSc7	francouzština
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
značnému	značný	k2eAgNnSc3d1	značné
zjednodušení	zjednodušení	k1gNnSc3	zjednodušení
gramatiky	gramatika	k1gFnSc2	gramatika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
flektivního	flektivní	k2eAgInSc2d1	flektivní
jazyka	jazyk	k1gInSc2	jazyk
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
převážně	převážně	k6eAd1	převážně
analytický	analytický	k2eAgInSc4d1	analytický
jazyk	jazyk	k1gInSc4	jazyk
s	s	k7c7	s
pouhými	pouhý	k2eAgInPc7d1	pouhý
zbytky	zbytek	k1gInPc7	zbytek
flexe	flexe	k1gFnSc2	flexe
<g/>
.	.	kIx.	.
</s>
<s>
Angličtina	angličtina	k1gFnSc1	angličtina
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
analytický	analytický	k2eAgInSc1d1	analytický
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
ustálený	ustálený	k2eAgInSc4d1	ustálený
slovosled	slovosled	k1gInSc4	slovosled
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
češtiny	čeština	k1gFnSc2	čeština
používá	používat	k5eAaImIp3nS	používat
daleko	daleko	k6eAd1	daleko
více	hodně	k6eAd2	hodně
funkčních	funkční	k2eAgNnPc2d1	funkční
slov	slovo	k1gNnPc2	slovo
(	(	kIx(	(
<g/>
determinanty	determinanta	k1gFnPc1	determinanta
<g/>
,	,	kIx,	,
předložky	předložka	k1gFnPc1	předložka
<g/>
,	,	kIx,	,
spojky	spojka	k1gFnPc1	spojka
<g/>
,	,	kIx,	,
zájmena	zájmeno	k1gNnPc1	zájmeno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
latinskému	latinský	k2eAgMnSc3d1	latinský
a	a	k8xC	a
francouzskému	francouzský	k2eAgInSc3d1	francouzský
vlivu	vliv	k1gInSc3	vliv
má	mít	k5eAaImIp3nS	mít
angličtina	angličtina	k1gFnSc1	angličtina
velmi	velmi	k6eAd1	velmi
bohatou	bohatý	k2eAgFnSc4d1	bohatá
slovní	slovní	k2eAgFnSc4d1	slovní
zásobu	zásoba	k1gFnSc4	zásoba
a	a	k8xC	a
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
řada	řada	k1gFnSc1	řada
synonym	synonymum	k1gNnPc2	synonymum
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
platí	platit	k5eAaImIp3nS	platit
pravidlo	pravidlo	k1gNnSc1	pravidlo
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
hovoru	hovor	k1gInSc6	hovor
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
slova	slovo	k1gNnPc4	slovo
germánského	germánský	k2eAgInSc2d1	germánský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
ve	v	k7c6	v
formálním	formální	k2eAgInSc6d1	formální
hovoru	hovor	k1gInSc6	hovor
či	či	k8xC	či
ve	v	k7c6	v
psané	psaný	k2eAgFnSc6d1	psaná
podobě	podoba	k1gFnSc6	podoba
slova	slovo	k1gNnSc2	slovo
francouzského	francouzský	k2eAgInSc2d1	francouzský
a	a	k8xC	a
latinského	latinský	k2eAgInSc2d1	latinský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
<g/>
:	:	kIx,	:
ask	ask	k?	ask
(	(	kIx(	(
<g/>
germ.	germ.	k?	germ.
<g/>
)	)	kIx)	)
→	→	k?	→
demand	demand	k1gInSc1	demand
(	(	kIx(	(
<g/>
fr.	fr.	k?	fr.
<g/>
)	)	kIx)	)
→	→	k?	→
interrogate	interrogat	k1gInSc5	interrogat
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
-	-	kIx~	-
nouns	nouns	k1gInSc1	nouns
Determinátory	Determinátor	k1gInPc1	Determinátor
-	-	kIx~	-
determiners	determiners	k6eAd1	determiners
Přídavná	přídavný	k2eAgNnPc1d1	přídavné
jména	jméno	k1gNnPc1	jméno
-	-	kIx~	-
adjectives	adjectives	k1gInSc1	adjectives
Číslovky	číslovka	k1gFnSc2	číslovka
-	-	kIx~	-
numerals	numerals	k1gInSc1	numerals
Zájmena	zájmeno	k1gNnSc2	zájmeno
-	-	kIx~	-
pronouns	pronouns	k1gInSc1	pronouns
Slovesa	sloveso	k1gNnSc2	sloveso
-	-	kIx~	-
verbs	verbsit	k5eAaPmRp2nS	verbsit
Příslovce	příslovce	k1gNnSc4	příslovce
-	-	kIx~	-
adverbs	adverbsit	k5eAaPmRp2nS	adverbsit
Předložky	předložka	k1gFnSc2	předložka
-	-	kIx~	-
prepositions	prepositionsit	k5eAaPmRp2nS	prepositionsit
Spojky	spojka	k1gFnSc2	spojka
-	-	kIx~	-
conjunctions	conjunctionsit	k5eAaPmRp2nS	conjunctionsit
Citoslovce	citoslovce	k1gNnSc4	citoslovce
-	-	kIx~	-
interjections	interjections	k6eAd1	interjections
Užívají	užívat	k5eAaImIp3nP	užívat
se	se	k3xPyFc4	se
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
se	s	k7c7	s
členem	člen	k1gInSc7	člen
určitým	určitý	k2eAgInSc7d1	určitý
nebo	nebo	k8xC	nebo
neurčitým	určitý	k2eNgInSc7d1	neurčitý
(	(	kIx(	(
<g/>
člen	člen	k1gInSc1	člen
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nahrazen	nahrazen	k2eAgInSc1d1	nahrazen
přivlastňovacím	přivlastňovací	k2eAgNnSc7d1	přivlastňovací
zájmenem	zájmeno	k1gNnSc7	zájmeno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Případy	případ	k1gInPc1	případ
absence	absence	k1gFnSc2	absence
členu	člen	k1gInSc2	člen
jsou	být	k5eAaImIp3nP	být
stanoveny	stanovit	k5eAaPmNgFnP	stanovit
pravidly	pravidlo	k1gNnPc7	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
angličtina	angličtina	k1gFnSc1	angličtina
nepoužívá	používat	k5eNaImIp3nS	používat
gramatické	gramatický	k2eAgInPc4d1	gramatický
pády	pád	k1gInPc4	pád
<g/>
,	,	kIx,	,
využívá	využívat	k5eAaImIp3nS	využívat
tzv.	tzv.	kA	tzv.
saský	saský	k2eAgInSc1d1	saský
genitiv	genitiv	k1gInSc1	genitiv
k	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
příslušnosti	příslušnost	k1gFnSc2	příslušnost
<g/>
.	.	kIx.	.
</s>
<s>
Množné	množný	k2eAgNnSc1d1	množné
číslo	číslo	k1gNnSc1	číslo
podstatných	podstatný	k2eAgNnPc2d1	podstatné
jmen	jméno	k1gNnPc2	jméno
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
příponou	přípona	k1gFnSc7	přípona
-	-	kIx~	-
<g/>
s	s	k7c7	s
nebo	nebo	k8xC	nebo
-	-	kIx~	-
<g/>
es	es	k1gNnSc1	es
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
však	však	k9	však
výjimky	výjimka	k1gFnPc4	výjimka
<g/>
.	.	kIx.	.
</s>
<s>
Podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
nemají	mít	k5eNaImIp3nP	mít
vždy	vždy	k6eAd1	vždy
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgNnSc2	jenž
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
je	on	k3xPp3gInPc4	on
identifikovat	identifikovat	k5eAaBmF	identifikovat
<g/>
;	;	kIx,	;
proto	proto	k8xC	proto
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c4	na
postavení	postavení	k1gNnSc4	postavení
daného	daný	k2eAgNnSc2d1	dané
slova	slovo	k1gNnSc2	slovo
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Infinitiv	infinitiv	k1gInSc1	infinitiv
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
infinitivu	infinitiv	k1gInSc2	infinitiv
je	být	k5eAaImIp3nS	být
částice	částice	k1gFnSc1	částice
"	"	kIx"	"
<g/>
to	ten	k3xDgNnSc4	ten
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
vynechává	vynechávat	k5eAaImIp3nS	vynechávat
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
modálními	modální	k2eAgMnPc7d1	modální
a	a	k8xC	a
několika	několik	k4yIc7	několik
dalšími	další	k2eAgNnPc7d1	další
slovesy	sloveso	k1gNnPc7	sloveso
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
ve	v	k7c6	v
větách	věta	k1gFnPc6	věta
s	s	k7c7	s
pomocným	pomocný	k2eAgNnSc7d1	pomocné
slovesem	sloveso	k1gNnSc7	sloveso
"	"	kIx"	"
<g/>
do	do	k7c2	do
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
a	a	k8xC	a
v	v	k7c6	v
záporu	zápor	k1gInSc6	zápor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Infinitivu	infinitiv	k1gInSc2	infinitiv
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
i	i	k9	i
složené	složený	k2eAgInPc1d1	složený
slovesné	slovesný	k2eAgInPc1d1	slovesný
tvary	tvar	k1gInPc1	tvar
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
be	be	k?	be
waiting	waiting	k1gInSc1	waiting
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
have	havat	k5eAaPmIp3nS	havat
asked	asked	k1gInSc1	asked
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Časované	časovaný	k2eAgInPc1d1	časovaný
tvary	tvar	k1gInPc1	tvar
(	(	kIx(	(
<g/>
oznamovací	oznamovací	k2eAgInSc1d1	oznamovací
způsob	způsob	k1gInSc1	způsob
<g/>
)	)	kIx)	)
v	v	k7c6	v
6	[number]	k4	6
slovesných	slovesný	k2eAgInPc6d1	slovesný
časech	čas	k1gInPc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Průběhové	průběhový	k2eAgFnPc1d1	průběhová
a	a	k8xC	a
prosté	prostý	k2eAgFnPc1d1	prostá
formy	forma	k1gFnPc1	forma
časovaných	časovaný	k2eAgInPc2d1	časovaný
tvarů	tvar	k1gInPc2	tvar
sloves	sloveso	k1gNnPc2	sloveso
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Součástmi	součást	k1gFnPc7	součást
průběhové	průběhový	k2eAgFnSc2d1	průběhová
formy	forma	k1gFnSc2	forma
je	být	k5eAaImIp3nS	být
sloveso	sloveso	k1gNnSc1	sloveso
"	"	kIx"	"
<g/>
to	ten	k3xDgNnSc1	ten
be	be	k?	be
<g/>
"	"	kIx"	"
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
ze	z	k7c2	z
6	[number]	k4	6
časů	čas	k1gInPc2	čas
a	a	k8xC	a
významové	významový	k2eAgNnSc4d1	významové
sloveso	sloveso	k1gNnSc4	sloveso
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
příčestí	příčestí	k1gNnSc2	příčestí
přítomného	přítomný	k1gMnSc2	přítomný
(	(	kIx(	(
<g/>
s	s	k7c7	s
příponou	přípona	k1gFnSc7	přípona
-ing	ng	k1gMnSc1	-ing
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Rozkazovací	rozkazovací	k2eAgInSc1d1	rozkazovací
způsob	způsob	k1gInSc1	způsob
(	(	kIx(	(
<g/>
Tvar	tvar	k1gInSc1	tvar
infinitivu	infinitiv	k1gInSc2	infinitiv
bez	bez	k7c2	bez
částice	částice	k1gFnSc2	částice
"	"	kIx"	"
<g/>
to	ten	k3xDgNnSc1	ten
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Kondicionály	kondicionál	k1gInPc1	kondicionál
nultý	nultý	k4xOgInSc1	nultý
kondicionál	kondicionál	k1gInSc1	kondicionál
<g/>
:	:	kIx,	:
If	If	k1gFnSc1	If
this	thisa	k1gFnPc2	thisa
actor	actora	k1gFnPc2	actora
plays	plays	k1gInSc1	plays
<g/>
,	,	kIx,	,
the	the	k?	the
theatre	theatr	k1gMnSc5	theatr
is	is	k?	is
full	full	k1gInSc1	full
<g/>
.	.	kIx.	.
první	první	k4xOgInSc1	první
kondicionál	kondicionál	k1gInSc1	kondicionál
<g/>
:	:	kIx,	:
If	If	k1gFnSc1	If
you	you	k?	you
win	win	k?	win
<g/>
,	,	kIx,	,
I	i	k8xC	i
will	will	k1gInSc1	will
invite	invit	k1gInSc5	invit
you	you	k?	you
to	ten	k3xDgNnSc1	ten
the	the	k?	the
best	best	k1gInSc1	best
café	café	k1gNnSc1	café
<g/>
.	.	kIx.	.
druhý	druhý	k4xOgInSc1	druhý
kondicionál	kondicionál	k1gInSc1	kondicionál
<g/>
:	:	kIx,	:
If	If	k1gMnSc1	If
the	the	k?	the
train	train	k1gMnSc1	train
stopped	stopped	k1gMnSc1	stopped
<g/>
,	,	kIx,	,
I	i	k9	i
would	would	k1gMnSc1	would
get	get	k?	get
off	off	k?	off
here	here	k1gInSc1	here
<g/>
.	.	kIx.	.
třetí	třetí	k4xOgInSc1	třetí
kondicionál	kondicionál	k1gInSc1	kondicionál
<g/>
:	:	kIx,	:
If	If	k1gMnSc1	If
they	thea	k1gFnSc2	thea
had	had	k1gMnSc1	had
asked	asked	k1gMnSc1	asked
<g/>
,	,	kIx,	,
we	we	k?	we
would	would	k1gInSc1	would
have	have	k1gNnSc1	have
sold	sold	k1gInSc1	sold
them	them	k1gMnSc1	them
the	the	k?	the
tickets	tickets	k1gInSc1	tickets
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Gerundium	gerundium	k1gNnSc1	gerundium
-	-	kIx~	-
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
podstatné	podstatný	k2eAgNnSc4d1	podstatné
jméno	jméno	k1gNnSc4	jméno
slovesné	slovesný	k2eAgNnSc4d1	slovesné
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
Sleeping	Sleeping	k1gInSc1	Sleeping
on	on	k3xPp3gInSc1	on
the	the	k?	the
floor	floor	k1gInSc1	floor
is	is	k?	is
not	nota	k1gFnPc2	nota
comfortable	comfortable	k6eAd1	comfortable
<g/>
.	.	kIx.	.
</s>
<s>
They	Thea	k1gFnSc2	Thea
prefer	prefra	k1gFnPc2	prefra
staying	staying	k1gInSc1	staying
at	at	k?	at
a	a	k8xC	a
hotel	hotel	k1gInSc1	hotel
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Příčestí	příčestí	k1gNnSc1	příčestí
(	(	kIx(	(
<g/>
participles	participles	k1gInSc1	participles
<g/>
)	)	kIx)	)
příčestí	příčestí	k1gNnSc1	příčestí
minulé	minulý	k2eAgFnSc2d1	minulá
a	a	k8xC	a
příčestí	příčestí	k1gNnSc2	příčestí
trpné	trpný	k2eAgFnSc2d1	trpná
mají	mít	k5eAaImIp3nP	mít
jeden	jeden	k4xCgInSc4	jeden
tvar	tvar	k1gInSc4	tvar
-	-	kIx~	-
past	past	k1gFnSc1	past
participle	participle	k6eAd1	participle
<g/>
:	:	kIx,	:
We	We	k1gFnSc1	We
have	havat	k5eAaPmIp3nS	havat
invented	invented	k1gInSc4	invented
a	a	k8xC	a
better	better	k1gInSc4	better
method	methoda	k1gFnPc2	methoda
<g/>
.	.	kIx.	.
</s>
<s>
Where	Wher	k1gMnSc5	Wher
are	ar	k1gInSc5	ar
all	all	k?	all
the	the	k?	the
lost	lost	k2eAgInSc4d1	lost
things	things	k1gInSc4	things
<g/>
?	?	kIx.	?
</s>
<s>
Za	za	k7c4	za
příčestí	příčestí	k1gNnSc4	příčestí
minulé	minulý	k2eAgNnSc4d1	Minulé
se	se	k3xPyFc4	se
nepovažuje	považovat	k5eNaImIp3nS	považovat
tvar	tvar	k1gInSc1	tvar
minulého	minulý	k2eAgInSc2d1	minulý
času	čas	k1gInSc2	čas
prostého	prostý	k2eAgInSc2d1	prostý
(	(	kIx(	(
<g/>
They	Thea	k1gFnSc2	Thea
lost	lost	k1gInSc1	lost
a	a	k8xC	a
lot	lot	k1gInSc1	lot
of	of	k?	of
time	time	k1gInSc1	time
<g/>
)	)	kIx)	)
příčestí	příčestí	k1gNnSc1	příčestí
přítomné	přítomný	k2eAgNnSc1d1	přítomné
<g/>
:	:	kIx,	:
flying	flying	k1gInSc1	flying
objects	objectsa	k1gFnPc2	objectsa
-	-	kIx~	-
létající	létající	k2eAgInPc1d1	létající
předměty	předmět	k1gInPc1	předmět
Angličtina	angličtina	k1gFnSc1	angličtina
má	mít	k5eAaImIp3nS	mít
6	[number]	k4	6
slovesných	slovesný	k2eAgInPc2d1	slovesný
časů	čas	k1gInPc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
má	mít	k5eAaImIp3nS	mít
2	[number]	k4	2
formy	forma	k1gFnSc2	forma
<g/>
:	:	kIx,	:
průběhovou	průběhový	k2eAgFnSc7d1	průběhová
a	a	k8xC	a
prostou	prostý	k2eAgFnSc7d1	prostá
<g/>
.	.	kIx.	.
</s>
<s>
Názvy	název	k1gInPc1	název
slovesných	slovesný	k2eAgInPc2d1	slovesný
časů	čas	k1gInPc2	čas
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
lišit	lišit	k5eAaImF	lišit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vždy	vždy	k6eAd1	vždy
lze	lze	k6eAd1	lze
identifikovat	identifikovat	k5eAaBmF	identifikovat
1	[number]	k4	1
<g/>
.	.	kIx.	.
čas	čas	k1gInSc1	čas
přítomný	přítomný	k2eAgInSc1d1	přítomný
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
čas	čas	k1gInSc4	čas
minulý	minulý	k2eAgInSc4d1	minulý
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
čas	čas	k1gInSc4	čas
předpřítomný	předpřítomný	k2eAgInSc4d1	předpřítomný
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
čas	čas	k1gInSc1	čas
předminulý	předminulý	k2eAgInSc1d1	předminulý
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
čas	čas	k1gInSc4	čas
budoucí	budoucí	k2eAgInSc4d1	budoucí
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
čas	čas	k1gInSc4	čas
předbudoucí	předbudoucí	k2eAgInSc4d1	předbudoucí
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vyjadřování	vyjadřování	k1gNnSc3	vyjadřování
přítomných	přítomný	k2eAgInPc2d1	přítomný
dějů	děj	k1gInPc2	děj
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
čas	čas	k1gInSc1	čas
přítomný	přítomný	k2eAgInSc1d1	přítomný
a	a	k8xC	a
čas	čas	k1gInSc1	čas
předpřítomný	předpřítomný	k2eAgInSc1d1	předpřítomný
<g/>
,	,	kIx,	,
k	k	k7c3	k
vyjadřování	vyjadřování	k1gNnSc3	vyjadřování
minulosti	minulost	k1gFnSc2	minulost
časy	čas	k1gInPc4	čas
předpřítomný	předpřítomný	k2eAgInSc1d1	předpřítomný
<g/>
,	,	kIx,	,
minulý	minulý	k2eAgInSc1d1	minulý
a	a	k8xC	a
předminulý	předminulý	k2eAgInSc1d1	předminulý
<g/>
,	,	kIx,	,
k	k	k7c3	k
vyjadřování	vyjadřování	k1gNnSc3	vyjadřování
budoucnosti	budoucnost	k1gFnSc2	budoucnost
časy	čas	k1gInPc1	čas
budoucí	budoucí	k2eAgInPc1d1	budoucí
<g/>
,	,	kIx,	,
předbudoucí	předbudoucí	k2eAgInPc1d1	předbudoucí
a	a	k8xC	a
také	také	k9	také
průběhové	průběhový	k2eAgFnPc4d1	průběhová
formy	forma	k1gFnPc4	forma
času	čas	k1gInSc2	čas
přítomného	přítomný	k2eAgInSc2d1	přítomný
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
Přítomnost	přítomnost	k1gFnSc1	přítomnost
He	he	k0	he
lives	lives	k1gMnSc1	lives
in	in	k?	in
Paris	Paris	k1gMnSc1	Paris
<g/>
.	.	kIx.	.
</s>
<s>
It	It	k?	It
is	is	k?	is
raining	raining	k1gInSc1	raining
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Přítomný	přítomný	k2eAgInSc1d1	přítomný
čas	čas	k1gInSc1	čas
prostý	prostý	k2eAgInSc1d1	prostý
a	a	k8xC	a
průběhový	průběhový	k2eAgInSc1d1	průběhový
<g/>
)	)	kIx)	)
I	i	k9	i
have	have	k6eAd1	have
had	had	k1gMnSc1	had
the	the	k?	the
bike	bike	k1gNnPc2	bike
for	forum	k1gNnPc2	forum
two	two	k?	two
years	years	k6eAd1	years
<g/>
.	.	kIx.	.
</s>
<s>
We	We	k?	We
have	havat	k5eAaPmIp3nS	havat
been	been	k1gInSc1	been
waiting	waiting	k1gInSc1	waiting
here	here	k1gFnPc2	here
for	forum	k1gNnPc2	forum
two	two	k?	two
hours	hours	k6eAd1	hours
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Předpřítomný	Předpřítomný	k2eAgInSc1d1	Předpřítomný
čas	čas	k1gInSc1	čas
prostý	prostý	k2eAgInSc1d1	prostý
a	a	k8xC	a
průběhový	průběhový	k2eAgInSc1d1	průběhový
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Minulost	minulost	k1gFnSc1	minulost
<g/>
:	:	kIx,	:
She	She	k1gFnSc1	She
has	hasit	k5eAaImRp2nS	hasit
translated	translated	k1gInSc1	translated
the	the	k?	the
whole	whole	k1gInSc1	whole
manual	manual	k1gMnSc1	manual
for	forum	k1gNnPc2	forum
us	us	k?	us
<g/>
.	.	kIx.	.
</s>
<s>
Mr	Mr	k?	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Black	Black	k1gMnSc1	Black
moved	moved	k1gMnSc1	moved
two	two	k?	two
weeks	weeks	k6eAd1	weeks
ago	aga	k1gMnSc5	aga
<g/>
.	.	kIx.	.
</s>
<s>
They	Thea	k1gFnPc1	Thea
were	wer	k1gInSc2	wer
travelling	travelling	k1gInSc1	travelling
when	when	k1gInSc1	when
the	the	k?	the
message	messagat	k5eAaPmIp3nS	messagat
arrived	arrived	k1gInSc1	arrived
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
had	had	k1gMnSc1	had
seen	seen	k1gMnSc1	seen
him	him	k?	him
before	befor	k1gInSc5	befor
he	he	k0	he
left	left	k5eAaPmF	left
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Predpřítomný	Predpřítomný	k2eAgInSc4d1	Predpřítomný
čas	čas	k1gInSc4	čas
prostý	prostý	k2eAgInSc4d1	prostý
<g/>
,	,	kIx,	,
minulý	minulý	k2eAgInSc4d1	minulý
čas	čas	k1gInSc4	čas
prostý	prostý	k2eAgInSc4d1	prostý
a	a	k8xC	a
průběhový	průběhový	k2eAgInSc4d1	průběhový
<g/>
,	,	kIx,	,
předminulý	předminulý	k2eAgInSc4d1	předminulý
čas	čas	k1gInSc4	čas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Budoucnost	budoucnost	k1gFnSc1	budoucnost
<g/>
:	:	kIx,	:
It	It	k1gMnSc1	It
will	will	k1gMnSc1	will
be	be	k?	be
interesting	interesting	k1gInSc1	interesting
<g/>
.	.	kIx.	.
</s>
<s>
She	She	k?	She
will	will	k1gInSc1	will
be	be	k?	be
reading	reading	k1gInSc1	reading
in	in	k?	in
the	the	k?	the
afternoon	afternoon	k1gInSc1	afternoon
<g/>
.	.	kIx.	.
</s>
<s>
We	We	k?	We
are	ar	k1gInSc5	ar
playing	playing	k1gInSc4	playing
in	in	k?	in
Hungary	Hungara	k1gFnSc2	Hungara
next	nexta	k1gFnPc2	nexta
Sunday	Sundaa	k1gFnSc2	Sundaa
<g/>
.	.	kIx.	.
</s>
<s>
Tom	Tom	k1gMnSc1	Tom
is	is	k?	is
going	going	k1gMnSc1	going
to	ten	k3xDgNnSc1	ten
study	stud	k1gInPc1	stud
history	histor	k1gInPc1	histor
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
will	will	k1gMnSc1	will
have	hav	k1gFnSc2	hav
finished	finished	k1gMnSc1	finished
the	the	k?	the
repair	repair	k1gMnSc1	repair
by	by	kYmCp3nS	by
the	the	k?	the
end	end	k?	end
of	of	k?	of
May	May	k1gMnSc1	May
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Budoucí	budoucí	k2eAgInSc1d1	budoucí
čas	čas	k1gInSc1	čas
prostý	prostý	k2eAgInSc1d1	prostý
a	a	k8xC	a
průběhový	průběhový	k2eAgInSc1d1	průběhový
<g/>
,	,	kIx,	,
přítomný	přítomný	k2eAgInSc1d1	přítomný
čas	čas	k1gInSc1	čas
průběhový	průběhový	k2eAgInSc1d1	průběhový
<g/>
,	,	kIx,	,
model	model	k1gInSc1	model
"	"	kIx"	"
<g/>
be	be	k?	be
going	going	k1gMnSc1	going
to	ten	k3xDgNnSc4	ten
<g/>
"	"	kIx"	"
-	-	kIx~	-
gramaticky	gramaticky	k6eAd1	gramaticky
rovněž	rovněž	k9	rovněž
přítomný	přítomný	k2eAgInSc1d1	přítomný
čas	čas	k1gInSc1	čas
průběhový	průběhový	k2eAgInSc1d1	průběhový
<g/>
,	,	kIx,	,
čas	čas	k1gInSc1	čas
předbudoucí	předbudoucí	k2eAgInSc1d1	předbudoucí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Future	Futur	k1gMnSc5	Futur
Forms	Forms	k1gInSc4	Forms
V	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc1	několik
tvarů	tvar	k1gInPc2	tvar
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vztahovat	vztahovat	k5eAaImF	vztahovat
k	k	k7c3	k
budoucnosti	budoucnost	k1gFnSc3	budoucnost
-	-	kIx~	-
<g/>
will	will	k1gMnSc1	will
<g/>
,	,	kIx,	,
going	going	k1gMnSc1	going
to	ten	k3xDgNnSc4	ten
a	a	k8xC	a
přítomný	přítomný	k2eAgInSc1d1	přítomný
čas	čas	k1gInSc1	čas
průběhový	průběhový	k2eAgInSc1d1	průběhový
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
nebo	nebo	k8xC	nebo
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
v	v	k7c6	v
jistotě	jistota	k1gFnSc6	jistota
<g/>
;	;	kIx,	;
mluvčí	mluvčí	k1gMnPc1	mluvčí
volí	volit	k5eAaImIp3nP	volit
budoucí	budoucí	k2eAgInSc4d1	budoucí
tvar	tvar	k1gInSc4	tvar
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
učiněno	učiněn	k2eAgNnSc1d1	učiněno
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
a	a	k8xC	a
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
mluvčí	mluvčí	k1gFnSc1	mluvčí
na	na	k7c4	na
budoucí	budoucí	k2eAgFnSc4d1	budoucí
událost	událost	k1gFnSc4	událost
dívá	dívat	k5eAaImIp3nS	dívat
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Předvídání	předvídání	k1gNnSc1	předvídání
(	(	kIx(	(
<g/>
will	will	k1gMnSc1	will
<g/>
,	,	kIx,	,
going	going	k1gMnSc1	going
to	ten	k3xDgNnSc4	ten
<g/>
)	)	kIx)	)
Will	Will	k1gInSc1	Will
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
používá	používat	k5eAaImIp3nS	používat
jako	jako	k8xS	jako
pomocné	pomocný	k2eAgNnSc1d1	pomocné
sloveso	sloveso	k1gNnSc1	sloveso
ukazující	ukazující	k2eAgNnSc1d1	ukazující
na	na	k7c4	na
budoucí	budoucí	k2eAgInSc4d1	budoucí
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
budoucí	budoucí	k2eAgInSc4d1	budoucí
fakt	fakt	k1gInSc4	fakt
nebo	nebo	k8xC	nebo
předvídání	předvídání	k1gNnSc1	předvídání
(	(	kIx(	(
<g/>
We	We	k1gFnSc1	We
will	willa	k1gFnPc2	willa
be	be	k?	be
away	away	k1gInPc1	away
for	forum	k1gNnPc2	forum
two	two	k?	two
weeks	weeks	k6eAd1	weeks
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Will	Will	k1gInSc1	Will
použité	použitý	k2eAgFnSc2d1	použitá
pro	pro	k7c4	pro
předvídání	předvídání	k1gNnSc4	předvídání
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
založeno	založit	k5eAaPmNgNnS	založit
více	hodně	k6eAd2	hodně
na	na	k7c6	na
názoru	názor	k1gInSc6	názor
než	než	k8xS	než
na	na	k7c6	na
faktu	fakt	k1gInSc6	fakt
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
I	i	k9	i
think	think	k6eAd1	think
Laura	Laura	k1gFnSc1	Laura
will	willa	k1gFnPc2	willa
do	do	k7c2	do
very	vera	k1gFnSc2	vera
well	well	k1gInSc1	well
in	in	k?	in
her	hra	k1gFnPc2	hra
exams	exams	k6eAd1	exams
<g/>
.	.	kIx.	.
</s>
<s>
She	She	k?	She
works	works	k6eAd1	works
hard	hard	k6eAd1	hard
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Going	Going	k1gMnSc1	Going
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
také	také	k9	také
může	moct	k5eAaImIp3nS	moct
použít	použít	k5eAaPmF	použít
u	u	k7c2	u
předvídání	předvídání	k1gNnSc2	předvídání
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
přítomném	přítomný	k2eAgInSc6d1	přítomný
faktu	fakt	k1gInSc6	fakt
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
nějaký	nějaký	k3yIgInSc4	nějaký
důkaz	důkaz	k1gInSc4	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
něco	něco	k3yInSc1	něco
určitě	určitě	k6eAd1	určitě
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
She	She	k1gMnSc1	She
is	is	k?	is
going	going	k1gMnSc1	going
to	ten	k3xDgNnSc4	ten
have	have	k1gNnSc4	have
a	a	k8xC	a
baby	baby	k1gNnSc4	baby
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
dítě	dítě	k1gNnSc1	dítě
<g/>
.	.	kIx.	.
-	-	kIx~	-
Je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
těhotná	těhotný	k2eAgFnSc1d1	těhotná
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Někdy	někdy	k6eAd1	někdy
není	být	k5eNaImIp3nS	být
mezi	mezi	k7c4	mezi
will	will	k1gInSc4	will
a	a	k8xC	a
going	going	k1gInSc4	going
to	ten	k3xDgNnSc1	ten
žádný	žádný	k3yNgInSc1	žádný
rozdíl	rozdíl	k1gInSc1	rozdíl
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
a	a	k8xC	a
záměr	záměr	k1gInSc1	záměr
(	(	kIx(	(
<g/>
will	will	k1gMnSc1	will
<g/>
,	,	kIx,	,
going	going	k1gMnSc1	going
to	ten	k3xDgNnSc4	ten
<g/>
)	)	kIx)	)
Will	Will	k1gInSc1	Will
se	se	k3xPyFc4	se
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
modální	modální	k2eAgNnSc4d1	modální
pomocné	pomocný	k2eAgNnSc4d1	pomocné
sloveso	sloveso	k1gNnSc4	sloveso
k	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
<g/>
,	,	kIx,	,
záměru	záměr	k1gInSc2	záměr
nebo	nebo	k8xC	nebo
nabídky	nabídka	k1gFnSc2	nabídka
vytvořené	vytvořený	k2eAgFnSc2d1	vytvořená
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
mluvení	mluvení	k1gNnSc1	mluvení
(	(	kIx(	(
<g/>
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
ll	ll	k?	ll
have	have	k1gInSc1	have
the	the	k?	the
steak	steak	k1gInSc1	steak
please	please	k6eAd1	please
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Going	Going	k1gMnSc1	Going
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
budoucího	budoucí	k2eAgInSc2d1	budoucí
plánu	plán	k1gInSc2	plán
<g/>
,	,	kIx,	,
záměru	záměr	k1gInSc2	záměr
nebo	nebo	k8xC	nebo
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
vytvořeného	vytvořený	k2eAgNnSc2d1	vytvořené
před	před	k7c7	před
okamžikem	okamžik	k1gInSc7	okamžik
mluvení	mluvení	k1gNnSc2	mluvení
(	(	kIx(	(
<g/>
When	When	k1gInSc1	When
I	i	k8xC	i
grow	grow	k?	grow
up	up	k?	up
<g/>
,	,	kIx,	,
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
going	going	k1gMnSc1	going
to	ten	k3xDgNnSc4	ten
be	be	k?	be
a	a	k8xC	a
doctor	doctor	k1gMnSc1	doctor
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Dohody	dohoda	k1gFnPc1	dohoda
(	(	kIx(	(
<g/>
přítomný	přítomný	k2eAgInSc1d1	přítomný
čas	čas	k1gInSc1	čas
průběhový	průběhový	k2eAgInSc1d1	průběhový
<g/>
)	)	kIx)	)
Přítomný	přítomný	k2eAgInSc1d1	přítomný
čas	čas	k1gInSc1	čas
průběhový	průběhový	k2eAgInSc1d1	průběhový
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
použít	použít	k5eAaPmF	použít
k	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
budoucí	budoucí	k2eAgFnSc2d1	budoucí
dohody	dohoda	k1gFnSc2	dohoda
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
k	k	k7c3	k
blízké	blízký	k2eAgFnSc3d1	blízká
budoucnosti	budoucnost	k1gFnSc3	budoucnost
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
We	We	k1gFnSc1	We
<g/>
'	'	kIx"	'
<g/>
re	re	k9	re
going	going	k1gMnSc1	going
out	out	k?	out
with	with	k1gMnSc1	with
Jeremy	Jerema	k1gFnSc2	Jerema
tonight	tonight	k1gMnSc1	tonight
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Někdy	někdy	k6eAd1	někdy
není	být	k5eNaImIp3nS	být
rozdíl	rozdíl	k1gInSc4	rozdíl
<g />
.	.	kIx.	.
</s>
<s>
mezi	mezi	k7c7	mezi
odsouhlasenou	odsouhlasený	k2eAgFnSc7d1	odsouhlasená
dohodou	dohoda	k1gFnSc7	dohoda
(	(	kIx(	(
<g/>
přítomný	přítomný	k2eAgInSc1d1	přítomný
čas	čas	k1gInSc1	čas
průběhový	průběhový	k2eAgInSc1d1	průběhový
<g/>
)	)	kIx)	)
a	a	k8xC	a
záměrem	záměr	k1gInSc7	záměr
(	(	kIx(	(
<g/>
going	going	k1gMnSc1	going
to	ten	k3xDgNnSc4	ten
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
We	We	k1gFnSc1	We
<g/>
'	'	kIx"	'
<g/>
re	re	k9	re
going	going	k1gInSc1	going
to	ten	k3xDgNnSc4	ten
get	get	k?	get
/	/	kIx~	/
we	we	k?	we
<g/>
'	'	kIx"	'
<g/>
re	re	k9	re
getting	getting	k1gInSc1	getting
married	married	k1gInSc1	married
in	in	k?	in
the	the	k?	the
spring	spring	k1gInSc1	spring
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Trpný	trpný	k2eAgInSc1d1	trpný
rod	rod	k1gInSc1	rod
(	(	kIx(	(
<g/>
passive	passivat	k5eAaPmIp3nS	passivat
voice	voice	k1gFnSc1	voice
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
příslušného	příslušný	k2eAgInSc2d1	příslušný
tvaru	tvar	k1gInSc2	tvar
slovesa	sloveso	k1gNnSc2	sloveso
to	ten	k3xDgNnSc4	ten
be	be	k?	be
a	a	k8xC	a
příčestí	příčestí	k1gNnSc2	příčestí
trpného	trpný	k2eAgNnSc2d1	trpné
(	(	kIx(	(
<g/>
past	past	k1gFnSc4	past
participle	participle	k6eAd1	participle
<g/>
)	)	kIx)	)
významového	významový	k2eAgNnSc2d1	významové
slovesa	sloveso	k1gNnSc2	sloveso
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
story	story	k1gFnSc2	story
was	was	k?	was
written	written	k2eAgMnSc1d1	written
by	by	k9	by
my	my	k3xPp1nPc1	my
friend	friend	k1gMnSc1	friend
<g/>
.	.	kIx.	.
</s>
<s>
We	We	k?	We
are	ar	k1gInSc5	ar
being	being	k1gInSc4	being
sent	sent	k2eAgInSc1d1	sent
lots	lots	k1gInSc1	lots
of	of	k?	of
unwanted	unwanted	k1gInSc1	unwanted
messages	messages	k1gInSc1	messages
-	-	kIx~	-
Je	být	k5eAaImIp3nS	být
nám	my	k3xPp1nPc3	my
zasíláno	zasílán	k2eAgNnSc1d1	zasíláno
mnoho	mnoho	k4c1	mnoho
nežádoucích	žádoucí	k2eNgNnPc2d1	nežádoucí
sdělení	sdělení	k1gNnPc2	sdělení
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vyjadřování	vyjadřování	k1gNnSc4	vyjadřování
dějů	děj	k1gInPc2	děj
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
určitou	určitý	k2eAgFnSc4d1	určitá
délku	délka	k1gFnSc4	délka
trvání	trvání	k1gNnSc2	trvání
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
průběhové	průběhový	k2eAgFnPc1d1	průběhová
formy	forma	k1gFnPc1	forma
slovesných	slovesný	k2eAgInPc2d1	slovesný
časů	čas	k1gInPc2	čas
(	(	kIx(	(
<g/>
continunous	continunous	k1gInSc1	continunous
<g/>
,	,	kIx,	,
progressive	progressivat	k5eAaPmIp3nS	progressivat
tenses	tenses	k1gMnSc1	tenses
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
složeny	složit	k5eAaPmNgFnP	složit
z	z	k7c2	z
pomocného	pomocný	k2eAgNnSc2d1	pomocné
slovesa	sloveso	k1gNnSc2	sloveso
to	ten	k3xDgNnSc1	ten
be	be	k?	be
v	v	k7c6	v
příslušném	příslušný	k2eAgInSc6d1	příslušný
čase	čas	k1gInSc6	čas
a	a	k8xC	a
příčestí	příčestí	k1gNnSc6	příčestí
přítomného	přítomný	k2eAgNnSc2d1	přítomné
významového	významový	k2eAgNnSc2d1	významové
slovesa	sloveso	k1gNnSc2	sloveso
<g/>
:	:	kIx,	:
We	We	k1gFnSc1	We
were	werat	k5eAaPmIp3nS	werat
walking	walking	k1gInSc4	walking
across	acrossa	k1gFnPc2	acrossa
the	the	k?	the
field	field	k1gMnSc1	field
when	when	k1gMnSc1	when
we	we	k?	we
were	werat	k5eAaPmIp3nS	werat
attacked	attacked	k1gInSc4	attacked
by	by	kYmCp3nP	by
a	a	k8xC	a
bull	bulla	k1gFnPc2	bulla
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
<g/>
'	'	kIx"	'
<g/>
ve	v	k7c4	v
been	been	k1gInSc4	been
learning	learning	k1gInSc4	learning
English	English	k1gInSc4	English
for	forum	k1gNnPc2	forum
years	years	k6eAd1	years
<g/>
.	.	kIx.	.
</s>
<s>
What	What	k1gMnSc1	What
are	ar	k1gInSc5	ar
you	you	k?	you
doing	doing	k1gInSc4	doing
on	on	k3xPp3gMnSc1	on
your	your	k1gMnSc1	your
hands	handsa	k1gFnPc2	handsa
and	and	k?	and
knees	kneesa	k1gFnPc2	kneesa
<g/>
?	?	kIx.	?
</s>
<s>
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
phone	phonout	k5eAaPmIp3nS	phonout
at	at	k?	at
8.00	[number]	k4	8.00
<g/>
.	.	kIx.	.
</s>
<s>
We	We	k?	We
<g/>
'	'	kIx"	'
<g/>
ll	ll	k?	ll
be	be	k?	be
eating	eating	k1gInSc1	eating
<g/>
.	.	kIx.	.
</s>
<s>
Průběhový	průběhový	k2eAgInSc1d1	průběhový
tvar	tvar	k1gInSc1	tvar
klade	klást	k5eAaImIp3nS	klást
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
činnosti	činnost	k1gFnSc2	činnost
<g/>
:	:	kIx,	:
Jsme	být	k5eAaImIp1nP	být
<g/>
-li	i	k?	-li
si	se	k3xPyFc3	se
vědomi	vědom	k2eAgMnPc1d1	vědom
doby	doba	k1gFnSc2	doba
mezi	mezi	k7c7	mezi
začátkem	začátek	k1gInSc7	začátek
a	a	k8xC	a
ukončením	ukončení	k1gNnSc7	ukončení
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
však	však	k9	však
není	být	k5eNaImIp3nS	být
trvalá	trvalý	k2eAgFnSc1d1	trvalá
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
I	i	k9	i
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
staying	staying	k1gInSc1	staying
with	with	k1gInSc1	with
friends	friends	k1gInSc1	friends
until	untit	k5eAaImAgInS	untit
I	i	k9	i
find	find	k?	find
a	a	k8xC	a
flat	flat	k1gMnSc1	flat
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Protože	protože	k8xS	protože
činnost	činnost	k1gFnSc1	činnost
probíhá	probíhat	k5eAaImIp3nS	probíhat
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přerušena	přerušit	k5eAaPmNgFnS	přerušit
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Am	Am	k1gFnSc1	Am
I	i	k8xC	i
disturbing	disturbing	k1gInSc1	disturbing
you	you	k?	you
<g/>
?	?	kIx.	?
</s>
<s>
No	no	k9	no
<g/>
.	.	kIx.	.
</s>
<s>
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
just	just	k6eAd1	just
doing	doing	k1gInSc1	doing
the	the	k?	the
ironing	ironing	k1gInSc1	ironing
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Činnost	činnost	k1gFnSc1	činnost
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
ukončena	ukončit	k5eAaPmNgFnS	ukončit
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Who	Who	k1gFnSc1	Who
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
been	been	k1gInSc1	been
drinking	drinking	k1gInSc1	drinking
my	my	k3xPp1nPc1	my
beer	beer	k1gMnSc1	beer
<g/>
?	?	kIx.	?
</s>
<s>
-	-	kIx~	-
Nějaké	nějaký	k3yIgNnSc1	nějaký
zbylo	zbýt	k5eAaPmAgNnS	zbýt
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Činnosti	činnost	k1gFnPc4	činnost
označované	označovaný	k2eAgFnPc4d1	označovaná
určitými	určitý	k2eAgInPc7d1	určitý
slovesy	sloveso	k1gNnPc7	sloveso
trvají	trvat	k5eAaImIp3nP	trvat
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
např.	např.	kA	např.
live	live	k1gInSc1	live
<g/>
,	,	kIx,	,
work	work	k1gInSc1	work
<g/>
,	,	kIx,	,
play	play	k0	play
<g/>
;	;	kIx,	;
použití	použití	k1gNnSc2	použití
průběhového	průběhový	k2eAgInSc2d1	průběhový
tvaru	tvar	k1gInSc2	tvar
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
činnost	činnost	k1gFnSc1	činnost
byla	být	k5eAaImAgFnS	být
dočasná	dočasný	k2eAgFnSc1d1	dočasná
<g/>
,	,	kIx,	,
omezená	omezený	k2eAgFnSc1d1	omezená
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Hans	Hans	k1gMnSc1	Hans
is	is	k?	is
living	living	k1gInSc1	living
in	in	k?	in
London	London	k1gMnSc1	London
while	while	k6eAd1	while
he	he	k0	he
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
learning	learning	k1gInSc1	learning
English	English	k1gInSc4	English
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Modální	modální	k2eAgNnPc4d1	modální
slovesa	sloveso	k1gNnPc4	sloveso
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
oproti	oproti	k7c3	oproti
ostatním	ostatní	k2eAgNnPc3d1	ostatní
slovesům	sloveso	k1gNnPc3	sloveso
specifická	specifický	k2eAgNnPc4d1	specifické
pevná	pevný	k2eAgNnPc4d1	pevné
pravidla	pravidlo	k1gNnPc4	pravidlo
užití	užití	k1gNnSc2	užití
<g/>
.	.	kIx.	.
</s>
<s>
Nemají	mít	k5eNaImIp3nP	mít
tvary	tvar	k1gInPc4	tvar
infinitivu	infinitiv	k1gInSc2	infinitiv
a	a	k8xC	a
nahrazují	nahrazovat	k5eAaImIp3nP	nahrazovat
se	se	k3xPyFc4	se
v	v	k7c6	v
takových	takový	k3xDgInPc6	takový
případech	případ	k1gInPc6	případ
opisnými	opisný	k2eAgInPc7d1	opisný
tvary	tvar	k1gInPc7	tvar
<g/>
:	:	kIx,	:
can	can	k?	can
-	-	kIx~	-
be	be	k?	be
able	ablat	k5eAaPmIp3nS	ablat
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
must	must	k5eAaPmF	must
-	-	kIx~	-
have	havat	k5eAaPmIp3nS	havat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
may	may	k?	may
-	-	kIx~	-
be	be	k?	be
allowed	allowed	k1gInSc1	allowed
to	ten	k3xDgNnSc1	ten
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
modální	modální	k2eAgNnPc4d1	modální
slovesa	sloveso	k1gNnPc4	sloveso
patří	patřit	k5eAaImIp3nS	patřit
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
způsobu	způsob	k1gInSc3	způsob
použití	použití	k1gNnSc2	použití
i	i	k8xC	i
could	could	k1gMnSc1	could
<g/>
,	,	kIx,	,
might	might	k1gMnSc1	might
<g/>
,	,	kIx,	,
shall	shall	k1gMnSc1	shall
<g/>
,	,	kIx,	,
should	should	k1gMnSc1	should
<g/>
,	,	kIx,	,
will	will	k1gMnSc1	will
a	a	k8xC	a
would	would	k1gMnSc1	would
<g/>
.	.	kIx.	.
přechodník	přechodník	k1gInSc1	přechodník
přítomný	přítomný	k2eAgInSc1d1	přítomný
:	:	kIx,	:
asking	asking	k1gInSc1	asking
-	-	kIx~	-
tázaje	tázat	k5eAaImSgInS	tázat
se	se	k3xPyFc4	se
[	[	kIx(	[
<g/>
Sitting	Sitting	k1gInSc1	Sitting
in	in	k?	in
the	the	k?	the
hall	halnout	k5eAaPmAgMnS	halnout
<g/>
,	,	kIx,	,
he	he	k0	he
stared	stared	k1gMnSc1	stared
at	at	k?	at
the	the	k?	the
telephone	telephon	k1gInSc5	telephon
<g/>
.	.	kIx.	.
-	-	kIx~	-
Když	když	k8xS	když
<g />
.	.	kIx.	.
</s>
<s>
seděl	sedět	k5eAaImAgMnS	sedět
v	v	k7c6	v
hale	hala	k1gFnSc6	hala
<g/>
,	,	kIx,	,
díval	dívat	k5eAaImAgMnS	dívat
se	se	k3xPyFc4	se
upřeně	upřeně	k6eAd1	upřeně
na	na	k7c4	na
telefon	telefon	k1gInSc4	telefon
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
<g/>
;	;	kIx,	;
přechodník	přechodník	k1gInSc1	přechodník
minulý	minulý	k2eAgInSc1d1	minulý
:	:	kIx,	:
having	having	k1gInSc1	having
asked	asked	k1gInSc1	asked
-	-	kIx~	-
tázav	tázav	k1gInSc1	tázav
se	s	k7c7	s
[	[	kIx(	[
<g/>
Having	Having	k1gInSc1	Having
finished	finished	k1gInSc1	finished
his	his	k1gNnSc6	his
work	work	k1gInSc1	work
<g/>
,	,	kIx,	,
he	he	k0	he
went	went	k1gInSc4	went
home	home	k1gNnPc7	home
<g/>
.	.	kIx.	.
-	-	kIx~	-
Když	když	k8xS	když
skončil	skončit	k5eAaPmAgMnS	skončit
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
šel	jít	k5eAaImAgMnS	jít
domů	domů	k6eAd1	domů
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
<g/>
;	;	kIx,	;
having	having	k1gInSc1	having
been	been	k1gMnSc1	been
asked	asked	k1gMnSc1	asked
-	-	kIx~	-
[	[	kIx(	[
<g/>
Having	Having	k1gInSc1	Having
been	been	k1gMnSc1	been
asked	asked	k1gMnSc1	asked
for	forum	k1gNnPc2	forum
help	help	k1gInSc4	help
<g/>
,	,	kIx,	,
he	he	k0	he
promised	promised	k1gInSc1	promised
to	ten	k3xDgNnSc4	ten
come	comat	k5eAaPmIp3nS	comat
at	at	k?	at
once	once	k1gFnSc1	once
<g/>
.	.	kIx.	.
-	-	kIx~	-
Když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
požádán	požádat	k5eAaPmNgMnS	požádat
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
,	,	kIx,	,
slíbil	slíbit	k5eAaPmAgMnS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hned	hned	k6eAd1	hned
přijde	přijít	k5eAaPmIp3nS	přijít
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Quirk	Quirk	k1gInSc1	Quirk
<g/>
,	,	kIx,	,
R.	R.	kA	R.
<g/>
;	;	kIx,	;
Greenbaum	Greenbaum	k1gInSc1	Greenbaum
<g/>
,	,	kIx,	,
S.	S.	kA	S.
<g/>
;	;	kIx,	;
Leech	Leo	k1gMnPc6	Leo
<g/>
,	,	kIx,	,
G.	G.	kA	G.
<g/>
;	;	kIx,	;
Svartvik	Svartvik	k1gMnSc1	Svartvik
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
:	:	kIx,	:
A	a	k8xC	a
Student	student	k1gMnSc1	student
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Grammar	Grammar	k1gInSc1	Grammar
of	of	k?	of
the	the	k?	the
English	English	k1gInSc4	English
Language	language	k1gFnSc2	language
<g/>
.	.	kIx.	.
</s>
<s>
Longman	Longman	k1gMnSc1	Longman
<g/>
,	,	kIx,	,
Harlow	Harlow	k1gMnSc1	Harlow
<g/>
,	,	kIx,	,
Essex	Essex	k1gInSc1	Essex
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-582-05971-2	[number]	k4	0-582-05971-2
(	(	kIx(	(
<g/>
paperback	paperback	k1gInSc1	paperback
<g/>
)	)	kIx)	)
Biber	Biber	k1gInSc1	Biber
<g/>
,	,	kIx,	,
D.	D.	kA	D.
<g/>
;	;	kIx,	;
Johansson	Johansson	k1gMnSc1	Johansson
<g/>
,	,	kIx,	,
S.	S.	kA	S.
<g/>
;	;	kIx,	;
Leech	Leo	k1gMnPc6	Leo
<g/>
,	,	kIx,	,
G.	G.	kA	G.
<g/>
;	;	kIx,	;
Conrad	Conrad	k1gInSc1	Conrad
<g/>
,	,	kIx,	,
S.	S.	kA	S.
<g/>
;	;	kIx,	;
Finegan	Finegan	k1gMnSc1	Finegan
<g/>
,	,	kIx,	,
E.	E.	kA	E.
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Longman	Longman	k1gMnSc1	Longman
Grammar	Grammar	k1gMnSc1	Grammar
of	of	k?	of
Spoken	Spoken	k1gInSc1	Spoken
and	and	k?	and
Written	Written	k2eAgInSc1d1	Written
English	English	k1gInSc1	English
<g/>
.	.	kIx.	.
</s>
<s>
Longman	Longman	k1gMnSc1	Longman
<g/>
,	,	kIx,	,
Harlow	Harlow	k1gMnSc1	Harlow
<g/>
,	,	kIx,	,
Essex	Essex	k1gInSc1	Essex
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-582-23725-4	[number]	k4	0-582-23725-4
Dušková	Dušková	k1gFnSc1	Dušková
<g/>
,	,	kIx,	,
Libuše	Libuše	k1gFnSc1	Libuše
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Mluvnice	mluvnice	k1gFnSc1	mluvnice
současné	současný	k2eAgFnSc2d1	současná
angličtiny	angličtina	k1gFnSc2	angličtina
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-200-0486-6	[number]	k4	80-200-0486-6
Murphy	Murpha	k1gFnSc2	Murpha
<g/>
,	,	kIx,	,
Raymond	Raymond	k1gMnSc1	Raymond
<g/>
:	:	kIx,	:
Essential	Essential	k1gMnSc1	Essential
Grammar	Grammar	k1gMnSc1	Grammar
in	in	k?	in
Use	usus	k1gInSc5	usus
<g/>
.	.	kIx.	.
</s>
<s>
CUP	cup	k1gInSc1	cup
<g/>
,	,	kIx,	,
Cambridge	Cambridge	k1gFnSc1	Cambridge
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-521-52932-8	[number]	k4	0-521-52932-8
Hewings	Hewingsa	k1gFnPc2	Hewingsa
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
:	:	kIx,	:
Advanced	Advanced	k1gMnSc1	Advanced
Grammar	Grammar	k1gMnSc1	Grammar
in	in	k?	in
Use	usus	k1gInSc5	usus
<g/>
.	.	kIx.	.
</s>
<s>
CUP	cup	k1gInSc1	cup
<g/>
,	,	kIx,	,
Cambridge	Cambridge	k1gFnSc1	Cambridge
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-521-49868-6	[number]	k4	0-521-49868-6
Slovosled	slovosled	k1gInSc4	slovosled
anglické	anglický	k2eAgFnSc2d1	anglická
věty	věta	k1gFnSc2	věta
Tvoření	tvoření	k1gNnSc2	tvoření
slov	slovo	k1gNnPc2	slovo
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
anglická	anglický	k2eAgFnSc1d1	anglická
gramatika	gramatika	k1gFnSc1	gramatika
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Stručný	stručný	k2eAgInSc4d1	stručný
přehled	přehled	k1gInSc4	přehled
anglické	anglický	k2eAgFnSc2d1	anglická
gramatiky	gramatika	k1gFnSc2	gramatika
Přehled	přehled	k1gInSc1	přehled
anglických	anglický	k2eAgInPc2d1	anglický
časů	čas	k1gInPc2	čas
Anglické	anglický	k2eAgInPc4d1	anglický
časy	čas	k1gInPc4	čas
<g/>
,	,	kIx,	,
slovesné	slovesný	k2eAgFnPc4d1	slovesná
formy	forma	k1gFnPc4	forma
<g/>
,	,	kIx,	,
stavba	stavba	k1gFnSc1	stavba
věty	věta	k1gFnSc2	věta
-	-	kIx~	-
online	onlinout	k5eAaPmIp3nS	onlinout
cvičení	cvičení	k1gNnSc4	cvičení
a	a	k8xC	a
pravidla	pravidlo	k1gNnPc4	pravidlo
Elektronická	elektronický	k2eAgFnSc1d1	elektronická
mluvnice	mluvnice	k1gFnSc1	mluvnice
současné	současný	k2eAgFnSc2d1	současná
angličtiny	angličtina	k1gFnSc2	angličtina
-	-	kIx~	-
L.	L.	kA	L.
Dušková	Dušková	k1gFnSc1	Dušková
Výklady	výklad	k1gInPc4	výklad
anglické	anglický	k2eAgFnSc2d1	anglická
gramatiky	gramatika	k1gFnSc2	gramatika
zdarma	zdarma	k6eAd1	zdarma
Testy	test	k1gInPc1	test
anglické	anglický	k2eAgFnSc2d1	anglická
gramatiky	gramatika	k1gFnSc2	gramatika
zdarma	zdarma	k6eAd1	zdarma
na	na	k7c4	na
www.english-test-online.com	www.englishestnline.com	k1gInSc4	www.english-test-online.com
</s>
