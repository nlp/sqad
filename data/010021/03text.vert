<p>
<s>
Diego	Diega	k1gFnSc5	Diega
Armando	Armanda	k1gFnSc5	Armanda
Maradona	Maradon	k1gMnSc4	Maradon
(	(	kIx(	(
<g/>
*	*	kIx~	*
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1960	[number]	k4	1960
Lanús	Lanúsa	k1gFnPc2	Lanúsa
<g/>
,	,	kIx,	,
Buenos	Buenos	k1gMnSc1	Buenos
Aires	Aires	k1gMnSc1	Aires
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
argentinský	argentinský	k2eAgMnSc1d1	argentinský
fotbalista	fotbalista	k1gMnSc1	fotbalista
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
trenér	trenér	k1gMnSc1	trenér
argentinské	argentinský	k2eAgFnSc2d1	Argentinská
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
reprezentace	reprezentace	k1gFnSc2	reprezentace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
vede	vést	k5eAaImIp3nS	vést
tým	tým	k1gInSc1	tým
Al-Fudžajra	Al-Fudžajr	k1gInSc2	Al-Fudžajr
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
a	a	k8xC	a
nejtalentovanějších	talentovaný	k2eAgMnPc2d3	nejtalentovanější
světových	světový	k2eAgMnPc2d1	světový
hráčů	hráč	k1gMnPc2	hráč
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
národním	národní	k2eAgInSc7d1	národní
týmem	tým	k1gInSc7	tým
Argentiny	Argentina	k1gFnSc2	Argentina
zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
Mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
hráčů	hráč	k1gMnPc2	hráč
do	do	k7c2	do
20	[number]	k4	20
let	léto	k1gNnPc2	léto
1979	[number]	k4	1979
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
1986	[number]	k4	1986
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
druhý	druhý	k4xOgMnSc1	druhý
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
1990	[number]	k4	1990
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Pelé	Pelé	k1gNnSc1	Pelé
ho	on	k3xPp3gInSc2	on
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
zařadil	zařadit	k5eAaPmAgInS	zařadit
mezi	mezi	k7c4	mezi
125	[number]	k4	125
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
žijících	žijící	k2eAgMnPc2d1	žijící
fotbalistů	fotbalista	k1gMnPc2	fotbalista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hráčská	hráčský	k2eAgFnSc1d1	hráčská
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Diego	Diego	k1gMnSc1	Diego
Maradona	Maradon	k1gMnSc4	Maradon
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
ve	v	k7c4	v
Villa	Vill	k1gMnSc4	Vill
Fiorito	Fiorita	k1gFnSc5	Fiorita
<g/>
,	,	kIx,	,
chudinské	chudinský	k2eAgFnSc6d1	chudinská
čtvrti	čtvrt	k1gFnSc6	čtvrt
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Argentiny	Argentina	k1gFnSc2	Argentina
Buenos	Buenos	k1gMnSc1	Buenos
Aires	Aires	k1gMnSc1	Aires
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgMnPc4	dva
mladší	mladý	k2eAgMnPc4d2	mladší
bratry	bratr	k1gMnPc4	bratr
<g/>
,	,	kIx,	,
Huga	Hugo	k1gMnSc4	Hugo
a	a	k8xC	a
Eduarda	Eduard	k1gMnSc4	Eduard
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
byli	být	k5eAaImAgMnP	být
rovněž	rovněž	k6eAd1	rovněž
profesionálními	profesionální	k2eAgMnPc7d1	profesionální
fotbalisty	fotbalista	k1gMnPc7	fotbalista
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc1	svůj
debut	debut	k1gInSc1	debut
v	v	k7c6	v
profesionálním	profesionální	k2eAgInSc6d1	profesionální
fotbale	fotbal	k1gInSc6	fotbal
si	se	k3xPyFc3	se
Maradona	Maradona	k1gFnSc1	Maradona
prožil	prožít	k5eAaPmAgInS	prožít
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
Argentinos	Argentinos	k1gInSc1	Argentinos
Juniors	Juniors	k1gInSc1	Juniors
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
deset	deset	k4xCc4	deset
dní	den	k1gInPc2	den
před	před	k7c7	před
svými	svůj	k3xOyFgFnPc7	svůj
šestnáctými	šestnáctý	k4xOgFnPc7	šestnáctý
narozeninami	narozeniny	k1gFnPc7	narozeniny
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
klubu	klub	k1gInSc6	klub
Maradona	Maradon	k1gMnSc2	Maradon
hrál	hrát	k5eAaImAgMnS	hrát
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
do	do	k7c2	do
týmu	tým	k1gInSc2	tým
Boca	Bocum	k1gNnSc2	Bocum
Juniors	Juniorsa	k1gFnPc2	Juniorsa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
změnil	změnit	k5eAaPmAgMnS	změnit
své	svůj	k3xOyFgNnSc4	svůj
působiště	působiště	k1gNnSc4	působiště
znovu	znovu	k6eAd1	znovu
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
se	se	k3xPyFc4	se
stěhoval	stěhovat	k5eAaImAgMnS	stěhovat
do	do	k7c2	do
španělské	španělský	k2eAgFnSc2d1	španělská
Barcelony	Barcelona	k1gFnSc2	Barcelona
<g/>
.	.	kIx.	.
</s>
<s>
Maradona	Maradona	k1gFnSc1	Maradona
byl	být	k5eAaImAgMnS	být
ofenzivní	ofenzivní	k2eAgMnSc1d1	ofenzivní
hráč	hráč	k1gMnSc1	hráč
malého	malý	k2eAgInSc2d1	malý
<g/>
,	,	kIx,	,
podsaditého	podsaditý	k2eAgInSc2d1	podsaditý
vzrůstu	vzrůst	k1gInSc2	vzrůst
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
krátké	krátký	k2eAgFnPc1d1	krátká
silné	silný	k2eAgFnPc1d1	silná
nohy	noha	k1gFnPc1	noha
a	a	k8xC	a
nízko	nízko	k6eAd1	nízko
položené	položený	k2eAgNnSc1d1	položené
těžiště	těžiště	k1gNnSc1	těžiště
mu	on	k3xPp3gMnSc3	on
dávaly	dávat	k5eAaImAgFnP	dávat
výhodu	výhoda	k1gFnSc4	výhoda
v	v	k7c6	v
krátkých	krátký	k2eAgInPc6d1	krátký
sprintech	sprint	k1gInPc6	sprint
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
byl	být	k5eAaImAgMnS	být
Maradona	Maradon	k1gMnSc2	Maradon
geniální	geniální	k2eAgMnSc1d1	geniální
míčový	míčový	k2eAgMnSc1d1	míčový
kouzelník	kouzelník	k1gMnSc1	kouzelník
<g/>
.	.	kIx.	.
</s>
<s>
Hrál	hrát	k5eAaImAgInS	hrát
zpravidla	zpravidla	k6eAd1	zpravidla
levou	levý	k2eAgFnSc7d1	levá
nohou	noha	k1gFnSc7	noha
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
v	v	k7c6	v
situacích	situace	k1gFnPc6	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
poloze	poloha	k1gFnSc3	poloha
míče	míč	k1gInSc2	míč
zdánlivě	zdánlivě	k6eAd1	zdánlivě
vhodnější	vhodný	k2eAgNnSc1d2	vhodnější
hrát	hrát	k5eAaImF	hrát
pravačkou	pravačka	k1gFnSc7	pravačka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
MS	MS	kA	MS
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
MS	MS	kA	MS
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
MS	MS	kA	MS
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
MS	MS	kA	MS
1994	[number]	k4	1994
odehrál	odehrát	k5eAaPmAgInS	odehrát
21	[number]	k4	21
zápasů	zápas	k1gInPc2	zápas
a	a	k8xC	a
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
8	[number]	k4	8
branek	branka	k1gFnPc2	branka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
osobním	osobní	k2eAgInSc6d1	osobní
životě	život	k1gInSc6	život
Maradonu	Maradon	k1gInSc2	Maradon
stíhala	stíhat	k5eAaImAgFnS	stíhat
nejen	nejen	k6eAd1	nejen
obrovská	obrovský	k2eAgFnSc1d1	obrovská
sláva	sláva	k1gFnSc1	sláva
a	a	k8xC	a
uznání	uznání	k1gNnSc1	uznání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
závislost	závislost	k1gFnSc4	závislost
na	na	k7c6	na
kokainu	kokain	k1gInSc6	kokain
a	a	k8xC	a
pohyb	pohyb	k1gInSc1	pohyb
ve	v	k7c6	v
zločineckém	zločinecký	k2eAgNnSc6d1	zločinecké
podsvětí	podsvětí	k1gNnSc6	podsvětí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úspěchy	úspěch	k1gInPc1	úspěch
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Klubové	klubový	k2eAgMnPc4d1	klubový
===	===	k?	===
</s>
</p>
<p>
<s>
vítěz	vítěz	k1gMnSc1	vítěz
argentinské	argentinský	k2eAgFnSc2d1	Argentinská
Primera	primera	k1gFnSc1	primera
División	División	k1gMnSc1	División
(	(	kIx(	(
<g/>
Boca	Boca	k1gMnSc1	Boca
Juniors	Juniorsa	k1gFnPc2	Juniorsa
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vítěz	vítěz	k1gMnSc1	vítěz
španělského	španělský	k2eAgNnSc2d1	španělské
Copa	Copum	k1gNnSc2	Copum
de	de	k?	de
la	la	k1gNnSc2	la
Liga	liga	k1gFnSc1	liga
(	(	kIx(	(
<g/>
Ligový	ligový	k2eAgInSc1d1	ligový
pohár	pohár	k1gInSc1	pohár
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Barcelona	Barcelona	k1gFnSc1	Barcelona
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vítěz	vítěz	k1gMnSc1	vítěz
Copa	Copa	k1gMnSc1	Copa
del	del	k?	del
Rey	Rea	k1gFnSc2	Rea
(	(	kIx(	(
<g/>
Královský	královský	k2eAgInSc4d1	královský
pohár	pohár	k1gInSc4	pohár
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Barcelona	Barcelona	k1gFnSc1	Barcelona
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vítěz	vítěz	k1gMnSc1	vítěz
španělského	španělský	k2eAgMnSc2d1	španělský
Supercopa	Supercop	k1gMnSc2	Supercop
de	de	k?	de
Españ	Españ	k1gMnSc2	Españ
(	(	kIx(	(
<g/>
Barcelona	Barcelona	k1gFnSc1	Barcelona
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vítěz	vítěz	k1gMnSc1	vítěz
italské	italský	k2eAgFnSc2d1	italská
Serie	serie	k1gFnSc2	serie
A	a	k8xC	a
(	(	kIx(	(
<g/>
Neapol	Neapol	k1gFnSc1	Neapol
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
a	a	k8xC	a
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vítěz	vítěz	k1gMnSc1	vítěz
Coppa	Copp	k1gMnSc2	Copp
Italia	Italium	k1gNnSc2	Italium
(	(	kIx(	(
<g/>
Italský	italský	k2eAgInSc1d1	italský
pohár	pohár	k1gInSc1	pohár
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Neapol	Neapol	k1gFnSc1	Neapol
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vítěz	vítěz	k1gMnSc1	vítěz
Supercoppa	Supercopp	k1gMnSc2	Supercopp
Italiana	Italian	k1gMnSc2	Italian
(	(	kIx(	(
<g/>
Italský	italský	k2eAgInSc1d1	italský
superpohár	superpohár	k1gInSc1	superpohár
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Neapol	Neapol	k1gFnSc1	Neapol
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vítěz	vítěz	k1gMnSc1	vítěz
Poháru	pohár	k1gInSc2	pohár
UEFA	UEFA	kA	UEFA
(	(	kIx(	(
<g/>
Neapol	Neapol	k1gFnSc1	Neapol
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Reprezentační	reprezentační	k2eAgMnSc1d1	reprezentační
a	a	k8xC	a
individuální	individuální	k2eAgMnSc1d1	individuální
===	===	k?	===
</s>
</p>
<p>
<s>
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
hráč	hráč	k1gMnSc1	hráč
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
krát	krát	k6eAd1	krát
Fotbalista	fotbalista	k1gMnSc1	fotbalista
roku	rok	k1gInSc2	rok
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
krát	krát	k6eAd1	krát
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
hráč	hráč	k1gMnSc1	hráč
Argentiny	Argentina	k1gFnSc2	Argentina
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
krát	krát	k6eAd1	krát
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
střelec	střelec	k1gMnSc1	střelec
argentinské	argentinský	k2eAgFnSc2d1	Argentinská
ligy	liga	k1gFnSc2	liga
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
−	−	k?	−
22	[number]	k4	22
gólů	gól	k1gInPc2	gól
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
−	−	k?	−
26	[number]	k4	26
gólů	gól	k1gInPc2	gól
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
−	−	k?	−
43	[number]	k4	43
gólů	gól	k1gInPc2	gól
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
střelec	střelec	k1gMnSc1	střelec
italské	italský	k2eAgFnSc2d1	italská
ligy	liga	k1gFnSc2	liga
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
−	−	k?	−
15	[number]	k4	15
gólů	gól	k1gInPc2	gól
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
sportovec	sportovec	k1gMnSc1	sportovec
Argentiny	Argentina	k1gFnSc2	Argentina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Diego	Diego	k6eAd1	Diego
Maradona	Maradona	k1gFnSc1	Maradona
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Diego	Diego	k1gNnSc1	Diego
Maradona	Maradon	k1gMnSc2	Maradon
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Diego	Diego	k1gNnSc1	Diego
Maradona	Maradon	k1gMnSc2	Maradon
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
