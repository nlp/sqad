<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
prezident	prezident	k1gMnSc1	prezident
The	The	k1gMnSc1	The
Motion	Motion	k1gInSc4	Motion
Pictures	Pictures	k1gInSc1	Pictures
Producers	Producers	k1gInSc1	Producers
and	and	k?	and
Distributors	Distributors	k1gInSc1	Distributors
Association	Association	k1gInSc1	Association
(	(	kIx(	(
<g/>
MPPDA	MPPDA	kA	MPPDA
<g/>
)	)	kIx)	)
Will	Will	k1gInSc1	Will
Hays	Haysa	k1gFnPc2	Haysa
proto	proto	k8xC	proto
zavedl	zavést	k5eAaPmAgInS	zavést
Produkční	produkční	k2eAgInSc1d1	produkční
kodex	kodex	k1gInSc1	kodex
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
také	také	k9	také
"	"	kIx"	"
<g/>
Haysův	Haysův	k2eAgInSc1d1	Haysův
kodex	kodex	k1gInSc1	kodex
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vyžadoval	vyžadovat	k5eAaImAgMnS	vyžadovat
plnění	plnění	k1gNnSc4	plnění
určitých	určitý	k2eAgNnPc2d1	určité
předem	předem	k6eAd1	předem
daných	daný	k2eAgNnPc2d1	dané
cenzurních	cenzurní	k2eAgNnPc2d1	cenzurní
opatření	opatření	k1gNnPc2	opatření
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tento	tento	k3xDgInSc1	tento
kodex	kodex	k1gInSc1	kodex
byl	být	k5eAaImAgInS	být
chápán	chápat	k5eAaImNgInS	chápat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
menší	malý	k2eAgNnSc1d2	menší
zlo	zlo	k1gNnSc1	zlo
<g/>
"	"	kIx"	"
a	a	k8xC	a
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
jako	jako	k9	jako
reakce	reakce	k1gFnPc1	reakce
na	na	k7c4	na
možné	možný	k2eAgNnSc4d1	možné
zavedení	zavedení	k1gNnSc4	zavedení
nechtěné	chtěný	k2eNgFnSc2d1	nechtěná
státní	státní	k2eAgFnSc2d1	státní
cenzury	cenzura	k1gFnSc2	cenzura
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
kinematografii	kinematografie	k1gFnSc6	kinematografie
<g/>
.	.	kIx.	.
</s>
