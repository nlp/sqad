<p>
<s>
Beryl	beryl	k1gInSc1	beryl
<g/>
,	,	kIx,	,
chemický	chemický	k2eAgInSc1d1	chemický
vzorec	vzorec	k1gInSc1	vzorec
Be	Be	k1gFnSc2	Be
<g/>
3	[number]	k4	3
<g/>
Al	ala	k1gFnPc2	ala
<g/>
2	[number]	k4	2
<g/>
Si	se	k3xPyFc3	se
<g/>
6	[number]	k4	6
<g/>
O	o	k7c4	o
<g/>
18	[number]	k4	18
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
šesterečný	šesterečný	k2eAgInSc4d1	šesterečný
minerál	minerál	k1gInSc4	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
slova	slovo	k1gNnSc2	slovo
beryllos	beryllos	k1gInSc1	beryllos
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
označovaly	označovat	k5eAaImAgInP	označovat
drahokamy	drahokam	k1gInPc1	drahokam
modrozelené	modrozelený	k2eAgInPc1d1	modrozelený
jako	jako	k8xC	jako
mořská	mořský	k2eAgFnSc1d1	mořská
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Beryl	beryl	k1gInSc1	beryl
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgInSc1d1	jediný
běžný	běžný	k2eAgInSc1d1	běžný
minerál	minerál	k1gInSc1	minerál
Be	Be	k1gFnSc2	Be
<g/>
.	.	kIx.	.
</s>
<s>
Beryllium	Beryllium	k1gNnSc1	Beryllium
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
nekompatibilní	kompatibilní	k2eNgFnSc1d1	nekompatibilní
a	a	k8xC	a
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
vstupovat	vstupovat	k5eAaImF	vstupovat
do	do	k7c2	do
struktury	struktura	k1gFnSc2	struktura
většiny	většina	k1gFnSc2	většina
minerálů	minerál	k1gInPc2	minerál
<g/>
,	,	kIx,	,
při	při	k7c6	při
krystalizaci	krystalizace	k1gFnSc6	krystalizace
magmatických	magmatický	k2eAgFnPc2d1	magmatická
hornin	hornina	k1gFnPc2	hornina
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
hromadí	hromadit	k5eAaImIp3nP	hromadit
v	v	k7c6	v
tavenině	tavenina	k1gFnSc6	tavenina
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
závěrečné	závěrečný	k2eAgFnSc6d1	závěrečná
fázi	fáze	k1gFnSc6	fáze
krystalizace	krystalizace	k1gFnSc2	krystalizace
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
beryl	beryl	k1gInSc1	beryl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
==	==	k?	==
</s>
</p>
<p>
<s>
Magmatický	magmatický	k2eAgMnSc1d1	magmatický
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
berylových	berylový	k2eAgInPc6d1	berylový
pegmatitech	pegmatit	k1gInPc6	pegmatit
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
hydrotermálně	hydrotermálně	k6eAd1	hydrotermálně
pneumatolický	pneumatolický	k2eAgInSc1d1	pneumatolický
(	(	kIx(	(
<g/>
v	v	k7c6	v
greisenech	greisen	k1gInPc6	greisen
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
metamorfní	metamorfní	k2eAgNnSc1d1	metamorfní
(	(	kIx(	(
<g/>
především	především	k9	především
ve	v	k7c6	v
svorech	svor	k1gInPc6	svor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Morfologie	morfologie	k1gFnSc1	morfologie
a	a	k8xC	a
struktura	struktura	k1gFnSc1	struktura
==	==	k?	==
</s>
</p>
<p>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
prizmatické	prizmatický	k2eAgInPc1d1	prizmatický
hexagonální	hexagonální	k2eAgInPc1d1	hexagonální
krystaly	krystal	k1gInPc1	krystal
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
zakončené	zakončený	k2eAgNnSc1d1	zakončené
prostou	prostý	k2eAgFnSc7d1	prostá
bází	báze	k1gFnSc7	báze
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
bývají	bývat	k5eAaImIp3nP	bývat
vyvinuty	vyvinout	k5eAaPmNgFnP	vyvinout
i	i	k9	i
trojúhelnikovité	trojúhelnikovitý	k2eAgFnPc1d1	trojúhelnikovitý
plošky	ploška	k1gFnPc1	ploška
hexagonální	hexagonální	k2eAgFnSc2d1	hexagonální
pyramidy	pyramida	k1gFnSc2	pyramida
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
také	také	k9	také
zrnité	zrnitý	k2eAgInPc4d1	zrnitý
<g/>
,	,	kIx,	,
kompaktní	kompaktní	k2eAgInPc4d1	kompaktní
i	i	k8xC	i
radiálně	radiálně	k6eAd1	radiálně
paprsčité	paprsčitý	k2eAgInPc1d1	paprsčitý
agregáty	agregát	k1gInPc1	agregát
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
berylová	berylový	k2eAgNnPc1d1	berylový
slunce	slunce	k1gNnPc1	slunce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Beryl	beryl	k1gInSc1	beryl
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
cyklosilikáty	cyklosilikát	k1gInPc7	cyklosilikát
<g/>
,	,	kIx,	,
základ	základ	k1gInSc1	základ
jeho	jeho	k3xOp3gFnSc2	jeho
struktury	struktura	k1gFnSc2	struktura
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
šestičetnými	šestičetný	k2eAgInPc7d1	šestičetný
cykly	cyklus	k1gInPc7	cyklus
SiO	SiO	k1gFnSc2	SiO
<g/>
4	[number]	k4	4
tetraedrů	tetraedr	k1gInPc2	tetraedr
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
těchto	tento	k3xDgInPc2	tento
cyklů	cyklus	k1gInPc2	cyklus
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
volné	volný	k2eAgFnPc4d1	volná
prostory	prostora	k1gFnPc4	prostora
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
může	moct	k5eAaImIp3nS	moct
vstupovat	vstupovat	k5eAaImF	vstupovat
Na	na	k7c4	na
<g/>
,	,	kIx,	,
Cs	Cs	k1gMnPc7	Cs
<g/>
,	,	kIx,	,
CO2	CO2	k1gMnPc7	CO2
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
prvky	prvek	k1gInPc4	prvek
či	či	k8xC	či
jednoduché	jednoduchý	k2eAgFnPc4d1	jednoduchá
molekuly	molekula	k1gFnPc4	molekula
<g/>
.	.	kIx.	.
</s>
<s>
Substitucí	substituce	k1gFnSc7	substituce
Sc	Sc	k1gMnPc2	Sc
za	za	k7c2	za
Al	ala	k1gFnPc2	ala
vzniká	vznikat	k5eAaImIp3nS	vznikat
minerál	minerál	k1gInSc1	minerál
bazzit	bazzita	k1gFnPc2	bazzita
<g/>
,	,	kIx,	,
komplexní	komplexní	k2eAgNnSc1d1	komplexní
substitucí	substituce	k1gFnSc7	substituce
Li	li	k9	li
za	za	k7c4	za
Be	Be	k1gFnSc4	Be
a	a	k8xC	a
Cs	Cs	k1gFnSc4	Cs
za	za	k7c4	za
vakanci	vakance	k1gFnSc4	vakance
vzniká	vznikat	k5eAaImIp3nS	vznikat
pezzottait	pezzottait	k1gInSc1	pezzottait
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Fyzikální	fyzikální	k2eAgFnPc1d1	fyzikální
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Tvrdost	tvrdost	k1gFnSc1	tvrdost
7,5	[number]	k4	7,5
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
křehký	křehký	k2eAgInSc1d1	křehký
<g/>
,	,	kIx,	,
hustota	hustota	k1gFnSc1	hustota
2,6	[number]	k4	2,6
<g/>
–	–	k?	–
<g/>
2,8	[number]	k4	2,8
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm3	cm3	k4	cm3
<g/>
,	,	kIx,	,
štěpnost	štěpnost	k1gFnSc1	štěpnost
nedokonalá	dokonalý	k2eNgFnSc1d1	nedokonalá
podle	podle	k7c2	podle
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
lom	lom	k1gInSc1	lom
nerovný	rovný	k2eNgInSc1d1	nerovný
<g/>
,	,	kIx,	,
lasturnatý	lasturnatý	k2eAgInSc1d1	lasturnatý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Optické	optický	k2eAgFnPc1d1	optická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Barva	barva	k1gFnSc1	barva
<g/>
:	:	kIx,	:
bezbarvý	bezbarvý	k2eAgInSc1d1	bezbarvý
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgNnPc1d1	bílé
<g/>
,	,	kIx,	,
žlutobílá	žlutobílý	k2eAgNnPc1d1	žlutobílé
<g/>
,	,	kIx,	,
zlatožlutá	zlatožlutý	k2eAgNnPc1d1	zlatožluté
<g/>
,	,	kIx,	,
žlutozelená	žlutozelený	k2eAgNnPc1d1	žlutozelené
<g/>
,	,	kIx,	,
zelená	zelený	k2eAgNnPc1d1	zelené
<g/>
,	,	kIx,	,
růžová	růžový	k2eAgNnPc1d1	růžové
<g/>
,	,	kIx,	,
zelenomodrá	zelenomodrý	k2eAgNnPc1d1	zelenomodré
<g/>
,	,	kIx,	,
červená	červený	k2eAgNnPc1d1	červené
<g/>
.	.	kIx.	.
</s>
<s>
Lesk	lesk	k1gInSc1	lesk
skelný	skelný	k2eAgInSc1d1	skelný
<g/>
,	,	kIx,	,
matný	matný	k2eAgInSc1d1	matný
<g/>
,	,	kIx,	,
průhlednost	průhlednost	k1gFnSc1	průhlednost
<g/>
:	:	kIx,	:
průhledný	průhledný	k2eAgInSc1d1	průhledný
<g/>
,	,	kIx,	,
průsvitný	průsvitný	k2eAgInSc1d1	průsvitný
<g/>
,	,	kIx,	,
vryp	vryp	k1gInSc1	vryp
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chemické	chemický	k2eAgFnPc1d1	chemická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Složení	složení	k1gNnSc1	složení
<g/>
:	:	kIx,	:
Be	Be	k1gFnSc1	Be
5,03	[number]	k4	5,03
%	%	kIx~	%
<g/>
,	,	kIx,	,
Al	ala	k1gFnPc2	ala
10,04	[number]	k4	10,04
%	%	kIx~	%
<g/>
,	,	kIx,	,
Si	se	k3xPyFc3	se
31,35	[number]	k4	31,35
%	%	kIx~	%
<g/>
,	,	kIx,	,
O	o	k7c6	o
53,58	[number]	k4	53,58
%	%	kIx~	%
<g/>
,	,	kIx,	,
příměsi	příměs	k1gFnSc2	příměs
Fe	Fe	k1gFnSc2	Fe
<g/>
,	,	kIx,	,
Mn	Mn	k1gFnSc2	Mn
<g/>
,	,	kIx,	,
Mg	mg	kA	mg
<g/>
,	,	kIx,	,
Ca	ca	kA	ca
<g/>
,	,	kIx,	,
Cr	cr	k0	cr
<g/>
,	,	kIx,	,
Na	na	k7c4	na
<g/>
,	,	kIx,	,
Li	li	k8xS	li
<g/>
,	,	kIx,	,
Cs	Cs	k1gMnPc1	Cs
<g/>
.	.	kIx.	.
</s>
<s>
Rozpustný	rozpustný	k2eAgInSc1d1	rozpustný
v	v	k7c6	v
HF	HF	kA	HF
<g/>
,	,	kIx,	,
před	před	k7c7	před
dmuchavkou	dmuchavka	k1gFnSc7	dmuchavka
se	se	k3xPyFc4	se
netaví	tavit	k5eNaImIp3nS	tavit
<g/>
.	.	kIx.	.
</s>
<s>
Průhledné	průhledný	k2eAgFnPc1d1	průhledná
odrůdy	odrůda	k1gFnPc1	odrůda
se	s	k7c7	s
prudkým	prudký	k2eAgNnSc7d1	prudké
zahřátím	zahřátí	k1gNnSc7	zahřátí
mléčně	mléčně	k6eAd1	mléčně
zakalí	zakalit	k5eAaPmIp3nS	zakalit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odrůdy	odrůda	k1gFnSc2	odrůda
==	==	k?	==
</s>
</p>
<p>
<s>
obecný	obecný	k2eAgInSc1d1	obecný
beryl	beryl	k1gInSc1	beryl
–	–	k?	–
nejrozšířenější	rozšířený	k2eAgFnSc1d3	nejrozšířenější
odrůda	odrůda	k1gFnSc1	odrůda
<g/>
,	,	kIx,	,
zdroj	zdroj	k1gInSc1	zdroj
Be	Be	k1gFnSc2	Be
</s>
</p>
<p>
<s>
smaragd	smaragd	k1gInSc1	smaragd
–	–	k?	–
zelený	zelený	k2eAgInSc1d1	zelený
</s>
</p>
<p>
<s>
akvamarín	akvamarín	k1gInSc1	akvamarín
–	–	k?	–
zelenomodrý	zelenomodrý	k2eAgInSc1d1	zelenomodrý
až	až	k8xS	až
světle	světle	k6eAd1	světle
modrý	modrý	k2eAgInSc1d1	modrý
(	(	kIx(	(
<g/>
tmavě	tmavě	k6eAd1	tmavě
modré	modrý	k2eAgFnSc2d1	modrá
odrůdy	odrůda	k1gFnSc2	odrůda
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
anglické	anglický	k2eAgFnSc6d1	anglická
literatuře	literatura	k1gFnSc6	literatura
označovány	označovat	k5eAaImNgFnP	označovat
jako	jako	k9	jako
maxixe	maxixe	k1gFnPc1	maxixe
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
morganit	morganit	k1gInSc1	morganit
–	–	k?	–
růžový	růžový	k2eAgInSc1d1	růžový
</s>
</p>
<p>
<s>
vorobjevit	vorobjevit	k1gInSc1	vorobjevit
–	–	k?	–
růžový	růžový	k2eAgInSc1d1	růžový
až	až	k8xS	až
oranžový	oranžový	k2eAgMnSc1d1	oranžový
(	(	kIx(	(
<g/>
existují	existovat	k5eAaImIp3nP	existovat
dohady	dohad	k1gInPc4	dohad
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
vorobjevit	vorobjevit	k1gInSc4	vorobjevit
totožný	totožný	k2eAgInSc4d1	totožný
s	s	k7c7	s
morganitem	morganit	k1gInSc7	morganit
<g/>
,	,	kIx,	,
momentálně	momentálně	k6eAd1	momentálně
proběhly	proběhnout	k5eAaPmAgInP	proběhnout
výzkumy	výzkum	k1gInPc1	výzkum
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
tomu	ten	k3xDgNnSc3	ten
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
stále	stále	k6eAd1	stále
pro	pro	k7c4	pro
komplexní	komplexní	k2eAgNnSc4d1	komplexní
pochopení	pochopení	k1gNnSc4	pochopení
dané	daný	k2eAgFnSc2d1	daná
problematiky	problematika	k1gFnSc2	problematika
bude	být	k5eAaImBp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
dalších	další	k2eAgInPc2d1	další
výzkumů	výzkum	k1gInPc2	výzkum
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
tyto	tento	k3xDgFnPc4	tento
závěry	závěra	k1gFnPc4	závěra
definitivně	definitivně	k6eAd1	definitivně
potvrdit	potvrdit	k5eAaPmF	potvrdit
<g/>
,	,	kIx,	,
či	či	k8xC	či
vyvrátit	vyvrátit	k5eAaPmF	vyvrátit
)	)	kIx)	)
</s>
</p>
<p>
<s>
heliodor	heliodor	k1gInSc1	heliodor
–	–	k?	–
žlutý	žlutý	k2eAgInSc1d1	žlutý
<g/>
,	,	kIx,	,
zlatožlutý	zlatožlutý	k2eAgInSc1d1	zlatožlutý
a	a	k8xC	a
zlatý	zlatý	k2eAgInSc1d1	zlatý
(	(	kIx(	(
<g/>
anglická	anglický	k2eAgFnSc1d1	anglická
literatura	literatura	k1gFnSc1	literatura
někdy	někdy	k6eAd1	někdy
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
heliodor	heliodor	k1gInSc1	heliodor
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
golden	goldno	k1gNnPc2	goldno
beryl	beryl	k1gInSc1	beryl
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
goshenit	goshenit	k5eAaPmF	goshenit
–	–	k?	–
průhledný	průhledný	k2eAgInSc4d1	průhledný
bezbarvý	bezbarvý	k2eAgInSc4d1	bezbarvý
</s>
</p>
<p>
<s>
bixbit	bixbit	k5eAaPmF	bixbit
–	–	k?	–
tmavě	tmavě	k6eAd1	tmavě
červený	červený	k2eAgMnSc1d1	červený
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
ryolitů	ryolit	k1gInPc2	ryolit
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
oblasti	oblast	k1gFnSc6	oblast
v	v	k7c6	v
Utahu	Utah	k1gInSc6	Utah
(	(	kIx(	(
<g/>
neplést	plést	k5eNaImF	plést
s	s	k7c7	s
minerálem	minerál	k1gInSc7	minerál
bixbyitem	bixbyit	k1gMnSc7	bixbyit
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Podobné	podobný	k2eAgInPc4d1	podobný
minerály	minerál	k1gInPc4	minerál
==	==	k?	==
</s>
</p>
<p>
<s>
apatit	apatit	k1gInSc1	apatit
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
fosfát	fosfát	k1gInSc1	fosfát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
šestiboké	šestiboký	k2eAgInPc4d1	šestiboký
prizmatické	prizmatický	k2eAgInPc4d1	prizmatický
krystaly	krystal	k1gInPc4	krystal
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
zelené	zelený	k2eAgInPc1d1	zelený
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
společně	společně	k6eAd1	společně
s	s	k7c7	s
berylem	beryl	k1gInSc7	beryl
(	(	kIx(	(
<g/>
greiseny	greisen	k2eAgInPc1d1	greisen
<g/>
,	,	kIx,	,
pegmatity	pegmatit	k1gInPc1	pegmatit
<g/>
)	)	kIx)	)
a	a	k8xC	a
těžko	těžko	k6eAd1	těžko
se	se	k3xPyFc4	se
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
analýzy	analýza	k1gFnSc2	analýza
může	moct	k5eAaImIp3nS	moct
pomoci	pomoct	k5eAaPmF	pomoct
test	test	k1gInSc4	test
tvrdosti	tvrdost	k1gFnSc2	tvrdost
<g/>
,	,	kIx,	,
beryl	beryl	k1gInSc1	beryl
je	být	k5eAaImIp3nS	být
tvrdší	tvrdý	k2eAgNnSc4d2	tvrdší
než	než	k8xS	než
sklo	sklo	k1gNnSc4	sklo
a	a	k8xC	a
křemen	křemen	k1gInSc1	křemen
<g/>
,	,	kIx,	,
apatit	apatit	k1gInSc1	apatit
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
pezzottait	pezzottait	k1gInSc1	pezzottait
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
minerál	minerál	k1gInSc1	minerál
se	s	k7c7	s
strukturou	struktura	k1gFnSc7	struktura
berylu	beryl	k1gInSc2	beryl
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
část	část	k1gFnSc4	část
Be	Be	k1gFnSc2	Be
nahrazenou	nahrazený	k2eAgFnSc4d1	nahrazená
Cs	Cs	k1gFnSc4	Cs
a	a	k8xC	a
Li	li	k9	li
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
růžový	růžový	k2eAgInSc4d1	růžový
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
krátce	krátce	k6eAd1	krátce
sloupcovité	sloupcovitý	k2eAgInPc1d1	sloupcovitý
růžové	růžový	k2eAgInPc1d1	růžový
krystaly	krystal	k1gInPc1	krystal
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
analýzy	analýza	k1gFnSc2	analýza
nejde	jít	k5eNaImIp3nS	jít
odlišit	odlišit	k5eAaPmF	odlišit
morganit	morganit	k1gInSc4	morganit
od	od	k7c2	od
pezzottaitu	pezzottait	k1gInSc2	pezzottait
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Parageneze	parageneze	k1gFnSc2	parageneze
==	==	k?	==
</s>
</p>
<p>
<s>
křemen	křemen	k1gInSc1	křemen
<g/>
,	,	kIx,	,
živce	živec	k1gInPc1	živec
<g/>
,	,	kIx,	,
muskovit	muskovit	k1gInSc1	muskovit
<g/>
,	,	kIx,	,
lepidolit	lepidolit	k1gInSc1	lepidolit
<g/>
,	,	kIx,	,
spodumen	spodumen	k1gInSc1	spodumen
<g/>
,	,	kIx,	,
amblygonit	amblygonit	k1gInSc1	amblygonit
<g/>
,	,	kIx,	,
turmalín	turmalín	k1gInSc1	turmalín
<g/>
,	,	kIx,	,
topaz	topaz	k1gInSc1	topaz
<g/>
,	,	kIx,	,
kasiterit	kasiterit	k1gInSc1	kasiterit
<g/>
,	,	kIx,	,
columbit	columbit	k5eAaPmF	columbit
<g/>
,	,	kIx,	,
tantalit	tantalit	k1gInSc1	tantalit
</s>
</p>
<p>
<s>
==	==	k?	==
Získávání	získávání	k1gNnSc2	získávání
==	==	k?	==
</s>
</p>
<p>
<s>
Beryl	beryl	k1gInSc1	beryl
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
z	z	k7c2	z
pegmatitů	pegmatit	k1gInPc2	pegmatit
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
ho	on	k3xPp3gMnSc4	on
nalézt	nalézt	k5eAaPmF	nalézt
i	i	k9	i
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
při	při	k7c6	při
sesuvech	sesuv	k1gInPc6	sesuv
půdy	půda	k1gFnSc2	půda
na	na	k7c6	na
příkrých	příkrý	k2eAgNnPc6d1	příkré
pohořích	pohoří	k1gNnPc6	pohoří
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
spíše	spíše	k9	spíše
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
jeho	jeho	k3xOp3gNnSc1	jeho
odrůdy	odrůda	k1gFnPc1	odrůda
(	(	kIx(	(
<g/>
akvamarín	akvamarín	k1gInSc1	akvamarín
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Obecný	obecný	k2eAgInSc1d1	obecný
beryl	beryl	k1gInSc1	beryl
jako	jako	k8xC	jako
zdroj	zdroj	k1gInSc1	zdroj
berylia	berylium	k1gNnSc2	berylium
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
lehkých	lehký	k2eAgFnPc2d1	lehká
slitin	slitina	k1gFnPc2	slitina
<g/>
.	.	kIx.	.
</s>
<s>
Barevné	barevný	k2eAgFnPc4d1	barevná
odrůdy	odrůda	k1gFnPc4	odrůda
v	v	k7c6	v
klenotnictví	klenotnictví	k1gNnSc6	klenotnictví
jako	jako	k8xC	jako
drahý	drahý	k2eAgInSc1d1	drahý
kámen	kámen	k1gInSc1	kámen
(	(	kIx(	(
<g/>
fasetové	fasetový	k2eAgInPc1d1	fasetový
brusy	brus	k1gInPc1	brus
<g/>
,	,	kIx,	,
kabošony	kabošon	k1gInPc1	kabošon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čiré	čirý	k2eAgFnPc1d1	čirá
odrůdy	odrůda	k1gFnPc1	odrůda
se	se	k3xPyFc4	se
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
brousily	brousit	k5eAaImAgInP	brousit
do	do	k7c2	do
optických	optický	k2eAgFnPc2d1	optická
čoček	čočka	k1gFnPc2	čočka
a	a	k8xC	a
používaly	používat	k5eAaImAgFnP	používat
jako	jako	k9	jako
brýle	brýle	k1gFnPc1	brýle
(	(	kIx(	(
<g/>
beryl	beryl	k1gInSc1	beryl
císaře	císař	k1gMnSc2	císař
Nerona	Nero	k1gMnSc2	Nero
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Naleziště	naleziště	k1gNnSc2	naleziště
==	==	k?	==
</s>
</p>
<p>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
řídký	řídký	k2eAgInSc1d1	řídký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Česko	Česko	k1gNnSc4	Česko
–	–	k?	–
Písek	Písek	k1gInSc1	Písek
<g/>
,	,	kIx,	,
Meclov	Meclov	k1gInSc1	Meclov
<g/>
,	,	kIx,	,
Sobotín	Sobotín	k1gInSc1	Sobotín
</s>
</p>
<p>
<s>
Slovensko	Slovensko	k1gNnSc1	Slovensko
–	–	k?	–
Nízké	nízký	k2eAgFnSc2d1	nízká
Tatry	Tatra	k1gFnSc2	Tatra
</s>
</p>
<p>
<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
-	-	kIx~	-
alpský	alpský	k2eAgInSc1d1	alpský
důl	důl	k1gInSc1	důl
v	v	k7c6	v
Habachtalu	Habachtal	k1gMnSc6	Habachtal
těží	těžet	k5eAaImIp3nS	těžet
smaragdy	smaragd	k1gInPc7	smaragd
</s>
</p>
<p>
<s>
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
–	–	k?	–
největší	veliký	k2eAgInSc1d3	veliký
nalezený	nalezený	k2eAgInSc1d1	nalezený
krystal	krystal	k1gInSc1	krystal
<g/>
,	,	kIx,	,
18	[number]	k4	18
m	m	kA	m
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
a	a	k8xC	a
3,5	[number]	k4	3,5
m	m	kA	m
široký	široký	k2eAgInSc4d1	široký
</s>
</p>
<p>
<s>
USA	USA	kA	USA
–	–	k?	–
Jižní	jižní	k2eAgFnSc1d1	jižní
Dakota	Dakota	k1gFnSc1	Dakota
krystaly	krystal	k1gInPc4	krystal
až	až	k6eAd1	až
9	[number]	k4	9
m	m	kA	m
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
</s>
</p>
<p>
<s>
Brazílie	Brazílie	k1gFnSc1	Brazílie
–	–	k?	–
krystaly	krystal	k1gInPc1	krystal
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
až	až	k9	až
200	[number]	k4	200
t	t	k?	t
<g/>
,	,	kIx,	,
akvamaríny	akvamarín	k1gInPc1	akvamarín
<g/>
,	,	kIx,	,
heliodory	heliodor	k1gInPc1	heliodor
i	i	k8xC	i
smaragdy	smaragd	k1gInPc1	smaragd
</s>
</p>
<p>
<s>
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
-	-	kIx~	-
prvotřídní	prvotřídní	k2eAgInPc1d1	prvotřídní
šperkové	šperkový	k2eAgInPc1d1	šperkový
smaragdy	smaragd	k1gInPc1	smaragd
z	z	k7c2	z
lokality	lokalita	k1gFnSc2	lokalita
Muzo	Muzo	k6eAd1	Muzo
</s>
</p>
<p>
<s>
Afghánistán	Afghánistán	k1gInSc1	Afghánistán
-	-	kIx~	-
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
pegmatitů	pegmatit	k1gInPc2	pegmatit
s	s	k7c7	s
akvamaríny	akvamarín	k1gInPc7	akvamarín
a	a	k8xC	a
morganity	morganit	k1gInPc7	morganit
</s>
</p>
<p>
<s>
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
beryl	beryl	k1gInSc1	beryl
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
beryl	beryl	k1gInSc1	beryl
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Beryl	beryl	k1gInSc1	beryl
na	na	k7c6	na
webu	web	k1gInSc6	web
mindat	mindat	k5eAaImF	mindat
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Beryl	beryl	k1gInSc1	beryl
na	na	k7c6	na
webu	web	k1gInSc6	web
Webmineral	Webmineral	k1gFnSc2	Webmineral
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Beryl	beryl	k1gInSc1	beryl
v	v	k7c6	v
atlasu	atlas	k1gInSc6	atlas
minerálů	minerál	k1gInPc2	minerál
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Beryl	beryl	k1gInSc1	beryl
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
odrůdy	odrůda	k1gFnSc2	odrůda
<g/>
,	,	kIx,	,
kompletní	kompletní	k2eAgInSc4d1	kompletní
přehled	přehled	k1gInSc4	přehled
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mineral	Minerat	k5eAaPmAgMnS	Minerat
data	datum	k1gNnSc2	datum
publishing	publishing	k1gInSc1	publishing
(	(	kIx(	(
<g/>
PDF	PDF	kA	PDF
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Aquamarine	Aquamarin	k1gInSc5	Aquamarin
write-up	writep	k1gMnSc1	write-up
(	(	kIx(	(
<g/>
PDF	PDF	kA	PDF
<g/>
)	)	kIx)	)
</s>
</p>
