<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
souhrnně	souhrnně	k6eAd1	souhrnně
nazývá	nazývat	k5eAaImIp3nS	nazývat
soubor	soubor	k1gInSc1	soubor
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
křesťané	křesťan	k1gMnPc1	křesťan
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
posvátné	posvátný	k2eAgNnSc4d1	posvátné
<g/>
?	?	kIx.	?
</s>
