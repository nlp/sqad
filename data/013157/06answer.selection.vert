<s>
Bible	bible	k1gFnSc1	bible
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
τ	τ	k?	τ
β	β	k?	β
ta	ten	k3xDgFnSc1	ten
biblia	biblia	k1gFnSc1	biblia
-	-	kIx~	-
knihy	kniha	k1gFnPc1	kniha
<g/>
,	,	kIx,	,
svitky	svitek	k1gInPc1	svitek
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc1	soubor
starověkých	starověký	k2eAgInPc2d1	starověký
textů	text	k1gInPc2	text
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
křesťanství	křesťanství	k1gNnSc4	křesťanství
a	a	k8xC	a
zčásti	zčásti	k6eAd1	zčásti
i	i	k9	i
judaismus	judaismus	k1gInSc4	judaismus
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
posvátné	posvátný	k2eAgFnPc4d1	posvátná
a	a	k8xC	a
inspirované	inspirovaný	k2eAgFnPc4d1	inspirovaná
Bohem	bůh	k1gMnSc7	bůh
<g/>
.	.	kIx.	.
</s>
