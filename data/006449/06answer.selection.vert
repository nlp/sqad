<s>
Cyklamát	Cyklamát	k1gInSc1	Cyklamát
(	(	kIx(	(
<g/>
též	též	k9	též
označovaný	označovaný	k2eAgMnSc1d1	označovaný
jako	jako	k8xS	jako
cyklamát	cyklamát	k1gMnSc1	cyklamát
sodný	sodný	k2eAgMnSc1d1	sodný
–	–	k?	–
sodium	sodium	k1gNnSc1	sodium
cyclamate	cyclamat	k1gInSc5	cyclamat
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
umělé	umělý	k2eAgNnSc4d1	umělé
náhradní	náhradní	k2eAgNnSc4d1	náhradní
sladidlo	sladidlo	k1gNnSc4	sladidlo
<g/>
,	,	kIx,	,
asi	asi	k9	asi
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
<g/>
×	×	k?	×
sladší	sladký	k2eAgInSc4d2	sladší
než	než	k8xS	než
cukr	cukr	k1gInSc4	cukr
<g/>
.	.	kIx.	.
</s>
