<s>
Cyklamát	Cyklamát	k1gInSc1	Cyklamát
(	(	kIx(	(
<g/>
též	též	k9	též
označovaný	označovaný	k2eAgMnSc1d1	označovaný
jako	jako	k8xS	jako
cyklamát	cyklamát	k1gMnSc1	cyklamát
sodný	sodný	k2eAgMnSc1d1	sodný
–	–	k?	–
sodium	sodium	k1gNnSc1	sodium
cyclamate	cyclamat	k1gInSc5	cyclamat
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
umělé	umělý	k2eAgNnSc4d1	umělé
náhradní	náhradní	k2eAgNnSc4d1	náhradní
sladidlo	sladidlo	k1gNnSc4	sladidlo
<g/>
,	,	kIx,	,
asi	asi	k9	asi
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
<g/>
×	×	k?	×
sladší	sladký	k2eAgInSc4d2	sladší
než	než	k8xS	než
cukr	cukr	k1gInSc4	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
<g/>
[	[	kIx(	[
<g/>
kdo	kdo	k3yQnSc1	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
shledávají	shledávat	k5eAaImIp3nP	shledávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
nepříjemný	příjemný	k2eNgInSc1d1	nepříjemný
chuťový	chuťový	k2eAgInSc1d1	chuťový
účinek	účinek	k1gInSc1	účinek
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
však	však	k9	však
než	než	k8xS	než
sacharin	sacharin	k1gInSc4	sacharin
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
členských	členský	k2eAgInPc6d1	členský
státech	stát	k1gInPc6	stát
EU	EU	kA	EU
se	se	k3xPyFc4	se
cyklamát	cyklamát	k1gMnSc1	cyklamát
označuje	označovat	k5eAaImIp3nS	označovat
na	na	k7c6	na
výrobcích	výrobek	k1gInPc6	výrobek
kódem	kód	k1gInSc7	kód
E	E	kA	E
<g/>
952	[number]	k4	952
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
synteticky	synteticky	k6eAd1	synteticky
reakcí	reakce	k1gFnSc7	reakce
cyklohexylaminu	cyklohexylamin	k2eAgFnSc4d1	cyklohexylamin
s	s	k7c7	s
kyselinou	kyselina	k1gFnSc7	kyselina
chlorsulfonovou	chlorsulfonový	k2eAgFnSc7d1	chlorsulfonový
a	a	k8xC	a
následnou	následný	k2eAgFnSc7d1	následná
neutralizací	neutralizace	k1gFnSc7	neutralizace
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
kyseliny	kyselina	k1gFnSc2	kyselina
hydroxidem	hydroxid	k1gInSc7	hydroxid
sodným	sodný	k2eAgInSc7d1	sodný
nebo	nebo	k8xC	nebo
vápenatým	vápenatý	k2eAgInSc7d1	vápenatý
<g/>
.	.	kIx.	.
</s>
<s>
Směs	směs	k1gFnSc1	směs
deseti	deset	k4xCc2	deset
dílů	díl	k1gInPc2	díl
cyklamátu	cyklamát	k1gInSc2	cyklamát
a	a	k8xC	a
jednoho	jeden	k4xCgInSc2	jeden
dílu	díl	k1gInSc2	díl
sacharinu	sacharin	k1gInSc2	sacharin
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
vzájemné	vzájemný	k2eAgNnSc4d1	vzájemné
vyrušení	vyrušení	k1gNnSc4	vyrušení
chuťových	chuťový	k2eAgInPc2d1	chuťový
dojezdů	dojezd	k1gInPc2	dojezd
<g/>
.	.	kIx.	.
</s>
<s>
Cyklamát	Cyklamát	k1gMnSc1	Cyklamát
je	být	k5eAaImIp3nS	být
levnější	levný	k2eAgMnSc1d2	levnější
než	než	k8xS	než
většina	většina	k1gFnSc1	většina
sladidel	sladidlo	k1gNnPc2	sladidlo
<g/>
.	.	kIx.	.
</s>
<s>
Zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
nezávadnost	nezávadnost	k1gFnSc1	nezávadnost
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
sporů	spor	k1gInPc2	spor
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
cyklamát	cyklamát	k1gInSc1	cyklamát
může	moct	k5eAaImIp3nS	moct
u	u	k7c2	u
pokusných	pokusný	k2eAgFnPc2d1	pokusná
krys	krysa	k1gFnPc2	krysa
způsobit	způsobit	k5eAaPmF	způsobit
rakovinu	rakovina	k1gFnSc4	rakovina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
vydala	vydat	k5eAaPmAgFnS	vydat
americká	americký	k2eAgFnSc1d1	americká
agentura	agentura	k1gFnSc1	agentura
Food	Food	k1gInSc1	Food
&	&	k?	&
Drug	Drug	k1gInSc1	Drug
Administration	Administration	k1gInSc1	Administration
(	(	kIx(	(
<g/>
FDA	FDA	kA	FDA
<g/>
)	)	kIx)	)
na	na	k7c4	na
cyklamát	cyklamát	k1gInSc4	cyklamát
sodný	sodný	k2eAgInSc4d1	sodný
zákaz	zákaz	k1gInSc4	zákaz
používání	používání	k1gNnSc2	používání
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
"	"	kIx"	"
<g/>
rakovinotvorné	rakovinotvorný	k2eAgInPc4d1	rakovinotvorný
tumory	tumor	k1gInPc4	tumor
a	a	k8xC	a
vrozené	vrozený	k2eAgFnPc4d1	vrozená
vady	vada	k1gFnPc4	vada
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
obsah	obsah	k1gInSc4	obsah
cyklamátu	cyklamát	k1gInSc2	cyklamát
sodného	sodný	k2eAgInSc2d1	sodný
v	v	k7c6	v
nápojích	nápoj	k1gInPc6	nápoj
Coca	Coc	k2eAgFnSc1d1	Coca
cola	cola	k1gFnSc1	cola
ZERO	ZERO	kA	ZERO
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
ty	ten	k3xDgMnPc4	ten
určené	určený	k2eAgMnPc4d1	určený
pro	pro	k7c4	pro
Chile	Chile	k1gNnSc4	Chile
<g/>
,	,	kIx,	,
Venezuelu	Venezuela	k1gFnSc4	Venezuela
a	a	k8xC	a
některé	některý	k3yIgFnPc4	některý
další	další	k2eAgFnPc4d1	další
středoamerické	středoamerický	k2eAgFnPc4d1	středoamerická
země	zem	k1gFnPc4	zem
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
tento	tento	k3xDgInSc4	tento
nápoj	nápoj	k1gInSc4	nápoj
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2009	[number]	k4	2009
ve	v	k7c6	v
Venezuele	Venezuela	k1gFnSc6	Venezuela
zakázán	zakázat	k5eAaPmNgInS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Cyklamát	Cyklamát	k1gInSc1	Cyklamát
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
nápojů	nápoj	k1gInPc2	nápoj
prodávaných	prodávaný	k2eAgInPc2d1	prodávaný
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Coca	Coc	k2eAgFnSc1d1	Coca
cola	cola	k1gFnSc1	cola
ZERO	ZERO	kA	ZERO
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
vědci	vědec	k1gMnPc1	vědec
dlouhodobou	dlouhodobý	k2eAgFnSc7d1	dlouhodobá
studií	studie	k1gFnSc7	studie
potvrdili	potvrdit	k5eAaPmAgMnP	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
cyklamát	cyklamát	k5eAaPmF	cyklamát
nelze	lze	k6eNd1	lze
za	za	k7c4	za
karcinogen	karcinogen	k1gInSc4	karcinogen
jednoznačně	jednoznačně	k6eAd1	jednoznačně
považovat	považovat	k5eAaImF	považovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
let	léto	k1gNnPc2	léto
podávali	podávat	k5eAaImAgMnP	podávat
různým	různý	k2eAgMnPc3d1	různý
druhům	druh	k1gMnPc3	druh
opic	opice	k1gFnPc2	opice
(	(	kIx(	(
<g/>
21	[number]	k4	21
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
)	)	kIx)	)
5	[number]	k4	5
<g/>
x	x	k?	x
týdně	týdně	k6eAd1	týdně
cyklamát	cyklamát	k5eAaPmF	cyklamát
v	v	k7c6	v
dávkách	dávka	k1gFnPc6	dávka
100	[number]	k4	100
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
a	a	k8xC	a
500	[number]	k4	500
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
byly	být	k5eAaImAgFnP	být
diagnostikovány	diagnostikován	k2eAgFnPc4d1	diagnostikována
mírné	mírný	k2eAgFnPc4d1	mírná
formy	forma	k1gFnPc4	forma
nádorů	nádor	k1gInPc2	nádor
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
3	[number]	k4	3
opic	opice	k1gFnPc2	opice
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
faktu	fakt	k1gInSc3	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
podávané	podávaný	k2eAgFnPc1d1	podávaná
dávky	dávka	k1gFnPc1	dávka
překračují	překračovat	k5eAaImIp3nP	překračovat
minimálně	minimálně	k6eAd1	minimálně
10	[number]	k4	10
<g/>
x	x	k?	x
množství	množství	k1gNnSc2	množství
obsažené	obsažený	k2eAgFnPc1d1	obsažená
v	v	k7c6	v
Zero	Zero	k1gNnSc1	Zero
Cole	cola	k1gFnSc6	cola
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
tento	tento	k3xDgInSc4	tento
nápoj	nápoj	k1gInSc4	nápoj
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
bez	bez	k7c2	bez
obav	obava	k1gFnPc2	obava
konzumovat	konzumovat	k5eAaBmF	konzumovat
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
agentura	agentura	k1gFnSc1	agentura
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
rakoviny	rakovina	k1gFnSc2	rakovina
karcinogenitu	karcinogenita	k1gFnSc4	karcinogenita
za	za	k7c4	za
neprokázanou	prokázaný	k2eNgFnSc4d1	neprokázaná
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
cyklamáty	cyklamát	k1gInPc4	cyklamát
jako	jako	k8xC	jako
neklasifikované	klasifikovaný	k2eNgInPc4d1	neklasifikovaný
<g/>
.	.	kIx.	.
emulgatory	emulgator	k1gInPc4	emulgator
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
