<p>
<s>
Helská	Helský	k2eAgFnSc1d1	Helský
kosa	kosa	k1gFnSc1	kosa
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
nepřesně	přesně	k6eNd1	přesně
Helský	Helský	k2eAgInSc4d1	Helský
poloostrov	poloostrov	k1gInSc4	poloostrov
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Mierzeja	Mierzej	k2eAgFnSc1d1	Mierzeja
Helska	Helska	k1gFnSc1	Helska
nebo	nebo	k8xC	nebo
nepřesně	přesně	k6eNd1	přesně
Półwysep	Półwysep	k1gInSc4	Półwysep
Helski	Helsk	k1gFnSc2	Helsk
<g/>
,	,	kIx,	,
kašubsky	kašubsky	k6eAd1	kašubsky
Hélskô	Hélskô	k1gFnSc1	Hélskô
Sztremlëzna	Sztremlëzna	k1gFnSc1	Sztremlëzna
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
písečná	písečný	k2eAgFnSc1d1	písečná
kosa	kosa	k1gFnSc1	kosa
v	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poloha	poloha	k1gFnSc1	poloha
a	a	k8xC	a
rozměry	rozměr	k1gInPc1	rozměr
kosy	kosa	k1gFnSc2	kosa
==	==	k?	==
</s>
</p>
<p>
<s>
Helská	Helský	k2eAgFnSc1d1	Helský
kosa	kosa	k1gFnSc1	kosa
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
od	od	k7c2	od
něhož	jenž	k3xRgMnSc2	jenž
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
Pucký	Pucký	k2eAgInSc1d1	Pucký
záliv	záliv	k1gInSc1	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
součástí	součást	k1gFnSc7	součást
Gdaňského	gdaňský	k2eAgInSc2d1	gdaňský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c6	na
Kašubském	kašubský	k2eAgNnSc6d1	kašubské
pobřeží	pobřeží	k1gNnSc6	pobřeží
u	u	k7c2	u
severopolského	severopolský	k2eAgNnSc2d1	severopolský
města	město	k1gNnSc2	město
Władysławowo	Władysławowo	k6eAd1	Władysławowo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gFnSc1	její
šířka	šířka	k1gFnSc1	šířka
asi	asi	k9	asi
300	[number]	k4	300
m	m	kA	m
<g/>
,	,	kIx,	,
a	a	k8xC	a
táhne	táhnout	k5eAaImIp3nS	táhnout
se	se	k3xPyFc4	se
východním	východní	k2eAgInSc7d1	východní
směrem	směr	k1gInSc7	směr
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
35	[number]	k4	35
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejužším	úzký	k2eAgNnSc6d3	nejužší
místě	místo	k1gNnSc6	místo
u	u	k7c2	u
Chałup	Chałup	k1gInSc1	Chałup
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gFnSc1	její
šířka	šířka	k1gFnSc1	šířka
pouze	pouze	k6eAd1	pouze
150	[number]	k4	150
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc4d3	veliký
šířky	šířka	k1gFnPc4	šířka
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
kosa	kosa	k1gFnSc1	kosa
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
východním	východní	k2eAgInSc6d1	východní
konci	konec	k1gInSc6	konec
u	u	k7c2	u
města	město	k1gNnSc2	město
Hel	Hela	k1gFnPc2	Hela
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
2	[number]	k4	2
až	až	k9	až
3	[number]	k4	3
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Povrch	povrch	k1gInSc1	povrch
kosy	kosa	k1gFnSc2	kosa
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
32	[number]	k4	32
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
je	být	k5eAaImIp3nS	být
vrchol	vrchol	k1gInSc1	vrchol
duny	duna	k1gFnSc2	duna
Lubek	Lubek	k6eAd1	Lubek
poblíž	poblíž	k7c2	poblíž
Kuźnice	Kuźnice	k1gFnSc2	Kuźnice
(	(	kIx(	(
<g/>
23	[number]	k4	23
m	m	kA	m
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dopravní	dopravní	k2eAgFnSc1d1	dopravní
obslužnost	obslužnost	k1gFnSc1	obslužnost
==	==	k?	==
</s>
</p>
<p>
<s>
Spojení	spojení	k1gNnSc1	spojení
Helské	Helský	k2eAgFnSc2d1	Helský
kosy	kosa	k1gFnSc2	kosa
se	se	k3xPyFc4	se
zbytkem	zbytek	k1gInSc7	zbytek
země	zem	k1gFnSc2	zem
obstarává	obstarávat	k5eAaImIp3nS	obstarávat
silnice	silnice	k1gFnSc1	silnice
číslo	číslo	k1gNnSc1	číslo
216	[number]	k4	216
a	a	k8xC	a
železnice	železnice	k1gFnPc1	železnice
<g/>
,	,	kIx,	,
obojí	obojí	k4xRgInPc1	obojí
končící	končící	k2eAgInPc1d1	končící
v	v	k7c4	v
Helu	Hela	k1gFnSc4	Hela
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Helská	Helský	k2eAgFnSc1d1	Helský
kosa	kosa	k1gFnSc1	kosa
je	být	k5eAaImIp3nS	být
nejmladší	mladý	k2eAgFnSc7d3	nejmladší
částí	část	k1gFnSc7	část
polského	polský	k2eAgNnSc2d1	polské
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
200-300	[number]	k4	200-300
lety	léto	k1gNnPc7	léto
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
místě	místo	k1gNnSc6	místo
řetězec	řetězec	k1gInSc1	řetězec
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kosa	kosa	k1gFnSc1	kosa
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
velký	velký	k2eAgInSc4d1	velký
strategický	strategický	k2eAgInSc4d1	strategický
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
poskytovala	poskytovat	k5eAaImAgFnS	poskytovat
dobrou	dobrý	k2eAgFnSc4d1	dobrá
ochranu	ochrana	k1gFnSc4	ochrana
trojměstí	trojměstí	k1gNnSc2	trojměstí
Gdaňsk-Gdyně-Sopoty	Gdaňsk-Gdyně-Sopota	k1gFnSc2	Gdaňsk-Gdyně-Sopota
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
nových	nový	k2eAgFnPc2d1	nová
vojenských	vojenský	k2eAgFnPc2d1	vojenská
technologií	technologie	k1gFnPc2	technologie
však	však	k9	však
tento	tento	k3xDgInSc4	tento
její	její	k3xOp3gInSc4	její
význam	význam	k1gInSc4	význam
vymizel	vymizet	k5eAaPmAgMnS	vymizet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
