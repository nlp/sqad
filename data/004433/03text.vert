<s>
Old	Olda	k1gFnPc2	Olda
Trafford	Traffordo	k1gNnPc2	Traffordo
je	být	k5eAaImIp3nS	být
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
stadion	stadion	k1gInSc1	stadion
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
stretfordské	stretfordský	k2eAgFnSc6d1	stretfordský
čtvrti	čtvrt	k1gFnSc6	čtvrt
Old	Olda	k1gFnPc2	Olda
Trafford	Trafforda	k1gFnPc2	Trafforda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
jeho	on	k3xPp3gNnSc2	on
založení	založení	k1gNnSc2	založení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
je	být	k5eAaImIp3nS	být
stadion	stadion	k1gInSc4	stadion
domovem	domov	k1gInSc7	domov
ligového	ligový	k2eAgInSc2d1	ligový
klubu	klub	k1gInSc2	klub
Manchester	Manchester	k1gInSc1	Manchester
United	United	k1gInSc1	United
FC	FC	kA	FC
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
kapacita	kapacita	k1gFnSc1	kapacita
stadionu	stadion	k1gInSc2	stadion
je	být	k5eAaImIp3nS	být
75	[number]	k4	75
635	[number]	k4	635
diváků	divák	k1gMnPc2	divák
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
čistě	čistě	k6eAd1	čistě
fotbalovým	fotbalový	k2eAgInSc7d1	fotbalový
stadionem	stadion	k1gInSc7	stadion
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
,	,	kIx,	,
třetím	třetí	k4xOgInSc7	třetí
největším	veliký	k2eAgInSc7d3	veliký
stadionem	stadion	k1gInSc7	stadion
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
a	a	k8xC	a
jedenáctým	jedenáctý	k4xOgInSc7	jedenáctý
největším	veliký	k2eAgInSc7d3	veliký
stadionem	stadion	k1gInSc7	stadion
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Stadion	stadion	k1gInSc1	stadion
je	být	k5eAaImIp3nS	být
vzdálen	vzdálen	k2eAgMnSc1d1	vzdálen
800	[number]	k4	800
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
kriketového	kriketový	k2eAgNnSc2d1	kriketové
hřiště	hřiště	k1gNnSc2	hřiště
Old	Olda	k1gFnPc2	Olda
Trafford	Trafforda	k1gFnPc2	Trafforda
a	a	k8xC	a
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
zastávky	zastávka	k1gFnSc2	zastávka
<g/>
.	.	kIx.	.
</s>
<s>
Přezdívku	přezdívka	k1gFnSc4	přezdívka
"	"	kIx"	"
<g/>
The	The	k1gMnSc5	The
Theatre	Theatr	k1gMnSc5	Theatr
of	of	k?	of
Dreams	Dreams	k1gInSc1	Dreams
<g/>
"	"	kIx"	"
obdržel	obdržet	k5eAaPmAgInS	obdržet
od	od	k7c2	od
Bobbyho	Bobby	k1gMnSc4	Bobby
Charltona	Charlton	k1gMnSc2	Charlton
<g/>
.	.	kIx.	.
</s>
<s>
United	United	k1gMnSc1	United
zde	zde	k6eAd1	zde
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
sezón	sezóna	k1gFnPc2	sezóna
1941	[number]	k4	1941
<g/>
–	–	k?	–
<g/>
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
<g/>
.	.	kIx.	.
</s>
<s>
Stadion	stadion	k1gInSc1	stadion
byl	být	k5eAaImAgInS	být
totiž	totiž	k9	totiž
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
těžce	těžce	k6eAd1	těžce
poškozen	poškodit	k5eAaPmNgInS	poškodit
a	a	k8xC	a
tak	tak	k6eAd1	tak
za	za	k7c4	za
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
jeho	jeho	k3xOp3gFnSc2	jeho
opravy	oprava	k1gFnSc2	oprava
museli	muset	k5eAaImAgMnP	muset
vzít	vzít	k5eAaPmF	vzít
United	United	k1gInSc4	United
za	za	k7c4	za
vděk	vděk	k1gInSc4	vděk
Maine	Main	k1gInSc5	Main
Road	Road	k1gInSc1	Road
<g/>
,	,	kIx,	,
stadion	stadion	k1gInSc1	stadion
městského	městský	k2eAgMnSc2d1	městský
rivala	rival	k1gMnSc2	rival
<g/>
.	.	kIx.	.
</s>
<s>
Stadion	stadion	k1gInSc1	stadion
prošel	projít	k5eAaPmAgInS	projít
<g/>
,	,	kIx,	,
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
až	až	k9	až
po	po	k7c4	po
začátek	začátek	k1gInSc4	začátek
nového	nový	k2eAgNnSc2d1	nové
milénia	milénium	k1gNnSc2	milénium
<g/>
,	,	kIx,	,
řadu	řada	k1gFnSc4	řada
menších	malý	k2eAgNnPc2d2	menší
rozšíření	rozšíření	k1gNnPc2	rozšíření
<g/>
,	,	kIx,	,
kterými	který	k3yRgNnPc7	který
prošli	projít	k5eAaPmAgMnP	projít
tribuny	tribuna	k1gFnPc4	tribuna
North	Northa	k1gFnPc2	Northa
<g/>
,	,	kIx,	,
West	Westa	k1gFnPc2	Westa
a	a	k8xC	a
East	Easta	k1gFnPc2	Easta
Stand	Standa	k1gFnPc2	Standa
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
těmto	tento	k3xDgInPc3	tento
rozšířením	rozšíření	k1gNnSc7	rozšíření
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
stadion	stadion	k1gInSc1	stadion
maximální	maximální	k2eAgFnSc2d1	maximální
kapacity	kapacita	k1gFnSc2	kapacita
80	[number]	k4	80
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
je	být	k5eAaImIp3nS	být
plánovaná	plánovaný	k2eAgFnSc1d1	plánovaná
výstavba	výstavba	k1gFnSc1	výstavba
druhého	druhý	k4xOgNnSc2	druhý
podlaží	podlaží	k1gNnSc2	podlaží
na	na	k7c4	na
South	South	k1gInSc4	South
Standu	Standa	k1gFnSc4	Standa
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
dosahovat	dosahovat	k5eAaImF	dosahovat
celková	celkový	k2eAgFnSc1d1	celková
kapacita	kapacita	k1gFnSc1	kapacita
95	[number]	k4	95
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
návštěva	návštěva	k1gFnSc1	návštěva
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
FA	fa	k1gNnSc2	fa
Cupu	cup	k1gInSc2	cup
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zápas	zápas	k1gInSc1	zápas
mezi	mezi	k7c7	mezi
týmy	tým	k1gInPc7	tým
Wolverhampton	Wolverhampton	k1gInSc4	Wolverhampton
Wanderers	Wanderersa	k1gFnPc2	Wanderersa
a	a	k8xC	a
Grimsby	Grimsba	k1gFnSc2	Grimsba
Town	Towna	k1gFnPc2	Towna
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
76	[number]	k4	76
962	[number]	k4	962
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Old	Olda	k1gFnPc2	Olda
Trafford	Trafford	k1gMnSc1	Trafford
hostil	hostit	k5eAaImAgMnS	hostit
řadu	řada	k1gFnSc4	řada
důležitých	důležitý	k2eAgInPc2d1	důležitý
zápasů	zápas	k1gInPc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Jmenovitě	jmenovitě	k6eAd1	jmenovitě
semifinále	semifinále	k1gNnSc1	semifinále
i	i	k8xC	i
finále	finále	k1gNnSc1	finále
FA	fa	kA	fa
Cupu	cup	k1gInSc2	cup
<g/>
,	,	kIx,	,
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
zápasy	zápas	k1gInPc4	zápas
Anglické	anglický	k2eAgFnSc2d1	anglická
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
reprezentace	reprezentace	k1gFnSc2	reprezentace
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
zápasů	zápas	k1gInPc2	zápas
Mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
1966	[number]	k4	1966
a	a	k8xC	a
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
finále	finále	k1gNnSc7	finále
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc1	dva
finále	finále	k1gNnSc6	finále
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
Rugby	rugby	k1gNnSc6	rugby
league	league	k6eAd1	league
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
několik	několik	k4yIc1	několik
fotbalových	fotbalový	k2eAgInPc2d1	fotbalový
zápasů	zápas	k1gInPc2	zápas
při	při	k7c6	při
Letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
