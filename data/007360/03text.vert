<s>
Krakatice	krakatice	k1gFnPc1	krakatice
Teuthida	Teuthid	k1gMnSc2	Teuthid
A.	A.	kA	A.
Naef	Naef	k1gMnSc1	Naef
<g/>
,	,	kIx,	,
1916	[number]	k4	1916
je	být	k5eAaImIp3nS	být
řád	řád	k1gInSc1	řád
hlavonožců	hlavonožec	k1gMnPc2	hlavonožec
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
dorůstat	dorůstat	k5eAaImF	dorůstat
až	až	k9	až
délky	délka	k1gFnSc2	délka
30	[number]	k4	30
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
existence	existence	k1gFnSc1	existence
byla	být	k5eAaImAgFnS	být
prokázána	prokázat	k5eAaPmNgFnS	prokázat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1857	[number]	k4	1857
<g/>
.	.	kIx.	.
krakatice	krakatice	k1gFnSc2	krakatice
obrovská	obrovský	k2eAgFnSc1d1	obrovská
Architeuthis	Architeuthis	k1gFnSc1	Architeuthis
dux	dux	k?	dux
kalmar	kalmar	k1gInSc4	kalmar
Hamiltonův	Hamiltonův	k2eAgInSc4d1	Hamiltonův
Mesonychoteuthis	Mesonychoteuthis	k1gInSc4	Mesonychoteuthis
hamiltoni	hamilton	k1gMnPc1	hamilton
</s>
