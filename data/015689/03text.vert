<s>
Války	válka	k1gFnPc1
růží	růžit	k5eAaImIp3nP
</s>
<s>
Války	válka	k1gFnPc1
Růží	růžit	k5eAaImIp3nP
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1
růže	růže	k1gFnSc1
Yorků	York	k1gInPc2
a	a	k8xC
červená	červený	k2eAgFnSc1d1
růže	růže	k1gFnSc1
Lancasterů	Lancaster	k1gMnPc2
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1455	#num#	k4
–	–	k?
24	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1487	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Anglie	Anglie	k1gFnSc1
<g/>
,	,	kIx,
Wales	Wales	k1gInSc1
<g/>
,	,	kIx,
Calais	Calais	k1gNnSc1
</s>
<s>
casus	casus	k1gInSc4
belli	bell	k1gMnPc1
<g/>
:	:	kIx,
</s>
<s>
spor	spor	k1gInSc1
o	o	k7c4
následnictví	následnictví	k1gNnSc4
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
počáteční	počáteční	k2eAgNnSc4d1
vítězství	vítězství	k1gNnSc4
Yorků	York	k1gInPc2
</s>
<s>
vláda	vláda	k1gFnSc1
nad	nad	k7c7
Anglií	Anglie	k1gFnSc7
(	(	kIx(
<g/>
1461	#num#	k4
-	-	kIx~
1485	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
konečné	konečný	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
Lancasterů	Lancaster	k1gInPc2
</s>
<s>
vyvraždění	vyvraždění	k1gNnSc1
Yorků	York	k1gInPc2
</s>
<s>
nástup	nástup	k1gInSc1
Tudorovců	Tudorovec	k1gMnPc2
na	na	k7c4
anglický	anglický	k2eAgInSc4d1
trůn	trůn	k1gInSc4
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
Lancasterové	Lancasterové	k2eAgMnPc1d1
Tudorovci	Tudorovec	k1gMnPc1
</s>
<s>
podpora	podpora	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Skotské	skotský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Francouzské	francouzský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Yorkové	Yorkové	k2eAgFnSc1d1
</s>
<s>
podpora	podpora	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Burgundsko	Burgundsko	k1gNnSc1
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anglický	anglický	k2eAgInSc1d1
†	†	k?
Markéta	Markéta	k1gFnSc1
z	z	k7c2
Anjou	Anja	k1gMnSc7
</s>
<s>
Eduard	Eduard	k1gMnSc1
z	z	k7c2
Walesu	Wales	k1gInSc2
†	†	k?
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tudor	tudor	k1gInSc1
</s>
<s>
Richard	Richard	k1gMnSc1
Plantagenet	Plantagenet	k1gMnSc1
†	†	k?
</s>
<s>
Eduard	Eduard	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s>
Richard	Richard	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
†	†	k?
</s>
<s>
Války	válka	k1gFnPc1
růží	růže	k1gFnPc2
(	(	kIx(
<g/>
1455	#num#	k4
<g/>
–	–	k?
<g/>
1487	#num#	k4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
série	série	k1gFnSc1
bitev	bitva	k1gFnPc2
občanské	občanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
Anglii	Anglie	k1gFnSc6
mezi	mezi	k7c7
spojenci	spojenec	k1gMnPc7
rodů	rod	k1gInPc2
Lancasterů	Lancaster	k1gInPc2
a	a	k8xC
Yorků	York	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příčiny	příčina	k1gFnPc4
konfliktu	konflikt	k1gInSc2
sahaly	sahat	k5eAaImAgFnP
až	až	k9
k	k	k7c3
roku	rok	k1gInSc3
1399	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
přívrženci	přívrženec	k1gMnPc1
Lancasterů	Lancaster	k1gInPc2
svrhli	svrhnout	k5eAaPmAgMnP
krále	král	k1gMnSc4
Richarda	Richard	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
ustavili	ustavit	k5eAaPmAgMnP
novým	nový	k2eAgMnSc7d1
králem	král	k1gMnSc7
Jindřicha	Jindřich	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
smrti	smrt	k1gFnSc6
jeho	jeho	k3xOp3gMnSc2,k3xPp3gMnSc2
syna	syn	k1gMnSc2
Jindřicha	Jindřich	k1gMnSc2
V.	V.	kA
roku	rok	k1gInSc2
1422	#num#	k4
vládla	vládnout	k5eAaImAgFnS
zemi	zem	k1gFnSc4
regentská	regentský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
místo	místo	k7c2
nezletilého	zletilý	k2eNgMnSc2d1
Jindřicha	Jindřich	k1gMnSc2
VI	VI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozkrádání	rozkrádání	k1gNnSc3
královského	královský	k2eAgInSc2d1
majetku	majetek	k1gInSc2
regentskou	regentský	k2eAgFnSc7d1
radou	rada	k1gFnSc7
a	a	k8xC
neúspěchy	neúspěch	k1gInPc1
ve	v	k7c6
stoleté	stoletý	k2eAgFnSc6d1
válce	válka	k1gFnSc6
vedly	vést	k5eAaImAgFnP
Richarda	Richarda	k1gFnSc1
Plantageneta	Plantageneta	k1gFnSc1
z	z	k7c2
Yorku	York	k1gInSc2
k	k	k7c3
mobilizaci	mobilizace	k1gFnSc3
vojenských	vojenský	k2eAgFnPc2d1
sil	síla	k1gFnPc2
proti	proti	k7c3
královým	králův	k2eAgMnPc3d1
rádcům	rádce	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
anglické	anglický	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
roku	rok	k1gInSc2
1453	#num#	k4
zažilo	zažít	k5eAaPmAgNnS
ve	v	k7c6
stoleté	stoletý	k2eAgFnSc6d1
válce	válka	k1gFnSc6
definitivní	definitivní	k2eAgFnSc4d1
porážku	porážka	k1gFnSc4
<g/>
,	,	kIx,
král	král	k1gMnSc1
se	se	k3xPyFc4
zhroutil	zhroutit	k5eAaPmAgMnS
a	a	k8xC
Richard	Richard	k1gMnSc1
Plantagenet	Plantagenet	k1gMnSc1
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
protektorem	protektor	k1gMnSc7
Anglie	Anglie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
opatření	opatření	k1gNnSc1
vedoucí	vedoucí	k1gFnSc2
k	k	k7c3
hospodářské	hospodářský	k2eAgFnSc3d1
obnově	obnova	k1gFnSc3
a	a	k8xC
posílení	posílení	k1gNnSc3
bezpečnosti	bezpečnost	k1gFnSc2
však	však	k9
král	král	k1gMnSc1
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
nabyl	nabýt	k5eAaPmAgInS
roku	rok	k1gInSc2
1455	#num#	k4
vědomí	vědomí	k1gNnSc4
<g/>
,	,	kIx,
zrušil	zrušit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
York	York	k1gInSc1
musel	muset	k5eAaImAgInS
Londýn	Londýn	k1gInSc4
opustit	opustit	k5eAaPmF
a	a	k8xC
ještě	ještě	k6eAd1
ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
pak	pak	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
první	první	k4xOgFnSc6
bitvě	bitva	k1gFnSc6
mezi	mezi	k7c7
Lancastery	Lancaster	k1gInPc7
a	a	k8xC
Yorky	York	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítězství	vítězství	k1gNnSc1
v	v	k7c6
této	tento	k3xDgFnSc6
bitvě	bitva	k1gFnSc6
a	a	k8xC
další	další	k2eAgInSc4d1
záchvat	záchvat	k1gInSc4
krále	král	k1gMnSc2
znamenaly	znamenat	k5eAaImAgFnP
pro	pro	k7c4
Yorka	Yorek	k1gMnSc4
návrat	návrat	k1gInSc4
do	do	k7c2
funkce	funkce	k1gFnSc2
protektora	protektor	k1gMnSc2
<g/>
,	,	kIx,
setkal	setkat	k5eAaPmAgMnS
se	se	k3xPyFc4
však	však	k9
se	s	k7c7
silným	silný	k2eAgInSc7d1
odporem	odpor	k1gInSc7
královny	královna	k1gFnSc2
Markéty	Markéta	k1gFnSc2
z	z	k7c2
Anjou	Anja	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
několika	několik	k4yIc2
bitvách	bitva	k1gFnPc6
roku	rok	k1gInSc2
1459	#num#	k4
musel	muset	k5eAaImAgMnS
Richard	Richard	k1gMnSc1
Plantagenet	Plantagenet	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gMnPc1
spojenci	spojenec	k1gMnPc1
prchnout	prchnout	k5eAaPmF
ze	z	k7c2
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
následujícího	následující	k2eAgInSc2d1
roku	rok	k1gInSc2
Yorkové	Yorková	k1gFnSc2
vrátili	vrátit	k5eAaPmAgMnP
s	s	k7c7
armádou	armáda	k1gFnSc7
<g/>
,	,	kIx,
nevystupoval	vystupovat	k5eNaImAgMnS
již	již	k6eAd1
jen	jen	k9
proti	proti	k7c3
královým	králův	k2eAgMnPc3d1
rádcům	rádce	k1gMnPc3
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
již	již	k6eAd1
usiloval	usilovat	k5eAaImAgInS
o	o	k7c4
královskou	královský	k2eAgFnSc4d1
korunu	koruna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tu	tu	k6eAd1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
však	však	k9
nepodařilo	podařit	k5eNaPmAgNnS
získat	získat	k5eAaPmF
<g/>
,	,	kIx,
neboť	neboť	k8xC
v	v	k7c6
prosinci	prosinec	k1gInSc6
roku	rok	k1gInSc2
1460	#num#	k4
byl	být	k5eAaImAgMnS
Richard	Richard	k1gMnSc1
Plantagenet	Plantagenet	k1gMnSc1
zabit	zabít	k5eAaPmNgMnS
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Wakefieldu	Wakefield	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Dobová	dobový	k2eAgFnSc1d1
miniatura	miniatura	k1gFnSc1
zobrazující	zobrazující	k2eAgFnSc4d1
bitvu	bitva	k1gFnSc4
u	u	k7c2
Tewkesbury	Tewkesbura	k1gFnSc2
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1
syn	syn	k1gMnSc1
Eduard	Eduard	k1gMnSc1
se	se	k3xPyFc4
však	však	k9
dokázal	dokázat	k5eAaPmAgMnS
prosadit	prosadit	k5eAaPmF
<g/>
,	,	kIx,
zvítězil	zvítězit	k5eAaPmAgMnS
roku	rok	k1gInSc2
1461	#num#	k4
nad	nad	k7c4
Lancastery	Lancaster	k1gInPc4
a	a	k8xC
nechal	nechat	k5eAaPmAgMnS
se	se	k3xPyFc4
jako	jako	k9
Eduard	Eduard	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
korunovat	korunovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupem	postupem	k7c2
času	čas	k1gInSc2
se	se	k3xPyFc4
však	však	k9
začaly	začít	k5eAaPmAgInP
projevovat	projevovat	k5eAaImF
rozpory	rozpor	k1gInPc4
mezi	mezi	k7c7
ním	on	k3xPp3gMnSc7
a	a	k8xC
jeho	jeho	k3xOp3gMnSc7
spojencem	spojenec	k1gMnSc7
Richardem	Richard	k1gMnSc7
Nevillem	Nevill	k1gMnSc7
z	z	k7c2
Warwicku	Warwick	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
nakonec	nakonec	k6eAd1
přerostly	přerůst	k5eAaPmAgFnP
v	v	k7c4
otevřený	otevřený	k2eAgInSc4d1
boj	boj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Warwick	Warwick	k1gMnSc1
se	se	k3xPyFc4
spojil	spojit	k5eAaPmAgMnS
se	s	k7c7
šlechtou	šlechta	k1gFnSc7
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
byla	být	k5eAaImAgFnS
nespokojena	spokojit	k5eNaPmNgFnS
se	s	k7c7
vzestupem	vzestup	k1gInSc7
„	„	k?
<g/>
nové	nový	k2eAgFnSc2d1
šlechty	šlechta	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
Eduard	Eduard	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
podporoval	podporovat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Warwick	Warwicka	k1gFnPc2
se	s	k7c7
svým	svůj	k3xOyFgNnSc7
vojskem	vojsko	k1gNnSc7
roku	rok	k1gInSc2
1469	#num#	k4
zaznamenal	zaznamenat	k5eAaPmAgMnS
porážku	porážka	k1gFnSc4
a	a	k8xC
musel	muset	k5eAaImAgMnS
prchnout	prchnout	k5eAaPmF
do	do	k7c2
Calais	Calais	k1gNnSc2
<g/>
,	,	kIx,
tam	tam	k6eAd1
se	se	k3xPyFc4
však	však	k9
spojil	spojit	k5eAaPmAgMnS
s	s	k7c7
Markétou	Markéta	k1gFnSc7
z	z	k7c2
Anjou	Anjý	k2eAgFnSc4d1
a	a	k8xC
jejich	jejich	k3xOp3gFnPc1
společné	společný	k2eAgFnPc1d1
síly	síla	k1gFnPc1
se	se	k3xPyFc4
roku	rok	k1gInSc2
1470	#num#	k4
vylodily	vylodit	k5eAaPmAgFnP
v	v	k7c6
Anglii	Anglie	k1gFnSc6
a	a	k8xC
donutily	donutit	k5eAaPmAgFnP
zaskočeného	zaskočený	k2eAgMnSc2d1
Eduarda	Eduard	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
prchnout	prchnout	k5eAaPmF
do	do	k7c2
Burgundska	Burgundsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následujícího	následující	k2eAgInSc2d1
roku	rok	k1gInSc2
se	se	k3xPyFc4
Eduard	Eduard	k1gMnSc1
IV	Iva	k1gFnPc2
<g/>
.	.	kIx.
vylodil	vylodit	k5eAaPmAgMnS
v	v	k7c6
Anglii	Anglie	k1gFnSc6
a	a	k8xC
lancasterské	lancasterský	k2eAgNnSc4d1
vojsko	vojsko	k1gNnSc4
porazil	porazit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnozí	mnohý	k2eAgMnPc1d1
Lancasterové	Lancaster	k1gMnPc1
byli	být	k5eAaImAgMnP
popraveni	popravit	k5eAaPmNgMnP
a	a	k8xC
zavražděn	zavraždit	k5eAaPmNgMnS
byl	být	k5eAaImAgMnS
také	také	k9
uvězněný	uvězněný	k2eAgMnSc1d1
Jindřich	Jindřich	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
</s>
<s>
Ještě	ještě	k9
během	během	k7c2
dalších	další	k2eAgNnPc2d1
let	léto	k1gNnPc2
Eduardovy	Eduardův	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
se	se	k3xPyFc4
vyskytly	vyskytnout	k5eAaPmAgInP
spory	spor	k1gInPc1
mezi	mezi	k7c7
Yorky	York	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
vyvrcholily	vyvrcholit	k5eAaPmAgInP
po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
roku	rok	k1gInSc2
1483	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eduard	Eduard	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
si	se	k3xPyFc3
přál	přát	k5eAaImAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	s	k7c7
protektorem	protektor	k1gInSc7
stal	stát	k5eAaPmAgMnS
jeho	on	k3xPp3gMnSc4,k3xOp3gMnSc4
bratr	bratr	k1gMnSc1
Richard	Richard	k1gMnSc1
<g/>
,	,	kIx,
královna	královna	k1gFnSc1
Alžběta	Alžběta	k1gFnSc1
Woodvillová	Woodvillová	k1gFnSc1
však	však	k9
nechala	nechat	k5eAaPmAgFnS
korunovat	korunovat	k5eAaBmF
svého	svůj	k3xOyFgMnSc4
nezletilého	zletilý	k2eNgMnSc4d1
syna	syn	k1gMnSc4
Eduarda	Eduard	k1gMnSc2
V.	V.	kA
Richard	Richard	k1gMnSc1
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgMnS
prosadit	prosadit	k5eAaPmF
silou	síla	k1gFnSc7
<g/>
,	,	kIx,
popravil	popravit	k5eAaPmAgMnS
představitele	představitel	k1gMnPc4
Woodvillů	Woodvill	k1gMnPc2
a	a	k8xC
mladičkého	mladičký	k2eAgMnSc2d1
Eduarda	Eduard	k1gMnSc2
V.	V.	kA
i	i	k8xC
s	s	k7c7
jeho	jeho	k3xOp3gMnSc7
bratrem	bratr	k1gMnSc7
Richardem	Richard	k1gMnSc7
dal	dát	k5eAaPmAgMnS
střežit	střežit	k5eAaImF
v	v	k7c6
londýnském	londýnský	k2eAgNnSc6d1
Toweru	Towero	k1gNnSc6
a	a	k8xC
nechal	nechat	k5eAaPmAgMnS
se	se	k3xPyFc4
jako	jako	k9
Richard	Richard	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
korunovat	korunovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dodnes	dodnes	k6eAd1
není	být	k5eNaImIp3nS
objasněno	objasněn	k2eAgNnSc1d1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
se	se	k3xPyFc4
s	s	k7c7
malými	malý	k1gMnPc7
Princi	princa	k1gFnSc2
z	z	k7c2
Toweru	Tower	k1gInSc2
přesně	přesně	k6eAd1
stalo	stát	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s>
Proti	proti	k7c3
Richardovi	Richard	k1gMnSc3
III	III	kA
<g/>
.	.	kIx.
však	však	k9
stále	stále	k6eAd1
více	hodně	k6eAd2
rostla	růst	k5eAaImAgFnS
opozice	opozice	k1gFnSc2
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gMnSc7
představitelem	představitel	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Jindřich	Jindřich	k1gMnSc1
Tudor	tudor	k1gInSc4
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
přebýval	přebývat	k5eAaImAgMnS
v	v	k7c6
exilu	exil	k1gInSc6
v	v	k7c6
Bretani	Bretaň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
se	se	k3xPyFc4
roku	rok	k1gInSc2
1485	#num#	k4
se	se	k3xPyFc4
svými	svůj	k3xOyFgMnPc7
přívrženci	přívrženec	k1gMnPc7
vylodil	vylodit	k5eAaPmAgMnS
ve	v	k7c4
Walesu	Walesa	k1gMnSc4
a	a	k8xC
po	po	k7c6
vítězství	vítězství	k1gNnSc6
nad	nad	k7c7
Richardem	Richard	k1gMnSc7
III	III	kA
<g/>
.	.	kIx.
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Bosworthu	Bosworth	k1gInSc2
přijal	přijmout	k5eAaPmAgMnS
jako	jako	k8xC,k8xS
Jindřich	Jindřich	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
královskou	královský	k2eAgFnSc4d1
korunu	koruna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několik	několik	k4yIc1
dalších	další	k2eAgFnPc2d1
povstání	povstání	k1gNnSc4
již	již	k6eAd1
pak	pak	k6eAd1
postavení	postavení	k1gNnSc1
nového	nový	k2eAgMnSc2d1
krále	král	k1gMnSc2
neohrozilo	ohrozit	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s>
Příčiny	příčina	k1gFnPc4
války	válka	k1gFnSc2
</s>
<s>
Zjednodušený	zjednodušený	k2eAgInSc1d1
rodokmen	rodokmen	k1gInSc1
potomků	potomek	k1gMnPc2
Eduarda	Eduard	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
červeně	červeně	k6eAd1
vyznačeni	vyznačen	k2eAgMnPc1d1
králové	král	k1gMnPc1
</s>
<s>
Nastolení	nastolení	k1gNnSc1
lancasterské	lancasterský	k2eAgFnSc2d1
dynastie	dynastie	k1gFnSc2
</s>
<s>
Příčinou	příčina	k1gFnSc7
válek	válka	k1gFnPc2
růží	růž	k1gFnPc2
byly	být	k5eAaImAgFnP
události	událost	k1gFnPc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
se	se	k3xPyFc4
odehrály	odehrát	k5eAaPmAgFnP
dávno	dávno	k6eAd1
před	před	k7c7
vypuknutím	vypuknutí	k1gNnSc7
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1377	#num#	k4
zemřel	zemřít	k5eAaPmAgMnS
král	král	k1gMnSc1
Eduard	Eduard	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnSc1
dědic	dědic	k1gMnSc1
Černý	Černý	k1gMnSc1
princ	princ	k1gMnSc1
Eduard	Eduard	k1gMnSc1
však	však	k9
zemřel	zemřít	k5eAaPmAgMnS
rok	rok	k1gInSc4
před	před	k7c7
ním	on	k3xPp3gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nárok	nárok	k1gInSc1
na	na	k7c4
trůn	trůn	k1gInSc4
měl	mít	k5eAaImAgMnS
tedy	tedy	k9
syn	syn	k1gMnSc1
Černého	Černý	k1gMnSc2
prince	princ	k1gMnSc2
<g/>
,	,	kIx,
nezletilý	zletilý	k2eNgMnSc1d1
Richard	Richard	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1377	#num#	k4
<g/>
–	–	k?
<g/>
1399	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc7
regentem	regens	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc1
strýc	strýc	k1gMnSc1
Jan	Jan	k1gMnSc1
z	z	k7c2
Gentu	Gent	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1387	#num#	k4
Richard	Richard	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
dosáhl	dosáhnout	k5eAaPmAgMnS
zletilosti	zletilost	k1gFnPc4
<g/>
;	;	kIx,
počínal	počínat	k5eAaImAgMnS
si	se	k3xPyFc3
však	však	k9
svéhlavě	svéhlavě	k6eAd1
a	a	k8xC
neopatrně	opatrně	k6eNd1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
způsobovalo	způsobovat	k5eAaImAgNnS
jednak	jednak	k8xC
hospodářské	hospodářský	k2eAgFnPc1d1
potíže	potíž	k1gFnPc1
<g/>
,	,	kIx,
hlavně	hlavně	k9
však	však	k9
odpor	odpor	k1gInSc4
šlechty	šlechta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
roku	rok	k1gInSc2
1398	#num#	k4
zemřel	zemřít	k5eAaPmAgMnS
Jan	Jan	k1gMnSc1
Gentský	gentský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
majetek	majetek	k1gInSc4
svého	svůj	k3xOyFgMnSc2
bývalého	bývalý	k2eAgMnSc2d1
regenta	regens	k1gMnSc2
svévolně	svévolně	k6eAd1
zabavil	zabavit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlechta	šlechta	k1gFnSc1
se	se	k3xPyFc4
cítila	cítit	k5eAaImAgFnS
být	být	k5eAaImF
ve	v	k7c6
svých	svůj	k3xOyFgNnPc6
právech	právo	k1gNnPc6
ohrožena	ohrozit	k5eAaPmNgFnS
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
Jindřichem	Jindřich	k1gMnSc7
Bolinbrokem	Bolinbrok	k1gMnSc7
<g/>
,	,	kIx,
dědicem	dědic	k1gMnSc7
lancasterského	lancasterský	k2eAgInSc2d1
vévody	vévoda	k1gMnPc4
Jana	Jan	k1gMnSc4
Gentského	gentský	k2eAgMnSc4d1
<g/>
,	,	kIx,
vzbouřila	vzbouřit	k5eAaPmAgFnS
a	a	k8xC
Richarda	Richarda	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
svrhla	svrhnout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Králem	Král	k1gMnSc7
se	se	k3xPyFc4
pak	pak	k6eAd1
stal	stát	k5eAaPmAgMnS
Jindřich	Jindřich	k1gMnSc1
Bolinbrok	Bolinbrok	k1gInSc1
jakožto	jakožto	k8xS
Jindřich	Jindřich	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1399	#num#	k4
<g/>
–	–	k?
<g/>
1413	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tímto	tento	k3xDgNnSc7
ovšem	ovšem	k9
byl	být	k5eAaImAgMnS
zcela	zcela	k6eAd1
opominut	opominut	k2eAgInSc4d1
nárok	nárok	k1gInSc4
větve	větev	k1gFnSc2
Lionela	Lionel	k1gMnSc2
z	z	k7c2
Antwerp	Antwerp	k1gInSc1
<g/>
,	,	kIx,
druhorozeného	druhorozený	k2eAgMnSc4d1
syna	syn	k1gMnSc4
Eduarda	Eduard	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toho	ten	k3xDgMnSc4
sesazený	sesazený	k2eAgMnSc1d1
Richard	Richard	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
využil	využít	k5eAaPmAgInS
a	a	k8xC
za	za	k7c2
svého	svůj	k3xOyFgNnSc2
nástupce	nástupce	k1gMnSc1
určil	určit	k5eAaPmAgMnS
Lionelova	Lionelův	k2eAgMnSc4d1
vnuka	vnuk	k1gMnSc4
Rogera	Roger	k1gMnSc4
Mortimera	Mortimer	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
však	však	k9
zemřel	zemřít	k5eAaPmAgMnS
bez	bez	k7c2
dětí	dítě	k1gFnPc2
a	a	k8xC
své	svůj	k3xOyFgNnSc4
dědictví	dědictví	k1gNnSc4
i	i	k9
s	s	k7c7
nárokem	nárok	k1gInSc7
na	na	k7c4
trůn	trůn	k1gInSc4
odkázal	odkázat	k5eAaPmAgInS
na	na	k7c4
Richarda	Richard	k1gMnSc4
z	z	k7c2
Cambridge	Cambridge	k1gFnSc2
<g/>
,	,	kIx,
vévodu	vévoda	k1gMnSc4
z	z	k7c2
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
svržení	svržení	k1gNnSc4
krále	král	k1gMnSc2
bylo	být	k5eAaImAgNnS
útokem	útok	k1gInSc7
na	na	k7c4
princip	princip	k1gInSc4
božského	božský	k2eAgInSc2d1
původu	původ	k1gInSc2
královské	královský	k2eAgFnSc2d1
moci	moc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jindřich	Jindřich	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
vládl	vládnout	k5eAaImAgInS
jako	jako	k9
„	„	k?
<g/>
první	první	k4xOgInSc1
mezi	mezi	k7c7
rovnými	rovný	k2eAgFnPc7d1
<g/>
“	“	k?
<g/>
,	,	kIx,
tento	tento	k3xDgInSc1
nový	nový	k2eAgInSc1d1
princip	princip	k1gInSc1
jej	on	k3xPp3gMnSc4
však	však	k9
již	již	k6eAd1
nechránil	chránit	k5eNaImAgMnS
tolik	tolik	k6eAd1
jako	jako	k8xC,k8xS
idea	idea	k1gFnSc1
posvátného	posvátný	k2eAgInSc2d1
původu	původ	k1gInSc2
královské	královský	k2eAgFnSc2d1
moci	moc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jindřich	Jindřich	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
syn	syn	k1gMnSc1
Jindřich	Jindřich	k1gMnSc1
V.	V.	kA
(	(	kIx(
<g/>
1413	#num#	k4
<g/>
–	–	k?
<g/>
1422	#num#	k4
<g/>
)	)	kIx)
byli	být	k5eAaImAgMnP
schopnými	schopný	k2eAgMnPc7d1
vládci	vládce	k1gMnPc7
a	a	k8xC
dokázali	dokázat	k5eAaPmAgMnP
případný	případný	k2eAgInSc4d1
odpor	odpor	k1gInSc4
zpacifikovat	zpacifikovat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důležitým	důležitý	k2eAgNnSc7d1
povstáním	povstání	k1gNnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
pokus	pokus	k1gInSc4
Richarda	Richard	k1gMnSc2
z	z	k7c2
Cambridge	Cambridge	k1gFnSc2
o	o	k7c4
svržení	svržení	k1gNnSc4
krále	král	k1gMnSc2
roku	rok	k1gInSc2
1415	#num#	k4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
se	se	k3xPyFc4
však	však	k9
nepodařil	podařit	k5eNaPmAgMnS
<g/>
;	;	kIx,
Richard	Richard	k1gMnSc1
z	z	k7c2
Cambridge	Cambridge	k1gFnSc2
byl	být	k5eAaImAgMnS
popraven	popraven	k2eAgMnSc1d1
a	a	k8xC
pětiletý	pětiletý	k2eAgMnSc1d1
syn	syn	k1gMnSc1
Richard	Richard	k1gMnSc1
Plantagenet	Plantagenet	k1gMnSc1
z	z	k7c2
Yorku	York	k1gInSc2
zatím	zatím	k6eAd1
nepředstavoval	představovat	k5eNaImAgMnS
žádnou	žádný	k3yNgFnSc4
hrozbu	hrozba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podporu	podpor	k1gInSc2
si	se	k3xPyFc3
Jindřich	Jindřich	k1gMnSc1
V.	V.	kA
zajistil	zajistit	k5eAaPmAgMnS
také	také	k6eAd1
úspěchem	úspěch	k1gInSc7
ve	v	k7c6
stoleté	stoletý	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Svatba	svatba	k1gFnSc1
Jindřicha	Jindřich	k1gMnSc2
VI	VI	kA
<g/>
.	.	kIx.
s	s	k7c7
Markétou	Markéta	k1gFnSc7
z	z	k7c2
Anjou	Anjý	k2eAgFnSc4d1
</s>
<s>
Nestabilita	nestabilita	k1gFnSc1
za	za	k7c2
vlády	vláda	k1gFnSc2
Jindřicha	Jindřich	k1gMnSc2
VI	VI	kA
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1422	#num#	k4
ovšem	ovšem	k9
Jindřich	Jindřich	k1gMnSc1
V.	V.	kA
neočekávaně	očekávaně	k6eNd1
zemřel	zemřít	k5eAaPmAgMnS
a	a	k8xC
krom	krom	k7c2
královské	královský	k2eAgFnSc2d1
koruny	koruna	k1gFnSc2
zanechal	zanechat	k5eAaPmAgMnS
svému	svůj	k3xOyFgMnSc3
devítiměsíčnímu	devítiměsíční	k2eAgMnSc3d1
synu	syn	k1gMnSc3
Jindřichovi	Jindřich	k1gMnSc3
VI	VI	kA
<g/>
.	.	kIx.
také	také	k9
pokračující	pokračující	k2eAgInSc4d1
válečný	válečný	k2eAgInSc4d1
konflikt	konflikt	k1gInSc4
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
regentské	regentský	k2eAgFnSc6d1
radě	rada	k1gFnSc6
se	se	k3xPyFc4
rozpoutaly	rozpoutat	k5eAaPmAgInP
spory	spor	k1gInPc1
o	o	k7c4
moc	moc	k1gFnSc4
zejména	zejména	k9
mezi	mezi	k7c4
Humphreym	Humphreym	k1gInSc4
z	z	k7c2
Glouchesteru	Glouchester	k1gInSc2
a	a	k8xC
Beauforty	Beaufort	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
regent	regent	k1gMnSc1
Jan	Jan	k1gMnSc1
z	z	k7c2
Bedfordu	Bedford	k1gInSc2
až	až	k9
do	do	k7c2
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
roku	rok	k1gInSc2
1435	#num#	k4
jen	jen	k9
stěží	stěží	k6eAd1
dokázal	dokázat	k5eAaPmAgMnS
tlumit	tlumit	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Krom	krom	k7c2
toho	ten	k3xDgNnSc2
se	se	k3xPyFc4
snažila	snažit	k5eAaImAgFnS
získat	získat	k5eAaPmF
vliv	vliv	k1gInSc4
také	také	k9
rodina	rodina	k1gFnSc1
de	de	k?
la	la	k1gNnSc7
Pole	pole	k1gFnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
zbohatla	zbohatnout	k5eAaPmAgFnS
svým	svůj	k3xOyFgInSc7
obchodem	obchod	k1gInSc7
s	s	k7c7
vlnou	vlna	k1gFnSc7
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
povýšena	povýšit	k5eAaPmNgFnS
do	do	k7c2
šlechtického	šlechtický	k2eAgInSc2d1
stavu	stav	k1gInSc2
a	a	k8xC
získala	získat	k5eAaPmAgFnS
vévodství	vévodství	k1gNnSc4
Suffolk	Suffolka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Narůstající	narůstající	k2eAgInPc1d1
spory	spor	k1gInPc1
u	u	k7c2
dvora	dvůr	k1gInSc2
se	se	k3xPyFc4
odrážely	odrážet	k5eAaImAgFnP
v	v	k7c6
celé	celý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bohatnoucí	bohatnoucí	k2eAgFnSc1d1
střední	střední	k2eAgFnSc1d1
třída	třída	k1gFnSc1
(	(	kIx(
<g/>
yeomani	yeoman	k1gMnPc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
snažila	snažit	k5eAaImAgFnS
získat	získat	k5eAaPmF
politickou	politický	k2eAgFnSc4d1
moc	moc	k1gFnSc4
na	na	k7c4
úkor	úkor	k1gInSc4
šlechty	šlechta	k1gFnSc2
<g/>
,	,	kIx,
autorita	autorita	k1gFnSc1
krále	král	k1gMnSc2
i	i	k8xC
církve	církev	k1gFnSc2
upadala	upadat	k5eAaImAgFnS,k5eAaPmAgFnS
<g/>
,	,	kIx,
kriminalita	kriminalita	k1gFnSc1
narůstala	narůstat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
tomu	ten	k3xDgNnSc3
všemu	všecek	k3xTgNnSc3
přistupovaly	přistupovat	k5eAaImAgFnP
neúspěchy	neúspěch	k1gInPc4
ve	v	k7c6
stoleté	stoletý	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
:	:	kIx,
francouzská	francouzský	k2eAgNnPc4d1
vojska	vojsko	k1gNnPc4
ovládala	ovládat	k5eAaImAgFnS
stále	stále	k6eAd1
více	hodně	k6eAd2
území	území	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
když	když	k8xS
roku	rok	k1gInSc3
1444	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
anglo-francouzskému	anglo-francouzský	k2eAgNnSc3d1
jednání	jednání	k1gNnSc3
v	v	k7c6
Toursu	Tours	k1gInSc6
<g/>
,	,	kIx,
požadovali	požadovat	k5eAaImAgMnP
Francouzi	Francouz	k1gMnPc1
uzavření	uzavření	k1gNnSc2
příměří	příměří	k1gNnSc2
<g/>
,	,	kIx,
stažení	stažení	k1gNnSc1
anglických	anglický	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
do	do	k7c2
Calais	Calais	k1gNnSc2
a	a	k8xC
sňatek	sňatek	k1gInSc1
již	již	k6eAd1
zletilého	zletilý	k2eAgMnSc2d1
Jindřicha	Jindřich	k1gMnSc2
VI	VI	kA
<g/>
.	.	kIx.
s	s	k7c7
Markétou	Markéta	k1gFnSc7
z	z	k7c2
Anjou	Anjý	k2eAgFnSc7d1
<g/>
,	,	kIx,
aniž	aniž	k8xS,k8xC
by	by	kYmCp3nS
Jindřich	Jindřich	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
obdržel	obdržet	k5eAaPmAgMnS
nějaké	nějaký	k3yIgNnSc4
věno	věno	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvlivnější	vlivný	k2eAgInPc1d3
postavení	postavení	k1gNnSc4
v	v	k7c6
královské	královský	k2eAgFnSc6d1
radě	rada	k1gFnSc6
zastával	zastávat	k5eAaImAgMnS
Vilém	Vilém	k1gMnSc1
de	de	k?
la	la	k1gNnSc4
Pole	pole	k1gFnSc2
ze	z	k7c2
Suffolku	Suffolek	k1gInSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
svatbu	svatba	k1gFnSc4
prosadil	prosadit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
však	však	k9
anglická	anglický	k2eAgFnSc1d1
posádka	posádka	k1gFnSc1
ve	v	k7c6
Francii	Francie	k1gFnSc6
dozvěděla	dozvědět	k5eAaPmAgFnS
potupné	potupný	k2eAgFnPc4d1
okolnosti	okolnost	k1gFnPc4
sňatku	sňatek	k1gInSc2
<g/>
,	,	kIx,
odmítla	odmítnout	k5eAaPmAgFnS
se	se	k3xPyFc4
stáhnout	stáhnout	k5eAaPmF
<g/>
,	,	kIx,
takže	takže	k8xS
boje	boj	k1gInPc1
ve	v	k7c6
Francii	Francie	k1gFnSc6
pokračovaly	pokračovat	k5eAaImAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzská	francouzský	k2eAgFnSc1d1
vojska	vojsko	k1gNnPc1
však	však	k9
nakonec	nakonec	k6eAd1
anglickou	anglický	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
do	do	k7c2
roku	rok	k1gInSc2
1453	#num#	k4
zatlačila	zatlačit	k5eAaPmAgFnS
do	do	k7c2
Calais	Calais	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
představoval	představovat	k5eAaImAgInS
umírněného	umírněný	k2eAgMnSc4d1
a	a	k8xC
nerozhodného	rozhodný	k2eNgMnSc4d1
panovníka	panovník	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
spíše	spíše	k9
než	než	k8xS
o	o	k7c4
vládu	vláda	k1gFnSc4
zajímal	zajímat	k5eAaImAgMnS
o	o	k7c4
náboženství	náboženství	k1gNnSc4
a	a	k8xC
vzdělání	vzdělání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
se	se	k3xPyFc4
u	u	k7c2
něj	on	k3xPp3gMnSc2
někdy	někdy	k6eAd1
projevovala	projevovat	k5eAaImAgFnS
také	také	k9
tvrdohlavost	tvrdohlavost	k1gFnSc4
a	a	k8xC
pomstychtivost	pomstychtivost	k1gFnSc4
<g/>
,	,	kIx,
hlavní	hlavní	k2eAgFnSc4d1
iniciativu	iniciativa	k1gFnSc4
převzala	převzít	k5eAaPmAgFnS
jeho	jeho	k3xOp3gMnPc3
manželka	manželka	k1gFnSc1
Markéta	Markéta	k1gFnSc1
z	z	k7c2
Anjou	Anja	k1gFnSc7
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc4
na	na	k7c4
něj	on	k3xPp3gMnSc4
měla	mít	k5eAaImAgFnS
značný	značný	k2eAgInSc4d1
vliv	vliv	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Upřednostňovala	upřednostňovat	k5eAaImAgFnS
Somersety	Somerset	k1gInPc4
a	a	k8xC
Suffolky	Suffolka	k1gFnPc4
<g/>
,	,	kIx,
pronásledovala	pronásledovat	k5eAaImAgFnS
Glouchestery	Glouchester	k1gInPc4
a	a	k8xC
snažila	snažit	k5eAaImAgFnS
se	se	k3xPyFc4
také	také	k9
omezit	omezit	k5eAaPmF
vliv	vliv	k1gInSc4
Richarda	Richard	k1gMnSc2
Plantageneta	Plantagenet	k1gMnSc2
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
ten	ten	k3xDgMnSc1
doposud	doposud	k6eAd1
věrně	věrně	k6eAd1
sloužil	sloužit	k5eAaImAgMnS
králi	král	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boje	boj	k1gInSc2
ve	v	k7c6
Francii	Francie	k1gFnSc6
značně	značně	k6eAd1
vyčerpávaly	vyčerpávat	k5eAaImAgFnP
královskou	královský	k2eAgFnSc4d1
pokladnu	pokladna	k1gFnSc4
a	a	k8xC
královi	králův	k2eAgMnPc1d1
rádci	rádce	k1gMnPc1
(	(	kIx(
<g/>
Somerseti	Somerset	k1gMnPc1
a	a	k8xC
Suffolkové	Suffolka	k1gMnPc1
<g/>
)	)	kIx)
doporučovali	doporučovat	k5eAaImAgMnP
královi	králův	k2eAgMnPc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
svého	svůj	k3xOyFgInSc2
majetku	majetek	k1gInSc2
zbavoval	zbavovat	k5eAaImAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
tohoto	tento	k3xDgInSc2
majetku	majetek	k1gInSc2
posléze	posléze	k6eAd1
zmocnili	zmocnit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autorita	autorita	k1gFnSc1
krále	král	k1gMnPc4
dále	daleko	k6eAd2
klesala	klesat	k5eAaImAgFnS
<g/>
,	,	kIx,
narůstaly	narůstat	k5eAaImAgFnP
potyčky	potyčka	k1gFnPc1
mezi	mezi	k7c7
šlechtici	šlechtic	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlechta	Šlechta	k1gMnSc1
se	se	k3xPyFc4
zároveň	zároveň	k6eAd1
snažila	snažit	k5eAaImAgFnS
upřít	upřít	k5eAaPmF
volební	volební	k2eAgNnSc4d1
právo	právo	k1gNnSc4
yeomanům	yeoman	k1gInPc3
a	a	k8xC
nižším	nízký	k2eAgFnPc3d2
vrstvám	vrstva	k1gFnPc3
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neúspěchy	neúspěch	k1gInPc4
ve	v	k7c6
Francii	Francie	k1gFnSc6
a	a	k8xC
hroucení	hroucení	k1gNnSc6
zkorumpovaného	zkorumpovaný	k2eAgInSc2d1
systému	systém	k1gInSc2
však	však	k9
pro	pro	k7c4
Suffolky	Suffolek	k1gInPc4
nakonec	nakonec	k6eAd1
znamenal	znamenat	k5eAaImAgInS
katastrofu	katastrofa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vilém	Vilém	k1gMnSc1
ze	z	k7c2
Suffolku	Suffolek	k1gInSc2
byl	být	k5eAaImAgInS
na	na	k7c4
popud	popud	k1gInSc4
parlamentu	parlament	k1gInSc2
roku	rok	k1gInSc2
1450	#num#	k4
uvězněn	uvězněn	k2eAgInSc1d1
<g/>
,	,	kIx,
poté	poté	k6eAd1
králem	král	k1gMnSc7
vypovězen	vypovědět	k5eAaPmNgMnS
ze	z	k7c2
země	zem	k1gFnSc2
a	a	k8xC
při	při	k7c6
své	svůj	k3xOyFgFnSc6
cestě	cesta	k1gFnSc6
z	z	k7c2
Anglie	Anglie	k1gFnSc2
zlynčován	zlynčovat	k5eAaPmNgMnS
námořníky	námořník	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rebelie	rebelie	k1gFnPc1
a	a	k8xC
nepokoje	nepokoj	k1gInPc1
</s>
<s>
Vyhnání	vyhnání	k1gNnSc1
Viléma	Vilém	k1gMnSc2
ze	z	k7c2
Suffolku	Suffolek	k1gInSc2
však	však	k9
nepředstavovalo	představovat	k5eNaImAgNnS
dostatečné	dostatečný	k2eAgNnSc1d1
řešení	řešení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jack	Jack	k1gMnSc1
Cade	Cad	k1gInSc2
<g/>
,	,	kIx,
irský	irský	k2eAgMnSc1d1
zloděj	zloděj	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
prohlašoval	prohlašovat	k5eAaImAgMnS
za	za	k7c4
příbuzného	příbuzný	k1gMnSc4
Richarda	Richard	k1gMnSc4
Plantageneta	Plantagenet	k1gMnSc4
z	z	k7c2
Yorku	York	k1gInSc2
<g/>
,	,	kIx,
požadoval	požadovat	k5eAaImAgMnS
navrácení	navrácení	k1gNnSc4
rozkradeného	rozkradený	k2eAgInSc2d1
královského	královský	k2eAgInSc2d1
majetku	majetek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Král	Král	k1gMnSc1
jeho	jeho	k3xOp3gMnPc4
stoupence	stoupenec	k1gMnPc4
začal	začít	k5eAaPmAgMnS
pronásledovat	pronásledovat	k5eAaImF
<g/>
,	,	kIx,
mnozí	mnohý	k2eAgMnPc1d1
z	z	k7c2
nich	on	k3xPp3gMnPc2
však	však	k9
byli	být	k5eAaImAgMnP
veteráni	veterán	k1gMnPc1
z	z	k7c2
francouzských	francouzský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
<g/>
,	,	kIx,
takže	takže	k8xS
královské	královský	k2eAgNnSc4d1
vojsko	vojsko	k1gNnSc4
porazili	porazit	k5eAaPmAgMnP
<g/>
,	,	kIx,
a	a	k8xC
dokonce	dokonce	k9
ovládli	ovládnout	k5eAaPmAgMnP
Londýn	Londýn	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Londýňané	Londýňan	k1gMnPc1
sice	sice	k8xC
povstalce	povstalec	k1gMnSc2
z	z	k7c2
města	město	k1gNnSc2
vyhnali	vyhnat	k5eAaPmAgMnP
a	a	k8xC
Cade	Cade	k1gFnSc7
byl	být	k5eAaImAgMnS
zabit	zabít	k5eAaPmNgMnS
<g/>
,	,	kIx,
avšak	avšak	k8xC
nepokoje	nepokoj	k1gInPc1
se	se	k3xPyFc4
po	po	k7c6
zemi	zem	k1gFnSc6
šířily	šířit	k5eAaImAgInP
dále	daleko	k6eAd2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Castillionu	Castillion	k1gInSc2
roku	rok	k1gInSc2
1453	#num#	k4
<g/>
,	,	kIx,
definitivní	definitivní	k2eAgFnSc1d1
porážka	porážka	k1gFnSc1
Anglie	Anglie	k1gFnSc2
ve	v	k7c6
stoleté	stoletý	k2eAgFnSc6d1
válce	válka	k1gFnSc6
</s>
<s>
Samotný	samotný	k2eAgMnSc1d1
Richard	Richard	k1gMnSc1
Plantagenet	Plantagenet	k1gMnSc1
neusiloval	usilovat	k5eNaImAgMnS
o	o	k7c4
trůn	trůn	k1gInSc4
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
o	o	k7c4
nápravu	náprava	k1gFnSc4
poměrů	poměr	k1gInPc2
a	a	k8xC
odstranění	odstranění	k1gNnSc4
zkorumpovaných	zkorumpovaný	k2eAgMnPc2d1
rádců	rádce	k1gMnPc2
<g/>
,	,	kIx,
zejména	zejména	k9
Edmunda	Edmund	k1gMnSc4
Beauforta	Beaufort	k1gMnSc4
ze	z	k7c2
Somersetu	Somerset	k1gInSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
nahradil	nahradit	k5eAaPmAgMnS
Suffolka	Suffolka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Yorkův	Yorkův	k2eAgInSc4d1
návrat	návrat	k1gInSc4
z	z	k7c2
Irska	Irsko	k1gNnSc2
a	a	k8xC
pochod	pochod	k1gInSc1
na	na	k7c4
Londýn	Londýn	k1gInSc4
úspěch	úspěch	k1gInSc4
vyjednávání	vyjednávání	k1gNnPc2
se	s	k7c7
Somersetem	Somerset	k1gInSc7
nezajistil	zajistit	k5eNaPmAgInS
a	a	k8xC
protisomersetský	protisomersetský	k2eAgInSc1d1
parlament	parlament	k1gInSc1
byl	být	k5eAaImAgInS
rozpuštěn	rozpustit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
další	další	k2eAgFnSc6d1
mobilizaci	mobilizace	k1gFnSc6
Yorkových	Yorkův	k2eAgFnPc2d1
sil	síla	k1gFnPc2
proti	proti	k7c3
Somersetovi	Somerset	k1gMnSc6
byl	být	k5eAaImAgInS
York	York	k1gInSc1
prohlášen	prohlásit	k5eAaPmNgInS
za	za	k7c4
vzbouřence	vzbouřenec	k1gMnPc4
a	a	k8xC
Somerset	Somerset	k1gInSc4
za	za	k7c2
oddaného	oddaný	k2eAgMnSc2d1
poddaného	poddaný	k1gMnSc2
krále	král	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téměř	téměř	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
vojenskému	vojenský	k2eAgInSc3d1
střetu	střet	k1gInSc3
<g/>
,	,	kIx,
král	král	k1gMnSc1
však	však	k9
nechal	nechat	k5eAaPmAgMnS
Yorka	Yorko	k1gNnSc2
zrádně	zrádně	k6eAd1
zatknout	zatknout	k5eAaPmF
<g/>
,	,	kIx,
donutil	donutit	k5eAaPmAgMnS
jej	on	k3xPp3gMnSc4
přísahat	přísahat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
již	již	k6eAd1
nikdy	nikdy	k6eAd1
nesvolá	svolat	k5eNaPmIp3nS
armádu	armáda	k1gFnSc4
proti	proti	k7c3
králi	král	k1gMnSc3
<g/>
,	,	kIx,
a	a	k8xC
pak	pak	k6eAd1
jej	on	k3xPp3gMnSc4
propustil	propustit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spor	spor	k1gInSc1
získal	získat	k5eAaPmAgInS
další	další	k2eAgInSc4d1
rozměr	rozměr	k1gInSc4
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
do	do	k7c2
něj	on	k3xPp3gInSc2
přimíchaly	přimíchat	k5eAaPmAgInP
spory	spor	k1gInPc1
konkurenčních	konkurenční	k2eAgInPc2d1
rodů	rod	k1gInPc2
Nevillů	Nevill	k1gInPc2
a	a	k8xC
Percyů	Percy	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Král	Král	k1gMnSc1
podle	podle	k7c2
nich	on	k3xPp3gFnPc2
dědické	dědický	k2eAgInPc4d1
spory	spor	k1gInPc4
vyřešil	vyřešit	k5eAaPmAgMnS
nespravedlivě	spravedlivě	k6eNd1
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
Nevillové	Nevill	k1gMnPc1
rozhodli	rozhodnout	k5eAaPmAgMnP
získat	získat	k5eAaPmF
podporu	podpora	k1gFnSc4
u	u	k7c2
Yorka	Yorek	k1gInSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Percyové	Percyová	k1gFnPc1
u	u	k7c2
Somerseta	Somerseto	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
byli	být	k5eAaImAgMnP
roku	rok	k1gInSc2
1453	#num#	k4
Angličané	Angličan	k1gMnPc1
ve	v	k7c6
Francii	Francie	k1gFnSc6
definitivně	definitivně	k6eAd1
poraženi	poražen	k2eAgMnPc1d1
<g/>
,	,	kIx,
král	král	k1gMnSc1
zešílel	zešílet	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dočasnou	dočasný	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
vykonávala	vykonávat	k5eAaImAgFnS
královská	královský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
však	však	k9
nedisponovala	disponovat	k5eNaBmAgFnS
autoritou	autorita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zemi	zem	k1gFnSc6
propukla	propuknout	k5eAaPmAgFnS
anarchie	anarchie	k1gFnSc1
a	a	k8xC
Percyové	Percyus	k1gMnPc1
přepadli	přepadnout	k5eAaPmAgMnP
Nevilly	Nevill	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Do	do	k7c2
toho	ten	k3xDgNnSc2
královna	královna	k1gFnSc1
Markéta	Markéta	k1gFnSc1
z	z	k7c2
Anjou	Anjá	k1gFnSc4
porodila	porodit	k5eAaPmAgFnS
syna	syn	k1gMnSc4
Eduarda	Eduard	k1gMnSc4
(	(	kIx(
<g/>
byť	byť	k8xS
bylo	být	k5eAaImAgNnS
veřejným	veřejný	k2eAgNnSc7d1
tajemstvím	tajemství	k1gNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gMnSc7
otcem	otec	k1gMnSc7
není	být	k5eNaImIp3nS
král	král	k1gMnSc1
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
Somerset	Somerset	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
jen	jen	k6eAd1
zvětšilo	zvětšit	k5eAaPmAgNnS
obavy	obava	k1gFnPc4
z	z	k7c2
moci	moc	k1gFnSc2
Somersetů	Somerset	k1gInPc2
<g/>
;	;	kIx,
York	York	k1gInSc1
přijel	přijet	k5eAaPmAgInS
na	na	k7c4
pozvání	pozvání	k1gNnSc4
biskupů	biskup	k1gMnPc2
do	do	k7c2
Londýna	Londýn	k1gInSc2
<g/>
,	,	kIx,
prosadil	prosadit	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
královské	královský	k2eAgFnSc6d1
radě	rada	k1gFnSc6
a	a	k8xC
Somerset	Somerset	k1gMnSc1
byl	být	k5eAaImAgMnS
uvězněn	uvěznit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parlament	parlament	k1gInSc1
jmenoval	jmenovat	k5eAaImAgInS,k5eAaBmAgInS
Yorka	Yorek	k1gInSc2
regentem	regens	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgInS
záhy	záhy	k6eAd1
jmenován	jmenovat	k5eAaBmNgInS,k5eAaImNgInS
také	také	k9
protektorem	protektor	k1gInSc7
Anglie	Anglie	k1gFnSc2
a	a	k8xC
hlavním	hlavní	k2eAgMnSc7d1
královským	královský	k2eAgMnSc7d1
rádcem	rádce	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
York	York	k1gInSc1
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgInS
ozdravit	ozdravit	k5eAaPmF
královské	královský	k2eAgFnPc4d1
finance	finance	k1gFnPc4
<g/>
,	,	kIx,
redukoval	redukovat	k5eAaBmAgMnS
náklady	náklad	k1gInPc4
královské	královský	k2eAgFnSc2d1
domácnosti	domácnost	k1gFnSc2
<g/>
,	,	kIx,
vyplatil	vyplatit	k5eAaPmAgInS
zpožděné	zpožděný	k2eAgInPc4d1
platy	plat	k1gInPc4
dvorským	dvorští	k1gMnPc3
služebníkům	služebník	k1gMnPc3
<g/>
,	,	kIx,
usiloval	usilovat	k5eAaImAgInS
o	o	k7c6
urovnání	urovnání	k1gNnSc6
sporů	spor	k1gInPc2
v	v	k7c6
zemi	zem	k1gFnSc6
a	a	k8xC
o	o	k7c4
zneškodnění	zneškodnění	k1gNnSc4
pirátů	pirát	k1gMnPc2
v	v	k7c6
okolí	okolí	k1gNnSc6
anglických	anglický	k2eAgInPc2d1
břehů	břeh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
však	však	k9
král	král	k1gMnSc1
začal	začít	k5eAaPmAgMnS
uzdravovat	uzdravovat	k5eAaImF
a	a	k8xC
nabývat	nabývat	k5eAaImF
vědomí	vědomí	k1gNnSc4
<g/>
,	,	kIx,
propustil	propustit	k5eAaPmAgMnS
Somerseta	Somerset	k1gMnSc4
z	z	k7c2
vězení	vězení	k1gNnSc2
a	a	k8xC
Yorkova	Yorkův	k2eAgNnSc2d1
opatření	opatření	k1gNnSc2
rušil	rušit	k5eAaImAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Počáteční	počáteční	k2eAgFnSc1d1
fáze	fáze	k1gFnSc1
(	(	kIx(
<g/>
1455	#num#	k4
<g/>
–	–	k?
<g/>
1460	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Věž	věž	k1gFnSc1
ve	v	k7c6
St	St	kA
Albans	Albans	k1gInSc4
</s>
<s>
Počáteční	počáteční	k2eAgFnSc1d1
převaha	převaha	k1gFnSc1
a	a	k8xC
rozprchnutí	rozprchnutí	k1gNnSc1
Yorků	York	k1gInPc2
(	(	kIx(
<g/>
1455	#num#	k4
<g/>
–	–	k?
<g/>
1459	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
York	York	k1gInSc1
tedy	tedy	k9
raději	rád	k6eAd2
Londýn	Londýn	k1gInSc1
roku	rok	k1gInSc2
1455	#num#	k4
opustil	opustit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzápětí	vzápětí	k6eAd1
se	se	k3xPyFc4
sešel	sejít	k5eAaPmAgInS
parlament	parlament	k1gInSc1
<g/>
,	,	kIx,
na	na	k7c4
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
se	se	k3xPyFc4
však	však	k9
byli	být	k5eAaImAgMnP
pozváni	pozván	k2eAgMnPc1d1
pouze	pouze	k6eAd1
Yorkovi	Yorkův	k2eAgMnPc1d1
nepřátelé	nepřítel	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Yorkova	Yorkov	k1gInSc2
stížnost	stížnost	k1gFnSc4
králi	král	k1gMnSc3
<g/>
,	,	kIx,
že	že	k8xS
nebyl	být	k5eNaImAgMnS
pozván	pozvat	k5eAaPmNgMnS
<g/>
,	,	kIx,
a	a	k8xC
požadavek	požadavek	k1gInSc1
na	na	k7c6
odstranění	odstranění	k1gNnSc6
Somerseta	Somerseto	k1gNnSc2
nebyly	být	k5eNaImAgInP
vyslyšeny	vyslyšen	k2eAgInPc1d1
<g/>
,	,	kIx,
York	York	k1gInSc1
proto	proto	k8xC
zahájil	zahájit	k5eAaPmAgMnS
tažení	tažení	k1gNnSc4
a	a	k8xC
u	u	k7c2
St	St	kA
Albans	Albans	k1gInSc4
se	se	k3xPyFc4
střetl	střetnout	k5eAaPmAgMnS
s	s	k7c7
královským	královský	k2eAgNnSc7d1
vojskem	vojsko	k1gNnSc7
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
vyjednávání	vyjednávání	k1gNnSc1
se	s	k7c7
Somersetem	Somerset	k1gMnSc7
selhalo	selhat	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
bitva	bitva	k1gFnSc1
válek	válka	k1gFnPc2
růží	růžit	k5eAaImIp3nS
připomínala	připomínat	k5eAaImAgFnS
spíše	spíše	k9
jen	jen	k9
pouliční	pouliční	k2eAgFnSc4d1
bitku	bitka	k1gFnSc4
<g/>
,	,	kIx,
trvala	trvat	k5eAaImAgFnS
jen	jen	k9
asi	asi	k9
půl	půl	k6eAd1
hodiny	hodina	k1gFnPc4
<g/>
,	,	kIx,
jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
však	však	k9
o	o	k7c4
první	první	k4xOgFnSc4
otevřenou	otevřený	k2eAgFnSc4d1
srážku	srážka	k1gFnSc4
Yorků	York	k1gInPc2
s	s	k7c7
Lancastery	Lancaster	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Somerset	Somerset	k1gInSc4
však	však	k9
při	při	k7c6
ní	on	k3xPp3gFnSc6
zahynul	zahynout	k5eAaPmAgMnS
a	a	k8xC
král	král	k1gMnSc1
byl	být	k5eAaImAgMnS
zraněn	zranit	k5eAaPmNgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Richard	Richard	k1gMnSc1
Plantagenet	Plantagenet	k1gMnSc1
z	z	k7c2
Yorku	York	k1gInSc2
opět	opět	k6eAd1
zdůrazňoval	zdůrazňovat	k5eAaImAgInS
<g/>
,	,	kIx,
že	že	k8xS
nebojuje	bojovat	k5eNaImIp3nS
proti	proti	k7c3
králi	král	k1gMnSc3
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
proti	proti	k7c3
jeho	jeho	k3xOp3gMnPc3
rádcům	rádce	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nyní	nyní	k6eAd1
když	když	k8xS
hlavní	hlavní	k2eAgMnSc1d1
rádce	rádce	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
<g/>
,	,	kIx,
doprovodil	doprovodit	k5eAaPmAgMnS
se	s	k7c7
svými	svůj	k3xOyFgInPc7
druhy	druh	k1gInPc7
krále	král	k1gMnSc2
do	do	k7c2
Londýna	Londýn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Král	Král	k1gMnSc1
vyhlásil	vyhlásit	k5eAaPmAgMnS
všeobecnou	všeobecný	k2eAgFnSc4d1
amnestii	amnestie	k1gFnSc4
<g/>
,	,	kIx,
záhy	záhy	k6eAd1
jej	on	k3xPp3gMnSc4
však	však	k9
zasáhl	zasáhnout	k5eAaPmAgMnS
další	další	k2eAgInSc4d1
záchvat	záchvat	k1gInSc4
deprese	deprese	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
York	York	k1gInSc1
byl	být	k5eAaImAgInS
proto	proto	k8xC
znovu	znovu	k6eAd1
jmenován	jmenovat	k5eAaBmNgInS,k5eAaImNgInS
protektorem	protektor	k1gInSc7
Anglie	Anglie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
York	York	k1gInSc1
se	se	k3xPyFc4
znovu	znovu	k6eAd1
pustil	pustit	k5eAaPmAgMnS
do	do	k7c2
restituce	restituce	k1gFnSc2
královského	královský	k2eAgInSc2d1
majetku	majetek	k1gInSc2
<g/>
,	,	kIx,
stále	stále	k6eAd1
však	však	k9
narážel	narážet	k5eAaPmAgInS,k5eAaImAgInS
na	na	k7c4
odpor	odpor	k1gInSc4
protivníků	protivník	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
koncentrovali	koncentrovat	k5eAaBmAgMnP
okolo	okolo	k7c2
královny	královna	k1gFnSc2
Markéty	Markéta	k1gFnSc2
z	z	k7c2
Anjou	Anjá	k1gFnSc4
a	a	k8xC
disponovali	disponovat	k5eAaBmAgMnP
početnou	početný	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
(	(	kIx(
<g/>
označovali	označovat	k5eAaImAgMnP
své	svůj	k3xOyFgFnPc4
uniformy	uniforma	k1gFnPc4
labutí	labutí	k2eAgFnPc4d1
<g/>
,	,	kIx,
symbolem	symbol	k1gInSc7
prince	princ	k1gMnSc2
Eduarda	Eduard	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
zotavení	zotavení	k1gNnSc6
krále	král	k1gMnSc2
musel	muset	k5eAaImAgMnS
York	York	k1gInSc4
znovu	znovu	k6eAd1
královskou	královský	k2eAgFnSc4d1
radu	rada	k1gFnSc4
opustit	opustit	k5eAaPmF
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
však	však	k9
král	král	k1gMnSc1
ztratil	ztratit	k5eAaPmAgMnS
veškerou	veškerý	k3xTgFnSc4
autoritu	autorita	k1gFnSc4
a	a	k8xC
v	v	k7c6
zemi	zem	k1gFnSc6
zavládla	zavládnout	k5eAaPmAgFnS
anarchie	anarchie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
sporu	spor	k1gInSc6
mezi	mezi	k7c4
Yorky	York	k1gInPc4
již	již	k6eAd1
nemohl	moct	k5eNaImAgMnS
nikdo	nikdo	k3yNnSc1
zůstat	zůstat	k5eAaPmF
neutrální	neutrální	k2eAgFnSc1d1
<g/>
,	,	kIx,
celá	celý	k2eAgFnSc1d1
země	země	k1gFnSc1
se	se	k3xPyFc4
proto	proto	k8xC
rozštěpila	rozštěpit	k5eAaPmAgFnS
na	na	k7c4
dva	dva	k4xCgInPc4
nesmiřitelné	smiřitelný	k2eNgInPc4d1
tábory	tábor	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1459	#num#	k4
se	se	k3xPyFc4
královna	královna	k1gFnSc1
cítila	cítit	k5eAaImAgFnS
již	již	k6eAd1
dostatečně	dostatečně	k6eAd1
vojensky	vojensky	k6eAd1
silná	silný	k2eAgFnSc1d1
<g/>
,	,	kIx,
takže	takže	k8xS
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
bitvě	bitva	k1gFnSc3
u	u	k7c2
Blore	Blor	k1gInSc5
Heath	Heatha	k1gFnPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
ovšem	ovšem	k9
znovu	znovu	k6eAd1
skončila	skončit	k5eAaPmAgFnS
vítězstvím	vítězství	k1gNnSc7
Yorků	York	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následující	následující	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
u	u	k7c2
Ludford	Ludforda	k1gFnPc2
Bridge	Bridge	k1gNnSc2
však	však	k9
získali	získat	k5eAaPmAgMnP
Lancasterové	Lancaster	k1gMnPc1
navrch	navrch	k6eAd1
a	a	k8xC
Yorkové	Yorek	k1gMnPc1
se	se	k3xPyFc4
rozprchli	rozprchnout	k5eAaPmAgMnP
<g/>
:	:	kIx,
York	York	k1gInSc1
prchl	prchnout	k5eAaPmAgInS
do	do	k7c2
Irska	Irsko	k1gNnSc2
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
Neville	Neville	k1gFnSc2
ze	z	k7c2
Salisbury	Salisbura	k1gFnSc2
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
Neville	Neville	k1gFnSc2
z	z	k7c2
Warwicku	Warwick	k1gInSc2
a	a	k8xC
Yorkův	Yorkův	k2eAgMnSc1d1
syn	syn	k1gMnSc1
Eduard	Eduard	k1gMnSc1
z	z	k7c2
March	Marcha	k1gFnPc2
do	do	k7c2
Calais	Calais	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Královna	královna	k1gFnSc1
Markéta	Markéta	k1gFnSc1
z	z	k7c2
Anjou	Anjý	k2eAgFnSc4d1
s	s	k7c7
královým	králův	k2eAgInSc7d1
souhlasem	souhlas	k1gInSc7
ovládla	ovládnout	k5eAaPmAgFnS
veškeré	veškerý	k3xTgFnPc4
záležitosti	záležitost	k1gFnPc4
<g/>
,	,	kIx,
obvinila	obvinit	k5eAaPmAgFnS
Yorky	York	k1gInPc4
z	z	k7c2
velezrady	velezrada	k1gFnSc2
a	a	k8xC
prosadila	prosadit	k5eAaPmAgFnS
konfiskace	konfiskace	k1gFnPc4
jejich	jejich	k3xOp3gInSc2
majetku	majetek	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Triumf	triumf	k1gInSc1
a	a	k8xC
pád	pád	k1gInSc1
Richarda	Richard	k1gMnSc2
Plantageneta	Plantagenet	k1gMnSc2
(	(	kIx(
<g/>
1460	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1
Angličané	Angličan	k1gMnPc1
však	však	k9
vůbec	vůbec	k9
nebyli	být	k5eNaImAgMnP
přesvědčeni	přesvědčit	k5eAaPmNgMnP
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
York	York	k1gInSc1
byl	být	k5eAaImAgInS
zrádce	zrádce	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moc	moc	k1gFnSc1
Yorků	York	k1gInPc2
navíc	navíc	k6eAd1
nebyla	být	k5eNaImAgFnS
zlomena	zlomen	k2eAgFnSc1d1
<g/>
:	:	kIx,
Warwick	Warwicko	k1gNnPc2
a	a	k8xC
Salisbury	Salisbura	k1gFnSc2
ovládli	ovládnout	k5eAaPmAgMnP
bohaté	bohatý	k2eAgNnSc4d1
Calais	Calais	k1gNnSc4
a	a	k8xC
přes	přes	k7c4
moře	moře	k1gNnSc4
podnikali	podnikat	k5eAaImAgMnP
nájezdy	nájezd	k1gInPc4
na	na	k7c4
Kent	Kent	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1460	#num#	k4
se	se	k3xPyFc4
Yorkové	Yorek	k1gMnPc1
vylodili	vylodit	k5eAaPmAgMnP
v	v	k7c6
Sandwichi	Sandwich	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
York	York	k1gInSc1
zatím	zatím	k6eAd1
zůstal	zůstat	k5eAaPmAgInS
v	v	k7c6
Irsku	Irsko	k1gNnSc6
a	a	k8xC
vylodil	vylodit	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
Chesteru	Chester	k1gInSc6
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k8xS
vojska	vojsko	k1gNnSc2
jeho	jeho	k3xOp3gMnPc2
přívrženců	přívrženec	k1gMnPc2
zvítězila	zvítězit	k5eAaPmAgFnS
nad	nad	k7c7
Lancastery	Lancaster	k1gInPc7
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Northamptonu	Northampton	k1gInSc2
a	a	k8xC
zajala	zajmout	k5eAaPmAgFnS
krále	král	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Richarda	Richarda	k1gFnSc1
Plantageneta	Plantageneta	k1gFnSc1
v	v	k7c6
Londýně	Londýn	k1gInSc6
přivítali	přivítat	k5eAaPmAgMnP
jako	jako	k9
krále	král	k1gMnSc4
<g/>
,	,	kIx,
parlament	parlament	k1gInSc1
<g/>
,	,	kIx,
církevní	církevní	k2eAgMnSc1d1
představitelé	představitel	k1gMnPc1
i	i	k8xC
jeho	jeho	k3xOp3gMnPc1
přátelé	přítel	k1gMnPc1
jej	on	k3xPp3gNnSc4
však	však	k9
zrazovali	zrazovat	k5eAaImAgMnP
od	od	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
si	se	k3xPyFc3
královský	královský	k2eAgInSc4d1
titul	titul	k1gInSc4
nárokoval	nárokovat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parlament	parlament	k1gInSc1
nakonec	nakonec	k6eAd1
zákonem	zákon	k1gInSc7
Act	Act	k1gFnSc2
of	of	k?
Accord	Accord	k1gMnSc1
ustanovil	ustanovit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
králem	král	k1gMnSc7
zůstane	zůstat	k5eAaPmIp3nS
Jindřich	Jindřich	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
následnictví	následnictví	k1gNnPc1
„	„	k?
<g/>
syna	syn	k1gMnSc2
<g/>
“	“	k?
Eduarda	Eduard	k1gMnSc2
však	však	k8xC
zrušil	zrušit	k5eAaPmAgMnS
ve	v	k7c4
prospěch	prospěch	k1gInSc4
Yorka	Yorek	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Trosky	troska	k1gFnPc1
hradu	hrad	k1gInSc2
Sandal	Sandal	k1gFnPc1
</s>
<s>
Tento	tento	k3xDgInSc1
vývoj	vývoj	k1gInSc1
byl	být	k5eAaImAgInS
ovšem	ovšem	k9
zcela	zcela	k6eAd1
nepřípustný	přípustný	k2eNgMnSc1d1
pro	pro	k7c4
Lancastery	Lancaster	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
opět	opět	k6eAd1
shromáždili	shromáždit	k5eAaPmAgMnP
své	svůj	k3xOyFgNnSc4
vojsko	vojsko	k1gNnSc4
a	a	k8xC
začali	začít	k5eAaPmAgMnP
plenit	plenit	k5eAaImF
Yorkovy	Yorkův	k2eAgInPc4d1
pozemky	pozemek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
York	York	k1gInSc1
mobilizoval	mobilizovat	k5eAaBmAgInS
své	svůj	k3xOyFgFnPc4
síly	síla	k1gFnPc4
a	a	k8xC
přesunul	přesunout	k5eAaPmAgMnS
je	on	k3xPp3gNnPc4
v	v	k7c6
prosinci	prosinec	k1gInSc6
1460	#num#	k4
na	na	k7c4
hrad	hrad	k1gInSc4
Sandal	Sandal	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	on	k3xPp3gNnPc4
Lancasterové	Lancaster	k1gMnPc1
oblehli	oblehnout	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Yorkové	Yorkové	k2eAgInSc1d1
z	z	k7c2
hradu	hrad	k1gInSc2
vyšli	vyjít	k5eAaPmAgMnP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
protivníky	protivník	k1gMnPc7
utkali	utkat	k5eAaPmAgMnP
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Wakefieldu	Wakefield	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Yorkové	Yorek	k1gMnPc1
však	však	k9
utrpěli	utrpět	k5eAaPmAgMnP
drtivou	drtivý	k2eAgFnSc4d1
porážku	porážka	k1gFnSc4
<g/>
:	:	kIx,
Richard	Richard	k1gMnSc1
Plantagenet	Plantagenet	k1gMnSc1
z	z	k7c2
Yorku	York	k1gInSc2
byl	být	k5eAaImAgInS
zabit	zabít	k5eAaPmNgInS
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
Neville	Neville	k1gFnSc2
ze	z	k7c2
Salisbury	Salisbura	k1gFnSc2
a	a	k8xC
Yorkův	Yorkův	k2eAgMnSc1d1
druhý	druhý	k4xOgMnSc1
syn	syn	k1gMnSc1
Edmund	Edmund	k1gMnSc1
byli	být	k5eAaImAgMnP
krátce	krátce	k6eAd1
po	po	k7c6
bitvě	bitva	k1gFnSc6
Lancastery	Lancaster	k1gMnPc4
popraveni	popraven	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nastolení	nastolení	k1gNnSc1
Eduarda	Eduard	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1461	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Na	na	k7c6
základě	základ	k1gInSc6
zákona	zákon	k1gInSc2
Act	Act	k1gMnSc1
of	of	k?
Accord	Accord	k1gMnSc1
měl	mít	k5eAaImAgMnS
ovšem	ovšem	k9
nárok	nárok	k1gInSc4
na	na	k7c4
trůn	trůn	k1gInSc4
Yorkův	Yorkův	k2eAgMnSc1d1
syn	syn	k1gMnSc1
Eduard	Eduard	k1gMnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
roku	rok	k1gInSc2
1461	#num#	k4
zvítězil	zvítězit	k5eAaPmAgMnS
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Mortimer	Mortimra	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Cross	Cross	k1gInSc1
nad	nad	k7c7
Lancastery	Lancaster	k1gInPc7
vedenými	vedený	k2eAgInPc7d1
Jasperem	Jasper	k1gMnSc7
Tudorem	tudor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Markéta	Markéta	k1gFnSc1
z	z	k7c2
Anjou	Anjý	k2eAgFnSc7d1
však	však	k9
mezitím	mezitím	k6eAd1
odcestovala	odcestovat	k5eAaPmAgFnS
do	do	k7c2
Skotska	Skotsko	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
získala	získat	k5eAaPmAgFnS
podporu	podpora	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odtud	odtud	k6eAd1
táhla	táhlo	k1gNnPc1
se	s	k7c7
svým	svůj	k3xOyFgNnSc7
vojskem	vojsko	k1gNnSc7
na	na	k7c4
Londýn	Londýn	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Warwick	Warwick	k1gInSc1
se	se	k3xPyFc4
opevnil	opevnit	k5eAaPmAgInS
v	v	k7c6
St.	st.	kA
Albans	Albansa	k1gFnPc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gNnSc1
postavení	postavení	k1gNnSc1
však	však	k9
bylo	být	k5eAaImAgNnS
vyzrazeno	vyzrazen	k2eAgNnSc1d1
a	a	k8xC
jeho	jeho	k3xOp3gNnSc1
vojsko	vojsko	k1gNnSc1
v	v	k7c6
druhé	druhý	k4xOgFnSc6
bitvě	bitva	k1gFnSc6
u	u	k7c2
St	St	kA
Albans	Albansa	k1gFnPc2
utrpělo	utrpět	k5eAaPmAgNnS
porážku	porážka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Markétě	Markéta	k1gFnSc6
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
podařilo	podařit	k5eAaPmAgNnS
osvobodit	osvobodit	k5eAaPmF
Jindřicha	Jindřich	k1gMnSc4
VI	VI	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Londýňané	Londýňan	k1gMnPc1
vystrašení	vystrašený	k2eAgMnPc1d1
zvěstmi	zvěst	k1gFnPc7
o	o	k7c6
lancasterském	lancasterský	k2eAgNnSc6d1
drancování	drancování	k1gNnSc6
měst	město	k1gNnPc2
odmítli	odmítnout	k5eAaPmAgMnP
Lancasterům	Lancaster	k1gInPc3
otevřít	otevřít	k5eAaPmF
brány	brána	k1gFnPc4
města	město	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Eduard	Eduard	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s>
Místo	místo	k7c2
toho	ten	k3xDgNnSc2
vstoupil	vstoupit	k5eAaPmAgInS
do	do	k7c2
Londýna	Londýn	k1gInSc2
Yorkův	Yorkův	k2eAgMnSc1d1
syn	syn	k1gMnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
byl	být	k5eAaImAgMnS
jako	jako	k9
Eduard	Eduard	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
prohlášen	prohlášen	k2eAgMnSc1d1
králem	král	k1gMnSc7
v	v	k7c6
naději	naděje	k1gFnSc6
<g/>
,	,	kIx,
že	že	k8xS
bude	být	k5eAaImBp3nS
vládnout	vládnout	k5eAaImF
lépe	dobře	k6eAd2
než	než	k8xS
Jindřich	Jindřich	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	Nový	k1gMnSc1
král	král	k1gMnSc1
získal	získat	k5eAaPmAgMnS
lidovou	lidový	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
a	a	k8xC
Lancasterové	Lancasterový	k2eAgNnSc4d1
oslabení	oslabení	k1gNnSc4
návratem	návrat	k1gInSc7
Skotů	Skot	k1gMnPc2
do	do	k7c2
Skotska	Skotsko	k1gNnSc2
se	se	k3xPyFc4
museli	muset	k5eAaImAgMnP
stáhnout	stáhnout	k5eAaPmF
na	na	k7c4
sever	sever	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mohutné	mohutný	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
u	u	k7c2
Towtonu	Towton	k1gInSc2
Yorkové	Yorková	k1gFnSc2
nakonec	nakonec	k6eAd1
zvítězili	zvítězit	k5eAaPmAgMnP
<g/>
,	,	kIx,
Markéta	Markéta	k1gFnSc1
z	z	k7c2
Anjou	Anja	k1gFnSc7
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
synem	syn	k1gMnSc7
Eduardem	Eduard	k1gMnSc7
prchla	prchnout	k5eAaPmAgFnS
do	do	k7c2
Skotska	Skotsko	k1gNnSc2
a	a	k8xC
lancasterské	lancasterský	k2eAgNnSc4d1
vojsko	vojsko	k1gNnSc4
se	se	k3xPyFc4
rozpadlo	rozpadnout	k5eAaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vláda	vláda	k1gFnSc1
Yorků	York	k1gInPc2
(	(	kIx(
<g/>
1461	#num#	k4
<g/>
–	–	k?
<g/>
1485	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Spory	spora	k1gFnPc1
Eduarda	Eduard	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
s	s	k7c7
Warwickem	Warwicko	k1gNnSc7
(	(	kIx(
<g/>
1461	#num#	k4
<g/>
–	–	k?
<g/>
1469	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Po	po	k7c6
nastolení	nastolení	k1gNnSc6
Eduarda	Eduard	k1gMnSc2
králem	král	k1gMnSc7
se	se	k3xPyFc4
začal	začít	k5eAaPmAgMnS
v	v	k7c6
zemi	zem	k1gFnSc6
pomalu	pomalu	k6eAd1
obnovovat	obnovovat	k5eAaImF
pořádek	pořádek	k1gInSc4
<g/>
,	,	kIx,
oživovat	oživovat	k5eAaImF
obchod	obchod	k1gInSc4
a	a	k8xC
nastolovat	nastolovat	k5eAaImF
právní	právní	k2eAgInSc4d1
stav	stav	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Markéta	Markéta	k1gFnSc1
z	z	k7c2
Anjou	Anja	k1gMnSc7
se	se	k3xPyFc4
se	s	k7c7
synem	syn	k1gMnSc7
Eduardem	Eduard	k1gMnSc7
přesunula	přesunout	k5eAaPmAgFnS
do	do	k7c2
Francie	Francie	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
však	však	k9
nebyli	být	k5eNaImAgMnP
příliš	příliš	k6eAd1
vítanými	vítaný	k2eAgMnPc7d1
hosty	host	k1gMnPc7
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
je	být	k5eAaImIp3nS
francouzský	francouzský	k2eAgMnSc1d1
král	král	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
XI	XI	kA
<g/>
.	.	kIx.
vybavil	vybavit	k5eAaPmAgInS
malým	malý	k2eAgNnSc7d1
vojskem	vojsko	k1gNnSc7
<g/>
,	,	kIx,
se	s	k7c7
kterým	který	k3yQgInSc7,k3yRgInSc7,k3yIgInSc7
se	se	k3xPyFc4
roku	rok	k1gInSc2
1463	#num#	k4
vylodili	vylodit	k5eAaPmAgMnP
v	v	k7c6
Northumberlandu	Northumberland	k1gInSc6
<g/>
,	,	kIx,
obsadili	obsadit	k5eAaPmAgMnP
zde	zde	k6eAd1
několik	několik	k4yIc4
hradů	hrad	k1gInPc2
<g/>
,	,	kIx,
pak	pak	k6eAd1
však	však	k9
byli	být	k5eAaImAgMnP
roku	rok	k1gInSc2
1464	#num#	k4
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Hedgeley	Hedgelea	k1gFnSc2
Moor	Moora	k1gFnPc2
a	a	k8xC
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Hexhamu	Hexham	k1gInSc2
poraženi	poražen	k2eAgMnPc1d1
a	a	k8xC
mnozí	mnohý	k2eAgMnPc1d1
lancasterští	lancasterský	k2eAgMnPc1d1
velitelé	velitel	k1gMnPc1
byli	být	k5eAaImAgMnP
zabiti	zabít	k5eAaPmNgMnP
(	(	kIx(
<g/>
Jindřich	Jindřich	k1gMnSc1
Beaufort	Beaufort	k1gInSc4
ze	z	k7c2
Somersetu	Somerset	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šarvátky	šarvátka	k1gFnPc1
však	však	k9
na	na	k7c6
severu	sever	k1gInSc6
Anglie	Anglie	k1gFnSc2
pokračovaly	pokračovat	k5eAaImAgFnP
i	i	k9
v	v	k7c6
následujících	následující	k2eAgNnPc6d1
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Brzy	brzy	k6eAd1
po	po	k7c6
korunovaci	korunovace	k1gFnSc6
Eduarda	Eduard	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
se	se	k3xPyFc4
začaly	začít	k5eAaPmAgInP
projevovat	projevovat	k5eAaImF
rozpory	rozpor	k1gInPc4
mezi	mezi	k7c7
ním	on	k3xPp3gMnSc7
a	a	k8xC
Warwickem	Warwicko	k1gNnSc7
<g/>
,	,	kIx,
od	od	k7c2
jehož	jenž	k3xRgInSc2,k3xOyRp3gInSc2
vlivu	vliv	k1gInSc2
se	se	k3xPyFc4
chtěl	chtít	k5eAaImAgMnS
král	král	k1gMnSc1
osamostatnit	osamostatnit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Warwick	Warwick	k1gInSc1
se	se	k3xPyFc4
orientoval	orientovat	k5eAaBmAgInS
na	na	k7c6
Francii	Francie	k1gFnSc6
a	a	k8xC
vyjednával	vyjednávat	k5eAaImAgMnS
pro	pro	k7c4
Eduarda	Eduard	k1gMnSc4
s	s	k7c7
francouzským	francouzský	k2eAgMnSc7d1
králem	král	k1gMnSc7
svatbu	svatba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anglický	anglický	k2eAgMnSc1d1
král	král	k1gMnSc1
<g/>
,	,	kIx,
orientující	orientující	k2eAgMnSc1d1
se	se	k3xPyFc4
na	na	k7c4
Burgundsko	Burgundsko	k1gNnSc4
a	a	k8xC
Flandry	Flandry	k1gInPc4
<g/>
,	,	kIx,
však	však	k9
Warwicka	Warwicka	k1gFnSc1
veřejně	veřejně	k6eAd1
znemožnil	znemožnit	k5eAaPmAgInS
<g/>
,	,	kIx,
když	když	k8xS
v	v	k7c6
parlamentu	parlament	k1gInSc6
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
již	již	k6eAd1
několik	několik	k4yIc4
měsíců	měsíc	k1gInPc2
ženatý	ženatý	k2eAgMnSc1d1
s	s	k7c7
Alžbětou	Alžběta	k1gFnSc7
Woodville	Woodvill	k1gMnSc2
<g/>
,	,	kIx,
vdovou	vdova	k1gFnSc7
po	po	k7c6
Janu	Jan	k1gMnSc6
Greyovi	Greya	k1gMnSc6
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgInS
zabit	zabít	k5eAaPmNgMnS
v	v	k7c6
první	první	k4xOgFnSc6
bitvě	bitva	k1gFnSc6
u	u	k7c2
St.	st.	kA
Albans	Albansa	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
bojoval	bojovat	k5eAaImAgInS
za	za	k7c4
Lancastery	Lancaster	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Richard	Richard	k1gMnSc1
Neville	Neville	k1gFnSc2
z	z	k7c2
Warwicku	Warwick	k1gInSc2
</s>
<s>
Rovněž	rovněž	k9
prosazování	prosazování	k1gNnSc1
Woodvillů	Woodvill	k1gInPc2
(	(	kIx(
<g/>
„	„	k?
<g/>
nové	nový	k2eAgFnSc2d1
šlechty	šlechta	k1gFnSc2
<g/>
“	“	k?
<g/>
)	)	kIx)
do	do	k7c2
úřadů	úřad	k1gInPc2
na	na	k7c4
úkor	úkor	k1gInSc4
dosud	dosud	k6eAd1
věrných	věrný	k2eAgMnPc2d1
Nevillů	Nevill	k1gMnPc2
nesl	nést	k5eAaImAgInS
Warwick	Warwick	k1gInSc1
velmi	velmi	k6eAd1
nelibě	libě	k6eNd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
pak	pak	k6eAd1
králova	králův	k2eAgFnSc1d1
sestra	sestra	k1gFnSc1
provdala	provdat	k5eAaPmAgFnS
za	za	k7c4
burgundského	burgundský	k2eAgMnSc4d1
vévodu	vévoda	k1gMnSc4
Karla	Karel	k1gMnSc4
Smělého	Smělý	k1gMnSc4
<g/>
,	,	kIx,
začal	začít	k5eAaPmAgInS
se	se	k3xPyFc4
Warwick	Warwick	k1gInSc1
spojovat	spojovat	k5eAaImF
se	s	k7c7
„	„	k?
<g/>
starou	starý	k2eAgFnSc7d1
šlechtou	šlechta	k1gFnSc7
<g/>
“	“	k?
a	a	k8xC
královým	králův	k2eAgMnSc7d1
bratrem	bratr	k1gMnSc7
Jiřím	Jiří	k1gMnSc7
Plantagenetem	Plantagenet	k1gInSc7
z	z	k7c2
Clarence	Clarence	k1gFnSc2
proti	proti	k7c3
králi	král	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1469	#num#	k4
začal	začít	k5eAaPmAgInS
Warwick	Warwick	k1gInSc1
na	na	k7c6
severu	sever	k1gInSc6
Anglie	Anglie	k1gFnSc2
verbovat	verbovat	k5eAaImF
vojsko	vojsko	k1gNnSc4
a	a	k8xC
při	při	k7c6
svém	svůj	k3xOyFgInSc6
tažení	tažený	k2eAgMnPc1d1
zvítězili	zvítězit	k5eAaPmAgMnP
nad	nad	k7c7
královským	královský	k2eAgNnSc7d1
vojskem	vojsko	k1gNnSc7
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Edgecote	Edgecot	k1gInSc5
Moor	Moor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Král	Král	k1gMnSc1
však	však	k9
Warwicka	Warwicko	k1gNnPc4
překvapil	překvapit	k5eAaPmAgInS
<g/>
,	,	kIx,
když	když	k8xS
nechal	nechat	k5eAaPmAgMnS
rozpustit	rozpustit	k5eAaPmF
vojsko	vojsko	k1gNnSc4
a	a	k8xC
svěřil	svěřit	k5eAaPmAgInS
se	se	k3xPyFc4
do	do	k7c2
rukou	ruka	k1gFnPc2
arcibiskupa	arcibiskup	k1gMnSc2
z	z	k7c2
Yorku	York	k1gInSc2
<g/>
,	,	kIx,
tedy	tedy	k9
pod	pod	k7c4
ochranu	ochrana	k1gFnSc4
církve	církev	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Warwick	Warwick	k1gMnSc1
nevěděl	vědět	k5eNaImAgMnS
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
dále	daleko	k6eAd2
pokračovat	pokračovat	k5eAaImF
a	a	k8xC
stáhl	stáhnout	k5eAaPmAgMnS
se	se	k3xPyFc4
na	na	k7c4
sever	sever	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obnovení	obnovení	k1gNnSc1
války	válka	k1gFnSc2
(	(	kIx(
<g/>
1470	#num#	k4
<g/>
–	–	k?
<g/>
1471	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Warwick	Warwick	k1gInSc1
a	a	k8xC
Clarence	Clarence	k1gFnSc1
poté	poté	k6eAd1
prchli	prchnout	k5eAaPmAgMnP
do	do	k7c2
Calais	Calais	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Warwick	Warwick	k1gMnSc1
viděl	vidět	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
nyní	nyní	k6eAd1
musí	muset	k5eAaImIp3nS
hledat	hledat	k5eAaImF
mocnějšího	mocný	k2eAgMnSc4d2
protikrále	protikrál	k1gMnSc4
a	a	k8xC
navázal	navázat	k5eAaPmAgMnS
kontakty	kontakt	k1gInPc4
s	s	k7c7
Markétou	Markéta	k1gFnSc7
z	z	k7c2
Anjou	Anja	k1gFnSc7
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
nakonec	nakonec	k6eAd1
svolila	svolit	k5eAaPmAgFnS
ke	k	k7c3
sňatku	sňatek	k1gInSc2
svého	svůj	k3xOyFgMnSc2
syna	syn	k1gMnSc2
Eduarda	Eduard	k1gMnSc2
s	s	k7c7
Warwickovou	Warwickův	k2eAgFnSc7d1
dcerou	dcera	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zrazený	zrazený	k2eAgInSc4d1
Clarence	Clarenec	k1gInPc4
proto	proto	k8xC
obnovil	obnovit	k5eAaPmAgMnS
své	svůj	k3xOyFgInPc4
styky	styk	k1gInPc4
s	s	k7c7
Eduardem	Eduard	k1gMnSc7
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1470	#num#	k4
se	se	k3xPyFc4
Lancasterové	Lancaster	k1gMnPc1
s	s	k7c7
podporou	podpora	k1gFnSc7
Francie	Francie	k1gFnSc2
vylodili	vylodit	k5eAaPmAgMnP
v	v	k7c6
Anglii	Anglie	k1gFnSc6
a	a	k8xC
zcela	zcela	k6eAd1
zaskočeného	zaskočený	k2eAgMnSc2d1
Eduarda	Eduard	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
donutili	donutit	k5eAaPmAgMnP
uprchnout	uprchnout	k5eAaPmF
do	do	k7c2
Burgundska	Burgundsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Warwick	Warwicka	k1gFnPc2
pak	pak	k6eAd1
nechal	nechat	k5eAaPmAgMnS
Jindřicha	Jindřich	k1gMnSc4
VI	VI	kA
<g/>
.	.	kIx.
znovu	znovu	k6eAd1
korunovat	korunovat	k5eAaBmF
králem	král	k1gMnSc7
a	a	k8xC
sám	sám	k3xTgMnSc1
si	se	k3xPyFc3
přivlastnil	přivlastnit	k5eAaPmAgInS
roli	role	k1gFnSc4
královského	královský	k2eAgMnSc2d1
místodržícího	místodržící	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parlament	parlament	k1gInSc1
prohlásil	prohlásit	k5eAaPmAgMnS
Eduarda	Eduard	k1gMnSc4
IV	IV	kA
<g/>
.	.	kIx.
za	za	k7c4
nemanželského	manželský	k2eNgMnSc4d1
potomka	potomek	k1gMnSc4
a	a	k8xC
za	za	k7c4
právoplatného	právoplatný	k2eAgMnSc4d1
dědice	dědic	k1gMnSc4
trůnu	trůn	k1gInSc2
ustanovil	ustanovit	k5eAaPmAgMnS
Jiřího	Jiří	k1gMnSc2
z	z	k7c2
Clarence	Clarence	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Warwick	Warwick	k6eAd1
se	se	k3xPyFc4
po	po	k7c6
příslibu	příslib	k1gInSc6
podpory	podpora	k1gFnSc2
ze	z	k7c2
strany	strana	k1gFnSc2
francouzského	francouzský	k2eAgMnSc2d1
krále	král	k1gMnSc2
chystal	chystat	k5eAaImAgMnS
napadnout	napadnout	k5eAaPmF
Burgundsko	Burgundsko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
přimělo	přimět	k5eAaPmAgNnS
burgundského	burgundský	k2eAgMnSc4d1
panovníka	panovník	k1gMnSc4
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
Eduardovi	Eduard	k1gMnSc3
poskytl	poskytnout	k5eAaPmAgMnS
finanční	finanční	k2eAgInPc4d1
prostředky	prostředek	k1gInPc4
a	a	k8xC
vojsko	vojsko	k1gNnSc4
pro	pro	k7c4
invazi	invaze	k1gFnSc4
do	do	k7c2
Anglie	Anglie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eduard	Eduard	k1gMnSc1
se	se	k3xPyFc4
vylodil	vylodit	k5eAaPmAgMnS
s	s	k7c7
malým	malý	k2eAgNnSc7d1
vojskem	vojsko	k1gNnSc7
na	na	k7c6
yorkshirském	yorkshirský	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
<g/>
,	,	kIx,
krátce	krátce	k6eAd1
nato	nato	k6eAd1
obsadil	obsadit	k5eAaPmAgInS
York	York	k1gInSc1
a	a	k8xC
při	při	k7c6
tažení	tažení	k1gNnSc6
na	na	k7c4
Londýn	Londýn	k1gInSc4
se	se	k3xPyFc4
jeho	jeho	k3xOp3gNnSc1
vojsko	vojsko	k1gNnSc1
postupně	postupně	k6eAd1
zvětšovalo	zvětšovat	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
z	z	k7c2
Clarence	Clarence	k1gFnSc2
definitivně	definitivně	k6eAd1
lancasterskou	lancasterský	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
opustil	opustit	k5eAaPmAgMnS
<g/>
,	,	kIx,
Warwick	Warwick	k1gMnSc1
se	se	k3xPyFc4
vyhýbal	vyhýbat	k5eAaImAgMnS
přímému	přímý	k2eAgInSc3d1
boji	boj	k1gInSc3
<g/>
,	,	kIx,
takže	takže	k8xS
Eduard	Eduard	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
nakonec	nakonec	k6eAd1
vstoupil	vstoupit	k5eAaPmAgMnS
do	do	k7c2
Londýna	Londýn	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
znovu	znovu	k6eAd1
přijal	přijmout	k5eAaPmAgMnS
korunu	koruna	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Barnetu	Barnet	k1gInSc2
(	(	kIx(
<g/>
1471	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Teprve	teprve	k6eAd1
poté	poté	k6eAd1
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
střetu	střet	k1gInSc3
s	s	k7c7
Warwickovým	Warwickův	k2eAgNnSc7d1
vojskem	vojsko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nepřehledné	přehledný	k2eNgFnSc6d1
bitvě	bitva	k1gFnSc6
u	u	k7c2
Barnetu	Barnet	k1gInSc2
se	se	k3xPyFc4
Warwickovo	Warwickův	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
rozpadlo	rozpadnout	k5eAaPmAgNnS
a	a	k8xC
Warwick	Warwick	k1gMnSc1
sám	sám	k3xTgMnSc1
byl	být	k5eAaImAgMnS
zabit	zabít	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezitím	mezitím	k6eAd1
se	se	k3xPyFc4
do	do	k7c2
Anglie	Anglie	k1gFnSc2
vrátila	vrátit	k5eAaPmAgFnS
Markéta	Markéta	k1gFnSc1
z	z	k7c2
Anjou	Anjý	k2eAgFnSc4d1
i	i	k9
se	s	k7c7
sedmnáctiletým	sedmnáctiletý	k2eAgMnSc7d1
synem	syn	k1gMnSc7
Eduardem	Eduard	k1gMnSc7
a	a	k8xC
shromažďovala	shromažďovat	k5eAaImAgFnS
kolem	kolem	k6eAd1
sebe	sebe	k3xPyFc4
lancasterské	lancasterský	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
i	i	k9
přes	přes	k7c4
zprávu	zpráva	k1gFnSc4
o	o	k7c6
Warwickově	Warwickův	k2eAgFnSc6d1
prohře	prohra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nastalé	nastalý	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
u	u	k7c2
Tewkesbury	Tewkesbura	k1gFnSc2
král	král	k1gMnSc1
Eduard	Eduard	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
rychle	rychle	k6eAd1
zvítězil	zvítězit	k5eAaPmAgMnS
<g/>
,	,	kIx,
Markétin	Markétin	k2eAgMnSc1d1
syn	syn	k1gMnSc1
Eduard	Eduard	k1gMnSc1
a	a	k8xC
Edmund	Edmund	k1gMnSc1
Beaufort	Beaufort	k1gInSc4
ze	z	k7c2
Somersetu	Somerset	k1gInSc2
zabiti	zabit	k2eAgMnPc1d1
a	a	k8xC
Markéta	Markéta	k1gFnSc1
samotná	samotný	k2eAgFnSc1d1
zajata	zajat	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uprchnout	uprchnout	k5eAaPmF
ze	z	k7c2
země	zem	k1gFnSc2
se	se	k3xPyFc4
však	však	k9
podařilo	podařit	k5eAaPmAgNnS
Jasperu	Jaspera	k1gFnSc4
Tudorovi	Tudorův	k2eAgMnPc1d1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
byl	být	k5eAaImAgMnS
lancasterský	lancasterský	k2eAgMnSc1d1
vojevůdce	vojevůdce	k1gMnSc1
a	a	k8xC
syn	syn	k1gMnSc1
z	z	k7c2
druhého	druhý	k4xOgNnSc2
manželství	manželství	k1gNnSc2
vdovy	vdova	k1gFnSc2
po	po	k7c6
Jindřichovi	Jindřich	k1gMnSc6
V.	V.	kA
<g/>
,	,	kIx,
Kateřiny	Kateřina	k1gFnPc1
z	z	k7c2
Valois	Valois	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
jeho	jeho	k3xOp3gMnPc1
synovci	synovec	k1gMnPc1
Jindřichovi	Jindřichův	k2eAgMnPc1d1
Tudorovi	Tudorův	k2eAgMnPc5d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
návratu	návrat	k1gInSc6
Eduarda	Eduard	k1gMnSc2
IV	Iva	k1gFnPc2
<g/>
.	.	kIx.
do	do	k7c2
Londýna	Londýn	k1gInSc2
byl	být	k5eAaImAgInS
v	v	k7c6
Toweru	Tower	k1gInSc6
ubit	ubit	k2eAgMnSc1d1
Jindřich	Jindřich	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Spory	spor	k1gInPc1
mezi	mezi	k7c7
Yorky	York	k1gInPc7
(	(	kIx(
<g/>
1471	#num#	k4
<g/>
–	–	k?
<g/>
1485	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Po	po	k7c6
bitvě	bitva	k1gFnSc6
v	v	k7c4
Tewkesbury	Tewkesbur	k1gInPc4
Eduard	Eduard	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
konečně	konečně	k6eAd1
ovládl	ovládnout	k5eAaPmAgInS
celou	celý	k2eAgFnSc4d1
Anglii	Anglie	k1gFnSc4
a	a	k8xC
bezprostřední	bezprostřední	k2eAgNnSc4d1
ohrožení	ohrožení	k1gNnSc4
jeho	jeho	k3xOp3gFnSc2
moci	moc	k1gFnSc2
pominulo	pominout	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přežili	přežít	k5eAaPmAgMnP
však	však	k9
dva	dva	k4xCgMnPc1
potenciální	potenciální	k2eAgMnPc1d1
uchazeči	uchazeč	k1gMnPc1
o	o	k7c4
trůn	trůn	k1gInSc4
<g/>
:	:	kIx,
Eduardův	Eduardův	k2eAgMnSc1d1
přebíhavý	přebíhavý	k2eAgMnSc1d1
bratr	bratr	k1gMnSc1
Jiří	Jiří	k1gMnSc1
z	z	k7c2
Clarence	Clarence	k1gFnSc2
a	a	k8xC
Jindřich	Jindřich	k1gMnSc1
Tudor	tudor	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
utekl	utéct	k5eAaPmAgInS
do	do	k7c2
Bretaně	Bretaň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Clarence	Clarenec	k1gInSc2
se	se	k3xPyFc4
na	na	k7c6
poslední	poslední	k2eAgFnSc6d1
chvíli	chvíle	k1gFnSc6
přiklonil	přiklonit	k5eAaPmAgMnS
k	k	k7c3
Eduardovi	Eduard	k1gMnSc3
IV	IV	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
však	však	k9
přesto	přesto	k8xC
za	za	k7c4
trest	trest	k1gInSc4
zbaven	zbaven	k2eAgInSc4d1
svého	svůj	k3xOyFgInSc2
majetku	majetek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc4
mu	on	k3xPp3gMnSc3
však	však	k9
nebránilo	bránit	k5eNaImAgNnS
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
vedl	vést	k5eAaImAgInS
spor	spor	k1gInSc1
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
dalším	další	k2eAgMnSc7d1
bratrem	bratr	k1gMnSc7
Richardem	Richard	k1gMnSc7
z	z	k7c2
Gloucesteru	Gloucester	k1gInSc2
o	o	k7c4
Warwickovo	Warwickův	k2eAgNnSc4d1
dědictví	dědictví	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
Clarence	Clarenec	k1gInSc2
nakonec	nakonec	k6eAd1
získal	získat	k5eAaPmAgInS
polovinu	polovina	k1gFnSc4
Warwickova	Warwickův	k2eAgInSc2d1
majetku	majetek	k1gInSc2
<g/>
,	,	kIx,
další	další	k2eAgNnSc4d1
svévolné	svévolný	k2eAgNnSc4d1
jednání	jednání	k1gNnSc4
a	a	k8xC
svérázné	svérázný	k2eAgNnSc4d1
vykonávání	vykonávání	k1gNnSc4
práva	právo	k1gNnSc2
však	však	k9
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
jeho	jeho	k3xOp3gNnSc3
zatčení	zatčení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parlament	parlament	k1gInSc1
jej	on	k3xPp3gMnSc4
za	za	k7c4
justiční	justiční	k2eAgFnSc4d1
vraždu	vražda	k1gFnSc4
a	a	k8xC
věrolomnost	věrolomnost	k1gFnSc4
odsoudil	odsoudit	k5eAaPmAgInS
k	k	k7c3
smrti	smrt	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Clarence	Clarenec	k1gInSc2
se	se	k3xPyFc4
však	však	k9
nedočkal	dočkat	k5eNaPmAgMnS
řádné	řádný	k2eAgFnPc4d1
popravy	poprava	k1gFnPc4
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
roku	rok	k1gInSc2
1478	#num#	k4
zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
Toweru	Tower	k1gInSc6
za	za	k7c2
nejasných	jasný	k2eNgFnPc2d1
okolností	okolnost	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tím	ten	k3xDgNnSc7
však	však	k9
napětí	napětí	k1gNnSc1
mezi	mezi	k7c4
Yorky	York	k1gInPc4
nepominulo	pominout	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vynořily	vynořit	k5eAaPmAgInP
se	se	k3xPyFc4
totiž	totiž	k9
další	další	k2eAgInPc1d1
spory	spor	k1gInPc1
<g/>
,	,	kIx,
tentokrát	tentokrát	k6eAd1
mezi	mezi	k7c7
královninou	královnin	k2eAgFnSc7d1
rodinou	rodina	k1gFnSc7
Woodvillů	Woodvill	k1gInPc2
a	a	k8xC
Richardem	Richard	k1gMnSc7
z	z	k7c2
Gloucesteru	Gloucester	k1gInSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
opíral	opírat	k5eAaImAgMnS
svou	svůj	k3xOyFgFnSc4
moc	moc	k1gFnSc4
zejména	zejména	k9
o	o	k7c4
svá	svůj	k3xOyFgNnPc4
velká	velký	k2eAgNnPc4d1
území	území	k1gNnPc4
v	v	k7c6
severní	severní	k2eAgFnSc6d1
Anglii	Anglie	k1gFnSc6
a	a	k8xC
o	o	k7c4
svou	svůj	k3xOyFgFnSc4
popularitu	popularita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napětí	napětí	k1gNnSc1
bylo	být	k5eAaImAgNnS
o	o	k7c4
to	ten	k3xDgNnSc4
větší	veliký	k2eAgNnSc4d2
<g/>
,	,	kIx,
že	že	k8xS
Eduard	Eduard	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
měl	mít	k5eAaImAgMnS
dosud	dosud	k6eAd1
jen	jen	k9
dvě	dva	k4xCgFnPc4
malé	malý	k2eAgFnPc4d1
děti	dítě	k1gFnPc4
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
podle	podle	k7c2
něj	on	k3xPp3gInSc2
měl	mít	k5eAaImAgInS
případně	případně	k6eAd1
protektorem	protektor	k1gMnSc7
stát	stát	k5eAaPmF,k5eAaImF
Richard	Richard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
však	však	k9
Eduard	Eduard	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
roku	rok	k1gInSc2
1483	#num#	k4
zemřel	zemřít	k5eAaPmAgInS
<g/>
,	,	kIx,
požádala	požádat	k5eAaPmAgFnS
královna	královna	k1gFnSc1
Alžběta	Alžběta	k1gFnSc1
Woodville	Woodville	k1gFnSc1
o	o	k7c6
regentství	regentství	k1gNnSc6
a	a	k8xC
o	o	k7c6
korunovaci	korunovace	k1gFnSc6
jejího	její	k3xOp3gMnSc2
syna	syn	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novým	Nový	k1gMnSc7
králem	král	k1gMnSc7
se	se	k3xPyFc4
tedy	tedy	k9
stal	stát	k5eAaPmAgMnS
Eduard	Eduard	k1gMnSc1
V.	V.	kA
<g/>
,	,	kIx,
aniž	aniž	k8xC,k8xS
by	by	kYmCp3nS
však	však	k9
Richard	Richard	k1gMnSc1
byl	být	k5eAaImAgMnS
informován	informovat	k5eAaBmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Richard	Richarda	k1gFnPc2
proto	proto	k8xC
začal	začít	k5eAaPmAgInS
mobilizovat	mobilizovat	k5eAaBmF
vojsko	vojsko	k1gNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
prosadil	prosadit	k5eAaPmAgMnS
jako	jako	k9
protektor	protektor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nechal	nechat	k5eAaPmAgMnS
pak	pak	k6eAd1
popravit	popravit	k5eAaPmF
některé	některý	k3yIgMnPc4
významné	významný	k2eAgMnPc4d1
představitele	představitel	k1gMnPc4
rodu	rod	k1gInSc2
Woodvillů	Woodvill	k1gMnPc2
a	a	k8xC
dvanáctiletého	dvanáctiletý	k2eAgMnSc2d1
Eduarda	Eduard	k1gMnSc2
V.	V.	kA
i	i	k8xC
s	s	k7c7
jeho	jeho	k3xOp3gMnSc7
bratrem	bratr	k1gMnSc7
Richardem	Richard	k1gMnSc7
nechal	nechat	k5eAaPmAgMnS
uvěznit	uvěznit	k5eAaPmF
v	v	k7c6
Toweru	Tower	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Děti	dítě	k1gFnPc1
Eduarda	Eduard	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
byly	být	k5eAaImAgInP
nakonec	nakonec	k6eAd1
prohlášeny	prohlásit	k5eAaPmNgInP
za	za	k7c4
nelegitimní	legitimní	k2eNgFnSc4d1
a	a	k8xC
parlament	parlament	k1gInSc4
i	i	k9
Londýňané	Londýňan	k1gMnPc1
v	v	k7c6
naději	naděje	k1gFnSc6
<g/>
,	,	kIx,
že	že	k8xS
tak	tak	k9
spory	spor	k1gInPc1
skončí	skončit	k5eAaPmIp3nP
<g/>
,	,	kIx,
vyzvali	vyzvat	k5eAaPmAgMnP
Richarda	Richard	k1gMnSc4
z	z	k7c2
Gloucesteru	Gloucester	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
jakožto	jakožto	k8xS
Richard	Richard	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
přijal	přijmout	k5eAaPmAgMnS
korunu	koruna	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Bosworthu	Bosworth	k1gInSc2
(	(	kIx(
<g/>
1485	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Záhy	záhy	k6eAd1
však	však	k9
v	v	k7c6
zemi	zem	k1gFnSc6
vypuklo	vypuknout	k5eAaPmAgNnS
povstání	povstání	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
zprvu	zprvu	k6eAd1
osvobození	osvobození	k1gNnSc1
Eduarda	Eduard	k1gMnSc2
V.	V.	kA
a	a	k8xC
jeho	on	k3xPp3gMnSc2,k3xOp3gMnSc2
bratra	bratr	k1gMnSc2
z	z	k7c2
vězení	vězení	k1gNnSc2
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
však	však	k9
rozšířila	rozšířit	k5eAaPmAgFnS
zpráva	zpráva	k1gFnSc1
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
byli	být	k5eAaImAgMnP
ve	v	k7c6
vězení	vězení	k1gNnSc6
zabiti	zabít	k5eAaPmNgMnP
<g/>
,	,	kIx,
usilovali	usilovat	k5eAaImAgMnP
povstalci	povstalec	k1gMnPc1
o	o	k7c6
zavraždění	zavraždění	k1gNnSc6
Richarda	Richard	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
a	a	k8xC
nastolení	nastolení	k1gNnSc3
Jindřicha	Jindřich	k1gMnSc2
Tudora	tudor	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
měl	mít	k5eAaImAgMnS
oženit	oženit	k5eAaPmF
s	s	k7c7
dcerou	dcera	k1gFnSc7
Eduarda	Eduard	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alžbětou	Alžběta	k1gFnSc7
z	z	k7c2
Yorku	York	k1gInSc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
mělo	mít	k5eAaImAgNnS
dojít	dojít	k5eAaPmF
ke	k	k7c3
spojení	spojení	k1gNnSc3
lancastersko-tudorovské	lancastersko-tudorovský	k2eAgFnSc2d1
dynastie	dynastie	k1gFnSc2
s	s	k7c7
yorskou	yorský	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
přes	přes	k7c4
potlačení	potlačení	k1gNnSc4
povstání	povstání	k1gNnSc2
Richard	Richarda	k1gFnPc2
III	III	kA
<g/>
.	.	kIx.
rychle	rychle	k6eAd1
ztrácel	ztrácet	k5eAaImAgMnS
podporu	podpora	k1gFnSc4
a	a	k8xC
mnozí	mnohý	k2eAgMnPc1d1
odcházeli	odcházet	k5eAaImAgMnP
za	za	k7c7
Jindřichem	Jindřich	k1gMnSc7
Tudorem	tudor	k1gInSc7
do	do	k7c2
Bretaně	Bretaň	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tudor	tudor	k1gInSc1
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
Tudor	tudor	k1gInSc4
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
přívrženci	přívrženec	k1gMnPc7
roku	rok	k1gInSc2
1485	#num#	k4
vylodil	vylodit	k5eAaPmAgMnS
u	u	k7c2
Milford	Milforda	k1gFnPc2
Haven	Havno	k1gNnPc2
v	v	k7c6
jižním	jižní	k2eAgInSc6d1
Walesu	Wales	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Richard	Richard	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
podporu	podpora	k1gFnSc4
jen	jen	k9
těžko	těžko	k6eAd1
sháněl	shánět	k5eAaImAgMnS
vojáky	voják	k1gMnPc4
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
jeho	jeho	k3xOp3gNnSc1
vojsko	vojsko	k1gNnSc1
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Bosworthu	Bosworth	k1gInSc2
zaznamenalo	zaznamenat	k5eAaPmAgNnS
porážku	porážka	k1gFnSc4
a	a	k8xC
on	on	k3xPp3gMnSc1
sám	sám	k3xTgMnSc1
padl	padnout	k5eAaPmAgInS,k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tudor	tudor	k1gInSc1
byl	být	k5eAaImAgInS
korunován	korunován	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
Jindřich	Jindřich	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
a	a	k8xC
oženil	oženit	k5eAaPmAgMnS
se	se	k3xPyFc4
s	s	k7c7
Alžbětou	Alžběta	k1gFnSc7
z	z	k7c2
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parlament	parlament	k1gInSc1
jeho	jeho	k3xOp3gFnSc4
pozici	pozice	k1gFnSc4
potvrdil	potvrdit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následovalo	následovat	k5eAaImAgNnS
ještě	ještě	k6eAd1
několik	několik	k4yIc4
neúspěšných	úspěšný	k2eNgNnPc2d1
povstání	povstání	k1gNnPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
však	však	k9
byla	být	k5eAaImAgFnS
(	(	kIx(
<g/>
například	například	k6eAd1
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Stoke	Stok	k1gInSc2
roku	rok	k1gInSc2
1487	#num#	k4
<g/>
)	)	kIx)
potlačena	potlačit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svou	svůj	k3xOyFgFnSc4
moc	moc	k1gFnSc4
Jindřich	Jindřich	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
upevňoval	upevňovat	k5eAaImAgMnS
také	také	k6eAd1
popravami	poprava	k1gFnPc7
svých	svůj	k3xOyFgMnPc2
odpůrců	odpůrce	k1gMnPc2
(	(	kIx(
<g/>
například	například	k6eAd1
roku	rok	k1gInSc2
1499	#num#	k4
byl	být	k5eAaImAgMnS
popraven	popraven	k2eAgMnSc1d1
Perkin	Perkin	k1gMnSc1
Warbeck	Warbeck	k1gMnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
uváděl	uvádět	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
Richard	Richard	k1gMnSc1
<g/>
,	,	kIx,
mladší	mladý	k2eAgMnSc1d2
ze	z	k7c2
dvou	dva	k4xCgMnPc2
synů	syn	k1gMnPc2
Eduarda	Eduard	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
uvězněných	uvězněný	k2eAgInPc2d1
v	v	k7c6
Toweru	Tower	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tudor	tudor	k1gInSc1
</s>
<s>
Důsledky	důsledek	k1gInPc1
</s>
<s>
Růže	růže	k1gFnSc1
Tudorovců	Tudorovec	k1gMnPc2
</s>
<s>
Nejzřejmějším	zřejmý	k2eAgInSc7d3
důsledkem	důsledek	k1gInSc7
byl	být	k5eAaImAgInS
pád	pád	k1gInSc1
rodu	rod	k1gInSc2
Plantagenetů	Plantagenet	k1gInPc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
na	na	k7c6
anglickém	anglický	k2eAgInSc6d1
trůnu	trůn	k1gInSc6
nahrazen	nahradit	k5eAaPmNgInS
rodem	rod	k1gInSc7
Tudorovců	Tudorovec	k1gMnPc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
v	v	k7c6
následujících	následující	k2eAgNnPc6d1
letech	léto	k1gNnPc6
dramaticky	dramaticky	k6eAd1
změnil	změnit	k5eAaPmAgInS
Anglii	Anglie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlechta	šlechta	k1gFnSc1
utrpěla	utrpět	k5eAaPmAgFnS
během	během	k7c2
těchto	tento	k3xDgFnPc2
válek	válka	k1gFnPc2
<g/>
,	,	kIx,
doprovázených	doprovázený	k2eAgInPc2d1
důsledky	důsledek	k1gInPc7
morové	morový	k2eAgFnSc2d1
nákazy	nákaza	k1gFnSc2
černého	černý	k2eAgInSc2d1
moru	mor	k1gInSc2
<g/>
,	,	kIx,
velké	velký	k2eAgFnPc4d1
ztráty	ztráta	k1gFnPc4
moci	moct	k5eAaImF
ve	v	k7c4
prospěch	prospěch	k1gInSc4
vznikající	vznikající	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
obchodníků	obchodník	k1gMnPc2
a	a	k8xC
centralizované	centralizovaný	k2eAgFnSc2d1
královské	královský	k2eAgFnSc2d1
moci	moc	k1gFnSc2
v	v	k7c6
době	doba	k1gFnSc6
vlády	vláda	k1gFnSc2
Tudorovců	Tudorovec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ohlásila	ohlásit	k5eAaPmAgFnS
konec	konec	k1gInSc4
středověku	středověk	k1gInSc2
v	v	k7c6
Anglii	Anglie	k1gFnSc6
a	a	k8xC
příchod	příchod	k1gInSc4
renesance	renesance	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
druhé	druhý	k4xOgFnSc6
straně	strana	k1gFnSc6
se	se	k3xPyFc4
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
hrůzy	hrůza	k1gFnPc1
válek	válka	k1gFnPc2
byly	být	k5eAaImAgFnP
zveličovány	zveličován	k2eAgFnPc1d1
Jindřichem	Jindřich	k1gMnSc7
VII	VII	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
tak	tak	k6eAd1
zvětšil	zvětšit	k5eAaPmAgInS
své	svůj	k3xOyFgFnPc4
zásluhy	zásluha	k1gFnPc4
o	o	k7c4
uklidnění	uklidnění	k1gNnSc4
situace	situace	k1gFnSc2
a	a	k8xC
dosažení	dosažení	k1gNnSc4
míru	mír	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obchodníci	obchodník	k1gMnPc1
i	i	k8xC
prostý	prostý	k2eAgInSc4d1
lid	lid	k1gInSc4
byli	být	k5eAaImAgMnP
daleko	daleko	k6eAd1
méně	málo	k6eAd2
zasaženi	zasáhnout	k5eAaPmNgMnP
<g/>
,	,	kIx,
než	než	k8xS
některé	některý	k3yIgFnPc4
oblasti	oblast	k1gFnPc4
na	na	k7c6
kontinentu	kontinent	k1gInSc6
strádající	strádající	k2eAgFnSc1d1
z	z	k7c2
dlouhého	dlouhý	k2eAgNnSc2d1
obléhání	obléhání	k1gNnSc2
a	a	k8xC
plenění	plenění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
existovala	existovat	k5eAaImAgNnP
dlouhá	dlouhý	k2eAgNnPc1d1
obléhání	obléhání	k1gNnPc1
<g/>
,	,	kIx,
vyskytovala	vyskytovat	k5eAaImAgFnS
se	se	k3xPyFc4
v	v	k7c6
málo	málo	k6eAd1
osídlených	osídlený	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
vojska	vojsko	k1gNnSc2
potkala	potkat	k5eAaPmAgFnS
v	v	k7c6
hustě	hustě	k6eAd1
osídleném	osídlený	k2eAgNnSc6d1
území	území	k1gNnSc6
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gInSc1
konflikt	konflikt	k1gInSc1
byl	být	k5eAaImAgInS
rychle	rychle	k6eAd1
vyřešen	vyřešit	k5eAaPmNgInS
bitvou	bitva	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Války	válka	k1gFnPc1
byly	být	k5eAaImAgFnP
pro	pro	k7c4
Anglii	Anglie	k1gFnSc4
tragické	tragický	k2eAgFnSc2d1
ztrátou	ztráta	k1gFnSc7
převážné	převážný	k2eAgFnSc2d1
části	část	k1gFnSc2
území	území	k1gNnSc2
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
angličtí	anglický	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
pokračovali	pokračovat	k5eAaImAgMnP
ve	v	k7c6
výbojích	výboj	k1gInPc6
na	na	k7c6
kontinentu	kontinent	k1gInSc6
<g/>
,	,	kIx,
ovládnutí	ovládnutí	k1gNnSc4
území	území	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
dříve	dříve	k6eAd2
patřilo	patřit	k5eAaImAgNnS
pod	pod	k7c4
anglickou	anglický	k2eAgFnSc4d1
správu	správa	k1gFnSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
nepodařilo	podařit	k5eNaPmAgNnS
obnovit	obnovit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
někteří	některý	k3yIgMnPc1
panovníci	panovník	k1gMnPc1
z	z	k7c2
kontinentu	kontinent	k1gInSc2
(	(	kIx(
<g/>
Burgundsko	Burgundsko	k1gNnSc1
a	a	k8xC
Francie	Francie	k1gFnSc1
<g/>
)	)	kIx)
hráli	hrát	k5eAaImAgMnP
klíčovou	klíčový	k2eAgFnSc4d1
roli	role	k1gFnSc4
v	v	k7c6
průběhu	průběh	k1gInSc6
války	válka	k1gFnSc2
v	v	k7c6
Anglii	Anglie	k1gFnSc6
<g/>
,	,	kIx,
poskytnutím	poskytnutí	k1gNnSc7
azylu	azyl	k1gInSc2
a	a	k8xC
financí	finance	k1gFnPc2
poraženým	poražený	k1gMnPc3
šlechticům	šlechtic	k1gMnPc3
<g/>
,	,	kIx,
podporujíce	podporovat	k5eAaImSgMnP
tak	tak	k8xC,k8xS
trvání	trvání	k1gNnSc4
této	tento	k3xDgFnSc2
války	válka	k1gFnSc2
aby	aby	kYmCp3nP
zabránili	zabránit	k5eAaPmAgMnP
existenci	existence	k1gFnSc4
jednotné	jednotný	k2eAgFnSc2d1
a	a	k8xC
silné	silný	k2eAgFnSc2d1
Anglie	Anglie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Poválečné	poválečný	k2eAgNnSc1d1
období	období	k1gNnSc1
přineslo	přinést	k5eAaPmAgNnS
změnu	změna	k1gFnSc4
i	i	k8xC
v	v	k7c6
postavení	postavení	k1gNnSc6
šlechticů	šlechtic	k1gMnPc2
a	a	k8xC
jejich	jejich	k3xOp3gFnPc4
možnosti	možnost	k1gFnPc4
shromáždit	shromáždit	k5eAaPmF
a	a	k8xC
vyzbrojit	vyzbrojit	k5eAaPmF
své	svůj	k3xOyFgNnSc4
vlastní	vlastní	k2eAgNnSc4d1
vojsko	vojsko	k1gNnSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
jim	on	k3xPp3gMnPc3
Jindřich	Jindřich	k1gMnSc1
zakázal	zakázat	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nemohli	moct	k5eNaImAgMnP
tak	tak	k6eAd1
bojovat	bojovat	k5eAaImF
proti	proti	k7c3
sobě	se	k3xPyFc3
ani	ani	k8xC
proti	proti	k7c3
králi	král	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spory	spor	k1gInPc1
šlechticů	šlechtic	k1gMnPc2
tak	tak	k6eAd1
byly	být	k5eAaImAgInP
řešeny	řešit	k5eAaImNgInP
především	především	k6eAd1
u	u	k7c2
královského	královský	k2eAgInSc2d1
dvora	dvůr	k1gInSc2
s	s	k7c7
velkým	velký	k2eAgInSc7d1
vlivem	vliv	k1gInSc7
krále	král	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Klíčové	klíčový	k2eAgFnPc1d1
osobnosti	osobnost	k1gFnPc1
</s>
<s>
Angličtí	anglický	k2eAgMnPc1d1
králové	král	k1gMnPc1
<g/>
:	:	kIx,
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
–	–	k?
Lancaster	Lancaster	k1gInSc1
</s>
<s>
Eduard	Eduard	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
–	–	k?
York	York	k1gInSc1
</s>
<s>
Eduard	Eduard	k1gMnSc1
V.	V.	kA
–	–	k?
York	York	k1gInSc1
</s>
<s>
Richard	Richard	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
–	–	k?
York	York	k1gInSc1
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
–	–	k?
Tudor	tudor	k1gInSc1
</s>
<s>
Yorkové	Yorková	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
Richard	Richard	k1gMnSc1
Plantagenet	Plantagenet	k1gMnSc1
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Yorku	York	k1gInSc2
</s>
<s>
Richard	Richard	k1gMnSc1
Neville	Neville	k1gFnSc2
<g/>
,	,	kIx,
16	#num#	k4
<g/>
.	.	kIx.
hrabě	hrabě	k1gMnSc1
z	z	k7c2
Warwicku	Warwick	k1gInSc2
–	–	k?
bojoval	bojovat	k5eAaImAgMnS
na	na	k7c6
straně	strana	k1gFnSc6
Yorků	York	k1gInPc2
i	i	k8xC
Lancasterů	Lancaster	k1gInPc2
</s>
<s>
Richard	Richard	k1gMnSc1
Neville	Neville	k1gFnSc2
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
hrabě	hrabě	k1gMnSc1
ze	z	k7c2
Salisbury	Salisbura	k1gFnSc2
</s>
<s>
John	John	k1gMnSc1
Neville	Neville	k1gFnSc2
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
markýz	markýza	k1gFnPc2
z	z	k7c2
Montagu	Montag	k1gInSc2
</s>
<s>
Vilém	Vilém	k1gMnSc1
Neville	Neville	k1gFnSc2
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
hrabě	hrabě	k1gMnSc1
z	z	k7c2
Kentu	Kent	k1gInSc2
</s>
<s>
Lancasterové	Lancasterová	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
Markéta	Markéta	k1gFnSc1
z	z	k7c2
Anjou	Anja	k1gMnSc7
</s>
<s>
Markéta	Markéta	k1gFnSc1
Beaufortová	Beaufortová	k1gFnSc1
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
Percy	Perca	k1gFnSc2
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
hrabě	hrabě	k1gMnSc1
z	z	k7c2
Northumberlandu	Northumberland	k1gInSc2
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
Percy	Perca	k1gFnSc2
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
hrabě	hrabě	k1gMnSc1
z	z	k7c2
Northumberlandu	Northumberland	k1gInSc2
</s>
<s>
Richard	Richard	k1gMnSc1
Neville	Neville	k1gFnSc2
<g/>
,	,	kIx,
16	#num#	k4
<g/>
.	.	kIx.
hrabě	hrabě	k1gMnSc1
z	z	k7c2
Warwicku	Warwick	k1gInSc2
–	–	k?
bojoval	bojovat	k5eAaImAgMnS
na	na	k7c6
straně	strana	k1gFnSc6
Yorků	York	k1gInPc2
i	i	k8xC
Lancasterů	Lancaster	k1gInPc2
</s>
<s>
Edmund	Edmund	k1gMnSc1
Beaufort	Beaufort	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vévoda	vévoda	k1gMnSc1
ze	z	k7c2
Somersetu	Somerset	k1gInSc2
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
Beaufort	Beaufort	k1gInSc1
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
vévoda	vévoda	k1gMnSc1
ze	z	k7c2
Somersetu	Somerset	k1gInSc2
</s>
<s>
Edmund	Edmund	k1gMnSc1
Beaufort	Beaufort	k1gInSc1
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
vévoda	vévoda	k1gMnSc1
ze	z	k7c2
Somersetu	Somerset	k1gInSc2
</s>
<s>
Jiří	Jiří	k1gMnSc1
Plantagenet	Plantagenet	k1gMnSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Clarence	Clarence	k1gFnSc2
</s>
<s>
Bitvy	bitva	k1gFnPc1
</s>
<s>
První	první	k4xOgFnSc1
bitva	bitva	k1gFnSc1
u	u	k7c2
St	St	kA
Albans	Albans	k1gInSc1
–	–	k?
22	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
1455	#num#	k4
–	–	k?
vítězství	vítězství	k1gNnSc2
Yorků	York	k1gInPc2
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Blore	Blor	k1gInSc5
Heath	Heath	k1gInSc4
–	–	k?
23	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1459	#num#	k4
–	–	k?
vítězství	vítězství	k1gNnSc2
Yorků	York	k1gInPc2
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Ludford	Ludforda	k1gFnPc2
Bridge	Bridg	k1gFnSc2
–	–	k?
23	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
1459	#num#	k4
–	–	k?
vítězství	vítězství	k1gNnSc2
Lancasterů	Lancaster	k1gInPc2
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Northamptonu	Northampton	k1gInSc2
–	–	k?
10	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
1460	#num#	k4
–	–	k?
vítězství	vítězství	k1gNnSc2
Yorků	York	k1gInPc2
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Wakefieldu	Wakefield	k1gInSc2
–	–	k?
30	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
1460	#num#	k4
–	–	k?
vítězství	vítězství	k1gNnSc2
Lancasterů	Lancaster	k1gMnPc2
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Mortimers	Mortimersa	k1gFnPc2
Cross	Crossa	k1gFnPc2
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
1461	#num#	k4
–	–	k?
vítězství	vítězství	k1gNnSc2
Yorků	York	k1gInPc2
</s>
<s>
Druhá	druhý	k4xOgFnSc1
bitva	bitva	k1gFnSc1
u	u	k7c2
St	St	kA
Albans	Albans	k1gInSc1
–	–	k?
22	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
1461	#num#	k4
–	–	k?
vítězství	vítězství	k1gNnSc2
Lancasterů	Lancaster	k1gMnPc2
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Ferrybridge	Ferrybridg	k1gInSc2
–	–	k?
28	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
1461	#num#	k4
–	–	k?
nerozhodná	rozhodný	k2eNgFnSc1d1
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Towtonu	Towton	k1gInSc2
–	–	k?
29	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
1461	#num#	k4
–	–	k?
vítězství	vítězství	k1gNnSc2
Yorků	York	k1gInPc2
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Hedgeley	Hedgelea	k1gFnSc2
Moor	Moor	k1gInSc1
–	–	k?
29	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
1464	#num#	k4
–	–	k?
vítězství	vítězství	k1gNnSc2
Yorků	York	k1gInPc2
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Hexhamu	Hexham	k1gInSc2
–	–	k?
15	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
1464	#num#	k4
–	–	k?
vítězství	vítězství	k1gNnSc2
Yorků	York	k1gInPc2
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Edgecote	Edgecot	k1gInSc5
Moor	Moor	k1gInSc4
–	–	k?
26	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
1469	#num#	k4
–	–	k?
vítězství	vítězství	k1gNnSc2
Lancasterů	Lancaster	k1gMnPc2
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Losecoat	Losecoat	k1gInSc4
Field	Field	k1gInSc1
–	–	k?
12	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
1470	#num#	k4
–	–	k?
vítězství	vítězství	k1gNnSc2
Yorků	York	k1gInPc2
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Barnetu	Barnet	k1gInSc2
–	–	k?
14	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
1471	#num#	k4
–	–	k?
vítězství	vítězství	k1gNnSc2
Yorků	York	k1gInPc2
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Tewkesbury	Tewkesbura	k1gFnSc2
–	–	k?
4	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
1471	#num#	k4
–	–	k?
vítězství	vítězství	k1gNnSc2
Yorků	York	k1gInPc2
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Bosworthu	Bosworth	k1gInSc2
–	–	k?
22	#num#	k4
<g/>
.	.	kIx.
srpen	srpen	k1gInSc1
1485	#num#	k4
–	–	k?
vítězství	vítězství	k1gNnSc2
Tudorovců	Tudorovec	k1gMnPc2
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Stoke	Stok	k1gInSc2
–	–	k?
16	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
1487	#num#	k4
–	–	k?
vítězství	vítězství	k1gNnSc2
Tudorovců	Tudorovec	k1gMnPc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Wars	Wars	k1gInSc1
of	of	k?
the	the	k?
Roses	Roses	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
NEILLANDS	NEILLANDS	kA
<g/>
,	,	kIx,
Robin	robin	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Války	válka	k1gFnSc2
růží	růž	k1gFnPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Naše	náš	k3xOp1gNnSc1
vojsko	vojsko	k1gNnSc1
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
206	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
457	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
S.	S.	kA
13	#num#	k4
<g/>
–	–	k?
<g/>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jen	jen	k9
Neillands	Neillands	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Neillands	Neillands	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
16	#num#	k4
<g/>
–	–	k?
<g/>
19	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Neillands	Neillands	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
20	#num#	k4
<g/>
–	–	k?
<g/>
26	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Neillands	Neillands	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
19	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Naillands	Naillands	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
21	#num#	k4
<g/>
–	–	k?
<g/>
30	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Neillands	Neillands	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
31	#num#	k4
<g/>
–	–	k?
<g/>
40	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Neillands	Neillands	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
44	#num#	k4
<g/>
n.	n.	k?
<g/>
↑	↑	k?
Neillands	Neillands	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
45	#num#	k4
<g/>
–	–	k?
<g/>
52	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Neillands	Neillands	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
53	#num#	k4
<g/>
–	–	k?
<g/>
58	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Neillands	Neillands	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
60	#num#	k4
<g/>
–	–	k?
<g/>
64	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Neillands	Neillands	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
65	#num#	k4
<g/>
–	–	k?
<g/>
68	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Neillands	Neillands	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
69	#num#	k4
<g/>
–	–	k?
<g/>
75	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Neillands	Neillands	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
76	#num#	k4
<g/>
–	–	k?
<g/>
83	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Neillands	Neillands	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
83	#num#	k4
<g/>
–	–	k?
<g/>
85	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Neillands	Neillands	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
86	#num#	k4
<g/>
–	–	k?
<g/>
88	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Neillands	Neillands	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
89	#num#	k4
<g/>
–	–	k?
<g/>
93	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Neillands	Neillands	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
94	#num#	k4
<g/>
–	–	k?
<g/>
102	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Neillands	Neillands	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
103	#num#	k4
<g/>
–	–	k?
<g/>
106	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Neillands	Neillands	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
106	#num#	k4
<g/>
–	–	k?
<g/>
116	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Neillands	Neillands	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
117	#num#	k4
<g/>
–	–	k?
<g/>
124	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Neillands	Neillands	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
124	#num#	k4
<g/>
–	–	k?
<g/>
126	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Neillands	Neillands	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
126	#num#	k4
<g/>
–	–	k?
<g/>
131	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Neillands	Neillands	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
134	#num#	k4
<g/>
–	–	k?
<g/>
137	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Neillands	Neillands	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
140	#num#	k4
<g/>
–	–	k?
<g/>
144	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Neillands	Neillands	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
144	#num#	k4
<g/>
–	–	k?
<g/>
163	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Neillands	Neillands	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
164	#num#	k4
<g/>
–	–	k?
<g/>
174	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Neillands	Neillands	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
175	#num#	k4
<g/>
–	–	k?
<g/>
185	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
NEILLANDS	NEILLANDS	kA
<g/>
,	,	kIx,
Robin	robin	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Války	válka	k1gFnSc2
růží	růž	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Naše	náš	k3xOp1gNnSc1
vojsko	vojsko	k1gNnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
187	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
206	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
620	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Války	válka	k1gFnSc2
růží	růžit	k5eAaImIp3nS
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Lancasterové	Lancaster	k1gMnPc1
a	a	k8xC
Yorkové	Yorek	k1gMnPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Války	válka	k1gFnPc1
růží	růžit	k5eAaImIp3nP
</s>
<s>
První	první	k4xOgFnSc1
bitva	bitva	k1gFnSc1
u	u	k7c2
St	St	kA
Albans	Albans	k1gInSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Blore	Blor	k1gInSc5
Heath	Heath	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Ludford	Ludforda	k1gFnPc2
Bridge	Bridg	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Northamptonu	Northampton	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Wakefieldu	Wakefield	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Mortimers	Mortimersa	k1gFnPc2
Cross	Crossa	k1gFnPc2
•	•	k?
Druhá	druhý	k4xOgFnSc1
bitva	bitva	k1gFnSc1
u	u	k7c2
St	St	kA
Albans	Albans	k1gInSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Ferrybridge	Ferrybridg	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Towtonu	Towton	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Hedgeley	Hedgelea	k1gFnSc2
Moor	Moor	k1gInSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Hexhamu	Hexham	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Edgecote	Edgecot	k1gInSc5
Moor	Moor	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Losecoat	Losecoat	k1gInSc4
Field	Field	k1gInSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Barnetu	Barnet	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Tewkesbury	Tewkesbura	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Bosworthu	Bosworth	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Stoke	Stok	k1gInSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4178464-9	4178464-9	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85056769	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85056769	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Středověk	středověk	k1gInSc1
|	|	kIx~
Válka	válka	k1gFnSc1
</s>
