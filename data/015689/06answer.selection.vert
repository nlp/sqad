<s>
Nejzřejmějším	zřejmý	k2eAgInSc7d3
důsledkem	důsledek	k1gInSc7
byl	být	k5eAaImAgInS
pád	pád	k1gInSc1
rodu	rod	k1gInSc2
Plantagenetů	Plantagenet	k1gInPc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
na	na	k7c6
anglickém	anglický	k2eAgInSc6d1
trůnu	trůn	k1gInSc6
nahrazen	nahradit	k5eAaPmNgInS
rodem	rod	k1gInSc7
Tudorovců	Tudorovec	k1gMnPc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
v	v	k7c6
následujících	následující	k2eAgNnPc6d1
letech	léto	k1gNnPc6
dramaticky	dramaticky	k6eAd1
změnil	změnit	k5eAaPmAgInS
Anglii	Anglie	k1gFnSc4
<g/>
.	.	kIx.
</s>