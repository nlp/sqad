<s>
Q	Q	kA
</s>
<s>
Q	Q	kA
</s>
<s>
ZnakQNázev	ZnakQNázet	k5eAaPmDgInS
v	v	k7c6
UnicoduKódovánídechexUnicode	UnicoduKódovánídechexUnicod	k1gInSc5
<g/>
81	#num#	k4
<g/>
U	U	kA
<g/>
+	+	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
51	#num#	k4
<g/>
UTF-	UTF-	k1gFnPc2
<g/>
88151	#num#	k4
<g/>
Číselná	číselný	k2eAgFnSc1d1
entitaQQ	entitaQQ	k?
</s>
<s>
ZnakqNázev	ZnakqNázet	k5eAaPmDgInS
v	v	k7c6
UnicoduKódovánídechexUnicode	UnicoduKódovánídechexUnicod	k1gInSc5
<g/>
113	#num#	k4
<g/>
U	U	kA
<g/>
+	+	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
71	#num#	k4
<g/>
UTF-	UTF-	k1gFnPc2
<g/>
811371	#num#	k4
<g/>
Číselná	číselný	k2eAgFnSc1d1
entitaqq	entitaqq	k?
Hláskování	hláskování	k1gNnPc1
české	český	k2eAgFnSc2d1
</s>
<s>
Quido	Quido	k1gNnSc1
Hláskování	hláskování	k1gNnSc2
mezinárodní	mezinárodní	k2eAgFnSc2d1
</s>
<s>
Quebec	Quebec	k1gInSc1
Braille	Braille	k1gFnSc2
</s>
<s>
⠟	⠟	k?
Morseova	Morseův	k2eAgFnSc1d1
abeceda	abeceda	k1gFnSc1
</s>
<s>
–	–	k?
<g/>
⋅	⋅	k?
<g/>
–	–	k?
(	(	kIx(
<g/>
kvílí	kvílet	k5eAaImIp3nS
orkán	orkán	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Vizuální	vizuální	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
</s>
<s>
Q	Q	kA
je	být	k5eAaImIp3nS
17	#num#	k4
<g/>
.	.	kIx.
písmeno	písmeno	k1gNnSc4
latinské	latinský	k2eAgFnSc2d1
abecedy	abeceda	k1gFnSc2
<g/>
,	,	kIx,
vzniklo	vzniknout	k5eAaPmAgNnS
z	z	k7c2
řeckého	řecký	k2eAgMnSc2d1
Koppa	Kopp	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
angličtině	angličtina	k1gFnSc6
je	být	k5eAaImIp3nS
Q	Q	kA
zkratkou	zkratka	k1gFnSc7
pro	pro	k7c4
question	question	k1gInSc4
=	=	kIx~
otázka	otázka	k1gFnSc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
v	v	k7c6
termínech	termín	k1gInPc6
jako	jako	k8xS,k8xC
FAQ	FAQ	kA
(	(	kIx(
<g/>
často	často	k6eAd1
kladené	kladený	k2eAgFnPc4d1
otázky	otázka	k1gFnPc4
<g/>
)	)	kIx)
či	či	k8xC
Q	Q	kA
<g/>
&	&	k?
<g/>
A	a	k9
(	(	kIx(
<g/>
otázky	otázka	k1gFnPc4
a	a	k8xC
odpovědi	odpověď	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Písmeno	písmeno	k1gNnSc1
q	q	k?
(	(	kIx(
<g/>
quintal	quintal	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
taky	taky	k9
zkratka	zkratka	k1gFnSc1
pro	pro	k7c4
metrický	metrický	k2eAgInSc4d1
cent	cent	k1gInSc4
</s>
<s>
V	v	k7c6
biblistice	biblistika	k1gFnSc6
označuje	označovat	k5eAaImIp3nS
Q	Q	kA
sbírku	sbírka	k1gFnSc4
logií	logie	k1gFnPc2
Q.	Q.	kA
</s>
<s>
V	v	k7c6
biochemii	biochemie	k1gFnSc6
je	být	k5eAaImIp3nS
Q	Q	kA
symbol	symbol	k1gInSc1
pro	pro	k7c4
aminokyselinu	aminokyselina	k1gFnSc4
glutamin	glutamin	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
elektrotechnice	elektrotechnika	k1gFnSc6
je	být	k5eAaImIp3nS
Q	Q	kA
označení	označení	k1gNnSc2
pro	pro	k7c4
kvalitu	kvalita	k1gFnSc4
rezonančního	rezonanční	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
,	,	kIx,
např.	např.	kA
filtru	filtr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
fyzice	fyzika	k1gFnSc6
</s>
<s>
Q	Q	kA
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc4
pro	pro	k7c4
teplo	teplo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Q	Q	kA
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc4
pro	pro	k7c4
elektrický	elektrický	k2eAgInSc4d1
náboj	náboj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
kartách	karta	k1gFnPc6
je	být	k5eAaImIp3nS
Q	Q	kA
označení	označení	k1gNnSc2
pro	pro	k7c4
královnu	královna	k1gFnSc4
(	(	kIx(
<g/>
svrška	svrška	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
kinematografii	kinematografie	k1gFnSc6
</s>
<s>
Q	Q	kA
je	být	k5eAaImIp3nS
krycí	krycí	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
postavy	postava	k1gFnSc2
ze	z	k7c2
série	série	k1gFnSc2
o	o	k7c4
Jamesi	Jamese	k1gFnSc4
Bondovi	Bonda	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s>
Q	Q	kA
(	(	kIx(
<g/>
Star	Star	kA
Trek	Trek	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jméno	jméno	k1gNnSc4
postavy	postava	k1gFnSc2
a	a	k8xC
rasy	rasa	k1gFnSc2
ze	z	k7c2
sci-fi	sci-fi	k1gNnSc2
série	série	k1gFnSc2
Star	Star	kA
Trek	Trek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Q	Q	kA
je	být	k5eAaImIp3nS
queer	queer	k1gInSc4
magazín	magazín	k1gInSc4
České	český	k2eAgFnSc2d1
televize	televize	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
lékařství	lékařství	k1gNnSc6
je	být	k5eAaImIp3nS
Q	Q	kA
jméno	jméno	k1gNnSc4
choroby	choroba	k1gFnSc2
–	–	k?
viz	vidět	k5eAaImRp2nS
Q-horečka	Q-horečka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
lingvistice	lingvistika	k1gFnSc6
je	být	k5eAaImIp3nS
Q	Q	kA
název	název	k1gInSc1
pro	pro	k7c4
neznělou	znělý	k2eNgFnSc4d1
uvulární	uvulární	k2eAgFnSc4d1
plozivu	ploziva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
literatuře	literatura	k1gFnSc6
</s>
<s>
Q	Q	kA
(	(	kIx(
<g/>
časopis	časopis	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
britský	britský	k2eAgInSc1d1
hudební	hudební	k2eAgInSc1d1
časopis	časopis	k1gInSc1
</s>
<s>
Q	Q	kA
je	být	k5eAaImIp3nS
název	název	k1gInSc1
historické	historický	k2eAgFnSc2d1
novely	novela	k1gFnSc2
od	od	k7c2
Luthera	Luther	k1gMnSc2
Blissetta	Blissett	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Q	Q	kA
je	být	k5eAaImIp3nS
pseudonym	pseudonym	k1gInSc1
spisovatele	spisovatel	k1gMnSc2
Arthura	Arthur	k1gMnSc2
Quiller-Couche	Quiller-Couch	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Q	Q	kA
je	být	k5eAaImIp3nS
jméno	jméno	k1gNnSc4
hlavní	hlavní	k2eAgFnSc2d1
postavy	postava	k1gFnSc2
čínské	čínský	k2eAgFnSc2d1
novely	novela	k1gFnSc2
Skutečný	skutečný	k2eAgInSc1d1
příběh	příběh	k1gInSc1
Ah	ah	k0
Q	Q	kA
spisovatele	spisovatel	k1gMnSc2
Lu	Lu	k1gMnSc2
Xuna	Xunus	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
matematice	matematika	k1gFnSc6
je	být	k5eAaImIp3nS
ℚ	ℚ	k?
označení	označení	k1gNnSc4
množiny	množina	k1gFnSc2
racionálních	racionální	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Q	Q	kA
je	být	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgFnSc1d1
poznávací	poznávací	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Kataru	katar	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
soustavě	soustava	k1gFnSc6
SI	se	k3xPyFc3
je	být	k5eAaImIp3nS
q	q	k?
značka	značka	k1gFnSc1
metrického	metrický	k2eAgInSc2d1
centu	cent	k1gInSc2
(	(	kIx(
<g/>
100	#num#	k4
kg	kg	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
vojenství	vojenství	k1gNnSc6
je	být	k5eAaImIp3nS
Q	Q	kA
označení	označení	k1gNnSc1
protiponorkových	protiponorkový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
z	z	k7c2
1	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
–	–	k?
viz	vidět	k5eAaImRp2nS
Q	Q	kA
(	(	kIx(
<g/>
loď	loď	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Ԛ	Ԛ	k?
(	(	kIx(
<g/>
zastaralé	zastaralý	k2eAgNnSc1d1
písmeno	písmeno	k1gNnSc1
neslovanské	slovanský	k2eNgFnSc2d1
varianty	varianta	k1gFnSc2
cyrilice	cyrilice	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Viz	vidět	k5eAaImRp2nS
též	též	k9
rozcestník	rozcestník	k1gInSc4
Q	Q	kA
kód	kód	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Galerie	galerie	k1gFnSc1
Q	Q	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Q	Q	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Latinka	latinka	k1gFnSc1
Základní	základní	k2eAgFnSc1d1
písmena	písmeno	k1gNnPc4
dle	dle	k7c2
ISO	ISO	kA
<g/>
*	*	kIx~
</s>
<s>
Aa	Aa	k?
</s>
<s>
Bb	Bb	k?
</s>
<s>
Cc	Cc	k?
</s>
<s>
Dd	Dd	k?
</s>
<s>
Ee	Ee	k?
</s>
<s>
Ff	ff	kA
</s>
<s>
Gg	Gg	k?
</s>
<s>
Hh	Hh	k?
</s>
<s>
Ii	Ii	k?
</s>
<s>
Jj	Jj	k?
</s>
<s>
Kk	Kk	k?
</s>
<s>
Ll	Ll	k?
</s>
<s>
Mm	mm	kA
</s>
<s>
Nn	Nn	k?
</s>
<s>
Oo	Oo	k?
</s>
<s>
Pp	Pp	k?
</s>
<s>
Qq	Qq	k?
</s>
<s>
Rr	Rr	k?
</s>
<s>
Ss	Ss	k?
</s>
<s>
Tt	Tt	k?
</s>
<s>
Uu	Uu	k?
</s>
<s>
Vv	Vv	k?
</s>
<s>
Ww	Ww	k?
</s>
<s>
Xx	Xx	k?
</s>
<s>
Yy	Yy	k?
</s>
<s>
Zz	Zz	k?
Další	další	k2eAgFnSc1d1
písmena	písmeno	k1gNnPc4
</s>
<s>
Ə	Ə	k?
</s>
<s>
Ǝ	Ǝ	k?
</s>
<s>
Ɛ	Ɛ	k?
</s>
<s>
Ɣ	Ɣ	k?
</s>
<s>
Ɩ	Ɩ	k?
</s>
<s>
Ɐ	Ɐ	k?
</s>
<s>
Ꝺ	Ꝺ	k?
</s>
<s>
Þ	Þ	k?
</s>
<s>
Ꞇ	Ꞇ	k?
</s>
<s>
ʇ	ʇ	k?
</s>
<s>
Ŋ	Ŋ	k?
</s>
<s>
Ɔ	Ɔ	k?
</s>
<s>
ſ	ſ	k?
</s>
<s>
Ƨ	Ƨ	k?
</s>
<s>
Ʋ	Ʋ	k?
</s>
<s>
Ʊ	Ʊ	k?
</s>
<s>
Ꝩ	Ꝩ	k?
</s>
<s>
Ꝡ	Ꝡ	k?
</s>
<s>
ẜ	ẜ	k?
</s>
<s>
Ꞅ	Ꞅ	k?
</s>
<s>
Ꝭ	Ꝭ	k?
</s>
<s>
Ʒ	Ʒ	k?
</s>
<s>
Ƹ	Ƹ	k?
</s>
<s>
Ȣ	Ȣ	k?
</s>
<s>
Ʌ	Ʌ	k?
</s>
<s>
Ꞁ	Ꞁ	k?
</s>
<s>
Ỽ	Ỽ	k?
</s>
<s>
Ỿ	Ỿ	k?
</s>
<s>
ʚ	ʚ	k?
</s>
<s>
Ƣ	Ƣ	k?
</s>
<s>
Κ	Κ	k?
</s>
<s>
Ƿ	Ƿ	k?
</s>
<s>
Ⅎ	Ⅎ	k?
</s>
<s>
Ꝼ	Ꝼ	k?
</s>
<s>
Ꝛ	Ꝛ	k?
</s>
<s>
Ʀ	Ʀ	k?
</s>
<s>
Ᵹ	Ᵹ	k?
</s>
<s>
Ꝿ	Ꝿ	k?
</s>
<s>
Ɋ	Ɋ	k?
</s>
<s>
Ь	Ь	k?
</s>
<s>
ʞ	ʞ	k?
</s>
<s>
Ȝ	Ȝ	k?
</s>
<s>
Ꝫ	Ꝫ	k?
</s>
<s>
Ꜭ	Ꜭ	k?
</s>
<s>
ɥ	ɥ	k?
</s>
<s>
Ꝝ	Ꝝ	k?
</s>
<s>
Ƽ	Ƽ	k?
</s>
<s>
Ꝯ	Ꝯ	k?
</s>
<s>
Ɂ	Ɂ	k?
</s>
<s>
Ƕ	Ƕ	k?
</s>
<s>
Ꜧ	Ꜧ	k?
Spřežky	spřežka	k1gFnPc1
a	a	k8xC
ligatury	ligatura	k1gFnPc1
</s>
<s>
Æ	Æ	k?
</s>
<s>
Œ	Œ	k?
</s>
<s>
ẞ	ẞ	k?
</s>
<s>
Ỻ	Ỻ	k?
</s>
<s>
Ǆ	Ǆ	k?
</s>
<s>
Ǳ	Ǳ	k?
</s>
<s>
Ǉ	Ǉ	k?
</s>
<s>
Ǌ	Ǌ	k?
</s>
<s>
ȸ	ȸ	k?
</s>
<s>
ȹ	ȹ	k?
</s>
<s>
Ĳ	Ĳ	k?
</s>
<s>
ch	ch	k0
</s>
<s>
cz	cz	k?
</s>
<s>
gb	gb	k?
</s>
<s>
gh	gh	k?
</s>
<s>
gy	gy	k?
</s>
<s>
ll	ll	k?
</s>
<s>
ly	ly	k?
</s>
<s>
nh	nh	k?
</s>
<s>
ny	ny	k?
</s>
<s>
rr	rr	k?
</s>
<s>
sh	sh	k?
</s>
<s>
sz	sz	k?
</s>
<s>
th	th	k?
Varianty	varianta	k1gFnSc2
písmena	písmeno	k1gNnSc2
Q	Q	kA
</s>
<s>
Q	Q	kA
<g/>
̌	̌	k?
<g/>
q	q	k?
<g/>
̌	̌	k?
</s>
<s>
Q	Q	kA
<g/>
̊	̊	k?
<g/>
q	q	k?
<g/>
̊	̊	k?
</s>
<s>
Q	Q	kA
<g/>
̈	̈	k?
<g/>
q	q	k?
<g/>
̈	̈	k?
</s>
<s>
Q	Q	kA
<g/>
̣	̣	k?
<g/>
̈	̈	k?
<g/>
q	q	k?
<g/>
̣	̣	k?
<g/>
̈	̈	k?
</s>
<s>
Q	Q	kA
<g/>
̄	̄	k?
<g/>
q	q	k?
<g/>
̄	̄	k?
</s>
<s>
Q	Q	kA
<g/>
̃	̃	k?
<g/>
q	q	k?
<g/>
̃	̃	k?
</s>
<s>
Q	Q	kA
<g/>
́	́	k?
<g/>
q	q	k?
<g/>
́	́	k?
</s>
<s>
Q	Q	kA
<g/>
̇	̇	k?
<g/>
q	q	k?
<g/>
̇	̇	k?
</s>
<s>
Q	Q	kA
<g/>
̣	̣	k?
<g/>
q	q	k?
<g/>
̣	̣	k?
</s>
<s>
Q	Q	kA
<g/>
̣	̣	k?
<g/>
̇	̇	k?
<g/>
q	q	k?
<g/>
̣	̣	k?
<g/>
̇	̇	k?
</s>
<s>
Q	Q	kA
<g/>
̧	̧	k?
<g/>
q	q	k?
<g/>
̧	̧	k?
</s>
<s>
Ꝙ	Ꝙ	k?
</s>
<s>
Ꝗ	Ꝗ	k?
*	*	kIx~
tučně	tučně	k6eAd1
jsou	být	k5eAaImIp3nP
písmena	písmeno	k1gNnPc4
původní	původní	k2eAgFnSc2d1
latinské	latinský	k2eAgFnSc2d1
abecedy	abeceda	k1gFnSc2
</s>
<s>
Seznam	seznam	k1gInSc1
jazyků	jazyk	k1gInPc2
používajících	používající	k2eAgInPc2d1
latinku	latinka	k1gFnSc4
</s>
