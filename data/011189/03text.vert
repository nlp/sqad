<p>
<s>
Mamutí	mamutí	k2eAgFnSc1d1	mamutí
jeskyně	jeskyně	k1gFnSc1	jeskyně
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Mammoth	Mammoth	k1gMnSc1	Mammoth
Cave	Cav	k1gFnSc2	Cav
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejdelší	dlouhý	k2eAgFnSc7d3	nejdelší
jeskyní	jeskyně	k1gFnSc7	jeskyně
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
největší	veliký	k2eAgFnSc4d3	veliký
atrakci	atrakce	k1gFnSc4	atrakce
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
Mamutí	mamutí	k2eAgFnSc2d1	mamutí
jeskyně	jeskyně	k1gFnSc2	jeskyně
v	v	k7c4	v
Kentucky	Kentuck	k1gInPc4	Kentuck
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Známá	známý	k2eAgFnSc1d1	známá
délka	délka	k1gFnSc1	délka
všech	všecek	k3xTgInPc2	všecek
podzemních	podzemní	k2eAgInPc2d1	podzemní
prostorů	prostor	k1gInPc2	prostor
v	v	k7c6	v
jeskyni	jeskyně	k1gFnSc6	jeskyně
je	být	k5eAaImIp3nS	být
630	[number]	k4	630
km	km	kA	km
<g/>
,	,	kIx,	,
další	další	k2eAgInPc4d1	další
objevy	objev	k1gInPc4	objev
ji	on	k3xPp3gFnSc4	on
ovšem	ovšem	k9	ovšem
každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
o	o	k7c4	o
několik	několik	k4yIc4	několik
kilometrů	kilometr	k1gInPc2	kilometr
prodlužují	prodlužovat	k5eAaImIp3nP	prodlužovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
jeskyni	jeskyně	k1gFnSc6	jeskyně
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc1	několik
prohlídkových	prohlídkový	k2eAgFnPc2d1	prohlídková
tras	trasa	k1gFnPc2	trasa
<g/>
,	,	kIx,	,
nejzajímavější	zajímavý	k2eAgFnPc4d3	nejzajímavější
je	být	k5eAaImIp3nS	být
plavba	plavba	k1gFnSc1	plavba
po	po	k7c6	po
podzemní	podzemní	k2eAgFnSc6d1	podzemní
říčce	říčka	k1gFnSc6	říčka
Echo	echo	k1gNnSc1	echo
<g/>
.	.	kIx.	.
</s>
<s>
Vstupní	vstupní	k2eAgFnPc1d1	vstupní
části	část	k1gFnPc1	část
jeskyně	jeskyně	k1gFnSc2	jeskyně
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
tisíce	tisíc	k4xCgInPc1	tisíc
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
dokládají	dokládat	k5eAaImIp3nP	dokládat
archeologické	archeologický	k2eAgFnPc4d1	archeologická
vykopávky	vykopávka	k1gFnPc4	vykopávka
<g/>
.	.	kIx.	.
</s>
<s>
Jeskyně	jeskyně	k1gFnSc1	jeskyně
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
John	John	k1gMnSc1	John
Houchinsem	Houchins	k1gMnSc7	Houchins
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1797	[number]	k4	1797
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
těženo	těžen	k2eAgNnSc1d1	těženo
guano	guano	k1gNnSc1	guano
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgMnSc1d1	sloužící
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
střelného	střelný	k2eAgInSc2d1	střelný
prachu	prach	k1gInSc2	prach
a	a	k8xC	a
jeskyně	jeskyně	k1gFnSc1	jeskyně
byla	být	k5eAaImAgFnS	být
postupně	postupně	k6eAd1	postupně
objevována	objevován	k2eAgFnSc1d1	objevována
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
jeskyně	jeskyně	k1gFnSc1	jeskyně
stává	stávat	k5eAaImIp3nS	stávat
velkou	velký	k2eAgFnSc7d1	velká
turistickou	turistický	k2eAgFnSc7d1	turistická
atrakcí	atrakce	k1gFnSc7	atrakce
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
turistickému	turistický	k2eAgNnSc3d1	turistické
zpřístupnění	zpřístupnění	k1gNnSc3	zpřístupnění
a	a	k8xC	a
následně	následně	k6eAd1	následně
k	k	k7c3	k
"	"	kIx"	"
<g/>
bojům	boj	k1gInPc3	boj
<g/>
"	"	kIx"	"
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
společnosti	společnost	k1gFnPc1	společnost
budou	být	k5eAaImBp3nP	být
moci	moct	k5eAaImF	moct
vybírat	vybírat	k5eAaImF	vybírat
vstupné	vstupné	k1gNnSc4	vstupné
od	od	k7c2	od
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
objevování	objevování	k1gNnSc3	objevování
dalších	další	k2eAgFnPc2d1	další
jeskynních	jeskynní	k2eAgFnPc2d1	jeskynní
prostor	prostora	k1gFnPc2	prostora
i	i	k8xC	i
propojování	propojování	k1gNnSc2	propojování
s	s	k7c7	s
okolními	okolní	k2eAgFnPc7d1	okolní
jeskyněmi	jeskyně	k1gFnPc7	jeskyně
a	a	k8xC	a
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
jeskyně	jeskyně	k1gFnSc1	jeskyně
stává	stávat	k5eAaImIp3nS	stávat
nejdelším	dlouhý	k2eAgInSc7d3	nejdelší
jeskynním	jeskynní	k2eAgInSc7d1	jeskynní
systémem	systém	k1gInSc7	systém
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
je	být	k5eAaImIp3nS	být
jeskyně	jeskyně	k1gFnSc1	jeskyně
propojena	propojit	k5eAaPmNgFnS	propojit
s	s	k7c7	s
tehdy	tehdy	k6eAd1	tehdy
třetí	třetí	k4xOgFnSc7	třetí
nejdelší	dlouhý	k2eAgFnSc7d3	nejdelší
jeskyní	jeskyně	k1gFnSc7	jeskyně
na	na	k7c6	na
světě	svět	k1gInSc6	svět
-	-	kIx~	-
Flintovou	flintový	k2eAgFnSc7d1	Flintová
jeskyní	jeskyně	k1gFnSc7	jeskyně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
jeskyně	jeskyně	k1gFnSc1	jeskyně
skoro	skoro	k6eAd1	skoro
třikrát	třikrát	k6eAd1	třikrát
delší	dlouhý	k2eAgFnSc1d2	delší
než	než	k8xS	než
druhá	druhý	k4xOgFnSc1	druhý
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
jeskyně	jeskyně	k1gFnSc1	jeskyně
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
Optimistická	optimistický	k2eAgFnSc1d1	optimistická
jeskyně	jeskyně	k1gFnSc1	jeskyně
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Mamutí	mamutí	k2eAgFnSc2d1	mamutí
jeskyně	jeskyně	k1gFnSc2	jeskyně
==	==	k?	==
</s>
</p>
<p>
<s>
Okolí	okolí	k1gNnSc1	okolí
jeskyně	jeskyně	k1gFnSc2	jeskyně
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
národním	národní	k2eAgInSc7d1	národní
parkem	park	k1gInSc7	park
rozkládajícím	rozkládající	k2eAgInPc3d1	rozkládající
se	se	k3xPyFc4	se
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Edmonson	Edmonsona	k1gFnPc2	Edmonsona
na	na	k7c6	na
rozloze	rozloha	k1gFnSc6	rozloha
214	[number]	k4	214
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
zapsán	zapsat	k5eAaPmNgMnS	zapsat
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poblíž	poblíž	k7c2	poblíž
jeskyně	jeskyně	k1gFnSc2	jeskyně
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
další	další	k2eAgInPc1d1	další
dva	dva	k4xCgInPc1	dva
velké	velký	k2eAgInPc1d1	velký
jeskyní	jeskyně	k1gFnPc2	jeskyně
systémy	systém	k1gInPc1	systém
Fisher	Fishra	k1gFnPc2	Fishra
Ridge	Ridge	k1gFnPc2	Ridge
(	(	kIx(	(
<g/>
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
chodeb	chodba	k1gFnPc2	chodba
172	[number]	k4	172
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
Martin	Martin	k1gMnSc1	Martin
Ridge	Ridg	k1gFnSc2	Ridg
(	(	kIx(	(
<g/>
51	[number]	k4	51
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
je	on	k3xPp3gInPc4	on
podařilo	podařit	k5eAaPmAgNnS	podařit
propojit	propojit	k5eAaPmF	propojit
s	s	k7c7	s
Mamutí	mamutí	k2eAgFnSc7d1	mamutí
jeskyní	jeskyně	k1gFnSc7	jeskyně
<g/>
,	,	kIx,	,
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
systém	systém	k1gInSc1	systém
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
délku	délka	k1gFnSc4	délka
přes	přes	k7c4	přes
800	[number]	k4	800
km	km	kA	km
chodeb	chodba	k1gFnPc2	chodba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mamutí	mamutí	k2eAgFnSc2d1	mamutí
jeskyně	jeskyně	k1gFnSc2	jeskyně
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
skupiny	skupina	k1gFnSc2	skupina
pro	pro	k7c4	pro
obnovu	obnova	k1gFnSc4	obnova
jeskyně	jeskyně	k1gFnSc2	jeskyně
</s>
</p>
<p>
<s>
Stephen	Stephen	k2eAgInSc1d1	Stephen
Bishop	Bishop	k1gInSc1	Bishop
a	a	k8xC	a
Mamutí	mamutí	k2eAgFnSc1d1	mamutí
jeskyně	jeskyně	k1gFnSc1	jeskyně
</s>
</p>
