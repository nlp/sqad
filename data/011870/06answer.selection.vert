<s>
Teologie	teologie	k1gFnSc1	teologie
osvobození	osvobození	k1gNnSc2	osvobození
(	(	kIx(	(
<g/>
Liberation	Liberation	k1gInSc1	Liberation
theology	theolog	k1gMnPc4	theolog
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
první	první	k4xOgNnSc4	první
komplexní	komplexní	k2eAgNnSc4d1	komplexní
teologické	teologický	k2eAgNnSc4d1	teologické
hnutí	hnutí	k1gNnSc4	hnutí
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
církví	církev	k1gFnPc2	církev
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c4	na
neutěšené	utěšený	k2eNgInPc4d1	neutěšený
poměry	poměr	k1gInPc4	poměr
tzv.	tzv.	kA	tzv.
třetího	třetí	k4xOgInSc2	třetí
světa	svět	k1gInSc2	svět
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nP	snažit
se	se	k3xPyFc4	se
je	on	k3xPp3gFnPc4	on
vyřešit	vyřešit	k5eAaPmF	vyřešit
skrze	skrze	k?	skrze
křesťanské	křesťanský	k2eAgNnSc4d1	křesťanské
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
i	i	k9	i
revolučně	revolučně	k6eAd1	revolučně
<g/>
.	.	kIx.	.
</s>
