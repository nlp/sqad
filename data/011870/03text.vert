<p>
<s>
Teologie	teologie	k1gFnSc1	teologie
osvobození	osvobození	k1gNnSc2	osvobození
(	(	kIx(	(
<g/>
Liberation	Liberation	k1gInSc1	Liberation
theology	theolog	k1gMnPc4	theolog
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
první	první	k4xOgNnSc4	první
komplexní	komplexní	k2eAgNnSc4d1	komplexní
teologické	teologický	k2eAgNnSc4d1	teologické
hnutí	hnutí	k1gNnSc4	hnutí
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
církví	církev	k1gFnPc2	církev
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c4	na
neutěšené	utěšený	k2eNgInPc4d1	neutěšený
poměry	poměr	k1gInPc4	poměr
tzv.	tzv.	kA	tzv.
třetího	třetí	k4xOgInSc2	třetí
světa	svět	k1gInSc2	svět
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nP	snažit
se	se	k3xPyFc4	se
je	on	k3xPp3gFnPc4	on
vyřešit	vyřešit	k5eAaPmF	vyřešit
skrze	skrze	k?	skrze
křesťanské	křesťanský	k2eAgNnSc4d1	křesťanské
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
i	i	k9	i
revolučně	revolučně	k6eAd1	revolučně
<g/>
.	.	kIx.	.
</s>
<s>
Hnutí	hnutí	k1gNnSc1	hnutí
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
<g/>
,	,	kIx,	,
vznik	vznik	k1gInSc1	vznik
==	==	k?	==
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
náznaky	náznak	k1gInPc1	náznak
teologie	teologie	k1gFnSc2	teologie
osvobození	osvobození	k1gNnSc2	osvobození
můžeme	moct	k5eAaImIp1nP	moct
pozorovat	pozorovat	k5eAaImF	pozorovat
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
za	za	k7c4	za
vznik	vznik	k1gInSc4	vznik
hnutí	hnutí	k1gNnSc2	hnutí
můžeme	moct	k5eAaImIp1nP	moct
považovat	považovat	k5eAaImF	považovat
Medelínskou	Medelínský	k2eAgFnSc4d1	Medelínský
konferenci	konference	k1gFnSc4	konference
biskupů	biskup	k1gMnPc2	biskup
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vydala	vydat	k5eAaPmAgFnS	vydat
"	"	kIx"	"
<g/>
Základní	základní	k2eAgFnSc1d1	základní
dokument	dokument	k1gInSc1	dokument
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
formuloval	formulovat	k5eAaImAgMnS	formulovat
podstatu	podstata	k1gFnSc4	podstata
teologie	teologie	k1gFnSc2	teologie
osvobození	osvobození	k1gNnSc2	osvobození
<g/>
.	.	kIx.	.
</s>
<s>
Klíčovou	klíčový	k2eAgFnSc7d1	klíčová
postavou	postava	k1gFnSc7	postava
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Gustavo	Gustava	k1gFnSc5	Gustava
Gutiérrez	Gutiérrez	k1gMnSc1	Gutiérrez
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
vydal	vydat	k5eAaPmAgMnS	vydat
knihu	kniha	k1gFnSc4	kniha
"	"	kIx"	"
<g/>
Teologie	teologie	k1gFnSc1	teologie
osvobození	osvobození	k1gNnSc2	osvobození
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
formulovaní	formulovaný	k2eAgMnPc1d1	formulovaný
teologie	teologie	k1gFnSc2	teologie
osvobození	osvobození	k1gNnPc2	osvobození
vedl	vést	k5eAaImAgInS	vést
především	především	k6eAd1	především
vývoj	vývoj	k1gInSc1	vývoj
zemí	zem	k1gFnPc2	zem
v	v	k7c6	v
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
zemích	zem	k1gFnPc6	zem
byla	být	k5eAaImAgFnS	být
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
pevně	pevně	k6eAd1	pevně
zakořeněna	zakořenit	k5eAaPmNgFnS	zakořenit
již	již	k6eAd1	již
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
španělské	španělský	k2eAgFnSc2d1	španělská
a	a	k8xC	a
portugalské	portugalský	k2eAgFnSc2d1	portugalská
kolonizace	kolonizace	k1gFnSc2	kolonizace
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
zde	zde	k6eAd1	zde
však	však	k9	však
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
přetrvávala	přetrvávat	k5eAaImAgFnS	přetrvávat
extrémní	extrémní	k2eAgFnSc1d1	extrémní
chudoba	chudoba	k1gFnSc1	chudoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemích	zem	k1gFnPc6	zem
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
převládá	převládat	k5eAaImIp3nS	převládat
zahraniční	zahraniční	k2eAgInSc1d1	zahraniční
kapitál	kapitál	k1gInSc1	kapitál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
monopoly	monopol	k1gInPc4	monopol
s	s	k7c7	s
obrovskou	obrovský	k2eAgFnSc7d1	obrovská
koncentrací	koncentrace	k1gFnSc7	koncentrace
bohatství	bohatství	k1gNnSc2	bohatství
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
několika	několik	k4yIc2	několik
málo	málo	k4c1	málo
vlastníků	vlastník	k1gMnPc2	vlastník
firem	firma	k1gFnPc2	firma
nebo	nebo	k8xC	nebo
pozemků	pozemek	k1gInPc2	pozemek
<g/>
.	.	kIx.	.
</s>
<s>
Vládní	vládní	k2eAgFnSc1d1	vládní
podpora	podpora	k1gFnSc1	podpora
těchto	tento	k3xDgInPc2	tento
často	často	k6eAd1	často
severoamerických	severoamerický	k2eAgInPc2d1	severoamerický
konglomerátů	konglomerát	k1gInPc2	konglomerát
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
sociálnímu	sociální	k2eAgNnSc3d1	sociální
napětí	napětí	k1gNnSc3	napětí
a	a	k8xC	a
radikalizaci	radikalizace	k1gFnSc3	radikalizace
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
tak	tak	k9	tak
vzniká	vznikat	k5eAaImIp3nS	vznikat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
guerillových	guerillový	k2eAgNnPc2d1	guerillové
revolučních	revoluční	k2eAgNnPc2d1	revoluční
hnutí	hnutí	k1gNnPc2	hnutí
od	od	k7c2	od
Kuby	Kuba	k1gFnSc2	Kuba
přes	přes	k7c4	přes
Kolumbii	Kolumbie	k1gFnSc4	Kolumbie
<g/>
,	,	kIx,	,
Nikaraguu	Nikaragua	k1gFnSc4	Nikaragua
až	až	k9	až
po	po	k7c6	po
Chile	Chile	k1gNnSc6	Chile
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
těchto	tento	k3xDgNnPc2	tento
hnutí	hnutí	k1gNnSc2	hnutí
byla	být	k5eAaImAgNnP	být
inspirována	inspirovat	k5eAaBmNgNnP	inspirovat
extrémně	extrémně	k6eAd1	extrémně
levicovou	levicový	k2eAgFnSc7d1	levicová
rétorikou	rétorika	k1gFnSc7	rétorika
marxismu	marxismus	k1gInSc2	marxismus
<g/>
,	,	kIx,	,
komunismu	komunismus	k1gInSc2	komunismus
a	a	k8xC	a
socialismu	socialismus	k1gInSc2	socialismus
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
těmito	tento	k3xDgMnPc7	tento
odpůrci	odpůrce	k1gMnPc7	odpůrce
režimů	režim	k1gInPc2	režim
však	však	k8xC	však
byli	být	k5eAaImAgMnP	být
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
také	také	k9	také
upřímně	upřímně	k6eAd1	upřímně
věřící	věřící	k2eAgMnPc1d1	věřící
křesťané	křesťan	k1gMnPc1	křesťan
<g/>
.	.	kIx.	.
<g/>
Kněží	kněz	k1gMnPc1	kněz
a	a	k8xC	a
biskupové	biskup	k1gMnPc1	biskup
<g/>
,	,	kIx,	,
studenti	student	k1gMnPc1	student
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
mladí	mladý	k2eAgMnPc1d1	mladý
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
především	především	k9	především
v	v	k7c4	v
Brazílii	Brazílie	k1gFnSc4	Brazílie
připojili	připojit	k5eAaPmAgMnP	připojit
ke	k	k7c3	k
Katolické	katolický	k2eAgFnSc3d1	katolická
akci	akce	k1gFnSc3	akce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
snažila	snažit	k5eAaImAgFnS	snažit
zajistit	zajistit	k5eAaPmF	zajistit
sociální	sociální	k2eAgFnSc4d1	sociální
stabilitu	stabilita	k1gFnSc4	stabilita
pro	pro	k7c4	pro
katolické	katolický	k2eAgFnPc4d1	katolická
věřící	věřící	k1gFnPc4	věřící
<g/>
.	.	kIx.	.
</s>
<s>
Následoval	následovat	k5eAaImAgMnS	následovat
2	[number]	k4	2
<g/>
.	.	kIx.	.
vatikánský	vatikánský	k2eAgInSc1d1	vatikánský
koncil	koncil	k1gInSc1	koncil
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
otevřel	otevřít	k5eAaPmAgInS	otevřít
možnosti	možnost	k1gFnSc3	možnost
nové	nový	k2eAgFnSc2d1	nová
pastorační	pastorační	k2eAgFnSc2d1	pastorační
a	a	k8xC	a
biblické	biblický	k2eAgFnSc2d1	biblická
praxe	praxe	k1gFnSc2	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
vatikánský	vatikánský	k2eAgInSc4d1	vatikánský
koncil	koncil	k1gInSc4	koncil
navázala	navázat	k5eAaPmAgFnS	navázat
Medelínská	Medelínský	k2eAgFnSc1d1	Medelínský
konference	konference	k1gFnSc1	konference
biskupů	biskup	k1gMnPc2	biskup
(	(	kIx(	(
<g/>
CELAM	CELAM	kA	CELAM
II	II	kA	II
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
v	v	k7c6	v
Kolumbii	Kolumbie	k1gFnSc6	Kolumbie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
chtěla	chtít	k5eAaImAgFnS	chtít
koncilní	koncilní	k2eAgFnPc4d1	koncilní
myšlenky	myšlenka	k1gFnPc4	myšlenka
využít	využít	k5eAaPmF	využít
pro	pro	k7c4	pro
Latinskou	latinský	k2eAgFnSc4d1	Latinská
Ameriku	Amerika	k1gFnSc4	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
tak	tak	k9	tak
hnutí	hnutí	k1gNnSc1	hnutí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vytvářelo	vytvářet	k5eAaImAgNnS	vytvářet
"	"	kIx"	"
<g/>
církev	církev	k1gFnSc1	církev
odspodu	odspodu	k6eAd1	odspodu
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
soustředilo	soustředit	k5eAaPmAgNnS	soustředit
se	se	k3xPyFc4	se
na	na	k7c4	na
chudé	chudý	k2eAgFnPc4d1	chudá
vrstvy	vrstva	k1gFnPc4	vrstva
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
biskupů	biskup	k1gMnPc2	biskup
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
upozorňovalo	upozorňovat	k5eAaImAgNnS	upozorňovat
na	na	k7c4	na
katastrofální	katastrofální	k2eAgFnPc4d1	katastrofální
životní	životní	k2eAgFnPc4d1	životní
podmínky	podmínka	k1gFnPc4	podmínka
obyvatel	obyvatel	k1gMnPc2	obyvatel
–	–	k?	–
vysoká	vysoký	k2eAgFnSc1d1	vysoká
novorozenecká	novorozenecký	k2eAgFnSc1d1	novorozenecká
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
<g/>
,	,	kIx,	,
bídné	bídný	k2eAgInPc4d1	bídný
platy	plat	k1gInPc4	plat
<g/>
,	,	kIx,	,
nedostatek	nedostatek	k1gInSc4	nedostatek
potravin	potravina	k1gFnPc2	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Biskupové	biskup	k1gMnPc1	biskup
potřebovali	potřebovat	k5eAaImAgMnP	potřebovat
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
situaci	situace	k1gFnSc4	situace
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
a	a	k8xC	a
kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
odtrženost	odtrženost	k1gFnSc4	odtrženost
církve	církev	k1gFnSc2	církev
od	od	k7c2	od
reality	realita	k1gFnSc2	realita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hlavní	hlavní	k2eAgMnPc1d1	hlavní
zakladatelé	zakladatel	k1gMnPc1	zakladatel
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Gustavo	Gustava	k1gFnSc5	Gustava
Gutiérrez	Gutiérrez	k1gMnSc1	Gutiérrez
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
Lima	Lima	k1gFnSc1	Lima
<g/>
,	,	kIx,	,
Peru	Peru	k1gNnSc1	Peru
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
Gustavo	Gustava	k1gFnSc5	Gustava
Gutiérrez	Gutiérrez	k1gInSc1	Gutiérrez
bývá	bývat	k5eAaImIp3nS	bývat
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
otce	otec	k1gMnPc4	otec
teologie	teologie	k1gFnSc2	teologie
osvobození	osvobození	k1gNnSc2	osvobození
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
jedné	jeden	k4xCgFnSc2	jeden
ze	z	k7c2	z
klíčových	klíčový	k2eAgFnPc2d1	klíčová
knih	kniha	k1gFnPc2	kniha
hnutí	hnutí	k1gNnSc2	hnutí
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Teologie	teologie	k1gFnSc1	teologie
osvobození	osvobození	k1gNnSc2	osvobození
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
jeho	jeho	k3xOp3gFnPc4	jeho
předchozí	předchozí	k2eAgFnPc4d1	předchozí
myšlenky	myšlenka	k1gFnPc4	myšlenka
a	a	k8xC	a
vystoupení	vystoupení	k1gNnSc4	vystoupení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Leonardo	Leonardo	k1gMnSc1	Leonardo
Boff	Boff	k1gMnSc1	Boff
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
Concordia	Concordium	k1gNnSc2	Concordium
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
Leonardo	Leonardo	k1gMnSc1	Leonardo
Boff	Boff	k1gMnSc1	Boff
studoval	studovat	k5eAaImAgMnS	studovat
teologii	teologie	k1gFnSc4	teologie
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
i	i	k8xC	i
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
latinskoamerickým	latinskoamerický	k2eAgMnSc7d1	latinskoamerický
teologem	teolog	k1gMnSc7	teolog
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zpracoval	zpracovat	k5eAaPmAgMnS	zpracovat
christologii	christologie	k1gFnSc4	christologie
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
sociální	sociální	k2eAgFnSc2d1	sociální
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgMnS	vzdát
svého	svůj	k3xOyFgInSc2	svůj
kněžského	kněžský	k2eAgInSc2d1	kněžský
statusu	status	k1gInSc2	status
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Helder	Helder	k1gMnSc1	Helder
Camara	Camar	k1gMnSc2	Camar
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
–	–	k?	–
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
Helder	Helder	k1gMnSc1	Helder
Camara	Camar	k1gMnSc4	Camar
byl	být	k5eAaImAgMnS	být
politicky	politicky	k6eAd1	politicky
aktivním	aktivní	k2eAgMnSc7d1	aktivní
knězem	kněz	k1gMnSc7	kněz
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
pronásledován	pronásledovat	k5eAaImNgInS	pronásledovat
vojenským	vojenský	k2eAgInSc7d1	vojenský
režimem	režim	k1gInSc7	režim
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1964-1985	[number]	k4	1964-1985
byl	být	k5eAaImAgInS	být
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Enrique	Enrique	k1gNnPc1	Enrique
Dussel	Dussela	k1gFnPc2	Dussela
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
Mendoza	Mendoza	k1gFnSc1	Mendoza
<g/>
,	,	kIx,	,
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
teologii	teologie	k1gFnSc4	teologie
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
v	v	k7c4	v
Mexico	Mexico	k1gNnSc4	Mexico
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
profesorem	profesor	k1gMnSc7	profesor
etiky	etika	k1gFnSc2	etika
<g/>
,	,	kIx,	,
historie	historie	k1gFnSc2	historie
a	a	k8xC	a
teologie	teologie	k1gFnSc2	teologie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
Filozofie	filozofie	k1gFnSc2	filozofie
osvobození	osvobození	k1gNnSc2	osvobození
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
sktruktury	sktruktura	k1gFnPc1	sktruktura
kolonialismu	kolonialismus	k1gInSc2	kolonialismus
<g/>
,	,	kIx,	,
globalizace	globalizace	k1gFnSc2	globalizace
a	a	k8xC	a
rasismu	rasismus	k1gInSc2	rasismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Teologie	teologie	k1gFnSc2	teologie
==	==	k?	==
</s>
</p>
<p>
<s>
Metoda	metoda	k1gFnSc1	metoda
teologie	teologie	k1gFnSc2	teologie
osvobození	osvobození	k1gNnSc2	osvobození
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
stručně	stručně	k6eAd1	stručně
shrnout	shrnout	k5eAaPmF	shrnout
do	do	k7c2	do
tří	tři	k4xCgNnPc2	tři
slov	slovo	k1gNnPc2	slovo
"	"	kIx"	"
<g/>
vidět	vidět	k5eAaImF	vidět
–	–	k?	–
vyhodnotit	vyhodnotit	k5eAaPmF	vyhodnotit
–	–	k?	–
jednat	jednat	k5eAaImF	jednat
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
see	see	k?	see
–	–	k?	–
judge	judge	k1gInSc1	judge
–	–	k?	–
act	act	k?	act
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Křesťané	křesťan	k1gMnPc1	křesťan
si	se	k3xPyFc3	se
nejprve	nejprve	k6eAd1	nejprve
všímají	všímat	k5eAaImIp3nP	všímat
chudoby	chudoba	k1gFnPc1	chudoba
kolem	kolem	k7c2	kolem
sebe	se	k3xPyFc2	se
<g/>
,	,	kIx,	,
popisují	popisovat	k5eAaImIp3nP	popisovat
situaci	situace	k1gFnSc4	situace
a	a	k8xC	a
ptají	ptat	k5eAaImIp3nP	ptat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
současné	současný	k2eAgFnSc3d1	současná
situaci	situace	k1gFnSc3	situace
–	–	k?	–
nespravedlnost	nespravedlnost	k1gFnSc1	nespravedlnost
(	(	kIx(	(
<g/>
vidět	vidět	k5eAaImF	vidět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
obracejí	obracet	k5eAaImIp3nP	obracet
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
víře	víra	k1gFnSc3	víra
<g/>
,	,	kIx,	,
k	k	k7c3	k
Bibli	bible	k1gFnSc3	bible
<g/>
.	.	kIx.	.
</s>
<s>
Boží	boží	k2eAgNnSc1d1	boží
slovo	slovo	k1gNnSc1	slovo
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
aplikováno	aplikován	k2eAgNnSc1d1	aplikováno
na	na	k7c4	na
současnou	současný	k2eAgFnSc4d1	současná
situaci	situace	k1gFnSc4	situace
a	a	k8xC	a
společenství	společenství	k1gNnSc4	společenství
hledá	hledat	k5eAaImIp3nS	hledat
cesty	cesta	k1gFnPc4	cesta
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
Písmo	písmo	k1gNnSc1	písmo
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
k	k	k7c3	k
realitě	realita	k1gFnSc3	realita
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
kontextualizace	kontextualizace	k1gFnSc2	kontextualizace
provádělo	provádět	k5eAaImAgNnS	provádět
společenství	společenství	k1gNnSc1	společenství
jako	jako	k8xS	jako
celek	celek	k1gInSc1	celek
<g/>
,	,	kIx,	,
ne	ne	k9	ne
teolog	teolog	k1gMnSc1	teolog
nebo	nebo	k8xC	nebo
kněz	kněz	k1gMnSc1	kněz
(	(	kIx(	(
<g/>
vyhodnotit	vyhodnotit	k5eAaPmF	vyhodnotit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
přistoupit	přistoupit	k5eAaPmF	přistoupit
k	k	k7c3	k
činu	čin	k1gInSc3	čin
na	na	k7c6	na
základě	základ	k1gInSc6	základ
porozumění	porozumění	k1gNnSc4	porozumění
Bibli	bible	k1gFnSc4	bible
(	(	kIx(	(
<g/>
jednat	jednat	k5eAaImF	jednat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
metodu	metoda	k1gFnSc4	metoda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nezačíná	začínat	k5eNaImIp3nS	začínat
exegezí	exegeze	k1gFnSc7	exegeze
biblického	biblický	k2eAgInSc2d1	biblický
textu	text	k1gInSc2	text
a	a	k8xC	a
následnou	následný	k2eAgFnSc7d1	následná
aplikací	aplikace	k1gFnSc7	aplikace
na	na	k7c4	na
realitu	realita	k1gFnSc4	realita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prvním	první	k4xOgInSc7	první
krokem	krok	k1gInSc7	krok
jsou	být	k5eAaImIp3nP	být
fakta	faktum	k1gNnPc1	faktum
a	a	k8xC	a
popis	popis	k1gInSc1	popis
skutečnosti	skutečnost	k1gFnSc3	skutečnost
<g/>
.	.	kIx.	.
<g/>
Teologická	teologický	k2eAgNnPc1d1	teologické
východiska	východisko	k1gNnPc1	východisko
jsou	být	k5eAaImIp3nP	být
založena	založen	k2eAgNnPc1d1	založeno
na	na	k7c6	na
přístupu	přístup	k1gInSc6	přístup
k	k	k7c3	k
biblickému	biblický	k2eAgNnSc3d1	biblické
studiu	studio	k1gNnSc3	studio
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
utlačovaných	utlačovaný	k1gMnPc2	utlačovaný
<g/>
.	.	kIx.	.
</s>
<s>
Tváří	tvářet	k5eAaImIp3nS	tvářet
v	v	k7c4	v
tvář	tvář	k1gFnSc4	tvář
dlouhému	dlouhý	k2eAgInSc3d1	dlouhý
boji	boj	k1gInSc3	boj
proti	proti	k7c3	proti
sociální	sociální	k2eAgFnSc3d1	sociální
nespravedlnosti	nespravedlnost	k1gFnSc3	nespravedlnost
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
pozornosti	pozornost	k1gFnSc2	pozornost
Božímu	boží	k2eAgNnSc3d1	boží
království	království	k1gNnSc3	království
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
zákoně	zákon	k1gInSc6	zákon
s	s	k7c7	s
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
práci	práce	k1gFnSc4	práce
Krista	Kristus	k1gMnSc2	Kristus
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
identifikaci	identifikace	k1gFnSc4	identifikace
s	s	k7c7	s
chudými	chudý	k2eAgMnPc7d1	chudý
a	a	k8xC	a
utlačovanými	utlačovaný	k2eAgMnPc7d1	utlačovaný
<g/>
.	.	kIx.	.
</s>
<s>
Nemůžeme	moct	k5eNaImIp1nP	moct
to	ten	k3xDgNnSc1	ten
však	však	k9	však
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
jediný	jediný	k2eAgInSc4d1	jediný
důraz	důraz	k1gInSc4	důraz
<g/>
.	.	kIx.	.
</s>
<s>
Teologové	teolog	k1gMnPc1	teolog
teologie	teologie	k1gFnSc2	teologie
osvobození	osvobození	k1gNnSc2	osvobození
zpracovali	zpracovat	k5eAaPmAgMnP	zpracovat
také	také	k9	také
christologii	christologie	k1gFnSc4	christologie
(	(	kIx(	(
<g/>
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
historického	historický	k2eAgMnSc4d1	historický
Ježíše	Ježíš	k1gMnSc4	Ježíš
<g/>
)	)	kIx)	)
a	a	k8xC	a
eklesiologii	eklesiologie	k1gFnSc4	eklesiologie
(	(	kIx(	(
<g/>
především	především	k9	především
kritika	kritika	k1gFnSc1	kritika
klerikalismu	klerikalismus	k1gInSc2	klerikalismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Teologie	teologie	k1gFnSc1	teologie
osvobození	osvobození	k1gNnSc2	osvobození
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgNnPc4	tři
hlavní	hlavní	k2eAgNnPc4d1	hlavní
témata	téma	k1gNnPc4	téma
<g/>
,	,	kIx,	,
kterým	který	k3yQgNnSc7	který
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
–	–	k?	–
nová	nový	k2eAgFnSc1d1	nová
definice	definice	k1gFnSc1	definice
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
praxe	praxe	k1gFnSc2	praxe
<g/>
,	,	kIx,	,
nové	nový	k2eAgNnSc1d1	nové
pojetí	pojetí	k1gNnSc1	pojetí
dějin	dějiny	k1gFnPc2	dějiny
a	a	k8xC	a
nové	nový	k2eAgNnSc1d1	nové
chápání	chápání	k1gNnSc1	chápání
Písma	písmo	k1gNnSc2	písmo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nová	nový	k2eAgFnSc1d1	nová
definice	definice	k1gFnSc1	definice
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
praxe	praxe	k1gFnSc2	praxe
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
chudý	chudý	k2eAgMnSc1d1	chudý
<g/>
"	"	kIx"	"
není	být	k5eNaImIp3nS	být
jen	jen	k9	jen
pojmem	pojem	k1gInSc7	pojem
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
označuje	označovat	k5eAaImIp3nS	označovat
člověka	člověk	k1gMnSc4	člověk
bez	bez	k7c2	bez
majetku	majetek	k1gInSc2	majetek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
existenci	existence	k1gFnSc4	existence
společenských	společenský	k2eAgFnPc2d1	společenská
tříd	třída	k1gFnPc2	třída
a	a	k8xC	a
bojů	boj	k1gInPc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Úkolem	úkol	k1gInSc7	úkol
křesťanů	křesťan	k1gMnPc2	křesťan
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
být	být	k5eAaImF	být
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
chudých	chudý	k2eAgInPc2d1	chudý
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
stát	stát	k5eAaImF	stát
účastníkem	účastník	k1gMnSc7	účastník
třídního	třídní	k2eAgInSc2d1	třídní
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
tak	tak	k6eAd1	tak
nejsou	být	k5eNaImIp3nP	být
jen	jen	k9	jen
činy	čin	k1gInPc1	čin
milosrdenství	milosrdenství	k1gNnPc2	milosrdenství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vytvoření	vytvoření	k1gNnSc1	vytvoření
nového	nový	k2eAgInSc2d1	nový
společenského	společenský	k2eAgInSc2d1	společenský
pořádku	pořádek	k1gInSc2	pořádek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
porozumění	porozumění	k1gNnSc1	porozumění
dějinám	dějiny	k1gFnPc3	dějiny
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
třídní	třídní	k2eAgNnSc4d1	třídní
rozdělení	rozdělení	k1gNnSc4	rozdělení
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
pohledu	pohled	k1gInSc2	pohled
na	na	k7c4	na
církev	církev	k1gFnSc4	církev
jako	jako	k8xS	jako
součást	součást	k1gFnSc4	součást
tohoto	tento	k3xDgNnSc2	tento
rozdělení	rozdělení	k1gNnSc2	rozdělení
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
tak	tak	k9	tak
k	k	k7c3	k
radikálně	radikálně	k6eAd1	radikálně
jinému	jiný	k2eAgInSc3d1	jiný
pohledu	pohled	k1gInSc3	pohled
na	na	k7c4	na
misijní	misijní	k2eAgNnSc4d1	misijní
úsilí	úsilí	k1gNnSc4	úsilí
evropských	evropský	k2eAgFnPc2d1	Evropská
církví	církev	k1gFnPc2	církev
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
církve	církev	k1gFnPc1	církev
vnímány	vnímán	k2eAgFnPc1d1	vnímána
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
evropské	evropský	k2eAgFnSc2d1	Evropská
imperiální	imperiální	k2eAgFnSc2d1	imperiální
expanze	expanze	k1gFnSc2	expanze
<g/>
.	.	kIx.	.
</s>
<s>
Míguez	Míguez	k1gInSc1	Míguez
Bonino	bonin	k2eAgNnSc1d1	Bonino
toto	tento	k3xDgNnSc4	tento
napětí	napětí	k1gNnSc4	napětí
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
krizí	krize	k1gFnSc7	krize
svědomí	svědomí	k1gNnSc4	svědomí
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Nové	Nové	k2eAgNnSc1d1	Nové
chápání	chápání	k1gNnSc1	chápání
Písma	písmo	k1gNnSc2	písmo
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
porozumění	porozumění	k1gNnSc4	porozumění
Ježíšově	Ježíšův	k2eAgFnSc3d1	Ježíšova
osobě	osoba	k1gFnSc3	osoba
na	na	k7c6	na
základě	základ	k1gInSc6	základ
evangelií	evangelium	k1gNnPc2	evangelium
–	–	k?	–
úmyslně	úmyslně	k6eAd1	úmyslně
vynechává	vynechávat	k5eAaImIp3nS	vynechávat
katolickou	katolický	k2eAgFnSc4d1	katolická
tradici	tradice	k1gFnSc4	tradice
a	a	k8xC	a
dogmata	dogma	k1gNnPc4	dogma
<g/>
.	.	kIx.	.
</s>
<s>
Klade	klást	k5eAaImIp3nS	klást
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
historického	historický	k2eAgMnSc4d1	historický
Ježiše	Ježiš	k1gMnSc4	Ježiš
a	a	k8xC	a
na	na	k7c4	na
politický	politický	k2eAgInSc4d1	politický
a	a	k8xC	a
společenský	společenský	k2eAgInSc4d1	společenský
význam	význam	k1gInSc4	význam
jeho	jeho	k3xOp3gFnSc2	jeho
oběti	oběť	k1gFnSc2	oběť
jako	jako	k8xS	jako
důsledku	důsledek	k1gInSc2	důsledek
jeho	jeho	k3xOp3gInSc2	jeho
způsobu	způsob	k1gInSc2	způsob
života	život	k1gInSc2	život
a	a	k8xC	a
utrpení	utrpení	k1gNnSc2	utrpení
pro	pro	k7c4	pro
spravedlivou	spravedlivý	k2eAgFnSc4d1	spravedlivá
věc	věc	k1gFnSc4	věc
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
důrazem	důraz	k1gInSc7	důraz
nového	nový	k2eAgNnSc2d1	nové
chápání	chápání	k1gNnSc2	chápání
Písma	písmo	k1gNnSc2	písmo
je	být	k5eAaImIp3nS	být
eklesiologie	eklesiologie	k1gFnPc4	eklesiologie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
především	především	k9	především
Leonardo	Leonardo	k1gMnSc1	Leonardo
Boff	Boff	k1gMnSc1	Boff
<g/>
.	.	kIx.	.
</s>
<s>
Kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
hierarchické	hierarchický	k2eAgNnSc1d1	hierarchické
uspořádání	uspořádání	k1gNnSc1	uspořádání
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
klerikalismus	klerikalismus	k1gInSc4	klerikalismus
a	a	k8xC	a
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
se	se	k3xPyFc4	se
na	na	k7c4	na
malé	malý	k2eAgFnPc4d1	malá
<g/>
,	,	kIx,	,
zdola	zdola	k6eAd1	zdola
organizované	organizovaný	k2eAgFnSc2d1	organizovaná
komunity	komunita	k1gFnSc2	komunita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mohou	moct	k5eAaImIp3nP	moct
věřící	věřící	k2eAgMnPc1d1	věřící
aplikovat	aplikovat	k5eAaBmF	aplikovat
výše	vysoce	k6eAd2	vysoce
zmíněné	zmíněný	k2eAgInPc1d1	zmíněný
teologické	teologický	k2eAgInPc1d1	teologický
postupy	postup	k1gInPc1	postup
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
životech	život	k1gInPc6	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politické	politický	k2eAgNnSc1d1	politické
propojení	propojení	k1gNnSc1	propojení
==	==	k?	==
</s>
</p>
<p>
<s>
Teologie	teologie	k1gFnSc1	teologie
osvobození	osvobození	k1gNnSc2	osvobození
je	být	k5eAaImIp3nS	být
úzce	úzko	k6eAd1	úzko
propojena	propojit	k5eAaPmNgFnS	propojit
s	s	k7c7	s
politickým	politický	k2eAgInSc7d1	politický
vývojem	vývoj	k1gInSc7	vývoj
v	v	k7c6	v
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Teologové	teolog	k1gMnPc1	teolog
tohoto	tento	k3xDgNnSc2	tento
hnutí	hnutí	k1gNnSc2	hnutí
se	se	k3xPyFc4	se
ztotožňovali	ztotožňovat	k5eAaImAgMnP	ztotožňovat
s	s	k7c7	s
bojem	boj	k1gInSc7	boj
za	za	k7c4	za
politické	politický	k2eAgNnSc4d1	politické
a	a	k8xC	a
sociální	sociální	k2eAgNnSc4d1	sociální
osvobození	osvobození	k1gNnSc4	osvobození
podle	podle	k7c2	podle
marxistické	marxistický	k2eAgFnSc2d1	marxistická
analýzy	analýza	k1gFnSc2	analýza
dějin	dějiny	k1gFnPc2	dějiny
a	a	k8xC	a
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
třídním	třídní	k2eAgInSc7d1	třídní
bojem	boj	k1gInSc7	boj
<g/>
.	.	kIx.	.
</s>
<s>
Gutiérrez	Gutiérrez	k1gMnSc1	Gutiérrez
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
kritizují	kritizovat	k5eAaImIp3nP	kritizovat
kapitalismus	kapitalismus	k1gInSc4	kapitalismus
a	a	k8xC	a
jako	jako	k9	jako
cíl	cíl	k1gInSc4	cíl
teologie	teologie	k1gFnSc2	teologie
osvobození	osvobození	k1gNnSc2	osvobození
označovali	označovat	k5eAaImAgMnP	označovat
nejen	nejen	k6eAd1	nejen
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
lepší	lepšit	k5eAaImIp3nS	lepšit
životní	životní	k2eAgFnPc4d1	životní
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nový	nový	k2eAgInSc1d1	nový
způsob	způsob	k1gInSc1	způsob
lidství	lidství	k1gNnSc2	lidství
a	a	k8xC	a
permanentní	permanentní	k2eAgFnSc4d1	permanentní
kulturní	kulturní	k2eAgFnSc4d1	kulturní
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reakce	reakce	k1gFnPc4	reakce
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
==	==	k?	==
</s>
</p>
<p>
<s>
Předmětem	předmět	k1gInSc7	předmět
největší	veliký	k2eAgFnSc2d3	veliký
kritiky	kritika	k1gFnSc2	kritika
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
bylo	být	k5eAaImAgNnS	být
především	především	k9	především
propojení	propojení	k1gNnSc1	propojení
teologie	teologie	k1gFnSc2	teologie
osvobození	osvobození	k1gNnSc2	osvobození
a	a	k8xC	a
marxismu	marxismus	k1gInSc2	marxismus
<g/>
.	.	kIx.	.
</s>
<s>
Kardinál	kardinál	k1gMnSc1	kardinál
Joseph	Joseph	k1gMnSc1	Joseph
Ratzinger	Ratzinger	k1gMnSc1	Ratzinger
sepsal	sepsat	k5eAaPmAgMnS	sepsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
Instrukci	instrukce	k1gFnSc4	instrukce
Kongregace	kongregace	k1gFnSc2	kongregace
pro	pro	k7c4	pro
nauku	nauka	k1gFnSc4	nauka
víry	víra	k1gFnSc2	víra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
toto	tento	k3xDgNnSc4	tento
oficiálně	oficiálně	k6eAd1	oficiálně
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
<g/>
.	.	kIx.	.
</s>
<s>
Gutiérrez	Gutiérrez	k1gInSc1	Gutiérrez
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
reagoval	reagovat	k5eAaBmAgInS	reagovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
distancoval	distancovat	k5eAaBmAgMnS	distancovat
od	od	k7c2	od
marxistické	marxistický	k2eAgFnSc2d1	marxistická
antropologie	antropologie	k1gFnSc2	antropologie
a	a	k8xC	a
ateismu	ateismus	k1gInSc2	ateismus
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
vydala	vydat	k5eAaPmAgFnS	vydat
Kongregace	kongregace	k1gFnSc1	kongregace
pro	pro	k7c4	pro
nauku	nauka	k1gFnSc4	nauka
víry	víra	k1gFnSc2	víra
novou	nový	k2eAgFnSc4d1	nová
Instrukci	instrukce	k1gFnSc4	instrukce
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
svobody	svoboda	k1gFnSc2	svoboda
a	a	k8xC	a
osvobození	osvobození	k1gNnSc2	osvobození
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
uznala	uznat	k5eAaPmAgFnS	uznat
několik	několik	k4yIc4	několik
forem	forma	k1gFnPc2	forma
teologie	teologie	k1gFnSc2	teologie
osvobození	osvobození	k1gNnSc2	osvobození
s	s	k7c7	s
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
soucit	soucit	k1gInSc4	soucit
s	s	k7c7	s
chudými	chudý	k1gMnPc7	chudý
<g/>
.	.	kIx.	.
<g/>
Postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
teologie	teologie	k1gFnSc2	teologie
osvobození	osvobození	k1gNnSc2	osvobození
existují	existovat	k5eAaImIp3nP	existovat
různé	různý	k2eAgInPc1d1	různý
směry	směr	k1gInPc1	směr
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nejsou	být	k5eNaImIp3nP	být
zcela	zcela	k6eAd1	zcela
jednotné	jednotný	k2eAgMnPc4d1	jednotný
především	především	k6eAd1	především
v	v	k7c6	v
pohledu	pohled	k1gInSc6	pohled
na	na	k7c4	na
roli	role	k1gFnSc4	role
marxismu	marxismus	k1gInSc2	marxismus
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
hnutí	hnutí	k1gNnSc6	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Samuel	Samuel	k1gMnSc1	Samuel
Escobar	Escobar	k1gMnSc1	Escobar
rozpoznává	rozpoznávat	k5eAaImIp3nS	rozpoznávat
tři	tři	k4xCgInPc4	tři
různé	různý	k2eAgInPc4d1	různý
směry	směr	k1gInPc4	směr
teologie	teologie	k1gFnSc2	teologie
osvobození	osvobození	k1gNnSc2	osvobození
–	–	k?	–
pastorační	pastorační	k2eAgInSc4d1	pastorační
směr	směr	k1gInSc4	směr
Gutiérreze	Gutiérreze	k1gFnSc2	Gutiérreze
<g/>
,	,	kIx,	,
akademické	akademický	k2eAgInPc4d1	akademický
důrazy	důraz	k1gInPc4	důraz
Assmanna	Assmann	k1gInSc2	Assmann
a	a	k8xC	a
Segunda	Segunda	k1gFnSc1	Segunda
a	a	k8xC	a
populistický	populistický	k2eAgInSc1d1	populistický
styl	styl	k1gInSc1	styl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
využívá	využívat	k5eAaImIp3nS	využívat
rétoriku	rétorika	k1gFnSc4	rétorika
teologie	teologie	k1gFnSc2	teologie
osvobození	osvobození	k1gNnSc2	osvobození
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
konzervativně	konzervativně	k6eAd1	konzervativně
katolický	katolický	k2eAgMnSc1d1	katolický
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Teologie	teologie	k1gFnSc1	teologie
osvobození	osvobození	k1gNnSc2	osvobození
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
==	==	k?	==
</s>
</p>
<p>
<s>
Různé	různý	k2eAgFnPc1d1	různá
obměny	obměna	k1gFnPc1	obměna
a	a	k8xC	a
varianty	varianta	k1gFnPc1	varianta
teologie	teologie	k1gFnSc2	teologie
osvobození	osvobození	k1gNnSc2	osvobození
našly	najít	k5eAaPmAgFnP	najít
své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nezávislé	závislý	k2eNgFnPc4d1	nezávislá
větve	větev	k1gFnPc4	větev
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
neměly	mít	k5eNaImAgFnP	mít
s	s	k7c7	s
latinskoamerickým	latinskoamerický	k2eAgNnSc7d1	latinskoamerické
hnutím	hnutí	k1gNnSc7	hnutí
zpočátku	zpočátku	k6eAd1	zpočátku
žádný	žádný	k3yNgInSc4	žádný
kontakt	kontakt	k1gInSc4	kontakt
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
zařadit	zařadit	k5eAaPmF	zařadit
Černou	černý	k2eAgFnSc4d1	černá
(	(	kIx(	(
<g/>
černošskou	černošský	k2eAgFnSc4d1	černošská
<g/>
)	)	kIx)	)
teologii	teologie	k1gFnSc4	teologie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
především	především	k6eAd1	především
v	v	k7c4	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jako	jako	k8xS	jako
reakce	reakce	k1gFnSc2	reakce
na	na	k7c4	na
rasový	rasový	k2eAgInSc4d1	rasový
útlak	útlak	k1gInSc4	útlak
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
však	však	k9	však
z	z	k7c2	z
protestantských	protestantský	k2eAgInPc2d1	protestantský
kořenů	kořen	k1gInPc2	kořen
a	a	k8xC	a
například	například	k6eAd1	například
k	k	k7c3	k
marxismu	marxismus	k1gInSc3	marxismus
nikdy	nikdy	k6eAd1	nikdy
neměla	mít	k5eNaImAgFnS	mít
blízko	blízko	k6eAd1	blízko
<g/>
.	.	kIx.	.
<g/>
Další	další	k2eAgFnSc7d1	další
odnoží	odnož	k1gFnSc7	odnož
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
Minjung	Minjung	k1gInSc1	Minjung
teologie	teologie	k1gFnSc1	teologie
(	(	kIx(	(
<g/>
minjung	minjung	k1gInSc1	minjung
<g/>
=	=	kIx~	=
<g/>
lidé	člověk	k1gMnPc1	člověk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
Teologii	teologie	k1gFnSc4	teologie
osvobození	osvobození	k1gNnSc2	osvobození
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jako	jako	k8xS	jako
reakce	reakce	k1gFnSc2	reakce
na	na	k7c4	na
politický	politický	k2eAgInSc4d1	politický
a	a	k8xC	a
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
útlak	útlak	k1gInSc4	útlak
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Ekumenické	ekumenický	k2eAgFnSc2d1	ekumenická
asociace	asociace	k1gFnSc2	asociace
teologů	teolog	k1gMnPc2	teolog
třetího	třetí	k4xOgInSc2	třetí
světa	svět	k1gInSc2	svět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
těchto	tento	k3xDgNnPc2	tento
hnutí	hnutí	k1gNnPc2	hnutí
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nešlo	jít	k5eNaImAgNnS	jít
o	o	k7c4	o
přímé	přímý	k2eAgNnSc4d1	přímé
rozšíření	rozšíření	k1gNnSc4	rozšíření
hnutí	hnutí	k1gNnSc2	hnutí
z	z	k7c2	z
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
inspiraci	inspirace	k1gFnSc4	inspirace
a	a	k8xC	a
posun	posun	k1gInSc4	posun
do	do	k7c2	do
místních	místní	k2eAgInPc2d1	místní
kontextů	kontext	k1gInPc2	kontext
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
je	být	k5eAaImIp3nS	být
významná	významný	k2eAgFnSc1d1	významná
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
černá	černý	k2eAgFnSc1d1	černá
teologie	teologie	k1gFnSc1	teologie
(	(	kIx(	(
<g/>
boj	boj	k1gInSc1	boj
proti	proti	k7c3	proti
rasismu	rasismus	k1gInSc3	rasismus
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Africká	africký	k2eAgFnSc1d1	africká
teologie	teologie	k1gFnSc1	teologie
osvobození	osvobození	k1gNnSc2	osvobození
nebo	nebo	k8xC	nebo
Africká	africký	k2eAgFnSc1d1	africká
ženská	ženský	k2eAgFnSc1d1	ženská
teologie	teologie	k1gFnSc1	teologie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
působí	působit	k5eAaImIp3nS	působit
kromě	kromě	k7c2	kromě
Minjung	Minjunga	k1gFnPc2	Minjunga
také	také	k9	také
filipínská	filipínský	k2eAgFnSc1d1	filipínská
teologie	teologie	k1gFnSc1	teologie
útlaku	útlak	k1gInSc2	útlak
nebo	nebo	k8xC	nebo
indická	indický	k2eAgFnSc1d1	indická
dalitská	dalitský	k2eAgFnSc1d1	dalitský
teologie	teologie	k1gFnSc1	teologie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
bojuje	bojovat	k5eAaImIp3nS	bojovat
proti	proti	k7c3	proti
kastovnímu	kastovní	k2eAgInSc3d1	kastovní
systému	systém	k1gInSc3	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
The	The	k?	The
Brill	Brill	k1gMnSc1	Brill
dictionary	dictionara	k1gFnSc2	dictionara
of	of	k?	of
religion	religion	k1gInSc1	religion
:	:	kIx,	:
revised	revised	k1gInSc1	revised
edition	edition	k1gInSc1	edition
of	of	k?	of
Metzler	Metzler	k1gInSc1	Metzler
Lexikon	lexikon	k1gNnSc1	lexikon
Religion	religion	k1gInSc1	religion
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
ed	ed	k?	ed
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Boston	Boston	k1gInSc1	Boston
<g/>
:	:	kIx,	:
Brill	Brill	k1gInSc1	Brill
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
4	[number]	k4	4
volumes	volumesa	k1gFnPc2	volumesa
(	(	kIx(	(
<g/>
I	I	kA	I
<g/>
,	,	kIx,	,
2100	[number]	k4	2100
<g/>
)	)	kIx)	)
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
9789004124332	[number]	k4	9789004124332
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
THIA	THIA	kA	THIA
<g/>
,	,	kIx,	,
Cooper	Cooper	k1gMnSc1	Cooper
<g/>
.	.	kIx.	.
</s>
<s>
Controversies	Controversies	k1gMnSc1	Controversies
in	in	k?	in
political	politicat	k5eAaPmAgMnS	politicat
theology	theolog	k1gMnPc4	theolog
:	:	kIx,	:
development	development	k1gInSc1	development
or	or	k?	or
liberation	liberation	k1gInSc1	liberation
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
</s>
<s>
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
SCM	SCM	kA	SCM
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
208	[number]	k4	208
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
9780334041122	[number]	k4	9780334041122
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
New	New	k?	New
dictionary	dictionar	k1gInPc1	dictionar
of	of	k?	of
theology	theolog	k1gMnPc7	theolog
<g/>
.	.	kIx.	.
</s>
<s>
Leicester	Leicester	k1gInSc1	Leicester
<g/>
:	:	kIx,	:
Intervarsity	Intervarsit	k1gInPc1	Intervarsit
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
738	[number]	k4	738
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
9780830814008	[number]	k4	9780830814008
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MCGRATH	MCGRATH	kA	MCGRATH
<g/>
,	,	kIx,	,
Alister	Alister	k1gInSc1	Alister
E.	E.	kA	E.
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
Blackwellova	Blackwellův	k2eAgFnSc1d1	Blackwellova
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
moderního	moderní	k2eAgNnSc2d1	moderní
křesťanského	křesťanský	k2eAgNnSc2d1	křesťanské
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Jan	Jan	k1gMnSc1	Jan
Dus	dus	k1gInSc1	dus
<g/>
,	,	kIx,	,
Lubomír	Lubomír	k1gMnSc1	Lubomír
Miřejovský	Miřejovský	k1gMnSc1	Miřejovský
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
Plzák	Plzák	k1gMnSc1	Plzák
<g/>
,	,	kIx,	,
Marek	Marek	k1gMnSc1	Marek
Váňa	Váňa	k1gMnSc1	Váňa
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Návrat	návrat	k1gInSc1	návrat
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
593	[number]	k4	593
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7255	[number]	k4	7255
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
43	[number]	k4	43
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PETRELLA	PETRELLA	kA	PETRELLA
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Future	Futur	k1gMnSc5	Futur
of	of	k?	of
Liberation	Liberation	k1gInSc1	Liberation
Theology	theolog	k1gMnPc4	theolog
:	:	kIx,	:
An	An	k1gFnSc1	An
Argument	argument	k1gInSc1	argument
and	and	k?	and
Manifesto	Manifesta	k1gMnSc5	Manifesta
<g/>
.	.	kIx.	.
</s>
<s>
Milton	Milton	k1gInSc1	Milton
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
Abingdon	Abingdon	k1gNnSc1	Abingdon
<g/>
,	,	kIx,	,
Oxon	Oxon	k1gNnSc1	Oxon
<g/>
:	:	kIx,	:
Routledge	Routledge	k1gNnSc1	Routledge
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
9781351889131	[number]	k4	9781351889131
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
177	[number]	k4	177
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GUTIÉRREZ	GUTIÉRREZ	kA	GUTIÉRREZ
<g/>
,	,	kIx,	,
Gustavo	Gustava	k1gFnSc5	Gustava
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k8xC	a
Theology	theolog	k1gMnPc4	theolog
of	of	k?	of
Liberation	Liberation	k1gInSc1	Liberation
<g/>
:	:	kIx,	:
History	Histor	k1gInPc1	Histor
<g/>
,	,	kIx,	,
Politics	Politics	k1gInSc1	Politics
<g/>
,	,	kIx,	,
and	and	k?	and
Salvation	Salvation	k1gInSc1	Salvation
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Orbis	orbis	k1gInSc1	orbis
Books	Booksa	k1gFnPc2	Booksa
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
264	[number]	k4	264
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
9780883445426	[number]	k4	9780883445426
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KLAIBER	KLAIBER	kA	KLAIBER
<g/>
,	,	kIx,	,
Jeffrey	Jeffrea	k1gMnSc2	Jeffrea
L.	L.	kA	L.
Prophets	Prophets	k1gInSc1	Prophets
and	and	k?	and
Populists	Populists	k1gInSc1	Populists
<g/>
:	:	kIx,	:
Liberation	Liberation	k1gInSc1	Liberation
Theology	theolog	k1gMnPc7	theolog
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
-	-	kIx~	-
<g/>
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Americas	Americas	k1gInSc1	Americas
<g/>
.	.	kIx.	.
</s>
<s>
Jul	Jula	k1gFnPc2	Jula
<g/>
.	.	kIx.	.
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
46	[number]	k4	46
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
,	,	kIx,	,
s.	s.	k?	s.
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Encyclopedia	Encyclopedium	k1gNnSc2	Encyclopedium
of	of	k?	of
religion	religion	k1gInSc1	religion
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
nd	nd	k?	nd
ed	ed	k?	ed
<g/>
..	..	k?	..
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Detroit	Detroit	k1gInSc1	Detroit
<g/>
:	:	kIx,	:
Macmillan	Macmillan	k1gInSc1	Macmillan
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
865733	[number]	k4	865733
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Gustavo	Gustava	k1gFnSc5	Gustava
Gutiérrez	Gutiérreza	k1gFnPc2	Gutiérreza
</s>
</p>
<p>
<s>
Leonardo	Leonardo	k1gMnSc1	Leonardo
Boff	Boff	k1gMnSc1	Boff
</s>
</p>
<p>
<s>
Hélder	Hélder	k1gInSc1	Hélder
Câmara	Câmar	k1gMnSc2	Câmar
</s>
</p>
<p>
<s>
Latinská	latinský	k2eAgFnSc1d1	Latinská
Amerika	Amerika	k1gFnSc1	Amerika
</s>
</p>
<p>
<s>
Marxismus	marxismus	k1gInSc1	marxismus
</s>
</p>
<p>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
církev	církev	k1gFnSc1	církev
</s>
</p>
<p>
<s>
Feministická	feministický	k2eAgFnSc1d1	feministická
teologie	teologie	k1gFnSc1	teologie
</s>
</p>
<p>
<s>
Sociální	sociální	k2eAgFnSc1d1	sociální
spravedlnost	spravedlnost	k1gFnSc1	spravedlnost
</s>
</p>
<p>
<s>
Teologie	teologie	k1gFnSc1	teologie
</s>
</p>
<p>
<s>
Křesťanský	křesťanský	k2eAgInSc1d1	křesťanský
socialismus	socialismus	k1gInSc1	socialismus
</s>
</p>
<p>
<s>
Christologie	christologie	k1gFnSc1	christologie
</s>
</p>
<p>
<s>
Ekleziologie	Ekleziologie	k1gFnSc1	Ekleziologie
</s>
</p>
