<s>
Bílý	bílý	k2eAgInSc1d1	bílý
fosfor	fosfor	k1gInSc1	fosfor
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
z	z	k7c2	z
molekul	molekula	k1gFnPc2	molekula
P4	P4	k1gFnPc2	P4
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
příčinou	příčina	k1gFnSc7	příčina
jeho	jeho	k3xOp3gFnPc4	jeho
vysoké	vysoký	k2eAgFnPc4d1	vysoká
reaktivity	reaktivita	k1gFnPc4	reaktivita
<g/>
.	.	kIx.	.
</s>
