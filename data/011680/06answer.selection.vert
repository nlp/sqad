<s>
Milady	Milada	k1gFnPc4	Milada
Horákové	Horáková	k1gFnSc2	Horáková
je	být	k5eAaImIp3nS	být
ulice	ulice	k1gFnSc1	ulice
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
pražských	pražský	k2eAgInPc2d1	pražský
Hradčan	Hradčany	k1gInPc2	Hradčany
<g/>
,	,	kIx,	,
Střešovic	Střešovice	k1gFnPc2	Střešovice
<g/>
,	,	kIx,	,
Dejvic	Dejvice	k1gFnPc2	Dejvice
<g/>
,	,	kIx,	,
Bubenče	Bubeneč	k1gFnSc2	Bubeneč
a	a	k8xC	a
Holešovic	Holešovice	k1gFnPc2	Holešovice
<g/>
,	,	kIx,	,
procházející	procházející	k2eAgInSc1d1	procházející
západovýchodním	západovýchodní	k2eAgInSc7d1	západovýchodní
směrem	směr	k1gInSc7	směr
mezi	mezi	k7c7	mezi
ulicí	ulice	k1gFnSc7	ulice
Patočkovou	Patočkův	k2eAgFnSc7d1	Patočkova
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
Letenské	letenský	k2eAgNnSc4d1	Letenské
náměstí	náměstí	k1gNnSc4	náměstí
na	na	k7c4	na
Strossmayerovo	Strossmayerův	k2eAgNnSc4d1	Strossmayerovo
náměstí	náměstí	k1gNnSc4	náměstí
z	z	k7c2	z
Letné	Letná	k1gFnSc2	Letná
přes	přes	k7c4	přes
celou	celý	k2eAgFnSc4d1	celá
letenskou	letenský	k2eAgFnSc4d1	Letenská
pláň	pláň	k1gFnSc4	pláň
do	do	k7c2	do
Holešovic	Holešovice	k1gFnPc2	Holešovice
<g/>
.	.	kIx.	.
</s>
