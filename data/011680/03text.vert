<p>
<s>
Milady	Milada	k1gFnPc4	Milada
Horákové	Horáková	k1gFnSc2	Horáková
je	být	k5eAaImIp3nS	být
ulice	ulice	k1gFnSc1	ulice
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
pražských	pražský	k2eAgInPc2d1	pražský
Hradčan	Hradčany	k1gInPc2	Hradčany
<g/>
,	,	kIx,	,
Střešovic	Střešovice	k1gFnPc2	Střešovice
<g/>
,	,	kIx,	,
Dejvic	Dejvice	k1gFnPc2	Dejvice
<g/>
,	,	kIx,	,
Bubenče	Bubeneč	k1gFnSc2	Bubeneč
a	a	k8xC	a
Holešovic	Holešovice	k1gFnPc2	Holešovice
<g/>
,	,	kIx,	,
procházející	procházející	k2eAgInSc1d1	procházející
západovýchodním	západovýchodní	k2eAgInSc7d1	západovýchodní
směrem	směr	k1gInSc7	směr
mezi	mezi	k7c7	mezi
ulicí	ulice	k1gFnSc7	ulice
Patočkovou	Patočkův	k2eAgFnSc7d1	Patočkova
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
Letenské	letenský	k2eAgNnSc4d1	Letenské
náměstí	náměstí	k1gNnSc4	náměstí
na	na	k7c4	na
Strossmayerovo	Strossmayerův	k2eAgNnSc4d1	Strossmayerovo
náměstí	náměstí	k1gNnSc4	náměstí
z	z	k7c2	z
Letné	Letná	k1gFnSc2	Letná
přes	přes	k7c4	přes
celou	celý	k2eAgFnSc4d1	celá
letenskou	letenský	k2eAgFnSc4d1	Letenská
pláň	pláň	k1gFnSc4	pláň
do	do	k7c2	do
Holešovic	Holešovice	k1gFnPc2	Holešovice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byla	být	k5eAaImAgFnS	být
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
ze	z	k7c2	z
třídy	třída	k1gFnSc2	třída
Obránců	obránce	k1gMnPc2	obránce
míru	mír	k1gInSc2	mír
na	na	k7c4	na
Milady	Milada	k1gFnPc4	Milada
Horákové	Horáková	k1gFnSc2	Horáková
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
advokátky	advokátka	k1gFnSc2	advokátka
popravené	popravený	k2eAgFnSc2d1	popravená
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
je	být	k5eAaImIp3nS	být
vedena	veden	k2eAgFnSc1d1	vedena
dvoukolejná	dvoukolejný	k2eAgFnSc1d1	dvoukolejná
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
trať	trať	k1gFnSc1	trať
Strossmayerovo	Strossmayerův	k2eAgNnSc1d1	Strossmayerovo
náměstí	náměstí	k1gNnSc1	náměstí
-	-	kIx~	-
Hradčanská	hradčanský	k2eAgFnSc1d1	Hradčanská
-	-	kIx~	-
Malovanka	Malovanka	k1gFnSc1	Malovanka
pražské	pražský	k2eAgFnSc2d1	Pražská
MHD	MHD	kA	MHD
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
trpí	trpět	k5eAaImIp3nP	trpět
dopravními	dopravní	k2eAgFnPc7d1	dopravní
zácpami	zácpa	k1gFnPc7	zácpa
<g/>
,	,	kIx,	,
řešenými	řešený	k2eAgFnPc7d1	řešená
stavbou	stavba	k1gFnSc7	stavba
tunelu	tunel	k1gInSc2	tunel
Blanka	Blanka	k1gFnSc1	Blanka
a	a	k8xC	a
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
části	část	k1gFnSc6	část
odkloněním	odklonění	k1gNnSc7	odklonění
automobilové	automobilový	k2eAgFnSc2d1	automobilová
dopravy	doprava	k1gFnSc2	doprava
z	z	k7c2	z
Letenského	letenský	k2eAgNnSc2d1	Letenské
náměstí	náměstí	k1gNnSc2	náměstí
do	do	k7c2	do
Veletržní	veletržní	k2eAgFnSc2d1	veletržní
ulice	ulice	k1gFnSc2	ulice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
silnice	silnice	k1gFnSc2	silnice
osady	osada	k1gFnSc2	osada
Ovenec	Ovenec	k1gMnSc1	Ovenec
se	se	k3xPyFc4	se
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
páteřní	páteřní	k2eAgFnSc1d1	páteřní
komunikace	komunikace	k1gFnSc1	komunikace
předměstských	předměstský	k2eAgFnPc2d1	předměstská
osad	osada	k1gFnPc2	osada
Holešovice	Holešovice	k1gFnPc4	Holešovice
a	a	k8xC	a
Bubny	Bubny	k1gInPc4	Bubny
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
sloučeny	sloučit	k5eAaPmNgFnP	sloučit
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1884	[number]	k4	1884
připojeny	připojit	k5eAaPmNgInP	připojit
k	k	k7c3	k
Praze	Praha	k1gFnSc3	Praha
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
úpravu	úprava	k1gFnSc4	úprava
silnice	silnice	k1gFnSc2	silnice
na	na	k7c4	na
městskou	městský	k2eAgFnSc4d1	městská
ulici	ulice	k1gFnSc4	ulice
a	a	k8xC	a
stavební	stavební	k2eAgInSc4d1	stavební
boom	boom	k1gInSc4	boom
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
se	se	k3xPyFc4	se
nazývala	nazývat	k5eAaImAgFnS	nazývat
Belcrediho	Belcredi	k1gMnSc4	Belcredi
třída	třída	k1gFnSc1	třída
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
českého	český	k2eAgMnSc2d1	český
místodržitele	místodržitel	k1gMnSc2	místodržitel
a	a	k8xC	a
ministra	ministr	k1gMnSc2	ministr
Richarda	Richard	k1gMnSc2	Richard
Belcrediho	Belcredi	k1gMnSc2	Belcredi
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
okupace	okupace	k1gFnSc2	okupace
se	se	k3xPyFc4	se
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
Letenská	letenský	k2eAgFnSc1d1	Letenská
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
:	:	kIx,	:
Sommerbergstraße	Sommerbergstraße	k1gFnSc1	Sommerbergstraße
<g/>
,	,	kIx,	,
od	od	k7c2	od
června	červen	k1gInSc2	červen
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
Krále	Král	k1gMnSc2	Král
Jiřího	Jiří	k1gMnSc2	Jiří
VI	VI	kA	VI
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
projev	projev	k1gInSc4	projev
vděku	vděk	k1gInSc2	vděk
Velké	velký	k2eAgFnSc3d1	velká
Británii	Británie	k1gFnSc3	Británie
za	za	k7c4	za
účast	účast	k1gFnSc4	účast
v	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
osvobození	osvobození	k1gNnSc4	osvobození
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
)	)	kIx)	)
a	a	k8xC	a
poté	poté	k6eAd1	poté
po	po	k7c4	po
celé	celý	k2eAgNnSc4d1	celé
období	období	k1gNnSc4	období
socialismu	socialismus	k1gInSc2	socialismus
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
Obránců	obránce	k1gMnPc2	obránce
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
dětská	dětský	k2eAgNnPc4d1	dětské
léta	léto	k1gNnPc4	léto
zde	zde	k6eAd1	zde
prožitá	prožitý	k2eAgNnPc4d1	prožité
vzpomínal	vzpomínat	k5eAaImAgMnS	vzpomínat
spisovatel	spisovatel	k1gMnSc1	spisovatel
Václav	Václav	k1gMnSc1	Václav
Čtvrtek	čtvrtek	k1gInSc4	čtvrtek
v	v	k7c6	v
knížce	knížka	k1gFnSc6	knížka
Z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
i	i	k9	i
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1973	[number]	k4	1973
zde	zde	k6eAd1	zde
Olga	Olga	k1gFnSc1	Olga
Hepnarová	Hepnarový	k2eAgFnSc1d1	Hepnarová
na	na	k7c6	na
tramvajové	tramvajový	k2eAgFnSc6d1	tramvajová
zastávce	zastávka	k1gFnSc6	zastávka
Strossmayerovo	Strossmayerův	k2eAgNnSc4d1	Strossmayerovo
náměstí	náměstí	k1gNnSc4	náměstí
zabila	zabít	k5eAaPmAgFnS	zabít
8	[number]	k4	8
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úřední	úřední	k2eAgFnPc4d1	úřední
a	a	k8xC	a
obchodní	obchodní	k2eAgFnPc4d1	obchodní
budovy	budova	k1gFnPc4	budova
==	==	k?	==
</s>
</p>
<p>
<s>
Budova	budova	k1gFnSc1	budova
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
čp.	čp.	k?	čp.
1489	[number]	k4	1489
<g/>
/	/	kIx~	/
<g/>
VII	VII	kA	VII
postavena	postaven	k2eAgFnSc1d1	postavena
1935	[number]	k4	1935
<g/>
–	–	k?	–
<g/>
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
projekt	projekt	k1gInSc4	projekt
arch	archa	k1gFnPc2	archa
<g/>
.	.	kIx.	.
</s>
<s>
Kamil	Kamil	k1gMnSc1	Kamil
Roškot	Roškot	k1gInSc1	Roškot
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Kalous	Kalous	k1gMnSc1	Kalous
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Zázvorka	zázvorka	k1gMnSc1	zázvorka
<g/>
,	,	kIx,	,
lidově	lidově	k6eAd1	lidově
přezdívaná	přezdívaný	k2eAgFnSc1d1	přezdívaná
kachlíkárna	kachlíkárna	k1gFnSc1	kachlíkárna
<g/>
.	.	kIx.	.
</s>
<s>
Památkově	památkově	k6eAd1	památkově
chráněný	chráněný	k2eAgInSc4d1	chráněný
dominantní	dominantní	k2eAgInSc4d1	dominantní
objekt	objekt	k1gInSc4	objekt
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
Letenské	letenský	k2eAgFnSc2d1	Letenská
pláně	pláň	k1gFnSc2	pláň
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
budova	budova	k1gFnSc1	budova
dostavěna	dostavěn	k2eAgFnSc1d1	dostavěna
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hotel	hotel	k1gInSc1	hotel
Belvedere	Belveder	k1gInSc5	Belveder
-	-	kIx~	-
funkcionalistická	funkcionalistický	k2eAgFnSc1d1	funkcionalistická
budova	budova	k1gFnSc1	budova
ze	z	k7c2	z
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgMnSc1d1	sloužící
nepřetržitě	přetržitě	k6eNd1	přetržitě
svému	svůj	k3xOyFgInSc3	svůj
účelu	účel	k1gInSc3	účel
</s>
</p>
<p>
<s>
Obchodní	obchodní	k2eAgInSc1d1	obchodní
dům	dům	k1gInSc1	dům
Letná	Letná	k1gFnSc1	Letná
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Brouk	brouk	k1gMnSc1	brouk
a	a	k8xC	a
Babka	Babka	k1gMnSc1	Babka
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
administrativní	administrativní	k2eAgFnSc1d1	administrativní
a	a	k8xC	a
nájemní	nájemní	k2eAgFnSc1d1	nájemní
budova	budova	k1gFnSc1	budova
</s>
</p>
<p>
<s>
Erhartova	Erhartův	k2eAgFnSc1d1	Erhartova
cukrárna	cukrárna	k1gFnSc1	cukrárna
-	-	kIx~	-
podnik	podnik	k1gInSc1	podnik
se	s	k7c7	s
stylovým	stylový	k2eAgInSc7d1	stylový
funkcionalistickým	funkcionalistický	k2eAgInSc7d1	funkcionalistický
interiérem	interiér	k1gInSc7	interiér
<g/>
,	,	kIx,	,
otevřený	otevřený	k2eAgInSc1d1	otevřený
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
činžovní	činžovní	k2eAgInSc1d1	činžovní
dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
387	[number]	k4	387
<g/>
/	/	kIx~	/
<g/>
VIII	VIII	kA	VIII
<g/>
,	,	kIx,	,
návrh	návrh	k1gInSc4	návrh
arch	archa	k1gFnPc2	archa
<g/>
.	.	kIx.	.
</s>
<s>
Evžen	Evžen	k1gMnSc1	Evžen
Rosenberg	Rosenberg	k1gMnSc1	Rosenberg
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
zapsán	zapsat	k5eAaPmNgMnS	zapsat
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
nemovitých	movitý	k2eNgFnPc2d1	nemovitá
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
a	a	k8xC	a
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
prošel	projít	k5eAaPmAgInS	projít
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
</s>
</p>
<p>
<s>
Obvodní	obvodní	k2eAgFnSc1d1	obvodní
pošta	pošta	k1gFnSc1	pošta
a	a	k8xC	a
telefonní	telefonní	k2eAgFnSc1d1	telefonní
ústředna	ústředna	k1gFnSc1	ústředna
čp.	čp.	k?	čp.
383	[number]	k4	383
<g/>
/	/	kIx~	/
<g/>
VII	VII	kA	VII
<g/>
,	,	kIx,	,
1929	[number]	k4	1929
<g/>
-	-	kIx~	-
<g/>
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
návrh	návrh	k1gInSc4	návrh
arch	archa	k1gFnPc2	archa
<g/>
.	.	kIx.	.
</s>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Machoň	Machoň	k1gMnSc1	Machoň
<g/>
,	,	kIx,	,
budova	budova	k1gFnSc1	budova
památkově	památkově	k6eAd1	památkově
chráněná	chráněný	k2eAgFnSc1d1	chráněná
</s>
</p>
<p>
<s>
Budova	budova	k1gFnSc1	budova
Národního	národní	k2eAgInSc2d1	národní
archivu	archiv	k1gInSc2	archiv
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
Korunní	korunní	k2eAgInSc1d1	korunní
archiv	archiv	k1gInSc1	archiv
země	zem	k1gFnSc2	zem
České	český	k2eAgFnSc2d1	Česká
<g/>
,	,	kIx,	,
čp.	čp.	k?	čp.
133	[number]	k4	133
<g/>
/	/	kIx~	/
<g/>
VII	VII	kA	VII
<g/>
,	,	kIx,	,
pozměněná	pozměněný	k2eAgFnSc1d1	pozměněná
realizace	realizace	k1gFnSc1	realizace
původního	původní	k2eAgInSc2d1	původní
projektu	projekt	k1gInSc2	projekt
arch	archa	k1gFnPc2	archa
<g/>
.	.	kIx.	.
</s>
<s>
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Fragnera	Fragner	k1gMnSc2	Fragner
<g/>
;	;	kIx,	;
archivní	archivní	k2eAgInPc1d1	archivní
fondy	fond	k1gInPc1	fond
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
a	a	k8xC	a
klášterů	klášter	k1gInPc2	klášter
<g/>
,	,	kIx,	,
badatelna	badatelna	k1gFnSc1	badatelna
a	a	k8xC	a
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bývalá	bývalý	k2eAgFnSc1d1	bývalá
Letenská	letenský	k2eAgFnSc1d1	Letenská
kavárna	kavárna	k1gFnSc1	kavárna
-	-	kIx~	-
nárožní	nárožní	k2eAgFnPc4d1	nárožní
prostory	prostora	k1gFnPc4	prostora
v	v	k7c4	v
přízemí	přízemí	k1gNnPc4	přízemí
Molochova	Molochův	k2eAgNnPc4d1	Molochův
do	do	k7c2	do
Letenského	letenský	k2eAgNnSc2d1	Letenské
náměstí	náměstí	k1gNnSc2	náměstí
-	-	kIx~	-
místo	místo	k7c2	místo
setkávání	setkávání	k1gNnSc2	setkávání
umělců	umělec	k1gMnPc2	umělec
30	[number]	k4	30
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
Erotic	Erotice	k1gFnPc2	Erotice
City	City	k1gFnSc3	City
</s>
</p>
<p>
<s>
Velvyslanectví	velvyslanectví	k1gNnSc1	velvyslanectví
Indické	indický	k2eAgFnSc2d1	indická
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
čp.	čp.	k?	čp.
60	[number]	k4	60
<g/>
/	/	kIx~	/
<g/>
93	[number]	k4	93
</s>
</p>
<p>
<s>
Nárožní	nárožní	k2eAgFnSc1d1	nárožní
budova	budova	k1gFnSc1	budova
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yQgFnSc2	který
křižovatka	křižovatka	k1gFnSc1	křižovatka
třídy	třída	k1gFnSc2	třída
M.	M.	kA	M.
Horákové	Horáková	k1gFnPc4	Horáková
s	s	k7c7	s
Badeniho	Badeniha	k1gFnSc5	Badeniha
ulicí	ulice	k1gFnSc7	ulice
nesla	nést	k5eAaImAgFnS	nést
označení	označení	k1gNnSc4	označení
Špejchar	špejchar	k1gInSc1	špejchar
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
lidová	lidový	k2eAgFnSc1d1	lidová
restaurace	restaurace	k1gFnSc1	restaurace
s	s	k7c7	s
kioskem	kiosek	k1gInSc7	kiosek
a	a	k8xC	a
předzahrádkou	předzahrádka	k1gFnSc7	předzahrádka
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
byl	být	k5eAaImAgInS	být
kiosk	kiosk	k1gInSc1	kiosk
zbourán	zbourat	k5eAaPmNgInS	zbourat
<g/>
,	,	kIx,	,
budova	budova	k1gFnSc1	budova
oplocena	oplotit	k5eAaPmNgFnS	oplotit
a	a	k8xC	a
adaptována	adaptovat	k5eAaBmNgFnS	adaptovat
pro	pro	k7c4	pro
americkou	americký	k2eAgFnSc4d1	americká
církev	církev	k1gFnSc4	církev
mormonů	mormon	k1gMnPc2	mormon
</s>
</p>
<p>
<s>
Bývalá	bývalý	k2eAgFnSc1d1	bývalá
administrativní	administrativní	k2eAgFnSc1d1	administrativní
budova	budova	k1gFnSc1	budova
Škoda	škoda	k1gFnSc1	škoda
Praha	Praha	k1gFnSc1	Praha
Dodavatelsko-inženýrský	dodavatelskonženýrský	k2eAgInSc4d1	dodavatelsko-inženýrský
závod	závod	k1gInSc4	závod
<g/>
,	,	kIx,	,
stavba	stavba	k1gFnSc1	stavba
brutalistického	brutalistický	k2eAgInSc2d1	brutalistický
stylu	styl	k1gInSc2	styl
nad	nad	k7c7	nad
stanicí	stanice	k1gFnSc7	stanice
metra	metro	k1gNnSc2	metro
Hradčanská	hradčanský	k2eAgFnSc1d1	Hradčanská
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
sídlo	sídlo	k1gNnSc1	sídlo
zdravotních	zdravotní	k2eAgNnPc2d1	zdravotní
a	a	k8xC	a
tělocvičných	tělocvičný	k2eAgNnPc2d1	tělocvičné
zařízení	zařízení	k1gNnPc2	zařízení
</s>
</p>
<p>
<s>
==	==	k?	==
Obytné	obytný	k2eAgInPc1d1	obytný
domy	dům	k1gInPc1	dům
==	==	k?	==
</s>
</p>
<p>
<s>
Blok	blok	k1gInSc4	blok
13	[number]	k4	13
luxusních	luxusní	k2eAgInPc2d1	luxusní
činžovních	činžovní	k2eAgInPc2d1	činžovní
domů	dům	k1gInPc2	dům
zvaný	zvaný	k2eAgInSc1d1	zvaný
Molochov	Molochov	k1gInSc1	Molochov
(	(	kIx(	(
<g/>
252	[number]	k4	252
metrů	metr	k1gInPc2	metr
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
funkcionalistická	funkcionalistický	k2eAgFnSc1d1	funkcionalistická
architektura	architektura	k1gFnSc1	architektura
<g/>
,	,	kIx,	,
arch	arch	k1gInSc1	arch
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Havlíček	Havlíček	k1gMnSc1	Havlíček
a	a	k8xC	a
bratři	bratr	k1gMnPc1	bratr
Kohnové	Kohnová	k1gFnSc2	Kohnová
</s>
</p>
<p>
<s>
Činžovní	činžovní	k2eAgInSc1d1	činžovní
dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
479	[number]	k4	479
<g/>
/	/	kIx~	/
<g/>
VII	VII	kA	VII
<g/>
,	,	kIx,	,
č.	č.	k?	č.
<g/>
o.	o.	k?	o.
75	[number]	k4	75
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
prvního	první	k4xOgMnSc2	první
majitele	majitel	k1gMnSc2	majitel
zvaný	zvaný	k2eAgInSc1d1	zvaný
U	u	k7c2	u
Kutílků	Kutílek	k1gMnPc2	Kutílek
<g/>
,	,	kIx,	,
na	na	k7c6	na
fasádě	fasáda	k1gFnSc6	fasáda
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
Karla	Karel	k1gMnSc2	Karel
Kryla	Kryl	k1gMnSc2	Kryl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Novorenesanční	novorenesanční	k2eAgInSc1d1	novorenesanční
nárožní	nárožní	k2eAgInSc1d1	nárožní
dům	dům	k1gInSc1	dům
do	do	k7c2	do
Haškovy	Haškův	k2eAgFnSc2d1	Haškova
ulice	ulice	k1gFnSc2	ulice
<g/>
,	,	kIx,	,
čp.	čp.	k?	čp.
<g/>
599	[number]	k4	599
<g/>
/	/	kIx~	/
<g/>
VII	VII	kA	VII
<g/>
,	,	kIx,	,
se	s	k7c7	s
sochami	socha	k1gFnPc7	socha
múz	múza	k1gFnPc2	múza
v	v	k7c6	v
nikách	nika	k1gFnPc6	nika
</s>
</p>
<p>
<s>
Činžovní	činžovní	k2eAgInSc1d1	činžovní
dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
612	[number]	k4	612
<g/>
/	/	kIx~	/
<g/>
VII	VII	kA	VII
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
dal	dát	k5eAaPmAgMnS	dát
postavit	postavit	k5eAaPmF	postavit
řezbář	řezbář	k1gMnSc1	řezbář
Josef	Josef	k1gMnSc1	Josef
Krejčík	Krejčík	k1gMnSc1	Krejčík
<g/>
,	,	kIx,	,
na	na	k7c6	na
fasádě	fasáda	k1gFnSc6	fasáda
sgrafitta	sgrafitt	k1gMnSc2	sgrafitt
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
Mikoláše	Mikoláš	k1gMnSc2	Mikoláš
Alše	Aleš	k1gMnSc2	Aleš
</s>
</p>
<p>
<s>
Činžovní	činžovní	k2eAgInSc1d1	činžovní
dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
154	[number]	k4	154
<g/>
/	/	kIx~	/
<g/>
VII	VII	kA	VII
<g/>
,	,	kIx,	,
bydlel	bydlet	k5eAaImAgMnS	bydlet
zde	zde	k6eAd1	zde
malíř	malíř	k1gMnSc1	malíř
Emanuel	Emanuel	k1gMnSc1	Emanuel
Krescenc	Krescenc	k1gFnSc1	Krescenc
Liška	liška	k1gFnSc1	liška
</s>
</p>
<p>
<s>
==	==	k?	==
Ostatní	ostatní	k2eAgInPc4d1	ostatní
objekty	objekt	k1gInPc4	objekt
a	a	k8xC	a
místa	místo	k1gNnPc4	místo
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
od	od	k7c2	od
východu	východ	k1gInSc2	východ
k	k	k7c3	k
západu	západ	k1gInSc3	západ
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Strossmayerovo	Strossmayerův	k2eAgNnSc1d1	Strossmayerovo
náměstí	náměstí	k1gNnSc1	náměstí
</s>
</p>
<p>
<s>
Letenský	letenský	k2eAgInSc1d1	letenský
tunel	tunel	k1gInSc1	tunel
</s>
</p>
<p>
<s>
Letná	Letná	k1gFnSc1	Letná
</s>
</p>
<p>
<s>
Stadion	stadion	k1gInSc1	stadion
Sparty	Sparta	k1gFnSc2	Sparta
na	na	k7c6	na
Letné	Letná	k1gFnSc6	Letná
</s>
</p>
<p>
<s>
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
smyčka	smyčka	k1gFnSc1	smyčka
Špejchar	špejchar	k1gInSc1	špejchar
</s>
</p>
<p>
<s>
Hradčanská	hradčanský	k2eAgFnSc1d1	Hradčanská
(	(	kIx(	(
<g/>
stanice	stanice	k1gFnSc1	stanice
metra	metro	k1gNnSc2	metro
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Praha-Dejvice	Praha-Dejvice	k1gFnSc1	Praha-Dejvice
(	(	kIx(	(
<g/>
nádraží	nádraží	k1gNnSc1	nádraží
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tunelový	tunelový	k2eAgInSc1d1	tunelový
komplex	komplex	k1gInSc1	komplex
Blanka	Blanka	k1gFnSc1	Blanka
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
RUTH	Ruth	k1gFnSc1	Ruth
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Kronika	kronika	k1gFnSc1	kronika
královské	královský	k2eAgFnSc2d1	královská
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
obcí	obec	k1gFnPc2	obec
sousedních	sousední	k2eAgFnPc2d1	sousední
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Pavel	Pavel	k1gMnSc1	Pavel
Körber	Körber	k1gMnSc1	Körber
<g/>
,	,	kIx,	,
1903	[number]	k4	1903
<g/>
-	-	kIx~	-
<g/>
1904	[number]	k4	1904
<g/>
.	.	kIx.	.
1246	[number]	k4	1246
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Belcrediho	Belcredi	k1gMnSc2	Belcredi
třída	třída	k1gFnSc1	třída
<g/>
,	,	kIx,	,
s.	s.	k?	s.
32	[number]	k4	32
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
POCHE	POCHE	kA	POCHE
<g/>
,	,	kIx,	,
Emanuel	Emanuel	k1gMnSc1	Emanuel
<g/>
:	:	kIx,	:
<g/>
Prahou	Praha	k1gFnSc7	Praha
krok	krok	k1gInSc4	krok
za	za	k7c7	za
krokem	krok	k1gInSc7	krok
<g/>
.	.	kIx.	.
</s>
<s>
Panorama	panorama	k1gNnSc1	panorama
Praha	Praha	k1gFnSc1	Praha
1987	[number]	k4	1987
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
vydání	vydání	k1gNnSc2	vydání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
315	[number]	k4	315
<g/>
,	,	kIx,	,
381	[number]	k4	381
</s>
</p>
<p>
<s>
PYTLÍK	pytlík	k1gMnSc1	pytlík
<g/>
,	,	kIx,	,
Radko	Radka	k1gFnSc5	Radka
<g/>
:	:	kIx,	:
Toulky	toulka	k1gFnSc2	toulka
Prahou	Praha	k1gFnSc7	Praha
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Emporius	Emporius	k1gInSc1	Emporius
Praha	Praha	k1gFnSc1	Praha
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-86346-05-6	[number]	k4	80-86346-05-6
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Milady	Milada	k1gFnSc2	Milada
Horákové	Horáková	k1gFnSc2	Horáková
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
