<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
oslí	oslí	k2eAgMnSc1d1	oslí
(	(	kIx(	(
<g/>
Pygoscelis	Pygoscelis	k1gInSc1	Pygoscelis
papua	papu	k1gInSc2	papu
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nelétavý	létavý	k2eNgMnSc1d1	nelétavý
pták	pták	k1gMnSc1	pták
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Pygoscelis	Pygoscelis	k1gFnSc2	Pygoscelis
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
taktéž	taktéž	k?	taktéž
tučňák	tučňák	k1gMnSc1	tučňák
kroužkový	kroužkový	k2eAgMnSc1d1	kroužkový
(	(	kIx(	(
<g/>
Pygoscelis	Pygoscelis	k1gInSc1	Pygoscelis
adeliae	adeliaat	k5eAaPmIp3nS	adeliaat
<g/>
)	)	kIx)	)
a	a	k8xC	a
tučňák	tučňák	k1gMnSc1	tučňák
uzdičkový	uzdičkový	k2eAgMnSc1d1	uzdičkový
(	(	kIx(	(
<g/>
Pygoscelis	Pygoscelis	k1gInSc1	Pygoscelis
antarctica	antarctic	k1gInSc2	antarctic
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
pak	pak	k6eAd1	pak
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
čeledi	čeleď	k1gFnSc2	čeleď
tučňákovitých	tučňákovitý	k2eAgFnPc2d1	tučňákovitý
(	(	kIx(	(
<g/>
Sphenisciformes	Sphenisciformesa	k1gFnPc2	Sphenisciformesa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
jižního	jižní	k2eAgInSc2d1	jižní
polárního	polární	k2eAgInSc2d1	polární
kruhu	kruh	k1gInSc2	kruh
<g/>
,	,	kIx,	,
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
především	především	k9	především
na	na	k7c6	na
Antarktickém	antarktický	k2eAgInSc6d1	antarktický
poloostrově	poloostrov	k1gInSc6	poloostrov
a	a	k8xC	a
ostrovech	ostrov	k1gInPc6	ostrov
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Antarktické	antarktický	k2eAgFnSc2d1	antarktická
konvergence	konvergence	k1gFnSc2	konvergence
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
migrace	migrace	k1gFnSc2	migrace
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
až	až	k9	až
u	u	k7c2	u
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
,	,	kIx,	,
Tasmánie	Tasmánie	k1gFnSc2	Tasmánie
nebo	nebo	k8xC	nebo
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
zmínil	zmínit	k5eAaPmAgMnS	zmínit
německý	německý	k2eAgMnSc1d1	německý
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
skotského	skotský	k2eAgInSc2d1	skotský
původu	původ	k1gInSc2	původ
Johann	Johann	k1gMnSc1	Johann
Reinhold	Reinhold	k1gMnSc1	Reinhold
Forster	Forster	k1gMnSc1	Forster
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1781	[number]	k4	1781
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
střetl	střetnout	k5eAaPmAgInS	střetnout
při	při	k7c6	při
výzkumných	výzkumný	k2eAgFnPc6d1	výzkumná
plavbách	plavba	k1gFnPc6	plavba
na	na	k7c6	na
Falklandských	Falklandský	k2eAgInPc6d1	Falklandský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
hojně	hojně	k6eAd1	hojně
obývá	obývat	k5eAaImIp3nS	obývat
do	do	k7c2	do
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
až	až	k9	až
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
90	[number]	k4	90
cm	cm	kA	cm
<g/>
,	,	kIx,	,
průměrně	průměrně	k6eAd1	průměrně
75	[number]	k4	75
cm	cm	kA	cm
<g/>
,	,	kIx,	,
a	a	k8xC	a
drží	držet	k5eAaImIp3nS	držet
tak	tak	k9	tak
třetí	třetí	k4xOgFnSc4	třetí
příčku	příčka	k1gFnSc4	příčka
v	v	k7c6	v
pomyslném	pomyslný	k2eAgNnSc6d1	pomyslné
žebříčku	žebříčko	k1gNnSc6	žebříčko
největších	veliký	k2eAgMnPc2d3	veliký
žijících	žijící	k2eAgMnPc2d1	žijící
zástupců	zástupce	k1gMnPc2	zástupce
<g/>
.	.	kIx.	.
</s>
<s>
Dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
téměř	téměř	k6eAd1	téměř
devítikilogramové	devítikilogramový	k2eAgFnSc2d1	devítikilogramový
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
vysoce	vysoce	k6eAd1	vysoce
proměnlivá	proměnlivý	k2eAgFnSc1d1	proměnlivá
(	(	kIx(	(
<g/>
průměrně	průměrně	k6eAd1	průměrně
váží	vážit	k5eAaImIp3nS	vážit
pět	pět	k4xCc4	pět
kilogramů	kilogram	k1gInPc2	kilogram
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
potravou	potrava	k1gFnSc7	potrava
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
menšími	malý	k2eAgFnPc7d2	menší
rybami	ryba	k1gFnPc7	ryba
<g/>
,	,	kIx,	,
krilem	krilo	k1gNnSc7	krilo
a	a	k8xC	a
případně	případně	k6eAd1	případně
hlavonožci	hlavonožec	k1gMnPc1	hlavonožec
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
ročního	roční	k2eAgNnSc2d1	roční
období	období	k1gNnSc2	období
a	a	k8xC	a
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
polohy	poloha	k1gFnSc2	poloha
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
oslí	oslí	k2eAgMnSc1d1	oslí
je	být	k5eAaImIp3nS	být
společenský	společenský	k2eAgInSc1d1	společenský
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
hnízdící	hnízdící	k2eAgInPc1d1	hnízdící
v	v	k7c6	v
různě	různě	k6eAd1	různě
početných	početný	k2eAgFnPc6d1	početná
koloniích	kolonie	k1gFnPc6	kolonie
o	o	k7c6	o
desítkách	desítka	k1gFnPc6	desítka
ale	ale	k8xC	ale
i	i	k9	i
tisících	tisíc	k4xCgInPc6	tisíc
párech	pár	k1gInPc6	pár
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
monogamní	monogamní	k2eAgNnSc1d1	monogamní
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
spolu	spolu	k6eAd1	spolu
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
pár	pár	k1gInSc1	pár
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
často	často	k6eAd1	často
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Kolonie	kolonie	k1gFnPc1	kolonie
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
obvykle	obvykle	k6eAd1	obvykle
podél	podél	k7c2	podél
břehů	břeh	k1gInPc2	břeh
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mají	mít	k5eAaImIp3nP	mít
ptáci	pták	k1gMnPc1	pták
snadný	snadný	k2eAgInSc4d1	snadný
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
moři	moře	k1gNnSc3	moře
<g/>
.	.	kIx.	.
</s>
<s>
Rozmnožování	rozmnožování	k1gNnSc1	rozmnožování
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
je	být	k5eAaImIp3nS	být
sezónní	sezónní	k2eAgInSc4d1	sezónní
<g/>
;	;	kIx,	;
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
lokalitě	lokalita	k1gFnSc6	lokalita
nastává	nastávat	k5eAaImIp3nS	nastávat
počátkem	počátkem	k7c2	počátkem
května	květen	k1gInSc2	květen
až	až	k8xS	až
září	září	k1gNnSc2	září
<g/>
,	,	kIx,	,
a	a	k8xC	a
vrcholí	vrcholit	k5eAaImIp3nS	vrcholit
koncem	koncem	k7c2	koncem
října	říjen	k1gInSc2	říjen
až	až	k8xS	až
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Partneři	partner	k1gMnPc1	partner
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
rovným	rovný	k2eAgInSc7d1	rovný
dílem	díl	k1gInSc7	díl
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c4	na
péči	péče	k1gFnSc4	péče
o	o	k7c4	o
potomstvo	potomstvo	k1gNnSc4	potomstvo
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
snáší	snášet	k5eAaImIp3nS	snášet
obvykle	obvykle	k6eAd1	obvykle
dvě	dva	k4xCgNnPc1	dva
vejce	vejce	k1gNnPc1	vejce
<g/>
,	,	kIx,	,
každé	každý	k3xTgNnSc1	každý
o	o	k7c6	o
váze	váha	k1gFnSc6	váha
asi	asi	k9	asi
125	[number]	k4	125
gramů	gram	k1gInPc2	gram
a	a	k8xC	a
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
trvá	trvat	k5eAaImIp3nS	trvat
okolo	okolo	k7c2	okolo
35	[number]	k4	35
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
dospívají	dospívat	k5eAaImIp3nP	dospívat
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
80	[number]	k4	80
až	až	k9	až
100	[number]	k4	100
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
dožívá	dožívat	k5eAaImIp3nS	dožívat
průměrně	průměrně	k6eAd1	průměrně
13	[number]	k4	13
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
nejrychlejší	rychlý	k2eAgMnPc4d3	nejrychlejší
zástupce	zástupce	k1gMnPc4	zástupce
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
<g/>
;	;	kIx,	;
na	na	k7c6	na
základě	základ	k1gInSc6	základ
získaných	získaný	k2eAgInPc2d1	získaný
údajů	údaj	k1gInPc2	údaj
dokáže	dokázat	k5eAaPmIp3nS	dokázat
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
vyvinout	vyvinout	k5eAaPmF	vyvinout
rychlost	rychlost	k1gFnSc1	rychlost
blížící	blížící	k2eAgFnSc1d1	blížící
se	se	k3xPyFc4	se
k	k	k7c3	k
hranici	hranice	k1gFnSc3	hranice
40	[number]	k4	40
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
hod	hod	k1gInSc1	hod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Populace	populace	k1gFnSc1	populace
tučňáka	tučňák	k1gMnSc2	tučňák
oslího	oslí	k2eAgInSc2d1	oslí
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
stabilní	stabilní	k2eAgFnSc4d1	stabilní
a	a	k8xC	a
čítá	čítat	k5eAaImIp3nS	čítat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
620	[number]	k4	620
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
IUCN	IUCN	kA	IUCN
až	až	k9	až
774	[number]	k4	774
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
možnou	možný	k2eAgFnSc4d1	možná
hrozbu	hrozba	k1gFnSc4	hrozba
nebezpečnou	bezpečný	k2eNgFnSc4d1	nebezpečná
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
druh	druh	k1gInSc4	druh
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
považuje	považovat	k5eAaImIp3nS	považovat
znečišťování	znečišťování	k1gNnSc1	znečišťování
moří	mořit	k5eAaImIp3nS	mořit
lidským	lidský	k2eAgInSc7d1	lidský
odpadem	odpad	k1gInSc7	odpad
<g/>
,	,	kIx,	,
a	a	k8xC	a
případně	případně	k6eAd1	případně
nadměrný	nadměrný	k2eAgInSc1d1	nadměrný
rybolov	rybolov	k1gInSc1	rybolov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nomenklatura	nomenklatura	k1gFnSc1	nomenklatura
==	==	k?	==
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
oslí	oslí	k2eAgMnSc1d1	oslí
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
druhů	druh	k1gInPc2	druh
rodu	rod	k1gInSc2	rod
Pygoscelis	Pygoscelis	k1gFnPc2	Pygoscelis
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
taktéž	taktéž	k?	taktéž
řadí	řadit	k5eAaImIp3nS	řadit
žijící	žijící	k2eAgMnSc1d1	žijící
tučňák	tučňák	k1gMnSc1	tučňák
kroužkový	kroužkový	k2eAgMnSc1d1	kroužkový
(	(	kIx(	(
<g/>
Pygoscelis	Pygoscelis	k1gInSc1	Pygoscelis
adeliae	adeliaat	k5eAaPmIp3nS	adeliaat
<g/>
)	)	kIx)	)
a	a	k8xC	a
tučňák	tučňák	k1gMnSc1	tučňák
uzdičkový	uzdičkový	k2eAgMnSc1d1	uzdičkový
(	(	kIx(	(
<g/>
Pygoscelis	Pygoscelis	k1gInSc1	Pygoscelis
antarctica	antarctic	k1gInSc2	antarctic
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
se	se	k3xPyFc4	se
vyznačující	vyznačující	k2eAgMnPc1d1	vyznačující
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgMnPc4d1	jiný
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
kartáčovým	kartáčový	k2eAgInSc7d1	kartáčový
ocasem	ocas	k1gInSc7	ocas
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
moderní	moderní	k2eAgFnSc2d1	moderní
mitochondriální	mitochondriální	k2eAgFnSc2d1	mitochondriální
a	a	k8xC	a
jaderné	jaderný	k2eAgFnSc2d1	jaderná
DNA	DNA	kA	DNA
víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
předci	předek	k1gMnPc1	předek
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
oddělili	oddělit	k5eAaPmAgMnP	oddělit
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
tučňáků	tučňák	k1gMnPc2	tučňák
asi	asi	k9	asi
před	před	k7c7	před
38	[number]	k4	38
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc1	dva
miliony	milion	k4xCgInPc1	milion
let	léto	k1gNnPc2	léto
po	po	k7c6	po
předcích	předek	k1gInPc6	předek
rodu	rod	k1gInSc2	rod
Aptenodytes	Aptenodytesa	k1gFnPc2	Aptenodytesa
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
bazální	bazální	k2eAgFnSc7d1	bazální
větví	větev	k1gFnSc7	větev
celého	celý	k2eAgInSc2d1	celý
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
odtržení	odtržení	k1gNnSc3	odtržení
a	a	k8xC	a
existenci	existence	k1gFnSc3	existence
tučňáka	tučňák	k1gMnSc2	tučňák
oslího	oslí	k2eAgMnSc2d1	oslí
došlo	dojít	k5eAaPmAgNnS	dojít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
před	před	k7c7	před
14	[number]	k4	14
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
asi	asi	k9	asi
pět	pět	k4xCc4	pět
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
po	po	k7c6	po
příbuzném	příbuzný	k2eAgMnSc6d1	příbuzný
tučňákovi	tučňák	k1gMnSc6	tučňák
kroužkovém	kroužkový	k2eAgMnSc6d1	kroužkový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Etymologie	etymologie	k1gFnSc2	etymologie
===	===	k?	===
</s>
</p>
<p>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
jsou	být	k5eAaImIp3nP	být
poznamenáni	poznamenán	k2eAgMnPc1d1	poznamenán
chováním	chování	k1gNnSc7	chování
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgNnSc6	který
kradou	krást	k5eAaImIp3nP	krást
oblázky	oblázek	k1gInPc1	oblázek
z	z	k7c2	z
okolních	okolní	k2eAgFnPc2d1	okolní
sousedních	sousední	k2eAgFnPc2d1	sousední
hnízd	hnízdo	k1gNnPc2	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
rodový	rodový	k2eAgInSc1d1	rodový
název	název	k1gInSc1	název
tak	tak	k9	tak
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
latinského	latinský	k2eAgNnSc2d1	latinské
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
scelis	scelis	k1gFnSc1	scelis
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
scelus	scelus	k1gInSc1	scelus
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
zločin	zločin	k1gInSc1	zločin
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
krádež	krádež	k1gFnSc4	krádež
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
podle	podle	k7c2	podle
internetové	internetový	k2eAgFnSc2d1	internetová
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
ARKive	ARKiev	k1gFnSc2	ARKiev
<g/>
,	,	kIx,	,
dostali	dostat	k5eAaPmAgMnP	dostat
vědecké	vědecký	k2eAgNnSc4d1	vědecké
jméno	jméno	k1gNnSc4	jméno
Pygoscelis	Pygoscelis	k1gFnSc2	Pygoscelis
–	–	k?	–
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
překladu	překlad	k1gInSc6	překlad
"	"	kIx"	"
<g/>
opatřený	opatřený	k2eAgMnSc1d1	opatřený
smetákem	smeták	k1gInSc7	smeták
<g/>
"	"	kIx"	"
–	–	k?	–
na	na	k7c6	na
základě	základ	k1gInSc6	základ
komického	komický	k2eAgInSc2d1	komický
jevu	jev	k1gInSc2	jev
<g/>
,	,	kIx,	,
připomínající	připomínající	k2eAgNnSc1d1	připomínající
zametání	zametání	k1gNnSc1	zametání
<g/>
;	;	kIx,	;
tito	tento	k3xDgMnPc1	tento
tučňáci	tučňák	k1gMnPc1	tučňák
se	se	k3xPyFc4	se
po	po	k7c6	po
souši	souš	k1gFnSc6	souš
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
kolébavou	kolébavý	k2eAgFnSc7d1	kolébavá
chůzí	chůze	k1gFnSc7	chůze
<g/>
,	,	kIx,	,
a	a	k8xC	a
svým	svůj	k3xOyFgMnSc7	svůj
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
roztřepeným	roztřepený	k2eAgInSc7d1	roztřepený
ocasem	ocas	k1gInSc7	ocas
zavadí	zavadit	k5eAaPmIp3nS	zavadit
často	často	k6eAd1	často
o	o	k7c6	o
<g />
.	.	kIx.	.
</s>
<s>
zem	zem	k1gFnSc1	zem
<g/>
.	.	kIx.	.
<g/>
Druhové	druhový	k2eAgNnSc1d1	druhové
jméno	jméno	k1gNnSc1	jméno
papua	papua	k6eAd1	papua
pak	pak	k6eAd1	pak
nosí	nosit	k5eAaImIp3nP	nosit
poměrně	poměrně	k6eAd1	poměrně
neprávem	neprávo	k1gNnSc7	neprávo
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
chybné	chybný	k2eAgFnSc2d1	chybná
domněnky	domněnka	k1gFnSc2	domněnka
badatele	badatel	k1gMnPc4	badatel
a	a	k8xC	a
ornitologa	ornitolog	k1gMnSc4	ornitolog
Johanna	Johann	k1gMnSc4	Johann
Reinholda	Reinhold	k1gMnSc4	Reinhold
Forstera	Forster	k1gMnSc4	Forster
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
tučňák	tučňák	k1gMnSc1	tučňák
oslí	oslí	k2eAgMnSc1d1	oslí
obyvatelem	obyvatel	k1gMnSc7	obyvatel
přímořského	přímořský	k2eAgInSc2d1	přímořský
států	stát	k1gInPc2	stát
Papua-Nová	Papua-Nový	k2eAgNnPc4d1	Papua-Nový
Guiena	Guieno	k1gNnPc4	Guieno
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
je	být	k5eAaImIp3nS	být
pojmenován	pojmenovat	k5eAaPmNgMnS	pojmenovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
svého	svůj	k3xOyFgInSc2	svůj
specifického	specifický	k2eAgInSc2d1	specifický
hlasu	hlas	k1gInSc2	hlas
nápadně	nápadně	k6eAd1	nápadně
podobnému	podobný	k2eAgNnSc3d1	podobné
hýkání	hýkání	k1gNnSc3	hýkání
osla	osel	k1gMnSc2	osel
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnSc7	který
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
v	v	k7c6	v
období	období	k1gNnSc6	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Poddruhy	poddruh	k1gInPc4	poddruh
===	===	k?	===
</s>
</p>
<p>
<s>
Uznávají	uznávat	k5eAaImIp3nP	uznávat
se	se	k3xPyFc4	se
dva	dva	k4xCgInPc4	dva
podrruhy	podrruh	k1gInPc1	podrruh
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
odlišující	odlišující	k2eAgMnSc1d1	odlišující
podle	podle	k7c2	podle
fyzických	fyzický	k2eAgFnPc2d1	fyzická
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
;	;	kIx,	;
severněji	severně	k6eAd2	severně
hnízdící	hnízdící	k2eAgMnPc1d1	hnízdící
ptáci	pták	k1gMnPc1	pták
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
vyšších	vysoký	k2eAgInPc2d2	vyšší
rozměrů	rozměr	k1gInPc2	rozměr
než	než	k8xS	než
ptáci	pták	k1gMnPc1	pták
žijící	žijící	k2eAgFnSc2d1	žijící
blíže	blíž	k1gFnSc2	blíž
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Pygoscelis	Pygoscelis	k1gFnSc1	Pygoscelis
papua	papua	k1gFnSc1	papua
papua	papua	k1gFnSc1	papua
(	(	kIx(	(
<g/>
J.	J.	kA	J.
R.	R.	kA	R.
Forster	Forster	k1gMnSc1	Forster
<g/>
,	,	kIx,	,
1781	[number]	k4	1781
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
subspecie	subspecie	k1gFnSc1	subspecie
vyšších	vysoký	k2eAgInPc2d2	vyšší
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
,	,	kIx,	,
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
Falklandských	Falklandský	k2eAgInPc6d1	Falklandský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
třeba	třeba	k6eAd1	třeba
po	po	k7c6	po
boku	bok	k1gInSc6	bok
tučňáků	tučňák	k1gMnPc2	tučňák
královský	královský	k2eAgMnSc1d1	královský
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Maquarie	Maquarie	k1gFnSc2	Maquarie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pygoscelis	Pygoscelis	k1gFnSc1	Pygoscelis
papua	papu	k2eAgFnSc1d1	papu
ellsworthi	ellsworth	k1gMnPc7	ellsworth
(	(	kIx(	(
<g/>
Murphy	Murph	k1gMnPc7	Murph
<g/>
,	,	kIx,	,
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
subspecie	subspecie	k1gFnSc1	subspecie
nižších	nízký	k2eAgInPc2d2	nižší
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
,	,	kIx,	,
obývá	obývat	k5eAaImIp3nS	obývat
například	například	k6eAd1	například
antarktický	antarktický	k2eAgInSc1d1	antarktický
poloostrov	poloostrov	k1gInSc1	poloostrov
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc2d1	jižní
Shetlandy	Shetlanda	k1gFnSc2	Shetlanda
či	či	k8xC	či
Jižní	jižní	k2eAgInPc1d1	jižní
Sandwichovy	Sandwichův	k2eAgInPc1d1	Sandwichův
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Domovinou	domovina	k1gFnSc7	domovina
těchto	tento	k3xDgMnPc2	tento
ptáků	pták	k1gMnPc2	pták
jsou	být	k5eAaImIp3nP	být
chladná	chladný	k2eAgNnPc1d1	chladné
<g/>
,	,	kIx,	,
slaná	slaný	k2eAgNnPc1d1	slané
moře	moře	k1gNnPc1	moře
na	na	k7c6	na
Jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
obývají	obývat	k5eAaImIp3nP	obývat
zejména	zejména	k9	zejména
Antarktický	antarktický	k2eAgInSc4d1	antarktický
poloostrov	poloostrov	k1gInSc4	poloostrov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
asi	asi	k9	asi
25	[number]	k4	25
%	%	kIx~	%
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
a	a	k8xC	a
blízké	blízký	k2eAgInPc1d1	blízký
subantarktické	subantarktický	k2eAgInPc1d1	subantarktický
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
;	;	kIx,	;
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
nejvíce	hodně	k6eAd3	hodně
Falklandské	Falklandský	k2eAgInPc4d1	Falklandský
ostrovy	ostrov	k1gInPc4	ostrov
(	(	kIx(	(
<g/>
až	až	k9	až
40	[number]	k4	40
%	%	kIx~	%
světové	světový	k2eAgFnSc2d1	světová
populace	populace	k1gFnSc2	populace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc4d1	jižní
Georgii	Georgie	k1gFnSc4	Georgie
a	a	k8xC	a
Jižní	jižní	k2eAgInPc4d1	jižní
Sandwichovy	Sandwichův	k2eAgInPc4d1	Sandwichův
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnPc4d1	jižní
Shetlandy	Shetlanda	k1gFnPc4	Shetlanda
<g/>
,	,	kIx,	,
souostroví	souostroví	k1gNnSc3	souostroví
Kergueleny	Kerguelen	k2eAgInPc1d1	Kerguelen
<g/>
,	,	kIx,	,
Crozetovy	Crozetův	k2eAgInPc1d1	Crozetův
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
Heardův	Heardův	k2eAgInSc1d1	Heardův
ostrov	ostrov	k1gInSc1	ostrov
a	a	k8xC	a
McDonaldovy	McDonaldův	k2eAgInPc1d1	McDonaldův
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
Jižní	jižní	k2eAgFnSc2d1	jižní
Orkneje	Orkneje	k1gFnPc1	Orkneje
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgFnSc4d2	menší
<g/>
,	,	kIx,	,
jen	jen	k9	jen
několika	několik	k4yIc7	několik
tisícové	tisícový	k2eAgFnSc2d1	tisícová
kolonie	kolonie	k1gFnSc2	kolonie
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
například	například	k6eAd1	například
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Macquarie	Macquarie	k1gFnSc2	Macquarie
po	po	k7c6	po
boku	bok	k1gInSc6	bok
tučňáků	tučňák	k1gMnPc2	tučňák
královských	královský	k2eAgMnPc2d1	královský
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Marion	Marion	k1gInSc4	Marion
a	a	k8xC	a
sousedících	sousedící	k2eAgInPc6d1	sousedící
ostrovech	ostrov	k1gInPc6	ostrov
prince	princ	k1gMnSc2	princ
Edvarda	Edvard	k1gMnSc2	Edvard
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
nedávno	nedávno	k6eAd1	nedávno
byl	být	k5eAaImAgInS	být
zpozorován	zpozorovat	k5eAaPmNgInS	zpozorovat
i	i	k9	i
třeba	třeba	k6eAd1	třeba
na	na	k7c6	na
argentinském	argentinský	k2eAgInSc6d1	argentinský
ostrově	ostrov	k1gInSc6	ostrov
s	s	k7c7	s
celým	celý	k2eAgInSc7d1	celý
názvem	název	k1gInSc7	název
Martillo	Martillo	k1gNnSc4	Martillo
a	a	k8xC	a
Islas	Islas	k1gInSc4	Islas
de	de	k?	de
los	los	k1gInSc1	los
Estado	Estada	k1gFnSc5	Estada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
pouze	pouze	k6eAd1	pouze
pár	pár	k4xCyI	pár
stovek	stovka	k1gFnPc2	stovka
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Odborníci	odborník	k1gMnPc1	odborník
celkově	celkově	k6eAd1	celkově
evidují	evidovat	k5eAaImIp3nP	evidovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
300	[number]	k4	300
000	[number]	k4	000
hnízdících	hnízdící	k2eAgInPc2d1	hnízdící
párů	pár	k1gInPc2	pár
z	z	k7c2	z
prakticky	prakticky	k6eAd1	prakticky
neměnného	měnný	k2eNgInSc2d1	neměnný
trendu	trend	k1gInSc2	trend
trvajícího	trvající	k2eAgInSc2d1	trvající
již	již	k6eAd1	již
desítky	desítka	k1gFnPc4	desítka
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
Severněji	severně	k6eAd2	severně
hnízdící	hnízdící	k2eAgMnPc1d1	hnízdící
ptáci	pták	k1gMnPc1	pták
(	(	kIx(	(
<g/>
Pygoscelis	Pygoscelis	k1gInSc1	Pygoscelis
papua	papuus	k1gMnSc2	papuus
papua	papuus	k1gMnSc2	papuus
<g/>
)	)	kIx)	)
pobývají	pobývat	k5eAaImIp3nP	pobývat
ve	v	k7c6	v
vodách	voda	k1gFnPc6	voda
o	o	k7c6	o
průměrné	průměrný	k2eAgFnSc6d1	průměrná
teplotě	teplota	k1gFnSc6	teplota
4	[number]	k4	4
až	až	k9	až
8	[number]	k4	8
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jižněji	jižně	k6eAd2	jižně
hnízdící	hnízdící	k2eAgMnPc1d1	hnízdící
ptáci	pták	k1gMnPc1	pták
(	(	kIx(	(
<g/>
Pygoscelis	Pygoscelis	k1gInSc1	Pygoscelis
papua	papua	k6eAd1	papua
ellsworthi	ellsworthi	k6eAd1	ellsworthi
<g/>
)	)	kIx)	)
odolávají	odolávat	k5eAaImIp3nP	odolávat
velmi	velmi	k6eAd1	velmi
studeným	studený	k2eAgNnPc3d1	studené
mořím	moře	k1gNnPc3	moře
o	o	k7c6	o
průměrné	průměrný	k2eAgFnSc6d1	průměrná
teplotě	teplota	k1gFnSc6	teplota
-1	-1	k4	-1
až	až	k9	až
2	[number]	k4	2
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
V	v	k7c6	v
mimo	mimo	k6eAd1	mimo
hnízdním	hnízdní	k2eAgNnSc6d1	hnízdní
období	období	k1gNnSc6	období
není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
tito	tento	k3xDgMnPc1	tento
ptáci	pták	k1gMnPc1	pták
migrují	migrovat	k5eAaImIp3nP	migrovat
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
nebyli	být	k5eNaImAgMnP	být
provedeny	provést	k5eAaPmNgInP	provést
žádné	žádný	k3yNgInPc1	žádný
podstatné	podstatný	k2eAgInPc1d1	podstatný
výzkumy	výzkum	k1gInPc1	výzkum
<g/>
,	,	kIx,	,
ornitologové	ornitolog	k1gMnPc1	ornitolog
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zdržují	zdržovat	k5eAaImIp3nP	zdržovat
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
na	na	k7c6	na
hnízdišti	hnízdiště	k1gNnSc6	hnízdiště
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
blízkosti	blízkost	k1gFnSc6	blízkost
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
byla	být	k5eAaImAgFnS	být
řada	řada	k1gFnSc1	řada
tuláků	tulák	k1gMnPc2	tulák
spatřena	spatřit	k5eAaPmNgFnS	spatřit
až	až	k9	až
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Argentiny	Argentina	k1gFnSc2	Argentina
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
již	již	k6eAd1	již
nevelký	velký	k2eNgInSc1d1	nevelký
počet	počet	k1gInSc1	počet
ptáků	pták	k1gMnPc2	pták
dokonce	dokonce	k9	dokonce
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
Austrálie	Austrálie	k1gFnSc2	Austrálie
a	a	k8xC	a
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
oslí	oslí	k2eAgMnSc1d1	oslí
je	být	k5eAaImIp3nS	být
nelétavý	létavý	k2eNgMnSc1d1	nelétavý
pták	pták	k1gMnSc1	pták
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
přizpůsobený	přizpůsobený	k2eAgMnSc1d1	přizpůsobený
k	k	k7c3	k
životu	život	k1gInSc3	život
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
loví	lovit	k5eAaImIp3nP	lovit
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Namísto	namísto	k7c2	namísto
lehkých	lehký	k2eAgNnPc2d1	lehké
křídel	křídlo	k1gNnPc2	křídlo
má	mít	k5eAaImIp3nS	mít
tuhé	tuhý	k2eAgFnSc2d1	tuhá
ploutve	ploutev	k1gFnSc2	ploutev
připomínající	připomínající	k2eAgNnSc4d1	připomínající
vesla	veslo	k1gNnPc4	veslo
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mu	on	k3xPp3gNnSc3	on
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
obratně	obratně	k6eAd1	obratně
se	se	k3xPyFc4	se
v	v	k7c6	v
hluboké	hluboký	k2eAgFnSc6d1	hluboká
vodě	voda	k1gFnSc6	voda
pohybovat	pohybovat	k5eAaImF	pohybovat
<g/>
.	.	kIx.	.
<g/>
Jako	jako	k8xS	jako
ostatní	ostatní	k2eAgMnPc1d1	ostatní
tučňáci	tučňák	k1gMnPc1	tučňák
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
tučňák	tučňák	k1gMnSc1	tučňák
oslí	oslí	k2eAgMnSc1d1	oslí
má	mít	k5eAaImIp3nS	mít
tělo	tělo	k1gNnSc1	tělo
s	s	k7c7	s
kontrastním	kontrastní	k2eAgNnSc7d1	kontrastní
černobílým	černobílý	k2eAgNnSc7d1	černobílé
zbarvením	zbarvení	k1gNnSc7	zbarvení
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
maskován	maskovat	k5eAaBmNgInS	maskovat
při	při	k7c6	při
pobytu	pobyt	k1gInSc6	pobyt
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
<g/>
;	;	kIx,	;
tmavý	tmavý	k2eAgInSc1d1	tmavý
a	a	k8xC	a
světlý	světlý	k2eAgInSc1d1	světlý
odstín	odstín	k1gInSc1	odstín
se	se	k3xPyFc4	se
dobře	dobře	k6eAd1	dobře
mísí	mísit	k5eAaImIp3nP	mísit
s	s	k7c7	s
temným	temný	k2eAgInSc7d1	temný
oceánem	oceán	k1gInSc7	oceán
a	a	k8xC	a
světlou	světlý	k2eAgFnSc7d1	světlá
oblohou	obloha	k1gFnSc7	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Specifické	specifický	k2eAgFnPc1d1	specifická
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gFnPc1	jeho
bílé	bílý	k2eAgFnPc1d1	bílá
skvrny	skvrna	k1gFnPc1	skvrna
klínovitého	klínovitý	k2eAgInSc2d1	klínovitý
tvaru	tvar	k1gInSc2	tvar
nad	nad	k7c7	nad
očima	oko	k1gNnPc7	oko
a	a	k8xC	a
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Duhovku	duhovka	k1gFnSc4	duhovka
má	mít	k5eAaImIp3nS	mít
tmavě	tmavě	k6eAd1	tmavě
hnědou	hnědý	k2eAgFnSc4d1	hnědá
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jistě	jistě	k9	jistě
zaujme	zaujmout	k5eAaPmIp3nS	zaujmout
jasně	jasně	k6eAd1	jasně
oranžovým	oranžový	k2eAgInSc7d1	oranžový
až	až	k8xS	až
červeným	červený	k2eAgInSc7d1	červený
pruhem	pruh	k1gInSc7	pruh
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
zobáku	zobák	k1gInSc2	zobák
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
výrazně	výrazně	k6eAd1	výrazně
růžovýma	růžový	k2eAgFnPc7d1	růžová
až	až	k8xS	až
oranžovýma	oranžový	k2eAgFnPc7d1	oranžová
nohama	noha	k1gFnPc7	noha
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
ostatní	ostatní	k2eAgMnPc1d1	ostatní
ptáci	pták	k1gMnPc1	pták
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Pygoscelis	Pygoscelis	k1gFnSc2	Pygoscelis
má	mít	k5eAaImIp3nS	mít
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
kartáčový	kartáčový	k2eAgInSc1d1	kartáčový
ocas	ocas	k1gInSc1	ocas
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
14	[number]	k4	14
až	až	k9	až
18	[number]	k4	18
per	pero	k1gNnPc2	pero
<g/>
,	,	kIx,	,
dorůstajících	dorůstající	k2eAgFnPc2d1	dorůstající
až	až	k9	až
do	do	k7c2	do
délky	délka	k1gFnSc2	délka
15	[number]	k4	15
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
má	mít	k5eAaImIp3nS	mít
pokryté	pokrytý	k2eAgNnSc1d1	pokryté
velmi	velmi	k6eAd1	velmi
jemným	jemný	k2eAgNnSc7d1	jemné
peřím	peří	k1gNnSc7	peří
<g/>
;	;	kIx,	;
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
centimetru	centimetr	k1gInSc6	centimetr
čtverečním	čtvereční	k2eAgInSc6d1	čtvereční
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
až	až	k9	až
28	[number]	k4	28
peříček	peříčko	k1gNnPc2	peříčko
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
jej	on	k3xPp3gNnSc4	on
udržel	udržet	k5eAaPmAgMnS	udržet
vodotěsné	vodotěsný	k2eAgNnSc4d1	vodotěsné
<g/>
,	,	kIx,	,
zobákem	zobák	k1gInSc7	zobák
si	se	k3xPyFc3	se
ho	on	k3xPp3gInSc4	on
pravidelně	pravidelně	k6eAd1	pravidelně
mastí	mastit	k5eAaImIp3nP	mastit
vlastní	vlastní	k2eAgFnSc7d1	vlastní
olejovitou	olejovitý	k2eAgFnSc7d1	olejovitá
hmotou	hmota	k1gFnSc7	hmota
z	z	k7c2	z
nadocasní	nadocasní	k2eAgFnSc2d1	nadocasní
žlázy	žláza	k1gFnSc2	žláza
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
před	před	k7c7	před
dovršením	dovršení	k1gNnSc7	dovršení
14	[number]	k4	14
měsíců	měsíc	k1gInPc2	měsíc
prakticky	prakticky	k6eAd1	prakticky
stejně	stejně	k6eAd1	stejně
zbarvená	zbarvený	k2eAgFnSc1d1	zbarvená
(	(	kIx(	(
<g/>
šedě	šedě	k6eAd1	šedě
až	až	k9	až
černě	černě	k6eAd1	černě
na	na	k7c6	na
zádech	záda	k1gNnPc6	záda
<g/>
,	,	kIx,	,
bíle	bíle	k6eAd1	bíle
na	na	k7c6	na
břiše	břicho	k1gNnSc6	břicho
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
od	od	k7c2	od
dospělých	dospělí	k1gMnPc2	dospělí
odlišná	odlišný	k2eAgFnSc1d1	odlišná
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
bíle	bíle	k6eAd1	bíle
kresby	kresba	k1gFnPc1	kresba
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
až	až	k9	až
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
90	[number]	k4	90
cm	cm	kA	cm
<g/>
,	,	kIx,	,
průměrně	průměrně	k6eAd1	průměrně
75	[number]	k4	75
cm	cm	kA	cm
<g/>
,	,	kIx,	,
a	a	k8xC	a
drží	držet	k5eAaImIp3nS	držet
tak	tak	k9	tak
třetí	třetí	k4xOgFnSc4	třetí
příčku	příčka	k1gFnSc4	příčka
(	(	kIx(	(
<g/>
po	po	k7c6	po
tučňákovi	tučňák	k1gMnSc6	tučňák
císařském	císařský	k2eAgMnSc6d1	císařský
a	a	k8xC	a
patagonském	patagonský	k2eAgNnSc6d1	patagonské
<g/>
)	)	kIx)	)
v	v	k7c6	v
pomyslném	pomyslný	k2eAgNnSc6d1	pomyslné
žebříčku	žebříčko	k1gNnSc6	žebříčko
největších	veliký	k2eAgMnPc2d3	veliký
žijících	žijící	k2eAgMnPc2d1	žijící
zástupců	zástupce	k1gMnPc2	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
Sphenisciformes	Sphenisciformes	k1gInSc1	Sphenisciformes
<g/>
.	.	kIx.	.
</s>
<s>
Dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
téměř	téměř	k6eAd1	téměř
devítikilogramové	devítikilogramový	k2eAgFnSc2d1	devítikilogramový
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
vysoce	vysoce	k6eAd1	vysoce
proměnlivá	proměnlivý	k2eAgFnSc1d1	proměnlivá
<g/>
;	;	kIx,	;
v	v	k7c6	v
období	období	k1gNnSc6	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
či	či	k8xC	či
pelichání	pelichání	k1gNnSc2	pelichání
–	–	k?	–
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
vyznačeném	vyznačený	k2eAgInSc6d1	vyznačený
nižším	nízký	k2eAgFnPc3d2	nižší
příjem	příjem	k1gInSc4	příjem
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
jeho	jeho	k3xOp3gFnSc1	jeho
váha	váha	k1gFnSc1	váha
klesnout	klesnout	k5eAaPmF	klesnout
na	na	k7c4	na
méně	málo	k6eAd2	málo
než	než	k8xS	než
pět	pět	k4xCc4	pět
kilogramů	kilogram	k1gInPc2	kilogram
<g/>
.	.	kIx.	.
</s>
<s>
Severněji	severně	k6eAd2	severně
hnízdící	hnízdící	k2eAgMnPc1d1	hnízdící
ptáci	pták	k1gMnPc1	pták
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
vyšších	vysoký	k2eAgInPc2d2	vyšší
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
,	,	kIx,	,
než	než	k8xS	než
ptáci	pták	k1gMnPc1	pták
žijící	žijící	k2eAgFnSc2d1	žijící
blíže	blíž	k1gFnSc2	blíž
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavní	pohlavní	k2eAgInSc1d1	pohlavní
dimorfismus	dimorfismus	k1gInSc1	dimorfismus
je	být	k5eAaImIp3nS	být
nevýrazný	výrazný	k2eNgInSc1d1	nevýrazný
a	a	k8xC	a
obě	dva	k4xCgNnPc1	dva
pohlaví	pohlaví	k1gNnPc2	pohlaví
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	se	k3xPyFc2	se
liší	lišit	k5eAaImIp3nP	lišit
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
tělesných	tělesný	k2eAgInPc6d1	tělesný
parametrech	parametr	k1gInPc6	parametr
<g/>
:	:	kIx,	:
samice	samice	k1gFnPc1	samice
má	mít	k5eAaImIp3nS	mít
nepatrně	patrně	k6eNd1	patrně
nižší	nízký	k2eAgFnSc4d2	nižší
hmotnost	hmotnost	k1gFnSc4	hmotnost
<g/>
,	,	kIx,	,
výšku	výška	k1gFnSc4	výška
<g/>
,	,	kIx,	,
délku	délka	k1gFnSc4	délka
končetin	končetina	k1gFnPc2	končetina
či	či	k8xC	či
zobáku	zobák	k1gInSc2	zobák
<g/>
,	,	kIx,	,
nežli	nežli	k8xS	nežli
samec	samec	k1gMnSc1	samec
.	.	kIx.	.
<g/>
Průměrně	průměrně	k6eAd1	průměrně
se	se	k3xPyFc4	se
tučňák	tučňák	k1gMnSc1	tučňák
oslí	oslí	k2eAgMnSc1d1	oslí
dožívá	dožívat	k5eAaImIp3nS	dožívat
13	[number]	k4	13
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Kritickým	kritický	k2eAgNnSc7d1	kritické
obdobím	období	k1gNnSc7	období
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
druh	druh	k1gInSc4	druh
první	první	k4xOgFnSc6	první
rok	rok	k1gInSc1	rok
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterého	který	k3yRgInSc2	který
umírá	umírat	k5eAaImIp3nS	umírat
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
%	%	kIx~	%
mláďat	mládě	k1gNnPc2	mládě
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
téměř	téměř	k6eAd1	téměř
každé	každý	k3xTgNnSc1	každý
druhé	druhý	k4xOgNnSc1	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
dožije	dožít	k5eAaPmIp3nS	dožít
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
jedinec	jedinec	k1gMnSc1	jedinec
druhého	druhý	k4xOgInSc2	druhý
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
šance	šance	k1gFnPc1	šance
na	na	k7c4	na
přežití	přežití	k1gNnSc4	přežití
výrazně	výrazně	k6eAd1	výrazně
roste	růst	k5eAaImIp3nS	růst
(	(	kIx(	(
<g/>
až	až	k9	až
80	[number]	k4	80
%	%	kIx~	%
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
se	se	k3xPyFc4	se
dožívá	dožívat	k5eAaImIp3nS	dožívat
nezvykle	zvykle	k6eNd1	zvykle
nízkého	nízký	k2eAgInSc2d1	nízký
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
průměrně	průměrně	k6eAd1	průměrně
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chování	chování	k1gNnSc1	chování
==	==	k?	==
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
oslí	oslí	k2eAgMnSc1d1	oslí
je	být	k5eAaImIp3nS	být
společenský	společenský	k2eAgInSc1d1	společenský
druh	druh	k1gInSc1	druh
zvyklí	zvyklý	k2eAgMnPc1d1	zvyklý
hnízdit	hnízdit	k5eAaImF	hnízdit
v	v	k7c6	v
početných	početný	k2eAgFnPc6d1	početná
koloniích	kolonie	k1gFnPc6	kolonie
o	o	k7c6	o
desítkách	desítka	k1gFnPc6	desítka
až	až	k9	až
tisících	tisíc	k4xCgInPc6	tisíc
párech	pár	k1gInPc6	pár
<g/>
,	,	kIx,	,
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
po	po	k7c6	po
boku	bok	k1gInSc6	bok
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
tučňáků	tučňák	k1gMnPc2	tučňák
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgMnPc2d1	jiný
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
dostupných	dostupný	k2eAgInPc2d1	dostupný
pramenů	pramen	k1gInPc2	pramen
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
monogamní	monogamní	k2eAgFnSc1d1	monogamní
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
spolu	spolu	k6eAd1	spolu
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
pár	pár	k1gInSc1	pár
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
často	často	k6eAd1	často
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
a	a	k8xC	a
k	k	k7c3	k
přetržení	přetržení	k1gNnSc3	přetržení
vazeb	vazba	k1gFnPc2	vazba
dochází	docházet	k5eAaImIp3nS	docházet
jen	jen	k9	jen
zřídka	zřídka	k6eAd1	zřídka
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
páru	pár	k1gInSc2	pár
jednoduše	jednoduše	k6eAd1	jednoduše
nedaří	dařit	k5eNaImIp3nS	dařit
(	(	kIx(	(
<g/>
opomineme	opominout	k5eAaPmIp1nP	opominout
<g/>
-li	i	k?	-li
možné	možný	k2eAgInPc1d1	možný
úmrtí	úmrtí	k1gNnPc2	úmrtí
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
partnerů	partner	k1gMnPc2	partner
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgInPc1	takový
páry	pár	k1gInPc1	pár
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nezabývají	zabývat	k5eNaImIp3nP	zabývat
námluvami	námluva	k1gFnPc7	námluva
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vzniká	vznikat	k5eAaImIp3nS	vznikat
cenná	cenný	k2eAgFnSc1d1	cenná
úspora	úspora	k1gFnSc1	úspora
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Celkovou	celkový	k2eAgFnSc4d1	celková
procentuální	procentuální	k2eAgFnSc4d1	procentuální
bilanci	bilance	k1gFnSc4	bilance
mohou	moct	k5eAaImIp3nP	moct
zkreslit	zkreslit	k5eAaPmF	zkreslit
mladí	mladý	k2eAgMnPc1d1	mladý
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
nezkušení	zkušený	k2eNgMnPc1d1	nezkušený
jedinci	jedinec	k1gMnPc1	jedinec
<g/>
.	.	kIx.	.
<g/>
Kolonie	kolonie	k1gFnSc1	kolonie
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
obvykle	obvykle	k6eAd1	obvykle
podél	podél	k7c2	podél
břehů	břeh	k1gInPc2	břeh
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mají	mít	k5eAaImIp3nP	mít
ptáci	pták	k1gMnPc1	pták
snadný	snadný	k2eAgInSc4d1	snadný
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
moři	moře	k1gNnSc3	moře
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
jedna	jeden	k4xCgFnSc1	jeden
skupina	skupina	k1gFnSc1	skupina
ptáků	pták	k1gMnPc2	pták
může	moct	k5eAaImIp3nS	moct
hnízdit	hnízdit	k5eAaImF	hnízdit
na	na	k7c6	na
zasněžené	zasněžený	k2eAgFnSc6d1	zasněžená
půdě	půda	k1gFnSc6	půda
<g/>
,	,	kIx,	,
další	další	k2eAgMnSc1d1	další
chová	chovat	k5eAaImIp3nS	chovat
snůšku	snůška	k1gFnSc4	snůška
na	na	k7c6	na
travnatém	travnatý	k2eAgInSc6d1	travnatý
terénu	terén	k1gInSc6	terén
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
poněkud	poněkud	k6eAd1	poněkud
odlišných	odlišný	k2eAgFnPc6d1	odlišná
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
páry	pár	k1gInPc1	pár
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
nacházet	nacházet	k5eAaImF	nacházet
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
kilometrů	kilometr	k1gInPc2	kilometr
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
<g/>
.	.	kIx.	.
</s>
<s>
Hnízda	hnízdo	k1gNnPc1	hnízdo
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
nadmořských	nadmořský	k2eAgFnPc6d1	nadmořská
výškách	výška	k1gFnPc6	výška
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
115	[number]	k4	115
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
nedojde	dojít	k5eNaPmIp3nS	dojít
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
zaplavení	zaplavení	k1gNnSc3	zaplavení
<g/>
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgFnPc1d1	typická
pro	pro	k7c4	pro
ně	on	k3xPp3gFnPc4	on
jsou	být	k5eAaImIp3nP	být
oblázkové	oblázkový	k2eAgFnPc4d1	oblázková
pláže	pláž	k1gFnPc4	pláž
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterých	který	k3yQgFnPc2	který
si	se	k3xPyFc3	se
v	v	k7c6	v
zobáku	zobák	k1gInSc6	zobák
přenášejí	přenášet	k5eAaImIp3nP	přenášet
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
kamínky	kamínek	k1gInPc1	kamínek
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
asi	asi	k9	asi
pěti	pět	k4xCc2	pět
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
,	,	kIx,	,
a	a	k8xC	a
ty	ten	k3xDgFnPc1	ten
pokládají	pokládat	k5eAaImIp3nP	pokládat
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
hnízdo	hnízdo	k1gNnSc4	hnízdo
<g/>
.	.	kIx.	.
<g/>
Dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
nejrychlejší	rychlý	k2eAgMnPc4d3	nejrychlejší
zástupce	zástupce	k1gMnPc4	zástupce
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
<g/>
;	;	kIx,	;
na	na	k7c6	na
základě	základ	k1gInSc6	základ
získaných	získaný	k2eAgInPc2d1	získaný
údajů	údaj	k1gInPc2	údaj
dokáže	dokázat	k5eAaPmIp3nS	dokázat
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
vyvinout	vyvinout	k5eAaPmF	vyvinout
rychlost	rychlost	k1gFnSc1	rychlost
blížící	blížící	k2eAgFnSc1d1	blížící
se	se	k3xPyFc4	se
k	k	k7c3	k
hranici	hranice	k1gFnSc3	hranice
40	[number]	k4	40
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
hod	hod	k1gInSc1	hod
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
různých	různý	k2eAgInPc2d1	různý
pramenů	pramen	k1gInPc2	pramen
27	[number]	k4	27
<g/>
–	–	k?	–
<g/>
36	[number]	k4	36
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
hod	hod	k1gInSc1	hod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podniká	podnikat	k5eAaImIp3nS	podnikat
menší	malý	k2eAgInPc4d2	menší
ponory	ponor	k1gInPc4	ponor
<g/>
;	;	kIx,	;
obvykle	obvykle	k6eAd1	obvykle
okolo	okolo	k7c2	okolo
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
setrvává	setrvávat	k5eAaImIp3nS	setrvávat
průměrně	průměrně	k6eAd1	průměrně
dvě	dva	k4xCgFnPc4	dva
minuty	minuta	k1gFnPc4	minuta
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
často	často	k6eAd1	často
jen	jen	k9	jen
necelou	celý	k2eNgFnSc4d1	necelá
minutu	minuta	k1gFnSc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Disponuje	disponovat	k5eAaBmIp3nS	disponovat
však	však	k9	však
natolik	natolik	k6eAd1	natolik
dobrou	dobrý	k2eAgFnSc7d1	dobrá
anatomickou	anatomický	k2eAgFnSc7d1	anatomická
stavbou	stavba	k1gFnSc7	stavba
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dokáže	dokázat	k5eAaPmIp3nS	dokázat
potopit	potopit	k5eAaPmF	potopit
až	až	k9	až
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
170	[number]	k4	170
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
údajně	údajně	k6eAd1	údajně
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Potrava	potrava	k1gFnSc1	potrava
===	===	k?	===
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
oslí	oslí	k2eAgMnSc1d1	oslí
je	být	k5eAaImIp3nS	být
masožravec	masožravec	k1gMnSc1	masožravec
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
především	především	k9	především
menšími	malý	k2eAgFnPc7d2	menší
rybami	ryba	k1gFnPc7	ryba
<g/>
,	,	kIx,	,
krilem	krilo	k1gNnSc7	krilo
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
krunýřovkou	krunýřovka	k1gFnSc7	krunýřovka
krillovou	krillův	k2eAgFnSc7d1	krillův
<g/>
)	)	kIx)	)
a	a	k8xC	a
případně	případně	k6eAd1	případně
hlavonožci	hlavonožec	k1gMnPc1	hlavonožec
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
ročního	roční	k2eAgNnSc2d1	roční
období	období	k1gNnSc2	období
a	a	k8xC	a
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
polohy	poloha	k1gFnSc2	poloha
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
;	;	kIx,	;
například	například	k6eAd1	například
v	v	k7c6	v
měsících	měsíc	k1gInPc6	měsíc
únor-březen	únorřezen	k2eAgMnSc1d1	únor-březen
tvoří	tvořit	k5eAaImIp3nP	tvořit
korýši	korýš	k1gMnPc1	korýš
pouze	pouze	k6eAd1	pouze
10	[number]	k4	10
%	%	kIx~	%
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
stravy	strava	k1gFnSc2	strava
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
počátkem	počátkem	k7c2	počátkem
června	červen	k1gInSc2	červen
se	se	k3xPyFc4	se
tímto	tento	k3xDgMnSc7	tento
členovcem	členovec	k1gMnSc7	členovec
živí	živit	k5eAaImIp3nS	živit
již	již	k6eAd1	již
ze	z	k7c2	z
75	[number]	k4	75
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
června	červen	k1gInSc2	červen
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
živí	živit	k5eAaImIp3nS	živit
výhradně	výhradně	k6eAd1	výhradně
(	(	kIx(	(
<g/>
z	z	k7c2	z
90	[number]	k4	90
%	%	kIx~	%
<g/>
)	)	kIx)	)
rybí	rybí	k2eAgFnSc7d1	rybí
stravou	strava	k1gFnSc7	strava
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
ledovkovitých	ledovkovitý	k2eAgFnPc2d1	ledovkovitý
(	(	kIx(	(
<g/>
Nototheniidae	Nototheniidae	k1gFnPc2	Nototheniidae
<g/>
)	)	kIx)	)
–	–	k?	–
například	například	k6eAd1	například
druhem	druh	k1gInSc7	druh
Lepidonotothen	Lepidonotothna	k1gFnPc2	Lepidonotothna
squamifrons	squamifronsa	k1gFnPc2	squamifronsa
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Variabilita	variabilita	k1gFnSc1	variabilita
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
lokalit	lokalita	k1gFnPc2	lokalita
je	být	k5eAaImIp3nS	být
však	však	k9	však
poměrně	poměrně	k6eAd1	poměrně
výrazná	výrazný	k2eAgFnSc1d1	výrazná
<g/>
;	;	kIx,	;
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Marion	Marion	k1gInSc1	Marion
se	se	k3xPyFc4	se
již	již	k6eAd1	již
v	v	k7c6	v
měsíci	měsíc	k1gInSc6	měsíc
březen	březen	k1gInSc4	březen
živí	živit	k5eAaImIp3nP	živit
korýši	korýš	k1gMnPc1	korýš
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
80	[number]	k4	80
%	%	kIx~	%
<g/>
,	,	kIx,	,
a	a	k8xC	a
kolonie	kolonie	k1gFnSc1	kolonie
na	na	k7c6	na
Antarktickém	antarktický	k2eAgInSc6d1	antarktický
poloostrově	poloostrov	k1gInSc6	poloostrov
se	se	k3xPyFc4	se
praktický	praktický	k2eAgMnSc1d1	praktický
celé	celý	k2eAgNnSc4d1	celé
období	období	k1gNnSc4	období
chovu	chov	k1gInSc2	chov
živí	živit	k5eAaImIp3nP	živit
zejména	zejména	k9	zejména
krilem	kril	k1gInSc7	kril
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
severnějších	severní	k2eAgFnPc6d2	severnější
vodách	voda	k1gFnPc6	voda
živí	živit	k5eAaImIp3nP	živit
tučňáci	tučňák	k1gMnPc1	tučňák
převážně	převážně	k6eAd1	převážně
rybí	rybí	k2eAgFnSc7d1	rybí
stravou	strava	k1gFnSc7	strava
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
částí	část	k1gFnSc7	část
jejich	jejich	k3xOp3gInSc2	jejich
výskytu	výskyt	k1gInSc2	výskyt
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc1	jejich
potrava	potrava	k1gFnSc1	potrava
složena	složit	k5eAaPmNgFnS	složit
především	především	k6eAd1	především
z	z	k7c2	z
korýšů	korýš	k1gMnPc2	korýš
<g/>
.	.	kIx.	.
</s>
<s>
Sezónní	sezónní	k2eAgFnSc4d1	sezónní
změnu	změna	k1gFnSc4	změna
potravy	potrava	k1gFnSc2	potrava
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
hned	hned	k6eAd1	hned
několik	několik	k4yIc1	několik
faktorů	faktor	k1gInPc2	faktor
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
přítomnost	přítomnost	k1gFnSc1	přítomnost
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
tučňáků	tučňák	k1gMnPc2	tučňák
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
hnízdní	hnízdní	k2eAgFnPc4d1	hnízdní
migrace	migrace	k1gFnPc4	migrace
ptáků	pták	k1gMnPc2	pták
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
taktéž	taktéž	k?	taktéž
hloubka	hloubka	k1gFnSc1	hloubka
<g/>
,	,	kIx,	,
v	v	k7c6	v
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
kořist	kořist	k1gFnSc1	kořist
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
kořist	kořist	k1gFnSc1	kořist
nebývá	bývat	k5eNaImIp3nS	bývat
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
15	[number]	k4	15
centimetrů	centimetr	k1gInPc2	centimetr
a	a	k8xC	a
loví	lovit	k5eAaImIp3nP	lovit
v	v	k7c6	v
mořích	moře	k1gNnPc6	moře
poblíž	poblíž	k7c2	poblíž
svého	svůj	k3xOyFgNnSc2	svůj
hnízdiště	hnízdiště	k1gNnSc2	hnízdiště
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterého	který	k3yRgNnSc2	který
se	se	k3xPyFc4	se
jen	jen	k9	jen
zřídka	zřídka	k6eAd1	zřídka
vzdaluje	vzdalovat	k5eAaImIp3nS	vzdalovat
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
se	se	k3xPyFc4	se
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
asi	asi	k9	asi
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
hodin	hodina	k1gFnPc2	hodina
denně	denně	k6eAd1	denně
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
uplynutí	uplynutí	k1gNnSc6	uplynutí
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
zpátky	zpátky	k6eAd1	zpátky
na	na	k7c4	na
souš	souš	k1gFnSc4	souš
<g/>
,	,	kIx,	,
nakrmit	nakrmit	k5eAaPmF	nakrmit
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
===	===	k?	===
</s>
</p>
<p>
<s>
Rozmnožování	rozmnožování	k1gNnSc1	rozmnožování
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
je	být	k5eAaImIp3nS	být
sezónní	sezónní	k2eAgInSc4d1	sezónní
<g/>
;	;	kIx,	;
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
lokalitě	lokalita	k1gFnSc6	lokalita
nastává	nastávat	k5eAaImIp3nS	nastávat
počátkem	počátkem	k7c2	počátkem
května	květen	k1gInSc2	květen
až	až	k8xS	až
září	září	k1gNnSc2	září
<g/>
,	,	kIx,	,
a	a	k8xC	a
vrcholí	vrcholit	k5eAaImIp3nS	vrcholit
koncem	koncem	k7c2	koncem
října	říjen	k1gInSc2	říjen
až	až	k8xS	až
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
kolonie	kolonie	k1gFnPc1	kolonie
mají	mít	k5eAaImIp3nP	mít
totiž	totiž	k9	totiž
odlišné	odlišný	k2eAgFnPc1d1	odlišná
podmínky	podmínka	k1gFnPc1	podmínka
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
dříve	dříve	k6eAd2	dříve
začíná	začínat	k5eAaImIp3nS	začínat
hnízdit	hnízdit	k5eAaImF	hnízdit
severněji	severně	k6eAd2	severně
hnízdící	hnízdící	k2eAgFnSc1d1	hnízdící
subspecie	subspecie	k1gFnSc1	subspecie
(	(	kIx(	(
<g/>
Pygoscelis	Pygoscelis	k1gFnSc1	Pygoscelis
papua	papua	k1gFnSc1	papua
papua	papua	k1gFnSc1	papua
<g/>
)	)	kIx)	)
a	a	k8xC	a
o	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
jižněji	jižně	k6eAd2	jižně
hnízdící	hnízdící	k2eAgFnSc1d1	hnízdící
subspecie	subspecie	k1gFnSc1	subspecie
(	(	kIx(	(
<g/>
Pygoscelis	Pygoscelis	k1gFnSc1	Pygoscelis
papua	papu	k1gInSc2	papu
ellsworthi	ellsworth	k1gFnSc2	ellsworth
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
oslí	oslí	k2eAgMnSc1d1	oslí
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
pářit	pářit	k5eAaImF	pářit
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
reprodukci	reprodukce	k1gFnSc6	reprodukce
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
bývá	bývat	k5eAaImIp3nS	bývat
obvykle	obvykle	k6eAd1	obvykle
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
až	až	k8xS	až
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prvotní	prvotní	k2eAgFnSc7d1	prvotní
úlohou	úloha	k1gFnSc7	úloha
samce	samec	k1gInSc2	samec
je	být	k5eAaImIp3nS	být
naleznout	naleznout	k5eAaPmF	naleznout
vyhovující	vyhovující	k2eAgNnSc4d1	vyhovující
místo	místo	k1gNnSc4	místo
k	k	k7c3	k
vybudování	vybudování	k1gNnSc3	vybudování
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
samotné	samotný	k2eAgFnSc3d1	samotná
realizaci	realizace	k1gFnSc3	realizace
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
k	k	k7c3	k
úpravě	úprava	k1gFnSc3	úprava
hnízda	hnízdo	k1gNnSc2	hnízdo
využitého	využitý	k2eAgNnSc2d1	využité
již	již	k6eAd1	již
předešlou	předešlý	k2eAgFnSc4d1	předešlá
sezónu	sezóna	k1gFnSc4	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Fixace	fixace	k1gFnSc1	fixace
páru	pár	k1gInSc2	pár
na	na	k7c4	na
první	první	k4xOgFnSc4	první
spolu	spolu	k6eAd1	spolu
tvořené	tvořený	k2eAgNnSc1d1	tvořené
hnízdo	hnízdo	k1gNnSc1	hnízdo
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
běžná	běžný	k2eAgFnSc1d1	běžná
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
stavebním	stavební	k2eAgInSc7d1	stavební
kamenem	kámen	k1gInSc7	kámen
jsou	být	k5eAaImIp3nP	být
oblázky	oblázek	k1gInPc4	oblázek
z	z	k7c2	z
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
si	se	k3xPyFc3	se
tučňák	tučňák	k1gMnSc1	tučňák
kolikrát	kolikrát	k6eAd1	kolikrát
pečlivě	pečlivě	k6eAd1	pečlivě
vybírá	vybírat	k5eAaImIp3nS	vybírat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
stálého	stálý	k2eAgNnSc2d1	stálé
zvelebování	zvelebování	k1gNnSc2	zvelebování
může	moct	k5eAaImIp3nS	moct
jedno	jeden	k4xCgNnSc4	jeden
takové	takový	k3xDgNnSc4	takový
hnízdo	hnízdo	k1gNnSc4	hnízdo
obsahovat	obsahovat	k5eAaImF	obsahovat
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
tisíc	tisíc	k4xCgInSc4	tisíc
podobně	podobně	k6eAd1	podobně
velkých	velký	k2eAgInPc2d1	velký
kamínků	kamínek	k1gInPc2	kamínek
<g/>
.	.	kIx.	.
</s>
<s>
Navršené	navršený	k2eAgNnSc1d1	navršené
množství	množství	k1gNnSc1	množství
pak	pak	k6eAd1	pak
tvoří	tvořit	k5eAaImIp3nS	tvořit
ohraničenou	ohraničený	k2eAgFnSc4d1	ohraničená
část	část	k1gFnSc4	část
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gFnSc6	jehož
prohlubni	prohlubeň	k1gFnSc6	prohlubeň
samice	samice	k1gFnSc2	samice
dobře	dobře	k6eAd1	dobře
uschová	uschovat	k5eAaPmIp3nS	uschovat
vejce	vejce	k1gNnSc1	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
páry	pár	k1gInPc1	pár
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nP	muset
mít	mít	k5eAaImF	mít
často	často	k6eAd1	často
na	na	k7c6	na
pozoru	pozor	k1gInSc6	pozor
před	před	k7c7	před
drzými	drzý	k2eAgMnPc7d1	drzý
zloději	zloděj	k1gMnPc7	zloděj
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
s	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
navštěvují	navštěvovat	k5eAaImIp3nP	navštěvovat
cizí	cizí	k2eAgNnPc1d1	cizí
hnízda	hnízdo	k1gNnPc1	hnízdo
a	a	k8xC	a
odnášejí	odnášet	k5eAaImIp3nP	odnášet
si	se	k3xPyFc3	se
jejich	jejich	k3xOp3gInSc4	jejich
obsah	obsah	k1gInSc4	obsah
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
si	se	k3xPyFc3	se
patřičně	patřičně	k6eAd1	patřičně
ulehčují	ulehčovat	k5eAaImIp3nP	ulehčovat
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
si	se	k3xPyFc3	se
nezadaný	zadaný	k2eNgMnSc1d1	nezadaný
samec	samec	k1gMnSc1	samec
hledá	hledat	k5eAaImIp3nS	hledat
družku	družka	k1gFnSc4	družka
<g/>
,	,	kIx,	,
vábit	vábit	k5eAaImF	vábit
ji	on	k3xPp3gFnSc4	on
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
svého	svůj	k3xOyFgNnSc2	svůj
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
;	;	kIx,	;
natáhne	natáhnout	k5eAaPmIp3nS	natáhnout
krk	krk	k1gInSc4	krk
<g/>
,	,	kIx,	,
vztyčí	vztyčit	k5eAaPmIp3nS	vztyčit
zobák	zobák	k1gInSc4	zobák
a	a	k8xC	a
třese	třást	k5eAaImIp3nS	třást
roztaženými	roztažený	k2eAgNnPc7d1	roztažené
křídly	křídlo	k1gNnPc7	křídlo
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
vydává	vydávat	k5eAaImIp3nS	vydávat
hlasité	hlasitý	k2eAgInPc4d1	hlasitý
hrdelní	hrdelní	k2eAgInPc4d1	hrdelní
tóny	tón	k1gInPc4	tón
<g/>
.	.	kIx.	.
</s>
<s>
Zaujme	zaujmout	k5eAaPmIp3nS	zaujmout
<g/>
-li	i	k?	-li
samec	samec	k1gMnSc1	samec
potenciální	potenciální	k2eAgFnSc4d1	potenciální
partnerku	partnerka	k1gFnSc4	partnerka
<g/>
,	,	kIx,	,
následuje	následovat	k5eAaImIp3nS	následovat
ho	on	k3xPp3gMnSc4	on
až	až	k6eAd1	až
k	k	k7c3	k
hnízdu	hnízdo	k1gNnSc3	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
přichází	přicházet	k5eAaImIp3nS	přicházet
fáze	fáze	k1gFnSc1	fáze
vzájemného	vzájemný	k2eAgNnSc2d1	vzájemné
poznávání	poznávání	k1gNnSc2	poznávání
a	a	k8xC	a
mezi	mezi	k7c4	mezi
charakteristické	charakteristický	k2eAgInPc4d1	charakteristický
znaky	znak	k1gInPc4	znak
uzavřeného	uzavřený	k2eAgInSc2d1	uzavřený
svazku	svazek	k1gInSc2	svazek
patří	patřit	k5eAaImIp3nS	patřit
společné	společný	k2eAgNnSc1d1	společné
uklánění	uklánění	k1gNnSc1	uklánění
či	či	k8xC	či
hlasité	hlasitý	k2eAgNnSc1d1	hlasité
troubení	troubení	k1gNnSc1	troubení
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
pět	pět	k4xCc4	pět
dní	den	k1gInPc2	den
po	po	k7c6	po
páření	páření	k1gNnSc6	páření
snese	snést	k5eAaPmIp3nS	snést
samice	samice	k1gFnSc1	samice
první	první	k4xOgNnPc1	první
vejce	vejce	k1gNnPc1	vejce
<g/>
,	,	kIx,	,
a	a	k8xC	a
druhé	druhý	k4xOgNnSc1	druhý
zpravidla	zpravidla	k6eAd1	zpravidla
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výjimečném	výjimečný	k2eAgInSc6d1	výjimečný
případě	případ	k1gInSc6	případ
položí	položit	k5eAaPmIp3nS	položit
samice	samice	k1gFnSc1	samice
pouze	pouze	k6eAd1	pouze
jedno	jeden	k4xCgNnSc1	jeden
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
případně	případně	k6eAd1	případně
až	až	k9	až
tři	tři	k4xCgNnPc4	tři
vejce	vejce	k1gNnPc4	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
nazelenalá	nazelenalý	k2eAgFnSc1d1	nazelenalá
a	a	k8xC	a
kulovitého	kulovitý	k2eAgInSc2d1	kulovitý
tvaru	tvar	k1gInSc2	tvar
o	o	k7c6	o
průměrné	průměrný	k2eAgFnSc6d1	průměrná
váze	váha	k1gFnSc6	váha
125	[number]	k4	125
gramů	gram	k1gInPc2	gram
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
trvá	trvat	k5eAaImIp3nS	trvat
okolo	okolo	k7c2	okolo
35	[number]	k4	35
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
líhnou	líhnout	k5eAaImIp3nP	líhnout
většinou	většinou	k6eAd1	většinou
současně	současně	k6eAd1	současně
<g/>
,	,	kIx,	,
navzdory	navzdory	k7c3	navzdory
odstupu	odstup	k1gInSc3	odstup
při	při	k7c6	při
snesení	snesení	k1gNnSc6	snesení
<g/>
,	,	kIx,	,
a	a	k8xC	a
každé	každý	k3xTgInPc4	každý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
váží	vážit	k5eAaImIp3nS	vážit
zhruba	zhruba	k6eAd1	zhruba
95	[number]	k4	95
gramů	gram	k1gInPc2	gram
<g/>
.	.	kIx.	.
</s>
<s>
Prvních	první	k4xOgFnPc2	první
asi	asi	k9	asi
27	[number]	k4	27
dnů	den	k1gInPc2	den
jsou	být	k5eAaImIp3nP	být
střežena	střežen	k2eAgNnPc1d1	střeženo
vždy	vždy	k6eAd1	vždy
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nastávající	nastávající	k1gFnSc6	nastávající
asi	asi	k9	asi
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
denní	denní	k2eAgFnSc4d1	denní
fázi	fáze	k1gFnSc4	fáze
se	se	k3xPyFc4	se
již	již	k6eAd1	již
volně	volně	k6eAd1	volně
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
po	po	k7c6	po
okolí	okolí	k1gNnSc6	okolí
a	a	k8xC	a
zdržují	zdržovat	k5eAaImIp3nP	zdržovat
se	se	k3xPyFc4	se
s	s	k7c7	s
jinými	jiný	k2eAgNnPc7d1	jiné
mláďaty	mládě	k1gNnPc7	mládě
v	v	k7c6	v
skupinkách	skupinka	k1gFnPc6	skupinka
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
je	být	k5eAaImIp3nS	být
oba	dva	k4xCgMnPc4	dva
rodiče	rodič	k1gMnPc4	rodič
chodí	chodit	k5eAaImIp3nS	chodit
nadále	nadále	k6eAd1	nadále
krmit	krmit	k5eAaImF	krmit
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
pozvolna	pozvolna	k6eAd1	pozvolna
úplně	úplně	k6eAd1	úplně
osamostatní	osamostatnit	k5eAaPmIp3nP	osamostatnit
a	a	k8xC	a
vstoupí	vstoupit	k5eAaPmIp3nP	vstoupit
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
již	jenž	k3xRgFnSc4	jenž
potravu	potrava	k1gFnSc4	potrava
shánějí	shánět	k5eAaImIp3nP	shánět
sama	sám	k3xTgFnSc1	sám
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
80	[number]	k4	80
až	až	k9	až
100	[number]	k4	100
dní	den	k1gInPc2	den
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
pár	pár	k4xCyI	pár
o	o	k7c4	o
snůšku	snůška	k1gFnSc4	snůška
přijde	přijít	k5eAaPmIp3nS	přijít
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
zahnízdit	zahnízdit	k5eAaPmF	zahnízdit
ještě	ještě	k9	ještě
tentýž	týž	k3xTgInSc4	týž
rok	rok	k1gInSc4	rok
znovu	znovu	k6eAd1	znovu
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
ale	ale	k9	ale
naruší	narušit	k5eAaPmIp3nS	narušit
–	–	k?	–
posune	posunout	k5eAaPmIp3nS	posunout
–	–	k?	–
jejich	jejich	k3xOp3gInSc4	jejich
rozmnožovací	rozmnožovací	k2eAgInSc4d1	rozmnožovací
cyklus	cyklus	k1gInSc4	cyklus
a	a	k8xC	a
následkem	následkem	k7c2	následkem
toho	ten	k3xDgNnSc2	ten
tomu	ten	k3xDgMnSc3	ten
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
v	v	k7c6	v
nadcházející	nadcházející	k2eAgFnSc6d1	nadcházející
sezóně	sezóna	k1gFnSc6	sezóna
zcela	zcela	k6eAd1	zcela
bez	bez	k7c2	bez
potomků	potomek	k1gMnPc2	potomek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgNnSc6	tento
nelehkém	lehký	k2eNgNnSc6d1	nelehké
období	období	k1gNnSc6	období
chovu	chov	k1gInSc2	chov
čeká	čekat	k5eAaImIp3nS	čekat
dospělé	dospělí	k1gMnPc4	dospělí
ještě	ještě	k9	ještě
obměna	obměna	k1gFnSc1	obměna
starého	starý	k2eAgNnSc2d1	staré
peří	peří	k1gNnSc2	peří
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
aby	aby	kYmCp3nP	aby
nabrali	nabrat	k5eAaPmAgMnP	nabrat
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
zotaví	zotavit	k5eAaPmIp3nS	zotavit
se	se	k3xPyFc4	se
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
chvíli	chvíle	k1gFnSc4	chvíle
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
o	o	k7c4	o
poměrně	poměrně	k6eAd1	poměrně
náročný	náročný	k2eAgInSc4d1	náročný
a	a	k8xC	a
nebezpečný	bezpečný	k2eNgInSc4d1	nebezpečný
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
v	v	k7c6	v
kterém	který	k3yRgInSc6	který
nemohou	moct	k5eNaImIp3nP	moct
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
nakrmit	nakrmit	k5eAaPmF	nakrmit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
nalézt	nalézt	k5eAaPmF	nalézt
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
útočiště	útočiště	k1gNnSc1	útočiště
<g/>
.	.	kIx.	.
</s>
<s>
Přepeření	přepeření	k1gNnSc1	přepeření
trvá	trvat	k5eAaImIp3nS	trvat
tři	tři	k4xCgInPc4	tři
až	až	k6eAd1	až
čtyři	čtyři	k4xCgInPc4	čtyři
týdny	týden	k1gInPc4	týden
(	(	kIx(	(
<g/>
průměrně	průměrně	k6eAd1	průměrně
asi	asi	k9	asi
25	[number]	k4	25
dní	den	k1gInPc2	den
<g/>
)	)	kIx)	)
a	a	k8xC	a
pták	pták	k1gMnSc1	pták
při	při	k7c6	při
něm	on	k3xPp3gInSc6	on
denně	denně	k6eAd1	denně
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
téměř	téměř	k6eAd1	téměř
čtvrt	čtvrt	k1gFnSc4	čtvrt
kilogramu	kilogram	k1gInSc2	kilogram
váhy	váha	k1gFnSc2	váha
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
celý	celý	k2eAgInSc4d1	celý
jeho	jeho	k3xOp3gInSc4	jeho
průběh	průběh	k1gInSc4	průběh
může	moct	k5eAaImIp3nS	moct
zchudnout	zchudnout	k5eAaPmF	zchudnout
až	až	k9	až
o	o	k7c4	o
pět	pět	k4xCc4	pět
kilogramů	kilogram	k1gInPc2	kilogram
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Predátoři	predátor	k1gMnPc1	predátor
a	a	k8xC	a
parazité	parazit	k1gMnPc1	parazit
===	===	k?	===
</s>
</p>
<p>
<s>
Predátoři	predátor	k1gMnPc1	predátor
představují	představovat	k5eAaImIp3nP	představovat
největší	veliký	k2eAgNnSc4d3	veliký
riziko	riziko	k1gNnSc4	riziko
pro	pro	k7c4	pro
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc7	jejich
příkladným	příkladný	k2eAgMnSc7d1	příkladný
nepřítelem	nepřítel	k1gMnSc7	nepřítel
je	být	k5eAaImIp3nS	být
chaluha	chaluha	k1gFnSc1	chaluha
subantraktická	subantraktický	k2eAgFnSc1d1	subantraktický
(	(	kIx(	(
<g/>
Stercorarius	Stercorarius	k1gMnSc1	Stercorarius
antarcticus	antarcticus	k1gMnSc1	antarcticus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
si	se	k3xPyFc3	se
v	v	k7c6	v
kolonii	kolonie	k1gFnSc6	kolonie
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
okrajové	okrajový	k2eAgNnSc1d1	okrajové
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
nezkušené	zkušený	k2eNgInPc1d1	nezkušený
páry	pár	k1gInPc1	pár
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
přístup	přístup	k1gInSc4	přístup
ke	k	k7c3	k
kořisti	kořist	k1gFnSc3	kořist
poměrně	poměrně	k6eAd1	poměrně
snadný	snadný	k2eAgInSc1d1	snadný
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
měsíc	měsíc	k1gInSc4	měsíc
mají	mít	k5eAaImIp3nP	mít
mláďata	mládě	k1gNnPc1	mládě
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
rodiče	rodič	k1gMnPc4	rodič
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
v	v	k7c6	v
nadcházející	nadcházející	k2eAgFnSc6d1	nadcházející
fázi	fáze	k1gFnSc6	fáze
chovu	chov	k1gInSc2	chov
nepřítomni	přítomen	k2eNgMnPc1d1	nepřítomen
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
rozmnožování	rozmnožování	k1gNnSc4	rozmnožování
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
brání	bránit	k5eAaImIp3nP	bránit
se	se	k3xPyFc4	se
mláďata	mládě	k1gNnPc1	mládě
alespoň	alespoň	k9	alespoň
takovým	takový	k3xDgInSc7	takový
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
vrstevníky	vrstevník	k1gMnPc7	vrstevník
tvoří	tvořit	k5eAaImIp3nP	tvořit
těsné	těsný	k2eAgFnPc1d1	těsná
skupinky	skupinka	k1gFnPc1	skupinka
<g/>
,	,	kIx,	,
takzvané	takzvaný	k2eAgFnPc1d1	takzvaná
školky	školka	k1gFnPc1	školka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
chaluhu	chaluha	k1gFnSc4	chaluha
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
obtížnější	obtížný	k2eAgMnSc1d2	obtížnější
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
mláďat	mládě	k1gNnPc2	mládě
odtáhnout	odtáhnout	k5eAaPmF	odtáhnout
a	a	k8xC	a
uštvat	uštvat	k5eAaPmF	uštvat
<g/>
,	,	kIx,	,
drží	držet	k5eAaImIp3nS	držet
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
pevně	pevně	k6eAd1	pevně
semknutého	semknutý	k2eAgInSc2d1	semknutý
houfu	houf	k1gInSc2	houf
(	(	kIx(	(
<g/>
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
období	období	k1gNnSc1	období
mládě	mládě	k1gNnSc4	mládě
není	být	k5eNaImIp3nS	být
již	již	k6eAd1	již
tak	tak	k6eAd1	tak
malé	malý	k2eAgNnSc1d1	malé
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
známými	známý	k2eAgMnPc7d1	známý
predátory	predátor	k1gMnPc7	predátor
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
jsou	být	k5eAaImIp3nP	být
<g/>
;	;	kIx,	;
chaluha	chaluha	k1gFnSc1	chaluha
antarktická	antarktický	k2eAgFnSc1d1	antarktická
(	(	kIx(	(
<g/>
Stercorarius	Stercorarius	k1gMnSc1	Stercorarius
maccormicki	maccormick	k1gFnSc2	maccormick
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chaluha	chaluha	k1gFnSc1	chaluha
chilská	chilský	k2eAgFnSc1d1	chilská
(	(	kIx(	(
<g/>
Stercorarius	Stercorarius	k1gMnSc1	Stercorarius
chilensis	chilensis	k1gFnSc2	chilensis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
racek	racek	k1gMnSc1	racek
jižní	jižní	k2eAgMnSc1d1	jižní
(	(	kIx(	(
<g/>
Larus	Larus	k1gMnSc1	Larus
dominicanus	dominicanus	k1gMnSc1	dominicanus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
nebo	nebo	k8xC	nebo
některé	některý	k3yIgInPc4	některý
druhy	druh	k1gInPc4	druh
sokolovitých	sokolovitý	k2eAgFnPc2d1	sokolovitý
(	(	kIx(	(
<g/>
Falconiformes	Falconiformesa	k1gFnPc2	Falconiformesa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
pobytu	pobyt	k1gInSc2	pobyt
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
představuje	představovat	k5eAaImIp3nS	představovat
hrozbu	hrozba	k1gFnSc4	hrozba
několik	několik	k4yIc1	několik
druhů	druh	k1gMnPc2	druh
ploutvonožců	ploutvonožec	k1gMnPc2	ploutvonožec
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
kytovců	kytovec	k1gMnPc2	kytovec
<g/>
.	.	kIx.	.
</s>
<s>
Častou	častý	k2eAgFnSc7d1	častá
obětí	oběť	k1gFnSc7	oběť
jsou	být	k5eAaImIp3nP	být
mladí	mladý	k2eAgMnPc1d1	mladý
nezkušení	zkušený	k2eNgMnPc1d1	nezkušený
ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
typické	typický	k2eAgMnPc4d1	typický
predátory	predátor	k1gMnPc4	predátor
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
tuleň	tuleň	k1gMnSc1	tuleň
leopardí	leopardí	k2eAgMnSc1d1	leopardí
(	(	kIx(	(
<g/>
Hydrurga	Hydrurga	k1gFnSc1	Hydrurga
leptonyx	leptonyx	k1gInSc1	leptonyx
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tuleň	tuleň	k1gMnSc1	tuleň
Weddellův	Weddellův	k2eAgMnSc1d1	Weddellův
(	(	kIx(	(
<g/>
Leptonychotes	Leptonychotes	k1gMnSc1	Leptonychotes
weddellii	weddellie	k1gFnSc4	weddellie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
druhů	druh	k1gInPc2	druh
lachtanů	lachtan	k1gMnPc2	lachtan
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Otaria	Otarium	k1gNnSc2	Otarium
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
třeba	třeba	k6eAd1	třeba
kosatka	kosatka	k1gFnSc1	kosatka
dravá	dravý	k2eAgFnSc1d1	dravá
(	(	kIx(	(
<g/>
Ornicus	Ornicus	k1gMnSc1	Ornicus
orca	orca	k1gMnSc1	orca
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInSc4d2	veliký
vliv	vliv	k1gInSc4	vliv
mají	mít	k5eAaImIp3nP	mít
tito	tento	k3xDgMnPc1	tento
predátoři	predátor	k1gMnPc1	predátor
v	v	k7c6	v
období	období	k1gNnSc6	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
tučňáci	tučňák	k1gMnPc1	tučňák
zdržují	zdržovat	k5eAaImIp3nP	zdržovat
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
souše	souš	k1gFnSc2	souš
<g/>
,	,	kIx,	,
a	a	k8xC	a
míří	mířit	k5eAaImIp3nS	mířit
<g/>
-li	i	k?	-li
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
dostatek	dostatek	k1gInSc4	dostatek
času	čas	k1gInSc2	čas
ani	ani	k8xC	ani
možností	možnost	k1gFnPc2	možnost
na	na	k7c4	na
únik	únik	k1gInSc4	únik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
případě	případ	k1gInSc6	případ
vrací	vracet	k5eAaImIp3nS	vracet
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
z	z	k7c2	z
moře	moře	k1gNnSc2	moře
na	na	k7c4	na
souš	souš	k1gFnSc4	souš
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
nakrmit	nakrmit	k5eAaPmF	nakrmit
mládě	mládě	k1gNnSc4	mládě
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nuceně	nuceně	k6eAd1	nuceně
riskují	riskovat	k5eAaBmIp3nP	riskovat
a	a	k8xC	a
vydávají	vydávat	k5eAaPmIp3nP	vydávat
se	se	k3xPyFc4	se
vstříc	vstříc	k7c3	vstříc
číhajícím	číhající	k2eAgMnPc3d1	číhající
savcům	savec	k1gMnPc3	savec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
se	se	k3xPyFc4	se
ptáci	pták	k1gMnPc1	pták
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
v	v	k7c6	v
semknutých	semknutý	k2eAgFnPc6d1	semknutá
skupinách	skupina	k1gFnPc6	skupina
<g/>
,	,	kIx,	,
v	v	k7c6	v
kterých	který	k3yQgInPc6	který
má	mít	k5eAaImIp3nS	mít
jednotlivec	jednotlivec	k1gMnSc1	jednotlivec
větší	veliký	k2eAgFnSc4d2	veliký
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
přežití	přežití	k1gNnSc4	přežití
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
obvyklé	obvyklý	k2eAgMnPc4d1	obvyklý
parazity	parazit	k1gMnPc4	parazit
(	(	kIx(	(
<g/>
ektoparazity	ektoparazit	k1gMnPc7	ektoparazit
<g/>
)	)	kIx)	)
tučňáků	tučňák	k1gMnPc2	tučňák
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nP	řadit
blechy	blecha	k1gFnPc1	blecha
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Parapsyllus	Parapsyllus	k1gInSc1	Parapsyllus
<g/>
,	,	kIx,	,
klíšťata	klíště	k1gNnPc1	klíště
(	(	kIx(	(
<g/>
především	především	k9	především
druh	druh	k1gInSc1	druh
Ixodes	Ixodesa	k1gFnPc2	Ixodesa
uriae	uriae	k1gFnPc2	uriae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
taktéž	taktéž	k?	taktéž
střevní	střevní	k2eAgMnPc1d1	střevní
parazité	parazit	k1gMnPc1	parazit
(	(	kIx(	(
<g/>
endoparaziti	endoparazit	k1gMnPc1	endoparazit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
Tetrabothrius	Tetrabothrius	k1gInSc1	Tetrabothrius
pauliani	paulian	k1gMnPc1	paulian
<g/>
,	,	kIx,	,
Tetrabothrius	Tetrabothrius	k1gMnSc1	Tetrabothrius
wrighti	wrighť	k1gFnSc2	wrighť
<g/>
,	,	kIx,	,
Parorchites	Parorchites	k1gMnSc1	Parorchites
zederi	zeder	k1gFnSc2	zeder
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
rod	rod	k1gInSc4	rod
Corynosoma	Corynosomum	k1gNnSc2	Corynosomum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ohrožení	ohrožení	k1gNnSc1	ohrožení
==	==	k?	==
</s>
</p>
<p>
<s>
Populace	populace	k1gFnSc1	populace
tučňáka	tučňák	k1gMnSc2	tučňák
oslího	oslí	k2eAgInSc2d1	oslí
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
stabilní	stabilní	k2eAgFnSc4d1	stabilní
a	a	k8xC	a
čítá	čítat	k5eAaImIp3nS	čítat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
620	[number]	k4	620
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
IUCN	IUCN	kA	IUCN
až	až	k9	až
774	[number]	k4	774
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
ptáků	pták	k1gMnPc2	pták
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
kolísá	kolísat	k5eAaImIp3nS	kolísat
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
populace	populace	k1gFnPc1	populace
ze	z	k7c2	z
subantarktických	subantarktický	k2eAgInPc2d1	subantarktický
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
naopak	naopak	k6eAd1	naopak
roste	růst	k5eAaImIp3nS	růst
(	(	kIx(	(
<g/>
obzvláště	obzvláště	k6eAd1	obzvláště
kolonie	kolonie	k1gFnPc1	kolonie
hnízdící	hnízdící	k2eAgFnPc1d1	hnízdící
na	na	k7c6	na
Antarktickém	antarktický	k2eAgInSc6d1	antarktický
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
jsou	být	k5eAaImIp3nP	být
některé	některý	k3yIgFnPc4	některý
změny	změna	k1gFnPc4	změna
konstantní	konstantní	k2eAgNnSc1d1	konstantní
až	až	k9	až
drastické	drastický	k2eAgNnSc1d1	drastické
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
populace	populace	k1gFnSc1	populace
tučňáků	tučňák	k1gMnPc2	tučňák
z	z	k7c2	z
Ptačího	ptačí	k2eAgInSc2d1	ptačí
ostrova	ostrov	k1gInSc2	ostrov
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Bird	Bird	k1gInSc1	Bird
Island	Island	k1gInSc1	Island
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
zmenšila	zmenšit	k5eAaPmAgFnS	zmenšit
o	o	k7c4	o
více	hodně	k6eAd2	hodně
než	než	k8xS	než
60	[number]	k4	60
%	%	kIx~	%
<g/>
,	,	kIx,	,
v	v	k7c6	v
červeném	červený	k2eAgInSc6d1	červený
seznamu	seznam	k1gInSc6	seznam
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
druhů	druh	k1gInPc2	druh
živočichů	živočich	k1gMnPc2	živočich
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
vedený	vedený	k2eAgInSc1d1	vedený
jako	jako	k8xS	jako
málo	málo	k6eAd1	málo
dotčený	dotčený	k2eAgInSc1d1	dotčený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Falklandské	Falklandský	k2eAgInPc1d1	Falklandský
ostrovy	ostrov	k1gInPc1	ostrov
jsou	být	k5eAaImIp3nP	být
prozatím	prozatím	k6eAd1	prozatím
jediným	jediný	k2eAgNnSc7d1	jediné
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tučňáci	tučňák	k1gMnPc1	tučňák
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
lidských	lidský	k2eAgNnPc2d1	lidské
sídel	sídlo	k1gNnPc2	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
zemědělství	zemědělství	k1gNnSc1	zemědělství
výrazně	výrazně	k6eAd1	výrazně
změnilo	změnit	k5eAaPmAgNnS	změnit
krajinu	krajina	k1gFnSc4	krajina
těchto	tento	k3xDgMnPc2	tento
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
ptáci	pták	k1gMnPc1	pták
se	se	k3xPyFc4	se
dokázali	dokázat	k5eAaPmAgMnP	dokázat
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
novým	nový	k2eAgFnPc3d1	nová
podmínkám	podmínka	k1gFnPc3	podmínka
<g/>
;	;	kIx,	;
nevadí	vadit	k5eNaImIp3nS	vadit
jim	on	k3xPp3gMnPc3	on
otevřené	otevřený	k2eAgFnPc4d1	otevřená
planiny	planina	k1gFnPc4	planina
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
ani	ani	k8xC	ani
přítomnost	přítomnost	k1gFnSc1	přítomnost
pasoucího	pasoucí	k2eAgInSc2d1	pasoucí
se	se	k3xPyFc4	se
dobytka	dobytek	k1gInSc2	dobytek
<g/>
.	.	kIx.	.
</s>
<s>
Zavedená	zavedený	k2eAgFnSc1d1	zavedená
doprava	doprava	k1gFnSc1	doprava
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
nárůstem	nárůst	k1gInSc7	nárůst
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
sice	sice	k8xC	sice
narušila	narušit	k5eAaPmAgFnS	narušit
jinak	jinak	k6eAd1	jinak
obvyklí	obvyklý	k2eAgMnPc1d1	obvyklý
běh	běh	k1gInSc1	běh
zdejší	zdejší	k2eAgFnSc2d1	zdejší
divoké	divoký	k2eAgFnSc2d1	divoká
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
provedená	provedený	k2eAgFnSc1d1	provedená
studie	studie	k1gFnSc1	studie
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c4	na
možný	možný	k2eAgInSc4d1	možný
negativní	negativní	k2eAgInSc4d1	negativní
dopad	dopad	k1gInSc4	dopad
neprokázala	prokázat	k5eNaPmAgFnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
stávající	stávající	k2eAgFnSc1d1	stávající
úroveň	úroveň	k1gFnSc1	úroveň
osídlení	osídlení	k1gNnSc2	osídlení
působila	působit	k5eAaImAgFnS	působit
na	na	k7c4	na
ptáky	pták	k1gMnPc4	pták
významně	významně	k6eAd1	významně
negativně	negativně	k6eAd1	negativně
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
ptáci	pták	k1gMnPc1	pták
dobře	dobře	k6eAd1	dobře
snáší	snášet	k5eAaImIp3nP	snášet
i	i	k9	i
přítomnost	přítomnost	k1gFnSc4	přítomnost
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
pakliže	pakliže	k8xS	pakliže
se	se	k3xPyFc4	se
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
v	v	k7c4	v
uctivé	uctivý	k2eAgFnPc4d1	uctivá
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
od	od	k7c2	od
jejich	jejich	k3xOp3gNnPc2	jejich
hnízd	hnízdo	k1gNnPc2	hnízdo
(	(	kIx(	(
<g/>
nejblíže	blízce	k6eAd3	blízce
asi	asi	k9	asi
do	do	k7c2	do
15	[number]	k4	15
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
sbírali	sbírat	k5eAaImAgMnP	sbírat
zdejší	zdejší	k2eAgMnPc1d1	zdejší
obyvatelé	obyvatel	k1gMnPc1	obyvatel
jejich	jejich	k3xOp3gNnSc2	jejich
vejce	vejce	k1gNnSc2	vejce
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
obživě	obživa	k1gFnSc3	obživa
ale	ale	k8xC	ale
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
na	na	k7c4	na
Falklandské	Falklandský	k2eAgInPc4d1	Falklandský
ostrovy	ostrov	k1gInPc4	ostrov
začali	začít	k5eAaPmAgMnP	začít
dovážet	dovážet	k5eAaImF	dovážet
vejce	vejce	k1gNnPc4	vejce
slepičí	slepičí	k2eAgNnPc4d1	slepičí
<g/>
,	,	kIx,	,
těchto	tento	k3xDgInPc2	tento
případů	případ	k1gInPc2	případ
rapidně	rapidně	k6eAd1	rapidně
ubylo	ubýt	k5eAaPmAgNnS	ubýt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
možnou	možný	k2eAgFnSc4d1	možná
hrozbu	hrozba	k1gFnSc4	hrozba
nebezpečnou	bezpečný	k2eNgFnSc4d1	nebezpečná
pro	pro	k7c4	pro
populaci	populace	k1gFnSc4	populace
těchto	tento	k3xDgMnPc2	tento
ptáků	pták	k1gMnPc2	pták
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
považuje	považovat	k5eAaImIp3nS	považovat
znečišťování	znečišťování	k1gNnSc1	znečišťování
moří	mořit	k5eAaImIp3nS	mořit
lidským	lidský	k2eAgInSc7d1	lidský
odpadem	odpad	k1gInSc7	odpad
nebo	nebo	k8xC	nebo
komerční	komerční	k2eAgInSc1d1	komerční
rybolov	rybolov	k1gInSc1	rybolov
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
může	moct	k5eAaImIp3nS	moct
výrazně	výrazně	k6eAd1	výrazně
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
dostupné	dostupný	k2eAgNnSc4d1	dostupné
množství	množství	k1gNnSc4	množství
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ochrana	ochrana	k1gFnSc1	ochrana
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
neprobíhají	probíhat	k5eNaImIp3nP	probíhat
žádná	žádný	k3yNgNnPc4	žádný
přísnější	přísný	k2eAgNnPc4d2	přísnější
opatření	opatření	k1gNnPc4	opatření
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
však	však	k9	však
několik	několik	k4yIc4	několik
doporučení	doporučení	k1gNnPc2	doporučení
a	a	k8xC	a
za	za	k7c4	za
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
nejpodstatnějších	podstatný	k2eAgInPc2d3	nejpodstatnější
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
stálé	stálý	k2eAgNnSc1d1	stálé
monitorování	monitorování	k1gNnSc1	monitorování
chovných	chovný	k2eAgFnPc2d1	chovná
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
nadále	nadále	k6eAd1	nadále
sledovat	sledovat	k5eAaImF	sledovat
možnou	možný	k2eAgFnSc4d1	možná
hrozbu	hrozba	k1gFnSc4	hrozba
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
turismu	turismus	k1gInSc2	turismus
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
omezovat	omezovat	k5eAaImF	omezovat
jeho	jeho	k3xOp3gFnSc7	jeho
výší	výše	k1gFnSc7	výše
v	v	k7c6	v
období	období	k1gNnSc6	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
<g/>
.	.	kIx.	.
</s>
<s>
Návštěvníkům	návštěvník	k1gMnPc3	návštěvník
Antarktického	antarktický	k2eAgInSc2d1	antarktický
poloostrova	poloostrov	k1gInSc2	poloostrov
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
nařízeno	nařídit	k5eAaPmNgNnS	nařídit
<g/>
,	,	kIx,	,
že	že	k8xS	že
lze	lze	k6eAd1	lze
k	k	k7c3	k
tučňákům	tučňák	k1gMnPc3	tučňák
přistoupit	přistoupit	k5eAaPmF	přistoupit
nejblíže	blízce	k6eAd3	blízce
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
pět	pět	k4xCc4	pět
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
počítat	počítat	k5eAaImF	počítat
s	s	k7c7	s
případnou	případný	k2eAgFnSc7d1	případná
predací	predace	k1gFnSc7	predace
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
invazivních	invazivní	k2eAgInPc2d1	invazivní
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
nastat	nastat	k5eAaPmF	nastat
především	především	k9	především
na	na	k7c4	na
lidmi	člověk	k1gMnPc7	člověk
osídlených	osídlený	k2eAgInPc6d1	osídlený
Falklandských	Falklandský	k2eAgInPc6d1	Falklandský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
ostrovy	ostrov	k1gInPc1	ostrov
smí	smět	k5eAaImIp3nP	smět
obývat	obývat	k5eAaImF	obývat
pouze	pouze	k6eAd1	pouze
výzkumníci	výzkumník	k1gMnPc1	výzkumník
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
mezinárodně	mezinárodně	k6eAd1	mezinárodně
chráněné	chráněný	k2eAgFnPc4d1	chráněná
oblasti	oblast	k1gFnPc4	oblast
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
označeny	označit	k5eAaPmNgInP	označit
za	za	k7c4	za
přírodní	přírodní	k2eAgFnSc4d1	přírodní
rezervaci	rezervace	k1gFnSc4	rezervace
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
ostrov	ostrov	k1gInSc1	ostrov
Maquaire	Maquair	k1gInSc5	Maquair
nebo	nebo	k8xC	nebo
ostrov	ostrov	k1gInSc4	ostrov
Marion	Marion	k1gInSc1	Marion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
jsou	být	k5eAaImIp3nP	být
některé	některý	k3yIgInPc1	některý
ostrovy	ostrov	k1gInPc1	ostrov
obydleny	obydlet	k5eAaPmNgInP	obydlet
pouze	pouze	k6eAd1	pouze
přechodně	přechodně	k6eAd1	přechodně
<g/>
,	,	kIx,	,
pokles	pokles	k1gInSc4	pokles
kolonií	kolonie	k1gFnPc2	kolonie
kupříkladu	kupříkladu	k6eAd1	kupříkladu
na	na	k7c6	na
Kerguelenech	Kerguelen	k1gInPc6	Kerguelen
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
přisuzován	přisuzovat	k5eAaImNgMnS	přisuzovat
rušivým	rušivý	k2eAgInPc3d1	rušivý
zásahům	zásah	k1gInPc3	zásah
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
V	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
nosí	nosit	k5eAaImIp3nS	nosit
název	název	k1gInSc1	název
Gentoo	Gentoo	k6eAd1	Gentoo
Penguin	Penguin	k2eAgInSc1d1	Penguin
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
je	být	k5eAaImIp3nS	být
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
i	i	k9	i
jedna	jeden	k4xCgNnPc1	jeden
GNU	gnu	k1gNnPc1	gnu
<g/>
/	/	kIx~	/
<g/>
Linuxová	linuxový	k2eAgFnSc1d1	linuxová
distribuce	distribuce	k1gFnSc1	distribuce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
oslí	oslí	k2eAgMnSc1d1	oslí
hraje	hrát	k5eAaImIp3nS	hrát
ústřední	ústřední	k2eAgFnSc4d1	ústřední
roli	role	k1gFnSc4	role
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
rodinné	rodinný	k2eAgFnSc6d1	rodinná
komedii	komedie	k1gFnSc6	komedie
Pan	Pan	k1gMnSc1	Pan
Popper	Popper	k1gMnSc1	Popper
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
tučňáci	tučňák	k1gMnPc1	tučňák
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Mr	Mr	k1gFnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Popper	Popper	k1gMnSc1	Popper
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Penguins	Penguinsa	k1gFnPc2	Penguinsa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
ztvárňuje	ztvárňovat	k5eAaImIp3nS	ztvárňovat
skupinku	skupinka	k1gFnSc4	skupinka
šesti	šest	k4xCc2	šest
tučňáků	tučňák	k1gMnPc2	tučňák
–	–	k?	–
Hlučouna	Hlučoun	k1gMnSc4	Hlučoun
<g/>
,	,	kIx,	,
Hryzouna	Hryzoun	k1gMnSc4	Hryzoun
<g/>
,	,	kIx,	,
Kapitána	kapitán	k1gMnSc4	kapitán
<g/>
,	,	kIx,	,
Pitomu	pitoma	k1gMnSc4	pitoma
<g/>
,	,	kIx,	,
Miláčka	miláček	k1gMnSc4	miláček
a	a	k8xC	a
Smraďocha	smraďoch	k1gMnSc4	smraďoch
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
namísto	namísto	k7c2	namísto
do	do	k7c2	do
Zoologické	zoologický	k2eAgFnSc2d1	zoologická
zahrady	zahrada	k1gFnSc2	zahrada
omylem	omylem	k6eAd1	omylem
dostanou	dostat	k5eAaPmIp3nP	dostat
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
úspěšného	úspěšný	k2eAgMnSc2d1	úspěšný
newyorského	newyorský	k2eAgMnSc2d1	newyorský
podnikatele	podnikatel	k1gMnSc2	podnikatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
tučňák	tučňák	k1gMnSc1	tučňák
oslí	oslí	k2eAgMnSc1d1	oslí
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Pygoscelis	Pygoscelis	k1gFnSc1	Pygoscelis
papua	papua	k1gMnSc1	papua
(	(	kIx(	(
<g/>
Gentoo	Gentoo	k1gMnSc1	Gentoo
penguin	penguin	k1gMnSc1	penguin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
animaldiversity	animaldiversit	k1gInPc1	animaldiversit
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Animal	animal	k1gMnSc1	animal
Diversity	Diversit	k1gInPc4	Diversit
Web	web	k1gInSc1	web
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2018	[number]	k4	2018
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
