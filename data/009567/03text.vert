<p>
<s>
Roleta	roleta	k1gFnSc1	roleta
je	být	k5eAaImIp3nS	být
stínící	stínící	k2eAgFnSc1d1	stínící
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
provedeních	provedení	k1gNnPc6	provedení
i	i	k8xC	i
zabezpečovací	zabezpečovací	k2eAgInSc4d1	zabezpečovací
prvek	prvek	k1gInSc4	prvek
umístěný	umístěný	k2eAgInSc4d1	umístěný
na	na	k7c6	na
okně	okno	k1gNnSc6	okno
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
variant	varianta	k1gFnPc2	varianta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rolety	roleta	k1gFnSc2	roleta
interiérové	interiérový	k2eAgFnSc2d1	interiérová
==	==	k?	==
</s>
</p>
<p>
<s>
Látkové	látkový	k2eAgFnPc1d1	látková
rolety	roleta	k1gFnPc1	roleta
-	-	kIx~	-
skládají	skládat	k5eAaImIp3nP	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
volně	volně	k6eAd1	volně
visící	visící	k2eAgFnSc2d1	visící
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
navíjí	navíjet	k5eAaImIp3nS	navíjet
na	na	k7c4	na
hřídel	hřídel	k1gFnSc4	hřídel
nad	nad	k7c7	nad
oknem	okno	k1gNnSc7	okno
<g/>
.	.	kIx.	.
</s>
<s>
Ovládá	ovládat	k5eAaImIp3nS	ovládat
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
řetízku	řetízek	k1gInSc2	řetízek
napojeného	napojený	k2eAgInSc2d1	napojený
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
hřídel	hřídel	k1gFnSc4	hřídel
<g/>
,	,	kIx,	,
elektromotorem	elektromotor	k1gInSc7	elektromotor
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
navíjí	navíjet	k5eAaImIp3nS	navíjet
pomocí	pomocí	k7c2	pomocí
pružiny	pružina	k1gFnSc2	pružina
umístěné	umístěný	k2eAgFnSc2d1	umístěná
v	v	k7c6	v
hřídeli	hřídel	k1gFnSc6	hřídel
<g/>
.	.	kIx.	.
</s>
<s>
Látka	látka	k1gFnSc1	látka
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
poloprůsvitná	poloprůsvitný	k2eAgFnSc1d1	poloprůsvitná
<g/>
,	,	kIx,	,
či	či	k8xC	či
neprůsvitná	průsvitný	k2eNgFnSc1d1	neprůsvitná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Římské	římský	k2eAgFnPc1d1	římská
rolety	roleta	k1gFnPc1	roleta
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
visící	visící	k2eAgFnSc7d1	visící
látkou	látka	k1gFnSc7	látka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
při	při	k7c6	při
vytahování	vytahování	k1gNnSc6	vytahování
vzhůru	vzhůru	k6eAd1	vzhůru
skládá	skládat	k5eAaImIp3nS	skládat
po	po	k7c6	po
obdélnících	obdélník	k1gInPc6	obdélník
<g/>
.	.	kIx.	.
</s>
<s>
Ovládání	ovládání	k1gNnSc1	ovládání
bývá	bývat	k5eAaImIp3nS	bývat
řešeno	řešit	k5eAaImNgNnS	řešit
řetízkem	řetízek	k1gInSc7	řetízek
nebo	nebo	k8xC	nebo
elektromotorem	elektromotor	k1gInSc7	elektromotor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zatemňovací	zatemňovací	k2eAgFnPc1d1	zatemňovací
rolety	roleta	k1gFnPc1	roleta
-	-	kIx~	-
látková	látkový	k2eAgFnSc1d1	látková
roleta	roleta	k1gFnSc1	roleta
vedená	vedený	k2eAgFnSc1d1	vedená
v	v	k7c6	v
kolejnicích	kolejnice	k1gFnPc6	kolejnice
uzpůsobená	uzpůsobený	k2eAgFnSc1d1	uzpůsobená
k	k	k7c3	k
úplnému	úplný	k2eAgNnSc3d1	úplné
zatemnění	zatemnění	k1gNnSc3	zatemnění
místnosti	místnost	k1gFnSc2	místnost
<g/>
.	.	kIx.	.
</s>
<s>
Kolejnice	kolejnice	k1gFnPc1	kolejnice
mají	mít	k5eAaImIp3nP	mít
hluboké	hluboký	k2eAgFnPc1d1	hluboká
vodicí	vodicí	k2eAgFnPc1d1	vodicí
kapsy	kapsa	k1gFnPc1	kapsa
a	a	k8xC	a
husté	hustý	k2eAgInPc1d1	hustý
kartáčky	kartáček	k1gInPc1	kartáček
k	k	k7c3	k
zamezení	zamezení	k1gNnSc3	zamezení
průniku	průnik	k1gInSc2	průnik
světla	světlo	k1gNnSc2	světlo
do	do	k7c2	do
místnosti	místnost	k1gFnSc2	místnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Duo	duo	k1gNnSc1	duo
rolety	roleta	k1gFnSc2	roleta
-	-	kIx~	-
tyto	tento	k3xDgFnPc1	tento
rolety	roleta	k1gFnPc1	roleta
jsou	být	k5eAaImIp3nP	být
specifické	specifický	k2eAgInPc4d1	specifický
nejen	nejen	k6eAd1	nejen
svým	svůj	k3xOyFgInSc7	svůj
vzhledem	vzhled	k1gInSc7	vzhled
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
svou	svůj	k3xOyFgFnSc7	svůj
funkčností	funkčnost	k1gFnSc7	funkčnost
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
okenní	okenní	k2eAgFnPc1d1	okenní
rolety	roleta	k1gFnPc1	roleta
kombinují	kombinovat	k5eAaImIp3nP	kombinovat
průhledné	průhledný	k2eAgInPc4d1	průhledný
pruhy	pruh	k1gInPc4	pruh
s	s	k7c7	s
pruhy	pruh	k1gInPc7	pruh
stínícími	stínící	k2eAgInPc7d1	stínící
<g/>
.	.	kIx.	.
</s>
<s>
Označovány	označován	k2eAgFnPc1d1	označována
jako	jako	k8xC	jako
duo	duo	k1gNnSc1	duo
rolety	roleta	k1gFnSc2	roleta
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzhledem	vzhled	k1gInSc7	vzhled
připomínají	připomínat	k5eAaImIp3nP	připomínat
rolety	roleta	k1gFnPc1	roleta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
funkčností	funkčnost	k1gFnSc7	funkčnost
žaluzie	žaluzie	k1gFnSc2	žaluzie
<g/>
.	.	kIx.	.
</s>
<s>
Těmto	tento	k3xDgFnPc3	tento
roletám	roleta	k1gFnPc3	roleta
se	se	k3xPyFc4	se
také	také	k6eAd1	také
říká	říkat	k5eAaImIp3nS	říkat
Rolety	roleta	k1gFnSc2	roleta
den	den	k1gInSc4	den
a	a	k8xC	a
noc	noc	k1gFnSc4	noc
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
Zebra	zebra	k1gFnSc1	zebra
rolety	roleta	k1gFnSc2	roleta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rolety	roleta	k1gFnSc2	roleta
exteriérové	exteriérový	k2eAgFnSc2d1	exteriérová
==	==	k?	==
</s>
</p>
<p>
<s>
Venkovní	venkovní	k2eAgFnPc1d1	venkovní
rolety	roleta	k1gFnPc1	roleta
se	se	k3xPyFc4	se
navíjejí	navíjet	k5eAaImIp3nP	navíjet
na	na	k7c4	na
hřídel	hřídel	k1gFnSc4	hřídel
umístěnou	umístěný	k2eAgFnSc7d1	umístěná
ve	v	k7c6	v
schránce	schránka	k1gFnSc6	schránka
umístěné	umístěný	k2eAgInPc1d1	umístěný
pod	pod	k7c7	pod
fasádou	fasáda	k1gFnSc7	fasáda
nebo	nebo	k8xC	nebo
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
<g/>
.	.	kIx.	.
</s>
<s>
Schránky	schránka	k1gFnPc1	schránka
pod	pod	k7c7	pod
fasádou	fasáda	k1gFnSc7	fasáda
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
speciálními	speciální	k2eAgFnPc7d1	speciální
překladovými	překladový	k2eAgFnPc7d1	překladová
tvárnicemi	tvárnice	k1gFnPc7	tvárnice
<g/>
,	,	kIx,	,
hliníkovým	hliníkový	k2eAgInSc7d1	hliníkový
plechem	plech	k1gInSc7	plech
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
plastové	plastový	k2eAgFnPc1d1	plastová
<g/>
.	.	kIx.	.
</s>
<s>
Schránky	schránka	k1gFnPc1	schránka
umístěné	umístěný	k2eAgFnPc1d1	umístěná
na	na	k7c6	na
fasádě	fasáda	k1gFnSc6	fasáda
bývají	bývat	k5eAaImIp3nP	bývat
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
z	z	k7c2	z
hliníkového	hliníkový	k2eAgInSc2d1	hliníkový
plechu	plech	k1gInSc2	plech
<g/>
.	.	kIx.	.
</s>
<s>
Předokenní	předokenní	k2eAgFnPc4d1	předokenní
rolety	roleta	k1gFnPc4	roleta
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ovládat	ovládat	k5eAaImF	ovládat
jak	jak	k6eAd1	jak
mechanicky	mechanicky	k6eAd1	mechanicky
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
motorově	motorově	k6eAd1	motorově
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hliníkové	hliníkový	k2eAgFnPc1d1	hliníková
-	-	kIx~	-
Sestavené	sestavený	k2eAgFnPc1d1	sestavená
z	z	k7c2	z
lamel	lamela	k1gFnPc2	lamela
z	z	k7c2	z
válcovaného	válcovaný	k2eAgInSc2d1	válcovaný
hliníkového	hliníkový	k2eAgInSc2d1	hliníkový
plechu	plech	k1gInSc2	plech
vyplněného	vyplněný	k2eAgInSc2d1	vyplněný
izolační	izolační	k2eAgFnSc7d1	izolační
pěnou	pěna	k1gFnSc7	pěna
<g/>
.	.	kIx.	.
</s>
<s>
Poskytují	poskytovat	k5eAaImIp3nP	poskytovat
téměř	téměř	k6eAd1	téměř
100	[number]	k4	100
<g/>
%	%	kIx~	%
zatemnění	zatemnění	k1gNnSc2	zatemnění
a	a	k8xC	a
fungují	fungovat	k5eAaImIp3nP	fungovat
jako	jako	k9	jako
tepelná	tepelný	k2eAgFnSc1d1	tepelná
a	a	k8xC	a
zvuková	zvukový	k2eAgFnSc1d1	zvuková
izolace	izolace	k1gFnSc1	izolace
<g/>
.	.	kIx.	.
</s>
<s>
Fungují	fungovat	k5eAaImIp3nP	fungovat
i	i	k9	i
jako	jako	k9	jako
zabezpečovací	zabezpečovací	k2eAgInSc1d1	zabezpečovací
prvek	prvek	k1gInSc1	prvek
pro	pro	k7c4	pro
byt	byt	k1gInSc4	byt
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dům	dům	k1gInSc1	dům
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
ochrana	ochrana	k1gFnSc1	ochrana
je	být	k5eAaImIp3nS	být
však	však	k9	však
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
rolet	roleta	k1gFnPc2	roleta
snadno	snadno	k6eAd1	snadno
prolomitelná	prolomitelný	k2eAgFnSc1d1	prolomitelná
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
odolnější	odolný	k2eAgFnPc1d2	odolnější
varianty	varianta	k1gFnPc1	varianta
s	s	k7c7	s
bezpečnostní	bezpečnostní	k2eAgFnSc7d1	bezpečnostní
atestací	atestace	k1gFnSc7	atestace
s	s	k7c7	s
lamelami	lamela	k1gFnPc7	lamela
z	z	k7c2	z
extrudovaného	extrudovaný	k2eAgInSc2d1	extrudovaný
hliníku	hliník	k1gInSc2	hliník
a	a	k8xC	a
bezpečnostními	bezpečnostní	k2eAgInPc7d1	bezpečnostní
prvky	prvek	k1gInPc7	prvek
proti	proti	k7c3	proti
vytržení	vytržení	k1gNnSc3	vytržení
a	a	k8xC	a
nadzdvihnutí	nadzdvihnutí	k1gNnSc3	nadzdvihnutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dřevěné	dřevěný	k2eAgNnSc1d1	dřevěné
-	-	kIx~	-
Kdysi	kdysi	k6eAd1	kdysi
nejrozšířenější	rozšířený	k2eAgInSc1d3	nejrozšířenější
typ	typ	k1gInSc1	typ
exteriérových	exteriérový	k2eAgFnPc2d1	exteriérová
rolet	roleta	k1gFnPc2	roleta
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
za	za	k7c4	za
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Republiky	republika	k1gFnPc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Roleta	roleta	k1gFnSc1	roleta
je	být	k5eAaImIp3nS	být
sestavena	sestavit	k5eAaPmNgFnS	sestavit
z	z	k7c2	z
dřevěných	dřevěný	k2eAgFnPc2d1	dřevěná
lamel	lamela	k1gFnPc2	lamela
spojených	spojený	k2eAgFnPc2d1	spojená
ocelovými	ocelový	k2eAgFnPc7d1	ocelová
nebo	nebo	k8xC	nebo
nerezovými	rezový	k2eNgFnPc7d1	nerezová
sponami	spona	k1gFnPc7	spona
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
částečném	částečný	k2eAgNnSc6d1	částečné
nadzdvihnutí	nadzdvihnutí	k1gNnSc6	nadzdvihnutí
rolety	roleta	k1gFnSc2	roleta
vzniknou	vzniknout	k5eAaPmIp3nP	vzniknout
mezi	mezi	k7c7	mezi
lamelami	lamela	k1gFnPc7	lamela
malé	malý	k2eAgFnPc1d1	malá
mezery	mezera	k1gFnPc1	mezera
umožňující	umožňující	k2eAgFnPc1d1	umožňující
provětrávání	provětrávání	k1gNnSc4	provětrávání
místnosti	místnost	k1gFnSc2	místnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Látkové	látkový	k2eAgInPc4d1	látkový
(	(	kIx(	(
<g/>
Screeny	Screen	k1gInPc4	Screen
<g/>
)	)	kIx)	)
-	-	kIx~	-
Látkové	látkový	k2eAgFnPc1d1	látková
rolety	roleta	k1gFnPc1	roleta
z	z	k7c2	z
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
odolají	odolat	k5eAaPmIp3nP	odolat
vnějším	vnější	k2eAgInPc3d1	vnější
vlivům	vliv	k1gInPc3	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Látka	látka	k1gFnSc1	látka
je	být	k5eAaImIp3nS	být
vedena	vést	k5eAaImNgFnS	vést
vodícími	vodící	k2eAgNnPc7d1	vodící
lanky	lanko	k1gNnPc7	lanko
nebo	nebo	k8xC	nebo
v	v	k7c6	v
kolejnicích	kolejnice	k1gFnPc6	kolejnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Žaluzie	žaluzie	k1gFnSc1	žaluzie
</s>
</p>
<p>
<s>
Markýza	Markýza	k1gFnSc1	Markýza
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Rolety	roleta	k1gFnPc1	roleta
den	dna	k1gFnPc2	dna
a	a	k8xC	a
noc	noc	k1gFnSc4	noc
na	na	k7c6	na
fexi	fex	k1gFnSc6	fex
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
