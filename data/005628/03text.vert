<s>
Irský	irský	k2eAgMnSc1d1	irský
vlkodav	vlkodav	k1gMnSc1	vlkodav
je	být	k5eAaImIp3nS	být
plemeno	plemeno	k1gNnSc4	plemeno
psa	pes	k1gMnSc2	pes
vzniklé	vzniklý	k2eAgFnSc3d1	vzniklá
zkřížením	zkřížení	k1gNnSc7	zkřížení
chrtů	chrt	k1gMnPc2	chrt
a	a	k8xC	a
dogovitých	dogovitý	k2eAgNnPc2d1	dogovitý
plemen	plemeno	k1gNnPc2	plemeno
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
klidný	klidný	k2eAgInSc1d1	klidný
a	a	k8xC	a
vyrovnaný	vyrovnaný	k2eAgMnSc1d1	vyrovnaný
pes	pes	k1gMnSc1	pes
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
určen	určit	k5eAaPmNgInS	určit
ke	k	k7c3	k
štvaní	štvaní	k1gNnSc3	štvaní
<g/>
,	,	kIx,	,
chytání	chytání	k1gNnSc3	chytání
a	a	k8xC	a
zabíjení	zabíjení	k1gNnSc4	zabíjení
vlků	vlk	k1gMnPc2	vlk
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
již	již	k6eAd1	již
napovídá	napovídat	k5eAaBmIp3nS	napovídat
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
zaměňován	zaměňován	k2eAgMnSc1d1	zaměňován
za	za	k7c4	za
deerhounda	deerhound	k1gMnSc4	deerhound
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgInSc3	jenž
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgInSc1d1	podobný
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
deerhounda	deerhound	k1gMnSc2	deerhound
je	být	k5eAaImIp3nS	být
irský	irský	k2eAgMnSc1d1	irský
vlkodav	vlkodav	k1gMnSc1	vlkodav
pes	pes	k1gMnSc1	pes
robustnější	robustní	k2eAgFnSc2d2	robustnější
postavy	postava	k1gFnSc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
těžší	těžký	k2eAgMnSc1d2	těžší
a	a	k8xC	a
hlavně	hlavně	k6eAd1	hlavně
vyšší	vysoký	k2eAgMnSc1d2	vyšší
(	(	kIx(	(
<g/>
průměrný	průměrný	k2eAgInSc1d1	průměrný
výška	výška	k1gFnSc1	výška
psů	pes	k1gMnPc2	pes
v	v	k7c6	v
kohoutku	kohoutek	k1gInSc6	kohoutek
je	být	k5eAaImIp3nS	být
85	[number]	k4	85
cm	cm	kA	cm
<g/>
,	,	kIx,	,
fen	fena	k1gFnPc2	fena
79	[number]	k4	79
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
extrémní	extrémní	k2eAgMnPc1d1	extrémní
jedinci	jedinec	k1gMnPc1	jedinec
mohou	moct	k5eAaImIp3nP	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
výšky	výška	k1gFnSc2	výška
přes	přes	k7c4	přes
95	[number]	k4	95
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
výšce	výška	k1gFnSc3	výška
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
nejvyšší	vysoký	k2eAgNnPc4d3	nejvyšší
známá	známý	k2eAgNnPc4d1	známé
psí	psí	k2eAgNnPc4d1	psí
plemena	plemeno	k1gNnPc4	plemeno
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
prvenství	prvenství	k1gNnSc1	prvenství
jedince	jedinec	k1gMnSc2	jedinec
drží	držet	k5eAaImIp3nS	držet
pes	pes	k1gMnSc1	pes
německé	německý	k2eAgFnSc2d1	německá
dogy	doga	k1gFnSc2	doga
(	(	kIx(	(
<g/>
112	[number]	k4	112
cm	cm	kA	cm
v	v	k7c6	v
kohoutku	kohoutek	k1gInSc6	kohoutek
a	a	k8xC	a
108	[number]	k4	108
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
vlkodav	vlkodav	k1gMnSc1	vlkodav
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
Česka	Česko	k1gNnSc2	Česko
dovezen	dovezen	k2eAgMnSc1d1	dovezen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlkodavové	vlkodav	k1gMnPc1	vlkodav
přišli	přijít	k5eAaPmAgMnP	přijít
do	do	k7c2	do
Irska	Irsko	k1gNnSc2	Irsko
s	s	k7c7	s
Kelty	Kelt	k1gMnPc7	Kelt
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
známí	známý	k2eAgMnPc1d1	známý
jako	jako	k8xC	jako
vynikající	vynikající	k2eAgMnPc1d1	vynikající
chovatelé	chovatel	k1gMnPc1	chovatel
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
domovině	domovina	k1gFnSc6	domovina
se	se	k3xPyFc4	se
odjakživa	odjakživa	k6eAd1	odjakživa
těšili	těšit	k5eAaImAgMnP	těšit
obrovské	obrovský	k2eAgFnSc3d1	obrovská
oblibě	obliba	k1gFnSc3	obliba
a	a	k8xC	a
vážnosti	vážnost	k1gFnSc3	vážnost
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dokazují	dokazovat	k5eAaImIp3nP	dokazovat
nesčetné	sčetný	k2eNgFnPc1d1	nesčetná
legendy	legenda	k1gFnPc1	legenda
a	a	k8xC	a
jiná	jiný	k2eAgNnPc1d1	jiné
literární	literární	k2eAgNnPc1d1	literární
díla	dílo	k1gNnPc1	dílo
opěvující	opěvující	k2eAgFnSc4d1	opěvující
tuto	tento	k3xDgFnSc4	tento
kouzelnou	kouzelný	k2eAgFnSc4d1	kouzelná
rasu	rasa	k1gFnSc4	rasa
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnictví	vlastnictví	k1gNnSc4	vlastnictví
těchto	tento	k3xDgMnPc2	tento
psů	pes	k1gMnPc2	pes
se	se	k3xPyFc4	se
vztahovalo	vztahovat	k5eAaImAgNnS	vztahovat
především	především	k9	především
na	na	k7c4	na
šlechtické	šlechtický	k2eAgInPc4d1	šlechtický
a	a	k8xC	a
královské	královský	k2eAgInPc4d1	královský
dvory	dvůr	k1gInPc4	dvůr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
klenot	klenot	k1gInSc4	klenot
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
dokazují	dokazovat	k5eAaImIp3nP	dokazovat
i	i	k8xC	i
obojky	obojek	k1gInPc1	obojek
ze	z	k7c2	z
zlata	zlato	k1gNnSc2	zlato
a	a	k8xC	a
drahých	drahý	k2eAgInPc2d1	drahý
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
zdobily	zdobit	k5eAaImAgInP	zdobit
jejich	jejich	k3xOp3gInPc1	jejich
krky	krk	k1gInPc1	krk
<g/>
.	.	kIx.	.
</s>
<s>
Vlkodav	vlkodav	k1gMnSc1	vlkodav
se	se	k3xPyFc4	se
také	také	k9	také
stal	stát	k5eAaPmAgInS	stát
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
darem	dar	k1gInSc7	dar
pro	pro	k7c4	pro
královské	královský	k2eAgFnPc4d1	královská
rodiny	rodina	k1gFnPc4	rodina
celé	celý	k2eAgFnSc2d1	celá
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
irském	irský	k2eAgMnSc6d1	irský
vlkodavovi	vlkodav	k1gMnSc6	vlkodav
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
391	[number]	k4	391
n.	n.	k?	n.
l.	l.	k?	l.
z	z	k7c2	z
dopisu	dopis	k1gInSc2	dopis
římského	římský	k2eAgMnSc2d1	římský
konzula	konzul	k1gMnSc2	konzul
Quinta	Quint	k1gMnSc2	Quint
Aurelia	Aurelius	k1gMnSc2	Aurelius
Symmacha	Symmach	k1gMnSc2	Symmach
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
děkuje	děkovat	k5eAaImIp3nS	děkovat
svému	svůj	k3xOyFgMnSc3	svůj
bratrovi	bratr	k1gMnSc3	bratr
za	za	k7c7	za
7	[number]	k4	7
irských	irský	k2eAgMnPc2d1	irský
vlkodavů	vlkodav	k1gMnPc2	vlkodav
se	s	k7c7	s
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Celý	celý	k2eAgInSc1d1	celý
Řím	Řím	k1gInSc1	Řím
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
hleděl	hledět	k5eAaImAgMnS	hledět
s	s	k7c7	s
obdivem	obdiv	k1gInSc7	obdiv
a	a	k8xC	a
okouzlením	okouzlení	k1gNnSc7	okouzlení
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
měl	mít	k5eAaImAgMnS	mít
královský	královský	k2eAgMnSc1d1	královský
pes	pes	k1gMnSc1	pes
hodnotu	hodnota	k1gFnSc4	hodnota
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
×	×	k?	×
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
kůň	kůň	k1gMnSc1	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Vlkodav	vlkodav	k1gMnSc1	vlkodav
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
také	také	k9	také
součástí	součást	k1gFnSc7	součást
znaku	znak	k1gInSc2	znak
irských	irský	k2eAgMnPc2d1	irský
králů	král	k1gMnPc2	král
(	(	kIx(	(
<g/>
vedle	vedle	k7c2	vedle
listu	list	k1gInSc2	list
jetele	jetel	k1gInSc2	jetel
a	a	k8xC	a
harfy	harfa	k1gFnSc2	harfa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
obsahoval	obsahovat	k5eAaImAgMnS	obsahovat
motto	motto	k1gNnSc4	motto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Milý	milý	k1gMnSc1	milý
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
hlazen	hlazen	k2eAgMnSc1d1	hlazen
<g/>
,	,	kIx,	,
divoký	divoký	k2eAgMnSc1d1	divoký
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
podrážděn	podrážděn	k2eAgMnSc1d1	podrážděn
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dějin	dějiny	k1gFnPc2	dějiny
se	se	k3xPyFc4	se
vlkodav	vlkodav	k1gMnSc1	vlkodav
stal	stát	k5eAaPmAgMnS	stát
natolik	natolik	k6eAd1	natolik
žádaným	žádaný	k2eAgInSc7d1	žádaný
artiklem	artikl	k1gInSc7	artikl
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
domovině	domovina	k1gFnSc6	domovina
vzácným	vzácný	k2eAgNnSc7d1	vzácné
<g/>
.	.	kIx.	.
</s>
<s>
Dovážen	dovážen	k2eAgInSc1d1	dovážen
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
Persie	Persie	k1gFnSc2	Persie
<g/>
,	,	kIx,	,
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
,	,	kIx,	,
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byl	být	k5eAaImAgMnS	být
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
lorda	lord	k1gMnSc2	lord
protektora	protektor	k1gMnSc2	protektor
O.	O.	kA	O.
Cromwella	Cromwell	k1gMnSc2	Cromwell
vývoz	vývoz	k1gInSc1	vývoz
vlkodavů	vlkodav	k1gMnPc2	vlkodav
z	z	k7c2	z
Irska	Irsko	k1gNnSc2	Irsko
a	a	k8xC	a
Anglie	Anglie	k1gFnSc2	Anglie
zakázán	zakázat	k5eAaPmNgInS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
vyhubení	vyhubení	k1gNnSc3	vyhubení
vlků	vlk	k1gMnPc2	vlk
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
a	a	k8xC	a
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
kvůli	kvůli	k7c3	kvůli
hladomoru	hladomor	k1gInSc3	hladomor
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1845	[number]	k4	1845
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vlkodav	vlkodav	k1gMnSc1	vlkodav
přepychovým	přepychový	k2eAgMnSc7d1	přepychový
"	"	kIx"	"
<g/>
zbožím	zboží	k1gNnSc7	zboží
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
poptávka	poptávka	k1gFnSc1	poptávka
po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
plemeni	plemeno	k1gNnSc6	plemeno
upadala	upadat	k5eAaPmAgFnS	upadat
a	a	k8xC	a
vlkodav	vlkodav	k1gMnSc1	vlkodav
sám	sám	k3xTgMnSc1	sám
byl	být	k5eAaImAgInS	být
ohrožen	ohrozit	k5eAaPmNgInS	ohrozit
vyhynutím	vyhynutí	k1gNnSc7	vyhynutí
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
naštěstí	naštěstí	k6eAd1	naštěstí
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
začal	začít	k5eAaPmAgMnS	začít
kapitán	kapitán	k1gMnSc1	kapitán
George	Georg	k1gInSc2	Georg
Graham	graham	k1gInSc4	graham
pokoušet	pokoušet	k5eAaImF	pokoušet
tuto	tento	k3xDgFnSc4	tento
rasu	rasa	k1gFnSc4	rasa
obnovit	obnovit	k5eAaPmF	obnovit
<g/>
.	.	kIx.	.
</s>
<s>
Graham	Graham	k1gMnSc1	Graham
byl	být	k5eAaImAgMnS	být
odborníkem	odborník	k1gMnSc7	odborník
v	v	k7c6	v
chovu	chov	k1gInSc6	chov
deerhoundů	deerhound	k1gInPc2	deerhound
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgMnS	být
právě	právě	k9	právě
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
obnovou	obnova	k1gFnSc7	obnova
započal	započnout	k5eAaPmAgMnS	započnout
<g/>
.	.	kIx.	.
</s>
<s>
Sestavil	sestavit	k5eAaPmAgMnS	sestavit
chovatelský	chovatelský	k2eAgInSc4d1	chovatelský
program	program	k1gInSc4	program
a	a	k8xC	a
vývoj	vývoj	k1gInSc4	vývoj
rasy	rasa	k1gFnSc2	rasa
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1886	[number]	k4	1886
objevoval	objevovat	k5eAaImAgInS	objevovat
standard	standard	k1gInSc1	standard
plemene	plemeno	k1gNnSc2	plemeno
a	a	k8xC	a
záchrana	záchrana	k1gFnSc1	záchrana
vlkodavů	vlkodav	k1gMnPc2	vlkodav
tím	ten	k3xDgNnSc7	ten
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
prototyp	prototyp	k1gInSc4	prototyp
plemene	plemeno	k1gNnSc2	plemeno
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
pes	pes	k1gMnSc1	pes
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Leary	Leara	k1gMnSc2	Leara
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
preparát	preparát	k1gInSc1	preparát
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
vystaven	vystavit	k5eAaPmNgInS	vystavit
v	v	k7c4	v
Museum	museum	k1gNnSc4	museum
of	of	k?	of
National	National	k1gFnSc1	National
History	Histor	k1gInPc1	Histor
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vlkodava	vlkodav	k1gMnSc4	vlkodav
spatřit	spatřit	k5eAaPmF	spatřit
například	například	k6eAd1	například
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
dudáků	dudák	k1gMnPc2	dudák
a	a	k8xC	a
bubeníků	bubeník	k1gMnPc2	bubeník
při	při	k7c6	při
střídání	střídání	k1gNnSc6	střídání
stráží	stráž	k1gFnPc2	stráž
v	v	k7c6	v
Buckinghamském	buckinghamský	k2eAgInSc6d1	buckinghamský
paláci	palác	k1gInSc6	palác
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Fotografie	fotografia	k1gFnPc1	fotografia
George	Georg	k1gInSc2	Georg
Graham	graham	k1gInSc1	graham
-	-	kIx~	-
Anglický	anglický	k2eAgMnSc1d1	anglický
kapitán	kapitán	k1gMnSc1	kapitán
<g/>
,	,	kIx,	,
chovatel	chovatel	k1gMnSc1	chovatel
deerhoundů	deerhound	k1gMnPc2	deerhound
a	a	k8xC	a
vlkodavů	vlkodav	k1gMnPc2	vlkodav
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
obnovil	obnovit	k5eAaPmAgInS	obnovit
rasu	rasa	k1gFnSc4	rasa
irského	irský	k2eAgMnSc2d1	irský
vlkodava	vlkodav	k1gMnSc2	vlkodav
v	v	k7c6	v
r.	r.	kA	r.
1886	[number]	k4	1886
<g/>
.	.	kIx.	.
</s>
<s>
Lady	lady	k1gFnSc1	lady
Talbot	Talbota	k1gFnPc2	Talbota
of	of	k?	of
Malahide	Malahid	k1gInSc5	Malahid
-	-	kIx~	-
Darovala	darovat	k5eAaPmAgFnS	darovat
irského	irský	k2eAgMnSc4d1	irský
vlkodava	vlkodav	k1gMnSc4	vlkodav
anglickému	anglický	k2eAgMnSc3d1	anglický
králi	král	k1gMnSc3	král
Jiřímu	Jiří	k1gMnSc3	Jiří
V.	V.	kA	V.
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Irský	irský	k2eAgMnSc1d1	irský
vlkodav	vlkodav	k1gMnSc1	vlkodav
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
WDK	WDK	kA	WDK
-	-	kIx~	-
Wolf	Wolf	k1gMnSc1	Wolf
&	&	k?	&
deer	deer	k1gInSc1	deer
hound	hound	k1gMnSc1	hound
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
chovatelé	chovatel	k1gMnPc1	chovatel
plemen	plemeno	k1gNnPc2	plemeno
irský	irský	k2eAgMnSc1d1	irský
vlkodav	vlkodav	k1gMnSc1	vlkodav
a	a	k8xC	a
deerhound	deerhound	k1gMnSc1	deerhound
</s>
