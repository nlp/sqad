<s>
Soča	Soča	k1gFnSc1	Soča
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
Isonzo	Isonza	k1gFnSc5	Isonza
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
a	a	k8xC	a
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
(	(	kIx(	(
<g/>
oblast	oblast	k1gFnSc1	oblast
Furlansko-Julské	Furlansko-Julský	k2eAgNnSc1d1	Furlansko-Julské
Benátsko	Benátsko	k1gNnSc1	Benátsko
-	-	kIx~	-
Provincie	provincie	k1gFnSc1	provincie
Gorizia	Gorizia	k1gFnSc1	Gorizia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
136	[number]	k4	136
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
Povodí	povodí	k1gNnSc1	povodí
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
3	[number]	k4	3
500	[number]	k4	500
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Pramení	pramenit	k5eAaImIp3nP	pramenit
v	v	k7c6	v
Julských	Julský	k2eAgFnPc6d1	Julský
Alpách	Alpy	k1gFnPc6	Alpy
nedaleko	nedaleko	k7c2	nedaleko
obce	obec	k1gFnSc2	obec
Trenta	Trento	k1gNnSc2	Trento
pod	pod	k7c7	pod
horou	hora	k1gFnSc7	hora
Jalovec	jalovec	k1gInSc1	jalovec
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
města	město	k1gNnSc2	město
Gorica	Goric	k1gInSc2	Goric
protéká	protékat	k5eAaImIp3nS	protékat
přes	přes	k7c4	přes
státní	státní	k2eAgFnSc4d1	státní
hranici	hranice	k1gFnSc4	hranice
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
také	také	k9	také
do	do	k7c2	do
Benátské	benátský	k2eAgFnSc2d1	Benátská
nížiny	nížina	k1gFnSc2	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Ústí	ústí	k1gNnSc1	ústí
do	do	k7c2	do
Terstského	terstský	k2eAgInSc2d1	terstský
zálivu	záliv	k1gInSc2	záliv
Jaderského	jaderský	k2eAgNnSc2d1	Jaderské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
sněhový	sněhový	k2eAgInSc1d1	sněhový
a	a	k8xC	a
dešťový	dešťový	k2eAgInSc1d1	dešťový
<g/>
.	.	kIx.	.
</s>
<s>
Vodnatost	vodnatost	k1gFnSc1	vodnatost
řeky	řeka	k1gFnSc2	řeka
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
a	a	k8xC	a
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
<g/>
,	,	kIx,	,
nejmenší	malý	k2eAgInSc4d3	nejmenší
naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
a	a	k8xC	a
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
průtok	průtok	k1gInSc1	průtok
vody	voda	k1gFnSc2	voda
činí	činit	k5eAaImIp3nS	činit
135	[number]	k4	135
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
několik	několik	k4yIc1	několik
vodních	vodní	k2eAgFnPc2d1	vodní
elektráren	elektrárna	k1gFnPc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
dolní	dolní	k2eAgFnSc4d1	dolní
toku	toka	k1gFnSc4	toka
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
vodní	vodní	k2eAgFnSc1d1	vodní
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Peřeje	peřej	k1gFnPc1	peřej
na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
u	u	k7c2	u
Bovce	Bovce	k1gFnSc2	Bovce
jsou	být	k5eAaImIp3nP	být
vyhledávané	vyhledávaný	k2eAgMnPc4d1	vyhledávaný
vodáky	vodák	k1gMnPc4	vodák
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
několik	několik	k4yIc4	několik
částí	část	k1gFnPc2	část
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
obtížností	obtížnost	k1gFnSc7	obtížnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bovci	Bovce	k1gFnSc6	Bovce
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
půjčoven	půjčovna	k1gFnPc2	půjčovna
vodáckého	vodácký	k2eAgNnSc2d1	vodácké
vybavení	vybavení	k1gNnSc2	vybavení
a	a	k8xC	a
provozuje	provozovat	k5eAaImIp3nS	provozovat
se	se	k3xPyFc4	se
tu	ten	k3xDgFnSc4	ten
komerční	komerční	k2eAgInSc1d1	komerční
rafting	rafting	k1gInSc1	rafting
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Bitvy	bitva	k1gFnSc2	bitva
na	na	k7c6	na
Soči	Soči	k1gNnSc6	Soči
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1915	[number]	k4	1915
<g/>
-	-	kIx~	-
<g/>
17	[number]	k4	17
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
řeky	řeka	k1gFnSc2	řeka
k	k	k7c3	k
těžkým	těžký	k2eAgInPc3d1	těžký
bojům	boj	k1gInPc3	boj
mezi	mezi	k7c7	mezi
italským	italský	k2eAgInSc7d1	italský
a	a	k8xC	a
rakousko-uherským	rakouskoherský	k2eAgInSc7d1	rakousko-uherský
<g/>
/	/	kIx~	/
<g/>
německým	německý	k2eAgNnSc7d1	německé
vojskem	vojsko	k1gNnSc7	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
12	[number]	k4	12
bitvám	bitva	k1gFnPc3	bitva
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
se	se	k3xPyFc4	se
italským	italský	k2eAgNnPc3d1	italské
vojskům	vojsko	k1gNnPc3	vojsko
nepodařilo	podařit	k5eNaPmAgNnS	podařit
prorazit	prorazit	k5eAaPmF	prorazit
obranu	obrana	k1gFnSc4	obrana
protivníka	protivník	k1gMnSc2	protivník
<g/>
.	.	kIx.	.
</s>
<s>
Sočské	sočský	k2eAgInPc1d1	sočský
boje	boj	k1gInPc1	boj
zaznamenaly	zaznamenat	k5eAaPmAgInP	zaznamenat
jen	jen	k9	jen
malé	malý	k2eAgInPc1d1	malý
pohyby	pohyb	k1gInPc1	pohyb
frontové	frontový	k2eAgFnSc2d1	frontová
linie	linie	k1gFnSc2	linie
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
významný	významný	k2eAgInSc4d1	významný
podíl	podíl	k1gInSc4	podíl
na	na	k7c4	na
udržení	udržení	k1gNnSc4	udržení
obrany	obrana	k1gFnSc2	obrana
proti	proti	k7c3	proti
italským	italský	k2eAgInPc3d1	italský
útokům	útok	k1gInPc3	útok
měly	mít	k5eAaImAgFnP	mít
jednotky	jednotka	k1gFnPc4	jednotka
z	z	k7c2	z
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
tu	tu	k6eAd1	tu
prokázaly	prokázat	k5eAaPmAgFnP	prokázat
velké	velký	k2eAgNnSc4d1	velké
hrdinství	hrdinství	k1gNnSc4	hrdinství
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
po	po	k7c6	po
ofenzivě	ofenziva	k1gFnSc6	ofenziva
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1917	[number]	k4	1917
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
německým	německý	k2eAgFnPc3d1	německá
posilám	posila	k1gFnPc3	posila
podařilo	podařit	k5eAaPmAgNnS	podařit
prolomit	prolomit	k5eAaPmF	prolomit
frontu	fronta	k1gFnSc4	fronta
a	a	k8xC	a
zahnat	zahnat	k5eAaPmF	zahnat
italské	italský	k2eAgNnSc4d1	italské
vojsko	vojsko	k1gNnSc4	vojsko
sto	sto	k4xCgNnSc4	sto
kilometrů	kilometr	k1gInPc2	kilometr
na	na	k7c4	na
západ	západ	k1gInSc4	západ
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
Piavě	Piava	k1gFnSc3	Piava
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
Letopisy	letopis	k1gInPc4	letopis
Narnie	Narnie	k1gFnSc1	Narnie
<g/>
:	:	kIx,	:
Princ	princ	k1gMnSc1	princ
Kaspian	Kaspiana	k1gFnPc2	Kaspiana
je	být	k5eAaImIp3nS	být
využita	využít	k5eAaPmNgFnS	využít
ke	k	k7c3	k
ztvárnění	ztvárnění	k1gNnSc3	ztvárnění
řeky	řeka	k1gFnSc2	řeka
Beruny	Beruna	k1gFnSc2	Beruna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
И	И	k?	И
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Slovinské	slovinský	k2eAgFnPc1d1	slovinská
Alpy	Alpy	k1gFnPc1	Alpy
Julské	Julský	k2eAgFnSc2d1	Julský
Alpy	alpa	k1gFnSc2	alpa
Jalovec	jalovec	k1gInSc1	jalovec
(	(	kIx(	(
<g/>
hora	hora	k1gFnSc1	hora
<g/>
)	)	kIx)	)
Monte	Mont	k1gInSc5	Mont
Canin	Canin	k1gMnSc1	Canin
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Soča	Soč	k1gInSc2	Soč
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
