<p>
<s>
Šeřík	šeřík	k1gInSc1	šeřík
čínský	čínský	k2eAgInSc1d1	čínský
(	(	kIx(	(
<g/>
Syringa	syringa	k1gFnSc1	syringa
×	×	k?	×
chinensis	chinensis	k1gInSc1	chinensis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rostlina	rostlina	k1gFnSc1	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
olivovníkovité	olivovníkovitý	k2eAgFnSc2d1	olivovníkovitý
(	(	kIx(	(
<g/>
Oleaceae	Oleacea	k1gFnSc2	Oleacea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzrůstem	vzrůst	k1gInSc7	vzrůst
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
velikosti	velikost	k1gFnPc4	velikost
keře	keř	k1gInSc2	keř
vysokého	vysoký	k2eAgInSc2d1	vysoký
3	[number]	k4	3
až	až	k9	až
4	[number]	k4	4
metry	metr	k1gInPc4	metr
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
křížence	kříženka	k1gFnSc6	kříženka
šeříku	šeřík	k1gInSc2	šeřík
obecného	obecný	k2eAgInSc2d1	obecný
(	(	kIx(	(
<g/>
Syringa	syringa	k1gFnSc1	syringa
vulgaris	vulgaris	k1gFnSc1	vulgaris
<g/>
)	)	kIx)	)
s	s	k7c7	s
druhem	druh	k1gMnSc7	druh
šeříku	šeřík	k1gInSc2	šeřík
Syringa	syringa	k1gFnSc1	syringa
protolaciniata	protolaciniata	k1gFnSc1	protolaciniata
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Šeřík	šeřík	k1gInSc1	šeřík
čínský	čínský	k2eAgInSc1d1	čínský
kvete	kvést	k5eAaImIp3nS	kvést
v	v	k7c6	v
měsíci	měsíc	k1gInSc6	měsíc
květnu	květen	k1gInSc6	květen
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
růžovofialové	růžovofialový	k2eAgInPc1d1	růžovofialový
<g/>
.	.	kIx.	.
</s>
<s>
Stavbou	stavba	k1gFnSc7	stavba
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
trubkovité	trubkovitý	k2eAgInPc1d1	trubkovitý
<g/>
,	,	kIx,	,
čtyřcípé	čtyřcípý	k2eAgInPc1d1	čtyřcípý
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
se	se	k3xPyFc4	se
také	také	k9	také
svojí	svůj	k3xOyFgFnSc7	svůj
charakteristickou	charakteristický	k2eAgFnSc7d1	charakteristická
vůní	vůně	k1gFnSc7	vůně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vysazován	vysazován	k2eAgMnSc1d1	vysazován
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
nenáročnosti	nenáročnost	k1gFnSc3	nenáročnost
a	a	k8xC	a
mrazuvzdornosti	mrazuvzdornost	k1gFnSc3	mrazuvzdornost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
O	o	k7c6	o
šeříku	šeřík	k1gInSc6	šeřík
čínském	čínský	k2eAgInSc6d1	čínský
–	–	k?	–
zahradaweb	zahradawba	k1gFnPc2	zahradawba
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
O	o	k7c6	o
šeříku	šeřík	k1gInSc6	šeřík
čínském	čínský	k2eAgInSc6d1	čínský
–	–	k?	–
garten	gartno	k1gNnPc2	gartno
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
