<s>
Hobit	hobit	k1gMnSc1	hobit
aneb	aneb	k?	aneb
Cesta	cesta	k1gFnSc1	cesta
tam	tam	k6eAd1	tam
a	a	k8xC	a
zase	zase	k9	zase
zpátky	zpátky	k6eAd1	zpátky
(	(	kIx(	(
<g/>
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
originálu	originál	k1gInSc6	originál
The	The	k1gMnSc1	The
Hobbit	Hobbit	k1gMnSc1	Hobbit
<g/>
,	,	kIx,	,
or	or	k?	or
There	Ther	k1gMnSc5	Ther
and	and	k?	and
Back	Back	k1gMnSc1	Back
Again	Again	k1gMnSc1	Again
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dobrodružný	dobrodružný	k2eAgInSc1d1	dobrodružný
román	román	k1gInSc1	román
anglického	anglický	k2eAgMnSc2d1	anglický
spisovatele	spisovatel	k1gMnSc2	spisovatel
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkiena	Tolkien	k1gMnSc2	Tolkien
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
vydaný	vydaný	k2eAgInSc1d1	vydaný
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
