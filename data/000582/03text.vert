<s>
Hobit	hobit	k1gMnSc1	hobit
aneb	aneb	k?	aneb
Cesta	cesta	k1gFnSc1	cesta
tam	tam	k6eAd1	tam
a	a	k8xC	a
zase	zase	k9	zase
zpátky	zpátky	k6eAd1	zpátky
(	(	kIx(	(
<g/>
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
originálu	originál	k1gInSc6	originál
The	The	k1gMnSc1	The
Hobbit	Hobbit	k1gMnSc1	Hobbit
<g/>
,	,	kIx,	,
or	or	k?	or
There	Ther	k1gMnSc5	Ther
and	and	k?	and
Back	Back	k1gMnSc1	Back
Again	Again	k1gMnSc1	Again
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dobrodružný	dobrodružný	k2eAgInSc1d1	dobrodružný
román	román	k1gInSc1	román
anglického	anglický	k2eAgMnSc2d1	anglický
spisovatele	spisovatel	k1gMnSc2	spisovatel
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkiena	Tolkien	k1gMnSc2	Tolkien
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
vydaný	vydaný	k2eAgInSc1d1	vydaný
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
původně	původně	k6eAd1	původně
jako	jako	k8xS	jako
vedlejší	vedlejší	k2eAgInSc1d1	vedlejší
produkt	produkt	k1gInSc1	produkt
autorem	autor	k1gMnSc7	autor
vytvářených	vytvářený	k2eAgFnPc2d1	vytvářená
legend	legenda	k1gFnPc2	legenda
fiktivní	fiktivní	k2eAgFnSc2d1	fiktivní
země	zem	k1gFnSc2	zem
Středozemě	Středozem	k1gFnSc2	Středozem
<g/>
.	.	kIx.	.
</s>
<s>
Zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
knihu	kniha	k1gFnSc4	kniha
přiměl	přimět	k5eAaPmAgMnS	přimět
Tolkiena	Tolkien	k1gMnSc4	Tolkien
k	k	k7c3	k
sepsání	sepsání	k1gNnSc3	sepsání
jejího	její	k3xOp3gNnSc2	její
monumentálního	monumentální	k2eAgNnSc2d1	monumentální
pokračování	pokračování	k1gNnSc2	pokračování
<g/>
,	,	kIx,	,
vydaného	vydaný	k2eAgNnSc2d1	vydané
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
setkalo	setkat	k5eAaPmAgNnS	setkat
s	s	k7c7	s
ještě	ještě	k6eAd1	ještě
větším	veliký	k2eAgInSc7d2	veliký
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
kniha	kniha	k1gFnSc1	kniha
poprvé	poprvé	k6eAd1	poprvé
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Františka	František	k1gMnSc2	František
Vrby	Vrba	k1gMnSc2	Vrba
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
dětské	dětský	k2eAgMnPc4d1	dětský
čtenáře	čtenář	k1gMnPc4	čtenář
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
zapadá	zapadat	k5eAaPmIp3nS	zapadat
do	do	k7c2	do
autorem	autor	k1gMnSc7	autor
budované	budovaný	k2eAgFnSc2d1	budovaná
sítě	síť	k1gFnSc2	síť
příběhů	příběh	k1gInPc2	příběh
a	a	k8xC	a
legend	legenda	k1gFnPc2	legenda
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
velmi	velmi	k6eAd1	velmi
rozsáhle	rozsáhle	k6eAd1	rozsáhle
popisuje	popisovat	k5eAaImIp3nS	popisovat
především	především	k9	především
reálie	reálie	k1gFnSc1	reálie
Středozemě	Středozem	k1gFnSc2	Středozem
a	a	k8xC	a
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
do	do	k7c2	do
příběhu	příběh	k1gInSc2	příběh
opravdu	opravdu	k6eAd1	opravdu
ponořit	ponořit	k5eAaPmF	ponořit
<g/>
.	.	kIx.	.
</s>
<s>
Tolkien	Tolkien	k1gInSc1	Tolkien
svým	svůj	k3xOyFgNnSc7	svůj
dílem	dílo	k1gNnSc7	dílo
stvořil	stvořit	k5eAaPmAgInS	stvořit
skutečný	skutečný	k2eAgInSc1d1	skutečný
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
postavy	postava	k1gFnPc1	postava
v	v	k7c6	v
příběhu	příběh	k1gInSc6	příběh
neustále	neustále	k6eAd1	neustále
narážejí	narážet	k5eAaImIp3nP	narážet
na	na	k7c4	na
minulost	minulost	k1gFnSc4	minulost
Středozemě	Středozem	k1gFnSc2	Středozem
<g/>
,	,	kIx,	,
na	na	k7c4	na
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
tak	tak	k9	tak
dávno	dávno	k6eAd1	dávno
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
nikdo	nikdo	k3yNnSc1	nikdo
nepamatuje	pamatovat	k5eNaImIp3nS	pamatovat
a	a	k8xC	a
přesto	přesto	k8xC	přesto
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
do	do	k7c2	do
jejich	jejich	k3xOp3gFnSc2	jejich
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Světem	svět	k1gInSc7	svět
a	a	k8xC	a
příběhy	příběh	k1gInPc1	příběh
se	se	k3xPyFc4	se
inspirovalo	inspirovat	k5eAaBmAgNnS	inspirovat
mnoho	mnoho	k6eAd1	mnoho
dalších	další	k2eAgNnPc2d1	další
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
literárních	literární	k2eAgFnPc2d1	literární
<g/>
,	,	kIx,	,
filmových	filmový	k2eAgFnPc2d1	filmová
nebo	nebo	k8xC	nebo
počítačových	počítačový	k2eAgFnPc2d1	počítačová
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
domu	dům	k1gInSc2	dům
hobita	hobit	k1gMnSc2	hobit
Bilba	Bilb	k1gMnSc2	Bilb
Pytlíka	pytlík	k1gMnSc2	pytlík
přišel	přijít	k5eAaPmAgInS	přijít
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
čaroděj	čaroděj	k1gMnSc1	čaroděj
Gandalf	Gandalf	k1gMnSc1	Gandalf
<g/>
.	.	kIx.	.
</s>
<s>
Čaroděj	čaroděj	k1gMnSc1	čaroděj
pak	pak	k6eAd1	pak
proti	proti	k7c3	proti
vůli	vůle	k1gFnSc3	vůle
mladého	mladý	k2eAgMnSc2d1	mladý
hobita	hobit	k1gMnSc2	hobit
přivedl	přivést	k5eAaPmAgMnS	přivést
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
nory	nora	k1gFnSc2	nora
třináct	třináct	k4xCc4	třináct
trpaslíků	trpaslík	k1gMnPc2	trpaslík
<g/>
,	,	kIx,	,
před	před	k7c7	před
nimiž	jenž	k3xRgMnPc7	jenž
označil	označit	k5eAaPmAgMnS	označit
Bilba	Bilba	k1gMnSc1	Bilba
za	za	k7c4	za
vynikajícího	vynikající	k2eAgMnSc4d1	vynikající
lupiče	lupič	k1gMnSc4	lupič
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
začíná	začínat	k5eAaImIp3nS	začínat
dobrodružná	dobrodružný	k2eAgFnSc1d1	dobrodružná
výprava	výprava	k1gFnSc1	výprava
za	za	k7c7	za
získáním	získání	k1gNnSc7	získání
pokladu	poklad	k1gInSc2	poklad
<g/>
,	,	kIx,	,
o	o	k7c4	o
který	který	k3yQgInSc4	který
trpaslíky	trpaslík	k1gMnPc4	trpaslík
před	před	k7c7	před
mnoha	mnoho	k4c7	mnoho
lety	léto	k1gNnPc7	léto
připravil	připravit	k5eAaPmAgMnS	připravit
drak	drak	k1gMnSc1	drak
Šmak	šmak	k1gInSc4	šmak
<g/>
.	.	kIx.	.
</s>
<s>
Výprava	výprava	k1gFnSc1	výprava
vedená	vedený	k2eAgFnSc1d1	vedená
Gandalfem	Gandalf	k1gInSc7	Gandalf
a	a	k8xC	a
Thorinem	Thorin	k1gInSc7	Thorin
Pavézou	pavéza	k1gFnSc7	pavéza
již	již	k6eAd1	již
druhého	druhý	k4xOgInSc2	druhý
dne	den	k1gInSc2	den
opustila	opustit	k5eAaPmAgFnS	opustit
Kraj	kraj	k1gInSc4	kraj
a	a	k8xC	a
při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
Osamělé	osamělý	k2eAgFnSc3d1	osamělá
hoře	hora	k1gFnSc3	hora
se	se	k3xPyFc4	se
záhy	záhy	k6eAd1	záhy
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
nebezpečných	bezpečný	k2eNgFnPc2d1	nebezpečná
situací	situace	k1gFnPc2	situace
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
Bilba	Bilba	k1gFnSc1	Bilba
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc4	jeho
přátele	přítel	k1gMnPc4	přítel
málem	málem	k6eAd1	málem
snědla	sníst	k5eAaPmAgFnS	sníst
trojice	trojice	k1gFnSc1	trojice
zlobrů	zlobr	k1gMnPc2	zlobr
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
výprava	výprava	k1gFnSc1	výprava
navštívila	navštívit	k5eAaPmAgFnS	navštívit
"	"	kIx"	"
<g/>
Poslední	poslední	k2eAgInSc1d1	poslední
domácký	domácký	k2eAgInSc1d1	domácký
dům	dům	k1gInSc1	dům
<g/>
"	"	kIx"	"
v	v	k7c6	v
Roklince	roklinka	k1gFnSc6	roklinka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
trpaslíci	trpaslík	k1gMnPc1	trpaslík
<g/>
,	,	kIx,	,
hobit	hobit	k1gMnSc1	hobit
a	a	k8xC	a
čaroděj	čaroděj	k1gMnSc1	čaroděj
setkali	setkat	k5eAaPmAgMnP	setkat
s	s	k7c7	s
elfy	elf	k1gMnPc7	elf
a	a	k8xC	a
jejich	jejich	k3xOp3gMnSc7	jejich
mocným	mocný	k2eAgMnSc7d1	mocný
pánem	pán	k1gMnSc7	pán
Elrondem	Elrond	k1gMnSc7	Elrond
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jim	on	k3xPp3gMnPc3	on
před	před	k7c7	před
dalším	další	k2eAgNnSc7d1	další
putováním	putování	k1gNnSc7	putování
poskytnul	poskytnout	k5eAaPmAgMnS	poskytnout
mnoho	mnoho	k4c4	mnoho
důležitých	důležitý	k2eAgFnPc2d1	důležitá
rad	rada	k1gFnPc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
velkou	velký	k2eAgFnSc7d1	velká
překážkou	překážka	k1gFnSc7	překážka
při	při	k7c6	při
jejich	jejich	k3xOp3gFnSc6	jejich
cestě	cesta	k1gFnSc6	cesta
na	na	k7c4	na
východ	východ	k1gInSc4	východ
však	však	k9	však
představoval	představovat	k5eAaImAgInS	představovat
teprve	teprve	k6eAd1	teprve
obtížný	obtížný	k2eAgInSc1d1	obtížný
přechod	přechod	k1gInSc1	přechod
Mlžných	mlžný	k2eAgFnPc2d1	mlžná
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
dramatickým	dramatický	k2eAgFnPc3d1	dramatická
událostem	událost	k1gFnPc3	událost
<g/>
.	.	kIx.	.
</s>
<s>
Výprava	výprava	k1gFnSc1	výprava
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
zajata	zajat	k2eAgFnSc1d1	zajata
skřety	skřet	k1gMnPc4	skřet
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zběsilém	zběsilý	k2eAgInSc6d1	zběsilý
úprku	úprk	k1gInSc6	úprk
skřetími	skřetí	k1gNnPc7	skřetí
tunely	tunel	k1gInPc1	tunel
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
Bilbo	Bilba	k1gFnSc5	Bilba
ztratil	ztratit	k5eAaPmAgInS	ztratit
v	v	k7c6	v
podzemních	podzemní	k2eAgFnPc6d1	podzemní
jeskyních	jeskyně	k1gFnPc6	jeskyně
<g/>
.	.	kIx.	.
</s>
<s>
Osamocen	osamocen	k2eAgMnSc1d1	osamocen
a	a	k8xC	a
ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
doplazil	doplazit	k5eAaPmAgInS	doplazit
až	až	k9	až
na	na	k7c4	na
samé	samý	k3xTgNnSc4	samý
dno	dno	k1gNnSc4	dno
jeskynního	jeskynní	k2eAgInSc2d1	jeskynní
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
narazil	narazit	k5eAaPmAgMnS	narazit
na	na	k7c4	na
záhadný	záhadný	k2eAgInSc4d1	záhadný
prsten	prsten	k1gInSc4	prsten
pohozený	pohozený	k2eAgInSc4d1	pohozený
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
Mlžnými	mlžný	k2eAgFnPc7d1	mlžná
horami	hora	k1gFnPc7	hora
také	také	k9	také
potkal	potkat	k5eAaPmAgInS	potkat
tvora	tvor	k1gMnSc4	tvor
známého	známý	k1gMnSc4	známý
jako	jako	k8xS	jako
Glum	Glum	k1gInSc4	Glum
<g/>
.	.	kIx.	.
</s>
<s>
Glum	Glum	k1gMnSc1	Glum
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
kdysi	kdysi	k6eAd1	kdysi
také	také	k6eAd1	také
hobitem	hobit	k1gMnSc7	hobit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vlastníkem	vlastník	k1gMnSc7	vlastník
Bilbem	Bilb	k1gMnSc7	Bilb
nalezeného	nalezený	k2eAgInSc2d1	nalezený
kouzelného	kouzelný	k2eAgInSc2d1	kouzelný
prstu	prst	k1gInSc2	prst
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mu	on	k3xPp3gMnSc3	on
dával	dávat	k5eAaImAgMnS	dávat
dlouhověkost	dlouhověkost	k1gFnSc4	dlouhověkost
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
jej	on	k3xPp3gMnSc4	on
činil	činit	k5eAaImAgInS	činit
neviditelným	viditelný	k2eNgInPc3d1	neviditelný
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgMnS	moct
opustit	opustit	k5eAaPmF	opustit
Glumovu	Glumův	k2eAgFnSc4d1	Glumova
jeskyni	jeskyně	k1gFnSc4	jeskyně
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
Bilbo	Bilba	k1gFnSc5	Bilba
soutěž	soutěž	k1gFnSc4	soutěž
v	v	k7c6	v
hádankách	hádanka	k1gFnPc6	hádanka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
hádankách	hádanka	k1gFnPc6	hádanka
ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yIgInPc6	který
šlo	jít	k5eAaImAgNnS	jít
Bilbovi	Bilba	k1gMnSc3	Bilba
o	o	k7c4	o
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
utekl	utéct	k5eAaPmAgMnS	utéct
hobit	hobit	k1gMnSc1	hobit
z	z	k7c2	z
jeskyní	jeskyně	k1gFnPc2	jeskyně
i	i	k9	i
s	s	k7c7	s
Glumovým	Glumův	k2eAgInSc7d1	Glumův
prstenem	prsten	k1gInSc7	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
útěku	útěk	k1gInSc6	útěk
pak	pak	k6eAd1	pak
Bilbo	Bilba	k1gFnSc5	Bilba
odhalil	odhalit	k5eAaPmAgInS	odhalit
část	část	k1gFnSc4	část
tajemství	tajemství	k1gNnSc2	tajemství
nalezeného	nalezený	k2eAgInSc2d1	nalezený
prstenu	prsten	k1gInSc2	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Neznal	znát	k5eNaImAgInS	znát
však	však	k9	však
jeho	on	k3xPp3gInSc4	on
původ	původ	k1gInSc4	původ
a	a	k8xC	a
nevěděl	vědět	k5eNaImAgMnS	vědět
nic	nic	k3yNnSc4	nic
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
ničivé	ničivý	k2eAgFnSc6d1	ničivá
síle	síla	k1gFnSc6	síla
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
shledání	shledání	k1gNnSc6	shledání
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
prchala	prchat	k5eAaImAgFnS	prchat
výprava	výprava	k1gFnSc1	výprava
dál	daleko	k6eAd2	daleko
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
hor.	hor.	k?	hor.
Skřetí	Skřetý	k2eAgMnPc1d1	Skřetý
pronásledovatelé	pronásledovatel	k1gMnPc1	pronásledovatel
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yQgFnPc3	který
se	se	k3xPyFc4	se
přidali	přidat	k5eAaPmAgMnP	přidat
i	i	k9	i
nebezpeční	bezpečný	k2eNgMnPc1d1	nebezpečný
vrrci	vrrce	k1gMnPc1	vrrce
však	však	k9	však
Bilba	Bilba	k1gMnSc1	Bilba
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc4	jeho
přátele	přítel	k1gMnPc4	přítel
dohnali	dohnat	k5eAaPmAgMnP	dohnat
a	a	k8xC	a
před	před	k7c7	před
tímto	tento	k3xDgNnSc7	tento
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
je	být	k5eAaImIp3nS	být
zachránil	zachránit	k5eAaPmAgInS	zachránit
teprve	teprve	k6eAd1	teprve
včasný	včasný	k2eAgInSc1d1	včasný
přílet	přílet	k1gInSc1	přílet
a	a	k8xC	a
pomoc	pomoc	k1gFnSc1	pomoc
obrovských	obrovský	k2eAgMnPc2d1	obrovský
orlů	orel	k1gMnPc2	orel
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
orlího	orlí	k2eAgNnSc2d1	orlí
hnízda	hnízdo	k1gNnSc2	hnízdo
na	na	k7c6	na
Skalbalu	Skalbal	k1gInSc6	Skalbal
pak	pak	k6eAd1	pak
výprava	výprava	k1gFnSc1	výprava
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
do	do	k7c2	do
Meddědova	Meddědův	k2eAgInSc2d1	Meddědův
domu	dům	k1gInSc2	dům
stojícího	stojící	k2eAgInSc2d1	stojící
při	při	k7c6	při
okraji	okraj	k1gInSc6	okraj
Temného	temný	k2eAgInSc2d1	temný
hvozdu	hvozd	k1gInSc2	hvozd
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátkém	krátký	k2eAgInSc6d1	krátký
odpočinku	odpočinek	k1gInSc6	odpočinek
v	v	k7c6	v
Meddědově	Meddědův	k2eAgInSc6d1	Meddědův
příbytku	příbytek	k1gInSc6	příbytek
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
oddělil	oddělit	k5eAaPmAgMnS	oddělit
čaroděj	čaroděj	k1gMnSc1	čaroděj
Gandalf	Gandalf	k1gMnSc1	Gandalf
a	a	k8xC	a
trpaslíci	trpaslík	k1gMnPc1	trpaslík
společně	společně	k6eAd1	společně
s	s	k7c7	s
Bilbem	Bilb	k1gInSc7	Bilb
vybavení	vybavení	k1gNnSc1	vybavení
čarodějovými	čarodějův	k2eAgFnPc7d1	čarodějova
radami	rada	k1gFnPc7	rada
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
do	do	k7c2	do
temnoty	temnota	k1gFnSc2	temnota
lesa	les	k1gInSc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
korunami	koruna	k1gFnPc7	koruna
Temného	temný	k2eAgInSc2d1	temný
hvozdu	hvozd	k1gInSc2	hvozd
procházeli	procházet	k5eAaImAgMnP	procházet
mnoho	mnoho	k4c4	mnoho
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yIgInPc6	který
jim	on	k3xPp3gMnPc3	on
postupně	postupně	k6eAd1	postupně
došly	dojít	k5eAaPmAgInP	dojít
zásoby	zásoba	k1gFnPc4	zásoba
<g/>
.	.	kIx.	.
</s>
<s>
Museli	muset	k5eAaImAgMnP	muset
navíc	navíc	k6eAd1	navíc
překonávat	překonávat	k5eAaImF	překonávat
začarovanou	začarovaný	k2eAgFnSc4d1	začarovaná
řeku	řeka	k1gFnSc4	řeka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
tlustý	tlustý	k2eAgMnSc1d1	tlustý
trpaslík	trpaslík	k1gMnSc1	trpaslík
Bombur	Bombur	k1gMnSc1	Bombur
nalokal	nalokat	k5eAaPmAgMnS	nalokat
otrávené	otrávený	k2eAgFnPc4d1	otrávená
vody	voda	k1gFnPc4	voda
a	a	k8xC	a
upadnul	upadnout	k5eAaPmAgMnS	upadnout
do	do	k7c2	do
bezvědomí	bezvědomí	k1gNnSc2	bezvědomí
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
jej	on	k3xPp3gMnSc4	on
ostatní	ostatní	k2eAgMnPc1d1	ostatní
museli	muset	k5eAaImAgMnP	muset
nést	nést	k5eAaImF	nést
<g/>
.	.	kIx.	.
</s>
<s>
Vysílení	vysílený	k2eAgMnPc1d1	vysílený
cestovatelé	cestovatel	k1gMnPc1	cestovatel
nakonec	nakonec	k6eAd1	nakonec
narazili	narazit	k5eAaPmAgMnP	narazit
na	na	k7c4	na
místní	místní	k2eAgMnPc4d1	místní
elfy	elf	k1gMnPc4	elf
<g/>
.	.	kIx.	.
</s>
<s>
Přilákáni	přilákán	k2eAgMnPc1d1	přilákán
světly	světlo	k1gNnPc7	světlo
elfské	elfská	k1gFnSc2	elfská
slavnosti	slavnost	k1gFnSc2	slavnost
neuposlechli	uposlechnout	k5eNaPmAgMnP	uposlechnout
Gandalfova	Gandalfův	k2eAgNnPc4d1	Gandalfovo
varování	varování	k1gNnPc4	varování
a	a	k8xC	a
opustili	opustit	k5eAaPmAgMnP	opustit
lesní	lesní	k2eAgFnSc4d1	lesní
stezku	stezka	k1gFnSc4	stezka
<g/>
.	.	kIx.	.
</s>
<s>
Trpaslíci	trpaslík	k1gMnPc1	trpaslík
a	a	k8xC	a
Bilbo	Bilba	k1gFnSc5	Bilba
se	se	k3xPyFc4	se
v	v	k7c6	v
lese	les	k1gInSc6	les
záhy	záhy	k6eAd1	záhy
ztratili	ztratit	k5eAaPmAgMnP	ztratit
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
pochytáni	pochytat	k5eAaPmNgMnP	pochytat
velkými	velký	k2eAgMnPc7d1	velký
pavouky	pavouk	k1gMnPc7	pavouk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
je	být	k5eAaImIp3nS	být
všechny	všechen	k3xTgInPc4	všechen
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Bilba	Bilba	k1gFnSc1	Bilba
odnesli	odnést	k5eAaPmAgMnP	odnést
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Bilbo	Bilba	k1gMnSc5	Bilba
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
kouzelného	kouzelný	k2eAgInSc2d1	kouzelný
prstenu	prsten	k1gInSc2	prsten
pavouky	pavouk	k1gMnPc4	pavouk
odlákal	odlákat	k5eAaPmAgMnS	odlákat
a	a	k8xC	a
své	svůj	k3xOyFgMnPc4	svůj
přátele	přítel	k1gMnPc4	přítel
osvobodil	osvobodit	k5eAaPmAgInS	osvobodit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zahnání	zahnání	k1gNnSc6	zahnání
pavouků	pavouk	k1gMnPc2	pavouk
však	však	k8xC	však
všichni	všechen	k3xTgMnPc1	všechen
trpaslíci	trpaslík	k1gMnPc1	trpaslík
brzy	brzy	k6eAd1	brzy
padli	padnout	k5eAaPmAgMnP	padnout
do	do	k7c2	do
zajetí	zajetí	k1gNnSc2	zajetí
elfů	elf	k1gMnPc2	elf
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k6eAd1	jen
Bilbovi	Bilbův	k2eAgMnPc1d1	Bilbův
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
podařilo	podařit	k5eAaPmAgNnS	podařit
vyklouznout	vyklouznout	k5eAaPmF	vyklouznout
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
sledoval	sledovat	k5eAaImAgMnS	sledovat
elfí	elfí	k2eAgFnSc4d1	elfí
družinu	družina	k1gFnSc4	družina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
odvedla	odvést	k5eAaPmAgFnS	odvést
zajaté	zajatý	k1gMnPc4	zajatý
trpaslíky	trpaslík	k1gMnPc4	trpaslík
až	až	k9	až
do	do	k7c2	do
sídla	sídlo	k1gNnSc2	sídlo
svého	svůj	k3xOyFgMnSc2	svůj
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
elfů	elf	k1gMnPc2	elf
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
neměl	mít	k5eNaImAgMnS	mít
trpaslíky	trpaslík	k1gMnPc4	trpaslík
příliš	příliš	k6eAd1	příliš
v	v	k7c6	v
lásce	láska	k1gFnSc6	láska
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
popuzen	popudit	k5eAaPmNgMnS	popudit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
Thorin	Thorin	k1gInSc1	Thorin
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
společníci	společník	k1gMnPc1	společník
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
sdělit	sdělit	k5eAaPmF	sdělit
cíl	cíl	k1gInSc4	cíl
své	svůj	k3xOyFgFnSc2	svůj
výpravy	výprava	k1gFnSc2	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
nechal	nechat	k5eAaPmAgMnS	nechat
všechny	všechen	k3xTgMnPc4	všechen
své	svůj	k3xOyFgMnPc4	svůj
zajatce	zajatec	k1gMnPc4	zajatec
uvěznit	uvěznit	k5eAaPmF	uvěznit
v	v	k7c6	v
kobkách	kobka	k1gFnPc6	kobka
svého	svůj	k3xOyFgNnSc2	svůj
sídla	sídlo	k1gNnSc2	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
prstenu	prsten	k1gInSc3	prsten
se	se	k3xPyFc4	se
Bilbovi	Bilba	k1gMnSc6	Bilba
zanedlouho	zanedlouho	k6eAd1	zanedlouho
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
od	od	k7c2	od
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
dozorců	dozorce	k1gMnPc2	dozorce
klíče	klíč	k1gInSc2	klíč
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
trpaslíci	trpaslík	k1gMnPc1	trpaslík
vedení	vedení	k1gNnSc2	vedení
hobitem	hobit	k1gMnSc7	hobit
uprchli	uprchnout	k5eAaPmAgMnP	uprchnout
z	z	k7c2	z
cel	clo	k1gNnPc2	clo
a	a	k8xC	a
ukryli	ukrýt	k5eAaPmAgMnP	ukrýt
se	se	k3xPyFc4	se
do	do	k7c2	do
prázdných	prázdný	k2eAgInPc2d1	prázdný
vinných	vinný	k2eAgInPc2d1	vinný
soudků	soudek	k1gInPc2	soudek
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
nic	nic	k6eAd1	nic
netušící	tušící	k2eNgMnPc4d1	netušící
elfí	elfí	k6eAd1	elfí
stráže	stráž	k1gFnPc4	stráž
poslali	poslat	k5eAaPmAgMnP	poslat
po	po	k7c6	po
proudu	proud	k1gInSc6	proud
Lesní	lesní	k2eAgFnSc2d1	lesní
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sudech	sud	k1gInPc6	sud
dopluli	doplout	k5eAaPmAgMnP	doplout
trpaslíci	trpaslík	k1gMnPc1	trpaslík
a	a	k8xC	a
hobit	hobit	k1gMnSc1	hobit
až	až	k9	až
k	k	k7c3	k
Jezernímu	jezerní	k2eAgNnSc3d1	jezerní
městu	město	k1gNnSc3	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
místní	místní	k2eAgMnPc1d1	místní
lidé	člověk	k1gMnPc1	člověk
přivítali	přivítat	k5eAaPmAgMnP	přivítat
Thorina	Thorin	k1gMnSc4	Thorin
jako	jako	k8xC	jako
vracejícího	vracející	k2eAgMnSc4d1	vracející
se	se	k3xPyFc4	se
dědice	dědic	k1gMnPc4	dědic
Durinova	Durinův	k2eAgInSc2d1	Durinův
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
<s>
Výprava	výprava	k1gMnSc1	výprava
si	se	k3xPyFc3	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
odpočinula	odpočinout	k5eAaPmAgFnS	odpočinout
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
až	až	k9	až
k	k	k7c3	k
Osamělé	osamělý	k2eAgFnSc3d1	osamělá
hoře	hora	k1gFnSc3	hora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
trpaslíci	trpaslík	k1gMnPc1	trpaslík
začali	začít	k5eAaPmAgMnP	začít
pátrat	pátrat	k5eAaImF	pátrat
po	po	k7c6	po
tajném	tajný	k2eAgInSc6d1	tajný
vchodu	vchod	k1gInSc6	vchod
do	do	k7c2	do
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
Bilbo	Bilba	k1gFnSc5	Bilba
objevil	objevit	k5eAaPmAgMnS	objevit
ukryté	ukrytý	k2eAgFnPc4d1	ukrytá
dveře	dveře	k1gFnPc4	dveře
a	a	k8xC	a
nastal	nastat	k5eAaPmAgInS	nastat
Durinův	Durinův	k2eAgInSc4d1	Durinův
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
odemknul	odemknout	k5eAaPmAgInS	odemknout
Thorin	Thorin	k1gInSc1	Thorin
tajný	tajný	k2eAgInSc4d1	tajný
vchod	vchod	k1gInSc4	vchod
do	do	k7c2	do
Osamělé	osamělý	k2eAgFnSc2d1	osamělá
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
odemčení	odemčení	k1gNnSc4	odemčení
dveří	dveře	k1gFnPc2	dveře
vyslali	vyslat	k5eAaPmAgMnP	vyslat
trpaslíci	trpaslík	k1gMnPc1	trpaslík
Bilba	Bilba	k1gMnSc1	Bilba
jako	jako	k8xS	jako
lupiče	lupič	k1gMnSc2	lupič
výpravy	výprava	k1gFnSc2	výprava
na	na	k7c4	na
průzkum	průzkum	k1gInSc4	průzkum
do	do	k7c2	do
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Bilbo	Bilba	k1gMnSc5	Bilba
v	v	k7c6	v
nitru	nitro	k1gNnSc6	nitro
Ereboru	Erebor	k1gInSc2	Erebor
našel	najít	k5eAaPmAgMnS	najít
draka	drak	k1gMnSc4	drak
spícího	spící	k2eAgMnSc4d1	spící
na	na	k7c6	na
hromadě	hromada	k1gFnSc6	hromada
zlata	zlato	k1gNnSc2	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Bilbo	Bilba	k1gMnSc5	Bilba
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
nebezpečný	bezpečný	k2eNgInSc4d1	nebezpečný
rozhovor	rozhovor	k1gInSc4	rozhovor
s	s	k7c7	s
probuzeným	probuzený	k2eAgInSc7d1	probuzený
Šmakem	šmak	k1gInSc7	šmak
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
si	se	k3xPyFc3	se
odnesl	odnést	k5eAaPmAgMnS	odnést
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
zlatých	zlatý	k2eAgInPc2d1	zlatý
pohárů	pohár	k1gInPc2	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Šmak	šmak	k1gInSc1	šmak
si	se	k3xPyFc3	se
domyslel	domyslet	k5eAaPmAgInS	domyslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
hobitovi	hobitův	k2eAgMnPc1d1	hobitův
museli	muset	k5eAaImAgMnP	muset
při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
hoře	hora	k1gFnSc3	hora
pomoci	pomoc	k1gFnSc2	pomoc
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
Jezerního	jezerní	k2eAgNnSc2d1	jezerní
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
rozezlen	rozezlen	k2eAgMnSc1d1	rozezlen
opustil	opustit	k5eAaPmAgMnS	opustit
Erebor	Erebor	k1gInSc4	Erebor
a	a	k8xC	a
na	na	k7c4	na
nedaleké	daleký	k2eNgNnSc4d1	nedaleké
město	město	k1gNnSc4	město
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
<g/>
.	.	kIx.	.
</s>
<s>
Esgaroth	Esgaroth	k1gInSc1	Esgaroth
byl	být	k5eAaImAgInS	být
při	při	k7c6	při
Šmakově	Šmakův	k2eAgInSc6d1	Šmakův
útoku	útok	k1gInSc6	útok
zapálen	zapálen	k2eAgMnSc1d1	zapálen
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
zpustošen	zpustošen	k2eAgMnSc1d1	zpustošen
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Bardu	bard	k1gMnSc3	bard
Lučištníkovi	lučištník	k1gMnSc3	lučištník
se	se	k3xPyFc4	se
draka	drak	k1gMnSc4	drak
podařilo	podařit	k5eAaPmAgNnS	podařit
usmrtit	usmrtit	k5eAaPmF	usmrtit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
drakově	drakův	k2eAgFnSc6d1	Drakova
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
k	k	k7c3	k
lidem	člověk	k1gMnPc3	člověk
z	z	k7c2	z
Jezerního	jezerní	k2eAgNnSc2d1	jezerní
města	město	k1gNnSc2	město
přidalo	přidat	k5eAaPmAgNnS	přidat
vojsko	vojsko	k1gNnSc1	vojsko
elfů	elf	k1gMnPc2	elf
a	a	k8xC	a
společně	společně	k6eAd1	společně
obě	dva	k4xCgFnPc1	dva
spojené	spojený	k2eAgFnPc1d1	spojená
armády	armáda	k1gFnPc1	armáda
zamířily	zamířit	k5eAaPmAgFnP	zamířit
k	k	k7c3	k
Hoře	hora	k1gFnSc3	hora
<g/>
,	,	kIx,	,
doufajíc	doufat	k5eAaImSgFnS	doufat
ve	v	k7c4	v
snadnou	snadný	k2eAgFnSc4d1	snadná
kořist	kořist	k1gFnSc4	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
a	a	k8xC	a
elfové	elf	k1gMnPc1	elf
však	však	k9	však
nalezli	naleznout	k5eAaPmAgMnP	naleznout
pouze	pouze	k6eAd1	pouze
zapečetěnou	zapečetěný	k2eAgFnSc4d1	zapečetěná
bránu	brána	k1gFnSc4	brána
a	a	k8xC	a
Thorina	Thorin	k1gMnSc4	Thorin
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
družinu	družina	k1gFnSc4	družina
na	na	k7c6	na
stráži	stráž	k1gFnSc6	stráž
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdohlavý	tvrdohlavý	k2eAgMnSc1d1	tvrdohlavý
trpaslík	trpaslík	k1gMnSc1	trpaslík
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
jakékoliv	jakýkoliv	k3yIgNnSc4	jakýkoliv
dělení	dělení	k1gNnSc4	dělení
pokladu	poklad	k1gInSc2	poklad
svých	svůj	k3xOyFgMnPc2	svůj
předků	předek	k1gMnPc2	předek
a	a	k8xC	a
rázně	rázně	k6eAd1	rázně
uťal	utít	k5eAaPmAgInS	utít
snahy	snaha	k1gFnSc2	snaha
Barda	bard	k1gMnSc2	bard
a	a	k8xC	a
krále	král	k1gMnSc2	král
elfů	elf	k1gMnPc2	elf
o	o	k7c6	o
vyjednávání	vyjednávání	k1gNnSc6	vyjednávání
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
elfové	elf	k1gMnPc1	elf
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
Thorinovu	Thorinův	k2eAgFnSc4d1	Thorinův
výpravu	výprava	k1gFnSc4	výprava
v	v	k7c6	v
Osamělé	osamělý	k2eAgFnSc6d1	osamělá
hoře	hora	k1gFnSc6	hora
oblehli	oblehnout	k5eAaPmAgMnP	oblehnout
<g/>
.	.	kIx.	.
</s>
<s>
Trpaslíkům	trpaslík	k1gMnPc3	trpaslík
však	však	k9	však
zanedlouho	zanedlouho	k6eAd1	zanedlouho
přišel	přijít	k5eAaPmAgMnS	přijít
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
Thorinův	Thorinův	k2eAgMnSc1d1	Thorinův
bratranec	bratranec	k1gMnSc1	bratranec
<g/>
,	,	kIx,	,
Dáin	Dáin	k1gInSc4	Dáin
Železná	železný	k2eAgFnSc1d1	železná
noha	noha	k1gFnSc1	noha
<g/>
,	,	kIx,	,
s	s	k7c7	s
armádou	armáda	k1gFnSc7	armáda
ze	z	k7c2	z
Železných	železný	k2eAgInPc2d1	železný
hor.	hor.	k?	hor.
Bilbo	Bilba	k1gMnSc5	Bilba
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
chtěl	chtít	k5eAaImAgMnS	chtít
předejít	předejít	k5eAaPmF	předejít
zbytečnému	zbytečný	k2eAgNnSc3d1	zbytečné
krveprolití	krveprolití	k1gNnSc3	krveprolití
proto	proto	k8xC	proto
ukradl	ukradnout	k5eAaPmAgMnS	ukradnout
Arcikam	Arcikam	k1gInSc4	Arcikam
<g/>
,	,	kIx,	,
nejvzácnější	vzácný	k2eAgMnSc1d3	nejvzácnější
z	z	k7c2	z
klenotů	klenot	k1gInPc2	klenot
v	v	k7c6	v
Hoře	hora	k1gFnSc6	hora
<g/>
,	,	kIx,	,
a	a	k8xC	a
vydal	vydat	k5eAaPmAgInS	vydat
jej	on	k3xPp3gMnSc4	on
Bardovi	bardův	k2eAgMnPc1d1	bardův
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
hobitův	hobitův	k2eAgInSc1d1	hobitův
krok	krok	k1gInSc1	krok
však	však	k9	však
ke	k	k7c3	k
smíru	smír	k1gInSc3	smír
nevedl	vést	k5eNaImAgMnS	vést
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
návrat	návrat	k1gInSc4	návrat
Gandalfa	Gandalf	k1gMnSc2	Gandalf
odvrátil	odvrátit	k5eAaPmAgMnS	odvrátit
boj	boj	k1gInSc4	boj
mezi	mezi	k7c7	mezi
trpaslíky	trpaslík	k1gMnPc7	trpaslík
a	a	k8xC	a
spojenci	spojenec	k1gMnPc7	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Čaroděj	čaroděj	k1gMnSc1	čaroděj
informoval	informovat	k5eAaBmAgMnS	informovat
velitele	velitel	k1gMnSc4	velitel
o	o	k7c6	o
velkém	velký	k2eAgNnSc6d1	velké
vojsku	vojsko	k1gNnSc6	vojsko
skřetů	skřet	k1gMnPc2	skřet
a	a	k8xC	a
vrrků	vrrek	k1gMnPc2	vrrek
rychle	rychle	k6eAd1	rychle
se	s	k7c7	s
blížícím	blížící	k2eAgMnSc7d1	blížící
k	k	k7c3	k
Osamělé	osamělý	k2eAgFnSc3d1	osamělá
hoře	hora	k1gFnSc3	hora
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
kritické	kritický	k2eAgNnSc4d1	kritické
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
znepřátelené	znepřátelený	k2eAgFnSc2d1	znepřátelená
strany	strana	k1gFnSc2	strana
ihned	ihned	k6eAd1	ihned
sjednotilo	sjednotit	k5eAaPmAgNnS	sjednotit
a	a	k8xC	a
spojenci	spojenec	k1gMnPc1	spojenec
byli	být	k5eAaImAgMnP	být
záhy	záhy	k6eAd1	záhy
napadeni	napadnout	k5eAaPmNgMnP	napadnout
velkou	velký	k2eAgFnSc7d1	velká
přesilou	přesila	k1gFnSc7	přesila
skřetů	skřet	k1gMnPc2	skřet
a	a	k8xC	a
vrrků	vrrek	k1gMnPc2	vrrek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Osamělé	osamělý	k2eAgFnSc2d1	osamělá
hory	hora	k1gFnSc2	hora
tak	tak	k6eAd1	tak
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
bitvě	bitva	k1gFnSc3	bitva
pěti	pět	k4xCc2	pět
armád	armáda	k1gFnPc2	armáda
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
byla	být	k5eAaImAgNnP	být
spojená	spojený	k2eAgNnPc1d1	spojené
vojska	vojsko	k1gNnPc1	vojsko
elfů	elf	k1gMnPc2	elf
<g/>
,	,	kIx,	,
trpaslíků	trpaslík	k1gMnPc2	trpaslík
a	a	k8xC	a
lidí	člověk	k1gMnPc2	člověk
téměř	téměř	k6eAd1	téměř
poražena	poražen	k2eAgFnSc1d1	poražena
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
bitevní	bitevní	k2eAgFnSc2d1	bitevní
vřavy	vřava	k1gFnSc2	vřava
se	se	k3xPyFc4	se
zapojil	zapojit	k5eAaPmAgInS	zapojit
také	také	k9	také
Medděd	Medděd	k1gInSc1	Medděd
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
situaci	situace	k1gFnSc4	situace
na	na	k7c6	na
bojišti	bojiště	k1gNnSc6	bojiště
zachránil	zachránit	k5eAaPmAgInS	zachránit
teprve	teprve	k6eAd1	teprve
přílet	přílet	k1gInSc1	přílet
velkých	velký	k2eAgMnPc2d1	velký
orlů	orel	k1gMnPc2	orel
<g/>
,	,	kIx,	,
s	s	k7c7	s
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
pomocí	pomoc	k1gFnSc7	pomoc
byli	být	k5eAaImAgMnP	být
skřeti	skřet	k1gMnPc1	skřet
poraženi	porazit	k5eAaPmNgMnP	porazit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
byl	být	k5eAaImAgInS	být
smrtelně	smrtelně	k6eAd1	smrtelně
zraněn	zraněn	k2eAgInSc1d1	zraněn
Thorin	Thorin	k1gInSc1	Thorin
Pavéza	pavéza	k1gFnSc1	pavéza
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ještě	ještě	k9	ještě
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
Bilbovi	Bilba	k1gMnSc3	Bilba
odpustil	odpustit	k5eAaPmAgMnS	odpustit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
usmrcení	usmrcení	k1gNnSc6	usmrcení
draka	drak	k1gMnSc4	drak
a	a	k8xC	a
odražení	odražení	k1gNnSc4	odražení
skřetího	skřetí	k2eAgInSc2d1	skřetí
nájezdu	nájezd	k1gInSc2	nájezd
zavládl	zavládnout	k5eAaPmAgInS	zavládnout
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Osamělé	osamělý	k2eAgFnSc2d1	osamělá
hory	hora	k1gFnSc2	hora
mír	mír	k1gInSc1	mír
<g/>
.	.	kIx.	.
</s>
<s>
Trpaslíci	trpaslík	k1gMnPc1	trpaslík
obnovili	obnovit	k5eAaPmAgMnP	obnovit
Království	království	k1gNnSc4	království
pod	pod	k7c7	pod
Horou	Hora	k1gMnSc7	Hora
a	a	k8xC	a
jejich	jejich	k3xOp3gMnSc7	jejich
novým	nový	k2eAgMnSc7d1	nový
králem	král	k1gMnSc7	král
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Dáin	Dáin	k1gMnSc1	Dáin
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
pokladu	poklad	k1gInSc2	poklad
bylo	být	k5eAaImAgNnS	být
znovu	znovu	k6eAd1	znovu
vystavěno	vystavěn	k2eAgNnSc1d1	vystavěno
poničené	poničený	k2eAgNnSc1d1	poničené
Jezerní	jezerní	k2eAgNnSc1d1	jezerní
město	město	k1gNnSc1	město
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
vedení	vedení	k1gNnSc2	vedení
Bardem	bard	k1gMnSc7	bard
obnovili	obnovit	k5eAaPmAgMnP	obnovit
i	i	k9	i
starobylé	starobylý	k2eAgNnSc4d1	starobylé
zaniklé	zaniklý	k2eAgNnSc4d1	zaniklé
město	město	k1gNnSc4	město
Dol.	Dol.	k1gFnSc2	Dol.
Svůj	svůj	k3xOyFgInSc1	svůj
díl	díl	k1gInSc1	díl
dračího	dračí	k2eAgInSc2d1	dračí
pokladu	poklad	k1gInSc2	poklad
obdržel	obdržet	k5eAaPmAgInS	obdržet
také	také	k9	také
Bilbo	Bilba	k1gFnSc5	Bilba
Pytlík	pytlík	k1gInSc1	pytlík
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
rozloučil	rozloučit	k5eAaPmAgInS	rozloučit
se	s	k7c7	s
zbývajícími	zbývající	k2eAgMnPc7d1	zbývající
přáteli	přítel	k1gMnPc7	přítel
a	a	k8xC	a
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
Gandalfa	Gandalf	k1gMnSc2	Gandalf
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgMnS	vydat
na	na	k7c4	na
dalekou	daleký	k2eAgFnSc4d1	daleká
zpáteční	zpáteční	k2eAgFnSc4d1	zpáteční
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
čarodějem	čaroděj	k1gMnSc7	čaroděj
a	a	k8xC	a
Meddědem	Meddědo	k1gNnSc7	Meddědo
obešel	obejít	k5eAaPmAgInS	obejít
Bilbo	Bilba	k1gFnSc5	Bilba
severní	severní	k2eAgNnSc1d1	severní
konec	konec	k1gInSc4	konec
Temného	temný	k2eAgInSc2d1	temný
hvozdu	hvozd	k1gInSc2	hvozd
a	a	k8xC	a
ještě	ještě	k9	ještě
jednou	jednou	k6eAd1	jednou
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
v	v	k7c6	v
Meddědově	Meddědův	k2eAgInSc6d1	Meddědův
příbytku	příbytek	k1gInSc6	příbytek
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
Bilbo	Bilba	k1gFnSc5	Bilba
a	a	k8xC	a
Gandalf	Gandalf	k1gInSc4	Gandalf
překročili	překročit	k5eAaPmAgMnP	překročit
Mlžné	mlžný	k2eAgFnPc4d1	mlžná
hory	hora	k1gFnPc4	hora
a	a	k8xC	a
dorazili	dorazit	k5eAaPmAgMnP	dorazit
do	do	k7c2	do
Roklinky	roklinka	k1gFnSc2	roklinka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
setkali	setkat	k5eAaPmAgMnP	setkat
s	s	k7c7	s
Elrondem	Elrond	k1gInSc7	Elrond
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
již	již	k6eAd1	již
zamířili	zamířit	k5eAaPmAgMnP	zamířit
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
Kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
k	k	k7c3	k
Bilbovu	Bilbův	k2eAgInSc3d1	Bilbův
domovu	domov	k1gInSc3	domov
<g/>
.	.	kIx.	.
</s>
<s>
Doma	doma	k6eAd1	doma
ovšem	ovšem	k9	ovšem
Bilba	Bilb	k1gMnSc4	Bilb
čekalo	čekat	k5eAaImAgNnS	čekat
nemilé	milý	k2eNgNnSc1d1	nemilé
překvapení	překvapení	k1gNnSc1	překvapení
<g/>
,	,	kIx,	,
když	když	k8xS	když
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
odchodu	odchod	k1gInSc6	odchod
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
mrtvého	mrtvý	k1gMnSc4	mrtvý
a	a	k8xC	a
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
jeho	on	k3xPp3gInSc2	on
majetku	majetek	k1gInSc2	majetek
byla	být	k5eAaImAgNnP	být
prodána	prodat	k5eAaPmNgNnP	prodat
v	v	k7c6	v
dražbě	dražba	k1gFnSc6	dražba
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
návrat	návrat	k1gInSc1	návrat
z	z	k7c2	z
dobrodružné	dobrodružný	k2eAgFnSc2d1	dobrodružná
cesty	cesta	k1gFnSc2	cesta
a	a	k8xC	a
historky	historka	k1gFnSc2	historka
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
pohádkovém	pohádkový	k2eAgNnSc6d1	pohádkové
bohatství	bohatství	k1gNnSc6	bohatství
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
si	se	k3xPyFc3	se
z	z	k7c2	z
výpravy	výprava	k1gFnSc2	výprava
dovezl	dovézt	k5eAaPmAgMnS	dovézt
<g/>
,	,	kIx,	,
vyvolali	vyvolat	k5eAaPmAgMnP	vyvolat
v	v	k7c6	v
Kraji	kraj	k1gInSc6	kraj
hotovou	hotový	k2eAgFnSc4d1	hotová
seznaci	seznace	k1gFnSc4	seznace
<g/>
.	.	kIx.	.
</s>
<s>
Vůbec	vůbec	k9	vůbec
prvním	první	k4xOgInSc7	první
překladem	překlad	k1gInSc7	překlad
Hobita	hobit	k1gMnSc2	hobit
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
byl	být	k5eAaImAgInS	být
český	český	k2eAgInSc1d1	český
scénář	scénář	k1gInSc1	scénář
pro	pro	k7c4	pro
kreslenou	kreslený	k2eAgFnSc4d1	kreslená
verzi	verze	k1gFnSc4	verze
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
ztracený	ztracený	k2eAgInSc4d1	ztracený
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
uměleckém	umělecký	k2eAgNnSc6d1	umělecké
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Odeon	odeon	k1gInSc1	odeon
překlad	překlad	k1gInSc1	překlad
Františka	František	k1gMnSc2	František
Vrby	Vrba	k1gMnSc2	Vrba
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
měl	mít	k5eAaImAgInS	mít
však	však	k9	však
tehdy	tehdy	k6eAd1	tehdy
zákaz	zákaz	k1gInSc1	zákaz
publikování	publikování	k1gNnSc2	publikování
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
byl	být	k5eAaImAgMnS	být
místo	místo	k7c2	místo
něj	on	k3xPp3gNnSc2	on
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
uveden	uvést	k5eAaPmNgMnS	uvést
jeho	jeho	k3xOp3gMnSc1	jeho
přítel	přítel	k1gMnSc1	přítel
Lubomír	Lubomír	k1gMnSc1	Lubomír
Dorůžka	Dorůžka	k1gFnSc1	Dorůžka
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
rysem	rys	k1gInSc7	rys
tohoto	tento	k3xDgNnSc2	tento
vydání	vydání	k1gNnSc2	vydání
je	být	k5eAaImIp3nS	být
celkově	celkově	k6eAd1	celkově
dětské	dětský	k2eAgNnSc1d1	dětské
pohádkové	pohádkový	k2eAgNnSc1d1	pohádkové
pojetí	pojetí	k1gNnSc1	pojetí
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
u	u	k7c2	u
kresleného	kreslený	k2eAgInSc2d1	kreslený
filmu	film	k1gInSc2	film
z	z	k7c2	z
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
ilustrací	ilustrace	k1gFnPc2	ilustrace
byl	být	k5eAaImAgMnS	být
Jiří	Jiří	k1gMnSc1	Jiří
Šalamoun	Šalamoun	k1gMnSc1	Šalamoun
<g/>
,	,	kIx,	,
jazyk	jazyk	k1gMnSc1	jazyk
Vrbova	Vrbův	k2eAgInSc2d1	Vrbův
překladu	překlad	k1gInSc2	překlad
je	být	k5eAaImIp3nS	být
bohatý	bohatý	k2eAgInSc1d1	bohatý
na	na	k7c4	na
barvité	barvitý	k2eAgNnSc4d1	barvité
dětské	dětský	k2eAgNnSc4d1	dětské
vyjadřování	vyjadřování	k1gNnSc4	vyjadřování
<g/>
.	.	kIx.	.
</s>
<s>
Vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc1	první
filmová	filmový	k2eAgFnSc1d1	filmová
adaptace	adaptace	k1gFnSc1	adaptace
Hobita	hobit	k1gMnSc2	hobit
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
krátký	krátký	k2eAgInSc4d1	krátký
animovaný	animovaný	k2eAgInSc4d1	animovaný
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
režie	režie	k1gFnSc1	režie
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
Gene	gen	k1gInSc5	gen
Deitch	Deitch	k1gMnSc1	Deitch
<g/>
,	,	kIx,	,
animátor	animátor	k1gMnSc1	animátor
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
žijící	žijící	k2eAgMnSc1d1	žijící
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
<s>
Producentem	producent	k1gMnSc7	producent
filmu	film	k1gInSc2	film
byl	být	k5eAaImAgMnS	být
Oscarem	Oscar	k1gInSc7	Oscar
oceněný	oceněný	k2eAgInSc4d1	oceněný
William	William	k1gInSc4	William
Snyder	Snydra	k1gFnPc2	Snydra
a	a	k8xC	a
výtvarníkem	výtvarník	k1gMnSc7	výtvarník
filmu	film	k1gInSc2	film
Adolf	Adolf	k1gMnSc1	Adolf
Born	Born	k1gMnSc1	Born
<g/>
.	.	kIx.	.
</s>
<s>
William	William	k6eAd1	William
Snyder	Snyder	k1gMnSc1	Snyder
byl	být	k5eAaImAgMnS	být
vlastníkem	vlastník	k1gMnSc7	vlastník
práv	právo	k1gNnPc2	právo
na	na	k7c4	na
zfilmování	zfilmování	k1gNnSc4	zfilmování
Hobita	hobit	k1gMnSc2	hobit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
finančních	finanční	k2eAgInPc2d1	finanční
důvodů	důvod	k1gInPc2	důvod
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nedařilo	dařit	k5eNaImAgNnS	dařit
splnit	splnit	k5eAaPmF	splnit
základní	základní	k2eAgFnSc4d1	základní
podmínku	podmínka	k1gFnSc4	podmínka
pro	pro	k7c4	pro
udržení	udržení	k1gNnSc3	udržení
těchto	tento	k3xDgNnPc2	tento
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
natočení	natočení	k1gNnSc1	natočení
filmu	film	k1gInSc2	film
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
využil	využít	k5eAaPmAgMnS	využít
svých	svůj	k3xOyFgMnPc2	svůj
kontaktů	kontakt	k1gInPc2	kontakt
v	v	k7c6	v
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
Československu	Československo	k1gNnSc6	Československo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
natáčela	natáčet	k5eAaImAgFnS	natáčet
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
animovaných	animovaný	k2eAgInPc2d1	animovaný
seriálů	seriál	k1gInPc2	seriál
pro	pro	k7c4	pro
velká	velký	k2eAgNnPc4d1	velké
hollywoodská	hollywoodský	k2eAgNnPc4d1	hollywoodské
studia	studio	k1gNnPc4	studio
(	(	kIx(	(
<g/>
např.	např.	kA	např.
i	i	k8xC	i
Tom	Tom	k1gMnSc1	Tom
a	a	k8xC	a
Jerry	Jerra	k1gFnPc1	Jerra
<g/>
)	)	kIx)	)
a	a	k8xC	a
oslovil	oslovit	k5eAaPmAgInS	oslovit
Deitche	Deitche	k1gInSc1	Deitche
s	s	k7c7	s
nabídkou	nabídka	k1gFnSc7	nabídka
na	na	k7c6	na
spolupráci	spolupráce	k1gFnSc6	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Deitch	Deitch	k1gMnSc1	Deitch
s	s	k7c7	s
Adolfem	Adolf	k1gMnSc7	Adolf
Bornem	Born	k1gMnSc7	Born
rychle	rychle	k6eAd1	rychle
natočili	natočit	k5eAaBmAgMnP	natočit
krátký	krátký	k2eAgInSc4d1	krátký
dvanáctiminutový	dvanáctiminutový	k2eAgInSc4d1	dvanáctiminutový
animovaný	animovaný	k2eAgInSc4d1	animovaný
film	film	k1gInSc4	film
(	(	kIx(	(
<g/>
na	na	k7c4	na
YouTube	YouTub	k1gInSc5	YouTub
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
The	The	k1gFnSc2	The
HOBBIT	HOBBIT	kA	HOBBIT
1966	[number]	k4	1966
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
Snyder	Snyder	k1gInSc1	Snyder
v	v	k7c6	v
termínu	termín	k1gInSc6	termín
promítl	promítnout	k5eAaPmAgMnS	promítnout
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Práva	právo	k1gNnPc1	právo
na	na	k7c4	na
zfilmování	zfilmování	k1gNnSc4	zfilmování
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
prodloužena	prodloužen	k2eAgFnSc1d1	prodloužena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
začal	začít	k5eAaPmAgInS	začít
znovu	znovu	k6eAd1	znovu
uvažovat	uvažovat	k5eAaImF	uvažovat
o	o	k7c4	o
natočení	natočení	k1gNnSc4	natočení
celovečerního	celovečerní	k2eAgInSc2d1	celovečerní
animovaného	animovaný	k2eAgInSc2d1	animovaný
filmu	film	k1gInSc2	film
Hobit	hobit	k1gMnSc1	hobit
a	a	k8xC	a
tentokrát	tentokrát	k6eAd1	tentokrát
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c6	o
spolupráci	spolupráce	k1gFnSc6	spolupráce
Jiřího	Jiří	k1gMnSc4	Jiří
Trnku	Trnka	k1gMnSc4	Trnka
<g/>
.	.	kIx.	.
</s>
<s>
Trnka	Trnka	k1gMnSc1	Trnka
nakreslil	nakreslit	k5eAaPmAgMnS	nakreslit
pro	pro	k7c4	pro
film	film	k1gInSc4	film
několik	několik	k4yIc4	několik
návrhů	návrh	k1gInPc2	návrh
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
zemřel	zemřít	k5eAaPmAgInS	zemřít
a	a	k8xC	a
dílo	dílo	k1gNnSc4	dílo
nestačil	stačit	k5eNaBmAgMnS	stačit
dokončit	dokončit	k5eAaPmF	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Snyder	Snyder	k1gInSc1	Snyder
práva	právo	k1gNnSc2	právo
na	na	k7c4	na
Hobita	hobit	k1gMnSc4	hobit
nakonec	nakonec	k6eAd1	nakonec
prodal	prodat	k5eAaPmAgMnS	prodat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
natočili	natočit	k5eAaBmAgMnP	natočit
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgMnPc2d1	americký
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
Rankin	Rankin	k1gMnSc1	Rankin
<g/>
/	/	kIx~	/
<g/>
Bass	Bass	k1gMnSc1	Bass
Productions	Productionsa	k1gFnPc2	Productionsa
<g/>
,	,	kIx,	,
Inc	Inc	k1gFnSc1	Inc
<g/>
.	.	kIx.	.
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
The	The	k1gMnSc2	The
Hobbit	Hobbit	k1gMnSc2	Hobbit
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
78	[number]	k4	78
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
předešlou	předešlý	k2eAgFnSc4d1	předešlá
filmovou	filmový	k2eAgFnSc4d1	filmová
triogii	triogie	k1gFnSc4	triogie
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
natočil	natočit	k5eAaBmAgMnS	natočit
režisér	režisér	k1gMnSc1	režisér
Peter	Peter	k1gMnSc1	Peter
Jackson	Jackson	k1gMnSc1	Jackson
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2012	[number]	k4	2012
-	-	kIx~	-
2014	[number]	k4	2014
trilogii	trilogie	k1gFnSc6	trilogie
<g />
.	.	kIx.	.
</s>
<s>
Hobit	hobit	k1gMnSc1	hobit
<g/>
:	:	kIx,	:
Hobit	hobit	k1gMnSc1	hobit
<g/>
:	:	kIx,	:
Neočekávaná	očekávaný	k2eNgFnSc1d1	neočekávaná
cesta	cesta	k1gFnSc1	cesta
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Hobit	hobit	k1gMnSc1	hobit
<g/>
:	:	kIx,	:
Šmakova	Šmakův	k2eAgFnSc1d1	Šmakův
dračí	dračí	k2eAgFnSc1d1	dračí
poušť	poušť	k1gFnSc1	poušť
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Hobit	hobit	k1gMnSc1	hobit
<g/>
:	:	kIx,	:
Bitva	bitva	k1gFnSc1	bitva
pěti	pět	k4xCc2	pět
armád	armáda	k1gFnPc2	armáda
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
nastudovali	nastudovat	k5eAaBmAgMnP	nastudovat
adaptaci	adaptace	k1gFnSc4	adaptace
Hobita	hobit	k1gMnSc2	hobit
studenti	student	k1gMnPc1	student
DAMU	DAMU	kA	DAMU
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Ctibora	Ctibor	k1gMnSc2	Ctibor
Turby	turba	k1gFnSc2	turba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc4	jeho
audio	audio	k2eAgFnSc1d1	audio
adaptace	adaptace	k1gFnSc1	adaptace
vysílána	vysílat	k5eAaImNgFnS	vysílat
v	v	k7c6	v
rozhlase	rozhlas	k1gInSc6	rozhlas
(	(	kIx(	(
<g/>
načetl	načíst	k5eAaBmAgMnS	načíst
Petr	Petr	k1gMnSc1	Petr
Nárožný	Nárožný	k2eAgMnSc1d1	Nárožný
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
limitované	limitovaný	k2eAgFnSc6d1	limitovaná
edici	edice	k1gFnSc6	edice
vyšla	vyjít	k5eAaPmAgFnS	vyjít
také	také	k9	také
na	na	k7c6	na
magnetofonové	magnetofonový	k2eAgFnSc6d1	magnetofonová
pásce	páska	k1gFnSc6	páska
<g/>
.	.	kIx.	.
</s>
