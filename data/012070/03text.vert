<p>
<s>
Občanství	občanství	k1gNnSc1	občanství
je	být	k5eAaImIp3nS	být
časově	časově	k6eAd1	časově
relativně	relativně	k6eAd1	relativně
trvalý	trvalý	k2eAgInSc4d1	trvalý
<g/>
,	,	kIx,	,
místně	místně	k6eAd1	místně
neomezený	omezený	k2eNgInSc4d1	neomezený
právní	právní	k2eAgInSc4d1	právní
svazek	svazek	k1gInSc4	svazek
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
vztah	vztah	k1gInSc4	vztah
či	či	k8xC	či
status	status	k1gInSc4	status
<g/>
)	)	kIx)	)
fyzické	fyzický	k2eAgFnSc2d1	fyzická
osoby	osoba	k1gFnSc2	osoba
a	a	k8xC	a
daného	daný	k2eAgInSc2d1	daný
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgNnSc1d1	státní
občanství	občanství	k1gNnSc1	občanství
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
znaků	znak	k1gInPc2	znak
moderního	moderní	k2eAgInSc2d1	moderní
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
právní	právní	k2eAgInSc1d1	právní
svazek	svazek	k1gInSc1	svazek
mezi	mezi	k7c7	mezi
občanem	občan	k1gMnSc7	občan
a	a	k8xC	a
státem	stát	k1gInSc7	stát
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
proti	proti	k7c3	proti
vůli	vůle	k1gFnSc3	vůle
fyzické	fyzický	k2eAgFnPc4d1	fyzická
osoby	osoba	k1gFnPc4	osoba
nezrušitelný	zrušitelný	k2eNgMnSc1d1	nezrušitelný
a	a	k8xC	a
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
základě	základ	k1gInSc6	základ
vznikají	vznikat	k5eAaImIp3nP	vznikat
jeho	on	k3xPp3gInSc4	on
subjektům	subjekt	k1gInPc3	subjekt
vzájemná	vzájemný	k2eAgNnPc4d1	vzájemné
práva	právo	k1gNnPc4	právo
a	a	k8xC	a
povinnosti	povinnost	k1gFnPc4	povinnost
(	(	kIx(	(
<g/>
závazky	závazek	k1gInPc4	závazek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gFnPc4	on
patří	patřit	k5eAaImIp3nS	patřit
zejména	zejména	k9	zejména
právo	právo	k1gNnSc1	právo
občana	občan	k1gMnSc2	občan
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
státu	stát	k1gInSc2	stát
a	a	k8xC	a
povinnosti	povinnost	k1gFnSc2	povinnost
občana	občan	k1gMnSc2	občan
jako	jako	k9	jako
věrnost	věrnost	k1gFnSc1	věrnost
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
závazek	závazek	k1gInSc1	závazek
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
obraně	obrana	k1gFnSc3	obrana
<g/>
,	,	kIx,	,
výkon	výkon	k1gInSc4	výkon
určitých	určitý	k2eAgFnPc2d1	určitá
funkcí	funkce	k1gFnPc2	funkce
a	a	k8xC	a
dodržování	dodržování	k1gNnSc4	dodržování
právních	právní	k2eAgInPc2d1	právní
předpisů	předpis	k1gInPc2	předpis
státu	stát	k1gInSc2	stát
i	i	k8xC	i
mimo	mimo	k7c4	mimo
jeho	jeho	k3xOp3gNnSc4	jeho
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pojem	pojem	k1gInSc1	pojem
občanství	občanství	k1gNnSc2	občanství
bývá	bývat	k5eAaImIp3nS	bývat
někdy	někdy	k6eAd1	někdy
ztotožňován	ztotožňován	k2eAgInSc1d1	ztotožňován
s	s	k7c7	s
pojmem	pojem	k1gInSc7	pojem
státní	státní	k2eAgFnSc4d1	státní
příslušnost	příslušnost	k1gFnSc4	příslušnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ten	ten	k3xDgMnSc1	ten
je	být	k5eAaImIp3nS	být
obsahově	obsahově	k6eAd1	obsahově
širší	široký	k2eAgFnSc1d2	širší
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
do	do	k7c2	do
sebe	se	k3xPyFc2	se
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k8xC	i
právnické	právnický	k2eAgFnPc1d1	právnická
osoby	osoba	k1gFnPc1	osoba
příslušné	příslušný	k2eAgFnPc1d1	příslušná
k	k	k7c3	k
danému	daný	k2eAgInSc3d1	daný
státu	stát	k1gInSc3	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Římě	Řím	k1gInSc6	Řím
bychom	by	kYmCp1nP	by
pod	pod	k7c7	pod
slovem	slovo	k1gNnSc7	slovo
občanství	občanství	k1gNnSc2	občanství
nalezli	nalézt	k5eAaBmAgMnP	nalézt
pojem	pojem	k1gInSc4	pojem
status	status	k1gInSc4	status
civitatis	civitatis	k1gFnSc2	civitatis
<g/>
,	,	kIx,	,
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
dějinách	dějiny	k1gFnPc6	dějiny
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgInS	být
inkolát	inkolát	k1gInSc1	inkolát
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
pojem	pojem	k1gInSc1	pojem
občanství	občanství	k1gNnSc2	občanství
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
ze	z	k7c2	z
3	[number]	k4	3
<g/>
.	.	kIx.	.
stavu	stav	k1gInSc2	stav
(	(	kIx(	(
<g/>
měšťanstva	měšťanstvo	k1gNnSc2	měšťanstvo
<g/>
)	)	kIx)	)
během	během	k7c2	během
Velké	velký	k2eAgFnSc2d1	velká
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgNnSc1d1	státní
občanství	občanství	k1gNnSc1	občanství
v	v	k7c6	v
moderním	moderní	k2eAgInSc6d1	moderní
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
se	se	k3xPyFc4	se
zformovalo	zformovat	k5eAaPmAgNnS	zformovat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
buržoazních	buržoazní	k2eAgFnPc2d1	buržoazní
revolucí	revoluce	k1gFnPc2	revoluce
<g/>
,	,	kIx,	,
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k8xS	jako
moderní	moderní	k2eAgInSc1d1	moderní
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
konstitucionalismus	konstitucionalismus	k1gInSc1	konstitucionalismus
<g/>
,	,	kIx,	,
politické	politický	k2eAgFnSc2d1	politická
strany	strana	k1gFnSc2	strana
či	či	k8xC	či
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
svobody	svoboda	k1gFnSc2	svoboda
jednotlivce	jednotlivec	k1gMnSc2	jednotlivec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
dob	doba	k1gFnPc2	doba
nevolnictví	nevolnictví	k1gNnSc4	nevolnictví
nebyl	být	k5eNaImAgInS	být
umožněn	umožněn	k2eAgInSc1d1	umožněn
svobodný	svobodný	k2eAgInSc1d1	svobodný
pohyb	pohyb	k1gInSc1	pohyb
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
uplatňovala	uplatňovat	k5eAaImAgFnS	uplatňovat
zásada	zásada	k1gFnSc1	zásada
ius	ius	k?	ius
soli	sůl	k1gFnSc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
Ústava	ústava	k1gFnSc1	ústava
Francie	Francie	k1gFnSc1	Francie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1791	[number]	k4	1791
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Code	Code	k1gNnSc1	Code
Napoleon	napoleon	k1gInSc1	napoleon
(	(	kIx(	(
<g/>
občanský	občanský	k2eAgInSc1d1	občanský
zákoník	zákoník	k1gInSc1	zákoník
<g/>
)	)	kIx)	)
obnovují	obnovovat	k5eAaImIp3nP	obnovovat
zásadu	zásada	k1gFnSc4	zásada
ius	ius	k?	ius
sanguinis	sanguinis	k1gFnSc1	sanguinis
<g/>
,	,	kIx,	,
když	když	k8xS	když
zavádí	zavádět	k5eAaImIp3nS	zavádět
pojem	pojem	k1gInSc1	pojem
občanství	občanství	k1gNnSc2	občanství
místo	místo	k7c2	místo
poddanství	poddanství	k1gNnSc2	poddanství
(	(	kIx(	(
<g/>
termín	termín	k1gInSc1	termín
poddanství	poddanství	k1gNnSc1	poddanství
nebyl	být	k5eNaImAgInS	být
vhodný	vhodný	k2eAgInSc1d1	vhodný
pro	pro	k7c4	pro
vyjádření	vyjádření	k1gNnSc4	vyjádření
rovnosti	rovnost	k1gFnSc2	rovnost
před	před	k7c7	před
zákonem	zákon	k1gInSc7	zákon
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
byl	být	k5eAaImAgInS	být
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
základů	základ	k1gInPc2	základ
vytvářející	vytvářející	k2eAgFnSc2d1	vytvářející
se	se	k3xPyFc4	se
občanské	občanský	k2eAgFnSc2d1	občanská
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
osvícenských	osvícenský	k2eAgFnPc2d1	osvícenská
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozměr	rozměr	k1gInSc4	rozměr
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
v	v	k7c6	v
právu	právo	k1gNnSc6	právo
==	==	k?	==
</s>
</p>
<p>
<s>
Státní	státní	k2eAgNnSc1d1	státní
občanství	občanství	k1gNnSc1	občanství
má	mít	k5eAaImIp3nS	mít
rozměr	rozměr	k1gInSc4	rozměr
nejen	nejen	k6eAd1	nejen
vnitrostátní	vnitrostátní	k2eAgFnSc1d1	vnitrostátní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
právního	právní	k2eAgNnSc2d1	právní
zakotvení	zakotvení	k1gNnSc2	zakotvení
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
práva	právo	k1gNnSc2	právo
veřejného	veřejný	k2eAgNnSc2d1	veřejné
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
rámci	rámec	k1gInSc6	rámec
zejména	zejména	k9	zejména
do	do	k7c2	do
práva	právo	k1gNnSc2	právo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
ústavního	ústavní	k2eAgInSc2d1	ústavní
-	-	kIx~	-
základy	základ	k1gInPc1	základ
vztahu	vztah	k1gInSc2	vztah
jedince	jedinec	k1gMnSc2	jedinec
a	a	k8xC	a
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
vztahy	vztah	k1gInPc1	vztah
navenek	navenek	k6eAd1	navenek
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
trestního	trestní	k2eAgNnSc2d1	trestní
-	-	kIx~	-
stíhání	stíhání	k1gNnSc1	stíhání
za	za	k7c4	za
porušení	porušení	k1gNnSc4	porušení
věrnosti	věrnost	k1gFnSc2	věrnost
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
vlastizrada	vlastizrada	k1gFnSc1	vlastizrada
<g/>
,	,	kIx,	,
válečná	válečný	k2eAgFnSc1d1	válečná
zrada	zrada	k1gFnSc1	zrada
atd.	atd.	kA	atd.
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
správního	správní	k2eAgInSc2d1	správní
-	-	kIx~	-
evidence	evidence	k1gFnSc1	evidence
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
státoobčanské	státoobčanský	k2eAgNnSc4d1	státoobčanské
řízení	řízení	k1gNnSc4	řízení
<g/>
,	,	kIx,	,
doklady	doklad	k1gInPc4	doklad
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
finančního	finanční	k2eAgInSc2d1	finanční
-	-	kIx~	-
poplatky	poplatek	k1gInPc7	poplatek
při	při	k7c6	při
nabývání	nabývání	k1gNnSc6	nabývání
<g/>
,	,	kIx,	,
pozbývání	pozbývání	k1gNnSc6	pozbývání
a	a	k8xC	a
osvědčování	osvědčování	k1gNnSc6	osvědčování
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
veřejného	veřejný	k2eAgInSc2d1	veřejný
-	-	kIx~	-
smlouvy	smlouva	k1gFnPc4	smlouva
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
státy	stát	k1gInPc7	stát
o	o	k7c6	o
dvojím	dvojí	k4xRgInSc6	dvojí
občanství	občanství	k1gNnSc6	občanství
<g/>
,	,	kIx,	,
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
diplomatickou	diplomatický	k2eAgFnSc4d1	diplomatická
ochranu	ochrana	k1gFnSc4	ochrana
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
soukromého	soukromý	k2eAgInSc2d1	soukromý
–	–	k?	–
státní	státní	k2eAgNnSc4d1	státní
občanství	občanství	k1gNnSc4	občanství
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
hraniční	hraniční	k2eAgInSc4d1	hraniční
ukazatel	ukazatel	k1gInSc4	ukazatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typy	typ	k1gInPc1	typ
fyzických	fyzický	k2eAgFnPc2d1	fyzická
osob	osoba	k1gFnPc2	osoba
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
==	==	k?	==
</s>
</p>
<p>
<s>
Dle	dle	k7c2	dle
právního	právní	k2eAgInSc2d1	právní
vztahu	vztah	k1gInSc2	vztah
fyzické	fyzický	k2eAgFnSc2d1	fyzická
osoby	osoba	k1gFnSc2	osoba
ke	k	k7c3	k
státu	stát	k1gInSc3	stát
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
tři	tři	k4xCgInPc1	tři
typy	typ	k1gInPc1	typ
fyzických	fyzický	k2eAgFnPc2d1	fyzická
osob	osoba	k1gFnPc2	osoba
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgMnSc1d1	státní
občan	občan	k1gMnSc1	občan
-	-	kIx~	-
má	mít	k5eAaImIp3nS	mít
specifický	specifický	k2eAgInSc1d1	specifický
státoobčanský	státoobčanský	k2eAgInSc1d1	státoobčanský
poměr	poměr	k1gInSc1	poměr
ke	k	k7c3	k
státu	stát	k1gInSc3	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Cizinec	cizinec	k1gMnSc1	cizinec
-	-	kIx~	-
má	mít	k5eAaImIp3nS	mít
tentýž	týž	k3xTgInSc1	týž
poměr	poměr	k1gInSc1	poměr
jako	jako	k9	jako
státní	státní	k2eAgMnSc1d1	státní
občan	občan	k1gMnSc1	občan
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
k	k	k7c3	k
jinému	jiný	k2eAgInSc3d1	jiný
státu	stát	k1gInSc3	stát
<g/>
,	,	kIx,	,
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
cizinecký	cizinecký	k2eAgInSc1d1	cizinecký
režim	režim	k1gInSc1	režim
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
se	se	k3xPyFc4	se
cizineckým	cizinecký	k2eAgNnSc7d1	cizinecké
právem	právo	k1gNnSc7	právo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Bezdomovec	bezdomovec	k1gMnSc1	bezdomovec
(	(	kIx(	(
<g/>
apatrida	apatrida	k1gFnSc1	apatrida
<g/>
)	)	kIx)	)
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
bez	bez	k7c2	bez
státoobčanského	státoobčanský	k2eAgInSc2d1	státoobčanský
poměru	poměr	k1gInSc2	poměr
k	k	k7c3	k
jakémukoli	jakýkoli	k3yIgInSc3	jakýkoli
státu	stát	k1gInSc3	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Problém	problém	k1gInSc1	problém
bipolitismu	bipolitismus	k1gInSc2	bipolitismus
a	a	k8xC	a
apatrismu	apatrismus	k1gInSc2	apatrismus
==	==	k?	==
</s>
</p>
<p>
<s>
Bipolité	Bipolitý	k2eAgFnPc1d1	Bipolitý
jsou	být	k5eAaImIp3nP	být
osoby	osoba	k1gFnPc1	osoba
s	s	k7c7	s
dvojím	dvojit	k5eAaImIp1nS	dvojit
státním	státní	k2eAgNnSc7d1	státní
občanstvím	občanství	k1gNnSc7	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Dítě	dítě	k1gNnSc1	dítě
získá	získat	k5eAaPmIp3nS	získat
dvojí	dvojí	k4xRgNnSc4	dvojí
státní	státní	k2eAgNnSc4d1	státní
občanství	občanství	k1gNnSc4	občanství
např.	např.	kA	např.
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
narodilo	narodit	k5eAaPmAgNnS	narodit
rodičům	rodič	k1gMnPc3	rodič
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
státními	státní	k2eAgMnPc7d1	státní
občany	občan	k1gMnPc7	občan
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc2	jehož
právní	právní	k2eAgFnSc2d1	právní
normy	norma	k1gFnSc2	norma
upravující	upravující	k2eAgNnSc4d1	upravující
občanství	občanství	k1gNnSc4	občanství
vycházejí	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
principu	princip	k1gInSc2	princip
ius	ius	k?	ius
sanguinis	sanguinis	k1gInSc1	sanguinis
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
narodí	narodit	k5eAaPmIp3nP	narodit
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
jiného	jiný	k2eAgInSc2d1	jiný
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
svoji	svůj	k3xOyFgFnSc4	svůj
právní	právní	k2eAgFnSc4d1	právní
úpravu	úprava	k1gFnSc4	úprava
staví	stavit	k5eAaBmIp3nS	stavit
na	na	k7c6	na
principu	princip	k1gInSc6	princip
ius	ius	k?	ius
soli	sůl	k1gFnSc2	sůl
-	-	kIx~	-
narozené	narozený	k2eAgNnSc1d1	narozené
dítě	dítě	k1gNnSc1	dítě
tak	tak	k9	tak
získává	získávat	k5eAaImIp3nS	získávat
státní	státní	k2eAgNnSc4d1	státní
občanství	občanství	k1gNnSc4	občanství
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc7	jehož
občany	občan	k1gMnPc7	občan
jsou	být	k5eAaImIp3nP	být
rodiče	rodič	k1gMnPc1	rodič
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
občanství	občanství	k1gNnSc4	občanství
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
území	území	k1gNnSc6	území
se	se	k3xPyFc4	se
narodilo	narodit	k5eAaPmAgNnS	narodit
<g/>
.	.	kIx.	.
</s>
<s>
Dítě	dítě	k1gNnSc1	dítě
může	moct	k5eAaImIp3nS	moct
získat	získat	k5eAaPmF	získat
dvojí	dvojí	k4xRgNnSc4	dvojí
občanství	občanství	k1gNnSc4	občanství
rovněž	rovněž	k6eAd1	rovněž
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
občany	občan	k1gMnPc4	občan
různých	různý	k2eAgInPc2d1	různý
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
právní	právní	k2eAgInPc1d1	právní
řády	řád	k1gInPc1	řád
staví	stavit	k5eAaPmIp3nP	stavit
na	na	k7c6	na
principu	princip	k1gInSc6	princip
ius	ius	k?	ius
sanguinis	sanguinis	k1gFnSc2	sanguinis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Apatridé	Apatridý	k2eAgFnPc1d1	Apatridý
jsou	být	k5eAaImIp3nP	být
osoby	osoba	k1gFnPc1	osoba
bez	bez	k7c2	bez
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
bezdomovci	bezdomovec	k1gMnPc1	bezdomovec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Apatridy	Apatrid	k1gInPc1	Apatrid
se	se	k3xPyFc4	se
například	například	k6eAd1	například
stávají	stávat	k5eAaImIp3nP	stávat
děti	dítě	k1gFnPc4	dítě
apatridů	apatrid	k1gInPc2	apatrid
narozené	narozený	k2eAgFnPc4d1	narozená
na	na	k7c6	na
území	území	k1gNnSc6	území
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
svojí	svojit	k5eAaImIp3nS	svojit
právní	právní	k2eAgFnSc4d1	právní
úpravu	úprava	k1gFnSc4	úprava
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
staví	stavit	k5eAaImIp3nS	stavit
výlučně	výlučně	k6eAd1	výlučně
na	na	k7c6	na
principu	princip	k1gInSc6	princip
ius	ius	k?	ius
sanguinis	sanguinis	k1gInSc1	sanguinis
a	a	k8xC	a
děti	dítě	k1gFnPc1	dítě
tak	tak	k6eAd1	tak
nezískají	získat	k5eNaPmIp3nP	získat
občanství	občanství	k1gNnSc4	občanství
podle	podle	k7c2	podle
práva	právo	k1gNnSc2	právo
žádného	žádný	k3yNgInSc2	žádný
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
apatridy	apatrid	k1gInPc7	apatrid
není	být	k5eNaImIp3nS	být
vykonávána	vykonáván	k2eAgFnSc1d1	vykonávána
diplomatická	diplomatický	k2eAgFnSc1d1	diplomatická
ochrana	ochrana	k1gFnSc1	ochrana
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vyhoštěni	vyhostit	k5eAaPmNgMnP	vyhostit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jim	on	k3xPp3gMnPc3	on
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
povolen	povolit	k5eAaPmNgInS	povolit
vstup	vstup	k1gInSc1	vstup
na	na	k7c4	na
území	území	k1gNnSc4	území
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oba	dva	k4xCgInPc1	dva
tyto	tento	k3xDgInPc1	tento
jevy	jev	k1gInPc1	jev
se	se	k3xPyFc4	se
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
nežádoucí	žádoucí	k2eNgNnSc4d1	nežádoucí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Právní	právní	k2eAgFnSc1d1	právní
úprava	úprava	k1gFnSc1	úprava
občanství	občanství	k1gNnSc2	občanství
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
==	==	k?	==
</s>
</p>
<p>
<s>
O	o	k7c6	o
právní	právní	k2eAgFnSc6d1	právní
úpravě	úprava	k1gFnSc6	úprava
občanství	občanství	k1gNnSc2	občanství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
hned	hned	k6eAd1	hned
několik	několik	k4yIc1	několik
právních	právní	k2eAgInPc2d1	právní
předpisů	předpis	k1gInPc2	předpis
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Ústavy	ústava	k1gFnSc2	ústava
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
lze	lze	k6eAd1	lze
zmínit	zmínit	k5eAaPmF	zmínit
ustanovení	ustanovení	k1gNnSc4	ustanovení
čl	čl	kA	čl
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Nabývání	nabývání	k1gNnSc1	nabývání
a	a	k8xC	a
pozbývání	pozbývání	k1gNnSc1	pozbývání
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
stanoví	stanovit	k5eAaPmIp3nS	stanovit
zákon	zákon	k1gInSc1	zákon
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Nikdo	nikdo	k3yNnSc1	nikdo
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
proti	proti	k7c3	proti
své	svůj	k3xOyFgFnSc3	svůj
vůli	vůle	k1gFnSc3	vůle
zbaven	zbavit	k5eAaPmNgMnS	zbavit
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Listina	listina	k1gFnSc1	listina
základních	základní	k2eAgNnPc2d1	základní
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
svobod	svoboda	k1gFnPc2	svoboda
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
čl	čl	kA	čl
<g/>
.	.	kIx.	.
42	[number]	k4	42
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
3	[number]	k4	3
dodává	dodávat	k5eAaImIp3nS	dodávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
pokud	pokud	k8xS	pokud
dosavadní	dosavadní	k2eAgInPc1d1	dosavadní
předpisy	předpis	k1gInPc1	předpis
používají	používat	k5eAaImIp3nP	používat
pojmu	pojmout	k5eAaPmIp1nS	pojmout
"	"	kIx"	"
<g/>
občan	občan	k1gMnSc1	občan
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
rozumí	rozumět	k5eAaImIp3nS	rozumět
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
každý	každý	k3xTgMnSc1	každý
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
<g/>
-li	i	k?	-li
o	o	k7c4	o
základní	základní	k2eAgNnPc4d1	základní
práva	právo	k1gNnPc4	právo
a	a	k8xC	a
svobody	svoboda	k1gFnPc4	svoboda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
Listina	listina	k1gFnSc1	listina
přiznává	přiznávat	k5eAaImIp3nS	přiznávat
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
státní	státní	k2eAgNnSc4d1	státní
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
stěžejní	stěžejní	k2eAgInPc4d1	stěžejní
zákony	zákon	k1gInPc4	zákon
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
pojednávají	pojednávat	k5eAaImIp3nP	pojednávat
o	o	k7c4	o
občanství	občanství	k1gNnSc4	občanství
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
186	[number]	k4	186
<g/>
/	/	kIx~	/
<g/>
2013	[number]	k4	2013
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
státním	státní	k2eAgNnSc6d1	státní
občanství	občanství	k1gNnSc6	občanství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nabyl	nabýt	k5eAaPmAgInS	nabýt
účinnosti	účinnost	k1gFnSc2	účinnost
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
a	a	k8xC	a
nahradil	nahradit	k5eAaPmAgInS	nahradit
tak	tak	k6eAd1	tak
dosavadní	dosavadní	k2eAgInSc1d1	dosavadní
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
40	[number]	k4	40
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
nabývání	nabývání	k1gNnSc6	nabývání
a	a	k8xC	a
pozbývání	pozbývání	k1gNnSc6	pozbývání
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jak	jak	k8xC	jak
Ústava	ústava	k1gFnSc1	ústava
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
Listina	listina	k1gFnSc1	listina
pracují	pracovat	k5eAaImIp3nP	pracovat
s	s	k7c7	s
pojmem	pojem	k1gInSc7	pojem
občanství	občanství	k1gNnSc2	občanství
jako	jako	k8xS	jako
s	s	k7c7	s
pojmem	pojem	k1gInSc7	pojem
hotovým	hotový	k2eAgInSc7d1	hotový
<g/>
.	.	kIx.	.
</s>
<s>
Definici	definice	k1gFnSc4	definice
pojmu	pojem	k1gInSc2	pojem
však	však	k9	však
provedl	provést	k5eAaPmAgInS	provést
až	až	k9	až
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
v	v	k7c4	v
odůvodnění	odůvodnění	k1gNnSc4	odůvodnění
svého	svůj	k3xOyFgInSc2	svůj
nálezu	nález	k1gInSc2	nález
z	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
sp	sp	k?	sp
<g/>
.	.	kIx.	.
zn.	zn.	kA	zn.
Pl.	Pl.	k1gFnSc2	Pl.
ÚS	ÚS	kA	ÚS
9	[number]	k4	9
<g/>
/	/	kIx~	/
<g/>
94	[number]	k4	94
(	(	kIx(	(
<g/>
publikován	publikovat	k5eAaBmNgInS	publikovat
pod	pod	k7c4	pod
č.	č.	k?	č.
207	[number]	k4	207
<g/>
/	/	kIx~	/
<g/>
1994	[number]	k4	1994
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
charakterizoval	charakterizovat	k5eAaBmAgMnS	charakterizovat
jej	on	k3xPp3gInSc4	on
<g />
.	.	kIx.	.
</s>
<s>
jako	jako	k9	jako
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
časově	časově	k6eAd1	časově
trvalý	trvalý	k2eAgInSc1d1	trvalý
<g/>
,	,	kIx,	,
místně	místně	k6eAd1	místně
neomezený	omezený	k2eNgInSc4d1	neomezený
právní	právní	k2eAgInSc4d1	právní
vztah	vztah	k1gInSc4	vztah
fyzické	fyzický	k2eAgFnSc2d1	fyzická
osoby	osoba	k1gFnSc2	osoba
a	a	k8xC	a
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
vůli	vůle	k1gFnSc3	vůle
fyzické	fyzický	k2eAgFnSc2d1	fyzická
osoby	osoba	k1gFnSc2	osoba
zpravidla	zpravidla	k6eAd1	zpravidla
nezrušitelný	zrušitelný	k2eNgInSc1d1	nezrušitelný
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
základě	základ	k1gInSc6	základ
vznikají	vznikat	k5eAaImIp3nP	vznikat
jeho	on	k3xPp3gInSc4	on
subjektům	subjekt	k1gInPc3	subjekt
vzájemná	vzájemný	k2eAgNnPc4d1	vzájemné
práva	právo	k1gNnPc4	právo
a	a	k8xC	a
povinnosti	povinnost	k1gFnPc4	povinnost
<g/>
,	,	kIx,	,
spočívající	spočívající	k2eAgInPc4d1	spočívající
zejména	zejména	k9	zejména
v	v	k7c6	v
právu	právo	k1gNnSc6	právo
fyzické	fyzický	k2eAgFnSc2d1	fyzická
osoby	osoba	k1gFnSc2	osoba
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
státu	stát	k1gInSc2	stát
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
území	území	k1gNnSc4	území
i	i	k9	i
mimo	mimo	k7c4	mimo
ně	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
v	v	k7c6	v
právu	právo	k1gNnSc6	právo
pobytu	pobyt	k1gInSc2	pobyt
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
území	území	k1gNnSc6	území
a	a	k8xC	a
na	na	k7c6	na
právu	právo	k1gNnSc6	právo
účasti	účast	k1gFnSc2	účast
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
správě	správa	k1gFnSc6	správa
veřejných	veřejný	k2eAgFnPc2d1	veřejná
záležitostí	záležitost	k1gFnPc2	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Povinností	povinnost	k1gFnSc7	povinnost
občana	občan	k1gMnSc2	občan
je	být	k5eAaImIp3nS	být
především	především	k9	především
věrnost	věrnost	k1gFnSc1	věrnost
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
závazek	závazek	k1gInSc1	závazek
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
obraně	obrana	k1gFnSc3	obrana
<g/>
,	,	kIx,	,
výkon	výkon	k1gInSc4	výkon
určitých	určitý	k2eAgFnPc2d1	určitá
funkcí	funkce	k1gFnPc2	funkce
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yQgFnPc3	který
je	být	k5eAaImIp3nS	být
povolán	povolán	k2eAgMnSc1d1	povolán
<g/>
,	,	kIx,	,
a	a	k8xC	a
dodržování	dodržování	k1gNnSc1	dodržování
právních	právní	k2eAgInPc2d1	právní
předpisů	předpis	k1gInPc2	předpis
státu	stát	k1gInSc2	stát
i	i	k8xC	i
mimo	mimo	k7c4	mimo
jeho	jeho	k3xOp3gNnSc4	jeho
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
Ústavní	ústavní	k2eAgInSc4d1	ústavní
soud	soud	k1gInSc4	soud
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
dodal	dodat	k5eAaPmAgMnS	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
obsah	obsah	k1gInSc4	obsah
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
zákonodárstvím	zákonodárství	k1gNnSc7	zákonodárství
jednotlivého	jednotlivý	k2eAgInSc2d1	jednotlivý
svrchovaného	svrchovaný	k2eAgInSc2d1	svrchovaný
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
výsostným	výsostný	k2eAgNnSc7d1	výsostné
právem	právo	k1gNnSc7	právo
státu	stát	k1gInSc2	stát
určovat	určovat	k5eAaImF	určovat
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
za	za	k7c2	za
kterých	který	k3yRgInPc2	který
se	se	k3xPyFc4	se
nabývá	nabývat	k5eAaImIp3nS	nabývat
a	a	k8xC	a
pozbývá	pozbývat	k5eAaImIp3nS	pozbývat
státní	státní	k2eAgNnSc4d1	státní
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
řečeno	řečen	k2eAgNnSc1d1	řečeno
zejména	zejména	k9	zejména
<g/>
,	,	kIx,	,
že	že	k8xS	že
státní	státní	k2eAgNnSc1d1	státní
občanství	občanství	k1gNnSc1	občanství
je	být	k5eAaImIp3nS	být
regulováno	regulovat	k5eAaImNgNnS	regulovat
právním	právní	k2eAgInSc7d1	právní
řádem	řád	k1gInSc7	řád
jednotlivého	jednotlivý	k2eAgInSc2d1	jednotlivý
státu	stát	k1gInSc2	stát
–	–	k?	–
vzniká	vznikat	k5eAaImIp3nS	vznikat
<g/>
,	,	kIx,	,
trvá	trvat	k5eAaImIp3nS	trvat
i	i	k8xC	i
zaniká	zanikat	k5eAaImIp3nS	zanikat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Získání	získání	k1gNnSc1	získání
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
==	==	k?	==
</s>
</p>
<p>
<s>
Obecně	obecně	k6eAd1	obecně
můžeme	moct	k5eAaImIp1nP	moct
nabýt	nabýt	k5eAaPmF	nabýt
státní	státní	k2eAgNnSc1d1	státní
občanství	občanství	k1gNnSc1	občanství
následujícími	následující	k2eAgInPc7d1	následující
způsoby	způsob	k1gInPc7	způsob
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Narozením	narození	k1gNnSc7	narození
-	-	kIx~	-
Právní	právní	k2eAgFnSc1d1	právní
kultura	kultura	k1gFnSc1	kultura
kontinentální	kontinentální	k2eAgFnSc2d1	kontinentální
Evropy	Evropa	k1gFnSc2	Evropa
preferuje	preferovat	k5eAaImIp3nS	preferovat
zásadu	zásada	k1gFnSc4	zásada
ius	ius	k?	ius
sanguinis	sanguinis	k1gFnSc1	sanguinis
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
právo	právo	k1gNnSc1	právo
krve	krev	k1gFnSc2	krev
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
vznik	vznik	k1gInSc1	vznik
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
podle	podle	k7c2	podle
pokrevní	pokrevní	k2eAgFnSc2d1	pokrevní
příbuznosti	příbuznost	k1gFnSc2	příbuznost
-	-	kIx~	-
podle	podle	k7c2	podle
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Právní	právní	k2eAgFnSc1d1	právní
kultura	kultura	k1gFnSc1	kultura
angloamerická	angloamerický	k2eAgFnSc1d1	angloamerická
preferuje	preferovat	k5eAaImIp3nS	preferovat
zásadu	zásada	k1gFnSc4	zásada
ius	ius	k?	ius
soli	sůl	k1gFnSc2	sůl
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
právo	právo	k1gNnSc1	právo
místa	místo	k1gNnSc2	místo
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
vznik	vznik	k1gInSc1	vznik
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
podle	podle	k7c2	podle
místa	místo	k1gNnSc2	místo
narození	narození	k1gNnSc2	narození
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Naturalizací	naturalizace	k1gFnPc2	naturalizace
-	-	kIx~	-
do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
způsobu	způsob	k1gInSc2	způsob
patří	patřit	k5eAaImIp3nS	patřit
sňatek	sňatek	k1gInSc1	sňatek
<g/>
,	,	kIx,	,
osvojení	osvojení	k1gNnSc1	osvojení
<g/>
,	,	kIx,	,
určení	určení	k1gNnSc1	určení
otcovství	otcovství	k1gNnSc2	otcovství
<g/>
,	,	kIx,	,
legitimace	legitimace	k1gFnSc1	legitimace
<g/>
,	,	kIx,	,
udělení	udělení	k1gNnSc1	udělení
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
<g/>
,	,	kIx,	,
přijetí	přijetí	k1gNnSc2	přijetí
do	do	k7c2	do
státní	státní	k2eAgFnSc2d1	státní
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
nalezení	nalezení	k1gNnSc1	nalezení
apod.	apod.	kA	apod.
V	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
ústavním	ústavní	k2eAgNnSc6d1	ústavní
právu	právo	k1gNnSc6	právo
je	být	k5eAaImIp3nS	být
pojetí	pojetí	k1gNnSc1	pojetí
naturalizace	naturalizace	k1gFnSc2	naturalizace
zúženo	zúžen	k2eAgNnSc1d1	zúženo
jen	jen	k9	jen
na	na	k7c6	na
nabytí	nabytí	k1gNnSc6	nabytí
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
udělením	udělení	k1gNnSc7	udělení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Opcí	opce	k1gFnPc2	opce
-	-	kIx~	-
tou	ten	k3xDgFnSc7	ten
se	se	k3xPyFc4	se
rozumí	rozumět	k5eAaImIp3nS	rozumět
právo	právo	k1gNnSc1	právo
volby	volba	k1gFnSc2	volba
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgMnPc7	dva
či	či	k8xC	či
více	hodně	k6eAd2	hodně
státními	státní	k2eAgNnPc7d1	státní
občanstvími	občanství	k1gNnPc7	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
změně	změna	k1gFnSc6	změna
suveréna	suverén	k1gMnSc2	suverén
státního	státní	k2eAgNnSc2d1	státní
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
vojenské	vojenský	k2eAgFnSc2d1	vojenská
okupace	okupace	k1gFnSc2	okupace
či	či	k8xC	či
anexe	anexe	k1gFnSc2	anexe
cizího	cizí	k2eAgInSc2d1	cizí
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
považovat	považovat	k5eAaImF	považovat
nové	nový	k2eAgNnSc4d1	nové
vynucené	vynucený	k2eAgNnSc4d1	vynucené
státní	státní	k2eAgNnSc4d1	státní
občanství	občanství	k1gNnSc4	občanství
okupujícího	okupující	k2eAgInSc2d1	okupující
státu	stát	k1gInSc2	stát
za	za	k7c4	za
neplatné	platný	k2eNgNnSc4d1	neplatné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Získání	získání	k1gNnSc1	získání
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
v	v	k7c6	v
ČR	ČR	kA	ČR
===	===	k?	===
</s>
</p>
<p>
<s>
Získání	získání	k1gNnSc1	získání
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
je	být	k5eAaImIp3nS	být
upraveno	upravit	k5eAaPmNgNnS	upravit
v	v	k7c6	v
zákoně	zákon	k1gInSc6	zákon
č.	č.	k?	č.
186	[number]	k4	186
<g/>
/	/	kIx~	/
<g/>
2013	[number]	k4	2013
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
státním	státní	k2eAgNnSc6d1	státní
občanství	občanství	k1gNnSc6	občanství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
zákona	zákon	k1gInSc2	zákon
lze	lze	k6eAd1	lze
státní	státní	k2eAgNnSc4d1	státní
občanství	občanství	k1gNnSc4	občanství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
nabýt	nabýt	k5eAaPmF	nabýt
následujícími	následující	k2eAgInPc7d1	následující
způsoby	způsob	k1gInPc7	způsob
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Narozením	narození	k1gNnSc7	narození
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
v	v	k7c4	v
den	den	k1gInSc4	den
narození	narození	k1gNnSc4	narození
dítěte	dítě	k1gNnSc2	dítě
alespoň	alespoň	k9	alespoň
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
rodičů	rodič	k1gMnPc2	rodič
státním	státní	k2eAgMnSc7d1	státní
občanem	občan	k1gMnSc7	občan
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgNnSc1d1	státní
občanství	občanství	k1gNnSc1	občanství
zároveň	zároveň	k6eAd1	zároveň
nabývá	nabývat	k5eAaImIp3nS	nabývat
narozením	narození	k1gNnSc7	narození
i	i	k8xC	i
dítě	dítě	k1gNnSc1	dítě
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
narodí	narodit	k5eAaPmIp3nS	narodit
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
území	území	k1gNnSc6	území
a	a	k8xC	a
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
jinak	jinak	k6eAd1	jinak
stalo	stát	k5eAaPmAgNnS	stát
osobou	osoba	k1gFnSc7	osoba
bez	bez	k7c2	bez
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
(	(	kIx(	(
<g/>
bezdomovcem	bezdomovec	k1gMnSc7	bezdomovec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
dítěte	dítě	k1gNnSc2	dítě
bezdomovci	bezdomovec	k1gMnSc3	bezdomovec
a	a	k8xC	a
alespoň	alespoň	k9	alespoň
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
ke	k	k7c3	k
dni	den	k1gInSc3	den
narození	narození	k1gNnSc2	narození
dítěte	dítě	k1gNnSc2	dítě
povolen	povolit	k5eAaPmNgInS	povolit
pobyt	pobyt	k1gInSc1	pobyt
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
delší	dlouhý	k2eAgFnSc4d2	delší
než	než	k8xS	než
90	[number]	k4	90
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Osvojením	osvojení	k1gNnSc7	osvojení
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
alespoň	alespoň	k9	alespoň
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
osvojitelů	osvojitel	k1gMnPc2	osvojitel
státním	státní	k2eAgMnSc7d1	státní
občanem	občan	k1gMnSc7	občan
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dnem	den	k1gInSc7	den
nabytí	nabytí	k1gNnSc2	nabytí
právní	právní	k2eAgFnSc2d1	právní
moci	moc	k1gFnSc2	moc
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
o	o	k7c6	o
osvojení	osvojení	k1gNnSc6	osvojení
<g/>
.	.	kIx.	.
</s>
<s>
Dítě	dítě	k1gNnSc1	dítě
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
osvojeno	osvojit	k5eAaPmNgNnS	osvojit
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
orgánu	orgán	k1gInSc2	orgán
cizího	cizí	k2eAgInSc2d1	cizí
státu	stát	k1gInSc2	stát
a	a	k8xC	a
alespoň	alespoň	k9	alespoň
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
osvojitelů	osvojitel	k1gMnPc2	osvojitel
je	být	k5eAaImIp3nS	být
státní	státní	k2eAgMnSc1d1	státní
občan	občan	k1gMnSc1	občan
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
nabývá	nabývat	k5eAaImIp3nS	nabývat
státní	státní	k2eAgNnSc1d1	státní
občanství	občanství	k1gNnSc1	občanství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
dnem	dnem	k7c2	dnem
nabytí	nabytí	k1gNnSc2	nabytí
právní	právní	k2eAgFnSc2d1	právní
moci	moc	k1gFnSc2	moc
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
o	o	k7c4	o
uznání	uznání	k1gNnSc4	uznání
osvojení	osvojení	k1gNnSc2	osvojení
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
bylo	být	k5eAaImAgNnS	být
osvojení	osvojení	k1gNnSc1	osvojení
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
právu	právo	k1gNnSc6	právo
soukromém	soukromý	k2eAgNnSc6d1	soukromé
uznáno	uznat	k5eAaPmNgNnS	uznat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Určením	určení	k1gNnSc7	určení
otcovství	otcovství	k1gNnSc2	otcovství
-	-	kIx~	-
pokud	pokud	k8xS	pokud
matka	matka	k1gFnSc1	matka
dítěte	dítě	k1gNnSc2	dítě
není	být	k5eNaImIp3nS	být
státní	státní	k2eAgFnSc7d1	státní
občankou	občanka	k1gFnSc7	občanka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
otec	otec	k1gMnSc1	otec
ano	ano	k9	ano
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
dítě	dítě	k1gNnSc1	dítě
nabývá	nabývat	k5eAaImIp3nS	nabývat
státní	státní	k2eAgNnSc4d1	státní
občanství	občanství	k1gNnSc4	občanství
ČR	ČR	kA	ČR
dnem	dnem	k7c2	dnem
prohlášení	prohlášení	k1gNnSc2	prohlášení
rodičů	rodič	k1gMnPc2	rodič
o	o	k7c4	o
určení	určení	k1gNnSc4	určení
otcovství	otcovství	k1gNnSc2	otcovství
nebo	nebo	k8xC	nebo
dnem	den	k1gInSc7	den
právní	právní	k2eAgFnSc2d1	právní
moci	moc	k1gFnSc2	moc
rozsudku	rozsudek	k1gInSc2	rozsudek
o	o	k7c4	o
určení	určení	k1gNnSc4	určení
otcovství	otcovství	k1gNnSc2	otcovství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nalezením	nalezení	k1gNnSc7	nalezení
na	na	k7c4	na
území	území	k1gNnSc4	území
ČR	ČR	kA	ČR
-	-	kIx~	-
dítě	dítě	k1gNnSc1	dítě
mladší	mladý	k2eAgMnPc1d2	mladší
3	[number]	k4	3
let	léto	k1gNnPc2	léto
nalezené	nalezený	k2eAgFnSc2d1	nalezená
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
totožnost	totožnost	k1gFnSc1	totožnost
se	se	k3xPyFc4	se
nepodaří	podařit	k5eNaPmIp3nS	podařit
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
nabývá	nabývat	k5eAaImIp3nS	nabývat
státní	státní	k2eAgNnSc1d1	státní
občanství	občanství	k1gNnSc1	občanství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
dnem	dnem	k7c2	dnem
nalezení	nalezení	k1gNnSc2	nalezení
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
do	do	k7c2	do
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
ode	ode	k7c2	ode
dne	den	k1gInSc2	den
nalezení	nalezení	k1gNnSc2	nalezení
nevyjde	vyjít	k5eNaPmIp3nS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
nabylo	nabýt	k5eAaPmAgNnS	nabýt
státní	státní	k2eAgNnSc1d1	státní
občanství	občanství	k1gNnSc1	občanství
jiného	jiný	k2eAgInSc2d1	jiný
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prohlášením	prohlášení	k1gNnSc7	prohlášení
si	se	k3xPyFc3	se
může	moct	k5eAaImIp3nS	moct
zvolit	zvolit	k5eAaPmF	zvolit
státní	státní	k2eAgNnSc4d1	státní
občanství	občanství	k1gNnSc4	občanství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
fyzická	fyzický	k2eAgFnSc1d1	fyzická
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc3	prosinec
1992	[number]	k4	1992
státním	státní	k2eAgMnSc7d1	státní
občanem	občan	k1gMnSc7	občan
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
Federativní	federativní	k2eAgFnSc2d1	federativní
Republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neměla	mít	k5eNaImAgFnS	mít
státní	státní	k2eAgNnSc4d1	státní
občanství	občanství	k1gNnSc4	občanství
ČR	ČR	kA	ČR
ani	ani	k8xC	ani
státní	státní	k2eAgNnSc1d1	státní
občanství	občanství	k1gNnSc1	občanství
SR	SR	kA	SR
<g/>
.	.	kIx.	.
</s>
<s>
Nabytí	nabytí	k1gNnSc1	nabytí
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
není	být	k5eNaImIp3nS	být
umožněno	umožnit	k5eAaPmNgNnS	umožnit
fyzické	fyzický	k2eAgFnSc3d1	fyzická
osobě	osoba	k1gFnSc3	osoba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
ke	k	k7c3	k
dni	den	k1gInSc3	den
učinění	učinění	k1gNnSc2	učinění
prohlášení	prohlášení	k1gNnSc4	prohlášení
státním	státní	k2eAgMnSc7d1	státní
občanem	občan	k1gMnSc7	občan
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Prohlášení	prohlášení	k1gNnSc1	prohlášení
může	moct	k5eAaImIp3nS	moct
samostatně	samostatně	k6eAd1	samostatně
učinit	učinit	k5eAaPmF	učinit
fyzická	fyzický	k2eAgFnSc1d1	fyzická
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nabyla	nabýt	k5eAaPmAgFnS	nabýt
zletilosti	zletilost	k1gFnPc4	zletilost
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
prohlášení	prohlášení	k1gNnSc2	prohlášení
zahrnout	zahrnout	k5eAaPmF	zahrnout
i	i	k9	i
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
učinit	učinit	k5eAaImF	učinit
pro	pro	k7c4	pro
dítě	dítě	k1gNnSc4	dítě
samostatné	samostatný	k2eAgNnSc1d1	samostatné
prohlášení	prohlášení	k1gNnSc1	prohlášení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Udělením	udělení	k1gNnSc7	udělení
(	(	kIx(	(
<g/>
naturalizací	naturalizace	k1gFnPc2	naturalizace
<g/>
)	)	kIx)	)
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
podanou	podaný	k2eAgFnSc4d1	podaná
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
krajského	krajský	k2eAgInSc2d1	krajský
úřadu	úřad	k1gInSc2	úřad
(	(	kIx(	(
<g/>
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
)	)	kIx)	)
Ministerstvu	ministerstvo	k1gNnSc3	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
ČR	ČR	kA	ČR
po	po	k7c6	po
složení	složení	k1gNnSc6	složení
státoobčanského	státoobčanský	k2eAgInSc2d1	státoobčanský
slibu	slib	k1gInSc2	slib
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
udělení	udělení	k1gNnSc4	udělení
občanství	občanství	k1gNnSc2	občanství
musí	muset	k5eAaImIp3nP	muset
žadatel	žadatel	k1gMnSc1	žadatel
splňovat	splňovat	k5eAaImF	splňovat
následující	následující	k2eAgFnPc4d1	následující
podmínky	podmínka	k1gFnPc4	podmínka
(	(	kIx(	(
<g/>
některé	některý	k3yIgFnPc4	některý
podmínky	podmínka	k1gFnPc4	podmínka
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
prominout	prominout	k5eAaPmF	prominout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
při	při	k7c6	při
splnění	splnění	k1gNnSc6	splnění
všech	všecek	k3xTgFnPc2	všecek
podmínek	podmínka	k1gFnPc2	podmínka
není	být	k5eNaImIp3nS	být
na	na	k7c6	na
udělení	udělení	k1gNnSc6	udělení
občanství	občanství	k1gNnSc2	občanství
právní	právní	k2eAgInSc4d1	právní
nárok	nárok	k1gInSc4	nárok
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
ke	k	k7c3	k
dni	den	k1gInSc3	den
podání	podání	k1gNnSc2	podání
žádosti	žádost	k1gFnSc2	žádost
povolen	povolen	k2eAgInSc4d1	povolen
trvalý	trvalý	k2eAgInSc4d1	trvalý
pobyt	pobyt	k1gInSc4	pobyt
nepřetržitě	přetržitě	k6eNd1	přetržitě
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
alespoň	alespoň	k9	alespoň
5	[number]	k4	5
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
3	[number]	k4	3
let	léto	k1gNnPc2	léto
u	u	k7c2	u
občanů	občan	k1gMnPc2	občan
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Musí	muset	k5eAaImIp3nS	muset
zároveň	zároveň	k6eAd1	zároveň
prokázat	prokázat	k5eAaPmF	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
alespoň	alespoň	k9	alespoň
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
jedné	jeden	k4xCgFnSc2	jeden
poloviny	polovina	k1gFnSc2	polovina
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nebyl	být	k5eNaImAgInS	být
pravomocně	pravomocně	k6eAd1	pravomocně
odsouzen	odsoudit	k5eAaPmNgInS	odsoudit
pro	pro	k7c4	pro
nedbalostní	nedbalostní	k2eAgInSc4d1	nedbalostní
trestný	trestný	k2eAgInSc4d1	trestný
čin	čin	k1gInSc4	čin
k	k	k7c3	k
nepodmíněnému	podmíněný	k2eNgInSc3d1	nepodmíněný
trestu	trest	k1gInSc3	trest
odnětí	odnětí	k1gNnSc2	odnětí
svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
úmyslný	úmyslný	k2eAgInSc4d1	úmyslný
trestný	trestný	k2eAgInSc4d1	trestný
čin	čin	k1gInSc4	čin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prokáže	prokázat	k5eAaPmIp3nS	prokázat
znalost	znalost	k1gFnSc1	znalost
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prokáže	prokázat	k5eAaPmIp3nS	prokázat
základní	základní	k2eAgFnSc1d1	základní
znalost	znalost	k1gFnSc1	znalost
ústavního	ústavní	k2eAgInSc2d1	ústavní
systému	systém	k1gInSc2	systém
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
základní	základní	k2eAgFnSc4d1	základní
orientaci	orientace	k1gFnSc4	orientace
v	v	k7c6	v
kulturně-společenských	kulturněpolečenský	k2eAgFnPc6d1	kulturně-společenská
<g/>
,	,	kIx,	,
zeměpisných	zeměpisný	k2eAgFnPc6d1	zeměpisná
a	a	k8xC	a
historických	historický	k2eAgFnPc6d1	historická
reáliích	reálie	k1gFnPc6	reálie
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
3	[number]	k4	3
letech	léto	k1gNnPc6	léto
předcházejících	předcházející	k2eAgInPc2d1	předcházející
dni	den	k1gInSc3	den
podání	podání	k1gNnSc2	podání
žádosti	žádost	k1gFnSc2	žádost
neporušil	porušit	k5eNaPmAgInS	porušit
závažným	závažný	k2eAgInSc7d1	závažný
způsobem	způsob	k1gInSc7	způsob
povinnosti	povinnost	k1gFnSc2	povinnost
vyplývající	vyplývající	k2eAgFnSc2d1	vyplývající
z	z	k7c2	z
právních	právní	k2eAgInPc2d1	právní
předpisů	předpis	k1gInPc2	předpis
upravujících	upravující	k2eAgInPc2d1	upravující
vstup	vstup	k1gInSc4	vstup
a	a	k8xC	a
pobyt	pobyt	k1gInSc4	pobyt
cizinců	cizinec	k1gMnPc2	cizinec
na	na	k7c4	na
území	území	k1gNnSc4	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
veřejné	veřejný	k2eAgNnSc4d1	veřejné
zdravotní	zdravotní	k2eAgNnSc4d1	zdravotní
pojištění	pojištění	k1gNnSc4	pojištění
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgNnSc4d1	sociální
zabezpečení	zabezpečení	k1gNnSc4	zabezpečení
<g/>
,	,	kIx,	,
důchodové	důchodový	k2eAgNnSc4d1	důchodové
pojištění	pojištění	k1gNnSc4	pojištění
<g/>
,	,	kIx,	,
zaměstnanost	zaměstnanost	k1gFnSc4	zaměstnanost
<g/>
,	,	kIx,	,
daně	daň	k1gFnPc1	daň
<g/>
,	,	kIx,	,
cla	clo	k1gNnPc1	clo
<g/>
,	,	kIx,	,
odvody	odvod	k1gInPc1	odvod
a	a	k8xC	a
poplatky	poplatek	k1gInPc1	poplatek
<g/>
,	,	kIx,	,
vyživovací	vyživovací	k2eAgFnSc1d1	vyživovací
povinnost	povinnost	k1gFnSc1	povinnost
vůči	vůči	k7c3	vůči
dítěti	dítě	k1gNnSc3	dítě
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
má	mít	k5eAaImIp3nS	mít
trvalý	trvalý	k2eAgInSc4d1	trvalý
pobyt	pobyt	k1gInSc4	pobyt
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
veřejnoprávní	veřejnoprávní	k2eAgFnSc2d1	veřejnoprávní
povinnosti	povinnost	k1gFnSc2	povinnost
k	k	k7c3	k
obci	obec	k1gFnSc3	obec
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
je	být	k5eAaImIp3nS	být
žadatel	žadatel	k1gMnSc1	žadatel
přihlášen	přihlásit	k5eAaPmNgMnS	přihlásit
k	k	k7c3	k
pobytu	pobyt	k1gInSc3	pobyt
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
<g/>
-li	i	k?	-li
o	o	k7c4	o
povinnosti	povinnost	k1gFnPc4	povinnost
uložené	uložený	k2eAgFnPc4d1	uložená
obcí	obec	k1gFnSc7	obec
v	v	k7c4	v
samostatné	samostatný	k2eAgFnPc4d1	samostatná
působnosti	působnost	k1gFnPc4	působnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prokáže	prokázat	k5eAaPmIp3nS	prokázat
výši	výše	k1gFnSc4	výše
a	a	k8xC	a
zdroje	zdroj	k1gInPc4	zdroj
svých	svůj	k3xOyFgInPc2	svůj
příjmů	příjem	k1gInPc2	příjem
za	za	k7c4	za
poslední	poslední	k2eAgInPc4d1	poslední
3	[number]	k4	3
roky	rok	k1gInPc4	rok
a	a	k8xC	a
doloží	doložit	k5eAaPmIp3nP	doložit
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
odvádí	odvádět	k5eAaImIp3nS	odvádět
daň	daň	k1gFnSc1	daň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prokáže	prokázat	k5eAaPmIp3nS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
pobyt	pobyt	k1gInSc1	pobyt
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
3	[number]	k4	3
letech	léto	k1gNnPc6	léto
výrazně	výrazně	k6eAd1	výrazně
a	a	k8xC	a
bez	bez	k7c2	bez
vážných	vážný	k2eAgInPc2d1	vážný
důvodů	důvod	k1gInPc2	důvod
nezatěžoval	zatěžovat	k5eNaImAgInS	zatěžovat
systém	systém	k1gInSc1	systém
státní	státní	k2eAgFnSc2d1	státní
sociální	sociální	k2eAgFnSc2d1	sociální
podpory	podpora	k1gFnSc2	podpora
nebo	nebo	k8xC	nebo
systém	systém	k1gInSc1	systém
pomoci	pomoc	k1gFnSc2	pomoc
v	v	k7c6	v
hmotné	hmotný	k2eAgFnSc6d1	hmotná
nouzi	nouze	k1gFnSc6	nouze
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
Minsterstva	Minsterstvo	k1gNnSc2	Minsterstvo
vnitra	vnitro	k1gNnSc2	vnitro
byl	být	k5eAaImAgMnS	být
postup	postup	k1gInSc4	postup
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dítě	dítě	k1gNnSc1	dítě
cizinky	cizinka	k1gFnSc2	cizinka
získalo	získat	k5eAaPmAgNnS	získat
občanství	občanství	k1gNnSc1	občanství
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
muž	muž	k1gMnSc1	muž
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
<g/>
,	,	kIx,	,
zneužíván	zneužívat	k5eAaImNgInS	zneužívat
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
proto	proto	k8xC	proto
navrhlo	navrhnout	k5eAaPmAgNnS	navrhnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
svého	svůj	k3xOyFgNnSc2	svůj
dítěte	dítě	k1gNnSc2	dítě
neoženili	oženit	k5eNaPmAgMnP	oženit
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgNnSc4	svůj
otcovství	otcovství	k1gNnSc4	otcovství
doložili	doložit	k5eAaPmAgMnP	doložit
testy	test	k1gInPc4	test
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc4	návrh
zákona	zákon	k1gInSc2	zákon
byl	být	k5eAaImAgMnS	být
kritizován	kritizován	k2eAgMnSc1d1	kritizován
jako	jako	k9	jako
diskriminační	diskriminační	k2eAgFnSc4d1	diskriminační
<g/>
,	,	kIx,	,
upřednostňující	upřednostňující	k2eAgFnSc4d1	upřednostňující
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
zájmem	zájem	k1gInSc7	zájem
dítěte	dítě	k1gNnSc2	dítě
a	a	k8xC	a
nejasný	jasný	k2eNgInSc1d1	nejasný
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
testy	test	k1gInPc1	test
DNA	dno	k1gNnSc2	dno
nebudou	být	k5eNaImBp3nP	být
možné	možný	k2eAgInPc1d1	možný
kvůli	kvůli	k7c3	kvůli
úmrtí	úmrtí	k1gNnSc3	úmrtí
otce	otec	k1gMnSc2	otec
nebo	nebo	k8xC	nebo
finanční	finanční	k2eAgFnSc6d1	finanční
tísni	tíseň	k1gFnSc6	tíseň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zánik	zánik	k1gInSc1	zánik
občanství	občanství	k1gNnSc2	občanství
==	==	k?	==
</s>
</p>
<p>
<s>
Pozbytí	pozbytí	k1gNnSc1	pozbytí
občanství	občanství	k1gNnSc2	občanství
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
expatriace	expatriace	k1gFnSc1	expatriace
<g/>
.	.	kIx.	.
</s>
<s>
Ztratit	ztratit	k5eAaPmF	ztratit
státní	státní	k2eAgNnSc4d1	státní
občanství	občanství	k1gNnSc4	občanství
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc4d1	možné
např.	např.	kA	např.
nabytím	nabytí	k1gNnSc7	nabytí
cizího	cizí	k2eAgNnSc2d1	cizí
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
žádost	žádost	k1gFnSc4	žádost
<g/>
,	,	kIx,	,
sňatkem	sňatek	k1gInSc7	sňatek
<g/>
,	,	kIx,	,
propuštěním	propuštění	k1gNnSc7	propuštění
ze	z	k7c2	z
státního	státní	k2eAgInSc2d1	státní
svazku	svazek	k1gInSc2	svazek
<g/>
,	,	kIx,	,
prohlášením	prohlášení	k1gNnSc7	prohlášení
nabytí	nabytí	k1gNnSc2	nabytí
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
za	za	k7c4	za
neplatné	platný	k2eNgNnSc4d1	neplatné
apod.	apod.	kA	apod.
Článek	článek	k1gInSc4	článek
15	[number]	k4	15
Všeobecné	všeobecný	k2eAgFnSc2d1	všeobecná
deklarace	deklarace	k1gFnSc2	deklarace
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nikdo	nikdo	k3yNnSc1	nikdo
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
proti	proti	k7c3	proti
své	svůj	k3xOyFgFnSc3	svůj
vůli	vůle	k1gFnSc3	vůle
zbaven	zbavit	k5eAaPmNgMnS	zbavit
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
přináší	přinášet	k5eAaImIp3nS	přinášet
významnou	významný	k2eAgFnSc4d1	významná
garanci	garance	k1gFnSc4	garance
státoobčanského	státoobčanský	k2eAgInSc2d1	státoobčanský
statusu	status	k1gInSc2	status
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zánik	zánik	k1gInSc4	zánik
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
v	v	k7c6	v
ČR	ČR	kA	ČR
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgMnSc1d1	státní
občan	občan	k1gMnSc1	občan
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
pozbývá	pozbývat	k5eAaImIp3nS	pozbývat
státní	státní	k2eAgNnSc4d1	státní
občanství	občanství	k1gNnSc4	občanství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
prohlášením	prohlášení	k1gNnSc7	prohlášení
o	o	k7c6	o
vzdání	vzdání	k1gNnSc6	vzdání
se	se	k3xPyFc4	se
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
splňuje	splňovat	k5eAaImIp3nS	splňovat
současně	současně	k6eAd1	současně
tyto	tento	k3xDgFnPc4	tento
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
trvale	trvale	k6eAd1	trvale
se	se	k3xPyFc4	se
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
<g/>
,	,	kIx,	,
<g/>
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
přihlášen	přihlásit	k5eAaPmNgInS	přihlásit
k	k	k7c3	k
trvalému	trvalý	k2eAgInSc3d1	trvalý
pobytu	pobyt	k1gInSc3	pobyt
aje	aje	k?	aje
zároveň	zároveň	k6eAd1	zároveň
státním	státní	k2eAgMnSc7d1	státní
občanem	občan	k1gMnSc7	občan
cizího	cizí	k2eAgInSc2d1	cizí
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
žádá	žádat	k5eAaImIp3nS	žádat
o	o	k7c6	o
nabytí	nabytí	k1gNnSc6	nabytí
cizího	cizí	k2eAgNnSc2d1	cizí
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
a	a	k8xC	a
prohlášení	prohlášení	k1gNnSc2	prohlášení
o	o	k7c4	o
vzdání	vzdání	k1gNnSc4	vzdání
se	se	k3xPyFc4	se
státního	státní	k2eAgInSc2d1	státní
občanství	občanství	k1gNnSc4	občanství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
činí	činit	k5eAaImIp3nS	činit
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
nabytím	nabytí	k1gNnSc7	nabytí
tohoto	tento	k3xDgNnSc2	tento
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
<g/>
.	.	kIx.	.
<g/>
Do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2013	[number]	k4	2013
(	(	kIx(	(
<g/>
za	za	k7c2	za
účinnosti	účinnost	k1gFnSc2	účinnost
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
40	[number]	k4	40
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
pozbýt	pozbýt	k5eAaPmF	pozbýt
občanství	občanství	k1gNnSc4	občanství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
rovněž	rovněž	k9	rovněž
nabytím	nabytí	k1gNnSc7	nabytí
cizího	cizí	k2eAgNnSc2d1	cizí
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doklady	doklad	k1gInPc1	doklad
o	o	k7c6	o
státním	státní	k2eAgNnSc6d1	státní
občanství	občanství	k1gNnSc6	občanství
==	==	k?	==
</s>
</p>
<p>
<s>
Občanský	občanský	k2eAgInSc1d1	občanský
průkaz	průkaz	k1gInSc1	průkaz
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Cestovní	cestovní	k2eAgInSc1d1	cestovní
doklad	doklad	k1gInSc1	doklad
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Osvědčení	osvědčení	k1gNnSc4	osvědčení
ne	ne	k9	ne
starším	starší	k1gMnSc7	starší
1	[number]	k4	1
roku	rok	k1gInSc2	rok
</s>
</p>
<p>
<s>
Listinou	listina	k1gFnSc7	listina
o	o	k7c6	o
nabytí	nabytí	k1gNnSc6	nabytí
nebo	nebo	k8xC	nebo
udělení	udělení	k1gNnSc6	udělení
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
ne	ne	k9	ne
starší	starý	k2eAgInSc1d2	starší
1	[number]	k4	1
roku	rok	k1gInSc2	rok
</s>
</p>
<p>
<s>
==	==	k?	==
Mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
smlouvy	smlouva	k1gFnPc4	smlouva
vztahující	vztahující	k2eAgFnPc4d1	vztahující
se	se	k3xPyFc4	se
k	k	k7c3	k
úpravě	úprava	k1gFnSc3	úprava
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
==	==	k?	==
</s>
</p>
<p>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
úmluva	úmluva	k1gFnSc1	úmluva
o	o	k7c6	o
státním	státní	k2eAgNnSc6d1	státní
občanství	občanství	k1gNnSc6	občanství
(	(	kIx(	(
<g/>
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
pod	pod	k7c4	pod
č.	č.	k?	č.
76	[number]	k4	76
<g/>
/	/	kIx~	/
<g/>
2004	[number]	k4	2004
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
m.	m.	k?	m.
s.	s.	k?	s.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Úmluva	úmluva	k1gFnSc1	úmluva
o	o	k7c6	o
státním	státní	k2eAgNnSc6d1	státní
občanství	občanství	k1gNnSc6	občanství
vdaných	vdaný	k2eAgFnPc2d1	vdaná
žen	žena	k1gFnPc2	žena
(	(	kIx(	(
<g/>
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
pod	pod	k7c7	pod
č.	č.	k?	č.
72	[number]	k4	72
<g/>
/	/	kIx~	/
<g/>
1962	[number]	k4	1962
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Úmluva	úmluva	k1gFnSc1	úmluva
o	o	k7c4	o
omezení	omezení	k1gNnSc4	omezení
případů	případ	k1gInPc2	případ
bezdomovectví	bezdomovectví	k1gNnSc2	bezdomovectví
(	(	kIx(	(
<g/>
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
pod	pod	k7c7	pod
č.	č.	k?	č.
43	[number]	k4	43
<g/>
/	/	kIx~	/
<g/>
2002	[number]	k4	2002
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
m.	m.	k?	m.
s.	s.	k?	s.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Úmluva	úmluva	k1gFnSc1	úmluva
o	o	k7c6	o
odstranění	odstranění	k1gNnSc6	odstranění
všech	všecek	k3xTgFnPc2	všecek
forem	forma	k1gFnPc2	forma
diskriminace	diskriminace	k1gFnSc2	diskriminace
žen	žena	k1gFnPc2	žena
(	(	kIx(	(
<g/>
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
pod	pod	k7c7	pod
č.	č.	k?	č.
62	[number]	k4	62
<g/>
/	/	kIx~	/
<g/>
1987	[number]	k4	1987
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Úmluva	úmluva	k1gFnSc1	úmluva
o	o	k7c6	o
právním	právní	k2eAgNnSc6d1	právní
postavení	postavení	k1gNnSc6	postavení
osob	osoba	k1gFnPc2	osoba
bez	bez	k7c2	bez
státní	státní	k2eAgFnSc2d1	státní
příslušnostiDle	příslušnostiDle	k1gFnSc2	příslušnostiDle
článku	článek	k1gInSc2	článek
10	[number]	k4	10
Ústavy	ústava	k1gFnSc2	ústava
mají	mít	k5eAaImIp3nP	mít
mezinárodní	mezinárodní	k2eAgFnPc1d1	mezinárodní
smlouvy	smlouva	k1gFnPc1	smlouva
v	v	k7c6	v
případě	případ	k1gInSc6	případ
rozporu	rozpor	k1gInSc2	rozpor
aplikační	aplikační	k2eAgFnSc4d1	aplikační
přednost	přednost	k1gFnSc4	přednost
před	před	k7c7	před
vnitrostátním	vnitrostátní	k2eAgNnSc7d1	vnitrostátní
právem	právo	k1gNnSc7	právo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Legislativa	legislativa	k1gFnSc1	legislativa
===	===	k?	===
</s>
</p>
<p>
<s>
Zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
186	[number]	k4	186
<g/>
/	/	kIx~	/
<g/>
2013	[number]	k4	2013
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
státním	státní	k2eAgNnSc6d1	státní
občanství	občanství	k1gNnSc6	občanství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
dostupný	dostupný	k2eAgInSc1d1	dostupný
např.	např.	kA	např.
na	na	k7c4	na
zakonyprolidi	zakonyprolidit	k5eAaPmRp2nS	zakonyprolidit
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
40	[number]	k4	40
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
nabývání	nabývání	k1gNnSc6	nabývání
a	a	k8xC	a
pozbývání	pozbývání	k1gNnSc6	pozbývání
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
dostupný	dostupný	k2eAgInSc1d1	dostupný
např.	např.	kA	např.
na	na	k7c4	na
zakonyprolidi	zakonyprolidit	k5eAaPmRp2nS	zakonyprolidit
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Ústava	ústava	k1gFnSc1	ústava
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
dostupná	dostupný	k2eAgFnSc1d1	dostupná
např.	např.	kA	např.
na	na	k7c4	na
zakonyprolidi	zakonyprolidit	k5eAaPmRp2nS	zakonyprolidit
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Ústavní	ústavní	k2eAgInSc1d1	ústavní
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
opatřeních	opatření	k1gNnPc6	opatření
souvisejících	související	k2eAgFnPc2d1	související
se	s	k7c7	s
zánikem	zánik	k1gInSc7	zánik
ČSFR	ČSFR	kA	ČSFR
<g/>
,	,	kIx,	,
dostupný	dostupný	k2eAgInSc1d1	dostupný
např.	např.	kA	např.
na	na	k7c4	na
zakonyprolidi	zakonyprolidit	k5eAaPmRp2nS	zakonyprolidit
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Listina	listina	k1gFnSc1	listina
základních	základní	k2eAgNnPc2d1	základní
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
svobod	svoboda	k1gFnPc2	svoboda
<g/>
,	,	kIx,	,
dostupná	dostupný	k2eAgFnSc1d1	dostupná
např.	např.	kA	např.
na	na	k7c4	na
zakonyprolidi	zakonyprolidit	k5eAaPmRp2nS	zakonyprolidit
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Vyhláška	vyhláška	k1gFnSc1	vyhláška
č.	č.	k?	č.
433	[number]	k4	433
<g/>
/	/	kIx~	/
<g/>
2013	[number]	k4	2013
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
prokazování	prokazování	k1gNnSc6	prokazování
znalosti	znalost	k1gFnSc2	znalost
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
českých	český	k2eAgFnPc2d1	Česká
reálií	reálie	k1gFnPc2	reálie
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
udělování	udělování	k1gNnSc2	udělování
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
dostupná	dostupný	k2eAgFnSc1d1	dostupná
např.	např.	kA	např.
na	na	k7c4	na
zakonyprolidi	zakonyprolidit	k5eAaPmRp2nS	zakonyprolidit
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
91	[number]	k4	91
<g/>
/	/	kIx~	/
<g/>
2012	[number]	k4	2012
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
právu	právo	k1gNnSc6	právo
soukromém	soukromý	k2eAgNnSc6d1	soukromé
<g/>
,	,	kIx,	,
dostupný	dostupný	k2eAgMnSc1d1	dostupný
např.	např.	kA	např.
na	na	k7c4	na
zakonyprolidi	zakonyprolidit	k5eAaPmRp2nS	zakonyprolidit
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Apatrida	Apatrida	k1gFnSc1	Apatrida
</s>
</p>
<p>
<s>
Ius	Ius	k?	Ius
soli	sůl	k1gFnPc1	sůl
</s>
</p>
<p>
<s>
Ius	Ius	k?	Ius
sanguinis	sanguinis	k1gInSc1	sanguinis
</s>
</p>
<p>
<s>
Expatriace	expatriace	k1gFnSc1	expatriace
</s>
</p>
<p>
<s>
Repatriace	repatriace	k1gFnSc1	repatriace
</s>
</p>
<p>
<s>
Státní	státní	k2eAgNnSc1d1	státní
občanství	občanství	k1gNnSc1	občanství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Občanství	občanství	k1gNnSc1	občanství
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
</s>
</p>
<p>
<s>
Měšťanské	měšťanský	k2eAgNnSc1d1	měšťanské
právo	právo	k1gNnSc1	právo
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
občanství	občanství	k1gNnSc2	občanství
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
občanství	občanství	k1gNnSc2	občanství
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
bipolitismus	bipolitismus	k1gInSc1	bipolitismus
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Občanství	občanství	k1gNnSc2	občanství
státní	státní	k2eAgFnPc1d1	státní
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Informace	informace	k1gFnPc1	informace
k	k	k7c3	k
novému	nový	k2eAgInSc3d1	nový
zákonu	zákon	k1gInSc3	zákon
o	o	k7c6	o
státním	státní	k2eAgNnSc6d1	státní
občanství	občanství	k1gNnSc6	občanství
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
mvcr	mvcr	k1gInSc1	mvcr
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
</p>
