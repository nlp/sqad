<s>
Občanství	občanství	k1gNnSc1	občanství
je	být	k5eAaImIp3nS	být
časově	časově	k6eAd1	časově
relativně	relativně	k6eAd1	relativně
trvalý	trvalý	k2eAgInSc4d1	trvalý
<g/>
,	,	kIx,	,
místně	místně	k6eAd1	místně
neomezený	omezený	k2eNgInSc4d1	neomezený
právní	právní	k2eAgInSc4d1	právní
svazek	svazek	k1gInSc4	svazek
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
vztah	vztah	k1gInSc4	vztah
či	či	k8xC	či
status	status	k1gInSc4	status
<g/>
)	)	kIx)	)
fyzické	fyzický	k2eAgFnSc2d1	fyzická
osoby	osoba	k1gFnSc2	osoba
a	a	k8xC	a
daného	daný	k2eAgInSc2d1	daný
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
