<p>
<s>
Větrovec	Větrovec	k1gMnSc1	Větrovec
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Widkopf	Widkopf	k1gInSc1	Widkopf
nebo	nebo	k8xC	nebo
Windenberg	Windenberg	k1gInSc1	Windenberg
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kopec	kopec	k1gInSc4	kopec
v	v	k7c6	v
moravské	moravský	k2eAgFnSc6d1	Moravská
části	část	k1gFnSc6	část
podhůří	podhůří	k1gNnSc2	podhůří
Králického	králický	k2eAgInSc2d1	králický
Sněžníku	Sněžník	k1gInSc2	Sněžník
<g/>
.	.	kIx.	.
</s>
<s>
Nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
je	být	k5eAaImIp3nS	být
785	[number]	k4	785
m	m	kA	m
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
asi	asi	k9	asi
1,3	[number]	k4	1,3
km	km	kA	km
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
středu	střed	k1gInSc2	střed
vesnice	vesnice	k1gFnSc2	vesnice
Vysoká	vysoká	k1gFnSc1	vysoká
a	a	k8xC	a
asi	asi	k9	asi
1,4	[number]	k4	1,4
km	km	kA	km
SZ	SZ	kA	SZ
od	od	k7c2	od
kostela	kostel	k1gInSc2	kostel
ve	v	k7c6	v
Vysokých	vysoký	k2eAgFnPc6d1	vysoká
Žibřidovicích	Žibřidovice	k1gFnPc6	Žibřidovice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hydrologie	hydrologie	k1gFnSc2	hydrologie
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
jihozápadních	jihozápadní	k2eAgInPc2d1	jihozápadní
svahů	svah	k1gInPc2	svah
hory	hora	k1gFnSc2	hora
odtéká	odtékat	k5eAaImIp3nS	odtékat
voda	voda	k1gFnSc1	voda
do	do	k7c2	do
Zeleného	Zeleného	k2eAgInSc2d1	Zeleného
potoka	potok	k1gInSc2	potok
<g/>
,	,	kIx,	,
na	na	k7c4	na
severovýchod	severovýchod	k1gInSc4	severovýchod
do	do	k7c2	do
Prudkého	Prudkého	k2eAgInSc2d1	Prudkého
potoka	potok	k1gInSc2	potok
<g/>
,	,	kIx,	,
oblast	oblast	k1gFnSc1	oblast
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vegetace	vegetace	k1gFnSc2	vegetace
==	==	k?	==
</s>
</p>
<p>
<s>
Vrcholová	vrcholový	k2eAgFnSc1d1	vrcholová
partie	partie	k1gFnSc1	partie
a	a	k8xC	a
části	část	k1gFnSc3	část
svahů	svah	k1gInPc2	svah
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
do	do	k7c2	do
údolí	údolí	k1gNnSc2	údolí
Zeleného	Zeleného	k2eAgInSc2d1	Zeleného
potoka	potok	k1gInSc2	potok
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
zalesněny	zalesněn	k2eAgFnPc1d1	zalesněna
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
kulturními	kulturní	k2eAgFnPc7d1	kulturní
smrčinami	smrčina	k1gFnPc7	smrčina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ostatních	ostatní	k2eAgInPc6d1	ostatní
svazích	svah	k1gInPc6	svah
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nachází	nacházet	k5eAaImIp3nS	nacházet
louky	louka	k1gFnPc4	louka
<g/>
.	.	kIx.	.
</s>
</p>
