<s>
Větrovec	Větrovec	k1gMnSc1	Větrovec
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Widkopf	Widkopf	k1gInSc1	Widkopf
nebo	nebo	k8xC	nebo
Windenberg	Windenberg	k1gInSc1	Windenberg
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kopec	kopec	k1gInSc4	kopec
v	v	k7c6	v
moravské	moravský	k2eAgFnSc6d1	Moravská
části	část	k1gFnSc6	část
podhůří	podhůří	k1gNnSc2	podhůří
Králického	králický	k2eAgInSc2d1	králický
Sněžníku	Sněžník	k1gInSc2	Sněžník
<g/>
.	.	kIx.	.
</s>
<s>
Nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
je	být	k5eAaImIp3nS	být
785	[number]	k4	785
m	m	kA	m
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
asi	asi	k9	asi
1,3	[number]	k4	1,3
km	km	kA	km
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
středu	střed	k1gInSc2	střed
vesnice	vesnice	k1gFnSc2	vesnice
Vysoká	vysoká	k1gFnSc1	vysoká
a	a	k8xC	a
asi	asi	k9	asi
1,4	[number]	k4	1,4
km	km	kA	km
SZ	SZ	kA	SZ
od	od	k7c2	od
kostela	kostel	k1gInSc2	kostel
ve	v	k7c6	v
Vysokých	vysoký	k2eAgFnPc6d1	vysoká
Žibřidovicích	Žibřidovice	k1gFnPc6	Žibřidovice
<g/>
.	.	kIx.	.
</s>

