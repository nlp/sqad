<p>
<s>
Kuba	Kuba	k1gFnSc1	Kuba
dosud	dosud	k6eAd1	dosud
nelegalizovala	legalizovat	k5eNaBmAgFnS	legalizovat
stejnopohlavní	stejnopohlavní	k2eAgNnSc4d1	stejnopohlavní
manželství	manželství	k1gNnSc4	manželství
<g/>
,	,	kIx,	,
registrované	registrovaný	k2eAgNnSc4d1	registrované
partnerství	partnerství	k1gNnSc4	partnerství
<g/>
,	,	kIx,	,
a	a	k8xC	a
ani	ani	k8xC	ani
jinou	jiný	k2eAgFnSc4d1	jiná
formu	forma	k1gFnSc4	forma
právní	právní	k2eAgFnSc2d1	právní
úpravy	úprava	k1gFnSc2	úprava
homosexuálních	homosexuální	k2eAgInPc2d1	homosexuální
svazků	svazek	k1gInPc2	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Kubánská	kubánský	k2eAgFnSc1d1	kubánská
ústava	ústava	k1gFnSc1	ústava
definuje	definovat	k5eAaBmIp3nS	definovat
manželství	manželství	k1gNnSc4	manželství
jako	jako	k8xS	jako
svazek	svazek	k1gInSc4	svazek
muže	muž	k1gMnSc2	muž
a	a	k8xC	a
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Registrované	registrovaný	k2eAgNnSc1d1	registrované
partnerství	partnerství	k1gNnSc1	partnerství
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
návrh	návrh	k1gInSc1	návrh
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
registrovaném	registrovaný	k2eAgNnSc6d1	registrované
partnerství	partnerství	k1gNnSc6	partnerství
byl	být	k5eAaImAgInS	být
předložen	předložit	k5eAaPmNgInS	předložit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nestihlo	stihnout	k5eNaPmAgNnS	stihnout
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
hlasovat	hlasovat	k5eAaImF	hlasovat
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgInSc1d2	pozdější
návrh	návrh	k1gInSc1	návrh
této	tento	k3xDgFnSc2	tento
legislativy	legislativa	k1gFnSc2	legislativa
prezentovaný	prezentovaný	k2eAgInSc4d1	prezentovaný
v	v	k7c6	v
září	září	k1gNnSc6	září
2009	[number]	k4	2009
obsahoval	obsahovat	k5eAaImAgMnS	obsahovat
veškerá	veškerý	k3xTgNnPc4	veškerý
práva	právo	k1gNnPc4	právo
a	a	k8xC	a
povinnosti	povinnost	k1gFnPc4	povinnost
vyplývající	vyplývající	k2eAgFnPc4d1	vyplývající
z	z	k7c2	z
manželství	manželství	k1gNnSc2	manželství
<g/>
,	,	kIx,	,
vyjma	vyjma	k7c2	vyjma
užívání	užívání	k1gNnSc2	užívání
tohoto	tento	k3xDgInSc2	tento
názvu	název	k1gInSc2	název
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
novému	nový	k2eAgInSc3d1	nový
zákonu	zákon	k1gInSc3	zákon
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
veřejné	veřejný	k2eAgNnSc1d1	veřejné
slyšení	slyšení	k1gNnSc1	slyšení
před	před	k7c7	před
kubánským	kubánský	k2eAgInSc7d1	kubánský
parlamentem	parlament	k1gInSc7	parlament
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
jej	on	k3xPp3gMnSc4	on
podpořila	podpořit	k5eAaPmAgFnS	podpořit
Mariela	Mariela	k1gFnSc1	Mariela
Castrová	Castrová	k1gFnSc1	Castrová
<g/>
,	,	kIx,	,
ředitelka	ředitelka	k1gFnSc1	ředitelka
Kubánského	kubánský	k2eAgNnSc2d1	kubánské
centra	centrum	k1gNnSc2	centrum
pro	pro	k7c4	pro
sexuální	sexuální	k2eAgFnSc4d1	sexuální
výchovu	výchova	k1gFnSc4	výchova
a	a	k8xC	a
dcera	dcera	k1gFnSc1	dcera
bývalého	bývalý	k2eAgMnSc2d1	bývalý
kubánského	kubánský	k2eAgMnSc2d1	kubánský
prezidenta	prezident	k1gMnSc2	prezident
Raúla	Raúl	k1gMnSc2	Raúl
Castra	Castr	k1gMnSc2	Castr
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
tehdy	tehdy	k6eAd1	tehdy
zákon	zákon	k1gInSc1	zákon
prošel	projít	k5eAaPmAgInS	projít
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
Kuba	Kuba	k1gFnSc1	Kuba
prvním	první	k4xOgInSc7	první
karibským	karibský	k2eAgInSc7d1	karibský
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
legalizoval	legalizovat	k5eAaBmAgMnS	legalizovat
registrované	registrovaný	k2eAgNnSc4d1	registrované
partnerství	partnerství	k1gNnSc4	partnerství
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
také	také	k9	také
první	první	k4xOgFnSc4	první
komunistickou	komunistický	k2eAgFnSc4d1	komunistická
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
přijala	přijmout	k5eAaPmAgFnS	přijmout
takový	takový	k3xDgInSc4	takový
zákon	zákon	k1gInSc4	zákon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
návrh	návrh	k1gInSc4	návrh
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
registrovaném	registrovaný	k2eAgNnSc6d1	registrované
partnerství	partnerství	k1gNnSc6	partnerství
stále	stále	k6eAd1	stále
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
kubánském	kubánský	k2eAgInSc6d1	kubánský
parlamentu	parlament	k1gInSc6	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Mariela	Mariela	k1gFnSc1	Mariela
Castrová	Castrová	k1gFnSc1	Castrová
řekla	říct	k5eAaPmAgFnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
návrh	návrh	k1gInSc1	návrh
zákona	zákon	k1gInSc2	zákon
má	mít	k5eAaImIp3nS	mít
podporu	podpora	k1gFnSc4	podpora
jejího	její	k3xOp3gMnSc2	její
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
maximálně	maximálně	k6eAd1	maximálně
vynasnaží	vynasnažit	k5eAaPmIp3nS	vynasnažit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
prošel	projít	k5eAaPmAgMnS	projít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stejnopohlavní	Stejnopohlavní	k2eAgNnPc1d1	Stejnopohlavní
manželství	manželství	k1gNnPc1	manželství
==	==	k?	==
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
36	[number]	k4	36
kubánské	kubánský	k2eAgFnSc2d1	kubánská
ústavy	ústava	k1gFnSc2	ústava
definuje	definovat	k5eAaBmIp3nS	definovat
manželství	manželství	k1gNnSc1	manželství
jako	jako	k8xC	jako
dobrovolný	dobrovolný	k2eAgInSc1d1	dobrovolný
svazek	svazek	k1gInSc1	svazek
muže	muž	k1gMnSc2	muž
a	a	k8xC	a
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
článku	článek	k1gInSc2	článek
2	[number]	k4	2
kubánského	kubánský	k2eAgInSc2d1	kubánský
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
rodině	rodina	k1gFnSc6	rodina
je	být	k5eAaImIp3nS	být
manželství	manželství	k1gNnSc1	manželství
taktéž	taktéž	k?	taktéž
vymezené	vymezený	k2eAgFnSc2d1	vymezená
jako	jako	k8xC	jako
svazek	svazek	k1gInSc1	svazek
muže	muž	k1gMnSc2	muž
a	a	k8xC	a
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2017	[number]	k4	2017
zahájily	zahájit	k5eAaPmAgInP	zahájit
kubánské	kubánský	k2eAgFnSc2d1	kubánská
LGBT	LGBT	kA	LGBT
skupiny	skupina	k1gFnSc2	skupina
kampaň	kampaň	k1gFnSc1	kampaň
za	za	k7c4	za
novelu	novela	k1gFnSc4	novela
ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
umožnila	umožnit	k5eAaPmAgFnS	umožnit
homosexuálním	homosexuální	k2eAgInPc3d1	homosexuální
párům	pár	k1gInPc3	pár
uzavírat	uzavírat	k5eAaImF	uzavírat
manželství	manželství	k1gNnSc1	manželství
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2018	[number]	k4	2018
řekla	říct	k5eAaPmAgFnS	říct
Mariela	Mariela	k1gFnSc1	Mariela
Castrová	Castrová	k1gFnSc1	Castrová
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pokusí	pokusit	k5eAaPmIp3nS	pokusit
do	do	k7c2	do
rozsáhlých	rozsáhlý	k2eAgFnPc2d1	rozsáhlá
ústavních	ústavní	k2eAgFnPc2d1	ústavní
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
také	také	k9	také
uznání	uznání	k1gNnSc4	uznání
soukromého	soukromý	k2eAgNnSc2d1	soukromé
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
a	a	k8xC	a
volného	volný	k2eAgInSc2d1	volný
trhu	trh	k1gInSc2	trh
<g/>
,	,	kIx,	,
zahrnout	zahrnout	k5eAaPmF	zahrnout
také	také	k9	také
legalizaci	legalizace	k1gFnSc4	legalizace
stejnopohlavních	stejnopohlavní	k2eAgInPc2d1	stejnopohlavní
sňatků	sňatek	k1gInPc2	sňatek
<g/>
.	.	kIx.	.
</s>
<s>
Politické	politický	k2eAgFnPc1d1	politická
diskuze	diskuze	k1gFnPc1	diskuze
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
stejnopohlavního	stejnopohlavní	k2eAgNnSc2d1	stejnopohlavní
manželství	manželství	k1gNnSc2	manželství
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
výrazným	výrazný	k2eAgInSc7d1	výrazný
způsobem	způsob	k1gInSc7	způsob
změnit	změnit	k5eAaPmF	změnit
charakter	charakter	k1gInSc4	charakter
komunistické	komunistický	k2eAgFnSc2d1	komunistická
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
naplánovány	naplánovat	k5eAaBmNgInP	naplánovat
na	na	k7c4	na
červenec	červenec	k1gInSc4	červenec
2018.21	[number]	k4	2018.21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
oznámil	oznámit	k5eAaPmAgMnS	oznámit
tajemník	tajemník	k1gMnSc1	tajemník
Státní	státní	k2eAgFnSc2d1	státní
rady	rada	k1gFnSc2	rada
Homero	Homero	k1gNnSc1	Homero
Acosta	Acosta	k1gFnSc1	Acosta
<g/>
,	,	kIx,	,
že	že	k8xS	že
navržená	navržený	k2eAgFnSc1d1	navržená
změna	změna	k1gFnSc1	změna
ústavy	ústava	k1gFnSc2	ústava
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
ustanovení	ustanovení	k1gNnPc1	ustanovení
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgInSc2	jenž
je	být	k5eAaImIp3nS	být
manželství	manželství	k1gNnSc1	manželství
svazek	svazek	k1gInSc1	svazek
dvou	dva	k4xCgMnPc2	dva
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
reformy	reforma	k1gFnSc2	reforma
kubánské	kubánský	k2eAgFnSc2d1	kubánská
ústavy	ústava	k1gFnSc2	ústava
a	a	k8xC	a
politického	politický	k2eAgInSc2d1	politický
systému	systém	k1gInSc2	systém
musejí	muset	k5eAaImIp3nP	muset
zahrnovat	zahrnovat	k5eAaImF	zahrnovat
i	i	k9	i
legalizaci	legalizace	k1gFnSc4	legalizace
stejnopohlavních	stejnopohlavní	k2eAgInPc2d1	stejnopohlavní
sňatků	sňatek	k1gInPc2	sňatek
<g/>
.	.	kIx.	.
</s>
<s>
Kubánský	kubánský	k2eAgInSc1d1	kubánský
parlament	parlament	k1gInSc1	parlament
schválil	schválit	k5eAaPmAgInS	schválit
ústavní	ústavní	k2eAgFnSc4d1	ústavní
novelu	novela	k1gFnSc4	novela
22	[number]	k4	22
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
změnách	změna	k1gFnPc6	změna
ústavy	ústava	k1gFnSc2	ústava
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
ještě	ještě	k9	ještě
veřejně	veřejně	k6eAd1	veřejně
diskutovat	diskutovat	k5eAaImF	diskutovat
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
s	s	k7c7	s
následným	následný	k2eAgNnSc7d1	následné
referendem	referendum	k1gNnSc7	referendum
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
by	by	kYmCp3nS	by
je	on	k3xPp3gInPc4	on
mělo	mít	k5eAaImAgNnS	mít
definitivně	definitivně	k6eAd1	definitivně
potvrdit	potvrdit	k5eAaPmF	potvrdit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Recognition	Recognition	k1gInSc1	Recognition
of	of	k?	of
same-sex	sameex	k1gInSc1	same-sex
unions	unions	k1gInSc1	unions
in	in	k?	in
Cuba	Cuba	k1gFnSc1	Cuba
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
LGBT	LGBT	kA	LGBT
práva	práv	k2eAgFnSc1d1	práva
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
</s>
</p>
<p>
<s>
Práva	právo	k1gNnPc1	právo
párů	pár	k1gInPc2	pár
stejného	stejný	k2eAgNnSc2d1	stejné
pohlaví	pohlaví	k1gNnSc2	pohlaví
v	v	k7c6	v
amerických	americký	k2eAgFnPc6d1	americká
zemích	zem	k1gFnPc6	zem
</s>
</p>
