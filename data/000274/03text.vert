<s>
Krmelec	krmelec	k1gInSc1	krmelec
je	být	k5eAaImIp3nS	být
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
stavba	stavba	k1gFnSc1	stavba
umístěná	umístěný	k2eAgFnSc1d1	umístěná
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
lese	les	k1gInSc6	les
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
přikrmování	přikrmování	k1gNnSc4	přikrmování
zvěře	zvěř	k1gFnSc2	zvěř
<g/>
.	.	kIx.	.
</s>
<s>
Přikrmování	přikrmování	k1gNnSc1	přikrmování
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
provádí	provádět	k5eAaImIp3nS	provádět
v	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
nedostatek	nedostatek	k1gInSc1	nedostatek
potravy	potrava	k1gFnSc2	potrava
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
zelené	zelený	k2eAgFnSc2d1	zelená
píce	píce	k1gFnSc2	píce
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
krmelce	krmelec	k1gInSc2	krmelec
se	se	k3xPyFc4	se
zakládá	zakládat	k5eAaImIp3nS	zakládat
seno	seno	k1gNnSc1	seno
pro	pro	k7c4	pro
přikrmování	přikrmování	k1gNnSc4	přikrmování
lesních	lesní	k2eAgMnPc2d1	lesní
savců	savec	k1gMnPc2	savec
z	z	k7c2	z
čeledě	čeleď	k1gFnSc2	čeleď
jelenovitých	jelenovití	k1gMnPc2	jelenovití
(	(	kIx(	(
<g/>
jelen	jelen	k1gMnSc1	jelen
<g/>
,	,	kIx,	,
daněk	daněk	k1gMnSc1	daněk
<g/>
,	,	kIx,	,
srnec	srnec	k1gMnSc1	srnec
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
bývají	bývat	k5eAaImIp3nP	bývat
krmelce	krmelec	k1gInPc1	krmelec
ještě	ještě	k6eAd1	ještě
doplněny	doplněn	k2eAgInPc1d1	doplněn
o	o	k7c4	o
korýtka	korýtko	k1gNnPc4	korýtko
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
vkládá	vkládat	k5eAaImIp3nS	vkládat
tzv.	tzv.	kA	tzv.
liz	liz	k1gInSc1	liz
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
větší	veliký	k2eAgFnPc4d2	veliký
hroudy	hrouda	k1gFnPc4	hrouda
kamenné	kamenný	k2eAgFnPc4d1	kamenná
či	či	k8xC	či
hořečnaté	hořečnatý	k2eAgFnPc4d1	hořečnatá
soli	sůl	k1gFnPc4	sůl
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
zvířata	zvíře	k1gNnPc1	zvíře
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
olizují	olizovat	k5eAaImIp3nP	olizovat
a	a	k8xC	a
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
chybějící	chybějící	k2eAgFnPc4d1	chybějící
tělesné	tělesný	k2eAgFnPc4d1	tělesná
soli	sůl	k1gFnPc4	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Krmelec	krmelec	k1gInSc1	krmelec
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
