<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
stavba	stavba	k1gFnSc1	stavba
umístěná	umístěný	k2eAgFnSc1d1	umístěná
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
lese	les	k1gInSc6	les
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
přikrmování	přikrmování	k1gNnSc4	přikrmování
zvěře	zvěř	k1gFnSc2	zvěř
<g/>
?	?	kIx.	?
</s>
