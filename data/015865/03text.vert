<s>
Bazální	bazální	k2eAgFnSc1d1
stimulace	stimulace	k1gFnSc1
</s>
<s>
Bazální	bazální	k2eAgFnSc7d1
stimulací	stimulace	k1gFnSc7
se	se	k3xPyFc4
rozumí	rozumět	k5eAaImIp3nS
pedagogicko-ošetřovatelský	pedagogicko-ošetřovatelský	k2eAgInSc1d1
koncept	koncept	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
má	mít	k5eAaImIp3nS
nabídnout	nabídnout	k5eAaPmF
mentálně	mentálně	k6eAd1
postiženým	postižený	k2eAgMnPc3d1
jedincům	jedinec	k1gMnPc3
nebo	nebo	k8xC
pacientům	pacient	k1gMnPc3
v	v	k7c6
nemocnici	nemocnice	k1gFnSc6
(	(	kIx(
<g/>
často	často	k6eAd1
na	na	k7c6
JIP	JIP	kA
<g/>
)	)	kIx)
podněty	podnět	k1gInPc1
pro	pro	k7c4
vývoj	vývoj	k1gInSc4
jejich	jejich	k3xOp3gFnSc2
osobnosti	osobnost	k1gFnSc2
v	v	k7c6
jednoduché	jednoduchý	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Synonymem	synonymum	k1gNnSc7
pro	pro	k7c4
bazální	bazální	k2eAgFnSc4d1
stimulaci	stimulace	k1gFnSc4
je	být	k5eAaImIp3nS
bazální	bazální	k2eAgInSc1d1
dialog	dialog	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Koncept	koncept	k1gInSc1
vymyslel	vymyslet	k5eAaPmAgMnS
německý	německý	k2eAgMnSc1d1
speciální	speciální	k2eAgMnSc1d1
pedagog	pedagog	k1gMnSc1
prof.	prof.	kA
Andreas	Andreas	k1gMnSc1
D.	D.	kA
Fröhlich	Fröhlich	k1gMnSc1
<g/>
,	,	kIx,
tehdy	tehdy	k6eAd1
v	v	k7c6
rámci	rámec	k1gInSc6
své	svůj	k3xOyFgFnSc2
disertační	disertační	k2eAgFnSc2d1
práce	práce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc7
snahou	snaha	k1gFnSc7
bylo	být	k5eAaImAgNnS
umožnit	umožnit	k5eAaPmF
i	i	k9
lidem	lido	k1gNnSc7
s	s	k7c7
těžkým	těžký	k2eAgNnSc7d1
kombinovaným	kombinovaný	k2eAgNnSc7d1
postižením	postižení	k1gNnSc7
samostatný	samostatný	k2eAgInSc4d1
život	život	k1gInSc4
<g/>
,	,	kIx,
byť	byť	k8xS
ve	v	k7c6
velmi	velmi	k6eAd1
omezené	omezený	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
10	#num#	k4
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
profesor	profesor	k1gMnSc1
Frolich	Frolich	k1gMnSc1
svůj	svůj	k3xOyFgInSc4
koncept	koncept	k1gInSc4
používal	používat	k5eAaImAgMnS
u	u	k7c2
dětí	dítě	k1gFnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
tuto	tento	k3xDgFnSc4
metodu	metoda	k1gFnSc4
podařilo	podařit	k5eAaPmAgNnS
převést	převést	k5eAaPmF
do	do	k7c2
ošetřovatelství	ošetřovatelství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
to	ten	k3xDgNnSc4
se	se	k3xPyFc4
zasloužila	zasloužit	k5eAaPmAgFnS
německá	německý	k2eAgFnSc1d1
zdravotní	zdravotní	k2eAgFnSc1d1
sestra	sestra	k1gFnSc1
prof.	prof.	kA
Christel	Christel	k1gMnSc1
Bienstein	Bienstein	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Koncept	koncept	k1gInSc1
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
poznatků	poznatek	k1gInPc2
několika	několik	k4yIc2
vědních	vědní	k2eAgInPc2d1
oborů	obor	k1gInPc2
<g/>
,	,	kIx,
jako	jako	k9
pedagogiky	pedagogika	k1gFnSc2
<g/>
,	,	kIx,
fyziologie	fyziologie	k1gFnSc2
<g/>
,	,	kIx,
anatomie	anatomie	k1gFnSc2
<g/>
,	,	kIx,
neurologie	neurologie	k1gFnSc2
<g/>
,	,	kIx,
vývojové	vývojový	k2eAgFnSc2d1
psychologie	psychologie	k1gFnSc2
a	a	k8xC
medicíny	medicína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
koncept	koncept	k1gInSc4
má	mít	k5eAaImIp3nS
uplatnění	uplatnění	k1gNnSc4
ve	v	k7c6
zdravotnictví	zdravotnictví	k1gNnSc6
<g/>
,	,	kIx,
ve	v	k7c6
školství	školství	k1gNnSc6
i	i	k8xC
v	v	k7c6
sociální	sociální	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Propagátorkou	propagátorka	k1gFnSc7
bazální	bazální	k2eAgFnSc2d1
stimulace	stimulace	k1gFnSc2
v	v	k7c6
ČR	ČR	kA
je	být	k5eAaImIp3nS
zdravotní	zdravotní	k2eAgFnSc1d1
sestra	sestra	k1gFnSc1
PhDr.	PhDr.	kA
Karolína	Karolína	k1gFnSc1
Friedlová	Friedlový	k2eAgFnSc1d1
<g/>
,	,	kIx,
PhD	PhD	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Založila	založit	k5eAaPmAgFnS
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
Institut	institut	k1gInSc1
bazální	bazální	k2eAgFnSc2d1
stimulace	stimulace	k1gFnSc2
<g/>
.	.	kIx.
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
také	také	k6eAd1
vede	vést	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Princip	princip	k1gInSc1
</s>
<s>
Základní	základní	k2eAgFnSc7d1
myšlenkou	myšlenka	k1gFnSc7
je	být	k5eAaImIp3nS
stimulovat	stimulovat	k5eAaImF
pacienta	pacient	k1gMnSc4
(	(	kIx(
<g/>
dítě	dítě	k1gNnSc1
<g/>
,	,	kIx,
žáka	žák	k1gMnSc4
<g/>
,	,	kIx,
osobu	osoba	k1gFnSc4
<g/>
,	,	kIx,
uživatele	uživatel	k1gMnSc2
sociálních	sociální	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
vnímal	vnímat	k5eAaImAgInS
vlastní	vlastní	k2eAgNnSc4d1
tělo	tělo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
určitým	určitý	k2eAgFnPc3d1
metodám	metoda	k1gFnPc3
by	by	kYmCp3nP
měl	mít	k5eAaImAgInS
snáze	snadno	k6eAd2
a	a	k8xC
celkově	celkově	k6eAd1
kvalitněji	kvalitně	k6eAd2
vnímat	vnímat	k5eAaImF
okolní	okolní	k2eAgInSc4d1
svět	svět	k1gInSc4
a	a	k8xC
následně	následně	k6eAd1
s	s	k7c7
ním	on	k3xPp3gNnSc7
navázat	navázat	k5eAaPmF
komunikaci	komunikace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Druhy	druh	k1gInPc1
stimulací	stimulace	k1gFnPc2
</s>
<s>
Stimulace	stimulace	k1gFnSc1
lze	lze	k6eAd1
rozdělit	rozdělit	k5eAaPmF
do	do	k7c2
několika	několik	k4yIc2
typů	typ	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
Somatické	somatický	k2eAgInPc4d1
-	-	kIx~
tělesné	tělesný	k2eAgInPc4d1
podněty	podnět	k1gInPc4
(	(	kIx(
<g/>
doteky	dotek	k1gInPc4
<g/>
,	,	kIx,
masáže	masáž	k1gFnPc4
<g/>
,	,	kIx,
<g/>
...	...	k?
<g/>
)	)	kIx)
pomáhají	pomáhat	k5eAaImIp3nP
pacientovi	pacient	k1gMnSc3
vnímat	vnímat	k5eAaImF
vlastní	vlastní	k2eAgNnSc4d1
tělo	tělo	k1gNnSc4
<g/>
;	;	kIx,
</s>
<s>
Vibrační	vibrační	k2eAgFnSc1d1
-	-	kIx~
pomáhá	pomáhat	k5eAaImIp3nS
vnímat	vnímat	k5eAaImF
vibrace	vibrace	k1gFnPc4
a	a	k8xC
chvění	chvění	k1gNnSc4
lidského	lidský	k2eAgInSc2d1
hlasu	hlas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
jim	on	k3xPp3gMnPc3
také	také	k9
ovlivnit	ovlivnit	k5eAaPmF
rytmické	rytmický	k2eAgNnSc1d1
dýchání	dýchání	k1gNnSc1
pacienta	pacient	k1gMnSc2
<g/>
;	;	kIx,
</s>
<s>
Vestibulární	vestibulární	k2eAgFnSc1d1
-	-	kIx~
pomáhá	pomáhat	k5eAaImIp3nS
s	s	k7c7
orientací	orientace	k1gFnSc7
v	v	k7c4
prostoru	prostora	k1gFnSc4
a	a	k8xC
uvědomování	uvědomování	k1gNnSc4
si	se	k3xPyFc3
své	svůj	k3xOyFgNnSc4
vlastní	vlastnit	k5eAaImIp3nP
polohy	poloha	k1gFnPc1
v	v	k7c6
něm	on	k3xPp3gInSc6
<g/>
;	;	kIx,
</s>
<s>
Auditivní	auditivní	k2eAgMnPc1d1
-	-	kIx~
pacientovi	pacientův	k2eAgMnPc1d1
se	se	k3xPyFc4
pouštějí	pouštět	k5eAaImIp3nP
jeho	jeho	k3xOp3gInPc1
oblíbené	oblíbený	k2eAgInPc1d1
a	a	k8xC
známé	známý	k2eAgInPc1d1
zvuky	zvuk	k1gInPc1
<g/>
;	;	kIx,
</s>
<s>
Orální	orální	k2eAgFnPc1d1
-	-	kIx~
pacientovy	pacientův	k2eAgFnPc1d1
oblíbené	oblíbený	k2eAgFnPc1d1
tekutiny	tekutina	k1gFnPc1
stimulují	stimulovat	k5eAaImIp3nP
receptory	receptor	k1gInPc1
chuti	chuť	k1gFnSc2
<g/>
;	;	kIx,
</s>
<s>
Taktilně-haptická	Taktilně-haptický	k2eAgFnSc1d1
-	-	kIx~
zaměřená	zaměřený	k2eAgFnSc1d1
na	na	k7c4
osvojování	osvojování	k1gNnSc4
manipulace	manipulace	k1gFnSc2
s	s	k7c7
předměty	předmět	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Bazální	bazální	k2eAgFnSc1d1
stimulace	stimulace	k1gFnSc1
<g/>
.	.	kIx.
www.centrumbazalka.cz	www.centrumbazalka.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Rehabilitační	rehabilitační	k2eAgNnSc1d1
lékařství	lékařství	k1gNnSc1
</s>
<s>
Psychologie	psychologie	k1gFnSc1
</s>
<s>
Mentální	mentální	k2eAgNnSc1d1
narušení	narušení	k1gNnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
https://web.archive.org/web/20160108094323/http://www.centrumbazalka.cz/cz/page/2567/bazalni-stimulace.html	https://web.archive.org/web/20160108094323/http://www.centrumbazalka.cz/cz/page/2567/bazalni-stimulace.html	k1gMnSc1
</s>
<s>
http://wiki.rvp.cz/Knihovna/1.Pedagogicky_lexikon/B/Baz%C3%A1ln%C3%AD_stimulace	http://wiki.rvp.cz/Knihovna/1.Pedagogicky_lexikon/B/Baz%C3%A1ln%C3%AD_stimulace	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Medicína	medicína	k1gFnSc1
</s>
