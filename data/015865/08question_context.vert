<s>
Bazální	bazální	k2eAgFnSc7d1
stimulací	stimulace	k1gFnSc7
se	se	k3xPyFc4
rozumí	rozumět	k5eAaImIp3nS
pedagogicko-ošetřovatelský	pedagogicko-ošetřovatelský	k2eAgInSc1d1
koncept	koncept	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
má	mít	k5eAaImIp3nS
nabídnout	nabídnout	k5eAaPmF
mentálně	mentálně	k6eAd1
postiženým	postižený	k2eAgMnPc3d1
jedincům	jedinec	k1gMnPc3
nebo	nebo	k8xC
pacientům	pacient	k1gMnPc3
v	v	k7c6
nemocnici	nemocnice	k1gFnSc6
(	(	kIx(
<g/>
často	často	k6eAd1
na	na	k7c6
JIP	JIP	kA
<g/>
)	)	kIx)
podněty	podnět	k1gInPc1
pro	pro	k7c4
vývoj	vývoj	k1gInSc4
jejich	jejich	k3xOp3gFnSc2
osobnosti	osobnost	k1gFnSc2
v	v	k7c6
jednoduché	jednoduchý	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
<g/>
.	.	kIx.
</s>