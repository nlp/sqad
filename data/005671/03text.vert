<s>
Bonsaj	bonsaj	k1gFnSc1	bonsaj
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
盆	盆	k?	盆
<g/>
;	;	kIx,	;
též	též	k9	též
bonsai	bonsai	k1gFnSc1	bonsai
<g/>
,	,	kIx,	,
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
punsaj	punsaj	k1gInSc4	punsaj
nebo	nebo	k8xC	nebo
penjing	penjing	k1gInSc4	penjing
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
staré	starý	k2eAgNnSc1d1	staré
umění	umění	k1gNnSc1	umění
miniaturizace	miniaturizace	k1gFnSc2	miniaturizace
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
pěstování	pěstování	k1gNnSc4	pěstování
v	v	k7c6	v
miskách	miska	k1gFnPc6	miska
<g/>
.	.	kIx.	.
</s>
<s>
Doslova	doslova	k6eAd1	doslova
přeloženo	přeložen	k2eAgNnSc1d1	přeloženo
má	mít	k5eAaImIp3nS	mít
slovo	slovo	k1gNnSc1	slovo
bonsaj	bonsaj	k1gInSc1	bonsaj
význam	význam	k1gInSc1	význam
strom	strom	k1gInSc1	strom
v	v	k7c6	v
misce	miska	k1gFnSc6	miska
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bon	bon	k1gInSc1	bon
znamená	znamenat	k5eAaImIp3nS	znamenat
miska	miska	k1gFnSc1	miska
a	a	k8xC	a
sai	sai	k?	sai
strom	strom	k1gInSc1	strom
(	(	kIx(	(
<g/>
též	též	k9	též
sázet	sázet	k5eAaImF	sázet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Delší	dlouhý	k2eAgFnSc1d2	delší
a	a	k8xC	a
přesnější	přesný	k2eAgFnSc1d2	přesnější
definice	definice	k1gFnSc1	definice
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bonsaj	bonsaj	k1gInSc1	bonsaj
je	být	k5eAaImIp3nS	být
miniaturní	miniaturní	k2eAgInSc1d1	miniaturní
strom	strom	k1gInSc1	strom
pěstovaný	pěstovaný	k2eAgInSc1d1	pěstovaný
v	v	k7c6	v
misce	miska	k1gFnSc6	miska
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
bonsajovými	bonsajový	k2eAgFnPc7d1	bonsajová
technikami	technika	k1gFnPc7	technika
tvarován	tvarován	k2eAgInSc4d1	tvarován
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
navozoval	navozovat	k5eAaImAgInS	navozovat
iluzi	iluze	k1gFnSc4	iluze
starého	starý	k2eAgInSc2d1	starý
stromu	strom	k1gInSc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
prvky	prvek	k1gInPc4	prvek
pěstitelství	pěstitelství	k1gNnSc2	pěstitelství
i	i	k8xC	i
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc4	počátek
pěstování	pěstování	k1gNnSc4	pěstování
bonsají	bonsaj	k1gFnSc7	bonsaj
nejsou	být	k5eNaImIp3nP	být
doložené	doložený	k2eAgInPc1d1	doložený
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gFnPc4	jejich
stopy	stopa	k1gFnPc4	stopa
lze	lze	k6eAd1	lze
sledovat	sledovat	k5eAaImF	sledovat
do	do	k7c2	do
velmi	velmi	k6eAd1	velmi
hluboké	hluboký	k2eAgFnSc2d1	hluboká
minulosti	minulost	k1gFnSc2	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Prapůvod	prapůvod	k1gInSc1	prapůvod
klasických	klasický	k2eAgFnPc2d1	klasická
vaničkovitých	vaničkovitý	k2eAgFnPc2d1	vaničkovitý
bonsajistických	bonsajistický	k2eAgFnPc2d1	bonsajistická
misek	miska	k1gFnPc2	miska
sahá	sahat	k5eAaImIp3nS	sahat
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
až	až	k9	až
4	[number]	k4	4
tisíce	tisíc	k4xCgInPc1	tisíc
let	léto	k1gNnPc2	léto
do	do	k7c2	do
minulosti	minulost	k1gFnSc2	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Podobají	podobat	k5eAaImIp3nP	podobat
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
některým	některý	k3yIgInPc3	některý
archeologickým	archeologický	k2eAgInPc3d1	archeologický
artefaktům	artefakt	k1gInPc3	artefakt
z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
oltářním	oltářní	k2eAgNnPc3d1	oltářní
ohništím	ohniště	k1gNnPc3	ohniště
pro	pro	k7c4	pro
spalování	spalování	k1gNnPc4	spalování
obětin	obětina	k1gFnPc2	obětina
<g/>
:	:	kIx,	:
tvar	tvar	k1gInSc1	tvar
<g/>
,	,	kIx,	,
nožky	nožka	k1gFnPc1	nožka
<g/>
,	,	kIx,	,
ventilační	ventilační	k2eAgInPc1d1	ventilační
otvory	otvor	k1gInPc1	otvor
ve	v	k7c6	v
dně	dno	k1gNnSc6	dno
atd.	atd.	kA	atd.
Není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgInP	být
prvně	prvně	k?	prvně
využity	využít	k5eAaPmNgFnP	využít
jako	jako	k9	jako
pěstební	pěstební	k2eAgFnPc1d1	pěstební
nádoby	nádoba	k1gFnPc1	nádoba
<g/>
.	.	kIx.	.
</s>
<s>
Pěstování	pěstování	k1gNnSc1	pěstování
bonsají	bonsaj	k1gFnPc2	bonsaj
se	se	k3xPyFc4	se
po	po	k7c6	po
sinojaponské	sinojaponský	k2eAgFnSc6d1	sinojaponský
oblasti	oblast	k1gFnSc6	oblast
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
velmi	velmi	k6eAd1	velmi
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
paralelně	paralelně	k6eAd1	paralelně
se	s	k7c7	s
šířením	šíření	k1gNnSc7	šíření
buddhismu	buddhismus	k1gInSc2	buddhismus
<g/>
.	.	kIx.	.
</s>
<s>
Umění	umění	k1gNnSc1	umění
bonsai	bonsai	k1gFnSc1	bonsai
Kritéria	kritérion	k1gNnSc2	kritérion
pro	pro	k7c4	pro
posuzování	posuzování	k1gNnSc4	posuzování
rostlin	rostlina	k1gFnPc2	rostlina
jako	jako	k8xS	jako
bonsají	bonsaj	k1gFnPc2	bonsaj
jsou	být	k5eAaImIp3nP	být
sporná	sporný	k2eAgNnPc1d1	sporné
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
zde	zde	k6eAd1	zde
roli	role	k1gFnSc4	role
hraje	hrát	k5eAaImIp3nS	hrát
nejen	nejen	k6eAd1	nejen
technická	technický	k2eAgFnSc1d1	technická
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
estetická	estetický	k2eAgFnSc1d1	estetická
stránka	stránka	k1gFnSc1	stránka
úprav	úprava	k1gFnPc2	úprava
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc2	jejich
výsledků	výsledek	k1gInPc2	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
původním	původní	k2eAgNnSc6d1	původní
asijském	asijský	k2eAgNnSc6d1	asijské
pojetí	pojetí	k1gNnSc6	pojetí
jsou	být	k5eAaImIp3nP	být
bonsaje	bonsaj	k1gInPc1	bonsaj
nejen	nejen	k6eAd1	nejen
okrasnými	okrasný	k2eAgFnPc7d1	okrasná
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
uměleckými	umělecký	k2eAgInPc7d1	umělecký
artefakty	artefakt	k1gInPc7	artefakt
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
hodnotu	hodnota	k1gFnSc4	hodnota
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
nejen	nejen	k6eAd1	nejen
samotná	samotný	k2eAgFnSc1d1	samotná
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
pěstební	pěstební	k2eAgFnSc1d1	pěstební
miska	miska	k1gFnSc1	miska
a	a	k8xC	a
povrch	povrch	k1gInSc1	povrch
půdy	půda	k1gFnSc2	půda
(	(	kIx(	(
<g/>
zastupující	zastupující	k2eAgFnSc4d1	zastupující
krajinu	krajina	k1gFnSc4	krajina
nebo	nebo	k8xC	nebo
její	její	k3xOp3gFnSc1	její
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
úpravy	úprava	k1gFnSc2	úprava
rostlinu	rostlina	k1gFnSc4	rostlina
nemrzačí	mrzačet	k5eNaImIp3nS	mrzačet
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
miniaturizuje	miniaturizovat	k5eAaImIp3nS	miniaturizovat
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
obvykle	obvykle	k6eAd1	obvykle
ani	ani	k8xC	ani
zakrslá	zakrslý	k2eAgFnSc1d1	zakrslá
rostlina	rostlina	k1gFnSc1	rostlina
-	-	kIx~	-
neroste	růst	k5eNaImIp3nS	růst
<g/>
-li	i	k?	-li
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
co	co	k3yRnSc4	co
tvarovat	tvarovat	k5eAaImF	tvarovat
a	a	k8xC	a
jak	jak	k6eAd1	jak
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
pracovat	pracovat	k5eAaImF	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vysazení	vysazení	k1gNnSc6	vysazení
do	do	k7c2	do
volné	volný	k2eAgFnSc2d1	volná
půdy	půda	k1gFnSc2	půda
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
může	moct	k5eAaImIp3nS	moct
vyrůst	vyrůst	k5eAaPmF	vyrůst
typický	typický	k2eAgMnSc1d1	typický
zástupce	zástupce	k1gMnSc1	zástupce
daného	daný	k2eAgInSc2d1	daný
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vše	všechen	k3xTgNnSc1	všechen
na	na	k7c6	na
bonsaji	bonsaj	k1gInSc6	bonsaj
bylo	být	k5eAaImAgNnS	být
zmenšeno	zmenšen	k2eAgNnSc1d1	zmenšeno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
všechny	všechen	k3xTgInPc1	všechen
kompoziční	kompoziční	k2eAgInPc1d1	kompoziční
prvky	prvek	k1gInPc1	prvek
musí	muset	k5eAaImIp3nP	muset
tvořit	tvořit	k5eAaImF	tvořit
jeden	jeden	k4xCgInSc4	jeden
harmonický	harmonický	k2eAgInSc4d1	harmonický
celek	celek	k1gInSc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
bonsai	bonsai	k1gFnSc1	bonsai
lze	lze	k6eAd1	lze
pěstovat	pěstovat	k5eAaImF	pěstovat
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
rostlinu	rostlina	k1gFnSc4	rostlina
<g/>
,	,	kIx,	,
obtížnost	obtížnost	k1gFnSc1	obtížnost
pěstování	pěstování	k1gNnSc1	pěstování
a	a	k8xC	a
estetická	estetický	k2eAgFnSc1d1	estetická
uspokojivost	uspokojivost	k1gFnSc1	uspokojivost
výsledků	výsledek	k1gInPc2	výsledek
se	se	k3xPyFc4	se
ale	ale	k9	ale
u	u	k7c2	u
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
druhů	druh	k1gInPc2	druh
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
hlavním	hlavní	k2eAgInSc7d1	hlavní
typem	typ	k1gInSc7	typ
bonsají	bonsaj	k1gFnPc2	bonsaj
jsou	být	k5eAaImIp3nP	být
dřeviny	dřevina	k1gFnPc4	dřevina
-	-	kIx~	-
keře	keř	k1gInPc4	keř
<g/>
,	,	kIx,	,
liány	liána	k1gFnPc4	liána
a	a	k8xC	a
stromy	strom	k1gInPc4	strom
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
bonsajistických	bonsajistický	k2eAgFnPc2d1	bonsajistická
technik	technika	k1gFnPc2	technika
úspěšně	úspěšně	k6eAd1	úspěšně
využít	využít	k5eAaPmF	využít
i	i	k9	i
pro	pro	k7c4	pro
jakékoli	jakýkoli	k3yIgFnPc4	jakýkoli
byliny	bylina	k1gFnPc4	bylina
<g/>
,	,	kIx,	,
trávy	tráva	k1gFnPc4	tráva
či	či	k8xC	či
sukulenty	sukulent	k1gInPc4	sukulent
<g/>
.	.	kIx.	.
</s>
<s>
Pravou	pravý	k2eAgFnSc4d1	pravá
estetickou	estetický	k2eAgFnSc4d1	estetická
funkci	funkce	k1gFnSc4	funkce
bonsaje	bonsaj	k1gInSc2	bonsaj
lze	lze	k6eAd1	lze
nejlépe	dobře	k6eAd3	dobře
pochopit	pochopit	k5eAaPmF	pochopit
na	na	k7c6	na
půvabu	půvab	k1gInSc6	půvab
bonsaje	bonsaj	k1gFnSc2	bonsaj
např.	např.	kA	např.
z	z	k7c2	z
běžného	běžný	k2eAgInSc2d1	běžný
jahodníku	jahodník	k1gInSc2	jahodník
<g/>
,	,	kIx,	,
smetánky	smetánka	k1gFnSc2	smetánka
nebo	nebo	k8xC	nebo
révy	réva	k1gFnSc2	réva
vinné	vinný	k2eAgFnSc2d1	vinná
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
umění	umění	k1gNnSc2	umění
tvarovat	tvarovat	k5eAaImF	tvarovat
bonsaje	bonsaj	k1gInPc4	bonsaj
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
starověké	starověký	k2eAgFnSc2d1	starověká
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pramení	pramenit	k5eAaImIp3nS	pramenit
první	první	k4xOgFnPc4	první
zmínky	zmínka	k1gFnPc4	zmínka
o	o	k7c6	o
stromech	strom	k1gInPc6	strom
pěstovaných	pěstovaný	k2eAgInPc2d1	pěstovaný
v	v	k7c6	v
miskách	miska	k1gFnPc6	miska
již	již	k6eAd1	již
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
před	před	k7c7	před
přelomem	přelom	k1gInSc7	přelom
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Čínské	čínský	k2eAgFnPc1d1	čínská
bonsaje	bonsaj	k1gFnPc1	bonsaj
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
vysoce	vysoce	k6eAd1	vysoce
rozvitou	rozvitý	k2eAgFnSc4d1	rozvitá
symboliku	symbolika	k1gFnSc4	symbolika
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
prvků	prvek	k1gInPc2	prvek
jako	jako	k8xS	jako
mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
dřevo	dřevo	k1gNnSc1	dřevo
nebo	nebo	k8xC	nebo
obnažené	obnažený	k2eAgInPc1d1	obnažený
kořeny	kořen	k1gInPc1	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Čínské	čínský	k2eAgFnPc1d1	čínská
bonsaje	bonsaj	k1gFnPc1	bonsaj
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
velmi	velmi	k6eAd1	velmi
silně	silně	k6eAd1	silně
umělé	umělý	k2eAgInPc1d1	umělý
zásahy	zásah	k1gInPc1	zásah
-	-	kIx~	-
vůli	vůle	k1gFnSc4	vůle
pěstitele	pěstitel	k1gMnSc2	pěstitel
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původních	původní	k2eAgInPc2d1	původní
čínských	čínský	k2eAgInPc2d1	čínský
názvů	název	k1gInPc2	název
bonsají	bonsaj	k1gFnPc2	bonsaj
pchen-sai	pchenai	k6eAd1	pchen-sai
či	či	k8xC	či
šan-šuei	šan-šuei	k6eAd1	šan-šuei
pchen	pchen	k2eAgInSc4d1	pchen
ťing	ťing	k1gInSc4	ťing
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
pchen-ťing	pchen-ťing	k1gInSc1	pchen-ťing
byl	být	k5eAaImAgInS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
japonský	japonský	k2eAgInSc1d1	japonský
výraz	výraz	k1gInSc1	výraz
bonsaj	bonsaj	k1gFnSc1	bonsaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
pěstování	pěstování	k1gNnSc1	pěstování
bonsají	bonsaj	k1gFnPc2	bonsaj
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
pod	pod	k7c7	pod
silným	silný	k2eAgInSc7d1	silný
čínským	čínský	k2eAgInSc7d1	čínský
vlivem	vliv	k1gInSc7	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Čína	Čína	k1gFnSc1	Čína
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
Japonsko	Japonsko	k1gNnSc4	Japonsko
a	a	k8xC	a
Koreu	Korea	k1gFnSc4	Korea
nejen	nejen	k6eAd1	nejen
uměním	umění	k1gNnSc7	umění
bonsai	bonsai	k1gFnSc4	bonsai
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
kaligrafií	kaligrafie	k1gFnSc7	kaligrafie
nebo	nebo	k8xC	nebo
písmem	písmo	k1gNnSc7	písmo
<g/>
,	,	kIx,	,
<g/>
wei-či	wei-č	k1gInPc7	wei-č
<g/>
.	.	kIx.	.
</s>
<s>
Japonci	Japonec	k1gMnPc1	Japonec
umění	umění	k1gNnSc4	umění
pěstování	pěstování	k1gNnSc2	pěstování
bonsají	bonsaj	k1gFnPc2	bonsaj
zjednodušili	zjednodušit	k5eAaPmAgMnP	zjednodušit
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
propracovali	propracovat	k5eAaPmAgMnP	propracovat
a	a	k8xC	a
zdokonalili	zdokonalit	k5eAaPmAgMnP	zdokonalit
jak	jak	k6eAd1	jak
umělecké	umělecký	k2eAgNnSc4d1	umělecké
<g/>
,	,	kIx,	,
tak	tak	k9	tak
pěstitelské	pěstitelský	k2eAgInPc4d1	pěstitelský
aspekty	aspekt	k1gInPc4	aspekt
<g/>
.	.	kIx.	.
</s>
<s>
Pozorováním	pozorování	k1gNnSc7	pozorování
volně	volně	k6eAd1	volně
rostoucích	rostoucí	k2eAgInPc2d1	rostoucí
stromů	strom	k1gInPc2	strom
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
vypracovali	vypracovat	k5eAaPmAgMnP	vypracovat
klasické	klasický	k2eAgInPc4d1	klasický
styly	styl	k1gInPc4	styl
bonsají	bonsaj	k1gFnPc2	bonsaj
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterých	který	k3yQgInPc2	který
se	se	k3xPyFc4	se
bonsaje	bonsaj	k1gFnPc1	bonsaj
tvarují	tvarovat	k5eAaImIp3nP	tvarovat
<g/>
.	.	kIx.	.
</s>
<s>
Japonské	japonský	k2eAgInPc1d1	japonský
bonsaje	bonsaj	k1gInPc1	bonsaj
vynikají	vynikat	k5eAaImIp3nP	vynikat
elegancí	elegance	k1gFnSc7	elegance
a	a	k8xC	a
neviditelností	neviditelnost	k1gFnSc7	neviditelnost
pěstitelského	pěstitelský	k2eAgMnSc2d1	pěstitelský
"	"	kIx"	"
<g/>
násilí	násilí	k1gNnSc2	násilí
<g/>
"	"	kIx"	"
i	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
drastických	drastický	k2eAgInPc2d1	drastický
zásahů	zásah	k1gInPc2	zásah
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
velká	velký	k2eAgFnSc1d1	velká
výstava	výstava	k1gFnSc1	výstava
bonsají	bonsaj	k1gFnPc2	bonsaj
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
významných	významný	k2eAgFnPc2d1	významná
osobností	osobnost	k1gFnPc2	osobnost
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
hodnotu	hodnota	k1gFnSc4	hodnota
bonsají	bonsaj	k1gFnPc2	bonsaj
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
počet	počet	k1gInSc1	počet
pěstitelů	pěstitel	k1gMnPc2	pěstitel
prudce	prudko	k6eAd1	prudko
vzrůstal	vzrůstat	k5eAaImAgInS	vzrůstat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvacátých	dvacátý	k4xOgNnPc6	dvacátý
letech	léto	k1gNnPc6	léto
postihlo	postihnout	k5eAaPmAgNnS	postihnout
oblast	oblast	k1gFnSc4	oblast
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Hokkaidó	Hokkaidó	k1gFnSc2	Hokkaidó
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
bonsají	bonsaj	k1gFnPc2	bonsaj
pěstovala	pěstovat	k5eAaImAgFnS	pěstovat
<g/>
,	,	kIx,	,
ničivé	ničivý	k2eAgNnSc4d1	ničivé
zemětřesení	zemětřesení	k1gNnSc4	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
stromků	stromek	k1gInPc2	stromek
bylo	být	k5eAaImAgNnS	být
zničeno	zničit	k5eAaPmNgNnS	zničit
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
pěstitelů	pěstitel	k1gMnPc2	pěstitel
přišla	přijít	k5eAaPmAgFnS	přijít
o	o	k7c4	o
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
Japonci	Japonec	k1gMnPc1	Japonec
založili	založit	k5eAaPmAgMnP	založit
v	v	k7c4	v
Omije	Omij	k1gInPc4	Omij
nedaleko	nedaleko	k7c2	nedaleko
Tokia	Tokio	k1gNnSc2	Tokio
bonsajistickou	bonsajistický	k2eAgFnSc4d1	bonsajistická
vesnici	vesnice	k1gFnSc4	vesnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
znovu	znovu	k6eAd1	znovu
začali	začít	k5eAaPmAgMnP	začít
pěstovat	pěstovat	k5eAaImF	pěstovat
a	a	k8xC	a
tvarovat	tvarovat	k5eAaImF	tvarovat
miniaturní	miniaturní	k2eAgInPc4d1	miniaturní
stromky	stromek	k1gInPc4	stromek
<g/>
.	.	kIx.	.
</s>
<s>
Omiya	Omiya	k1gFnSc1	Omiya
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
tokijskou	tokijský	k2eAgFnSc7d1	Tokijská
aglomerací	aglomerace	k1gFnSc7	aglomerace
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
původních	původní	k2eAgFnPc2d1	původní
školek	školka	k1gFnPc2	školka
zde	zde	k6eAd1	zde
nicméně	nicméně	k8xC	nicméně
dosud	dosud	k6eAd1	dosud
existují	existovat	k5eAaImIp3nP	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Korea	Korea	k1gFnSc1	Korea
<g/>
,	,	kIx,	,
Vietnam	Vietnam	k1gInSc1	Vietnam
<g/>
,	,	kIx,	,
Tchaj-wan	Tchajan	k1gInSc1	Tchaj-wan
<g/>
,	,	kIx,	,
a	a	k8xC	a
Indonésie	Indonésie	k1gFnSc1	Indonésie
existuje	existovat	k5eAaImIp3nS	existovat
několikasetletá	několikasetletý	k2eAgFnSc1d1	několikasetletá
bonsajistická	bonsajistický	k2eAgFnSc1d1	bonsajistická
tradice	tradice	k1gFnSc1	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Tamní	tamní	k2eAgMnPc1d1	tamní
pěstitelé	pěstitel	k1gMnPc1	pěstitel
si	se	k3xPyFc3	se
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
vlastní	vlastní	k2eAgInPc4d1	vlastní
styly	styl	k1gInPc4	styl
pěstování	pěstování	k1gNnSc2	pěstování
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
výsledkem	výsledek	k1gInSc7	výsledek
nejen	nejen	k6eAd1	nejen
samostatného	samostatný	k2eAgInSc2d1	samostatný
vývoje	vývoj	k1gInSc2	vývoj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
rozdílných	rozdílný	k2eAgFnPc2d1	rozdílná
klimatických	klimatický	k2eAgFnPc2d1	klimatická
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
bonsaje	bonsaj	k1gFnPc1	bonsaj
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
téměř	téměř	k6eAd1	téměř
všude	všude	k6eAd1	všude
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
rostliny	rostlina	k1gFnPc1	rostlina
pěstovány	pěstovat	k5eAaImNgFnP	pěstovat
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
aktivními	aktivní	k2eAgFnPc7d1	aktivní
bonsajistickými	bonsajistický	k2eAgFnPc7d1	bonsajistická
oblastmi	oblast	k1gFnPc7	oblast
současnosti	současnost	k1gFnPc4	současnost
jsou	být	k5eAaImIp3nP	být
Severní	severní	k2eAgFnSc1d1	severní
i	i	k8xC	i
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc1d1	jižní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
pěstitelů	pěstitel	k1gMnPc2	pěstitel
bonsají	bonsaj	k1gFnPc2	bonsaj
žije	žít	k5eAaImIp3nS	žít
také	také	k9	také
v	v	k7c6	v
exsovětských	exsovětský	k2eAgInPc6d1	exsovětský
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
první	první	k4xOgInPc1	první
postupy	postup	k1gInPc1	postup
pro	pro	k7c4	pro
pěstování	pěstování	k1gNnSc4	pěstování
bonsají	bonsaj	k1gFnPc2	bonsaj
objevují	objevovat	k5eAaImIp3nP	objevovat
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
to	ten	k3xDgNnSc1	ten
však	však	k9	však
není	být	k5eNaImIp3nS	být
z	z	k7c2	z
dekorativních	dekorativní	k2eAgInPc2d1	dekorativní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ryze	ryze	k6eAd1	ryze
z	z	k7c2	z
praktických	praktický	k2eAgInPc2d1	praktický
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
podomní	podomní	k2eAgMnPc4d1	podomní
obchodníky	obchodník	k1gMnPc4	obchodník
bylo	být	k5eAaImAgNnS	být
výhodné	výhodný	k2eAgFnPc4d1	výhodná
nosit	nosit	k5eAaImF	nosit
prodávané	prodávaný	k2eAgInPc4d1	prodávaný
stromy	strom	k1gInPc4	strom
s	s	k7c7	s
sebou	se	k3xPyFc7	se
v	v	k7c6	v
nádobách	nádoba	k1gFnPc6	nádoba
<g/>
.	.	kIx.	.
</s>
<s>
Skutečné	skutečný	k2eAgInPc1d1	skutečný
bonsaje	bonsaj	k1gInPc1	bonsaj
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
poprvé	poprvé	k6eAd1	poprvé
na	na	k7c6	na
výstavě	výstava	k1gFnSc6	výstava
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
však	však	k9	však
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
o	o	k7c4	o
bonsaje	bonsaj	k1gInPc4	bonsaj
starat	starat	k5eAaImF	starat
a	a	k8xC	a
tak	tak	k6eAd1	tak
většina	většina	k1gFnSc1	většina
dovezených	dovezený	k2eAgInPc2d1	dovezený
stromů	strom	k1gInPc2	strom
hyne	hynout	k5eAaImIp3nS	hynout
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
rozmach	rozmach	k1gInSc1	rozmach
pěstování	pěstování	k1gNnSc1	pěstování
bonsají	bonsaj	k1gFnPc2	bonsaj
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
i	i	k9	i
v	v	k7c6	v
zbytku	zbytek	k1gInSc6	zbytek
světa	svět	k1gInSc2	svět
<g/>
)	)	kIx)	)
nastal	nastat	k5eAaPmAgInS	nastat
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
vojáci	voják	k1gMnPc1	voják
bojující	bojující	k2eAgMnPc1d1	bojující
na	na	k7c6	na
Dálném	dálný	k2eAgInSc6d1	dálný
východě	východ	k1gInSc6	východ
přivezli	přivézt	k5eAaPmAgMnP	přivézt
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
bonsají	bonsaj	k1gFnPc2	bonsaj
jako	jako	k8xC	jako
válečnou	válečný	k2eAgFnSc4d1	válečná
kořist	kořist	k1gFnSc4	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Bonsaje	bonsaj	k1gFnPc1	bonsaj
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
rozšiřují	rozšiřovat	k5eAaImIp3nP	rozšiřovat
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
a	a	k8xC	a
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
také	také	k9	také
postupy	postup	k1gInPc1	postup
pro	pro	k7c4	pro
správné	správný	k2eAgNnSc4d1	správné
pěstování	pěstování	k1gNnSc4	pěstování
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
USA	USA	kA	USA
se	se	k3xPyFc4	se
bonsaje	bonsaj	k1gFnPc1	bonsaj
dostaly	dostat	k5eAaPmAgFnP	dostat
především	především	k9	především
díky	díky	k7c3	díky
čínské	čínský	k2eAgFnSc3d1	čínská
a	a	k8xC	a
japonské	japonský	k2eAgFnSc3d1	japonská
emigraci	emigrace	k1gFnSc3	emigrace
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
přišla	přijít	k5eAaPmAgFnS	přijít
řada	řada	k1gFnSc1	řada
bonsajistickým	bonsajistický	k2eAgMnPc3d1	bonsajistický
mistrů	mistr	k1gMnPc2	mistr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
pěstování	pěstování	k1gNnSc1	pěstování
bonsají	bonsaj	k1gFnPc2	bonsaj
oproti	oproti	k7c3	oproti
zbylé	zbylý	k2eAgFnSc3d1	zbylá
Evropě	Evropa	k1gFnSc3	Evropa
výrazně	výrazně	k6eAd1	výrazně
opozdilo	opozdit	k5eAaPmAgNnS	opozdit
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
bonsajích	bonsaj	k1gInPc6	bonsaj
do	do	k7c2	do
Česka	Česko	k1gNnSc2	Česko
přinesl	přinést	k5eAaPmAgInS	přinést
již	již	k6eAd1	již
za	za	k7c2	za
Rakouského	rakouský	k2eAgNnSc2d1	rakouské
císařství	císařství	k1gNnSc2	císařství
cestovatel	cestovatel	k1gMnSc1	cestovatel
Kořenský	Kořenský	k2eAgMnSc1d1	Kořenský
a	a	k8xC	a
s	s	k7c7	s
nim	on	k3xPp3gInPc3	on
řada	řada	k1gFnSc1	řada
japanistů	japanista	k1gMnPc2	japanista
<g/>
,	,	kIx,	,
a	a	k8xC	a
přestože	přestože	k8xS	přestože
byly	být	k5eAaImAgFnP	být
rostliny	rostlina	k1gFnPc1	rostlina
importovány	importován	k2eAgFnPc1d1	importována
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
absenci	absence	k1gFnSc3	absence
technologických	technologický	k2eAgInPc2d1	technologický
postupů	postup	k1gInPc2	postup
byla	být	k5eAaImAgFnS	být
jejich	jejich	k3xOp3gFnSc1	jejich
introdukce	introdukce	k1gFnSc1	introdukce
neúspěšná	úspěšný	k2eNgFnSc1d1	neúspěšná
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgFnP	být
bonsaje	bonsaj	k1gFnPc1	bonsaj
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
víceméně	víceméně	k9	víceméně
neznámým	známý	k2eNgInSc7d1	neznámý
pojmem	pojem	k1gInSc7	pojem
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byla	být	k5eAaImAgFnS	být
především	především	k9	především
nepřítomnost	nepřítomnost	k1gFnSc1	nepřítomnost
českých	český	k2eAgMnPc2d1	český
vojáků	voják	k1gMnPc2	voják
na	na	k7c6	na
bojištích	bojiště	k1gNnPc6	bojiště
Dálného	dálný	k2eAgInSc2d1	dálný
východu	východ	k1gInSc2	východ
a	a	k8xC	a
poté	poté	k6eAd1	poté
uzavřenost	uzavřenost	k1gFnSc4	uzavřenost
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
společnosti	společnost	k1gFnSc2	společnost
ČSSR	ČSSR	kA	ČSSR
vůči	vůči	k7c3	vůči
Západu	západ	k1gInSc3	západ
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
totální	totální	k2eAgFnSc4d1	totální
absenci	absence	k1gFnSc4	absence
jakýchkoli	jakýkoli	k3yIgInPc2	jakýkoli
technologických	technologický	k2eAgInPc2d1	technologický
postupů	postup	k1gInPc2	postup
(	(	kIx(	(
<g/>
embargo	embargo	k1gNnSc1	embargo
literatury	literatura	k1gFnSc2	literatura
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
<g/>
)	)	kIx)	)
objevovalo	objevovat	k5eAaImAgNnS	objevovat
již	již	k6eAd1	již
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
několik	několik	k4yIc4	několik
pěstitelů	pěstitel	k1gMnPc2	pěstitel
bonsajistické	bonsajistický	k2eAgInPc4d1	bonsajistický
principy	princip	k1gInPc4	princip
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
bonsajový	bonsajový	k2eAgInSc1d1	bonsajový
klub	klub	k1gInSc1	klub
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
Bonsai	bonsai	k1gFnSc1	bonsai
klub	klub	k1gInSc1	klub
Praha	Praha	k1gFnSc1	Praha
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Výrazného	výrazný	k2eAgNnSc2d1	výrazné
rozšíření	rozšíření	k1gNnSc2	rozšíření
se	se	k3xPyFc4	se
pěstování	pěstování	k1gNnSc1	pěstování
bonsají	bonsaj	k1gFnPc2	bonsaj
dočkalo	dočkat	k5eAaPmAgNnS	dočkat
až	až	k9	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Bonsaje	bonsaj	k1gFnPc4	bonsaj
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
dělit	dělit	k5eAaImF	dělit
podle	podle	k7c2	podle
mnoha	mnoho	k4c2	mnoho
způsobů	způsob	k1gInPc2	způsob
<g/>
,	,	kIx,	,
nejběžnější	běžný	k2eAgNnSc1d3	nejběžnější
je	být	k5eAaImIp3nS	být
dělení	dělení	k1gNnSc1	dělení
podle	podle	k7c2	podle
velikosti	velikost	k1gFnSc2	velikost
a	a	k8xC	a
podle	podle	k7c2	podle
umístění	umístění	k1gNnSc2	umístění
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
podle	podle	k7c2	podle
umístění	umístění	k1gNnSc2	umístění
je	být	k5eAaImIp3nS	být
závislé	závislý	k2eAgNnSc1d1	závislé
na	na	k7c6	na
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
bonsaj	bonsaj	k1gFnSc1	bonsaj
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc1d1	stejný
strom	strom	k1gInSc1	strom
se	se	k3xPyFc4	se
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
může	moct	k5eAaImIp3nS	moct
pěstovat	pěstovat	k5eAaImF	pěstovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
interiéru	interiér	k1gInSc6	interiér
a	a	k8xC	a
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
jako	jako	k8xS	jako
venkovní	venkovní	k2eAgNnSc1d1	venkovní
<g/>
.	.	kIx.	.
</s>
<s>
Venkovní	venkovní	k2eAgFnPc1d1	venkovní
Venkovní	venkovní	k2eAgFnPc1d1	venkovní
bonsaje	bonsaj	k1gFnPc1	bonsaj
jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
ty	ten	k3xDgInPc1	ten
stromy	strom	k1gInPc1	strom
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
běžně	běžně	k6eAd1	běžně
rostou	růst	k5eAaImIp3nP	růst
v	v	k7c6	v
daném	daný	k2eAgNnSc6d1	dané
klimatickém	klimatický	k2eAgNnSc6d1	klimatické
pásmu	pásmo	k1gNnSc6	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nás	my	k3xPp1nPc4	my
jsou	být	k5eAaImIp3nP	být
nejoblíbenější	oblíbený	k2eAgInPc1d3	nejoblíbenější
smrky	smrk	k1gInPc1	smrk
<g/>
,	,	kIx,	,
borovice	borovice	k1gFnPc1	borovice
<g/>
,	,	kIx,	,
javory	javor	k1gInPc1	javor
<g/>
,	,	kIx,	,
buky	buk	k1gInPc1	buk
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
bonsaje	bonsaj	k1gInPc1	bonsaj
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
ve	v	k7c6	v
venkovním	venkovní	k2eAgNnSc6d1	venkovní
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
v	v	k7c6	v
interiéru	interiér	k1gInSc6	interiér
by	by	kYmCp3nP	by
nepřežily	přežít	k5eNaPmAgFnP	přežít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
zimovat	zimovat	k5eAaImF	zimovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
promrznutí	promrznutí	k1gNnSc3	promrznutí
kořenového	kořenový	k2eAgInSc2d1	kořenový
balu	bal	k1gInSc2	bal
a	a	k8xC	a
následného	následný	k2eAgNnSc2d1	následné
uschnutí	uschnutí	k1gNnSc2	uschnutí
(	(	kIx(	(
<g/>
led	led	k1gInSc1	led
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
rostliny	rostlina	k1gFnPc4	rostlina
voda	voda	k1gFnSc1	voda
fyziologicky	fyziologicky	k6eAd1	fyziologicky
suchá	suchý	k2eAgFnSc1d1	suchá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokojové	pokojový	k2eAgInPc1d1	pokojový
Pokojové	pokojový	k2eAgInPc1d1	pokojový
bonsaje	bonsaj	k1gInPc1	bonsaj
jsou	být	k5eAaImIp3nP	být
stromy	strom	k1gInPc7	strom
z	z	k7c2	z
teplejších	teplý	k2eAgNnPc2d2	teplejší
klimatických	klimatický	k2eAgNnPc2d1	klimatické
pásem	pásmo	k1gNnPc2	pásmo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
panují	panovat	k5eAaImIp3nP	panovat
rozdílné	rozdílný	k2eAgFnPc4d1	rozdílná
klimatické	klimatický	k2eAgFnPc4d1	klimatická
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
daných	daný	k2eAgFnPc6d1	daná
bytových	bytový	k2eAgFnPc6d1	bytová
podmínkách	podmínka	k1gFnPc6	podmínka
daří	dařit	k5eAaImIp3nS	dařit
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
bonsai	bonsai	k1gFnSc4	bonsai
vypěstovat	vypěstovat	k5eAaPmF	vypěstovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
(	(	kIx(	(
<g/>
a	a	k8xC	a
vhodné	vhodný	k2eAgInPc1d1	vhodný
<g/>
)	)	kIx)	)
tzv.	tzv.	kA	tzv.
letnění	letnění	k1gNnSc1	letnění
stromku	stromek	k1gInSc2	stromek
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
v	v	k7c6	v
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nehrozí	hrozit	k5eNaImIp3nP	hrozit
mrazy	mráz	k1gInPc1	mráz
<g/>
,	,	kIx,	,
strom	strom	k1gInSc1	strom
umístí	umístit	k5eAaPmIp3nS	umístit
ven	ven	k6eAd1	ven
a	a	k8xC	a
na	na	k7c4	na
zimu	zima	k1gFnSc4	zima
je	být	k5eAaImIp3nS	být
vrácen	vrátit	k5eAaPmNgInS	vrátit
do	do	k7c2	do
interiéru	interiér	k1gInSc2	interiér
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
některé	některý	k3yIgInPc4	některý
fíkusy	fíkus	k1gInPc4	fíkus
(	(	kIx(	(
<g/>
Benjamina	Benjamin	k1gMnSc2	Benjamin
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kraptomerie	kraptomerie	k1gFnSc1	kraptomerie
<g/>
.	.	kIx.	.
</s>
<s>
Letnění	letnění	k1gNnSc1	letnění
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
rostlin	rostlina	k1gFnPc2	rostlina
prospěšné	prospěšný	k2eAgNnSc1d1	prospěšné
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
nutno	nutno	k6eAd1	nutno
dát	dát	k5eAaPmF	dát
pozor	pozor	k1gInSc4	pozor
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
zvyklé	zvyklý	k2eAgFnPc1d1	zvyklá
na	na	k7c4	na
přímé	přímý	k2eAgNnSc4d1	přímé
sluneční	sluneční	k2eAgNnSc4d1	sluneční
záření	záření	k1gNnSc4	záření
<g/>
,	,	kIx,	,
nebyly	být	k5eNaImAgFnP	být
sluncem	slunce	k1gNnSc7	slunce
popáleny	popálit	k5eAaPmNgFnP	popálit
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
kategoriemi	kategorie	k1gFnPc7	kategorie
nejsou	být	k5eNaImIp3nP	být
přesně	přesně	k6eAd1	přesně
stanoveny	stanovit	k5eAaPmNgInP	stanovit
<g/>
.	.	kIx.	.
</s>
<s>
Minibonsaj	Minibonsaj	k1gFnSc1	Minibonsaj
(	(	kIx(	(
<g/>
mame	mam	k1gInSc5	mam
bonsai	bonsai	k1gFnSc1	bonsai
<g/>
)	)	kIx)	)
Výška	výška	k1gFnSc1	výška
1-8	[number]	k4	1-8
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Bonsaj	bonsaj	k1gFnSc1	bonsaj
se	se	k3xPyFc4	se
vejde	vejít	k5eAaPmIp3nS	vejít
na	na	k7c4	na
konec	konec	k1gInSc4	konec
prstu	prst	k1gInSc2	prst
<g/>
,	,	kIx,	,
japonci	japonek	k1gMnPc1	japonek
je	on	k3xPp3gNnSc4	on
nazývají	nazývat	k5eAaImIp3nP	nazývat
"	"	kIx"	"
<g/>
makové	makový	k2eAgNnSc1d1	makové
semínko	semínko	k1gNnSc1	semínko
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
bobové	bobový	k2eAgInPc1d1	bobový
bonsaje	bonsaj	k1gInPc1	bonsaj
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
velkou	velký	k2eAgFnSc4d1	velká
péči	péče	k1gFnSc4	péče
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
se	se	k3xPyFc4	se
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
ze	z	k7c2	z
semen	semeno	k1gNnPc2	semeno
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
misce	miska	k1gFnSc6	miska
<g/>
.	.	kIx.	.
</s>
<s>
Miniaturní	miniaturní	k2eAgFnSc1d1	miniaturní
Výška	výška	k1gFnSc1	výška
8-15	[number]	k4	8-15
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Bonsaj	bonsaj	k1gFnSc1	bonsaj
se	se	k3xPyFc4	se
vejde	vejít	k5eAaPmIp3nS	vejít
do	do	k7c2	do
dlaně	dlaň	k1gFnSc2	dlaň
ruky	ruka	k1gFnSc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgFnSc1d1	Malé
Výška	výška	k1gFnSc1	výška
15-30	[number]	k4	15-30
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Bonsaj	bonsaj	k1gInSc4	bonsaj
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
přenést	přenést	k5eAaPmF	přenést
jednou	jeden	k4xCgFnSc7	jeden
rukou	ruka	k1gFnSc7	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Středně	středně	k6eAd1	středně
velké	velká	k1gFnPc1	velká
Výška	výška	k1gFnSc1	výška
30-60	[number]	k4	30-60
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Bonsaj	bonsaj	k1gFnSc1	bonsaj
se	se	k3xPyFc4	se
přenáší	přenášet	k5eAaImIp3nS	přenášet
oběma	dva	k4xCgFnPc7	dva
rukama	ruka	k1gFnPc7	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velká	k1gFnSc3	velká
Výška	výška	k1gFnSc1	výška
60-90	[number]	k4	60-90
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Bonsaj	bonsaj	k1gFnSc4	bonsaj
musí	muset	k5eAaImIp3nP	muset
přenášet	přenášet	k5eAaImF	přenášet
dva	dva	k4xCgMnPc1	dva
muži	muž	k1gMnPc1	muž
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
pěstitelé	pěstitel	k1gMnPc1	pěstitel
stanovují	stanovovat	k5eAaImIp3nP	stanovovat
maximální	maximální	k2eAgFnSc4d1	maximální
výšku	výška	k1gFnSc4	výška
na	na	k7c4	na
120	[number]	k4	120
cm	cm	kA	cm
<g/>
,	,	kIx,	,
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
běžné	běžný	k2eAgFnPc1d1	běžná
i	i	k8xC	i
dvoumetrové	dvoumetrový	k2eAgFnPc1d1	dvoumetrová
bonsaje	bonsaj	k1gFnPc1	bonsaj
<g/>
.	.	kIx.	.
koupené	koupený	k2eAgFnPc1d1	koupená
z	z	k7c2	z
přírody	příroda	k1gFnSc2	příroda
(	(	kIx(	(
<g/>
jamadori	jamadori	k6eAd1	jamadori
<g/>
)	)	kIx)	)
-	-	kIx~	-
vykopání	vykopání	k1gNnSc1	vykopání
dospělého	dospělý	k2eAgInSc2d1	dospělý
stromu	strom	k1gInSc2	strom
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vlivem	vlivem	k7c2	vlivem
nepříznivých	příznivý	k2eNgFnPc2d1	nepříznivá
podmínek	podmínka	k1gFnPc2	podmínka
zůstal	zůstat	k5eAaPmAgInS	zůstat
zakrnělý	zakrnělý	k2eAgMnSc1d1	zakrnělý
upravené	upravený	k2eAgInPc4d1	upravený
stromky	stromek	k1gInPc4	stromek
vypěstované	vypěstovaný	k2eAgInPc4d1	vypěstovaný
ze	z	k7c2	z
semínka	semínko	k1gNnSc2	semínko
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
hrnkovky	hrnkovka	k1gFnPc1	hrnkovka
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
potensai	potensai	k6eAd1	potensai
-	-	kIx~	-
z	z	k7c2	z
anglického	anglický	k2eAgMnSc2d1	anglický
<g/>
:	:	kIx,	:
potencial	potencial	k1gInSc1	potencial
=	=	kIx~	=
potenciální	potenciální	k2eAgMnSc1d1	potenciální
+	+	kIx~	+
japonská	japonský	k2eAgFnSc1d1	japonská
koncovka	koncovka	k1gFnSc1	koncovka
–	–	k?	–
<g/>
sai	sai	k?	sai
=	=	kIx~	=
tác	tác	k1gInSc1	tác
<g/>
,	,	kIx,	,
mísa	mísa	k1gFnSc1	mísa
<g/>
)	)	kIx)	)
-	-	kIx~	-
(	(	kIx(	(
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
mladé	mladý	k2eAgFnPc4d1	mladá
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zatím	zatím	k6eAd1	zatím
nedosahují	dosahovat	k5eNaImIp3nP	dosahovat
kvalit	kvalita	k1gFnPc2	kvalita
bonsaje	bonsaj	k1gInSc2	bonsaj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
natolik	natolik	k6eAd1	natolik
kvalitní	kvalitní	k2eAgInPc1d1	kvalitní
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
dobrá	dobrá	k9	dobrá
bonsai	bonsai	k1gFnSc1	bonsai
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vypěstována	vypěstován	k2eAgFnSc1d1	vypěstována
<g/>
)	)	kIx)	)
vypěstované	vypěstovaný	k2eAgInPc1d1	vypěstovaný
z	z	k7c2	z
řízků	řízek	k1gInPc2	řízek
<g/>
,	,	kIx,	,
roubované	roubovaný	k2eAgFnPc1d1	roubovaná
(	(	kIx(	(
<g/>
kóradabuki	kóradabuk	k1gFnPc1	kóradabuk
<g/>
)	)	kIx)	)
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Styly	styl	k1gInPc4	styl
bonsají	bonsaj	k1gFnPc2	bonsaj
<g/>
.	.	kIx.	.
</s>
<s>
Bonsaje	bonsaj	k1gFnPc1	bonsaj
se	se	k3xPyFc4	se
rámcově	rámcově	k6eAd1	rámcově
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
především	především	k9	především
podle	podle	k7c2	podle
tvaru	tvar	k1gInSc2	tvar
kmene	kmen	k1gInSc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
základních	základní	k2eAgInPc2d1	základní
stylů	styl	k1gInPc2	styl
tak	tak	k6eAd1	tak
tvoří	tvořit	k5eAaImIp3nP	tvořit
<g/>
:	:	kIx,	:
vzpřímený	vzpřímený	k2eAgInSc4d1	vzpřímený
styl	styl	k1gInSc4	styl
-	-	kIx~	-
rovný	rovný	k2eAgInSc4d1	rovný
kmen	kmen	k1gInSc4	kmen
volně	volně	k6eAd1	volně
vzpřímený	vzpřímený	k2eAgInSc4d1	vzpřímený
styl	styl	k1gInSc4	styl
-	-	kIx~	-
kmen	kmen	k1gInSc1	kmen
je	být	k5eAaImIp3nS	být
lehce	lehko	k6eAd1	lehko
zvlněný	zvlněný	k2eAgInSc1d1	zvlněný
šikmý	šikmý	k2eAgInSc1d1	šikmý
styl	styl	k1gInSc1	styl
-	-	kIx~	-
kmen	kmen	k1gInSc1	kmen
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
zešikmený	zešikmený	k2eAgInSc4d1	zešikmený
volně	volně	k6eAd1	volně
šikmý	šikmý	k2eAgInSc4d1	šikmý
styl	styl	k1gInSc4	styl
-	-	kIx~	-
šikmo	šikmo	k6eAd1	šikmo
rostoucí	rostoucí	k2eAgInSc1d1	rostoucí
kmen	kmen	k1gInSc1	kmen
se	s	k7c7	s
zvlněnou	zvlněný	k2eAgFnSc7d1	zvlněná
linií	linie	k1gFnSc7	linie
polokaskádový	polokaskádový	k2eAgInSc1d1	polokaskádový
styl	styl	k1gInSc1	styl
-	-	kIx~	-
kmen	kmen	k1gInSc1	kmen
je	být	k5eAaImIp3nS	být
ohnutý	ohnutý	k2eAgInSc1d1	ohnutý
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
či	či	k8xC	či
víceméně	víceméně	k9	víceméně
vodorovný	vodorovný	k2eAgInSc1d1	vodorovný
-	-	kIx~	-
terminál	terminál	k1gInSc1	terminál
je	být	k5eAaImIp3nS	být
nad	nad	k7c7	nad
úrovní	úroveň	k1gFnSc7	úroveň
paty	pata	k1gFnSc2	pata
kmene	kmen	k1gInSc2	kmen
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
výšce	výška	k1gFnSc6	výška
<g/>
.	.	kIx.	.
kaskádový	kaskádový	k2eAgInSc1d1	kaskádový
styl	styl	k1gInSc1	styl
-	-	kIx~	-
kmen	kmen	k1gInSc1	kmen
se	se	k3xPyFc4	se
ohýbá	ohýbat	k5eAaImIp3nS	ohýbat
pod	pod	k7c4	pod
úroveň	úroveň	k1gFnSc4	úroveň
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
vrchol	vrchol	k1gInSc4	vrchol
kmene	kmen	k1gInSc2	kmen
leží	ležet	k5eAaImIp3nP	ležet
pod	pod	k7c7	pod
patou	pata	k1gFnSc7	pata
kmene	kmen	k1gInSc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Styly	styl	k1gInPc1	styl
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
počtu	počet	k1gInSc2	počet
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
uspořádání	uspořádání	k1gNnSc1	uspořádání
větví	větev	k1gFnPc2	větev
a	a	k8xC	a
kořenů	kořen	k1gInPc2	kořen
<g/>
,	,	kIx,	,
dramatických	dramatický	k2eAgInPc2d1	dramatický
doplňků	doplněk	k1gInPc2	doplněk
<g/>
,	,	kIx,	,
osazení	osazení	k1gNnSc2	osazení
stromu	strom	k1gInSc2	strom
atd.	atd.	kA	atd.
Bonsaje	bonsaj	k1gInSc2	bonsaj
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
dělit	dělit	k5eAaImF	dělit
i	i	k9	i
podle	podle	k7c2	podle
dalších	další	k2eAgNnPc2d1	další
kritérií	kritérion	k1gNnPc2	kritérion
-	-	kIx~	-
olistění	olistěný	k2eAgMnPc1d1	olistěný
(	(	kIx(	(
<g/>
listnaté	listnatý	k2eAgNnSc1d1	listnaté
<g/>
,	,	kIx,	,
jehličnaté	jehličnatý	k2eAgFnPc1d1	jehličnatá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kvetení	kvetení	k1gNnSc1	kvetení
(	(	kIx(	(
<g/>
kvetoucí	kvetoucí	k2eAgFnSc1d1	kvetoucí
<g/>
,	,	kIx,	,
nekvetoucí	kvetoucí	k2eNgFnSc1d1	nekvetoucí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
hlavního	hlavní	k2eAgInSc2d1	hlavní
okrasného	okrasný	k2eAgInSc2d1	okrasný
prvku	prvek	k1gInSc2	prvek
(	(	kIx(	(
<g/>
nahé	nahý	k2eAgFnSc2d1	nahá
<g/>
,	,	kIx,	,
olistění	olistěný	k2eAgMnPc1d1	olistěný
<g/>
,	,	kIx,	,
květ	květ	k1gInSc4	květ
<g/>
,	,	kIx,	,
plody	plod	k1gInPc4	plod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
podle	podle	k7c2	podle
dramatických	dramatický	k2eAgInPc2d1	dramatický
doplňků	doplněk	k1gInPc2	doplněk
(	(	kIx(	(
<g/>
sabamiki	sabamikit	k5eAaImRp2nS	sabamikit
<g/>
,	,	kIx,	,
sharimiki	sharimik	k1gMnSc5	sharimik
<g/>
,	,	kIx,	,
uro	uro	k?	uro
<g/>
,	,	kIx,	,
jin	jin	k?	jin
a	a	k8xC	a
pod	pod	k7c7	pod
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
podle	podle	k7c2	podle
charakteru	charakter	k1gInSc2	charakter
růstu	růst	k1gInSc2	růst
kořenů	kořen	k1gInPc2	kořen
<g/>
(	(	kIx(	(
chůdovité	chůdovitý	k2eAgInPc4d1	chůdovitý
kořeny	kořen	k1gInPc4	kořen
<g/>
,	,	kIx,	,
kořeny	kořen	k1gInPc4	kořen
na	na	k7c6	na
skalisku	skalisko	k1gNnSc6	skalisko
<g/>
,	,	kIx,	,
plochý	plochý	k2eAgInSc4d1	plochý
kořen	kořen	k1gInSc4	kořen
<g/>
,	,	kIx,	,
pilířové	pilířový	k2eAgInPc4d1	pilířový
kořeny	kořen	k1gInPc4	kořen
<g/>
)	)	kIx)	)
a	a	k8xC	a
pod	pod	k7c4	pod
<g />
.	.	kIx.	.
</s>
<s>
<g/>
scenérie	scenérie	k1gFnPc1	scenérie
z	z	k7c2	z
kamenů	kámen	k1gInPc2	kámen
-	-	kIx~	-
suiseki	suisekit	k5eAaImRp2nS	suisekit
krajina	krajina	k1gFnSc1	krajina
na	na	k7c6	na
míse	mísa	k1gFnSc6	mísa
bez	bez	k7c2	bez
živých	živý	k2eAgFnPc2d1	živá
rostlin	rostlina	k1gFnPc2	rostlina
-	-	kIx~	-
bonkei	bonkeit	k5eAaImRp2nS	bonkeit
krajina	krajina	k1gFnSc1	krajina
na	na	k7c6	na
míse	mísa	k1gFnSc6	mísa
s	s	k7c7	s
živými	živý	k2eAgFnPc7d1	živá
rostlinami	rostlina	k1gFnPc7	rostlina
-	-	kIx~	-
saikei	saikei	k6eAd1	saikei
děrovatý	děrovatý	k2eAgInSc1d1	děrovatý
kmen	kmen	k1gInSc1	kmen
-	-	kIx~	-
uro	uro	k?	uro
rozštěpený	rozštěpený	k2eAgInSc1d1	rozštěpený
kmen	kmen	k1gInSc1	kmen
-	-	kIx~	-
sabamiki	sabamiki	k1gNnSc1	sabamiki
<g/>
,	,	kIx,	,
kobukan	kobukan	k1gInSc1	kobukan
mrtvý	mrtvý	k2eAgInSc4d1	mrtvý
kmen	kmen	k1gInSc4	kmen
-	-	kIx~	-
šarimiki	šarimiki	k6eAd1	šarimiki
mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
dřevo	dřevo	k1gNnSc1	dřevo
-	-	kIx~	-
šari	šari	k6eAd1	šari
mrtvá	mrtvý	k2eAgFnSc1d1	mrtvá
větev	větev	k1gFnSc1	větev
<g/>
,	,	kIx,	,
odumřelý	odumřelý	k2eAgInSc4d1	odumřelý
vrchol	vrchol	k1gInSc4	vrchol
-	-	kIx~	-
džin	džin	k1gInSc4	džin
hadovitý	hadovitý	k2eAgInSc4d1	hadovitý
kořen	kořen	k1gInSc4	kořen
-	-	kIx~	-
necuanadari	cuanadari	k6eNd1	cuanadari
chůdovité	chůdovitý	k2eAgInPc1d1	chůdovitý
<g/>
,	,	kIx,	,
obnažené	obnažený	k2eAgInPc1d1	obnažený
kořeny	kořen	k1gInPc1	kořen
-	-	kIx~	-
neagari	agari	k6eNd1	agari
splývající	splývající	k2eAgInPc4d1	splývající
kořeny	kořen	k1gInPc4	kořen
-	-	kIx~	-
sekidžodžu	sekidžodzat	k5eAaPmIp1nS	sekidžodzat
<g/>
,	,	kIx,	,
iwajama	iwajama	k1gFnSc1	iwajama
doplňkové	doplňkový	k2eAgFnSc2d1	doplňková
rostliny	rostlina	k1gFnSc2	rostlina
-	-	kIx~	-
šitakusa	šitakus	k1gMnSc2	šitakus
</s>
