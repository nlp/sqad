<p>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
uhličitá	uhličitý	k2eAgFnSc1d1	uhličitá
je	být	k5eAaImIp3nS	být
slabá	slabý	k2eAgFnSc1d1	slabá
<g/>
,	,	kIx,	,
nestálá	stálý	k2eNgFnSc1d1	nestálá
kyselina	kyselina	k1gFnSc1	kyselina
<g/>
,	,	kIx,	,
existující	existující	k2eAgInSc1d1	existující
jen	jen	k9	jen
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
svého	svůj	k3xOyFgNnSc2	svůj
málo	málo	k6eAd1	málo
koncentrovaného	koncentrovaný	k2eAgNnSc2d1	koncentrované
vodného	vodné	k1gNnSc2	vodné
roztoku	roztok	k1gInSc2	roztok
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
kationty	kation	k1gInPc7	kation
tvoří	tvořit	k5eAaImIp3nP	tvořit
dva	dva	k4xCgInPc1	dva
typy	typ	k1gInPc1	typ
obvykle	obvykle	k6eAd1	obvykle
stabilních	stabilní	k2eAgFnPc2d1	stabilní
solí	sůl	k1gFnPc2	sůl
–	–	k?	–
uhličitany	uhličitan	k1gInPc1	uhličitan
a	a	k8xC	a
hydrogenuhličitany	hydrogenuhličitan	k1gInPc1	hydrogenuhličitan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příprava	příprava	k1gFnSc1	příprava
==	==	k?	==
</s>
</p>
<p>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
zaváděním	zavádění	k1gNnSc7	zavádění
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
z	z	k7c2	z
malé	malý	k2eAgFnSc2d1	malá
části	část	k1gFnSc2	část
slučuje	slučovat	k5eAaImIp3nS	slučovat
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
na	na	k7c4	na
kyselinu	kyselina	k1gFnSc4	kyselina
uhličitou	uhličitý	k2eAgFnSc4d1	uhličitá
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
CO2	CO2	k4	CO2
+	+	kIx~	+
H2O	H2O	k1gFnSc1	H2O
↔	↔	k?	↔
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
CO	co	k8xS	co
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
Tato	tento	k3xDgFnSc1	tento
reakce	reakce	k1gFnSc1	reakce
je	být	k5eAaImIp3nS	být
vratná	vratný	k2eAgFnSc1d1	vratná
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
rovnováha	rovnováha	k1gFnSc1	rovnováha
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
snadno	snadno	k6eAd1	snadno
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
teplotou	teplota	k1gFnSc7	teplota
(	(	kIx(	(
<g/>
s	s	k7c7	s
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
teplotou	teplota	k1gFnSc7	teplota
klesá	klesat	k5eAaImIp3nS	klesat
tvorba	tvorba	k1gFnSc1	tvorba
kyseliny	kyselina	k1gFnSc2	kyselina
uhličité	uhličitý	k2eAgFnSc2d1	uhličitá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
parciálním	parciální	k2eAgInSc7d1	parciální
tlakem	tlak	k1gInSc7	tlak
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
nad	nad	k7c7	nad
roztokem	roztok	k1gInSc7	roztok
(	(	kIx(	(
<g/>
při	při	k7c6	při
otevření	otevření	k1gNnSc6	otevření
láhve	láhev	k1gFnSc2	láhev
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
sycenou	sycený	k2eAgFnSc7d1	sycená
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
se	se	k3xPyFc4	se
kyselina	kyselina	k1gFnSc1	kyselina
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
a	a	k8xC	a
plynný	plynný	k2eAgInSc1d1	plynný
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
v	v	k7c6	v
bublinkách	bublinka	k1gFnPc6	bublinka
uniká	unikat	k5eAaImIp3nS	unikat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc1d1	poslední
teoretické	teoretický	k2eAgInPc1d1	teoretický
výpočty	výpočet	k1gInPc1	výpočet
však	však	k9	však
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c2	za
absolutní	absolutní	k2eAgFnSc2d1	absolutní
nepřítomnosti	nepřítomnost	k1gFnSc2	nepřítomnost
molekul	molekula	k1gFnPc2	molekula
vody	voda	k1gFnSc2	voda
jsou	být	k5eAaImIp3nP	být
samostatné	samostatný	k2eAgFnPc1d1	samostatná
molekuly	molekula	k1gFnPc1	molekula
H2CO3	H2CO3	k1gFnSc2	H2CO3
velmi	velmi	k6eAd1	velmi
stabilní	stabilní	k2eAgFnPc1d1	stabilní
<g/>
,	,	kIx,	,
s	s	k7c7	s
poločasem	poločas	k1gInSc7	poločas
rozpadu	rozpad	k1gInSc2	rozpad
přibližně	přibližně	k6eAd1	přibližně
180	[number]	k4	180
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
je	být	k5eAaImIp3nS	být
kyselina	kyselina	k1gFnSc1	kyselina
uhličitá	uhličitý	k2eAgFnSc1d1	uhličitá
stálou	stálý	k2eAgFnSc7d1	stálá
složkou	složka	k1gFnSc7	složka
dešťových	dešťový	k2eAgFnPc2d1	dešťová
srážek	srážka	k1gFnPc2	srážka
a	a	k8xC	a
přírodní	přírodní	k2eAgFnSc2d1	přírodní
vody	voda	k1gFnSc2	voda
všeobecně	všeobecně	k6eAd1	všeobecně
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
vzniku	vznik	k1gInSc2	vznik
přechodné	přechodný	k2eAgFnSc2d1	přechodná
tvrdosti	tvrdost	k1gFnSc2	tvrdost
vody	voda	k1gFnSc2	voda
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
uhličitanové	uhličitanový	k2eAgInPc4d1	uhličitanový
minerály	minerál	k1gInPc4	minerál
obsahující	obsahující	k2eAgInSc4d1	obsahující
vápník	vápník	k1gInSc4	vápník
a	a	k8xC	a
hořčík	hořčík	k1gInSc4	hořčík
<g/>
;	;	kIx,	;
stejný	stejný	k2eAgInSc4d1	stejný
mechanismus	mechanismus	k1gInSc4	mechanismus
je	být	k5eAaImIp3nS	být
i	i	k9	i
podstatou	podstata	k1gFnSc7	podstata
vzniku	vznik	k1gInSc2	vznik
krasových	krasový	k2eAgInPc2d1	krasový
útvarů	útvar	k1gInPc2	útvar
ve	v	k7c6	v
vápencových	vápencový	k2eAgFnPc6d1	vápencová
horninách	hornina	k1gFnPc6	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgInSc1d2	vyšší
obsah	obsah	k1gInSc1	obsah
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
kyseliny	kyselina	k1gFnPc1	kyselina
uhličité	uhličitý	k2eAgFnPc1d1	uhličitá
mají	mít	k5eAaImIp3nP	mít
některé	některý	k3yIgFnPc1	některý
minerální	minerální	k2eAgFnPc1d1	minerální
vody	voda	k1gFnPc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
živočišných	živočišný	k2eAgInPc6d1	živočišný
organismech	organismus	k1gInPc6	organismus
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgInSc7d1	významný
meziproduktem	meziprodukt	k1gInSc7	meziprodukt
v	v	k7c6	v
procesech	proces	k1gInPc6	proces
výměny	výměna	k1gFnSc2	výměna
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
mezi	mezi	k7c7	mezi
buňkami	buňka	k1gFnPc7	buňka
a	a	k8xC	a
krví	krev	k1gFnSc7	krev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
uhličitá	uhličitý	k2eAgFnSc1d1	uhličitá
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
slabá	slabý	k2eAgFnSc1d1	slabá
dvojsytná	dvojsytný	k2eAgFnSc1d1	dvojsytný
kyselina	kyselina	k1gFnSc1	kyselina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
jen	jen	k6eAd1	jen
částečně	částečně	k6eAd1	částečně
disociuje	disociovat	k5eAaBmIp3nS	disociovat
do	do	k7c2	do
prvního	první	k4xOgMnSc2	první
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
méně	málo	k6eAd2	málo
do	do	k7c2	do
druhého	druhý	k4xOgInSc2	druhý
stupně	stupeň	k1gInSc2	stupeň
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
H2CO3	H2CO3	k4	H2CO3
↔	↔	k?	↔
H	H	kA	H
<g/>
+	+	kIx~	+
+	+	kIx~	+
HCO	HCO	kA	HCO
<g/>
3	[number]	k4	3
<g/>
–	–	k?	–
↔	↔	k?	↔
2	[number]	k4	2
<g/>
H	H	kA	H
<g/>
+	+	kIx~	+
+	+	kIx~	+
CO	co	k6eAd1	co
<g/>
32	[number]	k4	32
<g/>
-Tvoří	-Tvořit	k5eAaPmIp3nS	-Tvořit
dva	dva	k4xCgInPc4	dva
typy	typ	k1gInPc4	typ
solí	solit	k5eAaImIp3nP	solit
uhličitany	uhličitan	k1gInPc4	uhličitan
a	a	k8xC	a
hydrogenuhličitany	hydrogenuhličitan	k1gInPc4	hydrogenuhličitan
(	(	kIx(	(
<g/>
starší	starý	k2eAgInSc4d2	starší
název	název	k1gInSc4	název
kyselé	kyselý	k2eAgInPc4d1	kyselý
uhličitany	uhličitan	k1gInPc4	uhličitan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stálé	stálý	k2eAgInPc1d1	stálý
hydrogenuhličitany	hydrogenuhličitan	k1gInPc1	hydrogenuhličitan
tvoří	tvořit	k5eAaImIp3nP	tvořit
pouze	pouze	k6eAd1	pouze
alkalické	alkalický	k2eAgInPc1d1	alkalický
kovy	kov	k1gInPc1	kov
a	a	k8xC	a
amoniak	amoniak	k1gInSc1	amoniak
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Hydrogenuhličitany	Hydrogenuhličitan	k1gInPc1	Hydrogenuhličitan
kovů	kov	k1gInPc2	kov
alkalických	alkalický	k2eAgFnPc2d1	alkalická
zemin	zemina	k1gFnPc2	zemina
existují	existovat	k5eAaImIp3nP	existovat
jen	jen	k9	jen
ve	v	k7c6	v
vodných	vodný	k2eAgInPc6d1	vodný
roztocích	roztok	k1gInPc6	roztok
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
podstatou	podstata	k1gFnSc7	podstata
přechodné	přechodný	k2eAgFnSc2d1	přechodná
tvrdosti	tvrdost	k1gFnSc2	tvrdost
vody	voda	k1gFnSc2	voda
<g/>
;	;	kIx,	;
zahřátím	zahřátí	k1gNnSc7	zahřátí
roztoku	roztok	k1gInSc2	roztok
k	k	k7c3	k
bodu	bod	k1gInSc3	bod
varu	var	k1gInSc2	var
se	se	k3xPyFc4	se
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Ca	ca	kA	ca
<g/>
(	(	kIx(	(
<g/>
HCO	HCO	kA	HCO
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
→	→	k?	→
CaCO	CaCO	k1gMnSc1	CaCO
<g/>
3	[number]	k4	3
+	+	kIx~	+
H2O	H2O	k1gFnSc2	H2O
+	+	kIx~	+
CO	co	k6eAd1	co
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
uhličitany	uhličitan	k1gInPc1	uhličitan
jsou	být	k5eAaImIp3nP	být
iontové	iontový	k2eAgFnPc1d1	iontová
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgInPc4d1	významný
uhličitany	uhličitan	k1gInPc4	uhličitan
a	a	k8xC	a
hydrogenuhličitany	hydrogenuhličitan	k1gInPc4	hydrogenuhličitan
patří	patřit	k5eAaImIp3nP	patřit
zejména	zejména	k9	zejména
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
uhličitan	uhličitan	k1gInSc1	uhličitan
sodný	sodný	k2eAgInSc1d1	sodný
(	(	kIx(	(
<g/>
bezvodá	bezvodý	k2eAgFnSc1d1	bezvodá
soda	soda	k1gFnSc1	soda
<g/>
,	,	kIx,	,
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
CO	co	k9	co
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
uhličitan	uhličitan	k1gInSc1	uhličitan
sodný	sodný	k2eAgInSc1d1	sodný
<g/>
,	,	kIx,	,
dekahydrát	dekahydrát	k1gInSc1	dekahydrát
(	(	kIx(	(
<g/>
soda	soda	k1gFnSc1	soda
<g/>
,	,	kIx,	,
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
CO	co	k9	co
<g/>
3	[number]	k4	3
·	·	k?	·
10	[number]	k4	10
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
hydrogenuhličitan	hydrogenuhličitan	k1gInSc1	hydrogenuhličitan
sodný	sodný	k2eAgInSc1d1	sodný
(	(	kIx(	(
<g/>
jedlá	jedlý	k2eAgFnSc1d1	jedlá
soda	soda	k1gFnSc1	soda
<g/>
,	,	kIx,	,
NaHCO	NaHCO	k1gFnSc1	NaHCO
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
uhličitan	uhličitan	k1gInSc1	uhličitan
draselný	draselný	k2eAgInSc1d1	draselný
(	(	kIx(	(
<g/>
potaš	potaš	k1gInSc1	potaš
<g/>
,	,	kIx,	,
K	k	k7c3	k
<g/>
2	[number]	k4	2
<g/>
CO	co	k8xS	co
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
uhličitan	uhličitan	k1gInSc1	uhličitan
amonný	amonný	k2eAgInSc1d1	amonný
(	(	kIx(	(
<g/>
cukrářské	cukrářský	k2eAgFnPc1d1	cukrářská
kvasnice	kvasnice	k1gFnPc1	kvasnice
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
NH	NH	kA	NH
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
CO	co	k8xS	co
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
hydrogenuhličitan	hydrogenuhličitan	k1gInSc1	hydrogenuhličitan
amonný	amonný	k2eAgInSc1d1	amonný
(	(	kIx(	(
<g/>
NH	NH	kA	NH
<g/>
4	[number]	k4	4
<g/>
HCO	HCO	kA	HCO
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Některé	některý	k3yIgInPc1	některý
uhličitany	uhličitan	k1gInPc1	uhličitan
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
známé	známý	k2eAgInPc4d1	známý
minerály	minerál	k1gInPc4	minerál
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
uhličitan	uhličitan	k1gInSc1	uhličitan
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
(	(	kIx(	(
<g/>
kalcit	kalcit	k1gInSc1	kalcit
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
hornina	hornina	k1gFnSc1	hornina
vápenec	vápenec	k1gInSc1	vápenec
a	a	k8xC	a
mramor	mramor	k1gInSc1	mramor
<g/>
,	,	kIx,	,
CaCO	CaCO	k1gFnSc1	CaCO
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
uhličitan	uhličitan	k1gInSc1	uhličitan
hořečnatý	hořečnatý	k2eAgInSc1d1	hořečnatý
(	(	kIx(	(
<g/>
magnezit	magnezit	k1gInSc1	magnezit
<g/>
,	,	kIx,	,
MgCO	MgCO	k1gFnSc1	MgCO
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
uhličitan	uhličitan	k1gInSc1	uhličitan
hořečnatovápenatý	hořečnatovápenatý	k2eAgInSc1d1	hořečnatovápenatý
(	(	kIx(	(
<g/>
dolomit	dolomit	k1gInSc1	dolomit
<g/>
,	,	kIx,	,
CaMg	CaMg	k1gInSc1	CaMg
<g/>
(	(	kIx(	(
<g/>
CO	co	k9	co
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
uhličitan	uhličitan	k1gInSc1	uhličitan
zinečnatý	zinečnatý	k2eAgInSc1d1	zinečnatý
(	(	kIx(	(
<g/>
smithsonit	smithsonit	k1gInSc1	smithsonit
<g/>
,	,	kIx,	,
ZnCO	ZnCO	k1gFnSc1	ZnCO
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
uhličitan-dihydroxid	uhličitanihydroxid	k1gInSc1	uhličitan-dihydroxid
diměďnatý	diměďnatý	k2eAgInSc1d1	diměďnatý
(	(	kIx(	(
<g/>
malachit	malachit	k1gInSc1	malachit
<g/>
,	,	kIx,	,
CuCO	CuCO	k1gFnSc1	CuCO
<g/>
3	[number]	k4	3
·	·	k?	·
Cu	Cu	k1gMnSc1	Cu
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
diuhličitan-dihydroxid	diuhličitanihydroxid	k1gInSc1	diuhličitan-dihydroxid
triměďnatý	triměďnatý	k2eAgInSc1d1	triměďnatý
(	(	kIx(	(
<g/>
azurit	azurit	k1gInSc1	azurit
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
CuCO	CuCO	k1gFnSc1	CuCO
<g/>
3	[number]	k4	3
·	·	k?	·
Cu	Cu	k1gMnSc1	Cu
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
uhličitan	uhličitan	k1gInSc1	uhličitan
železnatý	železnatý	k2eAgInSc1d1	železnatý
(	(	kIx(	(
<g/>
siderit	siderit	k1gInSc1	siderit
čili	čili	k8xC	čili
ocelek	ocelek	k1gInSc1	ocelek
<g/>
,	,	kIx,	,
FeCO	FeCO	k1gFnSc1	FeCO
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Roztok	roztok	k1gInSc1	roztok
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
a	a	k8xC	a
kyseliny	kyselina	k1gFnSc2	kyselina
uhličité	uhličitý	k2eAgFnSc2d1	uhličitá
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
pod	pod	k7c4	pod
názvy	název	k1gInPc4	název
sodová	sodový	k2eAgFnSc1d1	sodová
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
sodovka	sodovka	k1gFnSc1	sodovka
nebo	nebo	k8xC	nebo
sifon	sifon	k1gInSc1	sifon
podstatou	podstata	k1gFnSc7	podstata
součástí	součást	k1gFnPc2	součást
perlivých	perlivý	k2eAgInPc2d1	perlivý
nápojů	nápoj	k1gInPc2	nápoj
v	v	k7c6	v
potravinářství	potravinářství	k1gNnSc6	potravinářství
<g/>
.	.	kIx.	.
</s>
<s>
Podzemní	podzemní	k2eAgFnSc1d1	podzemní
voda	voda	k1gFnSc1	voda
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
nejméně	málo	k6eAd3	málo
1	[number]	k4	1
g	g	kA	g
rozpuštěného	rozpuštěný	k2eAgInSc2d1	rozpuštěný
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
v	v	k7c4	v
1	[number]	k4	1
litru	litr	k1gInSc2	litr
je	být	k5eAaImIp3nS	být
uhličitá	uhličitý	k2eAgFnSc1d1	uhličitá
minerální	minerální	k2eAgFnSc1d1	minerální
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
kyselka	kyselka	k1gFnSc1	kyselka
<g/>
.	.	kIx.	.
</s>
</p>
