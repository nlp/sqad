<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Limy	Lima	k1gFnSc2	Lima
<g/>
,	,	kIx,	,
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Peru	Peru	k1gNnSc2	Peru
<g/>
,	,	kIx,	,
započaly	započnout	k5eAaPmAgFnP	započnout
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1535	[number]	k4	1535
založením	založení	k1gNnSc7	založení
města	město	k1gNnSc2	město
španělským	španělský	k2eAgMnSc7d1	španělský
conquistadorem	conquistador	k1gMnSc7	conquistador
Franciscem	Francisce	k1gMnSc7	Francisce
Pizarrem	Pizarr	k1gMnSc7	Pizarr
<g/>
.	.	kIx.	.
</s>
