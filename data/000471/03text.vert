<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Limy	Lima	k1gFnSc2	Lima
<g/>
,	,	kIx,	,
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Peru	Peru	k1gNnSc2	Peru
<g/>
,	,	kIx,	,
započaly	započnout	k5eAaPmAgFnP	započnout
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1535	[number]	k4	1535
založením	založení	k1gNnSc7	založení
města	město	k1gNnSc2	město
španělským	španělský	k2eAgMnSc7d1	španělský
conquistadorem	conquistador	k1gMnSc7	conquistador
Franciscem	Francisce	k1gMnSc7	Francisce
Pizarrem	Pizarr	k1gMnSc7	Pizarr
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
řeky	řeka	k1gFnSc2	řeka
Rímac	Rímac	k1gFnSc1	Rímac
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
osídlené	osídlený	k2eAgFnSc2d1	osídlená
Ychsmy	Ychsma	k1gFnSc2	Ychsma
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
tvořili	tvořit	k5eAaImAgMnP	tvořit
součást	součást	k1gFnSc4	součást
říše	říš	k1gFnSc2	říš
Inků	Ink	k1gMnPc2	Ink
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1543	[number]	k4	1543
se	se	k3xPyFc4	se
Lima	Lima	k1gFnSc1	Lima
stala	stát	k5eAaPmAgFnS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
místokrálovství	místokrálovství	k1gNnPc2	místokrálovství
Peru	Peru	k1gNnSc7	Peru
a	a	k8xC	a
sídlem	sídlo	k1gNnSc7	sídlo
soudu	soud	k1gInSc2	soud
Real	Real	k1gInSc4	Real
Audiencia	Audiencium	k1gNnSc2	Audiencium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
navzdory	navzdory	k6eAd1	navzdory
zemětřesením	zemětřesení	k1gNnSc7	zemětřesení
i	i	k8xC	i
hrozbě	hrozba	k1gFnSc3	hrozba
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
pirátů	pirát	k1gMnPc2	pirát
<g/>
,	,	kIx,	,
prosperovalo	prosperovat	k5eAaImAgNnS	prosperovat
jako	jako	k9	jako
centrum	centrum	k1gNnSc1	centrum
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
obchodní	obchodní	k2eAgFnSc2d1	obchodní
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
však	však	k9	však
kvůli	kvůli	k7c3	kvůli
bourbonským	bourbonský	k2eAgFnPc3d1	Bourbonská
reformám	reforma	k1gFnPc3	reforma
začalo	začít	k5eAaPmAgNnS	začít
upadat	upadat	k5eAaImF	upadat
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
Limy	Lima	k1gFnSc2	Lima
hrálo	hrát	k5eAaImAgNnS	hrát
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1821	[number]	k4	1821
<g/>
-	-	kIx~	-
<g/>
1824	[number]	k4	1824
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
v	v	k7c6	v
peruánské	peruánský	k2eAgFnSc6d1	peruánská
válce	válka	k1gFnSc6	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
;	;	kIx,	;
město	město	k1gNnSc1	město
utrpělo	utrpět	k5eAaPmAgNnS	utrpět
jak	jak	k6eAd1	jak
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
royalistických	royalistický	k2eAgFnPc2d1	royalistická
<g/>
,	,	kIx,	,
tak	tak	k9	tak
povstaleckých	povstalecký	k2eAgFnPc2d1	povstalecká
armád	armáda	k1gFnPc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
získání	získání	k1gNnSc6	získání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
se	se	k3xPyFc4	se
Lima	Lima	k1gFnSc1	Lima
stala	stát	k5eAaPmAgFnS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
nové	nový	k2eAgFnSc2d1	nová
Peruánské	peruánský	k2eAgFnSc2d1	peruánská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zažívala	zažívat	k5eAaImAgFnS	zažívat
krátké	krátký	k2eAgNnSc4d1	krátké
období	období	k1gNnSc4	období
prosperity	prosperita	k1gFnSc2	prosperita
až	až	k9	až
do	do	k7c2	do
vypuknutí	vypuknutí	k1gNnSc2	vypuknutí
druhé	druhý	k4xOgFnSc2	druhý
tichomořské	tichomořský	k2eAgFnSc2d1	tichomořská
války	válka	k1gFnSc2	válka
mezi	mezi	k7c7	mezi
Peru	Peru	k1gNnSc7	Peru
a	a	k8xC	a
Bolívií	Bolívie	k1gFnSc7	Bolívie
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
Chile	Chile	k1gNnSc6	Chile
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
druhé	druhý	k4xOgFnSc6	druhý
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1879	[number]	k4	1879
<g/>
-	-	kIx~	-
<g/>
1883	[number]	k4	1883
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
obsazeno	obsadit	k5eAaPmNgNnS	obsadit
a	a	k8xC	a
vypleněno	vyplenit	k5eAaPmNgNnS	vyplenit
chilskými	chilský	k2eAgFnPc7d1	chilská
jednotkami	jednotka	k1gFnPc7	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
Lima	Lima	k1gFnSc1	Lima
procházela	procházet	k5eAaImAgFnS	procházet
obdobím	období	k1gNnSc7	období
populačního	populační	k2eAgInSc2d1	populační
růstu	růst	k1gInSc2	růst
i	i	k8xC	i
urbanistického	urbanistický	k2eAgInSc2d1	urbanistický
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Značný	značný	k2eAgInSc1d1	značný
populační	populační	k2eAgInSc1d1	populační
nárůst	nárůst	k1gInSc1	nárůst
nastal	nastat	k5eAaPmAgInS	nastat
zejména	zejména	k9	zejména
ve	v	k7c4	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
díky	díky	k7c3	díky
přistěhovalcům	přistěhovalec	k1gMnPc3	přistěhovalec
z	z	k7c2	z
peruánských	peruánský	k2eAgFnPc2d1	peruánská
And	Anda	k1gFnPc2	Anda
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
přispělo	přispět	k5eAaPmAgNnS	přispět
i	i	k9	i
k	k	k7c3	k
velkému	velký	k2eAgNnSc3d1	velké
rozšíření	rozšíření	k1gNnSc3	rozšíření
slumových	slumův	k2eAgNnPc2d1	slumův
městeček	městečko	k1gNnPc2	městečko
(	(	kIx(	(
<g/>
známých	známý	k2eAgNnPc2d1	známé
jako	jako	k8xC	jako
pueblos	pueblos	k1gMnSc1	pueblos
jóvenes	jóvenes	k1gMnSc1	jóvenes
<g/>
)	)	kIx)	)
na	na	k7c6	na
předměstích	předměstí	k1gNnPc6	předměstí
Limy	Lima	k1gFnSc2	Lima
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
úřady	úřad	k1gInPc1	úřad
nedokázaly	dokázat	k5eNaPmAgInP	dokázat
rozšiřování	rozšiřování	k1gNnSc4	rozšiřování
města	město	k1gNnSc2	město
udržet	udržet	k5eAaPmF	udržet
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předkolumbovské	předkolumbovský	k2eAgFnSc6d1	předkolumbovská
éře	éra	k1gFnSc6	éra
byla	být	k5eAaImAgFnS	být
oblast	oblast	k1gFnSc1	oblast
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Limy	Lima	k1gFnSc2	Lima
osídlena	osídlit	k5eAaPmNgFnS	osídlit
několika	několik	k4yIc7	několik
skupinami	skupina	k1gFnPc7	skupina
andských	andský	k2eAgMnPc2d1	andský
indiánů	indián	k1gMnPc2	indián
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
ovládnutím	ovládnutí	k1gNnSc7	ovládnutí
údolí	údolí	k1gNnSc2	údolí
řek	řeka	k1gFnPc2	řeka
Rímac	Rímac	k1gInSc4	Rímac
a	a	k8xC	a
Lurín	Lurín	k1gInSc4	Lurín
Inckou	incký	k2eAgFnSc7d1	incká
říší	říš	k1gFnSc7	říš
byli	být	k5eAaImAgMnP	být
místní	místní	k2eAgMnPc1d1	místní
Indiáni	Indián	k1gMnPc1	Indián
sdruženi	sdružen	k2eAgMnPc1d1	sdružen
v	v	k7c6	v
Ychsmackém	Ychsmacký	k2eAgInSc6d1	Ychsmacký
svazu	svaz	k1gInSc6	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
přítomnost	přítomnost	k1gFnSc1	přítomnost
zanechala	zanechat	k5eAaPmAgFnS	zanechat
na	na	k7c6	na
oblasti	oblast	k1gFnSc6	oblast
stopy	stopa	k1gFnSc2	stopa
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
40	[number]	k4	40
pyramid	pyramida	k1gFnPc2	pyramida
a	a	k8xC	a
zavlažovacího	zavlažovací	k2eAgInSc2d1	zavlažovací
systému	systém	k1gInSc2	systém
v	v	k7c6	v
údolích	údolí	k1gNnPc6	údolí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
Inkové	Ink	k1gMnPc1	Ink
oblast	oblast	k1gFnSc4	oblast
dobyli	dobýt	k5eAaPmAgMnP	dobýt
a	a	k8xC	a
na	na	k7c6	na
místech	místo	k1gNnPc6	místo
jako	jako	k8xC	jako
Pachacamác	Pachacamác	k1gInSc4	Pachacamác
vztyčili	vztyčit	k5eAaPmAgMnP	vztyčit
své	svůj	k3xOyFgFnPc4	svůj
vlastní	vlastní	k2eAgFnPc4d1	vlastní
veřejné	veřejný	k2eAgFnPc4d1	veřejná
budovy	budova	k1gFnPc4	budova
<g/>
.	.	kIx.	.
</s>
<s>
Pachamac	Pachamac	k1gFnSc1	Pachamac
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc1d1	ležící
asi	asi	k9	asi
30	[number]	k4	30
kilometrů	kilometr	k1gInPc2	kilometr
na	na	k7c4	na
jihovýchod	jihovýchod	k1gInSc4	jihovýchod
od	od	k7c2	od
Limy	Lima	k1gFnSc2	Lima
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
největším	veliký	k2eAgNnSc7d3	veliký
mocenským	mocenský	k2eAgNnSc7d1	mocenské
centrem	centrum	k1gNnSc7	centrum
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
a	a	k8xC	a
svůj	svůj	k3xOyFgInSc4	svůj
vliv	vliv	k1gInSc4	vliv
si	se	k3xPyFc3	se
držel	držet	k5eAaImAgInS	držet
až	až	k9	až
do	do	k7c2	do
španělské	španělský	k2eAgFnSc2d1	španělská
conquisty	conquista	k1gFnSc2	conquista
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1532	[number]	k4	1532
porazila	porazit	k5eAaPmAgFnS	porazit
skupina	skupina	k1gFnSc1	skupina
španělských	španělský	k2eAgMnPc2d1	španělský
conquistadorů	conquistador	k1gMnPc2	conquistador
<g/>
,	,	kIx,	,
vedených	vedený	k2eAgFnPc2d1	vedená
Franciscem	Francisec	k1gInSc7	Francisec
Pizarrem	Pizarr	k1gMnSc7	Pizarr
<g/>
,	,	kIx,	,
posledního	poslední	k2eAgMnSc2d1	poslední
inckého	incký	k2eAgMnSc2d1	incký
císaře	císař	k1gMnSc2	císař
Atahualpu	Atahualp	k1gInSc2	Atahualp
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
převzala	převzít	k5eAaPmAgFnS	převzít
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
říší	říš	k1gFnSc7	říš
Inků	Ink	k1gMnPc2	Ink
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
byl	být	k5eAaImAgMnS	být
Pizarro	Pizarro	k1gNnSc4	Pizarro
jmenován	jmenován	k2eAgMnSc1d1	jmenován
guvernérem	guvernér	k1gMnSc7	guvernér
všech	všecek	k3xTgNnPc2	všecek
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
dobyl	dobýt	k5eAaPmAgMnS	dobýt
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
španělské	španělský	k2eAgFnSc2d1	španělská
koruny	koruna	k1gFnSc2	koruna
<g/>
,	,	kIx,	,
hledal	hledat	k5eAaImAgInS	hledat
vhodné	vhodný	k2eAgNnSc4d1	vhodné
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
založení	založení	k1gNnSc4	založení
svého	svůj	k3xOyFgNnSc2	svůj
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
volba	volba	k1gFnSc1	volba
padla	padnout	k5eAaImAgFnS	padnout
na	na	k7c4	na
město	město	k1gNnSc4	město
Jauja	Jaujum	k1gNnSc2	Jaujum
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
hluboko	hluboko	k6eAd1	hluboko
v	v	k7c6	v
Andách	Anda	k1gFnPc6	Anda
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
poloha	poloha	k1gFnSc1	poloha
byla	být	k5eAaImAgFnS	být
nakonec	nakonec	k6eAd1	nakonec
shledána	shledat	k5eAaPmNgFnS	shledat
nevhodnou	vhodný	k2eNgFnSc7d1	nevhodná
pro	pro	k7c4	pro
velkou	velký	k2eAgFnSc4d1	velká
nadmořskou	nadmořský	k2eAgFnSc4d1	nadmořská
výšku	výška	k1gFnSc4	výška
a	a	k8xC	a
velkou	velký	k2eAgFnSc4d1	velká
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
od	od	k7c2	od
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Španělští	španělský	k2eAgMnPc1d1	španělský
průzkumníci	průzkumník	k1gMnPc1	průzkumník
později	pozdě	k6eAd2	pozdě
podali	podat	k5eAaPmAgMnP	podat
zprávy	zpráva	k1gFnSc2	zpráva
o	o	k7c6	o
údolí	údolí	k1gNnSc6	údolí
Rímac	Rímac	k1gFnSc1	Rímac
<g/>
,	,	kIx,	,
ležícím	ležící	k2eAgNnSc6d1	ležící
poblíž	poblíž	k6eAd1	poblíž
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
množství	množství	k1gNnSc1	množství
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
zásoby	zásoba	k1gFnPc4	zásoba
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
rozsáhlá	rozsáhlý	k2eAgNnPc4d1	rozsáhlé
pole	pole	k1gNnPc4	pole
a	a	k8xC	a
dobré	dobrý	k2eAgNnSc4d1	dobré
podnebí	podnebí	k1gNnSc4	podnebí
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
Francisco	Francisco	k6eAd1	Francisco
Pizarro	Pizarro	k1gNnSc4	Pizarro
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1535	[number]	k4	1535
založil	založit	k5eAaPmAgMnS	založit
své	svůj	k3xOyFgNnSc4	svůj
nové	nový	k2eAgNnSc4d1	nové
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
Ciudad	Ciudad	k1gInSc1	Ciudad
de	de	k?	de
los	los	k1gInSc1	los
Reyes	Reyes	k1gInSc1	Reyes
-	-	kIx~	-
Město	město	k1gNnSc1	město
králů	král	k1gMnPc2	král
<g/>
;	;	kIx,	;
jméno	jméno	k1gNnSc1	jméno
Lima	limo	k1gNnSc2	limo
je	být	k5eAaImIp3nS	být
poté	poté	k6eAd1	poté
zkomolenina	zkomolenina	k1gFnSc1	zkomolenina
názvu	název	k1gInSc2	název
Rímac	Rímac	k1gFnSc1	Rímac
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1536	[number]	k4	1536
Limu	Lima	k1gFnSc4	Lima
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
i	i	k9	i
staré	starý	k2eAgNnSc4d1	staré
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Inků	Ink	k1gMnPc2	Ink
Cuzco	Cuzco	k1gMnSc1	Cuzco
<g/>
,	,	kIx,	,
oblehla	oblehnout	k5eAaPmAgFnS	oblehnout
armáda	armáda	k1gFnSc1	armáda
Manky	Manka	k1gFnSc2	Manka
Cápaca	Cápac	k2eAgFnSc1d1	Cápac
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
vůdce	vůdce	k1gMnSc1	vůdce
inckých	incký	k2eAgMnPc2d1	incký
povstalců	povstalec	k1gMnPc2	povstalec
proti	proti	k7c3	proti
španělské	španělský	k2eAgFnSc3d1	španělská
koloniální	koloniální	k2eAgFnSc3d1	koloniální
moci	moc	k1gFnSc3	moc
<g/>
.	.	kIx.	.
</s>
<s>
Francisco	Francisco	k6eAd1	Francisco
Pizarro	Pizarro	k1gNnSc1	Pizarro
v	v	k7c6	v
Limě	Lima	k1gFnSc6	Lima
disponoval	disponovat	k5eAaBmAgInS	disponovat
jen	jen	k9	jen
stem	sto	k4xCgNnSc7	sto
španělských	španělský	k2eAgMnPc2d1	španělský
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
větším	veliký	k2eAgInSc7d2	veliký
oddílem	oddíl	k1gInSc7	oddíl
Indiánů	Indián	k1gMnPc2	Indián
<g/>
.	.	kIx.	.
</s>
<s>
Obléhání	obléhání	k1gNnSc1	obléhání
trvalo	trvat	k5eAaImAgNnS	trvat
šest	šest	k4xCc4	šest
týdnů	týden	k1gInPc2	týden
a	a	k8xC	a
Španělům	Španěl	k1gMnPc3	Španěl
se	se	k3xPyFc4	se
povstalce	povstalec	k1gMnPc4	povstalec
po	po	k7c6	po
tvrdých	tvrdý	k2eAgInPc6d1	tvrdý
bojích	boj	k1gInPc6	boj
v	v	k7c6	v
městských	městský	k2eAgFnPc6d1	městská
ulicích	ulice	k1gFnPc6	ulice
i	i	k8xC	i
okolí	okolí	k1gNnSc1	okolí
podařilo	podařit	k5eAaPmAgNnS	podařit
porazit	porazit	k5eAaPmF	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Španělé	Španěl	k1gMnPc1	Španěl
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
vítězství	vítězství	k1gNnSc4	vítězství
i	i	k8xC	i
v	v	k7c6	v
Cuzku	Cuzko	k1gNnSc6	Cuzko
<g/>
;	;	kIx,	;
na	na	k7c6	na
obou	dva	k4xCgNnPc6	dva
vítězstvích	vítězství	k1gNnPc6	vítězství
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
podíleli	podílet	k5eAaImAgMnP	podílet
jejich	jejich	k3xOp3gMnPc1	jejich
indiánští	indiánský	k2eAgMnPc1d1	indiánský
spojenci	spojenec	k1gMnPc1	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1536	[number]	k4	1536
pak	pak	k6eAd1	pak
španělská	španělský	k2eAgFnSc1d1	španělská
koruna	koruna	k1gFnSc1	koruna
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
založení	založení	k1gNnSc4	založení
města	město	k1gNnSc2	město
a	a	k8xC	a
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1537	[number]	k4	1537
udělil	udělit	k5eAaPmAgMnS	udělit
císař	císař	k1gMnSc1	císař
Karel	Karel	k1gMnSc1	Karel
V.	V.	kA	V.
Limě	Lima	k1gFnSc6	Lima
městský	městský	k2eAgInSc1d1	městský
znak	znak	k1gInSc1	znak
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dalších	další	k2eAgNnPc2d1	další
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
zažívala	zažívat	k5eAaImAgFnS	zažívat
Lima	Lima	k1gFnSc1	Lima
bouře	bouře	k1gFnSc1	bouře
způsobené	způsobený	k2eAgFnPc4d1	způsobená
rozbroji	rozbroj	k1gInSc3	rozbroj
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
frakcemi	frakce	k1gFnPc7	frakce
samotných	samotný	k2eAgMnPc2d1	samotný
Španělů	Španěl	k1gMnPc2	Španěl
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
přívrženci	přívrženec	k1gMnPc7	přívrženec
znepřáteleného	znepřátelený	k2eAgNnSc2d1	znepřátelené
Francisca	Franciscum	k1gNnSc2	Franciscum
Pizarra	Pizarr	k1gMnSc2	Pizarr
a	a	k8xC	a
Diega	Dieg	k1gMnSc2	Dieg
Almagra	Almagr	k1gMnSc2	Almagr
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1538	[number]	k4	1538
byl	být	k5eAaImAgMnS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
Almagro	Almagro	k1gNnSc1	Almagro
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1541	[number]	k4	1541
stoupenci	stoupenec	k1gMnPc1	stoupenec
jeho	jeho	k3xOp3gMnSc2	jeho
syna	syn	k1gMnSc2	syn
Diega	Dieg	k1gMnSc2	Dieg
v	v	k7c6	v
Limě	Lima	k1gFnSc6	Lima
zavraždili	zavraždit	k5eAaPmAgMnP	zavraždit
i	i	k9	i
Francisca	Francisca	k1gMnSc1	Francisca
Pizarra	Pizarra	k1gMnSc1	Pizarra
a	a	k8xC	a
Diego	Diego	k1gMnSc1	Diego
Almagro	Almagro	k1gNnSc1	Almagro
mladší	mladý	k2eAgMnSc1d2	mladší
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
brzy	brzy	k6eAd1	brzy
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
dorazil	dorazit	k5eAaPmAgMnS	dorazit
zmocněnec	zmocněnec	k1gMnSc1	zmocněnec
španělského	španělský	k2eAgMnSc2d1	španělský
krále	král	k1gMnSc2	král
Cristóbal	Cristóbal	k1gInSc4	Cristóbal
vaca	vacum	k1gNnSc2	vacum
de	de	k?	de
Castro	Castro	k1gNnSc1	Castro
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
zmocnil	zmocnit	k5eAaPmAgMnS	zmocnit
Limy	Lima	k1gFnSc2	Lima
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1543	[number]	k4	1543
porazil	porazit	k5eAaPmAgMnS	porazit
Almagra	Almagra	k1gMnSc1	Almagra
i	i	k8xC	i
jeho	jeho	k3xOp3gMnSc2	jeho
přívržence	přívrženec	k1gMnSc2	přívrženec
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
skončila	skončit	k5eAaPmAgFnS	skončit
nejkrvavější	krvavý	k2eAgFnSc1d3	nejkrvavější
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Peru	Peru	k1gNnSc6	Peru
koloniálního	koloniální	k2eAgNnSc2d1	koloniální
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
samé	samý	k3xTgFnSc6	samý
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1543	[number]	k4	1543
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
získalo	získat	k5eAaPmAgNnS	získat
na	na	k7c6	na
prestiži	prestiž	k1gFnSc6	prestiž
díky	díky	k7c3	díky
ustanovení	ustanovení	k1gNnSc4	ustanovení
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
místokrálovství	místokrálovství	k1gNnSc2	místokrálovství
Peru	prát	k5eAaImIp1nS	prát
a	a	k8xC	a
sídelního	sídelní	k2eAgNnSc2d1	sídelní
města	město	k1gNnSc2	město
soudu	soud	k1gInSc2	soud
Real	Real	k1gInSc1	Real
Audiencia	Audiencius	k1gMnSc2	Audiencius
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
společně	společně	k6eAd1	společně
s	s	k7c7	s
místokrálem	místokrál	k1gMnSc7	místokrál
měl	mít	k5eAaImAgMnS	mít
prosazovat	prosazovat	k5eAaImF	prosazovat
politiku	politika	k1gFnSc4	politika
španělského	španělský	k2eAgMnSc2d1	španělský
krále	král	k1gMnSc2	král
vůči	vůči	k7c3	vůči
conquistadorům	conquistador	k1gMnPc3	conquistador
v	v	k7c6	v
Peru	Peru	k1gNnSc6	Peru
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1551	[number]	k4	1551
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Limě	Lima	k1gFnSc6	Lima
založena	založit	k5eAaPmNgFnS	založit
první	první	k4xOgFnSc1	první
univerzita	univerzita	k1gFnSc1	univerzita
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1584	[number]	k4	1584
první	první	k4xOgFnPc1	první
knihtiskárna	knihtiskárna	k1gFnSc1	knihtiskárna
<g/>
.	.	kIx.	.
</s>
<s>
Lima	Lima	k1gFnSc1	Lima
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
důležitým	důležitý	k2eAgNnSc7d1	důležité
střediskem	středisko	k1gNnSc7	středisko
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
v	v	k7c6	v
Peru	Peru	k1gNnSc6	Peru
<g/>
;	;	kIx,	;
roku	rok	k1gInSc2	rok
1541	[number]	k4	1541
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
diecéze	diecéze	k1gFnSc1	diecéze
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
o	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
povýšena	povýšit	k5eAaPmNgFnS	povýšit
na	na	k7c6	na
arcidiecézi	arcidiecéze	k1gFnSc6	arcidiecéze
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k6eAd1	rovněž
byla	být	k5eAaImAgFnS	být
sídlem	sídlo	k1gNnSc7	sídlo
inkvizice	inkvizice	k1gFnSc2	inkvizice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
španělských	španělský	k2eAgFnPc2d1	španělská
kolonií	kolonie	k1gFnPc2	kolonie
v	v	k7c6	v
Západní	západní	k2eAgFnSc6d1	západní
Indii	Indie	k1gFnSc6	Indie
se	se	k3xPyFc4	se
državám	država	k1gFnPc3	država
na	na	k7c6	na
tichomořské	tichomořský	k2eAgFnSc6d1	tichomořská
straně	strana	k1gFnSc6	strana
amerického	americký	k2eAgNnSc2d1	americké
pobřeží	pobřeží	k1gNnSc2	pobřeží
vyhýbaly	vyhýbat	k5eAaImAgInP	vyhýbat
nájezdy	nájezd	k1gInPc1	nájezd
anglických	anglický	k2eAgMnPc2d1	anglický
a	a	k8xC	a
francouzských	francouzský	k2eAgMnPc2d1	francouzský
pirátů	pirát	k1gMnPc2	pirát
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Francouzi	Francouz	k1gMnPc1	Francouz
ani	ani	k8xC	ani
Angličané	Angličan	k1gMnPc1	Angličan
nebyli	být	k5eNaImAgMnP	být
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
tak	tak	k6eAd1	tak
zdatní	zdatný	k2eAgMnPc1d1	zdatný
a	a	k8xC	a
zkušení	zkušený	k2eAgMnPc1d1	zkušený
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Španělé	Španěl	k1gMnPc1	Španěl
a	a	k8xC	a
Portugalci	Portugalec	k1gMnPc1	Portugalec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
mohli	moct	k5eAaImAgMnP	moct
dovolit	dovolit	k5eAaPmF	dovolit
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Pacifiku	Pacifik	k1gInSc2	Pacifik
Magalhã	Magalhã	k1gFnSc2	Magalhã
průlivem	průliv	k1gInSc7	průliv
<g/>
.	.	kIx.	.
</s>
<s>
Španělské	španělský	k2eAgNnSc1d1	španělské
pacifické	pacifický	k2eAgNnSc1d1	pacifické
loďstvo	loďstvo	k1gNnSc1	loďstvo
tak	tak	k6eAd1	tak
obvykle	obvykle	k6eAd1	obvykle
nepotřebovalo	potřebovat	k5eNaImAgNnS	potřebovat
žádnou	žádný	k3yNgFnSc4	žádný
ochranu	ochrana	k1gFnSc4	ochrana
ani	ani	k8xC	ani
při	při	k7c6	při
přepravě	přeprava	k1gFnSc6	přeprava
drahocenných	drahocenný	k2eAgInPc2d1	drahocenný
nákladů	náklad	k1gInPc2	náklad
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
pirát	pirát	k1gMnSc1	pirát
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
Pacifiku	Pacifik	k1gInSc6	Pacifik
objevil	objevit	k5eAaPmAgMnS	objevit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
roku	rok	k1gInSc2	rok
1576	[number]	k4	1576
Angličan	Angličan	k1gMnSc1	Angličan
John	John	k1gMnSc1	John
Oxenham	Oxenham	k1gInSc4	Oxenham
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
přistál	přistát	k5eAaPmAgInS	přistát
na	na	k7c6	na
karibském	karibský	k2eAgNnSc6d1	Karibské
pobřeží	pobřeží	k1gNnSc6	pobřeží
Panamy	Panama	k1gFnSc2	Panama
<g/>
,	,	kIx,	,
přešel	přejít	k5eAaPmAgInS	přejít
Panamskou	panamský	k2eAgFnSc4d1	Panamská
šíji	šíje	k1gFnSc4	šíje
a	a	k8xC	a
na	na	k7c6	na
pacifickém	pacifický	k2eAgNnSc6d1	pacifické
pobřeží	pobřeží	k1gNnSc6	pobřeží
postavil	postavit	k5eAaPmAgMnS	postavit
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
Indiánů	Indián	k1gMnPc2	Indián
i	i	k8xC	i
uprchlých	uprchlý	k2eAgMnPc2d1	uprchlý
otroků	otrok	k1gMnPc2	otrok
malou	malý	k2eAgFnSc4d1	malá
loď	loď	k1gFnSc4	loď
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
ohrožoval	ohrožovat	k5eAaImAgMnS	ohrožovat
španělské	španělský	k2eAgFnSc2d1	španělská
lodě	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Španělé	Španěl	k1gMnPc1	Španěl
však	však	k9	však
jeho	jeho	k3xOp3gMnPc1	jeho
loď	loď	k1gFnSc4	loď
brzy	brzy	k6eAd1	brzy
dostihli	dostihnout	k5eAaPmAgMnP	dostihnout
<g/>
,	,	kIx,	,
zničili	zničit	k5eAaPmAgMnP	zničit
a	a	k8xC	a
Oxenhama	Oxenhama	k1gNnSc4	Oxenhama
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
inkviziční	inkviziční	k2eAgFnSc7d1	inkviziční
tribunál	tribunál	k1gInSc4	tribunál
v	v	k7c6	v
Limě	Lima	k1gFnSc6	Lima
odsoudil	odsoudit	k5eAaPmAgInS	odsoudit
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1579	[number]	k4	1579
však	však	k9	však
do	do	k7c2	do
Pacifiku	Pacifik	k1gInSc2	Pacifik
Magalhã	Magalhã	k1gMnPc2	Magalhã
průlivem	průliv	k1gInSc7	průliv
pronikl	proniknout	k5eAaPmAgInS	proniknout
Francis	Francis	k1gFnSc4	Francis
Drake	Drak	k1gMnSc2	Drak
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
lodi	loď	k1gFnSc6	loď
Pelikán	Pelikán	k1gMnSc1	Pelikán
přepadal	přepadat	k5eAaImAgMnS	přepadat
a	a	k8xC	a
drancoval	drancovat	k5eAaImAgMnS	drancovat
většinou	většina	k1gFnSc7	většina
překvapené	překvapený	k2eAgFnSc2d1	překvapená
a	a	k8xC	a
nepřipravené	připravený	k2eNgFnSc2d1	nepřipravená
španělské	španělský	k2eAgFnSc2d1	španělská
kolonie	kolonie	k1gFnSc2	kolonie
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
;	;	kIx,	;
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
Drake	Drake	k1gInSc1	Drake
vplul	vplout	k5eAaPmAgInS	vplout
do	do	k7c2	do
přístavu	přístav	k1gInSc2	přístav
v	v	k7c6	v
Limě	Lima	k1gFnSc6	Lima
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyloupil	vyloupit	k5eAaPmAgMnS	vyloupit
a	a	k8xC	a
zničil	zničit	k5eAaPmAgMnS	zničit
zakotvené	zakotvený	k2eAgFnPc4d1	zakotvená
obchodní	obchodní	k2eAgFnPc4d1	obchodní
lodě	loď	k1gFnPc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Místokrál	místokrál	k1gMnSc1	místokrál
Francisco	Francisco	k1gMnSc1	Francisco
de	de	k?	de
Toledo	Toledo	k1gNnSc4	Toledo
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
k	k	k7c3	k
akci	akce	k1gFnSc3	akce
a	a	k8xC	a
zorganizoval	zorganizovat	k5eAaPmAgMnS	zorganizovat
trestnou	trestný	k2eAgFnSc4d1	trestná
výpravu	výprava	k1gFnSc4	výprava
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
Pedra	Pedr	k1gMnSc2	Pedr
Sarmienta	Sarmient	k1gMnSc2	Sarmient
de	de	k?	de
Gamboy	Gamboa	k1gMnSc2	Gamboa
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
jal	jmout	k5eAaPmAgMnS	jmout
Drakea	Drakea	k1gMnSc1	Drakea
stíhat	stíhat	k5eAaImF	stíhat
<g/>
.	.	kIx.	.
</s>
<s>
Španělům	Španěl	k1gMnPc3	Španěl
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nepodařilo	podařit	k5eNaPmAgNnS	podařit
samotného	samotný	k2eAgMnSc4d1	samotný
Drakea	Drakeus	k1gMnSc4	Drakeus
dopadnout	dopadnout	k5eAaPmF	dopadnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zahradili	zahradit	k5eAaPmAgMnP	zahradit
cestu	cesta	k1gFnSc4	cesta
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
jih	jih	k1gInSc4	jih
a	a	k8xC	a
Angličané	Angličan	k1gMnPc1	Angličan
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
museli	muset	k5eAaImAgMnP	muset
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
vrátit	vrátit	k5eAaPmF	vrátit
přes	přes	k7c4	přes
Tichý	tichý	k2eAgInSc4d1	tichý
a	a	k8xC	a
Indický	indický	k2eAgInSc4d1	indický
oceán	oceán	k1gInSc4	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Francisco	Francisco	k1gNnSc1	Francisco
de	de	k?	de
Toledo	Toledo	k1gNnSc1	Toledo
v	v	k7c6	v
Limě	Lima	k1gFnSc6	Lima
poté	poté	k6eAd1	poté
věnoval	věnovat	k5eAaPmAgMnS	věnovat
svou	svůj	k3xOyFgFnSc4	svůj
pozornost	pozornost	k1gFnSc4	pozornost
zvýšení	zvýšení	k1gNnSc4	zvýšení
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
amerických	americký	k2eAgFnPc2d1	americká
osad	osada	k1gFnPc2	osada
<g/>
;	;	kIx,	;
dal	dát	k5eAaPmAgInS	dát
v	v	k7c6	v
přístavu	přístav	k1gInSc6	přístav
Callao	Callao	k6eAd1	Callao
postavit	postavit	k5eAaPmF	postavit
Armadu	Armada	k1gFnSc4	Armada
del	del	k?	del
Mar	Mar	k1gFnSc2	Mar
del	del	k?	del
Sur	Sur	k1gFnSc2	Sur
-	-	kIx~	-
Jižní	jižní	k2eAgNnSc1d1	jižní
loďstvo	loďstvo	k1gNnSc1	loďstvo
a	a	k8xC	a
opevnit	opevnit	k5eAaPmF	opevnit
proti	proti	k7c3	proti
pirátům	pirát	k1gMnPc3	pirát
peruánské	peruánský	k2eAgFnPc1d1	peruánská
přístavy	přístav	k1gInPc4	přístav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Limě	Lima	k1gFnSc6	Lima
kvetl	kvést	k5eAaImAgInS	kvést
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
zahraniční	zahraniční	k2eAgInSc1d1	zahraniční
obchod	obchod	k1gInSc1	obchod
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
místokrálovství	místokrálovství	k1gNnSc4	místokrálovství
Peru	Peru	k1gNnSc2	Peru
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
kontaktů	kontakt	k1gInPc2	kontakt
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
oblastmi	oblast	k1gFnPc7	oblast
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
Evropy	Evropa	k1gFnSc2	Evropa
i	i	k8xC	i
Dálného	dálný	k2eAgInSc2d1	dálný
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Obchodníci	obchodník	k1gMnPc1	obchodník
exportovali	exportovat	k5eAaBmAgMnP	exportovat
peruánské	peruánský	k2eAgNnSc4d1	peruánské
stříbro	stříbro	k1gNnSc4	stříbro
přes	přes	k7c4	přes
nedaleký	daleký	k2eNgInSc4d1	nedaleký
přístav	přístav	k1gInSc4	přístav
Callao	Callao	k6eAd1	Callao
a	a	k8xC	a
směňovali	směňovat	k5eAaImAgMnP	směňovat
ho	on	k3xPp3gMnSc4	on
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
v	v	k7c6	v
Portobelu	Portobel	k1gInSc6	Portobel
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
Panamě	Panama	k1gFnSc6	Panama
za	za	k7c4	za
dovezené	dovezený	k2eAgNnSc4d1	dovezené
zboží	zboží	k1gNnSc4	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
praxe	praxe	k1gFnSc1	praxe
však	však	k9	však
byla	být	k5eAaImAgFnS	být
vynucena	vynutit	k5eAaPmNgFnS	vynutit
zákonem	zákon	k1gInSc7	zákon
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
španělské	španělský	k2eAgFnPc1d1	španělská
úřady	úřad	k1gInPc4	úřad
vyžadovaly	vyžadovat	k5eAaImAgFnP	vyžadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
všechen	všechen	k3xTgInSc4	všechen
zámořský	zámořský	k2eAgInSc4d1	zámořský
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
místokrálovstvím	místokrálovství	k1gNnSc7	místokrálovství
Peru	Peru	k1gNnSc2	Peru
procházel	procházet	k5eAaImAgMnS	procházet
Callaem	Callaem	k1gInSc4	Callaem
<g/>
.	.	kIx.	.
</s>
<s>
Výsledná	výsledný	k2eAgFnSc1d1	výsledná
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
prosperita	prosperita	k1gFnSc1	prosperita
Limy	Lima	k1gFnSc2	Lima
se	se	k3xPyFc4	se
odrážela	odrážet	k5eAaImAgFnS	odrážet
na	na	k7c6	na
rapidním	rapidní	k2eAgInSc6d1	rapidní
růstu	růst	k1gInSc6	růst
populace	populace	k1gFnSc2	populace
<g/>
;	;	kIx,	;
roku	rok	k1gInSc2	rok
1619	[number]	k4	1619
měla	mít	k5eAaImAgFnS	mít
Lima	Lima	k1gFnSc1	Lima
asi	asi	k9	asi
25	[number]	k4	25
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1687	[number]	k4	1687
již	již	k9	již
80	[number]	k4	80
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Limě	Lima	k1gFnSc6	Lima
vládla	vládnout	k5eAaImAgFnS	vládnout
především	především	k6eAd1	především
bělošská	bělošský	k2eAgFnSc1d1	bělošská
koloniální	koloniální	k2eAgFnSc1d1	koloniální
honorace	honorace	k1gFnSc1	honorace
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgInSc3	ten
vlivní	vlivný	k2eAgMnPc1d1	vlivný
vlastníci	vlastník	k1gMnPc1	vlastník
půdy	půda	k1gFnSc2	půda
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
a	a	k8xC	a
Cuzcu	Cuzcus	k1gInSc6	Cuzcus
byli	být	k5eAaImAgMnP	být
míšenci	míšenec	k1gMnPc1	míšenec
a	a	k8xC	a
indiánské	indiánský	k2eAgFnPc1d1	indiánská
komuny	komuna	k1gFnPc1	komuna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1650	[number]	k4	1650
žilo	žít	k5eAaImAgNnS	žít
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
a	a	k8xC	a
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
také	také	k9	také
až	až	k9	až
60	[number]	k4	60
000	[number]	k4	000
černochů	černoch	k1gMnPc2	černoch
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
Limě	Lima	k1gFnSc6	Lima
se	se	k3xPyFc4	se
nevyhýbaly	vyhýbat	k5eNaImAgFnP	vyhýbat
ani	ani	k8xC	ani
pohromy	pohroma	k1gFnPc1	pohroma
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1687	[number]	k4	1687
město	město	k1gNnSc4	město
postihla	postihnout	k5eAaPmAgFnS	postihnout
mohutná	mohutný	k2eAgFnSc1d1	mohutná
zemětřesení	zemětřesení	k1gNnPc1	zemětřesení
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
poškodila	poškodit	k5eAaPmAgFnS	poškodit
většinu	většina	k1gFnSc4	většina
města	město	k1gNnSc2	město
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
okolí	okolí	k1gNnSc4	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Následné	následný	k2eAgFnPc1d1	následná
epidemie	epidemie	k1gFnPc1	epidemie
a	a	k8xC	a
nedostatek	nedostatek	k1gInSc1	nedostatek
potravin	potravina	k1gFnPc2	potravina
způsobily	způsobit	k5eAaPmAgInP	způsobit
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1692	[number]	k4	1692
pokles	pokles	k1gInSc1	pokles
populace	populace	k1gFnSc2	populace
Limy	Lima	k1gFnSc2	Lima
pod	pod	k7c7	pod
40	[number]	k4	40
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgNnSc7	druhý
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
byla	být	k5eAaImAgFnS	být
přítomnost	přítomnost	k1gFnSc1	přítomnost
pirátů	pirát	k1gMnPc2	pirát
a	a	k8xC	a
korzárů	korzár	k1gMnPc2	korzár
v	v	k7c6	v
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Holandská	holandský	k2eAgFnSc1d1	holandská
námořní	námořní	k2eAgFnSc1d1	námořní
expedice	expedice	k1gFnSc1	expedice
zaútočila	zaútočit	k5eAaPmAgFnS	zaútočit
roku	rok	k1gInSc2	rok
1624	[number]	k4	1624
na	na	k7c4	na
přístav	přístav	k1gInSc4	přístav
Callao	Callao	k6eAd1	Callao
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
místokráli	místokrál	k1gMnSc3	místokrál
Diegovi	Dieg	k1gMnSc3	Dieg
Fernándezovi	Fernández	k1gMnSc3	Fernández
de	de	k?	de
Córdobovi	Córdob	k1gMnSc3	Córdob
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
Holanďany	Holanďan	k1gMnPc4	Holanďan
odrazit	odrazit	k5eAaPmF	odrazit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
ve	v	k7c6	v
vodách	voda	k1gFnPc6	voda
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
rozmáhaly	rozmáhat	k5eAaImAgFnP	rozmáhat
aktivity	aktivita	k1gFnPc1	aktivita
anglických	anglický	k2eAgMnPc2d1	anglický
bukanýrů	bukanýr	k1gMnPc2	bukanýr
<g/>
,	,	kIx,	,
až	až	k9	až
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
obchodníci	obchodník	k1gMnPc1	obchodník
z	z	k7c2	z
Limy	Lima	k1gFnSc2	Lima
nepostavili	postavit	k5eNaPmAgMnP	postavit
roku	rok	k1gInSc2	rok
1690	[number]	k4	1690
na	na	k7c4	na
odpor	odpor	k1gInSc4	odpor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1684	[number]	k4	1684
<g/>
-	-	kIx~	-
<g/>
1687	[number]	k4	1687
nechal	nechat	k5eAaPmAgMnS	nechat
místokrál	místokrál	k1gMnSc1	místokrál
Melchor	Melchor	k1gMnSc1	Melchor
de	de	k?	de
Navarra	Navarra	k1gFnSc1	Navarra
y	y	k?	y
Rocafull	Rocafull	k1gInSc1	Rocafull
jako	jako	k8xC	jako
preventivní	preventivní	k2eAgNnSc1d1	preventivní
opatření	opatření	k1gNnSc1	opatření
vybudovat	vybudovat	k5eAaPmF	vybudovat
limské	limský	k2eAgNnSc4d1	limský
městské	městský	k2eAgNnSc4d1	Městské
opevnění	opevnění	k1gNnSc4	opevnění
<g/>
.	.	kIx.	.
</s>
<s>
Zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1687	[number]	k4	1687
znamenalo	znamenat	k5eAaImAgNnS	znamenat
konec	konec	k1gInSc4	konec
prosperity	prosperita	k1gFnSc2	prosperita
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yQnSc3	což
přispělo	přispět	k5eAaPmAgNnS	přispět
několik	několik	k4yIc4	několik
faktorů	faktor	k1gInPc2	faktor
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
úpadek	úpadek	k1gInSc1	úpadek
obchodu	obchod	k1gInSc2	obchod
i	i	k8xC	i
těžby	těžba	k1gFnSc2	těžba
stříbra	stříbro	k1gNnSc2	stříbro
a	a	k8xC	a
vysoká	vysoký	k2eAgFnSc1d1	vysoká
konkurence	konkurence	k1gFnSc1	konkurence
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
dalších	další	k2eAgNnPc2d1	další
koloniálních	koloniální	k2eAgNnPc2d1	koloniální
měst	město	k1gNnPc2	město
jako	jako	k8xC	jako
Buenos	Buenosa	k1gFnPc2	Buenosa
Aires	Airesa	k1gFnPc2	Airesa
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
těmto	tento	k3xDgInPc3	tento
problémům	problém	k1gInPc3	problém
se	s	k7c7	s
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1746	[number]	k4	1746
připojilo	připojit	k5eAaPmAgNnS	připojit
další	další	k2eAgNnSc1d1	další
velké	velký	k2eAgNnSc1d1	velké
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
město	město	k1gNnSc1	město
rozsáhle	rozsáhle	k6eAd1	rozsáhle
poškodilo	poškodit	k5eAaPmAgNnS	poškodit
a	a	k8xC	a
zničilo	zničit	k5eAaPmAgNnS	zničit
přístav	přístav	k1gInSc4	přístav
Callao	Callao	k6eAd1	Callao
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
si	se	k3xPyFc3	se
za	za	k7c2	za
místokrále	místokrál	k1gMnSc2	místokrál
José	Josá	k1gFnSc2	Josá
Antonia	Antonio	k1gMnSc2	Antonio
Mansa	Mans	k1gMnSc2	Mans
de	de	k?	de
Velasca	Velasc	k1gInSc2	Velasc
vyžádalo	vyžádat	k5eAaPmAgNnS	vyžádat
značnou	značný	k2eAgFnSc4d1	značná
přestavbu	přestavba	k1gFnSc4	přestavba
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
katastrofa	katastrofa	k1gFnSc1	katastrofa
vedla	vést	k5eAaImAgFnS	vést
rovněž	rovněž	k9	rovněž
k	k	k7c3	k
velkému	velký	k2eAgInSc3d1	velký
rozvoji	rozvoj	k1gInSc3	rozvoj
náboženského	náboženský	k2eAgInSc2d1	náboženský
života	život	k1gInSc2	život
a	a	k8xC	a
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
1746	[number]	k4	1746
se	se	k3xPyFc4	se
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
v	v	k7c6	v
průvodu	průvod	k1gInSc6	průvod
nese	nést	k5eAaImIp3nS	nést
obrázek	obrázek	k1gInSc1	obrázek
Krista	Kristus	k1gMnSc2	Kristus
známého	známý	k1gMnSc2	známý
jako	jako	k9	jako
Pán	pán	k1gMnSc1	pán
zázraků	zázrak	k1gInPc2	zázrak
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
pozdní	pozdní	k2eAgFnSc2d1	pozdní
koloniální	koloniální	k2eAgFnSc2d1	koloniální
éry	éra	k1gFnSc2	éra
<g/>
,	,	kIx,	,
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
bourbonské	bourbonský	k2eAgFnSc2d1	Bourbonská
dynastie	dynastie	k1gFnSc2	dynastie
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
Limě	Lima	k1gFnSc6	Lima
prosadily	prosadit	k5eAaPmAgFnP	prosadit
a	a	k8xC	a
ovlivňovaly	ovlivňovat	k5eAaImAgFnP	ovlivňovat
dění	dění	k1gNnSc4	dění
myšlenky	myšlenka	k1gFnSc2	myšlenka
osvícenství	osvícenství	k1gNnSc2	osvícenství
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
byly	být	k5eAaImAgFnP	být
postaveny	postaven	k2eAgFnPc1d1	postavena
mnohé	mnohý	k2eAgFnPc1d1	mnohá
nové	nový	k2eAgFnPc1d1	nová
budovy	budova	k1gFnPc1	budova
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Plaza	plaz	k1gMnSc4	plaz
de	de	k?	de
toros	toros	k1gMnSc1	toros
de	de	k?	de
Acho	Acho	k1gMnSc1	Acho
(	(	kIx(	(
<g/>
aréna	aréna	k1gFnSc1	aréna
pro	pro	k7c4	pro
býčí	býčí	k2eAgInPc4d1	býčí
zápasy	zápas	k1gInPc4	zápas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aréna	aréna	k1gFnSc1	aréna
pro	pro	k7c4	pro
kohoutí	kohoutí	k2eAgInPc4d1	kohoutí
zápasy	zápas	k1gInPc4	zápas
či	či	k8xC	či
ústřední	ústřední	k2eAgInSc1d1	ústřední
hřbitov	hřbitov	k1gInSc1	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
městské	městský	k2eAgFnPc1d1	městská
arény	aréna	k1gFnPc1	aréna
pro	pro	k7c4	pro
zápasy	zápas	k1gInPc4	zápas
zvířat	zvíře	k1gNnPc2	zvíře
byly	být	k5eAaImAgFnP	být
postaveny	postavit	k5eAaPmNgFnP	postavit
proto	proto	k6eAd1	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
pořádání	pořádání	k1gNnSc1	pořádání
zápasů	zápas	k1gInPc2	zápas
přitáhlo	přitáhnout	k5eAaPmAgNnS	přitáhnout
na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byly	být	k5eAaImAgInP	být
kohoutí	kohoutí	k2eAgInPc1d1	kohoutí
a	a	k8xC	a
býčí	býčí	k2eAgInPc1d1	býčí
zápasy	zápas	k1gInPc1	zápas
praktikovány	praktikován	k2eAgInPc1d1	praktikován
roztroušeně	roztroušeně	k6eAd1	roztroušeně
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
městě	město	k1gNnSc6	město
<g/>
;	;	kIx,	;
hřbitov	hřbitov	k1gInSc1	hřbitov
pak	pak	k6eAd1	pak
měl	mít	k5eAaImAgInS	mít
učinit	učinit	k5eAaImF	učinit
přítrž	přítrž	k1gFnSc4	přítrž
pohřbům	pohřeb	k1gInPc3	pohřeb
v	v	k7c6	v
kostelech	kostel	k1gInPc6	kostel
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
úřady	úřad	k1gInPc1	úřad
považovaly	považovat	k5eAaImAgInP	považovat
za	za	k7c4	za
nehygienické	hygienický	k2eNgNnSc4d1	nehygienické
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
Lima	Lima	k1gFnSc1	Lima
značně	značně	k6eAd1	značně
zasažena	zasáhnout	k5eAaPmNgFnS	zasáhnout
bourbonskými	bourbonský	k2eAgFnPc7d1	Bourbonská
reformami	reforma	k1gFnPc7	reforma
<g/>
,	,	kIx,	,
ztratila	ztratit	k5eAaPmAgFnS	ztratit
své	svůj	k3xOyFgNnSc4	svůj
dominantní	dominantní	k2eAgNnSc4d1	dominantní
postavení	postavení	k1gNnSc4	postavení
v	v	k7c6	v
zámořském	zámořský	k2eAgInSc6d1	zámořský
obchodu	obchod	k1gInSc6	obchod
a	a	k8xC	a
důležitá	důležitý	k2eAgFnSc1d1	důležitá
těžební	těžební	k2eAgFnSc1d1	těžební
oblast	oblast	k1gFnSc1	oblast
horního	horní	k2eAgNnSc2d1	horní
Peru	Peru	k1gNnSc2	Peru
byla	být	k5eAaImAgFnS	být
převedena	převést	k5eAaPmNgFnS	převést
pod	pod	k7c4	pod
správu	správa	k1gFnSc4	správa
místokrálovství	místokrálovství	k1gNnSc2	místokrálovství
Río	Río	k1gFnPc2	Río
de	de	k?	de
la	la	k1gNnSc1	la
Plata	plato	k1gNnSc2	plato
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
úpadek	úpadek	k1gInSc1	úpadek
tak	tak	k6eAd1	tak
učinil	učinit	k5eAaImAgInS	učinit
limské	limský	k2eAgFnPc4d1	Limská
elity	elita	k1gFnPc4	elita
závislejšími	závislý	k2eAgFnPc7d2	závislejší
na	na	k7c6	na
králi	král	k1gMnSc6	král
a	a	k8xC	a
církvi	církev	k1gFnSc6	církev
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
zdráhaly	zdráhat	k5eAaImAgFnP	zdráhat
podporovat	podporovat	k5eAaImF	podporovat
ideu	idea	k1gFnSc4	idea
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
desetiletí	desetiletí	k1gNnSc6	desetiletí
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
během	během	k7c2	během
zuřících	zuřící	k2eAgFnPc2d1	zuřící
jihoamerických	jihoamerický	k2eAgFnPc2d1	jihoamerická
válek	válka	k1gFnPc2	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Lima	Lima	k1gFnSc1	Lima
stala	stát	k5eAaPmAgFnS	stát
baštou	bašta	k1gFnSc7	bašta
royalistů	royalista	k1gMnPc2	royalista
vedených	vedený	k2eAgFnPc2d1	vedená
silným	silný	k2eAgMnSc7d1	silný
španělským	španělský	k2eAgMnSc7d1	španělský
místokrálem	místokrál	k1gMnSc7	místokrál
José	Josá	k1gFnSc2	Josá
Fernandem	Fernand	k1gInSc7	Fernand
de	de	k?	de
Abascal	Abascal	k1gFnSc1	Abascal
y	y	k?	y
Sousa	Sousa	k1gFnSc1	Sousa
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1820	[number]	k4	1820
vpadla	vpadnout	k5eAaPmAgFnS	vpadnout
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
spojená	spojený	k2eAgFnSc1d1	spojená
expedice	expedice	k1gFnSc1	expedice
chilských	chilský	k2eAgMnPc2d1	chilský
a	a	k8xC	a
argentinských	argentinský	k2eAgMnPc2d1	argentinský
povstalců	povstalec	k1gMnPc2	povstalec
vedená	vedený	k2eAgFnSc1d1	vedená
generálem	generál	k1gMnSc7	generál
José	José	k1gNnSc7	José
de	de	k?	de
San	San	k1gFnSc2	San
Martínem	Martín	k1gInSc7	Martín
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
nenapadla	napadnout	k5eNaPmAgFnS	napadnout
samotné	samotný	k2eAgNnSc4d1	samotné
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Místokrál	místokrál	k1gMnSc1	místokrál
José	Josý	k2eAgNnSc1d1	José
de	de	k?	de
la	la	k1gNnSc1	la
Serna	Sern	k1gInSc2	Sern
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
čelil	čelit	k5eAaImAgMnS	čelit
námořní	námořní	k2eAgFnSc3d1	námořní
blokádě	blokáda	k1gFnSc3	blokáda
i	i	k8xC	i
guerrillovým	guerrillův	k2eAgInPc3d1	guerrillův
útokům	útok	k1gInPc3	útok
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgMnS	být
donucen	donutit	k5eAaPmNgMnS	donutit
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1821	[number]	k4	1821
evakuovat	evakuovat	k5eAaBmF	evakuovat
Limu	Lima	k1gFnSc4	Lima
a	a	k8xC	a
zachránit	zachránit	k5eAaPmF	zachránit
tak	tak	k6eAd1	tak
svou	svůj	k3xOyFgFnSc4	svůj
royalistickou	royalistický	k2eAgFnSc4d1	royalistická
armádu	armáda	k1gFnSc4	armáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obavách	obava	k1gFnPc6	obava
před	před	k7c7	před
lidovým	lidový	k2eAgNnSc7d1	lidové
povstáním	povstání	k1gNnSc7	povstání
pozvala	pozvat	k5eAaPmAgFnS	pozvat
městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
San	San	k1gMnSc2	San
Martína	Martín	k1gMnSc2	Martín
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
žádost	žádost	k1gFnSc4	žádost
byla	být	k5eAaImAgFnS	být
podepsána	podepsán	k2eAgFnSc1d1	podepsána
deklarace	deklarace	k1gFnSc1	deklarace
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
tím	ten	k3xDgNnSc7	ten
však	však	k9	však
u	u	k7c2	u
konce	konec	k1gInSc2	konec
nebyla	být	k5eNaImAgFnS	být
a	a	k8xC	a
během	během	k7c2	během
dalších	další	k2eAgNnPc2d1	další
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
Lima	Lima	k1gFnSc1	Lima
dostala	dostat	k5eAaPmAgFnS	dostat
několikrát	několikrát	k6eAd1	několikrát
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
obou	dva	k4xCgInPc2	dva
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Ayacucha	Ayacuch	k1gMnSc2	Ayacuch
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1824	[number]	k4	1824
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
již	již	k6eAd1	již
byla	být	k5eAaImAgFnS	být
válka	válka	k1gFnSc1	válka
rozhodnuta	rozhodnout	k5eAaPmNgFnS	rozhodnout
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Lima	Lima	k1gFnSc1	Lima
značně	značně	k6eAd1	značně
poznamenána	poznamenat	k5eAaPmNgFnS	poznamenat
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
se	se	k3xPyFc4	se
Lima	Lima	k1gFnSc1	Lima
stala	stát	k5eAaPmAgFnS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
nové	nový	k2eAgFnSc2d1	nová
Peruánské	peruánský	k2eAgFnSc2d1	peruánská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
stagnace	stagnace	k1gFnSc1	stagnace
a	a	k8xC	a
politický	politický	k2eAgInSc1d1	politický
úpadek	úpadek	k1gInSc1	úpadek
zastavily	zastavit	k5eAaPmAgFnP	zastavit
její	její	k3xOp3gInSc4	její
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
stagnace	stagnace	k1gFnSc2	stagnace
skončilo	skončit	k5eAaPmAgNnS	skončit
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
v	v	k7c6	v
soukromém	soukromý	k2eAgInSc6d1	soukromý
i	i	k8xC	i
veřejném	veřejný	k2eAgInSc6d1	veřejný
sektoru	sektor	k1gInSc6	sektor
zvýšily	zvýšit	k5eAaPmAgInP	zvýšit
výnosy	výnos	k1gInPc1	výnos
z	z	k7c2	z
vývozu	vývoz	k1gInSc2	vývoz
guána	guáno	k1gNnSc2	guáno
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
rychlému	rychlý	k2eAgInSc3d1	rychlý
rozvoji	rozvoj	k1gInSc3	rozvoj
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
dvou	dva	k4xCgNnPc6	dva
desetiletích	desetiletí	k1gNnPc6	desetiletí
stát	stát	k1gInSc4	stát
zajišťoval	zajišťovat	k5eAaImAgInS	zajišťovat
výstavbu	výstavba	k1gFnSc4	výstavba
velkých	velký	k2eAgFnPc2d1	velká
veřejných	veřejný	k2eAgFnPc2d1	veřejná
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
nahradily	nahradit	k5eAaPmAgFnP	nahradit
budovy	budova	k1gFnPc1	budova
koloniální	koloniální	k2eAgFnSc2d1	koloniální
správy	správa	k1gFnSc2	správa
<g/>
;	;	kIx,	;
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
například	například	k6eAd1	například
budova	budova	k1gFnSc1	budova
centrální	centrální	k2eAgFnSc2d1	centrální
tržnice	tržnice	k1gFnSc2	tržnice
<g/>
,	,	kIx,	,
jatek	jatka	k1gFnPc2	jatka
<g/>
,	,	kIx,	,
azyl	azyl	k1gInSc4	azyl
pro	pro	k7c4	pro
duševně	duševně	k6eAd1	duševně
nemocné	nemocný	k1gMnPc4	nemocný
<g/>
,	,	kIx,	,
věznice	věznice	k1gFnSc2	věznice
či	či	k8xC	či
nemocnice	nemocnice	k1gFnSc2	nemocnice
Dos	Dos	k1gMnSc1	Dos
de	de	k?	de
Mayo	Mayo	k1gMnSc1	Mayo
<g/>
.	.	kIx.	.
</s>
<s>
Zlepšení	zlepšení	k1gNnSc1	zlepšení
se	se	k3xPyFc4	se
promítlo	promítnout	k5eAaPmAgNnS	promítnout
také	také	k9	také
ve	v	k7c6	v
výstavbě	výstavba	k1gFnSc6	výstavba
komunikací	komunikace	k1gFnPc2	komunikace
<g/>
;	;	kIx,	;
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
železnice	železnice	k1gFnSc1	železnice
mezi	mezi	k7c7	mezi
Limou	Lima	k1gFnSc7	Lima
a	a	k8xC	a
Callaem	Calla	k1gInSc7	Calla
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
železný	železný	k2eAgInSc1d1	železný
most	most	k1gInSc1	most
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
Rímac	Rímac	k1gFnSc4	Rímac
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1872	[number]	k4	1872
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
očekávání	očekávání	k1gNnSc6	očekávání
dalšího	další	k2eAgInSc2d1	další
růstu	růst	k1gInSc2	růst
strženo	stržen	k2eAgNnSc1d1	strženo
městské	městský	k2eAgNnSc1d1	Městské
opevnění	opevnění	k1gNnSc1	opevnění
<g/>
.	.	kIx.	.
</s>
<s>
Peruánská	peruánský	k2eAgFnSc1d1	peruánská
ekonomika	ekonomika	k1gFnSc1	ekonomika
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
exportu	export	k1gInSc6	export
sice	sice	k8xC	sice
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
hospodářskému	hospodářský	k2eAgInSc3d1	hospodářský
rozvoji	rozvoj	k1gInSc3	rozvoj
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
prohloubila	prohloubit	k5eAaPmAgFnS	prohloubit
propast	propast	k1gFnSc1	propast
mezi	mezi	k7c7	mezi
bohatými	bohatý	k2eAgFnPc7d1	bohatá
a	a	k8xC	a
chudými	chudý	k2eAgFnPc7d1	chudá
vrstvami	vrstva	k1gFnPc7	vrstva
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vyvolávalo	vyvolávat	k5eAaImAgNnS	vyvolávat
sociální	sociální	k2eAgNnSc1d1	sociální
napětí	napětí	k1gNnSc1	napětí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1879	[number]	k4	1879
<g/>
-	-	kIx~	-
<g/>
1883	[number]	k4	1883
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
probíhala	probíhat	k5eAaImAgFnS	probíhat
válka	válka	k1gFnSc1	válka
o	o	k7c4	o
Pacifik	Pacifik	k1gInSc4	Pacifik
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Lima	Lima	k1gFnSc1	Lima
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
se	se	k3xPyFc4	se
odpor	odpor	k1gInSc1	odpor
peruánských	peruánský	k2eAgFnPc2d1	peruánská
jednotek	jednotka	k1gFnPc2	jednotka
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c6	o
San	San	k1gFnSc6	San
Juan	Juan	k1gMnSc1	Juan
a	a	k8xC	a
bitvě	bitva	k1gFnSc3	bitva
u	u	k7c2	u
Miraflores	Mirafloresa	k1gFnPc2	Mirafloresa
zhroutil	zhroutit	k5eAaPmAgInS	zhroutit
<g/>
,	,	kIx,	,
obsazeno	obsadit	k5eAaPmNgNnS	obsadit
chilskou	chilský	k2eAgFnSc7d1	chilská
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
okupací	okupace	k1gFnPc2	okupace
utrpělo	utrpět	k5eAaPmAgNnS	utrpět
<g/>
,	,	kIx,	,
když	když	k8xS	když
Chilané	Chilan	k1gMnPc1	Chilan
vyplenili	vyplenit	k5eAaPmAgMnP	vyplenit
veřejná	veřejný	k2eAgNnPc4d1	veřejné
muzea	muzeum	k1gNnPc4	muzeum
<g/>
,	,	kIx,	,
knihovny	knihovna	k1gFnPc4	knihovna
a	a	k8xC	a
vzdělávací	vzdělávací	k2eAgFnPc4d1	vzdělávací
instituce	instituce	k1gFnPc4	instituce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
také	také	k9	také
dav	dav	k1gInSc1	dav
chudiny	chudina	k1gFnSc2	chudina
zaútočil	zaútočit	k5eAaPmAgInS	zaútočit
na	na	k7c4	na
bohaté	bohatý	k2eAgMnPc4d1	bohatý
občany	občan	k1gMnPc4	občan
a	a	k8xC	a
Asiaty	Asiat	k1gMnPc4	Asiat
a	a	k8xC	a
rozkradl	rozkrást	k5eAaPmAgMnS	rozkrást
jejich	jejich	k3xOp3gInSc4	jejich
majetek	majetek	k1gInSc4	majetek
a	a	k8xC	a
obchody	obchod	k1gInPc4	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
město	město	k1gNnSc1	město
podstoupilo	podstoupit	k5eAaPmAgNnS	podstoupit
obnovovací	obnovovací	k2eAgInSc4d1	obnovovací
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
trval	trvat	k5eAaImAgInS	trvat
od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
až	až	k9	až
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
centrum	centrum	k1gNnSc1	centrum
Limy	Lima	k1gFnSc2	Lima
bylo	být	k5eAaImAgNnS	být
přelidněné	přelidněný	k2eAgNnSc1d1	přelidněné
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
vybudována	vybudován	k2eAgFnSc1d1	vybudována
obytná	obytný	k2eAgFnSc1d1	obytná
zóna	zóna	k1gFnSc1	zóna
La	la	k1gNnSc2	la
Victoria	Victorium	k1gNnSc2	Victorium
pro	pro	k7c4	pro
dělnickou	dělnický	k2eAgFnSc4d1	Dělnická
třídu	třída	k1gFnSc4	třída
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
byl	být	k5eAaImAgInS	být
upraven	upravit	k5eAaPmNgInS	upravit
i	i	k8xC	i
městský	městský	k2eAgInSc1d1	městský
plán	plán	k1gInSc1	plán
vybudováním	vybudování	k1gNnSc7	vybudování
širokých	široký	k2eAgFnPc2d1	široká
tříd	třída	k1gFnPc2	třída
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
křižovaly	křižovat	k5eAaImAgFnP	křižovat
město	město	k1gNnSc4	město
a	a	k8xC	a
spojily	spojit	k5eAaPmAgFnP	spojit
je	on	k3xPp3gMnPc4	on
se	s	k7c7	s
satelitními	satelitní	k2eAgNnPc7d1	satelitní
sídly	sídlo	k1gNnPc7	sídlo
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
s	s	k7c7	s
městem	město	k1gNnSc7	město
Miraflores	Mirafloresa	k1gFnPc2	Mirafloresa
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byly	být	k5eAaImAgFnP	být
přestavěny	přestavět	k5eAaPmNgFnP	přestavět
mnohé	mnohý	k2eAgFnPc1d1	mnohá
budovy	budova	k1gFnPc1	budova
historického	historický	k2eAgNnSc2d1	historické
centra	centrum	k1gNnSc2	centrum
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
vládní	vládní	k2eAgInSc1d1	vládní
palác	palác	k1gInSc1	palác
a	a	k8xC	a
radnice	radnice	k1gFnSc1	radnice
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1940	[number]	k4	1940
další	další	k2eAgInSc4d1	další
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
zničilo	zničit	k5eAaPmAgNnS	zničit
většinu	většina	k1gFnSc4	většina
čtvrtí	čtvrt	k1gFnPc2	čtvrt
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
postaveny	postavit	k5eAaPmNgInP	postavit
z	z	k7c2	z
vepřovic	vepřovice	k1gFnPc2	vepřovice
nebo	nebo	k8xC	nebo
quinchy	quincha	k1gFnSc2	quincha
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
Lima	limo	k1gNnSc2	limo
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
období	období	k1gNnSc4	období
růstu	růst	k1gInSc2	růst
populace	populace	k1gFnSc2	populace
urychlené	urychlený	k2eAgFnSc2d1	urychlená
migrací	migrace	k1gFnSc7	migrace
obyvatel	obyvatel	k1gMnPc2	obyvatel
z	z	k7c2	z
horských	horský	k2eAgFnPc2d1	horská
oblastí	oblast	k1gFnPc2	oblast
Peru	Peru	k1gNnSc2	Peru
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
přicházeli	přicházet	k5eAaImAgMnP	přicházet
do	do	k7c2	do
města	město	k1gNnSc2	město
za	za	k7c7	za
prací	práce	k1gFnSc7	práce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
městská	městský	k2eAgFnSc1d1	městská
zástavba	zástavba	k1gFnSc1	zástavba
zaujímala	zaujímat	k5eAaImAgFnS	zaujímat
omezenou	omezený	k2eAgFnSc4d1	omezená
trojúhelníkovou	trojúhelníkový	k2eAgFnSc4d1	trojúhelníková
plochu	plocha	k1gFnSc4	plocha
ohraničenou	ohraničený	k2eAgFnSc4d1	ohraničená
historickým	historický	k2eAgNnSc7d1	historické
centrem	centrum	k1gNnSc7	centrum
<g/>
,	,	kIx,	,
Callaem	Callaum	k1gNnSc7	Callaum
a	a	k8xC	a
Chorrillem	Chorrillo	k1gNnSc7	Chorrillo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
desetiletích	desetiletí	k1gNnPc6	desetiletí
se	se	k3xPyFc4	se
Lima	Lima	k1gFnSc1	Lima
rozrostla	rozrůst	k5eAaPmAgFnS	rozrůst
především	především	k9	především
na	na	k7c4	na
sever	sever	k1gInSc4	sever
za	za	k7c4	za
řeku	řeka	k1gFnSc4	řeka
Rímac	Rímac	k1gFnSc4	Rímac
<g/>
,	,	kIx,	,
na	na	k7c4	na
východ	východ	k1gInSc4	východ
podél	podél	k7c2	podél
centrální	centrální	k2eAgFnSc2d1	centrální
dálnice	dálnice	k1gFnSc2	dálnice
a	a	k8xC	a
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
<s>
Řady	řada	k1gFnPc1	řada
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nejprve	nejprve	k6eAd1	nejprve
žili	žít	k5eAaImAgMnP	žít
stísnění	stísnění	k1gNnSc4	stísnění
ve	v	k7c6	v
slumových	slumův	k2eAgNnPc6d1	slumův
městech	město	k1gNnPc6	město
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Limy	Lima	k1gFnSc2	Lima
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
touto	tento	k3xDgFnSc7	tento
expanzí	expanze	k1gFnSc7	expanze
rychle	rychle	k6eAd1	rychle
množily	množit	k5eAaImAgFnP	množit
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
velkému	velký	k2eAgNnSc3d1	velké
rozšíření	rozšíření	k1gNnSc3	rozšíření
chatrčových	chatrčová	k1gFnPc2	chatrčová
městeček	městečko	k1gNnPc2	městečko
známých	známý	k1gMnPc2	známý
jako	jako	k8xS	jako
pueblos	pueblosa	k1gFnPc2	pueblosa
jóvenes	jóvenesa	k1gFnPc2	jóvenesa
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
veřejné	veřejný	k2eAgFnPc1d1	veřejná
práce	práce	k1gFnPc1	práce
byly	být	k5eAaImAgFnP	být
odstartovány	odstartovat	k5eAaPmNgFnP	odstartovat
především	především	k6eAd1	především
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Manuela	Manuel	k1gMnSc2	Manuel
A.	A.	kA	A.
Odríi	Odrí	k1gFnSc2	Odrí
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
-	-	kIx~	-
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
a	a	k8xC	a
Juana	Juan	k1gMnSc2	Juan
Velasca	Velasca	k1gFnSc1	Velasca
Alvarada	Alvarada	k1gFnSc1	Alvarada
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
-	-	kIx~	-
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dominoval	dominovat	k5eAaImAgInS	dominovat
brutalistický	brutalistický	k2eAgInSc4d1	brutalistický
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
masivní	masivní	k2eAgFnSc1d1	masivní
centrála	centrála	k1gFnSc1	centrála
státem	stát	k1gInSc7	stát
vlastněné	vlastněný	k2eAgFnSc2d1	vlastněná
petrolejářské	petrolejářský	k2eAgFnSc2d1	petrolejářská
společnosti	společnost	k1gFnSc2	společnost
PETROPERU	PETROPERU	kA	PETROPERU
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
v	v	k7c6	v
Limě	Lima	k1gFnSc6	Lima
žilo	žít	k5eAaImAgNnS	žít
6	[number]	k4	6
400	[number]	k4	400
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
28,4	[number]	k4	28,4
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
populace	populace	k1gFnSc2	populace
Peru	Peru	k1gNnSc1	Peru
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
v	v	k7c6	v
Limě	Lima	k1gFnSc6	Lima
žilo	žít	k5eAaImAgNnS	žít
jen	jen	k9	jen
9,4	[number]	k4	9,4
%	%	kIx~	%
peruánské	peruánský	k2eAgFnSc2d1	peruánská
populace	populace	k1gFnSc2	populace
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
má	mít	k5eAaImIp3nS	mít
Lima	Lima	k1gFnSc1	Lima
hustotu	hustota	k1gFnSc4	hustota
zalidnění	zalidnění	k1gNnSc4	zalidnění
více	hodně	k6eAd2	hodně
než	než	k8xS	než
242	[number]	k4	242
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c4	na
kilometr	kilometr	k1gInSc4	kilometr
čtvereční	čtvereční	k2eAgInSc4d1	čtvereční
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
desetiprocentní	desetiprocentní	k2eAgInSc4d1	desetiprocentní
nárůst	nárůst	k1gInSc4	nárůst
oproti	oproti	k7c3	oproti
roku	rok	k1gInSc3	rok
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
územní	územní	k2eAgInSc4d1	územní
rozvoj	rozvoj	k1gInSc4	rozvoj
Limy	Lima	k1gFnSc2	Lima
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
</s>
