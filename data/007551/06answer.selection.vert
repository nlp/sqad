<s>
Náměstí	náměstí	k1gNnSc1	náměstí
Svobody	svoboda	k1gFnSc2	svoboda
(	(	kIx(	(
<g/>
lidově	lidově	k6eAd1	lidově
Svoboďák	Svoboďák	k1gInSc1	Svoboďák
<g/>
;	;	kIx,	;
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k8xS	jako
Dolní	dolní	k2eAgInSc1d1	dolní
trh	trh	k1gInSc1	trh
či	či	k8xC	či
německy	německy	k6eAd1	německy
Grosser	Grosser	k1gMnSc1	Grosser
Platz	Platz	k1gMnSc1	Platz
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
Velké	velký	k2eAgNnSc1d1	velké
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvýznamnější	významný	k2eAgNnSc1d3	nejvýznamnější
náměstí	náměstí	k1gNnSc1	náměstí
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
