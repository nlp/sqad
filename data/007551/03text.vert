<s>
Náměstí	náměstí	k1gNnSc1	náměstí
Svobody	svoboda	k1gFnSc2	svoboda
(	(	kIx(	(
<g/>
lidově	lidově	k6eAd1	lidově
Svoboďák	Svoboďák	k1gInSc1	Svoboďák
<g/>
;	;	kIx,	;
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k8xC	jako
Dolní	dolní	k2eAgInSc1d1	dolní
trh	trh	k1gInSc1	trh
či	či	k8xC	či
německy	německy	k6eAd1	německy
Grosser	Grosser	k1gMnSc1	Grosser
Platz	Platz	k1gMnSc1	Platz
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
Velké	velký	k2eAgNnSc1d1	velké
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvýznamnější	významný	k2eAgNnSc1d3	nejvýznamnější
náměstí	náměstí	k1gNnSc1	náměstí
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
historického	historický	k2eAgNnSc2d1	historické
jádra	jádro	k1gNnSc2	jádro
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
Město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
trojúhelníkového	trojúhelníkový	k2eAgInSc2d1	trojúhelníkový
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Náměstí	náměstí	k1gNnSc1	náměstí
Svobody	svoboda	k1gFnSc2	svoboda
je	být	k5eAaImIp3nS	být
pomyslný	pomyslný	k2eAgInSc1d1	pomyslný
střed	střed	k1gInSc1	střed
a	a	k8xC	a
centrum	centrum	k1gNnSc1	centrum
celého	celý	k2eAgNnSc2d1	celé
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
pocházejí	pocházet	k5eAaImIp3nP	pocházet
ze	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
neslo	nést	k5eAaImAgNnS	nést
název	název	k1gInSc1	název
Forum	forum	k1gNnSc1	forum
Inferius	Inferius	k1gInSc1	Inferius
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
přeložilo	přeložit	k5eAaPmAgNnS	přeložit
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
jako	jako	k8xC	jako
Dolní	dolní	k2eAgInSc4d1	dolní
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dobách	doba	k1gFnPc6	doba
středověku	středověk	k1gInSc2	středověk
si	se	k3xPyFc3	se
zde	zde	k6eAd1	zde
bohatí	bohatý	k2eAgMnPc1d1	bohatý
měšťané	měšťan	k1gMnPc1	měšťan
a	a	k8xC	a
šlechtici	šlechtic	k1gMnPc1	šlechtic
budovali	budovat	k5eAaImAgMnP	budovat
domy	dům	k1gInPc7	dům
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
význam	význam	k1gInSc1	význam
náměstí	náměstí	k1gNnSc3	náměstí
rostl	růst	k5eAaImAgInS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1679	[number]	k4	1679
byl	být	k5eAaImAgInS	být
vystavěn	vystavěn	k2eAgInSc1d1	vystavěn
morový	morový	k2eAgInSc1d1	morový
sloup	sloup	k1gInSc1	sloup
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
stal	stát	k5eAaPmAgInS	stát
dominantou	dominanta	k1gFnSc7	dominanta
celého	celý	k2eAgNnSc2d1	celé
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1869	[number]	k4	1869
byl	být	k5eAaImAgInS	být
zbořen	zbořit	k5eAaPmNgInS	zbořit
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc4	jeho
poslední	poslední	k2eAgInPc4d1	poslední
zbytky	zbytek	k1gInPc4	zbytek
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
po	po	k7c6	po
náletu	nálet	k1gInSc6	nálet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
bylo	být	k5eAaImAgNnS	být
náměstí	náměstí	k1gNnSc1	náměstí
rozsáhle	rozsáhle	k6eAd1	rozsáhle
přebudováno	přebudován	k2eAgNnSc1d1	přebudováno
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c4	mnoho
domů	dům	k1gInPc2	dům
bylo	být	k5eAaImAgNnS	být
zbořeno	zbořit	k5eAaPmNgNnS	zbořit
a	a	k8xC	a
přestavěno	přestavět	k5eAaPmNgNnS	přestavět
v	v	k7c6	v
novorenesančním	novorenesanční	k2eAgInSc6d1	novorenesanční
slohu	sloh	k1gInSc6	sloh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
straně	strana	k1gFnSc6	strana
náměstí	náměstí	k1gNnSc2	náměstí
podle	podle	k7c2	podle
projektu	projekt	k1gInSc2	projekt
architekta	architekt	k1gMnSc2	architekt
Bohuslava	Bohuslav	k1gMnSc2	Bohuslav
Fuchse	Fuchs	k1gMnSc2	Fuchs
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
funkcionalistická	funkcionalistický	k2eAgFnSc1d1	funkcionalistická
budova	budova	k1gFnSc1	budova
Moravské	moravský	k2eAgFnSc2d1	Moravská
banky	banka	k1gFnSc2	banka
s	s	k7c7	s
pasáží	pasáž	k1gFnSc7	pasáž
Beta	beta	k1gNnSc2	beta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
náměstí	náměstí	k1gNnSc4	náměstí
proťala	protít	k5eAaPmAgFnS	protít
v	v	k7c6	v
severojižním	severojižní	k2eAgInSc6d1	severojižní
směru	směr	k1gInSc6	směr
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
trať	trať	k1gFnSc1	trať
od	od	k7c2	od
hlavního	hlavní	k2eAgNnSc2d1	hlavní
nádraží	nádraží	k1gNnSc2	nádraží
na	na	k7c4	na
Moravské	moravský	k2eAgNnSc4d1	Moravské
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yQgFnSc3	který
se	se	k3xPyFc4	se
během	během	k7c2	během
několika	několik	k4yIc2	několik
dalších	další	k2eAgNnPc2d1	další
let	léto	k1gNnPc2	léto
přidaly	přidat	k5eAaPmAgFnP	přidat
i	i	k8xC	i
tratě	trať	k1gFnPc1	trať
ze	z	k7c2	z
Šilingrova	Šilingrův	k2eAgNnSc2d1	Šilingrovo
náměstí	náměstí	k1gNnSc2	náměstí
(	(	kIx(	(
<g/>
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
po	po	k7c6	po
Zámečnické	zámečnický	k2eAgFnSc6d1	zámečnická
ulici	ulice	k1gFnSc6	ulice
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
Kobližné	kobližný	k2eAgFnSc2d1	Kobližná
ulice	ulice	k1gFnSc2	ulice
(	(	kIx(	(
<g/>
od	od	k7c2	od
východu	východ	k1gInSc2	východ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
se	se	k3xPyFc4	se
dochovala	dochovat	k5eAaPmAgFnS	dochovat
pouze	pouze	k6eAd1	pouze
první	první	k4xOgFnSc7	první
z	z	k7c2	z
tratí	trať	k1gFnPc2	trať
vedená	vedený	k2eAgNnPc4d1	vedené
severojižním	severojižní	k2eAgInSc7d1	severojižní
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
poslední	poslední	k2eAgFnSc1d1	poslední
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
náměstí	náměstí	k1gNnSc2	náměstí
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
náměstí	náměstí	k1gNnSc1	náměstí
kompletně	kompletně	k6eAd1	kompletně
předlážděno	předláždit	k5eAaPmNgNnS	předláždit
<g/>
,	,	kIx,	,
osazena	osazen	k2eAgFnSc1d1	osazena
bronzová	bronzový	k2eAgFnSc1d1	bronzová
kašna	kašna	k1gFnSc1	kašna
s	s	k7c7	s
verši	verš	k1gInPc7	verš
Jana	Jan	k1gMnSc2	Jan
Skácela	Skácel	k1gMnSc2	Skácel
<g/>
,	,	kIx,	,
opraven	opraven	k2eAgInSc1d1	opraven
morový	morový	k2eAgInSc1d1	morový
sloup	sloup	k1gInSc1	sloup
<g/>
,	,	kIx,	,
vyznačena	vyznačen	k2eAgFnSc1d1	vyznačena
poloha	poloha	k1gFnSc1	poloha
někdejšího	někdejší	k2eAgInSc2d1	někdejší
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Mikuláše	mikuláš	k1gInSc2	mikuláš
a	a	k8xC	a
nově	nově	k6eAd1	nově
vysazeno	vysadit	k5eAaPmNgNnS	vysadit
několik	několik	k4yIc1	několik
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
podzemí	podzemí	k1gNnSc6	podzemí
vybudovány	vybudován	k2eAgInPc1d1	vybudován
kolektory	kolektor	k1gInPc1	kolektor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
umístěn	umístěn	k2eAgInSc1d1	umístěn
orloj	orloj	k1gInSc1	orloj
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
ovšem	ovšem	k9	ovšem
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
orloj	orloj	k1gInSc4	orloj
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
smyslu	smysl	k1gInSc6	smysl
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
zvláštní	zvláštní	k2eAgFnPc4d1	zvláštní
hodiny	hodina	k1gFnPc4	hodina
vyrobené	vyrobený	k2eAgFnPc4d1	vyrobená
z	z	k7c2	z
africké	africký	k2eAgFnSc2d1	africká
žuly	žula	k1gFnSc2	žula
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
náboje	náboj	k1gInSc2	náboj
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
tak	tak	k6eAd1	tak
mají	mít	k5eAaImIp3nP	mít
upomínat	upomínat	k5eAaImF	upomínat
na	na	k7c4	na
obléhání	obléhání	k1gNnSc4	obléhání
Brna	Brno	k1gNnSc2	Brno
Švédy	Švéda	k1gMnSc2	Švéda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1645	[number]	k4	1645
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostnímu	slavnostní	k2eAgNnSc3d1	slavnostní
otevření	otevření	k1gNnSc3	otevření
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
předcházela	předcházet	k5eAaImAgFnS	předcházet
několikaletá	několikaletý	k2eAgFnSc1d1	několikaletá
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
si	se	k3xPyFc3	se
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
celkem	celkem	k6eAd1	celkem
180	[number]	k4	180
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Opraven	opravit	k5eAaPmNgInS	opravit
byl	být	k5eAaImAgInS	být
i	i	k9	i
mariánský	mariánský	k2eAgInSc1d1	mariánský
morový	morový	k2eAgInSc1d1	morový
sloup	sloup	k1gInSc1	sloup
a	a	k8xC	a
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
nově	nově	k6eAd1	nově
přibyla	přibýt	k5eAaPmAgFnS	přibýt
bronzová	bronzový	k2eAgFnSc1d1	bronzová
kašna	kašna	k1gFnSc1	kašna
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
15	[number]	k4	15
metrů	metr	k1gInPc2	metr
zdobená	zdobený	k2eAgFnSc1d1	zdobená
roštem	rošt	k1gInSc7	rošt
s	s	k7c7	s
verši	verš	k1gInPc7	verš
Jana	Jan	k1gMnSc2	Jan
Skácela	Skácel	k1gMnSc2	Skácel
<g/>
.	.	kIx.	.
</s>
<s>
Básníka	básník	k1gMnSc2	básník
Jana	Jan	k1gMnSc2	Jan
Skácela	Skácel	k1gMnSc2	Skácel
připomíná	připomínat	k5eAaImIp3nS	připomínat
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
nejen	nejen	k6eAd1	nejen
stejnojmenná	stejnojmenný	k2eAgFnSc1d1	stejnojmenná
ulice	ulice	k1gFnSc1	ulice
v	v	k7c6	v
Králově	Králův	k2eAgNnSc6d1	Královo
Poli	pole	k1gNnSc6	pole
a	a	k8xC	a
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
v	v	k7c6	v
Kotlářské	kotlářský	k2eAgFnSc6d1	Kotlářská
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žil	žít	k5eAaImAgMnS	žít
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
svou	svůj	k3xOyFgFnSc4	svůj
kašnu	kašna	k1gFnSc4	kašna
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Odlití	odlití	k1gNnSc1	odlití
jeho	jeho	k3xOp3gInPc2	jeho
veršů	verš	k1gInPc2	verš
do	do	k7c2	do
bronzových	bronzový	k2eAgInPc2d1	bronzový
roštů	rošt	k1gInPc2	rošt
se	se	k3xPyFc4	se
však	však	k9	však
neobešlo	obešnout	k5eNaPmAgNnS	obešnout
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
okolí	okolí	k1gNnSc1	okolí
kruhové	kruhový	k2eAgFnPc1d1	kruhová
kašny	kašna	k1gFnPc1	kašna
ozdobí	ozdobit	k5eAaPmIp3nP	ozdobit
úryvky	úryvek	k1gInPc4	úryvek
několika	několik	k4yIc2	několik
Skácelových	Skácelových	k2eAgFnPc2d1	Skácelových
básní	báseň	k1gFnPc2	báseň
o	o	k7c6	o
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
vedení	vedení	k1gNnSc1	vedení
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
bez	bez	k7c2	bez
vědomí	vědomí	k1gNnSc2	vědomí
dědičky	dědička	k1gFnSc2	dědička
autorských	autorský	k2eAgNnPc2d1	autorské
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
Skácelovy	Skácelův	k2eAgFnPc1d1	Skácelova
manželky	manželka	k1gFnPc1	manželka
Boženy	Božena	k1gFnSc2	Božena
<g/>
.	.	kIx.	.
</s>
<s>
Té	ten	k3xDgFnSc6	ten
se	se	k3xPyFc4	se
nelíbilo	líbit	k5eNaImAgNnS	líbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
magistrát	magistrát	k1gInSc1	magistrát
začal	začít	k5eAaPmAgInS	začít
problém	problém	k1gInSc4	problém
řešit	řešit	k5eAaImF	řešit
až	až	k9	až
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
dokončením	dokončení	k1gNnSc7	dokončení
kašny	kašna	k1gFnSc2	kašna
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Investor	investor	k1gMnSc1	investor
se	se	k3xPyFc4	se
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
obrátil	obrátit	k5eAaPmAgMnS	obrátit
zpětně	zpětně	k6eAd1	zpětně
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
vše	všechen	k3xTgNnSc1	všechen
vyřešilo	vyřešit	k5eAaPmAgNnS	vyřešit
dohodou	dohoda	k1gFnSc7	dohoda
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
popsal	popsat	k5eAaPmAgMnS	popsat
situaci	situace	k1gFnSc4	situace
starou	starý	k2eAgFnSc4d1	stará
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
mluvčí	mluvčí	k1gMnSc1	mluvčí
magistrátu	magistrát	k1gInSc2	magistrát
Pavel	Pavel	k1gMnSc1	Pavel
Žára	Žára	k1gMnSc1	Žára
<g/>
.	.	kIx.	.
</s>
<s>
Právnička	právnička	k1gFnSc1	právnička
Skácelové	Skácelová	k1gFnSc2	Skácelová
původně	původně	k6eAd1	původně
požadovala	požadovat	k5eAaImAgFnS	požadovat
za	za	k7c4	za
autorská	autorský	k2eAgNnPc4d1	autorské
práva	právo	k1gNnPc4	právo
tři	tři	k4xCgNnPc4	tři
miliony	milion	k4xCgInPc4	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
Brno	Brno	k1gNnSc1	Brno
zaplatilo	zaplatit	k5eAaPmAgNnS	zaplatit
450	[number]	k4	450
tisíc	tisíc	k4xCgInPc2	tisíc
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
podoba	podoba	k1gFnSc1	podoba
náměstí	náměstí	k1gNnSc2	náměstí
"	"	kIx"	"
<g/>
nemá	mít	k5eNaImIp3nS	mít
autora	autor	k1gMnSc4	autor
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
soutěž	soutěž	k1gFnSc1	soutěž
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
nové	nový	k2eAgFnSc2d1	nová
podoby	podoba	k1gFnSc2	podoba
náměstí	náměstí	k1gNnSc2	náměstí
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
architekti	architekt	k1gMnPc1	architekt
Petr	Petr	k1gMnSc1	Petr
Kocourek	Kocourek	k1gMnSc1	Kocourek
a	a	k8xC	a
Juraj	Juraj	k1gMnSc1	Juraj
Sonlajtner	Sonlajtner	k1gMnSc1	Sonlajtner
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
město	město	k1gNnSc1	město
nemohlo	moct	k5eNaImAgNnS	moct
práci	práce	k1gFnSc4	práce
zadat	zadat	k5eAaPmF	zadat
přímo	přímo	k6eAd1	přímo
vítězi	vítěz	k1gMnSc3	vítěz
architektonické	architektonický	k2eAgFnSc2d1	architektonická
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Muselo	muset	k5eAaImAgNnS	muset
proto	proto	k8xC	proto
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
výběrové	výběrový	k2eAgNnSc4d1	výběrové
řízení	řízení	k1gNnSc4	řízení
na	na	k7c4	na
projektanta	projektant	k1gMnSc4	projektant
<g/>
.	.	kIx.	.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
architekti	architekt	k1gMnPc1	architekt
na	na	k7c4	na
výzvu	výzva	k1gFnSc4	výzva
k	k	k7c3	k
účasti	účast	k1gFnSc3	účast
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
nereflektovali	reflektovat	k5eNaImAgMnP	reflektovat
<g/>
,	,	kIx,	,
magistrát	magistrát	k1gInSc1	magistrát
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
neshodl	shodnout	k5eNaPmAgMnS	shodnout
v	v	k7c6	v
názorech	názor	k1gInPc6	názor
a	a	k8xC	a
požadavcích	požadavek	k1gInPc6	požadavek
<g/>
.	.	kIx.	.
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
autoři	autor	k1gMnPc1	autor
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
s	s	k7c7	s
magistrátem	magistrát	k1gInSc7	magistrát
soudit	soudit	k5eAaImF	soudit
<g/>
.	.	kIx.	.
</s>
<s>
Výběrové	výběrový	k2eAgNnSc1d1	výběrové
řízení	řízení	k1gNnSc1	řízení
na	na	k7c4	na
projektanta	projektant	k1gMnSc4	projektant
poté	poté	k6eAd1	poté
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
společnost	společnost	k1gFnSc1	společnost
Vlastimila	Vlastimil	k1gMnSc2	Vlastimil
Nováka	Novák	k1gMnSc2	Novák
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
si	se	k3xPyFc3	se
jako	jako	k9	jako
hlavní	hlavní	k2eAgMnSc1d1	hlavní
projektant	projektant	k1gMnSc1	projektant
vzal	vzít	k5eAaPmAgMnS	vzít
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
architekty	architekt	k1gMnPc4	architekt
Tomáše	Tomáš	k1gMnSc4	Tomáš
Rusína	Rusín	k1gMnSc4	Rusín
a	a	k8xC	a
Ivana	Ivan	k1gMnSc4	Ivan
Wahlu	Wahla	k1gMnSc4	Wahla
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
na	na	k7c6	na
výběru	výběr	k1gInSc6	výběr
mobiliáře	mobiliář	k1gInSc2	mobiliář
<g/>
,	,	kIx,	,
výsledné	výsledný	k2eAgFnSc2d1	výsledná
podoby	podoba	k1gFnSc2	podoba
osvětlení	osvětlení	k1gNnSc2	osvětlení
<g/>
,	,	kIx,	,
způsobu	způsob	k1gInSc2	způsob
kladení	kladení	k1gNnSc2	kladení
dlažby	dlažba	k1gFnSc2	dlažba
atd.	atd.	kA	atd.
Autory	autor	k1gMnPc7	autor
kašny	kašna	k1gFnSc2	kašna
jsou	být	k5eAaImIp3nP	být
Ladislav	Ladislav	k1gMnSc1	Ladislav
Kuba	Kuba	k1gMnSc1	Kuba
a	a	k8xC	a
Tomáš	Tomáš	k1gMnSc1	Tomáš
Pilař	Pilař	k1gMnSc1	Pilař
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
vítězství	vítězství	k1gNnSc6	vítězství
se	se	k3xPyFc4	se
porota	porota	k1gFnSc1	porota
shodla	shodnout	k5eAaPmAgFnS	shodnout
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
a	a	k8xC	a
kteří	který	k3yIgMnPc1	který
akceptovali	akceptovat	k5eAaBmAgMnP	akceptovat
požadavky	požadavek	k1gInPc4	požadavek
zadavatele	zadavatel	k1gMnSc2	zadavatel
(	(	kIx(	(
<g/>
magistrátu	magistrát	k1gInSc2	magistrát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nichž	jenž	k3xRgMnPc2	jenž
změnili	změnit	k5eAaPmAgMnP	změnit
použitý	použitý	k2eAgInSc4d1	použitý
materiál	materiál	k1gInSc4	materiál
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Tato	tento	k3xDgFnSc1	tento
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
kombinace	kombinace	k1gFnSc1	kombinace
rozhodování	rozhodování	k1gNnSc2	rozhodování
úředníků	úředník	k1gMnPc2	úředník
<g/>
,	,	kIx,	,
památkářů	památkář	k1gMnPc2	památkář
<g/>
,	,	kIx,	,
architektů	architekt	k1gMnPc2	architekt
a	a	k8xC	a
projektantů	projektant	k1gMnPc2	projektant
přinesla	přinést	k5eAaPmAgFnS	přinést
výsledek	výsledek	k1gInSc4	výsledek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
téměř	téměř	k6eAd1	téměř
nikoho	nikdo	k3yNnSc4	nikdo
nenadchl	nadchnout	k5eNaPmAgInS	nadchnout
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
konstatoval	konstatovat	k5eAaBmAgMnS	konstatovat
Jan	Jan	k1gMnSc1	Jan
Chmelíček	chmelíček	k1gInSc4	chmelíček
v	v	k7c6	v
deníku	deník	k1gInSc6	deník
Právo	právo	k1gNnSc1	právo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obručí	obručí	k1gNnSc6	obručí
jsou	být	k5eAaImIp3nP	být
kolem	kolem	k6eAd1	kolem
kašny	kašna	k1gFnPc1	kašna
vepsány	vepsán	k2eAgInPc4d1	vepsán
následující	následující	k2eAgInPc4d1	následující
verše	verš	k1gInPc4	verš
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Studánka	studánka	k1gFnSc1	studánka
vyschla	vyschnout	k5eAaPmAgFnS	vyschnout
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
chodili	chodit	k5eAaImAgMnP	chodit
jsme	být	k5eAaImIp1nP	být
pít	pít	k5eAaImF	pít
za	za	k7c2	za
horkých	horký	k2eAgInPc2d1	horký
letních	letní	k2eAgInPc2d1	letní
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
teď	teď	k6eAd1	teď
ani	ani	k8xC	ani
krůpěj	krůpěj	k1gFnSc1	krůpěj
neroní	ronit	k5eNaImIp3nS	ronit
zem	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
tajemná	tajemný	k2eAgFnSc1d1	tajemná
<g/>
,	,	kIx,	,
vlhká	vlhký	k2eAgFnSc1d1	vlhká
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Nadarmo	nadarmo	k6eAd1	nadarmo
stojí	stát	k5eAaImIp3nS	stát
otlučený	otlučený	k2eAgInSc1d1	otlučený
hrnek	hrnek	k1gInSc1	hrnek
na	na	k7c6	na
kameni	kámen	k1gInSc6	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
dlouho	dlouho	k6eAd1	dlouho
potrvá	trvat	k5eAaImIp3nS	trvat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
dlouho	dlouho	k6eAd1	dlouho
trvat	trvat	k5eAaImF	trvat
bude	být	k5eAaImBp3nS	být
<g/>
,	,	kIx,	,
než	než	k8xS	než
na	na	k7c4	na
kraj	kraj	k1gInSc4	kraj
spadne	spadnout	k5eAaPmIp3nS	spadnout
hustý	hustý	k2eAgInSc4d1	hustý
déšť	déšť	k1gInSc4	déšť
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
U	u	k7c2	u
břehu	břeh	k1gInSc2	břeh
po	po	k7c4	po
kolena	koleno	k1gNnPc4	koleno
stojí	stát	k5eAaImIp3nS	stát
rákosí	rákosí	k1gNnSc2	rákosí
a	a	k8xC	a
šeptá	šeptat	k5eAaImIp3nS	šeptat
<g/>
,	,	kIx,	,
šeptá	šeptat	k5eAaImIp3nS	šeptat
větru	vítr	k1gInSc3	vítr
po	po	k7c6	po
vůli	vůle	k1gFnSc6	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Vlny	vlna	k1gFnPc1	vlna
narážejí	narážet	k5eAaImIp3nP	narážet
na	na	k7c4	na
kůly	kůl	k1gInPc4	kůl
<g/>
,	,	kIx,	,
hladina	hladina	k1gFnSc1	hladina
je	být	k5eAaImIp3nS	být
plná	plný	k2eAgFnSc1d1	plná
modrých	modrý	k2eAgNnPc2d1	modré
zrcátek	zrcátko	k1gNnPc2	zrcátko
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Když	když	k8xS	když
pláče	pláč	k1gInSc2	pláč
slon	slon	k1gMnSc1	slon
je	být	k5eAaImIp3nS	být
slza	slza	k1gFnSc1	slza
velká	velký	k2eAgFnSc1d1	velká
jak	jak	k8xC	jak
malý	malý	k2eAgInSc1d1	malý
rybník	rybník	k1gInSc1	rybník
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
plave	plavat	k5eAaImIp3nS	plavat
docela	docela	k6eAd1	docela
smutná	smutný	k2eAgFnSc1d1	smutná
vodní	vodní	k2eAgFnSc1d1	vodní
slípka	slípka	k1gFnSc1	slípka
a	a	k8xC	a
potáplice	potáplice	k1gFnPc1	potáplice
naříkavé	naříkavý	k2eAgFnPc1d1	naříkavá
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
A	a	k9	a
padal	padat	k5eAaImAgInS	padat
déšť	déšť	k1gInSc1	déšť
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
tak	tak	k6eAd1	tak
těžký	těžký	k2eAgInSc1d1	těžký
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
byl	být	k5eAaImAgMnS	být
tak	tak	k9	tak
sám	sám	k3xTgMnSc1	sám
a	a	k8xC	a
beze	beze	k7c2	beze
jména	jméno	k1gNnSc2	jméno
a	a	k8xC	a
každá	každý	k3xTgFnSc1	každý
kapka	kapka	k1gFnSc1	kapka
byla	být	k5eAaImAgFnS	být
hřebík	hřebík	k1gInSc4	hřebík
ukovaný	ukovaný	k2eAgInSc4d1	ukovaný
za	za	k7c4	za
studena	studeno	k1gNnPc4	studeno
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Kdyby	kdyby	kYmCp3nP	kdyby
tak	tak	k9	tak
pršelo	pršet	k5eAaImAgNnS	pršet
celou	celý	k2eAgFnSc4d1	celá
noc	noc	k1gFnSc4	noc
a	a	k8xC	a
celý	celý	k2eAgInSc4d1	celý
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
a	a	k8xC	a
potom	potom	k6eAd1	potom
ještě	ještě	k6eAd1	ještě
<g/>
,	,	kIx,	,
jednu	jeden	k4xCgFnSc4	jeden
noc	noc	k1gFnSc4	noc
<g/>
,	,	kIx,	,
pramínek	pramínek	k1gInSc4	pramínek
<g/>
,	,	kIx,	,
třebas	třebas	k8wRxS	třebas
prstu	prst	k1gInSc2	prst
tenči	tenčit	k5eAaImRp2nS	tenčit
<g/>
,	,	kIx,	,
jistě	jistě	k6eAd1	jistě
by	by	kYmCp3nS	by
vytryskl	vytrysknout	k5eAaPmAgMnS	vytrysknout
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
A	a	k9	a
když	když	k8xS	když
jsme	být	k5eAaImIp1nP	být
vyšli	vyjít	k5eAaPmAgMnP	vyjít
ven	ven	k6eAd1	ven
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
trošku	trošku	k6eAd1	trošku
popršelo	popršet	k5eAaPmAgNnS	popršet
<g/>
.	.	kIx.	.
</s>
<s>
Stromy	strom	k1gInPc1	strom
však	však	k9	však
nadál	nadál	k6eAd1	nadál
dychtily	dychtit	k5eAaImAgFnP	dychtit
<g/>
,	,	kIx,	,
v	v	k7c6	v
lupení	lupení	k1gNnSc6	lupení
frkal	frkat	k5eAaImAgInS	frkat
vraný	vraný	k2eAgInSc1d1	vraný
déšť	déšť	k1gInSc1	déšť
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Sám	sám	k3xTgMnSc1	sám
bez	bez	k7c2	bez
ničeho	nic	k3yNnSc2	nic
naposled	naposled	k6eAd1	naposled
a	a	k8xC	a
marně	marně	k6eAd1	marně
kdo	kdo	k3yInSc1	kdo
za	za	k7c2	za
nás	my	k3xPp1nPc2	my
od	od	k7c2	od
včerejška	včerejšek	k1gInSc2	včerejšek
celou	celý	k2eAgFnSc4d1	celá
pravdu	pravda	k1gFnSc4	pravda
má	mít	k5eAaImIp3nS	mít
trápí	trápit	k5eAaImIp3nS	trápit
mne	já	k3xPp1nSc2	já
žízeň	žízeň	k1gFnSc4	žízeň
a	a	k8xC	a
jsem	být	k5eAaImIp1nS	být
vyčerpaný	vyčerpaný	k2eAgMnSc1d1	vyčerpaný
jak	jak	k8xC	jak
studna	studna	k1gFnSc1	studna
na	na	k7c6	na
návsi	náves	k1gFnSc6	náves
když	když	k8xS	když
hoří	hořet	k5eAaImIp3nS	hořet
stodola	stodola	k1gFnSc1	stodola
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Déšť	déšť	k1gInSc1	déšť
padá	padat	k5eAaImIp3nS	padat
na	na	k7c4	na
růže	růž	k1gFnPc4	růž
prší	pršet	k5eAaImIp3nS	pršet
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nekonečné	konečný	k2eNgNnSc1d1	nekonečné
zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
padat	padat	k5eAaImF	padat
věčně	věčně	k6eAd1	věčně
do	do	k7c2	do
zahrad	zahrada	k1gFnPc2	zahrada
kolmý	kolmý	k2eAgInSc1d1	kolmý
zlatý	zlatý	k2eAgInSc1d1	zlatý
déšť	déšť	k1gInSc1	déšť
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
A	a	k9	a
znovu	znovu	k6eAd1	znovu
přelétají	přelétat	k5eAaPmIp3nP	přelétat
ptáci	pták	k1gMnPc1	pták
jak	jak	k8xC	jak
ve	v	k7c6	v
snu	sen	k1gInSc6	sen
lidé	člověk	k1gMnPc1	člověk
přes	přes	k7c4	přes
most	most	k1gInSc4	most
jdou	jít	k5eAaImIp3nP	jít
který	který	k3yIgMnSc1	který
tu	tu	k6eAd1	tu
zbyl	zbýt	k5eAaPmAgInS	zbýt
jak	jak	k8xS	jak
zbytek	zbytek	k1gInSc1	zbytek
křídla	křídlo	k1gNnSc2	křídlo
a	a	k8xC	a
modrý	modrý	k2eAgInSc1d1	modrý
úžas	úžas	k1gInSc1	úžas
nad	nad	k7c7	nad
vodou	voda	k1gFnSc7	voda
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
slyšet	slyšet	k5eAaImF	slyšet
rány	rána	k1gFnPc4	rána
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
malých	malý	k2eAgFnPc6d1	malá
sáňkách	sáňky	k1gFnPc6	sáňky
slunce	slunce	k1gNnSc2	slunce
se	se	k3xPyFc4	se
svezlo	svézt	k5eAaPmAgNnS	svézt
po	po	k7c6	po
sekyře	sekyra	k1gFnSc6	sekyra
a	a	k8xC	a
spadlo	spadnout	k5eAaPmAgNnS	spadnout
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Vždyť	vždyť	k9	vždyť
v	v	k7c6	v
nitru	nitro	k1gNnSc6	nitro
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
z	z	k7c2	z
nás	my	k3xPp1nPc2	my
<g/>
,	,	kIx,	,
hladina	hladina	k1gFnSc1	hladina
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
,	,	kIx,	,
hladina	hladina	k1gFnSc1	hladina
čisté	čistý	k2eAgFnSc2d1	čistá
spodní	spodní	k2eAgFnSc2d1	spodní
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Nejprve	nejprve	k6eAd1	nejprve
přinášeli	přinášet	k5eAaImAgMnP	přinášet
v	v	k7c6	v
dlani	dlaň	k1gFnSc6	dlaň
jedinou	jediný	k2eAgFnSc4d1	jediná
studánku	studánka	k1gFnSc4	studánka
<g/>
,	,	kIx,	,
dávali	dávat	k5eAaImAgMnP	dávat
hvězdám	hvězda	k1gFnPc3	hvězda
pít	pít	k5eAaImF	pít
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
kdosi	kdosi	k3yInSc1	kdosi
vzal	vzít	k5eAaPmAgMnS	vzít
tu	ten	k3xDgFnSc4	ten
dlaň	dlaň	k1gFnSc4	dlaň
a	a	k8xC	a
učinil	učinit	k5eAaImAgInS	učinit
ji	on	k3xPp3gFnSc4	on
čirou	čirý	k2eAgFnSc4d1	čirá
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
žíznivé	žíznivý	k2eAgNnSc1d1	žíznivé
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
platí	platit	k5eAaImIp3nS	platit
krutou	krutý	k2eAgFnSc4d1	krutá
daň	daň	k1gFnSc4	daň
<g/>
,	,	kIx,	,
a	a	k8xC	a
vložil	vložit	k5eAaPmAgMnS	vložit
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
růži	růže	k1gFnSc4	růže
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
lilie	lilie	k1gFnSc1	lilie
a	a	k8xC	a
nelilie	nelilie	k1gFnSc1	nelilie
<g/>
,	,	kIx,	,
déšť	déšť	k1gInSc1	déšť
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
padá	padat	k5eAaImIp3nS	padat
a	a	k8xC	a
jenž	jenž	k3xRgMnSc1	jenž
lije	lít	k5eAaImIp3nS	lít
a	a	k8xC	a
život	život	k1gInSc1	život
bývá	bývat	k5eAaImIp3nS	bývat
rovněž	rovněž	k9	rovněž
těžký	těžký	k2eAgInSc1d1	těžký
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
koníčci	koníček	k1gMnPc1	koníček
chodí	chodit	k5eAaImIp3nP	chodit
radši	rád	k6eAd2	rád
pěšky	pěšky	k6eAd1	pěšky
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Aby	aby	k9	aby
tvá	tvůj	k3xOp2gFnSc1	tvůj
mince	mince	k1gFnSc1	mince
zněla	znět	k5eAaImAgFnS	znět
v	v	k7c6	v
čase	čas	k1gInSc6	čas
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
neskonale	skonale	k6eNd1	skonale
tak	tak	k6eAd1	tak
jak	jak	k6eAd1	jak
zní	znět	k5eAaImIp3nS	znět
únava	únava	k1gFnSc1	únava
ve	v	k7c6	v
fontáně	fontána	k1gFnSc6	fontána
a	a	k8xC	a
římské	římský	k2eAgNnSc1d1	římské
číslo	číslo	k1gNnSc1	číslo
vročení	vročení	k1gNnSc2	vročení
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Prší	pršet	k5eAaImIp3nP	pršet
a	a	k8xC	a
kapky	kapka	k1gFnPc1	kapka
tančí	tančit	k5eAaImIp3nP	tančit
na	na	k7c4	na
asfalt	asfalt	k1gInSc4	asfalt
prší	pršet	k5eAaImIp3nS	pršet
do	do	k7c2	do
skřivánčí	skřivánčí	k2eAgInPc4d1	skřivánčí
to	ten	k3xDgNnSc1	ten
bude	být	k5eAaImBp3nS	být
sýkorek	sýkorka	k1gFnPc2	sýkorka
mokrých	mokrý	k2eAgMnPc2d1	mokrý
prší	pršet	k5eAaImIp3nS	pršet
<g />
.	.	kIx.	.
</s>
<s>
jak	jak	k8xS	jak
pršelo	pršet	k5eAaImAgNnS	pršet
by	by	kYmCp3nS	by
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
jak	jak	k8xC	jak
do	do	k7c2	do
tvých	tvůj	k3xOp2gInPc2	tvůj
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
O	o	k7c4	o
mnoho	mnoho	k4c4	mnoho
nejde	jít	k5eNaImIp3nS	jít
deštěm	dešť	k1gInSc7	dešť
kamenným	kamenný	k2eAgInSc7d1	kamenný
nechat	nechat	k5eAaPmF	nechat
si	se	k3xPyFc3	se
rozbít	rozbít	k5eAaPmF	rozbít
tvář	tvář	k1gFnSc4	tvář
a	a	k8xC	a
duši	duše	k1gFnSc4	duše
neporanit	poranit	k5eNaPmF	poranit
od	od	k7c2	od
narození	narození	k1gNnSc2	narození
po	po	k7c4	po
smrt	smrt	k1gFnSc4	smrt
pršívá	pršívat	k5eAaImIp3nS	pršívat
žulové	žulový	k2eAgInPc4d1	žulový
deště	dešť	k1gInPc4	dešť
máčejí	máčet	k5eAaImIp3nP	máčet
nás	my	k3xPp1nPc2	my
na	na	k7c4	na
nit	nit	k1gFnSc4	nit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Studánku	studánka	k1gFnSc4	studánka
dej	dát	k5eAaPmRp2nS	dát
mi	já	k3xPp1nSc3	já
blízko	blízko	k1gNnSc4	blízko
u	u	k7c2	u
lesa	les	k1gInSc2	les
a	a	k8xC	a
nehlubokou	hluboký	k2eNgFnSc4d1	nehluboká
jenom	jenom	k6eAd1	jenom
na	na	k7c4	na
dlaň	dlaň	k1gFnSc4	dlaň
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
žabku	žabka	k1gFnSc4	žabka
která	který	k3yQgFnSc1	který
vodu	voda	k1gFnSc4	voda
čistí	čistit	k5eAaImIp3nS	čistit
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
povybírám	povybírat	k5eAaImIp1nS	povybírat
napadané	napadaný	k2eAgNnSc1d1	napadané
listí	listí	k1gNnSc1	listí
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
se	se	k3xPyFc4	se
budu	být	k5eAaImBp1nS	být
starat	starat	k5eAaImF	starat
aby	aby	kYmCp3nS	aby
nezamrzla	zamrznout	k5eNaPmAgFnS	zamrznout
a	a	k8xC	a
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
zavedu	zavést	k5eAaPmIp1nS	zavést
tam	tam	k6eAd1	tam
lidi	člověk	k1gMnPc4	člověk
žíznivé	žíznivý	k2eAgNnSc1d1	žíznivé
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Prší	pršet	k5eAaImIp3nS	pršet
<g/>
.	.	kIx.	.
</s>
<s>
Kapky	kapka	k1gFnPc1	kapka
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
skle	sklo	k1gNnSc6	sklo
jsou	být	k5eAaImIp3nP	být
vysvlečené	vysvlečený	k2eAgFnPc1d1	vysvlečená
perly	perla	k1gFnPc1	perla
a	a	k8xC	a
tráva	tráva	k1gFnSc1	tráva
vstává	vstávat	k5eAaImIp3nS	vstávat
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
okna	okno	k1gNnSc2	okno
<g/>
,	,	kIx,	,
perla	perla	k1gFnSc1	perla
<g/>
,	,	kIx,	,
celá	celý	k2eAgFnSc1d1	celá
mokrá	mokrý	k2eAgFnSc1d1	mokrá
<g/>
,	,	kIx,	,
utíká	utíkat	k5eAaImIp3nS	utíkat
se	se	k3xPyFc4	se
schovat	schovat	k5eAaPmF	schovat
za	za	k7c4	za
špinavý	špinavý	k2eAgInSc4d1	špinavý
rám	rám	k1gInSc4	rám
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Ta	ten	k3xDgNnPc1	ten
němá	němý	k2eAgNnPc1d1	němé
slova	slovo	k1gNnPc1	slovo
vypůjčím	vypůjčit	k5eAaPmIp1nS	vypůjčit
si	se	k3xPyFc3	se
od	od	k7c2	od
ryb	ryba	k1gFnPc2	ryba
budu	být	k5eAaImBp1nS	být
je	on	k3xPp3gFnPc4	on
říkat	říkat	k5eAaImF	říkat
vroucně	vroucně	k6eAd1	vroucně
<g />
.	.	kIx.	.
</s>
<s>
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
ani	ani	k8xC	ani
trochu	trochu	k6eAd1	trochu
nebude	být	k5eNaImBp3nS	být
mi	já	k3xPp1nSc3	já
líto	líto	k6eAd1	líto
jestli	jestli	k9	jestli
se	se	k3xPyFc4	se
utopí	utopit	k5eAaPmIp3nP	utopit
a	a	k8xC	a
na	na	k7c4	na
břeh	břeh	k1gInSc4	břeh
nedojdou	dojít	k5eNaPmIp3nP	dojít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Pod	pod	k7c7	pod
okny	okno	k1gNnPc7	okno
koupají	koupat	k5eAaImIp3nP	koupat
se	se	k3xPyFc4	se
kosi	kos	k1gMnPc1	kos
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
po	po	k7c6	po
ránu	ráno	k1gNnSc6	ráno
a	a	k8xC	a
kapka	kapka	k1gFnSc1	kapka
rosy	rosa	k1gFnSc2	rosa
váží	vážit	k5eAaImIp3nS	vážit
tak	tak	k6eAd1	tak
málo	málo	k6eAd1	málo
a	a	k8xC	a
skoro	skoro	k6eAd1	skoro
nic	nic	k3yNnSc1	nic
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
neváha	neváha	k1gFnSc1	neváha
nás	my	k3xPp1nPc4	my
zavažuje	zavažovat	k5eAaImIp3nS	zavažovat
a	a	k8xC	a
sklání	sklánět	k5eAaImIp3nS	sklánět
k	k	k7c3	k
zvláštní	zvláštní	k2eAgFnSc3d1	zvláštní
pokoře	pokora	k1gFnSc3	pokora
<g/>
,	,	kIx,	,
v	v	k7c6	v
kaluži	kaluž	k1gFnSc6	kaluž
pluje	plout	k5eAaImIp3nS	plout
černé	černý	k2eAgNnSc1d1	černé
peří	peří	k1gNnSc1	peří
a	a	k8xC	a
zpěv	zpěv	k1gInSc1	zpěv
se	se	k3xPyFc4	se
nese	nést	k5eAaImIp3nS	nést
po	po	k7c6	po
dvoře	dvůr	k1gInSc6	dvůr
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Malá	malý	k2eAgFnSc1d1	malá
kašna	kašna	k1gFnSc1	kašna
Na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
menší	malý	k2eAgFnSc1d2	menší
kašna	kašna	k1gFnSc1	kašna
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
s	s	k7c7	s
verši	verš	k1gInPc7	verš
od	od	k7c2	od
Jana	Jan	k1gMnSc2	Jan
Skácela	Skácel	k1gMnSc2	Skácel
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
žízeň	žízeň	k1gFnSc1	žízeň
Jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
dávno	dávno	k6eAd1	dávno
co	co	k3yRnSc4	co
jsme	být	k5eAaImIp1nP	být
odešli	odejít	k5eAaPmAgMnP	odejít
a	a	k8xC	a
kolik	kolik	k9	kolik
žízně	žízně	k6eAd1	žízně
<g/>
/	/	kIx~	/
kolik	kolik	k9	kolik
krásné	krásný	k2eAgFnSc2d1	krásná
žízně	žízeň	k1gFnSc2	žízeň
jsme	být	k5eAaImIp1nP	být
zanechali	zanechat	k5eAaPmAgMnP	zanechat
doma	doma	k6eAd1	doma
<g/>
/	/	kIx~	/
Je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
dávno	dávno	k6eAd1	dávno
a	a	k8xC	a
kolik	kolik	k4yQc4	kolik
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
uplynulo	uplynout	k5eAaPmAgNnS	uplynout
let	léto	k1gNnPc2	léto
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
k	k	k7c3	k
nevíře	nevíra	k1gFnSc3	nevíra
A	a	k9	a
přece	přece	k9	přece
dodnes	dodnes	k6eAd1	dodnes
po	po	k7c6	po
křemeni	křemen	k1gInSc6	křemen
chutná	chutnat	k5eAaImIp3nS	chutnat
po	po	k7c6	po
síře	síra	k1gFnSc6	síra
ta	ten	k3xDgFnSc1	ten
žízeň	žízeň	k1gFnSc1	žízeň
navždy	navždy	k6eAd1	navždy
žízeň	žízeň	k1gFnSc4	žízeň
naposled	naposled	k6eAd1	naposled
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
–	–	k?	–
forum	forum	k1gNnSc1	forum
Inferius	Inferius	k1gMnSc1	Inferius
(	(	kIx(	(
<g/>
Dolní	dolní	k2eAgInSc1d1	dolní
trh	trh	k1gInSc1	trh
<g/>
)	)	kIx)	)
14	[number]	k4	14
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
–	–	k?	–
forum	forum	k1gNnSc1	forum
Inferius	Inferius	k1gMnSc1	Inferius
+	+	kIx~	+
Lugek	Lugek	k1gMnSc1	Lugek
(	(	kIx(	(
<g/>
Dolní	dolní	k2eAgInSc1d1	dolní
trh	trh	k1gInSc1	trh
+	+	kIx~	+
Lugek	Lugek	k1gInSc1	Lugek
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
15	[number]	k4	15
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
–	–	k?	–
Dolní	dolní	k2eAgInSc4d1	dolní
trh	trh	k1gInSc4	trh
(	(	kIx(	(
<g/>
Unterring	Unterring	k1gInSc4	Unterring
<g/>
,	,	kIx,	,
též	též	k9	též
Niederring	Niederring	k1gInSc1	Niederring
<g/>
)	)	kIx)	)
18	[number]	k4	18
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
–	–	k?	–
Velké	velký	k2eAgNnSc1d1	velké
náměstí	náměstí	k1gNnSc1	náměstí
(	(	kIx(	(
<g/>
Grosser	Grosser	k1gMnSc1	Grosser
Platz	Platz	k1gMnSc1	Platz
<g/>
)	)	kIx)	)
15	[number]	k4	15
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1915	[number]	k4	1915
–	–	k?	–
náměstí	náměstí	k1gNnSc2	náměstí
Císaře	Císař	k1gMnSc2	Císař
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
(	(	kIx(	(
<g/>
Kaiser-Franz-Josef-Platz	Kaiser-Franz-Josef-Platz	k1gMnSc1	Kaiser-Franz-Josef-Platz
<g/>
)	)	kIx)	)
30	[number]	k4	30
<g/>
<g />
.	.	kIx.	.
</s>
<s>
12	[number]	k4	12
<g/>
.	.	kIx.	.
1918	[number]	k4	1918
–	–	k?	–
náměstí	náměstí	k1gNnSc4	náměstí
Svobody	svoboda	k1gFnSc2	svoboda
(	(	kIx(	(
<g/>
Freiheitsplatz	Freiheitsplatz	k1gMnSc1	Freiheitsplatz
<g/>
)	)	kIx)	)
17	[number]	k4	17
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1939	[number]	k4	1939
–	–	k?	–
náměstí	náměstí	k1gNnSc2	náměstí
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
(	(	kIx(	(
<g/>
Adolf-Hitler-Platz	Adolf-Hitler-Platz	k1gMnSc1	Adolf-Hitler-Platz
<g/>
)	)	kIx)	)
18	[number]	k4	18
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1939	[number]	k4	1939
–	–	k?	–
náměstí	náměstí	k1gNnSc4	náměstí
Svobody	svoboda	k1gFnSc2	svoboda
(	(	kIx(	(
<g/>
Freiheitsplatz	Freiheitsplatz	k1gMnSc1	Freiheitsplatz
<g/>
)	)	kIx)	)
26	[number]	k4	26
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1942	[number]	k4	1942
–	–	k?	–
náměstí	náměstí	k1gNnSc1	náměstí
Viktoria	Viktoria	k1gFnSc1	Viktoria
(	(	kIx(	(
<g/>
Viktoria-Platz	Viktoria-Platz	k1gInSc1	Viktoria-Platz
<g/>
)	)	kIx)	)
10	[number]	k4	10
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1945	[number]	k4	1945
–	–	k?	–
náměstí	náměstí	k1gNnSc4	náměstí
Svobody	svoboda	k1gFnSc2	svoboda
</s>
