<s>
Myanmar	Myanmar	k1gInSc1	Myanmar
či	či	k8xC	či
Barma	Barma	k1gFnSc1	Barma
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
také	také	k9	také
Myanma	Myanma	k1gFnSc1	Myanma
<g/>
,	,	kIx,	,
oficiálním	oficiální	k2eAgInSc7d1	oficiální
názvem	název	k1gInSc7	název
Svazová	svazový	k2eAgFnSc1d1	svazová
republika	republika	k1gFnSc1	republika
Myanmar	Myanmara	k1gFnPc2	Myanmara
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
straně	strana	k1gFnSc6	strana
poloostrova	poloostrov	k1gInSc2	poloostrov
Zadní	zadní	k2eAgFnSc2d1	zadní
Indie	Indie	k1gFnSc2	Indie
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
Bengálského	bengálský	k2eAgInSc2d1	bengálský
zálivu	záliv	k1gInSc2	záliv
a	a	k8xC	a
Andamanského	Andamanský	k2eAgNnSc2d1	Andamanské
moře	moře	k1gNnSc2	moře
v	v	k7c6	v
Indickém	indický	k2eAgInSc6d1	indický
oceánu	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Sousedními	sousední	k2eAgInPc7d1	sousední
státy	stát	k1gInPc7	stát
jsou	být	k5eAaImIp3nP	být
Bangladéš	Bangladéš	k1gInSc4	Bangladéš
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Laos	Laos	k1gInSc1	Laos
a	a	k8xC	a
Thajsko	Thajsko	k1gNnSc1	Thajsko
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
odhadován	odhadovat	k5eAaImNgInS	odhadovat
kolem	kolem	k7c2	kolem
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Novým	nový	k2eAgNnSc7d1	nové
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
Neipyijto	Neipyijto	k1gNnSc4	Neipyijto
<g/>
.	.	kIx.	.
</s>
<s>
Úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
barmština	barmština	k1gFnSc1	barmština
<g/>
.	.	kIx.	.
</s>
<s>
Převážná	převážný	k2eAgFnSc1d1	převážná
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
buddhismu	buddhismus	k1gInSc3	buddhismus
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc4	dějiny
Barmy	Barma	k1gFnSc2	Barma
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
barmským	barmský	k2eAgInSc7d1	barmský
státem	stát	k1gInSc7	stát
bylo	být	k5eAaImAgNnS	být
království	království	k1gNnSc1	království
Pugam	Pugam	k1gInSc1	Pugam
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
rozkládalo	rozkládat	k5eAaImAgNnS	rozkládat
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
území	území	k1gNnSc2	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
Myanmaru	Myanmar	k1gInSc2	Myanmar
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
podlehlo	podlehnout	k5eAaPmAgNnS	podlehnout
vpádům	vpád	k1gInPc3	vpád
Mongolů	Mongol	k1gMnPc2	Mongol
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
Pugamským	Pugamský	k2eAgNnSc7d1	Pugamský
královstvím	království	k1gNnSc7	království
na	na	k7c6	na
území	území	k1gNnSc6	území
Myanmaru	Myanmar	k1gInSc2	Myanmar
existovalo	existovat	k5eAaImAgNnS	existovat
několik	několik	k4yIc1	několik
různých	různý	k2eAgFnPc2d1	různá
civilizací	civilizace	k1gFnPc2	civilizace
a	a	k8xC	a
kultur	kultura	k1gFnPc2	kultura
(	(	kIx(	(
<g/>
např.	např.	kA	např.
městské	městský	k2eAgInPc1d1	městský
státy	stát	k1gInPc1	stát
Pyu	Pyu	k1gFnSc2	Pyu
a	a	k8xC	a
království	království	k1gNnSc2	království
Mon	Mon	k1gFnSc2	Mon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
byla	být	k5eAaImAgFnS	být
znovu	znovu	k6eAd1	znovu
sjednocena	sjednotit	k5eAaPmNgFnS	sjednotit
až	až	k9	až
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
řadu	řada	k1gFnSc4	řada
staletí	staletí	k1gNnSc2	staletí
spolu	spolu	k6eAd1	spolu
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
etnické	etnický	k2eAgFnPc1d1	etnická
skupiny	skupina	k1gFnPc1	skupina
sváděly	svádět	k5eAaImAgFnP	svádět
boje	boj	k1gInPc4	boj
o	o	k7c4	o
moc	moc	k1gFnSc4	moc
a	a	k8xC	a
nadvládu	nadvláda	k1gFnSc4	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
Barmánci	Barmánec	k1gMnPc1	Barmánec
získali	získat	k5eAaPmAgMnP	získat
převahu	převaha	k1gFnSc4	převaha
až	až	k6eAd1	až
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následující	následující	k2eAgFnSc2d1	následující
vlády	vláda	k1gFnSc2	vláda
barmských	barmský	k2eAgMnPc2d1	barmský
králů	král	k1gMnPc2	král
získal	získat	k5eAaPmAgMnS	získat
Myanmar	Myanmar	k1gMnSc1	Myanmar
své	svůj	k3xOyFgFnSc2	svůj
stávající	stávající	k2eAgFnSc2d1	stávající
hranice	hranice	k1gFnSc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1824	[number]	k4	1824
přerostlo	přerůst	k5eAaPmAgNnS	přerůst
napětí	napětí	k1gNnSc1	napětí
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
královské	královský	k2eAgFnSc2d1	královská
Barmy	Barma	k1gFnSc2	Barma
a	a	k8xC	a
Britské	britský	k2eAgFnSc2d1	britská
Indie	Indie	k1gFnSc2	Indie
v	v	k7c4	v
první	první	k4xOgFnSc4	první
britsko-barmskou	britskoarmský	k2eAgFnSc4d1	britsko-barmský
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
válka	válka	k1gFnSc1	válka
propukla	propuknout	k5eAaPmAgFnS	propuknout
v	v	k7c6	v
letech	let	k1gInPc6	let
1852	[number]	k4	1852
<g/>
–	–	k?	–
<g/>
1853	[number]	k4	1853
a	a	k8xC	a
k	k	k7c3	k
poslední	poslední	k2eAgFnSc3d1	poslední
válce	válka	k1gFnSc3	válka
pak	pak	k6eAd1	pak
došlo	dojít	k5eAaPmAgNnS	dojít
roku	rok	k1gInSc3	rok
1885	[number]	k4	1885
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
těchto	tento	k3xDgFnPc2	tento
tří	tři	k4xCgFnPc2	tři
britsko-barmských	britskoarmský	k2eAgFnPc2d1	britsko-barmský
válek	válka	k1gFnPc2	válka
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
celé	celý	k2eAgNnSc1d1	celé
území	území	k1gNnSc1	území
Barmy	Barma	k1gFnSc2	Barma
dostalo	dostat	k5eAaPmAgNnS	dostat
pod	pod	k7c4	pod
britskou	britský	k2eAgFnSc4d1	britská
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
skončilo	skončit	k5eAaPmAgNnS	skončit
období	období	k1gNnSc1	období
vlády	vláda	k1gFnSc2	vláda
barmských	barmský	k2eAgMnPc2d1	barmský
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1866	[number]	k4	1866
byla	být	k5eAaImAgFnS	být
Barma	Barma	k1gFnSc1	Barma
připojena	připojit	k5eAaPmNgFnS	připojit
jako	jako	k8xC	jako
provincie	provincie	k1gFnSc1	provincie
k	k	k7c3	k
Britské	britský	k2eAgFnSc3d1	britská
Indii	Indie	k1gFnSc3	Indie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
se	se	k3xPyFc4	se
Barma	Barma	k1gFnSc1	Barma
stala	stát	k5eAaPmAgFnS	stát
britskou	britský	k2eAgFnSc7d1	britská
korunní	korunní	k2eAgFnSc7d1	korunní
kolonií	kolonie	k1gFnSc7	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Britská	britský	k2eAgFnSc1d1	britská
správa	správa	k1gFnSc1	správa
výrazně	výrazně	k6eAd1	výrazně
změnila	změnit	k5eAaPmAgFnS	změnit
charakter	charakter	k1gInSc4	charakter
Barmy	Barma	k1gFnSc2	Barma
<g/>
.	.	kIx.	.
</s>
<s>
Přinesla	přinést	k5eAaPmAgFnS	přinést
rozvoj	rozvoj	k1gInSc4	rozvoj
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
a	a	k8xC	a
rozšíření	rozšíření	k1gNnSc4	rozšíření
těžby	těžba	k1gFnSc2	těžba
nerostných	nerostný	k2eAgFnPc2d1	nerostná
surovin	surovina	k1gFnPc2	surovina
i	i	k8xC	i
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
produkce	produkce	k1gFnSc2	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Barmy	Barma	k1gFnSc2	Barma
začaly	začít	k5eAaPmAgFnP	začít
pronikat	pronikat	k5eAaImF	pronikat
zahraniční	zahraniční	k2eAgInPc4d1	zahraniční
kulturní	kulturní	k2eAgInPc4d1	kulturní
prvky	prvek	k1gInPc4	prvek
a	a	k8xC	a
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
země	zem	k1gFnSc2	zem
přicházelo	přicházet	k5eAaImAgNnS	přicházet
také	také	k9	také
mnoho	mnoho	k4c1	mnoho
imigrantů	imigrant	k1gMnPc2	imigrant
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
oblastí	oblast	k1gFnPc2	oblast
britské	britský	k2eAgFnSc2d1	britská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Britskou	britský	k2eAgFnSc4d1	britská
nadvládu	nadvláda	k1gFnSc4	nadvláda
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
provázely	provázet	k5eAaImAgInP	provázet
hlasy	hlas	k1gInPc1	hlas
řady	řada	k1gFnSc2	řada
obyvatel	obyvatel	k1gMnPc2	obyvatel
po	po	k7c6	po
nezávislosti	nezávislost	k1gFnSc6	nezávislost
Barmy	Barma	k1gFnSc2	Barma
a	a	k8xC	a
toto	tento	k3xDgNnSc4	tento
protikoloniální	protikoloniální	k2eAgNnSc4d1	protikoloniální
úsilí	úsilí	k1gNnSc4	úsilí
postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
dále	daleko	k6eAd2	daleko
sílilo	sílit	k5eAaImAgNnS	sílit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Barmy	Barma	k1gFnSc2	Barma
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
Barma	Barma	k1gFnSc1	Barma
okupována	okupován	k2eAgFnSc1d1	okupována
Japonskem	Japonsko	k1gNnSc7	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
Japonsko	Japonsko	k1gNnSc1	Japonsko
svou	svůj	k3xOyFgFnSc4	svůj
invazi	invaze	k1gFnSc4	invaze
v	v	k7c6	v
Barmě	Barma	k1gFnSc6	Barma
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
boje	boj	k1gInSc2	boj
se	se	k3xPyFc4	se
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
japonské	japonský	k2eAgFnSc2d1	japonská
armády	armáda	k1gFnSc2	armáda
zapojily	zapojit	k5eAaPmAgInP	zapojit
také	také	k9	také
dobrovolnické	dobrovolnický	k2eAgFnPc1d1	dobrovolnická
jednotky	jednotka	k1gFnPc1	jednotka
Barmánců	Barmánec	k1gMnPc2	Barmánec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
japonský	japonský	k2eAgInSc4d1	japonský
vpád	vpád	k1gInSc4	vpád
vnímali	vnímat	k5eAaImAgMnP	vnímat
jako	jako	k9	jako
příležitost	příležitost	k1gFnSc4	příležitost
k	k	k7c3	k
ukončení	ukončení	k1gNnSc3	ukončení
britské	britský	k2eAgFnSc2d1	britská
koloniální	koloniální	k2eAgFnSc2d1	koloniální
nadvlády	nadvláda	k1gFnSc2	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
japonské	japonský	k2eAgFnSc2d1	japonská
vojsko	vojsko	k1gNnSc1	vojsko
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
celé	celý	k2eAgNnSc1d1	celé
území	území	k1gNnSc1	území
Barmy	Barma	k1gFnSc2	Barma
a	a	k8xC	a
zavedlo	zavést	k5eAaPmAgNnS	zavést
zde	zde	k6eAd1	zde
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
správu	správa	k1gFnSc4	správa
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1943	[number]	k4	1943
Barma	Barma	k1gFnSc1	Barma
získala	získat	k5eAaPmAgFnS	získat
formální	formální	k2eAgFnSc4d1	formální
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vliv	vliv	k1gInSc4	vliv
v	v	k7c4	v
zemí	zem	k1gFnPc2	zem
si	se	k3xPyFc3	se
i	i	k9	i
nadále	nadále	k6eAd1	nadále
udrželo	udržet	k5eAaPmAgNnS	udržet
japonské	japonský	k2eAgNnSc4d1	Japonské
velení	velení	k1gNnSc4	velení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
pokračující	pokračující	k2eAgFnSc2d1	pokračující
okupace	okupace	k1gFnSc2	okupace
se	se	k3xPyFc4	se
v	v	k7c6	v
Barmě	Barma	k1gFnSc6	Barma
začalo	začít	k5eAaPmAgNnS	začít
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
protijaponské	protijaponský	k2eAgNnSc1d1	protijaponský
odbojové	odbojový	k2eAgNnSc1d1	odbojové
hnutí	hnutí	k1gNnSc1	hnutí
a	a	k8xC	a
přední	přední	k2eAgMnPc1d1	přední
barmští	barmský	k2eAgMnPc1d1	barmský
političtí	politický	k2eAgMnPc1d1	politický
představitelé	představitel	k1gMnPc1	představitel
se	se	k3xPyFc4	se
přiklonili	přiklonit	k5eAaPmAgMnP	přiklonit
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
spojenců	spojenec	k1gMnPc2	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
odbojová	odbojový	k2eAgFnSc1d1	odbojová
fronta	fronta	k1gFnSc1	fronta
Liga	liga	k1gFnSc1	liga
proti	proti	k7c3	proti
fašismu	fašismus	k1gInSc3	fašismus
a	a	k8xC	a
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
lidu	lid	k1gInSc2	lid
(	(	kIx(	(
<g/>
též	též	k9	též
jen	jen	k9	jen
Liga	liga	k1gFnSc1	liga
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
zahájila	zahájit	k5eAaPmAgFnS	zahájit
spojenecká	spojenecký	k2eAgFnSc1d1	spojenecká
vojska	vojsko	k1gNnSc2	vojsko
ofenzivu	ofenziva	k1gFnSc4	ofenziva
<g/>
,	,	kIx,	,
podporovanou	podporovaný	k2eAgFnSc4d1	podporovaná
místním	místní	k2eAgNnSc7d1	místní
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
,	,	kIx,	,
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
vytlačila	vytlačit	k5eAaPmAgFnS	vytlačit
japonské	japonský	k2eAgFnPc4d1	japonská
jednotky	jednotka	k1gFnPc4	jednotka
z	z	k7c2	z
Barmy	Barma	k1gFnSc2	Barma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Barmě	Barma	k1gFnSc6	Barma
byla	být	k5eAaImAgFnS	být
obnovena	obnoven	k2eAgFnSc1d1	obnovena
britská	britský	k2eAgFnSc1d1	britská
správa	správa	k1gFnSc1	správa
<g/>
,	,	kIx,	,
a	a	k8xC	a
země	zem	k1gFnPc1	zem
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
znovu	znovu	k6eAd1	znovu
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
Zpočátku	zpočátku	k6eAd1	zpočátku
nebyla	být	k5eNaImAgFnS	být
koloniální	koloniální	k2eAgFnSc1d1	koloniální
správa	správa	k1gFnSc1	správa
ochotná	ochotný	k2eAgFnSc1d1	ochotná
přenechat	přenechat	k5eAaPmF	přenechat
barmským	barmský	k2eAgMnPc3d1	barmský
politikům	politik	k1gMnPc3	politik
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
boje	boj	k1gInSc2	boj
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
se	se	k3xPyFc4	se
postavila	postavit	k5eAaPmAgFnS	postavit
Liga	liga	k1gFnSc1	liga
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
podporu	podpora	k1gFnSc4	podpora
velké	velký	k2eAgFnSc3d1	velká
části	část	k1gFnSc3	část
obyvatel	obyvatel	k1gMnPc2	obyvatel
Barmy	Barma	k1gFnSc2	Barma
<g/>
.	.	kIx.	.
</s>
<s>
Liga	liga	k1gFnSc1	liga
<g/>
,	,	kIx,	,
vedená	vedený	k2eAgFnSc1d1	vedená
Aun	Aun	k1gMnSc7	Aun
Schanem	Schan	k1gMnSc7	Schan
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
jednat	jednat	k5eAaImF	jednat
s	s	k7c7	s
vládou	vláda	k1gFnSc7	vláda
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
o	o	k7c6	o
širším	široký	k2eAgNnSc6d2	širší
zastoupení	zastoupení	k1gNnSc6	zastoupení
barmských	barmský	k2eAgMnPc2d1	barmský
politiků	politik	k1gMnPc2	politik
ve	v	k7c6	v
správě	správa	k1gFnSc6	správa
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
Lize	liga	k1gFnSc3	liga
podařilo	podařit	k5eAaPmAgNnS	podařit
vyjednat	vyjednat	k5eAaPmF	vyjednat
vznik	vznik	k1gInSc4	vznik
většinové	většinový	k2eAgFnSc2d1	většinová
barmské	barmský	k2eAgFnSc2d1	barmská
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
voleb	volba	k1gFnPc2	volba
do	do	k7c2	do
ústavodárného	ústavodárný	k2eAgNnSc2d1	Ústavodárné
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgFnPc6	který
také	také	k9	také
Liga	liga	k1gFnSc1	liga
s	s	k7c7	s
výraznou	výrazný	k2eAgFnSc7d1	výrazná
většinou	většina	k1gFnSc7	většina
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
byl	být	k5eAaImAgMnS	být
Aun	Aun	k1gMnSc1	Aun
Schan	Schan	k1gMnSc1	Schan
zastřelen	zastřelit	k5eAaPmNgMnS	zastřelit
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	nedlouho	k1gNnSc1	nedlouho
poté	poté	k6eAd1	poté
bylo	být	k5eAaImAgNnS	být
úsilí	úsilí	k1gNnSc1	úsilí
o	o	k7c4	o
získání	získání	k1gNnSc4	získání
samostatnosti	samostatnost	k1gFnSc2	samostatnost
završeno	završit	k5eAaPmNgNnS	završit
oficiální	oficiální	k2eAgFnSc7d1	oficiální
dohodou	dohoda	k1gFnSc7	dohoda
s	s	k7c7	s
vládou	vláda	k1gFnSc7	vláda
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Barmská	barmský	k2eAgFnSc1d1	barmská
nezávislost	nezávislost	k1gFnSc1	nezávislost
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
nesla	nést	k5eAaImAgFnS	nést
nový	nový	k2eAgInSc4d1	nový
oficiální	oficiální	k2eAgInSc4d1	oficiální
název	název	k1gInSc4	název
Barmský	barmský	k2eAgInSc1d1	barmský
svaz	svaz	k1gInSc1	svaz
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	let	k1gInPc6	let
1974	[number]	k4	1974
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
s	s	k7c7	s
přívlastkem	přívlastek	k1gInSc7	přívlastek
Socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
Barmský	barmský	k2eAgInSc1d1	barmský
svaz	svaz	k1gInSc1	svaz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
Barma	Barma	k1gFnSc1	Barma
získala	získat	k5eAaPmAgFnS	získat
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
prudce	prudko	k6eAd1	prudko
zhoršily	zhoršit	k5eAaPmAgInP	zhoršit
vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
vedením	vedení	k1gNnSc7	vedení
Ligy	liga	k1gFnSc2	liga
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Aun	Aun	k1gMnSc4	Aun
Schana	Schan	k1gMnSc4	Schan
vedl	vést	k5eAaImAgInS	vést
U	u	k7c2	u
Nu	nu	k9	nu
<g/>
,	,	kIx,	,
a	a	k8xC	a
komunistickou	komunistický	k2eAgFnSc7d1	komunistická
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
přistoupila	přistoupit	k5eAaPmAgFnS	přistoupit
na	na	k7c4	na
násilnou	násilný	k2eAgFnSc4d1	násilná
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
převzetí	převzetí	k1gNnSc3	převzetí
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
zahájila	zahájit	k5eAaPmAgFnS	zahájit
proti	proti	k7c3	proti
barmské	barmský	k2eAgFnSc3d1	barmská
vládě	vláda	k1gFnSc3	vláda
ozbrojený	ozbrojený	k2eAgInSc4d1	ozbrojený
odpor	odpor	k1gInSc4	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
nastalého	nastalý	k2eAgInSc2d1	nastalý
konfliktu	konflikt	k1gInSc2	konflikt
se	se	k3xPyFc4	se
záhy	záhy	k6eAd1	záhy
zapojily	zapojit	k5eAaPmAgFnP	zapojit
také	také	k9	také
některé	některý	k3yIgFnPc1	některý
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
organizace	organizace	k1gFnPc1	organizace
etnických	etnický	k2eAgFnPc2d1	etnická
menšin	menšina	k1gFnPc2	menšina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
usilovaly	usilovat	k5eAaImAgFnP	usilovat
o	o	k7c4	o
větší	veliký	k2eAgFnSc4d2	veliký
míru	míra	k1gFnSc4	míra
samosprávy	samospráva	k1gFnSc2	samospráva
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
ponořila	ponořit	k5eAaPmAgFnS	ponořit
do	do	k7c2	do
vleklé	vleklý	k2eAgFnSc2d1	vleklá
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
počátečních	počáteční	k2eAgInPc6d1	počáteční
letech	let	k1gInPc6	let
nejprudších	prudký	k2eAgInPc2d3	nejprudší
bojů	boj	k1gInPc2	boj
přesunula	přesunout	k5eAaPmAgFnS	přesunout
do	do	k7c2	do
pohraničních	pohraniční	k2eAgFnPc2d1	pohraniční
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
pak	pak	k6eAd1	pak
probíhala	probíhat	k5eAaImAgFnS	probíhat
po	po	k7c4	po
několik	několik	k4yIc4	několik
dalších	další	k2eAgNnPc2d1	další
desetiletí	desetiletí	k1gNnPc2	desetiletí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
důležitosti	důležitost	k1gFnSc6	důležitost
tak	tak	k6eAd1	tak
nabyla	nabýt	k5eAaPmAgFnS	nabýt
barmská	barmský	k2eAgFnSc1d1	barmská
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nezbytnou	zbytný	k2eNgFnSc7d1	zbytný
pro	pro	k7c4	pro
zachování	zachování	k1gNnSc4	zachování
autority	autorita	k1gFnSc2	autorita
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Liga	liga	k1gFnSc1	liga
si	se	k3xPyFc3	se
i	i	k9	i
nadále	nadále	k6eAd1	nadále
udržela	udržet	k5eAaPmAgFnS	udržet
pozici	pozice	k1gFnSc4	pozice
nejsilnější	silný	k2eAgFnSc2d3	nejsilnější
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
pokračující	pokračující	k2eAgFnSc3d1	pokračující
občanské	občanský	k2eAgFnSc3d1	občanská
válce	válka	k1gFnSc3	válka
Liga	liga	k1gFnSc1	liga
neztratila	ztratit	k5eNaPmAgFnS	ztratit
podporu	podpora	k1gFnSc4	podpora
barmských	barmský	k2eAgMnPc2d1	barmský
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
parlamentních	parlamentní	k2eAgFnPc2d1	parlamentní
voleb	volba	k1gFnPc2	volba
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Názorové	názorový	k2eAgInPc1d1	názorový
spory	spor	k1gInPc1	spor
se	se	k3xPyFc4	se
časem	časem	k6eAd1	časem
objevily	objevit	k5eAaPmAgFnP	objevit
také	také	k9	také
uvnitř	uvnitř	k7c2	uvnitř
samotné	samotný	k2eAgFnSc2d1	samotná
Ligy	liga	k1gFnSc2	liga
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
rozštěpila	rozštěpit	k5eAaPmAgFnS	rozštěpit
na	na	k7c4	na
několik	několik	k4yIc4	několik
proudů	proud	k1gInPc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Rozkol	rozkol	k1gInSc1	rozkol
v	v	k7c6	v
Lize	liga	k1gFnSc6	liga
vyústil	vyústit	k5eAaPmAgInS	vyústit
ve	v	k7c6	v
vyostření	vyostření	k1gNnSc6	vyostření
vztahů	vztah	k1gInPc2	vztah
s	s	k7c7	s
velením	velení	k1gNnSc7	velení
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
obávalo	obávat	k5eAaImAgNnS	obávat
ztráty	ztráta	k1gFnSc2	ztráta
stability	stabilita	k1gFnSc2	stabilita
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Napětí	napětí	k1gNnSc1	napětí
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS	vyvrcholit
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
armádní	armádní	k2eAgNnSc1d1	armádní
velení	velení	k1gNnSc1	velení
převzalo	převzít	k5eAaPmAgNnS	převzít
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
generál	generál	k1gMnSc1	generál
Nei	Nei	k1gMnSc1	Nei
Win	Win	k1gMnSc1	Win
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
vláda	vláda	k1gFnSc1	vláda
měla	mít	k5eAaImAgFnS	mít
obnovit	obnovit	k5eAaPmF	obnovit
pořádek	pořádek	k1gInSc4	pořádek
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
připravit	připravit	k5eAaPmF	připravit
konání	konání	k1gNnSc4	konání
nových	nový	k2eAgFnPc2d1	nová
parlamentních	parlamentní	k2eAgFnPc2d1	parlamentní
voleb	volba	k1gFnPc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgFnPc6d1	následující
volbách	volba	k1gFnPc6	volba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
stran	strana	k1gFnPc2	strana
vzniknuvších	vzniknuvší	k2eAgFnPc2d1	vzniknuvší
rozpadem	rozpad	k1gInSc7	rozpad
Ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
této	tento	k3xDgFnSc2	tento
strany	strana	k1gFnSc2	strana
stál	stát	k5eAaImAgInS	stát
U	u	k7c2	u
Nu	nu	k9	nu
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
ale	ale	k8xC	ale
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
výraznějšímu	výrazný	k2eAgInSc3d2	výraznější
obratu	obrat	k1gInSc3	obrat
v	v	k7c6	v
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
ani	ani	k8xC	ani
posílení	posílení	k1gNnSc4	posílení
stability	stabilita	k1gFnSc2	stabilita
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
opětovnému	opětovný	k2eAgInSc3d1	opětovný
vojenskému	vojenský	k2eAgInSc3d1	vojenský
převratu	převrat	k1gInSc3	převrat
<g/>
.	.	kIx.	.
</s>
<s>
Vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
uchopila	uchopit	k5eAaPmAgFnS	uchopit
Revoluční	revoluční	k2eAgFnSc1d1	revoluční
rada	rada	k1gFnSc1	rada
<g/>
,	,	kIx,	,
vedená	vedený	k2eAgFnSc1d1	vedená
Nei	Nei	k1gMnSc7	Nei
Winem	Win	k1gMnSc7	Win
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
platnost	platnost	k1gFnSc1	platnost
ústavy	ústava	k1gFnSc2	ústava
a	a	k8xC	a
rozpuštěn	rozpuštěn	k2eAgInSc1d1	rozpuštěn
parlament	parlament	k1gInSc1	parlament
i	i	k8xC	i
vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
veškerou	veškerý	k3xTgFnSc4	veškerý
zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
moc	moc	k1gFnSc4	moc
i	i	k8xC	i
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
moc	moc	k1gFnSc4	moc
převzala	převzít	k5eAaPmAgFnS	převzít
Revoluční	revoluční	k2eAgFnSc1d1	revoluční
rada	rada	k1gFnSc1	rada
<g/>
,	,	kIx,	,
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
armádních	armádní	k2eAgMnPc2d1	armádní
velitelů	velitel	k1gMnPc2	velitel
<g/>
.	.	kIx.	.
</s>
<s>
Revoluční	revoluční	k2eAgFnSc1d1	revoluční
rada	rada	k1gFnSc1	rada
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
program	program	k1gInSc4	program
Barmské	barmský	k2eAgFnSc2d1	barmská
cesty	cesta	k1gFnSc2	cesta
k	k	k7c3	k
socialismu	socialismus	k1gInSc3	socialismus
<g/>
,	,	kIx,	,
zrušila	zrušit	k5eAaPmAgFnS	zrušit
stávající	stávající	k2eAgNnSc4d1	stávající
administrativní	administrativní	k2eAgNnSc4d1	administrativní
rozdělení	rozdělení	k1gNnSc4	rozdělení
země	zem	k1gFnSc2	zem
a	a	k8xC	a
pozměnila	pozměnit	k5eAaPmAgFnS	pozměnit
i	i	k9	i
celý	celý	k2eAgInSc4d1	celý
aparát	aparát	k1gInSc4	aparát
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
začala	začít	k5eAaPmAgFnS	začít
nově	nově	k6eAd1	nově
působit	působit	k5eAaImF	působit
řada	řada	k1gFnSc1	řada
příslušníků	příslušník	k1gMnPc2	příslušník
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Revoluční	revoluční	k2eAgFnSc1d1	revoluční
rada	rada	k1gFnSc1	rada
zahájila	zahájit	k5eAaPmAgFnS	zahájit
rozhovory	rozhovor	k1gInPc4	rozhovor
se	s	k7c7	s
zástupci	zástupce	k1gMnPc7	zástupce
protivládních	protivládní	k2eAgFnPc2d1	protivládní
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Jednání	jednání	k1gNnSc1	jednání
ale	ale	k8xC	ale
nepřinesla	přinést	k5eNaPmAgFnS	přinést
žádný	žádný	k3yNgInSc4	žádný
výsledek	výsledek	k1gInSc4	výsledek
a	a	k8xC	a
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
opět	opět	k6eAd1	opět
obnovena	obnoven	k2eAgFnSc1d1	obnovena
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
vedla	vést	k5eAaImAgFnS	vést
Revoluční	revoluční	k2eAgFnSc1d1	revoluční
rada	rada	k1gFnSc1	rada
jednání	jednání	k1gNnSc2	jednání
o	o	k7c6	o
spolupráci	spolupráce	k1gFnSc6	spolupráce
také	také	k9	také
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
politickými	politický	k2eAgFnPc7d1	politická
stranami	strana	k1gFnPc7	strana
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
ale	ale	k9	ale
nabízenou	nabízený	k2eAgFnSc4d1	nabízená
účast	účast	k1gFnSc4	účast
odmítly	odmítnout	k5eAaPmAgInP	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
si	se	k3xPyFc3	se
Revoluční	revoluční	k2eAgFnSc1d1	revoluční
rada	rada	k1gFnSc1	rada
založila	založit	k5eAaPmAgFnS	založit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
Stranu	strana	k1gFnSc4	strana
barmského	barmský	k2eAgInSc2d1	barmský
socialistického	socialistický	k2eAgInSc2d1	socialistický
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
zákazu	zákaz	k1gInSc6	zákaz
činnosti	činnost	k1gFnSc2	činnost
všech	všecek	k3xTgFnPc2	všecek
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
stala	stát	k5eAaPmAgFnS	stát
jedinou	jediný	k2eAgFnSc7d1	jediná
povolenou	povolený	k2eAgFnSc7d1	povolená
politickou	politický	k2eAgFnSc7d1	politická
stranou	strana	k1gFnSc7	strana
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yInSc7	co
dál	daleko	k6eAd2	daleko
častěji	často	k6eAd2	často
zaznívala	zaznívat	k5eAaImAgFnS	zaznívat
otevřená	otevřený	k2eAgFnSc1d1	otevřená
kritika	kritika	k1gFnSc1	kritika
vojenské	vojenský	k2eAgFnSc2d1	vojenská
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Revoluční	revoluční	k2eAgFnSc1d1	revoluční
rada	rada	k1gFnSc1	rada
reagovala	reagovat	k5eAaBmAgFnS	reagovat
rozsáhlým	rozsáhlý	k2eAgNnSc7d1	rozsáhlé
zatýkáním	zatýkání	k1gNnSc7	zatýkání
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
opozičních	opoziční	k2eAgMnPc2d1	opoziční
politiků	politik	k1gMnPc2	politik
<g/>
,	,	kIx,	,
novinářů	novinář	k1gMnPc2	novinář
i	i	k8xC	i
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
byla	být	k5eAaImAgFnS	být
zavedena	zaveden	k2eAgFnSc1d1	zavedena
cenzura	cenzura	k1gFnSc1	cenzura
<g/>
.	.	kIx.	.
</s>
<s>
Revoluční	revoluční	k2eAgFnSc1d1	revoluční
rada	rada	k1gFnSc1	rada
upevňovala	upevňovat	k5eAaImAgFnS	upevňovat
centralizovanou	centralizovaný	k2eAgFnSc4d1	centralizovaná
státní	státní	k2eAgFnSc4d1	státní
správu	správa	k1gFnSc4	správa
a	a	k8xC	a
posilovala	posilovat	k5eAaImAgFnS	posilovat
svou	svůj	k3xOyFgFnSc4	svůj
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
oblastmi	oblast	k1gFnPc7	oblast
etnických	etnický	k2eAgFnPc2d1	etnická
menšin	menšina	k1gFnPc2	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c6	v
Barmě	Barma	k1gFnSc6	Barma
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
nová	nový	k2eAgFnSc1d1	nová
ústava	ústava	k1gFnSc1	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
této	tento	k3xDgFnSc2	tento
ústavy	ústava	k1gFnSc2	ústava
byla	být	k5eAaImAgFnS	být
země	země	k1gFnSc1	země
nově	nově	k6eAd1	nově
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
na	na	k7c4	na
Socialistickou	socialistický	k2eAgFnSc4d1	socialistická
republiku	republika	k1gFnSc4	republika
Barmský	barmský	k2eAgInSc4d1	barmský
svaz	svaz	k1gInSc4	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
existenci	existence	k1gFnSc4	existence
jen	jen	k6eAd1	jen
jediné	jediný	k2eAgFnSc2d1	jediná
politické	politický	k2eAgFnSc2d1	politická
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
–	–	k?	–
vládní	vládní	k2eAgFnSc2d1	vládní
Strany	strana	k1gFnSc2	strana
barmského	barmský	k2eAgInSc2d1	barmský
socialistického	socialistický	k2eAgInSc2d1	socialistický
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
Barmy	Barma	k1gFnSc2	Barma
bylo	být	k5eAaImAgNnS	být
administrativně	administrativně	k6eAd1	administrativně
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
čtrnácti	čtrnáct	k4xCc2	čtrnáct
správních	správní	k2eAgInPc2d1	správní
celků	celek	k1gInPc2	celek
–	–	k?	–
sedmi	sedm	k4xCc2	sedm
oblastí	oblast	k1gFnPc2	oblast
a	a	k8xC	a
sedmi	sedm	k4xCc2	sedm
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
i	i	k8xC	i
státech	stát	k1gInPc6	stát
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
obdobný	obdobný	k2eAgInSc1d1	obdobný
správní	správní	k2eAgInSc1d1	správní
aparát	aparát	k1gInSc1	aparát
plně	plně	k6eAd1	plně
podřízený	podřízený	k2eAgInSc1d1	podřízený
vládě	vláda	k1gFnSc3	vláda
Barmského	barmský	k2eAgInSc2d1	barmský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Státy	stát	k1gInPc1	stát
na	na	k7c4	na
území	území	k1gNnSc4	území
hlavních	hlavní	k2eAgFnPc2d1	hlavní
etnických	etnický	k2eAgFnPc2d1	etnická
menšin	menšina	k1gFnPc2	menšina
(	(	kIx(	(
<g/>
Kačjinský	Kačjinský	k2eAgInSc4d1	Kačjinský
<g/>
,	,	kIx,	,
Šanský	Šanský	k2eAgInSc4d1	Šanský
<g/>
,	,	kIx,	,
Čjinský	Čjinský	k2eAgInSc4d1	Čjinský
<g/>
,	,	kIx,	,
Rakhinský	Rakhinský	k2eAgInSc4d1	Rakhinský
<g/>
,	,	kIx,	,
Karenský	karenský	k2eAgInSc4d1	karenský
<g/>
,	,	kIx,	,
Kajaský	Kajaský	k2eAgInSc4d1	Kajaský
a	a	k8xC	a
Monský	Monský	k2eAgInSc4d1	Monský
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
částečně	částečně	k6eAd1	částečně
mimo	mimo	k7c4	mimo
vládní	vládní	k2eAgFnSc4d1	vládní
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
okrajová	okrajový	k2eAgNnPc1d1	okrajové
území	území	k1gNnPc1	území
Barmy	Barma	k1gFnSc2	Barma
ovládaly	ovládat	k5eAaImAgFnP	ovládat
povstalecké	povstalecký	k2eAgFnPc1d1	povstalecká
skupiny	skupina	k1gFnPc1	skupina
etnických	etnický	k2eAgFnPc2d1	etnická
menšin	menšina	k1gFnPc2	menšina
bojující	bojující	k2eAgFnSc1d1	bojující
s	s	k7c7	s
centrální	centrální	k2eAgFnSc7d1	centrální
vládou	vláda	k1gFnSc7	vláda
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
pruducenti	pruducent	k1gMnPc1	pruducent
opia	opium	k1gNnSc2	opium
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
tzv.	tzv.	kA	tzv.
Zlatého	zlatý	k2eAgInSc2d1	zlatý
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
nespokojenost	nespokojenost	k1gFnSc1	nespokojenost
barmských	barmský	k2eAgMnPc2d1	barmský
obyvatel	obyvatel	k1gMnPc2	obyvatel
s	s	k7c7	s
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
vládou	vláda	k1gFnSc7	vláda
a	a	k8xC	a
situací	situace	k1gFnSc7	situace
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
zemi	zem	k1gFnSc6	zem
zachvátily	zachvátit	k5eAaPmAgFnP	zachvátit
mohutné	mohutný	k2eAgFnPc1d1	mohutná
demonstrace	demonstrace	k1gFnPc1	demonstrace
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
vyšli	vyjít	k5eAaPmAgMnP	vyjít
do	do	k7c2	do
ulic	ulice	k1gFnPc2	ulice
a	a	k8xC	a
požadovali	požadovat	k5eAaImAgMnP	požadovat
odstoupení	odstoupení	k1gNnSc4	odstoupení
režimu	režim	k1gInSc2	režim
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
uvrhl	uvrhnout	k5eAaPmAgInS	uvrhnout
zemi	zem	k1gFnSc4	zem
do	do	k7c2	do
bídy	bída	k1gFnSc2	bída
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
demonstracím	demonstrace	k1gFnPc3	demonstrace
byly	být	k5eAaImAgFnP	být
nasazeny	nasazen	k2eAgFnPc1d1	nasazena
jednotky	jednotka	k1gFnPc1	jednotka
barmské	barmský	k2eAgFnSc2d1	barmská
policie	policie	k1gFnSc2	policie
a	a	k8xC	a
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
tvrdé	tvrdý	k2eAgInPc1d1	tvrdý
zásahy	zásah	k1gInPc1	zásah
si	se	k3xPyFc3	se
vyžádaly	vyžádat	k5eAaPmAgInP	vyžádat
několik	několik	k4yIc4	několik
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Generál	generál	k1gMnSc1	generál
New	New	k1gMnSc1	New
Win	Win	k1gMnSc1	Win
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
funkce	funkce	k1gFnSc2	funkce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
poté	poté	k6eAd1	poté
protesty	protest	k1gInPc1	protest
pokračovaly	pokračovat	k5eAaImAgInP	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
popularitě	popularita	k1gFnSc6	popularita
nabývala	nabývat	k5eAaImAgFnS	nabývat
především	především	k9	především
Aun	Aun	k1gFnSc1	Aun
Schan	Schany	k1gInPc2	Schany
Su	Su	k?	Su
Ťij	Ťij	k1gFnSc1	Ťij
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
letech	léto	k1gNnPc6	léto
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
Barmy	Barma	k1gFnSc2	Barma
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
starala	starat	k5eAaImAgFnS	starat
o	o	k7c4	o
nemocnou	mocný	k2eNgFnSc4d1	mocný
matku	matka	k1gFnSc4	matka
<g/>
.	.	kIx.	.
</s>
<s>
Náhle	náhle	k6eAd1	náhle
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vojenskému	vojenský	k2eAgInSc3d1	vojenský
převratu	převrat	k1gInSc3	převrat
a	a	k8xC	a
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
uchopila	uchopit	k5eAaPmAgFnS	uchopit
nová	nový	k2eAgFnSc1d1	nová
vojenská	vojenský	k2eAgFnSc1d1	vojenská
junta	junta	k1gFnSc1	junta
<g/>
.	.	kIx.	.
</s>
<s>
Junta	junta	k1gFnSc1	junta
přislíbila	přislíbit	k5eAaPmAgFnS	přislíbit
konání	konání	k1gNnSc4	konání
voleb	volba	k1gFnPc2	volba
do	do	k7c2	do
zákonodárného	zákonodárný	k2eAgNnSc2d1	zákonodárné
shromáždění	shromáždění	k1gNnSc2	shromáždění
a	a	k8xC	a
povolila	povolit	k5eAaPmAgFnS	povolit
zakládání	zakládání	k1gNnSc4	zakládání
nových	nový	k2eAgFnPc2d1	nová
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Aun	Aun	k?	Aun
Schan	Schan	k1gMnSc1	Schan
Su	Su	k?	Su
Ťij	Ťij	k1gMnSc1	Ťij
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
představiteli	představitel	k1gMnPc7	představitel
opozičního	opoziční	k2eAgNnSc2d1	opoziční
hnutí	hnutí	k1gNnSc2	hnutí
založila	založit	k5eAaPmAgFnS	založit
stranu	strana	k1gFnSc4	strana
Národní	národní	k2eAgFnSc1d1	národní
liga	liga	k1gFnSc1	liga
pro	pro	k7c4	pro
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
Aun	Aun	k1gFnSc4	Aun
Schan	Schany	k1gInPc2	Schany
Su	Su	k?	Su
Ťij	Ťij	k1gFnSc1	Ťij
internována	internován	k2eAgFnSc1d1	internována
v	v	k7c6	v
domácím	domácí	k2eAgNnSc6d1	domácí
vězení	vězení	k1gNnSc6	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Junta	junta	k1gFnSc1	junta
také	také	k9	také
změnila	změnit	k5eAaPmAgFnS	změnit
oficiální	oficiální	k2eAgInSc4d1	oficiální
název	název	k1gInSc4	název
státu	stát	k1gInSc2	stát
na	na	k7c4	na
Myanmarský	Myanmarský	k2eAgInSc4d1	Myanmarský
svaz	svaz	k1gInSc4	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Myanmar	Myanmar	k1gInSc1	Myanmar
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
transkripce	transkripce	k1gFnSc1	transkripce
jména	jméno	k1gNnSc2	jméno
země	zem	k1gFnSc2	zem
v	v	k7c6	v
barmštině	barmština	k1gFnSc6	barmština
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
a	a	k8xC	a
jejich	jejich	k3xOp3gMnSc7	jejich
jednoznačným	jednoznačný	k2eAgMnSc7d1	jednoznačný
vítězem	vítěz	k1gMnSc7	vítěz
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Národní	národní	k2eAgFnSc1d1	národní
liga	liga	k1gFnSc1	liga
pro	pro	k7c4	pro
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Následná	následný	k2eAgFnSc1d1	následná
reakce	reakce	k1gFnSc1	reakce
junty	junta	k1gFnSc2	junta
byla	být	k5eAaImAgFnS	být
překvapující	překvapující	k2eAgFnSc1d1	překvapující
–	–	k?	–
junta	junta	k1gFnSc1	junta
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
uznat	uznat	k5eAaPmF	uznat
výsledky	výsledek	k1gInPc4	výsledek
voleb	volba	k1gFnPc2	volba
a	a	k8xC	a
předat	předat	k5eAaPmF	předat
vládu	vláda	k1gFnSc4	vláda
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
zvolených	zvolený	k2eAgMnPc2d1	zvolený
zástupců	zástupce	k1gMnPc2	zástupce
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
byla	být	k5eAaImAgFnS	být
Aun	Aun	k1gFnSc1	Aun
Schan	Schana	k1gFnPc2	Schana
Su	Su	k?	Su
Ťij	Ťij	k1gMnPc2	Ťij
udělena	udělit	k5eAaPmNgFnS	udělit
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
Aun	Aun	k?	Aun
Schan	Schan	k1gInSc1	Schan
Su	Su	k?	Su
Ťij	Ťij	k1gFnSc2	Ťij
strávila	strávit	k5eAaPmAgFnS	strávit
v	v	k7c6	v
domácím	domácí	k2eAgNnSc6d1	domácí
vězení	vězení	k1gNnSc6	vězení
většinu	většina	k1gFnSc4	většina
z	z	k7c2	z
posledních	poslední	k2eAgNnPc2d1	poslední
dvaceti	dvacet	k4xCc2	dvacet
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
vypukly	vypuknout	k5eAaPmAgFnP	vypuknout
v	v	k7c6	v
Barmě	Barma	k1gFnSc6	Barma
lidové	lidový	k2eAgInPc4d1	lidový
protesty	protest	k1gInPc4	protest
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
bezprostřední	bezprostřední	k2eAgFnSc7d1	bezprostřední
příčinou	příčina	k1gFnSc7	příčina
bylo	být	k5eAaImAgNnS	být
skokové	skokový	k2eAgNnSc1d1	skokové
zvýšení	zvýšení	k1gNnSc1	zvýšení
cen	cena	k1gFnPc2	cena
pohonných	pohonný	k2eAgFnPc2d1	pohonná
hmot	hmota	k1gFnPc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
vyšli	vyjít	k5eAaPmAgMnP	vyjít
do	do	k7c2	do
ulic	ulice	k1gFnPc2	ulice
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
do	do	k7c2	do
protestů	protest	k1gInPc2	protest
zapojili	zapojit	k5eAaPmAgMnP	zapojit
také	také	k9	také
buddhističtí	buddhistický	k2eAgMnPc1d1	buddhistický
mniši	mnich	k1gMnPc1	mnich
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
podle	podle	k7c2	podle
šafránové	šafránový	k2eAgFnSc2d1	šafránová
barvy	barva	k1gFnSc2	barva
oděvů	oděv	k1gInPc2	oděv
buddhistických	buddhistický	k2eAgInPc2d1	buddhistický
mnichů	mnich	k1gInPc2	mnich
získaly	získat	k5eAaPmAgFnP	získat
tyto	tento	k3xDgFnPc1	tento
demonstrace	demonstrace	k1gFnPc1	demonstrace
označení	označení	k1gNnSc2	označení
šafránová	šafránový	k2eAgFnSc1d1	šafránová
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tvrdém	tvrdý	k2eAgInSc6d1	tvrdý
zásahu	zásah	k1gInSc6	zásah
jednotek	jednotka	k1gFnPc2	jednotka
barmské	barmský	k2eAgFnSc2d1	barmská
policie	policie	k1gFnSc2	policie
a	a	k8xC	a
armády	armáda	k1gFnSc2	armáda
byly	být	k5eAaImAgInP	být
protesty	protest	k1gInPc1	protest
potlačeny	potlačit	k5eAaPmNgInP	potlačit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2008	[number]	k4	2008
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
a	a	k8xC	a
zdevastoval	zdevastovat	k5eAaPmAgInS	zdevastovat
deltu	delta	k1gFnSc4	delta
řeky	řeka	k1gFnSc2	řeka
Iravádí	Iravádí	k1gNnSc2	Iravádí
cyklon	cyklon	k1gInSc1	cyklon
Nargis	Nargis	k1gFnSc2	Nargis
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
si	se	k3xPyFc3	se
vyžádal	vyžádat	k5eAaPmAgInS	vyžádat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
obětí	oběť	k1gFnPc2	oběť
a	a	k8xC	a
připravil	připravit	k5eAaPmAgMnS	připravit
přes	přes	k7c4	přes
milion	milion	k4xCgInSc4	milion
lidí	člověk	k1gMnPc2	člověk
o	o	k7c4	o
domov	domov	k1gInSc4	domov
<g/>
.	.	kIx.	.
</s>
<s>
Junta	junta	k1gFnSc1	junta
zpočátku	zpočátku	k6eAd1	zpočátku
bránila	bránit	k5eAaImAgFnS	bránit
vstupu	vstup	k1gInSc3	vstup
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
humanitárních	humanitární	k2eAgMnPc2d1	humanitární
pracovníků	pracovník	k1gMnPc2	pracovník
do	do	k7c2	do
země	zem	k1gFnSc2	zem
a	a	k8xC	a
blokovala	blokovat	k5eAaImAgFnS	blokovat
distribuci	distribuce	k1gFnSc4	distribuce
materiální	materiální	k2eAgFnSc2d1	materiální
humanitární	humanitární	k2eAgFnSc2d1	humanitární
pomoci	pomoc	k1gFnSc2	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnSc2d1	Česká
humanitární	humanitární	k2eAgFnSc2d1	humanitární
organizace	organizace	k1gFnSc2	organizace
(	(	kIx(	(
<g/>
Člověk	člověk	k1gMnSc1	člověk
v	v	k7c6	v
tísni	tíseň	k1gFnSc6	tíseň
<g/>
,	,	kIx,	,
ADRA	ADRA	kA	ADRA
<g/>
)	)	kIx)	)
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
působily	působit	k5eAaImAgFnP	působit
skrze	skrze	k?	skrze
své	svůj	k3xOyFgMnPc4	svůj
lokální	lokální	k2eAgMnPc4d1	lokální
partnery	partner	k1gMnPc4	partner
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
došlo	dojít	k5eAaPmAgNnS	dojít
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
voleb	volba	k1gFnPc2	volba
k	k	k7c3	k
formální	formální	k2eAgFnSc3d1	formální
změně	změna	k1gFnSc3	změna
státního	státní	k2eAgNnSc2d1	státní
zřízení	zřízení	k1gNnSc2	zřízení
na	na	k7c4	na
prezidentskou	prezidentský	k2eAgFnSc4d1	prezidentská
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
Zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
moc	moc	k1gFnSc4	moc
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
dvoukomorový	dvoukomorový	k2eAgInSc1d1	dvoukomorový
parlament	parlament	k1gInSc1	parlament
<g/>
,	,	kIx,	,
výkonná	výkonný	k2eAgFnSc1d1	výkonná
moc	moc	k1gFnSc1	moc
náleží	náležet	k5eAaImIp3nS	náležet
vládě	vláda	k1gFnSc3	vláda
složené	složený	k2eAgFnSc3d1	složená
ze	z	k7c2	z
30	[number]	k4	30
ministrů	ministr	k1gMnPc2	ministr
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
svazové	svazový	k2eAgFnSc2d1	svazová
solidarity	solidarita	k1gFnSc2	solidarita
a	a	k8xC	a
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
jednoznačným	jednoznačný	k2eAgMnSc7d1	jednoznačný
vítězem	vítěz	k1gMnSc7	vítěz
voleb	volba	k1gFnPc2	volba
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
úzce	úzko	k6eAd1	úzko
spjata	spjat	k2eAgFnSc1d1	spjata
s	s	k7c7	s
armádou	armáda	k1gFnSc7	armáda
a	a	k8xC	a
do	do	k7c2	do
pozadí	pozadí	k1gNnSc2	pozadí
odstupující	odstupující	k2eAgFnSc7d1	odstupující
juntou	junta	k1gFnSc7	junta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
se	se	k3xPyFc4	se
svých	svůj	k3xOyFgFnPc2	svůj
funkcí	funkce	k1gFnPc2	funkce
ujalo	ujmout	k5eAaPmAgNnS	ujmout
30	[number]	k4	30
ministrů	ministr	k1gMnPc2	ministr
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
pouze	pouze	k6eAd1	pouze
4	[number]	k4	4
nemají	mít	k5eNaImIp3nP	mít
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
minulost	minulost	k1gFnSc4	minulost
a	a	k8xC	a
rovná	rovný	k2eAgFnSc1d1	rovná
polovina	polovina	k1gFnSc1	polovina
působila	působit	k5eAaImAgFnS	působit
i	i	k9	i
v	v	k7c6	v
předešlé	předešlý	k2eAgFnSc6d1	předešlá
vojenské	vojenský	k2eAgFnSc6d1	vojenská
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Novým	nový	k2eAgMnSc7d1	nový
prezidentem	prezident	k1gMnSc7	prezident
země	zem	k1gFnSc2	zem
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
Thein	thein	k1gInSc1	thein
Sein	Seina	k1gFnPc2	Seina
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zastával	zastávat	k5eAaImAgMnS	zastávat
funkci	funkce	k1gFnSc4	funkce
premiéra	premiér	k1gMnSc2	premiér
v	v	k7c6	v
předchozí	předchozí	k2eAgFnSc6d1	předchozí
"	"	kIx"	"
<g/>
loutkové	loutkový	k2eAgFnSc6d1	loutková
<g/>
"	"	kIx"	"
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Boj	boj	k1gInSc1	boj
mezi	mezi	k7c7	mezi
myanmarskou	myanmarský	k2eAgFnSc7d1	myanmarská
armádou	armáda	k1gFnSc7	armáda
a	a	k8xC	a
ozbrojenými	ozbrojený	k2eAgFnPc7d1	ozbrojená
organizacemi	organizace	k1gFnPc7	organizace
Karenů	Karen	k1gMnPc2	Karen
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
karenské	karenský	k2eAgFnPc1d1	karenský
organizace	organizace	k1gFnPc1	organizace
(	(	kIx(	(
<g/>
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
Karenský	karenský	k2eAgInSc1d1	karenský
národní	národní	k2eAgInSc1d1	národní
svaz	svaz	k1gInSc1	svaz
<g/>
)	)	kIx)	)
dosud	dosud	k6eAd1	dosud
nepřestaly	přestat	k5eNaPmAgFnP	přestat
bojovat	bojovat	k5eAaImF	bojovat
za	za	k7c4	za
větší	veliký	k2eAgFnSc4d2	veliký
míru	míra	k1gFnSc4	míra
samosprávy	samospráva	k1gFnSc2	samospráva
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
až	až	k9	až
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Boje	boj	k1gInPc1	boj
probíhají	probíhat	k5eAaImIp3nP	probíhat
v	v	k7c6	v
odlehlých	odlehlý	k2eAgFnPc6d1	odlehlá
oblastech	oblast	k1gFnPc6	oblast
Karenského	karenský	k2eAgInSc2d1	karenský
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
hraničícího	hraničící	k2eAgInSc2d1	hraničící
s	s	k7c7	s
Thajskem	Thajsko	k1gNnSc7	Thajsko
<g/>
.	.	kIx.	.
</s>
<s>
Postup	postup	k1gInSc1	postup
myanmarských	myanmarský	k2eAgMnPc2d1	myanmarský
vojáků	voják	k1gMnPc2	voják
proti	proti	k7c3	proti
karenským	karenský	k2eAgMnPc3d1	karenský
civilistům	civilista	k1gMnPc3	civilista
bývá	bývat	k5eAaImIp3nS	bývat
nekompromisní	kompromisní	k2eNgMnSc1d1	nekompromisní
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
až	až	k9	až
o	o	k7c6	o
etnické	etnický	k2eAgFnSc6d1	etnická
čistce	čistka	k1gFnSc6	čistka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Útoky	útok	k1gInPc1	útok
myanmarské	myanmarský	k2eAgFnSc2d1	myanmarská
armády	armáda	k1gFnSc2	armáda
jsou	být	k5eAaImIp3nP	být
provázeny	provázet	k5eAaImNgFnP	provázet
vypalováním	vypalování	k1gNnSc7	vypalování
vesnic	vesnice	k1gFnPc2	vesnice
<g/>
,	,	kIx,	,
znásilňováním	znásilňování	k1gNnSc7	znásilňování
a	a	k8xC	a
zabíjením	zabíjení	k1gNnSc7	zabíjení
civilistů	civilista	k1gMnPc2	civilista
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
velmi	velmi	k6eAd1	velmi
tvrdého	tvrdý	k2eAgNnSc2d1	tvrdé
potlačování	potlačování	k1gNnSc2	potlačování
politické	politický	k2eAgFnSc2d1	politická
opozice	opozice	k1gFnSc2	opozice
i	i	k8xC	i
zmíněného	zmíněný	k2eAgInSc2d1	zmíněný
vojenského	vojenský	k2eAgInSc2d1	vojenský
postupu	postup	k1gInSc2	postup
v	v	k7c6	v
Karenském	karenský	k2eAgInSc6d1	karenský
státě	stát	k1gInSc6	stát
je	být	k5eAaImIp3nS	být
barmská	barmský	k2eAgFnSc1d1	barmská
junta	junta	k1gFnSc1	junta
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
za	za	k7c4	za
systematické	systematický	k2eAgNnSc4d1	systematické
porušování	porušování	k1gNnSc4	porušování
základních	základní	k2eAgNnPc2d1	základní
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
obnoven	obnovit	k5eAaPmNgInS	obnovit
kačjinský	kačjinský	k2eAgInSc1d1	kačjinský
konflikt	konflikt	k1gInSc1	konflikt
a	a	k8xC	a
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2015	[number]	k4	2015
zahájila	zahájit	k5eAaPmAgFnS	zahájit
barmská	barmský	k2eAgFnSc1d1	barmská
armáda	armáda	k1gFnSc1	armáda
další	další	k2eAgFnSc4d1	další
ofenzivu	ofenziva	k1gFnSc4	ofenziva
na	na	k7c6	na
území	území	k1gNnSc6	území
Kačjinského	Kačjinský	k2eAgInSc2d1	Kačjinský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
několika	několik	k4yIc3	několik
pogromům	pogrom	k1gInPc3	pogrom
proti	proti	k7c3	proti
menšině	menšina	k1gFnSc3	menšina
muslimských	muslimský	k2eAgMnPc2d1	muslimský
Rohingyů	Rohingy	k1gMnPc2	Rohingy
<g/>
.	.	kIx.	.
</s>
<s>
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
junta	junta	k1gFnSc1	junta
měla	mít	k5eAaImAgFnS	mít
politickou	politický	k2eAgFnSc4d1	politická
podporu	podpora	k1gFnSc4	podpora
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
pohraničním	pohraniční	k2eAgInPc3d1	pohraniční
incidentům	incident	k1gInPc3	incident
a	a	k8xC	a
Myanmar	Myanmar	k1gMnSc1	Myanmar
obvinil	obvinit	k5eAaPmAgMnS	obvinit
Čínu	Čína	k1gFnSc4	Čína
z	z	k7c2	z
podpory	podpora	k1gFnSc2	podpora
čínských	čínský	k2eAgMnPc2d1	čínský
etnických	etnický	k2eAgMnPc2d1	etnický
rebelů	rebel	k1gMnPc2	rebel
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Kokang	Kokanga	k1gFnPc2	Kokanga
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
také	také	k9	také
s	s	k7c7	s
některými	některý	k3yIgInPc7	některý
západními	západní	k2eAgInPc7d1	západní
koncerny	koncern	k1gInPc7	koncern
<g/>
,	,	kIx,	,
vládami	vláda	k1gFnPc7	vláda
a	a	k8xC	a
společnostmi	společnost	k1gFnPc7	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
má	mít	k5eAaImIp3nS	mít
naleziště	naleziště	k1gNnSc4	naleziště
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
těžbě	těžba	k1gFnSc6	těžba
se	se	k3xPyFc4	se
podílely	podílet	k5eAaImAgInP	podílet
americké	americký	k2eAgInPc1d1	americký
ropné	ropný	k2eAgInPc1d1	ropný
koncerny	koncern	k1gInPc1	koncern
Unocal	Unocal	k1gFnSc2	Unocal
a	a	k8xC	a
Chevron	Chevron	k1gMnSc1	Chevron
Texaco	Texaco	k1gMnSc1	Texaco
a	a	k8xC	a
francouzský	francouzský	k2eAgMnSc1d1	francouzský
Total	totat	k5eAaImAgMnS	totat
Oil	Oil	k1gMnSc3	Oil
<g/>
.	.	kIx.	.
</s>
<s>
Chevron	Chevron	k1gMnSc1	Chevron
Texaco	Texaco	k1gMnSc1	Texaco
však	však	k9	však
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
na	na	k7c6	na
těžbě	těžba	k1gFnSc6	těžba
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
neparticipuje	participovat	k5eNaImIp3nS	participovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2007	[number]	k4	2007
podepsal	podepsat	k5eAaPmAgMnS	podepsat
Myanmar	Myanmar	k1gMnSc1	Myanmar
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
o	o	k7c4	o
vybudování	vybudování	k1gNnSc4	vybudování
jaderného	jaderný	k2eAgInSc2d1	jaderný
reaktoru	reaktor	k1gInSc2	reaktor
a	a	k8xC	a
jaderného	jaderný	k2eAgNnSc2d1	jaderné
výzkumného	výzkumný	k2eAgNnSc2d1	výzkumné
střediska	středisko	k1gNnSc2	středisko
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
kritizováno	kritizovat	k5eAaImNgNnS	kritizovat
vládou	vláda	k1gFnSc7	vláda
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
Rusko	Rusko	k1gNnSc1	Rusko
utlumilo	utlumit	k5eAaPmAgNnS	utlumit
jadernou	jaderný	k2eAgFnSc4d1	jaderná
spolupráci	spolupráce	k1gFnSc4	spolupráce
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
režim	režim	k1gInSc4	režim
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
se	s	k7c7	s
Severní	severní	k2eAgFnSc7d1	severní
Koreou	Korea	k1gFnSc7	Korea
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
zavedena	zaveden	k2eAgFnSc1d1	zavedena
cenzura	cenzura	k1gFnSc1	cenzura
internetu	internet	k1gInSc2	internet
–	–	k?	–
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
1	[number]	k4	1
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
legálně	legálně	k6eAd1	legálně
lze	lze	k6eAd1	lze
prohlížet	prohlížet	k5eAaImF	prohlížet
jen	jen	k9	jen
stránky	stránka	k1gFnPc4	stránka
z	z	k7c2	z
předem	předem	k6eAd1	předem
schváleného	schválený	k2eAgInSc2d1	schválený
seznamu	seznam	k1gInSc2	seznam
a	a	k8xC	a
všechny	všechen	k3xTgFnPc1	všechen
internetové	internetový	k2eAgFnPc1d1	internetová
kavárny	kavárna	k1gFnPc1	kavárna
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
nařízení	nařízení	k1gNnSc2	nařízení
vlády	vláda	k1gFnSc2	vláda
monitorovány	monitorovat	k5eAaImNgInP	monitorovat
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Myanmaru	Myanmar	k1gInSc2	Myanmar
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
do	do	k7c2	do
území	území	k1gNnSc2	území
státu	stát	k1gInSc2	stát
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
Himálaj	Himálaj	k1gFnSc1	Himálaj
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
horská	horský	k2eAgNnPc4d1	horské
pásma	pásmo	k1gNnPc4	pásmo
zakončená	zakončený	k2eAgNnPc4d1	zakončené
Arakanským	Arakanský	k2eAgNnSc7d1	Arakanský
pohořím	pohoří	k1gNnSc7	pohoří
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Šanská	Šanský	k2eAgFnSc1d1	Šanský
plošina	plošina	k1gFnSc1	plošina
<g/>
,	,	kIx,	,
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
pak	pak	k6eAd1	pak
nížina	nížina	k1gFnSc1	nížina
řeky	řeka	k1gFnSc2	řeka
Iravádí	Iravádí	k1gNnSc2	Iravádí
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
vlhké	vlhký	k2eAgNnSc4d1	vlhké
<g/>
,	,	kIx,	,
teplé	teplý	k2eAgNnSc4d1	teplé
<g/>
,	,	kIx,	,
monzunové	monzunový	k2eAgNnSc4d1	monzunové
<g/>
,	,	kIx,	,
jen	jen	k9	jen
na	na	k7c6	na
severu	sever	k1gInSc6	sever
subtropické	subtropický	k2eAgNnSc1d1	subtropické
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
polovinu	polovina	k1gFnSc4	polovina
země	zem	k1gFnSc2	zem
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
deštné	deštný	k2eAgInPc1d1	deštný
pralesy	prales	k1gInPc1	prales
s	s	k7c7	s
cennými	cenný	k2eAgFnPc7d1	cenná
dřevinami	dřevina	k1gFnPc7	dřevina
a	a	k8xC	a
opadavé	opadavý	k2eAgInPc4d1	opadavý
lesy	les	k1gInPc4	les
<g/>
.	.	kIx.	.
</s>
<s>
Myanmar	Myanmar	k1gInSc1	Myanmar
je	být	k5eAaImIp3nS	být
společně	společně	k6eAd1	společně
s	s	k7c7	s
Kambodžou	Kambodža	k1gFnSc7	Kambodža
a	a	k8xC	a
Laosem	Laos	k1gInSc7	Laos
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejchudších	chudý	k2eAgMnPc2d3	nejchudší
států	stát	k1gInPc2	stát
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
části	část	k1gFnSc6	část
Myanmaru	Myanmar	k1gInSc2	Myanmar
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
vysoké	vysoký	k2eAgNnSc1d1	vysoké
horské	horský	k2eAgNnSc1d1	horské
pásmo	pásmo	k1gNnSc1	pásmo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
předhůří	předhůří	k1gNnSc2	předhůří
tibetsko-himálajské	tibetskoimálajský	k2eAgFnSc2d1	tibetsko-himálajský
horské	horský	k2eAgFnSc2d1	horská
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
Myanmaru	Myanmar	k1gInSc2	Myanmar
je	být	k5eAaImIp3nS	být
hora	hora	k1gFnSc1	hora
Kchakabhouyazi	Kchakabhouyaze	k1gFnSc3	Kchakabhouyaze
s	s	k7c7	s
5881	[number]	k4	5881
metry	metro	k1gNnPc7	metro
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgInPc4d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
myanmarsko-čínské	myanmarsko-čínský	k2eAgFnSc6d1	myanmarsko-čínský
hranici	hranice	k1gFnSc6	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
a	a	k8xC	a
východu	východ	k1gInSc3	východ
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
postupně	postupně	k6eAd1	postupně
snižuje	snižovat	k5eAaImIp3nS	snižovat
a	a	k8xC	a
severní	severní	k2eAgNnSc1d1	severní
horské	horský	k2eAgNnSc1d1	horské
pásmo	pásmo	k1gNnSc1	pásmo
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c6	v
nižší	nízký	k2eAgFnSc6d2	nižší
Čjinské	Čjinská	k1gFnSc6	Čjinská
<g/>
,	,	kIx,	,
Arakanské	Arakanský	k2eAgNnSc4d1	Arakanský
<g/>
,	,	kIx,	,
Peguské	Peguský	k2eAgNnSc4d1	Peguský
a	a	k8xC	a
Tenaserimské	Tenaserimský	k2eAgNnSc4d1	Tenaserimský
pohoří	pohoří	k1gNnSc4	pohoří
a	a	k8xC	a
Šanskou	Šanský	k2eAgFnSc4d1	Šanský
náhorní	náhorní	k2eAgFnSc4d1	náhorní
plošinu	plošina	k1gFnSc4	plošina
<g/>
.	.	kIx.	.
</s>
<s>
Centrální	centrální	k2eAgMnSc1d1	centrální
převážně	převážně	k6eAd1	převážně
nížinatou	nížinatý	k2eAgFnSc7d1	nížinatá
částí	část	k1gFnSc7	část
země	zem	k1gFnSc2	zem
protékají	protékat	k5eAaImIp3nP	protékat
mohutné	mohutný	k2eAgFnPc1d1	mohutná
řeky	řeka	k1gFnPc1	řeka
Iravádí	Iravádí	k1gNnPc2	Iravádí
<g/>
,	,	kIx,	,
Čjintwin	Čjintwina	k1gFnPc2	Čjintwina
<g/>
,	,	kIx,	,
Sistaun	Sistauna	k1gFnPc2	Sistauna
a	a	k8xC	a
Salwin	Salwina	k1gFnPc2	Salwina
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
Myanmaru	Myanmar	k1gInSc2	Myanmar
leží	ležet	k5eAaImIp3nS	ležet
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
třetin	třetina	k1gFnPc2	třetina
v	v	k7c6	v
tropické	tropický	k2eAgFnSc6d1	tropická
monzunové	monzunový	k2eAgFnSc6d1	monzunová
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
střídají	střídat	k5eAaImIp3nP	střídat
období	období	k1gNnSc4	období
suchého	suchý	k2eAgInSc2d1	suchý
severozápadního	severozápadní	k2eAgInSc2d1	severozápadní
a	a	k8xC	a
teplého	teplý	k2eAgInSc2d1	teplý
jihovýchodního	jihovýchodní	k2eAgInSc2d1	jihovýchodní
monzunu	monzun	k1gInSc2	monzun
<g/>
.	.	kIx.	.
</s>
<s>
Suché	Suché	k2eAgNnSc1d1	Suché
severozápadní	severozápadní	k2eAgNnSc1d1	severozápadní
proudění	proudění	k1gNnSc1	proudění
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
přináší	přinášet	k5eAaImIp3nS	přinášet
sušší	suchý	k2eAgNnSc1d2	sušší
a	a	k8xC	a
chladnější	chladný	k2eAgNnSc1d2	chladnější
počasí	počasí	k1gNnSc1	počasí
<g/>
,	,	kIx,	,
nastává	nastávat	k5eAaImIp3nS	nastávat
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
Teplé	Teplé	k2eAgNnSc1d1	Teplé
jihovýchodní	jihovýchodní	k2eAgNnSc1d1	jihovýchodní
proudění	proudění	k1gNnSc1	proudění
převažuje	převažovat	k5eAaImIp3nS	převažovat
od	od	k7c2	od
června	červen	k1gInSc2	červen
do	do	k7c2	do
září	září	k1gNnSc2	září
a	a	k8xC	a
přináší	přinášet	k5eAaImIp3nS	přinášet
převážnou	převážný	k2eAgFnSc4d1	převážná
část	část	k1gFnSc4	část
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
roční	roční	k2eAgFnPc1d1	roční
srážky	srážka	k1gFnPc1	srážka
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
mezi	mezi	k7c7	mezi
750	[number]	k4	750
až	až	k9	až
5	[number]	k4	5
000	[number]	k4	000
milimetry	milimetr	k1gInPc1	milimetr
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
polovinu	polovina	k1gFnSc4	polovina
celkové	celkový	k2eAgFnSc2d1	celková
rozlohy	rozloha	k1gFnSc2	rozloha
Myanmaru	Myanmar	k1gInSc2	Myanmar
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
lesy	les	k1gInPc1	les
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
současné	současný	k2eAgFnSc2d1	současná
doby	doba	k1gFnSc2	doba
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
nezměněno	změněn	k2eNgNnSc1d1	nezměněno
správní	správní	k2eAgNnSc1d1	správní
rozdělení	rozdělení	k1gNnSc1	rozdělení
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
zavedeno	zavést	k5eAaPmNgNnS	zavést
novou	nový	k2eAgFnSc7d1	nová
ústavou	ústava	k1gFnSc7	ústava
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
Barma	Barma	k1gFnSc1	Barma
byla	být	k5eAaImAgFnS	být
administrativně	administrativně	k6eAd1	administrativně
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
sedm	sedm	k4xCc4	sedm
oblastí	oblast	k1gFnPc2	oblast
a	a	k8xC	a
sedm	sedm	k4xCc4	sedm
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmi	sedm	k4xCc6	sedm
oblastech	oblast	k1gFnPc6	oblast
tvoří	tvořit	k5eAaImIp3nS	tvořit
převážnou	převážný	k2eAgFnSc4d1	převážná
většinu	většina	k1gFnSc4	většina
Barmánci	Barmánec	k1gMnSc3	Barmánec
a	a	k8xC	a
sedm	sedm	k4xCc1	sedm
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
ležících	ležící	k2eAgFnPc2d1	ležící
v	v	k7c6	v
příhraničních	příhraniční	k2eAgFnPc6d1	příhraniční
oblastech	oblast	k1gFnPc6	oblast
Myanmaru	Myanmar	k1gInSc2	Myanmar
<g/>
,	,	kIx,	,
obývají	obývat	k5eAaImIp3nP	obývat
většinově	většinově	k6eAd1	většinově
národní	národní	k2eAgFnPc1d1	národní
etnické	etnický	k2eAgFnPc1d1	etnická
rasy	rasa	k1gFnPc1	rasa
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
urbanizace	urbanizace	k1gFnSc2	urbanizace
je	být	k5eAaImIp3nS	být
uváděna	uvádět	k5eAaImNgFnS	uvádět
přes	přes	k7c4	přes
33	[number]	k4	33
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
městem	město	k1gNnSc7	město
Myanmaru	Myanmar	k1gInSc2	Myanmar
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
Neipyijto	Neipyijto	k1gNnSc4	Neipyijto
<g/>
.	.	kIx.	.
</s>
<s>
Obchodní	obchodní	k2eAgNnSc1d1	obchodní
a	a	k8xC	a
průmyslové	průmyslový	k2eAgNnSc1d1	průmyslové
centrum	centrum	k1gNnSc1	centrum
Myanmaru	Myanmar	k1gInSc2	Myanmar
ale	ale	k8xC	ale
představuje	představovat	k5eAaImIp3nS	představovat
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
Rangún	Rangún	k1gInSc1	Rangún
s	s	k7c7	s
5	[number]	k4	5
miliony	milion	k4xCgInPc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
velikými	veliký	k2eAgMnPc7d1	veliký
městy	město	k1gNnPc7	město
jsou	být	k5eAaImIp3nP	být
Mandalaj	Mandalaj	k1gMnSc1	Mandalaj
<g/>
,	,	kIx,	,
Moulmein	Moulmein	k1gMnSc1	Moulmein
<g/>
,	,	kIx,	,
Basein	Basein	k1gMnSc1	Basein
<g/>
,	,	kIx,	,
Pegu	Pegus	k1gInSc2	Pegus
a	a	k8xC	a
Sistwei	Sistwe	k1gFnSc2	Sistwe
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Myanmaru	Myanmar	k1gInSc2	Myanmar
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Myanmaru	Myanmar	k1gInSc2	Myanmar
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
přepočtu	přepočet	k1gInSc6	přepočet
k	k	k7c3	k
HDP	HDP	kA	HDP
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejslabších	slabý	k2eAgFnPc2d3	nejslabší
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
regionu	region	k1gInSc6	region
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
se	se	k3xPyFc4	se
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
podepsala	podepsat	k5eAaPmAgFnS	podepsat
vládní	vládní	k2eAgFnSc1d1	vládní
politika	politika	k1gFnSc1	politika
od	od	k7c2	od
dosažení	dosažení	k1gNnSc2	dosažení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pod	pod	k7c7	pod
britskou	britský	k2eAgFnSc7d1	britská
správou	správa	k1gFnSc7	správa
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
druhá	druhý	k4xOgFnSc1	druhý
nejbohatší	bohatý	k2eAgFnSc1d3	nejbohatší
země	země	k1gFnSc1	země
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
premiér	premiéra	k1gFnPc2	premiéra
U	U	kA	U
Nu	nu	k9	nu
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
politiku	politika	k1gFnSc4	politika
znárodňování	znárodňování	k1gNnSc2	znárodňování
a	a	k8xC	a
stát	stát	k1gInSc1	stát
byl	být	k5eAaImAgInS	být
prohlášen	prohlásit	k5eAaPmNgInS	prohlásit
vlastníkem	vlastník	k1gMnSc7	vlastník
veškeré	veškerý	k3xTgFnSc2	veškerý
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
také	také	k9	také
vyhlášeny	vyhlášen	k2eAgFnPc1d1	vyhlášena
osmiletky	osmiletka	k1gFnPc1	osmiletka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
export	export	k1gInSc1	export
rýže	rýže	k1gFnSc2	rýže
propadl	propadnout	k5eAaPmAgInS	propadnout
o	o	k7c4	o
dvě	dva	k4xCgFnPc4	dva
třetiny	třetina	k1gFnPc4	třetina
a	a	k8xC	a
export	export	k1gInSc4	export
minerálů	minerál	k1gInPc2	minerál
o	o	k7c4	o
96	[number]	k4	96
<g/>
%	%	kIx~	%
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
předchozí	předchozí	k2eAgFnSc7d1	předchozí
úrovní	úroveň	k1gFnSc7	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Plány	plán	k1gInPc1	plán
byly	být	k5eAaImAgFnP	být
částečně	částečně	k6eAd1	částečně
financovány	financovat	k5eAaBmNgFnP	financovat
tištěním	tištění	k1gNnSc7	tištění
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
inflaci	inflace	k1gFnSc3	inflace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
puči	puč	k1gInSc6	puč
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
program	program	k1gInSc1	program
Barmské	barmský	k2eAgFnSc2d1	barmská
cesty	cesta	k1gFnSc2	cesta
k	k	k7c3	k
socialismu	socialismus	k1gInSc3	socialismus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
znárodnil	znárodnit	k5eAaPmAgInS	znárodnit
všechny	všechen	k3xTgInPc4	všechen
sektory	sektor	k1gInPc4	sektor
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
program	program	k1gInSc1	program
skončil	skončit	k5eAaPmAgInS	skončit
katastrofou	katastrofa	k1gFnSc7	katastrofa
a	a	k8xC	a
zemi	zem	k1gFnSc6	zem
zcela	zcela	k6eAd1	zcela
ekonomicky	ekonomicky	k6eAd1	ekonomicky
zbídačil	zbídačit	k5eAaPmAgMnS	zbídačit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
byla	být	k5eAaImAgFnS	být
Barma	Barma	k1gFnSc1	Barma
zařazena	zařadit	k5eAaPmNgFnS	zařadit
mezi	mezi	k7c4	mezi
nejméně	málo	k6eAd3	málo
rozvinuté	rozvinutý	k2eAgFnPc4d1	rozvinutá
země	zem	k1gFnPc4	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
nicméně	nicméně	k8xC	nicméně
civilní	civilní	k2eAgFnSc1d1	civilní
vláda	vláda	k1gFnSc1	vláda
Theina	Thein	k2eAgFnSc1d1	Theina
Seina	Seina	k1gFnSc1	Seina
provádí	provádět	k5eAaImIp3nS	provádět
některé	některý	k3yIgFnPc4	některý
ekonomické	ekonomický	k2eAgFnPc4d1	ekonomická
a	a	k8xC	a
politické	politický	k2eAgFnPc4d1	politická
reformy	reforma	k1gFnPc4	reforma
<g/>
,	,	kIx,	,
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
čehož	což	k3yRnSc2	což
se	se	k3xPyFc4	se
zvýšily	zvýšit	k5eAaPmAgFnP	zvýšit
zahraniční	zahraniční	k2eAgFnPc1d1	zahraniční
investice	investice	k1gFnPc1	investice
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
zrušeny	zrušit	k5eAaPmNgFnP	zrušit
mezinárodní	mezinárodní	k2eAgFnPc1d1	mezinárodní
sankce	sankce	k1gFnPc1	sankce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
výrazněji	výrazně	k6eAd2	výrazně
převládá	převládat	k5eAaImIp3nS	převládat
zemědělství	zemědělství	k1gNnSc1	zemědělství
nad	nad	k7c7	nad
průmyslovou	průmyslový	k2eAgFnSc7d1	průmyslová
výrobou	výroba	k1gFnSc7	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
zemědělskou	zemědělský	k2eAgFnSc7d1	zemědělská
oblastí	oblast	k1gFnSc7	oblast
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
delta	delta	k1gFnSc1	delta
řeky	řeka	k1gFnSc2	řeka
Iravadi	Iravadi	k1gNnSc2	Iravadi
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
převažuje	převažovat	k5eAaImIp3nS	převažovat
pěstitelství	pěstitelství	k1gNnSc4	pěstitelství
rýže	rýže	k1gFnSc2	rýže
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nížinných	nížinný	k2eAgFnPc6d1	nížinná
centrální	centrální	k2eAgFnSc6d1	centrální
částech	část	k1gFnPc6	část
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
rýže	rýže	k1gFnPc4	rýže
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
i	i	k9	i
cukrová	cukrový	k2eAgFnSc1d1	cukrová
třtina	třtina	k1gFnSc1	třtina
<g/>
,	,	kIx,	,
bavlna	bavlna	k1gFnSc1	bavlna
<g/>
,	,	kIx,	,
proso	proso	k1gNnSc1	proso
<g/>
,	,	kIx,	,
sezam	sezam	k1gInSc1	sezam
<g/>
,	,	kIx,	,
podzemnice	podzemnice	k1gFnSc1	podzemnice
olejná	olejný	k2eAgFnSc1d1	olejná
<g/>
,	,	kIx,	,
kukuřice	kukuřice	k1gFnSc1	kukuřice
a	a	k8xC	a
tabák	tabák	k1gInSc1	tabák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejjižnějších	jižní	k2eAgFnPc6d3	nejjižnější
přímořských	přímořský	k2eAgFnPc6d1	přímořská
oblastech	oblast	k1gFnPc6	oblast
rostou	růst	k5eAaImIp3nP	růst
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
tropického	tropický	k2eAgNnSc2d1	tropické
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgFnSc1d1	významná
je	být	k5eAaImIp3nS	být
také	také	k9	také
těžba	těžba	k1gFnSc1	těžba
některých	některý	k3yIgFnPc2	některý
nerostných	nerostný	k2eAgFnPc2d1	nerostná
surovin	surovina	k1gFnPc2	surovina
<g/>
,	,	kIx,	,
drahokamů	drahokam	k1gInPc2	drahokam
a	a	k8xC	a
tropického	tropický	k2eAgNnSc2d1	tropické
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nerostných	nerostný	k2eAgFnPc2d1	nerostná
surovin	surovina	k1gFnPc2	surovina
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
především	především	k9	především
ropa	ropa	k1gFnSc1	ropa
<g/>
,	,	kIx,	,
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
a	a	k8xC	a
rudy	ruda	k1gFnPc1	ruda
cínu	cín	k1gInSc2	cín
<g/>
,	,	kIx,	,
zinku	zinek	k1gInSc2	zinek
<g/>
,	,	kIx,	,
niklu	nikl	k1gInSc2	nikl
<g/>
,	,	kIx,	,
wolframu	wolfram	k1gInSc2	wolfram
a	a	k8xC	a
olova	olovo	k1gNnSc2	olovo
<g/>
,	,	kIx,	,
z	z	k7c2	z
drahokamů	drahokam	k1gInPc2	drahokam
a	a	k8xC	a
polodrahokamů	polodrahokam	k1gInPc2	polodrahokam
pak	pak	k6eAd1	pak
rubíny	rubín	k1gInPc7	rubín
a	a	k8xC	a
jadeity	jadeit	k1gInPc7	jadeit
a	a	k8xC	a
z	z	k7c2	z
tropických	tropický	k2eAgNnPc2d1	tropické
dřev	dřevo	k1gNnPc2	dřevo
zejména	zejména	k9	zejména
teak	teak	k1gInSc4	teak
<g/>
.	.	kIx.	.
</s>
<s>
Barma	Barma	k1gFnSc1	Barma
je	být	k5eAaImIp3nS	být
také	také	k9	také
druhým	druhý	k4xOgMnSc7	druhý
největším	veliký	k2eAgMnSc7d3	veliký
světovým	světový	k2eAgMnSc7d1	světový
producentem	producent	k1gMnSc7	producent
opia	opium	k1gNnSc2	opium
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
25	[number]	k4	25
<g/>
%	%	kIx~	%
celé	celý	k2eAgFnSc2d1	celá
světové	světový	k2eAgFnSc2d1	světová
produkce	produkce	k1gFnSc2	produkce
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
nelegálních	legální	k2eNgFnPc2d1	nelegální
drog	droga	k1gFnPc2	droga
včetně	včetně	k7c2	včetně
amfetaminů	amfetamin	k1gInPc2	amfetamin
<g/>
.	.	kIx.	.
</s>
<s>
Zákazy	zákaz	k1gInPc1	zákaz
opia	opium	k1gNnSc2	opium
jsou	být	k5eAaImIp3nP	být
realizovány	realizovat	k5eAaBmNgFnP	realizovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
tlak	tlak	k1gInSc1	tlak
přestal	přestat	k5eAaPmAgInS	přestat
působit	působit	k5eAaImF	působit
na	na	k7c4	na
zemědělce	zemědělec	k1gMnPc4	zemědělec
původně	původně	k6eAd1	původně
pěstující	pěstující	k2eAgMnSc1d1	pěstující
mák	mák	k1gMnSc1	mák
bez	bez	k7c2	bez
trvale	trvale	k6eAd1	trvale
udržitelných	udržitelný	k2eAgInPc2d1	udržitelný
zdrojů	zdroj	k1gInPc2	zdroj
příjmu	příjem	k1gInSc2	příjem
v	v	k7c6	v
regionech	region	k1gInPc6	region
Kokang	Kokanga	k1gFnPc2	Kokanga
a	a	k8xC	a
Wa	Wa	k1gFnPc2	Wa
<g/>
.	.	kIx.	.
</s>
<s>
Příjem	příjem	k1gInSc1	příjem
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
příležitostných	příležitostný	k2eAgFnPc6d1	příležitostná
pracích	práce	k1gFnPc6	práce
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Demografie	demografie	k1gFnSc2	demografie
Myanmaru	Myanmar	k1gInSc2	Myanmar
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Myanmaru	Myanmar	k1gInSc2	Myanmar
bývá	bývat	k5eAaImIp3nS	bývat
uváděn	uvádět	k5eAaImNgInS	uvádět
mezi	mezi	k7c7	mezi
48	[number]	k4	48
a	a	k8xC	a
55	[number]	k4	55
miliony	milion	k4xCgInPc4	milion
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
odhadovala	odhadovat	k5eAaImAgFnS	odhadovat
Organizace	organizace	k1gFnSc1	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
počet	počet	k1gInSc4	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
země	zem	k1gFnSc2	zem
na	na	k7c4	na
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
přirozený	přirozený	k2eAgInSc1d1	přirozený
přírůstek	přírůstek	k1gInSc1	přírůstek
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
je	být	k5eAaImIp3nS	být
udáván	udávat	k5eAaImNgInS	udávat
okolo	okolo	k7c2	okolo
0,8	[number]	k4	0,8
%	%	kIx~	%
a	a	k8xC	a
střední	střední	k2eAgFnSc1d1	střední
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
asi	asi	k9	asi
63	[number]	k4	63
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Antropologicky	antropologicky	k6eAd1	antropologicky
patří	patřit	k5eAaImIp3nS	patřit
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Myanmaru	Myanmar	k1gInSc2	Myanmar
z	z	k7c2	z
98	[number]	k4	98
<g/>
%	%	kIx~	%
k	k	k7c3	k
mongolskému	mongolský	k2eAgInSc3d1	mongolský
typu	typ	k1gInSc3	typ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
junta	junta	k1gFnSc1	junta
v	v	k7c6	v
Myanmaru	Myanmar	k1gInSc6	Myanmar
oficiálně	oficiálně	k6eAd1	oficiálně
uznává	uznávat	k5eAaImIp3nS	uznávat
135	[number]	k4	135
etnických	etnický	k2eAgFnPc2d1	etnická
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Osm	osm	k4xCc1	osm
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
135	[number]	k4	135
etnických	etnický	k2eAgFnPc2d1	etnická
skupin	skupina	k1gFnPc2	skupina
tvoří	tvořit	k5eAaImIp3nP	tvořit
hlavní	hlavní	k2eAgFnPc1d1	hlavní
národnosti	národnost	k1gFnPc1	národnost
označované	označovaný	k2eAgFnPc1d1	označovaná
také	také	k9	také
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
národní	národní	k2eAgFnSc2d1	národní
etnické	etnický	k2eAgFnSc2d1	etnická
rasy	rasa	k1gFnSc2	rasa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
národní	národní	k2eAgFnSc2d1	národní
etnické	etnický	k2eAgFnSc2d1	etnická
rasy	rasa	k1gFnSc2	rasa
jsou	být	k5eAaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
Barmánci	Barmánec	k1gMnPc1	Barmánec
<g/>
,	,	kIx,	,
Šanové	Šanus	k1gMnPc1	Šanus
<g/>
,	,	kIx,	,
Kačjinové	Kačjin	k1gMnPc1	Kačjin
<g/>
,	,	kIx,	,
Čjinové	Čjin	k1gMnPc1	Čjin
<g/>
,	,	kIx,	,
Rakhinové	Rakhin	k1gMnPc1	Rakhin
<g/>
,	,	kIx,	,
Karenové	Karen	k1gMnPc1	Karen
<g/>
,	,	kIx,	,
Monové	Monus	k1gMnPc1	Monus
a	a	k8xC	a
Kajaové	Kajaus	k1gMnPc1	Kajaus
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
země	zem	k1gFnSc2	zem
představují	představovat	k5eAaImIp3nP	představovat
Barmánci	Barmánec	k1gMnPc1	Barmánec
asi	asi	k9	asi
68	[number]	k4	68
%	%	kIx~	%
<g/>
,	,	kIx,	,
Šanové	Šanové	k2eAgFnSc1d1	Šanové
9	[number]	k4	9
%	%	kIx~	%
<g/>
,	,	kIx,	,
Karenové	Karenové	k2eAgFnSc1d1	Karenové
7	[number]	k4	7
%	%	kIx~	%
<g/>
,	,	kIx,	,
Rakhinové	Rakhinové	k2eAgFnSc1d1	Rakhinové
4	[number]	k4	4
%	%	kIx~	%
<g/>
,	,	kIx,	,
Čjinové	Čjinové	k2eAgFnSc1d1	Čjinové
2	[number]	k4	2
%	%	kIx~	%
<g/>
,	,	kIx,	,
Monové	Monové	k2eAgFnSc1d1	Monové
2	[number]	k4	2
%	%	kIx~	%
a	a	k8xC	a
Kačjinové	Kačjinové	k2eAgMnSc1d1	Kačjinové
1,5	[number]	k4	1,5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Zbylou	zbylý	k2eAgFnSc4d1	zbylá
část	část	k1gFnSc4	část
obyvatel	obyvatel	k1gMnPc2	obyvatel
tvoří	tvořit	k5eAaImIp3nP	tvořit
menší	malý	k2eAgFnPc1d2	menší
etnické	etnický	k2eAgFnPc1d1	etnická
skupiny	skupina	k1gFnPc1	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Indové	Ind	k1gMnPc1	Ind
tvoří	tvořit	k5eAaImIp3nP	tvořit
asi	asi	k9	asi
2	[number]	k4	2
%	%	kIx~	%
a	a	k8xC	a
Číňané	Číňan	k1gMnPc1	Číňan
3	[number]	k4	3
%	%	kIx~	%
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mezi	mezi	k7c4	mezi
samostatné	samostatný	k2eAgFnPc4d1	samostatná
etnické	etnický	k2eAgFnPc4d1	etnická
skupiny	skupina	k1gFnPc4	skupina
se	se	k3xPyFc4	se
neřadí	řadit	k5eNaImIp3nP	řadit
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
oficiálně	oficiálně	k6eAd1	oficiálně
pokládáni	pokládat	k5eAaImNgMnP	pokládat
za	za	k7c4	za
nepůvodní	původní	k2eNgNnPc4d1	nepůvodní
a	a	k8xC	a
naturalizovaná	naturalizovaný	k2eAgNnPc4d1	naturalizované
etnika	etnikum	k1gNnPc4	etnikum
<g/>
.	.	kIx.	.
</s>
<s>
Úředním	úřední	k2eAgMnPc3d1	úřední
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
jazykem	jazyk	k1gInSc7	jazyk
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
barmština	barmština	k1gFnSc1	barmština
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
mateřským	mateřský	k2eAgInSc7d1	mateřský
jazykem	jazyk	k1gInSc7	jazyk
přibližně	přibližně	k6eAd1	přibližně
pro	pro	k7c4	pro
60	[number]	k4	60
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
etnické	etnický	k2eAgFnPc1d1	etnická
skupiny	skupina	k1gFnPc1	skupina
hovoří	hovořit	k5eAaImIp3nP	hovořit
vlastními	vlastní	k2eAgInPc7d1	vlastní
jazyky	jazyk	k1gInPc7	jazyk
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
rozvětvují	rozvětvovat	k5eAaImIp3nP	rozvětvovat
v	v	k7c4	v
řadu	řada	k1gFnSc4	řada
dialektů	dialekt	k1gInPc2	dialekt
<g/>
.	.	kIx.	.
</s>
<s>
Barmánci	Barmánec	k1gMnPc1	Barmánec
<g/>
,	,	kIx,	,
Karenové	Karen	k1gMnPc1	Karen
<g/>
,	,	kIx,	,
Šanové	Šanus	k1gMnPc1	Šanus
a	a	k8xC	a
Monové	Monus	k1gMnPc1	Monus
používají	používat	k5eAaImIp3nP	používat
písmo	písmo	k1gNnSc4	písmo
založené	založený	k2eAgFnSc2d1	založená
na	na	k7c6	na
slabičném	slabičný	k2eAgNnSc6d1	slabičné
písmu	písmo	k1gNnSc6	písmo
indického	indický	k2eAgInSc2d1	indický
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Kačjinové	Kačjin	k1gMnPc1	Kačjin
a	a	k8xC	a
Čjinové	Čjin	k1gMnPc1	Čjin
píší	psát	k5eAaImIp3nP	psát
latinkou	latinka	k1gFnSc7	latinka
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc4d1	mnohý
dialekty	dialekt	k1gInPc4	dialekt
vlastní	vlastní	k2eAgNnSc4d1	vlastní
písmo	písmo	k1gNnSc4	písmo
nemají	mít	k5eNaImIp3nP	mít
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
gramotnosti	gramotnost	k1gFnSc2	gramotnost
je	být	k5eAaImIp3nS	být
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
asi	asi	k9	asi
na	na	k7c4	na
80	[number]	k4	80
<g/>
–	–	k?	–
<g/>
90	[number]	k4	90
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Nejrozšířenějším	rozšířený	k2eAgNnSc7d3	nejrozšířenější
a	a	k8xC	a
převažujícím	převažující	k2eAgNnSc7d1	převažující
náboženstvím	náboženství	k1gNnSc7	náboženství
v	v	k7c6	v
Myanmaru	Myanmar	k1gInSc6	Myanmar
je	být	k5eAaImIp3nS	být
théravádský	théravádský	k2eAgInSc1d1	théravádský
buddhismus	buddhismus	k1gInSc1	buddhismus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
80-89	[number]	k4	80-89
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
počtu	počet	k1gInSc2	počet
buddhistických	buddhistický	k2eAgMnPc2d1	buddhistický
mnichů	mnich	k1gMnPc2	mnich
a	a	k8xC	a
výdajů	výdaj	k1gInPc2	výdaj
na	na	k7c4	na
náboženství	náboženství	k1gNnSc4	náboženství
je	být	k5eAaImIp3nS	být
Myanmar	Myanmar	k1gMnSc1	Myanmar
nejbuddhističtější	buddhistický	k2eAgFnSc2d3	buddhistický
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Mniši	mnich	k1gMnPc1	mnich
jsou	být	k5eAaImIp3nP	být
váženými	vážený	k2eAgMnPc7d1	vážený
členy	člen	k1gMnPc7	člen
myanmarské	myanmarský	k2eAgFnSc2d1	myanmarská
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarším	starý	k2eAgNnSc7d3	nejstarší
náboženstvím	náboženství	k1gNnSc7	náboženství
uznávaným	uznávaný	k2eAgNnSc7d1	uznávané
na	na	k7c4	na
území	území	k1gNnSc4	území
Myanmaru	Myanmar	k1gInSc2	Myanmar
je	být	k5eAaImIp3nS	být
hinduismus	hinduismus	k1gInSc1	hinduismus
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgInSc3	který
se	se	k3xPyFc4	se
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
hlásí	hlásit	k5eAaImIp3nS	hlásit
zhruba	zhruba	k6eAd1	zhruba
2	[number]	k4	2
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
Britští	britský	k2eAgMnPc1d1	britský
kolonisté	kolonista	k1gMnPc1	kolonista
odvedli	odvést	k5eAaPmAgMnP	odvést
miliony	milion	k4xCgInPc1	milion
indických	indický	k2eAgMnPc2d1	indický
hinduistů	hinduista	k1gMnPc2	hinduista
na	na	k7c4	na
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
Barmě	Barma	k1gFnSc6	Barma
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracovali	pracovat	k5eAaImAgMnP	pracovat
především	především	k9	především
na	na	k7c6	na
plantážích	plantáž	k1gFnPc6	plantáž
a	a	k8xC	a
v	v	k7c6	v
dolech	dol	k1gInPc6	dol
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prosadili	prosadit	k5eAaPmAgMnP	prosadit
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
obchodě	obchod	k1gInSc6	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
na	na	k7c6	na
Británii	Británie	k1gFnSc6	Británie
xenofobní	xenofobní	k2eAgFnSc1d1	xenofobní
politika	politika	k1gFnSc1	politika
hlavy	hlava	k1gFnSc2	hlava
státu	stát	k1gInSc2	stát
Nei	Nei	k1gFnSc1	Nei
Wina	Wina	k1gFnSc1	Wina
vykázala	vykázat	k5eAaPmAgFnS	vykázat
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
1963	[number]	k4	1963
a	a	k8xC	a
1967	[number]	k4	1967
na	na	k7c4	na
300	[number]	k4	300
000	[number]	k4	000
myanmarských	myanmarský	k2eAgMnPc2d1	myanmarský
etnických	etnický	k2eAgMnPc2d1	etnický
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
000	[number]	k4	000
čínských	čínský	k2eAgMnPc2d1	čínský
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Indická	indický	k2eAgFnSc1d1	indická
politika	politika	k1gFnSc1	politika
podporující	podporující	k2eAgInPc1d1	podporující
demokratické	demokratický	k2eAgInPc1d1	demokratický
protesty	protest	k1gInPc1	protest
v	v	k7c6	v
Myanmaru	Myanmar	k1gInSc6	Myanmar
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
perzekuci	perzekuce	k1gFnSc4	perzekuce
hinduistů	hinduista	k1gMnPc2	hinduista
a	a	k8xC	a
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
podpoře	podpora	k1gFnSc3	podpora
Myanmaru	Myanmar	k1gInSc2	Myanmar
zbývajících	zbývající	k2eAgFnPc2d1	zbývající
skupin	skupina	k1gFnPc2	skupina
rebelů	rebel	k1gMnPc2	rebel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
usídlili	usídlit	k5eAaPmAgMnP	usídlit
v	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
otevření	otevření	k1gNnSc4	otevření
se	se	k3xPyFc4	se
myanmarského	myanmarský	k2eAgInSc2d1	myanmarský
trhu	trh	k1gInSc2	trh
a	a	k8xC	a
navázání	navázání	k1gNnSc4	navázání
nových	nový	k2eAgInPc2d1	nový
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
vztahů	vztah	k1gInPc2	vztah
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
větší	veliký	k2eAgFnSc3d2	veliký
toleranci	tolerance	k1gFnSc3	tolerance
hinduistů	hinduista	k1gMnPc2	hinduista
a	a	k8xC	a
menších	malý	k2eAgFnPc2d2	menší
náboženských	náboženský	k2eAgFnPc2d1	náboženská
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
křesťanství	křesťanství	k1gNnSc3	křesťanství
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
výzkumu	výzkum	k1gInSc2	výzkum
hlásí	hlásit	k5eAaImIp3nS	hlásit
4-7	[number]	k4	4-7
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgFnPc1	čtyři
pětiny	pětina	k1gFnPc1	pětina
jsou	být	k5eAaImIp3nP	být
protestanti	protestant	k1gMnPc1	protestant
<g/>
,	,	kIx,	,
zbylá	zbylý	k2eAgFnSc1d1	zbylá
jedna	jeden	k4xCgFnSc1	jeden
pětina	pětina	k1gFnSc1	pětina
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
baptisty	baptista	k1gMnPc7	baptista
a	a	k8xC	a
katolíky	katolík	k1gMnPc7	katolík
<g/>
.	.	kIx.	.
</s>
<s>
Křesťané	křesťan	k1gMnPc1	křesťan
čelí	čelit	k5eAaImIp3nP	čelit
náboženským	náboženský	k2eAgFnPc3d1	náboženská
perzekucím	perzekuce	k1gFnPc3	perzekuce
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
velmi	velmi	k6eAd1	velmi
těžké	těžký	k2eAgNnSc1d1	těžké
až	až	k9	až
nemožné	možný	k2eNgNnSc1d1	nemožné
navázat	navázat	k5eAaPmF	navázat
pracovní	pracovní	k2eAgInSc4d1	pracovní
poměr	poměr	k1gInSc4	poměr
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
<g/>
,	,	kIx,	,
či	či	k8xC	či
státní	státní	k2eAgFnSc3d1	státní
správě	správa	k1gFnSc3	správa
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Myanmaru	Myanmar	k1gInSc6	Myanmar
hlavní	hlavní	k2eAgFnSc1d1	hlavní
cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
úspěchu	úspěch	k1gInSc3	úspěch
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
vypalování	vypalování	k1gNnSc3	vypalování
křesťanských	křesťanský	k2eAgInPc2d1	křesťanský
kostelů	kostel	k1gInPc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
provedeného	provedený	k2eAgInSc2d1	provedený
vládou	vláda	k1gFnSc7	vláda
Myanmaru	Myanmar	k1gInSc2	Myanmar
islám	islám	k1gInSc1	islám
na	na	k7c6	na
území	území	k1gNnSc6	území
země	zem	k1gFnSc2	zem
vyznávají	vyznávat	k5eAaImIp3nP	vyznávat
4	[number]	k4	4
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Výzkumy	výzkum	k1gInPc1	výzkum
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgFnPc2d1	americká
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
území	území	k1gNnSc6	území
Myanmaru	Myanmar	k1gInSc2	Myanmar
žije	žít	k5eAaImIp3nS	žít
až	až	k9	až
10	[number]	k4	10
%	%	kIx~	%
muslimské	muslimský	k2eAgFnSc2d1	muslimská
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
muslimů	muslim	k1gMnPc2	muslim
jsou	být	k5eAaImIp3nP	být
sunnité	sunnita	k1gMnPc1	sunnita
a	a	k8xC	a
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
komunitách	komunita	k1gFnPc6	komunita
napříč	napříč	k7c7	napříč
celou	celý	k2eAgFnSc7d1	celá
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Indičtí	indický	k2eAgMnPc1d1	indický
muslimové	muslim	k1gMnPc1	muslim
se	se	k3xPyFc4	se
soustředují	soustředovat	k5eAaBmIp3nP	soustředovat
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Rangún	Rangún	k1gInSc1	Rangún
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýraznější	výrazný	k2eAgFnSc7d3	nejvýraznější
muslimskou	muslimský	k2eAgFnSc7d1	muslimská
menšinou	menšina	k1gFnSc7	menšina
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
Rohingyové	Rohingyové	k2eAgInPc7d1	Rohingyové
<g/>
,	,	kIx,	,
hlavně	hlavně	k6eAd1	hlavně
na	na	k7c6	na
severu	sever	k1gInSc6	sever
východního	východní	k2eAgInSc2d1	východní
státu	stát	k1gInSc2	stát
Rakhin	Rakhina	k1gFnPc2	Rakhina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
bojovali	bojovat	k5eAaImAgMnP	bojovat
za	za	k7c4	za
vlastní	vlastní	k2eAgInSc4d1	vlastní
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
boje	boj	k1gInPc1	boj
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgInPc1d1	známý
pod	pod	k7c7	pod
pojmem	pojem	k1gInSc7	pojem
Rohingyjský	Rohingyjský	k2eAgInSc4d1	Rohingyjský
konflikt	konflikt	k1gInSc4	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
vyhlášení	vyhlášení	k1gNnPc2	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
vláda	vláda	k1gFnSc1	vláda
neposkytuje	poskytovat	k5eNaImIp3nS	poskytovat
občanství	občanství	k1gNnSc4	občanství
Rohingyjským	Rohingyjský	k2eAgMnPc3d1	Rohingyjský
muslimům	muslim	k1gMnPc3	muslim
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
menšinu	menšina	k1gFnSc4	menšina
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
perzekuce	perzekuce	k1gFnSc1	perzekuce
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
přinutilo	přinutit	k5eAaPmAgNnS	přinutit
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
Rohingyů	Rohingy	k1gInPc2	Rohingy
prchnout	prchnout	k5eAaPmF	prchnout
do	do	k7c2	do
Bangladéše	Bangladéš	k1gInSc2	Bangladéš
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgFnPc2d1	jiná
muslimských	muslimský	k2eAgFnPc2d1	muslimská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Muslimové	muslim	k1gMnPc1	muslim
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
oběťmi	oběť	k1gFnPc7	oběť
etnického	etnický	k2eAgNnSc2d1	etnické
násilí	násilí	k1gNnSc2	násilí
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
většinových	většinový	k2eAgMnPc2d1	většinový
buddhistů	buddhista	k1gMnPc2	buddhista
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
většina	většina	k1gFnSc1	většina
muslimů	muslim	k1gMnPc2	muslim
nemohla	moct	k5eNaImAgFnS	moct
volit	volit	k5eAaImF	volit
ani	ani	k8xC	ani
kandidovat	kandidovat	k5eAaImF	kandidovat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původních	původní	k2eAgInPc2d1	původní
tisíců	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
vyznávajících	vyznávající	k2eAgMnPc2d1	vyznávající
judaismus	judaismus	k1gInSc4	judaismus
<g/>
,	,	kIx,	,
zbylo	zbýt	k5eAaPmAgNnS	zbýt
posledních	poslední	k2eAgMnPc2d1	poslední
20	[number]	k4	20
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
sídlí	sídlet	k5eAaImIp3nP	sídlet
v	v	k7c6	v
největším	veliký	k2eAgNnSc6d3	veliký
městě	město	k1gNnSc6	město
Myanmaru	Myanmar	k1gInSc2	Myanmar
Rangún	Rangún	k1gInSc1	Rangún
<g/>
,	,	kIx,	,
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
jediné	jediný	k2eAgFnSc2d1	jediná
synagogy	synagoga	k1gFnSc2	synagoga
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Odchod	odchod	k1gInSc4	odchod
židů	žid	k1gMnPc2	žid
způsobila	způsobit	k5eAaPmAgFnS	způsobit
druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Zbylá	zbylý	k2eAgNnPc1d1	zbylé
procenta	procento	k1gNnPc1	procento
věřících	věřící	k1gMnPc2	věřící
tvoří	tvořit	k5eAaImIp3nP	tvořit
menšiny	menšina	k1gFnPc4	menšina
vyznávající	vyznávající	k2eAgInSc4d1	vyznávající
tradiční	tradiční	k2eAgInSc4d1	tradiční
východoasijská	východoasijský	k2eAgNnPc1d1	východoasijské
či	či	k8xC	či
animistická	animistický	k2eAgNnPc1d1	animistické
náboženství	náboženství	k1gNnPc1	náboženství
<g/>
.	.	kIx.	.
</s>
