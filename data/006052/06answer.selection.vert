<s>
Sublimace	sublimace	k1gFnSc1	sublimace
je	být	k5eAaImIp3nS	být
skupenská	skupenský	k2eAgFnSc1d1	skupenská
přeměna	přeměna	k1gFnSc1	přeměna
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
pevná	pevný	k2eAgFnSc1d1	pevná
látka	látka	k1gFnSc1	látka
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
plyn	plyn	k1gInSc4	plyn
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
tání	tání	k1gNnSc3	tání
pevné	pevný	k2eAgFnSc2d1	pevná
látky	látka	k1gFnSc2	látka
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
bez	bez	k7c2	bez
průchodu	průchod	k1gInSc2	průchod
kapalnou	kapalný	k2eAgFnSc7d1	kapalná
fází	fáze	k1gFnSc7	fáze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
