<s>
Sublimace	sublimace	k1gFnSc1	sublimace
je	být	k5eAaImIp3nS	být
skupenská	skupenský	k2eAgFnSc1d1	skupenská
přeměna	přeměna	k1gFnSc1	přeměna
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
pevná	pevný	k2eAgFnSc1d1	pevná
látka	látka	k1gFnSc1	látka
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
plyn	plyn	k1gInSc4	plyn
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
tání	tání	k1gNnSc3	tání
pevné	pevný	k2eAgFnSc2d1	pevná
látky	látka	k1gFnSc2	látka
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
bez	bez	k7c2	bez
průchodu	průchod	k1gInSc2	průchod
kapalnou	kapalný	k2eAgFnSc7d1	kapalná
fází	fáze	k1gFnSc7	fáze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Opačný	opačný	k2eAgInSc1d1	opačný
proces	proces	k1gInSc1	proces
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
desublimace	desublimace	k1gFnSc1	desublimace
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
desublimaci	desublimace	k1gFnSc6	desublimace
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
plyn	plyn	k1gInSc1	plyn
mění	měnit	k5eAaImIp3nS	měnit
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
pevnou	pevný	k2eAgFnSc4d1	pevná
látku	látka	k1gFnSc4	látka
bez	bez	k7c2	bez
předchozí	předchozí	k2eAgFnSc2d1	předchozí
kondenzace	kondenzace	k1gFnSc2	kondenzace
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sublimaci	sublimace	k1gFnSc6	sublimace
je	být	k5eAaImIp3nS	být
tlak	tlak	k1gInSc1	tlak
nasycených	nasycený	k2eAgFnPc2d1	nasycená
par	para	k1gFnPc2	para
nad	nad	k7c7	nad
pevnou	pevný	k2eAgFnSc7d1	pevná
fází	fáze	k1gFnSc7	fáze
vždy	vždy	k6eAd1	vždy
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
jejich	jejich	k3xOp3gInSc1	jejich
tlak	tlak	k1gInSc1	tlak
nad	nad	k7c7	nad
kapalnou	kapalný	k2eAgFnSc7d1	kapalná
fází	fáze	k1gFnSc7	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
normálního	normální	k2eAgInSc2d1	normální
tlaku	tlak	k1gInSc2	tlak
sublimují	sublimovat	k5eAaBmIp3nP	sublimovat
např.	např.	kA	např.
tyto	tento	k3xDgFnPc1	tento
látky	látka	k1gFnPc1	látka
<g/>
:	:	kIx,	:
jód	jód	k1gInSc1	jód
<g/>
,	,	kIx,	,
led	led	k1gInSc1	led
<g/>
,	,	kIx,	,
naftalen	naftalen	k1gInSc1	naftalen
<g/>
,	,	kIx,	,
salmiak	salmiak	k1gInSc1	salmiak
aj.	aj.	kA	aj.
Při	při	k7c6	při
dostatečně	dostatečně	k6eAd1	dostatečně
nízkém	nízký	k2eAgInSc6d1	nízký
tlaku	tlak	k1gInSc6	tlak
může	moct	k5eAaImIp3nS	moct
sublimovat	sublimovat	k5eAaBmF	sublimovat
většina	většina	k1gFnSc1	většina
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Schopnost	schopnost	k1gFnSc1	schopnost
látky	látka	k1gFnSc2	látka
sublimovat	sublimovat	k5eAaBmF	sublimovat
zjistíme	zjistit	k5eAaPmIp1nP	zjistit
z	z	k7c2	z
jejího	její	k3xOp3gInSc2	její
fázového	fázový	k2eAgInSc2d1	fázový
diagramu	diagram	k1gInSc2	diagram
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
je	být	k5eAaImIp3nS	být
příklad	příklad	k1gInSc1	příklad
chemické	chemický	k2eAgFnSc2d1	chemická
sloučeniny	sloučenina	k1gFnSc2	sloučenina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
sublimuje	sublimovat	k5eAaBmIp3nS	sublimovat
za	za	k7c2	za
atmosférického	atmosférický	k2eAgInSc2d1	atmosférický
tlaku	tlak	k1gInSc2	tlak
(	(	kIx(	(
<g/>
suchý	suchý	k2eAgInSc1d1	suchý
led	led	k1gInSc1	led
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sníh	sníh	k1gInSc1	sníh
a	a	k8xC	a
vodní	vodní	k2eAgInSc1d1	vodní
led	led	k1gInSc1	led
také	také	k9	také
sublimuje	sublimovat	k5eAaBmIp3nS	sublimovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podstatně	podstatně	k6eAd1	podstatně
pomaleji	pomale	k6eAd2	pomale
(	(	kIx(	(
<g/>
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
pod	pod	k7c7	pod
bodem	bod	k1gInSc7	bod
mrazu	mráz	k1gInSc2	mráz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
proto	proto	k8xC	proto
také	také	k9	také
prádlo	prádlo	k1gNnSc1	prádlo
uschne	uschnout	k5eAaPmIp3nS	uschnout
i	i	k9	i
za	za	k7c2	za
mrazu	mráz	k1gInSc2	mráz
<g/>
.	.	kIx.	.
</s>
<s>
Sublimace	sublimace	k1gFnSc1	sublimace
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
při	při	k7c6	při
čištění	čištění	k1gNnSc6	čištění
chemických	chemický	k2eAgFnPc2d1	chemická
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
směsí	směs	k1gFnPc2	směs
sublimujících	sublimující	k2eAgFnPc2d1	sublimující
a	a	k8xC	a
nesublimujících	sublimující	k2eNgFnPc2d1	sublimující
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
ohřívání	ohřívání	k1gNnSc6	ohřívání
a	a	k8xC	a
ochlazování	ochlazování	k1gNnSc6	ochlazování
směsi	směs	k1gFnSc2	směs
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
sublimace	sublimace	k1gFnSc2	sublimace
a	a	k8xC	a
desublimace	desublimace	k1gFnSc2	desublimace
jedné	jeden	k4xCgFnSc2	jeden
složky	složka	k1gFnSc2	složka
směsi	směs	k1gFnSc2	směs
<g/>
.	.	kIx.	.
</s>
<s>
Výsledný	výsledný	k2eAgInSc1d1	výsledný
produkt	produkt	k1gInSc1	produkt
po	po	k7c6	po
ochlazení	ochlazení	k1gNnSc6	ochlazení
a	a	k8xC	a
opětné	opětný	k2eAgFnSc6d1	opětná
přeměně	přeměna	k1gFnSc6	přeměna
zpátky	zpátky	k6eAd1	zpátky
do	do	k7c2	do
pevného	pevný	k2eAgNnSc2d1	pevné
skupenství	skupenství	k1gNnSc2	skupenství
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
sublimát	sublimát	k1gInSc1	sublimát
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
též	též	k9	též
sublimační	sublimační	k2eAgFnSc1d1	sublimační
metoda	metoda	k1gFnSc1	metoda
(	(	kIx(	(
<g/>
komerčního	komerční	k2eAgInSc2d1	komerční
<g/>
)	)	kIx)	)
potisku	potisk	k1gInSc2	potisk
textilií	textilie	k1gFnPc2	textilie
a	a	k8xC	a
plachtovin	plachtovina	k1gFnPc2	plachtovina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sublimaci	sublimace	k1gFnSc6	sublimace
je	být	k5eAaImIp3nS	být
založená	založený	k2eAgFnSc1d1	založená
lyofilizace	lyofilizace	k1gFnSc1	lyofilizace
<g/>
.	.	kIx.	.
</s>
<s>
Jod	jod	k1gInSc1	jod
znečištěný	znečištěný	k2eAgInSc1d1	znečištěný
např.	např.	kA	např.
pískem	písek	k1gInSc7	písek
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
kádince	kádinka	k1gFnSc6	kádinka
<g/>
,	,	kIx,	,
do	do	k7c2	do
jejíhož	jejíž	k3xOyRp3gInSc2	jejíž
otvoru	otvor	k1gInSc2	otvor
je	být	k5eAaImIp3nS	být
položena	položen	k2eAgFnSc1d1	položena
baňka	baňka	k1gFnSc1	baňka
s	s	k7c7	s
chladnou	chladný	k2eAgFnSc7d1	chladná
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Kádinka	kádinka	k1gFnSc1	kádinka
se	se	k3xPyFc4	se
začne	začít	k5eAaPmIp3nS	začít
ohřívat	ohřívat	k5eAaImF	ohřívat
nad	nad	k7c7	nad
kahanem	kahan	k1gInSc7	kahan
a	a	k8xC	a
jod	jod	k1gInSc1	jod
pomalu	pomalu	k6eAd1	pomalu
sublimuje	sublimovat	k5eAaBmIp3nS	sublimovat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
nafialovělé	nafialovělý	k2eAgInPc1d1	nafialovělý
páry	pár	k1gInPc1	pár
stoupají	stoupat	k5eAaImIp3nP	stoupat
vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	on	k3xPp3gInPc4	on
zastavuje	zastavovat	k5eAaImIp3nS	zastavovat
baňka	baňka	k1gFnSc1	baňka
s	s	k7c7	s
chladnou	chladný	k2eAgFnSc7d1	chladná
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
opět	opět	k6eAd1	opět
desublimují	desublimovat	k5eAaImIp3nP	desublimovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dně	dno	k1gNnSc6	dno
baňky	baňka	k1gFnSc2	baňka
(	(	kIx(	(
<g/>
z	z	k7c2	z
vnější	vnější	k2eAgFnSc2d1	vnější
strany	strana	k1gFnSc2	strana
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
přesublimovaný	přesublimovaný	k2eAgInSc4d1	přesublimovaný
čistý	čistý	k2eAgInSc4d1	čistý
jod	jod	k1gInSc4	jod
<g/>
.	.	kIx.	.
</s>
<s>
Termodynamika	termodynamika	k1gFnSc1	termodynamika
Skupenské	skupenský	k2eAgNnSc4d1	skupenské
teplo	teplo	k1gNnSc4	teplo
sublimace	sublimace	k1gFnSc2	sublimace
Měrné	měrný	k2eAgNnSc4d1	měrné
skupenské	skupenský	k2eAgNnSc4d1	skupenské
teplo	teplo	k1gNnSc4	teplo
sublimace	sublimace	k1gFnSc2	sublimace
Fázový	fázový	k2eAgInSc4d1	fázový
přechod	přechod	k1gInSc4	přechod
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
sublimace	sublimace	k1gFnSc2	sublimace
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
sublimace	sublimace	k1gFnSc2	sublimace
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
