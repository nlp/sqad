<s>
RMS	RMS	kA	RMS
(	(	kIx(	(
<g/>
Royal	Royal	k1gInSc1	Royal
Mail	mail	k1gInSc1	mail
Ship	Ship	k1gInSc1	Ship
<g/>
)	)	kIx)	)
<g/>
Titanic	Titanic	k1gInSc1	Titanic
byl	být	k5eAaImAgInS	být
zaoceánský	zaoceánský	k2eAgInSc1d1	zaoceánský
parník	parník	k1gInSc1	parník
třídy	třída	k1gFnSc2	třída
Olympic	Olympice	k1gFnPc2	Olympice
patřící	patřící	k2eAgFnSc2d1	patřící
společnosti	společnost	k1gFnSc2	společnost
White	Whit	k1gInSc5	Whit
Star	Star	kA	Star
Line	linout	k5eAaImIp3nS	linout
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
největší	veliký	k2eAgInSc4d3	veliký
osobní	osobní	k2eAgInSc4d1	osobní
parník	parník	k1gInSc4	parník
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
převoz	převoz	k1gInSc4	převoz
cestujících	cestující	k1gMnPc2	cestující
a	a	k8xC	a
pošty	pošta	k1gFnSc2	pošta
mezi	mezi	k7c7	mezi
Evropou	Evropa	k1gFnSc7	Evropa
a	a	k8xC	a
Severní	severní	k2eAgFnSc7d1	severní
Amerikou	Amerika	k1gFnSc7	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
trase	trasa	k1gFnSc6	trasa
měl	mít	k5eAaImAgMnS	mít
konkurovat	konkurovat	k5eAaImF	konkurovat
podobným	podobný	k2eAgInPc3d1	podobný
parníkům	parník	k1gInPc3	parník
společnosti	společnost	k1gFnSc2	společnost
Cunard	Cunard	k1gInSc1	Cunard
Line	linout	k5eAaImIp3nS	linout
-	-	kIx~	-
Mauretanii	Mauretanie	k1gFnSc3	Mauretanie
a	a	k8xC	a
Lusitanii	Lusitanie	k1gFnSc3	Lusitanie
<g/>
.	.	kIx.	.
</s>
<s>
Kapacita	kapacita	k1gFnSc1	kapacita
lodi	loď	k1gFnSc2	loď
dovolovala	dovolovat	k5eAaImAgFnS	dovolovat
převážet	převážet	k5eAaImF	převážet
2	[number]	k4	2
453	[number]	k4	453
až	až	k9	až
2	[number]	k4	2
603	[number]	k4	603
cestujících	cestující	k1gMnPc2	cestující
a	a	k8xC	a
omezený	omezený	k2eAgInSc1d1	omezený
počet	počet	k1gInSc1	počet
kočárů	kočár	k1gInPc2	kočár
nebo	nebo	k8xC	nebo
automobilů	automobil	k1gInPc2	automobil
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
provoz	provoz	k1gInSc4	provoz
lodě	loď	k1gFnSc2	loď
a	a	k8xC	a
o	o	k7c4	o
pohodlí	pohodlí	k1gNnSc4	pohodlí
cestujících	cestující	k1gFnPc2	cestující
se	se	k3xPyFc4	se
staralo	starat	k5eAaImAgNnS	starat
885	[number]	k4	885
až	až	k8xS	až
899	[number]	k4	899
členů	člen	k1gMnPc2	člen
posádky	posádka	k1gFnSc2	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Titanic	Titanic	k1gInSc1	Titanic
však	však	k9	však
ztroskotal	ztroskotat	k5eAaPmAgInS	ztroskotat
již	již	k6eAd1	již
během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
první	první	k4xOgFnSc2	první
plavby	plavba	k1gFnSc2	plavba
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1912	[number]	k4	1912
v	v	k7c4	v
23	[number]	k4	23
<g/>
:	:	kIx,	:
<g/>
40	[number]	k4	40
se	se	k3xPyFc4	se
parník	parník	k1gInSc1	parník
srazil	srazit	k5eAaPmAgInS	srazit
s	s	k7c7	s
ledovcem	ledovec	k1gInSc7	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
necelých	celý	k2eNgFnPc6d1	necelá
třech	tři	k4xCgFnPc6	tři
hodinách	hodina	k1gFnPc6	hodina
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
ránem	ráno	k1gNnSc7	ráno
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
v	v	k7c6	v
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
20	[number]	k4	20
<g/>
,	,	kIx,	,
klesl	klesnout	k5eAaPmAgInS	klesnout
ke	k	k7c3	k
dnu	dno	k1gNnSc3	dno
<g/>
.	.	kIx.	.
</s>
<s>
Zahynulo	zahynout	k5eAaPmAgNnS	zahynout
kolem	kolem	k6eAd1	kolem
1	[number]	k4	1
500	[number]	k4	500
cestujících	cestující	k1gMnPc2	cestující
a	a	k8xC	a
členů	člen	k1gMnPc2	člen
posádky	posádka	k1gFnSc2	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
vysokého	vysoký	k2eAgInSc2d1	vysoký
počtu	počet	k1gInSc2	počet
obětí	oběť	k1gFnPc2	oběť
byl	být	k5eAaImAgMnS	být
zejména	zejména	k9	zejména
nedostatek	nedostatek	k1gInSc4	nedostatek
záchranných	záchranný	k2eAgInPc2d1	záchranný
člunů	člun	k1gInPc2	člun
a	a	k8xC	a
špatná	špatný	k2eAgFnSc1d1	špatná
organizace	organizace	k1gFnSc1	organizace
záchranných	záchranný	k2eAgFnPc2d1	záchranná
prací	práce	k1gFnPc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Zkáze	zkáza	k1gFnSc3	zkáza
Titanicu	Titanicus	k1gInSc2	Titanicus
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
široké	široký	k2eAgFnPc4d1	široká
publicity	publicita	k1gFnPc4	publicita
pro	pro	k7c4	pro
velký	velký	k2eAgInSc4d1	velký
počet	počet	k1gInSc4	počet
obětí	oběť	k1gFnPc2	oběť
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgMnPc7	jenž
bylo	být	k5eAaImAgNnS	být
mnoho	mnoho	k4c1	mnoho
bohatých	bohatý	k2eAgMnPc2d1	bohatý
a	a	k8xC	a
známých	známý	k2eAgFnPc2d1	známá
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
legendám	legenda	k1gFnPc3	legenda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
kolem	kolem	k7c2	kolem
příčiny	příčina	k1gFnSc2	příčina
a	a	k8xC	a
průběhu	průběh	k1gInSc2	průběh
potopení	potopení	k1gNnSc2	potopení
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
po	po	k7c6	po
objevení	objevení	k1gNnSc6	objevení
zachovalého	zachovalý	k2eAgInSc2d1	zachovalý
vraku	vrak	k1gInSc2	vrak
lodi	loď	k1gFnSc2	loď
v	v	k7c6	v
hlubinách	hlubina	k1gFnPc6	hlubina
Atlantiku	Atlantik	k1gInSc2	Atlantik
<g/>
.	.	kIx.	.
</s>
<s>
Titanic	Titanic	k1gInSc1	Titanic
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
Belfastu	Belfast	k1gInSc6	Belfast
v	v	k7c6	v
loděnicích	loděnice	k1gFnPc6	loděnice
Harland	Harland	k1gInSc1	Harland
&	&	k?	&
Wolff	Wolff	k1gInSc1	Wolff
na	na	k7c4	na
objednávku	objednávka	k1gFnSc4	objednávka
společnosti	společnost	k1gFnSc2	společnost
White	Whit	k1gInSc5	Whit
Star	Star	kA	Star
Line	linout	k5eAaImIp3nS	linout
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
druhý	druhý	k4xOgMnSc1	druhý
ze	z	k7c2	z
série	série	k1gFnSc2	série
tří	tři	k4xCgFnPc2	tři
lodí	loď	k1gFnPc2	loď
třídy	třída	k1gFnSc2	třída
Olympic	Olympic	k1gMnSc1	Olympic
(	(	kIx(	(
<g/>
RMS	RMS	kA	RMS
Olympic	Olympic	k1gMnSc1	Olympic
<g/>
,	,	kIx,	,
RMS	RMS	kA	RMS
Titanic	Titanic	k1gInSc1	Titanic
a	a	k8xC	a
HMHS	HMHS	kA	HMHS
Britannic	Britannice	k1gFnPc2	Britannice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
těchto	tento	k3xDgFnPc2	tento
lodí	loď	k1gFnPc2	loď
byla	být	k5eAaImAgFnS	být
odpovědí	odpověď	k1gFnSc7	odpověď
konkurenční	konkurenční	k2eAgFnSc2d1	konkurenční
společnosti	společnost	k1gFnSc2	společnost
Cunard	Cunard	k1gMnSc1	Cunard
Line	linout	k5eAaImIp3nS	linout
vlastnící	vlastnící	k2eAgInPc4d1	vlastnící
parníky	parník	k1gInPc4	parník
RMS	RMS	kA	RMS
Lusitania	Lusitanium	k1gNnSc2	Lusitanium
a	a	k8xC	a
RMS	RMS	kA	RMS
Mauretania	Mauretanium	k1gNnSc2	Mauretanium
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc4	projekt
Titanicu	Titanicus	k1gInSc2	Titanicus
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Lord	lord	k1gMnSc1	lord
Pirrie	Pirrie	k1gFnSc2	Pirrie
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
ředitelem	ředitel	k1gMnSc7	ředitel
loděnic	loděnice	k1gFnPc2	loděnice
Harland	Harland	k1gInSc1	Harland
and	and	k?	and
Wolff	Wolff	k1gInSc1	Wolff
i	i	k8xC	i
plavební	plavební	k2eAgFnPc1d1	plavební
společnosti	společnost	k1gFnPc1	společnost
White	Whit	k1gInSc5	Whit
Star	Star	kA	Star
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
a	a	k8xC	a
konstrukci	konstrukce	k1gFnSc6	konstrukce
pracoval	pracovat	k5eAaImAgMnS	pracovat
šéfkonstruktér	šéfkonstruktér	k1gMnSc1	šéfkonstruktér
Thomas	Thomas	k1gMnSc1	Thomas
Andrews	Andrews	k1gInSc1	Andrews
<g/>
,	,	kIx,	,
stavbyvedoucí	stavbyvedoucí	k1gMnSc1	stavbyvedoucí
a	a	k8xC	a
vedoucí	vedoucí	k1gMnSc1	vedoucí
konstrukce	konstrukce	k1gFnSc2	konstrukce
Alexandr	Alexandr	k1gMnSc1	Alexandr
Carlisle	Carlisle	k1gMnSc1	Carlisle
<g/>
,	,	kIx,	,
generální	generální	k2eAgMnSc1d1	generální
manažer	manažer	k1gMnSc1	manažer
a	a	k8xC	a
vedoucí	vedoucí	k1gMnSc1	vedoucí
projektant	projektant	k1gMnSc1	projektant
odpovědný	odpovědný	k2eAgMnSc1d1	odpovědný
nejen	nejen	k6eAd1	nejen
za	za	k7c4	za
výzdobu	výzdoba	k1gFnSc4	výzdoba
a	a	k8xC	a
interiér	interiér	k1gInSc4	interiér
plavidla	plavidlo	k1gNnSc2	plavidlo
a	a	k8xC	a
který	který	k3yIgInSc1	který
taktéž	taktéž	k?	taktéž
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
mechanismus	mechanismus	k1gInSc1	mechanismus
spouštění	spouštění	k1gNnSc2	spouštění
záchranných	záchranný	k2eAgInPc2d1	záchranný
člunů	člun	k1gInPc2	člun
<g/>
.	.	kIx.	.
</s>
<s>
Carlisle	Carlisle	k6eAd1	Carlisle
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
ale	ale	k9	ale
odešel	odejít	k5eAaPmAgMnS	odejít
ke	k	k7c3	k
společnosti	společnost	k1gFnSc3	společnost
Welin	Welin	k2eAgInSc1d1	Welin
Davit	Davit	k1gInSc1	Davit
&	&	k?	&
Engineering	Engineering	k1gInSc1	Engineering
Company	Compana	k1gFnSc2	Compana
Ltd	ltd	kA	ltd
<g/>
.	.	kIx.	.
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
prací	práce	k1gFnPc2	práce
se	se	k3xPyFc4	se
na	na	k7c6	na
Titanicu	Titanicus	k1gInSc6	Titanicus
neúčastnil	účastnit	k5eNaImAgMnS	účastnit
<g/>
.	.	kIx.	.
</s>
<s>
Titanic	Titanic	k1gInSc1	Titanic
byl	být	k5eAaImAgInS	být
269,1	[number]	k4	269,1
metrů	metr	k1gInPc2	metr
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
a	a	k8xC	a
28,25	[number]	k4	28,25
metrů	metr	k1gInPc2	metr
široký	široký	k2eAgInSc1d1	široký
<g/>
.	.	kIx.	.
</s>
<s>
Hrubá	hrubý	k2eAgFnSc1d1	hrubá
tonáž	tonáž	k1gFnSc1	tonáž
představovala	představovat	k5eAaImAgFnS	představovat
46	[number]	k4	46
328	[number]	k4	328
BRT	BRT	k?	BRT
při	při	k7c6	při
výtlaku	výtlak	k1gInSc6	výtlak
52	[number]	k4	52
31	[number]	k4	31
tun	tuna	k1gFnPc2	tuna
při	při	k7c6	při
maximálním	maximální	k2eAgInSc6d1	maximální
ponoru	ponor	k1gInSc6	ponor
10,54	[number]	k4	10,54
m.	m.	k?	m.
Výška	výška	k1gFnSc1	výška
paluby	paluba	k1gFnSc2	paluba
od	od	k7c2	od
čáry	čára	k1gFnSc2	čára
ponoru	ponor	k1gInSc2	ponor
byla	být	k5eAaImAgFnS	být
18	[number]	k4	18
m.	m.	k?	m.
Tyto	tento	k3xDgFnPc1	tento
hodnoty	hodnota	k1gFnPc1	hodnota
vypovídají	vypovídat	k5eAaPmIp3nP	vypovídat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Titanic	Titanic	k1gInSc1	Titanic
byl	být	k5eAaImAgInS	být
větší	veliký	k2eAgFnSc4d2	veliký
než	než	k8xS	než
jeho	jeho	k3xOp3gMnSc1	jeho
předchůdce	předchůdce	k1gMnSc1	předchůdce
Olympic	Olympic	k1gMnSc1	Olympic
a	a	k8xC	a
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
největším	veliký	k2eAgNnSc7d3	veliký
námořním	námořní	k2eAgNnSc7d1	námořní
plavidlem	plavidlo	k1gNnSc7	plavidlo
<g/>
.	.	kIx.	.
</s>
<s>
Trup	trup	k1gInSc1	trup
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
důvodů	důvod	k1gInPc2	důvod
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
16	[number]	k4	16
záplavových	záplavový	k2eAgFnPc2d1	záplavová
komor	komora	k1gFnPc2	komora
a	a	k8xC	a
dno	dno	k1gNnSc1	dno
lodi	loď	k1gFnSc2	loď
bylo	být	k5eAaImAgNnS	být
zdvojeno	zdvojit	k5eAaPmNgNnS	zdvojit
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
konstrukce	konstrukce	k1gFnSc1	konstrukce
měla	mít	k5eAaImAgFnS	mít
Titanicu	Titanic	k1gMnSc3	Titanic
zajistit	zajistit	k5eAaPmF	zajistit
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
a	a	k8xC	a
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
představě	představa	k1gFnSc3	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nepotopitelný	potopitelný	k2eNgMnSc1d1	nepotopitelný
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
domněnku	domněnka	k1gFnSc4	domněnka
zveličil	zveličit	k5eAaPmAgMnS	zveličit
i	i	k9	i
odborný	odborný	k2eAgInSc4d1	odborný
časopis	časopis	k1gInSc4	časopis
Shipbuilder	Shipbuilder	k1gMnSc1	Shipbuilder
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
vodotěsných	vodotěsný	k2eAgFnPc2d1	vodotěsná
dveří	dveře	k1gFnPc2	dveře
v	v	k7c6	v
přepážkách	přepážka	k1gFnPc6	přepážka
se	se	k3xPyFc4	se
loď	loď	k1gFnSc1	loď
stává	stávat	k5eAaImIp3nS	stávat
prakticky	prakticky	k6eAd1	prakticky
nepotopitelnou	potopitelný	k2eNgFnSc7d1	nepotopitelná
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
udržet	udržet	k5eAaPmF	udržet
na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
i	i	k8xC	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
zaplavení	zaplavení	k1gNnSc2	zaplavení
až	až	k9	až
4	[number]	k4	4
vodotěsných	vodotěsný	k2eAgFnPc2d1	vodotěsná
komor	komora	k1gFnPc2	komora
současně	současně	k6eAd1	současně
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pohon	pohon	k1gInSc4	pohon
lodních	lodní	k2eAgInPc2d1	lodní
šroubů	šroub	k1gInPc2	šroub
<g/>
,	,	kIx,	,
pomocné	pomocný	k2eAgInPc4d1	pomocný
agregáty	agregát	k1gInPc4	agregát
a	a	k8xC	a
dodávku	dodávka	k1gFnSc4	dodávka
energie	energie	k1gFnSc2	energie
bylo	být	k5eAaImAgNnS	být
instalováno	instalovat	k5eAaBmNgNnS	instalovat
celkem	celkem	k6eAd1	celkem
29	[number]	k4	29
parních	parní	k2eAgInPc2d1	parní
kotlů	kotel	k1gInPc2	kotel
(	(	kIx(	(
<g/>
24	[number]	k4	24
oboustranných	oboustranný	k2eAgInPc2d1	oboustranný
parních	parní	k2eAgInPc2d1	parní
kotlů	kotel	k1gInPc2	kotel
se	s	k7c7	s
6	[number]	k4	6
topeništi	topeniště	k1gNnPc7	topeniště
a	a	k8xC	a
5	[number]	k4	5
jednostranných	jednostranný	k2eAgInPc2d1	jednostranný
kotlů	kotel	k1gInPc2	kotel
se	s	k7c7	s
3	[number]	k4	3
topeništi	topeniště	k1gNnPc7	topeniště
<g/>
)	)	kIx)	)
v	v	k7c6	v
6	[number]	k4	6
samostatných	samostatný	k2eAgFnPc6d1	samostatná
kotelnách	kotelna	k1gFnPc6	kotelna
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
se	s	k7c7	s
159	[number]	k4	159
topeništi	topeniště	k1gNnPc7	topeniště
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
kotle	kotel	k1gInPc1	kotel
měly	mít	k5eAaImAgInP	mít
průměr	průměr	k1gInSc1	průměr
4,79	[number]	k4	4,79
m.	m.	k?	m.
Délka	délka	k1gFnSc1	délka
oboustranných	oboustranný	k2eAgInPc2d1	oboustranný
kotlů	kotel	k1gInPc2	kotel
byla	být	k5eAaImAgFnS	být
6,08	[number]	k4	6,08
m	m	kA	m
a	a	k8xC	a
jednostranných	jednostranný	k2eAgInPc2d1	jednostranný
3,5	[number]	k4	3,5
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Kouř	kouř	k1gInSc1	kouř
z	z	k7c2	z
kotlů	kotel	k1gInPc2	kotel
byl	být	k5eAaImAgInS	být
odváděn	odvádět	k5eAaImNgInS	odvádět
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
<g/>
,	,	kIx,	,
19	[number]	k4	19
metrů	metr	k1gInPc2	metr
vysokých	vysoký	k2eAgInPc2d1	vysoký
<g/>
,	,	kIx,	,
komínů	komín	k1gInPc2	komín
s	s	k7c7	s
přirozeným	přirozený	k2eAgInSc7d1	přirozený
tahem	tah	k1gInSc7	tah
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
ventilátorů	ventilátor	k1gInPc2	ventilátor
se	se	k3xPyFc4	se
vháněl	vhánět	k5eAaImAgInS	vhánět
vzduch	vzduch	k1gInSc1	vzduch
pod	pod	k7c7	pod
rošty	rošt	k1gInPc7	rošt
topeniště	topeniště	k1gNnSc2	topeniště
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
tří	tři	k4xCgInPc2	tři
hlavních	hlavní	k2eAgInPc2d1	hlavní
komínů	komín	k1gInPc2	komín
byl	být	k5eAaImAgInS	být
instalován	instalovat	k5eAaBmNgInS	instalovat
i	i	k9	i
jeden	jeden	k4xCgInSc1	jeden
falešný	falešný	k2eAgInSc1d1	falešný
komín	komín	k1gInSc1	komín
na	na	k7c6	na
zádi	záď	k1gFnSc6	záď
sloužící	sloužící	k2eAgMnSc1d1	sloužící
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgInSc1d1	hlavní
vývod	vývod	k1gInSc1	vývod
ventilace	ventilace	k1gFnSc2	ventilace
a	a	k8xC	a
komín	komín	k1gInSc4	komín
kuchyně	kuchyně	k1gFnSc2	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
pak	pak	k6eAd1	pak
se	s	k7c7	s
čtyřmi	čtyři	k4xCgInPc7	čtyři
komíny	komín	k1gInPc7	komín
vypadala	vypadat	k5eAaImAgFnS	vypadat
větší	veliký	k2eAgFnSc1d2	veliký
a	a	k8xC	a
důstojnější	důstojný	k2eAgFnSc1d2	důstojnější
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kotlích	kotel	k1gInPc6	kotel
se	se	k3xPyFc4	se
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
pára	pára	k1gFnSc1	pára
pro	pro	k7c4	pro
2	[number]	k4	2
čtyřválcové	čtyřválcový	k2eAgInPc4d1	čtyřválcový
sdružené	sdružený	k2eAgInPc4d1	sdružený
parní	parní	k2eAgInPc4d1	parní
stroje	stroj	k1gInPc4	stroj
s	s	k7c7	s
třístupňovou	třístupňový	k2eAgFnSc7d1	třístupňová
postupnou	postupný	k2eAgFnSc7d1	postupná
expanzí	expanze	k1gFnSc7	expanze
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
15	[number]	k4	15
000	[number]	k4	000
koní	kůň	k1gMnPc2	kůň
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
reverzace	reverzace	k1gFnSc2	reverzace
chodu	chod	k1gInSc2	chod
<g/>
,	,	kIx,	,
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
nízkotlakou	nízkotlaký	k2eAgFnSc4d1	nízkotlaká
turbínu	turbína	k1gFnSc4	turbína
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
16	[number]	k4	16
000	[number]	k4	000
koní	kůň	k1gMnPc2	kůň
bez	bez	k7c2	bez
možnosti	možnost	k1gFnSc2	možnost
reverzace	reverzace	k1gFnSc2	reverzace
<g/>
.	.	kIx.	.
</s>
<s>
Parní	parní	k2eAgFnSc1d1	parní
turbína	turbína	k1gFnSc1	turbína
byla	být	k5eAaImAgFnS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
páté	pátý	k4xOgFnSc6	pátý
vodotěsné	vodotěsný	k2eAgFnSc6d1	vodotěsná
komoře	komora	k1gFnSc6	komora
od	od	k7c2	od
zádi	záď	k1gFnSc2	záď
<g/>
,	,	kIx,	,
pístové	pístový	k2eAgInPc1d1	pístový
stroje	stroj	k1gInPc1	stroj
v	v	k7c6	v
šesté	šestý	k4xOgFnSc6	šestý
<g/>
.	.	kIx.	.
</s>
<s>
Registrovaný	registrovaný	k2eAgInSc1d1	registrovaný
výkon	výkon	k1gInSc1	výkon
byl	být	k5eAaImAgInS	být
50	[number]	k4	50
000	[number]	k4	000
hp	hp	k?	hp
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sdružený	sdružený	k2eAgInSc1d1	sdružený
výkon	výkon	k1gInSc1	výkon
všech	všecek	k3xTgNnPc2	všecek
soustrojí	soustrojí	k1gNnPc2	soustrojí
byl	být	k5eAaImAgInS	být
odhadován	odhadovat	k5eAaImNgInS	odhadovat
až	až	k9	až
na	na	k7c4	na
55	[number]	k4	55
000	[number]	k4	000
hp	hp	k?	hp
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
měla	mít	k5eAaImAgFnS	mít
tři	tři	k4xCgInPc4	tři
lodní	lodní	k2eAgInPc4d1	lodní
šrouby	šroub	k1gInPc4	šroub
<g/>
,	,	kIx,	,
prostřední	prostřednět	k5eAaImIp3nS	prostřednět
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
22	[number]	k4	22
tun	tuna	k1gFnPc2	tuna
se	s	k7c7	s
čtyřmi	čtyři	k4xCgInPc7	čtyři
listy	list	k1gInPc7	list
<g/>
,	,	kIx,	,
krajní	krajní	k2eAgInPc1d1	krajní
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
38	[number]	k4	38
tun	tuna	k1gFnPc2	tuna
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
listy	list	k1gInPc7	list
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
rychlost	rychlost	k1gFnSc1	rychlost
lodi	loď	k1gFnSc2	loď
byla	být	k5eAaImAgFnS	být
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
24	[number]	k4	24
uzlů	uzel	k1gInPc2	uzel
(	(	kIx(	(
<g/>
44	[number]	k4	44
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tou	ten	k3xDgFnSc7	ten
však	však	k9	však
Titanic	Titanic	k1gInSc1	Titanic
nikdy	nikdy	k6eAd1	nikdy
neplul	plout	k5eNaImAgInS	plout
<g/>
.	.	kIx.	.
</s>
<s>
Kormidlo	kormidlo	k1gNnSc4	kormidlo
měl	mít	k5eAaImAgInS	mít
Titanic	Titanic	k1gInSc1	Titanic
pouze	pouze	k6eAd1	pouze
jedno	jeden	k4xCgNnSc1	jeden
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
bylo	být	k5eAaImAgNnS	být
krátké	krátké	k1gNnSc1	krátké
a	a	k8xC	a
malé	malé	k1gNnSc1	malé
plochou	plocha	k1gFnSc7	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Kormidlo	kormidlo	k1gNnSc1	kormidlo
se	se	k3xPyFc4	se
pohybovalo	pohybovat	k5eAaImAgNnS	pohybovat
ve	v	k7c6	v
čtvrtkruhu	čtvrtkruh	k1gInSc6	čtvrtkruh
(	(	kIx(	(
<g/>
±	±	k?	±
<g/>
45	[number]	k4	45
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
ovládáno	ovládat	k5eAaImNgNnS	ovládat
převodem	převod	k1gInSc7	převod
přes	přes	k7c4	přes
ozubené	ozubený	k2eAgNnSc4d1	ozubené
kolo	kolo	k1gNnSc4	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
omezení	omezení	k1gNnSc4	omezení
vibrací	vibrace	k1gFnPc2	vibrace
vznikajících	vznikající	k2eAgFnPc2d1	vznikající
prouděním	proudění	k1gNnSc7	proudění
vody	voda	k1gFnSc2	voda
za	za	k7c7	za
lodními	lodní	k2eAgInPc7d1	lodní
šrouby	šroub	k1gInPc7	šroub
a	a	k8xC	a
pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
rychlé	rychlý	k2eAgFnSc2d1	rychlá
změny	změna	k1gFnSc2	změna
polohy	poloha	k1gFnSc2	poloha
byly	být	k5eAaImAgInP	být
do	do	k7c2	do
mechanismu	mechanismus	k1gInSc2	mechanismus
vloženy	vložen	k2eAgFnPc1d1	vložena
tuhé	tuhý	k2eAgFnPc1d1	tuhá
pružiny	pružina	k1gFnPc1	pružina
<g/>
,	,	kIx,	,
tlumící	tlumící	k2eAgNnSc1d1	tlumící
tyto	tento	k3xDgFnPc4	tento
vibrace	vibrace	k1gFnPc4	vibrace
<g/>
.	.	kIx.	.
</s>
<s>
Kormidlo	kormidlo	k1gNnSc1	kormidlo
bylo	být	k5eAaImAgNnS	být
ovládáno	ovládat	k5eAaImNgNnS	ovládat
dvěma	dva	k4xCgInPc7	dva
samostatnými	samostatný	k2eAgInPc7d1	samostatný
parními	parní	k2eAgInPc7d1	parní
stroji	stroj	k1gInPc7	stroj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
byl	být	k5eAaImAgInS	být
ale	ale	k9	ale
využíván	využívat	k5eAaImNgInS	využívat
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgMnSc1	jeden
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
byl	být	k5eAaImAgInS	být
záložní	záložní	k2eAgInSc1d1	záložní
<g/>
,	,	kIx,	,
a	a	k8xC	a
pracovaly	pracovat	k5eAaImAgFnP	pracovat
na	na	k7c4	na
společný	společný	k2eAgInSc4d1	společný
hřídel	hřídel	k1gInSc4	hřídel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
nouze	nouze	k1gFnSc2	nouze
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
kormidlo	kormidlo	k1gNnSc1	kormidlo
ovládat	ovládat	k5eAaImF	ovládat
lanovými	lanový	k2eAgInPc7d1	lanový
převody	převod	k1gInPc7	převod
přes	přes	k7c4	přes
dva	dva	k4xCgInPc4	dva
vrátky	vrátek	k1gInPc4	vrátek
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
stavbou	stavba	k1gFnSc7	stavba
RMS	RMS	kA	RMS
Titanicu	Titanicum	k1gNnSc3	Titanicum
bylo	být	k5eAaImAgNnS	být
započato	započnout	k5eAaPmNgNnS	započnout
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1909	[number]	k4	1909
a	a	k8xC	a
stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
financována	financovat	k5eAaBmNgFnS	financovat
J.	J.	kA	J.
P.	P.	kA	P.
Morganem	morgan	k1gMnSc7	morgan
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc7	jeho
společností	společnost	k1gFnSc7	společnost
International	International	k1gMnSc5	International
Mercantile	Mercantil	k1gMnSc5	Mercantil
Marine	Marin	k1gMnSc5	Marin
Co	co	k3yRnSc4	co
<g/>
.	.	kIx.	.
</s>
<s>
Trup	trup	k1gInSc1	trup
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
spuštěn	spustit	k5eAaPmNgInS	spustit
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1911	[number]	k4	1911
ve	v	k7c4	v
12	[number]	k4	12
<g/>
:	:	kIx,	:
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
práce	práce	k1gFnPc1	práce
na	na	k7c4	na
dokončení	dokončení	k1gNnSc4	dokončení
trvaly	trvat	k5eAaImAgFnP	trvat
téměř	téměř	k6eAd1	téměř
další	další	k2eAgInSc4d1	další
rok	rok	k1gInSc4	rok
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
dokončeny	dokončit	k5eAaPmNgInP	dokončit
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
následovala	následovat	k5eAaImAgFnS	následovat
zkušební	zkušební	k2eAgFnSc1d1	zkušební
plavba	plavba	k1gFnSc1	plavba
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
při	při	k7c6	při
návrhu	návrh	k1gInSc6	návrh
interiéru	interiér	k1gInSc2	interiér
a	a	k8xC	a
vybavení	vybavení	k1gNnSc2	vybavení
pro	pro	k7c4	pro
Titanic	Titanic	k1gInSc4	Titanic
se	se	k3xPyFc4	se
počítalo	počítat	k5eAaImAgNnS	počítat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
představovat	představovat	k5eAaImF	představovat
luxus	luxus	k1gInSc4	luxus
a	a	k8xC	a
bohatství	bohatství	k1gNnSc4	bohatství
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
byla	být	k5eAaImAgFnS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
palubním	palubní	k2eAgInSc7d1	palubní
telefonním	telefonní	k2eAgInSc7d1	telefonní
komunikačním	komunikační	k2eAgInSc7d1	komunikační
systémem	systém	k1gInSc7	systém
<g/>
,	,	kIx,	,
knihovnou	knihovna	k1gFnSc7	knihovna
i	i	k8xC	i
holičstvím	holičství	k1gNnSc7	holičství
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
cestující	cestující	k1gFnSc4	cestující
první	první	k4xOgFnSc2	první
třídy	třída	k1gFnSc2	třída
byl	být	k5eAaImAgInS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
i	i	k8xC	i
bazén	bazén	k1gInSc1	bazén
<g/>
,	,	kIx,	,
tělocvična	tělocvična	k1gFnSc1	tělocvična
<g/>
,	,	kIx,	,
hřiště	hřiště	k1gNnSc1	hřiště
na	na	k7c4	na
squash	squash	k1gInSc4	squash
<g/>
,	,	kIx,	,
turecké	turecký	k2eAgFnPc1d1	turecká
lázně	lázeň	k1gFnPc1	lázeň
<g/>
,	,	kIx,	,
elektrické	elektrický	k2eAgFnPc1d1	elektrická
lázně	lázeň	k1gFnPc1	lázeň
a	a	k8xC	a
Veranda	veranda	k1gFnSc1	veranda
Cafe	Cafe	k1gFnSc1	Cafe
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
Café	café	k1gNnPc2	café
Parisien	Parisino	k1gNnPc2	Parisino
nabízelo	nabízet	k5eAaImAgNnS	nabízet
speciality	specialita	k1gFnPc4	specialita
pro	pro	k7c4	pro
cestující	cestující	k2eAgFnPc4d1	cestující
první	první	k4xOgFnPc4	první
třídy	třída	k1gFnPc4	třída
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
mohli	moct	k5eAaImAgMnP	moct
konzumovat	konzumovat	k5eAaBmF	konzumovat
na	na	k7c6	na
prosluněné	prosluněný	k2eAgFnSc6d1	prosluněná
verandě	veranda	k1gFnSc6	veranda
s	s	k7c7	s
dekorovaným	dekorovaný	k2eAgNnSc7d1	dekorované
mřížovím	mřížoví	k1gNnSc7	mřížoví
<g/>
.	.	kIx.	.
</s>
<s>
Společenské	společenský	k2eAgFnPc1d1	společenská
místnosti	místnost	k1gFnPc1	místnost
první	první	k4xOgFnSc2	první
třídy	třída	k1gFnSc2	třída
byly	být	k5eAaImAgInP	být
obloženy	obložit	k5eAaPmNgInP	obložit
vyřezávaným	vyřezávaný	k2eAgNnSc7d1	vyřezávané
dřevěným	dřevěný	k2eAgNnSc7d1	dřevěné
obložením	obložení	k1gNnSc7	obložení
<g/>
,	,	kIx,	,
drahým	drahý	k2eAgInSc7d1	drahý
nábytkem	nábytek	k1gInSc7	nábytek
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
dekoracemi	dekorace	k1gFnPc7	dekorace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
třídě	třída	k1gFnSc6	třída
bylo	být	k5eAaImAgNnS	být
vybavení	vybavení	k1gNnSc1	vybavení
skromné	skromný	k2eAgNnSc1d1	skromné
<g/>
,	,	kIx,	,
borovicové	borovicový	k2eAgNnSc1d1	borovicové
obložení	obložení	k1gNnSc1	obložení
a	a	k8xC	a
robustní	robustní	k2eAgInSc1d1	robustní
teakový	teakový	k2eAgInSc1d1	teakový
nábytek	nábytek	k1gInSc1	nábytek
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
rozdílu	rozdíl	k1gInSc6	rozdíl
vybavenosti	vybavenost	k1gFnSc2	vybavenost
kajut	kajuta	k1gFnPc2	kajuta
a	a	k8xC	a
poskytovaného	poskytovaný	k2eAgInSc2d1	poskytovaný
komfortu	komfort	k1gInSc2	komfort
svědčí	svědčit	k5eAaImIp3nS	svědčit
i	i	k9	i
cenové	cenový	k2eAgInPc4d1	cenový
rozdíly	rozdíl	k1gInPc4	rozdíl
lodních	lodní	k2eAgInPc2d1	lodní
lístků	lístek	k1gInPc2	lístek
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
salónní	salónní	k2eAgFnSc2d1	salónní
kajuty	kajuta	k1gFnSc2	kajuta
<g/>
:	:	kIx,	:
4	[number]	k4	4
350	[number]	k4	350
USD	USD	kA	USD
/	/	kIx~	/
870	[number]	k4	870
liber	libra	k1gFnPc2	libra
šterlinků	šterlink	k1gInPc2	šterlink
(	(	kIx(	(
<g/>
v	v	k7c6	v
dnešních	dnešní	k2eAgFnPc6d1	dnešní
cenách	cena	k1gFnPc6	cena
128	[number]	k4	128
000	[number]	k4	000
USD	USD	kA	USD
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
kajuta	kajuta	k1gFnSc1	kajuta
v	v	k7c6	v
první	první	k4xOgFnSc6	první
třídě	třída	k1gFnSc6	třída
<g/>
:	:	kIx,	:
150	[number]	k4	150
USD	USD	kA	USD
/	/	kIx~	/
30	[number]	k4	30
liber	libra	k1gFnPc2	libra
(	(	kIx(	(
<g/>
v	v	k7c6	v
dnešních	dnešní	k2eAgFnPc6d1	dnešní
cenách	cena	k1gFnPc6	cena
4	[number]	k4	4
400	[number]	k4	400
USD	USD	kA	USD
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
kajuta	kajuta	k1gFnSc1	kajuta
v	v	k7c4	v
druhé	druhý	k4xOgInPc4	druhý
<g />
.	.	kIx.	.
</s>
<s>
třídě	třída	k1gFnSc3	třída
<g/>
:	:	kIx,	:
60	[number]	k4	60
USD	USD	kA	USD
/	/	kIx~	/
13	[number]	k4	13
liber	libra	k1gFnPc2	libra
(	(	kIx(	(
<g/>
v	v	k7c6	v
dnešních	dnešní	k2eAgFnPc6d1	dnešní
cenách	cena	k1gFnPc6	cena
1	[number]	k4	1
900	[number]	k4	900
USD	USD	kA	USD
<g/>
)	)	kIx)	)
a	a	k8xC	a
palubní	palubní	k2eAgInSc4d1	palubní
lístek	lístek	k1gInSc4	lístek
pro	pro	k7c4	pro
třetí	třetí	k4xOgFnSc4	třetí
třídu	třída	k1gFnSc4	třída
<g/>
:	:	kIx,	:
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
40	[number]	k4	40
USD	USD	kA	USD
/	/	kIx~	/
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
liber	libra	k1gFnPc2	libra
(	(	kIx(	(
<g/>
v	v	k7c6	v
dnešních	dnešní	k2eAgFnPc6d1	dnešní
cenách	cena	k1gFnPc6	cena
440	[number]	k4	440
<g/>
-	-	kIx~	-
<g/>
1170	[number]	k4	1170
USD	USD	kA	USD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
byla	být	k5eAaImAgFnS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
tehdy	tehdy	k6eAd1	tehdy
špičkovou	špičkový	k2eAgFnSc7d1	špičková
technologií	technologie	k1gFnSc7	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tyto	tento	k3xDgFnPc4	tento
vymoženosti	vymoženost	k1gFnPc4	vymoženost
patřily	patřit	k5eAaImAgInP	patřit
i	i	k8xC	i
tři	tři	k4xCgInPc1	tři
elektrické	elektrický	k2eAgInPc1d1	elektrický
výtahy	výtah	k1gInPc1	výtah
v	v	k7c6	v
první	první	k4xOgFnSc6	první
třídě	třída	k1gFnSc6	třída
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
ve	v	k7c6	v
třídě	třída	k1gFnSc6	třída
druhé	druhý	k4xOgFnSc2	druhý
a	a	k8xC	a
loď	loď	k1gFnSc1	loď
byla	být	k5eAaImAgFnS	být
kompletně	kompletně	k6eAd1	kompletně
vybavena	vybavit	k5eAaPmNgFnS	vybavit
elektrickým	elektrický	k2eAgNnSc7d1	elektrické
osvětlením	osvětlení	k1gNnSc7	osvětlení
<g/>
.	.	kIx.	.
</s>
<s>
Komunikaci	komunikace	k1gFnSc4	komunikace
lodě	loď	k1gFnSc2	loď
s	s	k7c7	s
okolním	okolní	k2eAgInSc7d1	okolní
světem	svět	k1gInSc7	svět
zajišťovaly	zajišťovat	k5eAaImAgInP	zajišťovat
dvě	dva	k4xCgFnPc4	dva
rádiové	rádiový	k2eAgFnPc4d1	rádiová
stanice	stanice	k1gFnPc4	stanice
Marconi	Marcon	k1gMnPc1	Marcon
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
5	[number]	k4	5
000	[number]	k4	000
wattů	watt	k1gInPc2	watt
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jedné	jeden	k4xCgFnSc2	jeden
stanice	stanice	k1gFnSc2	stanice
se	se	k3xPyFc4	se
ve	v	k7c6	v
směnách	směna	k1gFnPc6	směna
střídali	střídat	k5eAaImAgMnP	střídat
dva	dva	k4xCgMnPc1	dva
operátoři	operátor	k1gMnPc1	operátor
společnosti	společnost	k1gFnSc2	společnost
Marconi	Marcon	k1gMnPc1	Marcon
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc7	jejich
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
odesílání	odesílání	k1gNnSc1	odesílání
a	a	k8xC	a
přijímání	přijímání	k1gNnSc1	přijímání
osobních	osobní	k2eAgFnPc2d1	osobní
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
telegrafická	telegrafický	k2eAgFnSc1d1	telegrafická
služba	služba	k1gFnSc1	služba
byla	být	k5eAaImAgFnS	být
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
cestující	cestující	k1gFnSc4	cestující
první	první	k4xOgFnSc2	první
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Nejdražší	drahý	k2eAgInSc1d3	nejdražší
jednosměrný	jednosměrný	k2eAgInSc1d1	jednosměrný
transatlantický	transatlantický	k2eAgInSc1d1	transatlantický
přenos	přenos	k1gInSc1	přenos
stál	stát	k5eAaImAgInS	stát
870	[number]	k4	870
liber	libra	k1gFnPc2	libra
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
63	[number]	k4	63
837	[number]	k4	837
librám	libra	k1gFnPc3	libra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
1	[number]	k4	1
363	[number]	k4	363
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
30	[number]	k4	30
917	[number]	k4	917
dolarů	dolar	k1gInPc2	dolar
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
vybavení	vybavení	k1gNnSc3	vybavení
patřil	patřit	k5eAaImAgInS	patřit
prostor	prostor	k1gInSc1	prostor
pro	pro	k7c4	pro
venčení	venčení	k1gNnSc4	venčení
psů	pes	k1gMnPc2	pes
<g/>
,	,	kIx,	,
nemocnice	nemocnice	k1gFnPc1	nemocnice
s	s	k7c7	s
operačním	operační	k2eAgInSc7d1	operační
sálem	sál	k1gInSc7	sál
místo	místo	k7c2	místo
běžné	běžný	k2eAgFnSc2d1	běžná
lodní	lodní	k2eAgFnSc2d1	lodní
ošetřovny	ošetřovna	k1gFnSc2	ošetřovna
<g/>
,	,	kIx,	,
mrazící	mrazící	k2eAgNnSc1d1	mrazící
skladiště	skladiště	k1gNnSc1	skladiště
řezaných	řezaný	k2eAgFnPc2d1	řezaná
květin	květina	k1gFnPc2	květina
<g/>
,	,	kIx,	,
ubytovací	ubytovací	k2eAgFnPc4d1	ubytovací
prostory	prostora	k1gFnPc4	prostora
pro	pro	k7c4	pro
služebnictvo	služebnictvo	k1gNnSc4	služebnictvo
první	první	k4xOgFnSc2	první
třídy	třída	k1gFnSc2	třída
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
RMS	RMS	kA	RMS
Titanic	Titanic	k1gInSc1	Titanic
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jiná	jiný	k2eAgNnPc4d1	jiné
plavidla	plavidlo	k1gNnPc4	plavidlo
pro	pro	k7c4	pro
přepravu	přeprava	k1gFnSc4	přeprava
cestujících	cestující	k1gMnPc2	cestující
po	po	k7c6	po
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
záchrannými	záchranný	k2eAgInPc7d1	záchranný
čluny	člun	k1gInPc7	člun
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
White	Whit	k1gInSc5	Whit
Star	Star	kA	Star
při	při	k7c6	při
schvalování	schvalování	k1gNnSc6	schvalování
počtu	počet	k1gInSc2	počet
záchranných	záchranný	k2eAgInPc2d1	záchranný
člunů	člun	k1gInPc2	člun
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
kapacity	kapacita	k1gFnSc2	kapacita
vycházela	vycházet	k5eAaImAgNnP	vycházet
z	z	k7c2	z
tehdy	tehdy	k6eAd1	tehdy
platných	platný	k2eAgInPc2d1	platný
námořních	námořní	k2eAgInPc2d1	námořní
předpisů	předpis	k1gInPc2	předpis
a	a	k8xC	a
požadavků	požadavek	k1gInPc2	požadavek
Board	Boarda	k1gFnPc2	Boarda
of	of	k?	of
Trade	Trad	k1gInSc5	Trad
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
předpisy	předpis	k1gInPc1	předpis
vyžadovaly	vyžadovat	k5eAaImAgInP	vyžadovat
pouze	pouze	k6eAd1	pouze
takovou	takový	k3xDgFnSc4	takový
kapacitu	kapacita	k1gFnSc4	kapacita
záchranných	záchranný	k2eAgInPc2d1	záchranný
člunů	člun	k1gInPc2	člun
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
výtlaku	výtlak	k1gInSc2	výtlak
plavidla	plavidlo	k1gNnSc2	plavidlo
a	a	k8xC	a
předpokládanému	předpokládaný	k2eAgNnSc3d1	předpokládané
množství	množství	k1gNnSc3	množství
přepravovaných	přepravovaný	k2eAgMnPc2d1	přepravovaný
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
určení	určení	k1gNnSc4	určení
byly	být	k5eAaImAgFnP	být
ale	ale	k8xC	ale
nesprávně	správně	k6eNd1	správně
použity	použit	k2eAgInPc4d1	použit
údaje	údaj	k1gInPc4	údaj
od	od	k7c2	od
sesterské	sesterský	k2eAgFnSc2d1	sesterská
lodi	loď	k1gFnSc2	loď
Olympic	Olympice	k1gFnPc2	Olympice
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
počet	počet	k1gInSc1	počet
cestujících	cestující	k1gMnPc2	cestující
na	na	k7c4	na
Olympicu	Olympica	k1gFnSc4	Olympica
nepřesáhl	přesáhnout	k5eNaPmAgMnS	přesáhnout
1	[number]	k4	1
100	[number]	k4	100
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
byla	být	k5eAaImAgFnS	být
instalace	instalace	k1gFnSc1	instalace
pouze	pouze	k6eAd1	pouze
20	[number]	k4	20
záchranných	záchranný	k2eAgInPc2d1	záchranný
člunů	člun	k1gInPc2	člun
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
kapacitě	kapacita	k1gFnSc6	kapacita
1	[number]	k4	1
178	[number]	k4	178
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
celkové	celkový	k2eAgFnSc2d1	celková
kapacity	kapacita	k1gFnSc2	kapacita
Titanicu	Titanicus	k1gInSc2	Titanicus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
katastrofy	katastrofa	k1gFnSc2	katastrofa
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
Titanicu	Titanicus	k1gInSc6	Titanicus
přes	přes	k7c4	přes
2	[number]	k4	2
200	[number]	k4	200
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
platných	platný	k2eAgInPc2d1	platný
předpisů	předpis	k1gInPc2	předpis
Board	Boarda	k1gFnPc2	Boarda
of	of	k?	of
Trade	Trad	k1gInSc5	Trad
bylo	být	k5eAaImAgNnS	být
stanovení	stanovení	k1gNnSc1	stanovení
počtu	počet	k1gInSc2	počet
záchranných	záchranný	k2eAgInPc2d1	záchranný
člunů	člun	k1gInPc2	člun
správné	správný	k2eAgNnSc1d1	správné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
britské	britský	k2eAgFnPc1d1	britská
lodě	loď	k1gFnPc1	loď
o	o	k7c6	o
výtlaku	výtlak	k1gInSc6	výtlak
nad	nad	k7c7	nad
10	[number]	k4	10
000	[number]	k4	000
tun	tuna	k1gFnPc2	tuna
měly	mít	k5eAaImAgFnP	mít
nést	nést	k5eAaImF	nést
16	[number]	k4	16
člunů	člun	k1gInPc2	člun
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
prostornosti	prostornost	k1gFnSc6	prostornost
5	[number]	k4	5
500	[number]	k4	500
ft3	ft3	k4	ft3
(	(	kIx(	(
<g/>
krychlových	krychlový	k2eAgFnPc2d1	krychlová
stop	stopa	k1gFnPc2	stopa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
160	[number]	k4	160
m3	m3	k4	m3
a	a	k8xC	a
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
další	další	k2eAgInPc1d1	další
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
kapacitu	kapacita	k1gFnSc4	kapacita
jiných	jiný	k2eAgNnPc2d1	jiné
záchranných	záchranný	k2eAgNnPc2d1	záchranné
plavidel	plavidlo	k1gNnPc2	plavidlo
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
75	[number]	k4	75
%	%	kIx~	%
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
snížena	snížit	k5eAaPmNgFnS	snížit
na	na	k7c4	na
50	[number]	k4	50
%	%	kIx~	%
u	u	k7c2	u
lodí	loď	k1gFnPc2	loď
s	s	k7c7	s
vodotěsnými	vodotěsný	k2eAgFnPc7d1	vodotěsná
přepážkami	přepážka	k1gFnPc7	přepážka
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
kapacitě	kapacita	k1gFnSc3	kapacita
záchranných	záchranný	k2eAgInPc2d1	záchranný
člunů	člun	k1gInPc2	člun
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
mohla	moct	k5eAaImAgFnS	moct
společnost	společnost	k1gFnSc1	společnost
White	Whit	k1gInSc5	Whit
Star	Star	kA	Star
Line	linout	k5eAaImIp3nS	linout
využít	využít	k5eAaPmF	využít
výjimky	výjimka	k1gFnPc4	výjimka
pro	pro	k7c4	pro
lodě	loď	k1gFnPc4	loď
s	s	k7c7	s
vodotěsnými	vodotěsný	k2eAgFnPc7d1	vodotěsná
přepážkami	přepážka	k1gFnPc7	přepážka
a	a	k8xC	a
celkovou	celkový	k2eAgFnSc4d1	celková
kapacitu	kapacita	k1gFnSc4	kapacita
v	v	k7c6	v
záchranných	záchranný	k2eAgInPc6d1	záchranný
prostředcích	prostředek	k1gInPc6	prostředek
snížit	snížit	k5eAaPmF	snížit
až	až	k6eAd1	až
na	na	k7c4	na
756	[number]	k4	756
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
těchto	tento	k3xDgInPc2	tento
předpisů	předpis	k1gInPc2	předpis
mohla	moct	k5eAaImAgFnS	moct
společnost	společnost	k1gFnSc1	společnost
White	Whit	k1gInSc5	Whit
Star	Star	kA	Star
Line	linout	k5eAaImIp3nS	linout
instalovat	instalovat	k5eAaBmF	instalovat
i	i	k9	i
méně	málo	k6eAd2	málo
člunů	člun	k1gInPc2	člun
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
jich	on	k3xPp3gFnPc2	on
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
jí	on	k3xPp3gFnSc3	on
bylo	být	k5eAaImAgNnS	být
předpisy	předpis	k1gInPc4	předpis
ukládáno	ukládán	k2eAgNnSc1d1	ukládáno
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
toto	tento	k3xDgNnSc4	tento
navýšení	navýšení	k1gNnSc4	navýšení
<g/>
,	,	kIx,	,
nad	nad	k7c4	nad
požadavek	požadavek	k1gInSc4	požadavek
předpisů	předpis	k1gInPc2	předpis
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
posléze	posléze	k6eAd1	posléze
dostačující	dostačující	k2eAgNnSc1d1	dostačující
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
20	[number]	k4	20
člunů	člun	k1gInPc2	člun
bylo	být	k5eAaImAgNnS	být
14	[number]	k4	14
velkých	velký	k2eAgInPc2d1	velký
dřevěných	dřevěný	k2eAgInPc2d1	dřevěný
člunů	člun	k1gInPc2	člun
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
65	[number]	k4	65
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
2	[number]	k4	2
tzv.	tzv.	kA	tzv.
pohotovostní	pohotovostní	k2eAgInPc4d1	pohotovostní
kutry	kutr	k1gInPc4	kutr
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
40	[number]	k4	40
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
4	[number]	k4	4
Engelhardtovy	Engelhardtův	k2eAgInPc4d1	Engelhardtův
skládací	skládací	k2eAgInPc4d1	skládací
čluny	člun	k1gInPc4	člun
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
47	[number]	k4	47
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
měly	mít	k5eAaImAgFnP	mít
plátěné	plátěný	k2eAgFnPc1d1	plátěná
bočnice	bočnice	k1gFnPc1	bočnice
s	s	k7c7	s
korkovou	korkový	k2eAgFnSc7d1	korková
obrubou	obruba	k1gFnSc7	obruba
a	a	k8xC	a
mohly	moct	k5eAaImAgFnP	moct
sloužit	sloužit	k5eAaImF	sloužit
buď	buď	k8xC	buď
jako	jako	k9	jako
vor	vor	k1gInSc4	vor
při	při	k7c6	při
spuštění	spuštění	k1gNnSc6	spuštění
bočnic	bočnice	k1gFnPc2	bočnice
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
člun	člun	k1gInSc1	člun
při	při	k7c6	při
jejich	jejich	k3xOp3gNnSc6	jejich
zvednutí	zvednutí	k1gNnSc6	zvednutí
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgInPc1d1	velký
čluny	člun	k1gInPc1	člun
a	a	k8xC	a
kutry	kutr	k1gInPc1	kutr
byly	být	k5eAaImAgInP	být
označeny	označit	k5eAaPmNgInP	označit
čísly	číslo	k1gNnPc7	číslo
1	[number]	k4	1
až	až	k8xS	až
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
Čluny	člun	k1gInPc1	člun
se	s	k7c7	s
sudými	sudý	k2eAgNnPc7d1	sudé
čísly	číslo	k1gNnPc7	číslo
byly	být	k5eAaImAgInP	být
upevněny	upevněn	k2eAgInPc1d1	upevněn
na	na	k7c6	na
levoboku	levobok	k1gInSc6	levobok
<g/>
,	,	kIx,	,
s	s	k7c7	s
lichými	lichý	k2eAgInPc7d1	lichý
na	na	k7c6	na
pravoboku	pravobok	k1gInSc6	pravobok
<g/>
.	.	kIx.	.
</s>
<s>
Skládací	skládací	k2eAgInPc1d1	skládací
čluny	člun	k1gInPc1	člun
byly	být	k5eAaImAgInP	být
označeny	označit	k5eAaPmNgInP	označit
písmeny	písmeno	k1gNnPc7	písmeno
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
,	,	kIx,	,
C	C	kA	C
a	a	k8xC	a
D.	D.	kA	D.
Dva	dva	k4xCgInPc1	dva
pohotovostní	pohotovostní	k2eAgInPc1d1	pohotovostní
kutry	kutr	k1gInPc1	kutr
byly	být	k5eAaImAgInP	být
úplně	úplně	k6eAd1	úplně
vepředu	vepředu	k6eAd1	vepředu
vedle	vedle	k7c2	vedle
kormidelny	kormidelna	k1gFnSc2	kormidelna
<g/>
,	,	kIx,	,
po	po	k7c6	po
jednom	jeden	k4xCgMnSc6	jeden
na	na	k7c6	na
pravoboku	pravobok	k1gInSc2	pravobok
a	a	k8xC	a
jednom	jeden	k4xCgNnSc6	jeden
na	na	k7c6	na
levoboku	levobok	k1gMnSc6	levobok
<g/>
.	.	kIx.	.
</s>
<s>
Čtrnáct	čtrnáct	k4xCc4	čtrnáct
velkých	velký	k2eAgInPc2d1	velký
člunů	člun	k1gInPc2	člun
bylo	být	k5eAaImAgNnS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
po	po	k7c6	po
sedmi	sedm	k4xCc6	sedm
na	na	k7c4	na
každý	každý	k3xTgInSc4	každý
bok	bok	k1gInSc4	bok
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
tři	tři	k4xCgMnPc1	tři
vpředu	vpředu	k6eAd1	vpředu
a	a	k8xC	a
čtyři	čtyři	k4xCgMnPc1	čtyři
vzadu	vzadu	k6eAd1	vzadu
<g/>
.	.	kIx.	.
</s>
<s>
Skládací	skládací	k2eAgInPc1d1	skládací
čluny	člun	k1gInPc1	člun
byly	být	k5eAaImAgInP	být
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
po	po	k7c6	po
dvou	dva	k4xCgInPc2	dva
na	na	k7c4	na
pravobok	pravobok	k1gInSc4	pravobok
a	a	k8xC	a
levobok	levobok	k1gInSc4	levobok
<g/>
.	.	kIx.	.
</s>
<s>
Čluny	člun	k1gInPc1	člun
C	C	kA	C
a	a	k8xC	a
D	D	kA	D
byly	být	k5eAaImAgFnP	být
uloženy	uložit	k5eAaPmNgFnP	uložit
vedle	vedle	k7c2	vedle
kutrů	kutr	k1gInPc2	kutr
<g/>
,	,	kIx,	,
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
řadě	řada	k1gFnSc6	řada
<g/>
,	,	kIx,	,
a	a	k8xC	a
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
spuštěny	spuštěn	k2eAgInPc1d1	spuštěn
stejnými	stejný	k2eAgInPc7d1	stejný
výložníky	výložník	k1gInPc7	výložník
<g/>
.	.	kIx.	.
</s>
<s>
Skládací	skládací	k2eAgInPc1d1	skládací
čluny	člun	k1gInPc1	člun
A	A	kA	A
a	a	k8xC	a
B	B	kA	B
byly	být	k5eAaImAgFnP	být
nevýhodně	výhodně	k6eNd1	výhodně
umístěny	umístit	k5eAaPmNgInP	umístit
až	až	k9	až
na	na	k7c6	na
střeše	střecha	k1gFnSc6	střecha
důstojnických	důstojnický	k2eAgFnPc2d1	důstojnická
kajut	kajuta	k1gFnPc2	kajuta
<g/>
,	,	kIx,	,
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
řadě	řada	k1gFnSc6	řada
<g/>
,	,	kIx,	,
a	a	k8xC	a
i	i	k9	i
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
podmínek	podmínka	k1gFnPc2	podmínka
bylo	být	k5eAaImAgNnS	být
obtížné	obtížný	k2eAgInPc4d1	obtížný
tyto	tento	k3xDgInPc4	tento
čluny	člun	k1gInPc4	člun
<g/>
,	,	kIx,	,
vážící	vážící	k2eAgInPc4d1	vážící
asi	asi	k9	asi
1	[number]	k4	1
350	[number]	k4	350
kilogramů	kilogram	k1gInPc2	kilogram
<g/>
,	,	kIx,	,
dostat	dostat	k5eAaPmF	dostat
na	na	k7c4	na
člunovou	člunový	k2eAgFnSc4d1	člunová
palubu	paluba	k1gFnSc4	paluba
mezi	mezi	k7c4	mezi
ramena	rameno	k1gNnPc4	rameno
výložníků	výložník	k1gInPc2	výložník
a	a	k8xC	a
spustit	spustit	k5eAaPmF	spustit
na	na	k7c4	na
hladinu	hladina	k1gFnSc4	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Titanic	Titanic	k1gInSc1	Titanic
byl	být	k5eAaImAgInS	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
moderními	moderní	k2eAgInPc7d1	moderní
Welinovými	Welinův	k2eAgInPc7d1	Welinův
dvojitými	dvojitý	k2eAgInPc7d1	dvojitý
výložníky	výložník	k1gInPc7	výložník
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnPc1	jejich
ramena	rameno	k1gNnPc1	rameno
mohla	moct	k5eAaImAgNnP	moct
být	být	k5eAaImF	být
vykloněna	vykloněn	k2eAgNnPc1d1	vykloněn
na	na	k7c4	na
obě	dva	k4xCgFnPc4	dva
dvě	dva	k4xCgFnPc4	dva
strany	strana	k1gFnPc4	strana
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
obsluhovat	obsluhovat	k5eAaImF	obsluhovat
druhou	druhý	k4xOgFnSc4	druhý
řadu	řada	k1gFnSc4	řada
záchranných	záchranný	k2eAgMnPc2d1	záchranný
člunů	člun	k1gInPc2	člun
umístěnou	umístěný	k2eAgFnSc7d1	umístěná
uvnitř	uvnitř	k6eAd1	uvnitř
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
toho	ten	k3xDgNnSc2	ten
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
upuštěno	upuštěn	k2eAgNnSc1d1	upuštěno
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
člunová	člunový	k2eAgFnSc1d1	člunová
paluba	paluba	k1gFnSc1	paluba
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
přeplněná	přeplněný	k2eAgFnSc1d1	přeplněná
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
zmenšil	zmenšit	k5eAaPmAgInS	zmenšit
prostor	prostor	k1gInSc1	prostor
pro	pro	k7c4	pro
promenádu	promenáda	k1gFnSc4	promenáda
cestujících	cestující	k1gMnPc2	cestující
a	a	k8xC	a
zhoršil	zhoršit	k5eAaPmAgInS	zhoršit
výhled	výhled	k1gInSc4	výhled
na	na	k7c4	na
oceán	oceán	k1gInSc4	oceán
<g/>
.	.	kIx.	.
</s>
<s>
RMS	RMS	kA	RMS
Titanic	Titanic	k1gInSc1	Titanic
byl	být	k5eAaImAgInS	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
systémem	systém	k1gInSc7	systém
5	[number]	k4	5
balastních	balastní	k2eAgFnPc2d1	balastní
nádrží	nádrž	k1gFnPc2	nádrž
s	s	k7c7	s
čerpadly	čerpadlo	k1gNnPc7	čerpadlo
a	a	k8xC	a
dalšími	další	k2eAgNnPc7d1	další
3	[number]	k4	3
čerpadly	čerpadlo	k1gNnPc7	čerpadlo
<g/>
,	,	kIx,	,
každé	každý	k3xTgInPc4	každý
s	s	k7c7	s
přečerpávací	přečerpávací	k2eAgFnSc7d1	přečerpávací
kapacitou	kapacita	k1gFnSc7	kapacita
150	[number]	k4	150
tun	tuna	k1gFnPc2	tuna
<g/>
/	/	kIx~	/
<g/>
hod	hod	k1gInSc1	hod
<g/>
.	.	kIx.	.
</s>
<s>
Kapacita	kapacita	k1gFnSc1	kapacita
všech	všecek	k3xTgFnPc2	všecek
současně	současně	k6eAd1	současně
pracujících	pracující	k2eAgNnPc2d1	pracující
čerpadel	čerpadlo	k1gNnPc2	čerpadlo
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
celkové	celkový	k2eAgFnPc4d1	celková
výtlačné	výtlačný	k2eAgFnPc4d1	výtlačná
kapacity	kapacita	k1gFnPc4	kapacita
1	[number]	k4	1
700	[number]	k4	700
tun	tuna	k1gFnPc2	tuna
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgNnPc4	dva
hlavní	hlavní	k2eAgNnPc4d1	hlavní
potrubí	potrubí	k1gNnPc4	potrubí
balastních	balastní	k2eAgFnPc2d1	balastní
nádrží	nádrž	k1gFnPc2	nádrž
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
10	[number]	k4	10
<g/>
''	''	k?	''
(	(	kIx(	(
<g/>
~	~	kIx~	~
<g/>
250	[number]	k4	250
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
ventily	ventil	k1gInPc1	ventil
rozvodu	rozvod	k1gInSc2	rozvod
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
položena	položit	k5eAaPmNgFnS	položit
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
trupu	trup	k1gInSc2	trup
nad	nad	k7c7	nad
přepážkami	přepážka	k1gFnPc7	přepážka
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
katastrofy	katastrofa	k1gFnSc2	katastrofa
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
pomocí	pomocí	k7c2	pomocí
čerpadel	čerpadlo	k1gNnPc2	čerpadlo
zpomalit	zpomalit	k5eAaPmF	zpomalit
zaplavení	zaplavení	k1gNnSc4	zaplavení
6	[number]	k4	6
<g/>
.	.	kIx.	.
kotelny	kotelna	k1gFnPc1	kotelna
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
deseti	deset	k4xCc6	deset
minutách	minuta	k1gFnPc6	minuta
po	po	k7c6	po
srážce	srážka	k1gFnSc6	srážka
s	s	k7c7	s
ledovcem	ledovec	k1gInSc7	ledovec
a	a	k8xC	a
snížit	snížit	k5eAaPmF	snížit
rychlost	rychlost	k1gFnSc4	rychlost
zaplavování	zaplavování	k1gNnPc2	zaplavování
5	[number]	k4	5
<g/>
.	.	kIx.	.
kotelny	kotelna	k1gFnPc4	kotelna
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
odčerpávání	odčerpávání	k1gNnSc1	odčerpávání
vody	voda	k1gFnSc2	voda
nemohlo	moct	k5eNaImAgNnS	moct
zachovat	zachovat	k5eAaPmF	zachovat
potřebný	potřebný	k2eAgInSc4d1	potřebný
vztlak	vztlak	k1gInSc4	vztlak
trupu	trup	k1gInSc2	trup
<g/>
,	,	kIx,	,
zpomalilo	zpomalit	k5eAaPmAgNnS	zpomalit
však	však	k9	však
zaplavování	zaplavování	k1gNnSc1	zaplavování
<g/>
.	.	kIx.	.
</s>
<s>
Čerpadla	čerpadlo	k1gNnPc1	čerpadlo
pracovala	pracovat	k5eAaImAgNnP	pracovat
až	až	k6eAd1	až
do	do	k7c2	do
23	[number]	k4	23
<g/>
:	:	kIx,	:
<g/>
50	[number]	k4	50
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
zaplavena	zaplavit	k5eAaPmNgFnS	zaplavit
a	a	k8xC	a
vyřazena	vyřadit	k5eAaPmNgFnS	vyřadit
z	z	k7c2	z
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
RMS	RMS	kA	RMS
Titanic	Titanic	k1gInSc1	Titanic
a	a	k8xC	a
RMS	RMS	kA	RMS
Olympic	Olympice	k1gInPc2	Olympice
byly	být	k5eAaImAgFnP	být
sesterské	sesterský	k2eAgFnPc1d1	sesterská
lodě	loď	k1gFnPc1	loď
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Olympic	Olympice	k1gInPc2	Olympice
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
dříve	dříve	k6eAd2	dříve
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
byly	být	k5eAaImAgFnP	být
obě	dva	k4xCgFnPc1	dva
lodě	loď	k1gFnPc1	loď
vyráběny	vyrábět	k5eAaImNgFnP	vyrábět
současně	současně	k6eAd1	současně
vedle	vedle	k6eAd1	vedle
sebe	sebe	k3xPyFc4	sebe
v	v	k7c6	v
téže	týž	k3xTgFnSc6	týž
loděnici	loděnice	k1gFnSc6	loděnice
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
Titanicu	Titanicus	k1gInSc2	Titanicus
byla	být	k5eAaImAgFnS	být
poněkud	poněkud	k6eAd1	poněkud
zpožděna	zpozdit	k5eAaPmNgFnS	zpozdit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
musel	muset	k5eAaImAgMnS	muset
ustoupit	ustoupit	k5eAaPmF	ustoupit
nutné	nutný	k2eAgFnSc3d1	nutná
opravě	oprava	k1gFnSc3	oprava
Olympicu	Olympicus	k1gInSc2	Olympicus
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
nešťastné	šťastný	k2eNgFnSc6d1	nešťastná
srážce	srážka	k1gFnSc6	srážka
s	s	k7c7	s
křižníkem	křižník	k1gInSc7	křižník
Hawkee	Hawke	k1gInSc2	Hawke
-	-	kIx~	-
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
pocházejí	pocházet	k5eAaImIp3nP	pocházet
také	také	k9	také
společné	společný	k2eAgFnPc4d1	společná
fotografie	fotografia	k1gFnPc4	fotografia
obou	dva	k4xCgFnPc2	dva
lodí	loď	k1gFnPc2	loď
v	v	k7c6	v
Belfastu	Belfast	k1gInSc6	Belfast
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
Titanicem	Titanic	k1gMnSc7	Titanic
a	a	k8xC	a
Olympicem	Olympic	k1gMnSc7	Olympic
byl	být	k5eAaImAgMnS	být
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
patrný	patrný	k2eAgInSc4d1	patrný
pouze	pouze	k6eAd1	pouze
ze	z	k7c2	z
zábradlí	zábradlí	k1gNnSc2	zábradlí
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
A	a	k9	a
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
změněno	změnit	k5eAaPmNgNnS	změnit
na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Obě	dva	k4xCgFnPc1	dva
lodě	loď	k1gFnPc1	loď
měly	mít	k5eAaImAgFnP	mít
trup	trup	k1gInSc4	trup
stejně	stejně	k6eAd1	stejně
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
(	(	kIx(	(
<g/>
rozdíl	rozdíl	k1gInSc1	rozdíl
činil	činit	k5eAaImAgInS	činit
10	[number]	k4	10
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jen	jen	k9	jen
Titanic	Titanic	k1gInSc1	Titanic
měl	mít	k5eAaImAgInS	mít
větší	veliký	k2eAgFnSc4d2	veliký
registrovanou	registrovaný	k2eAgFnSc4d1	registrovaná
hmotnost	hmotnost	k1gFnSc4	hmotnost
(	(	kIx(	(
<g/>
BRT	BRT	k?	BRT
<g/>
)	)	kIx)	)
a	a	k8xC	a
obě	dva	k4xCgFnPc1	dva
lodě	loď	k1gFnPc1	loď
měly	mít	k5eAaImAgFnP	mít
i	i	k9	i
velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgInSc4d1	podobný
vzhled	vzhled	k1gInSc4	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Lišily	lišit	k5eAaImAgInP	lišit
se	se	k3xPyFc4	se
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
přední	přední	k2eAgFnSc1d1	přední
promenáda	promenáda	k1gFnSc1	promenáda
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
A	a	k9	a
Titanicu	Titanicus	k1gInSc3	Titanicus
byla	být	k5eAaImAgFnS	být
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
cestujících	cestující	k1gFnPc2	cestující
před	před	k7c7	před
počasím	počasí	k1gNnSc7	počasí
<g/>
,	,	kIx,	,
paluby	paluba	k1gFnPc1	paluba
B	B	kA	B
měly	mít	k5eAaImAgFnP	mít
jiné	jiný	k2eAgNnSc4d1	jiné
uspořádání	uspořádání	k1gNnSc4	uspořádání
a	a	k8xC	a
na	na	k7c4	na
Olympicu	Olympica	k1gFnSc4	Olympica
nebyla	být	k5eNaImAgFnS	být
"	"	kIx"	"
<g/>
Café	café	k1gNnSc1	café
Parisien	Parisina	k1gFnPc2	Parisina
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
Olympic	Olympice	k1gFnPc2	Olympice
přestavěna	přestavěn	k2eAgFnSc1d1	přestavěna
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
oblibě	obliba	k1gFnSc3	obliba
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
si	se	k3xPyFc3	se
získala	získat	k5eAaPmAgFnS	získat
na	na	k7c6	na
Titaniku	Titanic	k1gInSc6	Titanic
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
Titanicu	Titanicus	k1gInSc2	Titanicus
byly	být	k5eAaImAgFnP	být
odstraněny	odstranit	k5eAaPmNgFnP	odstranit
i	i	k9	i
některé	některý	k3yIgInPc4	některý
konstrukční	konstrukční	k2eAgInPc4d1	konstrukční
nedostatky	nedostatek	k1gInPc4	nedostatek
zjištěné	zjištěný	k2eAgInPc4d1	zjištěný
na	na	k7c4	na
Olympicu	Olympica	k1gFnSc4	Olympica
<g/>
.	.	kIx.	.
</s>
<s>
Světla	světlo	k1gNnPc1	světlo
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
A	a	k9	a
Titanicu	Titanicus	k1gInSc3	Titanicus
byla	být	k5eAaImAgFnS	být
kulatá	kulatý	k2eAgFnSc1d1	kulatá
<g/>
,	,	kIx,	,
na	na	k7c4	na
Olympicu	Olympica	k1gFnSc4	Olympica
oválná	oválný	k2eAgFnSc1d1	oválná
<g/>
.	.	kIx.	.
</s>
<s>
Kormidelna	kormidelna	k1gFnSc1	kormidelna
na	na	k7c4	na
Titanicu	Titanica	k1gFnSc4	Titanica
byla	být	k5eAaImAgFnS	být
protáhlejší	protáhlý	k2eAgFnSc1d2	protáhlejší
-	-	kIx~	-
užší	úzký	k2eAgFnSc1d2	užší
a	a	k8xC	a
delší	dlouhý	k2eAgFnSc1d2	delší
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
provedené	provedený	k2eAgFnPc1d1	provedená
úpravy	úprava	k1gFnPc1	úprava
přispěly	přispět	k5eAaPmAgFnP	přispět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
Titanic	Titanic	k1gInSc1	Titanic
při	při	k7c6	při
spuštění	spuštění	k1gNnSc6	spuštění
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
o	o	k7c4	o
1	[number]	k4	1
004	[number]	k4	004
BRT	BRT	k?	BRT
větší	veliký	k2eAgMnSc1d2	veliký
<g/>
,	,	kIx,	,
a	a	k8xC	a
loď	loď	k1gFnSc1	loď
se	se	k3xPyFc4	se
jevila	jevit	k5eAaImAgFnS	jevit
užší	úzký	k2eAgFnSc1d2	užší
a	a	k8xC	a
delší	dlouhý	k2eAgFnSc1d2	delší
(	(	kIx(	(
<g/>
šlo	jít	k5eAaImAgNnS	jít
ovšem	ovšem	k9	ovšem
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
optický	optický	k2eAgInSc4d1	optický
dojem	dojem	k1gInSc4	dojem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Trup	trup	k1gInSc1	trup
Titanicu	Titanicum	k1gNnSc3	Titanicum
byl	být	k5eAaImAgInS	být
spuštěn	spustit	k5eAaPmNgInS	spustit
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
řeky	řeka	k1gFnSc2	řeka
Lagan	Lagan	k1gInSc4	Lagan
dne	den	k1gInSc2	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1911	[number]	k4	1911
ve	v	k7c4	v
12	[number]	k4	12
<g/>
:	:	kIx,	:
<g/>
13	[number]	k4	13
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dal	dát	k5eAaPmAgMnS	dát
předseda	předseda	k1gMnSc1	předseda
správní	správní	k2eAgFnSc2d1	správní
rady	rada	k1gFnSc2	rada
loděnice	loděnice	k1gFnSc2	loděnice
William	William	k1gInSc1	William
James	James	k1gMnSc1	James
Pirrie	Pirrie	k1gFnSc1	Pirrie
pokyn	pokyn	k1gInSc4	pokyn
k	k	k7c3	k
zahájení	zahájení	k1gNnSc3	zahájení
spuštění	spuštění	k1gNnSc2	spuštění
<g/>
.	.	kIx.	.
</s>
<s>
Události	událost	k1gFnSc2	událost
přihlíželo	přihlížet	k5eAaImAgNnS	přihlížet
množství	množství	k1gNnSc1	množství
pozvaných	pozvaný	k2eAgFnPc2d1	pozvaná
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
zvědavých	zvědavý	k2eAgMnPc2d1	zvědavý
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Spuštění	spuštění	k1gNnSc1	spuštění
lodi	loď	k1gFnSc2	loď
ale	ale	k8xC	ale
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
bez	bez	k7c2	bez
obvyklých	obvyklý	k2eAgInPc2d1	obvyklý
obřadů	obřad	k1gInPc2	obřad
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
křest	křest	k1gInSc4	křest
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
White	Whit	k1gInSc5	Whit
Star	Star	kA	Star
Line	linout	k5eAaImIp3nS	linout
sice	sice	k8xC	sice
nikdy	nikdy	k6eAd1	nikdy
své	svůj	k3xOyFgFnPc4	svůj
lodě	loď	k1gFnPc4	loď
nekřtila	křtít	k5eNaImAgFnS	křtít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
absence	absence	k1gFnSc1	absence
křtu	křest	k1gInSc2	křest
laiky	laik	k1gMnPc4	laik
pokládána	pokládat	k5eAaImNgFnS	pokládat
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
příčin	příčina	k1gFnPc2	příčina
katastrofy	katastrofa	k1gFnSc2	katastrofa
-	-	kIx~	-
společnost	společnost	k1gFnSc1	společnost
White	Whit	k1gInSc5	Whit
Star	Star	kA	Star
Line	linout	k5eAaImIp3nS	linout
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
nepokřtila	pokřtít	k5eNaPmAgFnS	pokřtít
žádnou	žádný	k3yNgFnSc4	žádný
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
nekřtila	křtít	k5eNaImAgFnS	křtít
své	svůj	k3xOyFgFnPc4	svůj
lodě	loď	k1gFnPc4	loď
konkurenční	konkurenční	k2eAgFnPc4d1	konkurenční
státem	stát	k1gInSc7	stát
subvencovaná	subvencovaný	k2eAgFnSc1d1	subvencovaná
Cunard	Cunard	k1gInSc4	Cunard
Line	linout	k5eAaImIp3nS	linout
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
spuštění	spuštění	k1gNnSc6	spuštění
zůstal	zůstat	k5eAaPmAgInS	zůstat
Titanic	Titanic	k1gInSc4	Titanic
na	na	k7c6	na
pozemcích	pozemek	k1gInPc6	pozemek
loděnic	loděnice	k1gFnPc2	loděnice
Harland	Harland	k1gInSc1	Harland
&	&	k?	&
Wolff	Wolff	k1gInSc4	Wolff
k	k	k7c3	k
dokončení	dokončení	k1gNnSc3	dokončení
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
osadit	osadit	k5eAaPmF	osadit
kotle	kotel	k1gInPc4	kotel
<g/>
,	,	kIx,	,
parní	parní	k2eAgInPc4d1	parní
stroje	stroj	k1gInPc4	stroj
<g/>
,	,	kIx,	,
namontovat	namontovat	k5eAaPmF	namontovat
stožáry	stožár	k1gInPc4	stožár
<g/>
,	,	kIx,	,
komíny	komín	k1gInPc4	komín
a	a	k8xC	a
zařídit	zařídit	k5eAaPmF	zařídit
a	a	k8xC	a
vybavit	vybavit	k5eAaPmF	vybavit
interiéry	interiér	k1gInPc4	interiér
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
práce	práce	k1gFnPc1	práce
trvaly	trvat	k5eAaImAgFnP	trvat
skoro	skoro	k6eAd1	skoro
další	další	k2eAgInSc4d1	další
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
<g/>
,	,	kIx,	,
po	po	k7c6	po
dohotovení	dohotovení	k1gNnSc6	dohotovení
dokončovacích	dokončovací	k2eAgFnPc2d1	dokončovací
prací	práce	k1gFnPc2	práce
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1912	[number]	k4	1912
zahájeny	zahájen	k2eAgFnPc1d1	zahájena
zatěžkávací	zatěžkávací	k2eAgFnPc1d1	zatěžkávací
zkoušky	zkouška	k1gFnPc1	zkouška
v	v	k7c6	v
Irském	irský	k2eAgNnSc6d1	irské
moři	moře	k1gNnSc6	moře
podle	podle	k7c2	podle
tehdejších	tehdejší	k2eAgInPc2d1	tehdejší
předpisů	předpis	k1gInPc2	předpis
-	-	kIx~	-
rozjetí	rozjetí	k1gNnSc1	rozjetí
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
zastavení	zastavení	k1gNnSc3	zastavení
<g/>
,	,	kIx,	,
nejkratší	krátký	k2eAgInSc1d3	nejkratší
oblouk	oblouk	k1gInSc1	oblouk
v	v	k7c6	v
"	"	kIx"	"
<g/>
plné	plný	k2eAgFnSc6d1	plná
rychlosti	rychlost	k1gFnSc6	rychlost
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
největší	veliký	k2eAgFnSc4d3	veliký
rychlost	rychlost	k1gFnSc4	rychlost
Titanikem	Titanic	k1gInSc7	Titanic
dosaženou	dosažený	k2eAgFnSc4d1	dosažená
<g />
.	.	kIx.	.
</s>
<s>
21,5	[number]	k4	21,5
uzlu	uzel	k1gInSc2	uzel
-	-	kIx~	-
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
obdivuhodné	obdivuhodný	k2eAgNnSc1d1	obdivuhodné
i	i	k9	i
dnes	dnes	k6eAd1	dnes
<g/>
)	)	kIx)	)
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
byl	být	k5eAaImAgMnS	být
Titanic	Titanic	k1gInSc4	Titanic
po	po	k7c6	po
zkouškách	zkouška	k1gFnPc6	zkouška
uznán	uznat	k5eAaPmNgInS	uznat
plavby	plavba	k1gFnSc2	plavba
schopným	schopný	k2eAgMnPc3d1	schopný
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
a	a	k8xC	a
dostal	dostat	k5eAaPmAgMnS	dostat
povolení	povolení	k1gNnSc4	povolení
k	k	k7c3	k
plavbě	plavba	k1gFnSc6	plavba
Victoriiným	Victoriin	k2eAgInSc7d1	Victoriin
kanálem	kanál	k1gInSc7	kanál
z	z	k7c2	z
Belfastu	Belfast	k1gInSc2	Belfast
do	do	k7c2	do
Irského	irský	k2eAgNnSc2d1	irské
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
půlnoci	půlnoc	k1gFnSc6	půlnoc
3	[number]	k4	3
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1912	[number]	k4	1912
vplul	vplout	k5eAaPmAgInS	vplout
Titanic	Titanic	k1gInSc1	Titanic
do	do	k7c2	do
Southamptonu	Southampton	k1gInSc2	Southampton
<g/>
.	.	kIx.	.
</s>
<s>
Zkoušky	zkouška	k1gFnPc1	zkouška
probíhaly	probíhat	k5eAaImAgFnP	probíhat
bez	bez	k7c2	bez
obsluhujícího	obsluhující	k2eAgInSc2d1	obsluhující
personálu	personál	k1gInSc2	personál
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
Titanicu	Titanicus	k1gInSc2	Titanicus
bylo	být	k5eAaImAgNnS	být
78	[number]	k4	78
topičů	topič	k1gMnPc2	topič
a	a	k8xC	a
strojníků	strojník	k1gMnPc2	strojník
a	a	k8xC	a
41	[number]	k4	41
členů	člen	k1gMnPc2	člen
posádky	posádka	k1gFnSc2	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
lodní	lodní	k2eAgFnSc6d1	lodní
palubě	paluba	k1gFnSc6	paluba
byli	být	k5eAaImAgMnP	být
přítomni	přítomen	k2eAgMnPc1d1	přítomen
zástupci	zástupce	k1gMnPc1	zástupce
různých	různý	k2eAgFnPc2d1	různá
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Harold	Harold	k1gMnSc1	Harold
A.	A.	kA	A.
Sanderson	Sanderson	k1gMnSc1	Sanderson
z	z	k7c2	z
I.	I.	kA	I.
<g/>
M.	M.	kA	M.
<g/>
M	M	kA	M
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Andrews	Andrews	k1gInSc1	Andrews
a	a	k8xC	a
Edward	Edward	k1gMnSc1	Edward
Wilding	Wilding	k1gInSc1	Wilding
z	z	k7c2	z
Harland	Harlanda	k1gFnPc2	Harlanda
&	&	k?	&
Wolff	Wolff	k1gMnSc1	Wolff
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nemoc	nemoc	k1gFnSc4	nemoc
se	se	k3xPyFc4	se
omluvili	omluvit	k5eAaPmAgMnP	omluvit
J.	J.	kA	J.
Bruce	Bruce	k1gMnSc2	Bruce
Ismay	Ismaa	k1gMnSc2	Ismaa
a	a	k8xC	a
Lord	lord	k1gMnSc1	lord
Pirrie	Pirrie	k1gFnSc2	Pirrie
<g/>
.	.	kIx.	.
</s>
<s>
Jack	Jack	k6eAd1	Jack
Phillips	Phillips	k1gInSc1	Phillips
a	a	k8xC	a
Harold	Harold	k1gInSc1	Harold
Bride	Brid	k1gInSc5	Brid
byli	být	k5eAaImAgMnP	být
operátoři	operátor	k1gMnPc1	operátor
radiostanice	radiostanice	k1gFnSc2	radiostanice
Marconi	Marcon	k1gMnPc1	Marcon
a	a	k8xC	a
prováděli	provádět	k5eAaImAgMnP	provádět
jemné	jemný	k2eAgNnSc4d1	jemné
doladění	doladění	k1gNnSc4	doladění
stanice	stanice	k1gFnSc2	stanice
-	-	kIx~	-
což	což	k3yQnSc1	což
vyžadovalo	vyžadovat	k5eAaImAgNnS	vyžadovat
několik	několik	k4yIc1	několik
složitých	složitý	k2eAgInPc2d1	složitý
obratů	obrat	k1gInPc2	obrat
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
osobou	osoba	k1gFnSc7	osoba
byl	být	k5eAaImAgInS	být
Francis	Francis	k1gInSc1	Francis
Carruthers	Carruthersa	k1gFnPc2	Carruthersa
z	z	k7c2	z
Board	Boarda	k1gFnPc2	Boarda
of	of	k?	of
Trade	Trad	k1gMnSc5	Trad
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
po	po	k7c6	po
prověření	prověření	k1gNnSc6	prověření
lodě	loď	k1gFnSc2	loď
podepsal	podepsat	k5eAaPmAgInS	podepsat
dokument	dokument	k1gInSc1	dokument
Agreement	Agreement	k1gMnSc1	Agreement
and	and	k?	and
Account	Account	k1gMnSc1	Account
of	of	k?	of
Voyages	Voyages	k1gMnSc1	Voyages
and	and	k?	and
Crew	Crew	k1gMnSc1	Crew
<g/>
,	,	kIx,	,
opravňující	opravňující	k2eAgFnSc4d1	opravňující
loď	loď	k1gFnSc4	loď
k	k	k7c3	k
plavbě	plavba	k1gFnSc3	plavba
s	s	k7c7	s
pasažéry	pasažér	k1gMnPc7	pasažér
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
dvanácti	dvanáct	k4xCc2	dvanáct
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
šesti	šest	k4xCc6	šest
hodinách	hodina	k1gFnPc6	hodina
plavebních	plavební	k2eAgFnPc2d1	plavební
zkoušek	zkouška	k1gFnPc2	zkouška
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
opustil	opustit	k5eAaPmAgInS	opustit
Titanic	Titanic	k1gInSc4	Titanic
v	v	k7c4	v
poledne	poledne	k1gNnSc4	poledne
Belfast	Belfast	k1gInSc1	Belfast
a	a	k8xC	a
vydal	vydat	k5eAaPmAgMnS	vydat
se	se	k3xPyFc4	se
na	na	k7c4	na
550	[number]	k4	550
mil	míle	k1gFnPc2	míle
(	(	kIx(	(
<g/>
890	[number]	k4	890
km	km	kA	km
<g/>
)	)	kIx)	)
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Southamptonu	Southampton	k1gInSc2	Southampton
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
kapitána	kapitán	k1gMnSc2	kapitán
Herberta	Herbert	k1gMnSc2	Herbert
Haddocka	Haddocka	k1gFnSc1	Haddocka
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
byla	být	k5eAaImAgFnS	být
provedena	proveden	k2eAgFnSc1d1	provedena
poslední	poslední	k2eAgFnSc1d1	poslední
inspekce	inspekce	k1gFnSc1	inspekce
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
provedl	provést	k5eAaPmAgMnS	provést
sám	sám	k3xTgMnSc1	sám
konstruktér	konstruktér	k1gMnSc1	konstruktér
Titanicu	Titanicus	k1gInSc2	Titanicus
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Andrews	Andrews	k1gInSc1	Andrews
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
poslední	poslední	k2eAgFnSc1d1	poslední
plavba	plavba	k1gFnSc1	plavba
směřovala	směřovat	k5eAaImAgFnS	směřovat
z	z	k7c2	z
anglického	anglický	k2eAgMnSc2d1	anglický
Southampton	Southampton	k1gInSc4	Southampton
přes	přes	k7c4	přes
francouzský	francouzský	k2eAgInSc4d1	francouzský
Cherbourg	Cherbourg	k1gInSc4	Cherbourg
a	a	k8xC	a
irský	irský	k2eAgInSc1d1	irský
Queenstown	Queenstown	k1gInSc1	Queenstown
(	(	kIx(	(
<g/>
Cobh	Cobh	k1gInSc1	Cobh
<g/>
)	)	kIx)	)
do	do	k7c2	do
amerického	americký	k2eAgMnSc2d1	americký
New	New	k1gMnSc2	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
plavbu	plavba	k1gFnSc4	plavba
se	se	k3xPyFc4	se
Titanic	Titanic	k1gInSc1	Titanic
vydal	vydat	k5eAaPmAgInS	vydat
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1912	[number]	k4	1912
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
kapitána	kapitán	k1gMnSc2	kapitán
Edwarda	Edward	k1gMnSc2	Edward
J.	J.	kA	J.
Smithe	Smith	k1gMnSc2	Smith
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
komodorem	komodor	k1gMnSc7	komodor
flotily	flotila	k1gFnSc2	flotila
White	Whit	k1gInSc5	Whit
Star	Star	kA	Star
Line	linout	k5eAaImIp3nS	linout
a	a	k8xC	a
předtím	předtím	k6eAd1	předtím
sloužil	sloužit	k5eAaImAgInS	sloužit
na	na	k7c4	na
Olympicu	Olympica	k1gFnSc4	Olympica
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
bylo	být	k5eAaImAgNnS	být
tradicí	tradice	k1gFnSc7	tradice
<g/>
,	,	kIx,	,
že	že	k8xS	že
společnost	společnost	k1gFnSc1	společnost
White	Whit	k1gInSc5	Whit
Star	Star	kA	Star
Line	linout	k5eAaImIp3nS	linout
vypravovala	vypravovat	k5eAaImAgFnS	vypravovat
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
vlak	vlak	k1gInSc4	vlak
z	z	k7c2	z
Londýna	Londýn	k1gInSc2	Londýn
do	do	k7c2	do
přístavu	přístav	k1gInSc2	přístav
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
cestující	cestující	k1gMnPc1	cestující
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
strávili	strávit	k5eAaPmAgMnP	strávit
poslední	poslední	k2eAgFnSc4d1	poslední
noc	noc	k1gFnSc4	noc
na	na	k7c6	na
pevnině	pevnina	k1gFnSc6	pevnina
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
<g/>
,	,	kIx,	,
měli	mít	k5eAaImAgMnP	mít
zajištěnu	zajištěn	k2eAgFnSc4d1	zajištěna
dopravu	doprava	k1gFnSc4	doprava
k	k	k7c3	k
jejím	její	k3xOp3gFnPc3	její
lodím	loď	k1gFnPc3	loď
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
plavba	plavba	k1gFnSc1	plavba
Titanicu	Titanicus	k1gInSc2	Titanicus
byla	být	k5eAaImAgFnS	být
málem	málem	k6eAd1	málem
na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
odložena	odložit	k5eAaPmNgFnS	odložit
kvůli	kvůli	k7c3	kvůli
probíhající	probíhající	k2eAgFnSc3d1	probíhající
stávce	stávka	k1gFnSc3	stávka
horníků	horník	k1gMnPc2	horník
-	-	kIx~	-
na	na	k7c4	na
Titanic	Titanic	k1gInSc4	Titanic
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
přeložena	přeložit	k5eAaPmNgFnS	přeložit
zásoba	zásoba	k1gFnSc1	zásoba
uhlí	uhlí	k1gNnSc2	uhlí
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
dostupných	dostupný	k2eAgFnPc2d1	dostupná
lodí	loď	k1gFnPc2	loď
White	Whit	k1gInSc5	Whit
Star	Star	kA	Star
Line	linout	k5eAaImIp3nS	linout
v	v	k7c6	v
Southamptonu	Southampton	k1gInSc6	Southampton
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyhověl	vyhovět	k5eAaPmAgMnS	vyhovět
úřednímu	úřední	k2eAgNnSc3d1	úřední
zjištění	zjištění	k1gNnSc3	zjištění
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yIgMnSc2	který
je	být	k5eAaImIp3nS	být
vybaven	vybaven	k2eAgInSc1d1	vybaven
dostatečným	dostatečný	k2eAgNnSc7d1	dostatečné
množstvím	množství	k1gNnSc7	množství
uhlí	uhlí	k1gNnSc2	uhlí
pro	pro	k7c4	pro
plavbu	plavba	k1gFnSc4	plavba
přes	přes	k7c4	přes
Atlantik	Atlantik	k1gInSc4	Atlantik
-	-	kIx~	-
není	být	k5eNaImIp3nS	být
bez	bez	k7c2	bez
zajímavosti	zajímavost	k1gFnSc2	zajímavost
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc4	tento
limit	limit	k1gInSc4	limit
<g/>
,	,	kIx,	,
nastavený	nastavený	k2eAgInSc4d1	nastavený
pro	pro	k7c4	pro
daleko	daleko	k6eAd1	daleko
menší	malý	k2eAgFnPc4d2	menší
lodě	loď	k1gFnPc4	loď
<g/>
,	,	kIx,	,
překročil	překročit	k5eAaPmAgMnS	překročit
Titanic	Titanic	k1gInSc4	Titanic
jen	jen	k9	jen
o	o	k7c4	o
několik	několik	k4yIc4	několik
set	set	k1gInSc4	set
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
palubu	paluba	k1gFnSc4	paluba
se	se	k3xPyFc4	se
nalodilo	nalodit	k5eAaPmAgNnS	nalodit
přes	přes	k7c4	přes
1	[number]	k4	1
300	[number]	k4	300
cestujících	cestující	k1gMnPc2	cestující
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
více	hodně	k6eAd2	hodně
než	než	k8xS	než
300	[number]	k4	300
v	v	k7c6	v
první	první	k4xOgFnSc6	první
třídě	třída	k1gFnSc6	třída
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
300	[number]	k4	300
osob	osoba	k1gFnPc2	osoba
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
třídě	třída	k1gFnSc6	třída
a	a	k8xC	a
přes	přes	k7c4	přes
700	[number]	k4	700
ve	v	k7c6	v
třídě	třída	k1gFnSc6	třída
třetí	třetí	k4xOgFnSc6	třetí
<g/>
.	.	kIx.	.
</s>
<s>
Titanic	Titanic	k1gInSc1	Titanic
tedy	tedy	k9	tedy
nebyl	být	k5eNaImAgInS	být
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
první	první	k4xOgFnSc6	první
plavbě	plavba	k1gFnSc6	plavba
kapacitně	kapacitně	k6eAd1	kapacitně
zcela	zcela	k6eAd1	zcela
vytížen	vytížit	k5eAaPmNgInS	vytížit
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
posádkou	posádka	k1gFnSc7	posádka
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
až	až	k9	až
2	[number]	k4	2
223	[number]	k4	223
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Velení	velení	k1gNnSc1	velení
lodi	loď	k1gFnSc2	loď
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
rukách	ruka	k1gFnPc6	ruka
kapitána	kapitán	k1gMnSc2	kapitán
Edwarda	Edward	k1gMnSc2	Edward
J.	J.	kA	J.
Smithe	Smith	k1gMnSc2	Smith
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
k	k	k7c3	k
ruce	ruka	k1gFnSc3	ruka
7	[number]	k4	7
důstojníků	důstojník	k1gMnPc2	důstojník
(	(	kIx(	(
<g/>
vrchní	vrchní	k2eAgMnSc1d1	vrchní
důstojník	důstojník	k1gMnSc1	důstojník
H.	H.	kA	H.
T.	T.	kA	T.
Wilde	Wild	k1gInSc5	Wild
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
důstojník	důstojník	k1gMnSc1	důstojník
William	William	k1gInSc4	William
McMaster	McMaster	k1gMnSc1	McMaster
Murdoch	Murdoch	k1gMnSc1	Murdoch
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Charles	Charles	k1gMnSc1	Charles
Herbert	Herbert	k1gMnSc1	Herbert
Lightoller	Lightoller	k1gMnSc1	Lightoller
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Herbert	Herbert	k1gMnSc1	Herbert
John	John	k1gMnSc1	John
Pitman	Pitman	k1gMnSc1	Pitman
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Joseph	Joseph	k1gMnSc1	Joseph
Grove	Groev	k1gFnSc2	Groev
Boxhall	Boxhall	k1gMnSc1	Boxhall
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Harold	Harold	k6eAd1	Harold
Godfrey	Godfre	k2eAgInPc1d1	Godfre
Lowe	Low	k1gInPc1	Low
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gMnSc1	James
Pell	Pell	k1gMnSc1	Pell
Moody	Mooda	k1gFnSc2	Mooda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
lodi	loď	k1gFnSc6	loď
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
i	i	k9	i
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
lodní	lodní	k2eAgFnSc2d1	lodní
společnosti	společnost	k1gFnSc2	společnost
White	Whit	k1gInSc5	Whit
Star	Star	kA	Star
Line	linout	k5eAaImIp3nS	linout
lord	lord	k1gMnSc1	lord
J.	J.	kA	J.
Bruce	Bruce	k1gMnSc1	Bruce
Ismay	Ismaa	k1gFnSc2	Ismaa
a	a	k8xC	a
šéfkonstruktér	šéfkonstruktér	k1gMnSc1	šéfkonstruktér
Thomas	Thomas	k1gMnSc1	Thomas
Andrews	Andrews	k1gInSc1	Andrews
<g/>
,	,	kIx,	,
projektant	projektant	k1gMnSc1	projektant
R.	R.	kA	R.
R.	R.	kA	R.
C.	C.	kA	C.
Chisholm	Chisholm	k1gMnSc1	Chisholm
<g/>
,	,	kIx,	,
elektrikáři	elektrikář	k1gMnSc3	elektrikář
W.	W.	kA	W.
H.	H.	kA	H.
M.	M.	kA	M.
Parr	Parr	k1gInSc4	Parr
a	a	k8xC	a
E.	E.	kA	E.
H.	H.	kA	H.
Watson	Watson	k1gMnSc1	Watson
<g/>
,	,	kIx,	,
zámečníci	zámečník	k1gMnPc1	zámečník
A.	A.	kA	A.
F.	F.	kA	F.
Cunnigham	Cunnigham	k1gInSc1	Cunnigham
<g/>
,	,	kIx,	,
A.	A.	kA	A.
W.	W.	kA	W.
Frost	Frost	k1gMnSc1	Frost
<g/>
,	,	kIx,	,
R.	R.	kA	R.
J.	J.	kA	J.
Knight	Knight	k1gMnSc1	Knight
a	a	k8xC	a
instalatér	instalatér	k1gMnSc1	instalatér
Francis	Francis	k1gFnSc2	Francis
"	"	kIx"	"
<g/>
Frank	Frank	k1gMnSc1	Frank
<g/>
"	"	kIx"	"
Parkers	Parkers	k1gInSc1	Parkers
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
plavba	plavba	k1gFnSc1	plavba
byla	být	k5eAaImAgFnS	být
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
několik	několik	k4yIc4	několik
málo	málo	k4c4	málo
cestujících	cestující	k1gMnPc2	cestující
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
plavili	plavit	k5eAaImAgMnP	plavit
ze	z	k7c2	z
Southamptonu	Southampton	k1gInSc2	Southampton
do	do	k7c2	do
Cherbourgu	Cherbourg	k1gInSc2	Cherbourg
nebo	nebo	k8xC	nebo
Queenstownu	Queenstown	k1gInSc2	Queenstown
a	a	k8xC	a
další	další	k2eAgFnSc2d1	další
plavby	plavba	k1gFnSc2	plavba
se	se	k3xPyFc4	se
neúčastnili	účastnit	k5eNaImAgMnP	účastnit
<g/>
.	.	kIx.	.
</s>
<s>
Přístav	přístav	k1gInSc1	přístav
v	v	k7c6	v
Queenstownu	Queenstown	k1gInSc6	Queenstown
nebyl	být	k5eNaImAgInS	být
stavebně	stavebně	k6eAd1	stavebně
vybaven	vybavit	k5eAaPmNgInS	vybavit
pro	pro	k7c4	pro
tak	tak	k6eAd1	tak
velkou	velký	k2eAgFnSc4d1	velká
loď	loď	k1gFnSc4	loď
a	a	k8xC	a
naloďování	naloďování	k1gNnSc1	naloďování
a	a	k8xC	a
vyloďování	vyloďování	k1gNnSc1	vyloďování
neprobíhalo	probíhat	k5eNaImAgNnS	probíhat
u	u	k7c2	u
mola	molo	k1gNnSc2	molo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pomocí	pomocí	k7c2	pomocí
lodí	loď	k1gFnPc2	loď
speciálně	speciálně	k6eAd1	speciálně
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
účel	účel	k1gInSc4	účel
společností	společnost	k1gFnPc2	společnost
zakoupených	zakoupený	k2eAgMnPc2d1	zakoupený
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
pasažéry	pasažér	k1gMnPc7	pasažér
převážely	převážet	k5eAaImAgFnP	převážet
mezi	mezi	k7c7	mezi
lodí	loď	k1gFnSc7	loď
a	a	k8xC	a
pevninou	pevnina	k1gFnSc7	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyplutí	vyplutí	k1gNnSc6	vyplutí
z	z	k7c2	z
Queenstownu	Queenstown	k1gInSc2	Queenstown
směr	směr	k1gInSc4	směr
New	New	k1gFnSc4	New
York	York	k1gInSc4	York
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
2	[number]	k4	2
201	[number]	k4	201
osob	osoba	k1gFnPc2	osoba
<g/>
;	;	kIx,	;
1	[number]	k4	1
316	[number]	k4	316
cestujících	cestující	k1gMnPc2	cestující
a	a	k8xC	a
855	[number]	k4	855
členů	člen	k1gMnPc2	člen
posádky	posádka	k1gFnSc2	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
první	první	k4xOgFnSc6	první
plavbě	plavba	k1gFnSc6	plavba
Titanicu	Titanicus	k1gInSc2	Titanicus
přes	přes	k7c4	přes
Atlantský	atlantský	k2eAgInSc4d1	atlantský
oceán	oceán	k1gInSc4	oceán
bylo	být	k5eAaImAgNnS	být
mnoho	mnoho	k4c1	mnoho
významných	významný	k2eAgMnPc2d1	významný
cestujících	cestující	k1gMnPc2	cestující
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
<g/>
:	:	kIx,	:
milionář	milionář	k1gMnSc1	milionář
John	John	k1gMnSc1	John
Jacob	Jacoba	k1gFnPc2	Jacoba
Astor	Astora	k1gFnPc2	Astora
IV	IV	kA	IV
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
Madeleine	Madeleine	k1gFnSc1	Madeleine
Force	force	k1gFnSc1	force
Astorová	Astorová	k1gFnSc1	Astorová
<g/>
,	,	kIx,	,
průmyslník	průmyslník	k1gMnSc1	průmyslník
Benjamin	Benjamin	k1gMnSc1	Benjamin
Guggenheim	Guggenheim	k1gMnSc1	Guggenheim
<g/>
,	,	kIx,	,
majitel	majitel	k1gMnSc1	majitel
obchodního	obchodní	k2eAgInSc2d1	obchodní
řetězce	řetězec	k1gInSc2	řetězec
Macy	Maca	k1gFnSc2	Maca
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
<g/>
;	;	kIx,	;
Isidor	Isidor	k1gMnSc1	Isidor
Straus	Straus	k1gMnSc1	Straus
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
<g />
.	.	kIx.	.
</s>
<s>
Ida	Ida	k1gFnSc1	Ida
<g/>
,	,	kIx,	,
milionářka	milionářka	k1gFnSc1	milionářka
Margaret	Margareta	k1gFnPc2	Margareta
"	"	kIx"	"
<g/>
Molly	Moll	k1gInPc1	Moll
<g/>
"	"	kIx"	"
Brownová	Brownová	k1gFnSc1	Brownová
z	z	k7c2	z
Denveru	Denver	k1gInSc2	Denver
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
přezdívaná	přezdívaný	k2eAgFnSc1d1	přezdívaná
"	"	kIx"	"
<g/>
Nepotopitelná	potopitelný	k2eNgFnSc1d1	nepotopitelná
Molly	Molla	k1gFnPc1	Molla
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
pomoc	pomoc	k1gFnSc4	pomoc
ostatním	ostatní	k2eAgInPc3d1	ostatní
při	při	k7c6	při
ztroskotání	ztroskotání	k1gNnSc6	ztroskotání
<g/>
,	,	kIx,	,
sir	sir	k1gMnSc1	sir
Cosmo	Cosma	k1gFnSc5	Cosma
Duff-Gordon	Duff-Gordon	k1gInSc1	Duff-Gordon
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
<g/>
,	,	kIx,	,
módní	módní	k2eAgFnSc1d1	módní
návrhářka	návrhářka	k1gFnSc1	návrhářka
Lucy	Luca	k1gFnSc2	Luca
Duffová-Gordonová	Duffová-Gordonová	k1gFnSc1	Duffová-Gordonová
<g/>
,	,	kIx,	,
George	George	k1gFnSc1	George
Dunton	Dunton	k1gInSc1	Dunton
Widener	Widener	k1gInSc1	Widener
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
<g />
.	.	kIx.	.
</s>
<s>
Eleanor	Eleanor	k1gMnSc1	Eleanor
se	s	k7c7	s
synem	syn	k1gMnSc7	syn
Harrym	Harrym	k1gInSc4	Harrym
<g/>
,	,	kIx,	,
hráčem	hráč	k1gMnSc7	hráč
kriketu	kriket	k1gInSc2	kriket
<g/>
,	,	kIx,	,
obchodník	obchodník	k1gMnSc1	obchodník
John	John	k1gMnSc1	John
Borland	Borland	kA	Borland
Thayer	Thayer	k1gMnSc1	Thayer
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Marian	Mariana	k1gFnPc2	Mariana
a	a	k8xC	a
sedmnáctiletým	sedmnáctiletý	k2eAgMnSc7d1	sedmnáctiletý
synem	syn	k1gMnSc7	syn
Jackem	Jacek	k1gMnSc7	Jacek
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
William	William	k1gInSc1	William
Thomas	Thomas	k1gMnSc1	Thomas
Stead	Stead	k1gInSc1	Stead
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1892	[number]	k4	1892
napsal	napsat	k5eAaPmAgMnS	napsat
knihu	kniha	k1gFnSc4	kniha
o	o	k7c6	o
zaoceánském	zaoceánský	k2eAgInSc6d1	zaoceánský
parníku	parník	k1gInSc6	parník
potopeném	potopený	k2eAgInSc6d1	potopený
ledovou	ledový	k2eAgFnSc4d1	ledová
krou	kra	k1gFnSc7	kra
<g/>
,	,	kIx,	,
hraběnka	hraběnka	k1gFnSc1	hraběnka
Rothes	Rothes	k1gMnSc1	Rothes
<g/>
,	,	kIx,	,
asistent	asistent	k1gMnSc1	asistent
prezidenta	prezident	k1gMnSc2	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgFnPc2d1	americká
Archibald	Archibalda	k1gFnPc2	Archibalda
Butt	Butt	k1gMnSc1	Butt
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
a	a	k8xC	a
socioložka	socioložka	k1gFnSc1	socioložka
Helen	Helena	k1gFnPc2	Helena
Churchill	Churchilla	k1gFnPc2	Churchilla
Candeeová	Candeeový	k2eAgFnSc1d1	Candeeový
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Jacques	Jacques	k1gMnSc1	Jacques
Futrelle	Futrelle	k1gInSc4	Futrelle
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
May	May	k1gMnSc1	May
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
přátelé	přítel	k1gMnPc1	přítel
<g/>
,	,	kIx,	,
producenti	producent	k1gMnPc1	producent
z	z	k7c2	z
Broadwaye	Broadway	k1gInSc2	Broadway
<g/>
,	,	kIx,	,
Henry	henry	k1gInSc2	henry
a	a	k8xC	a
Rene	Rene	k1gFnSc4	Rene
Harrisovi	Harrisův	k2eAgMnPc1d1	Harrisův
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
němého	němý	k2eAgInSc2d1	němý
filmu	film	k1gInSc2	film
Dorothy	Dorotha	k1gFnSc2	Dorotha
Gibsonová	Gibsonová	k1gFnSc1	Gibsonová
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Plavby	plavba	k1gFnPc1	plavba
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
neúčastnil	účastnit	k5eNaImAgMnS	účastnit
bankéř	bankéř	k1gMnSc1	bankéř
J.	J.	kA	J.
P.	P.	kA	P.
Morgan	morgan	k1gMnSc1	morgan
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
plavbu	plavba	k1gFnSc4	plavba
zrušil	zrušit	k5eAaPmAgInS	zrušit
-	-	kIx~	-
podle	podle	k7c2	podle
tehdejších	tehdejší	k2eAgNnPc2d1	tehdejší
měřítek	měřítko	k1gNnPc2	měřítko
nebyla	být	k5eNaImAgFnS	být
společnost	společnost	k1gFnSc1	společnost
na	na	k7c6	na
panenské	panenský	k2eAgFnSc6d1	panenská
plavbě	plavba	k1gFnSc6	plavba
Titanicu	Titanicus	k1gInSc2	Titanicus
-	-	kIx~	-
navzdory	navzdory	k7c3	navzdory
pozdějším	pozdní	k2eAgInPc3d2	pozdější
výmyslům	výmysl	k1gInPc3	výmysl
-	-	kIx~	-
nijak	nijak	k6eAd1	nijak
výjimečná	výjimečný	k2eAgFnSc1d1	výjimečná
či	či	k8xC	či
vybraná	vybraný	k2eAgFnSc1d1	vybraná
a	a	k8xC	a
podle	podle	k7c2	podle
záznamů	záznam	k1gInPc2	záznam
rovněž	rovněž	k9	rovněž
loď	loď	k1gFnSc1	loď
nepřevážela	převážet	k5eNaImAgFnS	převážet
žádné	žádný	k3yNgFnPc4	žádný
významnější	významný	k2eAgFnPc4d2	významnější
cennosti	cennost	k1gFnPc4	cennost
či	či	k8xC	či
poklady	poklad	k1gInPc4	poklad
(	(	kIx(	(
<g/>
tím	ten	k3xDgNnSc7	ten
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
zmiňovanou	zmiňovaný	k2eAgFnSc4d1	zmiňovaná
mumii	mumie	k1gFnSc4	mumie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dodnes	dodnes	k6eAd1	dodnes
pokojně	pokojně	k6eAd1	pokojně
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Britském	britský	k2eAgNnSc6d1	Britské
muzeu	muzeum	k1gNnSc6	muzeum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
tři	tři	k4xCgFnPc4	tři
desítky	desítka	k1gFnPc1	desítka
lidí	člověk	k1gMnPc2	člověk
pak	pak	k8xC	pak
plavbu	plavba	k1gFnSc4	plavba
na	na	k7c6	na
Titanicu	Titanicus	k1gInSc6	Titanicus
odřekly	odřeknout	k5eAaPmAgInP	odřeknout
na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
kterými	který	k3yIgFnPc7	který
nechyběly	chybět	k5eNaImAgFnP	chybět
ani	ani	k8xC	ani
zlé	zlý	k2eAgFnPc1d1	zlá
předtuchy	předtucha	k1gFnPc1	předtucha
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
ovšem	ovšem	k9	ovšem
panovaly	panovat	k5eAaImAgFnP	panovat
i	i	k9	i
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
-	-	kIx~	-
několik	několik	k4yIc4	několik
přeživších	přeživší	k2eAgFnPc2d1	přeživší
vzpomínalo	vzpomínat	k5eAaImAgNnS	vzpomínat
na	na	k7c4	na
kokrhání	kokrhání	k1gNnSc4	kokrhání
kohouta	kohout	k1gMnSc2	kohout
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
v	v	k7c6	v
Cornwalu	Cornwal	k1gInSc6	Cornwal
a	a	k8xC	a
Walesu	Wales	k1gInSc2	Wales
ohlašuje	ohlašovat	k5eAaImIp3nS	ohlašovat
smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Drůbež	drůbež	k1gFnSc1	drůbež
ušlechtilého	ušlechtilý	k2eAgInSc2d1	ušlechtilý
chovu	chov	k1gInSc2	chov
byla	být	k5eAaImAgFnS	být
ovšem	ovšem	k9	ovšem
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
nákladu	náklad	k1gInSc2	náklad
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
ubytována	ubytován	k2eAgFnSc1d1	ubytována
vedle	vedle	k7c2	vedle
psů	pes	k1gMnPc2	pes
nedaleko	nedaleko	k7c2	nedaleko
kuchyně	kuchyně	k1gFnSc2	kuchyně
první	první	k4xOgFnSc2	první
třídy	třída	k1gFnSc2	třída
-	-	kIx~	-
v	v	k7c6	v
těsném	těsný	k2eAgNnSc6d1	těsné
sousedství	sousedství	k1gNnSc6	sousedství
kajut	kajuta	k1gFnPc2	kajuta
druhé	druhý	k4xOgFnSc2	druhý
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
rovněž	rovněž	k9	rovněž
bez	bez	k7c2	bez
zajímavosti	zajímavost	k1gFnSc2	zajímavost
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
členů	člen	k1gMnPc2	člen
posádky	posádka	k1gFnSc2	posádka
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Černého	Černého	k2eAgInSc2d1	Černého
gangu	gang	k1gInSc2	gang
<g/>
)	)	kIx)	)
dezertoval	dezertovat	k5eAaBmAgInS	dezertovat
ze	z	k7c2	z
služby	služba	k1gFnSc2	služba
v	v	k7c6	v
Cóbhu	Cóbh	k1gInSc6	Cóbh
(	(	kIx(	(
<g/>
Queenstownu	Queenstowno	k1gNnSc6	Queenstowno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gNnSc7	jeho
bydlištěm	bydliště	k1gNnSc7	bydliště
<g/>
,	,	kIx,	,
a	a	k8xC	a
Titanic	Titanic	k1gInSc4	Titanic
mu	on	k3xPp3gMnSc3	on
posloužil	posloužit	k5eAaPmAgMnS	posloužit
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jen	jen	k9	jen
jako	jako	k9	jako
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
zadarmo	zadarmo	k6eAd1	zadarmo
dostat	dostat	k5eAaPmF	dostat
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
z	z	k7c2	z
členů	člen	k1gMnPc2	člen
posádky	posádka	k1gFnSc2	posádka
pak	pak	k6eAd1	pak
ztratil	ztratit	k5eAaPmAgMnS	ztratit
svoji	svůj	k3xOyFgFnSc4	svůj
námořnickou	námořnický	k2eAgFnSc4d1	námořnická
knížku	knížka	k1gFnSc4	knížka
a	a	k8xC	a
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
místě	místo	k1gNnSc6	místo
zahynul	zahynout	k5eAaPmAgMnS	zahynout
na	na	k7c4	na
Titanicu	Titanica	k1gFnSc4	Titanica
neznámý	známý	k2eNgMnSc1d1	neznámý
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Čechy	Čech	k1gMnPc4	Čech
není	být	k5eNaImIp3nS	být
bez	bez	k7c2	bez
zajímavosti	zajímavost	k1gFnSc2	zajímavost
<g/>
,	,	kIx,	,
že	že	k8xS	že
jistý	jistý	k2eAgMnSc1d1	jistý
"	"	kIx"	"
<g/>
Francouz	Francouz	k1gMnSc1	Francouz
<g/>
"	"	kIx"	"
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
Michel	Michel	k1gMnSc1	Michel
Navratil	Navratil	k1gMnSc1	Navratil
cestoval	cestovat	k5eAaImAgMnS	cestovat
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
malými	malý	k2eAgFnPc7d1	malá
dětmi	dítě	k1gFnPc7	dítě
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
unesl	unést	k5eAaPmAgMnS	unést
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zachránil	zachránit	k5eAaPmAgMnS	zachránit
své	svůj	k3xOyFgNnSc4	svůj
rozpadlé	rozpadlý	k2eAgNnSc4d1	rozpadlé
manželství	manželství	k1gNnSc4	manželství
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
dokázal	dokázat	k5eAaPmAgMnS	dokázat
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
člunu	člun	k1gInSc2	člun
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
sám	sám	k3xTgMnSc1	sám
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
zahynul	zahynout	k5eAaPmAgMnS	zahynout
-	-	kIx~	-
jeho	jeho	k3xOp3gNnSc1	jeho
tělo	tělo	k1gNnSc1	tělo
bylo	být	k5eAaImAgNnS	být
vyloveno	vyloven	k2eAgNnSc1d1	vyloveno
a	a	k8xC	a
halifaxský	halifaxský	k2eAgMnSc1d1	halifaxský
rabín	rabín	k1gMnSc1	rabín
o	o	k7c6	o
ostatcích	ostatek	k1gInPc6	ostatek
česko-francouzského	českorancouzský	k2eAgMnSc2d1	česko-francouzský
katolíka	katolík	k1gMnSc2	katolík
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
Žida	Žid	k1gMnSc4	Žid
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
Michel	Michel	k1gMnSc1	Michel
Navrátil	navrátit	k5eAaPmAgMnS	navrátit
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
na	na	k7c6	na
židovském	židovský	k2eAgInSc6d1	židovský
hřbitově	hřbitov	k1gInSc6	hřbitov
v	v	k7c6	v
Halifaxu	Halifax	k1gInSc6	Halifax
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
cestujícími	cestující	k1gMnPc7	cestující
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
Brity	Brit	k1gMnPc4	Brit
<g/>
,	,	kIx,	,
Iry	Ir	k1gMnPc4	Ir
ale	ale	k8xC	ale
především	především	k9	především
Skandinávce	Skandinávec	k1gMnSc4	Skandinávec
-	-	kIx~	-
ve	v	k7c6	v
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
White	Whit	k1gInSc5	Whit
Star	Star	kA	Star
Line	linout	k5eAaImIp3nS	linout
mohutně	mohutně	k6eAd1	mohutně
inzerovala	inzerovat	k5eAaImAgFnS	inzerovat
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
především	především	k6eAd1	především
švédských	švédský	k2eAgInPc2d1	švédský
rodiny	rodina	k1gFnSc2	rodina
koupila	koupit	k5eAaPmAgFnS	koupit
lístek	lístek	k1gInSc4	lístek
"	"	kIx"	"
<g/>
na	na	k7c4	na
první	první	k4xOgFnSc4	první
volnou	volný	k2eAgFnSc4d1	volná
loď	loď	k1gFnSc4	loď
do	do	k7c2	do
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Mezi	mezi	k7c7	mezi
cestujícími	cestující	k1gMnPc7	cestující
byli	být	k5eAaImAgMnP	být
však	však	k9	však
také	také	k9	také
Syřané	Syřan	k1gMnPc1	Syřan
<g/>
,	,	kIx,	,
Arméni	Armén	k1gMnPc1	Armén
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
národnosti	národnost	k1gFnPc1	národnost
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
<g/>
,	,	kIx,	,
nastoupivší	nastoupivší	k2eAgFnSc1d1	nastoupivší
v	v	k7c6	v
Cherbourgu	Cherbourg	k1gInSc6	Cherbourg
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
dopravili	dopravit	k5eAaPmAgMnP	dopravit
vlakem	vlak	k1gInSc7	vlak
z	z	k7c2	z
Marseille	Marseille	k1gFnSc2	Marseille
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posádce	posádka	k1gFnSc6	posádka
převažovali	převažovat	k5eAaImAgMnP	převažovat
Britové	Brit	k1gMnPc1	Brit
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
nebylo	být	k5eNaImAgNnS	být
na	na	k7c6	na
britských	britský	k2eAgFnPc6d1	britská
lodích	loď	k1gFnPc6	loď
pravidlem	pravidlem	k6eAd1	pravidlem
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
"	"	kIx"	"
<g/>
Černém	černý	k2eAgInSc6d1	černý
gangu	gang	k1gInSc6	gang
<g/>
"	"	kIx"	"
topičů	topič	k1gMnPc2	topič
a	a	k8xC	a
strojníků	strojník	k1gMnPc2	strojník
nalézáme	nalézat	k5eAaImIp1nP	nalézat
především	především	k9	především
irská	irský	k2eAgNnPc4d1	irské
jména	jméno	k1gNnPc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Titanic	Titanic	k1gInSc4	Titanic
opouštěl	opouštět	k5eAaImAgMnS	opouštět
ve	v	k7c4	v
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
Queenstown	Queenstown	k1gInSc1	Queenstown
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
záhuby	záhuba	k1gFnSc2	záhuba
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
irských	irský	k2eAgMnPc2d1	irský
emigrantů	emigrant	k1gMnPc2	emigrant
Eugene	Eugen	k1gInSc5	Eugen
Dally	Dall	k1gInPc7	Dall
hrál	hrát	k5eAaImAgInS	hrát
na	na	k7c4	na
irské	irský	k2eAgFnPc4d1	irská
dudy	dudy	k1gFnPc4	dudy
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Irish	Irish	k1gInSc1	Irish
Lament	Lament	k?	Lament
-	-	kIx~	-
Irské	irský	k2eAgFnSc3d1	irská
lamento	lamento	k?	lamento
<g/>
"	"	kIx"	"
-	-	kIx~	-
tentýž	týž	k3xTgInSc1	týž
Dally	Dalla	k1gMnSc2	Dalla
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
katastrofu	katastrofa	k1gFnSc4	katastrofa
přežil	přežít	k5eAaPmAgMnS	přežít
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
žaloval	žalovat	k5eAaImAgInS	žalovat
mezi	mezi	k7c7	mezi
stovkami	stovka	k1gFnPc7	stovka
jiných	jiný	k2eAgFnPc2d1	jiná
společnost	společnost	k1gFnSc1	společnost
White	Whit	k1gInSc5	Whit
Star	Star	kA	Star
Line	linout	k5eAaImIp3nS	linout
o	o	k7c4	o
náhradu	náhrada	k1gFnSc4	náhrada
škody	škoda	k1gFnSc2	škoda
-	-	kIx~	-
potopené	potopený	k2eAgFnPc1d1	potopená
dudy	dudy	k1gFnPc1	dudy
<g/>
.	.	kIx.	.
</s>
<s>
Plavba	plavba	k1gFnSc1	plavba
Titanicu	Titanicus	k1gInSc2	Titanicus
začínala	začínat	k5eAaImAgFnS	začínat
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1912	[number]	k4	1912
ve	v	k7c4	v
12	[number]	k4	12
<g/>
:	:	kIx,	:
<g/>
14	[number]	k4	14
vyvedením	vyvedení	k1gNnSc7	vyvedení
lodě	loď	k1gFnSc2	loď
z	z	k7c2	z
přístavních	přístavní	k2eAgFnPc2d1	přístavní
vod	voda	k1gFnPc2	voda
v	v	k7c6	v
Southamptonu	Southampton	k1gInSc6	Southampton
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
lodivoda	lodivod	k1gMnSc2	lodivod
George	Georg	k1gMnSc2	Georg
Bowyera	Bowyer	k1gMnSc2	Bowyer
<g/>
.	.	kIx.	.
</s>
<s>
Titanic	Titanic	k1gInSc1	Titanic
byl	být	k5eAaImAgInS	být
příliš	příliš	k6eAd1	příliš
velký	velký	k2eAgInSc1d1	velký
a	a	k8xC	a
neohrabaný	ohrabaný	k2eNgInSc1d1	neohrabaný
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgMnS	moct
sám	sám	k3xTgInSc4	sám
vymanévrovat	vymanévrovat	k5eAaPmF	vymanévrovat
z	z	k7c2	z
přístavu	přístav	k1gInSc2	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
mu	on	k3xPp3gNnSc3	on
zde	zde	k6eAd1	zde
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
přístavech	přístav	k1gInPc6	přístav
<g/>
,	,	kIx,	,
pomáhalo	pomáhat	k5eAaImAgNnS	pomáhat
několik	několik	k4yIc1	několik
remorkérů	remorkér	k1gInPc2	remorkér
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
incidentu	incident	k1gInSc3	incident
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
mohl	moct	k5eAaImAgInS	moct
předčasně	předčasně	k6eAd1	předčasně
plavbu	plavba	k1gFnSc4	plavba
ukončit	ukončit	k5eAaPmF	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	daleko	k6eNd1	daleko
plavební	plavební	k2eAgFnPc1d1	plavební
trasy	trasa	k1gFnPc1	trasa
Titanicu	Titanicus	k1gInSc2	Titanicus
byly	být	k5eAaImAgFnP	být
ukotveny	ukotvit	k5eAaPmNgInP	ukotvit
další	další	k2eAgInPc1d1	další
dva	dva	k4xCgInPc1	dva
parníky	parník	k1gInPc1	parník
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
a	a	k8xC	a
Oceanic	Oceanice	k1gFnPc2	Oceanice
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
míjení	míjení	k1gNnSc6	míjení
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
došlo	dojít	k5eAaPmAgNnS	dojít
vlivem	vlivem	k7c2	vlivem
proudění	proudění	k1gNnSc2	proudění
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
hnané	hnaný	k2eAgInPc4d1	hnaný
šrouby	šroub	k1gInPc4	šroub
Titanicu	Titanicus	k1gInSc2	Titanicus
<g/>
,	,	kIx,	,
k	k	k7c3	k
sacímu	sací	k2eAgInSc3d1	sací
efektu	efekt	k1gInSc3	efekt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
natolik	natolik	k6eAd1	natolik
silný	silný	k2eAgInSc1d1	silný
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
přetrhla	přetrhnout	k5eAaPmAgNnP	přetrhnout
kotvící	kotvící	k2eAgNnPc1d1	kotvící
lana	lano	k1gNnPc1	lano
New	New	k1gMnSc2	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
zadní	zadní	k2eAgFnSc1d1	zadní
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
nebezpečně	bezpečně	k6eNd1	bezpečně
přibližovat	přibližovat	k5eAaImF	přibližovat
k	k	k7c3	k
Titanicu	Titanicus	k1gInSc3	Titanicus
<g/>
.	.	kIx.	.
</s>
<s>
Hrozící	hrozící	k2eAgFnSc4d1	hrozící
kolizi	kolize	k1gFnSc4	kolize
odvrátil	odvrátit	k5eAaPmAgMnS	odvrátit
kapitán	kapitán	k1gMnSc1	kapitán
E.	E.	kA	E.
J.	J.	kA	J.
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nechal	nechat	k5eAaPmAgMnS	nechat
zastavit	zastavit	k5eAaPmF	zastavit
stroje	stroj	k1gInPc4	stroj
na	na	k7c4	na
Titanicu	Titanica	k1gFnSc4	Titanica
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
remorkérů	remorkér	k1gInPc2	remorkér
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
obeplul	obeplout	k5eAaPmAgInS	obeplout
New	New	k1gFnSc4	New
York	York	k1gInSc1	York
a	a	k8xC	a
pomocí	pomocí	k7c2	pomocí
lana	lano	k1gNnSc2	lano
jej	on	k3xPp3gMnSc4	on
táhl	táhnout	k5eAaImAgInS	táhnout
zpět	zpět	k6eAd1	zpět
ke	k	k7c3	k
břehu	břeh	k1gInSc3	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
lodě	loď	k1gFnPc1	loď
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
minuly	minout	k5eAaImAgFnP	minout
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
desítek	desítka	k1gFnPc2	desítka
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Kolizi	kolize	k1gFnSc4	kolize
s	s	k7c7	s
Titanicem	Titanic	k1gMnSc7	Titanic
bylo	být	k5eAaImAgNnS	být
sice	sice	k8xC	sice
zabráněno	zabránit	k5eAaPmNgNnS	zabránit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
opřel	opřít	k5eAaPmAgMnS	opřít
o	o	k7c4	o
bok	bok	k1gInSc4	bok
Oceanicu	Oceanicus	k1gInSc2	Oceanicus
<g/>
,	,	kIx,	,
naštěstí	naštěstí	k6eAd1	naštěstí
bez	bez	k7c2	bez
větších	veliký	k2eAgFnPc2d2	veliký
škod	škoda	k1gFnPc2	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
plavbě	plavba	k1gFnSc6	plavba
přes	přes	k7c4	přes
Lamanšský	lamanšský	k2eAgInSc4d1	lamanšský
průliv	průliv	k1gInSc4	průliv
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
zjistil	zjistit	k5eAaPmAgInS	zjistit
George	George	k1gInSc1	George
Symons	Symons	k1gInSc1	Symons
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
hlídkové	hlídkový	k2eAgFnSc2d1	hlídková
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
pozorovacím	pozorovací	k2eAgInSc6d1	pozorovací
koši	koš	k1gInSc6	koš
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
vraním	vraní	k2eAgNnSc6d1	vraní
hnízdě	hnízdo	k1gNnSc6	hnízdo
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
na	na	k7c6	na
předním	přední	k2eAgInSc6d1	přední
stožáru	stožár	k1gInSc6	stožár
chybí	chybět	k5eAaImIp3nS	chybět
dalekohledy	dalekohled	k1gInPc4	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Vyhledal	vyhledat	k5eAaPmAgMnS	vyhledat
a	a	k8xC	a
požádal	požádat	k5eAaPmAgMnS	požádat
2	[number]	k4	2
<g/>
.	.	kIx.	.
důstojníka	důstojník	k1gMnSc4	důstojník
Lightollera	Lightoller	k1gMnSc4	Lightoller
o	o	k7c4	o
zajištění	zajištění	k1gNnSc4	zajištění
dalekohledů	dalekohled	k1gInPc2	dalekohled
<g/>
,	,	kIx,	,
a	a	k8xC	a
ten	ten	k3xDgInSc1	ten
tento	tento	k3xDgInSc1	tento
požadavek	požadavek	k1gInSc1	požadavek
dále	daleko	k6eAd2	daleko
předal	předat	k5eAaPmAgInS	předat
1	[number]	k4	1
<g/>
.	.	kIx.	.
důstojníkovi	důstojník	k1gMnSc3	důstojník
Murdochovi	Murdoch	k1gMnSc3	Murdoch
<g/>
.	.	kIx.	.
</s>
<s>
Prozatím	prozatím	k6eAd1	prozatím
byl	být	k5eAaImAgInS	být
George	George	k1gInSc1	George
Symons	Symons	k1gInSc1	Symons
obeznámen	obeznámen	k2eAgInSc1d1	obeznámen
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
věc	věc	k1gFnSc1	věc
se	se	k3xPyFc4	se
řeší	řešit	k5eAaImIp3nS	řešit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
plavbě	plavba	k1gFnSc6	plavba
z	z	k7c2	z
Belfastu	Belfast	k1gInSc2	Belfast
byl	být	k5eAaImAgInS	být
pozorovací	pozorovací	k2eAgInSc1d1	pozorovací
koš	koš	k1gInSc1	koš
ještě	ještě	k9	ještě
dalekohledy	dalekohled	k1gInPc4	dalekohled
vybaven	vybaven	k2eAgInSc1d1	vybaven
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
2	[number]	k4	2
<g/>
.	.	kIx.	.
důstojník	důstojník	k1gMnSc1	důstojník
Blair	Blair	k1gMnSc1	Blair
opouštěl	opouštět	k5eAaImAgMnS	opouštět
v	v	k7c6	v
Southamptonu	Southampton	k1gInSc6	Southampton
loď	loď	k1gFnSc4	loď
<g/>
,	,	kIx,	,
tyto	tento	k3xDgInPc4	tento
dalekohledy	dalekohled	k1gInPc4	dalekohled
uložil	uložit	k5eAaPmAgInS	uložit
v	v	k7c6	v
kajutě	kajuta	k1gFnSc6	kajuta
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
nástupce	nástupce	k1gMnSc1	nástupce
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
důstojník	důstojník	k1gMnSc1	důstojník
Lightoller	Lightoller	k1gMnSc1	Lightoller
však	však	k9	však
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
nevěděl	vědět	k5eNaImAgMnS	vědět
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
hlídky	hlídka	k1gFnPc1	hlídka
v	v	k7c6	v
pozorovacím	pozorovací	k2eAgInSc6d1	pozorovací
koši	koš	k1gInSc6	koš
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
bez	bez	k7c2	bez
dalekohledů	dalekohled	k1gInPc2	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
podvečer	podvečer	k1gInSc4	podvečer
doplul	doplout	k5eAaPmAgInS	doplout
Titanic	Titanic	k1gInSc1	Titanic
do	do	k7c2	do
francouzského	francouzský	k2eAgInSc2d1	francouzský
Cherbourgu	Cherbourg	k1gInSc2	Cherbourg
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgFnP	být
doplněny	doplnit	k5eAaPmNgFnP	doplnit
další	další	k2eAgFnPc1d1	další
zásoby	zásoba	k1gFnPc1	zásoba
a	a	k8xC	a
naloděni	naloděn	k2eAgMnPc1d1	naloděn
další	další	k2eAgMnPc1d1	další
cestující	cestující	k1gMnPc1	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
zastávce	zastávka	k1gFnSc6	zastávka
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
v	v	k7c6	v
21	[number]	k4	21
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
vydal	vydat	k5eAaPmAgMnS	vydat
k	k	k7c3	k
pobřeží	pobřeží	k1gNnSc3	pobřeží
Irska	Irsko	k1gNnSc2	Irsko
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1912	[number]	k4	1912
měl	mít	k5eAaImAgInS	mít
Titanic	Titanic	k1gInSc1	Titanic
poslední	poslední	k2eAgFnSc4d1	poslední
zastávku	zastávka	k1gFnSc4	zastávka
u	u	k7c2	u
evropského	evropský	k2eAgNnSc2d1	Evropské
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
v	v	k7c6	v
irském	irský	k2eAgInSc6d1	irský
přístavu	přístav	k1gInSc6	přístav
Queenstown	Queenstown	k1gInSc1	Queenstown
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přístavu	přístav	k1gInSc6	přístav
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
hloubka	hloubka	k1gFnSc1	hloubka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
projevila	projevit	k5eAaPmAgFnS	projevit
velká	velký	k2eAgFnSc1d1	velká
sací	sací	k2eAgFnSc1d1	sací
síla	síla	k1gFnSc1	síla
lodních	lodní	k2eAgInPc2d1	lodní
šroubů	šroub	k1gInPc2	šroub
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
zvířeno	zvířen	k2eAgNnSc4d1	zvířen
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
písku	písek	k1gInSc2	písek
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
znepokojil	znepokojit	k5eAaPmAgInS	znepokojit
pasažéry	pasažér	k1gMnPc7	pasažér
a	a	k8xC	a
posádka	posádka	k1gFnSc1	posádka
musela	muset	k5eAaImAgFnS	muset
vysvětlovat	vysvětlovat	k5eAaImF	vysvětlovat
příčinu	příčina	k1gFnSc4	příčina
tohoto	tento	k3xDgInSc2	tento
jevu	jev	k1gInSc2	jev
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Titanic	Titanic	k1gInSc1	Titanic
při	při	k7c6	při
plavbě	plavba	k1gFnSc6	plavba
v	v	k7c6	v
queenstownském	queenstownský	k2eAgInSc6d1	queenstownský
přístavu	přístav	k1gInSc6	přístav
dokonce	dokonce	k9	dokonce
táhl	táhnout	k5eAaImAgMnS	táhnout
potopený	potopený	k2eAgInSc4d1	potopený
vlečný	vlečný	k2eAgInSc4d1	vlečný
člun	člun	k1gInSc4	člun
asi	asi	k9	asi
800	[number]	k4	800
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
při	při	k7c6	při
předchozí	předchozí	k2eAgFnSc6d1	předchozí
zastávce	zastávka	k1gFnSc6	zastávka
v	v	k7c6	v
Cherbourgu	Cherbourg	k1gInSc6	Cherbourg
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
i	i	k9	i
zde	zde	k6eAd1	zde
doplněny	doplnit	k5eAaPmNgFnP	doplnit
zásoby	zásoba	k1gFnPc1	zásoba
<g/>
,	,	kIx,	,
naloděno	naloděn	k2eAgNnSc1d1	naloděno
posledních	poslední	k2eAgMnPc2d1	poslední
130	[number]	k4	130
cestujících	cestující	k1gMnPc2	cestující
a	a	k8xC	a
1	[number]	k4	1
400	[number]	k4	400
pytlů	pytel	k1gInPc2	pytel
pošty	pošta	k1gFnSc2	pošta
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
12	[number]	k4	12
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
vyplul	vyplout	k5eAaPmAgInS	vyplout
západním	západní	k2eAgInSc7d1	západní
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
Atlantiku	Atlantik	k1gInSc2	Atlantik
<g/>
,	,	kIx,	,
směr	směr	k1gInSc1	směr
New	New	k1gMnSc1	New
York	York	k1gInSc1	York
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
odpoledne	odpoledne	k6eAd1	odpoledne
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
plul	plout	k5eAaImAgMnS	plout
Titanic	Titanic	k1gInSc4	Titanic
poblíž	poblíž	k7c2	poblíž
pobřeží	pobřeží	k1gNnSc2	pobřeží
Irska	Irsko	k1gNnSc2	Irsko
a	a	k8xC	a
až	až	k9	až
k	k	k7c3	k
večeru	večer	k1gInSc3	večer
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
proplul	proplout	k5eAaPmAgInS	proplout
kolem	kolem	k7c2	kolem
Fastnet	Fastneta	k1gFnPc2	Fastneta
Rocku	rock	k1gInSc2	rock
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
definitivně	definitivně	k6eAd1	definitivně
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
vod	voda	k1gFnPc2	voda
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Plavba	plavba	k1gFnSc1	plavba
probíhala	probíhat	k5eAaImAgFnS	probíhat
bez	bez	k7c2	bez
komplikací	komplikace	k1gFnPc2	komplikace
<g/>
,	,	kIx,	,
cestující	cestující	k1gFnSc1	cestující
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
si	se	k3xPyFc3	se
dopřávali	dopřávat	k5eAaImAgMnP	dopřávat
komfortu	komfort	k1gInSc2	komfort
poskytovanému	poskytovaný	k2eAgInSc3d1	poskytovaný
lodní	lodní	k2eAgFnSc7d1	lodní
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
komfort	komfort	k1gInSc4	komfort
jenž	jenž	k3xRgMnSc1	jenž
si	se	k3xPyFc3	se
zaplatili	zaplatit	k5eAaPmAgMnP	zaplatit
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
i	i	k9	i
o	o	k7c6	o
cestující	cestující	k1gFnSc6	cestující
3	[number]	k4	3
<g/>
.	.	kIx.	.
třídy	třída	k1gFnPc1	třída
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
postaráno	postarán	k2eAgNnSc1d1	postaráno
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
cestující	cestující	k1gMnPc4	cestující
nevěděli	vědět	k5eNaImAgMnP	vědět
a	a	k8xC	a
co	co	k3yQnSc1	co
bylo	být	k5eAaImAgNnS	být
hlavně	hlavně	k9	hlavně
určeno	určit	k5eAaPmNgNnS	určit
pro	pro	k7c4	pro
kapitána	kapitán	k1gMnSc4	kapitán
a	a	k8xC	a
důstojníky	důstojník	k1gMnPc4	důstojník
Titanicu	Titanicus	k1gInSc2	Titanicus
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
ledovém	ledový	k2eAgNnSc6d1	ledové
poli	pole	k1gNnSc6	pole
před	před	k7c7	před
nimi	on	k3xPp3gMnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
zprávy	zpráva	k1gFnPc1	zpráva
přicházely	přicházet	k5eAaImAgFnP	přicházet
rádiodepešemi	rádiodepeše	k1gFnPc7	rádiodepeše
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
z	z	k7c2	z
parníku	parník	k1gInSc2	parník
Touraine	Tourain	k1gMnSc5	Tourain
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
výskytu	výskyt	k1gInSc6	výskyt
ledu	led	k1gInSc2	led
byly	být	k5eAaImAgFnP	být
zaznamenány	zaznamenat	k5eAaPmNgFnP	zaznamenat
na	na	k7c6	na
mapě	mapa	k1gFnSc6	mapa
<g/>
,	,	kIx,	,
důstojníci	důstojník	k1gMnPc1	důstojník
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
shodli	shodnout	k5eAaPmAgMnP	shodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
neleží	ležet	k5eNaImIp3nS	ležet
v	v	k7c6	v
plánované	plánovaný	k2eAgFnSc6d1	plánovaná
trase	trasa	k1gFnSc6	trasa
plavby	plavba	k1gFnSc2	plavba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poledne	poledne	k1gNnSc2	poledne
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
poledne	poledne	k1gNnSc2	poledne
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
urazil	urazit	k5eAaPmAgMnS	urazit
Titanic	Titanic	k1gInSc4	Titanic
386	[number]	k4	386
mil	míle	k1gFnPc2	míle
(	(	kIx(	(
<g/>
621	[number]	k4	621
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Plavba	plavba	k1gFnSc1	plavba
probíhala	probíhat	k5eAaImAgFnS	probíhat
stejně	stejně	k6eAd1	stejně
poklidně	poklidně	k6eAd1	poklidně
jako	jako	k9	jako
předešlého	předešlý	k2eAgInSc2d1	předešlý
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poledne	poledne	k1gNnSc2	poledne
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
poledne	poledne	k1gNnSc2	poledne
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
urazil	urazit	k5eAaPmAgMnS	urazit
Titanic	Titanic	k1gInSc4	Titanic
519	[number]	k4	519
mil	míle	k1gFnPc2	míle
(	(	kIx(	(
<g/>
835	[number]	k4	835
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počasí	počasí	k1gNnSc1	počasí
bylo	být	k5eAaImAgNnS	být
stále	stále	k6eAd1	stále
dobré	dobrý	k2eAgNnSc1d1	dobré
<g/>
,	,	kIx,	,
v	v	k7c6	v
dopoledních	dopolední	k2eAgFnPc6d1	dopolední
hodinách	hodina	k1gFnPc6	hodina
sloužil	sloužit	k5eAaImAgMnS	sloužit
kapitán	kapitán	k1gMnSc1	kapitán
E.	E.	kA	E.
J.	J.	kA	J.
Smith	Smith	k1gMnSc1	Smith
bohoslužbu	bohoslužba	k1gFnSc4	bohoslužba
v	v	k7c6	v
jídelně	jídelna	k1gFnSc6	jídelna
a	a	k8xC	a
poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
jídelna	jídelna	k1gFnSc1	jídelna
opět	opět	k6eAd1	opět
připravena	připravit	k5eAaPmNgFnS	připravit
ke	k	k7c3	k
stolování	stolování	k1gNnSc3	stolování
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zaznamenávaných	zaznamenávaný	k2eAgInPc2d1	zaznamenávaný
rychlostních	rychlostní	k2eAgInPc2d1	rychlostní
údajů	údaj	k1gInPc2	údaj
se	se	k3xPyFc4	se
rychlost	rychlost	k1gFnSc1	rychlost
Titanicu	Titanicus	k1gInSc2	Titanicus
stále	stále	k6eAd1	stále
zvyšovala	zvyšovat	k5eAaImAgFnS	zvyšovat
a	a	k8xC	a
k	k	k7c3	k
poledni	poledne	k1gNnSc3	poledne
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
urazil	urazit	k5eAaPmAgMnS	urazit
dalších	další	k2eAgFnPc2d1	další
546	[number]	k4	546
mil	míle	k1gFnPc2	míle
(	(	kIx(	(
<g/>
879	[number]	k4	879
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
J.	J.	kA	J.
Bruce	Bruce	k1gFnSc1	Bruce
Ismaye	Ismaye	k1gFnSc1	Ismaye
a	a	k8xC	a
kapitán	kapitán	k1gMnSc1	kapitán
E.	E.	kA	E.
J.	J.	kA	J.
Smith	Smith	k1gMnSc1	Smith
sice	sice	k8xC	sice
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
dnech	den	k1gInPc6	den
po	po	k7c6	po
vyplutí	vyplutí	k1gNnSc6	vyplutí
předpokládali	předpokládat	k5eAaImAgMnP	předpokládat
vyšší	vysoký	k2eAgFnSc4d2	vyšší
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
kapitán	kapitán	k1gMnSc1	kapitán
při	při	k7c6	při
zachování	zachování	k1gNnSc6	zachování
dobrého	dobrý	k2eAgNnSc2d1	dobré
počasí	počasí	k1gNnSc2	počasí
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
rekordního	rekordní	k2eAgInSc2d1	rekordní
času	čas	k1gInSc2	čas
plavby	plavba	k1gFnSc2	plavba
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
zapálení	zapálení	k1gNnSc2	zapálení
pod	pod	k7c7	pod
dalšími	další	k2eAgFnPc7d1	další
kotli	kotel	k1gInSc3	kotel
<g/>
,	,	kIx,	,
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
,	,	kIx,	,
plánovalo	plánovat	k5eAaImAgNnS	plánovat
až	až	k9	až
na	na	k7c4	na
15	[number]	k4	15
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
pod	pod	k7c7	pod
kotli	kotel	k1gInSc6	kotel
zapáleno	zapálit	k5eAaPmNgNnS	zapálit
již	již	k6eAd1	již
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
a	a	k8xC	a
Titanic	Titanic	k1gInSc1	Titanic
již	již	k6eAd1	již
plul	plout	k5eAaImAgInS	plout
rychlostí	rychlost	k1gFnSc7	rychlost
21	[number]	k4	21
uzlů	uzel	k1gInPc2	uzel
(	(	kIx(	(
<g/>
39	[number]	k4	39
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dopoledne	dopoledne	k1gNnSc2	dopoledne
přicházely	přicházet	k5eAaImAgFnP	přicházet
další	další	k2eAgFnPc1d1	další
radiodepeše	radiodepeše	k1gFnPc1	radiodepeše
-	-	kIx~	-
"	"	kIx"	"
<g/>
marconigramy	marconigram	k1gInPc1	marconigram
<g/>
"	"	kIx"	"
se	s	k7c7	s
znepokojujícími	znepokojující	k2eAgFnPc7d1	znepokojující
informacemi	informace	k1gFnPc7	informace
o	o	k7c6	o
výskytu	výskyt	k1gInSc6	výskyt
ledového	ledový	k2eAgNnSc2d1	ledové
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
zpráva	zpráva	k1gFnSc1	zpráva
lodi	loď	k1gFnSc2	loď
Caronia	Caronium	k1gNnSc2	Caronium
<g/>
:	:	kIx,	:
ledové	ledový	k2eAgNnSc1d1	ledové
pole	pole	k1gNnSc1	pole
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
až	až	k6eAd1	až
,	,	kIx,	,
11	[number]	k4	11
<g/>
:	:	kIx,	:
<g/>
40	[number]	k4	40
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
parník	parník	k1gInSc1	parník
Noordam	Noordam	k1gInSc1	Noordam
<g />
.	.	kIx.	.
</s>
<s>
předchozí	předchozí	k2eAgFnSc4d1	předchozí
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
v	v	k7c6	v
13	[number]	k4	13
<g/>
:	:	kIx,	:
<g/>
42	[number]	k4	42
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
zpráva	zpráva	k1gFnSc1	zpráva
parníku	parník	k1gInSc2	parník
Baltic	Baltice	k1gFnPc2	Baltice
<g/>
:	:	kIx,	:
Mírný	mírný	k2eAgInSc1d1	mírný
<g/>
,	,	kIx,	,
proměnlivý	proměnlivý	k2eAgInSc1d1	proměnlivý
vítr	vítr	k1gInSc1	vítr
<g/>
,	,	kIx,	,
jasné	jasný	k2eAgNnSc1d1	jasné
a	a	k8xC	a
dobré	dobrý	k2eAgNnSc1d1	dobré
počasí	počasí	k1gNnSc1	počasí
a	a	k8xC	a
předal	předat	k5eAaPmAgMnS	předat
zprávu	zpráva	k1gFnSc4	zpráva
řecké	řecký	k2eAgFnSc2d1	řecká
lodi	loď	k1gFnSc2	loď
Athenai	Athena	k1gFnSc2	Athena
<g/>
:	:	kIx,	:
ledové	ledový	k2eAgNnSc1d1	ledové
pole	pole	k1gNnSc1	pole
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
,	,	kIx,	,
v	v	k7c6	v
13	[number]	k4	13
<g/>
:	:	kIx,	:
<g/>
45	[number]	k4	45
zpráva	zpráva	k1gFnSc1	zpráva
německého	německý	k2eAgInSc2d1	německý
parníku	parník	k1gInSc2	parník
<g />
.	.	kIx.	.
</s>
<s>
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
:	:	kIx,	:
setkání	setkání	k1gNnSc1	setkání
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
velkými	velký	k2eAgInPc7d1	velký
ledovci	ledovec	k1gInPc7	ledovec
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
V	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
plul	plout	k5eAaImAgInS	plout
Titanic	Titanic	k1gInSc1	Titanic
rychlostí	rychlost	k1gFnSc7	rychlost
přibližně	přibližně	k6eAd1	přibližně
22	[number]	k4	22
uzlů	uzel	k1gInPc2	uzel
(	(	kIx(	(
<g/>
41	[number]	k4	41
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
a	a	k8xC	a
blížil	blížit	k5eAaImAgMnS	blížit
se	se	k3xPyFc4	se
k	k	k7c3	k
mělčinám	mělčina	k1gFnPc3	mělčina
v	v	k7c6	v
kurzu	kurz	k1gInSc6	kurz
242	[number]	k4	242
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgInS	mít
změnit	změnit	k5eAaPmF	změnit
kurz	kurz	k1gInSc1	kurz
na	na	k7c4	na
265	[number]	k4	265
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
New	New	k1gMnSc1	New
York	York	k1gInSc1	York
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
200	[number]	k4	200
mil	míle	k1gFnPc2	míle
(	(	kIx(	(
<g/>
320	[number]	k4	320
km	km	kA	km
<g/>
)	)	kIx)	)
severně	severně	k6eAd1	severně
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
trasy	trasa	k1gFnSc2	trasa
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
ledová	ledový	k2eAgFnSc1d1	ledová
pole	pole	k1gFnSc1	pole
<g/>
,	,	kIx,	,
na	na	k7c4	na
která	který	k3yQgFnSc1	který
upozornila	upozornit	k5eAaPmAgFnS	upozornit
loď	loď	k1gFnSc4	loď
Caronia	Caronium	k1gNnSc2	Caronium
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
informace	informace	k1gFnPc1	informace
ukazovaly	ukazovat	k5eAaImAgFnP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ledová	ledový	k2eAgNnPc1d1	ledové
pole	pole	k1gNnPc1	pole
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
neobvykle	obvykle	k6eNd1	obvykle
jižněji	jižně	k6eAd2	jižně
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
očekávalo	očekávat	k5eAaImAgNnS	očekávat
<g/>
.	.	kIx.	.
</s>
<s>
Velení	velení	k1gNnSc1	velení
Titanicu	Titanicus	k1gInSc2	Titanicus
mělo	mít	k5eAaImAgNnS	mít
dostatek	dostatek	k1gInSc4	dostatek
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
výskytu	výskyt	k1gInSc6	výskyt
ledovců	ledovec	k1gInPc2	ledovec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
se	s	k7c7	s
zprávami	zpráva	k1gFnPc7	zpráva
nebylo	být	k5eNaImAgNnS	být
účelně	účelně	k6eAd1	účelně
nakládáno	nakládat	k5eAaImNgNnS	nakládat
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
zprávu	zpráva	k1gFnSc4	zpráva
z	z	k7c2	z
parníku	parník	k1gInSc2	parník
Baltic	Baltice	k1gFnPc2	Baltice
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
taktéž	taktéž	k?	taktéž
patřil	patřit	k5eAaImAgInS	patřit
společnosti	společnost	k1gFnSc3	společnost
White	Whit	k1gInSc5	Whit
Star	Star	kA	Star
Line	linout	k5eAaImIp3nS	linout
<g/>
,	,	kIx,	,
kapitán	kapitán	k1gMnSc1	kapitán
Smith	Smith	k1gMnSc1	Smith
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ponechal	ponechat	k5eAaPmAgMnS	ponechat
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
u	u	k7c2	u
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
po	po	k7c6	po
obědě	oběd	k1gInSc6	oběd
ji	on	k3xPp3gFnSc4	on
předal	předat	k5eAaPmAgMnS	předat
J.	J.	kA	J.
B.	B.	kA	B.
Ismayovi	Ismaya	k1gMnSc3	Ismaya
<g/>
,	,	kIx,	,
řediteli	ředitel	k1gMnSc3	ředitel
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
sice	sice	k8xC	sice
přečetl	přečíst	k5eAaPmAgMnS	přečíst
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ponechal	ponechat	k5eAaPmAgInS	ponechat
taktéž	taktéž	k?	taktéž
u	u	k7c2	u
sebe	se	k3xPyFc2	se
a	a	k8xC	a
odpoledne	odpoledne	k6eAd1	odpoledne
ji	on	k3xPp3gFnSc4	on
ukázal	ukázat	k5eAaPmAgMnS	ukázat
některým	některý	k3yIgMnPc3	některý
cestujícím	cestující	k1gMnPc3	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
zapomněl	zapomnět	k5eAaImAgMnS	zapomnět
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nedostala	dostat	k5eNaPmAgFnS	dostat
na	na	k7c4	na
kapitánský	kapitánský	k2eAgInSc4d1	kapitánský
můstek	můstek	k1gInSc4	můstek
k	k	k7c3	k
velícímu	velící	k2eAgMnSc3d1	velící
důstojníkovi	důstojník	k1gMnSc3	důstojník
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
zprávy	zpráva	k1gFnSc2	zpráva
z	z	k7c2	z
parníku	parník	k1gInSc2	parník
Amerika	Amerika	k1gFnSc1	Amerika
není	být	k5eNaImIp3nS	být
taktéž	taktéž	k?	taktéž
zcela	zcela	k6eAd1	zcela
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
<g/>
-li	i	k?	-li
doručena	doručen	k2eAgFnSc1d1	doručena
až	až	k9	až
na	na	k7c4	na
kapitánský	kapitánský	k2eAgInSc4d1	kapitánský
můstek	můstek	k1gInSc4	můstek
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
byla	být	k5eAaImAgFnS	být
totiž	totiž	k9	totiž
zachycena	zachytit	k5eAaPmNgFnS	zachytit
na	na	k7c4	na
radiostanici	radiostanice	k1gFnSc4	radiostanice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
občanský	občanský	k2eAgInSc4d1	občanský
provoz	provoz	k1gInSc4	provoz
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
na	na	k7c6	na
vlnách	vlna	k1gFnPc6	vlna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nebyly	být	k5eNaImAgFnP	být
určeny	určit	k5eAaPmNgFnP	určit
pro	pro	k7c4	pro
námořní	námořní	k2eAgInSc4d1	námořní
provoz	provoz	k1gInSc4	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
večerním	večerní	k2eAgNnSc7d1	večerní
střídáním	střídání	k1gNnSc7	střídání
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
v	v	k7c6	v
17	[number]	k4	17
<g/>
:	:	kIx,	:
<g/>
50	[number]	k4	50
změnil	změnit	k5eAaPmAgInS	změnit
Titanic	Titanic	k1gInSc1	Titanic
kurz	kurz	k1gInSc1	kurz
na	na	k7c6	na
289	[number]	k4	289
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
šest	šest	k4xCc4	šest
hodin	hodina	k1gFnPc2	hodina
večer	večer	k6eAd1	večer
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
střídání	střídání	k1gNnSc1	střídání
důstojníků	důstojník	k1gMnPc2	důstojník
na	na	k7c6	na
můstku	můstek	k1gInSc6	můstek
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
služby	služba	k1gFnSc2	služba
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
2	[number]	k4	2
<g/>
.	.	kIx.	.
důstojník	důstojník	k1gMnSc1	důstojník
Lightoller	Lightoller	k1gMnSc1	Lightoller
<g/>
,	,	kIx,	,
důstojníci	důstojník	k1gMnPc1	důstojník
H.	H.	kA	H.
G.	G.	kA	G.
Lowe	Lowe	k1gInSc4	Lowe
a	a	k8xC	a
H.	H.	kA	H.
J.	J.	kA	J.
Pitman	Pitman	k1gMnSc1	Pitman
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kormidlu	kormidlo	k1gNnSc3	kormidlo
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
2	[number]	k4	2
<g/>
.	.	kIx.	.
kormidelník	kormidelník	k1gMnSc1	kormidelník
Robert	Robert	k1gMnSc1	Robert
Hitchens	Hitchens	k1gInSc4	Hitchens
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
plul	plout	k5eAaImAgInS	plout
Titanic	Titanic	k1gInSc1	Titanic
v	v	k7c6	v
kurzu	kurz	k1gInSc6	kurz
289	[number]	k4	289
<g/>
°	°	k?	°
a	a	k8xC	a
kurz	kurz	k1gInSc1	kurz
měl	mít	k5eAaImAgInS	mít
udržovat	udržovat	k5eAaImF	udržovat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
střídání	střídání	k1gNnSc6	střídání
služeb	služba	k1gFnPc2	služba
nastoupili	nastoupit	k5eAaPmAgMnP	nastoupit
do	do	k7c2	do
pozorovacího	pozorovací	k2eAgInSc2d1	pozorovací
koše	koš	k1gInSc2	koš
na	na	k7c6	na
předním	přední	k2eAgInSc6d1	přední
stožáru	stožár	k1gInSc6	stožár
Jewell	Jewell	k1gInSc1	Jewell
a	a	k8xC	a
Symons	Symons	k1gInSc1	Symons
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
důstojník	důstojník	k1gMnSc1	důstojník
Lowe	Low	k1gFnSc2	Low
se	se	k3xPyFc4	se
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
do	do	k7c2	do
služby	služba	k1gFnSc2	služba
stavil	stavit	k5eAaImAgMnS	stavit
v	v	k7c6	v
mapovně	mapovna	k1gFnSc6	mapovna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zahlédl	zahlédnout	k5eAaPmAgMnS	zahlédnout
lístek	lístek	k1gInSc4	lístek
s	s	k7c7	s
poznámkou	poznámka	k1gFnSc7	poznámka
"	"	kIx"	"
<g/>
led	led	k1gInSc1	led
<g/>
"	"	kIx"	"
s	s	k7c7	s
udanými	udaný	k2eAgFnPc7d1	udaná
souřadnicemi	souřadnice	k1gFnPc7	souřadnice
<g/>
.	.	kIx.	.
</s>
<s>
Provedl	provést	k5eAaPmAgInS	provést
výpočet	výpočet	k1gInSc1	výpočet
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgNnSc2	který
mu	on	k3xPp3gMnSc3	on
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
místu	místo	k1gNnSc3	místo
setkání	setkání	k1gNnSc2	setkání
s	s	k7c7	s
ledem	led	k1gInSc7	led
dojde	dojít	k5eAaPmIp3nS	dojít
až	až	k9	až
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
jeho	jeho	k3xOp3gFnSc2	jeho
služby	služba	k1gFnSc2	služba
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
o	o	k7c4	o
toto	tento	k3xDgNnSc4	tento
nezajímal	zajímat	k5eNaImAgMnS	zajímat
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
sedmé	sedmý	k4xOgFnSc2	sedmý
hodiny	hodina	k1gFnSc2	hodina
přišel	přijít	k5eAaPmAgInS	přijít
na	na	k7c4	na
můstek	můstek	k1gInSc4	můstek
1	[number]	k4	1
<g/>
.	.	kIx.	.
důstojník	důstojník	k1gMnSc1	důstojník
Murdoch	Murdoch	k1gMnSc1	Murdoch
a	a	k8xC	a
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
Lightollera	Lightoller	k1gMnSc4	Lightoller
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
odešel	odejít	k5eAaPmAgMnS	odejít
na	na	k7c6	na
večeři	večeře	k1gFnSc6	večeře
<g/>
.	.	kIx.	.
</s>
<s>
Murdoch	Murdoch	k1gMnSc1	Murdoch
si	se	k3xPyFc3	se
byl	být	k5eAaImAgMnS	být
vědom	vědom	k2eAgMnSc1d1	vědom
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
k	k	k7c3	k
ledovému	ledový	k2eAgNnSc3d1	ledové
poli	pole	k1gNnSc3	pole
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zlepšení	zlepšení	k1gNnSc4	zlepšení
pozorování	pozorování	k1gNnSc2	pozorování
požádal	požádat	k5eAaPmAgMnS	požádat
námořníka	námořník	k1gMnSc4	námořník
Samuela	Samuel	k1gMnSc4	Samuel
Hemminga	Hemming	k1gMnSc4	Hemming
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
všechny	všechen	k3xTgInPc4	všechen
otvory	otvor	k1gInPc4	otvor
<g/>
,	,	kIx,	,
kterými	který	k3yQgInPc7	který
na	na	k7c4	na
přední	přední	k2eAgFnSc4d1	přední
palubu	paluba	k1gFnSc4	paluba
pronikalo	pronikat	k5eAaImAgNnS	pronikat
světlo	světlo	k1gNnSc1	světlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
byla	být	k5eAaImAgFnS	být
zachycena	zachytit	k5eAaPmNgFnS	zachytit
zpráva	zpráva	k1gFnSc1	zpráva
parníku	parník	k1gInSc2	parník
Californian	Californiany	k1gInPc2	Californiany
a	a	k8xC	a
parníku	parník	k1gInSc6	parník
Antillian	Antilliana	k1gFnPc2	Antilliana
<g/>
:	:	kIx,	:
tři	tři	k4xCgFnPc1	tři
velké	velká	k1gFnPc1	velká
ledovce	ledovec	k1gInSc2	ledovec
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
Kolem	kolem	k7c2	kolem
osmé	osmý	k4xOgFnSc2	osmý
hodiny	hodina	k1gFnSc2	hodina
večer	večer	k6eAd1	večer
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
další	další	k2eAgNnSc1d1	další
střídání	střídání	k1gNnSc1	střídání
a	a	k8xC	a
na	na	k7c4	na
místa	místo	k1gNnPc4	místo
mladších	mladý	k2eAgMnPc2d2	mladší
důstojníků	důstojník	k1gMnPc2	důstojník
nastoupili	nastoupit	k5eAaPmAgMnP	nastoupit
Boxhall	Boxhalla	k1gFnPc2	Boxhalla
a	a	k8xC	a
Moody	Mooda	k1gFnSc2	Mooda
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
vystřídány	vystřídán	k2eAgFnPc1d1	vystřídána
hlídky	hlídka	k1gFnPc1	hlídka
ve	v	k7c6	v
vraním	vraní	k2eAgNnSc6d1	vraní
hnízdě	hnízdo	k1gNnSc6	hnízdo
a	a	k8xC	a
taktéž	taktéž	k?	taktéž
kormidelník	kormidelník	k1gMnSc1	kormidelník
Robert	Robert	k1gMnSc1	Robert
Hitchens	Hitchens	k1gInSc4	Hitchens
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
můstku	můstek	k1gInSc6	můstek
zůstal	zůstat	k5eAaPmAgMnS	zůstat
důstojník	důstojník	k1gMnSc1	důstojník
Lightoller	Lightoller	k1gMnSc1	Lightoller
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
končila	končit	k5eAaImAgFnS	končit
služba	služba	k1gFnSc1	služba
až	až	k6eAd1	až
ve	v	k7c6	v
22	[number]	k4	22
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Moody	Moody	k6eAd1	Moody
byl	být	k5eAaImAgMnS	být
Lightollerem	Lightoller	k1gMnSc7	Lightoller
požádán	požádat	k5eAaPmNgMnS	požádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
podle	podle	k7c2	podle
posledních	poslední	k2eAgMnPc2d1	poslední
údajů	údaj	k1gInPc2	údaj
provedl	provést	k5eAaPmAgInS	provést
přepočet	přepočet	k1gInSc1	přepočet
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
polohy	poloha	k1gFnSc2	poloha
lodě	loď	k1gFnSc2	loď
a	a	k8xC	a
oblasti	oblast	k1gFnSc2	oblast
ledovců	ledovec	k1gInPc2	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výsledku	výsledek	k1gInSc2	výsledek
mělo	mít	k5eAaImAgNnS	mít
k	k	k7c3	k
setkání	setkání	k1gNnSc3	setkání
dojít	dojít	k5eAaPmF	dojít
až	až	k6eAd1	až
před	před	k7c7	před
jedenáctou	jedenáctý	k4xOgFnSc7	jedenáctý
hodinou	hodina	k1gFnSc7	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
21	[number]	k4	21
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
hovořil	hovořit	k5eAaImAgMnS	hovořit
kapitán	kapitán	k1gMnSc1	kapitán
Smith	Smith	k1gMnSc1	Smith
s	s	k7c7	s
2	[number]	k4	2
<g/>
.	.	kIx.	.
důstojníkem	důstojník	k1gMnSc7	důstojník
Lightollerem	Lightoller	k1gMnSc7	Lightoller
o	o	k7c6	o
vývoji	vývoj	k1gInSc6	vývoj
počasí	počasí	k1gNnSc1	počasí
<g/>
,	,	kIx,	,
snížení	snížení	k1gNnSc1	snížení
teploty	teplota	k1gFnSc2	teplota
nad	nad	k7c7	nad
oceánem	oceán	k1gInSc7	oceán
a	a	k8xC	a
o	o	k7c6	o
pravděpodobném	pravděpodobný	k2eAgNnSc6d1	pravděpodobné
setkání	setkání	k1gNnSc6	setkání
s	s	k7c7	s
plovoucím	plovoucí	k2eAgInSc7d1	plovoucí
ledem	led	k1gInSc7	led
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nevál	vát	k5eNaImAgInS	vát
vítr	vítr	k1gInSc1	vítr
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
hladina	hladina	k1gFnSc1	hladina
oceánu	oceán	k1gInSc2	oceán
klidná	klidný	k2eAgFnSc1d1	klidná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
vln	vlna	k1gFnPc2	vlna
na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
tříštily	tříštit	k5eAaImAgFnP	tříštit
o	o	k7c4	o
ledovec	ledovec	k1gInSc4	ledovec
a	a	k8xC	a
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
pěna	pěna	k1gFnSc1	pěna
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
lépe	dobře	k6eAd2	dobře
pozorovatelná	pozorovatelný	k2eAgFnSc1d1	pozorovatelná
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
tmavý	tmavý	k2eAgInSc1d1	tmavý
ledovec	ledovec	k1gInSc1	ledovec
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
21	[number]	k4	21
<g/>
:	:	kIx,	:
<g/>
40	[number]	k4	40
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
další	další	k2eAgFnSc1d1	další
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
výskytu	výskyt	k1gInSc6	výskyt
ledu	led	k1gInSc2	led
z	z	k7c2	z
parníku	parník	k1gInSc2	parník
Mesaba	Mesaba	k1gFnSc1	Mesaba
<g/>
:	:	kIx,	:
led	led	k1gInSc1	led
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
až	až	k6eAd1	až
,	,	kIx,	,
velké	velký	k2eAgInPc4d1	velký
kusy	kus	k1gInPc4	kus
ledu	led	k1gInSc2	led
a	a	k8xC	a
značný	značný	k2eAgInSc4d1	značný
počet	počet	k1gInSc4	počet
velkých	velký	k2eAgFnPc2d1	velká
ledových	ledový	k2eAgFnPc2d1	ledová
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
ledová	ledový	k2eAgNnPc4d1	ledové
pole	pole	k1gNnPc4	pole
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
22	[number]	k4	22
<g/>
:	:	kIx,	:
<g/>
15	[number]	k4	15
se	se	k3xPyFc4	se
parník	parník	k1gInSc1	parník
Californian	Californiana	k1gFnPc2	Californiana
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
ledového	ledový	k2eAgNnSc2d1	ledové
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
22	[number]	k4	22
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
přišla	přijít	k5eAaPmAgFnS	přijít
zpráva	zpráva	k1gFnSc1	zpráva
z	z	k7c2	z
parníku	parník	k1gInSc2	parník
Rappahannock	Rappahannocka	k1gFnPc2	Rappahannocka
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
propluli	proplout	k5eAaPmAgMnP	proplout
jsme	být	k5eAaImIp1nP	být
ledovým	ledový	k2eAgNnSc7d1	ledové
polem	pole	k1gNnSc7	pole
a	a	k8xC	a
mezi	mezi	k7c7	mezi
několika	několik	k4yIc7	několik
ledovými	ledový	k2eAgFnPc7d1	ledová
horami	hora	k1gFnPc7	hora
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
23	[number]	k4	23
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
zpozoroval	zpozorovat	k5eAaPmAgMnS	zpozorovat
kapitán	kapitán	k1gMnSc1	kapitán
parníku	parník	k1gInSc2	parník
Californian	Californian	k1gMnSc1	Californian
na	na	k7c6	na
obzoru	obzor	k1gInSc6	obzor
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
3	[number]	k4	3
<g/>
.	.	kIx.	.
důstojník	důstojník	k1gMnSc1	důstojník
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
světla	světlo	k1gNnPc4	světlo
osobního	osobní	k2eAgInSc2d1	osobní
parníku	parník	k1gInSc2	parník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
23	[number]	k4	23
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
se	se	k3xPyFc4	se
radista	radista	k1gMnSc1	radista
Cyril	Cyril	k1gMnSc1	Cyril
Evans	Evans	k1gInSc4	Evans
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
parníku	parník	k1gInSc2	parník
Californian	Californian	k1gMnSc1	Californian
snažil	snažit	k5eAaImAgMnS	snažit
varovat	varovat	k5eAaImF	varovat
Titanic	Titanic	k1gInSc4	Titanic
před	před	k7c7	před
ledovými	ledový	k2eAgNnPc7d1	ledové
poli	pole	k1gNnPc7	pole
<g/>
.	.	kIx.	.
</s>
<s>
Radista	radista	k1gMnSc1	radista
Titanicu	Titanicus	k1gInSc2	Titanicus
jej	on	k3xPp3gNnSc4	on
však	však	k9	však
přerušil	přerušit	k5eAaPmAgMnS	přerušit
a	a	k8xC	a
v	v	k7c6	v
23	[number]	k4	23
<g/>
:	:	kIx,	:
<g/>
35	[number]	k4	35
Evans	Evans	k1gInSc1	Evans
ukončil	ukončit	k5eAaPmAgInS	ukončit
vysílání	vysílání	k1gNnSc4	vysílání
a	a	k8xC	a
unaven	unaven	k2eAgMnSc1d1	unaven
šel	jít	k5eAaImAgMnS	jít
spát	spát	k5eAaImF	spát
<g/>
.	.	kIx.	.
</s>
<s>
Titanic	Titanic	k1gInSc1	Titanic
pluje	plout	k5eAaImIp3nS	plout
stále	stále	k6eAd1	stále
rychlostí	rychlost	k1gFnSc7	rychlost
kolem	kolem	k7c2	kolem
21	[number]	k4	21
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
uzlů	uzel	k1gInPc2	uzel
(	(	kIx(	(
<g/>
39	[number]	k4	39
<g/>
-	-	kIx~	-
<g/>
41	[number]	k4	41
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
ledovému	ledový	k2eAgNnSc3d1	ledové
poli	pole	k1gNnSc3	pole
<g/>
.	.	kIx.	.
</s>
<s>
Pár	pár	k4xCyI	pár
minut	minuta	k1gFnPc2	minuta
po	po	k7c4	po
varování	varování	k1gNnSc4	varování
od	od	k7c2	od
Californianu	Californian	k1gInSc2	Californian
hlídka	hlídka	k1gFnSc1	hlídka
v	v	k7c6	v
předním	přední	k2eAgNnSc6d1	přední
vraním	vraní	k2eAgNnSc6d1	vraní
hnízdě	hnízdo	k1gNnSc6	hnízdo
(	(	kIx(	(
<g/>
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
Reginald	Reginald	k1gMnSc1	Reginald
Robinson	Robinson	k1gMnSc1	Robinson
Lee	Lea	k1gFnSc6	Lea
a	a	k8xC	a
Frederick	Frederick	k1gMnSc1	Frederick
Fleet	Fleet	k1gMnSc1	Fleet
<g/>
)	)	kIx)	)
zpozorovala	zpozorovat	k5eAaPmAgFnS	zpozorovat
mlžný	mlžný	k2eAgInSc4d1	mlžný
opar	opar	k1gInSc4	opar
vpředu	vpředu	k6eAd1	vpředu
<g/>
,	,	kIx,	,
před	před	k7c7	před
přídí	příď	k1gFnSc7	příď
(	(	kIx(	(
<g/>
známka	známka	k1gFnSc1	známka
blízkosti	blízkost	k1gFnSc2	blízkost
ledovce	ledovec	k1gInSc2	ledovec
<g/>
,	,	kIx,	,
mlha	mlha	k1gFnSc1	mlha
navíc	navíc	k6eAd1	navíc
snižuje	snižovat	k5eAaImIp3nS	snižovat
dohlednost	dohlednost	k1gFnSc1	dohlednost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fleet	Fleet	k1gInSc1	Fleet
posléze	posléze	k6eAd1	posléze
rozeznal	rozeznat	k5eAaPmAgInS	rozeznat
ledovou	ledový	k2eAgFnSc4d1	ledová
masu	masa	k1gFnSc4	masa
před	před	k7c7	před
nimi	on	k3xPp3gInPc7	on
a	a	k8xC	a
v	v	k7c6	v
23	[number]	k4	23
<g/>
:	:	kIx,	:
<g/>
39	[number]	k4	39
třemi	tři	k4xCgInPc7	tři
údery	úder	k1gInPc4	úder
zvonce	zvonec	k1gInSc2	zvonec
signalizoval	signalizovat	k5eAaImAgMnS	signalizovat
"	"	kIx"	"
<g/>
Předmět	předmět	k1gInSc1	předmět
před	před	k7c7	před
lodí	loď	k1gFnSc7	loď
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
pomocí	pomocí	k7c2	pomocí
telefonu	telefon	k1gInSc2	telefon
předal	předat	k5eAaPmAgInS	předat
6	[number]	k4	6
<g/>
.	.	kIx.	.
důstojníkovi	důstojník	k1gMnSc3	důstojník
J.	J.	kA	J.
P.	P.	kA	P.
Moodymu	Moodym	k1gInSc6	Moodym
na	na	k7c4	na
můstek	můstek	k1gInSc4	můstek
hlášení	hlášení	k1gNnSc2	hlášení
"	"	kIx"	"
<g/>
Led	led	k1gInSc1	led
přímo	přímo	k6eAd1	přímo
před	před	k7c7	před
námi	my	k3xPp1nPc7	my
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zvuk	zvuk	k1gInSc1	zvuk
zvonce	zvonec	k1gInSc2	zvonec
vyburcoval	vyburcovat	k5eAaPmAgInS	vyburcovat
1	[number]	k4	1
<g/>
.	.	kIx.	.
důstojníka	důstojník	k1gMnSc4	důstojník
Murdocha	Murdoch	k1gMnSc4	Murdoch
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
měl	mít	k5eAaImAgInS	mít
hlídku	hlídka	k1gFnSc4	hlídka
na	na	k7c6	na
můstku	můstek	k1gInSc6	můstek
<g/>
.	.	kIx.	.
</s>
<s>
Murdoch	Murdoch	k1gMnSc1	Murdoch
po	po	k7c6	po
vyslechnutí	vyslechnutí	k1gNnSc6	vyslechnutí
zprávy	zpráva	k1gFnSc2	zpráva
okamžitě	okamžitě	k6eAd1	okamžitě
přikázal	přikázat	k5eAaPmAgInS	přikázat
strojovně	strojovna	k1gFnSc3	strojovna
"	"	kIx"	"
<g/>
STOP	stop	k1gInSc1	stop
<g/>
"	"	kIx"	"
a	a	k8xC	a
kormidelníkovi	kormidelník	k1gMnSc3	kormidelník
Robertu	Robert	k1gMnSc3	Robert
Hichensovi	Hichens	k1gMnSc3	Hichens
velel	velet	k5eAaImAgMnS	velet
"	"	kIx"	"
<g/>
Docela	docela	k6eAd1	docela
vpravo	vpravo	k6eAd1	vpravo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
v	v	k7c6	v
lodní	lodní	k2eAgFnSc6d1	lodní
terminologii	terminologie	k1gFnSc6	terminologie
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
znamená	znamenat	k5eAaImIp3nS	znamenat
záď	záď	k1gFnSc1	záď
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
příď	příď	k1gFnSc1	příď
doleva	doleva	k6eAd1	doleva
<g/>
.	.	kIx.	.
</s>
<s>
Sled	sled	k1gInSc1	sled
vydaných	vydaný	k2eAgInPc2d1	vydaný
povelů	povel	k1gInPc2	povel
však	však	k9	však
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
Titanic	Titanic	k1gInSc4	Titanic
tragický	tragický	k2eAgInSc4d1	tragický
<g/>
.	.	kIx.	.
</s>
<s>
Moody	Moody	k6eAd1	Moody
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
provedení	provedení	k1gNnSc4	provedení
Murdochova	Murdochův	k2eAgInSc2d1	Murdochův
rozkazu	rozkaz	k1gInSc2	rozkaz
"	"	kIx"	"
<g/>
Docela	docela	k6eAd1	docela
vpravo	vpravo	k6eAd1	vpravo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
Murdoch	Murdoch	k1gInSc1	Murdoch
vydal	vydat	k5eAaPmAgInS	vydat
povel	povel	k1gInSc1	povel
strojovně	strojovna	k1gFnSc3	strojovna
"	"	kIx"	"
<g/>
Plný	plný	k2eAgInSc4d1	plný
zpětný	zpětný	k2eAgInSc4d1	zpětný
chod	chod	k1gInSc4	chod
<g/>
"	"	kIx"	"
a	a	k8xC	a
ke	k	k7c3	k
kormidlu	kormidlo	k1gNnSc3	kormidlo
"	"	kIx"	"
<g/>
Docela	docela	k6eAd1	docela
vlevo	vlevo	k6eAd1	vlevo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
před	před	k7c7	před
vyšetřovateli	vyšetřovatel	k1gMnPc7	vyšetřovatel
jak	jak	k8xC	jak
americké	americký	k2eAgInPc4d1	americký
<g/>
,	,	kIx,	,
tak	tak	k9	tak
britské	britský	k2eAgNnSc4d1	Britské
(	(	kIx(	(
<g/>
proběhl	proběhnout	k5eAaPmAgMnS	proběhnout
zde	zde	k6eAd1	zde
spor	spor	k1gInSc4	spor
o	o	k7c4	o
kompetence	kompetence	k1gFnPc4	kompetence
<g/>
)	)	kIx)	)
vyšetřovací	vyšetřovací	k2eAgFnSc2d1	vyšetřovací
komise	komise	k1gFnSc2	komise
byla	být	k5eAaImAgFnS	být
poněkud	poněkud	k6eAd1	poněkud
zveličena	zveličen	k2eAgFnSc1d1	zveličena
otázka	otázka	k1gFnSc1	otázka
nepřítomnosti	nepřítomnost	k1gFnSc2	nepřítomnost
dalekohledů	dalekohled	k1gInPc2	dalekohled
ve	v	k7c6	v
skříňce	skříňka	k1gFnSc6	skříňka
strážního	strážní	k2eAgInSc2d1	strážní
koše	koš	k1gInSc2	koš
-	-	kIx~	-
byly	být	k5eAaImAgInP	být
zde	zde	k6eAd1	zde
nejpozději	pozdě	k6eAd3	pozdě
při	při	k7c6	při
plavebních	plavební	k2eAgFnPc6d1	plavební
zkouškách	zkouška	k1gFnPc6	zkouška
Titanicu	Titanicus	k1gInSc2	Titanicus
2	[number]	k4	2
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
-	-	kIx~	-
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
však	však	k9	však
z	z	k7c2	z
dnes	dnes	k6eAd1	dnes
neznámých	známý	k2eNgInPc2d1	neznámý
důvodů	důvod	k1gInPc2	důvod
ztratily	ztratit	k5eAaPmAgFnP	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Dalekohled	dalekohled	k1gInSc1	dalekohled
však	však	k9	však
slouží	sloužit	k5eAaImIp3nS	sloužit
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
obecnému	obecný	k2eAgNnSc3d1	obecné
přehlédnutí	přehlédnutí	k1gNnSc3	přehlédnutí
místa	místo	k1gNnSc2	místo
a	a	k8xC	a
detail	detail	k1gInSc1	detail
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
pozorován	pozorován	k2eAgInSc1d1	pozorován
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
<g/>
.	.	kIx.	.
</s>
<s>
Ledovec	ledovec	k1gInSc1	ledovec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
v	v	k7c6	v
plavební	plavební	k2eAgFnSc6d1	plavební
dráze	dráha	k1gFnSc6	dráha
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
před	před	k7c7	před
několika	několik	k4yIc7	několik
málo	málo	k6eAd1	málo
hodinami	hodina	k1gFnPc7	hodina
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
převrátil	převrátit	k5eAaPmAgInS	převrátit
a	a	k8xC	a
stékající	stékající	k2eAgFnSc1d1	stékající
voda	voda	k1gFnSc1	voda
jej	on	k3xPp3gMnSc4	on
učinila	učinit	k5eAaPmAgFnS	učinit
tmavým	tmavý	k2eAgNnSc7d1	tmavé
<g/>
,	,	kIx,	,
a	a	k8xC	a
pro	pro	k7c4	pro
hlídku	hlídka	k1gFnSc4	hlídka
neviditelným	viditelný	k2eNgNnSc7d1	neviditelné
<g/>
.	.	kIx.	.
</s>
<s>
Frederic	Frederic	k1gMnSc1	Frederic
Fleet	Fleet	k1gMnSc1	Fleet
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
neobyčejně	obyčejně	k6eNd1	obyčejně
jasné	jasný	k2eAgFnSc6d1	jasná
noci	noc	k1gFnSc6	noc
zpozoroval	zpozorovat	k5eAaPmAgMnS	zpozorovat
ze	z	k7c2	z
strážního	strážní	k2eAgInSc2d1	strážní
koše	koš	k1gInSc2	koš
jako	jako	k8xS	jako
první	první	k4xOgInSc4	první
<g/>
,	,	kIx,	,
ho	on	k3xPp3gMnSc4	on
rozeznal	rozeznat	k5eAaPmAgInS	rozeznat
jen	jen	k9	jen
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
silueta	silueta	k1gFnSc1	silueta
zakrývala	zakrývat	k5eAaImAgFnS	zakrývat
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
reakce	reakce	k1gFnSc1	reakce
byla	být	k5eAaImAgFnS	být
rychlá	rychlý	k2eAgFnSc1d1	rychlá
-	-	kIx~	-
hlášení	hlášení	k1gNnPc2	hlášení
telefonem	telefon	k1gInSc7	telefon
a	a	k8xC	a
bezprostředně	bezprostředně	k6eAd1	bezprostředně
následující	následující	k2eAgInPc4d1	následující
tři	tři	k4xCgInPc4	tři
údery	úder	k1gInPc4	úder
na	na	k7c4	na
zvon	zvon	k1gInSc4	zvon
(	(	kIx(	(
<g/>
předmět	předmět	k1gInSc4	předmět
v	v	k7c6	v
kurzu	kurz	k1gInSc6	kurz
lodi	loď	k1gFnSc2	loď
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlášení	hlášení	k1gNnSc4	hlášení
převzal	převzít	k5eAaPmAgInS	převzít
6	[number]	k4	6
<g/>
.	.	kIx.	.
důstojník	důstojník	k1gMnSc1	důstojník
Moody	Mooda	k1gFnSc2	Mooda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
hlášení	hlášení	k1gNnSc2	hlášení
z	z	k7c2	z
vraního	vraní	k2eAgNnSc2d1	vraní
hnízda	hnízdo	k1gNnSc2	hnízdo
do	do	k7c2	do
kolize	kolize	k1gFnSc2	kolize
s	s	k7c7	s
ledovcem	ledovec	k1gInSc7	ledovec
uplynulo	uplynout	k5eAaPmAgNnS	uplynout
37	[number]	k4	37
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Titanic	Titanic	k1gInSc1	Titanic
měl	mít	k5eAaImAgInS	mít
velkou	velký	k2eAgFnSc4d1	velká
hmotnost	hmotnost	k1gFnSc4	hmotnost
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
velkou	velký	k2eAgFnSc4d1	velká
setrvačnost	setrvačnost	k1gFnSc4	setrvačnost
<g/>
,	,	kIx,	,
malé	malý	k2eAgNnSc1d1	malé
kormidlo	kormidlo	k1gNnSc1	kormidlo
<g/>
,	,	kIx,	,
ledovec	ledovec	k1gInSc1	ledovec
byl	být	k5eAaImAgInS	být
zpozorován	zpozorovat	k5eAaPmNgInS	zpozorovat
pozdě	pozdě	k6eAd1	pozdě
a	a	k8xC	a
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
názorů	názor	k1gInPc2	názor
1	[number]	k4	1
<g/>
.	.	kIx.	.
důstojník	důstojník	k1gMnSc1	důstojník
Murdoch	Murdoch	k1gMnSc1	Murdoch
po	po	k7c4	po
přijetí	přijetí	k1gNnSc4	přijetí
zprávy	zpráva	k1gFnSc2	zpráva
z	z	k7c2	z
koše	koš	k1gInSc2	koš
od	od	k7c2	od
Moodyho	Moody	k1gMnSc2	Moody
nevydal	vydat	k5eNaPmAgMnS	vydat
úplně	úplně	k6eAd1	úplně
správné	správný	k2eAgInPc4d1	správný
povely	povel	k1gInPc4	povel
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
Murdoch	Murdoch	k1gMnSc1	Murdoch
vydal	vydat	k5eAaPmAgMnS	vydat
povely	povel	k1gInPc4	povel
zcela	zcela	k6eAd1	zcela
logické	logický	k2eAgNnSc4d1	logické
-	-	kIx~	-
vydal	vydat	k5eAaPmAgMnS	vydat
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zabránil	zabránit	k5eAaPmAgInS	zabránit
čelní	čelní	k2eAgFnSc3d1	čelní
srážce	srážka	k1gFnSc3	srážka
s	s	k7c7	s
neznámým	známý	k2eNgInSc7d1	neznámý
předmětem	předmět	k1gInSc7	předmět
-	-	kIx~	-
zcela	zcela	k6eAd1	zcela
dobře	dobře	k6eAd1	dobře
a	a	k8xC	a
chladnokrevně	chladnokrevně	k6eAd1	chladnokrevně
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
výsledné	výsledný	k2eAgNnSc1d1	výsledné
roztržení	roztržení	k1gNnSc1	roztržení
boku	bok	k1gInSc2	bok
lodi	loď	k1gFnSc2	loď
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
Titanik	Titanik	k1gInSc4	Titanik
daleko	daleko	k6eAd1	daleko
více	hodně	k6eAd2	hodně
devastující	devastující	k2eAgInSc4d1	devastující
než	než	k8xS	než
čelní	čelní	k2eAgInSc4d1	čelní
náraz	náraz	k1gInSc4	náraz
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
otázkou	otázka	k1gFnSc7	otázka
konstrukce	konstrukce	k1gFnSc2	konstrukce
lodi	loď	k1gFnSc2	loď
a	a	k8xC	a
nikoli	nikoli	k9	nikoli
chybných	chybný	k2eAgInPc2d1	chybný
povelů	povel	k1gInPc2	povel
důstojníka	důstojník	k1gMnSc2	důstojník
který	který	k3yRgInSc1	který
udělal	udělat	k5eAaPmAgMnS	udělat
vše	všechen	k3xTgNnSc4	všechen
pro	pro	k7c4	pro
záchranu	záchrana	k1gFnSc4	záchrana
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Murdoch	Murdoch	k1gMnSc1	Murdoch
byl	být	k5eAaImAgMnS	být
také	také	k9	také
nejčastěji	často	k6eAd3	často
obviňován	obviňován	k2eAgMnSc1d1	obviňován
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
katastrofy	katastrofa	k1gFnSc2	katastrofa
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
neprávem	neprávo	k1gNnSc7	neprávo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
reakce	reakce	k1gFnSc1	reakce
vyhnutí	vyhnutí	k1gNnSc2	vyhnutí
se	se	k3xPyFc4	se
překážce	překážka	k1gFnSc3	překážka
byla	být	k5eAaImAgFnS	být
přímo	přímo	k6eAd1	přímo
učebnicová	učebnicový	k2eAgFnSc1d1	učebnicová
-	-	kIx~	-
nedostatek	nedostatek	k1gInSc1	nedostatek
času	čas	k1gInSc2	čas
na	na	k7c4	na
takový	takový	k3xDgInSc4	takový
manévr	manévr	k1gInSc4	manévr
nebyl	být	k5eNaImAgMnS	být
jeho	jeho	k3xOp3gFnSc7	jeho
chybou	chyba	k1gFnSc7	chyba
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Titanic	Titanic	k1gInSc1	Titanic
velmi	velmi	k6eAd1	velmi
pomalu	pomalu	k6eAd1	pomalu
začal	začít	k5eAaPmAgInS	začít
odklánět	odklánět	k5eAaImF	odklánět
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
původního	původní	k2eAgInSc2d1	původní
kurzu	kurz	k1gInSc2	kurz
<g/>
,	,	kIx,	,
přídí	příď	k1gFnPc2	příď
doleva	doleva	k6eAd1	doleva
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
kurzu	kurz	k1gInSc2	kurz
nestačila	stačit	k5eNaBmAgFnS	stačit
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
Titanic	Titanic	k1gInSc1	Titanic
ledovci	ledovec	k1gInPc7	ledovec
vyhnul	vyhnout	k5eAaPmAgMnS	vyhnout
<g/>
.	.	kIx.	.
</s>
<s>
Titanic	Titanic	k1gInSc1	Titanic
narazil	narazit	k5eAaPmAgInS	narazit
do	do	k7c2	do
ledovce	ledovec	k1gInSc2	ledovec
pravým	pravý	k2eAgInSc7d1	pravý
bokem	bok	k1gInSc7	bok
hned	hned	k6eAd1	hned
za	za	k7c2	za
přídí	příď	k1gFnPc2	příď
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nárazu	náraz	k1gInSc3	náraz
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
důstojník	důstojník	k1gMnSc1	důstojník
vydal	vydat	k5eAaPmAgMnS	vydat
povel	povel	k1gInSc4	povel
ke	k	k7c3	k
zpětnému	zpětný	k2eAgInSc3d1	zpětný
chodu	chod	k1gInSc3	chod
strojů	stroj	k1gInPc2	stroj
a	a	k8xC	a
vzápětí	vzápětí	k6eAd1	vzápětí
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
vodotěsné	vodotěsný	k2eAgFnPc4d1	vodotěsná
přepážky	přepážka	k1gFnPc4	přepážka
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
posádka	posádka	k1gFnSc1	posádka
a	a	k8xC	a
cestující	cestující	k1gMnPc1	cestující
nepocítili	pocítit	k5eNaPmAgMnP	pocítit
přímý	přímý	k2eAgInSc4d1	přímý
náraz	náraz	k1gInSc4	náraz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
chvění	chvění	k1gNnSc4	chvění
lodi	loď	k1gFnSc2	loď
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
10	[number]	k4	10
sekund	sekunda	k1gFnPc2	sekunda
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
trhlina	trhlina	k1gFnSc1	trhlina
pod	pod	k7c7	pod
čarou	čára	k1gFnSc7	čára
ponoru	ponor	k1gInSc2	ponor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nad	nad	k7c7	nad
úrovní	úroveň	k1gFnSc7	úroveň
dvojitého	dvojitý	k2eAgNnSc2d1	dvojité
dna	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
90	[number]	k4	90
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zasahovala	zasahovat	k5eAaImAgFnS	zasahovat
až	až	k9	až
do	do	k7c2	do
vodotěsné	vodotěsný	k2eAgFnSc2d1	vodotěsná
komory	komora	k1gFnSc2	komora
č.	č.	k?	č.
6	[number]	k4	6
<g/>
,	,	kIx,	,
zásobníku	zásobník	k1gInSc2	zásobník
uhlí	uhlí	k1gNnSc2	uhlí
pro	pro	k7c4	pro
pátou	pátý	k4xOgFnSc4	pátý
kotelnu	kotelna	k1gFnSc4	kotelna
<g/>
.	.	kIx.	.
</s>
<s>
Trhlina	trhlina	k1gFnSc1	trhlina
se	se	k3xPyFc4	se
táhla	táhnout	k5eAaImAgFnS	táhnout
přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
3	[number]	k4	3
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
kýlem	kýl	k1gInSc7	kýl
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
na	na	k7c6	na
horních	horní	k2eAgFnPc6d1	horní
palubách	paluba	k1gFnPc6	paluba
nevypadal	vypadat	k5eNaImAgInS	vypadat
náraz	náraz	k1gInSc1	náraz
nijak	nijak	k6eAd1	nijak
dramaticky	dramaticky	k6eAd1	dramaticky
<g/>
,	,	kIx,	,
v	v	k7c6	v
podpalubí	podpalubí	k1gNnSc6	podpalubí
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
D	D	kA	D
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
ubytovací	ubytovací	k2eAgFnPc4d1	ubytovací
prostory	prostora	k1gFnPc4	prostora
topičů	topič	k1gInPc2	topič
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
náraz	náraz	k1gInSc1	náraz
cítit	cítit	k5eAaImF	cítit
velmi	velmi	k6eAd1	velmi
silně	silně	k6eAd1	silně
<g/>
.	.	kIx.	.
</s>
<s>
Topič	topič	k1gMnSc1	topič
John	John	k1gMnSc1	John
Thompson	Thompson	k1gMnSc1	Thompson
později	pozdě	k6eAd2	pozdě
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
nárazem	náraz	k1gInSc7	náraz
někteří	některý	k3yIgMnPc1	některý
vypadli	vypadnout	k5eAaPmAgMnP	vypadnout
z	z	k7c2	z
lůžek	lůžko	k1gNnPc2	lůžko
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
topič	topič	k1gInSc1	topič
William	William	k1gInSc1	William
Small	Small	k1gInSc1	Small
všem	všecek	k3xTgMnPc3	všecek
přikázal	přikázat	k5eAaPmAgInS	přikázat
"	"	kIx"	"
<g/>
dolů	dolů	k6eAd1	dolů
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
do	do	k7c2	do
kotelny	kotelna	k1gFnSc2	kotelna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
toto	tento	k3xDgNnSc1	tento
již	již	k6eAd1	již
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
prostory	prostor	k1gInPc1	prostor
pod	pod	k7c7	pod
průlezy	průlez	k1gInPc7	průlez
již	již	k6eAd1	již
zaplavovala	zaplavovat	k5eAaImAgFnS	zaplavovat
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
nárazu	náraz	k1gInSc6	náraz
přišel	přijít	k5eAaPmAgMnS	přijít
kapitán	kapitán	k1gMnSc1	kapitán
Smith	Smith	k1gMnSc1	Smith
na	na	k7c4	na
můstek	můstek	k1gInSc4	můstek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
předtím	předtím	k6eAd1	předtím
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
1	[number]	k4	1
<g/>
.	.	kIx.	.
důstojník	důstojník	k1gMnSc1	důstojník
vodotěsné	vodotěsný	k2eAgFnPc4d1	vodotěsná
dveře	dveře	k1gFnPc4	dveře
v	v	k7c6	v
přepážkách	přepážka	k1gFnPc6	přepážka
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
komor	komora	k1gFnPc2	komora
<g/>
.	.	kIx.	.
</s>
<s>
Komory	komora	k1gFnPc1	komora
však	však	k9	však
byly	být	k5eAaImAgFnP	být
vodotěsné	vodotěsný	k2eAgFnPc1d1	vodotěsná
pouze	pouze	k6eAd1	pouze
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
shora	shora	k6eAd1	shora
<g/>
,	,	kIx,	,
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
palubám	paluba	k1gFnPc3	paluba
<g/>
,	,	kIx,	,
tomu	ten	k3xDgNnSc3	ten
tak	tak	k9	tak
nebylo	být	k5eNaImAgNnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Plnící	plnící	k2eAgMnSc1d1	plnící
se	se	k3xPyFc4	se
přední	přední	k2eAgFnPc1d1	přední
komory	komora	k1gFnPc1	komora
zatěžovaly	zatěžovat	k5eAaImAgFnP	zatěžovat
trup	trup	k1gInSc4	trup
lodi	loď	k1gFnSc2	loď
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
ten	ten	k3xDgMnSc1	ten
začal	začít	k5eAaPmAgMnS	začít
klesat	klesat	k5eAaImF	klesat
přídí	příď	k1gFnSc7	příď
pod	pod	k7c4	pod
hladinu	hladina	k1gFnSc4	hladina
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
začala	začít	k5eAaPmAgFnS	začít
přelévat	přelévat	k5eAaImF	přelévat
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
komor	komora	k1gFnPc2	komora
přes	přes	k7c4	přes
horní	horní	k2eAgFnSc4d1	horní
hranu	hrana	k1gFnSc4	hrana
<g/>
.	.	kIx.	.
</s>
<s>
Prohlídka	prohlídka	k1gFnSc1	prohlídka
poškozené	poškozený	k2eAgFnSc2d1	poškozená
přídě	příď	k1gFnSc2	příď
přinesla	přinést	k5eAaPmAgNnP	přinést
zjištění	zjištění	k1gNnPc1	zjištění
<g/>
,	,	kIx,	,
že	že	k8xS	že
Titanic	Titanic	k1gInSc1	Titanic
nelze	lze	k6eNd1	lze
udržet	udržet	k5eAaPmF	udržet
na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
ani	ani	k8xC	ani
při	při	k7c6	při
odčerpávání	odčerpávání	k1gNnSc6	odčerpávání
vody	voda	k1gFnSc2	voda
balastními	balastní	k2eAgNnPc7d1	balastní
čerpadly	čerpadlo	k1gNnPc7	čerpadlo
<g/>
.	.	kIx.	.
</s>
<s>
Šéfkonstruktér	šéfkonstruktér	k1gMnSc1	šéfkonstruktér
Andrews	Andrewsa	k1gFnPc2	Andrewsa
odhadoval	odhadovat	k5eAaImAgMnS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
loď	loď	k1gFnSc1	loď
udrží	udržet	k5eAaPmIp3nS	udržet
na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
přibližně	přibližně	k6eAd1	přibližně
jednu	jeden	k4xCgFnSc4	jeden
až	až	k8xS	až
jednu	jeden	k4xCgFnSc4	jeden
a	a	k8xC	a
půl	půl	k1xP	půl
hodiny	hodina	k1gFnSc2	hodina
a	a	k8xC	a
že	že	k8xS	že
čerpadla	čerpadlo	k1gNnPc1	čerpadlo
mohou	moct	k5eAaImIp3nP	moct
přidat	přidat	k5eAaPmF	přidat
jen	jen	k9	jen
pár	pár	k4xCyI	pár
minut	minuta	k1gFnPc2	minuta
před	před	k7c7	před
potopením	potopení	k1gNnSc7	potopení
<g/>
.	.	kIx.	.
</s>
<s>
Výkon	výkon	k1gInSc1	výkon
čerpadel	čerpadlo	k1gNnPc2	čerpadlo
sice	sice	k8xC	sice
dovoloval	dovolovat	k5eAaImAgInS	dovolovat
přečerpání	přečerpání	k1gNnSc4	přečerpání
1	[number]	k4	1
700	[number]	k4	700
tun	tuna	k1gFnPc2	tuna
vody	voda	k1gFnSc2	voda
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
po	po	k7c6	po
nárazu	náraz	k1gInSc6	náraz
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
úroveň	úroveň	k1gFnSc1	úroveň
zatopení	zatopení	k1gNnSc2	zatopení
přídě	příď	k1gFnSc2	příď
až	až	k9	až
14	[number]	k4	14
metrů	metr	k1gInPc2	metr
v	v	k7c6	v
5	[number]	k4	5
předních	přední	k2eAgInPc6d1	přední
oddílech	oddíl	k1gInPc6	oddíl
a	a	k8xC	a
hladina	hladina	k1gFnSc1	hladina
rychle	rychle	k6eAd1	rychle
stoupala	stoupat	k5eAaImAgFnS	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
množství	množství	k1gNnSc1	množství
vody	voda	k1gFnSc2	voda
již	již	k9	již
nemohla	moct	k5eNaImAgFnS	moct
čerpadla	čerpadlo	k1gNnSc2	čerpadlo
pojmout	pojmout	k5eAaPmF	pojmout
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
půlnoci	půlnoc	k1gFnSc2	půlnoc
<g/>
,	,	kIx,	,
cca	cca	kA	cca
20	[number]	k4	20
minut	minuta	k1gFnPc2	minuta
po	po	k7c6	po
srážce	srážka	k1gFnSc6	srážka
již	již	k6eAd1	již
pronikla	proniknout	k5eAaPmAgFnS	proniknout
voda	voda	k1gFnSc1	voda
do	do	k7c2	do
obytných	obytný	k2eAgFnPc2d1	obytná
prostor	prostora	k1gFnPc2	prostora
3	[number]	k4	3
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Kapitán	kapitán	k1gMnSc1	kapitán
s	s	k7c7	s
Andrewsem	Andrews	k1gMnSc7	Andrews
se	se	k3xPyFc4	se
shodli	shodnout	k5eAaBmAgMnP	shodnout
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
panika	panika	k1gFnSc1	panika
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
počet	počet	k1gInSc1	počet
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
záchranných	záchranný	k2eAgInPc6d1	záchranný
člunech	člun	k1gInPc6	člun
nebyl	být	k5eNaImAgInS	být
dostačující	dostačující	k2eAgInSc1d1	dostačující
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
cestující	cestující	k1gMnPc1	cestující
i	i	k8xC	i
posádka	posádka	k1gFnSc1	posádka
dostali	dostat	k5eAaPmAgMnP	dostat
záchranné	záchranný	k2eAgFnPc4d1	záchranná
vesty	vesta	k1gFnPc4	vesta
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
chladnému	chladný	k2eAgNnSc3d1	chladné
počasí	počasí	k1gNnSc3	počasí
a	a	k8xC	a
ledové	ledový	k2eAgFnSc3d1	ledová
vodě	voda	k1gFnSc3	voda
severního	severní	k2eAgInSc2d1	severní
Atlantiku	Atlantik	k1gInSc2	Atlantik
mohli	moct	k5eAaImAgMnP	moct
cestující	cestující	k1gMnPc1	cestující
přežít	přežít	k5eAaPmF	přežít
pouze	pouze	k6eAd1	pouze
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
odolnější	odolný	k2eAgInSc1d2	odolnější
jedinci	jedinec	k1gMnPc7	jedinec
i	i	k8xC	i
minut	minuta	k1gFnPc2	minuta
20	[number]	k4	20
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
minut	minuta	k1gFnPc2	minuta
po	po	k7c6	po
půlnoci	půlnoc	k1gFnSc6	půlnoc
vydává	vydávat	k5eAaPmIp3nS	vydávat
kapitán	kapitán	k1gMnSc1	kapitán
rozkaz	rozkaz	k1gInSc1	rozkaz
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
záchranných	záchranný	k2eAgInPc2d1	záchranný
člunů	člun	k1gInPc2	člun
<g/>
,	,	kIx,	,
v	v	k7c6	v
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
10	[number]	k4	10
bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
vyklonění	vyklonění	k1gNnSc1	vyklonění
člunů	člun	k1gInPc2	člun
a	a	k8xC	a
v	v	k7c6	v
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
25	[number]	k4	25
rozkaz	rozkaz	k1gInSc1	rozkaz
k	k	k7c3	k
opuštění	opuštění	k1gNnSc3	opuštění
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
lodních	lodní	k2eAgFnPc2d1	lodní
zvyklostí	zvyklost	k1gFnPc2	zvyklost
"	"	kIx"	"
<g/>
ženy	žena	k1gFnPc1	žena
a	a	k8xC	a
děti	dítě	k1gFnPc1	dítě
první	první	k4xOgFnSc2	první
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
se	se	k3xPyFc4	se
první	první	k4xOgInPc4	první
čluny	člun	k1gInPc1	člun
začaly	začít	k5eAaPmAgInP	začít
plnit	plnit	k5eAaImF	plnit
dětmi	dítě	k1gFnPc7	dítě
a	a	k8xC	a
ženami	žena	k1gFnPc7	žena
<g/>
,	,	kIx,	,
v	v	k7c6	v
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
45	[number]	k4	45
dosedl	dosednout	k5eAaPmAgInS	dosednout
na	na	k7c4	na
hladinu	hladina	k1gFnSc4	hladina
první	první	k4xOgInSc1	první
záchranný	záchranný	k2eAgInSc1d1	záchranný
člun	člun	k1gInSc1	člun
číslo	číslo	k1gNnSc1	číslo
7	[number]	k4	7
na	na	k7c6	na
pravoboku	pravobok	k1gInSc6	pravobok
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
řídili	řídit	k5eAaImAgMnP	řídit
nástup	nástup	k1gInSc4	nástup
důstojníci	důstojník	k1gMnPc1	důstojník
Lowe	Lowe	k1gFnSc1	Lowe
a	a	k8xC	a
Pitman	Pitman	k1gMnSc1	Pitman
<g/>
,	,	kIx,	,
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
65	[number]	k4	65
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
naplněný	naplněný	k2eAgInSc4d1	naplněný
pouze	pouze	k6eAd1	pouze
28	[number]	k4	28
cestujícími	cestující	k1gMnPc7	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
levoboku	levobok	k1gInSc6	levobok
nastupování	nastupování	k1gNnSc2	nastupování
do	do	k7c2	do
člunů	člun	k1gInPc2	člun
řídil	řídit	k5eAaImAgInS	řídit
2	[number]	k4	2
<g/>
.	.	kIx.	.
důstojník	důstojník	k1gMnSc1	důstojník
C.	C.	kA	C.
H.	H.	kA	H.
Lightoller	Lightoller	k1gMnSc1	Lightoller
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
podle	podle	k7c2	podle
rozkazu	rozkaz	k1gInSc2	rozkaz
ženy	žena	k1gFnSc2	žena
a	a	k8xC	a
děti	dítě	k1gFnPc4	dítě
první	první	k4xOgInSc4	první
raději	rád	k6eAd2	rád
spustil	spustit	k5eAaPmAgMnS	spustit
člun	člun	k1gInSc4	člun
neúplně	úplně	k6eNd1	úplně
obsazený	obsazený	k2eAgMnSc1d1	obsazený
<g/>
,	,	kIx,	,
než	než	k8xS	než
aby	aby	kYmCp3nS	aby
dovolil	dovolit	k5eAaPmAgMnS	dovolit
nastoupit	nastoupit	k5eAaPmF	nastoupit
mužům	muž	k1gMnPc3	muž
<g/>
,	,	kIx,	,
i	i	k8xC	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
další	další	k2eAgFnPc1d1	další
ženy	žena	k1gFnPc1	žena
ani	ani	k8xC	ani
děti	dítě	k1gFnPc1	dítě
právě	právě	k9	právě
nečekaly	čekat	k5eNaImAgFnP	čekat
na	na	k7c6	na
nastoupení	nastoupení	k1gNnSc6	nastoupení
<g/>
.	.	kIx.	.
</s>
<s>
Záchranný	záchranný	k2eAgInSc1d1	záchranný
člun	člun	k1gInSc1	člun
č.	č.	k?	č.
6	[number]	k4	6
a	a	k8xC	a
5	[number]	k4	5
na	na	k7c6	na
levoboku	levobok	k1gInSc6	levobok
byly	být	k5eAaImAgFnP	být
spuštěny	spuštěn	k2eAgFnPc1d1	spuštěna
o	o	k7c4	o
deset	deset	k4xCc4	deset
minut	minuta	k1gFnPc2	minuta
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Člun	člun	k1gInSc1	člun
č.	č.	k?	č.
1	[number]	k4	1
na	na	k7c6	na
pravoboku	pravobok	k1gInSc6	pravobok
byl	být	k5eAaImAgInS	být
pátým	pátý	k4xOgNnSc7	pátý
spuštěným	spuštěný	k2eAgInSc7d1	spuštěný
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
12	[number]	k4	12
cestujícími	cestující	k1gMnPc7	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
člun	člun	k1gInSc1	člun
č.	č.	k?	č.
11	[number]	k4	11
byl	být	k5eAaImAgInS	být
přeplněn	přeplnit	k5eAaPmNgInS	přeplnit
70	[number]	k4	70
cestujícími	cestující	k1gMnPc7	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Skládací	skládací	k2eAgInSc1d1	skládací
člun	člun	k1gInSc1	člun
D	D	kA	D
byl	být	k5eAaImAgInS	být
posledním	poslední	k2eAgInSc7d1	poslední
spuštěným	spuštěný	k2eAgInSc7d1	spuštěný
člunem	člun	k1gInSc7	člun
<g/>
.	.	kIx.	.
</s>
<s>
Spouštění	spouštění	k1gNnSc1	spouštění
člunů	člun	k1gInPc2	člun
se	se	k3xPyFc4	se
na	na	k7c6	na
Titanicu	Titanicus	k1gInSc6	Titanicus
neobešlo	neobešlo	k1gNnSc1	neobešlo
bez	bez	k7c2	bez
potíží	potíž	k1gFnPc2	potíž
<g/>
:	:	kIx,	:
nebylo	být	k5eNaImAgNnS	být
posádkou	posádka	k1gFnSc7	posádka
nacvičeno	nacvičen	k2eAgNnSc4d1	nacvičeno
a	a	k8xC	a
ani	ani	k9	ani
cestující	cestující	k1gMnPc4	cestující
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
stále	stále	k6eAd1	stále
svítících	svítící	k2eAgNnPc2d1	svítící
světel	světlo	k1gNnPc2	světlo
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
<g/>
,	,	kIx,	,
nechtěli	chtít	k5eNaImAgMnP	chtít
nastupovat	nastupovat	k5eAaImF	nastupovat
do	do	k7c2	do
člunů	člun	k1gInPc2	člun
a	a	k8xC	a
ponořit	ponořit	k5eAaPmF	ponořit
se	se	k3xPyFc4	se
do	do	k7c2	do
tmy	tma	k1gFnSc2	tma
nad	nad	k7c7	nad
oceánem	oceán	k1gInSc7	oceán
a	a	k8xC	a
taktéž	taktéž	k?	taktéž
se	se	k3xPyFc4	se
některé	některý	k3yIgInPc1	některý
manželské	manželský	k2eAgInPc1d1	manželský
páry	pár	k1gInPc1	pár
nechtěly	chtít	k5eNaImAgInP	chtít
rozdělit	rozdělit	k5eAaPmF	rozdělit
<g/>
.	.	kIx.	.
</s>
<s>
Nastupování	nastupování	k1gNnSc4	nastupování
ztěžovala	ztěžovat	k5eAaImAgFnS	ztěžovat
i	i	k9	i
nevhodná	vhodný	k2eNgFnSc1d1	nevhodná
organizace	organizace	k1gFnSc1	organizace
záchranných	záchranný	k2eAgFnPc2d1	záchranná
prací	práce	k1gFnPc2	práce
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
stevardi	stevard	k1gMnPc1	stevard
John	John	k1gMnSc1	John
Edward	Edward	k1gMnSc1	Edward
Hart	Hart	k1gMnSc1	Hart
a	a	k8xC	a
William	William	k1gInSc1	William
Denton	Denton	k1gInSc1	Denton
Cox	Cox	k1gFnPc2	Cox
úspěšně	úspěšně	k6eAd1	úspěšně
vedli	vést	k5eAaImAgMnP	vést
skupiny	skupina	k1gFnSc2	skupina
cestujících	cestující	k1gMnPc2	cestující
z	z	k7c2	z
třetí	třetí	k4xOgFnSc2	třetí
třídy	třída	k1gFnSc2	třída
do	do	k7c2	do
záchranných	záchranný	k2eAgInPc2d1	záchranný
člunů	člun	k1gInPc2	člun
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
důvodů	důvod	k1gInPc2	důvod
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
spustit	spustit	k5eAaPmF	spustit
skládací	skládací	k2eAgInPc4d1	skládací
čluny	člun	k1gInPc4	člun
A	A	kA	A
a	a	k8xC	a
B.	B.	kA	B.
Z	z	k7c2	z
Titanicu	Titanicus	k1gInSc2	Titanicus
se	se	k3xPyFc4	se
ve	v	k7c6	v
člunech	člun	k1gInPc6	člun
zachránilo	zachránit	k5eAaPmAgNnS	zachránit
706	[number]	k4	706
cestujících	cestující	k1gMnPc2	cestující
a	a	k8xC	a
členů	člen	k1gMnPc2	člen
posádky	posádka	k1gFnSc2	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc1d1	poslední
dva	dva	k4xCgInPc1	dva
záchranné	záchranný	k2eAgInPc1d1	záchranný
čluny	člun	k1gInPc1	člun
již	již	k6eAd1	již
nebyly	být	k5eNaImAgInP	být
použitelné	použitelný	k2eAgInPc1d1	použitelný
<g/>
,	,	kIx,	,
skládací	skládací	k2eAgInSc1d1	skládací
člun	člun	k1gInSc1	člun
B	B	kA	B
byl	být	k5eAaImAgInS	být
dnem	den	k1gInSc7	den
vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc1d1	další
částečně	částečně	k6eAd1	částečně
zaplaveny	zaplaven	k2eAgFnPc1d1	zaplavena
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
bylo	být	k5eAaImAgNnS	být
plátno	plátno	k1gNnSc1	plátno
bočnic	bočnice	k1gFnPc2	bočnice
protrženo	protrhnout	k5eAaPmNgNnS	protrhnout
pádem	pád	k1gInSc7	pád
ze	z	k7c2	z
střechy	střecha	k1gFnSc2	střecha
kajut	kajuta	k1gFnPc2	kajuta
důstojníků	důstojník	k1gMnPc2	důstojník
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
<g/>
,	,	kIx,	,
v	v	k7c6	v
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
15	[number]	k4	15
<g/>
,	,	kIx,	,
začali	začít	k5eAaPmAgMnP	začít
radisté	radista	k1gMnPc1	radista
Jack	Jacko	k1gNnPc2	Jacko
Phillips	Phillipsa	k1gFnPc2	Phillipsa
a	a	k8xC	a
Harold	Harolda	k1gFnPc2	Harolda
Bride	Brid	k1gInSc5	Brid
vysílat	vysílat	k5eAaImF	vysílat
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
nouzový	nouzový	k2eAgInSc1d1	nouzový
signál	signál	k1gInSc1	signál
"	"	kIx"	"
<g/>
CQD	CQD	kA	CQD
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
postupně	postupně	k6eAd1	postupně
zachytilo	zachytit	k5eAaPmAgNnS	zachytit
několik	několik	k4yIc1	několik
lodí	loď	k1gFnPc2	loď
jako	jako	k8xC	jako
Mount	Mounta	k1gFnPc2	Mounta
Temple	templ	k1gInSc5	templ
společnosti	společnost	k1gFnSc2	společnost
Canadian	Canadian	k1gMnSc1	Canadian
Pacific	Pacific	k1gMnSc1	Pacific
<g/>
,	,	kIx,	,
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
společnosti	společnost	k1gFnSc2	společnost
Norddeutscher	Norddeutschra	k1gFnPc2	Norddeutschra
Lloyd	Lloyd	k1gMnSc1	Lloyd
<g/>
,	,	kIx,	,
Virginian	Virginian	k1gMnSc1	Virginian
společnosti	společnost	k1gFnSc2	společnost
Allan	Allan	k1gMnSc1	Allan
Line	linout	k5eAaImIp3nS	linout
<g/>
,	,	kIx,	,
sesterská	sesterský	k2eAgFnSc1d1	sesterská
loď	loď	k1gFnSc1	loď
Olympic	Olympice	k1gFnPc2	Olympice
<g/>
,	,	kIx,	,
ruský	ruský	k2eAgInSc1d1	ruský
parník	parník	k1gInSc1	parník
Birma	Birma	k1gFnSc1	Birma
i	i	k9	i
japonská	japonský	k2eAgFnSc1d1	japonská
loď	loď	k1gFnSc1	loď
Ypiranga	Ypiranga	k1gFnSc1	Ypiranga
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
tradičních	tradiční	k2eAgInPc2d1	tradiční
znaků	znak	k1gInPc2	znak
CQD	CQD	kA	CQD
vysílal	vysílat	k5eAaImAgInS	vysílat
Jack	Jack	k1gInSc1	Jack
Phillips	Phillips	k1gInSc4	Phillips
i	i	k9	i
nový	nový	k2eAgInSc4d1	nový
signál	signál	k1gInSc4	signál
"	"	kIx"	"
<g/>
SOS	sos	k1gInSc4	sos
<g/>
"	"	kIx"	"
-	-	kIx~	-
na	na	k7c6	na
upozornění	upozornění	k1gNnSc6	upozornění
druhého	druhý	k4xOgMnSc2	druhý
radisty	radista	k1gMnSc2	radista
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
signál	signál	k1gInSc1	signál
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
u	u	k7c2	u
White	Whit	k1gInSc5	Whit
Star	Star	kA	Star
Line	linout	k5eAaImIp3nS	linout
použit	použít	k5eAaPmNgInS	použít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
při	při	k7c6	při
ztroskotání	ztroskotání	k1gNnSc6	ztroskotání
lodě	loď	k1gFnSc2	loď
SS	SS	kA	SS
Florida	Florida	k1gFnSc1	Florida
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgFnSc7	první
toto	tento	k3xDgNnSc4	tento
volání	volání	k1gNnSc4	volání
zachytil	zachytit	k5eAaPmAgMnS	zachytit
radista	radista	k1gMnSc1	radista
Cottam	Cottam	k1gInSc4	Cottam
na	na	k7c6	na
parníku	parník	k1gInSc6	parník
Carpathia	Carpathius	k1gMnSc2	Carpathius
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
čase	čas	k1gInSc6	čas
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
odvysíláno	odvysílat	k5eAaPmNgNnS	odvysílat
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
informoval	informovat	k5eAaBmAgMnS	informovat
kapitána	kapitán	k1gMnSc4	kapitán
Arthura	Arthur	k1gMnSc4	Arthur
Rostrona	Rostron	k1gMnSc4	Rostron
o	o	k7c6	o
nouzovém	nouzový	k2eAgNnSc6d1	nouzové
vysílání	vysílání	k1gNnSc6	vysílání
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
okamžitě	okamžitě	k6eAd1	okamžitě
nařídil	nařídit	k5eAaPmAgMnS	nařídit
změnu	změna	k1gFnSc4	změna
kurzu	kurz	k1gInSc2	kurz
<g/>
.	.	kIx.	.
</s>
<s>
Carpathia	Carpathia	k1gFnSc1	Carpathia
byla	být	k5eAaImAgFnS	být
nejbližší	blízký	k2eAgFnSc7d3	nejbližší
lodí	loď	k1gFnSc7	loď
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
mohla	moct	k5eAaImAgFnS	moct
přispěchat	přispěchat	k5eAaPmF	přispěchat
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
58	[number]	k4	58
mil	míle	k1gFnPc2	míle
(	(	kIx(	(
<g/>
93	[number]	k4	93
km	km	kA	km
<g/>
)	)	kIx)	)
nemohla	moct	k5eNaImAgFnS	moct
však	však	k9	však
překonat	překonat	k5eAaPmF	překonat
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
za	za	k7c4	za
4	[number]	k4	4
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
26	[number]	k4	26
vysílali	vysílat	k5eAaImAgMnP	vysílat
radisté	radista	k1gMnPc1	radista
opravenou	opravený	k2eAgFnSc4d1	opravená
polohu	poloha	k1gFnSc4	poloha
Titanicu	Titanicus	k1gInSc2	Titanicus
a	a	k8xC	a
žádali	žádat	k5eAaImAgMnP	žádat
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
parník	parník	k1gInSc1	parník
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
<g/>
.	.	kIx.	.
</s>
<s>
Sdělili	sdělit	k5eAaPmAgMnP	sdělit
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Titanic	Titanic	k1gInSc1	Titanic
potápí	potápět	k5eAaImIp3nS	potápět
<g/>
.	.	kIx.	.
</s>
<s>
Lodě	loď	k1gFnPc1	loď
v	v	k7c6	v
dosahu	dosah	k1gInSc6	dosah
odpověděly	odpovědět	k5eAaPmAgFnP	odpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
připlují	připlout	k5eAaPmIp3nP	připlout
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žádná	žádný	k3yNgFnSc1	žádný
nebyla	být	k5eNaImAgFnS	být
tak	tak	k6eAd1	tak
blízko	blízko	k6eAd1	blízko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
Titanicu	Titanica	k1gFnSc4	Titanica
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
potopil	potopit	k5eAaPmAgInS	potopit
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgNnSc7d1	jediné
suchozemským	suchozemský	k2eAgNnSc7d1	suchozemské
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zachytilo	zachytit	k5eAaPmAgNnS	zachytit
nouzové	nouzový	k2eAgNnSc1d1	nouzové
volání	volání	k1gNnSc1	volání
z	z	k7c2	z
Titanicu	Titanicus	k1gInSc2	Titanicus
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
radiová	radiový	k2eAgFnSc1d1	radiová
stanice	stanice	k1gFnSc1	stanice
Cape	capat	k5eAaImIp3nS	capat
Race	Race	k1gInSc4	Race
na	na	k7c4	na
Newfoundlandu	Newfoundlanda	k1gFnSc4	Newfoundlanda
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
na	na	k7c6	na
některých	některý	k3yIgFnPc6	některý
lodích	loď	k1gFnPc6	loď
pochopili	pochopit	k5eAaPmAgMnP	pochopit
závažnost	závažnost	k1gFnSc4	závažnost
situace	situace	k1gFnSc2	situace
okamžitě	okamžitě	k6eAd1	okamžitě
<g/>
,	,	kIx,	,
na	na	k7c6	na
jiných	jiný	k2eAgInPc6d1	jiný
ještě	ještě	k6eAd1	ještě
v	v	k7c4	v
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
90	[number]	k4	90
minut	minuta	k1gFnPc2	minuta
po	po	k7c6	po
prvním	první	k4xOgNnSc6	první
vysílání	vysílání	k1gNnSc6	vysílání
CQD	CQD	kA	CQD
<g/>
,	,	kIx,	,
neměli	mít	k5eNaImAgMnP	mít
o	o	k7c6	o
situaci	situace	k1gFnSc6	situace
reálný	reálný	k2eAgInSc4d1	reálný
obraz	obraz	k1gInSc4	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Olympicu	Olympicus	k1gInSc2	Olympicus
<g/>
,	,	kIx,	,
sesterské	sesterský	k2eAgFnSc2d1	sesterská
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tázali	tázat	k5eAaImAgMnP	tázat
<g/>
,	,	kIx,	,
pluje	plout	k5eAaImIp3nS	plout
<g/>
-li	i	k?	-li
Titanic	Titanic	k1gInSc4	Titanic
jižním	jižní	k2eAgInSc7d1	jižní
směrem	směr	k1gInSc7	směr
a	a	k8xC	a
z	z	k7c2	z
parníku	parník	k1gInSc2	parník
Frankfurt	Frankfurt	k1gInSc4	Frankfurt
stále	stále	k6eAd1	stále
žádali	žádat	k5eAaImAgMnP	žádat
o	o	k7c4	o
více	hodně	k6eAd2	hodně
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
kapitán	kapitán	k1gMnSc1	kapitán
Smith	Smith	k1gMnSc1	Smith
osobně	osobně	k6eAd1	osobně
navštívil	navštívit	k5eAaPmAgMnS	navštívit
radisty	radista	k1gMnPc4	radista
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jim	on	k3xPp3gFnPc3	on
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
udělali	udělat	k5eAaPmAgMnP	udělat
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
mohli	moct	k5eAaImAgMnP	moct
<g/>
,	,	kIx,	,
a	a	k8xC	a
nařídil	nařídit	k5eAaPmAgMnS	nařídit
jim	on	k3xPp3gMnPc3	on
opustit	opustit	k5eAaPmF	opustit
kabinu	kabina	k1gFnSc4	kabina
a	a	k8xC	a
pokusit	pokusit	k5eAaPmF	pokusit
se	se	k3xPyFc4	se
o	o	k7c4	o
záchranu	záchrana	k1gFnSc4	záchrana
<g/>
.	.	kIx.	.
</s>
<s>
Totéž	týž	k3xTgNnSc4	týž
zopakoval	zopakovat	k5eAaPmAgMnS	zopakovat
svým	svůj	k3xOyFgMnPc3	svůj
důstojníkům	důstojník	k1gMnPc3	důstojník
a	a	k8xC	a
všem	všecek	k3xTgMnPc3	všecek
členům	člen	k1gMnPc3	člen
posádky	posádka	k1gFnSc2	posádka
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
potkal	potkat	k5eAaPmAgInS	potkat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
ten	ten	k3xDgInSc4	ten
moment	moment	k1gInSc4	moment
zbývalo	zbývat	k5eAaImAgNnS	zbývat
Titanicu	Titanica	k1gFnSc4	Titanica
do	do	k7c2	do
potopení	potopení	k1gNnSc2	potopení
už	už	k6eAd1	už
jen	jen	k9	jen
15	[number]	k4	15
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zdůraznit	zdůraznit	k5eAaPmF	zdůraznit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
záchrana	záchrana	k1gFnSc1	záchrana
již	již	k6eAd1	již
zcela	zcela	k6eAd1	zcela
nemožná	možný	k2eNgFnSc1d1	nemožná
(	(	kIx(	(
<g/>
přestože	přestože	k8xS	přestože
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
radistů	radista	k1gMnPc2	radista
se	s	k7c7	s
šťastnou	šťastný	k2eAgFnSc7d1	šťastná
náhodou	náhoda	k1gFnSc7	náhoda
zachránil	zachránit	k5eAaPmAgInS	zachránit
<g/>
)	)	kIx)	)
-	-	kIx~	-
své	své	k1gNnSc4	své
hrdiny	hrdina	k1gMnSc2	hrdina
si	se	k3xPyFc3	se
tak	tak	k9	tak
dodnes	dodnes	k6eAd1	dodnes
připomíná	připomínat	k5eAaImIp3nS	připomínat
jak	jak	k6eAd1	jak
britská	britský	k2eAgFnSc1d1	britská
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
americká	americký	k2eAgFnSc1d1	americká
pošta	pošta	k1gFnSc1	pošta
(	(	kIx(	(
<g/>
poštovní	poštovní	k2eAgMnPc1d1	poštovní
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
neopustili	opustit	k5eNaPmAgMnP	opustit
svou	svůj	k3xOyFgFnSc4	svůj
povinnost	povinnost	k1gFnSc4	povinnost
pečovat	pečovat	k5eAaImF	pečovat
o	o	k7c4	o
zásilky	zásilka	k1gFnPc4	zásilka
<g />
.	.	kIx.	.
</s>
<s>
do	do	k7c2	do
úplného	úplný	k2eAgInSc2d1	úplný
konce	konec	k1gInSc2	konec
<g/>
)	)	kIx)	)
a	a	k8xC	a
britští	britský	k2eAgMnPc1d1	britský
námořníci	námořník	k1gMnPc1	námořník
-	-	kIx~	-
strojníci	strojník	k1gMnPc1	strojník
dodnes	dodnes	k6eAd1	dodnes
nosí	nosit	k5eAaImIp3nP	nosit
svá	svůj	k3xOyFgNnPc4	svůj
hodnostní	hodnostní	k2eAgNnPc4d1	hodnostní
označení	označení	k1gNnPc4	označení
na	na	k7c6	na
královském	královský	k2eAgInSc6d1	královský
purpuru	purpur	k1gInSc6	purpur
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
svých	svůj	k3xOyFgMnPc2	svůj
kolegů	kolega	k1gMnPc2	kolega
z	z	k7c2	z
Titanicu	Titanicus	k1gInSc2	Titanicus
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
neopustili	opustit	k5eNaPmAgMnP	opustit
svá	svůj	k3xOyFgNnPc4	svůj
místa	místo	k1gNnPc4	místo
a	a	k8xC	a
do	do	k7c2	do
posledního	poslední	k2eAgInSc2d1	poslední
okamžiku	okamžik	k1gInSc2	okamžik
zajišťovali	zajišťovat	k5eAaImAgMnP	zajišťovat
hroutící	hroutící	k2eAgFnSc4d1	hroutící
se	se	k3xPyFc4	se
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
síť	síť	k1gFnSc4	síť
-	-	kIx~	-
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
při	při	k7c6	při
plnění	plnění	k1gNnSc6	plnění
svých	svůj	k3xOyFgFnPc2	svůj
povinností	povinnost	k1gFnPc2	povinnost
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
těmto	tento	k3xDgMnPc3	tento
lidem	člověk	k1gMnPc3	člověk
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
Titanic	Titanic	k1gInSc1	Titanic
považován	považován	k2eAgInSc1d1	považován
nejen	nejen	k6eAd1	nejen
za	za	k7c4	za
katastrofu	katastrofa	k1gFnSc4	katastrofa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
za	za	k7c4	za
symbol	symbol	k1gInSc4	symbol
síly	síla	k1gFnSc2	síla
lidského	lidský	k2eAgMnSc2d1	lidský
ducha	duch	k1gMnSc2	duch
a	a	k8xC	a
"	"	kIx"	"
<g/>
britské	britský	k2eAgFnSc2d1	britská
<g/>
"	"	kIx"	"
morálky	morálka	k1gFnSc2	morálka
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
130	[number]	k4	130
minut	minuta	k1gFnPc2	minuta
po	po	k7c6	po
nárazu	náraz	k1gInSc6	náraz
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
voda	voda	k1gFnSc1	voda
přelévat	přelévat	k5eAaImF	přelévat
ze	z	k7c2	z
šesté	šestý	k4xOgFnSc2	šestý
do	do	k7c2	do
sedmé	sedmý	k4xOgFnSc2	sedmý
komory	komora	k1gFnSc2	komora
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
10	[number]	k4	10
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgNnP	začít
zvedat	zvedat	k5eAaImF	zvedat
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
záď	záď	k1gFnSc4	záď
a	a	k8xC	a
začaly	začít	k5eAaPmAgFnP	začít
se	se	k3xPyFc4	se
vynořovat	vynořovat	k5eAaImF	vynořovat
lodní	lodní	k2eAgInPc4d1	lodní
šrouby	šroub	k1gInPc4	šroub
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
17	[number]	k4	17
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
hladina	hladina	k1gFnSc1	hladina
lodní	lodní	k2eAgFnSc2d1	lodní
paluby	paluba	k1gFnSc2	paluba
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
zhroutil	zhroutit	k5eAaPmAgInS	zhroutit
přední	přední	k2eAgInSc1d1	přední
komín	komín	k1gInSc1	komín
<g/>
,	,	kIx,	,
když	když	k8xS	když
popraskala	popraskat	k5eAaPmAgFnS	popraskat
jeho	jeho	k3xOp3gNnPc4	jeho
kotvící	kotvící	k2eAgNnPc4d1	kotvící
lana	lano	k1gNnPc4	lano
<g/>
.	.	kIx.	.
</s>
<s>
Komín	Komín	k1gInSc1	Komín
pádem	pád	k1gInSc7	pád
rozdrtil	rozdrtit	k5eAaPmAgInS	rozdrtit
několik	několik	k4yIc4	několik
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
a	a	k8xC	a
i	i	k9	i
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
snažili	snažit	k5eAaImAgMnP	snažit
dostat	dostat	k5eAaPmF	dostat
na	na	k7c4	na
záď	záď	k1gFnSc4	záď
Titanicu	Titanicus	k1gInSc2	Titanicus
nebo	nebo	k8xC	nebo
skákali	skákat	k5eAaImAgMnP	skákat
přes	přes	k7c4	přes
palubu	paluba	k1gFnSc4	paluba
v	v	k7c6	v
naději	naděje	k1gFnSc6	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
dostanou	dostat	k5eAaPmIp3nP	dostat
do	do	k7c2	do
některého	některý	k3yIgInSc2	některý
záchranného	záchranný	k2eAgInSc2d1	záchranný
člunu	člun	k1gInSc2	člun
na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
zvyšujícím	zvyšující	k2eAgMnSc7d1	zvyšující
se	se	k3xPyFc4	se
náklonem	náklon	k1gInSc7	náklon
začaly	začít	k5eAaPmAgInP	začít
padat	padat	k5eAaImF	padat
nezajištěné	zajištěný	k2eNgInPc1d1	nezajištěný
předměty	předmět	k1gInPc1	předmět
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
i	i	k8xC	i
ve	v	k7c6	v
vnitřku	vnitřek	k1gInSc6	vnitřek
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
dalším	další	k2eAgNnSc7d1	další
zaplavováním	zaplavování	k1gNnSc7	zaplavování
podpalubí	podpalubí	k1gNnSc2	podpalubí
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zaplavení	zaplavení	k1gNnSc3	zaplavení
posledních	poslední	k2eAgFnPc2d1	poslední
kotelen	kotelna	k1gFnPc2	kotelna
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
byly	být	k5eAaImAgFnP	být
vyřazeny	vyřadit	k5eAaPmNgInP	vyřadit
i	i	k9	i
generátory	generátor	k1gInPc4	generátor
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
-	-	kIx~	-
Titanic	Titanic	k1gInSc1	Titanic
se	se	k3xPyFc4	se
ponořil	ponořit	k5eAaPmAgInS	ponořit
do	do	k7c2	do
tmy	tma	k1gFnSc2	tma
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
trup	trup	k1gMnSc1	trup
Titanicu	Titanicus	k1gInSc2	Titanicus
rozlomil	rozlomit	k5eAaPmAgMnS	rozlomit
za	za	k7c7	za
polovinou	polovina	k1gFnSc7	polovina
délky	délka	k1gFnSc2	délka
trupu	trup	k1gInSc2	trup
<g/>
.	.	kIx.	.
</s>
<s>
Přední	přední	k2eAgFnSc1d1	přední
část	část	k1gFnSc1	část
šla	jít	k5eAaImAgFnS	jít
okamžitě	okamžitě	k6eAd1	okamžitě
pod	pod	k7c4	pod
hladinu	hladina	k1gFnSc4	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Zadní	zadní	k2eAgFnSc1d1	zadní
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
po	po	k7c6	po
odlehčení	odlehčení	k1gNnSc6	odlehčení
na	na	k7c4	na
chvíli	chvíle	k1gFnSc4	chvíle
narovnala	narovnat	k5eAaPmAgFnS	narovnat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
roztrženým	roztržený	k2eAgInSc7d1	roztržený
trupem	trup	k1gInSc7	trup
drala	drát	k5eAaImAgFnS	drát
voda	voda	k1gFnSc1	voda
dovnitř	dovnitř	k6eAd1	dovnitř
<g/>
,	,	kIx,	,
po	po	k7c6	po
chvíli	chvíle	k1gFnSc6	chvíle
se	se	k3xPyFc4	se
počala	počnout	k5eAaPmAgFnS	počnout
potápět	potápět	k5eAaImF	potápět
a	a	k8xC	a
ve	v	k7c6	v
svislé	svislý	k2eAgFnSc6d1	svislá
poloze	poloha	k1gFnSc6	poloha
se	se	k3xPyFc4	se
ponořila	ponořit	k5eAaPmAgFnS	ponořit
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1912	[number]	k4	1912
ve	v	k7c4	v
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
20	[number]	k4	20
zmizel	zmizet	k5eAaPmAgInS	zmizet
Titanic	Titanic	k1gInSc1	Titanic
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
potopení	potopení	k1gNnSc6	potopení
Titanicu	Titanicus	k1gInSc2	Titanicus
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
dva	dva	k4xCgMnPc1	dva
z	z	k7c2	z
20	[number]	k4	20
člunů	člun	k1gInPc2	člun
vrátily	vrátit	k5eAaPmAgInP	vrátit
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
potopení	potopení	k1gNnSc2	potopení
a	a	k8xC	a
hledaly	hledat	k5eAaImAgFnP	hledat
přeživší	přeživší	k2eAgFnPc1d1	přeživší
<g/>
.	.	kIx.	.
</s>
<s>
Záchranný	záchranný	k2eAgInSc1d1	záchranný
člun	člun	k1gInSc1	člun
číslo	číslo	k1gNnSc1	číslo
4	[number]	k4	4
byl	být	k5eAaImAgMnS	být
poměrně	poměrně	k6eAd1	poměrně
blízko	blízko	k6eAd1	blízko
a	a	k8xC	a
vyzvedl	vyzvednout	k5eAaPmAgMnS	vyzvednout
pět	pět	k4xCc4	pět
lidí	člověk	k1gMnPc2	člověk
<g/>
;	;	kIx,	;
dva	dva	k4xCgMnPc1	dva
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
později	pozdě	k6eAd2	pozdě
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
o	o	k7c4	o
hodinu	hodina	k1gFnSc4	hodina
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
ještě	ještě	k9	ještě
záchranný	záchranný	k2eAgInSc1d1	záchranný
člun	člun	k1gInSc1	člun
číslo	číslo	k1gNnSc1	číslo
14	[number]	k4	14
a	a	k8xC	a
zachránil	zachránit	k5eAaPmAgMnS	zachránit
čtyři	čtyři	k4xCgFnPc4	čtyři
osoby	osoba	k1gFnPc4	osoba
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
jeden	jeden	k4xCgInSc4	jeden
později	pozdě	k6eAd2	pozdě
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
skočili	skočit	k5eAaPmAgMnP	skočit
z	z	k7c2	z
paluby	paluba	k1gFnSc2	paluba
a	a	k8xC	a
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
člunů	člun	k1gInPc2	člun
<g/>
.	.	kIx.	.
</s>
<s>
Některým	některý	k3yIgNnSc7	některý
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
povedlo	povést	k5eAaPmAgNnS	povést
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
byli	být	k5eAaImAgMnP	být
odmítáni	odmítat	k5eAaImNgMnP	odmítat
v	v	k7c6	v
obavě	obava	k1gFnSc6	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
přeplnění	přeplnění	k1gNnSc3	přeplnění
člunu	člun	k1gInSc2	člun
a	a	k8xC	a
jeho	on	k3xPp3gNnSc2	on
převrácení	převrácení	k1gNnSc2	převrácení
<g/>
.	.	kIx.	.
</s>
<s>
Posádky	posádka	k1gFnPc1	posádka
člunů	člun	k1gInPc2	člun
se	se	k3xPyFc4	se
snažily	snažit	k5eAaImAgFnP	snažit
odplout	odplout	k5eAaPmF	odplout
co	co	k9	co
nejdále	daleko	k6eAd3	daleko
od	od	k7c2	od
místa	místo	k1gNnSc2	místo
potopení	potopení	k1gNnSc2	potopení
z	z	k7c2	z
obavy	obava	k1gFnSc2	obava
ze	z	k7c2	z
sacího	sací	k2eAgInSc2d1	sací
efektu	efekt	k1gInSc2	efekt
<g/>
;	;	kIx,	;
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
efekt	efekt	k1gInSc1	efekt
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
malý	malý	k2eAgInSc1d1	malý
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k7c2	poblíž
Titanicu	Titanicus	k1gInSc2	Titanicus
byla	být	k5eAaImAgFnS	být
spatřena	spatřen	k2eAgFnSc1d1	spatřena
loď	loď	k1gFnSc1	loď
<g/>
,	,	kIx,	,
asi	asi	k9	asi
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
5	[number]	k4	5
mil	míle	k1gFnPc2	míle
(	(	kIx(	(
<g/>
8,0	[number]	k4	8,0
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
stála	stát	k5eAaImAgFnS	stát
přes	přes	k7c4	přes
noc	noc	k1gFnSc4	noc
v	v	k7c6	v
ledovém	ledový	k2eAgNnSc6d1	ledové
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
loď	loď	k1gFnSc1	loď
představuje	představovat	k5eAaImIp3nS	představovat
dodnes	dodnes	k6eAd1	dodnes
záhadu	záhada	k1gFnSc4	záhada
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
paluby	paluba	k1gFnSc2	paluba
Titanicu	Titanicus	k1gInSc2	Titanicus
bylo	být	k5eAaImAgNnS	být
vidět	vidět	k5eAaImF	vidět
loď	loď	k1gFnSc4	loď
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
nemohli	moct	k5eNaImAgMnP	moct
identifikovat	identifikovat	k5eAaBmF	identifikovat
<g/>
.	.	kIx.	.
</s>
<s>
Mohlo	moct	k5eAaImAgNnS	moct
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
parník	parník	k1gInSc4	parník
Californian	Californiany	k1gInPc2	Californiany
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
o	o	k7c4	o
norskou	norský	k2eAgFnSc4d1	norská
loď	loď	k1gFnSc4	loď
Samson	Samson	k1gMnSc1	Samson
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
loď	loď	k1gFnSc1	loď
nereagovala	reagovat	k5eNaBmAgFnS	reagovat
na	na	k7c4	na
rádiové	rádiový	k2eAgNnSc4d1	rádiové
vysílání	vysílání	k1gNnSc4	vysílání
a	a	k8xC	a
tak	tak	k9	tak
se	s	k7c7	s
4	[number]	k4	4
<g/>
.	.	kIx.	.
proviantní	proviantní	k2eAgMnSc1d1	proviantní
důstojník	důstojník	k1gMnSc1	důstojník
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gMnSc1	Joseph
Grove	Groev	k1gFnSc2	Groev
Boxhall	Boxhall	k1gMnSc1	Boxhall
<g/>
,	,	kIx,	,
pokusil	pokusit	k5eAaPmAgMnS	pokusit
o	o	k7c4	o
přímé	přímý	k2eAgNnSc4d1	přímé
spojení	spojení	k1gNnSc4	spojení
Morseovou	Morseův	k2eAgFnSc7d1	Morseova
lampou	lampa	k1gFnSc7	lampa
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
loď	loď	k1gFnSc1	loď
nereagovala	reagovat	k5eNaBmAgFnS	reagovat
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
vystřelovat	vystřelovat	k5eAaImF	vystřelovat
světlice	světlice	k1gFnPc4	světlice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
mezitím	mezitím	k6eAd1	mezitím
přinesl	přinést	k5eAaPmAgMnS	přinést
kormidelník	kormidelník	k1gMnSc1	kormidelník
George	Georg	k1gMnSc2	Georg
Rowe	Rowe	k1gNnSc2	Rowe
<g/>
,	,	kIx,	,
a	a	k8xC	a
Rowe	Rowe	k1gInSc1	Rowe
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c4	v
signalizování	signalizování	k1gNnSc4	signalizování
Morseovou	Morseův	k2eAgFnSc7d1	Morseova
lampou	lampa	k1gFnSc7	lampa
<g/>
.	.	kIx.	.
</s>
<s>
Nejblíže	blízce	k6eAd3	blízce
reagující	reagující	k2eAgFnSc7d1	reagující
lodí	loď	k1gFnSc7	loď
byla	být	k5eAaImAgFnS	být
Carpathia	Carpathia	k1gFnSc1	Carpathia
kapitána	kapitán	k1gMnSc2	kapitán
Arthura	Arthur	k1gMnSc2	Arthur
A.	A.	kA	A.
Rostona	Roston	k1gMnSc2	Roston
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgNnSc1d1	patřící
rejdařství	rejdařství	k1gNnSc1	rejdařství
Cunard	Cunarda	k1gFnPc2	Cunarda
<g/>
,	,	kIx,	,
o	o	k7c6	o
výtlaku	výtlak	k1gInSc6	výtlak
8	[number]	k4	8
600	[number]	k4	600
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
plula	plout	k5eAaImAgFnS	plout
z	z	k7c2	z
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
Gibraltarskému	gibraltarský	k2eAgInSc3d1	gibraltarský
průlivu	průliv	k1gInSc3	průliv
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
na	na	k7c4	na
Titanicu	Titanica	k1gFnSc4	Titanica
<g/>
,	,	kIx,	,
i	i	k8xC	i
zde	zde	k6eAd1	zde
radisté	radista	k1gMnPc1	radista
přijali	přijmout	k5eAaPmAgMnP	přijmout
zprávy	zpráva	k1gFnPc4	zpráva
o	o	k7c6	o
výskytu	výskyt	k1gInSc6	výskyt
ledovců	ledovec	k1gInPc2	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Radista	radista	k1gMnSc1	radista
Cottam	Cottam	k1gInSc4	Cottam
se	se	k3xPyFc4	se
již	již	k6eAd1	již
chystal	chystat	k5eAaImAgMnS	chystat
vypnout	vypnout	k5eAaPmF	vypnout
radiostanici	radiostanice	k1gFnSc4	radiostanice
<g/>
,	,	kIx,	,
když	když	k8xS	když
zaslechl	zaslechnout	k5eAaPmAgMnS	zaslechnout
první	první	k4xOgNnSc4	první
volání	volání	k1gNnSc4	volání
Titanicu	Titanicus	k1gInSc2	Titanicus
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
-	-	kIx~	-
CQD	CQD	kA	CQD
a	a	k8xC	a
souřadnice	souřadnice	k1gFnSc1	souřadnice
Zpráva	zpráva	k1gFnSc1	zpráva
byla	být	k5eAaImAgFnS	být
neprodleně	prodleně	k6eNd1	prodleně
doručena	doručit	k5eAaPmNgFnS	doručit
až	až	k9	až
ke	k	k7c3	k
kapitánovi	kapitán	k1gMnSc3	kapitán
<g/>
.	.	kIx.	.
</s>
<s>
Carpathia	Carpathia	k1gFnSc1	Carpathia
byla	být	k5eAaImAgFnS	být
vzdálena	vzdálit	k5eAaPmNgFnS	vzdálit
58	[number]	k4	58
mil	míle	k1gFnPc2	míle
(	(	kIx(	(
<g/>
93	[number]	k4	93
km	km	kA	km
<g/>
)	)	kIx)	)
od	od	k7c2	od
Titanicu	Titanicus	k1gInSc2	Titanicus
<g/>
.	.	kIx.	.
</s>
<s>
Kapitán	kapitán	k1gMnSc1	kapitán
okamžitě	okamžitě	k6eAd1	okamžitě
nařídil	nařídit	k5eAaPmAgMnS	nařídit
kurz	kurz	k1gInSc4	kurz
308	[number]	k4	308
<g/>
°	°	k?	°
a	a	k8xC	a
rozestavil	rozestavit	k5eAaPmAgMnS	rozestavit
hlídky	hlídka	k1gFnPc4	hlídka
pro	pro	k7c4	pro
sledování	sledování	k1gNnSc4	sledování
hladiny	hladina	k1gFnSc2	hladina
a	a	k8xC	a
hlášení	hlášení	k1gNnSc2	hlášení
pozorovaných	pozorovaný	k2eAgInPc2d1	pozorovaný
ledovců	ledovec	k1gInPc2	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
si	se	k3xPyFc3	se
dobře	dobře	k6eAd1	dobře
vědom	vědom	k2eAgMnSc1d1	vědom
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
čeká	čekat	k5eAaImIp3nS	čekat
cestující	cestující	k1gMnPc4	cestující
v	v	k7c6	v
ledové	ledový	k2eAgFnSc6d1	ledová
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
a	a	k8xC	a
udělal	udělat	k5eAaPmAgMnS	udělat
vše	všechen	k3xTgNnSc4	všechen
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
dorazil	dorazit	k5eAaPmAgMnS	dorazit
co	co	k9	co
nejdříve	dříve	k6eAd3	dříve
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc4	což
si	se	k3xPyFc3	se
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
jak	jak	k8xS	jak
pochvalu	pochvala	k1gFnSc4	pochvala
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
kritiku	kritika	k1gFnSc4	kritika
za	za	k7c4	za
podstoupení	podstoupení	k1gNnSc4	podstoupení
přílišného	přílišný	k2eAgNnSc2d1	přílišné
rizika	riziko	k1gNnSc2	riziko
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
s	s	k7c7	s
lodí	loď	k1gFnSc7	loď
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
maximální	maximální	k2eAgFnSc1d1	maximální
rychlost	rychlost	k1gFnSc1	rychlost
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
14	[number]	k4	14
uzlů	uzel	k1gInPc2	uzel
(	(	kIx(	(
<g/>
26	[number]	k4	26
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prokličkoval	prokličkovat	k5eAaPmAgInS	prokličkovat
ledovým	ledový	k2eAgNnSc7d1	ledové
polem	pole	k1gNnSc7	pole
a	a	k8xC	a
mezi	mezi	k7c7	mezi
krami	kra	k1gFnPc7	kra
rychlostí	rychlost	k1gFnPc2	rychlost
o	o	k7c4	o
více	hodně	k6eAd2	hodně
než	než	k8xS	než
3	[number]	k4	3
uzly	uzel	k1gInPc1	uzel
vyšší	vysoký	k2eAgInPc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
rádiová	rádiový	k2eAgFnSc1d1	rádiová
depeše	depeše	k1gFnSc1	depeše
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
Carpathii	Carpathie	k1gFnSc4	Carpathie
zachycena	zachytit	k5eAaPmNgFnS	zachytit
v	v	k7c6	v
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
45	[number]	k4	45
<g/>
.	.	kIx.	.
</s>
<s>
Rychlostí	rychlost	k1gFnPc2	rychlost
17,5	[number]	k4	17,5
uzlů	uzel	k1gInPc2	uzel
(	(	kIx(	(
<g/>
32,4	[number]	k4	32,4
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
proplula	proplout	k5eAaPmAgFnS	proplout
Carpathia	Carpathia	k1gFnSc1	Carpathia
k	k	k7c3	k
místu	místo	k1gNnSc3	místo
ztroskotání	ztroskotání	k1gNnSc2	ztroskotání
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
45	[number]	k4	45
byla	být	k5eAaImAgFnS	být
Carpathia	Carpathia	k1gFnSc1	Carpathia
téměř	téměř	k6eAd1	téměř
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
před	před	k7c7	před
čtvrtou	čtvrtý	k4xOgFnSc7	čtvrtý
hodinou	hodina	k1gFnSc7	hodina
ranní	ranní	k1gFnSc2	ranní
nechal	nechat	k5eAaPmAgMnS	nechat
kapitán	kapitán	k1gMnSc1	kapitán
zastavit	zastavit	k5eAaPmF	zastavit
stroje	stroj	k1gInPc4	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
hodině	hodina	k1gFnSc6	hodina
ranní	ranní	k1gFnSc1	ranní
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
Carpathie	Carpathie	k1gFnSc2	Carpathie
spatřena	spatřen	k2eAgFnSc1d1	spatřena
signální	signální	k2eAgFnSc1d1	signální
raketa	raketa	k1gFnSc1	raketa
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
ze	z	k7c2	z
záchranných	záchranný	k2eAgInPc2d1	záchranný
člunů	člun	k1gInPc2	člun
Titanicu	Titanicus	k1gInSc2	Titanicus
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
400	[number]	k4	400
metrů	metr	k1gInPc2	metr
daleko	daleko	k6eAd1	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
necelých	celý	k2eNgNnPc2d1	necelé
deset	deset	k4xCc4	deset
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
ve	v	k7c4	v
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
začali	začít	k5eAaPmAgMnP	začít
námořníci	námořník	k1gMnPc1	námořník
vyzvedávat	vyzvedávat	k5eAaImF	vyzvedávat
první	první	k4xOgMnPc4	první
trosečníky	trosečník	k1gMnPc4	trosečník
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
vyzvednutý	vyzvednutý	k2eAgInSc1d1	vyzvednutý
člun	člun	k1gInSc1	člun
a	a	k8xC	a
zachránění	zachráněný	k2eAgMnPc1d1	zachráněný
byli	být	k5eAaImAgMnP	být
ze	z	k7c2	z
člunu	člun	k1gInSc2	člun
č.	č.	k?	č.
2	[number]	k4	2
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
velel	velet	k5eAaImAgInS	velet
4	[number]	k4	4
<g/>
.	.	kIx.	.
důstojník	důstojník	k1gMnSc1	důstojník
Titanicu	Titanicus	k1gInSc2	Titanicus
Boxhall	Boxhall	k1gMnSc1	Boxhall
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
následovalo	následovat	k5eAaImAgNnS	následovat
vyzvednutí	vyzvednutí	k1gNnSc1	vyzvednutí
dalších	další	k2eAgInPc2d1	další
člunů	člun	k1gInPc2	člun
<g/>
.	.	kIx.	.
</s>
<s>
Situaci	situace	k1gFnSc4	situace
komplikovaly	komplikovat	k5eAaBmAgFnP	komplikovat
vlny	vlna	k1gFnPc1	vlna
a	a	k8xC	a
ledovce	ledovec	k1gInPc1	ledovec
plující	plující	k2eAgInPc1d1	plující
po	po	k7c6	po
hladině	hladina	k1gFnSc6	hladina
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
musely	muset	k5eAaImAgInP	muset
čluny	člun	k1gInPc1	člun
i	i	k9	i
Carpathia	Carpathia	k1gFnSc1	Carpathia
vyhýbat	vyhýbat	k5eAaImF	vyhýbat
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
člunem	člun	k1gInSc7	člun
byl	být	k5eAaImAgInS	být
člun	člun	k1gInSc1	člun
č.	č.	k?	č.
12	[number]	k4	12
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
2	[number]	k4	2
<g/>
.	.	kIx.	.
důstojníka	důstojník	k1gMnSc4	důstojník
Lightollera	Lightoller	k1gMnSc4	Lightoller
z	z	k7c2	z
Titanicu	Titanicus	k1gInSc2	Titanicus
<g/>
.	.	kIx.	.
</s>
<s>
Člun	člun	k1gInSc1	člun
byl	být	k5eAaImAgInS	být
přetížen	přetížen	k2eAgMnSc1d1	přetížen
<g/>
,	,	kIx,	,
nemohl	moct	k5eNaImAgMnS	moct
dobře	dobře	k6eAd1	dobře
manévrovat	manévrovat	k5eAaImF	manévrovat
ve	v	k7c6	v
vlnách	vlna	k1gFnPc6	vlna
<g/>
,	,	kIx,	,
a	a	k8xC	a
hrozilo	hrozit	k5eAaImAgNnS	hrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
převrhne	převrhnout	k5eAaPmIp3nS	převrhnout
několik	několik	k4yIc1	několik
stovek	stovka	k1gFnPc2	stovka
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
Carpathie	Carpathie	k1gFnSc2	Carpathie
<g/>
.	.	kIx.	.
</s>
<s>
Kapitán	kapitán	k1gMnSc1	kapitán
Rostron	Rostron	k1gInSc1	Rostron
proto	proto	k8xC	proto
otočil	otočit	k5eAaPmAgInS	otočit
Carpathii	Carpathie	k1gFnSc4	Carpathie
přídí	příď	k1gFnPc2	příď
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
člunu	člun	k1gInSc3	člun
a	a	k8xC	a
tak	tak	k6eAd1	tak
zkrátil	zkrátit	k5eAaPmAgMnS	zkrátit
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
na	na	k7c6	na
"	"	kIx"	"
<g/>
pouhých	pouhý	k2eAgInPc6d1	pouhý
<g/>
"	"	kIx"	"
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
tak	tak	k6eAd1	tak
ale	ale	k8xC	ale
trosečníci	trosečník	k1gMnPc1	trosečník
neměli	mít	k5eNaImAgMnP	mít
záchranu	záchrana	k1gFnSc4	záchrana
jistou	jistý	k2eAgFnSc4d1	jistá
<g/>
,	,	kIx,	,
během	během	k7c2	během
přibližování	přibližování	k1gNnSc2	přibližování
se	se	k3xPyFc4	se
několik	několik	k4yIc1	několik
vln	vlna	k1gFnPc2	vlna
přelilo	přelít	k5eAaPmAgNnS	přelít
přes	přes	k7c4	přes
příď	příď	k1gFnSc4	příď
člunu	člun	k1gInSc2	člun
a	a	k8xC	a
museli	muset	k5eAaImAgMnP	muset
obeplout	obeplout	k5eAaPmF	obeplout
příď	příď	k1gFnSc4	příď
Carpathie	Carpathie	k1gFnSc2	Carpathie
na	na	k7c4	na
závětrnou	závětrný	k2eAgFnSc4d1	závětrná
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
hladina	hladina	k1gFnSc1	hladina
klidnější	klidný	k2eAgFnSc1d2	klidnější
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
byli	být	k5eAaImAgMnP	být
zachráněni	zachráněn	k2eAgMnPc1d1	zachráněn
poslední	poslední	k2eAgMnPc1d1	poslední
ztroskotanci	ztroskotanec	k1gMnPc1	ztroskotanec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
50	[number]	k4	50
se	se	k3xPyFc4	se
Carpathia	Carpathia	k1gFnSc1	Carpathia
vydala	vydat	k5eAaPmAgFnS	vydat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
New	New	k1gFnSc3	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
původnímu	původní	k2eAgInSc3d1	původní
cíli	cíl	k1gInSc3	cíl
plavby	plavba	k1gFnSc2	plavba
Titanicu	Titanicus	k1gInSc2	Titanicus
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
přesahující	přesahující	k2eAgFnSc4d1	přesahující
2	[number]	k4	2
200	[number]	k4	200
osob	osoba	k1gFnPc2	osoba
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
následkem	následkem	k7c2	následkem
potopení	potopení	k1gNnSc2	potopení
Titanicu	Titanicus	k1gInSc2	Titanicus
kolem	kolem	k7c2	kolem
1	[number]	k4	1
500	[number]	k4	500
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
přibližně	přibližně	k6eAd1	přibližně
představuje	představovat	k5eAaImIp3nS	představovat
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
lidských	lidský	k2eAgFnPc2d1	lidská
ztrát	ztráta	k1gFnPc2	ztráta
bylo	být	k5eAaImAgNnS	být
podchlazení	podchlazení	k1gNnSc4	podchlazení
ve	v	k7c6	v
studené	studený	k2eAgFnSc6d1	studená
vodě	voda	k1gFnSc6	voda
o	o	k7c6	o
teplotě	teplota	k1gFnSc6	teplota
-	-	kIx~	-
<g/>
+	+	kIx~	+
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
°	°	k?	°
<g/>
C	C	kA	C
severního	severní	k2eAgInSc2d1	severní
Atlantiku	Atlantik	k1gInSc2	Atlantik
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
ledovců	ledovec	k1gInPc2	ledovec
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
k	k	k7c3	k
úmrtí	úmrtí	k1gNnSc3	úmrtí
na	na	k7c4	na
podchlazení	podchlazení	k1gNnSc4	podchlazení
dochází	docházet	k5eAaImIp3nS	docházet
během	během	k7c2	během
15	[number]	k4	15
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
oběťmi	oběť	k1gFnPc7	oběť
byli	být	k5eAaImAgMnP	být
kapitán	kapitán	k1gMnSc1	kapitán
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
vrchní	vrchní	k2eAgMnSc1d1	vrchní
důstojník	důstojník	k1gMnSc1	důstojník
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
a	a	k8xC	a
šestý	šestý	k4xOgMnSc1	šestý
důstojník	důstojník	k1gMnSc1	důstojník
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
šéfkonstruktér	šéfkonstruktér	k1gMnSc1	šéfkonstruktér
lodi	loď	k1gFnSc2	loď
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
8	[number]	k4	8
spolupracovníky	spolupracovník	k1gMnPc7	spolupracovník
z	z	k7c2	z
loděnice	loděnice	k1gFnSc2	loděnice
<g/>
,	,	kIx,	,
vrchní	vrchní	k2eAgMnSc1d1	vrchní
strojmistr	strojmistr	k1gMnSc1	strojmistr
se	s	k7c7	s
všemi	všecek	k3xTgMnPc7	všecek
strojníky	strojník	k1gMnPc7	strojník
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
členové	člen	k1gMnPc1	člen
posádky	posádka	k1gFnSc2	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Zemřela	zemřít	k5eAaPmAgFnS	zemřít
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
57	[number]	k4	57
milionářů	milionář	k1gMnPc2	milionář
cestujících	cestující	k1gMnPc6	cestující
první	první	k4xOgFnSc7	první
třídou	třída	k1gFnSc7	třída
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
multimilionářů	multimilionář	k1gMnPc2	multimilionář
Johna	John	k1gMnSc2	John
Jacoba	Jacoba	k1gFnSc1	Jacoba
Astora	Astora	k1gFnSc1	Astora
<g/>
,	,	kIx,	,
Benjamina	Benjamin	k1gMnSc2	Benjamin
Guggenheima	Guggenheim	k1gMnSc2	Guggenheim
<g/>
,	,	kIx,	,
Idy	Ida	k1gFnSc2	Ida
a	a	k8xC	a
Isidora	Isidor	k1gMnSc2	Isidor
Strausových	Strausový	k2eAgFnPc2d1	Strausová
a	a	k8xC	a
stovky	stovka	k1gFnPc4	stovka
dalších	další	k2eAgMnPc2d1	další
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chladných	chladný	k2eAgFnPc6d1	chladná
vodách	voda	k1gFnPc6	voda
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
i	i	k9	i
všech	všecek	k3xTgInPc2	všecek
5	[number]	k4	5
poštovních	poštovní	k2eAgMnPc2d1	poštovní
úředníků	úředník	k1gMnPc2	úředník
<g/>
;	;	kIx,	;
W.	W.	kA	W.
L.	L.	kA	L.
Gwynn	Gwynn	k1gMnSc1	Gwynn
<g/>
,	,	kIx,	,
J.	J.	kA	J.
S.	S.	kA	S.
March	March	k1gMnSc1	March
<g/>
,	,	kIx,	,
J.	J.	kA	J.
R.	R.	kA	R.
J.	J.	kA	J.
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
J.	J.	kA	J.
B.	B.	kA	B.
Williamson	Williamson	k1gInSc4	Williamson
a	a	k8xC	a
O.	O.	kA	O.
S.	S.	kA	S.
Woody	Wooda	k1gMnSc2	Wooda
<g/>
,	,	kIx,	,
celý	celý	k2eAgInSc1d1	celý
lodní	lodní	k2eAgInSc1d1	lodní
orchestr	orchestr	k1gInSc1	orchestr
<g/>
;	;	kIx,	;
T.	T.	kA	T.
R.	R.	kA	R.
Brailey	Brailea	k1gMnSc2	Brailea
<g/>
,	,	kIx,	,
R.	R.	kA	R.
M.	M.	kA	M.
Bricoux	Bricoux	k1gInSc1	Bricoux
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
J.	J.	kA	J.
F.	F.	kA	F.
P.	P.	kA	P.
Clarke	Clark	k1gMnSc2	Clark
<g/>
,	,	kIx,	,
W.	W.	kA	W.
H.	H.	kA	H.
Hartley	Hartlea	k1gMnSc2	Hartlea
<g/>
,	,	kIx,	,
J.	J.	kA	J.
L.	L.	kA	L.
Hume	Hum	k1gMnSc2	Hum
<g/>
,	,	kIx,	,
G.	G.	kA	G.
A.	A.	kA	A.
Krins	Krinsa	k1gFnPc2	Krinsa
<g/>
,	,	kIx,	,
P.	P.	kA	P.
C.	C.	kA	C.
Taylor	Taylor	k1gMnSc1	Taylor
<g/>
,	,	kIx,	,
J.	J.	kA	J.
W.	W.	kA	W.
Woodward	Woodward	k1gInSc4	Woodward
i	i	k8xC	i
J.	J.	kA	J.
G.	G.	kA	G.
Phillips	Phillipsa	k1gFnPc2	Phillipsa
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
telegrafistů	telegrafista	k1gMnPc2	telegrafista
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
do	do	k7c2	do
poslední	poslední	k2eAgFnSc2d1	poslední
chvíle	chvíle	k1gFnSc2	chvíle
rádiem	rádio	k1gNnSc7	rádio
žádali	žádat	k5eAaImAgMnP	žádat
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
přeživší	přeživší	k2eAgInSc1d1	přeživší
z	z	k7c2	z
Titanicu	Titanicus	k1gInSc2	Titanicus
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
na	na	k7c4	na
událost	událost	k1gFnSc4	událost
vzpomínky	vzpomínka	k1gFnSc2	vzpomínka
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
tehdy	tehdy	k6eAd1	tehdy
sedmiletá	sedmiletý	k2eAgFnSc1d1	sedmiletá
holčička	holčička	k1gFnSc1	holčička
Eva	Eva	k1gFnSc1	Eva
Hartová	Hartová	k1gFnSc1	Hartová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zemřela	zemřít	k5eAaPmAgFnS	zemřít
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
91	[number]	k4	91
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1996	[number]	k4	1996
(	(	kIx(	(
<g/>
rok	rok	k1gInSc4	rok
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
oskarový	oskarový	k2eAgInSc1d1	oskarový
film	film	k1gInSc1	film
založený	založený	k2eAgInSc1d1	založený
na	na	k7c4	na
vzpomínání	vzpomínání	k1gNnSc4	vzpomínání
poslední	poslední	k2eAgFnSc2d1	poslední
přeživší	přeživší	k2eAgFnSc2d1	přeživší
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
tehdy	tehdy	k6eAd1	tehdy
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
17	[number]	k4	17
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
ještě	ještě	k9	ještě
sedm	sedm	k4xCc1	sedm
dětí	dítě	k1gFnPc2	dítě
mladších	mladý	k2eAgMnPc2d2	mladší
než	než	k8xS	než
Hartová	Hartová	k1gFnSc1	Hartová
<g/>
,	,	kIx,	,
ty	ty	k3xPp2nSc1	ty
si	se	k3xPyFc3	se
ale	ale	k9	ale
na	na	k7c4	na
událost	událost	k1gFnSc4	událost
vědomě	vědomě	k6eAd1	vědomě
nepamatovaly	pamatovat	k5eNaImAgInP	pamatovat
<g/>
:	:	kIx,	:
Edith	Edith	k1gInSc1	Edith
Brown	Brown	k1gMnSc1	Brown
Haismanová	Haismanová	k1gFnSc1	Haismanová
<g/>
,	,	kIx,	,
Barbara	Barbara	k1gFnSc1	Barbara
Westová	Westový	k2eAgFnSc1d1	Westová
a	a	k8xC	a
Millvina	Millvin	k2eAgFnSc1d1	Millvina
Deanová	Deanová	k1gFnSc1	Deanová
(	(	kIx(	(
<g/>
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Michael	Michael	k1gMnSc1	Michael
Navratil	Navratil	k1gMnSc2	Navratil
<g/>
,	,	kIx,	,
Louise	Louis	k1gMnSc2	Louis
Larocheová	Larocheová	k1gFnSc1	Larocheová
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Eleanor	Eleanor	k1gMnSc1	Eleanor
Shumanová	Shumanová	k1gFnSc1	Shumanová
<g/>
,	,	kIx,	,
Winnifred	Winnifred	k1gMnSc1	Winnifred
Tongerloo	Tongerloo	k1gMnSc1	Tongerloo
a	a	k8xC	a
Lillian	Lilliana	k1gFnPc2	Lilliana
Asplundová	Asplundový	k2eAgFnSc1d1	Asplundový
ze	z	k7c2	z
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nejmladší	mladý	k2eAgMnSc1d3	nejmladší
a	a	k8xC	a
současně	současně	k6eAd1	současně
poslední	poslední	k2eAgMnSc1d1	poslední
přeživší	přeživší	k2eAgMnSc1d1	přeživší
cestující	cestující	k1gMnSc1	cestující
Titaniku	Titanic	k1gInSc2	Titanic
<g/>
,	,	kIx,	,
Millvina	Millvin	k2eAgFnSc1d1	Millvina
Deanová	Deanová	k1gFnSc1	Deanová
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1912	[number]	k4	1912
a	a	k8xC	a
v	v	k7c6	v
době	doba	k1gFnSc6	doba
plavby	plavba	k1gFnPc1	plavba
jí	on	k3xPp3gFnSc3	on
byly	být	k5eAaImAgFnP	být
2	[number]	k4	2
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Zemřela	zemřít	k5eAaPmAgFnS	zemřít
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
Carpathia	Carpathia	k1gFnSc1	Carpathia
se	s	k7c7	s
zachráněnými	zachráněná	k1gFnPc7	zachráněná
doplula	doplout	k5eAaPmAgFnS	doplout
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
rozsahu	rozsah	k1gInSc3	rozsah
katastrofy	katastrofa	k1gFnSc2	katastrofa
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c6	o
vyšetření	vyšetření	k1gNnSc6	vyšetření
celé	celý	k2eAgFnSc2d1	celá
události	událost	k1gFnSc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Senát	senát	k1gInSc1	senát
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
zahájil	zahájit	k5eAaPmAgInS	zahájit
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
následující	následující	k2eAgInSc1d1	následující
den	den	k1gInSc1	den
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1912	[number]	k4	1912
co	co	k8xS	co
Carpathia	Carpathia	k1gFnSc1	Carpathia
připlula	připlout	k5eAaPmAgFnS	připlout
<g/>
.	.	kIx.	.
</s>
<s>
Vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
vedl	vést	k5eAaImAgMnS	vést
senátor	senátor	k1gMnSc1	senátor
William	William	k1gInSc4	William
Alden	Alden	k2eAgInSc4d1	Alden
Smith	Smith	k1gInSc4	Smith
až	až	k6eAd1	až
do	do	k7c2	do
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
byly	být	k5eAaImAgFnP	být
zapsány	zapsán	k2eAgFnPc1d1	zapsána
výpovědi	výpověď	k1gFnPc1	výpověď
všech	všecek	k3xTgFnPc2	všecek
přeživších	přeživší	k2eAgFnPc2d1	přeživší
a	a	k8xC	a
ostatní	ostatní	k2eAgNnSc4d1	ostatní
svědectví	svědectví	k1gNnSc4	svědectví
s	s	k7c7	s
případem	případ	k1gInSc7	případ
související	související	k2eAgMnSc1d1	související
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
podání	podání	k1gNnSc1	podání
svědectví	svědectví	k1gNnSc2	svědectví
bylo	být	k5eAaImAgNnS	být
britským	britský	k2eAgMnSc7d1	britský
občanům	občan	k1gMnPc3	občan
povoleno	povolit	k5eAaPmNgNnS	povolit
opustit	opustit	k5eAaPmF	opustit
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
tím	ten	k3xDgNnSc7	ten
britský	britský	k2eAgInSc1d1	britský
tisk	tisk	k1gInSc1	tisk
očernil	očernit	k5eAaPmAgInS	očernit
senátora	senátor	k1gMnSc4	senátor
Smitha	Smith	k1gMnSc4	Smith
jako	jako	k8xC	jako
osobu	osoba	k1gFnSc4	osoba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
z	z	k7c2	z
neštěstí	neštěstí	k1gNnSc2	neštěstí
chtěla	chtít	k5eAaImAgFnS	chtít
vytlouci	vytlouct	k5eAaPmF	vytlouct
politický	politický	k2eAgInSc4d1	politický
kapitál	kapitál	k1gInSc4	kapitál
<g/>
.	.	kIx.	.
</s>
<s>
Smith	Smith	k1gMnSc1	Smith
však	však	k9	však
měl	mít	k5eAaImAgMnS	mít
spíše	spíše	k9	spíše
pověst	pověst	k1gFnSc4	pověst
zastánce	zastánce	k1gMnSc2	zastánce
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
na	na	k7c6	na
železnici	železnice	k1gFnSc6	železnice
<g/>
,	,	kIx,	,
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c7	mezi
společností	společnost	k1gFnSc7	společnost
Railroad	Railroad	k1gInSc4	Railroad
Tycoon	Tycoon	k1gNnSc1	Tycoon
a	a	k8xC	a
Titanicem	Titanic	k1gMnSc7	Titanic
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
měly	mít	k5eAaImAgFnP	mít
společného	společný	k2eAgMnSc4d1	společný
majitele	majitel	k1gMnSc4	majitel
<g/>
:	:	kIx,	:
J.	J.	kA	J.
P.	P.	kA	P.
Morgana	morgan	k1gMnSc2	morgan
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
na	na	k7c6	na
britské	britský	k2eAgFnSc6d1	britská
straně	strana	k1gFnSc6	strana
byla	být	k5eAaImAgFnS	být
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
vyšetřovací	vyšetřovací	k2eAgFnSc1d1	vyšetřovací
komise	komise	k1gFnSc1	komise
British	British	k1gMnSc1	British
Board	Board	k1gMnSc1	Board
of	of	k?	of
Trade	Trad	k1gMnSc5	Trad
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vedl	vést	k5eAaImAgMnS	vést
lord	lord	k1gMnSc1	lord
Mersey	Mersea	k1gFnSc2	Mersea
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
pracovala	pracovat	k5eAaImAgFnS	pracovat
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1912	[number]	k4	1912
do	do	k7c2	do
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
vyslechla	vyslechnout	k5eAaPmAgFnS	vyslechnout
přeživší	přeživší	k2eAgFnSc4d1	přeživší
cestující	cestující	k1gFnSc4	cestující
i	i	k8xC	i
členy	člen	k1gMnPc4	člen
posádky	posádka	k1gFnSc2	posádka
Titanicu	Titanicus	k1gInSc2	Titanicus
<g/>
,	,	kIx,	,
kapitána	kapitán	k1gMnSc2	kapitán
Arthura	Arthur	k1gMnSc2	Arthur
Rostrona	Rostron	k1gMnSc2	Rostron
lodi	loď	k1gFnSc2	loď
Carpathia	Carpathius	k1gMnSc2	Carpathius
<g/>
,	,	kIx,	,
posádku	posádka	k1gFnSc4	posádka
lodě	loď	k1gFnSc2	loď
společnosti	společnost	k1gFnSc2	společnost
Leyland	Leylanda	k1gFnPc2	Leylanda
Line	linout	k5eAaImIp3nS	linout
Californian	Californian	k1gInSc1	Californian
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Smith	Smith	k1gMnSc1	Smith
-	-	kIx~	-
senátor	senátor	k1gMnSc1	senátor
vnitrozemského	vnitrozemský	k2eAgInSc2d1	vnitrozemský
státu	stát	k1gInSc2	stát
Unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
Michigan	Michigan	k1gInSc1	Michigan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vycítil	vycítit	k5eAaPmAgMnS	vycítit
možnost	možnost	k1gFnSc4	možnost
se	se	k3xPyFc4	se
zviditelnit	zviditelnit	k5eAaPmF	zviditelnit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
učinit	učinit	k5eAaImF	učinit
hlavního	hlavní	k2eAgMnSc4d1	hlavní
viníka	viník	k1gMnSc4	viník
katastrofy	katastrofa	k1gFnSc2	katastrofa
z	z	k7c2	z
Bruce	Bruec	k1gInSc2	Bruec
Ismaye	Ismay	k1gInSc2	Ismay
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
formální	formální	k2eAgFnSc7d1	formální
hlavou	hlava	k1gFnSc7	hlava
White	Whit	k1gInSc5	Whit
Star	Star	kA	Star
Line	linout	k5eAaImIp3nS	linout
vybudované	vybudovaný	k2eAgNnSc1d1	vybudované
jeho	jeho	k3xOp3gMnPc7	jeho
otcem	otec	k1gMnSc7	otec
<g/>
.	.	kIx.	.
</s>
<s>
White	Whiit	k5eAaBmRp2nP	Whiit
Star	star	k1gFnSc1	star
Line	linout	k5eAaImIp3nS	linout
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
již	již	k6eAd1	již
dávno	dávno	k6eAd1	dávno
nepatřila	patřit	k5eNaImAgFnS	patřit
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
společnost	společnost	k1gFnSc1	společnost
dávno	dávno	k6eAd1	dávno
ve	v	k7c6	v
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
amerického	americký	k2eAgInSc2d1	americký
Morganova	morganův	k2eAgInSc2d1	morganův
trustu	trust	k1gInSc2	trust
<g/>
.	.	kIx.	.
</s>
<s>
Ismayovou	Ismayový	k2eAgFnSc7d1	Ismayový
vinou	vina	k1gFnSc7	vina
bylo	být	k5eAaImAgNnS	být
pak	pak	k6eAd1	pak
patrně	patrně	k6eAd1	patrně
pouze	pouze	k6eAd1	pouze
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
přežil	přežít	k5eAaPmAgInS	přežít
-	-	kIx~	-
média	médium	k1gNnPc1	médium
jej	on	k3xPp3gNnSc4	on
vykreslila	vykreslit	k5eAaPmAgFnS	vykreslit
jako	jako	k9	jako
sobeckého	sobecký	k2eAgMnSc4d1	sobecký
britského	britský	k2eAgMnSc4d1	britský
miliardáře	miliardář	k1gMnSc4	miliardář
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zabral	zabrat	k5eAaPmAgMnS	zabrat
místo	místo	k6eAd1	místo
dětem	dítě	k1gFnPc3	dítě
a	a	k8xC	a
ženám	žena	k1gFnPc3	žena
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zachoval	zachovat	k5eAaPmAgInS	zachovat
vlastní	vlastní	k2eAgInSc4d1	vlastní
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Šek	šek	k1gMnSc1	šek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
Ismay	Ismay	k1gInPc4	Ismay
vystavil	vystavit	k5eAaPmAgMnS	vystavit
všem	všecek	k3xTgMnPc3	všecek
námořníkům	námořník	k1gMnPc3	námořník
v	v	k7c6	v
záchranném	záchranný	k2eAgInSc6d1	záchranný
člunu	člun	k1gInSc6	člun
"	"	kIx"	"
<g/>
na	na	k7c4	na
úhradu	úhrada	k1gFnSc4	úhrada
výstroje	výstroj	k1gFnSc2	výstroj
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
dezinterpretován	dezinterpretovat	k5eAaImNgInS	dezinterpretovat
jako	jako	k9	jako
úplatek	úplatek	k1gInSc1	úplatek
<g/>
.	.	kIx.	.
</s>
<s>
Bruce	Bruce	k1gMnSc1	Bruce
Ismay	Ismaa	k1gMnSc2	Ismaa
byl	být	k5eAaImAgInS	být
flegmatický	flegmatický	k2eAgMnSc1d1	flegmatický
<g/>
,	,	kIx,	,
znuděný	znuděný	k2eAgMnSc1d1	znuděný
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
absolutně	absolutně	k6eAd1	absolutně
nezajímal	zajímat	k5eNaImAgMnS	zajímat
o	o	k7c4	o
lodě	loď	k1gFnPc4	loď
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc3	jejich
navigaci	navigace	k1gFnSc3	navigace
-	-	kIx~	-
jeho	jeho	k3xOp3gNnSc1	jeho
zasahování	zasahování	k1gNnSc1	zasahování
do	do	k7c2	do
vedení	vedení	k1gNnSc2	vedení
lodi	loď	k1gFnSc2	loď
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
absurdní	absurdní	k2eAgInSc1d1	absurdní
výmysl	výmysl	k1gInSc4	výmysl
živený	živený	k2eAgInSc4d1	živený
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
jakým	jaký	k3yQgInSc7	jaký
je	být	k5eAaImIp3nS	být
nacistický	nacistický	k2eAgInSc1d1	nacistický
Titanic	Titanic	k1gInSc1	Titanic
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
nástup	nástup	k1gInSc1	nástup
do	do	k7c2	do
záchraného	záchraný	k2eAgInSc2d1	záchraný
člunu	člun	k1gInSc2	člun
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
obětavě	obětavě	k6eAd1	obětavě
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
posádce	posádka	k1gFnSc3	posádka
jej	on	k3xPp3gMnSc4	on
naplnit	naplnit	k5eAaPmF	naplnit
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
neviděl	vidět	k5eNaImAgMnS	vidět
nikoho	nikdo	k3yNnSc4	nikdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
upřednostněn	upřednostnit	k5eAaPmNgInS	upřednostnit
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
nevykazuje	vykazovat	k5eNaImIp3nS	vykazovat
známky	známka	k1gFnPc4	známka
nějakého	nějaký	k3yIgNnSc2	nějaký
sobectví	sobectví	k1gNnSc2	sobectví
<g/>
.	.	kIx.	.
</s>
<s>
Ismay	Ismaa	k1gFnPc1	Ismaa
se	se	k3xPyFc4	se
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
stal	stát	k5eAaPmAgMnS	stát
obětním	obětní	k2eAgMnSc7d1	obětní
beránkem	beránek	k1gMnSc7	beránek
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgInS	být
dokonce	dokonce	k9	dokonce
zadržen	zadržet	k5eAaPmNgMnS	zadržet
<g/>
)	)	kIx)	)
-	-	kIx~	-
ovšem	ovšem	k9	ovšem
neprávem	neprávo	k1gNnSc7	neprávo
<g/>
.	.	kIx.	.
</s>
<s>
Podobného	podobný	k2eAgInSc2d1	podobný
osudu	osud	k1gInSc2	osud
se	se	k3xPyFc4	se
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
později	pozdě	k6eAd2	pozdě
dočkal	dočkat	k5eAaPmAgMnS	dočkat
kapitán	kapitán	k1gMnSc1	kapitán
SS	SS	kA	SS
Califorian	Califorian	k1gMnSc1	Califorian
Stanley	Stanlea	k1gFnSc2	Stanlea
Lord	lord	k1gMnSc1	lord
-	-	kIx~	-
který	který	k3yQgMnSc1	který
dokonce	dokonce	k9	dokonce
napsal	napsat	k5eAaPmAgMnS	napsat
několik	několik	k4yIc4	několik
knih	kniha	k1gFnPc2	kniha
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
obhajobu	obhajoba	k1gFnSc4	obhajoba
<g/>
.	.	kIx.	.
</s>
<s>
Případ	případ	k1gInSc1	případ
lodi	loď	k1gFnSc2	loď
SS	SS	kA	SS
Californian	Californian	k1gInSc4	Californian
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
součástí	součást	k1gFnSc7	součást
šetření	šetření	k1gNnSc2	šetření
britské	britský	k2eAgFnSc2d1	britská
vyšetřovací	vyšetřovací	k2eAgFnSc2d1	vyšetřovací
komise	komise	k1gFnSc2	komise
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
podaných	podaný	k2eAgNnPc2d1	podané
svědectví	svědectví	k1gNnPc2	svědectví
kapitána	kapitán	k1gMnSc2	kapitán
Stanley	Stanlea	k1gMnSc2	Stanlea
Lorda	lord	k1gMnSc2	lord
a	a	k8xC	a
třetího	třetí	k4xOgMnSc2	třetí
důstojníka	důstojník	k1gMnSc2	důstojník
C.	C.	kA	C.
V.	V.	kA	V.
Grovese	Grovese	k1gFnSc1	Grovese
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
paluby	paluba	k1gFnSc2	paluba
lodi	loď	k1gFnSc2	loď
byla	být	k5eAaImAgFnS	být
jižním	jižní	k2eAgInSc7d1	jižní
směrem	směr	k1gInSc7	směr
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
světla	světlo	k1gNnSc2	světlo
jiné	jiný	k2eAgFnPc1d1	jiná
lodě	loď	k1gFnPc1	loď
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
osobního	osobní	k2eAgInSc2d1	osobní
parníku	parník	k1gInSc2	parník
<g/>
.	.	kIx.	.
</s>
<s>
C.	C.	kA	C.
V.	V.	kA	V.
Groves	Groves	k1gMnSc1	Groves
v	v	k7c6	v
23	[number]	k4	23
<g/>
:	:	kIx,	:
<g/>
10	[number]	k4	10
ukončil	ukončit	k5eAaPmAgMnS	ukončit
službu	služba	k1gFnSc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
23	[number]	k4	23
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
se	se	k3xPyFc4	se
radista	radista	k1gMnSc1	radista
Cyril	Cyril	k1gMnSc1	Cyril
Evans	Evans	k1gInSc4	Evans
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
parníku	parník	k1gInSc2	parník
Californian	Californiana	k1gFnPc2	Californiana
snažil	snažit	k5eAaImAgMnS	snažit
Titanic	Titanic	k1gInSc4	Titanic
varovat	varovat	k5eAaImF	varovat
před	před	k7c7	před
ledovci	ledovec	k1gInPc7	ledovec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ten	ten	k3xDgMnSc1	ten
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
varování	varování	k1gNnSc4	varování
nereagoval	reagovat	k5eNaBmAgMnS	reagovat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
paluby	paluba	k1gFnSc2	paluba
Californianu	Californian	k1gMnSc3	Californian
pozorovali	pozorovat	k5eAaImAgMnP	pozorovat
loď	loď	k1gFnSc4	loď
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
světla	světlo	k1gNnPc1	světlo
poblikávala	poblikávat	k5eAaImAgNnP	poblikávat
a	a	k8xC	a
zdálo	zdát	k5eAaImAgNnS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
otočila	otočit	k5eAaPmAgFnS	otočit
zádí	záď	k1gFnSc7	záď
k	k	k7c3	k
pozorovatelům	pozorovatel	k1gMnPc3	pozorovatel
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
na	na	k7c4	na
rozkaz	rozkaz	k1gInSc4	rozkaz
kapitána	kapitán	k1gMnSc2	kapitán
S.	S.	kA	S.
Lorda	lord	k1gMnSc2	lord
byly	být	k5eAaImAgFnP	být
z	z	k7c2	z
Californianu	Californian	k1gInSc2	Californian
odvysílány	odvysílán	k2eAgFnPc1d1	odvysílána
zprávy	zpráva	k1gFnPc1	zpráva
pomocí	pomocí	k7c2	pomocí
Morseovy	Morseův	k2eAgFnSc2d1	Morseova
lampy	lampa	k1gFnSc2	lampa
v	v	k7c6	v
čase	čas	k1gInSc6	čas
23	[number]	k4	23
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
a	a	k8xC	a
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tyto	tento	k3xDgFnPc4	tento
zprávy	zpráva	k1gFnPc4	zpráva
však	však	k9	však
neznámá	známý	k2eNgFnSc1d1	neznámá
loď	loď	k1gFnSc1	loď
neodpověděla	odpovědět	k5eNaPmAgFnS	odpovědět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zápisu	zápis	k1gInSc6	zápis
je	být	k5eAaImIp3nS	být
taktéž	taktéž	k?	taktéž
uvedeno	uvést	k5eAaPmNgNnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
Morseova	Morseův	k2eAgFnSc1d1	Morseova
lampa	lampa	k1gFnSc1	lampa
na	na	k7c4	na
Californianu	Californiana	k1gFnSc4	Californiana
byla	být	k5eAaImAgFnS	být
viditelná	viditelný	k2eAgFnSc1d1	viditelná
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
4	[number]	k4	4
mil	míle	k1gFnPc2	míle
(	(	kIx(	(
<g/>
asi	asi	k9	asi
6	[number]	k4	6
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
na	na	k7c6	na
Titanicu	Titanicus	k1gInSc6	Titanicus
nemohla	moct	k5eNaImAgFnS	moct
být	být	k5eAaImF	být
vidět	vidět	k5eAaImF	vidět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
23	[number]	k4	23
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
šel	jít	k5eAaImAgMnS	jít
kapitán	kapitán	k1gMnSc1	kapitán
Lord	lord	k1gMnSc1	lord
spát	spát	k5eAaImF	spát
a	a	k8xC	a
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
zůstal	zůstat	k5eAaPmAgInS	zůstat
2	[number]	k4	2
<g/>
.	.	kIx.	.
důstojník	důstojník	k1gMnSc1	důstojník
Herbert	Herbert	k1gMnSc1	Herbert
Stone	ston	k1gInSc5	ston
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
15	[number]	k4	15
oznámil	oznámit	k5eAaPmAgMnS	oznámit
kapitánovi	kapitán	k1gMnSc3	kapitán
<g/>
,	,	kIx,	,
že	že	k8xS	že
zahlédl	zahlédnout	k5eAaPmAgMnS	zahlédnout
světlici	světlice	k1gFnSc4	světlice
a	a	k8xC	a
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
další	další	k2eAgFnSc6d1	další
čtyři	čtyři	k4xCgMnPc4	čtyři
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
světlice	světlice	k1gFnPc1	světlice
však	však	k9	však
měly	mít	k5eAaImAgFnP	mít
bílou	bílý	k2eAgFnSc4d1	bílá
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
podle	podle	k7c2	podle
barvy	barva	k1gFnSc2	barva
světlic	světlice	k1gFnPc2	světlice
nebylo	být	k5eNaImAgNnS	být
možno	možno	k6eAd1	možno
určit	určit	k5eAaPmF	určit
loď	loď	k1gFnSc4	loď
lodní	lodní	k2eAgFnSc2d1	lodní
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
rakety	raketa	k1gFnPc4	raketa
vystřelila	vystřelit	k5eAaPmAgFnS	vystřelit
<g/>
.	.	kIx.	.
</s>
<s>
Kapitán	kapitán	k1gMnSc1	kapitán
Lord	lord	k1gMnSc1	lord
pouze	pouze	k6eAd1	pouze
nařídil	nařídit	k5eAaPmAgMnS	nařídit
další	další	k2eAgNnSc4d1	další
sledování	sledování	k1gNnSc4	sledování
neznámé	známý	k2eNgFnSc2d1	neznámá
lodě	loď	k1gFnSc2	loď
a	a	k8xC	a
odvysílání	odvysílání	k1gNnSc2	odvysílání
další	další	k2eAgFnSc2d1	další
Morseovy	Morseův	k2eAgFnSc2d1	Morseova
zprávy	zpráva	k1gFnSc2	zpráva
lampou	lampa	k1gFnSc7	lampa
a	a	k8xC	a
šel	jít	k5eAaImAgInS	jít
opět	opět	k6eAd1	opět
spát	spát	k5eAaImF	spát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
50	[number]	k4	50
byly	být	k5eAaImAgInP	být
z	z	k7c2	z
paluby	paluba	k1gFnSc2	paluba
Californianu	Californian	k1gInSc2	Californian
pozorovány	pozorován	k2eAgFnPc4d1	pozorována
další	další	k2eAgFnPc4d1	další
světlice	světlice	k1gFnPc4	světlice
<g/>
,	,	kIx,	,
důstojník	důstojník	k1gMnSc1	důstojník
Stone	ston	k1gInSc5	ston
přitom	přitom	k6eAd1	přitom
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
neznámá	známý	k2eNgFnSc1d1	neznámá
loď	loď	k1gFnSc1	loď
vypadá	vypadat	k5eAaImIp3nS	vypadat
podivně	podivně	k6eAd1	podivně
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
nakloněná	nakloněný	k2eAgFnSc1d1	nakloněná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
15	[number]	k4	15
se	se	k3xPyFc4	se
kapitán	kapitán	k1gMnSc1	kapitán
Lord	lord	k1gMnSc1	lord
opět	opět	k6eAd1	opět
dotázal	dotázat	k5eAaPmAgMnS	dotázat
na	na	k7c4	na
neznámou	známý	k2eNgFnSc4d1	neznámá
loď	loď	k1gFnSc4	loď
a	a	k8xC	a
její	její	k3xOp3gFnPc4	její
světlice	světlice	k1gFnPc4	světlice
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
dostal	dostat	k5eAaPmAgMnS	dostat
odpověď	odpověď	k1gFnSc4	odpověď
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
bílé	bílý	k2eAgFnPc1d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
Californian	Californian	k1gMnSc1	Californian
se	se	k3xPyFc4	se
do	do	k7c2	do
záchranné	záchranný	k2eAgFnSc2d1	záchranná
akce	akce	k1gFnSc2	akce
zapojil	zapojit	k5eAaPmAgInS	zapojit
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
kdy	kdy	k6eAd1	kdy
radista	radista	k1gMnSc1	radista
Cyril	Cyril	k1gMnSc1	Cyril
Furmstone	Furmston	k1gInSc5	Furmston
Evans	Evans	k1gInSc1	Evans
opět	opět	k6eAd1	opět
zapnul	zapnout	k5eAaPmAgInS	zapnout
radiostanici	radiostanice	k1gFnSc4	radiostanice
a	a	k8xC	a
Californian	Californian	k1gMnSc1	Californian
obdržel	obdržet	k5eAaPmAgMnS	obdržet
rádiový	rádiový	k2eAgInSc4d1	rádiový
příkaz	příkaz	k1gInSc4	příkaz
od	od	k7c2	od
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
George	Georg	k1gMnSc2	Georg
Stewarta	Stewart	k1gMnSc2	Stewart
k	k	k7c3	k
pomoci	pomoc	k1gFnSc3	pomoc
Titanicu	Titanicus	k1gInSc2	Titanicus
<g/>
;	;	kIx,	;
teprve	teprve	k6eAd1	teprve
tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
kapitán	kapitán	k1gMnSc1	kapitán
Lord	lord	k1gMnSc1	lord
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
o	o	k7c4	o
potopení	potopení	k1gNnSc4	potopení
Titanicu	Titanicus	k1gInSc2	Titanicus
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgNnSc1d2	pozdější
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
britské	britský	k2eAgFnSc2d1	britská
komise	komise	k1gFnSc2	komise
ztotožnilo	ztotožnit	k5eAaPmAgNnS	ztotožnit
tuto	tento	k3xDgFnSc4	tento
neznámou	známý	k2eNgFnSc4d1	neznámá
loď	loď	k1gFnSc4	loď
s	s	k7c7	s
parníkem	parník	k1gInSc7	parník
SS	SS	kA	SS
Californian	Californian	k1gInSc1	Californian
a	a	k8xC	a
komise	komise	k1gFnSc1	komise
obvinila	obvinit	k5eAaPmAgFnS	obvinit
kapitána	kapitán	k1gMnSc4	kapitán
Lorda	lord	k1gMnSc4	lord
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc4	jeho
důstojníky	důstojník	k1gMnPc4	důstojník
z	z	k7c2	z
hrubého	hrubý	k2eAgNnSc2d1	hrubé
pochybení	pochybení	k1gNnSc2	pochybení
<g/>
.	.	kIx.	.
</s>
<s>
Kapitán	kapitán	k1gMnSc1	kapitán
Lord	lord	k1gMnSc1	lord
toto	tento	k3xDgNnSc4	tento
odmítal	odmítat	k5eAaImAgMnS	odmítat
a	a	k8xC	a
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
zbytek	zbytek	k1gInSc4	zbytek
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
očistit	očistit	k5eAaPmF	očistit
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
od	od	k7c2	od
rozsudku	rozsudek	k1gInSc2	rozsudek
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mu	on	k3xPp3gMnSc3	on
zničil	zničit	k5eAaPmAgMnS	zničit
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Odmítal	odmítat	k5eAaImAgMnS	odmítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
loď	loď	k1gFnSc1	loď
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
on	on	k3xPp3gMnSc1	on
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
důstojníci	důstojník	k1gMnPc1	důstojník
viděli	vidět	k5eAaImAgMnP	vidět
z	z	k7c2	z
můstku	můstek	k1gInSc2	můstek
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Titanic	Titanic	k1gInSc1	Titanic
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
Californianem	Californian	k1gMnSc7	Californian
a	a	k8xC	a
Titanicem	Titanic	k1gMnSc7	Titanic
nacházela	nacházet	k5eAaImAgFnS	nacházet
třetí	třetí	k4xOgFnSc4	třetí
loď	loď	k1gFnSc4	loď
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
viděly	vidět	k5eAaImAgFnP	vidět
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
<g/>
,	,	kIx,	,
a	a	k8xC	a
která	který	k3yRgFnSc1	který
odplula	odplout	k5eAaPmAgFnS	odplout
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
Titanic	Titanic	k1gInSc1	Titanic
potopil	potopit	k5eAaPmAgInS	potopit
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
neexistují	existovat	k5eNaImIp3nP	existovat
přímé	přímý	k2eAgInPc4d1	přímý
důkazy	důkaz	k1gInPc4	důkaz
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
obhajobu	obhajoba	k1gFnSc4	obhajoba
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
pravděpodobnější	pravděpodobný	k2eAgMnSc1d2	pravděpodobnější
než	než	k8xS	než
závěr	závěr	k1gInSc1	závěr
vyšetřovací	vyšetřovací	k2eAgFnSc2d1	vyšetřovací
komise	komise	k1gFnSc2	komise
<g/>
.	.	kIx.	.
</s>
<s>
Chybou	chyba	k1gFnSc7	chyba
však	však	k9	však
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kapitán	kapitán	k1gMnSc1	kapitán
Lord	lord	k1gMnSc1	lord
nenechal	nechat	k5eNaPmAgMnS	nechat
vzbudit	vzbudit	k5eAaPmF	vzbudit
radistu	radista	k1gMnSc4	radista
a	a	k8xC	a
nepřikázal	přikázat	k5eNaPmAgMnS	přikázat
mu	on	k3xPp3gMnSc3	on
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
děje	děj	k1gInSc2	děj
<g/>
;	;	kIx,	;
toto	tento	k3xDgNnSc1	tento
opomenutí	opomenutí	k1gNnSc1	opomenutí
stálo	stát	k5eAaImAgNnS	stát
životy	život	k1gInPc4	život
mnoho	mnoho	k4c4	mnoho
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Parník	parník	k1gInSc1	parník
SS	SS	kA	SS
Californian	Californian	k1gInSc1	Californian
byl	být	k5eAaImAgMnS	být
nejbližší	blízký	k2eAgFnSc7d3	nejbližší
lodí	loď	k1gFnSc7	loď
k	k	k7c3	k
potápějícímu	potápějící	k2eAgMnSc3d1	potápějící
se	se	k3xPyFc4	se
Titanicu	Titanicus	k1gInSc6	Titanicus
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
19,5	[number]	k4	19,5
mil	míle	k1gFnPc2	míle
(	(	kIx(	(
<g/>
31,4	[number]	k4	31,4
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnoho	mnoho	k4c1	mnoho
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
předpisů	předpis	k1gInPc2	předpis
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
zastaralých	zastaralý	k2eAgInPc2d1	zastaralý
a	a	k8xC	a
nevyhovujících	vyhovující	k2eNgInPc2d1	nevyhovující
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
aktualizaci	aktualizace	k1gFnSc3	aktualizace
předpisů	předpis	k1gInPc2	předpis
podle	podle	k7c2	podle
nových	nový	k2eAgFnPc2d1	nová
potřeb	potřeba	k1gFnPc2	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
řadu	řada	k1gFnSc4	řada
inovovaných	inovovaný	k2eAgInPc2d1	inovovaný
a	a	k8xC	a
nových	nový	k2eAgInPc2d1	nový
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
předpisů	předpis	k1gInPc2	předpis
pro	pro	k7c4	pro
záoceánské	záoceánský	k2eAgFnPc4d1	záoceánská
lodě	loď	k1gFnPc4	loď
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
ukládaly	ukládat	k5eAaImAgFnP	ukládat
vylepšení	vylepšení	k1gNnSc4	vylepšení
vodotěsných	vodotěsný	k2eAgFnPc2d1	vodotěsná
přepážek	přepážka	k1gFnPc2	přepážka
v	v	k7c6	v
trupu	trup	k1gInSc6	trup
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
konstrukci	konstrukce	k1gFnSc4	konstrukce
<g/>
,	,	kIx,	,
technickou	technický	k2eAgFnSc4d1	technická
změnu	změna	k1gFnSc4	změna
pro	pro	k7c4	pro
nástup	nástup	k1gInSc4	nástup
a	a	k8xC	a
výstup	výstup	k1gInSc4	výstup
cestujících	cestující	k1gMnPc2	cestující
<g/>
,	,	kIx,	,
nové	nový	k2eAgInPc1d1	nový
požadavky	požadavek	k1gInPc1	požadavek
na	na	k7c4	na
záchranné	záchranný	k2eAgInPc4d1	záchranný
čluny	člun	k1gInPc4	člun
<g/>
,	,	kIx,	,
lepší	dobrý	k2eAgNnSc4d2	lepší
provedení	provedení	k1gNnSc4	provedení
a	a	k8xC	a
funkčnost	funkčnost	k1gFnSc4	funkčnost
záchranných	záchranný	k2eAgFnPc2d1	záchranná
vest	vesta	k1gFnPc2	vesta
<g/>
,	,	kIx,	,
pořádání	pořádání	k1gNnSc1	pořádání
cvičení	cvičení	k1gNnPc2	cvičení
k	k	k7c3	k
opuštění	opuštění	k1gNnSc3	opuštění
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
lepší	dobrý	k2eAgNnSc1d2	lepší
předávání	předávání	k1gNnSc1	předávání
zpráv	zpráva	k1gFnPc2	zpráva
a	a	k8xC	a
informací	informace	k1gFnPc2	informace
<g/>
,	,	kIx,	,
radiokomunikační	radiokomunikační	k2eAgInPc4d1	radiokomunikační
zákony	zákon	k1gInPc4	zákon
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Vyšetřovatelé	vyšetřovatel	k1gMnPc1	vyšetřovatel
také	také	k9	také
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
Titanicu	Titanicus	k1gInSc6	Titanicus
byly	být	k5eAaImAgInP	být
kapacitně	kapacitně	k6eAd1	kapacitně
odpovídající	odpovídající	k2eAgInPc1d1	odpovídající
záchranné	záchranný	k2eAgInPc1d1	záchranný
čluny	člun	k1gInPc1	člun
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
cestující	cestující	k1gFnSc4	cestující
první	první	k4xOgFnSc2	první
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
pro	pro	k7c4	pro
nižší	nízký	k2eAgFnPc4d2	nižší
třídy	třída	k1gFnPc4	třída
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vyšetřování	vyšetřování	k1gNnSc6	vyšetřování
se	se	k3xPyFc4	se
také	také	k9	také
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
cestující	cestující	k1gMnSc1	cestující
třetí	třetí	k4xOgFnSc2	třetí
třídy	třída	k1gFnSc2	třída
ani	ani	k8xC	ani
nevěděli	vědět	k5eNaImAgMnP	vědět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
Titanicu	Titanicus	k1gInSc2	Titanicus
záchranné	záchranný	k2eAgInPc1d1	záchranný
čluny	člun	k1gInPc1	člun
nacházejí	nacházet	k5eAaImIp3nP	nacházet
<g/>
,	,	kIx,	,
a	a	k8xC	a
ani	ani	k8xC	ani
nevěděli	vědět	k5eNaImAgMnP	vědět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
na	na	k7c4	na
horní	horní	k2eAgFnSc4d1	horní
<g/>
,	,	kIx,	,
člunovou	člunový	k2eAgFnSc4d1	člunová
palubu	paluba	k1gFnSc4	paluba
<g/>
.	.	kIx.	.
</s>
<s>
Imigrační	imigrační	k2eAgInPc1d1	imigrační
zákony	zákon	k1gInPc1	zákon
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
taktéž	taktéž	k?	taktéž
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nařizovaly	nařizovat	k5eAaImAgFnP	nařizovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
cestující	cestující	k1gMnSc1	cestující
třetí	třetí	k4xOgFnSc2	třetí
třídy	třída	k1gFnSc2	třída
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
odděleni	oddělit	k5eAaPmNgMnP	oddělit
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
cestujících	cestující	k1gMnPc2	cestující
<g/>
;	;	kIx,	;
toto	tento	k3xDgNnSc1	tento
pak	pak	k6eAd1	pak
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
ztíženému	ztížený	k2eAgInSc3d1	ztížený
přístupu	přístup	k1gInSc3	přístup
z	z	k7c2	z
třetí	třetí	k4xOgFnSc2	třetí
třídy	třída	k1gFnSc2	třída
na	na	k7c4	na
horní	horní	k2eAgFnPc4d1	horní
paluby	paluba	k1gFnPc4	paluba
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
evidentní	evidentní	k2eAgNnSc1d1	evidentní
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavní	hlavní	k2eAgFnSc7d1	hlavní
příčinou	příčina	k1gFnSc7	příčina
ztroskotání	ztroskotání	k1gNnSc2	ztroskotání
Titanicu	Titanicus	k1gInSc6	Titanicus
byla	být	k5eAaImAgFnS	být
kolize	kolize	k1gFnSc1	kolize
s	s	k7c7	s
ledovcem	ledovec	k1gInSc7	ledovec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
způsobil	způsobit	k5eAaPmAgInS	způsobit
trhlinu	trhlina	k1gFnSc4	trhlina
pod	pod	k7c7	pod
čarou	čára	k1gFnSc7	čára
ponoru	ponor	k1gInSc2	ponor
procházející	procházející	k2eAgFnSc1d1	procházející
přes	přes	k7c4	přes
několik	několik	k4yIc4	několik
vodotěsných	vodotěsný	k2eAgFnPc2d1	vodotěsná
komor	komora	k1gFnPc2	komora
v	v	k7c6	v
přední	přední	k2eAgFnSc6d1	přední
části	část	k1gFnSc6	část
na	na	k7c6	na
pravoboku	pravobok	k1gInSc6	pravobok
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
fakt	fakt	k1gInSc1	fakt
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
důsledkem	důsledek	k1gInSc7	důsledek
předcházejících	předcházející	k2eAgFnPc2d1	předcházející
událostí	událost	k1gFnPc2	událost
i	i	k8xC	i
konstrukce	konstrukce	k1gFnSc2	konstrukce
Titanicu	Titanicus	k1gInSc2	Titanicus
samotného	samotný	k2eAgInSc2d1	samotný
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
potopení	potopení	k1gNnSc3	potopení
Titanicu	Titanicus	k1gInSc2	Titanicus
mohla	moct	k5eAaImAgFnS	moct
značnou	značný	k2eAgFnSc7d1	značná
mírou	míra	k1gFnSc7	míra
přispět	přispět	k5eAaPmF	přispět
i	i	k9	i
nezvyklá	zvyklý	k2eNgFnSc1d1	nezvyklá
konstelace	konstelace	k1gFnSc1	konstelace
Měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
Slunce	slunce	k1gNnSc2	slunce
vůči	vůči	k7c3	vůči
Zemi	zem	k1gFnSc3	zem
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
silný	silný	k2eAgInSc4d1	silný
příliv	příliv	k1gInSc4	příliv
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgInSc4d1	umožňující
průnik	průnik	k1gInSc4	průnik
statisticky	statisticky	k6eAd1	statisticky
výrazně	výrazně	k6eAd1	výrazně
nadprůměrného	nadprůměrný	k2eAgNnSc2d1	nadprůměrné
množství	množství	k1gNnSc2	množství
velkých	velký	k2eAgInPc2d1	velký
ledovců	ledovec	k1gInPc2	ledovec
do	do	k7c2	do
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
úvahy	úvaha	k1gFnPc1	úvaha
o	o	k7c6	o
trhlině	trhlina	k1gFnSc6	trhlina
způsobené	způsobený	k2eAgFnSc6d1	způsobená
ledovcem	ledovec	k1gInSc7	ledovec
byly	být	k5eAaImAgFnP	být
takové	takový	k3xDgNnSc4	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
ledovec	ledovec	k1gInSc1	ledovec
doslova	doslova	k6eAd1	doslova
prořízl	proříznout	k5eAaPmAgInS	proříznout
trup	trup	k1gInSc1	trup
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sonarových	sonarův	k2eAgInPc2d1	sonarův
výzkumů	výzkum	k1gInPc2	výzkum
vraku	vrak	k1gInSc2	vrak
<g/>
,	,	kIx,	,
provedených	provedený	k2eAgInPc2d1	provedený
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kolize	kolize	k1gFnSc1	kolize
mezi	mezi	k7c7	mezi
ledovcem	ledovec	k1gInSc7	ledovec
a	a	k8xC	a
trupem	trup	k1gInSc7	trup
způsobila	způsobit	k5eAaPmAgFnS	způsobit
"	"	kIx"	"
<g/>
pouze	pouze	k6eAd1	pouze
<g/>
"	"	kIx"	"
deformaci	deformace	k1gFnSc3	deformace
lodních	lodní	k2eAgInPc2d1	lodní
plátů	plát	k1gInPc2	plát
trupu	trup	k1gInSc2	trup
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
ocelové	ocelový	k2eAgInPc1d1	ocelový
pláty	plát	k1gInPc1	plát
měly	mít	k5eAaImAgInP	mít
tloušťku	tloušťka	k1gFnSc4	tloušťka
1	[number]	k4	1
až	až	k9	až
1	[number]	k4	1
<g/>
1⁄	1⁄	k?	1⁄
palce	palec	k1gInPc1	palec
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
2,5	[number]	k4	2,5
až	až	k9	až
3,8	[number]	k4	3,8
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnou	podrobný	k2eAgFnSc4d1	podrobná
analýzu	analýza	k1gFnSc4	analýza
malých	malý	k2eAgInPc2d1	malý
kousků	kousek	k1gInPc2	kousek
oceli	ocel	k1gFnSc2	ocel
z	z	k7c2	z
trupu	trup	k1gInSc2	trup
Titanicu	Titanicus	k1gInSc2	Titanicus
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
uhlíková	uhlíkový	k2eAgFnSc1d1	uhlíková
ocel	ocel	k1gFnSc1	ocel
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
houževnatost	houževnatost	k1gFnSc4	houževnatost
a	a	k8xC	a
zkřehne	zkřehnout	k5eAaPmIp3nS	zkřehnout
ve	v	k7c6	v
studené	studený	k2eAgFnSc6d1	studená
nebo	nebo	k8xC	nebo
ledové	ledový	k2eAgFnSc6d1	ledová
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
pružnost	pružnost	k1gFnSc1	pružnost
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
náchylná	náchylný	k2eAgFnSc1d1	náchylná
k	k	k7c3	k
protržení	protržení	k1gNnSc3	protržení
<g/>
.	.	kIx.	.
</s>
<s>
Chemickou	chemický	k2eAgFnSc7d1	chemická
analýzou	analýza	k1gFnSc7	analýza
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ocel	ocel	k1gFnSc1	ocel
měla	mít	k5eAaImAgFnS	mít
vysoký	vysoký	k2eAgInSc4d1	vysoký
obsah	obsah	k1gInSc4	obsah
fosforu	fosfor	k1gInSc2	fosfor
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
)	)	kIx)	)
a	a	k8xC	a
síry	síra	k1gFnSc2	síra
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
)	)	kIx)	)
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
moderní	moderní	k2eAgFnSc7d1	moderní
ocelí	ocel	k1gFnSc7	ocel
a	a	k8xC	a
poměr	poměr	k1gInSc1	poměr
manganu	mangan	k1gInSc2	mangan
a	a	k8xC	a
síry	síra	k1gFnSc2	síra
byl	být	k5eAaImAgInS	být
6,8	[number]	k4	6,8
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
u	u	k7c2	u
moderních	moderní	k2eAgFnPc2d1	moderní
ocelí	ocel	k1gFnPc2	ocel
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
poměr	poměr	k1gInSc1	poměr
větší	veliký	k2eAgFnSc4d2	veliký
než	než	k8xS	než
200	[number]	k4	200
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Souhrn	souhrn	k1gInSc1	souhrn
těchto	tento	k3xDgInPc2	tento
poměrů	poměr	k1gInPc2	poměr
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
vysoký	vysoký	k2eAgInSc4d1	vysoký
obsah	obsah	k1gInSc4	obsah
fosforu	fosfor	k1gInSc2	fosfor
<g/>
,	,	kIx,	,
napomáhá	napomáhat	k5eAaImIp3nS	napomáhat
vzniku	vznik	k1gInSc3	vznik
lomu	lom	k1gInSc2	lom
<g/>
,	,	kIx,	,
síra	síra	k1gFnSc1	síra
tvoří	tvořit	k5eAaImIp3nP	tvořit
zrna	zrno	k1gNnPc1	zrno
sulfidu	sulfid	k1gInSc2	sulfid
železnatého	železnatý	k2eAgInSc2d1	železnatý
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
usnadňují	usnadňovat	k5eAaImIp3nP	usnadňovat
šíření	šíření	k1gNnSc4	šíření
trhlin	trhlina	k1gFnPc2	trhlina
<g/>
,	,	kIx,	,
a	a	k8xC	a
nedostatek	nedostatek	k1gInSc1	nedostatek
manganu	mangan	k1gInSc2	mangan
snižuje	snižovat	k5eAaImIp3nS	snižovat
tvárnost	tvárnost	k1gFnSc4	tvárnost
oceli	ocel	k1gFnSc2	ocel
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
toto	tento	k3xDgNnSc4	tento
byla	být	k5eAaImAgFnS	být
použitá	použitý	k2eAgFnSc1d1	použitá
ocel	ocel	k1gFnSc1	ocel
nejkvalitnější	kvalitní	k2eAgFnSc1d3	nejkvalitnější
<g/>
,	,	kIx,	,
jaká	jaký	k3yQgFnSc1	jaký
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
stavby	stavba	k1gFnSc2	stavba
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
i	i	k9	i
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
v	v	k7c6	v
nízkých	nízký	k2eAgFnPc6d1	nízká
teplotách	teplota	k1gFnPc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Chování	chování	k1gNnSc1	chování
oceli	ocel	k1gFnSc2	ocel
při	při	k7c6	při
namáhání	namáhání	k1gNnSc6	namáhání
taktéž	taktéž	k?	taktéž
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
směr	směr	k1gInSc1	směr
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgInSc7	jaký
je	být	k5eAaImIp3nS	být
válcována	válcován	k2eAgFnSc1d1	válcována
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jakým	jaký	k3yIgInSc7	jaký
směrem	směr	k1gInSc7	směr
je	být	k5eAaImIp3nS	být
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
struktura	struktura	k1gFnSc1	struktura
<g/>
,	,	kIx,	,
směr	směr	k1gInSc1	směr
zrn	zrno	k1gNnPc2	zrno
<g/>
,	,	kIx,	,
oceli	ocel	k1gFnPc4	ocel
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
a	a	k8xC	a
jakým	jaký	k3yIgInSc7	jaký
směrem	směr	k1gInSc7	směr
působí	působit	k5eAaImIp3nP	působit
síly	síla	k1gFnPc4	síla
při	při	k7c6	při
zatížení	zatížení	k1gNnSc6	zatížení
<g/>
.	.	kIx.	.
</s>
<s>
Ocelové	ocelový	k2eAgInPc1d1	ocelový
pláty	plát	k1gInPc1	plát
pro	pro	k7c4	pro
Titanic	Titanic	k1gInSc4	Titanic
byl	být	k5eAaImAgInS	být
dodány	dodán	k2eAgFnPc4d1	dodána
firmou	firma	k1gFnSc7	firma
David	David	k1gMnSc1	David
Colville	Colville	k1gFnSc2	Colville
&	&	k?	&
Sons	Sons	k1gInSc1	Sons
z	z	k7c2	z
oceláren	ocelárna	k1gFnPc2	ocelárna
Dalzell	Dalzell	k1gMnSc1	Dalzell
Steel	Steel	k1gMnSc1	Steel
and	and	k?	and
Iron	iron	k1gInSc1	iron
Works	Works	kA	Works
v	v	k7c6	v
Motherwellu	Motherwell	k1gInSc6	Motherwell
nedaleko	nedaleko	k7c2	nedaleko
Glasgow	Glasgow	k1gNnSc2	Glasgow
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vysvětlilo	vysvětlit	k5eAaPmAgNnS	vysvětlit
vysoký	vysoký	k2eAgInSc4d1	vysoký
obsah	obsah	k1gInSc4	obsah
fosforu	fosfor	k1gInSc2	fosfor
a	a	k8xC	a
síry	síra	k1gFnSc2	síra
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
analýzy	analýza	k1gFnSc2	analýza
šesti	šest	k4xCc2	šest
malých	malý	k2eAgFnPc2d1	malá
částí	část	k1gFnPc2	část
trupu	trup	k1gInSc2	trup
při	při	k7c6	při
pokojové	pokojový	k2eAgFnSc6d1	pokojová
teplotě	teplota	k1gFnSc6	teplota
a	a	k8xC	a
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
32	[number]	k4	32
°	°	k?	°
<g/>
F	F	kA	F
<g/>
)	)	kIx)	)
ale	ale	k8xC	ale
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
použitá	použitý	k2eAgFnSc1d1	použitá
ocel	ocel	k1gFnSc1	ocel
nebyla	být	k5eNaImAgFnS	být
křehká	křehký	k2eAgFnSc1d1	křehká
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
kolem	kolem	k7c2	kolem
0	[number]	k4	0
°	°	k?	°
<g/>
C.	C.	kA	C.
Dřívější	dřívější	k2eAgInPc1d1	dřívější
testy	test	k1gInPc1	test
byly	být	k5eAaImAgInP	být
totiž	totiž	k9	totiž
prováděny	prováděn	k2eAgInPc1d1	prováděn
se	s	k7c7	s
simulovaným	simulovaný	k2eAgInSc7d1	simulovaný
nárazem	náraz	k1gInSc7	náraz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
simulované	simulovaný	k2eAgNnSc1d1	simulované
"	"	kIx"	"
<g/>
pomalé	pomalý	k2eAgNnSc1d1	pomalé
prohýbání	prohýbání	k1gNnSc1	prohýbání
<g/>
"	"	kIx"	"
více	hodně	k6eAd2	hodně
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
skutečnému	skutečný	k2eAgInSc3d1	skutečný
průběhu	průběh	k1gInSc3	průběh
deformace	deformace	k1gFnSc2	deformace
při	při	k7c6	při
nárazu	náraz	k1gInSc6	náraz
na	na	k7c4	na
ledovec	ledovec	k1gInSc4	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
faktorem	faktor	k1gInSc7	faktor
ovlivňujícím	ovlivňující	k2eAgInSc7d1	ovlivňující
rozsah	rozsah	k1gInSc4	rozsah
trhliny	trhlina	k1gFnPc1	trhlina
byly	být	k5eAaImAgFnP	být
nýty	nýt	k1gInPc4	nýt
<g/>
,	,	kIx,	,
držící	držící	k2eAgInSc4d1	držící
trup	trup	k1gInSc4	trup
pohromadě	pohromadě	k6eAd1	pohromadě
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
nýty	nýt	k1gInPc1	nýt
totiž	totiž	k9	totiž
byly	být	k5eAaImAgInP	být
křehčí	křehký	k2eAgInPc4d2	křehčí
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
.	.	kIx.	.
</s>
<s>
Použité	použitý	k2eAgInPc1d1	použitý
nýty	nýt	k1gInPc1	nýt
byly	být	k5eAaImAgInP	být
osazovány	osazovat	k5eAaImNgInP	osazovat
na	na	k7c4	na
trup	trup	k1gInSc4	trup
pomocí	pomocí	k7c2	pomocí
nýtovacího	nýtovací	k2eAgInSc2d1	nýtovací
stroje	stroj	k1gInSc2	stroj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ten	ten	k3xDgInSc4	ten
nebylo	být	k5eNaImAgNnS	být
možno	možno	k6eAd1	možno
použít	použít	k5eAaPmF	použít
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
prostorách	prostora	k1gFnPc6	prostora
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
příď	příď	k1gFnSc4	příď
a	a	k8xC	a
záď	záď	k1gFnSc4	záď
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
používalo	používat	k5eAaImAgNnS	používat
ruční	ruční	k2eAgNnSc1d1	ruční
nýtování	nýtování	k1gNnSc1	nýtování
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
několika	několik	k4yIc2	několik
milionů	milion	k4xCgInPc2	milion
nýtů	nýt	k1gInPc2	nýt
držících	držící	k2eAgInPc2d1	držící
Titanic	Titanic	k1gInSc4	Titanic
pohromadě	pohromadě	k6eAd1	pohromadě
bylo	být	k5eAaImAgNnS	být
vyloveno	vylovit	k5eAaPmNgNnS	vylovit
čtyřicet	čtyřicet	k4xCc1	čtyřicet
osm	osm	k4xCc1	osm
<g/>
;	;	kIx,	;
šest	šest	k4xCc1	šest
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
identifikováno	identifikovat	k5eAaBmNgNnS	identifikovat
jako	jako	k9	jako
nýty	nýt	k1gInPc4	nýt
z	z	k7c2	z
trupu	trup	k1gInSc2	trup
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
Američané	Američan	k1gMnPc1	Američan
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
experimentů	experiment	k1gInPc2	experiment
s	s	k7c7	s
nýty	nýt	k1gInPc7	nýt
vyrobenými	vyrobený	k2eAgInPc7d1	vyrobený
co	co	k9	co
nejpřesněji	přesně	k6eAd3	přesně
podle	podle	k7c2	podle
nýtů	nýt	k1gInPc2	nýt
na	na	k7c4	na
Titanicu	Titanica	k1gFnSc4	Titanica
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nýty	nýt	k1gInPc1	nýt
obsahovaly	obsahovat	k5eAaImAgInP	obsahovat
velkou	velký	k2eAgFnSc4d1	velká
koncentraci	koncentrace	k1gFnSc4	koncentrace
strusky	struska	k1gFnSc2	struska
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
způsobila	způsobit	k5eAaPmAgFnS	způsobit
jejich	jejich	k3xOp3gFnSc4	jejich
větší	veliký	k2eAgFnSc4d2	veliký
křehkost	křehkost	k1gFnSc4	křehkost
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
při	při	k7c6	při
nárazu	náraz	k1gInSc6	náraz
Titanicu	Titanicus	k1gInSc2	Titanicus
na	na	k7c4	na
ledovec	ledovec	k1gInSc4	ledovec
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
výpočtů	výpočet	k1gInPc2	výpočet
na	na	k7c4	na
každý	každý	k3xTgInSc4	každý
nýt	nýt	k1gInSc4	nýt
vyvinut	vyvinut	k2eAgInSc1d1	vyvinut
tlak	tlak	k1gInSc1	tlak
asi	asi	k9	asi
6	[number]	k4	6
350	[number]	k4	350
kg	kg	kA	kg
<g/>
,	,	kIx,	,
nýty	nýt	k1gInPc1	nýt
praskaly	praskat	k5eAaImAgInP	praskat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zřejmě	zřejmě	k6eAd1	zřejmě
již	již	k6eAd1	již
při	při	k7c6	při
tlaku	tlak	k1gInSc6	tlak
4	[number]	k4	4
530	[number]	k4	530
kg	kg	kA	kg
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
měly	mít	k5eAaImAgFnP	mít
vydržet	vydržet	k5eAaPmF	vydržet
až	až	k9	až
dvojnásobek	dvojnásobek	k1gInSc4	dvojnásobek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
archivech	archiv	k1gInPc6	archiv
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
kování	kování	k1gNnSc4	kování
nýtů	nýt	k1gInPc2	nýt
bylo	být	k5eAaImAgNnS	být
nakoupeno	nakoupen	k2eAgNnSc1d1	nakoupeno
železo	železo	k1gNnSc1	železo
č.	č.	k?	č.
3	[number]	k4	3
"	"	kIx"	"
<g/>
best	best	k5eAaPmF	best
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
<g/>
)	)	kIx)	)
s	s	k7c7	s
pevností	pevnost	k1gFnSc7	pevnost
v	v	k7c6	v
tahu	tah	k1gInSc6	tah
cca	cca	kA	cca
73	[number]	k4	73
%	%	kIx~	%
pevnosti	pevnost	k1gFnSc3	pevnost
oceli	ocel	k1gFnSc2	ocel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
správně	správně	k6eAd1	správně
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
nakoupeno	nakoupen	k2eAgNnSc4d1	nakoupeno
železo	železo	k1gNnSc4	železo
č.	č.	k?	č.
4	[number]	k4	4
"	"	kIx"	"
<g/>
best-best	bestest	k1gInSc1	best-best
<g/>
"	"	kIx"	"
s	s	k7c7	s
pevností	pevnost	k1gFnSc7	pevnost
v	v	k7c6	v
tahu	tah	k1gInSc6	tah
80	[number]	k4	80
%	%	kIx~	%
pevnosti	pevnost	k1gFnSc2	pevnost
oceli	ocel	k1gFnSc2	ocel
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
nevhodné	vhodný	k2eNgInPc4d1	nevhodný
nýty	nýt	k1gInPc4	nýt
na	na	k7c6	na
přídi	příď	k1gFnSc6	příď
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnPc1d3	veliký
škody	škoda	k1gFnPc1	škoda
utrpěly	utrpět	k5eAaPmAgFnP	utrpět
prostory	prostora	k1gFnPc4	prostora
s	s	k7c7	s
kotelnami	kotelna	k1gFnPc7	kotelna
5	[number]	k4	5
a	a	k8xC	a
6	[number]	k4	6
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
použity	použít	k5eAaPmNgInP	použít
ocelové	ocelový	k2eAgInPc1d1	ocelový
nýty	nýt	k1gInPc1	nýt
<g/>
.	.	kIx.	.
</s>
<s>
Kormidlo	kormidlo	k1gNnSc1	kormidlo
"	"	kIx"	"
<g/>
Titanicu	Titanicus	k1gInSc2	Titanicus
<g/>
"	"	kIx"	"
mělo	mít	k5eAaImAgNnS	mít
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
rozměrovou	rozměrový	k2eAgFnSc4d1	rozměrová
velikost	velikost	k1gFnSc4	velikost
zdánlivě	zdánlivě	k6eAd1	zdánlivě
dostačující	dostačující	k2eAgFnSc4d1	dostačující
plochu	plocha	k1gFnSc4	plocha
pro	pro	k7c4	pro
ovládání	ovládání	k1gNnSc4	ovládání
směru	směr	k1gInSc2	směr
plavby	plavba	k1gFnSc2	plavba
a	a	k8xC	a
manévrování	manévrování	k1gNnSc2	manévrování
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
takto	takto	k6eAd1	takto
velkou	velký	k2eAgFnSc4d1	velká
loď	loď	k1gFnSc4	loď
bylo	být	k5eAaImAgNnS	být
kormidlo	kormidlo	k1gNnSc1	kormidlo
poddimenzované	poddimenzovaný	k2eAgNnSc1d1	poddimenzované
(	(	kIx(	(
<g/>
malá	malý	k2eAgFnSc1d1	malá
plocha	plocha	k1gFnSc1	plocha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
návrhu	návrh	k1gInSc6	návrh
nebyly	být	k5eNaImAgInP	být
provedeny	provést	k5eAaPmNgInP	provést
žádné	žádný	k3yNgInPc1	žádný
výpočty	výpočet	k1gInPc1	výpočet
nebo	nebo	k8xC	nebo
pokusy	pokus	k1gInPc1	pokus
s	s	k7c7	s
modelem	model	k1gInSc7	model
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
by	by	kYmCp3nP	by
ověřily	ověřit	k5eAaPmAgInP	ověřit
ovladatelnost	ovladatelnost	k1gFnSc4	ovladatelnost
lodě	loď	k1gFnSc2	loď
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
269,10	[number]	k4	269,10
metrů	metr	k1gInPc2	metr
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nouzového	nouzový	k2eAgNnSc2d1	nouzové
manévrování	manévrování	k1gNnSc2	manévrování
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
Achillovu	Achillův	k2eAgFnSc4d1	Achillova
patu	pata	k1gFnSc4	pata
Titanicu	Titanicus	k1gInSc2	Titanicus
<g/>
.	.	kIx.	.
</s>
<s>
Reálně	reálně	k6eAd1	reálně
<g/>
,	,	kIx,	,
při	při	k7c6	při
návrhu	návrh	k1gInSc6	návrh
kormidla	kormidlo	k1gNnSc2	kormidlo
byl	být	k5eAaImAgInS	být
neuváženě	uváženě	k6eNd1	uváženě
použit	použít	k5eAaPmNgInS	použít
model	model	k1gInSc1	model
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
použití	použití	k1gNnSc1	použití
vysokého	vysoký	k2eAgNnSc2d1	vysoké
kormidla	kormidlo	k1gNnSc2	kormidlo
je	být	k5eAaImIp3nS	být
účelné	účelný	k2eAgNnSc1d1	účelné
pro	pro	k7c4	pro
lodě	loď	k1gFnPc4	loď
s	s	k7c7	s
běžnou	běžný	k2eAgFnSc7d1	běžná
(	(	kIx(	(
<g/>
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
<g/>
)	)	kIx)	)
rychlostí	rychlost	k1gFnSc7	rychlost
plavby	plavba	k1gFnSc2	plavba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
kratší	krátký	k2eAgNnSc1d2	kratší
kormidlo	kormidlo	k1gNnSc1	kormidlo
je	být	k5eAaImIp3nS	být
výhodnější	výhodný	k2eAgNnSc1d2	výhodnější
při	při	k7c6	při
manévrování	manévrování	k1gNnSc6	manévrování
při	při	k7c6	při
malých	malý	k2eAgFnPc6d1	malá
rychlostech	rychlost	k1gFnPc6	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
porovnání	porovnání	k1gNnSc6	porovnání
velikosti	velikost	k1gFnSc2	velikost
kormidla	kormidlo	k1gNnSc2	kormidlo
podle	podle	k7c2	podle
dnešních	dnešní	k2eAgInPc2d1	dnešní
předpisů	předpis	k1gInPc2	předpis
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
požadováno	požadován	k2eAgNnSc1d1	požadováno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
kormidlo	kormidlo	k1gNnSc1	kormidlo
mělo	mít	k5eAaImAgNnS	mít
velikost	velikost	k1gFnSc4	velikost
1,5	[number]	k4	1,5
%	%	kIx~	%
až	až	k9	až
5	[number]	k4	5
%	%	kIx~	%
z	z	k7c2	z
profilu	profil	k1gInSc2	profil
lodi	loď	k1gFnSc2	loď
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
poměr	poměr	k1gInSc1	poměr
u	u	k7c2	u
Titanicu	Titanicus	k1gInSc2	Titanicus
1,9	[number]	k4	1,9
%	%	kIx~	%
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
dolní	dolní	k2eAgFnSc3d1	dolní
mezi	mezi	k7c7	mezi
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
uspořádání	uspořádání	k1gNnSc1	uspořádání
je	být	k5eAaImIp3nS	být
však	však	k9	však
výhodnější	výhodný	k2eAgMnSc1d2	výhodnější
pro	pro	k7c4	pro
lepší	dobrý	k2eAgNnSc4d2	lepší
laminární	laminární	k2eAgNnSc4d1	laminární
proudění	proudění	k1gNnSc4	proudění
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgMnS	být
Olympic	Olympic	k1gMnSc1	Olympic
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
většina	většina	k1gFnSc1	většina
významných	významný	k2eAgFnPc2d1	významná
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
vybaven	vybaven	k2eAgInSc1d1	vybaven
stejným	stejný	k2eAgNnSc7d1	stejné
polooválným	polooválný	k2eAgNnSc7d1	polooválný
kormidlem	kormidlo	k1gNnSc7	kormidlo
jako	jako	k8xS	jako
Titanic	Titanic	k1gInSc1	Titanic
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohly	moct	k5eAaImAgFnP	moct
tyto	tento	k3xDgFnPc1	tento
lodě	loď	k1gFnPc1	loď
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
vyšší	vysoký	k2eAgFnPc4d2	vyšší
rychlosti	rychlost	k1gFnPc4	rychlost
a	a	k8xC	a
uniknout	uniknout	k5eAaPmF	uniknout
ponorkám	ponorka	k1gFnPc3	ponorka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tragédii	tragédie	k1gFnSc3	tragédie
částečně	částečně	k6eAd1	částečně
přispěla	přispět	k5eAaPmAgFnS	přispět
i	i	k9	i
konstrukce	konstrukce	k1gFnSc1	konstrukce
pohonu	pohon	k1gInSc2	pohon
lodních	lodní	k2eAgInPc2d1	lodní
šroubů	šroub	k1gInPc2	šroub
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgInS	mít
Titanic	Titanic	k1gInSc1	Titanic
dva	dva	k4xCgInPc4	dva
postranní	postranní	k2eAgInPc4d1	postranní
šrouby	šroub	k1gInPc4	šroub
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
reverzace	reverzace	k1gFnSc2	reverzace
chodu	chod	k1gInSc2	chod
<g/>
,	,	kIx,	,
poháněné	poháněný	k2eAgInPc1d1	poháněný
parními	parní	k2eAgInPc7d1	parní
stroji	stroj	k1gInPc7	stroj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prostřední	prostřední	k2eAgInSc1d1	prostřední
šroub	šroub	k1gInSc1	šroub
byl	být	k5eAaImAgInS	být
poháněný	poháněný	k2eAgInSc1d1	poháněný
parní	parní	k2eAgFnSc7d1	parní
turbínou	turbína	k1gFnSc7	turbína
bez	bez	k7c2	bez
možnosti	možnost	k1gFnSc2	možnost
reverzace	reverzace	k1gFnSc2	reverzace
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
4	[number]	k4	4
<g/>
.	.	kIx.	.
důstojníka	důstojník	k1gMnSc4	důstojník
Josepha	Joseph	k1gMnSc4	Joseph
Boxhalla	Boxhall	k1gMnSc4	Boxhall
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
přišel	přijít	k5eAaPmAgMnS	přijít
na	na	k7c4	na
můstek	můstek	k1gInSc4	můstek
po	po	k7c6	po
srážce	srážka	k1gFnSc6	srážka
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
důstojník	důstojník	k1gMnSc1	důstojník
Murdoch	Murdoch	k1gMnSc1	Murdoch
sice	sice	k8xC	sice
vydal	vydat	k5eAaPmAgInS	vydat
strojovně	strojovna	k1gFnSc3	strojovna
povel	povel	k1gInSc1	povel
"	"	kIx"	"
<g/>
zpětný	zpětný	k2eAgInSc1d1	zpětný
chod	chod	k1gInSc1	chod
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
odvrátil	odvrátit	k5eAaPmAgMnS	odvrátit
srážku	srážka	k1gFnSc4	srážka
s	s	k7c7	s
ledovcem	ledovec	k1gInSc7	ledovec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protože	protože	k8xS	protože
střední	střední	k2eAgInSc4d1	střední
šroub	šroub	k1gInSc4	šroub
byl	být	k5eAaImAgInS	být
poháněný	poháněný	k2eAgInSc1d1	poháněný
turbínou	turbína	k1gFnSc7	turbína
<g/>
,	,	kIx,	,
nemohla	moct	k5eNaImAgFnS	moct
být	být	k5eAaImF	být
tato	tento	k3xDgFnSc1	tento
reverzována	reverzován	k2eAgFnSc1d1	reverzován
na	na	k7c4	na
zpětný	zpětný	k2eAgInSc4d1	zpětný
chod	chod	k1gInSc4	chod
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
zastavena	zastavit	k5eAaPmNgFnS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
obrázku	obrázek	k1gInSc2	obrázek
je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
,	,	kIx,	,
že	že	k8xS	že
střední	střední	k2eAgInSc1d1	střední
šroub	šroub	k1gInSc1	šroub
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
v	v	k7c6	v
ose	osa	k1gFnSc6	osa
kormidla	kormidlo	k1gNnSc2	kormidlo
a	a	k8xC	a
při	při	k7c6	při
zastavení	zastavení	k1gNnSc6	zastavení
chodu	chod	k1gInSc2	chod
šroubu	šroub	k1gInSc2	šroub
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
k	k	k7c3	k
podstatnému	podstatný	k2eAgNnSc3d1	podstatné
snížení	snížení	k1gNnSc3	snížení
obtékání	obtékání	k1gNnSc1	obtékání
kormidla	kormidlo	k1gNnSc2	kormidlo
proudící	proudící	k2eAgFnSc7d1	proudící
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
pro	pro	k7c4	pro
vyvození	vyvození	k1gNnSc4	vyvození
síly	síla	k1gFnSc2	síla
otáčející	otáčející	k2eAgFnSc4d1	otáčející
loď	loď	k1gFnSc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
Murdoch	Murdoch	k1gInSc1	Murdoch
vydal	vydat	k5eAaPmAgInS	vydat
pouze	pouze	k6eAd1	pouze
povel	povel	k1gInSc1	povel
"	"	kIx"	"
<g/>
docela	docela	k6eAd1	docela
vpravo	vpravo	k6eAd1	vpravo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
bez	bez	k7c2	bez
rozkazu	rozkaz	k1gInSc2	rozkaz
"	"	kIx"	"
<g/>
zpětný	zpětný	k2eAgInSc1d1	zpětný
chod	chod	k1gInSc1	chod
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgInS	moct
se	se	k3xPyFc4	se
Titanic	Titanic	k1gInSc1	Titanic
ještě	ještě	k6eAd1	ještě
ledovci	ledovec	k1gInSc3	ledovec
vyhnout	vyhnout	k5eAaPmF	vyhnout
<g/>
.	.	kIx.	.
</s>
<s>
Zachráněný	zachráněný	k2eAgMnSc1d1	zachráněný
strojník	strojník	k1gMnSc1	strojník
Frederick	Frederick	k1gMnSc1	Frederick
Scott	Scott	k1gMnSc1	Scott
ale	ale	k8xC	ale
při	při	k7c6	při
výpovědi	výpověď	k1gFnSc6	výpověď
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgFnPc1	všechen
čtyři	čtyři	k4xCgFnPc1	čtyři
sady	sada	k1gFnPc1	sada
telegrafů	telegraf	k1gInPc2	telegraf
se	se	k3xPyFc4	se
změnily	změnit	k5eAaPmAgInP	změnit
na	na	k7c4	na
"	"	kIx"	"
<g/>
STOP	stop	k2eAgFnSc4d1	stop
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
až	až	k9	až
po	po	k7c6	po
kolizi	kolize	k1gFnSc6	kolize
s	s	k7c7	s
ledovcem	ledovec	k1gInSc7	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgInSc4d1	vlastní
vyhýbací	vyhýbací	k2eAgInSc4d1	vyhýbací
manévr	manévr	k1gInSc4	manévr
řídil	řídit	k5eAaImAgMnS	řídit
první	první	k4xOgMnSc1	první
důstojník	důstojník	k1gMnSc1	důstojník
Titanicu	Titanicus	k1gInSc2	Titanicus
William	William	k1gInSc1	William
M.	M.	kA	M.
Murdoch	Murdoch	k1gInSc1	Murdoch
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zjištění	zjištění	k1gNnSc4	zjištění
průběhu	průběh	k1gInSc2	průběh
vyhýbacího	vyhýbací	k2eAgInSc2d1	vyhýbací
manévru	manévr	k1gInSc2	manévr
byly	být	k5eAaImAgInP	být
později	pozdě	k6eAd2	pozdě
prováděny	provádět	k5eAaImNgInP	provádět
výpočty	výpočet	k1gInPc1	výpočet
a	a	k8xC	a
zkoušky	zkouška	k1gFnPc1	zkouška
se	s	k7c7	s
sesterskou	sesterský	k2eAgFnSc7d1	sesterská
lodí	loď	k1gFnSc7	loď
Olympic	Olympice	k1gFnPc2	Olympice
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
vykonání	vykonání	k1gNnSc4	vykonání
rozkazu	rozkaz	k1gInSc2	rozkaz
"	"	kIx"	"
<g/>
zpětný	zpětný	k2eAgInSc1d1	zpětný
chod	chod	k1gInSc1	chod
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
20	[number]	k4	20
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
povel	povel	k1gInSc1	povel
nebyl	být	k5eNaImAgInS	být
ani	ani	k8xC	ani
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
plavby	plavba	k1gFnSc2	plavba
předtím	předtím	k6eAd1	předtím
vydán	vydat	k5eAaPmNgInS	vydat
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
brát	brát	k5eAaImF	brát
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
i	i	k9	i
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
strojníci	strojník	k1gMnPc1	strojník
vykonávali	vykonávat	k5eAaImAgMnP	vykonávat
i	i	k9	i
další	další	k2eAgFnPc4d1	další
činnosti	činnost	k1gFnPc4	činnost
<g/>
,	,	kIx,	,
potřebné	potřebný	k2eAgInPc4d1	potřebný
k	k	k7c3	k
chodu	chod	k1gInSc3	chod
lodních	lodní	k2eAgInPc2d1	lodní
strojů	stroj	k1gInPc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
k	k	k7c3	k
předání	předání	k1gNnSc3	předání
rozkazu	rozkaz	k1gInSc2	rozkaz
bylo	být	k5eAaImAgNnS	být
potřeba	potřeba	k6eAd1	potřeba
pouze	pouze	k6eAd1	pouze
10	[number]	k4	10
sekund	sekunda	k1gFnPc2	sekunda
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
technicky	technicky	k6eAd1	technicky
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
včasnému	včasný	k2eAgNnSc3d1	včasné
zastavení	zastavení	k1gNnSc3	zastavení
strojů	stroj	k1gInPc2	stroj
<g/>
,	,	kIx,	,
provedení	provedení	k1gNnSc3	provedení
reverzace	reverzace	k1gFnSc2	reverzace
chodu	chod	k1gInSc2	chod
a	a	k8xC	a
vyvinutí	vyvinutí	k1gNnSc2	vyvinutí
zpětného	zpětný	k2eAgInSc2d1	zpětný
tahu	tah	k1gInSc2	tah
lodních	lodní	k2eAgInPc2d1	lodní
šroubů	šroub	k1gInPc2	šroub
před	před	k7c7	před
srážkou	srážka	k1gFnSc7	srážka
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ke	k	k7c3	k
zpětnému	zpětný	k2eAgInSc3d1	zpětný
chodu	chod	k1gInSc3	chod
před	před	k7c7	před
srážkou	srážka	k1gFnSc7	srážka
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
,	,	kIx,	,
hovoří	hovořit	k5eAaImIp3nS	hovořit
tři	tři	k4xCgInPc4	tři
fakty	fakt	k1gInPc4	fakt
<g/>
:	:	kIx,	:
Při	při	k7c6	při
zpětném	zpětný	k2eAgInSc6d1	zpětný
chodu	chod	k1gInSc6	chod
strojů	stroj	k1gInPc2	stroj
by	by	kYmCp3nP	by
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
silným	silný	k2eAgFnPc3d1	silná
vibracím	vibrace	k1gFnPc3	vibrace
<g/>
,	,	kIx,	,
pociťovatelné	pociťovatelný	k2eAgFnSc3d1	pociťovatelný
na	na	k7c6	na
celé	celý	k2eAgFnSc6d1	celá
lodi	loď	k1gFnSc6	loď
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
pouze	pouze	k6eAd1	pouze
vibrace	vibrace	k1gFnPc1	vibrace
na	na	k7c6	na
přídi	příď	k1gFnSc6	příď
<g/>
,	,	kIx,	,
způsobené	způsobený	k2eAgFnPc4d1	způsobená
nárazem	náraz	k1gInSc7	náraz
na	na	k7c4	na
ledovec	ledovec	k1gInSc4	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
svědectví	svědectví	k1gNnSc2	svědectví
lodního	lodní	k2eAgMnSc2d1	lodní
strojníka	strojník	k1gMnSc2	strojník
Fredericka	Fredericka	k1gFnSc1	Fredericka
Scotta	Scotta	k1gFnSc1	Scotta
přišel	přijít	k5eAaPmAgInS	přijít
povel	povel	k1gInSc1	povel
telegrafem	telegraf	k1gInSc7	telegraf
k	k	k7c3	k
zastavení	zastavení	k1gNnSc3	zastavení
až	až	k6eAd1	až
po	po	k7c6	po
nárazu	náraz	k1gInSc6	náraz
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgMnSc1d1	hlavní
topič	topič	k1gMnSc1	topič
Frederick	Frederick	k1gMnSc1	Frederick
Barrett	Barrett	k1gMnSc1	Barrett
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
v	v	k7c6	v
kotelně	kotelna	k1gFnSc6	kotelna
telegrafy	telegraf	k1gInPc7	telegraf
indikovaly	indikovat	k5eAaBmAgFnP	indikovat
stejný	stejný	k2eAgInSc4d1	stejný
povel	povel	k1gInSc4	povel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úvahách	úvaha	k1gFnPc6	úvaha
o	o	k7c6	o
možném	možný	k2eAgNnSc6d1	možné
odvrácení	odvrácení	k1gNnSc6	odvrácení
kolize	kolize	k1gFnSc2	kolize
se	se	k3xPyFc4	se
hovořilo	hovořit	k5eAaImAgNnS	hovořit
i	i	k9	i
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohl	moct	k5eAaImAgMnS	moct
Murdoch	Murdoch	k1gMnSc1	Murdoch
srážce	srážka	k1gFnSc3	srážka
předejít	předejít	k5eAaPmF	předejít
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
vydal	vydat	k5eAaPmAgInS	vydat
povel	povel	k1gInSc1	povel
ke	k	k7c3	k
zpětnému	zpětný	k2eAgInSc3d1	zpětný
chodu	chod	k1gInSc3	chod
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
levý	levý	k2eAgInSc4d1	levý
lodní	lodní	k2eAgInSc4d1	lodní
šroub	šroub	k1gInSc4	šroub
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
tento	tento	k3xDgInSc1	tento
povel	povel	k1gInSc1	povel
by	by	kYmCp3nS	by
však	však	k9	však
nevedl	vést	k5eNaImAgMnS	vést
k	k	k7c3	k
odvrácení	odvrácení	k1gNnSc3	odvrácení
katastrofy	katastrofa	k1gFnSc2	katastrofa
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pro	pro	k7c4	pro
reverzaci	reverzace	k1gFnSc4	reverzace
chodu	chod	k1gInSc2	chod
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
delší	dlouhý	k2eAgInSc4d2	delší
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
než	než	k8xS	než
který	který	k3yQgInSc1	který
zbýval	zbývat	k5eAaImAgInS	zbývat
do	do	k7c2	do
nárazu	náraz	k1gInSc2	náraz
<g/>
.	.	kIx.	.
</s>
<s>
Britská	britský	k2eAgFnSc1d1	britská
vyšetřovací	vyšetřovací	k2eAgFnSc1d1	vyšetřovací
komise	komise	k1gFnSc1	komise
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Titanic	Titanic	k1gInSc1	Titanic
byl	být	k5eAaImAgInS	být
vůči	vůči	k7c3	vůči
ledovci	ledovec	k1gInSc3	ledovec
natočen	natočen	k2eAgMnSc1d1	natočen
vlevo	vlevo	k6eAd1	vlevo
<g/>
,	,	kIx,	,
v	v	k7c6	v
úhlu	úhel	k1gInSc6	úhel
22,5	[number]	k4	22,5
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
pokusů	pokus	k1gInPc2	pokus
<g/>
,	,	kIx,	,
prováděných	prováděný	k2eAgFnPc2d1	prováděná
se	se	k3xPyFc4	se
sesterskou	sesterský	k2eAgFnSc7d1	sesterská
lodí	loď	k1gFnSc7	loď
Olympic	Olympice	k1gInPc2	Olympice
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
plné	plný	k2eAgFnSc6d1	plná
rychlosti	rychlost	k1gFnSc6	rychlost
<g/>
,	,	kIx,	,
plnému	plný	k2eAgNnSc3d1	plné
vychýlení	vychýlení	k1gNnSc3	vychýlení
kormidla	kormidlo	k1gNnSc2	kormidlo
a	a	k8xC	a
době	doba	k1gFnSc6	doba
k	k	k7c3	k
nárazu	náraz	k1gInSc3	náraz
37	[number]	k4	37
sekund	sekunda	k1gFnPc2	sekunda
<g/>
,	,	kIx,	,
odpovídala	odpovídat	k5eAaImAgFnS	odpovídat
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
od	od	k7c2	od
ledovce	ledovec	k1gInSc2	ledovec
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
zpozorování	zpozorování	k1gNnSc2	zpozorování
přibližně	přibližně	k6eAd1	přibližně
410	[number]	k4	410
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	kYmCp3nS	kdyby
se	se	k3xPyFc4	se
Titanic	Titanic	k1gInSc1	Titanic
při	při	k7c6	při
vyhýbacím	vyhýbací	k2eAgInSc6d1	vyhýbací
manévru	manévr	k1gInSc6	manévr
natočil	natočit	k5eAaBmAgMnS	natočit
více	hodně	k6eAd2	hodně
doleva	doleva	k6eAd1	doleva
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
by	by	kYmCp3nS	by
ke	k	k7c3	k
střetu	střet	k1gInSc3	střet
s	s	k7c7	s
ledovcem	ledovec	k1gInSc7	ledovec
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
trupu	trup	k1gInSc2	trup
na	na	k7c6	na
pravoboku	pravobok	k1gInSc6	pravobok
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
jiné	jiný	k2eAgFnSc2d1	jiná
trajektorie	trajektorie	k1gFnSc2	trajektorie
zádi	záď	k1gFnSc2	záď
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
delší	dlouhý	k2eAgFnSc1d2	delší
a	a	k8xC	a
liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
od	od	k7c2	od
trajektorie	trajektorie	k1gFnSc2	trajektorie
přídě	příď	k1gFnSc2	příď
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozkazu	rozkaz	k1gInSc6	rozkaz
"	"	kIx"	"
<g/>
docela	docela	k6eAd1	docela
vpravo	vpravo	k6eAd1	vpravo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vydaném	vydaný	k2eAgNnSc6d1	vydané
Murdochem	Murdoch	k1gInSc7	Murdoch
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
přijít	přijít	k5eAaPmF	přijít
ve	v	k7c4	v
vhodný	vhodný	k2eAgInSc4d1	vhodný
okamžik	okamžik	k1gInSc4	okamžik
rozkaz	rozkaz	k1gInSc1	rozkaz
"	"	kIx"	"
<g/>
docela	docela	k6eAd1	docela
vlevo	vlevo	k6eAd1	vlevo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
zádí	záď	k1gFnPc2	záď
pryč	pryč	k6eAd1	pryč
od	od	k7c2	od
ledovce	ledovec	k1gInSc2	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Murdoch	Murdoch	k1gMnSc1	Murdoch
sice	sice	k8xC	sice
použil	použít	k5eAaPmAgMnS	použít
učebnicový	učebnicový	k2eAgInSc4d1	učebnicový
model	model	k1gInSc4	model
vyhýbacího	vyhýbací	k2eAgInSc2d1	vyhýbací
manévru	manévr	k1gInSc2	manévr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
problém	problém	k1gInSc1	problém
u	u	k7c2	u
Titanicu	Titanicus	k1gInSc2	Titanicus
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
dané	daný	k2eAgFnSc6d1	daná
rychlosti	rychlost	k1gFnSc6	rychlost
měl	mít	k5eAaImAgMnS	mít
příliš	příliš	k6eAd1	příliš
velký	velký	k2eAgInSc4d1	velký
poloměr	poloměr	k1gInSc4	poloměr
otáčení	otáčení	k1gNnSc2	otáčení
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
řada	řada	k1gFnSc1	řada
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
tento	tento	k3xDgInSc4	tento
manévr	manévr	k1gInSc4	manévr
kritizovaly	kritizovat	k5eAaImAgFnP	kritizovat
<g/>
:	:	kIx,	:
podle	podle	k7c2	podle
nich	on	k3xPp3gFnPc2	on
měl	mít	k5eAaImAgMnS	mít
Murdoch	Murdoch	k1gMnSc1	Murdoch
zastavit	zastavit	k5eAaPmF	zastavit
stroje	stroj	k1gInPc4	stroj
a	a	k8xC	a
přepnout	přepnout	k5eAaPmF	přepnout
je	on	k3xPp3gNnSc4	on
na	na	k7c4	na
zpětný	zpětný	k2eAgInSc4d1	zpětný
chod	chod	k1gInSc4	chod
<g/>
.	.	kIx.	.
</s>
<s>
Titanic	Titanic	k1gInSc1	Titanic
by	by	kYmCp3nS	by
pak	pak	k6eAd1	pak
do	do	k7c2	do
ledovce	ledovec	k1gInSc2	ledovec
narazil	narazit	k5eAaPmAgMnS	narazit
přídí	příď	k1gFnSc7	příď
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přímém	přímý	k2eAgInSc6d1	přímý
nárazu	náraz	k1gInSc6	náraz
přídí	příď	k1gFnPc2	příď
na	na	k7c4	na
ledovec	ledovec	k1gInSc4	ledovec
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
poškozeno	poškodit	k5eAaPmNgNnS	poškodit
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
stop	stopa	k1gFnPc2	stopa
trupu	trup	k1gInSc2	trup
od	od	k7c2	od
hrany	hrana	k1gFnSc2	hrana
přídě	příď	k1gFnSc2	příď
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
by	by	k9	by
proraženy	prorazit	k5eAaPmNgFnP	prorazit
nanejvýše	nanejvýše	k6eAd1	nanejvýše
3	[number]	k4	3
vodotěsné	vodotěsný	k2eAgFnSc2d1	vodotěsná
komory	komora	k1gFnSc2	komora
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
by	by	kYmCp3nP	by
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
přišli	přijít	k5eAaPmAgMnP	přijít
o	o	k7c4	o
život	život	k1gInSc4	život
i	i	k9	i
členové	člen	k1gMnPc1	člen
posádky	posádka	k1gFnSc2	posádka
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
své	svůj	k3xOyFgFnPc4	svůj
kajuty	kajuta	k1gFnPc4	kajuta
na	na	k7c6	na
přídi	příď	k1gFnSc6	příď
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kritika	kritika	k1gFnSc1	kritika
ovšem	ovšem	k9	ovšem
nebere	brát	k5eNaImIp3nS	brát
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
<g/>
,	,	kIx,	,
že	že	k8xS	že
Murdoch	Murdoch	k1gMnSc1	Murdoch
neměl	mít	k5eNaImAgMnS	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
přesnou	přesný	k2eAgFnSc4d1	přesná
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
mezi	mezi	k7c7	mezi
Titanicem	Titanic	k1gMnSc7	Titanic
a	a	k8xC	a
ledovcem	ledovec	k1gInSc7	ledovec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
mohl	moct	k5eAaImAgMnS	moct
odvodit	odvodit	k5eAaPmF	odvodit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
srážce	srážka	k1gFnSc3	srážka
nevyhne	vyhnout	k5eNaPmIp3nS	vyhnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
ani	ani	k8xC	ani
nezohledňuje	zohledňovat	k5eNaImIp3nS	zohledňovat
morální	morální	k2eAgFnSc4d1	morální
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
za	za	k7c4	za
usmrcení	usmrcení	k1gNnSc4	usmrcení
členů	člen	k1gMnPc2	člen
posádky	posádka	k1gFnSc2	posádka
při	při	k7c6	při
přímém	přímý	k2eAgInSc6d1	přímý
střetu	střet	k1gInSc6	střet
s	s	k7c7	s
ledovcem	ledovec	k1gInSc7	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Kritika	kritika	k1gFnSc1	kritika
zpochybnila	zpochybnit	k5eAaPmAgFnS	zpochybnit
i	i	k9	i
uzavření	uzavření	k1gNnSc4	uzavření
vodotěsných	vodotěsný	k2eAgFnPc2d1	vodotěsná
přepážek	přepážka	k1gFnPc2	přepážka
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
změnil	změnit	k5eAaPmAgInS	změnit
náklon	náklon	k1gInSc1	náklon
vlivem	vlivem	k7c2	vlivem
extrémního	extrémní	k2eAgNnSc2d1	extrémní
zatížení	zatížení	k1gNnSc2	zatížení
přídě	příď	k1gFnSc2	příď
<g/>
,	,	kIx,	,
a	a	k8xC	a
Titanic	Titanic	k1gInSc1	Titanic
se	se	k3xPyFc4	se
údajně	údajně	k6eAd1	údajně
mohl	moct	k5eAaImAgInS	moct
potápět	potápět	k5eAaImF	potápět
pomaleji	pomale	k6eAd2	pomale
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
Murdoch	Murdoch	k1gMnSc1	Murdoch
nemohl	moct	k5eNaImAgMnS	moct
znát	znát	k5eAaImF	znát
rozsah	rozsah	k1gInSc4	rozsah
poškození	poškození	k1gNnSc2	poškození
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
rychlost	rychlost	k1gFnSc4	rychlost
zaplavování	zaplavování	k1gNnSc2	zaplavování
podpalubí	podpalubí	k1gNnSc2	podpalubí
a	a	k8xC	a
podle	podle	k7c2	podle
standardního	standardní	k2eAgInSc2d1	standardní
postupu	postup	k1gInSc2	postup
při	při	k7c6	při
kolizi	kolize	k1gFnSc6	kolize
uzavřel	uzavřít	k5eAaPmAgMnS	uzavřít
přepážky	přepážka	k1gFnPc4	přepážka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pozdější	pozdní	k2eAgNnSc1d2	pozdější
uzavření	uzavření	k1gNnSc1	uzavření
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
opravdu	opravdu	k6eAd1	opravdu
již	již	k6eAd1	již
pozdní	pozdní	k2eAgInSc1d1	pozdní
<g/>
.	.	kIx.	.
</s>
<s>
Žádný	žádný	k3yNgMnSc1	žádný
konstruktér	konstruktér	k1gMnSc1	konstruktér
lodí	loď	k1gFnPc2	loď
by	by	kYmCp3nS	by
nepředpokládal	předpokládat	k5eNaImAgInS	předpokládat
opačný	opačný	k2eAgInSc4d1	opačný
postup	postup	k1gInSc4	postup
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
varianta	varianta	k1gFnSc1	varianta
později	pozdě	k6eAd2	pozdě
zkoumána	zkoumat	k5eAaImNgFnS	zkoumat
počítačovou	počítačový	k2eAgFnSc7d1	počítačová
simulací	simulace	k1gFnSc7	simulace
na	na	k7c6	na
modelu	model	k1gInSc6	model
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
výsledkem	výsledek	k1gInSc7	výsledek
bylo	být	k5eAaImAgNnS	být
zjištění	zjištění	k1gNnSc1	zjištění
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
Titanic	Titanic	k1gInSc1	Titanic
potopil	potopit	k5eAaPmAgInS	potopit
dokonce	dokonce	k9	dokonce
o	o	k7c4	o
40	[number]	k4	40
minut	minuta	k1gFnPc2	minuta
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Zabránění	zabránění	k1gNnSc1	zabránění
zavření	zavření	k1gNnSc2	zavření
přepážek	přepážka	k1gFnPc2	přepážka
ani	ani	k9	ani
nebylo	být	k5eNaImAgNnS	být
technicky	technicky	k6eAd1	technicky
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
při	při	k7c6	při
dosažení	dosažení	k1gNnSc6	dosažení
kritické	kritický	k2eAgFnSc2d1	kritická
hladiny	hladina	k1gFnSc2	hladina
vody	voda	k1gFnSc2	voda
uvnitř	uvnitř	k7c2	uvnitř
lodě	loď	k1gFnSc2	loď
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
aktivovalo	aktivovat	k5eAaBmAgNnS	aktivovat
automatické	automatický	k2eAgNnSc1d1	automatické
uzavírání	uzavírání	k1gNnSc1	uzavírání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
britské	britský	k2eAgFnSc2d1	britská
vyšetřovací	vyšetřovací	k2eAgFnSc2d1	vyšetřovací
komise	komise	k1gFnSc2	komise
je	být	k5eAaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
rozsah	rozsah	k1gInSc1	rozsah
ztrát	ztráta	k1gFnPc2	ztráta
po	po	k7c6	po
kolizi	kolize	k1gFnSc6	kolize
s	s	k7c7	s
ledovcem	ledovec	k1gInSc7	ledovec
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
i	i	k9	i
nadprůměrná	nadprůměrný	k2eAgFnSc1d1	nadprůměrná
rychlost	rychlost	k1gFnSc1	rychlost
lodi	loď	k1gFnSc2	loď
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
záznamů	záznam	k1gInPc2	záznam
plul	plout	k5eAaImAgInS	plout
Titanic	Titanic	k1gInSc1	Titanic
v	v	k7c6	v
době	doba	k1gFnSc6	doba
srážky	srážka	k1gFnSc2	srážka
normální	normální	k2eAgFnSc1d1	normální
rychlostí	rychlost	k1gFnSc7	rychlost
kolem	kolem	k7c2	kolem
21	[number]	k4	21
uzlů	uzel	k1gInPc2	uzel
(	(	kIx(	(
<g/>
39	[number]	k4	39
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc4	jeho
maximální	maximální	k2eAgFnSc4d1	maximální
rychlost	rychlost	k1gFnSc4	rychlost
23	[number]	k4	23
uzlů	uzel	k1gInPc2	uzel
(	(	kIx(	(
<g/>
43	[number]	k4	43
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
rychlost	rychlost	k1gFnSc1	rychlost
nebyla	být	k5eNaImAgFnS	být
sice	sice	k8xC	sice
obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
nebyla	být	k5eNaImAgFnS	být
nijak	nijak	k6eAd1	nijak
výjimečná	výjimečný	k2eAgFnSc1d1	výjimečná
ani	ani	k8xC	ani
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
očekávat	očekávat	k5eAaImF	očekávat
výskyt	výskyt	k1gInSc4	výskyt
ledovců	ledovec	k1gInPc2	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ledovec	ledovec	k1gInSc1	ledovec
bude	být	k5eAaImBp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
velký	velký	k2eAgInSc1d1	velký
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gMnSc4	on
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
zahlédnout	zahlédnout	k5eAaPmF	zahlédnout
v	v	k7c6	v
dostatečném	dostatečný	k2eAgInSc6d1	dostatečný
předstihu	předstih	k1gInSc6	předstih
a	a	k8xC	a
provést	provést	k5eAaPmF	provést
úhybný	úhybný	k2eAgInSc4d1	úhybný
manévr	manévr	k1gInSc4	manévr
<g/>
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
Bruce	Bruce	k1gFnSc1	Bruce
Ismayovi	Ismaya	k1gMnSc3	Ismaya
a	a	k8xC	a
kapitánu	kapitán	k1gMnSc3	kapitán
Smithovi	Smith	k1gMnSc3	Smith
bylo	být	k5eAaImAgNnS	být
potom	potom	k6eAd1	potom
dáváno	dávat	k5eAaImNgNnS	dávat
za	za	k7c4	za
vinu	vina	k1gFnSc4	vina
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
přikázal	přikázat	k5eAaPmAgInS	přikázat
plavbu	plavba	k1gFnSc4	plavba
takovou	takový	k3xDgFnSc7	takový
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
sporný	sporný	k2eAgInSc4d1	sporný
bod	bod	k1gInSc4	bod
příčiny	příčina	k1gFnPc1	příčina
katastrofy	katastrofa	k1gFnSc2	katastrofa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
zmiňován	zmiňován	k2eAgMnSc1d1	zmiňován
a	a	k8xC	a
zdůrazňován	zdůrazňován	k2eAgMnSc1d1	zdůrazňován
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ve	v	k7c6	v
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
filmu	film	k1gInSc6	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
kolize	kolize	k1gFnSc2	kolize
bylo	být	k5eAaImAgNnS	být
moře	moře	k1gNnSc1	moře
klidné	klidný	k2eAgNnSc1d1	klidné
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
byla	být	k5eAaImAgFnS	být
i	i	k9	i
bezměsíčná	bezměsíčný	k2eAgFnSc1d1	bezměsíčná
noc	noc	k1gFnSc1	noc
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
normálních	normální	k2eAgInPc2d1	normální
<g/>
,	,	kIx,	,
obvyklých	obvyklý	k2eAgInPc2d1	obvyklý
<g/>
,	,	kIx,	,
podmínek	podmínka	k1gFnPc2	podmínka
v	v	k7c6	v
severním	severní	k2eAgInSc6d1	severní
Atlantiku	Atlantik	k1gInSc6	Atlantik
fouká	foukat	k5eAaImIp3nS	foukat
vítr	vítr	k1gInSc1	vítr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
žene	hnát	k5eAaImIp3nS	hnát
vlny	vlna	k1gFnSc2	vlna
na	na	k7c4	na
základnu	základna	k1gFnSc4	základna
ledovců	ledovec	k1gInPc2	ledovec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
tříští	tříštit	k5eAaImIp3nS	tříštit
a	a	k8xC	a
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
pěnu	pěna	k1gFnSc4	pěna
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
viditelná	viditelný	k2eAgFnSc1d1	viditelná
a	a	k8xC	a
upozorňuje	upozorňovat	k5eAaImIp3nS	upozorňovat
na	na	k7c4	na
ledovce	ledovec	k1gInPc4	ledovec
i	i	k9	i
za	za	k7c4	za
bezměsíčné	bezměsíčný	k2eAgFnPc4d1	bezměsíčná
noci	noc	k1gFnPc4	noc
<g/>
.	.	kIx.	.
</s>
<s>
Vítr	vítr	k1gInSc1	vítr
vanoucí	vanoucí	k2eAgInSc1d1	vanoucí
od	od	k7c2	od
severozápadu	severozápad	k1gInSc2	severozápad
a	a	k8xC	a
nízká	nízký	k2eAgFnSc1d1	nízká
teplota	teplota	k1gFnSc1	teplota
blízko	blízko	k7c2	blízko
nuly	nula	k1gFnSc2	nula
jsou	být	k5eAaImIp3nP	být
nepříjemné	příjemný	k2eNgFnPc1d1	nepříjemná
pro	pro	k7c4	pro
pozorovací	pozorovací	k2eAgFnPc4d1	pozorovací
hlídky	hlídka	k1gFnPc4	hlídka
<g/>
;	;	kIx,	;
taktéž	taktéž	k?	taktéž
vítr	vítr	k1gInSc1	vítr
mohl	moct	k5eAaImAgInS	moct
hnát	hnát	k1gInSc4	hnát
ledovce	ledovec	k1gInSc2	ledovec
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
proti	proti	k7c3	proti
lodi	loď	k1gFnSc3	loď
<g/>
.	.	kIx.	.
</s>
<s>
Srážka	srážka	k1gFnSc1	srážka
s	s	k7c7	s
ledovcem	ledovec	k1gInSc7	ledovec
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
způsobila	způsobit	k5eAaPmAgFnS	způsobit
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
trhlinu	trhlina	k1gFnSc4	trhlina
v	v	k7c6	v
trupu	trup	k1gInSc6	trup
Titanicu	Titanicus	k1gInSc2	Titanicus
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
šesti	šest	k4xCc2	šest
komor	komora	k1gFnPc2	komora
<g/>
,	,	kIx,	,
způsobila	způsobit	k5eAaPmAgFnS	způsobit
poškození	poškození	k1gNnSc3	poškození
trupu	trup	k1gInSc2	trup
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
větším	veliký	k2eAgInSc6d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
konstruktéři	konstruktér	k1gMnPc1	konstruktér
při	při	k7c6	při
projektování	projektování	k1gNnSc6	projektování
trupu	trup	k1gInSc2	trup
očekávali	očekávat	k5eAaImAgMnP	očekávat
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
dokumentu	dokument	k1gInSc2	dokument
"	"	kIx"	"
<g/>
Titanic	Titanic	k1gInSc1	Titanic
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
New	New	k1gFnSc1	New
Evidence	evidence	k1gFnSc1	evidence
<g/>
"	"	kIx"	"
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
příčinou	příčina	k1gFnSc7	příčina
potopení	potopení	k1gNnSc2	potopení
plavidla	plavidlo	k1gNnSc2	plavidlo
požár	požár	k1gInSc1	požár
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
před	před	k7c7	před
vyplutím	vyplutí	k1gNnSc7	vyplutí
ze	z	k7c2	z
Southamptonu	Southampton	k1gInSc2	Southampton
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
v	v	k7c6	v
podpalubním	podpalubní	k2eAgInSc6d1	podpalubní
uhelném	uhelný	k2eAgInSc6d1	uhelný
bunkru	bunkr	k1gInSc6	bunkr
č.	č.	k?	č.
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Požárem	požár	k1gInSc7	požár
způsobené	způsobený	k2eAgFnSc2d1	způsobená
vysoké	vysoký	k2eAgFnSc2d1	vysoká
teploty	teplota	k1gFnSc2	teplota
mohly	moct	k5eAaImAgFnP	moct
snížit	snížit	k5eAaPmF	snížit
pevnost	pevnost	k1gFnSc4	pevnost
trupu	trup	k1gInSc2	trup
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
následně	následně	k6eAd1	následně
fatálně	fatálně	k6eAd1	fatálně
poškodil	poškodit	k5eAaPmAgInS	poškodit
ledovec	ledovec	k1gInSc1	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
zachráněných	zachráněný	k2eAgInPc2d1	zachráněný
topičů	topič	k1gInPc2	topič
J.	J.	kA	J.
Dilley	Dillea	k1gFnSc2	Dillea
později	pozdě	k6eAd2	pozdě
vypověděl	vypovědět	k5eAaPmAgMnS	vypovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nemohli	moct	k5eNaImAgMnP	moct
jsme	být	k5eAaImIp1nP	být
dostat	dostat	k5eAaPmF	dostat
oheň	oheň	k1gInSc4	oheň
pod	pod	k7c4	pod
kontrolu	kontrola	k1gFnSc4	kontrola
a	a	k8xC	a
mezi	mezi	k7c7	mezi
topiči	topič	k1gMnPc7	topič
se	se	k3xPyFc4	se
říkalo	říkat	k5eAaImAgNnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
až	až	k9	až
přistaneme	přistat	k5eAaPmIp1nP	přistat
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
musíme	muset	k5eAaImIp1nP	muset
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
pasažérů	pasažér	k1gMnPc2	pasažér
hned	hned	k6eAd1	hned
vyprázdnit	vyprázdnit	k5eAaPmF	vyprázdnit
ostatní	ostatní	k2eAgFnPc4d1	ostatní
velké	velký	k2eAgFnPc4d1	velká
uhelny	uhelna	k1gFnPc4	uhelna
a	a	k8xC	a
pak	pak	k6eAd1	pak
přivolat	přivolat	k5eAaPmF	přivolat
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
hasičské	hasičský	k2eAgFnSc2d1	hasičská
lodě	loď	k1gFnSc2	loď
z	z	k7c2	z
tamního	tamní	k2eAgInSc2d1	tamní
přístavu	přístav	k1gInSc2	přístav
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
potopení	potopení	k1gNnSc6	potopení
lodi	loď	k1gFnSc2	loď
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
první	první	k4xOgFnPc1	první
snahy	snaha	k1gFnPc1	snaha
o	o	k7c4	o
nalezení	nalezení	k1gNnSc4	nalezení
lodi	loď	k1gFnSc2	loď
a	a	k8xC	a
vyzvednutí	vyzvednutí	k1gNnSc1	vyzvednutí
jejího	její	k3xOp3gInSc2	její
trupu	trup	k1gInSc2	trup
<g/>
.	.	kIx.	.
</s>
<s>
Hloubka	hloubka	k1gFnSc1	hloubka
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
ale	ale	k9	ale
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
čtyř	čtyři	k4xCgInPc2	čtyři
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
tyto	tento	k3xDgInPc1	tento
plány	plán	k1gInPc1	plán
nebyly	být	k5eNaImAgInP	být
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tehdejší	tehdejší	k2eAgFnSc3d1	tehdejší
technice	technika	k1gFnSc3	technika
realizovatelné	realizovatelný	k2eAgFnPc1d1	realizovatelná
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
vážný	vážný	k2eAgInSc1d1	vážný
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
nalezení	nalezení	k1gNnSc4	nalezení
vraku	vrak	k1gInSc2	vrak
Titanicu	Titanicus	k1gInSc2	Titanicus
podnikl	podniknout	k5eAaPmAgMnS	podniknout
texaský	texaský	k2eAgMnSc1d1	texaský
naftař	naftař	k1gMnSc1	naftař
a	a	k8xC	a
multimilionář	multimilionář	k1gMnSc1	multimilionář
Jack	Jack	k1gMnSc1	Jack
Grimm	Grimm	k1gMnSc1	Grimm
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
neuspěl	uspět	k5eNaPmAgInS	uspět
však	však	k9	však
ani	ani	k9	ani
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
dalších	další	k2eAgInPc2d1	další
pokusů	pokus	k1gInPc2	pokus
<g/>
.	.	kIx.	.
</s>
<s>
Následoval	následovat	k5eAaImAgMnS	následovat
jej	on	k3xPp3gMnSc4	on
oceánograf	oceánograf	k1gMnSc1	oceánograf
a	a	k8xC	a
geolog	geolog	k1gMnSc1	geolog
Robert	Robert	k1gMnSc1	Robert
D.	D.	kA	D.
Ballard	Ballard	k1gMnSc1	Ballard
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
expedice	expedice	k1gFnSc1	expedice
objevila	objevit	k5eAaPmAgFnS	objevit
první	první	k4xOgFnPc4	první
trosky	troska	k1gFnPc4	troska
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
Finanční	finanční	k2eAgNnSc1d1	finanční
i	i	k8xC	i
technické	technický	k2eAgNnSc1d1	technické
zajištění	zajištění	k1gNnSc1	zajištění
celé	celý	k2eAgFnSc2d1	celá
operace	operace	k1gFnSc2	operace
spadalo	spadat	k5eAaPmAgNnS	spadat
pod	pod	k7c4	pod
americké	americký	k2eAgNnSc4d1	americké
námořnictvo	námořnictvo	k1gNnSc4	námořnictvo
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
expedice	expedice	k1gFnSc2	expedice
nebylo	být	k5eNaImAgNnS	být
pátrání	pátrání	k1gNnSc1	pátrání
po	po	k7c6	po
Titanicu	Titanicus	k1gInSc6	Titanicus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k6eAd1	především
nalezení	nalezení	k1gNnSc3	nalezení
a	a	k8xC	a
prozkoumání	prozkoumání	k1gNnSc3	prozkoumání
vraků	vrak	k1gInPc2	vrak
dvou	dva	k4xCgFnPc6	dva
potopených	potopený	k2eAgFnPc2d1	potopená
amerických	americký	k2eAgFnPc2d1	americká
jaderných	jaderný	k2eAgFnPc2d1	jaderná
ponorek	ponorka	k1gFnPc2	ponorka
-	-	kIx~	-
USS	USS	kA	USS
Thresher	Threshra	k1gFnPc2	Threshra
a	a	k8xC	a
USS	USS	kA	USS
Scorpion	Scorpion	k1gInSc1	Scorpion
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zmizely	zmizet	k5eAaPmAgFnP	zmizet
i	i	k9	i
s	s	k7c7	s
posádkami	posádka	k1gFnPc7	posádka
(	(	kIx(	(
<g/>
Thresher	Threshra	k1gFnPc2	Threshra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
a	a	k8xC	a
Scorpion	Scorpion	k1gInSc1	Scorpion
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
D.	D.	kA	D.
Ballard	Ballard	k1gMnSc1	Ballard
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
námořnictvu	námořnictvo	k1gNnSc3	námořnictvo
své	svůj	k3xOyFgFnSc2	svůj
bohaté	bohatý	k2eAgFnSc2d1	bohatá
zkušenosti	zkušenost	k1gFnSc2	zkušenost
oceánografa	oceánograf	k1gMnSc2	oceánograf
a	a	k8xC	a
především	především	k6eAd1	především
nové	nový	k2eAgNnSc4d1	nové
video	video	k1gNnSc4	video
technologie	technologie	k1gFnSc1	technologie
k	k	k7c3	k
průzkumu	průzkum	k1gInSc3	průzkum
mořského	mořský	k2eAgNnSc2d1	mořské
dna	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
rozlišovací	rozlišovací	k2eAgFnSc4d1	rozlišovací
schopnost	schopnost	k1gFnSc4	schopnost
a	a	k8xC	a
umožňovaly	umožňovat	k5eAaImAgFnP	umožňovat
detailní	detailní	k2eAgInSc4d1	detailní
průzkum	průzkum	k1gInSc4	průzkum
a	a	k8xC	a
mapování	mapování	k1gNnSc4	mapování
sledované	sledovaný	k2eAgFnSc2d1	sledovaná
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vedením	vedení	k1gNnSc7	vedení
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
Ballard	Ballard	k1gInSc1	Ballard
dohodl	dohodnout	k5eAaPmAgInS	dohodnout
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podaří	podařit	k5eAaPmIp3nS	podařit
objevit	objevit	k5eAaPmF	objevit
a	a	k8xC	a
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
obě	dva	k4xCgFnPc4	dva
potopené	potopený	k2eAgFnPc4d1	potopená
jaderné	jaderný	k2eAgFnPc4d1	jaderná
ponorky	ponorka	k1gFnPc4	ponorka
a	a	k8xC	a
nepřekročí	překročit	k5eNaPmIp3nP	překročit
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
povolený	povolený	k2eAgInSc1d1	povolený
rozpočet	rozpočet	k1gInSc4	rozpočet
<g/>
,	,	kIx,	,
získá	získat	k5eAaPmIp3nS	získat
možnost	možnost	k1gFnSc4	možnost
ve	v	k7c6	v
zbylém	zbylý	k2eAgInSc6d1	zbylý
čase	čas	k1gInSc6	čas
expedice	expedice	k1gFnSc1	expedice
pátrat	pátrat	k5eAaImF	pátrat
i	i	k9	i
po	po	k7c6	po
vraku	vrak	k1gInSc6	vrak
Titanicu	Titanicus	k1gInSc2	Titanicus
<g/>
.	.	kIx.	.
</s>
<s>
Námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
USA	USA	kA	USA
poskytlo	poskytnout	k5eAaPmAgNnS	poskytnout
loď	loď	k1gFnSc4	loď
i	i	k8xC	i
posádku	posádka	k1gFnSc4	posádka
<g/>
,	,	kIx,	,
financovalo	financovat	k5eAaBmAgNnS	financovat
výrobu	výroba	k1gFnSc4	výroba
vlečené	vlečený	k2eAgFnSc2d1	vlečená
plošiny	plošina	k1gFnSc2	plošina
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
byly	být	k5eAaImAgFnP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
videokamery	videokamera	k1gFnPc1	videokamera
a	a	k8xC	a
osvětlení	osvětlení	k1gNnSc1	osvětlení
<g/>
,	,	kIx,	,
a	a	k8xC	a
finančně	finančně	k6eAd1	finančně
zajistilo	zajistit	k5eAaPmAgNnS	zajistit
celou	celá	k1gFnSc4	celá
přísně	přísně	k6eAd1	přísně
utajenou	utajený	k2eAgFnSc4d1	utajená
operaci	operace	k1gFnSc4	operace
<g/>
.	.	kIx.	.
</s>
<s>
Důstojník	důstojník	k1gMnSc1	důstojník
v	v	k7c6	v
záloze	záloha	k1gFnSc6	záloha
Robert	Robert	k1gMnSc1	Robert
D.	D.	kA	D.
Ballard	Ballard	k1gMnSc1	Ballard
byl	být	k5eAaImAgMnS	být
povolán	povolat	k5eAaPmNgMnS	povolat
do	do	k7c2	do
aktivní	aktivní	k2eAgFnSc2d1	aktivní
služby	služba	k1gFnSc2	služba
a	a	k8xC	a
vázán	vázán	k2eAgMnSc1d1	vázán
přísahou	přísaha	k1gFnSc7	přísaha
mlčenlivosti	mlčenlivost	k1gFnSc2	mlčenlivost
<g/>
.	.	kIx.	.
</s>
<s>
Vraky	vrak	k1gInPc1	vrak
obou	dva	k4xCgFnPc2	dva
ponorek	ponorka	k1gFnPc2	ponorka
se	se	k3xPyFc4	se
týmu	tým	k1gInSc2	tým
nakonec	nakonec	k6eAd1	nakonec
podařilo	podařit	k5eAaPmAgNnS	podařit
objevit	objevit	k5eAaPmF	objevit
a	a	k8xC	a
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
hledání	hledání	k1gNnSc6	hledání
Titanicu	Titanicus	k1gInSc2	Titanicus
zbylo	zbýt	k5eAaPmAgNnS	zbýt
jen	jen	k9	jen
posledních	poslední	k2eAgInPc2d1	poslední
čtrnáct	čtrnáct	k4xCc4	čtrnáct
dnů	den	k1gInPc2	den
pátrání	pátrání	k1gNnSc2	pátrání
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
Ballard	Ballard	k1gInSc1	Ballard
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
cílovou	cílový	k2eAgFnSc4d1	cílová
oblast	oblast	k1gFnSc4	oblast
pátrání	pátrání	k1gNnSc2	pátrání
prohledával	prohledávat	k5eAaImAgInS	prohledávat
v	v	k7c6	v
navazujících	navazující	k2eAgInPc6d1	navazující
pásech	pás	k1gInPc6	pás
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
kterými	který	k3yIgInPc7	který
ale	ale	k8xC	ale
nechával	nechávat	k5eAaImAgMnS	nechávat
mílové	mílový	k2eAgInPc4d1	mílový
rozestupy	rozestup	k1gInPc4	rozestup
a	a	k8xC	a
doufal	doufat	k5eAaImAgMnS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
na	na	k7c4	na
stopu	stopa	k1gFnSc4	stopa
vraku	vrak	k1gInSc2	vrak
přivede	přivést	k5eAaPmIp3nS	přivést
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
pás	pás	k1gInSc1	pás
trosek	troska	k1gFnPc2	troska
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
Ballardova	Ballardův	k2eAgInSc2d1	Ballardův
odhadu	odhad	k1gInSc2	odhad
musel	muset	k5eAaImAgMnS	muset
vytvořit	vytvořit	k5eAaPmF	vytvořit
po	po	k7c4	po
rozlomení	rozlomení	k1gNnSc4	rozlomení
potápějící	potápějící	k2eAgFnSc2d1	potápějící
se	se	k3xPyFc4	se
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
taktika	taktika	k1gFnSc1	taktika
se	se	k3xPyFc4	se
ukázala	ukázat	k5eAaPmAgFnS	ukázat
být	být	k5eAaImF	být
správná	správný	k2eAgFnSc1d1	správná
a	a	k8xC	a
pouhé	pouhý	k2eAgInPc4d1	pouhý
čtyři	čtyři	k4xCgInPc4	čtyři
dny	den	k1gInPc4	den
před	před	k7c7	před
ukončením	ukončení	k1gNnSc7	ukončení
expedice	expedice	k1gFnSc1	expedice
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
robot	robot	k1gMnSc1	robot
Argo	Argo	k1gMnSc1	Argo
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
kamery	kamera	k1gFnPc1	kamera
první	první	k4xOgFnSc2	první
stopy	stopa	k1gFnSc2	stopa
po	po	k7c6	po
vraku	vrak	k1gInSc6	vrak
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Ballard	Ballard	k1gMnSc1	Ballard
na	na	k7c6	na
pozdější	pozdní	k2eAgFnSc6d2	pozdější
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
objevil	objevit	k5eAaPmAgInS	objevit
vrak	vrak	k1gInSc4	vrak
Titanicu	Titanicus	k1gInSc2	Titanicus
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
nucen	nutit	k5eAaImNgMnS	nutit
zatajit	zatajit	k5eAaPmF	zatajit
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
pravém	pravý	k2eAgInSc6d1	pravý
účelu	účel	k1gInSc6	účel
expedice	expedice	k1gFnSc2	expedice
a	a	k8xC	a
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
americké	americký	k2eAgNnSc4d1	americké
námořnictvo	námořnictvo	k1gNnSc4	námořnictvo
pouze	pouze	k6eAd1	pouze
testoval	testovat	k5eAaImAgMnS	testovat
nové	nový	k2eAgFnPc4d1	nová
technologie	technologie	k1gFnPc4	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozlomení	rozlomení	k1gNnSc6	rozlomení
trupu	trup	k1gInSc2	trup
se	se	k3xPyFc4	se
každá	každý	k3xTgFnSc1	každý
část	část	k1gFnSc1	část
potápěla	potápět	k5eAaImAgFnS	potápět
odlišně	odlišně	k6eAd1	odlišně
<g/>
.	.	kIx.	.
</s>
<s>
Přední	přední	k2eAgFnSc1d1	přední
část	část	k1gFnSc1	část
poměrně	poměrně	k6eAd1	poměrně
měkce	měkko	k6eAd1	měkko
dosedla	dosednout	k5eAaPmAgFnS	dosednout
na	na	k7c4	na
dno	dno	k1gNnSc4	dno
<g/>
,	,	kIx,	,
zadní	zadní	k2eAgFnSc1d1	zadní
část	část	k1gFnSc1	část
klesala	klesat	k5eAaImAgFnS	klesat
ke	k	k7c3	k
dnu	dno	k1gNnSc3	dno
daleko	daleko	k6eAd1	daleko
rychleji	rychle	k6eAd2	rychle
<g/>
,	,	kIx,	,
i	i	k8xC	i
vlivem	vliv	k1gInSc7	vliv
imploze	imploze	k1gFnSc2	imploze
stlačeného	stlačený	k2eAgInSc2d1	stlačený
vzduchu	vzduch	k1gInSc2	vzduch
ve	v	k7c6	v
vodotěsných	vodotěsný	k2eAgFnPc6d1	vodotěsná
komorách	komora	k1gFnPc6	komora
na	na	k7c6	na
zádi	záď	k1gFnSc6	záď
<g/>
.	.	kIx.	.
</s>
<s>
Zadní	zadní	k2eAgFnSc1d1	zadní
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
zaryla	zarýt	k5eAaPmAgFnS	zarýt
hluboko	hluboko	k6eAd1	hluboko
do	do	k7c2	do
bahna	bahno	k1gNnSc2	bahno
<g/>
.	.	kIx.	.
</s>
<s>
Jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejzajímavější	zajímavý	k2eAgNnSc1d3	nejzajímavější
částí	část	k1gFnSc7	část
trosek	troska	k1gFnPc2	troska
<g/>
,	,	kIx,	,
objevených	objevený	k2eAgFnPc2d1	objevená
týmem	tým	k1gInSc7	tým
Roberta	Robert	k1gMnSc2	Robert
D.	D.	kA	D.
Ballarda	Ballard	k1gMnSc2	Ballard
<g/>
,	,	kIx,	,
představoval	představovat	k5eAaImAgInS	představovat
lodní	lodní	k2eAgInSc1d1	lodní
kotel	kotel	k1gInSc1	kotel
na	na	k7c6	na
souřadnicích	souřadnice	k1gFnPc6	souřadnice
O	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
nalezena	naleznout	k5eAaPmNgFnS	naleznout
přední	přední	k2eAgFnSc1d1	přední
část	část	k1gFnSc1	část
trupu	trup	k1gInSc2	trup
(	(	kIx(	(
<g/>
prokázalo	prokázat	k5eAaPmAgNnS	prokázat
se	se	k3xPyFc4	se
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
měla	mít	k5eAaImAgFnS	mít
pravdu	pravda	k1gFnSc4	pravda
ta	ten	k3xDgNnPc4	ten
svědectví	svědectví	k1gNnSc4	svědectví
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
vyšetřovací	vyšetřovací	k2eAgFnSc7d1	vyšetřovací
komisí	komise	k1gFnSc7	komise
odmítnuta	odmítnut	k2eAgFnSc1d1	odmítnuta
a	a	k8xC	a
která	který	k3yRgFnSc1	který
tvrdila	tvrdit	k5eAaImAgFnS	tvrdit
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
že	že	k8xS	že
trup	trup	k1gInSc1	trup
lodi	loď	k1gFnSc2	loď
se	se	k3xPyFc4	se
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
potopením	potopení	k1gNnSc7	potopení
rozlomil	rozlomit	k5eAaPmAgMnS	rozlomit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
)	)	kIx)	)
na	na	k7c6	na
pozici	pozice	k1gFnSc6	pozice
Ještě	ještě	k9	ještě
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
nalezena	nalézt	k5eAaBmNgFnS	nalézt
i	i	k9	i
záď	záď	k1gFnSc1	záď
ležící	ležící	k2eAgFnSc1d1	ležící
600	[number]	k4	600
stop	stopa	k1gFnPc2	stopa
od	od	k7c2	od
přídě	příď	k1gFnSc2	příď
na	na	k7c6	na
souřadnicích	souřadnice	k1gFnPc6	souřadnice
Za	za	k7c4	za
místo	místo	k1gNnSc4	místo
potopení	potopení	k1gNnSc1	potopení
Titanicu	Titanicus	k1gInSc2	Titanicus
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
brát	brát	k5eAaImF	brát
polohu	poloha	k1gFnSc4	poloha
nalezeného	nalezený	k2eAgInSc2d1	nalezený
kotle	kotel	k1gInSc2	kotel
<g/>
,	,	kIx,	,
u	u	k7c2	u
něhož	jenž	k3xRgNnSc2	jenž
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
uvolnil	uvolnit	k5eAaPmAgInS	uvolnit
při	při	k7c6	při
rozlomení	rozlomení	k1gNnSc6	rozlomení
Titanicu	Titanicus	k1gInSc2	Titanicus
a	a	k8xC	a
klesal	klesat	k5eAaImAgInS	klesat
přímo	přímo	k6eAd1	přímo
ke	k	k7c3	k
dnu	dno	k1gNnSc3	dno
<g/>
.	.	kIx.	.
</s>
<s>
Bohužel	bohužel	k9	bohužel
neexistuje	existovat	k5eNaImIp3nS	existovat
přímý	přímý	k2eAgInSc4d1	přímý
důkaz	důkaz	k1gInSc4	důkaz
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
fázi	fáze	k1gFnSc6	fáze
potápění	potápění	k1gNnSc2	potápění
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
úplnému	úplný	k2eAgNnSc3d1	úplné
oddělení	oddělení	k1gNnSc3	oddělení
obou	dva	k4xCgInPc2	dva
částí	část	k1gFnPc2	část
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
jaké	jaký	k3yIgFnSc6	jaký
hloubce	hloubka	k1gFnSc6	hloubka
během	během	k7c2	během
potápění	potápění	k1gNnSc2	potápění
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
příď	příď	k1gFnSc1	příď
je	být	k5eAaImIp3nS	být
zabořena	zabořit	k5eAaPmNgFnS	zabořit
v	v	k7c6	v
bahně	bahno	k1gNnSc6	bahno
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
možno	možno	k6eAd1	možno
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
celou	celý	k2eAgFnSc4d1	celá
trhlinu	trhlina	k1gFnSc4	trhlina
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
třetí	třetí	k4xOgFnSc2	třetí
přepážky	přepážka	k1gFnSc2	přepážka
dál	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
inkriminovaná	inkriminovaný	k2eAgFnSc1d1	inkriminovaná
část	část	k1gFnSc1	část
trupu	trup	k1gInSc2	trup
přístupná	přístupný	k2eAgFnSc1d1	přístupná
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenán	k2eAgNnSc1d1	zaznamenáno
rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
poškození	poškození	k1gNnSc1	poškození
zjevně	zjevně	k6eAd1	zjevně
související	související	k2eAgMnSc1d1	související
s	s	k7c7	s
nárazem	náraz	k1gInSc7	náraz
na	na	k7c4	na
kru	kra	k1gFnSc4	kra
<g/>
.	.	kIx.	.
</s>
<s>
Nešlo	jít	k5eNaImAgNnS	jít
o	o	k7c4	o
celistvou	celistvý	k2eAgFnSc4d1	celistvá
trhlinu	trhlina	k1gFnSc4	trhlina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
vyrvané	vyrvaný	k2eAgInPc4d1	vyrvaný
nýty	nýt	k1gInPc4	nýt
a	a	k8xC	a
rozestoupení	rozestoupení	k1gNnSc4	rozestoupení
a	a	k8xC	a
zprohýbání	zprohýbání	k1gNnSc4	zprohýbání
ocelových	ocelový	k2eAgInPc2d1	ocelový
plátů	plát	k1gInPc2	plát
tvořících	tvořící	k2eAgFnPc2d1	tvořící
trup	trupa	k1gFnPc2	trupa
<g/>
.	.	kIx.	.
</s>
<s>
Experti	expert	k1gMnPc1	expert
došli	dojít	k5eAaPmAgMnP	dojít
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
vrak	vrak	k1gInSc1	vrak
nelze	lze	k6eNd1	lze
vyzvednout	vyzvednout	k5eAaPmF	vyzvednout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
poškozen	poškodit	k5eAaPmNgInS	poškodit
korozí	koroze	k1gFnSc7	koroze
a	a	k8xC	a
při	při	k7c6	při
zvedání	zvedání	k1gNnSc6	zvedání
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
nutně	nutně	k6eAd1	nutně
rozpadl	rozpadnout	k5eAaPmAgInS	rozpadnout
na	na	k7c4	na
kusy	kus	k1gInPc4	kus
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výzkumů	výzkum	k1gInPc2	výzkum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
přispívají	přispívat	k5eAaImIp3nP	přispívat
k	k	k7c3	k
rozpadu	rozpad	k1gInSc3	rozpad
vraku	vrak	k1gInSc2	vrak
významně	významně	k6eAd1	významně
i	i	k9	i
nově	nova	k1gFnSc3	nova
objevené	objevený	k2eAgFnSc2d1	objevená
bakterie	bakterie	k1gFnSc2	bakterie
Halmonas	Halmonasa	k1gFnPc2	Halmonasa
titanicae	titanica	k1gFnSc2	titanica
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
vrak	vrak	k1gInSc1	vrak
navštíven	navštívit	k5eAaPmNgInS	navštívit
ještě	ještě	k6eAd1	ještě
několikrát	několikrát	k6eAd1	několikrát
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
francouzská	francouzský	k2eAgFnSc1d1	francouzská
výprava	výprava	k1gFnSc1	výprava
vyzvedla	vyzvednout	k5eAaPmAgFnS	vyzvednout
několik	několik	k4yIc4	několik
set	sto	k4xCgNnPc2	sto
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
setkalo	setkat	k5eAaPmAgNnS	setkat
s	s	k7c7	s
hlubokým	hluboký	k2eAgInSc7d1	hluboký
nesouhlasem	nesouhlas	k1gInSc7	nesouhlas
zejména	zejména	k9	zejména
americké	americký	k2eAgFnSc2d1	americká
a	a	k8xC	a
britské	britský	k2eAgFnSc2d1	britská
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
žijících	žijící	k2eAgMnPc2d1	žijící
účastníků	účastník	k1gMnPc2	účastník
katastrofy	katastrofa	k1gFnSc2	katastrofa
a	a	k8xC	a
příbuzných	příbuzný	k2eAgFnPc2d1	příbuzná
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
expedice	expedice	k1gFnSc1	expedice
byla	být	k5eAaImAgFnS	být
označena	označit	k5eAaPmNgFnS	označit
za	za	k7c4	za
vykradače	vykradač	k1gMnPc4	vykradač
hrobů	hrob	k1gInPc2	hrob
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
vzneseny	vznést	k5eAaPmNgInP	vznést
požadavky	požadavek	k1gInPc1	požadavek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
Titanicu	Titanic	k1gMnSc3	Titanic
dostalo	dostat	k5eAaPmAgNnS	dostat
oficiálního	oficiální	k2eAgNnSc2d1	oficiální
prohlášení	prohlášení	k1gNnSc2	prohlášení
za	za	k7c4	za
hrob	hrob	k1gInSc4	hrob
a	a	k8xC	a
památník	památník	k1gInSc4	památník
neštěstí	neštěstí	k1gNnSc2	neštěstí
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
ale	ale	k9	ale
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
mezinárodních	mezinárodní	k2eAgFnPc6d1	mezinárodní
vodách	voda	k1gFnPc6	voda
a	a	k8xC	a
nemá	mít	k5eNaImIp3nS	mít
majitele	majitel	k1gMnPc4	majitel
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgMnSc7d1	jediný
oficiálním	oficiální	k2eAgMnSc7d1	oficiální
držitelem	držitel	k1gMnSc7	držitel
práv	právo	k1gNnPc2	právo
na	na	k7c4	na
vyzvedávání	vyzvedávání	k1gNnSc4	vyzvedávání
artefaktů	artefakt	k1gInPc2	artefakt
z	z	k7c2	z
Titanicu	Titanicus	k1gInSc2	Titanicus
je	být	k5eAaImIp3nS	být
společnost	společnost	k1gFnSc1	společnost
RMS	RMS	kA	RMS
Titanic	Titanic	k1gInSc1	Titanic
<g/>
,	,	kIx,	,
Inc	Inc	k1gFnSc1	Inc
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gMnPc1	jejíž
pracovníci	pracovník	k1gMnPc1	pracovník
se	se	k3xPyFc4	se
k	k	k7c3	k
vraku	vrak	k1gInSc3	vrak
potápějí	potápět	k5eAaImIp3nP	potápět
a	a	k8xC	a
zkoumají	zkoumat	k5eAaImIp3nP	zkoumat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
vrak	vrak	k1gInSc1	vrak
od	od	k7c2	od
předešlých	předešlý	k2eAgNnPc2d1	předešlé
let	léto	k1gNnPc2	léto
změnil	změnit	k5eAaPmAgInS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Kongres	kongres	k1gInSc1	kongres
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
schválil	schválit	k5eAaPmAgInS	schválit
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
prodej	prodej	k1gInSc1	prodej
a	a	k8xC	a
vystavování	vystavování	k1gNnSc1	vystavování
předmětů	předmět	k1gInPc2	předmět
vyzvednutých	vyzvednutý	k2eAgInPc2d1	vyzvednutý
z	z	k7c2	z
Titanicu	Titanicus	k1gInSc2	Titanicus
na	na	k7c6	na
území	území	k1gNnSc6	území
USA	USA	kA	USA
a	a	k8xC	a
vyzval	vyzvat	k5eAaPmAgInS	vyzvat
k	k	k7c3	k
respektování	respektování	k1gNnSc3	respektování
vraku	vrak	k1gInSc2	vrak
jako	jako	k8xC	jako
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
památníku	památník	k1gInSc2	památník
a	a	k8xC	a
hrobu	hrob	k1gInSc2	hrob
1	[number]	k4	1
517	[number]	k4	517
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
–	–	k?	–
<g/>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Robert	Robert	k1gMnSc1	Robert
Ballard	Ballard	k1gMnSc1	Ballard
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
září	září	k1gNnSc1	září
1985	[number]	k4	1985
Jako	jako	k8xC	jako
každá	každý	k3xTgFnSc1	každý
velká	velký	k2eAgFnSc1d1	velká
tragédie	tragédie	k1gFnSc1	tragédie
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
pozornosti	pozornost	k1gFnSc2	pozornost
a	a	k8xC	a
publicity	publicita	k1gFnSc2	publicita
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
ztroskotání	ztroskotání	k1gNnSc1	ztroskotání
Titanicu	Titanicus	k1gInSc2	Titanicus
přitáhlo	přitáhnout	k5eAaPmAgNnS	přitáhnout
k	k	k7c3	k
sobě	se	k3xPyFc3	se
velkou	velký	k2eAgFnSc4d1	velká
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
<g/>
,	,	kIx,	,
objeví	objevit	k5eAaPmIp3nS	objevit
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
napsaná	napsaný	k2eAgFnSc1d1	napsaná
kniha	kniha	k1gFnSc1	kniha
nebo	nebo	k8xC	nebo
povídka	povídka	k1gFnSc1	povídka
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
obsahu	obsah	k1gInSc6	obsah
je	být	k5eAaImIp3nS	být
popsáno	popsán	k2eAgNnSc1d1	popsáno
ztroskotání	ztroskotání	k1gNnSc1	ztroskotání
podobné	podobný	k2eAgFnSc2d1	podobná
lodi	loď	k1gFnSc2	loď
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
lze	lze	k6eAd1	lze
vysledovat	vysledovat	k5eAaPmF	vysledovat
mystické	mystický	k2eAgFnPc4d1	mystická
souvislosti	souvislost	k1gFnPc4	souvislost
mezi	mezi	k7c7	mezi
událostmi	událost	k1gFnPc7	událost
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
...	...	k?	...
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
vydal	vydat	k5eAaPmAgMnS	vydat
Morgan	morgan	k1gMnSc1	morgan
Robertson	Robertson	k1gMnSc1	Robertson
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
námořník	námořník	k1gMnSc1	námořník
<g/>
,	,	kIx,	,
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
M.	M.	kA	M.
F.	F.	kA	F.
Mansfield	Mansfield	k1gMnSc1	Mansfield
povídku	povídka	k1gFnSc4	povídka
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
popisováno	popisovat	k5eAaImNgNnS	popisovat
ztroskotání	ztroskotání	k1gNnSc1	ztroskotání
"	"	kIx"	"
<g/>
nepotopitelné	potopitelný	k2eNgNnSc1d1	nepotopitelné
<g/>
"	"	kIx"	"
a	a	k8xC	a
největší	veliký	k2eAgFnPc4d3	veliký
dosud	dosud	k6eAd1	dosud
postavené	postavený	k2eAgFnPc4d1	postavená
lodi	loď	k1gFnPc4	loď
Titan	titan	k1gInSc1	titan
podobných	podobný	k2eAgInPc2d1	podobný
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
,	,	kIx,	,
parametrů	parametr	k1gInPc2	parametr
a	a	k8xC	a
konstrukce	konstrukce	k1gFnSc1	konstrukce
jako	jako	k8xC	jako
Titanic	Titanic	k1gInSc1	Titanic
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
povídce	povídka	k1gFnSc6	povídka
loď	loď	k1gFnSc1	loď
na	na	k7c6	na
plavbě	plavba	k1gFnSc6	plavba
(	(	kIx(	(
<g/>
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
<g/>
)	)	kIx)	)
narazila	narazit	k5eAaPmAgFnS	narazit
na	na	k7c4	na
kru	kra	k1gFnSc4	kra
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
se	se	k3xPyFc4	se
zaplavovat	zaplavovat	k5eAaImF	zaplavovat
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
potopila	potopit	k5eAaPmAgFnS	potopit
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
událost	událost	k1gFnSc1	událost
stála	stát	k5eAaImAgFnS	stát
mnoho	mnoho	k4c4	mnoho
životů	život	k1gInPc2	život
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nebylo	být	k5eNaImAgNnS	být
dostatek	dostatek	k1gInSc4	dostatek
záchranných	záchranný	k2eAgInPc2d1	záchranný
člunů	člun	k1gInPc2	člun
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
spojení	spojení	k1gNnSc1	spojení
Titanic	Titanic	k1gInSc1	Titanic
-	-	kIx~	-
"	"	kIx"	"
<g/>
nepotopitelný	potopitelný	k2eNgInSc1d1	nepotopitelný
<g/>
"	"	kIx"	"
po	po	k7c6	po
katastrofě	katastrofa	k1gFnSc6	katastrofa
objevovalo	objevovat	k5eAaImAgNnS	objevovat
téměř	téměř	k6eAd1	téměř
neustále	neustále	k6eAd1	neustále
a	a	k8xC	a
přežívá	přežívat	k5eAaImIp3nS	přežívat
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
pravdou	pravda	k1gFnSc7	pravda
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
první	první	k4xOgFnSc7	první
plavbou	plavba	k1gFnSc7	plavba
takto	takto	k6eAd1	takto
označován	označován	k2eAgMnSc1d1	označován
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
společností	společnost	k1gFnPc2	společnost
White	Whit	k1gInSc5	Whit
Star	Star	kA	Star
Line	linout	k5eAaImIp3nS	linout
nebo	nebo	k8xC	nebo
loděnicí	loděnice	k1gFnSc7	loděnice
Harland	Harlanda	k1gFnPc2	Harlanda
and	and	k?	and
Wolff	Wolff	k1gMnSc1	Wolff
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
často	často	k6eAd1	často
zmiňovaného	zmiňovaný	k2eAgInSc2d1	zmiňovaný
lodního	lodní	k2eAgInSc2d1	lodní
orchestru	orchestr	k1gInSc2	orchestr
je	být	k5eAaImIp3nS	být
sporné	sporný	k2eAgNnSc1d1	sporné
<g/>
,	,	kIx,	,
hrál	hrát	k5eAaImAgMnS	hrát
<g/>
-li	i	k?	-li
až	až	k8xS	až
do	do	k7c2	do
poslední	poslední	k2eAgFnSc2d1	poslední
chvíle	chvíle	k1gFnSc2	chvíle
na	na	k7c6	na
člunové	člunový	k2eAgFnSc6d1	člunová
palubě	paluba	k1gFnSc6	paluba
a	a	k8xC	a
hrál	hrát	k5eAaImAgMnS	hrát
<g/>
-li	i	k?	-li
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
katastrofy	katastrofa	k1gFnSc2	katastrofa
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jaké	jaký	k3yIgNnSc4	jaký
hrál	hrát	k5eAaImAgMnS	hrát
skladby	skladba	k1gFnPc4	skladba
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
věta	věta	k1gFnSc1	věta
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
hudebníků	hudebník	k1gMnPc2	hudebník
"	"	kIx"	"
<g/>
Pánové	pán	k1gMnPc1	pán
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
mi	já	k3xPp1nSc3	já
ctí	ctít	k5eAaImIp3nP	ctít
s	s	k7c7	s
vámi	vy	k3xPp2nPc7	vy
dnes	dnes	k6eAd1	dnes
hrát	hrát	k5eAaImF	hrát
<g/>
"	"	kIx"	"
ve	v	k7c6	v
filmu	film	k1gInSc6	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
je	být	k5eAaImIp3nS	být
působivá	působivý	k2eAgFnSc1d1	působivá
<g/>
.	.	kIx.	.
</s>
<s>
Irští	irský	k2eAgMnPc1d1	irský
katoličtí	katolický	k2eAgMnPc1d1	katolický
dělníci	dělník	k1gMnPc1	dělník
si	se	k3xPyFc3	se
s	s	k7c7	s
hrůzou	hrůza	k1gFnSc7	hrůza
šeptali	šeptat	k5eAaImAgMnP	šeptat
o	o	k7c6	o
údajném	údajný	k2eAgNnSc6d1	údajné
sériovém	sériový	k2eAgNnSc6d1	sériové
čísle	číslo	k1gNnSc6	číslo
Titanicu	Titanicus	k1gInSc2	Titanicus
3909	[number]	k4	3909
04	[number]	k4	04
(	(	kIx(	(
<g/>
Titanic	Titanic	k1gInSc1	Titanic
měl	mít	k5eAaImAgInS	mít
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
sériové	sériový	k2eAgNnSc4d1	sériové
číslo	číslo	k1gNnSc4	číslo
401	[number]	k4	401
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
z	z	k7c2	z
paluby	paluba	k1gFnSc2	paluba
pak	pak	k6eAd1	pak
zrcadlově	zrcadlově	k6eAd1	zrcadlově
odražené	odražený	k2eAgNnSc1d1	odražené
číslo	číslo	k1gNnSc1	číslo
od	od	k7c2	od
vody	voda	k1gFnSc2	voda
vypadá	vypadat	k5eAaPmIp3nS	vypadat
jako	jako	k9	jako
nápis	nápis	k1gInSc1	nápis
"	"	kIx"	"
<g/>
no	no	k9	no
pope	pop	k1gMnSc5	pop
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
volně	volně	k6eAd1	volně
přeloženo	přeložen	k2eAgNnSc1d1	přeloženo
"	"	kIx"	"
<g/>
papež	papež	k1gMnSc1	papež
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Číslo	číslo	k1gNnSc1	číslo
3909	[number]	k4	3909
04	[number]	k4	04
si	se	k3xPyFc3	se
vymysleli	vymyslet	k5eAaPmAgMnP	vymyslet
protestantští	protestantský	k2eAgMnPc1d1	protestantský
dělníci	dělník	k1gMnPc1	dělník
loděnice	loděnice	k1gFnSc2	loděnice
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
postrašili	postrašit	k5eAaPmAgMnP	postrašit
katolické	katolický	k2eAgMnPc4d1	katolický
dělníky	dělník	k1gMnPc4	dělník
<g/>
.	.	kIx.	.
</s>
<s>
Omylem	omylem	k6eAd1	omylem
je	být	k5eAaImIp3nS	být
i	i	k9	i
údajně	údajně	k6eAd1	údajně
první	první	k4xOgNnSc1	první
vysílání	vysílání	k1gNnSc1	vysílání
signálu	signál	k1gInSc2	signál
"	"	kIx"	"
<g/>
SOS	sos	k1gInSc1	sos
<g/>
"	"	kIx"	"
právě	právě	k9	právě
z	z	k7c2	z
paluby	paluba	k1gFnSc2	paluba
potápějícího	potápějící	k2eAgInSc2d1	potápějící
se	se	k3xPyFc4	se
Titanicu	Titanicus	k1gInSc2	Titanicus
<g/>
,	,	kIx,	,
kterému	který	k3yQgNnSc3	který
okolní	okolní	k2eAgFnPc1d1	okolní
lodě	loď	k1gFnPc1	loď
nerozuměly	rozumět	k5eNaImAgFnP	rozumět
<g/>
.	.	kIx.	.
</s>
<s>
Volání	volání	k1gNnSc1	volání
"	"	kIx"	"
<g/>
SOS	sos	k1gInSc1	sos
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
v	v	k7c6	v
Morseově	Morseův	k2eAgFnSc6d1	Morseova
abecedě	abeceda	k1gFnSc6	abeceda
·	·	k?	·
·	·	k?	·
·	·	k?	·
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
·	·	k?	·
·	·	k?	·
·	·	k?	·
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
mezinárodně	mezinárodně	k6eAd1	mezinárodně
používáno	používat	k5eAaImNgNnS	používat
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
souběžně	souběžně	k6eAd1	souběžně
používalo	používat	k5eAaImAgNnS	používat
i	i	k9	i
starší	starý	k2eAgNnSc1d2	starší
volání	volání	k1gNnSc1	volání
"	"	kIx"	"
<g/>
CQD	CQD	kA	CQD
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
v	v	k7c6	v
Morseově	Morseův	k2eAgFnSc6d1	Morseova
abecedě	abeceda	k1gFnSc6	abeceda
-	-	kIx~	-
·	·	k?	·
-	-	kIx~	-
·	·	k?	·
-	-	kIx~	-
-	-	kIx~	-
·	·	k?	·
-	-	kIx~	-
-	-	kIx~	-
·	·	k?	·
·	·	k?	·
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Titanicem	Titanic	k1gMnSc7	Titanic
je	být	k5eAaImIp3nS	být
spojováno	spojovat	k5eAaImNgNnS	spojovat
i	i	k9	i
prokletí	prokletí	k1gNnSc1	prokletí
pro	pro	k7c4	pro
údajnou	údajný	k2eAgFnSc4d1	údajná
přepravu	přeprava	k1gFnSc4	přeprava
starodávné	starodávný	k2eAgFnSc2d1	starodávná
mumie	mumie	k1gFnSc2	mumie
<g/>
.	.	kIx.	.
</s>
<s>
Spekuluje	spekulovat	k5eAaImIp3nS	spekulovat
se	se	k3xPyFc4	se
i	i	k9	i
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
jednat	jednat	k5eAaImF	jednat
i	i	k9	i
o	o	k7c4	o
pojistný	pojistný	k2eAgInSc4d1	pojistný
podvod	podvod	k1gInSc4	podvod
-	-	kIx~	-
potopen	potopen	k2eAgMnSc1d1	potopen
neměl	mít	k5eNaImAgMnS	mít
být	být	k5eAaImF	být
Titanic	Titanic	k1gInSc4	Titanic
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Olympic	Olympic	k1gMnSc1	Olympic
-	-	kIx~	-
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
obě	dva	k4xCgFnPc1	dva
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
doku	dok	k1gInSc6	dok
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
totiž	totiž	k9	totiž
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
záměně	záměna	k1gFnSc3	záměna
co	co	k9	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
jejich	jejich	k3xOp3gFnPc2	jejich
insignií	insignie	k1gFnPc2	insignie
<g/>
,	,	kIx,	,
nápisů	nápis	k1gInPc2	nápis
<g/>
,	,	kIx,	,
značení	značení	k1gNnSc1	značení
na	na	k7c6	na
jejich	jejich	k3xOp3gFnPc6	jejich
palubách	paluba	k1gFnPc6	paluba
atd.	atd.	kA	atd.
První	první	k4xOgFnSc1	první
filmové	filmový	k2eAgFnPc1d1	filmová
zpracování	zpracování	k1gNnSc4	zpracování
katastrofy	katastrofa	k1gFnSc2	katastrofa
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
němého	němý	k2eAgInSc2d1	němý
filmu	film	k1gInSc2	film
Saved	Saved	k1gMnSc1	Saved
from	from	k1gMnSc1	from
the	the	k?	the
Titanic	Titanic	k1gInSc1	Titanic
(	(	kIx(	(
<g/>
Zachráněná	zachráněná	k1gFnSc1	zachráněná
z	z	k7c2	z
Titanicu	Titanicus	k1gInSc2	Titanicus
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
již	již	k9	již
několik	několik	k4yIc1	několik
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
katastrofě	katastrofa	k1gFnSc6	katastrofa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
rolí	role	k1gFnPc2	role
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
herečka	herečka	k1gFnSc1	herečka
Dorothy	Dorotha	k1gFnSc2	Dorotha
Gibsonová	Gibsonová	k1gFnSc1	Gibsonová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
katastrofu	katastrofa	k1gFnSc4	katastrofa
přežila	přežít	k5eAaPmAgFnS	přežít
ve	v	k7c6	v
člunu	člun	k1gInSc6	člun
7	[number]	k4	7
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
i	i	k9	i
autorkou	autorka	k1gFnSc7	autorka
scénáře	scénář	k1gInSc2	scénář
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
14	[number]	k4	14
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgInP	následovat
další	další	k2eAgInPc1d1	další
filmy	film	k1gInPc1	film
<g/>
:	:	kIx,	:
Atlantik	Atlantik	k1gInSc1	Atlantik
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
Titanic	Titanic	k1gInSc1	Titanic
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
Titanic	Titanic	k1gInSc1	Titanic
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
Oscar	Oscar	k1gInSc1	Oscar
za	za	k7c4	za
scénář	scénář	k1gInSc4	scénář
<g/>
)	)	kIx)	)
Zkáza	zkáza	k1gFnSc1	zkáza
Titanicu	Titanicus	k1gInSc2	Titanicus
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
S.	S.	kA	S.
<g/>
O.S.	O.S.	k1gFnSc1	O.S.
Titanic	Titanic	k1gInSc1	Titanic
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
Vyzvednutí	vyzvednutí	k1gNnSc3	vyzvednutí
Titanicu	Titanicum	k1gNnSc3	Titanicum
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
Titanica	Titanic	k1gInSc2	Titanic
(	(	kIx(	(
<g/>
94	[number]	k4	94
<g/>
minutový	minutový	k2eAgInSc1d1	minutový
dokument	dokument	k1gInSc1	dokument
se	s	k7c7	s
záběry	záběr	k1gInPc7	záběr
z	z	k7c2	z
vraku	vrak	k1gInSc2	vrak
<g/>
)	)	kIx)	)
Titanic	Titanic	k1gInSc1	Titanic
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Titanic	Titanic	k1gInSc1	Titanic
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
velkofilm	velkofilm	k1gInSc4	velkofilm
režiséra	režisér	k1gMnSc2	režisér
Jamese	Jamese	k1gFnSc2	Jamese
Camerona	Cameron	k1gMnSc2	Cameron
<g/>
,	,	kIx,	,
11	[number]	k4	11
Oscarů	Oscar	k1gInPc2	Oscar
<g/>
)	)	kIx)	)
Titanic	Titanic	k1gInSc1	Titanic
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
čtyřdílná	čtyřdílný	k2eAgFnSc1d1	čtyřdílná
britská	britský	k2eAgFnSc1d1	britská
koprodukční	koprodukční	k2eAgFnSc1d1	koprodukční
televizní	televizní	k2eAgFnSc1d1	televizní
minisérie	minisérie	k1gFnSc1	minisérie
<g/>
)	)	kIx)	)
</s>
