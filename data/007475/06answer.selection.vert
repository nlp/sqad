<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1777	[number]	k4	1777
je	být	k5eAaImIp3nS	být
Brno	Brno	k1gNnSc4	Brno
také	také	k9	také
sídlem	sídlo	k1gNnSc7	sídlo
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
brněnské	brněnský	k2eAgFnSc2d1	brněnská
diecéze	diecéze	k1gFnSc2	diecéze
<g/>
,	,	kIx,	,
biskupským	biskupský	k2eAgInSc7d1	biskupský
chrámem	chrám	k1gInSc7	chrám
je	být	k5eAaImIp3nS	být
katedrála	katedrála	k1gFnSc1	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc2	Pavel
na	na	k7c6	na
Petrově	Petrov	k1gInSc6	Petrov
<g/>
.	.	kIx.	.
</s>
