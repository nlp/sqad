<p>
<s>
Roudný	Roudný	k2eAgMnSc1d1	Roudný
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
vesnice	vesnice	k1gFnSc1	vesnice
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
14	[number]	k4	14
částí	část	k1gFnPc2	část
obce	obec	k1gFnSc2	obec
Zvěstov	Zvěstov	k1gInSc1	Zvěstov
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Benešov	Benešov	k1gInSc1	Benešov
ve	v	k7c6	v
Středočeském	středočeský	k2eAgInSc6d1	středočeský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Bývalá	bývalý	k2eAgFnSc1d1	bývalá
dělnická	dělnický	k2eAgFnSc1d1	Dělnická
kolonie	kolonie	k1gFnSc1	kolonie
spjatá	spjatý	k2eAgFnSc1d1	spjatá
s	s	k7c7	s
někdejším	někdejší	k2eAgInSc7d1	někdejší
zlatodolem	zlatodůl	k1gInSc7	zlatodůl
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
cca	cca	kA	cca
2,5	[number]	k4	2,5
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Zvěstova	zvěstův	k2eAgNnSc2d1	zvěstův
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
evidováno	evidovat	k5eAaImNgNnS	evidovat
10	[number]	k4	10
adres	adresa	k1gFnPc2	adresa
<g/>
.	.	kIx.	.
</s>
<s>
Roudný	Roudný	k2eAgMnSc1d1	Roudný
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Bořkovice	Bořkovice	k1gFnSc2	Bořkovice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ZEMEK	zemek	k1gMnSc1	zemek
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Zlatodůl	zlatodůl	k1gInSc1	zlatodůl
Roudný	Roudný	k2eAgInSc1d1	Roudný
u	u	k7c2	u
Vlašimi	Vlašim	k1gFnSc2	Vlašim
<g/>
.	.	kIx.	.
</s>
<s>
Vlastivědný	vlastivědný	k2eAgInSc1d1	vlastivědný
časopis	časopis	k1gInSc1	časopis
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
(	(	kIx(	(
<g/>
XXVII	XXVII	kA	XXVII
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
samostatná	samostatný	k2eAgFnSc1d1	samostatná
příloha	příloha	k1gFnSc1	příloha
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Roudný	Roudný	k2eAgMnSc1d1	Roudný
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Roudný	Roudný	k2eAgMnSc1d1	Roudný
v	v	k7c6	v
Muzeu	muzeum	k1gNnSc6	muzeum
České	český	k2eAgFnSc2d1	Česká
Sibiře	Sibiř	k1gFnSc2	Sibiř
</s>
</p>
