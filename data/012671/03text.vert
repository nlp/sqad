<p>
<s>
Studený	studený	k2eAgInSc1d1	studený
vrch	vrch	k1gInSc1	vrch
je	být	k5eAaImIp3nS	být
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
rozkládající	rozkládající	k2eAgFnSc2d1	rozkládající
se	se	k3xPyFc4	se
na	na	k7c6	na
vrcholovém	vrcholový	k2eAgInSc6d1	vrcholový
kuželu	kužel	k1gInSc6	kužel
hory	hora	k1gFnSc2	hora
Studenec	Studenec	k1gInSc1	Studenec
<g/>
.	.	kIx.	.
</s>
<s>
Rezervace	rezervace	k1gFnSc1	rezervace
představuje	představovat	k5eAaImIp3nS	představovat
výjimečně	výjimečně	k6eAd1	výjimečně
cenný	cenný	k2eAgInSc4d1	cenný
komplex	komplex	k1gInSc4	komplex
lesních	lesní	k2eAgInPc2d1	lesní
porostů	porost	k1gInPc2	porost
a	a	k8xC	a
společenstev	společenstvo	k1gNnPc2	společenstvo
se	s	k7c7	s
složením	složení	k1gNnSc7	složení
blízkým	blízký	k2eAgMnPc3d1	blízký
přirozené	přirozený	k2eAgFnSc3d1	přirozená
skladbě	skladba	k1gFnSc3	skladba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Rezervace	rezervace	k1gFnSc1	rezervace
byla	být	k5eAaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
původních	původní	k2eAgInPc2d1	původní
listnatých	listnatý	k2eAgInPc2d1	listnatý
a	a	k8xC	a
smíšených	smíšený	k2eAgInPc2d1	smíšený
lesních	lesní	k2eAgInPc2d1	lesní
porostů	porost	k1gInPc2	porost
<g/>
,	,	kIx,	,
charakteristických	charakteristický	k2eAgFnPc2d1	charakteristická
pro	pro	k7c4	pro
Lužické	lužický	k2eAgFnPc4d1	Lužická
hory	hora	k1gFnPc4	hora
<g/>
.	.	kIx.	.
</s>
<s>
Druhová	druhový	k2eAgFnSc1d1	druhová
skladba	skladba	k1gFnSc1	skladba
lesů	les	k1gInPc2	les
nebyla	být	k5eNaImAgFnS	být
člověkem	člověk	k1gMnSc7	člověk
výrazněji	výrazně	k6eAd2	výrazně
ovlivněna	ovlivněn	k2eAgFnSc1d1	ovlivněna
<g/>
.	.	kIx.	.
</s>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
se	s	k7c7	s
suťovými	suťový	k2eAgNnPc7d1	suťové
poli	pole	k1gNnPc7	pole
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
řídké	řídký	k2eAgInPc4d1	řídký
vzrostlé	vzrostlý	k2eAgInPc4d1	vzrostlý
buky	buk	k1gInPc4	buk
<g/>
,	,	kIx,	,
v	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
polohách	poloha	k1gFnPc6	poloha
s	s	k7c7	s
přimíseným	přimísený	k2eAgInSc7d1	přimísený
javorem	javor	k1gInSc7	javor
klenem	klen	k1gInSc7	klen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vlhčích	vlhký	k2eAgNnPc6d2	vlhčí
místech	místo	k1gNnPc6	místo
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
také	také	k9	také
jasan	jasan	k1gInSc1	jasan
<g/>
,	,	kIx,	,
vzácněji	vzácně	k6eAd2	vzácně
pak	pak	k6eAd1	pak
jilm	jilm	k1gInSc1	jilm
a	a	k8xC	a
javor	javor	k1gInSc1	javor
mléč	mléč	k1gInSc1	mléč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
bylinném	bylinný	k2eAgNnSc6d1	bylinné
patře	patro	k1gNnSc6	patro
se	se	k3xPyFc4	se
na	na	k7c6	na
severních	severní	k2eAgInPc6d1	severní
svazích	svah	k1gInPc6	svah
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
kapraďorosty	kapraďorost	k1gInPc1	kapraďorost
<g/>
,	,	kIx,	,
na	na	k7c6	na
jižních	jižní	k2eAgFnPc6d1	jižní
roste	růst	k5eAaImIp3nS	růst
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
bažanka	bažanka	k1gFnSc1	bažanka
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
<g/>
,	,	kIx,	,
kyčelnice	kyčelnice	k1gFnSc1	kyčelnice
devítilistá	devítilistý	k2eAgFnSc1d1	devítilistá
<g/>
,	,	kIx,	,
kyčelnice	kyčelnice	k1gFnSc1	kyčelnice
cibulkonosná	cibulkonosný	k2eAgFnSc1d1	cibulkonosná
<g/>
,	,	kIx,	,
měsíčnice	měsíčnice	k1gFnSc1	měsíčnice
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
a	a	k8xC	a
náprstníky	náprstník	k1gInPc7	náprstník
<g/>
.	.	kIx.	.
</s>
<s>
Hojně	hojně	k6eAd1	hojně
zde	zde	k6eAd1	zde
roste	růst	k5eAaImIp3nS	růst
netýkavka	netýkavka	k1gFnSc1	netýkavka
a	a	k8xC	a
mařinka	mařinka	k1gFnSc1	mařinka
vonná	vonný	k2eAgFnSc1d1	vonná
<g/>
,	,	kIx,	,
vzácněji	vzácně	k6eAd2	vzácně
pak	pak	k6eAd1	pak
samorostlík	samorostlík	k1gInSc1	samorostlík
klasnatý	klasnatý	k2eAgInSc1d1	klasnatý
a	a	k8xC	a
lilie	lilie	k1gFnSc1	lilie
zlatohlávek	zlatohlávka	k1gFnPc2	zlatohlávka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jarních	jarní	k2eAgInPc6d1	jarní
měsících	měsíc	k1gInPc6	měsíc
zde	zde	k6eAd1	zde
rozkvétají	rozkvétat	k5eAaImIp3nP	rozkvétat
lýkovce	lýkovec	k1gInPc1	lýkovec
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
zde	zde	k6eAd1	zde
učiněny	učiněn	k2eAgInPc1d1	učiněn
zajímavé	zajímavý	k2eAgInPc1d1	zajímavý
nálezy	nález	k1gInPc1	nález
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
měkkýšů	měkkýš	k1gMnPc2	měkkýš
<g/>
,	,	kIx,	,
hmyzu	hmyz	k1gInSc2	hmyz
a	a	k8xC	a
pavouků	pavouk	k1gMnPc2	pavouk
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
jinak	jinak	k6eAd1	jinak
obvykle	obvykle	k6eAd1	obvykle
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
až	až	k9	až
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
nadmořských	nadmořský	k2eAgFnPc6d1	nadmořská
výškách	výška	k1gFnPc6	výška
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
suťové	suťový	k2eAgFnSc6d1	suťová
části	část	k1gFnSc6	část
bez	bez	k7c2	bez
stromového	stromový	k2eAgNnSc2d1	stromové
patra	patro	k1gNnSc2	patro
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
řada	řada	k1gFnSc1	řada
vzácných	vzácný	k2eAgInPc2d1	vzácný
mechorostů	mechorost	k1gInPc2	mechorost
a	a	k8xC	a
druhů	druh	k1gInPc2	druh
bezobratlých	bezobratlý	k2eAgInPc2d1	bezobratlý
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
tzv.	tzv.	kA	tzv.
glaciální	glaciální	k2eAgInPc1d1	glaciální
relikty	relikt	k1gInPc1	relikt
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
kterým	který	k3yQgNnSc7	který
specifické	specifický	k2eAgNnSc1d1	specifické
prostředí	prostředí	k1gNnSc1	prostředí
sutí	suť	k1gFnPc2	suť
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
přežít	přežít	k5eAaPmF	přežít
od	od	k7c2	od
poslední	poslední	k2eAgFnSc2d1	poslední
doby	doba	k1gFnSc2	doba
ledové	ledový	k2eAgFnPc1d1	ledová
a	a	k8xC	a
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
výskyt	výskyt	k1gInSc1	výskyt
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
omezuje	omezovat	k5eAaImIp3nS	omezovat
na	na	k7c6	na
území	území	k1gNnSc6	území
severní	severní	k2eAgFnSc2d1	severní
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Alp	Alpy	k1gFnPc2	Alpy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
rezervaci	rezervace	k1gFnSc6	rezervace
a	a	k8xC	a
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
žijí	žít	k5eAaImIp3nP	žít
kamzíci	kamzík	k1gMnPc1	kamzík
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zde	zde	k6eAd1	zde
byli	být	k5eAaImAgMnP	být
uměle	uměle	k6eAd1	uměle
vysazeni	vysadit	k5eAaPmNgMnP	vysadit
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
sem	sem	k6eAd1	sem
přivezeni	přivézt	k5eAaPmNgMnP	přivézt
z	z	k7c2	z
Alp	Alpy	k1gFnPc2	Alpy
a	a	k8xC	a
zdejším	zdejší	k2eAgFnPc3d1	zdejší
podmínkám	podmínka	k1gFnPc3	podmínka
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
přizpůsobili	přizpůsobit	k5eAaPmAgMnP	přizpůsobit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Naučná	naučný	k2eAgFnSc1d1	naučná
stezka	stezka	k1gFnSc1	stezka
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
tudy	tudy	k6eAd1	tudy
vedena	vést	k5eAaImNgFnS	vést
Naučná	naučný	k2eAgFnSc1d1	naučná
stezka	stezka	k1gFnSc1	stezka
Okolím	okolí	k1gNnSc7	okolí
Studence	studenka	k1gFnSc3	studenka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
podává	podávat	k5eAaImIp3nS	podávat
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
zdejším	zdejší	k2eAgInSc6d1	zdejší
panelu	panel	k1gInSc6	panel
zevrubné	zevrubný	k2eAgFnSc2d1	zevrubná
informace	informace	k1gFnSc2	informace
o	o	k7c4	o
rezervaci	rezervace	k1gFnSc4	rezervace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
chráněných	chráněný	k2eAgNnPc2d1	chráněné
území	území	k1gNnPc2	území
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Děčín	Děčín	k1gInSc1	Děčín
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
památných	památný	k2eAgInPc2d1	památný
stromů	strom	k1gInPc2	strom
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Děčín	Děčín	k1gInSc1	Děčín
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Studený	studený	k2eAgInSc4d1	studený
vrch	vrch	k1gInSc4	vrch
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Bližší	blízký	k2eAgInSc1d2	bližší
popis	popis	k1gInSc1	popis
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
Studený	studený	k2eAgInSc1d1	studený
vrch	vrch	k1gInSc1	vrch
</s>
</p>
<p>
<s>
Fotografie	fotografia	k1gFnPc1	fotografia
z	z	k7c2	z
NPR	NPR	kA	NPR
Studený	studený	k2eAgInSc1d1	studený
vrch	vrch	k1gInSc1	vrch
</s>
</p>
