<s>
Válka	válka	k1gFnSc1	válka
skončila	skončit	k5eAaPmAgFnS	skončit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Angličané	Angličan	k1gMnPc1	Angličan
ztratili	ztratit	k5eAaPmAgMnP	ztratit
všechna	všechen	k3xTgNnPc4	všechen
francouzská	francouzský	k2eAgNnPc4d1	francouzské
území	území	k1gNnPc4	území
(	(	kIx(	(
<g/>
která	který	k3yQgNnPc4	který
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
vlastnili	vlastnit	k5eAaImAgMnP	vlastnit
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
Normandské	normandský	k2eAgNnSc4d1	normandské
vévodství	vévodství	k1gNnSc4	vévodství
<g/>
)	)	kIx)	)
kromě	kromě	k7c2	kromě
Calais	Calais	k1gNnSc2	Calais
a	a	k8xC	a
Normanských	normanský	k2eAgInPc2d1	normanský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
