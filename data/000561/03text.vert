<s>
Silvestr	Silvestr	k1gMnSc1	Silvestr
(	(	kIx(	(
<g/>
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
jazycích	jazyk	k1gInPc6	jazyk
a	a	k8xC	a
dříve	dříve	k6eAd2	dříve
také	také	k9	také
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
Sylvestr	Sylvestr	k1gInSc1	Sylvestr
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mužské	mužský	k2eAgNnSc4d1	mužské
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
latinského	latinský	k2eAgInSc2d1	latinský
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
silva	silv	k1gMnSc2	silv
-	-	kIx~	-
les	les	k1gInSc1	les
<g/>
,	,	kIx,	,
silvestris	silvestris	k1gFnSc1	silvestris
-	-	kIx~	-
lesní	lesní	k2eAgFnSc1d1	lesní
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
"	"	kIx"	"
<g/>
muž	muž	k1gMnSc1	muž
z	z	k7c2	z
lesa	les	k1gInSc2	les
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
českého	český	k2eAgInSc2d1	český
a	a	k8xC	a
slovenského	slovenský	k2eAgInSc2d1	slovenský
kalendáře	kalendář	k1gInSc2	kalendář
má	mít	k5eAaImIp3nS	mít
svátek	svátek	k1gInSc1	svátek
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
(	(	kIx(	(
<g/>
protože	protože	k8xS	protože
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
335	[number]	k4	335
zemřel	zemřít	k5eAaPmAgMnS	zemřít
papež	papež	k1gMnSc1	papež
Silvestr	Silvestr	k1gMnSc1	Silvestr
I.	I.	kA	I.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
připadají	připadat	k5eAaPmIp3nP	připadat
silvestrovské	silvestrovský	k2eAgFnPc1d1	silvestrovská
oslavy	oslava	k1gFnPc1	oslava
<g/>
.	.	kIx.	.
</s>
<s>
Ženským	ženský	k2eAgInSc7d1	ženský
protějškem	protějšek	k1gInSc7	protějšek
je	být	k5eAaImIp3nS	být
Silvie	Silvie	k1gFnSc1	Silvie
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
četnost	četnost	k1gFnSc4	četnost
základní	základní	k2eAgFnSc2d1	základní
varianty	varianta	k1gFnSc2	varianta
jména	jméno	k1gNnSc2	jméno
Silvestr	Silvestr	k1gMnSc1	Silvestr
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
pořadí	pořadí	k1gNnSc2	pořadí
mezi	mezi	k7c7	mezi
mužskými	mužský	k2eAgNnPc7d1	mužské
jmény	jméno	k1gNnPc7	jméno
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
roků	rok	k1gInPc2	rok
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yQgInPc4	který
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
údaje	údaj	k1gInPc1	údaj
MV	MV	kA	MV
ČR	ČR	kA	ČR
-	-	kIx~	-
lze	lze	k6eAd1	lze
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
tedy	tedy	k9	tedy
vysledovat	vysledovat	k5eAaPmF	vysledovat
trend	trend	k1gInSc4	trend
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
:	:	kIx,	:
Změna	změna	k1gFnSc1	změna
procentního	procentní	k2eAgNnSc2d1	procentní
zastoupení	zastoupení	k1gNnSc2	zastoupení
jména	jméno	k1gNnSc2	jméno
mezi	mezi	k7c7	mezi
žijícími	žijící	k2eAgMnPc7d1	žijící
muži	muž	k1gMnPc7	muž
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
procentní	procentní	k2eAgFnSc1d1	procentní
změna	změna	k1gFnSc1	změna
se	se	k3xPyFc4	se
započítáním	započítání	k1gNnSc7	započítání
celkového	celkový	k2eAgInSc2d1	celkový
úbytku	úbytek	k1gInSc2	úbytek
mužů	muž	k1gMnPc2	muž
v	v	k7c6	v
ČR	ČR	kA	ČR
za	za	k7c4	za
sledované	sledovaný	k2eAgInPc4d1	sledovaný
roky	rok	k1gInPc4	rok
1999	[number]	k4	1999
<g/>
-	-	kIx~	-
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
-	-	kIx~	-
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
značném	značný	k2eAgInSc6d1	značný
poklesu	pokles	k1gInSc6	pokles
obliby	obliba	k1gFnSc2	obliba
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
Do	do	k7c2	do
statistiky	statistika	k1gFnSc2	statistika
nebyly	být	k5eNaImAgInP	být
započítány	započítat	k5eAaPmNgInP	započítat
další	další	k2eAgInPc1d1	další
<g/>
,	,	kIx,	,
řídce	řídce	k6eAd1	řídce
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgFnPc1d1	vyskytující
varianty	varianta	k1gFnPc1	varianta
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
×	×	k?	×
Sylvester	Sylvester	k1gMnSc1	Sylvester
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
×	×	k?	×
Silvestru	Silvestr	k1gMnSc6	Silvestr
<g/>
)	)	kIx)	)
a	a	k8xC	a
nemohou	moct	k5eNaImIp3nP	moct
být	být	k5eAaImF	být
započítány	započítat	k5eAaPmNgFnP	započítat
ani	ani	k8xC	ani
dvojjmenné	dvojjmenný	k2eAgFnPc1d1	dvojjmenný
entity	entita	k1gFnPc1	entita
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
lze	lze	k6eAd1	lze
používat	používat	k5eAaImF	používat
od	od	k7c2	od
r.	r.	kA	r.
2000	[number]	k4	2000
a	a	k8xC	a
které	který	k3yIgFnPc4	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
uváděny	uvádět	k5eAaImNgFnP	uvádět
samostatně	samostatně	k6eAd1	samostatně
(	(	kIx(	(
<g/>
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
nejčastější	častý	k2eAgInPc4d3	nejčastější
4	[number]	k4	4
<g/>
×	×	k?	×
Tomáš	Tomáš	k1gMnSc1	Tomáš
Silvestr	Silvestr	k1gMnSc1	Silvestr
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
×	×	k?	×
Milič	Milič	k1gMnSc1	Milič
Silvestr	Silvestr	k1gMnSc1	Silvestr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Silvestr	Silvestr	k1gMnSc1	Silvestr
Hetzer	Hetzer	k1gMnSc1	Hetzer
-	-	kIx~	-
opat	opat	k1gMnSc1	opat
cisterciáckého	cisterciácký	k2eAgInSc2d1	cisterciácký
kláštera	klášter	k1gInSc2	klášter
v	v	k7c6	v
Plasích	Plasy	k1gInPc6	Plasy
Silvestr	Silvestr	k1gMnSc1	Silvestr
I.	I.	kA	I.
-	-	kIx~	-
papež	papež	k1gMnSc1	papež
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
Silvestr	Silvestr	k1gMnSc1	Silvestr
II	II	kA	II
<g/>
.	.	kIx.	.
-	-	kIx~	-
papež	papež	k1gMnSc1	papež
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
Silvestr	Silvestr	k1gMnSc1	Silvestr
III	III	kA	III
<g/>
.	.	kIx.	.
-	-	kIx~	-
papež	papež	k1gMnSc1	papež
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
Silvestr	Silvestr	k1gInSc4	Silvestr
Braito	Brait	k2eAgNnSc1d1	Braito
-	-	kIx~	-
dominikánský	dominikánský	k2eAgMnSc1d1	dominikánský
teolog	teolog	k1gMnSc1	teolog
Silvester	Silvester	k1gMnSc1	Silvester
Ács	Ács	k1gMnSc1	Ács
-	-	kIx~	-
slovenský	slovenský	k2eAgMnSc1d1	slovenský
a	a	k8xC	a
československý	československý	k2eAgMnSc1d1	československý
politik	politik	k1gMnSc1	politik
KSS	KSS	kA	KSS
z	z	k7c2	z
regionu	region	k1gInSc2	region
Šaľa	Šaľum	k1gNnSc2	Šaľum
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
FS	FS	kA	FS
<g />
.	.	kIx.	.
</s>
<s>
za	za	k7c2	za
normalizace	normalizace	k1gFnSc2	normalizace
Silvestr	Silvestr	k1gMnSc1	Silvestr
Bláha	Bláha	k1gMnSc1	Bláha
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
voják	voják	k1gMnSc1	voják
a	a	k8xC	a
odbojář	odbojář	k1gMnSc1	odbojář
Sylvester	Sylvester	k1gMnSc1	Sylvester
Stallone	Stallon	k1gInSc5	Stallon
-	-	kIx~	-
herec	herec	k1gMnSc1	herec
Sylvestr	Sylvestr	k1gInSc4	Sylvestr
Krnka	Krnek	k1gInSc2	Krnek
-	-	kIx~	-
puškař	puškař	k1gMnSc1	puškař
a	a	k8xC	a
vynálezce	vynálezce	k1gMnSc1	vynálezce
James	James	k1gMnSc1	James
Joseph	Joseph	k1gMnSc1	Joseph
Sylvester	Sylvester	k1gMnSc1	Sylvester
-	-	kIx~	-
anglický	anglický	k2eAgMnSc1d1	anglický
matematik	matematik	k1gMnSc1	matematik
Jakub	Jakub	k1gMnSc1	Jakub
Sylvestr	Sylvestr	k1gInSc1	Sylvestr
-	-	kIx~	-
slovenský	slovenský	k2eAgMnSc1d1	slovenský
fotbalista	fotbalista	k1gMnSc1	fotbalista
Sylvester	Sylvester	k1gMnSc1	Sylvester
(	(	kIx(	(
<g/>
kráter	kráter	k1gInSc1	kráter
<g/>
)	)	kIx)	)
-	-	kIx~	-
kráter	kráter	k1gInSc1	kráter
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
Měsíce	měsíc	k1gInSc2	měsíc
Catena	Caten	k2eAgFnSc1d1	Catena
Sylvester	Sylvester	k1gInSc1	Sylvester
-	-	kIx~	-
měsíční	měsíční	k2eAgNnSc1d1	měsíční
údolí	údolí	k1gNnSc1	údolí
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Silvestr	Silvestr	k1gMnSc1	Silvestr
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
