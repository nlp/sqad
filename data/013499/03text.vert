<s>
Lineage	Lineage	k1gFnSc1
II	II	kA
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Lineage	Lineage	k6eAd1
IIVývojářNCsoftVydavatelNCsoftEngineUnreal	IIVývojářNCsoftVydavatelNCsoftEngineUnreal	k1gInSc1
Engine	Engin	k1gInSc5
2.0	2.0	k4
<g/>
PlatformaWindowsDatum	PlatformaWindowsDatum	k1gNnSc4
vydání	vydání	k1gNnSc2
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2003	#num#	k4
27	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2004	#num#	k4
<g/>
ŽánrMMORPGHerní	ŽánrMMORPGHerní	k2eAgMnSc1d1
módmultiplayerKlasifikacePEGI	módmultiplayerKlasifikacePEGI	k?
ESRB	ESRB	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Lineage	Lineage	k1gFnSc1
II	II	kA
<g/>
:	:	kIx,
The	The	kA
Chaotic	Chaotice	k2eAgFnSc1d1
Chronicle	Chronicle	k1gFnSc1
je	být	k5eAaImIp3nS
počítačová	počítačový	k2eAgFnSc1d1
fantasy	fantas	k1gInPc1
hromadná	hromadný	k2eAgFnSc1d1
online	online	k2eAgFnSc1d1
hra	hra	k1gFnSc1
na	na	k7c4
hrdiny	hrdina	k1gMnPc4
(	(	kIx(
<g/>
MMORPG	MMORPG	kA
<g/>
)	)	kIx)
vytvořená	vytvořený	k2eAgFnSc1d1
korejskou	korejský	k2eAgFnSc7d1
obchodní	obchodní	k2eAgFnSc7d1
společností	společnost	k1gFnSc7
NCsoft	NCsofta	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
pokračování	pokračování	k1gNnSc4
počítačové	počítačový	k2eAgFnSc2d1
hry	hra	k1gFnSc2
Lineage	Lineag	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnSc1d1
charakteristika	charakteristika	k1gFnSc1
</s>
<s>
Hráč	hráč	k1gMnSc1
si	se	k3xPyFc3
vytvoří	vytvořit	k5eAaPmIp3nS
vlastní	vlastní	k2eAgFnSc4d1
postavu	postava	k1gFnSc4
–	–	k?
charakter	charakter	k1gInSc1
<g/>
,	,	kIx,
s	s	k7c7
níž	jenž	k3xRgFnSc7
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
v	v	k7c6
prostředí	prostředí	k1gNnSc6
virtuálního	virtuální	k2eAgInSc2d1
světa	svět	k1gInSc2
Aden	Aden	k1gInSc1
a	a	k8xC
Elmore	Elmor	k1gInSc5
ve	v	k7c6
středověkém	středověký	k2eAgInSc6d1
až	až	k8xS
fantasy	fantas	k1gInPc4
stylu	styl	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Může	moct	k5eAaImIp3nS
si	se	k3xPyFc3
vybrat	vybrat	k5eAaPmF
z	z	k7c2
šesti	šest	k4xCc2
ras	rasa	k1gFnPc2
<g/>
:	:	kIx,
člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
elf	elf	k1gMnSc1
<g/>
,	,	kIx,
temný	temný	k2eAgMnSc1d1
elf	elf	k1gMnSc1
<g/>
,	,	kIx,
ork	ork	k?
<g/>
,	,	kIx,
trpaslík	trpaslík	k1gMnSc1
a	a	k8xC
Kamael	Kamael	k1gMnSc1
(	(	kIx(
<g/>
kombinace	kombinace	k1gFnSc1
lidí	člověk	k1gMnPc2
a	a	k8xC
andělů	anděl	k1gMnPc2
vybavená	vybavený	k2eAgNnPc4d1
jedním	jeden	k4xCgNnSc7
křídlem	křídlo	k1gNnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postavě	postava	k1gFnSc3
také	také	k9
vybere	vybrat	k5eAaPmIp3nS
povolání	povolání	k1gNnSc4
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
od	od	k7c2
bojovníka	bojovník	k1gMnSc2
po	po	k7c4
čaroděje	čaroděj	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výjimkou	výjimka	k1gFnSc7
je	být	k5eAaImIp3nS
trpaslík	trpaslík	k1gMnSc1
a	a	k8xC
Kamael	Kamael	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
můžu	můžu	k?
mít	mít	k5eAaImF
pouze	pouze	k6eAd1
povolání	povolání	k1gNnSc4
typu	typ	k1gInSc2
bojovníka	bojovník	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
trpaslík	trpaslík	k1gMnSc1
má	mít	k5eAaImIp3nS
jiné	jiný	k2eAgFnPc4d1
výhody	výhoda	k1gFnPc4
oproti	oproti	k7c3
ostatním	ostatní	k2eAgMnPc3d1
(	(	kIx(
<g/>
získávání	získávání	k1gNnSc4
–	–	k?
spoil	spoil	k1gInSc1
věcí	věc	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
craft	craft	k1gInSc1
–	–	k?
jejich	jejich	k3xOp3gFnSc6
výroba	výroba	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráč	hráč	k1gMnSc1
může	moct	k5eAaImIp3nS
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
hrdinou	hrdina	k1gMnSc7
prozkoumávat	prozkoumávat	k5eAaImF
svět	svět	k1gInSc4
Aden	Aden	k1gInSc4
<g/>
,	,	kIx,
bojovat	bojovat	k5eAaImF
s	s	k7c7
různými	různý	k2eAgNnPc7d1
monstry	monstrum	k1gNnPc7
<g/>
,	,	kIx,
dobývat	dobývat	k5eAaImF
pevnosti	pevnost	k1gFnPc4
a	a	k8xC
plnit	plnit	k5eAaImF
úkoly	úkol	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc4
činnosti	činnost	k1gFnPc4
může	moct	k5eAaImIp3nS
provádět	provádět	k5eAaImF
sám	sám	k3xTgMnSc1
nebo	nebo	k8xC
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
jinými	jiný	k2eAgMnPc7d1
hráči	hráč	k1gMnPc7
<g/>
,	,	kIx,
tato	tento	k3xDgFnSc1
spolupráce	spolupráce	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
někdy	někdy	k6eAd1
nutná	nutný	k2eAgFnSc1d1
k	k	k7c3
porážce	porážka	k1gFnSc3
obzvláště	obzvláště	k6eAd1
silných	silný	k2eAgMnPc2d1
nepřátel	nepřítel	k1gMnPc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
raid	raid	k1gInSc4
bossové	boss	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
tom	ten	k3xDgNnSc6
všem	všecek	k3xTgMnPc3
hrdina	hrdina	k1gMnSc1
získává	získávat	k5eAaImIp3nS
nové	nový	k2eAgFnPc4d1
schopnosti	schopnost	k1gFnPc4
<g/>
,	,	kIx,
zkušenostní	zkušenostní	k2eAgInPc4d1
body	bod	k1gInPc4
a	a	k8xC
různé	různý	k2eAgInPc4d1
předměty	předmět	k1gInPc4
<g/>
,	,	kIx,
rovněž	rovněž	k9
je	být	k5eAaImIp3nS
mu	on	k3xPp3gMnSc3
umožněn	umožnit	k5eAaPmNgInS
přístup	přístup	k1gInSc1
do	do	k7c2
dalších	další	k2eAgFnPc2d1
lokací	lokace	k1gFnPc2
hry	hra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
po	po	k7c6
dosáhnutí	dosáhnutý	k2eAgMnPc1d1
další	další	k2eAgFnPc4d1
úrovně	úroveň	k1gFnSc2
(	(	kIx(
<g/>
levelu	level	k1gInSc2
<g/>
)	)	kIx)
dostává	dostávat	k5eAaImIp3nS
hráč	hráč	k1gMnSc1
různé	různý	k2eAgFnSc2d1
nové	nový	k2eAgFnSc2d1
schopnosti	schopnost	k1gFnSc2
(	(	kIx(
<g/>
skills	skills	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
hráče	hráč	k1gMnPc4
je	být	k5eAaImIp3nS
důležité	důležitý	k2eAgNnSc1d1
si	se	k3xPyFc3
vybrat	vybrat	k5eAaPmF
povolání	povolání	k1gNnSc4
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
se	se	k3xPyFc4
vybírá	vybírat	k5eAaImIp3nS
pomocí	pomocí	k7c2
přestupových	přestupový	k2eAgInPc2d1
questů	quest	k1gInPc2
(	(	kIx(
<g/>
ukolů	ukol	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
dostane	dostat	k5eAaPmIp3nS
po	po	k7c6
dosáhnutí	dosáhnutý	k2eAgMnPc1d1
20	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
40	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
76	#num#	k4
<g/>
.	.	kIx.
úrovně	úroveň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mimo	mimo	k7c4
získávání	získávání	k1gNnSc4
zkušeností	zkušenost	k1gFnPc2
(	(	kIx(
<g/>
exp	exp	kA
<g/>
)	)	kIx)
a	a	k8xC
skill	skill	k1gInSc1
points	pointsa	k1gFnPc2
(	(	kIx(
<g/>
sp	sp	k?
<g/>
)	)	kIx)
zabíjením	zabíjení	k1gNnSc7
monster	monstrum	k1gNnPc2
je	být	k5eAaImIp3nS
podstatnou	podstatný	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
hry	hra	k1gFnSc2
PvP	PvP	k1gFnSc2
(	(	kIx(
<g/>
player	playrat	k5eAaPmRp2nS
vs	vs	k?
<g/>
.	.	kIx.
player	player	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
PK	PK	kA
(	(	kIx(
<g/>
player	player	k1gMnSc1
kill	kill	k1gMnSc1
<g/>
/	/	kIx~
<g/>
er	er	k?
<g/>
)	)	kIx)
systém	systém	k1gInSc1
a	a	k8xC
clan	clan	k1gInSc1
wars	warsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
tedy	tedy	k9
o	o	k7c6
soupeření	soupeření	k1gNnSc6
mezi	mezi	k7c7
hráči	hráč	k1gMnPc7
o	o	k7c4
hrady	hrad	k1gInPc4
<g/>
,	,	kIx,
pevnosti	pevnost	k1gFnPc4
(	(	kIx(
<g/>
v	v	k7c6
Interlude	Interlud	k1gInSc5
<g/>
)	)	kIx)
a	a	k8xC
také	také	k9
o	o	k7c4
preciznost	preciznost	k1gFnSc4
<g/>
,	,	kIx,
respekt	respekt	k1gInSc4
a	a	k8xC
další	další	k2eAgNnSc4d1
<g/>
…	…	k?
</s>
<s>
Enhancements	Enhancements	k1gInSc1
<g/>
/	/	kIx~
<g/>
vylepšování	vylepšování	k1gNnSc1
</s>
<s>
Hráč	hráč	k1gMnSc1
může	moct	k5eAaImIp3nS
díky	díky	k7c3
svitkům	svitek	k1gInPc3
<g/>
,	,	kIx,
elementálním	elementální	k2eAgInPc3d1
kamenům	kámen	k1gInPc3
<g/>
,	,	kIx,
soul	soul	k1gInSc4
crystalům	crystal	k1gMnPc3
<g/>
,	,	kIx,
life	life	k6eAd1
stonům	ston	k1gInPc3
či	či	k8xC
díky	díky	k7c3
famu	famus	k1gInSc3
<g/>
(	(	kIx(
<g/>
slávě	sláva	k1gFnSc3
<g/>
)	)	kIx)
vylepšovat	vylepšovat	k5eAaImF
své	svůj	k3xOyFgFnPc4
zbraně	zbraň	k1gFnPc4
nebo	nebo	k8xC
brnění	brnění	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
svitky	svitek	k1gInPc1
–	–	k?
enchant	enchant	k1gInSc1
scrolls	scrolls	k1gInSc1
<g/>
:	:	kIx,
svitky	svitek	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
zvyšují	zvyšovat	k5eAaImIp3nP
sílu	síla	k1gFnSc4
brnění	brnění	k1gNnSc3
<g/>
,	,	kIx,
nebo	nebo	k8xC
zbraně	zbraň	k1gFnPc1
(	(	kIx(
<g/>
p.	p.	k?
<g/>
def	def	k?
<g/>
/	/	kIx~
<g/>
p.	p.	k?
<g/>
atk	atk	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
brnění	brnění	k1gNnSc2
platí	platit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
pokud	pokud	k8xS
jsou	být	k5eAaImIp3nP
všechny	všechen	k3xTgFnPc1
setové	setový	k2eAgFnPc1d1
části	část	k1gFnPc1
očarovány	očarován	k2eAgFnPc1d1
(	(	kIx(
<g/>
naenchantujeme	naenchantovat	k5eAaBmIp1nP,k5eAaImIp1nP,k5eAaPmIp1nP
<g/>
)	)	kIx)
na	na	k7c4
+6	+6	k4
a	a	k8xC
více	hodně	k6eAd2
<g/>
,	,	kIx,
hráč	hráč	k1gMnSc1
dostane	dostat	k5eAaPmIp3nS
speciální	speciální	k2eAgInSc4d1
bonus	bonus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbraně	zbraň	k1gFnPc1
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
jsou	být	k5eAaImIp3nP
očarovány	očarovat	k5eAaPmNgFnP
na	na	k7c4
+4	+4	k4
začínají	začínat	k5eAaImIp3nP
svítit	svítit	k5eAaImF
<g/>
,	,	kIx,
čím	co	k3yRnSc7,k3yQnSc7,k3yInSc7
více	hodně	k6eAd2
zbraň	zbraň	k1gFnSc4
očarujeme	očarovat	k5eAaPmIp1nP
tím	ten	k3xDgNnSc7
více	hodně	k6eAd2
svítí	svítit	k5eAaImIp3nS
a	a	k8xC
vyšší	vysoký	k2eAgMnSc1d2
má	mít	k5eAaImIp3nS
sílu	síla	k1gFnSc4
útoku	útok	k1gInSc2
a	a	k8xC
u	u	k7c2
+16	+16	k4
začíná	začínat	k5eAaImIp3nS
svítit	svítit	k5eAaImF
červeně	červeně	k6eAd1
<g/>
.	.	kIx.
<g/>
(	(	kIx(
<g/>
kupříkladu	kupříkladu	k6eAd1
+16	+16	k4
C	C	kA
grade	grad	k1gInSc5
zbraň	zbraň	k1gFnSc4
je	být	k5eAaImIp3nS
decentně	decentně	k6eAd1
silnější	silný	k2eAgFnSc1d2
<g/>
,	,	kIx,
než	než	k8xS
S	s	k7c7
grade	grad	k1gInSc5
+3	+3	k4
<g/>
)	)	kIx)
bezpečně	bezpečně	k6eAd1
jde	jít	k5eAaImIp3nS
věci	věc	k1gFnPc4
očarovat	očarovat	k5eAaPmF
do	do	k7c2
+3	+3	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
předmět	předmět	k1gInSc1
očarováváme	očarovávat	k5eAaImIp1nP
na	na	k7c4
+4	+4	k4
a	a	k8xC
více	hodně	k6eAd2
může	moct	k5eAaImIp3nS
se	se	k3xPyFc4
rozpadnout	rozpadnout	k5eAaPmF
na	na	k7c4
krystaly	krystal	k1gInPc4
podle	podle	k7c2
třídy	třída	k1gFnSc2
předmětu	předmět	k1gInSc2
(	(	kIx(
<g/>
D	D	kA
grade	grad	k1gInSc5
brnění	brnění	k1gNnSc3
=	=	kIx~
D	D	kA
grade	grad	k1gInSc5
krystaly	krystal	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
výjimkou	výjimka	k1gFnSc7
jsou	být	k5eAaImIp3nP
brnění	brnění	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yQgNnPc1,k3yRgNnPc1
jsou	být	k5eAaImIp3nP
kompletem	komplet	k1gInSc7
horní	horní	k2eAgFnSc1d1
<g/>
/	/	kIx~
<g/>
spodní	spodní	k2eAgFnSc1d1
část	část	k1gFnSc1
<g/>
,	,	kIx,
tyto	tento	k3xDgFnPc4
lze	lze	k6eAd1
očarovat	očarovat	k5eAaPmF
bezpečně	bezpečně	k6eAd1
do	do	k7c2
+4	+4	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
také	také	k9
blessed	blessed	k1gInSc4
svitky	svitek	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yIgInPc4,k3yRgInPc4
zaručí	zaručit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
předmět	předmět	k1gInSc1
nerozpadne	rozpadnout	k5eNaPmIp3nS
na	na	k7c4
krystaly	krystal	k1gInPc4
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
se	se	k3xPyFc4
vrátí	vrátit	k5eAaPmIp3nS
na	na	k7c4
původních	původní	k2eAgInPc2d1
+0	+0	k4
<g/>
.	.	kIx.
</s>
<s>
Elementální	Elementální	k2eAgInPc1d1
kameny	kámen	k1gInPc1
–	–	k?
kameny	kámen	k1gInPc7
<g/>
,	,	kIx,
díky	díky	k7c3
kterým	který	k3yQgFnPc3,k3yRgFnPc3,k3yIgFnPc3
můžeme	moct	k5eAaImIp1nP
zvýšit	zvýšit	k5eAaPmF
svou	svůj	k3xOyFgFnSc4
obranu	obrana	k1gFnSc4
proti	proti	k7c3
různým	různý	k2eAgInPc3d1
elementům	element	k1gInPc3
<g/>
,	,	kIx,
nebo	nebo	k8xC
zvýšit	zvýšit	k5eAaPmF
jejich	jejich	k3xOp3gInSc4
útok	útok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kameny	kámen	k1gInPc7
se	se	k3xPyFc4
dají	dát	k5eAaPmIp3nP
získat	získat	k5eAaPmF
s	s	k7c7
elementem	element	k1gInSc7
<g/>
:	:	kIx,
vody	voda	k1gFnSc2
(	(	kIx(
<g/>
water	water	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ohně	oheň	k1gInSc2
(	(	kIx(
<g/>
fire	fire	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
země	země	k1gFnSc1
(	(	kIx(
<g/>
earth	earth	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
větru	vítr	k1gInSc3
(	(	kIx(
<g/>
wind	wind	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
temné	temný	k2eAgFnPc1d1
(	(	kIx(
<g/>
dark	dark	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
svaté	svatá	k1gFnSc2
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
holy	hola	k1gFnPc1
<g/>
)	)	kIx)
..	..	k?
při	při	k7c6
použití	použití	k1gNnSc6
do	do	k7c2
zbraně	zbraň	k1gFnSc2
zvýšíte	zvýšit	k5eAaPmIp2nP
útok	útok	k1gInSc4
v	v	k7c6
tom	ten	k3xDgNnSc6
jistému	jistý	k2eAgInSc3d1
elementu	element	k1gInSc3
<g/>
,	,	kIx,
v	v	k7c6
brnění	brnění	k1gNnSc6
pak	pak	k6eAd1
sílu	síla	k1gFnSc4
obrany	obrana	k1gFnSc2
opačného	opačný	k2eAgInSc2d1
elementu	element	k1gInSc2
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
použitém	použitý	k2eAgInSc6d1
kamenu	kámen	k1gInSc6
<g/>
(	(	kIx(
<g/>
např.	např.	kA
kámen	kámen	k1gInSc1
země	země	k1gFnSc1
zvýší	zvýšit	k5eAaPmIp3nS
odolnost	odolnost	k1gFnSc4
vůči	vůči	k7c3
větru	vítr	k1gInSc3
<g/>
,	,	kIx,
nebo	nebo	k8xC
přidá	přidat	k5eAaPmIp3nS
zemní	zemní	k2eAgInSc1d1
útok	útok	k1gInSc1
do	do	k7c2
zbraně	zbraň	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dají	dát	k5eAaPmIp3nP
se	se	k3xPyFc4
používat	používat	k5eAaImF
jen	jen	k9
u	u	k7c2
S	s	k7c7
<g/>
,	,	kIx,
<g/>
S	s	k7c7
<g/>
80	#num#	k4
a	a	k8xC
S84	S84	k1gMnSc1
grade	grad	k1gInSc5
</s>
<s>
Soul	Soul	k1gInSc1
crystaly	crystat	k5eAaImAgInP,k5eAaPmAgInP
–	–	k?
krystaly	krystal	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
můžete	moct	k5eAaImIp2nP
u	u	k7c2
kováře	kovář	k1gMnSc2
zakovat	zakovat	k5eAaPmF
do	do	k7c2
zbraně	zbraň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
jim	on	k3xPp3gMnPc3
přidáte	přidat	k5eAaPmIp2nP
speciální	speciální	k2eAgFnSc4d1
vlastnost	vlastnost	k1gFnSc4
(	(	kIx(
<g/>
např.	např.	kA
rychlost	rychlost	k1gFnSc1
útoku	útok	k1gInSc2
<g/>
,	,	kIx,
sílu	síla	k1gFnSc4
magického	magický	k2eAgInSc2d1
útoku	útok	k1gInSc2
<g/>
,	,	kIx,
úhybnost	úhybnost	k1gFnSc4
hráče	hráč	k1gMnSc2
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
se	se	k3xPyFc4
vám	vy	k3xPp2nPc3
zvýší	zvýšit	k5eAaPmIp3nS
síla	síla	k1gFnSc1
útoku	útok	k1gInSc2
v	v	k7c6
PvP	PvP	k1gFnSc1
(	(	kIx(
<g/>
hráč	hráč	k1gMnSc1
proti	proti	k7c3
hráči	hráč	k1gMnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Life	Life	k6eAd1
stony	ston	k1gInPc1
–	–	k?
kameny	kámen	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
se	se	k3xPyFc4
dají	dát	k5eAaPmIp3nP
přidat	přidat	k5eAaPmF
do	do	k7c2
zbraně	zbraň	k1gFnSc2
u	u	k7c2
kováře	kovář	k1gMnSc2
<g/>
.	.	kIx.
je	být	k5eAaImIp3nS
jich	on	k3xPp3gMnPc2
několik	několik	k4yIc1
druhů	druh	k1gMnPc2
<g/>
:	:	kIx,
Life	Life	k1gNnSc1
stone	ston	k1gInSc5
<g/>
,	,	kIx,
Mid-Grade	Mid-Grad	k1gInSc5
Life	Lifus	k1gInSc5
stone	ston	k1gInSc5
<g/>
,	,	kIx,
Hight	Hight	k1gInSc1
Grade-Life	Grade-Lif	k1gInSc5
stone	ston	k1gInSc5
<g/>
,	,	kIx,
Top-Grade	Top-Grad	k1gInSc5
Lifestone	Lifeston	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čím	co	k3yQnSc7,k3yInSc7,k3yRnSc7
vyšší	vysoký	k2eAgFnSc1d2
třída	třída	k1gFnSc1
<g/>
(	(	kIx(
<g/>
grade	grad	k1gInSc5
<g/>
)	)	kIx)
kamene	kámen	k1gInSc2
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
vyšší	vysoký	k2eAgFnSc1d2
šance	šance	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
do	do	k7c2
zbraně	zbraň	k1gFnSc2
dostanete	dostat	k5eAaPmIp2nP
Aktivní	aktivní	k2eAgInSc4d1
skill	skill	k1gInSc4
(	(	kIx(
<g/>
například	například	k6eAd1
léčitel	léčitel	k1gMnSc1
pak	pak	k6eAd1
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
díky	díky	k7c3
zbrani	zbraň	k1gFnSc6
útočná	útočný	k2eAgNnPc1d1
kouzla	kouzlo	k1gNnPc1
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jinak	jinak	k6eAd1
jsou	být	k5eAaImIp3nP
efekty	efekt	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
se	se	k3xPyFc4
na	na	k7c6
zbrani	zbraň	k1gFnSc6
objeví	objevit	k5eAaPmIp3nP
<g/>
,	,	kIx,
náhodné	náhodný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
použití	použití	k1gNnSc6
Top-Grade	Top-Grad	k1gInSc5
lifestonu	lifeston	k1gInSc6
se	se	k3xPyFc4
zbraň	zbraň	k1gFnSc1
navíc	navíc	k6eAd1
rozzáří	rozzářit	k5eAaPmIp3nS
a	a	k8xC
"	"	kIx"
<g/>
zelektrizuje	zelektrizovat	k5eAaPmIp3nS
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s>
Fame	Fame	k1gFnSc1
(	(	kIx(
<g/>
sláva	sláva	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
získáváte	získávat	k5eAaImIp2nP
při	při	k7c6
obléhání	obléhání	k1gNnSc6
pevností	pevnost	k1gFnPc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
při	při	k7c6
bojích	boj	k1gInPc6
o	o	k7c4
hrad	hrad	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
tento	tento	k3xDgInSc4
Fame	Fame	k1gInSc4
<g/>
,	,	kIx,
pak	pak	k6eAd1
můžete	moct	k5eAaImIp2nP
své	svůj	k3xOyFgFnSc3
zbrani	zbraň	k1gFnSc3
nebo	nebo	k8xC
zbroji	zbroj	k1gFnSc3
přidat	přidat	k5eAaPmF
další	další	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
ovšem	ovšem	k9
započítávají	započítávat	k5eAaImIp3nP
jen	jen	k9
v	v	k7c6
boji	boj	k1gInSc6
proti	proti	k7c3
jiným	jiný	k2eAgMnPc3d1
hráčům	hráč	k1gMnPc3
(	(	kIx(
<g/>
např.	např.	kA
snížení	snížení	k1gNnSc4
obdrženého	obdržený	k2eAgNnSc2d1
poškození	poškození	k1gNnSc2
v	v	k7c6
pvp	pvp	k?
u	u	k7c2
zbrojí	zbroj	k1gFnPc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
zvýšená	zvýšený	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
útoku	útok	k1gInSc2
u	u	k7c2
zbraní	zbraň	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Foundation	Foundation	k1gInSc1
věci	věc	k1gFnSc2
...	...	k?
tyto	tento	k3xDgFnPc1
věci	věc	k1gFnPc1
se	se	k3xPyFc4
dají	dát	k5eAaPmIp3nP
získat	získat	k5eAaPmF
pouze	pouze	k6eAd1
pokud	pokud	k8xS
si	se	k3xPyFc3
je	on	k3xPp3gInPc4
sami	sám	k3xTgMnPc1
vyrobíte	vyrobit	k5eAaPmIp2nP
(	(	kIx(
<g/>
u	u	k7c2
hráče	hráč	k1gMnSc2
trpaslíka	trpaslík	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
koupíte	koupit	k5eAaPmIp2nP
u	u	k7c2
jiného	jiný	k2eAgMnSc2d1
hráče	hráč	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
úplně	úplně	k6eAd1
stejné	stejný	k2eAgFnPc1d1
jako	jako	k8xS,k8xC
ty	ten	k3xDgFnPc1
obyčejné	obyčejný	k2eAgFnPc1d1
<g/>
,	,	kIx,
akorát	akorát	k6eAd1
přidávají	přidávat	k5eAaImIp3nP
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
několik	několik	k4yIc1
<g/>
,	,	kIx,
vlastností	vlastnost	k1gFnPc2
navíc	navíc	k6eAd1
<g/>
(	(	kIx(
<g/>
úhybnost	úhybnost	k1gFnSc1
<g/>
,	,	kIx,
počet	počet	k1gInSc1
životů	život	k1gInPc2
atd.	atd.	kA
<g/>
)	)	kIx)
</s>
<s>
Verzování	Verzování	k1gNnSc1
a	a	k8xC
rozšiřování	rozšiřování	k1gNnSc1
</s>
<s>
Hra	hra	k1gFnSc1
je	být	k5eAaImIp3nS
postupně	postupně	k6eAd1
rozšiřována	rozšiřovat	k5eAaImNgFnS
a	a	k8xC
upravována	upravovat	k5eAaImNgFnS
pomocí	pomocí	k7c2
souhrnných	souhrnný	k2eAgInPc2d1
updatů	update	k1gInPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInPc1
názvy	název	k1gInPc1
jsou	být	k5eAaImIp3nP
odvozeny	odvodit	k5eAaPmNgInP
od	od	k7c2
scénáře	scénář	k1gInSc2
<g/>
,	,	kIx,
kterým	který	k3yRgInSc7
se	se	k3xPyFc4
herní	herní	k2eAgInSc1d1
svět	svět	k1gInSc1
ubírá	ubírat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgMnSc7
byla	být	k5eAaImAgFnS
beta	beta	k1gNnSc2
verze	verze	k1gFnSc2
nazvaná	nazvaný	k2eAgFnSc1d1
The	The	k1gFnSc1
Chaotic	Chaotice	k1gFnPc2
Chronicle	Chronicle	k1gFnSc2
<g/>
:	:	kIx,
Prelude	Prelud	k1gInSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Aktuální	aktuální	k2eAgFnSc7d1
verzí	verze	k1gFnSc7
potom	potom	k6eAd1
je	být	k5eAaImIp3nS
Lineage	Lineage	k1gInSc4
2	#num#	k4
Goddess	Goddess	k1gInSc1
of	of	k?
Destruction	Destruction	k1gInSc1
<g/>
'	'	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Všechny	všechen	k3xTgInPc1
tyto	tento	k3xDgInPc1
updaty	update	k1gInPc1
jsou	být	k5eAaImIp3nP
hráčům	hráč	k1gMnPc3
dostupné	dostupný	k2eAgInPc1d1
zdarma	zdarma	k6eAd1
v	v	k7c6
podobě	podoba	k1gFnSc6
updatování	updatování	k1gNnSc2
jejich	jejich	k3xOp3gMnSc2
herního	herní	k2eAgMnSc2d1
klienta	klient	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Obvyklou	obvyklý	k2eAgFnSc7d1
náplní	náplň	k1gFnSc7
jednotlivých	jednotlivý	k2eAgInPc2d1
updatů	update	k1gInPc2
je	být	k5eAaImIp3nS
rozšiřování	rozšiřování	k1gNnSc1
světa	svět	k1gInSc2
<g/>
,	,	kIx,
nové	nový	k2eAgInPc1d1
předměty	předmět	k1gInPc1
<g/>
,	,	kIx,
schopnosti	schopnost	k1gFnPc1
postav	postava	k1gFnPc1
<g/>
,	,	kIx,
nepřátelé	nepřítel	k1gMnPc1
či	či	k8xC
dokonce	dokonce	k9
rasy	rasa	k1gFnPc1
herních	herní	k2eAgFnPc2d1
postav	postava	k1gFnPc2
a	a	k8xC
jejich	jejich	k3xOp3gNnSc2
povolání	povolání	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgInPc1d1
pojmy	pojem	k1gInPc1
</s>
<s>
CH	Ch	kA
=	=	kIx~
clan	clan	k1gMnSc1
hall	hall	k1gMnSc1
<g/>
,	,	kIx,
klanová	klanový	k2eAgFnSc1d1
hala	hala	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
CP	CP	kA
=	=	kIx~
combat	combat	k5eAaImF,k5eAaPmF
point	pointa	k1gFnPc2
<g/>
,	,	kIx,
stínové	stínový	k2eAgInPc1d1
životy	život	k1gInPc1
<g/>
,	,	kIx,
použité	použitý	k2eAgInPc1d1
při	při	k7c6
PvP	PvP	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ubírají	ubírat	k5eAaImIp3nP
se	s	k7c7
první	první	k4xOgFnSc7
<g/>
,	,	kIx,
až	až	k6eAd1
potom	potom	k6eAd1
zranění	zranění	k1gNnSc1
ubírá	ubírat	k5eAaImIp3nS
normální	normální	k2eAgInPc4d1
životy	život	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
CS	CS	kA
=	=	kIx~
castle	castlat	k5eAaPmIp3nS
siege	siege	k1gFnSc1
<g/>
,	,	kIx,
bitva	bitva	k1gFnSc1
o	o	k7c4
hrad	hrad	k1gInSc4
mezi	mezi	k7c7
klany	klan	k1gInPc7
nebo	nebo	k8xC
souboj	souboj	k1gInSc1
klanu	klan	k1gInSc2
proti	proti	k7c3
NPC	NPC	kA
(	(	kIx(
<g/>
pokud	pokud	k8xS
hrad	hrad	k1gInSc1
nikdo	nikdo	k3yNnSc1
doposud	doposud	k6eAd1
nedobyl	dobýt	k5eNaPmAgMnS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Craft	Craft	k1gInSc1
=	=	kIx~
povolání	povolání	k1gNnSc1
postavy	postava	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
umí	umět	k5eAaImIp3nP
vyrábět	vyrábět	k5eAaImF
věci	věc	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Grade	grad	k1gInSc5
=	=	kIx~
(	(	kIx(
<g/>
no	no	k9
grade	grad	k1gInSc5
<g/>
,	,	kIx,
D	D	kA
grade	grad	k1gInSc5
<g/>
,	,	kIx,
C	C	kA
grade	grad	k1gInSc5
<g/>
,	,	kIx,
B	B	kA
grade	grad	k1gInSc5
<g/>
,	,	kIx,
A	a	k9
grade	grad	k1gInSc5
<g/>
,	,	kIx,
S	s	k7c7
grade	grad	k1gInSc5
<g/>
,	,	kIx,
S80	S80	k1gMnSc1
grade	grad	k1gInSc5
a	a	k8xC
S84	S84	k1gMnSc1
grade	grad	k1gInSc5
<g/>
,	,	kIx,
R	R	kA
<g/>
,	,	kIx,
R	R	kA
<g/>
95	#num#	k4
<g/>
,	,	kIx,
R	R	kA
<g/>
99	#num#	k4
<g/>
)	)	kIx)
stupeň	stupeň	k1gInSc4
předmětů	předmět	k1gInPc2
<g/>
,	,	kIx,
rozděleno	rozdělen	k2eAgNnSc1d1
podle	podle	k7c2
úrovně	úroveň	k1gFnSc2
od	od	k7c2
kterého	který	k3yQgInSc2,k3yRgInSc2,k3yIgInSc2
jej	on	k3xPp3gInSc4
může	moct	k5eAaImIp3nS
postava	postava	k1gFnSc1
používat	používat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čím	co	k3yQnSc7,k3yInSc7,k3yRnSc7
vyšší	vysoký	k2eAgInSc1d2
stupeň	stupeň	k1gInSc1
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
lepší	lepšit	k5eAaImIp3nS
předmět	předmět	k1gInSc4
<g/>
,	,	kIx,
ovšem	ovšem	k9
pro	pro	k7c4
každý	každý	k3xTgInSc4
stupeň	stupeň	k1gInSc4
musí	muset	k5eAaImIp3nS
postava	postava	k1gFnSc1
mít	mít	k5eAaImF
dostatečnou	dostatečný	k2eAgFnSc4d1
úroveň	úroveň	k1gFnSc4
(	(	kIx(
<g/>
NG	NG	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
,	,	kIx,
D	D	kA
20	#num#	k4
<g/>
-	-	kIx~
<g/>
39	#num#	k4
<g/>
,	,	kIx,
C	C	kA
40	#num#	k4
<g/>
-	-	kIx~
<g/>
51	#num#	k4
<g/>
,	,	kIx,
B	B	kA
52	#num#	k4
<g/>
-	-	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
60	#num#	k4
<g/>
,	,	kIx,
A	a	k9
61	#num#	k4
<g/>
-	-	kIx~
<g/>
75	#num#	k4
<g/>
,	,	kIx,
S	s	k7c7
76	#num#	k4
<g/>
-	-	kIx~
<g/>
79	#num#	k4
<g/>
,	,	kIx,
S80	S80	k1gFnSc1
80	#num#	k4
<g/>
-	-	kIx~
<g/>
83	#num#	k4
<g/>
,	,	kIx,
S84	S84	k1gFnSc1
84	#num#	k4
<g/>
-	-	kIx~
<g/>
85	#num#	k4
<g/>
,	,	kIx,
R	R	kA
85	#num#	k4
<g/>
-	-	kIx~
<g/>
94	#num#	k4
<g/>
,	,	kIx,
R95	R95	k1gFnSc1
95-98	95-98	k4
a	a	k8xC
R99	R99	k1gMnSc3
95	#num#	k4
<g/>
-	-	kIx~
<g/>
99	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
postava	postava	k1gFnSc1
nosí	nosit	k5eAaImIp3nS
předmět	předmět	k1gInSc4
s	s	k7c7
vyšším	vysoký	k2eAgInSc7d2
stupněm	stupeň	k1gInSc7
<g/>
,	,	kIx,
než	než	k8xS
dovoluje	dovolovat	k5eAaImIp3nS
její	její	k3xOp3gFnSc4
úroveň	úroveň	k1gFnSc4
<g/>
,	,	kIx,
dostane	dostat	k5eAaPmIp3nS
postih	postih	k1gInSc4
v	v	k7c6
podobě	podoba	k1gFnSc6
snížení	snížení	k1gNnSc1
všech	všecek	k3xTgFnPc2
jejích	její	k3xOp3gFnPc2
vlastností	vlastnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
GK	GK	kA
=	=	kIx~
gatekeeper	gatekeeper	k1gMnSc1
<g/>
,	,	kIx,
NPC	NPC	kA
umožňující	umožňující	k2eAgNnSc1d1
teleportování	teleportování	k1gNnSc1
mezi	mezi	k7c7
městy	město	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s>
Hero	Hero	k1gMnSc1
=	=	kIx~
hrdina	hrdina	k1gMnSc1
<g/>
,	,	kIx,
postava	postava	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
vyhraje	vyhrát	k5eAaPmIp3nS
olympiádu	olympiáda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Status	status	k1gInSc1
hrdiny	hrdina	k1gMnSc2
je	být	k5eAaImIp3nS
udělován	udělovat	k5eAaImNgInS
na	na	k7c4
jeden	jeden	k4xCgInSc4
měsíc	měsíc	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postava	postava	k1gFnSc1
získá	získat	k5eAaPmIp3nS
exkluzivní	exkluzivní	k2eAgFnSc4d1
auru	aura	k1gFnSc4
<g/>
,	,	kIx,
dovednosti	dovednost	k1gFnPc4
a	a	k8xC
předměty	předmět	k1gInPc4
<g/>
,	,	kIx,
za	za	k7c4
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
si	se	k3xPyFc3
může	moct	k5eAaImIp3nS
kupovat	kupovat	k5eAaImF
jinak	jinak	k6eAd1
špatně	špatně	k6eAd1
dostupné	dostupný	k2eAgInPc1d1
svitky	svitek	k1gInPc1
očarování	očarování	k1gNnSc2
na	na	k7c4
vylepšení	vylepšení	k1gNnSc4
zbraní	zbraň	k1gFnPc2
a	a	k8xC
zbroje	zbroj	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
L	L	kA
<g/>
2	#num#	k4
<g/>
,	,	kIx,
LA2	LA2	k1gFnSc1
=	=	kIx~
Lineage	Lineage	k1gInSc1
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Noblesse	Noblesse	k1gFnSc1
=	=	kIx~
šlechtic	šlechtic	k1gMnSc1
<g/>
,	,	kIx,
postava	postava	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
splnila	splnit	k5eAaPmAgFnS
speciální	speciální	k2eAgInSc4d1
úkol	úkol	k1gInSc4
a	a	k8xC
má	mít	k5eAaImIp3nS
patřičnou	patřičný	k2eAgFnSc4d1
úroveň	úroveň	k1gFnSc4
na	na	k7c6
hlavním	hlavní	k2eAgNnSc6d1
i	i	k8xC
vedlejším	vedlejší	k2eAgNnSc6d1
povolání	povolání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Může	moct	k5eAaImIp3nS
se	se	k3xPyFc4
zúčastnit	zúčastnit	k5eAaPmF
olympiády	olympiáda	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Přestupák	Přestupák	k1gInSc1
=	=	kIx~
speciální	speciální	k2eAgInSc4d1
úkol	úkol	k1gInSc4
<g/>
,	,	kIx,
díky	díky	k7c3
kterému	který	k3yIgInSc3,k3yQgInSc3,k3yRgInSc3
se	se	k3xPyFc4
povolání	povolání	k1gNnSc2
postavy	postava	k1gFnSc2
dostane	dostat	k5eAaPmIp3nS
na	na	k7c4
vyšší	vysoký	k2eAgInSc4d2
stupeň	stupeň	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Raidboss	Raidboss	k1gInSc1
=	=	kIx~
silná	silný	k2eAgFnSc1d1
nestvůra	nestvůra	k1gFnSc1
<g/>
,	,	kIx,
k	k	k7c3
jejíž	jejíž	k3xOyRp3gFnSc1
zabití	zabití	k1gNnSc4
je	být	k5eAaImIp3nS
potřeba	potřeba	k6eAd1
skupina	skupina	k1gFnSc1
hráčů	hráč	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
WB	WB	kA
=	=	kIx~
World	World	k1gMnSc1
Boss	boss	k1gMnSc1
<g/>
,	,	kIx,
extra	extra	k6eAd1
silní	silný	k2eAgMnPc1d1
bossové	boss	k1gMnPc1
k	k	k7c3
jejímž	jejíž	k3xOyRp3gInSc6
zabití	zabití	k1gNnSc6
je	být	k5eAaImIp3nS
potřeba	potřeba	k1gFnSc1
při	při	k7c6
nejmenším	malý	k2eAgMnSc6d3
hodně	hodně	k6eAd1
dobrý	dobrý	k2eAgInSc4d1
klan	klan	k1gInSc4
či	či	k8xC
allianci	alliance	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
8	#num#	k4
World	Worldo	k1gNnPc2
Bossů	boss	k1gMnPc2
a	a	k8xC
to	ten	k3xDgNnSc1
Antharas	Antharas	k1gInSc4
<g/>
,	,	kIx,
Zaken	Zaken	k1gInSc1
<g/>
,	,	kIx,
Baium	Baium	k1gInSc1
<g/>
,	,	kIx,
Ant	Ant	k1gMnSc1
Queen	Queen	k1gInSc1
<g/>
,	,	kIx,
Valakas	Valakas	k1gInSc1
<g/>
,	,	kIx,
Core	Core	k1gInSc1
<g/>
,	,	kIx,
Orfen	Orfen	k1gInSc1
<g/>
,	,	kIx,
Frintezza	Frintezza	k1gFnSc1
a	a	k8xC
hlavně	hlavně	k9
epické	epický	k2eAgNnSc4d1
monstrum	monstrum	k1gNnSc4
AlkoN	AlkoN	k1gFnSc2
kterýcho	kterýcho	k6eAd1
ještě	ještě	k6eAd1
nikdo	nikdo	k3yNnSc1
nikdy	nikdy	k6eAd1
nepokořil	pokořit	k5eNaPmAgMnS
<g/>
,	,	kIx,
při	při	k7c6
jehož	jehož	k3xOyRp3gInSc6
zabití	zabití	k1gNnSc6
dosáhnete	dosáhnout	k5eAaPmIp2nP
konce	konec	k1gInPc1
hry	hra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
také	také	k9
známí	známit	k5eAaImIp3nP
jako	jako	k9
EP	EP	kA
(	(	kIx(
<g/>
Epic	Epic	k1gFnSc1
Boss	boss	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Soulshot	Soulshot	k1gInSc1
<g/>
,	,	kIx,
Spirit	Spirit	k1gInSc1
shot	shot	k1gInSc1
<g/>
,	,	kIx,
Blessed	Blessed	k1gInSc1
spirit	spirit	k1gInSc1
shot	shot	k1gInSc1
(	(	kIx(
<g/>
SS	SS	kA
<g/>
,	,	kIx,
SpS	SpS	k1gFnSc1
<g/>
,	,	kIx,
BSpS	BSpS	k1gFnSc1
<g/>
)	)	kIx)
=	=	kIx~
speciální	speciální	k2eAgInSc1d1
předmět	předmět	k1gInSc1
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
nazýván	nazýván	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
náboj	náboj	k1gInSc1
<g/>
,	,	kIx,
díky	díky	k7c3
kterému	který	k3yRgMnSc3,k3yQgMnSc3,k3yIgMnSc3
postava	postava	k1gFnSc1
způsobuje	způsobovat	k5eAaImIp3nS
větší	veliký	k2eAgNnSc4d2
zranění	zranění	k1gNnSc4
zbraní	zbraň	k1gFnPc2
(	(	kIx(
<g/>
v	v	k7c6
případě	případ	k1gInSc6
soulshotu	soulshot	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
zvyšuje	zvyšovat	k5eAaImIp3nS
účinek	účinek	k1gInSc1
kouzel	kouzlo	k1gNnPc2
(	(	kIx(
<g/>
v	v	k7c6
případě	případ	k1gInSc6
spiritshotů	spiritshot	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
zbraň	zbraň	k1gFnSc4
stupně	stupeň	k1gInSc2
A	a	k9
potřebujete	potřebovat	k5eAaImIp2nP
soulshoty	soulshot	k1gInPc1
stupně	stupeň	k1gInSc2
A	a	k9
atd.	atd.	kA
<g/>
,	,	kIx,
zvětšení	zvětšení	k1gNnSc1
Zranění	zranění	k1gNnSc2
je	být	k5eAaImIp3nS
zhruba	zhruba	k6eAd1
o	o	k7c4
100	#num#	k4
%	%	kIx~
k	k	k7c3
původnímu	původní	k2eAgMnSc3d1
(	(	kIx(
<g/>
tzn.	tzn.	kA
bez	bez	k7c2
soulshotu	soulshot	k1gInSc2
je	být	k5eAaImIp3nS
např.	např.	kA
zranění	zranění	k1gNnSc3
hodnotě	hodnota	k1gFnSc6
200	#num#	k4
<g/>
,	,	kIx,
se	se	k3xPyFc4
soulshotem	soulshot	k1gInSc7
je	být	k5eAaImIp3nS
zranění	zranění	k1gNnSc1
hodnotně	hodnotně	k6eAd1
na	na	k7c6
úrovni	úroveň	k1gFnSc6
400	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Blessed	Blessed	k1gInSc1
Spirit	Spirita	k1gFnPc2
shoty	shot	k1gInPc7
zrychlují	zrychlovat	k5eAaImIp3nP
tzv.	tzv.	kA
Cast	Cast	k2eAgInSc4d1
time	time	k1gInSc4
+	+	kIx~
sílu	síla	k1gFnSc4
kouzla	kouzlo	k1gNnSc2
taktéž	taktéž	k?
skoro	skoro	k6eAd1
dvojnásobně	dvojnásobně	k6eAd1
<g/>
,	,	kIx,
tj.	tj.	kA
například	například	k6eAd1
vyvolání	vyvolání	k1gNnSc1
útočného	útočný	k2eAgNnSc2d1
kouzla	kouzlo	k1gNnSc2
Wind	Wind	k1gInSc1
striku	strika	k1gFnSc4
bez	bez	k7c2
Bsps	Bspsa	k1gFnPc2
je	být	k5eAaImIp3nS
cca	cca	kA
5	#num#	k4
Sekund	sekunda	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
z	z	k7c2
bsps	bspsa	k1gFnPc2
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
zhruba	zhruba	k6eAd1
3	#num#	k4
sekundy	sekunda	k1gFnPc4
<g/>
,	,	kIx,
Bráno	brán	k2eAgNnSc4d1
bez	bez	k7c2
buffů	buff	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Spoil	Spoil	k1gInSc1
=	=	kIx~
povolání	povolání	k1gNnSc2
postavy	postava	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
umí	umět	k5eAaImIp3nS
získat	získat	k5eAaPmF
lepší	dobrý	k2eAgFnSc4d2
kořist	kořist	k1gFnSc4
ze	z	k7c2
zabitých	zabitý	k2eAgFnPc2d1
nestvůr	nestvůra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Servery	server	k1gInPc1
</s>
<s>
Hra	hra	k1gFnSc1
Lineage	Lineag	k1gFnSc2
II	II	kA
se	se	k3xPyFc4
hraje	hrát	k5eAaImIp3nS
na	na	k7c6
řadě	řada	k1gFnSc6
serverů	server	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základě	základ	k1gInSc6
zeměpisné	zeměpisný	k2eAgFnSc2d1
polohy	poloha	k1gFnSc2
jsou	být	k5eAaImIp3nP
pro	pro	k7c4
nás	my	k3xPp1nPc4
určené	určený	k2eAgNnSc1d1
oficiální	oficiální	k2eAgInPc1d1
servery	server	k1gInPc1
L2NA	L2NA	k1gFnSc2
(	(	kIx(
<g/>
USA	USA	kA
a	a	k8xC
EU	EU	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
avšak	avšak	k8xC
hraní	hranit	k5eAaImIp3nS
na	na	k7c6
nich	on	k3xPp3gFnPc6
není	být	k5eNaImIp3nS
blokováno	blokován	k2eAgNnSc1d1
na	na	k7c6
základě	základ	k1gInSc6
původu	původ	k1gInSc2
IP	IP	kA
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
praxe	praxe	k1gFnSc1
jinde	jinde	k6eAd1
ve	v	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
existuje	existovat	k5eAaImIp3nS
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
neoficiálních	oficiální	k2eNgInPc2d1,k2eAgInPc2d1
tzv.	tzv.	kA
private-serverů	private-server	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
některých	některý	k3yIgInPc2
z	z	k7c2
těchto	tento	k3xDgInPc2
serverů	server	k1gInPc2
jsou	být	k5eAaImIp3nP
upravené	upravený	k2eAgInPc1d1
tzv.	tzv.	kA
raty	raty	k1gInPc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
násobitel	násobitel	k1gInSc1
počtu	počet	k1gInSc2
získaných	získaný	k2eAgFnPc2d1
zkušeností	zkušenost	k1gFnPc2
(	(	kIx(
<g/>
EXP	exp	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zkušenostních	zkušenostní	k2eAgInPc2d1
bodů	bod	k1gInPc2
(	(	kIx(
<g/>
SP	SP	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
šance	šance	k1gFnSc1
na	na	k7c6
DROP	drop	k1gMnSc1
(	(	kIx(
<g/>
vypadnutí	vypadnutí	k1gNnSc1
ze	z	k7c2
zabitého	zabitý	k2eAgNnSc2d1
monstra	monstrum	k1gNnSc2
<g/>
)	)	kIx)
celých	celý	k2eAgInPc2d1
předmětů	předmět	k1gInPc2
a	a	k8xC
množství	množství	k1gNnSc4
získaných	získaný	k2eAgFnPc2d1
aden	adena	k1gFnPc2
(	(	kIx(
<g/>
měna	měna	k1gFnSc1
ve	v	k7c6
světě	svět	k1gInSc6
L	L	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
na	na	k7c6
oficiálních	oficiální	k2eAgInPc6d1
serverech	server	k1gInPc6
jsou	být	k5eAaImIp3nP
raty	rata	k1gFnPc1
na	na	k7c6
začátku	začátek	k1gInSc6
nastaveny	nastavit	k5eAaPmNgInP
na	na	k7c6
1	#num#	k4
<g/>
×	×	k?
<g/>
,	,	kIx,
na	na	k7c6
private-serverech	private-server	k1gInPc6
se	se	k3xPyFc4
pohybují	pohybovat	k5eAaImIp3nP
od	od	k7c2
1	#num#	k4
<g/>
×	×	k?
až	až	k6eAd1
do	do	k7c2
nekonečna	nekonečno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Jelikož	jelikož	k8xS
je	být	k5eAaImIp3nS
už	už	k9
Lineage	Lineage	k1gFnSc1
II	II	kA
Zdarma	zdarma	k6eAd1
na	na	k7c6
oficiálních	oficiální	k2eAgInPc6d1
serverech	server	k1gInPc6
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
většina	většina	k1gFnSc1
private-serverů	private-server	k1gMnPc2
je	být	k5eAaImIp3nS
bez	bez	k7c2
hráčů	hráč	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Prelude	Prelud	k1gMnSc5
<g/>
↑	↑	k?
Goddess	Goddess	k1gInSc1
of	of	k?
Destruction	Destruction	k1gInSc1
–	–	k?
EN	EN	kA
<g/>
↑	↑	k?
Official	Official	k1gMnSc1
page	pag	k1gFnSc2
–	–	k?
EN	EN	kA
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
MMORPG	MMORPG	kA
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Lineage	Lineage	k1gInSc1
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
com	com	k?
–	–	k?
oficiální	oficiální	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
hry	hra	k1gFnSc2
</s>
<s>
Lineage	Lineage	k1gFnSc1
2	#num#	k4
recenze	recenze	k1gFnSc1
na	na	k7c4
czechgamer	czechgamer	k1gInSc4
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
Lineage	Lineage	k1gInSc1
2	#num#	k4
na	na	k7c6
České	český	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
her	hra	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Počítačové	počítačový	k2eAgFnPc1d1
hry	hra	k1gFnPc1
</s>
