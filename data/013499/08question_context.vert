<s>
Obvyklou	obvyklý	k2eAgFnSc7d1
náplní	náplň	k1gFnSc7
jednotlivých	jednotlivý	k2eAgInPc2d1
updatů	update	k1gInPc2
je	být	k5eAaImIp3nS
rozšiřování	rozšiřování	k1gNnSc1
světa	svět	k1gInSc2
<g/>
,	,	kIx,
nové	nový	k2eAgInPc1d1
předměty	předmět	k1gInPc1
<g/>
,	,	kIx,
schopnosti	schopnost	k1gFnPc1
postav	postava	k1gFnPc1
<g/>
,	,	kIx,
nepřátelé	nepřítel	k1gMnPc1
či	či	k8xC
dokonce	dokonce	k9
rasy	rasa	k1gFnPc1
herních	herní	k2eAgFnPc2d1
postav	postava	k1gFnPc2
a	a	k8xC
jejich	jejich	k3xOp3gNnSc2
povolání	povolání	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Lineage	Lineage	k1gFnSc1
II	II	kA
<g/>
:	:	kIx,
The	The	kA
Chaotic	Chaotice	k2eAgFnSc1d1
Chronicle	Chronicle	k1gFnSc1
je	být	k5eAaImIp3nS
počítačová	počítačový	k2eAgFnSc1d1
fantasy	fantas	k1gInPc1
hromadná	hromadný	k2eAgFnSc1d1
online	online	k2eAgFnSc1d1
hra	hra	k1gFnSc1
na	na	k7c4
hrdiny	hrdina	k1gMnPc4
(	(	kIx(
<g/>
MMORPG	MMORPG	kA
<g/>
)	)	kIx)
vytvořená	vytvořený	k2eAgFnSc1d1
korejskou	korejský	k2eAgFnSc7d1
obchodní	obchodní	k2eAgFnSc7d1
společností	společnost	k1gFnSc7
NCsoft	NCsofta	k1gInSc1
<g/>
.	.	kIx.
</s>