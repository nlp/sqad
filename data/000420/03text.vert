<s>
Kain	Kain	k1gMnSc1	Kain
byl	být	k5eAaImAgMnS	být
podle	podle	k7c2	podle
4	[number]	k4	4
<g/>
.	.	kIx.	.
kapitoly	kapitola	k1gFnPc4	kapitola
biblické	biblický	k2eAgFnSc2d1	biblická
knihy	kniha	k1gFnSc2	kniha
Genesis	Genesis	k1gFnSc4	Genesis
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
prvních	první	k4xOgMnPc2	první
lidí	člověk	k1gMnPc2	člověk
Adama	Adam	k1gMnSc2	Adam
a	a	k8xC	a
Evy	Eva	k1gFnSc2	Eva
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
po	po	k7c6	po
vyhnání	vyhnání	k1gNnSc6	vyhnání
ze	z	k7c2	z
zahrady	zahrada	k1gFnSc2	zahrada
Edenu	Eden	k1gInSc2	Eden
a	a	k8xC	a
podle	podle	k7c2	podle
Midraše	Midraše	k1gFnSc2	Midraše
společně	společně	k6eAd1	společně
se	s	k7c7	s
sestrou-dvojčetem	sestrouvojče	k1gNnSc7	sestrou-dvojče
<g/>
..	..	k?	..
</s>
<s>
Je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xC	jako
bratrovrah	bratrovrah	k1gMnSc1	bratrovrah
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zabil	zabít	k5eAaPmAgMnS	zabít
svého	svůj	k3xOyFgMnSc4	svůj
mladšího	mladý	k2eAgMnSc4d2	mladší
bratra	bratr	k1gMnSc4	bratr
Ábela	Ábel	k1gMnSc4	Ábel
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Hospodin	Hospodin	k1gMnSc1	Hospodin
nepřijal	přijmout	k5eNaPmAgMnS	přijmout
jeho	jeho	k3xOp3gFnSc4	jeho
oběť	oběť	k1gFnSc4	oběť
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Ábelovu	Ábelův	k2eAgFnSc4d1	Ábelova
ano	ano	k9	ano
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
čin	čin	k1gInSc4	čin
Bohem	bůh	k1gMnSc7	bůh
vyhnán	vyhnat	k5eAaPmNgMnS	vyhnat
a	a	k8xC	a
označen	označen	k2eAgInSc1d1	označen
zvláštním	zvláštní	k2eAgNnSc7d1	zvláštní
znamením	znamení	k1gNnSc7	znamení
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gMnSc4	on
nemohl	moct	k5eNaImAgMnS	moct
nikdo	nikdo	k3yNnSc1	nikdo
zabít	zabít	k5eAaPmF	zabít
<g/>
,	,	kIx,	,
a	a	k8xC	a
ušetřit	ušetřit	k5eAaPmF	ušetřit
jej	on	k3xPp3gMnSc4	on
tak	tak	k8xS	tak
trestu	trest	k1gInSc3	trest
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
dalším	další	k2eAgMnSc7d1	další
mnohem	mnohem	k6eAd1	mnohem
mladším	mladý	k2eAgMnSc7d2	mladší
bratrem	bratr	k1gMnSc7	bratr
byl	být	k5eAaImAgMnS	být
Šét	Šét	k1gFnSc4	Šét
<g/>
,	,	kIx,	,
o	o	k7c6	o
němž	jenž	k3xRgInSc6	jenž
Eva	Eva	k1gFnSc1	Eva
řekla	říct	k5eAaPmAgFnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bůh	bůh	k1gMnSc1	bůh
jí	jíst	k5eAaImIp3nS	jíst
vložil	vložit	k5eAaPmAgMnS	vložit
dalšího	další	k2eAgMnSc4d1	další
potomka	potomek	k1gMnSc4	potomek
místo	místo	k7c2	místo
Ábela	Ábel	k1gMnSc2	Ábel
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
"	"	kIx"	"
<g/>
Kain	Kain	k1gMnSc1	Kain
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
ק	ק	k?	ק
<g/>
ַ	ַ	k?	ַ
<g/>
י	י	k?	י
<g/>
ִ	ִ	k?	ִ
<g/>
ן	ן	k?	ן
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vykládá	vykládat	k5eAaImIp3nS	vykládat
nejednotně	jednotně	k6eNd1	jednotně
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
od	od	k7c2	od
"	"	kIx"	"
<g/>
Získaný	získaný	k2eAgInSc4d1	získaný
<g/>
"	"	kIx"	"
přes	přes	k7c4	přes
"	"	kIx"	"
<g/>
Tepající	tepající	k2eAgMnPc4d1	tepající
<g/>
"	"	kIx"	"
až	až	k9	až
po	po	k7c4	po
"	"	kIx"	"
<g/>
Závistivý	závistivý	k2eAgInSc4d1	závistivý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kain	Kain	k1gMnSc1	Kain
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Rolníkem	rolník	k1gMnSc7	rolník
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čase	čas	k1gInSc6	čas
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kain	Kain	k1gMnSc1	Kain
přinesl	přinést	k5eAaPmAgMnS	přinést
Hospodinu	Hospodin	k1gMnSc3	Hospodin
obětní	obětní	k2eAgInSc4d1	obětní
dar	dar	k1gInSc4	dar
z	z	k7c2	z
plodů	plod	k1gInPc2	plod
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
Ábel	Ábel	k1gMnSc1	Ábel
přinesl	přinést	k5eAaPmAgMnS	přinést
Hospodinu	Hospodin	k1gMnSc3	Hospodin
obětní	obětní	k2eAgInSc4d1	obětní
dar	dar	k1gInSc4	dar
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
on	on	k3xPp3gMnSc1	on
z	z	k7c2	z
prvorozených	prvorozený	k2eAgMnPc2d1	prvorozený
svého	svůj	k3xOyFgNnSc2	svůj
stáda	stádo	k1gNnSc2	stádo
a	a	k8xC	a
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
tučné	tučný	k2eAgFnPc1d1	tučná
<g/>
.	.	kIx.	.
</s>
<s>
Hospodin	Hospodin	k1gMnSc1	Hospodin
shlédl	shlédnout	k5eAaPmAgMnS	shlédnout
na	na	k7c4	na
Ábela	Ábel	k1gMnSc4	Ábel
a	a	k8xC	a
na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
obětní	obětní	k2eAgInSc4d1	obětní
dar	dar	k1gInSc4	dar
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
Kaina	Kain	k1gMnSc4	Kain
a	a	k8xC	a
na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
obětní	obětní	k2eAgInSc4d1	obětní
dar	dar	k1gInSc4	dar
neshlédl	shlédnout	k5eNaPmAgMnS	shlédnout
<g/>
.	.	kIx.	.
</s>
<s>
Kain	Kain	k1gMnSc1	Kain
vzplanul	vzplanout	k5eAaPmAgMnS	vzplanout
velikým	veliký	k2eAgInSc7d1	veliký
hněvem	hněv	k1gInSc7	hněv
<g/>
.	.	kIx.	.
</s>
<s>
Hospodin	Hospodin	k1gMnSc1	Hospodin
se	se	k3xPyFc4	se
Kaina	Kain	k1gMnSc2	Kain
ptá	ptat	k5eAaImIp3nS	ptat
po	po	k7c6	po
důvodech	důvod	k1gInPc6	důvod
jeho	on	k3xPp3gInSc2	on
hněvu	hněv	k1gInSc2	hněv
<g/>
.	.	kIx.	.
</s>
<s>
Ubezpečuje	ubezpečovat	k5eAaImIp3nS	ubezpečovat
Kaina	Kain	k1gMnSc4	Kain
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k8xC	i
on	on	k3xPp3gMnSc1	on
bude	být	k5eAaImBp3nS	být
"	"	kIx"	"
<g/>
pozdvižen	pozdvižen	k2eAgMnSc1d1	pozdvižen
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
li	li	k8xS	li
jednat	jednat	k5eAaImF	jednat
správně	správně	k6eAd1	správně
<g/>
.	.	kIx.	.
</s>
<s>
Hospodin	Hospodin	k1gMnSc1	Hospodin
dále	daleko	k6eAd2	daleko
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
hříchu	hřích	k1gInSc6	hřích
<g/>
.	.	kIx.	.
</s>
<s>
Hřích	hřích	k1gInSc1	hřích
číhá	číhat	k5eAaImIp3nS	číhat
ve	v	k7c6	v
dveřích	dveře	k1gFnPc6	dveře
<g/>
.	.	kIx.	.
</s>
<s>
Hřích	hřích	k1gInSc1	hřích
po	po	k7c6	po
Kainovi	Kain	k1gMnSc6	Kain
dychtí	dychtit	k5eAaImIp3nS	dychtit
<g/>
.	.	kIx.	.
</s>
<s>
Hospodin	Hospodin	k1gMnSc1	Hospodin
ale	ale	k9	ale
od	od	k7c2	od
Kaina	Kain	k1gMnSc2	Kain
očekává	očekávat	k5eAaImIp3nS	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
nad	nad	k7c7	nad
hříchem	hřích	k1gInSc7	hřích
vládnout	vládnout	k5eAaImF	vládnout
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgMnS	být
Kain	Kain	k1gMnSc1	Kain
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Ábelem	Ábel	k1gMnSc7	Ábel
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
Kain	Kain	k1gMnSc1	Kain
povstal	povstat	k5eAaPmAgMnS	povstat
proti	proti	k7c3	proti
svému	svůj	k3xOyFgMnSc3	svůj
bratru	bratr	k1gMnSc3	bratr
Ábelovi	Ábel	k1gMnSc3	Ábel
a	a	k8xC	a
zabil	zabít	k5eAaPmAgInS	zabít
ho	on	k3xPp3gNnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
člověk	člověk	k1gMnSc1	člověk
viděl	vidět	k5eAaImAgMnS	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
ukončit	ukončit	k5eAaPmF	ukončit
život	život	k1gInSc4	život
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Hospodin	Hospodin	k1gMnSc1	Hospodin
se	se	k3xPyFc4	se
Kaina	Kain	k1gMnSc2	Kain
ptal	ptat	k5eAaImAgMnS	ptat
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Ábel	Ábel	k1gMnSc1	Ábel
<g/>
?	?	kIx.	?
</s>
<s>
Kain	Kain	k1gMnSc1	Kain
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
strážcem	strážce	k1gMnSc7	strážce
svého	svůj	k1gMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Hospodin	Hospodin	k1gMnSc1	Hospodin
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
Kaina	Kain	k1gMnSc2	Kain
zeptal	zeptat	k5eAaPmAgMnS	zeptat
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
to	ten	k3xDgNnSc4	ten
udělal	udělat	k5eAaPmAgMnS	udělat
<g/>
,	,	kIx,	,
a	a	k8xC	a
pak	pak	k6eAd1	pak
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
krev	krev	k1gFnSc1	krev
jeho	on	k3xPp3gMnSc2	on
bratra	bratr	k1gMnSc2	bratr
k	k	k7c3	k
Hospodinu	Hospodin	k1gMnSc3	Hospodin
křičí	křičet	k5eAaImIp3nS	křičet
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
!	!	kIx.	!
</s>
<s>
Potom	potom	k6eAd1	potom
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
Hospodin	Hospodin	k1gMnSc1	Hospodin
kletbu	kletba	k1gFnSc4	kletba
<g/>
,	,	kIx,	,
že	že	k8xS	že
nyní	nyní	k6eAd1	nyní
bude	být	k5eAaImBp3nS	být
Kain	Kain	k1gMnSc1	Kain
proklet	proklít	k5eAaPmNgMnS	proklít
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
otevřela	otevřít	k5eAaPmAgFnS	otevřít
svá	svůj	k3xOyFgNnPc4	svůj
ústa	ústa	k1gNnPc4	ústa
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
z	z	k7c2	z
Kainovy	Kainův	k2eAgFnSc2d1	Kainova
ruky	ruka	k1gFnSc2	ruka
přijala	přijmout	k5eAaPmAgFnS	přijmout
krev	krev	k1gFnSc1	krev
jeho	on	k3xPp3gMnSc2	on
bratra	bratr	k1gMnSc2	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
bude	být	k5eAaImBp3nS	být
Kain	Kain	k1gMnSc1	Kain
obdělávat	obdělávat	k5eAaImF	obdělávat
zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
již	jenž	k3xRgFnSc4	jenž
mu	on	k3xPp3gMnSc3	on
nevydá	vydat	k5eNaPmIp3nS	vydat
svou	svůj	k3xOyFgFnSc4	svůj
sílu	síla	k1gFnSc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Kain	Kain	k1gMnSc1	Kain
bude	být	k5eAaImBp3nS	být
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
bezdomovcem	bezdomovec	k1gMnSc7	bezdomovec
a	a	k8xC	a
tulákem	tulák	k1gMnSc7	tulák
<g/>
.	.	kIx.	.
</s>
<s>
Kain	Kain	k1gMnSc1	Kain
Hospodinu	Hospodin	k1gMnSc3	Hospodin
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
trest	trest	k1gInSc1	trest
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgInSc1d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
může	moct	k5eAaImIp3nS	moct
unést	unést	k5eAaPmF	unést
<g/>
,	,	kIx,	,
že	že	k8xS	že
dnes	dnes	k6eAd1	dnes
jej	on	k3xPp3gMnSc4	on
Hospodin	Hospodin	k1gMnSc1	Hospodin
vyhnal	vyhnat	k5eAaPmAgMnS	vyhnat
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
země	zem	k1gFnSc2	zem
a	a	k8xC	a
před	před	k7c7	před
Jeho	jeho	k3xOp3gFnSc7	jeho
tváří	tvář	k1gFnSc7	tvář
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
Kain	Kain	k1gMnSc1	Kain
ukrývat	ukrývat	k5eAaImF	ukrývat
<g/>
.	.	kIx.	.
</s>
<s>
Bude	být	k5eAaImBp3nS	být
na	na	k7c4	na
zemi	zem	k1gFnSc4	zem
bezdomovcem	bezdomovec	k1gMnSc7	bezdomovec
a	a	k8xC	a
tulákem	tulák	k1gMnSc7	tulák
a	a	k8xC	a
každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
jej	on	k3xPp3gMnSc4	on
najde	najít	k5eAaPmIp3nS	najít
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
jej	on	k3xPp3gMnSc4	on
zabít	zabít	k5eAaPmF	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Hospodin	Hospodin	k1gMnSc1	Hospodin
mu	on	k3xPp3gMnSc3	on
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
tak	tak	k6eAd1	tak
to	ten	k3xDgNnSc1	ten
nebude	být	k5eNaImBp3nS	být
<g/>
,	,	kIx,	,
kdokoliv	kdokoliv	k3yInSc1	kdokoliv
by	by	kYmCp3nS	by
Kaina	Kain	k1gMnSc2	Kain
zabil	zabít	k5eAaPmAgMnS	zabít
<g/>
,	,	kIx,	,
přivodí	přivodit	k5eAaBmIp3nS	přivodit
si	se	k3xPyFc3	se
sedminásobnou	sedminásobný	k2eAgFnSc4d1	sedminásobná
pomstu	pomsta	k1gFnSc4	pomsta
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
Hospodin	Hospodin	k1gMnSc1	Hospodin
vložil	vložit	k5eAaPmAgMnS	vložit
na	na	k7c4	na
Kaina	Kain	k1gMnSc4	Kain
znamení	znamení	k1gNnSc2	znamení
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gMnSc4	on
nikdo	nikdo	k3yNnSc1	nikdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
ho	on	k3xPp3gMnSc4	on
najde	najít	k5eAaPmIp3nS	najít
<g/>
,	,	kIx,	,
nezabil	zabít	k5eNaPmAgMnS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Raši	Raši	k6eAd1	Raši
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bůh	bůh	k1gMnSc1	bůh
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c4	na
Kainovo	Kainův	k2eAgNnSc4d1	Kainovo
čelo	čelo	k1gNnSc4	čelo
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
písmen	písmeno	k1gNnPc2	písmeno
tetragramu	tetragram	k1gInSc2	tetragram
svého	svůj	k3xOyFgNnSc2	svůj
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Kain	Kain	k1gMnSc1	Kain
odešel	odejít	k5eAaPmAgMnS	odejít
od	od	k7c2	od
Hospodina	Hospodin	k1gMnSc2	Hospodin
a	a	k8xC	a
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
Nódu	nód	k1gInSc2	nód
na	na	k7c4	na
východ	východ	k1gInSc4	východ
Edenu	Eden	k1gInSc2	Eden
<g/>
.	.	kIx.	.
</s>
<s>
Nestal	stát	k5eNaPmAgInS	stát
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
ateista	ateista	k1gMnSc1	ateista
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
Hospodina	Hospodin	k1gMnSc4	Hospodin
zná	znát	k5eAaImIp3nS	znát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odešel	odejít	k5eAaPmAgMnS	odejít
od	od	k7c2	od
něj	on	k3xPp3gInSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nějakém	nějaký	k3yIgInSc6	nějaký
čase	čas	k1gInSc6	čas
poznal	poznat	k5eAaPmAgMnS	poznat
Kain	Kain	k1gMnSc1	Kain
svou	svůj	k3xOyFgFnSc4	svůj
ženu	žena	k1gFnSc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
otěhotněla	otěhotnět	k5eAaPmAgFnS	otěhotnět
a	a	k8xC	a
porodila	porodit	k5eAaPmAgFnS	porodit
Kainovi	Kain	k1gMnSc3	Kain
Enocha	Enoch	k1gMnSc4	Enoch
<g/>
.	.	kIx.	.
</s>
<s>
Kain	Kain	k1gMnSc1	Kain
stavěl	stavět	k5eAaImAgMnS	stavět
město	město	k1gNnSc4	město
a	a	k8xC	a
nazval	nazvat	k5eAaPmAgMnS	nazvat
jméno	jméno	k1gNnSc4	jméno
toho	ten	k3xDgNnSc2	ten
města	město	k1gNnSc2	město
podle	podle	k7c2	podle
jména	jméno	k1gNnSc2	jméno
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
<g/>
.	.	kIx.	.
</s>
<s>
Dál	daleko	k6eAd2	daleko
Bible	bible	k1gFnSc1	bible
sleduje	sledovat	k5eAaImIp3nS	sledovat
Kainovu	Kainův	k2eAgFnSc4d1	Kainova
rodovou	rodový	k2eAgFnSc4d1	rodová
linii	linie	k1gFnSc4	linie
přes	přes	k7c4	přes
Enocha	Enoch	k1gMnSc4	Enoch
až	až	k9	až
k	k	k7c3	k
Túbal-kainovi	Túbalain	k1gMnSc3	Túbal-kain
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
sestře	sestra	k1gFnSc3	sestra
Naamě	Naama	k1gFnSc3	Naama
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
Midraše	Midraše	k1gFnSc2	Midraše
stala	stát	k5eAaPmAgFnS	stát
manželkou	manželka	k1gFnSc7	manželka
Noema	Noe	k1gMnSc2	Noe
<g/>
.	.	kIx.	.
</s>
<s>
Kain	Kain	k1gMnSc1	Kain
je	být	k5eAaImIp3nS	být
novozákonní	novozákonní	k2eAgFnSc7d1	novozákonní
částí	část	k1gFnSc7	část
Bible	bible	k1gFnSc2	bible
interpretován	interpretován	k2eAgInSc4d1	interpretován
jako	jako	k8xS	jako
příklad	příklad	k1gInSc4	příklad
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
ze	z	k7c2	z
Zlého	zlé	k1gNnSc2	zlé
<g/>
.	.	kIx.	.
</s>
<s>
Motivem	motiv	k1gInSc7	motiv
vraždy	vražda	k1gFnSc2	vražda
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc2	který
se	se	k3xPyFc4	se
Kain	Kain	k1gMnSc1	Kain
dopustil	dopustit	k5eAaPmAgMnS	dopustit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
hněv	hněv	k1gInSc4	hněv
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
obtíženého	obtížený	k2eAgMnSc2d1	obtížený
vinou	vinou	k7c2	vinou
vlastních	vlastní	k2eAgInPc2d1	vlastní
zlých	zlý	k2eAgInPc2d1	zlý
skutků	skutek	k1gInPc2	skutek
<g/>
,	,	kIx,	,
vůči	vůči	k7c3	vůči
usvědčující	usvědčující	k2eAgFnSc3d1	usvědčující
spravedlnosti	spravedlnost	k1gFnSc3	spravedlnost
bratra	bratr	k1gMnSc2	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Kain	Kain	k1gMnSc1	Kain
přinesl	přinést	k5eAaPmAgMnS	přinést
Bohu	bůh	k1gMnSc3	bůh
oběť	oběť	k1gFnSc4	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
tedy	tedy	k9	tedy
příkladem	příklad	k1gInSc7	příklad
člověka	člověk	k1gMnSc2	člověk
nevěřícího	nevěřící	k1gMnSc2	nevěřící
<g/>
,	,	kIx,	,
či	či	k8xC	či
ateisty	ateista	k1gMnPc4	ateista
<g/>
.	.	kIx.	.
</s>
<s>
Nebyla	být	k5eNaImAgFnS	být
to	ten	k3xDgNnSc1	ten
ovšem	ovšem	k9	ovšem
oběť	oběť	k1gFnSc1	oběť
<g/>
,	,	kIx,	,
skrze	skrze	k?	skrze
kterou	který	k3yIgFnSc4	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dostalo	dostat	k5eAaPmAgNnS	dostat
svědectví	svědectví	k1gNnSc4	svědectví
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
spravedlivý	spravedlivý	k2eAgInSc1d1	spravedlivý
<g/>
,	,	kIx,	,
když	když	k8xS	když
Bůh	bůh	k1gMnSc1	bůh
vydával	vydávat	k5eAaImAgMnS	vydávat
svědectví	svědectví	k1gNnSc4	svědectví
při	při	k7c6	při
jeho	jeho	k3xOp3gInPc6	jeho
darech	dar	k1gInPc6	dar
<g/>
.	.	kIx.	.
</s>
<s>
Novozákonní	novozákonní	k2eAgInSc1d1	novozákonní
list	list	k1gInSc1	list
Judův	Judův	k2eAgInSc1d1	Judův
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
biblické	biblický	k2eAgNnSc1d1	biblické
"	"	kIx"	"
<g/>
běda	běda	k1gFnSc1	běda
<g/>
"	"	kIx"	"
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
vydali	vydat	k5eAaPmAgMnP	vydat
Kainovou	Kainův	k2eAgFnSc7d1	Kainova
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
