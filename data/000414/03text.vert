<s>
Newtonovy	Newtonův	k2eAgInPc1d1	Newtonův
pohybové	pohybový	k2eAgInPc1d1	pohybový
zákony	zákon	k1gInPc1	zákon
jsou	být	k5eAaImIp3nP	být
fyzikální	fyzikální	k2eAgInPc1d1	fyzikální
zákony	zákon	k1gInPc1	zákon
formulované	formulovaný	k2eAgFnSc2d1	formulovaná
Isaacem	Isaace	k1gMnSc7	Isaace
Newtonem	Newton	k1gMnSc7	Newton
<g/>
.	.	kIx.	.
</s>
<s>
Popisují	popisovat	k5eAaImIp3nP	popisovat
vztah	vztah	k1gInSc4	vztah
mezi	mezi	k7c7	mezi
pohybem	pohyb	k1gInSc7	pohyb
tělesa	těleso	k1gNnSc2	těleso
a	a	k8xC	a
silami	síla	k1gFnPc7	síla
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
těleso	těleso	k1gNnSc4	těleso
působí	působit	k5eAaImIp3nS	působit
<g/>
.	.	kIx.	.
</s>
<s>
Newton	Newton	k1gMnSc1	Newton
zavedl	zavést	k5eAaPmAgMnS	zavést
celkem	celkem	k6eAd1	celkem
tři	tři	k4xCgInPc1	tři
pohybové	pohybový	k2eAgInPc1d1	pohybový
zákony	zákon	k1gInPc1	zákon
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
základ	základ	k1gInSc4	základ
klasické	klasický	k2eAgFnSc2d1	klasická
mechaniky	mechanika	k1gFnSc2	mechanika
a	a	k8xC	a
zejména	zejména	k9	zejména
dynamiky	dynamika	k1gFnSc2	dynamika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
příčiny	příčina	k1gFnPc1	příčina
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
zákony	zákon	k1gInPc1	zákon
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
jaký	jaký	k3yQgInSc1	jaký
bude	být	k5eAaImBp3nS	být
pohyb	pohyb	k1gInSc1	pohyb
tělesa	těleso	k1gNnSc2	těleso
v	v	k7c6	v
inerciální	inerciální	k2eAgFnSc6d1	inerciální
vztažné	vztažný	k2eAgFnSc6d1	vztažná
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
známy	znám	k2eAgInPc1d1	znám
síly	síl	k1gInPc1	síl
působící	působící	k2eAgInPc1d1	působící
na	na	k7c4	na
těleso	těleso	k1gNnSc4	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
také	také	k9	také
Zákon	zákon	k1gInSc1	zákon
setrvačnosti	setrvačnost	k1gFnSc2	setrvačnost
<g/>
.	.	kIx.	.
</s>
<s>
Corpus	corpus	k1gInSc1	corpus
omne	omnout	k5eAaPmIp3nS	omnout
perseverare	perseverar	k1gMnSc5	perseverar
in	in	k?	in
statu	status	k1gInSc6	status
suo	suo	k?	suo
quiescendi	quiescend	k1gMnPc1	quiescend
vel	velet	k5eAaImRp2nS	velet
movendi	movendit	k5eAaPmRp2nS	movendit
uniformiter	uniformiter	k1gInSc1	uniformiter
in	in	k?	in
directum	directum	k1gNnSc1	directum
<g/>
,	,	kIx,	,
nisi	nisi	k6eAd1	nisi
quatenus	quatenus	k1gMnSc1	quatenus
illud	illud	k1gMnSc1	illud
a	a	k8xC	a
viribus	viribus	k1gMnSc1	viribus
impressis	impressis	k1gFnSc2	impressis
cogitur	cogitura	k1gFnPc2	cogitura
statum	statum	k1gNnSc1	statum
suum	suum	k1gMnSc1	suum
mutare	mutar	k1gMnSc5	mutar
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
na	na	k7c4	na
těleso	těleso	k1gNnSc4	těleso
nepůsobí	působit	k5eNaImIp3nP	působit
žádné	žádný	k3yNgFnPc4	žádný
vnější	vnější	k2eAgFnPc4d1	vnější
síly	síla	k1gFnPc4	síla
nebo	nebo	k8xC	nebo
výslednice	výslednice	k1gFnSc1	výslednice
sil	síla	k1gFnPc2	síla
je	být	k5eAaImIp3nS	být
nulová	nulový	k2eAgFnSc1d1	nulová
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
těleso	těleso	k1gNnSc1	těleso
setrvává	setrvávat	k5eAaImIp3nS	setrvávat
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
nebo	nebo	k8xC	nebo
v	v	k7c6	v
rovnoměrném	rovnoměrný	k2eAgInSc6d1	rovnoměrný
přímočarém	přímočarý	k2eAgInSc6d1	přímočarý
pohybu	pohyb	k1gInSc6	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Ekvivalentní	ekvivalentní	k2eAgFnSc1d1	ekvivalentní
(	(	kIx(	(
<g/>
srozumitelná	srozumitelný	k2eAgFnSc1d1	srozumitelná
<g/>
)	)	kIx)	)
formulace	formulace	k1gFnSc1	formulace
zní	znět	k5eAaImIp3nS	znět
<g/>
:	:	kIx,	:
Těleso	těleso	k1gNnSc1	těleso
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
nebo	nebo	k8xC	nebo
rovnoměrném	rovnoměrný	k2eAgInSc6d1	rovnoměrný
přímočarém	přímočarý	k2eAgInSc6d1	přímočarý
pohybu	pohyb	k1gInSc6	pohyb
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
nuceno	nutit	k5eAaImNgNnS	nutit
vnějšími	vnější	k2eAgFnPc7d1	vnější
silami	síla	k1gFnPc7	síla
tento	tento	k3xDgInSc4	tento
stav	stav	k1gInSc4	stav
změnit	změnit	k5eAaPmF	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zákon	zákon	k1gInSc1	zákon
lze	lze	k6eAd1	lze
experimentálně	experimentálně	k6eAd1	experimentálně
dokázat	dokázat	k5eAaPmF	dokázat
jen	jen	k9	jen
při	při	k7c6	při
vyloučení	vyloučení	k1gNnSc6	vyloučení
nebo	nebo	k8xC	nebo
kompenzaci	kompenzace	k1gFnSc4	kompenzace
všech	všecek	k3xTgFnPc2	všecek
vnějších	vnější	k2eAgFnPc2d1	vnější
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
plné	plný	k2eAgFnSc6d1	plná
míře	míra	k1gFnSc6	míra
nemožné	možný	k2eNgNnSc1d1	nemožné
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
to	ten	k3xDgNnSc4	ten
však	však	k9	však
vyřešit	vyřešit	k5eAaPmF	vyřešit
lze	lze	k6eAd1	lze
<g/>
.	.	kIx.	.
</s>
<s>
Odporové	odporový	k2eAgFnPc1d1	odporová
síly	síla	k1gFnPc1	síla
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
působí	působit	k5eAaImIp3nP	působit
v	v	k7c6	v
látkovém	látkový	k2eAgNnSc6d1	látkové
prostředí	prostředí	k1gNnSc6	prostředí
i	i	k8xC	i
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
odstranit	odstranit	k5eAaPmF	odstranit
umístěním	umístění	k1gNnSc7	umístění
tělesa	těleso	k1gNnSc2	těleso
do	do	k7c2	do
vakua	vakuum	k1gNnSc2	vakuum
<g/>
.	.	kIx.	.
</s>
<s>
Gravitační	gravitační	k2eAgFnSc4d1	gravitační
sílu	síla	k1gFnSc4	síla
lze	lze	k6eAd1	lze
kompenzovat	kompenzovat	k5eAaBmF	kompenzovat
odstředivou	odstředivý	k2eAgFnSc7d1	odstředivá
silou	síla	k1gFnSc7	síla
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
kosmické	kosmický	k2eAgFnSc6d1	kosmická
lodi	loď	k1gFnSc6	loď
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
kolem	kolem	k7c2	kolem
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
Newtonův	Newtonův	k2eAgInSc1d1	Newtonův
zákon	zákon	k1gInSc1	zákon
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
síla	síla	k1gFnSc1	síla
není	být	k5eNaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
tělesa	těleso	k1gNnPc1	těleso
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
i	i	k9	i
bez	bez	k7c2	bez
působení	působení	k1gNnSc2	působení
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
tento	tento	k3xDgInSc1	tento
pohyb	pohyb	k1gInSc1	pohyb
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
rovnoměrný	rovnoměrný	k2eAgMnSc1d1	rovnoměrný
a	a	k8xC	a
přímočarý	přímočarý	k2eAgMnSc1d1	přímočarý
(	(	kIx(	(
<g/>
nemění	měnit	k5eNaImIp3nS	měnit
se	se	k3xPyFc4	se
velikost	velikost	k1gFnSc1	velikost
rychlosti	rychlost	k1gFnSc2	rychlost
ani	ani	k8xC	ani
směr	směr	k1gInSc1	směr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Těleso	těleso	k1gNnSc1	těleso
si	se	k3xPyFc3	se
tedy	tedy	k9	tedy
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
svůj	svůj	k3xOyFgInSc4	svůj
pohybový	pohybový	k2eAgInSc4d1	pohybový
stav	stav	k1gInSc4	stav
z	z	k7c2	z
okamžiku	okamžik	k1gInSc2	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
přestala	přestat	k5eAaPmAgFnS	přestat
působit	působit	k5eAaImF	působit
poslední	poslední	k2eAgFnSc1d1	poslední
síla	síla	k1gFnSc1	síla
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
snaha	snaha	k1gFnSc1	snaha
setrvávat	setrvávat	k5eAaImF	setrvávat
v	v	k7c6	v
okamžitém	okamžitý	k2eAgInSc6d1	okamžitý
pohybovém	pohybový	k2eAgInSc6d1	pohybový
stavu	stav	k1gInSc6	stav
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
setrvačností	setrvačnost	k1gFnSc7	setrvačnost
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Setrvačností	setrvačnost	k1gFnPc2	setrvačnost
se	se	k3xPyFc4	se
těleso	těleso	k1gNnSc1	těleso
brání	bránit	k5eAaImIp3nS	bránit
proti	proti	k7c3	proti
změně	změna	k1gFnSc3	změna
svého	svůj	k3xOyFgInSc2	svůj
pohybového	pohybový	k2eAgInSc2d1	pohybový
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
proti	proti	k7c3	proti
zrychlení	zrychlení	k1gNnSc3	zrychlení
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
v	v	k7c6	v
obrácené	obrácený	k2eAgFnSc6d1	obrácená
verzi	verze	k1gFnSc6	verze
<g/>
:	:	kIx,	:
Jestliže	jestliže	k8xS	jestliže
je	být	k5eAaImIp3nS	být
těleso	těleso	k1gNnSc1	těleso
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
přímočaře	přímočaro	k6eAd1	přímočaro
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
nepůsobí	působit	k5eNaImIp3nS	působit
žádná	žádný	k3yNgFnSc1	žádný
síla	síla	k1gFnSc1	síla
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
výslednice	výslednice	k1gFnSc1	výslednice
působících	působící	k2eAgFnPc2d1	působící
sil	síla	k1gFnPc2	síla
nulová	nulový	k2eAgFnSc1d1	nulová
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
užitečné	užitečný	k2eAgNnSc1d1	užitečné
při	při	k7c6	při
určování	určování	k1gNnSc6	určování
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
působí	působit	k5eAaImIp3nP	působit
na	na	k7c4	na
těleso	těleso	k1gNnSc4	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
také	také	k9	také
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
zákon	zákon	k1gInSc1	zákon
mluví	mluvit	k5eAaImIp3nS	mluvit
pouze	pouze	k6eAd1	pouze
o	o	k7c6	o
vnějších	vnější	k2eAgFnPc6d1	vnější
silách	síla	k1gFnPc6	síla
<g/>
.	.	kIx.	.
</s>
<s>
Síly	síl	k1gInPc1	síl
působící	působící	k2eAgInPc1d1	působící
mezi	mezi	k7c7	mezi
částmi	část	k1gFnPc7	část
tělesa	těleso	k1gNnSc2	těleso
(	(	kIx(	(
<g/>
vnitřní	vnitřní	k2eAgFnPc1d1	vnitřní
síly	síla	k1gFnPc1	síla
<g/>
)	)	kIx)	)
nemají	mít	k5eNaImIp3nP	mít
žádný	žádný	k3yNgInSc4	žádný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
celkový	celkový	k2eAgInSc4d1	celkový
pohyb	pohyb	k1gInSc4	pohyb
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
řečeno	říct	k5eAaPmNgNnS	říct
na	na	k7c4	na
pohyb	pohyb	k1gInSc4	pohyb
jeho	jeho	k3xOp3gNnSc2	jeho
těžiště	těžiště	k1gNnSc2	těžiště
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
pokud	pokud	k8xS	pokud
se	s	k7c7	s
prostorem	prostor	k1gInSc7	prostor
volně	volně	k6eAd1	volně
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
vnějších	vnější	k2eAgFnPc2d1	vnější
sil	síla	k1gFnPc2	síla
<g/>
)	)	kIx)	)
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
bomba	bomba	k1gFnSc1	bomba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
okamžiku	okamžik	k1gInSc6	okamžik
rozletí	rozletět	k5eAaPmIp3nS	rozletět
na	na	k7c4	na
kusy	kus	k1gInPc4	kus
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
společné	společný	k2eAgNnSc4d1	společné
těžiště	těžiště	k1gNnSc4	těžiště
všech	všecek	k3xTgInPc2	všecek
těchto	tento	k3xDgInPc2	tento
kusů	kus	k1gInPc2	kus
bude	být	k5eAaImBp3nS	být
nadále	nadále	k6eAd1	nadále
vykonávat	vykonávat	k5eAaImF	vykonávat
rovnoměrný	rovnoměrný	k2eAgInSc4d1	rovnoměrný
přímočarý	přímočarý	k2eAgInSc4d1	přímočarý
pohyb	pohyb	k1gInSc4	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
důsledek	důsledek	k1gInSc1	důsledek
zákona	zákon	k1gInSc2	zákon
zachování	zachování	k1gNnSc2	zachování
hybnosti	hybnost	k1gFnSc2	hybnost
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
pohybový	pohybový	k2eAgInSc1d1	pohybový
zákon	zákon	k1gInSc1	zákon
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bez	bez	k7c2	bez
vnějšího	vnější	k2eAgNnSc2d1	vnější
působení	působení	k1gNnSc2	působení
si	se	k3xPyFc3	se
těleso	těleso	k1gNnSc1	těleso
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
svou	svůj	k3xOyFgFnSc4	svůj
hybnost	hybnost	k1gFnSc4	hybnost
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
Newtonův	Newtonův	k2eAgInSc1d1	Newtonův
zákon	zákon	k1gInSc1	zákon
platí	platit	k5eAaImIp3nS	platit
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
inerciálních	inerciální	k2eAgFnPc6d1	inerciální
soustavách	soustava	k1gFnPc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
také	také	k9	také
Zákon	zákon	k1gInSc1	zákon
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Mutationem	Mutation	k1gInSc7	Mutation
motus	motus	k1gMnSc1	motus
proportionalem	proportional	k1gInSc7	proportional
esse	esse	k6eAd1	esse
vi	vi	k?	vi
motrici	motrice	k1gFnSc4	motrice
impressae	impressaat	k5eAaPmIp3nS	impressaat
et	et	k?	et
fieri	fieri	k6eAd1	fieri
secundam	secundam	k6eAd1	secundam
lineam	lineam	k6eAd1	lineam
rectam	rectam	k6eAd1	rectam
qua	qua	k?	qua
vis	vis	k1gInSc1	vis
illa	illa	k1gMnSc1	illa
imprimitur	imprimitura	k1gFnPc2	imprimitura
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
na	na	k7c4	na
těleso	těleso	k1gNnSc4	těleso
působí	působit	k5eAaImIp3nP	působit
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
těleso	těleso	k1gNnSc1	těleso
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	s	k7c7	s
zrychlením	zrychlení	k1gNnSc7	zrychlení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
úměrné	úměrný	k2eAgFnSc3d1	úměrná
působící	působící	k2eAgFnSc3d1	působící
síle	síla	k1gFnSc3	síla
a	a	k8xC	a
nepřímo	přímo	k6eNd1	přímo
úměrné	úměrný	k2eAgFnSc6d1	úměrná
hmotnosti	hmotnost	k1gFnSc6	hmotnost
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Obecněji	obecně	k6eAd2	obecně
bývá	bývat	k5eAaImIp3nS	bývat
zákon	zákon	k1gInSc1	zákon
síly	síla	k1gFnSc2	síla
vyjadřován	vyjadřovat	k5eAaImNgInS	vyjadřovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
síla	síla	k1gFnSc1	síla
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}	}	kIx)	}
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
rovna	roven	k2eAgFnSc1d1	rovna
časové	časový	k2eAgFnSc3d1	časová
změně	změna	k1gFnSc3	změna
hybnosti	hybnost	k1gFnSc2	hybnost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
}	}	kIx)	}
,	,	kIx,	,
což	což	k3yQnSc1	což
lze	lze	k6eAd1	lze
matematicky	matematicky	k6eAd1	matematicky
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k8xS	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
(	(	kIx(	(
m	m	kA	m
:	:	kIx,	:
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s>
t	t	k?	t
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
m	m	kA	m
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Původní	původní	k2eAgFnPc1d1	původní
atomistické	atomistický	k2eAgFnPc1d1	atomistická
představy	představa	k1gFnPc1	představa
předpokládaly	předpokládat	k5eAaImAgFnP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejmenší	malý	k2eAgFnPc1d3	nejmenší
částice	částice	k1gFnPc1	částice
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
tělesa	těleso	k1gNnPc1	těleso
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
dále	daleko	k6eAd2	daleko
nedělitelné	dělitelný	k2eNgFnPc1d1	nedělitelná
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
vlastnosti	vlastnost	k1gFnPc1	vlastnost
se	se	k3xPyFc4	se
nemění	měnit	k5eNaImIp3nP	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Nemění	měnit	k5eNaImIp3nS	měnit
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
také	také	k9	také
hmotnost	hmotnost	k1gFnSc4	hmotnost
těchto	tento	k3xDgFnPc2	tento
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
počet	počet	k1gInSc1	počet
takových	takový	k3xDgFnPc2	takový
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
těleso	těleso	k1gNnSc1	těleso
složeno	složen	k2eAgNnSc1d1	složeno
<g/>
,	,	kIx,	,
nemění	měnit	k5eNaImIp3nS	měnit
během	během	k7c2	během
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
nemění	měnit	k5eNaImIp3nS	měnit
také	také	k9	také
celková	celkový	k2eAgFnSc1d1	celková
hmotnost	hmotnost	k1gFnSc1	hmotnost
pohybujícího	pohybující	k2eAgMnSc2d1	pohybující
se	se	k3xPyFc4	se
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
předpoklad	předpoklad	k1gInSc1	předpoklad
lze	lze	k6eAd1	lze
při	při	k7c6	při
makroskopických	makroskopický	k2eAgInPc6d1	makroskopický
pohybech	pohyb	k1gInPc6	pohyb
obvykle	obvykle	k6eAd1	obvykle
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
platný	platný	k2eAgInSc4d1	platný
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
nám	my	k3xPp1nPc3	my
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přejít	přejít	k5eAaPmF	přejít
k	k	k7c3	k
původní	původní	k2eAgFnSc3d1	původní
formulaci	formulace	k1gFnSc3	formulace
zákona	zákon	k1gInSc2	zákon
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
lze	lze	k6eAd1	lze
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
(	(	kIx(	(
<g/>
v	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
mechanice	mechanika	k1gFnSc6	mechanika
<g/>
)	)	kIx)	)
často	často	k6eAd1	často
používaným	používaný	k2eAgInSc7d1	používaný
vztahem	vztah	k1gInSc7	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
=	=	kIx~	=
m	m	kA	m
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
m	m	kA	m
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,,	,,	k?	,,
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k6eAd1	kde
F	F	kA	F
je	být	k5eAaImIp3nS	být
vektor	vektor	k1gInSc4	vektor
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
m	m	kA	m
je	být	k5eAaImIp3nS	být
hmotnost	hmotnost	k1gFnSc4	hmotnost
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vektor	vektor	k1gInSc4	vektor
zrychlení	zrychlení	k1gNnSc2	zrychlení
<g/>
.	.	kIx.	.
</s>
<s>
Vektory	vektor	k1gInPc1	vektor
síly	síla	k1gFnSc2	síla
a	a	k8xC	a
zrychlení	zrychlení	k1gNnSc1	zrychlení
mají	mít	k5eAaImIp3nP	mít
podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
rovnice	rovnice	k1gFnSc2	rovnice
stejný	stejný	k2eAgInSc4d1	stejný
směr	směr	k1gInSc4	směr
<g/>
.	.	kIx.	.
</s>
<s>
Zrychlení	zrychlení	k1gNnSc1	zrychlení
tělesa	těleso	k1gNnSc2	těleso
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
rychlost	rychlost	k1gFnSc1	rychlost
jeho	on	k3xPp3gInSc2	on
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
mění	měnit	k5eAaImIp3nS	měnit
se	se	k3xPyFc4	se
pohybový	pohybový	k2eAgInSc1d1	pohybový
stav	stav	k1gInSc1	stav
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
Newtonův	Newtonův	k2eAgInSc1d1	Newtonův
pohybový	pohybový	k2eAgInSc1d1	pohybový
zákon	zákon	k1gInSc1	zákon
tedy	tedy	k9	tedy
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
síla	síla	k1gFnSc1	síla
je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
změny	změna	k1gFnSc2	změna
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
pohybu	pohyb	k1gInSc2	pohyb
jako	jako	k8xS	jako
takového	takový	k3xDgNnSc2	takový
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
prvního	první	k4xOgInSc2	první
pohybového	pohybový	k2eAgInSc2d1	pohybový
zákona	zákon	k1gInSc2	zákon
se	se	k3xPyFc4	se
tělesa	těleso	k1gNnPc1	těleso
<g/>
,	,	kIx,	,
na	na	k7c6	na
která	který	k3yQgFnSc1	který
působí	působit	k5eAaImIp3nS	působit
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
nebudou	být	k5eNaImBp3nP	být
pohybovat	pohybovat	k5eAaImF	pohybovat
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
přímočaře	přímočaro	k6eAd1	přímočaro
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gInSc4	jejich
pohyb	pohyb	k1gInSc4	pohyb
bude	být	k5eAaImBp3nS	být
zrychlený	zrychlený	k2eAgMnSc1d1	zrychlený
<g/>
,	,	kIx,	,
zpomalený	zpomalený	k2eAgMnSc1d1	zpomalený
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
měnit	měnit	k5eAaImF	měnit
směr	směr	k1gInSc4	směr
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
kombinace	kombinace	k1gFnSc1	kombinace
těchto	tento	k3xDgFnPc2	tento
možností	možnost	k1gFnPc2	možnost
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
pohybu	pohyb	k1gInSc2	pohyb
(	(	kIx(	(
<g/>
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
)	)	kIx)	)
závisí	záviset	k5eAaImIp3nS	záviset
také	také	k9	také
na	na	k7c6	na
směru	směr	k1gInSc6	směr
působící	působící	k2eAgFnSc2d1	působící
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Síla	síla	k1gFnSc1	síla
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
pohybu	pohyb	k1gInSc2	pohyb
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
zrychlení	zrychlení	k1gNnSc1	zrychlení
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
síla	síla	k1gFnSc1	síla
proti	proti	k7c3	proti
směru	směr	k1gInSc3	směr
pohybu	pohyb	k1gInSc2	pohyb
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
zpomalení	zpomalení	k1gNnSc1	zpomalení
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Síla	síla	k1gFnSc1	síla
kolmá	kolmý	k2eAgFnSc1d1	kolmá
na	na	k7c4	na
pohyb	pohyb	k1gInSc4	pohyb
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
změnu	změna	k1gFnSc4	změna
směru	směr	k1gInSc2	směr
pohybu	pohyb	k1gInSc2	pohyb
tělesa	těleso	k1gNnSc2	těleso
(	(	kIx(	(
<g/>
zakřivení	zakřivení	k1gNnSc1	zakřivení
trajektorie	trajektorie	k1gFnSc2	trajektorie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
si	se	k3xPyFc3	se
uvědomíme	uvědomit	k5eAaPmIp1nP	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zrychlení	zrychlení	k1gNnSc1	zrychlení
je	být	k5eAaImIp3nS	být
derivace	derivace	k1gFnSc1	derivace
rychlosti	rychlost	k1gFnSc2	rychlost
neboli	neboli	k8xC	neboli
druhá	druhý	k4xOgFnSc1	druhý
derivace	derivace	k1gFnSc1	derivace
polohy	poloha	k1gFnSc2	poloha
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
zákon	zákon	k1gInSc1	zákon
síly	síla	k1gFnSc2	síla
použít	použít	k5eAaPmF	použít
k	k	k7c3	k
sestavení	sestavení	k1gNnSc3	sestavení
pohybové	pohybový	k2eAgFnSc2d1	pohybová
rovnice	rovnice	k1gFnSc2	rovnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
=	=	kIx~	=
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
m	m	kA	m
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}}	}}	k?	}}
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
,,	,,	k?	,,
<g/>
}	}	kIx)	}
:	:	kIx,	:
která	který	k3yIgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
řešit	řešit	k5eAaImF	řešit
konkrétní	konkrétní	k2eAgInPc4d1	konkrétní
pohybové	pohybový	k2eAgInPc4d1	pohybový
děje	děj	k1gInPc4	děj
(	(	kIx(	(
<g/>
určovat	určovat	k5eAaImF	určovat
polohu	poloha	k1gFnSc4	poloha
a	a	k8xC	a
rychlost	rychlost	k1gFnSc4	rychlost
těles	těleso	k1gNnPc2	těleso
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
čase	čas	k1gInSc6	čas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
známy	znám	k2eAgFnPc4d1	známa
konkrétní	konkrétní	k2eAgFnPc4d1	konkrétní
síly	síla	k1gFnPc4	síla
působící	působící	k2eAgFnPc4d1	působící
při	při	k7c6	při
dějích	děj	k1gInPc6	děj
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc6	hmotnost
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
sílu	síla	k1gFnSc4	síla
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
na	na	k7c4	na
levou	levý	k2eAgFnSc4d1	levá
stranu	strana	k1gFnSc4	strana
dosadí	dosadit	k5eAaPmIp3nS	dosadit
funkce	funkce	k1gFnSc1	funkce
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
polohy	poloha	k1gFnSc2	poloha
nebo	nebo	k8xC	nebo
i	i	k9	i
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Například	například	k6eAd1	například
odpor	odpor	k1gInSc1	odpor
vzduchu	vzduch	k1gInSc2	vzduch
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
rychlosti	rychlost	k1gFnSc6	rychlost
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Dostaneme	dostat	k5eAaPmIp1nP	dostat
tak	tak	k6eAd1	tak
diferenciální	diferenciální	k2eAgFnSc4d1	diferenciální
rovnici	rovnice	k1gFnSc4	rovnice
druhého	druhý	k4xOgInSc2	druhý
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gNnSc7	jejíž
řešením	řešení	k1gNnSc7	řešení
je	být	k5eAaImIp3nS	být
vektorová	vektorový	k2eAgFnSc1d1	vektorová
funkce	funkce	k1gFnSc1	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
(	(	kIx(	(
t	t	k?	t
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
polohu	poloha	k1gFnSc4	poloha
hmotného	hmotný	k2eAgInSc2d1	hmotný
bodu	bod	k1gInSc2	bod
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
rovnice	rovnice	k1gFnSc1	rovnice
ale	ale	k9	ale
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hmotnost	hmotnost	k1gFnSc1	hmotnost
tělesa	těleso	k1gNnSc2	těleso
se	se	k3xPyFc4	se
v	v	k7c6	v
čase	čas	k1gInSc6	čas
nemění	měnit	k5eNaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
vždy	vždy	k6eAd1	vždy
splněno	splnit	k5eAaPmNgNnS	splnit
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
raketě	raketa	k1gFnSc3	raketa
během	během	k7c2	během
letu	let	k1gInSc2	let
ubývá	ubývat	k5eAaImIp3nS	ubývat
palivo	palivo	k1gNnSc4	palivo
anebo	anebo	k8xC	anebo
se	se	k3xPyFc4	se
hmotnost	hmotnost	k1gFnSc1	hmotnost
mění	měnit	k5eAaImIp3nS	měnit
při	při	k7c6	při
relativistických	relativistický	k2eAgFnPc6d1	relativistická
rychlostech	rychlost	k1gFnPc6	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgInSc6	ten
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
užít	užít	k5eAaPmF	užít
obecnější	obecní	k2eAgInSc4d2	obecní
tvar	tvar	k1gInSc4	tvar
pohybové	pohybový	k2eAgFnSc2d1	pohybová
rovnice	rovnice	k1gFnSc2	rovnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
t	t	k?	t
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
/	/	kIx~	/
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
kde	kde	k6eAd1	kde
p	p	k?	p
je	být	k5eAaImIp3nS	být
hybnost	hybnost	k1gFnSc4	hybnost
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
velikost	velikost	k1gFnSc1	velikost
výslednice	výslednice	k1gFnSc2	výslednice
vnějších	vnější	k2eAgFnPc2d1	vnější
sil	síla	k1gFnPc2	síla
F	F	kA	F
a	a	k8xC	a
velikost	velikost	k1gFnSc1	velikost
zrychlení	zrychlení	k1gNnSc2	zrychlení
tělesa	těleso	k1gNnSc2	těleso
a	a	k8xC	a
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
lze	lze	k6eAd1	lze
ze	z	k7c2	z
vztahu	vztah	k1gInSc2	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
m	m	kA	m
=	=	kIx~	=
F	F	kA	F
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
m	m	kA	m
<g/>
=	=	kIx~	=
<g/>
F	F	kA	F
<g/>
/	/	kIx~	/
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
vypočítat	vypočítat	k5eAaPmF	vypočítat
hmotnost	hmotnost	k1gFnSc4	hmotnost
tělesa	těleso	k1gNnSc2	těleso
m.	m.	k?	m.
Tato	tento	k3xDgFnSc1	tento
hmotnost	hmotnost	k1gFnSc1	hmotnost
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
setrvačná	setrvačný	k2eAgFnSc1d1	setrvačná
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
hmotnost	hmotnost	k1gFnSc1	hmotnost
projevující	projevující	k2eAgFnSc1d1	projevující
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
setrvačností	setrvačnost	k1gFnSc7	setrvačnost
-	-	kIx~	-
odporem	odpor	k1gInSc7	odpor
vůči	vůči	k7c3	vůči
změnám	změna	k1gFnPc3	změna
pohybového	pohybový	k2eAgInSc2d1	pohybový
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Newton	Newton	k1gMnSc1	Newton
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
hmotnost	hmotnost	k1gFnSc1	hmotnost
je	být	k5eAaImIp3nS	být
totožná	totožný	k2eAgFnSc1d1	totožná
s	s	k7c7	s
gravitační	gravitační	k2eAgFnSc7d1	gravitační
hmotností	hmotnost	k1gFnSc7	hmotnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
zákoně	zákon	k1gInSc6	zákon
všeobecné	všeobecný	k2eAgFnSc2d1	všeobecná
gravitace	gravitace	k1gFnSc2	gravitace
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
sám	sám	k3xTgMnSc1	sám
formuloval	formulovat	k5eAaImAgMnS	formulovat
<g/>
.	.	kIx.	.
</s>
<s>
Vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
tuto	tento	k3xDgFnSc4	tento
shodu	shoda	k1gFnSc4	shoda
však	však	k9	však
dokázal	dokázat	k5eAaPmAgMnS	dokázat
až	až	k9	až
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
obecné	obecný	k2eAgFnSc6d1	obecná
teorii	teorie	k1gFnSc6	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
Newtonův	Newtonův	k2eAgInSc1d1	Newtonův
zákon	zákon	k1gInSc1	zákon
platí	platit	k5eAaImIp3nS	platit
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
inerciálních	inerciální	k2eAgFnPc6d1	inerciální
soustavách	soustava	k1gFnPc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
také	také	k9	také
Zákon	zákon	k1gInSc1	zákon
akce	akce	k1gFnSc2	akce
a	a	k8xC	a
reakce	reakce	k1gFnSc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Actioni	Action	k1gMnPc1	Action
contrariam	contrariam	k6eAd1	contrariam
semper	semper	k1gInSc1	semper
et	et	k?	et
aequalem	aequal	k1gInSc7	aequal
esse	esse	k1gNnSc2	esse
reactionem	reaction	k1gInSc7	reaction
<g/>
;	;	kIx,	;
sive	sive	k1gInSc1	sive
<g/>
:	:	kIx,	:
corporum	corporum	k1gInSc1	corporum
duorum	duorum	k1gNnSc1	duorum
actiones	actiones	k1gInSc1	actiones
in	in	k?	in
se	se	k3xPyFc4	se
mutuo	mutuo	k6eAd1	mutuo
semper	semper	k1gMnSc1	semper
esse	ess	k1gFnSc2	ess
aequales	aequales	k1gMnSc1	aequales
et	et	k?	et
in	in	k?	in
partes	partes	k?	partes
contrarias	contrarias	k1gInSc4	contrarias
dirigi	dirig	k1gFnSc2	dirig
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
každé	každý	k3xTgFnSc3	každý
akci	akce	k1gFnSc3	akce
vždy	vždy	k6eAd1	vždy
působí	působit	k5eAaImIp3nS	působit
stejná	stejný	k2eAgFnSc1d1	stejná
reakce	reakce	k1gFnSc1	reakce
<g/>
;	;	kIx,	;
jinak	jinak	k6eAd1	jinak
<g/>
:	:	kIx,	:
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
působení	působení	k1gNnSc2	působení
dvou	dva	k4xCgNnPc2	dva
těles	těleso	k1gNnPc2	těleso
jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
stejně	stejně	k6eAd1	stejně
velká	velký	k2eAgFnSc1d1	velká
a	a	k8xC	a
míří	mířit	k5eAaImIp3nS	mířit
na	na	k7c4	na
opačné	opačný	k2eAgFnPc4d1	opačná
strany	strana	k1gFnPc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Ekvivalentní	ekvivalentní	k2eAgFnPc1d1	ekvivalentní
formulace	formulace	k1gFnPc1	formulace
<g/>
:	:	kIx,	:
Jestliže	jestliže	k8xS	jestliže
těleso	těleso	k1gNnSc1	těleso
1	[number]	k4	1
působí	působit	k5eAaImIp3nS	působit
silou	síla	k1gFnSc7	síla
na	na	k7c4	na
těleso	těleso	k1gNnSc4	těleso
2	[number]	k4	2
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
také	také	k9	také
těleso	těleso	k1gNnSc1	těleso
2	[number]	k4	2
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
těleso	těleso	k1gNnSc4	těleso
1	[number]	k4	1
stejně	stejně	k6eAd1	stejně
velkou	velká	k1gFnSc4	velká
opačně	opačně	k6eAd1	opačně
orientovanou	orientovaný	k2eAgFnSc7d1	orientovaná
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Síly	síla	k1gFnPc1	síla
současně	současně	k6eAd1	současně
vznikají	vznikat	k5eAaImIp3nP	vznikat
a	a	k8xC	a
zanikají	zanikat	k5eAaImIp3nP	zanikat
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
Newtonův	Newtonův	k2eAgInSc1d1	Newtonův
zákon	zákon	k1gInSc1	zákon
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
působení	působení	k1gNnSc1	působení
těles	těleso	k1gNnPc2	těleso
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
vzájemné	vzájemný	k2eAgNnSc1d1	vzájemné
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
účinky	účinek	k1gInPc1	účinek
sil	síla	k1gFnPc2	síla
akce	akce	k1gFnPc4	akce
a	a	k8xC	a
reakce	reakce	k1gFnPc4	reakce
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
neruší	rušit	k5eNaImIp3nS	rušit
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
je	být	k5eAaImIp3nS	být
sčítat	sčítat	k5eAaImF	sčítat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
sil	síla	k1gFnPc2	síla
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
jiné	jiný	k2eAgNnSc4d1	jiné
těleso	těleso	k1gNnSc4	těleso
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
o	o	k7c4	o
rovnováhu	rovnováha	k1gFnSc4	rovnováha
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Matematicky	matematicky	k6eAd1	matematicky
lze	lze	k6eAd1	lze
zákon	zákon	k1gInSc1	zákon
akce	akce	k1gFnSc2	akce
a	a	k8xC	a
reakce	reakce	k1gFnSc2	reakce
formulovat	formulovat	k5eAaImF	formulovat
vztahem	vztah	k1gInSc7	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
:	:	kIx,	:
12	[number]	k4	12
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
F	F	kA	F
:	:	kIx,	:
:	:	kIx,	:
21	[number]	k4	21
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
12	[number]	k4	12
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
21	[number]	k4	21
<g/>
}}	}}	k?	}}
:	:	kIx,	:
kde	kde	k6eAd1	kde
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
:	:	kIx,	:
12	[number]	k4	12
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
12	[number]	k4	12
<g/>
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
působí	působit	k5eAaImIp3nS	působit
těleso	těleso	k1gNnSc1	těleso
1	[number]	k4	1
na	na	k7c4	na
těleso	těleso	k1gNnSc4	těleso
2	[number]	k4	2
<g/>
,	,	kIx,	,
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
:	:	kIx,	:
21	[number]	k4	21
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
21	[number]	k4	21
<g/>
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
odpovídající	odpovídající	k2eAgFnSc1d1	odpovídající
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
působí	působit	k5eAaImIp3nS	působit
těleso	těleso	k1gNnSc1	těleso
2	[number]	k4	2
na	na	k7c4	na
těleso	těleso	k1gNnSc4	těleso
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Síly	síla	k1gFnPc1	síla
tedy	tedy	k9	tedy
nepůsobí	působit	k5eNaImIp3nP	působit
nikdy	nikdy	k6eAd1	nikdy
osamoceně	osamoceně	k6eAd1	osamoceně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vždy	vždy	k6eAd1	vždy
ve	v	k7c6	v
dvojici	dvojice	k1gFnSc6	dvojice
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
nazveme	nazvat	k5eAaBmIp1nP	nazvat
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
:	:	kIx,	:
12	[number]	k4	12
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
12	[number]	k4	12
<g/>
}}	}}	k?	}}
akcí	akce	k1gFnPc2	akce
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
:	:	kIx,	:
21	[number]	k4	21
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
21	[number]	k4	21
<g/>
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
reakcí	reakce	k1gFnSc7	reakce
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Reaktivní	reaktivní	k2eAgFnSc1d1	reaktivní
síla	síla	k1gFnSc1	síla
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
tzv.	tzv.	kA	tzv.
zpětný	zpětný	k2eAgInSc4d1	zpětný
ráz	ráz	k1gInSc4	ráz
při	při	k7c6	při
střelbě	střelba	k1gFnSc6	střelba
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
se	se	k3xPyFc4	se
v	v	k7c6	v
reaktivních	reaktivní	k2eAgInPc6d1	reaktivní
motorech	motor	k1gInPc6	motor
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
tělesa	těleso	k1gNnPc1	těleso
působí	působit	k5eAaImIp3nP	působit
pouze	pouze	k6eAd1	pouze
silami	síla	k1gFnPc7	síla
akce	akce	k1gFnSc2	akce
a	a	k8xC	a
reakce	reakce	k1gFnSc2	reakce
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
úhrnná	úhrnný	k2eAgFnSc1d1	úhrnná
hybnost	hybnost	k1gFnSc1	hybnost
je	být	k5eAaImIp3nS	být
konstantní	konstantní	k2eAgFnSc1d1	konstantní
<g/>
.	.	kIx.	.
</s>
<s>
Izolovaná	izolovaný	k2eAgFnSc1d1	izolovaná
soustava	soustava	k1gFnSc1	soustava
těles	těleso	k1gNnPc2	těleso
je	být	k5eAaImIp3nS	být
taková	takový	k3xDgFnSc1	takový
soustava	soustava	k1gFnSc1	soustava
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
tělesa	těleso	k1gNnPc1	těleso
působí	působit	k5eAaImIp3nP	působit
vzájemně	vzájemně	k6eAd1	vzájemně
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
a	a	k8xC	a
žádné	žádný	k3yNgNnSc4	žádný
jiné	jiný	k2eAgNnSc4d1	jiné
těleso	těleso	k1gNnSc4	těleso
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
nepůsobí	působit	k5eNaImIp3nS	působit
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
Newtonův	Newtonův	k2eAgInSc1d1	Newtonův
zákon	zákon	k1gInSc1	zákon
(	(	kIx(	(
<g/>
Lex	Lex	k1gFnSc1	Lex
quarta	quarta	k1gFnSc1	quarta
<g/>
)	)	kIx)	)
bývá	bývat	k5eAaImIp3nS	bývat
někdy	někdy	k6eAd1	někdy
označován	označován	k2eAgInSc1d1	označován
princip	princip	k1gInSc1	princip
nezávislého	závislý	k2eNgNnSc2d1	nezávislé
skládání	skládání	k1gNnSc2	skládání
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
princip	princip	k1gInSc1	princip
superpozice	superpozice	k1gFnSc1	superpozice
<g/>
.	.	kIx.	.
</s>
<s>
Newton	Newton	k1gMnSc1	Newton
ho	on	k3xPp3gMnSc4	on
formuloval	formulovat	k5eAaImAgMnS	formulovat
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
jako	jako	k8xC	jako
nezávislý	závislý	k2eNgInSc1d1	nezávislý
doplněk	doplněk	k1gInSc1	doplněk
předchozích	předchozí	k2eAgInPc2d1	předchozí
tří	tři	k4xCgInPc2	tři
pohybových	pohybový	k2eAgInPc2d1	pohybový
zákonů	zákon	k1gInPc2	zákon
<g/>
:	:	kIx,	:
Jestliže	jestliže	k8xS	jestliže
na	na	k7c4	na
těleso	těleso	k1gNnSc4	těleso
působí	působit	k5eAaImIp3nS	působit
současně	současně	k6eAd1	současně
více	hodně	k6eAd2	hodně
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
rovnají	rovnat	k5eAaImIp3nP	rovnat
se	se	k3xPyFc4	se
silové	silový	k2eAgInPc1d1	silový
účinky	účinek	k1gInPc1	účinek
působení	působení	k1gNnSc2	působení
jediné	jediný	k2eAgFnSc2d1	jediná
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
výslednice	výslednice	k1gFnSc1	výslednice
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
rovna	roven	k2eAgFnSc1d1	rovna
vektorovému	vektorový	k2eAgNnSc3d1	vektorové
součtu	součet	k1gInSc6	součet
těchto	tento	k3xDgFnPc2	tento
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Matematicky	matematicky	k6eAd1	matematicky
lze	lze	k6eAd1	lze
zákon	zákon	k1gInSc1	zákon
akce	akce	k1gFnSc2	akce
a	a	k8xC	a
reakce	reakce	k1gFnSc2	reakce
formulovat	formulovat	k5eAaImF	formulovat
vztahem	vztah	k1gInSc7	vztah
pro	pro	k7c4	pro
výslednici	výslednice	k1gFnSc4	výslednice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}	}	kIx)	}
}	}	kIx)	}
působících	působící	k2eAgFnPc2d1	působící
sil	síla	k1gFnPc2	síla
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
...	...	k?	...
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
i	i	k9	i
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g />
.	.	kIx.	.
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc4	ldots
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
předchozích	předchozí	k2eAgInPc2d1	předchozí
zákonů	zákon	k1gInPc2	zákon
i	i	k9	i
zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
chápat	chápat	k5eAaImF	chápat
těleso	těleso	k1gNnSc4	těleso
jako	jako	k8xC	jako
bodové	bodový	k2eAgNnSc4d1	bodové
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nebylo	být	k5eNaImAgNnS	být
nutno	nutno	k6eAd1	nutno
uvažovat	uvažovat	k5eAaImF	uvažovat
rotační	rotační	k2eAgInPc4d1	rotační
účinky	účinek	k1gInPc4	účinek
sil	síla	k1gFnPc2	síla
nepůsobících	působící	k2eNgFnPc2d1	nepůsobící
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
přímce	přímka	k1gFnSc6	přímka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
principu	princip	k1gInSc2	princip
superpozice	superpozice	k1gFnSc2	superpozice
plyne	plynout	k5eAaImIp3nS	plynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzájemné	vzájemný	k2eAgInPc1d1	vzájemný
silové	silový	k2eAgInPc1d1	silový
působení	působení	k1gNnSc2	působení
dvou	dva	k4xCgNnPc2	dva
těles	těleso	k1gNnPc2	těleso
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
se	se	k3xPyFc4	se
nezmění	změnit	k5eNaPmIp3nS	změnit
<g/>
,	,	kIx,	,
budou	být	k5eAaImBp3nP	být
<g/>
-li	i	k?	-li
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
působit	působit	k5eAaImF	působit
i	i	k8xC	i
jiná	jiný	k2eAgNnPc1d1	jiné
tělesa	těleso	k1gNnPc1	těleso
(	(	kIx(	(
<g/>
rozumí	rozumět	k5eAaImIp3nP	rozumět
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
tímto	tento	k3xDgNnSc7	tento
působením	působení	k1gNnSc7	působení
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
případnou	případný	k2eAgFnSc7d1	případná
změnou	změna	k1gFnSc7	změna
uspořádání	uspořádání	k1gNnSc2	uspořádání
způsobenou	způsobený	k2eAgFnSc7d1	způsobená
dodatečnými	dodatečný	k2eAgFnPc7d1	dodatečná
silami	síla	k1gFnPc7	síla
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
princip	princip	k1gInSc1	princip
superpozice	superpozice	k1gFnSc2	superpozice
dává	dávat	k5eAaImIp3nS	dávat
přesný	přesný	k2eAgInSc4d1	přesný
smysl	smysl	k1gInSc4	smysl
pojmu	pojem	k1gInSc2	pojem
výslednice	výslednice	k1gFnSc2	výslednice
sil	síla	k1gFnPc2	síla
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
odvodit	odvodit	k5eAaPmF	odvodit
první	první	k4xOgFnSc4	první
a	a	k8xC	a
druhou	druhý	k4xOgFnSc4	druhý
impulsovou	impulsový	k2eAgFnSc4d1	impulsová
větu	věta	k1gFnSc4	věta
pro	pro	k7c4	pro
soustavu	soustava	k1gFnSc4	soustava
hmotných	hmotný	k2eAgInPc2d1	hmotný
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
Newtonův	Newtonův	k2eAgInSc1d1	Newtonův
zákon	zákon	k1gInSc1	zákon
společně	společně	k6eAd1	společně
s	s	k7c7	s
principem	princip	k1gInSc7	princip
superpozice	superpozice	k1gFnSc2	superpozice
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
i	i	k9	i
tzv.	tzv.	kA	tzv.
zákon	zákon	k1gInSc1	zákon
skládání	skládání	k1gNnSc2	skládání
pohybů	pohyb	k1gInPc2	pohyb
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yRgMnSc4	který
výsledný	výsledný	k2eAgInSc1d1	výsledný
pohyb	pohyb	k1gInSc1	pohyb
tělesa	těleso	k1gNnSc2	těleso
vykonávaný	vykonávaný	k2eAgInSc1d1	vykonávaný
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
výslednice	výslednice	k1gFnSc2	výslednice
sil	síla	k1gFnPc2	síla
je	být	k5eAaImIp3nS	být
součtem	součet	k1gInSc7	součet
pohybů	pohyb	k1gInPc2	pohyb
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
by	by	kYmCp3nS	by
vykonalo	vykonat	k5eAaPmAgNnS	vykonat
působením	působení	k1gNnSc7	působení
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
sil	síla	k1gFnPc2	síla
dílčích	dílčí	k2eAgMnPc2d1	dílčí
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c6	na
pořadí	pořadí	k1gNnSc6	pořadí
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
vektorově	vektorově	k6eAd1	vektorově
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
dílčí	dílčí	k2eAgFnPc4d1	dílčí
změny	změna	k1gFnPc4	změna
hybnosti	hybnost	k1gFnSc2	hybnost
čili	čili	k8xC	čili
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
tělesa	těleso	k1gNnPc4	těleso
s	s	k7c7	s
neproměnnou	proměnný	k2eNgFnSc7d1	neproměnná
hmotností	hmotnost	k1gFnSc7	hmotnost
<g/>
,	,	kIx,	,
vektorově	vektorově	k6eAd1	vektorově
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
i	i	k9	i
dílčí	dílčí	k2eAgNnSc1d1	dílčí
zrychlení	zrychlení	k1gNnSc1	zrychlení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pohybová	pohybový	k2eAgFnSc1d1	pohybová
rovnice	rovnice	k1gFnSc1	rovnice
Síla	síla	k1gFnSc1	síla
Hybnost	hybnost	k1gFnSc1	hybnost
Hmotnost	hmotnost	k1gFnSc1	hmotnost
Mechanika	mechanika	k1gFnSc1	mechanika
</s>
