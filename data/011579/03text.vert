<p>
<s>
Poeticon	Poeticon	k1gMnSc1	Poeticon
astronomicon	astronomicon	k1gMnSc1	astronomicon
je	být	k5eAaImIp3nS	být
hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
atlas	atlas	k1gInSc4	atlas
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
autorství	autorství	k1gNnSc1	autorství
se	se	k3xPyFc4	se
připisuje	připisovat	k5eAaImIp3nS	připisovat
Hyginovi	Hygin	k1gMnSc3	Hygin
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
skutečný	skutečný	k2eAgMnSc1d1	skutečný
autor	autor	k1gMnSc1	autor
není	být	k5eNaImIp3nS	být
jistý	jistý	k2eAgMnSc1d1	jistý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Poeticon	Poeticon	k1gMnSc1	Poeticon
astronomicon	astronomicon	k1gMnSc1	astronomicon
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Condos	Condos	k1gInSc1	Condos
<g/>
,	,	kIx,	,
Theony	Theon	k1gInPc1	Theon
<g/>
,	,	kIx,	,
Star	Star	kA	Star
Myths	Myths	k1gInSc1	Myths
of	of	k?	of
the	the	k?	the
Greeks	Greeks	k1gInSc1	Greeks
and	and	k?	and
Romans	romans	k1gInSc1	romans
<g/>
:	:	kIx,	:
A	a	k9	a
Sourcebook	Sourcebook	k1gInSc1	Sourcebook
<g/>
,	,	kIx,	,
Containing	Containing	k1gInSc1	Containing
The	The	k1gFnSc2	The
Constellations	Constellationsa	k1gFnPc2	Constellationsa
of	of	k?	of
Pseudo-Eratosthenes	Pseudo-Eratosthenes	k1gMnSc1	Pseudo-Eratosthenes
and	and	k?	and
the	the	k?	the
Poetic	Poetice	k1gFnPc2	Poetice
Astronomy	astronom	k1gMnPc4	astronom
of	of	k?	of
Hyginus	Hyginus	k1gMnSc1	Hyginus
(	(	kIx(	(
<g/>
Grand	grand	k1gMnSc1	grand
Rapids	Rapidsa	k1gFnPc2	Rapidsa
[	[	kIx(	[
<g/>
MI	já	k3xPp1nSc3	já
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Phanes	Phanes	k1gInSc1	Phanes
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
ISBN	ISBN	kA	ISBN
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
890482	[number]	k4	890482
<g/>
-	-	kIx~	-
<g/>
92	[number]	k4	92
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hard	Hard	k1gMnSc1	Hard
<g/>
,	,	kIx,	,
Robin	robin	k2eAgMnSc1d1	robin
(	(	kIx(	(
<g/>
transl	transl	k1gInSc1	transl
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Eratosthenes	Eratosthenes	k1gMnSc1	Eratosthenes
and	and	k?	and
Hyginus	Hyginus	k1gMnSc1	Hyginus
<g/>
:	:	kIx,	:
Constellation	Constellation	k1gInSc1	Constellation
Myths	Myths	k1gInSc1	Myths
<g/>
,	,	kIx,	,
with	with	k1gMnSc1	with
Aratus	Aratus	k1gMnSc1	Aratus
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Phaenomena	Phaenomen	k2eAgFnSc1d1	Phaenomena
(	(	kIx(	(
<g/>
Oxford	Oxford	k1gInSc1	Oxford
<g/>
:	:	kIx,	:
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
-	-	kIx~	-
<g/>
871698	[number]	k4	871698
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Poeticon	Poeticon	k1gMnSc1	Poeticon
astronomicon	astronomicon	k1gMnSc1	astronomicon
digitalizované	digitalizovaný	k2eAgNnSc4d1	digitalizované
vydání	vydání	k1gNnSc4	vydání
z	z	k7c2	z
1510	[number]	k4	1510
</s>
</p>
