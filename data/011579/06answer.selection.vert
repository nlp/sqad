<s>
Poeticon	Poeticon	k1gMnSc1	Poeticon
astronomicon	astronomicon	k1gMnSc1	astronomicon
je	být	k5eAaImIp3nS	být
hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
atlas	atlas	k1gInSc4	atlas
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
autorství	autorství	k1gNnSc1	autorství
se	se	k3xPyFc4	se
připisuje	připisovat	k5eAaImIp3nS	připisovat
Hyginovi	Hygin	k1gMnSc3	Hygin
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
skutečný	skutečný	k2eAgMnSc1d1	skutečný
autor	autor	k1gMnSc1	autor
není	být	k5eNaImIp3nS	být
jistý	jistý	k2eAgMnSc1d1	jistý
<g/>
.	.	kIx.	.
</s>
