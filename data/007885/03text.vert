<s>
Makedonie	Makedonie	k1gFnSc1	Makedonie
(	(	kIx(	(
<g/>
makedonsky	makedonsky	k6eAd1	makedonsky
М	М	k?	М
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Makedonská	makedonský	k2eAgFnSc1d1	makedonská
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
makedonsky	makedonsky	k6eAd1	makedonsky
<g/>
:	:	kIx,	:
Р	Р	k?	Р
М	М	k?	М
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vnitrozemský	vnitrozemský	k2eAgInSc1d1	vnitrozemský
stát	stát	k1gInSc1	stát
na	na	k7c6	na
Balkánském	balkánský	k2eAgInSc6d1	balkánský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
jako	jako	k8xC	jako
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nástupnických	nástupnický	k2eAgInPc2d1	nástupnický
států	stát	k1gInPc2	stát
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
.	.	kIx.	.
</s>
<s>
Makedonská	makedonský	k2eAgFnSc1d1	makedonská
republika	republika	k1gFnSc1	republika
hraničí	hraničit	k5eAaImIp3nS	hraničit
s	s	k7c7	s
Kosovem	Kosov	k1gInSc7	Kosov
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
<g/>
,	,	kIx,	,
Srbskem	Srbsko	k1gNnSc7	Srbsko
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
Bulharskem	Bulharsko	k1gNnSc7	Bulharsko
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
Řeckem	Řecko	k1gNnSc7	Řecko
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
s	s	k7c7	s
Albánií	Albánie	k1gFnSc7	Albánie
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
Skopje	Skopje	k1gFnSc1	Skopje
s	s	k7c7	s
507	[number]	k4	507
000	[number]	k4	000
obyvateli	obyvatel	k1gMnPc7	obyvatel
podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgNnPc4d1	ostatní
významná	významný	k2eAgNnPc4d1	významné
města	město	k1gNnPc4	město
jsou	být	k5eAaImIp3nP	být
Bitola	Bitola	k1gFnSc1	Bitola
<g/>
,	,	kIx,	,
Kumanovo	Kumanův	k2eAgNnSc1d1	Kumanovo
<g/>
,	,	kIx,	,
Prilep	Prilep	k1gInSc1	Prilep
<g/>
,	,	kIx,	,
Tetovo	Tetovo	k1gNnSc1	Tetovo
<g/>
,	,	kIx,	,
Ohrid	Ohrid	k1gInSc1	Ohrid
<g/>
,	,	kIx,	,
Veles	Veles	k1gMnSc1	Veles
<g/>
,	,	kIx,	,
Štip	štípit	k5eAaImRp2nS	štípit
<g/>
,	,	kIx,	,
Kočani	Kočaň	k1gFnSc6	Kočaň
a	a	k8xC	a
Gostivar	Gostivar	k1gInSc4	Gostivar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
leží	ležet	k5eAaImIp3nS	ležet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
jezer	jezero	k1gNnPc2	jezero
a	a	k8xC	a
šestnáct	šestnáct	k4xCc4	šestnáct
hor	hora	k1gFnPc2	hora
vyšších	vysoký	k2eAgFnPc2d2	vyšší
než	než	k8xS	než
2000	[number]	k4	2000
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Makedonie	Makedonie	k1gFnSc1	Makedonie
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
Středoevropské	středoevropský	k2eAgFnSc2d1	středoevropská
zóny	zóna	k1gFnSc2	zóna
volného	volný	k2eAgInSc2d1	volný
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2005	[number]	k4	2005
je	být	k5eAaImIp3nS	být
také	také	k9	také
kandidátem	kandidát	k1gMnSc7	kandidát
na	na	k7c4	na
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
také	také	k9	také
požádala	požádat	k5eAaPmAgFnS	požádat
o	o	k7c4	o
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
země	zem	k1gFnSc2	zem
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
Μ	Μ	k?	Μ
(	(	kIx(	(
<g/>
Makedonia	Makedonium	k1gNnSc2	Makedonium
<g/>
)	)	kIx)	)
<g/>
,,	,,	k?	,,
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
region	region	k1gInSc1	region
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pojmenovaný	pojmenovaný	k2eAgMnSc1d1	pojmenovaný
po	po	k7c6	po
starověkých	starověký	k2eAgMnPc6d1	starověký
Makedoncích	Makedonec	k1gMnPc6	Makedonec
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
jméno	jméno	k1gNnSc1	jméno
<g/>
,	,	kIx,	,
Μ	Μ	k?	Μ
(	(	kIx(	(
<g/>
Makedónes	Makedónes	k1gMnSc1	Makedónes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
původně	původně	k6eAd1	původně
od	od	k7c2	od
starořeckého	starořecký	k2eAgNnSc2d1	starořecké
přídavného	přídavný	k2eAgNnSc2d1	přídavné
jména	jméno	k1gNnSc2	jméno
makednós	makednósa	k1gFnPc2	makednósa
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
vysoký	vysoký	k2eAgMnSc1d1	vysoký
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
sdílí	sdílet	k5eAaImIp3nS	sdílet
stejný	stejný	k2eAgInSc4d1	stejný
kořen	kořen	k1gInSc4	kořen
jako	jako	k8xC	jako
jméno	jméno	k1gNnSc4	jméno
μ	μ	k?	μ
(	(	kIx(	(
<g/>
Makros	Makrosa	k1gFnPc2	Makrosa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
délka	délka	k1gFnSc1	délka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ve	v	k7c6	v
starořečtině	starořečtina	k1gFnSc6	starořečtina
tak	tak	k9	tak
i	i	k9	i
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
řečtině	řečtina	k1gFnSc6	řečtina
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
věřilo	věřit	k5eAaImAgNnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jméno	jméno	k1gNnSc1	jméno
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
horal	horal	k1gMnSc1	horal
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
ty	ten	k3xDgFnPc1	ten
vysoké	vysoká	k1gFnPc1	vysoká
<g/>
"	"	kIx"	"
možná	možná	k9	možná
díky	díky	k7c3	díky
odkazu	odkaz	k1gInSc3	odkaz
na	na	k7c4	na
fyzický	fyzický	k2eAgInSc4d1	fyzický
charakter	charakter	k1gInSc4	charakter
starých	starý	k2eAgMnPc2d1	starý
Makedonců	Makedonec	k1gMnPc2	Makedonec
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
hornatou	hornatý	k2eAgFnSc4d1	hornatá
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
oficiální	oficiální	k2eAgInSc4d1	oficiální
název	název	k1gInSc4	název
samostatného	samostatný	k2eAgInSc2d1	samostatný
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
Makedonie	Makedonie	k1gFnSc1	Makedonie
a	a	k8xC	a
Makedonská	makedonský	k2eAgFnSc1d1	makedonská
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dlouho	dlouho	k6eAd1	dlouho
vedly	vést	k5eAaImAgInP	vést
spory	spor	k1gInPc1	spor
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
protestů	protest	k1gInPc2	protest
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
země	zem	k1gFnPc1	zem
v	v	k7c6	v
OSN	OSN	kA	OSN
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
mezinárodních	mezinárodní	k2eAgFnPc6d1	mezinárodní
organizacích	organizace	k1gFnPc6	organizace
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
bylo	být	k5eAaImAgNnS	být
Řecko	Řecko	k1gNnSc1	Řecko
již	již	k6eAd1	již
před	před	k7c7	před
rozpadem	rozpad	k1gInSc7	rozpad
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
,	,	kIx,	,
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Bývalá	bývalý	k2eAgFnSc1d1	bývalá
jugoslávská	jugoslávský	k2eAgFnSc1d1	jugoslávská
republika	republika	k1gFnSc1	republika
Makedonie	Makedonie	k1gFnSc1	Makedonie
(	(	kIx(	(
<g/>
makedonsky	makedonsky	k6eAd1	makedonsky
П	П	k?	П
ј	ј	k?	ј
р	р	k?	р
М	М	k?	М
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Former	Former	k1gMnSc1	Former
Yugoslav	Yugoslav	k1gMnSc1	Yugoslav
Republic	Republice	k1gFnPc2	Republice
of	of	k?	of
Macedonia	Macedonium	k1gNnSc2	Macedonium
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
F.	F.	kA	F.
<g/>
Y.	Y.	kA	Y.
<g/>
R.	R.	kA	R.
<g/>
O.	O.	kA	O.
<g/>
M.	M.	kA	M.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc4	dějiny
Makedonie	Makedonie	k1gFnSc2	Makedonie
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
republiky	republika	k1gFnSc2	republika
Makedonie	Makedonie	k1gFnSc2	Makedonie
bylo	být	k5eAaImAgNnS	být
Slovany	Slovan	k1gMnPc4	Slovan
osídleno	osídlen	k2eAgNnSc1d1	osídleno
již	již	k6eAd1	již
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
a	a	k8xC	a
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
expanzi	expanze	k1gFnSc6	expanze
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
však	však	k9	však
podrobila	podrobit	k5eAaPmAgFnS	podrobit
Byzantská	byzantský	k2eAgFnSc1d1	byzantská
říše	říše	k1gFnSc1	říše
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
čtvrtině	čtvrtina	k1gFnSc6	čtvrtina
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
většina	většina	k1gFnSc1	většina
země	zem	k1gFnSc2	zem
pod	pod	k7c4	pod
vliv	vliv	k1gInSc4	vliv
říše	říš	k1gFnSc2	říš
Bulharské	bulharský	k2eAgFnSc2d1	bulharská
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
šířilo	šířit	k5eAaImAgNnS	šířit
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
velmi	velmi	k6eAd1	velmi
blízké	blízký	k2eAgFnSc2d1	blízká
Byzantské	byzantský	k2eAgFnSc2d1	byzantská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
ji	on	k3xPp3gFnSc4	on
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
definitivně	definitivně	k6eAd1	definitivně
roku	rok	k1gInSc2	rok
1018	[number]	k4	1018
<g/>
,	,	kIx,	,
o	o	k7c4	o
dvě	dva	k4xCgNnPc4	dva
století	století	k1gNnPc2	století
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
centrem	centr	k1gMnSc7	centr
nového	nový	k2eAgInSc2d1	nový
Bulharského	bulharský	k2eAgInSc2d1	bulharský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
koncem	koncem	k7c2	koncem
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ji	on	k3xPp3gFnSc4	on
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Turci	Turek	k1gMnPc1	Turek
během	během	k7c2	během
svých	svůj	k3xOyFgInPc2	svůj
výbojů	výboj	k1gInPc2	výboj
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Turecká	turecký	k2eAgFnSc1d1	turecká
vláda	vláda	k1gFnSc1	vláda
trvala	trvat	k5eAaImAgFnS	trvat
až	až	k6eAd1	až
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
ní	on	k3xPp3gFnSc2	on
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
Turci	Turek	k1gMnPc1	Turek
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
islamizovat	islamizovat	k5eAaBmF	islamizovat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
jen	jen	k9	jen
z	z	k7c2	z
části	část	k1gFnSc2	část
(	(	kIx(	(
<g/>
dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
33	[number]	k4	33
%	%	kIx~	%
muslimů	muslim	k1gMnPc2	muslim
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgInPc1d1	různý
národy	národ	k1gInPc1	národ
(	(	kIx(	(
<g/>
Řekové	Řek	k1gMnPc1	Řek
<g/>
,	,	kIx,	,
Srbové	Srb	k1gMnPc1	Srb
<g/>
,	,	kIx,	,
Bulhaři	Bulhar	k1gMnPc1	Bulhar
a	a	k8xC	a
Albánci	Albánec	k1gMnPc1	Albánec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
žily	žít	k5eAaImAgFnP	žít
postupně	postupně	k6eAd1	postupně
začaly	začít	k5eAaPmAgFnP	začít
organizovat	organizovat	k5eAaBmF	organizovat
národně	národně	k6eAd1	národně
osvobozenecké	osvobozenecký	k2eAgNnSc4d1	osvobozenecké
hnutí	hnutí	k1gNnSc4	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
někteří	některý	k3yIgMnPc1	některý
tito	tento	k3xDgMnPc1	tento
aktivisté	aktivista	k1gMnPc1	aktivista
žádali	žádat	k5eAaImAgMnP	žádat
přičlenění	přičlenění	k1gNnSc4	přičlenění
k	k	k7c3	k
nově	nově	k6eAd1	nově
vzniklému	vzniklý	k2eAgNnSc3d1	vzniklé
Bulharsku	Bulharsko	k1gNnSc3	Bulharsko
<g/>
,	,	kIx,	,
bohužel	bohužel	k9	bohužel
tato	tento	k3xDgFnSc1	tento
snaha	snaha	k1gFnSc1	snaha
neuspěla	uspět	k5eNaPmAgFnS	uspět
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
bylo	být	k5eAaImAgNnS	být
jako	jako	k8xS	jako
země	zem	k1gFnPc1	zem
příliš	příliš	k6eAd1	příliš
vzdálené	vzdálený	k2eAgFnPc1d1	vzdálená
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
ale	ale	k9	ale
po	po	k7c6	po
berlínském	berlínský	k2eAgInSc6d1	berlínský
kongresu	kongres	k1gInSc6	kongres
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1878	[number]	k4	1878
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
určitému	určitý	k2eAgNnSc3d1	určité
otevření	otevření	k1gNnSc3	otevření
Makedonie	Makedonie	k1gFnSc2	Makedonie
Bulharsku	Bulharsko	k1gNnSc3	Bulharsko
-	-	kIx~	-
hlavně	hlavně	k6eAd1	hlavně
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
obchodu	obchod	k1gInSc2	obchod
-	-	kIx~	-
ačkoliv	ačkoliv	k8xS	ačkoliv
sama	sám	k3xTgFnSc1	sám
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
stále	stále	k6eAd1	stále
pod	pod	k7c7	pod
Osmanskou	osmanský	k2eAgFnSc7d1	Osmanská
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
během	během	k7c2	během
balkánských	balkánský	k2eAgFnPc2d1	balkánská
válek	válka	k1gFnPc2	válka
byla	být	k5eAaImAgFnS	být
země	země	k1gFnSc1	země
konečně	konečně	k6eAd1	konečně
zbavena	zbaven	k2eAgFnSc1d1	zbavena
turecké	turecký	k2eAgFnPc4d1	turecká
nadvlády	nadvláda	k1gFnPc4	nadvláda
a	a	k8xC	a
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
mezi	mezi	k7c4	mezi
Bulharsko	Bulharsko	k1gNnSc4	Bulharsko
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc4	Řecko
<g/>
,	,	kIx,	,
Srbsko	Srbsko	k1gNnSc4	Srbsko
a	a	k8xC	a
Albánii	Albánie	k1gFnSc4	Albánie
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
konec	konec	k1gInSc1	konec
Makedonie	Makedonie	k1gFnSc2	Makedonie
jako	jako	k8xC	jako
jedné	jeden	k4xCgFnSc2	jeden
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
ustanovení	ustanovení	k1gNnSc4	ustanovení
Království	království	k1gNnSc2	království
SHS	SHS	kA	SHS
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Republiky	republika	k1gFnSc2	republika
Makedonie	Makedonie	k1gFnSc1	Makedonie
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
rámci	rámec	k1gInSc6	rámec
součástí	součást	k1gFnPc2	součást
Srbska	Srbsko	k1gNnSc2	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Království	království	k1gNnSc6	království
SHS	SHS	kA	SHS
k	k	k7c3	k
převratu	převrat	k1gInSc3	převrat
<g/>
,	,	kIx,	,
dosavadní	dosavadní	k2eAgFnSc6d1	dosavadní
členění	členění	k1gNnSc6	členění
respektující	respektující	k2eAgInPc4d1	respektující
předchozí	předchozí	k2eAgInPc4d1	předchozí
historické	historický	k2eAgInPc4d1	historický
celky	celek	k1gInPc4	celek
bylo	být	k5eAaImAgNnS	být
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
systémem	systém	k1gInSc7	systém
bánovin	bánovina	k1gFnPc2	bánovina
a	a	k8xC	a
území	území	k1gNnSc2	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Republiky	republika	k1gFnSc2	republika
Makedonie	Makedonie	k1gFnSc2	Makedonie
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
součástí	součást	k1gFnSc7	součást
Vardarské	Vardarský	k2eAgFnSc2d1	Vardarská
bánoviny	bánovina	k1gFnSc2	bánovina
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
většina	většina	k1gFnSc1	většina
území	území	k1gNnSc2	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Republiky	republika	k1gFnSc2	republika
Makedonie	Makedonie	k1gFnSc1	Makedonie
anektována	anektován	k2eAgFnSc1d1	anektována
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
1941	[number]	k4	1941
do	do	k7c2	do
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1944	[number]	k4	1944
Bulharskem	Bulharsko	k1gNnSc7	Bulharsko
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc2d1	západní
oblasti	oblast	k1gFnSc2	oblast
byly	být	k5eAaImAgFnP	být
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1941	[number]	k4	1941
anektovány	anektován	k2eAgFnPc1d1	anektována
Itálií	Itálie	k1gFnSc7	Itálie
a	a	k8xC	a
začleněny	začleněn	k2eAgInPc1d1	začleněn
do	do	k7c2	do
Albánie	Albánie	k1gFnSc2	Albánie
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1943	[number]	k4	1943
obsadila	obsadit	k5eAaPmAgFnS	obsadit
německá	německý	k2eAgFnSc1d1	německá
vojska	vojsko	k1gNnPc1	vojsko
oblasti	oblast	k1gFnSc2	oblast
předtím	předtím	k6eAd1	předtím
obsazené	obsazený	k2eAgNnSc1d1	obsazené
Itálií	Itálie	k1gFnSc7	Itálie
<g/>
,	,	kIx,	,
a	a	k8xC	a
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1944	[number]	k4	1944
také	také	k9	také
oblasti	oblast	k1gFnPc4	oblast
obsazené	obsazený	k2eAgFnPc4d1	obsazená
Bulharskem	Bulharsko	k1gNnSc7	Bulharsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
obě	dva	k4xCgFnPc1	dva
části	část	k1gFnPc1	část
okupovaly	okupovat	k5eAaBmAgFnP	okupovat
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
září	září	k1gNnSc2	září
1944	[number]	k4	1944
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1944	[number]	k4	1944
bylo	být	k5eAaImAgNnS	být
Kumanovo	Kumanův	k2eAgNnSc1d1	Kumanovo
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
přičleněno	přičleněn	k2eAgNnSc4d1	přičleněno
k	k	k7c3	k
Albánii	Albánie	k1gFnSc3	Albánie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
Lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
Makedonie	Makedonie	k1gFnSc1	Makedonie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1963	[number]	k4	1963
přejmenovaná	přejmenovaný	k2eAgFnSc1d1	přejmenovaná
na	na	k7c4	na
Socialistickou	socialistický	k2eAgFnSc4d1	socialistická
republiku	republika	k1gFnSc4	republika
Makedonii	Makedonie	k1gFnSc4	Makedonie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
nezávislost	nezávislost	k1gFnSc4	nezávislost
bez	bez	k7c2	bez
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
jediná	jediný	k2eAgFnSc1d1	jediná
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
jugoslávských	jugoslávský	k2eAgFnPc2d1	jugoslávská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1990	[number]	k4	1990
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
zaveden	zavést	k5eAaPmNgInS	zavést
politický	politický	k2eAgInSc1d1	politický
pluralismus	pluralismus	k1gInSc1	pluralismus
<g/>
;	;	kIx,	;
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
svobodných	svobodný	k2eAgFnPc2d1	svobodná
prezidentských	prezidentský	k2eAgFnPc2d1	prezidentská
voleb	volba	k1gFnPc2	volba
vyšel	vyjít	k5eAaPmAgMnS	vyjít
jako	jako	k9	jako
vítěz	vítěz	k1gMnSc1	vítěz
zkušený	zkušený	k2eAgMnSc1d1	zkušený
politik	politik	k1gMnSc1	politik
Kiro	Kira	k1gFnSc5	Kira
Gligorov	Gligorov	k1gInSc1	Gligorov
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
vládl	vládnout	k5eAaImAgInS	vládnout
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
1991	[number]	k4	1991
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
rozpadem	rozpad	k1gInSc7	rozpad
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
Makedonská	makedonský	k2eAgFnSc1d1	makedonská
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
<g/>
/	/	kIx~	/
<g/>
Socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
Makedonie	Makedonie	k1gFnSc1	Makedonie
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1991	[number]	k4	1991
svrchovanost	svrchovanost	k1gFnSc1	svrchovanost
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
SFRJ	SFRJ	kA	SFRJ
a	a	k8xC	a
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1991	[number]	k4	1991
změnila	změnit	k5eAaPmAgFnS	změnit
název	název	k1gInSc4	název
na	na	k7c4	na
Republika	republika	k1gFnSc1	republika
Makedonie	Makedonie	k1gFnSc1	Makedonie
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislost	nezávislost	k1gFnSc1	nezávislost
Republiky	republika	k1gFnSc2	republika
Makedonie	Makedonie	k1gFnSc1	Makedonie
byla	být	k5eAaImAgFnS	být
prohlášena	prohlásit	k5eAaPmNgFnS	prohlásit
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
Makedonie	Makedonie	k1gFnSc2	Makedonie
na	na	k7c6	na
území	území	k1gNnSc6	území
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
zpochybnilo	zpochybnit	k5eAaPmAgNnS	zpochybnit
Řecko	Řecko	k1gNnSc1	Řecko
legitimitu	legitimita	k1gFnSc4	legitimita
nového	nový	k2eAgInSc2d1	nový
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodně	mezinárodně	k6eAd1	mezinárodně
byla	být	k5eAaImAgFnS	být
proto	proto	k8xC	proto
Makedonie	Makedonie	k1gFnSc1	Makedonie
uznána	uznat	k5eAaPmNgFnS	uznat
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Řecko	Řecko	k1gNnSc1	Řecko
souhlasilo	souhlasit	k5eAaImAgNnS	souhlasit
s	s	k7c7	s
kompromisním	kompromisní	k2eAgInSc7d1	kompromisní
názvem	název	k1gInSc7	název
nového	nový	k2eAgInSc2d1	nový
makedonského	makedonský	k2eAgInSc2d1	makedonský
státu	stát	k1gInSc2	stát
Bývalá	bývalý	k2eAgFnSc1d1	bývalá
jugoslávská	jugoslávský	k2eAgFnSc1d1	jugoslávská
republika	republika	k1gFnSc1	republika
Makedonie	Makedonie	k1gFnSc1	Makedonie
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
nímž	jenž	k3xRgInSc7	jenž
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1993	[number]	k4	1993
přijata	přijmout	k5eAaPmNgFnS	přijmout
do	do	k7c2	do
OSN	OSN	kA	OSN
(	(	kIx(	(
<g/>
makedonská	makedonský	k2eAgFnSc1d1	makedonská
vláda	vláda	k1gFnSc1	vláda
tento	tento	k3xDgInSc4	tento
název	název	k1gInSc4	název
nepoužívala	používat	k5eNaImAgFnS	používat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vyhovělo	vyhovět	k5eAaPmAgNnS	vyhovět
protestům	protest	k1gInPc3	protest
Řecka	Řecko	k1gNnSc2	Řecko
proti	proti	k7c3	proti
užívání	užívání	k1gNnSc3	užívání
geografického	geografický	k2eAgInSc2d1	geografický
názvu	název	k1gInSc2	název
Makedonie	Makedonie	k1gFnSc2	Makedonie
a	a	k8xC	a
politického	politický	k2eAgInSc2d1	politický
názvu	název	k1gInSc2	název
Makedonská	makedonský	k2eAgFnSc1d1	makedonská
republika	republika	k1gFnSc1	republika
<g/>
/	/	kIx~	/
<g/>
Republika	republika	k1gFnSc1	republika
Makedonie	Makedonie	k1gFnSc1	Makedonie
<g/>
,	,	kIx,	,
dostala	dostat	k5eAaPmAgFnS	dostat
Makedonská	makedonský	k2eAgFnSc1d1	makedonská
republika	republika	k1gFnSc1	republika
podle	podle	k7c2	podle
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
OSN	OSN	kA	OSN
z	z	k7c2	z
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1993	[number]	k4	1993
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznaný	uznaný	k2eAgInSc4d1	uznaný
název	název	k1gInSc4	název
(	(	kIx(	(
<g/>
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
)	)	kIx)	)
Former	Former	k1gInSc1	Former
Yugoslav	Yugoslav	k1gMnSc1	Yugoslav
Republic	Republice	k1gFnPc2	Republice
of	of	k?	of
Macedonia	Macedonium	k1gNnSc2	Macedonium
(	(	kIx(	(
<g/>
FYROM	FYROM	kA	FYROM
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Bývalá	bývalý	k2eAgFnSc1d1	bývalá
jugoslávská	jugoslávský	k2eAgFnSc1d1	jugoslávská
republika	republika	k1gFnSc1	republika
Makedonie	Makedonie	k1gFnSc1	Makedonie
(	(	kIx(	(
<g/>
BJRM	BJRM	kA	BJRM
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
makedonsky	makedonsky	k6eAd1	makedonsky
Poranešna	Poranešna	k1gFnSc1	Poranešna
Jugoslovenska	Jugoslovensko	k1gNnSc2	Jugoslovensko
Republika	republika	k1gFnSc1	republika
Makedonija	Makedonija	k1gFnSc1	Makedonija
(	(	kIx(	(
<g/>
PJRM	PJRM	kA	PJRM
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
makedonsky	makedonsky	k6eAd1	makedonsky
cyrilicí	cyrilice	k1gFnSc7	cyrilice
П	П	k?	П
Ј	Ј	k?	Ј
Р	Р	k?	Р
М	М	k?	М
(	(	kIx(	(
<g/>
П	П	k?	П
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
název	název	k1gInSc1	název
používá	používat	k5eAaImIp3nS	používat
mezinárodní	mezinárodní	k2eAgNnPc4d1	mezinárodní
společenství	společenství	k1gNnPc4	společenství
i	i	k9	i
po	po	k7c6	po
urovnání	urovnání	k1gNnSc6	urovnání
makedonsko-řeckých	makedonsko-řecký	k2eAgInPc2d1	makedonsko-řecký
vztahů	vztah	k1gInPc2	vztah
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Makedonii	Makedonie	k1gFnSc6	Makedonie
se	se	k3xPyFc4	se
však	však	k9	však
název	název	k1gInSc1	název
BJRM	BJRM	kA	BJRM
nikdy	nikdy	k6eAd1	nikdy
nepoužíval	používat	k5eNaImAgMnS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Makedonie	Makedonie	k1gFnSc1	Makedonie
používá	používat	k5eAaImIp3nS	používat
název	název	k1gInSc4	název
Republika	republika	k1gFnSc1	republika
Makedonie	Makedonie	k1gFnSc1	Makedonie
<g/>
,	,	kIx,	,
makedonsky	makedonsky	k6eAd1	makedonsky
Republika	republika	k1gFnSc1	republika
Makedonija	Makedonija	k1gFnSc1	Makedonija
<g/>
.	.	kIx.	.
</s>
<s>
Republika	republika	k1gFnSc1	republika
Makedonie	Makedonie	k1gFnSc1	Makedonie
se	se	k3xPyFc4	se
dokázala	dokázat	k5eAaPmAgFnS	dokázat
držet	držet	k5eAaImF	držet
stranou	strana	k1gFnSc7	strana
všech	všecek	k3xTgInPc2	všecek
konfliktů	konflikt	k1gInPc2	konflikt
na	na	k7c6	na
území	území	k1gNnSc6	území
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
během	během	k7c2	během
konfliktu	konflikt	k1gInSc2	konflikt
v	v	k7c6	v
sousedním	sousední	k2eAgNnSc6d1	sousední
Kosovu	Kosův	k2eAgFnSc4d1	Kosova
se	se	k3xPyFc4	se
Makedonie	Makedonie	k1gFnSc1	Makedonie
stala	stát	k5eAaPmAgFnS	stát
dočasným	dočasný	k2eAgNnSc7d1	dočasné
útočištěm	útočiště	k1gNnSc7	útočiště
pro	pro	k7c4	pro
uprchlíky	uprchlík	k1gMnPc4	uprchlík
z	z	k7c2	z
Kosova	Kosův	k2eAgNnSc2d1	Kosovo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2000	[number]	k4	2000
<g/>
–	–	k?	–
<g/>
2001	[number]	k4	2001
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
zuřil	zuřit	k5eAaImAgMnS	zuřit
ozbrojený	ozbrojený	k2eAgInSc4d1	ozbrojený
spor	spor	k1gInSc4	spor
mezi	mezi	k7c7	mezi
makedonskou	makedonský	k2eAgFnSc7d1	makedonská
vládou	vláda	k1gFnSc7	vláda
a	a	k8xC	a
albánskými	albánský	k2eAgMnPc7d1	albánský
povstalci	povstalec	k1gMnPc7	povstalec
z	z	k7c2	z
Národní	národní	k2eAgFnSc2d1	národní
osvobozenecké	osvobozenecký	k2eAgFnSc2d1	osvobozenecká
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
až	až	k9	až
na	na	k7c4	na
hranu	hrana	k1gFnSc4	hrana
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zprostředkování	zprostředkování	k1gNnSc6	zprostředkování
NATO	NATO	kA	NATO
a	a	k8xC	a
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
se	s	k7c7	s
spor	spora	k1gFnPc2	spora
podařilo	podařit	k5eAaPmAgNnS	podařit
uklidnit	uklidnit	k5eAaPmF	uklidnit
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
napjaté	napjatý	k2eAgFnPc1d1	napjatá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
Makedonie	Makedonie	k1gFnSc1	Makedonie
podala	podat	k5eAaPmAgFnS	podat
přihlášku	přihláška	k1gFnSc4	přihláška
do	do	k7c2	do
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
v	v	k7c6	v
Makedonii	Makedonie	k1gFnSc6	Makedonie
znovu	znovu	k6eAd1	znovu
vypukly	vypuknout	k5eAaPmAgFnP	vypuknout
etnické	etnický	k2eAgInPc4d1	etnický
nepokoje	nepokoj	k1gInPc4	nepokoj
mezi	mezi	k7c7	mezi
Albánci	Albánec	k1gMnPc7	Albánec
a	a	k8xC	a
Makedonci	Makedonec	k1gMnPc7	Makedonec
a	a	k8xC	a
k	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
konfliktu	konflikt	k1gInSc3	konflikt
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Makedonie	Makedonie	k1gFnSc2	Makedonie
<g/>
.	.	kIx.	.
</s>
<s>
Republika	republika	k1gFnSc1	republika
Makedonie	Makedonie	k1gFnSc2	Makedonie
je	být	k5eAaImIp3nS	být
vnitrozemský	vnitrozemský	k2eAgInSc1d1	vnitrozemský
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
rozloha	rozloha	k1gFnSc1	rozloha
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
třikrát	třikrát	k6eAd1	třikrát
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
rozloha	rozloha	k1gFnSc1	rozloha
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Sousedí	sousedit	k5eAaImIp3nS	sousedit
se	se	k3xPyFc4	se
Srbskem	Srbsko	k1gNnSc7	Srbsko
a	a	k8xC	a
Kosovem	Kosovo	k1gNnSc7	Kosovo
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
Bulharskem	Bulharsko	k1gNnSc7	Bulharsko
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
s	s	k7c7	s
Řeckem	Řecko	k1gNnSc7	Řecko
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
,	,	kIx,	,
s	s	k7c7	s
Albánií	Albánie	k1gFnSc7	Albánie
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
Republika	republika	k1gFnSc1	republika
Makedonie	Makedonie	k1gFnSc1	Makedonie
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
severozápadní	severozápadní	k2eAgFnSc1d1	severozápadní
(	(	kIx(	(
<g/>
7	[number]	k4	7
%	%	kIx~	%
<g/>
)	)	kIx)	)
část	část	k1gFnSc1	část
historické	historický	k2eAgFnSc2d1	historická
a	a	k8xC	a
(	(	kIx(	(
<g/>
35,8	[number]	k4	35,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
geografické	geografický	k2eAgFnSc2d1	geografická
Makedonie	Makedonie	k1gFnSc2	Makedonie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
označovala	označovat	k5eAaImAgFnS	označovat
jako	jako	k9	jako
Vardarská	Vardarský	k2eAgFnSc1d1	Vardarská
Makedonie	Makedonie	k1gFnSc1	Makedonie
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
makedonskou	makedonský	k2eAgFnSc7d1	makedonská
řekou	řeka	k1gFnSc7	řeka
je	být	k5eAaImIp3nS	být
Vardar	Vardar	k1gInSc1	Vardar
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
388	[number]	k4	388
km	km	kA	km
a	a	k8xC	a
vlévá	vlévat	k5eAaImIp3nS	vlévat
se	se	k3xPyFc4	se
u	u	k7c2	u
řecké	řecký	k2eAgFnSc2d1	řecká
Soluně	Soluň	k1gFnSc2	Soluň
do	do	k7c2	do
Egejského	egejský	k2eAgNnSc2d1	Egejské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
úmoří	úmoří	k1gNnSc3	úmoří
Egejského	egejský	k2eAgNnSc2d1	Egejské
moře	moře	k1gNnSc2	moře
patří	patřit	k5eAaImIp3nS	patřit
87	[number]	k4	87
%	%	kIx~	%
území	území	k1gNnSc6	území
Makedonie	Makedonie	k1gFnSc2	Makedonie
<g/>
.	.	kIx.	.
13	[number]	k4	13
%	%	kIx~	%
území	území	k1gNnSc6	území
je	být	k5eAaImIp3nS	být
odvodňováno	odvodňovat	k5eAaImNgNnS	odvodňovat
do	do	k7c2	do
Jaderského	jaderský	k2eAgNnSc2d1	Jaderské
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
37	[number]	k4	37
km2	km2	k4	km2
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
úmoří	úmoří	k1gNnSc3	úmoří
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgInPc1	tři
národní	národní	k2eAgInPc1d1	národní
parky	park	k1gInPc1	park
<g/>
:	:	kIx,	:
Mavrovo	Mavrův	k2eAgNnSc4d1	Mavrovo
<g/>
,	,	kIx,	,
Galičica	Galičica	k1gMnSc1	Galičica
a	a	k8xC	a
Pelister	Pelister	k1gMnSc1	Pelister
<g/>
.	.	kIx.	.
</s>
<s>
Dohromady	dohromady	k6eAd1	dohromady
zabírají	zabírat	k5eAaImIp3nP	zabírat
plochu	plocha	k1gFnSc4	plocha
1	[number]	k4	1
083	[number]	k4	083
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
Makedonie	Makedonie	k1gFnSc2	Makedonie
<g/>
.	.	kIx.	.
</s>
<s>
Republika	republika	k1gFnSc1	republika
Makedonie	Makedonie	k1gFnSc2	Makedonie
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
parlamentní	parlamentní	k2eAgFnSc7d1	parlamentní
demokracií	demokracie	k1gFnSc7	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
o	o	k7c4	o
integraci	integrace	k1gFnSc4	integrace
země	zem	k1gFnSc2	zem
do	do	k7c2	do
NATO	NATO	kA	NATO
a	a	k8xC	a
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Republika	republika	k1gFnSc1	republika
Makedonie	Makedonie	k1gFnSc1	Makedonie
se	se	k3xPyFc4	se
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
potýká	potýkat	k5eAaImIp3nS	potýkat
s	s	k7c7	s
ostrým	ostrý	k2eAgInSc7d1	ostrý
nesouhlasem	nesouhlas	k1gInSc7	nesouhlas
Řecka	Řecko	k1gNnSc2	Řecko
vůči	vůči	k7c3	vůči
oficiálnímu	oficiální	k2eAgInSc3d1	oficiální
názvu	název	k1gInSc3	název
a	a	k8xC	a
symbolům	symbol	k1gInPc3	symbol
nezávislého	závislý	k2eNgInSc2d1	nezávislý
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
Řecko	Řecko	k1gNnSc1	Řecko
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
zcizené	zcizený	k2eAgFnPc4d1	zcizená
historické	historický	k2eAgFnPc4d1	historická
(	(	kIx(	(
<g/>
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
z	z	k7c2	z
části	část	k1gFnSc2	část
řecké	řecký	k2eAgNnSc1d1	řecké
<g/>
)	)	kIx)	)
Makedonii	Makedonie	k1gFnSc4	Makedonie
<g/>
.	.	kIx.	.
</s>
<s>
Makedonie	Makedonie	k1gFnSc1	Makedonie
nemá	mít	k5eNaImIp3nS	mít
okresy	okres	k1gInPc1	okres
či	či	k8xC	či
kraje	kraj	k1gInPc1	kraj
<g/>
;	;	kIx,	;
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
statistické	statistický	k2eAgFnPc4d1	statistická
potřeby	potřeba	k1gFnPc4	potřeba
se	se	k3xPyFc4	se
Makedonie	Makedonie	k1gFnSc1	Makedonie
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
8	[number]	k4	8
statistických	statistický	k2eAgInPc2d1	statistický
regionů	region	k1gInPc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Administrativními	administrativní	k2eAgFnPc7d1	administrativní
jednotkami	jednotka	k1gFnPc7	jednotka
prvního	první	k4xOgInSc2	první
řádu	řád	k1gInSc2	řád
jsou	být	k5eAaImIp3nP	být
opštiny	opština	k1gFnPc1	opština
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
reorganizaci	reorganizace	k1gFnSc6	reorganizace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
úpravách	úprava	k1gFnPc6	úprava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gFnPc2	on
celkem	celkem	k6eAd1	celkem
80	[number]	k4	80
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
10	[number]	k4	10
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
tzv.	tzv.	kA	tzv.
Velkého	velký	k2eAgInSc2d1	velký
Skopje	Skopje	k1gFnPc1	Skopje
<g/>
,	,	kIx,	,
samosprávné	samosprávný	k2eAgFnPc1d1	samosprávná
jednotky	jednotka	k1gFnPc1	jednotka
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
opštiny	opština	k1gFnPc1	opština
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
slučováním	slučování	k1gNnSc7	slučování
a	a	k8xC	a
úpravou	úprava	k1gFnSc7	úprava
hranic	hranice	k1gFnPc2	hranice
123	[number]	k4	123
opštin	opština	k1gFnPc2	opština
zřízených	zřízený	k2eAgFnPc2d1	zřízená
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
dříve	dříve	k6eAd2	dříve
existovalo	existovat	k5eAaImAgNnS	existovat
v	v	k7c6	v
Makedonii	Makedonie	k1gFnSc6	Makedonie
34	[number]	k4	34
správních	správní	k2eAgInPc2d1	správní
celků	celek	k1gInPc2	celek
různých	různý	k2eAgInPc2d1	různý
stupňů	stupeň	k1gInPc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Nejlidnatější	lidnatý	k2eAgFnSc7d3	nejlidnatější
opštinou	opština	k1gFnSc7	opština
je	být	k5eAaImIp3nS	být
Kumanovo	Kumanův	k2eAgNnSc1d1	Kumanovo
se	se	k3xPyFc4	se
105	[number]	k4	105
484	[number]	k4	484
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
,	,	kIx,	,
nejmenší	malý	k2eAgMnPc4d3	nejmenší
pak	pak	k6eAd1	pak
Vraneštica	Vraneštica	k1gFnSc1	Vraneštica
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
1	[number]	k4	1
322	[number]	k4	322
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Makedonie	Makedonie	k1gFnSc1	Makedonie
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Makedonie	Makedonie	k1gFnSc2	Makedonie
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
deseti	deset	k4xCc3	deset
nejslabším	slabý	k2eAgFnPc3d3	nejslabší
evropským	evropský	k2eAgFnPc3d1	Evropská
ekonomikám	ekonomika	k1gFnPc3	ekonomika
-	-	kIx~	-
hodnotou	hodnota	k1gFnSc7	hodnota
HDP	HDP	kA	HDP
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
v	v	k7c6	v
PPP	PPP	kA	PPP
8400	[number]	k4	8400
USD	USD	kA	USD
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
evropskými	evropský	k2eAgInPc7d1	evropský
státy	stát	k1gInPc7	stát
až	až	k9	až
na	na	k7c4	na
38	[number]	k4	38
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
banka	banka	k1gFnSc1	banka
ji	on	k3xPp3gFnSc4	on
řadí	řadit	k5eAaImIp3nS	řadit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
Lower-middle-income	Loweriddlencom	k1gInSc5	Lower-middle-incom
economies	economies	k1gInSc4	economies
(	(	kIx(	(
<g/>
podprůměrně	podprůměrně	k6eAd1	podprůměrně
příjmové	příjmový	k2eAgFnSc2d1	příjmová
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
status	status	k1gInSc4	status
kandidátské	kandidátský	k2eAgFnSc2d1	kandidátská
země	zem	k1gFnSc2	zem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Železniční	železniční	k2eAgFnSc1d1	železniční
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Makedonii	Makedonie	k1gFnSc6	Makedonie
<g/>
.	.	kIx.	.
</s>
<s>
Dopravní	dopravní	k2eAgFnSc1d1	dopravní
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
rozvinutá	rozvinutý	k2eAgFnSc1d1	rozvinutá
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
určeno	určit	k5eAaPmNgNnS	určit
mj.	mj.	kA	mj.
hornatým	hornatý	k2eAgInSc7d1	hornatý
povrchem	povrch	k1gInSc7	povrch
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
však	však	k9	však
kvalitnější	kvalitní	k2eAgMnSc1d2	kvalitnější
než	než	k8xS	než
v	v	k7c6	v
sousední	sousední	k2eAgFnSc6d1	sousední
Albánii	Albánie	k1gFnSc6	Albánie
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
tranzitní	tranzitní	k2eAgFnSc7d1	tranzitní
trasou	trasa	k1gFnSc7	trasa
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
vede	vést	k5eAaImIp3nS	vést
dálnice	dálnice	k1gFnSc1	dálnice
a	a	k8xC	a
hlavní	hlavní	k2eAgFnSc1d1	hlavní
železnice	železnice	k1gFnSc1	železnice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
směr	směr	k1gInSc1	směr
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
–	–	k?	–
Skopje	Skopje	k1gFnSc2	Skopje
–	–	k?	–
Soluň	Soluň	k1gFnSc1	Soluň
<g/>
.	.	kIx.	.
</s>
<s>
Dálniční	dálniční	k2eAgInPc1d1	dálniční
poplatky	poplatek	k1gInPc1	poplatek
se	se	k3xPyFc4	se
hradí	hradit	k5eAaImIp3nP	hradit
v	v	k7c6	v
hotovosti	hotovost	k1gFnSc6	hotovost
na	na	k7c6	na
mýtných	mýtné	k1gNnPc6	mýtné
stanicích	stanice	k1gFnPc6	stanice
(	(	kIx(	(
<g/>
cca	cca	kA	cca
1	[number]	k4	1
mýtná	mýtné	k1gNnPc1	mýtné
stanice	stanice	k1gFnSc2	stanice
na	na	k7c4	na
každých	každý	k3xTgInPc2	každý
10	[number]	k4	10
km	km	kA	km
dálnice	dálnice	k1gFnSc2	dálnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
silniční	silniční	k2eAgFnSc2d1	silniční
sítě	síť	k1gFnSc2	síť
je	být	k5eAaImIp3nS	být
10	[number]	k4	10
600	[number]	k4	600
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc1	několik
letišť	letiště	k1gNnPc2	letiště
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
zdaleka	zdaleka	k6eAd1	zdaleka
nejrušnější	rušný	k2eAgNnPc4d3	nejrušnější
je	být	k5eAaImIp3nS	být
Letiště	letiště	k1gNnPc4	letiště
Alexandra	Alexandr	k1gMnSc2	Alexandr
Velikého	veliký	k2eAgMnSc2d1	veliký
u	u	k7c2	u
Skopje	Skopje	k1gFnSc2	Skopje
<g/>
,	,	kIx,	,
menší	malý	k2eAgNnSc4d2	menší
letiště	letiště	k1gNnSc4	letiště
funguje	fungovat	k5eAaImIp3nS	fungovat
např.	např.	kA	např.
v	v	k7c6	v
Ohridu	Ohrid	k1gInSc6	Ohrid
<g/>
.	.	kIx.	.
</s>
<s>
Splavné	splavný	k2eAgFnPc1d1	splavná
řeky	řeka	k1gFnPc1	řeka
v	v	k7c6	v
Makedonii	Makedonie	k1gFnSc6	Makedonie
nejsou	být	k5eNaImIp3nP	být
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
síť	síť	k1gFnSc1	síť
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
920	[number]	k4	920
km	km	kA	km
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
hlavní	hlavní	k2eAgFnSc2d1	hlavní
trati	trať	k1gFnSc2	trať
vedoucí	vedoucí	k1gMnSc1	vedoucí
ze	z	k7c2	z
Srbska	Srbsko	k1gNnSc2	Srbsko
přes	přes	k7c4	přes
Skopje	Skopje	k1gFnPc4	Skopje
a	a	k8xC	a
napříč	napříč	k7c7	napříč
Makedonií	Makedonie	k1gFnSc7	Makedonie
do	do	k7c2	do
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
po	po	k7c6	po
níž	jenž	k3xRgFnSc6	jenž
jezdí	jezdit	k5eAaImIp3nP	jezdit
několik	několik	k4yIc1	několik
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
rychlíků	rychlík	k1gInPc2	rychlík
<g/>
,	,	kIx,	,
a	a	k8xC	a
z	z	k7c2	z
nepříliš	příliš	k6eNd1	příliš
frekventovaných	frekventovaný	k2eAgFnPc2d1	frekventovaná
odboček	odbočka	k1gFnPc2	odbočka
(	(	kIx(	(
<g/>
Skopje	Skopje	k1gFnSc1	Skopje
–	–	k?	–
Priština	Priština	k1gFnSc1	Priština
(	(	kIx(	(
<g/>
Kosovo	Kosův	k2eAgNnSc1d1	Kosovo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Skopje	Skopje	k1gFnSc2	Skopje
–	–	k?	–
Tetovo	Tetův	k2eAgNnSc1d1	Tetovo
–	–	k?	–
Kičevo	Kičevo	k1gNnSc1	Kičevo
<g/>
,	,	kIx,	,
Veles	Veles	k1gInSc1	Veles
–	–	k?	–
Bitola	Bitola	k1gFnSc1	Bitola
<g/>
,	,	kIx,	,
Veles	Veles	k1gInSc1	Veles
–	–	k?	–
Kočani	Kočaň	k1gFnSc3	Kočaň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
Albánií	Albánie	k1gFnSc7	Albánie
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
připravována	připravován	k2eAgFnSc1d1	připravována
stavba	stavba	k1gFnSc1	stavba
trati	trať	k1gFnSc2	trať
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
s	s	k7c7	s
Bulharskem	Bulharsko	k1gNnSc7	Bulharsko
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Makedonie	Makedonie	k1gFnSc2	Makedonie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dat	datum	k1gNnPc2	datum
z	z	k7c2	z
posledního	poslední	k2eAgNnSc2d1	poslední
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
měla	mít	k5eAaImAgFnS	mít
Makedonie	Makedonie	k1gFnSc1	Makedonie
2	[number]	k4	2
022	[number]	k4	022
547	[number]	k4	547
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
odhad	odhad	k1gInSc1	odhad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
zvláštních	zvláštní	k2eAgFnPc2d1	zvláštní
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
<g/>
dává	dávat	k5eAaImIp3nS	dávat
číslo	číslo	k1gNnSc1	číslo
2	[number]	k4	2
050	[number]	k4	050
671	[number]	k4	671
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
posledního	poslední	k2eAgNnSc2d1	poslední
sčítání	sčítání	k1gNnSc2	sčítání
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
nejpočetnější	početní	k2eAgNnSc4d3	nejpočetnější
etnikum	etnikum	k1gNnSc4	etnikum
Makedonci	Makedonec	k1gMnSc3	Makedonec
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc4	druhý
největší	veliký	k2eAgFnSc4d3	veliký
skupinu	skupina	k1gFnSc4	skupina
představují	představovat	k5eAaImIp3nP	představovat
Albánci	Albánec	k1gMnPc1	Albánec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
dominují	dominovat	k5eAaImIp3nP	dominovat
severozápdní	severozápdní	k2eAgFnPc4d1	severozápdní
části	část	k1gFnPc4	část
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
třetím	třetí	k4xOgNnSc6	třetí
místě	místo	k1gNnSc6	místo
následují	následovat	k5eAaImIp3nP	následovat
Turci	Turek	k1gMnPc1	Turek
s	s	k7c7	s
téměř	téměř	k6eAd1	téměř
80	[number]	k4	80
000	[number]	k4	000
obyvateli	obyvatel	k1gMnPc7	obyvatel
oficiálně	oficiálně	k6eAd1	oficiálně
<g/>
;	;	kIx,	;
neoficiální	neoficiální	k2eAgInPc1d1	neoficiální
odhady	odhad	k1gInPc1	odhad
hovoří	hovořit	k5eAaImIp3nP	hovořit
o	o	k7c4	o
170	[number]	k4	170
000	[number]	k4	000
až	až	k9	až
200	[number]	k4	200
000	[number]	k4	000
Turky	turek	k1gInPc7	turek
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
neoficiální	oficiální	k2eNgInPc1d1	neoficiální
odhady	odhad	k1gInPc1	odhad
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Makedonii	Makedonie	k1gFnSc6	Makedonie
žije	žít	k5eAaImIp3nS	žít
až	až	k9	až
260	[number]	k4	260
000	[number]	k4	000
Cikánů	cikán	k1gMnPc2	cikán
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgNnSc1d1	oficiální
a	a	k8xC	a
nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
jazykem	jazyk	k1gInSc7	jazyk
makedonština	makedonština	k1gFnSc1	makedonština
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
východní	východní	k2eAgFnSc2d1	východní
větve	větev	k1gFnSc2	větev
jihoslovanských	jihoslovanský	k2eAgInPc2d1	jihoslovanský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obcích	obec	k1gFnPc6	obec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
zastoupeny	zastoupit	k5eAaPmNgFnP	zastoupit
etnické	etnický	k2eAgFnPc1d1	etnická
skupiny	skupina	k1gFnPc1	skupina
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
<g/>
%	%	kIx~	%
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jazyk	jazyk	k1gInSc1	jazyk
tohoto	tento	k3xDgNnSc2	tento
daného	daný	k2eAgNnSc2d1	dané
etnika	etnikum	k1gNnSc2	etnikum
"	"	kIx"	"
<g/>
spoluúřední	spoluúřední	k2eAgFnSc1d1	spoluúřední
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Většinové	většinový	k2eAgNnSc1d1	většinové
náboženství	náboženství	k1gNnSc1	náboženství
republiky	republika	k1gFnSc2	republika
Makedonie	Makedonie	k1gFnSc2	Makedonie
je	být	k5eAaImIp3nS	být
pravoslaví	pravoslaví	k1gNnSc1	pravoslaví
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
65	[number]	k4	65
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc1	populace
a	a	k8xC	a
drtivá	drtivý	k2eAgFnSc1d1	drtivá
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
náleží	náležet	k5eAaImIp3nP	náležet
k	k	k7c3	k
makedonské	makedonský	k2eAgFnSc3d1	makedonská
pravoslavné	pravoslavný	k2eAgFnSc3d1	pravoslavná
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jiným	jiný	k2eAgFnPc3d1	jiná
křesťanským	křesťanský	k2eAgFnPc3d1	křesťanská
denominacím	denominace	k1gFnPc3	denominace
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
0,4	[number]	k4	0,4
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Muslimové	muslim	k1gMnPc1	muslim
tvoří	tvořit	k5eAaImIp3nP	tvořit
33,3	[number]	k4	33,3
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Makedonie	Makedonie	k1gFnSc1	Makedonie
má	mít	k5eAaImIp3nS	mít
pátý	pátý	k4xOgInSc1	pátý
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
podíl	podíl	k1gInSc1	podíl
muslimů	muslim	k1gMnPc2	muslim
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
po	po	k7c4	po
Kosovu	Kosův	k2eAgFnSc4d1	Kosova
(	(	kIx(	(
<g/>
96	[number]	k4	96
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Turecku	Turecko	k1gNnSc6	Turecko
(	(	kIx(	(
<g/>
90	[number]	k4	90
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Albánii	Albánie	k1gFnSc4	Albánie
(	(	kIx(	(
<g/>
59	[number]	k4	59
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
Bosně-Hercegovina	Bosně-Hercegovina	k1gFnSc1	Bosně-Hercegovina
(	(	kIx(	(
<g/>
51	[number]	k4	51
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
muslimů	muslim	k1gMnPc2	muslim
jsou	být	k5eAaImIp3nP	být
Albánci	Albánec	k1gMnPc1	Albánec
<g/>
,	,	kIx,	,
Turci	Turek	k1gMnPc1	Turek
nebo	nebo	k8xC	nebo
Cikáni	cikán	k1gMnPc1	cikán
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
menší	malý	k2eAgInPc4d2	menší
počty	počet	k1gInPc4	počet
makedonských	makedonský	k2eAgMnPc2d1	makedonský
muslimů	muslim	k1gMnPc2	muslim
<g/>
.	.	kIx.	.
</s>
<s>
Zbývajících	zbývající	k2eAgNnPc2d1	zbývající
1,4	[number]	k4	1,4
<g/>
%	%	kIx~	%
je	být	k5eAaImIp3nS	být
označeno	označit	k5eAaPmNgNnS	označit
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
nepřidružení	nepřidružení	k1gNnSc1	nepřidružení
<g/>
"	"	kIx"	"
dle	dle	k7c2	dle
odhadů	odhad	k1gInPc2	odhad
Pew	Pew	k1gFnSc2	Pew
Research	Researcha	k1gFnPc2	Researcha
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
prvním	první	k4xOgMnPc3	první
autorům	autor	k1gMnPc3	autor
makedonské	makedonský	k2eAgFnSc2d1	makedonská
literatury	literatura	k1gFnSc2	literatura
je	být	k5eAaImIp3nS	být
počítán	počítán	k2eAgMnSc1d1	počítán
Kiril	Kiril	k1gMnSc1	Kiril
Pejčinovič	Pejčinovič	k1gMnSc1	Pejčinovič
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
moderní	moderní	k2eAgFnSc2d1	moderní
makedonské	makedonský	k2eAgFnSc2d1	makedonská
literatury	literatura	k1gFnSc2	literatura
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
Kočo	Kočo	k6eAd1	Kočo
Racin	Racin	k1gInSc1	Racin
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
též	též	k9	též
titovský	titovský	k1gMnSc1	titovský
partyzán	partyzán	k1gMnSc1	partyzán
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
spisovatelem	spisovatel	k1gMnSc7	spisovatel
byl	být	k5eAaImAgMnS	být
Blaže	blažit	k5eAaImSgInS	blažit
Koneski	Konesk	k1gMnSc6	Konesk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
též	též	k9	též
významně	významně	k6eAd1	významně
přispěl	přispět	k5eAaPmAgMnS	přispět
ke	k	k7c3	k
kodifikaci	kodifikace	k1gFnSc3	kodifikace
spisovné	spisovný	k2eAgFnSc2d1	spisovná
makedonštiny	makedonština	k1gFnSc2	makedonština
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
postavou	postava	k1gFnSc7	postava
je	být	k5eAaImIp3nS	být
též	též	k9	též
prozaik	prozaik	k1gMnSc1	prozaik
a	a	k8xC	a
historik	historik	k1gMnSc1	historik
Ďordi	Ďord	k1gMnPc1	Ďord
Abadžijev	Abadžijev	k1gMnSc1	Abadžijev
či	či	k8xC	či
básník	básník	k1gMnSc1	básník
Mateja	Matejus	k1gMnSc2	Matejus
Matevski	Matevski	k1gNnSc2	Matevski
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Kole	Kola	k1gFnSc3	Kola
Nedelkovski	Nedelkovsk	k1gFnSc2	Nedelkovsk
<g/>
,	,	kIx,	,
Venko	Venka	k1gMnSc5	Venka
Markovski	Markovsk	k1gMnSc5	Markovsk
<g/>
,	,	kIx,	,
Gane	Ganus	k1gMnSc5	Ganus
Todorovski	Todorovsk	k1gMnSc5	Todorovsk
nebo	nebo	k8xC	nebo
Petre	Petr	k1gMnSc5	Petr
Mito	Mita	k1gMnSc5	Mita
Andreevski	Andreevsk	k1gMnSc5	Andreevsk
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnějším	slavný	k2eAgMnSc7d3	nejslavnější
makedonským	makedonský	k2eAgMnSc7d1	makedonský
umělcem	umělec	k1gMnSc7	umělec
byl	být	k5eAaImAgMnS	být
tragicky	tragicky	k6eAd1	tragicky
zahynulý	zahynulý	k2eAgMnSc1d1	zahynulý
zpěvák	zpěvák	k1gMnSc1	zpěvák
Toše	Toš	k1gFnSc2	Toš
Proeski	Proesk	k1gFnSc2	Proesk
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšnou	úspěšný	k2eAgFnSc7d1	úspěšná
byla	být	k5eAaImAgFnS	být
rovněž	rovněž	k9	rovněž
romská	romský	k2eAgFnSc1d1	romská
hudebnice	hudebnice	k1gFnSc1	hudebnice
Esma	Esma	k?	Esma
Redžepovová	Redžepovová	k1gFnSc1	Redžepovová
a	a	k8xC	a
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
je	být	k5eAaImIp3nS	být
také	také	k9	také
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Kaliopi	Kaliopi	k1gNnSc2	Kaliopi
<g/>
.	.	kIx.	.
</s>
<s>
Nejúspěšnějším	úspěšný	k2eAgMnSc7d3	nejúspěšnější
sochařem	sochař	k1gMnSc7	sochař
je	být	k5eAaImIp3nS	být
brutalista	brutalista	k1gMnSc1	brutalista
Dušan	Dušan	k1gMnSc1	Dušan
Džamonja	Džamonja	k1gMnSc1	Džamonja
<g/>
.	.	kIx.	.
</s>
<s>
Zlatého	zlatý	k2eAgMnSc4d1	zlatý
lva	lev	k1gMnSc4	lev
z	z	k7c2	z
Benátek	Benátky	k1gFnPc2	Benátky
a	a	k8xC	a
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
má	mít	k5eAaImIp3nS	mít
režisér	režisér	k1gMnSc1	režisér
Milčo	Milča	k1gFnSc5	Milča
Mančevski	Mančevsk	k1gFnSc3	Mančevsk
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Makedonská	makedonský	k2eAgFnSc1d1	makedonská
kuchyně	kuchyně	k1gFnSc1	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
Makedonská	makedonský	k2eAgFnSc1d1	makedonská
kuchyně	kuchyně	k1gFnSc1	kuchyně
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgFnSc1d1	tradiční
kuchyně	kuchyně	k1gFnSc1	kuchyně
v	v	k7c6	v
Republice	republika	k1gFnSc6	republika
Makedonii	Makedonie	k1gFnSc6	Makedonie
-	-	kIx~	-
zástupce	zástupce	k1gMnSc1	zástupce
balkánské	balkánský	k2eAgFnSc2d1	balkánská
kuchyně	kuchyně	k1gFnSc2	kuchyně
odrážející	odrážející	k2eAgInPc1d1	odrážející
vlivy	vliv	k1gInPc1	vliv
Středomoří	středomoří	k1gNnSc2	středomoří
<g/>
,	,	kIx,	,
řecké	řecký	k2eAgFnSc2d1	řecká
kuchyně	kuchyně	k1gFnSc2	kuchyně
<g/>
,	,	kIx,	,
turecké	turecký	k2eAgFnSc2d1	turecká
kuchyně	kuchyně	k1gFnSc2	kuchyně
a	a	k8xC	a
kuchyně	kuchyně	k1gFnSc2	kuchyně
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
menším	malý	k2eAgInSc6d2	menší
rozsahu	rozsah	k1gInSc6	rozsah
italské	italský	k2eAgFnPc1d1	italská
<g/>
,	,	kIx,	,
německé	německý	k2eAgFnPc1d1	německá
a	a	k8xC	a
východoevropské	východoevropský	k2eAgFnPc1d1	východoevropská
kuchyně	kuchyně	k1gFnPc1	kuchyně
(	(	kIx(	(
<g/>
speciálně	speciálně	k6eAd1	speciálně
maďarské	maďarský	k2eAgFnPc1d1	maďarská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Relativně	relativně	k6eAd1	relativně
teplé	teplý	k2eAgNnSc1d1	teplé
klima	klima	k1gNnSc1	klima
v	v	k7c4	v
Makedonie	Makedonie	k1gFnPc4	Makedonie
nabízí	nabízet	k5eAaImIp3nS	nabízet
vynikající	vynikající	k2eAgFnPc4d1	vynikající
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
růst	růst	k1gInSc4	růst
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
zeleniny	zelenina	k1gFnSc2	zelenina
<g/>
,	,	kIx,	,
ovoce	ovoce	k1gNnSc2	ovoce
a	a	k8xC	a
bylin	bylina	k1gFnPc2	bylina
<g/>
.	.	kIx.	.
</s>
<s>
Makedonské	makedonský	k2eAgFnSc2d1	makedonská
kuchyně	kuchyně	k1gFnSc2	kuchyně
je	být	k5eAaImIp3nS	být
také	také	k9	také
známá	známý	k2eAgFnSc1d1	známá
pro	pro	k7c4	pro
rozmanitost	rozmanitost	k1gFnSc4	rozmanitost
a	a	k8xC	a
kvalitu	kvalita	k1gFnSc4	kvalita
mléčných	mléčný	k2eAgInPc2d1	mléčný
výrobků	výrobek	k1gInPc2	výrobek
<g/>
,	,	kIx,	,
vína	víno	k1gNnSc2	víno
a	a	k8xC	a
místních	místní	k2eAgInPc2d1	místní
alkoholických	alkoholický	k2eAgInPc2d1	alkoholický
nápojů	nápoj	k1gInPc2	nápoj
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
rakije	rakija	k1gFnSc2	rakija
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
národní	národní	k2eAgNnSc4d1	národní
jídlo	jídlo	k1gNnSc4	jídlo
Makedonie	Makedonie	k1gFnSc2	Makedonie
se	se	k3xPyFc4	se
pokládají	pokládat	k5eAaImIp3nP	pokládat
tavče	tavč	k1gInPc1	tavč
gravče	gravč	k1gFnSc2	gravč
(	(	kIx(	(
<g/>
zapečené	zapečený	k2eAgFnSc2d1	zapečená
fazole	fazole	k1gFnSc2	fazole
<g/>
)	)	kIx)	)
a	a	k8xC	a
likér	likér	k1gInSc4	likér
mastika	mastikum	k1gNnSc2	mastikum
<g/>
.	.	kIx.	.
</s>
<s>
Nejpopulárnějším	populární	k2eAgInSc7d3	nejpopulárnější
sportem	sport	k1gInSc7	sport
v	v	k7c6	v
Makedonii	Makedonie	k1gFnSc6	Makedonie
je	být	k5eAaImIp3nS	být
fotbal	fotbal	k1gInSc4	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
Nejúspěšnějším	úspěšný	k2eAgMnSc7d3	nejúspěšnější
makedonským	makedonský	k2eAgMnSc7d1	makedonský
fotbalistou	fotbalista	k1gMnSc7	fotbalista
je	být	k5eAaImIp3nS	být
vítěz	vítěz	k1gMnSc1	vítěz
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
kopačky	kopačka	k1gFnSc2	kopačka
Darko	Darko	k1gNnSc1	Darko
Pančev	Pančev	k1gFnSc2	Pančev
a	a	k8xC	a
vítěz	vítěz	k1gMnSc1	vítěz
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
Goran	Goran	k1gInSc1	Goran
Pandev	Pandva	k1gFnPc2	Pandva
<g/>
,	,	kIx,	,
trenérem	trenér	k1gMnSc7	trenér
Miljan	Miljan	k1gMnSc1	Miljan
Miljanić	Miljanić	k1gMnSc1	Miljanić
<g/>
.	.	kIx.	.
</s>
<s>
Neúspěšnějším	úspěšný	k2eNgMnSc7d2	neúspěšnější
házenkářem	házenkář	k1gMnSc7	házenkář
je	být	k5eAaImIp3nS	být
Kiril	Kiril	k1gMnSc1	Kiril
Lazarov	Lazarov	k1gInSc1	Lazarov
<g/>
,	,	kIx,	,
basketbalistou	basketbalista	k1gMnSc7	basketbalista
Pero	pero	k1gNnSc1	pero
Antić	Antić	k1gMnPc3	Antić
<g/>
.	.	kIx.	.
</s>
<s>
HRADEČNÝ	HRADEČNÝ	kA	HRADEČNÝ
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Makedonie	Makedonie	k1gFnSc1	Makedonie
<g/>
.	.	kIx.	.
</s>
<s>
Slovanský	slovanský	k2eAgInSc1d1	slovanský
přehled	přehled	k1gInSc1	přehled
<g/>
.	.	kIx.	.
</s>
<s>
Review	Review	k?	Review
for	forum	k1gNnPc2	forum
Central	Central	k1gMnPc2	Central
<g/>
,	,	kIx,	,
Eastern	Eastern	k1gMnSc1	Eastern
and	and	k?	and
Southeastern	Southeastern	k1gInSc1	Southeastern
European	European	k1gInSc1	European
History	Histor	k1gInPc1	Histor
<g/>
.	.	kIx.	.
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
82	[number]	k4	82
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
,	,	kIx,	,
s.	s.	k?	s.
117	[number]	k4	117
<g/>
-	-	kIx~	-
<g/>
150	[number]	k4	150
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
37	[number]	k4	37
<g/>
-	-	kIx~	-
<g/>
6922	[number]	k4	6922
<g/>
.	.	kIx.	.
</s>
<s>
PELIKÁN	Pelikán	k1gMnSc1	Pelikán
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Jihoslovanských	jihoslovanský	k2eAgFnPc2d1	Jihoslovanská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
758	[number]	k4	758
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
375	[number]	k4	375
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Rychlík	Rychlík	k1gMnSc1	Rychlík
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
Makedonie	Makedonie	k1gFnSc2	Makedonie
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2003	[number]	k4	2003
Makedonie	Makedonie	k1gFnSc1	Makedonie
(	(	kIx(	(
<g/>
region	region	k1gInSc1	region
<g/>
)	)	kIx)	)
Země	země	k1gFnSc1	země
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
Seznam	seznam	k1gInSc1	seznam
měst	město	k1gNnPc2	město
Republiky	republika	k1gFnSc2	republika
Makedonie	Makedonie	k1gFnSc2	Makedonie
Seznam	seznam	k1gInSc1	seznam
představitelů	představitel	k1gMnPc2	představitel
Makedonie	Makedonie	k1gFnSc2	Makedonie
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Makedonie	Makedonie	k1gFnSc2	Makedonie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Makedonie	Makedonie	k1gFnSc2	Makedonie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Makedonie	Makedonie	k1gFnSc2	Makedonie
na	na	k7c4	na
OpenStreetMap	OpenStreetMap	k1gInSc4	OpenStreetMap
(	(	kIx(	(
<g/>
makedonsky	makedonsky	k6eAd1	makedonsky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Národní	národní	k2eAgInSc1d1	národní
turistický	turistický	k2eAgInSc1d1	turistický
portál	portál	k1gInSc1	portál
Macedonia	Macedonium	k1gNnSc2	Macedonium
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gMnPc2	International
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Macedonia	Macedonium	k1gNnPc1	Macedonium
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc4	Freedom
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Macedonia	Macedonium	k1gNnSc2	Macedonium
Country	country	k2eAgInSc4d1	country
Report	report	k1gInSc4	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Bureau	Burea	k2eAgFnSc4d1	Burea
of	of	k?	of
European	Europeana	k1gFnPc2	Europeana
and	and	k?	and
Eurasian	Eurasian	k1gInSc1	Eurasian
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Not	k1gInSc2	Not
<g/>
:	:	kIx,	:
Macedonia	Macedonium	k1gNnSc2	Macedonium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
31	[number]	k4	31
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Macedonia	Macedonium	k1gNnSc2	Macedonium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
REV	REV	kA	REV
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
ve	v	k7c4	v
Skopje	Skopje	k1gFnPc4	Skopje
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Makedonie	Makedonie	k1gFnSc1	Makedonie
(	(	kIx(	(
<g/>
FYROM	FYROM	kA	FYROM
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
17	[number]	k4	17
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
ALLCOCK	ALLCOCK	kA	ALLCOCK
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
B	B	kA	B
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Macedonia	Macedonium	k1gNnPc1	Macedonium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
