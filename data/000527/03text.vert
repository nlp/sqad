<s>
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Hohenelbe	Hohenelb	k1gMnSc5	Hohenelb
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Albipolis	Albipolis	k1gFnSc1	Albipolis
<g/>
,	,	kIx,	,
krkonošským	krkonošský	k2eAgNnSc7d1	Krkonošské
nářečím	nářečí	k1gNnSc7	nářečí
Verchláb	Verchlába	k1gFnPc2	Verchlába
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
české	český	k2eAgNnSc1d1	české
město	město	k1gNnSc1	město
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Královéhradeckého	královéhradecký	k2eAgInSc2d1	královéhradecký
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Nazýváno	nazýván	k2eAgNnSc1d1	nazýváno
bývá	bývat	k5eAaImIp3nS	bývat
jako	jako	k9	jako
Vstupní	vstupní	k2eAgFnSc1d1	vstupní
brána	brána	k1gFnSc1	brána
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
zhruba	zhruba	k6eAd1	zhruba
13	[number]	k4	13
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
přibližně	přibližně	k6eAd1	přibližně
8	[number]	k4	8
300	[number]	k4	300
v	v	k7c6	v
produktivním	produktivní	k2eAgInSc6d1	produktivní
věku	věk	k1gInSc6	věk
<g/>
.	.	kIx.	.
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1	katastrální
výměra	výměra	k1gFnSc1	výměra
města	město	k1gNnSc2	město
činí	činit	k5eAaImIp3nS	činit
2	[number]	k4	2
766	[number]	k4	766
ha	ha	kA	ha
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zhruba	zhruba	k6eAd1	zhruba
28	[number]	k4	28
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgNnPc2d3	nejvýznamnější
center	centrum	k1gNnPc2	centrum
varhanářství	varhanářství	k1gNnSc2	varhanářství
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
se	se	k3xPyFc4	se
vypíná	vypínat	k5eAaImIp3nS	vypínat
hora	hora	k1gFnSc1	hora
Žalý	Žalý	k1gFnSc1	Žalý
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
název	název	k1gInSc1	název
města	město	k1gNnSc2	město
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgInS	dát
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
jako	jako	k9	jako
Vrchní	vrchní	k2eAgNnSc4d1	vrchní
či	či	k8xC	či
Horní	horní	k2eAgNnSc4d1	horní
Labe	Labe	k1gNnSc4	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Doslovný	doslovný	k2eAgInSc1d1	doslovný
překlad	překlad	k1gInSc1	překlad
německého	německý	k2eAgNnSc2d1	německé
pojmenování	pojmenování	k1gNnSc2	pojmenování
tohoto	tento	k3xDgNnSc2	tento
města	město	k1gNnSc2	město
-	-	kIx~	-
Hohenelbe	Hohenelb	k1gInSc5	Hohenelb
zní	znět	k5eAaImIp3nS	znět
Horní	horní	k2eAgNnSc4d1	horní
Labe	Labe	k1gNnSc4	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Latinsky	latinsky	k6eAd1	latinsky
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
nazývá	nazývat	k5eAaImIp3nS	nazývat
Albipolis	Albipolis	k1gFnSc4	Albipolis
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
česky	česky	k6eAd1	česky
znamená	znamenat	k5eAaImIp3nS	znamenat
Labské	labský	k2eAgNnSc4d1	Labské
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
objevovalo	objevovat	k5eAaImAgNnS	objevovat
další	další	k2eAgNnSc1d1	další
německé	německý	k2eAgNnSc1d1	německé
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
Vrchlabí	Vrchlabí	k1gNnSc4	Vrchlabí
-	-	kIx~	-
Giessdorf	Giessdorf	k1gInSc1	Giessdorf
<g/>
.	.	kIx.	.
</s>
<s>
Nepříbuznost	Nepříbuznost	k1gFnSc1	Nepříbuznost
tohoto	tento	k3xDgInSc2	tento
názvu	název	k1gInSc2	název
přivedla	přivést	k5eAaPmAgFnS	přivést
některé	některý	k3yIgMnPc4	některý
historiky	historik	k1gMnPc4	historik
k	k	k7c3	k
úvaze	úvaha	k1gFnSc3	úvaha
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
ležely	ležet	k5eAaImAgFnP	ležet
dvě	dva	k4xCgFnPc1	dva
vsi	ves	k1gFnPc1	ves
-	-	kIx~	-
české	český	k2eAgNnSc1d1	české
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
-	-	kIx~	-
Vrchlab	Vrchlab	k1gMnSc1	Vrchlab
a	a	k8xC	a
německý	německý	k2eAgMnSc1d1	německý
Gorswinsdorf	Gorswinsdorf	k1gMnSc1	Gorswinsdorf
nebo	nebo	k8xC	nebo
Giessdorf	Giessdorf	k1gMnSc1	Giessdorf
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Hohenelbe	Hohenelb	k1gInSc5	Hohenelb
dal	dát	k5eAaPmAgInS	dát
městu	město	k1gNnSc3	město
významný	významný	k2eAgMnSc1d1	významný
majitel	majitel	k1gMnSc1	majitel
celého	celý	k2eAgNnSc2d1	celé
panství	panství	k1gNnSc2	panství
Kryštof	Kryštof	k1gMnSc1	Kryštof
z	z	k7c2	z
Gendorfu	Gendorf	k1gInSc2	Gendorf
<g/>
.	.	kIx.	.
</s>
<s>
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
nazývalo	nazývat	k5eAaImAgNnS	nazývat
Wrchlabie	Wrchlabie	k1gFnSc2	Wrchlabie
<g/>
,	,	kIx,	,
Wrychlab	Wrychlab	k1gInSc1	Wrychlab
<g/>
,	,	kIx,	,
Wrchlabí	Wrchlabí	k1gNnSc1	Wrchlabí
<g/>
,	,	kIx,	,
Wrchlabj	Wrchlabj	k1gInSc1	Wrchlabj
<g/>
,	,	kIx,	,
Wrchlaby	Wrchlab	k1gInPc1	Wrchlab
Werchlab	Werchlaba	k1gFnPc2	Werchlaba
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc3	jeho
dnešnímu	dnešní	k2eAgNnSc3d1	dnešní
jménu	jméno	k1gNnSc3	jméno
velice	velice	k6eAd1	velice
podobné	podobný	k2eAgFnPc1d1	podobná
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
objevujících	objevující	k2eAgFnPc2d1	objevující
variant	varianta	k1gFnPc2	varianta
je	být	k5eAaImIp3nS	být
také	také	k9	také
pojmenování	pojmenování	k1gNnSc1	pojmenování
Wrchlab	Wrchlaba	k1gFnPc2	Wrchlaba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
objevuje	objevovat	k5eAaImIp3nS	objevovat
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
deskách	deska	k1gFnPc6	deska
zemských	zemský	k2eAgFnPc2d1	zemská
roku	rok	k1gInSc2	rok
1624	[number]	k4	1624
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomuto	tento	k3xDgNnSc3	tento
pojmenování	pojmenování	k1gNnSc1	pojmenování
Vrchlabí	Vrchlabí	k1gNnPc2	Vrchlabí
například	například	k6eAd1	například
získala	získat	k5eAaPmAgFnS	získat
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
osada	osada	k1gFnSc1	osada
Labe	Labe	k1gNnSc2	Labe
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
částí	část	k1gFnPc2	část
Jindřichova	Jindřichův	k2eAgFnSc1d1	Jindřichova
u	u	k7c2	u
Velkých	velký	k2eAgFnPc2d1	velká
Losin	Losiny	k1gFnPc2	Losiny
<g/>
.	.	kIx.	.
</s>
<s>
Prvními	první	k4xOgInPc7	první
osadníky	osadník	k1gMnPc7	osadník
byli	být	k5eAaImAgMnP	být
totiž	totiž	k9	totiž
lidé	člověk	k1gMnPc1	člověk
pocházející	pocházející	k2eAgMnPc1d1	pocházející
z	z	k7c2	z
Vrchlabí	Vrchlabí	k1gNnPc2	Vrchlabí
–	–	k?	–
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
elby	elba	k1gFnSc2	elba
a	a	k8xC	a
díky	díky	k7c3	díky
špatnému	špatný	k2eAgInSc3d1	špatný
překladu	překlad	k1gInSc3	překlad
názvu	název	k1gInSc2	název
elba	elb	k1gInSc2	elb
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
slovo	slovo	k1gNnSc1	slovo
Labe	Labe	k1gNnSc2	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgNnSc4d1	přesné
datum	datum	k1gNnSc4	datum
založení	založení	k1gNnSc2	založení
sídla	sídlo	k1gNnSc2	sídlo
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
zakladatel	zakladatel	k1gMnSc1	zakladatel
<g/>
,	,	kIx,	,
či	či	k8xC	či
první	první	k4xOgMnPc1	první
osídlenci	osídlenec	k1gMnPc1	osídlenec
nejsou	být	k5eNaImIp3nP	být
známi	znám	k2eAgMnPc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
údaj	údaj	k1gInSc1	údaj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
přesně	přesně	k6eAd1	přesně
dokazoval	dokazovat	k5eAaImAgInS	dokazovat
existenci	existence	k1gFnSc4	existence
sídla	sídlo	k1gNnSc2	sídlo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
údaj	údaj	k1gInSc4	údaj
obsahující	obsahující	k2eAgInSc4d1	obsahující
letopočet	letopočet	k1gInSc4	letopočet
1359	[number]	k4	1359
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
letopočet	letopočet	k1gInSc1	letopočet
byl	být	k5eAaImAgInS	být
napsán	napsat	k5eAaPmNgInS	napsat
ve	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
úmrtím	úmrtí	k1gNnSc7	úmrtí
zdejšího	zdejší	k2eAgMnSc2d1	zdejší
kněze	kněz	k1gMnSc2	kněz
Petra	Petr	k1gMnSc2	Petr
<g/>
,	,	kIx,	,
po	po	k7c6	po
němž	jenž	k3xRgNnSc6	jenž
byl	být	k5eAaImAgMnS	být
zde	zde	k6eAd1	zde
knězem	kněz	k1gMnSc7	kněz
Heřman	Heřman	k1gMnSc1	Heřman
z	z	k7c2	z
Děčína	Děčín	k1gInSc2	Děčín
<g/>
.	.	kIx.	.
</s>
<s>
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
tedy	tedy	k9	tedy
jasně	jasně	k6eAd1	jasně
muselo	muset	k5eAaImAgNnS	muset
existovat	existovat	k5eAaImF	existovat
před	před	k7c7	před
napsáním	napsání	k1gNnSc7	napsání
tohoto	tento	k3xDgInSc2	tento
listu	list	k1gInSc2	list
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
sídla	sídlo	k1gNnSc2	sídlo
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
Jiří	Jiří	k1gMnSc1	Jiří
Louda	Louda	k1gMnSc1	Louda
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
publikaci	publikace	k1gFnSc6	publikace
"	"	kIx"	"
<g/>
Zmizelé	zmizelý	k2eAgNnSc1d1	zmizelé
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
<g/>
"	"	kIx"	"
na	na	k7c4	na
přelom	přelom	k1gInSc4	přelom
13	[number]	k4	13
<g/>
.	.	kIx.	.
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ves	ves	k1gFnSc1	ves
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
spadala	spadat	k5eAaPmAgNnP	spadat
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
pod	pod	k7c4	pod
trutnovské	trutnovský	k2eAgNnSc4d1	trutnovské
manství	manství	k1gNnSc4	manství
<g/>
,	,	kIx,	,
založené	založený	k2eAgNnSc4d1	založené
roku	rok	k1gInSc2	rok
1277	[number]	k4	1277
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1359	[number]	k4	1359
spravoval	spravovat	k5eAaImAgInS	spravovat
celé	celý	k2eAgNnSc4d1	celé
vrchlabské	vrchlabský	k2eAgNnSc4d1	vrchlabské
panství	panství	k1gNnSc4	panství
Hašek	Hašek	k1gMnSc1	Hašek
z	z	k7c2	z
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1363	[number]	k4	1363
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
nechal	nechat	k5eAaPmAgMnS	nechat
vystavět	vystavět	k5eAaPmF	vystavět
v	v	k7c6	v
přibližně	přibližně	k6eAd1	přibližně
místech	místo	k1gNnPc6	místo
dnešního	dnešní	k2eAgInSc2d1	dnešní
rybníka	rybník	k1gInSc2	rybník
v	v	k7c6	v
zámeckém	zámecký	k2eAgInSc6d1	zámecký
parku	park	k1gInSc6	park
dřevěnou	dřevěný	k2eAgFnSc4d1	dřevěná
vodní	vodní	k2eAgFnSc4d1	vodní
tvrz	tvrz	k1gFnSc4	tvrz
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc4	některý
zdroje	zdroj	k1gInPc4	zdroj
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
připomínaná	připomínaný	k2eAgFnSc1d1	připomínaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1454	[number]	k4	1454
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc4	některý
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
připomínaná	připomínaný	k2eAgFnSc1d1	připomínaná
až	až	k9	až
roku	rok	k1gInSc2	rok
1525	[number]	k4	1525
<g/>
.	.	kIx.	.
</s>
<s>
Tvrz	tvrz	k1gFnSc1	tvrz
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
připomíná	připomínat	k5eAaImIp3nS	připomínat
až	až	k9	až
roku	rok	k1gInSc2	rok
1574	[number]	k4	1574
jako	jako	k8xC	jako
starý	starý	k2eAgInSc1d1	starý
zámek	zámek	k1gInSc1	zámek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
obklopen	obklopit	k5eAaPmNgInS	obklopit
vodními	vodní	k2eAgInPc7d1	vodní
příkopy	příkop	k1gInPc7	příkop
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vyhořela	vyhořet	k5eAaPmAgFnS	vyhořet
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
byla	být	k5eAaImAgFnS	být
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1575	[number]	k4	1575
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
zdědil	zdědit	k5eAaPmAgInS	zdědit
panství	panství	k1gNnSc6	panství
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Petr	Petr	k1gMnSc1	Petr
(	(	kIx(	(
<g/>
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nezletilý	nezletilý	k1gMnSc1	nezletilý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
něj	on	k3xPp3gInSc4	on
zde	zde	k6eAd1	zde
vládla	vládnout	k5eAaImAgFnS	vládnout
vdova	vdova	k1gFnSc1	vdova
po	po	k7c6	po
Haškovi	Hašek	k1gMnSc6	Hašek
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1370	[number]	k4	1370
<g/>
.	.	kIx.	.
</s>
<s>
Zpráv	zpráva	k1gFnPc2	zpráva
o	o	k7c6	o
existenci	existence	k1gFnSc6	existence
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
máme	mít	k5eAaImIp1nP	mít
velice	velice	k6eAd1	velice
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většinou	k6eAd1	většinou
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
zprávy	zpráva	k1gFnPc1	zpráva
týkají	týkat	k5eAaImIp3nP	týkat
církevních	církevní	k2eAgFnPc2d1	církevní
záležitostí	záležitost	k1gFnPc2	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
změny	změna	k1gFnPc1	změna
kněží	kněz	k1gMnPc2	kněz
<g/>
,	,	kIx,	,
zřizování	zřizování	k1gNnSc1	zřizování
kaplanství	kaplanství	k1gNnPc2	kaplanství
atd.	atd.	kA	atd.
V	v	k7c6	v
začátku	začátek	k1gInSc6	začátek
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
přibývají	přibývat	k5eAaImIp3nP	přibývat
záznamy	záznam	k1gInPc1	záznam
o	o	k7c6	o
převodech	převod	k1gInPc6	převod
majetku	majetek	k1gInSc2	majetek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
objevovat	objevovat	k5eAaImF	objevovat
nejen	nejen	k6eAd1	nejen
české	český	k2eAgNnSc4d1	české
pojmenování	pojmenování	k1gNnSc4	pojmenování
sídla	sídlo	k1gNnSc2	sídlo
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
německé	německý	k2eAgFnSc2d1	německá
Giessdorf	Giessdorf	k1gInSc4	Giessdorf
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
některých	některý	k3yIgInPc2	některý
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1424	[number]	k4	1424
či	či	k8xC	či
1421	[number]	k4	1421
vypálilo	vypálit	k5eAaPmAgNnS	vypálit
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
Žižkovo	Žižkův	k2eAgNnSc1d1	Žižkovo
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Potvrzené	potvrzený	k2eAgNnSc1d1	potvrzené
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
vypálena	vypálen	k2eAgFnSc1d1	vypálena
byla	být	k5eAaImAgFnS	být
sousední	sousední	k2eAgFnSc1d1	sousední
Klášterská	klášterský	k2eAgFnSc1d1	Klášterská
Lhota	Lhota	k1gFnSc1	Lhota
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
vrchlabské	vrchlabský	k2eAgNnSc1d1	vrchlabské
proboštství	proboštství	k1gNnSc1	proboštství
benediktinů	benediktin	k1gMnPc2	benediktin
s	s	k7c7	s
klášterem	klášter	k1gInSc7	klášter
nazývaným	nazývaný	k2eAgInSc7d1	nazývaný
"	"	kIx"	"
<g/>
cela	cela	k1gFnSc1	cela
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
stál	stát	k5eAaImAgMnS	stát
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
dnešní	dnešní	k2eAgFnSc2d1	dnešní
obalovny	obalovna	k1gFnSc2	obalovna
směrem	směr	k1gInSc7	směr
na	na	k7c6	na
Hostinné	hostinný	k2eAgFnSc6d1	hostinná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
pobočkou	pobočka	k1gFnSc7	pobočka
benediktinského	benediktinský	k2eAgInSc2d1	benediktinský
kláštera	klášter	k1gInSc2	klášter
v	v	k7c6	v
Opatovicích	Opatovice	k1gFnPc6	Opatovice
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
ostatními	ostatní	k2eAgFnPc7d1	ostatní
nevýznamnými	významný	k2eNgFnPc7d1	nevýznamná
a	a	k8xC	a
krátkými	krátký	k2eAgFnPc7d1	krátká
zprávami	zpráva	k1gFnPc7	zpráva
z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
proto	proto	k8xC	proto
vynikají	vynikat	k5eAaImIp3nP	vynikat
záznamy	záznam	k1gInPc1	záznam
o	o	k7c6	o
sporu	spor	k1gInSc6	spor
Petra	Petr	k1gMnSc2	Petr
z	z	k7c2	z
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
a	a	k8xC	a
jistého	jistý	k2eAgMnSc2d1	jistý
Aleše	Aleš	k1gMnSc2	Aleš
Šanovce	Šanovec	k1gMnSc2	Šanovec
ze	z	k7c2	z
Šonova	Šonov	k1gInSc2	Šonov
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
zástavní	zástavní	k2eAgMnSc1d1	zástavní
pán	pán	k1gMnSc1	pán
v	v	k7c6	v
Hostinném	hostinný	k2eAgInSc6d1	hostinný
o	o	k7c6	o
práva	právo	k1gNnPc1	právo
nad	nad	k7c7	nad
lánovskými	lánovský	k2eAgMnPc7d1	lánovský
poddanými	poddaný	k1gMnPc7	poddaný
a	a	k8xC	a
o	o	k7c4	o
právo	právo	k1gNnSc4	právo
lovu	lov	k1gInSc2	lov
v	v	k7c6	v
okolních	okolní	k2eAgInPc6d1	okolní
lesích	les	k1gInPc6	les
<g/>
.	.	kIx.	.
</s>
<s>
Vrchlabský	vrchlabský	k2eAgMnSc1d1	vrchlabský
pán	pán	k1gMnSc1	pán
nakonec	nakonec	k6eAd1	nakonec
tento	tento	k3xDgInSc1	tento
spor	spor	k1gInSc1	spor
roku	rok	k1gInSc2	rok
1486	[number]	k4	1486
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
Hašků	Hašek	k1gMnPc2	Hašek
z	z	k7c2	z
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
s	s	k7c7	s
bílou	bílý	k2eAgFnSc7d1	bílá
psí	psí	k2eAgFnSc7d1	psí
hlavou	hlava	k1gFnSc7	hlava
se	s	k7c7	s
zlatým	zlatý	k2eAgInSc7d1	zlatý
obojkem	obojek	k1gInSc7	obojek
v	v	k7c6	v
černém	černý	k2eAgNnSc6d1	černé
poli	pole	k1gNnSc6	pole
ve	v	k7c6	v
svém	své	k1gNnSc6	své
erbu	erb	k1gInSc2	erb
opustili	opustit	k5eAaPmAgMnP	opustit
své	svůj	k3xOyFgNnSc4	svůj
rodiště	rodiště	k1gNnSc4	rodiště
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
je	být	k5eAaImIp3nS	být
vysoudila	vysoudit	k5eAaPmAgFnS	vysoudit
pro	pro	k7c4	pro
sirotky	sirotka	k1gFnPc4	sirotka
po	po	k7c6	po
Petrovi	Petr	k1gMnSc6	Petr
z	z	k7c2	z
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
teta	teta	k1gFnSc1	teta
Barbora	Barbora	k1gFnSc1	Barbora
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
zboží	zboží	k1gNnSc2	zboží
od	od	k7c2	od
nich	on	k3xPp3gFnPc2	on
nabyl	nabýt	k5eAaPmAgMnS	nabýt
Jan	Jan	k1gMnSc1	Jan
Krupý	Krupý	k2eAgMnSc1d1	Krupý
z	z	k7c2	z
Probluze	Probluze	k1gFnSc2	Probluze
-	-	kIx~	-
známý	známý	k2eAgMnSc1d1	známý
obchodník	obchodník	k1gMnSc1	obchodník
s	s	k7c7	s
nemovitostmi	nemovitost	k1gFnPc7	nemovitost
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
Jan	Jana	k1gFnPc2	Jana
z	z	k7c2	z
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
prodal	prodat	k5eAaPmAgInS	prodat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1502	[number]	k4	1502
bratřím	bratřit	k5eAaImIp1nS	bratřit
Janovi	Jan	k1gMnSc3	Jan
a	a	k8xC	a
Hynkovi	Hynek	k1gMnSc3	Hynek
Kordulům	Kordulum	k1gNnPc3	Kordulum
ze	z	k7c2	z
Sloupna	Sloupno	k1gNnSc2	Sloupno
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
prameny	pramen	k1gInPc4	pramen
však	však	k9	však
uvádí	uvádět	k5eAaImIp3nS	uvádět
vrchlabských	vrchlabský	k2eAgMnPc2d1	vrchlabský
vladyků	vladyka	k1gMnPc2	vladyka
daleko	daleko	k6eAd1	daleko
více	hodně	k6eAd2	hodně
<g/>
:	:	kIx,	:
měli	mít	k5eAaImAgMnP	mít
jimi	on	k3xPp3gMnPc7	on
být	být	k5eAaImF	být
Hašek	Hašek	k1gMnSc1	Hašek
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
Čeněk	Čeněk	k1gMnSc1	Čeněk
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
z	z	k7c2	z
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
a	a	k8xC	a
Šítka	Šítko	k1gNnSc2	Šítko
z	z	k7c2	z
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1506	[number]	k4	1506
pak	pak	k6eAd1	pak
Jan	Jan	k1gMnSc1	Jan
Kordule	kordule	k1gFnSc2	kordule
ze	z	k7c2	z
Sloupna	Sloupn	k1gInSc2	Sloupn
získal	získat	k5eAaPmAgInS	získat
bratrův	bratrův	k2eAgInSc1d1	bratrův
díl	díl	k1gInSc1	díl
i	i	k9	i
díl	díl	k1gInSc1	díl
Jana	Jan	k1gMnSc2	Jan
Krupého	Krupý	k2eAgMnSc2d1	Krupý
z	z	k7c2	z
Probluze	Probluze	k1gFnSc2	Probluze
a	a	k8xC	a
majetek	majetek	k1gInSc4	majetek
spojil	spojit	k5eAaPmAgMnS	spojit
<g/>
.	.	kIx.	.
</s>
<s>
Majetek	majetek	k1gInSc1	majetek
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
sestával	sestávat	k5eAaImAgMnS	sestávat
z	z	k7c2	z
tvrze	tvrz	k1gFnSc2	tvrz
<g/>
,	,	kIx,	,
dvora	dvůr	k1gInSc2	dvůr
<g/>
,	,	kIx,	,
vsí	ves	k1gFnSc7	ves
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
<g/>
,	,	kIx,	,
Žďáru	Žďár	k1gInSc2	Žďár
<g/>
,	,	kIx,	,
Kněžic	kněžice	k1gFnPc2	kněžice
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgFnPc1d1	Nové
Vsi	ves	k1gFnPc1	ves
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc1d1	dnešní
Černý	černý	k2eAgInSc1d1	černý
Důl	důl	k1gInSc1	důl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Holenic	holenice	k1gFnPc2	holenice
(	(	kIx(	(
<g/>
u	u	k7c2	u
Lomnice	Lomnice	k1gFnSc2	Lomnice
nad	nad	k7c7	nad
Popelkou	Popelka	k1gFnSc7	Popelka
<g/>
)	)	kIx)	)
a	a	k8xC	a
částí	část	k1gFnSc7	část
Lánova	Lánov	k1gInSc2	Lánov
<g/>
,	,	kIx,	,
Lhoty	Lhota	k1gFnSc2	Lhota
(	(	kIx(	(
<g/>
snad	snad	k9	snad
Klášterské	klášterský	k2eAgFnSc3d1	Klášterská
<g/>
)	)	kIx)	)
a	a	k8xC	a
Horní	horní	k2eAgNnSc1d1	horní
Branné	branný	k2eAgInPc1d1	branný
<g/>
.	.	kIx.	.
</s>
<s>
Historik	historik	k1gMnSc1	historik
August	August	k1gMnSc1	August
Sedláček	Sedláček	k1gMnSc1	Sedláček
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
díle	díl	k1gInSc6	díl
Hrady	hrad	k1gInPc1	hrad
<g/>
,	,	kIx,	,
zámky	zámek	k1gInPc1	zámek
a	a	k8xC	a
tvrze	tvrz	k1gFnPc1	tvrz
Království	království	k1gNnSc2	království
českého	český	k2eAgNnSc2d1	české
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hrad	hrad	k1gInSc1	hrad
Purkhybl	Purkhybl	k1gFnSc2	Purkhybl
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
zaniklý	zaniklý	k2eAgInSc1d1	zaniklý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
nad	nad	k7c7	nad
soutokem	soutok	k1gInSc7	soutok
říčky	říčka	k1gFnSc2	říčka
Čisté	čistá	k1gFnSc2	čistá
a	a	k8xC	a
Zrcadlového	zrcadlový	k2eAgInSc2d1	zrcadlový
potoka	potok	k1gInSc2	potok
u	u	k7c2	u
silnice	silnice	k1gFnSc2	silnice
do	do	k7c2	do
Černého	Černého	k2eAgInSc2d1	Černého
Dolu	dol	k1gInSc2	dol
byl	být	k5eAaImAgInS	být
sídlem	sídlo	k1gNnSc7	sídlo
tohoto	tento	k3xDgInSc2	tento
"	"	kIx"	"
<g/>
statku	statek	k1gInSc2	statek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dědička	dědička	k1gFnSc1	dědička
Jana	Jan	k1gMnSc2	Jan
Korduly	Kordula	k1gFnSc2	Kordula
ze	z	k7c2	z
Sloupna	Sloupn	k1gInSc2	Sloupn
Kordula	Kordula	k1gFnSc1	Kordula
(	(	kIx(	(
<g/>
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
zároveň	zároveň	k6eAd1	zároveň
jeho	jeho	k3xOp3gFnSc7	jeho
dcerou	dcera	k1gFnSc7	dcera
<g/>
)	)	kIx)	)
nechala	nechat	k5eAaPmAgFnS	nechat
zapsat	zapsat	k5eAaPmF	zapsat
tyto	tento	k3xDgInPc4	tento
statky	statek	k1gInPc4	statek
roku	rok	k1gInSc2	rok
1518	[number]	k4	1518
svému	svůj	k3xOyFgMnSc3	svůj
manželovi	manžel	k1gMnSc3	manžel
Janovi	Jan	k1gMnSc3	Jan
Tetaurovi	Tetaur	k1gMnSc3	Tetaur
z	z	k7c2	z
Tetova	Tetov	k1gInSc2	Tetov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
však	však	k9	však
vyskytl	vyskytnout	k5eAaPmAgInS	vyskytnout
velký	velký	k2eAgInSc1d1	velký
problém	problém	k1gInSc1	problém
<g/>
.	.	kIx.	.
</s>
<s>
Kordulin	Kordulin	k2eAgMnSc1d1	Kordulin
otec	otec	k1gMnSc1	otec
neplnil	plnit	k5eNaImAgMnS	plnit
své	svůj	k3xOyFgFnPc4	svůj
manské	manský	k2eAgFnPc4d1	manská
povinnosti	povinnost	k1gFnPc4	povinnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
král	král	k1gMnSc1	král
Ludvík	Ludvík	k1gMnSc1	Ludvík
Jagellonský	jagellonský	k2eAgInSc1d1	jagellonský
považoval	považovat	k5eAaImAgInS	považovat
manský	manský	k2eAgInSc1d1	manský
statek	statek	k1gInSc1	statek
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
po	po	k7c6	po
Janově	Janův	k2eAgFnSc6d1	Janova
smrti	smrt	k1gFnSc6	smrt
za	za	k7c4	za
volný	volný	k2eAgInSc4d1	volný
a	a	k8xC	a
manželé	manžel	k1gMnPc1	manžel
Kordula	Kordula	k1gFnSc1	Kordula
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Tetaurovi	Tetaurův	k2eAgMnPc1d1	Tetaurův
museli	muset	k5eAaImAgMnP	muset
vynaložit	vynaložit	k5eAaPmF	vynaložit
velkou	velký	k2eAgFnSc4d1	velká
píli	píle	k1gFnSc4	píle
a	a	k8xC	a
velké	velký	k2eAgInPc4d1	velký
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
udrželi	udržet	k5eAaPmAgMnP	udržet
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
svého	své	k1gNnSc2	své
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1525	[number]	k4	1525
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
Ludvík	Ludvík	k1gMnSc1	Ludvík
propustil	propustit	k5eAaPmAgMnS	propustit
z	z	k7c2	z
léna	léno	k1gNnSc2	léno
vrchlabskou	vrchlabský	k2eAgFnSc4d1	vrchlabská
tvrz	tvrz	k1gFnSc4	tvrz
<g/>
,	,	kIx,	,
dvůr	dvůr	k1gInSc4	dvůr
<g/>
,	,	kIx,	,
ves	ves	k1gFnSc4	ves
<g/>
,	,	kIx,	,
dvory	dvůr	k1gInPc4	dvůr
v	v	k7c4	v
Horní	horní	k2eAgFnPc4d1	horní
Branné	branný	k2eAgFnPc4d1	Branná
<g/>
,	,	kIx,	,
vsi	ves	k1gFnPc4	ves
Kněžice	kněžice	k1gFnSc2	kněžice
<g/>
,	,	kIx,	,
Žďár	Žďár	k1gInSc1	Žďár
<g/>
,	,	kIx,	,
Holenice	holenice	k1gFnSc1	holenice
a	a	k8xC	a
část	část	k1gFnSc1	část
Lhoty	Lhota	k1gFnSc2	Lhota
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
stalo	stát	k5eAaPmAgNnS	stát
dědičným	dědičný	k2eAgNnSc7d1	dědičné
panstvím	panství	k1gNnSc7	panství
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
pána	pán	k1gMnSc2	pán
z	z	k7c2	z
Tetova	Tetov	k1gInSc2	Tetov
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
spor	spor	k1gInSc1	spor
o	o	k7c4	o
doly	dol	k1gInPc4	dol
u	u	k7c2	u
Lánova	Lánov	k1gInSc2	Lánov
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
Jan	Jan	k1gMnSc1	Jan
Tetaur	Tetaur	k1gMnSc1	Tetaur
vedl	vést	k5eAaImAgMnS	vést
s	s	k7c7	s
Voršilou	Voršila	k1gFnSc7	Voršila
z	z	k7c2	z
Vartenberka	Vartenberka	k1gFnSc1	Vartenberka
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
roku	rok	k1gInSc2	rok
1533	[number]	k4	1533
koupil	koupit	k5eAaPmAgMnS	koupit
panství	panství	k1gNnSc4	panství
(	(	kIx(	(
<g/>
které	který	k3yRgNnSc1	který
čítalo	čítat	k5eAaImAgNnS	čítat
vrchlabskou	vrchlabský	k2eAgFnSc4d1	vrchlabská
tvrz	tvrz	k1gFnSc4	tvrz
<g/>
,	,	kIx,	,
poplužní	poplužní	k2eAgInSc4d1	poplužní
dvůr	dvůr	k1gInSc4	dvůr
a	a	k8xC	a
samotnou	samotný	k2eAgFnSc4d1	samotná
ves	ves	k1gFnSc4	ves
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
<g/>
,	,	kIx,	,
pustý	pustý	k2eAgInSc4d1	pustý
hrad	hrad	k1gInSc4	hrad
Purkhybl	Purkhybl	k1gFnSc2	Purkhybl
(	(	kIx(	(
<g/>
Burghübl	Burghübl	k1gMnSc1	Burghübl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
Lánova	Lánovo	k1gNnSc2	Lánovo
s	s	k7c7	s
horou	hora	k1gFnSc7	hora
železnou	železný	k2eAgFnSc7d1	železná
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
šest	šest	k4xCc1	šest
vsí	ves	k1gFnPc2	ves
<g/>
)	)	kIx)	)
od	od	k7c2	od
Jana	Jan	k1gMnSc2	Jan
Tetaura	Tetaur	k1gMnSc4	Tetaur
šestatřicetiletý	šestatřicetiletý	k2eAgMnSc1d1	šestatřicetiletý
Kryštof	Kryštof	k1gMnSc1	Kryštof
z	z	k7c2	z
Gendorfu	Gendorf	k1gInSc2	Gendorf
-	-	kIx~	-
korutanský	korutanský	k2eAgMnSc1d1	korutanský
důlní	důlní	k2eAgMnSc1d1	důlní
odborník	odborník	k1gMnSc1	odborník
za	za	k7c4	za
1750	[number]	k4	1750
kop	kop	k1gInSc4	kop
českých	český	k2eAgInPc2d1	český
grošů	groš	k1gInPc2	groš
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
některé	některý	k3yIgInPc4	některý
prameny	pramen	k1gInPc4	pramen
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
musel	muset	k5eAaImAgMnS	muset
pán	pán	k1gMnSc1	pán
z	z	k7c2	z
Gendorfu	Gendorf	k1gInSc2	Gendorf
zaplatit	zaplatit	k5eAaPmF	zaplatit
více	hodně	k6eAd2	hodně
-	-	kIx~	-
3	[number]	k4	3
750	[number]	k4	750
kop	kop	k1gInSc4	kop
českých	český	k2eAgInPc2d1	český
grošů	groš	k1gInPc2	groš
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
vymohl	vymoct	k5eAaPmAgInS	vymoct
na	na	k7c6	na
tehdejším	tehdejší	k2eAgMnSc6d1	tehdejší
královi	král	k1gMnSc6	král
Ferdinandovi	Ferdinand	k1gMnSc6	Ferdinand
udělení	udělení	k1gNnSc4	udělení
práv	právo	k1gNnPc2	právo
města	město	k1gNnSc2	město
a	a	k8xC	a
znaku	znak	k1gInSc2	znak
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
Kryštofovi	Kryštof	k1gMnSc3	Kryštof
z	z	k7c2	z
Gendorfu	Gendorf	k1gInSc2	Gendorf
a	a	k8xC	a
Ferdinandovi	Ferdinandův	k2eAgMnPc1d1	Ferdinandův
I.	I.	kA	I.
mohli	moct	k5eAaImAgMnP	moct
od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1533	[number]	k4	1533
usazovat	usazovat	k5eAaImF	usazovat
řemeslníci	řemeslník	k1gMnPc1	řemeslník
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
dostalo	dostat	k5eAaPmAgNnS	dostat
nejen	nejen	k6eAd1	nejen
svůj	svůj	k3xOyFgInSc4	svůj
znak	znak	k1gInSc4	znak
a	a	k8xC	a
právo	právo	k1gNnSc4	právo
zelené	zelený	k2eAgFnSc2d1	zelená
pečeti	pečeť	k1gFnSc2	pečeť
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
privilegium	privilegium	k1gNnSc4	privilegium
dvou	dva	k4xCgMnPc2	dva
výročních	výroční	k2eAgMnPc2d1	výroční
trhů	trh	k1gInPc2	trh
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgInP	moct
konat	konat	k5eAaImF	konat
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
o	o	k7c6	o
svátcích	svátek	k1gInPc6	svátek
svatých	svatý	k1gMnPc2	svatý
Filipa	Filip	k1gMnSc2	Filip
a	a	k8xC	a
Jakuba	Jakub	k1gMnSc2	Jakub
a	a	k8xC	a
sv.	sv.	kA	sv.
Matouše	Matouš	k1gMnSc2	Matouš
<g/>
.	.	kIx.	.
</s>
<s>
Znak	znak	k1gInSc4	znak
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
město	město	k1gNnSc4	město
obdrželo	obdržet	k5eAaPmAgNnS	obdržet
byl	být	k5eAaImAgInS	být
štít	štít	k1gInSc1	štít
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
rozdělený	rozdělený	k2eAgInSc1d1	rozdělený
cimbuřovým	cimbuřův	k2eAgInSc7d1	cimbuřův
řezem	řez	k1gInSc7	řez
na	na	k7c4	na
vrchní	vrchní	k1gFnSc4	vrchní
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
a	a	k8xC	a
spodní	spodní	k2eAgFnSc4d1	spodní
červenou	červený	k2eAgFnSc4d1	červená
část	část	k1gFnSc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
sníženin	sníženina	k1gFnPc2	sníženina
řezu	řez	k1gInSc2	řez
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
později	pozdě	k6eAd2	pozdě
znázorňován	znázorňován	k2eAgMnSc1d1	znázorňován
jako	jako	k8xC	jako
skutečné	skutečný	k2eAgNnSc1d1	skutečné
hradební	hradební	k2eAgNnSc1d1	hradební
cimbuří	cimbuří	k1gNnSc1	cimbuří
z	z	k7c2	z
červených	červený	k2eAgFnPc2d1	červená
cihel	cihla	k1gFnPc2	cihla
<g/>
,	,	kIx,	,
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
dvě	dva	k4xCgFnPc1	dva
jedle	jedle	k1gFnPc1	jedle
a	a	k8xC	a
na	na	k7c6	na
červeném	červený	k2eAgNnSc6d1	červené
poli	pole	k1gNnSc6	pole
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgInSc1d2	menší
štít	štít	k1gInSc1	štít
s	s	k7c7	s
mlátkem	mlátek	k1gInSc7	mlátek
a	a	k8xC	a
želízkem	želízko	k1gNnSc7	želízko
-	-	kIx~	-
hornickým	hornický	k2eAgNnSc7d1	Hornické
nářadím	nářadí	k1gNnSc7	nářadí
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
status	status	k1gInSc1	status
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
městečko	městečko	k1gNnSc1	městečko
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
svého	svůj	k3xOyFgMnSc4	svůj
vlastního	vlastní	k2eAgMnSc4d1	vlastní
purkmistra	purkmistr	k1gMnSc4	purkmistr
<g/>
,	,	kIx,	,
rychtáře	rychtář	k1gMnSc4	rychtář
a	a	k8xC	a
radu	rada	k1gFnSc4	rada
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
kterými	který	k3yQgFnPc7	který
jako	jako	k8xS	jako
prvními	první	k4xOgMnPc7	první
byli	být	k5eAaImAgMnP	být
<g/>
:	:	kIx,	:
purkmistr	purkmistr	k1gMnSc1	purkmistr
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Schaller	Schaller	k1gMnSc1	Schaller
a	a	k8xC	a
rychtář	rychtář	k1gMnSc1	rychtář
Hanns	Hannsa	k1gFnPc2	Hannsa
Matzer	Matzer	k1gMnSc1	Matzer
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1538	[number]	k4	1538
jsou	být	k5eAaImIp3nP	být
zmiňováni	zmiňován	k2eAgMnPc1d1	zmiňován
též	též	k9	též
městští	městský	k2eAgMnPc1d1	městský
starší	starší	k1gMnPc1	starší
<g/>
,	,	kIx,	,
kterými	který	k3yRgMnPc7	který
byli	být	k5eAaImAgMnP	být
Caspar	Caspar	k1gMnSc1	Caspar
Marl	Marl	k1gMnSc1	Marl
a	a	k8xC	a
Jörg	Jörg	k1gMnSc1	Jörg
Schan	Schan	k1gMnSc1	Schan
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
ustanovení	ustanovení	k1gNnSc2	ustanovení
vykonávala	vykonávat	k5eAaImAgFnS	vykonávat
hrdelní	hrdelní	k2eAgNnSc4d1	hrdelní
právo	právo	k1gNnSc4	právo
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
panství	panství	k1gNnSc6	panství
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
tato	tento	k3xDgFnSc1	tento
instituce	instituce	k1gFnSc1	instituce
podléhala	podléhat	k5eAaImAgFnS	podléhat
jako	jako	k9	jako
další	další	k2eAgMnPc1d1	další
Gendorfovi	Gendorfův	k2eAgMnPc1d1	Gendorfův
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc4d1	další
privilegia	privilegium	k1gNnPc4	privilegium
získal	získat	k5eAaPmAgInS	získat
pro	pro	k7c4	pro
obce	obec	k1gFnPc4	obec
Lánov	Lánovo	k1gNnPc2	Lánovo
a	a	k8xC	a
Nová	nový	k2eAgFnSc1d1	nová
Ves	ves	k1gFnSc1	ves
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
obce	obec	k1gFnPc1	obec
pak	pak	k6eAd1	pak
mohly	moct	k5eAaImAgFnP	moct
vařit	vařit	k5eAaImF	vařit
a	a	k8xC	a
čepovat	čepovat	k5eAaImF	čepovat
pivo	pivo	k1gNnSc4	pivo
<g/>
,	,	kIx,	,
provozovat	provozovat	k5eAaImF	provozovat
řemesla	řemeslo	k1gNnPc4	řemeslo
a	a	k8xC	a
konat	konat	k5eAaImF	konat
týdenní	týdenní	k2eAgInSc4d1	týdenní
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
Doly	dol	k1gInPc1	dol
s	s	k7c7	s
hamry	hamr	k1gInPc7	hamr
stály	stát	k5eAaImAgInP	stát
na	na	k7c6	na
panství	panství	k1gNnSc6	panství
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
nejpozději	pozdě	k6eAd3	pozdě
od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
půlky	půlka	k1gFnSc2	půlka
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rozvoj	rozvoj	k1gInSc4	rozvoj
tohoto	tento	k3xDgNnSc2	tento
odvětví	odvětví	k1gNnSc2	odvětví
průmyslu	průmysl	k1gInSc2	průmysl
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
Jan	Jan	k1gMnSc1	Jan
Tetaur	Tetaur	k1gMnSc1	Tetaur
z	z	k7c2	z
Tetova	Tetov	k1gInSc2	Tetov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
chyběly	chybět	k5eAaImAgInP	chybět
finance	finance	k1gFnPc4	finance
<g/>
.	.	kIx.	.
</s>
<s>
Finance	finance	k1gFnPc1	finance
se	se	k3xPyFc4	se
však	však	k9	však
dostávaly	dostávat	k5eAaImAgFnP	dostávat
Kryštofu	Kryštof	k1gMnSc3	Kryštof
Gendorfovi	Gendorf	k1gMnSc3	Gendorf
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
hornictví	hornictví	k1gNnPc1	hornictví
na	na	k7c6	na
vrchlabském	vrchlabský	k2eAgNnSc6d1	vrchlabské
panství	panství	k1gNnSc6	panství
dařilo	dařit	k5eAaImAgNnS	dařit
dobře	dobře	k6eAd1	dobře
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
Gendorf	Gendorf	k1gMnSc1	Gendorf
získal	získat	k5eAaPmAgMnS	získat
od	od	k7c2	od
krále	král	k1gMnSc2	král
před	před	k7c7	před
koupí	koupě	k1gFnSc7	koupě
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
i	i	k9	i
horní	horní	k2eAgNnSc1d1	horní
privilegium	privilegium	k1gNnSc1	privilegium
na	na	k7c4	na
Krkonoše	Krkonoše	k1gFnPc4	Krkonoše
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc4	jejich
podhůří	podhůří	k1gNnPc4	podhůří
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
vlastně	vlastně	k9	vlastně
získal	získat	k5eAaPmAgMnS	získat
volnou	volný	k2eAgFnSc4d1	volná
ruku	ruka	k1gFnSc4	ruka
v	v	k7c6	v
horním	horní	k2eAgInSc6d1	horní
a	a	k8xC	a
hutnickém	hutnický	k2eAgNnSc6d1	hutnické
podnikání	podnikání	k1gNnSc6	podnikání
na	na	k7c6	na
všech	všecek	k3xTgNnPc6	všecek
panstvích	panství	k1gNnPc6	panství
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
si	se	k3xPyFc3	se
koupí	koupit	k5eAaPmIp3nP	koupit
<g/>
.	.	kIx.	.
</s>
<s>
Dostal	dostat	k5eAaPmAgMnS	dostat
možnost	možnost	k1gFnSc4	možnost
zakládání	zakládání	k1gNnSc2	zakládání
hornických	hornický	k2eAgNnPc2d1	Hornické
měst	město	k1gNnPc2	město
a	a	k8xC	a
osad	osada	k1gFnPc2	osada
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
tato	tento	k3xDgNnPc1	tento
privilegia	privilegium	k1gNnPc1	privilegium
byla	být	k5eAaImAgNnP	být
časem	časem	k6eAd1	časem
rozšiřována	rozšiřován	k2eAgNnPc1d1	rozšiřováno
<g/>
.	.	kIx.	.
</s>
<s>
Gendorf	Gendorf	k1gMnSc1	Gendorf
koupil	koupit	k5eAaPmAgMnS	koupit
nejen	nejen	k6eAd1	nejen
Vrchlabí	Vrchlabí	k1gNnSc4	Vrchlabí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
další	další	k2eAgFnPc4d1	další
vesnice	vesnice	k1gFnPc4	vesnice
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1534	[number]	k4	1534
získal	získat	k5eAaPmAgMnS	získat
přilehlé	přilehlý	k2eAgFnPc4d1	přilehlá
Kunčice	Kunčice	k1gFnPc4	Kunčice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgInSc6	ten
samém	samý	k3xTgInSc6	samý
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
do	do	k7c2	do
jeho	jeho	k3xOp3gFnPc2	jeho
rukou	ruka	k1gFnPc2	ruka
dostává	dostávat	k5eAaImIp3nS	dostávat
panství	panství	k1gNnSc1	panství
Trutnov	Trutnov	k1gInSc1	Trutnov
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1541	[number]	k4	1541
Žacléř	Žacléř	k1gInSc4	Žacléř
a	a	k8xC	a
okolí	okolí	k1gNnSc4	okolí
s	s	k7c7	s
tamějšími	tamější	k2eAgInPc7d1	tamější
zlatými	zlatý	k2eAgInPc7d1	zlatý
doly	dol	k1gInPc7	dol
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
velké	velký	k2eAgNnSc1d1	velké
úsilí	úsilí	k1gNnSc1	úsilí
nakonec	nakonec	k6eAd1	nakonec
vrcholí	vrcholit	k5eAaImIp3nS	vrcholit
roku	rok	k1gInSc2	rok
1547	[number]	k4	1547
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
do	do	k7c2	do
jeho	jeho	k3xOp3gNnSc2	jeho
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
dostává	dostávat	k5eAaImIp3nS	dostávat
samotné	samotný	k2eAgNnSc1d1	samotné
město	město	k1gNnSc1	město
Trutnov	Trutnov	k1gInSc1	Trutnov
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
měl	mít	k5eAaImAgMnS	mít
Kryštof	Kryštof	k1gMnSc1	Kryštof
z	z	k7c2	z
Gendorfu	Gendorf	k1gInSc2	Gendorf
postavit	postavit	k5eAaPmF	postavit
největší	veliký	k2eAgFnPc1d3	veliký
železárny	železárna	k1gFnPc1	železárna
v	v	k7c6	v
celých	celý	k2eAgFnPc6d1	celá
tehdejších	tehdejší	k2eAgFnPc6d1	tehdejší
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
poloha	poloha	k1gFnSc1	poloha
není	být	k5eNaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Louda	Louda	k1gMnSc1	Louda
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
"	"	kIx"	"
<g/>
Zmizelé	zmizelý	k2eAgNnSc1d1	zmizelé
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
<g/>
"	"	kIx"	"
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
pojmenování	pojmenování	k1gNnSc2	pojmenování
Schmalzgruben	Schmalzgruben	k2eAgMnSc1d1	Schmalzgruben
a	a	k8xC	a
Hammerberg	Hammerberg	k1gMnSc1	Hammerberg
nejspíše	nejspíše	k9	nejspíše
stály	stát	k5eAaImAgFnP	stát
v	v	k7c6	v
Hořením	hoření	k2eAgNnSc6d1	hoření
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
mezi	mezi	k7c7	mezi
odbočkami	odbočka	k1gFnPc7	odbočka
na	na	k7c4	na
Benecko	Benecko	k1gNnSc4	Benecko
a	a	k8xC	a
Strážné	strážná	k1gFnPc4	strážná
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
plechů	plech	k1gInPc2	plech
a	a	k8xC	a
"	"	kIx"	"
<g/>
obyčejného	obyčejný	k2eAgMnSc2d1	obyčejný
<g/>
"	"	kIx"	"
železa	železo	k1gNnSc2	železo
se	se	k3xPyFc4	se
v	v	k7c6	v
železárnách	železárna	k1gFnPc6	železárna
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
vrchlabská	vrchlabský	k2eAgFnSc1d1	vrchlabská
specialita	specialita	k1gFnSc1	specialita
-	-	kIx~	-
kosy	kosa	k1gFnPc1	kosa
<g/>
.	.	kIx.	.
</s>
<s>
Gendorfovi	Gendorf	k1gMnSc3	Gendorf
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
podařilo	podařit	k5eAaPmAgNnS	podařit
ovládnout	ovládnout	k5eAaPmF	ovládnout
český	český	k2eAgInSc4d1	český
trh	trh	k1gInSc4	trh
s	s	k7c7	s
výrobky	výrobek	k1gInPc7	výrobek
z	z	k7c2	z
kovu	kov	k1gInSc2	kov
právě	právě	k6eAd1	právě
těmito	tento	k3xDgFnPc7	tento
kosami	kosa	k1gFnPc7	kosa
<g/>
.	.	kIx.	.
</s>
<s>
Kosy	kosa	k1gFnPc1	kosa
se	se	k3xPyFc4	se
v	v	k7c6	v
místních	místní	k2eAgFnPc6d1	místní
železárnách	železárna	k1gFnPc6	železárna
nevyráběly	vyrábět	k5eNaImAgFnP	vyrábět
jen	jen	k6eAd1	jen
pro	pro	k7c4	pro
import	import	k1gInSc4	import
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
pro	pro	k7c4	pro
export	export	k1gInSc4	export
<g/>
.	.	kIx.	.
</s>
<s>
Kosy	kosa	k1gFnPc1	kosa
z	z	k7c2	z
Gendorfovy	Gendorfův	k2eAgFnSc2d1	Gendorfův
hutě	huť	k1gFnSc2	huť
byly	být	k5eAaImAgInP	být
dodávané	dodávaný	k2eAgInPc1d1	dodávaný
do	do	k7c2	do
Vratislavi	Vratislav	k1gFnSc2	Vratislav
či	či	k8xC	či
do	do	k7c2	do
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1552	[number]	k4	1552
je	být	k5eAaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
hutě	huť	k1gFnPc1	huť
ročně	ročně	k6eAd1	ročně
produkovaly	produkovat	k5eAaImAgFnP	produkovat
neuvěřitelných	uvěřitelný	k2eNgNnPc2d1	neuvěřitelné
13	[number]	k4	13
500	[number]	k4	500
kos	kosa	k1gFnPc2	kosa
<g/>
.	.	kIx.	.
</s>
<s>
Gendorf	Gendorf	k1gInSc1	Gendorf
dal	dát	k5eAaPmAgInS	dát
také	také	k9	také
městu	město	k1gNnSc3	město
nové	nový	k2eAgNnSc4d1	nové
německé	německý	k2eAgNnSc4d1	německé
jméno	jméno	k1gNnSc4	jméno
-	-	kIx~	-
dodnes	dodnes	k6eAd1	dodnes
používané	používaný	k2eAgInPc1d1	používaný
Hohenelbe	Hohenelb	k1gInSc5	Hohenelb
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jsou	být	k5eAaImIp3nP	být
doloženy	doložen	k2eAgInPc1d1	doložen
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
první	první	k4xOgInPc4	první
cechy	cech	k1gInPc4	cech
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
byly	být	k5eAaImAgInP	být
cechy	cech	k1gInPc1	cech
soukeníků	soukeník	k1gMnPc2	soukeník
a	a	k8xC	a
pekařů	pekař	k1gMnPc2	pekař
(	(	kIx(	(
<g/>
1554	[number]	k4	1554
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
cechy	cech	k1gInPc4	cech
kožešníků	kožešník	k1gMnPc2	kožešník
(	(	kIx(	(
<g/>
1558	[number]	k4	1558
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obuvníků	obuvník	k1gMnPc2	obuvník
(	(	kIx(	(
<g/>
1559	[number]	k4	1559
<g/>
)	)	kIx)	)
aj.	aj.	kA	aj.
Mnoho	mnoho	k4c1	mnoho
důležitých	důležitý	k2eAgNnPc2d1	důležité
řemesel	řemeslo	k1gNnPc2	řemeslo
však	však	k9	však
muselo	muset	k5eAaImAgNnS	muset
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
cech	cech	k1gInSc4	cech
počkat	počkat	k5eAaPmF	počkat
do	do	k7c2	do
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Městečko	městečko	k1gNnSc1	městečko
roku	rok	k1gInSc2	rok
1546	[number]	k4	1546
mělo	mít	k5eAaImAgNnS	mít
40	[number]	k4	40
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc4	kostel
<g/>
,	,	kIx,	,
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
pivovar	pivovar	k1gInSc1	pivovar
<g/>
,	,	kIx,	,
chorobinec	chorobinec	k1gInSc1	chorobinec
a	a	k8xC	a
lázeň	lázeň	k1gFnSc1	lázeň
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
byla	být	k5eAaImAgFnS	být
dokončena	dokončen	k2eAgFnSc1d1	dokončena
část	část	k1gFnSc1	část
zámku	zámek	k1gInSc2	zámek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
nechal	nechat	k5eAaPmAgMnS	nechat
majitel	majitel	k1gMnSc1	majitel
panství	panství	k1gNnSc4	panství
postavit	postavit	k5eAaPmF	postavit
vedle	vedle	k7c2	vedle
nevyhovující	vyhovující	k2eNgFnSc2d1	nevyhovující
dřevěné	dřevěný	k2eAgFnSc2d1	dřevěná
tvrze	tvrz	k1gFnSc2	tvrz
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Gendorfa	Gendorf	k1gMnSc2	Gendorf
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
obnovily	obnovit	k5eAaPmAgInP	obnovit
staré	starý	k2eAgInPc1d1	starý
hraniční	hraniční	k2eAgInPc1d1	hraniční
spory	spor	k1gInPc1	spor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
si	se	k3xPyFc3	se
vysloužily	vysloužit	k5eAaPmAgFnP	vysloužit
přívlastek	přívlastek	k1gInSc4	přívlastek
"	"	kIx"	"
<g/>
vrchlabské	vrchlabský	k2eAgFnSc2d1	vrchlabská
války	válka	k1gFnSc2	válka
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
poprvé	poprvé	k6eAd1	poprvé
použil	použít	k5eAaPmAgMnS	použít
August	August	k1gMnSc1	August
Sedláček	Sedláček	k1gMnSc1	Sedláček
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozhořely	rozhořet	k5eAaPmAgFnP	rozhořet
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
dolům	dol	k1gInPc3	dol
mezi	mezi	k7c7	mezi
Černým	černý	k2eAgInSc7d1	černý
Dolem	dol	k1gInSc7	dol
a	a	k8xC	a
Dolním	dolní	k2eAgInSc7d1	dolní
Dvorem	Dvůr	k1gInSc7	Dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Těm	ten	k3xDgMnPc3	ten
se	se	k3xPyFc4	se
říkalo	říkat	k5eAaImAgNnS	říkat
"	"	kIx"	"
<g/>
Lánovské	Lánovský	k2eAgFnPc4d1	Lánovský
hory	hora	k1gFnPc4	hora
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
vrchlabským	vrchlabský	k2eAgNnSc7d1	vrchlabské
a	a	k8xC	a
hostinským	hostinský	k2eAgNnSc7d1	hostinské
panstvím	panství	k1gNnSc7	panství
tam	tam	k6eAd1	tam
byla	být	k5eAaImAgFnS	být
nejistá	jistý	k2eNgFnSc1d1	nejistá
<g/>
.	.	kIx.	.
</s>
<s>
Lánov	Lánov	k1gInSc1	Lánov
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
mezi	mezi	k7c4	mezi
tato	tento	k3xDgNnPc4	tento
dvě	dva	k4xCgNnPc4	dva
panství	panství	k1gNnPc4	panství
rozdělený	rozdělený	k2eAgInSc1d1	rozdělený
<g/>
.	.	kIx.	.
</s>
<s>
Spor	spor	k1gInSc1	spor
se	se	k3xPyFc4	se
také	také	k6eAd1	také
týkal	týkat	k5eAaImAgInS	týkat
tzv.	tzv.	kA	tzv.
lánovských	lánovský	k2eAgInPc2d1	lánovský
dolů	dol	k1gInPc2	dol
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgInP	mít
nacházet	nacházet	k5eAaImF	nacházet
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Rudník	rudník	k1gMnSc1	rudník
<g/>
–	–	k?	–
<g/>
Javorník	Javorník	k1gInSc1	Javorník
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
spor	spor	k1gInSc1	spor
sice	sice	k8xC	sice
existoval	existovat	k5eAaImAgInS	existovat
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
když	když	k8xS	když
vrchlabské	vrchlabský	k2eAgNnSc4d1	vrchlabské
panství	panství	k1gNnSc4	panství
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
Jan	Jan	k1gMnSc1	Jan
Tetaur	Tetaur	k1gMnSc1	Tetaur
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
odkoupení	odkoupení	k1gNnSc6	odkoupení
panství	panství	k1gNnSc2	panství
Hostinné	hostinný	k2eAgNnSc4d1	Hostinné
od	od	k7c2	od
Voršily	Voršila	k1gFnSc2	Voršila
z	z	k7c2	z
Vartenberka	Vartenberka	k1gFnSc1	Vartenberka
Valdštejny	Valdštejna	k1gFnSc2	Valdštejna
byl	být	k5eAaImAgMnS	být
stále	stále	k6eAd1	stále
aktuální	aktuální	k2eAgInSc4d1	aktuální
a	a	k8xC	a
nevymizel	vymizet	k5eNaPmAgInS	vymizet
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
z	z	k7c2	z
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
své	svůj	k3xOyFgNnSc4	svůj
právo	právo	k1gNnSc4	právo
hájil	hájit	k5eAaImAgInS	hájit
kradením	kradení	k1gNnSc7	kradení
Gendorfova	Gendorfův	k2eAgNnSc2d1	Gendorfův
obilí	obilí	k1gNnSc2	obilí
či	či	k8xC	či
dobytka	dobytek	k1gInSc2	dobytek
<g/>
.	.	kIx.	.
</s>
<s>
Též	též	k6eAd1	též
bral	brát	k5eAaImAgMnS	brát
do	do	k7c2	do
zajetí	zajetí	k1gNnSc2	zajetí
Gendorfovy	Gendorfův	k2eAgMnPc4d1	Gendorfův
dřevaře	dřevař	k1gMnPc4	dřevař
<g/>
.	.	kIx.	.
</s>
<s>
Všechno	všechen	k3xTgNnSc1	všechen
toto	tento	k3xDgNnSc1	tento
drobné	drobný	k2eAgNnSc1d1	drobné
záškodnictví	záškodnictví	k1gNnSc1	záškodnictví
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS	vyvrcholit
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
když	když	k8xS	když
Valdštejnovi	Valdštejnův	k2eAgMnPc1d1	Valdštejnův
lidé	člověk	k1gMnPc1	člověk
na	na	k7c6	na
Gendorfa	Gendorf	k1gMnSc2	Gendorf
stříleli	střílet	k5eAaImAgMnP	střílet
<g/>
.	.	kIx.	.
</s>
<s>
Nepomohl	pomoct	k5eNaPmAgMnS	pomoct
však	však	k9	však
ani	ani	k8xC	ani
soud	soud	k1gInSc4	soud
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1541	[number]	k4	1541
vtrhl	vtrhnout	k5eAaPmAgMnS	vtrhnout
hostinský	hostinský	k1gMnSc1	hostinský
pán	pán	k1gMnSc1	pán
na	na	k7c4	na
Gendorfovo	Gendorfův	k2eAgNnSc4d1	Gendorfův
zboží	zboží	k1gNnSc4	zboží
s	s	k7c7	s
několika	několik	k4yIc7	několik
sty	sto	k4xCgNnPc7	sto
dalšími	další	k2eAgMnPc7d1	další
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Zničil	zničit	k5eAaPmAgMnS	zničit
záseky	záseka	k1gFnPc4	záseka
na	na	k7c6	na
Gendorfových	Gendorfův	k2eAgFnPc6d1	Gendorfův
cestách	cesta	k1gFnPc6	cesta
<g/>
,	,	kIx,	,
poničil	poničit	k5eAaPmAgInS	poničit
důlní	důlní	k2eAgNnSc4d1	důlní
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
zničil	zničit	k5eAaPmAgMnS	zničit
hamry	hamr	k1gInPc4	hamr
a	a	k8xC	a
všechny	všechen	k3xTgMnPc4	všechen
Gendorfovy	Gendorfův	k2eAgMnPc4d1	Gendorfův
pracovníky	pracovník	k1gMnPc4	pracovník
rozprášil	rozprášit	k5eAaPmAgInS	rozprášit
<g/>
.	.	kIx.	.
</s>
<s>
Zemský	zemský	k2eAgInSc1d1	zemský
soud	soud	k1gInSc1	soud
zase	zase	k9	zase
zakročil	zakročit	k5eAaPmAgInS	zakročit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výsledek	výsledek	k1gInSc1	výsledek
byl	být	k5eAaImAgInS	být
nulový	nulový	k2eAgMnSc1d1	nulový
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
začali	začít	k5eAaPmAgMnP	začít
příbuzní	příbuzný	k2eAgMnPc1d1	příbuzný
Valdštejnové	Valdštejn	k1gMnPc1	Valdštejn
dokonce	dokonce	k9	dokonce
střídat	střídat	k5eAaImF	střídat
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k6eAd1	jen
co	co	k3yRnSc4	co
přestal	přestat	k5eAaPmAgInS	přestat
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
dorážet	dorážet	k5eAaImF	dorážet
Vilém	Vilém	k1gMnSc1	Vilém
z	z	k7c2	z
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
panství	panství	k1gNnSc4	panství
ve	v	k7c6	v
Štěpanicích	Štěpanice	k1gFnPc6	Štěpanice
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
sousedilo	sousedit	k5eAaImAgNnS	sousedit
z	z	k7c2	z
vrchlabským	vrchlabský	k2eAgNnPc3d1	vrchlabské
panstvím	panství	k1gNnPc3	panství
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1541	[number]	k4	1541
si	se	k3xPyFc3	se
tento	tento	k3xDgInSc4	tento
pán	pán	k1gMnSc1	pán
stěžoval	stěžovat	k5eAaImAgMnS	stěžovat
královskému	královský	k2eAgMnSc3d1	královský
hofmistrovi	hofmistr	k1gMnSc3	hofmistr
na	na	k7c4	na
Gendorfa	Gendorf	k1gMnSc4	Gendorf
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
prý	prý	k9	prý
škodí	škodit	k5eAaImIp3nS	škodit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
plavené	plavený	k2eAgNnSc4d1	plavené
dřevo	dřevo	k1gNnSc4	dřevo
mu	on	k3xPp3gMnSc3	on
vybíjí	vybíjet	k5eAaImIp3nS	vybíjet
ryby	ryba	k1gFnPc4	ryba
a	a	k8xC	a
ničí	ničit	k5eAaImIp3nS	ničit
jeho	jeho	k3xOp3gFnSc4	jeho
část	část	k1gFnSc4	část
Labe	Labe	k1gNnSc2	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Valdštejni	Valdštejn	k1gMnPc1	Valdštejn
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
s	s	k7c7	s
Gendorfem	Gendorf	k1gInSc7	Gendorf
naoko	naoko	k6eAd1	naoko
usmířili	usmířit	k5eAaPmAgMnP	usmířit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pak	pak	k6eAd1	pak
využili	využít	k5eAaPmAgMnP	využít
Gendorfovy	Gendorfův	k2eAgFnPc4d1	Gendorfův
nepřítomnosti	nepřítomnost	k1gFnPc4	nepřítomnost
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1547	[number]	k4	1547
obsadili	obsadit	k5eAaPmAgMnP	obsadit
jeho	jeho	k3xOp3gInPc4	jeho
doly	dol	k1gInPc4	dol
na	na	k7c4	na
železo	železo	k1gNnSc4	železo
na	na	k7c6	na
Staré	Staré	k2eAgFnSc6d1	Staré
hoře	hora	k1gFnSc6	hora
u	u	k7c2	u
Herlíkovic	Herlíkovice	k1gFnPc2	Herlíkovice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
tam	tam	k6eAd1	tam
dodnes	dodnes	k6eAd1	dodnes
připomínají	připomínat	k5eAaImIp3nP	připomínat
tzv.	tzv.	kA	tzv.
Herlíkovické	Herlíkovický	k2eAgFnSc2d1	Herlíkovická
štoly	štola	k1gFnSc2	štola
po	po	k7c6	po
levé	levý	k2eAgFnSc6d1	levá
straně	strana	k1gFnSc6	strana
Labe	Labe	k1gNnSc2	Labe
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
na	na	k7c6	na
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Strážného	strážný	k1gMnSc2	strážný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
ochromili	ochromit	k5eAaPmAgMnP	ochromit
Gendorfovo	Gendorfův	k2eAgNnSc4d1	Gendorfův
železářství	železářství	k1gNnSc4	železářství
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc6	konec
to	ten	k3xDgNnSc1	ten
dopadalo	dopadat	k5eAaImAgNnS	dopadat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
měli	mít	k5eAaImAgMnP	mít
strach	strach	k1gInSc4	strach
i	i	k9	i
o	o	k7c4	o
holý	holý	k2eAgInSc4d1	holý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
prchli	prchnout	k5eAaPmAgMnP	prchnout
a	a	k8xC	a
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
byla	být	k5eAaImAgFnS	být
i	i	k9	i
Gendorfova	Gendorfův	k2eAgFnSc1d1	Gendorfův
žena	žena	k1gFnSc1	žena
Magdaléna	Magdaléna	k1gFnSc1	Magdaléna
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
některých	některý	k3yIgFnPc2	některý
Margareta	Margareta	k1gFnSc1	Margareta
-	-	kIx~	-
Markéta	Markéta	k1gFnSc1	Markéta
<g/>
)	)	kIx)	)
s	s	k7c7	s
dcerami	dcera	k1gFnPc7	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
delší	dlouhý	k2eAgFnSc6d2	delší
přestávce	přestávka	k1gFnSc6	přestávka
v	v	k7c6	v
boji	boj	k1gInSc6	boj
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
válka	válka	k1gFnSc1	válka
<g/>
"	"	kIx"	"
znovu	znovu	k6eAd1	znovu
rozhořela	rozhořet	k5eAaPmAgFnS	rozhořet
roku	rok	k1gInSc2	rok
1551	[number]	k4	1551
<g/>
.	.	kIx.	.
</s>
<s>
Gendorf	Gendorf	k1gMnSc1	Gendorf
zajal	zajmout	k5eAaPmAgMnS	zajmout
několik	několik	k4yIc4	několik
Vilémových	Vilémův	k2eAgMnPc2d1	Vilémův
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
odvedl	odvést	k5eAaPmAgMnS	odvést
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
<g/>
.	.	kIx.	.
</s>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
mu	on	k3xPp3gMnSc3	on
na	na	k7c4	na
oplátku	oplátka	k1gFnSc4	oplátka
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
zničil	zničit	k5eAaPmAgMnS	zničit
milíře	milíř	k1gInSc2	milíř
v	v	k7c6	v
Herlíkovicích	Herlíkovice	k1gFnPc6	Herlíkovice
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pak	pak	k6eAd1	pak
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
plavil	plavit	k5eAaImAgMnS	plavit
Gendorf	Gendorf	k1gInSc4	Gendorf
dřevo	dřevo	k1gNnSc1	dřevo
po	po	k7c6	po
Labi	Labe	k1gNnSc6	Labe
<g/>
,	,	kIx,	,
Vilém	Vilém	k1gMnSc1	Vilém
mu	on	k3xPp3gMnSc3	on
ho	on	k3xPp3gNnSc4	on
nad	nad	k7c7	nad
Vrchlabím	Vrchlabí	k1gNnSc7	Vrchlabí
zatarasil	zatarasit	k5eAaPmAgMnS	zatarasit
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
odchytil	odchytit	k5eAaPmAgInS	odchytit
všechno	všechen	k3xTgNnSc4	všechen
plavené	plavený	k2eAgNnSc4d1	plavené
dřevo	dřevo	k1gNnSc4	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
krokem	krok	k1gInSc7	krok
mu	on	k3xPp3gMnSc3	on
Vilém	Vilém	k1gMnSc1	Vilém
omezil	omezit	k5eAaPmAgInS	omezit
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
oněch	onen	k3xDgFnPc6	onen
proslulých	proslulý	k2eAgFnPc6d1	proslulá
železárnách	železárna	k1gFnPc6	železárna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
například	například	k6eAd1	například
omezil	omezit	k5eAaPmAgInS	omezit
provoz	provoz	k1gInSc1	provoz
i	i	k8xC	i
Gendorfova	Gendorfův	k2eAgInSc2d1	Gendorfův
pivovaru	pivovar	k1gInSc2	pivovar
<g/>
.	.	kIx.	.
</s>
<s>
Vilémovi	Vilémův	k2eAgMnPc1d1	Vilémův
lidé	člověk	k1gMnPc1	člověk
Gendorfovi	Gendorfův	k2eAgMnPc1d1	Gendorfův
ještě	ještě	k6eAd1	ještě
navrch	navrch	k6eAd1	navrch
zatopili	zatopit	k5eAaPmAgMnP	zatopit
jeho	jeho	k3xOp3gInSc4	jeho
důl	důl	k1gInSc4	důl
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
kroky	krok	k1gInPc1	krok
provedené	provedený	k2eAgFnSc2d1	provedená
Valdštejny	Valdštejna	k1gFnSc2	Valdštejna
proti	proti	k7c3	proti
Gendorfovi	Gendorf	k1gMnSc3	Gendorf
měly	mít	k5eAaImAgInP	mít
silný	silný	k2eAgInSc4d1	silný
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
město	město	k1gNnSc4	město
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
purkmistrem	purkmistr	k1gMnSc7	purkmistr
a	a	k8xC	a
Gendorfovi	Gendorfův	k2eAgMnPc1d1	Gendorfův
pracovníci	pracovník	k1gMnPc1	pracovník
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgMnSc4	ten
pak	pak	k6eAd1	pak
poslali	poslat	k5eAaPmAgMnP	poslat
králi	král	k1gMnSc3	král
petici	petice	k1gFnSc4	petice
proti	proti	k7c3	proti
Vilémovi	Vilém	k1gMnSc3	Vilém
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
všechno	všechen	k3xTgNnSc1	všechen
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
k	k	k7c3	k
soudu	soud	k1gInSc3	soud
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgInS	dát
Gendorfovi	Gendorf	k1gMnSc3	Gendorf
za	za	k7c4	za
pravdu	pravda	k1gFnSc4	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
nepřátelské	přátelský	k2eNgInPc1d1	nepřátelský
vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgMnPc7	dva
šlechtici	šlechtic	k1gMnPc7	šlechtic
trvaly	trvat	k5eAaImAgInP	trvat
ještě	ještě	k9	ještě
dlouho	dlouho	k6eAd1	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
Kryštof	Kryštof	k1gMnSc1	Kryštof
z	z	k7c2	z
Gendorfu	Gendorf	k1gInSc2	Gendorf
<g/>
,	,	kIx,	,
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
z	z	k7c2	z
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
udělal	udělat	k5eAaPmAgInS	udělat
něco	něco	k3yInSc4	něco
víc	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
obyčejnou	obyčejný	k2eAgFnSc4d1	obyčejná
vesnici	vesnice	k1gFnSc4	vesnice
<g/>
,	,	kIx,	,
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
také	také	k9	také
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
66	[number]	k4	66
let	léto	k1gNnPc2	léto
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1563	[number]	k4	1563
zemřel	zemřít	k5eAaPmAgInS	zemřít
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
týž	týž	k3xTgInSc4	týž
rok	rok	k1gInSc4	rok
tam	tam	k6eAd1	tam
byl	být	k5eAaImAgMnS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
prakticky	prakticky	k6eAd1	prakticky
bez	bez	k7c2	bez
mužských	mužský	k2eAgMnPc2d1	mužský
potomků	potomek	k1gMnPc2	potomek
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
již	již	k6eAd1	již
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1561	[number]	k4	1561
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jí	jíst	k5eAaImIp3nS	jíst
ho	on	k3xPp3gMnSc4	on
Kryštof	Kryštof	k1gMnSc1	Kryštof
prodal	prodat	k5eAaPmAgMnS	prodat
<g/>
)	)	kIx)	)
jeho	jeho	k3xOp3gFnSc1	jeho
dcera	dcera	k1gFnSc1	dcera
Eustachie	Eustachi	k1gMnPc4	Eustachi
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgNnP	být
již	již	k6eAd1	již
vdovou	vdova	k1gFnSc7	vdova
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
zasloužila	zasloužit	k5eAaPmAgFnS	zasloužit
o	o	k7c6	o
povýšení	povýšení	k1gNnSc6	povýšení
jiného	jiný	k2eAgNnSc2d1	jiné
sídla	sídlo	k1gNnSc2	sídlo
v	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
-	-	kIx~	-
Černého	Černého	k2eAgInSc2d1	Černého
Dolu	dol	k1gInSc2	dol
-	-	kIx~	-
ze	z	k7c2	z
vsi	ves	k1gFnSc2	ves
na	na	k7c4	na
městečko	městečko	k1gNnSc4	městečko
<g/>
.	.	kIx.	.
</s>
<s>
Gendorfovi	Gendorf	k1gMnSc3	Gendorf
se	se	k3xPyFc4	se
narodilo	narodit	k5eAaPmAgNnS	narodit
šest	šest	k4xCc1	šest
dcer	dcera	k1gFnPc2	dcera
(	(	kIx(	(
<g/>
Pavla	Pavel	k1gMnSc4	Pavel
<g/>
,	,	kIx,	,
Eustachie	Eustachi	k1gMnPc4	Eustachi
<g/>
,	,	kIx,	,
Benigna	Benigna	k1gFnSc1	Benigna
<g/>
,	,	kIx,	,
Kordula	Kordula	k1gFnSc1	Kordula
<g/>
,	,	kIx,	,
Lukrecie	Lukrecie	k1gFnSc1	Lukrecie
a	a	k8xC	a
Eleonora	Eleonora	k1gFnSc1	Eleonora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
čtyři	čtyři	k4xCgInPc4	čtyři
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
v	v	k7c6	v
Podkrkonoší	Podkrkonoší	k1gNnSc6	Podkrkonoší
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
Pavla	Pavla	k1gFnSc1	Pavla
si	se	k3xPyFc3	se
vzala	vzít	k5eAaPmAgFnS	vzít
jistého	jistý	k2eAgMnSc4d1	jistý
Jana	Jan	k1gMnSc4	Jan
Cetritze	Cetritze	k1gFnSc2	Cetritze
z	z	k7c2	z
Karische	Karisch	k1gInSc2	Karisch
na	na	k7c6	na
Fuchsberku	Fuchsberk	k1gInSc6	Fuchsberk
(	(	kIx(	(
<g/>
Liščím	liščí	k2eAgInSc6d1	liščí
kopci	kopec	k1gInSc6	kopec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
opravdu	opravdu	k6eAd1	opravdu
doložena	doložit	k5eAaPmNgFnS	doložit
tvrz	tvrz	k1gFnSc1	tvrz
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
část	část	k1gFnSc1	část
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
se	s	k7c7	s
sídlištěm	sídliště	k1gNnSc7	sídliště
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Eustachie	Eustachi	k1gMnPc4	Eustachi
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ovdověla	ovdovět	k5eAaPmAgFnS	ovdovět
roku	rok	k1gInSc2	rok
1555	[number]	k4	1555
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
tři	tři	k4xCgFnPc4	tři
dcery	dcera	k1gFnPc4	dcera
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yRgFnPc4	který
se	se	k3xPyFc4	se
po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
úmrtí	úmrtí	k1gNnSc6	úmrtí
roku	rok	k1gInSc2	rok
1568	[number]	k4	1568
vrchlabské	vrchlabský	k2eAgNnSc1d1	vrchlabské
panství	panství	k1gNnSc1	panství
rozdělilo	rozdělit	k5eAaPmAgNnS	rozdělit
<g/>
.	.	kIx.	.
</s>
<s>
Pány	pan	k1gMnPc4	pan
na	na	k7c6	na
vrchlabském	vrchlabský	k2eAgNnSc6d1	vrchlabské
panství	panství	k1gNnSc6	panství
tak	tak	k6eAd1	tak
byli	být	k5eAaImAgMnP	být
Míčanové	Míčan	k1gMnPc1	Míčan
z	z	k7c2	z
Klinštejna	Klinštejno	k1gNnSc2	Klinštejno
<g/>
,	,	kIx,	,
Markvartové	Markvartová	k1gFnSc2	Markvartová
z	z	k7c2	z
Hrádku	Hrádok	k1gInSc2	Hrádok
a	a	k8xC	a
Miřkovští	Miřkovský	k2eAgMnPc1d1	Miřkovský
ze	z	k7c2	z
Stropčic	Stropčice	k1gFnPc2	Stropčice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
dalším	další	k2eAgNnSc7d1	další
důležitým	důležitý	k2eAgNnSc7d1	důležité
průmyslovým	průmyslový	k2eAgNnSc7d1	průmyslové
odvětvím	odvětví	k1gNnSc7	odvětví
pro	pro	k7c4	pro
Vrchlabí	Vrchlabí	k1gNnSc4	Vrchlabí
stávalo	stávat	k5eAaImAgNnS	stávat
tkaní	tkaní	k1gNnSc1	tkaní
lnu	len	k1gInSc2	len
<g/>
.	.	kIx.	.
</s>
<s>
Tkalcovský	tkalcovský	k2eAgInSc1d1	tkalcovský
cech	cech	k1gInSc1	cech
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
založen	založen	k2eAgInSc1d1	založen
roku	rok	k1gInSc2	rok
1590	[number]	k4	1590
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
největšího	veliký	k2eAgInSc2d3	veliký
růstu	růst	k1gInSc2	růst
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1602	[number]	k4	1602
celé	celý	k2eAgNnSc4d1	celé
vrchlabské	vrchlabský	k2eAgNnSc4d1	vrchlabské
panství	panství	k1gNnSc4	panství
spojil	spojit	k5eAaPmAgMnS	spojit
Vilém	Vilém	k1gMnSc1	Vilém
Miřkovský	Miřkovský	k2eAgMnSc1d1	Miřkovský
z	z	k7c2	z
Tropčic	Tropčice	k1gFnPc2	Tropčice
ml	ml	kA	ml
<g/>
,	,	kIx,	,
manžel	manžel	k1gMnSc1	manžel
Veroniky	Veronika	k1gFnSc2	Veronika
Mráčské	Mráčský	k2eAgFnSc2d1	Mráčský
z	z	k7c2	z
Dubé	Dubá	k1gFnSc2	Dubá
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Rosiny	Rosin	k2eAgFnSc2d1	Rosina
Bockové	Bocková	k1gFnSc2	Bocková
z	z	k7c2	z
Hemsdorfu	Hemsdorf	k1gInSc2	Hemsdorf
na	na	k7c6	na
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
a	a	k8xC	a
Viléma	Vilém	k1gMnSc2	Vilém
Miřkovského	Miřkovský	k2eAgMnSc2d1	Miřkovský
z	z	k7c2	z
Tropčic	Tropčice	k1gFnPc2	Tropčice
(	(	kIx(	(
<g/>
též	též	k9	též
Vilém	Vilém	k1gMnSc1	Vilém
Miřkovský	Miřkovský	k2eAgMnSc1d1	Miřkovský
ze	z	k7c2	z
Stropčic	Stropčice	k1gFnPc2	Stropčice
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
1590	[number]	k4	1590
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byly	být	k5eAaImAgFnP	být
pověstné	pověstný	k2eAgFnPc1d1	pověstná
vrchlabské	vrchlabský	k2eAgFnPc1d1	vrchlabská
hutě	huť	k1gFnPc1	huť
částečně	částečně	k6eAd1	částečně
ve	v	k7c4	v
vlastnictví	vlastnictví	k1gNnSc4	vlastnictví
majitele	majitel	k1gMnSc2	majitel
panství	panství	k1gNnSc4	panství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byly	být	k5eAaImAgInP	být
i	i	k9	i
pronajaty	pronajmout	k5eAaPmNgInP	pronajmout
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
patřily	patřit	k5eAaImAgFnP	patřit
stále	stále	k6eAd1	stále
k	k	k7c3	k
těm	ten	k3xDgInPc3	ten
nejmodernějším	moderní	k2eAgInPc3d3	nejmodernější
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Kupříkladu	kupříkladu	k6eAd1	kupříkladu
jedny	jeden	k4xCgFnPc1	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
vysokých	vysoký	k2eAgFnPc2d1	vysoká
pecí	pec	k1gFnPc2	pec
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
Dolním	dolní	k2eAgInSc6d1	dolní
Dvoře	Dvůr	k1gInSc6	Dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
sortiment	sortiment	k1gInSc1	sortiment
pověstných	pověstný	k2eAgFnPc2d1	pověstná
vrchlabských	vrchlabský	k2eAgFnPc2d1	vrchlabská
železáren	železárna	k1gFnPc2	železárna
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
o	o	k7c4	o
kosy	kos	k1gMnPc4	kos
<g/>
,	,	kIx,	,
srpy	srp	k1gInPc1	srp
<g/>
,	,	kIx,	,
pily	pila	k1gFnPc1	pila
<g/>
,	,	kIx,	,
součástky	součástka	k1gFnPc1	součástka
pro	pro	k7c4	pro
větrné	větrný	k2eAgInPc4d1	větrný
mlýny	mlýn	k1gInPc4	mlýn
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
o	o	k7c4	o
hlavně	hlavně	k6eAd1	hlavně
a	a	k8xC	a
zámky	zámek	k1gInPc1	zámek
pušek	puška	k1gFnPc2	puška
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomuto	tento	k3xDgInSc3	tento
širokému	široký	k2eAgInSc3d1	široký
sortimentu	sortiment	k1gInSc3	sortiment
výrobků	výrobek	k1gInPc2	výrobek
se	se	k3xPyFc4	se
železářské	železářský	k2eAgInPc1d1	železářský
podniky	podnik	k1gInPc1	podnik
dostaly	dostat	k5eAaPmAgInP	dostat
až	až	k9	až
ke	k	k7c3	k
kostelu	kostel	k1gInSc3	kostel
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
protihabsburského	protihabsburský	k2eAgNnSc2d1	protihabsburské
povstání	povstání	k1gNnSc2	povstání
se	se	k3xPyFc4	se
evangelík	evangelík	k1gMnSc1	evangelík
Vilém	Vilém	k1gMnSc1	Vilém
nezapletl	zaplést	k5eNaPmAgMnS	zaplést
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
i	i	k9	i
po	po	k7c6	po
popravě	poprava	k1gFnSc6	poprava
vrcholných	vrcholný	k2eAgInPc2d1	vrcholný
vzbouřenců	vzbouřenec	k1gMnPc2	vzbouřenec
na	na	k7c6	na
Staroměstském	staroměstský	k2eAgNnSc6d1	Staroměstské
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
blízké	blízký	k2eAgFnSc2d1	blízká
Horní	horní	k2eAgFnSc2d1	horní
Branné	branný	k2eAgFnSc2d1	Branná
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
tehdejšímu	tehdejší	k2eAgMnSc3d1	tehdejší
majiteli	majitel	k1gMnSc3	majitel
zkonfiskována	zkonfiskován	k2eAgMnSc4d1	zkonfiskován
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Vrchlabské	vrchlabský	k2eAgNnSc4d1	vrchlabské
panství	panství	k1gNnSc4	panství
chtěl	chtít	k5eAaImAgMnS	chtít
ale	ale	k9	ale
získat	získat	k5eAaPmF	získat
šlechtic	šlechtic	k1gMnSc1	šlechtic
Albrecht	Albrecht	k1gMnSc1	Albrecht
z	z	k7c2	z
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
vnukem	vnuk	k1gMnSc7	vnuk
Gendorfova	Gendorfův	k2eAgMnSc4d1	Gendorfův
nepřítele	nepřítel	k1gMnSc4	nepřítel
Jiřího	Jiří	k1gMnSc4	Jiří
z	z	k7c2	z
Valdštejna	Valdštejno	k1gNnSc2	Valdštejno
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgInSc7	který
Kryštof	Kryštof	k1gMnSc1	Kryštof
vedl	vést	k5eAaImAgMnS	vést
vrchlabské	vrchlabský	k2eAgFnSc2d1	vrchlabská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1624	[number]	k4	1624
Vilém	Vilém	k1gMnSc1	Vilém
Miřkovský	Miřkovský	k2eAgMnSc1d1	Miřkovský
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
neměl	mít	k5eNaImAgMnS	mít
mužského	mužský	k2eAgMnSc4d1	mužský
dědice	dědic	k1gMnSc4	dědic
<g/>
,	,	kIx,	,
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
nátlaku	nátlak	k1gInSc3	nátlak
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
vyvíjel	vyvíjet	k5eAaImAgMnS	vyvíjet
"	"	kIx"	"
<g/>
šéf	šéf	k1gMnSc1	šéf
<g/>
"	"	kIx"	"
českých	český	k2eAgNnPc2d1	české
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
,	,	kIx,	,
generalisimus	generalisimus	k1gMnSc1	generalisimus
Albrecht	Albrecht	k1gMnSc1	Albrecht
z	z	k7c2	z
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
a	a	k8xC	a
celé	celý	k2eAgNnSc4d1	celé
panství	panství	k1gNnSc4	panství
i	i	k9	i
se	s	k7c7	s
zámkem	zámek	k1gInSc7	zámek
prodal	prodat	k5eAaPmAgMnS	prodat
za	za	k7c4	za
110	[number]	k4	110
000	[number]	k4	000
míšenských	míšenský	k2eAgFnPc2d1	Míšenská
kop	kopa	k1gFnPc2	kopa
<g/>
.	.	kIx.	.
</s>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
na	na	k7c6	na
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
1628	[number]	k4	1628
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
po	po	k7c6	po
prodeji	prodej	k1gInSc6	prodej
panství	panství	k1gNnSc1	panství
<g/>
.	.	kIx.	.
</s>
<s>
Pán	pán	k1gMnSc1	pán
z	z	k7c2	z
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
udělal	udělat	k5eAaPmAgInS	udělat
z	z	k7c2	z
vrchlabských	vrchlabský	k2eAgFnPc2d1	vrchlabská
železáren	železárna	k1gFnPc2	železárna
velkovýrobnu	velkovýrobna	k1gFnSc4	velkovýrobna
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
objevili	objevit	k5eAaPmAgMnP	objevit
specializovaní	specializovaný	k2eAgMnPc1d1	specializovaný
řemeslníci	řemeslník	k1gMnPc1	řemeslník
-	-	kIx~	-
kováři	kovář	k1gMnPc1	kovář
hlavní	hlavní	k2eAgMnPc1d1	hlavní
<g/>
,	,	kIx,	,
vrtači	vrtač	k1gMnPc1	vrtač
hlavní	hlavní	k2eAgFnSc2d1	hlavní
<g/>
,	,	kIx,	,
brusiči	brusič	k1gMnPc5	brusič
<g/>
,	,	kIx,	,
šroubaři	šroubař	k1gMnPc5	šroubař
<g/>
,	,	kIx,	,
zámečníci	zámečník	k1gMnPc5	zámečník
<g/>
,	,	kIx,	,
pažbaři	pažbař	k1gMnPc5	pažbař
-	-	kIx~	-
a	a	k8xC	a
počet	počet	k1gInSc1	počet
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
železáren	železárna	k1gFnPc2	železárna
byl	být	k5eAaImAgInS	být
okolo	okolo	k7c2	okolo
150	[number]	k4	150
<g/>
.	.	kIx.	.
</s>
<s>
Železárny	železárna	k1gFnPc1	železárna
byly	být	k5eAaImAgFnP	být
společně	společně	k6eAd1	společně
s	s	k7c7	s
hamry	hamr	k1gInPc7	hamr
v	v	k7c6	v
Raspenavě	Raspenavě	k1gFnPc6	Raspenavě
hlavními	hlavní	k2eAgFnPc7d1	hlavní
výrobnami	výrobna	k1gFnPc7	výrobna
zbraní	zbraň	k1gFnPc2	zbraň
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
nejen	nejen	k6eAd1	nejen
to	ten	k3xDgNnSc4	ten
-	-	kIx~	-
Albrecht	Albrecht	k1gMnSc1	Albrecht
začal	začít	k5eAaPmAgMnS	začít
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
znovu	znovu	k6eAd1	znovu
těžit	těžit	k5eAaImF	těžit
stříbro	stříbro	k1gNnSc4	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Ranou	Rana	k1gFnSc7	Rana
pro	pro	k7c4	pro
něj	on	k3xPp3gNnSc4	on
asi	asi	k9	asi
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1629	[number]	k4	1629
začala	začít	k5eAaPmAgFnS	začít
Stará	starý	k2eAgFnSc1d1	stará
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc4d1	hlavní
ložisko	ložisko	k1gNnSc4	ložisko
<g/>
,	,	kIx,	,
dávat	dávat	k5eAaImF	dávat
méně	málo	k6eAd2	málo
železné	železný	k2eAgFnPc1d1	železná
rudy	ruda	k1gFnPc1	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
Albrecht	Albrecht	k1gMnSc1	Albrecht
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
násilné	násilný	k2eAgFnSc2d1	násilná
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
představitelem	představitel	k1gMnSc7	představitel
rodu	rod	k1gInSc3	rod
Morzinů	Morzin	k1gInPc2	Morzin
na	na	k7c6	na
vrchlabském	vrchlabský	k2eAgNnSc6d1	vrchlabské
panství	panství	k1gNnSc6	panství
byl	být	k5eAaImAgMnS	být
maršálek	maršálek	k1gMnSc1	maršálek
Jan	Jan	k1gMnSc1	Jan
Rudolf	Rudolf	k1gMnSc1	Rudolf
z	z	k7c2	z
Morzinu	Morzin	k1gInSc2	Morzin
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
spiklenců	spiklenec	k1gMnPc2	spiklenec
proti	proti	k7c3	proti
Albrechtovi	Albrecht	k1gMnSc3	Albrecht
z	z	k7c2	z
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
Vrchlabí	Vrchlabí	k1gNnSc4	Vrchlabí
dostal	dostat	k5eAaPmAgMnS	dostat
roku	rok	k1gInSc2	rok
1635	[number]	k4	1635
od	od	k7c2	od
císaře	císař	k1gMnSc2	císař
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
darem	dar	k1gInSc7	dar
za	za	k7c4	za
prokázané	prokázaný	k2eAgFnPc4d1	prokázaná
služby	služba	k1gFnPc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
celým	celý	k2eAgNnSc7d1	celé
okolím	okolí	k1gNnSc7	okolí
prohnala	prohnat	k5eAaPmAgFnS	prohnat
vojska	vojsko	k1gNnPc4	vojsko
všech	všecek	k3xTgFnPc2	všecek
zúčastněných	zúčastněný	k2eAgFnPc2d1	zúčastněná
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
bylo	být	k5eAaImAgNnS	být
plenění	plenění	k1gNnSc4	plenění
a	a	k8xC	a
ničení	ničení	k1gNnSc4	ničení
ušetřeno	ušetřen	k2eAgNnSc4d1	ušetřeno
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgNnPc1d1	české
vojska	vojsko	k1gNnPc1	vojsko
neměla	mít	k5eNaImAgNnP	mít
důvod	důvod	k1gInSc4	důvod
a	a	k8xC	a
švédská	švédský	k2eAgNnPc1d1	švédské
luterská	luterský	k2eAgNnPc1d1	luterské
vojska	vojsko	k1gNnPc1	vojsko
se	se	k3xPyFc4	se
s	s	k7c7	s
městem	město	k1gNnSc7	město
stejného	stejný	k2eAgNnSc2d1	stejné
vyznání	vyznání	k1gNnSc2	vyznání
nějak	nějak	k6eAd1	nějak
domluvila	domluvit	k5eAaPmAgFnS	domluvit
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
roku	rok	k1gInSc2	rok
1664	[number]	k4	1664
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
krachu	krach	k1gInSc2	krach
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
prodalo	prodat	k5eAaPmAgNnS	prodat
jak	jak	k8xS	jak
lázně	lázeň	k1gFnPc1	lázeň
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
vinné	vinný	k2eAgInPc4d1	vinný
sklepy	sklep	k1gInPc4	sklep
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
maršálka	maršálek	k1gMnSc2	maršálek
z	z	k7c2	z
Morzinu	Morzin	k1gInSc2	Morzin
převzal	převzít	k5eAaPmAgMnS	převzít
roku	rok	k1gInSc2	rok
1646	[number]	k4	1646
vrchlabské	vrchlabský	k2eAgFnSc2d1	vrchlabská
panství	panství	k1gNnSc6	panství
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
hrabě	hrabě	k1gMnSc1	hrabě
Pavel	Pavel	k1gMnSc1	Pavel
Morzin	Morzin	k1gMnSc1	Morzin
<g/>
,	,	kIx,	,
s	s	k7c7	s
jehož	jehož	k3xOyRp3gNnSc7	jehož
jménem	jméno	k1gNnSc7	jméno
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
Vrchlabsku	Vrchlabska	k1gFnSc4	Vrchlabska
spjata	spjat	k2eAgFnSc1d1	spjata
rekatolizace	rekatolizace	k1gFnSc1	rekatolizace
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
jeho	jeho	k3xOp3gInSc7	jeho
významnějším	významný	k2eAgInSc7d2	významnější
krokem	krok	k1gInSc7	krok
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
směřoval	směřovat	k5eAaImAgInS	směřovat
k	k	k7c3	k
rekatolizaci	rekatolizace	k1gFnSc3	rekatolizace
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
povolání	povolání	k1gNnSc1	povolání
jezuitů	jezuita	k1gMnPc2	jezuita
z	z	k7c2	z
Jičína	Jičín	k1gInSc2	Jičín
roku	rok	k1gInSc2	rok
1638	[number]	k4	1638
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
za	za	k7c2	za
dob	doba	k1gFnPc2	doba
Gendorfa	Gendorf	k1gMnSc2	Gendorf
bylo	být	k5eAaImAgNnS	být
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
silně	silně	k6eAd1	silně
luteránské	luteránský	k2eAgFnPc4d1	luteránská
<g/>
,	,	kIx,	,
Pavlovy	Pavlův	k2eAgFnPc4d1	Pavlova
metody	metoda	k1gFnPc4	metoda
vynucování	vynucování	k1gNnSc2	vynucování
konvertování	konvertování	k1gNnSc2	konvertování
na	na	k7c4	na
katolickou	katolický	k2eAgFnSc4d1	katolická
víru	víra	k1gFnSc4	víra
přinesly	přinést	k5eAaPmAgFnP	přinést
roku	rok	k1gInSc2	rok
1651	[number]	k4	1651
podnět	podnět	k1gInSc4	podnět
k	k	k7c3	k
povstání	povstání	k1gNnSc3	povstání
občanů	občan	k1gMnPc2	občan
luterského	luterský	k2eAgNnSc2d1	luterské
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
stavěny	stavit	k5eAaImNgFnP	stavit
barikády	barikáda	k1gFnPc1	barikáda
a	a	k8xC	a
objevily	objevit	k5eAaPmAgFnP	objevit
se	se	k3xPyFc4	se
i	i	k9	i
zbraně	zbraň	k1gFnPc1	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Povstání	povstání	k1gNnSc4	povstání
však	však	k9	však
ukončil	ukončit	k5eAaPmAgMnS	ukončit
oddíl	oddíl	k1gInSc4	oddíl
50	[number]	k4	50
mušketýrů	mušketýr	k1gMnPc2	mušketýr
Mikuláše	mikuláš	k1gInSc2	mikuláš
ze	z	k7c2	z
Schönfeldu	Schönfeld	k1gInSc2	Schönfeld
<g/>
.	.	kIx.	.
</s>
<s>
Představitelé	představitel	k1gMnPc1	představitel
povstání	povstání	k1gNnSc4	povstání
utekli	utéct	k5eAaPmAgMnP	utéct
buďto	buďto	k8xC	buďto
do	do	k7c2	do
Horní	horní	k2eAgFnSc2d1	horní
Branné	branný	k2eAgFnSc2d1	Branná
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jako	jako	k8xS	jako
vůdce	vůdce	k1gMnSc1	vůdce
Theodosius	Theodosius	k1gMnSc1	Theodosius
Freybrich	Freybrich	k1gMnSc1	Freybrich
do	do	k7c2	do
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
také	také	k9	také
opustilo	opustit	k5eAaPmAgNnS	opustit
město	město	k1gNnSc1	město
a	a	k8xC	a
Dolejší	Dolejší	k1gFnSc1	Dolejší
a	a	k8xC	a
Hořejší	Hořejší	k1gFnSc1	Hořejší
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
dohromady	dohromady	k6eAd1	dohromady
149	[number]	k4	149
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Vítězství	vítězství	k1gNnSc1	vítězství
rekatolizace	rekatolizace	k1gFnSc2	rekatolizace
bylo	být	k5eAaImAgNnS	být
zpečetěno	zpečetit	k5eAaPmNgNnS	zpečetit
<g/>
.	.	kIx.	.
</s>
<s>
Železářská	železářský	k2eAgFnSc1d1	železářská
výroba	výroba	k1gFnSc1	výroba
byla	být	k5eAaImAgFnS	být
za	za	k7c2	za
doby	doba	k1gFnSc2	doba
Pavla	Pavla	k1gFnSc1	Pavla
Morzina	Morzina	k1gFnSc1	Morzina
spíše	spíše	k9	spíše
prodělečná	prodělečný	k2eAgFnSc1d1	prodělečná
<g/>
.	.	kIx.	.
</s>
<s>
Zato	zato	k6eAd1	zato
puškařství	puškařství	k1gNnSc1	puškařství
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
Albrechtových	Albrechtová	k1gFnPc2	Albrechtová
<g/>
,	,	kIx,	,
zažívalo	zažívat	k5eAaImAgNnS	zažívat
svou	svůj	k3xOyFgFnSc4	svůj
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zde	zde	k6eAd1	zde
máme	mít	k5eAaImIp1nP	mít
dochované	dochovaný	k2eAgInPc1d1	dochovaný
záznamy	záznam	k1gInPc1	záznam
o	o	k7c4	o
29	[number]	k4	29
mistrech	mistr	k1gMnPc6	mistr
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
pocházeli	pocházet	k5eAaImAgMnP	pocházet
z	z	k7c2	z
rodin	rodina	k1gFnPc2	rodina
Ludwigů	Ludwig	k1gMnPc2	Ludwig
a	a	k8xC	a
Erbenů	Erben	k1gMnPc2	Erben
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
nový	nový	k2eAgMnSc1d1	nový
pán	pán	k1gMnSc1	pán
také	také	k9	také
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
plátenickou	plátenický	k2eAgFnSc4d1	plátenická
výrobu	výroba	k1gFnSc4	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Výrobky	výrobek	k1gInPc1	výrobek
z	z	k7c2	z
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
se	se	k3xPyFc4	se
vyvážely	vyvážet	k5eAaImAgFnP	vyvážet
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
Španělska	Španělsko	k1gNnSc2	Španělsko
a	a	k8xC	a
také	také	k9	také
do	do	k7c2	do
Orientu	Orient	k1gInSc2	Orient
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ve	v	k7c4	v
Vrchlabí	Vrchlabí	k1gNnSc4	Vrchlabí
novým	nový	k2eAgNnSc7d1	nové
odvětvím	odvětví	k1gNnSc7	odvětví
průmyslu	průmysl	k1gInSc2	průmysl
bylo	být	k5eAaImAgNnS	být
barchetnictví	barchetnictví	k1gNnSc1	barchetnictví
–	–	k?	–
vyrábění	vyrábění	k1gNnSc1	vyrábění
tkaniny	tkanina	k1gFnSc2	tkanina
ze	z	k7c2	z
lnu	len	k1gInSc2	len
a	a	k8xC	a
bavlny	bavlna	k1gFnSc2	bavlna
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
pochází	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1647	[number]	k4	1647
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
obor	obor	k1gInSc1	obor
začal	začít	k5eAaPmAgInS	začít
zaujímat	zaujímat	k5eAaImF	zaujímat
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
mezi	mezi	k7c7	mezi
ekonomickými	ekonomický	k2eAgInPc7d1	ekonomický
obory	obor	k1gInPc7	obor
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
hornictví	hornictví	k1gNnSc1	hornictví
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
dostávat	dostávat	k5eAaImF	dostávat
do	do	k7c2	do
ústupu	ústup	k1gInSc2	ústup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
části	část	k1gFnSc6	část
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
tvořily	tvořit	k5eAaImAgFnP	tvořit
platby	platba	k1gFnPc1	platba
tkalců	tkadlec	k1gMnPc2	tkadlec
přes	přes	k7c4	přes
polovinu	polovina	k1gFnSc4	polovina
všech	všecek	k3xTgInPc2	všecek
panských	panský	k2eAgInPc2d1	panský
příjmů	příjem	k1gInPc2	příjem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
–	–	k?	–
roku	rok	k1gInSc2	rok
1712	[number]	k4	1712
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
55	[number]	k4	55
tkalcovských	tkalcovský	k2eAgMnPc2d1	tkalcovský
mistrů	mistr	k1gMnPc2	mistr
a	a	k8xC	a
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
panství	panství	k1gNnSc6	panství
jich	on	k3xPp3gInPc2	on
bylo	být	k5eAaImAgNnS	být
okolo	okolo	k7c2	okolo
dvou	dva	k4xCgNnPc2	dva
set	sto	k4xCgNnPc2	sto
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
odvětví	odvětví	k1gNnPc1	odvětví
<g/>
,	,	kIx,	,
papírenství	papírenství	k1gNnPc1	papírenství
<g/>
,	,	kIx,	,
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
roku	rok	k1gInSc2	rok
1667	[number]	k4	1667
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Pavel	Pavel	k1gMnSc1	Pavel
Morzin	Morzin	k1gMnSc1	Morzin
věnoval	věnovat	k5eAaPmAgMnS	věnovat
Kryštofu	Kryštof	k1gMnSc3	Kryštof
Weiszovi	Weisz	k1gMnSc3	Weisz
na	na	k7c6	na
založení	založení	k1gNnSc6	založení
papírny	papírna	k1gFnSc2	papírna
pozemek	pozemek	k1gInSc4	pozemek
pod	pod	k7c7	pod
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
z	z	k7c2	z
Morzinu	Morzin	k1gInSc2	Morzin
také	také	k9	také
zakládal	zakládat	k5eAaImAgInS	zakládat
boudy	bouda	k1gFnSc2	bouda
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
či	či	k8xC	či
střediska	středisko	k1gNnSc2	středisko
chovu	chov	k1gInSc3	chov
dobytka	dobytek	k1gInSc2	dobytek
<g/>
,	,	kIx,	,
založil	založit	k5eAaPmAgMnS	založit
chudobinec	chudobinec	k1gInSc4	chudobinec
a	a	k8xC	a
zřídil	zřídit	k5eAaPmAgMnS	zřídit
při	při	k7c6	při
úřadu	úřad	k1gInSc6	úřad
purkmistra	purkmistr	k1gMnSc2	purkmistr
oddělení	oddělení	k1gNnSc2	oddělení
finanční	finanční	k2eAgFnSc2d1	finanční
péče	péče	k1gFnSc2	péče
o	o	k7c4	o
sirotky	sirotek	k1gMnPc4	sirotek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1665	[number]	k4	1665
nechal	nechat	k5eAaPmAgMnS	nechat
většinu	většina	k1gFnSc4	většina
svých	svůj	k3xOyFgNnPc2	svůj
panství	panství	k1gNnPc2	panství
změnit	změnit	k5eAaPmF	změnit
na	na	k7c4	na
fideikomis	fideikomis	k1gInSc4	fideikomis
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
otce	otec	k1gMnSc2	otec
se	se	k3xPyFc4	se
panství	panství	k1gNnSc2	panství
ujal	ujmout	k5eAaPmAgMnS	ujmout
Jan	Jan	k1gMnSc1	Jan
Rudolf	Rudolf	k1gMnSc1	Rudolf
Morzin	Morzin	k1gMnSc1	Morzin
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ho	on	k3xPp3gMnSc4	on
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1688	[number]	k4	1688
<g/>
-	-	kIx~	-
<g/>
1702	[number]	k4	1702
<g/>
.	.	kIx.	.
</s>
<s>
Inicioval	iniciovat	k5eAaBmAgMnS	iniciovat
obnovu	obnova	k1gFnSc4	obnova
těžby	těžba	k1gFnSc2	těžba
ve	v	k7c6	v
Svatém	svatý	k1gMnSc6	svatý
Petru	Petr	k1gMnSc6	Petr
ve	v	k7c6	v
Špindlerově	Špindlerův	k2eAgInSc6d1	Špindlerův
Mlýně	mlýn	k1gInSc6	mlýn
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
nechal	nechat	k5eAaPmAgMnS	nechat
vybudovat	vybudovat	k5eAaPmF	vybudovat
hutě	huť	k1gFnPc4	huť
na	na	k7c4	na
měď	měď	k1gFnSc4	měď
a	a	k8xC	a
stříbro	stříbro	k1gNnSc4	stříbro
v	v	k7c6	v
Hořejším	hořejší	k2eAgNnSc6d1	hořejší
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
<g/>
.	.	kIx.	.
</s>
<s>
Kovy	kov	k1gInPc4	kov
také	také	k6eAd1	také
začal	začít	k5eAaPmAgInS	začít
dodávat	dodávat	k5eAaImF	dodávat
do	do	k7c2	do
Kutné	kutný	k2eAgFnSc2d1	Kutná
Hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
těžba	těžba	k1gFnSc1	těžba
byla	být	k5eAaImAgFnS	být
sice	sice	k8xC	sice
málo	málo	k6eAd1	málo
výnosná	výnosný	k2eAgFnSc1d1	výnosná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
přesto	přesto	k8xC	přesto
vydržela	vydržet	k5eAaPmAgFnS	vydržet
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1737	[number]	k4	1737
<g/>
.	.	kIx.	.
</s>
<s>
Těžba	těžba	k1gFnSc1	těžba
však	však	k9	však
ležela	ležet	k5eAaImAgFnS	ležet
i	i	k9	i
na	na	k7c6	na
bedrech	bedra	k1gNnPc6	bedra
měšťanů	měšťan	k1gMnPc2	měšťan
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
tomu	ten	k3xDgInSc3	ten
roku	rok	k1gInSc3	rok
1692	[number]	k4	1692
vzbouřili	vzbouřit	k5eAaPmAgMnP	vzbouřit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
výnosy	výnos	k1gInPc1	výnos
se	se	k3xPyFc4	se
pomalu	pomalu	k6eAd1	pomalu
začaly	začít	k5eAaPmAgFnP	začít
propadat	propadat	k5eAaImF	propadat
pod	pod	k7c4	pod
vložené	vložený	k2eAgInPc4d1	vložený
peníze	peníz	k1gInPc4	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
neúspěchu	neúspěch	k1gInSc6	neúspěch
byli	být	k5eAaImAgMnP	být
potrestaní	potrestaný	k2eAgMnPc1d1	potrestaný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dostalo	dostat	k5eAaPmAgNnS	dostat
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
i	i	k9	i
robotních	robotní	k2eAgFnPc2d1	robotní
úlev	úleva	k1gFnPc2	úleva
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Rudolf	Rudolf	k1gMnSc1	Rudolf
také	také	k9	také
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
otce	otec	k1gMnSc2	otec
založil	založit	k5eAaPmAgMnS	založit
první	první	k4xOgFnSc4	první
nemocnici	nemocnice	k1gFnSc4	nemocnice
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1654	[number]	k4	1654
až	až	k9	až
1700	[number]	k4	1700
město	město	k1gNnSc1	město
rychle	rychle	k6eAd1	rychle
rostlo	růst	k5eAaImAgNnS	růst
<g/>
,	,	kIx,	,
z	z	k7c2	z
84	[number]	k4	84
domů	dům	k1gInPc2	dům
jich	on	k3xPp3gMnPc2	on
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
bylo	být	k5eAaImAgNnS	být
228	[number]	k4	228
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
dnešní	dnešní	k2eAgFnSc7d1	dnešní
památkou	památka	k1gFnSc7	památka
na	na	k7c4	na
Jana	Jan	k1gMnSc4	Jan
Rudolfa	Rudolf	k1gMnSc4	Rudolf
z	z	k7c2	z
Morzinu	Morzin	k1gInSc2	Morzin
je	být	k5eAaImIp3nS	být
morový	morový	k2eAgInSc1d1	morový
sloup	sloup	k1gInSc1	sloup
na	na	k7c6	na
Náměstí	náměstí	k1gNnSc6	náměstí
Míru	mír	k1gInSc2	mír
u	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc4	ten
nechala	nechat	k5eAaPmAgFnS	nechat
vystavět	vystavět	k5eAaPmF	vystavět
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
Eva	Eva	k1gFnSc1	Eva
Konstancie	Konstancie	k1gFnSc1	Konstancie
Vratislava	Vratislava	k1gFnSc1	Vratislava
z	z	k7c2	z
Mitrovic	Mitrovice	k1gFnPc2	Mitrovice
roku	rok	k1gInSc2	rok
1696	[number]	k4	1696
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Rudolf	Rudolf	k1gMnSc1	Rudolf
Morzin	Morzin	k1gMnSc1	Morzin
zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
1702	[number]	k4	1702
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
však	však	k9	však
ještě	ještě	k6eAd1	ještě
stihl	stihnout	k5eAaPmAgMnS	stihnout
získat	získat	k5eAaPmF	získat
jako	jako	k9	jako
nejstarší	starý	k2eAgFnSc1d3	nejstarší
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
rodu	rod	k1gInSc2	rod
hraběcí	hraběcí	k2eAgInSc4d1	hraběcí
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Dědičné	dědičný	k2eAgNnSc1d1	dědičné
panství	panství	k1gNnSc1	panství
získal	získat	k5eAaPmAgInS	získat
roku	rok	k1gInSc2	rok
1702	[number]	k4	1702
Janův	Janův	k2eAgMnSc1d1	Janův
syn	syn	k1gMnSc1	syn
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
Morzin	Morzin	k1gMnSc1	Morzin
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
položil	položit	k5eAaPmAgMnS	položit
roku	rok	k1gInSc2	rok
1705	[number]	k4	1705
základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
pro	pro	k7c4	pro
dnešní	dnešní	k2eAgFnSc4d1	dnešní
dominantu	dominanta	k1gFnSc4	dominanta
města	město	k1gNnSc2	město
–	–	k?	–
klášter	klášter	k1gInSc4	klášter
poustevníků	poustevník	k1gMnPc2	poustevník
augustiniánů	augustinián	k1gMnPc2	augustinián
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
samého	samý	k3xTgInSc2	samý
roku	rok	k1gInSc2	rok
stihl	stihnout	k5eAaPmAgInS	stihnout
také	také	k9	také
položit	položit	k5eAaPmF	položit
základní	základní	k2eAgInSc4d1	základní
kámen	kámen	k1gInSc4	kámen
panského	panský	k2eAgInSc2d1	panský
špitálu	špitál	k1gInSc2	špitál
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
zakladatel	zakladatel	k1gMnSc1	zakladatel
zemřel	zemřít	k5eAaPmAgMnS	zemřít
rok	rok	k1gInSc4	rok
po	po	k7c6	po
položení	položení	k1gNnSc6	položení
obou	dva	k4xCgInPc2	dva
základních	základní	k2eAgInPc2d1	základní
kamenů	kámen	k1gInPc2	kámen
roku	rok	k1gInSc2	rok
1706	[number]	k4	1706
<g/>
.	.	kIx.	.
</s>
<s>
Panství	panství	k1gNnSc1	panství
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
převzal	převzít	k5eAaPmAgMnS	převzít
druhý	druhý	k4xOgMnSc1	druhý
syn	syn	k1gMnSc1	syn
Jana	Jan	k1gMnSc2	Jan
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Morzina	Morzina	k1gMnSc1	Morzina
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Václav	Václav	k1gMnSc1	Václav
Morzin	Morzin	k1gMnSc1	Morzin
(	(	kIx(	(
<g/>
*	*	kIx~	*
1676	[number]	k4	1676
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
osvícený	osvícený	k2eAgMnSc1d1	osvícený
a	a	k8xC	a
uměnímilovný	uměnímilovný	k2eAgMnSc1d1	uměnímilovný
představitel	představitel	k1gMnSc1	představitel
rodu	rod	k1gInSc2	rod
Morzinů	Morzin	k1gMnPc2	Morzin
nechal	nechat	k5eAaPmAgMnS	nechat
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
zakázkách	zakázka	k1gFnPc6	zakázka
pracovat	pracovat	k5eAaImF	pracovat
umělce	umělec	k1gMnPc4	umělec
<g/>
,	,	kIx,	,
jakými	jaký	k3yRgInPc7	jaký
byli	být	k5eAaImAgMnP	být
například	například	k6eAd1	například
Jan	Jan	k1gMnSc1	Jan
Blažej	Blažej	k1gMnSc1	Blažej
Santini-Aichel	Santini-Aichel	k1gMnSc1	Santini-Aichel
<g/>
,	,	kIx,	,
Matyáš	Matyáš	k1gMnSc1	Matyáš
Brokoff	Brokoff	k1gMnSc1	Brokoff
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Petr	Petr	k1gMnSc1	Petr
Molitor	Molitor	k1gMnSc1	Molitor
nebo	nebo	k8xC	nebo
slavný	slavný	k2eAgMnSc1d1	slavný
skladatel	skladatel	k1gMnSc1	skladatel
Antonio	Antonio	k1gMnSc1	Antonio
Vivaldi	Vivald	k1gMnPc1	Vivald
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
mu	on	k3xPp3gMnSc3	on
věnoval	věnovat	k5eAaImAgInS	věnovat
svou	svůj	k3xOyFgFnSc4	svůj
slavnou	slavný	k2eAgFnSc4d1	slavná
skladbu	skladba	k1gFnSc4	skladba
Čtvero	čtvero	k4xRgNnSc1	čtvero
ročních	roční	k2eAgFnPc2d1	roční
období	období	k1gNnPc2	období
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
kapele	kapela	k1gFnSc6	kapela
působil	působit	k5eAaImAgMnS	působit
například	například	k6eAd1	například
i	i	k9	i
Antonín	Antonín	k1gMnSc1	Antonín
Reichenauer	Reichenauer	k1gMnSc1	Reichenauer
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
hudbymilovnost	hudbymilovnost	k1gFnSc4	hudbymilovnost
však	však	k9	však
hrabě	hrabě	k1gMnSc1	hrabě
těžce	těžce	k6eAd1	těžce
zaplatil	zaplatit	k5eAaPmAgMnS	zaplatit
–	–	k?	–
zadlužoval	zadlužovat	k5eAaImAgMnS	zadlužovat
se	se	k3xPyFc4	se
a	a	k8xC	a
bankrot	bankrot	k1gInSc1	bankrot
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
blížil	blížit	k5eAaImAgInS	blížit
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
ho	on	k3xPp3gNnSc4	on
však	však	k9	však
již	již	k6eAd1	již
nezažil	zažít	k5eNaPmAgMnS	zažít
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
1737	[number]	k4	1737
<g/>
,	,	kIx,	,
vrchlabské	vrchlabský	k2eAgNnSc1d1	vrchlabské
panství	panství	k1gNnSc1	panství
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
31	[number]	k4	31
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Karel	Karel	k1gMnSc1	Karel
Josef	Josef	k1gMnSc1	Josef
Morzin	Morzin	k1gMnSc1	Morzin
zdědil	zdědit	k5eAaPmAgMnS	zdědit
po	po	k7c6	po
otci	otec	k1gMnSc6	otec
toto	tento	k3xDgNnSc1	tento
panství	panství	k1gNnSc1	panství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
ho	on	k3xPp3gMnSc4	on
jen	jen	k9	jen
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1741	[number]	k4	1741
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
měl	mít	k5eAaImAgMnS	mít
tomuto	tento	k3xDgMnSc3	tento
panství	panství	k1gNnSc4	panství
vládnout	vládnout	k5eAaImF	vládnout
nezletilý	zletilý	k2eNgMnSc1d1	nezletilý
František	František	k1gMnSc1	František
Xaver	Xaver	k1gMnSc1	Xaver
Morzin	Morzin	k1gMnSc1	Morzin
(	(	kIx(	(
<g/>
*	*	kIx~	*
1734	[number]	k4	1734
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
něho	on	k3xPp3gNnSc2	on
však	však	k9	však
fakticky	fakticky	k6eAd1	fakticky
vládli	vládnout	k5eAaImAgMnP	vládnout
jeho	jeho	k3xOp3gMnPc1	jeho
poručníci	poručník	k1gMnPc1	poručník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
dokázali	dokázat	k5eAaPmAgMnP	dokázat
bankrot	bankrot	k1gInSc4	bankrot
odvrátit	odvrátit	k5eAaPmF	odvrátit
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nP	by
museli	muset	k5eAaImAgMnP	muset
rozprodat	rozprodat	k5eAaPmF	rozprodat
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
však	však	k9	však
vlády	vláda	k1gFnSc2	vláda
ujal	ujmout	k5eAaPmAgMnS	ujmout
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Vládl	vládnout	k5eAaImAgMnS	vládnout
však	však	k9	však
velice	velice	k6eAd1	velice
špatně	špatně	k6eAd1	špatně
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
možná	možná	k9	možná
pro	pro	k7c4	pro
panství	panství	k1gNnSc1	panství
úlevou	úleva	k1gFnSc7	úleva
<g/>
,	,	kIx,	,
že	že	k8xS	že
roku	rok	k1gInSc2	rok
1791	[number]	k4	1791
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1791	[number]	k4	1791
vrchlabské	vrchlabský	k2eAgNnSc4d1	vrchlabské
panství	panství	k1gNnSc4	panství
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
Rudolf	Rudolf	k1gMnSc1	Rudolf
Morzin	Morzin	k1gMnSc1	Morzin
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
se	se	k3xPyFc4	se
krach	krach	k1gInSc1	krach
již	již	k6eAd1	již
nevyhnul	vyhnout	k5eNaPmAgInS	vyhnout
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1795	[number]	k4	1795
musel	muset	k5eAaImAgMnS	muset
z	z	k7c2	z
finančních	finanční	k2eAgInPc2d1	finanční
důvodů	důvod	k1gInPc2	důvod
prodat	prodat	k5eAaPmF	prodat
fideikomis	fideikomis	k1gInSc4	fideikomis
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
trval	trvat	k5eAaImAgInS	trvat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1665	[number]	k4	1665
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgNnSc1d1	jediné
panství	panství	k1gNnSc1	panství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
si	se	k3xPyFc3	se
nechal	nechat	k5eAaPmAgMnS	nechat
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
právě	právě	k9	právě
panství	panství	k1gNnSc1	panství
vrchlabské	vrchlabský	k2eAgNnSc1d1	vrchlabské
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
mělo	mít	k5eAaImAgNnS	mít
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
252	[number]	k4	252
domů	dům	k1gInPc2	dům
a	a	k8xC	a
2500	[number]	k4	2500
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
dům	dům	k1gInSc4	dům
tedy	tedy	k9	tedy
připadalo	připadat	k5eAaImAgNnS	připadat
necelých	celý	k2eNgInPc2d1	necelý
10	[number]	k4	10
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
řemesel	řemeslo	k1gNnPc2	řemeslo
tu	tu	k6eAd1	tu
byl	být	k5eAaImAgMnS	být
jeden	jeden	k4xCgMnSc1	jeden
jediný	jediný	k2eAgMnSc1d1	jediný
puškař	puškař	k1gMnSc1	puškař
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
tkalcovských	tkalcovský	k2eAgMnPc2d1	tkalcovský
mistrů	mistr	k1gMnPc2	mistr
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
bylo	být	k5eAaImAgNnS	být
24	[number]	k4	24
<g/>
.	.	kIx.	.
</s>
<s>
Obchodníků	obchodník	k1gMnPc2	obchodník
s	s	k7c7	s
přízí	příz	k1gFnSc7	příz
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1737	[number]	k4	1737
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
nová	nový	k2eAgFnSc1d1	nová
radnice	radnice	k1gFnSc1	radnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sídlila	sídlit	k5eAaImAgFnS	sídlit
samospráva	samospráva	k1gFnSc1	samospráva
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
purkmistrem	purkmistr	k1gMnSc7	purkmistr
a	a	k8xC	a
státem	stát	k1gInSc7	stát
zkoušenými	zkoušený	k2eAgInPc7d1	zkoušený
a	a	k8xC	a
státu	stát	k1gInSc2	stát
odpovědnými	odpovědný	k2eAgMnPc7d1	odpovědný
úředníky	úředník	k1gMnPc7	úředník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1770	[number]	k4	1770
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
přechodně	přechodně	k6eAd1	přechodně
zřízena	zřídit	k5eAaPmNgFnS	zřídit
první	první	k4xOgFnSc1	první
tkalcovská	tkalcovský	k2eAgFnSc1d1	tkalcovská
škola	škola	k1gFnSc1	škola
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
na	na	k7c4	na
trvalo	trvalo	k1gNnSc4	trvalo
fungovat	fungovat	k5eAaImF	fungovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1873	[number]	k4	1873
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1786	[number]	k4	1786
byla	být	k5eAaImAgFnS	být
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
stará	starý	k2eAgFnSc1d1	stará
škola	škola	k1gFnSc1	škola
na	na	k7c6	na
kostelním	kostelní	k2eAgNnSc6d1	kostelní
náměstí	náměstí	k1gNnSc6	náměstí
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
zavedení	zavedení	k1gNnSc1	zavedení
povinné	povinný	k2eAgFnSc2d1	povinná
školní	školní	k2eAgFnSc2d1	školní
docházky	docházka	k1gFnSc2	docházka
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
konvent	konvent	k1gInSc1	konvent
augustiniánů	augustinián	k1gMnPc2	augustinián
fungoval	fungovat	k5eAaImAgInS	fungovat
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
omezeně	omezeně	k6eAd1	omezeně
<g/>
,	,	kIx,	,
i	i	k8xC	i
po	po	k7c6	po
zrušení	zrušení	k1gNnSc6	zrušení
všech	všecek	k3xTgInPc2	všecek
klášterů	klášter	k1gInPc2	klášter
roku	rok	k1gInSc2	rok
1781	[number]	k4	1781
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgFnP	být
nejhorší	zlý	k2eAgFnSc7d3	nejhorší
událostí	událost	k1gFnSc7	událost
na	na	k7c6	na
vrchlabském	vrchlabský	k2eAgNnSc6d1	vrchlabské
panství	panství	k1gNnSc6	panství
slezské	slezský	k2eAgFnSc2d1	Slezská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1741	[number]	k4	1741
až	až	k8xS	až
1742	[number]	k4	1742
se	se	k3xPyFc4	se
nakvartýrovaly	nakvartýrovat	k5eAaPmAgInP	nakvartýrovat
na	na	k7c4	na
vrchlabské	vrchlabský	k2eAgNnSc4d1	vrchlabské
panství	panství	k1gNnSc4	panství
oddíly	oddíl	k1gInPc1	oddíl
královských	královský	k2eAgMnPc2d1	královský
mušketýrů	mušketýr	k1gMnPc2	mušketýr
<g/>
,	,	kIx,	,
jezdců	jezdec	k1gMnPc2	jezdec
a	a	k8xC	a
dělostřelců	dělostřelec	k1gMnPc2	dělostřelec
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1744	[number]	k4	1744
město	město	k1gNnSc4	město
obsadili	obsadit	k5eAaPmAgMnP	obsadit
nepřátelští	přátelský	k2eNgMnPc1d1	nepřátelský
Prusové	Prus	k1gMnPc1	Prus
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
vypálili	vypálit	k5eAaPmAgMnP	vypálit
několik	několik	k4yIc4	několik
domů	dům	k1gInPc2	dům
a	a	k8xC	a
vykradli	vykrást	k5eAaPmAgMnP	vykrást
klášter	klášter	k1gInSc4	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byli	být	k5eAaImAgMnP	být
zajati	zajmout	k5eAaPmNgMnP	zajmout
u	u	k7c2	u
Maršova	Maršův	k2eAgNnSc2d1	Maršův
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
jejich	jejich	k3xOp3gInSc2	jejich
klášterního	klášterní	k2eAgInSc2d1	klášterní
lupu	lup	k1gInSc2	lup
do	do	k7c2	do
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
vrátila	vrátit	k5eAaPmAgFnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Rakousko	Rakousko	k1gNnSc1	Rakousko
válku	válka	k1gFnSc4	válka
o	o	k7c4	o
Slezsko	Slezsko	k1gNnSc4	Slezsko
prohrálo	prohrát	k5eAaPmAgNnS	prohrát
<g/>
,	,	kIx,	,
ocitlo	ocitnout	k5eAaPmAgNnS	ocitnout
se	se	k3xPyFc4	se
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
v	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
státní	státní	k2eAgFnSc2d1	státní
hranice	hranice	k1gFnSc2	hranice
s	s	k7c7	s
Pruskem	Prusko	k1gNnSc7	Prusko
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodnost	Nevýhodnost	k1gFnSc1	Nevýhodnost
této	tento	k3xDgFnSc2	tento
pozice	pozice	k1gFnSc2	pozice
se	se	k3xPyFc4	se
ukázala	ukázat	k5eAaPmAgFnS	ukázat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1756	[number]	k4	1756
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začala	začít	k5eAaPmAgFnS	začít
sedmiletá	sedmiletý	k2eAgFnSc1d1	sedmiletá
válka	válka	k1gFnSc1	válka
s	s	k7c7	s
Pruskem	Prusko	k1gNnSc7	Prusko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
střídaly	střídat	k5eAaImAgFnP	střídat
pruské	pruský	k2eAgFnPc1d1	pruská
a	a	k8xC	a
rakouské	rakouský	k2eAgFnPc1d1	rakouská
posádky	posádka	k1gFnPc1	posádka
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
obě	dva	k4xCgFnPc1	dva
se	se	k3xPyFc4	se
k	k	k7c3	k
obyvatelům	obyvatel	k1gMnPc3	obyvatel
a	a	k8xC	a
městu	město	k1gNnSc3	město
chovaly	chovat	k5eAaImAgInP	chovat
stejně	stejně	k6eAd1	stejně
<g/>
:	:	kIx,	:
například	například	k6eAd1	například
roku	rok	k1gInSc2	rok
1758	[number]	k4	1758
bylo	být	k5eAaImAgNnS	být
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
vydrancováno	vydrancován	k2eAgNnSc1d1	vydrancováno
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
několik	několik	k4yIc1	několik
obyvatel	obyvatel	k1gMnPc2	obyvatel
oběšeno	oběsit	k5eAaPmNgNnS	oběsit
právě	právě	k9	právě
vojáky	voják	k1gMnPc4	voják
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1778	[number]	k4	1778
začala	začít	k5eAaPmAgFnS	začít
další	další	k2eAgFnSc1d1	další
velká	velký	k2eAgFnSc1d1	velká
válka	válka	k1gFnSc1	válka
mezi	mezi	k7c7	mezi
Rakouskem	Rakousko	k1gNnSc7	Rakousko
a	a	k8xC	a
Pruskem	Prusko	k1gNnSc7	Prusko
–	–	k?	–
válka	válka	k1gFnSc1	válka
o	o	k7c4	o
bavorské	bavorský	k2eAgNnSc4d1	bavorské
dědictví	dědictví	k1gNnSc4	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
sehrály	sehrát	k5eAaPmAgInP	sehrát
kopce	kopec	k1gInPc1	kopec
ve	v	k7c6	v
vrchlabském	vrchlabský	k2eAgNnSc6d1	vrchlabské
okolí	okolí	k1gNnSc6	okolí
strategickou	strategický	k2eAgFnSc4d1	strategická
úlohu	úloha	k1gFnSc4	úloha
<g/>
.	.	kIx.	.
</s>
<s>
Vojska	vojsko	k1gNnPc1	vojsko
následníka	následník	k1gMnSc2	následník
trůnu	trůn	k1gInSc2	trůn
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
byla	být	k5eAaImAgFnS	být
opevněna	opevnit	k5eAaPmNgFnS	opevnit
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
Labe	Labe	k1gNnSc2	Labe
od	od	k7c2	od
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
k	k	k7c3	k
Jaroměři	Jaroměř	k1gFnSc3	Jaroměř
a	a	k8xC	a
od	od	k7c2	od
východu	východ	k1gInSc2	východ
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
blížila	blížit	k5eAaImAgFnS	blížit
vojska	vojsko	k1gNnSc2	vojsko
Fridricha	Fridrich	k1gMnSc2	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Rakouská	rakouský	k2eAgNnPc1d1	rakouské
opevnění	opevnění	k1gNnPc1	opevnění
se	se	k3xPyFc4	se
táhla	táhnout	k5eAaImAgNnP	táhnout
od	od	k7c2	od
Strážné	strážný	k2eAgFnSc2d1	Strážná
hůry	hůra	k1gFnSc2	hůra
(	(	kIx(	(
<g/>
824	[number]	k4	824
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
přes	přes	k7c4	přes
Jankův	Jankův	k2eAgInSc4d1	Jankův
vrch	vrch	k1gInSc4	vrch
(	(	kIx(	(
<g/>
694	[number]	k4	694
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
nad	nad	k7c7	nad
centrem	centrum	k1gNnSc7	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
Zimův	Zimův	k2eAgInSc1d1	Zimův
vrch	vrch	k1gInSc1	vrch
nad	nad	k7c7	nad
dnešní	dnešní	k2eAgFnSc7d1	dnešní
automobilkou	automobilka	k1gFnSc7	automobilka
až	až	k9	až
po	po	k7c4	po
vrch	vrch	k1gInSc4	vrch
Hůrka	hůrka	k1gFnSc1	hůrka
(	(	kIx(	(
<g/>
492	[number]	k4	492
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
nad	nad	k7c7	nad
Podhůřím	Podhůří	k1gNnSc7	Podhůří
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
pruský	pruský	k2eAgMnSc1d1	pruský
panovník	panovník	k1gMnSc1	panovník
pokusil	pokusit	k5eAaPmAgMnS	pokusit
tyto	tento	k3xDgFnPc4	tento
rakouské	rakouský	k2eAgFnPc4d1	rakouská
pozice	pozice	k1gFnPc4	pozice
obejít	obejít	k5eAaPmF	obejít
přes	přes	k7c4	přes
sever	sever	k1gInSc4	sever
a	a	k8xC	a
nepodařilo	podařit	k5eNaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
prý	prý	k9	prý
prohlásit	prohlásit	k5eAaPmF	prohlásit
větu	věta	k1gFnSc4	věta
"	"	kIx"	"
<g/>
Mrzí	mrzet	k5eAaImIp3nS	mrzet
mě	já	k3xPp1nSc2	já
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vidím	vidět	k5eAaImIp1nS	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
nedá	dát	k5eNaPmIp3nS	dát
nic	nic	k3yNnSc1	nic
dělat	dělat	k5eAaImF	dělat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Místo	místo	k7c2	místo
většího	veliký	k2eAgInSc2d2	veliký
útoku	útok	k1gInSc2	útok
na	na	k7c4	na
opevněnou	opevněný	k2eAgFnSc4d1	opevněná
linii	linie	k1gFnSc4	linie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
bylo	být	k5eAaImAgNnS	být
skryto	skrýt	k5eAaPmNgNnS	skrýt
40	[number]	k4	40
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
jenom	jenom	k6eAd1	jenom
raboval	rabovat	k5eAaImAgMnS	rabovat
vsi	ves	k1gFnSc2	ves
východně	východně	k6eAd1	východně
od	od	k7c2	od
císařských	císařský	k2eAgFnPc2d1	císařská
pozic	pozice	k1gFnPc2	pozice
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
bitvě	bitva	k1gFnSc3	bitva
však	však	k9	však
už	už	k6eAd1	už
během	během	k7c2	během
války	válka	k1gFnSc2	válka
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
Prusové	Prus	k1gMnPc1	Prus
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
také	také	k9	také
Vrchlabí	Vrchlabí	k1gNnSc4	Vrchlabí
poprvé	poprvé	k6eAd1	poprvé
navštívil	navštívit	k5eAaPmAgMnS	navštívit
a	a	k8xC	a
městu	město	k1gNnSc3	město
za	za	k7c4	za
škody	škoda	k1gFnPc4	škoda
utrpěné	utrpěný	k2eAgFnPc4d1	utrpěná
válkou	válka	k1gFnSc7	válka
daroval	darovat	k5eAaPmAgMnS	darovat
80	[number]	k4	80
000	[number]	k4	000
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
postihl	postihnout	k5eAaPmAgInS	postihnout
panství	panství	k1gNnSc4	panství
velký	velký	k2eAgInSc1d1	velký
hladomor	hladomor	k1gInSc1	hladomor
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ilustraci	ilustrace	k1gFnSc4	ilustrace
–	–	k?	–
roku	rok	k1gInSc2	rok
1772	[number]	k4	1772
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
na	na	k7c4	na
hlad	hlad	k1gInSc4	hlad
889	[number]	k4	889
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
jelikož	jelikož	k8xS	jelikož
byla	být	k5eAaImAgFnS	být
zrovna	zrovna	k6eAd1	zrovna
doba	doba	k1gFnSc1	doba
"	"	kIx"	"
<g/>
úrodná	úrodný	k2eAgFnSc1d1	úrodná
<g/>
"	"	kIx"	"
na	na	k7c4	na
selská	selské	k1gNnPc4	selské
povstání	povstání	k1gNnSc2	povstání
<g/>
,	,	kIx,	,
poddaní	poddaný	k1gMnPc1	poddaný
se	se	k3xPyFc4	se
začali	začít	k5eAaPmAgMnP	začít
roku	rok	k1gInSc2	rok
1775	[number]	k4	1775
bouřit	bouřit	k5eAaImF	bouřit
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
mnoho	mnoho	k4c1	mnoho
nespokojenců	nespokojenec	k1gMnPc2	nespokojenec
přišlo	přijít	k5eAaPmAgNnS	přijít
před	před	k7c4	před
radnici	radnice	k1gFnSc4	radnice
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
jim	on	k3xPp3gMnPc3	on
vyšli	vyjít	k5eAaPmAgMnP	vyjít
radní	radní	k1gMnPc1	radní
vstříc	vstříc	k6eAd1	vstříc
<g/>
,	,	kIx,	,
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
na	na	k7c4	na
zámek	zámek	k1gInSc4	zámek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
svůj	svůj	k3xOyFgInSc4	svůj
hněv	hněv	k1gInSc4	hněv
vylili	vylít	k5eAaPmAgMnP	vylít
na	na	k7c6	na
panských	panský	k2eAgMnPc6d1	panský
úřednících	úředník	k1gMnPc6	úředník
<g/>
.	.	kIx.	.
</s>
<s>
Nespokojenci	nespokojenec	k1gMnPc1	nespokojenec
poté	poté	k6eAd1	poté
vypili	vypít	k5eAaPmAgMnP	vypít
panský	panský	k2eAgInSc4d1	panský
pivovar	pivovar	k1gInSc4	pivovar
a	a	k8xC	a
vyjedli	vyjíst	k5eAaPmAgMnP	vyjíst
pekařství	pekařství	k1gNnSc4	pekařství
a	a	k8xC	a
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
byli	být	k5eAaImAgMnP	být
kvůli	kvůli	k7c3	kvůli
neshodám	neshoda	k1gFnPc3	neshoda
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
řadách	řada	k1gFnPc6	řada
rozehnáni	rozehnat	k5eAaPmNgMnP	rozehnat
vojskem	vojsko	k1gNnSc7	vojsko
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
i	i	k8xC	i
potrestáni	potrestat	k5eAaPmNgMnP	potrestat
<g/>
.	.	kIx.	.
</s>
<s>
Tresty	trest	k1gInPc1	trest
ovšem	ovšem	k9	ovšem
byly	být	k5eAaImAgInP	být
velice	velice	k6eAd1	velice
mírné	mírný	k2eAgInPc1d1	mírný
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
nejtěžším	těžký	k2eAgInSc7d3	nejtěžší
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
potrestaný	potrestaný	k2eAgMnSc1d1	potrestaný
musel	muset	k5eAaImAgMnS	muset
čtyři	čtyři	k4xCgInPc4	čtyři
týdny	týden	k1gInPc4	týden
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
okovech	okov	k1gInPc6	okov
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1781	[number]	k4	1781
císařem	císař	k1gMnSc7	císař
vydán	vydat	k5eAaPmNgInS	vydat
patent	patent	k1gInSc1	patent
o	o	k7c4	o
zrušení	zrušení	k1gNnSc4	zrušení
nevolnictví	nevolnictví	k1gNnSc2	nevolnictví
<g/>
,	,	kIx,	,
vrchlabské	vrchlabský	k2eAgNnSc1d1	vrchlabské
vrchnost	vrchnost	k1gFnSc4	vrchnost
zrušila	zrušit	k5eAaPmAgFnS	zrušit
i	i	k9	i
robotu	robota	k1gFnSc4	robota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
dvě	dva	k4xCgFnPc1	dva
další	další	k2eAgFnPc1d1	další
vrchlabské	vrchlabský	k2eAgFnPc1d1	vrchlabská
vsi	ves	k1gFnPc1	ves
–	–	k?	–
Harta	Harta	k1gFnSc1	Harta
a	a	k8xC	a
Fuchsberk	Fuchsberk	k1gInSc1	Fuchsberk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ve	v	k7c6	v
městě	město	k1gNnSc6	město
působil	působit	k5eAaImAgMnS	působit
malíř	malíř	k1gMnSc1	malíř
Josef	Josef	k1gMnSc1	Josef
Hollmann	Hollmann	k1gMnSc1	Hollmann
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
roku	rok	k1gInSc2	rok
1720	[number]	k4	1720
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
32	[number]	k4	32
lunet	luneta	k1gFnPc2	luneta
o	o	k7c6	o
svatém	svatý	k2eAgInSc6d1	svatý
Augustinovi	Augustin	k1gMnSc3	Augustin
do	do	k7c2	do
místního	místní	k2eAgInSc2d1	místní
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Zachovalo	zachovat	k5eAaPmAgNnS	zachovat
se	se	k3xPyFc4	se
jich	on	k3xPp3gFnPc2	on
však	však	k9	však
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
většina	většina	k1gFnSc1	většina
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
řada	řada	k1gFnSc1	řada
níže	nízce	k6eAd2	nízce
popsaných	popsaný	k2eAgFnPc2d1	popsaná
soch	socha	k1gFnPc2	socha
a	a	k8xC	a
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
století	století	k1gNnSc6	století
začalo	začít	k5eAaPmAgNnS	začít
pomalu	pomalu	k6eAd1	pomalu
upadat	upadat	k5eAaPmF	upadat
lnářství	lnářství	k1gNnSc4	lnářství
a	a	k8xC	a
do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
se	se	k3xPyFc4	se
dostávala	dostávat	k5eAaImAgFnS	dostávat
bělidla	bělidlo	k1gNnSc2	bělidlo
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
taková	takový	k3xDgFnSc1	takový
stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
roku	rok	k1gInSc2	rok
1803	[number]	k4	1803
v	v	k7c6	v
Podhůří	Podhůří	k1gNnSc6	Podhůří
Wenzlem	Wenzlo	k1gNnSc7	Wenzlo
Zirmem	Zirm	k1gMnSc7	Zirm
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1802	[number]	k4	1802
založili	založit	k5eAaPmAgMnP	založit
bratři	bratr	k1gMnPc1	bratr
Kieslingové	Kieslingový	k2eAgFnSc2d1	Kieslingový
v	v	k7c6	v
Dolním	dolní	k2eAgNnSc6d1	dolní
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
historicky	historicky	k6eAd1	historicky
druhou	druhý	k4xOgFnSc4	druhý
papírnu	papírna	k1gFnSc4	papírna
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
fungovala	fungovat	k5eAaImAgFnS	fungovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1812	[number]	k4	1812
lépe	dobře	k6eAd2	dobře
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
švagr	švagr	k1gMnSc1	švagr
majitele	majitel	k1gMnSc2	majitel
Gabriela	Gabriel	k1gMnSc2	Gabriel
Ettela	Ettel	k1gMnSc2	Ettel
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Kablík	kablík	k1gInSc4	kablík
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
novou	nový	k2eAgFnSc4d1	nová
metodu	metoda	k1gFnSc4	metoda
bělení	bělení	k1gNnSc2	bělení
a	a	k8xC	a
barvení	barvení	k1gNnSc2	barvení
papíru	papír	k1gInSc2	papír
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
výrobky	výrobek	k1gInPc1	výrobek
byly	být	k5eAaImAgInP	být
tak	tak	k6eAd1	tak
kvalitní	kvalitní	k2eAgInPc1d1	kvalitní
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1834	[number]	k4	1834
mohli	moct	k5eAaImAgMnP	moct
používat	používat	k5eAaImF	používat
titul	titul	k1gInSc4	titul
císařsko-královská	císařskorálovský	k2eAgFnSc1d1	císařsko-královská
zemská	zemský	k2eAgFnSc1d1	zemská
privilegovaná	privilegovaný	k2eAgFnSc1d1	privilegovaná
papírna	papírna	k1gFnSc1	papírna
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1815	[number]	k4	1815
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
městský	městský	k2eAgInSc1d1	městský
špitál	špitál	k1gInSc1	špitál
<g/>
,	,	kIx,	,
na	na	k7c6	na
čemž	což	k3yRnSc6	což
měli	mít	k5eAaImAgMnP	mít
hlavní	hlavní	k2eAgFnSc4d1	hlavní
zásluhu	zásluha	k1gFnSc4	zásluha
Anton	anton	k1gInSc4	anton
a	a	k8xC	a
Elizabeth	Elizabeth	k1gFnSc4	Elizabeth
Puntschuhovi	Puntschuh	k1gMnSc3	Puntschuh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1829	[number]	k4	1829
byla	být	k5eAaImAgFnS	být
blízko	blízko	k7c2	blízko
středu	střed	k1gInSc2	střed
města	město	k1gNnSc2	město
založena	založit	k5eAaPmNgFnS	založit
J.	J.	kA	J.
Seidlem	Seidel	k1gMnSc7	Seidel
tiskárna	tiskárna	k1gFnSc1	tiskárna
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
později	pozdě	k6eAd2	pozdě
rod	rod	k1gInSc1	rod
Wendtů	Wendt	k1gInPc2	Wendt
změnil	změnit	k5eAaPmAgInS	změnit
na	na	k7c4	na
bělidlo	bělidlo	k1gNnSc4	bělidlo
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
Podhůří	Podhůří	k1gNnSc6	Podhůří
byla	být	k5eAaImAgFnS	být
baronem	baron	k1gMnSc7	baron
Beustem	Beust	k1gInSc7	Beust
založena	založit	k5eAaPmNgFnS	založit
přádelna	přádelna	k1gFnSc1	přádelna
bavlny	bavlna	k1gFnSc2	bavlna
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1837	[number]	k4	1837
ve	v	k7c6	v
městě	město	k1gNnSc6	město
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
pošta	pošta	k1gFnSc1	pošta
a	a	k8xC	a
k	k	k7c3	k
pohánění	pohánění	k1gNnSc3	pohánění
mandlu	mandl	k1gInSc2	mandl
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
používat	používat	k5eAaImF	používat
parní	parní	k2eAgInSc4d1	parní
stroj	stroj	k1gInSc4	stroj
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
první	první	k4xOgNnSc1	první
využití	využití	k1gNnSc1	využití
parního	parní	k2eAgInSc2d1	parní
stroje	stroj	k1gInSc2	stroj
v	v	k7c6	v
tehdejším	tehdejší	k2eAgInSc6d1	tehdejší
Bydžovském	bydžovský	k2eAgInSc6d1	bydžovský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgFnSc1d1	stará
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
učilo	učit	k5eAaImAgNnS	učit
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
nevyhovující	vyhovující	k2eNgFnSc1d1	nevyhovující
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
stržena	stržen	k2eAgFnSc1d1	stržena
<g/>
.	.	kIx.	.
</s>
<s>
Učilo	učít	k5eAaPmAgNnS	učít
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
či	či	k8xC	či
v	v	k7c6	v
městském	městský	k2eAgInSc6d1	městský
špitálu	špitál	k1gInSc6	špitál
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgFnPc1d1	Nové
školy	škola	k1gFnPc1	škola
se	se	k3xPyFc4	se
žáci	žák	k1gMnPc1	žák
dočkali	dočkat	k5eAaPmAgMnP	dočkat
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1862	[number]	k4	1862
<g/>
.	.	kIx.	.
</s>
<s>
Revoluční	revoluční	k2eAgInSc4d1	revoluční
rok	rok	k1gInSc4	rok
1848	[number]	k4	1848
měl	mít	k5eAaImAgMnS	mít
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
klidný	klidný	k2eAgInSc4d1	klidný
průběh	průběh	k1gInSc4	průběh
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
se	s	k7c7	s
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
město	město	k1gNnSc1	město
dozvědělo	dozvědět	k5eAaPmAgNnS	dozvědět
o	o	k7c6	o
příslibu	příslib	k1gInSc6	příslib
konstituce	konstituce	k1gFnSc2	konstituce
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
navečer	navečer	k6eAd1	navečer
zaplavili	zaplavit	k5eAaPmAgMnP	zaplavit
lidé	člověk	k1gMnPc1	člověk
ulice	ulice	k1gFnSc2	ulice
a	a	k8xC	a
jásali	jásat	k5eAaImAgMnP	jásat
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
se	se	k3xPyFc4	se
však	však	k9	však
všechno	všechen	k3xTgNnSc1	všechen
otočilo	otočit	k5eAaPmAgNnS	otočit
a	a	k8xC	a
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
se	se	k3xPyFc4	se
sloužila	sloužit	k5eAaImAgFnS	sloužit
za	za	k7c2	za
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
oběti	oběť	k1gFnSc2	oběť
mše	mše	k1gFnSc2	mše
a	a	k8xC	a
na	na	k7c4	na
výzvu	výzva	k1gFnSc4	výzva
samotného	samotný	k2eAgMnSc2d1	samotný
starosty	starosta	k1gMnSc2	starosta
Franze	Franze	k1gFnSc2	Franze
Watzela	Watzela	k1gMnSc2	Watzela
se	se	k3xPyFc4	se
muži	muž	k1gMnPc7	muž
zapisovali	zapisovat	k5eAaImAgMnP	zapisovat
do	do	k7c2	do
národní	národní	k2eAgFnSc2d1	národní
gardy	garda	k1gFnSc2	garda
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
gardy	garda	k1gFnPc1	garda
fungovaly	fungovat	k5eAaImAgFnP	fungovat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1851	[number]	k4	1851
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
však	však	k9	však
změnil	změnit	k5eAaPmAgMnS	změnit
starosta	starosta	k1gMnSc1	starosta
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Franz	Franz	k1gMnSc1	Franz
Watzel	Watzel	k1gMnSc1	Watzel
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
poslancem	poslanec	k1gMnSc7	poslanec
kroměřížského	kroměřížský	k2eAgInSc2d1	kroměřížský
sněmu	sněm	k1gInSc2	sněm
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
sídlem	sídlo	k1gNnSc7	sídlo
okresního	okresní	k2eAgNnSc2d1	okresní
hejtmanství	hejtmanství	k1gNnSc2	hejtmanství
<g/>
,	,	kIx,	,
pod	pod	k7c4	pod
které	který	k3yQgMnPc4	který
spadaly	spadat	k5eAaPmAgInP	spadat
soudní	soudní	k2eAgInPc1d1	soudní
okresy	okres	k1gInPc1	okres
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
<g/>
,	,	kIx,	,
Jilemnici	Jilemnice	k1gFnSc6	Jilemnice
a	a	k8xC	a
Rokytnici	Rokytnice	k1gFnSc6	Rokytnice
nad	nad	k7c7	nad
Jizerou	Jizera	k1gFnSc7	Jizera
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
však	však	k9	však
trvalo	trvat	k5eAaImAgNnS	trvat
jenom	jenom	k9	jenom
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1855	[number]	k4	1855
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
rozdělil	rozdělit	k5eAaPmAgInS	rozdělit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
slučování	slučování	k1gNnSc2	slučování
však	však	k9	však
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
jiný	jiný	k2eAgInSc1d1	jiný
z	z	k7c2	z
vrchlabského	vrchlabský	k2eAgInSc2d1	vrchlabský
a	a	k8xC	a
hostinského	hostinský	k2eAgInSc2d1	hostinský
soudního	soudní	k2eAgInSc2d1	soudní
okresu	okres	k1gInSc2	okres
a	a	k8xC	a
až	až	k9	až
na	na	k7c4	na
malé	malý	k2eAgFnPc4d1	malá
změny	změna	k1gFnPc4	změna
vydržel	vydržet	k5eAaPmAgMnS	vydržet
až	až	k9	až
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Prusko-rakouská	pruskoakouský	k2eAgFnSc1d1	prusko-rakouská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1866	[number]	k4	1866
se	se	k3xPyFc4	se
naštěstí	naštěstí	k6eAd1	naštěstí
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
příliš	příliš	k6eAd1	příliš
nedotkla	dotknout	k5eNaPmAgNnP	dotknout
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
zde	zde	k6eAd1	zde
rekvírovány	rekvírován	k2eAgFnPc1d1	rekvírován
potraviny	potravina	k1gFnPc1	potravina
<g/>
,	,	kIx,	,
nápoje	nápoj	k1gInPc1	nápoj
a	a	k8xC	a
píce	píce	k1gFnSc1	píce
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgFnPc1d1	celková
škody	škoda	k1gFnPc1	škoda
města	město	k1gNnSc2	město
činily	činit	k5eAaImAgFnP	činit
6602	[number]	k4	6602
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
požádali	požádat	k5eAaPmAgMnP	požádat
významní	významný	k2eAgMnPc1d1	významný
vrchlabští	vrchlabský	k2eAgMnPc1d1	vrchlabský
občané	občan	k1gMnPc1	občan
Franz	Franz	k1gMnSc1	Franz
Ritschel	Ritschel	k1gMnSc1	Ritschel
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
velitel	velitel	k1gMnSc1	velitel
národní	národní	k2eAgFnSc2d1	národní
gardy	garda	k1gFnSc2	garda
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
<g/>
,	,	kIx,	,
Willibald	Willibald	k1gMnSc1	Willibald
Jerie	Jerie	k1gFnSc2	Jerie
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
Višňák	višňák	k1gMnSc1	višňák
o	o	k7c4	o
udělení	udělení	k1gNnSc4	udělení
povolení	povolení	k1gNnSc2	povolení
k	k	k7c3	k
postavení	postavení	k1gNnSc3	postavení
mechanické	mechanický	k2eAgFnSc2d1	mechanická
přádelny	přádelna	k1gFnSc2	přádelna
v	v	k7c6	v
Hořejším	hořejší	k2eAgNnSc6d1	hořejší
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgNnPc1	ten
byla	být	k5eAaImAgNnP	být
postavena	postavit	k5eAaPmNgNnP	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1852	[number]	k4	1852
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
bez	bez	k7c2	bez
J.	J.	kA	J.
Višňáka	višňák	k1gMnSc2	višňák
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
podíl	podíl	k1gInSc4	podíl
převzal	převzít	k5eAaPmAgMnS	převzít
Josef	Josef	k1gMnSc1	Josef
Stoczek	Stoczka	k1gFnPc2	Stoczka
(	(	kIx(	(
<g/>
Štoček	štoček	k1gInSc1	štoček
<g/>
)	)	kIx)	)
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
továrna	továrna	k1gFnSc1	továrna
měla	mít	k5eAaImAgFnS	mít
1500	[number]	k4	1500
vřeten	vřeteno	k1gNnPc2	vřeteno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k9	již
roku	rok	k1gInSc2	rok
1855	[number]	k4	1855
bylo	být	k5eAaImAgNnS	být
vřeten	vřeten	k1gInSc4	vřeten
4000	[number]	k4	4000
<g/>
.	.	kIx.	.
</s>
<s>
Personální	personální	k2eAgFnPc4d1	personální
změny	změna	k1gFnPc4	změna
pak	pak	k8xC	pak
továrnu	továrna	k1gFnSc4	továrna
čekaly	čekat	k5eAaImAgFnP	čekat
dále	daleko	k6eAd2	daleko
–	–	k?	–
odstoupili	odstoupit	k5eAaPmAgMnP	odstoupit
Jerie	Jerie	k1gFnPc4	Jerie
se	s	k7c7	s
Štočkem	štoček	k1gInSc7	štoček
a	a	k8xC	a
přistoupil	přistoupit	k5eAaPmAgMnS	přistoupit
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
A.	A.	kA	A.
Rotter	Rotter	k1gMnSc1	Rotter
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
původně	původně	k6eAd1	původně
v	v	k7c6	v
Králíkách	Králíkách	k?	Králíkách
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
Rotter	Rotter	k1gInSc1	Rotter
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
roku	rok	k1gInSc2	rok
1859	[number]	k4	1859
jediným	jediný	k2eAgMnSc7d1	jediný
majitelem	majitel	k1gMnSc7	majitel
přádelny	přádelna	k1gFnSc2	přádelna
<g/>
.	.	kIx.	.
</s>
<s>
Jerie	Jerie	k1gFnPc4	Jerie
měl	mít	k5eAaImAgInS	mít
svou	svůj	k3xOyFgFnSc4	svůj
přádelnu	přádelna	k1gFnSc4	přádelna
pod	pod	k7c7	pod
Liščím	liščí	k2eAgInSc7d1	liščí
kopcem	kopec	k1gInSc7	kopec
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
koupil	koupit	k5eAaPmAgMnS	koupit
původně	původně	k6eAd1	původně
Ettelovu	Ettelův	k2eAgFnSc4d1	Ettelův
papírnu	papírna	k1gFnSc4	papírna
<g/>
,	,	kIx,	,
zboural	zbourat	k5eAaPmAgMnS	zbourat
ji	on	k3xPp3gFnSc4	on
a	a	k8xC	a
postavil	postavit	k5eAaPmAgMnS	postavit
na	na	k7c6	na
pozemku	pozemek	k1gInSc6	pozemek
přádelnu	přádelna	k1gFnSc4	přádelna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
sloužit	sloužit	k5eAaImF	sloužit
roku	rok	k1gInSc2	rok
1857	[number]	k4	1857
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
přádelny	přádelna	k1gFnPc1	přádelna
patřily	patřit	k5eAaImAgFnP	patřit
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgNnSc4d3	veliký
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
měla	mít	k5eAaImAgFnS	mít
Rotterova	Rotterův	k2eAgFnSc1d1	Rotterův
20	[number]	k4	20
000	[number]	k4	000
vřeten	vřeteno	k1gNnPc2	vřeteno
<g/>
,	,	kIx,	,
Jeriova	Jeriův	k2eAgFnSc1d1	Jeriův
11	[number]	k4	11
000	[number]	k4	000
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
uváděno	uvádět	k5eAaImNgNnS	uvádět
již	již	k9	již
12	[number]	k4	12
000	[number]	k4	000
vřeten	vřeteno	k1gNnPc2	vřeteno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
opuštěné	opuštěný	k2eAgFnSc2d1	opuštěná
budovy	budova	k1gFnSc2	budova
pod	pod	k7c7	pod
Liščím	liščí	k2eAgInSc7d1	liščí
kopcem	kopec	k1gInSc7	kopec
po	po	k7c6	po
Willibaldovi	Willibald	k1gMnSc6	Willibald
Jeriemu	Jeriem	k1gMnSc6	Jeriem
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1857	[number]	k4	1857
dostali	dostat	k5eAaPmAgMnP	dostat
bratři	bratr	k1gMnPc1	bratr
Eugen	Eugno	k1gNnPc2	Eugno
a	a	k8xC	a
Victor	Victor	k1gMnSc1	Victor
Cypersovi	Cypers	k1gMnSc3	Cypers
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1863	[number]	k4	1863
nedaleko	nedaleko	k7c2	nedaleko
závodu	závod	k1gInSc2	závod
číslo	číslo	k1gNnSc4	číslo
II	II	kA	II
firmy	firma	k1gFnPc1	firma
Rotter	Rotter	k1gMnSc1	Rotter
v	v	k7c6	v
Hořejším	horní	k2eAgNnSc6d2	hořejší
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
vyhořela	vyhořet	k5eAaPmAgFnS	vyhořet
Dresselova	Dresselův	k2eAgFnSc1d1	Dresselův
papírna	papírna	k1gFnSc1	papírna
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
přádelna	přádelna	k1gFnSc1	přádelna
nedaleko	nedaleko	k7c2	nedaleko
středu	střed	k1gInSc2	střed
města	město	k1gNnSc2	město
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1864	[number]	k4	1864
občanem	občan	k1gMnSc7	občan
Goldschmidtem	Goldschmidt	k1gMnSc7	Goldschmidt
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
známky	známka	k1gFnPc1	známka
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
patrné	patrný	k2eAgFnSc2d1	patrná
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
odvětví	odvětví	k1gNnSc1	odvětví
průmyslu	průmysl	k1gInSc2	průmysl
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
města	město	k1gNnSc2	město
uvedeno	uvést	k5eAaPmNgNnS	uvést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1864	[number]	k4	1864
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Theodorem	Theodor	k1gMnSc7	Theodor
Peterou	Petera	k1gMnSc7	Petera
z	z	k7c2	z
Pecky	pecka	k1gFnSc2	pecka
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
kočáry	kočár	k1gInPc4	kočár
a	a	k8xC	a
sedlářství	sedlářství	k1gNnSc4	sedlářství
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
vrchlabského	vrchlabský	k2eAgInSc2d1	vrchlabský
průmyslu	průmysl	k1gInSc2	průmysl
bylo	být	k5eAaImAgNnS	být
rozhodující	rozhodující	k2eAgFnSc7d1	rozhodující
událostí	událost	k1gFnSc7	událost
založení	založení	k1gNnSc2	založení
tratě	trať	k1gFnPc4	trať
z	z	k7c2	z
Kunčic	Kunčice	k1gFnPc2	Kunčice
do	do	k7c2	do
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1872	[number]	k4	1872
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
město	město	k1gNnSc1	město
mělo	mít	k5eAaImAgNnS	mít
plynárnu	plynárna	k1gFnSc4	plynárna
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
i	i	k8xC	i
vlastní	vlastní	k2eAgInSc1d1	vlastní
vodovod	vodovod	k1gInSc1	vodovod
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
i	i	k9	i
elektrický	elektrický	k2eAgInSc4d1	elektrický
podnik	podnik	k1gInSc4	podnik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
zřízena	zřídit	k5eAaPmNgFnS	zřídit
první	první	k4xOgFnSc1	první
spořitelna	spořitelna	k1gFnSc1	spořitelna
(	(	kIx(	(
<g/>
Spořitelna	spořitelna	k1gFnSc1	spořitelna
města	město	k1gNnSc2	město
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
<g/>
)	)	kIx)	)
a	a	k8xC	a
záložna	záložna	k1gFnSc1	záložna
(	(	kIx(	(
<g/>
Okresní	okresní	k2eAgFnSc1d1	okresní
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
záložna	záložna	k1gFnSc1	záložna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
Česká	český	k2eAgFnSc1d1	Česká
Unionbanka	Unionbanka	k1gFnSc1	Unionbanka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
po	po	k7c4	po
zřícení	zřícení	k1gNnSc4	zřícení
střechy	střecha	k1gFnSc2	střecha
zbourán	zbourán	k2eAgInSc1d1	zbourán
panský	panský	k2eAgInSc1d1	panský
pivovar	pivovar	k1gInSc1	pivovar
na	na	k7c6	na
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Náměstí	náměstí	k1gNnSc6	náměstí
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
bez	bez	k7c2	bez
pivovaru	pivovar	k1gInSc2	pivovar
<g/>
,	,	kIx,	,
nový	nový	k2eAgInSc1d1	nový
akciový	akciový	k2eAgInSc1d1	akciový
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
funkčním	funkční	k2eAgNnSc7d1	funkční
roku	rok	k1gInSc2	rok
1903	[number]	k4	1903
<g/>
.	.	kIx.	.
<g/>
Spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
sodovkárnou	sodovkárna	k1gFnSc7	sodovkárna
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gInSc1	jeho
provoz	provoz	k1gInSc1	provoz
ukončen	ukončen	k2eAgInSc1d1	ukončen
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
byla	být	k5eAaImAgFnS	být
tkalcovská	tkalcovský	k2eAgFnSc1d1	tkalcovská
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1770	[number]	k4	1770
(	(	kIx(	(
<g/>
první	první	k4xOgFnPc4	první
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obnovena	obnoven	k2eAgNnPc4d1	obnoveno
natrvalo	natrvalo	k6eAd1	natrvalo
<g/>
.	.	kIx.	.
</s>
<s>
Gymnázium	gymnázium	k1gNnSc1	gymnázium
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
bylo	být	k5eAaImAgNnS	být
jako	jako	k8xC	jako
vyšší	vysoký	k2eAgFnSc1d2	vyšší
škola	škola	k1gFnSc1	škola
vybudované	vybudovaný	k2eAgFnSc2d1	vybudovaná
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1856	[number]	k4	1856
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
první	první	k4xOgFnSc1	první
soukromá	soukromý	k2eAgFnSc1d1	soukromá
nemocnice	nemocnice	k1gFnSc1	nemocnice
doktorem	doktor	k1gMnSc7	doktor
Hubenym	Hubenym	k?	Hubenym
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
byl	být	k5eAaImAgInS	být
hraběnkou	hraběnka	k1gFnSc7	hraběnka
Aloisií	Aloisie	k1gFnSc7	Aloisie
Czernin-Morzinovou	Czernin-Morzinová	k1gFnSc7	Czernin-Morzinová
v	v	k7c6	v
Husově	Husův	k2eAgFnSc6d1	Husova
ulici	ulice	k1gFnSc6	ulice
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
vrchnostenský	vrchnostenský	k2eAgInSc1d1	vrchnostenský
chorobinec	chorobinec	k1gInSc1	chorobinec
<g/>
,	,	kIx,	,
rozšíření	rozšíření	k1gNnSc1	rozšíření
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgMnS	dočkat
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
okresní	okresní	k2eAgFnSc1d1	okresní
nemocnice	nemocnice	k1gFnSc1	nemocnice
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
a	a	k8xC	a
před	před	k7c7	před
první	první	k4xOgFnSc7	první
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
se	se	k3xPyFc4	se
vybudování	vybudování	k1gNnSc2	vybudování
dočkal	dočkat	k5eAaPmAgMnS	dočkat
sirotčinec	sirotčinec	k1gInSc4	sirotčinec
a	a	k8xC	a
domov	domov	k1gInSc4	domov
sociální	sociální	k2eAgFnSc2d1	sociální
péče	péče	k1gFnSc2	péče
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
ochotníci	ochotník	k1gMnPc1	ochotník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
tvořili	tvořit	k5eAaImAgMnP	tvořit
významnou	významný	k2eAgFnSc4d1	významná
součást	součást	k1gFnSc4	součást
kulturního	kulturní	k2eAgInSc2d1	kulturní
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
bezpečně	bezpečně	k6eAd1	bezpečně
roku	rok	k1gInSc2	rok
1836	[number]	k4	1836
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1856	[number]	k4	1856
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
začal	začít	k5eAaPmAgInS	začít
fungovat	fungovat	k5eAaImF	fungovat
spolek	spolek	k1gInSc1	spolek
pro	pro	k7c4	pro
hudbu	hudba	k1gFnSc4	hudba
s	s	k7c7	s
názvem	název	k1gInSc7	název
Liedertafel	Liedertafela	k1gFnPc2	Liedertafela
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1859	[number]	k4	1859
se	se	k3xPyFc4	se
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
narodila	narodit	k5eAaPmAgFnS	narodit
významná	významný	k2eAgFnSc1d1	významná
hudební	hudební	k2eAgFnSc1d1	hudební
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
důležitost	důležitost	k1gFnSc4	důležitost
nebyla	být	k5eNaImAgFnS	být
dosud	dosud	k6eAd1	dosud
doceněna	doceněn	k2eAgFnSc1d1	doceněna
–	–	k?	–
Karel	Karel	k1gMnSc1	Karel
Halíř	halíř	k1gInSc1	halíř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1869	[number]	k4	1869
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
sbor	sbor	k1gInSc1	sbor
dobrovolných	dobrovolný	k2eAgMnPc2d1	dobrovolný
hasičů	hasič	k1gMnPc2	hasič
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
století	století	k1gNnSc6	století
přímo	přímo	k6eAd1	přímo
vznikaly	vznikat	k5eAaImAgInP	vznikat
spolky	spolek	k1gInPc4	spolek
školské	školská	k1gFnSc2	školská
<g/>
,	,	kIx,	,
dělnické	dělnický	k2eAgFnSc2d1	Dělnická
<g/>
,	,	kIx,	,
církevní	církevní	k2eAgFnSc2d1	církevní
<g/>
,	,	kIx,	,
obchodní	obchodní	k2eAgFnSc2d1	obchodní
<g/>
,	,	kIx,	,
finanční	finanční	k2eAgFnSc2d1	finanční
<g/>
,	,	kIx,	,
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
<g/>
,	,	kIx,	,
profesní	profesní	k2eAgFnSc2d1	profesní
<g/>
,	,	kIx,	,
nacionální	nacionální	k2eAgFnSc2d1	nacionální
<g/>
,	,	kIx,	,
politické	politický	k2eAgFnSc2d1	politická
<g/>
,	,	kIx,	,
bruslařské	bruslařský	k2eAgFnSc2d1	bruslařská
<g/>
,	,	kIx,	,
lyžařské	lyžařský	k2eAgFnSc2d1	lyžařská
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čtenářský	čtenářský	k2eAgInSc4d1	čtenářský
<g/>
,	,	kIx,	,
pěvecký	pěvecký	k2eAgInSc4d1	pěvecký
<g/>
,	,	kIx,	,
dohromady	dohromady	k6eAd1	dohromady
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
80	[number]	k4	80
spolků	spolek	k1gInPc2	spolek
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
spolkem	spolek	k1gInSc7	spolek
byl	být	k5eAaImAgInS	být
Rakouský	rakouský	k2eAgInSc1d1	rakouský
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Německý	německý	k2eAgInSc1d1	německý
krkonošský	krkonošský	k2eAgInSc1d1	krkonošský
spolek	spolek	k1gInSc1	spolek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
a	a	k8xC	a
propagoval	propagovat	k5eAaImAgMnS	propagovat
Krkonoše	Krkonoše	k1gFnPc4	Krkonoše
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
němu	on	k3xPp3gNnSc3	on
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1881	[number]	k4	1881
vrchlabská	vrchlabský	k2eAgFnSc1d1	vrchlabská
knihovna	knihovna	k1gFnSc1	knihovna
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1883	[number]	k4	1883
Krkonošské	krkonošský	k2eAgNnSc4d1	Krkonošské
muzeum	muzeum	k1gNnSc4	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
významnými	významný	k2eAgInPc7d1	významný
spolky	spolek	k1gInPc7	spolek
byla	být	k5eAaImAgFnS	být
Národní	národní	k2eAgFnSc1d1	národní
jednota	jednota	k1gFnSc1	jednota
severočeská	severočeský	k2eAgFnSc1d1	Severočeská
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
<g/>
.	.	kIx.	.
</s>
<s>
Vrchlabská	vrchlabský	k2eAgFnSc1d1	vrchlabská
pobočka	pobočka	k1gFnSc1	pobočka
Sokola	Sokol	k1gMnSc2	Sokol
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1888	[number]	k4	1888
byl	být	k5eAaImAgInS	být
zbourán	zbourat	k5eAaPmNgInS	zbourat
starý	starý	k2eAgInSc1d1	starý
kostel	kostel	k1gInSc1	kostel
a	a	k8xC	a
díky	díky	k7c3	díky
hraběnce	hraběnka	k1gFnSc3	hraběnka
Czernin-Morzinové	Czernin-Morzinové	k2eAgMnSc2d1	Czernin-Morzinové
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
postaven	postaven	k2eAgInSc4d1	postaven
nový	nový	k2eAgInSc4d1	nový
kostel	kostel	k1gInSc4	kostel
Stefanem	Stefan	k1gMnSc7	Stefan
Traglem	Tragl	k1gMnSc7	Tragl
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
modlitebna	modlitebna	k1gFnSc1	modlitebna
jiného	jiný	k2eAgNnSc2d1	jiné
než	než	k8xS	než
katolického	katolický	k2eAgNnSc2d1	katolické
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
,	,	kIx,	,
lutherská	lutherský	k2eAgFnSc1d1	lutherská
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
posvěcena	posvěcen	k2eAgFnSc1d1	posvěcena
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
začala	začít	k5eAaPmAgFnS	začít
sloužit	sloužit	k5eAaImF	sloužit
evangelické	evangelický	k2eAgFnSc3d1	evangelická
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
<s>
Evangelický	evangelický	k2eAgInSc1d1	evangelický
sbor	sbor	k1gInSc1	sbor
začal	začít	k5eAaPmAgInS	začít
fungovat	fungovat	k5eAaImF	fungovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
byla	být	k5eAaImAgFnS	být
u	u	k7c2	u
modlitebny	modlitebna	k1gFnSc2	modlitebna
postavená	postavený	k2eAgFnSc1d1	postavená
zvonička	zvonička	k1gFnSc1	zvonička
se	s	k7c7	s
zvony	zvon	k1gInPc7	zvon
nesoucími	nesoucí	k2eAgInPc7d1	nesoucí
jméno	jméno	k1gNnSc4	jméno
Martin	Martina	k1gFnPc2	Martina
Luther	Luthra	k1gFnPc2	Luthra
<g/>
,	,	kIx,	,
Ernest	Ernest	k1gMnSc1	Ernest
Moritz	Moritz	k1gMnSc1	Moritz
Arndt	Arndt	k1gMnSc1	Arndt
a	a	k8xC	a
Otto	Otto	k1gMnSc1	Otto
Bismarck	Bismarck	k1gMnSc1	Bismarck
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
července	červenec	k1gInSc2	červenec
1897	[number]	k4	1897
postihly	postihnout	k5eAaPmAgFnP	postihnout
město	město	k1gNnSc4	město
povodně	povodeň	k1gFnSc2	povodeň
<g/>
.	.	kIx.	.
</s>
<s>
Škoda	škoda	k1gFnSc1	škoda
činila	činit	k5eAaImAgFnS	činit
tehdejších	tehdejší	k2eAgInPc2d1	tehdejší
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
Dobový	dobový	k2eAgInSc1d1	dobový
tisk	tisk	k1gInSc1	tisk
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
bylo	být	k5eAaImAgNnS	být
poničeno	poničit	k5eAaPmNgNnS	poničit
osm	osm	k4xCc1	osm
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc1	tři
mosty	most	k1gInPc1	most
<g/>
,	,	kIx,	,
městský	městský	k2eAgInSc1d1	městský
vodovod	vodovod	k1gInSc1	vodovod
a	a	k8xC	a
plynárna	plynárna	k1gFnSc1	plynárna
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
plynojemem	plynojem	k1gInSc7	plynojem
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
voda	voda	k1gFnSc1	voda
také	také	k9	také
zničila	zničit	k5eAaPmAgFnS	zničit
příjezdovou	příjezdový	k2eAgFnSc4d1	příjezdová
cestu	cesta	k1gFnSc4	cesta
na	na	k7c4	na
Špindlerův	Špindlerův	k2eAgInSc4d1	Špindlerův
Mlýn	mlýn	k1gInSc4	mlýn
v	v	k7c6	v
Hořejším	hořejší	k2eAgNnSc6d1	hořejší
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
<g/>
,	,	kIx,	,
poničeno	poničen	k2eAgNnSc4d1	poničeno
bylo	být	k5eAaImAgNnS	být
Ettlovo	Ettlův	k2eAgNnSc1d1	Ettlův
bělidlo	bělidlo	k1gNnSc4	bělidlo
ležící	ležící	k2eAgNnSc4d1	ležící
pod	pod	k7c7	pod
Stavidlovým	stavidlový	k2eAgInSc7d1	stavidlový
vrchem	vrch	k1gInSc7	vrch
<g/>
,	,	kIx,	,
stržený	stržený	k2eAgInSc1d1	stržený
most	most	k1gInSc1	most
je	být	k5eAaImIp3nS	být
most	most	k1gInSc4	most
dnes	dnes	k6eAd1	dnes
ležící	ležící	k2eAgFnSc2d1	ležící
za	za	k7c7	za
budovou	budova	k1gFnSc7	budova
Hotelu	hotel	k1gInSc2	hotel
Gendorf	Gendorf	k1gInSc1	Gendorf
u	u	k7c2	u
"	"	kIx"	"
<g/>
Domu	dům	k1gInSc2	dům
u	u	k7c2	u
sedmi	sedm	k4xCc2	sedm
štítů	štít	k1gInPc2	štít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
padlo	padnout	k5eAaPmAgNnS	padnout
dohromady	dohromady	k6eAd1	dohromady
přes	přes	k7c4	přes
300	[number]	k4	300
osob	osoba	k1gFnPc2	osoba
z	z	k7c2	z
Hořejšího	Hořejšího	k2eAgNnPc2d1	Hořejšího
<g/>
,	,	kIx,	,
Dolejšího	Dolejšího	k2eAgNnPc2d1	Dolejšího
Vrchlabí	Vrchlabí	k1gNnPc2	Vrchlabí
a	a	k8xC	a
Podhůří	podhůří	k1gNnPc2	podhůří
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
samostatnosti	samostatnost	k1gFnSc2	samostatnost
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
vůči	vůči	k7c3	vůči
Rakousko-Uhersku	Rakousko-Uhersko	k1gNnSc3	Rakousko-Uhersko
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
Národní	národní	k2eAgInSc1d1	národní
výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
okres	okres	k1gInSc4	okres
vrchlabský	vrchlabský	k2eAgInSc4d1	vrchlabský
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
ředitelem	ředitel	k1gMnSc7	ředitel
školy	škola	k1gFnSc2	škola
Robertem	Robert	k1gMnSc7	Robert
Turkou	Turka	k1gMnSc7	Turka
<g/>
.	.	kIx.	.
</s>
<s>
Reakcí	reakce	k1gFnSc7	reakce
na	na	k7c6	na
osamostatnění	osamostatnění	k1gNnSc6	osamostatnění
bylo	být	k5eAaImAgNnS	být
založení	založení	k1gNnSc1	založení
separatistické	separatistický	k2eAgFnSc2d1	separatistická
vlády	vláda	k1gFnSc2	vláda
Deutschböhmen	Deutschböhmen	k1gInSc1	Deutschböhmen
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
a	a	k8xC	a
této	tento	k3xDgFnSc3	tento
vládě	vláda	k1gFnSc3	vláda
slíbily	slíbit	k5eAaPmAgInP	slíbit
věrnost	věrnost	k1gFnSc1	věrnost
všechny	všechen	k3xTgInPc4	všechen
důležité	důležitý	k2eAgInPc4d1	důležitý
úřady	úřad	k1gInPc4	úřad
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
dvojvládí	dvojvládí	k1gNnSc1	dvojvládí
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
odstraněno	odstranit	k5eAaPmNgNnS	odstranit
<g/>
,	,	kIx,	,
vpochodovala	vpochodovat	k5eAaPmAgFnS	vpochodovat
do	do	k7c2	do
města	město	k1gNnSc2	město
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1918	[number]	k4	1918
jičínská	jičínský	k2eAgFnSc1d1	Jičínská
vojenská	vojenský	k2eAgFnSc1d1	vojenská
jednotka	jednotka	k1gFnSc1	jednotka
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Sokoly	Sokol	k1gMnPc7	Sokol
z	z	k7c2	z
Horní	horní	k2eAgFnSc2d1	horní
Branné	branný	k2eAgFnSc2d1	Branná
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
směrů	směr	k1gInPc2	směr
<g/>
:	:	kIx,	:
od	od	k7c2	od
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
přijeli	přijet	k5eAaPmAgMnP	přijet
vlakem	vlak	k1gInSc7	vlak
<g/>
,	,	kIx,	,
a	a	k8xC	a
od	od	k7c2	od
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
obsazeno	obsadit	k5eAaPmNgNnS	obsadit
do	do	k7c2	do
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Osoby	osoba	k1gFnPc1	osoba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
proti	proti	k7c3	proti
české	český	k2eAgFnSc3d1	Česká
samostatnosti	samostatnost	k1gFnSc3	samostatnost
a	a	k8xC	a
slíbily	slíbit	k5eAaPmAgInP	slíbit
věrnost	věrnost	k1gFnSc4	věrnost
separatistům	separatista	k1gMnPc3	separatista
<g/>
,	,	kIx,	,
musely	muset	k5eAaImAgFnP	muset
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
postů	post	k1gInPc2	post
odejít	odejít	k5eAaPmF	odejít
a	a	k8xC	a
město	město	k1gNnSc1	město
dostalo	dostat	k5eAaPmAgNnS	dostat
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
posádku	posádka	k1gFnSc4	posádka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
sídlila	sídlit	k5eAaImAgFnS	sídlit
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
Zum	Zum	k1gFnSc2	Zum
Mohren	Mohrna	k1gFnPc2	Mohrna
poblíž	poblíž	k7c2	poblíž
Náměstí	náměstí	k1gNnPc2	náměstí
Tomáše	Tomáš	k1gMnSc2	Tomáš
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Městská	městský	k2eAgFnSc1d1	městská
knihovna	knihovna	k1gFnSc1	knihovna
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
<g/>
)	)	kIx)	)
a	a	k8xC	a
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
2	[number]	k4	2
<g/>
.	.	kIx.	.
rotou	rota	k1gFnSc7	rota
hraničářského	hraničářský	k2eAgInSc2d1	hraničářský
praporu	prapor	k1gInSc2	prapor
2	[number]	k4	2
z	z	k7c2	z
Trutnova	Trutnov	k1gInSc2	Trutnov
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
nespokojenost	nespokojenost	k1gFnSc4	nespokojenost
Němci	Němec	k1gMnPc1	Němec
dali	dát	k5eAaPmAgMnP	dát
najevo	najevo	k6eAd1	najevo
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
generální	generální	k2eAgFnSc4d1	generální
stávku	stávka	k1gFnSc4	stávka
<g/>
.	.	kIx.	.
</s>
<s>
Střet	střet	k1gInSc1	střet
demonstrujících	demonstrující	k2eAgMnPc2d1	demonstrující
s	s	k7c7	s
armádou	armáda	k1gFnSc7	armáda
byl	být	k5eAaImAgInS	být
hladký	hladký	k2eAgMnSc1d1	hladký
<g/>
,	,	kIx,	,
jenom	jenom	k9	jenom
si	se	k3xPyFc3	se
nadávali	nadávat	k5eAaImAgMnP	nadávat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sousedním	sousední	k2eAgNnSc6d1	sousední
Hostinném	hostinný	k2eAgNnSc6d1	Hostinné
byly	být	k5eAaImAgFnP	být
zabity	zabít	k5eAaPmNgFnP	zabít
dvě	dva	k4xCgFnPc1	dva
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
se	se	k3xPyFc4	se
během	během	k7c2	během
prvních	první	k4xOgInPc2	první
dvou	dva	k4xCgInPc2	dva
let	léto	k1gNnPc2	léto
od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
nové	nový	k2eAgFnSc2d1	nová
republiky	republika	k1gFnSc2	republika
pokusili	pokusit	k5eAaPmAgMnP	pokusit
o	o	k7c4	o
demonstrace	demonstrace	k1gFnPc4	demonstrace
<g/>
,	,	kIx,	,
k	k	k7c3	k
jedné	jeden	k4xCgFnSc3	jeden
větší	veliký	k2eAgFnSc3d2	veliký
došlo	dojít	k5eAaPmAgNnS	dojít
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
okresním	okresní	k2eAgInSc6d1	okresní
úřadě	úřad	k1gInSc6	úřad
ztlučen	ztlučen	k2eAgMnSc1d1	ztlučen
komisař	komisař	k1gMnSc1	komisař
Kraus	Kraus	k1gMnSc1	Kraus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
té	ten	k3xDgFnSc6	ten
se	se	k3xPyFc4	se
však	však	k9	však
podíleli	podílet	k5eAaImAgMnP	podílet
hlavně	hlavně	k9	hlavně
čeští	český	k2eAgMnPc1d1	český
dělníci	dělník	k1gMnPc1	dělník
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byly	být	k5eAaImAgFnP	být
velice	velice	k6eAd1	velice
malé	malý	k2eAgInPc1d1	malý
příděly	příděl	k1gInPc1	příděl
potravin	potravina	k1gFnPc2	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
interpelace	interpelace	k1gFnSc2	interpelace
poslanců	poslanec	k1gMnPc2	poslanec
na	na	k7c4	na
ministry	ministr	k1gMnPc4	ministr
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
,	,	kIx,	,
národní	národní	k2eAgFnSc2d1	národní
obrany	obrana	k1gFnSc2	obrana
a	a	k8xC	a
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
z	z	k7c2	z
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1920	[number]	k4	1920
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
0,25	[number]	k4	0,25
kg	kg	kA	kg
mouky	mouka	k1gFnSc2	mouka
na	na	k7c4	na
vaření	vaření	k1gNnSc4	vaření
<g/>
,	,	kIx,	,
0,25	[number]	k4	0,25
kg	kg	kA	kg
ovesné	ovesný	k2eAgFnSc2d1	ovesná
rýže	rýže	k1gFnSc2	rýže
<g/>
,	,	kIx,	,
0,25	[number]	k4	0,25
až	až	k9	až
0,5	[number]	k4	0,5
kg	kg	kA	kg
brambor	brambora	k1gFnPc2	brambora
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
většinou	většinou	k6eAd1	většinou
ve	v	k7c6	v
velice	velice	k6eAd1	velice
špatném	špatný	k2eAgInSc6d1	špatný
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
stále	stále	k6eAd1	stále
neměnila	měnit	k5eNaImAgFnS	měnit
k	k	k7c3	k
lepšímu	dobrý	k2eAgNnSc3d2	lepší
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
závodech	závod	k1gInPc6	závod
zastavena	zastaven	k2eAgFnSc1d1	zastavena
dělnická	dělnický	k2eAgFnSc1d1	Dělnická
práce	práce	k1gFnSc1	práce
a	a	k8xC	a
dohromady	dohromady	k6eAd1	dohromady
3000	[number]	k4	3000
nespokojenců	nespokojenec	k1gMnPc2	nespokojenec
z	z	k7c2	z
Hořejšího	Hořejšího	k2eAgNnSc2d1	Hořejšího
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
<g/>
,	,	kIx,	,
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
a	a	k8xC	a
Dolní	dolní	k2eAgFnPc1d1	dolní
Branné	branný	k2eAgFnPc1d1	Branná
přišlo	přijít	k5eAaPmAgNnS	přijít
před	před	k7c4	před
okresní	okresní	k2eAgNnSc4d1	okresní
hejtmanství	hejtmanství	k1gNnSc4	hejtmanství
<g/>
.	.	kIx.	.
</s>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
z	z	k7c2	z
jejich	jejich	k3xOp3gFnPc2	jejich
řad	řada	k1gFnPc2	řada
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vydali	vydat	k5eAaPmAgMnP	vydat
k	k	k7c3	k
okresnímu	okresní	k2eAgMnSc3d1	okresní
hejtmanu	hejtman	k1gMnSc3	hejtman
Gogelovi	Gogel	k1gMnSc3	Gogel
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
oznámili	oznámit	k5eAaPmAgMnP	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
neodejdou	odejít	k5eNaPmIp3nP	odejít
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
jim	on	k3xPp3gFnPc3	on
nebude	být	k5eNaImBp3nS	být
zajištěn	zajistit	k5eAaPmNgInS	zajistit
lepší	dobrý	k2eAgInSc1d2	lepší
přísun	přísun	k1gInSc1	přísun
potravin	potravina	k1gFnPc2	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
Gogela	Gogela	k1gFnSc1	Gogela
a	a	k8xC	a
přednosta	přednosta	k1gMnSc1	přednosta
obilního	obilní	k2eAgInSc2d1	obilní
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
komisař	komisař	k1gMnSc1	komisař
Kraus	Kraus	k1gMnSc1	Kraus
<g/>
,	,	kIx,	,
jednání	jednání	k1gNnSc1	jednání
pro	pro	k7c4	pro
nespokojence	nespokojenec	k1gMnSc4	nespokojenec
nesnesitelně	snesitelně	k6eNd1	snesitelně
zdržovali	zdržovat	k5eAaImAgMnP	zdržovat
<g/>
,	,	kIx,	,
nespokojenci	nespokojenec	k1gMnPc1	nespokojenec
zaútočili	zaútočit	k5eAaPmAgMnP	zaútočit
na	na	k7c4	na
budovu	budova	k1gFnSc4	budova
a	a	k8xC	a
vojenská	vojenský	k2eAgFnSc1d1	vojenská
a	a	k8xC	a
četnická	četnický	k2eAgFnSc1d1	četnická
ostraha	ostraha	k1gFnSc1	ostraha
před	před	k7c7	před
nimi	on	k3xPp3gMnPc7	on
ustoupila	ustoupit	k5eAaPmAgFnS	ustoupit
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
nespokojenci	nespokojenec	k1gMnPc1	nespokojenec
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
do	do	k7c2	do
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
zadržovali	zadržovat	k5eAaImAgMnP	zadržovat
je	on	k3xPp3gNnSc4	on
sami	sám	k3xTgMnPc1	sám
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
přišli	přijít	k5eAaPmAgMnP	přijít
nahoru	nahoru	k6eAd1	nahoru
vyjednávat	vyjednávat	k5eAaImF	vyjednávat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
však	však	k9	však
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgMnS	být
komisař	komisař	k1gMnSc1	komisař
Krauss	Krauss	k1gInSc4	Krauss
zbit	zbit	k2eAgInSc4d1	zbit
nespokojenými	spokojený	k2eNgInPc7d1	nespokojený
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budově	budova	k1gFnSc6	budova
nespokojení	nespokojení	k1gNnSc2	nespokojení
ničili	ničit	k5eAaImAgMnP	ničit
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
vybavení	vybavení	k1gNnSc4	vybavení
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnPc4d3	veliký
škody	škoda	k1gFnPc4	škoda
snad	snad	k9	snad
způsobili	způsobit	k5eAaPmAgMnP	způsobit
<g/>
,	,	kIx,	,
když	když	k8xS	když
zničili	zničit	k5eAaPmAgMnP	zničit
telefonní	telefonní	k2eAgFnSc4d1	telefonní
centrálu	centrála	k1gFnSc4	centrála
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
znovuobnovení	znovuobnovení	k1gNnSc6	znovuobnovení
klidu	klid	k1gInSc2	klid
musela	muset	k5eAaImAgFnS	muset
přijít	přijít	k5eAaPmF	přijít
vojenská	vojenský	k2eAgFnSc1d1	vojenská
posila	posila	k1gFnSc1	posila
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
1	[number]	k4	1
<g/>
.	.	kIx.	.
roty	rota	k1gFnSc2	rota
hraničářského	hraničářský	k2eAgInSc2d1	hraničářský
praporu	prapor	k1gInSc2	prapor
z	z	k7c2	z
Trutnova	Trutnov	k1gInSc2	Trutnov
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
velitel	velitel	k1gMnSc1	velitel
do	do	k7c2	do
hlášení	hlášení	k1gNnSc2	hlášení
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
:	:	kIx,	:
Ihned	ihned	k6eAd1	ihned
druhého	druhý	k4xOgInSc2	druhý
dne	den	k1gInSc2	den
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
vyhláška	vyhláška	k1gFnSc1	vyhláška
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgFnPc4	který
například	například	k6eAd1	například
stálo	stát	k5eAaImAgNnS	stát
<g/>
:	:	kIx,	:
A	a	k9	a
následně	následně	k6eAd1	následně
nastalo	nastat	k5eAaPmAgNnS	nastat
i	i	k9	i
zatýkání	zatýkání	k1gNnSc1	zatýkání
-	-	kIx~	-
16	[number]	k4	16
osob	osoba	k1gFnPc2	osoba
bylo	být	k5eAaImAgNnS	být
zatčeno	zatknout	k5eAaPmNgNnS	zatknout
a	a	k8xC	a
posláno	poslán	k2eAgNnSc1d1	Posláno
k	k	k7c3	k
soudu	soud	k1gInSc3	soud
v	v	k7c6	v
Jičíně	Jičín	k1gInSc6	Jičín
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
uvězněni	uvěznit	k5eAaPmNgMnP	uvěznit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
města	město	k1gNnSc2	město
též	též	k9	též
přibyla	přibýt	k5eAaPmAgFnS	přibýt
vojenská	vojenský	k2eAgFnSc1d1	vojenská
posádka	posádka	k1gFnSc1	posádka
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
350	[number]	k4	350
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
městu	město	k1gNnSc3	město
hodně	hodně	k6eAd1	hodně
přitížilo	přitížit	k5eAaPmAgNnS	přitížit
a	a	k8xC	a
na	na	k7c4	na
vydržování	vydržování	k1gNnSc4	vydržování
této	tento	k3xDgFnSc2	tento
ostrahy	ostraha	k1gFnSc2	ostraha
si	se	k3xPyFc3	se
muselo	muset	k5eAaImAgNnS	muset
vypůjčit	vypůjčit	k5eAaPmF	vypůjčit
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
napsali	napsat	k5eAaPmAgMnP	napsat
poslanci	poslanec	k1gMnPc1	poslanec
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Kreibichem	Kreibich	k1gMnSc7	Kreibich
<g/>
,	,	kIx,	,
Antonínem	Antonín	k1gMnSc7	Antonín
Roscherem	Roscher	k1gMnSc7	Roscher
a	a	k8xC	a
Otto	Otto	k1gMnSc1	Otto
Hahnem	Hahn	k1gInSc7	Hahn
interpelaci	interpelace	k1gFnSc4	interpelace
na	na	k7c4	na
ministry	ministr	k1gMnPc4	ministr
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
,	,	kIx,	,
národní	národní	k2eAgFnSc2d1	národní
obrany	obrana	k1gFnSc2	obrana
a	a	k8xC	a
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
požádali	požádat	k5eAaPmAgMnP	požádat
o	o	k7c4	o
stažení	stažení	k1gNnSc4	stažení
posádky	posádka	k1gFnSc2	posádka
<g/>
,	,	kIx,	,
sesazení	sesazení	k1gNnSc4	sesazení
komisaře	komisař	k1gMnSc2	komisař
Krause	Kraus	k1gMnSc2	Kraus
a	a	k8xC	a
zrychlení	zrychlení	k1gNnSc1	zrychlení
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
zatčených	zatčený	k1gMnPc2	zatčený
v	v	k7c6	v
soudě	soud	k1gInSc6	soud
v	v	k7c6	v
Jičíně	Jičín	k1gInSc6	Jičín
<g/>
.	.	kIx.	.
</s>
<s>
Vyhověno	vyhověn	k2eAgNnSc1d1	vyhověno
jim	on	k3xPp3gMnPc3	on
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
bodech	bod	k1gInPc6	bod
skutečně	skutečně	k6eAd1	skutečně
bylo	být	k5eAaImAgNnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
se	se	k3xPyFc4	se
posádka	posádka	k1gFnSc1	posádka
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
úplně	úplně	k6eAd1	úplně
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
bylo	být	k5eAaImAgNnS	být
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
bez	bez	k7c2	bez
vojenské	vojenský	k2eAgFnSc2d1	vojenská
posádky	posádka	k1gFnSc2	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vazbě	vazba	k1gFnSc6	vazba
každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
nespokojených	spokojený	k2eNgMnPc2d1	nespokojený
pobyl	pobýt	k5eAaPmAgMnS	pobýt
různě	různě	k6eAd1	různě
dlouho	dlouho	k6eAd1	dlouho
<g/>
:	:	kIx,	:
jeden	jeden	k4xCgMnSc1	jeden
od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
do	do	k7c2	do
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
dva	dva	k4xCgMnPc1	dva
od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
do	do	k7c2	do
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
,	,	kIx,	,
dva	dva	k4xCgMnPc1	dva
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
do	do	k7c2	do
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
,	,	kIx,	,
pět	pět	k4xCc4	pět
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
do	do	k7c2	do
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
,	,	kIx,	,
pět	pět	k4xCc4	pět
od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
do	do	k7c2	do
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
od	od	k7c2	od
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
do	do	k7c2	do
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Přednosta	přednosta	k1gMnSc1	přednosta
obilního	obilní	k2eAgInSc2d1	obilní
úřadu	úřad	k1gInSc2	úřad
Kraus	Kraus	k1gMnSc1	Kraus
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
místě	místo	k1gNnSc6	místo
zůstal	zůstat	k5eAaPmAgMnS	zůstat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
ministr	ministr	k1gMnSc1	ministr
vnitra	vnitro	k1gNnSc2	vnitro
usoudil	usoudit	k5eAaPmAgMnS	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
vůbec	vůbec	k9	vůbec
nedařilo	dařit	k5eNaImAgNnS	dařit
lnářství	lnářství	k1gNnSc1	lnářství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
byla	být	k5eAaImAgFnS	být
Jerieho	Jerie	k1gMnSc4	Jerie
přádelna	přádelna	k1gFnSc1	přádelna
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
a	a	k8xC	a
Rotterova	Rotterův	k2eAgFnSc1d1	Rotterův
přádelna	přádelna	k1gFnSc1	přádelna
se	se	k3xPyFc4	se
dala	dát	k5eAaPmAgFnS	dát
na	na	k7c4	na
spřádání	spřádání	k1gNnSc4	spřádání
juty	juta	k1gFnSc2	juta
a	a	k8xC	a
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
papírovou	papírový	k2eAgFnSc4d1	papírová
přízi	příze	k1gFnSc4	příze
<g/>
.	.	kIx.	.
</s>
<s>
Automobilový	automobilový	k2eAgInSc1d1	automobilový
průmysl	průmysl	k1gInSc1	průmysl
byl	být	k5eAaImAgInS	být
zastoupen	zastoupit	k5eAaPmNgInS	zastoupit
firmou	firma	k1gFnSc7	firma
Petera	Peter	k1gMnSc2	Peter
a	a	k8xC	a
synové	syn	k1gMnPc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Arnold	Arnold	k1gMnSc1	Arnold
Löwit	Löwit	k1gMnSc1	Löwit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
založil	založit	k5eAaPmAgMnS	založit
továrnu	továrna	k1gFnSc4	továrna
na	na	k7c4	na
elektrické	elektrický	k2eAgInPc4d1	elektrický
vodiče	vodič	k1gInPc4	vodič
a	a	k8xC	a
kabely	kabel	k1gInPc4	kabel
v	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
textilce	textilka	k1gFnSc6	textilka
<g/>
.	.	kIx.	.
</s>
<s>
Nacionální	nacionální	k2eAgInPc1d1	nacionální
problémy	problém	k1gInPc1	problém
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
důležitými	důležitý	k2eAgFnPc7d1	důležitá
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Velké	velký	k2eAgFnSc2d1	velká
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
dostala	dostat	k5eAaPmAgFnS	dostat
Sudetoněmecká	sudetoněmecký	k2eAgFnSc1d1	Sudetoněmecká
strana	strana	k1gFnSc1	strana
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Konrádem	Konrád	k1gMnSc7	Konrád
Henleinem	Henlein	k1gMnSc7	Henlein
64,1	[number]	k4	64,1
%	%	kIx~	%
všech	všecek	k3xTgInPc2	všecek
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
v	v	k7c6	v
soudním	soudní	k2eAgInSc6d1	soudní
okrese	okres	k1gInSc6	okres
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
pak	pak	k6eAd1	pak
59,8	[number]	k4	59,8
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
v	v	k7c6	v
sousedním	sousední	k2eAgInSc6d1	sousední
maršovském	maršovský	k2eAgInSc6d1	maršovský
okrese	okres	k1gInSc6	okres
získali	získat	k5eAaPmAgMnP	získat
o	o	k7c6	o
10	[number]	k4	10
%	%	kIx~	%
navíc	navíc	k6eAd1	navíc
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
strana	strana	k1gFnSc1	strana
také	také	k9	také
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
na	na	k7c6	na
radnici	radnice	k1gFnSc6	radnice
většinu	většina	k1gFnSc4	většina
s	s	k7c7	s
nacistickým	nacistický	k2eAgMnSc7d1	nacistický
starostou	starosta	k1gMnSc7	starosta
JUDr.	JUDr.	kA	JUDr.
Karl	Karl	k1gMnSc1	Karl
Wendtem	Wendt	k1gInSc7	Wendt
<g/>
,	,	kIx,	,
bývalým	bývalý	k2eAgMnSc7d1	bývalý
právníkem	právník	k1gMnSc7	právník
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
však	však	k9	však
zemřel	zemřít	k5eAaPmAgMnS	zemřít
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
<s>
Vrchlabští	vrchlabský	k2eAgMnPc1d1	vrchlabský
nacisté	nacista	k1gMnPc1	nacista
již	již	k6eAd1	již
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1938	[number]	k4	1938
založili	založit	k5eAaPmAgMnP	založit
šest	šest	k4xCc1	šest
družstev	družstvo	k1gNnPc2	družstvo
vojenské	vojenský	k2eAgFnSc2d1	vojenská
jednotky	jednotka	k1gFnSc2	jednotka
Freiwilliger	Freiwilliger	k1gMnSc1	Freiwilliger
Schutzdienst	Schutzdienst	k1gMnSc1	Schutzdienst
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
SA	SA	kA	SA
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
strany	strana	k1gFnPc1	strana
kromě	kromě	k7c2	kromě
SdP	SdP	k1gMnPc2	SdP
<g/>
,	,	kIx,	,
sociálních	sociální	k2eAgMnPc2d1	sociální
demokratů	demokrat	k1gMnPc2	demokrat
a	a	k8xC	a
komunistů	komunista	k1gMnPc2	komunista
se	se	k3xPyFc4	se
rozpadly	rozpadnout	k5eAaPmAgInP	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
připojení	připojení	k1gNnSc6	připojení
Sudet	Sudety	k1gInPc2	Sudety
k	k	k7c3	k
Německu	Německo	k1gNnSc3	Německo
bylo	být	k5eAaImAgNnS	být
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
nadšeno	nadšen	k2eAgNnSc1d1	nadšeno
<g/>
.	.	kIx.	.
</s>
<s>
Wehrmacht	wehrmacht	k1gFnSc1	wehrmacht
do	do	k7c2	do
města	město	k1gNnSc2	město
vpochodoval	vpochodovat	k5eAaPmAgInS	vpochodovat
8	[number]	k4	8
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1938	[number]	k4	1938
skrze	skrze	k?	skrze
vyzdobené	vyzdobený	k2eAgFnSc2d1	vyzdobená
slavobrány	slavobrána	k1gFnSc2	slavobrána
<g/>
.	.	kIx.	.
</s>
<s>
Freikorps	Freikorps	k1gInSc1	Freikorps
přišel	přijít	k5eAaPmAgInS	přijít
až	až	k9	až
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
z	z	k7c2	z
většiny	většina	k1gFnSc2	většina
prchlo	prchnout	k5eAaPmAgNnS	prchnout
společně	společně	k6eAd1	společně
s	s	k7c7	s
antifašisty	antifašista	k1gMnPc7	antifašista
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původních	původní	k2eAgMnPc2d1	původní
1200	[number]	k4	1200
Čechů	Čech	k1gMnPc2	Čech
jich	on	k3xPp3gFnPc2	on
zbylo	zbýt	k5eAaPmAgNnS	zbýt
jenom	jenom	k9	jenom
220	[number]	k4	220
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
gestapo	gestapo	k1gNnSc1	gestapo
zatklo	zatknout	k5eAaPmAgNnS	zatknout
29	[number]	k4	29
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
sedm	sedm	k4xCc1	sedm
jich	on	k3xPp3gFnPc2	on
bylo	být	k5eAaImAgNnS	být
posláno	poslat	k5eAaPmNgNnS	poslat
do	do	k7c2	do
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
změně	změna	k1gFnSc6	změna
hranic	hranice	k1gFnPc2	hranice
Československa	Československo	k1gNnSc2	Československo
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
landrát	landrát	k1gInSc1	landrát
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dohromady	dohromady	k6eAd1	dohromady
spravoval	spravovat	k5eAaImAgInS	spravovat
62	[number]	k4	62
obcí	obec	k1gFnPc2	obec
v	v	k7c6	v
Podkrkonoší	Podkrkonoší	k1gNnSc6	Podkrkonoší
<g/>
.	.	kIx.	.
</s>
<s>
Namísto	namísto	k7c2	namísto
odvedených	odvedený	k2eAgMnPc2d1	odvedený
mužů	muž	k1gMnPc2	muž
se	se	k3xPyFc4	se
do	do	k7c2	do
města	město	k1gNnSc2	město
dostali	dostat	k5eAaPmAgMnP	dostat
zajatci	zajatec	k1gMnPc1	zajatec
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
Francouzů	Francouz	k1gMnPc2	Francouz
<g/>
,	,	kIx,	,
Angličanů	Angličan	k1gMnPc2	Angličan
<g/>
,	,	kIx,	,
Nizozemců	Nizozemec	k1gMnPc2	Nizozemec
<g/>
,	,	kIx,	,
Belgičanů	Belgičan	k1gMnPc2	Belgičan
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
národností	národnost	k1gFnPc2	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Lágry	lágr	k1gInPc1	lágr
stály	stát	k5eAaImAgInP	stát
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dnešní	dnešní	k2eAgFnSc2d1	dnešní
automobilky	automobilka	k1gFnSc2	automobilka
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
říkalo	říkat	k5eAaImAgNnS	říkat
na	na	k7c6	na
Ehingerově	Ehingerův	k2eAgFnSc6d1	Ehingerův
louce	louka	k1gFnSc6	louka
<g/>
,	,	kIx,	,
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
továrny	továrna	k1gFnSc2	továrna
Friedrich	Friedrich	k1gMnSc1	Friedrich
Stolzenberg	Stolzenberg	k1gMnSc1	Stolzenberg
<g/>
,	,	kIx,	,
u	u	k7c2	u
letiště	letiště	k1gNnSc2	letiště
<g/>
,	,	kIx,	,
v	v	k7c6	v
Dolních	dolní	k2eAgFnPc6d1	dolní
Herlíkovicích	Herlíkovice	k1gFnPc6	Herlíkovice
byl	být	k5eAaImAgInS	být
lágr	lágr	k1gInSc1	lágr
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
rekreačním	rekreační	k2eAgInSc6d1	rekreační
areálu	areál	k1gInSc6	areál
Eden	Eden	k1gInSc4	Eden
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
nad	nad	k7c7	nad
dnešním	dnešní	k2eAgNnSc7d1	dnešní
autobusovým	autobusový	k2eAgNnSc7d1	autobusové
nádražím	nádraží	k1gNnSc7	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
byly	být	k5eAaImAgInP	být
na	na	k7c6	na
kostele	kostel	k1gInSc6	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
zrekvírovány	zrekvírován	k2eAgInPc1d1	zrekvírován
zvony	zvon	k1gInPc1	zvon
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vydržely	vydržet	k5eAaPmAgFnP	vydržet
i	i	k9	i
první	první	k4xOgFnSc4	první
světovou	světový	k2eAgFnSc4d1	světová
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
války	válka	k1gFnSc2	válka
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
začala	začít	k5eAaPmAgFnS	začít
působit	působit	k5eAaImF	působit
německá	německý	k2eAgFnSc1d1	německá
firma	firma	k1gFnSc1	firma
Friedrich	Friedrich	k1gMnSc1	Friedrich
Stolzenberg	Stolzenberg	k1gMnSc1	Stolzenberg
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
dnešní	dnešní	k2eAgFnSc2d1	dnešní
firmy	firma	k1gFnSc2	firma
Labit	Labita	k1gFnPc2	Labita
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
motory	motor	k1gInPc4	motor
pro	pro	k7c4	pro
německé	německý	k2eAgInPc4d1	německý
letouny	letoun	k1gInPc4	letoun
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
součásti	součást	k1gFnPc1	součást
k	k	k7c3	k
řízeným	řízený	k2eAgFnPc3d1	řízená
střelám	střela	k1gFnPc3	střela
V-	V-	k1gFnSc2	V-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Peterova	Peterův	k2eAgFnSc1d1	Peterova
automobilka	automobilka	k1gFnSc1	automobilka
přešla	přejít	k5eAaPmAgFnS	přejít
na	na	k7c4	na
vojenské	vojenský	k2eAgInPc4d1	vojenský
vozy	vůz	k1gInPc4	vůz
a	a	k8xC	a
kluzáky	kluzák	k1gInPc4	kluzák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Hořejším	hořejší	k2eAgNnSc6d1	hořejší
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
začala	začít	k5eAaPmAgFnS	začít
působit	působit	k5eAaImF	působit
firma	firma	k1gFnSc1	firma
Lorenz	Lorenz	k1gMnSc1	Lorenz
ag	ag	k?	ag
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
elektronky	elektronka	k1gFnPc4	elektronka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
-	-	kIx~	-
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
gestapo	gestapo	k1gNnSc1	gestapo
stihlo	stihnout	k5eAaPmAgNnS	stihnout
na	na	k7c4	na
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
neexistujícím	existující	k2eNgInSc6d1	neexistující
židovském	židovský	k2eAgInSc6d1	židovský
hřbitově	hřbitov	k1gInSc6	hřbitov
nad	nad	k7c7	nad
civilním	civilní	k2eAgInSc7d1	civilní
hřbitovem	hřbitov	k1gInSc7	hřbitov
zastřelit	zastřelit	k5eAaPmF	zastřelit
šest	šest	k4xCc1	šest
ruských	ruský	k2eAgMnPc2d1	ruský
zajatců	zajatec	k1gMnPc2	zajatec
<g/>
.	.	kIx.	.
</s>
<s>
Osvobozující	osvobozující	k2eAgMnPc1d1	osvobozující
Sověti	Sovět	k1gMnPc1	Sovět
do	do	k7c2	do
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
přišli	přijít	k5eAaPmAgMnP	přijít
před	před	k7c7	před
půlnocí	půlnoc	k1gFnSc7	půlnoc
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
sídlil	sídlit	k5eAaImAgInS	sídlit
hlavní	hlavní	k2eAgInSc1d1	hlavní
štáb	štáb	k1gInSc1	štáb
sovětských	sovětský	k2eAgNnPc2d1	sovětské
vojsk	vojsko	k1gNnPc2	vojsko
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
Labuť	labuť	k1gFnSc1	labuť
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
předsedou	předseda	k1gMnSc7	předseda
Městského	městský	k2eAgInSc2d1	městský
národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
byl	být	k5eAaImAgMnS	být
Alois	Alois	k1gMnSc1	Alois
Vaňous	Vaňous	k1gMnSc1	Vaňous
<g/>
,	,	kIx,	,
zástupcem	zástupce	k1gMnSc7	zástupce
Otto	Otta	k1gMnSc5	Otta
Herbe	Herb	k1gMnSc5	Herb
<g/>
,	,	kIx,	,
tajemníkem	tajemník	k1gMnSc7	tajemník
J.	J.	kA	J.
Bulušek	Buluška	k1gFnPc2	Buluška
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1945	[number]	k4	1945
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
díky	díky	k7c3	díky
novým	nový	k2eAgMnPc3d1	nový
osídlencům	osídlenec	k1gMnPc3	osídlenec
3	[number]	k4	3
555	[number]	k4	555
Čechů	Čech	k1gMnPc2	Čech
na	na	k7c4	na
2	[number]	k4	2
566	[number]	k4	566
Němců	Němec	k1gMnPc2	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Odsuny	odsun	k1gInPc1	odsun
obyvatel	obyvatel	k1gMnPc2	obyvatel
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
začaly	začít	k5eAaPmAgInP	začít
již	již	k6eAd1	již
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
dne	den	k1gInSc2	den
do	do	k7c2	do
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
celkem	celkem	k6eAd1	celkem
25	[number]	k4	25
transportech	transport	k1gInPc6	transport
z	z	k7c2	z
okresů	okres	k1gInPc2	okres
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
a	a	k8xC	a
Jilemnice	Jilemnice	k1gFnSc2	Jilemnice
vypraveno	vypravit	k5eAaPmNgNnS	vypravit
19	[number]	k4	19
070	[number]	k4	070
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Řádný	řádný	k2eAgInSc1d1	řádný
odsun	odsun	k1gInSc1	odsun
Němců	Němec	k1gMnPc2	Němec
do	do	k7c2	do
americké	americký	k2eAgFnSc2d1	americká
zóny	zóna	k1gFnSc2	zóna
přes	přes	k7c4	přes
Domažlice	Domažlice	k1gFnPc4	Domažlice
začal	začít	k5eAaPmAgMnS	začít
ve	v	k7c6	v
vrchlabském	vrchlabský	k2eAgInSc6d1	vrchlabský
okrese	okres	k1gInSc6	okres
až	až	k6eAd1	až
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1946	[number]	k4	1946
odjel	odjet	k5eAaPmAgMnS	odjet
z	z	k7c2	z
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
první	první	k4xOgInSc4	první
oficiální	oficiální	k2eAgInSc4d1	oficiální
transport	transport	k1gInSc4	transport
hned	hned	k6eAd1	hned
po	po	k7c6	po
setmění	setmění	k1gNnSc6	setmění
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dochováno	dochován	k2eAgNnSc1d1	dochováno
<g/>
,	,	kIx,	,
že	že	k8xS	že
odjel	odjet	k5eAaPmAgMnS	odjet
za	za	k7c2	za
velmi	velmi	k6eAd1	velmi
mrazivého	mrazivý	k2eAgInSc2d1	mrazivý
větru	vítr	k1gInSc2	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
dnech	den	k1gInPc6	den
jízdy	jízda	k1gFnSc2	jízda
vlak	vlak	k1gInSc1	vlak
přijel	přijet	k5eAaPmAgInS	přijet
do	do	k7c2	do
Odenwaldu	Odenwald	k1gInSc2	Odenwald
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
půle	půle	k1gFnSc1	půle
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
do	do	k7c2	do
Bensheimu	Bensheim	k1gInSc2	Bensheim
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
půle	půle	k1gFnSc1	půle
do	do	k7c2	do
Heppenheimu	Heppenheim	k1gInSc2	Heppenheim
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
u	u	k7c2	u
sebe	sebe	k3xPyFc4	sebe
mohl	moct	k5eAaImAgInS	moct
mít	mít	k5eAaImF	mít
zavazadlo	zavazadlo	k1gNnSc1	zavazadlo
vážící	vážící	k2eAgNnSc1d1	vážící
maximálně	maximálně	k6eAd1	maximálně
50	[number]	k4	50
kilogramů	kilogram	k1gInPc2	kilogram
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
mít	mít	k5eAaImF	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
500	[number]	k4	500
říšských	říšský	k2eAgFnPc2d1	říšská
marek	marka	k1gFnPc2	marka
<g/>
.	.	kIx.	.
</s>
<s>
Transport	transport	k1gInSc1	transport
číslo	číslo	k1gNnSc1	číslo
dvě	dva	k4xCgNnPc1	dva
vyjel	vyjet	k5eAaPmAgInS	vyjet
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
okolo	okolo	k7c2	okolo
deseti	deset	k4xCc2	deset
hodin	hodina	k1gFnPc2	hodina
večer	večer	k6eAd1	večer
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
cestě	cesta	k1gFnSc6	cesta
přes	přes	k7c4	přes
Prahu	Praha	k1gFnSc4	Praha
dojel	dojet	k5eAaPmAgInS	dojet
transport	transport	k1gInSc1	transport
čtvrtého	čtvrtý	k4xOgInSc2	čtvrtý
dne	den	k1gInSc2	den
jízdy	jízda	k1gFnSc2	jízda
do	do	k7c2	do
Frankenbergu	Frankenberg	k1gInSc2	Frankenberg
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
v	v	k7c6	v
10.20	[number]	k4	10.20
odjeli	odjet	k5eAaPmAgMnP	odjet
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
co	co	k9	co
čekali	čekat	k5eAaImAgMnP	čekat
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgNnSc2d1	dnešní
fotbalového	fotbalový	k2eAgNnSc2d1	fotbalové
hřiště	hřiště	k1gNnSc2	hřiště
u	u	k7c2	u
automobilky	automobilka	k1gFnSc2	automobilka
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
transport	transport	k1gInSc1	transport
mířil	mířit	k5eAaImAgInS	mířit
do	do	k7c2	do
Dachau	Dachaus	k1gInSc2	Dachaus
<g/>
,	,	kIx,	,
Bad	Bad	k1gMnSc3	Bad
Reichenhallu	Reichenhall	k1gMnSc3	Reichenhall
a	a	k8xC	a
Berchtesgadenu	Berchtesgaden	k2eAgFnSc4d1	Berchtesgaden
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
transporty	transport	k1gInPc1	transport
přepravily	přepravit	k5eAaPmAgInP	přepravit
Němce	Němka	k1gFnSc3	Němka
do	do	k7c2	do
Augsburgu	Augsburg	k1gInSc2	Augsburg
<g/>
,	,	kIx,	,
Kasselu	Kassel	k1gInSc2	Kassel
<g/>
,	,	kIx,	,
Frankenbergu	Frankenberg	k1gInSc2	Frankenberg
<g/>
,	,	kIx,	,
Heidelbergu	Heidelberg	k1gInSc2	Heidelberg
<g/>
,	,	kIx,	,
Zeitzu	Zeitza	k1gFnSc4	Zeitza
<g/>
,	,	kIx,	,
Nordhausenu	Nordhausen	k2eAgFnSc4d1	Nordhausen
<g/>
,	,	kIx,	,
další	další	k2eAgInPc1d1	další
čtyři	čtyři	k4xCgInPc1	čtyři
transporty	transport	k1gInPc1	transport
směřovaly	směřovat	k5eAaImAgInP	směřovat
do	do	k7c2	do
sovětské	sovětský	k2eAgFnSc2d1	sovětská
okupační	okupační	k2eAgFnSc2d1	okupační
zóny	zóna	k1gFnSc2	zóna
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgMnPc1d1	sociální
demokraté	demokrat	k1gMnPc1	demokrat
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
Hostinného	hostinný	k2eAgNnSc2d1	Hostinné
z	z	k7c2	z
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
jeli	jet	k5eAaImAgMnP	jet
do	do	k7c2	do
Aschaffenburgu	Aschaffenburg	k1gInSc2	Aschaffenburg
<g/>
,	,	kIx,	,
antifašisté	antifašista	k1gMnPc1	antifašista
odjeli	odjet	k5eAaPmAgMnP	odjet
do	do	k7c2	do
Kemptenu	Kempten	k1gInSc2	Kempten
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc1d1	další
transporty	transporta	k1gFnPc1	transporta
směřovaly	směřovat	k5eAaImAgFnP	směřovat
znovu	znovu	k6eAd1	znovu
k	k	k7c3	k
Sovětům	Sověty	k1gInPc3	Sověty
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
do	do	k7c2	do
měst	město	k1gNnPc2	město
Gera	Gera	k1gFnSc1	Gera
<g/>
,	,	kIx,	,
Mittweida	Mittweida	k1gFnSc1	Mittweida
<g/>
,	,	kIx,	,
Pirna	Pirna	k1gFnSc1	Pirna
a	a	k8xC	a
Usedom	Usedom	k1gInSc1	Usedom
<g/>
,	,	kIx,	,
281	[number]	k4	281
antifašistů	antifašista	k1gMnPc2	antifašista
do	do	k7c2	do
německého	německý	k2eAgNnSc2d1	německé
města	město	k1gNnSc2	město
Marktoberdorf	Marktoberdorf	k1gMnSc1	Marktoberdorf
přivezlo	přivézt	k5eAaPmAgNnS	přivézt
prapor	prapor	k1gInSc4	prapor
města	město	k1gNnSc2	město
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
<g/>
.	.	kIx.	.
</s>
<s>
Tajně	tajně	k6eAd1	tajně
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
dalších	další	k2eAgInPc6d1	další
transportech	transport	k1gInPc6	transport
následoval	následovat	k5eAaImAgInS	následovat
poslední	poslední	k2eAgInSc1d1	poslední
do	do	k7c2	do
města	město	k1gNnSc2	město
Pfarrkirchen	Pfarrkirchna	k1gFnPc2	Pfarrkirchna
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1946	[number]	k4	1946
byl	být	k5eAaImAgInS	být
odsun	odsun	k1gInSc4	odsun
u	u	k7c2	u
konce	konec	k1gInSc2	konec
<g/>
.	.	kIx.	.
</s>
<s>
Německy	německy	k6eAd1	německy
mluvící	mluvící	k2eAgNnSc1d1	mluvící
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
Vrchlabska	Vrchlabsko	k1gNnSc2	Vrchlabsko
se	se	k3xPyFc4	se
ocitlo	ocitnout	k5eAaPmAgNnS	ocitnout
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
bylo	být	k5eAaImAgNnS	být
5481	[number]	k4	5481
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
pouhých	pouhý	k2eAgMnPc2d1	pouhý
197	[number]	k4	197
Němců	Němec	k1gMnPc2	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
se	se	k3xPyFc4	se
po	po	k7c6	po
odsídlení	odsídlení	k1gNnSc6	odsídlení
veškerého	veškerý	k3xTgNnSc2	veškerý
německého	německý	k2eAgNnSc2d1	německé
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
naštěstí	naštěstí	k6eAd1	naštěstí
nestalo	stát	k5eNaPmAgNnS	stát
revírem	revír	k1gInSc7	revír
vykrádačů	vykrádač	k1gMnPc2	vykrádač
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
školy	škola	k1gFnPc1	škola
svojí	svojit	k5eAaImIp3nP	svojit
funkci	funkce	k1gFnSc3	funkce
začaly	začít	k5eAaPmAgFnP	začít
plnit	plnit	k5eAaImF	plnit
už	už	k6eAd1	už
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
však	však	k9	však
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
jasnou	jasný	k2eAgFnSc4d1	jasná
převahu	převaha	k1gFnSc4	převaha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
získala	získat	k5eAaPmAgFnS	získat
60	[number]	k4	60
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Následný	následný	k2eAgInSc1d1	následný
převrat	převrat	k1gInSc1	převrat
na	na	k7c4	na
Vrchlabsku	Vrchlabska	k1gFnSc4	Vrchlabska
tedy	tedy	k8xC	tedy
proběhl	proběhnout	k5eAaPmAgMnS	proběhnout
bez	bez	k7c2	bez
větších	veliký	k2eAgInPc2d2	veliký
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
velká	velký	k2eAgFnSc1d1	velká
správní	správní	k2eAgFnSc1d1	správní
změna	změna	k1gFnSc1	změna
<g/>
:	:	kIx,	:
k	k	k7c3	k
dříve	dříve	k6eAd2	dříve
samostatné	samostatný	k2eAgFnSc3d1	samostatná
obci	obec	k1gFnSc3	obec
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
se	se	k3xPyFc4	se
připojily	připojit	k5eAaPmAgInP	připojit
obce	obec	k1gFnSc2	obec
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
samostatné	samostatný	k2eAgFnSc2d1	samostatná
<g/>
)	)	kIx)	)
Hořejší	Hořejší	k2eAgNnSc1d1	Hořejší
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
(	(	kIx(	(
<g/>
čítající	čítající	k2eAgFnPc1d1	čítající
tehdy	tehdy	k6eAd1	tehdy
Kněžice	kněžice	k1gFnPc4	kněžice
<g/>
,	,	kIx,	,
Hořejší	Hořejší	k2eAgNnSc4d1	Hořejší
Vrchlabí	Vrchlabí	k1gNnSc4	Vrchlabí
a	a	k8xC	a
část	část	k1gFnSc4	část
Herlíkovic	Herlíkovice	k1gFnPc2	Herlíkovice
<g/>
)	)	kIx)	)
a	a	k8xC	a
Podhůří	Podhůří	k1gNnSc1	Podhůří
(	(	kIx(	(
<g/>
Podhůří	Podhůří	k1gNnSc1	Podhůří
a	a	k8xC	a
Liščí	liščí	k2eAgInSc1d1	liščí
kopec	kopec	k1gInSc1	kopec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
ochotníci	ochotník	k1gMnPc1	ochotník
zahájili	zahájit	k5eAaPmAgMnP	zahájit
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Divadelní	divadelní	k2eAgFnSc1d1	divadelní
jednota	jednota	k1gFnSc1	jednota
Šír	šíro	k1gNnPc2	šíro
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
vlastence	vlastenec	k1gMnSc2	vlastenec
a	a	k8xC	a
spisovatele	spisovatel	k1gMnSc2	spisovatel
Josefa	Josef	k1gMnSc2	Josef
Šíra	Šír	k1gMnSc2	Šír
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
schůzka	schůzka	k1gFnSc1	schůzka
byla	být	k5eAaImAgFnS	být
svolaná	svolaný	k2eAgFnSc1d1	svolaná
již	již	k9	již
na	na	k7c4	na
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
tohoto	tento	k3xDgInSc2	tento
souboru	soubor	k1gInSc2	soubor
stanuli	stanout	k5eAaPmAgMnP	stanout
J.	J.	kA	J.
Štěpánek	Štěpánka	k1gFnPc2	Štěpánka
a	a	k8xC	a
J.	J.	kA	J.
Veigl	Veigl	k1gInSc1	Veigl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
stanul	stanout	k5eAaPmAgMnS	stanout
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
souboru	soubor	k1gInSc2	soubor
Jaroslav	Jaroslava	k1gFnPc2	Jaroslava
Kašlík	kašlík	k1gInSc1	kašlík
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
stanuly	stanout	k5eAaPmAgFnP	stanout
významné	významný	k2eAgFnPc1d1	významná
osobnosti	osobnost	k1gFnPc1	osobnost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
nakladatel	nakladatel	k1gMnSc1	nakladatel
Josef	Josef	k1gMnSc1	Josef
Krbal	Krbal	k1gMnSc1	Krbal
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
vrchlabské	vrchlabský	k2eAgFnSc2d1	vrchlabská
Městské	městský	k2eAgFnSc2d1	městská
hudební	hudební	k2eAgFnSc2d1	hudební
školy	škola	k1gFnSc2	škola
Josef	Josef	k1gMnSc1	Josef
Václav	Václav	k1gMnSc1	Václav
Kratochvíl	Kratochvíl	k1gMnSc1	Kratochvíl
či	či	k8xC	či
jiní	jiný	k2eAgMnPc1d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
V.	V.	kA	V.
Kratochvíl	Kratochvíl	k1gMnSc1	Kratochvíl
ještě	ještě	k6eAd1	ještě
prvního	první	k4xOgInSc2	první
poválečného	poválečný	k2eAgInSc2d1	poválečný
roku	rok	k1gInSc2	rok
založil	založit	k5eAaPmAgMnS	založit
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
Městskou	městský	k2eAgFnSc4d1	městská
hudební	hudební	k2eAgFnSc4d1	hudební
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
národopisný	národopisný	k2eAgInSc4d1	národopisný
soubor	soubor	k1gInSc4	soubor
Krakonoš	Krakonoš	k1gMnSc1	Krakonoš
skládajícího	skládající	k2eAgNnSc2d1	skládající
se	se	k3xPyFc4	se
za	za	k7c2	za
smíšeného	smíšený	k2eAgInSc2d1	smíšený
pěveckého	pěvecký	k2eAgInSc2d1	pěvecký
sboru	sbor	k1gInSc2	sbor
<g/>
,	,	kIx,	,
dětského	dětský	k2eAgInSc2d1	dětský
pěveckého	pěvecký	k2eAgInSc2d1	pěvecký
kolektivu	kolektiv	k1gInSc2	kolektiv
<g/>
,	,	kIx,	,
tanečního	taneční	k2eAgInSc2d1	taneční
kolektivu	kolektiv	k1gInSc2	kolektiv
<g/>
,	,	kIx,	,
kolektivu	kolektiv	k1gInSc2	kolektiv
vypravěčů	vypravěč	k1gMnPc2	vypravěč
a	a	k8xC	a
z	z	k7c2	z
doprovodné	doprovodný	k2eAgFnSc2d1	doprovodná
instrumentální	instrumentální	k2eAgFnSc2d1	instrumentální
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
soubor	soubor	k1gInSc1	soubor
měl	mít	k5eAaImAgInS	mít
například	například	k6eAd1	například
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
programu	program	k1gInSc6	program
pásmo	pásmo	k1gNnSc4	pásmo
Krakonošův	Krakonošův	k2eAgInSc1d1	Krakonošův
rok	rok	k1gInSc1	rok
od	od	k7c2	od
krkonošských	krkonošský	k2eAgFnPc2d1	Krkonošská
autorek	autorka	k1gFnPc2	autorka
Amálie	Amálie	k1gFnSc2	Amálie
Kutinové	kutinový	k2eAgFnSc2d1	Kutinová
a	a	k8xC	a
Marie	Maria	k1gFnSc2	Maria
Kubátové	Kubátová	k1gFnSc2	Kubátová
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Krkonošské	krkonošský	k2eAgNnSc1d1	Krkonošské
muzeum	muzeum	k1gNnSc1	muzeum
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
činnosti	činnost	k1gFnSc6	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
předválečné	předválečný	k2eAgFnSc6d1	předválečná
činnosti	činnost	k1gFnSc6	činnost
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
evangelická	evangelický	k2eAgFnSc1d1	evangelická
církev	církev	k1gFnSc1	církev
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
,	,	kIx,	,
na	na	k7c4	na
její	její	k3xOp3gNnSc4	její
místo	místo	k1gNnSc4	místo
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
modlitebnu	modlitebna	k1gFnSc4	modlitebna
převzala	převzít	k5eAaPmAgFnS	převzít
Českobratrská	českobratrský	k2eAgFnSc1d1	Českobratrská
církev	církev	k1gFnSc1	církev
evangelická	evangelický	k2eAgFnSc1d1	evangelická
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
oficiálně	oficiálně	k6eAd1	oficiálně
svůj	svůj	k3xOyFgInSc4	svůj
farní	farní	k2eAgInSc4d1	farní
sbor	sbor	k1gInSc4	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
církev	církev	k1gFnSc1	církev
husitská	husitský	k2eAgFnSc1d1	husitská
do	do	k7c2	do
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
přesunula	přesunout	k5eAaPmAgFnS	přesunout
svůj	svůj	k3xOyFgInSc4	svůj
farní	farní	k2eAgInSc4d1	farní
sbor	sbor	k1gInSc4	sbor
z	z	k7c2	z
Dolní	dolní	k2eAgFnSc2d1	dolní
Kalné	Kalná	k1gFnSc2	Kalná
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
bohoslužebným	bohoslužebný	k2eAgInPc3d1	bohoslužebný
účelům	účel	k1gInPc3	účel
začala	začít	k5eAaPmAgFnS	začít
využívat	využívat	k5eAaImF	využívat
bývalou	bývalý	k2eAgFnSc4d1	bývalá
zámeckou	zámecký	k2eAgFnSc4d1	zámecká
Morzinskou	Morzinský	k2eAgFnSc4d1	Morzinský
hrobku	hrobka	k1gFnSc4	hrobka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
vrchlabský	vrchlabský	k2eAgInSc1d1	vrchlabský
průmysl	průmysl	k1gInSc1	průmysl
více	hodně	k6eAd2	hodně
méně	málo	k6eAd2	málo
věnoval	věnovat	k5eAaPmAgInS	věnovat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
čím	co	k3yQnSc7	co
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Elektronky	elektronka	k1gFnPc1	elektronka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
Lorenzova	Lorenzův	k2eAgFnSc1d1	Lorenzova
firma	firma	k1gFnSc1	firma
<g/>
,	,	kIx,	,
převzala	převzít	k5eAaPmAgFnS	převzít
firma	firma	k1gFnSc1	firma
Tesla	Tesla	k1gFnSc1	Tesla
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
OCZ	OCZ	kA	OCZ
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Strojní	strojní	k2eAgFnSc4d1	strojní
výrobu	výroba	k1gFnSc4	výroba
po	po	k7c6	po
firmě	firma	k1gFnSc6	firma
Stolzenberg	Stolzenberg	k1gInSc4	Stolzenberg
převzala	převzít	k5eAaPmAgFnS	převzít
firma	firma	k1gFnSc1	firma
TOS	TOS	kA	TOS
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
Argo-Hytos	Argo-Hytos	k1gInSc1	Argo-Hytos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Peterově	Peterův	k2eAgFnSc6d1	Peterova
firmě	firma	k1gFnSc6	firma
převzala	převzít	k5eAaPmAgFnS	převzít
výrobu	výroba	k1gFnSc4	výroba
firma	firma	k1gFnSc1	firma
Škoda	škoda	k6eAd1	škoda
Auto	auto	k1gNnSc1	auto
<g/>
.	.	kIx.	.
</s>
<s>
Poválečné	poválečný	k2eAgNnSc1d1	poválečné
období	období	k1gNnSc1	období
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
definitivní	definitivní	k2eAgInSc4d1	definitivní
konec	konec	k1gInSc4	konec
lnářství	lnářství	k1gNnSc2	lnářství
<g/>
.	.	kIx.	.
</s>
<s>
Textilní	textilní	k2eAgFnSc1d1	textilní
výroba	výroba	k1gFnSc1	výroba
byla	být	k5eAaImAgFnS	být
převzata	převzít	k5eAaPmNgFnS	převzít
firmami	firma	k1gFnPc7	firma
Mileta	Mileta	k1gFnSc1	Mileta
a	a	k8xC	a
Tiba	tiba	k1gFnSc1	tiba
<g/>
.	.	kIx.	.
</s>
<s>
Vodiče	vodič	k1gInPc4	vodič
začala	začít	k5eAaPmAgFnS	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
firma	firma	k1gFnSc1	firma
Kablo	Kablo	k1gNnSc4	Kablo
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
později	pozdě	k6eAd2	pozdě
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
i	i	k9	i
elektrotechnický	elektrotechnický	k2eAgInSc1d1	elektrotechnický
průmysl	průmysl	k1gInSc1	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
zrušen	zrušit	k5eAaPmNgInS	zrušit
okresní	okresní	k2eAgInSc1d1	okresní
úřad	úřad	k1gInSc1	úřad
a	a	k8xC	a
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
začalo	začít	k5eAaPmAgNnS	začít
patřit	patřit	k5eAaImF	patřit
do	do	k7c2	do
trutnovského	trutnovský	k2eAgInSc2d1	trutnovský
okresu	okres	k1gInSc2	okres
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
v	v	k7c6	v
závorce	závorka	k1gFnSc6	závorka
první	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
<g/>
/	/	kIx~	/
<g/>
osadě	osada	k1gFnSc6	osada
<g/>
)	)	kIx)	)
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
1	[number]	k4	1
(	(	kIx(	(
<g/>
katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
<g/>
)	)	kIx)	)
samotné	samotný	k2eAgNnSc1d1	samotné
centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
s	s	k7c7	s
osadou	osada	k1gFnSc7	osada
Třídomí	Třídomí	k1gNnSc4	Třídomí
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1807	[number]	k4	1807
<g/>
)	)	kIx)	)
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
3	[number]	k4	3
(	(	kIx(	(
<g/>
katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
Podhůří-Harta	Podhůří-Harta	k1gFnSc1	Podhůří-Harta
<g/>
)	)	kIx)	)
osada	osada	k1gFnSc1	osada
Liščí	liščit	k5eAaPmIp3nS	liščit
Kopec	kopec	k1gInSc1	kopec
(	(	kIx(	(
<g/>
1561	[number]	k4	1561
<g/>
)	)	kIx)	)
a	a	k8xC	a
obec	obec	k1gFnSc1	obec
Podhůří	Podhůří	k1gNnSc2	Podhůří
(	(	kIx(	(
<g/>
1574	[number]	k4	1574
<g/>
)	)	kIx)	)
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
4	[number]	k4	4
(	(	kIx(	(
<g/>
katastrální	katastrální	k2eAgNnPc1d1	katastrální
území	území	k1gNnPc1	území
Hořejší	Hořejší	k2eAgNnSc2d1	Hořejší
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
osad	osada	k1gFnPc2	osada
Hořejší	Hořejší	k2eAgFnSc2d1	Hořejší
Herlíkovice	Herlíkovice	k1gFnSc2	Herlíkovice
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
1606	[number]	k4	1606
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
obce	obec	k1gFnPc1	obec
Hořejší	Hořejší	k2eAgNnSc2d1	Hořejší
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
1624	[number]	k4	1624
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
osady	osada	k1gFnSc2	osada
Kamenná	kamenný	k2eAgFnSc1d1	kamenná
cesta	cesta	k1gFnSc1	cesta
(	(	kIx(	(
<g/>
rok	rok	k1gInSc1	rok
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kněžice	kněžice	k1gFnSc1	kněžice
(	(	kIx(	(
<g/>
1454	[number]	k4	1454
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
osady	osada	k1gFnSc2	osada
Seidlovy	Seidlův	k2eAgInPc1d1	Seidlův
Domky	domek	k1gInPc1	domek
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Třídomí	Třídomí	k1gNnSc1	Třídomí
(	(	kIx(	(
<g/>
1807	[number]	k4	1807
<g/>
)	)	kIx)	)
V	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
osídleno	osídlen	k2eAgNnSc1d1	osídleno
dřevorubci	dřevorubec	k1gMnPc7	dřevorubec
a	a	k8xC	a
uhlíři	uhlíř	k1gMnPc7	uhlíř
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
díky	díky	k7c3	díky
objevení	objevení	k1gNnSc3	objevení
rudných	rudný	k2eAgFnPc2d1	Rudná
žil	žíla	k1gFnPc2	žíla
rozvoj	rozvoj	k1gInSc4	rozvoj
hornictví	hornictví	k1gNnSc2	hornictví
<g/>
,	,	kIx,	,
hutnictví	hutnictví	k1gNnSc2	hutnictví
a	a	k8xC	a
železářství	železářství	k1gNnSc2	železářství
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
železárenská	železárenský	k2eAgFnSc1d1	Železárenská
výroba	výroba	k1gFnSc1	výroba
<g/>
,	,	kIx,	,
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
specializovaná	specializovaný	k2eAgFnSc1d1	specializovaná
na	na	k7c4	na
kosy	kosa	k1gFnPc4	kosa
přeorientovala	přeorientovat	k5eAaPmAgFnS	přeorientovat
na	na	k7c4	na
válečnou	válečný	k2eAgFnSc4d1	válečná
produkci	produkce	k1gFnSc4	produkce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téže	tenže	k3xDgFnSc6	tenže
době	doba	k1gFnSc6	doba
začátky	začátek	k1gInPc1	začátek
textilního	textilní	k2eAgInSc2d1	textilní
(	(	kIx(	(
<g/>
lnářského	lnářský	k2eAgInSc2d1	lnářský
<g/>
)	)	kIx)	)
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
taktéž	taktéž	k?	taktéž
varhanářství	varhanářství	k1gNnSc1	varhanářství
<g/>
,	,	kIx,	,
specializovaná	specializovaný	k2eAgFnSc1d1	specializovaná
výroba	výroba	k1gFnSc1	výroba
varhan	varhany	k1gFnPc2	varhany
rodinou	rodina	k1gFnSc7	rodina
Tauchmanových	Tauchmanová	k1gFnPc2	Tauchmanová
<g/>
.	.	kIx.	.
</s>
<s>
Vrchlabské	vrchlabský	k2eAgInPc4d1	vrchlabský
cechy	cech	k1gInPc4	cech
soukeníků	soukeník	k1gMnPc2	soukeník
a	a	k8xC	a
pláteníků	pláteník	k1gMnPc2	pláteník
navázaly	navázat	k5eAaPmAgFnP	navázat
například	například	k6eAd1	například
styky	styk	k1gInPc1	styk
s	s	k7c7	s
norimberskými	norimberský	k2eAgInPc7d1	norimberský
obchodními	obchodní	k2eAgInPc7d1	obchodní
domy	dům	k1gInPc7	dům
Gewandschneiderem	Gewandschneider	k1gInSc7	Gewandschneider
a	a	k8xC	a
Preller-Viatisem	Preller-Viatis	k1gInSc7	Preller-Viatis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
díky	díky	k7c3	díky
úpadku	úpadek	k1gInSc3	úpadek
železářství	železářství	k1gNnSc2	železářství
a	a	k8xC	a
díky	díky	k7c3	díky
mechanizování	mechanizování	k1gNnSc3	mechanizování
výroby	výroba	k1gFnSc2	výroba
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
textilní	textilní	k2eAgFnSc1d1	textilní
produkce	produkce	k1gFnSc1	produkce
města	město	k1gNnSc2	město
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
<g/>
,	,	kIx,	,
narůstá	narůstat	k5eAaImIp3nS	narůstat
množství	množství	k1gNnSc1	množství
přádelen	přádelna	k1gFnPc2	přádelna
<g/>
,	,	kIx,	,
bělidel	bělidlo	k1gNnPc2	bělidlo
a	a	k8xC	a
tkalcoven	tkalcovna	k1gFnPc2	tkalcovna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
pozastavený	pozastavený	k2eAgInSc4d1	pozastavený
rozvoj	rozvoj	k1gInSc4	rozvoj
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
názornost	názornost	k1gFnSc4	názornost
<g/>
:	:	kIx,	:
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1926	[number]	k4	1926
<g/>
-	-	kIx~	-
<g/>
1928	[number]	k4	1928
vydala	vydat	k5eAaPmAgFnS	vydat
vrchlabská	vrchlabský	k2eAgFnSc1d1	vrchlabská
železniční	železniční	k2eAgFnSc1d1	železniční
výdejna	výdejna	k1gFnSc1	výdejna
mezi	mezi	k7c7	mezi
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
100	[number]	k4	100
000	[number]	k4	000
vlakových	vlakový	k2eAgFnPc2d1	vlaková
jízdenek	jízdenka	k1gFnPc2	jízdenka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
sezony	sezona	k1gFnSc2	sezona
městem	město	k1gNnSc7	město
projelo	projet	k5eAaPmAgNnS	projet
až	až	k9	až
4000	[number]	k4	4000
motorových	motorový	k2eAgNnPc2d1	motorové
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
sezonu	sezona	k1gFnSc4	sezona
pouhých	pouhý	k2eAgInPc2d1	pouhý
500	[number]	k4	500
<g/>
-	-	kIx~	-
<g/>
600	[number]	k4	600
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
docela	docela	k6eAd1	docela
mnoho	mnoho	k6eAd1	mnoho
<g/>
.	.	kIx.	.
</s>
<s>
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
nebylo	být	k5eNaImAgNnS	být
jenom	jenom	k6eAd1	jenom
městem	město	k1gNnSc7	město
hornickým	hornický	k2eAgNnSc7d1	Hornické
či	či	k8xC	či
textilním	textilní	k2eAgNnSc7d1	textilní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
i	i	k9	i
centrem	centrum	k1gNnSc7	centrum
varhanářské	varhanářský	k2eAgFnSc2d1	varhanářská
manufaktury	manufaktura	k1gFnSc2	manufaktura
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
na	na	k7c6	na
našem	náš	k3xOp1gInSc6	náš
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
prochází	procházet	k5eAaImIp3nS	procházet
silnice	silnice	k1gFnSc1	silnice
první	první	k4xOgFnSc2	první
třídy	třída	k1gFnSc2	třída
č.	č.	k?	č.
14	[number]	k4	14
z	z	k7c2	z
České	český	k2eAgFnSc2d1	Česká
Třebové	Třebová	k1gFnSc2	Třebová
do	do	k7c2	do
Liberce	Liberec	k1gInSc2	Liberec
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
silnice	silnice	k1gFnPc1	silnice
druhé	druhý	k4xOgFnSc2	druhý
třídy	třída	k1gFnSc2	třída
č.	č.	k?	č.
295	[number]	k4	295
ze	z	k7c2	z
Studence	studenka	k1gFnSc3	studenka
do	do	k7c2	do
Špindlerova	Špindlerův	k2eAgInSc2d1	Špindlerův
Mlýna	mlýn	k1gInSc2	mlýn
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
<g/>
,	,	kIx,	,
dodnes	dodnes	k6eAd1	dodnes
fungující	fungující	k2eAgNnSc4d1	fungující
autobusové	autobusový	k2eAgNnSc4d1	autobusové
spojení	spojení	k1gNnSc4	spojení
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
se	s	k7c7	s
Špindlerovým	Špindlerův	k2eAgInSc7d1	Špindlerův
Mlýnem	mlýn	k1gInSc7	mlýn
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgNnSc1d1	železniční
spojení	spojení	k1gNnSc1	spojení
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
Trať	trať	k1gFnSc4	trať
0	[number]	k4	0
<g/>
44	[number]	k4	44
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zde	zde	k6eAd1	zde
začíná	začínat	k5eAaImIp3nS	začínat
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
do	do	k7c2	do
4,7	[number]	k4	4,7
kilometrů	kilometr	k1gInPc2	kilometr
vzdálených	vzdálený	k2eAgFnPc2d1	vzdálená
Kunčic	Kunčice	k1gFnPc2	Kunčice
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
umožněn	umožnit	k5eAaPmNgInS	umožnit
přestup	přestup	k1gInSc1	přestup
na	na	k7c4	na
vlaky	vlak	k1gInPc4	vlak
trati	trať	k1gFnSc2	trať
040	[number]	k4	040
z	z	k7c2	z
Chlumce	Chlumec	k1gInSc2	Chlumec
nad	nad	k7c7	nad
Cidlinou	Cidlina	k1gFnSc7	Cidlina
do	do	k7c2	do
Trutnova	Trutnov	k1gInSc2	Trutnov
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
osobních	osobní	k2eAgInPc2d1	osobní
vlaků	vlak	k1gInPc2	vlak
však	však	k9	však
jezdí	jezdit	k5eAaImIp3nP	jezdit
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
Trutnova	Trutnov	k1gInSc2	Trutnov
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
vstupem	vstup	k1gInSc7	vstup
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
jízdního	jízdní	k2eAgInSc2d1	jízdní
řádu	řád	k1gInSc2	řád
2005	[number]	k4	2005
<g/>
/	/	kIx~	/
<g/>
2006	[number]	k4	2006
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
specifický	specifický	k2eAgInSc1d1	specifický
způsob	způsob	k1gInSc1	způsob
odbavení	odbavení	k1gNnSc2	odbavení
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
platnosti	platnost	k1gFnSc2	platnost
jízdního	jízdní	k2eAgInSc2d1	jízdní
řádu	řád	k1gInSc2	řád
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
2013	[number]	k4	2013
nabízely	nabízet	k5eAaImAgFnP	nabízet
České	český	k2eAgFnPc1d1	Česká
dráhy	dráha	k1gFnPc1	dráha
v	v	k7c4	v
pracovní	pracovní	k2eAgInPc4d1	pracovní
dny	den	k1gInPc4	den
25	[number]	k4	25
spojů	spoj	k1gInPc2	spoj
osobních	osobní	k2eAgInPc2d1	osobní
vlaků	vlak	k1gInPc2	vlak
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
do	do	k7c2	do
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
a	a	k8xC	a
24	[number]	k4	24
spojů	spoj	k1gInPc2	spoj
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
do	do	k7c2	do
Kunčic	Kunčice	k1gFnPc2	Kunčice
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
8	[number]	k4	8
spojů	spoj	k1gInPc2	spoj
bylo	být	k5eAaImAgNnS	být
přímých	přímý	k2eAgFnPc2d1	přímá
z	z	k7c2	z
Trutnova	Trutnov	k1gInSc2	Trutnov
a	a	k8xC	a
9	[number]	k4	9
do	do	k7c2	do
Trutnova	Trutnov	k1gInSc2	Trutnov
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
víkendu	víkend	k1gInSc6	víkend
přijede	přijet	k5eAaPmIp3nS	přijet
do	do	k7c2	do
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
17	[number]	k4	17
párů	pár	k1gInPc2	pár
osobních	osobní	k2eAgInPc2d1	osobní
vlaků	vlak	k1gInPc2	vlak
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc4	jeden
spoj	spoj	k1gInSc4	spoj
přímý	přímý	k2eAgInSc4d1	přímý
do	do	k7c2	do
Trutnova	Trutnov	k1gInSc2	Trutnov
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yQgInPc2	který
se	se	k3xPyFc4	se
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
skládají	skládat	k5eAaImIp3nP	skládat
-	-	kIx~	-
ve	v	k7c6	v
Vrchlabské	vrchlabský	k2eAgFnSc6d1	vrchlabská
vrchovině	vrchovina	k1gFnSc6	vrchovina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
Janský	janský	k2eAgInSc4d1	janský
hřbet	hřbet	k1gInSc4	hřbet
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
také	také	k9	také
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k8xC	jako
Zlatý	zlatý	k2eAgInSc1d1	zlatý
a	a	k8xC	a
Lánovskou	Lánovský	k2eAgFnSc4d1	Lánovský
vrchovinu	vrchovina	k1gFnSc4	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
Středem	středem	k7c2	středem
města	město	k1gNnSc2	město
protéká	protékat	k5eAaImIp3nS	protékat
řeka	řeka	k1gFnSc1	řeka
Labe	Labe	k1gNnSc2	Labe
a	a	k8xC	a
vlévá	vlévat	k5eAaImIp3nS	vlévat
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
na	na	k7c6	na
vrchlabském	vrchlabský	k2eAgNnSc6d1	vrchlabské
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
několik	několik	k4yIc4	několik
potoků	potok	k1gInPc2	potok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Dolních	dolní	k2eAgFnPc6d1	dolní
Herlíkovicích	Herlíkovice	k1gFnPc6	Herlíkovice
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
po	po	k7c6	po
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
Šindelová	šindelový	k2eAgFnSc1d1	Šindelová
strouha	strouha	k1gFnSc1	strouha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
teče	téct	k5eAaImIp3nS	téct
z	z	k7c2	z
přes	přes	k7c4	přes
Dumlichův	Dumlichův	k2eAgInSc4d1	Dumlichův
důl	důl	k1gInSc4	důl
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
do	do	k7c2	do
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
zřízen	zřídit	k5eAaPmNgInS	zřídit
5,1	[number]	k4	5,1
km	km	kA	km
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
vodovod	vodovod	k1gInSc4	vodovod
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
něco	něco	k3yInSc4	něco
níže	nízce	k6eAd2	nízce
se	se	k3xPyFc4	se
do	do	k7c2	do
Labe	Labe	k1gNnSc2	Labe
napravo	napravo	k6eAd1	napravo
vlévá	vlévat	k5eAaImIp3nS	vlévat
Hlemýždí	hlemýždí	k2eAgInSc4d1	hlemýždí
potok	potok	k1gInSc4	potok
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
hotelu	hotel	k1gInSc2	hotel
Albis	Albis	k1gFnSc2	Albis
se	se	k3xPyFc4	se
nalevo	nalevo	k6eAd1	nalevo
od	od	k7c2	od
toku	tok	k1gInSc2	tok
vlévá	vlévat	k5eAaImIp3nS	vlévat
Richterova	Richterův	k2eAgFnSc1d1	Richterova
strouha	strouha	k1gFnSc1	strouha
<g/>
,	,	kIx,	,
pramenící	pramenící	k2eAgFnSc1d1	pramenící
na	na	k7c6	na
Strážném	strážný	k2eAgNnSc6d1	strážné
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vrchlabského	vrchlabský	k2eAgInSc2d1	vrchlabský
minipivovaru	minipivovar	k1gInSc2	minipivovar
se	se	k3xPyFc4	se
nalevo	nalevo	k6eAd1	nalevo
vlévá	vlévat	k5eAaImIp3nS	vlévat
Pekelský	pekelský	k2eAgInSc4d1	pekelský
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
o	o	k7c4	o
trochu	trocha	k1gFnSc4	trocha
níže	nízce	k6eAd2	nízce
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
zprava	zprava	k6eAd1	zprava
Hamerský	hamerský	k2eAgInSc1d1	hamerský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
pramenící	pramenící	k2eAgFnSc1d1	pramenící
v	v	k7c6	v
Kněžicích	kněžice	k1gFnPc6	kněžice
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
labským	labský	k2eAgInSc7d1	labský
přítokem	přítok	k1gInSc7	přítok
je	být	k5eAaImIp3nS	být
i	i	k9	i
potok	potok	k1gInSc1	potok
Bělá	bělat	k5eAaImIp3nS	bělat
jehož	jehož	k3xOyRp3gNnSc1	jehož
povodí	povodí	k1gNnSc1	povodí
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
úctyhodné	úctyhodný	k2eAgFnPc4d1	úctyhodná
délky	délka	k1gFnPc4	délka
7,011	[number]	k4	7,011
km	km	kA	km
<g/>
2	[number]	k4	2
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
délku	délka	k1gFnSc4	délka
5,1	[number]	k4	5,1
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kunčicích	Kunčice	k1gFnPc6	Kunčice
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
do	do	k7c2	do
Labe	Labe	k1gNnSc2	Labe
ústí	ústit	k5eAaImIp3nS	ústit
Vápenický	vápenický	k2eAgInSc1d1	vápenický
potok	potok	k1gInSc1	potok
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
9,4	[number]	k4	9,4
km	km	kA	km
s	s	k7c7	s
povodím	povodí	k1gNnSc7	povodí
o	o	k7c6	o
obsahu	obsah	k1gInSc6	obsah
8,768	[number]	k4	8,768
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
bylo	být	k5eAaImAgNnS	být
také	také	k6eAd1	také
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
pohánění	pohánění	k1gNnSc2	pohánění
strojů	stroj	k1gInPc2	stroj
továren	továrna	k1gFnPc2	továrna
založeno	založit	k5eAaPmNgNnS	založit
několik	několik	k4yIc4	několik
labských	labský	k2eAgInPc2d1	labský
náhonů	náhon	k1gInPc2	náhon
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgNnSc7d1	významné
prameništěm	prameniště	k1gNnSc7	prameniště
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
Labe	Labe	k1gNnSc2	Labe
Stavidlový	stavidlový	k2eAgInSc4d1	stavidlový
vrch	vrch	k1gInSc4	vrch
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgMnPc4d1	jiný
i	i	k8xC	i
kaplička	kaplička	k1gFnSc1	kaplička
svaté	svatý	k2eAgFnSc2d1	svatá
Anny	Anna	k1gFnSc2	Anna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rozmezí	rozmezí	k1gNnSc6	rozmezí
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
a	a	k8xC	a
Dolního	dolní	k2eAgNnSc2d1	dolní
Lánova	Lánovo	k1gNnSc2	Lánovo
pramení	pramenit	k5eAaImIp3nS	pramenit
Suchý	suchý	k2eAgInSc1d1	suchý
(	(	kIx(	(
<g/>
Mezilabský	Mezilabský	k2eAgInSc1d1	Mezilabský
<g/>
)	)	kIx)	)
potok	potok	k1gInSc1	potok
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
vrchlabským	vrchlabský	k2eAgInSc7d1	vrchlabský
rybníkem	rybník	k1gInSc7	rybník
jsou	být	k5eAaImIp3nP	být
Vejsplachy	Vejsplach	k1gInPc4	Vejsplach
<g/>
,	,	kIx,	,
rybník	rybník	k1gInSc1	rybník
někdy	někdy	k6eAd1	někdy
nazýván	nazývat	k5eAaImNgInS	nazývat
jako	jako	k9	jako
Kačák	Kačák	k1gInSc1	Kačák
<g/>
,	,	kIx,	,
hluboké	hluboký	k2eAgInPc1d1	hluboký
až	až	k9	až
2,3	[number]	k4	2,3
m	m	kA	m
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc1d1	ležící
západně	západně	k6eAd1	západně
od	od	k7c2	od
sídliště	sídliště	k1gNnSc2	sídliště
Liščí	liščí	k2eAgInSc4d1	liščí
kopec	kopec	k1gInSc4	kopec
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
rybníky	rybník	k1gInPc7	rybník
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
zámecký	zámecký	k2eAgInSc4d1	zámecký
rybník	rybník	k1gInSc4	rybník
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
stála	stát	k5eAaImAgFnS	stát
dříve	dříve	k6eAd2	dříve
tvrz	tvrz	k1gFnSc1	tvrz
<g/>
,	,	kIx,	,
rybník	rybník	k1gInSc1	rybník
u	u	k7c2	u
letiště	letiště	k1gNnSc2	letiště
na	na	k7c4	na
Lánovské	Lánovské	k2eAgFnSc4d1	Lánovské
ulici	ulice	k1gFnSc4	ulice
či	či	k8xC	či
rybník	rybník	k1gInSc4	rybník
v	v	k7c6	v
Podhůří	Podhůří	k1gNnSc6	Podhůří
pod	pod	k7c7	pod
zámečkem	zámeček	k1gInSc7	zámeček
<g/>
.	.	kIx.	.
</s>
<s>
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
podhorské	podhorský	k2eAgFnSc6d1	podhorská
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
srážkový	srážkový	k2eAgInSc1d1	srážkový
úhrn	úhrn	k1gInSc1	úhrn
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1961	[number]	k4	1961
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
činí	činit	k5eAaImIp3nS	činit
810	[number]	k4	810
<g/>
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Průměrně	průměrně	k6eAd1	průměrně
nejvíc	nejvíc	k6eAd1	nejvíc
srážek	srážka	k1gFnPc2	srážka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
(	(	kIx(	(
<g/>
průměrně	průměrně	k6eAd1	průměrně
90	[number]	k4	90
<g/>
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejméně	málo	k6eAd3	málo
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
(	(	kIx(	(
<g/>
průměrně	průměrně	k6eAd1	průměrně
39	[number]	k4	39
<g/>
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
počet	počet	k1gInSc1	počet
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
napršelo	napršet	k5eAaPmAgNnS	napršet
10	[number]	k4	10
<g/>
mm	mm	kA	mm
a	a	k8xC	a
více	hodně	k6eAd2	hodně
je	být	k5eAaImIp3nS	být
25,4	[number]	k4	25,4
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Nejmenší	malý	k2eAgInSc1d3	nejmenší
počet	počet	k1gInSc1	počet
těchto	tento	k3xDgInPc2	tento
dní	den	k1gInPc2	den
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
(	(	kIx(	(
<g/>
průměrně	průměrně	k6eAd1	průměrně
1,2	[number]	k4	1,2
dní	den	k1gInPc2	den
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejvíce	nejvíce	k6eAd1	nejvíce
pak	pak	k6eAd1	pak
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
(	(	kIx(	(
<g/>
průměrně	průměrně	k6eAd1	průměrně
3	[number]	k4	3
dny	den	k1gInPc4	den
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgInPc1d1	maximální
denní	denní	k2eAgInPc1d1	denní
úhrny	úhrn	k1gInPc1	úhrn
srážek	srážka	k1gFnPc2	srážka
se	se	k3xPyFc4	se
ročně	ročně	k6eAd1	ročně
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
okolo	okolo	k7c2	okolo
65	[number]	k4	65
<g/>
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Znak	znak	k1gInSc1	znak
byl	být	k5eAaImAgInS	být
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
udělen	udělen	k2eAgInSc4d1	udělen
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
s	s	k7c7	s
povýšením	povýšení	k1gNnSc7	povýšení
na	na	k7c4	na
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
majitele	majitel	k1gMnSc2	majitel
panství	panství	k1gNnSc2	panství
Kryštofa	Kryštof	k1gMnSc2	Kryštof
z	z	k7c2	z
Gendorfu	Gendorf	k1gInSc2	Gendorf
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
listinou	listina	k1gFnSc7	listina
krále	král	k1gMnSc2	král
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
I.	I.	kA	I.
současně	současně	k6eAd1	současně
s	s	k7c7	s
dalšími	další	k2eAgNnPc7d1	další
městskými	městský	k2eAgNnPc7d1	Městské
právy	právo	k1gNnPc7	právo
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
podoba	podoba	k1gFnSc1	podoba
znaku	znak	k1gInSc2	znak
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
až	až	k9	až
na	na	k7c4	na
drobné	drobný	k2eAgInPc4d1	drobný
detaily	detail	k1gInPc4	detail
respektuje	respektovat	k5eAaImIp3nS	respektovat
popis	popis	k1gInSc1	popis
z	z	k7c2	z
královské	královský	k2eAgFnSc2d1	královská
listiny	listina	k1gFnSc2	listina
<g/>
,	,	kIx,	,
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
vyobrazení	vyobrazení	k1gNnSc2	vyobrazení
v	v	k7c6	v
soupisu	soupis	k1gInSc6	soupis
Městské	městský	k2eAgInPc1d1	městský
znaky	znak	k1gInPc1	znak
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
od	od	k7c2	od
Jiřího	Jiří	k1gMnSc4	Jiří
Čarka	Čarek	k1gMnSc2	Čarek
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
text	text	k1gInSc1	text
z	z	k7c2	z
listiny	listina	k1gFnSc2	listina
popisuje	popisovat	k5eAaImIp3nS	popisovat
prapor	prapor	k1gInSc4	prapor
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
štít	štít	k1gInSc1	štít
na	na	k7c6	na
dvé	dvé	k?	dvé
rozdělený	rozdělený	k2eAgInSc4d1	rozdělený
<g/>
:	:	kIx,	:
v	v	k7c6	v
vrchním	vrchní	k2eAgNnSc6d1	vrchní
bílém	bílý	k2eAgNnSc6d1	bílé
poli	pole	k1gNnSc6	pole
dvě	dva	k4xCgFnPc1	dva
jedle	jedle	k1gFnPc1	jedle
zelené	zelený	k2eAgFnPc1d1	zelená
<g/>
,	,	kIx,	,
v	v	k7c6	v
dolním	dolní	k2eAgNnSc6d1	dolní
červeném	červený	k2eAgNnSc6d1	červené
poli	pole	k1gNnSc6	pole
dvě	dva	k4xCgFnPc4	dva
kladiva	kladivo	k1gNnPc4	kladivo
havérská	havérský	k2eAgNnPc4d1	havérský
<g/>
,	,	kIx,	,
nakříž	nakříž	k6eAd1	nakříž
přeložená	přeložený	k2eAgFnSc1d1	přeložená
<g/>
,	,	kIx,	,
v	v	k7c6	v
malým	malý	k2eAgInPc3d1	malý
žlutým	žlutý	k2eAgInSc7d1	žlutý
štítku	štítek	k1gInSc2	štítek
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jakž	jakž	k8xC	jakž
to	ten	k3xDgNnSc4	ten
všecko	všecek	k3xTgNnSc4	všecek
rukú	rukú	k?	rukú
a	a	k8xC	a
vtipem	vtip	k1gInSc7	vtip
umění	umění	k1gNnSc2	umění
maléřského	maléřský	k2eAgInSc2d1	maléřský
lépe	dobře	k6eAd2	dobře
a	a	k8xC	a
dostatečněji	dostatečně	k6eAd2	dostatečně
jest	být	k5eAaImIp3nS	být
vymalováno	vymalován	k2eAgNnSc1d1	vymalováno
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
a	a	k8xC	a
nahradila	nahradit	k5eAaPmAgFnS	nahradit
řadu	řada	k1gFnSc4	řada
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
využívaných	využívaný	k2eAgInPc2d1	využívaný
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
heraldicky	heraldicky	k6eAd1	heraldicky
nesprávných	správný	k2eNgFnPc2d1	nesprávná
variant	varianta	k1gFnPc2	varianta
znaku	znak	k1gInSc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Podoba	podoba	k1gFnSc1	podoba
praporu	prapor	k1gInSc2	prapor
byla	být	k5eAaImAgFnS	být
projednávána	projednáván	k2eAgFnSc1d1	projednávána
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2003	[number]	k4	2003
na	na	k7c6	na
schůzi	schůze	k1gFnSc6	schůze
podvýboru	podvýbor	k1gInSc2	podvýbor
pro	pro	k7c4	pro
heraldiku	heraldika	k1gFnSc4	heraldika
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Prapor	prapor	k1gInSc1	prapor
města	město	k1gNnSc2	město
byl	být	k5eAaImAgInS	být
městu	město	k1gNnSc3	město
udělen	udělen	k2eAgMnSc1d1	udělen
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2003	[number]	k4	2003
30	[number]	k4	30
<g/>
.	.	kIx.	.
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
Předsedy	předseda	k1gMnSc2	předseda
PS	PS	kA	PS
PČR	PČR	kA	PČR
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
praporu	prapor	k1gInSc2	prapor
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
heraldických	heraldický	k2eAgFnPc2d1	heraldická
úprav	úprava	k1gFnPc2	úprava
městského	městský	k2eAgInSc2d1	městský
znaku	znak	k1gInSc2	znak
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Lukáš	Lukáš	k1gMnSc1	Lukáš
Teplý	Teplý	k1gMnSc1	Teplý
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
setrvale	setrvale	k6eAd1	setrvale
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
tabulka	tabulka	k1gFnSc1	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
jsou	být	k5eAaImIp3nP	být
zastoupeny	zastoupit	k5eAaPmNgFnP	zastoupit
římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
<g/>
,	,	kIx,	,
českobratrská	českobratrský	k2eAgFnSc1d1	Českobratrská
evangelická	evangelický	k2eAgFnSc1d1	evangelická
<g/>
,	,	kIx,	,
československá	československý	k2eAgFnSc1d1	Československá
husitská	husitský	k2eAgFnSc1d1	husitská
a	a	k8xC	a
adventistická	adventistický	k2eAgFnSc1d1	Adventistická
církev	církev	k1gFnSc1	církev
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
Sál	sál	k1gInSc4	sál
Království	království	k1gNnSc2	království
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
Podhůří	Podhůří	k1gNnSc6	Podhůří
i	i	k9	i
Svědkové	svědek	k1gMnPc1	svědek
Jehovovi	Jehovův	k2eAgMnPc1d1	Jehovův
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
lidu	lid	k1gInSc2	lid
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
se	se	k3xPyFc4	se
ke	k	k7c3	k
katolickému	katolický	k2eAgNnSc3d1	katolické
náboženství	náboženství	k1gNnSc3	náboženství
přihlásilo	přihlásit	k5eAaPmAgNnS	přihlásit
1	[number]	k4	1
995	[number]	k4	995
z	z	k7c2	z
tehdejších	tehdejší	k2eAgInPc2d1	tehdejší
13	[number]	k4	13
894	[number]	k4	894
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
14,4	[number]	k4	14,4
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
sčítání	sčítání	k1gNnSc2	sčítání
účastníků	účastník	k1gMnPc2	účastník
katolických	katolický	k2eAgFnPc2d1	katolická
bohoslužeb	bohoslužba	k1gFnPc2	bohoslužba
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2003	[number]	k4	2003
vyplynul	vyplynout	k5eAaPmAgInS	vyplynout
počet	počet	k1gInSc1	počet
návštěvníků	návštěvník	k1gMnPc2	návštěvník
na	na	k7c4	na
160	[number]	k4	160
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
díky	díky	k7c3	díky
rozvoji	rozvoj	k1gInSc3	rozvoj
hornictví	hornictví	k1gNnSc2	hornictví
začal	začít	k5eAaPmAgInS	začít
příliv	příliv	k1gInSc1	příliv
Němců	Němec	k1gMnPc2	Němec
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
stalo	stát	k5eAaPmAgNnS	stát
hlavně	hlavně	k9	hlavně
německy	německy	k6eAd1	německy
mluvícím	mluvící	k2eAgNnSc7d1	mluvící
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
Vrchlabí	Vrchlabí	k1gNnPc2	Vrchlabí
mělo	mít	k5eAaImAgNnS	mít
oficiálně	oficiálně	k6eAd1	oficiálně
jenom	jenom	k9	jenom
7	[number]	k4	7
%	%	kIx~	%
Čechů	Čech	k1gMnPc2	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
samostatné	samostatný	k2eAgFnSc2d1	samostatná
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
však	však	k9	však
oficiální	oficiální	k2eAgInSc1d1	oficiální
počet	počet	k1gInSc1	počet
Čechů	Čech	k1gMnPc2	Čech
zvedl	zvednout	k5eAaPmAgInS	zvednout
na	na	k7c4	na
1	[number]	k4	1
200	[number]	k4	200
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgInPc1d1	významný
vrchlabské	vrchlabský	k2eAgInPc1d1	vrchlabský
podniky	podnik	k1gInPc1	podnik
a	a	k8xC	a
firmy	firma	k1gFnPc1	firma
<g/>
:	:	kIx,	:
Tesla	Tesla	k1gFnSc1	Tesla
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
transformována	transformován	k2eAgFnSc1d1	transformována
do	do	k7c2	do
množství	množství	k1gNnSc2	množství
menších	malý	k2eAgFnPc2d2	menší
firem	firma	k1gFnPc2	firma
<g/>
)	)	kIx)	)
TOS	TOS	kA	TOS
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Hytos	Hytos	k1gInSc1	Hytos
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Argo-Hytos	Argo-Hytos	k1gInSc1	Argo-Hytos
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
Škoda	škoda	k6eAd1	škoda
Auto	auto	k1gNnSc1	auto
NKT	NKT	kA	NKT
Cables	Cables	k1gInSc4	Cables
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
KABLO	KABLO	kA	KABLO
ELEKTRO	ELEKTRO	kA	ELEKTRO
a.s.	a.s.	k?	a.s.
OCZ	OCZ	kA	OCZ
Vrchlabí	Vrchlabí	k1gNnPc2	Vrchlabí
a.s.	a.s.	k?	a.s.
(	(	kIx(	(
<g/>
bývalý	bývalý	k2eAgMnSc1d1	bývalý
OPTREX	OPTREX	kA	OPTREX
Czech	Czech	k1gInSc4	Czech
<g />
.	.	kIx.	.
</s>
<s>
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
objektu	objekt	k1gInSc6	objekt
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Tesly	Tesla	k1gFnSc2	Tesla
<g/>
)	)	kIx)	)
NAF	NAF	kA	NAF
(	(	kIx(	(
<g/>
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
objektu	objekt	k1gInSc6	objekt
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Tesly	Tesla	k1gFnSc2	Tesla
<g/>
,	,	kIx,	,
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
zkratkou	zkratka	k1gFnSc7	zkratka
slov	slovo	k1gNnPc2	slovo
Nástroje	nástroj	k1gInSc2	nástroj
a	a	k8xC	a
formy	forma	k1gFnSc2	forma
<g/>
)	)	kIx)	)
Labit	Labit	k1gInSc1	Labit
a.s.	a.s.	k?	a.s.
-	-	kIx~	-
Tato	tento	k3xDgFnSc1	tento
firma	firma	k1gFnSc1	firma
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
kdysi	kdysi	k6eAd1	kdysi
fungovala	fungovat	k5eAaImAgFnS	fungovat
Weissova	Weissův	k2eAgFnSc1d1	Weissova
papírna	papírna	k1gFnSc1	papírna
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
sídlo	sídlo	k1gNnSc4	sídlo
firmy	firma	k1gFnSc2	firma
Jerie	Jerie	k1gFnSc2	Jerie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
budovou	budova	k1gFnSc7	budova
pro	pro	k7c4	pro
firmu	firma	k1gFnSc4	firma
Friedrich	Friedrich	k1gMnSc1	Friedrich
Stolzenberg	Stolzenberg	k1gMnSc1	Stolzenberg
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dodávala	dodávat	k5eAaImAgFnS	dodávat
motory	motor	k1gInPc4	motor
pro	pro	k7c4	pro
německé	německý	k2eAgInPc4d1	německý
letouny	letoun	k1gInPc4	letoun
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
součásti	součást	k1gFnPc1	součást
k	k	k7c3	k
řízeným	řízený	k2eAgFnPc3d1	řízená
střelám	střela	k1gFnPc3	střela
V	v	k7c6	v
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
sídlem	sídlo	k1gNnSc7	sídlo
firmy	firma	k1gFnSc2	firma
Zbrojovka	zbrojovka	k1gFnSc1	zbrojovka
Brno	Brno	k1gNnSc4	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
sídlem	sídlo	k1gNnSc7	sídlo
koncernu	koncern	k1gInSc2	koncern
TOS	TOS	kA	TOS
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
firma	firma	k1gFnSc1	firma
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
přejmenovaná	přejmenovaný	k2eAgFnSc1d1	přejmenovaná
na	na	k7c4	na
Labit	Labit	k1gInSc4	Labit
a.s.	a.s.	k?	a.s.
Toto	tento	k3xDgNnSc4	tento
umělecké	umělecký	k2eAgNnSc4d1	umělecké
těleso	těleso	k1gNnSc4	těleso
založil	založit	k5eAaPmAgMnS	založit
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
místní	místní	k2eAgMnPc1d1	místní
varhaník	varhaník	k1gMnSc1	varhaník
a	a	k8xC	a
učitel	učitel	k1gMnSc1	učitel
hudby	hudba	k1gFnSc2	hudba
Stanislav	Stanislav	k1gMnSc1	Stanislav
Skalský	Skalský	k1gMnSc1	Skalský
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
sbor	sbor	k1gInSc4	sbor
také	také	k9	také
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
vedl	vést	k5eAaImAgMnS	vést
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
sbor	sbor	k1gInSc1	sbor
veřejně	veřejně	k6eAd1	veřejně
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
na	na	k7c6	na
pravidelném	pravidelný	k2eAgInSc6d1	pravidelný
Vánočním	vánoční	k2eAgInSc6d1	vánoční
koncertu	koncert	k1gInSc6	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
novým	nový	k2eAgMnSc7d1	nový
sbormistrem	sbormistr	k1gMnSc7	sbormistr
Radek	Radek	k1gMnSc1	Radek
Hanuš	Hanuš	k1gMnSc1	Hanuš
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
<g/>
.	.	kIx.	.
</s>
<s>
Vyhláškou	vyhláška	k1gFnSc7	vyhláška
z	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1990	[number]	k4	1990
je	být	k5eAaImIp3nS	být
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
městskou	městský	k2eAgFnSc7d1	městská
památkovou	památkový	k2eAgFnSc7d1	památková
zónou	zóna	k1gFnSc7	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
patří	patřit	k5eAaImIp3nS	patřit
řada	řada	k1gFnSc1	řada
níže	nízce	k6eAd2	nízce
uvedených	uvedený	k2eAgFnPc2d1	uvedená
památek	památka	k1gFnPc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
za	za	k7c7	za
názvem	název	k1gInSc7	název
značka	značka	k1gFnSc1	značka
(	(	kIx(	(
<g/>
MPR	MPR	kA	MPR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sousoší	sousoší	k1gNnSc1	sousoší
Kristus	Kristus	k1gMnSc1	Kristus
na	na	k7c6	na
hoře	hora	k1gFnSc6	hora
Olivetské	olivetský	k2eAgFnSc3d1	Olivetská
–	–	k?	–
Na	na	k7c6	na
návrší	návrší	k1gNnSc6	návrší
nad	nad	k7c7	nad
poliklinikou	poliklinika	k1gFnSc7	poliklinika
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
toto	tento	k3xDgNnSc1	tento
sousoší	sousoší	k1gNnSc1	sousoší
nedaleko	nedaleko	k7c2	nedaleko
kaple	kaple	k1gFnSc2	kaple
sv.	sv.	kA	sv.
Anny	Anna	k1gFnSc2	Anna
<g/>
.	.	kIx.	.
</s>
<s>
Peníze	peníz	k1gInPc1	peníz
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
J.	J.	kA	J.
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
Wancke	Wanck	k1gMnSc2	Wanck
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
neznámý	známý	k2eNgMnSc1d1	neznámý
<g/>
.	.	kIx.	.
</s>
<s>
Vítězný	vítězný	k2eAgInSc4d1	vítězný
tank	tank	k1gInSc4	tank
–	–	k?	–
Tank	tank	k1gInSc1	tank
umístěný	umístěný	k2eAgInSc1d1	umístěný
před	před	k7c7	před
budovou	budova	k1gFnSc7	budova
školy	škola	k1gFnSc2	škola
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Odstraněn	odstranit	k5eAaPmNgInS	odstranit
byl	být	k5eAaImAgInS	být
ještě	ještě	k9	ještě
za	za	k7c2	za
komunismu	komunismus	k1gInSc2	komunismus
<g/>
.	.	kIx.	.
</s>
<s>
Malý	malý	k2eAgInSc1d1	malý
morový	morový	k2eAgInSc1d1	morový
sloup	sloup	k1gInSc1	sloup
–	–	k?	–
Na	na	k7c6	na
křižovatce	křižovatka	k1gFnSc6	křižovatka
ulic	ulice	k1gFnPc2	ulice
Pražská	pražský	k2eAgFnSc1d1	Pražská
a	a	k8xC	a
Fügnerova	Fügnerův	k2eAgFnSc1d1	Fügnerova
stávala	stávat	k5eAaImAgFnS	stávat
menší	malý	k2eAgFnSc1d2	menší
kopie	kopie	k1gFnSc1	kopie
morového	morový	k2eAgInSc2d1	morový
sloupu	sloup	k1gInSc2	sloup
z	z	k7c2	z
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
komunismu	komunismus	k1gInSc2	komunismus
byl	být	k5eAaImAgInS	být
zničen	zničit	k5eAaPmNgInS	zničit
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
Morový	morový	k2eAgInSc1d1	morový
sloup	sloup	k1gInSc1	sloup
na	na	k7c6	na
(	(	kIx(	(
<g/>
kostelním	kostelní	k2eAgNnSc6d1	kostelní
<g/>
)	)	kIx)	)
Náměstí	náměstí	k1gNnSc6	náměstí
Míru	mír	k1gInSc2	mír
(	(	kIx(	(
<g/>
MPZ	MPZ	kA	MPZ
<g/>
)	)	kIx)	)
–	–	k?	–
Na	na	k7c6	na
pískovcovém	pískovcový	k2eAgInSc6d1	pískovcový
sloupu	sloup	k1gInSc6	sloup
stojící	stojící	k2eAgFnSc1d1	stojící
socha	socha	k1gFnSc1	socha
Immaculaty	Immacule	k1gNnPc7	Immacule
=	=	kIx~	=
Neposkvrněné	poskvrněný	k2eNgFnSc2d1	neposkvrněná
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
je	být	k5eAaImIp3nS	být
dílem	dílo	k1gNnSc7	dílo
sochaře	sochař	k1gMnSc2	sochař
Krystiana	Krystian	k1gMnSc2	Krystian
Punschucha	Punschuch	k1gMnSc2	Punschuch
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1696	[number]	k4	1696
<g/>
.	.	kIx.	.
</s>
<s>
Socha	socha	k1gFnSc1	socha
byla	být	k5eAaImAgFnS	být
vytesána	vytesat	k5eAaPmNgFnS	vytesat
z	z	k7c2	z
kamene	kámen	k1gInSc2	kámen
z	z	k7c2	z
Lánova	Lánov	k1gInSc2	Lánov
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
morový	morový	k2eAgInSc1d1	morový
sloup	sloup	k1gInSc1	sloup
však	však	k9	však
nebyl	být	k5eNaImAgInS	být
dílem	díl	k1gInSc7	díl
jedné	jeden	k4xCgFnSc2	jeden
osoby	osoba	k1gFnSc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Punschuchem	Punschuch	k1gInSc7	Punschuch
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
pracovali	pracovat	k5eAaImAgMnP	pracovat
Jonáš	Jonáš	k1gMnSc1	Jonáš
Hancke	Hanck	k1gFnSc2	Hanck
a	a	k8xC	a
Kryštof	Kryštof	k1gMnSc1	Kryštof
Tauch	Taucha	k1gFnPc2	Taucha
–	–	k?	–
kameníci	kameník	k1gMnPc1	kameník
a	a	k8xC	a
Zikmund	Zikmund	k1gMnSc1	Zikmund
Bandt	Bandt	k1gMnSc1	Bandt
–	–	k?	–
zámečník	zámečník	k1gMnSc1	zámečník
<g/>
.	.	kIx.	.
</s>
<s>
Barevnost	barevnost	k1gFnSc1	barevnost
sousoší	sousoší	k1gNnSc2	sousoší
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
samotný	samotný	k2eAgInSc1d1	samotný
sloup	sloup	k1gInSc1	sloup
lemovaly	lemovat	k5eAaImAgFnP	lemovat
čtyři	čtyři	k4xCgFnPc1	čtyři
sochy	socha	k1gFnPc1	socha
sv.	sv.	kA	sv.
Šebestiána	Šebestián	k1gMnSc4	Šebestián
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Rocha	Rocha	k1gFnSc1	Rocha
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Rozálie	Rozálie	k1gFnSc1	Rozálie
a	a	k8xC	a
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
byly	být	k5eAaImAgInP	být
odebrané	odebraný	k2eAgInPc1d1	odebraný
do	do	k7c2	do
depozitáře	depozitář	k1gInSc2	depozitář
místního	místní	k2eAgNnSc2d1	místní
muzea	muzeum	k1gNnSc2	muzeum
roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
zajistil	zajistit	k5eAaPmAgMnS	zajistit
malíř	malíř	k1gMnSc1	malíř
Václav	Václav	k1gMnSc1	Václav
Hallmann	Hallmann	k1gMnSc1	Hallmann
<g/>
.	.	kIx.	.
</s>
<s>
Sloup	sloup	k1gInSc1	sloup
dříve	dříve	k6eAd2	dříve
stál	stát	k5eAaImAgInS	stát
uprostřed	uprostřed	k7c2	uprostřed
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
přemístěn	přemístit	k5eAaPmNgInS	přemístit
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
ovšem	ovšem	k9	ovšem
spatříte	spatřit	k5eAaPmIp2nP	spatřit
jenom	jenom	k9	jenom
kopii	kopie	k1gFnSc4	kopie
originálu	originál	k1gInSc2	originál
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zchátralý	zchátralý	k2eAgInSc4d1	zchátralý
originál	originál	k1gInSc4	originál
nahradila	nahradit	k5eAaPmAgFnS	nahradit
kopie	kopie	k1gFnSc1	kopie
od	od	k7c2	od
Eduarda	Eduard	k1gMnSc2	Eduard
Wlačihy	Wlačih	k1gInPc4	Wlačih
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
jako	jako	k9	jako
kameník	kameník	k1gMnSc1	kameník
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
stavby	stavba	k1gFnPc4	stavba
hraběcí	hraběcí	k2eAgFnSc2d1	hraběcí
hrobky	hrobka	k1gFnSc2	hrobka
naproti	naproti	k7c3	naproti
městskému	městský	k2eAgInSc3d1	městský
parku	park	k1gInSc3	park
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
je	být	k5eAaImIp3nS	být
sloup	sloup	k1gInSc1	sloup
nemovitou	movitý	k2eNgFnSc7d1	nemovitá
kulturní	kulturní	k2eAgFnSc7d1	kulturní
památkou	památka	k1gFnSc7	památka
<g/>
.	.	kIx.	.
</s>
<s>
Socha	socha	k1gFnSc1	socha
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Pomocné	pomocný	k2eAgFnSc2d1	pomocná
–	–	k?	–
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
u	u	k7c2	u
bývalého	bývalý	k2eAgInSc2d1	bývalý
městského	městský	k2eAgInSc2d1	městský
parku	park	k1gInSc2	park
nad	nad	k7c7	nad
kinem	kino	k1gNnSc7	kino
<g/>
.	.	kIx.	.
</s>
<s>
Financována	financován	k2eAgFnSc1d1	financována
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
prostředků	prostředek	k1gInPc2	prostředek
měšťana	měšťan	k1gMnSc2	měšťan
G.	G.	kA	G.
Wagnera	Wagner	k1gMnSc2	Wagner
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
založení	založení	k1gNnSc6	založení
Městského	městský	k2eAgInSc2d1	městský
parku	park	k1gInSc2	park
nad	nad	k7c7	nad
sochou	socha	k1gFnSc7	socha
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
sochy	socha	k1gFnSc2	socha
na	na	k7c6	na
Strážné	strážná	k1gFnSc6	strážná
vysázena	vysázen	k2eAgFnSc1d1	vysázena
lipová	lipový	k2eAgFnSc1d1	Lipová
alej	alej	k1gFnSc1	alej
<g/>
.	.	kIx.	.
</s>
<s>
Socha	socha	k1gFnSc1	socha
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
"	"	kIx"	"
<g/>
u	u	k7c2	u
mostu	most	k1gInSc2	most
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
MPZ	MPZ	kA	MPZ
<g/>
)	)	kIx)	)
–	–	k?	–
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
u	u	k7c2	u
kamenného	kamenný	k2eAgInSc2d1	kamenný
mostu	most	k1gInSc2	most
přes	přes	k7c4	přes
Labe	Labe	k1gNnSc4	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Autora	autor	k1gMnSc4	autor
neznáme	neznat	k5eAaImIp1nP	neznat
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
donátora	donátor	k1gMnSc4	donátor
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnSc7	který
byl	být	k5eAaImAgMnS	být
A.	A.	kA	A.
Kolonitz	Kolonitz	k1gInSc1	Kolonitz
ano	ano	k9	ano
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pískovcovém	pískovcový	k2eAgInSc6d1	pískovcový
podstavci	podstavec	k1gInSc6	podstavec
stojí	stát	k5eAaImIp3nS	stát
nápis	nápis	k1gInSc1	nápis
Ex	ex	k6eAd1	ex
voto	voto	k6eAd1	voto
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
česky	česky	k6eAd1	česky
znamená	znamenat	k5eAaImIp3nS	znamenat
splnění	splnění	k1gNnSc4	splnění
slibu	slib	k1gInSc2	slib
a	a	k8xC	a
letopočet	letopočet	k1gInSc4	letopočet
1709	[number]	k4	1709
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
světových	světový	k2eAgFnPc2d1	světová
stran	strana	k1gFnPc2	strana
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
podstavci	podstavec	k1gInSc6	podstavec
zobrazen	zobrazen	k2eAgMnSc1d1	zobrazen
ukřižovaný	ukřižovaný	k2eAgMnSc1d1	ukřižovaný
Ježíš	Ježíš	k1gMnSc1	Ježíš
<g/>
,	,	kIx,	,
svatý	svatý	k2eAgMnSc1d1	svatý
Vavřinec	Vavřinec	k1gMnSc1	Vavřinec
<g/>
,	,	kIx,	,
svatý	svatý	k2eAgMnSc1d1	svatý
Václav	Václav	k1gMnSc1	Václav
a	a	k8xC	a
svatá	svatý	k2eAgFnSc1d1	svatá
Ludmila	Ludmila	k1gFnSc1	Ludmila
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
německé	německý	k2eAgNnSc1d1	německé
věnování	věnování	k1gNnSc1	věnování
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
obsažen	obsažen	k2eAgInSc1d1	obsažen
datum	datum	k1gInSc1	datum
16	[number]	k4	16
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
světec	světec	k1gMnSc1	světec
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
je	být	k5eAaImIp3nS	být
socha	socha	k1gFnSc1	socha
věnována	věnován	k2eAgFnSc1d1	věnována
slaví	slavit	k5eAaImIp3nS	slavit
svátek	svátek	k1gInSc4	svátek
<g/>
.	.	kIx.	.
</s>
<s>
Socha	socha	k1gFnSc1	socha
svatého	svatý	k2eAgMnSc2d1	svatý
Josefa	Josef	k1gMnSc2	Josef
(	(	kIx(	(
<g/>
MPZ	MPZ	kA	MPZ
<g/>
)	)	kIx)	)
-	-	kIx~	-
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
Městského	městský	k2eAgNnSc2d1	Městské
muzea	muzeum	k1gNnSc2	muzeum
za	za	k7c7	za
třemi	tři	k4xCgInPc7	tři
historickými	historický	k2eAgInPc7d1	historický
domky	domek	k1gInPc7	domek
za	za	k7c7	za
Náměstím	náměstí	k1gNnSc7	náměstí
Míru	mír	k1gInSc2	mír
Socha	socha	k1gFnSc1	socha
svaté	svatý	k2eAgFnSc2d1	svatá
Anny	Anna	k1gFnSc2	Anna
Samotřetí	Samotřetí	k1gNnSc2	Samotřetí
(	(	kIx(	(
<g/>
MPZ	MPZ	kA	MPZ
<g/>
)	)	kIx)	)
–	–	k?	–
Sousoší	sousoší	k1gNnSc4	sousoší
svaté	svatý	k2eAgFnSc2d1	svatá
Anny	Anna	k1gFnSc2	Anna
Samotřetí	Samotřetí	k1gNnSc2	Samotřetí
neboli	neboli	k8xC	neboli
Anenská	anenský	k2eAgFnSc1d1	Anenská
statue	statue	k1gFnSc1	statue
stojící	stojící	k2eAgFnSc1d1	stojící
vedle	vedle	k7c2	vedle
děkanství	děkanství	k1gNnSc2	děkanství
bylo	být	k5eAaImAgNnS	být
původně	původně	k6eAd1	původně
umístěno	umístit	k5eAaPmNgNnS	umístit
vedle	vedle	k7c2	vedle
zdi	zeď	k1gFnSc2	zeď
dřevěného	dřevěný	k2eAgInSc2d1	dřevěný
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
Budova	budova	k1gFnSc1	budova
městské	městský	k2eAgFnSc2d1	městská
knihovny	knihovna	k1gFnSc2	knihovna
–	–	k?	–
více	hodně	k6eAd2	hodně
v	v	k7c6	v
kapitole	kapitola	k1gFnSc6	kapitola
Městská	městský	k2eAgFnSc1d1	městská
knihovna	knihovna	k1gFnSc1	knihovna
Klášter	klášter	k1gInSc1	klášter
augustiniánů	augustinián	k1gMnPc2	augustinián
(	(	kIx(	(
<g/>
MPZ	MPZ	kA	MPZ
<g/>
)	)	kIx)	)
–	–	k?	–
více	hodně	k6eAd2	hodně
viz	vidět	k5eAaImRp2nS	vidět
Klášter	klášter	k1gInSc1	klášter
augustiniánů	augustinián	k1gMnPc2	augustinián
(	(	kIx(	(
<g/>
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Stará	starý	k2eAgFnSc1d1	stará
<g/>
"	"	kIx"	"
radnice	radnice	k1gFnSc1	radnice
"	"	kIx"	"
<g/>
Nová	nový	k2eAgFnSc1d1	nová
<g/>
"	"	kIx"	"
radnice	radnice	k1gFnSc1	radnice
(	(	kIx(	(
<g/>
MPZ	MPZ	kA	MPZ
<g/>
)	)	kIx)	)
–	–	k?	–
Stavba	stavba	k1gFnSc1	stavba
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1591	[number]	k4	1591
ležící	ležící	k2eAgFnSc2d1	ležící
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
okraji	okraj	k1gInSc6	okraj
Masarykova	Masarykův	k2eAgNnSc2d1	Masarykovo
náměstí	náměstí	k1gNnSc2	náměstí
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
třech	tři	k4xCgInPc2	tři
měšťanských	měšťanský	k2eAgInPc2d1	měšťanský
domů	dům	k1gInPc2	dům
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
podoby	podoba	k1gFnSc2	podoba
vystavena	vystavit	k5eAaPmNgFnS	vystavit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1732	[number]	k4	1732
až	až	k9	až
1737	[number]	k4	1737
architektem	architekt	k1gMnSc7	architekt
Janem	Jan	k1gMnSc7	Jan
Jiřím	Jiří	k1gMnSc7	Jiří
Seehakem	Seehak	k1gMnSc7	Seehak
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
vzhled	vzhled	k1gInSc1	vzhled
renesanční	renesanční	k2eAgInSc1d1	renesanční
byl	být	k5eAaImAgInS	být
změněn	změněn	k2eAgInSc1d1	změněn
na	na	k7c4	na
barokní	barokní	k2eAgFnSc4d1	barokní
podobu	podoba	k1gFnSc4	podoba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
ta	ten	k3xDgFnSc1	ten
dnešní	dnešní	k2eAgFnSc1d1	dnešní
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
Krkonošské	krkonošský	k2eAgFnSc3d1	Krkonošská
ulici	ulice	k1gFnSc3	ulice
má	mít	k5eAaImIp3nS	mít
tato	tento	k3xDgFnSc1	tento
stavba	stavba	k1gFnSc1	stavba
štítové	štítový	k2eAgNnSc4d1	štítové
průčelí	průčelí	k1gNnSc4	průčelí
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
tvořené	tvořený	k2eAgFnSc2d1	tvořená
pětiosým	pětiosý	k2eAgNnSc7d1	pětiosé
podloubím	podloubí	k1gNnSc7	podloubí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
štítu	štít	k1gInSc6	štít
je	být	k5eAaImIp3nS	být
poplašný	poplašný	k2eAgInSc4d1	poplašný
zvonek	zvonek	k1gInSc4	zvonek
<g/>
.	.	kIx.	.
</s>
<s>
Radnice	radnice	k1gFnSc1	radnice
má	mít	k5eAaImIp3nS	mít
věžičku	věžička	k1gFnSc4	věžička
s	s	k7c7	s
bání	báně	k1gFnSc7	báně
a	a	k8xC	a
hodinami	hodina	k1gFnPc7	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hodinách	hodina	k1gFnPc6	hodina
je	být	k5eAaImIp3nS	být
latinský	latinský	k2eAgInSc1d1	latinský
nápis	nápis	k1gInSc1	nápis
<g/>
:	:	kIx,	:
Ultima	ultimo	k1gNnSc2	ultimo
latet	latet	k5eAaPmF	latet
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
česky	česky	k6eAd1	česky
Poslední	poslední	k2eAgFnSc1d1	poslední
hodina	hodina	k1gFnSc1	hodina
je	být	k5eAaImIp3nS	být
skryta	skrýt	k5eAaPmNgFnS	skrýt
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
k	k	k7c3	k
náměstí	náměstí	k1gNnSc3	náměstí
je	být	k5eAaImIp3nS	být
šest	šest	k4xCc4	šest
zazděných	zazděný	k2eAgNnPc2d1	zazděné
oken	okno	k1gNnPc2	okno
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
bylo	být	k5eAaImAgNnS	být
přízemí	přízemí	k1gNnSc1	přízemí
využito	využit	k2eAgNnSc1d1	využito
jako	jako	k8xS	jako
hospoda	hospoda	k?	hospoda
"	"	kIx"	"
<g/>
U	u	k7c2	u
zeleného	zelený	k2eAgInSc2d1	zelený
hroznu	hrozen	k1gInSc2	hrozen
<g/>
"	"	kIx"	"
a	a	k8xC	a
sídlo	sídlo	k1gNnSc1	sídlo
Národní	národní	k2eAgFnSc2d1	národní
gardy	garda	k1gFnSc2	garda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
byla	být	k5eAaImAgFnS	být
zde	zde	k6eAd1	zde
zprostředkovatelna	zprostředkovatelna	k1gFnSc1	zprostředkovatelna
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
jakýsi	jakýsi	k3yIgInSc1	jakýsi
Pracovní	pracovní	k2eAgInSc1d1	pracovní
úřad	úřad	k1gInSc1	úřad
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
opět	opět	k6eAd1	opět
restaurace	restaurace	k1gFnSc1	restaurace
"	"	kIx"	"
<g/>
Radnice	radnice	k1gFnSc1	radnice
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
Turistické	turistický	k2eAgNnSc1d1	turistické
Informační	informační	k2eAgNnSc1d1	informační
Centrum	centrum	k1gNnSc1	centrum
a	a	k8xC	a
ve	v	k7c6	v
vyšších	vysoký	k2eAgNnPc6d2	vyšší
patrech	patro	k1gNnPc6	patro
kanceláře	kancelář	k1gFnSc2	kancelář
městského	městský	k2eAgInSc2d1	městský
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
na	na	k7c6	na
budově	budova	k1gFnSc6	budova
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
radnice	radnice	k1gFnSc1	radnice
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
do	do	k7c2	do
novorenesanční	novorenesanční	k2eAgFnSc2d1	novorenesanční
podoby	podoba	k1gFnSc2	podoba
městským	městský	k2eAgMnSc7d1	městský
architektem	architekt	k1gMnSc7	architekt
Hansem	Hans	k1gMnSc7	Hans
Knollem	Knoll	k1gMnSc7	Knoll
<g/>
.	.	kIx.	.
</s>
<s>
Gymnázium	gymnázium	k1gNnSc1	gymnázium
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
–	–	k?	–
viz	vidět	k5eAaImRp2nS	vidět
Gymnázium	gymnázium	k1gNnSc1	gymnázium
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
Kostel	kostel	k1gInSc4	kostel
sv.	sv.	kA	sv.
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
(	(	kIx(	(
<g/>
MPZ	MPZ	kA	MPZ
<g/>
)	)	kIx)	)
–	–	k?	–
viz	vidět	k5eAaImRp2nS	vidět
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
(	(	kIx(	(
<g/>
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
<g/>
)	)	kIx)	)
Zámek	zámek	k1gInSc1	zámek
(	(	kIx(	(
<g/>
MPZ	MPZ	kA	MPZ
<g/>
)	)	kIx)	)
-	-	kIx~	-
viz	vidět	k5eAaImRp2nS	vidět
Zámek	zámek	k1gInSc1	zámek
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
Dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
159	[number]	k4	159
"	"	kIx"	"
<g/>
U	u	k7c2	u
Sedmi	sedm	k4xCc2	sedm
štítů	štít	k1gInPc2	štít
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
MPZ	MPZ	kA	MPZ
<g/>
)	)	kIx)	)
Domy	dům	k1gInPc1	dům
<g />
.	.	kIx.	.
</s>
<s>
čp.	čp.	k?	čp.
222	[number]	k4	222
<g/>
,	,	kIx,	,
223	[number]	k4	223
<g/>
,	,	kIx,	,
224	[number]	k4	224
"	"	kIx"	"
<g/>
Tří	tři	k4xCgFnPc2	tři
domky	domek	k1gInPc4	domek
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
MPZ	MPZ	kA	MPZ
<g/>
)	)	kIx)	)
Fara	fara	k1gFnSc1	fara
<g/>
,	,	kIx,	,
čp.	čp.	k?	čp.
287	[number]	k4	287
(	(	kIx(	(
<g/>
MPZ	MPZ	kA	MPZ
<g/>
)	)	kIx)	)
Domy	dům	k1gInPc1	dům
čp.	čp.	k?	čp.
21	[number]	k4	21
<g/>
,	,	kIx,	,
44	[number]	k4	44
<g/>
,	,	kIx,	,
100	[number]	k4	100
<g/>
,	,	kIx,	,
106	[number]	k4	106
<g/>
,	,	kIx,	,
173	[number]	k4	173
<g/>
,	,	kIx,	,
197	[number]	k4	197
<g/>
,	,	kIx,	,
198	[number]	k4	198
<g/>
,	,	kIx,	,
210	[number]	k4	210
<g/>
,	,	kIx,	,
298	[number]	k4	298
<g/>
,	,	kIx,	,
309	[number]	k4	309
<g/>
,	,	kIx,	,
386	[number]	k4	386
<g/>
,	,	kIx,	,
471	[number]	k4	471
(	(	kIx(	(
<g/>
MPZ	MPZ	kA	MPZ
<g/>
)	)	kIx)	)
Kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Kříže	kříž	k1gInSc2	kříž
–	–	k?	–
Výklenková	výklenkový	k2eAgFnSc1d1	výklenková
kaple	kaple	k1gFnSc1	kaple
ležící	ležící	k2eAgFnSc1d1	ležící
na	na	k7c6	na
Stavidlovém	stavidlový	k2eAgInSc6d1	stavidlový
vrchu	vrch	k1gInSc6	vrch
nad	nad	k7c7	nad
kaplí	kaple	k1gFnSc7	kaple
svaté	svatý	k2eAgFnSc2d1	svatá
Anny	Anna	k1gFnSc2	Anna
z	z	k7c2	z
přelomu	přelom	k1gInSc2	přelom
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
nástupu	nástup	k1gInSc2	nástup
komunismu	komunismus	k1gInSc6	komunismus
tato	tento	k3xDgFnSc1	tento
stavba	stavba	k1gFnSc1	stavba
postupně	postupně	k6eAd1	postupně
chátrala	chátrat	k5eAaImAgFnS	chátrat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
sdružení	sdružení	k1gNnSc1	sdružení
Zdravé	zdravá	k1gFnSc2	zdravá
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
zorganizovalo	zorganizovat	k5eAaPmAgNnS	zorganizovat
roční	roční	k2eAgFnSc4d1	roční
opravu	oprava	k1gFnSc4	oprava
<g/>
.	.	kIx.	.
</s>
<s>
Interiér	interiér	k1gInSc4	interiér
i	i	k8xC	i
exteriér	exteriér	k1gInSc4	exteriér
zdobí	zdobit	k5eAaImIp3nP	zdobit
malby	malba	k1gFnPc1	malba
od	od	k7c2	od
klášterskolhotského	klášterskolhotský	k2eAgMnSc2d1	klášterskolhotský
malíře	malíř	k1gMnSc2	malíř
Mariána	Marián	k1gMnSc2	Marián
Puchnika	Puchnik	k1gMnSc2	Puchnik
<g/>
.	.	kIx.	.
</s>
<s>
Kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Anny	Anna	k1gFnSc2	Anna
–	–	k?	–
Výklenková	výklenkový	k2eAgFnSc1d1	výklenková
kaple	kaple	k1gFnSc1	kaple
ležící	ležící	k2eAgFnSc1d1	ležící
u	u	k7c2	u
koryta	koryto	k1gNnSc2	koryto
Labe	Labe	k1gNnSc2	Labe
pod	pod	k7c7	pod
Stavidlovým	stavidlový	k2eAgInSc7d1	stavidlový
vrchem	vrch	k1gInSc7	vrch
<g/>
.	.	kIx.	.
</s>
<s>
Kaple	kaple	k1gFnSc1	kaple
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1832	[number]	k4	1832
nad	nad	k7c7	nad
studánkou	studánka	k1gFnSc7	studánka
s	s	k7c7	s
pitnou	pitný	k2eAgFnSc7d1	pitná
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Sdružení	sdružení	k1gNnSc1	sdružení
Zdravé	zdravý	k2eAgFnPc1d1	zdravá
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2003	[number]	k4	2003
<g/>
-	-	kIx~	-
<g/>
2004	[number]	k4	2004
provedlo	provést	k5eAaPmAgNnS	provést
kompletní	kompletní	k2eAgFnSc4d1	kompletní
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
zničené	zničený	k2eAgFnSc2d1	zničená
kaple	kaple	k1gFnSc2	kaple
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
zbyla	zbýt	k5eAaPmAgFnS	zbýt
jen	jen	k6eAd1	jen
štítová	štítový	k2eAgFnSc1d1	štítová
zeď	zeď	k1gFnSc1	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
výzdobu	výzdoba	k1gFnSc4	výzdoba
provedla	provést	k5eAaPmAgFnS	provést
malířka	malířka	k1gFnSc1	malířka
Květa	Květa	k1gFnSc1	Květa
Krchánková	Krchánková	k1gFnSc1	Krchánková
<g/>
.	.	kIx.	.
</s>
<s>
Kaple	kaple	k1gFnSc1	kaple
byla	být	k5eAaImAgFnS	být
slavnostně	slavnostně	k6eAd1	slavnostně
vysvěcena	vysvětit	k5eAaPmNgFnS	vysvětit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
vrchlabským	vrchlabský	k2eAgMnSc7d1	vrchlabský
děkanem	děkan	k1gMnSc7	děkan
Stanislavem	Stanislav	k1gMnSc7	Stanislav
Skalským	Skalský	k1gMnSc7	Skalský
<g/>
.	.	kIx.	.
</s>
<s>
Akciový	akciový	k2eAgInSc1d1	akciový
pivovar	pivovar	k1gInSc1	pivovar
–	–	k?	–
Po	po	k7c6	po
stržení	stržení	k1gNnSc6	stržení
starého	starý	k2eAgInSc2d1	starý
vrchnostenského	vrchnostenský	k2eAgInSc2d1	vrchnostenský
pivovaru	pivovar	k1gInSc2	pivovar
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
ocitlo	ocitnout	k5eAaPmAgNnS	ocitnout
bez	bez	k7c2	bez
pivovaru	pivovar	k1gInSc2	pivovar
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1902	[number]	k4	1902
proto	proto	k8xC	proto
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
nový	nový	k2eAgInSc4d1	nový
<g/>
,	,	kIx,	,
akciový	akciový	k2eAgInSc4d1	akciový
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
začal	začít	k5eAaPmAgInS	začít
fungovat	fungovat	k5eAaImF	fungovat
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
<g/>
.	.	kIx.	.
</s>
<s>
Pivovar	pivovar	k1gInSc1	pivovar
dal	dát	k5eAaPmAgInS	dát
také	také	k9	také
jméno	jméno	k1gNnSc4	jméno
celé	celá	k1gFnSc2	celá
dnešní	dnešní	k2eAgFnSc4d1	dnešní
Komenského	Komenského	k2eAgFnSc4d1	Komenského
ulici	ulice	k1gFnSc4	ulice
<g/>
,	,	kIx,	,
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
se	se	k3xPyFc4	se
Bräuhaustraße	Bräuhaustraße	k1gFnSc1	Bräuhaustraße
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Pivovarská	pivovarský	k2eAgFnSc1d1	Pivovarská
ulice	ulice	k1gFnSc1	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
byl	být	k5eAaImAgInS	být
majetkem	majetek	k1gInSc7	majetek
akciové	akciový	k2eAgFnSc2d1	akciová
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
majetkem	majetek	k1gInSc7	majetek
Hradecké	Hradecké	k2eAgInPc7d1	Hradecké
pivovary	pivovar	k1gInPc7	pivovar
n.	n.	k?	n.
<g/>
p.	p.	k?	p.
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1953	[number]	k4	1953
<g/>
-	-	kIx~	-
<g/>
1957	[number]	k4	1957
vlastníkem	vlastník	k1gMnSc7	vlastník
Krkonošské	krkonošský	k2eAgInPc1d1	krkonošský
pivovary	pivovar	k1gInPc1	pivovar
n.	n.	k?	n.
<g/>
p.	p.	k?	p.
<g/>
,	,	kIx,	,
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Hradeckých	Hradeckých	k2eAgInPc2d1	Hradeckých
pivovarů	pivovar	k1gInPc2	pivovar
n.	n.	k?	n.
<g/>
p.	p.	k?	p.
se	se	k3xPyFc4	se
pivovar	pivovar	k1gInSc1	pivovar
vrátil	vrátit	k5eAaPmAgInS	vrátit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1958	[number]	k4	1958
<g/>
-	-	kIx~	-
<g/>
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
vlastníkem	vlastník	k1gMnSc7	vlastník
Východočeské	východočeský	k2eAgInPc1d1	východočeský
pivovary	pivovar	k1gInPc1	pivovar
n.	n.	k?	n.
<g/>
p.	p.	k?	p.
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
ukončil	ukončit	k5eAaPmAgInS	ukončit
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
sodovkárnou	sodovkárna	k1gFnSc7	sodovkárna
svoji	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
s	s	k7c7	s
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
objektu	objekt	k1gInSc2	objekt
a	a	k8xC	a
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
s	s	k7c7	s
dostavbou	dostavba	k1gFnSc7	dostavba
čtyřech	čtyři	k4xCgInPc2	čtyři
dalších	další	k2eAgInPc2d1	další
domů	dům	k1gInPc2	dům
na	na	k7c4	na
bytový	bytový	k2eAgInSc4d1	bytový
komplex	komplex	k1gInSc4	komplex
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
komplex	komplex	k1gInSc1	komplex
nedostavěný	dostavěný	k2eNgInSc1d1	nedostavěný
a	a	k8xC	a
práce	práce	k1gFnPc1	práce
pozastaveny	pozastavit	k5eAaPmNgFnP	pozastavit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pivovaru	pivovar	k1gInSc6	pivovar
se	se	k3xPyFc4	se
vařilo	vařit	k5eAaImAgNnS	vařit
pivo	pivo	k1gNnSc1	pivo
Rýbrcoul	Rýbrcoul	k1gMnSc1	Rýbrcoul
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
Pomník	pomník	k1gInSc4	pomník
bojovníkům	bojovník	k1gMnPc3	bojovník
proti	proti	k7c3	proti
fašismu	fašismus	k1gInSc3	fašismus
(	(	kIx(	(
<g/>
MPZ	MPZ	kA	MPZ
<g/>
)	)	kIx)	)
–	–	k?	–
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
Památník	památník	k1gInSc1	památník
Sokola	Sokol	k1gMnSc2	Sokol
–	–	k?	–
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1936	[number]	k4	1936
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Hraničářského	hraničářský	k2eAgInSc2d1	hraničářský
dne	den	k1gInSc2	den
na	na	k7c4	na
oslavu	oslava	k1gFnSc4	oslava
25	[number]	k4	25
<g/>
.	.	kIx.	.
založení	založení	k1gNnSc4	založení
vrchlabského	vrchlabský	k2eAgMnSc2d1	vrchlabský
Sokola	Sokol	k1gMnSc2	Sokol
odhalen	odhalit	k5eAaPmNgInS	odhalit
v	v	k7c6	v
malém	malé	k1gNnSc6	malé
sokoly	sokol	k1gMnPc4	sokol
<g />
.	.	kIx.	.
</s>
<s>
vybudovaném	vybudovaný	k2eAgInSc6d1	vybudovaný
parku	park	k1gInSc6	park
naproti	naproti	k7c3	naproti
nádraží	nádraží	k1gNnSc3	nádraží
kámen	kámen	k1gInSc1	kámen
s	s	k7c7	s
pamětní	pamětní	k2eAgFnSc7d1	pamětní
deskou	deska	k1gFnSc7	deska
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
<g/>
:	:	kIx,	:
Památce	památka	k1gFnSc3	památka
Tyršově	Tyršův	k2eAgFnSc3d1	Tyršova
<g/>
/	/	kIx~	/
věnuje	věnovat	k5eAaImIp3nS	věnovat
<g/>
/	/	kIx~	/
Sokol	Sokol	k1gInSc1	Sokol
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
<g/>
/	/	kIx~	/
v	v	k7c6	v
25	[number]	k4	25
<g/>
.	.	kIx.	.
r.	r.	kA	r.
svého	svůj	k3xOyFgNnSc2	svůj
trvání	trvání	k1gNnSc2	trvání
<g/>
/	/	kIx~	/
1911	[number]	k4	1911
-	-	kIx~	-
1936	[number]	k4	1936
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
byla	být	k5eAaImAgFnS	být
deska	deska	k1gFnSc1	deska
sundána	sundat	k5eAaPmNgFnS	sundat
a	a	k8xC	a
nevysoký	vysoký	k2eNgInSc4d1	nevysoký
prázdný	prázdný	k2eAgInSc4d1	prázdný
pomníček	pomníček	k1gInSc4	pomníček
nad	nad	k7c7	nad
Vápenickým	vápenický	k2eAgInSc7d1	vápenický
potokem	potok	k1gInSc7	potok
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
spatřit	spatřit	k5eAaPmF	spatřit
i	i	k9	i
nyní	nyní	k6eAd1	nyní
<g/>
.	.	kIx.	.
</s>
<s>
Městský	městský	k2eAgInSc1d1	městský
park	park	k1gInSc1	park
–	–	k?	–
Dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
poloopuštěný	poloopuštěný	k2eAgInSc1d1	poloopuštěný
park	park	k1gInSc1	park
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
byl	být	k5eAaImAgMnS	být
díky	díky	k7c3	díky
pronajetí	pronajetí	k1gNnSc3	pronajetí
pozemku	pozemek	k1gInSc2	pozemek
od	od	k7c2	od
Kablíkovy	Kablíkův	k2eAgFnSc2d1	Kablíkův
nadace	nadace	k1gFnSc2	nadace
založen	založen	k2eAgInSc1d1	založen
Městský	městský	k2eAgInSc1d1	městský
park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
plán	plán	k1gInSc4	plán
zhotovil	zhotovit	k5eAaPmAgMnS	zhotovit
Jan	Jan	k1gMnSc1	Jan
Mrázek	Mrázek	k1gMnSc1	Mrázek
<g/>
,	,	kIx,	,
zámecký	zámecký	k2eAgMnSc1d1	zámecký
zahradník	zahradník	k1gMnSc1	zahradník
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
stavitel	stavitel	k1gMnSc1	stavitel
Kleofas	Kleofas	k1gMnSc1	Kleofas
Hollmann	Hollmann	k1gMnSc1	Hollmann
a	a	k8xC	a
zahradník	zahradník	k1gMnSc1	zahradník
Senger	Senger	k1gMnSc1	Senger
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
muži	muž	k1gMnPc1	muž
dokázali	dokázat	k5eAaPmAgMnP	dokázat
městský	městský	k2eAgInSc4d1	městský
park	park	k1gInSc4	park
vybudovat	vybudovat	k5eAaPmF	vybudovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
Park	park	k1gInSc1	park
v	v	k7c6	v
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
delší	dlouhý	k2eAgFnSc6d2	delší
o	o	k7c4	o
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
oproti	oproti	k7c3	oproti
původním	původní	k2eAgInPc3d1	původní
předpokladům	předpoklad	k1gInPc3	předpoklad
<g/>
:	:	kIx,	:
Měřil	měřit	k5eAaImAgMnS	měřit
350	[number]	k4	350
m.	m.	k?	m.
Při	při	k7c6	při
přístupové	přístupový	k2eAgFnSc6d1	přístupová
cestě	cesta	k1gFnSc6	cesta
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
návrší	návrší	k1gNnSc1	návrší
s	s	k7c7	s
břízkami	břízka	k1gFnPc7	břízka
<g/>
,	,	kIx,	,
výše	vysoce	k6eAd2	vysoce
byla	být	k5eAaImAgFnS	být
dvě	dva	k4xCgNnPc4	dva
jezírka	jezírko	k1gNnPc4	jezírko
s	s	k7c7	s
mostem	most	k1gInSc7	most
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
areál	areál	k1gInSc1	areál
se	se	k3xPyFc4	se
však	však	k9	však
stále	stále	k6eAd1	stále
rozšiřoval	rozšiřovat	k5eAaImAgMnS	rozšiřovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1910	[number]	k4	1910
až	až	k9	až
1911	[number]	k4	1911
měl	mít	k5eAaImAgInS	mít
park	park	k1gInSc1	park
rozlohu	rozloha	k1gFnSc4	rozloha
5	[number]	k4	5
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sochy	socha	k1gFnSc2	socha
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Pomocné	pomocný	k2eAgFnSc2d1	pomocná
na	na	k7c6	na
Strážné	strážná	k1gFnSc6	strážná
byly	být	k5eAaImAgFnP	být
vysázeny	vysázen	k2eAgFnPc1d1	vysázena
lípy	lípa	k1gFnPc1	lípa
<g/>
.	.	kIx.	.
</s>
<s>
Výletní	výletní	k2eAgFnPc4d1	výletní
restaurace	restaurace	k1gFnPc4	restaurace
stojící	stojící	k2eAgFnPc4d1	stojící
na	na	k7c6	na
zalesněném	zalesněný	k2eAgNnSc6d1	zalesněné
návrší	návrší	k1gNnSc6	návrší
však	však	k9	však
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1939	[number]	k4	1939
vyhořela	vyhořet	k5eAaPmAgFnS	vyhořet
a	a	k8xC	a
obnovena	obnoven	k2eAgFnSc1d1	obnovena
již	již	k6eAd1	již
nebyla	být	k5eNaImAgFnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgFnSc1d1	dnešní
knihovna	knihovna	k1gFnSc1	knihovna
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Krkonošské	krkonošský	k2eAgFnSc6d1	Krkonošská
ulici	ulice	k1gFnSc6	ulice
vedle	vedle	k7c2	vedle
náměstí	náměstí	k1gNnSc2	náměstí
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
v	v	k7c6	v
bývalém	bývalý	k2eAgInSc6d1	bývalý
objektu	objekt	k1gInSc6	objekt
hotelu	hotel	k1gInSc2	hotel
Zum	Zum	k1gFnSc2	Zum
Mohren	Mohrna	k1gFnPc2	Mohrna
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yIgInSc6	který
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
sídlila	sídlit	k5eAaImAgFnS	sídlit
vrchlabská	vrchlabský	k2eAgFnSc1d1	vrchlabská
ZUŠ	ZUŠ	kA	ZUŠ
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
hudební	hudební	k2eAgFnSc1d1	hudební
škola	škola	k1gFnSc1	škola
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
začala	začít	k5eAaPmAgFnS	začít
fungovat	fungovat	k5eAaImF	fungovat
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1945	[number]	k4	1945
a	a	k8xC	a
stála	stát	k5eAaImAgFnS	stát
ve	v	k7c6	v
Dvořákově	Dvořákův	k2eAgFnSc6d1	Dvořákova
ulici	ulice	k1gFnSc6	ulice
v	v	k7c6	v
bývalém	bývalý	k2eAgNnSc6d1	bývalé
sídle	sídlo	k1gNnSc6	sídlo
stavitelské	stavitelský	k2eAgFnSc2d1	stavitelská
firmy	firma	k1gFnSc2	firma
Hutter	Hutter	k1gMnSc1	Hutter
a	a	k8xC	a
Beránek	Beránek	k1gMnSc1	Beránek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
budovy	budova	k1gFnSc2	budova
se	se	k3xPyFc4	se
hned	hned	k6eAd1	hned
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
na	na	k7c4	na
třídu	třída	k1gFnSc4	třída
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Edvarda	Edvard	k1gMnSc2	Edvard
Beneše	Beneš	k1gMnSc2	Beneš
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Krkonošská	krkonošský	k2eAgFnSc1d1	Krkonošská
ulice	ulice	k1gFnSc1	ulice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budově	budova	k1gFnSc6	budova
dnešní	dnešní	k2eAgFnSc2d1	dnešní
městské	městský	k2eAgFnSc2d1	městská
knihovny	knihovna	k1gFnSc2	knihovna
sídlila	sídlit	k5eAaImAgFnS	sídlit
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
instituce	instituce	k1gFnSc1	instituce
funguje	fungovat	k5eAaImIp3nS	fungovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
Hořejší	Hořejší	k2eAgNnSc2d1	Hořejší
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
Podhůří	Podhůří	k1gNnSc2	Podhůří
-	-	kIx~	-
oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
styku	styk	k1gInSc6	styk
nikdy	nikdy	k6eAd1	nikdy
zcela	zcela	k6eAd1	zcela
nevžil	vžít	k5eNaPmAgMnS	vžít
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
starší	starý	k2eAgInSc1d2	starší
název	název	k1gInSc1	název
Harta	Harta	k1gFnSc1	Harta
<g/>
;	;	kIx,	;
katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
zahrnující	zahrnující	k2eAgNnSc1d1	zahrnující
tuto	tento	k3xDgFnSc4	tento
část	část	k1gFnSc4	část
města	město	k1gNnSc2	město
a	a	k8xC	a
místní	místní	k2eAgFnSc4d1	místní
část	část	k1gFnSc4	část
Liščí	liščí	k2eAgInSc4d1	liščí
kopec	kopec	k1gInSc4	kopec
nese	nést	k5eAaImIp3nS	nést
označení	označení	k1gNnSc4	označení
Podhůří-Harta	Podhůří-Hart	k1gMnSc2	Podhůří-Hart
<g/>
.	.	kIx.	.
</s>
<s>
Liščí	liščí	k2eAgInSc1d1	liščí
kopec	kopec	k1gInSc1	kopec
Kněžice	kněžice	k1gFnSc2	kněžice
Kryštof	Kryštof	k1gMnSc1	Kryštof
z	z	k7c2	z
Gendorfu	Gendorf	k1gInSc2	Gendorf
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
horní	horní	k2eAgMnSc1d1	horní
hejtman	hejtman	k1gMnSc1	hejtman
Království	království	k1gNnSc2	království
českého	český	k2eAgMnSc2d1	český
<g/>
,	,	kIx,	,
původce	původce	k1gMnSc2	původce
povýšení	povýšení	k1gNnSc2	povýšení
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
na	na	k7c4	na
město	město	k1gNnSc4	město
1533	[number]	k4	1533
Georg	Georg	k1gMnSc1	Georg
Ambrosius	Ambrosius	k1gMnSc1	Ambrosius
Tauchmann	Tauchmann	k1gMnSc1	Tauchmann
<g/>
,	,	kIx,	,
varhanář	varhanář	k1gMnSc1	varhanář
<g/>
,	,	kIx,	,
průkopník	průkopník	k1gMnSc1	průkopník
proslulé	proslulý	k2eAgFnSc2d1	proslulá
vrchlabské	vrchlabský	k2eAgFnSc2d1	vrchlabská
varhanářské	varhanářský	k2eAgFnSc2d1	varhanářská
školy	škola	k1gFnSc2	škola
Rolf	Rolf	k1gMnSc1	Rolf
Clemens	Clemensa	k1gFnPc2	Clemensa
Wagner	Wagner	k1gMnSc1	Wagner
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
levicový	levicový	k2eAgMnSc1d1	levicový
terorista	terorista	k1gMnSc1	terorista
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
RAF	raf	k0	raf
Bruno	Bruno	k1gMnSc1	Bruno
Schier	Schier	k1gMnSc1	Schier
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
německo-český	německo-český	k2eAgMnSc1d1	německo-český
germanista	germanista	k1gMnSc1	germanista
a	a	k8xC	a
slavista	slavista	k1gMnSc1	slavista
Ladislav	Ladislav	k1gMnSc1	Ladislav
Rygl	Rygl	k?	Rygl
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
reprezentant	reprezentant	k1gMnSc1	reprezentant
v	v	k7c6	v
severské	severský	k2eAgFnSc6d1	severská
kombinaci	kombinace	k1gFnSc6	kombinace
Ladislav	Ladislav	k1gMnSc1	Ladislav
Rygl	Rygl	k?	Rygl
ml.	ml.	kA	ml.
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
reprezentant	reprezentant	k1gMnSc1	reprezentant
v	v	k7c6	v
severské	severský	k2eAgFnSc6d1	severská
kombinaci	kombinace	k1gFnSc6	kombinace
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Balcar	Balcar	k1gMnSc1	Balcar
<g/>
,	,	kIx,	,
skokan	skokan	k1gMnSc1	skokan
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
Vladimír	Vladimír	k1gMnSc1	Vladimír
Guth	Guth	k1gMnSc1	Guth
<g/>
,	,	kIx,	,
astronom	astronom	k1gMnSc1	astronom
Josefína	Josefína	k1gFnSc1	Josefína
Kablíková	Kablíková	k1gFnSc1	Kablíková
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
žena-botanička	ženaotanička	k1gFnSc1	žena-botanička
v	v	k7c6	v
Rakousko-Uhersku	Rakousko-Uhersko	k1gNnSc6	Rakousko-Uhersko
<g/>
,	,	kIx,	,
objevitelka	objevitelka	k1gFnSc1	objevitelka
a	a	k8xC	a
systematička	systematička	k1gFnSc1	systematička
mnoha	mnoho	k4c2	mnoho
druhů	druh	k1gInPc2	druh
<g />
.	.	kIx.	.
</s>
<s>
krkonošské	krkonošský	k2eAgFnSc2d1	Krkonošská
flóry	flóra	k1gFnSc2	flóra
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Kablík	kablík	k1gInSc1	kablík
<g/>
,	,	kIx,	,
chemik	chemik	k1gMnSc1	chemik
<g/>
,	,	kIx,	,
objevitel	objevitel	k1gMnSc1	objevitel
moderního	moderní	k2eAgInSc2d1	moderní
postupu	postup	k1gInSc2	postup
bělení	bělení	k1gNnSc2	bělení
papíru	papír	k1gInSc2	papír
a	a	k8xC	a
bezpečnostní	bezpečnostní	k2eAgFnSc2d1	bezpečnostní
škrtací	škrtací	k2eAgFnSc2d1	škrtací
směsi	směs	k1gFnSc2	směs
na	na	k7c4	na
zápalky	zápalka	k1gFnPc4	zápalka
Karel	Karla	k1gFnPc2	Karla
Halíř	halíř	k1gInSc1	halíř
<g/>
,	,	kIx,	,
houslista	houslista	k1gMnSc1	houslista
<g/>
,	,	kIx,	,
sólista	sólista	k1gMnSc1	sólista
Berlínské	berlínský	k2eAgFnSc2d1	Berlínská
filharmonie	filharmonie	k1gFnSc2	filharmonie
Josef	Josef	k1gMnSc1	Josef
Tippelt	Tippelt	k1gMnSc1	Tippelt
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
-	-	kIx~	-
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
učitel	učitel	k1gMnSc1	učitel
<g/>
,	,	kIx,	,
odpůrce	odpůrce	k1gMnSc1	odpůrce
nacismu	nacismus	k1gInSc2	nacismus
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
popraven	popraven	k2eAgMnSc1d1	popraven
Pavel	Pavel	k1gMnSc1	Pavel
Wonka	Wonka	k1gMnSc1	Wonka
<g/>
,	,	kIx,	,
politický	politický	k2eAgMnSc1d1	politický
vězeň	vězeň	k1gMnSc1	vězeň
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
<g/>
,	,	kIx,	,
občanský	občanský	k2eAgMnSc1d1	občanský
aktivista	aktivista	k1gMnSc1	aktivista
Viktor	Viktor	k1gMnSc1	Viktor
Kugler	Kugler	k1gMnSc1	Kugler
<g/>
,	,	kIx,	,
obchodník	obchodník	k1gMnSc1	obchodník
<g/>
,	,	kIx,	,
ochránce	ochránce	k1gMnSc1	ochránce
rodiny	rodina	k1gFnSc2	rodina
Anny	Anna	k1gFnSc2	Anna
Frankové	Franková	k1gFnSc2	Franková
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
vysokých	vysoká	k1gFnPc2	vysoká
státních	státní	k2eAgNnPc2d1	státní
vyznamenání	vyznamenání	k1gNnPc2	vyznamenání
Izraele	Izrael	k1gInSc2	Izrael
a	a	k8xC	a
Kanady	Kanada	k1gFnSc2	Kanada
Pavel	Pavel	k1gMnSc1	Pavel
Kuchyňka	Kuchyňka	k1gMnSc1	Kuchyňka
<g/>
,	,	kIx,	,
oftalmolog	oftalmolog	k1gMnSc1	oftalmolog
Karel	Karel	k1gMnSc1	Karel
Šebek	Šebek	k1gMnSc1	Šebek
<g/>
,	,	kIx,	,
surrealistický	surrealistický	k2eAgMnSc1d1	surrealistický
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
výtvarník	výtvarník	k1gMnSc1	výtvarník
Konrád	Konrád	k1gMnSc1	Konrád
Wiesner	Wiesner	k1gMnSc1	Wiesner
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
a	a	k8xC	a
mědirytec	mědirytec	k1gMnSc1	mědirytec
Vincenc	Vincenc	k1gMnSc1	Vincenc
Erben	Erben	k1gMnSc1	Erben
<g/>
,	,	kIx,	,
archivář	archivář	k1gMnSc1	archivář
<g/>
,	,	kIx,	,
blízký	blízký	k2eAgMnSc1d1	blízký
spolupracovník	spolupracovník	k1gMnSc1	spolupracovník
Františka	František	k1gMnSc4	František
Palackého	Palacký	k1gMnSc4	Palacký
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
Svatováclavského	svatováclavský	k2eAgInSc2d1	svatováclavský
korunního	korunní	k2eAgInSc2d1	korunní
archivu	archiv	k1gInSc2	archiv
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Anna	Anna	k1gFnSc1	Anna
K	k	k7c3	k
(	(	kIx(	(
<g/>
vl	vl	k?	vl
<g/>
.	.	kIx.	.
jm	jm	k?	jm
<g/>
.	.	kIx.	.
</s>
<s>
Lucianna	Lucianen	k2eAgFnSc1d1	Lucianna
Krecarová	Krecarová	k1gFnSc1	Krecarová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
a	a	k8xC	a
herečka	herečka	k1gFnSc1	herečka
Martin	Martin	k1gMnSc1	Martin
Veselovský	Veselovský	k1gMnSc1	Veselovský
<g/>
,	,	kIx,	,
TV	TV	kA	TV
a	a	k8xC	a
rozhlasový	rozhlasový	k2eAgMnSc1d1	rozhlasový
redaktor	redaktor	k1gMnSc1	redaktor
a	a	k8xC	a
moderátor	moderátor	k1gMnSc1	moderátor
Kamila	Kamila	k1gFnSc1	Kamila
Rundusová	Rundusový	k2eAgFnSc1d1	Rundusový
<g/>
,	,	kIx,	,
šéfkuchařka	šéfkuchařka	k1gFnSc1	šéfkuchařka
a	a	k8xC	a
popularizátorka	popularizátorka	k1gFnSc1	popularizátorka
asijského	asijský	k2eAgNnSc2d1	asijské
jídla	jídlo	k1gNnSc2	jídlo
Marie	Marie	k1gFnSc1	Marie
Kubátová	Kubátová	k1gFnSc1	Kubátová
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
a	a	k8xC	a
sběratelka	sběratelka	k1gFnSc1	sběratelka
krkonošských	krkonošský	k2eAgFnPc2d1	Krkonošská
pohádek	pohádka	k1gFnPc2	pohádka
Kryštof	Kryštof	k1gMnSc1	Kryštof
Ludwig	Ludwig	k1gMnSc1	Ludwig
<g/>
,	,	kIx,	,
barokní	barokní	k2eAgMnSc1d1	barokní
puškař	puškař	k1gMnSc1	puškař
Theodor	Theodor	k1gMnSc1	Theodor
Petera	Petera	k1gMnSc1	Petera
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
automobilového	automobilový	k2eAgInSc2d1	automobilový
průmyslu	průmysl	k1gInSc2	průmysl
<g />
.	.	kIx.	.
</s>
<s>
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
Hana	Hana	k1gFnSc1	Hana
Jüptnerová	Jüptnerová	k1gFnSc1	Jüptnerová
<g/>
,	,	kIx,	,
disidentka	disidentka	k1gFnSc1	disidentka
Karolína	Karolína	k1gFnSc1	Karolína
Erbanová	Erbanová	k1gFnSc1	Erbanová
<g/>
,	,	kIx,	,
rychlobruslařka	rychlobruslařka	k1gFnSc1	rychlobruslařka
Radomír	Radomír	k1gMnSc1	Radomír
Šimůnek	Šimůnek	k1gMnSc1	Šimůnek
<g/>
,	,	kIx,	,
československý	československý	k2eAgMnSc1d1	československý
a	a	k8xC	a
český	český	k2eAgMnSc1d1	český
cyklokrosový	cyklokrosový	k2eAgMnSc1d1	cyklokrosový
reprezentant	reprezentant	k1gMnSc1	reprezentant
Veronika	Veronika	k1gFnSc1	Veronika
Vítková	Vítková	k1gFnSc1	Vítková
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
reprezentantka	reprezentantka	k1gFnSc1	reprezentantka
v	v	k7c6	v
biatlonu	biatlon	k1gInSc6	biatlon
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Vítek	Vítek	k1gMnSc1	Vítek
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
reprezentant	reprezentant	k1gMnSc1	reprezentant
v	v	k7c6	v
biatlonu	biatlon	k1gInSc6	biatlon
Jan	Jan	k1gMnSc1	Jan
Matouš	Matouš	k1gMnSc1	Matouš
<g/>
,	,	kIx,	,
biatlonista	biatlonista	k1gMnSc1	biatlonista
Radek	Radek	k1gMnSc1	Radek
Dejmek	Dejmek	k1gMnSc1	Dejmek
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
<g/>
,	,	kIx,	,
obránce	obránce	k1gMnSc1	obránce
<g />
.	.	kIx.	.
</s>
<s>
FC	FC	kA	FC
Slovan	Slovan	k1gInSc1	Slovan
Liberec	Liberec	k1gInSc1	Liberec
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Vašina	Vašina	k1gMnSc1	Vašina
<g/>
,	,	kIx,	,
hornista	hornista	k1gMnSc1	hornista
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Pražské	pražský	k2eAgFnSc2d1	Pražská
komorní	komorní	k2eAgFnSc2d1	komorní
filharmonie	filharmonie	k1gFnSc2	filharmonie
Aleš	Aleš	k1gMnSc1	Aleš
Suk	Suk	k1gMnSc1	Suk
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
dokumentarista	dokumentarista	k1gMnSc1	dokumentarista
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Hakl	Hakl	k1gMnSc1	Hakl
<g/>
,	,	kIx,	,
automobilový	automobilový	k2eAgMnSc1d1	automobilový
závodník	závodník	k1gMnSc1	závodník
Petr	Petr	k1gMnSc1	Petr
Kuchař	Kuchař	k1gMnSc1	Kuchař
<g/>
,	,	kIx,	,
motocyklový	motocyklový	k2eAgMnSc1d1	motocyklový
jezdec	jezdec	k1gMnSc1	jezdec
<g/>
,	,	kIx,	,
průkopník	průkopník	k1gMnSc1	průkopník
freestyle	freestyl	k1gInSc5	freestyl
motocrossu	motocross	k1gInSc2	motocross
Jiří	Jiří	k1gMnSc1	Jiří
Jebavý	Jebavý	k1gMnSc1	Jebavý
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
<g/>
,	,	kIx,	,
obránce	obránce	k1gMnSc1	obránce
HC	HC	kA	HC
Slavia	Slavia	k1gFnSc1	Slavia
Praha	Praha	k1gFnSc1	Praha
Lukáš	Lukáš	k1gMnSc1	Lukáš
Bolf	Bolf	k1gMnSc1	Bolf
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
<g/>
,	,	kIx,	,
obránce	obránce	k1gMnSc1	obránce
HC	HC	kA	HC
Oceláři	ocelář	k1gMnPc1	ocelář
Třinec	Třinec	k1gInSc4	Třinec
Eva	Eva	k1gFnSc1	Eva
Samková	Samková	k1gFnSc1	Samková
<g/>
,	,	kIx,	,
snowboardistka	snowboardistka	k1gFnSc1	snowboardistka
Lucie	Lucie	k1gFnSc1	Lucie
Charvátová	Charvátová	k1gFnSc1	Charvátová
<g/>
,	,	kIx,	,
biatlonistka	biatlonistka	k1gFnSc1	biatlonistka
Amálie	Amálie	k1gFnSc1	Amálie
Kutinová	kutinový	k2eAgFnSc1d1	Kutinová
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
-	-	kIx~	-
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
a	a	k8xC	a
folkloristka	folkloristka	k1gFnSc1	folkloristka
Miloš	Miloš	k1gMnSc1	Miloš
Gerstner	Gerstner	k1gMnSc1	Gerstner
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
-	-	kIx~	-
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podkrkonošský	podkrkonošský	k2eAgMnSc1d1	podkrkonošský
literát	literát	k1gMnSc1	literát
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
Miloš	Miloš	k1gMnSc1	Miloš
Fišera	Fišer	k1gMnSc2	Fišer
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
československý	československý	k2eAgMnSc1d1	československý
reprezentant	reprezentant	k1gMnSc1	reprezentant
v	v	k7c6	v
cyklokrosu	cyklokros	k1gInSc6	cyklokros
Václav	Václav	k1gMnSc1	Václav
Kuťák	Kuťák	k1gMnSc1	Kuťák
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
-	-	kIx~	-
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
učitel	učitel	k1gMnSc1	učitel
a	a	k8xC	a
lichenolog	lichenolog	k1gMnSc1	lichenolog
Baunatal	Baunatal	k1gMnSc1	Baunatal
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
Trouville	Trouville	k1gFnSc2	Trouville
sur	sur	k?	sur
Mer	Mer	k1gFnSc2	Mer
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
Kowary	Kowara	k1gFnSc2	Kowara
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
Ovidiopol	Ovidiopola	k1gFnPc2	Ovidiopola
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
</s>
