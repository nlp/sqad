<s>
Josh	Josh	k1gMnSc1	Josh
Hartnett	Hartnett	k1gMnSc1	Hartnett
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Joshua	Joshua	k1gMnSc1	Joshua
Daniel	Daniel	k1gMnSc1	Daniel
Hartnett	Hartnett	k1gMnSc1	Hartnett
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
Saint	Saint	k1gMnSc1	Saint
Paul	Paul	k1gMnSc1	Paul
<g/>
,	,	kIx,	,
Minnesota	Minnesota	k1gFnSc1	Minnesota
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
<g/>
.	.	kIx.	.
</s>
<s>
Josh	Josh	k1gMnSc1	Josh
Hartnett	Hartnett	k1gMnSc1	Hartnett
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
21	[number]	k4	21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1978	[number]	k4	1978
v	v	k7c6	v
minnesotské	minnesotský	k2eAgFnSc6d1	Minnesotská
Nemocnici	nemocnice	k1gFnSc6	nemocnice
Sv.	sv.	kA	sv.
Pavla	Pavel	k1gMnSc2	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
ze	z	k7c2	z
čtyř	čtyři	k4xCgMnPc2	čtyři
sourozenců	sourozenec	k1gMnPc2	sourozenec
(	(	kIx(	(
<g/>
jména	jméno	k1gNnSc2	jméno
dalších	další	k2eAgInPc2d1	další
tří	tři	k4xCgInPc2	tři
jsou	být	k5eAaImIp3nP	být
Jake	Jake	k1gFnSc1	Jake
<g/>
,	,	kIx,	,
Joe	Joe	k1gFnSc1	Joe
a	a	k8xC	a
Jessica	Jessica	k1gFnSc1	Jessica
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
studia	studio	k1gNnSc2	studio
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
Sv.	sv.	kA	sv.
Pavla	Pavel	k1gMnSc2	Pavel
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgInS	mít
příležitost	příležitost	k1gFnSc4	příležitost
seznámit	seznámit	k5eAaPmF	seznámit
se	se	k3xPyFc4	se
s	s	k7c7	s
budoucí	budoucí	k2eAgFnSc7d1	budoucí
herečkou	herečka	k1gFnSc7	herečka
Rachel	Rachel	k1gMnSc1	Rachel
Leigh	Leigh	k1gMnSc1	Leigh
Cookovou	Cooková	k1gFnSc4	Cooková
<g/>
,	,	kIx,	,
hrál	hrát	k5eAaImAgMnS	hrát
fotbal	fotbal	k1gInSc4	fotbal
a	a	k8xC	a
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
místním	místní	k2eAgInSc6d1	místní
obchodě	obchod	k1gInSc6	obchod
s	s	k7c7	s
multimédii	multimédium	k1gNnPc7	multimédium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
šestnácti	šestnáct	k4xCc2	šestnáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
ale	ale	k9	ale
nepříjemně	příjemně	k6eNd1	příjemně
poranil	poranit	k5eAaPmAgMnS	poranit
nohu	noha	k1gFnSc4	noha
a	a	k8xC	a
s	s	k7c7	s
fotbalem	fotbal	k1gInSc7	fotbal
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgInS	muset
na	na	k7c4	na
rok	rok	k1gInSc4	rok
rozloučit	rozloučit	k5eAaPmF	rozloučit
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
právě	právě	k9	právě
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
si	se	k3xPyFc3	se
našel	najít	k5eAaPmAgMnS	najít
další	další	k2eAgFnSc4d1	další
zálibu	záliba	k1gFnSc4	záliba
–	–	k?	–
mládežnické	mládežnický	k2eAgNnSc1d1	mládežnické
herectví	herectví	k1gNnSc1	herectví
v	v	k7c6	v
lokálních	lokální	k2eAgInPc6d1	lokální
spolcích	spolek	k1gInPc6	spolek
jako	jako	k8xC	jako
Childrens	Childrens	k1gInSc1	Childrens
Theatre	Theatr	k1gInSc5	Theatr
Company	Compana	k1gFnPc1	Compana
<g/>
,	,	kIx,	,
Youth	Youth	k1gInSc1	Youth
Performance	performance	k1gFnSc2	performance
Company	Compana	k1gFnSc2	Compana
nebo	nebo	k8xC	nebo
Stepping	Stepping	k1gInSc1	Stepping
Stone	ston	k1gInSc5	ston
Company	Compan	k1gMnPc4	Compan
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
maturitě	maturita	k1gFnSc6	maturita
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
Josh	Josh	k1gMnSc1	Josh
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
o	o	k7c4	o
studium	studium	k1gNnSc4	studium
herectví	herectví	k1gNnSc2	herectví
na	na	k7c4	na
State	status	k1gInSc5	status
University	universita	k1gFnSc2	universita
of	of	k?	of
New	New	k1gMnSc4	New
York	York	k1gInSc1	York
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
však	však	k9	však
nenašel	najít	k5eNaPmAgInS	najít
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
působení	působení	k1gNnSc2	působení
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
dnes	dnes	k6eAd1	dnes
vyhýbavě	vyhýbavě	k6eAd1	vyhýbavě
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
silnou	silný	k2eAgFnSc4d1	silná
averzi	averze	k1gFnSc4	averze
vůči	vůči	k7c3	vůči
systému	systém	k1gInSc3	systém
vyučování	vyučování	k1gNnSc2	vyučování
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tam	tam	k6eAd1	tam
panoval	panovat	k5eAaImAgInS	panovat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
školu	škola	k1gFnSc4	škola
brzy	brzy	k6eAd1	brzy
opustil	opustit	k5eAaPmAgMnS	opustit
a	a	k8xC	a
vydal	vydat	k5eAaPmAgMnS	vydat
se	se	k3xPyFc4	se
na	na	k7c4	na
opačný	opačný	k2eAgInSc4d1	opačný
břeh	břeh	k1gInSc4	břeh
USA	USA	kA	USA
<g/>
,	,	kIx,	,
do	do	k7c2	do
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
měl	mít	k5eAaImAgInS	mít
příležitost	příležitost	k1gFnSc4	příležitost
zahrát	zahrát	k5eAaPmF	zahrát
si	se	k3xPyFc3	se
drobné	drobný	k2eAgFnPc4d1	drobná
role	role	k1gFnPc4	role
v	v	k7c6	v
šesti	šest	k4xCc6	šest
epizodách	epizoda	k1gFnPc6	epizoda
seriálu	seriál	k1gInSc2	seriál
"	"	kIx"	"
<g/>
Dawsons	Dawsons	k1gInSc1	Dawsons
Creek	Creek	k1gInSc1	Creek
<g/>
"	"	kIx"	"
a	a	k8xC	a
první	první	k4xOgFnSc4	první
větší	veliký	k2eAgFnSc4d2	veliký
úlohu	úloha	k1gFnSc4	úloha
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
"	"	kIx"	"
<g/>
Cracker	Cracker	k1gMnSc1	Cracker
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
referencemi	reference	k1gFnPc7	reference
režisérů	režisér	k1gMnPc2	režisér
ohledně	ohledně	k7c2	ohledně
jeho	jeho	k3xOp3gFnSc2	jeho
pracovitosti	pracovitost	k1gFnSc2	pracovitost
tak	tak	k6eAd1	tak
vnikl	vniknout	k5eAaPmAgMnS	vniknout
do	do	k7c2	do
podvědomí	podvědomí	k1gNnSc2	podvědomí
Hollywoodu	Hollywood	k1gInSc2	Hollywood
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
další	další	k2eAgInSc4d1	další
rok	rok	k1gInSc4	rok
se	se	k3xPyFc4	se
Joshovi	Joshův	k2eAgMnPc1d1	Joshův
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
rolí	role	k1gFnPc2	role
v	v	k7c6	v
hororu	horor	k1gInSc6	horor
Halloween	Halloween	k2eAgMnSc1d1	Halloween
H20	H20	k1gMnSc1	H20
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
:	:	kIx,	:
Singularity	singularita	k1gFnSc2	singularita
(	(	kIx(	(
<g/>
Singularity	singularita	k1gFnSc2	singularita
<g/>
)	)	kIx)	)
–	–	k?	–
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
James	James	k1gMnSc1	James
Stewart	Stewart	k1gMnSc1	Stewart
<g/>
/	/	kIx~	/
<g/>
Jay	Jay	k1gMnSc1	Jay
Fennel	Fennel	k1gMnSc1	Fennel
2011	[number]	k4	2011
:	:	kIx,	:
Stuck	Stuck	k1gInSc4	Stuck
Between	Between	k2eAgInSc4d1	Between
Stations	Stations	k1gInSc4	Stations
(	(	kIx(	(
<g/>
Stuck	Stuck	k1gInSc4	Stuck
Between	Between	k2eAgInSc4d1	Between
Stations	Stations	k1gInSc4	Stations
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
–	–	k?	–
role	role	k1gFnSc2	role
<g/>
:	:	kIx,	:
Paddy	Padda	k1gFnSc2	Padda
2011	[number]	k4	2011
:	:	kIx,	:
Girl	girl	k1gFnSc6	girl
Walks	Walksa	k1gFnPc2	Walksa
Into	Into	k6eAd1	Into
a	a	k8xC	a
Bar	bar	k1gInSc4	bar
(	(	kIx(	(
<g/>
Girl	girl	k1gFnSc4	girl
Walks	Walksa	k1gFnPc2	Walksa
Into	Into	k6eAd1	Into
a	a	k8xC	a
Bar	bar	k1gInSc4	bar
<g/>
)	)	kIx)	)
–	–	k?	–
role	role	k1gFnSc2	role
<g/>
:	:	kIx,	:
Sam	Sam	k1gMnSc1	Sam
Salazar	Salazar	k1gInSc4	Salazar
2010	[number]	k4	2010
:	:	kIx,	:
Bunraku	Bunrak	k1gInSc2	Bunrak
(	(	kIx(	(
<g/>
Bunraku	Bunrak	k1gInSc2	Bunrak
<g/>
)	)	kIx)	)
–	–	k?	–
role	role	k1gFnSc2	role
<g/>
:	:	kIx,	:
The	The	k1gFnPc2	The
Drifter	Driftrum	k1gNnPc2	Driftrum
2009	[number]	k4	2009
:	:	kIx,	:
Přicházím	přicházet	k5eAaImIp1nS	přicházet
s	s	k7c7	s
deštěm	dešť	k1gInSc7	dešť
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
I	i	k9	i
Come	Come	k1gNnSc1	Come
with	with	k1gMnSc1	with
the	the	k?	the
Rain	Rain	k1gMnSc1	Rain
<g/>
)	)	kIx)	)
–	–	k?	–
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Kline	klinout	k5eAaImIp3nS	klinout
2008	[number]	k4	2008
:	:	kIx,	:
Srpen	srpen	k1gInSc4	srpen
před	před	k7c7	před
bouří	bouř	k1gFnSc7	bouř
(	(	kIx(	(
<g/>
August	August	k1gMnSc1	August
<g/>
)	)	kIx)	)
–	–	k?	–
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Tom	Tom	k1gMnSc1	Tom
Sterling	sterling	k1gInSc4	sterling
2008	[number]	k4	2008
:	:	kIx,	:
T	T	kA	T
Takes	Takes	k1gMnSc1	Takes
(	(	kIx(	(
<g/>
T	T	kA	T
Takes	Takes	k1gInSc1	Takes
<g/>
)	)	kIx)	)
–	–	k?	–
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Josh	Josh	k1gInSc1	Josh
2007	[number]	k4	2007
:	:	kIx,	:
30	[number]	k4	30
dní	den	k1gInPc2	den
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
noc	noc	k1gFnSc1	noc
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
30	[number]	k4	30
Days	Days	k1gInSc1	Days
of	of	k?	of
Night	Night	k1gInSc1	Night
<g/>
)	)	kIx)	)
–	–	k?	–
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Eben	eben	k1gInSc1	eben
Oleson	Oleson	k1gNnSc1	Oleson
2007	[number]	k4	2007
:	:	kIx,	:
Reportér	reportér	k1gMnSc1	reportér
v	v	k7c6	v
ringu	ring	k1gInSc6	ring
(	(	kIx(	(
<g/>
Resurrecting	Resurrecting	k1gInSc1	Resurrecting
the	the	k?	the
Champ	Champ	k1gInSc1	Champ
<g/>
)	)	kIx)	)
–	–	k?	–
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Erik	Erik	k1gMnSc1	Erik
Kernan	Kernan	k1gMnSc1	Kernan
2007	[number]	k4	2007
:	:	kIx,	:
Příběhy	příběh	k1gInPc4	příběh
z	z	k7c2	z
USA	USA	kA	USA
(	(	kIx(	(
<g/>
Stories	Stories	k1gMnSc1	Stories
USA	USA	kA	USA
<g/>
)	)	kIx)	)
–	–	k?	–
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Gianni	Gianeň	k1gFnSc6	Gianeň
2006	[number]	k4	2006
:	:	kIx,	:
Černá	Černá	k1gFnSc1	Černá
<g />
.	.	kIx.	.
</s>
<s>
Dahlia	Dahlia	k1gFnSc1	Dahlia
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Black	Black	k1gInSc1	Black
Dahlia	Dahlia	k1gFnSc1	Dahlia
<g/>
)	)	kIx)	)
–	–	k?	–
role	role	k1gFnSc2	role
<g/>
:	:	kIx,	:
Dwight	Dwight	k2eAgMnSc1d1	Dwight
'	'	kIx"	'
<g/>
Bucky	bucek	k1gMnPc4	bucek
<g/>
'	'	kIx"	'
Bleichert	Bleichert	k1gInSc1	Bleichert
2006	[number]	k4	2006
:	:	kIx,	:
Nabít	nabít	k5eAaBmF	nabít
a	a	k8xC	a
zabít	zabít	k5eAaPmF	zabít
(	(	kIx(	(
<g/>
Lucky	lucky	k6eAd1	lucky
Number	Number	k1gInSc1	Number
Slevin	Slevina	k1gFnPc2	Slevina
<g/>
)	)	kIx)	)
–	–	k?	–
role	role	k1gFnSc2	role
<g/>
:	:	kIx,	:
Slevin	Slevina	k1gFnPc2	Slevina
Kelevra	Kelevr	k1gInSc2	Kelevr
2005	[number]	k4	2005
:	:	kIx,	:
Sin	sin	kA	sin
City	city	k1gNnSc1	city
-	-	kIx~	-
Město	město	k1gNnSc1	město
hříchu	hřích	k1gInSc2	hřích
(	(	kIx(	(
<g/>
Sin	sin	kA	sin
City	city	k1gNnSc1	city
<g/>
)	)	kIx)	)
–	–	k?	–
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
nájemný	nájemný	k2eAgInSc1d1	nájemný
zabiják	zabiják	k1gInSc1	zabiják
2005	[number]	k4	2005
:	:	kIx,	:
Zamilovaní	zamilovaný	k2eAgMnPc1d1	zamilovaný
blázni	blázen	k1gMnPc1	blázen
(	(	kIx(	(
<g/>
Mozart	Mozart	k1gMnSc1	Mozart
and	and	k?	and
the	the	k?	the
Whale	Whale	k1gInSc1	Whale
<g/>
)	)	kIx)	)
–	–	k?	–
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Donald	Donald	k1gMnSc1	Donald
Morton	Morton	k1gInSc4	Morton
2004	[number]	k4	2004
:	:	kIx,	:
Miluj	milovat	k5eAaImRp2nS	milovat
mě	já	k3xPp1nSc2	já
<g/>
!	!	kIx.	!
</s>
<s>
...	...	k?	...
<g/>
prosím	prosit	k5eAaImIp1nS	prosit
(	(	kIx(	(
<g/>
Wicker	Wicker	k1gInSc4	Wicker
Park	park	k1gInSc4	park
<g/>
)	)	kIx)	)
–	–	k?	–
role	role	k1gFnSc2	role
<g/>
:	:	kIx,	:
Matthew	Matthew	k1gFnSc2	Matthew
2003	[number]	k4	2003
:	:	kIx,	:
Detektivové	detektiv	k1gMnPc1	detektiv
z	z	k7c2	z
Hollywoodu	Hollywood	k1gInSc2	Hollywood
(	(	kIx(	(
<g/>
Hollywood	Hollywood	k1gInSc4	Hollywood
Homicide	Homicid	k1gInSc5	Homicid
<g/>
)	)	kIx)	)
–	–	k?	–
role	role	k1gFnSc2	role
<g/>
:	:	kIx,	:
K.C.	K.C.	k1gFnPc2	K.C.
Calden	Caldno	k1gNnPc2	Caldno
2002	[number]	k4	2002
:	:	kIx,	:
40	[number]	k4	40
dnů	den	k1gInPc2	den
a	a	k8xC	a
40	[number]	k4	40
nocí	noc	k1gFnPc2	noc
(	(	kIx(	(
<g/>
40	[number]	k4	40
Days	Days	k1gInSc4	Days
and	and	k?	and
40	[number]	k4	40
Nights	Nightsa	k1gFnPc2	Nightsa
<g/>
)	)	kIx)	)
–	–	k?	–
role	role	k1gFnSc2	role
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
Matt	Matt	k2eAgMnSc1d1	Matt
Sullivan	Sullivan	k1gMnSc1	Sullivan
2001	[number]	k4	2001
:	:	kIx,	:
Černý	černý	k2eAgMnSc1d1	černý
jestřáb	jestřáb	k1gMnSc1	jestřáb
sestřelen	sestřelen	k2eAgMnSc1d1	sestřelen
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Hawk	Hawk	k1gMnSc1	Hawk
Down	Down	k1gMnSc1	Down
<g/>
)	)	kIx)	)
–	–	k?	–
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Eversmann	Eversmann	k1gInSc1	Eversmann
2001	[number]	k4	2001
:	:	kIx,	:
O	O	kA	O
/	/	kIx~	/
Othello	Othello	k1gMnSc1	Othello
(	(	kIx(	(
<g/>
O	O	kA	O
<g/>
)	)	kIx)	)
–	–	k?	–
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Hugo	Hugo	k1gMnSc1	Hugo
Goulding	Goulding	k1gInSc4	Goulding
2001	[number]	k4	2001
:	:	kIx,	:
Pearl	Pearl	k1gMnSc1	Pearl
Harbor	Harbor	k1gMnSc1	Harbor
(	(	kIx(	(
<g/>
Pearl	Pearl	k1gMnSc1	Pearl
Harbor	Harbor	k1gMnSc1	Harbor
<g/>
)	)	kIx)	)
–	–	k?	–
role	role	k1gFnPc4	role
<g/>
:	:	kIx,	:
Danny	Dann	k1gInPc4	Dann
Walker	Walker	k1gInSc1	Walker
2001	[number]	k4	2001
:	:	kIx,	:
Dohola	dohola	k6eAd1	dohola
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
Blow	Blow	k1gMnSc2	Blow
Dry	Dry	k1gMnSc2	Dry
<g/>
)	)	kIx)	)
–	–	k?	–
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Brian	Brian	k1gMnSc1	Brian
Allen	Allen	k1gMnSc1	Allen
2001	[number]	k4	2001
:	:	kIx,	:
Taková	takový	k3xDgFnSc1	takový
rodinná	rodinný	k2eAgFnSc1d1	rodinná
romance	romance	k1gFnSc1	romance
(	(	kIx(	(
<g/>
Town	Town	k1gInSc1	Town
&	&	k?	&
Country	country	k2eAgMnSc2d1	country
<g/>
)	)	kIx)	)
–	–	k?	–
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Tom	Tom	k1gMnSc1	Tom
Stoddard	Stoddard	k1gMnSc1	Stoddard
2000	[number]	k4	2000
:	:	kIx,	:
Nejpevnější	pevný	k2eAgNnSc1d3	nejpevnější
pouto	pouto	k1gNnSc1	pouto
(	(	kIx(	(
<g/>
Here	Her	k1gMnSc2	Her
on	on	k3xPp3gMnSc1	on
Earth	Earth	k1gMnSc1	Earth
<g/>
)	)	kIx)	)
–	–	k?	–
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Jasper	Jasper	k1gMnSc1	Jasper
Arnold	Arnold	k1gMnSc1	Arnold
1999	[number]	k4	1999
:	:	kIx,	:
Smrt	smrt	k1gFnSc1	smrt
<g />
.	.	kIx.	.
</s>
<s>
panen	panna	k1gFnPc2	panna
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Virgin	Virgin	k1gMnSc1	Virgin
Suicides	Suicides	k1gMnSc1	Suicides
<g/>
)	)	kIx)	)
–	–	k?	–
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Trip	Trip	k1gInSc1	Trip
Fontaine	Fontain	k1gInSc5	Fontain
1998	[number]	k4	1998
:	:	kIx,	:
Fakulta	fakulta	k1gFnSc1	fakulta
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Faculty	Facult	k1gInPc1	Facult
<g/>
)	)	kIx)	)
–	–	k?	–
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Zeke	Zeke	k1gFnSc1	Zeke
Tyler	Tyler	k1gInSc1	Tyler
1998	[number]	k4	1998
:	:	kIx,	:
Halloween	Halloween	k1gInSc1	Halloween
<g/>
:	:	kIx,	:
H20	H20	k1gFnSc1	H20
(	(	kIx(	(
<g/>
Halloween	Halloween	k1gInSc1	Halloween
H	H	kA	H
<g/>
20	[number]	k4	20
<g/>
:	:	kIx,	:
20	[number]	k4	20
Years	Years	k1gInSc1	Years
Later	Later	k1gInSc4	Later
<g/>
)	)	kIx)	)
–	–	k?	–
role	role	k1gFnSc1	role
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
John	John	k1gMnSc1	John
Tate	Tat	k1gFnSc2	Tat
1998	[number]	k4	1998
:	:	kIx,	:
Debutante	debutant	k1gMnSc5	debutant
(	(	kIx(	(
<g/>
Debutante	debutant	k1gMnSc5	debutant
<g/>
)	)	kIx)	)
–	–	k?	–
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Bill	Bill	k1gMnSc1	Bill
(	(	kIx(	(
<g/>
Krátkometrážní	krátkometrážní	k2eAgMnSc1d1	krátkometrážní
<g/>
)	)	kIx)	)
1997	[number]	k4	1997
<g/>
–	–	k?	–
<g/>
1999	[number]	k4	1999
:	:	kIx,	:
Cracker	Cracker	k1gInSc1	Cracker
(	(	kIx(	(
<g/>
Cracker	Cracker	k1gInSc1	Cracker
<g/>
)	)	kIx)	)
–	–	k?	–
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Michael	Michael	k1gMnSc1	Michael
'	'	kIx"	'
<g/>
Fitz	Fitz	k1gMnSc1	Fitz
<g/>
'	'	kIx"	'
Fitzgerald	Fitzgerald	k1gMnSc1	Fitzgerald
(	(	kIx(	(
<g/>
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
2008	[number]	k4	2008
:	:	kIx,	:
Srpen	srpen	k1gInSc4	srpen
před	před	k7c7	před
bouří	bouř	k1gFnSc7	bouř
(	(	kIx(	(
<g/>
August	August	k1gMnSc1	August
<g/>
)	)	kIx)	)
2009	[number]	k4	2009
:	:	kIx,	:
Nobody	Noboda	k1gFnSc2	Noboda
(	(	kIx(	(
<g/>
Nobody	Noboda	k1gFnSc2	Noboda
<g/>
)	)	kIx)	)
</s>
