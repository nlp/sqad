<p>
<s>
FC	FC	kA	FC
Čichura	Čichura	k1gFnSc1	Čichura
Sačchere	Sačcher	k1gInSc5	Sačcher
(	(	kIx(	(
<g/>
gruzínsky	gruzínsky	k6eAd1	gruzínsky
ჩ	ჩ	k?	ჩ
ს	ს	k?	ს
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
gruzínský	gruzínský	k2eAgInSc1d1	gruzínský
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
z	z	k7c2	z
města	město	k1gNnSc2	město
Sačchere	Sačcher	k1gInSc5	Sačcher
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
zápasy	zápas	k1gInPc4	zápas
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c6	na
Centrálním	centrální	k2eAgInSc6d1	centrální
stadionu	stadion	k1gInSc6	stadion
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úspěchy	úspěch	k1gInPc1	úspěch
==	==	k?	==
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
×	×	k?	×
vítěz	vítěz	k1gMnSc1	vítěz
gruzínského	gruzínský	k2eAgInSc2d1	gruzínský
Superpoháru	superpohár	k1gInSc2	superpohár
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Soupiska	soupiska	k1gFnSc1	soupiska
==	==	k?	==
</s>
</p>
<p>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
klubu	klub	k1gInSc2	klub
na	na	k7c4	na
transfermarkt	transfermarkt	k1gInSc4	transfermarkt
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
klubu	klub	k1gInSc2	klub
na	na	k7c6	na
Weltfussballarchiv	Weltfussballarchiva	k1gFnPc2	Weltfussballarchiva
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
