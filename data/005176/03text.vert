<s>
Panthalassa	Panthalassa	k1gFnSc1	Panthalassa
(	(	kIx(	(
<g/>
řecký	řecký	k2eAgInSc1d1	řecký
název	název	k1gInSc1	název
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
moře	moře	k1gNnSc4	moře
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
původního	původní	k2eAgInSc2d1	původní
praoceánu	praoceán	k1gInSc2	praoceán
obklopujícího	obklopující	k2eAgInSc2d1	obklopující
prakontinent	prakontinent	k1gInSc1	prakontinent
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
Pangea	Pangea	k1gFnSc1	Pangea
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
paleozoika	paleozoikum	k1gNnSc2	paleozoikum
a	a	k8xC	a
brzkého	brzký	k2eAgNnSc2d1	brzké
mezozoika	mezozoikum	k1gNnSc2	mezozoikum
<g/>
.	.	kIx.	.
</s>
<s>
Praoceán	Praoceán	k1gMnSc1	Praoceán
se	se	k3xPyFc4	se
skládal	skládat	k5eAaImAgMnS	skládat
z	z	k7c2	z
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
z	z	k7c2	z
oceánu	oceán	k1gInSc2	oceán
Tethys	Tethysa	k1gFnPc2	Tethysa
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dalších	další	k2eAgInPc2d1	další
posunů	posun	k1gInPc2	posun
kontinentů	kontinent	k1gInPc2	kontinent
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
a	a	k8xC	a
Severního	severní	k2eAgInSc2d1	severní
ledového	ledový	k2eAgInSc2d1	ledový
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Panthalassa	Panthalassa	k1gFnSc1	Panthalassa
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
označuje	označovat	k5eAaImIp3nS	označovat
za	za	k7c4	za
předchůdce	předchůdce	k1gMnSc4	předchůdce
Pacifiku	Pacifik	k1gInSc2	Pacifik
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
praoceán	praoceán	k1gMnSc1	praoceán
nacházel	nacházet	k5eAaImAgMnS	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
velké	velký	k2eAgNnSc1d1	velké
sjednocení	sjednocení	k1gNnSc1	sjednocení
kontinentů	kontinent	k1gInPc2	kontinent
nastane	nastat	k5eAaPmIp3nS	nastat
asi	asi	k9	asi
za	za	k7c4	za
150-250	[number]	k4	150-250
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
