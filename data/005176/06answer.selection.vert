<s>
Panthalassa	Panthalassa	k1gFnSc1	Panthalassa
(	(	kIx(	(
<g/>
řecký	řecký	k2eAgInSc1d1	řecký
název	název	k1gInSc1	název
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
moře	moře	k1gNnSc4	moře
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
původního	původní	k2eAgInSc2d1	původní
praoceánu	praoceán	k1gInSc2	praoceán
obklopujícího	obklopující	k2eAgInSc2d1	obklopující
prakontinent	prakontinent	k1gInSc1	prakontinent
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
Pangea	Pangea	k1gFnSc1	Pangea
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
paleozoika	paleozoikum	k1gNnSc2	paleozoikum
a	a	k8xC	a
brzkého	brzký	k2eAgNnSc2d1	brzké
mezozoika	mezozoikum	k1gNnSc2	mezozoikum
<g/>
.	.	kIx.	.
</s>
