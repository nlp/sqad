<p>
<s>
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Alexandrovič	Alexandrovič	k1gMnSc1	Alexandrovič
Berďajev	Berďajev	k1gMnSc1	Berďajev
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
Н	Н	k?	Н
А	А	k?	А
Б	Б	k?	Б
<g/>
,	,	kIx,	,
přepisován	přepisován	k2eAgInSc1d1	přepisován
také	také	k9	také
jako	jako	k9	jako
Berdyaev	Berdyaev	k1gFnSc1	Berdyaev
<g/>
,	,	kIx,	,
Berdjajew	Berdjajew	k1gMnSc1	Berdjajew
nebo	nebo	k8xC	nebo
Berdiaeff	Berdiaeff	k1gMnSc1	Berdiaeff
<g/>
;	;	kIx,	;
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
jul	jul	k?	jul
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1874	[number]	k4	1874
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Kyjev	Kyjev	k1gInSc1	Kyjev
–	–	k?	–
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
Clamart	Clamart	k1gInSc1	Clamart
u	u	k7c2	u
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
ruský	ruský	k2eAgMnSc1d1	ruský
křesťanský	křesťanský	k2eAgMnSc1d1	křesťanský
filosof	filosof	k1gMnSc1	filosof
a	a	k8xC	a
publicista	publicista	k1gMnSc1	publicista
<g/>
,	,	kIx,	,
blízký	blízký	k2eAgInSc4d1	blízký
personalismu	personalismus	k1gInSc3	personalismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Rod	rod	k1gInSc1	rod
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgNnSc2	který
Berďajev	Berďajev	k1gMnSc1	Berďajev
pocházel	pocházet	k5eAaImAgMnS	pocházet
<g/>
,	,	kIx,	,
zaujímal	zaujímat	k5eAaImAgMnS	zaujímat
v	v	k7c6	v
carském	carský	k2eAgNnSc6d1	carské
Rusku	Rusko	k1gNnSc6	Rusko
dosti	dosti	k6eAd1	dosti
významné	významný	k2eAgNnSc4d1	významné
postavení	postavení	k1gNnSc4	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Berďajevův	Berďajevův	k2eAgMnSc1d1	Berďajevův
praděd	praděd	k1gMnSc1	praděd
byl	být	k5eAaImAgMnS	být
novorossijským	novorossijský	k2eAgMnSc7d1	novorossijský
generálním	generální	k2eAgMnSc7d1	generální
gubernátorem	gubernátor	k1gMnSc7	gubernátor
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
děd	děd	k1gMnSc1	děd
byl	být	k5eAaImAgMnS	být
atamanem	ataman	k1gMnSc7	ataman
"	"	kIx"	"
<g/>
Vojska	vojsko	k1gNnSc2	vojsko
donského	donský	k2eAgNnSc2d1	donský
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterýžto	kterýžto	k?	kterýžto
titul	titul	k1gInSc1	titul
měl	mít	k5eAaImAgInS	mít
původ	původ	k1gInSc4	původ
v	v	k7c6	v
mongolské	mongolský	k2eAgFnSc6d1	mongolská
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
kozácké	kozácký	k2eAgFnSc6d1	kozácká
hierarchii	hierarchie	k1gFnSc6	hierarchie
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
Nikolaje	Nikolaj	k1gMnSc4	Nikolaj
byl	být	k5eAaImAgMnS	být
důstojník	důstojník	k1gMnSc1	důstojník
kavalerie	kavalerie	k1gFnSc2	kavalerie
<g/>
,	,	kIx,	,
tak	tak	k9	tak
jako	jako	k9	jako
jeho	jeho	k3xOp3gMnPc1	jeho
předci	předek	k1gMnPc1	předek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
odstavci	odstavec	k1gInSc6	odstavec
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
nastíníme	nastínit	k5eAaPmIp1nP	nastínit
nejenom	nejenom	k6eAd1	nejenom
Berďajevův	Berďajevův	k2eAgInSc4d1	Berďajevův
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
rodinu	rodina	k1gFnSc4	rodina
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ukáže	ukázat	k5eAaPmIp3nS	ukázat
<g/>
,	,	kIx,	,
jaký	jaký	k3yIgInSc4	jaký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
cítění	cítění	k1gNnSc4	cítění
a	a	k8xC	a
přemýšlení	přemýšlení	k1gNnSc4	přemýšlení
měla	mít	k5eAaImAgFnS	mít
vojenská	vojenský	k2eAgFnSc1d1	vojenská
tradice	tradice	k1gFnSc1	tradice
jeho	jeho	k3xOp3gFnSc2	jeho
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
rodu	rod	k1gInSc2	rod
byla	být	k5eAaImAgFnS	být
přirozeně	přirozeně	k6eAd1	přirozeně
i	i	k9	i
Berďajevova	Berďajevův	k2eAgFnSc1d1	Berďajevova
matka	matka	k1gFnSc1	matka
<g/>
.	.	kIx.	.
</s>
<s>
Pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Kudaševových	Kudaševová	k1gFnPc2	Kudaševová
a	a	k8xC	a
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
francouzského	francouzský	k2eAgInSc2d1	francouzský
rodu	rod	k1gInSc2	rod
Choisel	Choisela	k1gFnPc2	Choisela
<g/>
,	,	kIx,	,
považovala	považovat	k5eAaImAgFnS	považovat
se	se	k3xPyFc4	se
taktéž	taktéž	k?	taktéž
za	za	k7c4	za
Francouzku	Francouzka	k1gFnSc4	Francouzka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
píše	psát	k5eAaImIp3nS	psát
Berďajev	Berďajev	k1gFnSc1	Berďajev
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
autobiografii	autobiografie	k1gFnSc6	autobiografie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
nenaučila	naučit	k5eNaPmAgFnS	naučit
používat	používat	k5eAaImF	používat
spisovnou	spisovný	k2eAgFnSc4d1	spisovná
ruštinu	ruština	k1gFnSc4	ruština
<g/>
.	.	kIx.	.
</s>
<s>
Veškeré	veškerý	k3xTgInPc4	veškerý
dopisy	dopis	k1gInPc4	dopis
psala	psát	k5eAaImAgFnS	psát
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
nejdříve	dříve	k6eAd3	dříve
kadetní	kadetní	k2eAgFnSc4d1	kadetní
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
měl	mít	k5eAaImAgInS	mít
možnost	možnost	k1gFnSc1	možnost
ji	on	k3xPp3gFnSc4	on
kdykoli	kdykoli	k6eAd1	kdykoli
opustit	opustit	k5eAaPmF	opustit
a	a	k8xC	a
jít	jít	k5eAaImF	jít
studovat	studovat	k5eAaImF	studovat
na	na	k7c4	na
pážecí	pážecí	k2eAgFnSc4d1	pážecí
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
určenou	určený	k2eAgFnSc7d1	určená
pro	pro	k7c4	pro
potomky	potomek	k1gMnPc4	potomek
významných	významný	k2eAgFnPc2d1	významná
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
Berďajev	Berďajev	k1gMnSc1	Berďajev
kadetku	kadetka	k1gFnSc4	kadetka
opustil	opustit	k5eAaPmAgMnS	opustit
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
připravovat	připravovat	k5eAaImF	připravovat
na	na	k7c4	na
maturitní	maturitní	k2eAgFnPc4d1	maturitní
zkoušky	zkouška	k1gFnPc4	zkouška
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterých	který	k3yIgFnPc6	který
chtěl	chtít	k5eAaImAgMnS	chtít
nastoupit	nastoupit	k5eAaPmF	nastoupit
na	na	k7c4	na
vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1894	[number]	k4	1894
ukončil	ukončit	k5eAaPmAgInS	ukončit
středoškolská	středoškolský	k2eAgNnPc4d1	středoškolské
studia	studio	k1gNnPc4	studio
maturitou	maturita	k1gFnSc7	maturita
a	a	k8xC	a
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
studiem	studio	k1gNnSc7	studio
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
studovat	studovat	k5eAaImF	studovat
Kyjevskou	kyjevský	k2eAgFnSc4d1	Kyjevská
přírodovědeckou	přírodovědecký	k2eAgFnSc4d1	Přírodovědecká
fakultu	fakulta	k1gFnSc4	fakulta
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
na	na	k7c4	na
fakultu	fakulta	k1gFnSc4	fakulta
právnickou	právnický	k2eAgFnSc4d1	právnická
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
Berďajevův	Berďajevův	k2eAgInSc1d1	Berďajevův
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
rys	rys	k1gInSc1	rys
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
je	být	k5eAaImIp3nS	být
neustálá	neustálý	k2eAgFnSc1d1	neustálá
revolta	revolta	k1gFnSc1	revolta
vůči	vůči	k7c3	vůči
zavedeným	zavedený	k2eAgInPc3d1	zavedený
pořádkům	pořádek	k1gInPc3	pořádek
a	a	k8xC	a
vůči	vůči	k7c3	vůči
nedotknutelným	nedotknutelným	k?	nedotknutelným
autoritám	autorita	k1gFnPc3	autorita
<g/>
.	.	kIx.	.
</s>
<s>
Berďajev	Berďajet	k5eAaPmDgInS	Berďajet
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
zúčastňoval	zúčastňovat	k5eAaImAgInS	zúčastňovat
schůzí	schůze	k1gFnSc7	schůze
marxistů	marxista	k1gMnPc2	marxista
<g/>
.	.	kIx.	.
</s>
<s>
Vyhnanství	vyhnanství	k1gNnSc1	vyhnanství
ve	v	k7c6	v
Vologodské	Vologodský	k2eAgFnSc6d1	Vologodská
gubernii	gubernie	k1gFnSc6	gubernie
v	v	k7c6	v
centrálním	centrální	k2eAgNnSc6d1	centrální
Rusku	Rusko	k1gNnSc6	Rusko
na	na	k7c6	na
sebe	sebe	k3xPyFc4	sebe
nenechalo	nechat	k5eNaPmAgNnS	nechat
dlouho	dlouho	k6eAd1	dlouho
čekat	čekat	k5eAaImF	čekat
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
pro	pro	k7c4	pro
Berďajevovu	Berďajevův	k2eAgFnSc4d1	Berďajevova
filozofii	filozofie	k1gFnSc4	filozofie
mělo	mít	k5eAaImAgNnS	mít
vyhnanství	vyhnanství	k1gNnSc4	vyhnanství
zásadní	zásadní	k2eAgInSc1d1	zásadní
význam	význam	k1gInSc1	význam
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
k	k	k7c3	k
náboženskému	náboženský	k2eAgInSc3d1	náboženský
obratu	obrat	k1gInSc3	obrat
a	a	k8xC	a
začíná	začínat	k5eAaImIp3nS	začínat
"	"	kIx"	"
<g/>
bohohledačské	bohohledačský	k2eAgNnSc4d1	bohohledačský
<g/>
"	"	kIx"	"
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
až	až	k9	až
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
neskončí	skončit	k5eNaPmIp3nS	skončit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
se	se	k3xPyFc4	se
Berďajev	Berďajev	k1gFnSc4	Berďajev
oženil	oženit	k5eAaPmAgMnS	oženit
a	a	k8xC	a
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
do	do	k7c2	do
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
dění	dění	k1gNnSc2	dění
nejen	nejen	k6eAd1	nejen
kulturního	kulturní	k2eAgNnSc2d1	kulturní
a	a	k8xC	a
politického	politický	k2eAgNnSc2d1	politické
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
revolučního	revoluční	k2eAgNnSc2d1	revoluční
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgInSc6	ten
se	se	k3xPyFc4	se
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
před	před	k7c7	před
Zimním	zimní	k2eAgInSc7d1	zimní
palácem	palác	k1gInSc7	palác
krvavě	krvavě	k6eAd1	krvavě
potlačena	potlačen	k2eAgFnSc1d1	potlačena
demonstrace	demonstrace	k1gFnSc1	demonstrace
předznamenávající	předznamenávající	k2eAgFnSc4d1	předznamenávající
revoluci	revoluce	k1gFnSc4	revoluce
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
značně	značně	k6eAd1	značně
napjaté	napjatý	k2eAgFnSc3d1	napjatá
situaci	situace	k1gFnSc3	situace
se	se	k3xPyFc4	se
car	car	k1gMnSc1	car
ještě	ještě	k6eAd1	ještě
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
ustavit	ustavit	k5eAaPmF	ustavit
parlament	parlament	k1gInSc1	parlament
<g/>
;	;	kIx,	;
tedy	tedy	k9	tedy
Dumu	duma	k1gFnSc4	duma
(	(	kIx(	(
<g/>
od	od	k7c2	od
ruského	ruský	k2eAgMnSc2d1	ruský
д	д	k?	д
-	-	kIx~	-
myslet	myslet	k5eAaImF	myslet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dalším	další	k2eAgInSc6d1	další
roce	rok	k1gInSc6	rok
dochází	docházet	k5eAaImIp3nS	docházet
ještě	ještě	k6eAd1	ještě
k	k	k7c3	k
úpravám	úprava	k1gFnPc3	úprava
ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
jejímu	její	k3xOp3gMnSc3	její
dalšímu	další	k2eAgMnSc3d1	další
"	"	kIx"	"
<g/>
zdemokratičtění	zdemokratičtění	k1gNnSc3	zdemokratičtění
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Snahy	snaha	k1gFnPc1	snaha
posledního	poslední	k2eAgMnSc2d1	poslední
cara	car	k1gMnSc2	car
"	"	kIx"	"
<g/>
Vší	všecek	k3xTgFnSc2	všecek
Rusi	Rus	k1gFnSc2	Rus
<g/>
"	"	kIx"	"
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
byly	být	k5eAaImAgFnP	být
"	"	kIx"	"
<g/>
labutí	labuť	k1gFnPc2	labuť
písní	píseň	k1gFnPc2	píseň
<g/>
"	"	kIx"	"
starých	starý	k2eAgInPc2d1	starý
časů	čas	k1gInPc2	čas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Filozofický	filozofický	k2eAgInSc1d1	filozofický
radikalismus	radikalismus	k1gInSc1	radikalismus
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
poblouzněním	poblouznění	k1gNnSc7	poblouznění
části	část	k1gFnSc2	část
ruské	ruský	k2eAgFnSc2d1	ruská
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
politická	politický	k2eAgFnSc1d1	politická
liberalizace	liberalizace	k1gFnSc1	liberalizace
poměrů	poměr	k1gInPc2	poměr
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
neutěšené	utěšený	k2eNgInPc1d1	neutěšený
poměry	poměr	k1gInPc1	poměr
probíhající	probíhající	k2eAgInPc1d1	probíhající
první	první	k4xOgFnPc1	první
světové	světový	k2eAgFnPc1d1	světová
války	válka	k1gFnPc1	válka
s	s	k7c7	s
negativní	negativní	k2eAgFnSc7d1	negativní
bolševickou	bolševický	k2eAgFnSc7d1	bolševická
agitací	agitace	k1gFnSc7	agitace
a	a	k8xC	a
rozvratem	rozvrat	k1gInSc7	rozvrat
armády	armáda	k1gFnSc2	armáda
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
únorovému	únorový	k2eAgInSc3d1	únorový
politickému	politický	k2eAgInSc3d1	politický
převratu	převrat	k1gInSc3	převrat
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
a	a	k8xC	a
nástupu	nástup	k1gInSc6	nástup
slabé	slabý	k2eAgFnSc2d1	slabá
a	a	k8xC	a
desorientované	desorientovaný	k2eAgFnSc2d1	desorientovaná
vládní	vládní	k2eAgFnSc2d1	vládní
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
vyústily	vyústit	k5eAaPmAgFnP	vyústit
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
<g/>
/	/	kIx~	/
<g/>
listopadu	listopad	k1gInSc2	listopad
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
v	v	k7c4	v
puč	puč	k1gInSc4	puč
a	a	k8xC	a
převzetí	převzetí	k1gNnSc4	převzetí
moci	moct	k5eAaImF	moct
bolševiky	bolševik	k1gMnPc7	bolševik
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
dramatický	dramatický	k2eAgInSc1d1	dramatický
převrat	převrat	k1gInSc1	převrat
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
následován	následovat	k5eAaImNgInS	následovat
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
nejkrutějších	krutý	k2eAgFnPc2d3	nejkrutější
a	a	k8xC	a
nejdelších	dlouhý	k2eAgFnPc2d3	nejdelší
občanských	občanský	k2eAgFnPc2d1	občanská
válek	válka	k1gFnPc2	válka
v	v	k7c6	v
celých	celý	k2eAgFnPc6d1	celá
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
,	,	kIx,	,
trvající	trvající	k2eAgFnSc1d1	trvající
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Radikální	radikální	k2eAgFnSc1d1	radikální
změna	změna	k1gFnSc1	změna
<g/>
,	,	kIx,	,
pauperizace	pauperizace	k1gFnSc1	pauperizace
a	a	k8xC	a
krutost	krutost	k1gFnSc1	krutost
společenských	společenský	k2eAgInPc2d1	společenský
poměrů	poměr	k1gInPc2	poměr
dopadla	dopadnout	k5eAaPmAgFnS	dopadnout
tvrdě	tvrdě	k6eAd1	tvrdě
i	i	k9	i
na	na	k7c4	na
Berďajeva	Berďajevo	k1gNnPc4	Berďajevo
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
byl	být	k5eAaImAgMnS	být
dvakrát	dvakrát	k6eAd1	dvakrát
vězněn	věznit	k5eAaImNgMnS	věznit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
dokázal	dokázat	k5eAaPmAgMnS	dokázat
založit	založit	k5eAaPmF	založit
"	"	kIx"	"
<g/>
Svobodnou	svobodný	k2eAgFnSc4d1	svobodná
akademii	akademie	k1gFnSc4	akademie
duchovní	duchovní	k2eAgFnSc2d1	duchovní
kultury	kultura	k1gFnSc2	kultura
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
В	В	k?	В
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
byl	být	k5eAaImAgMnS	být
donucen	donutit	k5eAaPmNgMnS	donutit
opustit	opustit	k5eAaPmF	opustit
nově	nově	k6eAd1	nově
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
žil	žít	k5eAaImAgMnS	žít
Berďajev	Berďajev	k1gFnPc7	Berďajev
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
organizace	organizace	k1gFnSc2	organizace
YMCA	YMCA	kA	YMCA
založil	založit	k5eAaPmAgMnS	založit
"	"	kIx"	"
<g/>
Náboženskou	náboženský	k2eAgFnSc4d1	náboženská
akademii	akademie	k1gFnSc4	akademie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
ruských	ruský	k2eAgMnPc2d1	ruský
emigrantů	emigrant	k1gMnPc2	emigrant
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
se	se	k3xPyFc4	se
Berďajev	Berďajev	k1gFnSc4	Berďajev
připojil	připojit	k5eAaPmAgMnS	připojit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgMnS	být
filozof	filozof	k1gMnSc1	filozof
neobyčejně	obyčejně	k6eNd1	obyčejně
aktivní	aktivní	k2eAgMnSc1d1	aktivní
a	a	k8xC	a
obnovil	obnovit	k5eAaPmAgMnS	obnovit
vydávání	vydávání	k1gNnSc4	vydávání
časopisu	časopis	k1gInSc2	časopis
"	"	kIx"	"
<g/>
П	П	k?	П
<g/>
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Cesta	cesta	k1gFnSc1	cesta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vycházel	vycházet	k5eAaImAgMnS	vycházet
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
let	léto	k1gNnPc2	léto
1926	[number]	k4	1926
až	až	k6eAd1	až
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
Berďajevovi	Berďajeva	k1gMnSc3	Berďajeva
relativní	relativní	k2eAgInSc4d1	relativní
klid	klid	k1gInSc4	klid
na	na	k7c4	na
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
většina	většina	k1gFnSc1	většina
jeho	on	k3xPp3gNnSc2	on
filosofického	filosofický	k2eAgNnSc2d1	filosofické
díla	dílo	k1gNnSc2	dílo
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
právě	právě	k6eAd1	právě
tam	tam	k6eAd1	tam
<g/>
.	.	kIx.	.
</s>
<s>
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Alexandrovič	Alexandrovič	k1gMnSc1	Alexandrovič
Berďajev	Berďajev	k1gMnSc1	Berďajev
už	už	k9	už
se	se	k3xPyFc4	se
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
rodné	rodný	k2eAgFnSc2d1	rodná
země	zem	k1gFnSc2	zem
nikdy	nikdy	k6eAd1	nikdy
nepodíval	podívat	k5eNaImAgMnS	podívat
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
od	od	k7c2	od
Ruska	Rusko	k1gNnSc2	Rusko
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
myšlenkách	myšlenka	k1gFnPc6	myšlenka
nikdy	nikdy	k6eAd1	nikdy
neodpoutal	odpoutat	k5eNaPmAgInS	odpoutat
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
v	v	k7c6	v
Clamart	Clamarta	k1gFnPc2	Clamarta
u	u	k7c2	u
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Berďajevův	Berďajevův	k2eAgInSc4d1	Berďajevův
filozofický	filozofický	k2eAgInSc4d1	filozofický
konstrukt	konstrukt	k1gInSc4	konstrukt
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Marxismus	marxismus	k1gInSc4	marxismus
===	===	k?	===
</s>
</p>
<p>
<s>
Berďajev	Berďajet	k5eAaPmDgInS	Berďajet
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
myšlenkami	myšlenka	k1gFnPc7	myšlenka
ruského	ruský	k2eAgInSc2d1	ruský
socialismu	socialismus	k1gInSc2	socialismus
už	už	k6eAd1	už
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
až	až	k9	až
při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
začal	začít	k5eAaPmAgMnS	začít
nad	nad	k7c7	nad
socialismem	socialismus	k1gInSc7	socialismus
přemýšlet	přemýšlet	k5eAaImF	přemýšlet
systematicky	systematicky	k6eAd1	systematicky
<g/>
.	.	kIx.	.
</s>
<s>
Zásadním	zásadní	k2eAgMnSc7d1	zásadní
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
pro	pro	k7c4	pro
Berďajeva	Berďajev	k1gMnSc4	Berďajev
rok	rok	k1gInSc4	rok
1894	[number]	k4	1894
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
v	v	k7c6	v
Kyjevě	Kyjev	k1gInSc6	Kyjev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
páté	pátý	k4xOgFnSc6	pátý
kapitole	kapitola	k1gFnSc6	kapitola
svého	svůj	k3xOyFgInSc2	svůj
životopisu	životopis	k1gInSc2	životopis
s	s	k7c7	s
názvem	název	k1gInSc7	název
Obrácení	obrácení	k1gNnSc2	obrácení
se	se	k3xPyFc4	se
k	k	k7c3	k
socialismu	socialismus	k1gInSc3	socialismus
<g/>
.	.	kIx.	.
</s>
<s>
Svět	svět	k1gInSc1	svět
revoluční	revoluční	k2eAgInSc1d1	revoluční
<g/>
.	.	kIx.	.
</s>
<s>
Marxismus	marxismus	k1gInSc1	marxismus
a	a	k8xC	a
idealismus	idealismus	k1gInSc1	idealismus
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
Berďajev	Berďajev	k1gFnSc1	Berďajev
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
o	o	k7c6	o
D.J.	D.J.	k1gMnSc6	D.J.
Longvinském	Longvinský	k2eAgMnSc6d1	Longvinský
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
Berďajevovým	Berďajevův	k2eAgMnSc7d1	Berďajevův
spolužákem	spolužák	k1gMnSc7	spolužák
na	na	k7c6	na
přírodovědecké	přírodovědecký	k2eAgFnSc6d1	Přírodovědecká
fakultě	fakulta	k1gFnSc6	fakulta
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
filozofovým	filozofův	k2eAgMnSc7d1	filozofův
přítelem	přítel	k1gMnSc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgMnS	být
u	u	k7c2	u
samotářského	samotářský	k2eAgNnSc2d1	samotářské
Berďajeva	Berďajevo	k1gNnSc2	Berďajevo
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
sám	sám	k3xTgMnSc1	sám
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
jev	jev	k1gInSc1	jev
zcela	zcela	k6eAd1	zcela
neobvyklý	obvyklý	k2eNgInSc1d1	neobvyklý
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
skrze	skrze	k?	skrze
svého	svůj	k3xOyFgMnSc2	svůj
přítele	přítel	k1gMnSc2	přítel
se	se	k3xPyFc4	se
Berďajev	Berďajev	k1gMnSc1	Berďajev
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
okruhu	okruh	k1gInSc2	okruh
studentů	student	k1gMnPc2	student
sympatizujících	sympatizující	k2eAgMnPc2d1	sympatizující
s	s	k7c7	s
marxismem	marxismus	k1gInSc7	marxismus
<g/>
.	.	kIx.	.
</s>
<s>
Berďajev	Berďajet	k5eAaPmDgInS	Berďajet
viděl	vidět	k5eAaImAgMnS	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
ruský	ruský	k2eAgInSc1d1	ruský
socialismus	socialismus	k1gInSc1	socialismus
je	být	k5eAaImIp3nS	být
úplné	úplný	k2eAgNnSc4d1	úplné
novum	novum	k1gNnSc4	novum
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
definovat	definovat	k5eAaBmF	definovat
svůj	svůj	k3xOyFgInSc4	svůj
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
hnutí	hnutí	k1gNnSc3	hnutí
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Marxismus	marxismus	k1gInSc1	marxismus
označoval	označovat	k5eAaImAgInS	označovat
dokonale	dokonale	k6eAd1	dokonale
novou	nový	k2eAgFnSc4d1	nová
formaci	formace	k1gFnSc4	formace
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
krizí	krize	k1gFnSc7	krize
ruské	ruský	k2eAgFnSc2d1	ruská
inteligence	inteligence	k1gFnSc2	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
marxistické	marxistický	k2eAgNnSc1d1	marxistické
hnutí	hnutí	k1gNnSc1	hnutí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
mnohem	mnohem	k6eAd1	mnohem
vyšší	vysoký	k2eAgFnSc4d2	vyšší
kulturní	kulturní	k2eAgFnSc4d1	kulturní
úroveň	úroveň	k1gFnSc4	úroveň
než	než	k8xS	než
ostatní	ostatní	k2eAgNnSc4d1	ostatní
hnutí	hnutí	k1gNnSc4	hnutí
revoluční	revoluční	k2eAgFnSc2d1	revoluční
inteligence	inteligence	k1gFnSc2	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
typ	typ	k1gInSc1	typ
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
podobný	podobný	k2eAgInSc1d1	podobný
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgMnSc2	jenž
později	pozdě	k6eAd2	pozdě
vyšel	vyjít	k5eAaPmAgInS	vyjít
bolševismus	bolševismus	k1gInSc1	bolševismus
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
kritickým	kritický	k2eAgMnSc7d1	kritický
marxistou	marxista	k1gMnSc7	marxista
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
mi	já	k3xPp1nSc3	já
dalo	dát	k5eAaPmAgNnS	dát
možnost	možnost	k1gFnSc4	možnost
zůstat	zůstat	k5eAaPmF	zůstat
idealistou	idealista	k1gMnSc7	idealista
ve	v	k7c6	v
filozofii	filozofie	k1gFnSc6	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
staré	starý	k2eAgMnPc4d1	starý
ruské	ruský	k2eAgMnPc4d1	ruský
revolucionáře	revolucionář	k1gMnPc4	revolucionář
byla	být	k5eAaImAgFnS	být
revoluce	revoluce	k1gFnSc2	revoluce
náboženstvím	náboženství	k1gNnSc7	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
mne	já	k3xPp1nSc4	já
revoluce	revoluce	k1gFnPc1	revoluce
náboženstvím	náboženství	k1gNnSc7	náboženství
nebyla	být	k5eNaImAgFnS	být
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Podle	podle	k7c2	podle
dalších	další	k2eAgInPc2d1	další
náznaků	náznak	k1gInPc2	náznak
v	v	k7c6	v
autobiografii	autobiografie	k1gFnSc6	autobiografie
můžu	můžu	k?	můžu
konstatovat	konstatovat	k5eAaBmF	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Berďajevovi	Berďajevův	k2eAgMnPc1d1	Berďajevův
se	se	k3xPyFc4	se
marxismus	marxismus	k1gInSc1	marxismus
jevil	jevit	k5eAaImAgInS	jevit
jako	jako	k9	jako
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
(	(	kIx(	(
<g/>
celosvětový	celosvětový	k2eAgInSc1d1	celosvětový
<g/>
)	)	kIx)	)
fenomén	fenomén	k1gInSc1	fenomén
<g/>
.	.	kIx.	.
</s>
<s>
Marxismus	marxismus	k1gInSc1	marxismus
chápal	chápat	k5eAaImAgInS	chápat
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
raném	raný	k2eAgNnSc6d1	rané
období	období	k1gNnSc6	období
jako	jako	k8xC	jako
proud	proud	k1gInSc1	proud
"	"	kIx"	"
<g/>
poevropšťující	poevropšťující	k2eAgMnSc1d1	poevropšťující
<g/>
"	"	kIx"	"
myšlení	myšlení	k1gNnSc1	myšlení
ruské	ruský	k2eAgFnSc2d1	ruská
inteligence	inteligence	k1gFnSc2	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jím	jíst	k5eAaImIp1nS	jíst
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
"	"	kIx"	"
<g/>
poblázněn	poblázněn	k2eAgMnSc1d1	poblázněn
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jako	jako	k9	jako
většina	většina	k1gFnSc1	většina
ruské	ruský	k2eAgFnSc2d1	ruská
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
evropské	evropský	k2eAgFnSc2d1	Evropská
inteligence	inteligence	k1gFnSc2	inteligence
přelomu	přelom	k1gInSc2	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
již	již	k9	již
za	za	k7c4	za
několik	několik	k4yIc4	několik
málo	málo	k4c4	málo
let	léto	k1gNnPc2	léto
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
Berďajevův	Berďajevův	k2eAgInSc1d1	Berďajevův
přístup	přístup	k1gInSc1	přístup
kritičtější	kritický	k2eAgInSc1d2	kritičtější
<g/>
.	.	kIx.	.
</s>
<s>
Ruskou	ruský	k2eAgFnSc4d1	ruská
inteligenci	inteligence	k1gFnSc4	inteligence
ještě	ještě	k6eAd1	ještě
carské	carský	k2eAgFnSc2d1	carská
doby	doba	k1gFnSc2	doba
popisoval	popisovat	k5eAaImAgMnS	popisovat
Berďajev	Berďajev	k1gMnSc1	Berďajev
například	například	k6eAd1	například
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Inteligence	inteligence	k1gFnSc1	inteligence
je	být	k5eAaImIp3nS	být
sto	sto	k4xCgNnSc1	sto
uvěřit	uvěřit	k5eAaPmF	uvěřit
jakékoli	jakýkoli	k3yIgFnSc3	jakýkoli
filosofii	filosofie	k1gFnSc3	filosofie
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
podmínkou	podmínka	k1gFnSc7	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
obhajovat	obhajovat	k5eAaImF	obhajovat
její	její	k3xOp3gInPc4	její
sociální	sociální	k2eAgInPc4d1	sociální
ideály	ideál	k1gInPc4	ideál
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nekriticky	kriticky	k6eNd1	kriticky
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
každou	každý	k3xTgFnSc4	každý
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
sebehlubší	sebehlubšit	k5eAaPmIp3nS	sebehlubšit
a	a	k8xC	a
sebepravdivější	sebepravdivý	k2eAgFnSc3d2	sebepravdivější
filosofii	filosofie	k1gFnSc3	filosofie
jen	jen	k9	jen
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
považuje	považovat	k5eAaImIp3nS	považovat
svůj	svůj	k3xOyFgInSc4	svůj
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
těmto	tento	k3xDgFnPc3	tento
tradičním	tradiční	k2eAgFnPc3d1	tradiční
hodnotám	hodnota	k1gFnPc3	hodnota
a	a	k8xC	a
ideálům	ideál	k1gInPc3	ideál
za	za	k7c4	za
negativní	negativní	k2eAgInPc4d1	negativní
<g/>
,	,	kIx,	,
či	či	k8xC	či
jen	jen	k9	jen
kritický	kritický	k2eAgInSc1d1	kritický
<g/>
.	.	kIx.	.
</s>
<s>
Nepřátelství	nepřátelství	k1gNnPc1	nepřátelství
vůči	vůči	k7c3	vůči
idealistickým	idealistický	k2eAgInPc3d1	idealistický
a	a	k8xC	a
nábožensko-mystickým	náboženskoystický	k2eAgInPc3d1	nábožensko-mystický
směrům	směr	k1gInPc3	směr
a	a	k8xC	a
ignorování	ignorování	k1gNnSc3	ignorování
původní	původní	k2eAgFnSc2d1	původní
ruské	ruský	k2eAgFnSc2d1	ruská
filosofie	filosofie	k1gFnSc2	filosofie
plné	plný	k2eAgFnSc2d1	plná
tvůrčích	tvůrčí	k2eAgFnPc2d1	tvůrčí
schopností	schopnost	k1gFnPc2	schopnost
je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
"	"	kIx"	"
<g/>
katolické	katolický	k2eAgFnSc6d1	katolická
<g/>
"	"	kIx"	"
psychologii	psychologie	k1gFnSc6	psychologie
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecný	všeobecný	k2eAgInSc1d1	všeobecný
utilitarismus	utilitarismus	k1gInSc1	utilitarismus
v	v	k7c6	v
hodnocení	hodnocení	k1gNnSc6	hodnocení
čehokoli	cokoli	k3yInSc2	cokoli
a	a	k8xC	a
uctívání	uctívání	k1gNnSc3	uctívání
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
jednou	jednou	k6eAd1	jednou
rolnictva	rolnictvo	k1gNnSc2	rolnictvo
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
proletariátu	proletariát	k1gInSc2	proletariát
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
morálním	morální	k2eAgNnSc7d1	morální
dogmatem	dogma	k1gNnSc7	dogma
většiny	většina	k1gFnSc2	většina
inteligence	inteligence	k1gFnSc2	inteligence
<g/>
.	.	kIx.	.
...	...	k?	...
Ukázalo	ukázat	k5eAaPmAgNnS	ukázat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
falešnáláska	falešnáláska	k1gFnSc1	falešnáláska
k	k	k7c3	k
člověku	člověk	k1gMnSc3	člověk
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
lásku	láska	k1gFnSc4	láska
k	k	k7c3	k
Bohu	bůh	k1gMnSc3	bůh
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
láska	láska	k1gFnSc1	láska
k	k	k7c3	k
pravdě	pravda	k1gFnSc3	pravda
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ke	k	k7c3	k
kráse	krása	k1gFnSc3	krása
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
ke	k	k7c3	k
všem	všecek	k3xTgFnPc3	všecek
absolutním	absolutní	k2eAgFnPc3d1	absolutní
hodnotám	hodnota	k1gFnPc3	hodnota
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vyjádření	vyjádření	k1gNnSc4	vyjádření
lásky	láska	k1gFnSc2	láska
k	k	k7c3	k
božství	božství	k1gNnSc3	božství
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
V	v	k7c6	v
příspěvku	příspěvek	k1gInSc6	příspěvek
do	do	k7c2	do
sborníku	sborník	k1gInSc2	sborník
o	o	k7c6	o
ruské	ruský	k2eAgFnSc6d1	ruská
inteligenci	inteligence	k1gFnSc6	inteligence
s	s	k7c7	s
názvem	název	k1gInSc7	název
Milníky	milník	k1gInPc7	milník
se	se	k3xPyFc4	se
vyznal	vyznat	k5eAaPmAgInS	vyznat
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
poblouznění	poblouznění	k1gNnSc2	poblouznění
a	a	k8xC	a
kritikou	kritika	k1gFnSc7	kritika
sebe	sebe	k3xPyFc4	sebe
sama	sám	k3xTgFnSc1	sám
a	a	k8xC	a
svých	svůj	k3xOyFgMnPc2	svůj
současníků	současník	k1gMnPc2	současník
chtěl	chtít	k5eAaImAgMnS	chtít
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
revize	revize	k1gFnPc4	revize
ruského	ruský	k2eAgInSc2d1	ruský
filozofického	filozofický	k2eAgInSc2d1	filozofický
i	i	k8xC	i
politického	politický	k2eAgInSc2d1	politický
diskurzu	diskurz	k1gInSc2	diskurz
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Konzervatismus	konzervatismus	k1gInSc4	konzervatismus
a	a	k8xC	a
zkostnatělost	zkostnatělost	k1gFnSc4	zkostnatělost
v	v	k7c6	v
duševním	duševní	k2eAgInSc6d1	duševní
životě	život	k1gInSc6	život
se	se	k3xPyFc4	se
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
skloubily	skloubit	k5eAaPmAgInP	skloubit
se	s	k7c7	s
zalíbením	zalíbení	k1gNnSc7	zalíbení
v	v	k7c6	v
novinkách	novinka	k1gFnPc6	novinka
<g/>
,	,	kIx,	,
v	v	k7c6	v
nejnovějších	nový	k2eAgInPc6d3	nejnovější
evropských	evropský	k2eAgInPc6d1	evropský
proudech	proud	k1gInPc6	proud
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
nebyly	být	k5eNaImAgInP	být
vnímány	vnímat	k5eAaImNgInP	vnímat
v	v	k7c6	v
celé	celá	k1gFnSc6	celá
své	svůj	k3xOyFgFnSc6	svůj
hloubce	hloubka	k1gFnSc6	hloubka
<g/>
.	.	kIx.	.
</s>
<s>
Totéž	týž	k3xTgNnSc1	týž
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
říci	říct	k5eAaPmF	říct
i	i	k9	i
o	o	k7c6	o
oblasti	oblast	k1gFnSc6	oblast
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
očí	oko	k1gNnPc2	oko
bije	bít	k5eAaImIp3nS	bít
především	především	k9	především
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
vztah	vztah	k1gInSc1	vztah
k	k	k7c3	k
filosofii	filosofie	k1gFnSc3	filosofie
byl	být	k5eAaImAgInS	být
stejně	stejně	k6eAd1	stejně
nekulturní	kulturní	k2eNgInSc1d1	nekulturní
jako	jako	k8xC	jako
vztah	vztah	k1gInSc1	vztah
k	k	k7c3	k
ostatním	ostatní	k2eAgFnPc3d1	ostatní
duchovním	duchovní	k2eAgFnPc3d1	duchovní
hodnotám	hodnota	k1gFnPc3	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Filosofie	filosofie	k1gFnSc1	filosofie
ztratila	ztratit	k5eAaPmAgFnS	ztratit
samostatnost	samostatnost	k1gFnSc4	samostatnost
a	a	k8xC	a
utilitárně	utilitárně	k6eAd1	utilitárně
se	se	k3xPyFc4	se
podřídila	podřídit	k5eAaPmAgFnS	podřídit
společenským	společenský	k2eAgInPc3d1	společenský
cílům	cíl	k1gInPc3	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Výsadní	výsadní	k2eAgFnSc1d1	výsadní
<g/>
,	,	kIx,	,
despotická	despotický	k2eAgFnSc1d1	despotická
nadvláda	nadvláda	k1gFnSc1	nadvláda
utilitárně-morálních	utilitárněorální	k2eAgNnPc2d1	utilitárně-morální
kritérií	kritérion	k1gNnPc2	kritérion
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
absolutní	absolutní	k2eAgFnSc1d1	absolutní
nadvláda	nadvláda	k1gFnSc1	nadvláda
lásky	láska	k1gFnSc2	láska
k	k	k7c3	k
lidu	lid	k1gInSc3	lid
a	a	k8xC	a
proletariátu	proletariát	k1gInSc3	proletariát
<g/>
,	,	kIx,	,
zbožšťování	zbožšťování	k1gNnSc3	zbožšťování
lidu	lid	k1gInSc3	lid
a	a	k8xC	a
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
užitečných	užitečný	k2eAgMnPc2d1	užitečný
zájmů	zájem	k1gInPc2	zájem
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
jako	jako	k8xC	jako
útlak	útlak	k1gInSc4	útlak
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
politického	politický	k2eAgInSc2d1	politický
despotismu	despotismus	k1gInSc2	despotismus
-	-	kIx~	-
to	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
úroveň	úroveň	k1gFnSc1	úroveň
filosofické	filosofický	k2eAgFnSc2d1	filosofická
kultury	kultura	k1gFnSc2	kultura
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
velmi	velmi	k6eAd1	velmi
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
že	že	k8xS	že
filosofické	filosofický	k2eAgFnPc4d1	filosofická
znalosti	znalost	k1gFnPc4	znalost
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
filosofie	filosofie	k1gFnPc1	filosofie
byly	být	k5eAaImAgFnP	být
mezi	mezi	k7c7	mezi
naší	náš	k3xOp1gFnSc7	náš
inteligencí	inteligence	k1gFnSc7	inteligence
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
rozšířeny	rozšířit	k5eAaPmNgFnP	rozšířit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Marxismem	marxismus	k1gInSc7	marxismus
se	se	k3xPyFc4	se
filozof	filozof	k1gMnSc1	filozof
zabýval	zabývat	k5eAaImAgMnS	zabývat
i	i	k9	i
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Říše	říš	k1gFnSc2	říš
ducha	duch	k1gMnSc2	duch
a	a	k8xC	a
říše	říše	k1gFnSc1	říše
císařova	císařův	k2eAgFnSc1d1	císařova
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
pojednání	pojednání	k1gNnSc1	pojednání
bylo	být	k5eAaImAgNnS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
Berďajevových	Berďajevův	k2eAgNnPc2d1	Berďajevovo
posledních	poslední	k2eAgNnPc2d1	poslední
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc4	který
stačil	stačit	k5eAaBmAgMnS	stačit
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
napsat	napsat	k5eAaBmF	napsat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
nás	my	k3xPp1nPc4	my
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
o	o	k7c6	o
marxismu	marxismus	k1gInSc6	marxismus
zde	zde	k6eAd1	zde
může	moct	k5eAaImIp3nS	moct
Berďajev	Berďajev	k1gFnSc4	Berďajev
psát	psát	k5eAaImF	psát
vyzbrojen	vyzbrojit	k5eAaPmNgMnS	vyzbrojit
zkušeností	zkušenost	k1gFnSc7	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Říše	říše	k1gFnSc1	říše
ducha	duch	k1gMnSc2	duch
a	a	k8xC	a
říše	říše	k1gFnSc1	říše
císařova	císařův	k2eAgFnSc1d1	císařova
vyšla	vyjít	k5eAaPmAgFnS	vyjít
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Berďajev	Berďajev	k1gMnSc7	Berďajev
vyzbrojen	vyzbrojit	k5eAaPmNgMnS	vyzbrojit
nejen	nejen	k6eAd1	nejen
zkušeností	zkušenost	k1gFnSc7	zkušenost
s	s	k7c7	s
marxistickou	marxistický	k2eAgFnSc7d1	marxistická
filozofií	filozofie	k1gFnSc7	filozofie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
s	s	k7c7	s
důsledky	důsledek	k1gInPc7	důsledek
aplikace	aplikace	k1gFnSc2	aplikace
takové	takový	k3xDgFnSc2	takový
teorie	teorie	k1gFnSc2	teorie
v	v	k7c6	v
sovětském	sovětský	k2eAgNnSc6d1	sovětské
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduji	odhadovat	k5eAaImIp1nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
Berďajevova	Berďajevův	k2eAgNnSc2d1	Berďajevovo
ražení	ražení	k1gNnSc2	ražení
měl	mít	k5eAaImAgInS	mít
enormní	enormní	k2eAgInSc1d1	enormní
zájem	zájem	k1gInSc1	zájem
a	a	k8xC	a
unikátní	unikátní	k2eAgFnSc1d1	unikátní
možnost	možnost	k1gFnSc1	možnost
sledovat	sledovat	k5eAaImF	sledovat
důsledky	důsledek	k1gInPc4	důsledek
použití	použití	k1gNnSc2	použití
deformovaného	deformovaný	k2eAgInSc2d1	deformovaný
marxismu	marxismus	k1gInSc2	marxismus
a	a	k8xC	a
hodnotit	hodnotit	k5eAaImF	hodnotit
jej	on	k3xPp3gMnSc4	on
z	z	k7c2	z
vnějšku	vnějšek	k1gInSc2	vnějšek
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
z	z	k7c2	z
exilu	exil	k1gInSc2	exil
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kapitole	kapitola	k1gFnSc6	kapitola
příznačně	příznačně	k6eAd1	příznačně
nazvané	nazvaný	k2eAgInPc1d1	nazvaný
Protiklady	protiklad	k1gInPc1	protiklad
marxismu	marxismus	k1gInSc2	marxismus
se	se	k3xPyFc4	se
autor	autor	k1gMnSc1	autor
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
důvody	důvod	k1gInPc4	důvod
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
zesilování	zesilování	k1gNnSc3	zesilování
pozice	pozice	k1gFnSc2	pozice
marxistické	marxistický	k2eAgFnSc2d1	marxistická
ideologie	ideologie	k1gFnSc2	ideologie
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Příčinou	příčina	k1gFnSc7	příčina
pozoruhodné	pozoruhodný	k2eAgFnPc1d1	pozoruhodná
dynamiky	dynamika	k1gFnPc1	dynamika
a	a	k8xC	a
působivosti	působivost	k1gFnPc1	působivost
marxistického	marxistický	k2eAgInSc2d1	marxistický
komunismu	komunismus	k1gInSc2	komunismus
je	být	k5eAaImIp3nS	být
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
všechny	všechen	k3xTgInPc4	všechen
znaky	znak	k1gInPc4	znak
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Vědecká	vědecký	k2eAgFnSc1d1	vědecká
teorie	teorie	k1gFnSc1	teorie
a	a	k8xC	a
politická	politický	k2eAgFnSc1d1	politická
praxe	praxe	k1gFnSc1	praxe
by	by	kYmCp3nP	by
nikdy	nikdy	k6eAd1	nikdy
nemohly	moct	k5eNaImAgFnP	moct
sehrát	sehrát	k5eAaPmF	sehrát
tak	tak	k6eAd1	tak
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgInPc4d1	hlavní
náboženské	náboženský	k2eAgInPc4d1	náboženský
rysy	rys	k1gInPc4	rys
marxismu	marxismus	k1gInSc2	marxismus
patří	patřit	k5eAaImIp3nP	patřit
jeho	jeho	k3xOp3gInSc4	jeho
strohý	strohý	k2eAgInSc4d1	strohý
dogmatický	dogmatický	k2eAgInSc4d1	dogmatický
systém	systém	k1gInSc4	systém
(	(	kIx(	(
<g/>
třeba	třeba	k6eAd1	třeba
že	že	k8xS	že
si	se	k3xPyFc3	se
jinak	jinak	k6eAd1	jinak
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
praktickou	praktický	k2eAgFnSc4d1	praktická
pružnost	pružnost	k1gFnSc4	pružnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozdělení	rozdělení	k1gNnSc1	rozdělení
na	na	k7c4	na
ortodoxii	ortodoxie	k1gFnSc4	ortodoxie
a	a	k8xC	a
herezi	hereze	k1gFnSc4	hereze
<g/>
,	,	kIx,	,
zkostnatělost	zkostnatělost	k1gFnSc4	zkostnatělost
filosofie	filosofie	k1gFnSc2	filosofie
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
svatá	svatá	k1gFnSc1	svatá
písma	písmo	k1gNnSc2	písmo
<g/>
"	"	kIx"	"
Marxe	Marx	k1gMnSc2	Marx
<g/>
,	,	kIx,	,
Engelse	Engels	k1gMnSc2	Engels
<g/>
,	,	kIx,	,
Lenina	Lenin	k1gMnSc2	Lenin
a	a	k8xC	a
Stalina	Stalin	k1gMnSc2	Stalin
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
pouze	pouze	k6eAd1	pouze
vykládána	vykládán	k2eAgFnSc1d1	vykládána
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
zpochybňována	zpochybňován	k2eAgNnPc1d1	zpochybňováno
<g/>
;	;	kIx,	;
rozdělení	rozdělení	k1gNnSc1	rozdělení
světa	svět	k1gInSc2	svět
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
věřící	věřící	k2eAgMnPc4d1	věřící
vyznavače	vyznavač	k1gMnPc4	vyznavač
a	a	k8xC	a
nevěřící	věřící	k2eNgMnPc4d1	nevěřící
nepřátele	nepřítel	k1gMnPc4	nepřítel
<g/>
;	;	kIx,	;
hierarchicky	hierarchicky	k6eAd1	hierarchicky
organizovaná	organizovaný	k2eAgFnSc1d1	organizovaná
komunistická	komunistický	k2eAgFnSc1d1	komunistická
církev	církev	k1gFnSc1	církev
s	s	k7c7	s
direktivami	direktiva	k1gFnPc7	direktiva
shora	shora	k6eAd1	shora
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Bohohledačské	Bohohledačský	k2eAgNnSc4d1	Bohohledačský
období	období	k1gNnSc4	období
===	===	k?	===
</s>
</p>
<p>
<s>
Berďajev	Berďajet	k5eAaPmDgInS	Berďajet
ve	v	k7c6	v
Vlastním	vlastní	k2eAgInSc6d1	vlastní
životopise	životopis	k1gInSc6	životopis
zeširoka	zeširoka	k6eAd1	zeširoka
popisuje	popisovat	k5eAaImIp3nS	popisovat
vliv	vliv	k1gInSc4	vliv
rodiny	rodina	k1gFnSc2	rodina
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
duchovní	duchovní	k2eAgInSc4d1	duchovní
růst	růst	k1gInSc4	růst
i	i	k9	i
své	svůj	k3xOyFgNnSc4	svůj
obrácení	obrácení	k1gNnSc4	obrácení
se	se	k3xPyFc4	se
ke	k	k7c3	k
křesťanství	křesťanství	k1gNnSc3	křesťanství
<g/>
,	,	kIx,	,
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
hledání	hledání	k1gNnSc4	hledání
Boha	bůh	k1gMnSc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Filosofův	filosofův	k2eAgMnSc1d1	filosofův
otec	otec	k1gMnSc1	otec
vyrostl	vyrůst	k5eAaPmAgMnS	vyrůst
v	v	k7c6	v
církevně-mnišské	církevněnišský	k2eAgFnSc6d1	církevně-mnišský
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Časté	častý	k2eAgInPc1d1	častý
půsty	půst	k1gInPc1	půst
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
podle	podle	k7c2	podle
Berďajeva	Berďajevo	k1gNnSc2	Berďajevo
vystupňovaly	vystupňovat	k5eAaPmAgInP	vystupňovat
nechuť	nechuť	k1gFnSc4	nechuť
k	k	k7c3	k
církevním	církevní	k2eAgFnPc3d1	církevní
strukturám	struktura	k1gFnPc3	struktura
i	i	k8xC	i
dogmatům	dogma	k1gNnPc3	dogma
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
<g/>
,	,	kIx,	,
v	v	k7c6	v
intencích	intence	k1gFnPc6	intence
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
svého	svůj	k1gMnSc2	svůj
vysokého	vysoký	k2eAgNnSc2d1	vysoké
společenského	společenský	k2eAgNnSc2d1	společenské
postavení	postavení	k1gNnSc2	postavení
<g/>
,	,	kIx,	,
voltairovcem	voltairovec	k1gMnSc7	voltairovec
a	a	k8xC	a
osvícencem	osvícenec	k1gMnSc7	osvícenec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
jeho	jeho	k3xOp3gNnSc4	jeho
pochopení	pochopení	k1gNnSc4	pochopení
Boha	bůh	k1gMnSc2	bůh
se	se	k3xPyFc4	se
zračil	zračit	k5eAaImAgMnS	zračit
deismus	deismus	k1gInSc4	deismus
<g/>
.	.	kIx.	.
</s>
<s>
Berďajevova	Berďajevův	k2eAgFnSc1d1	Berďajevova
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
více	hodně	k6eAd2	hodně
Francouzskou	francouzský	k2eAgFnSc4d1	francouzská
nežli	nežli	k8xS	nežli
Ruskou	ruský	k2eAgFnSc4d1	ruská
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
po	po	k7c6	po
otci	otec	k1gMnSc6	otec
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
<g/>
,	,	kIx,	,
přejala	přejmout	k5eAaPmAgFnS	přejmout
matčinu	matčin	k2eAgFnSc4d1	matčina
katolickou	katolický	k2eAgFnSc4d1	katolická
duchovnost	duchovnost	k1gFnSc4	duchovnost
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
prý	prý	k9	prý
neviděla	vidět	k5eNaImAgFnS	vidět
mezi	mezi	k7c7	mezi
pravoslavím	pravoslaví	k1gNnSc7	pravoslaví
a	a	k8xC	a
katolictvím	katolictví	k1gNnSc7	katolictví
nijakých	nijaký	k3yNgInPc2	nijaký
rozdílů	rozdíl	k1gInPc2	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Sedmá	sedmý	k4xOgFnSc1	sedmý
kapitola	kapitola	k1gFnSc1	kapitola
Berďajevovy	Berďajevův	k2eAgFnSc2d1	Berďajevova
autobiografie	autobiografie	k1gFnSc2	autobiografie
s	s	k7c7	s
názvem	název	k1gInSc7	název
Obracení	obracení	k1gNnSc2	obracení
se	se	k3xPyFc4	se
ke	k	k7c3	k
křesťanství	křesťanství	k1gNnSc3	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Náboženské	náboženský	k2eAgNnSc1d1	náboženské
drama	drama	k1gNnSc1	drama
<g/>
.	.	kIx.	.
</s>
<s>
Duchovní	duchovní	k2eAgNnSc1d1	duchovní
setkávání	setkávání	k1gNnSc1	setkávání
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
začíná	začínat	k5eAaImIp3nS	začínat
tvrzením	tvrzení	k1gNnSc7	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
filozof	filozof	k1gMnSc1	filozof
neměl	mít	k5eNaImAgMnS	mít
žádné	žádný	k3yNgNnSc4	žádný
tradiční	tradiční	k2eAgNnSc4d1	tradiční
pravoslavné	pravoslavný	k2eAgNnSc4d1	pravoslavné
dětství	dětství	k1gNnSc4	dětství
<g/>
,	,	kIx,	,
neměl	mít	k5eNaImAgMnS	mít
ani	ani	k8xC	ani
žádné	žádný	k3yNgInPc1	žádný
trvalé	trvalý	k2eAgInPc1d1	trvalý
dojmy	dojem	k1gInPc1	dojem
z	z	k7c2	z
velmi	velmi	k6eAd1	velmi
silně	silně	k6eAd1	silně
působící	působící	k2eAgFnSc2d1	působící
pravoslavné	pravoslavný	k2eAgFnSc2d1	pravoslavná
liturgie	liturgie	k1gFnSc2	liturgie
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
líbil	líbit	k5eAaImAgInS	líbit
katolický	katolický	k2eAgInSc4d1	katolický
ritus	ritus	k1gInSc4	ritus
<g/>
.	.	kIx.	.
</s>
<s>
Berďajev	Berďajet	k5eAaPmDgInS	Berďajet
ale	ale	k8xC	ale
nevyšel	vyjít	k5eNaPmAgInS	vyjít
z	z	k7c2	z
naivní	naivní	k2eAgFnSc2d1	naivní
ortodoxie	ortodoxie	k1gFnSc2	ortodoxie
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
chápal	chápat	k5eAaImAgMnS	chápat
jako	jako	k8xS	jako
dědictví	dědictví	k1gNnSc4	dědictví
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
jako	jako	k9	jako
něco	něco	k3yInSc4	něco
daného	daný	k2eAgNnSc2d1	dané
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Berďajevova	Berďajevův	k2eAgNnSc2d1	Berďajevovo
vyprávění	vyprávění	k1gNnSc2	vyprávění
můžeme	moct	k5eAaImIp1nP	moct
cítit	cítit	k5eAaImF	cítit
i	i	k9	i
jistou	jistý	k2eAgFnSc4d1	jistá
lítost	lítost	k1gFnSc4	lítost
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jako	jako	k9	jako
dítě	dítě	k1gNnSc4	dítě
neprožíval	prožívat	k5eNaImAgMnS	prožívat
pravoslavnou	pravoslavný	k2eAgFnSc4d1	pravoslavná
bohoslužbu	bohoslužba	k1gFnSc4	bohoslužba
tak	tak	k9	tak
intenzivně	intenzivně	k6eAd1	intenzivně
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
přál	přát	k5eAaImAgMnS	přát
<g/>
.	.	kIx.	.
</s>
<s>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
až	až	k9	až
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Berďajev	Berďajet	k5eAaPmDgInS	Berďajet
považoval	považovat	k5eAaImAgMnS	považovat
pravoslaví	pravoslaví	k1gNnSc2	pravoslaví
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
oprávněně	oprávněně	k6eAd1	oprávněně
<g/>
,	,	kIx,	,
za	za	k7c4	za
více	hodně	k6eAd2	hodně
iracionální	iracionální	k2eAgFnSc4d1	iracionální
a	a	k8xC	a
hůře	zle	k6eAd2	zle
definovatelné	definovatelný	k2eAgFnPc1d1	definovatelná
než	než	k8xS	než
katolictví	katolictví	k1gNnSc1	katolictví
či	či	k8xC	či
protestantismus	protestantismus	k1gInSc1	protestantismus
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
iracionalitě	iracionalita	k1gFnSc6	iracionalita
pravoslaví	pravoslaví	k1gNnSc2	pravoslaví
viděl	vidět	k5eAaImAgMnS	vidět
Berďajev	Berďajev	k1gMnSc1	Berďajev
její	její	k3xOp3gFnSc4	její
větší	veliký	k2eAgFnSc4d2	veliký
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
původ	původ	k1gInSc4	původ
svobody	svoboda	k1gFnSc2	svoboda
viděl	vidět	k5eAaImAgMnS	vidět
filozof	filozof	k1gMnSc1	filozof
u	u	k7c2	u
Boha	bůh	k1gMnSc2	bůh
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Ale	ale	k9	ale
tuto	tento	k3xDgFnSc4	tento
svobodu	svoboda	k1gFnSc4	svoboda
jsem	být	k5eAaImIp1nS	být
neprožíval	prožívat	k5eNaImAgMnS	prožívat
jako	jako	k9	jako
lehkost	lehkost	k1gFnSc4	lehkost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
těžkost	těžkost	k1gFnSc1	těžkost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
chápání	chápání	k1gNnSc1	chápání
svobody	svoboda	k1gFnSc2	svoboda
jako	jako	k8xS	jako
povinnosti	povinnost	k1gFnSc2	povinnost
<g/>
,	,	kIx,	,
břemene	břemeno	k1gNnSc2	břemeno
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
zdroje	zdroj	k1gInSc2	zdroj
tragismu	tragismus	k1gInSc2	tragismus
je	být	k5eAaImIp3nS	být
mi	já	k3xPp1nSc3	já
zvláště	zvláště	k6eAd1	zvláště
blízký	blízký	k2eAgMnSc1d1	blízký
Dostojevskij	Dostojevskij	k1gMnSc1	Dostojevskij
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
zřeknutí	zřeknutí	k1gNnSc1	zřeknutí
se	se	k3xPyFc4	se
svobody	svoboda	k1gFnSc2	svoboda
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
lehkost	lehkost	k1gFnSc1	lehkost
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
dát	dát	k5eAaPmF	dát
štěstí	štěstí	k1gNnSc4	štěstí
poslušným	poslušný	k2eAgMnPc3d1	poslušný
"	"	kIx"	"
<g/>
kojencům	kojenec	k1gMnPc3	kojenec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
hřích	hřích	k1gInSc4	hřích
pociťuji	pociťovat	k5eAaImIp1nS	pociťovat
ne	ne	k9	ne
jako	jako	k9	jako
neposlušnost	neposlušnost	k1gFnSc4	neposlušnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
ztrátu	ztráta	k1gFnSc4	ztráta
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Svobodu	Svoboda	k1gMnSc4	Svoboda
však	však	k9	však
cítím	cítit	k5eAaImIp1nS	cítit
jako	jako	k8xS	jako
božskou	božská	k1gFnSc4	božská
<g/>
.	.	kIx.	.
</s>
<s>
Bůh	bůh	k1gMnSc1	bůh
je	být	k5eAaImIp3nS	být
svoboda	svoboda	k1gFnSc1	svoboda
a	a	k8xC	a
svobodu	svoboda	k1gFnSc4	svoboda
dává	dávat	k5eAaImIp3nS	dávat
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
není	být	k5eNaImIp3nS	být
vládce	vládce	k1gMnSc4	vládce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Osvoboditel	osvoboditel	k1gMnSc1	osvoboditel
<g/>
,	,	kIx,	,
Osvoboditel	osvoboditel	k1gMnSc1	osvoboditel
z	z	k7c2	z
otroctví	otroctví	k1gNnSc2	otroctví
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Bůh	bůh	k1gMnSc1	bůh
působí	působit	k5eAaImIp3nS	působit
skrze	skrze	k?	skrze
svobodu	svoboda	k1gFnSc4	svoboda
a	a	k8xC	a
na	na	k7c4	na
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
nepůsobí	působit	k5eNaImIp3nS	působit
skrze	skrze	k?	skrze
nezbytnost	nezbytnost	k1gFnSc4	nezbytnost
a	a	k8xC	a
na	na	k7c4	na
nezbytnost	nezbytnost	k1gFnSc4	nezbytnost
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
nenutí	nutit	k5eNaImIp3nS	nutit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
uznán	uznat	k5eAaPmNgInS	uznat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
je	být	k5eAaImIp3nS	být
skryto	skryt	k2eAgNnSc1d1	skryto
tajemství	tajemství	k1gNnSc1	tajemství
světového	světový	k2eAgInSc2d1	světový
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
prvotní	prvotní	k2eAgFnSc1d1	prvotní
náboženská	náboženský	k2eAgFnSc1d1	náboženská
zkušenost	zkušenost	k1gFnSc1	zkušenost
<g/>
,	,	kIx,	,
popletená	popletený	k2eAgFnSc1d1	popletená
a	a	k8xC	a
zkreslená	zkreslený	k2eAgFnSc1d1	zkreslená
vrstvami	vrstva	k1gFnPc7	vrstva
světové	světový	k2eAgFnSc2d1	světová
nezbytnosti	nezbytnost	k1gFnSc2	nezbytnost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Vztah	vztah	k1gInSc1	vztah
církve	církev	k1gFnSc2	církev
s	s	k7c7	s
okolní	okolní	k2eAgFnSc7d1	okolní
realitou	realita	k1gFnSc7	realita
===	===	k?	===
</s>
</p>
<p>
<s>
Berďajev	Berďajet	k5eAaPmDgInS	Berďajet
intenzivně	intenzivně	k6eAd1	intenzivně
zkoumal	zkoumat	k5eAaImAgInS	zkoumat
problém	problém	k1gInSc1	problém
vztahu	vztah	k1gInSc2	vztah
církve	církev	k1gFnSc2	církev
s	s	k7c7	s
okolní	okolní	k2eAgFnSc7d1	okolní
realitou	realita	k1gFnSc7	realita
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
řešení	řešení	k1gNnSc6	řešení
tohoto	tento	k3xDgInSc2	tento
problému	problém	k1gInSc2	problém
podroboval	podrobovat	k5eAaImAgInS	podrobovat
kritice	kritika	k1gFnSc3	kritika
rozšířený	rozšířený	k2eAgInSc4d1	rozšířený
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
chápal	chápat	k5eAaImAgInS	chápat
církev	církev	k1gFnSc4	církev
jen	jen	k9	jen
jako	jako	k9	jako
hierarchizovanou	hierarchizovaný	k2eAgFnSc4d1	hierarchizovaná
instituci	instituce	k1gFnSc4	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pohled	pohled	k1gInSc1	pohled
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
zdá	zdát	k5eAaImIp3nS	zdát
vcelku	vcelku	k6eAd1	vcelku
přirozený	přirozený	k2eAgInSc1d1	přirozený
<g/>
.	.	kIx.	.
</s>
<s>
Církev	církev	k1gFnSc1	církev
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
institucí	instituce	k1gFnPc2	instituce
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gMnPc1	jejíž
představitelé	představitel	k1gMnPc1	představitel
vždy	vždy	k6eAd1	vždy
využívali	využívat	k5eAaImAgMnP	využívat
<g/>
,	,	kIx,	,
či	či	k8xC	či
zneužívali	zneužívat	k5eAaImAgMnP	zneužívat
svých	svůj	k3xOyFgNnPc2	svůj
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc4	ten
Berďajev	Berďajev	k1gMnSc3	Berďajev
věděl	vědět	k5eAaImAgMnS	vědět
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
i	i	k9	i
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
dřívější	dřívější	k2eAgFnSc3d1	dřívější
exkomunikaci	exkomunikace	k1gFnSc3	exkomunikace
<g/>
.	.	kIx.	.
</s>
<s>
Církev	církev	k1gFnSc1	církev
ale	ale	k8xC	ale
nechápal	chápat	k5eNaImAgMnS	chápat
tak	tak	k6eAd1	tak
jednostranně	jednostranně	k6eAd1	jednostranně
<g/>
.	.	kIx.	.
</s>
<s>
Přemýšlení	přemýšlení	k1gNnSc1	přemýšlení
o	o	k7c6	o
církvi	církev	k1gFnSc6	církev
chtěl	chtít	k5eAaImAgMnS	chtít
očistit	očistit	k5eAaPmF	očistit
od	od	k7c2	od
nánosů	nános	k1gInPc2	nános
světskosti	světskost	k1gFnSc2	světskost
<g/>
.	.	kIx.	.
</s>
<s>
Chápal	chápat	k5eAaImAgMnS	chápat
jí	jíst	k5eAaImIp3nS	jíst
především	především	k9	především
jako	jako	k9	jako
materiálně	materiálně	k6eAd1	materiálně
neuchopitelnou	uchopitelný	k2eNgFnSc4d1	neuchopitelná
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Podstata	podstata	k1gFnSc1	podstata
církve	církev	k1gFnSc2	církev
je	být	k5eAaImIp3nS	být
duchovní	duchovní	k2eAgNnSc1d1	duchovní
<g/>
.	.	kIx.	.
</s>
<s>
Církev	církev	k1gFnSc1	církev
náleží	náležet	k5eAaImIp3nS	náležet
duchovnímu	duchovní	k2eAgInSc3d1	duchovní
světu	svět	k1gInSc3	svět
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
světu	svět	k1gInSc3	svět
přirozenému	přirozený	k2eAgInSc3d1	přirozený
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
církev	církev	k1gFnSc1	církev
nevtělovala	vtělovat	k5eNaImAgFnS	vtělovat
a	a	k8xC	a
viditelně	viditelně	k6eAd1	viditelně
neprojevovala	projevovat	k5eNaImAgFnS	projevovat
v	v	k7c6	v
přírodním	přírodní	k2eAgInSc6d1	přírodní
a	a	k8xC	a
dějinném	dějinný	k2eAgInSc6d1	dějinný
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
neviditelnou	viditelný	k2eNgFnSc7d1	neviditelná
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Další	další	k2eAgFnSc1d1	další
vlastnost	vlastnost	k1gFnSc1	vlastnost
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Berďajev	Berďajev	k1gMnSc1	Berďajev
přisuzuje	přisuzovat	k5eAaImIp3nS	přisuzovat
církvi	církev	k1gFnSc3	církev
je	on	k3xPp3gFnPc4	on
Sobornosť	Sobornosť	k1gFnPc4	Sobornosť
(	(	kIx(	(
<g/>
Můžeme	moct	k5eAaImIp1nP	moct
přeložit	přeložit	k5eAaPmF	přeložit
jako	jako	k9	jako
soudržnost	soudržnost	k1gFnSc4	soudržnost
<g/>
,	,	kIx,	,
či	či	k8xC	či
příslušnost	příslušnost	k1gFnSc1	příslušnost
k	k	k7c3	k
církvi	církev	k1gFnSc3	církev
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
k	k	k7c3	k
chrámu	chrám	k1gInSc3	chrám
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vlastně	vlastně	k9	vlastně
způsob	způsob	k1gInSc1	způsob
vnímání	vnímání	k1gNnSc2	vnímání
duchovních	duchovní	k2eAgFnPc2d1	duchovní
zkušeností	zkušenost	k1gFnPc2	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Berďajev	Berďajet	k5eAaPmDgInS	Berďajet
je	být	k5eAaImIp3nS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
duchovním	duchovní	k2eAgNnSc6d1	duchovní
setkávání	setkávání	k1gNnSc6	setkávání
s	s	k7c7	s
Kristem	Kristus	k1gMnSc7	Kristus
skrze	skrze	k?	skrze
církev	církev	k1gFnSc1	církev
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
metafyzickou	metafyzický	k2eAgFnSc4d1	metafyzická
platformu	platforma	k1gFnSc4	platforma
pro	pro	k7c4	pro
tato	tento	k3xDgNnPc4	tento
setkání	setkání	k1gNnPc4	setkání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
setkává	setkávat	k5eAaImIp3nS	setkávat
i	i	k9	i
se	s	k7c7	s
všemi	všecek	k3xTgMnPc7	všecek
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
podobnou	podobný	k2eAgFnSc4d1	podobná
zkušenost	zkušenost	k1gFnSc4	zkušenost
zažili	zažít	k5eAaPmAgMnP	zažít
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
člověk	člověk	k1gMnSc1	člověk
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
Kristem	Kristus	k1gMnSc7	Kristus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
setkává	setkávat	k5eAaImIp3nS	setkávat
i	i	k9	i
s	s	k7c7	s
"	"	kIx"	"
<g/>
celým	celý	k2eAgInSc7d1	celý
křesťanským	křesťanský	k2eAgInSc7d1	křesťanský
světem	svět	k1gInSc7	svět
<g/>
,	,	kIx,	,
s	s	k7c7	s
apoštoly	apoštol	k1gMnPc7	apoštol
<g/>
,	,	kIx,	,
světci	světec	k1gMnPc7	světec
<g/>
,	,	kIx,	,
s	s	k7c7	s
bratry	bratr	k1gMnPc7	bratr
v	v	k7c4	v
Kristu	Krista	k1gFnSc4	Krista
<g/>
,	,	kIx,	,
živými	živý	k2eAgFnPc7d1	živá
i	i	k8xC	i
zemřelými	zemřelý	k2eAgFnPc7d1	zemřelá
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
církvi	církev	k1gFnSc3	církev
patří	patřit	k5eAaImIp3nP	patřit
nejen	nejen	k6eAd1	nejen
žijící	žijící	k2eAgMnPc1d1	žijící
pokolení	pokolení	k1gNnSc2	pokolení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
všechna	všechen	k3xTgNnPc4	všechen
pokolení	pokolení	k1gNnPc4	pokolení
zemřelá	zemřelý	k2eAgNnPc4d1	zemřelé
<g/>
,	,	kIx,	,
i	i	k8xC	i
ta	ten	k3xDgNnPc1	ten
žijí	žít	k5eAaImIp3nP	žít
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
církví	církev	k1gFnSc7	církev
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
trvá	trvat	k5eAaImIp3nS	trvat
skutečné	skutečný	k2eAgNnSc1d1	skutečné
společenství	společenství	k1gNnSc1	společenství
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
bytostný	bytostný	k2eAgInSc4d1	bytostný
projev	projev	k1gInSc4	projev
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Berďajev	Berďajev	k1gMnSc1	Berďajev
byl	být	k5eAaImAgMnS	být
nonkonformní	nonkonformní	k2eAgMnSc1d1	nonkonformní
myslitel	myslitel	k1gMnSc1	myslitel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
chápal	chápat	k5eAaImAgInS	chápat
původní	původní	k2eAgFnSc4d1	původní
podstatu	podstata	k1gFnSc4	podstata
církve	církev	k1gFnSc2	církev
"	"	kIx"	"
<g/>
jako	jako	k8xC	jako
duchovního	duchovní	k2eAgInSc2d1	duchovní
organismu	organismus	k1gInSc2	organismus
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
mystického	mystický	k2eAgNnSc2d1	mystické
těla	tělo	k1gNnSc2	tělo
Kristova	Kristův	k2eAgNnSc2d1	Kristovo
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Tato	tento	k3xDgFnSc1	tento
tvrzení	tvrzení	k1gNnSc4	tvrzení
jsou	být	k5eAaImIp3nP	být
zcela	zcela	k6eAd1	zcela
v	v	k7c6	v
intencích	intence	k1gFnPc6	intence
Berďajevovy	Berďajevův	k2eAgFnSc2d1	Berďajevova
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
personalistické	personalistický	k2eAgFnSc2d1	personalistická
filozofie	filozofie	k1gFnSc2	filozofie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Theodicea	Theodiceum	k1gNnSc2	Theodiceum
===	===	k?	===
</s>
</p>
<p>
<s>
Berďajev	Berďajet	k5eAaPmDgInS	Berďajet
zkoumal	zkoumat	k5eAaImAgInS	zkoumat
i	i	k9	i
problém	problém	k1gInSc1	problém
theodiceje	theodicej	k1gMnSc2	theodicej
(	(	kIx(	(
<g/>
Poprvé	poprvé	k6eAd1	poprvé
použil	použít	k5eAaPmAgInS	použít
tento	tento	k3xDgInSc4	tento
pojem	pojem	k1gInSc4	pojem
G.	G.	kA	G.
W.	W.	kA	W.
Leibnitz	Leibnitz	k1gMnSc1	Leibnitz
roku	rok	k1gInSc2	rok
1710	[number]	k4	1710
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
ovšem	ovšem	k9	ovšem
starší	starý	k2eAgInSc1d2	starší
filosofický	filosofický	k2eAgInSc1d1	filosofický
problém	problém	k1gInSc1	problém
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
ospravedlnění	ospravedlnění	k1gNnSc1	ospravedlnění
Boha	bůh	k1gMnSc4	bůh
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
existenci	existence	k1gFnSc3	existence
zla	zlo	k1gNnSc2	zlo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
a	a	k8xC	a
nejzkoumanějších	zkoumaný	k2eAgInPc2d3	zkoumaný
fenoménů	fenomén	k1gInPc2	fenomén
filozofie	filozofie	k1gFnSc2	filozofie
a	a	k8xC	a
teologie	teologie	k1gFnSc2	teologie
<g/>
.	.	kIx.	.
</s>
<s>
Otázkou	otázka	k1gFnSc7	otázka
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
vyrovnat	vyrovnat	k5eAaBmF	vyrovnat
s	s	k7c7	s
utrpením	utrpení	k1gNnSc7	utrpení
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
kterému	který	k3yRgInSc3	který
Bůh	bůh	k1gMnSc1	bůh
nezabraňuje	zabraňovat	k5eNaImIp3nS	zabraňovat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
řešení	řešení	k1gNnSc6	řešení
tohoto	tento	k3xDgInSc2	tento
problému	problém	k1gInSc2	problém
si	se	k3xPyFc3	se
Berďajev	Berďajev	k1gMnSc1	Berďajev
vypomáhá	vypomáhat	k5eAaImIp3nS	vypomáhat
Dostojevským	Dostojevský	k2eAgMnSc7d1	Dostojevský
<g/>
.	.	kIx.	.
</s>
<s>
Středobodem	středobod	k1gInSc7	středobod
Dostojevského	Dostojevský	k2eAgInSc2d1	Dostojevský
zájmu	zájem	k1gInSc2	zájem
totiž	totiž	k9	totiž
je	být	k5eAaImIp3nS	být
utrpení	utrpení	k1gNnSc1	utrpení
a	a	k8xC	a
vyrovnávání	vyrovnávání	k1gNnSc1	vyrovnávání
se	se	k3xPyFc4	se
s	s	k7c7	s
utrpením	utrpení	k1gNnSc7	utrpení
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
jeho	jeho	k3xOp3gFnSc1	jeho
literární	literární	k2eAgFnSc1d1	literární
postava	postava	k1gFnSc1	postava
prochází	procházet	k5eAaImIp3nS	procházet
přerodem	přerod	k1gInSc7	přerod
skrz	skrz	k7c4	skrz
utrpení	utrpení	k1gNnSc4	utrpení
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
zkušeností	zkušenost	k1gFnSc7	zkušenost
ovšem	ovšem	k9	ovšem
nakládá	nakládat	k5eAaImIp3nS	nakládat
každá	každý	k3xTgFnSc1	každý
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
<g/>
.	.	kIx.	.
</s>
<s>
Berďajev	Berďajet	k5eAaPmDgInS	Berďajet
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
vypůjčuje	vypůjčovat	k5eAaImIp3nS	vypůjčovat
Dostojevského	Dostojevský	k2eAgInSc2d1	Dostojevský
přístup	přístup	k1gInSc4	přístup
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
tedy	tedy	k9	tedy
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
rodí	rodit	k5eAaImIp3nS	rodit
utrpení	utrpení	k1gNnSc4	utrpení
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
svoboda	svoboda	k1gFnSc1	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
myslitelů	myslitel	k1gMnPc2	myslitel
nechce	chtít	k5eNaImIp3nS	chtít
svět	svět	k1gInSc1	svět
bez	bez	k7c2	bez
svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
bez	bez	k7c2	bez
utrpení	utrpení	k1gNnSc2	utrpení
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
nechtějí	chtít	k5eNaImIp3nP	chtít
svět	svět	k1gInSc4	svět
bez	bez	k7c2	bez
utrpení	utrpení	k1gNnSc2	utrpení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
bez	bez	k7c2	bez
zápasů	zápas	k1gInPc2	zápas
o	o	k7c4	o
dobro	dobro	k1gNnSc4	dobro
<g/>
.	.	kIx.	.
</s>
<s>
Bůh	bůh	k1gMnSc1	bůh
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
lidí	člověk	k1gMnPc2	člověk
i	i	k8xC	i
pro	pro	k7c4	pro
učení	učení	k1gNnSc4	učení
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
církví	církev	k1gFnPc2	církev
ekvivalentem	ekvivalent	k1gInSc7	ekvivalent
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Berďajeva	Berďajev	k1gMnSc4	Berďajev
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Chápe	chápat	k5eAaImIp3nS	chápat
kategorie	kategorie	k1gFnPc4	kategorie
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
síly	síla	k1gFnSc2	síla
jako	jako	k8xS	jako
sociologický	sociologický	k2eAgInSc1d1	sociologický
konstrukt	konstrukt	k1gInSc1	konstrukt
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
produkt	produkt	k1gInSc4	produkt
"	"	kIx"	"
<g/>
sociologických	sociologický	k2eAgFnPc2d1	sociologická
domluv	domluva	k1gFnPc2	domluva
či	či	k8xC	či
sugescí	sugesce	k1gFnPc2	sugesce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Moc	moc	k6eAd1	moc
a	a	k8xC	a
síla	síla	k1gFnSc1	síla
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
Berďajeva	Berďajevo	k1gNnSc2	Berďajevo
kategoriemi	kategorie	k1gFnPc7	kategorie
aplikovatelnými	aplikovatelný	k2eAgFnPc7d1	aplikovatelná
např.	např.	kA	např.
na	na	k7c4	na
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
má	mít	k5eAaImIp3nS	mít
ovšem	ovšem	k9	ovšem
podle	podle	k7c2	podle
filozofových	filozofův	k2eAgNnPc2d1	filozofovo
slov	slovo	k1gNnPc2	slovo
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
k	k	k7c3	k
Bohu	bůh	k1gMnSc3	bůh
nízký	nízký	k2eAgInSc4d1	nízký
základ	základ	k1gInSc4	základ
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
nejsou	být	k5eNaImIp3nP	být
kategorie	kategorie	k1gFnSc1	kategorie
jako	jako	k8xC	jako
moc	moc	k1gFnSc1	moc
a	a	k8xC	a
síla	síla	k1gFnSc1	síla
převoditelná	převoditelný	k2eAgFnSc1d1	převoditelná
na	na	k7c6	na
uvažování	uvažování	k1gNnSc6	uvažování
o	o	k7c6	o
Bohu	bůh	k1gMnSc6	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Bůh	bůh	k1gMnSc1	bůh
nemá	mít	k5eNaImIp3nS	mít
moc	moc	k1gFnSc4	moc
a	a	k8xC	a
sílu	síla	k1gFnSc4	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Berďajev	Berďajev	k1gFnPc4	Berďajev
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc4	jeho
možnosti	možnost	k1gFnPc4	možnost
uplatnění	uplatnění	k1gNnSc2	uplatnění
v	v	k7c6	v
emigraci	emigrace	k1gFnSc6	emigrace
===	===	k?	===
</s>
</p>
<p>
<s>
Berďajev	Berďajet	k5eAaPmDgInS	Berďajet
si	se	k3xPyFc3	se
uvědomoval	uvědomovat	k5eAaImAgMnS	uvědomovat
<g/>
,	,	kIx,	,
možná	možná	k9	možná
lépe	dobře	k6eAd2	dobře
než	než	k8xS	než
ostatní	ostatní	k2eAgMnPc1d1	ostatní
<g/>
,	,	kIx,	,
že	že	k8xS	že
nový	nový	k2eAgInSc1d1	nový
komunistický	komunistický	k2eAgInSc1d1	komunistický
režim	režim	k1gInSc1	režim
bude	být	k5eAaImBp3nS	být
nepřátelský	přátelský	k2eNgInSc1d1	nepřátelský
vůči	vůči	k7c3	vůči
svobodnému	svobodný	k2eAgNnSc3d1	svobodné
vyjádření	vyjádření	k1gNnSc3	vyjádření
názoru	názor	k1gInSc2	názor
<g/>
.	.	kIx.	.
</s>
<s>
Jistě	jistě	k6eAd1	jistě
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
režim	režim	k1gInSc1	režim
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
prosazovat	prosazovat	k5eAaImF	prosazovat
násilím	násilí	k1gNnSc7	násilí
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
chtěl	chtít	k5eAaImAgMnS	chtít
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
rodné	rodný	k2eAgFnSc6d1	rodná
zemi	zem	k1gFnSc6	zem
zůstat	zůstat	k5eAaPmF	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
tehdejší	tehdejší	k2eAgMnPc1d1	tehdejší
myslitelé	myslitel	k1gMnPc1	myslitel
si	se	k3xPyFc3	se
přáli	přát	k5eAaImAgMnP	přát
modernizaci	modernizace	k1gFnSc4	modernizace
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Berďajev	Berďajev	k1gMnSc1	Berďajev
upozorňoval	upozorňovat	k5eAaImAgMnS	upozorňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
modernizace	modernizace	k1gFnSc1	modernizace
přinese	přinést	k5eAaPmIp3nS	přinést
jen	jen	k9	jen
škody	škoda	k1gFnPc4	škoda
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
bude	být	k5eAaImBp3nS	být
vedena	vést	k5eAaImNgFnS	vést
"	"	kIx"	"
<g/>
petrohradskou	petrohradský	k2eAgFnSc7d1	Petrohradská
byrokratickou	byrokratický	k2eAgFnSc7d1	byrokratická
myslí	mysl	k1gFnSc7	mysl
po	po	k7c6	po
evropské	evropský	k2eAgFnSc6d1	Evropská
sekulární	sekulární	k2eAgFnSc6d1	sekulární
<g/>
,	,	kIx,	,
racionální	racionální	k2eAgFnSc6d1	racionální
a	a	k8xC	a
technokratické	technokratický	k2eAgFnSc6d1	technokratická
cestě	cesta	k1gFnSc6	cesta
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Deportaci	deportace	k1gFnSc3	deportace
inteligence	inteligence	k1gFnSc2	inteligence
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
1923	[number]	k4	1923
předcházel	předcházet	k5eAaImAgMnS	předcházet
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
Lesley	Lesle	k2eAgFnPc1d1	Lesle
Chamberlainová	Chamberlainová	k1gFnSc1	Chamberlainová
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Parník	parník	k1gInSc1	parník
filozofů	filozof	k1gMnPc2	filozof
<g/>
;	;	kIx,	;
Lenin	Lenin	k1gMnSc1	Lenin
a	a	k8xC	a
vyhnání	vyhnání	k1gNnSc1	vyhnání
inteligence	inteligence	k1gFnSc2	inteligence
nazývá	nazývat	k5eAaImIp3nS	nazývat
Papírovou	papírový	k2eAgFnSc7d1	papírová
občanskou	občanský	k2eAgFnSc7d1	občanská
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Válka	válka	k1gFnSc1	válka
mezi	mezi	k7c7	mezi
bolševiky	bolševik	k1gMnPc7	bolševik
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
kritiky	kritika	k1gFnSc2	kritika
vedená	vedený	k2eAgFnSc1d1	vedená
v	v	k7c6	v
tisku	tisk	k1gInSc6	tisk
v	v	k7c6	v
prvních	první	k4xOgNnPc6	první
letech	léto	k1gNnPc6	léto
po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
zdůrazněna	zdůrazněn	k2eAgFnSc1d1	zdůrazněna
její	její	k3xOp3gFnSc4	její
důležitost	důležitost	k1gFnSc4	důležitost
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
nazývat	nazývat	k5eAaImF	nazývat
Papírová	papírový	k2eAgFnSc1d1	papírová
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
předcházela	předcházet	k5eAaImAgFnS	předcházet
občanské	občanský	k2eAgInPc4d1	občanský
válce	válec	k1gInPc4	válec
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
-	-	kIx~	-
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vojenskému	vojenský	k2eAgInSc3d1	vojenský
konfliktu	konflikt	k1gInSc3	konflikt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
příštích	příští	k2eAgNnPc6d1	příští
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
rozsekal	rozsekat	k5eAaPmAgMnS	rozsekat
Rusko	Rusko	k1gNnSc4	Rusko
na	na	k7c4	na
kusy	kus	k1gInPc4	kus
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zdůraznění	zdůraznění	k1gNnSc4	zdůraznění
tohoto	tento	k3xDgInSc2	tento
boje	boj	k1gInSc2	boj
myšlenek	myšlenka	k1gFnPc2	myšlenka
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
vidět	vidět	k5eAaImF	vidět
krutý	krutý	k2eAgInSc1d1	krutý
boj	boj	k1gInSc1	boj
mezi	mezi	k7c7	mezi
rudou	rudý	k2eAgFnSc7d1	rudá
a	a	k8xC	a
bílou	bílý	k2eAgFnSc7d1	bílá
armádou	armáda	k1gFnSc7	armáda
jako	jako	k8xS	jako
centrum	centrum	k1gNnSc1	centrum
širší	široký	k2eAgFnSc2d2	širší
kampaně	kampaň	k1gFnSc2	kampaň
–	–	k?	–
trvající	trvající	k2eAgInSc1d1	trvající
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
–	–	k?	–
na	na	k7c4	na
zničení	zničení	k1gNnSc4	zničení
opozice	opozice	k1gFnSc2	opozice
proti	proti	k7c3	proti
bolševismu	bolševismus	k1gInSc3	bolševismus
mezi	mezi	k7c7	mezi
lidem	lid	k1gInSc7	lid
všeobecně	všeobecně	k6eAd1	všeobecně
a	a	k8xC	a
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
mezi	mezi	k7c7	mezi
inteligencí	inteligence	k1gFnSc7	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
historicko-politický	historickoolitický	k2eAgInSc4d1	historicko-politický
rámec	rámec	k1gInSc4	rámec
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
předkládám	předkládat	k5eAaImIp1nS	předkládat
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
knize	kniha	k1gFnSc6	kniha
a	a	k8xC	a
který	který	k3yIgInSc1	který
dává	dávat	k5eAaImIp3nS	dávat
vyhánění	vyhánění	k1gNnSc4	vyhánění
smysl	smysl	k1gInSc1	smysl
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Papírová	papírový	k2eAgFnSc1d1	papírová
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
probíhala	probíhat	k5eAaImAgFnS	probíhat
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
periodik	periodikum	k1gNnPc2	periodikum
<g/>
,	,	kIx,	,
knih	kniha	k1gFnPc2	kniha
i	i	k8xC	i
pamfletů	pamflet	k1gInPc2	pamflet
současně	současně	k6eAd1	současně
s	s	k7c7	s
občanskou	občanský	k2eAgFnSc7d1	občanská
válkou	válka	k1gFnSc7	válka
vedenou	vedený	k2eAgFnSc4d1	vedená
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
sestavování	sestavování	k1gNnSc4	sestavování
seznamů	seznam	k1gInPc2	seznam
nežádoucích	žádoucí	k2eNgFnPc2d1	nežádoucí
osob	osoba	k1gFnPc2	osoba
určených	určený	k2eAgFnPc2d1	určená
k	k	k7c3	k
deportaci	deportace	k1gFnSc3	deportace
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
Lenina	Lenin	k1gMnSc2	Lenin
staly	stát	k5eAaPmAgFnP	stát
zkušenosti	zkušenost	k1gFnPc1	zkušenost
z	z	k7c2	z
papírové	papírový	k2eAgFnSc2d1	papírová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Příslušníci	příslušník	k1gMnPc1	příslušník
inteligence	inteligence	k1gFnSc2	inteligence
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
"	"	kIx"	"
<g/>
válce	válka	k1gFnSc6	válka
<g/>
"	"	kIx"	"
angažovali	angažovat	k5eAaBmAgMnP	angažovat
proti	proti	k7c3	proti
bolševikům	bolševik	k1gMnPc3	bolševik
a	a	k8xC	a
ideologii	ideologie	k1gFnSc3	ideologie
komunismu	komunismus	k1gInSc2	komunismus
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
mohli	moct	k5eAaImAgMnP	moct
být	být	k5eAaImF	být
jisti	jist	k2eAgMnPc1d1	jist
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
seznamech	seznam	k1gInPc6	seznam
lidí	člověk	k1gMnPc2	člověk
určených	určený	k2eAgMnPc2d1	určený
k	k	k7c3	k
deportaci	deportace	k1gFnSc3	deportace
objeví	objevit	k5eAaPmIp3nS	objevit
také	také	k6eAd1	také
<g/>
.	.	kIx.	.
</s>
<s>
Protikomunistickou	protikomunistický	k2eAgFnSc4d1	protikomunistická
inteligenci	inteligence	k1gFnSc4	inteligence
sjednocoval	sjednocovat	k5eAaImAgInS	sjednocovat
její	její	k3xOp3gInSc1	její
budoucí	budoucí	k2eAgInSc1d1	budoucí
nelehký	lehký	k2eNgInSc1d1	nelehký
osud	osud	k1gInSc1	osud
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jednotná	jednotný	k2eAgFnSc1d1	jednotná
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
odporu	odpor	k1gInSc6	odpor
proti	proti	k7c3	proti
etablujícímu	etablující	k2eAgMnSc3d1	etablující
se	se	k3xPyFc4	se
režimu	režim	k1gInSc6	režim
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
případě	případ	k1gInSc6	případ
nebyla	být	k5eNaImAgFnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
organizace	organizace	k1gFnPc1	organizace
a	a	k8xC	a
kvazipolitická	kvazipolitický	k2eAgNnPc4d1	kvazipolitický
uskupení	uskupení	k1gNnPc4	uskupení
uspokojivě	uspokojivě	k6eAd1	uspokojivě
popisuje	popisovat	k5eAaImIp3nS	popisovat
kromě	kromě	k7c2	kromě
Chamberlainové	Chamberlainová	k1gFnSc2	Chamberlainová
i	i	k9	i
Martin	Martin	k1gMnSc1	Martin
Malia	Malius	k1gMnSc2	Malius
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
obsáhlém	obsáhlý	k2eAgInSc6d1	obsáhlý
díle	díl	k1gInSc6	díl
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
tragédie	tragédie	k1gFnSc1	tragédie
<g/>
.	.	kIx.	.
</s>
<s>
Leninův	Leninův	k2eAgInSc1d1	Leninův
rozkaz	rozkaz	k1gInSc1	rozkaz
o	o	k7c6	o
vyhnání	vyhnání	k1gNnSc6	vyhnání
inteligence	inteligence	k1gFnSc2	inteligence
i	i	k9	i
s	s	k7c7	s
rodinami	rodina	k1gFnPc7	rodina
se	se	k3xPyFc4	se
týkal	týkat	k5eAaImAgInS	týkat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvou	dva	k4xCgNnPc2	dva
set	sto	k4xCgNnPc2	sto
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
do	do	k7c2	do
emigrace	emigrace	k1gFnSc2	emigrace
dostávali	dostávat	k5eAaImAgMnP	dostávat
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
se	se	k3xPyFc4	se
část	část	k1gFnSc1	část
vyhnané	vyhnaný	k2eAgFnSc2d1	vyhnaná
inteligence	inteligence	k1gFnSc2	inteligence
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
emigrace	emigrace	k1gFnSc2	emigrace
na	na	k7c6	na
palubách	paluba	k1gFnPc6	paluba
německých	německý	k2eAgInPc2d1	německý
parníků	parník	k1gInPc2	parník
Oberbürgermeister	Oberbürgermeister	k1gMnSc1	Oberbürgermeister
Haken	Haken	k1gMnSc1	Haken
a	a	k8xC	a
Preussen	Preussen	k1gInSc1	Preussen
z	z	k7c2	z
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
do	do	k7c2	do
Štětína	Štětín	k1gInSc2	Štětín
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc1d1	další
intelektuálové	intelektuál	k1gMnPc1	intelektuál
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
postihl	postihnout	k5eAaPmAgInS	postihnout
Leninův	Leninův	k2eAgInSc1d1	Leninův
rozkaz	rozkaz	k1gInSc1	rozkaz
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
během	během	k7c2	během
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
dostávali	dostávat	k5eAaImAgMnP	dostávat
vlaky	vlak	k1gInPc4	vlak
do	do	k7c2	do
Rigy	Riga	k1gFnSc2	Riga
nebo	nebo	k8xC	nebo
byli	být	k5eAaImAgMnP	být
převezeni	převézt	k5eAaPmNgMnP	převézt
lodí	loď	k1gFnPc2	loď
z	z	k7c2	z
Oděsy	Oděsa	k1gFnSc2	Oděsa
do	do	k7c2	do
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
<g/>
.	.	kIx.	.
</s>
<s>
Filozof	filozof	k1gMnSc1	filozof
Berďajev	Berďajev	k1gMnSc1	Berďajev
se	s	k7c7	s
ženou	žena	k1gFnSc7	žena
cestovali	cestovat	k5eAaImAgMnP	cestovat
do	do	k7c2	do
emigrace	emigrace	k1gFnSc2	emigrace
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
Oberbürgermeister	Oberbürgermeister	k1gMnSc1	Oberbürgermeister
Haken	Haken	k1gMnSc1	Haken
<g/>
.	.	kIx.	.
</s>
<s>
Plavby	plavba	k1gFnPc1	plavba
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
lodi	loď	k1gFnSc6	loď
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
25	[number]	k4	25
členů	člen	k1gMnPc2	člen
ruské	ruský	k2eAgFnSc2d1	ruská
inteligence	inteligence	k1gFnSc2	inteligence
s	s	k7c7	s
rodinami	rodina	k1gFnPc7	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
tedy	tedy	k9	tedy
zhruba	zhruba	k6eAd1	zhruba
o	o	k7c4	o
75	[number]	k4	75
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
kterými	který	k3yQgMnPc7	který
se	se	k3xPyFc4	se
nacházeli	nacházet	k5eAaImAgMnP	nacházet
kromě	kromě	k7c2	kromě
Berďajeva	Berďajev	k1gMnSc2	Berďajev
ještě	ještě	k6eAd1	ještě
např.	např.	kA	např.
filozofové	filozof	k1gMnPc1	filozof
a	a	k8xC	a
teologové	teolog	k1gMnPc1	teolog
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Losskij	Losskij	k1gMnSc1	Losskij
a	a	k8xC	a
Sergej	Sergej	k1gMnSc1	Sergej
Bulgakov	Bulgakov	k1gInSc4	Bulgakov
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
světoznámý	světoznámý	k2eAgMnSc1d1	světoznámý
sociolog	sociolog	k1gMnSc1	sociolog
Pitirim-Sorokin	Pitirim-Sorokin	k1gMnSc1	Pitirim-Sorokin
<g/>
.	.	kIx.	.
</s>
<s>
Emigranti	emigrant	k1gMnPc1	emigrant
si	se	k3xPyFc3	se
museli	muset	k5eAaImAgMnP	muset
parník	parník	k1gInSc4	parník
sami	sám	k3xTgMnPc1	sám
pronajmout	pronajmout	k5eAaPmF	pronajmout
a	a	k8xC	a
zaplatit	zaplatit	k5eAaPmF	zaplatit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
příjezdu	příjezd	k1gInSc6	příjezd
do	do	k7c2	do
Štětína	Štětín	k1gInSc2	Štětín
se	se	k3xPyFc4	se
emigranti	emigrant	k1gMnPc1	emigrant
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
usadit	usadit	k5eAaPmF	usadit
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Berlín	Berlín	k1gInSc1	Berlín
===	===	k?	===
</s>
</p>
<p>
<s>
Berlín	Berlín	k1gInSc1	Berlín
byl	být	k5eAaImAgInS	být
už	už	k6eAd1	už
tehdy	tehdy	k6eAd1	tehdy
městem	město	k1gNnSc7	město
s	s	k7c7	s
početnou	početný	k2eAgFnSc7d1	početná
ruskou	ruský	k2eAgFnSc7d1	ruská
diasporou	diaspora	k1gFnSc7	diaspora
<g/>
.	.	kIx.	.
</s>
<s>
Nacházeli	nacházet	k5eAaImAgMnP	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
emigranti	emigrant	k1gMnPc1	emigrant
ještě	ještě	k9	ještě
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
carského	carský	k2eAgNnSc2d1	carské
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pozdější	pozdní	k2eAgMnSc1d2	pozdější
<g/>
.	.	kIx.	.
</s>
<s>
Berďajev	Berďajet	k5eAaPmDgInS	Berďajet
strávil	strávit	k5eAaPmAgMnS	strávit
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
vlastních	vlastní	k2eAgInPc2d1	vlastní
zápisků	zápisek	k1gInPc2	zápisek
je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
emigraci	emigrace	k1gFnSc6	emigrace
dosti	dosti	k6eAd1	dosti
trápil	trápit	k5eAaImAgMnS	trápit
<g/>
.	.	kIx.	.
</s>
<s>
Jednak	jednak	k8xC	jednak
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemohl	moct	k5eNaImAgMnS	moct
působit	působit	k5eAaImF	působit
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
milované	milovaný	k2eAgFnSc6d1	milovaná
vlasti	vlast	k1gFnSc6	vlast
a	a	k8xC	a
také	také	k9	také
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
Rusové	Rus	k1gMnPc1	Rus
v	v	k7c6	v
berlínské	berlínský	k2eAgFnSc6d1	Berlínská
diaspoře	diaspora	k1gFnSc6	diaspora
nechtěli	chtít	k5eNaImAgMnP	chtít
přijmout	přijmout	k5eAaPmF	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
averze	averze	k1gFnSc2	averze
k	k	k7c3	k
celé	celý	k2eAgFnSc3d1	celá
skupině	skupina	k1gFnSc3	skupina
emigrantů	emigrant	k1gMnPc2	emigrant
byl	být	k5eAaImAgMnS	být
jejich	jejich	k3xOp3gInSc4	jejich
příjezd	příjezd	k1gInSc4	příjezd
z	z	k7c2	z
již	již	k9	již
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznaného	uznaný	k2eAgInSc2d1	uznaný
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
berlínskými	berlínský	k2eAgMnPc7d1	berlínský
krajany	krajan	k1gMnPc7	krajan
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
šířily	šířit	k5eAaImAgFnP	šířit
domněnky	domněnka	k1gFnPc1	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
tito	tento	k3xDgMnPc1	tento
nově	nově	k6eAd1	nově
přišedší	přišedší	k2eAgMnPc1d1	přišedší
jsou	být	k5eAaImIp3nP	být
Leninovými	Leninův	k2eAgMnPc7d1	Leninův
agenty	agent	k1gMnPc7	agent
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
vysláni	vyslán	k2eAgMnPc1d1	vyslán
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
rozbití	rozbití	k1gNnSc2	rozbití
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
"	"	kIx"	"
<g/>
opozice	opozice	k1gFnSc2	opozice
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nejspíše	nejspíše	k9	nejspíše
se	se	k3xPyFc4	se
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
my	my	k3xPp1nPc1	my
podivovali	podivovat	k5eAaImAgMnP	podivovat
nad	nad	k7c7	nad
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
emigranti	emigrant	k1gMnPc1	emigrant
z	z	k7c2	z
lodí	loď	k1gFnPc2	loď
filozofů	filozof	k1gMnPc2	filozof
nebyli	být	k5eNaImAgMnP	být
prostě	prostě	k6eAd1	prostě
zastřeleni	zastřelit	k5eAaPmNgMnP	zastřelit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nicméně	nicméně	k8xC	nicméně
i	i	k9	i
přes	přes	k7c4	přes
tuto	tento	k3xDgFnSc4	tento
epizodu	epizoda	k1gFnSc4	epizoda
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
několika	několik	k4yIc3	několik
setkáním	setkání	k1gNnPc3	setkání
s	s	k7c7	s
představiteli	představitel	k1gMnPc7	představitel
berlínské	berlínský	k2eAgFnSc2d1	Berlínská
bílé	bílý	k2eAgFnSc2d1	bílá
emigrace	emigrace	k1gFnSc2	emigrace
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Berďajev	Berďajev	k1gMnSc1	Berďajev
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
vůdcem	vůdce	k1gMnSc7	vůdce
bílého	bílý	k2eAgNnSc2d1	bílé
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
ekonomem	ekonom	k1gMnSc7	ekonom
a	a	k8xC	a
filozofem	filozof	k1gMnSc7	filozof
Petrem	Petr	k1gMnSc7	Petr
Struvem	Struv	k1gMnSc7	Struv
<g/>
.	.	kIx.	.
</s>
<s>
Struve	Struvat	k5eAaPmIp3nS	Struvat
se	se	k3xPyFc4	se
s	s	k7c7	s
Berďajevem	Berďajev	k1gInSc7	Berďajev
znal	znát	k5eAaImAgInS	znát
už	už	k6eAd1	už
z	z	k7c2	z
mládí	mládí	k1gNnSc2	mládí
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
oba	dva	k4xCgInPc1	dva
dva	dva	k4xCgInPc1	dva
propadli	propadnout	k5eAaPmAgMnP	propadnout
šálivému	šálivý	k2eAgNnSc3d1	šálivé
kouzlu	kouzlo	k1gNnSc3	kouzlo
marxismu	marxismus	k1gInSc2	marxismus
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
v	v	k7c6	v
emigraci	emigrace	k1gFnSc6	emigrace
se	se	k3xPyFc4	se
rozkmotřili	rozkmotřit	k5eAaPmAgMnP	rozkmotřit
a	a	k8xC	a
přerušili	přerušit	k5eAaPmAgMnP	přerušit
styky	styk	k1gInPc4	styk
<g/>
.	.	kIx.	.
</s>
<s>
Struve	Struvat	k5eAaPmIp3nS	Struvat
jako	jako	k9	jako
protibolševický	protibolševický	k2eAgInSc1d1	protibolševický
radikál	radikál	k1gInSc1	radikál
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
byl	být	k5eAaImAgMnS	být
ostatně	ostatně	k6eAd1	ostatně
Berďajev	Berďajev	k1gMnSc1	Berďajev
taktéž	taktéž	k?	taktéž
<g/>
,	,	kIx,	,
nemohl	moct	k5eNaImAgMnS	moct
např.	např.	kA	např.
pochopit	pochopit	k5eAaPmF	pochopit
Berďajevův	Berďajevův	k2eAgInSc4d1	Berďajevův
názor	názor	k1gInSc4	názor
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
filozof	filozof	k1gMnSc1	filozof
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
kladně	kladně	k6eAd1	kladně
k	k	k7c3	k
otázce	otázka	k1gFnSc3	otázka
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
uznání	uznání	k1gNnSc2	uznání
sovětské	sovětský	k2eAgFnSc2d1	sovětská
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
pokud	pokud	k8xS	pokud
bude	být	k5eAaImBp3nS	být
sovětské	sovětský	k2eAgNnSc1d1	sovětské
Rusko	Rusko	k1gNnSc1	Rusko
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
do	do	k7c2	do
světového	světový	k2eAgNnSc2d1	světové
dění	dění	k1gNnSc2	dění
<g/>
,	,	kIx,	,
budou	být	k5eAaImBp3nP	být
zmírněny	zmírnit	k5eAaPmNgFnP	zmírnit
nejhorší	zlý	k2eAgFnPc1d3	nejhorší
stránky	stránka	k1gFnPc1	stránka
bolševismu	bolševismus	k1gInSc2	bolševismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přes	přes	k7c4	přes
počáteční	počáteční	k2eAgFnPc4d1	počáteční
neshody	neshoda	k1gFnPc4	neshoda
s	s	k7c7	s
bílou	bílý	k2eAgFnSc7d1	bílá
emigrací	emigrace	k1gFnSc7	emigrace
se	se	k3xPyFc4	se
Berďajev	Berďajev	k1gMnSc1	Berďajev
rychle	rychle	k6eAd1	rychle
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
kulturního	kulturní	k2eAgInSc2d1	kulturní
života	život	k1gInSc2	život
diaspory	diaspora	k1gFnSc2	diaspora
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
v	v	k7c6	v
založení	založení	k1gNnSc6	založení
takzvaného	takzvaný	k2eAgInSc2d1	takzvaný
Ruského	ruský	k2eAgInSc2d1	ruský
vědeckého	vědecký	k2eAgInSc2d1	vědecký
institutu	institut	k1gInSc2	institut
už	už	k6eAd1	už
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
platforma	platforma	k1gFnSc1	platforma
pro	pro	k7c4	pro
kulturní	kulturní	k2eAgInSc4d1	kulturní
a	a	k8xC	a
vědecký	vědecký	k2eAgInSc4d1	vědecký
život	život	k1gInSc4	život
ruské	ruský	k2eAgFnSc2d1	ruská
inteligence	inteligence	k1gFnSc2	inteligence
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
a	a	k8xC	a
Berďajev	Berďajev	k1gMnSc1	Berďajev
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
děkanů	děkan	k1gMnPc2	děkan
institutu	institut	k1gInSc2	institut
<g/>
.	.	kIx.	.
</s>
<s>
Vedl	vést	k5eAaImAgInS	vést
kurzy	kurz	k1gInPc4	kurz
historie	historie	k1gFnSc2	historie
ruského	ruský	k2eAgNnSc2d1	ruské
myšlení	myšlení	k1gNnSc2	myšlení
a	a	k8xC	a
kurzy	kurz	k1gInPc4	kurz
etiky	etika	k1gFnSc2	etika
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
ještě	ještě	k9	ještě
další	další	k2eAgFnPc4d1	další
instituce	instituce	k1gFnPc4	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
Religiózně-filozofická	Religiózněilozofický	k2eAgFnSc1d1	Religiózně-filozofický
akademie	akademie	k1gFnSc1	akademie
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
vznik	vznik	k1gInSc4	vznik
inicioval	iniciovat	k5eAaBmAgMnS	iniciovat
sám	sám	k3xTgMnSc1	sám
Berďajev	Berďajev	k1gMnSc1	Berďajev
za	za	k7c2	za
podpory	podpora	k1gFnSc2	podpora
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
působící	působící	k2eAgInSc1d1	působící
americké	americký	k2eAgFnSc3d1	americká
organizaci	organizace	k1gFnSc3	organizace
YMCA	YMCA	kA	YMCA
<g/>
.	.	kIx.	.
</s>
<s>
Filozof	filozof	k1gMnSc1	filozof
akademii	akademie	k1gFnSc4	akademie
chápal	chápat	k5eAaImAgMnS	chápat
jako	jako	k8xC	jako
pokračovatelku	pokračovatelka	k1gFnSc4	pokračovatelka
předrevoluční	předrevoluční	k2eAgFnSc2d1	předrevoluční
moskevské	moskevský	k2eAgFnSc2d1	Moskevská
Svobodné	svobodný	k2eAgFnSc2d1	svobodná
akademie	akademie	k1gFnSc2	akademie
duchovní	duchovní	k2eAgFnSc2d1	duchovní
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Akademie	akademie	k1gFnSc1	akademie
pořádala	pořádat	k5eAaImAgFnS	pořádat
každý	každý	k3xTgInSc4	každý
měsíc	měsíc	k1gInSc4	měsíc
cyklus	cyklus	k1gInSc1	cyklus
přednášek	přednáška	k1gFnPc2	přednáška
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
často	často	k6eAd1	často
týkaly	týkat	k5eAaImAgInP	týkat
osudů	osud	k1gInPc2	osud
Ruska	Rusko	k1gNnSc2	Rusko
po	po	k7c6	po
bolševické	bolševický	k2eAgFnSc6d1	bolševická
revoluci	revoluce	k1gFnSc6	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgInSc1	první
sjezd	sjezd	k1gInSc1	sjezd
akademie	akademie	k1gFnSc2	akademie
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
Přerově	Přerov	k1gInSc6	Přerov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Berďajev	Berďajet	k5eAaPmDgInS	Berďajet
se	se	k3xPyFc4	se
i	i	k9	i
díky	díky	k7c3	díky
svému	svůj	k3xOyFgNnSc3	svůj
až	až	k9	až
nerusky	rusky	k6eNd1	rusky
vypjatému	vypjatý	k2eAgInSc3d1	vypjatý
individualismu	individualismus	k1gInSc3	individualismus
necítil	cítit	k5eNaImAgMnS	cítit
mezi	mezi	k7c7	mezi
krajany	krajan	k1gMnPc7	krajan
příliš	příliš	k6eAd1	příliš
dobře	dobře	k6eAd1	dobře
<g/>
.	.	kIx.	.
</s>
<s>
Snáze	snadno	k6eAd2	snadno
si	se	k3xPyFc3	se
myšlenkové	myšlenkový	k2eAgInPc1d1	myšlenkový
spřízněnce	spřízněnec	k1gMnSc4	spřízněnec
získával	získávat	k5eAaImAgInS	získávat
mezi	mezi	k7c7	mezi
domácími	domácí	k2eAgMnPc7d1	domácí
německými	německý	k2eAgMnPc7d1	německý
mysliteli	myslitel	k1gMnPc7	myslitel
<g/>
.	.	kIx.	.
</s>
<s>
Spřátelil	spřátelit	k5eAaPmAgMnS	spřátelit
se	se	k3xPyFc4	se
například	například	k6eAd1	například
s	s	k7c7	s
významným	významný	k2eAgMnSc7d1	významný
německým	německý	k2eAgMnSc7d1	německý
filozofem	filozof	k1gMnSc7	filozof
Maxem	Max	k1gMnSc7	Max
Schelerem	Scheler	k1gMnSc7	Scheler
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
s	s	k7c7	s
Hermannem	Hermann	k1gMnSc7	Hermann
Keyserlingem	Keyserling	k1gInSc7	Keyserling
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
též	též	k9	též
s	s	k7c7	s
Oswaldem	Oswald	k1gMnSc7	Oswald
Spenglerem	Spengler	k1gMnSc7	Spengler
<g/>
,	,	kIx,	,
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
duchů	duch	k1gMnPc2	duch
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
velkým	velký	k2eAgMnSc7d1	velký
inspirátorem	inspirátor	k1gMnSc7	inspirátor
ruského	ruský	k2eAgNnSc2d1	ruské
myšlení	myšlení	k1gNnSc2	myšlení
v	v	k7c6	v
emigraci	emigrace	k1gFnSc6	emigrace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
očích	oko	k1gNnPc6	oko
těchto	tento	k3xDgMnPc2	tento
mužů	muž	k1gMnPc2	muž
byl	být	k5eAaImAgMnS	být
Berďajev	Berďajev	k1gMnSc1	Berďajev
jediným	jediný	k2eAgMnSc7d1	jediný
ruským	ruský	k2eAgMnSc7d1	ruský
filozofem	filozof	k1gMnSc7	filozof
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
Evropana	Evropan	k1gMnSc4	Evropan
<g/>
.	.	kIx.	.
</s>
<s>
Berďajev	Berďajet	k5eAaPmDgInS	Berďajet
byl	být	k5eAaImAgInS	být
jakoby	jakoby	k8xS	jakoby
rozkročen	rozkročen	k2eAgInSc4d1	rozkročen
mezi	mezi	k7c7	mezi
východem	východ	k1gInSc7	východ
a	a	k8xC	a
západem	západ	k1gInSc7	západ
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
svým	svůj	k3xOyFgFnPc3	svůj
zkušenostem	zkušenost	k1gFnPc3	zkušenost
dokázal	dokázat	k5eAaPmAgMnS	dokázat
postihnout	postihnout	k5eAaPmF	postihnout
rozdíly	rozdíl	k1gInPc4	rozdíl
mezi	mezi	k7c7	mezi
duchem	duch	k1gMnSc7	duch
"	"	kIx"	"
<g/>
Západu	západ	k1gInSc2	západ
<g/>
"	"	kIx"	"
a	a	k8xC	a
duchem	duch	k1gMnSc7	duch
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Proto	proto	k8xC	proto
v	v	k7c6	v
ruské	ruský	k2eAgFnSc6d1	ruská
přírodě	příroda	k1gFnSc6	příroda
<g/>
,	,	kIx,	,
v	v	k7c6	v
ruských	ruský	k2eAgInPc6d1	ruský
domech	dům	k1gInPc6	dům
<g/>
,	,	kIx,	,
v	v	k7c6	v
ruských	ruský	k2eAgMnPc6d1	ruský
lidech	člověk	k1gMnPc6	člověk
jsem	být	k5eAaImIp1nS	být
často	často	k6eAd1	často
cítil	cítit	k5eAaImAgMnS	cítit
děsivost	děsivost	k1gFnSc4	děsivost
<g/>
,	,	kIx,	,
tajemno	tajemno	k1gNnSc4	tajemno
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
necítím	cítit	k5eNaImIp1nS	cítit
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
elementární	elementární	k2eAgMnPc1d1	elementární
duchové	duch	k1gMnPc1	duch
spoutáni	spoután	k2eAgMnPc1d1	spoután
a	a	k8xC	a
zakryti	zakryt	k2eAgMnPc1d1	zakryt
civilizací	civilizace	k1gFnSc7	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc1d1	západní
duše	duše	k1gFnSc1	duše
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
racionalizovaná	racionalizovaný	k2eAgFnSc1d1	racionalizovaná
<g/>
,	,	kIx,	,
uspořádaná	uspořádaný	k2eAgFnSc1d1	uspořádaná
<g/>
,	,	kIx,	,
organizovaná	organizovaný	k2eAgFnSc1d1	organizovaná
rozumem	rozum	k1gInSc7	rozum
umělé	umělý	k2eAgFnSc2d1	umělá
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
,	,	kIx,	,
než	než	k8xS	než
ruská	ruský	k2eAgFnSc1d1	ruská
duše	duše	k1gFnSc1	duše
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
vždy	vždy	k6eAd1	vždy
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
iracionální	iracionální	k2eAgMnSc1d1	iracionální
<g/>
,	,	kIx,	,
neorganizovaný	organizovaný	k2eNgInSc1d1	neorganizovaný
a	a	k8xC	a
neuspořádaný	uspořádaný	k2eNgInSc1d1	neuspořádaný
prvek	prvek	k1gInSc1	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
ruský	ruský	k2eAgInSc1d1	ruský
duševní	duševní	k2eAgInSc1d1	duševní
život	život	k1gInSc1	život
více	hodně	k6eAd2	hodně
vyjádřen	vyjádřit	k5eAaPmNgInS	vyjádřit
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vyjádřen	vyjádřit	k5eAaPmNgInS	vyjádřit
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
krajních	krajní	k2eAgInPc6d1	krajní
prvcích	prvek	k1gInPc6	prvek
<g/>
,	,	kIx,	,
než	než	k8xS	než
duševní	duševní	k2eAgInSc1d1	duševní
život	život	k1gInSc1	život
západního	západní	k2eAgMnSc2d1	západní
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
zakrytý	zakrytý	k2eAgInSc1d1	zakrytý
a	a	k8xC	a
potlačený	potlačený	k2eAgInSc1d1	potlačený
normami	norma	k1gFnPc7	norma
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgMnPc1d1	západní
lidé	člověk	k1gMnPc1	člověk
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
tímto	tento	k3xDgNnSc7	tento
ohromeni	ohromit	k5eAaPmNgMnP	ohromit
<g/>
,	,	kIx,	,
když	když	k8xS	když
čtou	číst	k5eAaImIp3nP	číst
ruskou	ruský	k2eAgFnSc4d1	ruská
literaturu	literatura	k1gFnSc4	literatura
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
Dostojevského	Dostojevský	k2eAgNnSc2d1	Dostojevský
a	a	k8xC	a
Tolstého	Tolstý	k2eAgNnSc2d1	Tolstý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
Turgeněva	Turgeněv	k1gMnSc4	Turgeněv
a	a	k8xC	a
Čechova	Čechov	k1gMnSc4	Čechov
<g/>
.	.	kIx.	.
</s>
<s>
Rusové	Rus	k1gMnPc1	Rus
jsou	být	k5eAaImIp3nP	být
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
sociabilní	sociabilní	k2eAgMnSc1d1	sociabilní
(	(	kIx(	(
<g/>
ne	ne	k9	ne
sociální	sociální	k2eAgMnPc1d1	sociální
v	v	k7c6	v
normativním	normativní	k2eAgInSc6d1	normativní
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
mají	mít	k5eAaImIp3nP	mít
větší	veliký	k2eAgInSc4d2	veliký
sklon	sklon	k1gInSc4	sklon
a	a	k8xC	a
schopnost	schopnost	k1gFnSc4	schopnost
ke	k	k7c3	k
stýkání	stýkání	k1gNnSc3	stýkání
se	se	k3xPyFc4	se
než	než	k8xS	než
lidé	člověk	k1gMnPc1	člověk
západní	západní	k2eAgFnSc2d1	západní
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Rusům	Rus	k1gMnPc3	Rus
chybí	chybit	k5eAaPmIp3nS	chybit
podmíněnost	podmíněnost	k1gFnSc1	podmíněnost
ve	v	k7c6	v
stýkání	stýkání	k1gNnSc6	stýkání
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
potřebu	potřeba	k1gFnSc4	potřeba
vidět	vidět	k5eAaImF	vidět
se	se	k3xPyFc4	se
nejen	nejen	k6eAd1	nejen
s	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
s	s	k7c7	s
dobrými	dobrý	k2eAgMnPc7d1	dobrý
známými	známý	k1gMnPc7	známý
<g/>
,	,	kIx,	,
dělit	dělit	k5eAaImF	dělit
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
o	o	k7c4	o
myšlenky	myšlenka	k1gFnPc4	myšlenka
a	a	k8xC	a
prožitky	prožitek	k1gInPc4	prožitek
<g/>
,	,	kIx,	,
přít	přít	k5eAaImF	přít
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Paříž	Paříž	k1gFnSc1	Paříž
===	===	k?	===
</s>
</p>
<p>
<s>
Do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
přijel	přijet	k5eAaPmAgMnS	přijet
filozof	filozof	k1gMnSc1	filozof
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
a	a	k8xC	a
ihned	ihned	k6eAd1	ihned
navázal	navázat	k5eAaPmAgInS	navázat
na	na	k7c4	na
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterou	který	k3yRgFnSc4	který
začal	začít	k5eAaPmAgMnS	začít
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Religiózně-filozofická	Religiózněilozofický	k2eAgFnSc1d1	Religiózně-filozofický
akademie	akademie	k1gFnSc1	akademie
byla	být	k5eAaImAgFnS	být
přemístěna	přemístit	k5eAaPmNgFnS	přemístit
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
činnost	činnost	k1gFnSc1	činnost
byla	být	k5eAaImAgFnS	být
záhy	záhy	k6eAd1	záhy
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
Berďajev	Berďajev	k1gMnSc1	Berďajev
začal	začít	k5eAaPmAgMnS	začít
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
akademie	akademie	k1gFnSc2	akademie
vydávat	vydávat	k5eAaImF	vydávat
časopis	časopis	k1gInSc1	časopis
"	"	kIx"	"
<g/>
П	П	k?	П
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Cesta	cesta	k1gFnSc1	cesta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
bází	báze	k1gFnSc7	báze
pro	pro	k7c4	pro
vyjádření	vyjádření	k1gNnSc4	vyjádření
názorů	názor	k1gInPc2	názor
ruských	ruský	k2eAgMnPc2d1	ruský
náboženských	náboženský	k2eAgMnPc2d1	náboženský
myslitelů	myslitel	k1gMnPc2	myslitel
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
byl	být	k5eAaImAgInS	být
vydáván	vydávat	k5eAaPmNgInS	vydávat
plných	plný	k2eAgNnPc2d1	plné
14	[number]	k4	14
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Paříž	Paříž	k1gFnSc1	Paříž
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
Francie	Francie	k1gFnSc1	Francie
padla	padnout	k5eAaPmAgFnS	padnout
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
nacistů	nacista	k1gMnPc2	nacista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lesley	Lesle	k2eAgFnPc1d1	Lesle
Chamberlainová	Chamberlainová	k1gFnSc1	Chamberlainová
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Parník	parník	k1gInSc1	parník
filozofů	filozof	k1gMnPc2	filozof
prezentuje	prezentovat	k5eAaBmIp3nS	prezentovat
i	i	k9	i
některá	některý	k3yIgNnPc4	některý
statistická	statistický	k2eAgNnPc4d1	statistické
data	datum	k1gNnPc4	datum
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
týkají	týkat	k5eAaImIp3nP	týkat
počtu	počet	k1gInSc2	počet
emigrantů	emigrant	k1gMnPc2	emigrant
ruského	ruský	k2eAgInSc2d1	ruský
původu	původ	k1gInSc2	původ
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Chamberlainová	Chamberlainová	k1gFnSc1	Chamberlainová
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1921	[number]	k4	1921
a	a	k8xC	a
1931	[number]	k4	1931
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
členů	člen	k1gMnPc2	člen
ruské	ruský	k2eAgFnSc2d1	ruská
diaspory	diaspora	k1gFnSc2	diaspora
zdvojnásobil	zdvojnásobit	k5eAaPmAgInS	zdvojnásobit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
oficiálních	oficiální	k2eAgInPc2d1	oficiální
údajů	údaj	k1gInPc2	údaj
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
podhodnocené	podhodnocený	k2eAgNnSc1d1	podhodnocené
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
tato	tento	k3xDgFnSc1	tento
diaspora	diaspora	k1gFnSc1	diaspora
až	až	k9	až
82	[number]	k4	82
000	[number]	k4	000
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Příchodem	příchod	k1gInSc7	příchod
první	první	k4xOgFnSc2	první
vlny	vlna	k1gFnSc2	vlna
emigrantů	emigrant	k1gMnPc2	emigrant
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
SSSR	SSSR	kA	SSSR
se	se	k3xPyFc4	se
společenský	společenský	k2eAgInSc4d1	společenský
život	život	k1gInSc4	život
ruské	ruský	k2eAgFnSc2d1	ruská
diaspory	diaspora	k1gFnSc2	diaspora
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
jiných	jiný	k2eAgNnPc6d1	jiné
emigrantských	emigrantský	k2eAgNnPc6d1	emigrantské
centrech	centrum	k1gNnPc6	centrum
ruské	ruský	k2eAgFnSc2d1	ruská
inteligence	inteligence	k1gFnSc2	inteligence
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
byl	být	k5eAaImAgInS	být
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
<g/>
,	,	kIx,	,
Berlín	Berlín	k1gInSc1	Berlín
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
New	New	k1gMnSc1	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
výše	výše	k1gFnSc2	výše
zmiňované	zmiňovaný	k2eAgFnSc2d1	zmiňovaná
Berďajevovy	Berďajevův	k2eAgFnSc2d1	Berďajevova
Religiozně-filozofické	Religiozněilozofický	k2eAgFnSc2d1	Religiozně-filozofický
akademie	akademie	k1gFnSc2	akademie
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
i	i	k9	i
Francouzsko-ruská	francouzskouský	k2eAgFnSc1d1	francouzsko-ruská
univerzita	univerzita	k1gFnSc1	univerzita
a	a	k8xC	a
Pravoslavný	pravoslavný	k2eAgInSc1d1	pravoslavný
teologický	teologický	k2eAgInSc1d1	teologický
institut	institut	k1gInSc1	institut
a	a	k8xC	a
seminář	seminář	k1gInSc1	seminář
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
univerzita	univerzita	k1gFnSc1	univerzita
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
pouze	pouze	k6eAd1	pouze
rok	rok	k1gInSc4	rok
po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
ruských	ruský	k2eAgMnPc2d1	ruský
emigrantů	emigrant	k1gMnPc2	emigrant
z	z	k7c2	z
Berlína	Berlín	k1gInSc2	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
strach	strach	k1gInSc1	strach
z	z	k7c2	z
budoucích	budoucí	k2eAgInPc2d1	budoucí
osudů	osud	k1gInPc2	osud
výmarské	výmarský	k2eAgFnSc2d1	Výmarská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
uspíšil	uspíšit	k5eAaPmAgMnS	uspíšit
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
řady	řada	k1gFnSc2	řada
emigrantů	emigrant	k1gMnPc2	emigrant
o	o	k7c6	o
přesídlení	přesídlení	k1gNnSc6	přesídlení
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
a	a	k8xC	a
jiných	jiný	k2eAgNnPc2d1	jiné
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
odchodu	odchod	k1gInSc2	odchod
z	z	k7c2	z
Berlína	Berlín	k1gInSc2	Berlín
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
Chamberlainová	Chamberlainový	k2eAgNnPc1d1	Chamberlainový
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgNnP	být
pro	pro	k7c4	pro
mnohé	mnohé	k1gNnSc4	mnohé
také	také	k9	také
inflace	inflace	k1gFnSc2	inflace
německé	německý	k2eAgFnSc2d1	německá
měny	měna	k1gFnSc2	měna
<g/>
.	.	kIx.	.
</s>
<s>
Emigranti	emigrant	k1gMnPc1	emigrant
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
úsporami	úspora	k1gFnPc7	úspora
vyžili	vyžít	k5eAaPmAgMnP	vyžít
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
stále	stále	k6eAd1	stále
obtížněji	obtížně	k6eAd2	obtížně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
inteligence	inteligence	k1gFnSc1	inteligence
zakládala	zakládat	k5eAaImAgFnS	zakládat
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
školy	škola	k1gFnSc2	škola
všech	všecek	k3xTgInPc2	všecek
stupňů	stupeň	k1gInPc2	stupeň
především	především	k9	především
pro	pro	k7c4	pro
posluchače	posluchač	k1gMnPc4	posluchač
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
etablovali	etablovat	k5eAaBmAgMnP	etablovat
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
emigrantských	emigrantský	k2eAgInPc2d1	emigrantský
kruhů	kruh	k1gInPc2	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc4	tento
školy	škola	k1gFnPc4	škola
navštěvovali	navštěvovat	k5eAaImAgMnP	navštěvovat
především	především	k6eAd1	především
děti	dítě	k1gFnPc4	dítě
emigrantů	emigrant	k1gMnPc2	emigrant
narozených	narozený	k2eAgMnPc2d1	narozený
ve	v	k7c6	v
vlasti	vlast	k1gFnSc6	vlast
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
diaspoře	diaspora	k1gFnSc6	diaspora
<g/>
.	.	kIx.	.
<g/>
I	i	k9	i
francouzské	francouzský	k2eAgFnSc2d1	francouzská
elity	elita	k1gFnSc2	elita
si	se	k3xPyFc3	se
vážily	vážit	k5eAaImAgInP	vážit
ruských	ruský	k2eAgMnPc2d1	ruský
emigrantů	emigrant	k1gMnPc2	emigrant
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc1d3	nejznámější
francouzská	francouzský	k2eAgFnSc1d1	francouzská
univerzita	univerzita	k1gFnSc1	univerzita
Sorbonne	Sorbonn	k1gInSc5	Sorbonn
přijímala	přijímat	k5eAaImAgFnS	přijímat
některé	některý	k3yIgMnPc4	některý
emigranty	emigrant	k1gMnPc4	emigrant
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
následně	následně	k6eAd1	následně
vedli	vést	k5eAaImAgMnP	vést
přednášky	přednáška	k1gFnPc4	přednáška
a	a	k8xC	a
semináře	seminář	k1gInPc4	seminář
týkající	týkající	k2eAgInPc4d1	týkající
se	se	k3xPyFc4	se
ruských	ruský	k2eAgFnPc2d1	ruská
dějin	dějiny	k1gFnPc2	dějiny
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
filozofie	filozofie	k1gFnSc2	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
Sorbonne	Sorbonnout	k5eAaImIp3nS	Sorbonnout
organizovala	organizovat	k5eAaBmAgFnS	organizovat
i	i	k8xC	i
mnoho	mnoho	k4c4	mnoho
veřejně	veřejně	k6eAd1	veřejně
přístupných	přístupný	k2eAgFnPc2d1	přístupná
přednášek	přednáška	k1gFnPc2	přednáška
na	na	k7c6	na
kterých	který	k3yIgInPc6	který
prezentovali	prezentovat	k5eAaBmAgMnP	prezentovat
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
ruští	ruský	k2eAgMnPc1d1	ruský
emigranti	emigrant	k1gMnPc1	emigrant
vzdělaní	vzdělaný	k2eAgMnPc1d1	vzdělaný
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
oborech	obor	k1gInPc6	obor
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Témata	téma	k1gNnPc4	téma
ruských	ruský	k2eAgFnPc2d1	ruská
přednášek	přednáška	k1gFnPc2	přednáška
o	o	k7c6	o
historicko-filozofických	historickoilozofický	k2eAgFnPc6d1	historicko-filozofická
otázkách	otázka	k1gFnPc6	otázka
na	na	k7c6	na
Sorbonne	Sorbonn	k1gInSc5	Sorbonn
byla	být	k5eAaImAgNnP	být
rozmanitá	rozmanitý	k2eAgFnSc1d1	rozmanitá
a	a	k8xC	a
lákavá	lákavý	k2eAgFnSc1d1	lákavá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
akademickém	akademický	k2eAgInSc6d1	akademický
roce	rok	k1gInSc6	rok
1924-1925	[number]	k4	1924-1925
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgFnP	být
mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgFnPc1d1	jiná
přednášky	přednáška	k1gFnPc1	přednáška
Lva	lev	k1gInSc2	lev
Šestova	Šestův	k2eAgInSc2d1	Šestův
o	o	k7c6	o
Pascalovi	Pascal	k1gMnSc6	Pascal
a	a	k8xC	a
Dostojevském	Dostojevský	k2eAgMnSc6d1	Dostojevský
či	či	k8xC	či
Alexandra	Alexandra	k1gFnSc1	Alexandra
Koyré	Koyrý	k2eAgNnSc1d1	Koyré
o	o	k7c6	o
"	"	kIx"	"
<g/>
Vladimiru	Vladimir	k1gMnSc3	Vladimir
Solovjovovi	Solovjova	k1gMnSc3	Solovjova
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
současnících	současník	k1gMnPc6	současník
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
týdnu	týden	k1gInSc6	týden
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Konstantin	Konstantin	k1gMnSc1	Konstantin
Močulskij	Močulskij	k1gMnSc1	Močulskij
přednášel	přednášet	k5eAaImAgMnS	přednášet
o	o	k7c4	o
"	"	kIx"	"
<g/>
Puškinovi	Puškin	k1gMnSc3	Puškin
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
plejádě	plejáda	k1gFnSc3	plejáda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
jevištní	jevištní	k2eAgNnSc1d1	jevištní
představení	představení	k1gNnSc1	představení
Tolstého	Tolstý	k2eAgInSc2d1	Tolstý
posledního	poslední	k2eAgInSc2d1	poslední
románu	román	k1gInSc2	román
Vzkříšení	vzkříšení	k1gNnSc2	vzkříšení
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Berďajev	Berďajev	k1gMnSc1	Berďajev
pocházel	pocházet	k5eAaImAgMnS	pocházet
ze	z	k7c2	z
vzdělané	vzdělaný	k2eAgFnSc2d1	vzdělaná
důstojnické	důstojnický	k2eAgFnSc2d1	důstojnická
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc7	jeho
starším	starý	k2eAgMnSc7d2	starší
bratrem	bratr	k1gMnSc7	bratr
byl	být	k5eAaImAgMnS	být
Serhij	Serhij	k1gMnSc1	Serhij
Berďajev	Berďajev	k1gMnSc1	Berďajev
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
četl	číst	k5eAaImAgMnS	číst
Hegela	Hegel	k1gMnSc4	Hegel
<g/>
,	,	kIx,	,
Schopenhauera	Schopenhauer	k1gMnSc4	Schopenhauer
a	a	k8xC	a
Kanta	Kant	k1gMnSc4	Kant
a	a	k8xC	a
naučil	naučit	k5eAaPmAgMnS	naučit
se	se	k3xPyFc4	se
řadu	řad	k1gInSc2	řad
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1894	[number]	k4	1894
začal	začít	k5eAaPmAgInS	začít
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Kyjevě	Kyjev	k1gInSc6	Kyjev
a	a	k8xC	a
jako	jako	k8xC	jako
mnoho	mnoho	k4c1	mnoho
jeho	jeho	k3xOp3gMnPc2	jeho
současníků	současník	k1gMnPc2	současník
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
marxistou	marxista	k1gMnSc7	marxista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
byl	být	k5eAaImAgMnS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
<g/>
,	,	kIx,	,
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
ze	z	k7c2	z
studia	studio	k1gNnSc2	studio
a	a	k8xC	a
strávil	strávit	k5eAaPmAgMnS	strávit
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
ve	v	k7c6	v
vyhnanství	vyhnanství	k1gNnSc6	vyhnanství
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
a	a	k8xC	a
usadil	usadit	k5eAaPmAgMnS	usadit
se	se	k3xPyFc4	se
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
pilně	pilně	k6eAd1	pilně
účastnil	účastnit	k5eAaImAgMnS	účastnit
intelektuálního	intelektuální	k2eAgInSc2d1	intelektuální
života	život	k1gInSc2	život
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
zabývat	zabývat	k5eAaImF	zabývat
filosofií	filosofie	k1gFnSc7	filosofie
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Patřil	patřit	k5eAaImAgInS	patřit
ke	k	k7c3	k
skupině	skupina	k1gFnSc3	skupina
ruských	ruský	k2eAgMnPc2d1	ruský
intelektuálů	intelektuál	k1gMnPc2	intelektuál
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
sborníkem	sborník	k1gInSc7	sborník
Věchi	Věch	k1gMnPc1	Věch
přihlásili	přihlásit	k5eAaPmAgMnP	přihlásit
ke	k	k7c3	k
křesťanství	křesťanství	k1gNnSc3	křesťanství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
Vasilij	Vasilij	k1gMnSc1	Vasilij
Zeňkovskij	Zeňkovskij	k1gMnSc1	Zeňkovskij
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
Dějinách	dějiny	k1gFnPc6	dějiny
ruské	ruský	k2eAgFnSc2d1	ruská
filosofie	filosofie	k1gFnSc2	filosofie
analyzoval	analyzovat	k5eAaImAgMnS	analyzovat
Berďajevovy	Berďajevův	k2eAgInPc4d1	Berďajevův
filosofické	filosofický	k2eAgInPc4d1	filosofický
názory	názor	k1gInPc4	názor
<g/>
,	,	kIx,	,
rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
jeho	jeho	k3xOp3gFnSc4	jeho
tvorbu	tvorba	k1gFnSc4	tvorba
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
period	perioda	k1gFnPc2	perioda
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
první	první	k4xOgNnSc4	první
období	období	k1gNnPc2	období
Berďajevovy	Berďajevův	k2eAgFnSc2d1	Berďajevova
tvorby	tvorba	k1gFnSc2	tvorba
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
etické	etický	k2eAgNnSc4d1	etické
téma	téma	k1gNnSc4	téma
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
období	období	k1gNnSc1	období
je	být	k5eAaImIp3nS	být
poznamenáno	poznamenat	k5eAaPmNgNnS	poznamenat
nábožensko-mystickým	náboženskoystický	k2eAgInSc7d1	nábožensko-mystický
přelomem	přelom	k1gInSc7	přelom
v	v	k7c6	v
Berďajevově	Berďajevův	k2eAgInSc6d1	Berďajevův
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
náboženská	náboženský	k2eAgFnSc1d1	náboženská
motivace	motivace	k1gFnSc1	motivace
stává	stávat	k5eAaImIp3nS	stávat
trvalou	trvalý	k2eAgFnSc7d1	trvalá
součástí	součást	k1gFnSc7	součást
jeho	jeho	k3xOp3gFnSc2	jeho
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetím	třetí	k4xOgNnSc6	třetí
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
u	u	k7c2	u
Berďajeva	Berďajev	k1gMnSc2	Berďajev
projevuje	projevovat	k5eAaImIp3nS	projevovat
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
historiosofické	historiosofický	k2eAgNnSc4d1	historiosofický
téma	téma	k1gNnSc4	téma
a	a	k8xC	a
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
filosofii	filosofie	k1gFnSc4	filosofie
dějin	dějiny	k1gFnPc2	dějiny
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
silný	silný	k2eAgInSc1d1	silný
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
především	především	k6eAd1	především
eschatologií	eschatologie	k1gFnPc2	eschatologie
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
svůj	svůj	k3xOyFgInSc4	svůj
originální	originální	k2eAgInSc4d1	originální
koncept	koncept	k1gInSc4	koncept
"	"	kIx"	"
<g/>
eschatologické	eschatologický	k2eAgFnSc2d1	eschatologická
metafyziky	metafyzika	k1gFnSc2	metafyzika
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc4d1	poslední
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
období	období	k1gNnSc4	období
je	být	k5eAaImIp3nS	být
spjato	spjat	k2eAgNnSc1d1	spjato
s	s	k7c7	s
personalistickým	personalistický	k2eAgNnSc7d1	personalistické
myšlením	myšlení	k1gNnSc7	myšlení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
základem	základ	k1gInSc7	základ
Berďajevovy	Berďajevův	k2eAgFnSc2d1	Berďajevova
personalistické	personalistický	k2eAgFnSc2d1	personalistická
etiky	etika	k1gFnSc2	etika
a	a	k8xC	a
antropologie	antropologie	k1gFnSc2	antropologie
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
těchto	tento	k3xDgInPc2	tento
čtyř	čtyři	k4xCgInPc2	čtyři
aspektů	aspekt	k1gInPc2	aspekt
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
Zeňkovskij	Zeňkovskij	k1gMnPc1	Zeňkovskij
dvě	dva	k4xCgFnPc4	dva
centrální	centrální	k2eAgFnPc4d1	centrální
Berďajevovy	Berďajevův	k2eAgFnPc4d1	Berďajevova
myšlenky	myšlenka	k1gFnPc4	myšlenka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
základními	základní	k2eAgInPc7d1	základní
stavebními	stavební	k2eAgInPc7d1	stavební
kameny	kámen	k1gInPc7	kámen
celé	celá	k1gFnSc2	celá
jeho	jeho	k3xOp3gFnSc2	jeho
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
...	...	k?	...
princip	princip	k1gInSc1	princip
objektivace	objektivace	k1gFnSc2	objektivace
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc4	druhý
primát	primát	k1gInSc4	primát
svobody	svoboda	k1gFnSc2	svoboda
před	před	k7c7	před
bytím	bytí	k1gNnSc7	bytí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
pravoslavný	pravoslavný	k2eAgMnSc1d1	pravoslavný
křesťan	křesťan	k1gMnSc1	křesťan
zastával	zastávat	k5eAaImAgMnS	zastávat
Berďajev	Berďajev	k1gMnPc3	Berďajev
křesťanský	křesťanský	k2eAgInSc4d1	křesťanský
univerzalismus	univerzalismus	k1gInSc4	univerzalismus
a	a	k8xC	a
několikrát	několikrát	k6eAd1	několikrát
ostře	ostro	k6eAd1	ostro
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
pravoslavnou	pravoslavný	k2eAgFnSc4d1	pravoslavná
církev	církev	k1gFnSc4	církev
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
článek	článek	k1gInSc4	článek
o	o	k7c6	o
Svatém	svatý	k2eAgInSc6d1	svatý
synodu	synod	k1gInSc6	synod
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
byl	být	k5eAaImAgInS	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
rouhání	rouhání	k1gNnSc2	rouhání
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
k	k	k7c3	k
doživotnímu	doživotní	k2eAgNnSc3d1	doživotní
vyhnanství	vyhnanství	k1gNnSc3	vyhnanství
<g/>
,	,	kIx,	,
čemuž	což	k3yRnSc3	což
zabránila	zabránit	k5eAaPmAgFnS	zabránit
Světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
bolševická	bolševický	k2eAgFnSc1d1	bolševická
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
článku	článek	k1gInSc6	článek
Pravda	pravda	k1gFnSc1	pravda
pravoslaví	pravoslaví	k1gNnSc2	pravoslaví
svůj	svůj	k3xOyFgInSc4	svůj
postoj	postoj	k1gInSc4	postoj
charakterizoval	charakterizovat	k5eAaBmAgMnS	charakterizovat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
učitelů	učitel	k1gMnPc2	učitel
Východní	východní	k2eAgFnSc2d1	východní
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
od	od	k7c2	od
Klementa	Klement	k1gMnSc2	Klement
Alexandrijského	alexandrijský	k2eAgMnSc2d1	alexandrijský
po	po	k7c4	po
Maxima	Maxim	k1gMnSc4	Maxim
Vyznavače	vyznavač	k1gMnSc4	vyznavač
<g/>
,	,	kIx,	,
zastávala	zastávat	k5eAaImAgFnS	zastávat
učení	učení	k1gNnSc4	učení
o	o	k7c6	o
obecném	obecný	k2eAgNnSc6d1	obecné
vzkříšení	vzkříšení	k1gNnSc6	vzkříšení
a	a	k8xC	a
spáse	spása	k1gFnSc6	spása
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Pravoslavné	pravoslavný	k2eAgNnSc1d1	pravoslavné
myšlení	myšlení	k1gNnSc1	myšlení
nikdy	nikdy	k6eAd1	nikdy
nepodlehlo	podlehnout	k5eNaPmAgNnS	podlehnout
myšlence	myšlenka	k1gFnSc3	myšlenka
Boží	boží	k2eAgFnSc3d1	boží
spravedlnosti	spravedlnost	k1gFnSc3	spravedlnost
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zapomnělo	zapomnět	k5eAaImAgNnS	zapomnět
na	na	k7c4	na
myšlenku	myšlenka	k1gFnSc4	myšlenka
Boží	boží	k2eAgFnSc2d1	boží
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
nevymezilo	vymezit	k5eNaPmAgNnS	vymezit
člověka	člověk	k1gMnSc4	člověk
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
Boží	boží	k2eAgFnSc2d1	boží
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
proměnění	proměnění	k1gNnSc2	proměnění
a	a	k8xC	a
zbožštění	zbožštění	k1gNnSc2	zbožštění
člověka	člověk	k1gMnSc2	člověk
i	i	k8xC	i
Vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Berďajev	Berďajet	k5eAaPmDgInS	Berďajet
napsal	napsat	k5eAaPmAgMnS	napsat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
25	[number]	k4	25
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
přeloženy	přeložit	k5eAaPmNgFnP	přeložit
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
a	a	k8xC	a
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
řadu	řada	k1gFnSc4	řada
myslitelů	myslitel	k1gMnPc2	myslitel
i	i	k8xC	i
celou	celý	k2eAgFnSc4d1	celá
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
filosofii	filosofie	k1gFnSc4	filosofie
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
i	i	k9	i
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
ale	ale	k9	ale
také	také	k9	také
mnoho	mnoho	k4c1	mnoho
sporů	spor	k1gInPc2	spor
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
s	s	k7c7	s
existencialistickými	existencialistický	k2eAgMnPc7d1	existencialistický
filosofy	filosof	k1gMnPc7	filosof
<g/>
,	,	kIx,	,
tak	tak	k9	tak
s	s	k7c7	s
teology	teolog	k1gMnPc7	teolog
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
myšlenky	myšlenka	k1gFnPc1	myšlenka
svobody	svoboda	k1gFnSc2	svoboda
a	a	k8xC	a
lidské	lidský	k2eAgFnSc2d1	lidská
tvořivosti	tvořivost	k1gFnSc2	tvořivost
i	i	k8xC	i
"	"	kIx"	"
<g/>
kosmická	kosmický	k2eAgFnSc1d1	kosmická
<g/>
"	"	kIx"	"
představa	představa	k1gFnSc1	představa
vzkříšení	vzkříšení	k1gNnSc2	vzkříšení
a	a	k8xC	a
spásy	spása	k1gFnSc2	spása
se	se	k3xPyFc4	se
ujaly	ujmout	k5eAaPmAgFnP	ujmout
jak	jak	k6eAd1	jak
ve	v	k7c6	v
francouzském	francouzský	k2eAgInSc6d1	francouzský
personalismu	personalismus	k1gInSc6	personalismus
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
křesťanském	křesťanský	k2eAgInSc6d1	křesťanský
a	a	k8xC	a
zejména	zejména	k6eAd1	zejména
katolickém	katolický	k2eAgNnSc6d1	katolické
myšlení	myšlení	k1gNnSc6	myšlení
poválečné	poválečný	k2eAgFnSc2d1	poválečná
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
==	==	k?	==
</s>
</p>
<p>
<s>
N.	N.	kA	N.
Berďajev	Berďajev	k1gMnSc1	Berďajev
<g/>
,	,	kIx,	,
Dostojevského	Dostojevský	k2eAgNnSc2d1	Dostojevský
pojetí	pojetí	k1gNnSc2	pojetí
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
OIKOYMENH	OIKOYMENH	kA	OIKOYMENH
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7298-020-3	[number]	k4	80-7298-020-3
</s>
</p>
<p>
<s>
N.	N.	kA	N.
Berďajev	Berďajev	k1gMnSc1	Berďajev
<g/>
,	,	kIx,	,
Duch	duch	k1gMnSc1	duch
a	a	k8xC	a
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
CDK	CDK	kA	CDK
<g/>
:	:	kIx,	:
Brno	Brno	k1gNnSc1	Brno
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
N.	N.	kA	N.
Berďajev	Berďajev	k1gFnSc1	Berďajev
<g/>
,	,	kIx,	,
Duše	duše	k1gFnSc1	duše
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
Petrov	Petrov	k1gInSc1	Petrov
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-85247-39-9	[number]	k4	80-85247-39-9
</s>
</p>
<p>
<s>
N.	N.	kA	N.
Berďajev	Berďajev	k1gFnSc1	Berďajev
<g/>
,	,	kIx,	,
Filosofie	filosofie	k1gFnSc1	filosofie
lidského	lidský	k2eAgInSc2d1	lidský
osudu	osud	k1gInSc2	osud
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
fragmenty	fragment	k1gInPc7	fragment
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
Smysl	smysl	k1gInSc1	smysl
dějin	dějiny	k1gFnPc2	dějiny
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
"	"	kIx"	"
<g/>
Zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
vydání	vydání	k1gNnSc4	vydání
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-85436-32-9	[number]	k4	80-85436-32-9
</s>
</p>
<p>
<s>
N.	N.	kA	N.
Berďajev	Berďajev	k1gFnSc1	Berďajev
<g/>
,	,	kIx,	,
Filosofie	filosofie	k1gFnSc1	filosofie
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Díl	díl	k1gInSc1	díl
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Filosofie	filosofie	k1gFnSc1	filosofie
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
:	:	kIx,	:
Votobia	Votobia	k1gFnSc1	Votobia
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7198-486-8	[number]	k4	80-7198-486-8
</s>
</p>
<p>
<s>
N.	N.	kA	N.
Berďajev	Berďajev	k1gFnSc1	Berďajev
<g/>
,	,	kIx,	,
Filosofie	filosofie	k1gFnSc1	filosofie
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Díl	díl	k1gInSc1	díl
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Původ	původ	k1gInSc1	původ
zla	zlo	k1gNnSc2	zlo
a	a	k8xC	a
smysl	smysl	k1gInSc1	smysl
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
:	:	kIx,	:
Votobia	Votobia	k1gFnSc1	Votobia
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7198-490-6	[number]	k4	80-7198-490-6
</s>
</p>
<p>
<s>
N.	N.	kA	N.
Berďajev	Berďajev	k1gFnSc1	Berďajev
<g/>
,	,	kIx,	,
Filosofie	filosofie	k1gFnSc1	filosofie
svobodného	svobodný	k2eAgMnSc2d1	svobodný
ducha	duch	k1gMnSc2	duch
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
část	část	k1gFnSc1	část
<g/>
.	.	kIx.	.
</s>
<s>
Červený	červený	k2eAgInSc1d1	červený
Kostelec	Kostelec	k1gInSc1	Kostelec
:	:	kIx,	:
Mervart	Mervart	k1gInSc1	Mervart
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-86818-97-9	[number]	k4	978-80-86818-97-9
</s>
</p>
<p>
<s>
N.	N.	kA	N.
Berďajev	Berďajev	k1gFnSc1	Berďajev
<g/>
,	,	kIx,	,
Filosofie	filosofie	k1gFnSc1	filosofie
svobodného	svobodný	k2eAgMnSc2d1	svobodný
ducha	duch	k1gMnSc2	duch
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
část	část	k1gFnSc1	část
<g/>
.	.	kIx.	.
</s>
<s>
Červený	červený	k2eAgInSc1d1	červený
Kostelec	Kostelec	k1gInSc1	Kostelec
:	:	kIx,	:
Mervart	Mervart	k1gInSc1	Mervart
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-86818-98-6	[number]	k4	978-80-86818-98-6
</s>
</p>
<p>
<s>
N.	N.	kA	N.
Berďajev	Berďajev	k1gFnSc1	Berďajev
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
středověk	středověk	k1gInSc1	středověk
:	:	kIx,	:
úvaha	úvaha	k1gFnSc1	úvaha
o	o	k7c6	o
osudu	osud	k1gInSc6	osud
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Červený	červený	k2eAgInSc1d1	červený
Kostelec	Kostelec	k1gInSc1	Kostelec
:	:	kIx,	:
Mervart	Mervart	k1gInSc1	Mervart
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-86818-05-5	[number]	k4	80-86818-05-5
</s>
</p>
<p>
<s>
N.	N.	kA	N.
Berďajev	Berďajev	k1gFnSc1	Berďajev
<g/>
,	,	kIx,	,
O	o	k7c6	o
hodnotě	hodnota	k1gFnSc6	hodnota
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
Větrné	větrný	k2eAgInPc1d1	větrný
Mlýny	mlýn	k1gInPc1	mlýn
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
</s>
</p>
<p>
<s>
N.	N.	kA	N.
Berďajev	Berďajev	k1gFnSc1	Berďajev
<g/>
,	,	kIx,	,
O	o	k7c6	o
otroctví	otroctví	k1gNnSc6	otroctví
a	a	k8xC	a
svobodě	svoboda	k1gFnSc6	svoboda
člověka	člověk	k1gMnSc4	člověk
:	:	kIx,	:
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
personalistickou	personalistický	k2eAgFnSc4d1	personalistická
filosofii	filosofie	k1gFnSc4	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
OIKOYMENH	OIKOYMENH	kA	OIKOYMENH
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-86005-50-X	[number]	k4	80-86005-50-X
</s>
</p>
<p>
<s>
N.	N.	kA	N.
Berďajev	Berďajev	k1gFnPc1	Berďajev
<g/>
,	,	kIx,	,
Říše	říš	k1gFnPc1	říš
ducha	duch	k1gMnSc2	duch
a	a	k8xC	a
říše	říše	k1gFnSc1	říše
císařova	císařův	k2eAgFnSc1d1	císařova
<g/>
.	.	kIx.	.
</s>
<s>
Červený	červený	k2eAgInSc1d1	červený
Kostelec	Kostelec	k1gInSc1	Kostelec
:	:	kIx,	:
Mervart	Mervart	k1gInSc1	Mervart
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-86818-16-0	[number]	k4	80-86818-16-0
</s>
</p>
<p>
<s>
N.	N.	kA	N.
Berďajev	Berďajev	k1gFnSc1	Berďajev
<g/>
,	,	kIx,	,
Ruská	ruský	k2eAgFnSc1d1	ruská
idea	idea	k1gFnSc1	idea
:	:	kIx,	:
základní	základní	k2eAgFnPc4d1	základní
otázky	otázka	k1gFnPc4	otázka
ruského	ruský	k2eAgNnSc2d1	ruské
myšlení	myšlení	k1gNnSc2	myšlení
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
OIKOYMENH	OIKOYMENH	kA	OIKOYMENH
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7298-069-6	[number]	k4	80-7298-069-6
</s>
</p>
<p>
<s>
N.	N.	kA	N.
Berďajev	Berďajev	k1gFnSc1	Berďajev
<g/>
,	,	kIx,	,
Smysl	smysl	k1gInSc1	smysl
dějin	dějiny	k1gFnPc2	dějiny
:	:	kIx,	:
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
filosofii	filosofie	k1gFnSc4	filosofie
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc2	jeho
osudu	osud	k1gInSc2	osud
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
I.	I.	kA	I.
<g/>
S.	S.	kA	S.
<g/>
E.	E.	kA	E.
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
OIKOYMENH	OIKOYMENH	kA	OIKOYMENH
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-85241-91-9	[number]	k4	80-85241-91-9
</s>
</p>
<p>
<s>
N.	N.	kA	N.
Berďajev	Berďajev	k1gFnSc1	Berďajev
<g/>
,	,	kIx,	,
Vlastní	vlastní	k2eAgInSc1d1	vlastní
životopis	životopis	k1gInSc1	životopis
<g/>
.	.	kIx.	.
</s>
<s>
Refugium	refugium	k1gNnSc1	refugium
<g/>
:	:	kIx,	:
Praha	Praha	k1gFnSc1	Praha
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
N.	N.	kA	N.
Berďajev	Berďajev	k1gMnSc1	Berďajev
<g/>
,	,	kIx,	,
S.	S.	kA	S.
Bulgakov	Bulgakov	k1gInSc1	Bulgakov
<g/>
,	,	kIx,	,
M.	M.	kA	M.
Geršenzon	Geršenzon	k1gInSc1	Geršenzon
<g/>
,	,	kIx,	,
A.	A.	kA	A.
Izgojev	Izgojev	k1gFnSc4	Izgojev
<g/>
,	,	kIx,	,
B.	B.	kA	B.
Kisťakovskij	Kisťakovskij	k1gFnSc4	Kisťakovskij
<g/>
,	,	kIx,	,
P.	P.	kA	P.
Struve	Struev	k1gFnPc1	Struev
<g/>
,	,	kIx,	,
S.	S.	kA	S.
Frank	frank	k1gInSc1	frank
<g/>
,	,	kIx,	,
Věchi	Věchi	k1gNnSc1	Věchi
(	(	kIx(	(
<g/>
Milníky	milník	k1gInPc1	milník
<g/>
)	)	kIx)	)
-	-	kIx~	-
Sborník	sborník	k1gInSc1	sborník
článků	článek	k1gInPc2	článek
o	o	k7c6	o
ruské	ruský	k2eAgFnSc6d1	ruská
inteligenci	inteligence	k1gFnSc6	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
Červený	červený	k2eAgInSc1d1	červený
Kostelec	Kostelec	k1gInSc1	Kostelec
:	:	kIx,	:
Mervart	Mervart	k1gInSc1	Mervart
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-903047-8-8	[number]	k4	80-903047-8-8
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Nikolai	Nikola	k1gFnSc2	Nikola
Berdyaev	Berdyava	k1gFnPc2	Berdyava
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
LOSSKIJ	LOSSKIJ	kA	LOSSKIJ
<g/>
,	,	kIx,	,
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Onufrijevič	Onufrijevič	k1gMnSc1	Onufrijevič
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
ruské	ruský	k2eAgFnSc2d1	ruská
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Vyd	Vyd	k?	Vyd
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Velehrad	Velehrad	k1gInSc1	Velehrad
<g/>
:	:	kIx,	:
Refugium	refugium	k1gNnSc1	refugium
Velehrad-Roma	Velehrad-Roma	k1gFnSc1	Velehrad-Roma
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
663	[number]	k4	663
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80-86715-26-4	[number]	k4	80-86715-26-4
[	[	kIx(	[
<g/>
Kapitola	kapitola	k1gFnSc1	kapitola
"	"	kIx"	"
<g/>
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Berďajev	Berďajev	k1gMnSc1	Berďajev
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
str	str	kA	str
<g/>
.	.	kIx.	.
361	[number]	k4	361
<g/>
–	–	k?	–
<g/>
385	[number]	k4	385
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
JABŮREK	Jabůrek	k1gMnSc1	Jabůrek
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Eschatologický	eschatologický	k2eAgInSc1d1	eschatologický
existencialismus	existencialismus	k1gInSc1	existencialismus
Nikolaje	Nikolaj	k1gMnSc2	Nikolaj
Berďajeva	Berďajev	k1gMnSc2	Berďajev
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
CDK	CDK	kA	CDK
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-7325-319-6	[number]	k4	978-80-7325-319-6
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Ekumenismus	ekumenismus	k1gInSc1	ekumenismus
</s>
</p>
<p>
<s>
Personalismus	personalismus	k1gInSc1	personalismus
</s>
</p>
<p>
<s>
Pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Berďajev	Berďajev	k1gFnSc7	Berďajev
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Berďajev	Berďajev	k1gMnSc1	Berďajev
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Berďajev	Berďajev	k1gMnSc1	Berďajev
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bibliografie	bibliografie	k1gFnSc1	bibliografie
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
Berďajevových	Berďajevův	k2eAgInPc2d1	Berďajevův
textů	text	k1gInPc2	text
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
</s>
</p>
<p>
<s>
Overview	Overview	k?	Overview
</s>
</p>
<p>
<s>
Fr.	Fr.	k?	Fr.
Men	Men	k1gFnSc1	Men
on	on	k3xPp3gMnSc1	on
Berdyaev	Berdyaev	k1gFnSc1	Berdyaev
</s>
</p>
<p>
<s>
PHILOSOPHER	PHILOSOPHER	kA	PHILOSOPHER
OF	OF	kA	OF
FREEDOM	FREEDOM	kA	FREEDOM
by	by	kYmCp3nS	by
Dimitri	Dimitri	k1gNnSc4	Dimitri
Lisin	Lisin	k2eAgMnSc1d1	Lisin
</s>
</p>
<p>
<s>
Nikolai	Nikolai	k1gNnSc1	Nikolai
Lossky	Losska	k1gFnSc2	Losska
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
bio	bio	k?	bio
on	on	k3xPp3gMnSc1	on
Berdyaev	Berdyava	k1gFnPc2	Berdyava
</s>
</p>
<p>
<s>
New	New	k?	New
York	York	k1gInSc1	York
Times	Times	k1gInSc1	Times
<g/>
:	:	kIx,	:
Russia	Russia	k1gFnSc1	Russia
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Castaway	Castawa	k1gMnPc7	Castawa
Intellectuals	Intellectuals	k1gInSc4	Intellectuals
in	in	k?	in
Revolution	Revolution	k1gInSc1	Revolution
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Wake	Wake	k1gFnSc7	Wake
</s>
</p>
<p>
<s>
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Berďajev	Berďajev	k1gMnSc1	Berďajev
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
existence-transcendence	existenceranscendence	k1gFnSc2	existence-transcendence
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Russian	Russian	k1gInSc1	Russian
idea	idea	k1gFnSc1	idea
of	of	k?	of
Nikolai	Nikola	k1gFnSc2	Nikola
Berdyaev	Berdyava	k1gFnPc2	Berdyava
Rus	Rus	k1gMnSc1	Rus
<g/>
.	.	kIx.	.
</s>
</p>
