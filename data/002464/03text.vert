<s>
Stéphane	Stéphanout	k5eAaPmIp3nS	Stéphanout
Mallarmé	Mallarmý	k2eAgNnSc1d1	Mallarmé
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1842	[number]	k4	1842
Paříž	Paříž	k1gFnSc1	Paříž
-	-	kIx~	-
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1898	[number]	k4	1898
Valvins	Valvinsa	k1gFnPc2	Valvinsa
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
esejista	esejista	k1gMnSc1	esejista
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
řazen	řadit	k5eAaImNgMnS	řadit
mezi	mezi	k7c4	mezi
prokleté	prokletý	k2eAgMnPc4d1	prokletý
básníky	básník	k1gMnPc4	básník
a	a	k8xC	a
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
nejvýraznějšího	výrazný	k2eAgMnSc4d3	nejvýraznější
představitele	představitel	k1gMnSc4	představitel
symbolismu	symbolismus	k1gInSc2	symbolismus
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
bytě	byt	k1gInSc6	byt
pořádal	pořádat	k5eAaImAgInS	pořádat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
literární	literární	k2eAgInPc4d1	literární
salony	salon	k1gInPc4	salon
(	(	kIx(	(
<g/>
zval	zvát	k5eAaImAgInS	zvát
literáty	literát	k1gMnPc4	literát
<g/>
,	,	kIx,	,
hudebníky	hudebník	k1gMnPc4	hudebník
<g/>
,	,	kIx,	,
malíře	malíř	k1gMnPc4	malíř
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
tvorba	tvorba	k1gFnSc1	tvorba
je	být	k5eAaImIp3nS	být
symbiózou	symbióza	k1gFnSc7	symbióza
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
malířství	malířství	k1gNnSc2	malířství
a	a	k8xC	a
poezie	poezie	k1gFnSc2	poezie
-	-	kIx~	-
verše	verš	k1gInPc1	verš
mají	mít	k5eAaImIp3nP	mít
navodit	navodit	k5eAaBmF	navodit
hudebnost	hudebnost	k1gFnSc4	hudebnost
<g/>
,	,	kIx,	,
grafické	grafický	k2eAgNnSc4d1	grafické
členění	členění	k1gNnSc4	členění
celé	celý	k2eAgFnSc2d1	celá
básně	báseň	k1gFnSc2	báseň
i	i	k8xC	i
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
veršů	verš	k1gInPc2	verš
pak	pak	k6eAd1	pak
výtvarný	výtvarný	k2eAgInSc4d1	výtvarný
dojem	dojem	k1gInSc4	dojem
<g/>
.	.	kIx.	.
</s>
<s>
Báseň	báseň	k1gFnSc1	báseň
má	mít	k5eAaImIp3nS	mít
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
obsah	obsah	k1gInSc4	obsah
či	či	k8xC	či
formu	forma	k1gFnSc4	forma
vyvolávat	vyvolávat	k5eAaImF	vyvolávat
dojmy	dojem	k1gInPc4	dojem
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
důležitou	důležitý	k2eAgFnSc4d1	důležitá
považuje	považovat	k5eAaImIp3nS	považovat
inspiraci	inspirace	k1gFnSc4	inspirace
-	-	kIx~	-
sen	sen	k1gInSc4	sen
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vsugeruje	vsugerovat	k5eAaPmIp3nS	vsugerovat
dojmy	dojem	k1gInPc4	dojem
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Báseň	báseň	k1gFnSc1	báseň
je	být	k5eAaImIp3nS	být
tajemství	tajemství	k1gNnSc4	tajemství
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgNnSc3	jenž
čtenář	čtenář	k1gMnSc1	čtenář
musí	muset	k5eAaImIp3nS	muset
hledat	hledat	k5eAaImF	hledat
klíč	klíč	k1gInSc4	klíč
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Účinek	účinek	k1gInSc1	účinek
básně	báseň	k1gFnSc2	báseň
není	být	k5eNaImIp3nS	být
daný	daný	k2eAgInSc4d1	daný
obsahem	obsah	k1gInSc7	obsah
<g/>
,	,	kIx,	,
formou	forma	k1gFnSc7	forma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dojmem	dojem	k1gInSc7	dojem
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vzbudí	vzbudit	k5eAaPmIp3nS	vzbudit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Mallarmého	Mallarmý	k2eAgInSc2d1	Mallarmý
dílo	dílo	k1gNnSc4	dílo
je	být	k5eAaImIp3nS	být
dílem	dílo	k1gNnSc7	dílo
cizeléra	cizelér	k1gMnSc2	cizelér
-	-	kIx~	-
neprodukoval	produkovat	k5eNaImAgMnS	produkovat
básnické	básnický	k2eAgFnPc4d1	básnická
sbírky	sbírka	k1gFnPc4	sbírka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
básně	báseň	k1gFnPc4	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Mnohá	mnohé	k1gNnPc1	mnohé
díla	dílo	k1gNnSc2	dílo
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
(	(	kIx(	(
<g/>
drama	drama	k1gFnSc1	drama
Igitur	Igitura	k1gFnPc2	Igitura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jiná	jiný	k2eAgFnSc1d1	jiná
přepracovával	přepracovávat	k5eAaImAgMnS	přepracovávat
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
opustil	opustit	k5eAaPmAgMnS	opustit
(	(	kIx(	(
<g/>
drama	drama	k1gFnSc1	drama
Herodias	Herodias	k1gFnSc1	Herodias
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
řada	řada	k1gFnSc1	řada
Mallarmého	Mallarmý	k2eAgInSc2d1	Mallarmý
básní	báseň	k1gFnSc7	báseň
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
sám	sám	k3xTgMnSc1	sám
nazýval	nazývat	k5eAaImAgMnS	nazývat
popěvky	popěvka	k1gFnPc4	popěvka
<g/>
,	,	kIx,	,
a	a	k8xC	a
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
výrazněji	výrazně	k6eAd2	výrazně
neliší	lišit	k5eNaImIp3nP	lišit
od	od	k7c2	od
produkce	produkce	k1gFnSc2	produkce
generace	generace	k1gFnSc2	generace
Parnasistů	parnasista	k1gMnPc2	parnasista
<g/>
.	.	kIx.	.
</s>
<s>
Mallarmé	Mallarmý	k2eAgNnSc1d1	Mallarmé
vycházel	vycházet	k5eAaImAgMnS	vycházet
stylově	stylově	k6eAd1	stylově
z	z	k7c2	z
Baudelaira	Baudelair	k1gInSc2	Baudelair
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc7	jeho
celoživotním	celoživotní	k2eAgInSc7d1	celoživotní
vzorem	vzor	k1gInSc7	vzor
bylo	být	k5eAaImAgNnS	být
ale	ale	k9	ale
dílo	dílo	k1gNnSc1	dílo
Edgara	Edgar	k1gMnSc2	Edgar
Allana	Allan	k1gMnSc2	Allan
Poea	Poeus	k1gMnSc2	Poeus
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
překládal	překládat	k5eAaImAgMnS	překládat
do	do	k7c2	do
francouzštiny	francouzština	k1gFnSc2	francouzština
<g/>
.	.	kIx.	.
</s>
<s>
Oběma	dva	k4xCgMnPc3	dva
autorům	autor	k1gMnPc3	autor
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
vzdálil	vzdálit	k5eAaPmAgInS	vzdálit
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
maximální	maximální	k2eAgFnSc4d1	maximální
prokomponovanost	prokomponovanost	k1gFnSc4	prokomponovanost
relativně	relativně	k6eAd1	relativně
krátkých	krátký	k2eAgInPc2d1	krátký
veršových	veršový	k2eAgInPc2d1	veršový
útvarů	útvar	k1gInPc2	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
vrcholná	vrcholný	k2eAgNnPc4d1	vrcholné
Mallarmého	Mallarmý	k2eAgNnSc2d1	Mallarmé
díla	dílo	k1gNnSc2	dílo
pak	pak	k6eAd1	pak
patří	patřit	k5eAaImIp3nP	patřit
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
sonety	sonet	k1gInPc4	sonet
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nesou	nést	k5eAaImIp3nP	nést
prostě	prostě	k9	prostě
jména	jméno	k1gNnPc4	jméno
svých	svůj	k3xOyFgFnPc2	svůj
úvodních	úvodní	k2eAgFnPc2d1	úvodní
strof	strofa	k1gFnPc2	strofa
(	(	kIx(	(
<g/>
Výšinám	výšina	k1gFnPc3	výšina
dávám	dávat	k5eAaImIp1nS	dávat
zář	zář	k1gFnSc1	zář
nehtu	nehet	k1gInSc2	nehet
s	s	k7c7	s
onyxem	onyx	k1gInSc7	onyx
<g/>
...	...	k?	...
<g/>
;	;	kIx,	;
Když	když	k8xS	když
vyvstala	vyvstat	k5eAaPmAgFnS	vyvstat
z	z	k7c2	z
obliny	oblina	k1gFnSc2	oblina
a	a	k8xC	a
vzepětí	vzepětí	k1gNnSc2	vzepětí
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
Krajka	krajka	k1gFnSc1	krajka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
ruší	rušit	k5eAaImIp3nS	rušit
sama	sám	k3xTgMnSc4	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mallarmé	Mallarmý	k2eAgNnSc1d1	Mallarmé
je	být	k5eAaImIp3nS	být
koncipoval	koncipovat	k5eAaBmAgInS	koncipovat
jako	jako	k8xC	jako
hermetická	hermetický	k2eAgFnSc1d1	hermetická
<g/>
,	,	kIx,	,
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
se	se	k3xPyFc4	se
zavíjející	zavíjející	k2eAgNnPc1d1	zavíjející
sdělení	sdělení	k1gNnPc1	sdělení
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
jediné	jediný	k2eAgFnSc6d1	jediná
<g/>
,	,	kIx,	,
bohatě	bohatě	k6eAd1	bohatě
rozvinuté	rozvinutý	k2eAgFnPc1d1	rozvinutá
věty	věta	k1gFnPc1	věta
<g/>
,	,	kIx,	,
dokonale	dokonale	k6eAd1	dokonale
propracované	propracovaný	k2eAgInPc1d1	propracovaný
<g/>
,	,	kIx,	,
psané	psaný	k2eAgNnSc1d1	psané
širokodechým	širokodechý	k2eAgInSc7d1	širokodechý
vázaným	vázaný	k2eAgInSc7d1	vázaný
veršem	verš	k1gInSc7	verš
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pochopení	pochopení	k1gNnSc4	pochopení
Mallarmého	Mallarmý	k2eAgNnSc2d1	Mallarmé
díla	dílo	k1gNnSc2	dílo
je	být	k5eAaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
znát	znát	k5eAaImF	znát
jeho	on	k3xPp3gInSc4	on
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
filosofii	filosofie	k1gFnSc4	filosofie
a	a	k8xC	a
lingvistiku	lingvistika	k1gFnSc4	lingvistika
<g/>
,	,	kIx,	,
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
jazyk	jazyk	k1gInSc1	jazyk
hovoru	hovor	k1gInSc2	hovor
a	a	k8xC	a
básně	báseň	k1gFnPc4	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
Mallarmého	Mallarmý	k2eAgNnSc2d1	Mallarmé
díla	dílo	k1gNnSc2	dílo
se	se	k3xPyFc4	se
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
toho	ten	k3xDgInSc2	ten
nejpodstatnějšího	podstatný	k2eAgInSc2d3	nejpodstatnější
s	s	k7c7	s
čím	co	k3yInSc7	co
se	se	k3xPyFc4	se
lidská	lidský	k2eAgFnSc1d1	lidská
existence	existence	k1gFnSc1	existence
v	v	k7c6	v
životě	život	k1gInSc6	život
potkává	potkávat	k5eAaImIp3nS	potkávat
<g/>
:	:	kIx,	:
metafyzické	metafyzický	k2eAgFnPc4d1	metafyzická
zkušenosti	zkušenost	k1gFnPc4	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
bývá	bývat	k5eAaImIp3nS	bývat
Mallarmé	Mallarmý	k2eAgNnSc1d1	Mallarmé
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
nejvýznamnějšího	významný	k2eAgMnSc4d3	nejvýznamnější
francouzského	francouzský	k2eAgMnSc4d1	francouzský
symbolistního	symbolistní	k2eAgMnSc4d1	symbolistní
básníka	básník	k1gMnSc4	básník
první	první	k4xOgFnSc2	první
generace	generace	k1gFnSc2	generace
tohoto	tento	k3xDgInSc2	tento
směru	směr	k1gInSc2	směr
<g/>
.	.	kIx.	.
</s>
<s>
Mallarmé	Mallarmý	k2eAgNnSc1d1	Mallarmé
sám	sám	k3xTgMnSc1	sám
svou	svůj	k3xOyFgFnSc4	svůj
metodu	metoda	k1gFnSc4	metoda
přirovnával	přirovnávat	k5eAaImAgMnS	přirovnávat
ke	k	k7c3	k
kabale	kabala	k1gFnSc3	kabala
<g/>
:	:	kIx,	:
veškerá	veškerý	k3xTgNnPc1	veškerý
použitá	použitý	k2eAgNnPc1d1	Použité
slova	slovo	k1gNnPc1	slovo
<g/>
,	,	kIx,	,
obrazy	obraz	k1gInPc1	obraz
<g/>
,	,	kIx,	,
figury	figura	k1gFnPc1	figura
mají	mít	k5eAaImIp3nP	mít
svůj	svůj	k3xOyFgInSc4	svůj
odraz	odraz	k1gInSc4	odraz
v	v	k7c6	v
transcendenci	transcendence	k1gFnSc6	transcendence
<g/>
.	.	kIx.	.
</s>
<s>
Typickými	typický	k2eAgNnPc7d1	typické
tématy	téma	k1gNnPc7	téma
Mallarmého	Mallarmý	k2eAgInSc2d1	Mallarmý
jsou	být	k5eAaImIp3nP	být
prohra	prohra	k1gFnSc1	prohra
<g/>
,	,	kIx,	,
vzepětí	vzepětí	k1gNnSc1	vzepětí
<g/>
,	,	kIx,	,
únava	únava	k1gFnSc1	únava
<g/>
,	,	kIx,	,
touha	touha	k1gFnSc1	touha
<g/>
,	,	kIx,	,
božství	božství	k1gNnSc1	božství
<g/>
,	,	kIx,	,
typickými	typický	k2eAgInPc7d1	typický
emblémy	emblém	k1gInPc7	emblém
labuť	labuť	k1gFnSc4	labuť
<g/>
,	,	kIx,	,
váza	váza	k1gFnSc1	váza
<g/>
,	,	kIx,	,
hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
obloha	obloha	k1gFnSc1	obloha
<g/>
,	,	kIx,	,
podzim	podzim	k1gInSc1	podzim
<g/>
,	,	kIx,	,
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
zašifrovanost	zašifrovanost	k1gFnSc4	zašifrovanost
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
při	při	k7c6	při
nepečlivém	pečlivý	k2eNgNnSc6d1	pečlivý
čtení	čtení	k1gNnSc6	čtení
působit	působit	k5eAaImF	působit
pouze	pouze	k6eAd1	pouze
jako	jako	k9	jako
ornamentální	ornamentální	k2eAgFnSc2d1	ornamentální
jazykové	jazykový	k2eAgFnSc2d1	jazyková
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
vrcholném	vrcholný	k2eAgInSc6d1	vrcholný
díle	díl	k1gInSc6	díl
<g/>
,	,	kIx,	,
poémě	poéma	k1gFnSc6	poéma
Vrh	vrh	k1gInSc1	vrh
kostek	kostka	k1gFnPc2	kostka
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
nezruší	zrušit	k5eNaPmIp3nS	zrušit
náhodu	náhoda	k1gFnSc4	náhoda
<g/>
.	.	kIx.	.
</s>
<s>
Mallarmé	Mallarmý	k2eAgNnSc1d1	Mallarmé
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
na	na	k7c4	na
vázaný	vázaný	k2eAgInSc4d1	vázaný
verš	verš	k1gInSc4	verš
a	a	k8xC	a
kromě	kromě	k7c2	kromě
zcela	zcela	k6eAd1	zcela
volné	volný	k2eAgFnSc2d1	volná
textové	textový	k2eAgFnSc2d1	textová
"	"	kIx"	"
<g/>
partitury	partitura	k1gFnSc2	partitura
<g/>
"	"	kIx"	"
experimentoval	experimentovat	k5eAaImAgMnS	experimentovat
i	i	k9	i
s	s	k7c7	s
roztříštěním	roztříštění	k1gNnSc7	roztříštění
narace	narace	k1gFnSc2	narace
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
různých	různý	k2eAgInPc2d1	různý
typů	typ	k1gInPc2	typ
fontů	font	k1gInPc2	font
je	být	k5eAaImIp3nS	být
Vrh	vrh	k1gInSc1	vrh
kostek	kostka	k1gFnPc2	kostka
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
předchůdce	předchůdce	k1gMnSc4	předchůdce
grafické	grafický	k2eAgFnSc2d1	grafická
poezie	poezie	k1gFnSc2	poezie
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
Mallarmého	Mallarmý	k2eAgInSc2d1	Mallarmý
textů	text	k1gInPc2	text
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
základ	základ	k1gInSc4	základ
pro	pro	k7c4	pro
významné	významný	k2eAgFnPc4d1	významná
hudební	hudební	k2eAgFnPc4d1	hudební
kompozice	kompozice	k1gFnPc4	kompozice
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Debussy	Debussa	k1gFnSc2	Debussa
<g/>
,	,	kIx,	,
Ravel	Ravel	k1gMnSc1	Ravel
<g/>
,	,	kIx,	,
Boulez	Boulez	k1gMnSc1	Boulez
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
Mallarmého	Mallarmý	k2eAgInSc2d1	Mallarmý
poezii	poezie	k1gFnSc4	poezie
překládali	překládat	k5eAaImAgMnP	překládat
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
<g/>
,	,	kIx,	,
Emanuel	Emanuel	k1gMnSc1	Emanuel
z	z	k7c2	z
Lešehradu	Lešehrad	k1gInSc2	Lešehrad
<g/>
,	,	kIx,	,
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Nezval	Nezval	k1gMnSc1	Nezval
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Hrubín	Hrubín	k1gMnSc1	Hrubín
<g/>
,	,	kIx,	,
Ota	Ota	k1gMnSc1	Ota
Nechutová	Nechutový	k2eAgNnPc1d1	Nechutový
anebo	anebo	k8xC	anebo
Jan	Jan	k1gMnSc1	Jan
M.	M.	kA	M.
Tomeš	Tomeš	k1gMnSc1	Tomeš
<g/>
.	.	kIx.	.
</s>
<s>
Herodias	Herodias	k1gFnSc1	Herodias
(	(	kIx(	(
<g/>
1871	[number]	k4	1871
<g/>
)	)	kIx)	)
-	-	kIx~	-
původně	původně	k6eAd1	původně
koncipováno	koncipovat	k5eAaBmNgNnS	koncipovat
jako	jako	k9	jako
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
tzv.	tzv.	kA	tzv.
Starý	starý	k2eAgInSc1d1	starý
předzpěv	předzpěv	k1gInSc1	předzpěv
k	k	k7c3	k
Herodiadě	Herodiada	k1gFnSc3	Herodiada
<g/>
,	,	kIx,	,
samotná	samotný	k2eAgFnSc1d1	samotná
báseň	báseň	k1gFnSc1	báseň
Herodias	Herodias	k1gFnSc1	Herodias
a	a	k8xC	a
apendix	apendix	k1gInSc1	apendix
Zpěv	zpěv	k1gInSc1	zpěv
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
postavě	postava	k1gFnSc6	postava
mladistvé	mladistvý	k2eAgFnSc2d1	mladistvá
matky	matka	k1gFnSc2	matka
princezny	princezna	k1gFnSc2	princezna
Salome	Salom	k1gInSc5	Salom
Mallarmé	Mallarmý	k2eAgNnSc4d1	Mallarmé
traktuje	traktovat	k5eAaImIp3nS	traktovat
Mallarmé	Mallarmý	k2eAgFnPc4d1	Mallarmý
mladistvou	mladistvý	k2eAgFnSc4d1	mladistvá
touhu	touha	k1gFnSc4	touha
po	po	k7c6	po
čistotě	čistota	k1gFnSc6	čistota
-	-	kIx~	-
ducha	duch	k1gMnSc2	duch
i	i	k9	i
těla	tělo	k1gNnSc2	tělo
-	-	kIx~	-
ničenou	ničený	k2eAgFnSc4d1	ničená
veškerou	veškerý	k3xTgFnSc4	veškerý
realitou	realita	k1gFnSc7	realita
okolo	okolo	k7c2	okolo
nás	my	k3xPp1nPc2	my
<g/>
.	.	kIx.	.
</s>
<s>
Zpěv	zpěv	k1gInSc1	zpěv
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
je	být	k5eAaImIp3nS	být
záznamem	záznam	k1gInSc7	záznam
vlastní	vlastní	k2eAgFnSc2d1	vlastní
dekapitace	dekapitace	k1gFnSc2	dekapitace
coby	coby	k?	coby
symbolu	symbol	k1gInSc2	symbol
přechodu	přechod	k1gInSc2	přechod
do	do	k7c2	do
vyššího	vysoký	k2eAgInSc2d2	vyšší
stupně	stupeň	k1gInSc2	stupeň
poznání	poznání	k1gNnSc2	poznání
<g/>
.	.	kIx.	.
</s>
<s>
Báseň	báseň	k1gFnSc1	báseň
Herodias	Herodias	k1gFnSc1	Herodias
použil	použít	k5eAaPmAgMnS	použít
skladatel	skladatel	k1gMnSc1	skladatel
Paul	Paul	k1gMnSc1	Paul
Hindemith	Hindemith	k1gMnSc1	Hindemith
jako	jako	k8xS	jako
textový	textový	k2eAgInSc1d1	textový
podklad	podklad	k1gInSc1	podklad
ke	k	k7c3	k
stejnojmennému	stejnojmenný	k2eAgInSc3d1	stejnojmenný
melodramu	melodram	k1gInSc3	melodram
<g/>
.	.	kIx.	.
</s>
<s>
Smuteční	smuteční	k2eAgInSc1d1	smuteční
přípitek	přípitek	k1gInSc1	přípitek
(	(	kIx(	(
<g/>
Náhrobek	náhrobek	k1gInSc1	náhrobek
Théophila	Théophil	k1gMnSc2	Théophil
Gautiera	Gautier	k1gMnSc2	Gautier
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
"	"	kIx"	"
<g/>
náhrobků	náhrobek	k1gInPc2	náhrobek
<g/>
"	"	kIx"	"
anebo	anebo	k8xC	anebo
"	"	kIx"	"
<g/>
poct	pocta	k1gFnPc2	pocta
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
tombeau	tombeau	k6eAd1	tombeau
<g/>
"	"	kIx"	"
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
dvojí	dvojit	k5eAaImIp3nS	dvojit
význam	význam	k1gInSc1	význam
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dalšího	další	k2eAgInSc2d1	další
typicky	typicky	k6eAd1	typicky
mallarméovského	mallarméovský	k2eAgInSc2d1	mallarméovský
útvaru	útvar	k1gInSc2	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
ve	v	k7c6	v
vysokém	vysoký	k2eAgInSc6d1	vysoký
stupni	stupeň	k1gInSc6	stupeň
abstrakce	abstrakce	k1gFnSc1	abstrakce
své	svůj	k3xOyFgNnSc4	svůj
téma	téma	k1gNnSc4	téma
neukončitelnosti	neukončitelnost	k1gFnSc2	neukončitelnost
a	a	k8xC	a
marnosti	marnost	k1gFnSc2	marnost
lidského	lidský	k2eAgNnSc2d1	lidské
snažení	snažení	k1gNnSc2	snažení
uprostřed	uprostřed	k7c2	uprostřed
náhodného	náhodný	k2eAgInSc2d1	náhodný
běhu	běh	k1gInSc2	běh
věcí	věc	k1gFnPc2	věc
<g/>
.	.	kIx.	.
</s>
<s>
Faunovo	Faunův	k2eAgNnSc1d1	Faunův
pozdní	pozdní	k2eAgNnSc1d1	pozdní
odpoledne	odpoledne	k1gNnSc1	odpoledne
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ekloga	ekloga	k1gFnSc1	ekloga
-	-	kIx~	-
původně	původně	k6eAd1	původně
zamýšleno	zamýšlet	k5eAaImNgNnS	zamýšlet
jako	jako	k9	jako
drama	drama	k1gNnSc1	drama
<g/>
.	.	kIx.	.
</s>
<s>
Faun	Faun	k1gMnSc1	Faun
si	se	k3xPyFc3	se
v	v	k7c6	v
básni	báseň	k1gFnSc6	báseň
evokuje	evokovat	k5eAaBmIp3nS	evokovat
milostné	milostný	k2eAgInPc4d1	milostný
prožitky	prožitek	k1gInPc4	prožitek
s	s	k7c7	s
nymfami	nymfa	k1gFnPc7	nymfa
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
skutečnost	skutečnost	k1gFnSc4	skutečnost
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
dál	daleko	k6eAd2	daleko
své	svůj	k3xOyFgFnPc4	svůj
představy	představa	k1gFnPc4	představa
<g/>
...	...	k?	...
Faunem	Faun	k1gMnSc7	Faun
se	se	k3xPyFc4	se
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
Claude	Claud	k1gInSc5	Claud
Debussy	Debuss	k1gInPc4	Debuss
<g/>
:	:	kIx,	:
napsal	napsat	k5eAaBmAgMnS	napsat
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
stejnojmennou	stejnojmenný	k2eAgFnSc4d1	stejnojmenná
symfonickou	symfonický	k2eAgFnSc4d1	symfonická
báseň	báseň	k1gFnSc4	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Vrh	vrh	k1gInSc1	vrh
kostek	kostka	k1gFnPc2	kostka
nikdy	nikdy	k6eAd1	nikdy
nezruší	zrušit	k5eNaPmIp3nS	zrušit
náhodu	náhoda	k1gFnSc4	náhoda
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
posmrtně	posmrtně	k6eAd1	posmrtně
<g/>
)	)	kIx)	)
-	-	kIx~	-
Poéma	poéma	k1gFnSc1	poéma
obkružuje	obkružovat	k5eAaImIp3nS	obkružovat
motiv	motiv	k1gInSc1	motiv
ztroskotavšího	ztroskotavší	k2eAgMnSc2d1	ztroskotavší
mořeplavce	mořeplavec	k1gMnSc2	mořeplavec
-	-	kIx~	-
předobrazem	předobraz	k1gInSc7	předobraz
byl	být	k5eAaImAgMnS	být
Vasco	Vasco	k1gMnSc1	Vasco
da	da	k?	da
Gamma	Gamma	k1gFnSc1	Gamma
<g/>
.	.	kIx.	.
</s>
<s>
Ztroskotání	ztroskotání	k1gNnSc1	ztroskotání
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
pojímáno	pojímán	k2eAgNnSc1d1	pojímáno
šířeji	šířej	k1gInSc6	šířej
a	a	k8xC	a
ztroskotancem	ztroskotanec	k1gMnSc7	ztroskotanec
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
sám	sám	k3xTgMnSc1	sám
básník	básník	k1gMnSc1	básník
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
pouti	pouť	k1gFnSc6	pouť
za	za	k7c7	za
vyšším	vysoký	k2eAgInSc7d2	vyšší
řádem	řád	k1gInSc7	řád
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
za	za	k7c7	za
souhvězdím	souhvězdí	k1gNnSc7	souhvězdí
<g/>
.	.	kIx.	.
</s>
<s>
Náhoda	náhoda	k1gFnSc1	náhoda
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
skaliskem	skalisko	k1gNnSc7	skalisko
<g/>
,	,	kIx,	,
o	o	k7c4	o
které	který	k3yRgInPc4	který
jeho	jeho	k3xOp3gFnSc1	jeho
snaha	snaha	k1gFnSc1	snaha
vždy	vždy	k6eAd1	vždy
znova	znova	k6eAd1	znova
ztroskotává	ztroskotávat	k5eAaImIp3nS	ztroskotávat
<g/>
.	.	kIx.	.
</s>
<s>
Bloudění	bloudění	k1gNnSc1	bloudění
je	být	k5eAaImIp3nS	být
cíl	cíl	k1gInSc4	cíl
pouti	pouť	k1gFnSc2	pouť
<g/>
.	.	kIx.	.
</s>
<s>
Labuť	labuť	k1gFnSc1	labuť
Rozrazí	rozrazit	k5eAaPmIp3nS	rozrazit
panenský	panenský	k2eAgInSc4d1	panenský
<g/>
,	,	kIx,	,
krásný	krásný	k2eAgInSc4d1	krásný
a	a	k8xC	a
rozkvetlý	rozkvetlý	k2eAgInSc4d1	rozkvetlý
dnešek	dnešek	k1gInSc4	dnešek
svým	svůj	k3xOyFgNnSc7	svůj
opilým	opilý	k2eAgNnSc7d1	opilé
křídlem	křídlo	k1gNnSc7	křídlo
to	ten	k3xDgNnSc1	ten
k	k	k7c3	k
nerozbití	nerozbití	k1gNnSc3	nerozbití
a	a	k8xC	a
pusté	pustý	k2eAgNnSc1d1	pusté
jezero	jezero	k1gNnSc1	jezero
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
jíní	jíní	k1gNnSc6	jíní
bludně	bludně	k6eAd1	bludně
svítí	svítit	k5eAaImIp3nS	svítit
průsvitný	průsvitný	k2eAgInSc1d1	průsvitný
ledovec	ledovec	k1gInSc1	ledovec
letů	let	k1gInPc2	let
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
nevzlétly	vzlétnout	k5eNaPmAgInP	vzlétnout
<g/>
?	?	kIx.	?
</s>
<s>
O	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
<g/>
,	,	kIx,	,
vznešené	vznešený	k2eAgMnPc4d1	vznešený
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
dávná	dávný	k2eAgFnSc1d1	dávná
labuť	labuť	k1gFnSc1	labuť
ví	vědět	k5eAaImIp3nS	vědět
jen	jen	k9	jen
v	v	k7c6	v
rozpomínání	rozpomínání	k1gNnSc6	rozpomínání
<g/>
,	,	kIx,	,
že	že	k8xS	že
nevzlétne	vzlétnout	k5eNaPmIp3nS	vzlétnout
už	už	k6eAd1	už
<g/>
,	,	kIx,	,
cítí	cítit	k5eAaImIp3nS	cítit
<g/>
,	,	kIx,	,
vždyť	vždyť	k9	vždyť
neopěvala	opěvat	k5eNaImAgFnS	opěvat
končinu	končina	k1gFnSc4	končina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
lze	lze	k6eAd1	lze
žíti	žít	k5eAaImF	žít
<g/>
,	,	kIx,	,
když	když	k8xS	když
nudou	nuda	k1gFnSc7	nuda
zaskví	zaskvět	k5eAaPmIp3nS	zaskvět
se	se	k3xPyFc4	se
neplodné	plodný	k2eNgInPc1d1	neplodný
zimní	zimní	k2eAgInPc1d1	zimní
dny	den	k1gInPc1	den
<g/>
.	.	kIx.	.
</s>
<s>
Pták	pták	k1gMnSc1	pták
šíjí	šíj	k1gFnPc2	šíj
setřese	setřást	k5eAaPmIp3nS	setřást
tu	ten	k3xDgFnSc4	ten
bílou	bílý	k2eAgFnSc4d1	bílá
agónii	agónie	k1gFnSc4	agónie
<g/>
,	,	kIx,	,
vnucenou	vnucený	k2eAgFnSc4d1	vnucená
prostorem	prostor	k1gInSc7	prostor
<g/>
,	,	kIx,	,
jejž	jenž	k3xRgInSc4	jenž
popírá	popírat	k5eAaImIp3nS	popírat
<g/>
,	,	kIx,	,
leč	leč	k8xC	leč
šíjí	šíj	k1gFnSc7	šíj
neshodí	shodit	k5eNaPmIp3nS	shodit
hrůzu	hrůza	k1gFnSc4	hrůza
ker	kra	k1gFnPc2	kra
<g/>
,	,	kIx,	,
v	v	k7c4	v
něž	jenž	k3xRgNnSc4	jenž
křídlo	křídlo	k1gNnSc4	křídlo
vmrzlo	vmrznout	k5eAaPmAgNnS	vmrznout
mu	on	k3xPp3gMnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Přízrak	přízrak	k1gInSc1	přízrak
<g/>
,	,	kIx,	,
jejž	jenž	k3xRgInSc4	jenž
vlastní	vlastní	k2eAgInSc4d1	vlastní
jas	jas	k1gInSc4	jas
a	a	k8xC	a
čirost	čirost	k1gFnSc4	čirost
vykázaly	vykázat	k5eAaPmAgInP	vykázat
právě	právě	k9	právě
sem	sem	k6eAd1	sem
<g/>
,	,	kIx,	,
ztuhne	ztuhnout	k5eAaPmIp3nS	ztuhnout
v	v	k7c4	v
led	led	k1gInSc4	led
pohrdavého	pohrdavý	k2eAgInSc2d1	pohrdavý
snu	sen	k1gInSc2	sen
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
v	v	k7c6	v
neužitečném	užitečný	k2eNgInSc6d1	neužitečný
exilu	exil	k1gInSc6	exil
Labuť	labuť	k1gFnSc1	labuť
halí	halit	k5eAaImIp3nS	halit
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
přeložil	přeložit	k5eAaPmAgMnS	přeložit
František	František	k1gMnSc1	František
Hrubín	Hrubín	k1gMnSc1	Hrubín
<g/>
)	)	kIx)	)
</s>
