<s>
Za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
více	hodně	k6eAd2	hodně
než	než	k8xS	než
25	[number]	k4	25
<g/>
letou	letý	k2eAgFnSc4d1	letá
kariéru	kariéra	k1gFnSc4	kariéra
skupina	skupina	k1gFnSc1	skupina
vydala	vydat	k5eAaPmAgFnS	vydat
11	[number]	k4	11
studiových	studiový	k2eAgNnPc2d1	studiové
alb	album	k1gNnPc2	album
<g/>
,	,	kIx,	,
kterých	který	k3yRgFnPc2	který
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
přes	přes	k7c4	přes
1	[number]	k4	1
000	[number]	k4	000
000	[number]	k4	000
kopií	kopie	k1gFnPc2	kopie
<g/>
,	,	kIx,	,
reprezentovala	reprezentovat	k5eAaImAgFnS	reprezentovat
Česko	Česko	k1gNnSc4	Česko
na	na	k7c6	na
soutěži	soutěž	k1gFnSc6	soutěž
Eurovision	Eurovision	k1gInSc4	Eurovision
Song	song	k1gInSc1	song
Contest	Contest	k1gInSc1	Contest
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ovšem	ovšem	k9	ovšem
neuspěla	uspět	k5eNaPmAgFnS	uspět
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
desátého	desátý	k4xOgInSc2	desátý
slavíka	slavík	k1gInSc2	slavík
za	za	k7c4	za
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
<g/>
.	.	kIx.	.
</s>
