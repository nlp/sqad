<p>
<s>
Ireducibilní	Ireducibilní	k2eAgInSc1d1	Ireducibilní
ideál	ideál	k1gInSc1	ideál
je	být	k5eAaImIp3nS	být
pojem	pojem	k1gInSc4	pojem
komutativní	komutativní	k2eAgFnSc2d1	komutativní
algebry	algebra	k1gFnSc2	algebra
<g/>
.	.	kIx.	.
</s>
<s>
Rozumí	rozumět	k5eAaImIp3nS	rozumět
se	se	k3xPyFc4	se
jím	jíst	k5eAaImIp1nS	jíst
takový	takový	k3xDgInSc1	takový
ideál	ideál	k1gInSc1	ideál
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
I	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
I	i	k9	i
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
komutativního	komutativní	k2eAgInSc2d1	komutativní
okruhu	okruh	k1gInSc2	okruh
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R	R	kA	R
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
který	který	k3yRgInSc4	který
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k9	jako
průnik	průnik	k1gInSc4	průnik
dvou	dva	k4xCgMnPc2	dva
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
různých	různý	k2eAgInPc2d1	různý
ideálů	ideál	k1gInPc2	ideál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
==	==	k?	==
</s>
</p>
<p>
<s>
Každý	každý	k3xTgInSc1	každý
prvoideál	prvoideál	k1gInSc1	prvoideál
je	být	k5eAaImIp3nS	být
ireducibilní	ireducibilní	k2eAgInSc1d1	ireducibilní
</s>
</p>
<p>
<s>
V	v	k7c6	v
oboru	obor	k1gInSc6	obor
integrity	integrita	k1gFnSc2	integrita
celých	celý	k2eAgNnPc2d1	celé
čísel	číslo	k1gNnPc2	číslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbba	k1gFnPc2	mathbba
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgInSc1d1	hlavní
ideál	ideál	k1gInSc1	ideál
prvku	prvek	k1gInSc2	prvek
5	[number]	k4	5
ireducibilní	ireducibilní	k2eAgInSc1d1	ireducibilní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavní	hlavní	k2eAgInSc1d1	hlavní
ideál	ideál	k1gInSc1	ideál
prvku	prvek	k1gInSc2	prvek
6	[number]	k4	6
není	být	k5eNaImIp3nS	být
ireducibilní	ireducibilní	k2eAgNnSc1d1	ireducibilní
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
platí	platit	k5eAaImIp3nS	platit
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
6	[number]	k4	6
</s>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
<s>
∩	∩	k?	∩
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
6	[number]	k4	6
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbba	k1gFnPc2	mathbba
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
3	[number]	k4	3
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbba	k1gFnPc2	mathbba
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
cap	cap	k1gMnSc1	cap
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbba	k1gFnPc2	mathbba
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
komutativním	komutativní	k2eAgInSc6d1	komutativní
noetherovském	noetherovský	k2eAgInSc6d1	noetherovský
okruhu	okruh	k1gInSc6	okruh
je	být	k5eAaImIp3nS	být
každý	každý	k3xTgInSc4	každý
ireducibilní	ireducibilní	k2eAgInSc4d1	ireducibilní
ideál	ideál	k1gInSc4	ideál
primárním	primární	k2eAgInSc7d1	primární
ideálem	ideál	k1gInSc7	ideál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
