<s desamb="1">
Podle	podle	k7c2
sčítání	sčítání	k1gNnSc2
lidu	lid	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
žije	žít	k5eAaImIp3nS
na	na	k7c6
Ukrajině	Ukrajina	k1gFnSc6
celkem	celkem	k6eAd1
17,3	17,3	k4
%	%	kIx~
Rusů	Rus	k1gMnPc2
<g/>
,	,	kIx,
většina	většina	k1gFnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
v	v	k7c6
regionech	region	k1gInPc6
na	na	k7c6
východě	východ	k1gInSc6
a	a	k8xC
jihu	jih	k1gInSc6
<g/>
,	,	kIx,
např.	např.	kA
regiony	region	k1gInPc1
jako	jako	k9
Slobodská	Slobodský	k2eAgFnSc1d1
Ukrajina	Ukrajina	k1gFnSc1
a	a	k8xC
Nové	Nové	k2eAgNnSc1d1
Rusko	Rusko	k1gNnSc1
<g/>
.	.	kIx.
</s>