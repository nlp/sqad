<s>
Ruská	ruský	k2eAgFnSc1d1
menšina	menšina	k1gFnSc1
na	na	k7c6
Ukrajině	Ukrajina	k1gFnSc6
</s>
<s>
Největší	veliký	k2eAgInSc1d3
etnický	etnický	k2eAgInSc1d1
původ	původ	k1gInSc1
ve	v	k7c6
městech	město	k1gNnPc6
a	a	k8xC
v	v	k7c6
oblastech	oblast	k1gFnPc6
Ukrajiny	Ukrajina	k1gFnSc2
Podle	podle	k7c2
2001	#num#	k4
sčítání	sčítání	k1gNnSc2
lidu	lid	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusové	Rus	k1gMnPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
modré	modrý	k2eAgFnSc6d1
barvě	barva	k1gFnSc6
</s>
<s>
Podíl	podíl	k1gInSc1
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
hlásí	hlásit	k5eAaImIp3nP
k	k	k7c3
ruské	ruský	k2eAgFnSc3d1
národnosti	národnost	k1gFnSc3
<g/>
,	,	kIx,
v	v	k7c6
jednotlivých	jednotlivý	k2eAgInPc6d1
regionech	region	k1gInPc6
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1
menšina	menšina	k1gFnSc1
na	na	k7c6
Ukrajině	Ukrajina	k1gFnSc6
je	být	k5eAaImIp3nS
největší	veliký	k2eAgFnSc7d3
národnostní	národnostní	k2eAgFnSc7d1
menšinou	menšina	k1gFnSc7
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
tvoří	tvořit	k5eAaImIp3nS
největší	veliký	k2eAgFnSc4d3
skupinu	skupina	k1gFnSc4
Rusů	Rus	k1gMnPc2
žijících	žijící	k2eAgMnPc2d1
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
ruské	ruský	k2eAgFnSc3d1
národnosti	národnost	k1gFnSc3
se	se	k3xPyFc4
během	během	k7c2
sčítání	sčítání	k1gNnSc2
lidu	lid	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
přihlásilo	přihlásit	k5eAaPmAgNnS
8	#num#	k4
334	#num#	k4
100	#num#	k4
osob	osoba	k1gFnPc2
(	(	kIx(
<g/>
17,3	17,3	k4
<g/>
%	%	kIx~
ukrajinské	ukrajinský	k2eAgFnSc2d1
populace	populace	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
číslo	číslo	k1gNnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
jak	jak	k8xS,k8xC
lidi	člověk	k1gMnPc4
pocházející	pocházející	k2eAgMnPc1d1
z	z	k7c2
ukrajinského	ukrajinský	k2eAgNnSc2d1
zahraničí	zahraničí	k1gNnSc2
tak	tak	k6eAd1
lidi	člověk	k1gMnPc4
narozené	narozený	k2eAgMnPc4d1
na	na	k7c6
Ukrajině	Ukrajina	k1gFnSc6
hlásící	hlásící	k2eAgFnSc1d1
se	se	k3xPyFc4
k	k	k7c3
ruské	ruský	k2eAgFnSc3d1
národnosti	národnost	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Geografická	geografický	k2eAgFnSc1d1
distribuce	distribuce	k1gFnSc1
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1
menšina	menšina	k1gFnSc1
převažuje	převažovat	k5eAaImIp3nS
na	na	k7c6
jihovýchodě	jihovýchod	k1gInSc6
Ukrajiny	Ukrajina	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
představuje	představovat	k5eAaImIp3nS
v	v	k7c6
některých	některý	k3yIgInPc6
regionech	region	k1gInPc6
(	(	kIx(
<g/>
Krym	Krym	k1gInSc1
<g/>
)	)	kIx)
dokonce	dokonce	k9
většinu	většina	k1gFnSc4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
sčítání	sčítání	k1gNnSc2
lidu	lid	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
žije	žít	k5eAaImIp3nS
na	na	k7c6
Ukrajině	Ukrajina	k1gFnSc6
celkem	celkem	k6eAd1
17,3	17,3	k4
%	%	kIx~
Rusů	Rus	k1gMnPc2
<g/>
,	,	kIx,
většina	většina	k1gFnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
v	v	k7c6
regionech	region	k1gInPc6
na	na	k7c6
východě	východ	k1gInSc6
a	a	k8xC
jihu	jih	k1gInSc6
<g/>
,	,	kIx,
např.	např.	kA
regiony	region	k1gInPc1
jako	jako	k9
Slobodská	Slobodský	k2eAgFnSc1d1
Ukrajina	Ukrajina	k1gFnSc1
a	a	k8xC
Nové	Nové	k2eAgNnSc1d1
Rusko	Rusko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
na	na	k7c6
Krymu	Krym	k1gInSc6
žilo	žít	k5eAaImAgNnS
58	#num#	k4
%	%	kIx~
Rusů	Rus	k1gMnPc2
<g/>
,	,	kIx,
v	v	k7c6
Doněcké	doněcký	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
38,2	38,2	k4
%	%	kIx~
<g/>
,	,	kIx,
v	v	k7c6
Luhanské	Luhanský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
39	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
v	v	k7c6
Charkovské	charkovský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
25,6	25,6	k4
%	%	kIx~
a	a	k8xC
v	v	k7c6
Oděské	oděský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
20,7	20,7	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
téhož	týž	k3xTgNnSc2
sčítání	sčítání	k1gNnSc2
však	však	k9
považuje	považovat	k5eAaImIp3nS
ruštinu	ruština	k1gFnSc4
za	za	k7c4
svůj	svůj	k3xOyFgInSc4
mateřský	mateřský	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
ještě	ještě	k6eAd1
více	hodně	k6eAd2
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
30	#num#	k4
%	%	kIx~
na	na	k7c6
celé	celý	k2eAgFnSc6d1
Ukrajině	Ukrajina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jihovýchodních	jihovýchodní	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
někdy	někdy	k6eAd1
i	i	k8xC
výrazně	výrazně	k6eAd1
více	hodně	k6eAd2
než	než	k8xS
se	se	k3xPyFc4
ukazuje	ukazovat	k5eAaImIp3nS
podle	podle	k7c2
národnostního	národnostní	k2eAgNnSc2d1
složení	složení	k1gNnSc2
<g/>
,	,	kIx,
například	například	k6eAd1
na	na	k7c6
Krymu	Krym	k1gInSc6
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc4
77	#num#	k4
%	%	kIx~
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
v	v	k7c6
Doněcké	doněcký	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
74,9	74,9	k4
%	%	kIx~
<g/>
,	,	kIx,
v	v	k7c6
Luhanské	Luhanský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
68,8	68,8	k4
%	%	kIx~
<g/>
,	,	kIx,
v	v	k7c6
Charkovské	charkovský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
44,3	44,3	k4
%	%	kIx~
a	a	k8xC
v	v	k7c6
Oděské	oděský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
41,9	41,9	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiné	jiný	k2eAgInPc1d1
průzkumy	průzkum	k1gInPc1
tvrdí	tvrdit	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
ruština	ruština	k1gFnSc1
je	být	k5eAaImIp3nS
ještě	ještě	k9
rozšířenější	rozšířený	k2eAgInSc1d2
–	–	k?
například	například	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
podle	podle	k7c2
Kyjevského	kyjevský	k2eAgInSc2d1
mezinárodního	mezinárodní	k2eAgInSc2d1
institutu	institut	k1gInSc2
pro	pro	k7c4
sociologii	sociologie	k1gFnSc4
používalo	používat	k5eAaImAgNnS
ruštinu	ruština	k1gFnSc4
doma	doma	k6eAd1
asi	asi	k9
43	#num#	k4
–	–	k?
46	#num#	k4
%	%	kIx~
obyvatel	obyvatel	k1gMnPc2
a	a	k8xC
v	v	k7c6
regionech	region	k1gInPc6
na	na	k7c6
východě	východ	k1gInSc6
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
kolem	kolem	k7c2
80	#num#	k4
%	%	kIx~
(	(	kIx(
<g/>
na	na	k7c6
Krymu	Krym	k1gInSc6
a	a	k8xC
v	v	k7c6
Doněcké	doněcký	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
dokonce	dokonce	k9
více	hodně	k6eAd2
než	než	k8xS
90	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
výzkumů	výzkum	k1gInPc2
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
považuje	považovat	k5eAaImIp3nS
ukrajinštinu	ukrajinština	k1gFnSc4
za	za	k7c4
svůj	svůj	k3xOyFgInSc4
mateřský	mateřský	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
55	#num#	k4
%	%	kIx~
obyvatel	obyvatel	k1gMnSc1
Ukrajiny	Ukrajina	k1gFnSc2
a	a	k8xC
ruštinu	ruština	k1gFnSc4
40	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
А	А	k?
р	р	k?
г	г	k?
н	н	k?
П	П	k?
в	в	k?
2004	#num#	k4
г	г	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kievský	Kievský	k2eAgInSc1d1
mezinárodní	mezinárodní	k2eAgInSc1d1
institut	institut	k1gInSc1
pro	pro	k7c4
sociologii	sociologie	k1gFnSc4
<g/>
,	,	kIx,
2005-01-18	2005-01-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2005	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
The	The	k1gMnPc1
language	language	k1gFnSc2
question	question	k1gInSc1
<g/>
,	,	kIx,
the	the	k?
results	results	k1gInSc1
of	of	k?
recent	recent	k1gInSc1
research	research	k1gInSc1
in	in	k?
2012	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rating	rating	k1gInSc1
<g/>
,	,	kIx,
2012-05-25	2012-05-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
