<s desamb="1">
Bylo	být	k5eAaImAgNnS
postaveno	postavit	k5eAaPmNgNnS
na	na	k7c4
počest	počest	k1gFnSc4
sovětského	sovětský	k2eAgMnSc2d1
vůdce	vůdce	k1gMnSc2
Josifa	Josif	k1gMnSc2
Vissarionoviče	Vissarionovič	k1gMnSc2
Stalina	Stalin	k1gMnSc2
v	v	k7c6
období	období	k1gNnSc6
poválečného	poválečný	k2eAgInSc2d1
kultu	kult	k1gInSc2
osobnosti	osobnost	k1gFnSc2
a	a	k8xC
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
místě	místo	k1gNnSc6
stálo	stát	k5eAaImAgNnS
v	v	k7c6
letech	let	k1gInPc6
1955	#num#	k4
<g/>
–	–	k?
<g/>
1962	#num#	k4
<g/>
.	.	kIx.
</s>