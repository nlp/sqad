<s>
Stalinův	Stalinův	k2eAgInSc4d1
pomník	pomník	k1gInSc4
</s>
<s>
Stalinův	Stalinův	k2eAgInSc4d1
pomník	pomník	k1gInSc4
Pohled	pohled	k1gInSc1
ze	z	k7c2
západní	západní	k2eAgFnSc2d1
stranyz	stranyz	k1gInSc1
přístupového	přístupový	k2eAgInSc2d1
chodníkuna	chodníkuna	k1gFnSc1
Letenskou	letenský	k2eAgFnSc4d1
pláňZákladní	pláňZákladný	k2eAgMnPc1d1
údaje	údaj	k1gInPc4
Autor	autor	k1gMnSc1
</s>
<s>
Otakar	Otakar	k1gMnSc1
Švec	Švec	k1gMnSc1
Rok	rok	k1gInSc4
vzniku	vznik	k1gInSc2
</s>
<s>
1955	#num#	k4
Umístění	umístění	k1gNnSc1
Umístění	umístění	k1gNnSc1
</s>
<s>
Letná	Letná	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
5	#num#	k4
<g/>
′	′	k?
<g/>
41	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
24	#num#	k4
<g/>
′	′	k?
<g/>
57,56	57,56	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Stalinův	Stalinův	k2eAgInSc4d1
pomník	pomník	k1gInSc4
bylo	být	k5eAaImAgNnS
žulové	žulový	k2eAgNnSc1d1
sousoší	sousoší	k1gNnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
na	na	k7c6
Letné	Letná	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
postaveno	postavit	k5eAaPmNgNnS
na	na	k7c4
počest	počest	k1gFnSc4
sovětského	sovětský	k2eAgMnSc2d1
vůdce	vůdce	k1gMnSc2
Josifa	Josif	k1gMnSc2
Vissarionoviče	Vissarionovič	k1gMnSc2
Stalina	Stalin	k1gMnSc2
v	v	k7c6
období	období	k1gNnSc6
poválečného	poválečný	k2eAgInSc2d1
kultu	kult	k1gInSc2
osobnosti	osobnost	k1gFnSc2
a	a	k8xC
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
místě	místo	k1gNnSc6
stálo	stát	k5eAaImAgNnS
v	v	k7c6
letech	let	k1gInPc6
1955	#num#	k4
<g/>
–	–	k?
<g/>
1962	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
největším	veliký	k2eAgNnPc3d3
skupinovým	skupinový	k2eAgNnPc3d1
sousoším	sousoší	k1gNnPc3
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohutná	mohutný	k2eAgFnSc1d1
podsklepená	podsklepený	k2eAgFnSc1d1
terasa	terasa	k1gFnSc1
pomníku	pomník	k1gInSc2
stojí	stát	k5eAaImIp3nS
na	na	k7c6
místě	místo	k1gNnSc6
dosud	dosud	k6eAd1
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1991	#num#	k4
byl	být	k5eAaImAgInS
na	na	k7c4
ni	on	k3xPp3gFnSc4
umístěn	umístěn	k2eAgInSc1d1
metronom	metronom	k1gInSc1
sochaře	sochař	k1gMnSc2
Vratislava	Vratislav	k1gMnSc2
Karla	Karel	k1gMnSc2
Nováka	Novák	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Vznik	vznik	k1gInSc1
pomníku	pomník	k1gInSc2
</s>
<s>
Kameny	kámen	k1gInPc1
ze	z	k7c2
základu	základ	k1gInSc2
Stalinova	Stalinův	k2eAgInSc2d1
pomníku	pomník	k1gInSc2
byly	být	k5eAaImAgInP
dovezeny	dovézt	k5eAaPmNgInP
ze	z	k7c2
všechkrajů	všechkraj	k1gInPc2
tehdejší	tehdejší	k2eAgFnSc2d1
ČSR	ČSR	kA
</s>
<s>
Podle	podle	k7c2
plánů	plán	k1gInPc2
z	z	k7c2
roku	rok	k1gInSc2
1912	#num#	k4
měl	mít	k5eAaImAgMnS
na	na	k7c6
místě	místo	k1gNnSc6
vzniknout	vzniknout	k5eAaPmF
pomník	pomník	k1gInSc1
zakladatelů	zakladatel	k1gMnPc2
Sokola	Sokol	k1gMnSc2
Jindřicha	Jindřich	k1gMnSc2
Fügnera	Fügner	k1gMnSc2
a	a	k8xC
Miroslava	Miroslav	k1gMnSc2
Tyrše	Tyrš	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
pomník	pomník	k1gInSc1
ale	ale	k9
nikdy	nikdy	k6eAd1
nevznikl	vzniknout	k5eNaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
započetím	započetí	k1gNnSc7
výstavby	výstavba	k1gFnSc2
Stalinova	Stalinův	k2eAgInSc2d1
pomníku	pomník	k1gInSc2
se	se	k3xPyFc4
na	na	k7c6
místě	místo	k1gNnSc6
nacházel	nacházet	k5eAaImAgInS
nově	nově	k6eAd1
postavený	postavený	k2eAgInSc4d1
stadion	stadion	k1gInSc4
fotbalového	fotbalový	k2eAgInSc2d1
klubu	klub	k1gInSc2
SK	Sk	kA
Slavia	Slavia	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
výstavbě	výstavba	k1gFnSc3
pomníku	pomník	k1gInSc2
byl	být	k5eAaImAgInS
ale	ale	k8xC
stadion	stadion	k1gInSc1
prakticky	prakticky	k6eAd1
ihned	ihned	k6eAd1
po	po	k7c6
dostavění	dostavění	k1gNnSc6
zbořen	zbořen	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Stavba	stavba	k1gFnSc1
pomníku	pomník	k1gInSc2
byla	být	k5eAaImAgFnS
zahájena	zahájen	k2eAgFnSc1d1
22	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1949	#num#	k4
za	za	k7c2
účasti	účast	k1gFnSc2
Antonína	Antonín	k1gMnSc2
Zápotockého	Zápotockého	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlastní	vlastní	k2eAgFnPc4d1
práce	práce	k1gFnPc4
na	na	k7c4
sousoší	sousoší	k1gNnSc4
začala	začít	k5eAaPmAgFnS
ale	ale	k8xC
až	až	k9
v	v	k7c6
únoru	únor	k1gInSc6
1952	#num#	k4
a	a	k8xC
celé	celý	k2eAgFnSc2d1
pak	pak	k6eAd1
bylo	být	k5eAaImAgNnS
odhaleno	odhalen	k2eAgNnSc1d1
<g/>
,	,	kIx,
pod	pod	k7c7
heslem	heslo	k1gNnSc7
„	„	k?
<g/>
Svému	svůj	k3xOyFgMnSc3
osvoboditeli	osvoboditel	k1gMnSc3
–	–	k?
československý	československý	k2eAgInSc1d1
lid	lid	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
po	po	k7c6
setmění	setmění	k1gNnSc6
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1955	#num#	k4
<g/>
,	,	kIx,
tj.	tj.	kA
po	po	k7c6
smrti	smrt	k1gFnSc6
Stalina	Stalin	k1gMnSc4
a	a	k8xC
nedlouho	dlouho	k6eNd1
před	před	k7c7
Chruščovovou	Chruščovový	k2eAgFnSc7d1
kritikou	kritika	k1gFnSc7
Stalinova	Stalinův	k2eAgInSc2d1
kultu	kult	k1gInSc2
osobnosti	osobnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Sochařem	sochař	k1gMnSc7
byl	být	k5eAaImAgMnS
Otakar	Otakar	k1gMnSc1
Švec	Švec	k1gMnSc1
<g/>
,	,	kIx,
architekty	architekt	k1gMnPc4
Jiří	Jiří	k1gMnSc4
Štursa	Šturs	k1gMnSc4
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
manželka	manželka	k1gFnSc1
Vlasta	Vlasta	k1gFnSc1
Štursová	Štursová	k1gFnSc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
řešili	řešit	k5eAaImAgMnP
podstavec	podstavec	k1gInSc4
a	a	k8xC
okolí	okolí	k1gNnSc4
pomníku	pomník	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Švec	Švec	k1gMnSc1
a	a	k8xC
Štursa	Štursa	k1gFnSc1
před	před	k7c7
válkou	válka	k1gFnSc7
pracovali	pracovat	k5eAaImAgMnP
na	na	k7c6
projektu	projekt	k1gInSc6
pomníku	pomník	k1gInSc2
prezidenta	prezident	k1gMnSc2
T.	T.	kA
G.	G.	kA
Masaryka	Masaryk	k1gMnSc2
v	v	k7c6
témže	týž	k3xTgNnSc6
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projekt	projekt	k1gInSc1
umístil	umístit	k5eAaPmAgInS
pomník	pomník	k1gInSc4
na	na	k7c4
dobře	dobře	k6eAd1
viditelné	viditelný	k2eAgNnSc4d1
místo	místo	k1gNnSc4
nad	nad	k7c4
letenské	letenský	k2eAgNnSc4d1
vyústění	vyústění	k1gNnSc4
Čechova	Čechův	k2eAgInSc2d1
mostu	most	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
ztvárněn	ztvárnit	k5eAaPmNgMnS
jako	jako	k8xC,k8xS
řada	řada	k1gFnSc1
za	za	k7c7
sebou	se	k3xPyFc7
stojících	stojící	k2eAgFnPc2d1
postav	postava	k1gFnPc2
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
Josifem	Josif	k1gMnSc7
Stalinem	Stalin	k1gMnSc7
<g/>
,	,	kIx,
za	za	k7c7
nímž	jenž	k3xRgInSc7
stály	stát	k5eAaImAgInP
postavy	postav	k1gInPc4
pracujících	pracující	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
Stalinově	Stalinův	k2eAgFnSc6d1
levé	levý	k2eAgFnSc6d1
ruce	ruka	k1gFnSc6
to	ten	k3xDgNnSc1
byli	být	k5eAaImAgMnP
zástupci	zástupce	k1gMnPc1
sovětského	sovětský	k2eAgInSc2d1
lidu	lid	k1gInSc2
(	(	kIx(
<g/>
dělník	dělník	k1gMnSc1
<g/>
,	,	kIx,
vědec	vědec	k1gMnSc1
<g/>
,	,	kIx,
kolchoznice	kolchoznice	k1gFnSc1
a	a	k8xC
rudoarmějec	rudoarmějec	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
pravé	pravý	k2eAgFnSc6d1
ruce	ruka	k1gFnSc6
pak	pak	k6eAd1
zástupci	zástupce	k1gMnPc1
lidu	lid	k1gInSc2
československého	československý	k2eAgInSc2d1
(	(	kIx(
<g/>
dělník	dělník	k1gMnSc1
<g/>
,	,	kIx,
rolnice	rolnice	k1gFnSc1
<g/>
,	,	kIx,
novátor	novátor	k1gMnSc1
a	a	k8xC
vojín	vojín	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvě	dva	k4xCgFnPc1
z	z	k7c2
postav	postava	k1gFnPc2
měly	mít	k5eAaImAgFnP
konkrétní	konkrétní	k2eAgFnPc1d1
podoby	podoba	k1gFnPc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
Julia	Julius	k1gMnSc4
Fučíka	Fučík	k1gMnSc4
a	a	k8xC
Ivana	Ivan	k1gMnSc4
Vladimiroviče	Vladimirovič	k1gMnSc4
Mičurina	Mičurin	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pomník	pomník	k1gInSc1
ze	z	k7c2
žlutobílých	žlutobílý	k2eAgInPc2d1
opracovaných	opracovaný	k2eAgInPc2d1
žulových	žulový	k2eAgInPc2d1
kvádrů	kvádr	k1gInPc2
stál	stát	k5eAaImAgMnS
na	na	k7c6
rozsáhlé	rozsáhlý	k2eAgFnSc6d1
železobetonové	železobetonový	k2eAgFnSc6d1
konstrukci	konstrukce	k1gFnSc6
o	o	k7c6
výšce	výška	k1gFnSc6
15	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
zapuštěné	zapuštěný	k2eAgFnPc1d1
do	do	k7c2
letenského	letenský	k2eAgInSc2d1
svahu	svah	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlastní	vlastní	k2eAgInSc4d1
sousoší	sousoší	k1gNnSc1
mělo	mít	k5eAaImAgNnS
rozměry	rozměr	k1gInPc4
<g/>
:	:	kIx,
15,5	15,5	k4
m	m	kA
výšky	výška	k1gFnSc2
<g/>
,	,	kIx,
12	#num#	k4
m	m	kA
šířky	šířka	k1gFnSc2
a	a	k8xC
22	#num#	k4
m	m	kA
délky	délka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
základech	základ	k1gInPc6
(	(	kIx(
<g/>
7	#num#	k4
metrů	metr	k1gInPc2
pod	pod	k7c7
horní	horní	k2eAgFnSc7d1
hranou	hrana	k1gFnSc7
podstavce	podstavec	k1gInSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
zabudováno	zabudovat	k5eAaPmNgNnS
třiadvacet	třiadvacet	k4xCc4
základních	základní	k2eAgInPc2d1
kamenů	kámen	k1gInPc2
z	z	k7c2
nejrůznějších	různý	k2eAgFnPc2d3
míst	místo	k1gNnPc2
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
například	například	k6eAd1
část	část	k1gFnSc4
ze	z	k7c2
základů	základ	k1gInPc2
Staroměstské	staroměstský	k2eAgFnSc2d1
radnice	radnice	k1gFnSc2
<g/>
,	,	kIx,
čedič	čedič	k1gInSc4
z	z	k7c2
Řípu	Říp	k1gInSc2
<g/>
,	,	kIx,
onyx	onyx	k1gInSc1
z	z	k7c2
Inovce	Inovce	k1gMnSc2
<g/>
,	,	kIx,
kámen	kámen	k1gInSc1
z	z	k7c2
Ležáků	ležák	k1gMnPc2
<g/>
,	,	kIx,
česká	český	k2eAgFnSc1d1
žula	žula	k1gFnSc1
ze	z	k7c2
Skutče	Skuteč	k1gFnSc2
a	a	k8xC
také	také	k9
část	část	k1gFnSc1
nejstarší	starý	k2eAgFnSc1d3
slovanské	slovanský	k2eAgFnPc4d1
baziliky	bazilika	k1gFnPc4
z	z	k7c2
Velehradu	Velehrad	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náklady	náklad	k1gInPc1
na	na	k7c4
stavbu	stavba	k1gFnSc4
tohoto	tento	k3xDgInSc2
monumentu	monument	k1gInSc2
byly	být	k5eAaImAgInP
140	#num#	k4
miliónů	milión	k4xCgInPc2
tehdejších	tehdejší	k2eAgFnPc2d1
nových	nový	k2eAgFnPc2d1
korun	koruna	k1gFnPc2
(	(	kIx(
<g/>
po	po	k7c6
měnové	měnový	k2eAgFnSc6d1
reformě	reforma	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1953	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
spotřebováno	spotřebovat	k5eAaPmNgNnS
bylo	být	k5eAaImAgNnS
17	#num#	k4
tisíc	tisíc	k4xCgInPc2
tun	tuna	k1gFnPc2
materiálu	materiál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Lidová	lidový	k2eAgFnSc1d1
tvořivost	tvořivost	k1gFnSc1
</s>
<s>
Ujalo	ujmout	k5eAaPmAgNnS
se	se	k3xPyFc4
lidové	lidový	k2eAgNnSc1d1
pojmenování	pojmenování	k1gNnSc1
pomníku	pomník	k1gInSc2
Fronta	fronta	k1gFnSc1
na	na	k7c4
maso	maso	k1gNnSc4
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Werich	Werich	k1gMnSc1
jej	on	k3xPp3gMnSc4
nazval	nazvat	k5eAaBmAgMnS,k5eAaPmAgMnS
tlačenicí	tlačenice	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotná	samotný	k2eAgFnSc1d1
čtvrť	čtvrť	k1gFnSc1
Letná	Letná	k1gFnSc1
byla	být	k5eAaImAgFnS
pak	pak	k6eAd1
přezdívána	přezdívat	k5eAaImNgFnS
Záprdelí	Záprdel	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Likvidace	likvidace	k1gFnSc1
pomníku	pomník	k1gInSc2
</s>
<s>
Odstřelování	odstřelování	k1gNnSc1
pomníku	pomník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dřevěné	dřevěný	k2eAgNnSc1d1
bednění	bednění	k1gNnSc1
mělo	mít	k5eAaImAgNnS
co	co	k9
nejvíce	hodně	k6eAd3,k6eAd1
zakrýt	zakrýt	k5eAaPmF
odstřel	odstřel	k1gInSc4
před	před	k7c7
zraky	zrak	k1gInPc7
zvědavců	zvědavec	k1gMnPc2
</s>
<s>
Metronom	metronom	k1gInSc1
umístěný	umístěný	k2eAgInSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
na	na	k7c6
místě	místo	k1gNnSc6
původního	původní	k2eAgInSc2d1
Stalinova	Stalinův	k2eAgInSc2d1
pomníku	pomník	k1gInSc2
</s>
<s>
Po	po	k7c6
vystoupení	vystoupení	k1gNnSc6
Stalinova	Stalinův	k2eAgMnSc2d1
nástupce	nástupce	k1gMnSc2
N.	N.	kA
S.	S.	kA
Chruščova	Chruščův	k2eAgFnSc1d1
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
odsoudil	odsoudit	k5eAaPmAgInS
tzv.	tzv.	kA
Stalinův	Stalinův	k2eAgInSc4d1
kult	kult	k1gInSc4
osobnosti	osobnost	k1gFnSc2
<g/>
,	,	kIx,
českoslovenští	československý	k2eAgMnPc1d1
komunisté	komunista	k1gMnPc1
rozhodli	rozhodnout	k5eAaPmAgMnP
pomník	pomník	k1gInSc4
zlikvidovat	zlikvidovat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konečné	Konečná	k1gFnSc6
rozhodnutí	rozhodnutí	k1gNnSc1
znělo	znět	k5eAaImAgNnS
monument	monument	k1gInSc4
zlikvidovat	zlikvidovat	k5eAaPmF
částečným	částečný	k2eAgInSc7d1
odstřelem	odstřel	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
tak	tak	k9
6	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
roku	rok	k1gInSc2
1962	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Destrukční	destrukční	k2eAgFnSc2d1
práce	práce	k1gFnSc2
si	se	k3xPyFc3
vyžádaly	vyžádat	k5eAaPmAgInP
4,5	4,5	k4
milionu	milion	k4xCgInSc2
korun	koruna	k1gFnPc2
<g/>
,	,	kIx,
dlažební	dlažební	k2eAgFnPc1d1
kostky	kostka	k1gFnPc1
a	a	k8xC
nepoužitelný	použitelný	k2eNgInSc1d1
materiál	materiál	k1gInSc1
byl	být	k5eAaImAgInS
odvezen	odvézt	k5eAaPmNgInS
do	do	k7c2
slepého	slepý	k2eAgNnSc2d1
ramene	rameno	k1gNnSc2
Vltavy	Vltava	k1gFnSc2
u	u	k7c2
Rohanského	Rohanský	k2eAgInSc2d1
ostrova	ostrov	k1gInSc2
(	(	kIx(
<g/>
některé	některý	k3yIgInPc1
kousky	kousek	k1gInPc1
však	však	k9
dodnes	dodnes	k6eAd1
zdobí	zdobit	k5eAaImIp3nP
sbírky	sbírka	k1gFnPc1
kuriozit	kuriozita	k1gFnPc2
mnoha	mnoho	k4c2
Pražanů	Pražan	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svoji	svůj	k3xOyFgFnSc4
funkci	funkce	k1gFnSc4
tak	tak	k6eAd1
plnil	plnit	k5eAaImAgInS
pouhých	pouhý	k2eAgInPc2d1
sedm	sedm	k4xCc4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odstřelován	odstřelován	k2eAgInSc1d1
byl	být	k5eAaImAgInS
několik	několik	k4yIc4
týdnů	týden	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celá	celý	k2eAgFnSc1d1
akce	akce	k1gFnSc1
byla	být	k5eAaImAgFnS
přísně	přísně	k6eAd1
střežena	stříct	k5eAaPmNgFnS,k5eAaImNgFnS
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
zakázáno	zakázán	k2eAgNnSc1d1
oblast	oblast	k1gFnSc4
fotografovat	fotografovat	k5eAaImF
<g/>
,	,	kIx,
detonace	detonace	k1gFnSc1
však	však	k9
byly	být	k5eAaImAgInP
slyšet	slyšet	k5eAaImF
po	po	k7c6
celé	celý	k2eAgFnSc6d1
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
zničení	zničení	k1gNnSc6
pomníku	pomník	k1gInSc2
</s>
<s>
Na	na	k7c6
konci	konec	k1gInSc6
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
st.	st.	kA
proběhla	proběhnout	k5eAaPmAgFnS
soutěž	soutěž	k1gFnSc4
na	na	k7c4
výstavbu	výstavba	k1gFnSc4
Pomníku	pomník	k1gInSc2
osvobození	osvobození	k1gNnSc2
Československa	Československo	k1gNnSc2
Sovětskou	sovětský	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
bylo	být	k5eAaImAgNnS
vyhodnoceno	vyhodnotit	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
1978	#num#	k4
a	a	k8xC
na	na	k7c4
podzim	podzim	k1gInSc4
proběhla	proběhnout	k5eAaPmAgFnS
na	na	k7c6
Staroměstské	staroměstský	k2eAgFnSc6d1
radnici	radnice	k1gFnSc6
výstava	výstava	k1gFnSc1
postoupivších	postoupivší	k2eAgInPc2d1
návrhů	návrh	k1gInPc2
do	do	k7c2
3	#num#	k4
<g/>
.	.	kIx.
kola	kolo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ústřední	ústřední	k2eAgInSc1d1
výbor	výbor	k1gInSc1
KSČ	KSČ	kA
přišel	přijít	k5eAaPmAgInS
s	s	k7c7
myšlenkami	myšlenka	k1gFnPc7
na	na	k7c6
zbudování	zbudování	k1gNnSc6
nové	nový	k2eAgFnSc2d1
dominanty	dominanta	k1gFnSc2
Prahy	Praha	k1gFnSc2
roku	rok	k1gInSc2
1988	#num#	k4
<g/>
,	,	kIx,
k	k	k7c3
jeho	jeho	k3xOp3gFnSc3
realizaci	realizace	k1gFnSc3
však	však	k9
již	již	k6eAd1
nedošlo	dojít	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
zde	zde	k6eAd1
krátce	krátce	k6eAd1
působil	působit	k5eAaImAgMnS
rockový	rockový	k2eAgInSc4d1
klub	klub	k1gInSc4
a	a	k8xC
také	také	k9
odtud	odtud	k6eAd1
vysílala	vysílat	k5eAaImAgFnS
první	první	k4xOgFnSc1
soukromá	soukromý	k2eAgFnSc1d1
rozhlasová	rozhlasový	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
(	(	kIx(
<g/>
později	pozdě	k6eAd2
nazvaná	nazvaný	k2eAgFnSc1d1
Radio	radio	k1gNnSc4
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
podstavci	podstavec	k1gInSc6
stojí	stát	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1991	#num#	k4
pohyblivý	pohyblivý	k2eAgInSc1d1
metronom	metronom	k1gInSc1
sochaře	sochař	k1gMnSc2
Vratislava	Vratislav	k1gMnSc2
Karla	Karel	k1gMnSc2
Nováka	Novák	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
nechala	nechat	k5eAaPmAgFnS
ODS	ODS	kA
na	na	k7c6
místě	místo	k1gNnSc6
pomníku	pomník	k1gInSc2
vyvěsit	vyvěsit	k5eAaPmF
obrovský	obrovský	k2eAgInSc4d1
billboard	billboard	k1gInSc4
s	s	k7c7
podobiznou	podobizna	k1gFnSc7
Václava	Václav	k1gMnSc2
Klause	Klaus	k1gMnSc2
(	(	kIx(
<g/>
a	a	k8xC
Miloše	Miloš	k1gMnSc2
Zemana	Zeman	k1gMnSc2
v	v	k7c6
pozadí	pozadí	k1gNnSc6
<g/>
)	)	kIx)
a	a	k8xC
nápisem	nápis	k1gInSc7
Myslíme	myslet	k5eAaImIp1nP
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
zahájila	zahájit	k5eAaPmAgFnS
svou	svůj	k3xOyFgFnSc4
předvolební	předvolební	k2eAgFnSc4d1
kampaň	kampaň	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
O	o	k7c6
půlnoci	půlnoc	k1gFnSc6
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2009	#num#	k4
zde	zde	k6eAd1
byla	být	k5eAaImAgFnS
instalována	instalovat	k5eAaBmNgFnS
obří	obří	k2eAgFnSc1d1
modrá	modrý	k2eAgFnSc1d1
plachta	plachta	k1gFnSc1
s	s	k7c7
logem	log	k1gInSc7
EU	EU	kA
<g/>
,	,	kIx,
vcelku	vcelku	k6eAd1
však	však	k9
nevydržela	vydržet	k5eNaPmAgFnS
ani	ani	k8xC
do	do	k7c2
druhého	druhý	k4xOgInSc2
dne	den	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Při	při	k7c6
příležitosti	příležitost	k1gFnSc6
desátého	desátý	k4xOgNnSc2
výročí	výročí	k1gNnSc2
vstupu	vstup	k1gInSc2
do	do	k7c2
EU	EU	kA
a	a	k8xC
pětadvacátého	pětadvacátý	k4xOgInSc2
výročí	výročí	k1gNnSc3
od	od	k7c2
pádu	pád	k1gInSc2
totality	totalita	k1gFnSc2
dne	den	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2014	#num#	k4
uspořádala	uspořádat	k5eAaPmAgFnS
obecně	obecně	k6eAd1
prospěšná	prospěšný	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Opona	opona	k1gFnSc1
pro	pro	k7c4
veřejnost	veřejnost	k1gFnSc4
jednodenní	jednodenní	k2eAgFnSc4d1
akci	akce	k1gFnSc4
–	–	k?
komentované	komentovaný	k2eAgFnPc4d1
prohlídky	prohlídka	k1gFnPc4
rozsáhlých	rozsáhlý	k2eAgFnPc2d1
podzemních	podzemní	k2eAgFnPc2d1
prostor	prostora	k1gFnPc2
pod	pod	k7c7
bývalým	bývalý	k2eAgInSc7d1
Stalinovým	Stalinův	k2eAgInSc7d1
pomníkem	pomník	k1gInSc7
v	v	k7c6
Praze	Praha	k1gFnSc6
na	na	k7c6
Letné	Letná	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prostorách	prostora	k1gFnPc6
pod	pod	k7c7
metronomem	metronom	k1gInSc7
byly	být	k5eAaImAgFnP
(	(	kIx(
<g/>
po	po	k7c6
dvaceti	dvacet	k4xCc6
letech	let	k1gInPc6
<g/>
)	)	kIx)
k	k	k7c3
vidění	vidění	k1gNnSc3
fragmenty	fragment	k1gInPc1
někdejší	někdejší	k2eAgFnSc2d1
monumentální	monumentální	k2eAgFnSc2d1
komunistické	komunistický	k2eAgFnSc2d1
stavby	stavba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veřejnost	veřejnost	k1gFnSc1
byla	být	k5eAaImAgFnS
informována	informovat	k5eAaBmNgFnS
i	i	k9
o	o	k7c6
historických	historický	k2eAgFnPc6d1
podrobnostech	podrobnost	k1gFnPc6
okolo	okolo	k7c2
vzniku	vznik	k1gInSc2
<g/>
,	,	kIx,
stavby	stavba	k1gFnSc2
a	a	k8xC
likvidace	likvidace	k1gFnSc2
pomníku	pomník	k1gInSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2019	#num#	k4
umělecká	umělecký	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
Ztohoven	Ztohovna	k1gFnPc2
na	na	k7c4
památku	památka	k1gFnSc4
50	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc4
sebeupálení	sebeupálení	k1gNnSc2
Jana	Jan	k1gMnSc2
Zajíce	Zajíc	k1gMnSc2
zažehla	zažehnout	k5eAaPmAgFnS
oheň	oheň	k1gInSc4
ve	v	k7c6
dvou	dva	k4xCgFnPc6
bronzových	bronzový	k2eAgFnPc6d1
mísách	mísa	k1gFnPc6
po	po	k7c6
stranách	strana	k1gFnPc6
pomníku	pomník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plameny	plamen	k1gInPc1
měly	mít	k5eAaImAgFnP
zároveň	zároveň	k6eAd1
připomenout	připomenout	k5eAaPmF
Vítězný	vítězný	k2eAgInSc4d1
únor	únor	k1gInSc4
a	a	k8xC
příchod	příchod	k1gInSc4
nového	nový	k2eAgMnSc2d1
vůdce	vůdce	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Únorový	únorový	k2eAgInSc4d1
puč	puč	k1gInSc4
předznamenal	předznamenat	k5eAaPmAgInS
příchod	příchod	k1gInSc1
nového	nový	k2eAgInSc2d1
druhu	druh	k1gInSc2
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
všichni	všechen	k3xTgMnPc1
s	s	k7c7
velkým	velký	k2eAgNnSc7d1
napětím	napětí	k1gNnSc7
očekávali	očekávat	k5eAaImAgMnP
<g/>
,	,	kIx,
nový	nový	k2eAgInSc1d1
model	model	k1gInSc1
vládnutí	vládnutí	k1gNnSc2
byl	být	k5eAaImAgInS
však	však	k9
přetaven	přetavit	k5eAaPmNgInS
do	do	k7c2
kultu	kult	k1gInSc2
diktátora	diktátor	k1gMnSc2
<g/>
,	,	kIx,
kterým	který	k3yQgMnSc7,k3yIgMnSc7,k3yRgMnSc7
byl	být	k5eAaImAgMnS
tehdy	tehdy	k6eAd1
Stalin	Stalin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
skupiny	skupina	k1gFnSc2
se	se	k3xPyFc4
podobná	podobný	k2eAgFnSc1d1
situace	situace	k1gFnSc1
může	moct	k5eAaImIp3nS
nyní	nyní	k6eAd1
zopakovat	zopakovat	k5eAaPmF
a	a	k8xC
kult	kult	k1gInSc1
osobnosti	osobnost	k1gFnSc2
se	se	k3xPyFc4
vrátí	vrátit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
narážce	narážka	k1gFnSc6
na	na	k7c4
příjmení	příjmení	k1gNnSc4
Jana	Jan	k1gMnSc2
Zajíce	Zajíc	k1gMnSc2
Petr	Petr	k1gMnSc1
Žílka	žílka	k1gFnSc1
rovněž	rovněž	k6eAd1
zmínil	zmínit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c4
letošek	letošek	k1gInSc4
připadá	připadat	k5eAaPmIp3nS,k5eAaImIp3nS
padesáté	padesátý	k4xOgNnSc4
výročí	výročí	k1gNnSc4
uvedení	uvedení	k1gNnSc2
sovětského	sovětský	k2eAgInSc2d1
seriálu	seriál	k1gInSc2
pro	pro	k7c4
děti	dítě	k1gFnPc4
„	„	k?
<g/>
Jen	jen	k8xS
počkej	počkat	k5eAaPmRp2nS
<g/>
,	,	kIx,
zajíci	zajíc	k1gMnSc5
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
zapálením	zapálení	k1gNnSc7
ohně	oheň	k1gInSc2
skupina	skupina	k1gFnSc1
musela	muset	k5eAaImAgFnS
z	z	k7c2
mís	mísa	k1gFnPc2
odstranit	odstranit	k5eAaPmF
mnoho	mnoho	k6eAd1
pytlů	pytel	k1gInPc2
odpadků	odpadek	k1gInPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
tam	tam	k6eAd1
za	za	k7c4
mnoho	mnoho	k4c4
desetiletí	desetiletí	k1gNnPc2
bez	bez	k7c2
údržby	údržba	k1gFnSc2
nahromadily	nahromadit	k5eAaPmAgInP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podstavec	podstavec	k1gInSc4
a	a	k8xC
jeho	jeho	k3xOp3gFnPc1
podzemní	podzemní	k2eAgFnPc1d1
prostory	prostora	k1gFnPc1
jsou	být	k5eAaImIp3nP
poškozené	poškozený	k2eAgFnPc1d1
<g/>
,	,	kIx,
betonové	betonový	k2eAgFnPc1d1
konstrukce	konstrukce	k1gFnPc1
se	se	k3xPyFc4
drolí	drolit	k5eAaImIp3nP
<g/>
,	,	kIx,
stropy	strop	k1gInPc4
prosakuje	prosakovat	k5eAaImIp3nS
voda	voda	k1gFnSc1
<g/>
,	,	kIx,
ocelové	ocelový	k2eAgFnPc1d1
výztuže	výztuž	k1gFnPc1
korodují	korodovat	k5eAaImIp3nP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Náklady	náklad	k1gInPc1
na	na	k7c4
opravu	oprava	k1gFnSc4
odborníci	odborník	k1gMnPc1
odhadují	odhadovat	k5eAaImIp3nP
nejméně	málo	k6eAd3
na	na	k7c4
50	#num#	k4
milionů	milion	k4xCgInPc2
korun	koruna	k1gFnPc2
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
dle	dle	k7c2
jiných	jiný	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
jde	jít	k5eAaImIp3nS
o	o	k7c4
částku	částka	k1gFnSc4
mnohem	mnohem	k6eAd1
vyšší	vysoký	k2eAgMnSc1d2
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
podzemních	podzemní	k2eAgFnPc6d1
prostorách	prostora	k1gFnPc6
pod	pod	k7c7
pomníkem	pomník	k1gInSc7
také	také	k6eAd1
proběhl	proběhnout	k5eAaPmAgInS
průzkum	průzkum	k1gInSc1
jeskyňářů	jeskyňář	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Ti	ten	k3xDgMnPc1
neobjevili	objevit	k5eNaPmAgMnP
žádné	žádný	k3yNgFnPc4
další	další	k2eAgFnPc4d1
tajné	tajný	k2eAgFnPc4d1
prostory	prostora	k1gFnPc4
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
ač	ač	k8xS
se	se	k3xPyFc4
o	o	k7c6
nich	on	k3xPp3gMnPc6
v	v	k7c6
minulosti	minulost	k1gFnSc6
často	často	k6eAd1
spekulovalo	spekulovat	k5eAaImAgNnS
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
středu	střed	k1gInSc6
18	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc4
2019	#num#	k4
rozhodl	rozhodnout	k5eAaPmAgInS
stavební	stavební	k2eAgInSc1d1
úřad	úřad	k1gInSc1
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha	Praha	k1gFnSc1
7	#num#	k4
na	na	k7c6
základě	základ	k1gInSc6
posudku	posudek	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
si	se	k3xPyFc3
nechal	nechat	k5eAaPmAgInS
zpracovat	zpracovat	k5eAaPmF
magistrátní	magistrátní	k2eAgInSc1d1
odbor	odbor	k1gInSc1
majetku	majetek	k1gInSc2
či	či	k8xC
pražský	pražský	k2eAgMnSc1d1
radní	radní	k1gMnSc1
pro	pro	k7c4
majetek	majetek	k1gInSc4
Jan	Jan	k1gMnSc1
Chabr	Chabr	k1gMnSc1
<g/>
,	,	kIx,
že	že	k8xS
město	město	k1gNnSc1
musí	muset	k5eAaImIp3nS
okamžitě	okamžitě	k6eAd1
uzavřít	uzavřít	k5eAaPmF
podzemní	podzemní	k2eAgFnSc4d1
část	část	k1gFnSc4
pomníku	pomník	k1gInSc2
a	a	k8xC
do	do	k7c2
pěti	pět	k4xCc2
dnů	den	k1gInPc2
pak	pak	k6eAd1
musí	muset	k5eAaImIp3nP
konstrukci	konstrukce	k1gFnSc4
podepřít	podepřít	k5eAaPmF
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
zůstane	zůstat	k5eAaPmIp3nS
uzavřeno	uzavřít	k5eAaPmNgNnS
i	i	k8xC
horní	horní	k2eAgNnSc1d1
prostranství	prostranství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
město	město	k1gNnSc1
musí	muset	k5eAaImIp3nS
nechat	nechat	k5eAaPmF
provést	provést	k5eAaPmF
podrobnější	podrobný	k2eAgFnSc4d2
analýzu	analýza	k1gFnSc4
včetně	včetně	k7c2
zátěžové	zátěžový	k2eAgFnSc2d1
zkoušky	zkouška	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
základě	základ	k1gInSc6
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
rozhodne	rozhodnout	k5eAaPmIp3nS
o	o	k7c6
dalším	další	k2eAgInSc6d1
postupu	postup	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posudek	posudek	k1gInSc1
<g/>
,	,	kIx,
na	na	k7c6
základě	základ	k1gInSc6
kterého	který	k3yIgNnSc2,k3yRgNnSc2,k3yQgNnSc2
bylo	být	k5eAaImAgNnS
rozhodnutí	rozhodnutí	k1gNnSc1
vydáno	vydat	k5eAaPmNgNnS
<g/>
,	,	kIx,
neobsahoval	obsahovat	k5eNaImAgInS
žádné	žádný	k3yNgInPc4
výpočty	výpočet	k1gInPc4
<g/>
,	,	kIx,
v	v	k7c6
době	doba	k1gFnSc6
vydání	vydání	k1gNnSc2
rozhodnutí	rozhodnutí	k1gNnSc2
pracovníci	pracovník	k1gMnPc1
Kloknerova	Kloknerův	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
teprve	teprve	k6eAd1
vytvářeli	vytvářet	k5eAaImAgMnP
rozšířený	rozšířený	k2eAgInSc4d1
posudek	posudek	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
rozhodnutí	rozhodnutí	k1gNnSc4
stavebního	stavební	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
Praha	Praha	k1gFnSc1
obratem	obrat	k1gInSc7
podzemní	podzemní	k2eAgFnSc1d1
část	část	k1gFnSc1
pomníku	pomník	k1gInSc2
zavřela	zavřít	k5eAaPmAgFnS
a	a	k8xC
zakázala	zakázat	k5eAaPmAgFnS
vstup	vstup	k1gInSc4
i	i	k9
do	do	k7c2
přilehlého	přilehlý	k2eAgNnSc2d1
okolí	okolí	k1gNnSc2
včetně	včetně	k7c2
horního	horní	k2eAgNnSc2d1
prostranství	prostranství	k1gNnSc2
kolem	kolem	k7c2
metronomu	metronom	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průchod	průchod	k1gInSc1
po	po	k7c6
chodníku	chodník	k1gInSc6
ve	v	k7c6
svahu	svah	k1gInSc6
bezprostředně	bezprostředně	k6eAd1
pod	pod	k7c7
památníkem	památník	k1gInSc7
zůstal	zůstat	k5eAaPmAgInS
otevřený	otevřený	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Městská	městský	k2eAgFnSc1d1
policie	policie	k1gFnSc1
dostala	dostat	k5eAaPmAgFnS
pokyn	pokyn	k1gInSc4
věnovat	věnovat	k5eAaPmF,k5eAaImF
dodržování	dodržování	k1gNnSc4
zákazu	zákaz	k1gInSc2
zvýšenou	zvýšený	k2eAgFnSc4d1
pozornost	pozornost	k1gFnSc4
a	a	k8xC
porušení	porušení	k1gNnSc4
zákazu	zákaz	k1gInSc2
pokutovat	pokutovat	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podzemní	podzemní	k2eAgFnPc1d1
prostory	prostora	k1gFnPc1
<g/>
,	,	kIx,
duben	duben	k1gInSc1
2014	#num#	k4
</s>
<s>
Návrhy	návrh	k1gInPc1
na	na	k7c4
další	další	k2eAgNnSc4d1
využití	využití	k1gNnSc4
prostor	prostora	k1gFnPc2
</s>
<s>
V	v	k7c6
červnu	červen	k1gInSc6
2000	#num#	k4
byla	být	k5eAaImAgFnS
vypsána	vypsán	k2eAgFnSc1d1
veřejná	veřejný	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
na	na	k7c4
využití	využití	k1gNnSc4
částí	část	k1gFnPc2
pomníku	pomník	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Vítězným	vítězný	k2eAgInSc7d1
projektem	projekt	k1gInSc7
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
stavba	stavba	k1gFnSc1
oceanária	oceanárium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Metronom	metronom	k1gInSc1
podle	podle	k7c2
plánů	plán	k1gInPc2
měl	mít	k5eAaImAgMnS
zůstat	zůstat	k5eAaPmF
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
realizaci	realizace	k1gFnSc3
nedošlo	dojít	k5eNaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
vedení	vedení	k1gNnSc4
města	město	k1gNnSc2
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
primátorkou	primátorka	k1gFnSc7
Adrianou	Adriana	k1gFnSc7
Krnáčovou	Krnáčův	k2eAgFnSc7d1
navrhlo	navrhnout	k5eAaPmAgNnS
v	v	k7c6
prostorách	prostora	k1gFnPc6
podstavce	podstavec	k1gInSc2
bývalého	bývalý	k2eAgInSc2d1
pomníku	pomník	k1gInSc2
zřídit	zřídit	k5eAaPmF
centrum	centrum	k1gNnSc4
současného	současný	k2eAgNnSc2d1
umění	umění	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
Součástí	součást	k1gFnSc7
jeho	jeho	k3xOp3gFnPc2
příprav	příprava	k1gFnPc2
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
podepsání	podepsání	k1gNnSc1
společného	společný	k2eAgNnSc2d1
memoranda	memorandum	k1gNnSc2
s	s	k7c7
Národní	národní	k2eAgFnSc7d1
galerií	galerie	k1gFnSc7
Praha	Praha	k1gFnSc1
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Město	město	k1gNnSc1
však	však	k9
o	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
od	od	k7c2
memoranda	memorandum	k1gNnSc2
odstoupilo	odstoupit	k5eAaPmAgNnS
a	a	k8xC
deklarovalo	deklarovat	k5eAaBmAgNnS
záměr	záměr	k1gInSc4
prostory	prostora	k1gFnSc2
po	po	k7c6
opravě	oprava	k1gFnSc6
využívat	využívat	k5eAaPmF,k5eAaImF
pouze	pouze	k6eAd1
pro	pro	k7c4
dočasné	dočasný	k2eAgFnPc4d1
kulturní	kulturní	k2eAgFnPc4d1
akce	akce	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stalinův	Stalinův	k2eAgInSc4d1
pomník	pomník	k1gInSc4
v	v	k7c6
umění	umění	k1gNnSc6
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Osudem	osud	k1gInSc7
pomníku	pomník	k1gInSc2
a	a	k8xC
jeho	jeho	k3xOp3gMnSc2,k3xPp3gMnSc2
tvůrce	tvůrce	k1gMnSc2
Otakara	Otakar	k1gMnSc2
Švece	Švec	k1gMnSc2
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
román	román	k1gInSc1
Rudolfa	Rudolf	k1gMnSc2
Cainera	Cainer	k1gMnSc2
(	(	kIx(
<g/>
uváděn	uvádět	k5eAaImNgInS
také	také	k9
jako	jako	k8xS,k8xC
Rudla	Rudla	k1gMnSc1
Cainer	Cainer	k1gMnSc1
<g/>
)	)	kIx)
Žulový	žulový	k2eAgMnSc1d1
Stalin	Stalin	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Film	film	k1gInSc1
</s>
<s>
Stalinův	Stalinův	k2eAgInSc1d1
pomník	pomník	k1gInSc1
byl	být	k5eAaImAgInS
údajně	údajně	k6eAd1
zachycen	zachytit	k5eAaPmNgInS
pouze	pouze	k6eAd1
ve	v	k7c6
čtyrech	čtyr	k1gInPc6
československých	československý	k2eAgInPc6d1
hraných	hraný	k2eAgInPc6d1
filmech	film	k1gInPc6
(	(	kIx(
<g/>
všechny	všechen	k3xTgFnPc1
jsou	být	k5eAaImIp3nP
z	z	k7c2
roku	rok	k1gInSc2
1960	#num#	k4
a	a	k8xC
jeden	jeden	k4xCgInSc1
z	z	k7c2
roku	rok	k1gInSc2
1957	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Valčík	valčík	k1gInSc1
pro	pro	k7c4
milión	milión	k4xCgInSc4
(	(	kIx(
<g/>
barevný	barevný	k2eAgInSc4d1
film	film	k1gInSc4
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Josef	Josef	k1gMnSc1
Mach	Mach	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Holubice	holubice	k1gFnSc1
(	(	kIx(
<g/>
režie	režie	k1gFnSc1
František	František	k1gMnSc1
Vláčil	vláčit	k5eAaImAgMnS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Páté	pátý	k4xOgNnSc1
oddělení	oddělení	k1gNnSc1
(	(	kIx(
<g/>
špionážní	špionážní	k2eAgFnSc1d1
detektivka	detektivka	k1gFnSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Jindřich	Jindřich	k1gMnSc1
Polák	Polák	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
Snadný	snadný	k2eAgInSc1d1
život	život	k1gInSc1
(	(	kIx(
<g/>
režie	režie	k1gFnSc1
Miloš	Miloš	k1gMnSc1
Makovec	Makovec	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
byla	být	k5eAaImAgFnS
na	na	k7c6
místě	místo	k1gNnSc6
pomníku	pomník	k1gInSc2
postavena	postaven	k2eAgFnSc1d1
jeho	jeho	k3xOp3gFnSc1
částečná	částečný	k2eAgFnSc1d1
replika	replika	k1gFnSc1
pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
filmu	film	k1gInSc2
Monstrum	monstrum	k1gNnSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
natočila	natočit	k5eAaBmAgFnS
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
podle	podle	k7c2
scénáře	scénář	k1gInSc2
a	a	k8xC
v	v	k7c6
režii	režie	k1gFnSc6
Viktora	Viktor	k1gMnSc2
Polesného	polesný	k1gMnSc2
(	(	kIx(
<g/>
film	film	k1gInSc1
je	být	k5eAaImIp3nS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
ke	k	k7c3
zhlédnutí	zhlédnutí	k1gNnSc3
na	na	k7c6
webu	web	k1gInSc6
ČT	ČT	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
Inspirací	inspirace	k1gFnPc2
pro	pro	k7c4
film	film	k1gInSc4
byla	být	k5eAaImAgFnS
kniha	kniha	k1gFnSc1
Žulový	žulový	k2eAgMnSc1d1
Stalin	Stalin	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pomníku	pomník	k1gInSc3
je	být	k5eAaImIp3nS
věnován	věnovat	k5eAaPmNgInS,k5eAaImNgInS
13	#num#	k4
<g/>
minutový	minutový	k2eAgInSc1d1
barevný	barevný	k2eAgInSc1d1
dokument	dokument	k1gInSc1
Pomník	pomník	k1gInSc4
lásky	láska	k1gFnSc2
a	a	k8xC
přátelství	přátelství	k1gNnSc2
z	z	k7c2
roku	rok	k1gInSc2
odhalení	odhalení	k1gNnSc2
pomníku	pomník	k1gInSc2
(	(	kIx(
<g/>
1955	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Divadlo	divadlo	k1gNnSc1
</s>
<s>
Národní	národní	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
Praha	Praha	k1gFnSc1
uvedlo	uvést	k5eAaPmAgNnS
31.3	31.3	k4
<g/>
.2017	.2017	k4
operu	opera	k1gFnSc4
Jiřího	Jiří	k1gMnSc2
Kadeřábka	Kadeřábek	k1gMnSc2
Žádný	žádný	k3yNgMnSc1
člověk	člověk	k1gMnSc1
v	v	k7c6
režii	režie	k1gFnSc6
Kathariny	Katharin	k1gInPc4
Schmitt	Schmittum	k1gNnPc2
<g/>
,	,	kIx,
inspirovanou	inspirovaný	k2eAgFnSc4d1
okolnostmi	okolnost	k1gFnPc7
výstavby	výstavba	k1gFnSc2
a	a	k8xC
následného	následný	k2eAgNnSc2d1
zničení	zničení	k1gNnSc2
pomníku	pomník	k1gInSc2
Stalina	Stalin	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Derniéra	derniéra	k1gFnSc1
inscenace	inscenace	k1gFnSc1
proběhla	proběhnout	k5eAaPmAgFnS
19	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Národní	národní	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
Brno	Brno	k1gNnSc4
uvedlo	uvést	k5eAaPmAgNnS
7	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2020	#num#	k4
operu	oprat	k5eAaPmIp1nS
Monument	monument	k1gInSc4
<g/>
,	,	kIx,
inspirovanou	inspirovaný	k2eAgFnSc4d1
osudem	osud	k1gInSc7
Otakara	Otakar	k1gMnSc2
Švece	Švec	k1gMnSc2
a	a	k8xC
vytváření	vytváření	k1gNnSc1
sochy	socha	k1gFnSc2
Stalina	Stalin	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
návrh	návrh	k1gInSc1
pomníku	pomník	k1gInSc2
Český	český	k2eAgInSc1d1
svět	svět	k1gInSc1
<g/>
,	,	kIx,
21.06	21.06	k4
<g/>
.1912	.1912	k4
<g/>
↑	↑	k?
HOUŠKA	Houška	k1gMnSc1
<g/>
,	,	kIx,
Vítězslav	Vítězslav	k1gMnSc1
<g/>
;	;	kIx,
PROCHÁZKA	Procházka	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Věčná	věčný	k2eAgFnSc1d1
Slavia	Slavia	k1gFnSc1
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Olympia	Olympia	k1gFnSc1
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
246	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7376	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
109	#num#	k4
<g/>
-	-	kIx~
<g/>
110	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Konečný	konečný	k2eAgInSc4d1
návrh	návrh	k1gInSc4
pomníku	pomník	k1gInSc2
J.	J.	kA
V.	V.	kA
Stalina	Stalin	k1gMnSc2
na	na	k7c6
Letenské	letenský	k2eAgFnSc6d1
pláni	pláň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1951	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Před	před	k7c7
60	#num#	k4
lety	léto	k1gNnPc7
začala	začít	k5eAaPmAgFnS
na	na	k7c6
Letné	Letná	k1gFnSc6
stavba	stavba	k1gFnSc1
Stalinova	Stalinův	k2eAgInSc2d1
pomníku	pomník	k1gInSc2
|	|	kIx~
Radio	radio	k1gNnSc1
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
www.radio.cz	www.radio.cz	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
TV	TV	kA
dokument	dokument	k1gInSc1
ČT	ČT	kA
<g/>
:	:	kIx,
Osudové	osudový	k2eAgInPc1d1
okamžiky	okamžik	k1gInPc1
–	–	k?
Stalinův	Stalinův	k2eAgInSc4d1
pomník	pomník	k1gInSc4
<g/>
↑	↑	k?
Jan	Jan	k1gMnSc1
Nejedlý	Nejedlý	k1gMnSc1
<g/>
,	,	kIx,
Jakub	Jakub	k1gMnSc1
Šofar	Šofar	k1gMnSc1
<g/>
:	:	kIx,
Po	po	k7c6
práci	práce	k1gFnSc6
legraci	legrace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
BizBooks	BizBooks	k1gInSc1
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
,	,	kIx,
200	#num#	k4
s.	s.	k?
<g/>
1	#num#	k4
2	#num#	k4
Žulový	žulový	k2eAgMnSc1d1
Stalin	Stalin	k1gMnSc1
<g/>
:	:	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
bourala	bourat	k5eAaImAgFnS
"	"	kIx"
<g/>
Fronta	fronta	k1gFnSc1
na	na	k7c4
maso	maso	k1gNnSc4
<g/>
"	"	kIx"
|	|	kIx~
Radio	radio	k1gNnSc1
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
www.radio.cz	www.radio.cz	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Praha	Praha	k1gFnSc1
78	#num#	k4
<g/>
:	:	kIx,
Výraz	výraz	k1gInSc1
vděčnosti	vděčnost	k1gFnSc2
a	a	k8xC
lásky	láska	k1gFnSc2
<g/>
↑	↑	k?
http://www.lidovky.cz/na-vlne-odchodu-se-veze-i-ctvrtnicek-vydelal-jsem-si-32-milionu-koncim-1gy-/lide.aspx?c=A110815_150350_lide_spa	http://www.lidovky.cz/na-vlne-odchodu-se-veze-i-ctvrtnicek-vydelal-jsem-si-32-milionu-koncim-1gy-/lide.aspx?c=A110815_150350_lide_spa	k1gFnSc1
<g/>
↑	↑	k?
https://www.ceskatelevize.cz/ivysilani/10430569360-gottland/213562269910004-misto-stalina,	https://www.ceskatelevize.cz/ivysilani/10430569360-gottland/213562269910004-misto-stalina,	k4
čas	čas	k1gInSc1
11	#num#	k4
<g/>
:	:	kIx,
<g/>
40	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
(	(	kIx(
<g/>
obsahuje	obsahovat	k5eAaImIp3nS
ukázku	ukázka	k1gFnSc4
z	z	k7c2
Událostí	událost	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vlajku	vlajka	k1gFnSc4
EU	EU	kA
na	na	k7c6
Letné	Letná	k1gFnSc6
po	po	k7c6
pár	pár	k4xCyI
hodinách	hodina	k1gFnPc6
někdo	někdo	k3yInSc1
poškodil	poškodit	k5eAaPmAgMnS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Podzemí	podzemí	k1gNnSc4
fronty	fronta	k1gFnSc2
na	na	k7c4
maso	maso	k1gNnSc4
se	se	k3xPyFc4
ve	v	k7c4
čtvrtek	čtvrtek	k1gInSc4
otevře	otevřít	k5eAaPmIp3nS
lidem	člověk	k1gMnPc3
(	(	kIx(
<g/>
29	#num#	k4
<g/>
.	.	kIx.
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
<g/>
:	:	kIx,
adu	adu	k?
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Skupina	skupina	k1gFnSc1
Ztohoven	Ztohovna	k1gFnPc2
ve	v	k7c6
Stalinových	Stalinových	k2eAgFnPc6d1
mísách	mísa	k1gFnPc6
zapálila	zapálit	k5eAaPmAgFnS
oheň	oheň	k1gInSc4
na	na	k7c4
počest	počest	k1gFnSc4
Jana	Jan	k1gMnSc2
Zajíce	Zajíc	k1gMnSc2
<g/>
,	,	kIx,
iDnes	iDnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
25	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
,	,	kIx,
tek	teka	k1gFnPc2
(	(	kIx(
<g/>
Tereza	Tereza	k1gFnSc1
Kučerová	Kučerová	k1gFnSc1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Stalinův	Stalinův	k2eAgInSc4d1
pomník	pomník	k1gInSc4
-	-	kIx~
dnes	dnes	k6eAd1
Metronom	metronom	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letná	letný	k2eAgFnSc1d1
a	a	k8xC
Letenská	letenský	k2eAgFnSc1d1
Pláň	pláň	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Metronom	metronom	k1gInSc1
a	a	k8xC
podzemní	podzemní	k2eAgFnPc1d1
prostory	prostora	k1gFnPc1
pomníku	pomník	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Stalin	Stalin	k1gMnSc1
<g/>
,	,	kIx,
Letná	Letná	k1gFnSc1
<g/>
,	,	kIx,
Stalinův	Stalinův	k2eAgInSc1d1
pomník	pomník	k1gInSc1
<g/>
,	,	kIx,
metronom	metronom	k1gInSc1
<g/>
.	.	kIx.
www.tyrkys.cz	www.tyrkys.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ct	ct	k?
<g/>
24	#num#	k4
<g/>
.	.	kIx.
<g/>
ceskatelevize	ceskatelevize	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Rozhovor	rozhovor	k1gInSc1
s	s	k7c7
pražským	pražský	k2eAgMnSc7d1
jeskyňářem	jeskyňář	k1gMnSc7
(	(	kIx(
<g/>
Portál	portál	k1gInSc4
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
www.praha.eu	www.praha.eu	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Podzemí	podzemí	k1gNnSc1
pod	pod	k7c7
pomníkem	pomník	k1gInSc7
J.V.	J.V.	k1gMnSc2
Stalina	Stalin	k1gMnSc2
bylo	být	k5eAaImAgNnS
vždy	vždy	k6eAd1
obestřeno	obestřít	k5eAaPmNgNnS
záhadami	záhada	k1gFnPc7
<g/>
.	.	kIx.
www.nautilus.cz	www.nautilus.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zuzana	Zuzana	k1gFnSc1
Fojčíková	Fojčíková	k1gFnSc1
<g/>
,	,	kIx,
Karolína	Karolína	k1gFnSc1
Novotná	Novotná	k1gFnSc1
<g/>
:	:	kIx,
Praha	Praha	k1gFnSc1
musí	muset	k5eAaImIp3nS
zavřít	zavřít	k5eAaPmF
podstavec	podstavec	k1gInSc4
Stalinova	Stalinův	k2eAgInSc2d1
pomníku	pomník	k1gInSc2
<g/>
,	,	kIx,
konstrukci	konstrukce	k1gFnSc4
hrozí	hrozit	k5eAaImIp3nS
zřícení	zřícení	k1gNnSc1
<g/>
,	,	kIx,
iDnes	iDnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
19	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
↑	↑	k?
Jakub	Jakub	k1gMnSc1
Augusta	Augusta	k1gMnSc1
<g/>
:	:	kIx,
Skateboardisté	skateboardista	k1gMnPc1
mají	mít	k5eAaImIp3nP
smůlu	smůla	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okolí	okolí	k1gNnPc1
Stalinova	Stalinův	k2eAgInSc2d1
pomníku	pomník	k1gInSc2
je	být	k5eAaImIp3nS
zavřené	zavřený	k2eAgNnSc1d1
<g/>
,	,	kIx,
hrozí	hrozit	k5eAaImIp3nS
pokuta	pokuta	k1gFnSc1
<g/>
,	,	kIx,
iDnes	iDnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
20	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
↑	↑	k?
Zastupitelům	zastupitel	k1gMnPc3
se	se	k3xPyFc4
místo	místo	k7c2
Stalina	Stalin	k1gMnSc2
nejvíce	hodně	k6eAd3,k6eAd1
zamlouvá	zamlouvat	k5eAaImIp3nS
oceanárium	oceanárium	k1gNnSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2001-01-25	2001-01-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Praha	Praha	k1gFnSc1
nepustí	pustit	k5eNaPmIp3nS
na	na	k7c4
Letnou	Letná	k1gFnSc4
žraloky	žralok	k1gMnPc4
ani	ani	k8xC
stadion	stadion	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatím	zatím	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008-12-21	2008-12-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Martina	Martina	k1gFnSc1
Smutná	Smutná	k1gFnSc1
<g/>
:	:	kIx,
Galerie	galerie	k1gFnSc1
na	na	k7c6
Letné	Letná	k1gFnSc6
bude	být	k5eAaImBp3nS
dominantou	dominanta	k1gFnSc7
<g/>
,	,	kIx,
věří	věřit	k5eAaImIp3nS
Krnáčová	Krnáčová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stát	stát	k1gInSc4
by	by	kYmCp3nS
mohla	moct	k5eAaImAgFnS
miliardu	miliarda	k4xCgFnSc4
<g/>
,	,	kIx,
iDnes	iDnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
↑	↑	k?
Archiweb	Archiwba	k1gFnPc2
-	-	kIx~
Fajt	Fajt	k1gInSc4
<g/>
:	:	kIx,
Podobu	podoba	k1gFnSc4
prostor	prostora	k1gFnPc2
pod	pod	k7c7
Stalinem	Stalin	k1gMnSc7
určí	určit	k5eAaPmIp3nS
architektonická	architektonický	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
<g/>
.	.	kIx.
www.archiweb.cz	www.archiweb.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Galerie	galerie	k1gFnSc1
pod	pod	k7c7
pomníkem	pomník	k1gInSc7
Stalina	Stalin	k1gMnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
nebude	být	k5eNaImBp3nS
<g/>
,	,	kIx,
rozhodla	rozhodnout	k5eAaPmAgFnS
rada	rada	k1gFnSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-12-05	2018-12-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Valčík	valčík	k1gInSc1
pro	pro	k7c4
milión	milión	k4xCgInSc4
<g/>
↑	↑	k?
http://www.ceskatelevize.cz/porady/10290259076-monstrum	http://www.ceskatelevize.cz/porady/10290259076-monstrum	k1gNnSc4
<g/>
↑	↑	k?
Tragický	tragický	k2eAgInSc4d1
osud	osud	k1gInSc4
autora	autor	k1gMnSc2
sochy	socha	k1gFnSc2
Stalina	Stalin	k1gMnSc2
na	na	k7c6
pražské	pražský	k2eAgFnSc6d1
Letné	Letná	k1gFnSc6
ožije	ožít	k5eAaPmIp3nS
ve	v	k7c6
filmu	film	k1gInSc6
Monstrum	monstrum	k1gNnSc1
<g/>
↑	↑	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
:	:	kIx,
https://www.youtube.com/watch?v=1BcwDUw42GM	https://www.youtube.com/watch?v=1BcwDUw42GM	k4
<g/>
↑	↑	k?
Inscenace	inscenace	k1gFnSc1
<g/>
.	.	kIx.
vis	vis	k1gInSc1
<g/>
.	.	kIx.
<g/>
idu	ido	k1gNnSc3
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KLEPAL	klepal	k1gMnSc1
<g/>
,	,	kIx,
Boris	Boris	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postavit	postavit	k5eAaPmF
sochu	socha	k1gFnSc4
<g/>
,	,	kIx,
zničit	zničit	k5eAaPmF
sochu	socha	k1gFnSc4
<g/>
,	,	kIx,
zničit	zničit	k5eAaPmF
sebe	sebe	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
opera	opera	k1gFnSc1
Monument	monument	k1gInSc1
je	být	k5eAaImIp3nS
vzácně	vzácně	k6eAd1
angažovaná	angažovaný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
ŠINDELÁŘ	Šindelář	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
máj	máj	k1gFnSc1
-	-	kIx~
kultu	kult	k1gInSc2
čas	čas	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
historie	historie	k1gFnSc2
pražského	pražský	k2eAgInSc2d1
Stalinova	Stalinův	k2eAgInSc2d1
pomníku	pomník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
a	a	k8xC
současnost	současnost	k1gFnSc1
5	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Socialistický	socialistický	k2eAgInSc1d1
realismus	realismus	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Stalinův	Stalinův	k2eAgInSc4d1
pomník	pomník	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Unikátní	unikátní	k2eAgInSc4d1
dokument	dokument	k1gInSc4
Pomník	pomník	k1gInSc4
lásky	láska	k1gFnSc2
a	a	k8xC
přátelství	přátelství	k1gNnSc2
</s>
<s>
Historie	historie	k1gFnSc1
demolice	demolice	k1gFnSc2
pomníku	pomník	k1gInSc2
na	na	k7c6
YouTube	YouTub	k1gInSc5
</s>
<s>
Informace	informace	k1gFnPc1
a	a	k8xC
fotografie	fotografia	k1gFnPc1
na	na	k7c6
serveru	server	k1gInSc6
Totalita	totalita	k1gFnSc1
</s>
<s>
Další	další	k2eAgFnPc1d1
fotografie	fotografia	k1gFnPc1
</s>
<s>
Informace	informace	k1gFnPc1
o	o	k7c6
pomníku	pomník	k1gInSc6
</s>
<s>
Kniha	kniha	k1gFnSc1
Žulový	žulový	k2eAgMnSc1d1
Stalin	Stalin	k1gMnSc1
–	–	k?
poprvé	poprvé	k6eAd1
uveřejňuje	uveřejňovat	k5eAaImIp3nS
fakta	faktum	k1gNnPc4
o	o	k7c6
stavbě	stavba	k1gFnSc6
a	a	k8xC
následné	následný	k2eAgFnSc6d1
demolici	demolice	k1gFnSc6
největšího	veliký	k2eAgInSc2d3
žulového	žulový	k2eAgInSc2d1
pomníku	pomník	k1gInSc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
postaveného	postavený	k2eAgMnSc2d1
na	na	k7c6
Letenské	letenský	k2eAgFnSc6d1
pláni	pláň	k1gFnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
a	a	k8xC
odtajněné	odtajněný	k2eAgInPc1d1
protokoly	protokol	k1gInPc1
o	o	k7c6
sebevraždě	sebevražda	k1gFnSc6
autora	autor	k1gMnSc2
pomníku	pomník	k1gInSc2
akad	akad	k1gMnSc1
<g/>
.	.	kIx.
sochaře	sochař	k1gMnSc2
Otakara	Otakar	k1gMnSc2
Švece	Švec	k1gMnSc2
</s>
<s>
1990	#num#	k4
rockový	rockový	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
fotografie	fotografie	k1gFnSc1
Peter	Petra	k1gFnPc2
Lind	Linda	k1gFnPc2
</s>
<s>
Výraz	výraz	k1gInSc1
vděčnosti	vděčnost	k1gFnSc2
a	a	k8xC
lásky	láska	k1gFnSc2
<g/>
,	,	kIx,
článek	článek	k1gInSc1
(	(	kIx(
<g/>
z	z	k7c2
časopisu	časopis	k1gInSc2
Praha	Praha	k1gFnSc1
78	#num#	k4
<g/>
)	)	kIx)
k	k	k7c3
soutěži	soutěž	k1gFnSc3
o	o	k7c4
nový	nový	k2eAgInSc4d1
pomník	pomník	k1gInSc4
<g/>
:	:	kIx,
Památník	památník	k1gInSc1
osvobození	osvobození	k1gNnSc2
Československa	Československo	k1gNnSc2
Sovětskou	sovětský	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Výběr	výběr	k1gInSc1
známých	známý	k1gMnPc2
výtvarných	výtvarný	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
a	a	k8xC
jejich	jejich	k3xOp3gNnSc4
umístění	umístění	k1gNnSc4
Starověk	starověk	k1gInSc1
<g/>
,	,	kIx,
<g/>
antika	antika	k1gFnSc1
a	a	k8xC
starémimoevropskéumění	starémimoevropskéumění	k1gNnSc1
</s>
<s>
Svět	svět	k1gInSc1
</s>
<s>
Busta	busta	k1gFnSc1
královny	královna	k1gFnSc2
Nefertiti	Nefertiti	k1gFnSc1
(	(	kIx(
<g/>
Berlín	Berlín	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Láokoón	Láokoón	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gMnPc1
synové	syn	k1gMnPc1
(	(	kIx(
<g/>
Řím	Řím	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Moai	Moai	k1gNnSc1
(	(	kIx(
<g/>
Velikonoční	velikonoční	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
pravěké	pravěký	k2eAgFnSc2d1
malby	malba	k1gFnSc2
(	(	kIx(
<g/>
jeskyně	jeskyně	k1gFnSc1
Altamira	Altamira	k1gFnSc1
<g/>
,	,	kIx,
Lascaux	Lascaux	k1gInSc1
aj.	aj.	kA
<g/>
)	)	kIx)
•	•	k?
Rhodský	rhodský	k2eAgInSc4d1
kolos	kolos	k1gInSc4
(	(	kIx(
<g/>
zničen	zničen	k2eAgInSc4d1
<g/>
)	)	kIx)
•	•	k?
Terakotová	terakotový	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
(	(	kIx(
<g/>
Si-an	Si-an	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Tutanchamonova	Tutanchamonův	k2eAgFnSc1d1
pohřební	pohřební	k2eAgFnSc1d1
výbava	výbava	k1gFnSc1
(	(	kIx(
<g/>
Káhira	Káhira	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Velká	velký	k2eAgFnSc1d1
sfinga	sfinga	k1gFnSc1
(	(	kIx(
<g/>
Gíza	Gíza	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Venuše	Venuše	k1gFnSc1
Mélská	Mélská	k1gFnSc1
(	(	kIx(
<g/>
Paříž	Paříž	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Myrón	Myrón	k1gMnSc1
Diskobolos	Diskobolos	k1gMnSc1
(	(	kIx(
<g/>
dochovány	dochován	k2eAgFnPc1d1
kopie	kopie	k1gFnPc1
<g/>
,	,	kIx,
např.	např.	kA
Řím	Řím	k1gInSc1
<g/>
)	)	kIx)
Česko	Česko	k1gNnSc1
</s>
<s>
Hlava	hlava	k1gFnSc1
Kelta	Kelt	k1gMnSc2
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Věstonická	věstonický	k2eAgFnSc1d1
venuše	venuše	k1gFnSc1
(	(	kIx(
<g/>
Brno	Brno	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Středověk	středověk	k1gInSc1
</s>
<s>
Svět	svět	k1gInSc1
</s>
<s>
Kapitolská	kapitolský	k2eAgFnSc1d1
vlčice	vlčice	k1gFnSc1
(	(	kIx(
<g/>
Řím	Řím	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
bratři	bratr	k1gMnPc1
z	z	k7c2
Limburka	Limburek	k1gMnSc2
Přebohaté	přebohatý	k2eAgFnSc2d1
hodinky	hodinka	k1gFnSc2
vévody	vévoda	k1gMnSc2
z	z	k7c2
Berry	Berra	k1gFnSc2
(	(	kIx(
<g/>
Chantilly	Chantilla	k1gFnSc2
u	u	k7c2
Paříže	Paříž	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Eyck	Eyck	k1gInSc1
Svatba	svatba	k1gFnSc1
manželů	manžel	k1gMnPc2
Arnolfiniových	Arnolfiniový	k2eAgMnPc2d1
(	(	kIx(
<g/>
Londýn	Londýn	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Rublev	Rublev	k1gFnSc1
Ikona	ikona	k1gFnSc1
Nejsvětější	nejsvětější	k2eAgFnSc1d1
Trojice	trojice	k1gFnSc1
(	(	kIx(
<g/>
Moskva	Moskva	k1gFnSc1
<g/>
)	)	kIx)
Česko	Česko	k1gNnSc1
</s>
<s>
Kodex	kodex	k1gInSc1
vyšehradský	vyšehradský	k2eAgInSc1d1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Madona	Madona	k1gFnSc1
z	z	k7c2
Veveří	veveří	k2eAgFnSc2d1
(	(	kIx(
<g/>
Brno	Brno	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Palladium	palladium	k1gNnSc4
země	zem	k1gFnSc2
české	český	k2eAgFnSc2d1
(	(	kIx(
<g/>
Stará	starat	k5eAaImIp3nS
Boleslav	Boleslav	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Relikviář	relikviář	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Maura	Maur	k1gMnSc2
(	(	kIx(
<g/>
Bečov	Bečov	k1gInSc1
n.	n.	k?
T.	T.	kA
<g/>
)	)	kIx)
•	•	k?
Svatováclavská	svatováclavský	k2eAgFnSc1d1
zbroj	zbroj	k1gFnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Vyšebrodský	vyšebrodský	k2eAgInSc1d1
cyklus	cyklus	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Zlatá	zlatý	k2eAgFnSc1d1
brána	brána	k1gFnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Theodorik	Theodorika	k1gFnPc2
výzdoba	výzdoba	k1gFnSc1
kaple	kaple	k1gFnSc2
sv.	sv.	kA
Kříže	kříž	k1gInSc2
(	(	kIx(
<g/>
Karlštejn	Karlštejn	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Renesance	renesance	k1gFnSc1
a	a	k8xC
manýrismus	manýrismus	k1gInSc1
</s>
<s>
Svět	svět	k1gInSc1
</s>
<s>
Arcimboldo	Arcimboldo	k1gNnSc1
Čtvero	čtvero	k4xRgNnSc1
ročních	roční	k2eAgNnPc2d1
období	období	k1gNnPc2
(	(	kIx(
<g/>
Vídeň	Vídeň	k1gFnSc1
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
aj.	aj.	kA
<g/>
)	)	kIx)
/	/	kIx~
Rudolf	Rudolf	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
jako	jako	k8xC,k8xS
Vertumnus	Vertumnus	k1gMnSc1
(	(	kIx(
<g/>
Skokloster	Skokloster	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Bosch	Bosch	kA
Zahrada	zahrada	k1gFnSc1
pozemských	pozemský	k2eAgFnPc2d1
rozkoší	rozkoš	k1gFnPc2
(	(	kIx(
<g/>
Madrid	Madrid	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Botticelli	Botticell	k1gMnPc1
Primavera	Primavero	k1gNnSc2
a	a	k8xC
Zrození	zrození	k1gNnSc2
Venuše	Venuše	k1gFnSc2
(	(	kIx(
<g/>
Florencie	Florencie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Brueghel	Brueghel	k1gMnSc1
st.	st.	kA
Stavba	stavba	k1gFnSc1
babylonské	babylonský	k2eAgFnSc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
věže	věž	k1gFnPc1
(	(	kIx(
<g/>
Vídeň	Vídeň	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Donatello	Donatello	k1gNnSc1
David	David	k1gMnSc1
(	(	kIx(
<g/>
Florencie	Florencie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Dürer	Dürer	k1gInSc1
Melancholie	melancholie	k1gFnSc1
I	i	k9
(	(	kIx(
<g/>
více	hodně	k6eAd2
výtisků	výtisk	k1gInPc2
<g/>
)	)	kIx)
•	•	k?
Grünewald	Grünewald	k1gInSc1
Isenheimský	Isenheimský	k2eAgInSc1d1
oltář	oltář	k1gInSc1
(	(	kIx(
<g/>
Kolmar	Kolmar	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Leonardo	Leonardo	k1gMnSc1
Dáma	dáma	k1gFnSc1
s	s	k7c7
hranostajem	hranostaj	k1gMnSc7
(	(	kIx(
<g/>
Krakov	Krakov	k1gInSc1
<g/>
)	)	kIx)
/	/	kIx~
Madona	Madona	k1gFnSc1
ve	v	k7c6
skalách	skála	k1gFnPc6
a	a	k8xC
Mona	Mon	k2eAgFnSc1d1
Lisa	Lisa	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
Paříž	Paříž	k1gFnSc1
<g/>
)	)	kIx)
/	/	kIx~
Poslední	poslední	k2eAgFnSc1d1
večeře	večeře	k1gFnSc1
(	(	kIx(
<g/>
Milán	Milán	k1gInSc1
<g/>
)	)	kIx)
/	/	kIx~
Vitruviánský	Vitruviánský	k2eAgMnSc1d1
muž	muž	k1gMnSc1
(	(	kIx(
<g/>
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
•	•	k?
Michelangelo	Michelangela	k1gFnSc5
David	David	k1gMnSc1
(	(	kIx(
<g/>
Florencie	Florencie	k1gFnSc1
<g/>
)	)	kIx)
/	/	kIx~
Pieta	pieta	k1gFnSc1
(	(	kIx(
<g/>
Řím	Řím	k1gInSc1
<g/>
)	)	kIx)
/	/	kIx~
Poslední	poslední	k2eAgInSc1d1
soud	soud	k1gInSc1
a	a	k8xC
Stvoření	stvoření	k1gNnSc1
Adama	Adam	k1gMnSc2
(	(	kIx(
<g/>
Řím	Řím	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Raffael	Raffael	k1gInSc1
Raffaelovy	Raffaelův	k2eAgInPc1d1
sály	sál	k1gInPc1
(	(	kIx(
<g/>
Řím	Řím	k1gInSc1
<g/>
)	)	kIx)
/	/	kIx~
Sixtinská	sixtinský	k2eAgFnSc1d1
madona	madona	k1gFnSc1
(	(	kIx(
<g/>
Drážďany	Drážďany	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
Tizian	Tiziana	k1gFnPc2
Urbinská	Urbinský	k2eAgFnSc1d1
Venuše	Venuše	k1gFnSc1
(	(	kIx(
<g/>
Florencie	Florencie	k1gFnSc1
<g/>
)	)	kIx)
Česko	Česko	k1gNnSc1
</s>
<s>
Pražské	pražský	k2eAgNnSc1d1
Jezulátko	Jezulátko	k1gNnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Dürer	Dürra	k1gFnPc2
Růžencová	růžencový	k2eAgFnSc1d1
slavnost	slavnost	k1gFnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Tizian	Tizian	k1gMnSc1
Apollo	Apollo	k1gMnSc1
a	a	k8xC
Marsyas	Marsyas	k1gMnSc1
(	(	kIx(
<g/>
Kroměříž	Kroměříž	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Baroko	baroko	k1gNnSc1
a	a	k8xC
rokoko	rokoko	k1gNnSc1
</s>
<s>
Svět	svět	k1gInSc1
</s>
<s>
Bernini	Bernin	k2eAgMnPc1d1
Extáze	extáze	k1gFnSc2
svaté	svatý	k2eAgFnSc2d1
Terezy	Tereza	k1gFnSc2
(	(	kIx(
<g/>
Řím	Řím	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Caravaggio	Caravaggio	k6eAd1
Povolání	povolání	k1gNnSc1
svatého	svatý	k2eAgMnSc2d1
Matouše	Matouš	k1gMnSc2
(	(	kIx(
<g/>
Řím	Řím	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Duquesnoy	Duquesnoa	k1gFnSc2
Čurající	čurající	k2eAgMnSc1d1
chlapeček	chlapeček	k1gMnSc1
(	(	kIx(
<g/>
Brusel	Brusel	k1gInSc1
<g/>
,	,	kIx,
originál	originál	k1gInSc1
v	v	k7c6
městském	městský	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
<g/>
)	)	kIx)
•	•	k?
Rembrandt	Rembrandt	k1gInSc1
Anatomie	anatomie	k1gFnSc1
doktora	doktor	k1gMnSc2
Tulpa	Tulp	k1gMnSc2
(	(	kIx(
<g/>
Haag	Haag	k1gInSc1
<g/>
)	)	kIx)
/	/	kIx~
Noční	noční	k2eAgFnSc1d1
hlídka	hlídka	k1gFnSc1
(	(	kIx(
<g/>
Amsterdam	Amsterdam	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Rubens	Rubens	k1gInSc1
Vztyčení	vztyčení	k1gNnSc2
kříže	kříž	k1gInSc2
(	(	kIx(
<g/>
Antverpy	Antverpy	k1gFnPc1
<g/>
)	)	kIx)
•	•	k?
Salvi	Salev	k1gFnSc3
Fontána	fontána	k1gFnSc1
di	di	k?
Trevi	Treev	k1gFnSc3
(	(	kIx(
<g/>
Řím	Řím	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Velázquez	Velázquez	k1gInSc4
Dvorní	dvorní	k2eAgFnSc2d1
dámy	dáma	k1gFnSc2
(	(	kIx(
<g/>
Madrid	Madrid	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Vermeer	Vermeer	k1gInSc1
Dívka	dívka	k1gFnSc1
s	s	k7c7
perlou	perla	k1gFnSc7
(	(	kIx(
<g/>
Haag	Haag	k1gInSc1
<g/>
)	)	kIx)
Česko	Česko	k1gNnSc1
</s>
<s>
Sloup	sloup	k1gInSc1
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
(	(	kIx(
<g/>
Olomouc	Olomouc	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Sochy	socha	k1gFnPc1
na	na	k7c6
Karlově	Karlův	k2eAgInSc6d1
mostě	most	k1gInSc6
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Bendl	Bendl	k1gFnSc3
Mariánský	mariánský	k2eAgInSc1d1
sloup	sloup	k1gInSc1
•	•	k?
Braun	Braun	k1gMnSc1
výzdoba	výzdoba	k1gFnSc1
Hospitálu	hospitál	k1gInSc2
a	a	k8xC
Betlém	Betlém	k1gInSc1
(	(	kIx(
<g/>
Kuks	Kuks	k1gInSc1
a	a	k8xC
okolí	okolí	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Klasicismus	klasicismus	k1gInSc1
až	až	k9
realismus	realismus	k1gInSc1
</s>
<s>
Svět	svět	k1gInSc1
</s>
<s>
Bartholdi	Barthold	k1gMnPc1
Socha	Socha	k1gMnSc1
Svobody	Svoboda	k1gMnSc2
(	(	kIx(
<g/>
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
David	David	k1gMnSc1
Maratova	Maratův	k2eAgFnSc1d1
smrt	smrt	k1gFnSc1
(	(	kIx(
<g/>
Brusel	Brusel	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Delacroix	Delacroix	k1gInSc1
Svoboda	Svoboda	k1gMnSc1
vede	vést	k5eAaImIp3nS
lid	lid	k1gInSc1
na	na	k7c4
barikády	barikáda	k1gFnPc4
(	(	kIx(
<g/>
Paříž	Paříž	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Falconet	Falconet	k1gInSc1
Měděný	měděný	k2eAgMnSc1d1
jezdec	jezdec	k1gMnSc1
(	(	kIx(
<g/>
Petrohrad	Petrohrad	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Friedrich	Friedrich	k1gMnSc1
Poutník	poutník	k1gMnSc1
nad	nad	k7c7
mořem	moře	k1gNnSc7
mlhy	mlha	k1gFnSc2
(	(	kIx(
<g/>
Hamburk	Hamburk	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Goya	Goy	k2eAgFnSc1d1
Nahá	nahý	k2eAgFnSc1d1
Maja	Maja	k1gFnSc1
a	a	k8xC
Popravy	poprava	k1gFnPc1
3	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1808	#num#	k4
(	(	kIx(
<g/>
Madrid	Madrid	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Hokusai	Hokusae	k1gFnSc4
36	#num#	k4
pohledů	pohled	k1gInPc2
na	na	k7c4
horu	hora	k1gFnSc4
Fudži	Fudž	k1gFnSc3
(	(	kIx(
<g/>
více	hodně	k6eAd2
exemplářů	exemplář	k1gInPc2
<g/>
)	)	kIx)
•	•	k?
Repin	Repin	k1gMnSc1
Burlaci	Burlace	k1gFnSc4
na	na	k7c6
Volze	Volha	k1gFnSc6
a	a	k8xC
Záporožští	záporožský	k2eAgMnPc1d1
kozáci	kozák	k1gMnPc1
píší	psát	k5eAaImIp3nP
dopis	dopis	k1gInSc4
tureckému	turecký	k2eAgMnSc3d1
sultánovi	sultán	k1gMnSc3
(	(	kIx(
<g/>
Petrohrad	Petrohrad	k1gInSc1
<g/>
)	)	kIx)
Česko	Česko	k1gNnSc1
</s>
<s>
Marold	Marold	k6eAd1
Bitva	bitva	k1gFnSc1
u	u	k7c2
Lipan	lipan	k1gMnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Myslbek	Myslbek	k1gInSc1
Pomník	pomník	k1gInSc1
sv.	sv.	kA
Václava	Václav	k1gMnSc2
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Klasická	klasický	k2eAgFnSc1d1
moderna	moderna	k1gFnSc1
</s>
<s>
Svět	svět	k1gInSc1
</s>
<s>
Böcklin	Böcklin	k2eAgInSc1d1
Ostrov	ostrov	k1gInSc1
mrtvých	mrtvý	k1gMnPc2
(	(	kIx(
<g/>
Basilej	Basilej	k1gFnSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
Berlín	Berlín	k1gInSc1
a	a	k8xC
Lipsko	Lipsko	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Borglum	Borglum	k1gInSc1
Mount	Mount	k1gInSc1
Rushmore	Rushmor	k1gInSc5
(	(	kIx(
<g/>
Keyston	Keyston	k1gInSc4
<g/>
)	)	kIx)
•	•	k?
Dalí	Dalí	k2eAgFnSc1d1
Persistence	persistence	k1gFnSc1
paměti	paměť	k1gFnSc2
(	(	kIx(
<g/>
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Duchamp	Duchamp	k1gInSc1
Akt	akt	k1gInSc1
sestupující	sestupující	k2eAgInSc1d1
se	s	k7c7
schodů	schod	k1gInPc2
(	(	kIx(
<g/>
Filadelfie	Filadelfie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Eriksen	Eriksna	k1gFnPc2
Malá	malý	k2eAgFnSc1d1
mořská	mořský	k2eAgFnSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
víla	víla	k1gFnSc1
(	(	kIx(
<g/>
Kodaň	Kodaň	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Gogh	Gogha	k1gFnPc2
Hvězdná	hvězdný	k2eAgFnSc1d1
noc	noc	k1gFnSc1
(	(	kIx(
<g/>
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
)	)	kIx)
/	/	kIx~
Slunečnice	slunečnice	k1gFnSc1
(	(	kIx(
<g/>
více	hodně	k6eAd2
verzí	verze	k1gFnSc7
<g/>
,	,	kIx,
např.	např.	kA
Mnichov	Mnichov	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Hopper	Hopper	k1gInSc4
Noční	noční	k2eAgMnPc1d1
ptáci	pták	k1gMnPc1
(	(	kIx(
<g/>
Chicago	Chicago	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Kahlo	Kahlo	k1gNnSc1
Dvě	dva	k4xCgFnPc1
Fridy	Frida	k1gFnPc1
(	(	kIx(
<g/>
Mexiko	Mexiko	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Klimt	Klimt	k2eAgInSc4d1
Polibek	polibek	k1gInSc4
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
Vídeň	Vídeň	k1gFnSc1
<g/>
)	)	kIx)
/	/	kIx~
Zlatá	zlatý	k2eAgFnSc1d1
Adele	Adele	k1gFnSc1
(	(	kIx(
<g/>
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Landowski	Landowski	k1gNnSc1
a	a	k8xC
Leonida	Leonida	k1gFnSc1
Kristus	Kristus	k1gMnSc1
Spasitel	spasitel	k1gMnSc1
(	(	kIx(
<g/>
Rio	Rio	k1gMnSc1
de	de	k?
Janeiro	Janeiro	k1gNnSc4
<g/>
)	)	kIx)
•	•	k?
Malevič	Malevič	k1gInSc1
Černý	Černý	k1gMnSc1
čtverec	čtverec	k1gInSc1
(	(	kIx(
<g/>
nejstarší	starý	k2eAgFnSc1d3
verze	verze	k1gFnSc1
Moskva	Moskva	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Manet	manet	k1gInSc1
Snídaně	snídaně	k1gFnSc2
v	v	k7c6
trávě	tráva	k1gFnSc6
(	(	kIx(
<g/>
Paříž	Paříž	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Monet	moneta	k1gFnPc2
Imprese	imprese	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
východ	východ	k1gInSc1
slunce	slunce	k1gNnSc2
(	(	kIx(
<g/>
Paříž	Paříž	k1gFnSc1
<g/>
)	)	kIx)
/	/	kIx~
Lekníny	leknín	k1gInPc1
(	(	kIx(
<g/>
asi	asi	k9
250	#num#	k4
verzí	verze	k1gFnPc2
<g/>
,	,	kIx,
např.	např.	kA
Paříž	Paříž	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Muchina	Muchina	k1gMnSc1
Dělník	dělník	k1gMnSc1
a	a	k8xC
kolchoznice	kolchoznice	k1gFnSc1
(	(	kIx(
<g/>
Moskva	Moskva	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Munch	Munch	k1gInSc1
Výkřik	výkřik	k1gInSc1
(	(	kIx(
<g/>
Oslo	Oslo	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Picasso	Picassa	k1gFnSc5
Avignonské	avignonský	k2eAgFnPc1d1
slečny	slečna	k1gFnPc1
(	(	kIx(
<g/>
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
)	)	kIx)
/	/	kIx~
Guernica	Guernica	k1gFnSc1
(	(	kIx(
<g/>
Madrid	Madrid	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Rodin	rodina	k1gFnPc2
Myslitel	myslitel	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
odlitek	odlitek	k1gInSc1
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
)	)	kIx)
Česko	Česko	k1gNnSc1
</s>
<s>
Mucha	Mucha	k1gMnSc1
Slovanská	slovanský	k2eAgFnSc1d1
epopej	epopej	k1gFnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Rousseau	Rousseau	k1gMnSc1
Vlastní	vlastnit	k5eAaImIp3nS
podobizna	podobizna	k1gFnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Šaloun	Šaloun	k1gInSc1
Pomník	pomník	k1gInSc1
mistra	mistr	k1gMnSc2
Jana	Jan	k1gMnSc2
Husa	Hus	k1gMnSc2
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Poválečné	poválečný	k2eAgNnSc1d1
umění	umění	k1gNnSc1
</s>
<s>
Svět	svět	k1gInSc1
</s>
<s>
Pollock	Pollock	k6eAd1
No	no	k9
<g/>
.	.	kIx.
5	#num#	k4
<g/>
,	,	kIx,
1948	#num#	k4
•	•	k?
Vučetič	Vučetič	k1gMnSc1
a	a	k8xC
Nikitin	Nikitin	k1gMnSc1
Matka	matka	k1gFnSc1
vlast	vlast	k1gFnSc1
volá	volat	k5eAaImIp3nS
(	(	kIx(
<g/>
Volgograd	Volgograd	k1gInSc1
<g/>
)	)	kIx)
Česko	Česko	k1gNnSc1
</s>
<s>
Černý	Černý	k1gMnSc1
Miminka	miminko	k1gNnSc2
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Rujbr	Rujbr	k1gMnSc1
a	a	k8xC
Kameník	Kameník	k1gMnSc1
Brněnský	brněnský	k2eAgInSc1d1
orloj	orloj	k1gInSc1
•	•	k?
Svolinský	Svolinský	k2eAgInSc1d1
Olomoucký	olomoucký	k2eAgInSc1d1
orloj	orloj	k1gInSc1
•	•	k?
Švec	Švec	k1gMnSc1
Stalinův	Stalinův	k2eAgInSc4d1
pomník	pomník	k1gInSc4
(	(	kIx(
<g/>
zničen	zničen	k2eAgInSc4d1
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Galerie	galerie	k1gFnSc1
umění	umění	k1gNnSc2
•	•	k?
Korunovační	korunovační	k2eAgInPc4d1
klenoty	klenot	k1gInPc4
•	•	k?
Sedm	sedm	k4xCc4
divů	div	k1gInPc2
světa	svět	k1gInSc2
•	•	k?
Seznam	seznam	k1gInSc1
malířů	malíř	k1gMnPc2
•	•	k?
Seznam	seznam	k1gInSc4
národních	národní	k2eAgFnPc2d1
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
•	•	k?
Seznam	seznam	k1gInSc1
českých	český	k2eAgNnPc2d1
muzeí	muzeum	k1gNnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Komunistický	komunistický	k2eAgInSc1d1
režim	režim	k1gInSc1
v	v	k7c6
Československu	Československo	k1gNnSc6
|	|	kIx~
Praha	Praha	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
314878994	#num#	k4
</s>
