<s>
Leopold	Leopold	k1gMnSc1	Leopold
Sviták	Sviták	k1gMnSc1	Sviták
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1856	[number]	k4	1856
Frenštát	Frenštát	k1gInSc1	Frenštát
pod	pod	k7c7	pod
Radhoštěm	Radhošť	k1gInSc7	Radhošť
–	–	k?	–
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1931	[number]	k4	1931
Hukvaldy	Hukvaldy	k1gInPc4	Hukvaldy
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
automobilový	automobilový	k2eAgMnSc1d1	automobilový
vynálezce	vynálezce	k1gMnSc1	vynálezce
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1897	[number]	k4	1897
až	až	k9	až
1898	[number]	k4	1898
sestavil	sestavit	k5eAaPmAgInS	sestavit
první	první	k4xOgInSc4	první
rakousko-uherský	rakouskoherský	k2eAgInSc4d1	rakousko-uherský
osobní	osobní	k2eAgInSc4d1	osobní
automobil	automobil	k1gInSc4	automobil
NW	NW	kA	NW
Präsident	Präsident	k1gMnSc1	Präsident
<g/>
.	.	kIx.	.
</s>
<s>
Nejmladší	mladý	k2eAgMnSc1d3	nejmladší
z	z	k7c2	z
dětí	dítě	k1gFnPc2	dítě
Josefa	Josef	k1gMnSc2	Josef
Svitáka	Sviták	k1gMnSc2	Sviták
Leopold	Leopold	k1gMnSc1	Leopold
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1856	[number]	k4	1856
ve	v	k7c6	v
Frenštátě	Frenštát	k1gInSc6	Frenštát
pod	pod	k7c7	pod
Radhoštěm	Radhošť	k1gInSc7	Radhošť
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
všichni	všechen	k3xTgMnPc1	všechen
jeho	jeho	k3xOp3gMnPc1	jeho
sourozenci	sourozenec	k1gMnPc1	sourozenec
se	se	k3xPyFc4	se
narodili	narodit	k5eAaPmAgMnP	narodit
na	na	k7c6	na
Hukvaldech	Hukvaldy	k1gInPc6	Hukvaldy
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
obecné	obecný	k2eAgFnSc2d1	obecná
školy	škola	k1gFnSc2	škola
chodil	chodit	k5eAaImAgMnS	chodit
na	na	k7c6	na
Hukvaldech	Hukvaldy	k1gInPc6	Hukvaldy
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
spolužákem	spolužák	k1gMnSc7	spolužák
hukvaldského	hukvaldský	k2eAgMnSc2d1	hukvaldský
rodáka	rodák	k1gMnSc2	rodák
a	a	k8xC	a
budoucího	budoucí	k2eAgMnSc2d1	budoucí
geniálního	geniální	k2eAgMnSc2d1	geniální
hudebního	hudební	k2eAgMnSc2d1	hudební
skladatele	skladatel	k1gMnSc2	skladatel
Leoše	Leoš	k1gMnSc2	Leoš
Janáčka	Janáček	k1gMnSc2	Janáček
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1865	[number]	k4	1865
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
na	na	k7c4	na
"	"	kIx"	"
<g/>
hlavní	hlavní	k2eAgFnSc4d1	hlavní
školu	škola	k1gFnSc4	škola
<g/>
"	"	kIx"	"
u	u	k7c2	u
piaristů	piarista	k1gMnPc2	piarista
v	v	k7c6	v
Příboře	Příbor	k1gInSc6	Příbor
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1866	[number]	k4	1866
až	až	k9	až
1869	[number]	k4	1869
studoval	studovat	k5eAaImAgInS	studovat
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
reálce	reálka	k1gFnSc6	reálka
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Jičíně	Jičín	k1gInSc6	Jičín
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úmrtí	úmrtí	k1gNnSc6	úmrtí
matky	matka	k1gFnSc2	matka
Josefy	Josefa	k1gFnSc2	Josefa
(	(	kIx(	(
<g/>
Leopoldovi	Leopold	k1gMnSc3	Leopold
bylo	být	k5eAaImAgNnS	být
pouhých	pouhý	k2eAgNnPc2d1	pouhé
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
otec	otec	k1gMnSc1	otec
znovu	znovu	k6eAd1	znovu
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1864	[number]	k4	1864
<g/>
)	)	kIx)	)
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Marianou	Mariana	k1gFnSc7	Mariana
Kubošovou	Kubošův	k2eAgFnSc7d1	Kubošova
–	–	k?	–
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
příliš	příliš	k6eAd1	příliš
nepřála	přát	k5eNaImAgFnS	přát
studiu	studio	k1gNnSc6	studio
svého	svůj	k3xOyFgMnSc2	svůj
nevlastního	vlastní	k2eNgMnSc2d1	nevlastní
syna	syn	k1gMnSc2	syn
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
Leopold	Leopold	k1gMnSc1	Leopold
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
13	[number]	k4	13
letech	let	k1gInPc6	let
dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1869	[number]	k4	1869
do	do	k7c2	do
učení	učení	k1gNnSc2	učení
u	u	k7c2	u
zámečnického	zámečnický	k2eAgMnSc2d1	zámečnický
mistra	mistr	k1gMnSc2	mistr
Karla	Karel	k1gMnSc2	Karel
Nesvadby	Nesvadba	k1gMnSc2	Nesvadba
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
<g/>
.	.	kIx.	.
</s>
<s>
Tovaryšem	tovaryš	k1gMnSc7	tovaryš
zámečnickým	zámečnický	k2eAgFnPc3d1	zámečnická
byl	být	k5eAaImAgMnS	být
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
přesně	přesně	k6eAd1	přesně
na	na	k7c4	na
den	den	k1gInSc4	den
o	o	k7c4	o
2	[number]	k4	2
roky	rok	k1gInPc7	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
nato	nato	k6eAd1	nato
–	–	k?	–
dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1872	[number]	k4	1872
–	–	k?	–
si	se	k3xPyFc3	se
nechává	nechávat	k5eAaImIp3nS	nechávat
hukvaldskou	hukvaldský	k2eAgFnSc7d1	Hukvaldská
obecní	obecní	k2eAgFnSc7d1	obecní
radou	rada	k1gFnSc7	rada
vystavit	vystavit	k5eAaPmF	vystavit
pracovní	pracovní	k2eAgFnSc4d1	pracovní
knížku	knížka	k1gFnSc4	knížka
č.	č.	k?	č.
109	[number]	k4	109
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Tři	tři	k4xCgMnPc4	tři
krále	král	k1gMnPc4	král
roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
se	se	k3xPyFc4	se
pouští	pouštět	k5eAaImIp3nS	pouštět
se	s	k7c7	s
zlatkou	zlatka	k1gFnSc7	zlatka
v	v	k7c6	v
kapse	kapsa	k1gFnSc6	kapsa
a	a	k8xC	a
s	s	k7c7	s
bochníkem	bochník	k1gInSc7	bochník
chleba	chléb	k1gInSc2	chléb
v	v	k7c6	v
ranci	ranec	k1gInSc6	ranec
na	na	k7c4	na
tehdy	tehdy	k6eAd1	tehdy
obvyklý	obvyklý	k2eAgInSc4d1	obvyklý
"	"	kIx"	"
<g/>
vandr	vandr	k1gInSc4	vandr
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
různých	různý	k2eAgFnPc6d1	různá
veselých	veselá	k1gFnPc6	veselá
i	i	k8xC	i
smutných	smutný	k2eAgFnPc6d1	smutná
příhodách	příhoda	k1gFnPc6	příhoda
<g/>
,	,	kIx,	,
na	na	k7c4	na
něž	jenž	k3xRgMnPc4	jenž
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
často	často	k6eAd1	často
vzpomínal	vzpomínat	k5eAaImAgMnS	vzpomínat
<g/>
,	,	kIx,	,
dorazil	dorazit	k5eAaPmAgMnS	dorazit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1875	[number]	k4	1875
do	do	k7c2	do
Lubna	Lubn	k1gInSc2	Lubn
(	(	kIx(	(
<g/>
Leoben	Leobna	k1gFnPc2	Leobna
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Štýrsku	Štýrsko	k1gNnSc6	Štýrsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přijal	přijmout	k5eAaPmAgInS	přijmout
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1875	[number]	k4	1875
místo	místo	k7c2	místo
zámečnického	zámečnický	k2eAgMnSc2d1	zámečnický
pomocníka	pomocník	k1gMnSc2	pomocník
u	u	k7c2	u
mistra	mistr	k1gMnSc2	mistr
zámečníka	zámečník	k1gMnSc2	zámečník
Josefa	Josef	k1gMnSc2	Josef
Kleina	Klein	k1gMnSc2	Klein
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
toho	ten	k3xDgNnSc2	ten
pobyl	pobýt	k5eAaPmAgMnS	pobýt
–	–	k?	–
s	s	k7c7	s
menším	malý	k2eAgNnSc7d2	menší
přerušením	přerušení	k1gNnSc7	přerušení
–	–	k?	–
až	až	k9	až
do	do	k7c2	do
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1877	[number]	k4	1877
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Vídeňského	vídeňský	k2eAgNnSc2d1	Vídeňské
Nového	Nového	k2eAgNnSc2d1	Nového
Města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
Wiener	Wiener	k1gMnSc1	Wiener
Neustadt	Neustadt	k1gMnSc1	Neustadt
<g/>
)	)	kIx)	)
a	a	k8xC	a
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
strojního	strojní	k2eAgNnSc2d1	strojní
zámečnictví	zámečnictví	k1gNnSc2	zámečnictví
firmy	firma	k1gFnSc2	firma
Prunner	Prunnra	k1gFnPc2	Prunnra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zastával	zastávat	k5eAaImAgMnS	zastávat
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
jedenadvaceti	jedenadvacet	k4xCc6	jedenadvacet
letech	léto	k1gNnPc6	léto
funkci	funkce	k1gFnSc4	funkce
mistra	mistr	k1gMnSc2	mistr
<g/>
.	.	kIx.	.
</s>
<s>
Blízkost	blízkost	k1gFnSc1	blízkost
Vídně	Vídeň	k1gFnSc2	Vídeň
lákala	lákat	k5eAaImAgFnS	lákat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
Leopold	Leopold	k1gMnSc1	Leopold
záhy	záhy	k6eAd1	záhy
odchází	odcházet	k5eAaImIp3nS	odcházet
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1884	[number]	k4	1884
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
naléhání	naléhání	k1gNnPc4	naléhání
svého	svůj	k3xOyFgNnSc2	svůj
tehdy	tehdy	k6eAd1	tehdy
již	již	k6eAd1	již
čtyřiasedmdesátiletého	čtyřiasedmdesátiletý	k2eAgMnSc2d1	čtyřiasedmdesátiletý
otce	otec	k1gMnSc2	otec
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
rodného	rodný	k2eAgInSc2d1	rodný
kraje	kraj	k1gInSc2	kraj
a	a	k8xC	a
přijímá	přijímat	k5eAaImIp3nS	přijímat
dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1884	[number]	k4	1884
místo	místo	k7c2	místo
předního	přední	k2eAgMnSc2d1	přední
dělníka	dělník	k1gMnSc2	dělník
u	u	k7c2	u
Ignáce	Ignác	k1gMnSc2	Ignác
Schustaly	Schustaly	k1gMnSc2	Schustaly
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
vagonce	vagonce	k?	vagonce
v	v	k7c6	v
Kopřivnici	Kopřivnice	k1gFnSc6	Kopřivnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
tato	tento	k3xDgFnSc1	tento
firma	firma	k1gFnSc1	firma
vyrobila	vyrobit	k5eAaPmAgFnS	vyrobit
právě	právě	k9	právě
stý	stý	k4xOgInSc4	stý
vagon	vagon	k1gInSc4	vagon
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
zakladatel	zakladatel	k1gMnSc1	zakladatel
kopřivnické	kopřivnický	k2eAgFnSc2d1	kopřivnická
kočárovky	kočárovka	k1gFnSc2	kočárovka
a	a	k8xC	a
vagonky	vagonek	k1gInPc4	vagonek
Ignác	Ignác	k1gMnSc1	Ignác
Schustala	Schustala	k1gMnSc1	Schustala
se	se	k3xPyFc4	se
učil	učít	k5eAaPmAgMnS	učít
právě	právě	k9	právě
u	u	k7c2	u
Leopoldova	Leopoldův	k2eAgMnSc2d1	Leopoldův
otce	otec	k1gMnSc2	otec
Josefa	Josef	k1gMnSc2	Josef
<g/>
,	,	kIx,	,
tak	tak	k9	tak
jak	jak	k8xS	jak
můžeme	moct	k5eAaImIp1nP	moct
vyčíst	vyčíst	k5eAaPmF	vyčíst
ze	z	k7c2	z
zápisu	zápis	k1gInSc2	zápis
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1842	[number]	k4	1842
ze	z	k7c2	z
Schustalovy	Schustalův	k2eAgFnSc2d1	Schustalův
vandrovní	vandrovní	k2eAgFnSc2d1	vandrovní
knížky	knížka	k1gFnSc2	knížka
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vyprávění	vyprávění	k1gNnSc2	vyprávění
Adofla	Adofla	k1gMnSc2	Adofla
Pítra	Pítr	k1gMnSc2	Pítr
Bartoně	Bartoň	k1gMnSc2	Bartoň
mladšího	mladý	k2eAgMnSc2d2	mladší
stavěl	stavět	k5eAaImAgMnS	stavět
Josef	Josef	k1gMnSc1	Josef
Sviták	Sviták	k1gMnSc1	Sviták
<g/>
,	,	kIx,	,
panský	panský	k2eAgMnSc1d1	panský
sedlář	sedlář	k1gMnSc1	sedlář
na	na	k7c6	na
Hukvaldech	Hukvaldy	k1gInPc6	Hukvaldy
<g/>
,	,	kIx,	,
také	také	k9	také
vozy	vůz	k1gInPc1	vůz
a	a	k8xC	a
bryčky	bryčka	k1gFnPc1	bryčka
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
proto	proto	k8xC	proto
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
šel	jít	k5eAaImAgMnS	jít
Ignác	Ignác	k1gMnSc1	Ignác
Schustala	Schustala	k1gMnSc1	Schustala
po	po	k7c4	po
vyučení	vyučení	k1gNnSc4	vyučení
na	na	k7c4	na
zkušenou	zkušená	k1gFnSc4	zkušená
za	za	k7c4	za
tovaryše	tovaryš	k1gMnPc4	tovaryš
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k9	co
Schustala	Schustala	k1gMnSc1	Schustala
založil	založit	k5eAaPmAgMnS	založit
továrnu	továrna	k1gFnSc4	továrna
<g/>
,	,	kIx,	,
existovala	existovat	k5eAaImAgFnS	existovat
prý	prý	k9	prý
jakási	jakýsi	k3yIgFnSc1	jakýsi
dohoda	dohoda	k1gFnSc1	dohoda
mezi	mezi	k7c7	mezi
Josefem	Josef	k1gMnSc7	Josef
Svitákem	Sviták	k1gMnSc7	Sviták
a	a	k8xC	a
Ignácem	Ignác	k1gMnSc7	Ignác
Schustalou	Schustala	k1gFnSc7	Schustala
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
dělal	dělat	k5eAaImAgMnS	dělat
Sviták	Sviták	k1gMnSc1	Sviták
na	na	k7c6	na
Hukvaldech	Hukvaldy	k1gInPc6	Hukvaldy
sedlářkou	sedlářka	k1gFnSc7	sedlářka
práci	práce	k1gFnSc4	práce
pro	pro	k7c4	pro
Schustalu	Schustal	k1gMnSc3	Schustal
v	v	k7c6	v
Kopřivnici	Kopřivnice	k1gFnSc6	Kopřivnice
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
o	o	k7c4	o
2	[number]	k4	2
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
mistrem	mistr	k1gMnSc7	mistr
a	a	k8xC	a
dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1886	[number]	k4	1886
se	se	k3xPyFc4	se
v	v	k7c6	v
Jaktaři	Jaktař	k1gMnSc6	Jaktař
u	u	k7c2	u
Opavy	Opava	k1gFnSc2	Opava
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Jenovefou	Jenovefý	k2eAgFnSc7d1	Jenovefý
Ondráčkovou	Ondráčková	k1gFnSc7	Ondráčková
<g/>
,	,	kIx,	,
narozenou	narozený	k2eAgFnSc4d1	narozená
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1861	[number]	k4	1861
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
Františka	František	k1gMnSc2	František
Ondráčka	Ondráček	k1gMnSc2	Ondráček
<g/>
,	,	kIx,	,
kováře	kovář	k1gMnSc2	kovář
z	z	k7c2	z
Jaktaře	Jaktař	k1gMnSc2	Jaktař
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
manželky	manželka	k1gFnPc1	manželka
Kateřiny	Kateřina	k1gFnSc2	Kateřina
<g/>
,	,	kIx,	,
rozené	rozený	k2eAgFnSc2d1	rozená
Neslarové	Neslarová	k1gFnSc2	Neslarová
<g/>
.	.	kIx.	.
</s>
<s>
Leopold	Leopold	k1gMnSc1	Leopold
se	se	k3xPyFc4	se
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
seznámil	seznámit	k5eAaPmAgMnS	seznámit
na	na	k7c6	na
Hukvaldech	Hukvaldy	k1gInPc6	Hukvaldy
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žila	žít	k5eAaImAgFnS	žít
na	na	k7c6	na
faře	fara	k1gFnSc6	fara
u	u	k7c2	u
patera	pater	k1gMnSc2	pater
Jana	Jan	k1gMnSc2	Jan
Šlapety	Šlapeta	k1gFnSc2	Šlapeta
<g/>
,	,	kIx,	,
rodáka	rodák	k1gMnSc4	rodák
z	z	k7c2	z
Místku	Místek	k1gInSc2	Místek
<g/>
.	.	kIx.	.
</s>
<s>
Šlapeta	Šlapeto	k1gNnPc4	Šlapeto
byl	být	k5eAaImAgInS	být
farářem	farář	k1gMnSc7	farář
na	na	k7c6	na
Hukvaldech	Hukvaldy	k1gInPc6	Hukvaldy
3	[number]	k4	3
roky	rok	k1gInPc7	rok
–	–	k?	–
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1883	[number]	k4	1883
–	–	k?	–
a	a	k8xC	a
na	na	k7c4	na
Hukvaldy	Hukvaldy	k1gInPc4	Hukvaldy
přišel	přijít	k5eAaPmAgInS	přijít
právě	právě	k6eAd1	právě
z	z	k7c2	z
Jaktaře	Jaktař	k1gMnSc2	Jaktař
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
horlivě	horlivě	k6eAd1	horlivě
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
národně	národně	k6eAd1	národně
katolickém	katolický	k2eAgMnSc6d1	katolický
duchu	duch	k1gMnSc6	duch
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
svatba	svatba	k1gFnSc1	svatba
byla	být	k5eAaImAgFnS	být
velice	velice	k6eAd1	velice
smutná	smutný	k2eAgFnSc1d1	smutná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
právě	právě	k9	právě
v	v	k7c4	v
den	den	k1gInSc4	den
svatby	svatba	k1gFnSc2	svatba
zemřela	zemřít	k5eAaPmAgFnS	zemřít
nevěstina	nevěstin	k2eAgFnSc1d1	nevěstina
matka	matka	k1gFnSc1	matka
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
svatebních	svatební	k2eAgMnPc2d1	svatební
svědků	svědek	k1gMnPc2	svědek
byl	být	k5eAaImAgMnS	být
Jan	Jan	k1gMnSc1	Jan
Uhlař	uhlař	k1gMnSc1	uhlař
<g/>
,	,	kIx,	,
hostinský	hostinský	k2eAgMnSc1d1	hostinský
z	z	k7c2	z
Hukvaldů	Hukvald	k1gMnPc2	Hukvald
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
1886	[number]	k4	1886
je	být	k5eAaImIp3nS	být
též	též	k9	též
rokem	rok	k1gInSc7	rok
zakcionování	zakcionování	k1gNnSc2	zakcionování
firmy	firma	k1gFnSc2	firma
Schustala	Schustala	k1gFnSc2	Schustala
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
v	v	k7c6	v
Kopřivnici	Kopřivnice	k1gFnSc6	Kopřivnice
v	v	k7c4	v
novou	nový	k2eAgFnSc4d1	nová
firmu	firma	k1gFnSc4	firma
<g/>
"	"	kIx"	"
<g/>
Nesselsdorfer	Nesselsdorfer	k1gInSc1	Nesselsdorfer
Wagonbaufabriks	Wagonbaufabriksa	k1gFnPc2	Wagonbaufabriksa
Gesellschaft	Gesellschafta	k1gFnPc2	Gesellschafta
<g/>
,	,	kIx,	,
vormals	vormals	k6eAd1	vormals
Schustala	Schustala	k1gFnSc1	Schustala
a	a	k8xC	a
co	co	k3yQnSc1	co
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Podle	podle	k7c2	podle
notářského	notářský	k2eAgInSc2d1	notářský
zápisu	zápis	k1gInSc2	zápis
ze	z	k7c2	z
dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1888	[number]	k4	1888
odstupuje	odstupovat	k5eAaImIp3nS	odstupovat
Josef	Josef	k1gMnSc1	Josef
Sviták	Sviták	k1gInSc4	Sviták
svému	svůj	k3xOyFgMnSc3	svůj
synu	syn	k1gMnSc3	syn
Leopoldu	Leopold	k1gMnSc3	Leopold
a	a	k8xC	a
snaše	snacha	k1gFnSc3	snacha
Jenovefě	Jenovefa	k1gFnSc6	Jenovefa
svůj	svůj	k3xOyFgInSc4	svůj
domek	domek	k1gInSc4	domek
č.	č.	k?	č.
86	[number]	k4	86
s	s	k7c7	s
polnostmi	polnost	k1gFnPc7	polnost
za	za	k7c4	za
částku	částka	k1gFnSc4	částka
1200	[number]	k4	1200
zlatých	zlatá	k1gFnPc2	zlatá
rakouské	rakouský	k2eAgFnSc2d1	rakouská
měny	měna	k1gFnSc2	měna
s	s	k7c7	s
výhradou	výhrada	k1gFnSc7	výhrada
používání	používání	k1gNnSc2	používání
tohoto	tento	k3xDgInSc2	tento
majetku	majetek	k1gInSc2	majetek
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
dožití	dožití	k1gNnSc2	dožití
odstupujícího	odstupující	k2eAgNnSc2d1	odstupující
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
umírá	umírat	k5eAaImIp3nS	umírat
Leopoldova	Leopoldův	k2eAgFnSc1d1	Leopoldova
macecha	macecha	k1gFnSc1	macecha
Marie	Marie	k1gFnSc1	Marie
(	(	kIx(	(
<g/>
Mariana	Mariana	k1gFnSc1	Mariana
<g/>
)	)	kIx)	)
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
76	[number]	k4	76
let	léto	k1gNnPc2	léto
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pohřbena	pohřbít	k5eAaPmNgFnS	pohřbít
v	v	k7c6	v
Rychalticích	Rychaltice	k1gFnPc6	Rychaltice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čtyřletém	čtyřletý	k2eAgNnSc6d1	čtyřleté
trvání	trvání	k1gNnSc6	trvání
akciové	akciový	k2eAgFnSc2d1	akciová
společnosti	společnost	k1gFnSc2	společnost
svěřuje	svěřovat	k5eAaImIp3nS	svěřovat
firma	firma	k1gFnSc1	firma
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1891	[number]	k4	1891
Leopoldu	Leopolda	k1gFnSc4	Leopolda
Svitákovi	Sviták	k1gMnSc3	Sviták
zkušební	zkušební	k2eAgNnSc1d1	zkušební
vedení	vedení	k1gNnSc1	vedení
soustružny	soustružna	k1gFnSc2	soustružna
<g/>
,	,	kIx,	,
zámečnické	zámečnický	k2eAgFnSc2d1	zámečnická
dílny	dílna	k1gFnSc2	dílna
a	a	k8xC	a
kovárny	kovárna	k1gFnSc2	kovárna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1893	[number]	k4	1893
je	být	k5eAaImIp3nS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
dílovedoucím	dílovedoucí	k1gMnSc7	dílovedoucí
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
(	(	kIx(	(
<g/>
vrchním	vrchní	k2eAgMnSc7d1	vrchní
mistrem	mistr	k1gMnSc7	mistr
<g/>
)	)	kIx)	)
s	s	k7c7	s
ročním	roční	k2eAgInSc7d1	roční
platem	plat	k1gInSc7	plat
1200	[number]	k4	1200
zlatých	zlatý	k1gInPc2	zlatý
rakouské	rakouský	k2eAgFnSc2d1	rakouská
měny	měna	k1gFnSc2	měna
a	a	k8xC	a
80	[number]	k4	80
zlatých	zlatá	k1gFnPc2	zlatá
ročního	roční	k2eAgInSc2d1	roční
přibytečného	přibytečný	k2eAgInSc2d1	přibytečný
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1894	[number]	k4	1894
mu	on	k3xPp3gMnSc3	on
byly	být	k5eAaImAgInP	být
tyto	tento	k3xDgInPc1	tento
požitky	požitek	k1gInPc1	požitek
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
záslužnou	záslužný	k2eAgFnSc4d1	záslužná
činnost	činnost	k1gFnSc4	činnost
zvýšeny	zvýšen	k2eAgInPc1d1	zvýšen
na	na	k7c4	na
1	[number]	k4	1
500	[number]	k4	500
zlatých	zlatý	k2eAgFnPc2d1	zlatá
a	a	k8xC	a
80	[number]	k4	80
zlatých	zlatá	k1gFnPc2	zlatá
přibytečného	přibytečný	k2eAgNnSc2d1	přibytečný
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
své	svůj	k3xOyFgFnSc2	svůj
druhé	druhý	k4xOgFnSc2	druhý
ženy	žena	k1gFnSc2	žena
umírá	umírat	k5eAaImIp3nS	umírat
na	na	k7c6	na
Hukvaldech	Hukvaldy	k1gInPc6	Hukvaldy
také	také	k9	také
Josef	Josef	k1gMnSc1	Josef
Sviták	Sviták	k1gMnSc1	Sviták
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
příčina	příčina	k1gFnSc1	příčina
smrti	smrt	k1gFnSc2	smrt
je	být	k5eAaImIp3nS	být
uvedena	uveden	k2eAgFnSc1d1	uvedena
záducha	záducha	k1gFnSc1	záducha
<g/>
.	.	kIx.	.
</s>
<s>
Pohřben	pohřbít	k5eAaPmNgInS	pohřbít
byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
Rychalticích	Rychaltice	k1gFnPc6	Rychaltice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úmrtí	úmrtí	k1gNnSc6	úmrtí
Josefa	Josef	k1gMnSc2	Josef
Svitáka	Sviták	k1gMnSc2	Sviták
se	se	k3xPyFc4	se
manželé	manžel	k1gMnPc1	manžel
Svitákovi	Svitákův	k2eAgMnPc1d1	Svitákův
ujímají	ujímat	k5eAaImIp3nP	ujímat
dědictví	dědictví	k1gNnPc4	dědictví
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
usnesení	usnesení	k1gNnSc2	usnesení
okresního	okresní	k2eAgInSc2d1	okresní
soudu	soud	k1gInSc2	soud
v	v	k7c6	v
Místku	místko	k1gNnSc6	místko
ze	z	k7c2	z
dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1895	[number]	k4	1895
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
nakupuje	nakupovat	k5eAaBmIp3nS	nakupovat
firma	firma	k1gFnSc1	firma
některé	některý	k3yIgInPc4	některý
patenty	patent	k1gInPc4	patent
firmy	firma	k1gFnSc2	firma
Benz	Benza	k1gFnPc2	Benza
a	a	k8xC	a
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
se	se	k3xPyFc4	se
stavět	stavět	k5eAaImF	stavět
automobily	automobil	k1gInPc4	automobil
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
počátek	počátek	k1gInSc1	počátek
nové	nový	k2eAgFnSc2d1	nová
<g/>
,	,	kIx,	,
usilovné	usilovný	k2eAgFnSc2d1	usilovná
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
přemýšlení	přemýšlení	k1gNnSc2	přemýšlení
pro	pro	k7c4	pro
Leopolda	Leopold	k1gMnSc4	Leopold
Svitáka	Sviták	k1gMnSc4	Sviták
<g/>
.	.	kIx.	.
</s>
<s>
Musel	muset	k5eAaImAgMnS	muset
totiž	totiž	k9	totiž
vytvářet	vytvářet	k5eAaImF	vytvářet
všechny	všechen	k3xTgFnPc4	všechen
potřebné	potřebný	k2eAgFnPc4d1	potřebná
součástky	součástka	k1gFnPc4	součástka
sám	sám	k3xTgInSc1	sám
od	od	k7c2	od
základů	základ	k1gInPc2	základ
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
neexistovaly	existovat	k5eNaImAgFnP	existovat
žádné	žádný	k3yNgNnSc1	žádný
továrny	továrna	k1gFnPc1	továrna
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
by	by	kYmCp3nP	by
automobilové	automobilový	k2eAgFnPc1d1	automobilová
součástky	součástka	k1gFnPc1	součástka
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
a	a	k8xC	a
dodávaly	dodávat	k5eAaImAgFnP	dodávat
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
úsilí	úsilí	k1gNnSc1	úsilí
bylo	být	k5eAaImAgNnS	být
korunováno	korunovat	k5eAaBmNgNnS	korunovat
úspěchem	úspěch	k1gInSc7	úspěch
–	–	k?	–
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
byl	být	k5eAaImAgInS	být
dohotoven	dohotovit	k5eAaPmNgInS	dohotovit
první	první	k4xOgInSc1	první
osobní	osobní	k2eAgInSc1d1	osobní
automobil	automobil	k1gInSc1	automobil
v	v	k7c6	v
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
Rakousku-Uhersku	Rakousku-Uhersko	k1gNnSc6	Rakousku-Uhersko
<g/>
.	.	kIx.	.
</s>
<s>
Dostal	dostat	k5eAaPmAgMnS	dostat
název	název	k1gInSc4	název
"	"	kIx"	"
<g/>
Praesident	Praesident	k1gMnSc1	Praesident
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byla	být	k5eAaImAgFnS	být
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
podniknuta	podniknout	k5eAaPmNgFnS	podniknout
první	první	k4xOgFnSc1	první
dálková	dálkový	k2eAgFnSc1d1	dálková
jízda	jízda	k1gFnSc1	jízda
spolehlivosti	spolehlivost	k1gFnSc2	spolehlivost
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Kopřivnice	Kopřivnice	k1gFnSc2	Kopřivnice
–	–	k?	–
Vídeň	Vídeň	k1gFnSc1	Vídeň
(	(	kIx(	(
<g/>
285	[number]	k4	285
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
sázku	sázka	k1gFnSc4	sázka
vídeňského	vídeňský	k2eAgInSc2d1	vídeňský
sportovního	sportovní	k2eAgInSc2d1	sportovní
klubu	klub	k1gInSc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
"	"	kIx"	"
<g/>
Praesidenta	Praesident	k1gMnSc2	Praesident
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
jízdy	jízda	k1gFnPc1	jízda
zúčastnily	zúčastnit	k5eAaPmAgFnP	zúčastnit
ještě	ještě	k6eAd1	ještě
další	další	k2eAgMnPc4d1	další
dva	dva	k4xCgInPc4	dva
vozy	vůz	k1gInPc4	vůz
<g/>
;	;	kIx,	;
všechny	všechen	k3xTgFnPc1	všechen
vzbuzovaly	vzbuzovat	k5eAaImAgFnP	vzbuzovat
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
cestě	cesta	k1gFnSc6	cesta
nebývalou	nebývalý	k2eAgFnSc4d1	nebývalá
pozornost	pozornost	k1gFnSc4	pozornost
a	a	k8xC	a
údiv	údiv	k1gInSc4	údiv
nad	nad	k7c7	nad
"	"	kIx"	"
<g/>
vozem	vůz	k1gInSc7	vůz
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jezdí	jezdit	k5eAaImIp3nS	jezdit
bez	bez	k7c2	bez
koní	kůň	k1gMnPc2	kůň
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
exemplář	exemplář	k1gInSc1	exemplář
"	"	kIx"	"
<g/>
Praesidenta	Praesident	k1gMnSc2	Praesident
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
uložen	uložit	k5eAaPmNgInS	uložit
ve	v	k7c6	v
sbírkách	sbírka	k1gFnPc6	sbírka
Národního	národní	k2eAgNnSc2d1	národní
technického	technický	k2eAgNnSc2d1	technické
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Letné	Letná	k1gFnSc6	Letná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
byl	být	k5eAaImAgMnS	být
Leopold	Leopold	k1gMnSc1	Leopold
Sviták	Sviták	k1gMnSc1	Sviták
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
závodním	závodní	k1gMnSc7	závodní
(	(	kIx(	(
<g/>
Betriebsleiter	Betriebsleiter	k1gMnSc1	Betriebsleiter
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
pracovní	pracovní	k2eAgFnSc6d1	pracovní
knížce	knížka	k1gFnSc6	knížka
vyznačen	vyznačen	k2eAgInSc4d1	vyznačen
výstup	výstup	k1gInSc4	výstup
ze	z	k7c2	z
služeb	služba	k1gFnPc2	služba
firmy	firma	k1gFnSc2	firma
dnem	dnem	k7c2	dnem
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byla	být	k5eAaImAgFnS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
firma	firma	k1gFnSc1	firma
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
vyslat	vyslat	k5eAaPmF	vyslat
svého	svůj	k3xOyFgMnSc4	svůj
závodního	závodní	k1gMnSc4	závodní
na	na	k7c4	na
delší	dlouhý	k2eAgFnSc4d2	delší
studijní	studijní	k2eAgFnSc4d1	studijní
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
ciziny	cizina	k1gFnSc2	cizina
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
prostudování	prostudování	k1gNnSc1	prostudování
stavu	stav	k1gInSc2	stav
výroby	výroba	k1gFnSc2	výroba
automobilů	automobil	k1gInPc2	automobil
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
Leopold	Leopold	k1gMnSc1	Leopold
Sviták	Sviták	k1gMnSc1	Sviták
cestuje	cestovat	k5eAaImIp3nS	cestovat
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
Drážďan	Drážďany	k1gInPc2	Drážďany
<g/>
,	,	kIx,	,
Chemnitz	Chemnitza	k1gFnPc2	Chemnitza
<g/>
,	,	kIx,	,
Berlína	Berlín	k1gInSc2	Berlín
<g/>
,	,	kIx,	,
Deutzu	Deutz	k1gInSc2	Deutz
<g/>
,	,	kIx,	,
Kolína	Kolín	k1gInSc2	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
a	a	k8xC	a
Mannheimu	Mannheim	k1gInSc2	Mannheim
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
této	tento	k3xDgFnSc2	tento
cesty	cesta	k1gFnSc2	cesta
byla	být	k5eAaImAgFnS	být
řada	řada	k1gFnSc1	řada
zlepšení	zlepšení	k1gNnSc2	zlepšení
konstrukčních	konstrukční	k2eAgInPc2d1	konstrukční
i	i	k8xC	i
výrobních	výrobní	k2eAgInPc2d1	výrobní
<g/>
.	.	kIx.	.
</s>
<s>
Pan	Pan	k1gMnSc1	Pan
Emil	Emil	k1gMnSc1	Emil
Hanzelka	Hanzelka	k1gMnSc1	Hanzelka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgMnS	být
tehdy	tehdy	k6eAd1	tehdy
památkovým	památkový	k2eAgMnSc7d1	památkový
referentem	referent	k1gMnSc7	referent
Klubu	klub	k1gInSc2	klub
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
a	a	k8xC	a
Okresní	okresní	k2eAgFnSc2d1	okresní
osvětové	osvětový	k2eAgFnSc2d1	osvětová
rady	rada	k1gFnSc2	rada
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Jičíně	Jičín	k1gInSc6	Jičín
<g/>
)	)	kIx)	)
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
na	na	k7c4	na
Leopolda	Leopold	k1gMnSc4	Leopold
Svitáka	Sviták	k1gMnSc4	Sviták
<g/>
,	,	kIx,	,
u	u	k7c2	u
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
vyučil	vyučit	k5eAaPmAgMnS	vyučit
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgFnPc4	všechen
součástky	součástka	k1gFnPc4	součástka
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
<g/>
,	,	kIx,	,
skicoval	skicovat	k5eAaImAgMnS	skicovat
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
obráběl	obrábět	k5eAaImAgMnS	obrábět
a	a	k8xC	a
montoval	montovat	k5eAaImAgMnS	montovat
<g/>
,	,	kIx,	,
sám	sám	k3xTgInSc1	sám
vyráběl	vyrábět	k5eAaImAgInS	vyrábět
karburátory	karburátor	k1gInPc4	karburátor
<g/>
,	,	kIx,	,
chladiče	chladič	k1gInPc4	chladič
<g/>
,	,	kIx,	,
kuličková	kuličkový	k2eAgNnPc4d1	kuličkové
ložiska	ložisko	k1gNnPc4	ložisko
a	a	k8xC	a
ozubená	ozubený	k2eAgNnPc4d1	ozubené
kola	kolo	k1gNnPc4	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
součástka	součástka	k1gFnSc1	součástka
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
vyzkoušena	vyzkoušet	k5eAaPmNgFnS	vyzkoušet
a	a	k8xC	a
často	často	k6eAd1	často
i	i	k9	i
mnohokrát	mnohokrát	k6eAd1	mnohokrát
vyměňována	vyměňován	k2eAgFnSc1d1	vyměňována
<g/>
.	.	kIx.	.
</s>
<s>
Jedině	jedině	k6eAd1	jedině
řetězy	řetěz	k1gInPc1	řetěz
<g/>
,	,	kIx,	,
zapalovací	zapalovací	k2eAgInPc1d1	zapalovací
magnety	magnet	k1gInPc1	magnet
a	a	k8xC	a
pneumatiky	pneumatika	k1gFnPc1	pneumatika
se	se	k3xPyFc4	se
kupovaly	kupovat	k5eAaImAgFnP	kupovat
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
dohotoveným	dohotovený	k2eAgInSc7d1	dohotovený
vozem	vůz	k1gInSc7	vůz
sám	sám	k3xTgMnSc1	sám
vyjížděl	vyjíždět	k5eAaImAgInS	vyjíždět
jako	jako	k9	jako
zkušební	zkušební	k2eAgInSc1d1	zkušební
jezdec	jezdec	k1gInSc1	jezdec
<g/>
.	.	kIx.	.
</s>
<s>
Pravou	pravý	k2eAgFnSc7d1	pravá
rukou	ruka	k1gFnSc7	ruka
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
Jan	Jan	k1gMnSc1	Jan
Kuchař	Kuchař	k1gMnSc1	Kuchař
z	z	k7c2	z
Kopřivnice	Kopřivnice	k1gFnSc2	Kopřivnice
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gInSc4	on
konstruktér	konstruktér	k1gMnSc1	konstruktér
a	a	k8xC	a
dílovedoucí	dílovedoucí	k1gMnSc1	dílovedoucí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
dává	dávat	k5eAaImIp3nS	dávat
si	se	k3xPyFc3	se
Leopold	Leopold	k1gMnSc1	Leopold
Sviták	Sviták	k1gMnSc1	Sviták
vypracovat	vypracovat	k5eAaPmF	vypracovat
od	od	k7c2	od
stavitele	stavitel	k1gMnSc2	stavitel
Jana	Jan	k1gMnSc2	Jan
Smitala	Smital	k1gMnSc2	Smital
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Jičíně	Jičín	k1gInSc6	Jičín
plánek	plánek	k1gInSc4	plánek
na	na	k7c4	na
přestavbu	přestavba	k1gFnSc4	přestavba
odstoupeného	odstoupený	k2eAgInSc2d1	odstoupený
domku	domek	k1gInSc2	domek
č.	č.	k?	č.
86	[number]	k4	86
na	na	k7c6	na
Hukvaldech	Hukvaldy	k1gInPc6	Hukvaldy
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
přestavba	přestavba	k1gFnSc1	přestavba
uskutečněna	uskutečněn	k2eAgFnSc1d1	uskutečněna
<g/>
,	,	kIx,	,
půjčují	půjčovat	k5eAaImIp3nP	půjčovat
si	se	k3xPyFc3	se
manželé	manžel	k1gMnPc1	manžel
Svitákovi	Svitákův	k2eAgMnPc1d1	Svitákův
v	v	k7c6	v
prosince	prosinec	k1gInSc2	prosinec
1899	[number]	k4	1899
u	u	k7c2	u
příborské	příborský	k2eAgFnSc2d1	příborská
pobočky	pobočka	k1gFnSc2	pobočka
Novojičínské	novojičínský	k2eAgFnSc2d1	novojičínská
záložny	záložna	k1gFnSc2	záložna
2000	[number]	k4	2000
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
Půjčku	půjčka	k1gFnSc4	půjčka
splatili	splatit	k5eAaPmAgMnP	splatit
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
4	[number]	k4	4
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dnem	dnem	k7c2	dnem
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1900	[number]	k4	1900
dostává	dostávat	k5eAaImIp3nS	dostávat
se	se	k3xPyFc4	se
Leopoldu	Leopold	k1gMnSc3	Leopold
Svitákovi	Sviták	k1gMnSc3	Sviták
dalšího	další	k2eAgNnSc2d1	další
zvýšení	zvýšení	k1gNnSc2	zvýšení
platu	plat	k1gInSc2	plat
na	na	k7c4	na
2000	[number]	k4	2000
zlatých	zlatá	k1gFnPc2	zlatá
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
1900	[number]	k4	1900
byl	být	k5eAaImAgInS	být
však	však	k9	však
pro	pro	k7c4	pro
něho	on	k3xPp3gMnSc4	on
rokem	rok	k1gInSc7	rok
osudným	osudný	k2eAgInSc7d1	osudný
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1900	[number]	k4	1900
řídil	řídit	k5eAaImAgInS	řídit
auto	auto	k1gNnSc4	auto
při	při	k7c6	při
zkušební	zkušební	k2eAgFnSc6d1	zkušební
jízdě	jízda	k1gFnSc6	jízda
po	po	k7c6	po
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
císařské	císařský	k2eAgFnSc3d1	císařská
<g/>
"	"	kIx"	"
silnici	silnice	k1gFnSc3	silnice
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
cílem	cíl	k1gInSc7	cíl
byl	být	k5eAaImAgInS	být
Místek	Místek	k1gInSc1	Místek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
chtěl	chtít	k5eAaImAgMnS	chtít
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
vyřídit	vyřídit	k5eAaPmF	vyřídit
nějaké	nějaký	k3yIgFnPc4	nějaký
osobní	osobní	k2eAgFnPc4d1	osobní
záležitosti	záležitost	k1gFnPc4	záležitost
na	na	k7c6	na
úřadě	úřad	k1gInSc6	úřad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rychalticích	Rychaltice	k1gFnPc6	Rychaltice
u	u	k7c2	u
mostu	most	k1gInSc2	most
přes	přes	k7c4	přes
Ondřejnici	Ondřejnice	k1gFnSc4	Ondřejnice
v	v	k7c6	v
prudkém	prudký	k2eAgNnSc6d1	prudké
klesání	klesání	k1gNnSc6	klesání
a	a	k8xC	a
v	v	k7c6	v
zatáčce	zatáčka	k1gFnSc6	zatáčka
se	se	k3xPyFc4	se
auto	auto	k1gNnSc1	auto
z	z	k7c2	z
neznámých	známý	k2eNgInPc2d1	neznámý
důvodů	důvod	k1gInPc2	důvod
převrhlo	převrhnout	k5eAaPmAgNnS	převrhnout
a	a	k8xC	a
řidič	řidič	k1gMnSc1	řidič
a	a	k8xC	a
dva	dva	k4xCgMnPc1	dva
spolujezdci	spolujezdec	k1gMnPc1	spolujezdec
byli	být	k5eAaImAgMnP	být
z	z	k7c2	z
vozidla	vozidlo	k1gNnSc2	vozidlo
vymrštěni	vymrštit	k5eAaPmNgMnP	vymrštit
<g/>
.	.	kIx.	.
</s>
<s>
Svitákův	Svitákův	k2eAgMnSc1d1	Svitákův
spolupracovník	spolupracovník	k1gMnSc1	spolupracovník
Jan	Jan	k1gMnSc1	Jan
Kuchař	Kuchař	k1gMnSc1	Kuchař
se	se	k3xPyFc4	se
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
zabil	zabít	k5eAaPmAgMnS	zabít
(	(	kIx(	(
<g/>
zřejmě	zřejmě	k6eAd1	zřejmě
nárazem	náraz	k1gInSc7	náraz
hlavou	hlava	k1gFnSc7	hlava
na	na	k7c4	na
patník	patník	k1gInSc4	patník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
spolujezdec	spolujezdec	k1gMnSc1	spolujezdec
byl	být	k5eAaImAgMnS	být
těžce	těžce	k6eAd1	těžce
zraněn	zranit	k5eAaPmNgMnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Sviták	Sviták	k1gMnSc1	Sviták
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
pravou	pravý	k2eAgFnSc7d1	pravá
nohou	noha	k1gFnSc7	noha
pod	pod	k7c4	pod
převrhnuté	převrhnutý	k2eAgNnSc4d1	převrhnutý
auto	auto	k1gNnSc4	auto
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
motor	motor	k1gInSc1	motor
i	i	k9	i
po	po	k7c6	po
havárii	havárie	k1gFnSc6	havárie
dál	daleko	k6eAd2	daleko
běžel	běžet	k5eAaImAgMnS	běžet
<g/>
,	,	kIx,	,
rozdrtila	rozdrtit	k5eAaPmAgFnS	rozdrtit
mu	on	k3xPp3gNnSc3	on
ojnice	ojnice	k1gFnSc1	ojnice
motoru	motor	k1gInSc2	motor
pravou	pravý	k2eAgFnSc4d1	pravá
nohu	noha	k1gFnSc4	noha
až	až	k9	až
po	po	k7c4	po
koleno	koleno	k1gNnSc4	koleno
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
sice	sice	k8xC	sice
hned	hned	k6eAd1	hned
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
domě	dům	k1gInSc6	dům
na	na	k7c6	na
Hukvaldech	Hukvaldy	k1gInPc6	Hukvaldy
–	–	k?	–
na	na	k7c6	na
jídelním	jídelní	k2eAgInSc6d1	jídelní
stole	stol	k1gInSc6	stol
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
bez	bez	k7c2	bez
narkózy	narkóza	k1gFnSc2	narkóza
(	(	kIx(	(
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
–	–	k?	–
operován	operován	k2eAgMnSc1d1	operován
(	(	kIx(	(
<g/>
k	k	k7c3	k
operaci	operace	k1gFnSc3	operace
vedení	vedení	k1gNnSc2	vedení
firmy	firma	k1gFnSc2	firma
přivezlo	přivézt	k5eAaPmAgNnS	přivézt
vynikajícího	vynikající	k2eAgMnSc4d1	vynikající
odborníka	odborník	k1gMnSc4	odborník
až	až	k9	až
z	z	k7c2	z
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
kliniky	klinika	k1gFnSc2	klinika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jeho	jeho	k3xOp3gInSc1	jeho
život	život	k1gInSc1	život
byl	být	k5eAaImAgInS	být
v	v	k7c4	v
ohrožení	ohrožení	k1gNnSc4	ohrožení
a	a	k8xC	a
visel	viset	k5eAaImAgInS	viset
jen	jen	k9	jen
na	na	k7c6	na
vlásku	vlásek	k1gInSc6	vlásek
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
jeho	jeho	k3xOp3gNnSc1	jeho
pevné	pevný	k2eAgNnSc1d1	pevné
zdraví	zdraví	k1gNnSc1	zdraví
a	a	k8xC	a
silná	silný	k2eAgFnSc1d1	silná
tělesná	tělesný	k2eAgFnSc1d1	tělesná
konstituce	konstituce	k1gFnSc1	konstituce
způsobily	způsobit	k5eAaPmAgInP	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokázal	dokázat	k5eAaPmAgInS	dokázat
snášet	snášet	k5eAaImF	snášet
velké	velký	k2eAgFnPc4d1	velká
bolesti	bolest	k1gFnPc4	bolest
a	a	k8xC	a
útrapy	útrapa	k1gFnPc4	útrapa
léčení	léčení	k1gNnSc2	léčení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
jednoročním	jednoroční	k2eAgNnSc6d1	jednoroční
léčení	léčení	k1gNnSc6	léčení
se	se	k3xPyFc4	se
natolik	natolik	k6eAd1	natolik
uzdravil	uzdravit	k5eAaPmAgMnS	uzdravit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
učit	učit	k5eAaImF	učit
chodit	chodit	k5eAaImF	chodit
s	s	k7c7	s
protézou	protéza	k1gFnSc7	protéza
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
neštěstí	neštěstí	k1gNnSc1	neštěstí
mělo	mít	k5eAaImAgNnS	mít
ještě	ještě	k6eAd1	ještě
soudní	soudní	k2eAgFnSc4d1	soudní
dohru	dohra	k1gFnSc4	dohra
<g/>
.	.	kIx.	.
</s>
<s>
Svitákovi	Svitákův	k2eAgMnPc1d1	Svitákův
hrozilo	hrozit	k5eAaImAgNnS	hrozit
obvinění	obvinění	k1gNnPc4	obvinění
ze	z	k7c2	z
zabití	zabití	k1gNnSc2	zabití
spolujezdce	spolujezdec	k1gMnSc2	spolujezdec
<g/>
.	.	kIx.	.
</s>
<s>
Těžko	těžko	k6eAd1	těžko
soudit	soudit	k5eAaImF	soudit
<g/>
,	,	kIx,	,
jaký	jaký	k3yIgInSc1	jaký
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
průběh	průběh	k1gInSc4	průběh
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
nebylo	být	k5eNaImAgNnS	být
svědectví	svědectví	k1gNnSc1	svědectví
libereckého	liberecký	k2eAgMnSc2d1	liberecký
průmyslníka	průmyslník	k1gMnSc2	průmyslník
a	a	k8xC	a
předsedy	předseda	k1gMnSc2	předseda
rakouského	rakouský	k2eAgInSc2d1	rakouský
autoklubu	autoklub	k1gInSc2	autoklub
barona	baron	k1gMnSc2	baron
Liebiga	Liebig	k1gMnSc2	Liebig
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
před	před	k7c7	před
soudem	soud	k1gInSc7	soud
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zná	znát	k5eAaImIp3nS	znát
Svitáka	Sviták	k1gMnSc4	Sviták
jako	jako	k8xC	jako
krajně	krajně	k6eAd1	krajně
opatrného	opatrný	k2eAgNnSc2d1	opatrné
a	a	k8xC	a
spolehlivého	spolehlivý	k2eAgMnSc2d1	spolehlivý
jezdce	jezdec	k1gMnSc2	jezdec
a	a	k8xC	a
řidiče	řidič	k1gMnSc2	řidič
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
tedy	tedy	k9	tedy
neštěstí	štěstit	k5eNaImIp3nS	štěstit
rozhodně	rozhodně	k6eAd1	rozhodně
nezpůsobil	způsobit	k5eNaPmAgMnS	způsobit
on	on	k3xPp3gMnSc1	on
<g/>
.	.	kIx.	.
</s>
<s>
Mimochodem	mimochodem	k9	mimochodem
–	–	k?	–
auto	auto	k1gNnSc1	auto
bylo	být	k5eAaImAgNnS	být
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
havárii	havárie	k1gFnSc6	havárie
z	z	k7c2	z
příkazu	příkaz	k1gInSc2	příkaz
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
Fischera	Fischer	k1gMnSc2	Fischer
rozebráno	rozebrat	k5eAaPmNgNnS	rozebrat
a	a	k8xC	a
Leopold	Leopold	k1gMnSc1	Leopold
Sviták	Sviták	k1gMnSc1	Sviták
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nedozvěděl	dozvědět	k5eNaPmAgMnS	dozvědět
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
bylo	být	k5eAaImAgNnS	být
pravou	pravý	k2eAgFnSc7d1	pravá
technickou	technický	k2eAgFnSc7d1	technická
příčinou	příčina	k1gFnSc7	příčina
neštěstí	neštěstí	k1gNnSc2	neštěstí
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
všechny	všechen	k3xTgFnPc4	všechen
výlohy	výloha	k1gFnPc4	výloha
operace	operace	k1gFnSc2	operace
i	i	k8xC	i
léčení	léčení	k1gNnSc6	léčení
byly	být	k5eAaImAgInP	být
hrazeny	hradit	k5eAaImNgInP	hradit
firmou	firma	k1gFnSc7	firma
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
slabou	slabý	k2eAgFnSc7d1	slabá
útěchou	útěcha	k1gFnSc7	útěcha
zvýšení	zvýšení	k1gNnSc2	zvýšení
jeho	jeho	k3xOp3gInSc2	jeho
platu	plat	k1gInSc2	plat
na	na	k7c4	na
4200	[number]	k4	4200
korun	koruna	k1gFnPc2	koruna
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
kopřivnické	kopřivnický	k2eAgFnSc2d1	kopřivnická
továrny	továrna	k1gFnSc2	továrna
nemohl	moct	k5eNaImAgMnS	moct
Leopold	Leopold	k1gMnSc1	Leopold
Sviták	Sviták	k1gMnSc1	Sviták
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
svého	svůj	k3xOyFgInSc2	svůj
těžkého	těžký	k2eAgInSc2d1	těžký
úrazu	úraz	k1gInSc2	úraz
vykonávat	vykonávat	k5eAaImF	vykonávat
svou	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
v	v	k7c6	v
plném	plný	k2eAgInSc6d1	plný
rozsahu	rozsah	k1gInSc6	rozsah
jako	jako	k8xC	jako
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
technickým	technický	k2eAgMnSc7d1	technický
poradcem	poradce	k1gMnSc7	poradce
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
jím	on	k3xPp3gMnSc7	on
pověřován	pověřovat	k5eAaImNgInS	pověřovat
různými	různý	k2eAgInPc7d1	různý
úkoly	úkol	k1gInPc7	úkol
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
když	když	k8xS	když
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
zavádění	zavádění	k1gNnPc4	zavádění
výrobních	výrobní	k2eAgFnPc2d1	výrobní
novinek	novinka	k1gFnPc2	novinka
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
po	po	k7c6	po
zahynuvším	zahynuvší	k2eAgInSc6d1	zahynuvší
Janu	Jan	k1gMnSc3	Jan
Kuchařovi	Kuchař	k1gMnSc3	Kuchař
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
ke	k	k7c3	k
Svitákovi	Sviták	k1gMnSc3	Sviták
jako	jako	k8xS	jako
podmistr	podmistr	k1gMnSc1	podmistr
Hans	Hans	k1gMnSc1	Hans
Ledwinka	Ledwinka	k1gFnSc1	Ledwinka
<g/>
,	,	kIx,	,
vídeňský	vídeňský	k2eAgMnSc1d1	vídeňský
Němec	Němec	k1gMnSc1	Němec
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnSc1d2	pozdější
ředitel	ředitel	k1gMnSc1	ředitel
firmy	firma	k1gFnSc2	firma
<g/>
,	,	kIx,	,
uplatňující	uplatňující	k2eAgFnSc2d1	uplatňující
rovněž	rovněž	k9	rovněž
svá	svůj	k3xOyFgNnPc4	svůj
práva	právo	k1gNnPc4	právo
na	na	k7c6	na
konstrukci	konstrukce	k1gFnSc6	konstrukce
prvního	první	k4xOgInSc2	první
automobilu	automobil	k1gInSc2	automobil
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Dnes	dnes	k6eAd1	dnes
–	–	k?	–
tj.	tj.	kA	tj.
ve	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
–	–	k?	–
se	se	k3xPyFc4	se
podíl	podíl	k1gInSc1	podíl
Ledwinkův	Ledwinkův	k2eAgInSc1d1	Ledwinkův
nikterak	nikterak	k6eAd1	nikterak
nepopírá	popírat	k5eNaImIp3nS	popírat
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1912	[number]	k4	1912
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
v	v	k7c6	v
kopřivnické	kopřivnický	k2eAgFnSc6d1	kopřivnická
továrně	továrna	k1gFnSc6	továrna
velká	velký	k2eAgFnSc1d1	velká
stávka	stávka	k1gFnSc1	stávka
za	za	k7c4	za
zvýšení	zvýšení	k1gNnSc4	zvýšení
mezd	mzda	k1gFnPc2	mzda
<g/>
.	.	kIx.	.
</s>
<s>
Trvala	trvalo	k1gNnSc2	trvalo
neuvěřitelných	uvěřitelný	k2eNgInPc2d1	neuvěřitelný
22	[number]	k4	22
týdnů	týden	k1gInPc2	týden
až	až	k6eAd1	až
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
Stávka	stávka	k1gFnSc1	stávka
se	se	k3xPyFc4	se
protáhla	protáhnout	k5eAaPmAgFnS	protáhnout
na	na	k7c4	na
tak	tak	k6eAd1	tak
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
pro	pro	k7c4	pro
neústupnost	neústupnost	k1gFnSc4	neústupnost
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
Fischera	Fischer	k1gMnSc2	Fischer
a	a	k8xC	a
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
Vítkovických	vítkovický	k2eAgInPc2d1	vítkovický
závodů	závod	k1gInPc2	závod
Sonnenscheina	Sonnenscheino	k1gNnSc2	Sonnenscheino
<g/>
.	.	kIx.	.
</s>
<s>
Stávka	stávka	k1gFnSc1	stávka
skončila	skončit	k5eAaPmAgFnS	skončit
kompromisem	kompromis	k1gInSc7	kompromis
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
otřásla	otřást	k5eAaPmAgFnS	otřást
Fischerovou	Fischerův	k2eAgFnSc7d1	Fischerova
pozicí	pozice	k1gFnSc7	pozice
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
odešel	odejít	k5eAaPmAgMnS	odejít
ze	z	k7c2	z
služeb	služba	k1gFnPc2	služba
firmy	firma	k1gFnSc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
současně	současně	k6eAd1	současně
odešla	odejít	k5eAaPmAgFnS	odejít
i	i	k9	i
řada	řada	k1gFnSc1	řada
starších	starý	k2eAgMnPc2d2	starší
mistrů	mistr	k1gMnPc2	mistr
a	a	k8xC	a
úředníků	úředník	k1gMnPc2	úředník
–	–	k?	–
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
56	[number]	k4	56
letech	let	k1gInPc6	let
i	i	k8xC	i
Leopold	Leopold	k1gMnSc1	Leopold
Sviták	Sviták	k1gMnSc1	Sviták
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
27	[number]	k4	27
letech	léto	k1gNnPc6	léto
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
Kopřivnici	Kopřivnice	k1gFnSc6	Kopřivnice
se	se	k3xPyFc4	se
dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1912	[number]	k4	1912
odstěhoval	odstěhovat	k5eAaPmAgInS	odstěhovat
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
a	a	k8xC	a
synem	syn	k1gMnSc7	syn
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
(	(	kIx(	(
<g/>
ostatní	ostatní	k2eAgFnPc1d1	ostatní
děti	dítě	k1gFnPc1	dítě
už	už	k6eAd1	už
byly	být	k5eAaImAgFnP	být
mimo	mimo	k7c4	mimo
domov	domov	k1gInSc4	domov
<g/>
)	)	kIx)	)
definitivně	definitivně	k6eAd1	definitivně
na	na	k7c4	na
Hukvaldy	Hukvaldy	k1gInPc4	Hukvaldy
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
své	svůj	k3xOyFgFnSc2	svůj
činnosti	činnost	k1gFnSc2	činnost
v	v	k7c6	v
Kopřivnici	Kopřivnice	k1gFnSc6	Kopřivnice
odchoval	odchovat	k5eAaPmAgMnS	odchovat
řadu	řada	k1gFnSc4	řada
vynikajících	vynikající	k2eAgMnPc2d1	vynikající
odborníků	odborník	k1gMnPc2	odborník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
–	–	k?	–
přes	přes	k7c4	přes
jeho	jeho	k3xOp3gFnSc4	jeho
velkou	velký	k2eAgFnSc4d1	velká
přísnost	přísnost	k1gFnSc4	přísnost
–	–	k?	–
na	na	k7c4	na
něho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
dobrém	dobré	k1gNnSc6	dobré
vzpomínali	vzpomínat	k5eAaImAgMnP	vzpomínat
<g/>
.	.	kIx.	.
</s>
<s>
Děkovali	děkovat	k5eAaImAgMnP	děkovat
mu	on	k3xPp3gMnSc3	on
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
ostrou	ostrý	k2eAgFnSc4d1	ostrá
životní	životní	k2eAgFnSc4d1	životní
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jim	on	k3xPp3gMnPc3	on
umožnila	umožnit	k5eAaPmAgFnS	umožnit
později	pozdě	k6eAd2	pozdě
zastávat	zastávat	k5eAaImF	zastávat
vysoké	vysoký	k2eAgFnPc1d1	vysoká
a	a	k8xC	a
odpovědné	odpovědný	k2eAgFnPc1d1	odpovědná
funkce	funkce	k1gFnPc1	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
řady	řada	k1gFnSc2	řada
jeho	jeho	k3xOp3gMnPc2	jeho
vyučenců	vyučenec	k1gMnPc2	vyučenec
jsem	být	k5eAaImIp1nS	být
již	již	k6eAd1	již
zmínil	zmínit	k5eAaPmAgMnS	zmínit
Hanse	hansa	k1gFnSc3	hansa
Ledwinku	Ledwinka	k1gFnSc4	Ledwinka
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
jím	jíst	k5eAaImIp1nS	jíst
i	i	k9	i
vrchní	vrchní	k2eAgMnSc1d1	vrchní
dílovedoucí	dílovedoucí	k1gMnSc1	dílovedoucí
automobilky	automobilka	k1gFnSc2	automobilka
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
Krompholc	Krompholc	k1gInSc1	Krompholc
<g/>
,	,	kIx,	,
rodák	rodák	k1gMnSc1	rodák
z	z	k7c2	z
Příbora	Příbor	k1gInSc2	Příbor
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Emil	Emil	k1gMnSc1	Emil
Hanzelka	Hanzelka	k1gMnSc1	Hanzelka
<g/>
,	,	kIx,	,
obchodník	obchodník	k1gMnSc1	obchodník
a	a	k8xC	a
osvětový	osvětový	k2eAgMnSc1d1	osvětový
pracovník	pracovník	k1gMnSc1	pracovník
<g/>
,	,	kIx,	,
a	a	k8xC	a
mnozí	mnohý	k2eAgMnPc1d1	mnohý
jiní	jiný	k1gMnPc1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
generálním	generální	k2eAgMnSc7d1	generální
ředitelem	ředitel	k1gMnSc7	ředitel
Fischer	Fischer	k1gMnSc1	Fischer
<g/>
,	,	kIx,	,
zavládl	zavládnout	k5eAaPmAgInS	zavládnout
ve	v	k7c6	v
firmě	firma	k1gFnSc6	firma
silný	silný	k2eAgInSc1d1	silný
germanizační	germanizační	k2eAgInSc1d1	germanizační
kurs	kurs	k1gInSc1	kurs
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
bohužel	bohužel	k6eAd1	bohužel
neštítil	štítit	k5eNaImAgMnS	štítit
žádného	žádný	k3yNgInSc2	žádný
nátlaku	nátlak	k1gInSc2	nátlak
<g/>
.	.	kIx.	.
</s>
<s>
Vzdor	vzdor	k7c3	vzdor
tomu	ten	k3xDgNnSc3	ten
–	–	k?	–
patrně	patrně	k6eAd1	patrně
i	i	k9	i
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnPc1	jeho
odborné	odborný	k2eAgFnPc1d1	odborná
kvality	kvalita	k1gFnPc1	kvalita
byly	být	k5eAaImAgFnP	být
nezpochybnitelné	zpochybnitelný	k2eNgInPc1d1	nezpochybnitelný
–	–	k?	–
zůstal	zůstat	k5eAaPmAgMnS	zůstat
Leopold	Leopold	k1gMnSc1	Leopold
Sviták	Sviták	k1gMnSc1	Sviták
uvědomělým	uvědomělý	k2eAgMnSc7d1	uvědomělý
Čechem	Čech	k1gMnSc7	Čech
a	a	k8xC	a
také	také	k9	také
všechny	všechen	k3xTgFnPc4	všechen
své	svůj	k3xOyFgFnPc4	svůj
děti	dítě	k1gFnPc4	dítě
v	v	k7c6	v
tomto	tento	k3xDgMnSc6	tento
duchu	duch	k1gMnSc6	duch
vychoval	vychovat	k5eAaPmAgMnS	vychovat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
byl	být	k5eAaImAgMnS	být
Sviták	Sviták	k1gMnSc1	Sviták
pokladníkem	pokladník	k1gMnSc7	pokladník
Lidové	lidový	k2eAgFnSc2d1	lidová
záložny	záložna	k1gFnSc2	záložna
v	v	k7c6	v
Kopřivnici	Kopřivnice	k1gFnSc6	Kopřivnice
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
sám	sám	k3xTgMnSc1	sám
zakládal	zakládat	k5eAaImAgInS	zakládat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
setrval	setrvat	k5eAaPmAgMnS	setrvat
až	až	k9	až
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
odchodu	odchod	k1gInSc2	odchod
na	na	k7c4	na
Hukvaldy	Hukvaldy	k1gInPc4	Hukvaldy
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
ani	ani	k8xC	ani
tam	tam	k6eAd1	tam
nezahálel	zahálet	k5eNaImAgMnS	zahálet
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
dozorčí	dozorčí	k2eAgFnSc2d1	dozorčí
rady	rada	k1gFnSc2	rada
Občanské	občanský	k2eAgFnSc2d1	občanská
záložny	záložna	k1gFnSc2	záložna
na	na	k7c6	na
Hukvaldech	Hukvaldy	k1gInPc6	Hukvaldy
<g/>
,	,	kIx,	,
vedl	vést	k5eAaImAgMnS	vést
za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
obecní	obecnět	k5eAaImIp3nS	obecnět
zásobovací	zásobovací	k2eAgFnSc4d1	zásobovací
agendu	agenda	k1gFnSc4	agenda
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
obecním	obecní	k2eAgMnSc7d1	obecní
kronikářem	kronikář	k1gMnSc7	kronikář
a	a	k8xC	a
jako	jako	k8xS	jako
člen	člen	k1gMnSc1	člen
finanční	finanční	k2eAgFnSc2d1	finanční
komise	komise	k1gFnSc2	komise
se	se	k3xPyFc4	se
staral	starat	k5eAaImAgInS	starat
o	o	k7c4	o
obecní	obecní	k2eAgNnSc4d1	obecní
hospodářství	hospodářství	k1gNnSc4	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgMnS	být
také	také	k9	také
členem	člen	k1gMnSc7	člen
správní	správní	k2eAgFnSc2d1	správní
rady	rada	k1gFnSc2	rada
akciové	akciový	k2eAgFnSc2d1	akciová
společnosti	společnost	k1gFnSc2	společnost
A.	A.	kA	A.
Kunz	Kunz	k1gMnSc1	Kunz
v	v	k7c6	v
Hranicích	Hranice	k1gFnPc6	Hranice
a	a	k8xC	a
výrobním	výrobní	k2eAgMnSc7d1	výrobní
poradcem	poradce	k1gMnSc7	poradce
této	tento	k3xDgFnSc2	tento
firmy	firma	k1gFnSc2	firma
–	–	k?	–
do	do	k7c2	do
Hranic	Hranice	k1gFnPc2	Hranice
také	také	k9	také
často	často	k6eAd1	často
z	z	k7c2	z
titulu	titul	k1gInSc2	titul
své	svůj	k3xOyFgFnSc2	svůj
funkce	funkce	k1gFnSc2	funkce
zajížděl	zajíždět	k5eAaImAgMnS	zajíždět
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc2	tento
funkce	funkce	k1gFnSc2	funkce
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgMnS	vzdát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
záslužnou	záslužný	k2eAgFnSc4d1	záslužná
činnost	činnost	k1gFnSc4	činnost
na	na	k7c6	na
Hukvaldech	Hukvaldy	k1gInPc6	Hukvaldy
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
poctěn	poctěn	k2eAgInSc1d1	poctěn
–	–	k?	–
jako	jako	k8xS	jako
druhý	druhý	k4xOgMnSc1	druhý
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
po	po	k7c6	po
Leoši	Leoš	k1gMnSc6	Leoš
Janáčkovi	Janáček	k1gMnSc6	Janáček
–	–	k?	–
čestným	čestný	k2eAgNnSc7d1	čestné
občanstvím	občanství	k1gNnSc7	občanství
obce	obec	k1gFnSc2	obec
Hukvaldy	Hukvaldy	k1gInPc4	Hukvaldy
–	–	k?	–
Sklenov	Sklenov	k1gInSc1	Sklenov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
poctu	pocta	k1gFnSc4	pocta
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
hrdý	hrdý	k2eAgInSc1d1	hrdý
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
19	[number]	k4	19
letech	léto	k1gNnPc6	léto
prožitých	prožitý	k2eAgInPc2d1	prožitý
na	na	k7c6	na
Hukvaldech	Hukvaldy	k1gInPc6	Hukvaldy
ulehl	ulehnout	k5eAaPmAgMnS	ulehnout
Leopold	Leopold	k1gMnSc1	Leopold
Sviták	Sviták	k1gMnSc1	Sviták
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1931	[number]	k4	1931
a	a	k8xC	a
víc	hodně	k6eAd2	hodně
už	už	k9	už
lože	lože	k1gNnSc4	lože
neopustil	opustit	k5eNaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Rozloučiv	rozloučit	k5eAaPmDgInS	rozloučit
se	se	k3xPyFc4	se
osobně	osobně	k6eAd1	osobně
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
dětmi	dítě	k1gFnPc7	dítě
na	na	k7c6	na
konci	konec	k1gInSc6	konec
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
usnul	usnout	k5eAaPmAgMnS	usnout
navěky	navěky	k6eAd1	navěky
v	v	k7c4	v
půl	půl	k1xP	půl
sedmé	sedmý	k4xOgFnSc2	sedmý
ráno	ráno	k6eAd1	ráno
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1931	[number]	k4	1931
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
své	svůj	k3xOyFgFnSc2	svůj
jedenasedmdesátilé	jedenasedmdesátilý	k2eAgFnSc2d1	jedenasedmdesátilý
manželky	manželka	k1gFnSc2	manželka
Jenovefy	Jenovef	k1gInPc1	Jenovef
a	a	k8xC	a
její	její	k3xOp3gFnPc1	její
sestry	sestra	k1gFnPc1	sestra
Františky	Františka	k1gFnSc2	Františka
Weyrichové	Weyrichová	k1gFnSc2	Weyrichová
z	z	k7c2	z
Jaktaře	Jaktař	k1gMnSc2	Jaktař
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
13	[number]	k4	13
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1931	[number]	k4	1931
opustil	opustit	k5eAaPmAgMnS	opustit
navždy	navždy	k6eAd1	navždy
svůj	svůj	k3xOyFgInSc4	svůj
domek	domek	k1gInSc4	domek
<g/>
,	,	kIx,	,
doprovázen	doprovázet	k5eAaImNgInS	doprovázet
svými	svůj	k3xOyFgMnPc7	svůj
nejbližšími	blízký	k2eAgMnPc7d3	nejbližší
a	a	k8xC	a
za	za	k7c2	za
obrovské	obrovský	k2eAgFnSc2d1	obrovská
účasti	účast	k1gFnSc2	účast
hukvaldských	hukvaldský	k2eAgMnPc2d1	hukvaldský
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
delegátů	delegát	k1gMnPc2	delegát
kopřivnické	kopřivnický	k2eAgFnSc2d1	kopřivnická
obce	obec	k1gFnSc2	obec
i	i	k8xC	i
zástupců	zástupce	k1gMnPc2	zástupce
firmy	firma	k1gFnSc2	firma
Tatra	Tatra	k1gFnSc1	Tatra
Kopřivnice	Kopřivnice	k1gFnSc1	Kopřivnice
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
vezli	vézt	k5eAaImAgMnP	vézt
na	na	k7c6	na
autech	aut	k1gInPc6	aut
rakev	rakev	k1gFnSc1	rakev
s	s	k7c7	s
tělesnými	tělesný	k2eAgInPc7d1	tělesný
pozůstatky	pozůstatek	k1gInPc7	pozůstatek
Leopolda	Leopold	k1gMnSc2	Leopold
Svitáka	Sviták	k1gMnSc2	Sviták
i	i	k8xC	i
četné	četný	k2eAgInPc4d1	četný
věnce	věnec	k1gInPc4	věnec
a	a	k8xC	a
květiny	květina	k1gFnPc4	květina
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
zvuků	zvuk	k1gInPc2	zvuk
automobilových	automobilový	k2eAgInPc2d1	automobilový
klaksonů	klakson	k1gInPc2	klakson
byl	být	k5eAaImAgMnS	být
Leopold	Leopold	k1gMnSc1	Leopold
Sviták	Sviták	k1gMnSc1	Sviták
uložen	uložit	k5eAaPmNgMnS	uložit
na	na	k7c6	na
hukvaldském	hukvaldský	k2eAgInSc6d1	hukvaldský
hřbitově	hřbitov	k1gInSc6	hřbitov
k	k	k7c3	k
poslednímu	poslední	k2eAgInSc3d1	poslední
odpočinku	odpočinek	k1gInSc3	odpočinek
<g/>
.	.	kIx.	.
</s>
<s>
Manželka	manželka	k1gFnSc1	manželka
Leopolda	Leopold	k1gMnSc2	Leopold
Svitáka	Sviták	k1gMnSc2	Sviták
Jenovefa	Jenovef	k1gMnSc2	Jenovef
se	se	k3xPyFc4	se
dožila	dožít	k5eAaPmAgFnS	dožít
92	[number]	k4	92
let	léto	k1gNnPc2	léto
a	a	k8xC	a
zemřela	zemřít	k5eAaPmAgFnS	zemřít
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1953	[number]	k4	1953
a	a	k8xC	a
pohřbena	pohřben	k2eAgFnSc1d1	pohřbena
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
na	na	k7c6	na
Hukvaldech	Hukvaldy	k1gInPc6	Hukvaldy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rodinném	rodinný	k2eAgInSc6d1	rodinný
hrobě	hrob	k1gInSc6	hrob
spočívají	spočívat	k5eAaImIp3nP	spočívat
ostatky	ostatek	k1gInPc1	ostatek
i	i	k8xC	i
většiny	většina	k1gFnSc2	většina
jejích	její	k3xOp3gFnPc2	její
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
vnoučat	vnouče	k1gNnPc2	vnouče
<g/>
,	,	kIx,	,
pravnoučat	pravnouče	k1gNnPc2	pravnouče
i	i	k8xC	i
jejich	jejich	k3xOp3gMnPc2	jejich
životních	životní	k2eAgMnPc2d1	životní
partnerů	partner	k1gMnPc2	partner
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
manželství	manželství	k1gNnSc2	manželství
Leopolda	Leopold	k1gMnSc2	Leopold
a	a	k8xC	a
Jenovefy	Jenovef	k1gMnPc7	Jenovef
Svitákových	Svitákův	k2eAgFnPc2d1	Svitákova
se	se	k3xPyFc4	se
narodilo	narodit	k5eAaPmAgNnS	narodit
5	[number]	k4	5
dětí	dítě	k1gFnPc2	dítě
<g/>
:	:	kIx,	:
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
,	,	kIx,	,
narozený	narozený	k2eAgMnSc1d1	narozený
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1887	[number]	k4	1887
v	v	k7c6	v
Kopřivnici	Kopřivnice	k1gFnSc6	Kopřivnice
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1955	[number]	k4	1955
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
;	;	kIx,	;
Božena	Božena	k1gFnSc1	Božena
<g/>
,	,	kIx,	,
narozená	narozený	k2eAgFnSc1d1	narozená
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1888	[number]	k4	1888
v	v	k7c6	v
Kopřivnici	Kopřivnice	k1gFnSc6	Kopřivnice
<g/>
,	,	kIx,	,
zemřela	zemřít	k5eAaPmAgFnS	zemřít
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
;	;	kIx,	;
Otakar	Otakar	k1gMnSc1	Otakar
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Otakar	Otakar	k1gMnSc1	Otakar
Sviták	Sviták	k1gMnSc1	Sviták
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
<g/>
,	,	kIx,	,
narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Kopřivnici	Kopřivnice	k1gFnSc6	Kopřivnice
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1900	[number]	k4	1900
a	a	k8xC	a
zemřela	zemřít	k5eAaPmAgFnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
<g/>
;	;	kIx,	;
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
,	,	kIx,	,
narozený	narozený	k2eAgMnSc1d1	narozený
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1904	[number]	k4	1904
v	v	k7c6	v
Kopřivnici	Kopřivnice	k1gFnSc6	Kopřivnice
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1965	[number]	k4	1965
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
