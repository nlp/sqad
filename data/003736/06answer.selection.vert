<s>
Všechny	všechen	k3xTgInPc1	všechen
jazyky	jazyk	k1gInPc1	jazyk
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
dnes	dnes	k6eAd1	dnes
píší	psát	k5eAaImIp3nP	psát
latinkou	latinka	k1gFnSc7	latinka
<g/>
,	,	kIx,	,
používají	používat	k5eAaImIp3nP	používat
také	také	k9	také
tzv.	tzv.	kA	tzv.
arabské	arabský	k2eAgFnSc2d1	arabská
číslice	číslice	k1gFnSc2	číslice
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
