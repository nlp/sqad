<p>
<s>
Heřmánek	heřmánek	k1gInSc1	heřmánek
pravý	pravý	k2eAgInSc1d1	pravý
(	(	kIx(	(
<g/>
Matricaria	Matricarium	k1gNnSc2	Matricarium
chamomilla	chamomillo	k1gNnSc2	chamomillo
<g/>
,	,	kIx,	,
synonymum	synonymum	k1gNnSc4	synonymum
Matricaria	Matricarium	k1gNnSc2	Matricarium
recutita	recutitum	k1gNnSc2	recutitum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
také	také	k9	také
heřmánek	heřmánek	k1gInSc1	heřmánek
lékařský	lékařský	k2eAgInSc1d1	lékařský
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jednoletá	jednoletý	k2eAgFnSc1d1	jednoletá
nebo	nebo	k8xC	nebo
ozimá	ozimý	k2eAgFnSc1d1	ozimá
léčivá	léčivý	k2eAgFnSc1d1	léčivá
rostlina	rostlina	k1gFnSc1	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
hvězdnicovitých	hvězdnicovitý	k2eAgMnPc2d1	hvězdnicovitý
<g/>
.	.	kIx.	.
</s>
<s>
Hojně	hojně	k6eAd1	hojně
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
v	v	k7c6	v
kosmetice	kosmetika	k1gFnSc6	kosmetika
a	a	k8xC	a
léčitelství	léčitelství	k1gNnSc6	léčitelství
<g/>
,	,	kIx,	,
v	v	k7c6	v
aromaterapii	aromaterapie	k1gFnSc6	aromaterapie
i	i	k8xC	i
homeopatii	homeopatie	k1gFnSc6	homeopatie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejoblíbenějších	oblíbený	k2eAgFnPc2d3	nejoblíbenější
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
dlouho	dlouho	k6eAd1	dlouho
známých	známý	k2eAgFnPc2d1	známá
léčivých	léčivý	k2eAgFnPc2d1	léčivá
bylin	bylina	k1gFnPc2	bylina
s	s	k7c7	s
prakticky	prakticky	k6eAd1	prakticky
univerzálním	univerzální	k2eAgInSc7d1	univerzální
účinkem	účinek	k1gInSc7	účinek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Názvosloví	názvosloví	k1gNnSc1	názvosloví
a	a	k8xC	a
systematika	systematika	k1gFnSc1	systematika
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Lidové	lidový	k2eAgInPc4d1	lidový
názvy	název	k1gInPc4	název
a	a	k8xC	a
etymologie	etymologie	k1gFnPc4	etymologie
===	===	k?	===
</s>
</p>
<p>
<s>
Lidově	lidově	k6eAd1	lidově
nazýván	nazýván	k2eAgInSc1d1	nazýván
rumánek	rumánek	k1gInSc1	rumánek
<g/>
,	,	kIx,	,
marunka	marunka	k1gFnSc1	marunka
<g/>
,	,	kIx,	,
harmaníček	harmaníček	k1gInSc1	harmaníček
či	či	k8xC	či
kamilka	kamilka	k1gFnSc1	kamilka
<g/>
.	.	kIx.	.
</s>
<s>
Odborný	odborný	k2eAgInSc1d1	odborný
název	název	k1gInSc1	název
pochází	pocházet	k5eAaImIp3nS	pocházet
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
z	z	k7c2	z
latinského	latinský	k2eAgNnSc2d1	latinské
slova	slovo	k1gNnSc2	slovo
mater	mater	k1gFnSc1	mater
(	(	kIx(	(
<g/>
matka	matka	k1gFnSc1	matka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
matrix	matrix	k1gInSc1	matrix
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
děloha	děloha	k1gFnSc1	děloha
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
podle	podle	k7c2	podle
dutého	dutý	k2eAgNnSc2d1	duté
květního	květní	k2eAgNnSc2d1	květní
lůžka	lůžko	k1gNnSc2	lůžko
nebo	nebo	k8xC	nebo
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
bylinou	bylina	k1gFnSc7	bylina
užívanou	užívaný	k2eAgFnSc4d1	užívaná
též	též	k9	též
k	k	k7c3	k
léčení	léčení	k1gNnSc3	léčení
ženských	ženský	k2eAgFnPc2d1	ženská
nemocí	nemoc	k1gFnPc2	nemoc
a	a	k8xC	a
hygieně	hygiena	k1gFnSc3	hygiena
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
výraz	výraz	k1gInSc1	výraz
heřmánek	heřmánek	k1gInSc1	heřmánek
potom	potom	k8xC	potom
z	z	k7c2	z
latinského	latinský	k2eAgNnSc2d1	latinské
armanilla	armanillo	k1gNnSc2	armanillo
s	s	k7c7	s
přidáním	přidání	k1gNnSc7	přidání
počátečního	počáteční	k2eAgInSc2d1	počáteční
"	"	kIx"	"
<g/>
h-	h-	k?	h-
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
staročesky	staročesky	k6eAd1	staročesky
ještě	ještě	k9	ještě
ormánek	ormánek	k1gInSc1	ormánek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Systematické	systematický	k2eAgNnSc4d1	systematické
zařazení	zařazení	k1gNnSc4	zařazení
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
čeledi	čeleď	k1gFnSc2	čeleď
hvězdnicovitých	hvězdnicovitý	k2eAgFnPc2d1	hvězdnicovitý
patří	patřit	k5eAaImIp3nS	patřit
rod	rod	k1gInSc4	rod
Matricaria	Matricarium	k1gNnSc2	Matricarium
do	do	k7c2	do
její	její	k3xOp3gFnSc2	její
nejrozsáhlejší	rozsáhlý	k2eAgFnSc2d3	nejrozsáhlejší
podčeledi	podčeleď	k1gFnSc2	podčeleď
Asteroideae	Asteroidea	k1gFnSc2	Asteroidea
a	a	k8xC	a
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
tribu	trib	k1gInSc2	trib
Anthemidae	Anthemida	k1gInSc2	Anthemida
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
podle	podle	k7c2	podle
molekulárních	molekulární	k2eAgNnPc2d1	molekulární
studií	studio	k1gNnPc2	studio
spoluvytváří	spoluvytvářet	k5eAaImIp3nS	spoluvytvářet
monofyletický	monofyletický	k2eAgInSc1d1	monofyletický
subtribus	subtribus	k1gInSc1	subtribus
Matricariinae	Matricariina	k1gMnSc2	Matricariina
společně	společně	k6eAd1	společně
rody	rod	k1gInPc4	rod
Achillea	Achilleus	k1gMnSc2	Achilleus
(	(	kIx(	(
<g/>
řebříček	řebříček	k1gInSc1	řebříček
<g/>
)	)	kIx)	)
v	v	k7c6	v
rozšířeném	rozšířený	k2eAgNnSc6d1	rozšířené
pojetí	pojetí	k1gNnSc6	pojetí
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
rodů	rod	k1gInPc2	rod
Leucocyclus	Leucocyclus	k1gInSc1	Leucocyclus
a	a	k8xC	a
Otanthus	Otanthus	k1gInSc1	Otanthus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Anacyclus	Anacyclus	k1gInSc1	Anacyclus
a	a	k8xC	a
Heliocauta	Heliocaut	k2eAgFnSc1d1	Heliocaut
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nomenklaturická	Nomenklaturický	k2eAgFnSc1d1	Nomenklaturický
poznámka	poznámka	k1gFnSc1	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Matricaria	Matricarium	k1gNnSc2	Matricarium
recutita	recutitum	k1gNnSc2	recutitum
L.	L.	kA	L.
<g/>
,	,	kIx,	,
1753	[number]	k4	1753
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
synonym	synonymum	k1gNnPc2	synonymum
<g/>
,	,	kIx,	,
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
poměrně	poměrně	k6eAd1	poměrně
komplikované	komplikovaný	k2eAgInPc4d1	komplikovaný
nomenklatorické	nomenklatorický	k2eAgInPc4d1	nomenklatorický
historiiː	historiiː	k?	historiiː
</s>
</p>
<p>
<s>
==	==	k?	==
Vzhled	vzhled	k1gInSc4	vzhled
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jednoletá	jednoletý	k2eAgFnSc1d1	jednoletá
nebo	nebo	k8xC	nebo
ozimá	ozimý	k2eAgFnSc1d1	ozimá
<g/>
,	,	kIx,	,
15	[number]	k4	15
až	až	k9	až
50	[number]	k4	50
cm	cm	kA	cm
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
příjemně	příjemně	k6eAd1	příjemně
vonící	vonící	k2eAgFnSc1d1	vonící
bylina	bylina	k1gFnSc1	bylina
<g/>
.	.	kIx.	.
</s>
<s>
Lodyha	lodyha	k1gFnSc1	lodyha
je	být	k5eAaImIp3nS	být
přímá	přímý	k2eAgFnSc1d1	přímá
nebo	nebo	k8xC	nebo
vystoupaná	vystoupaný	k2eAgFnSc1d1	vystoupaný
<g/>
,	,	kIx,	,
lysá	lysý	k2eAgFnSc1d1	Lysá
<g/>
,	,	kIx,	,
příp	příp	kA	příp
<g/>
.	.	kIx.	.
pod	pod	k7c4	pod
úbory	úbor	k1gInPc4	úbor
řídce	řídce	k6eAd1	řídce
chlupatá	chlupatý	k2eAgFnSc1d1	chlupatá
a	a	k8xC	a
zpravidla	zpravidla	k6eAd1	zpravidla
bohatě	bohatě	k6eAd1	bohatě
větvená	větvený	k2eAgFnSc1d1	větvená
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
řídké	řídký	k2eAgInPc1d1	řídký
<g/>
,	,	kIx,	,
střídavě	střídavě	k6eAd1	střídavě
přisedlé	přisedlý	k2eAgInPc1d1	přisedlý
<g/>
,	,	kIx,	,
3-7	[number]	k4	3-7
cm	cm	kA	cm
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
x	x	k?	x
zpeřené	zpeřený	k2eAgFnPc4d1	zpeřená
v	v	k7c4	v
drobné	drobný	k2eAgInPc4d1	drobný
čárkovité	čárkovitý	k2eAgInPc4d1	čárkovitý
až	až	k8xS	až
niťovité	niťovitý	k2eAgInPc4d1	niťovitý
úkrojky	úkrojek	k1gInPc4	úkrojek
<g/>
.	.	kIx.	.
</s>
<s>
Kořen	kořen	k1gInSc1	kořen
je	být	k5eAaImIp3nS	být
krátký	krátký	k2eAgInSc1d1	krátký
<g/>
,	,	kIx,	,
vřetenovitého	vřetenovitý	k2eAgInSc2d1	vřetenovitý
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
<g/>
Květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
jako	jako	k9	jako
u	u	k7c2	u
ostatních	ostatní	k2eAgFnPc2d1	ostatní
hvězdnicovitých	hvězdnicovitý	k2eAgFnPc2d1	hvězdnicovitý
rostlin	rostlina	k1gFnPc2	rostlina
drobné	drobný	k2eAgFnPc4d1	drobná
<g/>
,	,	kIx,	,
uspořádané	uspořádaný	k2eAgFnPc4d1	uspořádaná
do	do	k7c2	do
úborů	úbor	k1gInPc2	úbor
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
10-25	[number]	k4	10-25
mm	mm	kA	mm
vyrůstajících	vyrůstající	k2eAgInPc2d1	vyrůstající
na	na	k7c4	na
3-10	[number]	k4	3-10
cm	cm	kA	cm
dlouhých	dlouhý	k2eAgFnPc6d1	dlouhá
stopkách	stopka	k1gFnPc6	stopka
<g/>
;	;	kIx,	;
úbory	úbor	k1gInPc1	úbor
jsou	být	k5eAaImIp3nP	být
uspořádány	uspořádat	k5eAaPmNgInP	uspořádat
v	v	k7c6	v
řídkém	řídký	k2eAgNnSc6d1	řídké
vrcholičnatém	vrcholičnatý	k2eAgNnSc6d1	vrcholičnaté
květenství	květenství	k1gNnSc6	květenství
a	a	k8xC	a
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
rostlině	rostlina	k1gFnSc6	rostlina
jich	on	k3xPp3gInPc2	on
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
až	až	k9	až
200	[number]	k4	200
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úboru	úbor	k1gInSc6	úbor
jsou	být	k5eAaImIp3nP	být
květy	květ	k1gInPc4	květ
dvojího	dvojí	k4xRgMnSc2	dvojí
druhuː	druhuː	k?	druhuː
bílé	bílý	k2eAgInPc4d1	bílý
jazykovité	jazykovitý	k2eAgInPc4d1	jazykovitý
květy	květ	k1gInPc4	květ
s	s	k7c7	s
6-9	[number]	k4	6-9
mm	mm	kA	mm
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
ligulou	ligula	k1gFnSc7	ligula
<g/>
,	,	kIx,	,
kterých	který	k3yRgNnPc2	který
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
15	[number]	k4	15
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
uspořádány	uspořádat	k5eAaPmNgInP	uspořádat
po	po	k7c6	po
obvodu	obvod	k1gInSc6	obvod
<g/>
;	;	kIx,	;
brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
rozvití	rozvití	k1gNnSc6	rozvití
úboru	úbor	k1gInSc2	úbor
se	se	k3xPyFc4	se
sklánějí	sklánět	k5eAaImIp3nP	sklánět
směrem	směr	k1gInSc7	směr
dolů	dolů	k6eAd1	dolů
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
terči	terč	k1gInSc6	terč
úboru	úbor	k1gInSc2	úbor
jsou	být	k5eAaImIp3nP	být
květy	květ	k1gInPc1	květ
žluté	žlutý	k2eAgInPc1d1	žlutý
<g/>
,	,	kIx,	,
trubkovité	trubkovitý	k2eAgInPc1d1	trubkovitý
<g/>
,	,	kIx,	,
pětičetné	pětičetný	k2eAgInPc1d1	pětičetný
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
na	na	k7c6	na
kuželovitě	kuželovitě	k6eAd1	kuželovitě
vyklenutém	vyklenutý	k2eAgNnSc6d1	vyklenuté
květním	květní	k2eAgNnSc6d1	květní
lůžku	lůžko	k1gNnSc6	lůžko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
uvnitř	uvnitř	k7c2	uvnitř
duté	dutý	k2eAgFnSc2d1	dutá
a	a	k8xC	a
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
je	být	k5eAaImIp3nS	být
bez	bez	k7c2	bez
plevek	plevka	k1gFnPc2	plevka
<g/>
.	.	kIx.	.
</s>
<s>
Zákrov	zákrov	k1gInSc1	zákrov
je	být	k5eAaImIp3nS	být
polokulovitý	polokulovitý	k2eAgInSc1d1	polokulovitý
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
listeny	listen	k1gInPc1	listen
jsou	být	k5eAaImIp3nP	být
víceřadě	víceřadě	k6eAd1	víceřadě
uspořádané	uspořádaný	k2eAgFnPc1d1	uspořádaná
<g/>
,	,	kIx,	,
světle	světle	k6eAd1	světle
zelené	zelené	k1gNnSc1	zelené
s	s	k7c7	s
tmavší	tmavý	k2eAgFnSc7d2	tmavší
hnědou	hnědý	k2eAgFnSc7d1	hnědá
žilkou	žilka	k1gFnSc7	žilka
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
,	,	kIx,	,
tupě	tupě	k6eAd1	tupě
podlouhlé	podlouhlý	k2eAgFnPc1d1	podlouhlá
<g/>
,	,	kIx,	,
s	s	k7c7	s
širokým	široký	k2eAgInSc7d1	široký
suchomázdřitým	suchomázdřitý	k2eAgInSc7d1	suchomázdřitý
lemem	lem	k1gInSc7	lem
<g/>
.	.	kIx.	.
<g/>
Heřmánek	heřmánek	k1gInSc1	heřmánek
kvete	kvést	k5eAaImIp3nS	kvést
od	od	k7c2	od
května	květen	k1gInSc2	květen
či	či	k8xC	či
června	červen	k1gInSc2	červen
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
až	až	k8xS	až
září	září	k1gNnSc2	září
<g/>
;	;	kIx,	;
opylován	opylován	k2eAgInSc1d1	opylován
je	být	k5eAaImIp3nS	být
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
,	,	kIx,	,
docházet	docházet	k5eAaImF	docházet
může	moct	k5eAaImIp3nS	moct
i	i	k9	i
k	k	k7c3	k
autogamii	autogamie	k1gFnSc3	autogamie
<g/>
.	.	kIx.	.
</s>
<s>
Plodem	plod	k1gInSc7	plod
je	být	k5eAaImIp3nS	být
drobná	drobný	k2eAgFnSc1d1	drobná
neochmýřená	ochmýřený	k2eNgFnSc1d1	neochmýřená
nažka	nažka	k1gFnSc1	nažka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
1	[number]	k4	1
mm	mm	kA	mm
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
lehce	lehko	k6eAd1	lehko
zakřivená	zakřivený	k2eAgFnSc1d1	zakřivená
a	a	k8xC	a
smáčklá	smáčklý	k2eAgFnSc1d1	smáčklá
<g/>
,	,	kIx,	,
na	na	k7c4	na
bázi	báze	k1gFnSc4	báze
zúžená	zúžený	k2eAgFnSc1d1	zúžená
<g/>
,	,	kIx,	,
na	na	k7c6	na
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
straně	strana	k1gFnSc6	strana
s	s	k7c7	s
4-5	[number]	k4	4-5
nízkými	nízký	k2eAgInPc7d1	nízký
žebry	žebr	k1gInPc7	žebr
<g/>
.	.	kIx.	.
</s>
<s>
Semena	semeno	k1gNnPc1	semeno
jsou	být	k5eAaImIp3nP	být
běžně	běžně	k6eAd1	běžně
šířena	šířit	k5eAaImNgFnS	šířit
živočichy	živočich	k1gMnPc7	živočich
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ploidie	Ploidie	k1gFnSc1	Ploidie
druhu	druh	k1gInSc2	druh
je	být	k5eAaImIp3nS	být
2	[number]	k4	2
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Možnost	možnost	k1gFnSc1	možnost
záměny	záměna	k1gFnSc2	záměna
===	===	k?	===
</s>
</p>
<p>
<s>
Heřmánek	heřmánek	k1gInSc1	heřmánek
pravý	pravý	k2eAgInSc1d1	pravý
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zaměněn	zaměnit	k5eAaPmNgInS	zaměnit
za	za	k7c4	za
podobný	podobný	k2eAgInSc4d1	podobný
heřmánek	heřmánek	k1gInSc4	heřmánek
římský	římský	k2eAgMnSc1d1	římský
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
též	též	k9	též
léčivý	léčivý	k2eAgMnSc1d1	léčivý
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
za	za	k7c4	za
neúčinné	účinný	k2eNgInPc4d1	neúčinný
druhy	druh	k1gInPc4	druh
heřmánkovec	heřmánkovec	k1gInSc1	heřmánkovec
nevonný	vonný	k2eNgMnSc1d1	nevonný
nebo	nebo	k8xC	nebo
rmen	rmen	k1gInSc1	rmen
rolní	rolní	k2eAgInSc1d1	rolní
<g/>
.	.	kIx.	.
</s>
<s>
Heřmánek	heřmánek	k1gInSc1	heřmánek
pravý	pravý	k2eAgInSc1d1	pravý
jde	jít	k5eAaImIp3nS	jít
poznat	poznat	k5eAaPmF	poznat
podle	podle	k7c2	podle
typické	typický	k2eAgFnSc2d1	typická
intenzivní	intenzivní	k2eAgFnSc2d1	intenzivní
vůně	vůně	k1gFnSc2	vůně
a	a	k8xC	a
žlutého	žlutý	k2eAgInSc2d1	žlutý
vyklenutého	vyklenutý	k2eAgInSc2d1	vyklenutý
středu	střed	k1gInSc2	střed
květů	květ	k1gInPc2	květ
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
heřmánkovce	heřmánkovec	k1gInSc2	heřmánkovec
ani	ani	k8xC	ani
rmenu	rmen	k1gInSc2	rmen
nemají	mít	k5eNaImIp3nP	mít
charakteristickou	charakteristický	k2eAgFnSc4d1	charakteristická
vůni	vůně	k1gFnSc4	vůně
<g/>
,	,	kIx,	,
žádný	žádný	k3yNgInSc1	žádný
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
také	také	k9	také
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
heřmánku	heřmánek	k1gInSc2	heřmánek
pravého	pravý	k2eAgNnSc2d1	pravé
nemá	mít	k5eNaImIp3nS	mít
vyklenuté	vyklenutý	k2eAgNnSc1d1	vyklenuté
a	a	k8xC	a
duté	dutý	k2eAgNnSc1d1	duté
květní	květní	k2eAgNnSc1d1	květní
lůžko	lůžko	k1gNnSc1	lůžko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Ekologické	ekologický	k2eAgInPc4d1	ekologický
nároky	nárok	k1gInPc4	nárok
a	a	k8xC	a
rozšíření	rozšíření	k1gNnSc4	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Heřmánek	heřmánek	k1gInSc1	heřmánek
pravý	pravý	k2eAgInSc1d1	pravý
je	být	k5eAaImIp3nS	být
hemikryptofyt	hemikryptofyt	k1gInSc1	hemikryptofyt
nebo	nebo	k8xC	nebo
častěji	často	k6eAd2	často
terofyt	terofyt	k1gInSc1	terofyt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
roste	růst	k5eAaImIp3nS	růst
na	na	k7c6	na
ruderálních	ruderální	k2eAgNnPc6d1	ruderální
stanovištích	stanoviště	k1gNnPc6	stanoviště
a	a	k8xC	a
rumištích	rumiště	k1gNnPc6	rumiště
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
cest	cesta	k1gFnPc2	cesta
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
solených	solený	k2eAgInPc2d1	solený
okrajů	okraj	k1gInPc2	okraj
silnic	silnice	k1gFnPc2	silnice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
mezích	mez	k1gFnPc6	mez
<g/>
,	,	kIx,	,
úhorech	úhor	k1gInPc6	úhor
a	a	k8xC	a
slaniskách	slanisko	k1gNnPc6	slanisko
i	i	k8xC	i
jako	jako	k8xS	jako
polní	polní	k2eAgFnSc1d1	polní
plevel	plevel	k1gFnSc1	plevel
<g/>
.	.	kIx.	.
</s>
<s>
Vyhovují	vyhovovat	k5eAaImIp3nP	vyhovovat
mu	on	k3xPp3gMnSc3	on
půdy	půda	k1gFnPc4	půda
písčité	písčitý	k2eAgFnPc4d1	písčitá
<g/>
,	,	kIx,	,
hlinité	hlinitý	k2eAgFnPc4d1	hlinitá
i	i	k8xC	i
jílovité	jílovitý	k2eAgFnPc4d1	jílovitá
<g/>
,	,	kIx,	,
nepodmáčené	podmáčený	k2eNgFnPc4d1	podmáčený
<g/>
,	,	kIx,	,
mírně	mírně	k6eAd1	mírně
kyselé	kyselý	k2eAgInPc1d1	kyselý
nebo	nebo	k8xC	nebo
neutrální	neutrální	k2eAgInPc1d1	neutrální
<g/>
,	,	kIx,	,
nevápnité	vápnitý	k2eNgInPc1d1	vápnitý
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInPc4d2	veliký
nároky	nárok	k1gInPc4	nárok
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
světlo	světlo	k1gNnSc4	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
fytocenologii	fytocenologie	k1gFnSc6	fytocenologie
je	být	k5eAaImIp3nS	být
diagnostickým	diagnostický	k2eAgInSc7d1	diagnostický
druhem	druh	k1gInSc7	druh
svazu	svaz	k1gInSc2	svaz
Scleranthion	Scleranthion	k1gInSc4	Scleranthion
annui	annui	k1gNnSc2	annui
(	(	kIx(	(
<g/>
synonymum	synonymum	k1gNnSc1	synonymum
Aphanion	Aphanion	k1gInSc1	Aphanion
arvensis	arvensis	k1gFnSc1	arvensis
<g/>
,	,	kIx,	,
plevelová	plevelový	k2eAgFnSc1d1	plevelová
vegetace	vegetace	k1gFnSc1	vegetace
obilnin	obilnina	k1gFnPc2	obilnina
na	na	k7c6	na
minerálně	minerálně	k6eAd1	minerálně
chudých	chudý	k2eAgFnPc6d1	chudá
půdách	půda	k1gFnPc6	půda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
drobnější	drobný	k2eAgInSc1d2	drobnější
halofilní	halofilní	k2eAgInSc1d1	halofilní
morfotyp	morfotyp	k1gInSc1	morfotyp
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k8xS	jako
Matricaria	Matricarium	k1gNnPc1	Matricarium
recutita	recutit	k1gMnSc2	recutit
f.	f.	k?	f.
bayeri	bayer	k1gFnSc2	bayer
pak	pak	k8xC	pak
svazu	svaz	k1gInSc2	svaz
suchých	suchý	k2eAgInPc2d1	suchý
slaniskových	slaniskový	k2eAgInPc2d1	slaniskový
trávníků	trávník	k1gInPc2	trávník
Puccelinion	Puccelinion	k1gInSc4	Puccelinion
limosae	limosa	k1gFnSc2	limosa
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
léčivka	léčivka	k1gFnSc1	léčivka
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
pěstován	pěstovat	k5eAaImNgInS	pěstovat
v	v	k7c6	v
polních	polní	k2eAgFnPc6d1	polní
kulturách	kultura	k1gFnPc6	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dlouhodenní	dlouhodenní	k2eAgFnSc7d1	dlouhodenní
rostlinou	rostlina	k1gFnSc7	rostlina
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnPc4	jeho
semena	semeno	k1gNnPc4	semeno
klíčí	klíčit	k5eAaImIp3nS	klíčit
na	na	k7c6	na
světle	světlo	k1gNnSc6	světlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původním	původní	k2eAgInSc7d1	původní
druhem	druh	k1gInSc7	druh
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
areál	areál	k1gInSc1	areál
jeho	jeho	k3xOp3gNnSc2	jeho
rozšíření	rozšíření	k1gNnSc2	rozšíření
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
téměř	téměř	k6eAd1	téměř
celou	celý	k2eAgFnSc4d1	celá
Evropu	Evropa	k1gFnSc4	Evropa
a	a	k8xC	a
západní	západní	k2eAgFnSc6d1	západní
a	a	k8xC	a
Střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
místy	místo	k1gNnPc7	místo
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
a	a	k8xC	a
v	v	k7c6	v
mírnějších	mírný	k2eAgFnPc6d2	mírnější
oblastech	oblast	k1gFnPc6	oblast
Sibiře	Sibiř	k1gFnSc2	Sibiř
až	až	k9	až
po	po	k7c4	po
Dálný	dálný	k2eAgInSc4d1	dálný
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Zavlečen	zavlečen	k2eAgInSc1d1	zavlečen
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
Makaronézie	Makaronézie	k1gFnSc2	Makaronézie
<g/>
,	,	kIx,	,
obou	dva	k4xCgFnPc2	dva
Amerik	Amerika	k1gFnPc2	Amerika
<g/>
,	,	kIx,	,
do	do	k7c2	do
Austrálie	Austrálie	k1gFnSc2	Austrálie
a	a	k8xC	a
na	na	k7c4	na
Nový	nový	k2eAgInSc4d1	nový
Zéland	Zéland	k1gInSc4	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
zdomácnělým	zdomácnělý	k2eAgInSc7d1	zdomácnělý
archeofytem	archeofyt	k1gInSc7	archeofyt
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
nejhojněji	hojně	k6eAd3	hojně
v	v	k7c6	v
termofytiku	termofytikum	k1gNnSc6	termofytikum
a	a	k8xC	a
v	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
polohách	poloha	k1gFnPc6	poloha
mezofytika	mezofytikum	k1gNnSc2	mezofytikum
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
polohách	poloha	k1gFnPc6	poloha
a	a	k8xC	a
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
vápenitými	vápenitý	k2eAgFnPc7d1	vápenitá
půdami	půda	k1gFnPc7	půda
pouze	pouze	k6eAd1	pouze
přechodně	přechodně	k6eAd1	přechodně
<g/>
,	,	kIx,	,
především	především	k9	především
podél	podél	k7c2	podél
komunikací	komunikace	k1gFnPc2	komunikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsahové	obsahový	k2eAgFnPc1d1	obsahová
látky	látka	k1gFnPc1	látka
==	==	k?	==
</s>
</p>
<p>
<s>
Dosud	dosud	k6eAd1	dosud
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
heřmánku	heřmánek	k1gInSc6	heřmánek
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
120	[number]	k4	120
druhů	druh	k1gInPc2	druh
sekundárních	sekundární	k2eAgInPc2d1	sekundární
metabolitů	metabolit	k1gInPc2	metabolit
s	s	k7c7	s
potenciální	potenciální	k2eAgFnSc7d1	potenciální
farmakologickou	farmakologický	k2eAgFnSc7d1	farmakologická
účinností	účinnost	k1gFnSc7	účinnost
<g/>
.	.	kIx.	.
</s>
<s>
Aktivními	aktivní	k2eAgFnPc7d1	aktivní
látkami	látka	k1gFnPc7	látka
jsou	být	k5eAaImIp3nP	být
silice	silice	k1gFnPc4	silice
s	s	k7c7	s
modravými	modravý	k2eAgInPc7d1	modravý
azuleny	azulen	k1gInPc7	azulen
a	a	k8xC	a
proazuleny	proazulen	k2eAgFnPc1d1	proazulen
<g/>
,	,	kIx,	,
seskviterpeny	seskviterpen	k2eAgFnPc1d1	seskviterpen
a	a	k8xC	a
četné	četný	k2eAgInPc1d1	četný
flavonoidy	flavonoid	k1gInPc1	flavonoid
<g/>
,	,	kIx,	,
hořčiny	hořčina	k1gFnPc1	hořčina
<g/>
,	,	kIx,	,
taniny	tanin	k1gInPc1	tanin
<g/>
,	,	kIx,	,
mastné	mastný	k2eAgFnPc1d1	mastná
kyseliny	kyselina	k1gFnPc1	kyselina
a	a	k8xC	a
sliz	sliz	k1gInSc1	sliz
<g/>
.	.	kIx.	.
<g/>
Jmenovitý	jmenovitý	k2eAgInSc1d1	jmenovitý
soupis	soupis	k1gInSc1	soupis
látek	látka	k1gFnPc2	látka
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
např.	např.	kA	např.
terpeny	terpen	k1gInPc1	terpen
α	α	k?	α
<g/>
,	,	kIx,	,
apigenin	apigenin	k2eAgInSc1d1	apigenin
<g/>
,	,	kIx,	,
azulen	azulen	k1gInSc1	azulen
<g/>
,	,	kIx,	,
β	β	k?	β
<g/>
,	,	kIx,	,
bisabolen	bisabolen	k2eAgInSc1d1	bisabolen
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
borneol	borneol	k1gInSc1	borneol
<g/>
,	,	kIx,	,
trans-α	trans-α	k?	trans-α
<g/>
,	,	kIx,	,
trans-β	trans-β	k?	trans-β
<g/>
,	,	kIx,	,
farnesol	farnesol	k1gInSc1	farnesol
<g/>
,	,	kIx,	,
geraniol	geraniol	k1gInSc1	geraniol
<g/>
,	,	kIx,	,
guajazulen	guajazulen	k2eAgInSc1d1	guajazulen
<g/>
,	,	kIx,	,
chamazulen	chamazulen	k2eAgInSc1d1	chamazulen
<g/>
,	,	kIx,	,
chamomillol	chamomillol	k1gInSc1	chamomillol
<g/>
,	,	kIx,	,
karyofylen	karyofylen	k1gInSc1	karyofylen
<g/>
,	,	kIx,	,
kemferol	kemferol	k1gInSc1	kemferol
<g/>
,	,	kIx,	,
levonenol	levonenol	k1gInSc1	levonenol
<g/>
,	,	kIx,	,
matricin	matricin	k2eAgInSc1d1	matricin
<g/>
,	,	kIx,	,
matrikarin	matrikarin	k1gInSc1	matrikarin
<g/>
,	,	kIx,	,
thujon	thujon	k1gInSc1	thujon
<g/>
,	,	kIx,	,
aromatické	aromatický	k2eAgFnPc1d1	aromatická
kyseliny	kyselina	k1gFnPc1	kyselina
kyselinu	kyselina	k1gFnSc4	kyselina
salicylovou	salicylový	k2eAgFnSc4d1	salicylová
<g/>
,	,	kIx,	,
kyselinu	kyselina	k1gFnSc4	kyselina
<g />
.	.	kIx.	.
</s>
<s>
2,4	[number]	k4	2,4
<g/>
-dihydroxybenzoovou	ihydroxybenzoová	k1gFnSc4	-dihydroxybenzoová
<g/>
,	,	kIx,	,
kyselinu	kyselina	k1gFnSc4	kyselina
2,5	[number]	k4	2,5
<g/>
-dihydroxybenzoovou	ihydroxybenzoovat	k5eAaPmIp3nP	-dihydroxybenzoovat
<g/>
,	,	kIx,	,
kyselinu	kyselina	k1gFnSc4	kyselina
3,4	[number]	k4	3,4
<g/>
-dihydroxyskořicovou	ihydroxyskořicová	k1gFnSc4	-dihydroxyskořicová
<g/>
,	,	kIx,	,
kyselinu	kyselina	k1gFnSc4	kyselina
4	[number]	k4	4
<g/>
-methoxybenzoovou	ethoxybenzoovat	k5eAaPmIp3nP	-methoxybenzoovat
<g/>
,	,	kIx,	,
kyselinu	kyselina	k1gFnSc4	kyselina
kávovou	kávový	k2eAgFnSc4d1	kávová
<g/>
,	,	kIx,	,
kumariny	kumarin	k1gInPc1	kumarin
kyselinu	kyselina	k1gFnSc4	kyselina
kumarovou	kumarová	k1gFnSc7	kumarová
<g/>
,	,	kIx,	,
umbeliferon	umbeliferon	k1gInSc1	umbeliferon
<g/>
,	,	kIx,	,
herniarin	herniarin	k1gInSc1	herniarin
<g/>
,	,	kIx,	,
kumarin	kumarin	k1gInSc1	kumarin
<g/>
,	,	kIx,	,
steroidy	steroid	k1gInPc1	steroid
sitosterol	sitosterola	k1gFnPc2	sitosterola
<g/>
,	,	kIx,	,
stigmasterol	stigmasterol	k1gInSc1	stigmasterol
<g/>
,	,	kIx,	,
flavonoidy	flavonoid	k1gInPc1	flavonoid
apigenin	apigenina	k1gFnPc2	apigenina
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
luteolin	luteolin	k1gInSc1	luteolin
<g/>
,	,	kIx,	,
kvercitin	kvercitin	k1gInSc1	kvercitin
<g/>
,	,	kIx,	,
kvercetrin	kvercetrin	k1gInSc1	kvercetrin
<g/>
,	,	kIx,	,
sacharidy	sacharid	k1gInPc1	sacharid
fruktózu	fruktóza	k1gFnSc4	fruktóza
<g/>
,	,	kIx,	,
glukózu	glukóza	k1gFnSc4	glukóza
<g/>
,	,	kIx,	,
rhamnózu	rhamnóza	k1gFnSc4	rhamnóza
<g/>
,	,	kIx,	,
xylózu	xylóza	k1gFnSc4	xylóza
<g/>
,	,	kIx,	,
glykosidy	glykosid	k1gInPc4	glykosid
apigenin-	apigenin-	k?	apigenin-
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
"	"	kIx"	"
<g/>
O-acetyl	Ocetyl	k1gInSc1	O-acetyl
<g/>
)	)	kIx)	)
<g/>
glukosid	glukosid	k1gInSc1	glukosid
<g/>
,	,	kIx,	,
apigenin-	apigenin-	k?	apigenin-
<g/>
7	[number]	k4	7
<g/>
-glukosid	lukosida	k1gFnPc2	-glukosida
<g/>
,	,	kIx,	,
apigenin-	apigenin-	k?	apigenin-
<g/>
7	[number]	k4	7
<g/>
-rutinosid	utinosida	k1gFnPc2	-rutinosida
<g/>
,	,	kIx,	,
sitosterol-glukosid	sitosterollukosida	k1gFnPc2	sitosterol-glukosida
<g/>
,	,	kIx,	,
luteolin-	luteolin-	k?	luteolin-
<g/>
7	[number]	k4	7
<g/>
-glukosid	lukosida	k1gFnPc2	-glukosida
<g/>
,	,	kIx,	,
luteoloin-	luteoloin-	k?	luteoloin-
<g/>
7	[number]	k4	7
<g/>
-rhamnoglukosid	hamnoglukosida	k1gFnPc2	-rhamnoglukosida
<g/>
,	,	kIx,	,
kvercetin-	kvercetin-	k?	kvercetin-
<g/>
3	[number]	k4	3
<g/>
-O-galaktosid	-Oalaktosida	k1gFnPc2	-O-galaktosida
<g/>
,	,	kIx,	,
kvercetin-	kvercetin-	k?	kvercetin-
<g/>
7	[number]	k4	7
<g/>
-glukosid	lukosida	k1gFnPc2	-glukosida
<g/>
,	,	kIx,	,
vitaminy	vitamin	k1gInPc1	vitamin
kyselinu	kyselina	k1gFnSc4	kyselina
askorbovou	askorbový	k2eAgFnSc4d1	askorbová
<g/>
,	,	kIx,	,
niacin	niacin	k1gInSc4	niacin
a	a	k8xC	a
thiamin	thiamin	k1gInSc4	thiamin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Lidové	lidový	k2eAgFnPc4d1	lidová
a	a	k8xC	a
tradiční	tradiční	k2eAgNnSc1d1	tradiční
léčitelství	léčitelství	k1gNnSc1	léčitelství
===	===	k?	===
</s>
</p>
<p>
<s>
Heřmánek	heřmánek	k1gInSc1	heřmánek
pravý	pravý	k2eAgInSc1d1	pravý
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
léčivá	léčivý	k2eAgFnSc1d1	léčivá
rostlina	rostlina	k1gFnSc1	rostlina
užíván	užíván	k2eAgInSc1d1	užíván
již	již	k6eAd1	již
po	po	k7c6	po
tisíciletí	tisíciletí	k1gNnSc6	tisíciletí
<g/>
;	;	kIx,	;
byl	být	k5eAaImAgInS	být
znám	znám	k2eAgInSc1d1	znám
již	již	k6eAd1	již
ve	v	k7c6	v
starém	starý	k2eAgInSc6d1	starý
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
v	v	k7c6	v
antickém	antický	k2eAgNnSc6d1	antické
Řecku	Řecko	k1gNnSc6	Řecko
i	i	k8xC	i
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Anglosasů	Anglosas	k1gMnPc2	Anglosas
byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
devíti	devět	k4xCc2	devět
posvátných	posvátný	k2eAgFnPc2d1	posvátná
bylin	bylina	k1gFnPc2	bylina
darovaných	darovaný	k2eAgFnPc2d1	darovaná
člověku	člověk	k1gMnSc6	člověk
od	od	k7c2	od
Boha	bůh	k1gMnSc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Sbírány	sbírán	k2eAgInPc1d1	sbírán
jsou	být	k5eAaImIp3nP	být
rozkvetlé	rozkvetlý	k2eAgInPc1d1	rozkvetlý
úbory	úbor	k1gInPc1	úbor
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
rozvití	rozvití	k1gNnSc6	rozvití
(	(	kIx(	(
<g/>
Flos	Flos	k1gInSc1	Flos
chamomillae	chamomilla	k1gInSc2	chamomilla
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
suší	sušit	k5eAaImIp3nP	sušit
a	a	k8xC	a
užívají	užívat	k5eAaImIp3nP	užívat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
čajů	čaj	k1gInPc2	čaj
a	a	k8xC	a
odvarů	odvar	k1gInPc2	odvar
<g/>
,	,	kIx,	,
přidávají	přidávat	k5eAaImIp3nP	přidávat
se	se	k3xPyFc4	se
do	do	k7c2	do
ozdravných	ozdravný	k2eAgFnPc2d1	ozdravná
koupelí	koupel	k1gFnPc2	koupel
a	a	k8xC	a
do	do	k7c2	do
polštářů	polštář	k1gInPc2	polštář
pro	pro	k7c4	pro
lepší	dobrý	k2eAgInSc4d2	lepší
spánek	spánek	k1gInSc4	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
výpary	výpar	k1gInPc1	výpar
lze	lze	k6eAd1	lze
též	též	k9	též
inhalovat	inhalovat	k5eAaImF	inhalovat
<g/>
.	.	kIx.	.
</s>
<s>
Zevně	zevně	k6eAd1	zevně
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
heřmánkový	heřmánkový	k2eAgInSc1d1	heřmánkový
olej	olej	k1gInSc1	olej
a	a	k8xC	a
nejrůznější	různý	k2eAgFnPc1d3	nejrůznější
masti	mast	k1gFnPc1	mast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Heřmánková	heřmánkový	k2eAgFnSc1d1	heřmánková
droga	droga	k1gFnSc1	droga
má	mít	k5eAaImIp3nS	mít
celou	celý	k2eAgFnSc4d1	celá
škálu	škála	k1gFnSc4	škála
léčivých	léčivý	k2eAgInPc2d1	léčivý
účinků	účinek	k1gInPc2	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
jako	jako	k8xS	jako
antiflogistikum	antiflogistikum	k1gNnSc4	antiflogistikum
k	k	k7c3	k
léčbě	léčba	k1gFnSc3	léčba
zánětů	zánět	k1gInPc2	zánět
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
očních	oční	k2eAgFnPc2d1	oční
spojivek	spojivka	k1gFnPc2	spojivka
nebo	nebo	k8xC	nebo
pohlavního	pohlavní	k2eAgNnSc2d1	pohlavní
ústrojí	ústrojí	k1gNnSc2	ústrojí
<g/>
,	,	kIx,	,
při	při	k7c6	při
léčbě	léčba	k1gFnSc6	léčba
infikovaných	infikovaný	k2eAgFnPc2d1	infikovaná
sliznic	sliznice	k1gFnPc2	sliznice
a	a	k8xC	a
alergických	alergický	k2eAgFnPc2d1	alergická
reakcí	reakce	k1gFnPc2	reakce
na	na	k7c4	na
kůži	kůže	k1gFnSc4	kůže
<g/>
;	;	kIx,	;
jako	jako	k8xS	jako
karminativum	karminativum	k1gNnSc1	karminativum
při	při	k7c6	při
žaludečních	žaludeční	k2eAgFnPc6d1	žaludeční
a	a	k8xC	a
střevních	střevní	k2eAgFnPc6d1	střevní
potížích	potíž	k1gFnPc6	potíž
<g/>
,	,	kIx,	,
žaludečních	žaludeční	k2eAgInPc6d1	žaludeční
a	a	k8xC	a
dvanáctníkových	dvanáctníkový	k2eAgInPc6d1	dvanáctníkový
vředech	vřed	k1gInPc6	vřed
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
též	též	k9	též
proti	proti	k7c3	proti
nadýmání	nadýmání	k1gNnSc3	nadýmání
<g/>
.	.	kIx.	.
</s>
<s>
Coby	Coby	k?	Coby
spasmolytikum	spasmolytikum	k1gNnSc1	spasmolytikum
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
křeče	křeč	k1gFnPc4	křeč
a	a	k8xC	a
tiší	tišit	k5eAaImIp3nP	tišit
bolesti	bolest	k1gFnPc1	bolest
<g/>
.	.	kIx.	.
</s>
<s>
Užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
při	při	k7c6	při
bronchiálním	bronchiální	k2eAgNnSc6d1	bronchiální
astmatu	astma	k1gNnSc6	astma
a	a	k8xC	a
nachlazení	nachlazení	k1gNnSc6	nachlazení
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
diaforetikum	diaforetikum	k1gNnSc1	diaforetikum
(	(	kIx(	(
<g/>
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
pocení	pocení	k1gNnSc1	pocení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
při	při	k7c6	při
hojení	hojení	k1gNnSc6	hojení
ran	rána	k1gFnPc2	rána
<g/>
,	,	kIx,	,
popálenin	popálenina	k1gFnPc2	popálenina
a	a	k8xC	a
omrzlin	omrzlina	k1gFnPc2	omrzlina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
účinný	účinný	k2eAgInSc1d1	účinný
proti	proti	k7c3	proti
některým	některý	k3yIgInPc3	některý
stafylokokům	stafylokok	k1gInPc3	stafylokok
a	a	k8xC	a
streptokokům	streptokok	k1gInPc3	streptokok
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
mikrobům	mikrob	k1gInPc3	mikrob
Bacillus	Bacillus	k1gMnSc1	Bacillus
subtillis	subtillis	k1gFnSc1	subtillis
<g/>
,	,	kIx,	,
Escherichia	Escherichia	k1gFnSc1	Escherichia
coli	col	k1gFnSc2	col
nebo	nebo	k8xC	nebo
proti	proti	k7c3	proti
infekcím	infekce	k1gFnPc3	infekce
způsobeným	způsobený	k2eAgFnPc3d1	způsobená
kvasinkou	kvasinka	k1gFnSc7	kvasinka
Candida	Candida	k1gFnSc1	Candida
albicans	albicans	k6eAd1	albicans
<g/>
.	.	kIx.	.
</s>
<s>
Užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
též	též	k9	též
na	na	k7c4	na
poruchy	porucha	k1gFnPc4	porucha
menstruace	menstruace	k1gFnSc2	menstruace
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
hemoroidům	hemoroidy	k1gInPc3	hemoroidy
<g/>
,	,	kIx,	,
k	k	k7c3	k
umývání	umývání	k1gNnSc3	umývání
vlasů	vlas	k1gInPc2	vlas
<g/>
,	,	kIx,	,
na	na	k7c4	na
obklady	obklad	k1gInPc4	obklad
a	a	k8xC	a
koupele	koupel	k1gFnSc2	koupel
k	k	k7c3	k
celkové	celkový	k2eAgFnSc3d1	celková
harmonizaci	harmonizace	k1gFnSc3	harmonizace
a	a	k8xC	a
regeneraci	regenerace	k1gFnSc3	regenerace
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Snižuje	snižovat	k5eAaImIp3nS	snižovat
úzkost	úzkost	k1gFnSc4	úzkost
<g/>
.	.	kIx.	.
<g/>
Při	při	k7c6	při
dlouhodobém	dlouhodobý	k2eAgNnSc6d1	dlouhodobé
užívání	užívání	k1gNnSc6	užívání
může	moct	k5eAaImIp3nS	moct
vysušovat	vysušovat	k5eAaImF	vysušovat
sliznice	sliznice	k1gFnSc1	sliznice
<g/>
,	,	kIx,	,
předávkování	předávkování	k1gNnSc1	předávkování
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
nauseu	nausea	k1gFnSc4	nausea
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
citlivějších	citlivý	k2eAgFnPc2d2	citlivější
osob	osoba	k1gFnPc2	osoba
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
alergickou	alergický	k2eAgFnSc4d1	alergická
reakci	reakce	k1gFnSc4	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vhodný	vhodný	k2eAgInSc1d1	vhodný
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
už	už	k9	už
od	od	k7c2	od
kojeneckého	kojenecký	k2eAgInSc2d1	kojenecký
věku	věk	k1gInSc2	věk
<g/>
;	;	kIx,	;
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
při	při	k7c6	při
dětské	dětský	k2eAgFnSc6d1	dětská
kolice	kolika	k1gFnSc6	kolika
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
jej	on	k3xPp3gMnSc4	on
užívat	užívat	k5eAaImF	užívat
i	i	k9	i
těhotné	těhotný	k2eAgFnPc1d1	těhotná
a	a	k8xC	a
kojící	kojící	k2eAgFnPc1d1	kojící
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Farmaceutický	farmaceutický	k2eAgInSc4d1	farmaceutický
a	a	k8xC	a
kosmetický	kosmetický	k2eAgInSc4d1	kosmetický
průmysl	průmysl	k1gInSc4	průmysl
===	===	k?	===
</s>
</p>
<p>
<s>
Destilované	destilovaný	k2eAgFnPc1d1	destilovaná
heřmánkové	heřmánkový	k2eAgFnPc1d1	heřmánková
silice	silice	k1gFnPc1	silice
a	a	k8xC	a
esenciální	esenciální	k2eAgInPc1d1	esenciální
oleje	olej	k1gInPc1	olej
se	se	k3xPyFc4	se
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
míře	míra	k1gFnSc6	míra
využívají	využívat	k5eAaImIp3nP	využívat
i	i	k9	i
ve	v	k7c6	v
farmaceutickém	farmaceutický	k2eAgInSc6d1	farmaceutický
a	a	k8xC	a
kosmetickém	kosmetický	k2eAgInSc6d1	kosmetický
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
statistik	statistika	k1gFnPc2	statistika
je	být	k5eAaImIp3nS	být
heřmánek	heřmánek	k1gInSc1	heřmánek
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
jeho	jeho	k3xOp3gInPc4	jeho
sušené	sušený	k2eAgInPc4d1	sušený
květy	květ	k1gInPc4	květ
a	a	k8xC	a
modré	modrý	k2eAgInPc4d1	modrý
oleje	olej	k1gInPc4	olej
<g/>
)	)	kIx)	)
pátou	pátý	k4xOgFnSc7	pátý
nejobchodovanější	obchodovaný	k2eAgFnSc7d3	nejobchodovanější
bylinou	bylina	k1gFnSc7	bylina
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
;	;	kIx,	;
k	k	k7c3	k
největším	veliký	k2eAgMnPc3d3	veliký
světovým	světový	k2eAgMnPc3d1	světový
producentům	producent	k1gMnPc3	producent
patří	patřit	k5eAaImIp3nP	patřit
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Argentina	Argentina	k1gFnSc1	Argentina
a	a	k8xC	a
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
rozpadu	rozpad	k1gInSc2	rozpad
též	též	k9	též
Československo	Československo	k1gNnSc1	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Přidává	přidávat	k5eAaImIp3nS	přidávat
se	se	k3xPyFc4	se
do	do	k7c2	do
šampónů	šampónů	k?	šampónů
<g/>
,	,	kIx,	,
parfémů	parfém	k1gInPc2	parfém
<g/>
,	,	kIx,	,
kosmetických	kosmetický	k2eAgInPc2d1	kosmetický
krémů	krém	k1gInPc2	krém
<g/>
,	,	kIx,	,
mýdel	mýdlo	k1gNnPc2	mýdlo
<g/>
,	,	kIx,	,
toaletních	toaletní	k2eAgFnPc2d1	toaletní
vod	voda	k1gFnPc2	voda
<g/>
,	,	kIx,	,
zubních	zubní	k2eAgFnPc2d1	zubní
past	pasta	k1gFnPc2	pasta
<g/>
,	,	kIx,	,
masážních	masážní	k2eAgInPc2d1	masážní
olejů	olej	k1gInPc2	olej
a	a	k8xC	a
mnoha	mnoho	k4c2	mnoho
farmaceutických	farmaceutický	k2eAgInPc2d1	farmaceutický
přípravků	přípravek	k1gInPc2	přípravek
včetně	včetně	k7c2	včetně
likérů	likér	k1gInPc2	likér
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Fytoterapie	fytoterapie	k1gFnSc1	fytoterapie
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
heřmánek	heřmánek	k1gInSc1	heřmánek
pravý	pravý	k2eAgInSc1d1	pravý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Matricaria	Matricarium	k1gNnSc2	Matricarium
chamomilla	chamomilla	k6eAd1	chamomilla
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Botanika	botanika	k1gFnSc1	botanika
Wendys	Wendysa	k1gFnPc2	Wendysa
</s>
</p>
