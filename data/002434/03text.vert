<s>
Oldřich	Oldřich	k1gMnSc1	Oldřich
Prefát	Prefát	k1gMnSc1	Prefát
z	z	k7c2	z
Vlkanova	Vlkanův	k2eAgInSc2d1	Vlkanův
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1523	[number]	k4	1523
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1565	[number]	k4	1565
tamtéž	tamtéž	k6eAd1	tamtéž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
<g/>
,	,	kIx,	,
astronom	astronom	k1gMnSc1	astronom
a	a	k8xC	a
cestovatel	cestovatel	k1gMnSc1	cestovatel
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
bohaté	bohatý	k2eAgFnSc2d1	bohatá
měšťanské	měšťanský	k2eAgFnSc2d1	měšťanská
erbovní	erbovní	k2eAgFnSc2d1	erbovní
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
ve	v	k7c6	v
Wittenbergu	Wittenberg	k1gInSc6	Wittenberg
<g/>
,	,	kIx,	,
Lipsku	Lipsko	k1gNnSc6	Lipsko
<g/>
,	,	kIx,	,
Benátkách	Benátky	k1gFnPc6	Benátky
<g/>
,	,	kIx,	,
Římě	Řím	k1gInSc6	Řím
a	a	k8xC	a
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1546	[number]	k4	1546
<g/>
-	-	kIx~	-
<g/>
1547	[number]	k4	1547
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Palestiny	Palestina	k1gFnSc2	Palestina
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1552	[number]	k4	1552
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Španělsko	Španělsko	k1gNnSc4	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Uhelném	uhelný	k2eAgInSc6d1	uhelný
trhu	trh	k1gInSc6	trh
zdědil	zdědit	k5eAaPmAgInS	zdědit
dům	dům	k1gInSc1	dům
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
zde	zde	k6eAd1	zde
dílnu	dílna	k1gFnSc4	dílna
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
měřicích	měřicí	k2eAgInPc2d1	měřicí
přístrojů	přístroj	k1gInPc2	přístroj
a	a	k8xC	a
astronomických	astronomický	k2eAgFnPc2d1	astronomická
a	a	k8xC	a
matematických	matematický	k2eAgFnPc2d1	matematická
pomůcek	pomůcka	k1gFnPc2	pomůcka
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
do	do	k7c2	do
Benátek	Benátky	k1gFnPc2	Benátky
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
potom	potom	k6eAd1	potom
po	po	k7c6	po
moři	moře	k1gNnSc6	moře
až	až	k9	až
do	do	k7c2	do
Palestyny	Palestyna	k1gFnSc2	Palestyna
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jest	být	k5eAaImIp3nS	být
do	do	k7c2	do
krajiny	krajina	k1gFnSc2	krajina
někdy	někdy	k6eAd1	někdy
Židovské	židovský	k2eAgFnSc2d1	židovská
<g/>
,	,	kIx,	,
země	zem	k1gFnSc2	zem
Svaté	svatá	k1gFnSc2	svatá
<g/>
,	,	kIx,	,
do	do	k7c2	do
města	město	k1gNnSc2	město
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
k	k	k7c3	k
Božímu	boží	k2eAgInSc3d1	boží
hrobu	hrob	k1gInSc3	hrob
<g/>
,	,	kIx,	,
kteraužto	kteraužto	k1gNnSc1	kteraužto
cestu	cesta	k1gFnSc4	cesta
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Pána	pán	k1gMnSc2	pán
Boha	bůh	k1gMnSc2	bůh
všemohúcího	všemohúcí	k1gMnSc2	všemohúcí
šťastně	šťastně	k6eAd1	šťastně
vykonal	vykonat	k5eAaPmAgMnS	vykonat
Voldřich	Voldřich	k1gMnSc1	Voldřich
Prefát	Prefát	k1gMnSc1	Prefát
z	z	k7c2	z
Vlkanova	Vlkanův	k2eAgNnSc2d1	Vlkanův
léta	léto	k1gNnSc2	léto
Páně	páně	k2eAgInPc2d1	páně
MDXXXXVI	MDXXXXVI	kA	MDXXXXVI
<g/>
.	.	kIx.	.
-	-	kIx~	-
Často	často	k6eAd1	často
bývá	bývat	k5eAaImIp3nS	bývat
používán	používán	k2eAgInSc1d1	používán
název	název	k1gInSc1	název
<g/>
:	:	kIx,	:
Cesta	cesta	k1gFnSc1	cesta
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
do	do	k7c2	do
Benátek	Benátky	k1gFnPc2	Benátky
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
potom	potom	k6eAd1	potom
po	po	k7c6	po
moři	moře	k1gNnSc6	moře
až	až	k9	až
do	do	k7c2	do
Palestiny	Palestina	k1gFnSc2	Palestina
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
cestopis	cestopis	k1gInSc1	cestopis
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
přesný	přesný	k2eAgInSc1d1	přesný
a	a	k8xC	a
popisný	popisný	k2eAgInSc1d1	popisný
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
resp.	resp.	kA	resp.
nejpřesnější	přesný	k2eAgInPc1d3	nejpřesnější
cestopisy	cestopis	k1gInPc1	cestopis
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
modro-červeně	modro-červeně	k6eAd1	modro-červeně
polceném	polcený	k2eAgInSc6d1	polcený
štítě	štít	k1gInSc6	štít
je	být	k5eAaImIp3nS	být
stříbrný	stříbrný	k2eAgMnSc1d1	stříbrný
vlk	vlk	k1gMnSc1	vlk
(	(	kIx(	(
<g/>
mluvící	mluvící	k2eAgNnSc1d1	mluvící
znamení	znamení	k1gNnSc1	znamení
<g/>
)	)	kIx)	)
se	s	k7c7	s
zlatým	zlatý	k2eAgInSc7d1	zlatý
obojkem	obojek	k1gInSc7	obojek
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
štítem	štít	k1gInSc7	štít
kolčí	kolčí	k2eAgFnSc1d1	kolčí
přilba	přilba	k1gFnSc1	přilba
s	s	k7c7	s
modro-stříbrno-červenou	modrotříbrno-červený	k2eAgFnSc7d1	modro-stříbrno-červený
točenicí	točenice	k1gFnSc7	točenice
<g/>
.	.	kIx.	.
</s>
<s>
Pokryvadla	Pokryvadla	k?	Pokryvadla
jsou	být	k5eAaImIp3nP	být
modro-stříbrná	modrotříbrný	k2eAgNnPc1d1	modro-stříbrný
a	a	k8xC	a
červeno-stříbrná	červenotříbrný	k2eAgNnPc1d1	červeno-stříbrný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klenotu	klenot	k1gInSc6	klenot
polovina	polovina	k1gFnSc1	polovina
stříbrného	stříbrný	k1gInSc2	stříbrný
vlka	vlk	k1gMnSc2	vlk
se	s	k7c7	s
zlatým	zlatý	k2eAgInSc7d1	zlatý
obojkem	obojek	k1gInSc7	obojek
mezi	mezi	k7c7	mezi
modrým	modrý	k2eAgNnSc7d1	modré
a	a	k8xC	a
červeným	červený	k2eAgNnSc7d1	červené
křídlem	křídlo	k1gNnSc7	křídlo
<g/>
.	.	kIx.	.
</s>
