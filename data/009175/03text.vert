<p>
<s>
Voluta	voluta	k1gFnSc1	voluta
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
volutus	volutus	k1gInSc1	volutus
=	=	kIx~	=
svinutý	svinutý	k2eAgMnSc1d1	svinutý
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
spirálovitý	spirálovitý	k2eAgInSc4d1	spirálovitý
motiv	motiv	k1gInSc4	motiv
různých	různý	k2eAgFnPc2d1	různá
soustav	soustava	k1gFnPc2	soustava
výzdoby	výzdoba	k1gFnSc2	výzdoba
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
konstrukčně	konstrukčně	k6eAd1	konstrukčně
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
Archimédovy	Archimédův	k2eAgFnSc2d1	Archimédova
spirály	spirála	k1gFnSc2	spirála
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
motiv	motiv	k1gInSc1	motiv
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
architektuře	architektura	k1gFnSc6	architektura
využíván	využíván	k2eAgInSc4d1	využíván
od	od	k7c2	od
pravěku	pravěk	k1gInSc2	pravěk
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
různých	různý	k2eAgFnPc6d1	různá
formách	forma	k1gFnPc6	forma
<g/>
,	,	kIx,	,
polohách	poloha	k1gFnPc6	poloha
a	a	k8xC	a
způsobech	způsob	k1gInPc6	způsob
umístění	umístění	k1gNnSc2	umístění
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
byl	být	k5eAaImAgInS	být
využíván	využívat	k5eAaImNgInS	využívat
v	v	k7c6	v
sestavě	sestava	k1gFnSc6	sestava
hlavic	hlavice	k1gFnPc2	hlavice
<g/>
,	,	kIx,	,
palmet	palmeta	k1gFnPc2	palmeta
a	a	k8xC	a
ant	ant	k?	ant
v	v	k7c6	v
iónském	iónský	k2eAgInSc6d1	iónský
sloupovém	sloupový	k2eAgInSc6d1	sloupový
řádu	řád	k1gInSc6	řád
a	a	k8xC	a
korintském	korintský	k2eAgInSc6d1	korintský
sloupovém	sloupový	k2eAgInSc6d1	sloupový
řádu	řád	k1gInSc6	řád
a	a	k8xC	a
v	v	k7c6	v
sestavě	sestava	k1gFnSc6	sestava
volutového	volutový	k2eAgInSc2d1	volutový
štítu	štít	k1gInSc2	štít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
volut	voluta	k1gFnPc2	voluta
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
slozích	sloh	k1gInPc6	sloh
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Iónský	iónský	k2eAgInSc1d1	iónský
sloupový	sloupový	k2eAgInSc1d1	sloupový
řád	řád	k1gInSc1	řád
(	(	kIx(	(
<g/>
antika	antika	k1gFnSc1	antika
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Voluta	voluta	k1gFnSc1	voluta
je	být	k5eAaImIp3nS	být
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
prvkem	prvek	k1gInSc7	prvek
hlavic	hlavice	k1gFnPc2	hlavice
sloupů	sloup	k1gInPc2	sloup
tohoto	tento	k3xDgInSc2	tento
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Korintský	korintský	k2eAgInSc1d1	korintský
sloupový	sloupový	k2eAgInSc1d1	sloupový
řád	řád	k1gInSc1	řád
(	(	kIx(	(
<g/>
antika	antika	k1gFnSc1	antika
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Voluty	voluta	k1gFnPc1	voluta
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
složitých	složitý	k2eAgFnPc2d1	složitá
hlavic	hlavice	k1gFnPc2	hlavice
sloupu	sloup	k1gInSc2	sloup
tohoto	tento	k3xDgInSc2	tento
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Voluty	voluta	k1gFnPc1	voluta
tvoří	tvořit	k5eAaImIp3nP	tvořit
na	na	k7c6	na
těchto	tento	k3xDgFnPc6	tento
hlavicích	hlavice	k1gFnPc6	hlavice
především	především	k6eAd1	především
rohové	rohový	k2eAgInPc1d1	rohový
články	článek	k1gInPc1	článek
<g/>
,	,	kIx,	,
svinující	svinující	k2eAgMnSc1d1	svinující
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
deskou	deska	k1gFnSc7	deska
abaku	abak	k1gInSc2	abak
<g/>
,	,	kIx,	,
k	k	k7c3	k
těmto	tento	k3xDgFnPc3	tento
čtyřem	čtyři	k4xCgFnPc3	čtyři
rohovým	rohový	k2eAgFnPc3d1	rohová
volutám	voluta	k1gFnPc3	voluta
přistupují	přistupovat	k5eAaImIp3nP	přistupovat
často	často	k6eAd1	často
další	další	k2eAgFnPc1d1	další
vřazené	vřazený	k2eAgFnPc1d1	vřazená
voluty	voluta	k1gFnPc1	voluta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Volutový	volutový	k2eAgInSc1d1	volutový
ornament	ornament	k1gInSc1	ornament
(	(	kIx(	(
<g/>
pozdní	pozdní	k2eAgFnSc1d1	pozdní
renesance	renesance	k1gFnSc1	renesance
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
vrcholné	vrcholný	k2eAgFnSc6d1	vrcholná
renesanci	renesance	k1gFnSc6	renesance
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1570	[number]	k4	1570
<g/>
)	)	kIx)	)
v	v	k7c6	v
nizozemském	nizozemský	k2eAgInSc6d1	nizozemský
a	a	k8xC	a
německém	německý	k2eAgInSc6d1	německý
manýrismu	manýrismus	k1gInSc6	manýrismus
(	(	kIx(	(
<g/>
Florisův	Florisův	k2eAgInSc4d1	Florisův
styl	styl	k1gInSc4	styl
<g/>
)	)	kIx)	)
na	na	k7c6	na
fasádách	fasáda	k1gFnPc6	fasáda
radnic	radnice	k1gFnPc2	radnice
a	a	k8xC	a
měšťanských	měšťanský	k2eAgInPc2d1	měšťanský
domů	dům	k1gInPc2	dům
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
řeky	řeka	k1gFnSc2	řeka
Vezery	Vezera	k1gFnSc2	Vezera
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
vezerská	vezerský	k2eAgFnSc1d1	vezerský
renesance	renesance	k1gFnSc1	renesance
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Volutový	volutový	k2eAgInSc1d1	volutový
štít	štít	k1gInSc1	štít
(	(	kIx(	(
<g/>
renesance	renesance	k1gFnSc1	renesance
<g/>
,	,	kIx,	,
baroko	baroko	k1gNnSc1	baroko
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Dynamický	dynamický	k2eAgInSc4d1	dynamický
domovní	domovní	k2eAgInSc4d1	domovní
štít	štít	k1gInSc4	štít
<g/>
,	,	kIx,	,
typický	typický	k2eAgInSc4d1	typický
pro	pro	k7c4	pro
období	období	k1gNnSc4	období
baroka	baroko	k1gNnSc2	baroko
a	a	k8xC	a
rokoka	rokoko	k1gNnSc2	rokoko
<g/>
.	.	kIx.	.
</s>
<s>
Voluty	voluta	k1gFnPc1	voluta
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
využívaly	využívat	k5eAaImAgFnP	využívat
k	k	k7c3	k
vyřešení	vyřešení	k1gNnSc3	vyřešení
přechodu	přechod	k1gInSc2	přechod
mezi	mezi	k7c7	mezi
svislými	svislý	k2eAgFnPc7d1	svislá
a	a	k8xC	a
vodorovnými	vodorovný	k2eAgFnPc7d1	vodorovná
částmi	část	k1gFnPc7	část
stavby	stavba	k1gFnSc2	stavba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Voluty	voluta	k1gFnPc1	voluta
ve	v	k7c6	v
světové	světový	k2eAgFnSc6d1	světová
architektuře	architektura	k1gFnSc6	architektura
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Voluty	voluta	k1gFnPc1	voluta
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
architektuře	architektura	k1gFnSc6	architektura
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
