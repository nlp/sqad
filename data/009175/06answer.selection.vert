<s>
Voluta	voluta	k1gFnSc1	voluta
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
volutus	volutus	k1gInSc1	volutus
=	=	kIx~	=
svinutý	svinutý	k2eAgMnSc1d1	svinutý
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
spirálovitý	spirálovitý	k2eAgInSc4d1	spirálovitý
motiv	motiv	k1gInSc4	motiv
různých	různý	k2eAgFnPc2d1	různá
soustav	soustava	k1gFnPc2	soustava
výzdoby	výzdoba	k1gFnSc2	výzdoba
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
konstrukčně	konstrukčně	k6eAd1	konstrukčně
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
Archimédovy	Archimédův	k2eAgFnSc2d1	Archimédova
spirály	spirála	k1gFnSc2	spirála
<g/>
.	.	kIx.	.
</s>
