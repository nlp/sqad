<s>
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
(	(	kIx(	(
<g/>
slovinsky	slovinsky	k6eAd1	slovinsky
Slovenija	Slovenijus	k1gMnSc2	Slovenijus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Republika	republika	k1gFnSc1	republika
Slovinsko	Slovinsko	k1gNnSc4	Slovinsko
(	(	kIx(	(
<g/>
slovinsky	slovinsky	k6eAd1	slovinsky
Republika	republika	k1gFnSc1	republika
Slovenija	Slovenija	k1gFnSc1	Slovenija
<g/>
;	;	kIx,	;
výslovnost	výslovnost	k1gFnSc1	výslovnost
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
reˈ	reˈ	k?	reˈ
sloˈ	sloˈ	k?	sloˈ
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
středoevropský	středoevropský	k2eAgInSc4d1	středoevropský
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Samostatné	samostatný	k2eAgNnSc1d1	samostatné
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
roku	rok	k1gInSc3	rok
1991	[number]	k4	1991
jako	jako	k9	jako
první	první	k4xOgInPc4	první
z	z	k7c2	z
nástupnických	nástupnický	k2eAgInPc2d1	nástupnický
států	stát	k1gInPc2	stát
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
.	.	kIx.	.
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
OBSE	OBSE	kA	OBSE
(	(	kIx(	(
<g/>
od	od	k7c2	od
24	[number]	k4	24
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
OSN	OSN	kA	OSN
(	(	kIx(	(
<g/>
od	od	k7c2	od
22	[number]	k4	22
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
WTO	WTO	kA	WTO
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
od	od	k7c2	od
30	[number]	k4	30
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
EEA	EEA	kA	EEA
<g/>
,	,	kIx,	,
NATO	NATO	kA	NATO
(	(	kIx(	(
<g/>
od	od	k7c2	od
29	[number]	k4	29
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
EU	EU	kA	EU
(	(	kIx(	(
<g/>
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Eurozóny	Eurozón	k1gInPc1	Eurozón
(	(	kIx(	(
<g/>
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Evropské	evropský	k2eAgFnSc2d1	Evropská
celní	celní	k2eAgFnSc2d1	celní
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
Schengenského	schengenský	k2eAgInSc2d1	schengenský
prostoru	prostor	k1gInSc2	prostor
(	(	kIx(	(
<g/>
od	od	k7c2	od
21	[number]	k4	21
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
a	a	k8xC	a
OECD	OECD	kA	OECD
(	(	kIx(	(
<g/>
od	od	k7c2	od
21	[number]	k4	21
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
sousedy	soused	k1gMnPc7	soused
jsou	být	k5eAaImIp3nP	být
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
,	,	kIx,	,
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
a	a	k8xC	a
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
z	z	k7c2	z
necelých	celý	k2eNgInPc2d1	necelý
dvou	dva	k4xCgInPc2	dva
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
tvoří	tvořit	k5eAaImIp3nP	tvořit
Slovinci	Slovinec	k1gMnPc1	Slovinec
(	(	kIx(	(
<g/>
83	[number]	k4	83
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následují	následovat	k5eAaImIp3nP	následovat
Srbové	Srb	k1gMnPc1	Srb
(	(	kIx(	(
<g/>
1,98	[number]	k4	1,98
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Chorvaté	Chorvat	k1gMnPc1	Chorvat
(	(	kIx(	(
<g/>
1,81	[number]	k4	1,81
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
území	území	k1gNnSc6	území
několika	několik	k4yIc2	několik
menších	malý	k2eAgFnPc2d2	menší
historických	historický	k2eAgFnPc2d1	historická
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc7	jeho
jádrem	jádro	k1gNnSc7	jádro
je	být	k5eAaImIp3nS	být
někdejší	někdejší	k2eAgNnSc4d1	někdejší
Kraňsko	Kraňsko	k1gNnSc4	Kraňsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
Rakousko-Uherska	Rakousko-Uhersk	k1gInSc2	Rakousko-Uhersk
se	se	k3xPyFc4	se
jihoslovanské	jihoslovanský	k2eAgFnPc1d1	Jihoslovanská
země	zem	k1gFnPc1	zem
postupně	postupně	k6eAd1	postupně
zformovaly	zformovat	k5eAaPmAgFnP	zformovat
v	v	k7c4	v
jugoslávské	jugoslávský	k2eAgNnSc4d1	jugoslávské
království	království	k1gNnSc4	království
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
mezi	mezi	k7c4	mezi
Itálii	Itálie	k1gFnSc4	Itálie
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc4	Německo
a	a	k8xC	a
Uhersko	Uhersko	k1gNnSc4	Uhersko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1943	[number]	k4	1943
se	se	k3xPyFc4	se
Antifašistická	antifašistický	k2eAgFnSc1d1	Antifašistická
rada	rada	k1gFnSc1	rada
národního	národní	k2eAgNnSc2d1	národní
osvobození	osvobození	k1gNnSc2	osvobození
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
usnesla	usnést	k5eAaPmAgFnS	usnést
o	o	k7c6	o
obnovení	obnovení	k1gNnSc6	obnovení
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
na	na	k7c6	na
federálním	federální	k2eAgInSc6d1	federální
základě	základ	k1gInSc6	základ
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
federálních	federální	k2eAgFnPc2d1	federální
jednotek	jednotka	k1gFnPc2	jednotka
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
stát	stát	k5eAaPmF	stát
i	i	k9	i
Slovinsko	Slovinsko	k1gNnSc4	Slovinsko
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
datum	datum	k1gNnSc4	datum
vzniku	vznik	k1gInSc2	vznik
Slovinské	slovinský	k2eAgFnSc2d1	slovinská
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
19	[number]	k4	19
<g/>
.	.	kIx.	.
únor	únor	k1gInSc1	únor
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Črnomelju	Črnomelju	k1gFnSc6	Črnomelju
Slovinský	slovinský	k2eAgInSc1d1	slovinský
národněosvobozenecký	národněosvobozenecký	k2eAgInSc1d1	národněosvobozenecký
výbor	výbor	k1gInSc1	výbor
přeměněn	přeměněn	k2eAgInSc1d1	přeměněn
v	v	k7c4	v
Slovinskou	slovinský	k2eAgFnSc4d1	slovinská
národněosvobozeneckou	národněosvobozenecký	k2eAgFnSc4d1	národněosvobozenecký
radu	rada	k1gFnSc4	rada
<g/>
,	,	kIx,	,
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
první	první	k4xOgMnSc1	první
slovinský	slovinský	k2eAgInSc1d1	slovinský
parlament	parlament	k1gInSc1	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislost	nezávislost	k1gFnSc1	nezávislost
na	na	k7c6	na
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
Brionské	Brionský	k2eAgFnSc2d1	Brionská
deklarace	deklarace	k1gFnSc2	deklarace
byla	být	k5eAaImAgFnS	být
účinnost	účinnost	k1gFnSc1	účinnost
aktu	akt	k1gInSc2	akt
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
pozastavena	pozastaven	k2eAgFnSc1d1	pozastavena
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1991	[number]	k4	1991
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
společné	společný	k2eAgNnSc1d1	společné
zasedání	zasedání	k1gNnSc1	zasedání
všech	všecek	k3xTgFnPc2	všecek
komor	komora	k1gFnPc2	komora
Skupščiny	Skupščin	k2eAgInPc1d1	Skupščin
úmysl	úmysl	k1gInSc4	úmysl
nabýt	nabýt	k5eAaPmF	nabýt
nezávislost	nezávislost	k1gFnSc4	nezávislost
okamžitě	okamžitě	k6eAd1	okamžitě
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
uplyne	uplynout	k5eAaPmIp3nS	uplynout
tříměsíční	tříměsíční	k2eAgNnSc1d1	tříměsíční
moratorium	moratorium	k1gNnSc1	moratorium
smluvené	smluvený	k2eAgNnSc1d1	smluvené
Brionskou	Brionský	k2eAgFnSc7d1	Brionská
deklarací	deklarace	k1gFnSc7	deklarace
<g/>
.	.	kIx.	.
</s>
<s>
Moratorium	moratorium	k1gNnSc1	moratorium
vypršelo	vypršet	k5eAaPmAgNnS	vypršet
8	[number]	k4	8
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Slovinsko	Slovinsko	k1gNnSc4	Slovinsko
definitivně	definitivně	k6eAd1	definitivně
získalo	získat	k5eAaPmAgNnS	získat
svou	svůj	k3xOyFgFnSc4	svůj
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
současného	současný	k2eAgNnSc2d1	současné
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
byla	být	k5eAaImAgNnP	být
osídlena	osídlit	k5eAaPmNgNnP	osídlit
již	již	k6eAd1	již
v	v	k7c6	v
období	období	k1gNnSc6	období
starší	starý	k2eAgFnSc2d2	starší
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
kamenné	kamenný	k2eAgInPc1d1	kamenný
a	a	k8xC	a
kostěné	kostěný	k2eAgInPc1d1	kostěný
nálezy	nález	k1gInPc1	nález
<g/>
.	.	kIx.	.
</s>
<s>
Řecké	řecký	k2eAgInPc1d1	řecký
a	a	k8xC	a
římské	římský	k2eAgInPc1d1	římský
prameny	pramen	k1gInPc1	pramen
zmiňují	zmiňovat	k5eAaImIp3nP	zmiňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
období	období	k1gNnSc6	období
kolem	kolem	k7c2	kolem
2	[number]	k4	2
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byla	být	k5eAaImAgFnS	být
oblast	oblast	k1gFnSc1	oblast
osídlena	osídlit	k5eAaPmNgFnS	osídlit
Ilyry	Ilyr	k1gMnPc7	Ilyr
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
od	od	k7c2	od
názvů	název	k1gInPc2	název
některých	některý	k3yIgInPc2	některý
ilyrských	ilyrský	k2eAgInPc2d1	ilyrský
kmenů	kmen	k1gInPc2	kmen
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
pojmenování	pojmenování	k1gNnSc4	pojmenování
některých	některý	k3yIgFnPc2	některý
jadranských	jadranský	k2eAgFnPc2d1	Jadranská
oblastí	oblast	k1gFnPc2	oblast
-	-	kIx~	-
Dalmácie	Dalmácie	k1gFnSc1	Dalmácie
(	(	kIx(	(
<g/>
kmen	kmen	k1gInSc1	kmen
Dalmátů	Dalmát	k1gMnPc2	Dalmát
<g/>
)	)	kIx)	)
či	či	k8xC	či
Istrie	Istrie	k1gFnSc1	Istrie
(	(	kIx(	(
<g/>
kmen	kmen	k1gInSc1	kmen
Histri	Histr	k1gFnSc2	Histr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1000	[number]	k4	1000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
založili	založit	k5eAaPmAgMnP	založit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
svých	svůj	k3xOyFgFnPc2	svůj
kolonizačních	kolonizační	k2eAgFnPc2d1	kolonizační
aktivit	aktivita	k1gFnPc2	aktivita
osady	osada	k1gFnSc2	osada
Řekové	Řek	k1gMnPc1	Řek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
železné	železný	k2eAgFnSc6d1	železná
osídlili	osídlit	k5eAaPmAgMnP	osídlit
oblast	oblast	k1gFnSc4	oblast
Keltové	Kelt	k1gMnPc1	Kelt
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
keltský	keltský	k2eAgInSc1d1	keltský
kmen	kmen	k1gInSc1	kmen
-	-	kIx~	-
Norikové	Norikové	k?	Norikové
-	-	kIx~	-
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
současných	současný	k2eAgInPc2d1	současný
rakouských	rakouský	k2eAgInPc2d1	rakouský
Korutan	Korutany	k1gInPc2	Korutany
první	první	k4xOgMnSc1	první
státní	státní	k2eAgInSc1d1	státní
útvar	útvar	k1gInSc1	útvar
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
pozdější	pozdní	k2eAgNnSc1d2	pozdější
římské	římský	k2eAgInPc1d1	římský
prameny	pramen	k1gInPc1	pramen
označují	označovat	k5eAaImIp3nP	označovat
za	za	k7c4	za
Norické	norický	k2eAgNnSc4d1	norický
království	království	k1gNnSc4	království
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
království	království	k1gNnSc2	království
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
přibližně	přibližně	k6eAd1	přibližně
7	[number]	k4	7
kilometrů	kilometr	k1gInPc2	kilometr
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
dnešního	dnešní	k2eAgInSc2d1	dnešní
Klagenfurtu	Klagenfurt	k1gInSc2	Klagenfurt
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
druhého	druhý	k4xOgNnSc2	druhý
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
mělo	mít	k5eAaImAgNnS	mít
království	království	k1gNnSc1	království
obchodní	obchodní	k2eAgInPc1d1	obchodní
styky	styk	k1gInPc1	styk
s	s	k7c7	s
Římem	Řím	k1gInSc7	Řím
<g/>
,	,	kIx,	,
s	s	k7c7	s
čímž	což	k3yRnSc7	což
souvisel	souviset	k5eAaImAgMnS	souviset
vznik	vznik	k1gInSc4	vznik
římských	římský	k2eAgNnPc2d1	římské
měst	město	k1gNnPc2	město
-	-	kIx~	-
Emony	Emona	k1gFnPc1	Emona
<g/>
,	,	kIx,	,
Poetovie	Poetovie	k1gFnSc1	Poetovie
<g/>
,	,	kIx,	,
Nevioduna	Nevioduna	k1gFnSc1	Nevioduna
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
se	se	k3xPyFc4	se
Norického	norický	k2eAgNnSc2d1	norický
království	království	k1gNnSc2	království
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
v	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
15	[number]	k4	15
až	až	k6eAd1	až
10	[number]	k4	10
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
také	také	k9	také
ustavena	ustaven	k2eAgFnSc1d1	ustavena
římská	římský	k2eAgFnSc1d1	římská
provincie	provincie	k1gFnSc1	provincie
Noricum	Noricum	k1gNnSc4	Noricum
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
čtvrtého	čtvrtý	k4xOgNnSc2	čtvrtý
století	století	k1gNnSc2	století
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
byla	být	k5eAaImAgFnS	být
římská	římský	k2eAgFnSc1d1	římská
přítomnost	přítomnost	k1gFnSc1	přítomnost
ohrožována	ohrožovat	k5eAaImNgFnS	ohrožovat
celou	celý	k2eAgFnSc7d1	celá
řadou	řada	k1gFnSc7	řada
barbarských	barbarský	k2eAgInPc2d1	barbarský
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
v	v	k7c6	v
roce	rok	k1gInSc6	rok
379	[number]	k4	379
vyplenily	vyplenit	k5eAaPmAgInP	vyplenit
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
římských	římský	k2eAgNnPc2d1	římské
měst	město	k1gNnPc2	město
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
-	-	kIx~	-
Poetovii	Poetovie	k1gFnSc6	Poetovie
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc1d1	dnešní
Ptuj	Ptuj	k1gInSc1	Ptuj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pátém	pátý	k4xOgNnSc6	pátý
století	století	k1gNnSc6	století
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
panonské	panonský	k2eAgFnSc2d1	Panonská
nížiny	nížina	k1gFnSc2	nížina
protáhli	protáhnout	k5eAaPmAgMnP	protáhnout
Hunové	Hun	k1gMnPc1	Hun
<g/>
,	,	kIx,	,
stejnou	stejný	k2eAgFnSc7d1	stejná
cestou	cesta	k1gFnSc7	cesta
poté	poté	k6eAd1	poté
prošli	projít	k5eAaPmAgMnP	projít
i	i	k9	i
Ostrogóti	Ostrogót	k1gMnPc1	Ostrogót
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
šestého	šestý	k4xOgNnSc2	šestý
století	století	k1gNnSc2	století
přišel	přijít	k5eAaPmAgInS	přijít
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
germánský	germánský	k2eAgInSc4d1	germánský
kmen	kmen	k1gInSc4	kmen
z	z	k7c2	z
panonské	panonský	k2eAgFnSc2d1	Panonská
nížiny	nížina	k1gFnSc2	nížina
-	-	kIx~	-
Langobardi	Langobard	k1gMnPc1	Langobard
<g/>
.	.	kIx.	.
</s>
<s>
Vykopávky	vykopávka	k1gFnPc1	vykopávka
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
Kranje	Kranj	k1gFnSc2	Kranj
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
prostor	prostora	k1gFnPc2	prostora
současného	současný	k2eAgNnSc2d1	současné
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
obývali	obývat	k5eAaImAgMnP	obývat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
Langobardů	Langobard	k1gInPc2	Langobard
přicházejí	přicházet	k5eAaImIp3nP	přicházet
v	v	k7c6	v
šestém	šestý	k4xOgNnSc6	šestý
století	století	k1gNnSc6	století
do	do	k7c2	do
alpských	alpský	k2eAgNnPc2d1	alpské
údolí	údolí	k1gNnSc2	údolí
slovanské	slovanský	k2eAgInPc1d1	slovanský
kmeny	kmen	k1gInPc1	kmen
-	-	kIx~	-
tzv.	tzv.	kA	tzv.
alpští	alpský	k2eAgMnPc1d1	alpský
Slované	Slovan	k1gMnPc1	Slovan
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc1	dva
hypotézy	hypotéza	k1gFnPc1	hypotéza
o	o	k7c6	o
příchodu	příchod	k1gInSc6	příchod
alpských	alpský	k2eAgInPc2d1	alpský
Slovanů	Slovan	k1gInPc2	Slovan
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
všeobecně	všeobecně	k6eAd1	všeobecně
přijímána	přijímán	k2eAgFnSc1d1	přijímána
a	a	k8xC	a
doložena	doložen	k2eAgFnSc1d1	doložena
teze	teze	k1gFnSc1	teze
<g/>
,	,	kIx,	,
že	že	k8xS	že
prostor	prostor	k1gInSc1	prostor
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
byl	být	k5eAaImAgInS	být
osídlen	osídlit	k5eAaPmNgInS	osídlit
dvěma	dva	k4xCgFnPc7	dva
vlnami	vlna	k1gFnPc7	vlna
<g/>
:	:	kIx,	:
první	první	k4xOgFnSc1	první
vlna	vlna	k1gFnSc1	vlna
dorazila	dorazit	k5eAaPmAgFnS	dorazit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
550	[number]	k4	550
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
z	z	k7c2	z
oblastí	oblast	k1gFnPc2	oblast
osídlených	osídlený	k2eAgFnPc2d1	osídlená
západními	západní	k2eAgInPc7d1	západní
Slovany	Slovan	k1gInPc7	Slovan
<g/>
.	.	kIx.	.
</s>
<s>
Nálezy	nález	k1gInPc1	nález
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
tuto	tento	k3xDgFnSc4	tento
tezi	teze	k1gFnSc4	teze
potvrdily	potvrdit	k5eAaPmAgInP	potvrdit
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
nalezeny	nalézt	k5eAaBmNgInP	nalézt
až	až	k6eAd1	až
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
při	při	k7c6	při
výstavbě	výstavba	k1gFnSc6	výstavba
dálnice	dálnice	k1gFnSc2	dálnice
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Mariboru	Maribor	k1gInSc2	Maribor
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
vlna	vlna	k1gFnSc1	vlna
přišla	přijít	k5eAaPmAgFnS	přijít
později	pozdě	k6eAd2	pozdě
podél	podél	k7c2	podél
řek	řeka	k1gFnPc2	řeka
Drávy	Dráva	k1gFnSc2	Dráva
a	a	k8xC	a
Sávy	Sáva	k1gFnSc2	Sáva
z	z	k7c2	z
jihovýchodu	jihovýchod	k1gInSc2	jihovýchod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
-	-	kIx~	-
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
-	-	kIx~	-
za	za	k7c2	za
neexistence	neexistence	k1gFnSc2	neexistence
nálezů	nález	k1gInPc2	nález
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
by	by	kYmCp3nS	by
prokazovaly	prokazovat	k5eAaImAgFnP	prokazovat
vazbu	vazba	k1gFnSc4	vazba
se	s	k7c7	s
severními	severní	k2eAgFnPc7d1	severní
oblastmi	oblast	k1gFnPc7	oblast
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
podporována	podporovat	k5eAaImNgFnS	podporovat
představa	představa	k1gFnSc1	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
celý	celý	k2eAgInSc1d1	celý
prostor	prostor	k1gInSc1	prostor
byl	být	k5eAaImAgInS	být
osídlen	osídlit	k5eAaPmNgInS	osídlit
právě	právě	k6eAd1	právě
z	z	k7c2	z
jihovýchodu	jihovýchod	k1gInSc2	jihovýchod
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mělo	mít	k5eAaImAgNnS	mít
potvrzovat	potvrzovat	k5eAaImF	potvrzovat
jednotný	jednotný	k2eAgInSc4d1	jednotný
původ	původ	k1gInSc4	původ
jižních	jižní	k2eAgInPc2d1	jižní
Slovanů	Slovan	k1gInPc2	Slovan
<g/>
.	.	kIx.	.
</s>
<s>
Slované	Slovan	k1gMnPc1	Slovan
byli	být	k5eAaImAgMnP	být
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
zafixování	zafixování	k1gNnPc2	zafixování
langobradským	langobradský	k2eAgFnPc3d1	langobradský
limes	limesa	k1gFnPc2	limesa
a	a	k8xC	a
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
Karavankami	Karavanky	k1gFnPc7	Karavanky
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byli	být	k5eAaImAgMnP	být
vytlačeni	vytlačit	k5eAaPmNgMnP	vytlačit
německou	německý	k2eAgFnSc7d1	německá
kolonizací	kolonizace	k1gFnSc7	kolonizace
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
probíhala	probíhat	k5eAaImAgFnS	probíhat
od	od	k7c2	od
osmého	osmý	k4xOgNnSc2	osmý
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Fredegardova	Fredegardův	k2eAgFnSc1d1	Fredegardův
kronika	kronika	k1gFnSc1	kronika
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
vládě	vláda	k1gFnSc6	vláda
Valuka	Valuk	k1gMnSc2	Valuk
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
Sámovým	Sámův	k2eAgMnSc7d1	Sámův
vrstevníkem	vrstevník	k1gMnSc7	vrstevník
a	a	k8xC	a
spojencem	spojenec	k1gMnSc7	spojenec
zapojeným	zapojený	k2eAgMnSc7d1	zapojený
do	do	k7c2	do
kmenového	kmenový	k2eAgInSc2d1	kmenový
svazu	svaz	k1gInSc2	svaz
vzniklým	vzniklý	k2eAgInSc7d1	vzniklý
k	k	k7c3	k
obraně	obrana	k1gFnSc6	obrana
proti	proti	k7c3	proti
Avarům	Avar	k1gMnPc3	Avar
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
670	[number]	k4	670
se	se	k3xPyFc4	se
o	o	k7c6	o
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
žijí	žít	k5eAaImIp3nP	žít
alpští	alpský	k2eAgMnPc1d1	alpský
Slované	Slovan	k1gMnPc1	Slovan
<g/>
,	,	kIx,	,
hovoří	hovořit	k5eAaImIp3nS	hovořit
jako	jako	k9	jako
o	o	k7c4	o
Karantánii	Karantánie	k1gFnSc4	Karantánie
(	(	kIx(	(
<g/>
Carantania	Carantanium	k1gNnPc1	Carantanium
<g/>
,	,	kIx,	,
civitas	civitas	k1gInSc1	civitas
Carantana	Carantana	k1gFnSc1	Carantana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gNnSc7	jejíž
jádrem	jádro	k1gNnSc7	jádro
byly	být	k5eAaImAgInP	být
dnešní	dnešní	k2eAgInPc1d1	dnešní
Korutany	Korutany	k1gInPc1	Korutany
<g/>
.	.	kIx.	.
</s>
<s>
Anonymní	anonymní	k2eAgMnSc1d1	anonymní
ravenský	ravenský	k2eAgMnSc1d1	ravenský
autor	autor	k1gMnSc1	autor
pro	pro	k7c4	pro
alpské	alpský	k2eAgInPc4d1	alpský
Slovany	Slovan	k1gInPc4	Slovan
použil	použít	k5eAaPmAgMnS	použít
starého	starý	k2eAgNnSc2d1	staré
keltského	keltský	k2eAgNnSc2d1	keltské
pojmenování	pojmenování	k1gNnSc2	pojmenování
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
langobardští	langobardský	k2eAgMnPc1d1	langobardský
historici	historik	k1gMnPc1	historik
používali	používat	k5eAaImAgMnP	používat
pro	pro	k7c4	pro
alpské	alpský	k2eAgInPc4d1	alpský
Slovany	Slovan	k1gInPc4	Slovan
pojmenování	pojmenování	k1gNnSc2	pojmenování
Karantánci	Karantánec	k1gMnSc3	Karantánec
<g/>
.	.	kIx.	.
</s>
<s>
Centrem	centr	k1gInSc7	centr
Karantánie	Karantánie	k1gFnSc2	Karantánie
byl	být	k5eAaImAgInS	být
Krnski	Krnske	k1gFnSc4	Krnske
grad	grad	k1gInSc1	grad
(	(	kIx(	(
<g/>
Karnburg	Karnburg	k1gInSc1	Karnburg
<g/>
)	)	kIx)	)
ležící	ležící	k2eAgFnSc1d1	ležící
nedaleko	nedaleko	k7c2	nedaleko
dnešního	dnešní	k2eAgInSc2d1	dnešní
Klagenfurtu	Klagenfurt	k1gInSc2	Klagenfurt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
karantánských	karantánský	k2eAgInPc2d1	karantánský
Slovanů	Slovan	k1gInPc2	Slovan
byl	být	k5eAaImAgMnS	být
kníže	kníže	k1gMnSc1	kníže
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
výběru	výběr	k1gInSc6	výběr
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
tzv.	tzv.	kA	tzv.
kosezové	kosezová	k1gFnPc4	kosezová
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
družinu	družina	k1gFnSc4	družina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
z	z	k7c2	z
někdejších	někdejší	k2eAgMnPc2d1	někdejší
svobodných	svobodný	k2eAgMnPc2d1	svobodný
sedláků	sedlák	k1gMnPc2	sedlák
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
740	[number]	k4	740
zesílily	zesílit	k5eAaPmAgInP	zesílit
útoky	útok	k1gInPc1	útok
Avarů	Avar	k1gMnPc2	Avar
<g/>
,	,	kIx,	,
požádali	požádat	k5eAaPmAgMnP	požádat
karantánští	karantánský	k2eAgMnPc1d1	karantánský
Slované	Slovan	k1gMnPc1	Slovan
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
Bavory	Bavory	k1gInPc1	Bavory
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
745	[number]	k4	745
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
knížete	kníže	k1gMnSc2	kníže
Boruta	Borut	k1gMnSc2	Borut
Karantánci	Karantánek	k1gMnPc1	Karantánek
dobrovolně	dobrovolně	k6eAd1	dobrovolně
přijali	přijmout	k5eAaPmAgMnP	přijmout
ochranu	ochrana	k1gFnSc4	ochrana
a	a	k8xC	a
politickou	politický	k2eAgFnSc4d1	politická
nadvládu	nadvláda	k1gFnSc4	nadvláda
Bavorů	Bavor	k1gMnPc2	Bavor
a	a	k8xC	a
skrze	skrze	k?	skrze
tuto	tento	k3xDgFnSc4	tento
nadvládu	nadvláda	k1gFnSc4	nadvláda
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
nové	nový	k2eAgFnSc3d1	nová
vlně	vlna	k1gFnSc3	vlna
christianizace	christianizace	k1gFnSc2	christianizace
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
již	již	k9	již
v	v	k7c6	v
římských	římský	k2eAgFnPc6d1	římská
dobách	doba	k1gFnPc6	doba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
přišli	přijít	k5eAaPmAgMnP	přijít
do	do	k7c2	do
současného	současný	k2eAgInSc2d1	současný
slovinského	slovinský	k2eAgInSc2d1	slovinský
prostoru	prostor	k1gInSc2	prostor
misionáři	misionář	k1gMnSc3	misionář
<g/>
,	,	kIx,	,
jako	jako	k9	jako
rukojmí	rukojmí	k1gMnPc4	rukojmí
přijala	přijmout	k5eAaPmAgFnS	přijmout
v	v	k7c6	v
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
křesťanství	křesťanství	k1gNnSc2	křesťanství
i	i	k9	i
první	první	k4xOgNnPc1	první
karantánská	karantánský	k2eAgNnPc1d1	karantánský
knížata	kníže	k1gNnPc1	kníže
-	-	kIx~	-
Gorazd	Gorazd	k1gInSc1	Gorazd
a	a	k8xC	a
Hotimir	Hotimir	k1gInSc1	Hotimir
<g/>
.	.	kIx.	.
</s>
<s>
Hotimir	Hotimir	k1gInSc1	Hotimir
si	se	k3xPyFc3	se
od	od	k7c2	od
salcburského	salcburský	k2eAgMnSc2d1	salcburský
biskupa	biskup	k1gMnSc2	biskup
vyžádal	vyžádat	k5eAaPmAgMnS	vyžádat
chorbiskupa	chorbiskupa	k1gFnSc1	chorbiskupa
a	a	k8xC	a
touto	tento	k3xDgFnSc7	tento
funkcí	funkce	k1gFnSc7	funkce
byl	být	k5eAaImAgMnS	být
pověřen	pověřit	k5eAaPmNgMnS	pověřit
Ir	Ir	k1gMnSc1	Ir
Modestus	Modestus	k1gMnSc1	Modestus
<g/>
,	,	kIx,	,
z	z	k7c2	z
jehož	jehož	k3xOyRp3gMnPc2	jehož
spisů	spis	k1gInPc2	spis
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzpoury	vzpoura	k1gFnPc1	vzpoura
proti	proti	k7c3	proti
pokřesťanštění	pokřesťanštění	k1gNnSc3	pokřesťanštění
byly	být	k5eAaImAgInP	být
Bavory	Bavory	k1gInPc1	Bavory
násilně	násilně	k6eAd1	násilně
potlačovány	potlačován	k2eAgInPc1d1	potlačován
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
788	[number]	k4	788
stalo	stát	k5eAaPmAgNnS	stát
Bavorsko	Bavorsko	k1gNnSc1	Bavorsko
součástí	součást	k1gFnPc2	součást
Francké	francký	k2eAgFnSc2d1	Francká
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
dostala	dostat	k5eAaPmAgFnS	dostat
se	se	k3xPyFc4	se
i	i	k9	i
doposud	doposud	k6eAd1	doposud
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
autonomní	autonomní	k2eAgFnSc2d1	autonomní
Karantánie	Karantánie	k1gFnSc2	Karantánie
pod	pod	k7c4	pod
franckou	francký	k2eAgFnSc4d1	Francká
nadvládu	nadvláda	k1gFnSc4	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pramenů	pramen	k1gInPc2	pramen
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
osmém	osmý	k4xOgInSc6	osmý
století	století	k1gNnSc6	století
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Karavanek	Karavanky	k1gFnPc2	Karavanky
existovalo	existovat	k5eAaImAgNnS	existovat
malé	malý	k2eAgNnSc1d1	malé
slovanské	slovanský	k2eAgNnSc1d1	slovanské
knížectví	knížectví	k1gNnSc1	knížectví
-	-	kIx~	-
Karniola	Karniola	k1gFnSc1	Karniola
-	-	kIx~	-
s	s	k7c7	s
centrem	centrum	k1gNnSc7	centrum
v	v	k7c6	v
někdejším	někdejší	k2eAgNnSc6d1	někdejší
opevněném	opevněný	k2eAgNnSc6d1	opevněné
středisku	středisko	k1gNnSc6	středisko
Keltů	Kelt	k1gMnPc2	Kelt
i	i	k8xC	i
pozdějších	pozdní	k2eAgMnPc2d2	pozdější
Římanů	Říman	k1gMnPc2	Říman
Carnii	Carnie	k1gFnSc4	Carnie
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc1d1	dnešní
Kranj	Kranj	k1gInSc1	Kranj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
795	[number]	k4	795
spojená	spojený	k2eAgNnPc1d1	spojené
vojska	vojsko	k1gNnPc1	vojsko
Franků	Frank	k1gMnPc2	Frank
<g/>
,	,	kIx,	,
Karantánců	Karantánec	k1gMnPc2	Karantánec
a	a	k8xC	a
Karniolců	Karniolec	k1gMnPc2	Karniolec
drtivě	drtivě	k6eAd1	drtivě
porazila	porazit	k5eAaPmAgFnS	porazit
v	v	k7c6	v
Panonské	panonský	k2eAgFnSc6d1	Panonská
nížině	nížina	k1gFnSc6	nížina
Avary	Avar	k1gMnPc4	Avar
<g/>
.	.	kIx.	.
</s>
<s>
Karantáncům	Karantánec	k1gMnPc3	Karantánec
i	i	k8xC	i
Karniolcům	Karniolec	k1gMnPc3	Karniolec
bylo	být	k5eAaImAgNnS	být
tažení	tažení	k1gNnSc1	tažení
proti	proti	k7c3	proti
Avarům	Avar	k1gMnPc3	Avar
Franky	Frank	k1gMnPc7	Frank
přikázáno	přikázán	k2eAgNnSc1d1	přikázáno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
819	[number]	k4	819
až	až	k8xS	až
823	[number]	k4	823
se	se	k3xPyFc4	se
Slované	Slovan	k1gMnPc1	Slovan
z	z	k7c2	z
Karantánie	Karantánie	k1gFnSc2	Karantánie
a	a	k8xC	a
Karnioly	Karniola	k1gFnSc2	Karniola
připojili	připojit	k5eAaPmAgMnP	připojit
k	k	k7c3	k
protifranckému	protifrancký	k2eAgNnSc3d1	protifrancký
povstání	povstání	k1gNnSc3	povstání
Ljudevita	Ljudevita	k1gFnSc1	Ljudevita
Posávského	Posávský	k2eAgInSc2d1	Posávský
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
potlačení	potlačení	k1gNnSc6	potlačení
povstání	povstání	k1gNnSc2	povstání
byla	být	k5eAaImAgFnS	být
politická	politický	k2eAgFnSc1d1	politická
autonomie	autonomie	k1gFnSc1	autonomie
těchto	tento	k3xDgNnPc2	tento
knížectví	knížectví	k1gNnPc2	knížectví
fakticky	fakticky	k6eAd1	fakticky
zlikvidována	zlikvidován	k2eAgNnPc1d1	zlikvidováno
a	a	k8xC	a
dosavadní	dosavadní	k2eAgNnPc1d1	dosavadní
slovanská	slovanský	k2eAgNnPc1d1	slovanské
knížata	kníže	k1gNnPc1	kníže
byla	být	k5eAaImAgNnP	být
zbavena	zbavit	k5eAaPmNgNnP	zbavit
své	svůj	k3xOyFgFnSc2	svůj
moci	moct	k5eAaImF	moct
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
někdejší	někdejší	k2eAgFnSc2d1	někdejší
Karnioly	Karniola	k1gFnSc2	Karniola
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Kraňská	kraňský	k2eAgFnSc1d1	kraňská
marka	marka	k1gFnSc1	marka
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
vyšší	vysoký	k2eAgFnPc1d2	vyšší
sociální	sociální	k2eAgFnPc1d1	sociální
skupiny	skupina	k1gFnPc1	skupina
byly	být	k5eAaImAgFnP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
příslušníky	příslušník	k1gMnPc7	příslušník
nového	nový	k2eAgNnSc2d1	nové
německého	německý	k2eAgNnSc2d1	německé
etnika	etnikum	k1gNnSc2	etnikum
<g/>
,	,	kIx,	,
Slované	Slovan	k1gMnPc1	Slovan
-	-	kIx~	-
označovaní	označovaný	k2eAgMnPc1d1	označovaný
jako	jako	k8xS	jako
Vindové	Vind	k1gMnPc1	Vind
-	-	kIx~	-
žili	žít	k5eAaImAgMnP	žít
zejména	zejména	k9	zejména
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
jako	jako	k9	jako
rolníci	rolník	k1gMnPc1	rolník
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zde	zde	k6eAd1	zde
vládnoucích	vládnoucí	k2eAgMnPc2d1	vládnoucí
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
rodů	rod	k1gInPc2	rod
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
důrazněji	důrazně	k6eAd2	důrazně
prosadili	prosadit	k5eAaPmAgMnP	prosadit
zejména	zejména	k9	zejména
Eppensteinové	Eppenstein	k1gMnPc1	Eppenstein
<g/>
,	,	kIx,	,
Spanheimové	Spanheimové	k2eAgMnPc1d1	Spanheimové
a	a	k8xC	a
Celjští	Celjský	k2eAgMnPc1d1	Celjský
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
díky	díky	k7c3	díky
úspěšné	úspěšný	k2eAgFnSc6d1	úspěšná
sňatkové	sňatkový	k2eAgFnSc6d1	sňatková
politice	politika	k1gFnSc6	politika
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
hodnosti	hodnost	k1gFnPc4	hodnost
říšských	říšský	k2eAgNnPc2d1	říšské
knížat	kníže	k1gNnPc2	kníže
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
nadvláda	nadvláda	k1gFnSc1	nadvláda
znamenala	znamenat	k5eAaImAgFnS	znamenat
germanizaci	germanizace	k1gFnSc4	germanizace
slovinských	slovinský	k2eAgFnPc2d1	slovinská
osad	osada	k1gFnPc2	osada
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
Drávy	Dráva	k1gFnSc2	Dráva
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
uchování	uchování	k1gNnSc3	uchování
národní	národní	k2eAgFnSc2d1	národní
identity	identita	k1gFnSc2	identita
přispěla	přispět	k5eAaPmAgFnS	přispět
místní	místní	k2eAgFnSc1d1	místní
inteligence	inteligence	k1gFnSc1	inteligence
<g/>
,	,	kIx,	,
tvořená	tvořený	k2eAgFnSc1d1	tvořená
zejména	zejména	k9	zejména
katolickými	katolický	k2eAgMnPc7d1	katolický
duchovními	duchovní	k1gMnPc7	duchovní
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
si	se	k3xPyFc3	se
podstatná	podstatný	k2eAgFnSc1d1	podstatná
část	část	k1gFnSc1	část
slovanského	slovanský	k2eAgNnSc2d1	slovanské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
udržela	udržet	k5eAaPmAgFnS	udržet
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
svobodné	svobodný	k2eAgNnSc4d1	svobodné
postavení	postavení	k1gNnSc4	postavení
a	a	k8xC	a
řídila	řídit	k5eAaImAgFnS	řídit
se	s	k7c7	s
vlastními	vlastní	k2eAgInPc7d1	vlastní
zákony	zákon	k1gInPc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
feudálních	feudální	k2eAgFnPc2d1	feudální
vrstev	vrstva	k1gFnPc2	vrstva
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
svobodní	svobodný	k2eAgMnPc1d1	svobodný
rolníci	rolník	k1gMnPc1	rolník
měnili	měnit	k5eAaImAgMnP	měnit
v	v	k7c6	v
polosvobodné	polosvobodný	k2eAgFnSc6d1	polosvobodný
či	či	k8xC	či
v	v	k7c6	v
poddané	poddaná	k1gFnSc6	poddaná
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
doložena	doložit	k5eAaPmNgFnS	doložit
i	i	k9	i
existence	existence	k1gFnSc1	existence
otroků	otrok	k1gMnPc2	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Hospodaření	hospodaření	k1gNnSc1	hospodaření
se	se	k3xPyFc4	se
soustředilo	soustředit	k5eAaPmAgNnS	soustředit
do	do	k7c2	do
dvorců	dvorec	k1gInPc2	dvorec
zakládaných	zakládaný	k2eAgInPc2d1	zakládaný
panovníkem	panovník	k1gMnSc7	panovník
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
velmoži	velmož	k1gMnPc7	velmož
<g/>
.	.	kIx.	.
</s>
<s>
Podstatný	podstatný	k2eAgInSc1d1	podstatný
rozvoj	rozvoj	k1gInSc1	rozvoj
společnosti	společnost	k1gFnSc2	společnost
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
urbanizací	urbanizace	k1gFnSc7	urbanizace
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
docházelo	docházet	k5eAaImAgNnS	docházet
od	od	k7c2	od
9	[number]	k4	9
<g/>
.	.	kIx.	.
a	a	k8xC	a
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Vznikající	vznikající	k2eAgFnSc1d1	vznikající
nebo	nebo	k8xC	nebo
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
rozrůstající	rozrůstající	k2eAgNnPc1d1	rozrůstající
města	město	k1gNnPc1	město
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
obyvatelé	obyvatel	k1gMnPc1	obyvatel
byli	být	k5eAaImAgMnP	být
svobodní	svobodný	k2eAgMnPc1d1	svobodný
<g/>
,	,	kIx,	,
postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
získávali	získávat	k5eAaImAgMnP	získávat
další	další	k2eAgFnPc4d1	další
výhody	výhoda	k1gFnPc4	výhoda
a	a	k8xC	a
privilegia	privilegium	k1gNnPc4	privilegium
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
ražení	ražení	k1gNnPc2	ražení
mincí	mince	k1gFnPc2	mince
a	a	k8xC	a
vybírání	vybírání	k1gNnSc6	vybírání
poplatků	poplatek	k1gInPc2	poplatek
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
například	například	k6eAd1	například
i	i	k9	i
o	o	k7c4	o
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
hradeb	hradba	k1gFnPc2	hradba
<g/>
,	,	kIx,	,
volení	volení	k1gNnSc4	volení
vlastní	vlastní	k2eAgFnSc2d1	vlastní
samosprávy	samospráva	k1gFnSc2	samospráva
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
vytvářelo	vytvářet	k5eAaImAgNnS	vytvářet
na	na	k7c6	na
dnešním	dnešní	k2eAgNnSc6d1	dnešní
slovinském	slovinský	k2eAgNnSc6d1	slovinské
území	území	k1gNnSc6	území
i	i	k9	i
městské	městský	k2eAgNnSc4d1	Městské
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Význačným	význačný	k2eAgMnSc7d1	význačný
pozemkovým	pozemkový	k2eAgMnSc7d1	pozemkový
vlastníkem	vlastník	k1gMnSc7	vlastník
byla	být	k5eAaImAgFnS	být
i	i	k9	i
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
těžící	těžící	k2eAgFnSc1d1	těžící
z	z	k7c2	z
četných	četný	k2eAgFnPc2d1	četná
donací	donace	k1gFnPc2	donace
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
pozici	pozice	k1gFnSc4	pozice
zde	zde	k6eAd1	zde
měl	mít	k5eAaImAgMnS	mít
aquilejský	aquilejský	k2eAgInSc4d1	aquilejský
patriarchát	patriarchát	k1gInSc4	patriarchát
a	a	k8xC	a
některá	některý	k3yIgNnPc4	některý
biskupství	biskupství	k1gNnPc4	biskupství
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
freisinské	freisinský	k2eAgNnSc1d1	freisinský
a	a	k8xC	a
bamberské	bamberský	k2eAgNnSc1d1	bamberské
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
nich	on	k3xPp3gInPc2	on
vznikaly	vznikat	k5eAaImAgFnP	vznikat
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1000	[number]	k4	1000
i	i	k8xC	i
benediktinské	benediktinský	k2eAgInPc1d1	benediktinský
kláštery	klášter	k1gInPc1	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vývoj	vývoj	k1gInSc1	vývoj
se	se	k3xPyFc4	se
promítl	promítnout	k5eAaPmAgInS	promítnout
i	i	k9	i
do	do	k7c2	do
kulturní	kulturní	k2eAgFnSc2d1	kulturní
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působily	působit	k5eAaImAgInP	působit
různé	různý	k2eAgInPc1d1	různý
vlivy	vliv	k1gInPc1	vliv
z	z	k7c2	z
okolních	okolní	k2eAgFnPc2d1	okolní
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vrcholném	vrcholný	k2eAgInSc6d1	vrcholný
a	a	k8xC	a
pozdním	pozdní	k2eAgInSc6d1	pozdní
středověku	středověk	k1gInSc6	středověk
se	se	k3xPyFc4	se
původní	původní	k2eAgInPc1d1	původní
dvorce	dvorec	k1gInPc1	dvorec
rozpouštěly	rozpouštět	k5eAaImAgInP	rozpouštět
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
menších	malý	k2eAgFnPc2d2	menší
usedlostí	usedlost	k1gFnPc2	usedlost
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
robotovali	robotovat	k5eAaImAgMnP	robotovat
sami	sám	k3xTgMnPc1	sám
poddaní	poddaný	k2eAgMnPc1d1	poddaný
rolníci	rolník	k1gMnPc1	rolník
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
finanční	finanční	k2eAgInSc4d1	finanční
obnos	obnos	k1gInSc4	obnos
<g/>
.	.	kIx.	.
</s>
<s>
Rolník	rolník	k1gMnSc1	rolník
byl	být	k5eAaImAgMnS	být
povinen	povinen	k2eAgInSc4d1	povinen
omezený	omezený	k2eAgInSc4d1	omezený
počet	počet	k1gInSc4	počet
dnů	den	k1gInPc2	den
v	v	k7c6	v
roce	rok	k1gInSc6	rok
robotovat	robotovat	k5eAaImF	robotovat
a	a	k8xC	a
odvádět	odvádět	k5eAaImF	odvádět
vrchnosti	vrchnost	k1gFnPc4	vrchnost
naturální	naturální	k2eAgFnPc4d1	naturální
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
peněžité	peněžitý	k2eAgInPc1d1	peněžitý
<g/>
,	,	kIx,	,
dávky	dávka	k1gFnPc1	dávka
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
pokračující	pokračující	k2eAgFnSc4d1	pokračující
germanizaci	germanizace	k1gFnSc4	germanizace
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
zastavila	zastavit	k5eAaPmAgFnS	zastavit
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
imigrace	imigrace	k1gFnSc2	imigrace
slovanského	slovanský	k2eAgNnSc2d1	slovanské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
ohrožených	ohrožený	k2eAgMnPc2d1	ohrožený
Turky	Turek	k1gMnPc4	Turek
<g/>
.	.	kIx.	.
</s>
<s>
Pokračoval	pokračovat	k5eAaImAgInS	pokračovat
rozvoj	rozvoj	k1gInSc1	rozvoj
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
společenské	společenský	k2eAgFnPc1d1	společenská
elity	elita	k1gFnPc1	elita
mluvily	mluvit	k5eAaImAgFnP	mluvit
nejčastěji	často	k6eAd3	často
německým	německý	k2eAgInSc7d1	německý
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Podoba	podoba	k1gFnSc1	podoba
jazyka	jazyk	k1gInSc2	jazyk
Karantánců	Karantánec	k1gMnPc2	Karantánec
je	být	k5eAaImIp3nS	být
zachycena	zachytit	k5eAaPmNgFnS	zachytit
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
Frisinských	Frisinský	k2eAgInPc6d1	Frisinský
zlomcích	zlomek	k1gInPc6	zlomek
pocházejících	pocházející	k2eAgMnPc2d1	pocházející
z	z	k7c2	z
období	období	k1gNnSc2	období
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1000	[number]	k4	1000
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
freisinské	freisinský	k2eAgNnSc1d1	freisinský
biskupství	biskupství	k1gNnSc1	biskupství
sestavilo	sestavit	k5eAaPmAgNnS	sestavit
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
svého	svůj	k3xOyFgInSc2	svůj
kostela	kostel	k1gInSc2	kostel
ve	v	k7c6	v
Spittalu	Spittal	k1gMnSc6	Spittal
převážně	převážně	k6eAd1	převážně
latinský	latinský	k2eAgInSc1d1	latinský
liturgický	liturgický	k2eAgInSc1d1	liturgický
kodex	kodex	k1gInSc1	kodex
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
tři	tři	k4xCgInPc1	tři
slovanské	slovanský	k2eAgInPc1d1	slovanský
texty	text	k1gInPc1	text
-	-	kIx~	-
vzorec	vzorec	k1gInSc1	vzorec
všeobecné	všeobecný	k2eAgFnSc2d1	všeobecná
zpovědi	zpověď	k1gFnSc2	zpověď
<g/>
,	,	kIx,	,
krátké	krátký	k2eAgNnSc4d1	krátké
kázání	kázání	k1gNnSc4	kázání
o	o	k7c6	o
hříchu	hřích	k1gInSc6	hřích
a	a	k8xC	a
pokání	pokání	k1gNnSc1	pokání
a	a	k8xC	a
krátká	krátký	k2eAgFnSc1d1	krátká
zpovědní	zpovědní	k2eAgFnSc1d1	zpovědní
modlitba	modlitba	k1gFnSc1	modlitba
<g/>
.	.	kIx.	.
</s>
<s>
Frisinské	Frisinský	k2eAgInPc1d1	Frisinský
zlomky	zlomek	k1gInPc1	zlomek
jsou	být	k5eAaImIp3nP	být
nejen	nejen	k6eAd1	nejen
nejstarší	starý	k2eAgFnSc7d3	nejstarší
literární	literární	k2eAgFnSc7d1	literární
památkou	památka	k1gFnSc7	památka
ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
nejstarší	starý	k2eAgFnSc7d3	nejstarší
latinkou	latinka	k1gFnSc7	latinka
zaznamenaný	zaznamenaný	k2eAgInSc1d1	zaznamenaný
slovanský	slovanský	k2eAgInSc1d1	slovanský
text	text	k1gInSc1	text
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ustavení	ustavení	k1gNnSc6	ustavení
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
sloučením	sloučení	k1gNnSc7	sloučení
hrabství	hrabství	k1gNnPc2	hrabství
a	a	k8xC	a
pohraničních	pohraniční	k2eAgFnPc2d1	pohraniční
marek	marka	k1gFnPc2	marka
Velká	velká	k1gFnSc1	velká
Karantánie	Karantánie	k1gFnSc1	Karantánie
sloužící	sloužící	k1gFnSc1	sloužící
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
jižní	jižní	k2eAgFnSc2d1	jižní
hranice	hranice	k1gFnSc2	hranice
před	před	k7c7	před
maďarskými	maďarský	k2eAgInPc7d1	maďarský
výboji	výboj	k1gInPc7	výboj
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
hranici	hranice	k1gFnSc6	hranice
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
římská	římský	k2eAgFnSc1d1	římská
hranice	hranice	k1gFnSc1	hranice
na	na	k7c6	na
Sotle	Sotl	k1gInSc6	Sotl
a	a	k8xC	a
Kolpě	Kolpa	k1gFnSc3	Kolpa
vymezila	vymezit	k5eAaPmAgFnS	vymezit
východní	východní	k2eAgInPc4d1	východní
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
hranici	hranice	k1gFnSc6	hranice
slovinského	slovinský	k2eAgInSc2d1	slovinský
etnického	etnický	k2eAgInSc2d1	etnický
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
německé	německý	k2eAgFnSc3d1	německá
kolonizaci	kolonizace	k1gFnSc3	kolonizace
Podunají	Podunají	k1gNnSc2	Podunají
a	a	k8xC	a
vytlačení	vytlačení	k1gNnSc2	vytlačení
Slovanů	Slovan	k1gInPc2	Slovan
z	z	k7c2	z
prostoru	prostor	k1gInSc2	prostor
Balatonu	Balaton	k1gInSc2	Balaton
Maďary	maďar	k1gInPc7	maďar
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
souvislého	souvislý	k2eAgNnSc2d1	souvislé
slovanského	slovanský	k2eAgNnSc2d1	slovanské
osídlení	osídlení	k1gNnSc2	osídlení
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
Karantánie	Karantánie	k1gFnSc1	Karantánie
existující	existující	k2eAgFnSc1d1	existující
v	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
976	[number]	k4	976
až	až	k8xS	až
1077	[number]	k4	1077
byla	být	k5eAaImAgFnS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
Karantánským	Karantánský	k2eAgNnSc7d1	Karantánský
vévodstvím	vévodství	k1gNnSc7	vévodství
a	a	k8xC	a
několika	několik	k4yIc7	několik
markami	marka	k1gFnPc7	marka
<g/>
:	:	kIx,	:
Karantánskou	Karantánský	k2eAgFnSc7d1	Karantánský
<g/>
,	,	kIx,	,
Podrávskou	Podrávský	k2eAgFnSc7d1	Podrávský
<g/>
,	,	kIx,	,
Saviňskou	Saviňský	k2eAgFnSc7d1	Saviňský
<g/>
,	,	kIx,	,
Kraňskou	kraňský	k2eAgFnSc7d1	kraňská
(	(	kIx(	(
<g/>
Posávskou	Posávský	k2eAgFnSc7d1	Posávský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Istrijskou	istrijský	k2eAgFnSc4d1	istrijská
<g/>
,	,	kIx,	,
Furlanskou	Furlanský	k2eAgFnSc4d1	Furlanský
a	a	k8xC	a
Veronskou	veronský	k2eAgFnSc4d1	veronská
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
od	od	k7c2	od
vévodství	vévodství	k1gNnSc2	vévodství
oddělila	oddělit	k5eAaPmAgFnS	oddělit
Veronská	veronský	k2eAgFnSc1d1	veronská
a	a	k8xC	a
Furlanská	Furlanský	k2eAgFnSc1d1	Furlanský
marka	marka	k1gFnSc1	marka
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
také	také	k9	také
Karantánská	Karantánský	k2eAgFnSc1d1	Karantánský
marka	marka	k1gFnSc1	marka
<g/>
.	.	kIx.	.
</s>
<s>
Nejsevernějším	severní	k2eAgNnSc7d3	nejsevernější
hrabstvím	hrabství	k1gNnSc7	hrabství
Karantánské	Karantánský	k2eAgFnSc2d1	Karantánský
marky	marka	k1gFnSc2	marka
bylo	být	k5eAaImAgNnS	být
hrabství	hrabství	k1gNnSc1	hrabství
Steyer	Steyra	k1gFnPc2	Steyra
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
marce	marka	k1gFnSc3	marka
dalo	dát	k5eAaPmAgNnS	dát
nové	nový	k2eAgNnSc1d1	nové
označení	označení	k1gNnSc1	označení
-	-	kIx~	-
Štýrsko	Štýrsko	k1gNnSc1	Štýrsko
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc4	rod
Andechs-Meranů	Andechs-Meran	k1gInPc2	Andechs-Meran
vládnoucí	vládnoucí	k2eAgFnSc1d1	vládnoucí
Posávské	Posávský	k2eAgFnSc3d1	Posávský
marce	marka	k1gFnSc3	marka
získal	získat	k5eAaPmAgInS	získat
postupně	postupně	k6eAd1	postupně
i	i	k9	i
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
Saviňské	Saviňský	k2eAgFnSc2d1	Saviňský
marky	marka	k1gFnSc2	marka
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
byl	být	k5eAaImAgInS	být
dán	dát	k5eAaPmNgInS	dát
základ	základ	k1gInSc1	základ
vzniku	vznik	k1gInSc2	vznik
nové	nový	k2eAgFnSc2d1	nová
země	zem	k1gFnSc2	zem
-	-	kIx~	-
Kraňska	Kraňsko	k1gNnSc2	Kraňsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
většinu	většina	k1gFnSc4	většina
měli	mít	k5eAaImAgMnP	mít
Slované	Slovan	k1gMnPc1	Slovan
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
není	být	k5eNaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
Kraňské	kraňský	k2eAgFnSc2d1	kraňská
marky	marka	k1gFnSc2	marka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
slovanského	slovanský	k2eAgNnSc2d1	slovanské
označení	označení	k1gNnSc2	označení
Krajina	Krajina	k1gFnSc1	Krajina
používaného	používaný	k2eAgNnSc2d1	používané
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
pohraniční	pohraniční	k2eAgFnSc2d1	pohraniční
marky	marka	k1gFnSc2	marka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1251	[number]	k4	1251
připojil	připojit	k5eAaPmAgMnS	připojit
ke	k	k7c3	k
svým	svůj	k3xOyFgFnPc3	svůj
državám	država	k1gFnPc3	država
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Štýrsko	Štýrsko	k1gNnSc1	Štýrsko
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1270	[number]	k4	1270
také	také	k9	také
Korutany	Korutany	k1gInPc1	Korutany
<g/>
,	,	kIx,	,
Kraňsko	Kraňsko	k1gNnSc1	Kraňsko
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
enklávy	enkláva	k1gFnPc1	enkláva
ve	v	k7c6	v
Furlansku	Furlansko	k1gNnSc6	Furlansko
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgMnS	být
zastupován	zastupovat	k5eAaImNgMnS	zastupovat
místodržiteli	místodržitel	k1gMnPc7	místodržitel
-	-	kIx~	-
hejtmany	hejtman	k1gMnPc7	hejtman
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
většinou	většinou	k6eAd1	většinou
pocházeli	pocházet	k5eAaImAgMnP	pocházet
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
české	český	k2eAgFnSc2d1	Česká
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1276	[number]	k4	1276
byl	být	k5eAaImAgMnS	být
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
donucen	donutit	k5eAaPmNgMnS	donutit
své	svůj	k3xOyFgFnSc2	svůj
alpské	alpský	k2eAgFnSc2d1	alpská
země	zem	k1gFnSc2	zem
předat	předat	k5eAaPmF	předat
Rudolfu	Rudolf	k1gMnSc3	Rudolf
Habsburskému	habsburský	k2eAgMnSc3d1	habsburský
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1335	[number]	k4	1335
se	se	k3xPyFc4	se
v	v	k7c6	v
rukách	ruka	k1gFnPc6	ruka
Habsburků	Habsburk	k1gMnPc2	Habsburk
kumulovala	kumulovat	k5eAaImAgNnP	kumulovat
další	další	k2eAgNnPc1d1	další
území	území	k1gNnPc1	území
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc4	který
získali	získat	k5eAaPmAgMnP	získat
od	od	k7c2	od
gorických	gorický	k2eAgNnPc2d1	gorický
hrabat	hrabě	k1gNnPc2	hrabě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1382	[number]	k4	1382
se	se	k3xPyFc4	se
ze	z	k7c2	z
strachu	strach	k1gInSc2	strach
před	před	k7c4	před
Benátčany	Benátčan	k1gMnPc4	Benátčan
dalo	dát	k5eAaPmAgNnS	dát
pod	pod	k7c4	pod
ochranu	ochrana	k1gFnSc4	ochrana
Habsburků	Habsburk	k1gMnPc2	Habsburk
přístavní	přístavní	k2eAgNnSc1d1	přístavní
město	město	k1gNnSc1	město
Terst	Terst	k1gInSc1	Terst
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
habsburské	habsburský	k2eAgNnSc1d1	habsburské
panství	panství	k1gNnSc1	panství
zůstávalo	zůstávat	k5eAaImAgNnS	zůstávat
jen	jen	k9	jen
Gorické	Gorická	k1gFnSc2	Gorická
hrabství	hrabství	k1gNnSc2	hrabství
a	a	k8xC	a
města	město	k1gNnSc2	město
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
severní	severní	k2eAgFnSc2d1	severní
Istrie	Istrie	k1gFnSc2	Istrie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ovládala	ovládat	k5eAaImAgFnS	ovládat
Benátská	benátský	k2eAgFnSc1d1	Benátská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Konkurenty	konkurent	k1gMnPc4	konkurent
Habsburků	Habsburk	k1gInPc2	Habsburk
však	však	k9	však
byl	být	k5eAaImAgInS	být
rod	rod	k1gInSc4	rod
Celjských	Celjský	k2eAgMnPc2d1	Celjský
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
rod	rod	k1gInSc1	rod
německých	německý	k2eAgNnPc2d1	německé
hrabat	hrabě	k1gNnPc2	hrabě
pocházel	pocházet	k5eAaImAgInS	pocházet
původně	původně	k6eAd1	původně
z	z	k7c2	z
panství	panství	k1gNnSc2	panství
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
hradu	hrad	k1gInSc2	hrad
Žovnek	Žovnek	k1gInSc1	Žovnek
(	(	kIx(	(
<g/>
Sovneg	Sovneg	k1gInSc1	Sovneg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celjský	Celjský	k2eAgInSc1d1	Celjský
hrad	hrad	k1gInSc1	hrad
rod	rod	k1gInSc1	rod
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1341	[number]	k4	1341
<g/>
.	.	kIx.	.
</s>
<s>
Rodovým	rodový	k2eAgInSc7d1	rodový
znakem	znak	k1gInSc7	znak
Celjských	Celjský	k2eAgFnPc2d1	Celjská
byl	být	k5eAaImAgInS	být
modrý	modrý	k2eAgInSc1d1	modrý
štít	štít	k1gInSc1	štít
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
šesticípými	šesticípý	k2eAgFnPc7d1	šesticípá
zlatými	zlatý	k2eAgFnPc7d1	zlatá
hvězdami	hvězda	k1gFnPc7	hvězda
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
převzat	převzít	k5eAaPmNgInS	převzít
do	do	k7c2	do
současného	současný	k2eAgInSc2d1	současný
státního	státní	k2eAgInSc2d1	státní
znaku	znak	k1gInSc2	znak
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
čtrnáctého	čtrnáctý	k4xOgNnSc2	čtrnáctý
století	století	k1gNnSc2	století
zachránil	zachránit	k5eAaPmAgInS	zachránit
Herman	Herman	k1gMnSc1	Herman
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Celjský	Celjský	k2eAgInSc1d1	Celjský
Zikmunda	Zikmund	k1gMnSc2	Zikmund
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
před	před	k7c7	před
utopením	utopení	k1gNnSc7	utopení
v	v	k7c6	v
Dunaji	Dunaj	k1gInSc6	Dunaj
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
mocenský	mocenský	k2eAgInSc1d1	mocenský
vzestup	vzestup	k1gInSc1	vzestup
rodu	rod	k1gInSc2	rod
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1436	[number]	k4	1436
i	i	k8xC	i
povýšení	povýšení	k1gNnSc1	povýšení
do	do	k7c2	do
hodnosti	hodnost	k1gFnSc2	hodnost
říšských	říšský	k2eAgMnPc2d1	říšský
knížat	kníže	k1gMnPc2wR	kníže
<g/>
.	.	kIx.	.
</s>
<s>
Celjští	Celjský	k2eAgMnPc1d1	Celjský
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
panství	panství	k1gNnSc1	panství
se	se	k3xPyFc4	se
vyvíjelo	vyvíjet	k5eAaImAgNnS	vyvíjet
zcela	zcela	k6eAd1	zcela
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
Habsburcích	Habsburk	k1gInPc6	Habsburk
<g/>
,	,	kIx,	,
zamýšleli	zamýšlet	k5eAaImAgMnP	zamýšlet
svou	svůj	k3xOyFgFnSc7	svůj
sňatkovou	sňatkový	k2eAgFnSc7d1	sňatková
politikou	politika	k1gFnSc7	politika
propojit	propojit	k5eAaPmF	propojit
svůj	svůj	k3xOyFgInSc4	svůj
rod	rod	k1gInSc4	rod
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
rody	rod	k1gInPc7	rod
v	v	k7c6	v
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
Bosně	Bosna	k1gFnSc6	Bosna
a	a	k8xC	a
v	v	k7c6	v
Srbsku	Srbsko	k1gNnSc6	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
byl	být	k5eAaImAgInS	být
propojen	propojit	k5eAaPmNgInS	propojit
i	i	k8xC	i
s	s	k7c7	s
Lucemburky	Lucemburk	k1gMnPc7	Lucemburk
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
manželku	manželka	k1gFnSc4	manželka
vzal	vzít	k5eAaPmAgMnS	vzít
Hermanovu	Hermanův	k2eAgFnSc4d1	Hermanova
dceru	dcera	k1gFnSc4	dcera
Barbaru	Barbara	k1gFnSc4	Barbara
sám	sám	k3xTgMnSc1	sám
císař	císař	k1gMnSc1	císař
Zikmund	Zikmund	k1gMnSc1	Zikmund
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1443	[number]	k4	1443
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
Celjští	Celjský	k2eAgMnPc1d1	Celjský
s	s	k7c7	s
Habsburky	Habsburk	k1gMnPc7	Habsburk
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
na	na	k7c6	na
vzájemném	vzájemný	k2eAgNnSc6d1	vzájemné
dědictví	dědictví	k1gNnSc6	dědictví
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
rodů	rod	k1gInPc2	rod
vymře	vymřít	k5eAaPmIp3nS	vymřít
po	po	k7c6	po
meči	meč	k1gInSc6	meč
<g/>
.	.	kIx.	.
</s>
<s>
Ulrich	Ulrich	k1gMnSc1	Ulrich
II	II	kA	II
<g/>
.	.	kIx.	.
-	-	kIx~	-
poslední	poslední	k2eAgMnSc1d1	poslední
z	z	k7c2	z
Celjských	Celjský	k2eAgMnPc2d1	Celjský
-	-	kIx~	-
měl	mít	k5eAaImAgInS	mít
za	za	k7c4	za
manželku	manželka	k1gFnSc4	manželka
dceru	dcera	k1gFnSc4	dcera
srbského	srbský	k2eAgMnSc2d1	srbský
despoty	despota	k1gMnSc2	despota
Đorđe	Đorđ	k1gMnSc2	Đorđ
Brankoviće	Branković	k1gMnSc2	Branković
a	a	k8xC	a
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
patnáctého	patnáctý	k4xOgNnSc2	patnáctý
století	století	k1gNnSc2	století
byl	být	k5eAaImAgMnS	být
ustaven	ustavit	k5eAaPmNgMnS	ustavit
královským	královský	k2eAgMnSc7d1	královský
náměstkem	náměstek	k1gMnSc7	náměstek
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
<g/>
.	.	kIx.	.
</s>
<s>
Žárlivost	žárlivost	k1gFnSc1	žárlivost
maďarské	maďarský	k2eAgFnSc2d1	maďarská
šlechty	šlechta	k1gFnSc2	šlechta
vyústila	vyústit	k5eAaPmAgFnS	vyústit
v	v	k7c4	v
Ulrichovu	Ulrichův	k2eAgFnSc4d1	Ulrichova
vraždu	vražda	k1gFnSc4	vražda
při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
návštěvě	návštěva	k1gFnSc6	návštěva
uherského	uherský	k2eAgNnSc2d1	Uherské
hraničního	hraniční	k2eAgNnSc2d1	hraniční
města	město	k1gNnSc2	město
na	na	k7c6	na
Dunaji	Dunaj	k1gInSc6	Dunaj
Bělehradu	Bělehrad	k1gInSc2	Bělehrad
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1456	[number]	k4	1456
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
indicií	indicie	k1gFnPc2	indicie
stál	stát	k5eAaImAgInS	stát
za	za	k7c7	za
vraždou	vražda	k1gFnSc7	vražda
László	László	k1gFnSc2	László
Hunyadi	Hunyad	k1gMnPc1	Hunyad
<g/>
.	.	kIx.	.
</s>
<s>
Ulrichem	Ulrich	k1gMnSc7	Ulrich
vymřel	vymřít	k5eAaPmAgInS	vymřít
rod	rod	k1gInSc4	rod
Celjským	Celjský	k2eAgInSc7d1	Celjský
po	po	k7c6	po
meči	meč	k1gInSc6	meč
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gNnSc1	jejich
panství	panství	k1gNnSc1	panství
dostalo	dostat	k5eAaPmAgNnS	dostat
Ulrichovou	Ulrichův	k2eAgFnSc7d1	Ulrichova
vraždou	vražda	k1gFnSc7	vražda
do	do	k7c2	do
habsburských	habsburský	k2eAgFnPc2d1	habsburská
rukou	ruka	k1gFnPc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Definitivně	definitivně	k6eAd1	definitivně
Habsburkové	Habsburk	k1gMnPc1	Habsburk
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
slovinské	slovinský	k2eAgNnSc4d1	slovinské
etnické	etnický	k2eAgNnSc4d1	etnické
území	území	k1gNnSc4	území
-	-	kIx~	-
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Benátčany	Benátčan	k1gMnPc7	Benátčan
ovládaných	ovládaný	k2eAgNnPc2d1	ovládané
okrajových	okrajový	k2eAgNnPc2d1	okrajové
území	území	k1gNnPc2	území
v	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
Furlansku	Furlansko	k1gNnSc6	Furlansko
-	-	kIx~	-
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
šestnáctého	šestnáctý	k4xOgNnSc2	šestnáctý
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Vymřením	vymření	k1gNnSc7	vymření
zdejších	zdejší	k2eAgFnPc2d1	zdejší
dynastií	dynastie	k1gFnPc2	dynastie
a	a	k8xC	a
získáním	získání	k1gNnSc7	získání
jejich	jejich	k3xOp3gNnSc2	jejich
území	území	k1gNnSc2	území
se	se	k3xPyFc4	se
utvrdilo	utvrdit	k5eAaPmAgNnS	utvrdit
postavení	postavení	k1gNnSc1	postavení
habsburské	habsburský	k2eAgFnSc2d1	habsburská
dynastie	dynastie	k1gFnSc2	dynastie
v	v	k7c6	v
regionu	region	k1gInSc6	region
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1526	[number]	k4	1526
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jejich	jejich	k3xOp3gFnSc1	jejich
vláda	vláda	k1gFnSc1	vláda
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
již	již	k6eAd1	již
všechny	všechen	k3xTgFnPc4	všechen
slovinské	slovinský	k2eAgFnPc4d1	slovinská
země	zem	k1gFnPc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
tak	tak	k9	tak
bylo	být	k5eAaImAgNnS	být
po	po	k7c4	po
několik	několik	k4yIc4	několik
následujících	následující	k2eAgNnPc2d1	následující
století	století	k1gNnPc2	století
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
let	léto	k1gNnPc2	léto
1809	[number]	k4	1809
až	až	k6eAd1	až
1814	[number]	k4	1814
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnPc2	součást
Ilyrského	ilyrský	k2eAgNnSc2d1	ilyrské
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
Habsburků	Habsburk	k1gMnPc2	Habsburk
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
trpělo	trpět	k5eAaImAgNnS	trpět
zdejší	zdejší	k2eAgNnSc1d1	zdejší
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
intenzivními	intenzivní	k2eAgFnPc7d1	intenzivní
a	a	k8xC	a
často	často	k6eAd1	často
se	se	k3xPyFc4	se
opakujícími	opakující	k2eAgInPc7d1	opakující
loupeživými	loupeživý	k2eAgInPc7d1	loupeživý
nájezdy	nájezd	k1gInPc7	nájezd
osmanských	osmanský	k2eAgFnPc2d1	Osmanská
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vytvářelo	vytvářet	k5eAaImAgNnS	vytvářet
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
špatnými	špatný	k2eAgFnPc7d1	špatná
sociálními	sociální	k2eAgFnPc7d1	sociální
podmínkami	podmínka	k1gFnPc7	podmínka
<g/>
,	,	kIx,	,
předpoklady	předpoklad	k1gInPc7	předpoklad
k	k	k7c3	k
rolnickým	rolnický	k2eAgNnPc3d1	rolnické
povstáním	povstání	k1gNnPc3	povstání
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
těm	ten	k3xDgMnPc3	ten
pak	pak	k6eAd1	pak
opakovaně	opakovaně	k6eAd1	opakovaně
docházelo	docházet	k5eAaImAgNnS	docházet
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1473	[number]	k4	1473
až	až	k9	až
1478	[number]	k4	1478
a	a	k8xC	a
1515	[number]	k4	1515
<g/>
.	.	kIx.	.
</s>
<s>
Povstání	povstání	k1gNnSc1	povstání
bylo	být	k5eAaImAgNnS	být
vojensky	vojensky	k6eAd1	vojensky
potlačeno	potlačit	k5eAaPmNgNnS	potlačit
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc1	jeho
vůdcové	vůdce	k1gMnPc1	vůdce
popraveni	popravit	k5eAaPmNgMnP	popravit
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
nová	nový	k2eAgFnSc1d1	nová
daň	daň	k1gFnSc1	daň
-	-	kIx~	-
povstalecká	povstalecký	k2eAgFnSc1d1	povstalecká
dávka	dávka	k1gFnSc1	dávka
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
další	další	k2eAgInPc1d1	další
pokusy	pokus	k1gInPc1	pokus
o	o	k7c6	o
povstání	povstání	k1gNnSc6	povstání
byly	být	k5eAaImAgFnP	být
krvavě	krvavě	k6eAd1	krvavě
potlačeny	potlačen	k2eAgFnPc1d1	potlačena
<g/>
.	.	kIx.	.
</s>
<s>
Postavení	postavení	k1gNnSc1	postavení
Slovinců	Slovinec	k1gMnPc2	Slovinec
zlepšily	zlepšit	k5eAaPmAgFnP	zlepšit
reformy	reforma	k1gFnPc1	reforma
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
a	a	k8xC	a
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
existovala	existovat	k5eAaImAgFnS	existovat
na	na	k7c4	na
území	území	k1gNnSc4	území
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
korunní	korunní	k2eAgFnSc2d1	korunní
země	zem	k1gFnSc2	zem
Kraňsko	Kraňsko	k1gNnSc1	Kraňsko
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
části	část	k1gFnSc2	část
dalších	další	k2eAgFnPc2d1	další
korunních	korunní	k2eAgFnPc2d1	korunní
zemí	zem	k1gFnPc2	zem
<g/>
:	:	kIx,	:
Korutan	Korutany	k1gInPc2	Korutany
<g/>
,	,	kIx,	,
Štýrska	Štýrsko	k1gNnSc2	Štýrsko
<g/>
,	,	kIx,	,
Gorice	Gorice	k1gFnSc2	Gorice
a	a	k8xC	a
Gradišky	Gradiška	k1gFnSc2	Gradiška
<g/>
,	,	kIx,	,
Istrie	Istrie	k1gFnSc2	Istrie
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
Uherska	Uhersko	k1gNnSc2	Uhersko
(	(	kIx(	(
<g/>
Zámuří	Zámuří	k1gNnSc2	Zámuří
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
formovaly	formovat	k5eAaImAgFnP	formovat
moderní	moderní	k2eAgFnPc1d1	moderní
slovinské	slovinský	k2eAgFnPc1d1	slovinská
občanské	občanský	k2eAgFnPc1d1	občanská
síly	síla	k1gFnPc1	síla
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zahájení	zahájení	k1gNnSc6	zahájení
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
výjimečný	výjimečný	k2eAgInSc1d1	výjimečný
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zrušení	zrušení	k1gNnSc3	zrušení
řady	řada	k1gFnSc2	řada
politických	politický	k2eAgFnPc2d1	politická
svobod	svoboda	k1gFnPc2	svoboda
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
zpřísněna	zpřísněn	k2eAgFnSc1d1	zpřísněna
cenzura	cenzura	k1gFnSc1	cenzura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1917	[number]	k4	1917
přednesl	přednést	k5eAaPmAgMnS	přednést
na	na	k7c6	na
zasedání	zasedání	k1gNnSc6	zasedání
parlamentu	parlament	k1gInSc2	parlament
Anton	Anton	k1gMnSc1	Anton
Korošec	Korošec	k1gMnSc1	Korošec
Májovou	májový	k2eAgFnSc4d1	Májová
deklaraci	deklarace	k1gFnSc4	deklarace
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
všech	všecek	k3xTgMnPc2	všecek
22	[number]	k4	22
jihoslovanských	jihoslovanský	k2eAgMnPc2d1	jihoslovanský
poslanců	poslanec	k1gMnPc2	poslanec
vídeňského	vídeňský	k2eAgInSc2d1	vídeňský
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
požadovali	požadovat	k5eAaImAgMnP	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
samostatné	samostatný	k2eAgNnSc1d1	samostatné
státní	státní	k2eAgNnSc1d1	státní
těleso	těleso	k1gNnSc1	těleso
Slovinců	Slovinec	k1gMnPc2	Slovinec
<g/>
,	,	kIx,	,
Chorvatů	Chorvat	k1gMnPc2	Chorvat
a	a	k8xC	a
Srbů	Srb	k1gMnPc2	Srb
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
habsbursko-lotrinské	habsburskootrinský	k2eAgFnSc2d1	habsbursko-lotrinská
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1918	[number]	k4	1918
si	se	k3xPyFc3	se
již	jenž	k3xRgMnPc1	jenž
Slovinci	Slovinec	k1gMnPc1	Slovinec
uvědomili	uvědomit	k5eAaPmAgMnP	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
reforma	reforma	k1gFnSc1	reforma
Rakousko-Uherska	Rakousko-Uhersk	k1gInSc2	Rakousko-Uhersk
není	být	k5eNaImIp3nS	být
možná	možný	k2eAgFnSc1d1	možná
a	a	k8xC	a
že	že	k8xS	že
o	o	k7c6	o
budoucnosti	budoucnost	k1gFnSc6	budoucnost
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
mírová	mírový	k2eAgFnSc1d1	mírová
konference	konference	k1gFnSc1	konference
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
domáhat	domáhat	k5eAaImF	domáhat
práva	právo	k1gNnPc1	právo
na	na	k7c4	na
sebeurčení	sebeurčení	k1gNnSc4	sebeurčení
<g/>
.	.	kIx.	.
</s>
<s>
Rozchod	rozchod	k1gInSc1	rozchod
s	s	k7c7	s
habsburskou	habsburský	k2eAgFnSc7d1	habsburská
monarchií	monarchie	k1gFnSc7	monarchie
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
11	[number]	k4	11
<g/>
.	.	kIx.	.
říjnem	říjen	k1gInSc7	říjen
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
slovinská	slovinský	k2eAgFnSc1d1	slovinská
reprezentace	reprezentace	k1gFnSc1	reprezentace
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
možnost	možnost	k1gFnSc4	možnost
podílet	podílet	k5eAaImF	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
vídeňské	vídeňský	k2eAgFnSc6d1	Vídeňská
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Slovinci	Slovinec	k1gMnPc1	Slovinec
opustili	opustit	k5eAaPmAgMnP	opustit
habsburské	habsburský	k2eAgNnSc4d1	habsburské
soustátí	soustátí	k1gNnSc4	soustátí
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
Záhřebu	Záhřeb	k1gInSc6	Záhřeb
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Stát	stát	k1gInSc1	stát
Slovinců	Slovinec	k1gMnPc2	Slovinec
<g/>
,	,	kIx,	,
Chorvatů	Chorvat	k1gMnPc2	Chorvat
a	a	k8xC	a
Srbů	Srb	k1gMnPc2	Srb
(	(	kIx(	(
<g/>
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
1918	[number]	k4	1918
po	po	k7c6	po
spojení	spojení	k1gNnSc6	spojení
se	s	k7c7	s
Srbským	srbský	k2eAgNnSc7d1	srbské
královstvím	království	k1gNnSc7	království
Království	království	k1gNnSc2	království
Srbů	Srb	k1gMnPc2	Srb
<g/>
,	,	kIx,	,
Chorvatů	Chorvat	k1gMnPc2	Chorvat
a	a	k8xC	a
Slovinců	Slovinec	k1gMnPc2	Slovinec
<g/>
,	,	kIx,	,
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
1929	[number]	k4	1929
Království	království	k1gNnSc4	království
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rapallskou	Rapallský	k2eAgFnSc7d1	Rapallská
smlouvou	smlouva	k1gFnSc7	smlouva
z	z	k7c2	z
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1920	[number]	k4	1920
připadlo	připadnout	k5eAaPmAgNnS	připadnout
Přímoří	Přímoří	k1gNnSc1	Přímoří
<g/>
,	,	kIx,	,
Goricko	Goricko	k1gNnSc1	Goricko
<g/>
,	,	kIx,	,
Terst	Terst	k1gInSc1	Terst
<g/>
,	,	kIx,	,
Istrie	Istrie	k1gFnSc1	Istrie
a	a	k8xC	a
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
západního	západní	k2eAgNnSc2d1	západní
Korutanska	Korutansko	k1gNnSc2	Korutansko
(	(	kIx(	(
<g/>
Notraňsko	Notraňsko	k1gNnSc4	Notraňsko
<g/>
)	)	kIx)	)
Itálii	Itálie	k1gFnSc4	Itálie
<g/>
,	,	kIx,	,
plebiscitem	plebiscit	k1gInSc7	plebiscit
v	v	k7c6	v
Korutanech	Korutany	k1gInPc6	Korutany
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1920	[number]	k4	1920
připadly	připadnout	k5eAaPmAgInP	připadnout
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgInPc1d1	celý
Korutany	Korutany	k1gInPc1	Korutany
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
několik	několik	k4yIc1	několik
slovinských	slovinský	k2eAgFnPc2d1	slovinská
vesnic	vesnice	k1gFnPc2	vesnice
v	v	k7c6	v
Prekomurji	Prekomurj	k1gFnSc6	Prekomurj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
okupaci	okupace	k1gFnSc6	okupace
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1941	[number]	k4	1941
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
mezi	mezi	k7c4	mezi
Německo	Německo	k1gNnSc4	Německo
(	(	kIx(	(
<g/>
severovýchod	severovýchod	k1gInSc1	severovýchod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Itálii	Itálie	k1gFnSc4	Itálie
(	(	kIx(	(
<g/>
jihozápad	jihozápad	k1gInSc4	jihozápad
<g/>
)	)	kIx)	)
a	a	k8xC	a
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
(	(	kIx(	(
<g/>
Prekomurje	Prekomurje	k1gFnSc1	Prekomurje
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
okupační	okupační	k2eAgFnSc6d1	okupační
zóně	zóna	k1gFnSc6	zóna
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
násilné	násilný	k2eAgFnSc3d1	násilná
germanizaci	germanizace	k1gFnSc3	germanizace
a	a	k8xC	a
vysidlování	vysidlování	k1gNnSc1	vysidlování
etnických	etnický	k2eAgMnPc2d1	etnický
Slovinců	Slovinec	k1gMnPc2	Slovinec
<g/>
.	.	kIx.	.
</s>
<s>
Italská	italský	k2eAgFnSc1d1	italská
vláda	vláda	k1gFnSc1	vláda
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
své	své	k1gNnSc4	své
části	část	k1gFnSc2	část
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
částečnou	částečný	k2eAgFnSc4d1	částečná
autonomii	autonomie	k1gFnSc4	autonomie
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
slovinské	slovinský	k2eAgFnSc2d1	slovinská
politické	politický	k2eAgFnSc2d1	politická
elity	elita	k1gFnSc2	elita
pak	pak	k6eAd1	pak
s	s	k7c7	s
Italy	Ital	k1gMnPc7	Ital
kolaborovala	kolaborovat	k5eAaImAgFnS	kolaborovat
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1941	[number]	k4	1941
začalo	začít	k5eAaPmAgNnS	začít
ozbrojené	ozbrojený	k2eAgNnSc1d1	ozbrojené
povstání	povstání	k1gNnSc1	povstání
proti	proti	k7c3	proti
okupantům	okupant	k1gMnPc3	okupant
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
subjektem	subjekt	k1gInSc7	subjekt
národněosvobozeneckého	národněosvobozenecký	k2eAgInSc2d1	národněosvobozenecký
boje	boj	k1gInSc2	boj
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Osvobozenecká	osvobozenecký	k2eAgFnSc1d1	osvobozenecká
fronta	fronta	k1gFnSc1	fronta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
byla	být	k5eAaImAgFnS	být
dolomitským	dolomitský	k2eAgNnSc7d1	dolomitské
prohlášením	prohlášení	k1gNnSc7	prohlášení
formálně	formálně	k6eAd1	formálně
uznána	uznán	k2eAgFnSc1d1	uznána
role	role	k1gFnSc1	role
komunistů	komunista	k1gMnPc2	komunista
jako	jako	k8xS	jako
vůdců	vůdce	k1gMnPc2	vůdce
odboje	odboj	k1gInSc2	odboj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1943	[number]	k4	1943
se	se	k3xPyFc4	se
Antifašistická	antifašistický	k2eAgFnSc1d1	Antifašistická
rada	rada	k1gFnSc1	rada
národního	národní	k2eAgNnSc2d1	národní
osvobození	osvobození	k1gNnSc2	osvobození
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
usnesla	usnést	k5eAaPmAgFnS	usnést
o	o	k7c6	o
obnovení	obnovení	k1gNnSc6	obnovení
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
na	na	k7c6	na
federálním	federální	k2eAgInSc6d1	federální
základě	základ	k1gInSc6	základ
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
federálních	federální	k2eAgFnPc2d1	federální
jednotek	jednotka	k1gFnPc2	jednotka
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
stát	stát	k5eAaPmF	stát
i	i	k9	i
Slovinsko	Slovinsko	k1gNnSc4	Slovinsko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
se	se	k3xPyFc4	se
začali	začít	k5eAaPmAgMnP	začít
komunisté	komunista	k1gMnPc1	komunista
připravovat	připravovat	k5eAaImF	připravovat
na	na	k7c4	na
převzetí	převzetí	k1gNnSc4	převzetí
politické	politický	k2eAgFnSc2d1	politická
moci	moc	k1gFnSc2	moc
v	v	k7c6	v
poválečném	poválečný	k2eAgNnSc6d1	poválečné
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
návratu	návrat	k1gInSc3	návrat
vícestranického	vícestranický	k2eAgInSc2d1	vícestranický
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c4	o
vytvoření	vytvoření	k1gNnSc4	vytvoření
lidové	lidový	k2eAgFnSc2d1	lidová
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
bude	být	k5eAaImBp3nS	být
právě	právě	k6eAd1	právě
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
komunistů	komunista	k1gMnPc2	komunista
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1944	[number]	k4	1944
se	se	k3xPyFc4	se
v	v	k7c6	v
Črnomelju	Črnomelju	k1gFnSc6	Črnomelju
Slovinský	slovinský	k2eAgInSc1d1	slovinský
národněosvobozenecký	národněosvobozenecký	k2eAgInSc1d1	národněosvobozenecký
výbor	výbor	k1gInSc1	výbor
přejmenoval	přejmenovat	k5eAaPmAgInS	přejmenovat
na	na	k7c4	na
Slovinskou	slovinský	k2eAgFnSc4d1	slovinská
národněosvobozeneckou	národněosvobozenecký	k2eAgFnSc4d1	národněosvobozenecký
radu	rada	k1gFnSc4	rada
a	a	k8xC	a
současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
za	za	k7c4	za
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
zákonodárný	zákonodárný	k2eAgInSc4d1	zákonodárný
a	a	k8xC	a
zastupitelský	zastupitelský	k2eAgInSc4d1	zastupitelský
orgán	orgán	k1gInSc4	orgán
ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
bylo	být	k5eAaImAgNnS	být
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
uznáno	uznán	k2eAgNnSc1d1	uznáno
za	za	k7c4	za
státní	státní	k2eAgInSc4d1	státní
útvar	útvar	k1gInSc4	útvar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
získalo	získat	k5eAaPmAgNnS	získat
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
část	část	k1gFnSc1	část
západních	západní	k2eAgNnPc2d1	západní
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
připadla	připadnout	k5eAaPmAgFnS	připadnout
Itálii	Itálie	k1gFnSc4	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
mezi	mezi	k7c4	mezi
Itálii	Itálie	k1gFnSc4	Itálie
a	a	k8xC	a
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
i	i	k8xC	i
sporné	sporný	k2eAgNnSc1d1	sporné
území	území	k1gNnSc1	území
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Terstu	Terst	k1gInSc2	Terst
<g/>
.	.	kIx.	.
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jugoslávské	jugoslávský	k2eAgFnSc2d1	jugoslávská
federace	federace	k1gFnSc2	federace
bylo	být	k5eAaImAgNnS	být
nejprve	nejprve	k6eAd1	nejprve
lidovou	lidový	k2eAgFnSc7d1	lidová
republikou	republika	k1gFnSc7	republika
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
objevovat	objevovat	k5eAaImF	objevovat
rozpory	rozpor	k1gInPc4	rozpor
s	s	k7c7	s
centrem	centrum	k1gNnSc7	centrum
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Silniční	silniční	k2eAgFnSc1d1	silniční
aféra	aféra	k1gFnSc1	aféra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
skončily	skončit	k5eAaPmAgFnP	skončit
odstavením	odstavení	k1gNnSc7	odstavení
reformních	reformní	k2eAgMnPc2d1	reformní
komunistů	komunista	k1gMnPc2	komunista
(	(	kIx(	(
<g/>
Stane	stanout	k5eAaPmIp3nS	stanout
Kavčič	Kavčič	k1gMnSc1	Kavčič
<g/>
)	)	kIx)	)
a	a	k8xC	a
upevněním	upevnění	k1gNnSc7	upevnění
pozic	pozice	k1gFnPc2	pozice
konzervativců	konzervativec	k1gMnPc2	konzervativec
<g/>
.	.	kIx.	.
</s>
<s>
Rozklad	rozklad	k1gInSc1	rozklad
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
začal	začít	k5eAaPmAgInS	začít
smrtí	smrt	k1gFnSc7	smrt
maršála	maršál	k1gMnSc2	maršál
Tita	Titus	k1gMnSc2	Titus
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1987	[number]	k4	1987
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
další	další	k2eAgNnSc1d1	další
číslo	číslo	k1gNnSc1	číslo
časopisu	časopis	k1gInSc2	časopis
Nova	nova	k1gFnSc1	nova
revija	revija	k1gFnSc1	revija
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
byly	být	k5eAaImAgInP	být
zveřejněny	zveřejnit	k5eAaPmNgInP	zveřejnit
příspěvky	příspěvek	k1gInPc1	příspěvek
France	Franc	k1gMnSc2	Franc
Bučara	Bučar	k1gMnSc2	Bučar
<g/>
,	,	kIx,	,
Petera	Peter	k1gMnSc2	Peter
Jambreka	Jambreek	k1gMnSc2	Jambreek
<g/>
,	,	kIx,	,
Tine	Tin	k1gMnSc2	Tin
Hribara	Hribar	k1gMnSc2	Hribar
<g/>
,	,	kIx,	,
Ivana	Ivan	k1gMnSc2	Ivan
Urbančiče	Urbančič	k1gInSc2	Urbančič
a	a	k8xC	a
Jože	Joža	k1gFnSc6	Joža
Pučnika	Pučnik	k1gMnSc2	Pučnik
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
napadli	napadnout	k5eAaPmAgMnP	napadnout
vedoucí	vedoucí	k1gMnPc1	vedoucí
úlohu	úloha	k1gFnSc4	úloha
komunistů	komunista	k1gMnPc2	komunista
<g/>
,	,	kIx,	,
centralistické	centralistický	k2eAgNnSc1d1	centralistické
vedení	vedení	k1gNnSc1	vedení
jugoslávské	jugoslávský	k2eAgFnSc2d1	jugoslávská
politiky	politika	k1gFnSc2	politika
a	a	k8xC	a
formulovali	formulovat	k5eAaImAgMnP	formulovat
cíl	cíl	k1gInSc4	cíl
slovinského	slovinský	k2eAgInSc2d1	slovinský
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
samostatný	samostatný	k2eAgInSc1d1	samostatný
slovinský	slovinský	k2eAgInSc1d1	slovinský
stát	stát	k1gInSc1	stát
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
jugoslávské	jugoslávský	k2eAgFnSc2d1	jugoslávská
konfederace	konfederace	k1gFnSc2	konfederace
<g/>
.	.	kIx.	.
</s>
<s>
Následný	následný	k2eAgInSc1d1	následný
demokratizační	demokratizační	k2eAgInSc1d1	demokratizační
proces	proces	k1gInSc1	proces
prováděný	prováděný	k2eAgInSc1d1	prováděný
především	především	k9	především
formou	forma	k1gFnSc7	forma
ústavních	ústavní	k2eAgInPc2d1	ústavní
dodatků	dodatek	k1gInPc2	dodatek
realizovali	realizovat	k5eAaBmAgMnP	realizovat
sami	sám	k3xTgMnPc1	sám
slovinští	slovinský	k2eAgMnPc1d1	slovinský
komunisté	komunista	k1gMnPc1	komunista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1989	[number]	k4	1989
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
slovinská	slovinský	k2eAgFnSc1d1	slovinská
opozice	opozice	k1gFnSc1	opozice
Májovou	májový	k2eAgFnSc4d1	Májová
deklaraci	deklarace	k1gFnSc4	deklarace
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
požadovala	požadovat	k5eAaImAgFnS	požadovat
ustavení	ustavení	k1gNnSc3	ustavení
suverénního	suverénní	k2eAgInSc2d1	suverénní
slovinského	slovinský	k2eAgInSc2d1	slovinský
státu	stát	k1gInSc2	stát
a	a	k8xC	a
svobodu	svoboda	k1gFnSc4	svoboda
rozhodování	rozhodování	k1gNnSc2	rozhodování
o	o	k7c6	o
vnějších	vnější	k2eAgInPc6d1	vnější
svazcích	svazek	k1gInPc6	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Komunisté	komunista	k1gMnPc1	komunista
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
vydali	vydat	k5eAaPmAgMnP	vydat
Základní	základní	k2eAgFnSc4d1	základní
listinu	listina	k1gFnSc4	listina
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
také	také	k9	také
hovořila	hovořit	k5eAaImAgFnS	hovořit
o	o	k7c6	o
suverenitě	suverenita	k1gFnSc6	suverenita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlásila	hlásit	k5eAaImAgFnS	hlásit
se	se	k3xPyFc4	se
také	také	k9	také
k	k	k7c3	k
reformované	reformovaný	k2eAgFnSc3d1	reformovaná
jugoslávské	jugoslávský	k2eAgFnSc3d1	jugoslávská
federaci	federace	k1gFnSc3	federace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1990	[number]	k4	1990
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
první	první	k4xOgFnPc1	první
poválečné	poválečný	k2eAgFnPc1d1	poválečná
svobodné	svobodný	k2eAgFnPc1d1	svobodná
volby	volba	k1gFnPc1	volba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
přinesly	přinést	k5eAaPmAgFnP	přinést
vítězství	vítězství	k1gNnSc4	vítězství
opozičního	opoziční	k2eAgInSc2d1	opoziční
bloku	blok	k1gInSc2	blok
DEMOS	DEMOS	kA	DEMOS
v	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
a	a	k8xC	a
Milana	Milan	k1gMnSc2	Milan
Kučana	Kučan	k1gMnSc2	Kučan
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
předsedy	předseda	k1gMnSc2	předseda
Předsednictva	předsednictvo	k1gNnSc2	předsednictvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosincovém	prosincový	k2eAgNnSc6d1	prosincové
referendu	referendum	k1gNnSc6	referendum
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
hlasujících	hlasující	k1gMnPc2	hlasující
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
pro	pro	k7c4	pro
samostatnost	samostatnost	k1gFnSc4	samostatnost
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislost	nezávislost	k1gFnSc1	nezávislost
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
na	na	k7c4	na
což	což	k3yRnSc4	což
okamžitě	okamžitě	k6eAd1	okamžitě
reagovala	reagovat	k5eAaBmAgFnS	reagovat
Jugoslávská	jugoslávský	k2eAgFnSc1d1	jugoslávská
lidová	lidový	k2eAgFnSc1d1	lidová
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
patronací	patronace	k1gFnSc7	patronace
Evropských	evropský	k2eAgNnPc2d1	Evropské
společenství	společenství	k1gNnPc2	společenství
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
sjednána	sjednat	k5eAaPmNgFnS	sjednat
Brionská	Brionský	k2eAgFnSc1d1	Brionská
deklarace	deklarace	k1gFnSc1	deklarace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
znamenala	znamenat	k5eAaImAgFnS	znamenat
ukončení	ukončení	k1gNnSc4	ukončení
bojů	boj	k1gInPc2	boj
ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
tříměsíčního	tříměsíční	k2eAgNnSc2d1	tříměsíční
moratoria	moratorium	k1gNnSc2	moratorium
na	na	k7c4	na
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uplynutí	uplynutí	k1gNnSc6	uplynutí
moratoria	moratorium	k1gNnSc2	moratorium
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
slovinská	slovinský	k2eAgFnSc1d1	slovinská
skupščina	skupščin	k2eAgFnSc1d1	skupščin
nezávislost	nezávislost	k1gFnSc1	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1992	[number]	k4	1992
uznala	uznat	k5eAaPmAgFnS	uznat
Slovinsko	Slovinsko	k1gNnSc4	Slovinsko
většina	většina	k1gFnSc1	většina
členů	člen	k1gMnPc2	člen
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Samostatnost	samostatnost	k1gFnSc1	samostatnost
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
přinesla	přinést	k5eAaPmAgFnS	přinést
spory	spor	k1gInPc4	spor
s	s	k7c7	s
Chorvatskem	Chorvatsko	k1gNnSc7	Chorvatsko
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Krško	Krška	k1gMnSc5	Krška
a	a	k8xC	a
hranic	hranice	k1gFnPc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
elektrárna	elektrárna	k1gFnSc1	elektrárna
byla	být	k5eAaImAgFnS	být
budována	budovat	k5eAaImNgFnS	budovat
z	z	k7c2	z
finančních	finanční	k2eAgInPc2d1	finanční
prostředků	prostředek	k1gInPc2	prostředek
obou	dva	k4xCgFnPc2	dva
republik	republika	k1gFnPc2	republika
a	a	k8xC	a
tento	tento	k3xDgInSc4	tento
problém	problém	k1gInSc4	problém
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
vyřešit	vyřešit	k5eAaPmF	vyřešit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1993	[number]	k4	1993
až	až	k9	až
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Hraniční	hraniční	k2eAgInSc1d1	hraniční
spor	spor	k1gInSc1	spor
se	se	k3xPyFc4	se
týkal	týkat	k5eAaImAgInS	týkat
především	především	k9	především
přístupu	přístup	k1gInSc2	přístup
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
do	do	k7c2	do
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
vod	voda	k1gFnPc2	voda
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pobřežní	pobřežní	k2eAgInSc1d1	pobřežní
pás	pás	k1gInSc1	pás
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
od	od	k7c2	od
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
vod	voda	k1gFnPc2	voda
odloučil	odloučit	k5eAaPmAgInS	odloučit
slovinské	slovinský	k2eAgInPc4d1	slovinský
přístavy	přístav	k1gInPc4	přístav
Koper	kopra	k1gFnPc2	kopra
<g/>
,	,	kIx,	,
Izola	Izola	k1gFnSc1	Izola
<g/>
,	,	kIx,	,
Portorož	Portorož	k1gFnSc1	Portorož
a	a	k8xC	a
Piran	Piran	k1gInSc1	Piran
<g/>
.	.	kIx.	.
</s>
<s>
Hraniční	hraniční	k2eAgInPc1d1	hraniční
spory	spor	k1gInPc1	spor
stále	stále	k6eAd1	stále
nejsou	být	k5eNaImIp3nP	být
definitivně	definitivně	k6eAd1	definitivně
vyřešeny	vyřešen	k2eAgFnPc1d1	vyřešena
(	(	kIx(	(
<g/>
květen	květen	k1gInSc1	květen
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spory	spor	k1gInPc1	spor
vyvstaly	vyvstat	k5eAaPmAgInP	vyvstat
také	také	k9	také
ve	v	k7c6	v
vztazích	vztah	k1gInPc6	vztah
s	s	k7c7	s
Itálií	Itálie	k1gFnSc7	Itálie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zpochybnila	zpochybnit	k5eAaPmAgFnS	zpochybnit
některé	některý	k3yIgInPc4	některý
s	s	k7c7	s
bývalou	bývalý	k2eAgFnSc7d1	bývalá
Jugoslávií	Jugoslávie	k1gFnSc7	Jugoslávie
uzavřené	uzavřený	k2eAgFnSc2d1	uzavřená
dohody	dohoda	k1gFnSc2	dohoda
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
týkaly	týkat	k5eAaImAgInP	týkat
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
po	po	k7c6	po
přiřčení	přiřčení	k1gNnSc6	přiřčení
území	území	k1gNnSc2	území
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Terstu	Terst	k1gInSc2	Terst
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
přišly	přijít	k5eAaPmAgFnP	přijít
o	o	k7c4	o
svůj	svůj	k3xOyFgInSc4	svůj
majetek	majetek	k1gInSc4	majetek
v	v	k7c6	v
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
těchto	tento	k3xDgInPc2	tento
sporů	spor	k1gInPc2	spor
byla	být	k5eAaImAgFnS	být
blokace	blokace	k1gFnSc1	blokace
slovinských	slovinský	k2eAgInPc2d1	slovinský
přístupových	přístupový	k2eAgInPc2d1	přístupový
rozhovorů	rozhovor	k1gInPc2	rozhovor
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
přidružení	přidružení	k1gNnSc6	přidružení
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
k	k	k7c3	k
EU	EU	kA	EU
byla	být	k5eAaImAgFnS	být
podepsána	podepsat	k5eAaPmNgFnS	podepsat
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
když	když	k8xS	když
Italové	Ital	k1gMnPc1	Ital
akceptovali	akceptovat	k5eAaBmAgMnP	akceptovat
slovinské	slovinský	k2eAgNnSc4d1	slovinské
řešení	řešení	k1gNnSc4	řešení
-	-	kIx~	-
přijetí	přijetí	k1gNnSc4	přijetí
zákona	zákon	k1gInSc2	zákon
umožňujícího	umožňující	k2eAgInSc2d1	umožňující
občanům	občan	k1gMnPc3	občan
EU	EU	kA	EU
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
po	po	k7c6	po
ratifikaci	ratifikace	k1gFnSc6	ratifikace
vstoupit	vstoupit	k5eAaPmF	vstoupit
na	na	k7c4	na
trh	trh	k1gInSc4	trh
s	s	k7c7	s
nemovitostmi	nemovitost	k1gFnPc7	nemovitost
<g/>
.	.	kIx.	.
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
se	s	k7c7	s
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2004	[number]	k4	2004
stalo	stát	k5eAaPmAgNnS	stát
členským	členský	k2eAgInSc7d1	členský
státem	stát	k1gInSc7	stát
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
nahradili	nahradit	k5eAaPmAgMnP	nahradit
Slovinci	Slovinec	k1gMnPc1	Slovinec
svou	svůj	k3xOyFgFnSc4	svůj
národní	národní	k2eAgFnSc4d1	národní
měnu	měna	k1gFnSc4	měna
tolar	tolar	k1gInSc1	tolar
eurem	euro	k1gNnSc7	euro
<g/>
.	.	kIx.	.
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
přístup	přístup	k1gInSc4	přístup
ke	k	k7c3	k
Středozemnímu	středozemní	k2eAgNnSc3d1	středozemní
moři	moře	k1gNnSc3	moře
a	a	k8xC	a
na	na	k7c6	na
podstatné	podstatný	k2eAgFnSc6d1	podstatná
části	část	k1gFnSc6	část
jeho	jeho	k3xOp3gFnPc2	jeho
severních	severní	k2eAgFnPc2d1	severní
hranic	hranice	k1gFnPc2	hranice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Alpy	alpa	k1gFnSc2	alpa
<g/>
,	,	kIx,	,
jmenovitě	jmenovitě	k6eAd1	jmenovitě
horské	horský	k2eAgFnPc1d1	horská
skupiny	skupina	k1gFnPc1	skupina
Julské	Julský	k2eAgFnPc1d1	Julský
Alpy	Alpy	k1gFnPc1	Alpy
<g/>
,	,	kIx,	,
Kamnicko-Savinjské	Kamnicko-Savinjský	k2eAgFnPc1d1	Kamnicko-Savinjský
Alpy	Alpy	k1gFnPc1	Alpy
<g/>
,	,	kIx,	,
Karavanky	Karavanky	k1gFnPc1	Karavanky
a	a	k8xC	a
Pohorje	Pohorj	k1gFnPc1	Pohorj
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
pobřeží	pobřeží	k1gNnSc1	pobřeží
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgNnSc1d1	ležící
u	u	k7c2	u
Jaderského	jaderský	k2eAgNnSc2d1	Jaderské
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
přibližně	přibližně	k6eAd1	přibližně
47	[number]	k4	47
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Itálií	Itálie	k1gFnSc7	Itálie
k	k	k7c3	k
Chorvatsku	Chorvatsko	k1gNnSc3	Chorvatsko
<g/>
.	.	kIx.	.
</s>
<s>
Jihozápadní	jihozápadní	k2eAgFnPc1d1	jihozápadní
části	část	k1gFnPc1	část
země	zem	k1gFnSc2	zem
vévodí	vévodit	k5eAaImIp3nP	vévodit
krasy	kras	k1gInPc1	kras
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgMnPc2	jenž
je	být	k5eAaImIp3nS	být
registrováno	registrovat	k5eAaBmNgNnS	registrovat
necelých	celý	k2eNgInPc2d1	necelý
6,6	[number]	k4	6,6
tisíce	tisíc	k4xCgInSc2	tisíc
<g/>
,	,	kIx,	,
s	s	k7c7	s
podvodními	podvodní	k2eAgFnPc7d1	podvodní
řekami	řeka	k1gFnPc7	řeka
<g/>
,	,	kIx,	,
roklinami	roklina	k1gFnPc7	roklina
a	a	k8xC	a
jeskyněmi	jeskyně	k1gFnPc7	jeskyně
<g/>
,	,	kIx,	,
rozkládající	rozkládající	k2eAgMnSc1d1	rozkládající
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
Středozemním	středozemní	k2eAgNnSc7d1	středozemní
mořem	moře	k1gNnSc7	moře
a	a	k8xC	a
Lublaní	Lublaň	k1gFnSc7	Lublaň
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
východ	východ	k1gInSc1	východ
a	a	k8xC	a
severovýchod	severovýchod	k1gInSc1	severovýchod
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
<g/>
,	,	kIx,	,
při	při	k7c6	při
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Chorvatskem	Chorvatsko	k1gNnSc7	Chorvatsko
a	a	k8xC	a
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
takřka	takřka	k6eAd1	takřka
rovný	rovný	k2eAgMnSc1d1	rovný
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
podstatná	podstatný	k2eAgFnSc1d1	podstatná
část	část	k1gFnSc1	část
slovinského	slovinský	k2eAgNnSc2d1	slovinské
území	území	k1gNnSc2	území
kopcovitá	kopcovitý	k2eAgFnSc1d1	kopcovitá
až	až	k6eAd1	až
hornatá	hornatý	k2eAgFnSc1d1	hornatá
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
90	[number]	k4	90
%	%	kIx~	%
povrchu	povrch	k1gInSc2	povrch
leží	ležet	k5eAaImIp3nS	ležet
nad	nad	k7c4	nad
200	[number]	k4	200
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
se	se	k3xPyFc4	se
střetávají	střetávat	k5eAaImIp3nP	střetávat
čtyři	čtyři	k4xCgInPc1	čtyři
významné	významný	k2eAgInPc1d1	významný
evropské	evropský	k2eAgInPc1d1	evropský
geografické	geografický	k2eAgInPc1d1	geografický
regiony	region	k1gInPc1	region
-	-	kIx~	-
Alpy	Alpy	k1gFnPc1	Alpy
<g/>
,	,	kIx,	,
Dinárské	dinárský	k2eAgFnPc1d1	Dinárská
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
Panonská	panonský	k2eAgFnSc1d1	Panonská
pánev	pánev	k1gFnSc1	pánev
a	a	k8xC	a
Středomoří	středomoří	k1gNnSc1	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
je	být	k5eAaImIp3nS	být
Triglav	Triglav	k1gInSc1	Triglav
se	s	k7c7	s
2864	[number]	k4	2864
metry	metr	k1gInPc1	metr
<g/>
,	,	kIx,	,
průměrná	průměrný	k2eAgFnSc1d1	průměrná
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
činí	činit	k5eAaImIp3nS	činit
556,8	[number]	k4	556,8
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
leží	ležet	k5eAaImIp3nS	ležet
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
Jaderského	jaderský	k2eAgNnSc2d1	Jaderské
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
podstatná	podstatný	k2eAgFnSc1d1	podstatná
část	část	k1gFnSc1	část
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
poloviny	polovina	k1gFnSc2	polovina
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
11	[number]	k4	11
861	[number]	k4	861
km2	km2	k4	km2
<g/>
)	)	kIx)	)
zabírají	zabírat	k5eAaImIp3nP	zabírat
lesy	les	k1gInPc4	les
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
činí	činit	k5eAaImIp3nS	činit
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
<g/>
,	,	kIx,	,
po	po	k7c6	po
Finsku	Finsko	k1gNnSc6	Finsko
a	a	k8xC	a
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
,	,	kIx,	,
třetím	třetí	k4xOgInSc7	třetí
nejzalesněnějším	zalesněný	k2eAgInSc7d3	zalesněný
státem	stát	k1gInSc7	stát
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
slovinském	slovinský	k2eAgNnSc6d1	slovinské
území	území	k1gNnSc6	území
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
nalézt	nalézt	k5eAaBmF	nalézt
i	i	k9	i
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
pralesů	prales	k1gInPc2	prales
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Kočevje	Kočevj	k1gInSc2	Kočevj
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
plocha	plocha	k1gFnSc1	plocha
zabírá	zabírat	k5eAaImIp3nS	zabírat
přibližně	přibližně	k6eAd1	přibližně
4	[number]	k4	4
685	[number]	k4	685
km2	km2	k4	km2
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
orná	orný	k2eAgFnSc1d1	orná
půda	půda	k1gFnSc1	půda
cca	cca	kA	cca
1	[number]	k4	1
751	[number]	k4	751
km2	km2	k4	km2
<g/>
,	,	kIx,	,
trvalý	trvalý	k2eAgInSc1d1	trvalý
travní	travní	k2eAgInSc1d1	travní
porost	porost	k1gInSc1	porost
2	[number]	k4	2
673	[number]	k4	673
km2	km2	k4	km2
a	a	k8xC	a
zbývajících	zbývající	k2eAgInPc2d1	zbývající
260	[number]	k4	260
km2	km2	k4	km2
jsou	být	k5eAaImIp3nP	být
trvalé	trvalý	k2eAgFnPc1d1	trvalá
kultury	kultura	k1gFnPc1	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
nachází	nacházet	k5eAaImIp3nS	nacházet
161	[number]	k4	161
km2	km2	k4	km2
vinic	vinice	k1gFnPc2	vinice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
je	být	k5eAaImIp3nS	být
podnebí	podnebí	k1gNnSc4	podnebí
alpské	alpský	k2eAgFnSc2d1	alpská
<g/>
,	,	kIx,	,
kontinentální	kontinentální	k2eAgFnSc2d1	kontinentální
a	a	k8xC	a
středomořské	středomořský	k2eAgFnSc2d1	středomořská
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
má	mít	k5eAaImIp3nS	mít
kontinentální	kontinentální	k2eAgNnSc1d1	kontinentální
podnebí	podnebí	k1gNnSc1	podnebí
<g/>
,	,	kIx,	,
průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
-2	-2	k4	-2
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
a	a	k8xC	a
21	[number]	k4	21
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Prezident	prezident	k1gMnSc1	prezident
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
představitelem	představitel	k1gMnSc7	představitel
Republiky	republika	k1gFnSc2	republika
Slovinsko	Slovinsko	k1gNnSc4	Slovinsko
a	a	k8xC	a
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
Republiky	republika	k1gFnSc2	republika
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnSc1	funkce
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
neslučitelná	slučitelný	k2eNgFnSc1d1	neslučitelná
s	s	k7c7	s
výkonem	výkon	k1gInSc7	výkon
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
veřejné	veřejný	k2eAgFnSc2d1	veřejná
funkce	funkce	k1gFnSc2	funkce
či	či	k8xC	či
povolání	povolání	k1gNnSc2	povolání
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
účinnosti	účinnost	k1gFnSc2	účinnost
ústavy	ústava	k1gFnSc2	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
mělo	mít	k5eAaImAgNnS	mít
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
kolektivní	kolektivní	k2eAgFnSc4d1	kolektivní
hlavu	hlava	k1gFnSc4	hlava
státu	stát	k1gInSc2	stát
-	-	kIx~	-
Předsednictvo	předsednictvo	k1gNnSc1	předsednictvo
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
prezidentské	prezidentský	k2eAgFnPc1d1	prezidentská
volby	volba	k1gFnPc1	volba
se	se	k3xPyFc4	se
uskutečnily	uskutečnit	k5eAaPmAgFnP	uskutečnit
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
a	a	k8xC	a
jejich	jejich	k3xOp3gMnSc7	jejich
vítězem	vítěz	k1gMnSc7	vítěz
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Milan	Milan	k1gMnSc1	Milan
Kučan	Kučan	k1gMnSc1	Kučan
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
již	již	k6eAd1	již
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
1990	[number]	k4	1990
vykonával	vykonávat	k5eAaImAgInS	vykonávat
funkci	funkce	k1gFnSc4	funkce
předsedy	předseda	k1gMnSc2	předseda
Předsednictva	předsednictvo	k1gNnSc2	předsednictvo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1945	[number]	k4	1945
až	až	k9	až
1974	[number]	k4	1974
předseda	předseda	k1gMnSc1	předseda
Skupščiny	Skupščin	k2eAgFnSc2d1	Skupščin
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1974	[number]	k4	1974
až	až	k9	až
1992	[number]	k4	1992
Předsednictvo	předsednictvo	k1gNnSc1	předsednictvo
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
jeho	jeho	k3xOp3gMnSc1	jeho
předseda	předseda	k1gMnSc1	předseda
<g/>
,	,	kIx,	,
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
prezident	prezident	k1gMnSc1	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
slovinským	slovinský	k2eAgMnSc7d1	slovinský
prezidentem	prezident	k1gMnSc7	prezident
je	být	k5eAaImIp3nS	být
Borut	Borut	k1gMnSc1	Borut
Pahor	Pahor	k?	Pahor
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Zákonodárné	zákonodárný	k2eAgInPc1d1	zákonodárný
sbory	sbor	k1gInPc1	sbor
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
a	a	k8xC	a
Vláda	vláda	k1gFnSc1	vláda
Republiky	republika	k1gFnSc2	republika
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
<g/>
.	.	kIx.	.
</s>
<s>
Zákonodárné	zákonodárný	k2eAgInPc1d1	zákonodárný
sbory	sbor	k1gInPc1	sbor
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
byl	být	k5eAaImAgInS	být
slovinský	slovinský	k2eAgInSc1d1	slovinský
parlament	parlament	k1gInSc1	parlament
jednokomorový	jednokomorový	k2eAgInSc1d1	jednokomorový
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1953	[number]	k4	1953
až	až	k9	až
1963	[number]	k4	1963
dvoukomorový	dvoukomorový	k2eAgInSc4d1	dvoukomorový
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
pětikomorový	pětikomorový	k2eAgMnSc1d1	pětikomorový
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1974	[number]	k4	1974
až	až	k9	až
1991	[number]	k4	1991
tříkomorový	tříkomorový	k2eAgInSc1d1	tříkomorový
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
pojmem	pojem	k1gInSc7	pojem
parlament	parlament	k1gInSc1	parlament
souhrnně	souhrnně	k6eAd1	souhrnně
označuje	označovat	k5eAaImIp3nS	označovat
Státní	státní	k2eAgNnSc1d1	státní
shromáždění	shromáždění	k1gNnSc1	shromáždění
a	a	k8xC	a
Státní	státní	k2eAgFnSc1d1	státní
rada	rada	k1gFnSc1	rada
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
ústavně	ústavně	k6eAd1	ústavně
konstruovány	konstruovat	k5eAaImNgInP	konstruovat
bez	bez	k7c2	bez
podřazení	podřazení	k1gNnSc2	podřazení
pod	pod	k7c4	pod
společný	společný	k2eAgInSc4d1	společný
subjekt	subjekt	k1gInSc4	subjekt
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
slovinský	slovinský	k2eAgInSc1d1	slovinský
parlament	parlament	k1gInSc1	parlament
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
obecného	obecný	k2eAgNnSc2d1	obecné
hlediska	hledisko	k1gNnSc2	hledisko
nastaven	nastaven	k2eAgMnSc1d1	nastaven
jako	jako	k8xC	jako
nedokonalý	dokonalý	k2eNgInSc1d1	nedokonalý
dvoukomorový	dvoukomorový	k2eAgInSc1d1	dvoukomorový
s	s	k7c7	s
výraznou	výrazný	k2eAgFnSc7d1	výrazná
převahou	převaha	k1gFnSc7	převaha
dolní	dolní	k2eAgFnSc2d1	dolní
komory	komora	k1gFnSc2	komora
(	(	kIx(	(
<g/>
Státního	státní	k2eAgNnSc2d1	státní
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
zákonodárných	zákonodárný	k2eAgInPc2d1	zákonodárný
sborů	sbor	k1gInPc2	sbor
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
komor	komora	k1gFnPc2	komora
stál	stát	k5eAaImAgMnS	stát
vždy	vždy	k6eAd1	vždy
předseda	předseda	k1gMnSc1	předseda
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1946	[number]	k4	1946
až	až	k9	až
1953	[number]	k4	1953
pak	pak	k6eAd1	pak
ještě	ještě	k6eAd1	ještě
existoval	existovat	k5eAaImAgInS	existovat
i	i	k9	i
kolektivní	kolektivní	k2eAgInSc1d1	kolektivní
orgán	orgán	k1gInSc1	orgán
-	-	kIx~	-
Prezídium	prezídium	k1gNnSc1	prezídium
Skupščiny	Skupščin	k2eAgFnSc2d1	Skupščin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1953	[number]	k4	1953
až	až	k9	až
1974	[number]	k4	1974
byl	být	k5eAaImAgMnS	být
předseda	předseda	k1gMnSc1	předseda
Skupščiny	Skupščin	k2eAgFnSc2d1	Skupščin
zároveň	zároveň	k6eAd1	zároveň
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
představitelem	představitel	k1gMnSc7	představitel
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
má	mít	k5eAaImIp3nS	mít
svého	svůj	k3xOyFgMnSc4	svůj
předsedu	předseda	k1gMnSc4	předseda
Státní	státní	k2eAgFnSc1d1	státní
rada	rada	k1gFnSc1	rada
i	i	k8xC	i
Státní	státní	k2eAgNnSc1d1	státní
shromáždění	shromáždění	k1gNnSc1	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
orgánem	orgán	k1gInSc7	orgán
moci	moct	k5eAaImF	moct
výkonné	výkonný	k2eAgFnPc4d1	výkonná
je	být	k5eAaImIp3nS	být
Vláda	vláda	k1gFnSc1	vláda
Republiky	republika	k1gFnSc2	republika
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
ústavy	ústava	k1gFnSc2	ústava
a	a	k8xC	a
zákonů	zákon	k1gInPc2	zákon
se	se	k3xPyFc4	se
vláda	vláda	k1gFnSc1	vláda
jeví	jevit	k5eAaImIp3nS	jevit
-	-	kIx~	-
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
za	za	k7c2	za
účinnosti	účinnost	k1gFnSc2	účinnost
socialistické	socialistický	k2eAgFnSc2d1	socialistická
ústavy	ústava	k1gFnSc2	ústava
-	-	kIx~	-
jako	jako	k8xC	jako
výkonný	výkonný	k2eAgInSc1d1	výkonný
výbor	výbor	k1gInSc1	výbor
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
však	však	k9	však
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
již	již	k6eAd1	již
tak	tak	k6eAd1	tak
jednoznačné	jednoznačný	k2eAgNnSc1d1	jednoznačné
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Soudy	soud	k1gInPc1	soud
ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
<g/>
.	.	kIx.	.
</s>
<s>
Soudní	soudní	k2eAgFnSc1d1	soudní
moc	moc	k1gFnSc1	moc
ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
Ústavním	ústavní	k2eAgInSc7d1	ústavní
soudem	soud	k1gInSc7	soud
<g/>
,	,	kIx,	,
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
soudem	soud	k1gInSc7	soud
-	-	kIx~	-
jako	jako	k8xC	jako
nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
soudním	soudní	k2eAgNnSc7d1	soudní
tělesem	těleso	k1gNnSc7	těleso
-	-	kIx~	-
pod	pod	k7c7	pod
nímž	jenž	k3xRgInSc7	jenž
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
vrchní	vrchní	k2eAgInPc1d1	vrchní
soudy	soud	k1gInPc1	soud
a	a	k8xC	a
Vyšší	vysoký	k2eAgInSc1d2	vyšší
pracovní	pracovní	k2eAgInSc1d1	pracovní
a	a	k8xC	a
sociální	sociální	k2eAgInSc1d1	sociální
soud	soud	k1gInSc1	soud
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vrchními	vrchní	k2eAgInPc7d1	vrchní
soudy	soud	k1gInPc7	soud
jsou	být	k5eAaImIp3nP	být
soudy	soud	k1gInPc1	soud
krajské	krajský	k2eAgInPc1d1	krajský
a	a	k8xC	a
okresní	okresní	k2eAgInPc1d1	okresní
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
Vyšším	vysoký	k2eAgInSc7d2	vyšší
pracovním	pracovní	k2eAgInSc7d1	pracovní
a	a	k8xC	a
sociálním	sociální	k2eAgInSc7d1	sociální
soudem	soud	k1gInSc7	soud
jsou	být	k5eAaImIp3nP	být
pracovní	pracovní	k2eAgInPc1d1	pracovní
soudy	soud	k1gInPc1	soud
a	a	k8xC	a
Pracovní	pracovní	k2eAgInSc1d1	pracovní
a	a	k8xC	a
sociální	sociální	k2eAgInSc1d1	sociální
soud	soud	k1gInSc1	soud
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
byl	být	k5eAaImAgInS	být
vývoj	vývoj	k1gInSc4	vývoj
slovinské	slovinský	k2eAgNnSc1d1	slovinské
soudnictví	soudnictví	k1gNnSc1	soudnictví
ovlivňován	ovlivňovat	k5eAaImNgInS	ovlivňovat
příslušností	příslušnost	k1gFnPc2	příslušnost
k	k	k7c3	k
podunajské	podunajský	k2eAgFnSc3d1	Podunajská
monarchii	monarchie	k1gFnSc3	monarchie
<g/>
,	,	kIx,	,
v	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1918	[number]	k4	1918
až	až	k6eAd1	až
1991	[number]	k4	1991
-	-	kIx~	-
s	s	k7c7	s
přestávkou	přestávka	k1gFnSc7	přestávka
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
-	-	kIx~	-
pak	pak	k6eAd1	pak
vývojem	vývoj	k1gInSc7	vývoj
ve	v	k7c6	v
státu	stát	k1gInSc6	stát
jižních	jižní	k2eAgInPc2d1	jižní
Slovanů	Slovan	k1gInPc2	Slovan
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
soustava	soustava	k1gFnSc1	soustava
obecných	obecný	k2eAgInPc2d1	obecný
soudů	soud	k1gInPc2	soud
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
44	[number]	k4	44
okresními	okresní	k2eAgInPc7d1	okresní
soudy	soud	k1gInPc7	soud
(	(	kIx(	(
<g/>
okrajna	okrajen	k2eAgNnPc1d1	okrajen
sodišča	sodiščum	k1gNnPc1	sodiščum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
podřazeny	podřadit	k5eAaPmNgFnP	podřadit
pod	pod	k7c4	pod
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
jedenácti	jedenáct	k4xCc2	jedenáct
krajských	krajský	k2eAgInPc2d1	krajský
soudů	soud	k1gInPc2	soud
(	(	kIx(	(
<g/>
okrožna	okrožen	k2eAgFnSc1d1	okrožen
sodišča	sodišča	k1gFnSc1	sodišča
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
mezi	mezi	k7c4	mezi
čtyři	čtyři	k4xCgInPc4	čtyři
vrchní	vrchní	k2eAgInPc4d1	vrchní
soudy	soud	k1gInPc4	soud
(	(	kIx(	(
<g/>
Višja	Višj	k2eAgFnSc1d1	Višj
sodišča	sodišča	k1gFnSc1	sodišča
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
soustavy	soustava	k1gFnSc2	soustava
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
Republiky	republika	k1gFnSc2	republika
Slovinsko	Slovinsko	k1gNnSc4	Slovinsko
(	(	kIx(	(
<g/>
Vrhovno	Vrhovna	k1gFnSc5	Vrhovna
sodišče	sodišč	k1gInPc4	sodišč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
a	a	k8xC	a
úrovni	úroveň	k1gFnSc6	úroveň
vrchních	vrchní	k2eAgInPc2d1	vrchní
soudů	soud	k1gInPc2	soud
existuje	existovat	k5eAaImIp3nS	existovat
ještě	ještě	k6eAd1	ještě
Správní	správní	k2eAgInSc1d1	správní
soud	soud	k1gInSc1	soud
(	(	kIx(	(
<g/>
Upravno	Upravno	k1gNnSc1	Upravno
sodišče	sodišč	k1gInSc2	sodišč
<g/>
)	)	kIx)	)
a	a	k8xC	a
Vrchní	vrchní	k2eAgInSc1d1	vrchní
pracovní	pracovní	k2eAgInSc1d1	pracovní
a	a	k8xC	a
sociální	sociální	k2eAgInSc1d1	sociální
soud	soud	k1gInSc1	soud
(	(	kIx(	(
<g/>
Višje	Višje	k1gFnSc1	Višje
delovno	delovna	k1gFnSc5	delovna
in	in	k?	in
socialno	socialno	k1gNnSc4	socialno
sodišče	sodišč	k1gFnSc2	sodišč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pod	pod	k7c4	pod
který	který	k3yIgInSc4	který
patří	patřit	k5eAaImIp3nP	patřit
tři	tři	k4xCgInPc1	tři
pracovní	pracovní	k2eAgInPc1d1	pracovní
soudy	soud	k1gInPc1	soud
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
pracovní	pracovní	k2eAgInSc1d1	pracovní
a	a	k8xC	a
sociální	sociální	k2eAgInSc1d1	sociální
soud	soud	k1gInSc1	soud
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečné	výjimečný	k2eAgInPc1d1	výjimečný
soudy	soud	k1gInPc1	soud
a	a	k8xC	a
v	v	k7c6	v
době	doba	k1gFnSc6	doba
míru	mír	k1gInSc2	mír
vojenské	vojenský	k2eAgInPc1d1	vojenský
soudy	soud	k1gInPc1	soud
se	se	k3xPyFc4	se
z	z	k7c2	z
dikce	dikce	k1gFnSc2	dikce
ústavy	ústava	k1gFnSc2	ústava
nevytvářejí	vytvářet	k5eNaImIp3nP	vytvářet
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ústavy	ústava	k1gFnSc2	ústava
jsou	být	k5eAaImIp3nP	být
soudci	soudce	k1gMnPc1	soudce
slovinských	slovinský	k2eAgInPc2d1	slovinský
soudů	soud	k1gInPc2	soud
vázáni	vázán	k2eAgMnPc1d1	vázán
ústavou	ústava	k1gFnSc7	ústava
a	a	k8xC	a
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Specifickou	specifický	k2eAgFnSc7d1	specifická
institucí	instituce	k1gFnSc7	instituce
je	být	k5eAaImIp3nS	být
Rozpočtový	rozpočtový	k2eAgInSc4d1	rozpočtový
soud	soud	k1gInSc4	soud
(	(	kIx(	(
<g/>
Računsko	Računska	k1gMnSc5	Računska
sodišče	sodišec	k1gMnSc5	sodišec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
orgán	orgán	k1gInSc1	orgán
pro	pro	k7c4	pro
přezkoumání	přezkoumání	k1gNnSc4	přezkoumání
státních	státní	k2eAgInPc2d1	státní
účtů	účet	k1gInPc2	účet
<g/>
,	,	kIx,	,
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
a	a	k8xC	a
všech	všecek	k3xTgInPc2	všecek
veřejných	veřejný	k2eAgInPc2d1	veřejný
výdajů	výdaj	k1gInPc2	výdaj
<g/>
.	.	kIx.	.
</s>
<s>
Rozpočtový	rozpočtový	k2eAgInSc1d1	rozpočtový
soud	soud	k1gInSc1	soud
však	však	k9	však
nelze	lze	k6eNd1	lze
řadit	řadit	k5eAaImF	řadit
mezi	mezi	k7c4	mezi
ostatní	ostatní	k2eAgInPc4d1	ostatní
soudy	soud	k1gInPc4	soud
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
zakotvení	zakotvení	k1gNnSc4	zakotvení
nachází	nacházet	k5eAaImIp3nS	nacházet
zcela	zcela	k6eAd1	zcela
mimo	mimo	k7c4	mimo
moc	moc	k1gFnSc4	moc
soudní	soudní	k2eAgInSc1d1	soudní
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Politické	politický	k2eAgFnSc2d1	politická
strany	strana	k1gFnSc2	strana
ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
existuje	existovat	k5eAaImIp3nS	existovat
vícestranický	vícestranický	k2eAgInSc1d1	vícestranický
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
je	být	k5eAaImIp3nS	být
rozdělován	rozdělovat	k5eAaImNgInS	rozdělovat
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
fází	fáze	k1gFnPc2	fáze
<g/>
:	:	kIx,	:
do	do	k7c2	do
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
do	do	k7c2	do
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
do	do	k7c2	do
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
od	od	k7c2	od
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
fáze	fáze	k1gFnSc1	fáze
vývoje	vývoj	k1gInSc2	vývoj
slovinského	slovinský	k2eAgInSc2d1	slovinský
stranického	stranický	k2eAgInSc2d1	stranický
systému	systém	k1gInSc2	systém
v	v	k7c4	v
období	období	k1gNnPc4	období
od	od	k7c2	od
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
devatenáctého	devatenáctý	k4xOgNnSc2	devatenáctý
století	století	k1gNnSc2	století
do	do	k7c2	do
čtyřicátých	čtyřicátý	k4xOgNnPc2	čtyřicátý
let	léto	k1gNnPc2	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
rozvoje	rozvoj	k1gInSc2	rozvoj
občanské	občanský	k2eAgFnSc2d1	občanská
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
vytváření	vytváření	k1gNnSc1	vytváření
prvních	první	k4xOgFnPc2	první
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
politického	politický	k2eAgInSc2d1	politický
vývoje	vývoj	k1gInSc2	vývoj
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
faktická	faktický	k2eAgFnSc1d1	faktická
legální	legální	k2eAgFnSc1d1	legální
politická	politický	k2eAgFnSc1d1	politická
činnost	činnost	k1gFnSc1	činnost
znemožněna	znemožnit	k5eAaPmNgFnS	znemožnit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
existovala	existovat	k5eAaImAgFnS	existovat
jedna	jeden	k4xCgFnSc1	jeden
politická	politický	k2eAgFnSc1d1	politická
strana	strana	k1gFnSc1	strana
-	-	kIx~	-
Svaz	svaz	k1gInSc1	svaz
komunistů	komunista	k1gMnPc2	komunista
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
<g/>
,	,	kIx,	,
organizačně	organizačně	k6eAd1	organizačně
začleněná	začleněný	k2eAgFnSc1d1	začleněná
do	do	k7c2	do
Svazu	svaz	k1gInSc2	svaz
komunistů	komunista	k1gMnPc2	komunista
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
strany	strana	k1gFnSc2	strana
existovala	existovat	k5eAaImAgFnS	existovat
soustava	soustava	k1gFnSc1	soustava
společensko-politických	společenskoolitický	k2eAgFnPc2d1	společensko-politická
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1986	[number]	k4	1986
až	až	k9	až
1991	[number]	k4	1991
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přechodu	přechod	k1gInSc3	přechod
k	k	k7c3	k
pluralitnímu	pluralitní	k2eAgInSc3d1	pluralitní
stranickému	stranický	k2eAgInSc3d1	stranický
systému	systém	k1gInSc3	systém
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
funguje	fungovat	k5eAaImIp3nS	fungovat
současný	současný	k2eAgInSc1d1	současný
stranický	stranický	k2eAgInSc1d1	stranický
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Relevantní	relevantní	k2eAgFnPc1d1	relevantní
politické	politický	k2eAgFnPc1d1	politická
strany	strana	k1gFnPc1	strana
současné	současný	k2eAgFnSc2d1	současná
politické	politický	k2eAgFnSc2d1	politická
scény	scéna	k1gFnSc2	scéna
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
Sociální	sociální	k2eAgMnPc1d1	sociální
demokraté	demokrat	k1gMnPc1	demokrat
(	(	kIx(	(
<g/>
Socialni	Socialni	k1gFnPc2	Socialni
demokrati	demokrat	k1gMnPc1	demokrat
-	-	kIx~	-
SD	SD	kA	SD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Slovinská	slovinský	k2eAgFnSc1d1	slovinská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
Slovenska	Slovensko	k1gNnSc2	Slovensko
demokratska	demokratsk	k1gInSc2	demokratsk
stranka	stranka	k1gFnSc1	stranka
-	-	kIx~	-
SDS	SDS	kA	SDS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ZARES	ZARES	kA	ZARES
-	-	kIx~	-
Nová	nový	k2eAgFnSc1d1	nová
politika	politika	k1gFnSc1	politika
(	(	kIx(	(
<g/>
Zares	Zares	k1gInSc1	Zares
-	-	kIx~	-
Nova	nova	k1gFnSc1	nova
politika	politika	k1gFnSc1	politika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
důchodců	důchodce	k1gMnPc2	důchodce
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
(	(	kIx(	(
<g/>
Demokratična	Demokratično	k1gNnSc2	Demokratično
stranka	stranka	k1gFnSc1	stranka
upokojencev	upokojencev	k1gFnSc1	upokojencev
Slovenije	Slovenije	k1gFnSc1	Slovenije
-	-	kIx~	-
DeSUS	DeSUS	k1gFnSc1	DeSUS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Slovinská	slovinský	k2eAgFnSc1d1	slovinská
národní	národní	k2eAgFnSc1d1	národní
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
Slovenska	Slovensko	k1gNnSc2	Slovensko
nacionalna	nacionalno	k1gNnSc2	nacionalno
stranka	stranka	k1gFnSc1	stranka
-	-	kIx~	-
SNS	SNS	kA	SNS
<g/>
)	)	kIx)	)
a	a	k8xC	a
Liberální	liberální	k2eAgFnSc1d1	liberální
demokracie	demokracie	k1gFnSc1	demokracie
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
(	(	kIx(	(
<g/>
Liberalna	Liberalna	k1gFnSc1	Liberalna
demokracija	demokracija	k1gMnSc1	demokracija
Slovenije	Slovenije	k1gMnSc1	Slovenije
-	-	kIx~	-
LDS	LDS	kA	LDS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
slovinské	slovinský	k2eAgFnSc2d1	slovinská
země	zem	k1gFnSc2	zem
<g/>
"	"	kIx"	"
byly	být	k5eAaImAgFnP	být
označovány	označovat	k5eAaImNgInP	označovat
Kraňsko	Kraňsko	k1gNnSc1	Kraňsko
(	(	kIx(	(
<g/>
s	s	k7c7	s
centrem	centrum	k1gNnSc7	centrum
v	v	k7c6	v
Lublani	Lublaň	k1gFnSc6	Lublaň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dolní	dolní	k2eAgNnSc1d1	dolní
Štýrsko	Štýrsko	k1gNnSc1	Štýrsko
(	(	kIx(	(
<g/>
s	s	k7c7	s
centrem	centrum	k1gNnSc7	centrum
v	v	k7c6	v
Mariboru	Maribor	k1gInSc6	Maribor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Korutany	Korutany	k1gInPc1	Korutany
(	(	kIx(	(
<g/>
s	s	k7c7	s
centrem	centrum	k1gNnSc7	centrum
Celovci	Celovec	k1gInSc6	Celovec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gorice	Gorice	k1gFnSc1	Gorice
a	a	k8xC	a
Gradiška	Gradiška	k1gFnSc1	Gradiška
(	(	kIx(	(
<g/>
s	s	k7c7	s
centrem	centrum	k1gNnSc7	centrum
v	v	k7c6	v
Gorici	Gorice	k1gFnSc6	Gorice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Terst	Terst	k1gInSc1	Terst
<g/>
,	,	kIx,	,
Istrie	Istrie	k1gFnSc1	Istrie
(	(	kIx(	(
<g/>
s	s	k7c7	s
centrem	centrum	k1gNnSc7	centrum
v	v	k7c6	v
Pule	Pule	k1gFnSc6	Pule
<g/>
)	)	kIx)	)
a	a	k8xC	a
Zámuří	Zámuří	k2eAgFnSc1d1	Zámuří
(	(	kIx(	(
<g/>
s	s	k7c7	s
neoficiálním	oficiální	k2eNgNnSc7d1	neoficiální
centrem	centrum	k1gNnSc7	centrum
v	v	k7c6	v
Murské	Murský	k2eAgFnSc6d1	Murská
Sobotě	sobota	k1gFnSc6	sobota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
zůstala	zůstat	k5eAaPmAgFnS	zůstat
Slovincům	Slovinec	k1gMnPc3	Slovinec
jen	jen	k9	jen
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
etnicky	etnicky	k6eAd1	etnicky
homogenní	homogenní	k2eAgNnSc1d1	homogenní
území	území	k1gNnSc1	území
Kraňska	Kraňsko	k1gNnSc2	Kraňsko
<g/>
,	,	kIx,	,
Dolní	dolní	k2eAgNnSc1d1	dolní
Štýrsko	Štýrsko	k1gNnSc1	Štýrsko
a	a	k8xC	a
Zámuří	Zámuří	k1gNnSc1	Zámuří
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
početná	početný	k2eAgFnSc1d1	početná
maďarská	maďarský	k2eAgFnSc1d1	maďarská
menšina	menšina	k1gFnSc1	menšina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
meziválečném	meziválečný	k2eAgNnSc6d1	meziválečné
období	období	k1gNnSc6	období
byla	být	k5eAaImAgFnS	být
nejprve	nejprve	k6eAd1	nejprve
dosavadní	dosavadní	k2eAgFnSc1d1	dosavadní
územní	územní	k2eAgFnSc1d1	územní
organizace	organizace	k1gFnSc1	organizace
rozbita	rozbit	k2eAgFnSc1d1	rozbita
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
pak	pak	k6eAd1	pak
prakticky	prakticky	k6eAd1	prakticky
veškerá	veškerý	k3xTgNnPc1	veškerý
slovinská	slovinský	k2eAgNnPc1d1	slovinské
území	území	k1gNnPc1	území
připadla	připadnout	k5eAaPmAgNnP	připadnout
k	k	k7c3	k
Drávské	Drávský	k2eAgFnSc3d1	Drávská
bánovině	bánovina	k1gFnSc3	bánovina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
korektuře	korektura	k1gFnSc3	korektura
jugoslávsko-italské	jugoslávskotalský	k2eAgFnSc2d1	jugoslávsko-italský
hranice	hranice	k1gFnSc2	hranice
<g/>
,	,	kIx,	,
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
připojena	připojen	k2eAgNnPc1d1	připojeno
území	území	k1gNnPc1	území
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
Itálie	Itálie	k1gFnSc1	Itálie
zabrala	zabrat	k5eAaPmAgFnS	zabrat
<g/>
.	.	kIx.	.
</s>
<s>
Istrie	Istrie	k1gFnSc1	Istrie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
po	po	k7c6	po
odsunu	odsun	k1gInSc6	odsun
Italů	Ital	k1gMnPc2	Ital
měli	mít	k5eAaImAgMnP	mít
většinu	většina	k1gFnSc4	většina
Chorvaté	Chorvat	k1gMnPc1	Chorvat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
sovětsko-jugoslávské	sovětskougoslávský	k2eAgFnSc2d1	sovětsko-jugoslávská
roztržky	roztržka	k1gFnSc2	roztržka
nebyly	být	k5eNaImAgFnP	být
naplněny	naplnit	k5eAaPmNgFnP	naplnit
úvahy	úvaha	k1gFnPc1	úvaha
o	o	k7c4	o
připojení	připojení	k1gNnSc4	připojení
Korutan	Korutany	k1gInPc2	Korutany
(	(	kIx(	(
<g/>
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
)	)	kIx)	)
a	a	k8xC	a
celého	celý	k2eAgNnSc2d1	celé
území	území	k1gNnSc2	území
Terstu	Terst	k1gInSc2	Terst
(	(	kIx(	(
<g/>
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
)	)	kIx)	)
k	k	k7c3	k
Jugoslávii	Jugoslávie	k1gFnSc3	Jugoslávie
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
správní	správní	k2eAgFnSc3d1	správní
reformě	reforma	k1gFnSc3	reforma
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
19	[number]	k4	19
okresů	okres	k1gInPc2	okres
(	(	kIx(	(
<g/>
okraji	okraj	k1gInPc7	okraj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tvořené	tvořený	k2eAgNnSc1d1	tvořené
třemi	tři	k4xCgNnPc7	tři
městy	město	k1gNnPc7	město
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
jedno	jeden	k4xCgNnSc1	jeden
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
-	-	kIx~	-
Lublaň	Lublaň	k1gFnSc1	Lublaň
<g/>
)	)	kIx)	)
a	a	k8xC	a
371	[number]	k4	371
občin	občina	k1gFnPc2	občina
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
44	[number]	k4	44
městských	městský	k2eAgFnPc2d1	městská
občin	občina	k1gFnPc2	občina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
již	již	k6eAd1	již
existovaly	existovat	k5eAaImAgFnP	existovat
jen	jen	k9	jen
občiny	občina	k1gFnPc1	občina
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
počet	počet	k1gInSc1	počet
postupně	postupně	k6eAd1	postupně
klesal	klesat	k5eAaImAgInS	klesat
-	-	kIx~	-
od	od	k7c2	od
130	[number]	k4	130
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
na	na	k7c4	na
60	[number]	k4	60
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
občin	občina	k1gFnPc2	občina
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
na	na	k7c4	na
65	[number]	k4	65
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
opět	opět	k6eAd1	opět
klesl	klesnout	k5eAaPmAgInS	klesnout
na	na	k7c4	na
62	[number]	k4	62
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
osamostatnění	osamostatnění	k1gNnSc6	osamostatnění
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
se	se	k3xPyFc4	se
vedly	vést	k5eAaImAgFnP	vést
diskuse	diskuse	k1gFnPc1	diskuse
o	o	k7c6	o
reformě	reforma	k1gFnSc6	reforma
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
212	[number]	k4	212
občin	občina	k1gFnPc2	občina
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
11	[number]	k4	11
má	mít	k5eAaImIp3nS	mít
status	status	k1gInSc4	status
městské	městský	k2eAgFnSc2d1	městská
občiny	občina	k1gFnSc2	občina
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
z	z	k7c2	z
občin	občina	k1gFnPc2	občina
<g/>
,	,	kIx,	,
Ankaran	Ankarana	k1gFnPc2	Ankarana
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2011	[number]	k4	2011
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
původní	původní	k2eAgFnSc1d1	původní
koncepce	koncepce	k1gFnSc1	koncepce
počítala	počítat	k5eAaImAgFnS	počítat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
druhých	druhý	k4xOgFnPc6	druhý
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
orgánů	orgán	k1gInPc2	orgán
občin	občina	k1gFnPc2	občina
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
bude	být	k5eAaImBp3nS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
zákonná	zákonný	k2eAgFnSc1d1	zákonná
úprava	úprava	k1gFnSc1	úprava
krajského	krajský	k2eAgNnSc2d1	krajské
zřízení	zřízení	k1gNnSc2	zřízení
<g/>
,	,	kIx,	,
doposud	doposud	k6eAd1	doposud
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nestalo	stát	k5eNaPmAgNnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Zahraniční	zahraniční	k2eAgInPc1d1	zahraniční
vztahy	vztah	k1gInPc1	vztah
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
a	a	k8xC	a
Česko-slovinské	českolovinský	k2eAgInPc4d1	česko-slovinský
vztahy	vztah	k1gInPc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
samostatným	samostatný	k2eAgInSc7d1	samostatný
svrchovaným	svrchovaný	k2eAgInSc7d1	svrchovaný
státem	stát	k1gInSc7	stát
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1991	[number]	k4	1991
bylo	být	k5eAaImAgNnS	být
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
uznáno	uznat	k5eAaPmNgNnS	uznat
Chorvatskem	Chorvatsko	k1gNnSc7	Chorvatsko
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1991	[number]	k4	1991
byla	být	k5eAaImAgFnS	být
Brionskou	Brionský	k2eAgFnSc7d1	Brionská
deklarací	deklarace	k1gFnSc7	deklarace
účinnost	účinnost	k1gFnSc4	účinnost
aktů	akt	k1gInPc2	akt
z	z	k7c2	z
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1991	[number]	k4	1991
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
přerušena	přerušit	k5eAaPmNgFnS	přerušit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1991	[number]	k4	1991
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
svou	svůj	k3xOyFgFnSc4	svůj
vůli	vůle	k1gFnSc4	vůle
opustit	opustit	k5eAaPmF	opustit
Jugoslávii	Jugoslávie	k1gFnSc4	Jugoslávie
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
ho	on	k3xPp3gMnSc4	on
uznaly	uznat	k5eAaPmAgInP	uznat
některé	některý	k3yIgInPc1	některý
bývalé	bývalý	k2eAgInPc1d1	bývalý
státy	stát	k1gInPc1	stát
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1991	[number]	k4	1991
bylo	být	k5eAaImAgNnS	být
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
uznáno	uznán	k2eAgNnSc1d1	uznáno
Islandem	Island	k1gInSc7	Island
<g/>
,	,	kIx,	,
Švédskem	Švédsko	k1gNnSc7	Švédsko
a	a	k8xC	a
Spolkovou	spolkový	k2eAgFnSc7d1	spolková
republikou	republika	k1gFnSc7	republika
Německo	Německo	k1gNnSc1	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Vatikán	Vatikán	k1gInSc1	Vatikán
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
uznal	uznat	k5eAaPmAgInS	uznat
13	[number]	k4	13
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
San	San	k1gFnSc1	San
Marino	Marina	k1gFnSc5	Marina
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1992	[number]	k4	1992
a	a	k8xC	a
Evropská	evropský	k2eAgNnPc4d1	Evropské
společenství	společenství	k1gNnPc4	společenství
pak	pak	k6eAd1	pak
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
ČSFR	ČSFR	kA	ČSFR
uznala	uznat	k5eAaPmAgFnS	uznat
Slovinsko	Slovinsko	k1gNnSc4	Slovinsko
společně	společně	k6eAd1	společně
s	s	k7c7	s
Chorvatskem	Chorvatsko	k1gNnSc7	Chorvatsko
16	[number]	k4	16
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
uznaly	uznat	k5eAaPmAgFnP	uznat
Slovinsko	Slovinsko	k1gNnSc4	Slovinsko
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2004	[number]	k4	2004
členem	člen	k1gMnSc7	člen
NATO	nato	k6eAd1	nato
<g/>
,	,	kIx,	,
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2004	[number]	k4	2004
členem	člen	k1gMnSc7	člen
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
členem	člen	k1gMnSc7	člen
Evropské	evropský	k2eAgFnSc2d1	Evropská
měnové	měnový	k2eAgFnSc2d1	měnová
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
od	od	k7c2	od
21	[number]	k4	21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2010	[number]	k4	2010
pak	pak	k6eAd1	pak
i	i	k8xC	i
OECD	OECD	kA	OECD
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1997	[number]	k4	1997
až	až	k9	až
1999	[number]	k4	1999
bylo	být	k5eAaImAgNnS	být
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
nestálým	stálý	k2eNgInSc7d1	nestálý
členem	člen	k1gInSc7	člen
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
předsedalo	předsedat	k5eAaImAgNnS	předsedat
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
Radě	rada	k1gFnSc6	rada
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
v	v	k7c6	v
období	období	k1gNnSc6	období
května	květen	k1gInSc2	květen
až	až	k8xS	až
listopadu	listopad	k1gInSc2	listopad
2008	[number]	k4	2008
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
Výboru	výbor	k1gInSc2	výbor
ministrů	ministr	k1gMnPc2	ministr
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Mimořádnou	mimořádný	k2eAgFnSc7d1	mimořádná
a	a	k8xC	a
zplnomocněnou	zplnomocněný	k2eAgFnSc7d1	zplnomocněná
velvyslankyní	velvyslankyně	k1gFnSc7	velvyslankyně
Republiky	republika	k1gFnSc2	republika
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
února	únor	k1gInSc2	únor
2011	[number]	k4	2011
Smiljana	Smiljan	k1gMnSc2	Smiljan
Knez	Knez	k1gInSc4	Knez
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc4	počátek
industrializace	industrializace	k1gFnSc2	industrializace
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
sahají	sahat	k5eAaImIp3nP	sahat
do	do	k7c2	do
období	období	k1gNnSc2	období
habsburské	habsburský	k2eAgFnSc2d1	habsburská
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
státu	stát	k1gInSc2	stát
jižních	jižní	k2eAgInPc2d1	jižní
Slovanů	Slovan	k1gInPc2	Slovan
se	se	k3xPyFc4	se
slovinské	slovinský	k2eAgFnSc2d1	slovinská
oblasti	oblast	k1gFnSc2	oblast
řadily	řadit	k5eAaImAgFnP	řadit
k	k	k7c3	k
hospodářsky	hospodářsky	k6eAd1	hospodářsky
nejrozvinutějším	rozvinutý	k2eAgFnPc3d3	nejrozvinutější
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1945	[number]	k4	1945
až	až	k9	až
1946	[number]	k4	1946
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zestátnění	zestátnění	k1gNnSc3	zestátnění
velkého	velký	k2eAgInSc2d1	velký
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
bude	být	k5eAaImBp3nS	být
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
zejména	zejména	k9	zejména
těžký	těžký	k2eAgInSc1d1	těžký
průmysl	průmysl	k1gInSc1	průmysl
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1947	[number]	k4	1947
tak	tak	k8xC	tak
maršál	maršál	k1gMnSc1	maršál
Tito	tento	k3xDgMnPc1	tento
otevřel	otevřít	k5eAaPmAgInS	otevřít
železárnu	železárna	k1gFnSc4	železárna
v	v	k7c6	v
Lublani	Lublaň	k1gFnSc6	Lublaň
-	-	kIx~	-
Titovi	Titův	k2eAgMnPc1d1	Titův
zavodi	zavod	k1gMnPc1	zavod
Litostroj	Litostroj	k1gInSc4	Litostroj
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Litostroj	Litostroj	k1gInSc1	Litostroj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednostranná	jednostranný	k2eAgFnSc1d1	jednostranná
orientace	orientace	k1gFnSc1	orientace
na	na	k7c4	na
těžký	těžký	k2eAgInSc4d1	těžký
průmysl	průmysl	k1gInSc4	průmysl
byla	být	k5eAaImAgFnS	být
opuštěna	opustit	k5eAaPmNgFnS	opustit
v	v	k7c6	v
období	období	k1gNnSc6	období
reformní	reformní	k2eAgFnSc2d1	reformní
vlády	vláda	k1gFnSc2	vláda
Stane	stanout	k5eAaPmIp3nS	stanout
Kavčiče	Kavčič	k1gMnPc4	Kavčič
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
současně	současně	k6eAd1	současně
začalo	začít	k5eAaPmAgNnS	začít
období	období	k1gNnSc4	období
surovinově	surovinově	k6eAd1	surovinově
a	a	k8xC	a
technologicky	technologicky	k6eAd1	technologicky
efektivnější	efektivní	k2eAgFnSc2d2	efektivnější
výroby	výroba	k1gFnSc2	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
byli	být	k5eAaImAgMnP	být
reformní	reformní	k2eAgMnPc1d1	reformní
komunisté	komunista	k1gMnPc1	komunista
kolem	kolem	k7c2	kolem
Kavčiče	Kavčič	k1gInSc2	Kavčič
nakonec	nakonec	k6eAd1	nakonec
odstaveni	odstaven	k2eAgMnPc1d1	odstaven
<g/>
,	,	kIx,	,
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
si	se	k3xPyFc3	se
i	i	k9	i
nadále	nadále	k6eAd1	nadále
udrželo	udržet	k5eAaPmAgNnS	udržet
relativně	relativně	k6eAd1	relativně
intenzivní	intenzivní	k2eAgInPc4d1	intenzivní
ekonomické	ekonomický	k2eAgInPc4d1	ekonomický
styky	styk	k1gInPc4	styk
se	s	k7c7	s
západními	západní	k2eAgFnPc7d1	západní
zeměmi	zem	k1gFnPc7	zem
a	a	k8xC	a
firmami	firma	k1gFnPc7	firma
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ovšem	ovšem	k9	ovšem
naráželo	narážet	k5eAaImAgNnS	narážet
na	na	k7c4	na
odpor	odpor	k1gInSc4	odpor
srbského	srbský	k2eAgMnSc2d1	srbský
a	a	k8xC	a
jugoslávského	jugoslávský	k2eAgNnSc2d1	jugoslávské
vedení	vedení	k1gNnSc2	vedení
<g/>
.	.	kIx.	.
</s>
<s>
Vyhlášení	vyhlášení	k1gNnSc1	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
ztráta	ztráta	k1gFnSc1	ztráta
celojugoslávského	celojugoslávský	k2eAgInSc2d1	celojugoslávský
trhu	trh	k1gInSc2	trh
<g/>
,	,	kIx,	,
způsobila	způsobit	k5eAaPmAgFnS	způsobit
slovinskému	slovinský	k2eAgInSc3d1	slovinský
hospodářství	hospodářství	k1gNnSc2	hospodářství
značné	značný	k2eAgInPc4d1	značný
problémy	problém	k1gInPc4	problém
<g/>
:	:	kIx,	:
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
polovinu	polovina	k1gFnSc4	polovina
klesla	klesnout	k5eAaPmAgFnS	klesnout
průměrná	průměrný	k2eAgFnSc1d1	průměrná
mzda	mzda	k1gFnSc1	mzda
a	a	k8xC	a
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
výroba	výroba	k1gFnSc1	výroba
klesla	klesnout	k5eAaPmAgFnS	klesnout
na	na	k7c4	na
hodnoty	hodnota	k1gFnPc4	hodnota
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1995	[number]	k4	1995
až	až	k9	až
2008	[number]	k4	2008
byl	být	k5eAaImAgInS	být
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
růst	růst	k1gInSc1	růst
stabilní	stabilní	k2eAgInSc1d1	stabilní
a	a	k8xC	a
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
přibližně	přibližně	k6eAd1	přibližně
4	[number]	k4	4
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
globální	globální	k2eAgFnSc2d1	globální
krize	krize	k1gFnSc2	krize
k	k	k7c3	k
meziročnímu	meziroční	k2eAgInSc3d1	meziroční
poklesu	pokles	k1gInSc3	pokles
o	o	k7c4	o
devět	devět	k4xCc4	devět
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
výše	výše	k1gFnSc1	výše
hrubého	hrubý	k2eAgInSc2d1	hrubý
domácího	domácí	k2eAgInSc2d1	domácí
produktu	produkt	k1gInSc2	produkt
(	(	kIx(	(
<g/>
HDP	HDP	kA	HDP
<g/>
)	)	kIx)	)
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
17	[number]	k4	17
331	[number]	k4	331
€	€	k?	€
<g/>
,	,	kIx,	,
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
byla	být	k5eAaImAgFnS	být
5,9	[number]	k4	5,9
%	%	kIx~	%
<g/>
,	,	kIx,	,
inflace	inflace	k1gFnSc1	inflace
0,9	[number]	k4	0,9
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
mzda	mzda	k1gFnSc1	mzda
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
necelých	celý	k2eNgInPc2d1	necelý
1	[number]	k4	1
439	[number]	k4	439
€	€	k?	€
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
evidováno	evidovat	k5eAaImNgNnS	evidovat
přes	přes	k7c4	přes
sto	sto	k4xCgNnSc4	sto
padesát	padesát	k4xCc4	padesát
tisíc	tisíc	k4xCgInPc2	tisíc
podnikatelských	podnikatelský	k2eAgInPc2d1	podnikatelský
subjektů	subjekt	k1gInPc2	subjekt
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
nejznámější	známý	k2eAgFnSc4d3	nejznámější
jsou	být	k5eAaImIp3nP	být
Elan	Elan	k1gNnSc4	Elan
<g/>
,	,	kIx,	,
Gorenje	Gorenje	k1gFnSc1	Gorenje
<g/>
,	,	kIx,	,
Krka	Krka	k1gFnSc1	Krka
<g/>
,	,	kIx,	,
REVOZ	REVOZ	kA	REVOZ
a	a	k8xC	a
Triglav	Triglav	k1gInSc1	Triglav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
zaměstnáno	zaměstnat	k5eAaPmNgNnS	zaměstnat
32	[number]	k4	32
%	%	kIx~	%
pracovních	pracovní	k2eAgFnPc2d1	pracovní
sil	síla	k1gFnPc2	síla
a	a	k8xC	a
průmysl	průmysl	k1gInSc1	průmysl
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
necelých	celý	k2eNgNnPc2d1	necelé
69	[number]	k4	69
%	%	kIx~	%
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějšími	významný	k2eAgNnPc7d3	nejvýznamnější
odvětvími	odvětví	k1gNnPc7	odvětví
jsou	být	k5eAaImIp3nP	být
kovozpracující	kovozpracující	k2eAgInSc4d1	kovozpracující
průmysl	průmysl	k1gInSc4	průmysl
<g/>
,	,	kIx,	,
chemický	chemický	k2eAgInSc4d1	chemický
průmysl	průmysl	k1gInSc4	průmysl
<g/>
,	,	kIx,	,
elektronický	elektronický	k2eAgInSc4d1	elektronický
a	a	k8xC	a
elektrotechnický	elektrotechnický	k2eAgInSc4d1	elektrotechnický
<g/>
,	,	kIx,	,
potravinářský	potravinářský	k2eAgInSc4d1	potravinářský
<g/>
,	,	kIx,	,
papírenský	papírenský	k2eAgInSc4d1	papírenský
průmysl	průmysl	k1gInSc4	průmysl
a	a	k8xC	a
textilní	textilní	k2eAgInSc4d1	textilní
<g/>
,	,	kIx,	,
oděvní	oděvní	k2eAgInSc4d1	oděvní
a	a	k8xC	a
kožedělný	kožedělný	k2eAgInSc4d1	kožedělný
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
slovinské	slovinský	k2eAgNnSc4d1	slovinské
zemědělství	zemědělství	k1gNnSc4	zemědělství
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
maloplošných	maloplošný	k2eAgNnPc2d1	maloplošné
hospodářství	hospodářství	k1gNnPc2	hospodářství
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc4	jenž
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
obhospodařují	obhospodařovat	k5eAaImIp3nP	obhospodařovat
6,5	[number]	k4	6,5
hektaru	hektar	k1gInSc2	hektar
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Necelých	celý	k2eNgInPc2d1	necelý
64	[number]	k4	64
%	%	kIx~	%
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
plochy	plocha	k1gFnSc2	plocha
je	být	k5eAaImIp3nS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
mezi	mezi	k7c4	mezi
malé	malý	k2eAgInPc4d1	malý
statky	statek	k1gInPc4	statek
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
do	do	k7c2	do
15	[number]	k4	15
hektarů	hektar	k1gInPc2	hektar
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zemědělství	zemědělství	k1gNnSc4	zemědělství
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
nízká	nízký	k2eAgFnSc1d1	nízká
produktivita	produktivita	k1gFnSc1	produktivita
<g/>
.	.	kIx.	.
</s>
<s>
Slovinské	slovinský	k2eAgFnPc1d1	slovinská
železnice	železnice	k1gFnPc1	železnice
provozují	provozovat	k5eAaImIp3nP	provozovat
1	[number]	k4	1
228	[number]	k4	228
kilometrů	kilometr	k1gInPc2	kilometr
trati	trať	k1gFnSc2	trať
<g/>
,	,	kIx,	,
rozchod	rozchod	k1gInSc4	rozchod
kolejí	kolej	k1gFnPc2	kolej
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
435	[number]	k4	435
mm	mm	kA	mm
<g/>
,	,	kIx,	,
330	[number]	k4	330
kilometrů	kilometr	k1gInPc2	kilometr
je	být	k5eAaImIp3nS	být
dvoukolejných	dvoukolejný	k2eAgFnPc2d1	dvoukolejná
<g/>
.	.	kIx.	.
</s>
<s>
Železnice	železnice	k1gFnSc1	železnice
protíná	protínat	k5eAaImIp3nS	protínat
všechny	všechen	k3xTgFnPc4	všechen
části	část	k1gFnPc4	část
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Elektrifikace	elektrifikace	k1gFnSc1	elektrifikace
je	být	k5eAaImIp3nS	být
poskytována	poskytovat	k5eAaImNgFnS	poskytovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
systému	systém	k1gInSc2	systém
3	[number]	k4	3
<g/>
kV	kV	k?	kV
DC	DC	kA	DC
a	a	k8xC	a
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
503	[number]	k4	503
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
prvního	první	k4xOgInSc2	první
třicetikilometrového	třicetikilometrový	k2eAgInSc2d1	třicetikilometrový
dálničního	dálniční	k2eAgInSc2d1	dálniční
úseku	úsek	k1gInSc2	úsek
ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
i	i	k8xC	i
v	v	k7c6	v
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
-	-	kIx~	-
Vrhnika	Vrhnik	k1gMnSc2	Vrhnik
-	-	kIx~	-
Postojna	Postojna	k1gFnSc1	Postojna
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Úsek	úsek	k1gInSc1	úsek
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
profinancování	profinancování	k1gNnSc3	profinancování
výstavby	výstavba	k1gFnSc2	výstavba
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
i	i	k9	i
úvěr	úvěr	k1gInSc4	úvěr
od	od	k7c2	od
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
banky	banka	k1gFnSc2	banka
pro	pro	k7c4	pro
obnovu	obnova	k1gFnSc4	obnova
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Výstavbě	výstavba	k1gFnSc3	výstavba
dálnice	dálnice	k1gFnSc1	dálnice
předcházela	předcházet	k5eAaImAgFnS	předcházet
tzv.	tzv.	kA	tzv.
silniční	silniční	k2eAgFnSc1d1	silniční
aféra	aféra	k1gFnSc1	aféra
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
republice	republika	k1gFnSc6	republika
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
187	[number]	k4	187
kilometrů	kilometr	k1gInPc2	kilometr
dálnic	dálnice	k1gFnPc2	dálnice
a	a	k8xC	a
rychlostních	rychlostní	k2eAgFnPc2d1	rychlostní
silnic	silnice	k1gFnPc2	silnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
Národní	národní	k2eAgInSc1d1	národní
program	program	k1gInSc1	program
výstavby	výstavba	k1gFnSc2	výstavba
dálnic	dálnice	k1gFnPc2	dálnice
v	v	k7c6	v
Republice	republika	k1gFnSc6	republika
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
(	(	kIx(	(
<g/>
Nacionalni	Nacionalni	k1gFnPc1	Nacionalni
program	program	k1gInSc4	program
izgradnje	izgradnj	k1gFnSc2	izgradnj
avtocest	avtocest	k5eAaPmF	avtocest
v	v	k7c6	v
Republiki	Republik	k1gFnSc6	Republik
Sloveniji	Sloveniji	k1gFnSc2	Sloveniji
<g/>
,	,	kIx,	,
NPIA	NPIA	kA	NPIA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
počítá	počítat	k5eAaImIp3nS	počítat
především	především	k9	především
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
komunikací	komunikace	k1gFnPc2	komunikace
v	v	k7c6	v
ose	osa	k1gFnSc6	osa
severovýchod	severovýchod	k1gInSc1	severovýchod
<g/>
-	-	kIx~	-
<g/>
jihozápad	jihozápad	k1gInSc1	jihozápad
a	a	k8xC	a
severozápad	severozápad	k1gInSc1	severozápad
<g/>
-	-	kIx~	-
<g/>
jihovýchod	jihovýchod	k1gInSc1	jihovýchod
<g/>
.	.	kIx.	.
</s>
<s>
Dálnice	dálnice	k1gFnPc1	dálnice
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
označovány	označovat	k5eAaImNgInP	označovat
písmenem	písmeno	k1gNnSc7	písmeno
A	a	k8xC	a
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
souvisí	souviset	k5eAaImIp3nS	souviset
se	s	k7c7	s
slovinským	slovinský	k2eAgNnSc7d1	slovinské
pojmenováním	pojmenování	k1gNnSc7	pojmenování
-	-	kIx~	-
avtoceste	avtocést	k5eAaPmRp2nP	avtocést
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
povolená	povolený	k2eAgFnSc1d1	povolená
rychlost	rychlost	k1gFnSc1	rychlost
na	na	k7c6	na
dálnicích	dálnice	k1gFnPc6	dálnice
je	být	k5eAaImIp3nS	být
130	[number]	k4	130
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
hod	hod	k1gInSc1	hod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
byla	být	k5eAaImAgFnS	být
dálniční	dálniční	k2eAgFnSc1d1	dálniční
síť	síť	k1gFnSc1	síť
převedena	převést	k5eAaPmNgFnS	převést
na	na	k7c4	na
Společnost	společnost	k1gFnSc4	společnost
pro	pro	k7c4	pro
dálnice	dálnice	k1gFnPc4	dálnice
v	v	k7c6	v
Republice	republika	k1gFnSc6	republika
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
(	(	kIx(	(
<g/>
Družba	družba	k1gFnSc1	družba
za	za	k7c4	za
avtoceste	avtocést	k5eAaPmRp2nP	avtocést
v	v	k7c6	v
Republiki	Republik	k1gFnSc6	Republik
Sloveniji	Sloveniji	k1gFnSc2	Sloveniji
<g/>
,	,	kIx,	,
DARS	DARS	kA	DARS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dnes	dnes	k6eAd1	dnes
spravuje	spravovat	k5eAaImIp3nS	spravovat
592	[number]	k4	592
kilometrů	kilometr	k1gInPc2	kilometr
dálnic	dálnice	k1gFnPc2	dálnice
a	a	k8xC	a
155	[number]	k4	155
kilometrů	kilometr	k1gInPc2	kilometr
rychlostních	rychlostní	k2eAgFnPc2d1	rychlostní
silnic	silnice	k1gFnPc2	silnice
<g/>
.	.	kIx.	.
</s>
<s>
Rychlostní	rychlostní	k2eAgFnPc1d1	rychlostní
silnice	silnice	k1gFnPc1	silnice
jsou	být	k5eAaImIp3nP	být
označovány	označovat	k5eAaImNgFnP	označovat
písmenem	písmeno	k1gNnSc7	písmeno
H	H	kA	H
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
souvisí	souviset	k5eAaImIp3nS	souviset
se	s	k7c7	s
slovinským	slovinský	k2eAgNnSc7d1	slovinské
pojmenováním	pojmenování	k1gNnSc7	pojmenování
-	-	kIx~	-
hitra	hitra	k6eAd1	hitra
cesta	cesta	k1gFnSc1	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
povolená	povolený	k2eAgFnSc1d1	povolená
rychlost	rychlost	k1gFnSc1	rychlost
na	na	k7c6	na
rychlostních	rychlostní	k2eAgFnPc6d1	rychlostní
silnicích	silnice	k1gFnPc6	silnice
je	být	k5eAaImIp3nS	být
100	[number]	k4	100
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
hod	hod	k1gInSc1	hod
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
slovinským	slovinský	k2eAgInSc7d1	slovinský
přístavem	přístav	k1gInSc7	přístav
Terst	Terst	k1gInSc1	Terst
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
považován	považován	k2eAgMnSc1d1	považován
také	také	k9	také
za	za	k7c4	za
jediný	jediný	k2eAgInSc4d1	jediný
jižní	jižní	k2eAgInSc4d1	jižní
"	"	kIx"	"
<g/>
německý	německý	k2eAgMnSc1d1	německý
<g/>
"	"	kIx"	"
přístav	přístav	k1gInSc1	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
však	však	k9	však
Terst	Terst	k1gInSc1	Terst
stal	stát	k5eAaPmAgInS	stát
posléze	posléze	k6eAd1	posléze
součástí	součást	k1gFnSc7	součást
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
uvědomila	uvědomit	k5eAaPmAgFnS	uvědomit
si	se	k3xPyFc3	se
jugoslávská	jugoslávský	k2eAgFnSc1d1	jugoslávská
vláda	vláda	k1gFnSc1	vláda
nutnost	nutnost	k1gFnSc1	nutnost
vybudování	vybudování	k1gNnSc4	vybudování
přístavu	přístav	k1gInSc2	přístav
nového	nový	k2eAgInSc2d1	nový
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
založen	založit	k5eAaPmNgInS	založit
přístav	přístav	k1gInSc1	přístav
Koper	kopra	k1gFnPc2	kopra
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
otevřel	otevřít	k5eAaPmAgMnS	otevřít
mezinárodnímu	mezinárodní	k2eAgInSc3d1	mezinárodní
obchodu	obchod	k1gInSc3	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Přístav	přístav	k1gInSc1	přístav
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
rozšířen	rozšířen	k2eAgMnSc1d1	rozšířen
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
jím	on	k3xPp3gMnSc7	on
prošlo	projít	k5eAaPmAgNnS	projít
15	[number]	k4	15
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
nákladu	náklad	k1gInSc2	náklad
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
byl	být	k5eAaImAgInS	být
druhým	druhý	k4xOgInSc7	druhý
největším	veliký	k2eAgInSc7d3	veliký
přístavem	přístav	k1gInSc7	přístav
v	v	k7c6	v
Jaderském	jaderský	k2eAgNnSc6d1	Jaderské
moři	moře	k1gNnSc6	moře
po	po	k7c6	po
Terstu	Terst	k1gInSc6	Terst
před	před	k7c7	před
Rijekou	Rijeka	k1gFnSc7	Rijeka
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
rozšíření	rozšíření	k1gNnSc1	rozšíření
přístavu	přístav	k1gInSc2	přístav
je	být	k5eAaImIp3nS	být
závislé	závislý	k2eAgNnSc1d1	závislé
na	na	k7c4	na
vybudování	vybudování	k1gNnSc4	vybudování
železničního	železniční	k2eAgNnSc2d1	železniční
napojení	napojení	k1gNnSc2	napojení
<g/>
.	.	kIx.	.
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgNnPc4	tři
mezinárodní	mezinárodní	k2eAgNnPc4d1	mezinárodní
letiště	letiště	k1gNnPc4	letiště
<g/>
:	:	kIx,	:
Letiště	letiště	k1gNnPc1	letiště
Jože	Joža	k1gFnSc6	Joža
Pučnika	Pučnik	k1gMnSc2	Pučnik
nedaleko	nedaleko	k7c2	nedaleko
Lublaně	Lublaň	k1gFnSc2	Lublaň
<g/>
,	,	kIx,	,
Letiště	letiště	k1gNnSc4	letiště
Edvarda	Edvard	k1gMnSc2	Edvard
Rusjana	Rusjan	k1gMnSc2	Rusjan
u	u	k7c2	u
Mariboru	Maribor	k1gInSc2	Maribor
a	a	k8xC	a
malé	malý	k2eAgNnSc4d1	malé
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
letiště	letiště	k1gNnSc4	letiště
v	v	k7c4	v
Sečovlje	Sečovlj	k1gFnPc4	Sečovlj
nedaleko	nedaleko	k7c2	nedaleko
Portorože	Portorož	k1gFnSc2	Portorož
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Cerklje	Cerklj	k1gFnPc4	Cerklj
ob	ob	k7c4	ob
Krki	Krki	k1gNnSc4	Krki
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
vojenské	vojenský	k2eAgNnSc1d1	vojenské
letiště	letiště	k1gNnSc1	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
nastal	nastat	k5eAaPmAgInS	nastat
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
devatenáctého	devatenáctý	k4xOgNnSc2	devatenáctý
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
dostavěna	dostavěn	k2eAgFnSc1d1	dostavěna
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
spojující	spojující	k2eAgFnSc2d1	spojující
Terst	Terst	k1gInSc4	Terst
s	s	k7c7	s
podunajským	podunajský	k2eAgNnSc7d1	podunajské
vnitrozemím	vnitrozemí	k1gNnSc7	vnitrozemí
<g/>
,	,	kIx,	,
a	a	k8xC	a
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
byl	být	k5eAaImAgInS	být
spojen	spojit	k5eAaPmNgInS	spojit
především	především	k6eAd1	především
se	s	k7c7	s
sídly	sídlo	k1gNnPc7	sídlo
Bled	Bled	k1gMnSc1	Bled
a	a	k8xC	a
Postojna	Postojna	k1gFnSc1	Postojna
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
rozvoj	rozvoj	k1gInSc4	rozvoj
turismu	turismus	k1gInSc2	turismus
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
spojen	spojen	k2eAgInSc1d1	spojen
s	s	k7c7	s
reformní	reformní	k2eAgFnSc7d1	reformní
vládou	vláda	k1gFnSc7	vláda
Stane	stanout	k5eAaPmIp3nS	stanout
Kavčiče	Kavčič	k1gMnPc4	Kavčič
<g/>
.	.	kIx.	.
</s>
<s>
Klíčovým	klíčový	k2eAgInSc7d1	klíčový
zákonem	zákon	k1gInSc7	zákon
pro	pro	k7c4	pro
oblast	oblast	k1gFnSc4	oblast
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
je	být	k5eAaImIp3nS	být
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
podpoře	podpora	k1gFnSc6	podpora
rozvoje	rozvoj	k1gInSc2	rozvoj
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
z	z	k7c2	z
prosince	prosinec	k1gInSc2	prosinec
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
národní	národní	k2eAgFnSc7d1	národní
turistickou	turistický	k2eAgFnSc7d1	turistická
organizací	organizace	k1gFnSc7	organizace
je	být	k5eAaImIp3nS	být
Slovinská	slovinský	k2eAgFnSc1d1	slovinská
organizace	organizace	k1gFnSc1	organizace
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
(	(	kIx(	(
<g/>
Slovenska	Slovensko	k1gNnSc2	Slovensko
Turistična	Turistično	k1gNnSc2	Turistično
Organizacija	Organizacijum	k1gNnSc2	Organizacijum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
se	se	k3xPyFc4	se
konává	konávat	k5eAaImIp3nS	konávat
veletrh	veletrh	k1gInSc1	veletrh
zaměřený	zaměřený	k2eAgInSc1d1	zaměřený
na	na	k7c4	na
cestovní	cestovní	k2eAgInSc4d1	cestovní
ruch	ruch	k1gInSc4	ruch
-	-	kIx~	-
Alpe-Adria	Alpe-Adrium	k1gNnSc2	Alpe-Adrium
<g/>
:	:	kIx,	:
Turizem	Turiz	k1gMnSc7	Turiz
in	in	k?	in
prosti	prost	k2eAgMnPc1d1	prost
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Školství	školství	k1gNnSc2	školství
ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1693	[number]	k4	1693
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
Lublani	Lublaň	k1gFnSc6	Lublaň
první	první	k4xOgFnSc1	první
učená	učený	k2eAgFnSc1d1	učená
společnost	společnost	k1gFnSc1	společnost
sdružující	sdružující	k2eAgMnPc4d1	sdružující
lékaře	lékař	k1gMnPc4	lékař
<g/>
,	,	kIx,	,
právníky	právník	k1gMnPc4	právník
a	a	k8xC	a
teology	teolog	k1gMnPc4	teolog
-	-	kIx~	-
Academia	academia	k1gFnSc1	academia
Operosorum	Operosorum	k1gInSc1	Operosorum
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
fungovala	fungovat	k5eAaImAgFnS	fungovat
skoro	skoro	k6eAd1	skoro
celé	celý	k2eAgNnSc4d1	celé
čtvrtstoletí	čtvrtstoletí	k1gNnSc4	čtvrtstoletí
<g/>
.	.	kIx.	.
</s>
<s>
Významnými	významný	k2eAgFnPc7d1	významná
osobnostmi	osobnost	k1gFnPc7	osobnost
slovinské	slovinský	k2eAgFnSc2d1	slovinská
vědy	věda	k1gFnSc2	věda
jsou	být	k5eAaImIp3nP	být
Janez	Janez	k1gMnSc1	Janez
Vajkard	Vajkard	k1gMnSc1	Vajkard
Valvasor	Valvasor	k1gMnSc1	Valvasor
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
vydal	vydat	k5eAaPmAgMnS	vydat
německy	německy	k6eAd1	německy
psanou	psaný	k2eAgFnSc4d1	psaná
encyklopedickou	encyklopedický	k2eAgFnSc4d1	encyklopedická
práci	práce	k1gFnSc4	práce
Sláva	Sláva	k1gFnSc1	Sláva
vévodství	vévodství	k1gNnSc2	vévodství
kraňského	kraňský	k2eAgNnSc2d1	kraňské
(	(	kIx(	(
<g/>
Die	Die	k1gFnSc4	Die
Ehre	Ehr	k1gInSc2	Ehr
des	des	k1gNnSc2	des
Hertzogthums	Hertzogthumsa	k1gFnPc2	Hertzogthumsa
Crain	Craina	k1gFnPc2	Craina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
fyzik	fyzik	k1gMnSc1	fyzik
Jurij	Jurij	k1gMnSc1	Jurij
Vega	Vega	k1gMnSc1	Vega
<g/>
,	,	kIx,	,
fyzik	fyzik	k1gMnSc1	fyzik
Jožef	Jožef	k1gMnSc1	Jožef
Stefan	Stefan	k1gMnSc1	Stefan
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
Peter	Peter	k1gMnSc1	Peter
Kozler	Kozler	k1gMnSc1	Kozler
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
autorem	autor	k1gMnSc7	autor
podrobné	podrobný	k2eAgFnSc2d1	podrobná
mapy	mapa	k1gFnSc2	mapa
území	území	k1gNnSc2	území
osídleného	osídlený	k2eAgNnSc2d1	osídlené
slovinským	slovinský	k2eAgNnSc7d1	slovinské
etnikem	etnikum	k1gNnSc7	etnikum
<g/>
,	,	kIx,	,
letecký	letecký	k2eAgMnSc1d1	letecký
konstruktér	konstruktér	k1gMnSc1	konstruktér
Edvard	Edvard	k1gMnSc1	Edvard
Rusjan	Rusjan	k1gMnSc1	Rusjan
či	či	k8xC	či
jazykovědec	jazykovědec	k1gMnSc1	jazykovědec
Franc	Franc	k1gMnSc1	Franc
Miklošič	Miklošič	k1gMnSc1	Miklošič
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgMnSc7d1	jediný
slovinským	slovinský	k2eAgMnSc7d1	slovinský
nositelem	nositel	k1gMnSc7	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
je	být	k5eAaImIp3nS	být
Friderik	Friderik	k1gMnSc1	Friderik
Pregl	Pregl	k1gMnSc1	Pregl
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
však	však	k9	však
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
rakouském	rakouský	k2eAgInSc6d1	rakouský
Grazu	Graz	k1gInSc6	Graz
<g/>
.	.	kIx.	.
</s>
<s>
Školský	školský	k2eAgInSc1d1	školský
systém	systém	k1gInSc1	systém
ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
obecnou	obecný	k2eAgFnSc4d1	obecná
část	část	k1gFnSc4	část
a	a	k8xC	a
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
část	část	k1gFnSc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Obecnou	obecný	k2eAgFnSc4d1	obecná
část	část	k1gFnSc4	část
tvoří	tvořit	k5eAaImIp3nP	tvořit
předškolní	předškolní	k2eAgNnSc4d1	předškolní
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
,	,	kIx,	,
základní	základní	k2eAgNnSc4d1	základní
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
,	,	kIx,	,
střední	střední	k2eAgNnSc4d1	střední
vzdělání	vzdělání	k1gNnSc4	vzdělání
(	(	kIx(	(
<g/>
odborné	odborný	k2eAgNnSc4d1	odborné
a	a	k8xC	a
technické	technický	k2eAgNnSc4d1	technické
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
<g/>
,	,	kIx,	,
střední	střední	k2eAgNnSc4d1	střední
všeobecné	všeobecný	k2eAgNnSc4d1	všeobecné
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyšší	vysoký	k2eAgNnSc4d2	vyšší
odborné	odborný	k2eAgNnSc4d1	odborné
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
a	a	k8xC	a
terciární	terciární	k2eAgNnSc4d1	terciární
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
část	část	k1gFnSc1	část
systému	systém	k1gInSc2	systém
tvoří	tvořit	k5eAaImIp3nS	tvořit
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgNnSc4d1	hudební
a	a	k8xC	a
taneční	taneční	k2eAgNnSc4d1	taneční
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
<g/>
,	,	kIx,	,
speciální	speciální	k2eAgNnSc4d1	speciální
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
<g/>
,	,	kIx,	,
modifikované	modifikovaný	k2eAgInPc4d1	modifikovaný
vzdělávací	vzdělávací	k2eAgInPc4d1	vzdělávací
programy	program	k1gInPc4	program
a	a	k8xC	a
programy	program	k1gInPc4	program
v	v	k7c6	v
etnicky	etnicky	k6eAd1	etnicky
a	a	k8xC	a
jazykově	jazykově	k6eAd1	jazykově
smíšených	smíšený	k2eAgFnPc6d1	smíšená
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Školní	školní	k2eAgInSc1d1	školní
rok	rok	k1gInSc1	rok
trvá	trvat	k5eAaImIp3nS	trvat
ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
do	do	k7c2	do
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
školním	školní	k2eAgInSc6d1	školní
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
/	/	kIx~	/
<g/>
2009	[number]	k4	2009
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
791	[number]	k4	791
základních	základní	k2eAgFnPc2d1	základní
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
studovalo	studovat	k5eAaImAgNnS	studovat
necelých	celý	k2eNgInPc2d1	necelý
162	[number]	k4	162
tisíc	tisíc	k4xCgInPc2	tisíc
žáků	žák	k1gMnPc2	žák
<g/>
,	,	kIx,	,
129	[number]	k4	129
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgNnPc6	jenž
studovalo	studovat	k5eAaImAgNnS	studovat
přes	přes	k7c4	přes
87	[number]	k4	87
tisíc	tisíc	k4xCgInPc2	tisíc
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
terciárního	terciární	k2eAgNnSc2d1	terciární
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
působilo	působit	k5eAaImAgNnS	působit
89	[number]	k4	89
institucí	instituce	k1gFnPc2	instituce
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgNnPc6	jenž
studovalo	studovat	k5eAaImAgNnS	studovat
přes	přes	k7c4	přes
114	[number]	k4	114
tisíc	tisíc	k4xCgInPc2	tisíc
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
působí	působit	k5eAaImIp3nS	působit
čtyři	čtyři	k4xCgFnPc1	čtyři
univerzity	univerzita	k1gFnPc1	univerzita
<g/>
:	:	kIx,	:
Univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Lublani	Lublaň	k1gFnSc6	Lublaň
<g/>
,	,	kIx,	,
Univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Mariboru	Maribor	k1gMnSc6	Maribor
<g/>
,	,	kIx,	,
Primorská	Primorský	k2eAgFnSc1d1	Primorský
univerzita	univerzita	k1gFnSc1	univerzita
a	a	k8xC	a
Univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Nove	nov	k1gInSc5	nov
Gorici	Goric	k1gMnPc7	Goric
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Demografie	demografie	k1gFnSc2	demografie
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1857	[number]	k4	1857
žilo	žít	k5eAaImAgNnS	žít
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
1	[number]	k4	1
101	[number]	k4	101
854	[number]	k4	854
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
jejich	jejich	k3xOp3gInSc4	jejich
počet	počet	k1gInSc4	počet
až	až	k9	až
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
stoupal	stoupat	k5eAaImAgInS	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
bylo	být	k5eAaImAgNnS	být
období	období	k1gNnSc1	období
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
:	:	kIx,	:
jestliže	jestliže	k8xS	jestliže
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
žilo	žít	k5eAaImAgNnS	žít
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
1	[number]	k4	1
321	[number]	k4	321
0	[number]	k4	0
<g/>
98	[number]	k4	98
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
již	již	k6eAd1	již
jen	jen	k9	jen
1	[number]	k4	1
054	[number]	k4	054
919	[number]	k4	919
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
počet	počet	k1gInSc4	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
opět	opět	k6eAd1	opět
narůstal	narůstat	k5eAaImAgInS	narůstat
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1961	[number]	k4	1961
až	až	k9	až
1991	[number]	k4	1991
absolutní	absolutní	k2eAgInSc1d1	absolutní
počet	počet	k1gInSc1	počet
Slovinců	Slovinec	k1gMnPc2	Slovinec
rostl	růst	k5eAaImAgInS	růst
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
procentuální	procentuální	k2eAgInSc4d1	procentuální
podíl	podíl	k1gInSc4	podíl
s	s	k7c7	s
rostoucím	rostoucí	k2eAgNnSc7d1	rostoucí
zastoupením	zastoupení	k1gNnSc7	zastoupení
Srbů	Srb	k1gMnPc2	Srb
a	a	k8xC	a
Chorvatů	Chorvat	k1gMnPc2	Chorvat
klesal	klesat	k5eAaImAgInS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
1	[number]	k4	1
964	[number]	k4	964
036	[number]	k4	036
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
etnického	etnický	k2eAgNnSc2d1	etnické
složení	složení	k1gNnSc2	složení
mají	mít	k5eAaImIp3nP	mít
většinu	většina	k1gFnSc4	většina
Slovinci	Slovinec	k1gMnSc3	Slovinec
(	(	kIx(	(
<g/>
83,06	[number]	k4	83,06
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následují	následovat	k5eAaImIp3nP	následovat
je	on	k3xPp3gFnPc4	on
Srbové	Srbová	k1gFnPc4	Srbová
(	(	kIx(	(
<g/>
1,98	[number]	k4	1,98
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Chorvaté	Chorvat	k1gMnPc1	Chorvat
(	(	kIx(	(
<g/>
1,81	[number]	k4	1,81
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Bosňáci	Bosňáci	k?	Bosňáci
(	(	kIx(	(
<g/>
1,1	[number]	k4	1,1
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čechů	Čech	k1gMnPc2	Čech
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
době	doba	k1gFnSc6	doba
sčítání	sčítání	k1gNnSc2	sčítání
ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
273	[number]	k4	273
<g/>
,	,	kIx,	,
Slováků	Slovák	k1gMnPc2	Slovák
216	[number]	k4	216
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Náboženství	náboženství	k1gNnSc2	náboženství
ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
posledního	poslední	k2eAgNnSc2d1	poslední
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
k	k	k7c3	k
římskokatolické	římskokatolický	k2eAgFnSc3d1	Římskokatolická
církvi	církev	k1gFnSc3	církev
přihlásilo	přihlásit	k5eAaPmAgNnS	přihlásit
57,8	[number]	k4	57,8
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
evangelíků	evangelík	k1gMnPc2	evangelík
bylo	být	k5eAaImAgNnS	být
0,8	[number]	k4	0,8
%	%	kIx~	%
<g/>
,	,	kIx,	,
pravoslavných	pravoslavný	k2eAgInPc2d1	pravoslavný
2,3	[number]	k4	2,3
%	%	kIx~	%
<g/>
,	,	kIx,	,
muslimů	muslim	k1gMnPc2	muslim
2,4	[number]	k4	2,4
%	%	kIx~	%
<g/>
,	,	kIx,	,
věřících	věřící	k1gMnPc2	věřící
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
nehlásí	hlásit	k5eNaImIp3nP	hlásit
k	k	k7c3	k
žádné	žádný	k3yNgFnSc3	žádný
církvi	církev	k1gFnSc3	církev
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
3,5	[number]	k4	3,5
%	%	kIx~	%
<g/>
,	,	kIx,	,
ateistů	ateista	k1gMnPc2	ateista
bylo	být	k5eAaImAgNnS	být
10,1	[number]	k4	10,1
%	%	kIx~	%
a	a	k8xC	a
Židů	Žid	k1gMnPc2	Žid
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
99	[number]	k4	99
<g/>
.	.	kIx.	.
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
tradice	tradice	k1gFnSc1	tradice
sahá	sahat	k5eAaImIp3nS	sahat
ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
až	až	k9	až
do	do	k7c2	do
osmého	osmý	k4xOgNnSc2	osmý
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
druhá	druhý	k4xOgFnSc1	druhý
vlna	vlna	k1gFnSc1	vlna
christianizace	christianizace	k1gFnSc2	christianizace
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc7	první
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
římské	římský	k2eAgFnSc6d1	římská
době	doba	k1gFnSc6	doba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začal	začít	k5eAaPmAgMnS	začít
do	do	k7c2	do
slovinských	slovinský	k2eAgFnPc2d1	slovinská
oblastí	oblast	k1gFnPc2	oblast
pronikat	pronikat	k5eAaImF	pronikat
vliv	vliv	k1gInSc4	vliv
humanismu	humanismus	k1gInSc2	humanismus
a	a	k8xC	a
renesance	renesance	k1gFnSc2	renesance
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtyřicátých	čtyřicátý	k4xOgNnPc6	čtyřicátý
letech	léto	k1gNnPc6	léto
šestnáctého	šestnáctý	k4xOgNnSc2	šestnáctý
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
zformoval	zformovat	k5eAaPmAgInS	zformovat
první	první	k4xOgInSc1	první
protestantský	protestantský	k2eAgInSc1d1	protestantský
kroužek	kroužek	k1gInSc1	kroužek
<g/>
.	.	kIx.	.
</s>
<s>
Důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
rodný	rodný	k2eAgInSc4d1	rodný
jazyk	jazyk	k1gInSc4	jazyk
způsobil	způsobit	k5eAaPmAgMnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
první	první	k4xOgNnPc1	první
literární	literární	k2eAgNnPc1d1	literární
díla	dílo	k1gNnPc1	dílo
ve	v	k7c6	v
slovanském	slovanský	k2eAgInSc6d1	slovanský
(	(	kIx(	(
<g/>
slovinském	slovinský	k2eAgInSc6d1	slovinský
<g/>
)	)	kIx)	)
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1590	[number]	k4	1590
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
území	území	k1gNnSc4	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
protireformace	protireformace	k1gFnSc2	protireformace
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
vrátila	vrátit	k5eAaPmAgFnS	vrátit
ke	k	k7c3	k
katolictví	katolictví	k1gNnSc3	katolictví
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nachází	nacházet	k5eAaImIp3nS	nacházet
přibližně	přibližně	k6eAd1	přibližně
tři	tři	k4xCgInPc4	tři
tisíce	tisíc	k4xCgInPc4	tisíc
kostelů	kostel	k1gInPc2	kostel
a	a	k8xC	a
kaplí	kaple	k1gFnPc2	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kultura	kultura	k1gFnSc1	kultura
ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
<g/>
.	.	kIx.	.
</s>
<s>
Slovinská	slovinský	k2eAgFnSc1d1	slovinská
národní	národní	k2eAgFnSc1d1	národní
identita	identita	k1gFnSc1	identita
je	být	k5eAaImIp3nS	být
postavena	postaven	k2eAgFnSc1d1	postavena
především	především	k9	především
na	na	k7c6	na
slovinském	slovinský	k2eAgInSc6d1	slovinský
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
dvě	dva	k4xCgFnPc4	dva
knihy	kniha	k1gFnPc4	kniha
vydané	vydaný	k2eAgFnPc4d1	vydaná
ve	v	k7c6	v
slovanském	slovanský	k2eAgInSc6d1	slovanský
(	(	kIx(	(
<g/>
slovinském	slovinský	k2eAgInSc6d1	slovinský
<g/>
)	)	kIx)	)
jazyce	jazyk	k1gInSc6	jazyk
vydal	vydat	k5eAaPmAgMnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1550	[number]	k4	1550
Primož	Primož	k1gFnSc4	Primož
Trubar	Trubara	k1gFnPc2	Trubara
<g/>
.	.	kIx.	.
</s>
<s>
Knižní	knižní	k2eAgInSc1d1	knižní
standard	standard	k1gInSc1	standard
jazyka	jazyk	k1gInSc2	jazyk
položil	položit	k5eAaPmAgInS	položit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1584	[number]	k4	1584
svým	svůj	k3xOyFgInSc7	svůj
překladem	překlad	k1gInSc7	překlad
Bible	bible	k1gFnSc1	bible
Jurij	Jurij	k1gMnSc1	Jurij
Dalmatin	dalmatin	k1gMnSc1	dalmatin
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
utlumení	utlumení	k1gNnSc3	utlumení
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
období	období	k1gNnSc6	období
protireformace	protireformace	k1gFnSc2	protireformace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1768	[number]	k4	1768
vydal	vydat	k5eAaPmAgMnS	vydat
Marko	Marko	k1gMnSc1	Marko
Pohlin	Pohlin	k2eAgMnSc1d1	Pohlin
německy	německy	k6eAd1	německy
psanou	psaný	k2eAgFnSc4d1	psaná
Kraňskou	kraňský	k2eAgFnSc4d1	kraňská
gramatiku	gramatika	k1gFnSc4	gramatika
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
počátek	počátek	k1gInSc4	počátek
slovinského	slovinský	k2eAgNnSc2d1	slovinské
národního	národní	k2eAgNnSc2d1	národní
probuzení	probuzení	k1gNnSc2	probuzení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1784	[number]	k4	1784
až	až	k9	až
1802	[number]	k4	1802
je	být	k5eAaImIp3nS	být
znovu	znovu	k6eAd1	znovu
přeložena	přeložen	k2eAgFnSc1d1	přeložena
Bible	bible	k1gFnSc1	bible
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tento	tento	k3xDgInSc1	tento
počin	počin	k1gInSc1	počin
má	mít	k5eAaImIp3nS	mít
ukázat	ukázat	k5eAaPmF	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jazyk	jazyk	k1gInSc1	jazyk
Slovanů	Slovan	k1gMnPc2	Slovan
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
Kraňsku	Kraňsko	k1gNnSc6	Kraňsko
<g/>
,	,	kIx,	,
Korutanech	Korutany	k1gInPc6	Korutany
a	a	k8xC	a
Štýrsku	Štýrsko	k1gNnSc6	Štýrsko
představuje	představovat	k5eAaImIp3nS	představovat
jeden	jeden	k4xCgInSc4	jeden
celek	celek	k1gInSc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1797	[number]	k4	1797
začaly	začít	k5eAaPmAgInP	začít
vycházet	vycházet	k5eAaImF	vycházet
první	první	k4xOgFnSc6	první
slovanským	slovanský	k2eAgInSc7d1	slovanský
(	(	kIx(	(
<g/>
slovinským	slovinský	k2eAgInSc7d1	slovinský
<g/>
)	)	kIx)	)
jazykem	jazyk	k1gInSc7	jazyk
psané	psaný	k2eAgFnSc2d1	psaná
Lublanske	Lublansk	k1gFnSc2	Lublansk
novice	novice	k1gFnSc2	novice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
osmnáctého	osmnáctý	k4xOgNnSc2	osmnáctý
a	a	k8xC	a
devatenáctého	devatenáctý	k4xOgNnSc2	devatenáctý
století	století	k1gNnSc6	století
Slovinci	Slovinec	k1gMnPc1	Slovinec
převzali	převzít	k5eAaPmAgMnP	převzít
ze	z	k7c2	z
sousedních	sousední	k2eAgInPc2d1	sousední
jihoslovanských	jihoslovanský	k2eAgInPc2d1	jihoslovanský
jazyků	jazyk	k1gInPc2	jazyk
označení	označení	k1gNnPc2	označení
Slaven	slaven	k2eAgInSc1d1	slaven
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
ho	on	k3xPp3gNnSc4	on
používat	používat	k5eAaImF	používat
pro	pro	k7c4	pro
obecné	obecný	k2eAgNnSc4d1	obecné
označení	označení	k1gNnSc4	označení
slovanské	slovanský	k2eAgFnSc2d1	Slovanská
identity	identita	k1gFnSc2	identita
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgNnSc1d1	původní
označení	označení	k1gNnSc1	označení
Sloven	Slovena	k1gFnPc2	Slovena
a	a	k8xC	a
slovenski	slovenski	k1gNnSc1	slovenski
jezik	jezikum	k1gNnPc2	jezikum
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
chápat	chápat	k5eAaImF	chápat
jako	jako	k9	jako
Slovinci	Slovinec	k1gMnPc1	Slovinec
a	a	k8xC	a
slovinský	slovinský	k2eAgInSc4d1	slovinský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
postavou	postava	k1gFnSc7	postava
devatenáctého	devatenáctý	k4xOgNnSc2	devatenáctý
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
France	Franc	k1gMnPc4	Franc
Prešeren	Prešerna	k1gFnPc2	Prešerna
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
významnými	významný	k2eAgMnPc7d1	významný
literáty	literát	k1gMnPc7	literát
byli	být	k5eAaImAgMnP	být
Ivan	Ivan	k1gMnSc1	Ivan
Cankar	Cankar	k1gMnSc1	Cankar
<g/>
,	,	kIx,	,
Srečko	Srečko	k6eAd1	Srečko
Kosovel	Kosovel	k1gMnSc1	Kosovel
a	a	k8xC	a
Edvard	Edvard	k1gMnSc1	Edvard
Kocbek	Kocbek	k1gMnSc1	Kocbek
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějšími	významný	k2eAgMnPc7d3	nejvýznamnější
autory	autor	k1gMnPc7	autor
současnosti	současnost	k1gFnSc2	současnost
jsou	být	k5eAaImIp3nP	být
Aleš	Aleš	k1gMnSc1	Aleš
Debeljak	Debeljak	k1gMnSc1	Debeljak
<g/>
,	,	kIx,	,
Drago	Drago	k1gMnSc1	Drago
Jančar	jančar	k1gInSc1	jančar
a	a	k8xC	a
Tomaž	Tomaž	k1gFnSc1	Tomaž
Šalamun	Šalamuna	k1gFnPc2	Šalamuna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
vydáno	vydat	k5eAaPmNgNnS	vydat
1473	[number]	k4	1473
titulů	titul	k1gInPc2	titul
<g/>
.	.	kIx.	.
</s>
<s>
Intenzivní	intenzivní	k2eAgInSc1d1	intenzivní
rozvoj	rozvoj	k1gInSc1	rozvoj
stavebnictví	stavebnictví	k1gNnSc2	stavebnictví
nastal	nastat	k5eAaPmAgInS	nastat
zejména	zejména	k9	zejména
v	v	k7c6	v
období	období	k1gNnSc6	období
baroka	baroko	k1gNnSc2	baroko
<g/>
.	.	kIx.	.
</s>
<s>
Italští	italský	k2eAgMnPc1d1	italský
architekti	architekt	k1gMnPc1	architekt
Andrea	Andrea	k1gFnSc1	Andrea
Pozzo	Pozza	k1gFnSc5	Pozza
a	a	k8xC	a
Francesco	Francesco	k1gMnSc1	Francesco
Robba	Robba	k1gMnSc1	Robba
ovlivnili	ovlivnit	k5eAaPmAgMnP	ovlivnit
tvář	tvář	k1gFnSc4	tvář
Lublaně	Lublaň	k1gFnSc2	Lublaň
<g/>
.	.	kIx.	.
</s>
<s>
Lublaňskou	lublaňský	k2eAgFnSc4d1	Lublaňská
radnici	radnice	k1gFnSc4	radnice
<g/>
,	,	kIx,	,
vybudovanou	vybudovaný	k2eAgFnSc4d1	vybudovaná
také	také	k9	také
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
vystavěl	vystavět	k5eAaPmAgMnS	vystavět
Gregor	Gregor	k1gMnSc1	Gregor
Maček	mačka	k1gFnPc2	mačka
<g/>
.	.	kIx.	.
</s>
<s>
Barokní	barokní	k2eAgInSc4d1	barokní
ráz	ráz	k1gInSc4	ráz
Lublaně	Lublaň	k1gFnSc2	Lublaň
vydržel	vydržet	k5eAaPmAgMnS	vydržet
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Lublaň	Lublaň	k1gFnSc1	Lublaň
postihlo	postihnout	k5eAaPmAgNnS	postihnout
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
Starosta	Starosta	k1gMnSc1	Starosta
Ivan	Ivan	k1gMnSc1	Ivan
Hribar	Hribar	k1gMnSc1	Hribar
poté	poté	k6eAd1	poté
do	do	k7c2	do
města	město	k1gNnSc2	město
pozval	pozvat	k5eAaPmAgMnS	pozvat
přední	přední	k2eAgMnPc4d1	přední
architekty	architekt	k1gMnPc4	architekt
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
slovinským	slovinský	k2eAgMnSc7d1	slovinský
architektem	architekt	k1gMnSc7	architekt
byl	být	k5eAaImAgMnS	být
Jože	Joža	k1gFnSc3	Joža
Plečnik	Plečnik	k1gMnSc1	Plečnik
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgInS	být
také	také	k9	také
autorem	autor	k1gMnSc7	autor
úprav	úprava	k1gFnPc2	úprava
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
Plečnik	Plečnik	k1gInSc1	Plečnik
věnovat	věnovat	k5eAaImF	věnovat
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
Lublani	Lublaň	k1gFnSc6	Lublaň
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
Plečnikových	Plečnikův	k2eAgMnPc2d1	Plečnikův
žáků	žák	k1gMnPc2	žák
-	-	kIx~	-
Edvard	Edvard	k1gMnSc1	Edvard
Ravnikar	Ravnikar	k1gMnSc1	Ravnikar
-	-	kIx~	-
pak	pak	k6eAd1	pak
jako	jako	k8xS	jako
profesor	profesor	k1gMnSc1	profesor
architektury	architektura	k1gFnSc2	architektura
na	na	k7c6	na
Fakultě	fakulta	k1gFnSc6	fakulta
architektury	architektura	k1gFnSc2	architektura
Univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Lublani	Lublaň	k1gFnSc6	Lublaň
ovlivnil	ovlivnit	k5eAaPmAgMnS	ovlivnit
generace	generace	k1gFnPc4	generace
slovinských	slovinský	k2eAgMnPc2d1	slovinský
architektů	architekt	k1gMnPc2	architekt
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc4	jeho
vliv	vliv	k1gInSc4	vliv
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
patrný	patrný	k2eAgInSc1d1	patrný
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
stavbách	stavba	k1gFnPc6	stavba
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
slovinskými	slovinský	k2eAgFnPc7d1	slovinská
divadelními	divadelní	k2eAgFnPc7d1	divadelní
scénami	scéna	k1gFnPc7	scéna
jsou	být	k5eAaImIp3nP	být
Slovinské	slovinský	k2eAgNnSc4d1	slovinské
národní	národní	k2eAgNnSc4d1	národní
divadlo	divadlo	k1gNnSc4	divadlo
(	(	kIx(	(
<g/>
SND	SND	kA	SND
<g/>
)	)	kIx)	)
v	v	k7c6	v
Lublani	Lublaň	k1gFnSc6	Lublaň
<g/>
,	,	kIx,	,
SND	SND	kA	SND
v	v	k7c6	v
Mariboru	Maribor	k1gInSc6	Maribor
<g/>
,	,	kIx,	,
SND	SND	kA	SND
v	v	k7c6	v
Nové	Nové	k2eAgFnSc6d1	Nové
Gorici	Gorice	k1gFnSc6	Gorice
a	a	k8xC	a
Prešerenovo	Prešerenův	k2eAgNnSc1d1	Prešerenovo
divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Kranji	Kranj	k1gFnSc6	Kranj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
57	[number]	k4	57
kin	kino	k1gNnPc2	kino
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
slovinské	slovinský	k2eAgInPc1d1	slovinský
filmy	film	k1gInPc1	film
(	(	kIx(	(
<g/>
Sejem	Sejem	k?	Sejem
v	v	k7c6	v
Ljutomeru	Ljutomer	k1gInSc6	Ljutomer
<g/>
,	,	kIx,	,
Odhod	odhod	k1gInSc1	odhod
od	od	k7c2	od
maše	macha	k1gFnSc6	macha
v	v	k7c6	v
Ljutomeru	Ljutomer	k1gInSc6	Ljutomer
a	a	k8xC	a
Na	na	k7c4	na
domačem	domač	k1gInSc7	domač
vrtu	vrt	k1gInSc2	vrt
<g/>
)	)	kIx)	)
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
přelomu	přelom	k1gInSc2	přelom
let	léto	k1gNnPc2	léto
1905	[number]	k4	1905
a	a	k8xC	a
1906	[number]	k4	1906
a	a	k8xC	a
jejich	jejich	k3xOp3gMnSc7	jejich
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
Karol	Karol	k1gInSc4	Karol
Grossmann	Grossmann	k1gMnSc1	Grossmann
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
větším	veliký	k2eAgInSc7d2	veliký
počinem	počin	k1gInSc7	počin
byl	být	k5eAaImAgInS	být
němý	němý	k2eAgInSc1d1	němý
dokument	dokument	k1gInSc1	dokument
V	v	k7c6	v
kraljestvu	kraljestvo	k1gNnSc6	kraljestvo
zlatoroga	zlatorog	k1gMnSc2	zlatorog
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
pochází	pocházet	k5eAaImIp3nS	pocházet
film	film	k1gInSc1	film
Triglavske	Triglavsk	k1gFnSc2	Triglavsk
strmine	strmin	k1gInSc5	strmin
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
opatřen	opatřen	k2eAgInSc1d1	opatřen
zvukovým	zvukový	k2eAgInSc7d1	zvukový
komentářem	komentář	k1gInSc7	komentář
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
film	film	k1gInSc4	film
natočený	natočený	k2eAgInSc4d1	natočený
ve	v	k7c6	v
slovinském	slovinský	k2eAgInSc6d1	slovinský
jazyce	jazyk	k1gInSc6	jazyk
bylo	být	k5eAaImAgNnS	být
dílo	dílo	k1gNnSc1	dílo
Na	na	k7c4	na
svoji	svůj	k3xOyFgFnSc4	svůj
zemlji	zemlje	k1gFnSc4	zemlje
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
natočeno	natočit	k5eAaBmNgNnS	natočit
130	[number]	k4	130
slovinských	slovinský	k2eAgInPc2d1	slovinský
celovečerních	celovečerní	k2eAgInPc2d1	celovečerní
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
