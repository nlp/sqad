<s>
Deoxyribonukleová	deoxyribonukleový	k2eAgFnSc1d1
kyselina	kyselina	k1gFnSc1
<g/>
,	,	kIx,
běžně	běžně	k6eAd1
označovaná	označovaný	k2eAgFnSc1d1
DNA	dna	kA
(	(	kIx(
<g/>
z	z	k7c2
anglického	anglický	k2eAgInSc2d1
deoxyribonucleic	deoxyribonucleic	k1gInPc2
acid	 acid		k1gNnPc2
<g/>
,	,	kIx,
česky	česky	k6eAd1
zřídka	zřídka	k6eAd1
i	i	k9
DNK	DNK	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nukleová	nukleový	k2eAgFnSc1d1
kyselina	kyselina	k1gFnSc1
<g/>
,	,	kIx,
nositelka	nositelka	k1gFnSc1
genetické	genetický	k2eAgFnSc2d1
informace	informace	k1gFnSc2
všech	všechen	k3xTgInPc2
organismů	organismus	k1gInPc2
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
některých	některý	k3yIgMnPc2
nebuněčných	buněčný	k2eNgMnPc2d1
<g/>
,	,	kIx,
u	u	k7c2
nichž	jenž	k3xRgInPc2
hraje	hrát	k5eAaImIp3nS
tuto	tento	k3xDgFnSc4
úlohu	úloha	k1gFnSc4
RNA	RNA	kA
(	(	kIx(
<g/>
např.	např.	kA
RNA	RNA	kA
viry	vir	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>