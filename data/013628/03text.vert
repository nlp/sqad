<s>
DNA	DNA	kA
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
nositelce	nositelka	k1gFnSc6
genetické	genetický	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
metabolickém	metabolický	k2eAgNnSc6d1
onemocnění	onemocnění	k1gNnSc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
dna	dno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Struktura	struktura	k1gFnSc1
dvoušroubovice	dvoušroubovice	k1gFnSc2
DNA	DNA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
formě	forma	k1gFnSc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
většina	většina	k1gFnSc1
DNA	dno	k1gNnSc2
například	například	k6eAd1
v	v	k7c6
lidských	lidský	k2eAgFnPc6d1
buňkách	buňka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvoušroubovice	Dvoušroubovice	k1gFnSc1
je	být	k5eAaImIp3nS
tvořena	tvořit	k5eAaImNgFnS
dvěma	dva	k4xCgInPc7
řetězci	řetězec	k1gInPc7
nukleotidů	nukleotid	k1gInPc2
</s>
<s>
Deoxyribonukleová	deoxyribonukleový	k2eAgFnSc1d1
kyselina	kyselina	k1gFnSc1
<g/>
,	,	kIx,
běžně	běžně	k6eAd1
označovaná	označovaný	k2eAgFnSc1d1
DNA	dna	kA
(	(	kIx(
<g/>
z	z	k7c2
anglického	anglický	k2eAgInSc2d1
deoxyribonucleic	deoxyribonucleic	k1gInPc2
acid	 acid		k1gNnPc2
<g/>
,	,	kIx,
česky	česky	k6eAd1
zřídka	zřídka	k6eAd1
i	i	k9
DNK	DNK	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nukleová	nukleový	k2eAgFnSc1d1
kyselina	kyselina	k1gFnSc1
<g/>
,	,	kIx,
nositelka	nositelka	k1gFnSc1
genetické	genetický	k2eAgFnSc2d1
informace	informace	k1gFnSc2
všech	všechen	k3xTgInPc2
organismů	organismus	k1gInPc2
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
některých	některý	k3yIgMnPc2
nebuněčných	buněčný	k2eNgMnPc2d1
<g/>
,	,	kIx,
u	u	k7c2
nichž	jenž	k3xRgInPc2
hraje	hrát	k5eAaImIp3nS
tuto	tento	k3xDgFnSc4
úlohu	úloha	k1gFnSc4
RNA	RNA	kA
(	(	kIx(
<g/>
např.	např.	kA
RNA	RNA	kA
viry	vir	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
DNA	dna	k1gFnSc1
je	být	k5eAaImIp3nS
tedy	tedy	k9
pro	pro	k7c4
život	život	k1gInSc4
velmi	velmi	k6eAd1
důležitou	důležitý	k2eAgFnSc7d1
látkou	látka	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
struktuře	struktura	k1gFnSc6
kóduje	kódovat	k5eAaBmIp3nS
a	a	k8xC
buňkám	buňka	k1gFnPc3
zadává	zadávat	k5eAaImIp3nS
jejich	jejich	k3xOp3gInSc4
program	program	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
tím	ten	k3xDgNnSc7
předurčuje	předurčovat	k5eAaImIp3nS
vývoj	vývoj	k1gInSc4
a	a	k8xC
vlastnosti	vlastnost	k1gFnPc4
celého	celý	k2eAgInSc2d1
organismu	organismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
eukaryotických	eukaryotický	k2eAgInPc2d1
organismů	organismus	k1gInPc2
(	(	kIx(
<g/>
jako	jako	k8xC,k8xS
např.	např.	kA
rostliny	rostlina	k1gFnSc2
a	a	k8xC
živočichové	živočich	k1gMnPc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
DNA	dna	k1gFnSc1
hlavní	hlavní	k2eAgFnSc1d1
složkou	složka	k1gFnSc7
chromatinu	chromatin	k1gInSc2
<g/>
,	,	kIx,
směsi	směs	k1gFnSc2
nukleových	nukleový	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
a	a	k8xC
proteinů	protein	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
je	být	k5eAaImIp3nS
uložena	uložit	k5eAaPmNgFnS
zejména	zejména	k9
uvnitř	uvnitř	k7c2
buněčného	buněčný	k2eAgNnSc2d1
jádra	jádro	k1gNnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
u	u	k7c2
prokaryot	prokaryota	k1gFnPc2
(	(	kIx(
<g/>
např.	např.	kA
bakterie	bakterie	k1gFnSc2
a	a	k8xC
archea	archeum	k1gNnSc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
DNA	dna	k1gFnSc1
nachází	nacházet	k5eAaImIp3nS
volně	volně	k6eAd1
v	v	k7c6
cytoplazmě	cytoplazma	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
DNA	DNA	kA
je	být	k5eAaImIp3nS
biologická	biologický	k2eAgFnSc1d1
makromolekula	makromolekula	k1gFnSc1
–	–	k?
polymer	polymer	k1gInSc1
v	v	k7c6
podobě	podoba	k1gFnSc6
řetězce	řetězec	k1gInSc2
nukleotidů	nukleotid	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nukleotidy	nukleotid	k1gInPc7
jsou	být	k5eAaImIp3nP
vždy	vždy	k6eAd1
složeny	složit	k5eAaPmNgInP
z	z	k7c2
cukru	cukr	k1gInSc2
deoxyribózy	deoxyribóza	k1gFnSc2
<g/>
,	,	kIx,
fosfátové	fosfátový	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
a	a	k8xC
jedné	jeden	k4xCgFnSc2
ze	z	k7c2
čtyř	čtyři	k4xCgFnPc2
nukleových	nukleový	k2eAgFnPc2d1
bází	báze	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Informační	informační	k2eAgFnSc6d1
funkci	funkce	k1gFnSc6
mají	mít	k5eAaImIp3nP
právě	právě	k9
báze	báze	k1gFnSc2
<g/>
,	,	kIx,
jimiž	jenž	k3xRgInPc7
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
adenin	adenin	k1gInSc4
A	A	kA
<g/>
,	,	kIx,
guanin	guanin	k1gInSc1
G	G	kA
<g/>
,	,	kIx,
cytosin	cytosin	k1gInSc1
C	C	kA
nebo	nebo	k8xC
thymin	thymin	k1gInSc1
T.	T.	kA
První	první	k4xOgFnPc1
dvě	dva	k4xCgFnPc1
patří	patřit	k5eAaImIp3nP
mezi	mezi	k7c4
puriny	purin	k1gInPc4
<g/>
,	,	kIx,
zbylé	zbylý	k2eAgInPc4d1
mezi	mezi	k7c7
tzv.	tzv.	kA
pyrimidiny	pyrimidin	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvě	dva	k4xCgFnPc1
vlákna	vlákno	k1gNnSc2
DNA	DNA	kA
se	se	k3xPyFc4
často	často	k6eAd1
spojují	spojovat	k5eAaImIp3nP
a	a	k8xC
vytvářejí	vytvářet	k5eAaImIp3nP
dvoušroubovici	dvoušroubovice	k1gFnSc4
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
tvar	tvar	k1gInSc4
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
slavný	slavný	k2eAgMnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
kulturní	kulturní	k2eAgFnSc7d1
ikonou	ikona	k1gFnSc7
moderní	moderní	k2eAgFnSc2d1
doby	doba	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Dvoušroubovici	Dvoušroubovice	k1gFnSc3
DNA	dno	k1gNnSc2
tvoří	tvořit	k5eAaImIp3nS
dvě	dva	k4xCgFnPc4
navzájem	navzájem	k6eAd1
spletené	spletený	k2eAgFnSc2d1
šroubovice	šroubovice	k1gFnSc2
<g/>
,	,	kIx,
každá	každý	k3xTgFnSc1
mířící	mířící	k2eAgInSc4d1
opačným	opačný	k2eAgInSc7d1
směrem	směr	k1gInSc7
(	(	kIx(
<g/>
jsou	být	k5eAaImIp3nP
antiparalelní	antiparalelní	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
protilehlými	protilehlý	k2eAgFnPc7d1
bázemi	báze	k1gFnPc7
obou	dva	k4xCgInPc2
vláken	vlákno	k1gNnPc2
se	se	k3xPyFc4
vytvářejí	vytvářet	k5eAaImIp3nP
vodíkové	vodíkový	k2eAgInPc1d1
můstky	můstek	k1gInPc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
tři	tři	k4xCgFnPc4
mezi	mezi	k7c7
guaninem	guanin	k1gInSc7
a	a	k8xC
cytosinem	cytosin	k1gInSc7
nebo	nebo	k8xC
dva	dva	k4xCgInPc4
mezi	mezi	k7c7
adeninem	adenin	k1gInSc7
a	a	k8xC
thyminem	thymin	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
i	i	k9
jiné	jiný	k2eAgInPc4d1
způsoby	způsob	k1gInPc4
uspořádání	uspořádání	k1gNnSc2
řetězců	řetězec	k1gInPc2
<g/>
,	,	kIx,
vymykající	vymykající	k2eAgFnSc6d1
se	se	k3xPyFc4
tradiční	tradiční	k2eAgFnSc3d1
představě	představa	k1gFnSc3
dvoušroubovice	dvoušroubovice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Deoxyribonukleová	deoxyribonukleový	k2eAgFnSc1d1
kyselina	kyselina	k1gFnSc1
je	být	k5eAaImIp3nS
středem	střed	k1gInSc7
zájmu	zájem	k1gInSc2
vědců	vědec	k1gMnPc2
nejen	nejen	k6eAd1
z	z	k7c2
biologických	biologický	k2eAgInPc2d1
oborů	obor	k1gInPc2
a	a	k8xC
byly	být	k5eAaImAgFnP
vyvinuty	vyvinout	k5eAaPmNgFnP
promyšlené	promyšlený	k2eAgFnPc1d1
techniky	technika	k1gFnPc1
její	její	k3xOp3gFnSc2
izolace	izolace	k1gFnSc2
<g/>
,	,	kIx,
separace	separace	k1gFnSc2
<g/>
,	,	kIx,
barvení	barvení	k1gNnSc2
<g/>
,	,	kIx,
sekvenování	sekvenování	k1gNnSc2
<g/>
,	,	kIx,
umělé	umělý	k2eAgFnSc2d1
syntézy	syntéza	k1gFnSc2
a	a	k8xC
manipulace	manipulace	k1gFnSc2
s	s	k7c7
ní	on	k3xPp3gFnSc7
pomocí	pomoc	k1gFnSc7
metod	metoda	k1gFnPc2
genového	genový	k2eAgNnSc2d1
inženýrství	inženýrství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc4
tyto	tento	k3xDgInPc4
postupy	postup	k1gInPc4
jsou	být	k5eAaImIp3nP
důležité	důležitý	k2eAgInPc1d1
i	i	k9
pro	pro	k7c4
lékaře	lékař	k1gMnPc4
<g/>
,	,	kIx,
kriminalisty	kriminalista	k1gMnPc4
či	či	k8xC
evoluční	evoluční	k2eAgMnPc4d1
biology	biolog	k1gMnPc4
–	–	k?
DNA	dna	k1gFnSc1
je	být	k5eAaImIp3nS
zásadním	zásadní	k2eAgInSc7d1
materiálem	materiál	k1gInSc7
v	v	k7c6
diagnostice	diagnostika	k1gFnSc6
nemocí	nemoc	k1gFnPc2
<g/>
,	,	kIx,
testech	test	k1gInPc6
otcovství	otcovství	k1gNnSc4
<g/>
,	,	kIx,
při	při	k7c6
vyšetřování	vyšetřování	k1gNnSc6
zločinů	zločin	k1gInPc2
<g/>
,	,	kIx,
přípravě	příprava	k1gFnSc3
plodin	plodina	k1gFnPc2
s	s	k7c7
novými	nový	k2eAgFnPc7d1
vlastnostmi	vlastnost	k1gFnPc7
či	či	k8xC
třeba	třeba	k6eAd1
hledání	hledání	k1gNnSc1
příbuzenských	příbuzenský	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
mezi	mezi	k7c7
organismy	organismus	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
výzkumu	výzkum	k1gInSc2
</s>
<s>
James	James	k1gMnSc1
D.	D.	kA
Watson	Watson	k1gMnSc1
a	a	k8xC
Francis	Francis	k1gFnSc1
Crick	Cricka	k1gFnPc2
(	(	kIx(
<g/>
vpravo	vpravo	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
podávající	podávající	k2eAgFnSc1d1
si	se	k3xPyFc3
ruce	ruka	k1gFnSc6
s	s	k7c7
Maclynem	Maclyn	k1gInSc7
McCartym	McCartym	k1gInSc1
(	(	kIx(
<g/>
vlevo	vlevo	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
dějiny	dějiny	k1gFnPc1
objevu	objev	k1gInSc2
a	a	k8xC
výzkumu	výzkum	k1gInSc2
DNA	dno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Deoxyribonukleová	deoxyribonukleový	k2eAgFnSc1d1
kyselina	kyselina	k1gFnSc1
byla	být	k5eAaImAgFnS
popsána	popsán	k2eAgFnSc1d1
roku	rok	k1gInSc2
1869	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
švýcarský	švýcarský	k2eAgMnSc1d1
lékař	lékař	k1gMnSc1
Friedrich	Friedrich	k1gMnSc1
Miescher	Mieschra	k1gFnPc2
zkoumal	zkoumat	k5eAaImAgMnS
složení	složení	k1gNnSc3
hnisu	hnis	k1gInSc2
z	z	k7c2
nemocničních	nemocniční	k2eAgInPc2d1
obvazů	obvaz	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
jader	jádro	k1gNnPc2
bílých	bílý	k2eAgFnPc2d1
krvinek	krvinka	k1gFnPc2
přítomných	přítomný	k2eAgFnPc2d1
v	v	k7c6
tomto	tento	k3xDgInSc6
hnisu	hnis	k1gInSc6
získal	získat	k5eAaPmAgMnS
jisté	jistý	k2eAgNnSc4d1
množství	množství	k1gNnSc4
nukleových	nukleový	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
souhrnně	souhrnně	k6eAd1
nazýval	nazývat	k5eAaImAgMnS
nuklein	nuklein	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
počátku	počátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
Phoebus	Phoebus	k1gInSc1
Levene	Leven	k1gInSc5
rozpoznal	rozpoznat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
DNA	DNA	kA
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
cukrů	cukr	k1gInPc2
<g/>
,	,	kIx,
fosfátů	fosfát	k1gInPc2
a	a	k8xC
bází	báze	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
O	o	k7c6
funkci	funkce	k1gFnSc6
DNA	dno	k1gNnPc4
toho	ten	k3xDgNnSc2
dlouho	dlouho	k6eAd1
nebylo	být	k5eNaImAgNnS
moc	moc	k6eAd1
známo	znám	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc1
důkaz	důkaz	k1gInSc1
o	o	k7c4
roli	role	k1gFnSc4
DNA	DNA	kA
v	v	k7c6
přenosu	přenos	k1gInSc6
genetické	genetický	k2eAgFnPc4d1
informace	informace	k1gFnPc4
přinesl	přinést	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1944	#num#	k4
Averyho-MacLeodův-McCartyho	Averyho-MacLeodův-McCarty	k1gMnSc2
experiment	experiment	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
provedli	provést	k5eAaPmAgMnP
Oswald	Oswald	k1gInSc4
Avery	Avera	k1gFnSc2
společně	společně	k6eAd1
s	s	k7c7
Colinem	Colin	k1gMnSc7
MacLeodem	MacLeod	k1gMnSc7
a	a	k8xC
Maclynem	Maclyn	k1gInSc7
McCartym	McCartym	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sérií	série	k1gFnPc2
pokusů	pokus	k1gInPc2
s	s	k7c7
transformací	transformace	k1gFnSc7
pneumokoků	pneumokok	k1gInPc2
zjistili	zjistit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
DNA	DNA	kA
je	být	k5eAaImIp3nS
genetickým	genetický	k2eAgInSc7d1
materiálem	materiál	k1gInSc7
buněk	buňka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Další	další	k2eAgInSc1d1
důkaz	důkaz	k1gInSc1
přinesl	přinést	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1952	#num#	k4
Hersheyho	Hershey	k1gMnSc4
<g/>
–	–	k?
<g/>
Chaseové	Chaseové	k2eAgInSc1d1
experiment	experiment	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Patrně	patrně	k6eAd1
nejslavnějším	slavný	k2eAgInSc7d3
milníkem	milník	k1gInSc7
ve	v	k7c6
výzkumu	výzkum	k1gInSc6
DNA	dno	k1gNnSc2
bylo	být	k5eAaImAgNnS
odhalení	odhalení	k1gNnSc4
její	její	k3xOp3gFnSc2
trojrozměrné	trojrozměrný	k2eAgFnSc2d1
struktury	struktura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správný	správný	k2eAgInSc4d1
dvoušroubovicový	dvoušroubovicový	k2eAgInSc4d1
model	model	k1gInSc4
poprvé	poprvé	k6eAd1
představili	představit	k5eAaPmAgMnP
v	v	k7c6
roce	rok	k1gInSc6
1953	#num#	k4
v	v	k7c6
časopise	časopis	k1gInSc6
Nature	Natur	k1gMnSc5
James	James	k1gMnSc1
D.	D.	kA
Watson	Watson	k1gMnSc1
a	a	k8xC
Francis	Francis	k1gFnSc1
Crick	Cricko	k1gNnPc2
<g/>
,	,	kIx,
pozdější	pozdní	k2eAgMnPc1d2
laureáti	laureát	k1gMnPc1
Nobelovy	Nobelův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Vycházeli	vycházet	k5eAaImAgMnP
přitom	přitom	k6eAd1
z	z	k7c2
rentgenové	rentgenový	k2eAgFnSc2d1
difrakční	difrakční	k2eAgFnSc2d1
analýzy	analýza	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
o	o	k7c4
rok	rok	k1gInSc4
dříve	dříve	k6eAd2
provedli	provést	k5eAaPmAgMnP
Rosalind	Rosalinda	k1gFnPc2
Franklinová	Franklinový	k2eAgFnSc1d1
a	a	k8xC
Raymond	Raymond	k1gInSc1
Gosling	Gosling	k1gInSc1
a	a	k8xC
publikovali	publikovat	k5eAaBmAgMnP
ve	v	k7c6
stejném	stejný	k2eAgNnSc6d1
čísle	číslo	k1gNnSc6
Nature	Natur	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc1d1
článek	článek	k1gInSc1
v	v	k7c6
tomto	tento	k3xDgNnSc6
vydání	vydání	k1gNnSc6
předložil	předložit	k5eAaPmAgMnS
i	i	k8xC
Maurice	Maurika	k1gFnSc6
Wilkins	Wilkins	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1957	#num#	k4
předložil	předložit	k5eAaPmAgMnS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
již	již	k6eAd1
slavný	slavný	k2eAgInSc1d1
Crick	Crick	k1gInSc1
sérii	série	k1gFnSc4
pravidel	pravidlo	k1gNnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
označují	označovat	k5eAaImIp3nP
jako	jako	k9
centrální	centrální	k2eAgNnSc4d1
dogma	dogma	k1gNnSc4
molekulární	molekulární	k2eAgFnSc2d1
biologie	biologie	k1gFnSc2
a	a	k8xC
popisují	popisovat	k5eAaImIp3nP
vztahy	vztah	k1gInPc1
mezi	mezi	k7c7
DNA	DNA	kA
<g/>
,	,	kIx,
RNA	RNA	kA
a	a	k8xC
proteiny	protein	k1gInPc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
slavný	slavný	k2eAgMnSc1d1
Meselsonův	Meselsonův	k2eAgMnSc1d1
<g/>
–	–	k?
<g/>
Stahlův	Stahlův	k2eAgInSc1d1
experiment	experiment	k1gInSc1
umožnil	umožnit	k5eAaPmAgInS
poznat	poznat	k5eAaPmF
způsob	způsob	k1gInSc1
replikace	replikace	k1gFnSc2
DNA	DNA	kA
v	v	k7c6
buňkách	buňka	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Genetický	genetický	k2eAgInSc4d1
kód	kód	k1gInSc4
rozluštili	rozluštit	k5eAaPmAgMnP
na	na	k7c6
počátku	počátek	k1gInSc6
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
Har	Har	k1gMnSc1
Gobind	Gobind	k1gMnSc1
Khorana	Khorana	k1gFnSc1
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
W.	W.	kA
Holley	Holle	k1gMnPc7
a	a	k8xC
Marshall	Marshall	k1gMnSc1
Warren	Warrna	k1gFnPc2
Nirenberg	Nirenberg	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Původ	původ	k1gInSc1
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1
nukleových	nukleový	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
<g/>
,	,	kIx,
tedy	tedy	k9
DNA	DNA	kA
a	a	k8xC
RNA	RNA	kA
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
společnou	společný	k2eAgFnSc7d1
vlastností	vlastnost	k1gFnSc7
všech	všecek	k3xTgInPc2
známých	známý	k2eAgInPc2d1
pozemských	pozemský	k2eAgInPc2d1
organismů	organismus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veškerý	veškerý	k3xTgInSc4
život	život	k1gInSc1
je	být	k5eAaImIp3nS
založen	založit	k5eAaPmNgInS
na	na	k7c4
koexistenci	koexistence	k1gFnSc4
těchto	tento	k3xDgFnPc2
nukleových	nukleový	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
s	s	k7c7
bílkovinami	bílkovina	k1gFnPc7
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
není	být	k5eNaImIp3nS
zcela	zcela	k6eAd1
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
vztah	vztah	k1gInSc1
mezi	mezi	k7c7
DNA	DNA	kA
a	a	k8xC
bílkovinami	bílkovina	k1gFnPc7
vyvinul	vyvinout	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
některých	některý	k3yIgFnPc2
hypotéz	hypotéza	k1gFnPc2
nejprve	nejprve	k6eAd1
existovaly	existovat	k5eAaImAgFnP
bílkoviny	bílkovina	k1gFnPc1
a	a	k8xC
až	až	k9
následně	následně	k6eAd1
vznikly	vzniknout	k5eAaPmAgFnP
nukleové	nukleový	k2eAgFnPc1d1
kyseliny	kyselina	k1gFnPc1
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
nejvíce	hodně	k6eAd3,k6eAd1
příznivců	příznivec	k1gMnPc2
má	mít	k5eAaImIp3nS
zřejmě	zřejmě	k6eAd1
v	v	k7c6
současnosti	současnost	k1gFnSc6
představa	představa	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
prapůvodní	prapůvodní	k2eAgFnSc7d1
látkou	látka	k1gFnSc7
byla	být	k5eAaImAgFnS
nukleová	nukleový	k2eAgFnSc1d1
kyselina	kyselina	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
schopna	schopen	k2eAgFnSc1d1
biologické	biologický	k2eAgFnPc4d1
evoluce	evoluce	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
teorie	teorie	k1gFnSc2
RNA	RNA	kA
světa	svět	k1gInSc2
však	však	k9
hlavní	hlavní	k2eAgFnSc4d1
roli	role	k1gFnSc4
hrála	hrát	k5eAaImAgFnS
nejprve	nejprve	k6eAd1
spíše	spíše	k9
RNA	RNA	kA
a	a	k8xC
teprve	teprve	k6eAd1
posléze	posléze	k6eAd1
přejala	přejmout	k5eAaPmAgFnS
hlavní	hlavní	k2eAgFnSc4d1
roli	role	k1gFnSc4
DNA	DNA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Doklady	doklad	k1gInPc1
ve	v	k7c4
prospěch	prospěch	k1gInSc4
takových	takový	k3xDgFnPc2
hypotéz	hypotéza	k1gFnPc2
jsou	být	k5eAaImIp3nP
však	však	k9
vždy	vždy	k6eAd1
nepřímé	přímý	k2eNgNnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
nejsou	být	k5eNaImIp3nP
k	k	k7c3
dispozici	dispozice	k1gFnSc3
dostatečně	dostatečně	k6eAd1
staré	starý	k2eAgMnPc4d1
vzorky	vzorek	k1gInPc4
DNA	DNA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Život	život	k1gInSc1
vznikl	vzniknout	k5eAaPmAgInS
již	již	k6eAd1
před	před	k7c7
několika	několik	k4yIc7
miliardami	miliarda	k4xCgFnPc7
let	léto	k1gNnPc2
<g/>
,	,	kIx,
jenže	jenže	k8xC
už	už	k6eAd1
po	po	k7c6
několika	několik	k4yIc6
desítkách	desítka	k1gFnPc6
tisíců	tisíc	k4xCgInPc2
let	léto	k1gNnPc2
klesá	klesat	k5eAaImIp3nS
množství	množství	k1gNnSc1
DNA	dno	k1gNnSc2
na	na	k7c4
setinu	setina	k1gFnSc4
původního	původní	k2eAgInSc2d1
stavu	stav	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studie	studie	k1gFnSc1
v	v	k7c6
časopise	časopis	k1gInSc6
Nature	Natur	k1gMnSc5
z	z	k7c2
let	léto	k1gNnPc2
2000	#num#	k4
a	a	k8xC
2002	#num#	k4
nicméně	nicméně	k8xC
popisují	popisovat	k5eAaImIp3nP
nález	nález	k1gInSc4
až	až	k9
450	#num#	k4
milionů	milion	k4xCgInPc2
let	léto	k1gNnPc2
starých	starý	k2eAgInPc2d1
vzorků	vzorek	k1gInPc2
bakteriální	bakteriální	k2eAgFnSc1d1
DNA	dna	k1gFnSc1
uchovaných	uchovaný	k2eAgMnPc2d1
v	v	k7c6
solných	solný	k2eAgInPc6d1
krystalech	krystal	k1gInPc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
dále	daleko	k6eAd2
existuje	existovat	k5eAaImIp3nS
i	i	k9
řada	řada	k1gFnSc1
dalších	další	k2eAgMnPc2d1
<g/>
,	,	kIx,
více	hodně	k6eAd2
nebo	nebo	k8xC
méně	málo	k6eAd2
spolehlivých	spolehlivý	k2eAgFnPc2d1
studií	studie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Stavba	stavba	k1gFnSc1
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
krátkého	krátký	k2eAgInSc2d1
úseku	úsek	k1gInSc2
DNA	DNA	kA
<g/>
:	:	kIx,
v	v	k7c6
každém	každý	k3xTgInSc6
ze	z	k7c2
čtyř	čtyři	k4xCgInPc2
nukleotidů	nukleotid	k1gInPc2
je	být	k5eAaImIp3nS
deoxyribóza	deoxyribóza	k1gFnSc1
<g/>
,	,	kIx,
fosfátová	fosfátový	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
a	a	k8xC
dále	daleko	k6eAd2
jedna	jeden	k4xCgFnSc1
náhodná	náhodný	k2eAgFnSc1d1
nukleová	nukleový	k2eAgFnSc1d1
báze	báze	k1gFnSc1
(	(	kIx(
<g/>
ze	z	k7c2
čtyř	čtyři	k4xCgInPc2
možných	možný	k2eAgInPc2d1
<g/>
)	)	kIx)
</s>
<s>
Stavbu	stavba	k1gFnSc4
DNA	dno	k1gNnSc2
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
zkoumat	zkoumat	k5eAaImF
na	na	k7c6
několika	několik	k4yIc6
úrovních	úroveň	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pořadí	pořadí	k1gNnSc1
nukleotidů	nukleotid	k1gInPc2
v	v	k7c6
lineárním	lineární	k2eAgNnSc6d1
dvouvlákně	dvouvlákno	k1gNnSc6
je	být	k5eAaImIp3nS
záležitostí	záležitost	k1gFnSc7
tzv.	tzv.	kA
primární	primární	k2eAgFnSc2d1
struktury	struktura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stáčení	stáčení	k1gNnSc3
vlákna	vlákno	k1gNnSc2
do	do	k7c2
dvoušroubovice	dvoušroubovice	k1gFnSc2
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
jako	jako	k9
sekundární	sekundární	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
DNA	DNA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konečně	konečně	k6eAd1
pod	pod	k7c7
tzv.	tzv.	kA
terciární	terciární	k2eAgFnSc7d1
strukturou	struktura	k1gFnSc7
se	se	k3xPyFc4
rozumí	rozumět	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
nadšroubovicové	nadšroubovicový	k2eAgNnSc1d1
vinutí	vinutí	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
usnadňuje	usnadňovat	k5eAaImIp3nS
kondenzaci	kondenzace	k1gFnSc4
DNA	dno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
nukleová	nukleový	k2eAgFnSc1d1
kyselina	kyselina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
DNA	DNA	kA
vlastně	vlastně	k9
není	být	k5eNaImIp3nS
nic	nic	k3yNnSc1
jiného	jiný	k2eAgNnSc2d1
než	než	k8xS
velmi	velmi	k6eAd1
dlouhý	dlouhý	k2eAgInSc1d1
lineární	lineární	k2eAgInSc1d1
řetězec	řetězec	k1gInSc1
nukleotidů	nukleotid	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
uvnitř	uvnitř	k7c2
každého	každý	k3xTgInSc2
virionu	virion	k1gInSc2
planých	planý	k2eAgFnPc2d1
neštovic	neštovice	k1gFnPc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
DNA	DNA	kA
o	o	k7c6
délce	délka	k1gFnSc6
193	#num#	k4
mikrometrů	mikrometr	k1gInPc2
<g/>
,	,	kIx,
kruhová	kruhový	k2eAgFnSc1d1
DNA	dna	k1gFnSc1
u	u	k7c2
Escherichia	Escherichium	k1gNnSc2
coli	coli	k6eAd1
má	mít	k5eAaImIp3nS
délku	délka	k1gFnSc4
1	#num#	k4
600	#num#	k4
µ	µ	k?
(	(	kIx(
<g/>
1,6	1,6	k4
mm	mm	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
lidský	lidský	k2eAgInSc1d1
genom	genom	k1gInSc1
je	být	k5eAaImIp3nS
rozložen	rozložit	k5eAaPmNgInS
do	do	k7c2
23	#num#	k4
lineárních	lineární	k2eAgFnPc2d1
molekul	molekula	k1gFnPc2
DNA	DNA	kA
(	(	kIx(
<g/>
v	v	k7c6
haploidním	haploidní	k2eAgInSc6d1
stavu	stav	k1gInSc6
<g/>
)	)	kIx)
o	o	k7c6
celkové	celkový	k2eAgFnSc6d1
délce	délka	k1gFnSc6
1	#num#	k4
metru	metr	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Nukleotid	nukleotid	k1gInSc1
je	být	k5eAaImIp3nS
základní	základní	k2eAgFnSc7d1
stavební	stavební	k2eAgFnSc7d1
jednotkou	jednotka	k1gFnSc7
všech	všecek	k3xTgFnPc2
molekul	molekula	k1gFnPc2
DNA	dno	k1gNnSc2
<g/>
;	;	kIx,
existují	existovat	k5eAaImIp3nP
přitom	přitom	k6eAd1
čtyři	čtyři	k4xCgInPc4
základní	základní	k2eAgInPc4d1
typy	typ	k1gInPc4
nukleotidů	nukleotid	k1gInPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
se	se	k3xPyFc4
v	v	k7c6
DNA	DNA	kA
přirozeně	přirozeně	k6eAd1
vyskytují	vyskytovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
čtyři	čtyři	k4xCgInPc4
nukleotidy	nukleotid	k1gInPc4
(	(	kIx(
<g/>
dATP	dATP	k?
<g/>
,	,	kIx,
dGTP	dGTP	k?
<g/>
,	,	kIx,
dCTP	dCTP	k?
<g/>
,	,	kIx,
dTTP	dTTP	k?
<g/>
)	)	kIx)
se	se	k3xPyFc4
navzájem	navzájem	k6eAd1
liší	lišit	k5eAaImIp3nS
typem	typ	k1gInSc7
přivěšené	přivěšený	k2eAgFnSc2d1
nukleové	nukleový	k2eAgFnSc2d1
báze	báze	k1gFnSc2
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc7
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
především	především	k9
adenin	adenin	k1gInSc1
<g/>
,	,	kIx,
guanin	guanin	k1gInSc1
<g/>
,	,	kIx,
cytosin	cytosin	k1gInSc1
či	či	k8xC
thymin	thymin	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
Důležité	důležitý	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
každý	každý	k3xTgInSc1
nukleotid	nukleotid	k1gInSc1
má	mít	k5eAaImIp3nS
tři	tři	k4xCgFnPc4
důležité	důležitý	k2eAgFnPc4d1
stavební	stavební	k2eAgFnPc4d1
součásti	součást	k1gFnPc4
<g/>
:	:	kIx,
</s>
<s>
deoxyribóza	deoxyribóza	k1gFnSc1
–	–	k?
pětiuhlíkový	pětiuhlíkový	k2eAgInSc4d1
cukr	cukr	k1gInSc4
(	(	kIx(
<g/>
pentóza	pentóza	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
v	v	k7c6
DNA	DNA	kA
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
cyklické	cyklický	k2eAgFnSc6d1
furanózové	furanózový	k2eAgFnSc6d1
formě	forma	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInPc1
uhlíky	uhlík	k1gInPc1
se	se	k3xPyFc4
po	po	k7c6
směru	směr	k1gInSc6
pohybu	pohyb	k1gInSc2
hodinových	hodinový	k2eAgFnPc2d1
ručiček	ručička	k1gFnPc2
označují	označovat	k5eAaImIp3nP
1	#num#	k4
<g/>
'	'	kIx"
<g/>
,	,	kIx,
2	#num#	k4
<g/>
'	'	kIx"
<g/>
,	,	kIx,
3	#num#	k4
<g/>
'	'	kIx"
<g/>
,	,	kIx,
4	#num#	k4
<g/>
'	'	kIx"
a	a	k8xC
5	#num#	k4
<g/>
'	'	kIx"
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
na	na	k7c4
1	#num#	k4
<g/>
'	'	kIx"
uhlíku	uhlík	k1gInSc2
je	být	k5eAaImIp3nS
navěšena	navěšen	k2eAgFnSc1d1
nukleová	nukleový	k2eAgFnSc1d1
báze	báze	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c4
3	#num#	k4
<g/>
'	'	kIx"
a	a	k8xC
5	#num#	k4
<g/>
'	'	kIx"
uhlíku	uhlík	k1gInSc2
jsou	být	k5eAaImIp3nP
přes	přes	k7c4
OH	OH	kA
skupinu	skupina	k1gFnSc4
připevněny	připevněn	k2eAgFnPc1d1
fosfátové	fosfátový	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
<g/>
;	;	kIx,
</s>
<s>
fosfát	fosfát	k1gInSc1
–	–	k?
vazebný	vazebný	k2eAgInSc1d1
zbytek	zbytek	k1gInSc1
kyseliny	kyselina	k1gFnSc2
ortofosforečné	ortofosforečný	k2eAgFnSc2d1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
navázán	navázat	k5eAaPmNgInS
na	na	k7c4
5	#num#	k4
<g/>
'	'	kIx"
uhlíku	uhlík	k1gInSc2
každého	každý	k3xTgInSc2
nukleotidu	nukleotid	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záporný	záporný	k2eAgInSc1d1
náboj	náboj	k1gInSc1
na	na	k7c6
fosforečnanu	fosforečnan	k1gInSc6
je	být	k5eAaImIp3nS
důvodem	důvod	k1gInSc7
celkového	celkový	k2eAgInSc2d1
negativního	negativní	k2eAgInSc2d1
náboje	náboj	k1gInSc2
DNA	DNA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fosfátová	fosfátový	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
je	být	k5eAaImIp3nS
můstek	můstek	k1gInSc1
propojující	propojující	k2eAgInSc1d1
5	#num#	k4
<g/>
'	'	kIx"
uhlík	uhlík	k1gInSc1
každé	každý	k3xTgFnSc2
deoxyribózy	deoxyribóza	k1gFnSc2
s	s	k7c7
3	#num#	k4
<g/>
'	'	kIx"
uhlíkem	uhlík	k1gInSc7
předchozí	předchozí	k2eAgFnSc2d1
deoxyribózy	deoxyribóza	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
je	být	k5eAaImIp3nS
tzv.	tzv.	kA
cukr-fosfátová	cukr-fosfátový	k2eAgFnSc1d1
kostra	kostra	k1gFnSc1
DNA	DNA	kA
<g/>
.	.	kIx.
</s>
<s>
nukleová	nukleový	k2eAgFnSc1d1
báze	báze	k1gFnSc1
–	–	k?
dusíkatá	dusíkatý	k2eAgFnSc1d1
heterocyklická	heterocyklický	k2eAgFnSc1d1
sloučenina	sloučenina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
DNA	DNA	kA
se	se	k3xPyFc4
v	v	k7c6
různých	různý	k2eAgFnPc6d1
kombinacích	kombinace	k1gFnPc6
vyskytují	vyskytovat	k5eAaImIp3nP
především	především	k9
čtyři	čtyři	k4xCgFnPc4
základní	základní	k2eAgFnPc4d1
nukleové	nukleový	k2eAgFnPc4d1
báze	báze	k1gFnPc4
<g/>
,	,	kIx,
dvě	dva	k4xCgFnPc4
purinové	purinový	k2eAgFnPc4d1
(	(	kIx(
<g/>
adenin	adenin	k1gInSc4
A	A	kA
a	a	k8xC
guanin	guanin	k1gInSc1
G	G	kA
<g/>
)	)	kIx)
a	a	k8xC
dvě	dva	k4xCgFnPc4
pyrimidinové	pyrimidinový	k2eAgFnPc4d1
(	(	kIx(
<g/>
thymin	thymin	k1gInSc4
T	T	kA
a	a	k8xC
cytosin	cytosin	k1gMnSc1
C	C	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
z	z	k7c2
těchto	tento	k3xDgFnPc2
čtyř	čtyři	k4xCgFnPc2
nukleových	nukleový	k2eAgFnPc2d1
bází	báze	k1gFnPc2
je	být	k5eAaImIp3nS
připojena	připojit	k5eAaPmNgFnS
na	na	k7c4
1	#num#	k4
<g/>
'	'	kIx"
uhlíku	uhlík	k1gInSc2
deoxyribózy	deoxyribóza	k1gFnSc2
pomocí	pomocí	k7c2
N-glykosidové	N-glykosidový	k2eAgFnSc2d1
vazby	vazba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právě	právě	k6eAd1
existence	existence	k1gFnSc2
čtyř	čtyři	k4xCgFnPc2
nukleových	nukleový	k2eAgFnPc2d1
bází	báze	k1gFnPc2
je	být	k5eAaImIp3nS
zásadní	zásadní	k2eAgInSc1d1
pro	pro	k7c4
informační	informační	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
DNA	DNA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důležitá	důležitý	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
také	také	k9
schopnost	schopnost	k1gFnSc4
nukleových	nukleový	k2eAgFnPc2d1
bází	báze	k1gFnPc2
vytvářet	vytvářet	k5eAaImF
celou	celý	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
vodíkových	vodíkový	k2eAgInPc2d1
můstků	můstek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Primární	primární	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
DNA	dno	k1gNnSc2
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
znázornit	znázornit	k5eAaPmF
jako	jako	k9
lineární	lineární	k2eAgFnSc1d1
řada	řada	k1gFnSc1
nukleotidů	nukleotid	k1gInPc2
nebo	nebo	k8xC
třeba	třeba	k6eAd1
jako	jako	k9
řada	řada	k1gFnSc1
písmen	písmeno	k1gNnPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
odpovídají	odpovídat	k5eAaImIp3nP
dusíkatým	dusíkatý	k2eAgFnPc3d1
bázím	báze	k1gFnPc3
v	v	k7c6
těchto	tento	k3xDgInPc6
nukleotidech	nukleotid	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
je	být	k5eAaImIp3nS
důležité	důležitý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
DNA	dna	k1gFnSc1
je	být	k5eAaImIp3nS
směrovaná	směrovaný	k2eAgFnSc1d1
(	(	kIx(
<g/>
direkcionalizovaná	direkcionalizovaný	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tzn.	tzn.	kA
dají	dát	k5eAaPmIp3nP
se	se	k3xPyFc4
jednoznačně	jednoznačně	k6eAd1
odlišit	odlišit	k5eAaPmF
oba	dva	k4xCgInPc4
konce	konec	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Směr	směr	k1gInSc1
vláken	vlákna	k1gFnPc2
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
právě	právě	k9
podle	podle	k7c2
orientace	orientace	k1gFnSc2
deoxyribózy	deoxyribóza	k1gFnSc2
v	v	k7c6
něm	on	k3xPp3gNnSc6
<g/>
,	,	kIx,
tedy	tedy	k8xC
<g/>
:	:	kIx,
směr	směr	k1gInSc1
3	#num#	k4
<g/>
'	'	kIx"
<g/>
→	→	k?
<g/>
5	#num#	k4
<g/>
'	'	kIx"
a	a	k8xC
opačný	opačný	k2eAgInSc1d1
směr	směr	k1gInSc1
5	#num#	k4
<g/>
'	'	kIx"
<g/>
→	→	k?
<g/>
3	#num#	k4
<g/>
'	'	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
konvence	konvence	k1gFnSc2
se	se	k3xPyFc4
pořadí	pořadí	k1gNnSc1
nukleotidů	nukleotid	k1gInPc2
zapisuje	zapisovat	k5eAaImIp3nS
směrem	směr	k1gInSc7
5	#num#	k4
<g/>
'	'	kIx"
<g/>
→	→	k?
<g/>
3	#num#	k4
<g/>
'	'	kIx"
(	(	kIx(
<g/>
např.	např.	kA
TACGGACGGG	TACGGACGGG	kA
AGAAGCGCGC	AGAAGCGCGC	kA
GGGCGGGCCG	GGGCGGGCCG	kA
je	být	k5eAaImIp3nS
prvních	první	k4xOgNnPc6
30	#num#	k4
z	z	k7c2
3	#num#	k4
675	#num#	k4
nukleotidů	nukleotid	k1gInPc2
tvořících	tvořící	k2eAgFnPc2d1
přepisovanou	přepisovaný	k2eAgFnSc4d1
část	část	k1gFnSc4
genu	gen	k1gInSc2
pro	pro	k7c4
lidský	lidský	k2eAgInSc4d1
alfa-tubulin	alfa-tubulin	k2eAgInSc4d1
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
zpráva	zpráva	k1gFnSc1
o	o	k7c6
existenci	existence	k1gFnSc6
bakterií	bakterium	k1gNnPc2
GFAJ-	GFAJ-	k1gFnSc1
<g/>
1	#num#	k4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
údajně	údajně	k6eAd1
ve	v	k7c4
své	svůj	k3xOyFgMnPc4
DNA	dna	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
místo	místo	k1gNnSc4
fosfátových	fosfátový	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
arseničnany	arseničnana	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Hypotéza	hypotéza	k1gFnSc1
byla	být	k5eAaImAgFnS
definitivně	definitivně	k6eAd1
vyvrácena	vyvrátit	k5eAaPmNgFnS
v	v	k7c6
r.	r.	kA
2012	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Uspořádání	uspořádání	k1gNnSc1
řetězců	řetězec	k1gInPc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
párování	párování	k1gNnSc2
bází	báze	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Deoxyribonukleová	deoxyribonukleový	k2eAgFnSc1d1
kyselina	kyselina	k1gFnSc1
(	(	kIx(
<g/>
DNA	DNA	kA
<g/>
)	)	kIx)
může	moct	k5eAaImIp3nS
existovat	existovat	k5eAaImF
jako	jako	k9
samostatná	samostatný	k2eAgFnSc1d1
jednovláknová	jednovláknový	k2eAgFnSc1d1
molekula	molekula	k1gFnSc1
(	(	kIx(
<g/>
tzv.	tzv.	kA
ssDNA	ssDNA	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
velmi	velmi	k6eAd1
často	často	k6eAd1
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
vícevláknové	vícevláknový	k2eAgFnPc4d1
struktury	struktura	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
složené	složený	k2eAgFnSc2d1
z	z	k7c2
několika	několik	k4yIc2
řetězců	řetězec	k1gInPc2
spojených	spojený	k2eAgInPc2d1
vodíkovými	vodíkový	k2eAgInPc7d1
můstky	můstek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vodíkové	vodíkový	k2eAgInPc1d1
můstky	můstek	k1gInPc1
jsou	být	k5eAaImIp3nP
jedním	jeden	k4xCgInSc7
z	z	k7c2
typů	typ	k1gInPc2
poměrně	poměrně	k6eAd1
slabých	slabý	k2eAgFnPc2d1
vazebných	vazebný	k2eAgFnPc2d1
interakcí	interakce	k1gFnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
dvěma	dva	k4xCgInPc7
či	či	k8xC
více	hodně	k6eAd2
vlákny	vlákna	k1gFnPc4
DNA	DNA	kA
jich	on	k3xPp3gMnPc2
však	však	k9
může	moct	k5eAaImIp3nS
vzniknout	vzniknout	k5eAaPmF
obrovské	obrovský	k2eAgNnSc4d1
množství	množství	k1gNnSc4
<g/>
;	;	kIx,
výsledná	výsledný	k2eAgFnSc1d1
vícevláknová	vícevláknový	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
tak	tak	k9
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
stabilní	stabilní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typickou	typický	k2eAgFnSc7d1
formou	forma	k1gFnSc7
takového	takový	k3xDgNnSc2
vícevláknového	vícevláknový	k2eAgNnSc2d1
uspořádání	uspořádání	k1gNnSc2
DNA	dno	k1gNnSc2
je	být	k5eAaImIp3nS
dvoušroubovice	dvoušroubovice	k1gFnSc1
<g/>
,	,	kIx,
notoricky	notoricky	k6eAd1
známá	známý	k2eAgFnSc1d1
molekula	molekula	k1gFnSc1
DNA	DNA	kA
(	(	kIx(
<g/>
připomínající	připomínající	k2eAgInSc1d1
„	„	k?
<g/>
stočený	stočený	k2eAgInSc1d1
žebřík	žebřík	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
tvořená	tvořený	k2eAgFnSc1d1
dvěma	dva	k4xCgInPc7
lineárními	lineární	k2eAgInPc7d1
řetězci	řetězec	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
vznikla	vzniknout	k5eAaPmAgFnS
pravidelná	pravidelný	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
s	s	k7c7
velkým	velký	k2eAgNnSc7d1
množstvím	množství	k1gNnSc7
vodíkových	vodíkový	k2eAgInPc2d1
můstků	můstek	k1gInPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
žádoucí	žádoucí	k2eAgMnSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
vedle	vedle	k6eAd1
sebe	sebe	k3xPyFc4
„	„	k?
<g/>
v	v	k7c6
příčli	příčel	k1gInSc6
žebříku	žebřík	k1gInSc2
<g/>
“	“	k?
vyskytovaly	vyskytovat	k5eAaImAgInP
vždy	vždy	k6eAd1
určité	určitý	k2eAgFnSc2d1
nukleové	nukleový	k2eAgFnSc2d1
báze	báze	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
spolu	spolu	k6eAd1
ve	v	k7c6
správném	správný	k2eAgNnSc6d1
prostorovém	prostorový	k2eAgNnSc6d1
uspořádání	uspořádání	k1gNnSc6
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
několik	několik	k4yIc4
vodíkových	vodíkový	k2eAgInPc2d1
můstků	můstek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
typickém	typický	k2eAgInSc6d1
případě	případ	k1gInSc6
(	(	kIx(
<g/>
ne	ne	k9
však	však	k9
vždy	vždy	k6eAd1
<g/>
)	)	kIx)
se	se	k3xPyFc4
nukleové	nukleový	k2eAgFnSc2d1
báze	báze	k1gFnSc2
spojují	spojovat	k5eAaImIp3nP
navzájem	navzájem	k6eAd1
s	s	k7c7
odpovídající	odpovídající	k2eAgFnSc7d1
bází	báze	k1gFnSc7
podle	podle	k7c2
jednoduchého	jednoduchý	k2eAgInSc2d1
klíče	klíč	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
A	a	k9
se	se	k3xPyFc4
páruje	párovat	k5eAaImIp3nS
s	s	k7c7
T	T	kA
(	(	kIx(
<g/>
vzájemně	vzájemně	k6eAd1
jsou	být	k5eAaImIp3nP
spojeny	spojit	k5eAaPmNgFnP
dvěma	dva	k4xCgFnPc7
vodíkovými	vodíkový	k2eAgFnPc7d1
vazbami	vazba	k1gFnPc7
<g/>
)	)	kIx)
</s>
<s>
G	G	kA
se	se	k3xPyFc4
páruje	párovat	k5eAaImIp3nS
s	s	k7c7
C	C	kA
(	(	kIx(
<g/>
vzájemně	vzájemně	k6eAd1
jsou	být	k5eAaImIp3nP
spojeny	spojit	k5eAaPmNgFnP
třemi	tři	k4xCgFnPc7
vodíkovými	vodíkový	k2eAgFnPc7d1
vazbami	vazba	k1gFnPc7
<g/>
)	)	kIx)
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
tzv.	tzv.	kA
komplementaritu	komplementarita	k1gFnSc4
bází	báze	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
ní	on	k3xPp3gFnSc2
vychází	vycházet	k5eAaImIp3nS
vzájemná	vzájemný	k2eAgFnSc1d1
komplementarita	komplementarita	k1gFnSc1
obou	dva	k4xCgFnPc2
vláken	vlákna	k1gFnPc2
DNA	dno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vždy	vždy	k6eAd1
je	být	k5eAaImIp3nS
na	na	k7c6
určité	určitý	k2eAgFnSc6d1
pozici	pozice	k1gFnSc6
v	v	k7c6
molekule	molekula	k1gFnSc6
jeden	jeden	k4xCgInSc4
nukleotid	nukleotid	k1gInSc4
z	z	k7c2
dvojice	dvojice	k1gFnSc2
a	a	k8xC
v	v	k7c6
protějším	protější	k2eAgNnSc6d1
vlákně	vlákno	k1gNnSc6
druhý	druhý	k4xOgMnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takto	takto	k6eAd1
se	se	k3xPyFc4
uchovává	uchovávat	k5eAaImIp3nS
v	v	k7c6
každém	každý	k3xTgInSc6
z	z	k7c2
vláken	vlákno	k1gNnPc2
tatáž	týž	k3xTgFnSc1
informace	informace	k1gFnSc1
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
jedno	jeden	k4xCgNnSc1
z	z	k7c2
vláken	vlákno	k1gNnPc2
je	být	k5eAaImIp3nS
„	„	k?
<g/>
negativem	negativ	k1gInSc7
<g/>
“	“	k?
vlákna	vlákna	k1gFnSc1
druhého	druhý	k4xOgInSc2
–	–	k?
podle	podle	k7c2
jednoho	jeden	k4xCgNnSc2
vlákna	vlákno	k1gNnSc2
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
přiřazením	přiřazení	k1gNnSc7
komplementárních	komplementární	k2eAgFnPc2d1
bází	báze	k1gFnPc2
vytvořit	vytvořit	k5eAaPmF
vlákno	vlákno	k1gNnSc4
druhé	druhý	k4xOgFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poměr	poměr	k1gInSc1
AT	AT	kA
a	a	k8xC
GC	GC	kA
párů	pár	k1gInPc2
v	v	k7c6
molekule	molekula	k1gFnSc6
DNA	dno	k1gNnSc2
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
různý	různý	k2eAgMnSc1d1
<g/>
:	:	kIx,
tzv.	tzv.	kA
obsah	obsah	k1gInSc1
GC	GC	kA
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
u	u	k7c2
bakterií	bakterie	k1gFnPc2
od	od	k7c2
25	#num#	k4
do	do	k7c2
75	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
u	u	k7c2
savců	savec	k1gMnPc2
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
39	#num#	k4
<g/>
–	–	k?
<g/>
46	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Existuje	existovat	k5eAaImIp3nS
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
dalších	další	k2eAgFnPc2d1
možností	možnost	k1gFnPc2
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
pomocí	pomocí	k7c2
vodíkových	vodíkový	k2eAgInPc2d1
můstků	můstek	k1gInPc2
spárovat	spárovat	k5eAaImF,k5eAaPmF
báze	báze	k1gFnPc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
atomů	atom	k1gInPc2
schopných	schopný	k2eAgMnPc2d1
podílet	podílet	k5eAaImF
se	se	k3xPyFc4
na	na	k7c6
vzniku	vznik	k1gInSc6
vodíkových	vodíkový	k2eAgFnPc2d1
vazeb	vazba	k1gFnPc2
je	být	k5eAaImIp3nS
na	na	k7c6
molekulách	molekula	k1gFnPc6
purinů	purin	k1gInPc2
i	i	k9
pyrimidinů	pyrimidin	k1gInPc2
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samostatnou	samostatný	k2eAgFnSc7d1
kapitolou	kapitola	k1gFnSc7
je	být	k5eAaImIp3nS
tzv.	tzv.	kA
hoogsteenovské	hoogsteenovský	k2eAgNnSc1d1
párování	párování	k1gNnSc1
pojmenované	pojmenovaný	k2eAgNnSc1d1
podle	podle	k7c2
Karsta	Karst	k1gMnSc2
Hoogsteena	Hoogsteen	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
jako	jako	k9
první	první	k4xOgMnSc1
popsal	popsat	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Jinou	jiný	k2eAgFnSc7d1
možností	možnost	k1gFnSc7
je	být	k5eAaImIp3nS
tzv.	tzv.	kA
wobble	wobble	k6eAd1
párování	párování	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
umožňuje	umožňovat	k5eAaImIp3nS
úsporné	úsporný	k2eAgNnSc1d1
rozeznávání	rozeznávání	k1gNnSc1
kodonů	kodon	k1gMnPc2
pomocí	pomocí	k7c2
tRNA	trnout	k5eAaImSgInS
molekul	molekula	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
wobble	wobble	k6eAd1
párování	párování	k1gNnPc2
může	moct	k5eAaImIp3nS
například	například	k6eAd1
guanin	guanin	k1gInSc4
vytvářet	vytvářet	k5eAaImF
vazbu	vazba	k1gFnSc4
s	s	k7c7
uracilem	uracil	k1gInSc7
<g/>
;	;	kIx,
někdy	někdy	k6eAd1
je	být	k5eAaImIp3nS
rekrutován	rekrutován	k2eAgMnSc1d1
inosin	inosin	k1gMnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
má	mít	k5eAaImIp3nS
velmi	velmi	k6eAd1
obecné	obecný	k2eAgFnSc2d1
vazebné	vazebný	k2eAgFnSc2d1
schopnosti	schopnost	k1gFnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
schopen	schopen	k2eAgMnSc1d1
vázat	vázat	k5eAaImF
se	se	k3xPyFc4
na	na	k7c4
C	C	kA
<g/>
,	,	kIx,
A	A	kA
a	a	k8xC
U.	U.	kA
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kromě	kromě	k7c2
vodíkových	vodíkový	k2eAgInPc2d1
můstků	můstek	k1gInPc2
se	se	k3xPyFc4
uplatňuje	uplatňovat	k5eAaImIp3nS
hydrofobie	hydrofobie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dvoušroubovice	Dvoušroubovice	k1gFnSc1
</s>
<s>
Animace	animace	k1gFnSc1
otáčejícího	otáčející	k2eAgMnSc2d1
se	se	k3xPyFc4
prostorového	prostorový	k2eAgInSc2d1
modelu	model	k1gInSc2
dvoušroubovice	dvoušroubovice	k1gFnSc1
B-DNA	B-DNA	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
dvoušroubovice	dvoušroubovice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
drtivém	drtivý	k2eAgNnSc6d1
procentu	procento	k1gNnSc6
případů	případ	k1gInPc2
se	s	k7c7
DNA	DNA	kA
za	za	k7c2
běžných	běžný	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
uchovává	uchovávat	k5eAaImIp3nS
ve	v	k7c6
formě	forma	k1gFnSc6
pravotočivé	pravotočivý	k2eAgFnSc2d1
dvoušroubovice	dvoušroubovice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvoušroubovice	Dvoušroubovice	k1gFnSc1
DNA	dno	k1gNnSc2
je	být	k5eAaImIp3nS
tvořena	tvořit	k5eAaImNgFnS
dvěma	dva	k4xCgInPc7
vlákny	vlákno	k1gNnPc7
DNA	DNA	kA
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
se	se	k3xPyFc4
obtáčí	obtáčet	k5eAaImIp3nP
kolem	kolem	k7c2
společné	společný	k2eAgFnSc2d1
osy	osa	k1gFnSc2
a	a	k8xC
interagují	interagovat	k5eAaBmIp3nP,k5eAaPmIp3nP,k5eAaImIp3nP
spolu	spolu	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlákna	vlákna	k1gFnSc1
jsou	být	k5eAaImIp3nP
tzv.	tzv.	kA
antiparalelní	antiparalelní	k2eAgMnSc1d1
<g/>
,	,	kIx,
tzn.	tzn.	kA
směřují	směřovat	k5eAaImIp3nP
opačnými	opačný	k2eAgInPc7d1
směry	směr	k1gInPc7
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
–	–	k?
zatímco	zatímco	k8xS
jedno	jeden	k4xCgNnSc1
vlákno	vlákno	k1gNnSc1
můžeme	moct	k5eAaImIp1nP
jedním	jeden	k4xCgInSc7
směrem	směr	k1gInSc7
popsat	popsat	k5eAaPmF
jako	jako	k9
5	#num#	k4
<g/>
'	'	kIx"
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
'	'	kIx"
<g/>
,	,	kIx,
druhé	druhý	k4xOgNnSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
směru	směr	k1gInSc6
3	#num#	k4
<g/>
'	'	kIx"
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
'	'	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čísla	číslo	k1gNnSc2
3	#num#	k4
<g/>
'	'	kIx"
a	a	k8xC
5	#num#	k4
<g/>
'	'	kIx"
označují	označovat	k5eAaImIp3nP
čísla	číslo	k1gNnPc1
uhlíku	uhlík	k1gInSc2
na	na	k7c6
deoxyribóze	deoxyribóza	k1gFnSc6
<g/>
,	,	kIx,
na	na	k7c6
které	který	k3yIgFnSc6,k3yQgFnSc6,k3yRgFnSc6
se	se	k3xPyFc4
upínají	upínat	k5eAaImIp3nP
fosfátové	fosfátový	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
v	v	k7c6
cukr-fosfátové	cukr-fosfátový	k2eAgFnSc6d1
kostře	kostra	k1gFnSc6
DNA	DNA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
bázemi	báze	k1gFnPc7
v	v	k7c6
rámci	rámec	k1gInSc6
jednoho	jeden	k4xCgInSc2
„	„	k?
<g/>
patra	patro	k1gNnSc2
<g/>
“	“	k?
dvoušroubovice	dvoušroubovice	k1gFnSc1
platí	platit	k5eAaImIp3nP
pravidla	pravidlo	k1gNnPc1
Watson-Crickovské	Watson-Crickovský	k2eAgFnSc2d1
komplementarity	komplementarita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS
několik	několik	k4yIc4
tzv.	tzv.	kA
helikálních	helikální	k2eAgFnPc2d1
forem	forma	k1gFnPc2
(	(	kIx(
<g/>
konformací	konformace	k1gFnPc2
<g/>
)	)	kIx)
DNA	DNA	kA
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nP
celou	celý	k2eAgFnSc7d1
řadou	řada	k1gFnSc7
parametrů	parametr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typická	typický	k2eAgFnSc1d1
Watson-Crickovská	Watson-Crickovský	k2eAgFnSc1d1
pravotočivá	pravotočivý	k2eAgFnSc1d1
dvoušroubovice	dvoušroubovice	k1gFnSc1
(	(	kIx(
<g/>
tzv.	tzv.	kA
B-DNA	B-DNA	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nicméně	nicméně	k8xC
zcela	zcela	k6eAd1
převažující	převažující	k2eAgFnPc1d1
a	a	k8xC
ostatní	ostatní	k2eAgFnPc1d1
formy	forma	k1gFnPc1
(	(	kIx(
<g/>
zejména	zejména	k9
pravotočivá	pravotočivý	k2eAgFnSc1d1
A-DNA	A-DNA	k1gFnSc1
a	a	k8xC
levotočivá	levotočivý	k2eAgFnSc1d1
Z-DNA	Z-DNA	k1gFnSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
sice	sice	k8xC
mohou	moct	k5eAaImIp3nP
vyskytovat	vyskytovat	k5eAaImF
i	i	k9
v	v	k7c6
podmínkách	podmínka	k1gFnPc6
živé	živý	k2eAgFnSc2d1
buňky	buňka	k1gFnSc2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
spíše	spíše	k9
vzácně	vzácně	k6eAd1
a	a	k8xC
jen	jen	k9
za	za	k7c2
specifických	specifický	k2eAgFnPc2d1
okolností	okolnost	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jiná	jiný	k2eAgNnPc1d1
uspořádání	uspořádání	k1gNnPc1
řetězců	řetězec	k1gInPc2
</s>
<s>
G-kvartet	G-kvartet	k1gMnSc1
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
ze	z	k7c2
známých	známý	k2eAgFnPc2d1
alternativních	alternativní	k2eAgFnPc2d1
struktur	struktura	k1gFnPc2
DNA	DNA	kA
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
v	v	k7c6
buňkách	buňka	k1gFnPc6
</s>
<s>
V	v	k7c6
obecném	obecný	k2eAgNnSc6d1
povědomí	povědomí	k1gNnSc6
DNA	DNA	kA
tvoří	tvořit	k5eAaImIp3nS
dvoušroubovici	dvoušroubovice	k1gFnSc4
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
existují	existovat	k5eAaImIp3nP
i	i	k9
jiné	jiný	k2eAgInPc4d1
způsoby	způsob	k1gInPc4
uspořádání	uspořádání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
i	i	k9
v	v	k7c6
buňkách	buňka	k1gFnPc6
(	(	kIx(
<g/>
in	in	k?
vivo	vivo	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jiné	jiný	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
spíše	spíše	k9
laboratorní	laboratorní	k2eAgFnSc4d1
záležitost	záležitost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnohdy	mnohdy	k6eAd1
se	se	k3xPyFc4
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
neobvyklých	obvyklý	k2eNgNnPc2d1
párovacích	párovací	k2eAgNnPc2d1
míst	místo	k1gNnPc2
na	na	k7c6
molekulách	molekula	k1gFnPc6
bází	báze	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
případ	případ	k1gInSc1
tzv.	tzv.	kA
G-kvartetů	G-kvartet	k1gMnPc2
<g/>
,	,	kIx,
čtyřvláknových	čtyřvláknový	k2eAgMnPc2d1
úseků	úsek	k1gInPc2
DNA	DNA	kA
v	v	k7c6
telomerických	telomerický	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
chromozomů	chromozom	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgFnPc6
do	do	k7c2
kruhu	kruh	k1gInSc2
párují	párovat	k5eAaImIp3nP
čtyři	čtyři	k4xCgFnPc1
guaninové	guaninový	k2eAgFnPc1d1
báze	báze	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
Co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
se	se	k3xPyFc4
týče	týkat	k5eAaImIp3nS
trojšroubovice	trojšroubovice	k1gFnSc1
DNA	dno	k1gNnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
možná	možná	k9
dočasně	dočasně	k6eAd1
vzniká	vznikat	k5eAaImIp3nS
při	při	k7c6
tzv.	tzv.	kA
crossing-overu	crossing-over	k1gInSc6
<g/>
;	;	kIx,
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
laboratorně	laboratorně	k6eAd1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
trojvláknová	trojvláknový	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
připravena	připravit	k5eAaPmNgFnS
např.	např.	kA
z	z	k7c2
vláken	vlákno	k1gNnPc2
poly	pola	k1gFnSc2
<g/>
(	(	kIx(
<g/>
A	A	kA
<g/>
)	)	kIx)
a	a	k8xC
polydeoxy	polydeox	k1gInPc1
<g/>
(	(	kIx(
<g/>
U	u	k7c2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
DNA	DNA	kA
se	se	k3xPyFc4
také	také	k9
může	moct	k5eAaImIp3nS
větvit	větvit	k5eAaImF
a	a	k8xC
vznikají	vznikat	k5eAaImIp3nP
např.	např.	kA
třívláknová	třívláknový	k2eAgNnPc4d1
či	či	k8xC
čtyřvláknová	čtyřvláknový	k2eAgNnPc4d1
spojení	spojení	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
dvoušroubovicová	dvoušroubovicový	k2eAgFnSc1d1
DNA	dna	k1gFnSc1
na	na	k7c6
jednom	jeden	k4xCgInSc6
svém	svůj	k3xOyFgInSc6
konci	konec	k1gInSc6
lokálně	lokálně	k6eAd1
denaturuje	denaturovat	k5eAaBmIp3nS
a	a	k8xC
na	na	k7c4
uvolněné	uvolněný	k2eAgInPc4d1
konce	konec	k1gInPc4
se	se	k3xPyFc4
připojí	připojit	k5eAaPmIp3nS
třetí	třetí	k4xOgInSc1
řetězec	řetězec	k1gInSc1
–	–	k?
v	v	k7c6
prostředí	prostředí	k1gNnSc6
buňky	buňka	k1gFnSc2
by	by	kYmCp3nS
tato	tento	k3xDgFnSc1
struktura	struktura	k1gFnSc1
mohla	moct	k5eAaImAgFnS
vznikat	vznikat	k5eAaImF
při	při	k7c6
crossing-overu	crossing-over	k1gInSc6
<g/>
,	,	kIx,
pokud	pokud	k8xS
nedošlo	dojít	k5eNaPmAgNnS
k	k	k7c3
replikaci	replikace	k1gFnSc3
v	v	k7c6
jednom	jeden	k4xCgMnSc6
z	z	k7c2
genomů	genom	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Jindy	jindy	k6eAd1
takto	takto	k6eAd1
vlastně	vlastně	k9
denaturují	denaturovat	k5eAaBmIp3nP
dvě	dva	k4xCgFnPc4
dvoušroubovice	dvoušroubovice	k1gFnPc4
a	a	k8xC
vzájemně	vzájemně	k6eAd1
se	se	k3xPyFc4
komplementárně	komplementárně	k6eAd1
přiloží	přiložit	k5eAaPmIp3nP
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
vzniká	vznikat	k5eAaImIp3nS
čtyřvláknové	čtyřvláknový	k2eAgNnSc1d1
spojení	spojení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
crossing-overu	crossing-over	k1gInSc2
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
známý	známý	k2eAgInSc4d1
Hollidayův	Hollidayův	k2eAgInSc4d1
spoj	spoj	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
umožňuje	umožňovat	k5eAaImIp3nS
vlastní	vlastní	k2eAgFnSc4d1
výměnu	výměna	k1gFnSc4
homologních	homologní	k2eAgNnPc2d1
vláken	vlákno	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
Při	při	k7c6
replikaci	replikace	k1gFnSc6
DNA	DNA	kA
či	či	k8xC
při	při	k7c6
opravě	oprava	k1gFnSc6
DNA	dno	k1gNnSc2
mohou	moct	k5eAaImIp3nP
větvení	větvení	k1gNnPc4
vznikat	vznikat	k5eAaImF
také	také	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
laboratoři	laboratoř	k1gFnSc6
nicméně	nicméně	k8xC
vznikají	vznikat	k5eAaImIp3nP
ještě	ještě	k6eAd1
mnohem	mnohem	k6eAd1
fantastičtější	fantastický	k2eAgFnPc4d2
prostorové	prostorový	k2eAgFnPc4d1
struktury	struktura	k1gFnPc4
DNA	DNA	kA
–	–	k?
byly	být	k5eAaImAgInP
vyrobeny	vyrobit	k5eAaPmNgInP
např.	např.	kA
krychle	krychle	k1gFnSc1
či	či	k8xC
osmistěn	osmistěn	k1gInSc1
složené	složený	k2eAgFnSc2d1
celé	celá	k1gFnSc2
pouze	pouze	k6eAd1
z	z	k7c2
DNA	dno	k1gNnSc2
molekul	molekula	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
a	a	k8xC
další	další	k2eAgFnPc1d1
syntetické	syntetický	k2eAgFnPc1d1
struktury	struktura	k1gFnPc1
DNA	dno	k1gNnSc2
jsou	být	k5eAaImIp3nP
v	v	k7c6
centru	centrum	k1gNnSc6
zájmu	zájem	k1gInSc2
DNA	DNA	kA
nanotechnologů	nanotechnolog	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vyšší	vysoký	k2eAgFnPc1d2
úrovně	úroveň	k1gFnPc1
struktury	struktura	k1gFnSc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
kondenzace	kondenzace	k1gFnSc2
DNA	DNA	kA
<g/>
.	.	kIx.
</s>
<s>
Genom	genom	k1gInSc1
<g/>
,	,	kIx,
tedy	tedy	k8xC
souhrn	souhrn	k1gInSc1
DNA	DNA	kA
v	v	k7c6
buňce	buňka	k1gFnSc6
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
pouhou	pouhý	k2eAgFnSc7d1
změtí	změt	k1gFnSc7
dvoušroubovicové	dvoušroubovicový	k2eAgFnSc6d1
DNA	DNA	kA
–	–	k?
na	na	k7c6
vyšších	vysoký	k2eAgFnPc6d2
úrovních	úroveň	k1gFnPc6
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
pozorovat	pozorovat	k5eAaImF
komplikované	komplikovaný	k2eAgNnSc4d1
vinutí	vinutí	k1gNnSc4
a	a	k8xC
četné	četný	k2eAgFnPc4d1
interakce	interakce	k1gFnPc4
s	s	k7c7
buněčnými	buněčný	k2eAgFnPc7d1
bílkovinami	bílkovina	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
struktury	struktura	k1gFnPc1
také	také	k9
nesou	nést	k5eAaImIp3nP
genetickou	genetický	k2eAgFnSc4d1
informaci	informace	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
Zcela	zcela	k6eAd1
typické	typický	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
tzv.	tzv.	kA
nadšroubovicové	nadšroubovicový	k2eAgNnSc1d1
vinutí	vinutí	k1gNnSc1
(	(	kIx(
<g/>
supercoiling	supercoiling	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tedy	tedy	k8xC
dodatečné	dodatečný	k2eAgNnSc1d1
šroubovicové	šroubovicový	k2eAgNnSc1d1
vinutí	vinutí	k1gNnSc1
již	již	k6eAd1
existující	existující	k2eAgFnPc4d1
dvoušroubovice	dvoušroubovice	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
Nadšroubovicové	Nadšroubovicový	k2eAgNnSc1d1
vinutí	vinutí	k1gNnSc1
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
zjednodušeně	zjednodušeně	k6eAd1
představit	představit	k5eAaPmF
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
držíme	držet	k5eAaImIp1nP
v	v	k7c6
každé	každý	k3xTgFnSc6
ruce	ruka	k1gFnSc6
jeden	jeden	k4xCgInSc4
z	z	k7c2
obou	dva	k4xCgInPc2
konců	konec	k1gInPc2
provázku	provázek	k1gInSc2
a	a	k8xC
postupně	postupně	k6eAd1
na	na	k7c6
jednom	jeden	k4xCgInSc6
konci	konec	k1gInSc6
provázek	provázek	k1gInSc4
kroutíme	kroutit	k5eAaImIp1nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzniklé	vzniklý	k2eAgNnSc1d1
napětí	napětí	k1gNnSc1
se	se	k3xPyFc4
opět	opět	k6eAd1
uvolní	uvolnit	k5eAaPmIp3nS
(	(	kIx(
<g/>
relaxuje	relaxovat	k5eAaBmIp3nS
<g/>
)	)	kIx)
jen	jen	k9
tehdy	tehdy	k6eAd1
<g/>
,	,	kIx,
pokud	pokud	k8xS
uvolníme	uvolnit	k5eAaPmIp1nP
jednu	jeden	k4xCgFnSc4
ruku	ruka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvoušroubovice	Dvoušroubovice	k1gFnSc1
je	být	k5eAaImIp3nS
však	však	k9
stočená	stočený	k2eAgFnSc1d1
již	již	k6eAd1
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
relaxovaném	relaxovaný	k2eAgInSc6d1
stavu	stav	k1gInSc6
(	(	kIx(
<g/>
jedna	jeden	k4xCgFnSc1
otáčka	otáčka	k1gFnSc1
každých	každý	k3xTgInPc2
cca	cca	kA
10	#num#	k4
párů	pár	k1gInPc2
bází	báze	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
můžeme	moct	k5eAaImIp1nP
rozlišit	rozlišit	k5eAaPmF
<g/>
,	,	kIx,
zda	zda	k8xS
se	se	k3xPyFc4
nadšroubovice	nadšroubovice	k1gFnSc1
vine	vinout	k5eAaImIp3nS
stejným	stejný	k2eAgInSc7d1
směrem	směr	k1gInSc7
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
dvoušroubovice	dvoušroubovice	k1gFnSc1
(	(	kIx(
<g/>
tzv.	tzv.	kA
pozitivní	pozitivní	k2eAgInSc1d1
supercoiling	supercoiling	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
směrem	směr	k1gInSc7
opačným	opačný	k2eAgInSc7d1
(	(	kIx(
<g/>
negativní	negativní	k2eAgInSc4d1
supercoiling	supercoiling	k1gInSc4
<g/>
,	,	kIx,
uvolňuje	uvolňovat	k5eAaImIp3nS
DNA	DNA	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadšroubovicové	Nadšroubovicový	k2eAgNnSc1d1
vinutí	vinutí	k1gNnSc1
má	mít	k5eAaImIp3nS
celou	celý	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
důležitých	důležitý	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
a	a	k8xC
regulačních	regulační	k2eAgFnPc2d1
rolí	role	k1gFnPc2
<g/>
;	;	kIx,
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
žádném	žádný	k3yNgInSc6
případě	případ	k1gInSc6
se	se	k3xPyFc4
nejedná	jednat	k5eNaImIp3nS
pouze	pouze	k6eAd1
o	o	k7c4
anomálii	anomálie	k1gFnSc4
ve	v	k7c6
struktuře	struktura	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
DNA	DNA	kA
se	se	k3xPyFc4
v	v	k7c6
buňce	buňka	k1gFnSc6
dále	daleko	k6eAd2
organizuje	organizovat	k5eAaBmIp3nS
do	do	k7c2
mikroskopicky	mikroskopicky	k6eAd1
pozorovatelných	pozorovatelný	k2eAgInPc2d1
útvarů	útvar	k1gInPc2
známých	známá	k1gFnPc2
jako	jako	k8xS,k8xC
chromozomy	chromozom	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
bakterií	bakterie	k1gFnPc2
je	být	k5eAaImIp3nS
zřejmě	zřejmě	k6eAd1
systém	systém	k1gInSc1
kondenzace	kondenzace	k1gFnSc2
DNA	DNA	kA
do	do	k7c2
(	(	kIx(
<g/>
obvykle	obvykle	k6eAd1
jediného	jediný	k2eAgInSc2d1
<g/>
)	)	kIx)
chromozomu	chromozom	k1gInSc2
poněkud	poněkud	k6eAd1
méně	málo	k6eAd2
propracovaný	propracovaný	k2eAgInSc1d1
a	a	k8xC
např.	např.	kA
u	u	k7c2
Escherichia	Escherichium	k1gNnSc2
coli	col	k1gFnSc2
zahrnuje	zahrnovat	k5eAaImIp3nS
několik	několik	k4yIc1
proteinů	protein	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
schopné	schopný	k2eAgFnPc1d1
udržovat	udržovat	k5eAaImF
nadšroubovicové	nadšroubovicový	k2eAgNnSc4d1
vinutí	vinutí	k1gNnSc4
a	a	k8xC
vytvářet	vytvářet	k5eAaImF
ostré	ostrý	k2eAgInPc4d1
ohyby	ohyb	k1gInPc4
vlákna	vlákno	k1gNnSc2
DNA	DNA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Eukaryotické	Eukaryotický	k2eAgInPc1d1
organismy	organismus	k1gInPc1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
velmi	velmi	k6eAd1
komplikovaně	komplikovaně	k6eAd1
sbalenou	sbalený	k2eAgFnSc4d1
DNA	DNA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souvisí	souviset	k5eAaImIp3nS
to	ten	k3xDgNnSc1
s	s	k7c7
délkou	délka	k1gFnSc7
jejich	jejich	k3xOp3gNnSc2
DNA	dno	k1gNnSc2
–	–	k?
např.	např.	kA
lidský	lidský	k2eAgInSc1d1
genom	genom	k1gInSc1
má	mít	k5eAaImIp3nS
na	na	k7c4
délku	délka	k1gFnSc4
dva	dva	k4xCgInPc4
metry	metr	k1gInPc4
<g/>
,	,	kIx,
přitom	přitom	k6eAd1
buněčné	buněčný	k2eAgNnSc1d1
jádro	jádro	k1gNnSc1
má	mít	k5eAaImIp3nS
na	na	k7c4
délku	délka	k1gFnSc4
několik	několik	k4yIc4
mikrometrů	mikrometr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvouvlákno	Dvouvlákno	k1gNnSc1
DNA	DNA	kA
se	se	k3xPyFc4
nejprve	nejprve	k6eAd1
nabaluje	nabalovat	k5eAaImIp3nS
na	na	k7c4
bazické	bazický	k2eAgInPc4d1
proteiny	protein	k1gInPc4
známé	známý	k2eAgInPc4d1
jako	jako	k8xS,k8xC
histony	histon	k1gInPc1
<g/>
;	;	kIx,
DNA	DNA	kA
nabalená	nabalený	k2eAgFnSc1d1
na	na	k7c4
osm	osm	k4xCc4
histonů	histon	k1gInPc2
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
tzv.	tzv.	kA
„	„	k?
<g/>
nukleozom	nukleozom	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
na	na	k7c6
této	tento	k3xDgFnSc6
úrovni	úroveň	k1gFnSc6
DNA	dno	k1gNnSc2
vypadá	vypadat	k5eAaPmIp3nS,k5eAaImIp3nS
jako	jako	k9
řada	řada	k1gFnSc1
korálků	korálek	k1gInPc2
(	(	kIx(
<g/>
nukleozomů	nukleozom	k1gInPc2
<g/>
)	)	kIx)
na	na	k7c6
provázku	provázek	k1gInSc6
(	(	kIx(
<g/>
DNA	DNA	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
korálky	korálek	k1gInPc4
se	se	k3xPyFc4
však	však	k9
obvykle	obvykle	k6eAd1
ještě	ještě	k6eAd1
stáčí	stáčet	k5eAaImIp3nS
do	do	k7c2
30	#num#	k4
nanometrů	nanometr	k1gInPc2
tlusté	tlustý	k2eAgFnSc2d1
šroubovice	šroubovice	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
vzniku	vznik	k1gInSc6
chromozomů	chromozom	k1gInPc2
se	se	k3xPyFc4
podílí	podílet	k5eAaImIp3nS
ještě	ještě	k6eAd1
vyšší	vysoký	k2eAgFnPc4d2
úrovně	úroveň	k1gFnPc4
sbalení	sbalení	k1gNnSc2
DNA	dno	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
však	však	k9
méně	málo	k6eAd2
prostudované	prostudovaný	k2eAgFnPc1d1
a	a	k8xC
vznikají	vznikat	k5eAaImIp3nP
jen	jen	k9
v	v	k7c6
určitých	určitý	k2eAgFnPc6d1
fázích	fáze	k1gFnPc6
buněčného	buněčný	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Struktura	struktura	k1gFnSc1
chromatinu	chromatin	k1gInSc2
u	u	k7c2
jaderných	jaderný	k2eAgInPc2d1
organismů	organismus	k1gInPc2
<g/>
:	:	kIx,
v	v	k7c6
buňce	buňka	k1gFnSc6
se	s	k7c7
DNA	DNA	kA
shlukuje	shlukovat	k5eAaImIp3nS
do	do	k7c2
komplikovaných	komplikovaný	k2eAgInPc2d1
kondenzovaných	kondenzovaný	k2eAgInPc2d1
útvarů	útvar	k1gInPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
k	k	k7c3
nejvyšší	vysoký	k2eAgFnSc3d3
kondenzaci	kondenzace	k1gFnSc3
dochází	docházet	k5eAaImIp3nS
během	během	k7c2
buněčného	buněčný	k2eAgNnSc2d1
dělení	dělení	k1gNnSc2
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Vzestup	vzestup	k1gInSc1
absorbance	absorbance	k1gFnSc2
jako	jako	k8xS,k8xC
měřítko	měřítko	k1gNnSc4
denaturačního	denaturační	k2eAgInSc2d1
procesu	proces	k1gInSc2
nukleových	nukleový	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
text	text	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
DNA	DNA	kA
je	být	k5eAaImIp3nS
polymerní	polymerní	k2eAgFnSc7d1
sloučeninou	sloučenina	k1gFnSc7
s	s	k7c7
vysokou	vysoký	k2eAgFnSc7d1
molární	molární	k2eAgFnSc7d1
hmotností	hmotnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Molární	molární	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
závisí	záviset	k5eAaImIp3nS
na	na	k7c6
délce	délka	k1gFnSc6
DNA	dno	k1gNnSc2
a	a	k8xC
zhruba	zhruba	k6eAd1
platí	platit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
s	s	k7c7
každým	každý	k3xTgInSc7
nukleotidem	nukleotid	k1gInSc7
stoupá	stoupat	k5eAaImIp3nS
molární	molární	k2eAgFnSc4d1
hmotnost	hmotnost	k1gFnSc4
o	o	k7c4
330	#num#	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
případě	případ	k1gInSc6
dvouvláknové	dvouvláknový	k2eAgFnSc2d1
DNA	dno	k1gNnPc4
na	na	k7c4
jeden	jeden	k4xCgInSc4
pár	pár	k1gInSc4
bází	báze	k1gFnSc7
připadá	připadat	k5eAaImIp3nS,k5eAaPmIp3nS
asi	asi	k9
650	#num#	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
Deoxyribonukleová	deoxyribonukleový	k2eAgFnSc1d1
kyselina	kyselina	k1gFnSc1
je	být	k5eAaImIp3nS
záporně	záporně	k6eAd1
nabitá	nabitý	k2eAgFnSc1d1
(	(	kIx(
<g/>
díky	díky	k7c3
fosforečnanovým	fosforečnanův	k2eAgFnPc3d1
skupinám	skupina	k1gFnPc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
je	být	k5eAaImIp3nS
tedy	tedy	k9
polárního	polární	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomu	ten	k3xDgNnSc3
je	být	k5eAaImIp3nS
rozpustná	rozpustný	k2eAgFnSc1d1
ve	v	k7c6
vodě	voda	k1gFnSc6
<g/>
,	,	kIx,
naopak	naopak	k6eAd1
v	v	k7c6
ethanolu	ethanol	k1gInSc6
se	se	k3xPyFc4
sráží	srážet	k5eAaImIp3nS
(	(	kIx(
<g/>
neboť	neboť	k8xC
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
vyvázání	vyvázání	k1gNnSc3
záporných	záporný	k2eAgInPc2d1
nábojů	náboj	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
vysrážení	vysrážení	k1gNnSc6
má	mít	k5eAaImIp3nS
DNA	DNA	kA
bílou	bílý	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
Izolovaná	izolovaný	k2eAgFnSc1d1
DNA	dna	k1gFnSc1
zaujímá	zaujímat	k5eAaImIp3nS
dvoušroubovicové	dvoušroubovicový	k2eAgNnSc4d1
uspořádání	uspořádání	k1gNnSc4
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
však	však	k9
možné	možný	k2eAgNnSc1d1
rozrušit	rozrušit	k5eAaPmF
v	v	k7c6
procesu	proces	k1gInSc6
denaturace	denaturace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typicky	typicky	k6eAd1
se	se	k3xPyFc4
denaturace	denaturace	k1gFnSc1
provádí	provádět	k5eAaImIp3nS
zvýšením	zvýšení	k1gNnSc7
teploty	teplota	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
denaturaci	denaturace	k1gFnSc4
způsobuje	způsobovat	k5eAaImIp3nS
i	i	k9
nízká	nízký	k2eAgFnSc1d1
iontová	iontový	k2eAgFnSc1d1
síla	síla	k1gFnSc1
roztoku	roztok	k1gInSc2
nebo	nebo	k8xC
silně	silně	k6eAd1
zásadité	zásaditý	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
kyselé	kyselý	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
není	být	k5eNaImIp3nS
vhodné	vhodný	k2eAgNnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
hydrolýze	hydrolýza	k1gFnSc3
glykosidových	glykosidový	k2eAgFnPc2d1
vazeb	vazba	k1gFnPc2
mezi	mezi	k7c7
cukrem	cukr	k1gInSc7
a	a	k8xC
bází	báze	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
DNA	DNA	kA
absorbuje	absorbovat	k5eAaBmIp3nS
v	v	k7c6
UV	UV	kA
oblasti	oblast	k1gFnPc4
s	s	k7c7
absorpčním	absorpční	k2eAgNnSc7d1
maximem	maximum	k1gNnSc7
při	při	k7c6
vlnové	vlnový	k2eAgFnSc6d1
délce	délka	k1gFnSc6
260	#num#	k4
nm	nm	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
denaturaci	denaturace	k1gFnSc6
DNA	dno	k1gNnSc2
se	se	k3xPyFc4
absorbance	absorbance	k1gFnSc1
v	v	k7c6
této	tento	k3xDgFnSc6
oblasti	oblast	k1gFnSc6
zvyšuje	zvyšovat	k5eAaImIp3nS
–	–	k?
tomuto	tento	k3xDgInSc3
jevu	jev	k1gInSc3
se	se	k3xPyFc4
říká	říkat	k5eAaImIp3nS
hyperchromní	hyperchromní	k2eAgInSc1d1
efekt	efekt	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
dáno	dát	k5eAaPmNgNnS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c6
absorpci	absorpce	k1gFnSc6
se	se	k3xPyFc4
v	v	k7c6
největší	veliký	k2eAgFnSc6d3
míře	míra	k1gFnSc6
podílejí	podílet	k5eAaImIp3nP
báze	báze	k1gFnPc1
DNA	dno	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
dsDNA	dsDNA	k?
„	„	k?
<g/>
schované	schovaná	k1gFnSc2
<g/>
“	“	k?
uvnitř	uvnitř	k7c2
dvoušroubovice	dvoušroubovice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
denaturaci	denaturace	k1gFnSc6
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
„	„	k?
<g/>
obnažení	obnažení	k1gNnSc2
<g/>
“	“	k?
bází	báze	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
tak	tak	k9
mohou	moct	k5eAaImIp3nP
lépe	dobře	k6eAd2
absorbovat	absorbovat	k5eAaBmF
UV	UV	kA
záření	záření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Poločas	poločas	k1gInSc1
rozpadu	rozpad	k1gInSc2
DNA	DNA	kA
činí	činit	k5eAaImIp3nS
dle	dle	k7c2
studia	studio	k1gNnSc2
kosterních	kosterní	k2eAgInPc2d1
nálezů	nález	k1gInPc2
asi	asi	k9
521	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
DNA	DNA	kA
je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
za	za	k7c4
stabilní	stabilní	k2eAgFnSc4d1
molekulu	molekula	k1gFnSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vynikne	vyniknout	k5eAaPmIp3nS
zejména	zejména	k9
při	při	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
RNA	RNA	kA
jakožto	jakožto	k8xS
druhou	druhý	k4xOgFnSc7
významnou	významný	k2eAgFnSc7d1
nukleovou	nukleový	k2eAgFnSc7d1
kyselinou	kyselina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
molekule	molekula	k1gFnSc6
DNA	dno	k1gNnSc2
není	být	k5eNaImIp3nS
na	na	k7c4
2	#num#	k4
<g/>
'	'	kIx"
uhlíku	uhlík	k1gInSc2
OH	OH	kA
skupina	skupina	k1gFnSc1
–	–	k?
u	u	k7c2
RNA	RNA	kA
tam	tam	k6eAd1
tato	tento	k3xDgFnSc1
reaktivní	reaktivní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
je	být	k5eAaImIp3nS
a	a	k8xC
způsobuje	způsobovat	k5eAaImIp3nS
nižší	nízký	k2eAgFnSc4d2
stabilitu	stabilita	k1gFnSc4
RNA	RNA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
DNA	DNA	kA
se	se	k3xPyFc4
v	v	k7c6
laboratoři	laboratoř	k1gFnSc6
dlouhodobě	dlouhodobě	k6eAd1
skladuje	skladovat	k5eAaImIp3nS
při	při	k7c6
−	−	k?
<g/>
°	°	k?
nebo	nebo	k8xC
−	−	k?
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
vydrží	vydržet	k5eAaPmIp3nS
i	i	k9
několik	několik	k4yIc4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
teplotě	teplota	k1gFnSc6
4	#num#	k4
°	°	k?
<g/>
C	C	kA
v	v	k7c6
TE	TE	kA
pufru	pufr	k1gInSc2
vydrží	vydržet	k5eAaPmIp3nS
několik	několik	k4yIc1
týdnů	týden	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
Existuje	existovat	k5eAaImIp3nS
mnoho	mnoho	k4c1
různých	různý	k2eAgFnPc2d1
metod	metoda	k1gFnPc2
k	k	k7c3
uchování	uchování	k1gNnSc3
DNA	dno	k1gNnSc2
na	na	k7c4
delší	dlouhý	k2eAgInSc4d2
čas	čas	k1gInSc4
(	(	kIx(
<g/>
zmrazení	zmrazení	k1gNnSc3
vzorků	vzorek	k1gInPc2
tekutým	tekutý	k2eAgInSc7d1
dusíkem	dusík	k1gInSc7
<g/>
,	,	kIx,
FTA	FTA	kA
karty	karta	k1gFnSc2
<g/>
,	,	kIx,
plastové	plastový	k2eAgFnSc2d1
mikrozkumavky	mikrozkumavka	k1gFnSc2
<g/>
,	,	kIx,
uchování	uchování	k1gNnSc2
pomocí	pomocí	k7c2
chitosanu	chitosan	k1gInSc2
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvnitř	uvnitř	k7c2
těl	tělo	k1gNnPc2
živých	živý	k2eAgInPc2d1
organismů	organismus	k1gInPc2
však	však	k9
DNA	dna	k1gFnSc1
musí	muset	k5eAaImIp3nS
snášet	snášet	k5eAaImF
i	i	k9
poměrně	poměrně	k6eAd1
vysoké	vysoký	k2eAgFnPc4d1
teploty	teplota	k1gFnPc4
<g/>
,	,	kIx,
a	a	k8xC
přesto	přesto	k8xC
vydrží	vydržet	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krajním	krajní	k2eAgInSc7d1
případem	případ	k1gInSc7
jsou	být	k5eAaImIp3nP
hypertermofilní	hypertermofilní	k2eAgInPc1d1
organismy	organismus	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
žijí	žít	k5eAaImIp3nP
i	i	k9
při	při	k7c6
teplotách	teplota	k1gFnPc6
kolem	kolem	k7c2
100	#num#	k4
°	°	k?
<g/>
C.	C.	kA
Jejich	jejich	k3xOp3gNnPc4
DNA	dno	k1gNnPc4
čelí	čelit	k5eAaImIp3nS
jak	jak	k6eAd1
riziku	riziko	k1gNnSc3
denaturace	denaturace	k1gFnSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k8xC
termodegradaci	termodegradace	k1gFnSc3
(	(	kIx(
<g/>
rozpadu	rozpad	k1gInSc3
pevných	pevný	k2eAgFnPc2d1
chemických	chemický	k2eAgFnPc2d1
vazeb	vazba	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
žijí	žít	k5eAaImIp3nP
a	a	k8xC
mimo	mimo	k6eAd1
opravných	opravný	k2eAgInPc2d1
mechanismů	mechanismus	k1gInPc2
k	k	k7c3
tomu	ten	k3xDgNnSc3
zřejmě	zřejmě	k6eAd1
přispívá	přispívat	k5eAaImIp3nS
i	i	k9
nadšroubovicové	nadšroubovicový	k2eAgNnSc1d1
vinutí	vinutí	k1gNnSc1
a	a	k8xC
také	také	k9
optimální	optimální	k2eAgNnSc4d1
iontové	iontový	k2eAgNnSc4d1
složení	složení	k1gNnSc4
cytoplazmy	cytoplazma	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pro	pro	k7c4
DNA	DNA	kA
jsou	být	k5eAaImIp3nP
však	však	k9
dále	daleko	k6eAd2
typické	typický	k2eAgFnPc1d1
i	i	k9
některé	některý	k3yIgFnPc1
vlastnosti	vlastnost	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
ji	on	k3xPp3gFnSc4
do	do	k7c2
jisté	jistý	k2eAgFnSc2d1
míry	míra	k1gFnSc2
odlišují	odlišovat	k5eAaImIp3nP
od	od	k7c2
běžných	běžný	k2eAgFnPc2d1
chemických	chemický	k2eAgFnPc2d1
látek	látka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
buňce	buňka	k1gFnSc6
je	být	k5eAaImIp3nS
například	například	k6eAd1
možné	možný	k2eAgNnSc1d1
replikovat	replikovat	k5eAaImF
DNA	dno	k1gNnPc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
vytvářet	vytvářet	k5eAaImF
její	její	k3xOp3gFnPc4
kopie	kopie	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Víceméně	víceméně	k9
každé	každý	k3xTgNnSc1
buněčné	buněčný	k2eAgNnSc1d1
dělení	dělení	k1gNnSc1
vyžaduje	vyžadovat	k5eAaImIp3nS
zmnožení	zmnožení	k1gNnSc4
genetické	genetický	k2eAgFnSc2d1
informace	informace	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
jí	on	k3xPp3gFnSc3
v	v	k7c6
každé	každý	k3xTgFnSc6
buňce	buňka	k1gFnSc6
bylo	být	k5eAaImAgNnS
stále	stále	k6eAd1
konstantní	konstantní	k2eAgNnSc1d1
množství	množství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
procesu	proces	k1gInSc2
se	se	k3xPyFc4
oddělí	oddělit	k5eAaPmIp3nP
řetězce	řetězec	k1gInPc1
mateřské	mateřský	k2eAgInPc1d1
DNA	DNA	kA
a	a	k8xC
oba	dva	k4xCgMnPc1
slouží	sloužit	k5eAaImIp3nP
jako	jako	k8xS,k8xC
návod	návod	k1gInSc1
(	(	kIx(
<g/>
tzv.	tzv.	kA
„	„	k?
<g/>
templát	templát	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
pro	pro	k7c4
tvorbu	tvorba	k1gFnSc4
druhých	druhý	k4xOgFnPc2
vláken	vlákna	k1gFnPc2
v	v	k7c6
rámci	rámec	k1gInSc6
obou	dva	k4xCgFnPc2
nově	nově	k6eAd1
vznikajících	vznikající	k2eAgFnPc2d1
dvoušroubovic	dvoušroubovice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgInPc4
jsou	být	k5eAaImIp3nP
následně	následně	k6eAd1
napůl	napůl	k6eAd1
tvořeny	tvořit	k5eAaImNgFnP
původní	původní	k2eAgFnPc1d1
DNA	dna	k1gFnSc1
a	a	k8xC
napůl	napůl	k6eAd1
nově	nově	k6eAd1
dosyntetizované	dosyntetizovaný	k2eAgNnSc4d1
–	–	k?
celý	celý	k2eAgInSc1d1
proces	proces	k1gInSc1
je	být	k5eAaImIp3nS
semikonzervativní	semikonzervativní	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
dalším	další	k2eAgFnPc3d1
zajímavým	zajímavý	k2eAgFnPc3d1
vlastnostem	vlastnost	k1gFnPc3
DNA	dno	k1gNnSc2
v	v	k7c6
buňkách	buňka	k1gFnPc6
patří	patřit	k5eAaImIp3nS
možnost	možnost	k1gFnSc1
opravovat	opravovat	k5eAaImF
DNA	DNA	kA
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
ještě	ještě	k6eAd1
dále	daleko	k6eAd2
vylepšuje	vylepšovat	k5eAaImIp3nS
(	(	kIx(
<g/>
už	už	k9
tak	tak	k9
poměrně	poměrně	k6eAd1
precizní	precizní	k2eAgMnSc1d1
<g/>
)	)	kIx)
přenos	přenos	k1gInSc1
genetické	genetický	k2eAgFnSc2d1
informace	informace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
Bylo	být	k5eAaImAgNnS
by	by	kYmCp3nP
možno	možno	k6eAd1
najít	najít	k5eAaPmF
množství	množství	k1gNnSc4
dalších	další	k2eAgFnPc2d1
pozoruhodných	pozoruhodný	k2eAgFnPc2d1
vlastností	vlastnost	k1gFnPc2
DNA	dno	k1gNnSc2
<g/>
,	,	kIx,
vesměs	vesměs	k6eAd1
probíhajících	probíhající	k2eAgFnPc2d1
v	v	k7c6
buňce	buňka	k1gFnSc6
za	za	k7c2
pomoci	pomoc	k1gFnSc2
speciálních	speciální	k2eAgInPc2d1
enzymů	enzym	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Funkce	funkce	k1gFnSc1
</s>
<s>
Příklad	příklad	k1gInSc1
sekvence	sekvence	k1gFnSc2
DNA	DNA	kA
<g/>
:	:	kIx,
dole	dole	k6eAd1
jsou	být	k5eAaImIp3nP
uvedena	uveden	k2eAgNnPc1d1
písmena	písmeno	k1gNnPc1
reprezentující	reprezentující	k2eAgInPc4d1
jednotlivé	jednotlivý	k2eAgInPc4d1
nukleotidy	nukleotid	k1gInPc4
v	v	k7c6
lineárním	lineární	k2eAgInSc6d1
řetězci	řetězec	k1gInSc6
DNA	DNA	kA
<g/>
,	,	kIx,
tak	tak	k6eAd1
jak	jak	k6eAd1
jsou	být	k5eAaImIp3nP
čteny	čten	k2eAgInPc1d1
při	při	k7c6
sekvenování	sekvenování	k1gNnSc6
(	(	kIx(
<g/>
graf	graf	k1gInSc1
výše	výše	k1gFnSc2,k1gFnSc2wB
vzniká	vznikat	k5eAaImIp3nS
snímáním	snímání	k1gNnSc7
fluorescenčních	fluorescenční	k2eAgFnPc2d1
značek	značka	k1gFnPc2
při	při	k7c6
určitých	určitý	k2eAgInPc6d1
typech	typ	k1gInPc6
sekvenování	sekvenování	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článcích	článek	k1gInPc6
genom	genom	k1gInSc1
<g/>
,	,	kIx,
sekvence	sekvence	k1gFnPc1
DNA	DNA	kA
a	a	k8xC
genetický	genetický	k2eAgInSc4d1
kód	kód	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
DNA	DNA	kA
je	být	k5eAaImIp3nS
nositelkou	nositelka	k1gFnSc7
genetické	genetický	k2eAgFnSc2d1
informace	informace	k1gFnSc2
všech	všecek	k3xTgInPc2
živých	živý	k2eAgInPc2d1
organismů	organismus	k1gInPc2
v	v	k7c6
pravém	pravý	k2eAgInSc6d1
slova	slovo	k1gNnSc2
smyslu	smysl	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
mnoha	mnoho	k4c2
virů	vir	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
DNA	DNA	kA
je	být	k5eAaImIp3nS
zapsána	zapsán	k2eAgFnSc1d1
sekvence	sekvence	k1gFnSc1
všech	všecek	k3xTgFnPc2
bílkovin	bílkovina	k1gFnPc2
a	a	k8xC
přeneseně	přeneseně	k6eAd1
je	být	k5eAaImIp3nS
genetickou	genetický	k2eAgFnSc7d1
informací	informace	k1gFnSc7
podmíněna	podmíněn	k2eAgFnSc1d1
existence	existence	k1gFnSc1
všech	všecek	k3xTgFnPc2
biomolekul	biomolekula	k1gFnPc2
a	a	k8xC
buněčných	buněčný	k2eAgFnPc2d1
struktur	struktura	k1gFnPc2
(	(	kIx(
<g/>
k	k	k7c3
jejichž	jejichž	k3xOyRp3gFnSc3
tvorbě	tvorba	k1gFnSc3
jsou	být	k5eAaImIp3nP
potřeba	potřeba	k6eAd1
bílkoviny	bílkovina	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
Schopnost	schopnost	k1gFnSc1
ukládat	ukládat	k5eAaImF
a	a	k8xC
přenášet	přenášet	k5eAaImF
genetickou	genetický	k2eAgFnSc4d1
informaci	informace	k1gFnSc4
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
fundamentálních	fundamentální	k2eAgFnPc2d1
vlastností	vlastnost	k1gFnPc2
života	život	k1gInSc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
Bez	bez	k7c2
DNA	dno	k1gNnSc2
buňky	buňka	k1gFnSc2
vydrží	vydržet	k5eAaPmIp3nS
žít	žít	k5eAaImF
jen	jen	k9
omezenou	omezený	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
<g/>
;	;	kIx,
například	například	k6eAd1
lidské	lidský	k2eAgFnPc4d1
červené	červený	k2eAgFnPc4d1
krvinky	krvinka	k1gFnPc4
při	při	k7c6
svém	svůj	k3xOyFgNnSc6
zrání	zrání	k1gNnSc6
vyvrhují	vyvrhovat	k5eAaImIp3nP
jádro	jádro	k1gNnSc4
<g/>
,	,	kIx,
a	a	k8xC
protože	protože	k8xS
pak	pak	k6eAd1
nejsou	být	k5eNaImIp3nP
schopné	schopný	k2eAgFnPc1d1
vyrábět	vyrábět	k5eAaImF
nové	nový	k2eAgFnPc4d1
bílkoviny	bílkovina	k1gFnPc4
a	a	k8xC
udržovat	udržovat	k5eAaImF
buňku	buňka	k1gFnSc4
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
po	po	k7c6
několika	několik	k4yIc6
měsících	měsíc	k1gInPc6
poškozeny	poškozen	k2eAgFnPc1d1
a	a	k8xC
musí	muset	k5eAaImIp3nP
se	se	k3xPyFc4
z	z	k7c2
<g />
.	.	kIx.
</s>
<s hack="1">
oběhu	oběh	k1gInSc2
odstraňovat	odstraňovat	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
Některé	některý	k3yIgInPc1
viry	vir	k1gInPc1
jsou	být	k5eAaImIp3nP
sice	sice	k8xC
schopné	schopný	k2eAgNnSc1d1
uchovávat	uchovávat	k5eAaImF
svůj	svůj	k3xOyFgInSc4
genetický	genetický	k2eAgInSc4d1
materiál	materiál	k1gInSc4
v	v	k7c6
podobě	podoba	k1gFnSc6
RNA	RNA	kA
(	(	kIx(
<g/>
tzv.	tzv.	kA
RNA	RNA	kA
viry	vir	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jenže	jenže	k8xC
RNA	RNA	kA
genomy	genom	k1gInPc1
nepodléhají	podléhat	k5eNaImIp3nP
opravným	opravný	k2eAgInPc3d1
mechanismům	mechanismus	k1gInPc3
a	a	k8xC
rychle	rychle	k6eAd1
mutují	mutovat	k5eAaImIp3nP
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
mají	mít	k5eAaImIp3nP
limitovanou	limitovaný	k2eAgFnSc4d1
velikost	velikost	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
Život	život	k1gInSc1
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
jak	jak	k8xS,k8xC
ho	on	k3xPp3gMnSc4
známe	znát	k5eAaImIp1nP
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
proto	proto	k8xC
závislý	závislý	k2eAgMnSc1d1
na	na	k7c4
DNA	dno	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Konkrétní	konkrétní	k2eAgNnSc1d1
uložení	uložení	k1gNnSc1
DNA	DNA	kA
v	v	k7c6
buňce	buňka	k1gFnSc6
závisí	záviset	k5eAaImIp3nS
na	na	k7c6
příslušnosti	příslušnost	k1gFnSc6
organismu	organismus	k1gInSc2
k	k	k7c3
jedné	jeden	k4xCgFnSc3
z	z	k7c2
dvou	dva	k4xCgFnPc2
základních	základní	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
organismů	organismus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bakterie	bakterie	k1gFnSc1
a	a	k8xC
archea	archea	k1gFnSc1
(	(	kIx(
<g/>
souhrnně	souhrnně	k6eAd1
„	„	k?
<g/>
prokaryota	prokaryota	k1gFnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
mají	mít	k5eAaImIp3nP
DNA	DNA	kA
obvykle	obvykle	k6eAd1
uloženu	uložen	k2eAgFnSc4d1
volně	volně	k6eAd1
v	v	k7c6
cytoplazmě	cytoplazma	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obvykle	obvykle	k6eAd1
vzniká	vznikat	k5eAaImIp3nS
pouze	pouze	k6eAd1
jistá	jistý	k2eAgFnSc1d1
jaderná	jaderný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
,	,	kIx,
tzv.	tzv.	kA
nukleoid	nukleoid	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k6eAd1
to	ten	k3xDgNnSc1
řada	řada	k1gFnSc1
bakterií	bakterie	k1gFnPc2
vlastní	vlastní	k2eAgFnSc2d1
i	i	k8xC
malé	malý	k2eAgFnSc2d1
kruhové	kruhový	k2eAgFnSc2d1
molekuly	molekula	k1gFnSc2
DNA	DNA	kA
<g/>
,	,	kIx,
tzv.	tzv.	kA
plazmidy	plazmida	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
umožňují	umožňovat	k5eAaImIp3nP
mimo	mimo	k7c4
jiné	jiný	k2eAgFnPc4d1
horizontální	horizontální	k2eAgFnSc4d1
výměnu	výměna	k1gFnSc4
genetické	genetický	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbylé	zbylý	k2eAgInPc1d1
organismy	organismus	k1gInPc1
<g/>
,	,	kIx,
tedy	tedy	k9
např.	např.	kA
člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
rostliny	rostlina	k1gFnPc1
<g/>
,	,	kIx,
živočichové	živočich	k1gMnPc1
či	či	k8xC
prvoci	prvok	k1gMnPc1
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
DNA	dna	k1gFnSc1
uloženu	uložen	k2eAgFnSc4d1
především	především	k9
v	v	k7c6
buněčné	buněčný	k2eAgFnSc6d1
jádře	jádro	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
však	však	k9
se	se	k3xPyFc4
DNA	dna	k1gFnSc1
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
některých	některý	k3yIgFnPc6
eukaryotických	eukaryotický	k2eAgFnPc6d1
organelách	organela	k1gFnPc6
<g/>
,	,	kIx,
jmenovitě	jmenovitě	k6eAd1
v	v	k7c6
mitochondriích	mitochondrie	k1gFnPc6
a	a	k8xC
v	v	k7c6
plastidech	plastid	k1gInPc6
<g/>
,	,	kIx,
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
buňka	buňka	k1gFnSc1
vlastní	vlastní	k2eAgFnSc1d1
(	(	kIx(
<g/>
jev	jev	k1gInSc1
zvaný	zvaný	k2eAgInSc1d1
mimojaderná	mimojaderný	k2eAgFnSc1d1
dědičnost	dědičnost	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Informace	informace	k1gFnSc1
nesená	nesený	k2eAgFnSc1d1
sekvencí	sekvence	k1gFnSc7
nukleotidů	nukleotid	k1gInPc2
v	v	k7c6
DNA	DNA	kA
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
jako	jako	k9
genetická	genetický	k2eAgFnSc1d1
informace	informace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
každé	každý	k3xTgFnSc6
nukleotidové	nukleotidový	k2eAgFnSc6d1
pozici	pozice	k1gFnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
jedna	jeden	k4xCgFnSc1
ze	z	k7c2
čtyř	čtyři	k4xCgFnPc2
bází	báze	k1gFnPc2
(	(	kIx(
<g/>
A	A	kA
<g/>
,	,	kIx,
C	C	kA
<g/>
,	,	kIx,
G	G	kA
či	či	k8xC
T	T	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
sekvence	sekvence	k1gFnSc1
o	o	k7c6
délce	délka	k1gFnSc6
n	n	k0
může	moct	k5eAaImIp3nS
nabývat	nabývat	k5eAaImF
4	#num#	k4
<g/>
n	n	k0
stavů	stav	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
Pro	pro	k7c4
DNA	DNA	kA
dlouhou	dlouhý	k2eAgFnSc4d1
pouhých	pouhý	k2eAgInPc2d1
10	#num#	k4
nukleotidů	nukleotid	k1gInPc2
existuje	existovat	k5eAaImIp3nS
tedy	tedy	k9
teoreticky	teoreticky	k6eAd1
410	#num#	k4
=	=	kIx~
1	#num#	k4
048	#num#	k4
576	#num#	k4
kombinací	kombinace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidský	lidský	k2eAgInSc1d1
genom	genom	k1gInSc1
(	(	kIx(
<g/>
souhrn	souhrn	k1gInSc1
lidské	lidský	k2eAgFnSc2d1
jaderné	jaderný	k2eAgFnSc2d1
DNA	DNA	kA
<g/>
)	)	kIx)
přitom	přitom	k6eAd1
obsahuje	obsahovat	k5eAaImIp3nS
3,1	3,1	k4
miliardy	miliarda	k4xCgFnSc2
(	(	kIx(
<g/>
párů	pár	k1gInPc2
<g/>
)	)	kIx)
bází	báze	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
Nejvyšší	vysoký	k2eAgFnSc1d3
informační	informační	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
se	se	k3xPyFc4
přitom	přitom	k6eAd1
v	v	k7c6
genomu	genom	k1gInSc6
objevuje	objevovat	k5eAaImIp3nS
v	v	k7c6
místech	místo	k1gNnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
sídlí	sídlet	k5eAaImIp3nS
tzv.	tzv.	kA
geny	gen	k1gInPc1
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yRgNnPc1,k3yIgNnPc1
zaznamenávají	zaznamenávat	k5eAaImIp3nP
informaci	informace	k1gFnSc4
pro	pro	k7c4
tvorbu	tvorba	k1gFnSc4
RNA	RNA	kA
a	a	k8xC
potažmo	potažmo	k6eAd1
i	i	k8xC
všech	všecek	k3xTgFnPc2
bílkovin	bílkovina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Informace	informace	k1gFnSc1
pro	pro	k7c4
tvorbu	tvorba	k1gFnSc4
bílkovin	bílkovina	k1gFnPc2
je	být	k5eAaImIp3nS
zašifrována	zašifrovat	k5eAaPmNgFnS
pomocí	pomocí	k7c2
třípísmenného	třípísmenný	k2eAgInSc2d1
kódu	kód	k1gInSc2
známého	známý	k2eAgInSc2d1
jako	jako	k8xS,k8xC
genetický	genetický	k2eAgInSc4d1
kód	kód	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každé	každý	k3xTgInPc4
trojici	trojice	k1gFnSc3
bází	báze	k1gFnPc2
v	v	k7c6
DNA	DNA	kA
totiž	totiž	k9
u	u	k7c2
protein-kódujících	protein-kódující	k2eAgInPc2d1
genů	gen	k1gInPc2
odpovídá	odpovídat	k5eAaImIp3nS
určitá	určitý	k2eAgFnSc1d1
aminokyselina	aminokyselina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aminokyseliny	aminokyselina	k1gFnPc1
jsou	být	k5eAaImIp3nP
základní	základní	k2eAgInPc4d1
stavební	stavební	k2eAgInPc4d1
kameny	kámen	k1gInPc4
bílkovin	bílkovina	k1gFnPc2
<g/>
,	,	kIx,
takže	takže	k8xS
je	být	k5eAaImIp3nS
vlastně	vlastně	k9
genetická	genetický	k2eAgFnSc1d1
informace	informace	k1gFnSc1
jakýmsi	jakýsi	k3yIgInSc7
návodem	návod	k1gInSc7
na	na	k7c4
výrobu	výroba	k1gFnSc4
bílkovin	bílkovina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Genetická	genetický	k2eAgFnSc1d1
informace	informace	k1gFnSc1
je	být	k5eAaImIp3nS
uplatňována	uplatňovat	k5eAaImNgFnS
podle	podle	k7c2
tzv.	tzv.	kA
centrálního	centrální	k2eAgNnSc2d1
dogmatu	dogma	k1gNnSc2
molekulární	molekulární	k2eAgFnSc2d1
biologie	biologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
DNA	dna	k1gFnSc1
je	být	k5eAaImIp3nS
nejprve	nejprve	k6eAd1
přepisována	přepisován	k2eAgFnSc1d1
v	v	k7c6
RNA	RNA	kA
(	(	kIx(
<g/>
obvykle	obvykle	k6eAd1
tzv.	tzv.	kA
messenger	messengra	k1gFnPc2
RNA	RNA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
načež	načež	k6eAd1
je	být	k5eAaImIp3nS
tato	tento	k3xDgFnSc1
RNA	RNA	kA
použita	použít	k5eAaPmNgFnS
jako	jako	k8xS,k8xC
vzor	vzor	k1gInSc1
pro	pro	k7c4
tvorbu	tvorba	k1gFnSc4
bílkovin	bílkovina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc4
zmíněný	zmíněný	k2eAgInSc4d1
krok	krok	k1gInSc4
se	se	k3xPyFc4
jmenuje	jmenovat	k5eAaImIp3nS,k5eAaBmIp3nS
transkripce	transkripce	k1gFnSc1
<g/>
,	,	kIx,
druhý	druhý	k4xOgInSc1
translace	translace	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s>
Velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
genomu	genom	k1gInSc2
mnoha	mnoho	k4c2
organismů	organismus	k1gInPc2
však	však	k9
není	být	k5eNaImIp3nS
součástí	součást	k1gFnSc7
žádného	žádný	k3yNgInSc2
genu	gen	k1gInSc2
a	a	k8xC
dokonce	dokonce	k9
se	se	k3xPyFc4
ani	ani	k8xC
nepřepisuje	přepisovat	k5eNaImIp3nS
v	v	k7c6
RNA	RNA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Role	role	k1gFnSc1
této	tento	k3xDgFnSc6
tzv.	tzv.	kA
nekódující	kódující	k2eNgFnSc1d1
DNA	dna	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
mnoha	mnoho	k4c6
případech	případ	k1gInPc6
neznámá	známý	k2eNgFnSc1d1
<g/>
;	;	kIx,
někdy	někdy	k6eAd1
však	však	k9
pomáhá	pomáhat	k5eAaImIp3nS
regulovat	regulovat	k5eAaImF
spouštění	spouštění	k1gNnSc4
a	a	k8xC
vypínání	vypínání	k1gNnSc4
okolních	okolní	k2eAgInPc2d1
genů	gen	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
Velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
nekódující	kódující	k2eNgFnSc1d1
DNA	dna	k1gFnSc1
dle	dle	k7c2
současné	současný	k2eAgFnSc2d1
úrovně	úroveň	k1gFnSc2
znalostí	znalost	k1gFnPc2
nemá	mít	k5eNaImIp3nS
žádnou	žádný	k3yNgFnSc4
konkrétní	konkrétní	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
a	a	k8xC
označuje	označovat	k5eAaImIp3nS
se	se	k3xPyFc4
prostě	prostě	k9
jako	jako	k9
junk	junk	k6eAd1
(	(	kIx(
<g/>
odpadní	odpadní	k2eAgFnPc4d1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
DNA	DNA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
Část	část	k1gFnSc1
této	tento	k3xDgFnSc3
odpadní	odpadní	k2eAgFnSc1d1
DNA	dna	k1gFnSc1
však	však	k9
podle	podle	k7c2
výsledků	výsledek	k1gInPc2
projektu	projekt	k1gInSc2
ENCODE	ENCODE	kA
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
kóduje	kódovat	k5eAaBmIp3nS
různé	různý	k2eAgNnSc1d1
krátké	krátké	k1gNnSc1
regulační	regulační	k2eAgFnSc2d1
RNA	RNA	kA
<g/>
;	;	kIx,
celkem	celkem	k6eAd1
se	se	k3xPyFc4
odhaduje	odhadovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
10	#num#	k4
<g/>
–	–	k?
<g/>
20	#num#	k4
%	%	kIx~
genomu	genom	k1gInSc2
má	mít	k5eAaImIp3nS
díky	díky	k7c3
těmto	tento	k3xDgInPc3
RNA	RNA	kA
významnou	významný	k2eAgFnSc4d1
regulační	regulační	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
těsném	těsný	k2eAgNnSc6d1
okolí	okolí	k1gNnSc6
těchto	tento	k3xDgFnPc2
regulačních	regulační	k2eAgFnPc2d1
sekvencí	sekvence	k1gFnPc2
se	se	k3xPyFc4
tak	tak	k6eAd1
podle	podle	k7c2
ENCODE	ENCODE	kA
celkem	celkem	k6eAd1
nachází	nacházet	k5eAaImIp3nS
až	až	k9
95	#num#	k4
%	%	kIx~
lidského	lidský	k2eAgInSc2d1
genomu	genom	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Práce	práce	k1gFnSc1
s	s	k7c7
DNA	DNA	kA
</s>
<s>
Izolace	izolace	k1gFnSc1
a	a	k8xC
separace	separace	k1gFnSc1
</s>
<s>
Agarózový	Agarózový	k2eAgInSc1d1
gel	gel	k1gInSc1
po	po	k7c6
proběhlé	proběhlý	k2eAgFnSc6d1
DNA	DNA	kA
elektroforéze	elektroforéza	k1gFnSc6
<g/>
:	:	kIx,
pod	pod	k7c7
UV	UV	kA
lampou	lampa	k1gFnSc7
jsou	být	k5eAaImIp3nP
patrné	patrný	k2eAgFnPc1d1
fluoreskující	fluoreskující	k2eAgFnPc1d1
DNA	dna	k1gFnSc1
„	„	k?
<g/>
bandy	banda	k1gFnSc2
<g/>
“	“	k?
</s>
<s>
V	v	k7c6
celé	celý	k2eAgFnSc6d1
řadě	řada	k1gFnSc6
případů	případ	k1gInPc2
je	být	k5eAaImIp3nS
žádoucí	žádoucí	k2eAgMnSc1d1
izolovat	izolovat	k5eAaBmF
z	z	k7c2
buněk	buňka	k1gFnPc2
či	či	k8xC
z	z	k7c2
virových	virový	k2eAgFnPc2d1
partikulí	partikule	k1gFnPc2
jejich	jejich	k3xOp3gNnSc4
DNA	DNA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS
samozřejmě	samozřejmě	k6eAd1
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
metod	metoda	k1gFnPc2
extrakce	extrakce	k1gFnSc1
DNA	dna	k1gFnSc1
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
u	u	k7c2
všech	všecek	k3xTgFnPc2
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
získat	získat	k5eAaPmF
dostatečné	dostatečný	k2eAgNnSc4d1
množství	množství	k1gNnSc4
biologického	biologický	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
<g/>
,	,	kIx,
uvolnit	uvolnit	k5eAaPmF
DNA	dno	k1gNnPc4
a	a	k8xC
oddělit	oddělit	k5eAaPmF
ji	on	k3xPp3gFnSc4
z	z	k7c2
nadmolekulárních	nadmolekulární	k2eAgFnPc2d1
struktur	struktura	k1gFnPc2
<g/>
,	,	kIx,
načež	načež	k6eAd1
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
vzorek	vzorek	k1gInSc4
přečistit	přečistit	k5eAaPmF
a	a	k8xC
případně	případně	k6eAd1
zahustit	zahustit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
Důležitým	důležitý	k2eAgInSc7d1
krokem	krok	k1gInSc7
je	být	k5eAaImIp3nS
uvolnění	uvolnění	k1gNnSc1
DNA	dno	k1gNnSc2
z	z	k7c2
buněk	buňka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
u	u	k7c2
živočišných	živočišný	k2eAgFnPc2d1
buněk	buňka	k1gFnPc2
provádí	provádět	k5eAaImIp3nS
pomocí	pomocí	k7c2
detergentů	detergent	k1gInPc2
(	(	kIx(
<g/>
povrchově	povrchově	k6eAd1
aktivních	aktivní	k2eAgFnPc2d1
čisticích	čisticí	k2eAgFnPc2d1
látek	látka	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
rozrušují	rozrušovat	k5eAaImIp3nP
membrány	membrána	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
buněk	buňka	k1gFnPc2
s	s	k7c7
buněčnou	buněčný	k2eAgFnSc7d1
stěnou	stěna	k1gFnSc7
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
komplikovanější	komplikovaný	k2eAgFnSc1d2
a	a	k8xC
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
nasadit	nasadit	k5eAaPmF
třeba	třeba	k6eAd1
lysozymy	lysozymum	k1gNnPc7
(	(	kIx(
<g/>
na	na	k7c4
bakteriální	bakteriální	k2eAgFnSc4d1
buněčnou	buněčný	k2eAgFnSc4d1
stěnu	stěna	k1gFnSc4
<g/>
)	)	kIx)
či	či	k8xC
mechanickou	mechanický	k2eAgFnSc4d1
degradaci	degradace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
se	se	k3xPyFc4
týče	týkat	k5eAaImIp3nS
přečišťování	přečišťování	k1gNnSc1
buněčných	buněčný	k2eAgInPc2d1
extraktů	extrakt	k1gInPc2
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
se	se	k3xPyFc4
zbavit	zbavit	k5eAaPmF
bílkovin	bílkovina	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
představují	představovat	k5eAaImIp3nP
hlavní	hlavní	k2eAgFnSc4d1
kontaminaci	kontaminace	k1gFnSc4
vzorků	vzorek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
použít	použít	k5eAaPmF
proteázy	proteáza	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
mnohdy	mnohdy	k6eAd1
se	se	k3xPyFc4
proteiny	protein	k1gInPc1
sráží	srážet	k5eAaImIp3nP
fenolem	fenol	k1gInSc7
a	a	k8xC
chloroformem	chloroform	k1gInSc7
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
nukleové	nukleový	k2eAgFnSc2d1
kyseliny	kyselina	k1gFnSc2
zůstanou	zůstat	k5eAaPmIp3nP
v	v	k7c6
roztoku	roztok	k1gInSc6
a	a	k8xC
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
je	on	k3xPp3gInPc4
pak	pak	k6eAd1
vysrážet	vysrážet	k5eAaPmF
třeba	třeba	k6eAd1
ethanolem	ethanol	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
izolaci	izolace	k1gFnSc6
DNA	dno	k1gNnSc2
následuje	následovat	k5eAaImIp3nS
často	často	k6eAd1
separace	separace	k1gFnSc1
(	(	kIx(
<g/>
oddělení	oddělení	k1gNnSc1
<g/>
)	)	kIx)
požadovaných	požadovaný	k2eAgInPc2d1
druhů	druh	k1gInPc2
molekul	molekula	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Může	moct	k5eAaImIp3nS
být	být	k5eAaImF
žádoucí	žádoucí	k2eAgNnSc1d1
oddělení	oddělení	k1gNnSc1
třeba	třeba	k6eAd1
plazmidů	plazmid	k1gMnPc2
od	od	k7c2
genomové	genomový	k2eAgFnSc2d1
DNA	DNA	kA
bakterií	bakterie	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
dělá	dělat	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
jednoduše	jednoduše	k6eAd1
centrifugací	centrifugace	k1gFnPc2
při	při	k7c6
vhodně	vhodně	k6eAd1
nastavených	nastavený	k2eAgInPc6d1
parametrech	parametr	k1gInPc6
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
pomocí	pomocí	k7c2
denaturace	denaturace	k1gFnSc2
a	a	k8xC
následné	následný	k2eAgFnSc2d1
renaturace	renaturace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
Pro	pro	k7c4
jemnější	jemný	k2eAgNnSc4d2
rozdělování	rozdělování	k1gNnSc4
podle	podle	k7c2
velikosti	velikost	k1gFnSc2
i	i	k9
podle	podle	k7c2
topologie	topologie	k1gFnSc2
DNA	DNA	kA
se	se	k3xPyFc4
často	často	k6eAd1
používá	používat	k5eAaImIp3nS
elektroforéza	elektroforéza	k1gFnSc1
na	na	k7c6
agarózovém	agarózový	k2eAgMnSc6d1
(	(	kIx(
<g/>
či	či	k8xC
v	v	k7c6
případě	případ	k1gInSc6
velmi	velmi	k6eAd1
malých	malý	k2eAgFnPc2d1
molekul	molekula	k1gFnPc2
na	na	k7c6
polyakrylamidovém	polyakrylamidový	k2eAgInSc6d1
<g/>
)	)	kIx)
gelu	gel	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
extrémně	extrémně	k6eAd1
velkých	velký	k2eAgInPc2d1
fragmentů	fragment	k1gInPc2
DNA	DNA	kA
se	se	k3xPyFc4
užívá	užívat	k5eAaImIp3nS
tzv.	tzv.	kA
pulzní	pulzní	k2eAgFnSc1d1
gelová	gelový	k2eAgFnSc1d1
elektroforéza	elektroforéza	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
gelu	gel	k1gInSc2
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
následně	následně	k6eAd1
DNA	dno	k1gNnPc4
převést	převést	k5eAaPmF
na	na	k7c4
nitrocelulózovou	nitrocelulózový	k2eAgFnSc4d1
membránu	membrána	k1gFnSc4
pomocí	pomocí	k7c2
tzv.	tzv.	kA
Southernova	Southernov	k1gInSc2
přenosu	přenos	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
metodou	metoda	k1gFnSc7
dělení	dělení	k1gNnSc2
DNA	dno	k1gNnSc2
je	být	k5eAaImIp3nS
centrifugace	centrifugace	k1gFnSc1
v	v	k7c6
hustotním	hustotní	k2eAgInSc6d1
gradientu	gradient	k1gInSc6
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
v	v	k7c6
gradientu	gradient	k1gInSc6
chloridu	chlorid	k1gInSc2
cesného	cesný	k2eAgInSc2d1
–	–	k?
tato	tento	k3xDgFnSc1
metoda	metoda	k1gFnSc1
odděluje	oddělovat	k5eAaImIp3nS
zejména	zejména	k9
fragmenty	fragment	k1gInPc1
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nS
zastoupením	zastoupení	k1gNnSc7
bází	báze	k1gFnPc2
(	(	kIx(
<g/>
obsahem	obsah	k1gInSc7
GC	GC	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Barvení	barvení	k1gNnSc1
</s>
<s>
DAPI	DAPI	kA
<g/>
,	,	kIx,
jedna	jeden	k4xCgFnSc1
z	z	k7c2
chemikálií	chemikálie	k1gFnPc2
používaných	používaný	k2eAgInPc2d1
k	k	k7c3
barvení	barvení	k1gNnSc3
DNA	dno	k1gNnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
vmezeřuje	vmezeřovat	k5eAaImIp3nS
do	do	k7c2
malého	malý	k2eAgInSc2d1
žlábku	žlábek	k1gInSc2
a	a	k8xC
specificky	specificky	k6eAd1
tak	tak	k6eAd1
označuje	označovat	k5eAaImIp3nS
buněčnou	buněčný	k2eAgFnSc7d1
DNA	DNA	kA
<g/>
;	;	kIx,
na	na	k7c6
obrázku	obrázek	k1gInSc6
je	být	k5eAaImIp3nS
DAPI	DAPI	kA
fialově	fialově	k6eAd1
</s>
<s>
Byl	být	k5eAaImAgInS
vyvinut	vyvinout	k5eAaPmNgInS
nespočet	nespočet	k1gInSc1
způsobů	způsob	k1gInPc2
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
obarvit	obarvit	k5eAaPmF
DNA	dno	k1gNnPc4
–	–	k?
a	a	k8xC
to	ten	k3xDgNnSc1
jak	jak	k8xC,k8xS
přímo	přímo	k6eAd1
v	v	k7c6
buňce	buňka	k1gFnSc6
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k9
DNA	dna	k1gFnSc1
izolovanou	izolovaný	k2eAgFnSc4d1
v	v	k7c6
laboratorním	laboratorní	k2eAgNnSc6d1
skle	sklo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používají	používat	k5eAaImIp3nP
se	se	k3xPyFc4
často	často	k6eAd1
v	v	k7c6
laboratořích	laboratoř	k1gFnPc6
ve	v	k7c6
chvíli	chvíle	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgFnPc4d1
např.	např.	kA
v	v	k7c6
elektroforetickém	elektroforetický	k2eAgInSc6d1
gelu	gel	k1gInSc6
či	či	k8xC
přímo	přímo	k6eAd1
ve	v	k7c6
fixované	fixovaný	k2eAgFnSc6d1
buňce	buňka	k1gFnSc6
zvýraznit	zvýraznit	k5eAaPmF
DNA	DNA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
známým	známý	k2eAgMnPc3d1
takovým	takový	k3xDgNnPc3
barvivům	barvivo	k1gNnPc3
patří	patřit	k5eAaImIp3nS
(	(	kIx(
<g/>
bez	bez	k7c2
logické	logický	k2eAgFnSc2d1
následnosti	následnost	k1gFnSc2
<g/>
)	)	kIx)
<g/>
:	:	kIx,
SYBR	SYBR	kA
Green	Green	k1gInSc1
<g/>
,	,	kIx,
YOYO-	YOYO-	k1gFnSc1
<g/>
1	#num#	k4
<g/>
,	,	kIx,
TOTO-	TOTO-	k1gFnSc1
<g/>
1	#num#	k4
<g/>
,	,	kIx,
TO-PRO	TO-PRO	k1gFnSc1
<g/>
,	,	kIx,
SYTOX	SYTOX	kA
Green	Green	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
klasický	klasický	k2eAgInSc4d1
ethidiumbromid	ethidiumbromid	k1gInSc4
a	a	k8xC
propidiumjodid	propidiumjodid	k1gInSc4
<g/>
,	,	kIx,
akridinová	akridinový	k2eAgFnSc1d1
oranž	oranž	k1gFnSc1
<g/>
,	,	kIx,
různá	různý	k2eAgFnSc1d1
Hoechst	Hoechst	k1gFnSc1
barviva	barvivo	k1gNnSc2
či	či	k8xC
třeba	třeba	k9
DAPI	DAPI	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
velmi	velmi	k6eAd1
specifickým	specifický	k2eAgFnPc3d1
barvícím	barvící	k2eAgFnPc3d1
metodám	metoda	k1gFnPc3
patří	patřit	k5eAaImIp3nS
fluorescenční	fluorescenční	k2eAgFnSc1d1
in	in	k?
situ	siíst	k5eAaPmIp1nS
hybridizace	hybridizace	k1gFnSc1
(	(	kIx(
<g/>
FISH	FISH	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
navázání	navázání	k1gNnSc4
fluorescenčních	fluorescenční	k2eAgFnPc2d1
sond	sonda	k1gFnPc2
na	na	k7c4
konkrétní	konkrétní	k2eAgFnSc4d1
sekvenci	sekvence	k1gFnSc4
DNA	DNA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sekvenování	Sekvenování	k1gNnSc1
a	a	k8xC
umělá	umělý	k2eAgFnSc1d1
syntéza	syntéza	k1gFnSc1
</s>
<s>
Polymerázová	Polymerázový	k2eAgFnSc1d1
řetězová	řetězový	k2eAgFnSc1d1
reakce	reakce	k1gFnSc1
vyžaduje	vyžadovat	k5eAaImIp3nS
pro	pro	k7c4
správný	správný	k2eAgInSc4d1
průběh	průběh	k1gInSc4
směs	směs	k1gFnSc1
enzymů	enzym	k1gInPc2
<g/>
,	,	kIx,
substrátů	substrát	k1gInPc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
látek	látka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obvykle	obvykle	k6eAd1
se	se	k3xPyFc4
provádí	provádět	k5eAaImIp3nS
v	v	k7c6
Eppendorfových	Eppendorfův	k2eAgFnPc6d1
zkumavkách	zkumavka	k1gFnPc6
(	(	kIx(
<g/>
na	na	k7c6
obrázku	obrázek	k1gInSc6
každá	každý	k3xTgFnSc1
z	z	k7c2
nich	on	k3xPp3gFnPc2
obsahuje	obsahovat	k5eAaImIp3nS
100	#num#	k4
<g/>
mikrolitrovou	mikrolitrový	k2eAgFnSc4d1
reakci	reakce	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Sekvenování	Sekvenování	k1gNnSc1
DNA	dno	k1gNnSc2
je	být	k5eAaImIp3nS
souhrnný	souhrnný	k2eAgInSc1d1
termín	termín	k1gInSc1
pro	pro	k7c4
biochemické	biochemický	k2eAgFnPc4d1
metody	metoda	k1gFnPc4
<g/>
,	,	kIx,
jimiž	jenž	k3xRgFnPc7
se	se	k3xPyFc4
zjišťuje	zjišťovat	k5eAaImIp3nS
pořadí	pořadí	k1gNnSc1
nukleových	nukleový	k2eAgFnPc2d1
bází	báze	k1gFnPc2
v	v	k7c6
sekvencích	sekvence	k1gFnPc6
DNA	DNA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
Právě	právě	k6eAd1
pořadí	pořadí	k1gNnSc1
bází	báze	k1gFnPc2
je	být	k5eAaImIp3nS
princip	princip	k1gInSc1
zakódování	zakódování	k1gNnSc2
genetické	genetický	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
je	být	k5eAaImIp3nS
v	v	k7c6
centru	centrum	k1gNnSc6
zájmu	zájem	k1gInSc2
biologů	biolog	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgInPc1d1
a	a	k8xC
po	po	k7c4
dlouhá	dlouhý	k2eAgNnPc4d1
léta	léto	k1gNnPc4
převažující	převažující	k2eAgFnSc7d1
metodou	metoda	k1gFnSc7
bylo	být	k5eAaImAgNnS
tzv.	tzv.	kA
Sangerovo	Sangerův	k2eAgNnSc1d1
sekvenování	sekvenování	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
speciálně	speciálně	k6eAd1
chemicky	chemicky	k6eAd1
upravených	upravený	k2eAgInPc2d1
nukleotidů	nukleotid	k1gInPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
jsou	být	k5eAaImIp3nP
pomocí	pomocí	k7c2
DNA	dno	k1gNnSc2
polymerázy	polymeráza	k1gFnSc2
zařazovány	zařazován	k2eAgFnPc4d1
s	s	k7c7
určitou	určitý	k2eAgFnSc7d1
pravděpodobností	pravděpodobnost	k1gFnSc7
do	do	k7c2
prodlužující	prodlužující	k2eAgFnSc2d1
se	s	k7c7
DNA	DNA	kA
–	–	k?
tím	ten	k3xDgNnSc7
blokují	blokovat	k5eAaImIp3nP
další	další	k2eAgFnSc4d1
polymeraci	polymerace	k1gFnSc4
a	a	k8xC
výsledný	výsledný	k2eAgInSc1d1
produkt	produkt	k1gInSc1
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
detekovat	detekovat	k5eAaImF
pomocí	pomocí	k7c2
elektroforézy	elektroforéza	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
souvislosti	souvislost	k1gFnSc6
se	s	k7c7
snahou	snaha	k1gFnSc7
zrychlit	zrychlit	k5eAaPmF
a	a	k8xC
zlevnit	zlevnit	k5eAaPmF
sekvenovací	sekvenovací	k2eAgInSc4d1
proces	proces	k1gInSc4
byla	být	k5eAaImAgFnS
vyvinuta	vyvinout	k5eAaPmNgFnS
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
sekvenačních	sekvenační	k2eAgFnPc2d1
metod	metoda	k1gFnPc2
nové	nový	k2eAgFnSc2d1
generace	generace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
těm	ten	k3xDgInPc3
patří	patřit	k5eAaImIp3nS
např.	např.	kA
pyrosekvenování	pyrosekvenování	k1gNnSc1
a	a	k8xC
příbuzné	příbuzný	k2eAgFnPc1d1
metody	metoda	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studie	studie	k1gFnSc1
Zhang	Zhanga	k1gFnPc2
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
2011	#num#	k4
uvádí	uvádět	k5eAaImIp3nS
pět	pět	k4xCc4
moderních	moderní	k2eAgFnPc2d1
metod	metoda	k1gFnPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
jsou	být	k5eAaImIp3nP
komerčně	komerčně	k6eAd1
dostupné	dostupný	k2eAgInPc1d1
<g/>
:	:	kIx,
Roche	Roch	k1gInPc1
GS-FLX	GS-FLX	k1gFnSc1
454	#num#	k4
(	(	kIx(
<g/>
„	„	k?
<g/>
454	#num#	k4
sekvenování	sekvenování	k1gNnSc6
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Illumina	Illumin	k2eAgFnSc1d1
(	(	kIx(
<g/>
„	„	k?
<g/>
Solexa	Solexa	k1gMnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ABI	ABI	kA
SOLiD	solid	k1gInSc1
<g/>
,	,	kIx,
Polonator	Polonator	k1gInSc1
G	G	kA
<g/>
.007	.007	k4
a	a	k8xC
Helicos	Helicos	k1gMnSc1
HeliScope	HeliScop	k1gInSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Existuje	existovat	k5eAaImIp3nS
i	i	k9
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
postupů	postup	k1gInPc2
<g/>
,	,	kIx,
jak	jak	k6eAd1
si	se	k3xPyFc3
připravit	připravit	k5eAaPmF
či	či	k8xC
namnožit	namnožit	k5eAaPmF
konkrétní	konkrétní	k2eAgFnSc4d1
molekulu	molekula	k1gFnSc4
DNA	DNA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednou	jednou	k6eAd1
z	z	k7c2
možností	možnost	k1gFnPc2
je	být	k5eAaImIp3nS
chemická	chemický	k2eAgFnSc1d1
syntéza	syntéza	k1gFnSc1
DNA	dno	k1gNnSc2
<g/>
,	,	kIx,
při	při	k7c6
níž	jenž	k3xRgFnSc6
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
sestavování	sestavování	k1gNnSc3
krátkých	krátký	k2eAgInPc2d1
oligonukleotidů	oligonukleotid	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
postupným	postupný	k2eAgNnSc7d1
řazením	řazení	k1gNnSc7
nukleotidů	nukleotid	k1gInPc2
za	za	k7c7
sebou	se	k3xPyFc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
typickém	typický	k2eAgInSc6d1
případě	případ	k1gInSc6
však	však	k9
již	již	k6eAd1
je	být	k5eAaImIp3nS
určité	určitý	k2eAgNnSc4d1
množství	množství	k1gNnSc4
DNA	DNA	kA
k	k	k7c3
dispozici	dispozice	k1gFnSc3
a	a	k8xC
je	být	k5eAaImIp3nS
žádoucí	žádoucí	k2eAgNnSc1d1
ho	on	k3xPp3gNnSc4
pouze	pouze	k6eAd1
zmnožit	zmnožit	k5eAaPmF
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
všechny	všechen	k3xTgFnPc1
kopie	kopie	k1gFnPc1
měly	mít	k5eAaImAgFnP
pokud	pokud	k8xS
možno	možno	k6eAd1
totožnou	totožný	k2eAgFnSc4d1
sekvenci	sekvence	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
často	často	k6eAd1
dělá	dělat	k5eAaImIp3nS
buď	buď	k8xC
pomocí	pomocí	k7c2
klonování	klonování	k1gNnSc2
DNA	DNA	kA
nebo	nebo	k8xC
metodou	metoda	k1gFnSc7
polymerázové	polymerázový	k2eAgFnSc2d1
řetězové	řetězový	k2eAgFnSc2d1
reakce	reakce	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Význam	význam	k1gInSc1
ve	v	k7c6
společnosti	společnost	k1gFnSc6
</s>
<s>
Lékařská	lékařský	k2eAgFnSc1d1
diagnostika	diagnostika	k1gFnSc1
</s>
<s>
Vědecký	vědecký	k2eAgInSc1d1
pokrok	pokrok	k1gInSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
genetiky	genetika	k1gFnSc2
způsobil	způsobit	k5eAaPmAgMnS
boom	boom	k1gInSc4
v	v	k7c6
mnoha	mnoho	k4c6
oblastech	oblast	k1gFnPc6
lékařské	lékařský	k2eAgFnSc2d1
diagnostiky	diagnostika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
v	v	k7c6
bakteriologii	bakteriologie	k1gFnSc6
<g/>
,	,	kIx,
virologii	virologie	k1gFnSc6
a	a	k8xC
parazitologii	parazitologie	k1gFnSc6
se	se	k3xPyFc4
uplatnily	uplatnit	k5eAaPmAgFnP
metody	metoda	k1gFnPc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
umožňují	umožňovat	k5eAaImIp3nP
v	v	k7c6
napadené	napadený	k2eAgFnSc6d1
tkáni	tkáň	k1gFnSc6
detekovat	detekovat	k5eAaImF
DNA	dna	k1gFnSc1
pocházející	pocházející	k2eAgFnSc1d1
z	z	k7c2
mikroorganismů	mikroorganismus	k1gInPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
tuto	tento	k3xDgFnSc4
tkáň	tkáň	k1gFnSc4
napadly	napadnout	k5eAaPmAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
dělá	dělat	k5eAaImIp3nS
buď	buď	k8xC
pomocí	pomocí	k7c2
různých	různý	k2eAgNnPc2d1
DNA	dno	k1gNnSc2
prób	próba	k1gFnPc2
schopných	schopný	k2eAgFnPc2d1
se	se	k3xPyFc4
specificky	specificky	k6eAd1
vázat	vázat	k5eAaImF
na	na	k7c4
určitou	určitý	k2eAgFnSc4d1
sekvenci	sekvence	k1gFnSc4
typickou	typický	k2eAgFnSc4d1
pro	pro	k7c4
daného	daný	k2eAgMnSc4d1
parazita	parazit	k1gMnSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
např.	např.	kA
cestou	cesta	k1gFnSc7
namnožení	namnožení	k1gNnSc2
DNA	DNA	kA
pomocí	pomocí	k7c2
polymerázové	polymerázový	k2eAgFnSc2d1
řetězové	řetězový	k2eAgFnSc2d1
reakce	reakce	k1gFnSc2
a	a	k8xC
následným	následný	k2eAgNnSc7d1
sekvenováním	sekvenování	k1gNnSc7
–	–	k?
tím	ten	k3xDgNnSc7
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
získat	získat	k5eAaPmF
sekvenci	sekvence	k1gFnSc4
DNA	dno	k1gNnSc2
patogenních	patogenní	k2eAgInPc2d1
organismů	organismus	k1gInPc2
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc7
mikrobiologové	mikrobiolog	k1gMnPc1
srovnají	srovnat	k5eAaPmIp3nP
s	s	k7c7
databázemi	databáze	k1gFnPc7
patogenních	patogenní	k2eAgMnPc2d1
kmenů	kmen	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
pokročilé	pokročilý	k2eAgFnPc1d1
molekulární	molekulární	k2eAgFnPc1d1
metody	metoda	k1gFnPc1
se	se	k3xPyFc4
uplatňují	uplatňovat	k5eAaImIp3nP
např.	např.	kA
při	při	k7c6
identifikaci	identifikace	k1gFnSc6
těžko	těžko	k6eAd1
kultivovatelných	kultivovatelný	k2eAgFnPc2d1
bakterií	bakterie	k1gFnPc2
či	či	k8xC
při	při	k7c6
určování	určování	k1gNnSc6
celé	celý	k2eAgFnSc2d1
řady	řada	k1gFnSc2
virových	virový	k2eAgNnPc2d1
či	či	k8xC
parazitárních	parazitární	k2eAgNnPc2d1
onemocnění	onemocnění	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Součástí	součást	k1gFnSc7
diagnostické	diagnostický	k2eAgFnSc2d1
práce	práce	k1gFnSc2
je	být	k5eAaImIp3nS
však	však	k9
i	i	k9
studium	studium	k1gNnSc4
lidské	lidský	k2eAgFnSc2d1
DNA	DNA	kA
–	–	k?
uplatňuje	uplatňovat	k5eAaImIp3nS
se	se	k3xPyFc4
například	například	k6eAd1
v	v	k7c6
rakovinné	rakovinný	k2eAgFnSc6d1
terapii	terapie	k1gFnSc6
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
či	či	k8xC
při	při	k7c6
diagnostice	diagnostika	k1gFnSc6
některých	některý	k3yIgNnPc2
genetických	genetický	k2eAgNnPc2d1
onemocnění	onemocnění	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Své	svůj	k3xOyFgInPc4
místo	místo	k1gNnSc1
již	již	k6eAd1
molekulární	molekulární	k2eAgFnSc2d1
metody	metoda	k1gFnSc2
našly	najít	k5eAaPmAgFnP
v	v	k7c6
prenatální	prenatální	k2eAgFnSc6d1
diagnostice	diagnostika	k1gFnSc6
chorob	choroba	k1gFnPc2
<g/>
,	,	kIx,
např.	např.	kA
ze	z	k7c2
vzorku	vzorek	k1gInSc2
plodové	plodový	k2eAgFnSc2d1
vody	voda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
Další	další	k2eAgInPc1d1
testy	test	k1gInPc1
se	se	k3xPyFc4
rutinně	rutinně	k6eAd1
provádí	provádět	k5eAaImIp3nS
z	z	k7c2
kapky	kapka	k1gFnSc2
krve	krev	k1gFnSc2
novorozenců	novorozenec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Testy	testa	k1gFnSc2
DNA	DNA	kA
v	v	k7c6
rámci	rámec	k1gInSc6
genetického	genetický	k2eAgNnSc2d1
poradenství	poradenství	k1gNnSc2
však	však	k9
dnes	dnes	k6eAd1
mohou	moct	k5eAaImIp3nP
pomoci	pomoct	k5eAaPmF
i	i	k8xC
párům	pár	k1gInPc3
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
teprve	teprve	k6eAd1
dítě	dítě	k1gNnSc4
plánují	plánovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc4
vhodné	vhodný	k2eAgNnSc4d1
tehdy	tehdy	k6eAd1
<g/>
,	,	kIx,
vyskytuje	vyskytovat	k5eAaImIp3nS
<g/>
-li	-li	k?
se	se	k3xPyFc4
v	v	k7c6
rodinné	rodinný	k2eAgFnSc6d1
historii	historie	k1gFnSc6
nějaké	nějaký	k3yIgNnSc1
genetické	genetický	k2eAgNnSc1d1
onemocnění	onemocnění	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
jsou	být	k5eAaImIp3nP
genetické	genetický	k2eAgInPc1d1
testy	test	k1gInPc1
dostupné	dostupný	k2eAgInPc1d1
všem	všecek	k3xTgMnPc3
zájemcům	zájemce	k1gMnPc3
a	a	k8xC
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
o	o	k7c6
sobě	sebe	k3xPyFc6
zjistit	zjistit	k5eAaPmF
celou	celý	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
informací	informace	k1gFnPc2
od	od	k7c2
těch	ten	k3xDgInPc2
zřejmých	zřejmý	k2eAgInPc2d1
(	(	kIx(
<g/>
barva	barva	k1gFnSc1
očí	oko	k1gNnPc2
<g/>
)	)	kIx)
přes	přes	k7c4
různé	různý	k2eAgFnPc4d1
zajímavosti	zajímavost	k1gFnPc4
(	(	kIx(
<g/>
atletické	atletický	k2eAgFnPc4d1
vlohy	vloha	k1gFnPc4
<g/>
)	)	kIx)
až	až	k9
po	po	k7c4
vážné	vážný	k2eAgInPc4d1
údaje	údaj	k1gInPc4
(	(	kIx(
<g/>
náchylnost	náchylnost	k1gFnSc1
k	k	k7c3
rakovině	rakovina	k1gFnSc3
atp.	atp.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Genetická	genetický	k2eAgFnSc1d1
daktyloskopie	daktyloskopie	k1gFnSc1
</s>
<s>
Rozdíly	rozdíl	k1gInPc1
mezi	mezi	k7c7
6	#num#	k4
jedinci	jedinec	k1gMnPc7
prokázané	prokázaný	k2eAgFnSc2d1
analýzou	analýza	k1gFnSc7
jednoho	jeden	k4xCgInSc2
z	z	k7c2
VNTR	VNTR	kA
markerů	marker	k1gInPc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
genetická	genetický	k2eAgFnSc1d1
daktyloskopie	daktyloskopie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Některé	některý	k3yIgFnPc1
oblasti	oblast	k1gFnPc1
např.	např.	kA
lidské	lidský	k2eAgFnSc2d1
jaderné	jaderný	k2eAgFnSc2d1
DNA	DNA	kA
jsou	být	k5eAaImIp3nP
velmi	velmi	k6eAd1
proměnlivé	proměnlivý	k2eAgNnSc1d1
a	a	k8xC
člověk	člověk	k1gMnSc1
od	od	k7c2
člověka	člověk	k1gMnSc2
se	se	k3xPyFc4
v	v	k7c6
nich	on	k3xPp3gInPc6
téměř	téměř	k6eAd1
vždy	vždy	k6eAd1
liší	lišit	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgInSc2
důvodu	důvod	k1gInSc2
je	být	k5eAaImIp3nS
DNA	DNA	kA
v	v	k7c6
kriminalistice	kriminalistika	k1gFnSc6
a	a	k8xC
v	v	k7c6
forenzních	forenzní	k2eAgFnPc6d1
vědách	věda	k1gFnPc6
neocenitelným	ocenitelný	k2eNgInSc7d1
zdrojem	zdroj	k1gInSc7
informací	informace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Repetitivní	repetitivní	k2eAgFnSc1d1
sekvence	sekvence	k1gFnSc1
známé	známý	k2eAgFnSc2d1
jako	jako	k8xC,k8xS
VNTR	VNTR	kA
či	či	k8xC
STR	str	kA
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
ty	ten	k3xDgInPc4
nejčastěji	často	k6eAd3
studované	studovaný	k2eAgInPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studium	studium	k1gNnSc1
VNTR	VNTR	kA
repetic	repetice	k1gFnPc2
vyžaduje	vyžadovat	k5eAaImIp3nS
relativně	relativně	k6eAd1
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
DNA	DNA	kA
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
se	se	k3xPyFc4
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
zejména	zejména	k9
tehdy	tehdy	k6eAd1
<g/>
,	,	kIx,
máme	mít	k5eAaImIp1nP
<g/>
-li	-li	k?
k	k	k7c3
dispozici	dispozice	k1gFnSc3
vzorek	vzorek	k1gInSc4
krve	krev	k1gFnSc2
(	(	kIx(
<g/>
např.	např.	kA
u	u	k7c2
testů	test	k1gInPc2
otcovství	otcovství	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obvykle	obvykle	k6eAd1
se	se	k3xPyFc4
testují	testovat	k5eAaImIp3nP
metodou	metoda	k1gFnSc7
RFLP	RFLP	kA
(	(	kIx(
<g/>
jenž	jenž	k3xRgMnSc1
zkoumá	zkoumat	k5eAaImIp3nS
polymorfismus	polymorfismus	k1gInSc4
délky	délka	k1gFnSc2
restrikčních	restrikční	k2eAgInPc2d1
fragmentů	fragment	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
kriminalistice	kriminalistika	k1gFnSc6
našly	najít	k5eAaPmAgFnP
větší	veliký	k2eAgNnSc4d2
využití	využití	k1gNnSc4
tzv.	tzv.	kA
STR	str	kA
(	(	kIx(
<g/>
čili	čili	k8xC
~	~	kIx~
<g/>
mikrosatelity	mikrosatelita	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravděpodobnost	pravděpodobnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
dvě	dva	k4xCgFnPc1
osoby	osoba	k1gFnPc1
budou	být	k5eAaImBp3nP
mít	mít	k5eAaImF
jednu	jeden	k4xCgFnSc4
STR	str	kA
oblast	oblast	k1gFnSc4
shodnou	shodný	k2eAgFnSc7d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
pro	pro	k7c4
danou	daný	k2eAgFnSc4d1
variantu	varianta	k1gFnSc4
např.	např.	kA
1	#num#	k4
:	:	kIx,
83	#num#	k4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
by	by	kYmCp3nS
nebylo	být	k5eNaImAgNnS
příliš	příliš	k6eAd1
přesvědčivé	přesvědčivý	k2eAgNnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
13	#num#	k4
markerů	marker	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
se	se	k3xPyFc4
vyhodnocují	vyhodnocovat	k5eAaImIp3nP
zvlášť	zvlášť	k6eAd1
a	a	k8xC
vzájemný	vzájemný	k2eAgInSc1d1
pozitivní	pozitivní	k2eAgInSc1d1
výsledek	výsledek	k1gInSc1
důvěryhodnost	důvěryhodnost	k1gFnSc1
testu	test	k1gInSc2
mnohonásobně	mnohonásobně	k6eAd1
zvyšuje	zvyšovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgNnSc1
použití	použití	k1gNnSc1
DNA	DNA	kA
v	v	k7c6
kriminalistice	kriminalistika	k1gFnSc6
se	se	k3xPyFc4
datuje	datovat	k5eAaImIp3nS
do	do	k7c2
roku	rok	k1gInSc2
1986	#num#	k4
a	a	k8xC
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
němu	on	k3xPp3gNnSc3
v	v	k7c6
rámci	rámec	k1gInSc6
soudního	soudní	k2eAgNnSc2d1
řízení	řízení	k1gNnSc2
v	v	k7c6
Anglii	Anglie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Testování	testování	k1gNnPc1
STR	str	kA
oblastí	oblast	k1gFnPc2
se	se	k3xPyFc4
však	však	k9
dnes	dnes	k6eAd1
prosazuje	prosazovat	k5eAaImIp3nS
i	i	k9
v	v	k7c6
určování	určování	k1gNnSc6
otcovství	otcovství	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Genetická	genetický	k2eAgFnSc1d1
manipulace	manipulace	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článcích	článek	k1gInPc6
genetické	genetický	k2eAgFnSc2d1
inženýrství	inženýrství	k1gNnSc3
a	a	k8xC
geneticky	geneticky	k6eAd1
modifikovaný	modifikovaný	k2eAgInSc4d1
organismus	organismus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
současnosti	současnost	k1gFnSc6
je	být	k5eAaImIp3nS
lidstvo	lidstvo	k1gNnSc1
schopné	schopný	k2eAgNnSc1d1
provádět	provádět	k5eAaImF
cílené	cílený	k2eAgFnPc4d1
změny	změna	k1gFnPc4
v	v	k7c6
genetické	genetický	k2eAgFnSc6d1
informaci	informace	k1gFnSc6
(	(	kIx(
<g/>
v	v	k7c6
pořadí	pořadí	k1gNnSc6
nukleotidů	nukleotid	k1gInPc2
v	v	k7c6
DNA	DNA	kA
<g/>
)	)	kIx)
a	a	k8xC
ovlivňovat	ovlivňovat	k5eAaImF
tím	ten	k3xDgNnSc7
některé	některý	k3yIgFnPc1
vlastnosti	vlastnost	k1gFnPc1
organismů	organismus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
tzv.	tzv.	kA
genetické	genetický	k2eAgFnPc4d1
modifikace	modifikace	k1gFnPc4
způsobily	způsobit	k5eAaPmAgInP
revoluci	revoluce	k1gFnSc4
v	v	k7c6
celé	celý	k2eAgFnSc6d1
řadě	řada	k1gFnSc6
biotechnologických	biotechnologický	k2eAgNnPc2d1
odvětví	odvětví	k1gNnPc2
a	a	k8xC
umožňují	umožňovat	k5eAaImIp3nP
např.	např.	kA
průmyslovou	průmyslový	k2eAgFnSc4d1
produkci	produkce	k1gFnSc4
hormonů	hormon	k1gInPc2
<g/>
,	,	kIx,
srážecích	srážecí	k2eAgInPc2d1
faktorů	faktor	k1gInPc2
pro	pro	k7c4
hemofiliky	hemofilik	k1gMnPc4
<g/>
,	,	kIx,
enzymů	enzym	k1gInPc2
užívaných	užívaný	k2eAgInPc2d1
v	v	k7c6
potravinářství	potravinářství	k1gNnSc6
a	a	k8xC
některých	některý	k3yIgFnPc2
vakcín	vakcína	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
genetického	genetický	k2eAgNnSc2d1
inženýrství	inženýrství	k1gNnSc2
jsou	být	k5eAaImIp3nP
i	i	k9
různé	různý	k2eAgFnPc1d1
transgenní	transgenní	k2eAgFnPc1d1
plodiny	plodina	k1gFnPc1
<g/>
,	,	kIx,
např.	např.	kA
ty	ty	k3xPp2nSc5
odolné	odolný	k2eAgFnPc1d1
k	k	k7c3
herbicidům	herbicid	k1gInPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Evropské	evropský	k2eAgFnSc6d1
unii	unie	k1gFnSc6
je	být	k5eAaImIp3nS
z	z	k7c2
geneticky	geneticky	k6eAd1
modifikovaných	modifikovaný	k2eAgFnPc2d1
plodin	plodina	k1gFnPc2
povolena	povolit	k5eAaPmNgFnS
pouze	pouze	k6eAd1
Bt	Bt	k1gFnSc1
kukuřice	kukuřice	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
nese	nést	k5eAaImIp3nS
gen	gen	k1gInSc4
cry	cry	k?
pocházející	pocházející	k2eAgFnSc2d1
z	z	k7c2
půdní	půdní	k2eAgFnSc2d1
bakterie	bakterie	k1gFnSc2
Bacillus	Bacillus	k1gMnSc1
thuringiensis	thuringiensis	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
gen	gen	k1gInSc1
způsobuje	způsobovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
rostlina	rostlina	k1gFnSc1
pro	pro	k7c4
své	svůj	k3xOyFgMnPc4
hmyzí	hmyzí	k2eAgMnPc4d1
škůdce	škůdce	k1gMnPc4
jedovatá	jedovatý	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Biologická	biologický	k2eAgFnSc1d1
systematika	systematika	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článcích	článek	k1gInPc6
biologická	biologický	k2eAgFnSc1d1
systematika	systematika	k1gFnSc1
a	a	k8xC
fylogenetika	fylogenetika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
neposlední	poslední	k2eNgFnSc6d1
řadě	řada	k1gFnSc6
se	se	k3xPyFc4
studium	studium	k1gNnSc1
sekvencí	sekvence	k1gFnPc2
DNA	DNA	kA
uplatňuje	uplatňovat	k5eAaImIp3nS
v	v	k7c6
třídění	třídění	k1gNnSc6
organismů	organismus	k1gInPc2
podle	podle	k7c2
jejich	jejich	k3xOp3gFnSc2
příbuznosti	příbuznost	k1gFnSc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
v	v	k7c6
oboru	obor	k1gInSc6
biologie	biologie	k1gFnSc2
známém	známý	k2eAgNnSc6d1
jako	jako	k9
fylogenetika	fylogenetik	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgInSc7
z	z	k7c2
prvních	první	k4xOgInPc2
krůčků	krůček	k1gInPc2
v	v	k7c6
tomto	tento	k3xDgInSc6
oboru	obor	k1gInSc6
byla	být	k5eAaImAgNnP
v	v	k7c6
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
studie	studie	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
srovnávala	srovnávat	k5eAaImAgFnS
sekvenci	sekvence	k1gFnSc4
genu	gen	k1gInSc2
pro	pro	k7c4
cytochrom	cytochrom	k1gInSc4
c	c	k0
u	u	k7c2
různých	různý	k2eAgInPc2d1
organismů	organismus	k1gInPc2
<g/>
:	:	kIx,
výsledky	výsledek	k1gInPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
podstatě	podstata	k1gFnSc6
intuitivní	intuitivní	k2eAgNnSc1d1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
šimpanz	šimpanz	k1gMnSc1
má	mít	k5eAaImIp3nS
sekvenci	sekvence	k1gFnSc4
tohoto	tento	k3xDgInSc2
genu	gen	k1gInSc2
s	s	k7c7
člověkem	člověk	k1gMnSc7
zcela	zcela	k6eAd1
shodnou	shodnout	k5eAaPmIp3nP,k5eAaBmIp3nP
a	a	k8xC
makak	makak	k1gMnSc1
rhesus	rhesus	k1gMnSc1
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nS
pouze	pouze	k6eAd1
jedinou	jediný	k2eAgFnSc7d1
nukleotidovou	nukleotidový	k2eAgFnSc7d1
záměnou	záměna	k1gFnSc7
<g/>
,	,	kIx,
psí	psí	k2eAgInSc4d1
gen	gen	k1gInSc4
pro	pro	k7c4
cytochrom	cytochrom	k1gInSc4
už	už	k6eAd1
se	se	k3xPyFc4
od	od	k7c2
lidského	lidský	k2eAgInSc2d1
genu	gen	k1gInSc2
liší	lišit	k5eAaImIp3nS
na	na	k7c6
13	#num#	k4
místech	místo	k1gNnPc6
a	a	k8xC
kvasinkový	kvasinkový	k2eAgInSc4d1
gen	gen	k1gInSc4
dokonce	dokonce	k9
na	na	k7c6
56	#num#	k4
pozicích	pozice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základě	základ	k1gInSc6
těchto	tento	k3xDgFnPc2
informací	informace	k1gFnPc2
si	se	k3xPyFc3
lze	lze	k6eAd1
udělat	udělat	k5eAaPmF
obrázek	obrázek	k1gInSc4
o	o	k7c6
příbuzenských	příbuzenský	k2eAgInPc6d1
vztazích	vztah	k1gInPc6
mezi	mezi	k7c7
organismy	organismus	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
rozmachem	rozmach	k1gInSc7
sekvenování	sekvenování	k1gNnSc2
je	být	k5eAaImIp3nS
dnes	dnes	k6eAd1
k	k	k7c3
dispozici	dispozice	k1gFnSc3
obrovské	obrovský	k2eAgNnSc1d1
množství	množství	k1gNnSc1
sekvencí	sekvence	k1gFnPc2
DNA	dno	k1gNnSc2
celé	celý	k2eAgFnSc2d1
řady	řada	k1gFnSc2
organismů	organismus	k1gInPc2
a	a	k8xC
k	k	k7c3
jejich	jejich	k3xOp3gFnSc3
analýze	analýza	k1gFnSc3
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
různé	různý	k2eAgInPc1d1
sofistikované	sofistikovaný	k2eAgInPc1d1
nástroje	nástroj	k1gInPc1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
například	například	k6eAd1
metoda	metoda	k1gFnSc1
parsimonie	parsimonie	k1gFnSc1
nebo	nebo	k8xC
metoda	metoda	k1gFnSc1
maximální	maximální	k2eAgFnSc2d1
pravděpodobnosti	pravděpodobnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
i	i	k9
odhadnout	odhadnout	k5eAaPmF
čas	čas	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
dělí	dělit	k5eAaImIp3nS
v	v	k7c6
evoluční	evoluční	k2eAgFnSc6d1
historii	historie	k1gFnSc6
libovolné	libovolný	k2eAgInPc4d1
dva	dva	k4xCgInPc4
druhy	druh	k1gInPc4
–	–	k?
metoda	metoda	k1gFnSc1
k	k	k7c3
tomu	ten	k3xDgNnSc3
užívaná	užívaný	k2eAgFnSc1d1
opět	opět	k6eAd1
pracuje	pracovat	k5eAaImIp3nS
se	s	k7c7
sekvencemi	sekvence	k1gFnPc7
DNA	DNA	kA
a	a	k8xC
označuje	označovat	k5eAaImIp3nS
se	se	k3xPyFc4
jako	jako	k8xS,k8xC
molekulární	molekulární	k2eAgFnPc1d1
hodiny	hodina	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomocí	pomocí	k7c2
fylogenetických	fylogenetický	k2eAgInPc2d1
přístupů	přístup	k1gInPc2
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
odpovídat	odpovídat	k5eAaImF
na	na	k7c4
celou	celý	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
dalších	další	k2eAgFnPc2d1
otázek	otázka	k1gFnPc2
<g/>
,	,	kIx,
namátkou	namátkou	k6eAd1
„	„	k?
<g/>
jaký	jaký	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
vztah	vztah	k1gInSc4
mají	mít	k5eAaImIp3nP
neandertálci	neandertálec	k1gMnPc1
k	k	k7c3
dnešním	dnešní	k2eAgMnPc3d1
lidem	člověk	k1gMnPc3
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
jak	jak	k6eAd1
se	se	k3xPyFc4
mezi	mezi	k7c7
jednotlivými	jednotlivý	k2eAgMnPc7d1
nemocnými	nemocný	k1gMnPc7
šíří	šířit	k5eAaImIp3nS
virus	virus	k1gInSc1
HIV	HIV	kA
<g/>
“	“	k?
a	a	k8xC
podobně	podobně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pravěká	pravěký	k2eAgFnSc1d1
DNA	dna	k1gFnSc1
</s>
<s>
Od	od	k7c2
50	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
objevují	objevovat	k5eAaImIp3nP
studie	studie	k1gFnPc1
o	o	k7c6
údajném	údajný	k2eAgNnSc6d1
izolování	izolování	k1gNnSc6
sekvencí	sekvence	k1gFnPc2
DNA	dno	k1gNnSc2
pravěkých	pravěký	k2eAgInPc2d1
organismů	organismus	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
80	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
přinesl	přinést	k5eAaPmAgMnS
tomuto	tento	k3xDgInSc3
oboru	obor	k1gInSc3
(	(	kIx(
<g/>
„	„	k?
<g/>
paleogenetika	paleogenetika	k1gFnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
slávu	sláva	k1gFnSc4
Crichtonův	Crichtonův	k2eAgInSc1d1
román	román	k1gInSc1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
Spielbergova	Spielbergův	k2eAgFnSc1d1
filmová	filmový	k2eAgFnSc1d1
adaptace	adaptace	k1gFnSc1
Jurský	jurský	k2eAgInSc1d1
park	park	k1gInSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgNnSc6,k3yQgNnSc6,k3yRgNnSc6
byli	být	k5eAaImAgMnP
naklonováni	naklonovat	k5eAaPmNgMnP
druhohorní	druhohorní	k2eAgMnPc1d1
(	(	kIx(
<g/>
nejen	nejen	k6eAd1
ti	ten	k3xDgMnPc1
z	z	k7c2
jury	jura	k1gFnSc2
<g/>
)	)	kIx)
dinosauři	dinosaurus	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dinosauří	dinosauří	k2eAgInPc1d1
proteiny	protein	k1gInPc1
a	a	k8xC
měkké	měkký	k2eAgFnPc1d1
tkáně	tkáň	k1gFnPc1
byly	být	k5eAaImAgFnP
skutečně	skutečně	k6eAd1
získány	získat	k5eAaPmNgFnP
např.	např.	kA
Mary	Mary	k1gFnSc2
H.	H.	kA
Schweitzerovou	Schweitzerová	k1gFnSc7
a	a	k8xC
jejím	její	k3xOp3gInSc7
týmem	tým	k1gInSc7
<g/>
,	,	kIx,
pravěká	pravěký	k2eAgFnSc1d1
DNA	dna	k1gFnSc1
dinosaurů	dinosaurus	k1gMnPc2
ale	ale	k8xC
nejspíš	nejspíš	k9
nikdy	nikdy	k6eAd1
získána	získán	k2eAgFnSc1d1
nebude	být	k5eNaImBp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
Nepotvrdily	potvrdit	k5eNaPmAgInP
se	se	k3xPyFc4
ani	ani	k9
domněnky	domněnka	k1gFnPc1
o	o	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
získání	získání	k1gNnSc3
DNA	dno	k1gNnSc2
ze	z	k7c2
120	#num#	k4
<g/>
–	–	k?
<g/>
135	#num#	k4
milionů	milion	k4xCgInPc2
let	léto	k1gNnPc2
starého	starý	k2eAgInSc2d1
libanonského	libanonský	k2eAgInSc2d1
jantaru	jantar	k1gInSc2
entomologem	entomolog	k1gMnSc7
Georgem	Georg	k1gMnSc7
Poinarem	Poinar	k1gMnSc7
v	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
Lze	lze	k6eAd1
ovšem	ovšem	k9
získat	získat	k5eAaPmF
DNA	dna	k1gFnSc1
starou	stará	k1gFnSc4
více	hodně	k6eAd2
než	než	k8xS
1	#num#	k4
milión	milión	k4xCgInSc4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Některé	některý	k3yIgInPc4
paleontologické	paleontologický	k2eAgInPc4d1
objevy	objev	k1gInPc4
nicméně	nicméně	k8xC
naznačují	naznačovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
alespoň	alespoň	k9
stopy	stopa	k1gFnPc4
po	po	k7c4
původní	původní	k2eAgNnPc4d1
DNA	dno	k1gNnPc4
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
v	v	k7c6
dinosauřích	dinosauří	k2eAgFnPc6d1
fosiliích	fosilie	k1gFnPc6
z	z	k7c2
období	období	k1gNnSc2
druhohor	druhohory	k1gFnPc2
skutečně	skutečně	k6eAd1
objeveny	objevit	k5eAaPmNgFnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
se	se	k3xPyFc4
jich	on	k3xPp3gFnPc2
v	v	k7c6
DNA	DNA	kA
přirozeně	přirozeně	k6eAd1
vyskytuje	vyskytovat	k5eAaImIp3nS
8	#num#	k4
<g/>
,	,	kIx,
neboť	neboť	k8xC
kvůli	kvůli	k7c3
zamezení	zamezení	k1gNnSc3
genové	genový	k2eAgFnSc2d1
exprese	exprese	k1gFnSc2
jsou	být	k5eAaImIp3nP
některé	některý	k3yIgFnPc1
cytosinové	cytosinový	k2eAgFnPc1d1
báze	báze	k1gFnPc1
methylovány	methylovat	k5eAaBmNgFnP,k5eAaPmNgFnP,k5eAaImNgFnP
a	a	k8xC
cytosin	cytosin	k1gInSc1
je	být	k5eAaImIp3nS
nahrazen	nahradit	k5eAaPmNgInS
5	#num#	k4
<g/>
-methylcytosinem	-methylcytosino	k1gNnSc7
či	či	k8xC
5	#num#	k4
<g/>
-hydroxymethylcytosinem	-hydroxymethylcytosin	k1gInSc7
<g/>
,	,	kIx,
naopak	naopak	k6eAd1
při	při	k7c6
demethylaci	demethylace	k1gFnSc6
vznikají	vznikat	k5eAaImIp3nP
ještě	ještě	k9
5	#num#	k4
<g/>
-formylcytosin	-formylcytosina	k1gFnPc2
a	a	k8xC
5	#num#	k4
<g/>
-karboxylcytosin	-karboxylcytosina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Methylace	Methylace	k1gFnSc1
bází	báze	k1gFnPc2
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgInSc7
z	z	k7c2
klíčových	klíčový	k2eAgInPc2d1
epigenetických	epigenetický	k2eAgInPc2d1
mechanismů	mechanismus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
DNA	dno	k1gNnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
SEEMAN	SEEMAN	kA
<g/>
,	,	kIx,
N.	N.	kA
C.	C.	kA
DNA	DNA	kA
nanotechnology	nanotechnolog	k1gMnPc4
<g/>
:	:	kIx,
novel	novela	k1gFnPc2
DNA	dno	k1gNnSc2
constructions	constructionsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annu	Anna	k1gFnSc4
Rev	Rev	k1gFnSc2
Biophys	Biophysa	k1gFnPc2
Biomol	Biomol	k1gInSc1
Struct	Struct	k1gMnSc1
<g/>
.	.	kIx.
1998	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
27	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
225	#num#	k4
<g/>
–	–	k?
<g/>
48	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
PDF	PDF	kA
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1056	#num#	k4
<g/>
-	-	kIx~
<g/>
8700	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Dahm	Dahm	k1gInSc1
R.	R.	kA
Discovering	Discovering	k1gInSc1
DNA	DNA	kA
<g/>
:	:	kIx,
Friedrich	Friedrich	k1gMnSc1
Miescher	Mieschra	k1gFnPc2
and	and	k?
the	the	k?
early	earl	k1gMnPc4
years	yearsa	k1gFnPc2
of	of	k?
nucleic	nucleic	k1gMnSc1
acid	acid	k1gMnSc1
research	research	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hum	hum	k0wR
<g/>
.	.	kIx.
</s>
<s desamb="1">
Genet	Geneta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
January	Januara	k1gFnSc2
2008	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
122	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
6	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
565	#num#	k4
<g/>
–	–	k?
<g/>
81	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
439	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
433	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
17901982	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Levene	Leven	k1gMnSc5
<g/>
,	,	kIx,
P.	P.	kA
The	The	k1gFnSc1
structure	structur	k1gMnSc5
of	of	k?
yeast	yeast	k1gMnSc1
nucleic	nucleic	k1gMnSc1
acid	acid	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
J	J	kA
Biol	Biol	k1gInSc1
Chem.	Chem.	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
1919	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
40	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
415	#num#	k4
<g/>
–	–	k?
<g/>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Avery	Avera	k1gFnSc2
O.	O.	kA
<g/>
,	,	kIx,
MacLeod	MacLeod	k1gInSc1
C.	C.	kA
<g/>
,	,	kIx,
McCarty	McCarta	k1gFnSc2
M.	M.	kA
Studies	Studies	k1gMnSc1
on	on	k3xPp3gMnSc1
the	the	k?
chemical	chemicat	k5eAaPmAgInS
nature	natur	k1gMnSc5
of	of	k?
the	the	k?
substance	substance	k1gFnSc1
inducing	inducing	k1gInSc1
transformation	transformation	k1gInSc1
of	of	k?
pneumococcal	pneumococcat	k5eAaPmAgInS
types	types	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inductions	Inductions	k1gInSc1
of	of	k?
transformation	transformation	k1gInSc1
by	by	kYmCp3nS
a	a	k9
desoxyribonucleic	desoxyribonucleic	k1gMnSc1
acid	acida	k1gFnPc2
fraction	fraction	k1gInSc1
isolated	isolated	k1gMnSc1
from	from	k1gMnSc1
pneumococcus	pneumococcus	k1gMnSc1
type	typ	k1gInSc5
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
J	J	kA
Exp	exp	kA
Med	med	k1gInSc1
<g/>
.	.	kIx.
1944	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
79	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
137	#num#	k4
<g/>
–	–	k?
<g/>
158	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.108	10.108	k4
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
jem	jem	k?
<g/>
.79	.79	k4
<g/>
.2	.2	k4
<g/>
.137	.137	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
19871359	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Watson	Watson	k1gMnSc1
J.D.	J.D.	k1gMnSc1
and	and	k?
Crick	Crick	k1gMnSc1
F.	F.	kA
<g/>
H.C.	H.C.	k1gMnSc1
A	a	k8xC
Structure	Structur	k1gMnSc5
for	forum	k1gNnPc2
Deoxyribose	Deoxyribosa	k1gFnSc6
Nucleic	Nucleic	k1gMnSc1
Acid	Acid	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
<g/>
.	.	kIx.
1953	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
171	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
4356	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
737	#num#	k4
<g/>
–	–	k?
<g/>
738	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
PDF	PDF	kA
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
171737	#num#	k4
<g/>
a	a	k8xC
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
13054692	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Wilkins	Wilkins	k1gInSc1
<g/>
,	,	kIx,
M.	M.	kA
<g/>
H.F	H.F	k1gFnSc1
<g/>
;	;	kIx,
A.	A.	kA
<g/>
R.	R.	kA
Stokes	Stokes	k1gMnSc1
<g/>
;	;	kIx,
Wilson	Wilson	k1gMnSc1
<g/>
,	,	kIx,
H.	H.	kA
<g/>
R.	R.	kA
Molecular	Molecular	k1gMnSc1
Structure	Structur	k1gMnSc5
of	of	k?
Deoxypentose	Deoxypentosa	k1gFnSc3
Nucleic	Nucleic	k1gMnSc1
Acids	Acids	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
<g/>
.	.	kIx.
1953	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
171	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
4356	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
738	#num#	k4
<g/>
–	–	k?
<g/>
740	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
PDF	PDF	kA
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
171738	#num#	k4
<g/>
a	a	k8xC
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
13054693	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Crick	Crick	k1gInSc1
<g/>
,	,	kIx,
F.	F.	kA
<g/>
H.C.	H.C.	k1gFnSc1
On	on	k3xPp3gMnSc1
degenerate	degenerat	k1gInSc5
templates	templatesa	k1gFnPc2
and	and	k?
the	the	k?
adaptor	adaptor	k1gInSc1
hypothesis	hypothesis	k1gFnSc1
(	(	kIx(
<g/>
PDF	PDF	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivováno	archivován	k2eAgNnSc4d1
1	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
genome	genom	k1gInSc5
<g/>
.	.	kIx.
<g/>
wellcome	wellcom	k1gInSc5
<g/>
.	.	kIx.
<g/>
ac	ac	k?
<g/>
.	.	kIx.
<g/>
uk	uk	k?
(	(	kIx(
<g/>
Lecture	Lectur	k1gMnSc5
<g/>
,	,	kIx,
1955	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Accessed	Accessed	k1gInSc1
22	#num#	k4
December	Decembra	k1gFnPc2
2006	#num#	k4
<g/>
↑	↑	k?
CAMMACK	CAMMACK	kA
<g/>
,	,	kIx,
R.	R.	kA
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oxford	Oxford	k1gInSc1
dictionary	dictionara	k1gFnSc2
of	of	k?
biochemistry	biochemistr	k1gMnPc4
and	and	k?
molecular	molecular	k1gInSc1
biology	biolog	k1gMnPc4
<g/>
;	;	kIx,
revised	revised	k1gInSc1
edition	edition	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Oxford	Oxford	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
-	-	kIx~
<g/>
852917	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
415	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
The	The	k1gMnSc4
Nobel	Nobel	k1gMnSc1
Prize	Prize	k1gFnSc2
in	in	k?
Physiology	Physiolog	k1gMnPc4
or	or	k?
Medicine	Medicin	k1gInSc5
1968	#num#	k4
Nobelprize	Nobelprize	k1gFnPc1
<g/>
.	.	kIx.
<g/>
org	org	k?
Accessed	Accessed	k1gMnSc1
22	#num#	k4
December	December	k1gInSc1
0	#num#	k4
<g/>
6	#num#	k4
<g/>
↑	↑	k?
FLEGR	FLEGR	kA
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evoluční	evoluční	k2eAgFnSc1d1
biologie	biologie	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
rozšířené	rozšířený	k2eAgFnPc4d1
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
1767	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Kulturní	kulturní	k2eAgFnSc2d1
evoluce	evoluce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
320	#num#	k4
<g/>
–	–	k?
<g/>
329	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
VREELAND	VREELAND	kA
<g/>
,	,	kIx,
R.	R.	kA
H	H	kA
<g/>
;	;	kIx,
ROSENZWEIG	ROSENZWEIG	kA
<g/>
,	,	kIx,
W.	W.	kA
D	D	kA
<g/>
;	;	kIx,
POWERS	POWERS	kA
<g/>
,	,	kIx,
D.	D.	kA
W.	W.	kA
Isolation	Isolation	k1gInSc1
of	of	k?
a	a	k8xC
250	#num#	k4
million-year-old	million-year-old	k1gMnSc1
halotolerant	halotolerant	k1gMnSc1
bacterium	bacterium	k1gNnSc4
from	froma	k1gFnPc2
a	a	k8xC
primary	primara	k1gFnSc2
salt	salto	k1gNnPc2
crystal	crystat	k5eAaPmAgInS,k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
<g/>
.	.	kIx.
19	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
2000	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
407	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
6806	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
897	#num#	k4
<g/>
–	–	k?
<g/>
900	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
28	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
836	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
FISH	FISH	kA
<g/>
,	,	kIx,
S.	S.	kA
A	A	kA
<g/>
;	;	kIx,
SHEPHERD	SHEPHERD	kA
<g/>
,	,	kIx,
T.	T.	kA
J	J	kA
<g/>
;	;	kIx,
MCGENITY	MCGENITY	kA
<g/>
,	,	kIx,
T.	T.	kA
J	J	kA
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Recovery	Recover	k1gInPc7
of	of	k?
16S	16S	k4
ribosomal	ribosomal	k1gInSc1
RNA	RNA	kA
gene	gen	k1gInSc5
fragments	fragmentsit	k5eAaPmRp2nS
from	from	k1gMnSc1
ancient	ancient	k1gMnSc1
halite	halit	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
<g/>
.	.	kIx.
2002	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
417	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
6887	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
432	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
28	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
836	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
VOET	VOET	kA
<g/>
,	,	kIx,
Donald	Donald	k1gMnSc1
<g/>
;	;	kIx,
VOET	VOET	kA
<g/>
,	,	kIx,
Judith	Judith	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biochemistry	Biochemistr	k1gMnPc7
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Wiley	Wiley	k1gInPc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
470	#num#	k4
<g/>
-	-	kIx~
<g/>
57095	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
94	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Scientists	Scientists	k1gInSc1
identify	identif	k1gInPc4
seventh	seventh	k1gMnSc1
and	and	k?
eighth	eighth	k1gMnSc1
bases	bases	k1gMnSc1
of	of	k?
DNA	dna	k1gFnSc1
<g/>
,	,	kIx,
PhysOrg	PhysOrg	k1gInSc1
podle	podle	k7c2
Science	Science	k1gFnSc2
<g/>
,	,	kIx,
21	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
2007	#num#	k4
7.013	7.013	k4
Problem	Problo	k1gNnSc7
Set	set	k1gInSc1
3	#num#	k4
[	[	kIx(
<g/>
PDF	PDF	kA
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Massachusetts	Massachusetts	k1gNnSc1
Institute	institut	k1gInSc5
of	of	k?
Technology	technolog	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WOLFE-SIMON	WOLFE-SIMON	k1gMnSc1
<g/>
,	,	kIx,
F	F	kA
<g/>
;	;	kIx,
SWITZER	SWITZER	kA
BLUM	bluma	k1gFnPc2
<g/>
,	,	kIx,
J	J	kA
<g/>
;	;	kIx,
KULP	KULP	kA
<g/>
,	,	kIx,
T.	T.	kA
R	R	kA
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
bacterium	bacterium	k1gNnSc1
that	thata	k1gFnPc2
can	can	k?
grow	grow	k?
by	by	kYmCp3nS
using	using	k1gMnSc1
arsenic	arsenice	k1gFnPc2
instead	instead	k1gInSc4
of	of	k?
phosphorus	phosphorus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Science	Science	k1gFnSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
332	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
6034	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1163	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1095	#num#	k4
<g/>
-	-	kIx~
<g/>
9203	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
REAVES	REAVES	kA
<g/>
,	,	kIx,
Marshall	Marshall	k1gMnSc1
Louis	Louis	k1gMnSc1
<g/>
;	;	kIx,
SINHA	SINHA	kA
<g/>
,	,	kIx,
Sunita	sunita	k1gMnSc1
<g/>
;	;	kIx,
RABINOWITZ	RABINOWITZ	kA
<g/>
,	,	kIx,
Joshua	Joshua	k1gMnSc1
D	D	kA
<g/>
,	,	kIx,
KRUGLYAK	KRUGLYAK	kA
Leonid	Leonid	k1gInSc1
<g/>
,	,	kIx,
REDFIELD	REDFIELD	kA
Rosemary	Rosemara	k1gFnSc2
J.	J.	kA
Absence	absence	k1gFnSc1
of	of	k?
Detectable	Detectable	k1gMnSc5
Arsenate	Arsenat	k1gMnSc5
in	in	k?
DNA	DNA	kA
from	from	k1gMnSc1
Arsenate-Grown	Arsenate-Grown	k1gMnSc1
GFAJ-1	GFAJ-1	k1gMnSc1
Cells	Cells	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Science	Science	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Online	Onlin	k1gInSc5
před	před	k7c7
tiskem	tisk	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1095	#num#	k4
<g/>
-	-	kIx~
<g/>
9203	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.112	10.112	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
science	scienec	k1gInSc2
<g/>
.1219861	.1219861	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ERB	erb	k1gInSc1
<g/>
,	,	kIx,
Tobias	Tobias	k1gInSc1
J	J	kA
<g/>
;	;	kIx,
KIEFER	KIEFER	kA
<g/>
,	,	kIx,
Patrick	Patrick	k1gMnSc1
<g/>
;	;	kIx,
HATTENDORF	HATTENDORF	kA
<g/>
,	,	kIx,
Bodo	Bodo	k1gNnSc1
<g/>
,	,	kIx,
GÜNTHER	GÜNTHER	kA
Detlef	Detlef	k1gInSc1
<g/>
,	,	kIx,
VORHOLT	VORHOLT	kA
Julia	Julius	k1gMnSc2
A.	A.	kA
GFAJ-1	GFAJ-1	k1gMnSc1
Is	Is	k1gMnSc1
an	an	k?
Arsenate-Resistant	Arsenate-Resistant	k1gMnSc1
<g/>
,	,	kIx,
Phosphate-Dependent	Phosphate-Dependent	k1gMnSc1
Organism	Organism	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Science	Science	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Online	Onlin	k1gInSc5
před	před	k7c7
tiskem	tisk	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1095	#num#	k4
<g/>
-	-	kIx~
<g/>
9203	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.112	10.112	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
science	scienec	k1gInSc2
<g/>
.1218455	.1218455	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SHERIDAN	SHERIDAN	kA
<g/>
,	,	kIx,
Kerry	Kerra	k1gFnPc1
<g/>
:	:	kIx,
Scientists	Scientists	k1gInSc1
say	say	k?
NASA	NASA	kA
<g/>
'	'	kIx"
<g/>
s	s	k7c7
'	'	kIx"
<g/>
new	new	k?
arsenic	arsenice	k1gFnPc2
form	form	k1gMnSc1
of	of	k?
life	life	k1gInSc1
<g/>
'	'	kIx"
was	was	k?
untrue	untrue	k1gInSc1
(	(	kIx(
<g/>
popularizační	popularizační	k2eAgInSc1d1
článek	článek	k1gInSc1
k	k	k7c3
předchozím	předchozí	k2eAgFnPc3d1
2	#num#	k4
referencím	reference	k1gFnPc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
PhysOrg	PhysOrg	k1gMnSc1
<g/>
,	,	kIx,
9	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
2012	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Voet	Voet	k1gMnSc1
&	&	k?
Voet	Voet	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
84	#num#	k4
<g/>
–	–	k?
<g/>
85	#num#	k4
<g/>
↑	↑	k?
NELSON	Nelson	k1gMnSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
L	L	kA
<g/>
;	;	kIx,
COX	COX	kA
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
M.	M.	kA
Lehninger	Lehninger	k1gMnSc1
Principles	Principles	k1gMnSc1
of	of	k?
Biochemistry	Biochemistr	k1gMnPc7
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
W.	W.	kA
H.	H.	kA
Freeman	Freeman	k1gMnSc1
and	and	k?
Company	Compana	k1gFnSc2
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7167	#num#	k4
<g/>
-	-	kIx~
<g/>
7108	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
282	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LODISH	LODISH	kA
<g/>
,	,	kIx,
Harvey	Harvea	k1gFnSc2
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Molecular	Molecular	k1gInSc1
Cell	cello	k1gNnPc2
Biology	biolog	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
W.H.	W.H.	k1gMnSc1
Freedman	Freedman	k1gMnSc1
and	and	k?
Company	Compana	k1gFnSc2
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7167	#num#	k4
<g/>
-	-	kIx~
<g/>
4366	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
123	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
DNA	dno	k1gNnSc2
drží	držet	k5eAaImIp3nP
při	pře	k1gFnSc4
sobě	se	k3xPyFc3
spíše	spíše	k9
hydrofobní	hydrofobní	k2eAgFnPc1d1
interakce	interakce	k1gFnPc1
než	než	k8xS
vodíkové	vodíkový	k2eAgInPc1d1
můstky	můstek	k1gInPc1
<g/>
↑	↑	k?
Cammack	Cammack	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
47	#num#	k4
<g/>
↑	↑	k?
DUKE	DUKE	kA
<g/>
,	,	kIx,
Tom	Tom	k1gMnSc1
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Multiple	multipl	k1gInSc5
aspects	aspects	k1gInSc4
of	of	k?
DNA	DNA	kA
and	and	k?
RNA	RNA	kA
<g/>
:	:	kIx,
from	from	k1gInSc1
Biophysics	Biophysicsa	k1gFnPc2
to	ten	k3xDgNnSc4
Bioinformatics	Bioinformatics	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Didier	Didira	k1gFnPc2
Chatenay	Chatenaa	k1gFnSc2
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Elsevier	Elsevier	k1gMnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
444	#num#	k4
<g/>
-	-	kIx~
<g/>
52081	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
VARGASON	VARGASON	kA
<g/>
,	,	kIx,
J.	J.	kA
M	M	kA
<g/>
;	;	kIx,
HENDERSON	HENDERSON	kA
<g/>
,	,	kIx,
K	k	k7c3
<g/>
;	;	kIx,
HO	ho	k0
<g/>
,	,	kIx,
P.	P.	kA
S.	S.	kA
A	a	k8xC
crystallographic	crystallographic	k1gMnSc1
map	mapa	k1gFnPc2
of	of	k?
the	the	k?
transition	transition	k1gInSc1
from	from	k1gMnSc1
B-DNA	B-DNA	k1gMnSc1
to	ten	k3xDgNnSc4
A-DNA	A-DNA	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proc	proc	k1gMnSc1
Natl	Natl	k1gMnSc1
Acad	Acad	k1gInSc4
Sci	Sci	k1gMnSc3
U	U	kA
S	s	k7c7
A.	A.	kA
2001	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
98	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
13	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
7265	#num#	k4
<g/>
–	–	k?
<g/>
70	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
27	#num#	k4
<g/>
-	-	kIx~
<g/>
8424	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HA	ha	kA
<g/>
,	,	kIx,
S.	S.	kA
C	C	kA
<g/>
;	;	kIx,
LOWENHAUPT	LOWENHAUPT	kA
<g/>
,	,	kIx,
K	K	kA
<g/>
;	;	kIx,
RICH	RICH	kA
<g/>
,	,	kIx,
A	A	kA
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Crystal	Crystal	k1gFnSc1
structure	structur	k1gMnSc5
of	of	k?
a	a	k8xC
junction	junction	k1gInSc1
between	between	k2eAgMnSc1d1
B-DNA	B-DNA	k1gMnSc1
and	and	k?
Z-DNA	Z-DNA	k1gMnSc1
reveals	reveals	k6eAd1
two	two	k?
extruded	extruded	k1gMnSc1
bases	bases	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
<g/>
.	.	kIx.
2005	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
437	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
7062	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1183	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1476	#num#	k4
<g/>
-	-	kIx~
<g/>
4687	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Voet	Voet	k1gMnSc1
&	&	k?
Voet	Voet	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
1212	#num#	k4
<g/>
↑	↑	k?
ARCELLA	ARCELLA	kA
<g/>
,	,	kIx,
Annalisa	Annalisa	k1gFnSc1
<g/>
;	;	kIx,
PORTELLA	PORTELLA	kA
<g/>
,	,	kIx,
Guillem	Guill	k1gInSc7
<g/>
;	;	kIx,
LUZ	luza	k1gFnPc2
RUIZ	RUIZ	kA
<g/>
,	,	kIx,
Maria	Maria	k1gFnSc1
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Structure	Structur	k1gMnSc5
of	of	k?
Triplex	triplex	k1gInSc1
DNA	DNA	kA
in	in	k?
the	the	k?
Gas	Gas	k1gMnSc2
Phase	Phas	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
the	the	k?
American	American	k1gInSc4
Chemical	Chemical	k1gMnSc2
Society	societa	k1gFnSc2
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
134	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
15	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
6596	#num#	k4
<g/>
–	–	k?
<g/>
6606	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.102	10.102	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
ja	ja	k?
<g/>
209786	#num#	k4
<g/>
t.	t.	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
First	First	k1gInSc1
description	description	k1gInSc1
of	of	k?
a	a	k8xC
triple	tripl	k1gInSc5
DNA	DNA	kA
helix	helix	k1gInSc1
in	in	k?
a	a	k8xC
vacuum	vacuum	k1gInSc1
(	(	kIx(
<g/>
popularizační	popularizační	k2eAgInSc1d1
článek	článek	k1gInSc1
k	k	k7c3
předchozí	předchozí	k2eAgFnSc3d1
referenci	reference	k1gFnSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
PhysOrg	PhysOrg	k1gMnSc1
<g/>
,	,	kIx,
18	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2012	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Cammack	Cammack	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
679	#num#	k4
<g/>
↑	↑	k?
Lodish	Lodish	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
105	#num#	k4
<g/>
↑	↑	k?
GUO	GUO	kA
<g/>
,	,	kIx,
Q	Q	kA
<g/>
;	;	kIx,
LU	LU	kA
<g/>
,	,	kIx,
M	M	kA
<g/>
;	;	kIx,
CHURCHILL	CHURCHILL	kA
<g/>
,	,	kIx,
M.	M.	kA
E	E	kA
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asymmetric	Asymmetric	k1gMnSc1
structure	structur	k1gMnSc5
of	of	k?
a	a	k8xC
three-arm	three-arm	k1gInSc1
DNA	DNA	kA
junction	junction	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biochemistry	Biochemistr	k1gMnPc7
<g/>
.	.	kIx.
1990	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
29	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
49	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
10927	#num#	k4
<g/>
–	–	k?
<g/>
34	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
2960	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
COOPER	COOPER	kA
<g/>
,	,	kIx,
J.	J.	kA
P	P	kA
<g/>
;	;	kIx,
HAGERMAN	HAGERMAN	kA
<g/>
,	,	kIx,
P.	P.	kA
J.	J.	kA
Geometry	geometr	k1gInPc7
of	of	k?
a	a	k8xC
branched	branched	k1gInSc1
DNA	dno	k1gNnSc2
structure	structur	k1gMnSc5
in	in	k?
solution	solution	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proceedings	Proceedings	k1gInSc1
of	of	k?
the	the	k?
National	National	k1gMnSc1
Academy	Academa	k1gFnSc2
of	of	k?
Sciences	Sciences	k1gMnSc1
of	of	k?
the	the	k?
United	United	k1gMnSc1
States	States	k1gMnSc1
of	of	k?
America	America	k1gMnSc1
<g/>
.	.	kIx.
1989	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
86	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
19	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
7336	#num#	k4
<g/>
–	–	k?
<g/>
7340	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
27	#num#	k4
<g/>
-	-	kIx~
<g/>
8424	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
http://phys.org/news/2016-06-layer-dna.html	http://phys.org/news/2016-06-layer-dna.html	k1gMnSc1
-	-	kIx~
Second	Second	k1gMnSc1
layer	layer	k1gMnSc1
of	of	k?
information	information	k1gInSc1
in	in	k?
DNA	DNA	kA
confirmed	confirmed	k1gInSc1
<g/>
↑	↑	k?
ALBERTS	ALBERTS	kA
<g/>
,	,	kIx,
B.	B.	kA
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Molecular	Molecular	k1gMnSc1
Biology	biolog	k1gMnPc4
of	of	k?
the	the	k?
Cell	cello	k1gNnPc2
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Garland	Garland	k1gInSc1
Science	Science	k1gFnSc2
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
8153	#num#	k4
<g/>
-	-	kIx~
<g/>
4105	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
344	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WITZ	WITZ	kA
<g/>
,	,	kIx,
G	G	kA
<g/>
;	;	kIx,
STASIAK	STASIAK	kA
<g/>
,	,	kIx,
A.	A.	kA
DNA	DNA	kA
supercoiling	supercoiling	k1gInSc1
and	and	k?
its	its	k?
role	role	k1gFnSc2
in	in	k?
DNA	DNA	kA
decatenation	decatenation	k1gInSc1
and	and	k?
unknotting	unknotting	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nucleic	Nucleice	k1gInPc2
Acids	Acidsa	k1gFnPc2
Res	Res	k1gFnSc7
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
38	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
7	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2119	#num#	k4
<g/>
–	–	k?
<g/>
33	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1362	#num#	k4
<g/>
-	-	kIx~
<g/>
4962	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
GRISWOLD	GRISWOLD	kA
<g/>
,	,	kIx,
Ann	Ann	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Genome	genom	k1gInSc5
Packaging	Packaging	k1gInSc4
in	in	k?
Prokaryotes	Prokaryotes	k1gInSc1
<g/>
:	:	kIx,
the	the	k?
Circular	Circular	k1gInSc1
Chromosome	chromosom	k1gInSc5
of	of	k?
E.	E.	kA
coli	col	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
Education	Education	k1gInSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Alberts	Alberts	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
211	#num#	k4
<g/>
↑	↑	k?
Technical	Technical	k1gFnSc1
Appendix	Appendix	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Epibio	Epibio	k1gNnSc1
-	-	kIx~
Epicentre	Epicentr	k1gMnSc5
<g/>
®	®	k?
(	(	kIx(
<g/>
an	an	k?
Illumina	Illumin	k2eAgFnSc1d1
<g/>
®	®	k?
company	compan	k1gInPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
OSWALD	OSWALD	kA
<g/>
,	,	kIx,
Nick	Nick	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Basics	Basicsa	k1gFnPc2
<g/>
:	:	kIx,
How	How	k1gFnSc1
Ethanol	ethanol	k1gInSc1
Precipitation	Precipitation	k1gInSc1
of	of	k?
DNA	DNA	kA
and	and	k?
RNA	RNA	kA
Works	Works	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitesize	Bitesize	k1gFnSc1
Bio	Bio	k?
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Onion	Onion	k1gInSc1
Genomic	Genomic	k1gMnSc1
DNA	DNA	kA
Isolation	Isolation	k1gInSc1
[	[	kIx(
<g/>
PDF	PDF	kA
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
G-Biosciences	G-Biosciencesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
MULLIGAN	MULLIGAN	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
E.	E.	kA
The	The	k1gMnSc1
physical	physicat	k5eAaPmAgMnS
and	and	k?
chemical	chemicat	k5eAaPmAgMnS
properties	properties	k1gMnSc1
of	of	k?
nucleic	nucleic	k1gMnSc1
acids	acids	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1996	#num#	k4
<g/>
–	–	k?
<g/>
2003	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
VONDREJS	VONDREJS	kA
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
STORCHOVÁ	STORCHOVÁ	kA
<g/>
,	,	kIx,
Zuzana	Zuzana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Genové	genový	k2eAgNnSc1d1
inženýrství	inženýrství	k1gNnSc1
<g/>
,	,	kIx,
I.	I.	kA
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Karolinum	Karolinum	k1gNnSc1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
22	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MIHULKA	mihulka	k1gFnSc1
<g/>
,	,	kIx,
Stanislav	Stanislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jaký	jaký	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
je	být	k5eAaImIp3nS
poločas	poločas	k1gInSc4
rozkladu	rozklad	k1gInSc2
DNA	DNA	kA
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSEL	osel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
RNA	RNA	kA
vs	vs	k?
DNA	DNA	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NEWTON	Newton	k1gMnSc1
-	-	kIx~
Ask	Ask	k1gMnSc1
a	a	k8xC
scientist	scientist	k1gMnSc1
<g/>
,	,	kIx,
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Current	Current	k1gInSc1
Protocols	Protocols	k1gInSc1
in	in	k?
Molecular	Molecular	k1gInSc1
Biology	biolog	k1gMnPc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
John	John	k1gMnSc1
Wiley	Wilea	k1gFnSc2
&	&	k?
Sons	Sons	k1gInSc1
<g/>
,	,	kIx,
Inc	Inc	k1gFnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ŠRUBAŘOVÁ	ŠRUBAŘOVÁ	kA
<g/>
,	,	kIx,
P	P	kA
<g/>
;	;	kIx,
DVOŘÁK	Dvořák	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
VYUŽITÍ	využití	k1gNnSc2
CHITOSANU	CHITOSANU	kA
PRO	pro	k7c4
UCHOVÁNÍ	uchování	k1gNnSc4
DNA	dno	k1gNnSc2
A	a	k9
BIOLOGICKÝCH	biologický	k2eAgInPc2d1
VZORKŮ	vzorek	k1gInPc2
<g/>
.	.	kIx.
mendelu	mendel	k1gInSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
PDF	PDF	kA
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
,	,	kIx,
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MARGUET	MARGUET	kA
<g/>
,	,	kIx,
E	E	kA
<g/>
;	;	kIx,
FORTERRE	FORTERRE	kA
<g/>
,	,	kIx,
P.	P.	kA
DNA	dna	k1gFnSc1
stability	stabilita	k1gFnSc2
at	at	k?
temperatures	temperatures	k1gMnSc1
typical	typicat	k5eAaPmAgMnS
for	forum	k1gNnPc2
hyperthermophiles	hyperthermophilesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nucleic	Nucleice	k1gFnPc2
Acids	Acidsa	k1gFnPc2
Research	Research	k1gInSc1
<g/>
.	.	kIx.
1994	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
22	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
9	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1681	#num#	k4
<g/>
–	–	k?
<g/>
1686	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
305	#num#	k4
<g/>
-	-	kIx~
<g/>
1048	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Alberts	Alberts	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
2601	#num#	k4
2	#num#	k4
Nelson	Nelson	k1gMnSc1
&	&	k?
Cox	Cox	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
271	#num#	k4
<g/>
↑	↑	k?
SHERWOOD	SHERWOOD	kA
<g/>
,	,	kIx,
Lauralee	Lauralee	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Human	Human	k1gInSc1
physiology	physiolog	k1gMnPc7
<g/>
:	:	kIx,
from	from	k6eAd1
cells	cells	k6eAd1
to	ten	k3xDgNnSc1
systems	systems	k6eAd1
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Cengage	Cengage	k1gFnSc1
Learning	Learning	k1gInSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
928	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
395	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HUNT	hunt	k1gInSc1
<g/>
,	,	kIx,
Margaret	Margareta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Virology	virolog	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
„	„	k?
<g/>
RNA	RNA	kA
virus	virus	k1gInSc1
replication	replication	k1gInSc1
strategies	strategies	k1gInSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MARATHE	MARATHE	kA
<g/>
,	,	kIx,
A	A	kA
<g/>
;	;	kIx,
CONDON	CONDON	kA
<g/>
,	,	kIx,
A.	A.	kA
E	E	kA
<g/>
;	;	kIx,
CORN	CORN	kA
<g/>
,	,	kIx,
R.	R.	kA
M.	M.	kA
On	on	k3xPp3gMnSc1
combinatorial	combinatorial	k1gMnSc1
DNA	DNA	kA
word	word	k1gInSc1
design	design	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
J	J	kA
Comput	Comput	k1gInSc1
Biol.	Biol.	k1gFnSc1
2001	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
8	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
201	#num#	k4
<g/>
–	–	k?
<g/>
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1066	#num#	k4
<g/>
-	-	kIx~
<g/>
5277	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LITTLE	LITTLE	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
F.	F.	kA
R.	R.	kA
Structure	Structur	k1gMnSc5
and	and	k?
function	function	k1gInSc1
of	of	k?
the	the	k?
human	human	k1gInSc1
genome	genom	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Genome	genom	k1gInSc5
Research	Research	k1gMnSc1
<g/>
.	.	kIx.
2005	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
15	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
12	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1759	#num#	k4
<g/>
–	–	k?
<g/>
1766	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.110	10.110	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
gr	gr	k?
<g/>
.4560905	.4560905	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Alberts	Alberts	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
253	#num#	k4
<g/>
↑	↑	k?
Alberts	Alberts	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
204	#num#	k4
<g/>
↑	↑	k?
JHA	jho	k1gNnSc2
<g/>
,	,	kIx,
Alok	Aloka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Breakthrough	Breakthrougha	k1gFnPc2
study	stud	k1gInPc1
overturns	overturns	k6eAd1
theory	theora	k1gFnSc2
of	of	k?
'	'	kIx"
<g/>
junk	junk	k1gMnSc1
DNA	DNA	kA
<g/>
'	'	kIx"
in	in	k?
genome	genom	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HAMZELOU	HAMZELOU	kA
<g/>
,	,	kIx,
Jessica	Jessicum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Global	globat	k5eAaImAgMnS
project	project	k5eAaPmF
reveals	reveals	k6eAd1
just	just	k6eAd1
how	how	k?
active	activat	k5eAaPmIp3nS
our	our	k?
'	'	kIx"
<g/>
junk	junk	k1gMnSc1
<g/>
'	'	kIx"
DNA	DNA	kA
is	is	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
Scientist	Scientist	k1gMnSc1
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Vondrejs	Vondrejs	k1gInSc1
&	&	k?
Storchová	Storchová	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
171	#num#	k4
2	#num#	k4
Vondrejs	Vondrejs	k1gInSc1
&	&	k?
Storchová	Storchová	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
18	#num#	k4
<g/>
↑	↑	k?
Voet	Voet	k1gMnSc1
&	&	k?
Voet	Voet	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
159	#num#	k4
<g/>
↑	↑	k?
Nucleic	Nucleic	k1gMnSc1
Acid	Acid	k1gMnSc1
Stains	Stainsa	k1gFnPc2
<g/>
—	—	k?
<g/>
Section	Section	k1gInSc1
8.1	8.1	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
InVitrogen	InVitrogen	k1gInSc1
life	lif	k1gFnSc2
technologies	technologiesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Cammack	Cammack	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
248	#num#	k4
<g/>
↑	↑	k?
RACLAVSKÝ	RACLAVSKÝ	kA
<g/>
,	,	kIx,
Vladislav	Vladislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úvod	úvod	k1gInSc1
do	do	k7c2
základních	základní	k2eAgFnPc2d1
metod	metoda	k1gFnPc2
molekulární	molekulární	k2eAgFnSc2d1
genetiky	genetika	k1gFnSc2
[	[	kIx(
<g/>
PDF	PDF	kA
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústav	ústav	k1gInSc1
biologie	biologie	k1gFnSc2
Lékařské	lékařský	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
Univerzity	univerzita	k1gFnSc2
Palackého	Palacký	k1gMnSc2
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
<g/>
,	,	kIx,
červen	červen	k1gInSc1
1999	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sekvenování	Sekvenování	k1gNnSc1
DNA	DNA	kA
<g/>
,	,	kIx,
s.	s.	k?
21	#num#	k4
<g/>
–	–	k?
<g/>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7067	#num#	k4
<g/>
-	-	kIx~
<g/>
892	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ZHANG	ZHANG	kA
<g/>
,	,	kIx,
J	J	kA
<g/>
;	;	kIx,
CHIODINI	CHIODINI	kA
<g/>
,	,	kIx,
R	R	kA
<g/>
;	;	kIx,
BADR	BADR	kA
<g/>
,	,	kIx,
A	A	kA
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
impact	impact	k1gMnSc1
of	of	k?
next-generation	next-generation	k1gInSc1
sequencing	sequencing	k1gInSc1
on	on	k3xPp3gMnSc1
genomics	genomics	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
J	J	kA
Genet	Geneta	k1gFnPc2
Genomics	Genomicsa	k1gFnPc2
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
38	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
95	#num#	k4
<g/>
–	–	k?
<g/>
109	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1673	#num#	k4
<g/>
-	-	kIx~
<g/>
8527	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
VONDREJS	VONDREJS	kA
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Genové	genový	k2eAgNnSc1d1
inženýrství	inženýrství	k1gNnSc1
<g/>
,	,	kIx,
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Karolinum	Karolinum	k1gNnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
22	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KAYSER	KAYSER	kA
<g/>
,	,	kIx,
F.	F.	kA
H	H	kA
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Medical	Medical	k1gFnSc1
Microbiology	Microbiolog	k1gMnPc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Thieme	Thiem	k1gInSc5
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
216	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BRONCHUD	BRONCHUD	kA
<g/>
,	,	kIx,
Miguel	Miguel	k1gMnSc1
H	H	kA
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Totowa	Totowa	k1gMnSc1
<g/>
,	,	kIx,
New	New	k1gMnSc1
Jersey	Jersea	k1gFnSc2
<g/>
:	:	kIx,
Humana	Humana	k1gFnSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
45	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SADLER	SADLER	kA
<g/>
,	,	kIx,
Thomas	Thomas	k1gMnSc1
W.	W.	kA
Langman	Langman	k1gMnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Medical	Medical	k1gFnSc7
Embryology	embryolog	k1gMnPc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Lippincott	Lippincott	k2eAgInSc1d1
Williams	Williams	k1gInSc1
&	&	k?
Wilkins	Wilkins	k1gInSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
163	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LEWIS	LEWIS	kA
<g/>
,	,	kIx,
Ricki	Ricki	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Human	Human	k1gInSc1
Genetics	Genetics	k1gInSc4
<g/>
:	:	kIx,
Concepts	Concepts	k1gInSc1
and	and	k?
Applications	Applications	k1gInSc1
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
McGraw	McGraw	k1gMnSc1
<g/>
−	−	k?
<g/>
Hill	Hill	k1gMnSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
400	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
William	William	k1gInSc1
S.	S.	kA
Klug	Klug	k1gInSc1
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
R.	R.	kA
Cummings	Cummings	k1gInSc1
<g/>
,	,	kIx,
Charlotte	Charlott	k1gInSc5
A.	A.	kA
Spencer	Spencer	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Concepts	Concepts	k1gInSc1
of	of	k?
Genetics	Genetics	k1gInSc1
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
567	#num#	k4
<g/>
–	–	k?
<g/>
568	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Klug	Klug	k1gInSc1
&	&	k?
Cunnings	Cunnings	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
550	#num#	k4
<g/>
↑	↑	k?
DROBNÍK	DROBNÍK	kA
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přehledy	přehled	k1gInPc4
o	o	k7c6
dopadech	dopad	k1gInPc6
transgenních	transgenní	k2eAgFnPc2d1
plodin	plodina	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MIHULKA	mihulka	k1gFnSc1
<g/>
,	,	kIx,
Stanislav	Stanislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geneticky	geneticky	k6eAd1
modifikovaná	modifikovaný	k2eAgFnSc1d1
kukuřice	kukuřice	k1gFnSc1
a	a	k8xC
chrostíci	chrostík	k1gMnPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Klug	Klug	k1gInSc1
&	&	k?
Cunnings	Cunnings	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
656	#num#	k4
<g/>
–	–	k?
<g/>
658	#num#	k4
<g/>
↑	↑	k?
JONES	JONES	kA
<g/>
,	,	kIx,
Elizabeth	Elizabeth	k1gFnSc7
D.	D.	kA
Ancient	Ancient	k1gMnSc1
DNA	DNA	kA
<g/>
:	:	kIx,
s	s	k7c7
history	histor	k1gInPc7
of	of	k?
the	the	k?
science	science	k1gFnSc2
before	befor	k1gInSc5
Jurassic	Jurassic	k1gMnSc1
Park	park	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studies	Studies	k1gMnSc1
in	in	k?
History	Histor	k1gInPc1
and	and	k?
Philosophy	Philosopha	k1gFnSc2
of	of	k?
Science	Science	k1gFnSc1
<g/>
,	,	kIx,
Part	part	k1gInSc1
C	C	kA
<g/>
:	:	kIx,
Studies	Studies	k1gMnSc1
in	in	k?
History	Histor	k1gInPc1
and	and	k?
Philosophy	Philosopha	k1gFnSc2
of	of	k?
Biological	Biological	k1gMnSc1
and	and	k?
Biomedical	Biomedical	k1gMnSc1
Sciences	Sciences	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elsevier	Elsevier	k1gMnSc1
B.V	B.V	k1gMnSc1
<g/>
,	,	kIx,
9	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Online	Onlin	k1gInSc5
před	před	k7c7
tiskem	tisk	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1369	#num#	k4
<g/>
-	-	kIx~
<g/>
8486	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
0.1016	0.1016	k4
<g/>
/	/	kIx~
<g/>
j.	j.	k?
<g/>
shpsc	shpsc	k1gInSc1
<g/>
.2018	.2018	k4
<g/>
.02	.02	k4
<g/>
.001	.001	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
GUTIÉRREZ	GUTIÉRREZ	kA
<g/>
,	,	kIx,
Gabriel	Gabriel	k1gMnSc1
<g/>
;	;	kIx,
MARÍN	Marína	k1gFnPc2
<g/>
,	,	kIx,
Antonio	Antonio	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnPc2
most	most	k1gInSc1
ancient	ancient	k1gMnSc1
DNA	DNA	kA
recovered	recovered	k1gMnSc1
from	from	k1gMnSc1
an	an	k?
amber-preserved	amber-preserved	k1gMnSc1
specimen	specimen	k1gNnSc1
may	may	k?
not	nota	k1gFnPc2
be	be	k?
as	as	k9
ancient	ancient	k1gMnSc1
as	as	k1gNnSc2
it	it	k?
seems	seemsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Molecular	Molecular	k1gInSc1
Biology	biolog	k1gMnPc4
and	and	k?
Evolution	Evolution	k1gInSc1
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
1998	#num#	k4
<g/>
,	,	kIx,
svazek	svazek	k1gInSc1
15	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgFnPc1wB
<g/>
.	.	kIx.
s.	s.	k?
926	#num#	k4
<g/>
–	–	k?
<g/>
929	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
http://mbe.oxfordjournals.org/content/15/7/926.full.pdf	http://mbe.oxfordjournals.org/content/15/7/926.full.pdf	k1gInSc1
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
PDF	PDF	kA
<g/>
,	,	kIx,
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
737	#num#	k4
<g/>
-	-	kIx~
<g/>
4038	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PALMER	PALMER	kA
<g/>
,	,	kIx,
Roxanne	Roxann	k1gMnSc5
<g/>
.	.	kIx.
'	'	kIx"
<g/>
Jurassic	Jurassice	k1gFnPc2
Park	park	k1gInSc1
<g/>
'	'	kIx"
Method	Method	k1gInSc1
Of	Of	k1gMnPc2
Cloning	Cloning	k1gInSc1
Dinosaurs	Dinosaursa	k1gFnPc2
From	From	k1gInSc1
Amber-Preserved	Amber-Preserved	k1gMnSc1
Insects	Insectsa	k1gFnPc2
Not	nota	k1gFnPc2
Feasible	Feasible	k1gMnPc2
<g/>
,	,	kIx,
Scientists	Scientists	k1gInSc4
Say	Say	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Internation	Internation	k1gInSc1
Business	business	k1gInSc1
Times	Times	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
IBT	IBT	kA
Media	medium	k1gNnSc2
Inc	Inc	k1gMnSc7
<g/>
.	.	kIx.
<g/>
,	,	kIx,
13	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
2009	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
skutečně	skutečně	k6eAd1
objevena	objevit	k5eAaPmNgFnS
dinosauří	dinosauří	k2eAgFnSc1d1
DNA	dna	k1gFnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osel	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
AUSTIN	AUSTIN	kA
<g/>
,	,	kIx,
Jeremy	Jerem	k1gInPc7
J	J	kA
<g/>
;	;	kIx,
ROSS	ROSS	kA
<g/>
,	,	kIx,
Andrew	Andrew	k1gMnSc1
J	J	kA
<g/>
;	;	kIx,
SMITH	SMITH	kA
<g/>
,	,	kIx,
Andrew	Andrew	k1gMnSc1
B	B	kA
<g/>
;	;	kIx,
FORTEY	FORTEY	kA
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
A	A	kA
<g/>
;	;	kIx,
THOMAS	Thomas	k1gMnSc1
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
H.	H.	kA
Problems	Problems	k1gInSc1
of	of	k?
reproducibility	reproducibilita	k1gFnSc2
–	–	k?
does	does	k6eAd1
geologically	geologicalnout	k5eAaImAgInP,k5eAaBmAgInP,k5eAaPmAgInP
ancient	ancient	k1gInSc4
DNA	dno	k1gNnSc2
survive	surviev	k1gFnSc2
in	in	k?
amber	ambra	k1gFnPc2
<g/>
–	–	k?
<g/>
preserved	preserved	k1gInSc1
insects	insects	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
467	#num#	k4
<g/>
–	–	k?
<g/>
474	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proceedings	Proceedings	k1gInSc1
of	of	k?
the	the	k?
Royal	Royal	k1gMnSc1
Society	societa	k1gFnSc2
B	B	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
22	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
1997	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
264	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1381	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
467	#num#	k4
<g/>
–	–	k?
<g/>
474	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgInPc1d1
také	také	k9
na	na	k7c4
<g/>
:	:	kIx,
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
dostupné	dostupný	k2eAgNnSc1d1
na	na	k7c6
<g/>
:	:	kIx,
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1471	#num#	k4
<g/>
-	-	kIx~
<g/>
2954	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.109	10.109	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
rspb	rspb	k1gInSc1
<g/>
.1997	.1997	k4
<g/>
.0067	.0067	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
9149422	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PENNEY	PENNEY	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
;	;	kIx,
WADSWORTH	WADSWORTH	kA
<g/>
,	,	kIx,
Caroline	Carolin	k1gInSc5
<g/>
;	;	kIx,
FOX	fox	k1gInSc1
<g/>
,	,	kIx,
Graeme	Graem	k1gInSc5
<g/>
;	;	kIx,
KENNEDY	KENNEDY	kA
<g/>
,	,	kIx,
Sandra	Sandra	k1gFnSc1
L	L	kA
<g/>
;	;	kIx,
PREZIOSI	PREZIOSI	kA
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
F	F	kA
<g/>
;	;	kIx,
BROWN	BROWN	kA
<g/>
,	,	kIx,
Terence	Terence	k1gFnSc1
A.	A.	kA
Absence	absence	k1gFnPc1
of	of	k?
Ancient	Ancient	k1gMnSc1
DNA	DNA	kA
in	in	k?
Sub-Fossil	Sub-Fossil	k1gMnSc1
Insect	Insect	k1gMnSc1
Inclusions	Inclusionsa	k1gFnPc2
Preserved	Preserved	k1gMnSc1
in	in	k?
‘	‘	k?
<g/>
Anthropocene	Anthropocen	k1gInSc5
<g/>
’	’	k?
Colombian	Colombian	k1gMnSc1
Copal	Copal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
PLoS	plos	k1gMnSc1
ONE	ONE	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2013	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
8	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
9	#num#	k4
<g/>
:	:	kIx,
e	e	k0
<g/>
73150	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1932	#num#	k4
<g/>
-	-	kIx~
<g/>
6203	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.137	10.137	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
journal	journat	k5eAaImAgInS,k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
pone	pone	k1gInSc1
<g/>
.0073150	.0073150	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
https://phys.org/news/2019-09-game-changing-evolution-mysteries.html	https://phys.org/news/2019-09-game-changing-evolution-mysteries.html	k1gInSc1
-	-	kIx~
'	'	kIx"
<g/>
Game-changing	Game-changing	k1gInSc1
<g/>
'	'	kIx"
research	research	k1gInSc1
could	could	k6eAd1
solve	solvat	k5eAaPmIp3nS
evolution	evolution	k1gInSc1
mysteries	mysteries	k1gMnSc1
<g/>
↑	↑	k?
Alida	Alida	k1gMnSc1
M.	M.	kA
Bailleul	Bailleul	k1gInSc1
<g/>
,	,	kIx,
Wenxia	Wenxia	k1gFnSc1
Zheng	Zheng	k1gMnSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
R.	R.	kA
Horner	Horner	k1gMnSc1
<g/>
,	,	kIx,
Brian	Brian	k1gMnSc1
K.	K.	kA
Hall	Hall	k1gMnSc1
<g/>
,	,	kIx,
Casey	Casea	k1gMnSc2
M.	M.	kA
Holliday	Hollidaa	k1gMnSc2
&	&	k?
Mary	Mary	k1gFnSc2
H.	H.	kA
Schweitzer	Schweitzer	k1gMnSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evidence	evidence	k1gFnSc1
of	of	k?
proteins	proteins	k1gInSc1
<g/>
,	,	kIx,
chromosomes	chromosomes	k1gInSc1
and	and	k?
chemical	chemicat	k5eAaPmAgInS
markers	markers	k1gInSc1
of	of	k?
DNA	DNA	kA
in	in	k?
exceptionally	exceptionalla	k1gFnSc2
preserved	preserved	k1gMnSc1
dinosaur	dinosaur	k1gMnSc1
cartilage	cartilagat	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
National	National	k1gFnSc1
Science	Science	k1gFnSc1
Review	Review	k1gFnSc1
<g/>
,	,	kIx,
nwz	nwz	k?
<g/>
206	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1093/nsr/nwz206	https://doi.org/10.1093/nsr/nwz206	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Česky	česky	k6eAd1
</s>
<s>
ROSYPAL	ROSYPAL	kA
<g/>
,	,	kIx,
Stanislav	Stanislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úvod	úvod	k1gInSc1
do	do	k7c2
molekulární	molekulární	k2eAgFnSc2d1
biologie	biologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc4
díl	díl	k1gInSc4
<g/>
,	,	kIx,
Vstup	vstup	k1gInSc4
do	do	k7c2
molekulární	molekulární	k2eAgFnSc2d1
biologie	biologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Molekulární	molekulární	k2eAgFnSc1d1
biologie	biologie	k1gFnSc1
prokaryotické	prokaryotický	k2eAgFnSc2d1
buňky	buňka	k1gFnSc2
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
inov	inova	k1gFnPc2
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Rosypal	Rosypal	k1gMnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
289	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
902562	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
VOET	VOET	kA
<g/>
,	,	kIx,
Donald	Donald	k1gMnSc1
<g/>
;	;	kIx,
VOET	VOET	kA
<g/>
,	,	kIx,
Judith	Judith	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biochemie	biochemie	k1gFnSc1
(	(	kIx(
<g/>
původním	původní	k2eAgInSc7d1
názvem	název	k1gInSc7
<g/>
:	:	kIx,
Biochemistry	Biochemistr	k1gMnPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Arnošt	Arnošt	k1gMnSc1
Kotyk	Kotyk	k1gInSc1
a	a	k8xC
kolektiv	kolektiv	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Victoria	Victorium	k1gNnSc2
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
xiv	xiv	k?
<g/>
,	,	kIx,
1325	#num#	k4
<g/>
,	,	kIx,
xxiii	xxiie	k1gFnSc6
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85605	#num#	k4
<g/>
-	-	kIx~
<g/>
44	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ŠTÍPEK	Štípek	k1gMnSc1
<g/>
,	,	kIx,
Stanislav	Stanislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stručná	stručný	k2eAgFnSc1d1
biochemie	biochemie	k1gFnSc1
:	:	kIx,
uchování	uchování	k1gNnSc1
a	a	k8xC
exprese	exprese	k1gFnPc1
genetické	genetický	k2eAgFnPc4d1
informace	informace	k1gFnPc4
:	:	kIx,
učební	učební	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Medprint	Medprint	k1gMnSc1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
92	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
902036	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ŠMARDA	Šmarda	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Metody	metoda	k1gFnPc4
molekulární	molekulární	k2eAgFnSc2d1
biologie	biologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
MU	MU	kA
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
188	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
210	#num#	k4
<g/>
-	-	kIx~
<g/>
3841	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Anglicky	anglicky	k6eAd1
</s>
<s>
ALBERTS	ALBERTS	kA
<g/>
,	,	kIx,
Bruce	Bruce	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Molecular	Molecular	k1gMnSc1
Biology	biolog	k1gMnPc4
of	of	k?
the	the	k?
Cell	cello	k1gNnPc2
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Garland	Garland	k1gInSc1
Science	Science	k1gFnSc2
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
1600	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
8153	#num#	k4
<g/>
-	-	kIx~
<g/>
4105	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
LODISH	LODISH	kA
<g/>
,	,	kIx,
Harvey	Harvea	k1gFnSc2
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Molecular	Molecular	k1gInSc1
Cell	cello	k1gNnPc2
Biology	biolog	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
W.	W.	kA
H.	H.	kA
Freedman	Freedman	k1gMnSc1
and	and	k?
Company	Compana	k1gFnSc2
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7167	#num#	k4
<g/>
-	-	kIx~
<g/>
4366	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
DNA	dno	k1gNnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
DNA	DNA	kA
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
DNA	DNA	kA
nahrazuje	nahrazovat	k5eAaImIp3nS
pevné	pevný	k2eAgInPc4d1
disky	disk	k1gInPc4
i	i	k8xC
procesory	procesor	k1gInPc4
–	–	k?
současné	současný	k2eAgFnSc3d1
technické	technický	k2eAgFnSc3d1
možnosti	možnost	k1gFnSc3
využití	využití	k1gNnSc2
DNA	DNA	kA
pozn	pozn	kA
<g/>
.	.	kIx.
Celý	celý	k2eAgInSc1d1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
za	za	k7c4
poplatek	poplatek	k1gInSc4
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Fidelitysystems	Fidelitysystems	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
–	–	k?
fotografie	fotografia	k1gFnSc2
DNA	DNA	kA
pod	pod	k7c7
elektronovým	elektronový	k2eAgInSc7d1
mikroskopem	mikroskop	k1gInSc7
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
DNA	DNA	kA
Learning	Learning	k1gInSc1
Center	centrum	k1gNnPc2
–	–	k?
internetová	internetový	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
zaměřená	zaměřený	k2eAgFnSc1d1
na	na	k7c4
popularizaci	popularizace	k1gFnSc4
a	a	k8xC
výuku	výuka	k1gFnSc4
informací	informace	k1gFnPc2
o	o	k7c6
DNA	DNA	kA
a	a	k8xC
souvisejících	související	k2eAgNnPc6d1
tématech	téma	k1gNnPc6
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
DNA	DNA	kA
&	&	k?
RNA	RNA	kA
na	na	k7c6
portálu	portál	k1gInSc6
amerického	americký	k2eAgInSc2d1
NCBI	NCBI	kA
–	–	k?
seznam	seznam	k1gInSc1
vědeckých	vědecký	k2eAgFnPc2d1
databází	databáze	k1gFnPc2
a	a	k8xC
informačních	informační	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
DNA	DNA	kA
from	from	k1gInSc1
the	the	k?
Beginning	Beginning	k1gInSc1
–	–	k?
přehledný	přehledný	k2eAgInSc1d1
popis	popis	k1gInSc1
75	#num#	k4
objevů	objev	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
změnily	změnit	k5eAaPmAgFnP
pohled	pohled	k1gInSc4
na	na	k7c4
genetiku	genetika	k1gFnSc4
a	a	k8xC
na	na	k7c4
DNA	DNA	kA
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
National	National	k1gFnSc3
DNA	dno	k1gNnSc2
Day	Day	k1gFnSc2
–	–	k?
výukové	výukový	k2eAgInPc4d1
materiály	materiál	k1gInPc4
<g/>
,	,	kIx,
různé	různý	k2eAgFnPc4d1
učebnice	učebnice	k1gFnPc4
a	a	k8xC
články	článek	k1gInPc4
na	na	k7c4
téma	téma	k1gNnSc4
DNA	dno	k1gNnSc2
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
DNA	DNA	kA
–	–	k?
Molecule	Molecule	k1gFnSc2
of	of	k?
the	the	k?
Month	Month	k1gInSc1
–	–	k?
přehledný	přehledný	k2eAgInSc1d1
článek	článek	k1gInSc1
o	o	k7c6
struktuře	struktura	k1gFnSc6
DNA	dno	k1gNnSc2
Archivováno	archivovat	k5eAaBmNgNnS
4	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Hlavní	hlavní	k2eAgInPc1d1
typy	typ	k1gInPc1
nukleových	nukleový	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
Stavební	stavební	k2eAgInPc1d1
kameny	kámen	k1gInPc1
</s>
<s>
Nukleové	nukleový	k2eAgFnPc1d1
báze	báze	k1gFnPc1
•	•	k?
Nukleosidy	Nukleosida	k1gFnSc2
•	•	k?
Nukleotidy	nukleotid	k1gInPc4
•	•	k?
Deoxynukleotidy	Deoxynukleotida	k1gFnSc2
Obecná	obecný	k2eAgFnSc1d1
charakteristika	charakteristika	k1gFnSc1
</s>
<s>
Konformace	Konformace	k1gFnSc1
(	(	kIx(
<g/>
A-forma	A-forma	k1gFnSc1
<g/>
,	,	kIx,
B-forma	B-forma	k1gFnSc1
<g/>
,	,	kIx,
Z-forma	Z-forma	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Počet	počet	k1gInSc1
vláken	vlákna	k1gFnPc2
(	(	kIx(
<g/>
dsRNA	dsRNA	k?
•	•	k?
ssRNA	ssRNA	k?
•	•	k?
dsDNA	dsDNA	k?
•	•	k?
ssDNA	ssDNA	k?
•	•	k?
třívláknová	třívláknový	k2eAgFnSc1d1
DNA	dna	k1gFnSc1
•	•	k?
DNA-RNA	DNA-RNA	k1gFnSc2
vlákna	vlákno	k1gNnSc2
<g/>
)	)	kIx)
Deoxyribonukleová	deoxyribonukleový	k2eAgFnSc1d1
kyselina	kyselina	k1gFnSc1
</s>
<s>
genomová	genomový	k2eAgFnSc1d1
DNA	dna	k1gFnSc1
(	(	kIx(
<g/>
gDNA	gDNA	k?
<g/>
)	)	kIx)
•	•	k?
komplementární	komplementární	k2eAgFnSc1d1
DNA	dna	k1gFnSc1
(	(	kIx(
<g/>
cDNA	cDNA	k?
<g/>
)	)	kIx)
•	•	k?
chloroplastová	chloroplastová	k1gFnSc1
DNA	DNA	kA
(	(	kIx(
<g/>
cpDNA	cpDNA	k?
<g/>
)	)	kIx)
•	•	k?
mitochondriální	mitochondriální	k2eAgFnSc1d1
DNA	dna	k1gFnSc1
mtDNA	mtDNA	k?
•	•	k?
jednořetězcová	jednořetězcový	k2eAgFnSc1d1
DNA	dna	k1gFnSc1
o	o	k7c6
více	hodně	k6eAd2
kopiích	kopie	k1gFnPc6
(	(	kIx(
<g/>
msDNA	msDNA	k?
<g/>
)	)	kIx)
Ribonukleová	ribonukleový	k2eAgFnSc1d1
kyselina	kyselina	k1gFnSc1
</s>
<s>
kódující	kódující	k2eAgFnSc1d1
</s>
<s>
mediátorová	mediátorový	k2eAgFnSc1d1
RNA	RNA	kA
(	(	kIx(
<g/>
mRNA	mRNA	k?
<g/>
)	)	kIx)
(	(	kIx(
<g/>
pre-mRNA	pre-mRNA	k?
<g/>
/	/	kIx~
<g/>
hnRNA	hnRNA	k?
<g/>
)	)	kIx)
•	•	k?
transferová	transferový	k2eAgFnSc1d1
RNA	RNA	kA
(	(	kIx(
<g/>
tRNA	trnout	k5eAaImSgMnS
<g/>
)	)	kIx)
•	•	k?
ribozomální	ribozomální	k2eAgFnSc1d1
RNA	RNA	kA
(	(	kIx(
<g/>
rRNA	rRNA	k?
<g/>
)	)	kIx)
RNA	RNA	kA
interference	interference	k1gFnSc1
</s>
<s>
mikro	mikro	k6eAd1
RNA	RNA	kA
(	(	kIx(
<g/>
miRNA	miRNA	k?
<g/>
)	)	kIx)
•	•	k?
malá	malý	k2eAgFnSc1d1
interferující	interferující	k2eAgFnSc1d1
RNA	RNA	kA
(	(	kIx(
<g/>
siRNA	siRNA	k?
<g/>
)	)	kIx)
Úpravy	úprava	k1gFnSc2
RNA	RNA	kA
</s>
<s>
malá	malý	k2eAgFnSc1d1
jaderná	jaderný	k2eAgFnSc1d1
RNA	RNA	kA
(	(	kIx(
<g/>
snRNA	snRNA	k?
<g/>
)	)	kIx)
•	•	k?
malá	malý	k2eAgFnSc1d1
jadérková	jadérková	k1gFnSc1
RNA	RNA	kA
(	(	kIx(
<g/>
snoRNA	snoRNA	k?
<g/>
)	)	kIx)
•	•	k?
guide	guide	k6eAd1
RNA	RNA	kA
</s>
<s>
Analogy	analog	k1gInPc1
nukleových	nukleový	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
</s>
<s>
XNA	XNA	kA
(	(	kIx(
GNA	GNA	kA
/	/	kIx~
TNA	tnout	k5eAaBmSgMnS
/	/	kIx~
HNA	HNA	kA
)	)	kIx)
•	•	k?
LNA	lnout	k5eAaImSgInS
•	•	k?
PNA	pnout	k5eAaImSgInS
•	•	k?
morpholino	morpholin	k2eAgNnSc1d1
Klonovací	Klonovací	k2eAgInPc1d1
vektory	vektor	k1gInPc1
</s>
<s>
Fagemid	Fagemid	k1gInSc1
•	•	k?
Plasmid	Plasmid	k1gInSc1
•	•	k?
Lambda	lambda	k1gNnPc2
fág	fág	k1gInSc1
•	•	k?
Kosmid	Kosmid	k1gInSc1
•	•	k?
Fosmid	Fosmid	k1gInSc1
•	•	k?
Umělý	umělý	k2eAgInSc1d1
chromozom	chromozom	k1gInSc1
odvozený	odvozený	k2eAgInSc1d1
od	od	k7c2
fága	fág	k1gInSc2
P1	P1	k1gFnPc2
(	(	kIx(
<g/>
PAC	pac	k1gFnPc2
<g/>
)	)	kIx)
bakterií	bakterie	k1gFnPc2
(	(	kIx(
<g/>
BAC	bacit	k5eAaPmRp2nS
<g/>
)	)	kIx)
kvasinek	kvasinka	k1gFnPc2
(	(	kIx(
<g/>
YAC	YAC	kA
<g/>
)	)	kIx)
lidských	lidský	k2eAgInPc2d1
chromozomů	chromozom	k1gInPc2
HAC	HAC	kA
</s>
<s>
Složky	složka	k1gFnPc1
nukleových	nukleový	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
Nukleové	nukleový	k2eAgFnSc2d1
báze	báze	k1gFnSc2
</s>
<s>
Purinové	purinový	k2eAgNnSc1d1
</s>
<s>
Adenin	adenin	k1gInSc1
•	•	k?
Guanin	guanin	k1gInSc1
Pyrimidinové	Pyrimidinový	k2eAgInPc1d1
</s>
<s>
Thymin	Thymin	k2eAgMnSc1d1
•	•	k?
Uracil	Uracil	k1gMnSc1
•	•	k?
Cytosin	Cytosin	k1gMnSc1
</s>
<s>
Běžné	běžný	k2eAgFnPc1d1
modifikace	modifikace	k1gFnPc1
nukleových	nukleový	k2eAgFnPc2d1
bází	báze	k1gFnPc2
</s>
<s>
V	v	k7c6
RNA	RNA	kA
</s>
<s>
Adenin	adenin	k1gInSc1
→	→	k?
Xantin	Xantin	k1gInSc1
<g/>
,	,	kIx,
Hypoxantin	Hypoxantin	k2eAgInSc1d1
•	•	k?
Guanin	guanin	k1gInSc1
→	→	k?
7	#num#	k4
<g/>
-methylguanin	-methylguanin	k2eAgMnSc1d1
•	•	k?
Uracil	Uracil	k1gMnSc1
→	→	k?
Thymin	Thymin	k1gInSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
-methyluracil	-methyluracila	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Dihydrouridin	Dihydrouridin	k2eAgInSc1d1
V	v	k7c6
DNA	DNA	kA
</s>
<s>
Adenin	adenin	k1gInSc1
→	→	k?
N	N	kA
<g/>
6	#num#	k4
<g/>
-methyladenin	-methyladenin	k2eAgMnSc1d1
•	•	k?
Thymin	Thymin	k1gInSc1
→	→	k?
báze	báze	k1gFnSc2
J	J	kA
•	•	k?
Cytosin	Cytosin	k1gInSc1
→	→	k?
4	#num#	k4
<g/>
-methylcytosin	-methylcytosina	k1gFnPc2
<g/>
,	,	kIx,
5	#num#	k4
<g/>
-methylcytosin	-methylcytosina	k1gFnPc2
<g/>
,	,	kIx,
5	#num#	k4
<g/>
-hydroxymethylcytosin	-hydroxymethylcytosina	k1gFnPc2
<g/>
,	,	kIx,
5	#num#	k4
<g/>
-formylcytosin	-formylcytosina	k1gFnPc2
<g/>
,	,	kIx,
5	#num#	k4
<g/>
-karboxylcytosin	-karboxylcytosina	k1gFnPc2
</s>
<s>
Nukleosidy	Nukleosida	k1gFnPc1
</s>
<s>
Adenosin	Adenosin	k2eAgInSc1d1
•	•	k?
Uridin	Uridin	k2eAgInSc1d1
•	•	k?
Guanosin	Guanosin	k1gInSc1
•	•	k?
Cytidin	Cytidin	k1gInSc1
•	•	k?
modifikované	modifikovaný	k2eAgInPc1d1
<g/>
:	:	kIx,
Pseudouridin	Pseudouridin	k1gInSc1
<g/>
,	,	kIx,
Ribothymidin	Ribothymidin	k1gInSc1
<g/>
,	,	kIx,
Inosin	Inosin	k1gInSc1
(	(	kIx(
<g/>
jako	jako	k8xC,k8xS
bázi	báze	k1gFnSc6
obsahuje	obsahovat	k5eAaImIp3nS
hypoxantin	hypoxantin	k1gInSc1
<g/>
)	)	kIx)
Deoxynukleosidy	Deoxynukleosida	k1gFnPc1
</s>
<s>
Deoxyadenosin	Deoxyadenosin	k1gMnSc1
•	•	k?
Deoxyguanosin	Deoxyguanosin	k1gMnSc1
•	•	k?
Deoxycytidin	Deoxycytidin	k1gInSc1
•	•	k?
Deoxythymidin	Deoxythymidin	k1gInSc1
•	•	k?
modifikované	modifikovaný	k2eAgInPc1d1
<g/>
:	:	kIx,
Deoxyuridin	Deoxyuridin	k1gInSc1
Ribonukleotidy	Ribonukleotida	k1gFnSc2
</s>
<s>
AMP	AMP	kA
(	(	kIx(
<g/>
cAMP	camp	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
UMP	UMP	kA
•	•	k?
GMP	GMP	kA
(	(	kIx(
<g/>
cGMP	cGMP	k?
<g/>
)	)	kIx)
•	•	k?
CMP	CMP	kA
•	•	k?
ADP	ADP	kA
•	•	k?
UDP	UDP	kA
(	(	kIx(
<g/>
m	m	kA
<g/>
5	#num#	k4
<g/>
UDP	UDP	kA
<g/>
)	)	kIx)
•	•	k?
GDP	GDP	kA
•	•	k?
CDP	CDP	kA
•	•	k?
ATP	atp	kA
•	•	k?
UTP	UTP	kA
•	•	k?
GTP	GTP	kA
•	•	k?
CTP	CTP	kA
Deoxyribonukleotidy	Deoxyribonukleotida	k1gFnSc2
</s>
<s>
dAMP	dAMP	k?
•	•	k?
dTMP	dTMP	k?
•	•	k?
dUMP	dUMP	k?
•	•	k?
dGMP	dGMP	k?
•	•	k?
dCMP	dCMP	k?
•	•	k?
dADP	dADP	k?
•	•	k?
dTDP	dTDP	k?
•	•	k?
dUDP	dUDP	k?
•	•	k?
dGDP	dGDP	k?
•	•	k?
dCDP	dCDP	k?
•	•	k?
dATP	dATP	k?
•	•	k?
dTTP	dTTP	k?
•	•	k?
dUTP	dUTP	k?
(	(	kIx(
<g/>
m	m	kA
<g/>
5	#num#	k4
<g/>
UTP	UTP	kA
<g/>
)	)	kIx)
•	•	k?
dGTP	dGTP	k?
•	•	k?
dCTP	dCTP	k?
Analogy	analog	k1gInPc7
nukleových	nukleový	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
</s>
<s>
GNA	GNA	kA
•	•	k?
LNA	lnout	k5eAaImSgInS
•	•	k?
PNA	pnout	k5eAaImSgInS
•	•	k?
TNA	tnout	k5eAaBmSgInS
•	•	k?
morpholino	morpholin	k2eAgNnSc1d1
•	•	k?
syntetické	syntetický	k2eAgInPc4d1
purinové	purinový	k2eAgInPc4d1
analogy	analog	k1gInPc4
•	•	k?
syntetické	syntetický	k2eAgInPc4d1
pyrimidinové	pyrimidinový	k2eAgInPc4d1
analogy	analog	k1gInPc4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4070512-2	4070512-2	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
6247	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85037008	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85037008	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
