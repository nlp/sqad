<s>
Modré	modrý	k2eAgInPc1d1	modrý
límečky	límeček	k1gInPc1	límeček
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
označení	označení	k1gNnSc1	označení
dělníků	dělník	k1gMnPc2	dělník
-	-	kIx~	-
proti	proti	k7c3	proti
bílým	bílý	k2eAgInPc3d1	bílý
límečkům	límeček	k1gInPc3	límeček
úředníků	úředník	k1gMnPc2	úředník
Modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
obecně	obecně	k6eAd1	obecně
označuje	označovat	k5eAaImIp3nS	označovat
muže	muž	k1gMnPc4	muž
či	či	k8xC	či
chlapce	chlapec	k1gMnPc4	chlapec
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
růžové	růžový	k2eAgFnSc3d1	růžová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
označuje	označovat	k5eAaImIp3nS	označovat
ženy	žena	k1gFnPc1	žena
a	a	k8xC	a
dívky	dívka	k1gFnPc1	dívka
<g/>
.	.	kIx.	.
</s>
