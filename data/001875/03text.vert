<s>
Mem	Mem	k?	Mem
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
s	s	k7c7	s
verzí	verze	k1gFnSc7	verze
mém	můj	k3xOp1gMnSc6	můj
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
termín	termín	k1gInSc4	termín
pro	pro	k7c4	pro
kulturní	kulturní	k2eAgFnSc4d1	kulturní
obdobu	obdoba	k1gFnSc4	obdoba
genu	gen	k1gInSc2	gen
–	–	k?	–
replikující	replikující	k2eAgFnSc4d1	replikující
se	se	k3xPyFc4	se
jednotku	jednotka	k1gFnSc4	jednotka
kulturní	kulturní	k2eAgFnPc4d1	kulturní
informace	informace	k1gFnPc4	informace
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
termín	termín	k1gInSc1	termín
poprvé	poprvé	k6eAd1	poprvé
použil	použít	k5eAaPmAgInS	použít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Sobecký	sobecký	k2eAgInSc4d1	sobecký
gen	gen	k1gInSc4	gen
Richard	Richarda	k1gFnPc2	Richarda
Dawkins	Dawkinsa	k1gFnPc2	Dawkinsa
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
mem	mem	k?	mem
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
mimema	mimemum	k1gNnSc2	mimemum
–	–	k?	–
napodobovat	napodobovat	k5eAaImF	napodobovat
<g/>
.	.	kIx.	.
</s>
<s>
Evoluce	evoluce	k1gFnSc1	evoluce
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
nutně	nutně	k6eAd1	nutně
postavena	postavit	k5eAaPmNgNnP	postavit
jen	jen	k6eAd1	jen
na	na	k7c6	na
replikaci	replikace	k1gFnSc6	replikace
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejím	její	k3xOp3gInSc7	její
principem	princip	k1gInSc7	princip
je	být	k5eAaImIp3nS	být
šíření	šíření	k1gNnSc1	šíření
jakékoli	jakýkoli	k3yIgFnSc2	jakýkoli
informace	informace	k1gFnSc2	informace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
podléhá	podléhat	k5eAaImIp3nS	podléhat
změnám	změna	k1gFnPc3	změna
(	(	kIx(	(
<g/>
mutacím	mutace	k1gFnPc3	mutace
<g/>
)	)	kIx)	)
a	a	k8xC	a
selekčnímu	selekční	k2eAgInSc3d1	selekční
tlaku	tlak	k1gInSc3	tlak
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Memy	Mema	k1gFnPc1	Mema
se	se	k3xPyFc4	se
rozšiřují	rozšiřovat	k5eAaImIp3nP	rozšiřovat
jak	jak	k6eAd1	jak
z	z	k7c2	z
generace	generace	k1gFnSc2	generace
na	na	k7c4	na
generaci	generace	k1gFnSc4	generace
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
i	i	k9	i
komunikací	komunikace	k1gFnPc2	komunikace
s	s	k7c7	s
nepříbuzným	příbuzný	k2eNgNnSc7d1	nepříbuzné
okolím	okolí	k1gNnSc7	okolí
(	(	kIx(	(
<g/>
tato	tento	k3xDgFnSc1	tento
teorie	teorie	k1gFnSc1	teorie
předávání	předávání	k1gNnSc2	předávání
memů	mem	k1gInPc2	mem
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
souhlasí	souhlasit	k5eAaImIp3nS	souhlasit
s	s	k7c7	s
pojetím	pojetí	k1gNnSc7	pojetí
evoluce	evoluce	k1gFnSc2	evoluce
dle	dle	k7c2	dle
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
Lamarcka	Lamarcka	k1gFnSc1	Lamarcka
-	-	kIx~	-
lamarckismem	lamarckismus	k1gInSc7	lamarckismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
memů	mem	k1gInPc2	mem
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
kulturních	kulturní	k2eAgFnPc2d1	kulturní
dědičných	dědičný	k2eAgFnPc2d1	dědičná
informací	informace	k1gFnPc2	informace
v	v	k7c6	v
lidských	lidský	k2eAgFnPc6d1	lidská
mozkových	mozkový	k2eAgFnPc6d1	mozková
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
teorií	teorie	k1gFnPc2	teorie
i	i	k9	i
rostlinných	rostlinný	k2eAgFnPc6d1	rostlinná
a	a	k8xC	a
zvířecích	zvířecí	k2eAgFnPc6d1	zvířecí
<g/>
)	)	kIx)	)
buňkách	buňka	k1gFnPc6	buňka
je	být	k5eAaImIp3nS	být
evoluční	evoluční	k2eAgFnSc4d1	evoluční
psychologii	psychologie	k1gFnSc4	psychologie
kritizována	kritizován	k2eAgFnSc1d1	kritizována
jako	jako	k8xS	jako
pseudověda	pseudověda	k1gFnSc1	pseudověda
<g/>
.	.	kIx.	.
</s>
<s>
Susan	Susan	k1gMnSc1	Susan
Blackmoreová	Blackmoreová	k1gFnSc1	Blackmoreová
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
The	The	k1gFnSc2	The
meme	meme	k6eAd1	meme
machine	machinout	k5eAaPmIp3nS	machinout
(	(	kIx(	(
<g/>
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
vyšla	vyjít	k5eAaPmAgFnS	vyjít
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Teorie	teorie	k1gFnSc1	teorie
memů	mem	k1gInPc2	mem
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
memy	mema	k1gFnPc4	mema
popisuje	popisovat	k5eAaImIp3nS	popisovat
jako	jako	k9	jako
neovladatelné	ovladatelný	k2eNgNnSc1d1	neovladatelné
a	a	k8xC	a
neumlčitelné	umlčitelný	k2eNgNnSc1d1	neumlčitelné
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
memetickou	memetický	k2eAgFnSc7d1	memetická
evolucí	evoluce	k1gFnSc7	evoluce
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
uniknout	uniknout	k5eAaPmF	uniknout
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
i	i	k9	i
náš	náš	k3xOp1gInSc1	náš
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
obraz	obraz	k1gInSc1	obraz
sebe	sebe	k3xPyFc4	sebe
sama	sám	k3xTgMnSc4	sám
je	být	k5eAaImIp3nS	být
dle	dle	k7c2	dle
ní	on	k3xPp3gFnSc7	on
pouhou	pouhý	k2eAgFnSc7d1	pouhá
vítěznou	vítězný	k2eAgFnSc7d1	vítězná
skupinou	skupina	k1gFnSc7	skupina
memů	mem	k1gInPc2	mem
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
nás	my	k3xPp1nPc4	my
momentálně	momentálně	k6eAd1	momentálně
ovládají	ovládat	k5eAaImIp3nP	ovládat
<g/>
.	.	kIx.	.
</s>
<s>
Jiný	jiný	k2eAgInSc1d1	jiný
výraz	výraz	k1gInSc1	výraz
pro	pro	k7c4	pro
mem	mem	k?	mem
je	být	k5eAaImIp3nS	být
kulturgen	kulturgen	k1gInSc4	kulturgen
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
příliš	příliš	k6eAd1	příliš
neujal	ujmout	k5eNaPmAgMnS	ujmout
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
memy	mema	k1gFnPc1	mema
jsou	být	k5eAaImIp3nP	být
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
básnička	básnička	k1gFnSc1	básnička
<g/>
,	,	kIx,	,
recept	recept	k1gInSc1	recept
či	či	k8xC	či
melodie	melodie	k1gFnPc1	melodie
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
(	(	kIx(	(
<g/>
těm	ten	k3xDgMnPc3	ten
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
říká	říkat	k5eAaImIp3nS	říkat
memplex	memplex	k1gInSc1	memplex
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
komplikovanější	komplikovaný	k2eAgFnPc1d2	komplikovanější
–	–	k?	–
třeba	třeba	k6eAd1	třeba
náboženská	náboženský	k2eAgFnSc1d1	náboženská
víra	víra	k1gFnSc1	víra
nebo	nebo	k8xC	nebo
politické	politický	k2eAgNnSc1d1	politické
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
<g/>
.	.	kIx.	.
</s>
<s>
Memem	Memem	k6eAd1	Memem
je	být	k5eAaImIp3nS	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
i	i	k9	i
samotná	samotný	k2eAgFnSc1d1	samotná
teorie	teorie	k1gFnSc1	teorie
memů	mem	k1gMnPc2	mem
<g/>
.	.	kIx.	.
</s>
<s>
Memy	Mema	k1gFnPc1	Mema
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nP	šířit
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
účelnost	účelnost	k1gFnSc4	účelnost
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
v	v	k7c6	v
nejrůznější	různý	k2eAgFnSc6d3	nejrůznější
škále	škála	k1gFnSc6	škála
od	od	k7c2	od
výhodných	výhodný	k2eAgFnPc2d1	výhodná
<g/>
,	,	kIx,	,
naprosto	naprosto	k6eAd1	naprosto
neškodných	škodný	k2eNgFnPc6d1	neškodná
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
neutrální	neutrální	k2eAgFnSc4d1	neutrální
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c6	po
škodlivé	škodlivý	k2eAgFnSc6d1	škodlivá
(	(	kIx(	(
<g/>
přičemž	přičemž	k6eAd1	přičemž
hodnocení	hodnocení	k1gNnSc1	hodnocení
škodlivosti	škodlivost	k1gFnSc2	škodlivost
nebo	nebo	k8xC	nebo
užitečnosti	užitečnost	k1gFnSc2	užitečnost
určitých	určitý	k2eAgInPc2d1	určitý
jevů	jev	k1gInPc2	jev
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
memem	memem	k6eAd1	memem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
memy	mema	k1gFnPc1	mema
typu	typ	k1gInSc2	typ
kuřáctví	kuřáctví	k1gNnSc2	kuřáctví
<g/>
,	,	kIx,	,
závislost	závislost	k1gFnSc1	závislost
na	na	k7c6	na
drogách	droga	k1gFnPc6	droga
<g/>
,	,	kIx,	,
nápodoba	nápodoba	k1gFnSc1	nápodoba
destruktivního	destruktivní	k2eAgNnSc2d1	destruktivní
chování	chování	k1gNnSc2	chování
–	–	k?	–
násilí	násilí	k1gNnSc2	násilí
<g/>
,	,	kIx,	,
sebevražda	sebevražda	k1gFnSc1	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Memetikové	Memetik	k1gMnPc1	Memetik
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
popisují	popisovat	k5eAaImIp3nP	popisovat
dva	dva	k4xCgInPc4	dva
způsoby	způsob	k1gInPc4	způsob
šíření	šíření	k1gNnSc2	šíření
<g/>
.	.	kIx.	.
</s>
<s>
Jednak	jednak	k8xC	jednak
vertikální	vertikální	k2eAgInSc1d1	vertikální
(	(	kIx(	(
<g/>
mezigenerační	mezigenerační	k2eAgInSc1d1	mezigenerační
<g/>
)	)	kIx)	)
přenos	přenos	k1gInSc1	přenos
stylem	styl	k1gInSc7	styl
prarodiče	prarodič	k1gMnSc2	prarodič
-	-	kIx~	-
rodiče	rodič	k1gMnSc2	rodič
-	-	kIx~	-
děti	dítě	k1gFnPc1	dítě
-	-	kIx~	-
jejich	jejich	k3xOp3gMnPc1	jejich
potomci	potomek	k1gMnPc1	potomek
-	-	kIx~	-
...	...	k?	...
atd.	atd.	kA	atd.
Především	především	k6eAd1	především
se	se	k3xPyFc4	se
takto	takto	k6eAd1	takto
učíme	učit	k5eAaImIp1nP	učit
základním	základní	k2eAgInPc3d1	základní
vzorcům	vzorec	k1gInPc3	vzorec
chování	chování	k1gNnSc2	chování
<g/>
,	,	kIx,	,
získáváme	získávat	k5eAaImIp1nP	získávat
též	též	k9	též
nové	nový	k2eAgFnPc1d1	nová
dovednosti	dovednost	k1gFnPc1	dovednost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
babička	babička	k1gFnSc1	babička
naučí	naučit	k5eAaPmIp3nS	naučit
svou	svůj	k3xOyFgFnSc4	svůj
vnučku	vnučka	k1gFnSc4	vnučka
plést	plést	k5eAaImF	plést
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vertikálním	vertikální	k2eAgInSc6d1	vertikální
přenosu	přenos	k1gInSc6	přenos
se	se	k3xPyFc4	se
memy	mema	k1gFnPc1	mema
šíří	šířit	k5eAaImIp3nP	šířit
s	s	k7c7	s
geny	gen	k1gInPc7	gen
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
konstatovat	konstatovat	k5eAaBmF	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
prospívá	prospívat	k5eAaImIp3nS	prospívat
genům	gen	k1gInPc3	gen
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
prospívá	prospívat	k5eAaImIp3nS	prospívat
i	i	k9	i
memům	mem	k1gMnPc3	mem
<g/>
.	.	kIx.	.
</s>
<s>
Geny	gen	k1gInPc1	gen
a	a	k8xC	a
memy	mema	k1gFnSc2	mema
tedy	tedy	k8xC	tedy
jistým	jistý	k2eAgInSc7d1	jistý
způsobem	způsob	k1gInSc7	způsob
"	"	kIx"	"
<g/>
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Horizontálním	horizontální	k2eAgInSc7d1	horizontální
procesem	proces	k1gInSc7	proces
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
myšleno	myslet	k5eAaImNgNnS	myslet
předávání	předávání	k1gNnSc1	předávání
memů	mem	k1gInPc2	mem
mezi	mezi	k7c7	mezi
vrstevníky	vrstevník	k1gMnPc7	vrstevník
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
rodič	rodič	k1gMnSc1	rodič
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
snaží	snažit	k5eAaImIp3nS	snažit
svého	svůj	k3xOyFgMnSc4	svůj
potomka	potomek	k1gMnSc4	potomek
naučit	naučit	k5eAaPmF	naučit
adaptivním	adaptivní	k2eAgFnPc3d1	adaptivní
formám	forma	k1gFnPc3	forma
a	a	k8xC	a
vzorcům	vzorec	k1gInPc3	vzorec
chování	chování	k1gNnSc2	chování
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
příslušníky	příslušník	k1gMnPc7	příslušník
stejné	stejný	k2eAgFnSc2d1	stejná
generace	generace	k1gFnSc2	generace
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
od	od	k7c2	od
nerodičovských	rodičovský	k2eNgFnPc2d1	nerodičovská
autorit	autorita	k1gFnPc2	autorita
<g/>
,	,	kIx,	,
jsme	být	k5eAaImIp1nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
často	často	k6eAd1	často
pochytit	pochytit	k5eAaPmF	pochytit
memy	memy	k1gInPc4	memy
i	i	k9	i
prokazatelně	prokazatelně	k6eAd1	prokazatelně
škodlivé	škodlivý	k2eAgNnSc4d1	škodlivé
(	(	kIx(	(
<g/>
vykořisťování	vykořisťování	k1gNnSc4	vykořisťování
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
sobecké	sobecký	k2eAgInPc1d1	sobecký
geny	gen	k1gInPc1	gen
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
i	i	k9	i
memy	mema	k1gFnPc1	mema
vzájemně	vzájemně	k6eAd1	vzájemně
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
mem	mem	k?	mem
liberální	liberální	k2eAgFnSc1d1	liberální
demokracie	demokracie	k1gFnSc1	demokracie
s	s	k7c7	s
memem	memum	k1gNnSc7	memum
sekularismu	sekularismus	k1gInSc2	sekularismus
<g/>
)	)	kIx)	)
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
spolu	spolu	k6eAd1	spolu
soupeřit	soupeřit	k5eAaImF	soupeřit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
spolupráce	spolupráce	k1gFnSc2	spolupráce
se	se	k3xPyFc4	se
sdružují	sdružovat	k5eAaImIp3nP	sdružovat
v	v	k7c4	v
memplexy	memplex	k1gInPc4	memplex
<g/>
.	.	kIx.	.
</s>
<s>
Memplex	Memplex	k1gInSc1	Memplex
je	být	k5eAaImIp3nS	být
seskupením	seskupení	k1gNnSc7	seskupení
memů	mem	k1gInPc2	mem
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
se	se	k3xPyFc4	se
množí	množit	k5eAaImIp3nP	množit
většinou	většinou	k6eAd1	většinou
společně	společně	k6eAd1	společně
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
výhodnější	výhodný	k2eAgMnSc1d2	výhodnější
a	a	k8xC	a
efektivnější	efektivní	k2eAgMnSc1d2	efektivnější
<g/>
.	.	kIx.	.
</s>
<s>
Vysoce	vysoce	k6eAd1	vysoce
komplexními	komplexní	k2eAgInPc7d1	komplexní
memplexy	memplex	k1gInPc7	memplex
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
různé	různý	k2eAgFnPc4d1	různá
náboženské	náboženský	k2eAgFnPc4d1	náboženská
nauky	nauka	k1gFnPc4	nauka
či	či	k8xC	či
vědecké	vědecký	k2eAgFnPc4d1	vědecká
teorie	teorie	k1gFnPc4	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
kritiky	kritik	k1gMnPc7	kritik
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
mexický	mexický	k2eAgMnSc1d1	mexický
vědec	vědec	k1gMnSc1	vědec
Luis	Luisa	k1gFnPc2	Luisa
Benítez-Bribiesca	Benítez-Bribiesca	k1gMnSc1	Benítez-Bribiesca
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
toto	tento	k3xDgNnSc4	tento
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
pseudovědu	pseudověda	k1gFnSc4	pseudověda
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
například	například	k6eAd1	například
biolog	biolog	k1gMnSc1	biolog
Ernst	Ernst	k1gMnSc1	Ernst
Mayr	Mayr	k1gMnSc1	Mayr
<g/>
.	.	kIx.	.
</s>
<s>
Internetový	internetový	k2eAgInSc1d1	internetový
mem	mem	k?	mem
</s>
