<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
Raja	Raj	k2eAgFnSc1d1
jóga	jóga	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
shrnuje	shrnovat	k5eAaImIp3nS
osm	osm	k4xCc4
stupňů	stupeň	k1gInPc2
jógy	jóga	k1gFnSc2
do	do	k7c2
praktikování	praktikování	k1gNnSc2
jediné	jediný	k2eAgFnSc2d1
formy	forma	k1gFnSc2
jógy	jóga	k1gFnSc2
a	a	k8xC
vede	vést	k5eAaImIp3nS
ke	k	k7c3
znamenitosti	znamenitost	k1gFnSc3
a	a	k8xC
k	k	k7c3
extázi	extáze	k1gFnSc3
<g/>
.	.	kIx.
</s>