<s>
Kundalini	Kundalin	k2eAgMnPc1d1
jóga	jóga	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Kundalini	Kundalin	k2eAgMnPc1d1
jóga	jóga	k1gFnSc1
je	být	k5eAaImIp3nS
podle	podle	k7c2
Yogi	Yog	k1gFnSc2
Bhajana	Bhajana	k1gFnSc1
jogínská	jogínský	k2eAgFnSc1d1
tradice	tradice	k1gFnSc1
kombinující	kombinující	k2eAgFnSc4d1
meditaci	meditace	k1gFnSc4
<g/>
,	,	kIx,
mantru	mantra	k1gFnSc4
<g/>
,	,	kIx,
fyzické	fyzický	k2eAgNnSc4d1
cvičení	cvičení	k1gNnSc4
a	a	k8xC
dýchací	dýchací	k2eAgFnPc4d1
techniky	technika	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
Raja	Raj	k2eAgFnSc1d1
jóga	jóga	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
shrnuje	shrnovat	k5eAaImIp3nS
osm	osm	k4xCc4
stupňů	stupeň	k1gInPc2
jógy	jóga	k1gFnSc2
do	do	k7c2
praktikování	praktikování	k1gNnSc2
jediné	jediný	k2eAgFnSc2d1
formy	forma	k1gFnSc2
jógy	jóga	k1gFnSc2
a	a	k8xC
vede	vést	k5eAaImIp3nS
ke	k	k7c3
znamenitosti	znamenitost	k1gFnSc3
a	a	k8xC
k	k	k7c3
extázi	extáze	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kundalini	Kundalin	k2eAgMnPc5d1
doslovně	doslovně	k6eAd1
znamená	znamenat	k5eAaImIp3nS
„	„	k?
<g/>
lokna	lokna	k1gFnSc1
na	na	k7c6
pramínku	pramínek	k1gInSc6
vlasů	vlas	k1gInPc2
milého	milý	k1gMnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Metafora	metafora	k1gFnSc1
se	se	k3xPyFc4
vztahuje	vztahovat	k5eAaImIp3nS
k	k	k7c3
toku	tok	k1gInSc3
energie	energie	k1gFnSc2
a	a	k8xC
vědomí	vědomí	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
existuje	existovat	k5eAaImIp3nS
v	v	k7c6
každém	každý	k3xTgInSc6
z	z	k7c2
nás	my	k3xPp1nPc2
a	a	k8xC
které	který	k3yIgNnSc4,k3yRgNnSc4,k3yQgNnSc4
nám	my	k3xPp1nPc3
umožňuje	umožňovat	k5eAaImIp3nS
splynout	splynout	k5eAaPmF
s	s	k7c7
vesmírným	vesmírný	k2eAgNnSc7d1
vědomím	vědomí	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojení	spojení	k1gNnSc1
individuálního	individuální	k2eAgNnSc2d1
a	a	k8xC
vesmírného	vesmírný	k2eAgNnSc2d1
vědomí	vědomí	k1gNnSc2
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
božskou	božský	k2eAgFnSc4d1
jednotu	jednota	k1gFnSc4
<g/>
,	,	kIx,
zvanou	zvaný	k2eAgFnSc4d1
jóga	jóga	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
tisíce	tisíc	k4xCgInPc4
let	léto	k1gNnPc2
byla	být	k5eAaImAgFnS
tato	tento	k3xDgFnSc1
posvátná	posvátný	k2eAgFnSc1d1
věda	věda	k1gFnSc1
a	a	k8xC
technologie	technologie	k1gFnSc1
uchovávána	uchovávat	k5eAaImNgFnS
v	v	k7c6
tajnosti	tajnost	k1gFnSc6
a	a	k8xC
předávána	předávat	k5eAaImNgFnS
ústně	ústně	k6eAd1
od	od	k7c2
mistra	mistr	k1gMnSc2
k	k	k7c3
vybranému	vybraný	k2eAgNnSc3d1
učedníkovi	učedníkův	k2eAgMnPc1d1
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1969	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
ji	on	k3xPp3gFnSc4
Yogi	Yoge	k1gFnSc4
Bhajan	Bhajan	k1gMnSc1
<g/>
,	,	kIx,
mistr	mistr	k1gMnSc1
Kundalini	Kundalin	k2eAgMnPc1d1
jógy	jóga	k1gFnSc2
<g/>
,	,	kIx,
začal	začít	k5eAaPmAgInS
vyučovat	vyučovat	k5eAaImF
veřejně	veřejně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Jóga	jóga	k1gFnSc1
vědomí	vědomí	k1gNnSc2
</s>
<s>
Kundalini	Kundalin	k2eAgMnPc1d1
jóga	jóga	k1gFnSc1
je	být	k5eAaImIp3nS
take	take	k6eAd1
známá	známý	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
Jóga	jóga	k1gFnSc1
vědomí	vědomí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaměřuje	zaměřovat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
povědomí	povědomí	k1gNnSc6
o	o	k7c6
sobě	se	k3xPyFc3
sama	sám	k3xTgFnSc1
a	a	k8xC
na	na	k7c4
zážitek	zážitek	k1gInSc4
našeho	náš	k3xOp1gNnSc2
nejvyššího	vysoký	k2eAgNnSc2d3
vědomí	vědomí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technologie	technologie	k1gFnSc1
Kundalini	Kundalin	k2eAgMnPc1d1
jógy	jóg	k1gInPc7
podle	podle	k7c2
Yogi	Yog	k1gFnSc2
Bhajana	Bhajan	k1gMnSc2
je	být	k5eAaImIp3nS
věda	věda	k1gFnSc1
o	o	k7c6
mysli	mysl	k1gFnSc6
a	a	k8xC
těle	tělo	k1gNnSc6
a	a	k8xC
vede	vést	k5eAaImIp3nS
k	k	k7c3
pozvednutí	pozvednutí	k1gNnSc3
ducha	duch	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
nemá	mít	k5eNaImIp3nS
hranice	hranice	k1gFnPc4
a	a	k8xC
je	být	k5eAaImIp3nS
neomezený	omezený	k2eNgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
je	být	k5eAaImIp3nS
tato	tento	k3xDgFnSc1
jóga	jóga	k1gFnSc1
pro	pro	k7c4
každého	každý	k3xTgMnSc4
bez	bez	k7c2
rozdílu	rozdíl	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
starodávné	starodávný	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
jógy	jóga	k1gFnSc2
je	být	k5eAaImIp3nS
Kundalini	Kundalin	k2eAgMnPc1d1
jóga	jóga	k1gFnSc1
podle	podle	k7c2
Yogi	Yog	k1gFnSc2
Bhajana	Bhajana	k1gFnSc1
cestou	cestou	k7c2
normálních	normální	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
tj.	tj.	kA
vždy	vždy	k6eAd1
byla	být	k5eAaImAgFnS
praktikována	praktikovat	k5eAaImNgFnS
lidmi	člověk	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
měli	mít	k5eAaImAgMnP
rodiny	rodina	k1gFnPc4
a	a	k8xC
pracovali	pracovat	k5eAaImAgMnP
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
obvyklé	obvyklý	k2eAgFnSc2d1
cesty	cesta	k1gFnSc2
jogína	jogín	k1gMnSc2
<g/>
,	,	kIx,
tzn.	tzn.	kA
vzdání	vzdání	k1gNnSc1
se	se	k3xPyFc4
světských	světský	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
<g/>
,	,	kIx,
celibát	celibát	k1gInSc4
a	a	k8xC
odchod	odchod	k1gInSc4
ze	z	k7c2
společnosti	společnost	k1gFnSc2
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Cíl	cíl	k1gInSc1
kundalini	kundalin	k2eAgMnPc1d1
jógy	jóga	k1gFnSc2
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1
cílem	cíl	k1gInSc7
Kundalini	Kundalin	k2eAgMnPc1d1
jógy	jóga	k1gFnPc4
je	být	k5eAaImIp3nS
probudit	probudit	k5eAaPmF
plný	plný	k2eAgInSc4d1
potencial	potencial	k1gInSc4
lidského	lidský	k2eAgNnSc2d1
vědomí	vědomí	k1gNnSc2
v	v	k7c6
každém	každý	k3xTgMnSc6
jedinci	jedinec	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
rozpoznat	rozpoznat	k5eAaPmF
naše	náš	k3xOp1gNnSc4
vědomí	vědomí	k1gNnSc4
<g/>
,	,	kIx,
vyladit	vyladit	k5eAaPmF
ho	on	k3xPp3gMnSc4
a	a	k8xC
rozšířit	rozšířit	k5eAaPmF
ho	on	k3xPp3gMnSc4
do	do	k7c2
neomezeného	omezený	k2eNgNnSc2d1
vědomí	vědomí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odstranit	odstranit	k5eAaPmF
veškerou	veškerý	k3xTgFnSc4
vnitřní	vnitřní	k2eAgFnSc4d1
dualitu	dualita	k1gFnSc4
<g/>
,	,	kIx,
vytvořit	vytvořit	k5eAaPmF
schopnost	schopnost	k1gFnSc4
hluboce	hluboko	k6eAd1
naslouchat	naslouchat	k5eAaImF
<g/>
,	,	kIx,
kultivovat	kultivovat	k5eAaImF
vnitřní	vnitřní	k2eAgNnSc4d1
ticho	ticho	k1gNnSc4
a	a	k8xC
dosáhnout	dosáhnout	k5eAaPmF
prosperity	prosperita	k1gFnSc2
a	a	k8xC
výtečnosti	výtečnost	k1gFnSc2
ve	v	k7c6
všem	všecek	k3xTgNnSc6
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
děláme	dělat	k5eAaImIp1nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kundalini	Kundalin	k2eAgMnPc5d1
jóga	jóga	k1gFnSc1
dle	dle	k7c2
Yogi	Yogi	k1gNnSc2
Bhajana	Bhajana	k1gFnSc1
se	se	k3xPyFc4
zaměřuje	zaměřovat	k5eAaImIp3nS
na	na	k7c4
osobní	osobní	k2eAgFnSc4d1
zkušenost	zkušenost	k1gFnSc4
a	a	k8xC
vědomí	vědomí	k1gNnSc4
jedince	jedinec	k1gMnSc2
při	při	k7c6
praktikování	praktikování	k1gNnSc6
krijí	krijí	k1gNnPc2
–	–	k?
setu	set	k1gInSc6
cviků	cvik	k1gInPc2
<g/>
,	,	kIx,
zvuků	zvuk	k1gInPc2
<g/>
,	,	kIx,
muder	mudra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Probouzíme	probouzet	k5eAaImIp1nP
kundalini	kundalin	k2eAgMnPc1d1
<g/>
,	,	kIx,
abychom	aby	kYmCp1nP
byli	být	k5eAaImAgMnP
schopni	schopen	k2eAgMnPc1d1
dosáhnout	dosáhnout	k5eAaPmF
plného	plný	k2eAgInSc2d1
potenciálu	potenciál	k1gInSc2
našeho	náš	k3xOp1gInSc2
nervového	nervový	k2eAgInSc2d1
a	a	k8xC
žlázového	žlázový	k2eAgInSc2d1
systému	systém	k1gInSc2
a	a	k8xC
vyvážit	vyvážit	k5eAaPmF
subtilní	subtilní	k2eAgInSc4d1
systém	systém	k1gInSc4
čaker	čakra	k1gFnPc2
a	a	k8xC
meridiánů	meridián	k1gInPc2
v	v	k7c6
těle	tělo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krija	Krij	k1gInSc2
je	být	k5eAaImIp3nS
sladěný	sladěný	k2eAgInSc1d1
systém	systém	k1gInSc1
skládající	skládající	k2eAgFnSc2d1
se	se	k3xPyFc4
z	z	k7c2
pohybů	pohyb	k1gInPc2
<g/>
,	,	kIx,
zvuku	zvuk	k1gInSc2
<g/>
,	,	kIx,
pranayamu	pranayam	k1gInSc2
<g/>
,	,	kIx,
muder	mudra	k1gFnPc2
<g/>
,	,	kIx,
soustředění	soustředění	k1gNnSc1
se	se	k3xPyFc4
a	a	k8xC
meditace	meditace	k1gFnSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
automaticky	automaticky	k6eAd1
vede	vést	k5eAaImIp3nS
energie	energie	k1gFnSc1
v	v	k7c6
těle	tělo	k1gNnSc6
a	a	k8xC
mysli	mysl	k1gFnSc6
ke	k	k7c3
specifickému	specifický	k2eAgInSc3d1
výsledku	výsledek	k1gInSc3
neboli	neboli	k8xC
změně	změna	k1gFnSc3
vědomí	vědomí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Kundalini	Kundalin	k2eAgMnPc1d1
jóga	jóga	k1gFnSc1
podle	podle	k7c2
Yogi	Yog	k1gFnSc2
Bhajana	Bhajan	k1gMnSc2
nespoléhá	spoléhat	k5eNaImIp3nS
na	na	k7c4
žádnou	žádný	k3yNgFnSc4
z	z	k7c2
těchto	tento	k3xDgFnPc2
technik	technika	k1gFnPc2
samu	sám	k3xTgFnSc4
o	o	k7c4
sobě	se	k3xPyFc3
<g/>
,	,	kIx,
i	i	k9
když	když	k8xS
jich	on	k3xPp3gMnPc2
hodně	hodně	k6eAd1
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
jedinečný	jedinečný	k2eAgInSc1d1
a	a	k8xC
otestovaný	otestovaný	k2eAgInSc1d1
systém	systém	k1gInSc1
v	v	k7c6
rámci	rámec	k1gInSc6
struktury	struktura	k1gFnSc2
každé	každý	k3xTgFnSc2
kriyi	kriy	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
nám	my	k3xPp1nPc3
Yogi	Yogi	k1gNnSc1
Bhajan	Bhajana	k1gFnPc2
předal	předat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
systém	systém	k1gInSc1
nás	my	k3xPp1nPc4
vede	vést	k5eAaImIp3nS
k	k	k7c3
neustálému	neustálý	k2eAgMnSc3d1
a	a	k8xC
předvídatelnému	předvídatelný	k2eAgInSc3d1
pokroku	pokrok	k1gInSc3
a	a	k8xC
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
přitom	přitom	k6eAd1
základní	základní	k2eAgFnPc4d1
funkce	funkce	k1gFnPc4
těla	tělo	k1gNnSc2
a	a	k8xC
mysli	mysl	k1gFnSc2
<g/>
,	,	kIx,
k	k	k7c3
vytvoření	vytvoření	k1gNnSc3
rychlého	rychlý	k2eAgInSc2d1
a	a	k8xC
udržitelného	udržitelný	k2eAgInSc2d1
osobního	osobní	k2eAgInSc2d1
růstu	růst	k1gInSc2
a	a	k8xC
hojení	hojení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
tradici	tradice	k1gFnSc6
není	být	k5eNaImIp3nS
meditace	meditace	k1gFnSc1
považována	považován	k2eAgFnSc1d1
za	za	k7c4
oddělenou	oddělený	k2eAgFnSc4d1
od	od	k7c2
ásány	ásána	k1gFnSc2
nebo	nebo	k8xC
jógy	jóga	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nezbytnou	nezbytný	k2eAgFnSc7d1,k2eNgFnSc7d1
součástí	součást	k1gFnSc7
celku	celek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cviky	cvik	k1gInPc4
v	v	k7c6
kriji	krij	k1gFnSc6
přivádějí	přivádět	k5eAaImIp3nP
tělo	tělo	k1gNnSc4
a	a	k8xC
mysl	mysl	k1gFnSc4
do	do	k7c2
stavu	stav	k1gInSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgNnSc6,k3yRgNnSc6,k3yIgNnSc6
lze	lze	k6eAd1
snadno	snadno	k6eAd1
dosáhnout	dosáhnout	k5eAaPmF
hluboké	hluboký	k2eAgFnPc4d1
meditace	meditace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Indická	indický	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
a	a	k8xC
hinduismus	hinduismus	k1gInSc1
Ortodoxní	ortodoxní	k2eAgFnSc2d1
školy	škola	k1gFnSc2
</s>
<s>
Njája	Njáj	k2eAgFnSc1d1
•	•	k?
Vaišéšika	Vaišéšika	k1gFnSc1
•	•	k?
Sánkhja	Sánkhj	k2eAgFnSc1d1
•	•	k?
Jóga	jóga	k1gFnSc1
•	•	k?
Mímánsá	Mímánsý	k2eAgFnSc1d1
•	•	k?
Védánta	védánta	k1gFnSc1
•	•	k?
Advaita	Advaita	k1gMnSc1
•	•	k?
Višištádvaita	Višištádvaita	k1gMnSc1
•	•	k?
Dvaita	Dvaita	k1gFnSc1
Neortodoxní	ortodoxní	k2eNgFnPc1d1
školy	škola	k1gFnPc1
</s>
<s>
Čárváka	Čárvák	k1gMnSc4
•	•	k?
Džinismus	Džinismus	k1gInSc1
•	•	k?
Buddhismus	buddhismus	k1gInSc1
•	•	k?
Hínajána	Hínajána	k1gFnSc1
•	•	k?
Mahájána	Maháján	k2eAgFnSc1d1
•	•	k?
Vadžrajána	Vadžraján	k2eAgFnSc1d1
•	•	k?
Théraváda	Théraváda	k1gFnSc1
Osobnosti	osobnost	k1gFnSc2
</s>
<s>
Starověk	starověk	k1gInSc1
</s>
<s>
Kapila	Kapila	k1gFnSc1
•	•	k?
Pataňdžali	Pataňdžali	k1gFnSc2
•	•	k?
Gótama	Gótama	k?
•	•	k?
Gautama	Gautama	k1gNnSc1
Buddha	Buddha	k1gMnSc1
•	•	k?
Kanáda	Kanáda	k1gFnSc1
•	•	k?
Džaimini	Džaimin	k2eAgMnPc1d1
•	•	k?
Vjása	Vjás	k1gMnSc2
•	•	k?
Márkandéja	Márkandéjus	k1gMnSc2
•	•	k?
Pánini	Pánin	k1gMnPc5
Středověk	středověk	k1gInSc1
</s>
<s>
Ádi	Ádi	k?
Šankara	Šankara	k1gFnSc1
•	•	k?
Šrí	Šrí	k1gMnSc1
Rámánudžáčárja	Rámánudžáčárja	k1gMnSc1
•	•	k?
Šrí	Šrí	k1gMnSc1
Madhváčárja	Madhváčárja	k1gMnSc1
•	•	k?
Šrí	Šrí	k1gMnSc1
Nimbárkáčárja	Nimbárkáčárja	k1gMnSc1
•	•	k?
Šrí	Šrí	k1gMnSc1
Vallabháčárja	Vallabháčárja	k1gMnSc1
•	•	k?
Gosvámí	Gosvámí	k1gNnPc2
Tulsídás	Tulsídás	k1gInSc1
•	•	k?
Čaitanja	Čaitanja	k1gFnSc1
Maháprabhu	Maháprabh	k1gInSc2
•	•	k?
Mírábáí	Mírábáí	k2eAgInSc1d1
Novověk	novověk	k1gInSc1
</s>
<s>
A.	A.	kA
C.	C.	kA
Bhaktivédánta	Bhaktivédánta	k1gMnSc1
Svámí	Svámí	k1gNnSc2
Prabhupáda	Prabhupáda	k1gFnSc1
•	•	k?
Svámi	Svá	k1gFnPc7
Dajánanda	Dajánando	k1gNnSc2
Sarasvatí	Sarasvatý	k2eAgMnPc1d1
•	•	k?
Rabíndranáth	Rabíndranáth	k1gMnSc1
Thákur	Thákur	k1gMnSc1
•	•	k?
Mahátma	Mahátma	k1gFnSc1
Gándhí	Gándhí	k2eAgFnSc1d1
•	•	k?
Sarvepalli	Sarvepalle	k1gFnSc3
Rádhakrišnan	Rádhakrišnan	k1gMnSc1
•	•	k?
Šrí	Šrí	k1gMnSc1
Rámakršna	Rámakršn	k1gInSc2
•	•	k?
Svámí	Svámí	k1gNnSc1
Vivékánanda	Vivékánando	k1gNnSc2
•	•	k?
Šrí	Šrí	k1gMnSc1
Aurobindo	Aurobindo	k6eAd1
•	•	k?
Ramana	Ramana	k1gFnSc1
Maharši	Maharše	k1gFnSc4
•	•	k?
Šivánanda	Šivánando	k1gNnSc2
Sarasvatí	Sarasvatý	k2eAgMnPc1d1
•	•	k?
Paramahansa	Paramahansa	k1gFnSc1
Jógánanda	Jógánando	k1gNnSc2
•	•	k?
Mahariši	Mahariš	k1gMnSc3
Maheš	Maheš	k1gMnSc3
Jógi	Jóg	k1gMnSc3
•	•	k?
Pándurang	Pándurang	k1gMnSc1
Šástrí	Šástrí	k1gMnSc1
Áthavalé	Áthavalý	k2eAgNnSc1d1
•	•	k?
Brahmarši	Brahmarš	k1gInSc6
Dévrahá	Dévrahý	k2eAgFnSc1d1
Bábá	Bábá	k1gFnSc1
•	•	k?
Mata	mást	k5eAaImSgInS
Amritanandamayi	Amritanandamayi	k1gNnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Jóga	jóga	k1gFnSc1
|	|	kIx~
Hinduismus	hinduismus	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4114329-2	4114329-2	k4
</s>
