<s>
Černý	černý	k2eAgMnSc1d1	černý
trpaslík	trpaslík	k1gMnSc1	trpaslík
je	být	k5eAaImIp3nS	být
chladný	chladný	k2eAgInSc4d1	chladný
hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
objekt	objekt	k1gInSc4	objekt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
z	z	k7c2	z
bílého	bílý	k2eAgMnSc2d1	bílý
trpaslíka	trpaslík	k1gMnSc2	trpaslík
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
postupným	postupný	k2eAgNnSc7d1	postupné
zářením	záření	k1gNnSc7	záření
černého	černý	k2eAgNnSc2d1	černé
tělesa	těleso	k1gNnSc2	těleso
ochladl	ochladnout	k5eAaPmAgMnS	ochladnout
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
by	by	kYmCp3nS	by
neměl	mít	k5eNaImAgMnS	mít
dosud	dosud	k6eAd1	dosud
žádný	žádný	k3yNgMnSc1	žádný
černý	černý	k2eAgMnSc1d1	černý
trpaslík	trpaslík	k1gMnSc1	trpaslík
existovat	existovat	k5eAaImF	existovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
čas	čas	k1gInSc4	čas
potřebný	potřebný	k2eAgInSc4d1	potřebný
k	k	k7c3	k
ochlazení	ochlazení	k1gNnSc3	ochlazení
bílého	bílý	k1gMnSc2	bílý
trpaslíka	trpaslík	k1gMnSc2	trpaslík
na	na	k7c4	na
požadovanou	požadovaný	k2eAgFnSc4d1	požadovaná
úroveň	úroveň	k1gFnSc4	úroveň
je	být	k5eAaImIp3nS	být
delší	dlouhý	k2eAgFnSc1d2	delší
než	než	k8xS	než
dosavadní	dosavadní	k2eAgFnSc1d1	dosavadní
délka	délka	k1gFnSc1	délka
existence	existence	k1gFnSc2	existence
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
epoše	epocha	k1gFnSc6	epocha
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
černí	černý	k2eAgMnPc1d1	černý
trpaslíci	trpaslík	k1gMnPc1	trpaslík
budou	být	k5eAaImBp3nP	být
existovat	existovat	k5eAaImF	existovat
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
nesmírně	smírně	k6eNd1	smírně
obtížné	obtížný	k2eAgNnSc1d1	obtížné
je	být	k5eAaImIp3nS	být
detekovat	detekovat	k5eAaImF	detekovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
budou	být	k5eAaImBp3nP	být
vysílat	vysílat	k5eAaImF	vysílat
tepelné	tepelný	k2eAgNnSc4d1	tepelné
záření	záření	k1gNnSc4	záření
o	o	k7c6	o
teplotě	teplota	k1gFnSc6	teplota
jen	jen	k9	jen
o	o	k7c4	o
málo	málo	k6eAd1	málo
vyšší	vysoký	k2eAgNnSc4d2	vyšší
než	než	k8xS	než
kosmické	kosmický	k2eAgNnSc4d1	kosmické
mikrovlnné	mikrovlnný	k2eAgNnSc4d1	mikrovlnné
pozadí	pozadí	k1gNnSc4	pozadí
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
mála	málo	k1gNnSc2	málo
možností	možnost	k1gFnSc7	možnost
jejich	jejich	k3xOp3gNnSc2	jejich
odhalení	odhalení	k1gNnSc2	odhalení
tkví	tkvět	k5eAaImIp3nS	tkvět
v	v	k7c6	v
jejich	jejich	k3xOp3gInPc6	jejich
gravitačních	gravitační	k2eAgInPc6d1	gravitační
účincích	účinek	k1gInPc6	účinek
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zachycení	zachycení	k1gNnSc3	zachycení
světelných	světelný	k2eAgInPc2d1	světelný
paprsků	paprsek	k1gInPc2	paprsek
ohýbajících	ohýbající	k2eAgInPc2d1	ohýbající
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
trpaslíka	trpaslík	k1gMnSc2	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Rekonstruováním	rekonstruování	k1gNnSc7	rekonstruování
informace	informace	k1gFnSc2	informace
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
přenášejí	přenášet	k5eAaImIp3nP	přenášet
<g/>
,	,	kIx,	,
získáme	získat	k5eAaPmIp1nP	získat
deformovaný	deformovaný	k2eAgInSc4d1	deformovaný
obraz	obraz	k1gInSc4	obraz
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
kterého	který	k3yQgInSc2	který
můžeme	moct	k5eAaImIp1nP	moct
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgNnSc7	jaký
tělesem	těleso	k1gNnSc7	těleso
byly	být	k5eAaImAgInP	být
paprsky	paprsek	k1gInPc1	paprsek
ohnuty	ohnout	k5eAaPmNgInP	ohnout
<g/>
.	.	kIx.	.
</s>
<s>
Stejná	stejný	k2eAgFnSc1d1	stejná
metoda	metoda	k1gFnSc1	metoda
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
mj.	mj.	kA	mj.
i	i	k8xC	i
k	k	k7c3	k
detekci	detekce	k1gFnSc3	detekce
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
<g/>
.	.	kIx.	.
</s>
<s>
Černé	Černé	k2eAgMnPc4d1	Černé
trpaslíky	trpaslík	k1gMnPc4	trpaslík
nelze	lze	k6eNd1	lze
zaměňovat	zaměňovat	k5eAaImF	zaměňovat
s	s	k7c7	s
hnědými	hnědý	k2eAgMnPc7d1	hnědý
trpaslíky	trpaslík	k1gMnPc7	trpaslík
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
formují	formovat	k5eAaImIp3nP	formovat
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
z	z	k7c2	z
plynu	plyn	k1gInSc2	plyn
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedosáhnou	dosáhnout	k5eNaPmIp3nP	dosáhnout
dostatečné	dostatečný	k2eAgFnPc4d1	dostatečná
hmotnosti	hmotnost	k1gFnPc4	hmotnost
k	k	k7c3	k
zahájení	zahájení	k1gNnSc3	zahájení
a	a	k8xC	a
udržení	udržení	k1gNnSc3	udržení
vodíkové	vodíkový	k2eAgFnSc2d1	vodíková
nukleární	nukleární	k2eAgFnSc2d1	nukleární
fúze	fúze	k1gFnSc2	fúze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byli	být	k5eAaImAgMnP	být
občas	občas	k6eAd1	občas
"	"	kIx"	"
<g/>
hnědí	hnědý	k2eAgMnPc1d1	hnědý
trpaslíci	trpaslík	k1gMnPc1	trpaslík
<g/>
"	"	kIx"	"
nazýváni	nazýván	k2eAgMnPc1d1	nazýván
"	"	kIx"	"
<g/>
černými	černý	k2eAgMnPc7d1	černý
trpaslíky	trpaslík	k1gMnPc7	trpaslík
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Bílý	bílý	k2eAgMnSc1d1	bílý
trpaslík	trpaslík	k1gMnSc1	trpaslík
Červený	Červený	k1gMnSc1	Červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
Hnědý	hnědý	k2eAgMnSc1d1	hnědý
trpaslík	trpaslík	k1gMnSc1	trpaslík
Modrý	modrý	k2eAgMnSc1d1	modrý
trpaslík	trpaslík	k1gMnSc1	trpaslík
</s>
