<s>
Černý	černý	k2eAgMnSc1d1	černý
trpaslík	trpaslík	k1gMnSc1	trpaslík
je	být	k5eAaImIp3nS	být
chladný	chladný	k2eAgInSc4d1	chladný
hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
objekt	objekt	k1gInSc4	objekt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
z	z	k7c2	z
bílého	bílý	k2eAgMnSc2d1	bílý
trpaslíka	trpaslík	k1gMnSc2	trpaslík
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
postupným	postupný	k2eAgNnSc7d1	postupné
zářením	záření	k1gNnSc7	záření
černého	černý	k2eAgNnSc2d1	černé
tělesa	těleso	k1gNnSc2	těleso
ochladl	ochladnout	k5eAaPmAgMnS	ochladnout
<g/>
.	.	kIx.	.
</s>
