<s>
Alastair	Alastair	k1gInSc1
Reynolds	Reynoldsa	k1gFnPc2
</s>
<s>
Alastair	Alastair	k1gInSc1
Reynolds	Reynoldsa	k1gFnPc2
Alastair	Alastaira	k1gFnPc2
Reynolds	Reynolds	k1gInSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2010	#num#	k4
<g/>
)	)	kIx)
<g/>
Rodné	rodný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Alastair	Alastair	k1gInSc1
Preston	Preston	k1gInSc1
Reynolds	Reynoldsa	k1gFnPc2
Narození	narození	k1gNnSc2
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1966	#num#	k4
(	(	kIx(
<g/>
55	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Barry	Barr	k1gMnPc4
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc2
</s>
<s>
Univerzita	univerzita	k1gFnSc1
v	v	k7c6
St	St	kA
AndrewsUniverzita	AndrewsUniverzita	k1gFnSc1
Newcastle	Newcastle	k1gFnSc2
Povolání	povolání	k1gNnSc2
</s>
<s>
astrofyzik	astrofyzik	k1gMnSc1
<g/>
,	,	kIx,
spisovatel	spisovatel	k1gMnSc1
a	a	k8xC
autor	autor	k1gMnSc1
sci-fi	sci-fi	k1gNnSc2
Ocenění	ocenění	k1gNnSc2
</s>
<s>
BSFA	BSFA	kA
Award	Award	k1gMnSc1
for	forum	k1gNnPc2
Best	Besta	k1gFnPc2
Novel	novela	k1gFnPc2
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
Web	web	k1gInSc1
</s>
<s>
www.alastairreynolds.com	www.alastairreynolds.com	k1gInSc1
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Alastair	Alastair	k1gInSc1
Reynolds	Reynolds	k1gInSc1
(	(	kIx(
<g/>
*	*	kIx~
13	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1966	#num#	k4
Barry	Barra	k1gFnSc2
<g/>
,	,	kIx,
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
velšský	velšský	k2eAgMnSc1d1
autor	autor	k1gMnSc1
science	science	k1gFnSc2
fiction	fiction	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Specializuje	specializovat	k5eAaBmIp3nS
se	se	k3xPyFc4
hlavně	hlavně	k9
na	na	k7c6
space	spaka	k1gFnSc6
opery	opera	k1gFnSc2
a	a	k8xC
hard	hard	k1gMnSc1
science	science	k1gFnSc2
fiction	fiction	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Raná	raný	k2eAgNnPc4d1
léta	léto	k1gNnPc4
svého	svůj	k3xOyFgInSc2
života	život	k1gInSc2
strávil	strávit	k5eAaPmAgMnS
v	v	k7c6
Cornwallu	Cornwall	k1gInSc6
<g/>
,	,	kIx,
poté	poté	k6eAd1
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
zpátky	zpátky	k6eAd1
do	do	k7c2
Walesu	Wales	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Newcastlu	Newcastl	k1gInSc6
absolvoval	absolvovat	k5eAaPmAgMnS
fyziku	fyzika	k1gFnSc4
a	a	k8xC
astronomii	astronomie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
získání	získání	k1gNnSc6
doktorátu	doktorát	k1gInSc2
na	na	k7c6
skotské	skotský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
St.	st.	kA
Andrews	Andrews	k1gInSc1
roku	rok	k1gInSc2
1991	#num#	k4
se	se	k3xPyFc4
odstěhoval	odstěhovat	k5eAaPmAgMnS
do	do	k7c2
Nizozemska	Nizozemsko	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
potkal	potkat	k5eAaPmAgInS
svou	svůj	k3xOyFgFnSc4
partnerku	partnerka	k1gFnSc4
Josette	Josett	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následovalo	následovat	k5eAaImAgNnS
zhruba	zhruba	k6eAd1
dvacet	dvacet	k4xCc1
let	léto	k1gNnPc2
vědecké	vědecký	k2eAgFnSc2d1
práce	práce	k1gFnSc2
jako	jako	k8xS,k8xC
astrofyzik	astrofyzika	k1gFnPc2
–	–	k?
z	z	k7c2
toho	ten	k3xDgNnSc2
deset	deset	k4xCc4
let	léto	k1gNnPc2
pro	pro	k7c4
Evropskou	evropský	k2eAgFnSc4d1
kosmickou	kosmický	k2eAgFnSc4d1
agenturu	agentura	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Odhalený	odhalený	k2eAgInSc1d1
vesmír	vesmír	k1gInSc1
</s>
<s>
Odhalený	odhalený	k2eAgInSc1d1
vesmír	vesmír	k1gInSc1
1	#num#	k4
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Triton	triton	k1gMnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7254	#num#	k4
<g/>
-	-	kIx~
<g/>
430	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-7254-345-8	80-7254-345-8	k4
(	(	kIx(
<g/>
Revelation	Revelation	k1gInSc1
Space	Spaec	k1gInSc2
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kaldera	Kaldera	k1gFnSc1
1	#num#	k4
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Triton	triton	k1gMnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7254	#num#	k4
<g/>
-	-	kIx~
<g/>
483	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-7254-484-5	80-7254-484-5	k4
(	(	kIx(
<g/>
Chasm	Chasm	k1gInSc1
city	city	k1gFnSc2
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Archa	archa	k1gFnSc1
1	#num#	k4
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Triton	triton	k1gMnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7254	#num#	k4
<g/>
-	-	kIx~
<g/>
621	#num#	k4
<g/>
-X	-X	k?
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-7254-622-8	80-7254-622-8	k4
(	(	kIx(
<g/>
Redemption	Redemption	k1gInSc1
Ark	Ark	k1gFnSc2
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Údolí	údolí	k1gNnSc1
vykoupení	vykoupení	k1gNnSc2
1	#num#	k4
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Triton	triton	k1gMnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7254	#num#	k4
<g/>
-	-	kIx~
<g/>
794	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-7254-795-X	80-7254-795-X	k4
(	(	kIx(
<g/>
Absolution	Absolution	k1gInSc1
Gap	Gap	k1gFnSc2
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Diamond	Diamond	k1gInSc1
Dogs	Dogs	k1gInSc1
<g/>
,	,	kIx,
Turquoise	Turquoise	k1gFnSc1
Days	Daysa	k1gFnPc2
<g/>
,	,	kIx,
2003	#num#	k4
</s>
<s>
Galactic	Galactice	k1gFnPc2
North	Northa	k1gFnPc2
<g/>
,	,	kIx,
2006	#num#	k4
</s>
<s>
The	The	k?
Prefect	Prefect	k1gMnSc1
<g/>
,	,	kIx,
2007	#num#	k4
</s>
<s>
Poseidon	Poseidon	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Children	Childrna	k1gFnPc2
</s>
<s>
Blue	Blue	k6eAd1
Remembered	Remembered	k1gMnSc1
Earth	Earth	k1gMnSc1
<g/>
,	,	kIx,
2012	#num#	k4
</s>
<s>
On	on	k3xPp3gInSc1
the	the	k?
Steel	Steel	k1gInSc1
Breeze	Breeze	k1gFnSc1
<g/>
,	,	kIx,
2013	#num#	k4
</s>
<s>
Poseidon	Poseidon	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Wake	Wake	k1gFnSc7
<g/>
,	,	kIx,
2015	#num#	k4
</s>
<s>
Romány	román	k1gInPc1
</s>
<s>
Century	Centura	k1gFnPc1
Rain	Raina	k1gFnPc2
<g/>
,	,	kIx,
2004	#num#	k4
</s>
<s>
Transport	transport	k1gInSc1
ledu	led	k1gInSc2
(	(	kIx(
<g/>
Pushing	Pushing	k1gInSc1
Ice	Ice	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2005	#num#	k4
</s>
<s>
House	house	k1gNnSc1
of	of	k?
Suns	Sunsa	k1gFnPc2
<g/>
,	,	kIx,
2008	#num#	k4
</s>
<s>
Terminal	Terminat	k5eAaImAgMnS,k5eAaPmAgMnS
World	World	k1gMnSc1
<g/>
,	,	kIx,
2010	#num#	k4
</s>
<s>
Harvest	Harvest	k1gFnSc1
of	of	k?
Time	Time	k1gFnPc2
<g/>
,	,	kIx,
2013	#num#	k4
</s>
<s>
Povídky	povídka	k1gFnPc1
</s>
<s>
Nunivak	Nunivak	k1gMnSc1
Snowflakes	Snowflakes	k1gMnSc1
<g/>
,	,	kIx,
1990	#num#	k4
</s>
<s>
Prodloužený	prodloužený	k2eAgInSc1d1
spánek	spánek	k1gInSc1
<g/>
,	,	kIx,
Ikarie	Ikarie	k1gFnSc1
7	#num#	k4
<g/>
/	/	kIx~
<g/>
1992	#num#	k4
(	(	kIx(
<g/>
Dilation	Dilation	k1gInSc1
Sleep	Sleep	k1gInSc1
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Enola	Enola	k1gMnSc1
<g/>
,	,	kIx,
1991	#num#	k4
</s>
<s>
Digital	Digital	kA
to	ten	k3xDgNnSc4
Analogue	Analogue	k1gNnSc4
<g/>
,	,	kIx,
1992	#num#	k4
</s>
<s>
Byrd	Byrd	k1gMnSc1
Land	Land	k1gMnSc1
Six	Six	k1gMnSc1
<g/>
,	,	kIx,
1995	#num#	k4
</s>
<s>
Spirey	spirea	k1gFnPc1
and	and	k?
the	the	k?
Queen	Quena	k1gFnPc2
<g/>
,	,	kIx,
1996	#num#	k4
</s>
<s>
Špion	špion	k1gMnSc1
na	na	k7c6
Europě	Europa	k1gFnSc6
<g/>
,	,	kIx,
Ikarie	Ikarie	k1gFnSc1
5	#num#	k4
<g/>
/	/	kIx~
<g/>
2002	#num#	k4
(	(	kIx(
<g/>
A	a	k9
Spy	Spy	k1gMnSc2
in	in	k?
Europa	Europ	k1gMnSc2
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
On	on	k3xPp3gMnSc1
the	the	k?
Oodnadatta	Oodnadatta	k1gMnSc1
<g/>
,	,	kIx,
1998	#num#	k4
</s>
<s>
Stroboscopic	Stroboscopic	k1gMnSc1
<g/>
,	,	kIx,
1998	#num#	k4
</s>
<s>
Galaktický	galaktický	k2eAgInSc1d1
sever	sever	k1gInSc1
<g/>
,	,	kIx,
Ikarie	Ikarie	k1gFnSc1
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2003	#num#	k4
(	(	kIx(
<g/>
Galactic	Galactice	k1gFnPc2
North	North	k1gInSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Andělé	anděl	k1gMnPc1
z	z	k7c2
popela	popel	k1gInSc2
<g/>
,	,	kIx,
Ikarie	Ikarie	k1gFnSc1
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2001	#num#	k4
(	(	kIx(
<g/>
Angels	Angelsa	k1gFnPc2
of	of	k?
Ashes	Ashesa	k1gFnPc2
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Viper	Viper	k1gMnSc1
<g/>
,	,	kIx,
1999	#num#	k4
</s>
<s>
Velká	velký	k2eAgFnSc1d1
marsovská	marsovský	k2eAgFnSc1d1
zeď	zeď	k1gFnSc1
<g/>
,	,	kIx,
Ikarie	Ikarie	k1gFnSc1
9	#num#	k4
<g/>
/	/	kIx~
<g/>
2004	#num#	k4
(	(	kIx(
<g/>
Great	Great	k2eAgInSc1d1
Wall	Wall	k1gInSc1
of	of	k?
Mars	Mars	k1gInSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Merlin	Merlin	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Gun	Gun	k1gFnSc7
<g/>
,	,	kIx,
2000	#num#	k4
</s>
<s>
Hideaway	Hideaway	k1gInPc1
<g/>
,	,	kIx,
2000	#num#	k4
</s>
<s>
Glacial	Glacial	k1gMnSc1
<g/>
,	,	kIx,
2001	#num#	k4
</s>
<s>
Fresco	Fresco	k1gMnSc1
<g/>
,	,	kIx,
2001	#num#	k4
</s>
<s>
Diamond	Diamond	k1gInSc1
Dogs	Dogsa	k1gFnPc2
<g/>
,	,	kIx,
2001	#num#	k4
</s>
<s>
The	The	k?
Real	Real	k1gInSc1
Story	story	k1gFnPc2
<g/>
,	,	kIx,
2002	#num#	k4
</s>
<s>
Turquoise	Turquoise	k1gFnSc1
Days	Daysa	k1gFnPc2
<g/>
,	,	kIx,
2002	#num#	k4
</s>
<s>
Emmissary	Emmissar	k1gInPc1
<g/>
,	,	kIx,
2003	#num#	k4
</s>
<s>
Nightingaleová	Nightingaleová	k1gFnSc1
<g/>
,	,	kIx,
Ikarie	Ikarie	k1gFnSc1
7	#num#	k4
<g/>
/	/	kIx~
<g/>
2008	#num#	k4
(	(	kIx(
<g/>
Nightingale	Nightingala	k1gFnSc6
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
British	British	k1gMnSc1
Science	Science	k1gFnSc2
Fiction	Fiction	k1gInSc1
Award	Award	k1gMnSc1
</s>
<s>
2001	#num#	k4
–	–	k?
román	román	k1gInSc1
–	–	k?
Kaldera	Kaldera	k1gFnSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
anglických	anglický	k2eAgMnPc2d1
spisovatelů	spisovatel	k1gMnPc2
</s>
<s>
anglická	anglický	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
</s>
<s>
Sci-fi	sci-fi	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Alastair	Alastair	k1gInSc1
Reynolds	Reynoldsa	k1gFnPc2
</s>
<s>
Domovská	domovský	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
14243	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
123501679	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0891	#num#	k4
9685	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
43655	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
42057298	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
43655	#num#	k4
</s>
