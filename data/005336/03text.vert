<s>
Leoš	Leoš	k1gMnSc1	Leoš
je	být	k5eAaImIp3nS	být
mužské	mužský	k2eAgNnSc4d1	mužské
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
řeckého	řecký	k2eAgInSc2d1	řecký
původu	původ	k1gInSc2	původ
–	–	k?	–
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
zkráceninu	zkrácenina	k1gFnSc4	zkrácenina
jména	jméno	k1gNnSc2	jméno
Leopold	Leopolda	k1gFnPc2	Leopolda
nebo	nebo	k8xC	nebo
Leonard	Leonarda	k1gFnPc2	Leonarda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
českého	český	k2eAgInSc2d1	český
kalendáře	kalendář	k1gInSc2	kalendář
má	mít	k5eAaImIp3nS	mít
svátek	svátek	k1gInSc1	svátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
četnost	četnost	k1gFnSc4	četnost
jména	jméno	k1gNnSc2	jméno
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
pořadí	pořadí	k1gNnSc2	pořadí
mezi	mezi	k7c7	mezi
mužskými	mužský	k2eAgNnPc7d1	mužské
jmény	jméno	k1gNnPc7	jméno
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
dvou	dva	k4xCgInPc2	dva
roků	rok	k1gInPc2	rok
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgInPc4	který
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
údaje	údaj	k1gInPc1	údaj
MV	MV	kA	MV
ČR	ČR	kA	ČR
–	–	k?	–
lze	lze	k6eAd1	lze
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
tedy	tedy	k9	tedy
vysledovat	vysledovat	k5eAaImF	vysledovat
trend	trend	k1gInSc4	trend
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
:	:	kIx,	:
Změna	změna	k1gFnSc1	změna
procentního	procentní	k2eAgNnSc2d1	procentní
zastoupení	zastoupení	k1gNnSc2	zastoupení
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
mezi	mezi	k7c7	mezi
žijícími	žijící	k2eAgMnPc7d1	žijící
muži	muž	k1gMnPc7	muž
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
procentní	procentní	k2eAgFnSc1d1	procentní
změna	změna	k1gFnSc1	změna
se	se	k3xPyFc4	se
započítáním	započítání	k1gNnSc7	započítání
celkového	celkový	k2eAgInSc2d1	celkový
úbytku	úbytek	k1gInSc2	úbytek
mužů	muž	k1gMnPc2	muž
v	v	k7c6	v
ČR	ČR	kA	ČR
za	za	k7c4	za
sledované	sledovaný	k2eAgInPc4d1	sledovaný
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
1999	[number]	k4	1999
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
+1,2	+1,2	k4	+1,2
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Leoš	Leoš	k1gMnSc1	Leoš
Heger	Heger	k1gMnSc1	Heger
–	–	k?	–
český	český	k2eAgMnSc1d1	český
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
vysokoškolský	vysokoškolský	k2eAgMnSc1d1	vysokoškolský
pedagog	pedagog	k1gMnSc1	pedagog
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
strany	strana	k1gFnSc2	strana
TOP	topit	k5eAaImRp2nS	topit
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
července	červenec	k1gInSc2	červenec
2010	[number]	k4	2010
zastává	zastávat	k5eAaImIp3nS	zastávat
funkci	funkce	k1gFnSc4	funkce
ministra	ministr	k1gMnSc2	ministr
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
v	v	k7c6	v
Nečasově	Nečasův	k2eAgFnSc6d1	Nečasova
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Leoš	Leoš	k1gMnSc1	Leoš
Janáček	Janáček	k1gMnSc1	Janáček
–	–	k?	–
český	český	k2eAgMnSc1d1	český
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Leoš	Leoš	k1gMnSc1	Leoš
Jeleček	jeleček	k1gMnSc1	jeleček
–	–	k?	–
český	český	k2eAgMnSc1d1	český
historický	historický	k2eAgMnSc1d1	historický
geograf	geograf	k1gMnSc1	geograf
Leoš	Leoš	k1gMnSc1	Leoš
Mareš	Mareš	k1gMnSc1	Mareš
–	–	k?	–
český	český	k2eAgMnSc1d1	český
moderátor	moderátor	k1gMnSc1	moderátor
Leoš	Leoš	k1gMnSc1	Leoš
Středa	středa	k1gFnSc1	středa
–	–	k?	–
český	český	k2eAgMnSc1d1	český
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
rytíř	rytíř	k1gMnSc1	rytíř
Řádu	řád	k1gInSc2	řád
Josého	Josého	k2eAgFnSc1d1	Josého
Rizala	Rizala	k1gFnSc1	Rizala
na	na	k7c6	na
Filipínách	Filipíny	k1gFnPc6	Filipíny
Leoš	Leoš	k1gMnSc1	Leoš
Suchařípa	Suchařípa	k1gFnSc1	Suchařípa
–	–	k?	–
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
dramaturg	dramaturg	k1gMnSc1	dramaturg
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
Leoš	Leoš	k1gMnSc1	Leoš
Svárovský	Svárovský	k1gMnSc1	Svárovský
–	–	k?	–
český	český	k2eAgMnSc1d1	český
dirigent	dirigent	k1gMnSc1	dirigent
Leoš	Leoš	k1gMnSc1	Leoš
Šimánek	Šimánek	k1gMnSc1	Šimánek
–	–	k?	–
český	český	k2eAgMnSc1d1	český
cestovatel	cestovatel	k1gMnSc1	cestovatel
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
Seznam	seznam	k1gInSc4	seznam
článků	článek	k1gInPc2	článek
začínajících	začínající	k2eAgInPc2d1	začínající
na	na	k7c4	na
"	"	kIx"	"
<g/>
Leoš	Leoš	k1gMnSc1	Leoš
<g/>
"	"	kIx"	"
</s>
