<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
strukturované	strukturovaný	k2eAgNnSc1d1	strukturované
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
predstavit	predstavit	k5eAaPmF	predstavit
lidské	lidský	k2eAgNnSc4d1	lidské
poznání	poznání	k1gNnSc4	poznání
týkající	týkající	k2eAgNnSc4d1	týkající
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
oboru	obor	k1gInSc2	obor
<g/>
?	?	kIx.	?
</s>
