<p>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
έ	έ	k?	έ
enkyklios	enkyklios	k1gInSc4	enkyklios
okružní	okružní	k2eAgInSc4d1	okružní
+	+	kIx~	+
π	π	k?	π
paideia	paideia	k1gFnSc1	paideia
výchova	výchova	k1gFnSc1	výchova
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
strukturované	strukturovaný	k2eAgNnSc1d1	strukturované
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
objemné	objemný	k2eAgNnSc4d1	objemné
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
zevrubně	zevrubně	k6eAd1	zevrubně
představit	představit	k5eAaPmF	představit
lidské	lidský	k2eAgNnSc4d1	lidské
poznání	poznání	k1gNnSc4	poznání
týkající	týkající	k2eAgNnSc4d1	týkající
se	se	k3xPyFc4	se
jednoho	jeden	k4xCgMnSc2	jeden
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
všech	všecek	k3xTgInPc2	všecek
oborů	obor	k1gInPc2	obor
<g/>
.	.	kIx.	.
</s>
<s>
Pojmu	pojmout	k5eAaPmIp1nS	pojmout
έ	έ	k?	έ
(	(	kIx(	(
<g/>
enkyklopaideia	enkyklopaideius	k1gMnSc2	enkyklopaideius
<g/>
)	)	kIx)	)
poprvé	poprvé	k6eAd1	poprvé
užil	užít	k5eAaPmAgMnS	užít
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Hippias	Hippias	k1gMnSc1	Hippias
z	z	k7c2	z
Elidy	Elida	k1gFnSc2	Elida
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
jim	on	k3xPp3gMnPc3	on
označoval	označovat	k5eAaImAgMnS	označovat
všeobecné	všeobecný	k2eAgNnSc4d1	všeobecné
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obecná	obecný	k2eAgFnSc1d1	obecná
charakteristika	charakteristika	k1gFnSc1	charakteristika
==	==	k?	==
</s>
</p>
<p>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
je	být	k5eAaImIp3nS	být
specifický	specifický	k2eAgInSc1d1	specifický
druh	druh	k1gInSc1	druh
slovníku	slovník	k1gInSc2	slovník
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
naučný	naučný	k2eAgInSc4d1	naučný
slovník	slovník	k1gInSc4	slovník
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gNnPc1	jeho
hesla	heslo	k1gNnPc1	heslo
(	(	kIx(	(
<g/>
encyklopedická	encyklopedický	k2eAgNnPc1d1	encyklopedické
hesla	heslo	k1gNnPc1	heslo
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
tedy	tedy	k9	tedy
řazena	řadit	k5eAaImNgFnS	řadit
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
encyklopedie	encyklopedie	k1gFnPc1	encyklopedie
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
řazeny	řadit	k5eAaImNgFnP	řadit
abecedně	abecedně	k6eAd1	abecedně
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
tematicky	tematicky	k6eAd1	tematicky
<g/>
.	.	kIx.	.
</s>
<s>
Samostatnou	samostatný	k2eAgFnSc7d1	samostatná
kapitolou	kapitola	k1gFnSc7	kapitola
jsou	být	k5eAaImIp3nP	být
elektronické	elektronický	k2eAgFnPc1d1	elektronická
encyklopedie	encyklopedie	k1gFnPc1	encyklopedie
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yRgMnPc2	který
mnohdy	mnohdy	k6eAd1	mnohdy
ani	ani	k8xC	ani
nemá	mít	k5eNaImIp3nS	mít
otázka	otázka	k1gFnSc1	otázka
řazení	řazení	k1gNnSc2	řazení
hesel	heslo	k1gNnPc2	heslo
příliš	příliš	k6eAd1	příliš
smysl	smysl	k1gInSc4	smysl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
výkladového	výkladový	k2eAgInSc2d1	výkladový
slovníku	slovník	k1gInSc2	slovník
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
přináší	přinášet	k5eAaImIp3nS	přinášet
stručnou	stručný	k2eAgFnSc4d1	stručná
definici	definice	k1gFnSc4	definice
hesla	heslo	k1gNnSc2	heslo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
encyklopedie	encyklopedie	k1gFnPc1	encyklopedie
snaží	snažit	k5eAaImIp3nP	snažit
přiblížit	přiblížit	k5eAaPmF	přiblížit
předmět	předmět	k1gInSc4	předmět
hlouběji	hluboko	k6eAd2	hluboko
a	a	k8xC	a
podrobněji	podrobně	k6eAd2	podrobně
<g/>
.	.	kIx.	.
</s>
<s>
Encyklopedický	encyklopedický	k2eAgInSc1d1	encyklopedický
text	text	k1gInSc1	text
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
také	také	k9	také
lépe	dobře	k6eAd2	dobře
strukturován	strukturován	k2eAgMnSc1d1	strukturován
a	a	k8xC	a
přinést	přinést	k5eAaPmF	přinést
pro	pro	k7c4	pro
čtenáře	čtenář	k1gMnSc4	čtenář
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
informaci	informace	k1gFnSc4	informace
o	o	k7c6	o
daném	daný	k2eAgNnSc6d1	dané
tématu	téma	k1gNnSc6	téma
hesla	heslo	k1gNnSc2	heslo
<g/>
,	,	kIx,	,
doplněnou	doplněný	k2eAgFnSc4d1	doplněná
o	o	k7c4	o
ilustrace	ilustrace	k1gFnPc4	ilustrace
<g/>
,	,	kIx,	,
mapy	mapa	k1gFnPc4	mapa
<g/>
,	,	kIx,	,
přehledy	přehled	k1gInPc4	přehled
<g/>
,	,	kIx,	,
grafy	graf	k1gInPc4	graf
<g/>
,	,	kIx,	,
bibliografii	bibliografie	k1gFnSc4	bibliografie
apod.	apod.	kA	apod.
Také	také	k9	také
zpravidla	zpravidla	k6eAd1	zpravidla
často	často	k6eAd1	často
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
související	související	k2eAgNnPc4d1	související
hesla	heslo	k1gNnPc4	heslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Encyklopedii	encyklopedie	k1gFnSc4	encyklopedie
lze	lze	k6eAd1	lze
definovat	definovat	k5eAaBmF	definovat
pomocí	pomocí	k7c2	pomocí
čtyř	čtyři	k4xCgFnPc2	čtyři
základních	základní	k2eAgFnPc2d1	základní
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
:	:	kIx,	:
jejího	její	k3xOp3gInSc2	její
předmětu	předmět	k1gInSc2	předmět
<g/>
,	,	kIx,	,
účelu	účel	k1gInSc2	účel
<g/>
,	,	kIx,	,
metody	metoda	k1gFnSc2	metoda
či	či	k8xC	či
členění	členění	k1gNnSc2	členění
a	a	k8xC	a
proces	proces	k1gInSc1	proces
vzniku	vznik	k1gInSc2	vznik
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Předmět	předmět	k1gInSc1	předmět
<g/>
:	:	kIx,	:
Tematika	tematika	k1gFnSc1	tematika
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
obecná	obecná	k1gFnSc1	obecná
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
hesla	heslo	k1gNnSc2	heslo
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
oborů	obor	k1gInPc2	obor
(	(	kIx(	(
<g/>
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
naučný	naučný	k2eAgInSc1d1	naučný
<g/>
,	,	kIx,	,
Encyclopæ	Encyclopæ	k1gMnSc1	Encyclopæ
Britannica	Britannicum	k1gNnSc2	Britannicum
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
specializovat	specializovat	k5eAaBmF	specializovat
na	na	k7c4	na
určitý	určitý	k2eAgInSc4d1	určitý
obor	obor	k1gInSc4	obor
či	či	k8xC	či
okruh	okruh	k1gInSc1	okruh
hesel	heslo	k1gNnPc2	heslo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
filozofický	filozofický	k2eAgInSc1d1	filozofický
slovník	slovník	k1gInSc1	slovník
<g/>
,	,	kIx,	,
slovník	slovník	k1gInSc1	slovník
lékařství	lékařství	k1gNnSc2	lékařství
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
na	na	k7c4	na
určitou	určitý	k2eAgFnSc4d1	určitá
zeměpisnou	zeměpisný	k2eAgFnSc4d1	zeměpisná
či	či	k8xC	či
kulturní	kulturní	k2eAgFnSc4d1	kulturní
oblast	oblast	k1gFnSc4	oblast
(	(	kIx(	(
<g/>
např.	např.	kA	např.
o	o	k7c6	o
určité	určitý	k2eAgFnSc6d1	určitá
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
náboženství	náboženství	k1gNnSc1	náboženství
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Určení	určení	k1gNnSc1	určení
<g/>
:	:	kIx,	:
Encyklopedická	encyklopedický	k2eAgNnPc1d1	encyklopedické
díla	dílo	k1gNnPc1	dílo
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
přinést	přinést	k5eAaPmF	přinést
důležité	důležitý	k2eAgNnSc4d1	důležité
shromážděné	shromážděný	k2eAgNnSc4d1	shromážděné
poznání	poznání	k1gNnSc4	poznání
z	z	k7c2	z
určitého	určitý	k2eAgInSc2d1	určitý
oboru	obor	k1gInSc2	obor
<g/>
;	;	kIx,	;
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
podrobností	podrobnost	k1gFnSc7	podrobnost
a	a	k8xC	a
záběrem	záběr	k1gInSc7	záběr
<g/>
.	.	kIx.	.
</s>
<s>
Účel	účel	k1gInSc1	účel
díla	dílo	k1gNnSc2	dílo
může	moct	k5eAaImIp3nS	moct
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
i	i	k9	i
cílový	cílový	k2eAgInSc4d1	cílový
okruh	okruh	k1gInSc4	okruh
čtenářů	čtenář	k1gMnPc2	čtenář
<g/>
;	;	kIx,	;
dětské	dětský	k2eAgFnSc2d1	dětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
se	se	k3xPyFc4	se
s	s	k7c7	s
tématem	téma	k1gNnSc7	téma
vypořádávají	vypořádávat	k5eAaImIp3nP	vypořádávat
jinak	jinak	k6eAd1	jinak
než	než	k8xS	než
všeobecné	všeobecný	k2eAgFnPc1d1	všeobecná
encyklopedie	encyklopedie	k1gFnPc1	encyklopedie
či	či	k8xC	či
odborné	odborný	k2eAgInPc1d1	odborný
slovníky	slovník	k1gInPc1	slovník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Řazení	řazení	k1gNnSc1	řazení
<g/>
:	:	kIx,	:
Historicky	historicky	k6eAd1	historicky
existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc4	dva
rozdílné	rozdílný	k2eAgInPc4d1	rozdílný
způsoby	způsob	k1gInPc4	způsob
organizace	organizace	k1gFnSc2	organizace
tištěných	tištěný	k2eAgFnPc2d1	tištěná
encyklopedií	encyklopedie	k1gFnPc2	encyklopedie
<g/>
:	:	kIx,	:
abecední	abecední	k2eAgNnSc1d1	abecední
řazení	řazení	k1gNnSc1	řazení
a	a	k8xC	a
tematické	tematický	k2eAgNnSc1d1	tematické
řazení	řazení	k1gNnSc1	řazení
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnPc1d1	moderní
elektronické	elektronický	k2eAgFnPc1d1	elektronická
encyklopedie	encyklopedie	k1gFnPc1	encyklopedie
obvykle	obvykle	k6eAd1	obvykle
spojují	spojovat	k5eAaImIp3nP	spojovat
obě	dva	k4xCgFnPc1	dva
metody	metoda	k1gFnPc1	metoda
<g/>
,	,	kIx,	,
krom	krom	k7c2	krom
toho	ten	k3xDgNnSc2	ten
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
rychlé	rychlý	k2eAgNnSc4d1	rychlé
vyhledávání	vyhledávání	k1gNnSc4	vyhledávání
díky	díky	k7c3	díky
indexaci	indexace	k1gFnSc3	indexace
a	a	k8xC	a
křížovým	křížový	k2eAgInPc3d1	křížový
odkazům	odkaz	k1gInPc3	odkaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Metoda	metoda	k1gFnSc1	metoda
vzniku	vznik	k1gInSc2	vznik
<g/>
:	:	kIx,	:
Encyklopedická	encyklopedický	k2eAgNnPc1d1	encyklopedické
díla	dílo	k1gNnPc1	dílo
se	se	k3xPyFc4	se
také	také	k9	také
liší	lišit	k5eAaImIp3nP	lišit
ve	v	k7c6	v
volbě	volba	k1gFnSc6	volba
svých	svůj	k3xOyFgMnPc2	svůj
autorů	autor	k1gMnPc2	autor
a	a	k8xC	a
editorů	editor	k1gMnPc2	editor
<g/>
,	,	kIx,	,
způsobu	způsob	k1gInSc2	způsob
shromažďování	shromažďování	k1gNnSc2	shromažďování
<g/>
,	,	kIx,	,
ověřování	ověřování	k1gNnSc2	ověřování
a	a	k8xC	a
sepisování	sepisování	k1gNnSc2	sepisování
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
hesel	heslo	k1gNnPc2	heslo
<g/>
.	.	kIx.	.
</s>
<s>
Autoři	autor	k1gMnPc1	autor
encyklopedického	encyklopedický	k2eAgNnSc2d1	encyklopedické
díla	dílo	k1gNnSc2	dílo
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
renomovanými	renomovaný	k2eAgMnPc7d1	renomovaný
odborníky	odborník	k1gMnPc7	odborník
v	v	k7c6	v
daných	daný	k2eAgInPc6d1	daný
oborech	obor	k1gInPc6	obor
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
moderní	moderní	k2eAgFnPc1d1	moderní
encyklopedie	encyklopedie	k1gFnPc1	encyklopedie
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
a	a	k8xC	a
ověřovány	ověřovat	k5eAaImNgFnP	ověřovat
mnohem	mnohem	k6eAd1	mnohem
širším	široký	k2eAgInSc7d2	širší
okruhem	okruh	k1gInSc7	okruh
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
volný	volný	k2eAgInSc4d1	volný
přístup	přístup	k1gInSc4	přístup
praktikují	praktikovat	k5eAaImIp3nP	praktikovat
otevřené	otevřený	k2eAgFnPc1d1	otevřená
encyklopedie	encyklopedie	k1gFnPc1	encyklopedie
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
zejména	zejména	k9	zejména
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Raná	raný	k2eAgNnPc1d1	rané
encyklopedická	encyklopedický	k2eAgNnPc1d1	encyklopedické
díla	dílo	k1gNnPc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Idea	idea	k1gFnSc1	idea
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
stará	starý	k2eAgFnSc1d1	stará
–	–	k?	–
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
starého	starý	k2eAgNnSc2d1	staré
Řecka	Řecko	k1gNnSc2	Řecko
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
zaokrouhlené	zaokrouhlený	k2eAgNnSc1d1	zaokrouhlené
vzdělání	vzdělání	k1gNnSc1	vzdělání
<g/>
"	"	kIx"	"
–	–	k?	–
enkyklios	enkyklios	k1gInSc1	enkyklios
paideia	paideia	k1gFnSc1	paideia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
tento	tento	k3xDgInSc4	tento
pedagogický	pedagogický	k2eAgInSc4d1	pedagogický
koncept	koncept	k1gInSc4	koncept
přejímají	přejímat	k5eAaImIp3nP	přejímat
a	a	k8xC	a
v	v	k7c6	v
dílech	díl	k1gInPc6	díl
různých	různý	k2eAgMnPc2d1	různý
myslitelů	myslitel	k1gMnPc2	myslitel
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Varro	Varro	k1gNnSc1	Varro
<g/>
,	,	kIx,	,
Martianus	Martianus	k1gInSc1	Martianus
Capella	Capella	k1gFnSc1	Capella
<g/>
)	)	kIx)	)
postupně	postupně	k6eAd1	postupně
vykrystalizuje	vykrystalizovat	k5eAaPmIp3nS	vykrystalizovat
ve	v	k7c4	v
svobodná	svobodný	k2eAgNnPc4d1	svobodné
umění	umění	k1gNnPc4	umění
(	(	kIx(	(
<g/>
septem	septum	k1gNnSc7	septum
artes	artes	k1gMnSc1	artes
liberales	liberales	k1gMnSc1	liberales
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
přejímá	přejímat	k5eAaImIp3nS	přejímat
středověk	středověk	k1gInSc1	středověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
středověku	středověk	k1gInSc6	středověk
jsou	být	k5eAaImIp3nP	být
důležitými	důležitý	k2eAgMnPc7d1	důležitý
encyklopedisty	encyklopedista	k1gMnPc7	encyklopedista
Cassiodorus	Cassiodorus	k1gInSc4	Cassiodorus
<g/>
,	,	kIx,	,
Isidor	Isidor	k1gMnSc1	Isidor
ze	z	k7c2	z
Sevilly	Sevilla	k1gFnSc2	Sevilla
<g/>
,	,	kIx,	,
Beda	Beda	k1gMnSc1	Beda
Ctihodný	ctihodný	k2eAgMnSc1d1	ctihodný
či	či	k8xC	či
Rabanus	Rabanus	k1gMnSc1	Rabanus
Maurus	Maurus	k1gMnSc1	Maurus
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vrcholném	vrcholný	k2eAgInSc6d1	vrcholný
středověku	středověk	k1gInSc6	středověk
např.	např.	kA	např.
Thierry	Thierra	k1gFnPc1	Thierra
ze	z	k7c2	z
Chartres	Chartresa	k1gFnPc2	Chartresa
<g/>
,	,	kIx,	,
Alexander	Alexandra	k1gFnPc2	Alexandra
Neckam	Neckam	k1gInSc1	Neckam
či	či	k8xC	či
Vincent	Vincent	k1gMnSc1	Vincent
z	z	k7c2	z
Beauvais	Beauvais	k1gFnSc2	Beauvais
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
renesanci	renesance	k1gFnSc6	renesance
a	a	k8xC	a
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
éře	éra	k1gFnSc6	éra
osvícenství	osvícenství	k1gNnSc2	osvícenství
se	se	k3xPyFc4	se
touha	touha	k1gFnSc1	touha
popsat	popsat	k5eAaPmF	popsat
co	co	k3yInSc4	co
nejlépe	dobře	k6eAd3	dobře
a	a	k8xC	a
nejdetailněji	detailně	k6eAd3	detailně
okolní	okolní	k2eAgInSc4d1	okolní
svět	svět	k1gInSc4	svět
objevuje	objevovat	k5eAaImIp3nS	objevovat
znovu	znovu	k6eAd1	znovu
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
rozvíjely	rozvíjet	k5eAaImAgInP	rozvíjet
vědy	věda	k1gFnSc2	věda
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
přírodní	přírodní	k2eAgFnPc4d1	přírodní
vědy	věda	k1gFnPc4	věda
<g/>
)	)	kIx)	)
a	a	k8xC	a
vznikaly	vznikat	k5eAaImAgFnP	vznikat
první	první	k4xOgFnPc1	první
novodobé	novodobý	k2eAgFnPc1d1	novodobá
encyklopedie	encyklopedie	k1gFnPc1	encyklopedie
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
shrnout	shrnout	k5eAaPmF	shrnout
dosavadní	dosavadní	k2eAgNnSc4d1	dosavadní
lidské	lidský	k2eAgNnSc4d1	lidské
vědění	vědění	k1gNnSc4	vědění
a	a	k8xC	a
podat	podat	k5eAaPmF	podat
celkový	celkový	k2eAgInSc4d1	celkový
obraz	obraz	k1gInSc4	obraz
úsilí	úsilí	k1gNnSc2	úsilí
lidského	lidský	k2eAgMnSc2d1	lidský
ducha	duch	k1gMnSc2	duch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc7	první
velkou	velký	k2eAgFnSc7d1	velká
encyklopedií	encyklopedie	k1gFnSc7	encyklopedie
byla	být	k5eAaImAgFnS	být
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
aneb	aneb	k?	aneb
Racionální	racionální	k2eAgInSc4d1	racionální
slovník	slovník	k1gInSc4	slovník
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
umění	umění	k1gNnPc2	umění
a	a	k8xC	a
řemesel	řemeslo	k1gNnPc2	řemeslo
<g/>
,	,	kIx,	,
vydávaná	vydávaný	k2eAgFnSc1d1	vydávaná
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1751	[number]	k4	1751
<g/>
-	-	kIx~	-
<g/>
1772	[number]	k4	1772
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
sestavena	sestavit	k5eAaPmNgFnS	sestavit
Denisem	Denis	k1gInSc7	Denis
Diderotem	Diderot	k1gMnSc7	Diderot
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc7	jeho
spolupracovníky	spolupracovník	k1gMnPc7	spolupracovník
<g/>
,	,	kIx,	,
kterými	který	k3yRgFnPc7	který
byli	být	k5eAaImAgMnP	být
například	například	k6eAd1	například
Jean-Jacques	Jean-Jacques	k1gMnSc1	Jean-Jacques
Rousseau	Rousseau	k1gMnSc1	Rousseau
<g/>
,	,	kIx,	,
Jean	Jean	k1gMnSc1	Jean
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Alembert	Alembert	k1gMnSc1	Alembert
<g/>
,	,	kIx,	,
Voltaire	Voltair	k1gMnSc5	Voltair
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
Louis	Louis	k1gMnSc1	Louis
Montesquieu	Montesquiea	k1gFnSc4	Montesquiea
<g/>
,	,	kIx,	,
Étienne	Étienn	k1gInSc5	Étienn
Bonnot	Bonnota	k1gFnPc2	Bonnota
de	de	k?	de
Condillac	Condillac	k1gInSc1	Condillac
<g/>
,	,	kIx,	,
Claude-Adrien	Claude-Adrien	k2eAgMnSc1d1	Claude-Adrien
Helvétius	Helvétius	k1gMnSc1	Helvétius
či	či	k8xC	či
Paul	Paul	k1gMnSc1	Paul
Heinrich	Heinrich	k1gMnSc1	Heinrich
Dietrich	Dietrich	k1gMnSc1	Dietrich
von	von	k1gInSc4	von
Holbach	Holbacha	k1gFnPc2	Holbacha
<g/>
.	.	kIx.	.
</s>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
osvícenského	osvícenský	k2eAgInSc2d1	osvícenský
racionalismu	racionalismus	k1gInSc2	racionalismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prvními	první	k4xOgMnPc7	první
českými	český	k2eAgMnPc7d1	český
encyklopedisty	encyklopedista	k1gMnPc7	encyklopedista
osvícenské	osvícenský	k2eAgFnSc2d1	osvícenská
epochy	epocha	k1gFnSc2	epocha
byli	být	k5eAaImAgMnP	být
František	František	k1gMnSc1	František
Martin	Martin	k1gMnSc1	Martin
Pelcl	Pelcl	k1gMnSc1	Pelcl
a	a	k8xC	a
Bohumír	Bohumír	k1gMnSc1	Bohumír
Jan	Jan	k1gMnSc1	Jan
Dlabač	Dlabač	k1gMnSc1	Dlabač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Dagmar	Dagmar	k1gFnSc1	Dagmar
Hartmanová	Hartmanová	k1gFnSc1	Hartmanová
<g/>
:	:	kIx,	:
Historie	historie	k1gFnSc1	historie
československé	československý	k2eAgFnSc2d1	Československá
encyklopedistiky	encyklopedistika	k1gFnSc2	encyklopedistika
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
in	in	k?	in
<g/>
:	:	kIx,	:
Národní	národní	k2eAgFnSc1d1	národní
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
:	:	kIx,	:
knihovnická	knihovnický	k2eAgFnSc1d1	knihovnická
revue	revue	k1gFnSc1	revue
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
pp	pp	k?	pp
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1214-0678	[number]	k4	1214-0678
</s>
</p>
<p>
<s>
Dagmar	Dagmar	k1gFnSc1	Dagmar
Hartmanová	Hartmanová	k1gFnSc1	Hartmanová
<g/>
:	:	kIx,	:
Historie	historie	k1gFnSc1	historie
československé	československý	k2eAgFnSc2d1	Československá
encyklopedistiky	encyklopedistika	k1gFnSc2	encyklopedistika
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
in	in	k?	in
<g/>
:	:	kIx,	:
Národní	národní	k2eAgFnSc1d1	národní
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
:	:	kIx,	:
knihovnická	knihovnický	k2eAgFnSc1d1	knihovnická
revue	revue	k1gFnSc1	revue
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
pp	pp	k?	pp
<g/>
.	.	kIx.	.
80	[number]	k4	80
<g/>
–	–	k?	–
<g/>
88	[number]	k4	88
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1214-0678	[number]	k4	1214-0678
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Slovník	slovník	k1gInSc1	slovník
</s>
</p>
<p>
<s>
Lexikon	lexikon	k1gInSc1	lexikon
</s>
</p>
<p>
<s>
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
</s>
</p>
<p>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
aneb	aneb	k?	aneb
Racionální	racionální	k2eAgInSc4d1	racionální
slovník	slovník	k1gInSc4	slovník
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
umění	umění	k1gNnPc2	umění
a	a	k8xC	a
řemesel	řemeslo	k1gNnPc2	řemeslo
</s>
</p>
<p>
<s>
Ottův	Ottův	k2eAgInSc4d1	Ottův
slovník	slovník	k1gInSc4	slovník
naučný	naučný	k2eAgInSc4d1	naučný
</s>
</p>
<p>
<s>
Masarykův	Masarykův	k2eAgInSc4d1	Masarykův
slovník	slovník	k1gInSc4	slovník
naučný	naučný	k2eAgInSc4d1	naučný
</s>
</p>
<p>
<s>
Encyclopæ	Encyclopæ	k?	Encyclopæ
Britannica	Britannica	k1gFnSc1	Britannica
</s>
</p>
<p>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
Jung-le	Junge	k1gFnSc2	Jung-le
</s>
</p>
<p>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
českých	český	k2eAgFnPc2d1	Česká
právních	právní	k2eAgFnPc2d1	právní
dějin	dějiny	k1gFnPc2	dějiny
</s>
</p>
<p>
<s>
Velká	velký	k2eAgFnSc1d1	velká
sovětská	sovětský	k2eAgFnSc1d1	sovětská
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
</s>
</p>
<p>
<s>
Příruční	příruční	k2eAgInSc4d1	příruční
slovník	slovník	k1gInSc4	slovník
naučný	naučný	k2eAgInSc4d1	naučný
</s>
</p>
<p>
<s>
Malá	malý	k2eAgFnSc1d1	malá
československá	československý	k2eAgFnSc1d1	Československá
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
</s>
</p>
<p>
<s>
CO-JAK-PROČ	CO-JAK-PROČ	k?	CO-JAK-PROČ
</s>
</p>
<p>
<s>
Diderot	Diderot	k1gMnSc1	Diderot
(	(	kIx(	(
<g/>
encyklopedie	encyklopedie	k1gFnPc1	encyklopedie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Universum	universum	k1gNnSc1	universum
(	(	kIx(	(
<g/>
encyklopedie	encyklopedie	k1gFnPc1	encyklopedie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Slovník	slovník	k1gInSc1	slovník
(	(	kIx(	(
<g/>
naučný	naučný	k2eAgInSc4d1	naučný
<g/>
)	)	kIx)	)
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
terminologické	terminologický	k2eAgFnSc6d1	terminologická
databázi	databáze	k1gFnSc6	databáze
knihovnictví	knihovnictví	k1gNnSc2	knihovnictví
a	a	k8xC	a
informační	informační	k2eAgFnSc2d1	informační
vědy	věda	k1gFnSc2	věda
(	(	kIx(	(
<g/>
TDKIV	TDKIV	kA	TDKIV
<g/>
)	)	kIx)	)
</s>
</p>
