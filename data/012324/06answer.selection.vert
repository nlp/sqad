<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
έ	έ	k?	έ
enkyklios	enkyklios	k1gInSc4	enkyklios
okružní	okružní	k2eAgInSc4d1	okružní
+	+	kIx~	+
π	π	k?	π
paideia	paideia	k1gFnSc1	paideia
výchova	výchova	k1gFnSc1	výchova
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
strukturované	strukturovaný	k2eAgNnSc1d1	strukturované
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
objemné	objemný	k2eAgNnSc4d1	objemné
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
zevrubně	zevrubně	k6eAd1	zevrubně
představit	představit	k5eAaPmF	představit
lidské	lidský	k2eAgNnSc4d1	lidské
poznání	poznání	k1gNnSc4	poznání
týkající	týkající	k2eAgNnSc4d1	týkající
se	se	k3xPyFc4	se
jednoho	jeden	k4xCgMnSc2	jeden
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
všech	všecek	k3xTgInPc2	všecek
oborů	obor	k1gInPc2	obor
<g/>
.	.	kIx.	.
</s>
