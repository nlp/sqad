<p>
<s>
Joseph-Maurice	Joseph-Maurika	k1gFnSc3	Joseph-Maurika
Ravel	Ravel	k1gMnSc1	Ravel
[	[	kIx(	[
<g/>
žozef	žozef	k1gMnSc1	žozef
móris	móris	k1gFnSc2	móris
ravel	ravel	k1gMnSc1	ravel
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1875	[number]	k4	1875
Ciboure	Cibour	k1gMnSc5	Cibour
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1937	[number]	k4	1937
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
švýcarsko-španělského	švýcarsko-španělský	k2eAgInSc2d1	švýcarsko-španělský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xC	jako
impresionistický	impresionistický	k2eAgMnSc1d1	impresionistický
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zejména	zejména	k9	zejména
jeho	jeho	k3xOp3gNnPc1	jeho
pozdní	pozdní	k2eAgNnPc1d1	pozdní
díla	dílo	k1gNnPc1	dílo
nesou	nést	k5eAaImIp3nP	nést
výrazné	výrazný	k2eAgFnPc4d1	výrazná
stopy	stopa	k1gFnPc4	stopa
nastupující	nastupující	k2eAgFnSc2d1	nastupující
moderny	moderna	k1gFnSc2	moderna
<g/>
,	,	kIx,	,
expresionismu	expresionismus	k1gInSc2	expresionismus
a	a	k8xC	a
neoklasicismu	neoklasicismus	k1gInSc2	neoklasicismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Mládí	mládí	k1gNnSc2	mládí
a	a	k8xC	a
studia	studio	k1gNnSc2	studio
===	===	k?	===
</s>
</p>
<p>
<s>
Otec	otec	k1gMnSc1	otec
Maurice	Maurika	k1gFnSc3	Maurika
Ravela	Ravela	k1gFnSc1	Ravela
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gInSc1	Joseph
<g/>
,	,	kIx,	,
pocházel	pocházet	k5eAaImAgMnS	pocházet
ze	z	k7c2	z
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
inženýr	inženýr	k1gMnSc1	inženýr
na	na	k7c6	na
výstavbě	výstavba	k1gFnSc6	výstavba
železnice	železnice	k1gFnSc2	železnice
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
Delouartovou	Delouartová	k1gFnSc7	Delouartová
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc7	svůj
budoucí	budoucí	k2eAgFnSc7d1	budoucí
ženou	žena	k1gFnSc7	žena
a	a	k8xC	a
matkou	matka	k1gFnSc7	matka
dvou	dva	k4xCgMnPc2	dva
synů	syn	k1gMnPc2	syn
<g/>
,	,	kIx,	,
Maurice	Maurika	k1gFnSc3	Maurika
a	a	k8xC	a
Edouarda	Edouarda	k1gFnSc1	Edouarda
<g/>
.	.	kIx.	.
</s>
<s>
Manželé	manžel	k1gMnPc1	manžel
se	se	k3xPyFc4	se
usadili	usadit	k5eAaPmAgMnP	usadit
na	na	k7c6	na
francouzské	francouzský	k2eAgFnSc6d1	francouzská
straně	strana	k1gFnSc6	strana
Pyrenejí	Pyreneje	k1gFnPc2	Pyreneje
<g/>
,	,	kIx,	,
v	v	k7c6	v
Ciboure	Cibour	k1gMnSc5	Cibour
<g/>
,	,	kIx,	,
poblíž	poblíž	k6eAd1	poblíž
španělských	španělský	k2eAgFnPc2d1	španělská
hranic	hranice	k1gFnPc2	hranice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rodina	rodina	k1gFnSc1	rodina
záhy	záhy	k6eAd1	záhy
po	po	k7c6	po
Ravelově	Ravelův	k2eAgNnSc6d1	Ravelovo
narození	narození	k1gNnSc6	narození
přesídlila	přesídlit	k5eAaPmAgFnS	přesídlit
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
usadila	usadit	k5eAaPmAgFnS	usadit
na	na	k7c6	na
bulváru	bulvár	k1gInSc6	bulvár
Pigalle	Pigalle	k1gFnSc2	Pigalle
<g/>
.	.	kIx.	.
</s>
<s>
Ravel	Ravel	k1gMnSc1	Ravel
studoval	studovat	k5eAaImAgMnS	studovat
hru	hra	k1gFnSc4	hra
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
u	u	k7c2	u
Charlese	Charles	k1gMnSc2	Charles
W.	W.	kA	W.
de	de	k?	de
Bériota	Bériota	k1gFnSc1	Bériota
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
kompozici	kompozice	k1gFnSc4	kompozice
na	na	k7c6	na
Pařížské	pařížský	k2eAgFnSc6d1	Pařížská
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gMnPc4	jeho
učitele	učitel	k1gMnPc4	učitel
patřil	patřit	k5eAaImAgMnS	patřit
např.	např.	kA	např.
Gabriel	Gabriel	k1gMnSc1	Gabriel
Fauré	Faurý	k2eAgFnSc2d1	Faurý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
studoval	studovat	k5eAaImAgMnS	studovat
i	i	k9	i
Ricardo	Ricardo	k1gNnSc4	Ricardo
Viñ	Viñ	k1gMnSc1	Viñ
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnSc1d2	pozdější
interpret	interpret	k1gMnSc1	interpret
jeho	jeho	k3xOp3gFnPc2	jeho
klavírních	klavírní	k2eAgFnPc2d1	klavírní
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Maurice	Maurika	k1gFnSc3	Maurika
Ravel	Ravela	k1gFnPc2	Ravela
se	se	k3xPyFc4	se
neúspěšně	úspěšně	k6eNd1	úspěšně
ucházel	ucházet	k5eAaImAgInS	ucházet
o	o	k7c4	o
Římskou	římský	k2eAgFnSc4d1	římská
cenu	cena	k1gFnSc4	cena
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
podle	podle	k7c2	podle
zadání	zadání	k1gNnSc2	zadání
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
tři	tři	k4xCgMnPc1	tři
dnes	dnes	k6eAd1	dnes
málo	málo	k6eAd1	málo
známé	známý	k2eAgFnSc2d1	známá
kantáty	kantáta	k1gFnSc2	kantáta
–	–	k?	–
Alcyone	Alcyon	k1gMnSc5	Alcyon
<g/>
,	,	kIx,	,
Alyssa	Alyss	k1gMnSc4	Alyss
a	a	k8xC	a
Myrrha	Myrrh	k1gMnSc4	Myrrh
<g/>
;	;	kIx,	;
za	za	k7c4	za
Myrrhu	Myrrha	k1gFnSc4	Myrrha
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
pojal	pojmout	k5eAaPmAgMnS	pojmout
z	z	k7c2	z
ironie	ironie	k1gFnSc2	ironie
trochu	trochu	k6eAd1	trochu
sladkobolně	sladkobolně	k6eAd1	sladkobolně
<g/>
,	,	kIx,	,
dostal	dostat	k5eAaPmAgMnS	dostat
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I	i	k9	i
tato	tento	k3xDgFnSc1	tento
epizoda	epizoda	k1gFnSc1	epizoda
jistě	jistě	k9	jistě
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
později	pozdě	k6eAd2	pozdě
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
Řád	řád	k1gInSc1	řád
Čestné	čestný	k2eAgFnSc2d1	čestná
legie	legie	k1gFnSc2	legie
<g/>
,	,	kIx,	,
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
pocty	pocta	k1gFnSc2	pocta
ovšem	ovšem	k9	ovšem
přijímal	přijímat	k5eAaImAgMnS	přijímat
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
rytířem	rytíř	k1gMnSc7	rytíř
Belgického	belgický	k2eAgNnSc2d1	Belgické
království	království	k1gNnSc2	království
i	i	k8xC	i
doktorem	doktor	k1gMnSc7	doktor
honoris	honoris	k1gFnSc2	honoris
causa	causa	k1gFnSc1	causa
v	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
počátečních	počáteční	k2eAgInPc6d1	počáteční
nezdarech	nezdar	k1gInPc6	nezdar
si	se	k3xPyFc3	se
definitivně	definitivně	k6eAd1	definitivně
získal	získat	k5eAaPmAgMnS	získat
respekt	respekt	k1gInSc4	respekt
Španělskou	španělský	k2eAgFnSc7d1	španělská
rapsodií	rapsodie	k1gFnSc7	rapsodie
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
ovšem	ovšem	k9	ovšem
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
klavírními	klavírní	k2eAgInPc7d1	klavírní
Vodotrysky	vodotrysk	k1gInPc7	vodotrysk
<g/>
,	,	kIx,	,
všeobecně	všeobecně	k6eAd1	všeobecně
oblíbenou	oblíbený	k2eAgFnSc4d1	oblíbená
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
i	i	k9	i
Pavana	pavana	k1gFnSc1	pavana
za	za	k7c4	za
mrtvou	mrtvý	k2eAgFnSc4d1	mrtvá
infantku	infantka	k1gFnSc4	infantka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
Ravel	Ravel	k1gMnSc1	Ravel
dobrovolně	dobrovolně	k6eAd1	dobrovolně
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
ho	on	k3xPp3gInSc4	on
kvůli	kvůli	k7c3	kvůli
malému	malý	k2eAgInSc3d1	malý
vzrůstu	vzrůst	k1gInSc3	vzrůst
a	a	k8xC	a
nízké	nízký	k2eAgFnSc3d1	nízká
váze	váha	k1gFnSc3	váha
málem	málem	k6eAd1	málem
nepřijali	přijmout	k5eNaPmAgMnP	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k9	jako
řidič	řidič	k1gMnSc1	řidič
u	u	k7c2	u
kamionů	kamion	k1gInPc2	kamion
a	a	k8xC	a
ve	v	k7c6	v
volných	volný	k2eAgFnPc6d1	volná
chvílích	chvíle	k1gFnPc6	chvíle
složil	složit	k5eAaPmAgMnS	složit
na	na	k7c6	na
frontě	fronta	k1gFnSc6	fronta
Tři	tři	k4xCgInPc1	tři
sbory	sbor	k1gInPc1	sbor
a	a	k8xC	a
capella	capella	k1gFnSc1	capella
–	–	k?	–
na	na	k7c4	na
vlastní	vlastní	k2eAgInPc4d1	vlastní
texty	text	k1gInPc4	text
<g/>
,	,	kIx,	,
odkazující	odkazující	k2eAgInPc4d1	odkazující
ke	k	k7c3	k
kultuře	kultura	k1gFnSc3	kultura
francouzského	francouzský	k2eAgInSc2d1	francouzský
středověku	středověk	k1gInSc2	středověk
a	a	k8xC	a
renesance	renesance	k1gFnSc2	renesance
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
Montfort	Montfort	k1gInSc1	Montfort
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Amaury	Amaura	k1gFnSc2	Amaura
u	u	k7c2	u
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bydlel	bydlet	k5eAaImAgMnS	bydlet
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
mezitím	mezitím	k6eAd1	mezitím
však	však	k9	však
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
jako	jako	k9	jako
známý	známý	k2eAgMnSc1d1	známý
umělec	umělec	k1gMnSc1	umělec
<g/>
,	,	kIx,	,
cestoval	cestovat	k5eAaImAgMnS	cestovat
a	a	k8xC	a
napsal	napsat	k5eAaPmAgMnS	napsat
ještě	ještě	k6eAd1	ještě
řadu	řada	k1gFnSc4	řada
vynikajících	vynikající	k2eAgNnPc2d1	vynikající
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Skandinávii	Skandinávie	k1gFnSc4	Skandinávie
a	a	k8xC	a
Anglii	Anglie	k1gFnSc4	Anglie
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
USA	USA	kA	USA
a	a	k8xC	a
Kanadu	Kanada	k1gFnSc4	Kanada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Soukromý	soukromý	k2eAgInSc4d1	soukromý
život	život	k1gInSc4	život
===	===	k?	===
</s>
</p>
<p>
<s>
Maurice	Maurika	k1gFnSc3	Maurika
Ravel	Ravela	k1gFnPc2	Ravela
byl	být	k5eAaImAgMnS	být
svobodný	svobodný	k2eAgMnSc1d1	svobodný
<g/>
,	,	kIx,	,
bezdětný	bezdětný	k2eAgMnSc1d1	bezdětný
<g/>
,	,	kIx,	,
liboval	libovat	k5eAaImAgMnS	libovat
si	se	k3xPyFc3	se
v	v	k7c6	v
elegantním	elegantní	k2eAgNnSc6d1	elegantní
oblečení	oblečení	k1gNnSc6	oblečení
a	a	k8xC	a
silných	silný	k2eAgFnPc6d1	silná
cigaretách	cigareta	k1gFnPc6	cigareta
Caporal	Caporal	k1gFnSc2	Caporal
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
otci	otec	k1gMnSc6	otec
<g/>
,	,	kIx,	,
vynálezci	vynálezce	k1gMnSc6	vynálezce
<g/>
,	,	kIx,	,
zdědil	zdědit	k5eAaPmAgInS	zdědit
velký	velký	k2eAgInSc1d1	velký
zájem	zájem	k1gInSc1	zájem
o	o	k7c6	o
hodinové	hodinový	k2eAgFnSc6d1	hodinová
<g/>
,	,	kIx,	,
hrací	hrací	k2eAgFnSc6d1	hrací
i	i	k8xC	i
jiné	jiný	k2eAgInPc1d1	jiný
mechanické	mechanický	k2eAgInPc1d1	mechanický
strojky	strojek	k1gInPc1	strojek
a	a	k8xC	a
hračky	hračka	k1gFnPc1	hračka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
také	také	k9	také
sbíral	sbírat	k5eAaImAgInS	sbírat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
záliby	záliba	k1gFnPc4	záliba
patřil	patřit	k5eAaImAgInS	patřit
chov	chov	k1gInSc1	chov
exotických	exotický	k2eAgFnPc2d1	exotická
koček	kočka	k1gFnPc2	kočka
a	a	k8xC	a
pěstování	pěstování	k1gNnSc4	pěstování
exotických	exotický	k2eAgFnPc2d1	exotická
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
dobrou	dobrý	k2eAgFnSc7d1	dobrá
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
byla	být	k5eAaImAgFnS	být
Ida	Ida	k1gFnSc1	Ida
Rubinsteinová	Rubinsteinová	k1gFnSc1	Rubinsteinová
<g/>
,	,	kIx,	,
ruská	ruský	k2eAgFnSc1d1	ruská
tanečnice	tanečnice	k1gFnSc1	tanečnice
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
mecenáška	mecenáška	k1gFnSc1	mecenáška
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
Ravela	Ravel	k1gMnSc4	Ravel
k	k	k7c3	k
napsání	napsání	k1gNnSc3	napsání
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
nejznámějších	známý	k2eAgNnPc2d3	nejznámější
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
baletu	balet	k1gInSc2	balet
Bolero	bolero	k1gNnSc1	bolero
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
při	při	k7c6	při
jízdě	jízda	k1gFnSc6	jízda
taxíkem	taxík	k1gInSc7	taxík
zranění	zranění	k1gNnSc2	zranění
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
začal	začít	k5eAaPmAgInS	začít
trpět	trpět	k5eAaImF	trpět
apraxií	apraxie	k1gFnSc7	apraxie
a	a	k8xC	a
ačkoliv	ačkoliv	k8xS	ačkoliv
byl	být	k5eAaImAgInS	být
schopen	schopen	k2eAgInSc1d1	schopen
stále	stále	k6eAd1	stále
hudbu	hudba	k1gFnSc4	hudba
vnímat	vnímat	k5eAaImF	vnímat
a	a	k8xC	a
komponovat	komponovat	k5eAaImF	komponovat
<g/>
,	,	kIx,	,
ztratil	ztratit	k5eAaPmAgMnS	ztratit
schopnost	schopnost	k1gFnSc4	schopnost
své	svůj	k3xOyFgFnSc2	svůj
myšlenky	myšlenka	k1gFnSc2	myšlenka
zachytit	zachytit	k5eAaPmF	zachytit
do	do	k7c2	do
not	nota	k1gFnPc2	nota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
ještě	ještě	k6eAd1	ještě
naposledy	naposledy	k6eAd1	naposledy
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Španělsko	Španělsko	k1gNnSc4	Španělsko
a	a	k8xC	a
Maroko	Maroko	k1gNnSc4	Maroko
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1937	[number]	k4	1937
po	po	k7c6	po
nepovedené	povedený	k2eNgFnSc6d1	nepovedená
operaci	operace	k1gFnSc6	operace
mozku	mozek	k1gInSc2	mozek
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pochován	pochován	k2eAgMnSc1d1	pochován
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
v	v	k7c4	v
Levallois-Perret	Levallois-Perret	k1gInSc4	Levallois-Perret
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Styl	styl	k1gInSc1	styl
===	===	k?	===
</s>
</p>
<p>
<s>
Ravel	Ravel	k1gInSc1	Ravel
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
zpočátku	zpočátku	k6eAd1	zpočátku
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
Claudem	Claud	k1gInSc7	Claud
Debussym	Debussymum	k1gNnPc2	Debussymum
<g/>
,	,	kIx,	,
Erikem	Erik	k1gMnSc7	Erik
Satiem	Satius	k1gMnSc7	Satius
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
ruskou	ruský	k2eAgFnSc7d1	ruská
Mocnou	mocný	k2eAgFnSc7d1	mocná
hrstkou	hrstka	k1gFnSc7	hrstka
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
dobovými	dobový	k2eAgMnPc7d1	dobový
skladateli	skladatel	k1gMnPc7	skladatel
jako	jako	k8xC	jako
například	například	k6eAd1	například
Julesem	Jules	k1gInSc7	Jules
Massenetem	Massenet	k1gInSc7	Massenet
anebo	anebo	k8xC	anebo
Emmanuelem	Emmanuel	k1gMnSc7	Emmanuel
Chabrierem	Chabrier	k1gMnSc7	Chabrier
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
schopný	schopný	k2eAgInSc1d1	schopný
syntetizovat	syntetizovat	k5eAaImF	syntetizovat
velmi	velmi	k6eAd1	velmi
rozmanité	rozmanitý	k2eAgInPc4d1	rozmanitý
proudy	proud	k1gInPc4	proud
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
si	se	k3xPyFc3	se
již	již	k6eAd1	již
od	od	k7c2	od
svých	svůj	k3xOyFgNnPc2	svůj
prvních	první	k4xOgNnPc2	první
děl	dělo	k1gNnPc2	dělo
vytvořit	vytvořit	k5eAaPmF	vytvořit
dobře	dobře	k6eAd1	dobře
rozpoznatelný	rozpoznatelný	k2eAgInSc4d1	rozpoznatelný
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pouze	pouze	k6eAd1	pouze
obohacoval	obohacovat	k5eAaImAgMnS	obohacovat
o	o	k7c4	o
další	další	k2eAgInPc4d1	další
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příznačné	příznačný	k2eAgNnSc1d1	příznačné
pro	pro	k7c4	pro
Ravela	Ravel	k1gMnSc4	Ravel
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
vybroušené	vybroušený	k2eAgFnPc1d1	vybroušená
složité	složitý	k2eAgFnPc1d1	složitá
harmonie	harmonie	k1gFnPc1	harmonie
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
tonality	tonalita	k1gFnSc2	tonalita
<g/>
,	,	kIx,	,
časté	častý	k2eAgNnSc1d1	časté
používání	používání	k1gNnSc1	používání
neharmonických	harmonický	k2eNgFnPc2d1	neharmonická
prodlev	prodleva	k1gFnPc2	prodleva
<g/>
,	,	kIx,	,
rafinovanost	rafinovanost	k1gFnSc1	rafinovanost
sazby	sazba	k1gFnSc2	sazba
a	a	k8xC	a
sevřenost	sevřenost	k1gFnSc4	sevřenost
a	a	k8xC	a
přehlednost	přehlednost	k1gFnSc4	přehlednost
formy	forma	k1gFnSc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
Samostatnou	samostatný	k2eAgFnSc4d1	samostatná
kapitolu	kapitola	k1gFnSc4	kapitola
představuje	představovat	k5eAaImIp3nS	představovat
Ravelovo	Ravelův	k2eAgNnSc1d1	Ravelovo
umění	umění	k1gNnSc1	umění
orchestrace	orchestrace	k1gFnSc2	orchestrace
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Ravela	Ravel	k1gMnSc2	Ravel
se	se	k3xPyFc4	se
střídaly	střídat	k5eAaImAgFnP	střídat
impresionistické	impresionistický	k2eAgFnPc1d1	impresionistická
skladby	skladba	k1gFnPc1	skladba
s	s	k7c7	s
neoklasicistními	neoklasicistní	k2eAgInPc7d1	neoklasicistní
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgInPc2	jenž
kladl	klást	k5eAaImAgInS	klást
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
formální	formální	k2eAgFnSc4d1	formální
vytříbenost	vytříbenost	k1gFnSc4	vytříbenost
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
Ravelovy	Ravelův	k2eAgFnSc2d1	Ravelova
hudby	hudba	k1gFnSc2	hudba
sahá	sahat	k5eAaImIp3nS	sahat
od	od	k7c2	od
malátné	malátný	k2eAgFnSc2d1	malátná
křehkosti	křehkost	k1gFnSc2	křehkost
s	s	k7c7	s
odkazy	odkaz	k1gInPc7	odkaz
na	na	k7c4	na
středověkou	středověký	k2eAgFnSc4d1	středověká
a	a	k8xC	a
jinou	jiný	k2eAgFnSc4d1	jiná
historickou	historický	k2eAgFnSc4d1	historická
hudbu	hudba	k1gFnSc4	hudba
přes	přes	k7c4	přes
extrovertní	extrovertní	k2eAgFnSc4d1	extrovertní
a	a	k8xC	a
bujná	bujný	k2eAgNnPc1d1	bujné
allegra	allegro	k1gNnPc1	allegro
odkazující	odkazující	k2eAgNnPc1d1	odkazující
k	k	k7c3	k
španělské	španělský	k2eAgFnSc3d1	španělská
kultuře	kultura	k1gFnSc3	kultura
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
k	k	k7c3	k
lehčí	lehký	k2eAgFnSc3d2	lehčí
francouzské	francouzský	k2eAgFnSc3d1	francouzská
produkci	produkce	k1gFnSc3	produkce
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
až	až	k9	až
k	k	k7c3	k
velmi	velmi	k6eAd1	velmi
tvrdým	tvrdý	k2eAgFnPc3d1	tvrdá
<g/>
,	,	kIx,	,
harmonicky	harmonicky	k6eAd1	harmonicky
výbojným	výbojný	k2eAgFnPc3d1	výbojná
kompozicím	kompozice	k1gFnPc3	kompozice
s	s	k7c7	s
názvuky	názvuk	k1gInPc7	názvuk
expresionismu	expresionismus	k1gInSc2	expresionismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
celé	celá	k1gFnSc6	celá
jeho	jeho	k3xOp3gFnSc6	jeho
tvorbě	tvorba	k1gFnSc6	tvorba
je	být	k5eAaImIp3nS	být
patrný	patrný	k2eAgInSc1d1	patrný
vliv	vliv	k1gInSc1	vliv
lidových	lidový	k2eAgFnPc2d1	lidová
melodií	melodie	k1gFnPc2	melodie
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
iberských	iberský	k2eAgMnPc2d1	iberský
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yQgMnPc3	který
měl	mít	k5eAaImAgInS	mít
přirozeně	přirozeně	k6eAd1	přirozeně
blízko	blízko	k6eAd1	blízko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
<g/>
,	,	kIx,	,
cikánských	cikánský	k2eAgInPc2d1	cikánský
<g/>
,	,	kIx,	,
arabských	arabský	k2eAgInPc2d1	arabský
<g/>
,	,	kIx,	,
židovských	židovský	k2eAgInPc2d1	židovský
<g/>
,	,	kIx,	,
řeckých	řecký	k2eAgInPc2d1	řecký
<g/>
,	,	kIx,	,
maďarských	maďarský	k2eAgInPc2d1	maďarský
<g/>
,	,	kIx,	,
ruských	ruský	k2eAgInPc2d1	ruský
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Ravel	Ravel	k1gMnSc1	Ravel
byl	být	k5eAaImAgMnS	být
okouzlen	okouzlit	k5eAaPmNgMnS	okouzlit
světem	svět	k1gInSc7	svět
staré	starý	k2eAgFnSc2d1	stará
Vídně	Vídeň	k1gFnSc2	Vídeň
čehož	což	k3yRnSc2	což
ohlasem	ohlas	k1gInSc7	ohlas
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gFnSc4	jeho
klavírní	klavírní	k2eAgMnSc1d1	klavírní
Valses	Valses	k1gMnSc1	Valses
nobles	noblesa	k1gFnPc2	noblesa
et	et	k?	et
sentimentales	sentimentalesa	k1gFnPc2	sentimentalesa
a	a	k8xC	a
především	především	k6eAd1	především
symfonická	symfonický	k2eAgFnSc1d1	symfonická
báseň	báseň	k1gFnSc1	báseň
La	la	k1gNnSc2	la
Valse	Valse	k1gFnSc2	Valse
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
turné	turné	k1gNnSc6	turné
po	po	k7c6	po
USA	USA	kA	USA
a	a	k8xC	a
Kanadě	Kanada	k1gFnSc6	Kanada
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
se	s	k7c7	s
skladatelem	skladatel	k1gMnSc7	skladatel
Georgem	Georg	k1gMnSc7	Georg
Gershwinem	Gershwin	k1gMnSc7	Gershwin
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ho	on	k3xPp3gMnSc4	on
zavedl	zavést	k5eAaPmAgMnS	zavést
do	do	k7c2	do
Harlemu	Harlem	k1gInSc2	Harlem
na	na	k7c4	na
koncert	koncert	k1gInSc4	koncert
Duke	Duk	k1gMnSc2	Duk
Ellingtona	Ellington	k1gMnSc2	Ellington
<g/>
.	.	kIx.	.
</s>
<s>
Ravelovo	Ravelův	k2eAgNnSc1d1	Ravelovo
okouzlení	okouzlení	k1gNnSc1	okouzlení
jazzem	jazz	k1gInSc7	jazz
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
prvky	prvek	k1gInPc7	prvek
americké	americký	k2eAgFnSc2d1	americká
hudby	hudba	k1gFnSc2	hudba
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
projevilo	projevit	k5eAaPmAgNnS	projevit
i	i	k9	i
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
dalších	další	k2eAgFnPc6d1	další
skladbách	skladba	k1gFnPc6	skladba
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
opeře	opera	k1gFnSc6	opera
Dítě	Dítě	k1gMnPc2	Dítě
a	a	k8xC	a
kouzla	kouzlo	k1gNnSc2	kouzlo
a	a	k8xC	a
Druhé	druhý	k4xOgNnSc1	druhý
sonátě	sonáta	k1gFnSc3	sonáta
pro	pro	k7c4	pro
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
dvacátých	dvacátý	k4xOgNnPc6	dvacátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc1	jeho
styl	styl	k1gInSc1	styl
výrazně	výrazně	k6eAd1	výrazně
posouvá	posouvat	k5eAaImIp3nS	posouvat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
expresionismu	expresionismus	k1gInSc3	expresionismus
-	-	kIx~	-
jde	jít	k5eAaImIp3nS	jít
zejména	zejména	k9	zejména
o	o	k7c4	o
tvrdší	tvrdý	k2eAgFnSc4d2	tvrdší
disonantnost	disonantnost	k1gFnSc4	disonantnost
souzvuků	souzvuk	k1gInPc2	souzvuk
a	a	k8xC	a
nové	nový	k2eAgInPc4d1	nový
způsoby	způsob	k1gInPc4	způsob
instrumentace	instrumentace	k1gFnSc2	instrumentace
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
zejména	zejména	k9	zejména
Madagaskarských	madagaskarský	k2eAgFnPc2d1	madagaskarská
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
Dua	duo	k1gNnSc2	duo
pro	pro	k7c4	pro
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
violoncello	violoncello	k1gNnSc4	violoncello
<g/>
.	.	kIx.	.
</s>
<s>
Orchestrace	orchestrace	k1gFnSc1	orchestrace
u	u	k7c2	u
Ravela	Ravel	k1gMnSc2	Ravel
bývá	bývat	k5eAaImIp3nS	bývat
tak	tak	k6eAd1	tak
barvitá	barvitý	k2eAgFnSc1d1	barvitá
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
dobová	dobový	k2eAgFnSc1d1	dobová
kritika	kritika	k1gFnSc1	kritika
vytýkala	vytýkat	k5eAaImAgFnS	vytýkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
to	ten	k3xDgNnSc1	ten
první	první	k4xOgNnSc1	první
<g/>
,	,	kIx,	,
čeho	co	k3yInSc2	co
si	se	k3xPyFc3	se
u	u	k7c2	u
jeho	jeho	k3xOp3gFnPc2	jeho
skladeb	skladba	k1gFnPc2	skladba
člověk	člověk	k1gMnSc1	člověk
všímá	všímat	k5eAaImIp3nS	všímat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
tvorbě	tvorba	k1gFnSc6	tvorba
Ravel	Ravel	k1gMnSc1	Ravel
nepoužíval	používat	k5eNaImAgMnS	používat
často	často	k6eAd1	často
symfonickou	symfonický	k2eAgFnSc4d1	symfonická
formu	forma	k1gFnSc4	forma
a	a	k8xC	a
vyhýbal	vyhýbat	k5eAaImAgMnS	vyhýbat
se	se	k3xPyFc4	se
náboženským	náboženský	k2eAgNnPc3d1	náboženské
tématům	téma	k1gNnPc3	téma
<g/>
.	.	kIx.	.
</s>
<s>
Ravelovo	Ravelův	k2eAgNnSc1d1	Ravelovo
dílo	dílo	k1gNnSc1	dílo
není	být	k5eNaImIp3nS	být
početně	početně	k6eAd1	početně
velké	velký	k2eAgNnSc1d1	velké
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stalo	stát	k5eAaPmAgNnS	stát
se	s	k7c7	s
synonymem	synonymum	k1gNnSc7	synonymum
dokonalé	dokonalý	k2eAgFnSc2d1	dokonalá
skladatelské	skladatelský	k2eAgFnSc2d1	skladatelská
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
znovu	znovu	k6eAd1	znovu
prováděno	provádět	k5eAaImNgNnS	provádět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Klavírní	klavírní	k2eAgFnSc1d1	klavírní
tvorba	tvorba	k1gFnSc1	tvorba
===	===	k?	===
</s>
</p>
<p>
<s>
Le	Le	k?	Le
Menuet	menuet	k1gInSc1	menuet
antique	antique	k1gFnSc1	antique
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
orchestrováno	orchestrovat	k5eAaBmNgNnS	orchestrovat
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pavana	pavana	k1gFnSc1	pavana
za	za	k7c4	za
mrtvou	mrtvý	k2eAgFnSc4d1	mrtvá
infantku	infantka	k1gFnSc4	infantka
(	(	kIx(	(
<g/>
Pavane	Pavan	k1gMnSc5	Pavan
pour	pour	k1gInSc1	pour
une	une	k?	une
infante	infant	k1gMnSc5	infant
défunte	défunt	k1gMnSc5	défunt
<g/>
,	,	kIx,	,
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vodotrysky	vodotrysk	k1gInPc1	vodotrysk
(	(	kIx(	(
<g/>
Jeux	Jeux	k1gInSc1	Jeux
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
eau	eau	k?	eau
<g/>
,	,	kIx,	,
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
La	la	k1gNnSc1	la
Sonatine	Sonatin	k1gInSc5	Sonatin
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
–	–	k?	–
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zrcadla	zrcadlo	k1gNnPc1	zrcadlo
(	(	kIx(	(
<g/>
Mirroirs	Mirroirs	k1gInSc1	Mirroirs
<g/>
,	,	kIx,	,
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kašpar	Kašpar	k1gMnSc1	Kašpar
noci	noc	k1gFnSc2	noc
(	(	kIx(	(
<g/>
Gaspard	Gaspard	k1gInSc1	Gaspard
de	de	k?	de
la	la	k1gNnSc2	la
nuit	nuita	k1gFnPc2	nuita
<g/>
,	,	kIx,	,
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
-	-	kIx~	-
temný	temný	k2eAgInSc1d1	temný
klavírní	klavírní	k2eAgInSc1d1	klavírní
triptych	triptych	k1gInSc1	triptych
inspirovaný	inspirovaný	k2eAgInSc1d1	inspirovaný
básníkem	básník	k1gMnSc7	básník
Aloysiem	Aloysius	k1gMnSc7	Aloysius
Bertrandem	Bertrand	k1gInSc7	Bertrand
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
matka	matka	k1gFnSc1	matka
husa	husa	k1gFnSc1	husa
(	(	kIx(	(
<g/>
Ma	Ma	k1gFnSc1	Ma
mè	mè	k?	mè
L	L	kA	L
<g/>
́	́	k?	́
<g/>
Oye	Oye	k1gMnSc1	Oye
<g/>
,	,	kIx,	,
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
-	-	kIx~	-
čtyřruční	čtyřruční	k2eAgInSc1d1	čtyřruční
cyklus	cyklus	k1gInSc1	cyklus
podle	podle	k7c2	podle
starých	starý	k2eAgFnPc2d1	stará
francouzských	francouzský	k2eAgFnPc2d1	francouzská
pohádek	pohádka	k1gFnPc2	pohádka
</s>
</p>
<p>
<s>
Valčíky	valčík	k1gInPc1	valčík
vznešené	vznešený	k2eAgFnSc2d1	vznešená
a	a	k8xC	a
tesklivé	tesklivý	k2eAgFnSc2d1	tesklivá
(	(	kIx(	(
<g/>
Valses	Valsesa	k1gFnPc2	Valsesa
nobles	noblesa	k1gFnPc2	noblesa
et	et	k?	et
sentimentales	sentimentalesa	k1gFnPc2	sentimentalesa
<g/>
,	,	kIx,	,
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Náhrobek	náhrobek	k1gInSc1	náhrobek
Couperinův	Couperinův	k2eAgInSc1d1	Couperinův
(	(	kIx(	(
<g/>
Le	Le	k1gFnSc1	Le
Tombeau	Tombeaus	k1gInSc2	Tombeaus
de	de	k?	de
Couperin	Couperin	k1gInSc1	Couperin
<g/>
,	,	kIx,	,
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
–	–	k?	–
neoklasicistní	neoklasicistní	k2eAgFnSc1d1	neoklasicistní
suita	suita	k1gFnSc1	suita
starých	starý	k2eAgInPc2d1	starý
francouzských	francouzský	k2eAgInPc2d1	francouzský
tanců	tanec	k1gInPc2	tanec
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
věnován	věnovat	k5eAaImNgInS	věnovat
konkrétnímu	konkrétní	k2eAgMnSc3d1	konkrétní
Ravelovu	Ravelův	k2eAgMnSc3d1	Ravelův
spolubojovníkovi	spolubojovník	k1gMnSc3	spolubojovník
z	z	k7c2	z
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vokální	vokální	k2eAgFnSc1d1	vokální
tvorba	tvorba	k1gFnSc1	tvorba
===	===	k?	===
</s>
</p>
<p>
<s>
Ballade	Ballad	k1gInSc5	Ballad
de	de	k?	de
la	la	k1gNnSc6	la
reine	reinout	k5eAaPmIp3nS	reinout
morte	morit	k5eAaImRp2nP	morit
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
aimer	aimer	k1gMnSc1	aimer
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sainte	Saint	k1gMnSc5	Saint
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Chanson	Chanson	k1gMnSc1	Chanson
du	du	k?	du
rouet	rouet	k1gMnSc1	rouet
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Myrrha	Myrrha	k1gFnSc1	Myrrha
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
–	–	k?	–
kantáta	kantáta	k1gFnSc1	kantáta
</s>
</p>
<p>
<s>
Alcyone	Alcyon	k1gMnSc5	Alcyon
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
–	–	k?	–
kantáta	kantáta	k1gFnSc1	kantáta
</s>
</p>
<p>
<s>
Alyssa	Alyssa	k1gFnSc1	Alyssa
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
–	–	k?	–
kantáta	kantáta	k1gFnSc1	kantáta
</s>
</p>
<p>
<s>
Manteau	Manteau	k6eAd1	Manteau
de	de	k?	de
fleurs	fleurs	k1gInSc1	fleurs
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Shéhérazade	Shéhérazást	k5eAaPmIp3nS	Shéhérazást
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
–	–	k?	–
tři	tři	k4xCgFnPc4	tři
písně	píseň	k1gFnPc1	píseň
pro	pro	k7c4	pro
soprán	soprán	k1gInSc4	soprán
a	a	k8xC	a
orchestr	orchestr	k1gInSc4	orchestr
<g/>
,	,	kIx,	,
nejrozsáhlejší	rozsáhlý	k2eAgNnSc4d3	nejrozsáhlejší
dílo	dílo	k1gNnSc4	dílo
raného	raný	k2eAgNnSc2d1	rané
období	období	k1gNnSc2	období
</s>
</p>
<p>
<s>
Historky	historka	k1gFnPc1	historka
z	z	k7c2	z
přírodopisu	přírodopis	k1gInSc2	přírodopis
(	(	kIx(	(
<g/>
Histoires	Histoires	k1gMnSc1	Histoires
naturelles	naturelles	k1gMnSc1	naturelles
<g/>
,	,	kIx,	,
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
-	-	kIx~	-
při	při	k7c6	při
prvním	první	k4xOgInSc6	první
provedení	provedení	k1gNnSc6	provedení
způsobily	způsobit	k5eAaPmAgFnP	způsobit
skandál	skandál	k1gInSc1	skandál
</s>
</p>
<p>
<s>
Trois	Trois	k1gFnSc1	Trois
poè	poè	k?	poè
de	de	k?	de
Stéphane	Stéphan	k1gMnSc5	Stéphan
Mallarmé	Mallarmý	k2eAgInPc4d1	Mallarmý
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
-	-	kIx~	-
často	často	k6eAd1	často
nejceněnější	ceněný	k2eAgNnSc1d3	ceněný
Ravelovo	Ravelův	k2eAgNnSc1d1	Ravelovo
dílo	dílo	k1gNnSc1	dílo
ovlivněné	ovlivněný	k2eAgNnSc1d1	ovlivněné
Arnoldem	Arnold	k1gMnSc7	Arnold
Schönbergem	Schönberg	k1gMnSc7	Schönberg
</s>
</p>
<p>
<s>
Tři	tři	k4xCgInPc1	tři
sbory	sbor	k1gInPc1	sbor
a	a	k8xC	a
capella	capella	k1gFnSc1	capella
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Chansons	Chansons	k1gInSc1	Chansons
Madécasses	Madécasses	k1gInSc1	Madécasses
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
-	-	kIx~	-
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejdisonantnějších	disonantní	k2eAgNnPc2d3	disonantní
Ravelových	Ravelův	k2eAgNnPc2d1	Ravelovo
děl	dělo	k1gNnPc2	dělo
</s>
</p>
<p>
<s>
Don	Don	k1gMnSc1	Don
Quichotte	Quichott	k1gInSc5	Quichott
et	et	k?	et
Dulcinée	Dulcinée	k1gNnPc1	Dulcinée
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Morgiane	Morgianout	k5eAaPmIp3nS	Morgianout
–	–	k?	–
nedokončené	dokončený	k2eNgNnSc4d1	nedokončené
oratorium	oratorium	k1gNnSc4	oratorium
<g/>
/	/	kIx~	/
<g/>
balet	balet	k1gInSc1	balet
</s>
</p>
<p>
<s>
===	===	k?	===
Komorní	komorní	k2eAgFnSc1d1	komorní
tvorba	tvorba	k1gFnSc1	tvorba
===	===	k?	===
</s>
</p>
<p>
<s>
Sonáta	sonáta	k1gFnSc1	sonáta
č.	č.	k?	č.
1	[number]	k4	1
pro	pro	k7c4	pro
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Smyčcový	smyčcový	k2eAgInSc1d1	smyčcový
kvartet	kvartet	k1gInSc1	kvartet
v	v	k7c6	v
F	F	kA	F
dur	dur	k1gNnSc1	dur
</s>
</p>
<p>
<s>
Introduction	Introduction	k1gInSc1	Introduction
et	et	k?	et
Allegro	allegro	k6eAd1	allegro
pro	pro	k7c4	pro
harfu	harfa	k1gFnSc4	harfa
a	a	k8xC	a
komorní	komorní	k2eAgInSc1d1	komorní
ansámbl	ansámbl	k1gInSc1	ansámbl
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Trio	trio	k1gNnSc1	trio
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
violoncello	violoncello	k1gNnSc4	violoncello
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Cikán	cikán	k1gMnSc1	cikán
(	(	kIx(	(
<g/>
Tzigane	Tzigan	k1gMnSc5	Tzigan
<g/>
,	,	kIx,	,
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
–	–	k?	–
pro	pro	k7c4	pro
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
piano-luthéal	pianouthéal	k1gInSc4	piano-luthéal
</s>
</p>
<p>
<s>
Sonáta	sonáta	k1gFnSc1	sonáta
č.	č.	k?	č.
2	[number]	k4	2
pro	pro	k7c4	pro
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Orchestrální	orchestrální	k2eAgFnSc1d1	orchestrální
tvorba	tvorba	k1gFnSc1	tvorba
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
mnoho	mnoho	k4c4	mnoho
orchestrálních	orchestrální	k2eAgFnPc2d1	orchestrální
skladeb	skladba	k1gFnPc2	skladba
i	i	k8xC	i
baletů	balet	k1gInPc2	balet
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
dodatečnou	dodatečný	k2eAgFnSc7d1	dodatečná
instrumentací	instrumentace	k1gFnSc7	instrumentace
původně	původně	k6eAd1	původně
klavírních	klavírní	k2eAgFnPc2d1	klavírní
skladeb	skladba	k1gFnPc2	skladba
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Shéhérazade	Shéhérazást	k5eAaPmIp3nS	Shéhérazást
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
–	–	k?	–
předehra	předehra	k1gFnSc1	předehra
k	k	k7c3	k
nikdy	nikdy	k6eAd1	nikdy
nedopsané	dopsaný	k2eNgFnSc3d1	nedopsaná
opeře	opera	k1gFnSc3	opera
</s>
</p>
<p>
<s>
Španělská	španělský	k2eAgFnSc1d1	španělská
rapsodie	rapsodie	k1gFnSc1	rapsodie
(	(	kIx(	(
<g/>
Rapsodie	rapsodie	k1gFnSc1	rapsodie
espagnole	espagnole	k1gFnSc1	espagnole
<g/>
,	,	kIx,	,
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dafnis	Dafnis	k1gMnSc1	Dafnis
a	a	k8xC	a
Chloé	Chloé	k1gNnPc2	Chloé
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
–	–	k?	–
balet	balet	k1gInSc1	balet
pro	pro	k7c4	pro
taneční	taneční	k2eAgInSc4d1	taneční
soubor	soubor	k1gInSc4	soubor
Sergeje	Sergej	k1gMnSc2	Sergej
Ďagileva	Ďagilev	k1gMnSc2	Ďagilev
<g/>
,	,	kIx,	,
nejrozsáhlejší	rozsáhlý	k2eAgNnSc4d3	nejrozsáhlejší
Ravelovo	Ravelův	k2eAgNnSc4d1	Ravelovo
dílo	dílo	k1gNnSc4	dílo
</s>
</p>
<p>
<s>
La	la	k1gNnSc1	la
Valse	Valse	k1gFnSc2	Valse
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Fanfára	fanfára	k1gFnSc1	fanfára
do	do	k7c2	do
"	"	kIx"	"
<g/>
Janina	Janin	k2eAgInSc2d1	Janin
vějíře	vějíř	k1gInSc2	vějíř
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Fanfare	Fanfar	k1gMnSc5	Fanfar
pour	pour	k1gMnSc1	pour
"	"	kIx"	"
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Éventail	Éventail	k1gMnSc1	Éventail
de	de	k?	de
Jeanne	Jeann	k1gMnSc5	Jeann
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bolero	bolero	k1gNnSc1	bolero
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Koncert	koncert	k1gInSc1	koncert
pro	pro	k7c4	pro
levou	levý	k2eAgFnSc4d1	levá
ruku	ruka	k1gFnSc4	ruka
(	(	kIx(	(
<g/>
Concerto	Concerta	k1gFnSc5	Concerta
pour	pour	k1gInSc4	pour
la	la	k1gNnSc3	la
main	maina	k1gFnPc2	maina
gauche	gauch	k1gFnSc2	gauch
<g/>
,	,	kIx,	,
1929	[number]	k4	1929
–	–	k?	–
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
–	–	k?	–
pro	pro	k7c4	pro
pianistu	pianista	k1gMnSc4	pianista
Paula	Paul	k1gMnSc4	Paul
Wittgensteina	Wittgenstein	k1gMnSc4	Wittgenstein
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ztratil	ztratit	k5eAaPmAgMnS	ztratit
pravou	pravý	k2eAgFnSc4d1	pravá
ruku	ruka	k1gFnSc4	ruka
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
</s>
</p>
<p>
<s>
Klavírní	klavírní	k2eAgInSc4d1	klavírní
koncert	koncert	k1gInSc4	koncert
G	G	kA	G
dur	dur	k1gNnSc1	dur
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
–	–	k?	–
věnovaný	věnovaný	k2eAgInSc1d1	věnovaný
francouzské	francouzský	k2eAgFnSc3d1	francouzská
pianistce	pianistka	k1gFnSc3	pianistka
a	a	k8xC	a
interpretce	interpretka	k1gFnSc6	interpretka
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
Marguerite	Marguerit	k1gInSc5	Marguerit
Longové	Longový	k2eAgFnPc1d1	Longová
</s>
</p>
<p>
<s>
===	===	k?	===
Opery	opera	k1gFnSc2	opera
===	===	k?	===
</s>
</p>
<p>
<s>
Španělská	španělský	k2eAgFnSc1d1	španělská
hodinka	hodinka	k1gFnSc1	hodinka
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
́	́	k?	́
<g/>
Heure	Heur	k1gInSc5	Heur
espagnole	espagnola	k1gFnSc6	espagnola
<g/>
,	,	kIx,	,
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
–	–	k?	–
jednoaktová	jednoaktový	k2eAgFnSc1d1	jednoaktová
opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgFnSc1d1	hudební
komedie	komedie	k1gFnSc1	komedie
s	s	k7c7	s
brilantní	brilantní	k2eAgFnSc7d1	brilantní
instrumentací	instrumentace	k1gFnSc7	instrumentace
hodinového	hodinový	k2eAgInSc2d1	hodinový
krámku	krámek	k1gInSc2	krámek
</s>
</p>
<p>
<s>
Dítě	dítě	k1gNnSc1	dítě
a	a	k8xC	a
kouzla	kouzlo	k1gNnSc2	kouzlo
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
́	́	k?	́
<g/>
Enfant	Enfant	k1gInSc1	Enfant
et	et	k?	et
les	les	k1gInSc1	les
sortilè	sortilè	k?	sortilè
<g/>
,	,	kIx,	,
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
–	–	k?	–
opera	opera	k1gFnSc1	opera
<g/>
/	/	kIx~	/
<g/>
balet	balet	k1gInSc1	balet
o	o	k7c6	o
dvou	dva	k4xCgFnPc6	dva
částech	část	k1gFnPc6	část
na	na	k7c4	na
libreto	libreto	k1gNnSc4	libreto
Colette	Colett	k1gMnSc5	Colett
</s>
</p>
<p>
<s>
===	===	k?	===
Úpravy	úprava	k1gFnPc1	úprava
cizích	cizí	k2eAgNnPc2d1	cizí
děl	dělo	k1gNnPc2	dělo
===	===	k?	===
</s>
</p>
<p>
<s>
Aranžmá	aranžmá	k1gNnSc1	aranžmá
řeckých	řecký	k2eAgFnPc2d1	řecká
lidových	lidový	k2eAgFnPc2d1	lidová
písní	píseň	k1gFnPc2	píseň
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Debussyho	Debussyze	k6eAd1	Debussyze
Nokturna	nokturna	k1gFnSc1	nokturna
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
–	–	k?	–
přepis	přepis	k1gInSc4	přepis
pro	pro	k7c4	pro
dva	dva	k4xCgInPc4	dva
klavíry	klavír	k1gInPc4	klavír
</s>
</p>
<p>
<s>
Chovanština	Chovanština	k1gFnSc1	Chovanština
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
–	–	k?	–
orchestrace	orchestrace	k1gFnSc2	orchestrace
Musorgského	Musorgského	k2eAgFnSc2d1	Musorgského
opery	opera	k1gFnSc2	opera
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
z	z	k7c2	z
výstavy	výstava	k1gFnSc2	výstava
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
–	–	k?	–
orchestrace	orchestrace	k1gFnSc2	orchestrace
Musorgského	Musorgského	k2eAgFnSc2d1	Musorgského
suity	suita	k1gFnSc2	suita
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
HOLZKNECHT	HOLZKNECHT	kA	HOLZKNECHT
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Maurice	Maurika	k1gFnSc3	Maurika
Ravel	Ravel	k1gInSc4	Ravel
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Supraphon	supraphon	k1gInSc1	supraphon
(	(	kIx(	(
<g/>
Hudební	hudební	k2eAgInPc1d1	hudební
profily	profil	k1gInPc1	profil
–	–	k?	–
Svazek	svazek	k1gInSc1	svazek
15	[number]	k4	15
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
211	[number]	k4	211
s.	s.	k?	s.
</s>
</p>
<p>
<s>
NICHOLS	NICHOLS	kA	NICHOLS
<g/>
,	,	kIx,	,
Roger	Roger	k1gInSc1	Roger
<g/>
.	.	kIx.	.
</s>
<s>
Ravel	Ravel	k1gInSc1	Ravel
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Yale	Yale	k1gInSc1	Yale
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
430	[number]	k4	430
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3001	[number]	k4	3001
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
882	[number]	k4	882
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Maurice	Maurika	k1gFnSc3	Maurika
Ravel	Ravel	k1gInSc4	Ravel
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Maurice	Maurika	k1gFnSc3	Maurika
Ravel	Ravel	k1gInSc4	Ravel
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Volně	volně	k6eAd1	volně
přístupné	přístupný	k2eAgFnPc4d1	přístupná
partitury	partitura	k1gFnPc4	partitura
děl	dít	k5eAaBmAgMnS	dít
od	od	k7c2	od
M.	M.	kA	M.
Ravela	Ravela	k1gFnSc1	Ravela
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
IMSLP	IMSLP	kA	IMSLP
</s>
</p>
<p>
<s>
Maurice	Maurika	k1gFnSc3	Maurika
Ravel	Ravel	k1gInSc4	Ravel
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
Musopen	Musopen	k2eAgMnSc1d1	Musopen
</s>
</p>
<p>
<s>
Maurice	Maurika	k1gFnSc3	Maurika
Ravel	Ravel	k1gInSc4	Ravel
v	v	k7c6	v
internetové	internetový	k2eAgFnSc6d1	internetová
encyklopedii	encyklopedie	k1gFnSc6	encyklopedie
CoJeCo	CoJeCo	k1gMnSc1	CoJeCo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
