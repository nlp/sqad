<p>
<s>
Tónový	tónový	k2eAgInSc1d1	tónový
jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
fonologické	fonologický	k2eAgFnSc2d1	fonologická
typologie	typologie	k1gFnSc2	typologie
takový	takový	k3xDgInSc4	takový
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
má	mít	k5eAaImIp3nS	mít
tón	tón	k1gInSc4	tón
rozlišovací	rozlišovací	k2eAgInSc4d1	rozlišovací
(	(	kIx(	(
<g/>
distinktivní	distinktivní	k2eAgInSc4d1	distinktivní
<g/>
)	)	kIx)	)
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tón	tón	k1gInSc1	tón
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInSc7	jaký
je	být	k5eAaImIp3nS	být
slovo	slovo	k1gNnSc1	slovo
proneseno	pronesen	k2eAgNnSc1d1	proneseno
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
zásadní	zásadní	k2eAgInSc4d1	zásadní
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
rozlišení	rozlišení	k1gNnSc4	rozlišení
významu	význam	k1gInSc2	význam
různých	různý	k2eAgNnPc2d1	různé
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jinak	jinak	k6eAd1	jinak
znějí	znět	k5eAaImIp3nP	znět
stejně	stejně	k6eAd1	stejně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jazyky	jazyk	k1gInPc1	jazyk
mohou	moct	k5eAaImIp3nP	moct
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
výšku	výška	k1gFnSc4	výška
tónu	tón	k1gInSc2	tón
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vysoký	vysoký	k2eAgMnSc1d1	vysoký
<g/>
,	,	kIx,	,
střední	střední	k2eAgMnSc1d1	střední
<g/>
,	,	kIx,	,
nízký	nízký	k2eAgInSc1d1	nízký
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
průběh	průběh	k1gInSc4	průběh
tónu	tón	k1gInSc2	tón
(	(	kIx(	(
<g/>
např.	např.	kA	např.
stoupající	stoupající	k2eAgFnPc1d1	stoupající
<g/>
,	,	kIx,	,
klesající	klesající	k2eAgFnPc1d1	klesající
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
obojí	obojí	k4xRgInPc1	obojí
<g/>
.	.	kIx.	.
</s>
<s>
Nerozhoduje	rozhodovat	k5eNaImIp3nS	rozhodovat
absolutní	absolutní	k2eAgFnSc1d1	absolutní
výška	výška	k1gFnSc1	výška
tónu	tón	k1gInSc2	tón
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
relativní	relativní	k2eAgFnSc2d1	relativní
vůči	vůči	k7c3	vůči
ostatním	ostatní	k2eAgInPc3d1	ostatní
tónům	tón	k1gInPc3	tón
<g/>
.	.	kIx.	.
</s>
<s>
Vysoký	vysoký	k2eAgInSc1d1	vysoký
tón	tón	k1gInSc1	tón
tedy	tedy	k9	tedy
neznamená	znamenat	k5eNaImIp3nS	znamenat
například	například	k6eAd1	například
vysoké	vysoký	k2eAgNnSc4d1	vysoké
cé	cé	k?	cé
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
tón	tón	k1gInSc4	tón
vyšší	vysoký	k2eAgInSc4d2	vyšší
než	než	k8xS	než
střední	střední	k2eAgInSc4d1	střední
nebo	nebo	k8xC	nebo
nízký	nízký	k2eAgInSc4d1	nízký
tón	tón	k1gInSc4	tón
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tónovými	Tónův	k2eAgInPc7d1	Tónův
jazyky	jazyk	k1gInPc7	jazyk
převážně	převážně	k6eAd1	převážně
hovoří	hovořit	k5eAaImIp3nS	hovořit
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
zemí	zem	k1gFnPc2	zem
v	v	k7c6	v
subsaharské	subsaharský	k2eAgFnSc6d1	subsaharská
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
v	v	k7c6	v
Mezoamerice	Mezoamerika	k1gFnSc6	Mezoamerika
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
tónového	tónový	k2eAgInSc2d1	tónový
jazyka	jazyk	k1gInSc2	jazyk
je	být	k5eAaImIp3nS	být
čínština	čínština	k1gFnSc1	čínština
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
určitý	určitý	k2eAgInSc4d1	určitý
charakter	charakter	k1gInSc4	charakter
tónového	tónový	k2eAgInSc2d1	tónový
jazyka	jazyk	k1gInSc2	jazyk
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
čeština	čeština	k1gFnSc1	čeština
(	(	kIx(	(
<g/>
v	v	k7c6	v
omezené	omezený	k2eAgFnSc6d1	omezená
míře	míra	k1gFnSc6	míra
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
)	)	kIx)	)
-	-	kIx~	-
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
tvoření	tvoření	k1gNnSc2	tvoření
otázky	otázka	k1gFnSc2	otázka
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
čistě	čistě	k6eAd1	čistě
založené	založený	k2eAgNnSc1d1	založené
na	na	k7c6	na
tónu	tón	k1gInSc6	tón
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
protikladu	protiklad	k1gInSc6	protiklad
k	k	k7c3	k
angličtině	angličtina	k1gFnSc3	angličtina
či	či	k8xC	či
němčině	němčina	k1gFnSc6	němčina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
otázka	otázka	k1gFnSc1	otázka
tvoří	tvořit	k5eAaImIp3nS	tvořit
čistě	čistě	k6eAd1	čistě
gramaticky	gramaticky	k6eAd1	gramaticky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uplatňování	uplatňování	k1gNnSc1	uplatňování
tónu	tón	k1gInSc2	tón
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
suprasegmentální	suprasegmentální	k2eAgInPc4d1	suprasegmentální
jevy	jev	k1gInPc4	jev
<g/>
.	.	kIx.	.
</s>
</p>
