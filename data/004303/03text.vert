<s>
Národní	národní	k2eAgInSc4d1	národní
park	park	k1gInSc4	park
České	český	k2eAgNnSc1d1	české
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
hlavním	hlavní	k2eAgInSc7d1	hlavní
předmětem	předmět	k1gInSc7	předmět
ochrany	ochrana	k1gFnSc2	ochrana
jsou	být	k5eAaImIp3nP	být
unikátní	unikátní	k2eAgInPc4d1	unikátní
pískovcové	pískovcový	k2eAgInPc4d1	pískovcový
útvary	útvar	k1gInPc4	útvar
a	a	k8xC	a
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
vázaný	vázaný	k2eAgInSc1d1	vázaný
biotop	biotop	k1gInSc1	biotop
<g/>
.	.	kIx.	.
</s>
<s>
Mohutné	mohutný	k2eAgFnPc1d1	mohutná
skalní	skalní	k2eAgFnPc1d1	skalní
věže	věž	k1gFnPc1	věž
<g/>
,	,	kIx,	,
brány	brána	k1gFnPc1	brána
<g/>
,	,	kIx,	,
stěny	stěna	k1gFnPc1	stěna
<g/>
,	,	kIx,	,
rokle	rokle	k1gFnPc1	rokle
<g/>
,	,	kIx,	,
města	město	k1gNnPc1	město
a	a	k8xC	a
bludiště	bludiště	k1gNnSc1	bludiště
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
erozí	eroze	k1gFnPc2	eroze
křídových	křídový	k2eAgInPc2d1	křídový
mořských	mořský	k2eAgInPc2d1	mořský
sedimentů	sediment	k1gInPc2	sediment
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
vyzdviženy	vyzdvihnout	k5eAaPmNgInP	vyzdvihnout
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
v	v	k7c6	v
období	období	k1gNnSc6	období
čtvrtohor	čtvrtohory	k1gFnPc2	čtvrtohory
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
alpinského	alpinský	k2eAgNnSc2d1	alpinské
vrásnění	vrásnění	k1gNnSc2	vrásnění
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
skalním	skalní	k2eAgInSc7d1	skalní
útvarem	útvar	k1gInSc7	útvar
je	být	k5eAaImIp3nS	být
Pravčická	Pravčický	k2eAgFnSc1d1	Pravčická
brána	brána	k1gFnSc1	brána
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
symbolem	symbol	k1gInSc7	symbol
parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
status	status	k1gInSc4	status
národní	národní	k2eAgFnSc2d1	národní
přírodní	přírodní	k2eAgFnSc2d1	přírodní
památky	památka	k1gFnSc2	památka
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
největší	veliký	k2eAgFnSc4d3	veliký
pískovcovou	pískovcový	k2eAgFnSc4d1	pískovcová
skalní	skalní	k2eAgFnSc4d1	skalní
bránu	brána	k1gFnSc4	brána
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
ní	on	k3xPp3gFnSc2	on
byl	být	k5eAaImAgMnS	být
postaven	postaven	k2eAgInSc4d1	postaven
zámeček	zámeček	k1gInSc4	zámeček
Sokolí	sokolí	k2eAgNnSc1d1	sokolí
hnízdo	hnízdo	k1gNnSc1	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Děčín	Děčín	k1gInSc1	Děčín
mezi	mezi	k7c7	mezi
obcemi	obec	k1gFnPc7	obec
Hřensko	Hřensko	k1gNnSc1	Hřensko
<g/>
,	,	kIx,	,
Chřibská	chřibský	k2eAgFnSc1d1	Chřibská
a	a	k8xC	a
vesnicí	vesnice	k1gFnSc7	vesnice
Brtníky	brtník	k1gMnPc4	brtník
<g/>
.	.	kIx.	.
</s>
<s>
Zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
oblast	oblast	k1gFnSc4	oblast
při	při	k7c6	při
státní	státní	k2eAgFnSc6d1	státní
hranici	hranice	k1gFnSc6	hranice
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ho	on	k3xPp3gMnSc4	on
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
od	od	k7c2	od
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
Saské	saský	k2eAgNnSc1d1	Saské
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
parku	park	k1gInSc2	park
činí	činit	k5eAaImIp3nS	činit
79,23	[number]	k4	79,23
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
náleží	náležet	k5eAaImIp3nS	náležet
ke	k	k7c3	k
geomorfologickému	geomorfologický	k2eAgInSc3d1	geomorfologický
celku	celek	k1gInSc3	celek
Děčínské	děčínský	k2eAgFnSc2d1	Děčínská
vrchoviny	vrchovina	k1gFnSc2	vrchovina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Krušnohorské	krušnohorský	k2eAgFnSc2d1	Krušnohorská
hornatiny	hornatina	k1gFnSc2	hornatina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
strany	strana	k1gFnSc2	strana
park	park	k1gInSc1	park
obepíná	obepínat	k5eAaImIp3nS	obepínat
Chráněná	chráněný	k2eAgFnSc1d1	chráněná
krajinná	krajinný	k2eAgFnSc1d1	krajinná
oblast	oblast	k1gFnSc1	oblast
Labské	labský	k2eAgInPc1d1	labský
pískovce	pískovec	k1gInPc1	pískovec
<g/>
,	,	kIx,	,
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
strany	strana	k1gFnSc2	strana
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
přiléhá	přiléhat	k5eAaImIp3nS	přiléhat
Chráněná	chráněný	k2eAgFnSc1d1	chráněná
krajinná	krajinný	k2eAgFnSc1d1	krajinná
oblast	oblast	k1gFnSc1	oblast
Lužické	lužický	k2eAgFnSc2d1	Lužická
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parku	park	k1gInSc6	park
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jediná	jediný	k2eAgFnSc1d1	jediná
vesnice	vesnice	k1gFnSc1	vesnice
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
Mezná	mezný	k2eAgFnSc1d1	Mezná
patřící	patřící	k2eAgFnSc1d1	patřící
k	k	k7c3	k
obci	obec	k1gFnSc3	obec
Hřensko	Hřensko	k1gNnSc1	Hřensko
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
zřícenina	zřícenina	k1gFnSc1	zřícenina
vodního	vodní	k2eAgInSc2d1	vodní
mlýna	mlýn	k1gInSc2	mlýn
a	a	k8xC	a
několika	několik	k4yIc2	několik
skalních	skalní	k2eAgInPc2d1	skalní
hrádků	hrádek	k1gInPc2	hrádek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Falkenštejn	Falkenštejn	k1gNnSc1	Falkenštejn
<g/>
,	,	kIx,	,
Šaunštejn	Šaunštejn	k1gNnSc1	Šaunštejn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lesy	les	k1gInPc1	les
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
97	[number]	k4	97
%	%	kIx~	%
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
dominantní	dominantní	k2eAgInSc4d1	dominantní
buk	buk	k1gInSc4	buk
lesní	lesní	k2eAgFnSc2d1	lesní
byl	být	k5eAaImAgMnS	být
vytlačen	vytlačit	k5eAaPmNgMnS	vytlačit
dnes	dnes	k6eAd1	dnes
silně	silně	k6eAd1	silně
převažujícím	převažující	k2eAgInSc7d1	převažující
smrkem	smrk	k1gInSc7	smrk
ztepilým	ztepilý	k2eAgInSc7d1	ztepilý
<g/>
.	.	kIx.	.
</s>
<s>
Bylinné	bylinný	k2eAgNnSc1d1	bylinné
patro	patro	k1gNnSc1	patro
zde	zde	k6eAd1	zde
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
druhově	druhově	k6eAd1	druhově
rozmanité	rozmanitý	k2eAgFnPc1d1	rozmanitá
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
v	v	k7c6	v
patru	patro	k1gNnSc6	patro
mechovém	mechový	k2eAgNnSc6d1	mechové
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
stovky	stovka	k1gFnPc1	stovka
druhů	druh	k1gInPc2	druh
mechů	mech	k1gInPc2	mech
a	a	k8xC	a
lišejníků	lišejník	k1gInPc2	lišejník
<g/>
.	.	kIx.	.
</s>
<s>
Kdysi	kdysi	k6eAd1	kdysi
známou	známý	k2eAgFnSc7d1	známá
rostlinou	rostlina	k1gFnSc7	rostlina
parku	park	k1gInSc2	park
byl	být	k5eAaImAgMnS	být
blánatec	blánatec	k1gMnSc1	blánatec
kentský	kentský	k2eAgMnSc1d1	kentský
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
parku	park	k1gInSc6	park
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
řada	řada	k1gFnSc1	řada
živočichů	živočich	k1gMnPc2	živočich
typu	typ	k1gInSc2	typ
los	los	k1gInSc1	los
evropský	evropský	k2eAgInSc1d1	evropský
či	či	k8xC	či
kočka	kočka	k1gFnSc1	kočka
divoká	divoký	k2eAgFnSc1d1	divoká
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Českém	český	k2eAgNnSc6d1	české
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
desítky	desítka	k1gFnPc1	desítka
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
za	za	k7c4	za
všechny	všechen	k3xTgInPc4	všechen
lze	lze	k6eAd1	lze
jmenovat	jmenovat	k5eAaBmF	jmenovat
čápa	čáp	k1gMnSc2	čáp
černého	černý	k1gMnSc2	černý
nebo	nebo	k8xC	nebo
sokola	sokol	k1gMnSc2	sokol
stěhovavého	stěhovavý	k2eAgMnSc2d1	stěhovavý
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
zdejší	zdejší	k2eAgFnSc1d1	zdejší
populace	populace	k1gFnSc1	populace
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgInSc4d1	národní
park	park	k1gInSc4	park
České	český	k2eAgNnSc1d1	české
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
v	v	k7c6	v
Ústeckém	ústecký	k2eAgInSc6d1	ústecký
kraji	kraj	k1gInSc6	kraj
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Děčín	Děčín	k1gInSc1	Děčín
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
Šluknovského	šluknovský	k2eAgInSc2d1	šluknovský
výběžku	výběžek	k1gInSc2	výběžek
<g/>
.	.	kIx.	.
</s>
<s>
Zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
do	do	k7c2	do
území	území	k1gNnSc6	území
několika	několik	k4yIc2	několik
samosprávných	samosprávný	k2eAgInPc2d1	samosprávný
celků	celek	k1gInPc2	celek
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
měst	město	k1gNnPc2	město
Krásná	krásný	k2eAgFnSc1d1	krásná
Lípa	lípa	k1gFnSc1	lípa
a	a	k8xC	a
Chřibská	chřibský	k2eAgFnSc1d1	Chřibská
<g/>
,	,	kIx,	,
a	a	k8xC	a
obcí	obec	k1gFnSc7	obec
Hřensko	Hřensko	k1gNnSc1	Hřensko
<g/>
,	,	kIx,	,
Janov	Janov	k1gInSc1	Janov
<g/>
,	,	kIx,	,
Růžová	růžový	k2eAgFnSc1d1	růžová
<g/>
,	,	kIx,	,
Srbská	srbský	k2eAgFnSc1d1	Srbská
Kamenice	Kamenice	k1gFnSc1	Kamenice
<g/>
,	,	kIx,	,
Jetřichovice	Jetřichovice	k1gFnSc1	Jetřichovice
<g/>
,	,	kIx,	,
Doubice	Doubice	k1gFnSc1	Doubice
a	a	k8xC	a
Staré	Staré	k2eAgMnPc4d1	Staré
Křečany	Křečan	k1gMnPc4	Křečan
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
parku	park	k1gInSc2	park
je	být	k5eAaImIp3nS	být
79,23	[number]	k4	79,23
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Takřka	takřka	k6eAd1	takřka
celou	celá	k1gFnSc4	celá
severní	severní	k2eAgFnSc2d1	severní
a	a	k8xC	a
části	část	k1gFnSc2	část
západní	západní	k2eAgFnSc7d1	západní
stranou	strana	k1gFnSc7	strana
přiléhá	přiléhat	k5eAaImIp3nS	přiléhat
k	k	k7c3	k
německé	německý	k2eAgFnSc3d1	německá
hranici	hranice	k1gFnSc3	hranice
<g/>
,	,	kIx,	,
za	za	k7c2	za
níž	jenž	k3xRgFnSc2	jenž
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
navazuje	navazovat	k5eAaImIp3nS	navazovat
národní	národní	k2eAgInSc4d1	národní
park	park	k1gInSc4	park
Saské	saský	k2eAgNnSc1d1	Saské
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
(	(	kIx(	(
<g/>
Nationalpark	Nationalpark	k1gInSc1	Nationalpark
Sächsische	Sächsische	k1gNnSc1	Sächsische
Schweiz	Schweiz	k1gInSc1	Schweiz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
svou	svůj	k3xOyFgFnSc4	svůj
rozlohou	rozloha	k1gFnSc7	rozloha
93,5	[number]	k4	93,5
km2	km2	k4	km2
ještě	ještě	k6eAd1	ještě
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
velikost	velikost	k1gFnSc4	velikost
Českého	český	k2eAgNnSc2d1	české
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnSc4d1	východní
stranu	strana	k1gFnSc4	strana
parku	park	k1gInSc2	park
lemuje	lemovat	k5eAaImIp3nS	lemovat
Chráněná	chráněný	k2eAgFnSc1d1	chráněná
krajinná	krajinný	k2eAgFnSc1d1	krajinná
oblast	oblast	k1gFnSc1	oblast
Lužické	lužický	k2eAgFnSc2d1	Lužická
hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc2	jeho
ostatní	ostatní	k2eAgFnSc2d1	ostatní
strany	strana	k1gFnSc2	strana
obepíná	obepínat	k5eAaImIp3nS	obepínat
Chráněná	chráněný	k2eAgFnSc1d1	chráněná
krajinná	krajinný	k2eAgFnSc1d1	krajinná
oblast	oblast	k1gFnSc1	oblast
Labské	labský	k2eAgInPc1d1	labský
pískovce	pískovec	k1gInPc1	pískovec
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yIgFnSc4	který
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
rovněž	rovněž	k9	rovněž
navazuje	navazovat	k5eAaImIp3nS	navazovat
německý	německý	k2eAgInSc1d1	německý
protějšek	protějšek	k1gInSc1	protějšek
–	–	k?	–
Chráněná	chráněný	k2eAgFnSc1d1	chráněná
krajinná	krajinný	k2eAgFnSc1d1	krajinná
oblast	oblast	k1gFnSc1	oblast
Saské	saský	k2eAgNnSc1d1	Saské
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
(	(	kIx(	(
<g/>
LSG	LSG	kA	LSG
Sächsische	Sächsische	k1gInSc1	Sächsische
Schweiz	Schweiz	k1gInSc1	Schweiz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
obou	dva	k4xCgFnPc2	dva
chráněných	chráněný	k2eAgFnPc2d1	chráněná
krajinných	krajinný	k2eAgFnPc2d1	krajinná
oblasti	oblast	k1gFnSc2	oblast
včetně	včetně	k7c2	včetně
obou	dva	k4xCgInPc2	dva
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
Českosaské	českosaský	k2eAgNnSc1d1	Českosaské
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
(	(	kIx(	(
<g/>
Sächsisch-Böhmische	Sächsisch-Böhmische	k1gNnSc1	Sächsisch-Böhmische
Schweiz	Schweiza	k1gFnPc2	Schweiza
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Labské	labský	k2eAgInPc1d1	labský
pískovce	pískovec	k1gInPc1	pískovec
(	(	kIx(	(
<g/>
Elbsandsteingebirge	Elbsandsteingebirge	k1gFnSc1	Elbsandsteingebirge
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Německé	německý	k2eAgNnSc1d1	německé
a	a	k8xC	a
české	český	k2eAgNnSc1d1	české
území	území	k1gNnSc1	území
Labských	labský	k2eAgInPc2d1	labský
pískovců	pískovec	k1gInPc2	pískovec
společně	společně	k6eAd1	společně
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
evropských	evropský	k2eAgFnPc2d1	Evropská
pískovcových	pískovcový	k2eAgFnPc2d1	pískovcová
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
České	český	k2eAgNnSc1d1	české
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
"	"	kIx"	"
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
odvozením	odvození	k1gNnSc7	odvození
od	od	k7c2	od
názvu	název	k1gInSc2	název
Saské	saský	k2eAgNnSc1d1	Saské
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
používaném	používaný	k2eAgMnSc6d1	používaný
pro	pro	k7c4	pro
německou	německý	k2eAgFnSc4d1	německá
část	část	k1gFnSc4	část
Labských	labský	k2eAgInPc2d1	labský
pískovců	pískovec	k1gInPc2	pískovec
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tento	tento	k3xDgInSc4	tento
romantický	romantický	k2eAgInSc4d1	romantický
název	název	k1gInSc4	název
se	se	k3xPyFc4	se
zasloužili	zasloužit	k5eAaPmAgMnP	zasloužit
švýcarští	švýcarský	k2eAgMnPc1d1	švýcarský
malíři	malíř	k1gMnPc1	malíř
Adrian	Adrian	k1gMnSc1	Adrian
Zingg	Zingg	k1gMnSc1	Zingg
a	a	k8xC	a
Anton	Anton	k1gMnSc1	Anton
Graff	Graff	k1gMnSc1	Graff
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
působili	působit	k5eAaImAgMnP	působit
na	na	k7c6	na
Akademii	akademie	k1gFnSc6	akademie
v	v	k7c6	v
Drážďanech	Drážďany	k1gInPc6	Drážďany
a	a	k8xC	a
malovali	malovat	k5eAaImAgMnP	malovat
krajinu	krajina	k1gFnSc4	krajina
podél	podél	k7c2	podél
Labe	Labe	k1gNnSc2	Labe
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jim	on	k3xPp3gMnPc3	on
připomínala	připomínat	k5eAaImAgFnS	připomínat
domovinu	domovina	k1gFnSc4	domovina
<g/>
.	.	kIx.	.
</s>
<s>
Českému	český	k2eAgNnSc3d1	české
Švýcarsku	Švýcarsko	k1gNnSc3	Švýcarsko
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
říká	říkat	k5eAaImIp3nS	říkat
Zadní	zadní	k2eAgFnPc4d1	zadní
hory	hora	k1gFnPc4	hora
–	–	k?	–
tímto	tento	k3xDgNnSc7	tento
jménem	jméno	k1gNnSc7	jméno
krajinu	krajina	k1gFnSc4	krajina
obdařil	obdařit	k5eAaPmAgMnS	obdařit
liberecký	liberecký	k2eAgMnSc1d1	liberecký
spisovatel	spisovatel	k1gMnSc1	spisovatel
Miloslav	Miloslav	k1gMnSc1	Miloslav
Nevrlý	nevrlý	k2eAgMnSc1d1	nevrlý
<g/>
.	.	kIx.	.
</s>
<s>
Labské	labský	k2eAgInPc1d1	labský
pískovce	pískovec	k1gInPc1	pískovec
jsou	být	k5eAaImIp3nP	být
nejsevernějším	severní	k2eAgInSc7d3	nejsevernější
výběžkem	výběžek	k1gInSc7	výběžek
české	český	k2eAgFnSc2d1	Česká
křídové	křídový	k2eAgFnSc2d1	křídová
pánve	pánev	k1gFnSc2	pánev
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
většiny	většina	k1gFnSc2	většina
jsou	být	k5eAaImIp3nP	být
formovány	formován	k2eAgInPc1d1	formován
turonskými	turonský	k2eAgInPc7d1	turonský
pískovci	pískovec	k1gInPc7	pískovec
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
tvoří	tvořit	k5eAaImIp3nP	tvořit
výrazné	výrazný	k2eAgFnPc4d1	výrazná
krajinné	krajinný	k2eAgFnPc4d1	krajinná
dominanty	dominanta	k1gFnPc4	dominanta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
svrchní	svrchní	k2eAgFnSc2d1	svrchní
křídy	křída	k1gFnSc2	křída
<g/>
,	,	kIx,	,
před	před	k7c7	před
cca	cca	kA	cca
90	[number]	k4	90
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
pokrývalo	pokrývat	k5eAaImAgNnS	pokrývat
celé	celý	k2eAgNnSc1d1	celé
území	území	k1gNnSc1	území
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
dně	dno	k1gNnSc6	dno
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
navršily	navršit	k5eAaPmAgInP	navršit
sedimenty	sediment	k1gInPc1	sediment
o	o	k7c6	o
mocnosti	mocnost	k1gFnSc6	mocnost
až	až	k9	až
1	[number]	k4	1
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
sedimenty	sediment	k1gInPc1	sediment
byly	být	k5eAaImAgInP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
především	především	k6eAd1	především
pískovci	pískovec	k1gInPc7	pískovec
a	a	k8xC	a
slínovci	slínovec	k1gInPc7	slínovec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
prachovci	prachovec	k1gInSc3	prachovec
a	a	k8xC	a
slepenci	slepenec	k1gInSc3	slepenec
<g/>
.	.	kIx.	.
</s>
<s>
Písek	Písek	k1gInSc1	Písek
sem	sem	k6eAd1	sem
byl	být	k5eAaImAgInS	být
přiváděn	přiváděn	k2eAgInSc1d1	přiváděn
řekami	řeka	k1gFnPc7	řeka
z	z	k7c2	z
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
nachází	nacházet	k5eAaImIp3nP	nacházet
Jizerské	jizerský	k2eAgFnPc1d1	Jizerská
hory	hora	k1gFnPc1	hora
a	a	k8xC	a
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnSc1d1	střední
vrstva	vrstva	k1gFnSc1	vrstva
mocného	mocný	k2eAgInSc2d1	mocný
sedimentu	sediment	k1gInSc2	sediment
byla	být	k5eAaImAgNnP	být
tvořena	tvořit	k5eAaImNgNnP	tvořit
výhradně	výhradně	k6eAd1	výhradně
pískovci	pískovec	k1gInPc7	pískovec
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vrstva	vrstva	k1gFnSc1	vrstva
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
mocnosti	mocnost	k1gFnSc3	mocnost
350	[number]	k4	350
<g/>
–	–	k?	–
<g/>
420	[number]	k4	420
m	m	kA	m
<g/>
,	,	kIx,	,
a	a	k8xC	a
právě	právě	k9	právě
ona	onen	k3xDgFnSc1	onen
dala	dát	k5eAaPmAgFnS	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
členitým	členitý	k2eAgMnPc3d1	členitý
skalním	skalní	k2eAgMnPc3d1	skalní
útvarům	útvar	k1gInPc3	útvar
v	v	k7c6	v
národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
<g/>
.	.	kIx.	.
</s>
<s>
Pískovce	pískovec	k1gInPc1	pískovec
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Českosaského	českosaský	k2eAgNnSc2d1	Českosaské
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořen	k2eAgInPc1d1	tvořen
"	"	kIx"	"
<g/>
kvádry	kvádr	k1gInPc1	kvádr
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
od	od	k7c2	od
sebe	se	k3xPyFc2	se
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
pukliny	puklina	k1gFnSc2	puklina
<g/>
,	,	kIx,	,
pročež	pročež	k6eAd1	pročež
jsou	být	k5eAaImIp3nP	být
označovány	označován	k2eAgInPc1d1	označován
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
kvádrové	kvádrový	k2eAgInPc1d1	kvádrový
pískovce	pískovec	k1gInPc1	pískovec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
období	období	k1gNnSc4	období
třetihor	třetihory	k1gFnPc2	třetihory
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
silná	silný	k2eAgFnSc1d1	silná
vulkanická	vulkanický	k2eAgFnSc1d1	vulkanická
činnost	činnost	k1gFnSc1	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
dala	dát	k5eAaPmAgFnS	dát
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vzniknout	vzniknout	k5eAaPmF	vzniknout
mnohým	mnohý	k2eAgFnPc3d1	mnohá
dominantám	dominanta	k1gFnPc3	dominanta
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
tu	tu	k6eAd1	tu
nejvvyšší	nejvvyšší	k1gNnPc2	nejvvyšší
představuje	představovat	k5eAaImIp3nS	představovat
Růžovský	Růžovský	k2eAgInSc1d1	Růžovský
vrch	vrch	k1gInSc1	vrch
(	(	kIx(	(
<g/>
619	[number]	k4	619
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čedičový	čedičový	k2eAgInSc1d1	čedičový
kužel	kužel	k1gInSc1	kužel
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
pronikl	proniknout	k5eAaPmAgInS	proniknout
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
skrze	skrze	k?	skrze
pískovec	pískovec	k1gInSc1	pískovec
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vulkanické	vulkanický	k2eAgFnSc2d1	vulkanická
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
hor	hora	k1gFnPc2	hora
a	a	k8xC	a
kopců	kopec	k1gInPc2	kopec
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vulkanických	vulkanický	k2eAgFnPc2d1	vulkanická
elevací	elevace	k1gFnPc2	elevace
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
Mlýny	mlýn	k1gInPc1	mlýn
<g/>
,	,	kIx,	,
Suchý	suchý	k2eAgInSc1d1	suchý
vrch	vrch	k1gInSc1	vrch
<g/>
,	,	kIx,	,
Koliště	koliště	k1gNnSc1	koliště
<g/>
,	,	kIx,	,
Limberk	Limberk	k1gInSc1	Limberk
<g/>
,	,	kIx,	,
Vosí	vosí	k2eAgInSc1d1	vosí
vrch	vrch	k1gInSc1	vrch
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
vrch	vrch	k1gInSc1	vrch
<g/>
,	,	kIx,	,
Větrný	větrný	k2eAgInSc1d1	větrný
vrch	vrch	k1gInSc1	vrch
či	či	k8xC	či
Popovičský	Popovičský	k2eAgInSc1d1	Popovičský
vrch	vrch	k1gInSc1	vrch
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
dnešního	dnešní	k2eAgInSc2d1	dnešní
stavu	stav	k1gInSc2	stav
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
výrazně	výrazně	k6eAd1	výrazně
členitého	členitý	k2eAgInSc2d1	členitý
terénu	terén	k1gInSc2	terén
s	s	k7c7	s
dominujícími	dominující	k2eAgInPc7d1	dominující
pískovcovými	pískovcový	k2eAgInPc7d1	pískovcový
masivy	masiv	k1gInPc7	masiv
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
klíčové	klíčový	k2eAgNnSc1d1	klíčové
období	období	k1gNnSc1	období
čtvrtohor	čtvrtohory	k1gFnPc2	čtvrtohory
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
alpinského	alpinský	k2eAgNnSc2d1	alpinské
vrásnění	vrásnění	k1gNnSc2	vrásnění
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Českosaského	českosaský	k2eAgNnSc2d1	Českosaské
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
k	k	k7c3	k
mohutnému	mohutný	k2eAgInSc3d1	mohutný
tektonickému	tektonický	k2eAgInSc3d1	tektonický
zdvihu	zdvih	k1gInSc3	zdvih
pískovcových	pískovcový	k2eAgInPc2d1	pískovcový
sedimentů	sediment	k1gInPc2	sediment
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
pohyby	pohyb	k1gInPc1	pohyb
tektonických	tektonický	k2eAgFnPc2d1	tektonická
desek	deska	k1gFnPc2	deska
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
častým	častý	k2eAgNnSc7d1	časté
střídáním	střídání	k1gNnSc7	střídání
ledových	ledový	k2eAgFnPc2d1	ledová
a	a	k8xC	a
meziledových	meziledový	k2eAgFnPc2d1	meziledová
dob	doba	k1gFnPc2	doba
a	a	k8xC	a
především	především	k9	především
díky	díky	k7c3	díky
vodním	vodní	k2eAgInPc3d1	vodní
živlům	živel	k1gInPc3	živel
rychle	rychle	k6eAd1	rychle
postupovala	postupovat	k5eAaImAgFnS	postupovat
eroze	eroze	k1gFnSc1	eroze
místních	místní	k2eAgFnPc2d1	místní
hornin	hornina	k1gFnPc2	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
erozi	eroze	k1gFnSc6	eroze
pískovců	pískovec	k1gInPc2	pískovec
měly	mít	k5eAaImAgInP	mít
vliv	vliv	k1gInSc4	vliv
i	i	k8xC	i
další	další	k2eAgInPc4d1	další
přírodní	přírodní	k2eAgInPc4d1	přírodní
děje	děj	k1gInPc4	děj
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
klimatické	klimatický	k2eAgFnPc4d1	klimatická
změny	změna	k1gFnPc4	změna
a	a	k8xC	a
zvětrávání	zvětrávání	k1gNnSc4	zvětrávání
<g/>
.	.	kIx.	.
</s>
<s>
Vrstva	vrstva	k1gFnSc1	vrstva
pískovce	pískovec	k1gInSc2	pískovec
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
silná	silný	k2eAgFnSc1d1	silná
ještě	ještě	k9	ještě
několik	několik	k4yIc1	několik
stovek	stovka	k1gFnPc2	stovka
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
navazuje	navazovat	k5eAaImIp3nS	navazovat
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
a	a	k8xC	a
vodu	voda	k1gFnSc4	voda
nepropustná	propustný	k2eNgFnSc1d1	nepropustná
vrstva	vrstva	k1gFnSc1	vrstva
žuly	žula	k1gFnSc2	žula
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgInSc1d1	zdejší
pískovec	pískovec	k1gInSc1	pískovec
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
porézní	porézní	k2eAgInSc1d1	porézní
a	a	k8xC	a
nasákavý	nasákavý	k2eAgInSc1d1	nasákavý
materiál	materiál	k1gInSc1	materiál
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jakmile	jakmile	k8xS	jakmile
začne	začít	k5eAaPmIp3nS	začít
pršet	pršet	k5eAaImF	pršet
<g/>
,	,	kIx,	,
voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
už	už	k6eAd1	už
nemá	mít	k5eNaImIp3nS	mít
kam	kam	k6eAd1	kam
vsakovat	vsakovat	k5eAaImF	vsakovat
(	(	kIx(	(
<g/>
žula	žula	k1gFnSc1	žula
vodu	voda	k1gFnSc4	voda
nepropustí	propustit	k5eNaPmIp3nS	propustit
a	a	k8xC	a
pískovec	pískovec	k1gInSc1	pískovec
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
nasáklý	nasáklý	k2eAgInSc1d1	nasáklý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jen	jen	k9	jen
přispívá	přispívat	k5eAaImIp3nS	přispívat
ke	k	k7c3	k
zvýšené	zvýšený	k2eAgFnSc3d1	zvýšená
vlhkosti	vlhkost	k1gFnSc3	vlhkost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
převažuje	převažovat	k5eAaImIp3nS	převažovat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
roklích	rokle	k1gFnPc6	rokle
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bývá	bývat	k5eAaImIp3nS	bývat
nedostatek	nedostatek	k1gInSc1	nedostatek
slunečních	sluneční	k2eAgInPc2d1	sluneční
paprsků	paprsek	k1gInPc2	paprsek
<g/>
.	.	kIx.	.
</s>
<s>
Výše	vysoce	k6eAd2	vysoce
popsaným	popsaný	k2eAgInSc7d1	popsaný
způsobem	způsob	k1gInSc7	způsob
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
skalní	skalní	k2eAgInPc1d1	skalní
útvary	útvar	k1gInPc1	útvar
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Evropy	Evropa	k1gFnSc2	Evropa
unikátní	unikátní	k2eAgFnSc2d1	unikátní
<g/>
.	.	kIx.	.
</s>
<s>
Pískovec	pískovec	k1gInSc1	pískovec
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
natolik	natolik	k6eAd1	natolik
dominantní	dominantní	k2eAgFnSc1d1	dominantní
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
stav	stav	k1gInSc4	stav
vžilo	vžít	k5eAaPmAgNnS	vžít
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
pískovcový	pískovcový	k2eAgInSc1d1	pískovcový
fenomén	fenomén	k1gInSc1	fenomén
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
hluboce	hluboko	k6eAd1	hluboko
zařezaných	zařezaný	k2eAgInPc2d1	zařezaný
kaňonů	kaňon	k1gInPc2	kaňon
Labe	Labe	k1gNnSc2	Labe
<g/>
,	,	kIx,	,
Kamenice	Kamenice	k1gFnSc2	Kamenice
a	a	k8xC	a
Křinice	Křinice	k1gFnSc2	Křinice
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nP	nacházet
vysoké	vysoký	k2eAgFnPc1d1	vysoká
skalní	skalní	k2eAgFnPc1d1	skalní
stěny	stěna	k1gFnPc1	stěna
a	a	k8xC	a
věže	věž	k1gFnPc1	věž
<g/>
,	,	kIx,	,
plošiny	plošina	k1gFnPc1	plošina
<g/>
,	,	kIx,	,
převisy	převis	k1gInPc1	převis
a	a	k8xC	a
římsy	římsa	k1gFnPc1	římsa
<g/>
,	,	kIx,	,
skalní	skalní	k2eAgMnPc1d1	skalní
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
bludiště	bludiště	k1gNnSc2	bludiště
a	a	k8xC	a
výjimkou	výjimka	k1gFnSc7	výjimka
nejsou	být	k5eNaImIp3nP	být
ani	ani	k8xC	ani
hřibovité	hřibovitý	k2eAgInPc1d1	hřibovitý
útvary	útvar	k1gInPc1	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
nejznámějším	známý	k2eAgInSc7d3	nejznámější
skalním	skalní	k2eAgInSc7d1	skalní
útvarem	útvar	k1gInSc7	útvar
na	na	k7c6	na
území	území	k1gNnSc6	území
parku	park	k1gInSc2	park
je	být	k5eAaImIp3nS	být
Pravčická	Pravčický	k2eAgFnSc1d1	Pravčická
brána	brána	k1gFnSc1	brána
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozpětím	rozpětí	k1gNnSc7	rozpětí
26,5	[number]	k4	26,5
m	m	kA	m
a	a	k8xC	a
výškou	výška	k1gFnSc7	výška
16	[number]	k4	16
m	m	kA	m
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc7d3	veliký
přirozenou	přirozený	k2eAgFnSc7d1	přirozená
skalní	skalní	k2eAgFnSc7d1	skalní
branou	brána	k1gFnSc7	brána
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgFnSc7d2	menší
obdobou	obdoba	k1gFnSc7	obdoba
Pravčické	Pravčický	k2eAgFnSc2d1	Pravčická
brány	brána	k1gFnSc2	brána
je	být	k5eAaImIp3nS	být
Malá	malý	k2eAgFnSc1d1	malá
Pravčická	Pravčický	k2eAgFnSc1d1	Pravčická
brána	brána	k1gFnSc1	brána
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výšky	výška	k1gFnSc2	výška
2,3	[number]	k4	2,3
m	m	kA	m
a	a	k8xC	a
šířky	šířka	k1gFnPc1	šířka
3,3	[number]	k4	3,3
m	m	kA	m
<g/>
;	;	kIx,	;
šíře	šíře	k1gFnSc1	šíře
oblouku	oblouk	k1gInSc2	oblouk
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
1,5	[number]	k4	1,5
m.	m.	k?	m.
K	k	k7c3	k
dalším	další	k2eAgInPc3d1	další
netradičním	tradiční	k2eNgInPc3d1	netradiční
skalním	skalní	k2eAgInPc3d1	skalní
útvarům	útvar	k1gInPc3	útvar
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
pískovcové	pískovcový	k2eAgInPc4d1	pískovcový
převisy	převis	k1gInPc4	převis
Jeskyně	jeskyně	k1gFnSc2	jeskyně
českých	český	k2eAgMnPc2d1	český
bratří	bratr	k1gMnPc2	bratr
a	a	k8xC	a
Velký	velký	k2eAgInSc1d1	velký
pruský	pruský	k2eAgInSc1d1	pruský
tábor	tábor	k1gInSc1	tábor
<g/>
,	,	kIx,	,
skalní	skalní	k2eAgInSc1d1	skalní
průrva	průrva	k?	průrva
se	s	k7c7	s
schodištěm	schodiště	k1gNnSc7	schodiště
Úzké	úzký	k2eAgInPc1d1	úzký
schody	schod	k1gInPc1	schod
<g/>
,	,	kIx,	,
menší	malý	k2eAgInSc1d2	menší
skalní	skalní	k2eAgInSc1d1	skalní
útvar	útvar	k1gInSc1	útvar
Černá	černý	k2eAgFnSc1d1	černá
brána	brána	k1gFnSc1	brána
nebo	nebo	k8xC	nebo
jeskyně	jeskyně	k1gFnSc1	jeskyně
Vinný	vinný	k1gMnSc1	vinný
sklep	sklep	k1gInSc1	sklep
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
doklady	doklad	k1gInPc1	doklad
o	o	k7c6	o
lidském	lidský	k2eAgNnSc6d1	lidské
osídlení	osídlení	k1gNnSc6	osídlení
Českého	český	k2eAgNnSc2d1	české
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
spadají	spadat	k5eAaImIp3nP	spadat
do	do	k7c2	do
období	období	k1gNnSc2	období
mezolitu	mezolit	k1gInSc2	mezolit
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pohybovali	pohybovat	k5eAaImAgMnP	pohybovat
v	v	k7c6	v
menších	malý	k2eAgFnPc6d2	menší
skupinách	skupina	k1gFnPc6	skupina
<g/>
,	,	kIx,	,
hledajíce	hledat	k5eAaImSgFnP	hledat
útočiště	útočiště	k1gNnSc4	útočiště
pod	pod	k7c7	pod
pískovcovými	pískovcový	k2eAgInPc7d1	pískovcový
převisy	převis	k1gInPc7	převis
<g/>
.	.	kIx.	.
</s>
<s>
Drsný	drsný	k2eAgInSc1d1	drsný
charakter	charakter	k1gInSc1	charakter
zdejší	zdejší	k2eAgFnSc2d1	zdejší
přírody	příroda	k1gFnSc2	příroda
však	však	k9	však
neposkytoval	poskytovat	k5eNaImAgMnS	poskytovat
příliš	příliš	k6eAd1	příliš
dobré	dobrý	k2eAgFnPc4d1	dobrá
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
trvalejší	trvalý	k2eAgInSc4d2	trvalejší
pobyt	pobyt	k1gInSc4	pobyt
pravěkých	pravěký	k2eAgMnPc2d1	pravěký
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
jejich	jejich	k3xOp3gInSc1	jejich
výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
spíše	spíše	k9	spíše
sporadický	sporadický	k2eAgInSc1d1	sporadický
<g/>
.	.	kIx.	.
</s>
<s>
Dokládá	dokládat	k5eAaImIp3nS	dokládat
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
archeologických	archeologický	k2eAgInPc2d1	archeologický
nálezů	nález	k1gInPc2	nález
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
skalních	skalní	k2eAgMnPc2d1	skalní
převisů	převis	k1gInPc2	převis
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
doby	doba	k1gFnSc2	doba
halštatské	halštatský	k2eAgFnSc2d1	halštatská
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
raně	raně	k6eAd1	raně
středověké	středověký	k2eAgFnPc1d1	středověká
dokonce	dokonce	k9	dokonce
úplně	úplně	k6eAd1	úplně
chybí	chybit	k5eAaPmIp3nS	chybit
archeologické	archeologický	k2eAgInPc4d1	archeologický
nálezy	nález	k1gInPc4	nález
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
by	by	kYmCp3nP	by
dokládaly	dokládat	k5eAaImAgInP	dokládat
osídlení	osídlení	k1gNnSc4	osídlení
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
mezidobí	mezidobí	k1gNnSc6	mezidobí
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
tak	tak	k6eAd1	tak
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
prvních	první	k4xOgMnPc2	první
lidí	člověk	k1gMnPc2	člověk
až	až	k8xS	až
do	do	k7c2	do
prvních	první	k4xOgNnPc2	první
století	století	k1gNnPc2	století
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
byla	být	k5eAaImAgFnS	být
krajina	krajina	k1gFnSc1	krajina
Českého	český	k2eAgNnSc2d1	české
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
osídlena	osídlit	k5eAaPmNgFnS	osídlit
jen	jen	k6eAd1	jen
minimálně	minimálně	k6eAd1	minimálně
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
souvislejší	souvislý	k2eAgInPc1d2	souvislejší
doklady	doklad	k1gInPc1	doklad
o	o	k7c6	o
lidském	lidský	k2eAgNnSc6d1	lidské
osídlení	osídlení	k1gNnSc6	osídlení
pocházejí	pocházet	k5eAaImIp3nP	pocházet
až	až	k9	až
z	z	k7c2	z
8	[number]	k4	8
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
hned	hned	k6eAd1	hned
tři	tři	k4xCgFnPc4	tři
významné	významný	k2eAgFnPc4d1	významná
lokality	lokalita	k1gFnPc4	lokalita
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
je	on	k3xPp3gNnSc4	on
hradiště	hradiště	k1gNnSc4	hradiště
na	na	k7c6	na
ostrožně	ostrožna	k1gFnSc6	ostrožna
Adlerhorn	Adlerhorna	k1gFnPc2	Adlerhorna
při	při	k7c6	při
soutoku	soutok	k1gInSc6	soutok
Křinice	Křinice	k1gFnSc2	Křinice
a	a	k8xC	a
Brtnického	brtnický	k2eAgInSc2d1	brtnický
potoka	potok	k1gInSc2	potok
<g/>
.	.	kIx.	.
</s>
<s>
Zásadní	zásadní	k2eAgInSc4d1	zásadní
průlom	průlom	k1gInSc4	průlom
v	v	k7c6	v
osidlování	osidlování	k1gNnSc6	osidlování
Českého	český	k2eAgNnSc2d1	české
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
přinesla	přinést	k5eAaPmAgFnS	přinést
až	až	k9	až
pozdně	pozdně	k6eAd1	pozdně
středověká	středověký	k2eAgFnSc1d1	středověká
kolonizace	kolonizace	k1gFnSc1	kolonizace
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
většina	většina	k1gFnSc1	většina
sídel	sídlo	k1gNnPc2	sídlo
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
nachází	nacházet	k5eAaImIp3nS	nacházet
těsně	těsně	k6eAd1	těsně
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Arnoltice	Arnoltika	k1gFnSc6	Arnoltika
(	(	kIx(	(
<g/>
1352	[number]	k4	1352
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Růžová	růžový	k2eAgFnSc1d1	růžová
(	(	kIx(	(
<g/>
1352	[number]	k4	1352
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Huntířov	Huntířov	k1gInSc1	Huntířov
(	(	kIx(	(
<g/>
1352	[number]	k4	1352
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Osadníci	osadník	k1gMnPc1	osadník
sem	sem	k6eAd1	sem
přicházeli	přicházet	k5eAaImAgMnP	přicházet
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
feudálních	feudální	k2eAgNnPc2d1	feudální
sídel	sídlo	k1gNnPc2	sídlo
<g/>
:	:	kIx,	:
od	od	k7c2	od
jihu	jih	k1gInSc2	jih
z	z	k7c2	z
hradu	hrad	k1gInSc2	hrad
Ostrý	ostrý	k2eAgInSc4d1	ostrý
u	u	k7c2	u
Benešova	Benešov	k1gInSc2	Benešov
nad	nad	k7c7	nad
Ploučnicí	Ploučnice	k1gFnSc7	Ploučnice
a	a	k8xC	a
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
strany	strana	k1gFnSc2	strana
od	od	k7c2	od
hradu	hrad	k1gInSc2	hrad
Krásný	krásný	k2eAgInSc4d1	krásný
Buk	buk	k1gInSc4	buk
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
doby	doba	k1gFnSc2	doba
vrcholně	vrcholně	k6eAd1	vrcholně
středověké	středověký	k2eAgNnSc4d1	středověké
pochází	pocházet	k5eAaImIp3nS	pocházet
i	i	k9	i
několik	několik	k4yIc1	několik
hrádků	hrádek	k1gInPc2	hrádek
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
<g/>
:	:	kIx,	:
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c4	o
Falkenštejn	Falkenštejn	k1gInSc4	Falkenštejn
<g/>
,	,	kIx,	,
Šaunštejn	Šaunštejn	k1gInSc4	Šaunštejn
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Loupežnický	loupežnický	k2eAgInSc1d1	loupežnický
hrad	hrad	k1gInSc1	hrad
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kyjovský	kyjovský	k2eAgInSc1d1	kyjovský
hrádek	hrádek	k1gInSc1	hrádek
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Horní	horní	k2eAgInSc1d1	horní
Karlštejn	Karlštejn	k1gInSc1	Karlštejn
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Brtnický	brtnický	k2eAgInSc1d1	brtnický
hrádek	hrádek	k1gInSc1	hrádek
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Loupežný	loupežný	k2eAgInSc1d1	loupežný
hrádek	hrádek	k1gInSc1	hrádek
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
Vlčí	vlčí	k2eAgInSc1d1	vlčí
hrádek	hrádek	k1gInSc1	hrádek
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Pustý	pustý	k2eAgInSc1d1	pustý
zámek	zámek	k1gInSc1	zámek
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důvod	důvod	k1gInSc1	důvod
zbudování	zbudování	k1gNnSc2	zbudování
těchto	tento	k3xDgInPc2	tento
hrádků	hrádek	k1gInPc2	hrádek
není	být	k5eNaImIp3nS	být
dosud	dosud	k6eAd1	dosud
zcela	zcela	k6eAd1	zcela
objasněn	objasnit	k5eAaPmNgInS	objasnit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
nálezů	nález	k1gInPc2	nález
pozůstatků	pozůstatek	k1gInPc2	pozůstatek
po	po	k7c6	po
středověkých	středověký	k2eAgFnPc6d1	středověká
sklárnách	sklárna	k1gFnPc6	sklárna
a	a	k8xC	a
těžbě	těžba	k1gFnSc6	těžba
hornin	hornina	k1gFnPc2	hornina
(	(	kIx(	(
<g/>
vápence	vápenec	k1gInSc2	vápenec
a	a	k8xC	a
železné	železný	k2eAgFnSc2d1	železná
rudy	ruda	k1gFnSc2	ruda
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
skalní	skalní	k2eAgInPc1d1	skalní
hrádky	hrádek	k1gInPc1	hrádek
plnily	plnit	k5eAaImAgInP	plnit
funkci	funkce	k1gFnSc4	funkce
hornických	hornický	k2eAgInPc2d1	hornický
a	a	k8xC	a
výrobních	výrobní	k2eAgNnPc2d1	výrobní
center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Přísun	přísun	k1gInSc1	přísun
osadníku	osadník	k1gMnSc3	osadník
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
Šluknovska	Šluknovsko	k1gNnSc2	Šluknovsko
znamenal	znamenat	k5eAaImAgInS	znamenat
mnohonásobně	mnohonásobně	k6eAd1	mnohonásobně
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
poptávku	poptávka	k1gFnSc4	poptávka
po	po	k7c6	po
zemědělské	zemědělský	k2eAgFnSc6d1	zemědělská
půdě	půda	k1gFnSc6	půda
<g/>
.	.	kIx.	.
</s>
<s>
Kolonisté	kolonista	k1gMnPc1	kolonista
tento	tento	k3xDgInSc4	tento
problém	problém	k1gInSc4	problém
řešili	řešit	k5eAaImAgMnP	řešit
hlavně	hlavně	k9	hlavně
vypalováním	vypalování	k1gNnSc7	vypalování
lesních	lesní	k2eAgInPc2d1	lesní
porostů	porost	k1gInPc2	porost
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgMnS	být
nejrychlejší	rychlý	k2eAgInSc4d3	nejrychlejší
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
zalesněnou	zalesněný	k2eAgFnSc4d1	zalesněná
oblast	oblast	k1gFnSc4	oblast
přeměnit	přeměnit	k5eAaPmF	přeměnit
v	v	k7c4	v
zemědělskou	zemědělský	k2eAgFnSc4d1	zemědělská
<g/>
.	.	kIx.	.
</s>
<s>
Dnešního	dnešní	k2eAgInSc2d1	dnešní
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
se	se	k3xPyFc4	se
však	však	k9	však
tato	tento	k3xDgFnSc1	tento
přeměna	přeměna	k1gFnSc1	přeměna
příliš	příliš	k6eAd1	příliš
netýkala	týkat	k5eNaImAgFnS	týkat
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
špatné	špatný	k2eAgFnSc2d1	špatná
přístupnosti	přístupnost	k1gFnSc2	přístupnost
terénu	terén	k1gInSc2	terén
a	a	k8xC	a
nedostatku	nedostatek	k1gInSc2	nedostatek
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
obhospodařování	obhospodařování	k1gNnSc3	obhospodařování
vhodná	vhodný	k2eAgFnSc1d1	vhodná
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
lesní	lesní	k2eAgInPc4d1	lesní
porosty	porost	k1gInPc4	porost
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
sloužily	sloužit	k5eAaImAgInP	sloužit
jako	jako	k8xS	jako
zdroj	zdroj	k1gInSc1	zdroj
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Dřevo	dřevo	k1gNnSc1	dřevo
se	se	k3xPyFc4	se
přemisťovalo	přemisťovat	k5eAaImAgNnS	přemisťovat
mj.	mj.	kA	mj.
po	po	k7c6	po
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
na	na	k7c6	na
dolních	dolní	k2eAgInPc6d1	dolní
tocích	tok	k1gInPc6	tok
Kamenice	Kamenice	k1gFnSc2	Kamenice
a	a	k8xC	a
Křinice	Křinice	k1gFnSc2	Křinice
existovala	existovat	k5eAaImAgFnS	existovat
soustava	soustava	k1gFnSc1	soustava
jezů	jez	k1gInPc2	jez
a	a	k8xC	a
menších	malý	k2eAgFnPc2d2	menší
přehrad	přehrada	k1gFnPc2	přehrada
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterým	který	k3yIgFnPc3	který
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
dřevo	dřevo	k1gNnSc1	dřevo
dopravit	dopravit	k5eAaPmF	dopravit
až	až	k9	až
k	k	k7c3	k
Labi	Labe	k1gNnSc3	Labe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozkvětu	rozkvět	k1gInSc3	rozkvět
obcí	obec	k1gFnPc2	obec
při	při	k7c6	při
okrajích	okraj	k1gInPc6	okraj
dnešního	dnešní	k2eAgInSc2d1	dnešní
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
i	i	k9	i
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
<g/>
,	,	kIx,	,
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
byla	být	k5eAaImAgFnS	být
zejména	zejména	k9	zejména
textilní	textilní	k2eAgFnSc1d1	textilní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
strojní	strojní	k2eAgFnSc1d1	strojní
výroba	výroba	k1gFnSc1	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nastal	nastat	k5eAaPmAgInS	nastat
útlum	útlum	k1gInSc1	útlum
<g/>
,	,	kIx,	,
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
některé	některý	k3yIgFnPc1	některý
obce	obec	k1gFnPc1	obec
dokonce	dokonce	k9	dokonce
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
území	území	k1gNnSc6	území
parku	park	k1gInSc2	park
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
Zadní	zadní	k2eAgFnPc4d1	zadní
Doubice	Doubice	k1gFnPc4	Doubice
a	a	k8xC	a
Zadní	zadní	k2eAgFnPc4d1	zadní
Jetřichovice	Jetřichovice	k1gFnPc4	Jetřichovice
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
v	v	k7c6	v
parku	park	k1gInSc6	park
nachází	nacházet	k5eAaImIp3nS	nacházet
pouze	pouze	k6eAd1	pouze
jedna	jeden	k4xCgFnSc1	jeden
vesnice	vesnice	k1gFnSc1	vesnice
–	–	k?	–
Mezná	mezný	k2eAgFnSc1d1	Mezná
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Mezné	mezný	k2eAgFnSc2d1	Mezná
se	se	k3xPyFc4	se
v	v	k7c6	v
parku	park	k1gInSc6	park
nalézá	nalézat	k5eAaImIp3nS	nalézat
i	i	k9	i
několik	několik	k4yIc4	několik
dalších	další	k2eAgNnPc2d1	další
lidských	lidský	k2eAgNnPc2d1	lidské
sídel	sídlo	k1gNnPc2	sídlo
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
Mezní	mezní	k2eAgFnSc1d1	mezní
Louka	louka	k1gFnSc1	louka
<g/>
,	,	kIx,	,
restaurace	restaurace	k1gFnSc1	restaurace
Sokolí	sokolí	k2eAgNnSc4d1	sokolí
hnízdo	hnízdo	k1gNnSc4	hnízdo
u	u	k7c2	u
Pravčické	Pravčický	k2eAgFnSc2d1	Pravčická
brány	brána	k1gFnSc2	brána
<g/>
,	,	kIx,	,
restaurace	restaurace	k1gFnSc1	restaurace
v	v	k7c6	v
soutěskách	soutěska	k1gFnPc6	soutěska
Kamenice	Kamenice	k1gFnSc2	Kamenice
<g/>
,	,	kIx,	,
hotel	hotel	k1gInSc1	hotel
Zámeček	zámeček	k1gInSc1	zámeček
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
Lípy	lípa	k1gFnSc2	lípa
<g/>
,	,	kIx,	,
osada	osada	k1gFnSc1	osada
Na	na	k7c6	na
Tokání	tokání	k1gNnSc6	tokání
a	a	k8xC	a
hájovna	hájovna	k1gFnSc1	hájovna
Saula	Saul	k1gMnSc2	Saul
u	u	k7c2	u
Dolní	dolní	k2eAgFnSc2d1	dolní
Chřibské	chřibský	k2eAgFnSc2d1	Chřibská
<g/>
.	.	kIx.	.
</s>
<s>
Složení	složení	k1gNnSc1	složení
bioty	biota	k1gFnSc2	biota
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
je	být	k5eAaImIp3nS	být
nejvýrazněji	výrazně	k6eAd3	výrazně
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
geomorfologickou	geomorfologický	k2eAgFnSc7d1	geomorfologická
stavbou	stavba	k1gFnSc7	stavba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
vliv	vliv	k1gInSc4	vliv
i	i	k9	i
na	na	k7c4	na
ráz	ráz	k1gInSc4	ráz
celé	celý	k2eAgFnSc2d1	celá
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Vysoké	vysoký	k2eAgInPc1d1	vysoký
pískovcové	pískovcový	k2eAgInPc1d1	pískovcový
útvary	útvar	k1gInPc1	útvar
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
veliké	veliký	k2eAgNnSc4d1	veliké
nadmořské	nadmořský	k2eAgNnSc4d1	nadmořské
převýšení	převýšení	k1gNnSc4	převýšení
na	na	k7c6	na
malém	malý	k2eAgInSc6d1	malý
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
zde	zde	k6eAd1	zde
není	být	k5eNaImIp3nS	být
nouze	nouze	k1gFnSc1	nouze
o	o	k7c4	o
náhle	náhle	k6eAd1	náhle
zvraty	zvrat	k1gInPc1	zvrat
vegetačních	vegetační	k2eAgInPc2d1	vegetační
stupňů	stupeň	k1gInPc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
zvraty	zvrat	k1gInPc1	zvrat
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
výskyt	výskyt	k1gInSc4	výskyt
montánních	montánní	k2eAgFnPc2d1	montánní
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
subalpínských	subalpínský	k2eAgInPc2d1	subalpínský
druhů	druh	k1gInPc2	druh
v	v	k7c6	v
jinak	jinak	k6eAd1	jinak
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgFnSc6d1	nízká
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
<g/>
.	.	kIx.	.
</s>
<s>
Vzácností	vzácnost	k1gFnSc7	vzácnost
není	být	k5eNaImIp3nS	být
ani	ani	k8xC	ani
výskyt	výskyt	k1gInSc1	výskyt
reliktů	relikt	k1gInPc2	relikt
<g/>
.	.	kIx.	.
</s>
<s>
Složení	složení	k1gNnSc1	složení
flóry	flóra	k1gFnSc2	flóra
v	v	k7c6	v
parku	park	k1gInSc6	park
je	být	k5eAaImIp3nS	být
kromě	kromě	k7c2	kromě
geomorfologie	geomorfologie	k1gFnSc2	geomorfologie
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
klimatickými	klimatický	k2eAgInPc7d1	klimatický
poměry	poměr	k1gInPc7	poměr
<g/>
,	,	kIx,	,
vysokým	vysoký	k2eAgInSc7d1	vysoký
stupněm	stupeň	k1gInSc7	stupeň
zalesnění	zalesnění	k1gNnSc2	zalesnění
a	a	k8xC	a
složením	složení	k1gNnSc7	složení
geologického	geologický	k2eAgInSc2d1	geologický
substrátu	substrát	k1gInSc2	substrát
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
97	[number]	k4	97
%	%	kIx~	%
území	území	k1gNnSc6	území
parku	park	k1gInSc2	park
zabírají	zabírat	k5eAaImIp3nP	zabírat
lesy	les	k1gInPc1	les
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
dominantní	dominantní	k2eAgInSc4d1	dominantní
buk	buk	k1gInSc4	buk
lesní	lesní	k2eAgInSc4d1	lesní
(	(	kIx(	(
<g/>
Fagus	Fagus	k1gInSc4	Fagus
sylvatica	sylvatic	k1gInSc2	sylvatic
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
invazivním	invazivní	k2eAgInSc7d1	invazivní
smrkem	smrk	k1gInSc7	smrk
ztepilým	ztepilý	k2eAgInSc7d1	ztepilý
(	(	kIx(	(
<g/>
Picea	Piceum	k1gNnPc1	Piceum
abies	abiesa	k1gFnPc2	abiesa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
až	až	k9	až
na	na	k7c4	na
60	[number]	k4	60
%	%	kIx~	%
povrchu	povrch	k1gInSc3	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
skalním	skalní	k2eAgInSc6d1	skalní
podkladě	podklad	k1gInSc6	podklad
se	se	k3xPyFc4	se
dobře	dobře	k6eAd1	dobře
daří	dařit	k5eAaImIp3nS	dařit
borovici	borovice	k1gFnSc4	borovice
lesní	lesní	k2eAgFnSc4d1	lesní
(	(	kIx(	(
<g/>
Pinus	Pinus	k1gMnSc1	Pinus
sylvestris	sylvestris	k1gFnSc2	sylvestris
<g/>
)	)	kIx)	)
i	i	k8xC	i
bříze	bříza	k1gFnSc6	bříza
bělokoré	bělokorý	k2eAgNnSc1d1	bělokoré
(	(	kIx(	(
<g/>
Betula	Betula	k1gFnSc1	Betula
pendula	pendula	k1gFnSc1	pendula
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
hojně	hojně	k6eAd1	hojně
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
buk	buk	k1gInSc1	buk
a	a	k8xC	a
jedle	jedle	k1gFnSc1	jedle
bělokorá	bělokorý	k2eAgFnSc1d1	bělokorá
(	(	kIx(	(
<g/>
Abies	Abies	k1gInSc1	Abies
alba	album	k1gNnSc2	album
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
parku	park	k1gInSc6	park
nacházejí	nacházet	k5eAaImIp3nP	nacházet
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgFnSc6d1	malá
míře	míra	k1gFnSc6	míra
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
správa	správa	k1gFnSc1	správa
parku	park	k1gInSc2	park
snaží	snažit	k5eAaImIp3nS	snažit
o	o	k7c4	o
pozvolnou	pozvolný	k2eAgFnSc4d1	pozvolná
obnovu	obnova	k1gFnSc4	obnova
dominance	dominance	k1gFnSc2	dominance
těchto	tento	k3xDgFnPc2	tento
dřevin	dřevina	k1gFnPc2	dřevina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
některých	některý	k3yIgInPc6	některý
svazích	svah	k1gInPc6	svah
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
dub	dub	k1gInSc1	dub
zimní	zimní	k2eAgInSc1d1	zimní
(	(	kIx(	(
<g/>
Quercus	Quercus	k1gInSc1	Quercus
petraea	petrae	k1gInSc2	petrae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
zase	zase	k9	zase
olše	olše	k1gFnSc1	olše
lepkavá	lepkavý	k2eAgFnSc1d1	lepkavá
(	(	kIx(	(
<g/>
Alnus	Alnus	k1gMnSc1	Alnus
glutinosa	glutinosa	k1gFnSc1	glutinosa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInSc4d1	další
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
časté	častý	k2eAgMnPc4d1	častý
druhy	druh	k1gMnPc4	druh
patří	patřit	k5eAaImIp3nS	patřit
mj.	mj.	kA	mj.
jeřáb	jeřáb	k1gMnSc1	jeřáb
ptačí	ptačí	k2eAgMnSc1d1	ptačí
(	(	kIx(	(
<g/>
Sorbus	Sorbus	k1gInSc1	Sorbus
aucuparia	aucuparium	k1gNnSc2	aucuparium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krušina	krušina	k1gFnSc1	krušina
olšová	olšový	k2eAgFnSc1d1	olšová
(	(	kIx(	(
<g/>
Frangula	Frangula	k1gFnSc1	Frangula
alnus	alnus	k1gMnSc1	alnus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
líska	líska	k1gFnSc1	líska
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Corylus	Corylus	k1gInSc1	Corylus
avellana	avellan	k1gMnSc2	avellan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
střemcha	střemcha	k1gFnSc1	střemcha
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Prunus	Prunus	k1gMnSc1	Prunus
padus	padus	k1gMnSc1	padus
<g/>
)	)	kIx)	)
a	a	k8xC	a
vrba	vrba	k1gFnSc1	vrba
jíva	jíva	k1gFnSc1	jíva
(	(	kIx(	(
<g/>
Salix	Salix	k1gInSc1	Salix
caprea	capre	k1gInSc2	capre
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
čedičových	čedičový	k2eAgFnPc2d1	čedičová
vyvřelin	vyvřelina	k1gFnPc2	vyvřelina
se	se	k3xPyFc4	se
místy	místy	k6eAd1	místy
daří	dařit	k5eAaImIp3nS	dařit
kromě	kromě	k7c2	kromě
buku	buk	k1gInSc2	buk
lesního	lesní	k2eAgInSc2d1	lesní
i	i	k8xC	i
jasanu	jasan	k1gInSc2	jasan
ztepilému	ztepilý	k2eAgInSc3d1	ztepilý
(	(	kIx(	(
<g/>
Fraxinus	Fraxinus	k1gMnSc1	Fraxinus
excelsior	excelsior	k1gMnSc1	excelsior
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lípě	lípa	k1gFnSc3	lípa
srdčité	srdčitý	k2eAgFnSc2d1	srdčitá
(	(	kIx(	(
<g/>
Tilia	Tilius	k1gMnSc2	Tilius
cordata	cordat	k1gMnSc2	cordat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
javoru	javor	k1gInSc3	javor
klenu	klen	k1gInSc2	klen
(	(	kIx(	(
<g/>
Acer	Acer	k1gMnSc1	Acer
pseudoplatanus	pseudoplatanus	k1gMnSc1	pseudoplatanus
<g/>
)	)	kIx)	)
i	i	k8xC	i
mléči	mléč	k1gInPc7	mléč
(	(	kIx(	(
<g/>
Acer	Acer	k1gMnSc1	Acer
platanoides	platanoides	k1gMnSc1	platanoides
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jilmu	jilm	k1gInSc3	jilm
horskému	horský	k2eAgInSc3d1	horský
(	(	kIx(	(
<g/>
Ulmus	Ulmus	k1gMnSc1	Ulmus
glabra	glabra	k1gMnSc1	glabra
<g/>
)	)	kIx)	)
a	a	k8xC	a
habru	habr	k1gInSc2	habr
obecnému	obecný	k2eAgInSc3d1	obecný
(	(	kIx(	(
<g/>
Carpinus	Carpinus	k1gMnSc1	Carpinus
betulus	betulus	k1gMnSc1	betulus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velkým	velký	k2eAgInSc7d1	velký
problémem	problém	k1gInSc7	problém
parku	park	k1gInSc2	park
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
nepůvodní	původní	k2eNgFnPc4d1	nepůvodní
dřeviny	dřevina	k1gFnPc4	dřevina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgFnP	být
uměle	uměle	k6eAd1	uměle
vysazovány	vysazovat	k5eAaImNgFnP	vysazovat
již	již	k6eAd1	již
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
poptávky	poptávka	k1gFnSc2	poptávka
po	po	k7c6	po
dřevu	dřevo	k1gNnSc6	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInPc1d1	původní
lesy	les	k1gInPc1	les
s	s	k7c7	s
převládajícím	převládající	k2eAgInSc7d1	převládající
bukem	buk	k1gInSc7	buk
a	a	k8xC	a
jedlí	jedle	k1gFnSc7	jedle
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
neobnovovaly	obnovovat	k5eNaImAgInP	obnovovat
tak	tak	k9	tak
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
dostatečně	dostatečně	k6eAd1	dostatečně
uspokojily	uspokojit	k5eAaPmAgFnP	uspokojit
poptávku	poptávka	k1gFnSc4	poptávka
po	po	k7c6	po
dřevě	dřevo	k1gNnSc6	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
parku	park	k1gInSc2	park
či	či	k8xC	či
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
těsném	těsný	k2eAgNnSc6d1	těsné
okolí	okolí	k1gNnSc6	okolí
proto	proto	k8xC	proto
byly	být	k5eAaImAgFnP	být
pokusně	pokusně	k6eAd1	pokusně
vysazovány	vysazován	k2eAgInPc1d1	vysazován
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
dřevin	dřevina	k1gFnPc2	dřevina
převážně	převážně	k6eAd1	převážně
ze	z	k7c2	z
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
jako	jako	k8xS	jako
borovice	borovice	k1gFnSc2	borovice
vejmutovka	vejmutovka	k1gFnSc1	vejmutovka
(	(	kIx(	(
<g/>
Pinus	Pinus	k1gMnSc1	Pinus
strobus	strobus	k1gMnSc1	strobus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
borovice	borovice	k1gFnSc1	borovice
Banksova	Banksův	k2eAgFnSc1d1	Banksova
(	(	kIx(	(
<g/>
Pinus	Pinus	k1gMnSc1	Pinus
banksiana	banksian	k1gMnSc2	banksian
<g/>
)	)	kIx)	)
i	i	k9	i
douglaska	douglaska	k1gFnSc1	douglaska
(	(	kIx(	(
<g/>
Pseudotsuga	Pseudotsuga	k1gFnSc1	Pseudotsuga
menziesii	menziesie	k1gFnSc4	menziesie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cypřišek	cypřišek	k1gInSc1	cypřišek
Lawsonův	Lawsonův	k2eAgInSc1d1	Lawsonův
(	(	kIx(	(
<g/>
Chamaecyparis	Chamaecyparis	k1gInSc1	Chamaecyparis
lawsoniana	lawsonian	k1gMnSc2	lawsonian
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
modřín	modřín	k1gInSc1	modřín
japonský	japonský	k2eAgInSc1d1	japonský
(	(	kIx(	(
<g/>
Larix	Larix	k1gInSc1	Larix
leptolesis	leptolesis	k1gFnSc2	leptolesis
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
zerav	zerav	k1gInSc1	zerav
obrovský	obrovský	k2eAgInSc1d1	obrovský
(	(	kIx(	(
<g/>
Thuja	Thuj	k2eAgFnSc1d1	Thuja
plicata	plicata	k1gFnSc1	plicata
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
ještě	ještě	k9	ještě
pokusně	pokusně	k6eAd1	pokusně
zaváděn	zaváděn	k2eAgInSc1d1	zaváděn
dub	dub	k1gInSc1	dub
červený	červený	k2eAgInSc1d1	červený
(	(	kIx(	(
<g/>
Quercus	Quercus	k1gInSc1	Quercus
rubra	rubrum	k1gNnSc2	rubrum
<g/>
)	)	kIx)	)
a	a	k8xC	a
modřín	modřín	k1gInSc1	modřín
opadavý	opadavý	k2eAgInSc1d1	opadavý
(	(	kIx(	(
<g/>
Larix	Larix	k1gInSc1	Larix
decidua	decidu	k1gInSc2	decidu
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
dřevin	dřevina	k1gFnPc2	dřevina
se	se	k3xPyFc4	se
v	v	k7c6	v
parku	park	k1gInSc6	park
neuchytila	uchytit	k5eNaPmAgFnS	uchytit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
douglaska	douglaska	k1gFnSc1	douglaska
<g/>
,	,	kIx,	,
vejmutovka	vejmutovka	k1gFnSc1	vejmutovka
<g/>
,	,	kIx,	,
modřín	modřín	k1gInSc1	modřín
a	a	k8xC	a
dub	dub	k1gInSc1	dub
červený	červený	k2eAgInSc1d1	červený
zde	zde	k6eAd1	zde
zdomácněly	zdomácnět	k5eAaPmAgInP	zdomácnět
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
dřeviny	dřevina	k1gFnPc1	dřevina
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nP	šířit
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
dřevin	dřevina	k1gFnPc2	dřevina
původních	původní	k2eAgFnPc2d1	původní
a	a	k8xC	a
svým	svůj	k3xOyFgNnSc7	svůj
často	často	k6eAd1	často
netolerantním	tolerantní	k2eNgNnSc7d1	netolerantní
chováním	chování	k1gNnSc7	chování
vůči	vůči	k7c3	vůči
ostatním	ostatní	k2eAgFnPc3d1	ostatní
rostlinám	rostlina	k1gFnPc3	rostlina
působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
parku	park	k1gInSc6	park
problémy	problém	k1gInPc1	problém
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
vejmutovka	vejmutovka	k1gFnSc1	vejmutovka
svým	svůj	k3xOyFgNnSc7	svůj
silným	silný	k2eAgNnSc7d1	silné
přirozeným	přirozený	k2eAgNnSc7d1	přirozené
zmlazením	zmlazení	k1gNnSc7	zmlazení
a	a	k8xC	a
netolerancí	netolerance	k1gFnSc7	netolerance
i	i	k9	i
vůči	vůči	k7c3	vůči
bylinnému	bylinný	k2eAgNnSc3d1	bylinné
a	a	k8xC	a
mechovému	mechový	k2eAgNnSc3d1	mechové
patru	patro	k1gNnSc3	patro
působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
svého	svůj	k3xOyFgNnSc2	svůj
rozšíření	rozšíření	k1gNnSc2	rozšíření
rozpad	rozpad	k1gInSc1	rozpad
místních	místní	k2eAgInPc2d1	místní
ekosystémů	ekosystém	k1gInPc2	ekosystém
<g/>
.	.	kIx.	.
</s>
<s>
Bylinné	bylinný	k2eAgNnSc1d1	bylinné
patro	patro	k1gNnSc1	patro
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
mechového	mechový	k2eAgNnSc2d1	mechové
patra	patro	k1gNnSc2	patro
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
druhově	druhově	k6eAd1	druhově
rozmanité	rozmanitý	k2eAgFnPc1d1	rozmanitá
<g/>
.	.	kIx.	.
</s>
<s>
Vysoké	vysoký	k2eAgNnSc4d1	vysoké
zastoupení	zastoupení	k1gNnSc4	zastoupení
zde	zde	k6eAd1	zde
mají	mít	k5eAaImIp3nP	mít
subatlantské	subatlantský	k2eAgInPc4d1	subatlantský
druhy	druh	k1gInPc4	druh
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
koprník	koprník	k1gInSc1	koprník
štětinolistý	štětinolistý	k2eAgInSc1d1	štětinolistý
(	(	kIx(	(
<g/>
Meum	Meum	k1gInSc1	Meum
athamanticum	athamanticum	k1gInSc1	athamanticum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mochna	mochna	k1gFnSc1	mochna
anglická	anglický	k2eAgFnSc1d1	anglická
(	(	kIx(	(
<g/>
Potentilla	Potentilla	k1gFnSc1	Potentilla
anglica	anglica	k1gMnSc1	anglica
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mokrýš	mokrýš	k1gInSc1	mokrýš
vstřícnolistý	vstřícnolistý	k2eAgInSc1d1	vstřícnolistý
(	(	kIx(	(
<g/>
Chrysosplenium	Chrysosplenium	k1gNnSc1	Chrysosplenium
oppositifolium	oppositifolium	k1gNnSc1	oppositifolium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pérnatec	pérnatec	k1gMnSc1	pérnatec
horský	horský	k2eAgMnSc1d1	horský
(	(	kIx(	(
<g/>
Lastrea	Lastre	k2eAgFnSc1d1	Lastre
limbosperma	limbosperma	k1gFnSc1	limbosperma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sítina	sítina	k1gFnSc1	sítina
kostrbatá	kostrbatý	k2eAgFnSc1d1	kostrbatá
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Juncus	Juncus	k1gMnSc1	Juncus
squarrosus	squarrosus	k1gMnSc1	squarrosus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sítina	sítina	k1gFnSc1	sítina
ostrokvětá	ostrokvětý	k2eAgFnSc1d1	ostrokvětá
(	(	kIx(	(
<g/>
Juncus	Juncus	k1gMnSc1	Juncus
acutiflorus	acutiflorus	k1gMnSc1	acutiflorus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
svízel	svízel	k1gInSc1	svízel
skalní	skalní	k2eAgInSc1d1	skalní
(	(	kIx(	(
<g/>
Galium	galium	k1gNnSc1	galium
saxatile	saxatile	k6eAd1	saxatile
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
třezalka	třezalka	k1gFnSc1	třezalka
pěkná	pěkná	k1gFnSc1	pěkná
(	(	kIx(	(
<g/>
Hypericum	Hypericum	k1gInSc1	Hypericum
pulchrum	pulchrum	k1gInSc1	pulchrum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
třezalka	třezalka	k1gFnSc1	třezalka
rozprostřená	rozprostřený	k2eAgFnSc1d1	rozprostřená
(	(	kIx(	(
<g/>
Hypericum	Hypericum	k1gInSc1	Hypericum
humifusum	humifusum	k1gInSc1	humifusum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
štírovník	štírovník	k1gInSc1	štírovník
bažinný	bažinný	k2eAgInSc1d1	bažinný
(	(	kIx(	(
<g/>
Lotus	Lotus	kA	Lotus
uliginosus	uliginosus	k1gInSc1	uliginosus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vrbina	vrbina	k1gFnSc1	vrbina
hajní	hajní	k2eAgFnSc1d1	hajní
(	(	kIx(	(
<g/>
Lysimachia	Lysimachia	k1gFnSc1	Lysimachia
nemorum	nemorum	k1gInSc1	nemorum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zblochan	zblochan	k1gInSc1	zblochan
zoubkatý	zoubkatý	k2eAgInSc1d1	zoubkatý
(	(	kIx(	(
<g/>
Glyceria	Glycerium	k1gNnPc4	Glycerium
declinata	declinat	k2eAgNnPc4d1	declinat
<g/>
)	)	kIx)	)
a	a	k8xC	a
žebrovice	žebrovice	k1gFnSc1	žebrovice
různolistá	různolistý	k2eAgFnSc1d1	různolistá
(	(	kIx(	(
<g/>
Blechnum	Blechnum	k1gInSc1	Blechnum
spicant	spicant	k1gInSc1	spicant
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zástupců	zástupce	k1gMnPc2	zástupce
submontánních	submontánní	k2eAgInPc2d1	submontánní
a	a	k8xC	a
montánních	montánní	k2eAgInPc2d1	montánní
druhů	druh	k1gInPc2	druh
lze	lze	k6eAd1	lze
jmenovat	jmenovat	k5eAaBmF	jmenovat
hlavně	hlavně	k9	hlavně
devětsil	devětsil	k1gInSc1	devětsil
bílý	bílý	k2eAgInSc1d1	bílý
(	(	kIx(	(
<g/>
Petasites	Petasites	k1gInSc1	Petasites
albus	albus	k1gInSc1	albus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plavuň	plavuň	k1gFnSc4	plavuň
pučivou	pučivý	k2eAgFnSc4d1	pučivá
(	(	kIx(	(
<g/>
Lycopodium	Lycopodium	k1gNnSc1	Lycopodium
annotinum	annotinum	k1gNnSc1	annotinum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sedmikvítek	sedmikvítek	k1gInSc1	sedmikvítek
evropský	evropský	k2eAgInSc1d1	evropský
(	(	kIx(	(
<g/>
Trientalis	Trientalis	k1gInSc1	Trientalis
europaea	europae	k1gInSc2	europae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
violku	violka	k1gFnSc4	violka
dvoukvětou	dvoukvětý	k2eAgFnSc4d1	dvoukvětá
(	(	kIx(	(
<g/>
Viola	Viola	k1gFnSc1	Viola
biflora	biflora	k1gFnSc1	biflora
<g/>
)	)	kIx)	)
a	a	k8xC	a
vranec	vranec	k1gMnSc1	vranec
jedlový	jedlový	k2eAgMnSc1d1	jedlový
(	(	kIx(	(
<g/>
Huperzia	Huperzia	k1gFnSc1	Huperzia
selago	selago	k1gMnSc1	selago
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
druhy	druh	k1gInPc1	druh
se	se	k3xPyFc4	se
pojí	pojit	k5eAaImIp3nP	pojit
hlavně	hlavně	k9	hlavně
k	k	k7c3	k
hlubokým	hluboký	k2eAgFnPc3d1	hluboká
a	a	k8xC	a
stinným	stinný	k2eAgFnPc3d1	stinná
roklím	rokle	k1gFnPc3	rokle
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
teplomilná	teplomilný	k2eAgFnSc1d1	teplomilná
květena	květena	k1gFnSc1	květena
v	v	k7c6	v
Českém	český	k2eAgNnSc6d1	české
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
vzácná	vzácný	k2eAgFnSc1d1	vzácná
<g/>
,	,	kIx,	,
přece	přece	k9	přece
jen	jen	k9	jen
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
jí	on	k3xPp3gFnSc3	on
druhy	druh	k1gInPc7	druh
jako	jako	k9	jako
čistec	čistec	k1gInSc1	čistec
přímý	přímý	k2eAgInSc1d1	přímý
(	(	kIx(	(
<g/>
Stachys	Stachys	k1gInSc1	Stachys
recta	rect	k1gInSc2	rect
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hořčík	hořčík	k1gInSc1	hořčík
jestřábníkovitý	jestřábníkovitý	k2eAgInSc1d1	jestřábníkovitý
(	(	kIx(	(
<g/>
Picris	Picris	k1gInSc1	Picris
hieracioides	hieracioides	k1gInSc1	hieracioides
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chrastavec	chrastavec	k1gInSc4	chrastavec
křovištní	křovištní	k2eAgInSc4d1	křovištní
(	(	kIx(	(
<g/>
Knautia	Knautia	k1gFnSc1	Knautia
drymeia	drymeia	k1gFnSc1	drymeia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kakost	kakost	k1gInSc1	kakost
krvavý	krvavý	k2eAgInSc1d1	krvavý
(	(	kIx(	(
<g/>
Geranium	Geranium	k1gNnSc1	Geranium
sanguineum	sanguineum	k1gNnSc1	sanguineum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pelyněk	pelyněk	k1gInSc1	pelyněk
ladní	ladní	k2eAgInSc1d1	ladní
(	(	kIx(	(
<g/>
Artemisia	Artemisia	k1gFnSc1	Artemisia
campestris	campestris	k1gFnSc2	campestris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tolita	tolita	k1gFnSc1	tolita
lékařská	lékařský	k2eAgFnSc1d1	lékařská
(	(	kIx(	(
<g/>
Vincetoxicum	Vincetoxicum	k1gInSc1	Vincetoxicum
hirundinaria	hirundinarium	k1gNnSc2	hirundinarium
<g/>
)	)	kIx)	)
a	a	k8xC	a
žluťucha	žluťucha	k1gFnSc1	žluťucha
menší	malý	k2eAgFnSc1d2	menší
(	(	kIx(	(
<g/>
Thalictrum	Thalictrum	k1gNnSc1	Thalictrum
minus	minus	k1gNnSc2	minus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Českého	český	k2eAgNnSc2d1	české
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
se	se	k3xPyFc4	se
kdysi	kdysi	k6eAd1	kdysi
vyskytoval	vyskytovat	k5eAaImAgMnS	vyskytovat
i	i	k9	i
vzácný	vzácný	k2eAgMnSc1d1	vzácný
blánatec	blánatec	k1gMnSc1	blánatec
kentský	kentský	k2eAgMnSc1d1	kentský
(	(	kIx(	(
<g/>
Hymenophyllum	Hymenophyllum	k1gInSc1	Hymenophyllum
tunbrigense	tunbrigense	k1gFnSc2	tunbrigense
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
rostlina	rostlina	k1gFnSc1	rostlina
však	však	k9	však
z	z	k7c2	z
parku	park	k1gInSc2	park
už	už	k6eAd1	už
vymizela	vymizet	k5eAaPmAgFnS	vymizet
<g/>
,	,	kIx,	,
na	na	k7c6	na
čemž	což	k3yQnSc6	což
měl	mít	k5eAaImAgMnS	mít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nemalý	malý	k2eNgInSc4d1	nemalý
podíl	podíl	k1gInSc4	podíl
nadměrný	nadměrný	k2eAgInSc1d1	nadměrný
sběr	sběr	k1gInSc1	sběr
rostliny	rostlina	k1gFnSc2	rostlina
člověkem	člověk	k1gMnSc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
národní	národní	k2eAgFnSc6d1	národní
parku	park	k1gInSc2	park
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
nejméně	málo	k6eAd3	málo
246	[number]	k4	246
druhů	druh	k1gInPc2	druh
lišejníků	lišejník	k1gInPc2	lišejník
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
typickým	typický	k2eAgInPc3d1	typický
druhům	druh	k1gInPc3	druh
skalních	skalní	k2eAgFnPc2d1	skalní
stěn	stěna	k1gFnPc2	stěna
patří	patřit	k5eAaImIp3nS	patřit
Chrysotrix	Chrysotrix	k1gInSc1	Chrysotrix
chlorina	chlorino	k1gNnSc2	chlorino
<g/>
,	,	kIx,	,
plsťotvorka	plsťotvorka	k1gFnSc1	plsťotvorka
ebenová	ebenový	k2eAgFnSc1d1	ebenová
(	(	kIx(	(
<g/>
Cystocoleus	Cystocoleus	k1gMnSc1	Cystocoleus
ebeneus	ebeneus	k1gMnSc1	ebeneus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Racodium	Racodium	k1gNnSc1	Racodium
rupestre	rupestr	k1gInSc5	rupestr
a	a	k8xC	a
šálečka	šálečka	k1gFnSc1	šálečka
lesklá	lesklý	k2eAgFnSc1d1	lesklá
(	(	kIx(	(
<g/>
Psilolechia	Psilolechia	k1gFnSc1	Psilolechia
lucida	lucida	k1gFnSc1	lucida
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pískovcové	pískovcový	k2eAgInPc4d1	pískovcový
ostrohy	ostroh	k1gInPc4	ostroh
obývá	obývat	k5eAaImIp3nS	obývat
především	především	k9	především
terčovka	terčovka	k1gFnSc1	terčovka
prohnutá	prohnutý	k2eAgFnSc1d1	prohnutá
(	(	kIx(	(
<g/>
Parmelia	Parmelia	k1gFnSc1	Parmelia
incurva	incurva	k1gFnSc1	incurva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oblasti	oblast	k1gFnPc1	oblast
s	s	k7c7	s
čedičovým	čedičový	k2eAgNnSc7d1	čedičové
podložím	podloží	k1gNnSc7	podloží
jsou	být	k5eAaImIp3nP	být
typické	typický	k2eAgInPc1d1	typický
pro	pro	k7c4	pro
drobnovýtrusku	drobnovýtruska	k1gFnSc4	drobnovýtruska
hnědavou	hnědavý	k2eAgFnSc4d1	hnědavá
(	(	kIx(	(
<g/>
Acarospora	Acarospor	k1gMnSc2	Acarospor
fuscata	fuscat	k1gMnSc2	fuscat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
buelii	buelie	k1gFnSc4	buelie
stélkovou	stélkový	k2eAgFnSc4d1	stélkový
(	(	kIx(	(
<g/>
Buellia	Buellius	k1gMnSc2	Buellius
aethalea	aethaleus	k1gMnSc2	aethaleus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
misničku	misnička	k1gFnSc4	misnička
nazlátlou	nazlátlý	k2eAgFnSc4d1	nazlátlá
(	(	kIx(	(
<g/>
Lecanora	Lecanor	k1gMnSc2	Lecanor
subaurea	subaureus	k1gMnSc2	subaureus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mapovník	mapovník	k1gInSc1	mapovník
zeměpisný	zeměpisný	k2eAgInSc1d1	zeměpisný
(	(	kIx(	(
<g/>
Rhizocarpon	Rhizocarpon	k1gInSc1	Rhizocarpon
geographicum	geographicum	k1gInSc1	geographicum
<g/>
)	)	kIx)	)
a	a	k8xC	a
pevnokmínek	pevnokmínek	k1gInSc1	pevnokmínek
kloboukatý	kloboukatý	k2eAgInSc1d1	kloboukatý
(	(	kIx(	(
<g/>
Stereocaulon	Stereocaulon	k1gInSc1	Stereocaulon
pileatum	pileatum	k1gNnSc1	pileatum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
vzácnějších	vzácný	k2eAgInPc2d2	vzácnější
lišejníků	lišejník	k1gInPc2	lišejník
lze	lze	k6eAd1	lze
jmenovat	jmenovat	k5eAaImF	jmenovat
dutohlávku	dutohlávka	k1gFnSc4	dutohlávka
Cladonia	Cladonium	k1gNnSc2	Cladonium
subcervicornia	subcervicornium	k1gNnSc2	subcervicornium
<g/>
,	,	kIx,	,
Phaeographis	Phaeographis	k1gFnSc1	Phaeographis
inusta	inusta	k1gFnSc1	inusta
a	a	k8xC	a
třpytku	třpytka	k1gFnSc4	třpytka
Micarea	Micare	k2eAgMnSc4d1	Micare
pycnidiophora	pycnidiophor	k1gMnSc4	pycnidiophor
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ojedinělým	ojedinělý	k2eAgInPc3d1	ojedinělý
nálezům	nález	k1gInPc3	nález
patří	patřit	k5eAaImIp3nP	patřit
i	i	k9	i
druhy	druh	k1gInPc1	druh
citlivé	citlivý	k2eAgInPc1d1	citlivý
na	na	k7c4	na
znečištěné	znečištěný	k2eAgNnSc4d1	znečištěné
ovzduší	ovzduší	k1gNnSc4	ovzduší
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
ohrožené	ohrožený	k2eAgFnPc1d1	ohrožená
čárnička	čárnička	k1gFnSc1	čárnička
psaná	psaný	k2eAgFnSc1d1	psaná
(	(	kIx(	(
<g/>
Graphis	Graphis	k1gFnSc1	Graphis
scripta	scripta	k1gFnSc1	scripta
<g/>
)	)	kIx)	)
a	a	k8xC	a
cecatka	cecatka	k1gFnSc1	cecatka
chřástnatá	chřástnatý	k2eAgFnSc1d1	chřástnatý
(	(	kIx(	(
<g/>
Thelotrema	Thelotrema	k1gFnSc1	Thelotrema
lepadinum	lepadinum	k1gInSc1	lepadinum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2007	[number]	k4	2007
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
parku	park	k1gInSc6	park
známo	znám	k2eAgNnSc1d1	známo
334	[number]	k4	334
druhů	druh	k1gInPc2	druh
mechorostů	mechorost	k1gInPc2	mechorost
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
87	[number]	k4	87
je	být	k5eAaImIp3nS	být
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
na	na	k7c6	na
Červeném	červený	k2eAgInSc6d1	červený
seznamu	seznam	k1gInSc6	seznam
mechorostů	mechorost	k1gInPc2	mechorost
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
42	[number]	k4	42
zdejších	zdejší	k2eAgInPc2d1	zdejší
druhů	druh	k1gInPc2	druh
dokonce	dokonce	k9	dokonce
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
kriticky	kriticky	k6eAd1	kriticky
ohrožené	ohrožený	k2eAgNnSc1d1	ohrožené
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvzácnějším	vzácný	k2eAgMnPc3d3	nejvzácnější
druhům	druh	k1gMnPc3	druh
patří	patřit	k5eAaImIp3nP	patřit
subarkticko-subalpínské	subarktickoubalpínský	k2eAgInPc1d1	subarkticko-subalpínský
druhy	druh	k1gInPc1	druh
játrovky	játrovka	k1gFnSc2	játrovka
křižítka	křižítka	k1gFnSc1	křižítka
velkobuněčná	velkobuněčný	k2eAgFnSc1d1	velkobuněčný
(	(	kIx(	(
<g/>
Lophozia	Lophozia	k1gFnSc1	Lophozia
grandiretis	grandiretis	k1gFnSc2	grandiretis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mokřanka	mokřanka	k1gFnSc1	mokřanka
oddálená	oddálený	k2eAgFnSc1d1	oddálená
(	(	kIx(	(
<g/>
Hygrobiella	Hygrobiella	k1gFnSc1	Hygrobiella
laxifolia	laxifolia	k1gFnSc1	laxifolia
<g/>
)	)	kIx)	)
a	a	k8xC	a
polanka	polanka	k1gFnSc1	polanka
Michauxova	Michauxov	k1gInSc2	Michauxov
(	(	kIx(	(
<g/>
Anastrophyllum	Anastrophyllum	k1gInSc1	Anastrophyllum
michauxii	michauxie	k1gFnSc4	michauxie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
mechy	mech	k1gInPc1	mech
dvouhrotcovka	dvouhrotcovka	k1gFnSc1	dvouhrotcovka
drsná	drsný	k2eAgFnSc1d1	drsná
(	(	kIx(	(
<g/>
Dicranodontium	Dicranodontium	k1gNnSc1	Dicranodontium
asperulum	asperulum	k1gNnSc1	asperulum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chudozubík	chudozubík	k1gMnSc1	chudozubík
zahnutý	zahnutý	k2eAgMnSc1d1	zahnutý
(	(	kIx(	(
<g/>
Tetrodontium	Tetrodontium	k1gNnSc1	Tetrodontium
repandum	repandum	k1gNnSc1	repandum
<g/>
)	)	kIx)	)
a	a	k8xC	a
měřík	měřík	k1gMnSc1	měřík
prostřední	prostřednět	k5eAaImIp3nS	prostřednět
(	(	kIx(	(
<g/>
Plagiomnium	Plagiomnium	k1gNnSc1	Plagiomnium
medium	medium	k1gNnSc1	medium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlubokých	hluboký	k2eAgFnPc6d1	hluboká
inversních	inversní	k2eAgFnPc6d1	inversní
roklích	rokle	k1gFnPc6	rokle
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
např.	např.	kA	např.
ohrožené	ohrožený	k2eAgInPc4d1	ohrožený
druhy	druh	k1gInPc4	druh
játrovky	játrovka	k1gFnSc2	játrovka
křižítku	křižítek	k1gInSc2	křižítek
tupou	tupý	k2eAgFnSc7d1	tupá
(	(	kIx(	(
<g/>
Lophozia	Lophozius	k1gMnSc4	Lophozius
obtusa	obtus	k1gMnSc4	obtus
<g/>
)	)	kIx)	)
a	a	k8xC	a
křepenku	křepenka	k1gFnSc4	křepenka
bledou	bledý	k2eAgFnSc4d1	bledá
(	(	kIx(	(
<g/>
Cephalozia	Cephalozius	k1gMnSc2	Cephalozius
leucantha	leucanth	k1gMnSc2	leucanth
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
chudozubíka	chudozubík	k1gMnSc2	chudozubík
Brownového	Brownový	k2eAgMnSc2d1	Brownový
(	(	kIx(	(
<g/>
Tetrodontium	Tetrodontium	k1gNnSc1	Tetrodontium
brownianum	brownianum	k1gNnSc1	brownianum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kýlnatku	kýlnatka	k1gFnSc4	kýlnatka
drobnolistáou	drobnolistáá	k1gFnSc4	drobnolistáá
(	(	kIx(	(
<g/>
Scapania	Scapanium	k1gNnSc2	Scapanium
lingulata	lingule	k1gNnPc1	lingule
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nivenku	nivenka	k1gFnSc4	nivenka
štítovitáou	štítovitáý	k2eAgFnSc4d1	štítovitáý
(	(	kIx(	(
<g/>
Harpnathus	Harpnathus	k1gMnSc1	Harpnathus
scutatus	scutatus	k1gMnSc1	scutatus
<g/>
)	)	kIx)	)
či	či	k8xC	či
úzkolistce	úzkolistka	k1gFnSc3	úzkolistka
dlouholistého	dlouholistý	k2eAgNnSc2d1	dlouholistý
(	(	kIx(	(
<g/>
Rhynchostegiella	Rhynchostegiello	k1gNnSc2	Rhynchostegiello
teneriffae	teneriffa	k1gInSc2	teneriffa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
parku	park	k1gInSc6	park
nalezen	naleznout	k5eAaPmNgInS	naleznout
nový	nový	k2eAgInSc1d1	nový
druh	druh	k1gInSc1	druh
játrovky	játrovka	k1gFnSc2	játrovka
pro	pro	k7c4	pro
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
mokřanka	mokřanka	k1gFnSc1	mokřanka
oddálená	oddálený	k2eAgFnSc1d1	oddálená
(	(	kIx(	(
<g/>
Hygrobiella	Hygrobiella	k1gFnSc1	Hygrobiella
laxifolia	laxifolia	k1gFnSc1	laxifolia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
typičtějších	typický	k2eAgInPc2d2	typičtější
druhů	druh	k1gInPc2	druh
lze	lze	k6eAd1	lze
jmenovat	jmenovat	k5eAaImF	jmenovat
pískovce	pískovec	k1gInPc1	pískovec
prorůstající	prorůstající	k2eAgInSc1d1	prorůstající
čtyřzoubek	čtyřzoubek	k1gInSc4	čtyřzoubek
průzračný	průzračný	k2eAgInSc4d1	průzračný
(	(	kIx(	(
<g/>
Tetraphis	Tetraphis	k1gFnSc1	Tetraphis
pellucida	pellucida	k1gFnSc1	pellucida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kryjnice	kryjnice	k1gFnSc1	kryjnice
Meylanova	Meylanův	k2eAgFnSc1d1	Meylanův
(	(	kIx(	(
<g/>
Calypogeia	Calypogeia	k1gFnSc1	Calypogeia
integristipula	integristipula	k1gFnSc1	integristipula
<g/>
)	)	kIx)	)
a	a	k8xC	a
bělomech	bělomech	k1gInSc1	bělomech
skalní	skalní	k2eAgInSc1d1	skalní
(	(	kIx(	(
<g/>
Leucobryum	Leucobryum	k1gInSc1	Leucobryum
juniperoideum	juniperoideum	k1gInSc1	juniperoideum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgMnSc7d1	typický
obyvatelem	obyvatel	k1gMnSc7	obyvatel
podmáčených	podmáčený	k2eAgFnPc2d1	podmáčená
smrčin	smrčina	k1gFnPc2	smrčina
je	být	k5eAaImIp3nS	být
rohozec	rohozec	k1gInSc1	rohozec
trojlaločný	trojlaločný	k2eAgInSc1d1	trojlaločný
(	(	kIx(	(
<g/>
Bazzania	Bazzanium	k1gNnSc2	Bazzanium
trilobata	trilobata	k1gFnSc1	trilobata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
sušší	suchý	k2eAgInPc4d2	sušší
bory	bor	k1gInPc4	bor
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
křivonožka	křivonožka	k1gMnSc1	křivonožka
zprohýbaná	zprohýbaný	k2eAgFnSc1d1	zprohýbaná
(	(	kIx(	(
<g/>
Campylopus	Campylopus	k1gMnSc1	Campylopus
flexuosus	flexuosus	k1gMnSc1	flexuosus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
mykologického	mykologický	k2eAgInSc2d1	mykologický
průzkumu	průzkum	k1gInSc2	průzkum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
zaměřeného	zaměřený	k2eAgMnSc2d1	zaměřený
na	na	k7c6	na
bučiny	bučina	k1gFnSc2	bučina
<g/>
,	,	kIx,	,
reliktní	reliktní	k2eAgInPc1d1	reliktní
bory	bor	k1gInPc1	bor
<g/>
,	,	kIx,	,
inverzní	inverzní	k2eAgFnSc1d1	inverzní
rokle	rokle	k1gFnSc1	rokle
a	a	k8xC	a
spáleniště	spáleniště	k1gNnSc1	spáleniště
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
15	[number]	k4	15
druhů	druh	k1gInPc2	druh
makromycetů	makromycet	k1gInPc2	makromycet
zařazených	zařazený	k2eAgInPc2d1	zařazený
do	do	k7c2	do
Červeného	Červeného	k2eAgInSc2d1	Červeného
seznamu	seznam	k1gInSc2	seznam
hub	houba	k1gFnPc2	houba
(	(	kIx(	(
<g/>
makromycetů	makromycet	k1gMnPc2	makromycet
<g/>
)	)	kIx)	)
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bučinách	bučina	k1gFnPc6	bučina
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
pavučinec	pavučinec	k1gInSc1	pavučinec
načervenalý	načervenalý	k2eAgInSc1d1	načervenalý
nevroubený	vroubený	k2eNgInSc1d1	vroubený
(	(	kIx(	(
<g/>
Cortinarius	Cortinarius	k1gInSc1	Cortinarius
purpurascens	purpurascens	k1gInSc1	purpurascens
var.	var.	k?	var.
largusoides	largusoides	k1gInSc1	largusoides
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
korálovec	korálovec	k1gMnSc1	korálovec
ježatý	ježatý	k2eAgMnSc1d1	ježatý
(	(	kIx(	(
<g/>
Hericium	Hericium	k1gNnSc1	Hericium
erinaceus	erinaceus	k1gInSc1	erinaceus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šťavnatka	šťavnatka	k1gFnSc1	šťavnatka
rezavějící	rezavějící	k2eAgFnSc1d1	rezavějící
(	(	kIx(	(
<g/>
Hygrophorus	Hygrophorus	k1gMnSc1	Hygrophorus
discoxanthus	discoxanthus	k1gMnSc1	discoxanthus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šťavnatka	šťavnatka	k1gFnSc1	šťavnatka
básnická	básnický	k2eAgFnSc1d1	básnická
(	(	kIx(	(
<g/>
Hygrophorus	Hygrophorus	k1gInSc1	Hygrophorus
poetarum	poetarum	k1gInSc1	poetarum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ryzec	ryzec	k1gMnSc1	ryzec
rudohrdlý	rudohrdlý	k2eAgMnSc1d1	rudohrdlý
(	(	kIx(	(
<g/>
Lactarius	Lactarius	k1gMnSc1	Lactarius
rubrocinctus	rubrocinctus	k1gMnSc1	rubrocinctus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
helmovka	helmovka	k1gFnSc1	helmovka
dvojvonná	dvojvonný	k2eAgFnSc1d1	dvojvonný
(	(	kIx(	(
<g/>
Mycena	Mycen	k2eAgFnSc1d1	Mycena
diosma	diosma	k1gFnSc1	diosma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
štítovka	štítovka	k1gFnSc1	štítovka
lemovaná	lemovaný	k2eAgFnSc1d1	lemovaná
(	(	kIx(	(
<g/>
Pluteus	pluteus	k1gMnSc1	pluteus
luctuosus	luctuosus	k1gMnSc1	luctuosus
<g/>
)	)	kIx)	)
a	a	k8xC	a
holubinka	holubinka	k1gFnSc1	holubinka
sluneční	sluneční	k2eAgFnSc1d1	sluneční
(	(	kIx(	(
<g/>
Russula	Russula	k1gFnSc1	Russula
solaris	solaris	k1gFnSc2	solaris
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
inverzních	inverzní	k2eAgFnPc6d1	inverzní
roklích	rokle	k1gFnPc6	rokle
byly	být	k5eAaImAgFnP	být
objeveny	objevit	k5eAaPmNgFnP	objevit
mecháček	mecháček	k1gInSc4	mecháček
hladký	hladký	k2eAgInSc4d1	hladký
(	(	kIx(	(
<g/>
Cyphellostereum	Cyphellostereum	k1gInSc4	Cyphellostereum
laeve	laeev	k1gFnSc2	laeev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kalichovka	kalichovka	k1gFnSc1	kalichovka
leptoniová	leptoniový	k2eAgFnSc1d1	leptoniový
(	(	kIx(	(
<g/>
Omphalina	Omphalina	k1gFnSc1	Omphalina
epichysium	epichysium	k1gNnSc4	epichysium
<g/>
)	)	kIx)	)
a	a	k8xC	a
žilnatka	žilnatka	k1gFnSc1	žilnatka
bledá	bledý	k2eAgFnSc1d1	bledá
(	(	kIx(	(
<g/>
Phlebia	Phlebia	k1gFnSc1	Phlebia
centrifuga	centrifuga	k1gFnSc1	centrifuga
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reliktních	reliktní	k2eAgInPc6d1	reliktní
borech	bor	k1gInPc6	bor
roste	růst	k5eAaImIp3nS	růst
hřib	hřib	k1gInSc1	hřib
borový	borový	k2eAgInSc1d1	borový
(	(	kIx(	(
<g/>
Boletus	Boletus	k1gInSc1	Boletus
pinophilus	pinophilus	k1gInSc1	pinophilus
<g/>
)	)	kIx)	)
a	a	k8xC	a
šťavnatka	šťavnatka	k1gFnSc1	šťavnatka
slizoprstenná	slizoprstenný	k2eAgFnSc1d1	slizoprstenný
(	(	kIx(	(
<g/>
Hygrophorus	Hygrophorus	k1gMnSc1	Hygrophorus
gliocyclus	gliocyclus	k1gMnSc1	gliocyclus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
spáleništi	spáleniště	k1gNnSc6	spáleniště
roste	růst	k5eAaImIp3nS	růst
liškovec	liškovec	k1gInSc1	liškovec
spáleništní	spáleništní	k2eAgInSc1d1	spáleništní
(	(	kIx(	(
<g/>
Faerberia	Faerberium	k1gNnSc2	Faerberium
carbonaria	carbonarium	k1gNnSc2	carbonarium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zákonem	zákon	k1gInSc7	zákon
chráněných	chráněný	k2eAgInPc2d1	chráněný
druhů	druh	k1gInPc2	druh
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
doložena	doložit	k5eAaPmNgFnS	doložit
bolinka	bolinka	k1gFnSc1	bolinka
černohnědá	černohnědý	k2eAgFnSc1d1	černohnědá
(	(	kIx(	(
<g/>
Camarops	Camarops	k1gInSc1	Camarops
tubulina	tubulin	k2eAgMnSc2d1	tubulin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
parku	park	k1gInSc2	park
se	se	k3xPyFc4	se
vyskytovalo	vyskytovat	k5eAaImAgNnS	vyskytovat
přinejmenším	přinejmenším	k6eAd1	přinejmenším
62	[number]	k4	62
druhů	druh	k1gInPc2	druh
savců	savec	k1gMnPc2	savec
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
však	však	k9	však
byli	být	k5eAaImAgMnP	být
už	už	k6eAd1	už
dávno	dávno	k6eAd1	dávno
vyhubeni	vyhuben	k2eAgMnPc1d1	vyhuben
<g/>
,	,	kIx,	,
typicky	typicky	k6eAd1	typicky
vlk	vlk	k1gMnSc1	vlk
obecný	obecný	k2eAgMnSc1d1	obecný
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kočka	kočka	k1gFnSc1	kočka
divoká	divoký	k2eAgFnSc1d1	divoká
(	(	kIx(	(
<g/>
Felis	Felis	k1gFnSc1	Felis
silvestris	silvestris	k1gFnSc2	silvestris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
medvěd	medvěd	k1gMnSc1	medvěd
hnědý	hnědý	k2eAgMnSc1d1	hnědý
(	(	kIx(	(
<g/>
Ursus	Ursus	k1gMnSc1	Ursus
arctos	arctos	k1gMnSc1	arctos
<g/>
)	)	kIx)	)
a	a	k8xC	a
los	los	k1gInSc1	los
evropský	evropský	k2eAgInSc1d1	evropský
(	(	kIx(	(
<g/>
Alces	Alces	k1gInSc1	Alces
alces	alces	k1gInSc1	alces
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
současných	současný	k2eAgMnPc2d1	současný
zástupců	zástupce	k1gMnPc2	zástupce
savců	savec	k1gMnPc2	savec
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
např.	např.	kA	např.
rejsec	rejsec	k1gMnSc1	rejsec
vodní	vodní	k2eAgMnSc1d1	vodní
(	(	kIx(	(
<g/>
Neomys	Neomys	k1gInSc1	Neomys
fodiens	fodiens	k1gInSc1	fodiens
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rejsec	rejsec	k1gInSc1	rejsec
černý	černý	k2eAgInSc1d1	černý
(	(	kIx(	(
<g/>
Neomys	Neomys	k1gInSc1	Neomys
anomalus	anomalus	k1gInSc1	anomalus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bobr	bobr	k1gMnSc1	bobr
evropský	evropský	k2eAgMnSc1d1	evropský
(	(	kIx(	(
<g/>
Castor	Castor	k1gMnSc1	Castor
fiber	fiber	k1gMnSc1	fiber
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hraboš	hraboš	k1gMnSc1	hraboš
mokřadní	mokřadní	k2eAgMnSc1d1	mokřadní
(	(	kIx(	(
<g/>
Microtus	Microtus	k1gInSc1	Microtus
agrestis	agrestis	k1gFnSc2	agrestis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
myšice	myšice	k1gFnSc1	myšice
temnopásá	temnopásý	k2eAgFnSc1d1	temnopásá
(	(	kIx(	(
<g/>
Apodemus	Apodemus	k1gMnSc1	Apodemus
agrarius	agrarius	k1gMnSc1	agrarius
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plch	plch	k1gMnSc1	plch
velký	velký	k2eAgMnSc1d1	velký
(	(	kIx(	(
<g/>
Myoxus	Myoxus	k1gInSc1	Myoxus
glis	glis	k1gInSc1	glis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plch	plch	k1gMnSc1	plch
zahradní	zahradní	k2eAgMnSc1d1	zahradní
(	(	kIx(	(
<g/>
Eliomys	Eliomys	k1gInSc1	Eliomys
quercinus	quercinus	k1gInSc1	quercinus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plšík	plšík	k1gMnSc1	plšík
lískový	lískový	k2eAgMnSc1d1	lískový
(	(	kIx(	(
<g/>
Muscardinus	Muscardinus	k1gMnSc1	Muscardinus
avellanarius	avellanarius	k1gMnSc1	avellanarius
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
vydra	vydra	k1gFnSc1	vydra
říční	říční	k2eAgFnSc1d1	říční
(	(	kIx(	(
<g/>
Lutra	Lutra	k1gFnSc1	Lutra
lutra	lutra	k1gMnSc1	lutra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
větších	veliký	k2eAgFnPc2d2	veliký
šelem	šelma	k1gFnPc2	šelma
lze	lze	k6eAd1	lze
zmínit	zmínit	k5eAaPmF	zmínit
především	především	k9	především
rysa	rys	k1gMnSc4	rys
ostrovida	ostrovid	k1gMnSc4	ostrovid
(	(	kIx(	(
<g/>
Lynx	Lynx	k1gInSc1	Lynx
lynx	lynx	k1gInSc1	lynx
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
parku	park	k1gInSc6	park
pravidelně	pravidelně	k6eAd1	pravidelně
rozmnožuje	rozmnožovat	k5eAaImIp3nS	rozmnožovat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
přežvýkavců	přežvýkavec	k1gMnPc2	přežvýkavec
lze	lze	k6eAd1	lze
v	v	k7c6	v
rezervaci	rezervace	k1gFnSc6	rezervace
narazit	narazit	k5eAaPmF	narazit
na	na	k7c4	na
kamzíka	kamzík	k1gMnSc4	kamzík
horského	horský	k2eAgNnSc2d1	horské
(	(	kIx(	(
<g/>
Rupicapra	Rupicapro	k1gNnSc2	Rupicapro
rupicapra	rupicapr	k1gInSc2	rupicapr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jelena	jelen	k1gMnSc2	jelen
evropského	evropský	k2eAgMnSc2d1	evropský
(	(	kIx(	(
<g/>
Cervus	Cervus	k1gInSc1	Cervus
elaphus	elaphus	k1gInSc1	elaphus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
srnce	srnec	k1gMnSc2	srnec
obecného	obecný	k2eAgMnSc2d1	obecný
(	(	kIx(	(
<g/>
Capreolus	Capreolus	k1gInSc1	Capreolus
capreolus	capreolus	k1gInSc1	capreolus
<g/>
)	)	kIx)	)
a	a	k8xC	a
prase	prase	k1gNnSc1	prase
divoké	divoký	k2eAgNnSc1d1	divoké
(	(	kIx(	(
<g/>
Sus	Sus	k1gMnSc4	Sus
scrofa	scrof	k1gMnSc4	scrof
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Park	park	k1gInSc1	park
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
velmi	velmi	k6eAd1	velmi
příhodné	příhodný	k2eAgFnPc4d1	příhodná
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
výskyt	výskyt	k1gInSc4	výskyt
netopýrů	netopýr	k1gMnPc2	netopýr
<g/>
,	,	kIx,	,
kterých	který	k3yIgMnPc2	který
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
až	až	k9	až
15	[number]	k4	15
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Zmínit	zmínit	k5eAaPmF	zmínit
lze	lze	k6eAd1	lze
např.	např.	kA	např.
vrápence	vrápenec	k1gMnSc2	vrápenec
malého	malý	k2eAgMnSc2d1	malý
(	(	kIx(	(
<g/>
Rhinolophus	Rhinolophus	k1gInSc1	Rhinolophus
hipposideros	hipposiderosa	k1gFnPc2	hipposiderosa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
netopýra	netopýr	k1gMnSc2	netopýr
velkého	velký	k2eAgMnSc2d1	velký
(	(	kIx(	(
<g/>
Myotis	Myotis	k1gInSc1	Myotis
myotis	myotis	k1gFnSc2	myotis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
netopýra	netopýr	k1gMnSc2	netopýr
rezavého	rezavý	k2eAgMnSc2d1	rezavý
(	(	kIx(	(
<g/>
Nyctalus	Nyctalus	k1gInSc1	Nyctalus
noctula	noctulum	k1gNnSc2	noctulum
<g/>
)	)	kIx)	)
a	a	k8xC	a
netopýra	netopýr	k1gMnSc2	netopýr
hvízdavého	hvízdavý	k2eAgMnSc2d1	hvízdavý
(	(	kIx(	(
<g/>
Pipistrellus	Pipistrellus	k1gInSc1	Pipistrellus
pipistrellus	pipistrellus	k1gInSc1	pipistrellus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Českém	český	k2eAgNnSc6d1	české
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
227	[number]	k4	227
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
148	[number]	k4	148
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
představují	představovat	k5eAaImIp3nP	představovat
hnízdící	hnízdící	k2eAgInPc1d1	hnízdící
druhy	druh	k1gInPc1	druh
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
větších	veliký	k2eAgMnPc2d2	veliký
ptáků	pták	k1gMnPc2	pták
zde	zde	k6eAd1	zde
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
mj.	mj.	kA	mj.
čáp	čáp	k1gMnSc1	čáp
černý	černý	k1gMnSc1	černý
(	(	kIx(	(
<g/>
Ciconia	Ciconium	k1gNnSc2	Ciconium
nigra	nigr	k1gMnSc2	nigr
<g/>
)	)	kIx)	)
a	a	k8xC	a
sokol	sokol	k1gMnSc1	sokol
stěhovavý	stěhovavý	k2eAgMnSc1d1	stěhovavý
(	(	kIx(	(
<g/>
Falco	Falco	k1gMnSc1	Falco
peregrinus	peregrinus	k1gMnSc1	peregrinus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
zdejší	zdejší	k2eAgFnSc1d1	zdejší
populace	populace	k1gFnSc1	populace
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
výskyt	výskyt	k1gInSc1	výskyt
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
např.	např.	kA	např.
u	u	k7c2	u
bekasiny	bekasina	k1gFnSc2	bekasina
otavní	otavní	k2eAgMnSc1d1	otavní
(	(	kIx(	(
<g/>
Gallinago	Gallinago	k1gMnSc1	Gallinago
gallinago	gallinago	k1gMnSc1	gallinago
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bramborníčka	bramborníček	k1gMnSc2	bramborníček
hnědého	hnědý	k2eAgMnSc2d1	hnědý
(	(	kIx(	(
<g/>
Saxicola	Saxicola	k1gFnSc1	Saxicola
rubetra	rubetra	k1gFnSc1	rubetra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
datla	datel	k1gMnSc4	datel
černého	černý	k1gMnSc4	černý
(	(	kIx(	(
<g/>
Dryocopus	Dryocopus	k1gMnSc1	Dryocopus
martius	martius	k1gMnSc1	martius
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chřástala	chřástal	k1gMnSc2	chřástal
vodního	vodní	k2eAgMnSc2d1	vodní
(	(	kIx(	(
<g/>
Rallus	Rallus	k1gInSc1	Rallus
aquaticus	aquaticus	k1gInSc1	aquaticus
<g/>
)	)	kIx)	)
i	i	k8xC	i
polního	polní	k2eAgMnSc2d1	polní
(	(	kIx(	(
<g/>
Crex	Crex	k1gInSc1	Crex
crex	crex	k1gInSc1	crex
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
křepelky	křepelka	k1gFnPc1	křepelka
polní	polní	k2eAgFnPc1d1	polní
(	(	kIx(	(
<g/>
Coturnix	Coturnix	k1gInSc1	Coturnix
coturnix	coturnix	k1gInSc1	coturnix
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ledňáčka	ledňáček	k1gMnSc2	ledňáček
říčního	říční	k2eAgMnSc2d1	říční
(	(	kIx(	(
<g/>
Alcedo	Alcedo	k1gNnSc1	Alcedo
atthis	atthis	k1gFnPc2	atthis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lelka	lelek	k1gMnSc2	lelek
lesního	lesní	k2eAgMnSc2d1	lesní
(	(	kIx(	(
<g/>
Caprimulgus	Caprimulgus	k1gInSc1	Caprimulgus
europaeus	europaeus	k1gInSc1	europaeus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pisíka	pisík	k1gMnSc2	pisík
obecného	obecný	k2eAgMnSc2d1	obecný
(	(	kIx(	(
<g/>
Actitis	Actitis	k1gFnSc1	Actitis
hypoleucos	hypoleucos	k1gMnSc1	hypoleucos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skorce	skorec	k1gMnSc2	skorec
vodního	vodní	k2eAgMnSc2d1	vodní
(	(	kIx(	(
<g/>
Cinclus	Cinclus	k1gInSc1	Cinclus
cinclus	cinclus	k1gInSc1	cinclus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tetřívka	tetřívek	k1gMnSc2	tetřívek
obecného	obecný	k2eAgMnSc2d1	obecný
(	(	kIx(	(
<g/>
Tetrao	Tetrao	k1gNnSc1	Tetrao
tetrix	tetrix	k1gInSc1	tetrix
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ťuhýka	ťuhýk	k1gMnSc2	ťuhýk
obecného	obecný	k2eAgMnSc2d1	obecný
(	(	kIx(	(
<g/>
Lanius	Lanius	k1gInSc1	Lanius
collurio	collurio	k1gNnSc1	collurio
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sluky	sluka	k1gFnPc1	sluka
lesní	lesní	k2eAgFnPc1d1	lesní
(	(	kIx(	(
<g/>
Scolopax	Scolopax	k1gInSc1	Scolopax
rusticola	rusticola	k1gFnSc1	rusticola
<g/>
)	)	kIx)	)
a	a	k8xC	a
žluvy	žluva	k1gFnPc1	žluva
hajní	hajní	k2eAgFnPc1d1	hajní
(	(	kIx(	(
<g/>
Oriolus	Oriolus	k1gMnSc1	Oriolus
oriolus	oriolus	k1gMnSc1	oriolus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Park	park	k1gInSc1	park
je	být	k5eAaImIp3nS	být
bohatý	bohatý	k2eAgInSc1d1	bohatý
na	na	k7c4	na
výskyt	výskyt	k1gInSc4	výskyt
sov	sova	k1gFnPc2	sova
<g/>
,	,	kIx,	,
kterých	který	k3yRgInPc2	který
zde	zde	k6eAd1	zde
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
nejméně	málo	k6eAd3	málo
sedm	sedm	k4xCc4	sedm
druhů	druh	k1gInPc2	druh
<g/>
;	;	kIx,	;
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
např.	např.	kA	např.
o	o	k7c6	o
kulíška	kulíšek	k1gMnSc2	kulíšek
nejmenšího	malý	k2eAgMnSc2d3	nejmenší
(	(	kIx(	(
<g/>
Glaucidium	Glaucidium	k1gNnSc1	Glaucidium
passerinum	passerinum	k1gNnSc1	passerinum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sýce	sýc	k1gMnSc2	sýc
rousného	rousný	k2eAgMnSc2d1	rousný
(	(	kIx(	(
<g/>
Aegolius	Aegolius	k1gInSc1	Aegolius
funereus	funereus	k1gInSc1	funereus
<g/>
)	)	kIx)	)
a	a	k8xC	a
výra	výr	k1gMnSc2	výr
velkého	velký	k2eAgMnSc2d1	velký
(	(	kIx(	(
<g/>
Bubo	Bubo	k1gNnSc1	Bubo
bubo	bubo	k1gNnSc1	bubo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bezpečně	bezpečně	k6eAd1	bezpečně
prokázána	prokázán	k2eAgFnSc1d1	prokázána
byla	být	k5eAaImAgFnS	být
přítomnost	přítomnost	k1gFnSc1	přítomnost
16	[number]	k4	16
druhů	druh	k1gInPc2	druh
plazů	plaz	k1gInPc2	plaz
a	a	k8xC	a
8	[number]	k4	8
druhů	druh	k1gInPc2	druh
obojživelníků	obojživelník	k1gMnPc2	obojživelník
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
parku	park	k1gInSc6	park
už	už	k6eAd1	už
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
želva	želva	k1gFnSc1	želva
bahenní	bahenní	k2eAgFnSc1d1	bahenní
(	(	kIx(	(
<g/>
Emys	Emys	k1gInSc1	Emys
orbicularis	orbicularis	k1gFnSc2	orbicularis
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
i	i	k9	i
užovka	užovka	k1gFnSc1	užovka
podplamatá	podplamatá	k1gFnSc1	podplamatá
(	(	kIx(	(
<g/>
Natrix	Natrix	k1gInSc1	Natrix
tessellata	tesselle	k1gNnPc4	tesselle
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
mlokovitých	mlokovití	k1gMnPc2	mlokovití
lze	lze	k6eAd1	lze
zmínit	zmínit	k5eAaPmF	zmínit
především	především	k9	především
mloka	mlok	k1gMnSc2	mlok
skvrnitého	skvrnitý	k2eAgMnSc2d1	skvrnitý
(	(	kIx(	(
<g/>
Salamandra	salamandr	k1gMnSc2	salamandr
salamandra	salamandr	k1gMnSc2	salamandr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čolka	čolek	k1gMnSc2	čolek
hranatého	hranatý	k2eAgMnSc2d1	hranatý
(	(	kIx(	(
<g/>
Triturus	Triturus	k1gInSc1	Triturus
helveticus	helveticus	k1gInSc1	helveticus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čolka	čolek	k1gMnSc2	čolek
horského	horský	k2eAgMnSc2d1	horský
(	(	kIx(	(
<g/>
Triturus	Triturus	k1gInSc1	Triturus
alpestris	alpestris	k1gFnSc2	alpestris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čolka	čolek	k1gMnSc2	čolek
obecného	obecný	k2eAgMnSc2d1	obecný
(	(	kIx(	(
<g/>
Triturus	Triturus	k1gInSc1	Triturus
vulgaris	vulgaris	k1gFnSc2	vulgaris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzácně	vzácně	k6eAd1	vzácně
se	se	k3xPyFc4	se
v	v	k7c6	v
parku	park	k1gInSc6	park
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
čolek	čolek	k1gMnSc1	čolek
velký	velký	k2eAgMnSc1d1	velký
(	(	kIx(	(
<g/>
Triturus	Triturus	k1gMnSc1	Triturus
cristatus	cristatus	k1gMnSc1	cristatus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
populace	populace	k1gFnSc2	populace
žab	žába	k1gFnPc2	žába
je	být	k5eAaImIp3nS	být
nejčastější	častý	k2eAgMnSc1d3	nejčastější
skokan	skokan	k1gMnSc1	skokan
skřehotavý	skřehotavý	k2eAgMnSc1d1	skřehotavý
(	(	kIx(	(
<g/>
Rana	Rana	k1gFnSc1	Rana
ridibunda	ridibunda	k1gFnSc1	ridibunda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skokan	skokan	k1gMnSc1	skokan
štíhlý	štíhlý	k2eAgMnSc1d1	štíhlý
(	(	kIx(	(
<g/>
Rana	Rana	k1gFnSc1	Rana
dalmatina	dalmatin	k1gMnSc2	dalmatin
<g/>
)	)	kIx)	)
a	a	k8xC	a
skokan	skokan	k1gMnSc1	skokan
ostronosý	ostronosý	k2eAgMnSc1d1	ostronosý
(	(	kIx(	(
<g/>
Rana	Rana	k1gFnSc1	Rana
arvalis	arvalis	k1gFnSc2	arvalis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzácný	vzácný	k2eAgMnSc1d1	vzácný
je	být	k5eAaImIp3nS	být
výskyt	výskyt	k1gInSc1	výskyt
kuňky	kuňka	k1gFnSc2	kuňka
obecné	obecná	k1gFnSc2	obecná
(	(	kIx(	(
<g/>
Bombina	Bombin	k2eAgFnSc1d1	Bombina
bombina	bombina	k1gFnSc1	bombina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
častý	častý	k2eAgInSc1d1	častý
je	být	k5eAaImIp3nS	být
výskyt	výskyt	k1gInSc1	výskyt
ještěrky	ještěrka	k1gFnSc2	ještěrka
živorodé	živorodý	k2eAgNnSc1d1	živorodé
(	(	kIx(	(
<g/>
Zootoca	Zootoc	k2eAgFnSc1d1	Zootoca
vivipara	vivipara	k1gFnSc1	vivipara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ještěrky	ještěrka	k1gFnPc4	ještěrka
obecné	obecná	k1gFnSc2	obecná
(	(	kIx(	(
<g/>
Lacerta	Lacerta	k1gFnSc1	Lacerta
agilis	agilis	k1gFnSc2	agilis
<g/>
)	)	kIx)	)
i	i	k8xC	i
zmije	zmije	k1gFnSc1	zmije
obecné	obecná	k1gFnSc2	obecná
(	(	kIx(	(
<g/>
Vipera	Vipera	k1gFnSc1	Vipera
berus	berus	k1gMnSc1	berus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
relativně	relativně	k6eAd1	relativně
vzácně	vzácně	k6eAd1	vzácně
se	se	k3xPyFc4	se
v	v	k7c6	v
parku	park	k1gInSc6	park
objevuje	objevovat	k5eAaImIp3nS	objevovat
užovka	užovka	k1gFnSc1	užovka
hladká	hladký	k2eAgFnSc1d1	hladká
(	(	kIx(	(
<g/>
Coronella	Coronella	k1gFnSc1	Coronella
austriaca	austriaca	k1gMnSc1	austriaca
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ryby	Ryby	k1gFnPc1	Ryby
se	se	k3xPyFc4	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Labských	labský	k2eAgInPc2d1	labský
pískovců	pískovec	k1gInPc2	pískovec
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Labi	Labe	k1gNnSc6	Labe
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
jen	jen	k6eAd1	jen
míjí	míjet	k5eAaImIp3nS	míjet
západní	západní	k2eAgInSc4d1	západní
okraj	okraj	k1gInSc4	okraj
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
tokem	tok	k1gInSc7	tok
Českého	český	k2eAgNnSc2d1	české
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
je	být	k5eAaImIp3nS	být
Kamenice	Kamenice	k1gFnSc1	Kamenice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
toky	tok	k1gInPc7	tok
parku	park	k1gInSc2	park
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Červený	červený	k2eAgInSc1d1	červený
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Jetřichovický	Jetřichovický	k2eAgInSc1d1	Jetřichovický
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Křinice	Křinice	k1gFnSc1	Křinice
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
ke	k	k7c3	k
pstruhovému	pstruhový	k2eAgNnSc3d1	pstruhové
pásmu	pásmo	k1gNnSc3	pásmo
s	s	k7c7	s
hojným	hojný	k2eAgInSc7d1	hojný
výskytem	výskyt	k1gInSc7	výskyt
pstruha	pstruh	k1gMnSc2	pstruh
obecného	obecný	k2eAgMnSc2d1	obecný
(	(	kIx(	(
<g/>
Salmo	Salma	k1gFnSc5	Salma
trutta	trutto	k1gNnPc4	trutto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgMnPc2d1	další
zástupců	zástupce	k1gMnPc2	zástupce
ryb	ryba	k1gFnPc2	ryba
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
tocích	tok	k1gInPc6	tok
lze	lze	k6eAd1	lze
jmenovat	jmenovat	k5eAaBmF	jmenovat
lipana	lipan	k1gMnSc2	lipan
podhorního	podhorní	k2eAgMnSc2d1	podhorní
(	(	kIx(	(
<g/>
Thymallus	Thymallus	k1gInSc1	Thymallus
thymallus	thymallus	k1gInSc1	thymallus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vranku	vranka	k1gFnSc4	vranka
obecnou	obecný	k2eAgFnSc4d1	obecná
(	(	kIx(	(
<g/>
Cottus	Cottus	k1gMnSc1	Cottus
gobio	gobio	k1gMnSc1	gobio
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jílovském	jílovský	k2eAgInSc6d1	jílovský
potoce	potok	k1gInSc6	potok
ještě	ještě	k6eAd1	ještě
žije	žít	k5eAaImIp3nS	žít
střevle	střevle	k1gFnSc1	střevle
potoční	potoční	k2eAgFnSc1d1	potoční
(	(	kIx(	(
<g/>
Phoxinus	Phoxinus	k1gMnSc1	Phoxinus
phoxinus	phoxinus	k1gMnSc1	phoxinus
<g/>
)	)	kIx)	)
a	a	k8xC	a
mřenka	mřenka	k1gFnSc1	mřenka
mramorovaná	mramorovaný	k2eAgFnSc1d1	mramorovaná
(	(	kIx(	(
<g/>
Noemacheilus	Noemacheilus	k1gMnSc1	Noemacheilus
barbatulus	barbatulus	k1gMnSc1	barbatulus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
kruhoústých	kruhoústí	k1gMnPc2	kruhoústí
se	se	k3xPyFc4	se
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
už	už	k6eAd1	už
jen	jen	k9	jen
mihule	mihule	k1gFnSc1	mihule
potoční	potoční	k2eAgFnSc1d1	potoční
(	(	kIx(	(
<g/>
Lampetra	Lampetra	k1gFnSc1	Lampetra
planeri	planer	k1gFnSc2	planer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
toku	tok	k1gInSc6	tok
Křinice	Křinice	k1gFnSc2	Křinice
a	a	k8xC	a
Kamenice	Kamenice	k1gFnSc2	Kamenice
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
Správa	správa	k1gFnSc1	správa
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
snaží	snažit	k5eAaImIp3nS	snažit
o	o	k7c4	o
navrácení	navrácení	k1gNnSc4	navrácení
lososa	losos	k1gMnSc2	losos
obecného	obecný	k2eAgMnSc2d1	obecný
(	(	kIx(	(
<g/>
Salmo	Salma	k1gFnSc5	Salma
salar	salar	k1gInSc4	salar
<g/>
)	)	kIx)	)
do	do	k7c2	do
vod	voda	k1gFnPc2	voda
Kamenice	Kamenice	k1gFnSc2	Kamenice
<g/>
.	.	kIx.	.
</s>
<s>
Lososi	losos	k1gMnPc1	losos
sem	sem	k6eAd1	sem
kdysi	kdysi	k6eAd1	kdysi
každoročně	každoročně	k6eAd1	každoročně
táhli	táhnout	k5eAaImAgMnP	táhnout
za	za	k7c7	za
třením	tření	k1gNnSc7	tření
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vodních	vodní	k2eAgFnPc2d1	vodní
staveb	stavba	k1gFnPc2	stavba
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Labi	Labe	k1gNnSc6	Labe
však	však	k9	však
lososi	losos	k1gMnPc1	losos
z	z	k7c2	z
Kamenice	Kamenice	k1gFnSc2	Kamenice
i	i	k8xC	i
dalších	další	k2eAgInPc2d1	další
toků	tok	k1gInPc2	tok
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
úplně	úplně	k6eAd1	úplně
vymizeli	vymizet	k5eAaPmAgMnP	vymizet
<g/>
.	.	kIx.	.
</s>
<s>
Navrácení	navrácení	k1gNnSc1	navrácení
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
vysazování	vysazování	k1gNnSc2	vysazování
plůdků	plůdek	k1gInPc2	plůdek
lososa	losos	k1gMnSc2	losos
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
toku	tok	k1gInSc2	tok
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
až	až	k9	až
12	[number]	k4	12
000	[number]	k4	000
plůdků	plůdek	k1gInPc2	plůdek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
nezbývá	zbývat	k5eNaImIp3nS	zbývat
<g/>
,	,	kIx,	,
než	než	k8xS	než
čekat	čekat	k5eAaImF	čekat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
lososi	losos	k1gMnPc1	losos
už	už	k6eAd1	už
jako	jako	k9	jako
dospělí	dospělý	k2eAgMnPc1d1	dospělý
jedinci	jedinec	k1gMnPc1	jedinec
vrátí	vrátit	k5eAaPmIp3nP	vrátit
vytřít	vytřít	k5eAaPmF	vytřít
<g/>
.	.	kIx.	.
</s>
<s>
Úlovky	úlovek	k1gInPc1	úlovek
dospělých	dospělý	k2eAgMnPc2d1	dospělý
jedinců	jedinec	k1gMnPc2	jedinec
jsou	být	k5eAaImIp3nP	být
zatím	zatím	k6eAd1	zatím
ojedinělé	ojedinělý	k2eAgInPc1d1	ojedinělý
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
bezobratlých	bezobratlý	k2eAgMnPc2d1	bezobratlý
živočichů	živočich	k1gMnPc2	živočich
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
parku	park	k1gInSc6	park
velmi	velmi	k6eAd1	velmi
bohatý	bohatý	k2eAgInSc1d1	bohatý
a	a	k8xC	a
různorodý	různorodý	k2eAgInSc1d1	různorodý
<g/>
.	.	kIx.	.
</s>
<s>
Druhová	druhový	k2eAgFnSc1d1	druhová
rozmanitost	rozmanitost	k1gFnSc1	rozmanitost
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
především	především	k9	především
různými	různý	k2eAgInPc7d1	různý
typy	typ	k1gInPc7	typ
biotopů	biotop	k1gInPc2	biotop
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
45	[number]	k4	45
druhů	druh	k1gInPc2	druh
vážek	vážka	k1gFnPc2	vážka
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
v	v	k7c6	v
parku	park	k1gInSc6	park
setkat	setkat	k5eAaPmF	setkat
např.	např.	kA	např.
s	s	k7c7	s
klínatkou	klínatka	k1gFnSc7	klínatka
rohatou	rohatý	k2eAgFnSc7d1	rohatá
(	(	kIx(	(
<g/>
Ophiogomphus	Ophiogomphus	k1gInSc1	Ophiogomphus
cecilia	cecilium	k1gNnSc2	cecilium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
páskovcem	páskovec	k1gInSc7	páskovec
kroužkovaným	kroužkovaný	k2eAgInSc7d1	kroužkovaný
(	(	kIx(	(
<g/>
Cordulegaster	Cordulegaster	k1gInSc1	Cordulegaster
boltoni	boltoň	k1gFnSc3	boltoň
<g/>
)	)	kIx)	)
i	i	k8xC	i
dvojzubým	dvojzubý	k2eAgNnSc7d1	dvojzubý
(	(	kIx(	(
<g/>
Cordulegaster	Cordulegaster	k1gInSc1	Cordulegaster
bidentatus	bidentatus	k1gInSc1	bidentatus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šidélkem	šidélko	k1gNnSc7	šidélko
kopovitým	kopovitý	k2eAgNnSc7d1	kopovitý
(	(	kIx(	(
<g/>
Coenagrion	Coenagrion	k1gInSc1	Coenagrion
hastulatum	hastulatum	k1gNnSc1	hastulatum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šídlem	šídlo	k1gNnSc7	šídlo
<g />
.	.	kIx.	.
</s>
<s>
královským	královský	k2eAgNnSc7d1	královské
(	(	kIx(	(
<g/>
Anax	Anax	k1gInSc1	Anax
imperator	imperator	k1gInSc1	imperator
<g/>
)	)	kIx)	)
i	i	k8xC	i
sítinovým	sítinový	k2eAgInPc3d1	sítinový
(	(	kIx(	(
<g/>
Aeschna	Aeschen	k2eAgNnPc1d1	Aeschen
juncea	junceum	k1gNnPc1	junceum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vážkou	vážka	k1gFnSc7	vážka
čárkovanou	čárkovaný	k2eAgFnSc7d1	čárkovaná
(	(	kIx(	(
<g/>
Leucorrhinia	Leucorrhinium	k1gNnSc2	Leucorrhinium
dubia	dubium	k1gNnSc2	dubium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vážkou	vážka	k1gFnSc7	vážka
jasnoskvrnnou	jasnoskvrnný	k2eAgFnSc7d1	jasnoskvrnná
(	(	kIx(	(
<g/>
Leucorrhinia	Leucorrhinium	k1gNnPc1	Leucorrhinium
pectoralis	pectoralis	k1gFnPc2	pectoralis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vážkou	vážka	k1gFnSc7	vážka
podhorní	podhorní	k2eAgFnSc7d1	podhorní
(	(	kIx(	(
<g/>
Sympetrum	Sympetrum	k1gNnSc1	Sympetrum
pedemontanum	pedemontanum	k1gNnSc1	pedemontanum
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
vážkou	vážka	k1gFnSc7	vážka
tmavou	tmavý	k2eAgFnSc7d1	tmavá
(	(	kIx(	(
<g/>
Sympetrum	Sympetrum	k1gNnSc1	Sympetrum
danae	dana	k1gFnSc2	dana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhově	druhově	k6eAd1	druhově
velmi	velmi	k6eAd1	velmi
rozmanitá	rozmanitý	k2eAgFnSc1d1	rozmanitá
je	být	k5eAaImIp3nS	být
i	i	k9	i
populace	populace	k1gFnSc1	populace
motýlů	motýl	k1gMnPc2	motýl
<g/>
,	,	kIx,	,
kterých	který	k3yIgMnPc2	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
parku	park	k1gInSc6	park
pozorováno	pozorován	k2eAgNnSc1d1	pozorováno
přes	přes	k7c4	přes
1000	[number]	k4	1000
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
stojí	stát	k5eAaImIp3nS	stát
hlavně	hlavně	k9	hlavně
bělásek	bělásek	k1gMnSc1	bělásek
ovocný	ovocný	k2eAgMnSc1d1	ovocný
(	(	kIx(	(
<g/>
Aporia	Aporium	k1gNnPc4	Aporium
crataegi	crataeg	k1gFnSc2	crataeg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hnědásek	hnědásek	k1gMnSc1	hnědásek
chrastavcový	chrastavcový	k2eAgMnSc1d1	chrastavcový
(	(	kIx(	(
<g/>
Euphydryas	Euphydryas	k1gInSc1	Euphydryas
aurinia	aurinium	k1gNnSc2	aurinium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
okáč	okáč	k1gMnSc1	okáč
třeslicový	třeslicový	k2eAgMnSc1d1	třeslicový
(	(	kIx(	(
<g/>
Coenonympha	Coenonympha	k1gFnSc1	Coenonympha
glycerion	glycerion	k1gInSc1	glycerion
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ostruháček	ostruháček	k1gMnSc1	ostruháček
jilmový	jilmový	k2eAgMnSc1d1	jilmový
(	(	kIx(	(
<g/>
Satyrium	Satyrium	k1gNnSc1	Satyrium
w-album	wlbum	k1gNnSc1	w-album
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
otakárek	otakárek	k1gMnSc1	otakárek
fenyklový	fenyklový	k2eAgMnSc1d1	fenyklový
(	(	kIx(	(
<g/>
Papilio	Papilio	k1gMnSc1	Papilio
machaon	machaon	k1gMnSc1	machaon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přástevník	přástevník	k1gMnSc1	přástevník
kostivalový	kostivalový	k2eAgMnSc1d1	kostivalový
či	či	k8xC	či
např.	např.	kA	např.
soumračník	soumračník	k1gMnSc1	soumračník
černohnědý	černohnědý	k2eAgMnSc1d1	černohnědý
(	(	kIx(	(
<g/>
Heteropterus	Heteropterus	k1gMnSc1	Heteropterus
morpheus	morpheus	k1gMnSc1	morpheus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hojný	hojný	k2eAgInSc1d1	hojný
je	být	k5eAaImIp3nS	být
výskyt	výskyt	k1gInSc1	výskyt
okáčů	okáč	k1gMnPc2	okáč
<g/>
,	,	kIx,	,
bělásků	bělásek	k1gMnPc2	bělásek
<g/>
,	,	kIx,	,
žluťásků	žluťásek	k1gMnPc2	žluťásek
či	či	k8xC	či
hnědásků	hnědásek	k1gMnPc2	hnědásek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
babočkovitých	babočkovitý	k2eAgMnPc2d1	babočkovitý
zde	zde	k6eAd1	zde
žijí	žít	k5eAaImIp3nP	žít
mj.	mj.	kA	mj.
babočka	babočka	k1gFnSc1	babočka
admirál	admirál	k1gMnSc1	admirál
<g/>
,	,	kIx,	,
babočka	babočka	k1gFnSc1	babočka
bodláková	bodlákový	k2eAgFnSc1d1	Bodláková
<g/>
,	,	kIx,	,
kopřivová	kopřivový	k2eAgFnSc1d1	kopřivová
a	a	k8xC	a
paví	paví	k2eAgNnSc1d1	paví
oko	oko	k1gNnSc1	oko
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
brouků	brouk	k1gMnPc2	brouk
<g/>
,	,	kIx,	,
čeleď	čeleď	k1gFnSc1	čeleď
střevlíkovitých	střevlíkovitý	k2eAgInPc2d1	střevlíkovitý
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
např.	např.	kA	např.
střevlík	střevlík	k1gMnSc1	střevlík
nepravidelný	pravidelný	k2eNgMnSc1d1	nepravidelný
(	(	kIx(	(
<g/>
Carabus	Carabus	k1gInSc1	Carabus
irregularis	irregularis	k1gFnSc2	irregularis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Carabus	Carabus	k1gMnSc1	Carabus
arvensis	arvensis	k1gFnSc2	arvensis
<g/>
,	,	kIx,	,
Carabus	Carabus	k1gMnSc1	Carabus
auratus	auratus	k1gMnSc1	auratus
<g/>
,	,	kIx,	,
Carabus	Carabus	k1gMnSc1	Carabus
nitens	nitens	k1gInSc1	nitens
<g/>
,	,	kIx,	,
Carabus	Carabus	k1gMnSc1	Carabus
problematicus	problematicus	k1gMnSc1	problematicus
<g/>
,	,	kIx,	,
Dyschirius	Dyschirius	k1gMnSc1	Dyschirius
intermedius	intermedius	k1gMnSc1	intermedius
a	a	k8xC	a
Pterostichus	Pterostichus	k1gMnSc1	Pterostichus
quadrifoveolatus	quadrifoveolatus	k1gMnSc1	quadrifoveolatus
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sta	sto	k4xCgNnPc4	sto
druhů	druh	k1gInPc2	druh
zástupců	zástupce	k1gMnPc2	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
tesaříkovitých	tesaříkovitý	k2eAgMnPc2d1	tesaříkovitý
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
v	v	k7c6	v
parku	park	k1gInSc6	park
žijí	žít	k5eAaImIp3nP	žít
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
zmínit	zmínit	k5eAaPmF	zmínit
vzácné	vzácný	k2eAgInPc1d1	vzácný
druhy	druh	k1gInPc1	druh
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
tesařík	tesařík	k1gMnSc1	tesařík
pasekový	pasekový	k2eAgMnSc1d1	pasekový
(	(	kIx(	(
<g/>
Pachyta	Pachyta	k1gMnSc1	Pachyta
lamed	lamed	k1gMnSc1	lamed
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tesařík	tesařík	k1gMnSc1	tesařík
zavalitý	zavalitý	k2eAgMnSc1d1	zavalitý
(	(	kIx(	(
<g/>
Ergates	Ergates	k1gMnSc1	Ergates
faber	faber	k1gMnSc1	faber
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Acmaeops	Acmaeops	k1gInSc1	Acmaeops
septentrionis	septentrionis	k1gFnSc1	septentrionis
<g/>
,	,	kIx,	,
Anastrangalia	Anastrangalia	k1gFnSc1	Anastrangalia
dubia	dubia	k1gFnSc1	dubia
<g/>
,	,	kIx,	,
Clytus	Clytus	k1gMnSc1	Clytus
lama	lama	k1gMnSc1	lama
a	a	k8xC	a
Pedostrangalia	Pedostrangalia	k1gFnSc1	Pedostrangalia
pubescens	pubescensa	k1gFnPc2	pubescensa
<g/>
.	.	kIx.	.
</s>
<s>
Kovaříkovití	Kovaříkovitý	k2eAgMnPc1d1	Kovaříkovitý
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
parku	park	k1gInSc6	park
reprezentování	reprezentování	k1gNnSc2	reprezentování
mj.	mj.	kA	mj.
druhy	druh	k1gInPc4	druh
Agriotes	Agriotes	k1gMnSc1	Agriotes
pallidulus	pallidulus	k1gMnSc1	pallidulus
<g/>
,	,	kIx,	,
Aplotarsus	Aplotarsus	k1gMnSc1	Aplotarsus
incanus	incanus	k1gMnSc1	incanus
<g/>
,	,	kIx,	,
Cardiophorus	Cardiophorus	k1gMnSc1	Cardiophorus
asellus	asellus	k1gMnSc1	asellus
<g/>
,	,	kIx,	,
Ectinus	Ectinus	k1gMnSc1	Ectinus
aterrimus	aterrimus	k1gMnSc1	aterrimus
<g/>
,	,	kIx,	,
Pseudanostirus	Pseudanostirus	k1gMnSc1	Pseudanostirus
globicollis	globicollis	k1gFnSc2	globicollis
<g/>
,	,	kIx,	,
Sericus	Sericus	k1gMnSc1	Sericus
subaeneus	subaeneus	k1gMnSc1	subaeneus
a	a	k8xC	a
Stenagostus	Stenagostus	k1gMnSc1	Stenagostus
rufus	rufus	k1gMnSc1	rufus
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zástupců	zástupce	k1gMnPc2	zástupce
stehenáčových	stehenáčův	k2eAgMnPc2d1	stehenáčův
se	se	k3xPyFc4	se
v	v	k7c6	v
parku	park	k1gInSc6	park
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
např.	např.	kA	např.
druhy	druh	k1gInPc4	druh
Calopus	Calopus	k1gMnSc1	Calopus
serraticornis	serraticornis	k1gFnSc2	serraticornis
<g/>
,	,	kIx,	,
Clanoptilus	Clanoptilus	k1gMnSc1	Clanoptilus
geniculatus	geniculatus	k1gMnSc1	geniculatus
a	a	k8xC	a
Ischnomera	Ischnomera	k1gFnSc1	Ischnomera
cinerascens	cinerascens	k6eAd1	cinerascens
cinerascens	cinerascens	k6eAd1	cinerascens
<g/>
,	,	kIx,	,
z	z	k7c2	z
nosatcovitých	nosatcovitý	k2eAgFnPc2d1	nosatcovitý
a	a	k8xC	a
mandelinkovitých	mandelinkovitý	k2eAgFnPc2d1	mandelinkovitý
to	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
Aphytobius	Aphytobius	k1gInSc1	Aphytobius
sphaerion	sphaerion	k1gInSc1	sphaerion
<g/>
,	,	kIx,	,
Chrysolina	Chrysolin	k2eAgFnSc1d1	Chrysolina
rufa	rufa	k1gFnSc1	rufa
<g/>
,	,	kIx,	,
Minota	Minota	k1gFnSc1	Minota
obesa	obesa	k1gFnSc1	obesa
<g/>
,	,	kIx,	,
Notaris	Notaris	k1gFnSc1	Notaris
atterimus	atterimus	k1gMnSc1	atterimus
<g/>
,	,	kIx,	,
Otiorhynchus	Otiorhynchus	k1gMnSc1	Otiorhynchus
equestris	equestris	k1gFnSc2	equestris
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
Tropideres	Tropideres	k1gInSc4	Tropideres
albirostris	albirostris	k1gFnSc2	albirostris
<g/>
.	.	kIx.	.
</s>
<s>
Vzácný	vzácný	k2eAgInSc1d1	vzácný
je	být	k5eAaImIp3nS	být
výskyt	výskyt	k1gInSc1	výskyt
chrobáka	chrobák	k1gMnSc2	chrobák
černého	černý	k1gMnSc2	černý
(	(	kIx(	(
<g/>
Typhaeus	Typhaeus	k1gMnSc1	Typhaeus
typhoeus	typhoeus	k1gMnSc1	typhoeus
<g/>
)	)	kIx)	)
a	a	k8xC	a
hrobaříka	hrobařík	k1gMnSc2	hrobařík
Nicrophorus	Nicrophorus	k1gMnSc1	Nicrophorus
germanicus	germanicus	k1gMnSc1	germanicus
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgInPc2d1	další
zajímavých	zajímavý	k2eAgInPc2d1	zajímavý
nebo	nebo	k8xC	nebo
vzácných	vzácný	k2eAgInPc2d1	vzácný
druhů	druh	k1gInPc2	druh
bezobratlých	bezobratlí	k1gMnPc2	bezobratlí
lze	lze	k6eAd1	lze
zmínit	zmínit	k5eAaPmF	zmínit
stonožku	stonožka	k1gFnSc4	stonožka
Brachyschendyla	Brachyschendyla	k1gMnSc4	Brachyschendyla
montana	montan	k1gMnSc4	montan
<g/>
,	,	kIx,	,
měkkýše	měkkýš	k1gMnPc4	měkkýš
Arion	Arion	k1gMnSc1	Arion
intermedius	intermedius	k1gMnSc1	intermedius
a	a	k8xC	a
závornatku	závornatka	k1gFnSc4	závornatka
černavou	černavý	k2eAgFnSc4d1	černavá
(	(	kIx(	(
<g/>
Clausilia	Clausilius	k1gMnSc2	Clausilius
bidentata	bidentat	k1gMnSc2	bidentat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žížaly	žížala	k1gFnPc1	žížala
Aporectodea	Aporectodeum	k1gNnSc2	Aporectodeum
handlirschi	handlirschi	k1gNnSc2	handlirschi
<g/>
,	,	kIx,	,
Helodrilus	Helodrilus	k1gMnSc1	Helodrilus
oculatus	oculatus	k1gMnSc1	oculatus
a	a	k8xC	a
Dendrobaena	Dendrobaena	k1gFnSc1	Dendrobaena
attemsi	attemse	k1gFnSc3	attemse
attemsi	attemse	k1gFnSc4	attemse
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
korýše	korýš	k1gMnSc2	korýš
listonoha	listonoh	k1gMnSc2	listonoh
letního	letní	k2eAgMnSc2d1	letní
(	(	kIx(	(
<g/>
Triops	Triops	k1gInSc1	Triops
cancriformis	cancriformis	k1gFnSc2	cancriformis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzácný	vzácný	k2eAgInSc1d1	vzácný
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
výskyt	výskyt	k1gInSc1	výskyt
cikády	cikáda	k1gFnSc2	cikáda
chlumní	chlumní	k2eAgFnSc1d1	chlumní
(	(	kIx(	(
<g/>
Cicadetta	Cicadetta	k1gFnSc1	Cicadetta
montana	montana	k1gFnSc1	montana
<g/>
)	)	kIx)	)
a	a	k8xC	a
mravence	mravenec	k1gMnSc2	mravenec
Manica	Manicus	k1gMnSc2	Manicus
rubida	rubid	k1gMnSc2	rubid
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
lokalit	lokalita	k1gFnPc2	lokalita
koníka	koník	k1gMnSc2	koník
jeskynního	jeskynní	k2eAgMnSc2d1	jeskynní
na	na	k7c6	na
území	území	k1gNnSc6	území
Česka	Česko	k1gNnSc2	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
také	také	k9	také
vzácná	vzácný	k2eAgFnSc1d1	vzácná
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
území	území	k1gNnSc4	území
Česka	Česko	k1gNnSc2	Česko
endemická	endemický	k2eAgFnSc1d1	endemická
<g/>
,	,	kIx,	,
kobylka	kobylka	k1gFnSc1	kobylka
Pholidoptera	Pholidopter	k1gMnSc2	Pholidopter
aptera	apter	k1gMnSc2	apter
bohemica	bohemicus	k1gMnSc2	bohemicus
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
ochranu	ochrana	k1gFnSc4	ochrana
území	území	k1gNnSc2	území
Českého	český	k2eAgNnSc2d1	české
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
,	,	kIx,	,
potažmo	potažmo	k6eAd1	potažmo
celých	celý	k2eAgInPc2d1	celý
Labských	labský	k2eAgInPc2d1	labský
pískovců	pískovec	k1gInPc2	pískovec
<g/>
,	,	kIx,	,
sahají	sahat	k5eAaImIp3nP	sahat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1739	[number]	k4	1739
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
hrabě	hrabě	k1gMnSc1	hrabě
Jan	Jan	k1gMnSc1	Jan
Josef	Josef	k1gMnSc1	Josef
Thun	Thun	k1gMnSc1	Thun
zakázal	zakázat	k5eAaPmAgMnS	zakázat
na	na	k7c6	na
území	území	k1gNnSc6	území
Děčínského	děčínský	k2eAgNnSc2d1	děčínské
panství	panství	k1gNnSc2	panství
pastvu	pastva	k1gFnSc4	pastva
dobytka	dobytek	k1gInSc2	dobytek
a	a	k8xC	a
v	v	k7c6	v
určitém	určitý	k2eAgNnSc6d1	určité
období	období	k1gNnSc6	období
v	v	k7c6	v
roce	rok	k1gInSc6	rok
i	i	k9	i
těžbu	těžba	k1gFnSc4	těžba
dříví	dříví	k1gNnSc2	dříví
<g/>
.	.	kIx.	.
</s>
<s>
Hrabě	Hrabě	k1gMnSc1	Hrabě
tak	tak	k6eAd1	tak
chtěl	chtít	k5eAaImAgMnS	chtít
místní	místní	k2eAgMnSc1d1	místní
zvěři	zvěř	k1gFnSc3	zvěř
ponechat	ponechat	k5eAaPmF	ponechat
potřebný	potřebný	k2eAgInSc4d1	potřebný
klid	klid	k1gInSc4	klid
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
a	a	k8xC	a
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
<g/>
.	.	kIx.	.
</s>
<s>
Podobného	podobný	k2eAgInSc2d1	podobný
účelu	účel	k1gInSc2	účel
byla	být	k5eAaImAgFnS	být
i	i	k9	i
nařízení	nařízení	k1gNnSc4	nařízení
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
podle	podle	k7c2	podle
nařízení	nařízení	k1gNnSc2	nařízení
Kinských	Kinská	k1gFnPc2	Kinská
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1863	[number]	k4	1863
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
lesních	lesní	k2eAgInPc6d1	lesní
revírech	revír	k1gInPc6	revír
dnešního	dnešní	k2eAgInSc2d1	dnešní
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
zakázána	zakázat	k5eAaPmNgFnS	zakázat
seč	seč	k6eAd1	seč
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vyčleněna	vyčleněn	k2eAgNnPc4d1	vyčleněno
klidová	klidový	k2eAgNnPc4d1	klidové
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
ponechána	ponechat	k5eAaPmNgFnS	ponechat
samostatnému	samostatný	k2eAgInSc3d1	samostatný
vývoji	vývoj	k1gInSc3	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
tato	tento	k3xDgNnPc4	tento
opatření	opatření	k1gNnSc4	opatření
byly	být	k5eAaImAgFnP	být
populace	populace	k1gFnPc1	populace
tetřeva	tetřev	k1gMnSc2	tetřev
hlušce	hlušec	k1gMnSc2	hlušec
a	a	k8xC	a
tetřívka	tetřívek	k1gMnSc2	tetřívek
obecného	obecný	k2eAgMnSc2d1	obecný
<g/>
,	,	kIx,	,
o	o	k7c4	o
které	který	k3yRgMnPc4	který
hraběcí	hraběcí	k2eAgInSc1d1	hraběcí
rod	rod	k1gInSc1	rod
projevoval	projevovat	k5eAaImAgInS	projevovat
zájem	zájem	k1gInSc4	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Podobných	podobný	k2eAgInPc2d1	podobný
počinů	počin	k1gInPc2	počin
se	se	k3xPyFc4	se
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
ochrany	ochrana	k1gFnSc2	ochrana
Labských	labský	k2eAgInPc2d1	labský
pískovců	pískovec	k1gInPc2	pískovec
vyskytla	vyskytnout	k5eAaPmAgFnS	vyskytnout
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
napomohlo	napomoct	k5eAaPmAgNnS	napomoct
k	k	k7c3	k
zachování	zachování	k1gNnSc3	zachování
druhové	druhový	k2eAgFnSc2d1	druhová
rozmanitosti	rozmanitost	k1gFnSc2	rozmanitost
i	i	k8xC	i
vysoké	vysoký	k2eAgFnSc2d1	vysoká
hodnoty	hodnota	k1gFnSc2	hodnota
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
turismu	turismus	k1gInSc2	turismus
a	a	k8xC	a
s	s	k7c7	s
přílivem	příliv	k1gInSc7	příliv
návštěvníků	návštěvník	k1gMnPc2	návštěvník
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
začalo	začít	k5eAaPmAgNnS	začít
uvažovat	uvažovat	k5eAaImF	uvažovat
o	o	k7c6	o
řízené	řízený	k2eAgFnSc6d1	řízená
ochraně	ochrana	k1gFnSc6	ochrana
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
reálné	reálný	k2eAgInPc1d1	reálný
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
řízenou	řízený	k2eAgFnSc4d1	řízená
ochranu	ochrana	k1gFnSc4	ochrana
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
pera	pero	k1gNnSc2	pero
českého	český	k2eAgMnSc2d1	český
průkopníka	průkopník	k1gMnSc2	průkopník
moderní	moderní	k2eAgFnSc2d1	moderní
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Maximoviče	Maximovič	k1gMnSc2	Maximovič
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
České	český	k2eAgNnSc1d1	české
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
jest	být	k5eAaImIp3nS	být
vhodným	vhodný	k2eAgInSc7d1	vhodný
objektem	objekt	k1gInSc7	objekt
turistického	turistický	k2eAgInSc2d1	turistický
ruchu	ruch	k1gInSc2	ruch
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
opravdu	opravdu	k6eAd1	opravdu
o	o	k7c4	o
významné	významný	k2eAgInPc4d1	významný
objekty	objekt	k1gInPc4	objekt
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
má	mít	k5eAaImIp3nS	mít
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
detailu	detail	k1gInSc6	detail
[	[	kIx(	[
<g/>
...	...	k?	...
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jako	jako	k9	jako
krajinný	krajinný	k2eAgInSc1d1	krajinný
celek	celek	k1gInSc1	celek
nesporný	sporný	k2eNgInSc1d1	nesporný
charakter	charakter	k1gInSc4	charakter
přírodní	přírodní	k2eAgFnSc2d1	přírodní
památky	památka	k1gFnSc2	památka
vhodné	vhodný	k2eAgFnSc2d1	vhodná
ke	k	k7c3	k
zřízení	zřízení	k1gNnSc3	zřízení
parciální	parciální	k2eAgFnSc2d1	parciální
rezervace	rezervace	k1gFnSc2	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
má	mít	k5eAaImIp3nS	mít
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zájem	zájem	k1gInSc1	zájem
na	na	k7c6	na
udržení	udržení	k1gNnSc6	udržení
území	území	k1gNnSc2	území
v	v	k7c6	v
dochovaném	dochovaný	k2eAgInSc6d1	dochovaný
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
oddálení	oddálení	k1gNnSc4	oddálení
všech	všecek	k3xTgFnPc2	všecek
nebezpečí	nebezpeč	k1gFnPc2wB	nebezpeč
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
mu	on	k3xPp3gMnSc3	on
hrozila	hrozit	k5eAaImAgFnS	hrozit
a	a	k8xC	a
na	na	k7c6	na
vědeckém	vědecký	k2eAgNnSc6d1	vědecké
probádání	probádání	k1gNnSc6	probádání
a	a	k8xC	a
využití	využití	k1gNnSc6	využití
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
první	první	k4xOgFnSc1	první
řízená	řízený	k2eAgFnSc1d1	řízená
ochranná	ochranný	k2eAgFnSc1d1	ochranná
území	území	k1gNnSc6	území
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
až	až	k9	až
o	o	k7c4	o
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
:	:	kIx,	:
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
rezervace	rezervace	k1gFnSc2	rezervace
Edmundova	Edmundův	k2eAgFnSc1d1	Edmundova
soutěska	soutěska	k1gFnSc1	soutěska
<g/>
,	,	kIx,	,
Pravčická	Pravčický	k2eAgFnSc1d1	Pravčická
brána	brána	k1gFnSc1	brána
a	a	k8xC	a
Tiské	Tiský	k2eAgFnPc1d1	Tiský
stěny	stěna	k1gFnPc1	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Vznikala	vznikat	k5eAaImAgFnS	vznikat
však	však	k9	však
potřeba	potřeba	k1gFnSc1	potřeba
chránit	chránit	k5eAaImF	chránit
oblast	oblast	k1gFnSc4	oblast
jako	jako	k8xS	jako
celek	celek	k1gInSc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
návrh	návrh	k1gInSc1	návrh
na	na	k7c6	na
zřízení	zřízení	k1gNnSc6	zřízení
chráněné	chráněný	k2eAgFnSc2d1	chráněná
krajinné	krajinný	k2eAgFnSc2d1	krajinná
oblasti	oblast	k1gFnSc2	oblast
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Labe	Labe	k1gNnSc2	Labe
mezi	mezi	k7c7	mezi
Litoměřicemi	Litoměřice	k1gInPc7	Litoměřice
a	a	k8xC	a
Hřenskem	Hřensko	k1gNnSc7	Hřensko
podal	podat	k5eAaPmAgMnS	podat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
vládní	vládní	k2eAgMnSc1d1	vládní
prezident	prezident	k1gMnSc1	prezident
v	v	k7c6	v
Ústí	ústí	k1gNnSc6	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
z	z	k7c2	z
Okresního	okresní	k2eAgInSc2d1	okresní
archivu	archiv	k1gInSc2	archiv
v	v	k7c6	v
Děčíně	Děčín	k1gInSc6	Děčín
se	se	k3xPyFc4	se
o	o	k7c6	o
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
již	již	k9	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
<s>
Ochranářským	ochranářský	k2eAgFnPc3d1	ochranářská
snahám	snaha	k1gFnPc3	snaha
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
slyšení	slyšení	k1gNnSc1	slyšení
až	až	k9	až
v	v	k7c4	v
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
výnosem	výnos	k1gInSc7	výnos
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
kultury	kultura	k1gFnSc2	kultura
Československé	československý	k2eAgFnSc2d1	Československá
socialistické	socialistický	k2eAgFnSc2d1	socialistická
republiky	republika	k1gFnSc2	republika
č.	č.	k?	č.
4946	[number]	k4	4946
<g/>
/	/	kIx~	/
<g/>
72	[number]	k4	72
–	–	k?	–
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
zřízena	zřízen	k2eAgFnSc1d1	zřízena
Chráněná	chráněný	k2eAgFnSc1d1	chráněná
krajinná	krajinný	k2eAgFnSc1d1	krajinná
oblast	oblast	k1gFnSc1	oblast
Labské	labský	k2eAgInPc1d1	labský
pískovce	pískovec	k1gInPc1	pískovec
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
provedena	proveden	k2eAgFnSc1d1	provedena
zonace	zonace	k1gFnSc1	zonace
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
oblast	oblast	k1gFnSc1	oblast
se	se	k3xPyFc4	se
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
zóny	zóna	k1gFnPc4	zóna
odstupňované	odstupňovaný	k2eAgFnPc4d1	odstupňovaná
podle	podle	k7c2	podle
míry	míra	k1gFnSc2	míra
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Takováto	takovýto	k3xDgFnSc1	takovýto
zonace	zonace	k1gFnSc1	zonace
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
ještě	ještě	k6eAd1	ještě
upravena	upravit	k5eAaPmNgFnS	upravit
<g/>
.	.	kIx.	.
</s>
<s>
Ochranáři	ochranář	k1gMnPc1	ochranář
však	však	k9	však
požadovali	požadovat	k5eAaImAgMnP	požadovat
pro	pro	k7c4	pro
nejcennější	cenný	k2eAgFnSc4d3	nejcennější
část	část	k1gFnSc4	část
chráněné	chráněný	k2eAgFnSc2d1	chráněná
krajinné	krajinný	k2eAgFnSc2d1	krajinná
oblasti	oblast	k1gFnSc2	oblast
ještě	ještě	k9	ještě
vyšší	vysoký	k2eAgInSc4d2	vyšší
stupeň	stupeň	k1gInSc4	stupeň
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dlouholetém	dlouholetý	k2eAgNnSc6d1	dlouholeté
úsilí	úsilí	k1gNnSc6	úsilí
a	a	k8xC	a
i	i	k9	i
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
ochranářů	ochranář	k1gMnPc2	ochranář
z	z	k7c2	z
německé	německý	k2eAgFnSc2d1	německá
strany	strana	k1gFnSc2	strana
byl	být	k5eAaImAgInS	být
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc6	leden
2000	[number]	k4	2000
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
161	[number]	k4	161
<g/>
/	/	kIx~	/
<g/>
1999	[number]	k4	1999
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
vyhlášen	vyhlášen	k2eAgInSc4d1	vyhlášen
Národní	národní	k2eAgInSc4d1	národní
park	park	k1gInSc4	park
České	český	k2eAgNnSc1d1	české
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
zpracovávat	zpracovávat	k5eAaImF	zpracovávat
podklady	podklad	k1gInPc1	podklad
pro	pro	k7c4	pro
zřízení	zřízení	k1gNnSc4	zřízení
biosférické	biosférický	k2eAgFnSc2d1	biosférická
rezervace	rezervace	k1gFnSc2	rezervace
UNESCO	UNESCO	kA	UNESCO
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
kromě	kromě	k7c2	kromě
Českého	český	k2eAgNnSc2d1	české
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
zahrnovat	zahrnovat	k5eAaImF	zahrnovat
i	i	k9	i
Chráněnou	chráněný	k2eAgFnSc4d1	chráněná
krajinnou	krajinný	k2eAgFnSc4d1	krajinná
oblast	oblast	k1gFnSc4	oblast
Labské	labský	k2eAgInPc1d1	labský
pískovce	pískovec	k1gInPc1	pískovec
<g/>
.	.	kIx.	.
</s>
<s>
Nejcennější	cenný	k2eAgFnPc1d3	nejcennější
oblasti	oblast	k1gFnPc1	oblast
jsou	být	k5eAaImIp3nP	být
dále	daleko	k6eAd2	daleko
připravovány	připravován	k2eAgInPc1d1	připravován
pro	pro	k7c4	pro
začlenění	začlenění	k1gNnSc4	začlenění
do	do	k7c2	do
soustavy	soustava	k1gFnSc2	soustava
Natura	Natura	k1gFnSc1	Natura
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Park	park	k1gInSc1	park
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Evropsky	evropsky	k6eAd1	evropsky
významné	významný	k2eAgFnSc2d1	významná
lokality	lokalita	k1gFnSc2	lokalita
České	český	k2eAgNnSc1d1	české
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
a	a	k8xC	a
Ptačí	ptačí	k2eAgFnSc6d1	ptačí
oblasti	oblast	k1gFnSc6	oblast
Labské	labský	k2eAgInPc1d1	labský
pískovce	pískovec	k1gInPc1	pískovec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
tři	tři	k4xCgNnPc4	tři
maloplošná	maloplošný	k2eAgNnPc4d1	maloplošné
chráněná	chráněný	k2eAgNnPc4d1	chráněné
území	území	k1gNnPc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
přírodní	přírodní	k2eAgFnSc4d1	přírodní
památku	památka	k1gFnSc4	památka
Nad	nad	k7c7	nad
Dolským	dolský	k2eAgInSc7d1	dolský
mlýnem	mlýn	k1gInSc7	mlýn
<g/>
,	,	kIx,	,
národní	národní	k2eAgFnSc4d1	národní
přírodní	přírodní	k2eAgFnSc4d1	přírodní
rezervaci	rezervace	k1gFnSc4	rezervace
Růžák	růžák	k1gInSc4	růžák
a	a	k8xC	a
národní	národní	k2eAgFnSc4d1	národní
přírodní	přírodní	k2eAgFnSc4d1	přírodní
památku	památka	k1gFnSc4	památka
Pravčická	Pravčický	k2eAgFnSc1d1	Pravčická
brána	brána	k1gFnSc1	brána
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
parku	park	k1gInSc2	park
nacházely	nacházet	k5eAaImAgFnP	nacházet
i	i	k9	i
dvě	dva	k4xCgFnPc1	dva
přírodní	přírodní	k2eAgFnPc1d1	přírodní
rezervace	rezervace	k1gFnPc1	rezervace
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
Ponova	Ponův	k2eAgFnSc1d1	Ponův
louka	louka	k1gFnSc1	louka
a	a	k8xC	a
Babylon	Babylon	k1gInSc1	Babylon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
však	však	k9	však
byly	být	k5eAaImAgInP	být
zrušeny	zrušit	k5eAaPmNgInP	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
park	park	k1gInSc4	park
se	se	k3xPyFc4	se
stará	starý	k2eAgFnSc1d1	stará
Správa	správa	k1gFnSc1	správa
Národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
České	český	k2eAgNnSc1d1	české
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Krásné	krásný	k2eAgFnSc6d1	krásná
Lípě	lípa	k1gFnSc6	lípa
<g/>
.	.	kIx.	.
</s>
<s>
Dvakrát	dvakrát	k6eAd1	dvakrát
ročně	ročně	k6eAd1	ročně
vydává	vydávat	k5eAaPmIp3nS	vydávat
"	"	kIx"	"
<g/>
Zpravodaj	zpravodaj	k1gInSc1	zpravodaj
Správy	správa	k1gFnSc2	správa
Národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
České	český	k2eAgNnSc1d1	české
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
zájmu	zájem	k1gInSc2	zájem
turistů	turist	k1gMnPc2	turist
o	o	k7c4	o
krajinu	krajina	k1gFnSc4	krajina
Českého	český	k2eAgNnSc2d1	české
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
sahají	sahat	k5eAaImIp3nP	sahat
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	s	k7c7	s
krajinou	krajina	k1gFnSc7	krajina
toulali	toulat	k5eAaImAgMnP	toulat
jedni	jeden	k4xCgMnPc1	jeden
z	z	k7c2	z
jejích	její	k3xOp3gMnPc2	její
první	první	k4xOgMnPc1	první
propagátorů	propagátor	k1gMnPc2	propagátor
<g/>
,	,	kIx,	,
švýcarští	švýcarský	k2eAgMnPc1d1	švýcarský
malíři	malíř	k1gMnPc1	malíř
Adrian	Adrian	k1gMnSc1	Adrian
Zingg	Zingg	k1gMnSc1	Zingg
a	a	k8xC	a
Anton	Anton	k1gMnSc1	Anton
Graff	Graff	k1gMnSc1	Graff
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Děčín	Děčín	k1gInSc1	Děčín
–	–	k?	–
Drážďany	Drážďany	k1gInPc1	Drážďany
zahájen	zahájen	k2eAgInSc1d1	zahájen
provoz	provoz	k1gInSc4	provoz
lodní	lodní	k2eAgFnSc2d1	lodní
dopravy	doprava	k1gFnSc2	doprava
včetně	včetně	k7c2	včetně
výletních	výletní	k2eAgInPc2d1	výletní
parníků	parník	k1gInPc2	parník
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1851	[number]	k4	1851
vyjel	vyjet	k5eAaPmAgInS	vyjet
po	po	k7c6	po
nové	nový	k2eAgFnSc6d1	nová
železniční	železniční	k2eAgFnSc6d1	železniční
trase	trasa	k1gFnSc6	trasa
Drážďany	Drážďany	k1gInPc1	Drážďany
<g/>
–	–	k?	–
<g/>
Podmokly	podmoknout	k5eAaPmAgInP	podmoknout
první	první	k4xOgInSc4	první
vlak	vlak	k1gInSc4	vlak
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rozmach	rozmach	k1gInSc4	rozmach
turistiky	turistika	k1gFnSc2	turistika
v	v	k7c6	v
Českém	český	k2eAgNnSc6d1	české
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
se	se	k3xPyFc4	se
postaral	postarat	k5eAaPmAgInS	postarat
hlavně	hlavně	k9	hlavně
rod	rod	k1gInSc1	rod
Kinských	Kinský	k1gMnPc2	Kinský
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
českokamenické	českokamenický	k2eAgNnSc4d1	českokamenické
panství	panství	k1gNnSc4	panství
<g/>
,	,	kIx,	,
a	a	k8xC	a
rod	rod	k1gInSc1	rod
Clary-Aldringenů	Clary-Aldringen	k1gInPc2	Clary-Aldringen
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnPc3	který
patřilo	patřit	k5eAaImAgNnS	patřit
panství	panství	k1gNnSc1	panství
děčínské	děčínský	k2eAgNnSc1d1	děčínské
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
členech	člen	k1gInPc6	člen
těchto	tento	k3xDgMnPc2	tento
rodů	rod	k1gInPc2	rod
je	být	k5eAaImIp3nS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
řada	řada	k1gFnSc1	řada
skalních	skalní	k2eAgInPc2d1	skalní
útvarů	útvar	k1gInPc2	útvar
a	a	k8xC	a
míst	místo	k1gNnPc2	místo
jako	jako	k8xC	jako
např.	např.	kA	např.
Rudolfův	Rudolfův	k2eAgInSc1d1	Rudolfův
kámen	kámen	k1gInSc1	kámen
nebo	nebo	k8xC	nebo
Edmundova	Edmundův	k2eAgFnSc1d1	Edmundova
soutěska	soutěska	k1gFnSc1	soutěska
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
rod	rod	k1gInSc1	rod
Clary-Aldringenů	Clary-Aldringen	k1gInPc2	Clary-Aldringen
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zpřístupnil	zpřístupnit	k5eAaPmAgInS	zpřístupnit
Pravčickou	Pravčický	k2eAgFnSc4d1	Pravčická
bránu	brána	k1gFnSc4	brána
turistům	turist	k1gMnPc3	turist
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1878	[number]	k4	1878
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
Děčíně	Děčín	k1gInSc6	Děčín
první	první	k4xOgInSc1	první
spolek	spolek	k1gInSc1	spolek
podporující	podporující	k2eAgInSc1d1	podporující
turistický	turistický	k2eAgInSc1d1	turistický
ruch	ruch	k1gInSc1	ruch
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
Horský	horský	k2eAgInSc1d1	horský
spolek	spolek	k1gInSc1	spolek
pro	pro	k7c4	pro
České	český	k2eAgNnSc4d1	české
Švýcarsko	Švýcarsko	k1gNnSc4	Švýcarsko
(	(	kIx(	(
<g/>
Gebirgsverein	Gebirgsverein	k1gInSc1	Gebirgsverein
für	für	k?	für
die	die	k?	die
Böhmische	Böhmische	k1gInSc1	Böhmische
Schweiz	Schweiz	k1gInSc1	Schweiz
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
byl	být	k5eAaImAgInS	být
ještě	ještě	k9	ještě
v	v	k7c6	v
Krásné	krásný	k2eAgFnSc6d1	krásná
Lípě	lípa	k1gFnSc6	lípa
založen	založit	k5eAaPmNgInS	založit
Horský	horský	k2eAgInSc1d1	horský
spolek	spolek	k1gInSc1	spolek
pro	pro	k7c4	pro
nejsevernější	severní	k2eAgFnPc4d3	nejsevernější
Čechy	Čechy	k1gFnPc4	Čechy
(	(	kIx(	(
<g/>
Gebirsverein	Gebirsverein	k1gInSc1	Gebirsverein
für	für	k?	für
das	das	k?	das
nördlichste	nördlichsit	k5eAaImRp2nP	nördlichsit
Böhmen	Böhmen	k1gInSc1	Böhmen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
České	český	k2eAgNnSc1d1	české
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
zařadilo	zařadit	k5eAaPmAgNnS	zařadit
mezi	mezi	k7c4	mezi
European	European	k1gInSc4	European
Destinations	Destinations	k1gInSc1	Destinations
of	of	k?	of
Excellence	Excellence	k1gFnSc1	Excellence
(	(	kIx(	(
<g/>
Excelentní	excelentní	k2eAgFnSc1d1	excelentní
turistická	turistický	k2eAgFnSc1d1	turistická
destinace	destinace	k1gFnSc1	destinace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
park	park	k1gInSc1	park
zpřístupněn	zpřístupnit	k5eAaPmNgInS	zpřístupnit
sítí	síť	k1gFnSc7	síť
turistických	turistický	k2eAgFnPc2d1	turistická
tras	trasa	k1gFnPc2	trasa
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
přírodní	přírodní	k2eAgInPc1d1	přírodní
úkazy	úkaz	k1gInPc1	úkaz
jako	jako	k8xC	jako
Pravčická	Pravčický	k2eAgFnSc1d1	Pravčická
brána	brána	k1gFnSc1	brána
<g/>
,	,	kIx,	,
Edmundova	Edmundův	k2eAgFnSc1d1	Edmundova
soutěska	soutěska	k1gFnSc1	soutěska
či	či	k8xC	či
Tiské	Tiský	k2eAgFnPc1d1	Tiský
stěny	stěna	k1gFnPc1	stěna
jsou	být	k5eAaImIp3nP	být
zpoplatněny	zpoplatněn	k2eAgFnPc1d1	zpoplatněna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Českém	český	k2eAgNnSc6d1	české
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
má	mít	k5eAaImIp3nS	mít
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
tradicí	tradice	k1gFnSc7	tradice
horolezectví	horolezectví	k1gNnSc2	horolezectví
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
výstupy	výstup	k1gInPc1	výstup
na	na	k7c4	na
skalní	skalní	k2eAgFnPc4d1	skalní
věže	věž	k1gFnPc4	věž
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgInP	být
prováděny	provádět	k5eAaImNgInP	provádět
již	již	k6eAd1	již
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
k	k	k7c3	k
průkopníkům	průkopník	k1gMnPc3	průkopník
skalního	skalní	k2eAgNnSc2d1	skalní
lezení	lezení	k1gNnSc2	lezení
patřil	patřit	k5eAaImAgMnS	patřit
Oliver	Oliver	k1gMnSc1	Oliver
Perry	Perra	k1gFnSc2	Perra
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
z	z	k7c2	z
Čechů	Čech	k1gMnPc2	Čech
např.	např.	kA	např.
Bernd	Bernd	k1gMnSc1	Bernd
Arnold	Arnold	k1gMnSc1	Arnold
a	a	k8xC	a
Jindřich	Jindřich	k1gMnSc1	Jindřich
Hudeček	Hudeček	k1gMnSc1	Hudeček
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
horolezectví	horolezectví	k1gNnSc1	horolezectví
v	v	k7c6	v
parku	park	k1gInSc6	park
povoleno	povolit	k5eAaPmNgNnS	povolit
za	za	k7c2	za
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
Návštěvní	návštěvní	k2eAgInSc4d1	návštěvní
řád	řád	k1gInSc4	řád
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
České	český	k2eAgNnSc1d1	české
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Horolezecká	horolezecký	k2eAgFnSc1d1	horolezecká
činnost	činnost	k1gFnSc1	činnost
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
provozovat	provozovat	k5eAaImF	provozovat
jen	jen	k9	jen
v	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
lézt	lézt	k5eAaImF	lézt
zde	zde	k6eAd1	zde
mohou	moct	k5eAaImIp3nP	moct
pouze	pouze	k6eAd1	pouze
členové	člen	k1gMnPc1	člen
UIAA	UIAA	kA	UIAA
a	a	k8xC	a
přidružených	přidružený	k2eAgFnPc2d1	přidružená
organizací	organizace	k1gFnPc2	organizace
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
i	i	k9	i
členové	člen	k1gMnPc1	člen
Českého	český	k2eAgInSc2d1	český
horolezeckého	horolezecký	k2eAgInSc2d1	horolezecký
svazu	svaz	k1gInSc2	svaz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
v	v	k7c6	v
parku	park	k1gInSc6	park
nacházelo	nacházet	k5eAaImAgNnS	nacházet
několik	několik	k4yIc1	několik
trampských	trampský	k2eAgInPc2d1	trampský
kempů	kemp	k1gInPc2	kemp
<g/>
,	,	kIx,	,
se	s	k7c7	s
zřízením	zřízení	k1gNnSc7	zřízení
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
již	již	k6eAd1	již
zmíněných	zmíněný	k2eAgInPc2d1	zmíněný
hrádků	hrádek	k1gInPc2	hrádek
a	a	k8xC	a
obydlených	obydlený	k2eAgFnPc2d1	obydlená
staveb	stavba	k1gFnPc2	stavba
se	se	k3xPyFc4	se
v	v	k7c6	v
rezervaci	rezervace	k1gFnSc6	rezervace
nalézá	nalézat	k5eAaImIp3nS	nalézat
i	i	k9	i
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgFnPc2d1	další
upomínek	upomínka	k1gFnPc2	upomínka
na	na	k7c4	na
působení	působení	k1gNnSc4	působení
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
nejvýznamnější	významný	k2eAgMnPc1d3	nejvýznamnější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
Dolský	dolský	k2eAgInSc1d1	dolský
mlýn	mlýn	k1gInSc4	mlýn
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yIgInSc6	který
prameny	pramen	k1gInPc4	pramen
hovoří	hovořit	k5eAaImIp3nS	hovořit
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1515	[number]	k4	1515
<g/>
.	.	kIx.	.
</s>
<s>
Mlýn	mlýn	k1gInSc4	mlýn
vlastnilo	vlastnit	k5eAaImAgNnS	vlastnit
mnoho	mnoho	k4c1	mnoho
různých	různý	k2eAgMnPc2d1	různý
majitelů	majitel	k1gMnPc2	majitel
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
měnila	měnit	k5eAaImAgFnS	měnit
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
funkce	funkce	k1gFnSc1	funkce
–	–	k?	–
jednu	jeden	k4xCgFnSc4	jeden
dobu	doba	k1gFnSc4	doba
dokonce	dokonce	k9	dokonce
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k8xS	jako
palírna	palírna	k1gFnSc1	palírna
a	a	k8xC	a
výletní	výletní	k2eAgInSc1d1	výletní
hostinec	hostinec	k1gInSc1	hostinec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
zachování	zachování	k1gNnSc4	zachování
mlýna	mlýn	k1gInSc2	mlýn
Občanské	občanský	k2eAgNnSc4d1	občanské
sdružení	sdružení	k1gNnSc4	sdružení
pro	pro	k7c4	pro
záchranu	záchrana	k1gFnSc4	záchrana
a	a	k8xC	a
konzervaci	konzervace	k1gFnSc4	konzervace
kulturní	kulturní	k2eAgFnSc2d1	kulturní
památky	památka	k1gFnSc2	památka
Dolský	dolský	k2eAgInSc1d1	dolský
mlýn	mlýn	k1gInSc1	mlýn
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
stavbou	stavba	k1gFnSc7	stavba
v	v	k7c6	v
parku	park	k1gInSc6	park
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
chatka	chatka	k1gFnSc1	chatka
na	na	k7c6	na
Mariině	Mariin	k2eAgFnSc6d1	Mariina
skále	skála	k1gFnSc6	skála
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
části	část	k1gFnSc6	část
parku	park	k1gInSc2	park
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
nechal	nechat	k5eAaPmAgInS	nechat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1856	[number]	k4	1856
postavit	postavit	k5eAaPmF	postavit
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Bonaventura	Bonaventura	k1gFnSc1	Bonaventura
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Kinských	Kinská	k1gFnPc2	Kinská
<g/>
.	.	kIx.	.
</s>
<s>
Chatka	chatka	k1gFnSc1	chatka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
vyhořela	vyhořet	k5eAaPmAgFnS	vyhořet
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
však	však	k9	však
byla	být	k5eAaImAgFnS	být
obnovena	obnoven	k2eAgFnSc1d1	obnovena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgFnPc2d1	další
památek	památka	k1gFnPc2	památka
lidové	lidový	k2eAgFnSc2d1	lidová
a	a	k8xC	a
sakrální	sakrální	k2eAgFnSc2d1	sakrální
architektury	architektura	k1gFnSc2	architektura
<g/>
,	,	kIx,	,
ty	ten	k3xDgInPc1	ten
větší	veliký	k2eAgInPc1d2	veliký
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgInP	umístit
především	především	k9	především
ve	v	k7c6	v
vesnicích	vesnice	k1gFnPc6	vesnice
mimo	mimo	k7c4	mimo
území	území	k1gNnSc4	území
parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
