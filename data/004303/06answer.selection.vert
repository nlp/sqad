<s>
Byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
hlavním	hlavní	k2eAgInSc7d1	hlavní
předmětem	předmět	k1gInSc7	předmět
ochrany	ochrana	k1gFnSc2	ochrana
jsou	být	k5eAaImIp3nP	být
unikátní	unikátní	k2eAgInPc4d1	unikátní
pískovcové	pískovcový	k2eAgInPc4d1	pískovcový
útvary	útvar	k1gInPc4	útvar
a	a	k8xC	a
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
vázaný	vázaný	k2eAgInSc1d1	vázaný
biotop	biotop	k1gInSc1	biotop
<g/>
.	.	kIx.	.
</s>
