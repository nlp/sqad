<s>
Edinburgh	Edinburgh	k1gInSc1	Edinburgh
Festival	festival	k1gInSc1	festival
je	být	k5eAaImIp3nS	být
společný	společný	k2eAgInSc1d1	společný
název	název	k1gInSc1	název
několika	několik	k4yIc2	několik
současně	současně	k6eAd1	současně
probíhajících	probíhající	k2eAgInPc2d1	probíhající
kulturních	kulturní	k2eAgInPc2d1	kulturní
a	a	k8xC	a
uměleckých	umělecký	k2eAgInPc2d1	umělecký
festivalů	festival	k1gInPc2	festival
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
konají	konat	k5eAaImIp3nP	konat
v	v	k7c6	v
Edinburghu	Edinburgh	k1gInSc6	Edinburgh
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
festivaly	festival	k1gInPc1	festival
jsou	být	k5eAaImIp3nP	být
pořádány	pořádat	k5eAaImNgInP	pořádat
několika	několik	k4yIc2	několik
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
nezávislými	závislý	k2eNgFnPc7d1	nezávislá
organizacemi	organizace	k1gFnPc7	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Edinburgh	Edinburgh	k1gInSc1	Edinburgh
Festival	festival	k1gInSc1	festival
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc7d3	veliký
světovou	světový	k2eAgFnSc7d1	světová
kulturní	kulturní	k2eAgFnSc7d1	kulturní
událostí	událost	k1gFnSc7	událost
<g/>
,	,	kIx,	,
s	s	k7c7	s
tradicí	tradice	k1gFnSc7	tradice
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Edinburgh	Edinburgh	k1gInSc1	Edinburgh
International	International	k1gMnSc1	International
Festival	festival	k1gInSc1	festival
(	(	kIx(	(
<g/>
EIF	EIF	kA	EIF
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
byla	být	k5eAaImAgFnS	být
také	také	k9	také
založena	založit	k5eAaPmNgFnS	založit
tradice	tradice	k1gFnSc1	tradice
neoficiálních	neoficiální	k2eAgNnPc2d1	neoficiální
divadelních	divadelní	k2eAgNnPc2d1	divadelní
představení	představení	k1gNnPc2	představení
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
staly	stát	k5eAaPmAgFnP	stát
oficiálním	oficiální	k2eAgInSc7d1	oficiální
Edinburgh	Edinburgh	k1gInSc4	Edinburgh
Festival	festival	k1gInSc1	festival
Fringe	Fringe	k1gFnSc1	Fringe
(	(	kIx(	(
<g/>
EFF	EFF	kA	EFF
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
kulturní	kulturní	k2eAgFnPc1d1	kulturní
a	a	k8xC	a
umělecké	umělecký	k2eAgFnPc1d1	umělecká
přehlídky	přehlídka	k1gFnPc1	přehlídka
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
přidávaly	přidávat	k5eAaImAgFnP	přidávat
<g/>
.	.	kIx.	.
</s>
<s>
Edinburgh	Edinburgh	k1gMnSc1	Edinburgh
International	International	k1gFnSc2	International
Festival	festival	k1gInSc1	festival
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
–	–	k?	–
klasické	klasický	k2eAgNnSc4d1	klasické
i	i	k8xC	i
současné	současný	k2eAgNnSc4d1	současné
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
hudba	hudba	k1gFnSc1	hudba
tanec	tanec	k1gInSc1	tanec
<g/>
,	,	kIx,	,
vizuální	vizuální	k2eAgNnSc1d1	vizuální
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
přednášky	přednáška	k1gFnPc1	přednáška
a	a	k8xC	a
semináře	seminář	k1gInPc1	seminář
Edinburgh	Edinburgha	k1gFnPc2	Edinburgha
Fringe	Fringe	k1gFnPc2	Fringe
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
–	–	k?	–
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
komedie	komedie	k1gFnSc1	komedie
<g/>
,	,	kIx,	,
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
muzikál	muzikál	k1gInSc1	muzikál
<g/>
,	,	kIx,	,
tanec	tanec	k1gInSc1	tanec
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
dětská	dětský	k2eAgNnPc1d1	dětské
představení	představení	k1gNnPc1	představení
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
největší	veliký	k2eAgInSc1d3	veliký
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
festivalů	festival	k1gInPc2	festival
v	v	k7c6	v
Edinburghu	Edinburgh	k1gInSc6	Edinburgh
<g/>
)	)	kIx)	)
Edinburgh	Edinburgh	k1gMnSc1	Edinburgh
International	International	k1gFnSc2	International
Film	film	k1gInSc1	film
Festival	festival	k1gInSc1	festival
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
–	–	k?	–
původně	původně	k6eAd1	původně
srpnový	srpnový	k2eAgInSc4d1	srpnový
filmový	filmový	k2eAgInSc4d1	filmový
festival	festival	k1gInSc4	festival
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
pořádaný	pořádaný	k2eAgInSc1d1	pořádaný
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
Edinburgh	Edinburgh	k1gMnSc1	Edinburgh
Military	Militara	k1gFnSc2	Militara
Tattoo	Tattoo	k1gMnSc1	Tattoo
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
–	–	k?	–
přehlídka	přehlídka	k1gFnSc1	přehlídka
vojenských	vojenský	k2eAgFnPc2d1	vojenská
kapel	kapela	k1gFnPc2	kapela
Edinburgh	Edinburgh	k1gMnSc1	Edinburgh
<g />
.	.	kIx.	.
</s>
<s>
Jazz	jazz	k1gInSc1	jazz
and	and	k?	and
Blues	blues	k1gInSc1	blues
Festival	festival	k1gInSc1	festival
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
–	–	k?	–
hudební	hudební	k2eAgInSc1d1	hudební
festival	festival	k1gInSc1	festival
(	(	kIx(	(
<g/>
jazz	jazz	k1gInSc1	jazz
a	a	k8xC	a
blues	blues	k1gFnSc1	blues
<g/>
)	)	kIx)	)
Edinburgh	Edinburgh	k1gMnSc1	Edinburgh
International	International	k1gFnSc2	International
Book	Book	k1gMnSc1	Book
Festival	festival	k1gInSc1	festival
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
–	–	k?	–
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
knižní	knižní	k2eAgFnSc1d1	knižní
výstava	výstava	k1gFnSc1	výstava
Edinburgh	Edinburgha	k1gFnPc2	Edinburgha
Mela	mlít	k5eAaImSgInS	mlít
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
–	–	k?	–
festival	festival	k1gInSc1	festival
městských	městský	k2eAgFnPc2d1	městská
komunit	komunita	k1gFnPc2	komunita
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
Asie	Asie	k1gFnSc2	Asie
Edinburgh	Edinburgha	k1gFnPc2	Edinburgha
International	International	k1gFnSc2	International
Internet	Internet	k1gInSc1	Internet
Festival	festival	k1gInSc1	festival
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Edinburgh	Edinburgh	k1gMnSc1	Edinburgh
People	People	k1gMnSc1	People
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Festival	festival	k1gInSc1	festival
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
s	s	k7c7	s
kořeny	kořen	k1gInPc7	kořen
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1951	[number]	k4	1951
<g/>
–	–	k?	–
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
Edinburgh	Edinburgh	k1gInSc1	Edinburgh
Interactive	Interactiv	k1gInSc5	Interactiv
Festival	festival	k1gInSc1	festival
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Edinburgh	Edinburgh	k1gInSc1	Edinburgh
Art	Art	k1gMnSc1	Art
Festival	festival	k1gInSc1	festival
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Edinburgh	Edinburgh	k1gInSc1	Edinburgh
Annuale	Annuala	k1gFnSc3	Annuala
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
–	–	k?	–
současné	současný	k2eAgNnSc4d1	současné
umění	umění	k1gNnSc4	umění
Free	Fre	k1gInSc2	Fre
Edinburgh	Edinburgh	k1gInSc4	Edinburgh
Fringe	Fringe	k1gInSc1	Fringe
Festival	festival	k1gInSc1	festival
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
–	–	k?	–
představení	představení	k1gNnSc4	představení
zdarma	zdarma	k6eAd1	zdarma
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Edinburgh	Edinburgha	k1gFnPc2	Edinburgha
Fringe	Fringe	k1gFnPc2	Fringe
<g/>
)	)	kIx)	)
Festival	festival	k1gInSc1	festival
of	of	k?	of
Politics	Politics	k1gInSc1	Politics
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Festival	festival	k1gInSc1	festival
of	of	k?	of
Spirituality	spiritualita	k1gFnSc2	spiritualita
and	and	k?	and
Peace	Peace	k1gMnSc1	Peace
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
iFest	iFest	k1gFnSc1	iFest
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
–	–	k?	–
festival	festival	k1gInSc1	festival
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
konference	konference	k1gFnSc1	konference
o	o	k7c6	o
Internetu	Internet	k1gInSc6	Internet
Edinburgh	Edinburgha	k1gFnPc2	Edinburgha
Comedy	Comed	k1gMnPc4	Comed
Festival	festival	k1gInSc1	festival
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
West	West	k2eAgInSc1d1	West
Port	port	k1gInSc1	port
Book	Booka	k1gFnPc2	Booka
Festival	festival	k1gInSc1	festival
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
–	–	k?	–
secondhandový	secondhandový	k2eAgInSc4d1	secondhandový
knižní	knižní	k2eAgInSc4d1	knižní
festival	festival	k1gInSc4	festival
Edinburgh	Edinburgh	k1gInSc4	Edinburgh
Book	Booka	k1gFnPc2	Booka
Fringe	Fring	k1gInSc2	Fring
Islam	Islam	k1gInSc4	Islam
Festival	festival	k1gInSc1	festival
Edinburgh	Edinburgh	k1gInSc1	Edinburgh
Edinburgh	Edinburgh	k1gInSc1	Edinburgh
Swing	swing	k1gInSc1	swing
Festival	festival	k1gInSc1	festival
Edinburgh	Edinburgh	k1gInSc1	Edinburgh
Harvest	Harvest	k1gInSc1	Harvest
Festival	festival	k1gInSc1	festival
Edinburgh	Edinburgha	k1gFnPc2	Edinburgha
International	International	k1gFnSc2	International
Television	Television	k1gInSc1	Television
Festival	festival	k1gInSc1	festival
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Edinburský	Edinburský	k2eAgInSc4d1	Edinburský
festival	festival	k1gInSc4	festival
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Edinburgh	Edinburgh	k1gMnSc1	Edinburgh
Festivals	Festivalsa	k1gFnPc2	Festivalsa
Official	Official	k1gInSc1	Official
guide	guide	k6eAd1	guide
to	ten	k3xDgNnSc1	ten
all	all	k?	all
12	[number]	k4	12
festivals	festivals	k1gInSc1	festivals
that	that	k2eAgInSc4d1	that
take	take	k1gInSc4	take
place	plac	k1gInSc6	plac
in	in	k?	in
Edinburgh	Edinburgha	k1gFnPc2	Edinburgha
A	a	k9	a
history	histor	k1gInPc1	histor
of	of	k?	of
the	the	k?	the
festivals	festivals	k1gInSc1	festivals
Edinburgh	Edinburgh	k1gInSc1	Edinburgh
Festival	festival	k1gInSc1	festival
Classroom	Classroom	k1gInSc1	Classroom
resources	resourcesa	k1gFnPc2	resourcesa
</s>
