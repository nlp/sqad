<s>
Edinburgh	Edinburgh	k1gInSc1	Edinburgh
Festival	festival	k1gInSc1	festival
je	být	k5eAaImIp3nS	být
společný	společný	k2eAgInSc1d1	společný
název	název	k1gInSc1	název
několika	několik	k4yIc2	několik
současně	současně	k6eAd1	současně
probíhajících	probíhající	k2eAgInPc2d1	probíhající
kulturních	kulturní	k2eAgInPc2d1	kulturní
a	a	k8xC	a
uměleckých	umělecký	k2eAgInPc2d1	umělecký
festivalů	festival	k1gInPc2	festival
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
konají	konat	k5eAaImIp3nP	konat
v	v	k7c6	v
Edinburghu	Edinburgh	k1gInSc6	Edinburgh
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
<g/>
.	.	kIx.	.
</s>
