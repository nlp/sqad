<s>
Heike	Heike	k6eAd1	Heike
Kamerlingh	Kamerlingh	k1gMnSc1	Kamerlingh
Onnes	Onnes	k1gMnSc1	Onnes
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1853	[number]	k4	1853
<g/>
,	,	kIx,	,
Groningen	Groningen	k1gInSc1	Groningen
-	-	kIx~	-
21	[number]	k4	21
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
Leiden	Leidna	k1gFnPc2	Leidna
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
objevitel	objevitel	k1gMnSc1	objevitel
supravodivosti	supravodivost	k1gFnSc2	supravodivost
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
výzkum	výzkum	k1gInSc4	výzkum
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
nízkých	nízký	k2eAgFnPc2d1	nízká
teplot	teplota	k1gFnPc2	teplota
obdržel	obdržet	k5eAaPmAgInS	obdržet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Harm	Harm	k1gMnSc1	Harm
Kamerlingh	Kamerlingh	k1gMnSc1	Kamerlingh
Onnes	Onnes	k1gMnSc1	Onnes
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
cihelnu	cihelna	k1gFnSc4	cihelna
blízko	blízko	k7c2	blízko
Groningenu	Groningen	k1gInSc2	Groningen
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
Anna	Anna	k1gFnSc1	Anna
Gerdina	Gerdina	k1gFnSc1	Gerdina
Coers	Coers	k1gInSc4	Coers
z	z	k7c2	z
Arnhemu	Arnhem	k1gInSc2	Arnhem
byla	být	k5eAaImAgFnS	být
dcerou	dcera	k1gFnSc7	dcera
architekta	architekt	k1gMnSc2	architekt
<g/>
.	.	kIx.	.
</s>
<s>
Povinnou	povinný	k2eAgFnSc4d1	povinná
školní	školní	k2eAgFnSc4d1	školní
docházku	docházka	k1gFnSc4	docházka
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
rodném	rodný	k2eAgNnSc6d1	rodné
městě	město	k1gNnSc6	město
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
universitu	universita	k1gFnSc4	universita
v	v	k7c6	v
Groningenu	Groningen	k1gInSc6	Groningen
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
bakaláře	bakalář	k1gMnSc2	bakalář
a	a	k8xC	a
přešel	přejít	k5eAaPmAgMnS	přejít
do	do	k7c2	do
Heidelbergu	Heidelberg	k1gInSc2	Heidelberg
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
studoval	studovat	k5eAaImAgMnS	studovat
u	u	k7c2	u
Bunsena	Bunsen	k2eAgMnSc4d1	Bunsen
a	a	k8xC	a
Kirchhoffa	Kirchhoff	k1gMnSc4	Kirchhoff
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
1871	[number]	k4	1871
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
1873	[number]	k4	1873
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Groningenu	Groningen	k1gInSc2	Groningen
a	a	k8xC	a
složil	složit	k5eAaPmAgMnS	složit
zde	zde	k6eAd1	zde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1878	[number]	k4	1878
magisterské	magisterský	k2eAgFnPc4d1	magisterská
zkoušky	zkouška	k1gFnPc4	zkouška
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1879	[number]	k4	1879
získal	získat	k5eAaPmAgInS	získat
doktorský	doktorský	k2eAgInSc1d1	doktorský
titul	titul	k1gInSc1	titul
za	za	k7c4	za
práci	práce	k1gFnSc4	práce
Nové	Nové	k2eAgInPc4d1	Nové
důkazy	důkaz	k1gInPc4	důkaz
o	o	k7c6	o
rotaci	rotace	k1gFnSc6	rotace
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1878	[number]	k4	1878
byl	být	k5eAaImAgInS	být
asistentem	asistent	k1gMnSc7	asistent
na	na	k7c6	na
Polytechnice	polytechnika	k1gFnSc6	polytechnika
v	v	k7c6	v
Delftu	Delft	k1gInSc6	Delft
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracoval	pracovat	k5eAaImAgMnS	pracovat
pod	pod	k7c7	pod
Bosschou	Bosscha	k1gMnSc7	Bosscha
a	a	k8xC	a
kde	kde	k6eAd1	kde
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1881	[number]	k4	1881
a	a	k8xC	a
1882	[number]	k4	1882
přednášel	přednášet	k5eAaImAgMnS	přednášet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
profesorem	profesor	k1gMnSc7	profesor
experimentální	experimentální	k2eAgFnSc2d1	experimentální
fyziky	fyzika	k1gFnSc2	fyzika
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Leidenu	Leiden	k1gInSc6	Leiden
jako	jako	k8xC	jako
nástupce	nástupce	k1gMnSc1	nástupce
P.	P.	kA	P.
L.	L.	kA	L.
Rijkeho	Rijke	k1gMnSc2	Rijke
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
talent	talent	k1gInSc1	talent
na	na	k7c4	na
řešení	řešení	k1gNnSc4	řešení
vědeckých	vědecký	k2eAgInPc2d1	vědecký
problémů	problém	k1gInPc2	problém
byl	být	k5eAaImAgInS	být
zřejmý	zřejmý	k2eAgInSc1d1	zřejmý
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
osmnácti	osmnáct	k4xCc6	osmnáct
letech	léto	k1gNnPc6	léto
získal	získat	k5eAaPmAgMnS	získat
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
sponzorované	sponzorovaný	k2eAgNnSc1d1	sponzorované
Přírodovědeckou	přírodovědecký	k2eAgFnSc7d1	Přírodovědecká
fakultou	fakulta	k1gFnSc7	fakulta
university	universita	k1gFnSc2	universita
v	v	k7c6	v
Utrechtu	Utrecht	k1gInSc6	Utrecht
a	a	k8xC	a
příští	příští	k2eAgInSc4d1	příští
rok	rok	k1gInSc4	rok
získal	získat	k5eAaPmAgMnS	získat
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
v	v	k7c6	v
podobné	podobný	k2eAgFnSc6d1	podobná
soutěži	soutěž	k1gFnSc6	soutěž
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Groningenu	Groningen	k1gInSc6	Groningen
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pracoval	pracovat	k5eAaImAgMnS	pracovat
u	u	k7c2	u
Kirchhoffa	Kirchhoff	k1gMnSc2	Kirchhoff
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
získal	získat	k5eAaPmAgInS	získat
"	"	kIx"	"
<g/>
seminarpreis	seminarpreis	k1gFnSc4	seminarpreis
<g/>
"	"	kIx"	"
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
na	na	k7c4	na
rok	rok	k1gInSc4	rok
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
asistentem	asistent	k1gMnSc7	asistent
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
doktorské	doktorský	k2eAgFnSc6d1	doktorská
práci	práce	k1gFnSc6	práce
rozebral	rozebrat	k5eAaPmAgMnS	rozebrat
teoreticky	teoreticky	k6eAd1	teoreticky
i	i	k9	i
experimentálně	experimentálně	k6eAd1	experimentálně
známý	známý	k2eAgInSc1d1	známý
Foucaultův	Foucaultův	k2eAgInSc1d1	Foucaultův
experiment	experiment	k1gInSc1	experiment
s	s	k7c7	s
kyvadlem	kyvadlo	k1gNnSc7	kyvadlo
dokazující	dokazující	k2eAgFnSc2d1	dokazující
otáčení	otáčení	k1gNnSc2	otáčení
Země	zem	k1gFnSc2	zem
<g/>
;	;	kIx,	;
použil	použít	k5eAaPmAgInS	použít
i	i	k9	i
Cardanova	Cardanův	k2eAgInSc2d1	Cardanův
závěsu	závěs	k1gInSc2	závěs
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zabránil	zabránit	k5eAaPmAgInS	zabránit
kroucení	kroucení	k1gNnSc4	kroucení
kyvadla	kyvadlo	k1gNnSc2	kyvadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1881	[number]	k4	1881
publikoval	publikovat	k5eAaBmAgMnS	publikovat
článek	článek	k1gInSc4	článek
Allgemeene	Allgemeen	k1gInSc5	Allgemeen
theorie	theorie	k1gFnPc1	theorie
der	drát	k5eAaImRp2nS	drát
vloeistoffen	vloeistoffen	k1gInSc1	vloeistoffen
(	(	kIx(	(
<g/>
Obecná	obecný	k2eAgFnSc1d1	obecná
teorie	teorie	k1gFnSc1	teorie
kapalin	kapalina	k1gFnPc2	kapalina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tuto	tento	k3xDgFnSc4	tento
teorii	teorie	k1gFnSc4	teorie
spojil	spojit	k5eAaPmAgMnS	spojit
s	s	k7c7	s
kinetickou	kinetický	k2eAgFnSc7d1	kinetická
teorií	teorie	k1gFnSc7	teorie
kapalin	kapalina	k1gFnPc2	kapalina
využitím	využití	k1gNnSc7	využití
van	van	k1gInSc1	van
der	drát	k5eAaImRp2nS	drát
Waalsova	Waalsův	k2eAgInSc2d1	Waalsův
zákona	zákon	k1gInSc2	zákon
korespondujících	korespondující	k2eAgInPc2d1	korespondující
stavů	stav	k1gInPc2	stav
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
mechaniky	mechanika	k1gFnSc2	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
práci	práce	k1gFnSc4	práce
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
začátek	začátek	k1gInSc4	začátek
jeho	jeho	k3xOp3gNnSc2	jeho
celoživotního	celoživotní	k2eAgNnSc2d1	celoživotní
zkoumání	zkoumání	k1gNnSc2	zkoumání
vlastností	vlastnost	k1gFnPc2	vlastnost
hmoty	hmota	k1gFnSc2	hmota
za	za	k7c2	za
nízkých	nízký	k2eAgFnPc2d1	nízká
teplot	teplota	k1gFnPc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
inauguračním	inaugurační	k2eAgInSc6d1	inaugurační
projevu	projev	k1gInSc6	projev
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
Význam	význam	k1gInSc1	význam
kvantitativního	kvantitativní	k2eAgInSc2d1	kvantitativní
výzkumu	výzkum	k1gInSc2	výzkum
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
použil	použít	k5eAaPmAgInS	použít
své	svůj	k3xOyFgNnSc4	svůj
oblíbené	oblíbený	k2eAgNnSc1d1	oblíbené
motto	motto	k1gNnSc1	motto
"	"	kIx"	"
<g/>
Door	Door	k1gInSc1	Door
meten	meten	k2eAgInSc1d1	meten
tot	toto	k1gNnPc2	toto
weten	weten	k1gInSc1	weten
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
měřením	měření	k1gNnSc7	měření
k	k	k7c3	k
poznání	poznání	k1gNnSc3	poznání
<g/>
)	)	kIx)	)
a	a	k8xC	a
ocenil	ocenit	k5eAaPmAgMnS	ocenit
význam	význam	k1gInSc4	význam
experimentu	experiment	k1gInSc2	experiment
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ho	on	k3xPp3gNnSc4	on
provázel	provázet	k5eAaImAgInS	provázet
celou	celý	k2eAgFnSc4d1	celá
jeho	jeho	k3xOp3gFnSc7	jeho
vědeckou	vědecký	k2eAgFnSc7d1	vědecká
kariérou	kariéra	k1gFnSc7	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
zvolení	zvolení	k1gNnSc6	zvolení
do	do	k7c2	do
Physics	Physicsa	k1gFnPc2	Physicsa
Chair	Chaira	k1gFnPc2	Chaira
v	v	k7c6	v
Leidenu	Leiden	k1gInSc6	Leiden
reorganizoval	reorganizovat	k5eAaBmAgMnS	reorganizovat
fyzikální	fyzikální	k2eAgFnSc4d1	fyzikální
laboratoř	laboratoř	k1gFnSc4	laboratoř
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
známou	známý	k2eAgFnSc4d1	známá
jako	jako	k8xC	jako
laboratoř	laboratoř	k1gFnSc4	laboratoř
Kamerlingha	Kamerlingh	k1gMnSc2	Kamerlingh
Onnese	Onnese	k1gFnSc2	Onnese
<g/>
)	)	kIx)	)
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyhovovala	vyhovovat	k5eAaImAgFnS	vyhovovat
jeho	jeho	k3xOp3gFnSc1	jeho
výzkumu	výzkum	k1gInSc3	výzkum
založenému	založený	k2eAgInSc3d1	založený
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
teoriích	teorie	k1gFnPc6	teorie
jeho	jeho	k3xOp3gMnPc2	jeho
dvou	dva	k4xCgMnPc2	dva
krajanů	krajan	k1gMnPc2	krajan
J.	J.	kA	J.
D.	D.	kA	D.
van	vana	k1gFnPc2	vana
der	drát	k5eAaImRp2nS	drát
Waalse	Waalse	k1gFnSc1	Waalse
a	a	k8xC	a
H.	H.	kA	H.
A.	A.	kA	A.
Lorentze	Lorentze	k1gFnSc1	Lorentze
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
vytvořit	vytvořit	k5eAaPmF	vytvořit
kryogenickou	kryogenický	k2eAgFnSc4d1	Kryogenická
laboratoř	laboratoř	k1gFnSc4	laboratoř
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
ověřit	ověřit	k5eAaPmF	ověřit
van	van	k1gInSc4	van
der	drát	k5eAaImRp2nS	drát
Waalsův	Waalsův	k2eAgInSc1d1	Waalsův
zákon	zákon	k1gInSc4	zákon
korespondujících	korespondující	k2eAgInPc2d1	korespondující
stavů	stav	k1gInPc2	stav
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
rozmezí	rozmezí	k1gNnSc6	rozmezí
teplot	teplota	k1gFnPc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
dosažení	dosažení	k1gNnSc4	dosažení
co	co	k8xS	co
nejnižší	nízký	k2eAgFnSc2d3	nejnižší
teploty	teplota	k1gFnSc2	teplota
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
zkapalněním	zkapalnění	k1gNnSc7	zkapalnění
helia	helium	k1gNnSc2	helium
<g/>
.	.	kIx.	.
</s>
<s>
Ochlazením	ochlazení	k1gNnSc7	ochlazení
helia	helium	k1gNnSc2	helium
pod	pod	k7c4	pod
0,9	[number]	k4	0,9
K	K	kA	K
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
nejníže	nízce	k6eAd3	nízce
k	k	k7c3	k
absolutní	absolutní	k2eAgFnSc3d1	absolutní
nule	nula	k1gFnSc3	nula
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
říkával	říkávat	k5eAaImAgMnS	říkávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejchladnější	chladný	k2eAgNnSc1d3	nejchladnější
místo	místo	k1gNnSc1	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Leidenu	Leiden	k1gInSc6	Leiden
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
výzkum	výzkum	k1gInSc4	výzkum
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
nízkých	nízký	k2eAgFnPc2d1	nízká
teplot	teplota	k1gFnPc2	teplota
dostal	dostat	k5eAaPmAgInS	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
laboratoři	laboratoř	k1gFnSc6	laboratoř
zabýval	zabývat	k5eAaImAgMnS	zabývat
výzkumem	výzkum	k1gInSc7	výzkum
termodynamiky	termodynamika	k1gFnSc2	termodynamika
<g/>
,	,	kIx,	,
radioaktivity	radioaktivita	k1gFnSc2	radioaktivita
<g/>
,	,	kIx,	,
pozorováním	pozorování	k1gNnSc7	pozorování
optických	optický	k2eAgInPc2d1	optický
<g/>
,	,	kIx,	,
magnetických	magnetický	k2eAgInPc2d1	magnetický
a	a	k8xC	a
elektrických	elektrický	k2eAgInPc2d1	elektrický
jevů	jev	k1gInPc2	jev
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
studiem	studio	k1gNnSc7	studio
fluorescence	fluorescence	k1gFnSc2	fluorescence
a	a	k8xC	a
fosforescence	fosforescence	k1gFnSc2	fosforescence
<g/>
,	,	kIx,	,
magnetické	magnetický	k2eAgFnSc2d1	magnetická
rotace	rotace	k1gFnSc2	rotace
roviny	rovina	k1gFnSc2	rovina
polarizovaného	polarizovaný	k2eAgNnSc2d1	polarizované
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
absorpčních	absorpční	k2eAgNnPc2d1	absorpční
spekter	spektrum	k1gNnPc2	spektrum
krystalů	krystal	k1gInPc2	krystal
v	v	k7c6	v
magnetickém	magnetický	k2eAgNnSc6d1	magnetické
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
Hallův	Hallův	k2eAgInSc1d1	Hallův
jevem	jev	k1gInSc7	jev
<g/>
,	,	kIx,	,
dielektrickými	dielektrický	k2eAgFnPc7d1	dielektrická
konstantami	konstanta	k1gFnPc7	konstanta
a	a	k8xC	a
hlavně	hlavně	k6eAd1	hlavně
odporem	odpor	k1gInSc7	odpor
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
významným	významný	k2eAgInSc7d1	významný
objevem	objev	k1gInSc7	objev
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
supravodivost	supravodivost	k1gFnSc1	supravodivost
čistých	čistý	k2eAgInPc2d1	čistý
kovů	kov	k1gInPc2	kov
jako	jako	k8xS	jako
rtuti	rtuť	k1gFnSc2	rtuť
<g/>
,	,	kIx,	,
cínu	cín	k1gInSc2	cín
a	a	k8xC	a
olova	olovo	k1gNnSc2	olovo
při	při	k7c6	při
velmi	velmi	k6eAd1	velmi
nízkých	nízký	k2eAgFnPc6d1	nízká
teplotách	teplota	k1gFnPc6	teplota
a	a	k8xC	a
následné	následný	k2eAgNnSc4d1	následné
pozorování	pozorování	k1gNnSc4	pozorování
trvalých	trvalý	k2eAgInPc2d1	trvalý
proudů	proud	k1gInPc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc4	výsledek
výzkumu	výzkum	k1gInSc2	výzkum
publikoval	publikovat	k5eAaBmAgMnS	publikovat
v	v	k7c4	v
Proceedings	Proceedings	k1gInSc4	Proceedings
of	of	k?	of
the	the	k?	the
Royal	Royal	k1gMnSc1	Royal
Academy	Academa	k1gFnSc2	Academa
of	of	k?	of
Sciences	Sciences	k1gInSc1	Sciences
of	of	k?	of
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
a	a	k8xC	a
také	také	k9	také
v	v	k7c4	v
Communications	Communications	k1gInSc4	Communications
z	z	k7c2	z
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
laboratoře	laboratoř	k1gFnSc2	laboratoř
v	v	k7c6	v
Leydenu	Leyden	k1gInSc6	Leyden
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
vědců	vědec	k1gMnPc2	vědec
přijíždělo	přijíždět	k5eAaImAgNnS	přijíždět
do	do	k7c2	do
Leydenu	Leyden	k1gInSc2	Leyden
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
laboratoře	laboratoř	k1gFnSc2	laboratoř
na	na	k7c4	na
stáže	stáž	k1gFnPc4	stáž
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
ve	v	k7c6	v
30	[number]	k4	30
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgMnS	být
Kamerlingh	Kamerlingh	k1gMnSc1	Kamerlingh
Onnes	Onnes	k1gMnSc1	Onnes
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
členem	člen	k1gMnSc7	člen
Královské	královský	k2eAgFnSc2d1	královská
Akademie	akademie	k1gFnSc2	akademie
Věd	věda	k1gFnPc2	věda
v	v	k7c6	v
Amsterdamu	Amsterdam	k1gInSc6	Amsterdam
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
Association	Association	k1gInSc4	Association
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
Institut	institut	k1gInSc1	institut
<g/>
)	)	kIx)	)
du	du	k?	du
Froid	Froid	k1gInSc1	Froid
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
nejoblíbenější	oblíbený	k2eAgFnSc7d3	nejoblíbenější
rekreací	rekreace	k1gFnSc7	rekreace
byl	být	k5eAaImAgInS	být
rodinný	rodinný	k2eAgInSc1d1	rodinný
život	život	k1gInSc1	život
a	a	k8xC	a
pomáhání	pomáhání	k1gNnSc1	pomáhání
<g/>
,	,	kIx,	,
těm	ten	k3xDgFnPc3	ten
kdo	kdo	k3yQnSc1	kdo
to	ten	k3xDgNnSc4	ten
potřebovali	potřebovat	k5eAaImAgMnP	potřebovat
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
jeho	jeho	k3xOp3gFnSc1	jeho
práce	práce	k1gFnSc1	práce
byla	být	k5eAaImAgFnS	být
i	i	k9	i
jeho	jeho	k3xOp3gMnSc7	jeho
koníčkem	koníček	k1gMnSc7	koníček
<g/>
,	,	kIx,	,
nestal	stát	k5eNaPmAgMnS	stát
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nabubřelým	nabubřelý	k2eAgMnSc7d1	nabubřelý
vědcem	vědec	k1gMnSc7	vědec
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
to	ten	k3xDgNnSc4	ten
muž	muž	k1gMnSc1	muž
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
šarmem	šarm	k1gInSc7	šarm
<g/>
,	,	kIx,	,
filantrop	filantrop	k1gMnSc1	filantrop
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
velmi	velmi	k6eAd1	velmi
aktivní	aktivní	k2eAgFnSc4d1	aktivní
během	běh	k1gInSc7	běh
a	a	k8xC	a
po	po	k7c6	po
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
ve	v	k7c6	v
vyhlazování	vyhlazování	k1gNnSc6	vyhlazování
politických	politický	k2eAgInPc2d1	politický
rozdílů	rozdíl	k1gInPc2	rozdíl
mezi	mezi	k7c7	mezi
vědci	vědec	k1gMnPc7	vědec
a	a	k8xC	a
v	v	k7c4	v
pomoci	pomoct	k5eAaPmF	pomoct
hladovějícím	hladovějící	k2eAgFnPc3d1	hladovějící
dětem	dítě	k1gFnPc3	dítě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
Adrianou	Adriana	k1gFnSc7	Adriana
Wilhelminou	Wilhelmin	k2eAgFnSc7d1	Wilhelmina
Elisabeth	Elisabetha	k1gFnPc2	Elisabetha
Bijleveld	Bijleveld	k1gInSc1	Bijleveld
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
mu	on	k3xPp3gMnSc3	on
velmi	velmi	k6eAd1	velmi
pomáhala	pomáhat	k5eAaImAgFnS	pomáhat
s	s	k7c7	s
těmito	tento	k3xDgFnPc7	tento
aktivitami	aktivita	k1gFnPc7	aktivita
a	a	k8xC	a
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
mu	on	k3xPp3gMnSc3	on
dům	dům	k1gInSc4	dům
známý	známý	k2eAgInSc4d1	známý
pohostinností	pohostinnost	k1gFnSc7	pohostinnost
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
jednoho	jeden	k4xCgMnSc4	jeden
syna	syn	k1gMnSc4	syn
<g/>
,	,	kIx,	,
Alberta	Albert	k1gMnSc4	Albert
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vysoce	vysoce	k6eAd1	vysoce
postaveným	postavený	k2eAgMnSc7d1	postavený
státním	státní	k2eAgMnSc7d1	státní
úředníkem	úředník	k1gMnSc7	úředník
v	v	k7c6	v
Haagu	Haag	k1gInSc6	Haag
<g/>
.	.	kIx.	.
</s>
<s>
Kamerlingh	Kamerlingh	k1gMnSc1	Kamerlingh
Onnes	Onnes	k1gMnSc1	Onnes
měl	mít	k5eAaImAgMnS	mít
křehké	křehký	k2eAgNnSc4d1	křehké
zdraví	zdraví	k1gNnSc4	zdraví
a	a	k8xC	a
po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
nemoci	nemoc	k1gFnSc6	nemoc
zemřel	zemřít	k5eAaPmAgInS	zemřít
21	[number]	k4	21
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1926	[number]	k4	1926
v	v	k7c6	v
Leidenu	Leiden	k1gInSc6	Leiden
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Kamerling	Kamerling	k1gInSc1	Kamerling
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
formálně	formálně	k6eAd1	formálně
druhé	druhý	k4xOgNnSc4	druhý
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
část	část	k1gFnSc1	část
příjmení	příjmení	k1gNnSc2	příjmení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
bylo	být	k5eAaImAgNnS	být
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
rodině	rodina	k1gFnSc6	rodina
zvykem	zvyk	k1gInSc7	zvyk
ho	on	k3xPp3gMnSc4	on
vypisovat	vypisovat	k5eAaImF	vypisovat
<g/>
.	.	kIx.	.
</s>
<s>
Mělo	mít	k5eAaImAgNnS	mít
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
zkracovat	zkracovat	k5eAaImF	zkracovat
"	"	kIx"	"
<g/>
H.	H.	kA	H.
Kamerling	Kamerling	k1gInSc1	Kamerling
Onnes	Onnes	k1gInSc1	Onnes
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
"	"	kIx"	"
<g/>
H.	H.	kA	H.
K.	K.	kA	K.
Onnes	Onnes	k1gMnSc1	Onnes
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Kamerlingh	Kamerlingh	k1gMnSc1	Kamerlingh
Onnes	Onnes	k1gMnSc1	Onnes
<g/>
,	,	kIx,	,
H.	H.	kA	H.
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Nieuwe	Nieuwe	k1gInSc4	Nieuwe
bewijzen	bewijzen	k2eAgInSc4d1	bewijzen
voor	voor	k1gInSc4	voor
de	de	k?	de
aswenteling	aswenteling	k1gInSc1	aswenteling
der	drát	k5eAaImRp2nS	drát
aarde	aarde	k6eAd1	aarde
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
disertace	disertace	k1gFnSc2	disertace
<g/>
.	.	kIx.	.
</s>
<s>
Groningen	Groningen	k1gInSc1	Groningen
<g/>
,	,	kIx,	,
Netherlands	Netherlands	k1gInSc1	Netherlands
<g/>
,	,	kIx,	,
1879	[number]	k4	1879
<g/>
.	.	kIx.	.
</s>
<s>
Kamerlingh	Kamerlingh	k1gMnSc1	Kamerlingh
Onnes	Onnes	k1gMnSc1	Onnes
<g/>
,	,	kIx,	,
H.	H.	kA	H.
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Algemeene	Algemeen	k1gInSc5	Algemeen
theorie	theorie	k1gFnPc1	theorie
der	drát	k5eAaImRp2nS	drát
vloeistoffen	vloeistoffen	k1gInSc1	vloeistoffen
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
Akad	Akada	k1gFnPc2	Akada
<g/>
.	.	kIx.	.
</s>
<s>
Verhandl	Verhandnout	k5eAaPmAgMnS	Verhandnout
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
,	,	kIx,	,
1881	[number]	k4	1881
<g/>
.	.	kIx.	.
</s>
<s>
Kamerlingh	Kamerlingh	k1gMnSc1	Kamerlingh
Onnes	Onnes	k1gMnSc1	Onnes
<g/>
,	,	kIx,	,
H.	H.	kA	H.
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
On	on	k3xPp3gMnSc1	on
the	the	k?	the
Cryogenic	Cryogenice	k1gFnPc2	Cryogenice
Laboratory	Laborator	k1gInPc1	Laborator
at	at	k?	at
Leyden	Leydna	k1gFnPc2	Leydna
and	and	k?	and
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Production	Production	k1gInSc1	Production
of	of	k?	of
Very	Vera	k1gFnSc2	Vera
Low	Low	k1gMnSc5	Low
Temperature	Temperatur	k1gMnSc5	Temperatur
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Comm	Comm	k1gInSc1	Comm
<g/>
.	.	kIx.	.
</s>
<s>
Phys	Phys	k6eAd1	Phys
<g/>
.	.	kIx.	.
</s>
<s>
Lab	Lab	k?	Lab
<g/>
.	.	kIx.	.
</s>
<s>
Univ	Univ	k1gMnSc1	Univ
<g/>
.	.	kIx.	.
</s>
<s>
Leiden	Leidna	k1gFnPc2	Leidna
14	[number]	k4	14
<g/>
,	,	kIx,	,
1894	[number]	k4	1894
<g/>
.	.	kIx.	.
</s>
<s>
Kamerlingh	Kamerlingh	k1gMnSc1	Kamerlingh
Onnes	Onnes	k1gMnSc1	Onnes
<g/>
,	,	kIx,	,
H.	H.	kA	H.
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Théorie	Théorie	k1gFnPc4	Théorie
générale	générale	k6eAd1	générale
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
état	état	k1gMnSc1	état
fluide	fluid	k1gInSc5	fluid
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Haarlem	Haarl	k1gInSc7	Haarl
Arch	archa	k1gFnPc2	archa
<g/>
.	.	kIx.	.
</s>
<s>
Neerl	Neerl	k1gInSc1	Neerl
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
,	,	kIx,	,
1896	[number]	k4	1896
<g/>
.	.	kIx.	.
</s>
<s>
Kamerlingh	Kamerlingh	k1gMnSc1	Kamerlingh
Onnes	Onnes	k1gMnSc1	Onnes
<g/>
,	,	kIx,	,
H.	H.	kA	H.
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Superconductivity	Superconductivita	k1gFnSc2	Superconductivita
of	of	k?	of
Mercury	Mercura	k1gFnSc2	Mercura
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Comm	Comm	k1gInSc1	Comm
<g/>
.	.	kIx.	.
</s>
<s>
Phys	Phys	k6eAd1	Phys
<g/>
.	.	kIx.	.
</s>
<s>
Lab	Lab	k?	Lab
<g/>
.	.	kIx.	.
</s>
<s>
Univ	Univ	k1gMnSc1	Univ
<g/>
.	.	kIx.	.
</s>
<s>
Leiden	Leidno	k1gNnPc2	Leidno
<g/>
,	,	kIx,	,
Nos	nos	k1gInSc1	nos
<g/>
.	.	kIx.	.
122	[number]	k4	122
and	and	k?	and
124	[number]	k4	124
<g/>
,	,	kIx,	,
1911	[number]	k4	1911
Kamerlingh	Kamerlingha	k1gFnPc2	Kamerlingha
Onnes	Onnesa	k1gFnPc2	Onnesa
<g/>
,	,	kIx,	,
H.	H.	kA	H.
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
On	on	k3xPp3gMnSc1	on
the	the	k?	the
Lowest	Lowest	k1gMnSc1	Lowest
Temperature	Temperatur	k1gMnSc5	Temperatur
Yet	Yet	k1gFnPc3	Yet
Obtained	Obtained	k1gInSc4	Obtained
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Comm	Comm	k1gInSc1	Comm
<g/>
.	.	kIx.	.
</s>
<s>
Phys	Phys	k6eAd1	Phys
<g/>
.	.	kIx.	.
</s>
<s>
Lab	Lab	k?	Lab
<g/>
.	.	kIx.	.
</s>
<s>
Univ	Univ	k1gMnSc1	Univ
<g/>
.	.	kIx.	.
</s>
<s>
Leiden	Leidno	k1gNnPc2	Leidno
<g/>
,	,	kIx,	,
No	no	k9	no
<g/>
.	.	kIx.	.
159	[number]	k4	159
<g/>
,	,	kIx,	,
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
<s>
Lubomír	Lubomír	k1gMnSc1	Lubomír
Sodomka	Sodomka	k1gFnSc1	Sodomka
<g/>
,	,	kIx,	,
Magdalena	Magdalena	k1gFnSc1	Magdalena
Sodomková	Sodomková	k1gFnSc1	Sodomková
<g/>
,	,	kIx,	,
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
SET	set	k1gInSc1	set
OUT	OUT	kA	OUT
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-902058-5-2	[number]	k4	80-902058-5-2
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Heike	Heik	k1gInSc2	Heik
Kamerlingh	Kamerlingh	k1gInSc1	Kamerlingh
Onnes	Onnes	k1gInSc1	Onnes
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Životopis	životopis	k1gInSc1	životopis
na	na	k7c6	na
nobelprize	nobelpriza	k1gFnSc6	nobelpriza
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
