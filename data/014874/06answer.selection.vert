<s>
Americká	americký	k2eAgFnSc1d1	americká
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
1861	[number]	k4	1861
<g/>
–	–	k?	–
<g/>
1865	[number]	k4	1865
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
ozbrojený	ozbrojený	k2eAgInSc4d1	ozbrojený
konflikt	konflikt	k1gInSc4	konflikt
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
probíhal	probíhat	k5eAaImAgInS	probíhat
na	na	k7c6	na
severoamerickém	severoamerický	k2eAgInSc6d1	severoamerický
kontinentu	kontinent	k1gInSc6	kontinent
mezi	mezi	k7c7	mezi
státy	stát	k1gInPc7	stát
Unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
zakladatelskými	zakladatelský	k2eAgInPc7d1	zakladatelský
státy	stát	k1gInPc7	stát
USA	USA	kA	USA
<g/>
,	,	kIx,	,
a	a	k8xC	a
Státy	stát	k1gInPc1	stát
konfederace	konfederace	k1gFnSc2	konfederace
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
koalice	koalice	k1gFnSc1	koalice
jedenácti	jedenáct	k4xCc2	jedenáct
amerických	americký	k2eAgInPc2d1	americký
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
chtěly	chtít	k5eAaImAgInP	chtít
odtrhnout	odtrhnout	k5eAaPmF	odtrhnout
od	od	k7c2	od
Unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
