<p>
<s>
Sudička	sudička	k1gFnSc1	sudička
je	být	k5eAaImIp3nS	být
slovanský	slovanský	k2eAgMnSc1d1	slovanský
démon	démon	k1gMnSc1	démon
osudu	osud	k1gInSc2	osud
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
vystupující	vystupující	k2eAgMnPc1d1	vystupující
ve	v	k7c6	v
trojici	trojice	k1gFnSc6	trojice
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
určuje	určovat	k5eAaImIp3nS	určovat
osud	osud	k1gInSc4	osud
novorozence	novorozenec	k1gMnSc2	novorozenec
<g/>
.	.	kIx.	.
</s>
<s>
Sudičky	sudička	k1gFnPc1	sudička
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
především	především	k9	především
v	v	k7c6	v
západoslovanské	západoslovanský	k2eAgFnSc6d1	západoslovanská
a	a	k8xC	a
jihoslovanské	jihoslovanský	k2eAgFnSc6d1	Jihoslovanská
kultuře	kultura	k1gFnSc6	kultura
<g/>
,	,	kIx,	,
ve	v	k7c4	v
východoslovanské	východoslovanský	k2eAgFnPc4d1	východoslovanská
oblasti	oblast	k1gFnPc4	oblast
jejich	jejich	k3xOp3gNnSc4	jejich
místo	místo	k1gNnSc4	místo
zpravidla	zpravidla	k6eAd1	zpravidla
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
Rožanice	Rožanice	k1gFnPc1	Rožanice
<g/>
.	.	kIx.	.
</s>
<s>
Podobají	podobat	k5eAaImIp3nP	podobat
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
řecké	řecký	k2eAgFnSc2d1	řecká
Moiry	Moira	k1gFnSc2	Moira
<g/>
,	,	kIx,	,
římské	římský	k2eAgInPc1d1	římský
Parky	park	k1gInPc1	park
a	a	k8xC	a
severské	severský	k2eAgFnSc2d1	severská
Norny	norna	k1gFnSc2	norna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
Sudiček	sudička	k1gFnPc2	sudička
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
Rožanic	Rožanice	k1gFnPc2	Rožanice
<g/>
,	,	kIx,	,
mohly	moct	k5eAaImAgFnP	moct
osud	osud	k1gInSc4	osud
dítěti	dítě	k1gNnSc6	dítě
určovat	určovat	k5eAaImF	určovat
víly	víla	k1gFnPc4	víla
<g/>
,	,	kIx,	,
polské	polský	k2eAgInPc4d1	polský
boginje	boginj	k1gInPc4	boginj
<g/>
,	,	kIx,	,
pomořanští	pomořanský	k2eAgMnPc1d1	pomořanský
kraśniacy	kraśniaca	k1gFnSc2	kraśniaca
<g/>
,	,	kIx,	,
severoruští	severoruský	k2eAgMnPc1d1	severoruský
udělъ	udělъ	k?	udělъ
či	či	k8xC	či
huculští	huculský	k2eAgMnPc1d1	huculský
sudci	sudce	k1gMnPc1	sudce
<g/>
.	.	kIx.	.
</s>
<s>
Slovanská	slovanský	k2eAgFnSc1d1	Slovanská
lidová	lidový	k2eAgFnSc1d1	lidová
kultura	kultura	k1gFnSc1	kultura
další	další	k2eAgMnPc4d1	další
démony	démon	k1gMnPc4	démon
osudu	osud	k1gInSc2	osud
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
zosobnění	zosobnění	k1gNnSc1	zosobnění
dobrého	dobrý	k2eAgInSc2d1	dobrý
a	a	k8xC	a
zlého	zlý	k2eAgInSc2d1	zlý
osudu	osud	k1gInSc2	osud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pojmenování	pojmenování	k1gNnPc1	pojmenování
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
slovanských	slovanský	k2eAgInPc6d1	slovanský
jazycích	jazyk	k1gInPc6	jazyk
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gNnSc1	jejich
pojmenování	pojmenování	k1gNnSc1	pojmenování
zpravidla	zpravidla	k6eAd1	zpravidla
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
osudu	osud	k1gInSc2	osud
<g/>
,	,	kIx,	,
příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
české	český	k2eAgFnPc4d1	Česká
Sudice	sudice	k1gFnPc4	sudice
<g/>
,	,	kIx,	,
srbské	srbský	k2eAgFnSc2d1	Srbská
a	a	k8xC	a
chorvatské	chorvatský	k2eAgFnSc2d1	chorvatská
Sudjenice	Sudjenice	k1gFnSc2	Sudjenice
nebo	nebo	k8xC	nebo
bulharské	bulharský	k2eAgFnSc3d1	bulharská
Sudženici	Sudženice	k1gFnSc3	Sudženice
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
pojmenování	pojmenování	k1gNnSc1	pojmenování
jsou	být	k5eAaImIp3nP	být
jako	jako	k9	jako
u	u	k7c2	u
východoslovanských	východoslovanský	k2eAgFnPc2d1	východoslovanská
Rožanic	Rožanice	k1gFnPc2	Rožanice
odvozena	odvozen	k2eAgFnSc1d1	odvozena
od	od	k7c2	od
rození	rození	k1gNnSc2	rození
či	či	k8xC	či
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
polské	polský	k2eAgFnPc4d1	polská
Rodzanice	Rodzanice	k1gFnPc4	Rodzanice
<g/>
,	,	kIx,	,
slovinské	slovinský	k2eAgFnPc4d1	slovinská
Rojenice	rojenice	k1gFnPc4	rojenice
a	a	k8xC	a
chorvatské	chorvatský	k2eAgFnPc4d1	chorvatská
Rodjenice	Rodjenice	k1gFnPc4	Rodjenice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bulharštině	bulharština	k1gFnSc6	bulharština
se	se	k3xPyFc4	se
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
jméno	jméno	k1gNnSc1	jméno
narъ	narъ	k?	narъ
odvozené	odvozený	k2eAgFnSc2d1	odvozená
z	z	k7c2	z
narok	narok	k6eAd1	narok
"	"	kIx"	"
<g/>
úděl	úděl	k1gInSc1	úděl
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
orisnici	orisnice	k1gFnSc4	orisnice
<g/>
,	,	kIx,	,
urisnici	urisnice	k1gFnSc4	urisnice
či	či	k8xC	či
uresnici	uresnice	k1gFnSc4	uresnice
odvozené	odvozený	k2eAgNnSc1d1	odvozené
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
jsou	být	k5eAaImIp3nP	být
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
němčiny	němčina	k1gFnSc2	němčina
nazývány	nazýván	k2eAgFnPc4d1	nazývána
bílé	bílý	k2eAgFnPc4d1	bílá
ženy	žena	k1gFnPc4	žena
(	(	kIx(	(
<g/>
weisse	weisse	k6eAd1	weisse
Frauen	Frauen	k2eAgInSc1d1	Frauen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
želkinje	želkinje	k1gFnSc1	želkinje
či	či	k8xC	či
želik	želik	k1gInSc1	želik
žene	hnát	k5eAaImIp3nS	hnát
(	(	kIx(	(
<g/>
selige	selige	k1gNnSc1	selige
Weiber	Weibra	k1gFnPc2	Weibra
"	"	kIx"	"
<g/>
požehnané	požehnaný	k2eAgFnPc1d1	požehnaná
ženy	žena	k1gFnPc1	žena
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
aidovske	aidovske	k1gFnSc1	aidovske
deklice	deklice	k1gFnSc1	deklice
(	(	kIx(	(
<g/>
heidnische	heidnische	k1gNnSc1	heidnische
Weiberl	Weiberla	k1gFnPc2	Weiberla
"	"	kIx"	"
<g/>
pohanské	pohanský	k2eAgFnSc2d1	pohanská
ženy	žena	k1gFnSc2	žena
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Česká	český	k2eAgFnSc1d1	Česká
lidová	lidový	k2eAgFnSc1d1	lidová
kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
Sudičky	sudička	k1gFnPc1	sudička
<g/>
,	,	kIx,	,
Sudice	sudice	k1gFnPc1	sudice
<g/>
,	,	kIx,	,
Rodičky	rodička	k1gFnPc1	rodička
či	či	k8xC	či
Věštice	věštice	k1gFnSc1	věštice
a	a	k8xC	a
víra	víra	k1gFnSc1	víra
v	v	k7c4	v
ně	on	k3xPp3gFnPc4	on
je	být	k5eAaImIp3nS	být
doložena	doložit	k5eAaPmNgFnS	doložit
již	již	k6eAd1	již
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
zpravidla	zpravidla	k6eAd1	zpravidla
podobu	podoba	k1gFnSc4	podoba
bílých	bílý	k2eAgFnPc2d1	bílá
éterických	éterický	k2eAgFnPc2d1	éterická
panen	panna	k1gFnPc2	panna
či	či	k8xC	či
přívětivých	přívětivý	k2eAgFnPc2d1	přívětivá
babiček	babička	k1gFnPc2	babička
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
oděny	odět	k5eAaPmNgFnP	odět
v	v	k7c4	v
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
šat	šat	k1gInSc4	šat
a	a	k8xC	a
zahaleny	zahalen	k2eAgFnPc4d1	zahalena
plachetkou	plachetka	k1gFnSc7	plachetka
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
určují	určovat	k5eAaImIp3nP	určovat
osudu	osud	k1gInSc3	osud
dítěte	dítě	k1gNnSc2	dítě
mají	mít	k5eAaImIp3nP	mít
rukou	ruka	k1gFnSc7	ruka
svíce	svíce	k1gFnSc2	svíce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
když	když	k8xS	když
vyřknou	vyřknout	k5eAaPmIp3nP	vyřknout
svůj	svůj	k3xOyFgInSc4	svůj
výrok	výrok	k1gInSc4	výrok
zhasnou	zhasnout	k5eAaPmIp3nP	zhasnout
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
je	on	k3xPp3gNnPc4	on
může	moct	k5eAaImIp3nS	moct
vidět	vidět	k5eAaImF	vidět
jen	jen	k9	jen
zřídka	zřídka	k6eAd1	zřídka
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
že	že	k8xS	že
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
přesto	přesto	k8xC	přesto
stane	stanout	k5eAaPmIp3nS	stanout
ochrne	ochrnout	k5eAaPmIp3nS	ochrnout
hrůzou	hrůza	k1gFnSc7	hrůza
<g/>
.	.	kIx.	.
<g/>
Zjevují	zjevovat	k5eAaImIp3nP	zjevovat
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
o	o	k7c6	o
půlnoci	půlnoc	k1gFnSc6	půlnoc
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
dítěte	dítě	k1gNnSc2	dítě
a	a	k8xC	a
na	na	k7c4	na
rodičku	rodička	k1gFnSc4	rodička
sesílají	sesílat	k5eAaImIp3nP	sesílat
hluboký	hluboký	k2eAgInSc4d1	hluboký
spánek	spánek	k1gInSc4	spánek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
podáních	podání	k1gNnPc6	podání
nevstupují	vstupovat	k5eNaImIp3nP	vstupovat
do	do	k7c2	do
světnice	světnice	k1gFnSc2	světnice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
za	za	k7c7	za
okny	okno	k1gNnPc7	okno
nebo	nebo	k8xC	nebo
sedí	sedit	k5eAaImIp3nS	sedit
na	na	k7c6	na
střeše	střecha	k1gFnSc6	střecha
a	a	k8xC	a
věští	věštit	k5eAaImIp3nS	věštit
mu	on	k3xPp3gMnSc3	on
osud	osud	k1gInSc1	osud
z	z	k7c2	z
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
výroky	výrok	k1gInPc1	výrok
se	se	k3xPyFc4	se
týkaly	týkat	k5eAaImAgInP	týkat
předevších	předevší	k2eAgInPc2d1	předevší
třech	tři	k4xCgInPc2	tři
okruhů	okruh	k1gInPc2	okruh
života	život	k1gInSc2	život
novorozence	novorozenec	k1gMnSc2	novorozenec
<g/>
:	:	kIx,	:
věku	věk	k1gInSc6	věk
kterého	který	k3yIgMnSc4	který
se	se	k3xPyFc4	se
dožije	dožít	k5eAaPmIp3nS	dožít
<g/>
,	,	kIx,	,
chudoby	chudoba	k1gFnSc2	chudoba
či	či	k8xC	či
bohatství	bohatství	k1gNnSc2	bohatství
a	a	k8xC	a
způsobu	způsob	k1gInSc2	způsob
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
byla	být	k5eAaImAgFnS	být
třetí	třetí	k4xOgFnSc1	třetí
Sudička	sudička	k1gFnSc1	sudička
často	často	k6eAd1	často
ztotožňována	ztotožňovat	k5eAaImNgFnS	ztotožňovat
se	s	k7c7	s
Smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
byla	být	k5eAaImAgFnS	být
také	také	k9	také
představa	představa	k1gFnSc1	představa
že	že	k8xS	že
Sudičky	sudička	k1gFnPc1	sudička
život	život	k1gInSc4	život
člověka	člověk	k1gMnSc2	člověk
předou	příst	k5eAaImIp3nP	příst
jako	jako	k9	jako
nit	nit	k1gFnSc4	nit
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
délka	délka	k1gFnSc1	délka
určovala	určovat	k5eAaImAgFnS	určovat
kolika	kolik	k4yRc2	kolik
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
dožije	dožít	k5eAaPmIp3nS	dožít
<g/>
,	,	kIx,	,
a	a	k8xC	a
nejstarší	starý	k2eAgFnPc1d3	nejstarší
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
ji	on	k3xPp3gFnSc4	on
přestřihává	přestřihávat	k5eAaImIp3nS	přestřihávat
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
dítěti	dítě	k1gNnSc3	dítě
zajištěn	zajištěn	k2eAgInSc4d1	zajištěn
příznivý	příznivý	k2eAgInSc4d1	příznivý
osudem	osud	k1gInSc7	osud
tak	tak	k8xC	tak
se	se	k3xPyFc4	se
těmto	tento	k3xDgInPc3	tento
bytostem	bytost	k1gFnPc3	bytost
obětovalo	obětovat	k5eAaBmAgNnS	obětovat
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
nedodržení	nedodržení	k1gNnSc1	nedodržení
zvyků	zvyk	k1gInPc2	zvyk
mohlo	moct	k5eAaImAgNnS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
že	že	k8xS	že
Sudičky	sudička	k1gFnSc2	sudička
novorozence	novorozenec	k1gMnSc4	novorozenec
podvrhly	podvrhnout	k5eAaPmAgInP	podvrhnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odrazy	odraz	k1gInPc1	odraz
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Sudičky	sudička	k1gFnPc1	sudička
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
pohádce	pohádka	k1gFnSc6	pohádka
O	o	k7c6	o
Šípkové	Šípkové	k2eAgFnSc6d1	Šípkové
Růžence	Růženka	k1gFnSc6	Růženka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Předeme	příst	k5eAaImIp1nP	příst
<g/>
,	,	kIx,	,
předeme	příst	k5eAaImIp1nP	příst
zlatou	zlatý	k2eAgFnSc4d1	zlatá
nitku	nitka	k1gFnSc4	nitka
<g/>
,	,	kIx,	,
československá	československý	k2eAgNnPc4d1	Československé
filmová	filmový	k2eAgNnPc4d1	filmové
pohádka	pohádka	k1gFnSc1	pohádka
Vlasty	Vlasta	k1gMnSc2	Vlasta
Janečkové	Janečková	k1gFnSc2	Janečková
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
</s>
</p>
<p>
<s>
Sudičky	sudička	k1gFnPc1	sudička
jsou	být	k5eAaImIp3nP	být
zmíněny	zmínit	k5eAaPmNgFnP	zmínit
v	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
Píseň	píseň	k1gFnSc4	píseň
pro	pro	k7c4	pro
malou	malý	k2eAgFnSc4d1	malá
Lenku	Lenka	k1gFnSc4	Lenka
písničkáře	písničkář	k1gMnSc2	písničkář
Jaromíra	Jaromír	k1gMnSc2	Jaromír
Nohavici	nohavice	k1gFnSc4	nohavice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Rožanice	Rožanice	k1gFnSc1	Rožanice
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
