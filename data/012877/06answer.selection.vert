<s>
Sudička	sudička	k1gFnSc1	sudička
je	být	k5eAaImIp3nS	být
slovanský	slovanský	k2eAgMnSc1d1	slovanský
démon	démon	k1gMnSc1	démon
osudu	osud	k1gInSc2	osud
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
vystupující	vystupující	k2eAgMnPc1d1	vystupující
ve	v	k7c6	v
trojici	trojice	k1gFnSc6	trojice
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
určuje	určovat	k5eAaImIp3nS	určovat
osud	osud	k1gInSc4	osud
novorozence	novorozenec	k1gMnSc2	novorozenec
<g/>
.	.	kIx.	.
</s>
