<p>
<s>
Saint-Cyr	Saint-Cyr	k1gInSc1	Saint-Cyr
je	být	k5eAaImIp3nS	být
soundtrackové	soundtrackový	k2eAgNnSc4d1	soundtrackový
album	album	k1gNnSc4	album
Johna	John	k1gMnSc2	John
Calea	Caleus	k1gMnSc2	Caleus
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
u	u	k7c2	u
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
Archipel	archipel	k1gInSc4	archipel
35	[number]	k4	35
<g/>
/	/	kIx~	/
<g/>
Virgin	Virgin	k1gInSc1	Virgin
France	Franc	k1gMnSc2	Franc
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
filmu	film	k1gInSc3	film
Saint-Cyr	Saint-Cyra	k1gFnPc2	Saint-Cyra
<g/>
.	.	kIx.	.
</s>
<s>
Soundtrack	soundtrack	k1gInSc1	soundtrack
byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Césara	César	k1gMnSc4	César
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
filmu	film	k1gInSc3	film
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
skladeb	skladba	k1gFnPc2	skladba
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
HudebníciLisa	HudebníciLisa	k1gFnSc1	HudebníciLisa
Bielawa	Bielaw	k1gInSc2	Bielaw
−	−	k?	−
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
Peter	Peter	k1gMnSc1	Peter
Stuart	Stuarta	k1gFnPc2	Stuarta
−	−	k?	−
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
Mark	Mark	k1gMnSc1	Mark
R.	R.	kA	R.
Deffenbaugh	Deffenbaugh	k1gMnSc1	Deffenbaugh
−	−	k?	−
kytara	kytara	k1gFnSc1	kytara
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
Fideli	Fidel	k1gInPc7	Fidel
−	−	k?	−
flétna	flétna	k1gFnSc1	flétna
</s>
</p>
<p>
<s>
Stephanie	Stephanie	k1gFnSc1	Stephanie
Sarin	sarin	k1gInSc1	sarin
−	−	k?	−
flétna	flétna	k1gFnSc1	flétna
</s>
</p>
<p>
<s>
Jonathan	Jonathan	k1gMnSc1	Jonathan
Spitz	Spitz	k1gMnSc1	Spitz
−	−	k?	−
violoncello	violoncello	k1gNnSc4	violoncello
</s>
</p>
<p>
<s>
Greg	Greg	k1gInSc1	Greg
Hesselink	Hesselink	k1gInSc1	Hesselink
−	−	k?	−
violoncello	violoncello	k1gNnSc1	violoncello
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Whitfield	Whitfield	k1gMnSc1	Whitfield
−	−	k?	−
violoncello	violoncello	k1gNnSc4	violoncello
</s>
</p>
<p>
<s>
Bruce	Bruce	k1gMnSc1	Bruce
Wong	Wong	k1gMnSc1	Wong
−	−	k?	−
violoncello	violoncello	k1gNnSc4	violoncello
</s>
</p>
<p>
<s>
Jim	on	k3xPp3gMnPc3	on
O	o	k7c6	o
<g/>
'	'	kIx"	'
<g/>
Connor	Connor	k1gInSc1	Connor
−	−	k?	−
trubka	trubka	k1gFnSc1	trubka
</s>
</p>
<p>
<s>
Lee	Lea	k1gFnSc3	Lea
Wall	Wall	k1gMnSc1	Wall
−	−	k?	−
perkuseOstatníJohn	perkuseOstatníJohn	k1gMnSc1	perkuseOstatníJohn
Cale	Cal	k1gFnSc2	Cal
−	−	k?	−
producent	producent	k1gMnSc1	producent
</s>
</p>
<p>
<s>
Randall	Randall	k1gMnSc1	Randall
Woolf	Woolf	k1gMnSc1	Woolf
−	−	k?	−
aranže	aranže	k1gFnSc2	aranže
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
Voight	Voight	k2eAgMnSc1d1	Voight
−	−	k?	−
nahrávání	nahrávání	k1gNnSc1	nahrávání
<g/>
,	,	kIx,	,
mixing	mixing	k1gInSc1	mixing
</s>
</p>
<p>
<s>
Pierre	Pierr	k1gMnSc5	Pierr
Collier	Collier	k1gMnSc1	Collier
−	−	k?	−
design	design	k1gInSc1	design
obalu	obal	k1gInSc2	obal
</s>
</p>
<p>
<s>
Roger	Roger	k1gMnSc1	Roger
Arpajou	Arpaja	k1gMnSc7	Arpaja
−	−	k?	−
fotografie	fotografie	k1gFnSc1	fotografie
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Saint-Cyr	Saint-Cyr	k1gInSc1	Saint-Cyr
na	na	k7c4	na
Discogs	Discogs	k1gInSc4	Discogs
</s>
</p>
