<s>
Ovotestikulátní	Ovotestikulátní	k2eAgInSc1d1
intersexuál	intersexuál	k1gInSc1
byl	být	k5eAaImAgInS
dříve	dříve	k6eAd2
známý	známý	k2eAgInSc1d1
pod	pod	k7c7
názvem	název	k1gInSc7
pravý	pravý	k2eAgMnSc1d1
hermafrodit	hermafrodit	k1gMnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jedinec	jedinec	k1gMnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
pohlavní	pohlavní	k2eAgFnPc1d1
žlázy	žláza	k1gFnPc1
nejsou	být	k5eNaImIp3nP
jednotné	jednotný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc1
vnější	vnější	k2eAgFnPc1d1
genitálie	genitálie	k1gFnPc1
mohou	moct	k5eAaImIp3nP
vypadat	vypadat	k5eAaImF,k5eAaPmF
typicky	typicky	k6eAd1
žensky	žensky	k6eAd1
<g/>
,	,	kIx,
typicky	typicky	k6eAd1
mužsky	mužsky	k6eAd1
nebo	nebo	k8xC
také	také	k9
nejednoznačně	jednoznačně	k6eNd1
<g/>
.	.	kIx.
</s>