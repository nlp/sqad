<s desamb="1">
Jedinec	jedinec	k1gMnSc1
se	se	k3xPyFc4
však	však	k9
liší	lišit	k5eAaImIp3nS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
kombinaci	kombinace	k1gFnSc4
jednoho	jeden	k4xCgNnSc2
varlete	varle	k1gNnSc2
a	a	k8xC
jednoho	jeden	k4xCgInSc2
vaječníku	vaječník	k1gInSc2
nebo	nebo	k8xC
jeho	jeho	k3xOp3gFnSc2
pohlavní	pohlavní	k2eAgFnSc2d1
žlázy	žláza	k1gFnSc2
tvoří	tvořit	k5eAaImIp3nS
jedno	jeden	k4xCgNnSc1
varle	varle	k1gNnSc1
a	a	k8xC
tzv.	tzv.	kA
ovotestis	ovotestis	k1gInSc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
kombinace	kombinace	k1gFnSc1
varlete	varle	k1gNnSc2
a	a	k8xC
vaječníku	vaječník	k1gInSc2
<g/>
,	,	kIx,
popř.	popř.	kA
jeden	jeden	k4xCgInSc4
vaječník	vaječník	k1gInSc4
a	a	k8xC
ovotestis	ovotestis	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>