<s>
Moravština	moravština	k1gFnSc1	moravština
<g/>
,	,	kIx,	,
též	též	k9	též
moravský	moravský	k2eAgInSc4d1	moravský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
podoby	podoba	k1gFnSc2	podoba
češtiny	čeština	k1gFnSc2	čeština
užívané	užívaný	k2eAgFnSc2d1	užívaná
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
má	mít	k5eAaImIp3nS	mít
význam	význam	k1gInSc4	význam
především	především	k9	především
pro	pro	k7c4	pro
odlišení	odlišení	k1gNnSc4	odlišení
od	od	k7c2	od
podoby	podoba	k1gFnSc2	podoba
jazyka	jazyk	k1gInSc2	jazyk
užívané	užívaný	k2eAgFnPc4d1	užívaná
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
fakticky	fakticky	k6eAd1	fakticky
jednotný	jednotný	k2eAgInSc1d1	jednotný
moravský	moravský	k2eAgInSc1d1	moravský
jazyk	jazyk	k1gInSc1	jazyk
ani	ani	k8xC	ani
jednotné	jednotný	k2eAgNnSc1d1	jednotné
moravské	moravský	k2eAgNnSc1d1	Moravské
nářečí	nářečí	k1gNnSc1	nářečí
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
;	;	kIx,	;
moravské	moravský	k2eAgNnSc1d1	Moravské
území	území	k1gNnSc1	území
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
značně	značně	k6eAd1	značně
nářečně	nářečně	k6eAd1	nářečně
rozrůzněné	rozrůzněný	k2eAgInPc1d1	rozrůzněný
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
rozrůzněnější	rozrůzněný	k2eAgNnPc1d2	rozrůzněný
než	než	k8xS	než
území	území	k1gNnPc1	území
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
na	na	k7c4	na
středomoravská	středomoravský	k2eAgNnPc4d1	středomoravské
<g/>
,	,	kIx,	,
východomoravská	východomoravský	k2eAgNnPc4d1	východomoravské
a	a	k8xC	a
lašská	lašský	k2eAgNnPc4d1	Lašské
nářečí	nářečí	k1gNnPc4	nářečí
<g/>
.	.	kIx.	.
</s>
<s>
Moravská	moravský	k2eAgFnSc1d1	Moravská
podoba	podoba	k1gFnSc1	podoba
češtiny	čeština	k1gFnSc2	čeština
však	však	k9	však
byla	být	k5eAaImAgFnS	být
některými	některý	k3yIgMnPc7	některý
rodilými	rodilý	k2eAgMnPc7d1	rodilý
mluvčími	mluvčí	k1gMnPc7	mluvčí
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
natolik	natolik	k6eAd1	natolik
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
od	od	k7c2	od
spisovné	spisovný	k2eAgFnSc2d1	spisovná
češtiny	čeština	k1gFnSc2	čeština
<g/>
,	,	kIx,	,
že	že	k8xS	že
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
překonána	překonat	k5eAaPmNgFnS	překonat
výukou	výuka	k1gFnSc7	výuka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pak	pak	k6eAd1	pak
její	její	k3xOp3gFnSc4	její
uživatel	uživatel	k1gMnSc1	uživatel
byl	být	k5eAaImAgMnS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
Čecha	Čech	k1gMnSc4	Čech
<g/>
/	/	kIx~	/
<g/>
Češku	Češka	k1gFnSc4	Češka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
snahy	snaha	k1gFnPc1	snaha
některých	některý	k3yIgMnPc2	některý
moravistů	moravista	k1gMnPc2	moravista
o	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
zvláštního	zvláštní	k2eAgInSc2d1	zvláštní
moravského	moravský	k2eAgInSc2d1	moravský
spisovného	spisovný	k2eAgInSc2d1	spisovný
jazyka	jazyk	k1gInSc2	jazyk
z	z	k7c2	z
moravských	moravský	k2eAgNnPc2d1	Moravské
nářečí	nářečí	k1gNnPc2	nářečí
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
dosavadní	dosavadní	k2eAgFnSc1d1	dosavadní
jednotná	jednotný	k2eAgFnSc1d1	jednotná
česká	český	k2eAgFnSc1d1	Česká
národnost	národnost	k1gFnSc1	národnost
rozložila	rozložit	k5eAaPmAgFnS	rozložit
na	na	k7c4	na
českou	český	k2eAgFnSc4d1	Česká
<g/>
,	,	kIx,	,
moravskou	moravský	k2eAgFnSc4d1	Moravská
a	a	k8xC	a
slezskou	slezský	k2eAgFnSc4d1	Slezská
<g/>
,	,	kIx,	,
respondenti	respondent	k1gMnPc1	respondent
hlásící	hlásící	k2eAgMnSc1d1	hlásící
se	se	k3xPyFc4	se
ke	k	k7c3	k
zvláštní	zvláštní	k2eAgFnSc3d1	zvláštní
moravské	moravský	k2eAgFnSc3d1	Moravská
národnosti	národnost	k1gFnSc3	národnost
uváděli	uvádět	k5eAaImAgMnP	uvádět
jako	jako	k9	jako
svůj	svůj	k3xOyFgInSc4	svůj
mateřský	mateřský	k2eAgInSc4d1	mateřský
jazyk	jazyk	k1gInSc4	jazyk
češtinu	čeština	k1gFnSc4	čeština
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
příležitosti	příležitost	k1gFnSc3	příležitost
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
však	však	k9	však
vydal	vydat	k5eAaPmAgInS	vydat
Český	český	k2eAgInSc1d1	český
statistický	statistický	k2eAgInSc1d1	statistický
úřad	úřad	k1gInSc1	úřad
prohlášení	prohlášení	k1gNnSc2	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
moravský	moravský	k2eAgInSc1d1	moravský
jazyk	jazyk	k1gInSc1	jazyk
sčítán	sčítán	k2eAgInSc1d1	sčítán
jako	jako	k8xC	jako
plnohodnotný	plnohodnotný	k2eAgInSc1d1	plnohodnotný
údaj	údaj	k1gInSc1	údaj
s	s	k7c7	s
vlastním	vlastní	k2eAgInSc7d1	vlastní
číselným	číselný	k2eAgInSc7d1	číselný
kódem	kód	k1gInSc7	kód
odděleně	odděleně	k6eAd1	odděleně
od	od	k7c2	od
jazyka	jazyk	k1gInSc2	jazyk
českého	český	k2eAgInSc2d1	český
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgNnSc6	tento
sčítání	sčítání	k1gNnSc6	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
uvedlo	uvést	k5eAaPmAgNnS	uvést
moravštinu	moravština	k1gFnSc4	moravština
jako	jako	k8xC	jako
mateřský	mateřský	k2eAgInSc4d1	mateřský
jazyk	jazyk	k1gInSc4	jazyk
108	[number]	k4	108
469	[number]	k4	469
respondentů	respondent	k1gMnPc2	respondent
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
počtu	počet	k1gInSc2	počet
je	být	k5eAaImIp3nS	být
moravština	moravština	k1gFnSc1	moravština
pro	pro	k7c4	pro
62	[number]	k4	62
908	[number]	k4	908
osob	osoba	k1gFnPc2	osoba
jediným	jediný	k2eAgMnSc7d1	jediný
mateřským	mateřský	k2eAgInSc7d1	mateřský
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
zbylých	zbylý	k2eAgNnPc2d1	zbylé
45	[number]	k4	45
561	[number]	k4	561
osob	osoba	k1gFnPc2	osoba
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
mateřský	mateřský	k2eAgInSc4d1	mateřský
jazyk	jazyk	k1gInSc4	jazyk
jak	jak	k8xC	jak
moravštinu	moravština	k1gFnSc4	moravština
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
češtinu	čeština	k1gFnSc4	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
jazyka	jazyk	k1gInSc2	jazyk
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
jako	jako	k9	jako
moravského	moravský	k2eAgInSc2d1	moravský
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
slovanští	slovanštit	k5eAaImIp3nS	slovanštit
(	(	kIx(	(
<g/>
i	i	k8xC	i
germánští	germánský	k2eAgMnPc1d1	germánský
<g/>
)	)	kIx)	)
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Moravy	Morava	k1gFnSc2	Morava
považovali	považovat	k5eAaImAgMnP	považovat
především	především	k9	především
za	za	k7c4	za
Moravany	Moravan	k1gMnPc4	Moravan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
se	se	k3xPyFc4	se
pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
moravský	moravský	k2eAgInSc1d1	moravský
jazyk	jazyk	k1gInSc1	jazyk
<g/>
"	"	kIx"	"
někdy	někdy	k6eAd1	někdy
užíval	užívat	k5eAaImAgInS	užívat
vedle	vedle	k7c2	vedle
označení	označení	k1gNnSc2	označení
"	"	kIx"	"
<g/>
český	český	k2eAgInSc1d1	český
jazyk	jazyk	k1gInSc1	jazyk
<g/>
"	"	kIx"	"
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
stoletích	století	k1gNnPc6	století
až	až	k9	až
do	do	k7c2	do
novověku	novověk	k1gInSc2	novověk
a	a	k8xC	a
přežíval	přežívat	k5eAaImAgMnS	přežívat
až	až	k6eAd1	až
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Při	při	k7c6	při
sčítáních	sčítání	k1gNnPc6	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
začala	začít	k5eAaPmAgFnS	začít
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Česka	Česko	k1gNnSc2	Česko
zjišťovat	zjišťovat	k5eAaImF	zjišťovat
národnost	národnost	k1gFnSc4	národnost
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
národnost	národnost	k1gFnSc1	národnost
v	v	k7c6	v
Předlitavsku	Předlitavsko	k1gNnSc6	Předlitavsko
určovala	určovat	k5eAaImAgFnS	určovat
podle	podle	k7c2	podle
tzv.	tzv.	kA	tzv.
obcovacího	obcovací	k2eAgInSc2d1	obcovací
jazyka	jazyk	k1gInSc2	jazyk
(	(	kIx(	(
<g/>
Umgangssprache	Umgangssprache	k1gInSc1	Umgangssprache
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
musel	muset	k5eAaImAgMnS	muset
být	být	k5eAaImF	být
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
stanovených	stanovený	k2eAgInPc2d1	stanovený
devíti	devět	k4xCc2	devět
<g />
.	.	kIx.	.
</s>
<s>
jazyků	jazyk	k1gMnPc2	jazyk
<g/>
:	:	kIx,	:
němčina	němčina	k1gFnSc1	němčina
čeština	čeština	k1gFnSc1	čeština
(	(	kIx(	(
<g/>
v	v	k7c6	v
něm.	něm.	k?	něm.
originále	originál	k1gInSc6	originál
böhmisch	böhmischa	k1gFnPc2	böhmischa
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
jazyk	jazyk	k1gInSc4	jazyk
Čech	Čechy	k1gFnPc2	Čechy
<g/>
)	)	kIx)	)
<g/>
-moravština-slovenština	oravštinalovenština	k1gFnSc1	-moravština-slovenština
polština	polština	k1gFnSc1	polština
rusínština	rusínština	k1gFnSc1	rusínština
slovinština	slovinština	k1gFnSc1	slovinština
srbochorvatština	srbochorvatština	k1gFnSc1	srbochorvatština
italština	italština	k1gFnSc1	italština
rumunština	rumunština	k1gFnSc1	rumunština
maďarština	maďarština	k1gFnSc1	maďarština
Respondenti	respondent	k1gMnPc1	respondent
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1880	[number]	k4	1880
a	a	k8xC	a
1890	[number]	k4	1890
uvedli	uvést	k5eAaPmAgMnP	uvést
v	v	k7c6	v
Předlitavsku	Předlitavsko	k1gNnSc6	Předlitavsko
jako	jako	k8xS	jako
svou	svůj	k3xOyFgFnSc4	svůj
obcovací	obcovací	k2eAgFnSc4d1	obcovací
řeč	řeč	k1gFnSc4	řeč
češtinu-moravštinu-slovenštinu	češtinuoravštinulovenština	k1gFnSc4	češtinu-moravštinu-slovenština
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
uváděni	uvádět	k5eAaImNgMnP	uvádět
z	z	k7c2	z
národnostního	národnostní	k2eAgNnSc2d1	národnostní
hlediska	hledisko	k1gNnSc2	hledisko
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Češi	Čech	k1gMnPc1	Čech
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
české	český	k2eAgFnSc2d1	Česká
strany	strana	k1gFnSc2	strana
byla	být	k5eAaImAgFnS	být
uvedená	uvedený	k2eAgFnSc1d1	uvedená
sčítání	sčítání	k1gNnSc4	sčítání
lidu	lid	k1gInSc2	lid
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
zmanipulovaná	zmanipulovaný	k2eAgNnPc4d1	zmanipulované
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dobových	dobový	k2eAgFnPc2d1	dobová
zpráv	zpráva	k1gFnPc2	zpráva
např.	např.	kA	např.
němečtí	německý	k2eAgMnPc1d1	německý
sčítací	sčítací	k2eAgMnPc1d1	sčítací
komisaři	komisar	k1gMnPc1	komisar
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
odmítali	odmítat	k5eAaImAgMnP	odmítat
místním	místní	k2eAgMnPc3d1	místní
Čechům	Čech	k1gMnPc3	Čech
zapisovat	zapisovat	k5eAaImF	zapisovat
češtinu	čeština	k1gFnSc4	čeština
jako	jako	k8xS	jako
obcovací	obcovací	k2eAgFnSc4d1	obcovací
řeč	řeč	k1gFnSc4	řeč
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
s	s	k7c7	s
místními	místní	k2eAgInPc7d1	místní
německými	německý	k2eAgInPc7d1	německý
úřady	úřad	k1gInPc7	úřad
jednají	jednat	k5eAaImIp3nP	jednat
německy	německy	k6eAd1	německy
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
v	v	k7c6	v
Mošnově	Mošnov	k1gInSc6	Mošnov
německý	německý	k2eAgMnSc1d1	německý
sčítací	sčítací	k2eAgMnSc1d1	sčítací
komisař	komisař	k1gMnSc1	komisař
Josef	Josef	k1gMnSc1	Josef
Wank	Wank	k1gMnSc1	Wank
v	v	k7c6	v
dotaznících	dotazník	k1gInPc6	dotazník
škrtal	škrtat	k5eAaImAgMnS	škrtat
češtinu	čeština	k1gFnSc4	čeština
a	a	k8xC	a
nahrazoval	nahrazovat	k5eAaImAgMnS	nahrazovat
ji	on	k3xPp3gFnSc4	on
moravštinou	moravština	k1gFnSc7	moravština
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Češi	Čech	k1gMnPc1	Čech
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
Moravci	Moravec	k1gMnPc1	Moravec
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
slovanská	slovanský	k2eAgFnSc1d1	Slovanská
veřejnost	veřejnost	k1gFnSc1	veřejnost
rozvířena	rozvířen	k2eAgFnSc1d1	rozvířena
činností	činnost	k1gFnSc7	činnost
vzdělanců	vzdělanec	k1gMnPc2	vzdělanec
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
nepřesně	přesně	k6eNd1	přesně
zvána	zván	k2eAgFnSc1d1	zvána
"	"	kIx"	"
<g/>
moravský	moravský	k2eAgInSc1d1	moravský
jazykový	jazykový	k2eAgInSc1d1	jazykový
separatismus	separatismus	k1gInSc1	separatismus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Iniciátoři	iniciátor	k1gMnPc1	iniciátor
tohoto	tento	k3xDgNnSc2	tento
úsilí	úsilí	k1gNnSc2	úsilí
byli	být	k5eAaImAgMnP	být
rodáci	rodák	k1gMnPc1	rodák
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
přišlí	přišlý	k2eAgMnPc1d1	přišlý
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgNnPc3	jenž
se	se	k3xPyFc4	se
zalíbila	zalíbit	k5eAaPmAgNnP	zalíbit
tamní	tamní	k2eAgNnPc1d1	tamní
nářečí	nářečí	k1gNnPc1	nářečí
a	a	k8xC	a
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
jejich	jejich	k3xOp3gInPc4	jejich
prvky	prvek	k1gInPc4	prvek
vnést	vnést	k5eAaPmF	vnést
do	do	k7c2	do
spisovné	spisovný	k2eAgFnSc2d1	spisovná
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
hlavním	hlavní	k2eAgFnPc3d1	hlavní
osobnostem	osobnost	k1gFnPc3	osobnost
tohoto	tento	k3xDgNnSc2	tento
hnutí	hnutí	k1gNnSc2	hnutí
patřili	patřit	k5eAaImAgMnP	patřit
František	František	k1gMnSc1	František
Dobromysl	dobromysl	k1gFnSc1	dobromysl
Trnka	trnka	k1gFnSc1	trnka
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Cyril	Cyril	k1gMnSc1	Cyril
Kampelík	Kampelík	k1gMnSc1	Kampelík
a	a	k8xC	a
Vincenc	Vincenc	k1gMnSc1	Vincenc
Pavel	Pavel	k1gMnSc1	Pavel
Žák	Žák	k1gMnSc1	Žák
<g/>
.	.	kIx.	.
</s>
<s>
Nesnažili	snažit	k5eNaImAgMnP	snažit
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
nového	nový	k2eAgInSc2d1	nový
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
jejich	jejich	k3xOp3gFnSc3	jejich
činnosti	činnost	k1gFnSc3	činnost
postavil	postavit	k5eAaPmAgMnS	postavit
František	František	k1gMnSc1	František
Palacký	Palacký	k1gMnSc1	Palacký
<g/>
,	,	kIx,	,
po	po	k7c6	po
jehož	jehož	k3xOyRp3gFnSc6	jehož
intervenci	intervence	k1gFnSc6	intervence
zásahy	zásah	k1gInPc7	zásah
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
ustaly	ustat	k5eAaPmAgInP	ustat
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
Hlučínska	Hlučínsko	k1gNnSc2	Hlučínsko
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Prajzska	Prajzska	k?	Prajzska
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
do	do	k7c2	do
konce	konec	k1gInSc2	konec
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
součásti	součást	k1gFnSc2	součást
Pruského	pruský	k2eAgNnSc2d1	pruské
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
po	po	k7c6	po
rozdělení	rozdělení	k1gNnSc6	rozdělení
Slezska	Slezsko	k1gNnSc2	Slezsko
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
katolickému	katolický	k2eAgNnSc3d1	katolické
duchovenstvu	duchovenstvo	k1gNnSc3	duchovenstvo
uchovalo	uchovat	k5eAaPmAgNnS	uchovat
svůj	svůj	k3xOyFgInSc4	svůj
původní	původní	k2eAgInSc4d1	původní
jazyk	jazyk	k1gInSc4	jazyk
"	"	kIx"	"
<g/>
moravštinu	moravština	k1gFnSc4	moravština
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
svou	svůj	k3xOyFgFnSc4	svůj
řeč	řeč	k1gFnSc4	řeč
Hlučíňané	Hlučíňan	k1gMnPc1	Hlučíňan
nazývali	nazývat	k5eAaImAgMnP	nazývat
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
běžný	běžný	k2eAgInSc1d1	běžný
hovorový	hovorový	k2eAgInSc1d1	hovorový
jazyk	jazyk	k1gInSc1	jazyk
(	(	kIx(	(
<g/>
silně	silně	k6eAd1	silně
však	však	k9	však
promíšený	promíšený	k2eAgInSc4d1	promíšený
germanismy	germanismus	k1gInPc4	germanismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
dosavadní	dosavadní	k2eAgFnSc1d1	dosavadní
jednotná	jednotný	k2eAgFnSc1d1	jednotná
česká	český	k2eAgFnSc1d1	Česká
národnost	národnost	k1gFnSc1	národnost
rozložila	rozložit	k5eAaPmAgFnS	rozložit
na	na	k7c4	na
českou	český	k2eAgFnSc4d1	Česká
<g/>
,	,	kIx,	,
moravskou	moravský	k2eAgFnSc4d1	Moravská
a	a	k8xC	a
slezskou	slezský	k2eAgFnSc4d1	Slezská
<g/>
,	,	kIx,	,
respondenti	respondent	k1gMnPc1	respondent
hlásící	hlásící	k2eAgMnSc1d1	hlásící
se	se	k3xPyFc4	se
ke	k	k7c3	k
zvláštní	zvláštní	k2eAgFnSc3d1	zvláštní
moravské	moravský	k2eAgFnSc3d1	Moravská
národnosti	národnost	k1gFnSc3	národnost
uváděli	uvádět	k5eAaImAgMnP	uvádět
jako	jako	k9	jako
svůj	svůj	k3xOyFgInSc4	svůj
mateřský	mateřský	k2eAgInSc4d1	mateřský
jazyk	jazyk	k1gInSc4	jazyk
češtinu	čeština	k1gFnSc4	čeština
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
příležitosti	příležitost	k1gFnSc3	příležitost
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
vydal	vydat	k5eAaPmAgInS	vydat
Český	český	k2eAgInSc1d1	český
statistický	statistický	k2eAgInSc1d1	statistický
úřad	úřad	k1gInSc1	úřad
prohlášení	prohlášení	k1gNnSc2	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
moravský	moravský	k2eAgInSc1d1	moravský
jazyk	jazyk	k1gInSc1	jazyk
sčítán	sčítán	k2eAgInSc1d1	sčítán
jako	jako	k8xS	jako
plnohodnotný	plnohodnotný	k2eAgInSc1d1	plnohodnotný
údaj	údaj	k1gInSc1	údaj
s	s	k7c7	s
vlastním	vlastní	k2eAgInSc7d1	vlastní
číselným	číselný	k2eAgInSc7d1	číselný
kódem	kód	k1gInSc7	kód
odděleně	odděleně	k6eAd1	odděleně
od	od	k7c2	od
jazyka	jazyk	k1gInSc2	jazyk
českého	český	k2eAgInSc2d1	český
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
snahy	snaha	k1gFnPc1	snaha
některých	některý	k3yIgMnPc2	některý
moravistů	moravista	k1gMnPc2	moravista
o	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
zvláštního	zvláštní	k2eAgInSc2d1	zvláštní
moravského	moravský	k2eAgInSc2d1	moravský
spisovného	spisovný	k2eAgInSc2d1	spisovný
jazyka	jazyk	k1gInSc2	jazyk
z	z	k7c2	z
moravských	moravský	k2eAgNnPc2d1	Moravské
nářečí	nářečí	k1gNnPc2	nářečí
<g/>
.	.	kIx.	.
</s>
<s>
Mluvnice	mluvnice	k1gFnSc1	mluvnice
tohoto	tento	k3xDgInSc2	tento
jazyka	jazyk	k1gInSc2	jazyk
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
nazývat	nazývat	k5eAaImF	nazývat
"	"	kIx"	"
<g/>
moravopis	moravopis	k1gInSc1	moravopis
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
projekt	projekt	k1gInSc1	projekt
je	být	k5eAaImIp3nS	být
jazykovědci	jazykovědec	k1gMnSc3	jazykovědec
z	z	k7c2	z
Filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
vnímán	vnímán	k2eAgMnSc1d1	vnímán
jako	jako	k8xS	jako
kontroverzní	kontroverzní	k2eAgMnSc1d1	kontroverzní
a	a	k8xC	a
politicky	politicky	k6eAd1	politicky
podbarvený	podbarvený	k2eAgInSc1d1	podbarvený
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
moravisté	moravista	k1gMnPc1	moravista
totiž	totiž	k9	totiž
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Morava	Morava	k1gFnSc1	Morava
se	se	k3xPyFc4	se
dočká	dočkat	k5eAaPmIp3nS	dočkat
nezávislosti	nezávislost	k1gFnSc2	nezávislost
jedině	jedině	k6eAd1	jedině
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
bude	být	k5eAaImBp3nS	být
stát	stát	k5eAaPmF	stát
na	na	k7c6	na
národním	národní	k2eAgInSc6d1	národní
principu	princip	k1gInSc6	princip
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
vytvořit	vytvořit	k5eAaPmF	vytvořit
moravský	moravský	k2eAgInSc4d1	moravský
jazyk	jazyk	k1gInSc4	jazyk
jako	jako	k8xS	jako
nejdůležitější	důležitý	k2eAgInSc4d3	nejdůležitější
znak	znak	k1gInSc4	znak
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
BLÁHA	Bláha	k1gMnSc1	Bláha
<g/>
,	,	kIx,	,
Ondřej	Ondřej	k1gMnSc1	Ondřej
<g/>
.	.	kIx.	.
</s>
<s>
Moravský	moravský	k2eAgInSc1d1	moravský
jazykový	jazykový	k2eAgInSc1d1	jazykový
separatismus	separatismus	k1gInSc1	separatismus
<g/>
:	:	kIx,	:
zdroje	zdroj	k1gInPc1	zdroj
<g/>
,	,	kIx,	,
cíle	cíl	k1gInPc1	cíl
<g/>
,	,	kIx,	,
slovanský	slovanský	k2eAgInSc1d1	slovanský
kontext	kontext	k1gInSc1	kontext
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
Studia	studio	k1gNnSc2	studio
Moravica	Moravicus	k1gMnSc2	Moravicus
<g/>
.	.	kIx.	.
</s>
<s>
Acta	Act	k2eAgFnSc1d1	Acta
Universitatis	Universitatis	k1gFnSc1	Universitatis
Palackianae	Palackiana	k1gFnSc2	Palackiana
Olomucensis	Olomucensis	k1gFnPc2	Olomucensis
Facultas	Facultas	k1gMnSc1	Facultas
Philosophica	Philosophica	k1gMnSc1	Philosophica
-	-	kIx~	-
Moravica	Moravica	k1gMnSc1	Moravica
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
:	:	kIx,	:
UP	UP	kA	UP
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1801	[number]	k4	1801
<g/>
-	-	kIx~	-
<g/>
7061	[number]	k4	7061
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
s.	s.	k?	s.
293	[number]	k4	293
<g/>
–	–	k?	–
<g/>
299	[number]	k4	299
<g/>
.	.	kIx.	.
</s>
<s>
STICH	STICH	kA	STICH
<g/>
,	,	kIx,	,
Alexandr	Alexandr	k1gMnSc1	Alexandr
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
spisovné	spisovný	k2eAgFnSc6d1	spisovná
moravštině	moravština	k1gFnSc6	moravština
a	a	k8xC	a
jiných	jiný	k2eAgInPc6d1	jiný
"	"	kIx"	"
<g/>
malých	malý	k2eAgInPc6d1	malý
<g/>
"	"	kIx"	"
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Naše	náš	k3xOp1gFnSc1	náš
řeč	řeč	k1gFnSc1	řeč
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
83	[number]	k4	83
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
,	,	kIx,	,
s.	s.	k?	s.
260	[number]	k4	260
<g/>
–	–	k?	–
<g/>
264	[number]	k4	264
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
27	[number]	k4	27
<g/>
-	-	kIx~	-
<g/>
8203	[number]	k4	8203
<g/>
.	.	kIx.	.
</s>
<s>
Moravská	moravský	k2eAgNnPc1d1	Moravské
a	a	k8xC	a
slezská	slezský	k2eAgNnPc1d1	Slezské
nářečí	nářečí	k1gNnPc1	nářečí
na	na	k7c6	na
mapě	mapa	k1gFnSc6	mapa
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
jazyk	jazyk	k1gInSc4	jazyk
český	český	k2eAgInSc4d1	český
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Otázka	otázka	k1gFnSc1	otázka
kodifikace	kodifikace	k1gFnSc2	kodifikace
spisovného	spisovný	k2eAgInSc2d1	spisovný
moravského	moravský	k2eAgInSc2d1	moravský
jazyka	jazyk	k1gInSc2	jazyk
–	–	k?	–
návrh	návrh	k1gInSc1	návrh
slovenského	slovenský	k2eAgMnSc2d1	slovenský
publicisty	publicista	k1gMnSc2	publicista
Z.	Z.	kA	Z.
Šustka	Šustek	k1gMnSc2	Šustek
na	na	k7c6	na
vytvoření	vytvoření	k1gNnSc6	vytvoření
spisovné	spisovný	k2eAgFnSc2d1	spisovná
moravštiny	moravština	k1gFnSc2	moravština
(	(	kIx(	(
<g/>
článek	článek	k1gInSc1	článek
v	v	k7c6	v
estonském	estonský	k2eAgInSc6d1	estonský
sborníku	sborník	k1gInSc6	sborník
Slavica	slavicum	k1gNnSc2	slavicum
Tartuensia	Tartuensius	k1gMnSc2	Tartuensius
'	'	kIx"	'
<g/>
98	[number]	k4	98
<g/>
)	)	kIx)	)
O	o	k7c6	o
Moravě	Morava	k1gFnSc6	Morava
pro	pro	k7c4	pro
Moravany	Moravan	k1gMnPc4	Moravan
–	–	k?	–
stránky	stránka	k1gFnSc2	stránka
R.	R.	kA	R.
Svobody	Svoboda	k1gMnSc2	Svoboda
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgMnPc2d1	hlavní
iniciátorů	iniciátor	k1gMnPc2	iniciátor
vytváření	vytváření	k1gNnSc2	vytváření
nového	nový	k2eAgInSc2d1	nový
spisovného	spisovný	k2eAgInSc2d1	spisovný
jazyka	jazyk	k1gInSc2	jazyk
Ústav	ústav	k1gInSc1	ústav
jazyka	jazyk	k1gInSc2	jazyk
moravského	moravský	k2eAgInSc2d1	moravský
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
o.	o.	k?	o.
s.	s.	k?	s.
–	–	k?	–
sdružení	sdružení	k1gNnSc1	sdružení
3	[number]	k4	3
lidí	člověk	k1gMnPc2	člověk
usilující	usilující	k2eAgMnSc1d1	usilující
o	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
spisovné	spisovný	k2eAgFnSc2d1	spisovná
moravštiny	moravština	k1gFnSc2	moravština
(	(	kIx(	(
<g/>
prosazují	prosazovat	k5eAaImIp3nP	prosazovat
Svobodovy	Svobodův	k2eAgInPc1d1	Svobodův
vývody	vývod	k1gInPc1	vývod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
"	"	kIx"	"
<g/>
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
veledůležitým	veledůležitý	k2eAgInSc7d1	veledůležitý
prvkem	prvek	k1gInSc7	prvek
pro	pro	k7c4	pro
zachování	zachování	k1gNnSc4	zachování
moravské	moravský	k2eAgFnSc2d1	Moravská
národní	národní	k2eAgFnSc2d1	národní
identity	identita	k1gFnSc2	identita
občanů	občan	k1gMnPc2	občan
Moravy	Morava	k1gFnSc2	Morava
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
"	"	kIx"	"
Moravané	Moravan	k1gMnPc1	Moravan
tvoří	tvořit	k5eAaImIp3nP	tvořit
spisovnou	spisovný	k2eAgFnSc4d1	spisovná
moravštinu	moravština	k1gFnSc4	moravština
–	–	k?	–
článek	článek	k1gInSc1	článek
z	z	k7c2	z
listopadu	listopad	k1gInSc2	listopad
2008	[number]	k4	2008
v	v	k7c6	v
Brněnském	brněnský	k2eAgInSc6d1	brněnský
<g />
.	.	kIx.	.
</s>
<s>
deníku	deník	k1gInSc2	deník
<g/>
;	;	kIx,	;
referuje	referovat	k5eAaBmIp3nS	referovat
o	o	k7c6	o
snahách	snaha	k1gFnPc6	snaha
ÚJM	ÚJM	kA	ÚJM
Chronologie	chronologie	k1gFnSc1	chronologie
moravštiny	moravština	k1gFnSc2	moravština
–	–	k?	–
dějiny	dějiny	k1gFnPc4	dějiny
jazyka	jazyk	k1gInSc2	jazyk
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
Moravského	moravský	k2eAgInSc2d1	moravský
národního	národní	k2eAgInSc2d1	národní
kongresu	kongres	k1gInSc2	kongres
Morčevník	Morčevník	k1gInSc1	Morčevník
aneb	aneb	k?	aneb
1	[number]	k4	1
<g/>
.	.	kIx.	.
internetový	internetový	k2eAgInSc1d1	internetový
slovník	slovník	k1gInSc1	slovník
moravsko-český	moravsko-český	k2eAgInSc1d1	moravsko-český
–	–	k?	–
původně	původně	k6eAd1	původně
recese	recese	k1gFnSc2	recese
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
už	už	k9	už
databáze	databáze	k1gFnSc1	databáze
cca	cca	kA	cca
2500	[number]	k4	2500
slov	slovo	k1gNnPc2	slovo
ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
nářečí	nářečí	k1gNnPc2	nářečí
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
Slezska	Slezsko	k1gNnSc2	Slezsko
Existuje	existovat	k5eAaImIp3nS	existovat
moravský	moravský	k2eAgInSc1d1	moravský
národ	národ	k1gInSc1	národ
<g/>
?	?	kIx.	?
</s>
<s>
–	–	k?	–
záznam	záznam	k1gInSc1	záznam
pořadu	pořad	k1gInSc2	pořad
vysílaného	vysílaný	k2eAgInSc2d1	vysílaný
v	v	k7c6	v
r.	r.	kA	r.
2001	[number]	k4	2001
na	na	k7c6	na
ČRo	ČRo	k1gFnSc6	ČRo
7	[number]	k4	7
Radio	radio	k1gNnSc1	radio
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
diskutuje	diskutovat	k5eAaImIp3nS	diskutovat
se	se	k3xPyFc4	se
i	i	k9	i
otázka	otázka	k1gFnSc1	otázka
jazyková	jazykový	k2eAgFnSc1d1	jazyková
<g/>
)	)	kIx)	)
</s>
