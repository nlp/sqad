<s>
Moravština	moravština	k1gFnSc1	moravština
<g/>
,	,	kIx,	,
též	též	k9	též
moravský	moravský	k2eAgInSc4d1	moravský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
podoby	podoba	k1gFnSc2	podoba
češtiny	čeština	k1gFnSc2	čeština
užívané	užívaný	k2eAgFnSc2d1	užívaná
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
