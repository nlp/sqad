<s>
Parlament	parlament	k1gInSc1	parlament
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
opět	opět	k6eAd1	opět
dvoukomorový	dvoukomorový	k2eAgInSc1d1	dvoukomorový
<g/>
:	:	kIx,	:
tvoří	tvořit	k5eAaImIp3nS	tvořit
jej	on	k3xPp3gInSc4	on
Sejm	Sejm	k1gInSc4	Sejm
(	(	kIx(	(
<g/>
460	[number]	k4	460
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
)	)	kIx)	)
a	a	k8xC	a
Senát	senát	k1gInSc1	senát
(	(	kIx(	(
<g/>
100	[number]	k4	100
senátorů	senátor	k1gMnPc2	senátor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
