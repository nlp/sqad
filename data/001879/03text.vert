<s>
Bakterie	bakterie	k1gFnPc1	bakterie
(	(	kIx(	(
<g/>
Bacteria	Bacterium	k1gNnPc1	Bacterium
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
též	též	k9	též
Bacteriophyta	Bacteriophyta	k1gMnSc1	Bacteriophyta
či	či	k8xC	či
Schizomycetes	Schizomycetes	k1gMnSc1	Schizomycetes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
také	také	k9	také
eubakterie	eubakterie	k1gFnSc2	eubakterie
(	(	kIx(	(
<g/>
Eubacteria	Eubacterium	k1gNnSc2	Eubacterium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
doména	doména	k1gFnSc1	doména
jednobuněčných	jednobuněčný	k2eAgInPc2d1	jednobuněčný
prokaryotických	prokaryotický	k2eAgInPc2d1	prokaryotický
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Mívají	mívat	k5eAaImIp3nP	mívat
kokovitý	kokovitý	k2eAgInSc4d1	kokovitý
či	či	k8xC	či
tyčinkovitý	tyčinkovitý	k2eAgInSc4d1	tyčinkovitý
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
zpravidla	zpravidla	k6eAd1	zpravidla
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
velikosti	velikost	k1gFnPc4	velikost
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
několika	několik	k4yIc2	několik
mikrometrů	mikrometr	k1gInPc2	mikrometr
<g/>
.	.	kIx.	.
</s>
<s>
Studiem	studio	k1gNnSc7	studio
bakterií	bakterie	k1gFnPc2	bakterie
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
bakteriologie	bakteriologie	k1gFnSc1	bakteriologie
<g/>
,	,	kIx,	,
významně	významně	k6eAd1	významně
tuto	tento	k3xDgFnSc4	tento
vědu	věda	k1gFnSc4	věda
rozvinuli	rozvinout	k5eAaPmAgMnP	rozvinout
Robert	Robert	k1gMnSc1	Robert
Koch	Koch	k1gMnSc1	Koch
a	a	k8xC	a
Louis	Louis	k1gMnSc1	Louis
Pasteur	Pasteur	k1gMnSc1	Pasteur
<g/>
.	.	kIx.	.
</s>
<s>
Typickou	typický	k2eAgFnSc7d1	typická
součástí	součást	k1gFnSc7	součást
bakteriálních	bakteriální	k2eAgFnPc2d1	bakteriální
buněk	buňka	k1gFnPc2	buňka
je	být	k5eAaImIp3nS	být
peptidoglykanová	peptidoglykanový	k2eAgFnSc1d1	peptidoglykanový
buněčná	buněčný	k2eAgFnSc1d1	buněčná
stěna	stěna	k1gFnSc1	stěna
<g/>
,	,	kIx,	,
jaderná	jaderný	k2eAgFnSc1d1	jaderná
oblast	oblast	k1gFnSc1	oblast
(	(	kIx(	(
<g/>
nukleoid	nukleoid	k1gInSc1	nukleoid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
DNA	DNA	kA	DNA
bez	bez	k7c2	bez
intronů	intron	k1gInPc2	intron
<g/>
,	,	kIx,	,
plazmidy	plazmid	k1gInPc4	plazmid
a	a	k8xC	a
prokaryotický	prokaryotický	k2eAgInSc4d1	prokaryotický
typ	typ	k1gInSc4	typ
ribozomů	ribozom	k1gInPc2	ribozom
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
bakterií	bakterie	k1gFnPc2	bakterie
se	se	k3xPyFc4	se
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
pohlavní	pohlavní	k2eAgNnSc1d1	pohlavní
rozmnožování	rozmnožování	k1gNnSc1	rozmnožování
<g/>
,	,	kIx,	,
namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
dělí	dělit	k5eAaImIp3nS	dělit
binárně	binárně	k6eAd1	binárně
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnPc1	bakterie
jsou	být	k5eAaImIp3nP	být
nejrozšířenější	rozšířený	k2eAgFnSc7d3	nejrozšířenější
skupinou	skupina	k1gFnSc7	skupina
organismů	organismus	k1gInPc2	organismus
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
druhy	druh	k1gInPc1	druh
bakterií	bakterie	k1gFnPc2	bakterie
klasifikovaly	klasifikovat	k5eAaImAgInP	klasifikovat
podle	podle	k7c2	podle
vnějšího	vnější	k2eAgInSc2d1	vnější
vzhledu	vzhled	k1gInSc2	vzhled
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
moderní	moderní	k2eAgInPc1d1	moderní
zejména	zejména	k9	zejména
genetické	genetický	k2eAgFnPc4d1	genetická
metody	metoda	k1gFnPc4	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
nim	on	k3xPp3gFnPc3	on
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
asi	asi	k9	asi
25	[number]	k4	25
základních	základní	k2eAgInPc2d1	základní
kmenů	kmen	k1gInPc2	kmen
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnPc1	bakterie
mají	mít	k5eAaImIp3nP	mít
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
v	v	k7c6	v
planetárním	planetární	k2eAgInSc6d1	planetární
oběhu	oběh	k1gInSc6	oběh
živin	živina	k1gFnPc2	živina
a	a	k8xC	a
mnohdy	mnohdy	k6eAd1	mnohdy
vstupují	vstupovat	k5eAaImIp3nP	vstupovat
do	do	k7c2	do
oboustranně	oboustranně	k6eAd1	oboustranně
prospěšných	prospěšný	k2eAgInPc2d1	prospěšný
svazků	svazek	k1gInPc2	svazek
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
organismy	organismus	k1gInPc7	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgFnPc1d1	mnohá
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
komenzálické	komenzálický	k2eAgInPc4d1	komenzálický
druhy	druh	k1gInPc4	druh
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
žijí	žít	k5eAaImIp3nP	žít
například	například	k6eAd1	například
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
trávicí	trávicí	k2eAgFnSc6d1	trávicí
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
i	i	k9	i
mnoho	mnoho	k4c1	mnoho
patogenních	patogenní	k2eAgFnPc2d1	patogenní
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
infekce	infekce	k1gFnPc4	infekce
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k8xC	i
člověk	člověk	k1gMnSc1	člověk
mnohé	mnohé	k1gNnSc4	mnohé
z	z	k7c2	z
bakterií	bakterie	k1gFnPc2	bakterie
využívá	využívat	k5eAaImIp3nS	využívat
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
potravinářském	potravinářský	k2eAgInSc6d1	potravinářský
a	a	k8xC	a
chemickém	chemický	k2eAgInSc6d1	chemický
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
využívají	využívat	k5eAaPmIp3nP	využívat
bakterie	bakterie	k1gFnPc4	bakterie
ve	v	k7c6	v
výzkumu	výzkum	k1gInSc6	výzkum
a	a	k8xC	a
samotné	samotný	k2eAgFnPc1d1	samotná
bakterie	bakterie	k1gFnPc1	bakterie
jsou	být	k5eAaImIp3nP	být
předmětem	předmět	k1gInSc7	předmět
bádání	bádání	k1gNnSc2	bádání
bakteriologie	bakteriologie	k1gFnSc2	bakteriologie
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnSc1	bakterie
poprvé	poprvé	k6eAd1	poprvé
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
roku	rok	k1gInSc2	rok
1676	[number]	k4	1676
nizozemský	nizozemský	k2eAgInSc1d1	nizozemský
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
Antoni	Anton	k1gMnPc1	Anton
van	vana	k1gFnPc2	vana
Leeuwenhoek	Leeuwenhoek	k6eAd1	Leeuwenhoek
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
mikroskopem	mikroskop	k1gInSc7	mikroskop
vlastní	vlastní	k2eAgFnSc2d1	vlastní
výroby	výroba	k1gFnSc2	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
bacterium	bacterium	k1gNnSc4	bacterium
zavedl	zavést	k5eAaPmAgMnS	zavést
až	až	k9	až
Christian	Christian	k1gMnSc1	Christian
Gottfried	Gottfried	k1gMnSc1	Gottfried
Ehrenberg	Ehrenberg	k1gMnSc1	Ehrenberg
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1838	[number]	k4	1838
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
slova	slovo	k1gNnSc2	slovo
bacterion	bacterion	k1gInSc1	bacterion
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
znamená	znamenat	k5eAaImIp3nS	znamenat
malý	malý	k2eAgInSc1d1	malý
klacek	klacek	k1gInSc1	klacek
či	či	k8xC	či
tyčka	tyčka	k1gFnSc1	tyčka
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc2	první
pozorované	pozorovaný	k2eAgFnSc2d1	pozorovaná
bakterie	bakterie	k1gFnSc2	bakterie
byly	být	k5eAaImAgFnP	být
tyčinky	tyčinka	k1gFnPc1	tyčinka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1859	[number]	k4	1859
Louis	Louis	k1gMnSc1	Louis
Pasteur	Pasteur	k1gMnSc1	Pasteur
dokázal	dokázat	k5eAaPmAgMnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kvašení	kvašení	k1gNnSc4	kvašení
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
bakterie	bakterie	k1gFnSc1	bakterie
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
bakterie	bakterie	k1gFnPc1	bakterie
nevznikají	vznikat	k5eNaImIp3nP	vznikat
spontánně	spontánně	k6eAd1	spontánně
z	z	k7c2	z
neživé	živý	k2eNgFnSc2d1	neživá
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Pasteur	Pasteur	k1gMnSc1	Pasteur
také	také	k9	také
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
mikroorganismy	mikroorganismus	k1gInPc1	mikroorganismus
včetně	včetně	k7c2	včetně
bakterií	bakterie	k1gFnPc2	bakterie
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
nemoci	nemoc	k1gFnPc4	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
Koch	Koch	k1gMnSc1	Koch
byl	být	k5eAaImAgMnS	být
průkopníkem	průkopník	k1gMnSc7	průkopník
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
lékařské	lékařský	k2eAgFnSc2d1	lékařská
mikrobiologie	mikrobiologie	k1gFnSc2	mikrobiologie
a	a	k8xC	a
studoval	studovat	k5eAaImAgMnS	studovat
původce	původce	k1gMnSc1	původce
cholery	cholera	k1gFnSc2	cholera
<g/>
,	,	kIx,	,
TBC	TBC	kA	TBC
a	a	k8xC	a
anthrax	anthrax	k1gInSc1	anthrax
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výzkumu	výzkum	k1gInSc6	výzkum
TBC	TBC	kA	TBC
s	s	k7c7	s
konečnou	konečný	k2eAgFnSc7d1	konečná
platností	platnost	k1gFnSc7	platnost
dokázal	dokázat	k5eAaPmAgInS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bakterie	bakterie	k1gFnPc4	bakterie
jsou	být	k5eAaImIp3nP	být
původci	původce	k1gMnPc1	původce
této	tento	k3xDgFnSc2	tento
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
dostal	dostat	k5eAaPmAgMnS	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Takzvané	takzvaný	k2eAgInPc1d1	takzvaný
Kochovy	Kochův	k2eAgInPc1d1	Kochův
postuláty	postulát	k1gInPc1	postulát
jsou	být	k5eAaImIp3nP	být
výčtem	výčet	k1gInSc7	výčet
čtyř	čtyři	k4xCgInPc2	čtyři
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
nutná	nutný	k2eAgNnPc1d1	nutné
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
daný	daný	k2eAgInSc1d1	daný
patogen	patogen	k1gInSc1	patogen
uznán	uznat	k5eAaPmNgInS	uznat
za	za	k7c4	za
původce	původce	k1gMnPc4	původce
určité	určitý	k2eAgFnSc2d1	určitá
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
devatenáctém	devatenáctý	k4xOgInSc6	devatenáctý
století	století	k1gNnSc6	století
již	již	k9	již
bylo	být	k5eAaImAgNnS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
mnohé	mnohý	k2eAgFnPc1d1	mnohá
bakterie	bakterie	k1gFnPc1	bakterie
patogenní	patogenní	k2eAgFnPc1d1	patogenní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebyly	být	k5eNaImAgInP	být
známy	znám	k2eAgInPc1d1	znám
účinné	účinný	k2eAgInPc1d1	účinný
antibakteriální	antibakteriální	k2eAgInPc1d1	antibakteriální
léky	lék	k1gInPc1	lék
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
však	však	k8xC	však
Paul	Paul	k1gMnSc1	Paul
Ehrlich	Ehrlich	k1gMnSc1	Ehrlich
vyvinul	vyvinout	k5eAaPmAgMnS	vyvinout
první	první	k4xOgNnSc4	první
chemoterapeutikum	chemoterapeutikum	k1gNnSc4	chemoterapeutikum
proti	proti	k7c3	proti
bakterii	bakterie	k1gFnSc3	bakterie
Treponema	Treponemum	k1gNnSc2	Treponemum
pallidum	pallidum	k1gInSc4	pallidum
(	(	kIx(	(
<g/>
původce	původce	k1gMnSc2	původce
syfilis	syfilis	k1gFnSc1	syfilis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
díky	díky	k7c3	díky
záměně	záměna	k1gFnSc3	záměna
běžně	běžně	k6eAd1	běžně
používaných	používaný	k2eAgNnPc2d1	používané
laboratorních	laboratorní	k2eAgNnPc2d1	laboratorní
barviv	barvivo	k1gNnPc2	barvivo
za	za	k7c4	za
sloučeninu	sloučenina	k1gFnSc4	sloučenina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
selektivně	selektivně	k6eAd1	selektivně
zabíjela	zabíjet	k5eAaImAgFnS	zabíjet
bakterie	bakterie	k1gFnSc1	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
se	se	k3xPyFc4	se
však	však	k9	však
za	za	k7c4	za
první	první	k4xOgFnSc4	první
systémově	systémově	k6eAd1	systémově
použitelné	použitelný	k2eAgNnSc4d1	použitelné
antibiotikum	antibiotikum	k1gNnSc4	antibiotikum
uvádí	uvádět	k5eAaImIp3nS	uvádět
penicilin	penicilin	k1gInSc1	penicilin
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
účinků	účinek	k1gInPc2	účinek
si	se	k3xPyFc3	se
všiml	všimnout	k5eAaPmAgMnS	všimnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
Alexander	Alexandra	k1gFnPc2	Alexandra
Fleming	Fleming	k1gInSc1	Fleming
<g/>
.	.	kIx.	.
</s>
<s>
Gramovo	Gramův	k2eAgNnSc1d1	Gramovo
barvení	barvení	k1gNnSc1	barvení
<g/>
,	,	kIx,	,
metoda	metoda	k1gFnSc1	metoda
k	k	k7c3	k
rychlé	rychlý	k2eAgFnSc3d1	rychlá
klasifikaci	klasifikace	k1gFnSc3	klasifikace
bakterií	bakterie	k1gFnPc2	bakterie
do	do	k7c2	do
několika	několik	k4yIc2	několik
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
vyvinuto	vyvinout	k5eAaPmNgNnS	vyvinout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
Hansem	Hans	k1gMnSc7	Hans
Christianem	Christian	k1gMnSc7	Christian
Gramem	gram	k1gInSc7	gram
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vývojem	vývoj	k1gInSc7	vývoj
bakteriologie	bakteriologie	k1gFnSc2	bakteriologie
se	se	k3xPyFc4	se
vyvíjelo	vyvíjet	k5eAaImAgNnS	vyvíjet
i	i	k9	i
studium	studium	k1gNnSc1	studium
systematiky	systematika	k1gFnSc2	systematika
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starším	starý	k2eAgNnSc6d2	starší
pojetí	pojetí	k1gNnSc6	pojetí
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
říše	říše	k1gFnSc1	říše
bakterie	bakterie	k1gFnSc1	bakterie
(	(	kIx(	(
<g/>
Monera	Monera	k1gFnSc1	Monera
<g/>
)	)	kIx)	)
všechny	všechen	k3xTgInPc1	všechen
prokaryotní	prokaryotní	k2eAgInPc1d1	prokaryotní
organismy	organismus	k1gInPc1	organismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
Carl	Carl	k1gInSc4	Carl
Woese	Woese	k1gFnSc2	Woese
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Archaea	Archaea	k1gFnSc1	Archaea
mají	mít	k5eAaImIp3nP	mít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
vývojovou	vývojový	k2eAgFnSc4d1	vývojová
linii	linie	k1gFnSc4	linie
<g/>
,	,	kIx,	,
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
od	od	k7c2	od
linie	linie	k1gFnSc2	linie
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplývající	vyplývající	k2eAgFnSc1d1	vyplývající
taxonomie	taxonomie	k1gFnSc1	taxonomie
byla	být	k5eAaImAgFnS	být
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c4	na
sekvenaci	sekvenace	k1gFnSc4	sekvenace
určitého	určitý	k2eAgInSc2d1	určitý
úseku	úsek	k1gInSc2	úsek
rRNA	rRNA	k?	rRNA
<g/>
,	,	kIx,	,
a	a	k8xC	a
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
prokaryotické	prokaryotický	k2eAgInPc4d1	prokaryotický
organismy	organismus	k1gInPc4	organismus
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
samostatné	samostatný	k2eAgFnPc4d1	samostatná
domény	doména	k1gFnPc4	doména
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnPc1	bakterie
jsou	být	k5eAaImIp3nP	být
nejrozšířenější	rozšířený	k2eAgFnSc7d3	nejrozšířenější
skupinou	skupina	k1gFnSc7	skupina
organismů	organismus	k1gInPc2	organismus
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
žije	žít	k5eAaImIp3nS	žít
asi	asi	k9	asi
5	[number]	k4	5
<g/>
×	×	k?	×
<g/>
1030	[number]	k4	1030
(	(	kIx(	(
<g/>
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
)	)	kIx)	)
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
jen	jen	k9	jen
tušit	tušit	k5eAaImF	tušit
<g/>
,	,	kIx,	,
odhady	odhad	k1gInPc1	odhad
sahají	sahat	k5eAaImIp3nP	sahat
od	od	k7c2	od
107	[number]	k4	107
k	k	k7c3	k
109	[number]	k4	109
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnPc4	bakterie
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
nalézt	nalézt	k5eAaBmF	nalézt
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
<g/>
,	,	kIx,	,
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
ovzduší	ovzduší	k1gNnSc6	ovzduší
i	i	k8xC	i
jakožto	jakožto	k8xS	jakožto
symbionty	symbiont	k1gInPc4	symbiont
uvnitř	uvnitř	k6eAd1	uvnitř
a	a	k8xC	a
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
mnohobuněčných	mnohobuněčný	k2eAgInPc2d1	mnohobuněčný
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
gramu	gram	k1gInSc6	gram
půdy	půda	k1gFnSc2	půda
žije	žít	k5eAaImIp3nS	žít
asi	asi	k9	asi
40	[number]	k4	40
miliónů	milión	k4xCgInPc2	milión
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
mililitru	mililitr	k1gInSc6	mililitr
sladké	sladký	k2eAgFnSc2d1	sladká
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gNnPc2	on
přibližně	přibližně	k6eAd1	přibližně
milion	milion	k4xCgInSc1	milion
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
však	však	k9	však
známy	znám	k2eAgInPc1d1	znám
i	i	k8xC	i
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
specializují	specializovat	k5eAaBmIp3nP	specializovat
na	na	k7c6	na
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nP	by
ostatní	ostatní	k2eAgInPc1d1	ostatní
organismy	organismus	k1gInPc1	organismus
mohly	moct	k5eAaImAgInP	moct
přežívat	přežívat	k5eAaImF	přežívat
jen	jen	k9	jen
stěží	stěží	k6eAd1	stěží
(	(	kIx(	(
<g/>
vroucí	vroucí	k2eAgFnSc1d1	vroucí
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
sopečných	sopečný	k2eAgInPc6d1	sopečný
jezerech	jezero	k1gNnPc6	jezero
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
vrstvy	vrstva	k1gFnSc2	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
bakterií	bakterie	k1gFnPc2	bakterie
jsou	být	k5eAaImIp3nP	být
dle	dle	k7c2	dle
výzkumů	výzkum	k1gInPc2	výzkum
schopny	schopen	k2eAgFnPc1d1	schopna
přežít	přežít	k5eAaPmF	přežít
i	i	k9	i
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
a	a	k8xC	a
o	o	k7c6	o
teplotě	teplota	k1gFnSc6	teplota
-	-	kIx~	-
°	°	k?	°
<g/>
C.	C.	kA	C.
Bakterie	bakterie	k1gFnSc1	bakterie
však	však	k9	však
mají	mít	k5eAaImIp3nP	mít
různé	různý	k2eAgInPc4d1	různý
požadavky	požadavek	k1gInPc4	požadavek
na	na	k7c4	na
prostředí	prostředí	k1gNnSc4	prostředí
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
žijí	žít	k5eAaImIp3nP	žít
<g/>
,	,	kIx,	,
významným	významný	k2eAgNnSc7d1	významné
hlediskem	hledisko	k1gNnSc7	hledisko
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
kyselost	kyselost	k1gFnSc1	kyselost
a	a	k8xC	a
množství	množství	k1gNnSc1	množství
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
teplotního	teplotní	k2eAgNnSc2d1	teplotní
optima	optimum	k1gNnSc2	optimum
se	se	k3xPyFc4	se
bakterie	bakterie	k1gFnSc1	bakterie
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
psychrofilní	psychrofilní	k2eAgFnPc4d1	psychrofilní
(	(	kIx(	(
<g/>
do	do	k7c2	do
20	[number]	k4	20
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mezofilní	mezofilní	k2eAgFnSc1d1	mezofilní
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
40	[number]	k4	40
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
termofilní	termofilní	k2eAgFnSc1d1	termofilní
(	(	kIx(	(
<g/>
40	[number]	k4	40
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
více	hodně	k6eAd2	hodně
<g/>
)	)	kIx)	)
a	a	k8xC	a
případně	případně	k6eAd1	případně
též	též	k9	též
hypertermofilní	hypertermofilní	k2eAgFnSc1d1	hypertermofilní
s	s	k7c7	s
optimem	optimum	k1gNnSc7	optimum
kolem	kolem	k7c2	kolem
80	[number]	k4	80
°	°	k?	°
<g/>
C.	C.	kA	C.
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
kyselosti	kyselost	k1gFnSc2	kyselost
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
alkalofilní	alkalofilní	k2eAgNnPc1d1	alkalofilní
(	(	kIx(	(
<g/>
v	v	k7c6	v
zásaditém	zásaditý	k2eAgNnSc6d1	zásadité
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neutrofilní	neutrofilní	k2eAgFnSc1d1	neutrofilní
(	(	kIx(	(
<g/>
v	v	k7c6	v
±	±	k?	±
neutrálním	neutrální	k2eAgNnSc6d1	neutrální
prostředí	prostředí	k1gNnSc6	prostředí
s	s	k7c7	s
pH	ph	kA	ph
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
či	či	k8xC	či
acidofilní	acidofilní	k2eAgMnPc1d1	acidofilní
(	(	kIx(	(
<g/>
v	v	k7c6	v
kyselém	kyselý	k2eAgNnSc6d1	kyselé
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
významným	významný	k2eAgNnSc7d1	významné
hlediskem	hledisko	k1gNnSc7	hledisko
je	být	k5eAaImIp3nS	být
vztah	vztah	k1gInSc4	vztah
ke	k	k7c3	k
kyslíku	kyslík	k1gInSc3	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Aerobní	aerobní	k2eAgFnSc1d1	aerobní
bakterie	bakterie	k1gFnSc1	bakterie
(	(	kIx(	(
<g/>
př	př	kA	př
<g/>
.	.	kIx.	.
Mycobacterium	Mycobacterium	k1gNnSc4	Mycobacterium
<g/>
)	)	kIx)	)
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
kyslík	kyslík	k1gInSc4	kyslík
v	v	k7c6	v
atmosférické	atmosférický	k2eAgFnSc6d1	atmosférická
koncentraci	koncentrace	k1gFnSc6	koncentrace
<g/>
,	,	kIx,	,
mikroaerofilní	mikroaerofilní	k2eAgMnSc1d1	mikroaerofilní
(	(	kIx(	(
<g/>
Lactobacillus	Lactobacillus	k1gMnSc1	Lactobacillus
<g/>
)	)	kIx)	)
v	v	k7c6	v
koncentraci	koncentrace	k1gFnSc6	koncentrace
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgInPc1d1	nízký
(	(	kIx(	(
<g/>
cca	cca	kA	cca
2	[number]	k4	2
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
bakterií	bakterie	k1gFnPc2	bakterie
však	však	k9	však
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
fakultativně	fakultativně	k6eAd1	fakultativně
anaerobní	anaerobní	k2eAgFnPc4d1	anaerobní
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
rostou	růst	k5eAaImIp3nP	růst
lépe	dobře	k6eAd2	dobře
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dokáží	dokázat	k5eAaPmIp3nP	dokázat
růst	růst	k1gInSc4	růst
i	i	k9	i
bez	bez	k7c2	bez
něho	on	k3xPp3gMnSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
tohoto	tento	k3xDgNnSc2	tento
spektra	spektrum	k1gNnSc2	spektrum
jsou	být	k5eAaImIp3nP	být
striktně	striktně	k6eAd1	striktně
anaerobní	anaerobní	k2eAgInPc1d1	anaerobní
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
žijí	žít	k5eAaImIp3nP	žít
jen	jen	k9	jen
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
bez	bez	k7c2	bez
kyslíku	kyslík	k1gInSc2	kyslík
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
druhů	druh	k1gInPc2	druh
rodu	rod	k1gInSc2	rod
Clostridium	Clostridium	k1gNnSc1	Clostridium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgMnPc2d1	další
limitujících	limitující	k2eAgMnPc2d1	limitující
činitelů	činitel	k1gMnPc2	činitel
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
zmínit	zmínit	k5eAaPmF	zmínit
vlhkost	vlhkost	k1gFnSc1	vlhkost
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
je	být	k5eAaImIp3nS	být
vlhkomilných	vlhkomilný	k2eAgFnPc6d1	vlhkomilná
<g/>
,	,	kIx,	,
suchomilné	suchomilný	k2eAgInPc1d1	suchomilný
jsou	být	k5eAaImIp3nP	být
nokardie	nokardie	k1gFnPc1	nokardie
či	či	k8xC	či
aktinomycety	aktinomyceta	k1gFnPc1	aktinomyceta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hydrostatický	hydrostatický	k2eAgInSc1d1	hydrostatický
tlak	tlak	k1gInSc1	tlak
(	(	kIx(	(
<g/>
z	z	k7c2	z
hlubokých	hluboký	k2eAgNnPc2d1	hluboké
moří	moře	k1gNnPc2	moře
známe	znát	k5eAaImIp1nP	znát
i	i	k9	i
barofilní	barofilní	k2eAgFnPc1d1	barofilní
bakterie	bakterie	k1gFnPc1	bakterie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
osmotický	osmotický	k2eAgInSc1d1	osmotický
tlak	tlak	k1gInSc1	tlak
(	(	kIx(	(
<g/>
především	především	k9	především
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c4	na
množství	množství	k1gNnSc4	množství
solí	sůl	k1gFnPc2	sůl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Navenek	navenek	k6eAd1	navenek
je	být	k5eAaImIp3nS	být
nejnápadnějším	nápadní	k2eAgInSc7d3	nápadní
rysem	rys	k1gInSc7	rys
bakterií	bakterie	k1gFnPc2	bakterie
tvar	tvar	k1gInSc4	tvar
bakteriálních	bakteriální	k2eAgFnPc2d1	bakteriální
buněk	buňka	k1gFnPc2	buňka
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
bakterií	bakterie	k1gFnPc2	bakterie
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
následující	následující	k2eAgInPc4d1	následující
typy	typ	k1gInPc4	typ
buněk	buňka	k1gFnPc2	buňka
dle	dle	k7c2	dle
tvaru	tvar	k1gInSc2	tvar
<g/>
:	:	kIx,	:
kulovitý	kulovitý	k2eAgMnSc1d1	kulovitý
(	(	kIx(	(
<g/>
koky	koka	k1gFnPc1	koka
<g/>
)	)	kIx)	)
-	-	kIx~	-
pokud	pokud	k8xS	pokud
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
kolonie	kolonie	k1gFnPc1	kolonie
<g/>
,	,	kIx,	,
dělí	dělit	k5eAaImIp3nP	dělit
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
diplokoky	diplokokus	k1gMnPc4	diplokokus
(	(	kIx(	(
<g/>
kolonie	kolonie	k1gFnSc1	kolonie
tvořené	tvořený	k2eAgFnSc2d1	tvořená
dvěma	dva	k4xCgFnPc7	dva
buňkami	buňka	k1gFnPc7	buňka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tetrakoky	tetrakok	k1gInPc1	tetrakok
(	(	kIx(	(
<g/>
čtyři	čtyři	k4xCgFnPc1	čtyři
buňky	buňka	k1gFnPc1	buňka
v	v	k7c6	v
kolonii	kolonie	k1gFnSc6	kolonie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
streptokoky	streptokok	k1gInPc4	streptokok
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
řetízkovité	řetízkovitý	k2eAgFnSc2d1	řetízkovitý
kolonie	kolonie	k1gFnSc2	kolonie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stafylokoky	stafylokok	k1gInPc1	stafylokok
(	(	kIx(	(
<g/>
hroznovité	hroznovitý	k2eAgFnPc1d1	hroznovitá
kolonie	kolonie	k1gFnPc1	kolonie
<g/>
)	)	kIx)	)
a	a	k8xC	a
sarciny	sarcina	k1gFnSc2	sarcina
(	(	kIx(	(
<g/>
balíčkovité	balíčkovitý	k2eAgFnSc2d1	balíčkovitý
kolonie	kolonie	k1gFnSc2	kolonie
<g/>
)	)	kIx)	)
tyčinkovitý	tyčinkovitý	k2eAgMnSc1d1	tyčinkovitý
(	(	kIx(	(
<g/>
tyčinky	tyčinka	k1gFnPc1	tyčinka
čili	čili	k8xC	čili
bacily	bacil	k1gInPc1	bacil
<g/>
)	)	kIx)	)
-	-	kIx~	-
tyto	tento	k3xDgFnPc1	tento
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
sdružovat	sdružovat	k5eAaImF	sdružovat
v	v	k7c6	v
koloniích	kolonie	k1gFnPc6	kolonie
po	po	k7c6	po
dvou	dva	k4xCgInPc2	dva
(	(	kIx(	(
<g/>
diplobacily	diplobacil	k1gInPc1	diplobacil
<g/>
)	)	kIx)	)
či	či	k8xC	či
v	v	k7c6	v
řetízcích	řetízek	k1gInPc6	řetízek
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
streptobacily	streptobacila	k1gFnPc1	streptobacila
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
tvoří	tvořit	k5eAaImIp3nP	tvořit
palisády	palisáda	k1gFnPc4	palisáda
zakřivený	zakřivený	k2eAgInSc4d1	zakřivený
-	-	kIx~	-
takto	takto	k6eAd1	takto
tvarované	tvarovaný	k2eAgFnSc2d1	tvarovaná
bakterie	bakterie	k1gFnSc2	bakterie
nevytvářejí	vytvářet	k5eNaImIp3nP	vytvářet
kolonie	kolonie	k1gFnSc1	kolonie
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnPc4	on
vibria	vibrio	k1gNnPc4	vibrio
(	(	kIx(	(
<g/>
krátké	krátký	k2eAgFnSc2d1	krátká
lehce	lehko	k6eAd1	lehko
zakřivené	zakřivený	k2eAgFnSc2d1	zakřivená
tyčinky	tyčinka	k1gFnSc2	tyčinka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spirily	spirila	k1gFnPc4	spirila
(	(	kIx(	(
<g/>
lehce	lehko	k6eAd1	lehko
zvlněné	zvlněný	k2eAgFnSc2d1	zvlněná
tyčinky	tyčinka	k1gFnSc2	tyčinka
<g/>
)	)	kIx)	)
či	či	k8xC	či
spirochéty	spirochéta	k1gFnSc2	spirochéta
(	(	kIx(	(
<g/>
tyčinky	tyčinka	k1gFnSc2	tyčinka
šroubovitého	šroubovitý	k2eAgInSc2d1	šroubovitý
tvaru	tvar	k1gInSc2	tvar
<g/>
)	)	kIx)	)
vláknitý	vláknitý	k2eAgMnSc1d1	vláknitý
vláknité	vláknitý	k2eAgInPc1d1	vláknitý
kolonie	kolonie	k1gFnSc2	kolonie
větvený	větvený	k2eAgMnSc1d1	větvený
-	-	kIx~	-
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
buďto	buďto	k8xC	buďto
náznaky	náznak	k1gInPc1	náznak
větvení	větvení	k1gNnSc2	větvení
nebo	nebo	k8xC	nebo
větvení	větvení	k1gNnPc4	větvení
úplné	úplný	k2eAgFnPc4d1	úplná
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
skupina	skupina	k1gFnSc1	skupina
může	moct	k5eAaImIp3nS	moct
vytvářet	vytvářet	k5eAaImF	vytvářet
bakteriální	bakteriální	k2eAgNnPc4d1	bakteriální
mycelia	mycelium	k1gNnPc4	mycelium
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
bakterie	bakterie	k1gFnPc1	bakterie
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
kolonie	kolonie	k1gFnPc1	kolonie
podobné	podobný	k2eAgNnSc1d1	podobné
tělu	tělo	k1gNnSc3	tělo
jednoduchých	jednoduchý	k2eAgFnPc2d1	jednoduchá
mnohobuněčných	mnohobuněčný	k2eAgFnPc2d1	mnohobuněčná
eukaryot	eukaryota	k1gFnPc2	eukaryota
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
některé	některý	k3yIgFnPc4	některý
sinice	sinice	k1gFnPc4	sinice
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Anabaena	Anabaena	k1gFnSc1	Anabaena
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
myxobakterie	myxobakterie	k1gFnSc1	myxobakterie
(	(	kIx(	(
<g/>
Myxococcales	Myxococcales	k1gInSc1	Myxococcales
<g/>
)	)	kIx)	)
a	a	k8xC	a
mnohé	mnohý	k2eAgFnPc1d1	mnohá
další	další	k2eAgFnPc1d1	další
skupiny	skupina	k1gFnPc1	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgFnPc1	druhý
jmenované	jmenovaný	k2eAgFnPc1d1	jmenovaná
bakterie	bakterie	k1gFnPc1	bakterie
dokonce	dokonce	k9	dokonce
tvoří	tvořit	k5eAaImIp3nP	tvořit
za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
podmínek	podmínka	k1gFnPc2	podmínka
makroskopické	makroskopický	k2eAgFnSc2d1	makroskopická
plodničky	plodnička	k1gFnSc2	plodnička
se	s	k7c7	s
sporami	spora	k1gFnPc7	spora
uvnitř	uvnitř	k6eAd1	uvnitř
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
o	o	k7c6	o
dobře	dobře	k6eAd1	dobře
známých	známý	k2eAgFnPc6d1	známá
bakteriích	bakterie	k1gFnPc6	bakterie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
napadají	napadat	k5eAaBmIp3nP	napadat
lidské	lidský	k2eAgNnSc4d1	lidské
tělo	tělo	k1gNnSc4	tělo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokáží	dokázat	k5eAaPmIp3nP	dokázat
díky	díky	k7c3	díky
molekulárním	molekulární	k2eAgInPc3d1	molekulární
signálům	signál	k1gInPc3	signál
synchronizovat	synchronizovat	k5eAaBmF	synchronizovat
své	svůj	k3xOyFgNnSc4	svůj
chování	chování	k1gNnSc4	chování
a	a	k8xC	a
"	"	kIx"	"
<g/>
táhnout	táhnout	k5eAaImF	táhnout
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
provaz	provaz	k1gInSc4	provaz
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
bakterií	bakterie	k1gFnPc2	bakterie
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
rozmanitá	rozmanitý	k2eAgFnSc1d1	rozmanitá
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
ale	ale	k9	ale
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
desetinami	desetina	k1gFnPc7	desetina
a	a	k8xC	a
desítkami	desítka	k1gFnPc7	desítka
mikrometrů	mikrometr	k1gInPc2	mikrometr
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Escherichia	Escherichia	k1gFnSc1	Escherichia
coli	col	k1gFnSc2	col
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
délky	délka	k1gFnSc2	délka
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
μ	μ	k?	μ
a	a	k8xC	a
šířky	šířka	k1gFnPc1	šířka
0,6	[number]	k4	0,6
μ	μ	k?	μ
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
známo	znám	k2eAgNnSc1d1	známo
mnoho	mnoho	k6eAd1	mnoho
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
extrémních	extrémní	k2eAgFnPc2d1	extrémní
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
prokaryotická	prokaryotický	k2eAgFnSc1d1	prokaryotická
buňka	buňka	k1gFnSc1	buňka
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
řádově	řádově	k6eAd1	řádově
desítky	desítka	k1gFnPc1	desítka
mikrometrů	mikrometr	k1gInPc2	mikrometr
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
omezeným	omezený	k2eAgFnPc3d1	omezená
možnostem	možnost	k1gFnPc3	možnost
difuze	difuze	k1gFnSc2	difuze
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
hraje	hrát	k5eAaImIp3nS	hrát
hlavní	hlavní	k2eAgFnSc4d1	hlavní
úlohu	úloha	k1gFnSc4	úloha
při	při	k7c6	při
transportu	transport	k1gInSc6	transport
látek	látka	k1gFnPc2	látka
buňkou	buňka	k1gFnSc7	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
bakterie	bakterie	k1gFnPc4	bakterie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
zřejmě	zřejmě	k6eAd1	zřejmě
výjimkou	výjimka	k1gFnSc7	výjimka
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
pravidla	pravidlo	k1gNnSc2	pravidlo
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Epulopiscium	Epulopiscium	k1gNnSc4	Epulopiscium
fishelsoni	fishelsoň	k1gFnSc3	fishelsoň
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gMnSc1	jejíž
0,2	[number]	k4	0,2
<g/>
-	-	kIx~	-
<g/>
0,7	[number]	k4	0,7
mm	mm	kA	mm
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
buňky	buňka	k1gFnSc2	buňka
jsou	být	k5eAaImIp3nP	být
větší	veliký	k2eAgFnPc1d2	veliký
než	než	k8xS	než
buňky	buňka	k1gFnPc1	buňka
většiny	většina	k1gFnSc2	většina
prvoků	prvok	k1gMnPc2	prvok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
známá	známý	k2eAgFnSc1d1	známá
bakterie	bakterie	k1gFnSc1	bakterie
Thiomargarita	Thiomargarita	k1gFnSc1	Thiomargarita
namibiensis	namibiensis	k1gFnSc1	namibiensis
(	(	kIx(	(
<g/>
0,75	[number]	k4	0,75
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
nejmenší	malý	k2eAgFnPc1d3	nejmenší
jsou	být	k5eAaImIp3nP	být
bakterie	bakterie	k1gFnPc1	bakterie
rodu	rod	k1gInSc2	rod
Mycoplasma	Mycoplasmum	k1gNnSc2	Mycoplasmum
(	(	kIx(	(
<g/>
a	a	k8xC	a
příbuzný	příbuzný	k1gMnSc1	příbuzný
Ureaplasma	Ureaplasm	k1gMnSc2	Ureaplasm
<g/>
)	)	kIx)	)
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
buňky	buňka	k1gFnSc2	buňka
jen	jen	k9	jen
asi	asi	k9	asi
0,1	[number]	k4	0,1
<g/>
-	-	kIx~	-
<g/>
0,3	[number]	k4	0,3
μ	μ	k?	μ
<g/>
.	.	kIx.	.
</s>
<s>
Nemají	mít	k5eNaImIp3nP	mít
buněčnou	buněčný	k2eAgFnSc4d1	buněčná
stěnu	stěna	k1gFnSc4	stěna
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
byly	být	k5eAaImAgInP	být
dříve	dříve	k6eAd2	dříve
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c2	za
viry	vira	k1gFnSc2	vira
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgInPc1d1	Malé
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
rickettsie	rickettsie	k1gFnPc1	rickettsie
a	a	k8xC	a
chlamydie	chlamydie	k1gFnPc1	chlamydie
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgFnPc1d2	menší
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
kontroverzní	kontroverzní	k2eAgInPc1d1	kontroverzní
nálezy	nález	k1gInPc1	nález
<g/>
,	,	kIx,	,
označované	označovaný	k2eAgFnPc1d1	označovaná
jako	jako	k8xC	jako
nanobakterie	nanobakterie	k1gFnPc1	nanobakterie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
velikosti	velikost	k1gFnSc3	velikost
jen	jen	k9	jen
50	[number]	k4	50
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
nanometrů	nanometr	k1gInPc2	nanometr
(	(	kIx(	(
<g/>
0,05	[number]	k4	0,05
<g/>
-	-	kIx~	-
<g/>
0,2	[number]	k4	0,2
μ	μ	k?	μ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nejnovějších	nový	k2eAgInPc2d3	nejnovější
výzkumů	výzkum	k1gInPc2	výzkum
se	se	k3xPyFc4	se
však	však	k9	však
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
živé	živý	k2eAgInPc4d1	živý
organismy	organismus	k1gInPc4	organismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
krystaly	krystal	k1gInPc4	krystal
uhličitanu	uhličitan	k1gInSc2	uhličitan
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
v	v	k7c6	v
krevním	krevní	k2eAgNnSc6d1	krevní
séru	sérum	k1gNnSc6	sérum
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
prokaryotická	prokaryotický	k2eAgFnSc1d1	prokaryotická
buňka	buňka	k1gFnSc1	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Bakteriální	bakteriální	k2eAgFnSc1d1	bakteriální
buňka	buňka	k1gFnSc1	buňka
je	být	k5eAaImIp3nS	být
buňkou	buňka	k1gFnSc7	buňka
prokaryotní	prokaryotní	k2eAgFnSc7d1	prokaryotní
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
existují	existovat	k5eAaImIp3nP	existovat
značné	značný	k2eAgInPc4d1	značný
rozdíly	rozdíl	k1gInPc4	rozdíl
mezi	mezi	k7c7	mezi
stavbou	stavba	k1gFnSc7	stavba
buněk	buňka	k1gFnPc2	buňka
různých	různý	k2eAgFnPc2d1	různá
skupin	skupina	k1gFnPc2	skupina
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
vystopovat	vystopovat	k5eAaPmF	vystopovat
určité	určitý	k2eAgInPc4d1	určitý
společné	společný	k2eAgInPc4d1	společný
rysy	rys	k1gInPc4	rys
<g/>
.	.	kIx.	.
</s>
<s>
Známe	znát	k5eAaImIp1nP	znát
nejen	nejen	k6eAd1	nejen
mnohé	mnohý	k2eAgFnPc4d1	mnohá
struktury	struktura	k1gFnPc4	struktura
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
buněk	buňka	k1gFnPc2	buňka
(	(	kIx(	(
<g/>
membrána	membrána	k1gFnSc1	membrána
<g/>
,	,	kIx,	,
buněčná	buněčný	k2eAgNnPc4d1	buněčné
stěna	stěna	k1gFnSc1	stěna
<g/>
,	,	kIx,	,
pilusy	pilus	k1gInPc4	pilus
<g/>
,	,	kIx,	,
bičíky	bičík	k1gInPc4	bičík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
vnitrobuněčné	vnitrobuněčný	k2eAgFnPc4d1	vnitrobuněčná
struktury	struktura	k1gFnPc4	struktura
(	(	kIx(	(
<g/>
souhrnně	souhrnně	k6eAd1	souhrnně
protoplast	protoplast	k1gInSc4	protoplast
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
bakteriálních	bakteriální	k2eAgFnPc2d1	bakteriální
buněk	buňka	k1gFnPc2	buňka
je	být	k5eAaImIp3nS	být
cytoplazmatická	cytoplazmatický	k2eAgFnSc1d1	cytoplazmatická
membrána	membrána	k1gFnSc1	membrána
podobná	podobný	k2eAgFnSc1d1	podobná
membráně	membrána	k1gFnSc3	membrána
eukaryot	eukaryot	k1gInSc1	eukaryot
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
tím	ten	k3xDgInSc7	ten
rozdílem	rozdíl	k1gInSc7	rozdíl
<g/>
,	,	kIx,	,
že	že	k8xS	že
většinou	většina	k1gFnSc7	většina
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
žádné	žádný	k3yNgInPc4	žádný
steroidy	steroid	k1gInPc4	steroid
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
funkcí	funkce	k1gFnSc7	funkce
membrány	membrána	k1gFnSc2	membrána
bakterií	bakterie	k1gFnPc2	bakterie
je	být	k5eAaImIp3nS	být
tvorba	tvorba	k1gFnSc1	tvorba
ATP	atp	kA	atp
díky	díky	k7c3	díky
vytváření	vytváření	k1gNnSc3	vytváření
protonového	protonový	k2eAgInSc2d1	protonový
gradientu	gradient	k1gInSc2	gradient
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
+	+	kIx~	+
iontů	ion	k1gInPc2	ion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
bakterií	bakterie	k1gFnPc2	bakterie
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
buňky	buňka	k1gFnSc2	buňka
buněčnou	buněčný	k2eAgFnSc4d1	buněčná
stěnu	stěna	k1gFnSc4	stěna
z	z	k7c2	z
peptidoglykanu	peptidoglykan	k1gInSc2	peptidoglykan
(	(	kIx(	(
<g/>
murein	murein	k1gInSc1	murein
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
kyselinu	kyselina	k1gFnSc4	kyselina
muramovou	muramový	k2eAgFnSc4d1	muramový
jako	jako	k8xC	jako
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
složek	složka	k1gFnPc2	složka
<g/>
.	.	kIx.	.
</s>
<s>
Peptidoglykan	Peptidoglykan	k1gInSc1	Peptidoglykan
tvoří	tvořit	k5eAaImIp3nS	tvořit
kolem	kolem	k7c2	kolem
buněk	buňka	k1gFnPc2	buňka
pevnou	pevný	k2eAgFnSc4d1	pevná
síť	síť	k1gFnSc4	síť
vyplněnou	vyplněná	k1gFnSc7	vyplněná
peptidy	peptid	k1gInPc1	peptid
<g/>
.	.	kIx.	.
</s>
<s>
Buněčná	buněčný	k2eAgFnSc1d1	buněčná
stěna	stěna	k1gFnSc1	stěna
bakterií	bakterie	k1gFnPc2	bakterie
je	být	k5eAaImIp3nS	být
však	však	k9	však
velmi	velmi	k6eAd1	velmi
proměnlivá	proměnlivý	k2eAgFnSc1d1	proměnlivá
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgMnPc4d1	jiný
důležitým	důležitý	k2eAgInSc7d1	důležitý
znakem	znak	k1gInSc7	znak
při	při	k7c6	při
rozlišování	rozlišování	k1gNnSc6	rozlišování
bakterií	bakterie	k1gFnPc2	bakterie
na	na	k7c6	na
grampozitivní	grampozitivní	k2eAgFnSc6d1	grampozitivní
a	a	k8xC	a
gramnegativní	gramnegativní	k2eAgFnSc6d1	gramnegativní
<g/>
.	.	kIx.	.
</s>
<s>
Grampozitivní	Grampozitivní	k2eAgInPc1d1	Grampozitivní
mají	mít	k5eAaImIp3nP	mít
totiž	totiž	k9	totiž
v	v	k7c6	v
buněčné	buněčný	k2eAgFnSc6d1	buněčná
stěně	stěna	k1gFnSc6	stěna
více	hodně	k6eAd2	hodně
peptidoglykanu	peptidoglykan	k1gInSc2	peptidoglykan
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
více	hodně	k6eAd2	hodně
krystalové	krystalový	k2eAgFnPc4d1	krystalová
violeti	violeť	k1gFnPc4	violeť
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nP	tvořit
bakterie	bakterie	k1gFnPc1	bakterie
třídy	třída	k1gFnSc2	třída
Mollicutes	Mollicutes	k1gMnSc1	Mollicutes
(	(	kIx(	(
<g/>
např.	např.	kA	např.
rod	rod	k1gInSc1	rod
Mycoplasma	Mycoplasma	k1gNnSc1	Mycoplasma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nesyntetizují	syntetizovat	k5eNaImIp3nP	syntetizovat
peptidoglykan	peptidoglykan	k1gInSc4	peptidoglykan
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
jim	on	k3xPp3gMnPc3	on
chybí	chybět	k5eAaImIp3nS	chybět
buněčná	buněčný	k2eAgFnSc1d1	buněčná
stěna	stěna	k1gFnSc1	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
tvar	tvar	k1gInSc1	tvar
tak	tak	k6eAd1	tak
drží	držet	k5eAaImIp3nS	držet
pouze	pouze	k6eAd1	pouze
třívrstevná	třívrstevný	k2eAgFnSc1d1	třívrstevná
membrána	membrána	k1gFnSc1	membrána
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
ukládají	ukládat	k5eAaImIp3nP	ukládat
steroidy	steroid	k1gInPc1	steroid
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
zvláštnosti	zvláštnost	k1gFnSc3	zvláštnost
mají	mít	k5eAaImIp3nP	mít
tyto	tento	k3xDgFnPc4	tento
baktérie	baktérie	k1gFnPc4	baktérie
velkou	velký	k2eAgFnSc4d1	velká
plasticitu	plasticita	k1gFnSc4	plasticita
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
měnit	měnit	k5eAaImF	měnit
svůj	svůj	k3xOyFgInSc4	svůj
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
bakterií	bakterie	k1gFnPc2	bakterie
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
ještě	ještě	k6eAd1	ještě
další	další	k2eAgFnSc4d1	další
polysacharidovou	polysacharidový	k2eAgFnSc4d1	polysacharidová
či	či	k8xC	či
proteinovou	proteinový	k2eAgFnSc4d1	proteinová
ochrannou	ochranný	k2eAgFnSc4d1	ochranná
vrstvu	vrstva	k1gFnSc4	vrstva
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
kapsulu	kapsul	k1gInSc2	kapsul
(	(	kIx(	(
<g/>
pouzdro	pouzdro	k1gNnSc1	pouzdro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
funkcí	funkce	k1gFnSc7	funkce
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
zpevňovat	zpevňovat	k5eAaImF	zpevňovat
povrch	povrch	k1gInSc4	povrch
bakterií	bakterie	k1gFnPc2	bakterie
(	(	kIx(	(
<g/>
bakterie	bakterie	k1gFnSc1	bakterie
s	s	k7c7	s
kapsulou	kapsula	k1gFnSc7	kapsula
jsou	být	k5eAaImIp3nP	být
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
více	hodně	k6eAd2	hodně
patogenní	patogenní	k2eAgNnSc1d1	patogenní
<g/>
)	)	kIx)	)
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
lépe	dobře	k6eAd2	dobře
přichytit	přichytit	k5eAaPmF	přichytit
k	k	k7c3	k
substrátu	substrát	k1gInSc3	substrát
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
slizovitým	slizovitý	k2eAgMnPc3d1	slizovitý
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
soudržným	soudržný	k2eAgFnPc3d1	soudržná
kapsulám	kapsula	k1gFnPc3	kapsula
(	(	kIx(	(
<g/>
zvaným	zvaný	k2eAgInPc3d1	zvaný
slizová	slizový	k2eAgFnSc1d1	slizová
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
)	)	kIx)	)
vážou	vázat	k5eAaImIp3nP	vázat
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
buňky	buňka	k1gFnPc1	buňka
v	v	k7c6	v
bakteriálních	bakteriální	k2eAgFnPc6d1	bakteriální
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Dohromady	dohromady	k6eAd1	dohromady
se	se	k3xPyFc4	se
kapsule	kapsule	k?	kapsule
a	a	k8xC	a
slizové	slizový	k2eAgFnSc3d1	slizová
vrstvě	vrstva	k1gFnSc3	vrstva
také	také	k9	také
říká	říkat	k5eAaImIp3nS	říkat
glykokalyx	glykokalyx	k1gInSc1	glykokalyx
<g/>
.	.	kIx.	.
</s>
<s>
Vyjma	vyjma	k7c2	vyjma
plošných	plošný	k2eAgInPc2d1	plošný
útvarů	útvar	k1gInPc2	útvar
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
kryjí	krýt	k5eAaImIp3nP	krýt
buňku	buňka	k1gFnSc4	buňka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
ještě	ještě	k9	ještě
řada	řada	k1gFnSc1	řada
jiných	jiný	k2eAgFnPc2d1	jiná
struktur	struktura	k1gFnPc2	struktura
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
bičíky	bičík	k1gInPc1	bičík
a	a	k8xC	a
pilusy	pilus	k1gInPc1	pilus
(	(	kIx(	(
<g/>
rovněž	rovněž	k9	rovněž
zvané	zvaný	k2eAgFnPc1d1	zvaná
fimbrie	fimbrie	k1gFnPc1	fimbrie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bičíky	bičík	k1gInPc1	bičík
jsou	být	k5eAaImIp3nP	být
vlasovité	vlasovitý	k2eAgMnPc4d1	vlasovitý
(	(	kIx(	(
<g/>
asi	asi	k9	asi
20	[number]	k4	20
nm	nm	k?	nm
silné	silný	k2eAgInPc1d1	silný
<g/>
,	,	kIx,	,
20	[number]	k4	20
μ	μ	k?	μ
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
<g/>
)	)	kIx)	)
struktury	struktura	k1gFnPc4	struktura
ukotvené	ukotvený	k2eAgFnPc4d1	ukotvená
v	v	k7c6	v
membráně	membrána	k1gFnSc6	membrána
<g/>
,	,	kIx,	,
tvořené	tvořený	k2eAgFnPc4d1	tvořená
helikálně	helikálně	k6eAd1	helikálně
složeným	složený	k2eAgInSc7d1	složený
proteinem	protein	k1gInSc7	protein
flagelinem	flagelin	k1gInSc7	flagelin
a	a	k8xC	a
sloužící	sloužící	k1gFnSc7	sloužící
k	k	k7c3	k
pohybu	pohyb	k1gInSc3	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnPc1	bakterie
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
žádný	žádný	k3yNgMnSc1	žádný
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
(	(	kIx(	(
<g/>
monotricha	monotrich	k1gMnSc2	monotrich
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
(	(	kIx(	(
<g/>
lofotricha	lofotrich	k1gMnSc2	lofotrich
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc1	dva
uspořádané	uspořádaný	k2eAgInPc1d1	uspořádaný
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
(	(	kIx(	(
<g/>
amfitricha	amfitrich	k1gMnSc4	amfitrich
<g/>
)	)	kIx)	)
či	či	k8xC	či
velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
bičíků	bičík	k1gInPc2	bičík
rozložených	rozložený	k2eAgInPc2d1	rozložený
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
povrchu	povrch	k1gInSc6	povrch
bakterie	bakterie	k1gFnSc2	bakterie
(	(	kIx(	(
<g/>
peritricha	peritrich	k1gMnSc2	peritrich
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bičíky	bičík	k1gInPc1	bičík
bakterií	bakterie	k1gFnPc2	bakterie
se	se	k3xPyFc4	se
stavbou	stavba	k1gFnSc7	stavba
značně	značně	k6eAd1	značně
liší	lišit	k5eAaImIp3nS	lišit
od	od	k7c2	od
bičíků	bičík	k1gInPc2	bičík
eukaryot	eukaryota	k1gFnPc2	eukaryota
<g/>
.	.	kIx.	.
</s>
<s>
Pilus	Pilus	k1gInSc1	Pilus
čili	čili	k8xC	čili
fimbrie	fimbrie	k1gFnSc1	fimbrie
je	být	k5eAaImIp3nS	být
druhým	druhý	k4xOgInSc7	druhý
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
vlasovitým	vlasovitý	k2eAgInSc7d1	vlasovitý
útvarem	útvar	k1gInSc7	útvar
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kratší	krátký	k2eAgInPc4d2	kratší
<g/>
,	,	kIx,	,
tužší	tuhý	k2eAgInPc4d2	tužší
a	a	k8xC	a
užší	úzký	k2eAgInPc4d2	užší
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
<g/>
.	.	kIx.	.
</s>
<s>
Pilusy	Pilus	k1gInPc1	Pilus
bakterie	bakterie	k1gFnSc2	bakterie
využívají	využívat	k5eAaPmIp3nP	využívat
k	k	k7c3	k
přichycení	přichycení	k1gNnSc3	přichycení
na	na	k7c4	na
podklad	podklad	k1gInSc4	podklad
(	(	kIx(	(
<g/>
adheze	adheze	k1gFnSc1	adheze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
specializované	specializovaný	k2eAgInPc1d1	specializovaný
sexuální	sexuální	k2eAgInPc1d1	sexuální
pilusy	pilus	k1gInPc1	pilus
(	(	kIx(	(
<g/>
F	F	kA	F
pilusy	pilus	k1gInPc1	pilus
<g/>
)	)	kIx)	)
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
přenosu	přenos	k1gInSc3	přenos
DNA	DNA	kA	DNA
(	(	kIx(	(
<g/>
sexualita	sexualita	k1gFnSc1	sexualita
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
buňky	buňka	k1gFnSc2	buňka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
protoplast	protoplast	k1gInSc1	protoplast
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
množství	množství	k1gNnSc1	množství
různých	různý	k2eAgFnPc2d1	různá
struktur	struktura	k1gFnPc2	struktura
rozptýlených	rozptýlený	k2eAgFnPc2d1	rozptýlená
v	v	k7c6	v
cytosolu	cytosol	k1gInSc6	cytosol
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
vnitrobuněčné	vnitrobuněčný	k2eAgFnPc4d1	vnitrobuněčná
struktury	struktura	k1gFnPc4	struktura
u	u	k7c2	u
bakterií	bakterie	k1gFnPc2	bakterie
patří	patřit	k5eAaImIp3nS	patřit
nukleoid	nukleoid	k1gInSc1	nukleoid
(	(	kIx(	(
<g/>
jaderné	jaderný	k2eAgFnSc2d1	jaderná
oblasti	oblast	k1gFnSc2	oblast
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ribozomy	ribozom	k1gInPc1	ribozom
<g/>
,	,	kIx,	,
inkluze	inkluze	k1gFnPc1	inkluze
a	a	k8xC	a
cytoskelet	cytoskelet	k1gInSc1	cytoskelet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
cytosolu	cytosol	k1gInSc6	cytosol
však	však	k9	však
jsou	být	k5eAaImIp3nP	být
rozptýleny	rozptýlen	k2eAgFnPc1d1	rozptýlena
i	i	k8xC	i
jiné	jiný	k2eAgFnPc1d1	jiná
struktury	struktura	k1gFnPc1	struktura
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
množství	množství	k1gNnSc1	množství
proteinů	protein	k1gInPc2	protein
<g/>
,	,	kIx,	,
mRNA	mRNA	k?	mRNA
a	a	k8xC	a
bakteriální	bakteriální	k2eAgInPc1d1	bakteriální
metabolity	metabolit	k1gInPc1	metabolit
<g/>
.	.	kIx.	.
</s>
<s>
Nukleoid	Nukleoid	k1gInSc1	Nukleoid
(	(	kIx(	(
<g/>
bakteriální	bakteriální	k2eAgInSc1d1	bakteriální
chromozom	chromozom	k1gInSc1	chromozom
<g/>
,	,	kIx,	,
genofor	genofor	k1gInSc1	genofor
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jaderná	jaderný	k2eAgFnSc1d1	jaderná
oblast	oblast	k1gFnSc1	oblast
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jediný	jediný	k2eAgInSc4d1	jediný
obvykle	obvykle	k6eAd1	obvykle
kruhový	kruhový	k2eAgInSc4d1	kruhový
řetězec	řetězec	k1gInSc4	řetězec
tvořený	tvořený	k2eAgInSc4d1	tvořený
dvěma	dva	k4xCgFnPc7	dva
vlákny	vlákna	k1gFnPc4	vlákna
deoxyribonukleové	deoxyribonukleový	k2eAgFnSc2d1	deoxyribonukleová
kyseliny	kyselina	k1gFnSc2	kyselina
(	(	kIx(	(
<g/>
DNA	DNA	kA	DNA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzácně	vzácně	k6eAd1	vzácně
je	být	k5eAaImIp3nS	být
však	však	k9	však
DNA	dno	k1gNnPc4	dno
bakterií	bakterie	k1gFnPc2	bakterie
i	i	k8xC	i
lineární	lineární	k2eAgFnSc1d1	lineární
nebo	nebo	k8xC	nebo
rozdělená	rozdělený	k2eAgFnSc1d1	rozdělená
do	do	k7c2	do
více	hodně	k6eAd2	hodně
chromozomů	chromozom	k1gInPc2	chromozom
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
přítomny	přítomen	k2eAgInPc1d1	přítomen
histony	histon	k1gInPc1	histon
a	a	k8xC	a
netvoří	tvořit	k5eNaImIp3nP	tvořit
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
nukleozomy	nukleozom	k1gInPc1	nukleozom
<g/>
,	,	kIx,	,
celá	celý	k2eAgFnSc1d1	celá
oblast	oblast	k1gFnSc1	oblast
ani	ani	k8xC	ani
zpravidla	zpravidla	k6eAd1	zpravidla
není	být	k5eNaImIp3nS	být
obklopena	obklopen	k2eAgFnSc1d1	obklopena
žádnou	žádný	k3yNgFnSc7	žádný
membránou	membrána	k1gFnSc7	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
však	však	k9	však
existuje	existovat	k5eAaImIp3nS	existovat
výjimka	výjimka	k1gFnSc1	výjimka
a	a	k8xC	a
např.	např.	kA	např.
u	u	k7c2	u
planktomycet	planktomycet	k5eAaPmF	planktomycet
a	a	k8xC	a
poribakterií	poribakterie	k1gFnSc7	poribakterie
byly	být	k5eAaImAgFnP	být
nalezeny	naleznout	k5eAaPmNgFnP	naleznout
membránové	membránový	k2eAgFnPc1d1	membránová
struktury	struktura	k1gFnPc1	struktura
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
připomínají	připomínat	k5eAaImIp3nP	připomínat
eukaryotické	eukaryotický	k2eAgNnSc4d1	eukaryotické
buněčné	buněčný	k2eAgNnSc4d1	buněčné
jádro	jádro	k1gNnSc4	jádro
(	(	kIx(	(
<g/>
u	u	k7c2	u
rodu	rod	k1gInSc2	rod
Gemmata	Gemma	k1gNnPc4	Gemma
má	mít	k5eAaImIp3nS	mít
dokonce	dokonce	k9	dokonce
toto	tento	k3xDgNnSc1	tento
jádro	jádro	k1gNnSc1	jádro
dvojitou	dvojitý	k2eAgFnSc4d1	dvojitá
membránu	membrána	k1gFnSc4	membrána
a	a	k8xC	a
póry	pór	k1gInPc4	pór
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
cytoplazmě	cytoplazma	k1gFnSc6	cytoplazma
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
roztroušeny	roztroušen	k2eAgInPc1d1	roztroušen
plazmidy	plazmid	k1gInPc1	plazmid
<g/>
,	,	kIx,	,
malé	malý	k2eAgInPc1d1	malý
úseky	úsek	k1gInPc1	úsek
DNA	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
<s>
Ribozomy	Ribozom	k1gInPc7	Ribozom
jsou	být	k5eAaImIp3nP	být
prokaryotického	prokaryotický	k2eAgInSc2d1	prokaryotický
typu	typ	k1gInSc2	typ
<g/>
;	;	kIx,	;
jsou	být	k5eAaImIp3nP	být
charakterizovány	charakterizovat	k5eAaBmNgInP	charakterizovat
zkratkou	zkratka	k1gFnSc7	zkratka
30	[number]	k4	30
<g/>
S	s	k7c7	s
<g/>
+	+	kIx~	+
<g/>
50	[number]	k4	50
<g/>
S	s	k7c7	s
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
velikost	velikost	k1gFnSc4	velikost
a	a	k8xC	a
strukturu	struktura	k1gFnSc4	struktura
ribozomálních	ribozomální	k2eAgFnPc2d1	ribozomální
podjednotek	podjednotka	k1gFnPc2	podjednotka
<g/>
.	.	kIx.	.
</s>
<s>
Ribozomy	Ribozom	k1gInPc1	Ribozom
bakterií	bakterie	k1gFnPc2	bakterie
jsou	být	k5eAaImIp3nP	být
odlišné	odlišný	k2eAgInPc1d1	odlišný
od	od	k7c2	od
eukaryotických	eukaryotický	k2eAgInPc2d1	eukaryotický
nejen	nejen	k6eAd1	nejen
délkou	délka	k1gFnSc7	délka
proteinů	protein	k1gInPc2	protein
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jejich	jejich	k3xOp3gInSc7	jejich
typem	typ	k1gInSc7	typ
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
logicky	logicky	k6eAd1	logicky
stávají	stávat	k5eAaImIp3nP	stávat
častým	častý	k2eAgInSc7d1	častý
cílem	cíl	k1gInSc7	cíl
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
<g/>
.	.	kIx.	.
</s>
<s>
Inkluze	inkluze	k1gFnPc1	inkluze
jsou	být	k5eAaImIp3nP	být
váčky	váček	k1gInPc4	váček
či	či	k8xC	či
zrna	zrno	k1gNnPc4	zrno
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
sloužící	sloužící	k2eAgMnSc1d1	sloužící
jako	jako	k8xC	jako
zásobní	zásobní	k2eAgFnPc1d1	zásobní
struktury	struktura	k1gFnPc1	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
skladovat	skladovat	k5eAaImF	skladovat
glykogen	glykogen	k1gInSc4	glykogen
<g/>
,	,	kIx,	,
fosfáty	fosfát	k1gInPc4	fosfát
<g/>
,	,	kIx,	,
elementární	elementární	k2eAgFnSc4d1	elementární
síru	síra	k1gFnSc4	síra
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
však	však	k9	však
i	i	k9	i
barviva	barvivo	k1gNnPc1	barvivo
či	či	k8xC	či
enzymy	enzym	k1gInPc1	enzym
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnSc1	bakterie
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejstarším	starý	k2eAgInPc3d3	nejstarší
organismům	organismus	k1gInPc3	organismus
nalézaných	nalézaný	k2eAgFnPc2d1	nalézaná
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
fosílií	fosílie	k1gFnPc2	fosílie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
bakterie	bakterie	k1gFnPc1	bakterie
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
z	z	k7c2	z
archaika	archaikum	k1gNnSc2	archaikum
<g/>
,	,	kIx,	,
nalézány	nalézat	k5eAaImNgInP	nalézat
jsou	být	k5eAaImIp3nP	být
stromatolity	stromatolit	k1gInPc1	stromatolit
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
horniny	hornina	k1gFnSc2	hornina
obsahující	obsahující	k2eAgFnSc2d1	obsahující
fosílie	fosílie	k1gFnSc2	fosílie
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
sinic	sinice	k1gFnPc2	sinice
<g/>
.	.	kIx.	.
</s>
<s>
Předkem	předek	k1gInSc7	předek
dnešních	dnešní	k2eAgFnPc2d1	dnešní
bakterií	bakterie	k1gFnPc2	bakterie
byly	být	k5eAaImAgInP	být
jednobuněčné	jednobuněčný	k2eAgInPc1d1	jednobuněčný
organismy	organismus	k1gInPc1	organismus
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
před	před	k7c7	před
čtyřmi	čtyři	k4xCgNnPc7	čtyři
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
a	a	k8xC	a
patřily	patřit	k5eAaImAgFnP	patřit
k	k	k7c3	k
vůbec	vůbec	k9	vůbec
prvním	první	k4xOgMnSc6	první
formám	forma	k1gFnPc3	forma
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc4d1	další
tři	tři	k4xCgFnPc4	tři
miliardy	miliarda	k4xCgFnPc4	miliarda
let	let	k1gInSc1	let
všechny	všechen	k3xTgInPc4	všechen
organismy	organismus	k1gInPc4	organismus
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
byly	být	k5eAaImAgFnP	být
mikroskopické	mikroskopický	k2eAgFnPc1d1	mikroskopická
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
dominovaly	dominovat	k5eAaImAgFnP	dominovat
bakterie	bakterie	k1gFnPc1	bakterie
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
archea	arche	k1gInSc2	arche
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
byly	být	k5eAaImAgFnP	být
objeveny	objeven	k2eAgFnPc1d1	objevena
fosílie	fosílie	k1gFnPc1	fosílie
bakterií	bakterie	k1gFnPc2	bakterie
(	(	kIx(	(
<g/>
např.	např.	kA	např.
stromatolity	stromatolit	k1gInPc1	stromatolit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nedostatek	nedostatek	k1gInSc1	nedostatek
zjistitelných	zjistitelný	k2eAgInPc2d1	zjistitelný
znaků	znak	k1gInPc2	znak
znemožňuje	znemožňovat	k5eAaImIp3nS	znemožňovat
jejich	jejich	k3xOp3gNnSc1	jejich
určení	určení	k1gNnSc1	určení
a	a	k8xC	a
hlubší	hluboký	k2eAgNnSc1d2	hlubší
studium	studium	k1gNnSc1	studium
<g/>
.	.	kIx.	.
</s>
<s>
Srozumitelné	srozumitelný	k2eAgInPc1d1	srozumitelný
údaje	údaj	k1gInPc1	údaj
se	se	k3xPyFc4	se
zjišťují	zjišťovat	k5eAaImIp3nP	zjišťovat
pomocí	pomocí	k7c2	pomocí
sekvenace	sekvenace	k1gFnSc2	sekvenace
genomu	genom	k1gInSc2	genom
recentních	recentní	k2eAgFnPc2d1	recentní
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
technice	technika	k1gFnSc3	technika
vědci	vědec	k1gMnPc7	vědec
částečně	částečně	k6eAd1	částečně
rekonstruovali	rekonstruovat	k5eAaBmAgMnP	rekonstruovat
bakteriální	bakteriální	k2eAgInSc4d1	bakteriální
strom	strom	k1gInSc4	strom
života	život	k1gInSc2	život
a	a	k8xC	a
zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
bakterie	bakterie	k1gFnSc1	bakterie
jsou	být	k5eAaImIp3nP	být
postranní	postranní	k2eAgFnSc7d1	postranní
větví	větev	k1gFnSc7	větev
linie	linie	k1gFnSc2	linie
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
archea	archea	k1gFnSc1	archea
a	a	k8xC	a
eukaryota	eukaryota	k1gFnSc1	eukaryota
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
společným	společný	k2eAgInSc7d1	společný
předkem	předek	k1gInSc7	předek
bakterií	bakterie	k1gFnPc2	bakterie
a	a	k8xC	a
archeí	archeit	k5eAaBmIp3nS	archeit
byl	být	k5eAaImAgMnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
hypertermofil	hypertermofil	k1gMnSc1	hypertermofil
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
žil	žít	k5eAaImAgMnS	žít
před	před	k7c7	před
2,5	[number]	k4	2,5
<g/>
-	-	kIx~	-
<g/>
3,2	[number]	k4	3,2
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnSc1	bakterie
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
zúčastnily	zúčastnit	k5eAaPmAgFnP	zúčastnit
vzniku	vznik	k1gInSc2	vznik
eukaryot	eukaryota	k1gFnPc2	eukaryota
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
taxon	taxon	k1gInSc1	taxon
bakterie	bakterie	k1gFnSc2	bakterie
(	(	kIx(	(
<g/>
Bacteria	Bacterium	k1gNnSc2	Bacterium
<g/>
)	)	kIx)	)
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
doménu	doména	k1gFnSc4	doména
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
striktně	striktně	k6eAd1	striktně
oddělena	oddělit	k5eAaPmNgFnS	oddělit
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
(	(	kIx(	(
<g/>
doména	doména	k1gFnSc1	doména
Archaea	Archaea	k1gFnSc1	Archaea
i	i	k9	i
všechny	všechen	k3xTgFnPc4	všechen
eukaryotické	eukaryotický	k2eAgFnPc4d1	eukaryotická
říše	říš	k1gFnPc4	říš
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
rozdělení	rozdělení	k1gNnSc3	rozdělení
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
Archaea	Archaeum	k1gNnPc1	Archaeum
a	a	k8xC	a
Bacteria	Bacterium	k1gNnPc1	Bacterium
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
nepříbuzné	příbuzný	k2eNgFnPc1d1	nepříbuzná
skupiny	skupina	k1gFnPc1	skupina
lišící	lišící	k2eAgFnPc1d1	lišící
se	se	k3xPyFc4	se
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
genetických	genetický	k2eAgInPc2d1	genetický
i	i	k8xC	i
morfologických	morfologický	k2eAgInPc2d1	morfologický
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
více	hodně	k6eAd2	hodně
způsobů	způsob	k1gInPc2	způsob
klasifikace	klasifikace	k1gFnSc2	klasifikace
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
se	se	k3xPyFc4	se
klasifikovaly	klasifikovat	k5eAaImAgFnP	klasifikovat
především	především	k9	především
podle	podle	k7c2	podle
vzhledu	vzhled	k1gInSc2	vzhled
(	(	kIx(	(
<g/>
fenotypu	fenotyp	k1gInSc2	fenotyp
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
však	však	k9	však
mimoto	mimoto	k6eAd1	mimoto
používají	používat	k5eAaImIp3nP	používat
též	též	k9	též
analytické	analytický	k2eAgFnPc1d1	analytická
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
chemických	chemický	k2eAgFnPc2d1	chemická
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
)	)	kIx)	)
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
též	též	k9	též
genetické	genetický	k2eAgFnPc4d1	genetická
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
genotypu	genotyp	k1gInSc2	genotyp
<g/>
)	)	kIx)	)
metody	metoda	k1gFnSc2	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
jiných	jiný	k2eAgInPc2d1	jiný
organizmů	organizmus	k1gInPc2	organizmus
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
binomické	binomický	k2eAgNnSc1d1	binomické
názvosloví	názvosloví	k1gNnSc1	názvosloví
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
Escherichia	Escherichia	k1gFnSc1	Escherichia
coli	col	k1gFnSc2	col
<g/>
)	)	kIx)	)
a	a	k8xC	a
základním	základní	k2eAgInSc7d1	základní
taxonem	taxon	k1gInSc7	taxon
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc1	druh
<g/>
.	.	kIx.	.
</s>
<s>
Nižšími	nízký	k2eAgInPc7d2	nižší
taxony	taxon	k1gInPc7	taxon
pak	pak	k6eAd1	pak
jsou	být	k5eAaImIp3nP	být
poddruh	poddruh	k1gInSc4	poddruh
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
i	i	k9	i
morfovar	morfovar	k1gInSc1	morfovar
<g/>
,	,	kIx,	,
patovar	patovar	k1gInSc1	patovar
a	a	k8xC	a
serovar	serovar	k1gInSc1	serovar
<g/>
.	.	kIx.	.
</s>
<s>
Určování	určování	k1gNnSc1	určování
(	(	kIx(	(
<g/>
determinace	determinace	k1gFnSc1	determinace
<g/>
,	,	kIx,	,
identifikace	identifikace	k1gFnSc1	identifikace
<g/>
)	)	kIx)	)
bakterií	bakterium	k1gNnPc2	bakterium
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc1d1	velký
význam	význam	k1gInSc1	význam
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
správným	správný	k2eAgNnSc7d1	správné
stanovením	stanovení	k1gNnSc7	stanovení
původce	původce	k1gMnSc2	původce
dané	daný	k2eAgFnSc2d1	daná
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
infekce	infekce	k1gFnSc2	infekce
podmíněna	podmíněn	k2eAgFnSc1d1	podmíněna
následující	následující	k2eAgFnSc1d1	následující
léčba	léčba	k1gFnSc1	léčba
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
potřeba	potřeba	k1gFnSc1	potřeba
identifikovat	identifikovat	k5eAaBmF	identifikovat
tyto	tento	k3xDgFnPc4	tento
bakterie	bakterie	k1gFnSc2	bakterie
hlavním	hlavní	k2eAgInSc7d1	hlavní
impulsem	impuls	k1gInSc7	impuls
k	k	k7c3	k
vyvinutí	vyvinutí	k1gNnSc3	vyvinutí
determinačních	determinační	k2eAgFnPc2d1	determinační
technik	technika	k1gFnPc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Mikroskopickým	mikroskopický	k2eAgNnSc7d1	mikroskopické
pozorováním	pozorování	k1gNnSc7	pozorování
tělních	tělní	k2eAgFnPc2d1	tělní
tekutin	tekutina	k1gFnPc2	tekutina
se	se	k3xPyFc4	se
bakterie	bakterie	k1gFnPc1	bakterie
určují	určovat	k5eAaImIp3nP	určovat
jen	jen	k9	jen
zřídka	zřídka	k6eAd1	zřídka
<g/>
,	,	kIx,	,
častěji	často	k6eAd2	často
jsou	být	k5eAaImIp3nP	být
preparáty	preparát	k1gInPc1	preparát
barveny	barven	k2eAgInPc1d1	barven
<g/>
.	.	kIx.	.
</s>
<s>
Známým	známý	k2eAgInSc7d1	známý
typem	typ	k1gInSc7	typ
barvení	barvení	k1gNnSc2	barvení
je	být	k5eAaImIp3nS	být
Gramovo	Gramův	k2eAgNnSc1d1	Gramovo
barvení	barvení	k1gNnSc1	barvení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
bakterie	bakterie	k1gFnSc1	bakterie
grampozitivní	grampozitivní	k2eAgFnSc1d1	grampozitivní
(	(	kIx(	(
<g/>
G	G	kA	G
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
gramnegativní	gramnegativní	k2eAgFnSc1d1	gramnegativní
(	(	kIx(	(
<g/>
G	G	kA	G
<g/>
-	-	kIx~	-
<g/>
)	)	kIx)	)
a	a	k8xC	a
bez	bez	k7c2	bez
buněčné	buněčný	k2eAgFnSc2d1	buněčná
stěny	stěna	k1gFnSc2	stěna
(	(	kIx(	(
<g/>
Mollicutes	Mollicutes	k1gInSc1	Mollicutes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
mykobakterie	mykobakterie	k1gFnPc4	mykobakterie
(	(	kIx(	(
<g/>
Mycobacteria	Mycobacterium	k1gNnSc2	Mycobacterium
<g/>
)	)	kIx)	)
a	a	k8xC	a
nokardie	nokardie	k1gFnSc2	nokardie
(	(	kIx(	(
<g/>
Nocardia	Nocardium	k1gNnSc2	Nocardium
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zase	zase	k9	zase
používá	používat	k5eAaImIp3nS	používat
Ziehl-Neelsenovo	Ziehl-Neelsenův	k2eAgNnSc4d1	Ziehl-Neelsenův
barvení	barvení	k1gNnSc4	barvení
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
však	však	k9	však
nestačí	stačit	k5eNaBmIp3nS	stačit
ani	ani	k8xC	ani
barvit	barvit	k5eAaImF	barvit
vzorek	vzorek	k1gInSc4	vzorek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přistupuje	přistupovat	k5eAaImIp3nS	přistupovat
se	se	k3xPyFc4	se
ke	k	k7c3	k
kultivaci	kultivace	k1gFnSc3	kultivace
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
identifikaci	identifikace	k1gFnSc6	identifikace
bakterií	bakterie	k1gFnPc2	bakterie
také	také	k6eAd1	také
čím	co	k3yQnSc7	co
dál	daleko	k6eAd2	daleko
více	hodně	k6eAd2	hodně
používá	používat	k5eAaImIp3nS	používat
také	také	k9	také
genetických	genetický	k2eAgFnPc2d1	genetická
metod	metoda	k1gFnPc2	metoda
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
polymerázová	polymerázový	k2eAgFnSc1d1	polymerázová
řetězová	řetězový	k2eAgFnSc1d1	řetězová
reakce	reakce	k1gFnSc1	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc4	jejich
přesnost	přesnost	k1gFnSc4	přesnost
a	a	k8xC	a
rychlost	rychlost	k1gFnSc4	rychlost
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
kultivačními	kultivační	k2eAgFnPc7d1	kultivační
metodami	metoda	k1gFnPc7	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Systematika	systematika	k1gFnSc1	systematika
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
pojmenováváním	pojmenovávání	k1gNnSc7	pojmenovávání
bakteriálních	bakteriální	k2eAgInPc2d1	bakteriální
taxonů	taxon	k1gInPc2	taxon
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc7	jejich
seskupováním	seskupování	k1gNnSc7	seskupování
podle	podle	k7c2	podle
příbuznosti	příbuznost	k1gFnSc2	příbuznost
<g/>
.	.	kIx.	.
</s>
<s>
Klasifikace	klasifikace	k1gFnSc1	klasifikace
bakterií	bakterie	k1gFnPc2	bakterie
je	být	k5eAaImIp3nS	být
průběžně	průběžně	k6eAd1	průběžně
vydávána	vydáván	k2eAgFnSc1d1	vydávána
v	v	k7c6	v
International	International	k1gFnSc6	International
Journal	Journal	k1gFnPc2	Journal
of	of	k?	of
Systematic	Systematice	k1gFnPc2	Systematice
and	and	k?	and
Evolutionary	Evolutionar	k1gInPc1	Evolutionar
Microbiology	Microbiolog	k1gMnPc4	Microbiolog
(	(	kIx(	(
<g/>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
ročenka	ročenka	k1gFnSc1	ročenka
systematické	systematický	k2eAgFnSc2d1	systematická
a	a	k8xC	a
evoluční	evoluční	k2eAgFnSc2d1	evoluční
mikrobiologie	mikrobiologie	k1gFnSc2	mikrobiologie
<g/>
)	)	kIx)	)
a	a	k8xC	a
Bergey	Bergea	k1gFnSc2	Bergea
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Manual	Manual	k1gInSc1	Manual
of	of	k?	of
Systematic	Systematice	k1gFnPc2	Systematice
Bacteriology	Bacteriolog	k1gMnPc4	Bacteriolog
(	(	kIx(	(
<g/>
Bergeyho	Bergey	k1gMnSc4	Bergey
manuál	manuál	k1gInSc4	manuál
systematické	systematický	k2eAgFnSc2d1	systematická
bakteriologie	bakteriologie	k1gFnSc2	bakteriologie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
byla	být	k5eAaImAgFnS	být
systematika	systematika	k1gFnSc1	systematika
založena	založit	k5eAaPmNgFnS	založit
především	především	k9	především
na	na	k7c6	na
základě	základ	k1gInSc6	základ
morfologických	morfologický	k2eAgFnPc2d1	morfologická
a	a	k8xC	a
analytických	analytický	k2eAgFnPc2d1	analytická
metod	metoda	k1gFnPc2	metoda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
však	však	k9	však
dnes	dnes	k6eAd1	dnes
vytlačují	vytlačovat	k5eAaImIp3nP	vytlačovat
genetické	genetický	k2eAgFnPc4d1	genetická
metody	metoda	k1gFnPc4	metoda
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
jejich	jejich	k3xOp3gNnSc2	jejich
určování	určování	k1gNnSc2	určování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
používaným	používaný	k2eAgFnPc3d1	používaná
metodám	metoda	k1gFnPc3	metoda
patřilo	patřit	k5eAaImAgNnS	patřit
i	i	k9	i
Gramovo	Gramův	k2eAgNnSc1d1	Gramovo
barvení	barvení	k1gNnSc1	barvení
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
možnostmi	možnost	k1gFnPc7	možnost
bylo	být	k5eAaImAgNnS	být
dělení	dělení	k1gNnSc1	dělení
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozdílů	rozdíl	k1gInPc2	rozdíl
v	v	k7c6	v
buněčném	buněčný	k2eAgInSc6d1	buněčný
metabolismu	metabolismus	k1gInSc6	metabolismus
<g/>
,	,	kIx,	,
stavbě	stavba	k1gFnSc3	stavba
základních	základní	k2eAgFnPc2d1	základní
buněčných	buněčný	k2eAgFnPc2d1	buněčná
komponent	komponenta	k1gFnPc2	komponenta
(	(	kIx(	(
<g/>
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
,	,	kIx,	,
antigenů	antigen	k1gInPc2	antigen
<g/>
,	,	kIx,	,
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
metody	metoda	k1gFnPc1	metoda
však	však	k9	však
nezaručují	zaručovat	k5eNaImIp3nP	zaručovat
přirozenost	přirozenost	k1gFnSc4	přirozenost
taxonů	taxon	k1gInPc2	taxon
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
nich	on	k3xPp3gMnPc2	on
vytvořeny	vytvořit	k5eAaPmNgInP	vytvořit
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgFnPc1d1	dnešní
bakteriální	bakteriální	k2eAgFnPc1d1	bakteriální
klasifikační	klasifikační	k2eAgFnPc1d1	klasifikační
metody	metoda	k1gFnPc1	metoda
se	se	k3xPyFc4	se
soustředí	soustředit	k5eAaPmIp3nP	soustředit
především	především	k9	především
na	na	k7c4	na
molekulární	molekulární	k2eAgFnSc4d1	molekulární
systematiku	systematika	k1gFnSc4	systematika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
genetických	genetický	k2eAgFnPc2d1	genetická
metod	metoda	k1gFnPc2	metoda
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
sekvenace	sekvenace	k1gFnSc1	sekvenace
dlouhodoběji	dlouhodobě	k6eAd2	dlouhodobě
stabilních	stabilní	k2eAgFnPc2d1	stabilní
částí	část	k1gFnPc2	část
DNA	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
rRNA	rRNA	k?	rRNA
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
měřen	měřen	k2eAgInSc1d1	měřen
obsah	obsah	k1gInSc1	obsah
GC	GC	kA	GC
(	(	kIx(	(
<g/>
množství	množství	k1gNnSc1	množství
guaninu	guanin	k1gInSc2	guanin
a	a	k8xC	a
cytosinu	cytosina	k1gFnSc4	cytosina
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
DNA	DNA	kA	DNA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
zmíněných	zmíněný	k2eAgFnPc2d1	zmíněná
genetických	genetický	k2eAgFnPc2d1	genetická
metod	metoda	k1gFnPc2	metoda
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
domény	doména	k1gFnSc2	doména
bakterie	bakterie	k1gFnSc2	bakterie
identifikováno	identifikovat	k5eAaBmNgNnS	identifikovat
poměrně	poměrně	k6eAd1	poměrně
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
dán	dát	k5eAaPmNgInS	dát
a	a	k8xC	a
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
22	[number]	k4	22
<g/>
-	-	kIx~	-
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
</s>
<s>
Příbuzné	příbuzný	k2eAgInPc1d1	příbuzný
kmeny	kmen	k1gInPc1	kmen
se	se	k3xPyFc4	se
seskupují	seskupovat	k5eAaImIp3nP	seskupovat
do	do	k7c2	do
vývojových	vývojový	k2eAgFnPc2d1	vývojová
linií	linie	k1gFnPc2	linie
(	(	kIx(	(
<g/>
klád	kláda	k1gFnPc2	kláda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
známějších	známý	k2eAgInPc2d2	známější
systémů	systém	k1gInPc2	systém
je	být	k5eAaImIp3nS	být
ten	ten	k3xDgInSc1	ten
Cavalier-Smithův	Cavalier-Smithův	k2eAgInSc1d1	Cavalier-Smithův
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
růst	růst	k5eAaImF	růst
bakteriální	bakteriální	k2eAgFnPc1d1	bakteriální
populace	populace	k1gFnPc1	populace
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnSc1	bakterie
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
jim	on	k3xPp3gMnPc3	on
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
pro	pro	k7c4	pro
ně	on	k3xPp3gFnPc4	on
vhodné	vhodný	k2eAgFnPc4d1	vhodná
chemické	chemický	k2eAgFnPc4d1	chemická
i	i	k8xC	i
fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
rostou	růst	k5eAaImIp3nP	růst
a	a	k8xC	a
množí	množit	k5eAaImIp3nP	množit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Mechanismem	mechanismus	k1gInSc7	mechanismus
růstu	růst	k1gInSc2	růst
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
bakterií	bakterie	k1gFnPc2	bakterie
syntéza	syntéza	k1gFnSc1	syntéza
všech	všecek	k3xTgFnPc2	všecek
komponent	komponenta	k1gFnPc2	komponenta
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
hmotnost	hmotnost	k1gFnSc1	hmotnost
i	i	k8xC	i
objem	objem	k1gInSc1	objem
jedince	jedinec	k1gMnSc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
určitých	určitý	k2eAgInPc2d1	určitý
rozměrů	rozměr	k1gInPc2	rozměr
se	se	k3xPyFc4	se
bakterie	bakterie	k1gFnSc1	bakterie
rozdělí	rozdělit	k5eAaPmIp3nS	rozdělit
metodou	metoda	k1gFnSc7	metoda
binárního	binární	k2eAgNnSc2d1	binární
dělení	dělení	k1gNnSc2	dělení
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc1	typ
nepohlavního	pohlavní	k2eNgNnSc2d1	nepohlavní
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
děleními	dělení	k1gNnPc7	dělení
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
generační	generační	k2eAgFnSc1d1	generační
doba	doba	k1gFnSc1	doba
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dostatečném	dostatečný	k2eAgInSc6d1	dostatečný
počtu	počet	k1gInSc6	počet
bakterií	bakterie	k1gFnPc2	bakterie
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
populaci	populace	k1gFnSc6	populace
lze	lze	k6eAd1	lze
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c4	o
kolonii	kolonie	k1gFnSc4	kolonie
<g/>
,	,	kIx,	,
doba	doba	k1gFnSc1	doba
potřebná	potřebný	k2eAgFnSc1d1	potřebná
k	k	k7c3	k
zdvojnásobení	zdvojnásobení	k1gNnSc3	zdvojnásobení
počtu	počet	k1gInSc2	počet
buněk	buňka	k1gFnPc2	buňka
v	v	k7c6	v
kolonii	kolonie	k1gFnSc6	kolonie
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
doba	doba	k1gFnSc1	doba
zdvojení	zdvojení	k1gNnSc2	zdvojení
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnPc1	bakterie
se	se	k3xPyFc4	se
rozmnožují	rozmnožovat	k5eAaImIp3nP	rozmnožovat
nejčastěji	často	k6eAd3	často
binárním	binární	k2eAgNnSc7d1	binární
dělením	dělení	k1gNnSc7	dělení
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
však	však	k9	však
několik	několik	k4yIc1	několik
případů	případ	k1gInPc2	případ
odlišného	odlišný	k2eAgInSc2d1	odlišný
typu	typ	k1gInSc2	typ
nepohlavního	pohlavní	k2eNgNnSc2d1	nepohlavní
množení	množení	k1gNnSc2	množení
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pučení	pučení	k1gNnSc1	pučení
<g/>
,	,	kIx,	,
tvorba	tvorba	k1gFnSc1	tvorba
hormogonií	hormogonie	k1gFnPc2	hormogonie
<g/>
,	,	kIx,	,
baeocyty	baeocyt	k1gInPc4	baeocyt
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
binárním	binární	k2eAgNnSc6d1	binární
dělení	dělení	k1gNnSc6	dělení
se	se	k3xPyFc4	se
buňka	buňka	k1gFnSc1	buňka
nejprve	nejprve	k6eAd1	nejprve
prodlouží	prodloužit	k5eAaPmIp3nS	prodloužit
na	na	k7c4	na
dvojnásobnou	dvojnásobný	k2eAgFnSc4d1	dvojnásobná
délku	délka	k1gFnSc4	délka
a	a	k8xC	a
replikuje	replikovat	k5eAaImIp3nS	replikovat
svou	svůj	k3xOyFgFnSc4	svůj
DNA	dna	k1gFnSc1	dna
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
se	se	k3xPyFc4	se
uprostřed	uprostřed	k6eAd1	uprostřed
začne	začít	k5eAaPmIp3nS	začít
vytvářet	vytvářet	k5eAaImF	vytvářet
septum	septum	k1gNnSc1	septum
(	(	kIx(	(
<g/>
přehrádka	přehrádka	k1gFnSc1	přehrádka
složená	složený	k2eAgFnSc1d1	složená
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
membrán	membrána	k1gFnPc2	membrána
a	a	k8xC	a
základu	základ	k1gInSc2	základ
buněčné	buněčný	k2eAgFnSc2d1	buněčná
stěny	stěna	k1gFnSc2	stěna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Septum	septum	k1gNnSc1	septum
vždy	vždy	k6eAd1	vždy
vzniká	vznikat	k5eAaImIp3nS	vznikat
růstem	růst	k1gInSc7	růst
dvou	dva	k4xCgFnPc2	dva
přepážek	přepážka	k1gFnPc2	přepážka
od	od	k7c2	od
protilehlých	protilehlý	k2eAgFnPc2d1	protilehlá
stran	strana	k1gFnPc2	strana
buňky	buňka	k1gFnSc2	buňka
do	do	k7c2	do
jejího	její	k3xOp3gNnSc2	její
centra	centrum	k1gNnSc2	centrum
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
části	část	k1gFnPc1	část
spojí	spojit	k5eAaPmIp3nP	spojit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
procesu	proces	k1gInSc6	proces
se	se	k3xPyFc4	se
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
různé	různý	k2eAgInPc1d1	různý
enzymy	enzym	k1gInPc1	enzym
(	(	kIx(	(
<g/>
např.	např.	kA	např.
transpeptidázy	transpeptidáza	k1gFnSc2	transpeptidáza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
z	z	k7c2	z
každé	každý	k3xTgFnSc2	každý
mateřské	mateřský	k2eAgFnSc2d1	mateřská
buňky	buňka	k1gFnSc2	buňka
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
jedna	jeden	k4xCgFnSc1	jeden
sesterská	sesterský	k2eAgFnSc1d1	sesterská
buňka	buňka	k1gFnSc1	buňka
<g/>
,	,	kIx,	,
při	při	k7c6	při
nedokončeném	dokončený	k2eNgNnSc6d1	nedokončené
dělení	dělení	k1gNnSc6	dělení
septa	septum	k1gNnSc2	septum
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
shluků	shluk	k1gInPc2	shluk
bakterií	bakterie	k1gFnPc2	bakterie
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
článek	článek	k1gInSc1	článek
koky	kok	k1gInPc1	kok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
streptokoků	streptokok	k1gInPc2	streptokok
jsou	být	k5eAaImIp3nP	být
místa	místo	k1gNnPc4	místo
růstu	růst	k1gInSc2	růst
vzájemně	vzájemně	k6eAd1	vzájemně
orientovaná	orientovaný	k2eAgFnSc1d1	orientovaná
pod	pod	k7c7	pod
úhlem	úhel	k1gInSc7	úhel
180	[number]	k4	180
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vznikají	vznikat	k5eAaImIp3nP	vznikat
řetězce	řetězec	k1gInPc4	řetězec
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
stafylokoků	stafylokok	k1gInPc2	stafylokok
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
úhel	úhel	k1gInSc1	úhel
90	[number]	k4	90
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
vznik	vznik	k1gInSc4	vznik
shluků	shluk	k1gInPc2	shluk
<g/>
.	.	kIx.	.
</s>
<s>
Vyjma	vyjma	k7c2	vyjma
příčného	příčný	k2eAgNnSc2d1	příčné
dělení	dělení	k1gNnSc2	dělení
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
ještě	ještě	k9	ještě
mnoho	mnoho	k4c1	mnoho
jiných	jiný	k2eAgInPc2d1	jiný
alternativních	alternativní	k2eAgInPc2d1	alternativní
způsobů	způsob	k1gInPc2	způsob
nepohlavního	pohlavní	k2eNgNnSc2d1	nepohlavní
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
způsobem	způsob	k1gInSc7	způsob
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
vytváření	vytváření	k1gNnSc4	vytváření
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jedné	jeden	k4xCgFnSc2	jeden
spory	spora	k1gFnSc2	spora
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
ze	z	k7c2	z
sporulace	sporulace	k1gFnSc2	sporulace
stává	stávat	k5eAaImIp3nS	stávat
de	de	k?	de
facto	facto	k1gNnSc1	facto
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
podmínek	podmínka	k1gFnPc2	podmínka
dělit	dělit	k5eAaImF	dělit
Anaerobacter	Anaerobacter	k1gInSc4	Anaerobacter
(	(	kIx(	(
<g/>
vzniká	vznikat	k5eAaImIp3nS	vznikat
najednou	najednou	k6eAd1	najednou
až	až	k9	až
sedm	sedm	k4xCc4	sedm
spor	spora	k1gFnPc2	spora
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
možností	možnost	k1gFnSc7	možnost
je	být	k5eAaImIp3nS	být
mnohonásobné	mnohonásobný	k2eAgNnSc4d1	mnohonásobné
dělení	dělení	k1gNnSc4	dělení
jedné	jeden	k4xCgFnSc2	jeden
buňky	buňka	k1gFnSc2	buňka
na	na	k7c4	na
mnoho	mnoho	k4c4	mnoho
dílčích	dílčí	k2eAgInPc2d1	dílčí
baeocytů	baeocyt	k1gInPc2	baeocyt
u	u	k7c2	u
sinic	sinice	k1gFnPc2	sinice
řádu	řád	k1gInSc2	řád
Pleurocapsales	Pleurocapsalesa	k1gFnPc2	Pleurocapsalesa
<g/>
.	.	kIx.	.
</s>
<s>
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
buňka	buňka	k1gFnSc1	buňka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mnoho	mnoho	k4c4	mnoho
kopií	kopie	k1gFnPc2	kopie
DNA	DNA	kA	DNA
a	a	k8xC	a
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
okamžiku	okamžik	k1gInSc6	okamžik
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
v	v	k7c6	v
rychlém	rychlý	k2eAgInSc6d1	rychlý
sledu	sled	k1gInSc6	sled
mnohonásobné	mnohonásobný	k2eAgNnSc4d1	mnohonásobné
dělení	dělení	k1gNnSc4	dělení
a	a	k8xC	a
z	z	k7c2	z
popraskané	popraskaný	k2eAgFnSc2d1	popraskaná
mateřské	mateřský	k2eAgFnSc2d1	mateřská
buňky	buňka	k1gFnSc2	buňka
se	se	k3xPyFc4	se
uvolní	uvolnit	k5eAaPmIp3nP	uvolnit
nové	nový	k2eAgFnPc1d1	nová
buňky	buňka	k1gFnPc1	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
pučení	pučení	k1gNnSc1	pučení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
například	například	k6eAd1	například
u	u	k7c2	u
sinic	sinice	k1gFnPc2	sinice
nebo	nebo	k8xC	nebo
u	u	k7c2	u
Planctomycetes	Planctomycetesa	k1gFnPc2	Planctomycetesa
<g/>
,	,	kIx,	,
probíhá	probíhat	k5eAaImIp3nS	probíhat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
zralá	zralý	k2eAgFnSc1d1	zralá
buňka	buňka	k1gFnSc1	buňka
začne	začít	k5eAaPmIp3nS	začít
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
povrchu	povrch	k1gInSc6	povrch
vytvářet	vytvářet	k5eAaImF	vytvářet
zcela	zcela	k6eAd1	zcela
novou	nový	k2eAgFnSc4d1	nová
buňku	buňka	k1gFnSc4	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
dceřiná	dceřiný	k2eAgFnSc1d1	dceřiná
buňka	buňka	k1gFnSc1	buňka
doroste	dorůst	k5eAaPmIp3nS	dorůst
do	do	k7c2	do
dostatečné	dostatečný	k2eAgFnSc2d1	dostatečná
velikosti	velikost	k1gFnSc2	velikost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
vpuštěna	vpuštěn	k2eAgFnSc1d1	vpuštěna
DNA	DNA	kA	DNA
a	a	k8xC	a
buňka	buňka	k1gFnSc1	buňka
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
podstatně	podstatně	k6eAd1	podstatně
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
mateřská	mateřský	k2eAgFnSc1d1	mateřská
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
odpojena	odpojen	k2eAgFnSc1d1	odpojena
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
doroste	dorůst	k5eAaPmIp3nS	dorůst
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
začít	začít	k5eAaPmF	začít
sama	sám	k3xTgFnSc1	sám
rozmnožovat	rozmnožovat	k5eAaImF	rozmnožovat
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
zde	zde	k6eAd1	zde
předchází	předcházet	k5eAaImIp3nS	předcházet
pučení	pučení	k1gNnSc4	pučení
replikace	replikace	k1gFnSc2	replikace
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
přisedlé	přisedlý	k2eAgFnPc1d1	přisedlá
bakterie	bakterie	k1gFnPc1	bakterie
(	(	kIx(	(
<g/>
např.	např.	kA	např.
sinice	sinice	k1gFnSc2	sinice
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
množí	množit	k5eAaImIp3nP	množit
pomocí	pomocí	k7c2	pomocí
hormogonií	hormogonie	k1gFnPc2	hormogonie
<g/>
,	,	kIx,	,
krátkých	krátký	k2eAgNnPc2d1	krátké
vláken	vlákno	k1gNnPc2	vlákno
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
oddělí	oddělit	k5eAaPmIp3nP	oddělit
od	od	k7c2	od
mateřského	mateřský	k2eAgInSc2d1	mateřský
řetězce	řetězec	k1gInSc2	řetězec
a	a	k8xC	a
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
v	v	k7c4	v
nový	nový	k2eAgInSc4d1	nový
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
spora	spora	k1gFnSc1	spora
(	(	kIx(	(
<g/>
bakterie	bakterie	k1gFnSc1	bakterie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
bakterie	bakterie	k1gFnPc1	bakterie
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
spory	spor	k1gInPc4	spor
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zvláštní	zvláštní	k2eAgFnPc1d1	zvláštní
buňky	buňka	k1gFnPc1	buňka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
dlouhodobému	dlouhodobý	k2eAgNnSc3d1	dlouhodobé
přežití	přežití	k1gNnSc3	přežití
nepříznivých	příznivý	k2eNgFnPc2d1	nepříznivá
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc4	takový
bakterie	bakterie	k1gFnPc4	bakterie
nazýváme	nazývat	k5eAaImIp1nP	nazývat
sporulující	sporulující	k2eAgFnPc4d1	sporulující
<g/>
.	.	kIx.	.
</s>
<s>
Spory	spor	k1gInPc1	spor
tvoří	tvořit	k5eAaImIp3nP	tvořit
zejména	zejména	k9	zejména
některé	některý	k3yIgFnPc1	některý
grampozitivní	grampozitivní	k2eAgFnPc1d1	grampozitivní
bakterie	bakterie	k1gFnPc1	bakterie
bakterie	bakterie	k1gFnSc2	bakterie
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Firmicutes	Firmicutesa	k1gFnPc2	Firmicutesa
(	(	kIx(	(
<g/>
modelové	modelový	k2eAgInPc1d1	modelový
rody	rod	k1gInPc1	rod
Clostridium	Clostridium	k1gNnSc1	Clostridium
či	či	k8xC	či
Bacillus	Bacillus	k1gInSc1	Bacillus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
spory	spor	k1gInPc1	spor
nazýváme	nazývat	k5eAaImIp1nP	nazývat
endospory	endospora	k1gFnPc4	endospora
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vznikají	vznikat	k5eAaImIp3nP	vznikat
uvnitř	uvnitř	k7c2	uvnitř
buňky	buňka	k1gFnSc2	buňka
mateřské	mateřský	k2eAgFnSc2d1	mateřská
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
vždy	vždy	k6eAd1	vždy
jen	jen	k6eAd1	jen
jedna	jeden	k4xCgFnSc1	jeden
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
buňce	buňka	k1gFnSc6	buňka
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
okolní	okolní	k2eAgFnSc1d1	okolní
buňka	buňka	k1gFnSc1	buňka
mateřská	mateřský	k2eAgFnSc1d1	mateřská
se	se	k3xPyFc4	se
posléze	posléze	k6eAd1	posléze
rozpustí	rozpustit	k5eAaPmIp3nS	rozpustit
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
schopnost	schopnost	k1gFnSc1	schopnost
vydržet	vydržet	k5eAaPmF	vydržet
nehostinné	hostinný	k2eNgFnPc4d1	nehostinná
podmínky	podmínka	k1gFnPc4	podmínka
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
kryptobióza	kryptobióza	k1gFnSc1	kryptobióza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
mohou	moct	k5eAaImIp3nP	moct
spory	spor	k1gInPc1	spor
přežít	přežít	k5eAaPmF	přežít
extrémně	extrémně	k6eAd1	extrémně
vysoké	vysoký	k2eAgFnPc4d1	vysoká
teploty	teplota	k1gFnPc4	teplota
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
vydrží	vydržet	k5eAaPmIp3nS	vydržet
až	až	k9	až
několik	několik	k4yIc1	několik
hodin	hodina	k1gFnPc2	hodina
varu	var	k1gInSc2	var
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
radiaci	radiace	k1gFnSc4	radiace
<g/>
,	,	kIx,	,
kyselost	kyselost	k1gFnSc4	kyselost
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
dezinfekční	dezinfekční	k2eAgFnPc1d1	dezinfekční
látky	látka	k1gFnPc1	látka
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
bakteriální	bakteriální	k2eAgInPc1d1	bakteriální
spory	spor	k1gInPc1	spor
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
stávají	stávat	k5eAaImIp3nP	stávat
nejodolnějšími	odolný	k2eAgFnPc7d3	nejodolnější
známými	známý	k2eAgFnPc7d1	známá
buňkami	buňka	k1gFnPc7	buňka
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Endospory	endospora	k1gFnPc1	endospora
se	se	k3xPyFc4	se
za	za	k7c2	za
příznivějších	příznivý	k2eAgFnPc2d2	příznivější
okolností	okolnost	k1gFnPc2	okolnost
opět	opět	k6eAd1	opět
změní	změnit	k5eAaPmIp3nS	změnit
ve	v	k7c4	v
vegetativní	vegetativní	k2eAgFnPc4d1	vegetativní
buňky	buňka	k1gFnPc4	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Spory	spor	k1gInPc1	spor
odlišného	odlišný	k2eAgInSc2d1	odlišný
typu	typ	k1gInSc2	typ
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
jiné	jiný	k2eAgNnSc1d1	jiné
<g/>
,	,	kIx,	,
také	také	k9	také
grampozitivní	grampozitivní	k2eAgFnSc1d1	grampozitivní
bakterie	bakterie	k1gFnSc1	bakterie
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
aktinomycety	aktinomycet	k2eAgFnPc1d1	aktinomycet
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
vznikají	vznikat	k5eAaImIp3nP	vznikat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
vlákna	vlákno	k1gNnSc2	vlákno
aktinomycet	aktinomycet	k5eAaImF	aktinomycet
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
endospory	endospora	k1gFnPc4	endospora
<g/>
,	,	kIx,	,
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
mají	mít	k5eAaImIp3nP	mít
mírně	mírně	k6eAd1	mírně
odlišné	odlišný	k2eAgFnPc1d1	odlišná
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc7	třetí
skupinou	skupina	k1gFnSc7	skupina
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
sporuluje	sporulovat	k5eAaBmIp3nS	sporulovat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
bakterie	bakterie	k1gFnPc1	bakterie
rodu	rod	k1gInSc2	rod
Azotobacter	Azotobacter	k1gInSc1	Azotobacter
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
však	však	k9	však
odolné	odolný	k2eAgInPc1d1	odolný
útvary	útvar	k1gInPc1	útvar
spíše	spíše	k9	spíše
nazývají	nazývat	k5eAaImIp3nP	nazývat
cysty	cysta	k1gFnPc1	cysta
<g/>
.	.	kIx.	.
</s>
<s>
Cysty	cysta	k1gFnPc1	cysta
mají	mít	k5eAaImIp3nP	mít
zpomalený	zpomalený	k2eAgInSc4d1	zpomalený
metabolismus	metabolismus	k1gInSc4	metabolismus
a	a	k8xC	a
ztloustlou	ztloustlý	k2eAgFnSc4d1	ztloustlá
buněčnou	buněčný	k2eAgFnSc4d1	buněčná
stěnu	stěna	k1gFnSc4	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
živná	živný	k2eAgFnSc1d1	živná
půda	půda	k1gFnSc1	půda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
se	se	k3xPyFc4	se
mnohdy	mnohdy	k6eAd1	mnohdy
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
důvodů	důvod	k1gInPc2	důvod
bakterie	bakterie	k1gFnSc2	bakterie
kultivují	kultivovat	k5eAaImIp3nP	kultivovat
v	v	k7c6	v
umělém	umělý	k2eAgNnSc6d1	umělé
prostředí	prostředí	k1gNnSc6	prostředí
(	(	kIx(	(
<g/>
médiu	médium	k1gNnSc6	médium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
prostředí	prostředí	k1gNnSc1	prostředí
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
vhodné	vhodný	k2eAgFnPc4d1	vhodná
chemické	chemický	k2eAgFnPc4d1	chemická
a	a	k8xC	a
fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
pro	pro	k7c4	pro
určitý	určitý	k2eAgInSc4d1	určitý
typ	typ	k1gInSc4	typ
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Médium	médium	k1gNnSc1	médium
musí	muset	k5eAaImIp3nS	muset
proto	proto	k8xC	proto
obsahovat	obsahovat	k5eAaImF	obsahovat
všechny	všechen	k3xTgFnPc4	všechen
nezbytné	zbytný	k2eNgFnPc4d1	zbytný
živiny	živina	k1gFnPc4	živina
nutné	nutný	k2eAgFnPc4d1	nutná
pro	pro	k7c4	pro
život	život	k1gInSc4	život
a	a	k8xC	a
růst	růst	k1gInSc4	růst
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
bakterie	bakterie	k1gFnSc1	bakterie
požaduje	požadovat	k5eAaImIp3nS	požadovat
určité	určitý	k2eAgFnPc4d1	určitá
minerální	minerální	k2eAgFnPc4d1	minerální
látky	látka	k1gFnPc4	látka
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
přidávají	přidávat	k5eAaImIp3nP	přidávat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
různých	různý	k2eAgFnPc2d1	různá
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Kultivační	kultivační	k2eAgNnPc4d1	kultivační
média	médium	k1gNnPc4	médium
prošla	projít	k5eAaPmAgFnS	projít
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
Pasteura	Pasteur	k1gMnSc2	Pasteur
značným	značný	k2eAgInSc7d1	značný
vývojem	vývoj	k1gInSc7	vývoj
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
živná	živný	k2eAgFnSc1d1	živná
půda	půda	k1gFnSc1	půda
pro	pro	k7c4	pro
mikroorganismy	mikroorganismus	k1gInPc4	mikroorganismus
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgInP	používat
vývary	vývar	k1gInPc1	vývar
z	z	k7c2	z
kvasnic	kvasnice	k1gFnPc2	kvasnice
<g/>
,	,	kIx,	,
komorová	komorový	k2eAgFnSc1d1	komorová
voda	voda	k1gFnSc1	voda
z	z	k7c2	z
očí	oko	k1gNnPc2	oko
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
agar	agar	k1gInSc1	agar
(	(	kIx(	(
<g/>
polysacharid	polysacharid	k1gInSc1	polysacharid
z	z	k7c2	z
ruduch	ruducha	k1gFnPc2	ruducha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klinické	klinický	k2eAgFnSc6d1	klinická
bakteriologii	bakteriologie	k1gFnSc6	bakteriologie
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
základem	základ	k1gInSc7	základ
většiny	většina	k1gFnSc2	většina
živných	živný	k2eAgNnPc2d1	živné
médií	médium	k1gNnPc2	médium
krevní	krevní	k2eAgInSc1d1	krevní
agar	agar	k1gInSc1	agar
připravovaný	připravovaný	k2eAgInSc1d1	připravovaný
smíšením	smíšení	k1gNnSc7	smíšení
ovčí	ovčí	k2eAgFnSc2d1	ovčí
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
agaru	agar	k1gInSc2	agar
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dále	daleko	k6eAd2	daleko
upravován	upravovat	k5eAaImNgInS	upravovat
pro	pro	k7c4	pro
určité	určitý	k2eAgFnPc4d1	určitá
skupiny	skupina	k1gFnPc4	skupina
bakterií	bakterie	k1gFnPc2	bakterie
(	(	kIx(	(
<g/>
např.	např.	kA	např.
čokoládový	čokoládový	k2eAgInSc1d1	čokoládový
agar	agar	k1gInSc1	agar
pro	pro	k7c4	pro
meningokoky	meningokok	k1gInPc4	meningokok
<g/>
,	,	kIx,	,
Šulova	Šulův	k2eAgFnSc1d1	Šulův
půda	půda	k1gFnSc1	půda
pro	pro	k7c4	pro
mykobakterie	mykobakterie	k1gFnPc4	mykobakterie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takzvaná	takzvaný	k2eAgNnPc1d1	takzvané
selektivní	selektivní	k2eAgNnPc1d1	selektivní
média	médium	k1gNnPc1	médium
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
získat	získat	k5eAaPmF	získat
čistou	čistý	k2eAgFnSc4d1	čistá
kulturu	kultura	k1gFnSc4	kultura
určitých	určitý	k2eAgFnPc2d1	určitá
bakterií	bakterie	k1gFnPc2	bakterie
-	-	kIx~	-
například	například	k6eAd1	například
po	po	k7c6	po
přidání	přidání	k1gNnSc6	přidání
10	[number]	k4	10
<g/>
%	%	kIx~	%
roztoku	roztok	k1gInSc3	roztok
chloridu	chlorid	k1gInSc2	chlorid
sodného	sodný	k2eAgInSc2d1	sodný
vypěstujeme	vypěstovat	k5eAaPmIp1nP	vypěstovat
kulturu	kultura	k1gFnSc4	kultura
stafylokoků	stafylokok	k1gInPc2	stafylokok
<g/>
.	.	kIx.	.
</s>
<s>
Diagnostické	diagnostický	k2eAgNnSc1d1	diagnostické
médium	médium	k1gNnSc1	médium
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
diagnostikovat	diagnostikovat	k5eAaBmF	diagnostikovat
určitý	určitý	k2eAgInSc4d1	určitý
druh	druh	k1gInSc4	druh
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
např.	např.	kA	např.
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gInSc2	jejich
metabolického	metabolický	k2eAgInSc2d1	metabolický
produktu	produkt	k1gInSc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
bakterií	bakterie	k1gFnPc2	bakterie
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jediný	jediný	k2eAgInSc1d1	jediný
nukleoid	nukleoid	k1gInSc1	nukleoid
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
bakteriální	bakteriální	k2eAgInSc1d1	bakteriální
chromozom	chromozom	k1gInSc1	chromozom
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
kruhovou	kruhový	k2eAgFnSc4d1	kruhová
molekulu	molekula	k1gFnSc4	molekula
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
nukleových	nukleový	k2eAgFnPc2d1	nukleová
bází	báze	k1gFnPc2	báze
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
spirochéty	spirochéta	k1gFnPc1	spirochéta
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Borrelia	Borrelium	k1gNnSc2	Borrelium
mající	mající	k2eAgInSc4d1	mající
nukleoid	nukleoid	k1gInSc4	nukleoid
lineárního	lineární	k2eAgNnSc2d1	lineární
(	(	kIx(	(
<g/>
nikoliv	nikoliv	k9	nikoliv
kruhového	kruhový	k2eAgMnSc2d1	kruhový
<g/>
)	)	kIx)	)
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
bází	báze	k1gFnPc2	báze
sahá	sahat	k5eAaImIp3nS	sahat
od	od	k7c2	od
112	[number]	k4	112
000	[number]	k4	000
komplementárních	komplementární	k2eAgInPc2d1	komplementární
párů	pár	k1gInPc2	pár
bází	báze	k1gFnPc2	báze
(	(	kIx(	(
<g/>
u	u	k7c2	u
Nasuia	Nasuium	k1gNnSc2	Nasuium
deltocephalinicola	deltocephalinicola	k1gFnSc1	deltocephalinicola
<g/>
)	)	kIx)	)
až	až	k6eAd1	až
k	k	k7c3	k
12	[number]	k4	12
200	[number]	k4	200
000	[number]	k4	000
párům	pár	k1gInPc3	pár
u	u	k7c2	u
půdní	půdní	k2eAgFnSc2d1	půdní
bakterie	bakterie	k1gFnSc2	bakterie
Sorangium	Sorangium	k1gNnSc1	Sorangium
cellulosum	cellulosum	k1gInSc1	cellulosum
<g/>
.	.	kIx.	.
</s>
<s>
Geny	gen	k1gInPc1	gen
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
i	i	k9	i
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
bakterií	bakterie	k1gFnPc2	bakterie
existují	existovat	k5eAaImIp3nP	existovat
introny	intron	k1gInPc4	intron
(	(	kIx(	(
<g/>
nepřekládané	překládaný	k2eNgFnPc4d1	nepřekládaná
části	část	k1gFnPc4	část
genů	gen	k1gInPc2	gen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
bakterie	bakterie	k1gFnPc1	bakterie
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
plazmidy	plazmida	k1gFnPc4	plazmida
<g/>
,	,	kIx,	,
izolované	izolovaný	k2eAgFnPc4d1	izolovaná
části	část	k1gFnPc4	část
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgInSc4d1	obsahující
vždy	vždy	k6eAd1	vždy
jen	jen	k9	jen
několik	několik	k4yIc1	několik
málo	málo	k4c1	málo
genů	gen	k1gInPc2	gen
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnSc1	bakterie
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
nich	on	k3xPp3gMnPc2	on
sice	sice	k8xC	sice
zpravidla	zpravidla	k6eAd1	zpravidla
obejdou	obejít	k5eAaPmIp3nP	obejít
<g/>
,	,	kIx,	,
plazmidy	plazmida	k1gFnPc1	plazmida
však	však	k9	však
slouží	sloužit	k5eAaImIp3nP	sloužit
bakteriím	bakterie	k1gFnPc3	bakterie
například	například	k6eAd1	například
pro	pro	k7c4	pro
rezistenci	rezistence	k1gFnSc4	rezistence
k	k	k7c3	k
antibiotikům	antibiotikum	k1gNnPc3	antibiotikum
<g/>
,	,	kIx,	,
k	k	k7c3	k
fixaci	fixace	k1gFnSc3	fixace
vzdušného	vzdušný	k2eAgInSc2d1	vzdušný
dusíku	dusík	k1gInSc2	dusík
a	a	k8xC	a
k	k	k7c3	k
jiným	jiný	k2eAgInPc3d1	jiný
specializovaným	specializovaný	k2eAgInPc3d1	specializovaný
účelům	účel	k1gInPc3	účel
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
svým	svůj	k3xOyFgMnPc3	svůj
hostitelům	hostitel	k1gMnPc3	hostitel
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
selekční	selekční	k2eAgFnSc4d1	selekční
výhodu	výhoda	k1gFnSc4	výhoda
<g/>
.	.	kIx.	.
</s>
<s>
Plazmidy	Plazmida	k1gFnPc1	Plazmida
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
mezi	mezi	k7c7	mezi
bakteriemi	bakterie	k1gFnPc7	bakterie
horizontálně	horizontálně	k6eAd1	horizontálně
přenášet	přenášet	k5eAaImF	přenášet
procesy	proces	k1gInPc4	proces
transformací	transformace	k1gFnPc2	transformace
<g/>
,	,	kIx,	,
konjugací	konjugace	k1gFnPc2	konjugace
a	a	k8xC	a
transdukcí	transdukce	k1gFnPc2	transdukce
<g/>
.	.	kIx.	.
</s>
<s>
Geny	gen	k1gInPc1	gen
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
transkripce	transkripce	k1gFnSc2	transkripce
přepisovány	přepisovat	k5eAaImNgInP	přepisovat
do	do	k7c2	do
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
typů	typ	k1gInPc2	typ
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
mRNA	mRNA	k?	mRNA
<g/>
,	,	kIx,	,
rRNA	rRNA	k?	rRNA
či	či	k8xC	či
tRNA	trnout	k5eAaImSgMnS	trnout
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
mRNA	mRNA	k?	mRNA
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
syntetizují	syntetizovat	k5eAaImIp3nP	syntetizovat
proteiny	protein	k1gInPc1	protein
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
translace	translace	k1gFnSc2	translace
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnPc1	bakterie
se	se	k3xPyFc4	se
rozmnožují	rozmnožovat	k5eAaImIp3nP	rozmnožovat
nepohlavně	pohlavně	k6eNd1	pohlavně
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
dědí	dědit	k5eAaImIp3nP	dědit
identické	identický	k2eAgFnPc4d1	identická
kopie	kopie	k1gFnPc4	kopie
genomů	genom	k1gInPc2	genom
svých	svůj	k3xOyFgMnPc2	svůj
rodičů	rodič	k1gMnPc2	rodič
(	(	kIx(	(
<g/>
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
klony	klon	k1gInPc1	klon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
DNA	dna	k1gFnSc1	dna
může	moct	k5eAaImIp3nS	moct
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
díky	díky	k7c3	díky
rekombinaci	rekombinace	k1gFnSc3	rekombinace
či	či	k8xC	či
mutacím	mutace	k1gFnPc3	mutace
<g/>
.	.	kIx.	.
</s>
<s>
Mutace	mutace	k1gFnPc1	mutace
jsou	být	k5eAaImIp3nP	být
způsobeny	způsobit	k5eAaPmNgFnP	způsobit
chybami	chyba	k1gFnPc7	chyba
při	při	k7c6	při
replikaci	replikace	k1gFnSc6	replikace
DNA	DNA	kA	DNA
a	a	k8xC	a
vystavením	vystavení	k1gNnSc7	vystavení
různým	různý	k2eAgInPc3d1	různý
mutagenům	mutagen	k1gInPc3	mutagen
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
bakteriemi	bakterie	k1gFnPc7	bakterie
dochází	docházet	k5eAaImIp3nP	docházet
k	k	k7c3	k
výměně	výměna	k1gFnSc3	výměna
genetického	genetický	k2eAgInSc2d1	genetický
materiálu	materiál	k1gInSc2	materiál
horizontálním	horizontální	k2eAgInSc7d1	horizontální
přenosem	přenos	k1gInSc7	přenos
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
konjugace	konjugace	k1gFnSc2	konjugace
(	(	kIx(	(
<g/>
biologie	biologie	k1gFnSc1	biologie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
transdukce	transdukce	k1gFnSc1	transdukce
(	(	kIx(	(
<g/>
genetika	genetika	k1gFnSc1	genetika
<g/>
)	)	kIx)	)
a	a	k8xC	a
transformace	transformace	k1gFnSc1	transformace
(	(	kIx(	(
<g/>
genetika	genetika	k1gFnSc1	genetika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnPc1	bakterie
nemají	mít	k5eNaImIp3nP	mít
systém	systém	k1gInSc1	systém
pohlaví	pohlaví	k1gNnSc2	pohlaví
podobný	podobný	k2eAgInSc1d1	podobný
eukaryotickým	eukaryotický	k2eAgInPc3d1	eukaryotický
organismům	organismus	k1gInPc3	organismus
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
mohou	moct	k5eAaImIp3nP	moct
si	se	k3xPyFc3	se
navzájem	navzájem	k6eAd1	navzájem
vyměňovat	vyměňovat	k5eAaImF	vyměňovat
část	část	k1gFnSc4	část
svojí	svůj	k3xOyFgFnSc2	svůj
genetické	genetický	k2eAgFnSc2d1	genetická
informace	informace	k1gFnSc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
konjugace	konjugace	k1gFnSc1	konjugace
<g/>
.	.	kIx.	.
</s>
<s>
Přenos	přenos	k1gInSc1	přenos
probíhá	probíhat	k5eAaImIp3nS	probíhat
jedním	jeden	k4xCgInSc7	jeden
směrem	směr	k1gInSc7	směr
<g/>
:	:	kIx,	:
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
bakterií	bakterie	k1gFnPc2	bakterie
je	být	k5eAaImIp3nS	být
dárce	dárce	k1gMnSc1	dárce
čili	čili	k8xC	čili
donor	donor	k1gInSc1	donor
DNA	dno	k1gNnSc2	dno
(	(	kIx(	(
<g/>
nesprávně	správně	k6eNd1	správně
také	také	k9	také
"	"	kIx"	"
<g/>
samčí	samčí	k2eAgFnSc1d1	samčí
buňka	buňka	k1gFnSc1	buňka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
je	být	k5eAaImIp3nS	být
příjemce	příjemce	k1gMnSc1	příjemce
čili	čili	k8xC	čili
recipient	recipient	k1gMnSc1	recipient
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
samičí	samičí	k2eAgFnSc1d1	samičí
buňka	buňka	k1gFnSc1	buňka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výměna	výměna	k1gFnSc1	výměna
DNA	dno	k1gNnSc2	dno
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
spojením	spojení	k1gNnSc7	spojení
buněk	buňka	k1gFnPc2	buňka
přes	přes	k7c4	přes
sexuální	sexuální	k2eAgInPc4d1	sexuální
pilusy	pilus	k1gInPc4	pilus
<g/>
,	,	kIx,	,
vláknité	vláknitý	k2eAgInPc4d1	vláknitý
duté	dutý	k2eAgInPc4d1	dutý
útvary	útvar	k1gInPc4	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Schopnost	schopnost	k1gFnSc1	schopnost
tvořit	tvořit	k5eAaImF	tvořit
sexuální	sexuální	k2eAgInPc4d1	sexuální
pilusy	pilus	k1gInPc4	pilus
je	být	k5eAaImIp3nS	být
umožněna	umožnit	k5eAaPmNgFnS	umožnit
specifickými	specifický	k2eAgFnPc7d1	specifická
sekvencemi	sekvence	k1gFnPc7	sekvence
DNA	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
souhrnně	souhrnně	k6eAd1	souhrnně
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k8xC	jako
F-faktor	Faktor	k1gInSc4	F-faktor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
F-faktor	Faktor	k1gInSc4	F-faktor
jak	jak	k8xC	jak
část	část	k1gFnSc4	část
bakteriálního	bakteriální	k2eAgInSc2d1	bakteriální
chromozomu	chromozom	k1gInSc2	chromozom
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
plazmid	plazmid	k1gInSc1	plazmid
<g/>
.	.	kIx.	.
</s>
<s>
Dárce	dárce	k1gMnSc1	dárce
genetické	genetický	k2eAgFnSc2d1	genetická
informace	informace	k1gFnSc2	informace
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
F-faktor	Faktor	k1gInSc4	F-faktor
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
zván	zván	k2eAgMnSc1d1	zván
F	F	kA	F
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Konjugací	konjugace	k1gFnSc7	konjugace
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
genetická	genetický	k2eAgFnSc1d1	genetická
diverzita	diverzita	k1gFnSc1	diverzita
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnPc1	bakterie
mohou	moct	k5eAaImIp3nP	moct
genetický	genetický	k2eAgInSc4d1	genetický
materiál	materiál	k1gInSc4	materiál
přijímat	přijímat	k5eAaImF	přijímat
i	i	k9	i
z	z	k7c2	z
okolního	okolní	k2eAgNnSc2d1	okolní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
např.	např.	kA	např.
z	z	k7c2	z
mrtvých	mrtvý	k2eAgFnPc2d1	mrtvá
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
transformace	transformace	k1gFnSc1	transformace
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
transdukce	transdukce	k1gFnSc2	transdukce
zase	zase	k9	zase
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
znamená	znamenat	k5eAaImIp3nS	znamenat
přenos	přenos	k1gInSc1	přenos
genetického	genetický	k2eAgInSc2d1	genetický
materiálu	materiál	k1gInSc2	materiál
bakteriálními	bakteriální	k2eAgInPc7d1	bakteriální
viry	vir	k1gInPc7	vir
(	(	kIx(	(
<g/>
bakteriofágy	bakteriofág	k1gInPc7	bakteriofág
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
bakterií	bakterie	k1gFnPc2	bakterie
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
různých	různý	k2eAgInPc2d1	různý
metabolických	metabolický	k2eAgInPc2d1	metabolický
procesů	proces	k1gInPc2	proces
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
mnohem	mnohem	k6eAd1	mnohem
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
u	u	k7c2	u
eukaryotických	eukaryotický	k2eAgInPc2d1	eukaryotický
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
byl	být	k5eAaImAgInS	být
typ	typ	k1gInSc1	typ
metabolismu	metabolismus	k1gInSc2	metabolismus
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
základních	základní	k2eAgInPc2d1	základní
znaků	znak	k1gInPc2	znak
pro	pro	k7c4	pro
taxonomii	taxonomie	k1gFnSc4	taxonomie
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
taxonomie	taxonomie	k1gFnSc1	taxonomie
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
těchto	tento	k3xDgInPc6	tento
znacích	znak	k1gInPc6	znak
často	často	k6eAd1	často
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
skutečnému	skutečný	k2eAgInSc3d1	skutečný
fylogenetickému	fylogenetický	k2eAgInSc3d1	fylogenetický
vývoji	vývoj	k1gInSc3	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Metabolická	metabolický	k2eAgFnSc1d1	metabolická
aktivita	aktivita	k1gFnSc1	aktivita
je	být	k5eAaImIp3nS	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
podmíněná	podmíněný	k2eAgFnSc1d1	podmíněná
obsahem	obsah	k1gInSc7	obsah
biogenních	biogenní	k2eAgInPc2d1	biogenní
prvků	prvek	k1gInPc2	prvek
v	v	k7c6	v
substrátu	substrát	k1gInSc6	substrát
(	(	kIx(	(
<g/>
uhlík	uhlík	k1gInSc1	uhlík
<g/>
,	,	kIx,	,
dusík	dusík	k1gInSc1	dusík
<g/>
,	,	kIx,	,
síra	síra	k1gFnSc1	síra
<g/>
,	,	kIx,	,
fosfor	fosfor	k1gInSc1	fosfor
<g/>
)	)	kIx)	)
a	a	k8xC	a
vhodného	vhodný	k2eAgInSc2d1	vhodný
zdroje	zdroj	k1gInSc2	zdroj
energie	energie	k1gFnSc2	energie
k	k	k7c3	k
biosyntetickým	biosyntetický	k2eAgInPc3d1	biosyntetický
procesům	proces	k1gInPc3	proces
<g/>
.	.	kIx.	.
</s>
<s>
Auxotrofní	Auxotrofní	k2eAgInPc1d1	Auxotrofní
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
bakterie	bakterie	k1gFnPc1	bakterie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
si	se	k3xPyFc3	se
neumí	umět	k5eNaImIp3nS	umět
určitou	určitý	k2eAgFnSc4d1	určitá
sloučeninu	sloučenina	k1gFnSc4	sloučenina
syntetizovat	syntetizovat	k5eAaImF	syntetizovat
<g/>
,	,	kIx,	,
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
též	též	k9	též
některé	některý	k3yIgInPc1	některý
růstové	růstový	k2eAgInPc1d1	růstový
faktory	faktor	k1gInPc1	faktor
(	(	kIx(	(
<g/>
vitamíny	vitamín	k1gInPc1	vitamín
<g/>
,	,	kIx,	,
aminokyseliny	aminokyselina	k1gFnPc1	aminokyselina
<g/>
,	,	kIx,	,
purinové	purinový	k2eAgFnPc1d1	purinová
a	a	k8xC	a
pyrimidinové	pyrimidinový	k2eAgFnPc1d1	pyrimidinová
báze	báze	k1gFnPc1	báze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bakteriální	bakteriální	k2eAgInSc4d1	bakteriální
metabolismus	metabolismus	k1gInSc4	metabolismus
můžeme	moct	k5eAaImIp1nP	moct
rozdělit	rozdělit	k5eAaPmF	rozdělit
podle	podle	k7c2	podle
tří	tři	k4xCgNnPc2	tři
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
:	:	kIx,	:
druh	druh	k1gInSc1	druh
užívané	užívaný	k2eAgFnSc2d1	užívaná
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
zdroj	zdroj	k1gInSc4	zdroj
uhlíku	uhlík	k1gInSc2	uhlík
a	a	k8xC	a
donor	donor	k1gInSc4	donor
elektronů	elektron	k1gInPc2	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
druhu	druh	k1gInSc2	druh
užívané	užívaný	k2eAgFnSc2d1	užívaná
energie	energie	k1gFnSc2	energie
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
bakterie	bakterie	k1gFnPc1	bakterie
fototrofní	fototrofní	k2eAgFnPc1d1	fototrofní
a	a	k8xC	a
chemotrofní	chemotrofní	k2eAgFnPc1d1	chemotrofní
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
zdroje	zdroj	k1gInSc2	zdroj
uhlíku	uhlík	k1gInSc2	uhlík
na	na	k7c6	na
autotrofní	autotrofní	k2eAgFnSc6d1	autotrofní
a	a	k8xC	a
heterotrofní	heterotrofní	k2eAgFnSc6d1	heterotrofní
a	a	k8xC	a
dle	dle	k7c2	dle
donorů	donor	k1gInPc2	donor
elektronů	elektron	k1gInPc2	elektron
na	na	k7c6	na
litotrofní	litotrofní	k2eAgFnSc6d1	litotrofní
a	a	k8xC	a
organotrofní	organotrofní	k2eAgFnSc6d1	organotrofní
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
termíny	termín	k1gInPc1	termín
jsou	být	k5eAaImIp3nP	být
vzájemně	vzájemně	k6eAd1	vzájemně
kombinovatelné	kombinovatelný	k2eAgFnPc1d1	kombinovatelná
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
například	například	k6eAd1	například
cyanobakterie	cyanobakterie	k1gFnPc1	cyanobakterie
jsou	být	k5eAaImIp3nP	být
fotoautotrofní	fotoautotrofní	k2eAgFnPc1d1	fotoautotrofní
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
fototrofním	fototrofní	k2eAgFnPc3d1	fototrofní
bakteriím	bakterie	k1gFnPc3	bakterie
patří	patřit	k5eAaImIp3nS	patřit
především	především	k9	především
již	již	k6eAd1	již
zmíněné	zmíněný	k2eAgFnPc1d1	zmíněná
cyanobakterie	cyanobakterie	k1gFnPc1	cyanobakterie
(	(	kIx(	(
<g/>
sinice	sinice	k1gFnPc1	sinice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
zelené	zelený	k2eAgFnPc1d1	zelená
sirné	sirný	k2eAgFnPc1d1	sirná
bakterie	bakterie	k1gFnPc1	bakterie
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgFnPc1d1	zelená
nesirné	sirný	k2eNgFnPc1d1	sirný
bakterie	bakterie	k1gFnPc1	bakterie
<g/>
,	,	kIx,	,
heliobacterie	heliobacterie	k1gFnPc1	heliobacterie
<g/>
,	,	kIx,	,
purpurové	purpurový	k2eAgFnPc1d1	purpurová
bakterie	bakterie	k1gFnPc1	bakterie
a	a	k8xC	a
v	v	k7c6	v
r.	r.	kA	r.
2007	[number]	k4	2007
popsané	popsaný	k2eAgFnSc2d1	popsaná
chloracidobakterie	chloracidobakterie	k1gFnSc2	chloracidobakterie
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
za	za	k7c4	za
fotoautotrofy	fotoautotrof	k1gInPc4	fotoautotrof
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zpravidla	zpravidla	k6eAd1	zpravidla
získávají	získávat	k5eAaImIp3nP	získávat
uhlík	uhlík	k1gInSc4	uhlík
z	z	k7c2	z
anorganických	anorganický	k2eAgInPc2d1	anorganický
zdrojů	zdroj	k1gInPc2	zdroj
(	(	kIx(	(
<g/>
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
těchto	tento	k3xDgFnPc2	tento
bakterií	bakterie	k1gFnPc2	bakterie
probíhá	probíhat	k5eAaImIp3nS	probíhat
fotosyntéza	fotosyntéza	k1gFnSc1	fotosyntéza
<g/>
,	,	kIx,	,
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
pohání	pohánět	k5eAaImIp3nS	pohánět
energie	energie	k1gFnPc4	energie
ze	z	k7c2	z
slunečního	sluneční	k2eAgNnSc2d1	sluneční
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
fixace	fixace	k1gFnSc1	fixace
uhlíku	uhlík	k1gInSc2	uhlík
v	v	k7c6	v
organických	organický	k2eAgFnPc6d1	organická
sloučeninách	sloučenina	k1gFnPc6	sloučenina
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
Calvinova	Calvinův	k2eAgInSc2d1	Calvinův
cyklu	cyklus	k1gInSc2	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Donorem	donor	k1gInSc7	donor
elektronů	elektron	k1gInPc2	elektron
pro	pro	k7c4	pro
redukci	redukce	k1gFnSc4	redukce
NADP	NADP	kA	NADP
<g/>
+	+	kIx~	+
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
voda	voda	k1gFnSc1	voda
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
oxygenní	oxygenní	k2eAgFnSc1d1	oxygenní
fotosyntéza	fotosyntéza	k1gFnSc1	fotosyntéza
<g/>
,	,	kIx,	,
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
se	se	k3xPyFc4	se
kyslík	kyslík	k1gInSc1	kyslík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnPc1d1	jiná
látky	látka	k1gFnPc1	látka
(	(	kIx(	(
<g/>
vodík	vodík	k1gInSc1	vodík
<g/>
,	,	kIx,	,
síra	síra	k1gFnSc1	síra
<g/>
,	,	kIx,	,
thiosulfát	thiosulfát	k1gInSc1	thiosulfát
<g/>
,	,	kIx,	,
sirovodík	sirovodík	k1gInSc1	sirovodík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgNnPc6	jenž
se	se	k3xPyFc4	se
kyslík	kyslík	k1gInSc1	kyslík
neuvolňuje	uvolňovat	k5eNaImIp3nS	uvolňovat
<g/>
.	.	kIx.	.
</s>
<s>
Sinice	sinice	k1gFnPc1	sinice
využívají	využívat	k5eAaImIp3nP	využívat
při	při	k7c6	při
fotosyntéze	fotosyntéza	k1gFnSc6	fotosyntéza
barviva	barvivo	k1gNnSc2	barvivo
chlorofylu	chlorofyl	k1gInSc2	chlorofyl
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
pigmentů	pigment	k1gInPc2	pigment
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
fotoautotrofy	fotoautotrof	k1gMnPc4	fotoautotrof
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
několik	několik	k4yIc1	několik
fotoheterotrofů	fotoheterotrof	k1gMnPc2	fotoheterotrof
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
sice	sice	k8xC	sice
také	také	k6eAd1	také
fotosyntetizují	fotosyntetizovat	k5eAaBmIp3nP	fotosyntetizovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k8xS	jako
zdroje	zdroj	k1gInPc1	zdroj
uhlíku	uhlík	k1gInSc2	uhlík
užívají	užívat	k5eAaImIp3nP	užívat
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
(	(	kIx(	(
<g/>
acetát	acetát	k1gInSc1	acetát
<g/>
,	,	kIx,	,
pyruvát	pyruvát	k1gInSc1	pyruvát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chemotrofní	Chemotrofní	k2eAgFnSc1d1	Chemotrofní
bakterie	bakterie	k1gFnSc1	bakterie
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
bakterie	bakterie	k1gFnSc1	bakterie
využívající	využívající	k2eAgFnSc2d1	využívající
energie	energie	k1gFnSc2	energie
z	z	k7c2	z
vhodných	vhodný	k2eAgFnPc2d1	vhodná
chemických	chemický	k2eAgFnPc2d1	chemická
reakcí	reakce	k1gFnPc2	reakce
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
rovněž	rovněž	k6eAd1	rovněž
dvojího	dvojí	k4xRgInSc2	dvojí
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Takzvané	takzvaný	k2eAgInPc1d1	takzvaný
chemoautotrofní	chemoautotrofní	k2eAgInPc1d1	chemoautotrofní
získávají	získávat	k5eAaImIp3nP	získávat
uhlík	uhlík	k1gInSc4	uhlík
z	z	k7c2	z
anorganických	anorganický	k2eAgFnPc2d1	anorganická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
typicky	typicky	k6eAd1	typicky
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
a	a	k8xC	a
chemoheterotrofní	chemoheterotrofní	k2eAgFnPc4d1	chemoheterotrofní
z	z	k7c2	z
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
chemoheterotrofním	chemoheterotrofní	k2eAgFnPc3d1	chemoheterotrofní
bakteriím	bakterie	k1gFnPc3	bakterie
řadíme	řadit	k5eAaImIp1nP	řadit
například	například	k6eAd1	například
rozkladače	rozkladač	k1gInPc4	rozkladač
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
(	(	kIx(	(
<g/>
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
saprofyty	saprofyt	k1gInPc4	saprofyt
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
bakterie	bakterie	k1gFnSc2	bakterie
podílející	podílející	k2eAgFnSc2d1	podílející
se	se	k3xPyFc4	se
na	na	k7c6	na
kvasných	kvasný	k2eAgInPc6d1	kvasný
procesech	proces	k1gInPc6	proces
<g/>
.	.	kIx.	.
</s>
<s>
Známe	znát	k5eAaImIp1nP	znát
však	však	k9	však
rovněž	rovněž	k9	rovněž
mnoho	mnoho	k4c4	mnoho
chemoautotrofních	chemoautotrofní	k2eAgFnPc2d1	chemoautotrofní
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
bakterie	bakterie	k1gFnSc2	bakterie
vodíkové	vodíkový	k2eAgFnSc2d1	vodíková
<g/>
,	,	kIx,	,
sirné	sirný	k2eAgFnSc2d1	sirná
<g/>
,	,	kIx,	,
bakterie	bakterie	k1gFnSc2	bakterie
oxidující	oxidující	k2eAgNnSc4d1	oxidující
železo	železo	k1gNnSc4	železo
či	či	k8xC	či
síru	síra	k1gFnSc4	síra
<g/>
,	,	kIx,	,
nitrifikační	nitrifikační	k2eAgFnSc4d1	nitrifikační
a	a	k8xC	a
další	další	k2eAgFnSc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnPc4	bakterie
díky	díky	k7c3	díky
svým	svůj	k3xOyFgInPc3	svůj
unikátním	unikátní	k2eAgInPc3d1	unikátní
metabolickým	metabolický	k2eAgInPc3d1	metabolický
pochodům	pochod	k1gInPc3	pochod
hrají	hrát	k5eAaImIp3nP	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
v	v	k7c6	v
koloběhu	koloběh	k1gInSc6	koloběh
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
v	v	k7c6	v
koloběhu	koloběh	k1gInSc6	koloběh
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
,	,	kIx,	,
dusíku	dusík	k1gInSc2	dusík
a	a	k8xC	a
síry	síra	k1gFnSc2	síra
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
podílejí	podílet	k5eAaImIp3nP	podílet
na	na	k7c6	na
rozkladu	rozklad	k1gInSc6	rozklad
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
bakterie	bakterie	k1gFnPc1	bakterie
dokáží	dokázat	k5eAaPmIp3nP	dokázat
z	z	k7c2	z
atmosféry	atmosféra	k1gFnSc2	atmosféra
fixovat	fixovat	k5eAaImF	fixovat
vzdušný	vzdušný	k2eAgInSc4d1	vzdušný
dusík	dusík	k1gInSc4	dusík
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Rhizobium	Rhizobium	k1gNnSc1	Rhizobium
<g/>
,	,	kIx,	,
či	či	k8xC	či
sinice	sinice	k1gFnSc1	sinice
Anabaena	Anabaena	k1gFnSc1	Anabaena
a	a	k8xC	a
Nostoc	Nostoc	k1gFnSc1	Nostoc
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
bakterií	bakterie	k1gFnPc2	bakterie
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
obsaženy	obsáhnout	k5eAaPmNgInP	obsáhnout
v	v	k7c6	v
hlízkách	hlízka	k1gFnPc6	hlízka
bobovitých	bobovitý	k2eAgFnPc2d1	bobovitá
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgInPc1d1	významný
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
bakterie	bakterie	k1gFnPc1	bakterie
<g/>
,	,	kIx,	,
zapojené	zapojený	k2eAgFnPc1d1	zapojená
v	v	k7c6	v
přeměně	přeměna	k1gFnSc6	přeměna
dusíkatých	dusíkatý	k2eAgFnPc2d1	dusíkatá
látek	látka	k1gFnPc2	látka
na	na	k7c4	na
dusitany	dusitan	k1gInPc4	dusitan
a	a	k8xC	a
dusičnany	dusičnan	k1gInPc4	dusičnan
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
vzdušný	vzdušný	k2eAgInSc4d1	vzdušný
dusík	dusík	k1gInSc4	dusík
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnPc1	bakterie
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
nezastupitelné	zastupitelný	k2eNgFnSc6d1	nezastupitelná
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
významu	význam	k1gInSc6	význam
pro	pro	k7c4	pro
koloběh	koloběh	k1gInSc4	koloběh
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
symbiotické	symbiotický	k2eAgInPc1d1	symbiotický
oboustranně	oboustranně	k6eAd1	oboustranně
prospěšné	prospěšný	k2eAgInPc1d1	prospěšný
organismy	organismus	k1gInPc1	organismus
či	či	k8xC	či
jako	jako	k9	jako
výrobní	výrobní	k2eAgInSc4d1	výrobní
prostředek	prostředek	k1gInSc4	prostředek
v	v	k7c6	v
biotechnologiích	biotechnologie	k1gFnPc6	biotechnologie
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
tu	tu	k6eAd1	tu
také	také	k9	také
bakterie	bakterie	k1gFnSc1	bakterie
způsobující	způsobující	k2eAgFnSc2d1	způsobující
choroby	choroba	k1gFnSc2	choroba
a	a	k8xC	a
bakterie	bakterie	k1gFnSc2	bakterie
podílející	podílející	k2eAgInPc1d1	podílející
se	se	k3xPyFc4	se
na	na	k7c6	na
rozkladu	rozklad	k1gInSc6	rozklad
mrtvé	mrtvý	k2eAgFnSc2d1	mrtvá
organické	organický	k2eAgFnSc2d1	organická
hmoty	hmota	k1gFnSc2	hmota
(	(	kIx(	(
<g/>
destruenti	destruent	k1gMnPc1	destruent
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hydrolytické	hydrolytický	k2eAgFnPc1d1	hydrolytická
bakterie	bakterie	k1gFnPc1	bakterie
jsou	být	k5eAaImIp3nP	být
zodpovědné	zodpovědný	k2eAgInPc1d1	zodpovědný
za	za	k7c4	za
hydrolýzu	hydrolýza	k1gFnSc4	hydrolýza
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
významně	významně	k6eAd1	významně
se	se	k3xPyFc4	se
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
bakterie	bakterie	k1gFnSc2	bakterie
zapojily	zapojit	k5eAaPmAgInP	zapojit
do	do	k7c2	do
vzniku	vznik	k1gInSc2	vznik
eukaryotických	eukaryotický	k2eAgFnPc2d1	eukaryotická
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnPc1	bakterie
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
své	svůj	k3xOyFgInPc4	svůj
patogeny	patogen	k1gInPc4	patogen
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
viry	vir	k1gInPc1	vir
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
bakteriofágy	bakteriofág	k1gInPc1	bakteriofág
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
endosymbiotická	endosymbiotický	k2eAgFnSc1d1	endosymbiotická
teorie	teorie	k1gFnSc1	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
endosymbiotické	endosymbiotický	k2eAgFnSc2d1	endosymbiotická
teorie	teorie	k1gFnSc2	teorie
se	se	k3xPyFc4	se
dávné	dávný	k2eAgFnSc2d1	dávná
bakterie	bakterie	k1gFnSc2	bakterie
podílely	podílet	k5eAaImAgInP	podílet
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
eukaryotické	eukaryotický	k2eAgFnSc2d1	eukaryotická
buňky	buňka	k1gFnSc2	buňka
z	z	k7c2	z
primitivních	primitivní	k2eAgFnPc2d1	primitivní
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
příbuzné	příbuzný	k2eAgFnPc1d1	příbuzná
dnešním	dnešní	k2eAgNnSc6d1	dnešní
archeím	archeit	k5eAaPmIp1nS	archeit
<g/>
.	.	kIx.	.
</s>
<s>
Alfaproteobakterie	Alfaproteobakterie	k1gFnPc1	Alfaproteobakterie
byly	být	k5eAaImAgFnP	být
pohlceny	pohlcen	k2eAgInPc1d1	pohlcen
protoeukaryotickými	protoeukaryotický	k2eAgFnPc7d1	protoeukaryotický
buňkami	buňka	k1gFnPc7	buňka
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
mitochondrií	mitochondrie	k1gFnPc2	mitochondrie
či	či	k8xC	či
hydrogenozomů	hydrogenozom	k1gInPc2	hydrogenozom
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
organely	organela	k1gFnPc1	organela
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
nachází	nacházet	k5eAaImIp3nS	nacházet
u	u	k7c2	u
všech	všecek	k3xTgFnPc2	všecek
známých	známý	k2eAgFnPc2d1	známá
eukaryot	eukaryota	k1gFnPc2	eukaryota
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
redukované	redukovaný	k2eAgFnSc6d1	redukovaná
podobě	podoba	k1gFnSc6	podoba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
vedlo	vést	k5eAaImAgNnS	vést
pohlcení	pohlcení	k1gNnSc1	pohlcení
dalších	další	k2eAgInPc2d1	další
endosymbiontů	endosymbiont	k1gInPc2	endosymbiont
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
bakterií	bakterie	k1gFnPc2	bakterie
příbuzných	příbuzný	k1gMnPc2	příbuzný
sinicím	sinice	k1gFnPc3	sinice
<g/>
,	,	kIx,	,
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
chloroplastů	chloroplast	k1gInPc2	chloroplast
řas	řasa	k1gFnPc2	řasa
a	a	k8xC	a
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
mutualistických	mutualistický	k2eAgFnPc2d1	mutualistická
(	(	kIx(	(
<g/>
oboustranně	oboustranně	k6eAd1	oboustranně
prospěšných	prospěšný	k2eAgInPc2d1	prospěšný
<g/>
)	)	kIx)	)
symbiotických	symbiotický	k2eAgInPc2d1	symbiotický
vztahů	vztah	k1gInPc2	vztah
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
se	se	k3xPyFc4	se
v	v	k7c6	v
symbiotických	symbiotický	k2eAgInPc6d1	symbiotický
svazcích	svazek	k1gInPc6	svazek
účastní	účastnit	k5eAaImIp3nS	účastnit
sinice	sinice	k1gFnSc1	sinice
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
vstupují	vstupovat	k5eAaImIp3nP	vstupovat
jak	jak	k8xS	jak
svazků	svazek	k1gInPc2	svazek
s	s	k7c7	s
houbami	houba	k1gFnPc7	houba
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
cyanolišejníky	cyanolišejník	k1gInPc1	cyanolišejník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
též	též	k9	též
tvoří	tvořit	k5eAaImIp3nP	tvořit
symbiotické	symbiotický	k2eAgInPc1d1	symbiotický
vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
vyššími	vysoký	k2eAgFnPc7d2	vyšší
rostlinami	rostlina	k1gFnPc7	rostlina
(	(	kIx(	(
<g/>
sinice	sinice	k1gFnPc1	sinice
jako	jako	k8xS	jako
fixátoři	fixátor	k1gMnPc1	fixátor
dusíku	dusík	k1gInSc2	dusík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Symbiotické	symbiotický	k2eAgFnPc1d1	symbiotická
sinice	sinice	k1gFnPc1	sinice
vegetující	vegetující	k2eAgFnSc1d1	vegetující
v	v	k7c6	v
tělech	tělo	k1gNnPc6	tělo
živočichů	živočich	k1gMnPc2	živočich
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
zoocyanely	zoocyanela	k1gFnPc1	zoocyanela
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
je	on	k3xPp3gFnPc4	on
například	například	k6eAd1	například
pláštěnci	pláštěnec	k1gMnPc1	pláštěnec
(	(	kIx(	(
<g/>
Tunicata	Tunicata	k1gFnSc1	Tunicata
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
sinic	sinice	k1gFnPc2	sinice
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
i	i	k9	i
mnoho	mnoho	k4c1	mnoho
heterotrofních	heterotrofní	k2eAgFnPc2d1	heterotrofní
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nP	účastnit
symbiotických	symbiotický	k2eAgInPc2d1	symbiotický
svazků	svazek	k1gInPc2	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgInPc1d1	významný
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
hlízkové	hlízkový	k2eAgFnPc1d1	hlízkový
bakterie	bakterie	k1gFnPc1	bakterie
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Rhizobium	Rhizobium	k1gNnSc4	Rhizobium
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
množství	množství	k1gNnSc1	množství
bakterií	bakterie	k1gFnPc2	bakterie
v	v	k7c6	v
tělních	tělní	k2eAgFnPc6d1	tělní
dutinách	dutina	k1gFnPc6	dutina
živočichů	živočich	k1gMnPc2	živočich
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
jako	jako	k8xC	jako
součást	součást	k1gFnSc4	součást
střevní	střevní	k2eAgFnSc2d1	střevní
mikroflóry	mikroflóra	k1gFnSc2	mikroflóra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Známy	znám	k2eAgInPc1d1	znám
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
i	i	k9	i
symbiotické	symbiotický	k2eAgFnPc4d1	symbiotická
bioluminiscenční	bioluminiscenční	k2eAgFnPc4d1	bioluminiscenční
bakterie	bakterie	k1gFnPc4	bakterie
<g/>
,	,	kIx,	,
bakterie	bakterie	k1gFnPc4	bakterie
trávící	trávící	k2eAgFnPc4d1	trávící
celulózu	celulóza	k1gFnSc4	celulóza
a	a	k8xC	a
mnohé	mnohé	k1gNnSc4	mnohé
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
i	i	k9	i
uvnitř	uvnitř	k7c2	uvnitř
těl	tělo	k1gNnPc2	tělo
organismů	organismus	k1gInPc2	organismus
žije	žít	k5eAaImIp3nS	žít
poměrně	poměrně	k6eAd1	poměrně
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nazýváme	nazývat	k5eAaImIp1nP	nazývat
komenzálické	komenzálický	k2eAgFnPc1d1	komenzálický
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
svému	svůj	k3xOyFgMnSc3	svůj
hostiteli	hostitel	k1gMnSc3	hostitel
výrazně	výrazně	k6eAd1	výrazně
neškodí	škodit	k5eNaImIp3nS	škodit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
výrazně	výrazně	k6eAd1	výrazně
neprospívají	prospívat	k5eNaImIp3nP	prospívat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidském	lidský	k2eAgNnSc6d1	lidské
těle	tělo	k1gNnSc6	tělo
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
bohatá	bohatý	k2eAgFnSc1d1	bohatá
mikroflóra	mikroflóra	k1gFnSc1	mikroflóra
na	na	k7c4	na
kůži	kůže	k1gFnSc4	kůže
<g/>
,	,	kIx,	,
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
trávicí	trávicí	k2eAgFnSc6d1	trávicí
soustavě	soustava	k1gFnSc6	soustava
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
v	v	k7c6	v
tlustém	tlustý	k2eAgNnSc6d1	tlusté
střevě	střevo	k1gNnSc6	střevo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
horních	horní	k2eAgFnPc6d1	horní
dýchacích	dýchací	k2eAgFnPc6d1	dýchací
cestách	cesta	k1gFnPc6	cesta
<g/>
,	,	kIx,	,
v	v	k7c6	v
uchu	ucho	k1gNnSc6	ucho
a	a	k8xC	a
oku	oko	k1gNnSc6	oko
<g/>
,	,	kIx,	,
močové	močový	k2eAgFnSc3d1	močová
trubici	trubice	k1gFnSc3	trubice
a	a	k8xC	a
vagíně	vagína	k1gFnSc3	vagína
(	(	kIx(	(
<g/>
vaginální	vaginální	k2eAgFnSc1d1	vaginální
flóra	flóra	k1gFnSc1	flóra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
střevní	střevní	k2eAgFnSc1d1	střevní
mikroflóra	mikroflóra	k1gFnSc1	mikroflóra
mnoha	mnoho	k4c2	mnoho
obratlovců	obratlovec	k1gMnPc2	obratlovec
včetně	včetně	k7c2	včetně
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
tvořená	tvořený	k2eAgFnSc1d1	tvořená
právě	právě	k9	právě
komenzálickými	komenzálický	k2eAgFnPc7d1	komenzálický
bakteriemi	bakterie	k1gFnPc7	bakterie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
tlustém	tlustý	k2eAgNnSc6d1	tlusté
střevě	střevo	k1gNnSc6	střevo
přítomny	přítomen	k2eAgInPc1d1	přítomen
v	v	k7c6	v
obrovských	obrovský	k2eAgInPc6d1	obrovský
počtech	počet	k1gInPc6	počet
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
bakterií	bakterie	k1gFnPc2	bakterie
v	v	k7c6	v
gramu	gram	k1gInSc6	gram
střevní	střevní	k2eAgFnSc2d1	střevní
tráveniny	trávenina	k1gFnSc2	trávenina
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
1012	[number]	k4	1012
a	a	k8xC	a
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
celkové	celkový	k2eAgNnSc1d1	celkové
množství	množství	k1gNnSc1	množství
střevních	střevní	k2eAgFnPc2d1	střevní
bakterií	bakterie	k1gFnPc2	bakterie
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
počtu	počet	k1gInSc2	počet
lidských	lidský	k2eAgFnPc2d1	lidská
buněk	buňka	k1gFnPc2	buňka
daného	daný	k2eAgMnSc2d1	daný
jedince	jedinec	k1gMnSc2	jedinec
či	či	k8xC	či
ho	on	k3xPp3gMnSc4	on
dokonce	dokonce	k9	dokonce
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
komenzálické	komenzálický	k2eAgFnPc1d1	komenzálický
bakterie	bakterie	k1gFnPc1	bakterie
přechází	přecházet	k5eAaImIp3nP	přecházet
v	v	k7c4	v
patogeny	patogen	k1gInPc4	patogen
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
stávají	stávat	k5eAaImIp3nP	stávat
mutualisty	mutualista	k1gMnPc4	mutualista
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
bakterií	bakterie	k1gFnPc2	bakterie
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
mikrobiální	mikrobiální	k2eAgInSc1d1	mikrobiální
povlak	povlak	k1gInSc1	povlak
i	i	k9	i
na	na	k7c6	na
lidské	lidský	k2eAgFnSc6d1	lidská
kůži	kůže	k1gFnSc6	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
lidská	lidský	k2eAgFnSc1d1	lidská
pokožka	pokožka	k1gFnSc1	pokožka
suchá	suchý	k2eAgFnSc1d1	suchá
<g/>
,	,	kIx,	,
slaná	slaný	k2eAgFnSc1d1	slaná
a	a	k8xC	a
kyselá	kyselý	k2eAgFnSc1d1	kyselá
<g/>
,	,	kIx,	,
roste	růst	k5eAaImIp3nS	růst
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
velké	velká	k1gFnPc1	velká
množství	množství	k1gNnSc2	množství
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Corynebacterium	Corynebacterium	k1gNnSc1	Corynebacterium
<g/>
,	,	kIx,	,
Staphylococcus	Staphylococcus	k1gInSc1	Staphylococcus
<g/>
,	,	kIx,	,
Micrococcus	Micrococcus	k1gInSc1	Micrococcus
a	a	k8xC	a
mnohé	mnohé	k1gNnSc1	mnohé
další	další	k2eAgNnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
kožní	kožní	k2eAgFnSc2d1	kožní
bakterie	bakterie	k1gFnSc2	bakterie
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
potních	potní	k2eAgFnPc2d1	potní
žláz	žláza	k1gFnPc2	žláza
a	a	k8xC	a
u	u	k7c2	u
kořene	kořen	k1gInSc2	kořen
vlasů	vlas	k1gInPc2	vlas
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnPc1	bakterie
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
důvodem	důvod	k1gInSc7	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
lidé	člověk	k1gMnPc1	člověk
páchnou	páchnout	k5eAaImIp3nP	páchnout
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
potí	potit	k5eAaImIp3nS	potit
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
infekce	infekce	k1gFnSc2	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
bakterie	bakterie	k1gFnPc1	bakterie
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
choroby	choroba	k1gFnPc4	choroba
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
souhrnně	souhrnně	k6eAd1	souhrnně
zvané	zvaný	k2eAgFnSc2d1	zvaná
bakteriózy	bakterióza	k1gFnSc2	bakterióza
či	či	k8xC	či
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
infekce	infekce	k1gFnSc2	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
neomezují	omezovat	k5eNaImIp3nP	omezovat
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
různé	různý	k2eAgFnPc1d1	různá
bakterie	bakterie	k1gFnPc1	bakterie
napadají	napadat	k5eAaPmIp3nP	napadat
široké	široký	k2eAgNnSc4d1	široké
spektrum	spektrum	k1gNnSc4	spektrum
hostitelských	hostitelský	k2eAgInPc2d1	hostitelský
druhů	druh	k1gInPc2	druh
včetně	včetně	k7c2	včetně
hub	houba	k1gFnPc2	houba
<g/>
,	,	kIx,	,
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
prvoků	prvok	k1gMnPc2	prvok
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
některé	některý	k3yIgFnPc1	některý
bakterie	bakterie	k1gFnPc1	bakterie
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
patogeny	patogen	k1gInPc4	patogen
i	i	k9	i
na	na	k7c6	na
samotném	samotný	k2eAgInSc6d1	samotný
povrchu	povrch	k1gInSc6	povrch
těla	tělo	k1gNnSc2	tělo
daného	daný	k2eAgInSc2d1	daný
organismu	organismus	k1gInSc2	organismus
<g/>
,	,	kIx,	,
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
jich	on	k3xPp3gMnPc2	on
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
dovnitř	dovnitř	k6eAd1	dovnitř
různými	různý	k2eAgInPc7d1	různý
tělními	tělní	k2eAgInPc7d1	tělní
otvory	otvor	k1gInPc7	otvor
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
u	u	k7c2	u
rostlin	rostlina	k1gFnPc2	rostlina
průduchy	průduch	k1gInPc1	průduch
<g/>
,	,	kIx,	,
skrz	skrz	k7c4	skrz
sliznice	sliznice	k1gFnPc4	sliznice
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
ranami	rána	k1gFnPc7	rána
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
přes	přes	k7c4	přes
kůži	kůže	k1gFnSc4	kůže
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
usídlí	usídlit	k5eAaPmIp3nP	usídlit
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
způsobit	způsobit	k5eAaPmF	způsobit
hnisání	hnisání	k1gNnSc4	hnisání
<g/>
,	,	kIx,	,
ničit	ničit	k5eAaImF	ničit
tkáň	tkáň	k1gFnSc4	tkáň
či	či	k8xC	či
pletiva	pletivo	k1gNnSc2	pletivo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
nekróza	nekróza	k1gFnSc1	nekróza
<g/>
)	)	kIx)	)
či	či	k8xC	či
škodit	škodit	k5eAaImF	škodit
svými	svůj	k3xOyFgInPc7	svůj
vlastními	vlastní	k2eAgInPc7d1	vlastní
toxiny	toxin	k1gInPc7	toxin
<g/>
.	.	kIx.	.
</s>
<s>
Názvy	název	k1gInPc1	název
bakteriálních	bakteriální	k2eAgFnPc2d1	bakteriální
nemocí	nemoc	k1gFnPc2	nemoc
se	se	k3xPyFc4	se
často	často	k6eAd1	často
tvoří	tvořit	k5eAaImIp3nP	tvořit
přidáním	přidání	k1gNnSc7	přidání
koncovky	koncovka	k1gFnSc2	koncovka
-óza	-óz	k1gInSc2	-óz
k	k	k7c3	k
názvu	název	k1gInSc3	název
dané	daný	k2eAgFnSc2d1	daná
patogenní	patogenní	k2eAgFnSc2d1	patogenní
bakterie	bakterie	k1gFnSc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejběžnější	běžný	k2eAgFnPc4d3	nejběžnější
lidské	lidský	k2eAgFnPc4d1	lidská
bakteriální	bakteriální	k2eAgFnPc4d1	bakteriální
nemoci	nemoc	k1gFnPc4	nemoc
patří	patřit	k5eAaImIp3nP	patřit
například	například	k6eAd1	například
zubní	zubní	k2eAgInSc4d1	zubní
kaz	kaz	k1gInSc4	kaz
<g/>
,	,	kIx,	,
z	z	k7c2	z
vážnějších	vážní	k2eAgFnPc2d2	vážnější
nemocí	nemoc	k1gFnPc2	nemoc
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
velmi	velmi	k6eAd1	velmi
častá	častý	k2eAgFnSc1d1	častá
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
jí	on	k3xPp3gFnSc3	on
byly	být	k5eAaImAgInP	být
podle	podle	k7c2	podle
WHO	WHO	kA	WHO
infikovány	infikován	k2eAgFnPc4d1	infikována
dvě	dva	k4xCgFnPc4	dva
miliardy	miliarda	k4xCgFnPc4	miliarda
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
ročně	ročně	k6eAd1	ročně
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
umíraly	umírat	k5eAaImAgInP	umírat
dva	dva	k4xCgInPc1	dva
miliony	milion	k4xCgInPc1	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
různé	různý	k2eAgFnPc4d1	různá
možnosti	možnost	k1gFnPc4	možnost
nákazy	nákaza	k1gFnSc2	nákaza
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
potravou	potrava	k1gFnSc7	potrava
(	(	kIx(	(
<g/>
alimentárně	alimentárně	k6eAd1	alimentárně
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
dostává	dostávat	k5eAaImIp3nS	dostávat
například	například	k6eAd1	například
Salmonella	Salmonella	k1gFnSc1	Salmonella
(	(	kIx(	(
<g/>
salmonelóza	salmonelóza	k1gFnSc1	salmonelóza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Shigella	Shigella	k1gMnSc1	Shigella
spp	spp	k?	spp
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
shigelóza	shigelóza	k1gFnSc1	shigelóza
provázená	provázený	k2eAgFnSc1d1	provázená
průjmy	průjem	k1gInPc7	průjem
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Listeria	Listerium	k1gNnSc2	Listerium
spp	spp	k?	spp
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
původce	původce	k1gMnSc1	původce
listeriózy	listerióza	k1gFnSc2	listerióza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzdušnou	vzdušný	k2eAgFnSc7d1	vzdušná
cestou	cesta	k1gFnSc7	cesta
například	například	k6eAd1	například
Mycobacterium	Mycobacterium	k1gNnSc1	Mycobacterium
spp	spp	k?	spp
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
TBC	TBC	kA	TBC
a	a	k8xC	a
lepra	lepra	k1gFnSc1	lepra
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Chlamydophila	Chlamydophila	k1gFnSc1	Chlamydophila
psittaci	psittace	k1gFnSc4	psittace
(	(	kIx(	(
<g/>
původce	původce	k1gMnSc4	původce
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
chlamydióz	chlamydióza	k1gFnPc2	chlamydióza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ranami	Rana	k1gFnPc7	Rana
se	se	k3xPyFc4	se
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
dostane	dostat	k5eAaPmIp3nS	dostat
například	například	k6eAd1	například
původce	původce	k1gMnSc1	původce
tetanu	tetan	k1gInSc2	tetan
<g/>
,	,	kIx,	,
Clostridium	Clostridium	k1gNnSc4	Clostridium
tetani	tetaň	k1gFnSc3	tetaň
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgInSc1d1	speciální
případ	případ	k1gInSc1	případ
jsou	být	k5eAaImIp3nP	být
přenosy	přenos	k1gInPc1	přenos
přes	přes	k7c4	přes
členovce	členovec	k1gMnPc4	členovec
<g/>
,	,	kIx,	,
vyskytující	vyskytující	k2eAgInPc1d1	vyskytující
se	se	k3xPyFc4	se
například	například	k6eAd1	například
u	u	k7c2	u
bakterií	bakterie	k1gFnPc2	bakterie
Borrelia	Borrelius	k1gMnSc2	Borrelius
spp	spp	k?	spp
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgMnSc1	jeden
druh	druh	k1gMnSc1	druh
původcem	původce	k1gMnSc7	původce
boreliózy	borelióza	k1gFnPc1	borelióza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rickettsia	Rickettsia	k1gFnSc1	Rickettsia
spp	spp	k?	spp
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
tyfus	tyfus	k1gInSc1	tyfus
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
u	u	k7c2	u
mnohých	mnohý	k2eAgMnPc2d1	mnohý
dalších	další	k2eAgMnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
urogenitální	urogenitální	k2eAgInSc4d1	urogenitální
trakt	trakt	k1gInSc4	trakt
se	se	k3xPyFc4	se
bakterie	bakterie	k1gFnPc1	bakterie
mohou	moct	k5eAaImIp3nP	moct
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
pohlavním	pohlavní	k2eAgInSc7d1	pohlavní
stykem	styk	k1gInSc7	styk
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
Neisseria	Neisserium	k1gNnSc2	Neisserium
gonorrhoeae	gonorrhoeae	k1gNnSc1	gonorrhoeae
(	(	kIx(	(
<g/>
kapavka	kapavka	k1gFnSc1	kapavka
<g/>
)	)	kIx)	)
či	či	k8xC	či
Treponema	Treponema	k1gFnSc1	Treponema
pallidum	pallidum	k1gInSc1	pallidum
(	(	kIx(	(
<g/>
syfilis	syfilis	k1gFnSc1	syfilis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lidské	lidský	k2eAgNnSc1d1	lidské
tělo	tělo	k1gNnSc1	tělo
bojuje	bojovat	k5eAaImIp3nS	bojovat
s	s	k7c7	s
bakteriemi	bakterie	k1gFnPc7	bakterie
pomocí	pomocí	k7c2	pomocí
některých	některý	k3yIgFnPc2	některý
složek	složka	k1gFnPc2	složka
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
extracelulárním	extracelulární	k2eAgFnPc3d1	extracelulární
bakteriím	bakterie	k1gFnPc3	bakterie
(	(	kIx(	(
<g/>
žijícím	žijící	k2eAgInPc3d1	žijící
mimo	mimo	k6eAd1	mimo
lidské	lidský	k2eAgFnPc1d1	lidská
buňky	buňka	k1gFnPc1	buňka
<g/>
,	,	kIx,	,
obvyklé	obvyklý	k2eAgFnPc1d1	obvyklá
infekce	infekce	k1gFnPc1	infekce
<g/>
)	)	kIx)	)
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
bojují	bojovat	k5eAaImIp3nP	bojovat
hlavně	hlavně	k9	hlavně
buňky	buňka	k1gFnPc1	buňka
neutrofily	neutrofil	k1gMnPc4	neutrofil
-	-	kIx~	-
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
jim	on	k3xPp3gInPc3	on
však	však	k9	však
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
i	i	k9	i
tzv.	tzv.	kA	tzv.
komplement	komplement	k1gInSc1	komplement
a	a	k8xC	a
různé	různý	k2eAgFnPc1d1	různá
protilátky	protilátka	k1gFnPc1	protilátka
<g/>
.	.	kIx.	.
</s>
<s>
Vnitrobuněčné	vnitrobuněčný	k2eAgFnPc1d1	vnitrobuněčná
bakterie	bakterie	k1gFnPc1	bakterie
jsou	být	k5eAaImIp3nP	být
cílem	cíl	k1gInSc7	cíl
aktivovaných	aktivovaný	k2eAgInPc2d1	aktivovaný
makrofágů	makrofág	k1gInPc2	makrofág
a	a	k8xC	a
TC-lymfocytů	TCymfocyt	k1gInPc2	TC-lymfocyt
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
sám	sám	k3xTgMnSc1	sám
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
sobě	se	k3xPyFc3	se
i	i	k9	i
na	na	k7c4	na
sobě	se	k3xPyFc3	se
různé	různý	k2eAgInPc4d1	různý
druhy	druh	k1gInPc4	druh
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
kterých	který	k3yQgFnPc2	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
často	často	k6eAd1	často
neobešel	obešet	k5eNaPmAgInS	obešet
(	(	kIx(	(
<g/>
symbióza	symbióza	k1gFnSc1	symbióza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Diverzita	diverzita	k1gFnSc1	diverzita
patogenů	patogen	k1gInPc2	patogen
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
u	u	k7c2	u
domorodců	domorodec	k1gMnPc2	domorodec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nepřišli	přijít	k5eNaPmAgMnP	přijít
do	do	k7c2	do
styku	styk	k1gInSc2	styk
s	s	k7c7	s
moderní	moderní	k2eAgFnSc7d1	moderní
léčbou	léčba	k1gFnSc7	léčba
(	(	kIx(	(
<g/>
která	který	k3yRgFnSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
stavy	stav	k1gInPc4	stav
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
antibiotická	antibiotický	k2eAgFnSc1d1	antibiotická
rezistence	rezistence	k1gFnSc1	rezistence
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnSc1	bakterie
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Lactobacillus	Lactobacillus	k1gInSc1	Lactobacillus
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
s	s	k7c7	s
kvasinkami	kvasinka	k1gFnPc7	kvasinka
a	a	k8xC	a
plísněmi	plíseň	k1gFnPc7	plíseň
<g/>
)	)	kIx)	)
často	často	k6eAd1	často
již	již	k9	již
tisíce	tisíc	k4xCgInPc1	tisíc
let	léto	k1gNnPc2	léto
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
fermentovaných	fermentovaný	k2eAgFnPc2d1	fermentovaná
(	(	kIx(	(
<g/>
kvašených	kvašený	k2eAgFnPc2d1	kvašená
<g/>
)	)	kIx)	)
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
sýr	sýr	k1gInSc1	sýr
<g/>
,	,	kIx,	,
sójová	sójový	k2eAgFnSc1d1	sójová
omáčka	omáčka	k1gFnSc1	omáčka
<g/>
,	,	kIx,	,
nakládaná	nakládaný	k2eAgFnSc1d1	nakládaná
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
,	,	kIx,	,
kyselé	kyselý	k2eAgNnSc1d1	kyselé
zelí	zelí	k1gNnSc1	zelí
<g/>
,	,	kIx,	,
ocet	ocet	k1gInSc1	ocet
<g/>
,	,	kIx,	,
víno	víno	k1gNnSc1	víno
a	a	k8xC	a
jogurt	jogurt	k1gInSc1	jogurt
<g/>
.	.	kIx.	.
</s>
<s>
Jogurt	jogurt	k1gInSc1	jogurt
a	a	k8xC	a
kefír	kefír	k1gInSc1	kefír
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
kvašením	kvašení	k1gNnSc7	kvašení
mléka	mléko	k1gNnSc2	mléko
za	za	k7c4	za
přítomnosti	přítomnost	k1gFnPc4	přítomnost
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
;	;	kIx,	;
mléko	mléko	k1gNnSc1	mléko
díky	díky	k7c3	díky
tomuto	tento	k3xDgInSc3	tento
procesu	proces	k1gInSc3	proces
dostává	dostávat	k5eAaImIp3nS	dostávat
jiné	jiný	k2eAgFnSc2d1	jiná
příchuti	příchuť	k1gFnSc2	příchuť
<g/>
.	.	kIx.	.
</s>
<s>
Mléčných	mléčný	k2eAgFnPc2d1	mléčná
bakterií	bakterie	k1gFnPc2	bakterie
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
při	při	k7c6	při
průmyslové	průmyslový	k2eAgFnSc6d1	průmyslová
produkci	produkce	k1gFnSc6	produkce
kyseliny	kyselina	k1gFnSc2	kyselina
mléčné	mléčný	k2eAgFnSc2d1	mléčná
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
totiž	totiž	k9	totiž
vzniká	vznikat	k5eAaImIp3nS	vznikat
kvašením	kvašení	k1gNnSc7	kvašení
sacharidů	sacharid	k1gInPc2	sacharid
<g/>
.	.	kIx.	.
</s>
<s>
Fermentací	fermentace	k1gFnPc2	fermentace
sacharidů	sacharid	k1gInPc2	sacharid
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
máselných	máselný	k2eAgFnPc2d1	máselná
bakterií	bakterie	k1gFnPc2	bakterie
se	se	k3xPyFc4	se
zase	zase	k9	zase
využívá	využívat	k5eAaPmIp3nS	využívat
k	k	k7c3	k
produkci	produkce	k1gFnSc3	produkce
kyseliny	kyselina	k1gFnSc2	kyselina
octové	octový	k2eAgFnSc2d1	octová
a	a	k8xC	a
máselné	máselný	k2eAgFnSc2d1	máselná
<g/>
,	,	kIx,	,
kvašením	kvašení	k1gNnSc7	kvašení
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
kyseliny	kyselina	k1gFnSc2	kyselina
máselné	máselný	k2eAgFnSc2d1	máselná
se	se	k3xPyFc4	se
průmyslově	průmyslově	k6eAd1	průmyslově
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
butanol	butanol	k1gInSc4	butanol
a	a	k8xC	a
aceton	aceton	k1gInSc4	aceton
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
látkami	látka	k1gFnPc7	látka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
jsou	být	k5eAaImIp3nP	být
produkovány	produkovat	k5eAaImNgFnP	produkovat
bakteriemi	bakterie	k1gFnPc7	bakterie
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
xanthan	xanthan	k1gInSc4	xanthan
(	(	kIx(	(
<g/>
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jako	jako	k9	jako
mazivo	mazivo	k1gNnSc1	mazivo
<g/>
,	,	kIx,	,
přísada	přísada	k1gFnSc1	přísada
do	do	k7c2	do
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
nátěrových	nátěrový	k2eAgFnPc2d1	nátěrová
hmot	hmota	k1gFnPc2	hmota
<g/>
,	,	kIx,	,
keramiky	keramika	k1gFnSc2	keramika
a	a	k8xC	a
různých	různý	k2eAgInPc2d1	různý
dalších	další	k2eAgInPc2d1	další
prostředků	prostředek	k1gInPc2	prostředek
<g/>
)	)	kIx)	)
a	a	k8xC	a
kurdlany	kurdlana	k1gFnSc2	kurdlana
(	(	kIx(	(
<g/>
potenciální	potenciální	k2eAgNnSc1d1	potenciální
uplatnění	uplatnění	k1gNnSc1	uplatnění
v	v	k7c6	v
potravinářství	potravinářství	k1gNnSc6	potravinářství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
se	se	k3xPyFc4	se
též	též	k9	též
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
enzymy	enzym	k1gInPc4	enzym
získané	získaný	k2eAgInPc4d1	získaný
z	z	k7c2	z
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
proteázy	proteáza	k1gFnPc1	proteáza
se	se	k3xPyFc4	se
přidávají	přidávat	k5eAaImIp3nP	přidávat
do	do	k7c2	do
některých	některý	k3yIgInPc2	některý
pracích	prací	k2eAgInPc2d1	prací
prášků	prášek	k1gInPc2	prášek
<g/>
,	,	kIx,	,
ke	k	k7c3	k
štěpení	štěpení	k1gNnSc3	štěpení
škrobu	škrob	k1gInSc2	škrob
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
amyláz	amyláza	k1gFnPc2	amyláza
<g/>
,	,	kIx,	,
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
našly	najít	k5eAaPmAgFnP	najít
uplatnění	uplatnění	k1gNnSc4	uplatnění
streptokinázy	streptokináza	k1gFnSc2	streptokináza
<g/>
.	.	kIx.	.
</s>
<s>
Mnohdy	mnohdy	k6eAd1	mnohdy
se	se	k3xPyFc4	se
v	v	k7c6	v
biotechnologii	biotechnologie	k1gFnSc6	biotechnologie
přistupuje	přistupovat	k5eAaImIp3nS	přistupovat
k	k	k7c3	k
cílenému	cílený	k2eAgNnSc3d1	cílené
šlechtění	šlechtění	k1gNnSc3	šlechtění
bakterií	bakterie	k1gFnPc2	bakterie
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
zlepšení	zlepšení	k1gNnSc2	zlepšení
jejich	jejich	k3xOp3gFnPc2	jejich
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
hledat	hledat	k5eAaImF	hledat
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
náhodné	náhodný	k2eAgMnPc4d1	náhodný
mutanty	mutant	k1gMnPc4	mutant
<g/>
,	,	kIx,	,
bakterie	bakterie	k1gFnSc2	bakterie
s	s	k7c7	s
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
bez	bez	k7c2	bez
určitého	určitý	k2eAgInSc2d1	určitý
plazmidu	plazmid	k1gInSc2	plazmid
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
cíleně	cíleně	k6eAd1	cíleně
rekombinovat	rekombinovat	k5eAaBmF	rekombinovat
a	a	k8xC	a
mutovat	mutovat	k5eAaImF	mutovat
<g/>
.	.	kIx.	.
</s>
<s>
Uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
o	o	k7c6	o
šlechtění	šlechtění	k1gNnSc6	šlechtění
bakterií	bakterie	k1gFnPc2	bakterie
fixujících	fixující	k2eAgFnPc2d1	fixující
dusík	dusík	k1gInSc4	dusík
<g/>
.	.	kIx.	.
</s>
<s>
Schopnost	schopnost	k1gFnSc1	schopnost
bakterií	bakterie	k1gFnPc2	bakterie
rozkládat	rozkládat	k5eAaImF	rozkládat
mnohé	mnohý	k2eAgFnPc4d1	mnohá
látky	látka	k1gFnPc4	látka
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
především	především	k9	především
v	v	k7c6	v
zpracovávání	zpracovávání	k1gNnSc6	zpracovávání
(	(	kIx(	(
<g/>
např.	např.	kA	např.
toxického	toxický	k2eAgInSc2d1	toxický
<g/>
)	)	kIx)	)
odpadu	odpad	k1gInSc2	odpad
i	i	k8xC	i
jiných	jiný	k2eAgInPc6d1	jiný
způsobech	způsob	k1gInPc6	způsob
bioremediace	bioremediace	k1gFnSc2	bioremediace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čistírnách	čistírna	k1gFnPc6	čistírna
odpadních	odpadní	k2eAgFnPc2d1	odpadní
vod	voda	k1gFnPc2	voda
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
podporován	podporovat	k5eAaImNgInS	podporovat
růst	růst	k1gInSc1	růst
aerobních	aerobní	k2eAgFnPc2d1	aerobní
rozkladných	rozkladný	k2eAgFnPc2d1	rozkladná
bakterií	bakterie	k1gFnPc2	bakterie
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
odpadní	odpadní	k2eAgFnSc1d1	odpadní
voda	voda	k1gFnSc1	voda
promíchávána	promícháván	k2eAgFnSc1d1	promíchávána
kyslíkem	kyslík	k1gInSc7	kyslík
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
tzv.	tzv.	kA	tzv.
aktivovaného	aktivovaný	k2eAgInSc2d1	aktivovaný
kalu	kal	k1gInSc2	kal
<g/>
,	,	kIx,	,
rozkladných	rozkladný	k2eAgFnPc2d1	rozkladná
bakterií	bakterie	k1gFnPc2	bakterie
se	se	k3xPyFc4	se
však	však	k9	však
na	na	k7c6	na
podobném	podobný	k2eAgInSc6d1	podobný
principu	princip	k1gInSc6	princip
využívá	využívat	k5eAaImIp3nS	využívat
i	i	k9	i
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
domácích	domácí	k2eAgInPc6d1	domácí
septicích	septik	k1gInPc6	septik
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnPc1	bakterie
schopné	schopný	k2eAgFnPc1d1	schopná
trávit	trávit	k5eAaImF	trávit
uhlovodíky	uhlovodík	k1gInPc4	uhlovodík
v	v	k7c6	v
ropě	ropa	k1gFnSc6	ropa
jsou	být	k5eAaImIp3nP	být
využívány	využíván	k2eAgInPc1d1	využíván
při	při	k7c6	při
čištění	čištění	k1gNnSc6	čištění
ropných	ropný	k2eAgFnPc2d1	ropná
skvrn	skvrna	k1gFnPc2	skvrna
<g/>
,	,	kIx,	,
na	na	k7c4	na
pláže	pláž	k1gFnPc4	pláž
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
přidává	přidávat	k5eAaImIp3nS	přidávat
hnojivo	hnojivo	k1gNnSc4	hnojivo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
růst	růst	k1gInSc1	růst
bakterií	bakterie	k1gFnPc2	bakterie
podpořil	podpořit	k5eAaPmAgInS	podpořit
(	(	kIx(	(
<g/>
např.	např.	kA	např.
po	po	k7c6	po
havárii	havárie	k1gFnSc6	havárie
tankeru	tanker	k1gInSc2	tanker
Exxon	Exxon	k1gMnSc1	Exxon
Valdez	Valdez	k1gMnSc1	Valdez
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chemickém	chemický	k2eAgInSc6d1	chemický
průmyslu	průmysl	k1gInSc6	průmysl
se	se	k3xPyFc4	se
bakterie	bakterie	k1gFnPc1	bakterie
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
produkci	produkce	k1gFnSc3	produkce
nejrůznějších	různý	k2eAgFnPc2d3	nejrůznější
chemických	chemický	k2eAgFnPc2d1	chemická
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
léků	lék	k1gInPc2	lék
či	či	k8xC	či
agrochemikálií	agrochemikálie	k1gFnPc2	agrochemikálie
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnSc1	bakterie
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
používají	používat	k5eAaImIp3nP	používat
namísto	namísto	k7c2	namísto
pesticidů	pesticid	k1gInPc2	pesticid
v	v	k7c6	v
biologickém	biologický	k2eAgInSc6d1	biologický
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
škůdcům	škůdce	k1gMnPc3	škůdce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
půdní	půdní	k2eAgFnSc1d1	půdní
bakterie	bakterie	k1gFnSc1	bakterie
Bacillus	Bacillus	k1gInSc1	Bacillus
thuringiensis	thuringiensis	k1gFnSc1	thuringiensis
(	(	kIx(	(
<g/>
BT	BT	kA	BT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
geneticky	geneticky	k6eAd1	geneticky
upravených	upravený	k2eAgFnPc2d1	upravená
bakterií	bakterie	k1gFnPc2	bakterie
se	se	k3xPyFc4	se
také	také	k9	také
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
inzulin	inzulin	k1gInSc1	inzulin
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
hormony	hormon	k1gInPc1	hormon
<g/>
,	,	kIx,	,
enzymy	enzym	k1gInPc1	enzym
<g/>
,	,	kIx,	,
růstové	růstový	k2eAgInPc1d1	růstový
faktory	faktor	k1gInPc1	faktor
či	či	k8xC	či
protilátky	protilátka	k1gFnPc1	protilátka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výzkumu	výzkum	k1gInSc6	výzkum
se	se	k3xPyFc4	se
bakterií	bakterie	k1gFnSc7	bakterie
využívá	využívat	k5eAaImIp3nS	využívat
kvůli	kvůli	k7c3	kvůli
rychlému	rychlý	k2eAgInSc3d1	rychlý
růstu	růst	k1gInSc3	růst
a	a	k8xC	a
poměrně	poměrně	k6eAd1	poměrně
snadné	snadný	k2eAgFnSc6d1	snadná
manipulaci	manipulace	k1gFnSc6	manipulace
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnPc1	bakterie
jsou	být	k5eAaImIp3nP	být
modelové	modelový	k2eAgInPc4d1	modelový
organismy	organismus	k1gInPc4	organismus
pro	pro	k7c4	pro
molekulární	molekulární	k2eAgFnSc4d1	molekulární
biologii	biologie	k1gFnSc4	biologie
<g/>
,	,	kIx,	,
genetiku	genetika	k1gFnSc4	genetika
a	a	k8xC	a
biochemii	biochemie	k1gFnSc4	biochemie
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
například	například	k6eAd1	například
cíleně	cíleně	k6eAd1	cíleně
mutují	mutovat	k5eAaImIp3nP	mutovat
DNA	dno	k1gNnPc4	dno
bakterií	bakterie	k1gFnPc2	bakterie
a	a	k8xC	a
následné	následný	k2eAgInPc1d1	následný
fenotypy	fenotyp	k1gInPc1	fenotyp
zkoumají	zkoumat	k5eAaImIp3nP	zkoumat
-	-	kIx~	-
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
funkce	funkce	k1gFnSc1	funkce
genů	gen	k1gInPc2	gen
<g/>
,	,	kIx,	,
enzymů	enzym	k1gInPc2	enzym
a	a	k8xC	a
metabolických	metabolický	k2eAgFnPc2d1	metabolická
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
později	pozdě	k6eAd2	pozdě
aplikovat	aplikovat	k5eAaBmF	aplikovat
i	i	k9	i
na	na	k7c4	na
komplexnější	komplexní	k2eAgInPc4d2	komplexnější
organismy	organismus	k1gInPc4	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Modelovým	modelový	k2eAgInSc7d1	modelový
organismem	organismus	k1gInSc7	organismus
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
bakterie	bakterie	k1gFnSc1	bakterie
Escherichia	Escherichium	k1gNnSc2	Escherichium
coli	col	k1gFnSc2	col
<g/>
.	.	kIx.	.
</s>
<s>
Schopnosti	schopnost	k1gFnPc1	schopnost
některých	některý	k3yIgFnPc2	některý
patogenních	patogenní	k2eAgFnPc2d1	patogenní
bakterií	bakterie	k1gFnPc2	bakterie
inkorporovat	inkorporovat	k5eAaBmF	inkorporovat
plazmidy	plazmid	k1gInPc4	plazmid
do	do	k7c2	do
DNA	dno	k1gNnSc2	dno
hostitele	hostitel	k1gMnSc2	hostitel
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
v	v	k7c6	v
genetickém	genetický	k2eAgNnSc6d1	genetické
inženýrství	inženýrství	k1gNnSc6	inženýrství
<g/>
:	:	kIx,	:
zejména	zejména	k9	zejména
Agrobacterium	Agrobacterium	k1gNnSc1	Agrobacterium
tumefaciens	tumefaciens	k6eAd1	tumefaciens
je	být	k5eAaImIp3nS	být
používaná	používaný	k2eAgFnSc1d1	používaná
při	při	k7c6	při
cílené	cílený	k2eAgFnSc6d1	cílená
přípravě	příprava	k1gFnSc6	příprava
geneticky	geneticky	k6eAd1	geneticky
modifikovaných	modifikovaný	k2eAgFnPc2d1	modifikovaná
plodin	plodina	k1gFnPc2	plodina
<g/>
.	.	kIx.	.
</s>
