<p>
<s>
Wanda	Wanda	k1gFnSc1	Wanda
Alexandra	Alexandr	k1gMnSc2	Alexandr
Landowska	Landowsek	k1gMnSc2	Landowsek
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1879	[number]	k4	1879
<g/>
,	,	kIx,	,
Varšava	Varšava	k1gFnSc1	Varšava
–	–	k?	–
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
Lakeville	Lakeville	k1gFnSc1	Lakeville
<g/>
,	,	kIx,	,
Connecticut	Connecticut	k1gInSc1	Connecticut
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
cembalistka	cembalistka	k1gFnSc1	cembalistka
polského	polský	k2eAgInSc2d1	polský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
odbornicí	odbornice	k1gFnSc7	odbornice
na	na	k7c4	na
hudbu	hudba	k1gFnSc4	hudba
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
velkou	velký	k2eAgFnSc7d1	velká
propagátorkou	propagátorka	k1gFnSc7	propagátorka
cembala	cembalo	k1gNnSc2	cembalo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
významně	významně	k6eAd1	významně
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
tohoto	tento	k3xDgInSc2	tento
nástroje	nástroj	k1gInSc2	nástroj
na	na	k7c4	na
koncertní	koncertní	k2eAgNnPc4d1	koncertní
pódia	pódium	k1gNnPc4	pódium
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Wanda	Wanda	k1gFnSc1	Wanda
Landowska	Landowska	k1gFnSc1	Landowska
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc4	její
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
matka	matka	k1gFnSc1	matka
překladatelka	překladatelka	k1gFnSc1	překladatelka
a	a	k8xC	a
lingvistka	lingvistka	k1gFnSc1	lingvistka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
čtyř	čtyři	k4xCgNnPc2	čtyři
let	léto	k1gNnPc2	léto
hrála	hrát	k5eAaImAgFnS	hrát
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
učil	učít	k5eAaPmAgMnS	učít
ji	on	k3xPp3gFnSc4	on
nejprve	nejprve	k6eAd1	nejprve
Jan	Jan	k1gMnSc1	Jan
Kleczyński	Kleczyńsk	k1gFnSc2	Kleczyńsk
<g/>
,	,	kIx,	,
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
na	na	k7c6	na
varšavské	varšavský	k2eAgFnSc6d1	Varšavská
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
Aleksander	Aleksandra	k1gFnPc2	Aleksandra
Michałowski	Michałowsk	k1gFnSc2	Michałowsk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
konzervatoře	konzervatoř	k1gFnSc2	konzervatoř
odjela	odjet	k5eAaPmAgFnS	odjet
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
studovala	studovat	k5eAaImAgFnS	studovat
hru	hra	k1gFnSc4	hra
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
u	u	k7c2	u
Moritze	moritz	k1gInSc2	moritz
Moszkowského	Moszkowské	k1gNnSc2	Moszkowské
a	a	k8xC	a
skladbu	skladba	k1gFnSc4	skladba
u	u	k7c2	u
Heinricha	Heinrich	k1gMnSc2	Heinrich
Urbana	Urban	k1gMnSc2	Urban
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
se	se	k3xPyFc4	se
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
provdala	provdat	k5eAaPmAgFnS	provdat
se	se	k3xPyFc4	se
za	za	k7c4	za
folkloristu	folklorista	k1gMnSc4	folklorista
Henryho	Henry	k1gMnSc2	Henry
Lewa	Lewus	k1gMnSc2	Lewus
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
se	se	k3xPyFc4	se
naplno	naplno	k6eAd1	naplno
věnovat	věnovat	k5eAaImF	věnovat
studiu	studio	k1gNnSc3	studio
hudby	hudba	k1gFnSc2	hudba
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Vyučovala	vyučovat	k5eAaImAgFnS	vyučovat
klavír	klavír	k1gInSc4	klavír
na	na	k7c6	na
pařížské	pařížský	k2eAgFnSc6d1	Pařížská
hudební	hudební	k2eAgFnSc6d1	hudební
škole	škola	k1gFnSc6	škola
Schola	Schola	k1gFnSc1	Schola
Cantorum	Cantorum	k1gInSc4	Cantorum
<g/>
,	,	kIx,	,
a	a	k8xC	a
zpočátku	zpočátku	k6eAd1	zpočátku
také	také	k9	také
hrála	hrát	k5eAaImAgFnS	hrát
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
Bachovy	Bachův	k2eAgInPc4d1	Bachův
koncerty	koncert	k1gInPc4	koncert
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
čím	co	k3yInSc7	co
dál	daleko	k6eAd2	daleko
víc	hodně	k6eAd2	hodně
přesvědčená	přesvědčený	k2eAgFnSc1d1	přesvědčená
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
barokní	barokní	k2eAgFnSc4d1	barokní
hudbu	hudba	k1gFnSc4	hudba
je	být	k5eAaImIp3nS	být
cembalo	cembalo	k1gNnSc1	cembalo
vhodnějším	vhodný	k2eAgInSc7d2	vhodnější
nástrojem	nástroj	k1gInSc7	nástroj
než	než	k8xS	než
klavír	klavír	k1gInSc4	klavír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
na	na	k7c4	na
cembalo	cembalo	k1gNnSc4	cembalo
poprvé	poprvé	k6eAd1	poprvé
veřejně	veřejně	k6eAd1	veřejně
hrála	hrát	k5eAaImAgFnS	hrát
a	a	k8xC	a
poté	poté	k6eAd1	poté
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
začala	začít	k5eAaPmAgFnS	začít
vystupovat	vystupovat	k5eAaImF	vystupovat
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
včetně	včetně	k7c2	včetně
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
svou	svůj	k3xOyFgFnSc7	svůj
hrou	hra	k1gFnSc7	hra
zaujala	zaujmout	k5eAaPmAgFnS	zaujmout
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
Lva	lev	k1gInSc2	lev
Nikolajeviče	Nikolajevič	k1gMnSc2	Nikolajevič
Tolstého	Tolstý	k2eAgMnSc2d1	Tolstý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
vlastního	vlastní	k2eAgNnSc2d1	vlastní
hraní	hraní	k1gNnSc2	hraní
psala	psát	k5eAaImAgFnS	psát
odborné	odborný	k2eAgInPc4d1	odborný
články	článek	k1gInPc4	článek
o	o	k7c6	o
hře	hra	k1gFnSc6	hra
na	na	k7c4	na
cembalo	cembalo	k1gNnSc4	cembalo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k6eAd1	také
nástroj	nástroj	k1gInSc4	nástroj
obhajovala	obhajovat	k5eAaImAgFnS	obhajovat
vůči	vůči	k7c3	vůči
tehdejší	tehdejší	k2eAgFnSc3d1	tehdejší
kritice	kritika	k1gFnSc3	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Cembalo	cembalo	k1gNnSc1	cembalo
bylo	být	k5eAaImAgNnS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
překonaný	překonaný	k2eAgInSc4d1	překonaný
nástroj	nástroj	k1gInSc4	nástroj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nehodí	hodit	k5eNaPmIp3nS	hodit
ke	k	k7c3	k
koncertování	koncertování	k1gNnSc3	koncertování
zejména	zejména	k9	zejména
kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgInSc3	svůj
slabému	slabý	k2eAgInSc3d1	slabý
zvuku	zvuk	k1gInSc3	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Landowska	Landowska	k1gFnSc1	Landowska
ukazovala	ukazovat	k5eAaImAgFnS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
cembalo	cembalo	k1gNnSc1	cembalo
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
koncertních	koncertní	k2eAgNnPc6d1	koncertní
pódiích	pódium	k1gNnPc6	pódium
stále	stále	k6eAd1	stále
své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Studovala	studovat	k5eAaImAgFnS	studovat
a	a	k8xC	a
sbírala	sbírat	k5eAaImAgFnS	sbírat
staré	starý	k2eAgInPc4d1	starý
nástroje	nástroj	k1gInPc4	nástroj
a	a	k8xC	a
také	také	k9	také
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
také	také	k9	také
firma	firma	k1gFnSc1	firma
Pleyel	Pleyela	k1gFnPc2	Pleyela
vyrobila	vyrobit	k5eAaPmAgFnS	vyrobit
podle	podle	k7c2	podle
jejích	její	k3xOp3gInPc2	její
vlastních	vlastní	k2eAgInPc2d1	vlastní
návrhů	návrh	k1gInPc2	návrh
několik	několik	k4yIc4	několik
nových	nový	k2eAgInPc2d1	nový
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
používala	používat	k5eAaImAgFnS	používat
na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
robustní	robustní	k2eAgInPc4d1	robustní
typy	typ	k1gInPc4	typ
s	s	k7c7	s
šestnáctistopým	šestnáctistopý	k2eAgInSc7d1	šestnáctistopý
rejstříkem	rejstřík	k1gInSc7	rejstřík
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
stavbou	stavba	k1gFnSc7	stavba
podobají	podobat	k5eAaImIp3nP	podobat
klavíru	klavír	k1gInSc3	klavír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
vyučovala	vyučovat	k5eAaImAgFnS	vyučovat
cembalo	cembalo	k1gNnSc4	cembalo
na	na	k7c6	na
berlínkské	berlínkský	k2eAgFnSc6d1	berlínkský
Hochschule	Hochschula	k1gFnSc6	Hochschula
für	für	k?	für
Musik	musika	k1gFnPc2	musika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
a	a	k8xC	a
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c6	v
koncertování	koncertování	k1gNnSc6	koncertování
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
i	i	k8xC	i
cembalo	cembalo	k1gNnSc1	cembalo
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
barokní	barokní	k2eAgFnSc2d1	barokní
hudby	hudba	k1gFnSc2	hudba
hrála	hrát	k5eAaImAgFnS	hrát
také	také	k9	také
nové	nový	k2eAgFnPc4d1	nová
skladby	skladba	k1gFnPc4	skladba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
napsali	napsat	k5eAaBmAgMnP	napsat
soudobí	soudobý	k2eAgMnPc1d1	soudobý
skladatelé	skladatel	k1gMnPc1	skladatel
–	–	k?	–
koncerty	koncert	k1gInPc4	koncert
pro	pro	k7c4	pro
cembalo	cembalo	k1gNnSc4	cembalo
jí	on	k3xPp3gFnSc2	on
věnovali	věnovat	k5eAaPmAgMnP	věnovat
například	například	k6eAd1	například
Francis	Francis	k1gInSc4	Francis
Poulenc	Poulenc	k1gFnSc1	Poulenc
(	(	kIx(	(
<g/>
Concert	Concert	k1gInSc1	Concert
champê	champê	k?	champê
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Manuel	Manuel	k1gMnSc1	Manuel
de	de	k?	de
Falla	Falla	k1gMnSc1	Falla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
se	se	k3xPyFc4	se
usadila	usadit	k5eAaPmAgFnS	usadit
v	v	k7c6	v
pařížském	pařížský	k2eAgNnSc6d1	pařížské
severním	severní	k2eAgNnSc6d1	severní
předměstí	předměstí	k1gNnSc6	předměstí
Saint-Leu-la-Fôret	Saint-Leua-Fôreta	k1gFnPc2	Saint-Leu-la-Fôreta
a	a	k8xC	a
založila	založit	k5eAaPmAgFnS	založit
zde	zde	k6eAd1	zde
svou	svůj	k3xOyFgFnSc4	svůj
hudební	hudební	k2eAgFnSc4d1	hudební
školu	škola	k1gFnSc4	škola
École	École	k1gFnSc2	École
de	de	k?	de
Musique	Musiquus	k1gMnSc5	Musiquus
Ancienne	Ancienn	k1gMnSc5	Ancienn
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
sjížděli	sjíždět	k5eAaImAgMnP	sjíždět
studenti	student	k1gMnPc1	student
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
německé	německý	k2eAgFnSc6d1	německá
invazi	invaze	k1gFnSc6	invaze
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
emigrovala	emigrovat	k5eAaBmAgFnS	emigrovat
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nakonec	nakonec	k6eAd1	nakonec
zůstala	zůstat	k5eAaPmAgFnS	zůstat
i	i	k9	i
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
poměrně	poměrně	k6eAd1	poměrně
vysokém	vysoký	k2eAgInSc6d1	vysoký
věku	věk	k1gInSc6	věk
byla	být	k5eAaImAgFnS	být
stále	stále	k6eAd1	stále
aktivní	aktivní	k2eAgFnSc1d1	aktivní
<g/>
,	,	kIx,	,
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
ohlasem	ohlas	k1gInSc7	ohlas
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
a	a	k8xC	a
vyučovala	vyučovat	k5eAaImAgFnS	vyučovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
několika	několik	k4yIc2	několik
let	léto	k1gNnPc2	léto
pořídila	pořídit	k5eAaPmAgFnS	pořídit
nahrávky	nahrávka	k1gFnPc1	nahrávka
všech	všecek	k3xTgFnPc2	všecek
skladeb	skladba	k1gFnPc2	skladba
Dobře	dobře	k6eAd1	dobře
temperovaného	temperovaný	k2eAgInSc2d1	temperovaný
klavíru	klavír	k1gInSc2	klavír
Johanna	Johann	k1gMnSc4	Johann
Sebastiana	Sebastian	k1gMnSc4	Sebastian
Bacha	Bacha	k?	Bacha
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
dílo	dílo	k1gNnSc4	dílo
dokončila	dokončit	k5eAaPmAgFnS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
75	[number]	k4	75
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Wanda	Wanda	k1gFnSc1	Wanda
Landowska	Landowska	k1gFnSc1	Landowska
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
výraznou	výrazný	k2eAgFnSc7d1	výrazná
a	a	k8xC	a
respektovanou	respektovaný	k2eAgFnSc7d1	respektovaná
hudební	hudební	k2eAgFnSc7d1	hudební
osobností	osobnost	k1gFnSc7	osobnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
zásadní	zásadní	k2eAgInSc4d1	zásadní
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
obnovení	obnovení	k1gNnSc4	obnovení
zájmu	zájem	k1gInSc2	zájem
o	o	k7c4	o
cembalo	cembalo	k1gNnSc4	cembalo
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Položila	položit	k5eAaPmAgFnS	položit
základy	základ	k1gInPc4	základ
moderní	moderní	k2eAgFnSc2d1	moderní
cembalové	cembalový	k2eAgFnSc2d1	cembalová
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
vlastní	vlastní	k2eAgFnSc1d1	vlastní
hra	hra	k1gFnSc1	hra
se	se	k3xPyFc4	se
vyznačovala	vyznačovat	k5eAaImAgFnS	vyznačovat
ohromnou	ohromný	k2eAgFnSc7d1	ohromná
živostí	živost	k1gFnSc7	živost
a	a	k8xC	a
barvitostí	barvitost	k1gFnSc7	barvitost
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
prosazovala	prosazovat	k5eAaImAgFnS	prosazovat
analytický	analytický	k2eAgInSc4d1	analytický
přístup	přístup	k1gInSc4	přístup
a	a	k8xC	a
historicky	historicky	k6eAd1	historicky
poučenou	poučený	k2eAgFnSc4d1	poučená
interpretaci	interpretace	k1gFnSc4	interpretace
barokní	barokní	k2eAgFnSc2d1	barokní
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vedle	vedle	k7c2	vedle
koncertování	koncertování	k1gNnSc2	koncertování
a	a	k8xC	a
nahrávání	nahrávání	k1gNnSc2	nahrávání
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
hudbu	hudba	k1gFnSc4	hudba
vyučovala	vyučovat	k5eAaImAgFnS	vyučovat
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
šířila	šířit	k5eAaImAgFnS	šířit
své	svůj	k3xOyFgFnPc4	svůj
ideje	idea	k1gFnPc4	idea
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
odborných	odborný	k2eAgInPc2d1	odborný
článků	článek	k1gInPc2	článek
<g/>
;	;	kIx,	;
část	část	k1gFnSc1	část
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
společně	společně	k6eAd1	společně
vydána	vydán	k2eAgFnSc1d1	vydána
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Landowska	Landowsk	k1gInSc2	Landowsk
on	on	k3xPp3gMnSc1	on
Music	Music	k1gMnSc1	Music
(	(	kIx(	(
<g/>
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
