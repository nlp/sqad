<s>
Kratochvilka	Kratochvilka	k1gFnSc1
</s>
<s>
Kratochvilka	Kratochvilka	k1gFnSc1
</s>
<s>
znakvlajka	znakvlajka	k1gFnSc1
Lokalita	lokalita	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
obec	obec	k1gFnSc1
LAU	LAU	kA
2	#num#	k4
(	(	kIx(
<g/>
obec	obec	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
CZ0643	CZ0643	k4
583235	#num#	k4
Pověřená	pověřený	k2eAgFnSc1d1
obec	obec	k1gFnSc1
a	a	k8xC
obec	obec	k1gFnSc1
s	s	k7c7
rozšířenou	rozšířený	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
</s>
<s>
Rosice	Rosice	k1gFnPc1
Okres	okres	k1gInSc1
(	(	kIx(
<g/>
LAU	LAU	kA
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Brno-venkov	Brno-venkov	k1gInSc1
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
643	#num#	k4
<g/>
)	)	kIx)
Kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
NUTS	NUTS	kA
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jihomoravský	jihomoravský	k2eAgMnSc1d1
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
64	#num#	k4
<g/>
)	)	kIx)
Historická	historický	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Morava	Morava	k1gFnSc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
9	#num#	k4
<g/>
′	′	k?
<g/>
24	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
22	#num#	k4
<g/>
′	′	k?
<g/>
35	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
472	#num#	k4
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Rozloha	rozloha	k1gFnSc1
</s>
<s>
1,50	1,50	k4
km²	km²	k?
Katastrální	katastrální	k2eAgNnSc4d1
území	území	k1gNnSc4
</s>
<s>
Kratochvilka	Kratochvilka	k1gFnSc1
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
385	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
PSČ	PSČ	kA
</s>
<s>
664	#num#	k4
91	#num#	k4
Počet	počet	k1gInSc1
částí	část	k1gFnPc2
obce	obec	k1gFnSc2
</s>
<s>
1	#num#	k4
Počet	počet	k1gInSc1
k.	k.	k?
ú.	ú.	k?
</s>
<s>
1	#num#	k4
Počet	počet	k1gInSc1
ZSJ	ZSJ	kA
</s>
<s>
1	#num#	k4
Kontakt	kontakt	k1gInSc1
Adresa	adresa	k1gFnSc1
obecního	obecní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
</s>
<s>
Kratochvilka	Kratochvilka	k1gFnSc1
7664	#num#	k4
91	#num#	k4
Ivančice	Ivančice	k1gFnPc1
obec@kratochvilka.cz	obec@kratochvilka.cz	k1gMnSc1
Starosta	Starosta	k1gMnSc1
</s>
<s>
Ing.	ing.	kA
František	František	k1gMnSc1
Malý	Malý	k1gMnSc1
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
<g/>
:	:	kIx,
www.kratochvilka.cz	www.kratochvilka.cz	k1gInSc1
</s>
<s>
Kratochvilka	Kratochvilka	k1gFnSc1
</s>
<s>
Další	další	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Kód	kód	k1gInSc4
obce	obec	k1gFnPc1
</s>
<s>
583235	#num#	k4
Kód	kód	k1gInSc1
části	část	k1gFnSc2
obce	obec	k1gFnSc2
</s>
<s>
74136	#num#	k4
Geodata	Geodata	k1gFnSc1
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Zdroje	zdroj	k1gInSc2
k	k	k7c3
infoboxu	infobox	k1gInSc3
a	a	k8xC
českým	český	k2eAgNnPc3d1
sídlům	sídlo	k1gNnPc3
<g/>
.	.	kIx.
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kratochvilka	Kratochvilka	k1gFnSc1
je	být	k5eAaImIp3nS
obec	obec	k1gFnSc1
v	v	k7c6
okrese	okres	k1gInSc6
Brno-venkov	Brno-venkov	k1gInSc1
v	v	k7c6
Jihomoravském	jihomoravský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
Boskovické	boskovický	k2eAgFnSc6d1
brázdě	brázda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žije	žít	k5eAaImIp3nS
zde	zde	k6eAd1
472	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Už	už	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1626	#num#	k4
je	být	k5eAaImIp3nS
zmiňován	zmiňován	k2eAgInSc4d1
zájezdní	zájezdní	k2eAgInSc4d1
hostinec	hostinec	k1gInSc4
Kratochvilka	Kratochvilka	k1gFnSc1
na	na	k7c6
silnici	silnice	k1gFnSc6
Ivančice	Ivančice	k1gFnPc1
–	–	k?
Velká	velká	k1gFnSc1
Bíteš	Bíteš	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
písemná	písemný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
obci	obec	k1gFnSc6
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1783	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
(	(	kIx(
<g/>
sčítání	sčítání	k1gNnSc1
lidu	lid	k1gInSc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
18691880189019001910192119301950196119701980199120012011	#num#	k4
</s>
<s>
190209244360429482561586536518506436429445	#num#	k4
</s>
<s>
Pamětihodnosti	pamětihodnost	k1gFnPc1
</s>
<s>
V	v	k7c6
obci	obec	k1gFnSc6
se	se	k3xPyFc4
dříve	dříve	k6eAd2
na	na	k7c6
návsi	náves	k1gFnSc6
nacházela	nacházet	k5eAaImAgFnS
kaple	kaple	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
však	však	k9
byla	být	k5eAaImAgFnS
27	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1945	#num#	k4
při	při	k7c6
bombardování	bombardování	k1gNnSc6
obce	obec	k1gFnSc2
německou	německý	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
zničena	zničit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s>
Pomník	pomník	k1gInSc1
T.	T.	kA
G.	G.	kA
Masaryka	Masaryk	k1gMnSc2
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
</s>
<s>
Rozmarýnové	rozmarýnový	k2eAgInPc4d1
hody	hod	k1gInPc4
v	v	k7c6
Kratochvilce	Kratochvilka	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1978	#num#	k4
</s>
<s>
Rozmarýnové	rozmarýnový	k2eAgInPc4d1
hody	hod	k1gInPc4
v	v	k7c6
Kratochvilce	Kratochvilka	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
</s>
<s>
Kratochvilští	Kratochvilský	k2eAgMnPc1d1
horníci	horník	k1gMnPc1
v	v	k7c6
rosicko-oslavanských	rosicko-oslavanský	k2eAgInPc6d1
dolech	dol	k1gInPc6
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
:	:	kIx,
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
v	v	k7c6
obcích	obec	k1gFnPc6
-	-	kIx~
k	k	k7c3
1.1	1.1	k4
<g/>
.2021	.2021	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Historický	historický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
1869	#num#	k4
<g/>
-	-	kIx~
<g/>
2011	#num#	k4
<g/>
:	:	kIx,
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
a	a	k8xC
domů	dům	k1gInPc2
podle	podle	k7c2
krajů	kraj	k1gInPc2
<g/>
,	,	kIx,
okresů	okres	k1gInPc2
<g/>
,	,	kIx,
obcí	obec	k1gFnPc2
<g/>
,	,	kIx,
částí	část	k1gFnPc2
obcí	obec	k1gFnPc2
a	a	k8xC
historických	historický	k2eAgFnPc2d1
osad	osada	k1gFnPc2
/	/	kIx~
lokalit	lokalita	k1gFnPc2
v	v	k7c6
letech	léto	k1gNnPc6
1869	#num#	k4
-	-	kIx~
2011	#num#	k4
:	:	kIx,
Okres	okres	k1gInSc1
Brno-venkov	Brno-venkov	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2015-12-21	2015-12-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Neslovice	Neslovice	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Kratochvilka	Kratochvilka	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Kratochvilka	Kratochvilka	k1gFnSc1
v	v	k7c6
Registru	registrum	k1gNnSc6
územní	územní	k2eAgFnSc2d1
identifikace	identifikace	k1gFnSc2
<g/>
,	,	kIx,
adres	adresa	k1gFnPc2
a	a	k8xC
nemovitostí	nemovitost	k1gFnPc2
(	(	kIx(
<g/>
RÚIAN	RÚIAN	kA
<g/>
)	)	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
webové	webový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
obce	obec	k1gFnSc2
Kratochvilka	Kratochvilka	k1gFnSc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
facebookové	facebookový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
obce	obec	k1gFnSc2
Kratochvilka	Kratochvilka	k1gFnSc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
webové	webový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
Mikroregionu	mikroregion	k1gInSc2
Kahan	kahan	k1gInSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Města	město	k1gNnSc2
<g/>
,	,	kIx,
městyse	městys	k1gInSc2
a	a	k8xC
obce	obec	k1gFnSc2
okresu	okres	k1gInSc2
Brno-venkov	Brno-venkov	k1gInSc1
</s>
<s>
Babice	babice	k1gFnSc1
nad	nad	k7c7
Svitavou	Svitava	k1gFnSc7
•	•	k?
Babice	babice	k1gFnSc2
u	u	k7c2
Rosic	Rosice	k1gFnPc2
•	•	k?
Běleč	Běleč	k1gMnSc1
•	•	k?
Bílovice	Bílovice	k1gInPc1
nad	nad	k7c7
Svitavou	Svitava	k1gFnSc7
•	•	k?
Biskoupky	Biskoupka	k1gFnSc2
•	•	k?
Blažovice	Blažovice	k1gFnSc2
•	•	k?
Blučina	Blučina	k1gFnSc1
•	•	k?
Borač	Borač	k1gInSc1
•	•	k?
Borovník	borovník	k1gInSc1
•	•	k?
Braníškov	Braníškov	k1gInSc1
•	•	k?
Branišovice	Branišovice	k1gFnSc2
•	•	k?
Bratčice	Bratčice	k1gFnSc2
•	•	k?
Brumov	Brumov	k1gInSc1
•	•	k?
Březina	Březina	k1gFnSc1
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
okres	okres	k1gInSc1
Blansko	Blansko	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Březina	Březina	k1gMnSc1
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
okres	okres	k1gInSc1
Tišnov	Tišnov	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Bukovice	Bukovice	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Cvrčovice	Cvrčovice	k1gFnSc2
•	•	k?
Čebín	Čebín	k1gMnSc1
•	•	k?
Černvír	Černvír	k1gMnSc1
•	•	k?
Česká	český	k2eAgFnSc1d1
•	•	k?
Čučice	Čučice	k1gFnSc1
•	•	k?
Deblín	Deblín	k1gInSc1
•	•	k?
Dolní	dolní	k2eAgFnPc1d1
Kounice	Kounice	k1gFnPc1
•	•	k?
Dolní	dolní	k2eAgFnPc1d1
Loučky	loučka	k1gFnPc1
•	•	k?
Domašov	Domašov	k1gInSc1
•	•	k?
Doubravník	Doubravník	k1gInSc1
•	•	k?
Drahonín	Drahonín	k1gInSc1
•	•	k?
Drásov	Drásov	k1gInSc4
•	•	k?
Hajany	Hajany	k1gInPc4
•	•	k?
Heroltice	Heroltice	k1gFnSc2
•	•	k?
Hlína	hlína	k1gFnSc1
•	•	k?
Hluboké	hluboký	k2eAgInPc1d1
Dvory	Dvůr	k1gInPc1
•	•	k?
Holasice	Holasice	k1gFnSc2
•	•	k?
Horní	horní	k2eAgFnSc2d1
Loučky	loučka	k1gFnSc2
•	•	k?
Hostěnice	hostěnice	k1gFnSc1
•	•	k?
Hradčany	Hradčany	k1gInPc1
•	•	k?
Hrušovany	Hrušovany	k1gInPc4
u	u	k7c2
Brna	Brno	k1gNnSc2
•	•	k?
Hvozdec	Hvozdec	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Chudčice	Chudčice	k1gFnSc2
•	•	k?
Ivaň	Ivaň	k1gFnSc1
•	•	k?
Ivančice	Ivančice	k1gFnPc4
•	•	k?
Javůrek	Javůrek	k1gMnSc1
•	•	k?
Jinačovice	Jinačovice	k1gFnSc2
•	•	k?
Jiříkovice	Jiříkovice	k1gFnSc2
•	•	k?
Kaly	kala	k1gFnSc2
•	•	k?
Kanice	Kanice	k1gFnPc4
•	•	k?
Katov	Katov	k1gInSc1
•	•	k?
Ketkovice	Ketkovice	k1gFnSc2
•	•	k?
Kobylnice	Kobylnice	k1gFnPc4
•	•	k?
Kovalovice	Kovalovice	k1gFnPc4
•	•	k?
Kratochvilka	Kratochvilka	k1gFnSc1
•	•	k?
Křižínkov	Křižínkov	k1gInSc1
•	•	k?
Kupařovice	Kupařovice	k1gFnSc2
•	•	k?
Kuřim	Kuřim	k1gMnSc1
•	•	k?
Kuřimská	kuřimský	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
•	•	k?
Kuřimské	kuřimský	k2eAgNnSc4d1
Jestřabí	Jestřabí	k1gNnSc4
•	•	k?
Lažánky	Lažánka	k1gFnSc2
•	•	k?
Ledce	Ledce	k1gFnSc2
•	•	k?
Lelekovice	Lelekovice	k1gFnSc2
•	•	k?
Lesní	lesní	k2eAgFnPc1d1
Hluboké	Hluboká	k1gFnPc1
•	•	k?
Litostrov	Litostrov	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Loděnice	loděnice	k1gFnSc2
•	•	k?
Lomnice	Lomnice	k1gFnSc2
•	•	k?
Lomnička	Lomnička	k1gFnSc1
•	•	k?
Lubné	Lubná	k1gFnSc2
•	•	k?
Lukovany	Lukovan	k1gMnPc4
•	•	k?
Malešovice	Malešovice	k1gFnSc1
•	•	k?
Malhostovice	Malhostovice	k1gFnSc2
•	•	k?
Maršov	Maršov	k1gInSc1
•	•	k?
Medlov	Medlov	k1gInSc1
•	•	k?
Mělčany	Mělčan	k1gMnPc4
•	•	k?
Měnín	Měnín	k1gInSc1
•	•	k?
Modřice	Modřice	k1gFnSc2
•	•	k?
Mokrá-Horákov	Mokrá-Horákov	k1gInSc1
•	•	k?
Moravany	Moravan	k1gMnPc4
•	•	k?
Moravské	moravský	k2eAgFnSc2d1
Bránice	bránice	k1gFnSc2
•	•	k?
Moravské	moravský	k2eAgFnSc2d1
Knínice	Knínice	k1gFnSc2
•	•	k?
Moutnice	Moutnice	k1gFnSc2
•	•	k?
Nebovidy	Nebovida	k1gFnSc2
•	•	k?
Nedvědice	Nedvědice	k1gFnSc2
•	•	k?
Nelepeč-Žernůvka	Nelepeč-Žernůvka	k1gFnSc1
•	•	k?
Němčičky	Němčička	k1gFnSc2
•	•	k?
Neslovice	Neslovice	k1gFnSc2
•	•	k?
Nesvačilka	Nesvačilka	k1gFnSc1
•	•	k?
Níhov	Níhov	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Nosislav	Nosislav	k1gMnSc1
•	•	k?
Nová	nový	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
•	•	k?
Nové	Nová	k1gFnSc2
Bránice	bránice	k1gFnSc2
•	•	k?
Odrovice	Odrovice	k1gFnSc2
•	•	k?
Ochoz	ochoz	k1gInSc1
u	u	k7c2
Brna	Brno	k1gNnSc2
•	•	k?
Ochoz	ochoz	k1gInSc1
u	u	k7c2
Tišnova	Tišnov	k1gInSc2
•	•	k?
Olší	olše	k1gFnPc2
•	•	k?
Omice	Omice	k1gMnSc1
•	•	k?
Opatovice	Opatovice	k1gFnSc2
•	•	k?
Ořechov	Ořechov	k1gInSc1
•	•	k?
Osiky	osika	k1gFnSc2
•	•	k?
Oslavany	Oslavany	k1gInPc4
•	•	k?
Ostopovice	Ostopovice	k1gFnSc2
•	•	k?
Ostrovačice	Ostrovačice	k1gFnSc2
•	•	k?
Otmarov	Otmarov	k1gInSc1
•	•	k?
Pasohlávky	Pasohlávka	k1gFnSc2
•	•	k?
Pernštejnské	pernštejnský	k2eAgNnSc4d1
Jestřabí	Jestřabí	k1gNnSc4
•	•	k?
Podolí	Podolí	k1gNnSc2
•	•	k?
Pohořelice	Pohořelice	k1gFnPc4
•	•	k?
Ponětovice	Ponětovice	k1gFnPc4
•	•	k?
Popovice	Popovice	k1gFnPc4
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Popůvky	Popůvka	k1gFnSc2
•	•	k?
Pozořice	Pozořice	k1gFnSc2
•	•	k?
Prace	Prace	k1gFnSc2
•	•	k?
Pravlov	Pravlov	k1gInSc1
•	•	k?
Prštice	Prštice	k1gFnSc1
•	•	k?
Předklášteří	Předklášteří	k1gNnPc2
•	•	k?
Přibice	Přibice	k1gFnSc2
•	•	k?
Příbram	Příbram	k1gFnSc1
na	na	k7c6
Moravě	Morava	k1gFnSc6
•	•	k?
Přibyslavice	Přibyslavice	k1gFnSc2
•	•	k?
Přísnotice	Přísnotice	k1gFnSc2
•	•	k?
Radostice	Radostika	k1gFnSc3
•	•	k?
Rajhrad	Rajhrad	k1gInSc1
•	•	k?
Rajhradice	Rajhradice	k1gFnSc2
•	•	k?
Rašov	Rašov	k1gInSc1
•	•	k?
Rebešovice	Rebešovice	k1gFnSc2
•	•	k?
Rohozec	Rohozec	k1gMnSc1
•	•	k?
Rojetín	Rojetín	k1gMnSc1
•	•	k?
Rosice	Rosice	k1gFnPc4
•	•	k?
Rozdrojovice	Rozdrojovice	k1gFnPc4
•	•	k?
Rudka	rudka	k1gFnSc1
•	•	k?
Řícmanice	Řícmanice	k1gFnSc1
•	•	k?
Říčany	Říčany	k1gInPc1
•	•	k?
Říčky	říčka	k1gFnSc2
•	•	k?
Řikonín	Řikonín	k1gMnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Senorady	Senorada	k1gFnSc2
•	•	k?
Sentice	Sentice	k1gFnSc2
•	•	k?
Silůvky	Silůvka	k1gFnSc2
•	•	k?
Sivice	Sivice	k1gFnSc2
•	•	k?
Skalička	Skalička	k1gMnSc1
•	•	k?
Skryje	skrýt	k5eAaPmIp3nS
•	•	k?
Sobotovice	Sobotovice	k1gFnSc1
•	•	k?
Sokolnice	sokolnice	k1gFnSc1
•	•	k?
Stanoviště	stanoviště	k1gNnSc1
•	•	k?
Strhaře	strhař	k1gMnSc2
•	•	k?
Střelice	střelice	k1gFnSc2
•	•	k?
Svatoslav	Svatoslav	k1gMnSc1
•	•	k?
Synalov	Synalov	k1gInSc1
•	•	k?
Syrovice	syrovice	k1gFnSc2
•	•	k?
Šerkovice	Šerkovice	k1gFnSc2
•	•	k?
Šlapanice	Šlapanice	k1gFnSc2
•	•	k?
Štěpánovice	Štěpánovice	k1gFnSc2
•	•	k?
Šumice	Šumice	k1gFnSc2
•	•	k?
Telnice	Telnice	k1gFnSc2
•	•	k?
Těšany	Těšana	k1gFnSc2
•	•	k?
Tetčice	Tetčice	k1gFnSc2
•	•	k?
Tišnov	Tišnov	k1gInSc1
•	•	k?
Tišnovská	tišnovský	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
•	•	k?
Trboušany	Trboušana	k1gFnSc2
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Troskotovice	Troskotovice	k1gFnSc1
•	•	k?
Troubsko	Troubsko	k1gNnSc1
•	•	k?
Tvarožná	Tvarožný	k2eAgFnSc1d1
•	•	k?
Újezd	Újezd	k1gInSc1
u	u	k7c2
Brna	Brno	k1gNnSc2
•	•	k?
Újezd	Újezd	k1gInSc1
u	u	k7c2
Rosic	Rosice	k1gFnPc2
•	•	k?
Újezd	Újezd	k1gInSc1
u	u	k7c2
Tišnova	Tišnov	k1gInSc2
•	•	k?
Unín	Unín	k1gMnSc1
•	•	k?
Unkovice	Unkovice	k1gFnSc2
•	•	k?
Úsuší	Úsuší	k2eAgFnSc1d1
•	•	k?
Velatice	Velatice	k1gFnSc1
•	•	k?
Veverská	Veverský	k2eAgFnSc1d1
Bítýška	Bítýška	k1gFnSc1
•	•	k?
Veverské	Veverský	k2eAgFnSc2d1
Knínice	Knínice	k1gFnSc2
•	•	k?
Viničné	Viničný	k2eAgFnSc2d1
Šumice	Šumice	k1gFnSc2
•	•	k?
Vlasatice	vlasatice	k1gFnSc2
•	•	k?
Vohančice	Vohančice	k1gFnSc2
•	•	k?
Vojkovice	Vojkovice	k1gFnSc2
•	•	k?
Vranov	Vranov	k1gInSc1
•	•	k?
Vranovice	Vranovice	k1gFnSc2
•	•	k?
Vratislávka	Vratislávka	k1gFnSc1
•	•	k?
Všechovice	Všechovice	k1gFnSc2
•	•	k?
Vysoké	vysoký	k2eAgFnPc1d1
Popovice	Popovice	k1gFnPc1
•	•	k?
Zakřany	Zakřan	k1gMnPc4
•	•	k?
Zálesná	Zálesný	k2eAgFnSc1d1
Zhoř	Zhoř	k1gFnSc1
•	•	k?
Zastávka	zastávka	k1gFnSc1
•	•	k?
Zbraslav	Zbraslav	k1gFnSc1
•	•	k?
Zbýšov	Zbýšov	k1gInSc1
•	•	k?
Zhoř	Zhoř	k1gInSc1
•	•	k?
Žabčice	Žabčice	k1gFnSc2
•	•	k?
Žatčany	Žatčan	k1gMnPc4
•	•	k?
Žďárec	Žďárec	k1gInSc1
•	•	k?
Želešice	Želešice	k1gFnSc1
•	•	k?
Železné	železný	k2eAgFnPc4d1
•	•	k?
Židlochovice	Židlochovice	k1gFnPc4
legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
město	město	k1gNnSc1
<g/>
,	,	kIx,
městys	městys	k1gInSc1
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
</s>
