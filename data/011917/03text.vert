<p>
<s>
Flerovium	Flerovium	k1gNnSc1	Flerovium
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Fl	Fl	k1gFnSc2	Fl
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Flerovium	Flerovium	k1gNnSc4	Flerovium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
radioaktivní	radioaktivní	k2eAgInSc4d1	radioaktivní
transuran	transuran	k1gInSc4	transuran
s	s	k7c7	s
protonovým	protonový	k2eAgNnSc7d1	protonové
číslem	číslo	k1gNnSc7	číslo
114	[number]	k4	114
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
názvu	název	k1gInSc2	název
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
IUPAC	IUPAC	kA	IUPAC
předložila	předložit	k5eAaPmAgFnS	předložit
k	k	k7c3	k
veřejné	veřejný	k2eAgFnSc3d1	veřejná
diskusi	diskuse	k1gFnSc3	diskuse
své	svůj	k3xOyFgNnSc4	svůj
doporučení	doporučení	k1gNnSc4	doporučení
<g/>
,	,	kIx,	,
pojmenovat	pojmenovat	k5eAaPmF	pojmenovat
prvek	prvek	k1gInSc1	prvek
flerovium	flerovium	k1gNnSc1	flerovium
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
objevitelů	objevitel	k1gMnPc2	objevitel
–	–	k?	–
na	na	k7c6	na
počest	počest	k1gFnSc6	počest
fyzika	fyzik	k1gMnSc2	fyzik
Georgije	Georgije	k1gMnSc2	Georgije
Nikolajeviče	Nikolajevič	k1gMnSc2	Nikolajevič
Fljorova	Fljorův	k2eAgMnSc2d1	Fljorův
(	(	kIx(	(
<g/>
Г	Г	k?	Г
Н	Н	k?	Н
Ф	Ф	k?	Ф
<g/>
,	,	kIx,	,
1913	[number]	k4	1913
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
objevitele	objevitel	k1gMnSc2	objevitel
samovolného	samovolný	k2eAgNnSc2d1	samovolné
štěpení	štěpení	k1gNnSc2	štěpení
a	a	k8xC	a
spoluzakladatele	spoluzakladatel	k1gMnSc2	spoluzakladatel
Spojeného	spojený	k2eAgInSc2d1	spojený
ústavu	ústav	k1gInSc2	ústav
jaderných	jaderný	k2eAgInPc2d1	jaderný
výzkumů	výzkum	k1gInPc2	výzkum
v	v	k7c6	v
Dubně	Dubna	k1gFnSc6	Dubna
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
jméno	jméno	k1gNnSc1	jméno
bylo	být	k5eAaImAgNnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
schváleno	schválit	k5eAaPmNgNnS	schválit
IUPAC	IUPAC	kA	IUPAC
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
příprava	příprava	k1gFnSc1	příprava
tohoto	tento	k3xDgInSc2	tento
prvku	prvek	k1gInSc2	prvek
byla	být	k5eAaImAgFnS	být
neoficiálně	neoficiálně	k6eAd1	neoficiálně
oznámena	oznámit	k5eAaPmNgFnS	oznámit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1999	[number]	k4	1999
skupinou	skupina	k1gFnSc7	skupina
vědců	vědec	k1gMnPc2	vědec
z	z	k7c2	z
Dubny	duben	k1gInPc5	duben
<g/>
.	.	kIx.	.
</s>
<s>
Stejná	stejný	k2eAgFnSc1d1	stejná
skupina	skupina	k1gFnSc1	skupina
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
později	pozdě	k6eAd2	pozdě
připravila	připravit	k5eAaPmAgFnS	připravit
jiný	jiný	k2eAgInSc4d1	jiný
izotop	izotop	k1gInSc4	izotop
tohoto	tento	k3xDgInSc2	tento
prvku	prvek	k1gInSc2	prvek
<g/>
.	.	kIx.	.
<g/>
Flerovium	Flerovium	k1gNnSc1	Flerovium
bylo	být	k5eAaImAgNnS	být
připraveno	připravit	k5eAaPmNgNnS	připravit
bombardováním	bombardování	k1gNnSc7	bombardování
jader	jádro	k1gNnPc2	jádro
244	[number]	k4	244
<g/>
Pu	Pu	k1gMnPc1	Pu
proudem	proud	k1gInSc7	proud
iontů	ion	k1gInPc2	ion
48	[number]	k4	48
<g/>
Ca	ca	kA	ca
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
4820	[number]	k4	4820
Ca	ca	kA	ca
+	+	kIx~	+
24494	[number]	k4	24494
Pu	Pu	k1gFnSc2	Pu
→	→	k?	→
292114	[number]	k4	292114
Fl	Fl	k1gFnSc1	Fl
→	→	k?	→
289114	[number]	k4	289114
Fl	Fl	k1gFnPc2	Fl
+	+	kIx~	+
3	[number]	k4	3
10	[number]	k4	10
n	n	k0	n
</s>
</p>
<p>
<s>
==	==	k?	==
Izotopy	izotop	k1gInPc4	izotop
==	==	k?	==
</s>
</p>
<p>
<s>
Doposud	doposud	k6eAd1	doposud
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
6	[number]	k4	6
izotopů	izotop	k1gInPc2	izotop
flerovia	flerovius	k1gMnSc2	flerovius
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
velmi	velmi	k6eAd1	velmi
malému	malý	k2eAgInSc3d1	malý
počtu	počet	k1gInSc3	počet
dosud	dosud	k6eAd1	dosud
získaných	získaný	k2eAgNnPc2d1	získané
jader	jádro	k1gNnPc2	jádro
doposud	doposud	k6eAd1	doposud
nebyly	být	k5eNaImAgInP	být
pro	pro	k7c4	pro
některé	některý	k3yIgFnPc4	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
přesně	přesně	k6eAd1	přesně
určeny	určit	k5eAaPmNgFnP	určit
jejich	jejich	k3xOp3gFnPc1	jejich
jaderné	jaderný	k2eAgFnPc1d1	jaderná
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Přehled	přehled	k1gInSc4	přehled
dosud	dosud	k6eAd1	dosud
experimentálně	experimentálně	k6eAd1	experimentálně
zjištěných	zjištěný	k2eAgInPc2d1	zjištěný
izotopů	izotop	k1gInPc2	izotop
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
izomerních	izomerní	k2eAgInPc2d1	izomerní
stavů	stav	k1gInPc2	stav
<g/>
)	)	kIx)	)
uvádí	uvádět	k5eAaImIp3nS	uvádět
následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Isotopes	Isotopesa	k1gFnPc2	Isotopesa
of	of	k?	of
flerovium	flerovium	k1gNnSc1	flerovium
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
14	[number]	k4	14
<g/>
.	.	kIx.	.
skupina	skupina	k1gFnSc1	skupina
</s>
</p>
<p>
<s>
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
fyzika	fyzika	k1gFnSc1	fyzika
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
flerovium	flerovium	k1gNnSc4	flerovium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnPc1	galerie
flerovium	flerovium	k1gNnSc4	flerovium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
