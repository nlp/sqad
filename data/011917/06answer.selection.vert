<s>
Flerovium	Flerovium	k1gNnSc1	Flerovium
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Fl	Fl	k1gFnSc2	Fl
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Flerovium	Flerovium	k1gNnSc4	Flerovium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
radioaktivní	radioaktivní	k2eAgInSc4d1	radioaktivní
transuran	transuran	k1gInSc4	transuran
s	s	k7c7	s
protonovým	protonový	k2eAgNnSc7d1	protonové
číslem	číslo	k1gNnSc7	číslo
114	[number]	k4	114
<g/>
.	.	kIx.	.
</s>
