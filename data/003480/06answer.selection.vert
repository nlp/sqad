<s>
Svatba	svatba	k1gFnSc1	svatba
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
po	po	k7c6	po
téměř	téměř	k6eAd1	téměř
desetileté	desetiletý	k2eAgFnSc6d1	desetiletá
známosti	známost	k1gFnSc6	známost
roku	rok	k1gInSc2	rok
1842	[number]	k4	1842
a	a	k8xC	a
manželé	manžel	k1gMnPc1	manžel
Erbenovi	Erbenův	k2eAgMnPc1d1	Erbenův
měli	mít	k5eAaImAgMnP	mít
čtyři	čtyři	k4xCgFnPc4	čtyři
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
Blaženu	Blažena	k1gFnSc4	Blažena
(	(	kIx(	(
<g/>
1844	[number]	k4	1844
<g/>
–	–	k?	–
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ladislavu	Ladislava	k1gFnSc4	Ladislava
(	(	kIx(	(
<g/>
1846	[number]	k4	1846
<g/>
–	–	k?	–
<g/>
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jaromíra	Jaromíra	k1gFnSc1	Jaromíra
(	(	kIx(	(
<g/>
1848	[number]	k4	1848
<g/>
–	–	k?	–
<g/>
1849	[number]	k4	1849
<g/>
)	)	kIx)	)
a	a	k8xC	a
Bohuslavu	Bohuslava	k1gFnSc4	Bohuslava
(	(	kIx(	(
<g/>
1850	[number]	k4	1850
<g/>
–	–	k?	–
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
