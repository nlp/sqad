<s>
Míra	Míra	k1gFnSc1	Míra
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
podíl	podíl	k1gInSc4	podíl
nezaměstnaných	nezaměstnaný	k1gMnPc2	nezaměstnaný
ke	k	k7c3	k
všem	všecek	k3xTgFnPc3	všecek
osobám	osoba	k1gFnPc3	osoba
schopným	schopný	k2eAgMnPc3d1	schopný
pracovat	pracovat	k5eAaImF	pracovat
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
i	i	k9	i
zaměstnaným	zaměstnaný	k1gMnSc7	zaměstnaný
a	a	k8xC	a
nezaměstnaným	nezaměstnaný	k1gMnSc7	nezaměstnaný
<g/>
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
u	u	k7c2	u
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
E	E	kA	E
+	+	kIx~	+
U	u	k7c2	u
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
u	u	k7c2	u
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
<g />
.	.	kIx.	.
{	{	kIx(	{
<g/>
U	u	k7c2	u
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
E	E	kA	E
<g/>
+	+	kIx~	+
<g/>
U	U	kA	U
<g/>
}}}	}}}	k?	}}}
neboli	neboli	k8xC	neboli
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
u	u	k7c2	u
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
U	u	k7c2	u
L	L	kA	L
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
u	u	k7c2	u
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
U	u	k7c2	u
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
<g/>
L	L	kA	L
<g/>
}}}	}}}	k?	}}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
u	u	k7c2	u
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
u	u	k7c2	u
<g/>
}	}	kIx)	}
–	–	k?	–
míra	míra	k1gFnSc1	míra
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
(	(	kIx(	(
<g/>
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
často	často	k6eAd1	často
uvádí	uvádět	k5eAaImIp3nS	uvádět
v	v	k7c6	v
procentech	procento	k1gNnPc6	procento
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
hodnota	hodnota	k1gFnSc1	hodnota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
u	u	k7c2	u
⋅	⋅	k?	⋅
100	[number]	k4	100
:	:	kIx,	:
%	%	kIx~	%
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
.	.	kIx.	.
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
u	u	k7c2	u
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
100	[number]	k4	100
<g/>
~	~	kIx~	~
<g/>
\	\	kIx~	\
<g/>
%	%	kIx~	%
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
U	u	k7c2	u
<g/>
}	}	kIx)	}
–	–	k?	–
počet	počet	k1gInSc4	počet
lidí	člověk	k1gMnPc2	člověk
bez	bez	k7c2	bez
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E	E	kA	E
<g/>
}	}	kIx)	}
–	–	k?	–
počet	počet	k1gInSc4	počet
zaměstnaných	zaměstnaný	k2eAgMnPc2d1	zaměstnaný
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L	L	kA	L
<g/>
}	}	kIx)	}
–	–	k?	–
celkový	celkový	k2eAgInSc4d1	celkový
počet	počet	k1gInSc4	počet
pracovních	pracovní	k2eAgFnPc2d1	pracovní
sil	síla	k1gFnPc2	síla
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
=	=	kIx~	=
E	E	kA	E
+	+	kIx~	+
U	u	k7c2	u
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L	L	kA	L
<g/>
=	=	kIx~	=
<g/>
E	E	kA	E
<g/>
+	+	kIx~	+
<g/>
U	u	k7c2	u
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>

