<s>
Šamorín	Šamorín	k1gInSc1	Šamorín
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
Somorja	Somorj	k2eAgFnSc1d1	Somorj
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Sommerein	Sommerein	k1gInSc1	Sommerein
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Sancta	Sancta	k1gFnSc1	Sancta
Maria	Maria	k1gFnSc1	Maria
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
na	na	k7c6	na
jihozápadním	jihozápadní	k2eAgNnSc6d1	jihozápadní
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
cca	cca	kA	cca
17	[number]	k4	17
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Bratislavy	Bratislava	k1gFnSc2	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
Trnavském	trnavský	k2eAgInSc6d1	trnavský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Dunajská	dunajský	k2eAgFnSc1d1	Dunajská
Streda	Stred	k1gMnSc4	Stred
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
asi	asi	k9	asi
12	[number]	k4	12
726	[number]	k4	726
obyvatel	obyvatel	k1gMnPc2	obyvatel
převážně	převážně	k6eAd1	převážně
maďarské	maďarský	k2eAgFnSc2d1	maďarská
národnosti	národnost	k1gFnSc2	národnost
(	(	kIx(	(
<g/>
57,4	[number]	k4	57,4
%	%	kIx~	%
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Podunajské	podunajský	k2eAgFnSc6d1	Podunajská
rovině	rovina	k1gFnSc6	rovina
na	na	k7c6	na
Žitném	žitný	k2eAgInSc6d1	žitný
ostrově	ostrov	k1gInSc6	ostrov
<g/>
,	,	kIx,	,
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Dunaje	Dunaj	k1gInSc2	Dunaj
při	při	k7c6	při
vodním	vodní	k2eAgInSc6d1	vodní
díle	díl	k1gInSc6	díl
Gabčíkovo	Gabčíkovo	k1gNnSc1	Gabčíkovo
<g/>
,	,	kIx,	,
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
130	[number]	k4	130
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
5	[number]	k4	5
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
Šamorín	Šamorín	k1gInSc1	Šamorín
<g/>
,	,	kIx,	,
Bučuháza	Bučuháza	k1gFnSc1	Bučuháza
<g/>
,	,	kIx,	,
Čilistov	Čilistov	k1gInSc1	Čilistov
<g/>
,	,	kIx,	,
Kraľovianky	Kraľovianka	k1gFnPc1	Kraľovianka
a	a	k8xC	a
Mliečno	Mliečno	k1gNnSc1	Mliečno
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1238	[number]	k4	1238
a	a	k8xC	a
práva	právo	k1gNnPc4	právo
královského	královský	k2eAgNnSc2d1	královské
města	město	k1gNnSc2	město
získalo	získat	k5eAaPmAgNnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1405	[number]	k4	1405
od	od	k7c2	od
Zikmunda	Zikmund	k1gMnSc2	Zikmund
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
kalvínský	kalvínský	k2eAgInSc1d1	kalvínský
kostel	kostel	k1gInSc1	kostel
z	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
románsko-gotická	románskootický	k2eAgFnSc1d1	románsko-gotická
stavba	stavba	k1gFnSc1	stavba
původně	původně	k6eAd1	původně
zasvěcena	zasvětit	k5eAaPmNgFnS	zasvětit
Panně	Panna	k1gFnSc3	Panna
Marii	Maria	k1gFnSc3	Maria
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
ktoré	ktorý	k2eAgNnSc1d1	ktoré
dostalo	dostat	k5eAaPmAgNnS	dostat
jméno	jméno	k1gNnSc1	jméno
i	i	k8xC	i
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Římskokatolícky	Římskokatolícky	k6eAd1	Římskokatolícky
neskorobarokní	skorobarokní	k2eNgInSc1d1	skorobarokní
kostel	kostel	k1gInSc1	kostel
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
stojí	stát	k5eAaImIp3nS	stát
společně	společně	k6eAd1	společně
s	s	k7c7	s
klášterem	klášter	k1gInSc7	klášter
paulánů	paulán	k1gMnPc2	paulán
v	v	k7c6	v
centre	centr	k1gInSc5	centr
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
centre	centr	k1gInSc5	centr
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
taky	taky	k6eAd1	taky
klasicistický	klasicistický	k2eAgInSc1d1	klasicistický
evangelický	evangelický	k2eAgInSc1d1	evangelický
kostel	kostel	k1gInSc1	kostel
z	z	k7c2	z
konce	konec	k1gInSc2	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
sa	sa	k?	sa
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
synagoga	synagoga	k1gFnSc1	synagoga
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
sídlem	sídlo	k1gNnSc7	sídlo
At	At	k1gFnSc2	At
home	hom	k1gFnSc2	hom
gallery	galler	k1gInPc1	galler
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bývalé	bývalý	k2eAgFnSc6d1	bývalá
osadě	osada	k1gFnSc6	osada
Šámot	Šámota	k1gFnPc2	Šámota
se	se	k3xPyFc4	se
zachoval	zachovat	k5eAaPmAgInS	zachovat
románský	románský	k2eAgInSc1d1	románský
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Markéty	Markéta	k1gFnSc2	Markéta
Antiochijské	antiochijský	k2eAgFnSc2d1	Antiochijská
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dominika	Dominik	k1gMnSc4	Dominik
Stará	starat	k5eAaImIp3nS	starat
(	(	kIx(	(
<g/>
narozena	narozen	k2eAgFnSc1d1	narozena
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
-	-	kIx~	-
slovenská	slovenský	k2eAgFnSc1d1	slovenská
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Hainburg	Hainburg	k1gInSc1	Hainburg
an	an	k?	an
der	drát	k5eAaImRp2nS	drát
Donau	donau	k1gNnSc1	donau
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
Leiderdorp	Leiderdorp	k1gInSc1	Leiderdorp
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
Mosonmagyaróvár	Mosonmagyaróvár	k1gInSc1	Mosonmagyaróvár
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Šamorín	Šamorína	k1gFnPc2	Šamorína
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
