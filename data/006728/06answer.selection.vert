<s>
Mezi	mezi	k7c4	mezi
české	český	k2eAgMnPc4d1	český
skladatele	skladatel	k1gMnPc4	skladatel
hudebního	hudební	k2eAgInSc2d1	hudební
romantismu	romantismus	k1gInSc2	romantismus
patří	patřit	k5eAaImIp3nS	patřit
především	především	k9	především
Bedřich	Bedřich	k1gMnSc1	Bedřich
Smetana	Smetana	k1gMnSc1	Smetana
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
mj.	mj.	kA	mj.
cyklu	cyklus	k1gInSc3	cyklus
symfonických	symfonický	k2eAgFnPc2d1	symfonická
básní	báseň	k1gFnPc2	báseň
Má	mít	k5eAaImIp3nS	mít
vlast	vlast	k1gFnSc4	vlast
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
složil	složit	k5eAaPmAgInS	složit
symfonické	symfonický	k2eAgFnPc4d1	symfonická
básně	báseň	k1gFnPc4	báseň
Polednice	polednice	k1gFnSc2	polednice
<g/>
,	,	kIx,	,
Vodník	vodník	k1gMnSc1	vodník
<g/>
,	,	kIx,	,
Zlatý	zlatý	k2eAgInSc1d1	zlatý
kolovrat	kolovrat	k1gInSc1	kolovrat
a	a	k8xC	a
Holoubek	Holoubek	k1gMnSc1	Holoubek
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
Erbenových	Erbenových	k2eAgFnPc2d1	Erbenových
balad	balada	k1gFnPc2	balada
<g/>
.	.	kIx.	.
</s>
