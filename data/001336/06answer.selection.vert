<s>
Sodík	sodík	k1gInSc1	sodík
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Na	na	k7c4	na
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Natrium	natrium	k1gNnSc1	natrium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejběžnějším	běžný	k2eAgInSc7d3	nejběžnější
prvkem	prvek	k1gInSc7	prvek
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
hojně	hojně	k6eAd1	hojně
zastoupený	zastoupený	k2eAgInSc1d1	zastoupený
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
<g/>
,	,	kIx,	,
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
i	i	k8xC	i
živých	živý	k2eAgInPc6d1	živý
organismech	organismus	k1gInPc6	organismus
<g/>
.	.	kIx.	.
</s>
