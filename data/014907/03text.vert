<s>
Bouře	bouře	k1gFnSc1
(	(	kIx(
<g/>
Fibich	Fibich	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Bouře	boura	k1gFnSc3
Zdeněk	Zdeněk	k1gMnSc1
Fibich	Fibich	k1gMnSc1
(	(	kIx(
<g/>
kresba	kresba	k1gFnSc1
Jana	Jan	k1gMnSc2
Vilímka	Vilímek	k1gMnSc2
<g/>
,	,	kIx,
1881	#num#	k4
<g/>
)	)	kIx)
<g/>
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Žánr	žánr	k1gInSc1
</s>
<s>
zpěvohra	zpěvohra	k1gFnSc1
(	(	kIx(
<g/>
opera	opera	k1gFnSc1
<g/>
)	)	kIx)
Skladatel	skladatel	k1gMnSc1
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
Fibich	Fibich	k1gMnSc1
Libretista	libretista	k1gMnSc1
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Vrchlický	Vrchlický	k1gMnSc1
Počet	počet	k1gInSc4
dějství	dějství	k1gNnSc2
</s>
<s>
3	#num#	k4
Originální	originální	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
</s>
<s>
čeština	čeština	k1gFnSc1
Literární	literární	k2eAgFnSc1d1
předloha	předloha	k1gFnSc1
</s>
<s>
William	William	k1gInSc1
Shakespeare	Shakespeare	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Tempest	Tempest	k1gMnSc1
Datum	datum	k1gInSc4
vzniku	vznik	k1gInSc2
</s>
<s>
1893-94	1893-94	k4
Premiéra	premiéra	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1895	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Národní	národní	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Bouře	bouře	k1gFnSc1
(	(	kIx(
<g/>
Op	op	k1gMnSc1
<g/>
.	.	kIx.
40	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
opera	opera	k1gFnSc1
(	(	kIx(
<g/>
zpěvohra	zpěvohra	k1gFnSc1
<g/>
)	)	kIx)
o	o	k7c6
třech	tři	k4xCgNnPc6
jednáních	jednání	k1gNnPc6
českého	český	k2eAgMnSc2d1
skladatele	skladatel	k1gMnSc2
Zdeňka	Zdeněk	k1gMnSc2
Fibicha	Fibich	k1gMnSc2
na	na	k7c4
libreto	libreto	k1gNnSc4
českého	český	k2eAgMnSc2d1
básníka	básník	k1gMnSc2
Jaroslava	Jaroslav	k1gMnSc2
Vrchlického	Vrchlický	k1gMnSc2
<g/>
,	,	kIx,
napsané	napsaný	k2eAgNnSc1d1
na	na	k7c4
námět	námět	k1gInSc4
stejnojmenné	stejnojmenný	k2eAgFnSc2d1
hry	hra	k1gFnSc2
Williama	William	k1gMnSc2
Shakespeara	Shakespeare	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
byla	být	k5eAaImAgFnS
provedena	proveden	k2eAgFnSc1d1
dne	den	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1895	#num#	k4
v	v	k7c6
Národním	národní	k2eAgNnSc6d1
divadle	divadlo	k1gNnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Vznik	vznik	k1gInSc1
a	a	k8xC
historie	historie	k1gFnSc1
díla	dílo	k1gNnSc2
</s>
<s>
Po	po	k7c6
opeře	opera	k1gFnSc6
Nevěsta	nevěsta	k1gFnSc1
messinská	messinský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1884	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
předmětem	předmět	k1gInSc7
mnoha	mnoho	k4c2
kritických	kritický	k2eAgFnPc2d1
polemik	polemika	k1gFnPc2
a	a	k8xC
u	u	k7c2
publika	publikum	k1gNnSc2
propadla	propadnout	k5eAaPmAgFnS
<g/>
,	,	kIx,
se	se	k3xPyFc4
Fibich	Fibich	k1gMnSc1
na	na	k7c4
delší	dlouhý	k2eAgFnSc4d2
dobu	doba	k1gFnSc4
od	od	k7c2
operního	operní	k2eAgNnSc2d1
média	médium	k1gNnSc2
odvrátil	odvrátit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
cestě	cesta	k1gFnSc6
za	za	k7c4
přiblížení	přiblížení	k1gNnSc4
opery	opera	k1gFnSc2
dramatu	drama	k1gNnSc2
započaté	započatý	k2eAgNnSc4d1
„	„	k?
<g/>
Nevěstou	nevěsta	k1gFnSc7
<g/>
“	“	k?
pokračoval	pokračovat	k5eAaImAgInS
směrem	směr	k1gInSc7
k	k	k7c3
melodramu	melodram	k1gInSc3
a	a	k8xC
v	v	k7c6
letech	léto	k1gNnPc6
1889-1891	1889-1891	k4
vytvořil	vytvořit	k5eAaPmAgInS
melodramatickou	melodramatický	k2eAgFnSc4d1
trilogii	trilogie	k1gFnSc4
Hippodamie	Hippodamie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Návrat	návrat	k1gInSc1
k	k	k7c3
tradičnější	tradiční	k2eAgFnSc3d2
podobě	podoba	k1gFnSc3
hudebně-dramatické	hudebně-dramatický	k2eAgFnSc2d1
tvorby	tvorba	k1gFnSc2
souvisí	souviset	k5eAaImIp3nS
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
s	s	k7c7
počátkem	počátek	k1gInSc7
Fibichova	Fibichův	k2eAgInSc2d1
intenzivního	intenzivní	k2eAgInSc2d1
milostného	milostný	k2eAgInSc2d1
vztahu	vztah	k1gInSc2
s	s	k7c7
Anežkou	Anežka	k1gFnSc7
Schulzovou	Schulzová	k1gFnSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
datuje	datovat	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1892	#num#	k4
<g/>
,	,	kIx,
s	s	k7c7
mezinárodním	mezinárodní	k2eAgInSc7d1
úspěchem	úspěch	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
zažil	zažít	k5eAaPmAgMnS
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
při	při	k7c6
vídeňském	vídeňský	k2eAgNnSc6d1
provedení	provedení	k1gNnSc6
Námluv	námluva	k1gFnPc2
Pelopových	Pelopový	k2eAgFnPc2d1
a	a	k8xC
symfonie	symfonie	k1gFnSc1
č.	č.	k?
2	#num#	k4
Es	es	k1gNnSc2
dur	dur	k1gNnSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
se	s	k7c7
studiem	studio	k1gNnSc7
děl	dít	k5eAaBmAgMnS,k5eAaImAgMnS
Mozarta	Mozart	k1gMnSc2
<g/>
,	,	kIx,
Čajkovského	Čajkovský	k2eAgMnSc2d1
a	a	k8xC
Rimského-Korsakova	Rimského-Korsakův	k2eAgInSc2d1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc1
vliv	vliv	k1gInSc1
se	se	k3xPyFc4
v	v	k7c6
novém	nový	k2eAgInSc6d1
operním	operní	k2eAgInSc6d1
projektu	projekt	k1gInSc6
odráží	odrážet	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Se	s	k7c7
žádostí	žádost	k1gFnSc7
o	o	k7c4
libreto	libreto	k1gNnSc4
na	na	k7c6
„	„	k?
<g/>
světový	světový	k2eAgInSc4d1
<g/>
“	“	k?
(	(	kIx(
<g/>
tedy	tedy	k9
i	i	k9
v	v	k7c6
mimočeských	mimočeský	k2eAgNnPc6d1
divadlech	divadlo	k1gNnPc6
uplatnitelný	uplatnitelný	k2eAgInSc1d1
<g/>
)	)	kIx)
námět	námět	k1gInSc1
se	se	k3xPyFc4
Fibich	Fibich	k1gMnSc1
obrátil	obrátit	k5eAaPmAgMnS
na	na	k7c4
básníka	básník	k1gMnSc4
Jaroslava	Jaroslav	k1gMnSc4
Vrchlického	Vrchlický	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skladatel	skladatel	k1gMnSc1
již	již	k9
od	od	k7c2
počátku	počátek	k1gInSc2
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
udržoval	udržovat	k5eAaImAgMnS
úzký	úzký	k2eAgInSc4d1
styk	styk	k1gInSc4
s	s	k7c7
literáty	literát	k1gMnPc7
z	z	k7c2
kruhu	kruh	k1gInSc2
lumírovců	lumírovec	k1gMnPc2
a	a	k8xC
zejména	zejména	k9
s	s	k7c7
Vrchlickým	Vrchlický	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Libreto	libreto	k1gNnSc1
Bouře	bouř	k1gFnSc2
je	být	k5eAaImIp3nS
vyvrcholením	vyvrcholení	k1gNnSc7
jejich	jejich	k3xOp3gFnSc2
dlouhodobé	dlouhodobý	k2eAgFnSc2d1
spolupráce	spolupráce	k1gFnSc2
<g/>
,	,	kIx,
jejímiž	jejíž	k3xOyRp3gMnPc7
plody	plod	k1gInPc4
byly	být	k5eAaImAgInP
dříve	dříve	k6eAd2
kantáta	kantáta	k1gFnSc1
Jarní	jarní	k2eAgFnSc2d1
romance	romance	k1gFnSc2
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
koncertní	koncertní	k2eAgInPc4d1
melodramy	melodram	k1gInPc4
Královna	královna	k1gFnSc1
Emma	Emma	k1gFnSc1
a	a	k8xC
Hakon	Hakon	k1gNnSc1
<g/>
,	,	kIx,
předehra	předehra	k1gFnSc1
k	k	k7c3
Noci	noc	k1gFnSc3
na	na	k7c6
Karlštejně	Karlštejn	k1gInSc6
<g/>
,	,	kIx,
klavírní	klavírní	k2eAgInSc1d1
cyklus	cyklus	k1gInSc1
Z	z	k7c2
hor	hora	k1gFnPc2
a	a	k8xC
především	především	k9
Hippodamie	Hippodamie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Pro	pro	k7c4
Vrchlického	Vrchlický	k1gMnSc4
byla	být	k5eAaImAgFnS
Bouře	boura	k1gFnSc6
první	první	k4xOgNnSc4
operní	operní	k2eAgNnSc4d1
libreto	libreto	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc4,k3yQgNnSc4,k3yRgNnSc4
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
<g/>
;	;	kIx,
z	z	k7c2
hudebně-dramatických	hudebně-dramatický	k2eAgInPc2d1
útvarů	útvar	k1gInPc2
se	se	k3xPyFc4
dosud	dosud	k6eAd1
setkal	setkat	k5eAaPmAgMnS
jen	jen	k9
se	s	k7c7
scénickou	scénický	k2eAgFnSc7d1
kantátou	kantáta	k1gFnSc7
(	(	kIx(
<g/>
Švanda	švanda	k1gFnSc1
dudák	dudák	k1gInSc4
pro	pro	k7c4
Karla	Karel	k1gMnSc4
Bendla	Bendla	k1gMnSc4
<g/>
)	)	kIx)
a	a	k8xC
oratoriem	oratorium	k1gNnSc7
(	(	kIx(
<g/>
Svatá	svatý	k2eAgFnSc1d1
Ludmila	Ludmila	k1gFnSc1
pro	pro	k7c4
Antonína	Antonín	k1gMnSc4
Dvořáka	Dvořák	k1gMnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Oba	dva	k4xCgMnPc1
autoři	autor	k1gMnPc1
měli	mít	k5eAaImAgMnP
ke	k	k7c3
shakespaerovské	shakespaerovský	k2eAgFnSc3d1
tematice	tematika	k1gFnSc3
a	a	k8xC
konkrétně	konkrétně	k6eAd1
k	k	k7c3
Bouři	bouř	k1gFnSc3
vřelý	vřelý	k2eAgInSc4d1
vztah	vztah	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatně	ostatně	k6eAd1
Fibich	Fibich	k1gMnSc1
již	již	k6eAd1
roku	rok	k1gInSc2
1880	#num#	k4
zkomponoval	zkomponovat	k5eAaPmAgMnS
předehru	předehra	k1gFnSc4
Bouře	bouř	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
využil	využít	k5eAaPmAgMnS
i	i	k9
při	při	k7c6
kompozici	kompozice	k1gFnSc6
opery	opera	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
operním	operní	k2eAgNnSc6d1
zpracování	zpracování	k1gNnSc6
Shakespearovy	Shakespearův	k2eAgFnSc2d1
Bouře	bouř	k1gFnSc2
nebyl	být	k5eNaImAgMnS
Fibich	Fibich	k1gMnSc1
osamocen	osamotit	k5eAaPmNgMnS
<g/>
,	,	kIx,
na	na	k7c4
stejný	stejný	k2eAgInSc4d1
námět	námět	k1gInSc4
před	před	k7c7
ním	on	k3xPp3gInSc7
i	i	k9
po	po	k7c6
něm	on	k3xPp3gMnSc6
napsalo	napsat	k5eAaPmAgNnS,k5eAaBmAgNnS
svá	svůj	k3xOyFgNnPc4
díla	dílo	k1gNnPc4
téměř	téměř	k6eAd1
padesát	padesát	k4xCc1
dalších	další	k2eAgMnPc2d1
skladatelů	skladatel	k1gMnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
Johann	Johann	k1gMnSc1
Rudolf	Rudolf	k1gMnSc1
Zumsteeg	Zumsteeg	k1gMnSc1
<g/>
:	:	kIx,
Die	Die	k1gMnSc1
Geisterinsel	Geisterinsel	k1gMnSc1
(	(	kIx(
<g/>
1798	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Jacques	Jacques	k1gMnSc1
Fromental	Fromental	k1gMnSc1
Halévy	Haléva	k1gFnSc2
<g/>
:	:	kIx,
La	la	k1gNnSc1
tempesta	tempesta	k1gFnSc1
(	(	kIx(
<g/>
1850	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Frank	Frank	k1gMnSc1
Martin	Martin	k1gMnSc1
<g/>
:	:	kIx,
Der	drát	k5eAaImRp2nS
Sturm	Sturm	k1gInSc1
(	(	kIx(
<g/>
1956	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Thomas	Thomas	k1gMnSc1
Adè	Adè	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Tempest	Tempest	k1gMnSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Partitura	partitura	k1gFnSc1
vznikala	vznikat	k5eAaImAgFnS
poměrně	poměrně	k6eAd1
rychle	rychle	k6eAd1
v	v	k7c6
letech	léto	k1gNnPc6
1893-94	1893-94	k4
a	a	k8xC
byla	být	k5eAaImAgFnS
ihned	ihned	k6eAd1
přijata	přijmout	k5eAaPmNgFnS
k	k	k7c3
provozování	provozování	k1gNnSc3
Národním	národní	k2eAgNnSc7d1
divadlem	divadlo	k1gNnSc7
<g/>
;	;	kIx,
Fibich	Fibich	k1gMnSc1
se	s	k7c7
zmiňovanými	zmiňovaný	k2eAgInPc7d1
úspěchy	úspěch	k1gInPc7
na	na	k7c6
počátku	počátek	k1gInSc6
90	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc1
definitivně	definitivně	k6eAd1
zařadil	zařadit	k5eAaPmAgInS
mezi	mezi	k7c4
nejvýznamnější	významný	k2eAgMnPc4d3
české	český	k2eAgMnPc4d1
skladatele	skladatel	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Premiéra	premiéra	k1gFnSc1
se	se	k3xPyFc4
konala	konat	k5eAaImAgFnS
1	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1895	#num#	k4
v	v	k7c6
pečlivé	pečlivý	k2eAgFnSc6d1
výpravě	výprava	k1gFnSc6
a	a	k8xC
v	v	k7c6
režii	režie	k1gFnSc6
samotného	samotný	k2eAgMnSc2d1
ředitele	ředitel	k1gMnSc2
Národního	národní	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
F.	F.	kA
A.	A.	kA
Šuberta	Šubert	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Domácí	domácí	k2eAgFnSc1d1
i	i	k8xC
zahraniční	zahraniční	k2eAgFnSc1d1
kritika	kritika	k1gFnSc1
byly	být	k5eAaImAgFnP
vesměs	vesměs	k6eAd1
příznivé	příznivý	k2eAgFnPc1d1
a	a	k8xC
Bouře	bouře	k1gFnSc1
se	se	k3xPyFc4
dočkala	dočkat	k5eAaPmAgFnS
do	do	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
13	#num#	k4
repríz	repríza	k1gFnPc2
<g/>
,	,	kIx,
než	než	k8xS
ji	on	k3xPp3gFnSc4
z	z	k7c2
repertoáru	repertoár	k1gInSc2
vytlačila	vytlačit	k5eAaPmAgFnS
Fibichova	Fibichův	k2eAgFnSc1d1
následující	následující	k2eAgFnSc1d1
opera	opera	k1gFnSc1
Hedy	Heda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bouře	bouře	k1gFnSc1
spolu	spolu	k6eAd1
se	s	k7c7
dvěma	dva	k4xCgFnPc7
následujícími	následující	k2eAgFnPc7d1
operami	opera	k1gFnPc7
Hedy	Heda	k1gFnSc2
a	a	k8xC
Šárka	Šárka	k1gFnSc1
jsou	být	k5eAaImIp3nP
tradičně	tradičně	k6eAd1
zařazovány	zařazovat	k5eAaImNgFnP
do	do	k7c2
tzv.	tzv.	kA
„	„	k?
<g/>
lyrické	lyrický	k2eAgFnSc2d1
<g/>
“	“	k?
nebo	nebo	k8xC
též	též	k9
„	„	k?
<g/>
erotické	erotický	k2eAgFnSc2d1
trilogie	trilogie	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Libreto	libreto	k1gNnSc1
Bouře	bouř	k1gFnSc2
bylo	být	k5eAaImAgNnS
vydáni	vydán	k2eAgMnPc1d1
při	při	k7c6
příležitosti	příležitost	k1gFnSc6
premiéry	premiéra	k1gFnSc2
roku	rok	k1gInSc2
1895	#num#	k4
<g/>
,	,	kIx,
klavírní	klavírní	k2eAgInSc1d1
výtah	výtah	k1gInSc1
vyšel	vyjít	k5eAaPmAgInS
o	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celou	celý	k2eAgFnSc4d1
partituru	partitura	k1gFnSc4
vydalo	vydat	k5eAaPmAgNnS
nakladatelství	nakladatelství	k1gNnSc1
Dilia	Dilium	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1
díla	dílo	k1gNnSc2
</s>
<s>
Prospero	Prospero	k1gNnSc1
a	a	k8xC
Miranda	Miranda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obraz	obraz	k1gInSc1
Williama	William	k1gMnSc2
Mawa	Mawus	k1gMnSc2
Egleyho	Egley	k1gMnSc2
</s>
<s>
V	v	k7c6
libretu	libreto	k1gNnSc6
Bouře	bouř	k1gFnSc2
se	se	k3xPyFc4
Vrchlický	Vrchlický	k1gMnSc1
přidržuje	přidržovat	k5eAaImIp3nS
Shakespearovy	Shakespearův	k2eAgFnPc4d1
hry	hra	k1gFnPc4
s	s	k7c7
několika	několik	k4yIc7
odchylkami	odchylka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Text	text	k1gInSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
účely	účel	k1gInPc4
operního	operní	k2eAgNnSc2d1
zpracování	zpracování	k1gNnSc2
přirozeně	přirozeně	k6eAd1
značně	značně	k6eAd1
zkrácen	zkrácen	k2eAgInSc1d1
<g/>
,	,	kIx,
pět	pět	k4xCc1
jednání	jednání	k1gNnPc2
je	být	k5eAaImIp3nS
staženo	stažen	k2eAgNnSc1d1
na	na	k7c4
tří	tři	k4xCgInPc2
<g/>
,	,	kIx,
rovněž	rovněž	k9
jednotlivé	jednotlivý	k2eAgFnPc1d1
scény	scéna	k1gFnPc1
jsou	být	k5eAaImIp3nP
sloučeny	sloučen	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
požadavky	požadavek	k1gInPc7
na	na	k7c6
zhudebnění	zhudebnění	k1gNnSc6
jsou	být	k5eAaImIp3nP
posíleny	posílit	k5eAaPmNgFnP
příležitosti	příležitost	k1gFnPc1
pro	pro	k7c4
nasazení	nasazení	k1gNnSc4
sborů	sbor	k1gInPc2
(	(	kIx(
<g/>
duchů	duch	k1gMnPc2
a	a	k8xC
lidí-trosečníků	lidí-trosečník	k1gMnPc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
nejvíce	nejvíce	k6eAd1,k6eAd3
je	být	k5eAaImIp3nS
využit	využít	k5eAaPmNgInS
hudebně	hudebně	k6eAd1
efektní	efektní	k2eAgInSc1d1
kontrast	kontrast	k1gInSc1
mezi	mezi	k7c7
éterickým	éterický	k2eAgInSc7d1
Arielem	Ariel	k1gInSc7
a	a	k8xC
nízce	nízce	k6eAd1
tělesným	tělesný	k2eAgInSc7d1
Kalibanem	Kaliban	k1gInSc7
<g/>
,	,	kIx,
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
Fibichovou	Fibichův	k2eAgFnSc7d1
snahou	snaha	k1gFnSc7
o	o	k7c6
lyrizaci	lyrizace	k1gFnSc6
tématu	téma	k1gNnSc2
je	být	k5eAaImIp3nS
hlavní	hlavní	k2eAgFnSc1d1
pozornost	pozornost	k1gFnSc1
věnována	věnovat	k5eAaImNgFnS,k5eAaPmNgFnS
milostnému	milostný	k2eAgInSc3d1
vztahu	vztah	k1gInSc3
Fernanda	Fernando	k1gNnSc2
a	a	k8xC
Mirandy	Miranda	k1gFnSc2
vystavenému	vystavený	k2eAgInSc3d1
řadě	řada	k1gFnSc6
zkoušek	zkouška	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Zejména	zejména	k9
Vrchlický	Vrchlický	k1gMnSc1
doplnil	doplnit	k5eAaPmAgMnS
<g />
.	.	kIx.
</s>
<s hack="1">
scénu	scéna	k1gFnSc4
jejich	jejich	k3xOp3gFnSc2
šachové	šachový	k2eAgFnSc2d1
partie	partie	k1gFnSc2
<g/>
,	,	kIx,
při	při	k7c6
které	který	k3yIgFnSc6,k3yRgFnSc6,k3yQgFnSc6
se	se	k3xPyFc4
musí	muset	k5eAaImIp3nP
oba	dva	k4xCgMnPc1
milenci	milenec	k1gMnPc1
ovládat	ovládat	k5eAaImF
a	a	k8xC
nedat	dat	k5eNaBmF,k5eNaPmF,k5eNaImF
nijak	nijak	k6eAd1
fyzicky	fyzicky	k6eAd1
najevo	najevo	k6eAd1
svou	svůj	k3xOyFgFnSc4
lásku	láska	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Tato	tento	k3xDgFnSc1
pasáž	pasáž	k1gFnSc1
je	být	k5eAaImIp3nS
autostylizací	autostylizace	k1gFnSc7
Fibicha	Fibich	k1gMnSc2
jako	jako	k8xC,k8xS
Fernanda	Fernando	k1gNnPc1
a	a	k8xC
Anežky	Anežka	k1gFnPc1
Schulzové	Schulzová	k1gFnSc2
jako	jako	k8xC,k8xS
Mirandy	Miranda	k1gFnSc2
v	v	k7c6
jejich	jejich	k3xOp3gFnSc6
tehdejší	tehdejší	k2eAgFnSc6d1
situaci	situace	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
Vedle	vedle	k7c2
toho	ten	k3xDgNnSc2
Vrchlický	Vrchlický	k1gMnSc1
pozměnil	pozměnit	k5eAaPmAgMnS
vyznění	vyznění	k1gNnSc1
postavy	postava	k1gFnSc2
Prospera	Prospero	k1gNnSc2
(	(	kIx(
<g/>
„	„	k?
<g/>
Ne	ne	k9
kouzelník	kouzelník	k1gMnSc1
<g/>
,	,	kIx,
však	však	k9
člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
král	král	k1gMnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
po	po	k7c6
vzoru	vzor	k1gInSc6
zejména	zejména	k9
moudrého	moudrý	k2eAgMnSc2d1
Sarastra	Sarastr	k1gMnSc2
z	z	k7c2
Mozartovy	Mozartův	k2eAgFnSc2d1
Kouzelné	kouzelný	k2eAgFnSc2d1
flétny	flétna	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Celkové	celkový	k2eAgInPc1d1
pohádkové	pohádkový	k2eAgFnSc6d1
<g/>
,	,	kIx,
féerické	féerický	k2eAgFnSc6d1
náladě	nálada	k1gFnSc6
odpovídá	odpovídat	k5eAaImIp3nS
dramatická	dramatický	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
spočívající	spočívající	k2eAgFnSc1d1
v	v	k7c6
rychlém	rychlý	k2eAgInSc6d1
sledu	sled	k1gInSc6
poměrně	poměrně	k6eAd1
krátkých	krátká	k1gFnPc2
<g/>
,	,	kIx,
kontrastních	kontrastní	k2eAgFnPc2d1
scén	scéna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bouře	bouře	k1gFnSc1
je	být	k5eAaImIp3nS
„	„	k?
<g/>
nejsvětlejší	světlý	k2eAgFnSc1d3
opera	opera	k1gFnSc1
Fibichova	Fibichův	k2eAgFnSc1d1
<g/>
“	“	k?
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc1
„	„	k?
<g/>
jediná	jediný	k2eAgFnSc1d1
hudební	hudební	k2eAgFnSc1d1
pohádka	pohádka	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
totiž	totiž	k9
i	i	k9
jediná	jediný	k2eAgFnSc1d1
opera	opera	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
končí	končit	k5eAaImIp3nS
šťastně	šťastně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Tím	ten	k3xDgNnSc7
navazuje	navazovat	k5eAaImIp3nS
na	na	k7c4
dosud	dosud	k6eAd1
nejúsměvnější	úsměvný	k2eAgNnSc4d3
Fibichovo	Fibichův	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
<g/>
,	,	kIx,
kantátu	kantáta	k1gFnSc4
Jarní	jarní	k2eAgFnSc2d1
romance	romance	k1gFnSc2
rovněž	rovněž	k9
<g />
.	.	kIx.
</s>
<s hack="1">
na	na	k7c4
text	text	k1gInSc4
Vrchlického	Vrchlického	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Zejména	zejména	k9
ve	v	k7c6
scénách	scéna	k1gFnPc6
s	s	k7c7
Kalibanem	Kaliban	k1gInSc7
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
i	i	k9
humor	humor	k1gInSc4
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
ve	v	k7c6
Fibichových	Fibichových	k2eAgInPc6d1
dramatických	dramatický	k2eAgInPc6d1
dílech	díl	k1gInPc6
chybějící	chybějící	k2eAgMnSc1d1
<g/>
,	,	kIx,
snad	snad	k9
až	až	k9
na	na	k7c4
náznaky	náznak	k1gInPc4
v	v	k7c4
Hedy	Heda	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
Fibichových	Fibichových	k2eAgFnPc6d1
operách	opera	k1gFnPc6
<g />
.	.	kIx.
</s>
<s hack="1">
zde	zde	k6eAd1
hraje	hrát	k5eAaImIp3nS
velkou	velký	k2eAgFnSc4d1
roli	role	k1gFnSc4
osud	osud	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
však	však	k9
zosobněný	zosobněný	k2eAgInSc1d1
Prosperem	Prosper	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
moudře	moudřit	k5eAaImSgInS
a	a	k8xC
dobrotivě	dobrotivě	k6eAd1
řídí	řídit	k5eAaImIp3nS
kroky	krok	k1gInPc4
ostatních	ostatní	k2eAgFnPc2d1
postav	postava	k1gFnPc2
–	–	k?
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
neúprosného	úprosný	k2eNgInSc2d1
<g/>
,	,	kIx,
neosobního	osobní	k2eNgInSc2d1
tragického	tragický	k2eAgInSc2d1
osudu	osud	k1gInSc2
v	v	k7c6
Nevěstě	nevěsta	k1gFnSc6
messinské	messinský	k2eAgFnSc6d1
<g/>
,	,	kIx,
Hippodamii	Hippodamie	k1gFnSc6
nebo	nebo	k8xC
v	v	k7c6
Pádu	Pád	k1gInSc6
Arkuna	Arkuna	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fibich	Fibich	k1gMnSc1
tuto	tento	k3xDgFnSc4
operu	opera	k1gFnSc4
psal	psát	k5eAaImAgMnS
ve	v	k7c6
velmi	velmi	k6eAd1
intenzivní	intenzivní	k2eAgFnSc3d1
tvůrčí	tvůrčí	k2eAgFnSc3d1
etapě	etapa	k1gFnSc3
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
vznikla	vzniknout	k5eAaPmAgFnS
řada	řada	k1gFnSc1
významných	významný	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
(	(	kIx(
<g/>
symfonie	symfonie	k1gFnSc1
č.	č.	k?
2	#num#	k4
<g/>
,	,	kIx,
selanka	selanka	k1gFnSc1
V	v	k7c4
podvečer	podvečer	k1gInSc4
<g/>
,	,	kIx,
klavírní	klavírní	k2eAgInSc4d1
kvintet	kvintet	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Především	především	k6eAd1
se	se	k3xPyFc4
ale	ale	k9
doba	doba	k1gFnSc1
kompozice	kompozice	k1gFnSc1
Bouře	bouře	k1gFnSc1
časově	časově	k6eAd1
shoduje	shodovat	k5eAaImIp3nS
se	s	k7c7
vznikem	vznik	k1gInSc7
množství	množství	k1gNnSc2
drobných	drobný	k2eAgFnPc2d1
klavírních	klavírní	k2eAgFnPc2d1
skladeb	skladba	k1gFnPc2
a	a	k8xC
náčrtů	náčrt	k1gInPc2
později	pozdě	k6eAd2
uspořádaných	uspořádaný	k2eAgFnPc2d1
do	do	k7c2
I.	I.	kA
řady	řada	k1gFnSc2
Nálad	nálada	k1gFnPc2
<g/>
,	,	kIx,
dojmů	dojem	k1gInPc2
a	a	k8xC
upomínek	upomínka	k1gFnPc2
<g/>
,	,	kIx,
pro	pro	k7c4
něž	jenž	k3xRgMnPc4
nalezl	naleznout	k5eAaPmAgMnS,k5eAaBmAgMnS
inspiraci	inspirace	k1gFnSc4
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
vztahu	vztah	k1gInSc6
s	s	k7c7
Anežkou	Anežka	k1gFnSc7
Schulzovou	Schulzová	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bouře	bouře	k1gFnSc1
mezi	mezi	k7c7
Fibichovými	Fibichův	k2eAgFnPc7d1
operami	opera	k1gFnPc7
vyniká	vynikat	k5eAaImIp3nS
četností	četnost	k1gFnSc7
hudebních	hudební	k2eAgInPc2d1
motivů	motiv	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
řada	řada	k1gFnSc1
byla	být	k5eAaImAgFnS
převzata	převzít	k5eAaPmNgFnS
z	z	k7c2
klavírního	klavírní	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
nebo	nebo	k8xC
do	do	k7c2
něj	on	k3xPp3gInSc2
našla	najít	k5eAaPmAgFnS
zpětně	zpětně	k6eAd1
cestu	cesta	k1gFnSc4
z	z	k7c2
náčrtů	náčrt	k1gInPc2
opery	opera	k1gFnSc2
(	(	kIx(
<g/>
určení	určení	k1gNnSc4
jednoznačné	jednoznačný	k2eAgFnSc2d1
priority	priorita	k1gFnSc2
nebí	nebe	k1gNnPc2
vzhledem	vzhledem	k7c3
k	k	k7c3
současnému	současný	k2eAgInSc3d1
vzniku	vznik	k1gInSc3
vždy	vždy	k6eAd1
možné	možný	k2eAgFnSc3d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
například	například	k6eAd1
z	z	k7c2
Dojmů	dojem	k1gInPc2
věnovaných	věnovaný	k2eAgInPc2d1
popisu	popis	k1gInSc3
Anežčiny	Anežčin	k2eAgFnSc2d1
osoby	osoba	k1gFnSc2
byl	být	k5eAaImAgInS
převzat	převzít	k5eAaPmNgInS
jak	jak	k6eAd1
nadpozemsky	nadpozemsky	k6eAd1
průzračný	průzračný	k2eAgInSc4d1
motiv	motiv	k1gInSc4
Ariela	Ariela	k1gFnSc1
(	(	kIx(
<g/>
Dojem	dojem	k1gInSc1
č.	č.	k?
94	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
přízemně	přízemně	k6eAd1
tělesný	tělesný	k2eAgInSc4d1
motiv	motiv	k1gInSc4
Kalibana	Kalibana	k1gFnSc1
(	(	kIx(
<g/>
Dojem	dojem	k1gInSc1
č.	č.	k?
119	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příznačné	příznačný	k2eAgInPc4d1
motivy	motiv	k1gInPc4
hlavních	hlavní	k2eAgMnPc2d1
hrdinů	hrdina	k1gMnPc2
<g/>
,	,	kIx,
Fernanda	Fernanda	k1gFnSc1
a	a	k8xC
Mirandy	Mirand	k1gInPc1
<g/>
,	,	kIx,
vznikly	vzniknout	k5eAaPmAgFnP
transformací	transformace	k1gFnPc2
Dojmů	dojem	k1gInPc2
č.	č.	k?
98	#num#	k4
<g/>
-	-	kIx~
<g/>
100	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
z	z	k7c2
nejvášnivějších	vášnivý	k2eAgInPc2d3
motivů	motiv	k1gInPc2
<g/>
,	,	kIx,
ozývající	ozývající	k2eAgFnSc2d1
se	se	k3xPyFc4
v	v	k7c4
okamžik	okamžik	k1gInSc4
Fernandova	Fernandův	k2eAgNnSc2d1
zamilování	zamilování	k1gNnSc2
při	při	k7c6
výkřiku	výkřik	k1gInSc6
„	„	k?
<g/>
Královno	královna	k1gFnSc5
má	mít	k5eAaImIp3nS
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
<g/>
,	,	kIx,
odpovídá	odpovídat	k5eAaImIp3nS
náladě	nálada	k1gFnSc3
č.	č.	k?
44	#num#	k4
Anežka	Anežka	k1gFnSc1
ve	v	k7c6
fialových	fialový	k2eAgInPc6d1
šatech	šat	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celý	celý	k2eAgInSc1d1
soubor	soubor	k1gInSc1
motivů	motiv	k1gInPc2
byl	být	k5eAaImAgInS
přejat	přejmout	k5eAaPmNgInS
do	do	k7c2
předehry	předehra	k1gFnSc2
ke	k	k7c3
3	#num#	k4
<g/>
.	.	kIx.
jednání	jednání	k1gNnSc6
a	a	k8xC
do	do	k7c2
následující	následující	k2eAgFnSc2d1
ústřední	ústřední	k2eAgFnSc2d1
šachové	šachový	k2eAgFnSc2d1
scény	scéna	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
duchové	duch	k1gMnPc1
našeptávají	našeptávat	k5eAaImIp3nP
Fernandovi	Fernand	k1gMnSc3
a	a	k8xC
Mirandě	Miranda	k1gFnSc3
erotické	erotický	k2eAgFnSc2d1
myšlenky	myšlenka	k1gFnSc2
<g/>
:	:	kIx,
při	při	k7c6
zmínce	zmínka	k1gFnSc6
o	o	k7c6
vlasech	vlas	k1gInPc6
zní	znět	k5eAaImIp3nS
Dojem	dojem	k1gInSc1
č.	č.	k?
51	#num#	k4
Anežčiny	Anežčin	k2eAgInPc4d1
vlasy	vlas	k1gInPc4
<g/>
,	,	kIx,
o	o	k7c6
očích	oko	k1gNnPc6
Dojem	dojem	k1gInSc1
č.	č.	k?
56	#num#	k4
Anežčiny	Anežčin	k2eAgFnPc1d1
oči	oko	k1gNnPc4
<g/>
,	,	kIx,
při	při	k7c6
polibku	polibek	k1gInSc6
Dojem	dojem	k1gInSc1
č.	č.	k?
53	#num#	k4
Anežčina	Anežčin	k2eAgNnPc4d1
ústa	ústa	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Oba	dva	k4xCgInPc4
hlavní	hlavní	k2eAgInPc4d1
motivy	motiv	k1gInPc4
opery	opera	k1gFnSc2
<g/>
,	,	kIx,
motiv	motiv	k1gInSc4
bouře	bouř	k1gFnSc2
a	a	k8xC
motiv	motiv	k1gInSc4
lásky	láska	k1gFnSc2
(	(	kIx(
<g/>
nejčistěji	čisto	k6eAd3
při	při	k7c6
vyznání	vyznání	k1gNnSc6
Fernanda	Fernando	k1gNnSc2
na	na	k7c6
počátku	počátek	k1gInSc6
2	#num#	k4
<g/>
.	.	kIx.
dějství	dějství	k1gNnSc6
Však	však	k8xC
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
spanilá	spanilý	k2eAgFnSc1d1
a	a	k8xC
milá	milý	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
starší	starý	k2eAgFnSc4d2
historii	historie	k1gFnSc4
<g/>
,	,	kIx,
pocházejí	pocházet	k5eAaImIp3nP
totiž	totiž	k9
ze	z	k7c2
zmíněné	zmíněný	k2eAgFnSc2d1
programní	programní	k2eAgFnSc2d1
ouvertury	ouvertura	k1gFnSc2
Bouře	bouř	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1880	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bouře	bouře	k1gFnSc1
je	být	k5eAaImIp3nS
nejméně	málo	k6eAd3
„	„	k?
<g/>
wagnerovská	wagnerovský	k2eAgFnSc1d1
<g/>
“	“	k?
z	z	k7c2
Fibichových	Fibichových	k2eAgFnPc2d1
oper	opera	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skladatel	skladatel	k1gMnSc1
sice	sice	k8xC
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
příznačných	příznačný	k2eAgInPc2d1
motivů	motiv	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
často	často	k6eAd1
spíše	spíše	k9
v	v	k7c6
náladotvorném	náladotvorný	k2eAgMnSc6d1
než	než	k8xS
dramatickém	dramatický	k2eAgMnSc6d1
duchu	duch	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
dokonce	dokonce	k9
<g/>
,	,	kIx,
zcela	zcela	k6eAd1
proti	proti	k7c3
wagnerovským	wagnerovský	k2eAgFnPc3d1
zásadám	zásada	k1gFnPc3
hudebního	hudební	k2eAgNnSc2d1
dramatu	drama	k1gNnSc2
<g/>
,	,	kIx,
až	až	k9
dodatečně	dodatečně	k6eAd1
podložil	podložit	k5eAaPmAgInS
text	text	k1gInSc1
hotové	hotová	k1gFnSc2
hudbě	hudba	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Nejde	jít	k5eNaImIp3nS
ani	ani	k8xC
tak	tak	k6eAd1
o	o	k7c4
drama	drama	k1gNnSc4
jako	jako	k8xC,k8xS
o	o	k7c4
lyrickou	lyrický	k2eAgFnSc4d1
pohádku	pohádka	k1gFnSc4
<g/>
,	,	kIx,
„	„	k?
<g/>
zpěvohru	zpěvohra	k1gFnSc4
hýřivých	hýřivý	k2eAgFnPc2d1
náladových	náladový	k2eAgFnPc2d1
barev	barva	k1gFnPc2
<g/>
“	“	k?
(	(	kIx(
<g/>
J.	J.	kA
<g />
.	.	kIx.
</s>
<s hack="1">
Jiránek	Jiránek	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Každá	každý	k3xTgFnSc1
postava	postava	k1gFnSc1
má	mít	k5eAaImIp3nS
zcela	zcela	k6eAd1
jiný	jiný	k2eAgInSc4d1
hudební	hudební	k2eAgInSc4d1
charakter	charakter	k1gInSc4
(	(	kIx(
<g/>
kouzelník	kouzelník	k1gMnSc1
Prospero	Prospero	k1gNnSc4
<g/>
,	,	kIx,
milenci	milenec	k1gMnPc1
Fernando	Fernanda	k1gFnSc5
a	a	k8xC
Miranda	Mirando	k1gNnSc2
<g/>
,	,	kIx,
vzdušný	vzdušný	k2eAgMnSc1d1
duch	duch	k1gMnSc1
Ariel	Ariel	k1gMnSc1
<g/>
,	,	kIx,
netvor	netvor	k1gMnSc1
Kaliban	Kaliban	k1gMnSc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnSc1
lidští	lidštit	k5eAaImIp3nS
kumpáni	kumpáni	k?
Stefano	Stefana	k1gFnSc5
a	a	k8xC
Trinkulo	Trinkula	k1gFnSc5
<g/>
,	,	kIx,
trosečníci	trosečník	k1gMnPc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
duchové	duchový	k2eAgInPc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
k	k	k7c3
tomu	ten	k3xDgNnSc3
přistupují	přistupovat	k5eAaImIp3nP
zvukomalebné	zvukomalebný	k2eAgInPc4d1
přírodní	přírodní	k2eAgInPc4d1
obrazy	obraz	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Za	za	k7c4
nejlepší	dobrý	k2eAgNnPc4d3
místa	místo	k1gNnPc4
opery	opera	k1gFnSc2
je	být	k5eAaImIp3nS
považováno	považován	k2eAgNnSc1d1
milostné	milostný	k2eAgNnSc1d1
vyznání	vyznání	k1gNnSc1
Fernandovo	Fernandův	k2eAgNnSc1d1
ve	v	k7c6
2	#num#	k4
<g/>
.	.	kIx.
dějství	dějství	k1gNnSc6
<g/>
,	,	kIx,
orchestrální	orchestrální	k2eAgInSc4d1
úvod	úvod	k1gInSc4
ke	k	k7c3
3	#num#	k4
<g/>
.	.	kIx.
dějství	dějství	k1gNnSc6
a	a	k8xC
celkově	celkově	k6eAd1
rozsáhlé	rozsáhlý	k2eAgInPc4d1
dvojzpěvy	dvojzpěv	k1gInPc4
ústřední	ústřední	k2eAgFnSc2d1
milenecké	milenecký	k2eAgFnSc2d1
dvojice	dvojice	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Inscenační	inscenační	k2eAgFnSc1d1
historie	historie	k1gFnSc1
opery	opera	k1gFnSc2
</s>
<s>
Inscenační	inscenační	k2eAgNnSc1d1
pěstování	pěstování	k1gNnSc1
Bouře	bouř	k1gFnSc2
bylo	být	k5eAaImAgNnS
především	především	k6eAd1
doménou	doména	k1gFnSc7
pražského	pražský	k2eAgNnSc2d1
Národního	národní	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
prvním	první	k4xOgInSc6
novém	nový	k2eAgNnSc6d1
nastudování	nastudování	k1gNnSc6
roku	rok	k1gInSc2
1912	#num#	k4
připraveném	připravený	k2eAgInSc6d1
Karlem	Karel	k1gMnSc7
Kovařovicem	Kovařovic	k1gMnSc7
byly	být	k5eAaImAgFnP
další	další	k2eAgFnPc1d1
inscenace	inscenace	k1gFnPc1
uvedeny	uvést	k5eAaPmNgFnP
k	k	k7c3
fibichovským	fibichovský	k2eAgNnPc3d1
výročím	výročí	k1gNnPc3
v	v	k7c6
letech	let	k1gInPc6
1920	#num#	k4
<g/>
,	,	kIx,
1925	#num#	k4
a	a	k8xC
1935	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatní	ostatní	k2eAgFnSc1d1
česká	český	k2eAgFnSc1d1
divadla	divadlo	k1gNnSc2
ji	on	k3xPp3gFnSc4
hrála	hrát	k5eAaImAgNnP
řidčeji	řídce	k6eAd2
(	(	kIx(
<g/>
Brno	Brno	k1gNnSc1
1909	#num#	k4
<g/>
,	,	kIx,
1936	#num#	k4
<g/>
,	,	kIx,
Moravská	moravský	k2eAgFnSc1d1
Ostrava	Ostrava	k1gFnSc1
1925	#num#	k4
<g/>
…	…	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
poválečné	poválečný	k2eAgFnSc6d1
době	doba	k1gFnSc6
byla	být	k5eAaImAgFnS
Bouře	bouře	k1gFnSc1
prováděna	provádět	k5eAaImNgFnS
mnohem	mnohem	k6eAd1
méně	málo	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pražské	pražský	k2eAgNnSc1d1
Národní	národní	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
a	a	k8xC
Zemské	zemský	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
Ostrava	Ostrava	k1gFnSc1
ji	on	k3xPp3gFnSc4
nastudovaly	nastudovat	k5eAaBmAgFnP
roku	rok	k1gInSc2
1947	#num#	k4
<g/>
,	,	kIx,
poté	poté	k6eAd1
ji	on	k3xPp3gFnSc4
uvedlo	uvést	k5eAaPmAgNnS
ještě	ještě	k9
Divadlo	divadlo	k1gNnSc1
Josefa	Josef	k1gMnSc2
Kajetána	Kajetán	k1gMnSc2
Tyla	Tyl	k1gMnSc2
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
roku	rok	k1gInSc2
1962	#num#	k4
a	a	k8xC
poslední	poslední	k2eAgFnSc7d1
českou	český	k2eAgFnSc7d1
inscenací	inscenace	k1gFnSc7
byla	být	k5eAaImAgFnS
inscenace	inscenace	k1gFnSc1
Národního	národní	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
z	z	k7c2
roku	rok	k1gInSc2
1969	#num#	k4
(	(	kIx(
<g/>
premiéra	premiéra	k1gFnSc1
11	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
<g/>
,	,	kIx,
derniéra	derniéra	k1gFnSc1
30	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosud	dosud	k6eAd1
poslední	poslední	k2eAgFnSc4d1
inscenaci	inscenace	k1gFnSc4
Fibichovy	Fibichův	k2eAgFnSc2d1
Bouře	bouř	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
současně	současně	k6eAd1
její	její	k3xOp3gNnSc4
první	první	k4xOgNnSc4
zahraniční	zahraniční	k2eAgNnSc4d1
provedení	provedení	k1gNnSc4
(	(	kIx(
<g/>
nepočítáme	počítat	k5eNaImIp1nP
<g/>
-li	-li	k?
jedinou	jediný	k2eAgFnSc4d1
slovenskou	slovenský	k2eAgFnSc4d1
inscenaci	inscenace	k1gFnSc4
<g/>
,	,	kIx,
totiž	totiž	k9
ve	v	k7c6
Státním	státní	k2eAgNnSc6d1
divadle	divadlo	k1gNnSc6
Košice	Košice	k1gInPc1
<g/>
,	,	kIx,
premiéra	premiéra	k1gFnSc1
17	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
1961	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
uvedlo	uvést	k5eAaPmAgNnS
divadlo	divadlo	k1gNnSc1
v	v	k7c6
německém	německý	k2eAgInSc6d1
Bielefeldu	Bielefeld	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Premiéra	premiéra	k1gFnSc1
se	se	k3xPyFc4
konala	konat	k5eAaImAgFnS
31	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2007	#num#	k4
a	a	k8xC
představení	představení	k1gNnSc4
dne	den	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
přenášela	přenášet	k5eAaImAgFnS
rozhlasová	rozhlasový	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
DeutschlandRadio	DeutschlandRadio	k6eAd1
<g/>
;	;	kIx,
z	z	k7c2
tohoto	tento	k3xDgInSc2
přenosu	přenos	k1gInSc2
existuje	existovat	k5eAaImIp3nS
i	i	k9
neoficiálně	oficiálně	k6eNd1,k6eAd1
dostupný	dostupný	k2eAgInSc1d1
záznam	záznam	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
prosinci	prosinec	k1gInSc6
2016	#num#	k4
je	být	k5eAaImIp3nS
plánována	plánovat	k5eAaImNgFnS
premiéra	premiéra	k1gFnSc1
nové	nový	k2eAgFnSc2d1
inscenace	inscenace	k1gFnSc2
opery	opera	k1gFnSc2
v	v	k7c6
Ostravě	Ostrava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Osoby	osoba	k1gFnPc1
a	a	k8xC
první	první	k4xOgNnSc4
obsazení	obsazení	k1gNnSc4
</s>
<s>
osobahlasový	osobahlasový	k2eAgInSc1d1
oborpremiéra	oborpremiér	k1gMnSc2
(	(	kIx(
<g/>
1.3	1.3	k4
<g/>
.1895	.1895	k4
<g/>
)	)	kIx)
</s>
<s>
Alonso	Alonsa	k1gFnSc5
<g/>
,	,	kIx,
král	král	k1gMnSc1
neapolskýbarytonFrantišek	neapolskýbarytonFrantišek	k1gMnSc1
Šír	Šír	k1gMnSc1
</s>
<s>
Sebastiano	Sebastiana	k1gFnSc5
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnPc2
bratrtenorFrantišek	bratrtenorFrantiška	k1gFnPc2
Morda	morda	k1gFnSc1
</s>
<s>
Fernando	Fernando	k6eAd1
<g/>
,	,	kIx,
syn	syn	k1gMnSc1
krále	král	k1gMnSc2
neapolskéhotenorVladislav	neapolskéhotenorVladislav	k1gMnSc1
Florjanský	Florjanský	k2eAgMnSc1d1
</s>
<s>
Antonio	Antonio	k1gMnSc1
<g/>
,	,	kIx,
bratr	bratr	k1gMnSc1
Prosperův	Prosperův	k2eAgMnSc1d1
<g/>
,	,	kIx,
nepravý	pravý	k2eNgMnSc1d1
vévoda	vévoda	k1gMnSc1
milánskýtenorHynek	milánskýtenorHynka	k1gFnPc2
Švejda	Švejda	k1gMnSc1
</s>
<s>
Gonzalo	Gonzat	k5eAaPmAgNnS,k5eAaImAgNnS
<g/>
,	,	kIx,
dvořanbarytonFrantišek	dvořanbarytonFrantišek	k1gMnSc1
Zápotocký	Zápotocký	k1gMnSc1
</s>
<s>
Adriano	Adriana	k1gFnSc5
<g/>
,	,	kIx,
dvořanbasJosef	dvořanbasJosef	k1gMnSc1
Vecko	Vecka	k1gMnSc5
</s>
<s>
Prospero	Prospero	k1gNnSc1
<g/>
,	,	kIx,
kouzelník	kouzelník	k1gMnSc1
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
vévoda	vévoda	k1gMnSc1
milánskýbarytonVáclav	milánskýbarytonVáclat	k5eAaPmDgInS
Viktorin	Viktorin	k1gInSc4
</s>
<s>
Miranda	Miranda	k1gFnSc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc1
dcerasopránRůžena	dcerasopránRůžen	k2eAgFnSc1d1
Maturová	Maturový	k2eAgFnSc1d1
</s>
<s>
KalibanbasVáclav	KalibanbasVáclat	k5eAaPmDgInS
Kliment	Kliment	k1gMnSc1
</s>
<s>
Trinkulo	Trinkula	k1gFnSc5
<g/>
,	,	kIx,
dobrodruhtenorAdolf	dobrodruhtenorAdolf	k1gMnSc1
Krössing	Krössing	k1gInSc1
</s>
<s>
Stefano	Stefana	k1gFnSc5
<g/>
,	,	kIx,
dobrodruhbasRobert	dobrodruhbasRobert	k1gMnSc1
Polák	Polák	k1gMnSc1
</s>
<s>
ArielsopránAnna	ArielsopránAnen	k2eAgFnSc1d1
Veselá	Veselá	k1gFnSc1
</s>
<s>
První	první	k4xOgMnSc1
duch	duch	k1gMnSc1
<g/>
…	…	k?
<g/>
Emma	Emma	k1gFnSc1
Veverková	Veverková	k1gFnSc1
</s>
<s>
Druhý	druhý	k4xOgMnSc1
duch	duch	k1gMnSc1
<g/>
…	…	k?
<g/>
Růžena	Růžena	k1gFnSc1
Vykoukalová	Vykoukalová	k1gFnSc1
</s>
<s>
Třetí	třetí	k4xOgMnSc1
duch	duch	k1gMnSc1
<g/>
…	…	k?
<g/>
Anna	Anna	k1gFnSc1
Adamcová	Adamcová	k1gFnSc1
</s>
<s>
Čtvrtý	čtvrtý	k4xOgMnSc1
duch	duch	k1gMnSc1
<g/>
…	…	k?
<g/>
Anna	Anna	k1gFnSc1
Rufferová	Rufferová	k1gFnSc1
</s>
<s>
Sbor	sbor	k1gInSc1
duchů	duch	k1gMnPc2
</s>
<s>
Dirigent	dirigent	k1gMnSc1
<g/>
:	:	kIx,
Adolf	Adolf	k1gMnSc1
Čech	Čech	k1gMnSc1
<g/>
,	,	kIx,
režisér	režisér	k1gMnSc1
<g/>
:	:	kIx,
František	František	k1gMnSc1
Adolf	Adolf	k1gMnSc1
Šubert	Šubert	k1gMnSc1
<g/>
,	,	kIx,
scéna	scéna	k1gFnSc1
<g/>
:	:	kIx,
Robert	Robert	k1gMnSc1
Holzer	Holzer	k1gMnSc1
<g/>
,	,	kIx,
kostýmy	kostým	k1gInPc1
<g/>
:	:	kIx,
František	František	k1gMnSc1
Kolár	Kolár	k1gMnSc1
</s>
<s>
Děj	děj	k1gInSc1
opery	opera	k1gFnSc2
</s>
<s>
Na	na	k7c6
malém	malý	k2eAgInSc6d1
ostrově	ostrov	k1gInSc6
vládne	vládnout	k5eAaImIp3nS
Prospero	Prospero	k1gNnSc1
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
milánský	milánský	k2eAgMnSc1d1
vévoda	vévoda	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kdysi	kdysi	k6eAd1
se	se	k3xPyFc4
raději	rád	k6eAd2
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
tajným	tajný	k2eAgFnPc3d1
vědám	věda	k1gFnPc3
a	a	k8xC
vládu	vláda	k1gFnSc4
důvěřivě	důvěřivě	k6eAd1
přenechal	přenechat	k5eAaPmAgInS
svému	svůj	k3xOyFgMnSc3
bratru	bratr	k1gMnSc3
Antoniovi	Antonio	k1gMnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
ten	ten	k3xDgMnSc1
jej	on	k3xPp3gInSc4
lstí	lest	k1gFnSc7
s	s	k7c7
pomocí	pomoc	k1gFnSc7
neapolského	neapolský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Alonsa	Alons	k1gMnSc2
zbavil	zbavit	k5eAaPmAgMnS
trůnu	trůn	k1gInSc2
a	a	k8xC
zanechal	zanechat	k5eAaPmAgInS
–	–	k?
i	i	k9
s	s	k7c7
malou	malý	k2eAgFnSc7d1
dcerkou	dcerka	k1gFnSc7
Mirandou	Miranda	k1gFnSc7
–	–	k?
v	v	k7c6
malém	malý	k2eAgInSc6d1
člunu	člun	k1gInSc6
na	na	k7c6
širém	širý	k2eAgNnSc6d1
moři	moře	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jen	jen	k6eAd1
díky	díky	k7c3
dvořanu	dvořan	k1gMnSc3
Gonzalovi	Gonzal	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yQgInSc4,k3yRgInSc4
Prosperovi	Prosper	k1gMnSc3
a	a	k8xC
Mirandě	Miranda	k1gFnSc3
zanechal	zanechat	k5eAaPmAgMnS
v	v	k7c6
loďce	loďka	k1gFnSc6
potravu	potrava	k1gFnSc4
a	a	k8xC
pár	pár	k4xCyI
nejdůležitějších	důležitý	k2eAgFnPc2d3
knih	kniha	k1gFnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
oba	dva	k4xCgMnPc1
mohli	moct	k5eAaImAgMnP
zachránit	zachránit	k5eAaPmF
na	na	k7c4
lidmi	člověk	k1gMnPc7
neobydleném	obydlený	k2eNgInSc6d1
ostrově	ostrov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prospěro	Prospěra	k1gFnSc5
<g/>
,	,	kIx,
znalý	znalý	k2eAgMnSc1d1
tajných	tajný	k2eAgFnPc2d1
věd	věda	k1gFnPc2
a	a	k8xC
kouzel	kouzlo	k1gNnPc2
<g/>
,	,	kIx,
přemohl	přemoct	k5eAaPmAgMnS
dosavadního	dosavadní	k2eAgMnSc4d1
pána	pán	k1gMnSc4
ostrova	ostrov	k1gInSc2
Kalibana	Kalibana	k1gFnSc1
<g/>
,	,	kIx,
syna	syn	k1gMnSc2
čarodějky	čarodějka	k1gFnSc2
Sycorax	Sycorax	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
dějství	dějství	k1gNnSc6
</s>
<s>
Scéna	scéna	k1gFnSc1
s	s	k7c7
Mirandou	Miranda	k1gFnSc7
a	a	k8xC
Fernandem	Fernand	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obraz	obraz	k1gInSc1
Angeliky	Angelika	k1gFnSc2
Kauffmannové	Kauffmannová	k1gFnSc2
z	z	k7c2
r.	r.	kA
1782	#num#	k4
</s>
<s>
Na	na	k7c6
moři	moře	k1gNnSc6
zuří	zuřit	k5eAaImIp3nS
bouře	bouře	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Miranda	Miranda	k1gFnSc1
se	se	k3xPyFc4
strachuje	strachovat	k5eAaImIp3nS
o	o	k7c4
loď	loď	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
poblíž	poblíž	k7c2
ostrova	ostrov	k1gInSc2
zmítají	zmítat	k5eAaImIp3nP
vlny	vlna	k1gFnPc1
<g/>
,	,	kIx,
a	a	k8xC
prosí	prosit	k5eAaImIp3nP
otce	otec	k1gMnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
posádku	posádka	k1gFnSc4
korábu	koráb	k1gInSc2
zachránil	zachránit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prospero	Prospero	k1gNnSc1
jí	on	k3xPp3gFnSc3
vysvětluje	vysvětlovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
bouři	bouř	k1gFnSc3
rozpoutal	rozpoutat	k5eAaPmAgMnS
on	on	k3xPp3gMnSc1
sám	sám	k3xTgMnSc1
<g/>
,	,	kIx,
neboť	neboť	k8xC
na	na	k7c6
lodi	loď	k1gFnSc6
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
Antonio	Antonio	k1gMnSc1
a	a	k8xC
Alonso	Alonsa	k1gFnSc5
se	se	k3xPyFc4
svými	svůj	k3xOyFgMnPc7
lidmi	člověk	k1gMnPc7
<g/>
;	;	kIx,
svržený	svržený	k2eAgMnSc1d1
vévoda	vévoda	k1gMnSc1
chce	chtít	k5eAaImIp3nS
využít	využít	k5eAaPmF
příležitosti	příležitost	k1gFnPc4
a	a	k8xC
oba	dva	k4xCgMnPc1
ztrestat	ztrestat	k5eAaPmF
(	(	kIx(
<g/>
árie	árie	k1gFnPc1
Let	léto	k1gNnPc2
dvanáct	dvanáct	k4xCc4
s	s	k7c7
tebou	ty	k3xPp2nSc7
tady	tady	k6eAd1
přebývám	přebývat	k5eAaImIp1nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Miranda	Miranda	k1gFnSc1
je	být	k5eAaImIp3nS
otcovým	otcův	k2eAgNnSc7d1
vyprávěním	vyprávění	k1gNnSc7
unavena	unavit	k5eAaPmNgFnS
a	a	k8xC
usíná	usínat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Přilétá	přilétat	k5eAaImIp3nS
duch	duch	k1gMnSc1
Ariel	Ariel	k1gMnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
je	být	k5eAaImIp3nS
Prosperovi	Prosperův	k2eAgMnPc1d1
poddán	poddán	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
pánův	pánův	k2eAgInSc4d1
rozkaz	rozkaz	k1gInSc4
utišil	utišit	k5eAaPmAgMnS
bouři	bouře	k1gFnSc4
a	a	k8xC
všechny	všechen	k3xTgMnPc4
trosečníky	trosečník	k1gMnPc4
dostal	dostat	k5eAaPmAgMnS
ve	v	k7c4
zdraví	zdraví	k1gNnSc4
na	na	k7c4
břeh	břeh	k1gInSc4
<g/>
,	,	kIx,
i	i	k8xC
loď	loď	k1gFnSc1
je	být	k5eAaImIp3nS
opět	opět	k6eAd1
vcelku	vcelku	k6eAd1
a	a	k8xC
čeká	čekat	k5eAaImIp3nS
v	v	k7c6
zátoce	zátoka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Propustí	propustit	k5eAaPmIp3nS
jej	on	k3xPp3gInSc4
nyní	nyní	k6eAd1
Prospero	Prospero	k1gNnSc4
ze	z	k7c2
služby	služba	k1gFnSc2
(	(	kIx(
<g/>
árie	árie	k1gFnPc1
Na	na	k7c6
valném	valný	k2eAgNnSc6d1
moři	moře	k1gNnSc6
bez	bez	k7c2
hrází	hráz	k1gFnPc2
mhou	mha	k1gFnSc7
<g/>
)	)	kIx)
<g/>
?	?	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
Prospero	Prospero	k1gNnSc1
připomíná	připomínat	k5eAaImIp3nS
Arielovi	Ariel	k1gMnSc3
vděčnost	vděčnost	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
je	být	k5eAaImIp3nS
mu	on	k3xPp3gMnSc3
duch	duch	k1gMnSc1
povinován	povinovat	k5eAaImNgMnS
za	za	k7c4
osvobození	osvobození	k1gNnSc4
z	z	k7c2
otroctví	otroctví	k1gNnSc2
u	u	k7c2
čarodějky	čarodějka	k1gFnSc2
Sycorax	Sycorax	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
prozatím	prozatím	k6eAd1
odmítá	odmítat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nařizuje	nařizovat	k5eAaImIp3nS
Arielovi	Arielův	k2eAgMnPc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
trosečníky	trosečník	k1gMnPc4
rozptýlil	rozptýlit	k5eAaPmAgMnS
a	a	k8xC
pomocí	pomocí	k7c2
přeludů	přelud	k1gInPc2
vodil	vodit	k5eAaImAgInS
bezcílně	bezcílně	k6eAd1
a	a	k8xC
do	do	k7c2
úmoru	úmor	k1gInSc2
po	po	k7c6
ostrově	ostrov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
Prosperovi	Prosper	k1gMnSc3
nechť	nechť	k9
přivede	přivést	k5eAaPmIp3nS
jen	jen	k9
Alonsova	Alonsův	k2eAgMnSc2d1
syna	syn	k1gMnSc2
Fernanda	Fernando	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
se	se	k3xPyFc4
má	mít	k5eAaImIp3nS
stát	stát	k5eAaPmF,k5eAaImF
Mirandiným	Mirandin	k2eAgMnSc7d1
manželem	manžel	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Miranda	Miranda	k1gFnSc1
procitá	procitat	k5eAaImIp3nS
<g/>
;	;	kIx,
otec	otec	k1gMnSc1
jí	jíst	k5eAaImIp3nS
do	do	k7c2
spánku	spánek	k1gInSc2
přičaroval	přičarovat	k5eAaPmAgMnS
sen	sen	k1gInSc4
o	o	k7c6
krásném	krásný	k2eAgMnSc6d1
mladíkovi	mladík	k1gMnSc6
(	(	kIx(
<g/>
árie	árie	k1gFnSc2
Ach	ach	k0
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
to	ten	k3xDgNnSc1
krásný	krásný	k2eAgInSc1d1
sen	sen	k1gInSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pak	pak	k6eAd1
Prospero	Prospero	k1gNnSc1
spílá	spílat	k5eAaImIp3nS
svému	svůj	k3xOyFgMnSc3
sluhovi	sluha	k1gMnSc3
Kalibanovi	Kaliban	k1gMnSc3
a	a	k8xC
nemilostivě	milostivě	k6eNd1
mu	on	k3xPp3gMnSc3
ukládá	ukládat	k5eAaImIp3nS
úkoly	úkol	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
mu	on	k3xPp3gMnSc3
vyčítá	vyčítat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vévoda	vévoda	k1gMnSc1
zbavil	zbavit	k5eAaPmAgMnS
vlády	vláda	k1gFnSc2
spočívající	spočívající	k2eAgFnSc2d1
ve	v	k7c6
slastném	slastný	k2eAgNnSc6d1
lenošení	lenošení	k1gNnSc6
<g/>
,	,	kIx,
žádá	žádat	k5eAaImIp3nS
po	po	k7c6
něm	on	k3xPp3gNnSc6
jako	jako	k9
odškodnění	odškodnění	k1gNnSc4
za	za	k7c4
manželku	manželka	k1gFnSc4
Mirandu	Mirand	k1gInSc2
a	a	k8xC
nakonec	nakonec	k6eAd1
mu	on	k3xPp3gMnSc3
vyhrožuje	vyhrožovat	k5eAaImIp3nS
odplatou	odplata	k1gFnSc7
–	–	k?
aniž	aniž	k8xC,k8xS
by	by	kYmCp3nS
to	ten	k3xDgNnSc1
s	s	k7c7
Prosperem	Prosper	k1gInSc7
pohnulo	pohnout	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
poté	poté	k6eAd1
chráněn	chránit	k5eAaImNgInS
neviditelným	viditelný	k2eNgInSc7d1
pláštěm	plášť	k1gInSc7
pozoruje	pozorovat	k5eAaImIp3nS
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
Ariel	Ariel	k1gInSc1
a	a	k8xC
duchové	duch	k1gMnPc1
vodí	vodit	k5eAaImIp3nP
sem	sem	k6eAd1
a	a	k8xC
tam	tam	k6eAd1
zmatené	zmatený	k2eAgMnPc4d1
ztroskotance	ztroskotanec	k1gMnPc4
(	(	kIx(
<g/>
sbor	sbor	k1gInSc1
se	s	k7c7
sóly	sólo	k1gNnPc7
Ostrov	ostrov	k1gInSc1
plný	plný	k2eAgInSc4d1
vděků	vděk	k1gInPc2
luzných	luzný	k2eAgInPc6d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ariel	Ariel	k1gInSc1
přivádí	přivádět	k5eAaImIp3nS
Fernanda	Fernanda	k1gFnSc1
a	a	k8xC
se	s	k7c7
sborem	sbor	k1gInSc7
duchů	duch	k1gMnPc2
mu	on	k3xPp3gMnSc3
zpívá	zpívat	k5eAaImIp3nS
o	o	k7c6
domnělé	domnělý	k2eAgFnSc6d1
otcově	otcův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
(	(	kIx(
<g/>
Fernandova	Fernandův	k2eAgFnSc1d1
árie	árie	k1gFnSc1
Jdu	jít	k5eAaImIp1nS
za	za	k7c7
hudbou	hudba	k1gFnSc7
a	a	k8xC
nevím	vědět	k5eNaImIp1nS
sám	sám	k3xTgMnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
zní	znět	k5eAaImIp3nS
<g/>
…	…	k?
Na	na	k7c6
břehu	břeh	k1gInSc6
v	v	k7c6
skalní	skalní	k2eAgFnSc6d1
moře	mora	k1gFnSc6
zátoce	zátoka	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
pohledu	pohled	k1gInSc6
na	na	k7c4
Mirandu	Miranda	k1gFnSc4
Fernando	Fernanda	k1gFnSc5
zapomíná	zapomínat	k5eAaImIp3nS
na	na	k7c4
otce	otec	k1gMnSc4
a	a	k8xC
je	být	k5eAaImIp3nS
dívkou	dívka	k1gFnSc7
okouzlen	okouzlit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Miranda	Miranda	k1gFnSc1
žasne	žasnout	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
zjevil	zjevit	k5eAaPmAgInS
mladík	mladík	k1gInSc1
ze	z	k7c2
snu	sen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
vzájemného	vzájemný	k2eAgNnSc2d1
uchvácení	uchvácení	k1gNnSc2
je	on	k3xPp3gNnPc4
vytrhne	vytrhnout	k5eAaPmIp3nS
Prospero	Prospero	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejprve	nejprve	k6eAd1
Ferdinanda	Ferdinand	k1gMnSc4
záměrně	záměrně	k6eAd1
urazí	urazit	k5eAaPmIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
z	z	k7c2
jeho	jeho	k3xOp3gFnSc2
reakce	reakce	k1gFnSc2
přesvědčil	přesvědčit	k5eAaPmAgMnS
o	o	k7c6
mladíkově	mladíkův	k2eAgFnSc6d1
statečnosti	statečnost	k1gFnSc6
<g/>
,	,	kIx,
pak	pak	k6eAd1
se	se	k3xPyFc4
s	s	k7c7
ním	on	k3xPp3gNnSc7
však	však	k9
smíří	smířit	k5eAaPmIp3nS
a	a	k8xC
předvede	předvést	k5eAaPmIp3nS
mu	on	k3xPp3gInSc3
svou	svůj	k3xOyFgFnSc4
kouzelnou	kouzelný	k2eAgFnSc4d1
moc	moc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chce	chtít	k5eAaImIp3nS
<g/>
-li	-li	k?
Fernando	Fernanda	k1gFnSc5
získat	získat	k5eAaPmF
Mirandu	Mirando	k1gNnSc3
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nP
nejprve	nejprve	k6eAd1
Prosperovi	Prosperův	k2eAgMnPc1d1
sloužit	sloužit	k5eAaImF
tuhou	tuhý	k2eAgFnSc7d1
prací	práce	k1gFnSc7
a	a	k8xC
k	k	k7c3
jeho	jeho	k3xOp3gFnSc3
dceři	dcera	k1gFnSc3
se	se	k3xPyFc4
pod	pod	k7c7
trestem	trest	k1gInSc7
smrti	smrt	k1gFnSc2
nesmí	smět	k5eNaImIp3nS
ani	ani	k9
přiblížit	přiblížit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
dějství	dějství	k1gNnSc6
</s>
<s>
Prospero	Prospero	k1gNnSc1
<g/>
,	,	kIx,
Miranda	Miranda	k1gFnSc1
<g/>
,	,	kIx,
Fernando	Fernanda	k1gFnSc5
a	a	k8xC
Kaliban	Kalibany	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malba	malba	k1gFnSc1
Williama	Williama	k1gFnSc1
Hogartha	Hogartha	k1gFnSc1
<g/>
,	,	kIx,
cca	cca	kA
1728	#num#	k4
</s>
<s>
V	v	k7c6
hlubokém	hluboký	k2eAgInSc6d1
tropickém	tropický	k2eAgInSc6d1
pralese	prales	k1gInSc6
Ferdinand	Ferdinand	k1gMnSc1
kácí	kácet	k5eAaImIp3nS
stromy	strom	k1gInPc4
a	a	k8xC
štípe	štípat	k5eAaImIp3nS
a	a	k8xC
rovná	rovnat	k5eAaImIp3nS
dříví	dříví	k1gNnSc1
<g/>
,	,	kIx,
přitom	přitom	k6eAd1
uvažuje	uvažovat	k5eAaImIp3nS
o	o	k7c4
vysilující	vysilující	k2eAgFnSc4d1
práci	práce	k1gFnSc4
<g/>
,	,	kIx,
o	o	k7c6
ztracené	ztracený	k2eAgFnSc6d1
vlasti	vlast	k1gFnSc6
i	i	k8xC
o	o	k7c6
Mirandě	Miranda	k1gFnSc6
(	(	kIx(
<g/>
árie	árie	k1gFnPc1
Již	již	k9
ruka	ruka	k1gFnSc1
moje	můj	k3xOp1gFnSc1
zemdlela	zemdlet	k5eAaPmAgFnS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Miranda	Miranda	k1gFnSc1
jej	on	k3xPp3gMnSc4
lituje	litovat	k5eAaImIp3nS
a	a	k8xC
přichází	přicházet	k5eAaImIp3nS
mu	on	k3xPp3gMnSc3
pomoci	pomoct	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgMnPc1
si	se	k3xPyFc3
přísahají	přísahat	k5eAaImIp3nP
věčnou	věčný	k2eAgFnSc4d1
lásku	láska	k1gFnSc4
(	(	kIx(
<g/>
duet	duet	k1gInSc4
Mne	já	k3xPp1nSc4
máte	mít	k5eAaImIp2nP
rád	rád	k6eAd1
<g/>
,	,	kIx,
já	já	k3xPp1nSc1
se	se	k3xPyFc4
vám	vy	k3xPp2nPc3
líbím	líbit	k5eAaImIp1nS
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prospero	Prospero	k1gNnSc1
to	ten	k3xDgNnSc1
sleduje	sledovat	k5eAaImIp3nS
se	s	k7c7
zalíbením	zalíbení	k1gNnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
když	když	k8xS
se	se	k3xPyFc4
milenci	milenec	k1gMnPc1
chtějí	chtít	k5eAaImIp3nP
obejmout	obejmout	k5eAaPmF
<g/>
,	,	kIx,
zasáhne	zasáhnout	k5eAaPmIp3nS
a	a	k8xC
rozvádí	rozvádět	k5eAaImIp3nS
je	on	k3xPp3gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Vplíží	vplížit	k5eAaPmIp3nS
se	se	k3xPyFc4
Kaliban	Kaliban	k1gMnSc1
v	v	k7c6
doprovodu	doprovod	k1gInSc6
dvou	dva	k4xCgNnPc2
dobrodruhů	dobrodruh	k1gMnPc2
ze	z	k7c2
ztroskotané	ztroskotaný	k2eAgFnSc2d1
posádky	posádka	k1gFnSc2
<g/>
,	,	kIx,
Stefana	Stefan	k1gMnSc2
a	a	k8xC
Trinkula	Trinkul	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vypráví	vyprávět	k5eAaImIp3nP
jim	on	k3xPp3gMnPc3
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
on	on	k3xPp3gMnSc1
sám	sám	k3xTgMnSc1
je	být	k5eAaImIp3nS
zachránil	zachránit	k5eAaPmAgMnS
z	z	k7c2
bouře	bouř	k1gFnSc2
vyvolané	vyvolaný	k2eAgFnSc2d1
zlým	zlý	k2eAgNnSc7d1
Prosperem	Prospero	k1gNnSc7
<g/>
,	,	kIx,
a	a	k8xC
o	o	k7c6
slávě	sláva	k1gFnSc6
své	svůj	k3xOyFgFnSc2
někdejší	někdejší	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
než	než	k8xS
mu	on	k3xPp3gMnSc3
ji	on	k3xPp3gFnSc4
kouzelník	kouzelník	k1gMnSc1
uchvátil	uchvátit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
mu	on	k3xPp3gMnSc3
oba	dva	k4xCgMnPc1
muži	muž	k1gMnPc1
pomohou	pomoct	k5eAaPmIp3nP
zabít	zabít	k5eAaPmF
Prospera	Prosper	k1gMnSc4
(	(	kIx(
<g/>
a	a	k8xC
to	ten	k3xDgNnSc1
vražením	vražení	k1gNnSc7
hřebu	hřeb	k1gInSc2
do	do	k7c2
lebky	lebka	k1gFnSc2
ve	v	k7c6
spánku	spánek	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
udělá	udělat	k5eAaPmIp3nS
je	být	k5eAaImIp3nS
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
království	království	k1gNnSc6
ministry	ministr	k1gMnPc4
(	(	kIx(
<g/>
árie	árie	k1gFnSc1
Tiše	tiš	k1gFnSc2
<g/>
,	,	kIx,
hoši	hoch	k1gMnPc1
milí	milý	k2eAgMnPc1d1
<g/>
,	,	kIx,
odlehlé	odlehlý	k2eAgNnSc1d1
zde	zde	k6eAd1
kyne	kynout	k5eAaImIp3nS
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stefano	Stefana	k1gFnSc5
a	a	k8xC
Trinkulo	Trinkula	k1gFnSc5
jej	on	k3xPp3gMnSc4
poslouchají	poslouchat	k5eAaImIp3nP
jen	jen	k9
na	na	k7c4
polovic	polovice	k1gFnPc2
a	a	k8xC
hledí	hledět	k5eAaImIp3nS
si	se	k3xPyFc3
pití	pití	k1gNnSc4
<g/>
,	,	kIx,
zato	zato	k6eAd1
Ariel	Ariel	k1gInSc1
vše	všechen	k3xTgNnSc4
pozorně	pozorně	k6eAd1
vyslechne	vyslechnout	k5eAaPmIp3nS
a	a	k8xC
zpravuje	zpravovat	k5eAaImIp3nS
o	o	k7c6
tom	ten	k3xDgNnSc6
Prospera	Prospera	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Arielovi	Arielův	k2eAgMnPc1d1
duchové	duch	k1gMnPc1
přivádějí	přivádět	k5eAaImIp3nP
na	na	k7c4
scénu	scéna	k1gFnSc4
vyčerpanou	vyčerpaný	k2eAgFnSc4d1
<g/>
,	,	kIx,
hladovou	hladový	k2eAgFnSc4d1
a	a	k8xC
žíznivou	žíznivý	k2eAgFnSc4d1
skupinu	skupina	k1gFnSc4
ostatních	ostatní	k2eAgMnPc2d1
trosečníků	trosečník	k1gMnPc2
(	(	kIx(
<g/>
Arielova	Arielův	k2eAgFnSc1d1
árie	árie	k1gFnSc1
Vy	vy	k3xPp2nPc1
po	po	k7c6
lese	les	k1gInSc6
<g/>
,	,	kIx,
vy	vy	k3xPp2nPc1
po	po	k7c6
vodách	voda	k1gFnPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vykouzlí	vykouzlit	k5eAaPmIp3nP
jim	on	k3xPp3gMnPc3
přelud	přelud	k1gInSc4
prostřeného	prostřený	k2eAgInSc2d1
stolu	stol	k1gInSc2
s	s	k7c7
jídlem	jídlo	k1gNnSc7
a	a	k8xC
pitím	pití	k1gNnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
kdykoli	kdykoli	k6eAd1
se	se	k3xPyFc4
lidé	člověk	k1gMnPc1
chtějí	chtít	k5eAaImIp3nP
něčeho	něco	k3yInSc2
dotknout	dotknout	k5eAaPmF
<g/>
,	,	kIx,
pokrm	pokrm	k1gInSc4
či	či	k8xC
nápoj	nápoj	k1gInSc4
se	s	k7c7
zahřměním	zahřmění	k1gNnSc7
zmizí	zmizet	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ariel	Ariel	k1gMnSc1
jim	on	k3xPp3gMnPc3
pak	pak	k6eAd1
vyčítá	vyčítat	k5eAaImIp3nS
jejich	jejich	k3xOp3gNnPc4
provinění	provinění	k1gNnPc4
(	(	kIx(
<g/>
árie	árie	k1gFnSc1
Vy	vy	k3xPp2nPc1
na	na	k7c4
kolena	koleno	k1gNnPc4
<g/>
,	,	kIx,
zpupní	zpupnět	k5eAaImIp3nP,k5eAaPmIp3nP
hříšníci	hříšník	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zjevuje	zjevovat	k5eAaImIp3nS
se	se	k3xPyFc4
Prospero	Prospero	k1gNnSc4
v	v	k7c6
plné	plný	k2eAgFnSc6d1
čarodějnickém	čarodějnický	k2eAgInSc6d1
oděvu	oděv	k1gInSc6
<g/>
,	,	kIx,
na	na	k7c6
dračím	dračí	k2eAgInSc6d1
voze	vůz	k1gInSc6
obklopen	obklopit	k5eAaPmNgMnS
duchy	duch	k1gMnPc7
<g/>
,	,	kIx,
a	a	k8xC
opakuje	opakovat	k5eAaImIp3nS
Arielova	Arielův	k2eAgNnPc4d1
slova	slovo	k1gNnPc4
(	(	kIx(
<g/>
árie	árie	k1gFnSc1
Vy	vy	k3xPp2nPc1
znáte	znát	k5eAaImIp2nP
všichni	všechen	k3xTgMnPc1
velkou	velká	k1gFnSc7
vinu	vinout	k5eAaImIp1nS
svoji	svůj	k3xOyFgFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
dává	dávat	k5eAaImIp3nS
poznat	poznat	k5eAaPmF
jako	jako	k9
někdejší	někdejší	k2eAgMnSc1d1
milánský	milánský	k2eAgMnSc1d1
vévoda	vévoda	k1gMnSc1
a	a	k8xC
zděšené	zděšený	k2eAgMnPc4d1
trosečníky	trosečník	k1gMnPc4
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
Antoniem	Antonio	k1gMnSc7
a	a	k8xC
Alonsem	Alons	k1gMnSc7
zbavuje	zbavovat	k5eAaImIp3nS
rozumu	rozum	k1gInSc3
–	–	k?
až	až	k6eAd1
na	na	k7c4
věrného	věrný	k2eAgMnSc4d1
Gonzala	Gonzala	k1gMnSc4
<g/>
,	,	kIx,
kterého	který	k3yRgMnSc2,k3yQgMnSc2,k3yIgMnSc2
odvádí	odvádět	k5eAaImIp3nS
s	s	k7c7
sebou	se	k3xPyFc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nezapomene	zapomenout	k5eNaPmIp3nS,k5eNaImIp3nS
ani	ani	k9
na	na	k7c4
Kalibana	Kaliban	k1gMnSc4
a	a	k8xC
žene	hnát	k5eAaImIp3nS
jej	on	k3xPp3gMnSc4
opět	opět	k6eAd1
k	k	k7c3
otrocké	otrocký	k2eAgFnSc3d1
práci	práce	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
dějství	dějství	k1gNnSc6
</s>
<s>
Šachová	šachový	k2eAgFnSc1d1
partie	partie	k1gFnSc1
Fernanda	Fernando	k1gNnSc2
a	a	k8xC
Mirandy	Miranda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obraz	obraz	k1gInSc1
Edwarda	Edward	k1gMnSc2
Reginalda	Reginald	k1gMnSc2
Framptona	Frampton	k1gMnSc2
</s>
<s>
Prospero	Prospero	k1gNnSc1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
alchymistické	alchymistický	k2eAgFnSc6d1
pracovně	pracovna	k1gFnSc6
hovoří	hovořit	k5eAaImIp3nS
s	s	k7c7
Arielem	Ariel	k1gInSc7
<g/>
:	:	kIx,
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
jen	jen	k9
ještě	ještě	k6eAd1
podrobit	podrobit	k5eAaPmF
Fernanda	Fernanda	k1gFnSc1
poslední	poslední	k2eAgFnSc1d1
zkoušce	zkouška	k1gFnSc3
<g/>
,	,	kIx,
pak	pak	k6eAd1
se	se	k3xPyFc4
Prospero	Prospero	k1gNnSc1
vrátí	vrátit	k5eAaPmIp3nS
do	do	k7c2
Milána	Milán	k1gInSc2
a	a	k8xC
Ariel	Ariel	k1gMnSc1
bude	být	k5eAaImBp3nS
propuštěn	propuštěn	k2eAgMnSc1d1
ze	z	k7c2
služby	služba	k1gFnSc2
(	(	kIx(
<g/>
duet	duet	k1gInSc1
Můj	můj	k3xOp1gInSc1
čacký	čacký	k2eAgMnSc5d1
hochu	hoch	k1gMnSc5
<g/>
,	,	kIx,
dobře	dobře	k6eAd1
jsi	být	k5eAaImIp2nS
se	se	k3xPyFc4
držel	držet	k5eAaImAgMnS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ariel	Ariel	k1gInSc1
varuje	varovat	k5eAaImIp3nS
Prospera	Prosper	k1gMnSc4
před	před	k7c7
Kalibanovým	Kalibanův	k2eAgInSc7d1
plánem	plán	k1gInSc7
a	a	k8xC
kouzelník	kouzelník	k1gMnSc1
mu	on	k3xPp3gMnSc3
nařizuje	nařizovat	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
plán	plán	k1gInSc1
překazil	překazit	k5eAaPmAgInS
<g/>
;	;	kIx,
trestat	trestat	k5eAaImF
není	být	k5eNaImIp3nS
třeba	třeba	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Kouzelník	kouzelník	k1gMnSc1
Mirandě	Miranda	k1gFnSc3
a	a	k8xC
Fernandovi	Fernand	k1gMnSc3
připomíná	připomínat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
lásce	láska	k1gFnSc6
nemá	mít	k5eNaImIp3nS
být	být	k5eAaImF
hlavní	hlavní	k2eAgFnSc4d1
smyslnost	smyslnost	k1gFnSc4
(	(	kIx(
<g/>
zpěv	zpěv	k1gInSc4
Pochopíte	pochopit	k5eAaPmIp2nP
<g/>
,	,	kIx,
až	až	k9
lásky	láska	k1gFnSc2
pravý	pravý	k2eAgInSc4d1
taj	taj	k1gInSc4
a	a	k8xC
cenu	cena	k1gFnSc4
zvíte	zvědět	k5eAaPmIp2nP
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
mají	mít	k5eAaImIp3nP
on	on	k3xPp3gMnSc1
i	i	k9
Miranda	Miranda	k1gFnSc1
splnit	splnit	k5eAaPmF
ještě	ještě	k6eAd1
jeden	jeden	k4xCgInSc4
úkol	úkol	k1gInSc4
<g/>
:	:	kIx,
sehrát	sehrát	k5eAaPmF
spolu	spolu	k6eAd1
šachovou	šachový	k2eAgFnSc4d1
partii	partie	k1gFnSc4
<g/>
,	,	kIx,
aniž	aniž	k8xS,k8xC
by	by	kYmCp3nP
se	se	k3xPyFc4
vzali	vzít	k5eAaPmAgMnP
za	za	k7c4
ruce	ruka	k1gFnPc4
nebo	nebo	k8xC
políbili	políbit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mladí	mladý	k2eAgMnPc1d1
milenci	milenec	k1gMnPc1
zasedají	zasedat	k5eAaImIp3nP
k	k	k7c3
šachovnici	šachovnice	k1gFnSc3
(	(	kIx(
<g/>
šachová	šachový	k2eAgFnSc1d1
scéna	scéna	k1gFnSc1
Ta	ten	k3xDgFnSc1
zkouška	zkouška	k1gFnSc1
bude	být	k5eAaImBp3nS
veselá	veselý	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Duchové	duch	k1gMnPc1
je	on	k3xPp3gMnPc4
svádějí	svádět	k5eAaImIp3nP
a	a	k8xC
vnukají	vnukat	k5eAaImIp3nP
jim	on	k3xPp3gInPc3
erotické	erotický	k2eAgFnPc1d1
myšlenky	myšlenka	k1gFnPc1
<g/>
,	,	kIx,
Fernando	Fernanda	k1gFnSc5
a	a	k8xC
Miranda	Mirando	k1gNnPc1
kolísají	kolísat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naštěstí	naštěstí	k6eAd1
je	být	k5eAaImIp3nS
zachrání	zachránit	k5eAaPmIp3nS
Ariel	Ariel	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
mezi	mezi	k7c7
nimi	on	k3xPp3gMnPc7
místo	místo	k1gNnSc1
milostného	milostný	k2eAgNnSc2d1
sblížení	sblížení	k1gNnSc2
vyvolá	vyvolat	k5eAaPmIp3nS
hádku	hádka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrací	vracet	k5eAaImIp3nS
se	se	k3xPyFc4
Prospero	Prospero	k1gNnSc1
<g/>
:	:	kIx,
oba	dva	k4xCgInPc1
ve	v	k7c6
zkoušce	zkouška	k1gFnSc6
obstál	obstát	k5eAaPmAgMnS
a	a	k8xC
nyní	nyní	k6eAd1
si	se	k3xPyFc3
mohou	moct	k5eAaImIp3nP
padnout	padnout	k5eAaPmF,k5eAaImF
do	do	k7c2
náruče	náruč	k1gFnSc2
jako	jako	k8xS,k8xC
snoubenci	snoubenec	k1gMnPc1
s	s	k7c7
požehnáním	požehnání	k1gNnSc7
otce	otec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
opuštěné	opuštěný	k2eAgFnSc2d1
laboratoře	laboratoř	k1gFnSc2
se	se	k3xPyFc4
vkrade	vkrást	k5eAaPmIp3nS
Kaliban	Kaliban	k1gInSc1
se	s	k7c7
Stefanem	Stefan	k1gMnSc7
a	a	k8xC
Trinkulem	Trinkul	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chystají	chystat	k5eAaImIp3nP
se	se	k3xPyFc4
zabít	zabít	k5eAaPmF
Prospera	Prospera	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
nemohou	moct	k5eNaImIp3nP
najít	najít	k5eAaPmF
připravený	připravený	k2eAgInSc4d1
hřeb	hřeb	k1gInSc4
a	a	k8xC
důmyslné	důmyslný	k2eAgInPc1d1
zásahy	zásah	k1gInPc1
Ariela	Ariel	k1gMnSc4
mezi	mezi	k7c7
nimi	on	k3xPp3gNnPc7
vyvolají	vyvolat	k5eAaPmIp3nP
hádku	hádka	k1gFnSc4
a	a	k8xC
potyčku	potyčka	k1gFnSc4
<g/>
,	,	kIx,
až	až	k6eAd1
je	být	k5eAaImIp3nS
nakonec	nakonec	k6eAd1
Prospero	Prospero	k1gNnSc1
dává	dávat	k5eAaImIp3nS
vyštvat	vyštvat	k5eAaPmF
pekelnými	pekelný	k2eAgFnPc7d1
psy	pes	k1gMnPc4
(	(	kIx(
<g/>
scéna	scéna	k1gFnSc1
Po	po	k7c6
špičkách	špička	k1gFnPc6
<g/>
!	!	kIx.
</s>
<s desamb="1">
Po	po	k7c6
špičkách	špička	k1gFnPc6
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ariel	Ariel	k1gInSc1
se	se	k3xPyFc4
již	již	k6eAd1
těší	těšit	k5eAaImIp3nS
na	na	k7c4
návrat	návrat	k1gInSc4
do	do	k7c2
své	svůj	k3xOyFgFnSc2
nadpozemské	nadpozemský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
(	(	kIx(
<g/>
árie	árie	k1gFnSc1
Úkol	úkol	k1gInSc1
můj	můj	k3xOp1gInSc1
jest	být	k5eAaImIp3nS
ukončen	ukončen	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Prospero	Prospero	k1gNnSc1
se	se	k3xPyFc4
jímavě	jímavě	k6eAd1
loučí	loučit	k5eAaImIp3nS
s	s	k7c7
ostrovem	ostrov	k1gInSc7
(	(	kIx(
<g/>
árie	árie	k1gFnSc1
Tak	tak	k9
<g/>
,	,	kIx,
ostrove	ostrov	k1gInSc5
můj	můj	k3xOp1gInSc1
<g/>
,	,	kIx,
sbohem	sbohem	k0
buď	buď	k8xC
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mávnutím	mávnutí	k1gNnSc7
kouzelné	kouzelný	k2eAgFnSc2d1
hůlky	hůlka	k1gFnSc2
mizí	mizet	k5eAaImIp3nS
jeho	jeho	k3xOp3gNnPc2
obydlí	obydlí	k1gNnPc2
a	a	k8xC
u	u	k7c2
břehu	břeh	k1gInSc2
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
koráb	koráb	k1gInSc1
přichystaný	přichystaný	k2eAgInSc1d1
k	k	k7c3
odplutí	odplutí	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Ariel	Ariel	k1gInSc1
přivádí	přivádět	k5eAaImIp3nS
pomatené	pomatený	k2eAgMnPc4d1
trosečníky	trosečník	k1gMnPc4
<g/>
,	,	kIx,
Prospero	Prospero	k1gNnSc4
jim	on	k3xPp3gMnPc3
vrací	vracet	k5eAaImIp3nS
rozum	rozum	k1gInSc1
a	a	k8xC
odpouští	odpouštět	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všichni	všechen	k3xTgMnPc1
se	se	k3xPyFc4
objímají	objímat	k5eAaImIp3nP
<g/>
,	,	kIx,
Fernando	Fernanda	k1gFnSc5
představuje	představovat	k5eAaImIp3nS
otci	otec	k1gMnSc3
svou	svůj	k3xOyFgFnSc4
nevěstu	nevěsta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prospero	Prospero	k1gNnSc1
odpouští	odpouštět	k5eAaImIp3nP
i	i	k9
Antoniovi	Antoniův	k2eAgMnPc1d1
a	a	k8xC
Kalibanovi	Kalibanův	k2eAgMnPc1d1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
vrhá	vrhat	k5eAaImIp3nS
k	k	k7c3
nohám	noha	k1gFnPc3
<g/>
,	,	kIx,
vrací	vracet	k5eAaImIp3nS
vládu	vláda	k1gFnSc4
nad	nad	k7c7
ostrovem	ostrov	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naposledy	naposledy	k6eAd1
dává	dávat	k5eAaImIp3nS
vroucně	vroucně	k6eAd1
sbohem	sbohem	k0
Arielovi	Arielovi	k1gRnPc1
(	(	kIx(
<g/>
árie	árie	k1gFnSc1
Ty	ty	k3xPp2nSc5
duchu	duch	k1gMnSc6
můj	můj	k3xOp1gInSc1
<g/>
,	,	kIx,
sem	sem	k6eAd1
vposled	vposled	k6eAd1
spěj	spět	k5eAaImRp2nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všichni	všechen	k3xTgMnPc1
lidé	člověk	k1gMnPc1
vstupují	vstupovat	k5eAaImIp3nP
na	na	k7c4
loď	loď	k1gFnSc4
a	a	k8xC
vyplouvají	vyplouvat	k5eAaImIp3nP
k	k	k7c3
domovu	domov	k1gInSc3
(	(	kIx(
<g/>
sbor	sbor	k1gInSc1
Zpět	zpět	k6eAd1
ku	k	k7c3
domovu	domov	k1gInSc3
<g/>
,	,	kIx,
vlasť	vlastit	k5eAaPmRp2nS
juž	juž	k?
čeká	čekat	k5eAaImIp3nS
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
</s>
<s>
Nahrávky	nahrávka	k1gFnPc1
</s>
<s>
1950	#num#	k4
(	(	kIx(
<g/>
rozhlasová	rozhlasový	k2eAgFnSc1d1
nahrávka	nahrávka	k1gFnSc1
<g/>
,	,	kIx,
rekonstruováno	rekonstruovat	k5eAaBmNgNnS
2000	#num#	k4
<g/>
,	,	kIx,
vydala	vydat	k5eAaPmAgFnS
Společnost	společnost	k1gFnSc1
Beno	Beno	k6eAd1
Blachuta	Blachut	k2eAgFnSc1d1
2012	#num#	k4
<g/>
,	,	kIx,
SBB	SBB	kA
0	#num#	k4
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
)	)	kIx)
Zpívají	zpívat	k5eAaImIp3nP
(	(	kIx(
<g/>
Alonso	Alonsa	k1gFnSc5
<g/>
)	)	kIx)
Bořek	Bořek	k1gMnSc1
Rujan	Rujan	k1gMnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
Sebastiano	Sebastiana	k1gFnSc5
<g/>
)	)	kIx)
Karel	Karel	k1gMnSc1
Leiss	Leissa	k1gFnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
(	(	kIx(
<g/>
Fernando	Fernanda	k1gFnSc5
<g/>
)	)	kIx)
Beno	Beno	k1gMnSc1
Blachut	Blachut	k1gMnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
Antonio	Antonio	k1gMnSc1
<g/>
)	)	kIx)
Rudolf	Rudolf	k1gMnSc1
Vonásek	Vonásek	k1gMnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
Gonzalo	Gonzalo	k1gMnSc1
<g/>
)	)	kIx)
Ladislav	Ladislav	k1gMnSc1
Mráz	Mráz	k1gMnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
Adriano	Adriana	k1gFnSc5
<g/>
)	)	kIx)
Jan	Jan	k1gMnSc1
Soumar	soumar	k1gMnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
Prospero	Prospero	k1gNnSc1
<g/>
)	)	kIx)
Zdeněk	Zdeněk	k1gMnSc1
Otava	Otava	k1gFnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
Miranda	Miranda	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
Ludmila	Ludmila	k1gFnSc1
Červinková	Červinková	k1gFnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
Kaliban	Kaliban	k1gMnSc1
<g/>
)	)	kIx)
Vladimír	Vladimír	k1gMnSc1
Jedenáctík	Jedenáctík	k1gMnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
Trinkulo	Trinkula	k1gFnSc5
<g/>
)	)	kIx)
Karel	Karel	k1gMnSc1
Hruška	Hruška	k1gMnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
Stefano	Stefana	k1gFnSc5
<g/>
)	)	kIx)
Karel	Karel	k1gMnSc1
Kalaš	Kalaš	k1gMnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
Ariel	Ariel	k1gInSc1
<g/>
)	)	kIx)
Maria	Maria	k1gFnSc1
Tauberová	Tauberová	k1gFnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
duch	duch	k1gMnSc1
<g/>
)	)	kIx)
Miloslava	Miloslava	k1gFnSc1
Fidlerová	Fidlerová	k1gFnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
duch	duch	k1gMnSc1
<g/>
)	)	kIx)
Milada	Milada	k1gFnSc1
Jirásková	Jirásková	k1gFnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
duch	duch	k1gMnSc1
<g/>
)	)	kIx)
Ludmila	Ludmila	k1gFnSc1
Hanzalíková	Hanzalíková	k1gFnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
duch	duch	k1gMnSc1
<g/>
)	)	kIx)
Jaroslava	Jaroslava	k1gFnSc1
Dobrá	dobrý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pěvecký	pěvecký	k2eAgInSc1d1
sbor	sbor	k1gInSc1
Čs	čs	kA
<g/>
.	.	kIx.
rozhlasu	rozhlas	k1gInSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
a	a	k8xC
Symfonický	symfonický	k2eAgInSc1d1
orchestr	orchestr	k1gInSc1
Pražského	pražský	k2eAgInSc2d1
rozhlasu	rozhlas	k1gInSc2
řídí	řídit	k5eAaImIp3nS
Jaroslav	Jaroslav	k1gMnSc1
Vogel	Vogel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
</s>
<s desamb="1">
Supraphon	supraphon	k1gInSc1
(	(	kIx(
<g/>
jen	jen	k9
výběr	výběr	k1gInSc1
scén	scéna	k1gFnPc2
<g/>
)	)	kIx)
Zpívají	zpívat	k5eAaImIp3nP
Teodor	Teodor	k1gMnSc1
Šrubař	Šrubař	k1gMnSc1
<g/>
,	,	kIx,
Libuše	Libuše	k1gFnSc1
Domanínská	Domanínský	k2eAgFnSc1d1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
Berman	Berman	k1gMnSc1
<g/>
,	,	kIx,
Ivo	Ivo	k1gMnSc1
Žídek	Žídek	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
Kožušník	Kožušník	k1gMnSc1
<g/>
,	,	kIx,
Hynek	Hynek	k1gMnSc1
Maxa	Maxa	k1gMnSc1
<g/>
,	,	kIx,
Helena	Helena	k1gFnSc1
Tattermuschová	Tattermuschová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sbor	sbor	k1gInSc4
a	a	k8xC
orchestr	orchestr	k1gInSc4
Národního	národní	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
řídí	řídit	k5eAaImIp3nS
Josef	Josef	k1gMnSc1
Bartl	Bartl	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
1997	#num#	k4
(	(	kIx(
<g/>
rozhlasový	rozhlasový	k2eAgInSc4d1
záznam	záznam	k1gInSc4
Deutschlandradio	Deutschlandradio	k1gNnSc4
inscenace	inscenace	k1gFnSc2
z	z	k7c2
Bielefeldu	Bielefeld	k1gInSc2
<g/>
,	,	kIx,
oficiálně	oficiálně	k6eAd1
nevydán	vydán	k2eNgMnSc1d1
<g/>
)	)	kIx)
Zpívají	zpívat	k5eAaImIp3nP
(	(	kIx(
<g/>
Alonso	Alonsa	k1gFnSc5
<g/>
)	)	kIx)
Sung-Kon	Sung-Kon	k1gMnSc1
Kim	Kim	k1gMnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
Sebastiano	Sebastiana	k1gFnSc5
<g/>
)	)	kIx)
Lassi	Lasse	k1gFnSc6
Partanen	Partanen	k2eAgMnSc1d1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
Fernando	Fernanda	k1gFnSc5
<g/>
)	)	kIx)
Luca	Luca	k1gMnSc1
Martin	Martin	k1gMnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
Antonio	Antonio	k1gMnSc1
<g/>
)	)	kIx)
Vladimir	Vladimir	k1gMnSc1
Lortkipanidze	Lortkipanidze	k1gFnSc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
(	(	kIx(
<g/>
Gonzalo	Gonzalo	k1gMnPc5
<g/>
)	)	kIx)
Paata	Paata	k1gFnSc1
Tsivtsivadze	Tsivtsivadze	k1gFnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
Prospero	Prospero	k1gNnSc1
<g/>
)	)	kIx)
Maik	Maik	k1gMnSc1
Schwalm	Schwalm	k1gMnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
Miranda	Miranda	k1gFnSc1
<g/>
)	)	kIx)
Melanie	Melanie	k1gFnSc1
Kreuter	Kreuter	k1gInSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
Kaliban	Kaliban	k1gMnSc1
<g/>
)	)	kIx)
Jacek	Jacek	k1gMnSc1
Janiszewski	Janiszewsk	k1gFnSc2
<g/>
,	,	kIx,
(	(	kIx(
<g/>
Trinkulo	Trinkula	k1gFnSc5
<g/>
)	)	kIx)
Simeon	Simeon	k1gMnSc1
Esper	Esper	k1gMnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
Stefano	Stefana	k1gFnSc5
<g/>
)	)	kIx)
Mark	Mark	k1gMnSc1
Coles	Coles	k1gMnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
Ariel	Ariel	k1gInSc1
<g/>
)	)	kIx)
Cornelie	Cornelie	k1gFnSc1
Isenbürger	Isenbürgra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chor	Chor	k1gInSc1
des	des	k1gNnSc2
Theaters	Theatersa	k1gFnPc2
Bielefeld	Bielefeld	k1gMnSc1
a	a	k8xC
Bielefelder	Bielefelder	k1gMnSc1
Philharmoniker	Philharmoniker	k1gMnSc1
řídí	řídit	k5eAaImIp3nS
Leo	Leo	k1gMnSc1
Siberski	Sibersk	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Jiří	Jiří	k1gMnSc1
Kopecký	Kopecký	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdeněk	Zdeněk	k1gMnSc1
Fibich	Fibich	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
opera	opera	k1gFnSc1
Šárka	Šárka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hudební	hudební	k2eAgInPc1d1
rozhledy	rozhled	k1gInPc1
<g/>
.	.	kIx.
2004	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
LVIII	LVIII	kA
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
11	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
56	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
18	#num#	k4
<g/>
-	-	kIx~
<g/>
6996	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
HOSTOMSKÁ	HOSTOMSKÁ	kA
<g/>
,	,	kIx,
Anna	Anna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opera	opera	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průvodce	průvodce	k1gMnSc1
operní	operní	k2eAgFnSc2d1
tvorbou	tvorba	k1gFnSc7
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Státní	státní	k2eAgNnSc1d1
hudební	hudební	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
1959	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
625	#num#	k4
<g/>
-	-	kIx~
<g/>
627	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
JIRÁNEK	Jiránek	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdeněk	Zdeněk	k1gMnSc1
Fibich	Fibich	k1gMnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Akademie	akademie	k1gFnSc1
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
hudební	hudební	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
68	#num#	k4
<g/>
,	,	kIx,
175	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
TROJAN	Trojan	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
opery	opera	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
a	a	k8xC
Litomyšl	Litomyšl	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
348	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
229	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Jiránek	Jiránek	k1gMnSc1
<g/>
,	,	kIx,
c.	c.	k?
d.	d.	k?
<g/>
,	,	kIx,
s.	s.	k?
78.1	78.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
ŠÍP	Šíp	k1gMnSc1
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
opera	opera	k1gFnSc1
a	a	k8xC
její	její	k3xOp3gMnPc1
tvůrci	tvůrce	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Supraphon	supraphon	k1gInSc1
<g/>
,	,	kIx,
n.	n.	k?
p.	p.	k?
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
.	.	kIx.
400	#num#	k4
s.	s.	k?
Kapitola	kapitola	k1gFnSc1
Zdeněk	Zdeněk	k1gMnSc1
Fibich	Fibich	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
115	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Jiránek	Jiránek	k1gMnSc1
<g/>
,	,	kIx,
c.	c.	k?
d.	d.	k?
<g/>
,	,	kIx,
s.	s.	k?
125.1	125.1	k4
2	#num#	k4
3	#num#	k4
Šíp	Šíp	k1gMnSc1
<g/>
,	,	kIx,
c.	c.	k?
d.	d.	k?
<g/>
,	,	kIx,
s.	s.	k?
117.1	117.1	k4
2	#num#	k4
Jiránek	Jiránek	k1gMnSc1
<g/>
,	,	kIx,
c.	c.	k?
d.	d.	k?
<g/>
,	,	kIx,
s.	s.	k?
124	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BARTOŠ	Bartoš	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdeněk	Zdeněk	k1gMnSc1
Fibich	Fibich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
HUTTER	HUTTER	kA
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
;	;	kIx,
CHALABALA	CHALABALA	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgInPc1d1
umění	umění	k1gNnSc2
dramatické	dramatický	k2eAgNnSc4d1
II	II	kA
-	-	kIx~
Zpěvohra	zpěvohra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Šolc	Šolc	k1gMnSc1
a	a	k8xC
Šimáček	Šimáček	k1gMnSc1
<g/>
,	,	kIx,
společnost	společnost	k1gFnSc1
s	s	k7c7
r.	r.	kA
o.	o.	k?
<g/>
,	,	kIx,
1941	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
142	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HUDEC	Hudec	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fibich	Fibich	k1gMnSc1
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
LUDVOVÁ	Ludvová	k1gFnSc1
<g/>
,	,	kIx,
Jitka	Jitka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hudební	hudební	k2eAgInPc1d1
divadlo	divadlo	k1gNnSc4
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
:	:	kIx,
Osobnosti	osobnost	k1gFnSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Divadelní	divadelní	k2eAgInSc1d1
ústav	ústav	k1gInSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7008	#num#	k4
<g/>
-	-	kIx~
<g/>
188	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
1346	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
154	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Jiránek	Jiránek	k1gMnSc1
<g/>
,	,	kIx,
c.	c.	k?
d.	d.	k?
<g/>
,	,	kIx,
s.	s.	k?
198	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Jiránek	Jiránek	k1gMnSc1
<g/>
,	,	kIx,
c.	c.	k?
d.	d.	k?
<g/>
,	,	kIx,
s.	s.	k?
126	#num#	k4
<g/>
,	,	kIx,
128.1	128.1	k4
2	#num#	k4
Jiránek	Jiránek	k1gMnSc1
<g/>
,	,	kIx,
c.	c.	k?
d.	d.	k?
<g/>
,	,	kIx,
s.	s.	k?
175.1	175.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Bartoš	Bartoš	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
c.	c.	k?
d.	d.	k?
<g/>
,	,	kIx,
s.	s.	k?
144	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Jiránek	Jiránek	k1gMnSc1
<g/>
,	,	kIx,
c.	c.	k?
d.	d.	k?
<g/>
,	,	kIx,
s.	s.	k?
128	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Šíp	šíp	k1gInSc1
<g/>
,	,	kIx,
c.	c.	k?
d.	d.	k?
<g/>
,	,	kIx,
s.	s.	k?
116	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Jiránek	Jiránek	k1gMnSc1
<g/>
,	,	kIx,
c.	c.	k?
d.	d.	k?
<g/>
,	,	kIx,
s.	s.	k?
110	#num#	k4
<g/>
,	,	kIx,
113	#num#	k4
<g/>
,	,	kIx,
126	#num#	k4
<g/>
-	-	kIx~
<g/>
127	#num#	k4
<g/>
,	,	kIx,
175	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Jiránek	Jiránek	k1gMnSc1
<g/>
,	,	kIx,
c.	c.	k?
d.	d.	k?
<g/>
,	,	kIx,
s.	s.	k?
126	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KOPECKÝ	Kopecký	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Bielefeldu	Bielefeld	k1gInSc2
vlétla	vlétnout	k5eAaPmAgFnS
Bouře	bouře	k1gFnSc1
Zdeňka	Zdeněk	k1gMnSc2
Fibicha	Fibich	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hudební	hudební	k2eAgInPc1d1
rozhledy	rozhled	k1gInPc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
60	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
6	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
40	#num#	k4
<g/>
-	-	kIx~
<g/>
41	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
18	#num#	k4
<g/>
-	-	kIx~
<g/>
6966	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Ein	Ein	k1gFnSc1
Sturm	Sturm	k1gInSc1
aus	aus	k?
Bielefeld	Bielefeld	k1gInSc1
-	-	kIx~
Wiederentdeckung	Wiederentdeckung	k1gInSc1
einer	einer	k1gMnSc1
tschechischen	tschechischna	k1gFnPc2
Oper	opera	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deutschlandradio	Deutschlandradio	k1gNnSc1
Kultur	kultura	k1gFnPc2
<g/>
,	,	kIx,
2007-04-21	2007-04-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Archivní	archivní	k2eAgInSc4d1
a	a	k8xC
programové	programový	k2eAgInPc4d1
fondy	fond	k1gInPc4
Českého	český	k2eAgInSc2d1
rozhlasu	rozhlas	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
BARTOŠ	Bartoš	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdeněk	Zdeněk	k1gMnSc1
Fibich	Fibich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
HUTTER	HUTTER	kA
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
;	;	kIx,
CHALABALA	CHALABALA	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgInPc1d1
umění	umění	k1gNnSc2
dramatické	dramatický	k2eAgNnSc4d1
II	II	kA
-	-	kIx~
Zpěvohra	zpěvohra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Šolc	Šolc	k1gMnSc1
a	a	k8xC
Šimáček	Šimáček	k1gMnSc1
<g/>
,	,	kIx,
společnost	společnost	k1gFnSc1
s	s	k7c7
r.	r.	kA
o.	o.	k?
<g/>
,	,	kIx,
1941	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
142	#num#	k4
<g/>
-	-	kIx~
<g/>
146	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ŠÍP	Šíp	k1gMnSc1
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
opera	opera	k1gFnSc1
a	a	k8xC
její	její	k3xOp3gMnPc1
tvůrci	tvůrce	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Supraphon	supraphon	k1gInSc1
<g/>
,	,	kIx,
n.	n.	k?
p.	p.	k?
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
.	.	kIx.
400	#num#	k4
s.	s.	k?
Kapitola	kapitola	k1gFnSc1
Zdeněk	Zdeněk	k1gMnSc1
Fibich	Fibich	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
115	#num#	k4
<g/>
-	-	kIx~
<g/>
117	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
JANOTA	Janota	k1gMnSc1
<g/>
,	,	kIx,
Dalibor	Dalibor	k1gMnSc1
<g/>
;	;	kIx,
KUČERA	Kučera	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
P.	P.	kA
Malá	malý	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
české	český	k2eAgFnSc2d1
opery	opera	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Litomyšl	Litomyšl	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
236	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
30	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HUDEC	Hudec	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fibich	Fibich	k1gMnSc1
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
LUDVOVÁ	Ludvová	k1gFnSc1
<g/>
,	,	kIx,
Jitka	Jitka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hudební	hudební	k2eAgInPc1d1
divadlo	divadlo	k1gNnSc4
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osobnosti	osobnost	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Divadelní	divadelní	k2eAgInSc1d1
ústav	ústav	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7008	#num#	k4
<g/>
-	-	kIx~
<g/>
188	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
1346	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
152	#num#	k4
<g/>
-	-	kIx~
<g/>
156	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HOSTOMSKÁ	HOSTOMSKÁ	kA
<g/>
,	,	kIx,
Anna	Anna	k1gFnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opera	opera	k1gFnSc1
–	–	k?
Průvodce	průvodka	k1gFnSc6
operní	operní	k2eAgFnSc7d1
tvorbou	tvorba	k1gFnSc7
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
NS	NS	kA
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
.	.	kIx.
1466	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
205	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
637	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
734	#num#	k4
<g/>
–	–	k?
<g/>
736	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KOPECKÝ	Kopecký	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fibich	Fibich	k1gMnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Ústav	ústav	k1gInSc1
hudební	hudební	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
Filozofické	filozofický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2006-03-07	2006-03-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Bouře	bouře	k1gFnSc1
v	v	k7c6
databázi	databáze	k1gFnSc6
Archivu	archiv	k1gInSc2
Národního	národní	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
</s>
<s>
Klavírní	klavírní	k2eAgInSc1d1
výtah	výtah	k1gInSc1
Bouře	bouř	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Opery	opera	k1gFnPc1
a	a	k8xC
scénické	scénický	k2eAgInPc1d1
melodramy	melodram	k1gInPc1
Zdeňka	Zdeněk	k1gMnSc2
Fibicha	Fibich	k1gMnSc2
</s>
<s>
Bukovín	Bukovín	k1gInSc1
(	(	kIx(
<g/>
1871	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Blaník	Blaník	k1gInSc1
(	(	kIx(
<g/>
1877	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Nevěsta	nevěsta	k1gFnSc1
messinská	messinský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1883	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Námluvy	námluva	k1gFnSc2
Pelopovy	Pelopův	k2eAgFnSc2d1
(	(	kIx(
<g/>
1889	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Smír	smír	k1gInSc1
Tantalův	Tantalův	k2eAgInSc1d1
(	(	kIx(
<g/>
1890	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Smrt	smrt	k1gFnSc1
Hippodamie	Hippodamie	k1gFnSc1
(	(	kIx(
<g/>
1891	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bouře	bouře	k1gFnSc1
(	(	kIx(
<g/>
1894	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Hedy	Heda	k1gFnSc2
(	(	kIx(
<g/>
1895	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Šárka	Šárka	k1gFnSc1
(	(	kIx(
<g/>
1897	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pád	Pád	k1gInSc1
Arkuna	Arkuna	k1gFnSc1
(	(	kIx(
<g/>
1899	#num#	k4
<g/>
)	)	kIx)
</s>
