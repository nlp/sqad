<p>
<s>
Čtyři	čtyři	k4xCgNnPc1	čtyři
sta	sto	k4xCgNnPc1	sto
padesát	padesát	k4xCc1	padesát
tři	tři	k4xCgFnPc4	tři
je	být	k5eAaImIp3nS	být
přirozené	přirozený	k2eAgNnSc4d1	přirozené
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Římskými	římský	k2eAgFnPc7d1	římská
číslicemi	číslice	k1gFnPc7	číslice
se	se	k3xPyFc4	se
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
CDLIII	CDLIII	kA	CDLIII
a	a	k8xC	a
řeckými	řecký	k2eAgFnPc7d1	řecká
číslicemi	číslice	k1gFnPc7	číslice
υ	υ	k?	υ
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
po	po	k7c6	po
číslu	číslo	k1gNnSc3	číslo
čtyři	čtyři	k4xCgNnPc1	čtyři
sta	sto	k4xCgNnPc1	sto
padesát	padesát	k4xCc1	padesát
dva	dva	k4xCgMnPc1	dva
a	a	k8xC	a
předchází	předcházet	k5eAaImIp3nP	předcházet
číslu	číslo	k1gNnSc3	číslo
čtyři	čtyři	k4xCgNnPc1	čtyři
sta	sto	k4xCgNnPc1	sto
padesát	padesát	k4xCc1	padesát
čtyři	čtyři	k4xCgMnPc4	čtyři
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Matematika	matematika	k1gFnSc1	matematika
==	==	k?	==
</s>
</p>
<p>
<s>
453	[number]	k4	453
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Deficientní	Deficientní	k2eAgNnSc1d1	Deficientní
číslo	číslo	k1gNnSc1	číslo
</s>
</p>
<p>
<s>
Složené	složený	k2eAgNnSc1d1	složené
číslo	číslo	k1gNnSc1	číslo
</s>
</p>
<p>
<s>
Nešťastné	šťastný	k2eNgNnSc1d1	nešťastné
číslo	číslo	k1gNnSc1	číslo
</s>
</p>
<p>
<s>
==	==	k?	==
Roky	rok	k1gInPc1	rok
==	==	k?	==
</s>
</p>
<p>
<s>
453	[number]	k4	453
</s>
</p>
<p>
<s>
453	[number]	k4	453
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
</s>
</p>
