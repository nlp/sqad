<p>
<s>
C-4	C-4	k4	C-4
(	(	kIx(	(
<g/>
Composition	Composition	k1gInSc1	Composition
4	[number]	k4	4
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
plastická	plastický	k2eAgFnSc1d1	plastická
trhavina	trhavina	k1gFnSc1	trhavina
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vylepšenou	vylepšený	k2eAgFnSc4d1	vylepšená
britskou	britský	k2eAgFnSc4d1	britská
výbušninu	výbušnina	k1gFnSc4	výbušnina
Nobel	Nobel	k1gMnSc1	Nobel
808	[number]	k4	808
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
podobné	podobný	k2eAgNnSc4d1	podobné
složení	složení	k1gNnSc4	složení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolem	kolem	k7c2	kolem
91	[number]	k4	91
%	%	kIx~	%
trhaviny	trhavina	k1gFnSc2	trhavina
C-4	C-4	k1gFnSc2	C-4
tvoří	tvořit	k5eAaImIp3nS	tvořit
výbušnina	výbušnina	k1gFnSc1	výbušnina
RDX	RDX	kA	RDX
<g/>
,	,	kIx,	,
5,3	[number]	k4	5,3
%	%	kIx~	%
tvoří	tvořit	k5eAaImIp3nS	tvořit
dioktyl	dioktyl	k1gInSc1	dioktyl
sebakát	sebakát	k1gInSc1	sebakát
<g/>
,	,	kIx,	,
2,1	[number]	k4	2,1
%	%	kIx~	%
tvoří	tvořit	k5eAaImIp3nS	tvořit
polyisobutylen	polyisobutylen	k1gInSc4	polyisobutylen
a	a	k8xC	a
1,6	[number]	k4	1,6
%	%	kIx~	%
motorový	motorový	k2eAgInSc1d1	motorový
olej	olej	k1gInSc1	olej
<g/>
.	.	kIx.	.
</s>
<s>
C-4	C-4	k4	C-4
je	být	k5eAaImIp3nS	být
známá	známá	k1gFnSc1	známá
svojí	svůj	k3xOyFgFnSc7	svůj
spolehlivostí	spolehlivost	k1gFnSc7	spolehlivost
a	a	k8xC	a
odolností	odolnost	k1gFnSc7	odolnost
<g/>
.	.	kIx.	.
</s>
<s>
Neexploduje	explodovat	k5eNaBmIp3nS	explodovat
při	při	k7c6	při
obvyklých	obvyklý	k2eAgInPc6d1	obvyklý
nárazech	náraz	k1gInPc6	náraz
<g/>
,	,	kIx,	,
střelbě	střelba	k1gFnSc6	střelba
a	a	k8xC	a
ani	ani	k8xC	ani
při	při	k7c6	při
hození	hození	k1gNnSc6	hození
do	do	k7c2	do
ohně	oheň	k1gInSc2	oheň
<g/>
,	,	kIx,	,
jedinou	jediný	k2eAgFnSc7d1	jediná
účinnou	účinný	k2eAgFnSc7d1	účinná
metodou	metoda	k1gFnSc7	metoda
iniciace	iniciace	k1gFnSc2	iniciace
detonace	detonace	k1gFnSc2	detonace
je	být	k5eAaImIp3nS	být
použití	použití	k1gNnSc1	použití
rozbušky	rozbuška	k1gFnSc2	rozbuška
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
výbuchu	výbuch	k1gInSc2	výbuch
je	být	k5eAaImIp3nS	být
8	[number]	k4	8
050	[number]	k4	050
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Je	být	k5eAaImIp3nS	být
mírně	mírně	k6eAd1	mírně
brizantnější	brizantní	k2eAgMnSc1d2	brizantní
než	než	k8xS	než
Semtex	semtex	k1gInSc1	semtex
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
oproti	oproti	k7c3	oproti
němu	on	k3xPp3gInSc3	on
o	o	k7c4	o
něco	něco	k3yInSc4	něco
méně	málo	k6eAd2	málo
plastická	plastický	k2eAgFnSc1d1	plastická
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
454	[number]	k4	454
g	g	kA	g
trhaviny	trhavina	k1gFnSc2	trhavina
C4	C4	k1gFnSc2	C4
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
smrtelnou	smrtelný	k2eAgFnSc4d1	smrtelná
tlakovou	tlakový	k2eAgFnSc4d1	tlaková
vlnu	vlna	k1gFnSc4	vlna
s	s	k7c7	s
50	[number]	k4	50
<g/>
%	%	kIx~	%
úmrtností	úmrtnost	k1gFnSc7	úmrtnost
asi	asi	k9	asi
na	na	k7c4	na
1	[number]	k4	1
m	m	kA	m
a	a	k8xC	a
s	s	k7c7	s
99	[number]	k4	99
<g/>
%	%	kIx~	%
úmrtnostní	úmrtnostní	k2eAgFnSc1d1	úmrtnostní
kolem	kolem	k7c2	kolem
70	[number]	k4	70
cm	cm	kA	cm
(	(	kIx(	(
<g/>
nálož	nálož	k1gFnSc1	nálož
-	-	kIx~	-
horní	horní	k2eAgFnSc1d1	horní
polovina	polovina	k1gFnSc1	polovina
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
orientaci	orientace	k1gFnSc4	orientace
osoby	osoba	k1gFnSc2	osoba
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
schopna	schopen	k2eAgFnSc1d1	schopna
zcela	zcela	k6eAd1	zcela
roztrhat	roztrhat	k5eAaPmF	roztrhat
menší	malý	k2eAgInPc4d2	menší
předměty	předmět	k1gInPc4	předmět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
