<s>
Slezské	slezský	k2eAgFnPc4d1	Slezská
písně	píseň	k1gFnPc4	píseň
je	být	k5eAaImIp3nS	být
název	název	k1gInSc1	název
jediné	jediný	k2eAgFnSc2d1	jediná
básnické	básnický	k2eAgFnSc2d1	básnická
sbírky	sbírka	k1gFnSc2	sbírka
českého	český	k2eAgMnSc2d1	český
básníka	básník	k1gMnSc2	básník
Petra	Petr	k1gMnSc2	Petr
Bezruče	Bezruč	k1gFnSc2	Bezruč
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Vaška	Vašek	k1gMnSc2	Vašek
<g/>
.	.	kIx.	.
</s>
