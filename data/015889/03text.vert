<s>
Natálie	Natálie	k1gFnSc1
Kotková	Kotková	k1gFnSc1
</s>
<s>
Natálie	Natálie	k1gFnSc1
Kotková	Kotková	k1gFnSc1
Narození	narození	k1gNnSc6
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1994	#num#	k4
(	(	kIx(
<g/>
27	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Povolání	povolání	k1gNnPc2
</s>
<s>
modelkablogerka	modelkablogerka	k1gFnSc1
Výška	výška	k1gFnSc1
</s>
<s>
173	#num#	k4
cm	cm	kA
Váha	váha	k1gFnSc1
</s>
<s>
47	#num#	k4
kg	kg	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Natálie	Natálie	k1gFnSc1
Kotková	Kotková	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
11	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
česká	český	k2eAgFnSc1d1
modelka	modelka	k1gFnSc1
a	a	k8xC
blogerka	blogerka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Natálie	Natálie	k1gFnSc1
Kotková	Kotková	k1gFnSc1
se	se	k3xPyFc4
narodila	narodit	k5eAaPmAgFnS
11	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
Českou	český	k2eAgFnSc7d1
miss	miss	k1gFnSc7
World	Worlda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měří	měřit	k5eAaImIp3nS
173	#num#	k4
cm	cm	kA
a	a	k8xC
váží	vážit	k5eAaImIp3nS
47	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc2
míry	míra	k1gFnSc2
jsou	být	k5eAaImIp3nP
85-60-89	85-60-89	k4
(	(	kIx(
<g/>
v	v	k7c6
centimetrech	centimetr	k1gInPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gMnSc1
přítel	přítel	k1gMnSc1
je	být	k5eAaImIp3nS
zpěvák	zpěvák	k1gMnSc1
skupiny	skupina	k1gFnSc2
Slza	slza	k1gFnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
Lexa	Lexa	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c4
její	její	k3xOp3gInPc4
koníčky	koníček	k1gInPc4
patří	patřit	k5eAaImIp3nS
jóga	jóga	k1gFnSc1
<g/>
,	,	kIx,
street	street	k1gInSc1
dance	dance	k1gFnSc1
<g/>
,	,	kIx,
cestování	cestování	k1gNnSc1
<g/>
,	,	kIx,
longboarding	longboarding	k1gInSc1
<g/>
,	,	kIx,
snowboarding	snowboarding	k1gInSc1
a	a	k8xC
surfaření	surfaření	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
je	být	k5eAaImIp3nS
společně	společně	k6eAd1
s	s	k7c7
Agátou	Agáta	k1gFnSc7
Hanychovou	Hanychová	k1gFnSc7
a	a	k8xC
Jitkou	Jitka	k1gFnSc7
Nováčkovou	Nováčková	k1gFnSc7
jednou	jednou	k6eAd1
z	z	k7c2
moderátorek	moderátorka	k1gFnPc2
na	na	k7c4
Mall	Mall	k1gInSc4
TV	TV	kA
v	v	k7c6
pořadu	pořad	k1gInSc6
Mall	Mall	k1gMnSc1
Boxing	boxing	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Natálie	Natálie	k1gFnSc1
Kotková	Kotková	k1gFnSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Natálie	Natálie	k1gFnSc1
Kotková	Kotková	k1gFnSc1
<g/>
.	.	kIx.
www.fashion-models.cz	www.fashion-models.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Natálie	Natálie	k1gFnSc1
Kotková	Kotková	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
CELEBWIKI	CELEBWIKI	kA
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
