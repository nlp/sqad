<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
dále	daleko	k6eAd2	daleko
děleny	dělit	k5eAaImNgInP	dělit
do	do	k7c2	do
101	[number]	k4	101
departementů	departement	k1gInPc2	departement
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
očíslovány	očíslován	k2eAgInPc1d1	očíslován
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
pořadí	pořadí	k1gNnSc2	pořadí
v	v	k7c6	v
abecedě	abeceda	k1gFnSc6	abeceda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
má	mít	k5eAaImIp3nS	mít
následně	následně	k6eAd1	následně
význam	význam	k1gInSc1	význam
např.	např.	kA	např.
pro	pro	k7c4	pro
směrovací	směrovací	k2eAgNnPc4d1	směrovací
čísla	číslo	k1gNnPc4	číslo
nebo	nebo	k8xC	nebo
poznávací	poznávací	k2eAgFnPc4d1	poznávací
značky	značka	k1gFnPc4	značka
aut	auto	k1gNnPc2	auto
<g/>
.	.	kIx.	.
</s>
