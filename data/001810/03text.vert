<s>
Francie	Francie	k1gFnSc1	Francie
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
La	la	k1gNnSc1	la
France	Franc	k1gMnSc2	Franc
<g/>
,	,	kIx,	,
výslovnost	výslovnost	k1gFnSc1	výslovnost
/	/	kIx~	/
<g/>
fʀ	fʀ	k?	fʀ
<g/>
̃	̃	k?	̃
<g/>
s	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
République	République	k1gFnSc1	République
française	française	k1gFnSc1	française
<g/>
,	,	kIx,	,
výslovnost	výslovnost	k1gFnSc1	výslovnost
/	/	kIx~	/
<g/>
ʀ	ʀ	k?	ʀ
fʀ	fʀ	k?	fʀ
<g/>
̃	̃	k?	̃
<g/>
sɛ	sɛ	k?	sɛ
<g/>
/	/	kIx~	/
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
demokratický	demokratický	k2eAgInSc1d1	demokratický
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
většina	většina	k1gFnSc1	většina
území	území	k1gNnSc2	území
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
označovaná	označovaný	k2eAgFnSc1d1	označovaná
jako	jako	k8xC	jako
Metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Francouzskou	francouzský	k2eAgFnSc4d1	francouzská
republiku	republika	k1gFnSc4	republika
tvoří	tvořit	k5eAaImIp3nP	tvořit
i	i	k9	i
tzv.	tzv.	kA	tzv.
Zámořská	zámořský	k2eAgFnSc1d1	zámořská
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
území	území	k1gNnSc4	území
v	v	k7c6	v
Karibiku	Karibik	k1gInSc6	Karibik
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
v	v	k7c6	v
Indickém	indický	k2eAgInSc6d1	indický
oceánu	oceán	k1gInSc6	oceán
a	a	k8xC	a
Oceánii	Oceánie	k1gFnSc6	Oceánie
<g/>
.	.	kIx.	.
</s>
<s>
Francouzi	Francouz	k1gMnPc1	Francouz
metropolitní	metropolitní	k2eAgFnSc4d1	metropolitní
Francii	Francie	k1gFnSc4	Francie
kvůli	kvůli	k7c3	kvůli
jejímu	její	k3xOp3gInSc3	její
geografickému	geografický	k2eAgInSc3d1	geografický
tvaru	tvar	k1gInSc3	tvar
často	často	k6eAd1	často
přezdívají	přezdívat	k5eAaImIp3nP	přezdívat
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Hexagone	hexagon	k1gInSc5	hexagon
(	(	kIx(	(
<g/>
šestiúhelník	šestiúhelník	k1gInSc4	šestiúhelník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Francii	Francie	k1gFnSc3	Francie
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
malý	malý	k2eAgInSc1d1	malý
ostrov	ostrov	k1gInSc1	ostrov
Korsika	Korsika	k1gFnSc1	Korsika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členskou	členský	k2eAgFnSc7d1	členská
zemí	zem	k1gFnSc7	zem
Severoatlantické	severoatlantický	k2eAgFnSc2d1	Severoatlantická
aliance	aliance	k1gFnSc2	aliance
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
zakládajících	zakládající	k2eAgMnPc2d1	zakládající
členů	člen	k1gMnPc2	člen
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
pěti	pět	k4xCc2	pět
stálých	stálý	k2eAgMnPc2d1	stálý
členů	člen	k1gMnPc2	člen
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
s	s	k7c7	s
právem	právo	k1gNnSc7	právo
veta	veto	k1gNnSc2	veto
<g/>
,	,	kIx,	,
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
osmi	osm	k4xCc2	osm
uznaných	uznaný	k2eAgFnPc2d1	uznaná
jaderných	jaderný	k2eAgFnPc2d1	jaderná
mocností	mocnost	k1gFnPc2	mocnost
a	a	k8xC	a
členem	člen	k1gInSc7	člen
sdružení	sdružení	k1gNnSc1	sdružení
osmi	osm	k4xCc2	osm
nejvyspělejších	vyspělý	k2eAgInPc2d3	nejvyspělejší
států	stát	k1gInPc2	stát
světa	svět	k1gInSc2	svět
G	G	kA	G
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Spojeným	spojený	k2eAgNnSc7d1	spojené
královstvím	království	k1gNnSc7	království
je	být	k5eAaImIp3nS	být
Francie	Francie	k1gFnSc1	Francie
jedinou	jediný	k2eAgFnSc7d1	jediná
doposud	doposud	k6eAd1	doposud
existující	existující	k2eAgFnSc7d1	existující
koloniální	koloniální	k2eAgFnSc7d1	koloniální
velmocí	velmoc	k1gFnSc7	velmoc
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc4	dějiny
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
současné	současný	k2eAgFnSc2d1	současná
Francie	Francie	k1gFnSc2	Francie
zhruba	zhruba	k6eAd1	zhruba
souhlasí	souhlasit	k5eAaImIp3nS	souhlasit
s	s	k7c7	s
těmi	ten	k3xDgMnPc7	ten
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
starověké	starověký	k2eAgFnSc2d1	starověká
Galie	Galie	k1gFnSc2	Galie
obývané	obývaný	k2eAgNnSc1d1	obývané
keltským	keltský	k2eAgInSc7d1	keltský
kmenem	kmen	k1gInSc7	kmen
Galů	Gal	k1gMnPc2	Gal
<g/>
.	.	kIx.	.
</s>
<s>
Galie	Galie	k1gFnSc1	Galie
byla	být	k5eAaImAgFnS	být
podrobena	podrobit	k5eAaPmNgFnS	podrobit
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Římany	Říman	k1gMnPc7	Říman
a	a	k8xC	a
Galové	Gal	k1gMnPc1	Gal
částečně	částečně	k6eAd1	částečně
převzali	převzít	k5eAaPmAgMnP	převzít
latinský	latinský	k2eAgInSc4d1	latinský
jazyk	jazyk	k1gInSc4	jazyk
i	i	k8xC	i
kulturu	kultura	k1gFnSc4	kultura
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
romanizací	romanizace	k1gFnSc7	romanizace
území	území	k1gNnSc2	území
souvisí	souviset	k5eAaImIp3nS	souviset
i	i	k9	i
časné	časný	k2eAgNnSc1d1	časné
šíření	šíření	k1gNnSc1	šíření
křesťanství	křesťanství	k1gNnSc2	křesťanství
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Galské	galský	k2eAgFnPc1d1	galská
východní	východní	k2eAgFnPc1d1	východní
hranice	hranice	k1gFnPc1	hranice
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
obsazeny	obsazen	k2eAgInPc4d1	obsazen
germánskými	germánský	k2eAgInPc7d1	germánský
kmeny	kmen	k1gInPc7	kmen
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
Franky	Frank	k1gMnPc4	Frank
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
později	pozdě	k6eAd2	pozdě
dali	dát	k5eAaPmAgMnP	dát
zemi	zem	k1gFnSc6	zem
její	její	k3xOp3gNnSc4	její
dnešní	dnešní	k2eAgNnSc4d1	dnešní
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Existence	existence	k1gFnSc1	existence
Francie	Francie	k1gFnSc2	Francie
jako	jako	k8xS	jako
samostatného	samostatný	k2eAgInSc2d1	samostatný
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
datuje	datovat	k5eAaImIp3nS	datovat
od	od	k7c2	od
rozděleni	rozdělit	k5eAaPmNgMnP	rozdělit
Francké	francký	k2eAgFnSc2d1	Francká
říše	říš	k1gFnSc2	říš
roku	rok	k1gInSc2	rok
843	[number]	k4	843
na	na	k7c4	na
(	(	kIx(	(
<g/>
Francia	francium	k1gNnPc4	francium
orientalis	orientalis	k1gFnSc2	orientalis
<g/>
)	)	kIx)	)
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
Francia	francium	k1gNnSc2	francium
occidentalis	occidentalis	k1gFnSc2	occidentalis
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
Lotharingii	Lotharingie	k1gFnSc6	Lotharingie
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Východní	východní	k2eAgFnSc2d1	východní
Francké	francký	k2eAgFnSc2d1	Francká
Říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Svatá	svatý	k2eAgFnSc1d1	svatá
říše	říše	k1gFnSc1	říše
římská	římský	k2eAgFnSc1d1	římská
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
dala	dát	k5eAaPmAgFnS	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
dnešní	dnešní	k2eAgFnSc4d1	dnešní
Francii	Francie	k1gFnSc4	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Potomci	potomek	k1gMnPc1	potomek
Karla	Karel	k1gMnSc2	Karel
Velikého	veliký	k2eAgInSc2d1	veliký
vládli	vládnout	k5eAaImAgMnP	vládnout
Francii	Francie	k1gFnSc3	Francie
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
987	[number]	k4	987
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
korunován	korunovat	k5eAaBmNgMnS	korunovat
králem	král	k1gMnSc7	král
Hugo	Hugo	k1gMnSc1	Hugo
Kapet	Kapet	k1gMnSc1	Kapet
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
potomci	potomek	k1gMnPc1	potomek
<g/>
,	,	kIx,	,
Kapetovci	Kapetovec	k1gMnPc1	Kapetovec
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
vedlejší	vedlejší	k2eAgFnPc1d1	vedlejší
větve	větev	k1gFnPc1	větev
z	z	k7c2	z
Valois	Valois	k1gFnPc2	Valois
a	a	k8xC	a
Bourboni	Bourbon	k1gMnPc1	Bourbon
postupně	postupně	k6eAd1	postupně
pomocí	pomocí	k7c2	pomocí
série	série	k1gFnSc2	série
válek	válka	k1gFnPc2	válka
a	a	k8xC	a
dědictví	dědictví	k1gNnSc2	dědictví
sjednotili	sjednotit	k5eAaPmAgMnP	sjednotit
území	území	k1gNnSc4	území
pod	pod	k7c4	pod
centrální	centrální	k2eAgFnSc4d1	centrální
panovnickou	panovnický	k2eAgFnSc4d1	panovnická
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
angličtí	anglický	k2eAgMnPc1d1	anglický
králové	král	k1gMnPc1	král
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Plantagenetů	Plantagenet	k1gInPc2	Plantagenet
ovládali	ovládat	k5eAaImAgMnP	ovládat
rozsáhlá	rozsáhlý	k2eAgNnPc4d1	rozsáhlé
území	území	k1gNnPc4	území
západní	západní	k2eAgFnSc2d1	západní
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
k	k	k7c3	k
vyhnání	vyhnání	k1gNnSc3	vyhnání
Angličanů	Angličan	k1gMnPc2	Angličan
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
došlo	dojít	k5eAaPmAgNnS	dojít
teprve	teprve	k6eAd1	teprve
na	na	k7c6	na
konci	konec	k1gInSc6	konec
stoleté	stoletý	k2eAgFnSc2d1	stoletá
války	válka	k1gFnSc2	válka
mezi	mezi	k7c7	mezi
Anglií	Anglie	k1gFnSc7	Anglie
a	a	k8xC	a
Francií	Francie	k1gFnSc7	Francie
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Francii	Francie	k1gFnSc6	Francie
zasáhly	zasáhnout	k5eAaPmAgFnP	zasáhnout
náboženské	náboženský	k2eAgFnPc1d1	náboženská
války	válka	k1gFnPc1	válka
mezi	mezi	k7c7	mezi
hugenoty	hugenot	k1gMnPc7	hugenot
a	a	k8xC	a
katolíky	katolík	k1gMnPc7	katolík
<g/>
.	.	kIx.	.
</s>
<s>
Monarchie	monarchie	k1gFnSc1	monarchie
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
své	svůj	k3xOyFgFnPc4	svůj
největší	veliký	k2eAgFnPc4d3	veliký
moci	moc	k1gFnPc4	moc
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vládl	vládnout	k5eAaImAgInS	vládnout
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
začátkem	začátkem	k7c2	začátkem
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
měla	mít	k5eAaImAgFnS	mít
Francie	Francie	k1gFnSc1	Francie
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
evropskou	evropský	k2eAgFnSc4d1	Evropská
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
ekonomii	ekonomie	k1gFnSc4	ekonomie
a	a	k8xC	a
kulturu	kultura	k1gFnSc4	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
nejlidnatějším	lidnatý	k2eAgInSc7d3	nejlidnatější
státem	stát	k1gInSc7	stát
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
třetím	třetí	k4xOgMnSc6	třetí
na	na	k7c6	na
světě	svět	k1gInSc6	svět
po	po	k7c6	po
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
o	o	k7c4	o
španělské	španělský	k2eAgNnSc4d1	španělské
dědictví	dědictví	k1gNnSc4	dědictví
Francie	Francie	k1gFnSc2	Francie
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
dosazení	dosazení	k1gNnSc4	dosazení
Bourbonů	bourbon	k1gInPc2	bourbon
na	na	k7c4	na
španělský	španělský	k2eAgInSc4d1	španělský
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
však	však	k9	však
postupně	postupně	k6eAd1	postupně
propadala	propadat	k5eAaPmAgFnS	propadat
do	do	k7c2	do
rekordní	rekordní	k2eAgFnSc2d1	rekordní
zadluženosti	zadluženost	k1gFnSc2	zadluženost
a	a	k8xC	a
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
úpadku	úpadek	k1gInSc2	úpadek
a	a	k8xC	a
v	v	k7c6	v
sedmileté	sedmiletý	k2eAgFnSc6d1	sedmiletá
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
porážky	porážka	k1gFnSc2	porážka
od	od	k7c2	od
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
přišla	přijít	k5eAaPmAgFnS	přijít
o	o	k7c4	o
svou	svůj	k3xOyFgFnSc4	svůj
koloniální	koloniální	k2eAgFnSc4d1	koloniální
říši	říše	k1gFnSc4	říše
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
a	a	k8xC	a
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Úpadek	úpadek	k1gInSc1	úpadek
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
za	za	k7c2	za
následníků	následník	k1gMnPc2	následník
Ludvíka	Ludvík	k1gMnSc4	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
stále	stále	k6eAd1	stále
prohluboval	prohlubovat	k5eAaImAgInS	prohlubovat
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
příčin	příčina	k1gFnPc2	příčina
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
propukla	propuknout	k5eAaPmAgFnS	propuknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1789	[number]	k4	1789
a	a	k8xC	a
vedla	vést	k5eAaImAgFnS	vést
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1791	[number]	k4	1791
k	k	k7c3	k
nahrazení	nahrazení	k1gNnSc3	nahrazení
absolutistické	absolutistický	k2eAgFnSc2d1	absolutistická
monarchie	monarchie	k1gFnSc2	monarchie
konstituční	konstituční	k2eAgFnSc1d1	konstituční
<g/>
,	,	kIx,	,
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1792	[number]	k4	1792
byla	být	k5eAaImAgFnS	být
následně	následně	k6eAd1	následně
nastolena	nastolen	k2eAgFnSc1d1	nastolena
První	první	k4xOgFnSc1	první
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
však	však	k9	však
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
zůstala	zůstat	k5eAaPmAgFnS	zůstat
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
revolučního	revoluční	k2eAgInSc2d1	revoluční
kabinetu	kabinet	k1gInSc2	kabinet
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1795	[number]	k4	1795
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
ústava	ústava	k1gFnSc1	ústava
roku	rok	k1gInSc2	rok
III	III	kA	III
<g/>
,	,	kIx,	,
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
chopilo	chopit	k5eAaPmAgNnS	chopit
direktorium	direktorium	k1gNnSc1	direktorium
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1799	[number]	k4	1799
ovládl	ovládnout	k5eAaPmAgMnS	ovládnout
republiku	republika	k1gFnSc4	republika
Napoleon	Napoleon	k1gMnSc1	Napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
prvním	první	k4xOgMnSc7	první
konzulem	konzul	k1gMnSc7	konzul
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1804	[number]	k4	1804
státním	státní	k2eAgInSc7d1	státní
převratem	převrat	k1gInSc7	převrat
založil	založit	k5eAaPmAgMnS	založit
první	první	k4xOgNnSc4	první
francouzské	francouzský	k2eAgNnSc4d1	francouzské
císařství	císařství	k1gNnSc4	císařství
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
panovníkem	panovník	k1gMnSc7	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	napoleon	k1gInSc1	napoleon
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
většinu	většina	k1gFnSc4	většina
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
bojoval	bojovat	k5eAaImAgMnS	bojovat
se	s	k7c7	s
Spojeným	spojený	k2eAgNnSc7d1	spojené
Královstvím	království	k1gNnSc7	království
<g/>
,	,	kIx,	,
Pruskem	Prusko	k1gNnSc7	Prusko
<g/>
,	,	kIx,	,
Rakouskem	Rakousko	k1gNnSc7	Rakousko
a	a	k8xC	a
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
,	,	kIx,	,
založil	založit	k5eAaPmAgInS	založit
nová	nový	k2eAgNnPc4d1	nové
království	království	k1gNnPc4	království
<g/>
,	,	kIx,	,
do	do	k7c2	do
jejichž	jejichž	k3xOyRp3gNnSc2	jejichž
čela	čelo	k1gNnSc2	čelo
dosadil	dosadit	k5eAaPmAgInS	dosadit
členy	člen	k1gMnPc4	člen
své	svůj	k3xOyFgFnSc2	svůj
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
neúspěšné	úspěšný	k2eNgFnSc6d1	neúspěšná
invazi	invaze	k1gFnSc6	invaze
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
porážce	porážka	k1gFnSc3	porážka
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Lipska	Lipsko	k1gNnSc2	Lipsko
už	už	k6eAd1	už
nebyl	být	k5eNaImAgInS	být
schopen	schopen	k2eAgInSc1d1	schopen
čelit	čelit	k5eAaImF	čelit
aliančním	alianční	k2eAgFnPc3d1	alianční
armádám	armáda	k1gFnPc3	armáda
a	a	k8xC	a
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1814	[number]	k4	1814
abdikoval	abdikovat	k5eAaBmAgMnS	abdikovat
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
restauraci	restaurace	k1gFnSc3	restaurace
bourbonského	bourbonský	k2eAgNnSc2d1	Bourbonské
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Vídeňského	vídeňský	k2eAgInSc2d1	vídeňský
kongresu	kongres	k1gInSc2	kongres
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
z	z	k7c2	z
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Elba	Elb	k1gInSc2	Elb
a	a	k8xC	a
nakrátko	nakrátko	k6eAd1	nakrátko
obnovil	obnovit	k5eAaPmAgInS	obnovit
císařství	císařství	k1gNnSc4	císařství
<g/>
.	.	kIx.	.
</s>
<s>
Definitivně	definitivně	k6eAd1	definitivně
poražen	poražen	k2eAgMnSc1d1	poražen
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Waterloo	Waterloo	k1gNnSc2	Waterloo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1830	[number]	k4	1830
dala	dát	k5eAaPmAgFnS	dát
Červencová	červencový	k2eAgFnSc1d1	červencová
revoluce	revoluce	k1gFnSc1	revoluce
vzniknout	vzniknout	k5eAaPmF	vzniknout
konstituční	konstituční	k2eAgFnSc4d1	konstituční
monarchii	monarchie	k1gFnSc4	monarchie
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
vedlejší	vedlejší	k2eAgFnSc7d1	vedlejší
větví	větev	k1gFnSc7	větev
Bourbonů	bourbon	k1gInPc2	bourbon
<g/>
,	,	kIx,	,
nahrazené	nahrazený	k2eAgFnSc2d1	nahrazená
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
Druhou	druhý	k4xOgFnSc7	druhý
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
existence	existence	k1gFnSc1	existence
byla	být	k5eAaImAgFnS	být
zanedlouho	zanedlouho	k6eAd1	zanedlouho
ukončena	ukončit	k5eAaPmNgFnS	ukončit
zvolením	zvolení	k1gNnSc7	zvolení
dosavadního	dosavadní	k2eAgMnSc2d1	dosavadní
prezidenta	prezident	k1gMnSc2	prezident
a	a	k8xC	a
císařova	císařův	k2eAgMnSc2d1	císařův
synovce	synovec	k1gMnSc2	synovec
Ludvíka	Ludvík	k1gMnSc4	Ludvík
Napoleona	Napoleon	k1gMnSc4	Napoleon
císařem	císař	k1gMnSc7	císař
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
Napoleona	Napoleon	k1gMnSc4	Napoleon
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
zajat	zajmout	k5eAaPmNgInS	zajmout
po	po	k7c6	po
prohrané	prohraný	k2eAgFnSc6d1	prohraná
prusko-francouzské	pruskorancouzský	k2eAgFnSc6d1	prusko-francouzská
válce	válka	k1gFnSc6	válka
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Třetí	třetí	k4xOgFnSc1	třetí
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
vyšla	vyjít	k5eAaPmAgFnS	vyjít
Francie	Francie	k1gFnSc1	Francie
vítězně	vítězně	k6eAd1	vítězně
z	z	k7c2	z
první	první	k4xOgFnSc2	první
i	i	k8xC	i
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
ztrácela	ztrácet	k5eAaImAgFnS	ztrácet
během	během	k7c2	během
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
postupně	postupně	k6eAd1	postupně
bývalé	bývalý	k2eAgNnSc4d1	bývalé
velmocenské	velmocenský	k2eAgNnSc4d1	velmocenské
postavení	postavení	k1gNnSc4	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
byla	být	k5eAaImAgFnS	být
nastolena	nastolen	k2eAgFnSc1d1	nastolena
současná	současný	k2eAgFnSc1d1	současná
Pátá	pátá	k1gFnSc1	pátá
republika	republika	k1gFnSc1	republika
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
generálem	generál	k1gMnSc7	generál
Charlesem	Charles	k1gMnSc7	Charles
de	de	k?	de
Gaullem	Gaull	k1gMnSc7	Gaull
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
válek	válka	k1gFnPc2	válka
v	v	k7c6	v
Indočíně	Indočína	k1gFnSc6	Indočína
a	a	k8xC	a
Alžírsku	Alžírsko	k1gNnSc6	Alžírsko
a	a	k8xC	a
vlně	vlna	k1gFnSc6	vlna
dekolonizace	dekolonizace	k1gFnSc2	dekolonizace
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
přišla	přijít	k5eAaPmAgFnS	přijít
Francie	Francie	k1gFnSc1	Francie
o	o	k7c4	o
většinu	většina	k1gFnSc4	většina
svých	svůj	k3xOyFgFnPc2	svůj
kolonií	kolonie	k1gFnPc2	kolonie
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
se	se	k3xPyFc4	se
Francie	Francie	k1gFnSc1	Francie
smířila	smířit	k5eAaPmAgFnS	smířit
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
politickou	politický	k2eAgFnSc4d1	politická
a	a	k8xC	a
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
integraci	integrace	k1gFnSc4	integrace
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
států	stát	k1gInPc2	stát
podporujících	podporující	k2eAgInPc2d1	podporující
urychlení	urychlení	k1gNnSc4	urychlení
rozšíření	rozšíření	k1gNnSc4	rozšíření
měnové	měnový	k2eAgFnSc2d1	měnová
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
chtěla	chtít	k5eAaImAgFnS	chtít
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
vytvoření	vytvoření	k1gNnSc4	vytvoření
více	hodně	k6eAd2	hodně
sjednocené	sjednocený	k2eAgFnSc2d1	sjednocená
a	a	k8xC	a
schopné	schopný	k2eAgFnSc2d1	schopná
Evropské	evropský	k2eAgFnSc2d1	Evropská
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
obrany	obrana	k1gFnSc2	obrana
a	a	k8xC	a
bezpečnostního	bezpečnostní	k2eAgInSc2d1	bezpečnostní
aparátu	aparát	k1gInSc2	aparát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
o	o	k7c6	o
přijetí	přijetí	k1gNnSc6	přijetí
Smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c6	o
Evropské	evropský	k2eAgFnSc6d1	Evropská
ústavě	ústava	k1gFnSc6	ústava
však	však	k9	však
55	[number]	k4	55
%	%	kIx~	%
francouzských	francouzský	k2eAgMnPc2d1	francouzský
občanů	občan	k1gMnPc2	občan
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
proti	proti	k7c3	proti
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
Francii	Francie	k1gFnSc4	Francie
obvykle	obvykle	k6eAd1	obvykle
vnímáme	vnímat	k5eAaImIp1nP	vnímat
jako	jako	k9	jako
jednolitý	jednolitý	k2eAgInSc4d1	jednolitý
národní	národní	k2eAgInSc4d1	národní
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
na	na	k7c4	na
rané	raný	k2eAgFnPc4d1	raná
dějiny	dějiny	k1gFnPc4	dějiny
Francie	Francie	k1gFnSc2	Francie
je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
,	,	kIx,	,
že	že	k8xS	že
-	-	kIx~	-
slovy	slovo	k1gNnPc7	slovo
André	André	k1gMnPc3	André
Mauroise	Mauroise	k1gFnSc1	Mauroise
-	-	kIx~	-
"	"	kIx"	"
<g/>
jakási	jakýsi	k3yIgFnSc1	jakýsi
francouzská	francouzský	k2eAgFnSc1d1	francouzská
rasa	rasa	k1gFnSc1	rasa
nikdy	nikdy	k6eAd1	nikdy
neexistovala	existovat	k5eNaImAgFnS	existovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Francouzi	Francouz	k1gMnPc1	Francouz
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
značně	značně	k6eAd1	značně
různorodým	různorodý	k2eAgInSc7d1	různorodý
národnostním	národnostní	k2eAgInSc7d1	národnostní
původem	původ	k1gInSc7	původ
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jejich	jejich	k3xOp3gFnPc1	jejich
dějiny	dějiny	k1gFnPc1	dějiny
výsledkem	výsledek	k1gInSc7	výsledek
mnoha	mnoho	k4c2	mnoho
vlivů	vliv	k1gInPc2	vliv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
svých	svůj	k3xOyFgFnPc2	svůj
dějin	dějiny	k1gFnPc2	dějiny
zažila	zažít	k5eAaPmAgFnS	zažít
Francie	Francie	k1gFnSc1	Francie
množství	množství	k1gNnSc2	množství
přistěhovaleckých	přistěhovalecký	k2eAgFnPc2d1	přistěhovalecká
vln	vlna	k1gFnPc2	vlna
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
příznačné	příznačný	k2eAgNnSc1d1	příznačné
i	i	k9	i
pro	pro	k7c4	pro
současnou	současný	k2eAgFnSc4d1	současná
Francii	Francie	k1gFnSc4	Francie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
je	být	k5eAaImIp3nS	být
zřejmější	zřejmý	k2eAgFnSc1d2	zřejmější
při	při	k7c6	při
uvědomění	uvědomění	k1gNnSc6	uvědomění
si	se	k3xPyFc3	se
geografické	geografický	k2eAgFnPc4d1	geografická
polohy	poloha	k1gFnPc4	poloha
Francie	Francie	k1gFnSc2	Francie
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
okraji	okraj	k1gInSc6	okraj
evropského	evropský	k2eAgInSc2d1	evropský
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
vlivem	vliv	k1gInSc7	vliv
byla	být	k5eAaImAgFnS	být
Francie	Francie	k1gFnSc1	Francie
místem	místem	k6eAd1	místem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zastavovaly	zastavovat	k5eAaImAgFnP	zastavovat
invaze	invaze	k1gFnSc1	invaze
a	a	k8xC	a
usazovali	usazovat	k5eAaImAgMnP	usazovat
se	se	k3xPyFc4	se
vetřelci	vetřelec	k1gMnPc1	vetřelec
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
přestože	přestože	k8xS	přestože
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Francie	Francie	k1gFnSc1	Francie
má	mít	k5eAaImIp3nS	mít
přirozené	přirozený	k2eAgFnPc4d1	přirozená
hranice	hranice	k1gFnPc4	hranice
<g/>
,	,	kIx,	,
z	z	k7c2	z
historického	historický	k2eAgNnSc2d1	historické
hlediska	hledisko	k1gNnSc2	hledisko
nebyla	být	k5eNaImAgFnS	být
nijak	nijak	k6eAd1	nijak
uzavřeným	uzavřený	k2eAgInSc7d1	uzavřený
celkem	celek	k1gInSc7	celek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
území	území	k1gNnSc6	území
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
dějin	dějiny	k1gFnPc2	dějiny
setkáváme	setkávat	k5eAaImIp1nP	setkávat
s	s	k7c7	s
nejrozličnějšími	rozličný	k2eAgInPc7d3	rozličný
národy	národ	k1gInPc7	národ
–	–	k?	–
Kelty	Kelt	k1gMnPc7	Kelt
<g/>
,	,	kIx,	,
Řeky	Řek	k1gMnPc7	Řek
<g/>
,	,	kIx,	,
Římany	Říman	k1gMnPc7	Říman
<g/>
,	,	kIx,	,
Germány	Germán	k1gMnPc7	Germán
<g/>
,	,	kIx,	,
Franky	Frank	k1gMnPc7	Frank
<g/>
,	,	kIx,	,
Normany	Norman	k1gMnPc7	Norman
<g/>
,	,	kIx,	,
Židy	Žid	k1gMnPc7	Žid
<g/>
,	,	kIx,	,
Španěly	Španěl	k1gMnPc7	Španěl
<g/>
,	,	kIx,	,
Portugalci	Portugalec	k1gMnPc7	Portugalec
<g/>
,	,	kIx,	,
Italy	Ital	k1gMnPc7	Ital
<g/>
,	,	kIx,	,
Alžířany	Alžířan	k1gMnPc7	Alžířan
a	a	k8xC	a
mnoha	mnoho	k4c7	mnoho
dalšími	další	k2eAgFnPc7d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
stopy	stopa	k1gFnPc1	stopa
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
Francie	Francie	k1gFnSc2	Francie
zanechaly	zanechat	k5eAaPmAgInP	zanechat
vlivy	vliv	k1gInPc1	vliv
Galů	Gal	k1gMnPc2	Gal
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
ta	ten	k3xDgFnSc1	ten
část	část	k1gFnSc1	část
Keltů	Kelt	k1gMnPc2	Kelt
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
usadila	usadit	k5eAaPmAgFnS	usadit
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Římanů	Říman	k1gMnPc2	Říman
a	a	k8xC	a
Franků	Frank	k1gMnPc2	Frank
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
první	první	k4xOgInPc4	první
dva	dva	k4xCgInPc4	dva
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
splynuly	splynout	k5eAaPmAgFnP	splynout
dohromady	dohromady	k6eAd1	dohromady
a	a	k8xC	a
třetí	třetí	k4xOgMnSc1	třetí
dal	dát	k5eAaPmAgMnS	dát
Francii	Francie	k1gFnSc4	Francie
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
imigrantům	imigrant	k1gMnPc3	imigrant
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
území	území	k1gNnSc6	území
Francie	Francie	k1gFnSc2	Francie
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
asimilační	asimilační	k2eAgFnPc1d1	asimilační
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kultura	kultura	k1gFnSc1	kultura
<g/>
,	,	kIx,	,
jazyk	jazyk	k1gInSc1	jazyk
a	a	k8xC	a
genetické	genetický	k2eAgFnSc2d1	genetická
dispozice	dispozice	k1gFnSc2	dispozice
nově	nově	k6eAd1	nově
příchozích	příchozí	k1gMnPc2	příchozí
se	se	k3xPyFc4	se
rozpouštěly	rozpouštět	k5eAaImAgFnP	rozpouštět
mezi	mezi	k7c4	mezi
zde	zde	k6eAd1	zde
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
usazeným	usazený	k2eAgNnSc7d1	usazené
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
je	být	k5eAaImIp3nS	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
i	i	k9	i
obohacovaly	obohacovat	k5eAaImAgFnP	obohacovat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
civilizačního	civilizační	k2eAgInSc2d1	civilizační
byl	být	k5eAaImAgInS	být
nejzásadnější	zásadní	k2eAgInSc4d3	nejzásadnější
vliv	vliv	k1gInSc4	vliv
galorománské	galorománský	k2eAgFnSc2d1	galorománská
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
i	i	k9	i
na	na	k7c6	na
dnešní	dnešní	k2eAgFnSc6d1	dnešní
podobě	podoba	k1gFnSc6	podoba
francouzštiny	francouzština	k1gFnSc2	francouzština
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
řadíme	řadit	k5eAaImIp1nP	řadit
mezi	mezi	k7c4	mezi
románské	románský	k2eAgInPc4d1	románský
jazyky	jazyk	k1gInPc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
mnoha	mnoho	k4c2	mnoho
oblastních	oblastní	k2eAgMnPc2d1	oblastní
jazyků	jazyk	k1gMnPc2	jazyk
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
většinou	většinou	k6eAd1	většinou
nemají	mít	k5eNaImIp3nP	mít
s	s	k7c7	s
latinským	latinský	k2eAgInSc7d1	latinský
původem	původ	k1gInSc7	původ
příliš	příliš	k6eAd1	příliš
společného	společný	k2eAgInSc2d1	společný
(	(	kIx(	(
<g/>
bretonština	bretonština	k1gFnSc1	bretonština
<g/>
,	,	kIx,	,
baskičtina	baskičtina	k1gFnSc1	baskičtina
<g/>
,	,	kIx,	,
korsičtina	korsičtina	k1gFnSc1	korsičtina
<g/>
,	,	kIx,	,
vlámština	vlámština	k1gFnSc1	vlámština
nebo	nebo	k8xC	nebo
alsaské	alsaský	k2eAgNnSc1d1	alsaské
nářečí	nářečí	k1gNnSc1	nářečí
němčiny	němčina	k1gFnSc2	němčina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
však	však	k9	však
velmi	velmi	k6eAd1	velmi
názorně	názorně	k6eAd1	názorně
dokládá	dokládat	k5eAaImIp3nS	dokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
utváření	utváření	k1gNnSc1	utváření
francouzského	francouzský	k2eAgInSc2d1	francouzský
národa	národ	k1gInSc2	národ
bylo	být	k5eAaImAgNnS	být
mnohem	mnohem	k6eAd1	mnohem
složitější	složitý	k2eAgMnSc1d2	složitější
a	a	k8xC	a
nejednoznačnější	jednoznačný	k2eNgMnSc1d2	nejednoznačnější
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
každodenním	každodenní	k2eAgInSc7d1	každodenní
jazykem	jazyk	k1gInSc7	jazyk
většiny	většina	k1gFnSc2	většina
venkovského	venkovský	k2eAgNnSc2d1	venkovské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
třetině	třetina	k1gFnSc6	třetina
Francie	Francie	k1gFnSc2	Francie
románská	románský	k2eAgFnSc1d1	románská
okcitánština	okcitánština	k1gFnSc1	okcitánština
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
blízko	blízko	k6eAd1	blízko
ke	k	k7c3	k
katalánštině	katalánština	k1gFnSc3	katalánština
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Ačkoliv	ačkoliv	k8xS	ačkoliv
staletí	staletí	k1gNnSc1	staletí
trvající	trvající	k2eAgInPc1d1	trvající
centralizační	centralizační	k2eAgInPc1d1	centralizační
tlaky	tlak	k1gInPc1	tlak
stmelily	stmelit	k5eAaPmAgInP	stmelit
Francouze	Francouz	k1gMnSc2	Francouz
do	do	k7c2	do
jediného	jediný	k2eAgInSc2d1	jediný
národa	národ	k1gInSc2	národ
se	s	k7c7	s
silným	silný	k2eAgInSc7d1	silný
citem	cit	k1gInSc7	cit
pro	pro	k7c4	pro
národní	národní	k2eAgFnSc4d1	národní
identitu	identita	k1gFnSc4	identita
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
národ	národ	k1gInSc1	národ
utvářen	utvářit	k5eAaPmNgInS	utvářit
množstvím	množství	k1gNnSc7	množství
rozdílných	rozdílný	k2eAgNnPc2d1	rozdílné
etnik	etnikum	k1gNnPc2	etnikum
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
právě	právě	k9	právě
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
historické	historický	k2eAgFnSc2d1	historická
rozmanitosti	rozmanitost	k1gFnSc2	rozmanitost
pochází	pocházet	k5eAaImIp3nS	pocházet
kulturní	kulturní	k2eAgNnSc4d1	kulturní
a	a	k8xC	a
jazykové	jazykový	k2eAgNnSc4d1	jazykové
bohatství	bohatství	k1gNnSc4	bohatství
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
část	část	k1gFnSc1	část
území	území	k1gNnSc2	území
Francie	Francie	k1gFnSc2	Francie
(	(	kIx(	(
<g/>
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
Francie	Francie	k1gFnSc2	Francie
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hraniční	hraniční	k2eAgFnPc4d1	hraniční
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
s	s	k7c7	s
Belgií	Belgie	k1gFnSc7	Belgie
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
hranic	hranice	k1gFnPc2	hranice
620	[number]	k4	620
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
Lucemburskem	Lucembursko	k1gNnSc7	Lucembursko
(	(	kIx(	(
<g/>
73	[number]	k4	73
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
(	(	kIx(	(
<g/>
450	[number]	k4	450
km	km	kA	km
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
a	a	k8xC	a
Švýcarskem	Švýcarsko	k1gNnSc7	Švýcarsko
(	(	kIx(	(
<g/>
572	[number]	k4	572
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
s	s	k7c7	s
Itálií	Itálie	k1gFnSc7	Itálie
(	(	kIx(	(
<g/>
515	[number]	k4	515
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
se	s	k7c7	s
Španělskem	Španělsko	k1gNnSc7	Španělsko
(	(	kIx(	(
<g/>
649	[number]	k4	649
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Andorrou	Andorra	k1gFnSc7	Andorra
(	(	kIx(	(
<g/>
56,6	[number]	k4	56,6
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
Monakem	Monako	k1gNnSc7	Monako
(	(	kIx(	(
<g/>
4,5	[number]	k4	4,5
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
republika	republika	k1gFnSc1	republika
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
i	i	k9	i
tzv.	tzv.	kA	tzv.
zámořskou	zámořský	k2eAgFnSc7d1	zámořská
Francií	Francie	k1gFnSc7	Francie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
území	území	k1gNnSc2	území
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
Guyana	Guyana	k1gFnSc1	Guyana
673	[number]	k4	673
km	km	kA	km
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Brazílií	Brazílie	k1gFnSc7	Brazílie
a	a	k8xC	a
520	[number]	k4	520
km	km	kA	km
se	s	k7c7	s
Surinamem	Surinam	k1gInSc7	Surinam
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Indickém	indický	k2eAgInSc6d1	indický
a	a	k8xC	a
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
<g/>
,	,	kIx,	,
Karibiku	Karibika	k1gFnSc4	Karibika
(	(	kIx(	(
<g/>
ostrov	ostrov	k1gInSc4	ostrov
Svatý	svatý	k2eAgMnSc1d1	svatý
Martin	Martin	k1gMnSc1	Martin
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
a	a	k8xC	a
nizozemskou	nizozemský	k2eAgFnSc4d1	nizozemská
část	část	k1gFnSc4	část
hranicí	hranice	k1gFnSc7	hranice
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
10,2	[number]	k4	10,2
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
Antarktidě	Antarktida	k1gFnSc6	Antarktida
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
suverenita	suverenita	k1gFnSc1	suverenita
deklarovaná	deklarovaný	k2eAgFnSc1d1	deklarovaná
v	v	k7c6	v
Antarktidě	Antarktida	k1gFnSc6	Antarktida
nebyla	být	k5eNaImAgFnS	být
uznána	uznat	k5eAaPmNgFnS	uznat
většinou	většina	k1gFnSc7	většina
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
–	–	k?	–
viz	vidět	k5eAaImRp2nS	vidět
Antarktický	antarktický	k2eAgInSc4d1	antarktický
smluvní	smluvní	k2eAgInSc4d1	smluvní
systém	systém	k1gInSc4	systém
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
část	část	k1gFnSc1	část
Francie	Francie	k1gFnSc2	Francie
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
plochu	plocha	k1gFnSc4	plocha
543	[number]	k4	543
965	[number]	k4	965
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
západě	západ	k1gInSc6	západ
je	být	k5eAaImIp3nS	být
krajina	krajina	k1gFnSc1	krajina
rovinatá	rovinatý	k2eAgFnSc1d1	rovinatá
s	s	k7c7	s
mírným	mírný	k2eAgNnSc7d1	mírné
vlněním	vlnění	k1gNnSc7	vlnění
<g/>
,	,	kIx,	,
na	na	k7c6	na
zbytku	zbytek	k1gInSc6	zbytek
území	území	k1gNnSc2	území
převážně	převážně	k6eAd1	převážně
pahorkatá	pahorkatý	k2eAgFnSc1d1	pahorkatá
a	a	k8xC	a
hornatá	hornatý	k2eAgFnSc1d1	hornatá
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
francouzských	francouzský	k2eAgFnPc6d1	francouzská
Alpách	Alpy	k1gFnPc6	Alpy
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
Mont	Mont	k1gMnSc1	Mont
Blanc	Blanc	k1gMnSc1	Blanc
(	(	kIx(	(
<g/>
4	[number]	k4	4
810	[number]	k4	810
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
hornaté	hornatý	k2eAgInPc1d1	hornatý
kraje	kraj	k1gInPc1	kraj
země	zem	k1gFnSc2	zem
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
Pyreneje	Pyreneje	k1gFnPc1	Pyreneje
<g/>
,	,	kIx,	,
Centrální	centrální	k2eAgInSc1d1	centrální
masív	masív	k1gInSc1	masív
<g/>
,	,	kIx,	,
Jura	jura	k1gFnSc1	jura
<g/>
,	,	kIx,	,
Vogézy	Vogézy	k1gFnPc1	Vogézy
<g/>
,	,	kIx,	,
Armorský	Armorský	k2eAgInSc1d1	Armorský
masív	masív	k1gInSc1	masív
a	a	k8xC	a
Ardeny	Ardeny	k1gFnPc1	Ardeny
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgFnPc7d3	veliký
francouzskými	francouzský	k2eAgFnPc7d1	francouzská
řekami	řeka	k1gFnPc7	řeka
jsou	být	k5eAaImIp3nP	být
Loira	Loira	k1gFnSc1	Loira
<g/>
,	,	kIx,	,
Rhône	Rhôn	k1gMnSc5	Rhôn
(	(	kIx(	(
<g/>
pramenící	pramenící	k2eAgFnSc7d1	pramenící
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Garonna	Garonna	k1gFnSc1	Garonna
(	(	kIx(	(
<g/>
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Seina	Seina	k1gFnSc1	Seina
a	a	k8xC	a
část	část	k1gFnSc1	část
toku	tok	k1gInSc2	tok
Rýnu	rýna	k1gFnSc4	rýna
<g/>
.	.	kIx.	.
</s>
<s>
Loira	Loira	k1gFnSc1	Loira
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
také	také	k9	také
nejdelší	dlouhý	k2eAgFnSc7d3	nejdelší
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
řekou	řeka	k1gFnSc7	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
měst	město	k1gNnPc2	město
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
aglomeraci	aglomerace	k1gFnSc6	aglomerace
žije	žít	k5eAaImIp3nS	žít
přes	přes	k7c4	přes
12	[number]	k4	12
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
největšími	veliký	k2eAgNnPc7d3	veliký
městy	město	k1gNnPc7	město
Marseille	Marseille	k1gFnSc2	Marseille
<g/>
,	,	kIx,	,
Lyon	Lyon	k1gInSc1	Lyon
<g/>
,	,	kIx,	,
Toulouse	Toulouse	k1gInSc1	Toulouse
<g/>
,	,	kIx,	,
Nice	Nice	k1gFnSc1	Nice
<g/>
,	,	kIx,	,
Nantes	Nantes	k1gInSc1	Nantes
<g/>
,	,	kIx,	,
Štrasburk	Štrasburk	k1gInSc1	Štrasburk
<g/>
,	,	kIx,	,
Montpellier	Montpellier	k1gInSc1	Montpellier
<g/>
,	,	kIx,	,
Bordeaux	Bordeaux	k1gNnSc1	Bordeaux
<g/>
,	,	kIx,	,
Lille	Lille	k1gNnSc1	Lille
<g/>
,	,	kIx,	,
Rennes	Rennes	k1gInSc1	Rennes
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
Páté	pátá	k1gFnSc2	pátá
republiky	republika	k1gFnSc2	republika
byla	být	k5eAaImAgFnS	být
schválena	schválen	k2eAgFnSc1d1	schválena
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
</s>
<s>
Výrazně	výrazně	k6eAd1	výrazně
posílila	posílit	k5eAaPmAgFnS	posílit
autoritu	autorita	k1gFnSc4	autorita
výkonného	výkonný	k2eAgInSc2d1	výkonný
výboru	výbor	k1gInSc2	výbor
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
parlamentu	parlament	k1gInSc3	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
je	být	k5eAaImIp3nS	být
zastupitelská	zastupitelský	k2eAgFnSc1d1	zastupitelská
demokracie	demokracie	k1gFnSc1	demokracie
a	a	k8xC	a
poloprezidentská	poloprezidentský	k2eAgFnSc1d1	poloprezidentská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ústavy	ústava	k1gFnSc2	ústava
je	být	k5eAaImIp3nS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
prezident	prezident	k1gMnSc1	prezident
volen	volen	k2eAgMnSc1d1	volen
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
období	období	k1gNnSc6	období
pěti	pět	k4xCc2	pět
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
sedm	sedm	k4xCc1	sedm
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
rada	rada	k1gFnSc1	rada
zabezpečuje	zabezpečovat	k5eAaImIp3nS	zabezpečovat
regulérní	regulérní	k2eAgNnSc4d1	regulérní
fungování	fungování	k1gNnSc4	fungování
moci	moc	k1gFnSc2	moc
lidu	lid	k1gInSc2	lid
a	a	k8xC	a
kontinuitu	kontinuita	k1gFnSc4	kontinuita
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
předsedu	předseda	k1gMnSc4	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
předsedá	předsedat	k5eAaImIp3nS	předsedat
kabinetu	kabinet	k1gInSc2	kabinet
<g/>
,	,	kIx,	,
velí	velet	k5eAaImIp3nS	velet
ozbrojeným	ozbrojený	k2eAgFnPc3d1	ozbrojená
silám	síla	k1gFnPc3	síla
a	a	k8xC	a
uzavírá	uzavírat	k5eAaPmIp3nS	uzavírat
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
smlouvy	smlouva	k1gFnPc4	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
(	(	kIx(	(
<g/>
Assemblée	Assembléus	k1gMnSc5	Assembléus
Nationale	Nationale	k1gMnSc5	Nationale
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dolní	dolní	k2eAgFnSc7d1	dolní
komorou	komora	k1gFnSc7	komora
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
členové	člen	k1gMnPc1	člen
jsou	být	k5eAaImIp3nP	být
voleni	volit	k5eAaImNgMnP	volit
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
pětileté	pětiletý	k2eAgNnSc4d1	pětileté
období	období	k1gNnSc4	období
a	a	k8xC	a
všechna	všechen	k3xTgNnPc4	všechen
křesla	křeslo	k1gNnPc4	křeslo
jsou	být	k5eAaImIp3nP	být
volena	volit	k5eAaImNgFnS	volit
v	v	k7c6	v
jediných	jediný	k2eAgFnPc6d1	jediná
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
rozpustit	rozpustit	k5eAaPmF	rozpustit
vládu	vláda	k1gFnSc4	vláda
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
tedy	tedy	k9	tedy
většinová	většinový	k2eAgFnSc1d1	většinová
volba	volba	k1gFnSc1	volba
Shromáždění	shromáždění	k1gNnSc2	shromáždění
určuje	určovat	k5eAaImIp3nS	určovat
rozhodování	rozhodování	k1gNnSc4	rozhodování
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Senátoři	senátor	k1gMnPc1	senátor
jsou	být	k5eAaImIp3nP	být
vybíráni	vybírat	k5eAaImNgMnP	vybírat
volební	volební	k2eAgFnSc7d1	volební
akademií	akademie	k1gFnSc7	akademie
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
devíti	devět	k4xCc2	devět
let	léto	k1gNnPc2	léto
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
třetina	třetina	k1gFnSc1	třetina
Senátu	senát	k1gInSc2	senát
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
každé	každý	k3xTgInPc4	každý
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
(	(	kIx(	(
<g/>
počínaje	počínaje	k7c7	počínaje
rokem	rok	k1gInSc7	rok
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Legislativa	legislativa	k1gFnSc1	legislativa
Senátu	senát	k1gInSc2	senát
je	být	k5eAaImIp3nS	být
omezená	omezený	k2eAgFnSc1d1	omezená
<g/>
;	;	kIx,	;
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
komory	komora	k1gFnPc1	komora
neshodují	shodovat	k5eNaImIp3nP	shodovat
v	v	k7c6	v
nějakém	nějaký	k3yIgInSc6	nějaký
bodě	bod	k1gInSc6	bod
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
ústavních	ústavní	k2eAgNnPc2d1	ústavní
práv	právo	k1gNnPc2	právo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
má	mít	k5eAaImIp3nS	mít
poslední	poslední	k2eAgNnSc4d1	poslední
slovo	slovo	k1gNnSc4	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
má	mít	k5eAaImIp3nS	mít
silný	silný	k2eAgInSc4d1	silný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
utváření	utváření	k1gNnSc4	utváření
programu	program	k1gInSc2	program
Parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
je	být	k5eAaImIp3nS	být
zakládajícím	zakládající	k2eAgMnSc7d1	zakládající
členem	člen	k1gMnSc7	člen
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
velikou	veliký	k2eAgFnSc7d1	veliká
měrou	míra	k1gFnSc7wR	míra
definuje	definovat	k5eAaBmIp3nS	definovat
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
politiku	politika	k1gFnSc4	politika
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2005	[number]	k4	2005
Francie	Francie	k1gFnSc1	Francie
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
v	v	k7c6	v
referendu	referendum	k1gNnSc3	referendum
ratifikování	ratifikování	k1gNnSc2	ratifikování
Evropské	evropský	k2eAgFnSc2d1	Evropská
ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
když	když	k8xS	když
přibližně	přibližně	k6eAd1	přibližně
55	[number]	k4	55
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
vyslovilo	vyslovit	k5eAaPmAgNnS	vyslovit
své	své	k1gNnSc4	své
"	"	kIx"	"
<g/>
ne	ne	k9	ne
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
SPC	SPC	kA	SPC
(	(	kIx(	(
<g/>
Sekretariát	sekretariát	k1gInSc1	sekretariát
Pacifické	pacifický	k2eAgFnSc2d1	Pacifická
komunity	komunita	k1gFnSc2	komunita
<g/>
,	,	kIx,	,
Secretariat	Secretariat	k1gInSc1	Secretariat
of	of	k?	of
the	the	k?	the
Pacific	Pacific	k1gMnSc1	Pacific
Community	Communita	k1gFnSc2	Communita
<g/>
)	)	kIx)	)
a	a	k8xC	a
COI	COI	kA	COI
(	(	kIx(	(
<g/>
Komise	komise	k1gFnSc1	komise
Indického	indický	k2eAgInSc2d1	indický
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
Indian	Indiana	k1gFnPc2	Indiana
Ocean	Oceana	k1gFnPc2	Oceana
Commission	Commission	k1gInSc1	Commission
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
je	být	k5eAaImIp3nS	být
vedoucím	vedoucí	k2eAgMnSc7d1	vedoucí
členem	člen	k1gMnSc7	člen
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
organizace	organizace	k1gFnSc2	organizace
frankofonie	frankofonie	k1gFnSc2	frankofonie
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
Francophonie	Francophonie	k1gFnSc1	Francophonie
<g/>
,	,	kIx,	,
International	International	k1gFnSc1	International
Organisation	Organisation	k1gInSc1	Organisation
of	of	k?	of
Francophonie	Francophonie	k1gFnSc2	Francophonie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
51	[number]	k4	51
francouzsky	francouzsky	k6eAd1	francouzsky
mluvících	mluvící	k2eAgInPc2d1	mluvící
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
sídlí	sídlet	k5eAaImIp3nS	sídlet
Organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
spolupráci	spolupráce	k1gFnSc4	spolupráce
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
,	,	kIx,	,
UNESCO	UNESCO	kA	UNESCO
<g/>
,	,	kIx,	,
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
míry	míra	k1gFnPc4	míra
a	a	k8xC	a
váhy	váha	k1gFnPc4	váha
a	a	k8xC	a
Interpol	interpol	k1gInSc4	interpol
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Regiony	region	k1gInPc4	region
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Francouzské	francouzský	k2eAgInPc1d1	francouzský
departementy	departement	k1gInPc1	departement
a	a	k8xC	a
Zámořská	zámořský	k2eAgFnSc1d1	zámořská
Francie	Francie	k1gFnSc1	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
18	[number]	k4	18
regionů	region	k1gInPc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
dále	daleko	k6eAd2	daleko
děleny	dělit	k5eAaImNgInP	dělit
do	do	k7c2	do
101	[number]	k4	101
departementů	departement	k1gInPc2	departement
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
očíslovány	očíslován	k2eAgInPc1d1	očíslován
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
pořadí	pořadí	k1gNnSc2	pořadí
v	v	k7c6	v
abecedě	abeceda	k1gFnSc6	abeceda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
následně	následně	k6eAd1	následně
význam	význam	k1gInSc1	význam
např.	např.	kA	např.
pro	pro	k7c4	pro
směrovací	směrovací	k2eAgNnPc4d1	směrovací
čísla	číslo	k1gNnPc4	číslo
nebo	nebo	k8xC	nebo
poznávací	poznávací	k2eAgFnPc4d1	poznávací
značky	značka	k1gFnPc4	značka
aut	auto	k1gNnPc2	auto
<g/>
.	.	kIx.	.
</s>
<s>
13	[number]	k4	13
regionů	region	k1gInPc2	region
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
části	část	k1gFnSc6	část
Francie	Francie	k1gFnSc2	Francie
(	(	kIx(	(
<g/>
France	Franc	k1gMnSc4	Franc
métropolitaine	métropolitainout	k5eAaPmIp3nS	métropolitainout
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
jeden	jeden	k4xCgInSc1	jeden
tvoří	tvořit	k5eAaImIp3nS	tvořit
Korsika	Korsika	k1gFnSc1	Korsika
se	s	k7c7	s
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
statutem	statut	k1gInSc7	statut
(	(	kIx(	(
<g/>
collectivité	collectivitý	k2eAgNnSc1d1	collectivitý
territoriale	territoriale	k6eAd1	territoriale
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
zbylých	zbylý	k2eAgNnPc2d1	zbylé
pět	pět	k4xCc4	pět
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
vždy	vždy	k6eAd1	vždy
jen	jen	k9	jen
jedním	jeden	k4xCgInSc7	jeden
departmentem	department	k1gInSc7	department
a	a	k8xC	a
bývají	bývat	k5eAaImIp3nP	bývat
tak	tak	k6eAd1	tak
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k8xS	jako
zámořské	zámořský	k2eAgInPc1d1	zámořský
departementy	departement	k1gInPc1	departement
a	a	k8xC	a
zámořské	zámořský	k2eAgInPc1d1	zámořský
regiony	region	k1gInPc1	region
(	(	kIx(	(
<g/>
Départements	Départements	k1gInSc1	Départements
et	et	k?	et
régions	régions	k1gInSc1	régions
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
outre-mer	outreer	k1gMnSc1	outre-mer
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
DOM-ROM	DOM-ROM	k1gFnSc1	DOM-ROM
<g/>
,	,	kIx,	,
jmenovitě	jmenovitě	k6eAd1	jmenovitě
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
Réunion	Réunion	k1gInSc4	Réunion
<g/>
,	,	kIx,	,
Mayotte	Mayott	k1gMnSc5	Mayott
<g/>
,	,	kIx,	,
Francouzskou	francouzský	k2eAgFnSc4d1	francouzská
Guyanu	Guyana	k1gFnSc4	Guyana
<g/>
,	,	kIx,	,
Martinik	Martinik	k1gInSc4	Martinik
a	a	k8xC	a
Guadeloupe	Guadeloupe	k1gFnSc4	Guadeloupe
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Departmenty	department	k1gInPc1	department
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
na	na	k7c4	na
342	[number]	k4	342
arrondisementů	arrondisement	k1gInPc2	arrondisement
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yQgInPc2	který
se	se	k3xPyFc4	se
nevolí	volit	k5eNaImIp3nS	volit
žádné	žádný	k3yNgNnSc1	žádný
zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
a	a	k8xC	a
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
smysl	smysl	k1gInSc4	smysl
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
zařazení	zařazení	k1gNnSc4	zařazení
a	a	k8xC	a
administraci	administrace	k1gFnSc4	administrace
<g/>
.	.	kIx.	.
</s>
<s>
Arrondisementy	Arrondisement	k1gInPc1	Arrondisement
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nP	členit
na	na	k7c4	na
2054	[number]	k4	2054
kantonů	kanton	k1gInPc2	kanton
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ty	ten	k3xDgMnPc4	ten
opět	opět	k6eAd1	opět
slouží	sloužit	k5eAaImIp3nP	sloužit
jen	jen	k9	jen
k	k	k7c3	k
administraci	administrace	k1gFnSc3	administrace
a	a	k8xC	a
rozdělení	rozdělení	k1gNnSc4	rozdělení
voličů	volič	k1gMnPc2	volič
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc4d1	poslední
článek	článek	k1gInSc4	článek
tvoří	tvořit	k5eAaImIp3nP	tvořit
obce	obec	k1gFnPc1	obec
<g/>
,	,	kIx,	,
kterých	který	k3yRgFnPc2	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
36	[number]	k4	36
700	[number]	k4	700
<g/>
.	.	kIx.	.
</s>
<s>
Obce	obec	k1gFnPc1	obec
jsou	být	k5eAaImIp3nP	být
samosprávné	samosprávný	k2eAgInPc1d1	samosprávný
s	s	k7c7	s
voleným	volený	k2eAgNnSc7d1	volené
zastupitelstvem	zastupitelstvo	k1gNnSc7	zastupitelstvo
(	(	kIx(	(
<g/>
obecní	obecní	k2eAgFnSc1d1	obecní
rada	rada	k1gFnSc1	rada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
měly	mít	k5eAaImAgInP	mít
arrondisementy	arrondisement	k1gInPc4	arrondisement
i	i	k8xC	i
kantony	kanton	k1gInPc4	kanton
určité	určitý	k2eAgFnSc2d1	určitá
pravomoci	pravomoc	k1gFnSc2	pravomoc
a	a	k8xC	a
vlastní	vlastní	k2eAgNnPc4d1	vlastní
zastupitelstva	zastupitelstvo	k1gNnPc4	zastupitelstvo
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
městských	městský	k2eAgInPc2d1	městský
obvodů	obvod	k1gInPc2	obvod
tento	tento	k3xDgInSc4	tento
stav	stav	k1gInSc4	stav
skončil	skončit	k5eAaPmAgMnS	skončit
rokem	rok	k1gInSc7	rok
1940	[number]	k4	1940
díky	díky	k7c3	díky
vládě	vláda	k1gFnSc3	vláda
ve	v	k7c4	v
Vichy	Vicha	k1gFnPc4	Vicha
a	a	k8xC	a
definitivně	definitivně	k6eAd1	definitivně
odstraněn	odstranit	k5eAaPmNgInS	odstranit
založením	založení	k1gNnSc7	založení
Čtvrté	čtvrtá	k1gFnSc2	čtvrtá
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
kantonů	kanton	k1gInPc2	kanton
tomu	ten	k3xDgNnSc3	ten
tak	tak	k9	tak
bylo	být	k5eAaImAgNnS	být
mnohem	mnohem	k6eAd1	mnohem
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
18	[number]	k4	18
regionů	region	k1gInPc2	region
a	a	k8xC	a
101	[number]	k4	101
departmentů	department	k1gInPc2	department
pod	pod	k7c4	pod
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
suverenitu	suverenita	k1gFnSc4	suverenita
spadají	spadat	k5eAaPmIp3nP	spadat
Nová	nový	k2eAgFnSc1d1	nová
Kaledonie	Kaledonie	k1gFnSc1	Kaledonie
<g/>
,	,	kIx,	,
Wallis	Wallis	k1gFnSc1	Wallis
a	a	k8xC	a
Futuna	Futuna	k1gFnSc1	Futuna
<g/>
,	,	kIx,	,
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
Polynésie	Polynésie	k1gFnSc1	Polynésie
<g/>
,	,	kIx,	,
Clippertonův	Clippertonův	k2eAgInSc1d1	Clippertonův
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
Saint-Pierre	Saint-Pierr	k1gInSc5	Saint-Pierr
a	a	k8xC	a
Miquelon	Miquelon	k1gInSc1	Miquelon
<g/>
,	,	kIx,	,
Svatý	svatý	k2eAgMnSc1d1	svatý
Bartoloměj	Bartoloměj	k1gMnSc1	Bartoloměj
<g/>
,	,	kIx,	,
Svatý	svatý	k2eAgMnSc1d1	svatý
Martin	Martin	k1gMnSc1	Martin
a	a	k8xC	a
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
jižní	jižní	k2eAgFnSc1d1	jižní
a	a	k8xC	a
antarktická	antarktický	k2eAgNnPc1d1	antarktické
území	území	k1gNnPc1	území
<g/>
.	.	kIx.	.
</s>
<s>
Teritoria	teritorium	k1gNnPc1	teritorium
v	v	k7c6	v
Pacifiku	Pacifik	k1gInSc6	Pacifik
nadále	nadále	k6eAd1	nadále
používají	používat	k5eAaImIp3nP	používat
frank	frank	k1gInSc4	frank
jako	jako	k8xC	jako
svou	svůj	k3xOyFgFnSc4	svůj
měnu	měna	k1gFnSc4	měna
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
eurem	euro	k1gNnSc7	euro
<g/>
.	.	kIx.	.
</s>
<s>
Polynéský	polynéský	k2eAgInSc1d1	polynéský
frank	frank	k1gInSc1	frank
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
kurs	kurs	k1gInSc1	kurs
vázán	vázán	k2eAgInSc1d1	vázán
na	na	k7c4	na
euro	euro	k1gNnSc4	euro
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Armáda	armáda	k1gFnSc1	armáda
Francie	Francie	k1gFnSc1	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Francouzské	francouzský	k2eAgFnPc1d1	francouzská
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
Pozemní	pozemní	k2eAgNnSc1d1	pozemní
vojsko	vojsko	k1gNnSc1	vojsko
(	(	kIx(	(
<g/>
Armée	Armée	k1gFnSc1	Armée
de	de	k?	de
Terre	Terr	k1gInSc5	Terr
<g/>
)	)	kIx)	)
Chasseurs	Chasseursa	k1gFnPc2	Chasseursa
Alpins	Alpins	k1gInSc1	Alpins
-	-	kIx~	-
horská	horský	k2eAgFnSc1d1	horská
divize	divize	k1gFnSc1	divize
armády	armáda	k1gFnSc2	armáda
Cizinecká	cizinecký	k2eAgFnSc1d1	cizinecká
legie	legie	k1gFnSc1	legie
Námořní	námořní	k2eAgFnSc1d1	námořní
pěchota	pěchota	k1gFnSc1	pěchota
Lehké	Lehká	k1gFnSc2	Lehká
letectvo	letectvo	k1gNnSc1	letectvo
(	(	kIx(	(
<g/>
ALAT	ALAT	kA	ALAT
-	-	kIx~	-
Aviation	Aviation	k1gInSc1	Aviation
Légére	Légér	k1gMnSc5	Légér
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Armée	Armé	k1gMnSc2	Armé
de	de	k?	de
Terre	Terr	k1gInSc5	Terr
<g/>
)	)	kIx)	)
Technikové	technik	k1gMnPc1	technik
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Génie	génius	k1gMnPc4	génius
<g/>
)	)	kIx)	)
-	-	kIx~	-
včetně	včetně	k7c2	včetně
Pařížského	pařížský	k2eAgInSc2d1	pařížský
požárního	požární	k2eAgInSc2d1	požární
sboru	sbor	k1gInSc2	sbor
Námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
(	(	kIx(	(
<g/>
Marine	Marin	k1gMnSc5	Marin
Nationale	Nationale	k1gMnSc5	Nationale
<g/>
)	)	kIx)	)
Námořní	námořní	k2eAgNnSc1d1	námořní
letectvo	letectvo	k1gNnSc1	letectvo
Fusiliers	Fusiliersa	k1gFnPc2	Fusiliersa
de	de	k?	de
Marine	Marin	k1gInSc5	Marin
-	-	kIx~	-
ochrana	ochrana	k1gFnSc1	ochrana
námořních	námořní	k2eAgInPc2d1	námořní
objektů	objekt	k1gInPc2	objekt
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
Commandos	Commandos	k1gMnSc1	Commandos
Marines	Marines	k1gMnSc1	Marines
-	-	kIx~	-
speciální	speciální	k2eAgFnSc1d1	speciální
jednotka	jednotka	k1gFnSc1	jednotka
francouzské	francouzský	k2eAgFnSc2d1	francouzská
armády	armáda	k1gFnSc2	armáda
-	-	kIx~	-
včetně	včetně	k7c2	včetně
požárního	požární	k2eAgInSc2d1	požární
praporu	prapor	k1gInSc2	prapor
města	město	k1gNnSc2	město
Marseille	Marseille	k1gFnSc2	Marseille
Letectvo	letectvo	k1gNnSc1	letectvo
(	(	kIx(	(
<g/>
Armée	Armée	k1gFnSc1	Armée
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Air	Air	k1gFnSc1	Air
<g/>
)	)	kIx)	)
Teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
letecká	letecký	k2eAgFnSc1d1	letecká
obrana	obrana	k1gFnSc1	obrana
Letectvo	letectvo	k1gNnSc1	letectvo
Četníci	četník	k1gMnPc5	četník
(	(	kIx(	(
<g/>
Gendarmerie	Gendarmerie	k1gFnSc1	Gendarmerie
Nationale	Nationale	k1gMnSc1	Nationale
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
armádní	armádní	k2eAgFnPc1d1	armádní
policie	policie	k1gFnPc1	policie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
fungují	fungovat	k5eAaImIp3nP	fungovat
jako	jako	k9	jako
částečně	částečně	k6eAd1	částečně
jako	jako	k8xC	jako
policie	policie	k1gFnSc1	policie
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
a	a	k8xC	a
jako	jako	k8xS	jako
policie	policie	k1gFnSc1	policie
taková	takový	k3xDgFnSc1	takový
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vstoupit	vstoupit	k5eAaPmF	vstoupit
v	v	k7c6	v
17	[number]	k4	17
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Alžírské	alžírský	k2eAgFnSc2d1	alžírská
války	válka	k1gFnSc2	válka
o	o	k7c4	o
nezávislost	nezávislost	k1gFnSc4	nezávislost
byla	být	k5eAaImAgFnS	být
branná	branný	k2eAgFnSc1d1	Branná
povinnost	povinnost	k1gFnSc1	povinnost
omezena	omezit	k5eAaPmNgFnS	omezit
a	a	k8xC	a
úplně	úplně	k6eAd1	úplně
zrušena	zrušen	k2eAgFnSc1d1	zrušena
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
Jacquesem	Jacques	k1gMnSc7	Jacques
Chiracem	Chirace	k1gMnSc7	Chirace
Francii	Francie	k1gFnSc6	Francie
se	se	k3xPyFc4	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
armádních	armádní	k2eAgInPc2d1	armádní
výdajů	výdaj	k1gInPc2	výdaj
vyrovná	vyrovnat	k5eAaBmIp3nS	vyrovnat
jen	jen	k9	jen
Německo	Německo	k1gNnSc1	Německo
a	a	k8xC	a
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
ročně	ročně	k6eAd1	ročně
utratí	utratit	k5eAaPmIp3nS	utratit
na	na	k7c4	na
armádu	armáda	k1gFnSc4	armáda
bezmála	bezmála	k6eAd1	bezmála
50	[number]	k4	50
mld	mld	k?	mld
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
2,6	[number]	k4	2,6
%	%	kIx~	%
HDP	HDP	kA	HDP
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
statistik	statistika	k1gFnPc2	statistika
NATO	nato	k6eAd1	nato
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
absolutních	absolutní	k2eAgNnPc6d1	absolutní
číslech	číslo	k1gNnPc6	číslo
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
za	za	k7c4	za
USA	USA	kA	USA
a	a	k8xC	a
Čínou	Čína	k1gFnSc7	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Spojeným	spojený	k2eAgNnSc7d1	spojené
královstvím	království	k1gNnSc7	království
platí	platit	k5eAaImIp3nS	platit
40	[number]	k4	40
%	%	kIx~	%
z	z	k7c2	z
celkových	celkový	k2eAgInPc2d1	celkový
výdajů	výdaj	k1gInPc2	výdaj
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
celé	celá	k1gFnSc2	celá
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
pěti	pět	k4xCc2	pět
států	stát	k1gInPc2	stát
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vlastní	vlastnit	k5eAaImIp3nP	vlastnit
jak	jak	k6eAd1	jak
jaderné	jaderný	k2eAgFnPc1d1	jaderná
a	a	k8xC	a
termojaderné	termojaderný	k2eAgFnPc1d1	termojaderná
zbraně	zbraň	k1gFnPc1	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Francie	Francie	k1gFnSc1	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Francie	Francie	k1gFnSc2	Francie
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
soukromé	soukromý	k2eAgFnSc2d1	soukromá
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
2,5	[number]	k4	2,5
milionu	milion	k4xCgInSc2	milion
registrovaných	registrovaný	k2eAgMnPc2d1	registrovaný
<g/>
)	)	kIx)	)
se	s	k7c7	s
společnostmi	společnost	k1gFnPc7	společnost
se	s	k7c7	s
značnou	značný	k2eAgFnSc7d1	značná
měrou	míra	k1gFnSc7wR	míra
intervence	intervence	k1gFnSc2	intervence
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
si	se	k3xPyFc3	se
ponechala	ponechat	k5eAaPmAgFnS	ponechat
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
klíčové	klíčový	k2eAgInPc4d1	klíčový
segmenty	segment	k1gInPc4	segment
sektorů	sektor	k1gInPc2	sektor
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
v	v	k7c6	v
otázkách	otázka	k1gFnPc6	otázka
železnice	železnice	k1gFnSc2	železnice
<g/>
,	,	kIx,	,
energetiky	energetika	k1gFnSc2	energetika
<g/>
,	,	kIx,	,
letectví	letectví	k1gNnSc2	letectví
a	a	k8xC	a
telekomunikace	telekomunikace	k1gFnSc2	telekomunikace
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
byl	být	k5eAaImAgInS	být
výhodný	výhodný	k2eAgInSc1d1	výhodný
asi	asi	k9	asi
do	do	k7c2	do
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
vláda	vláda	k1gFnSc1	vláda
pomalu	pomalu	k6eAd1	pomalu
odprodává	odprodávat	k5eAaImIp3nS	odprodávat
své	svůj	k3xOyFgInPc4	svůj
podíly	podíl	k1gInPc4	podíl
ve	v	k7c6	v
společnostech	společnost	k1gFnPc6	společnost
France	Franc	k1gMnSc2	Franc
Télécom	Télécom	k1gInSc4	Télécom
<g/>
,	,	kIx,	,
Air	Air	k1gMnSc4	Air
France	Franc	k1gMnSc4	Franc
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
jako	jako	k9	jako
pojišťovny	pojišťovna	k1gFnPc1	pojišťovna
<g/>
,	,	kIx,	,
banky	banka	k1gFnPc1	banka
a	a	k8xC	a
zbrojní	zbrojní	k2eAgInSc1d1	zbrojní
průmysl	průmysl	k1gInSc1	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
skupiny	skupina	k1gFnSc2	skupina
G	G	kA	G
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
ekonomika	ekonomika	k1gFnSc1	ekonomika
je	být	k5eAaImIp3nS	být
5	[number]	k4	5
<g/>
.	.	kIx.	.
největší	veliký	k2eAgFnSc7d3	veliký
ekonomikou	ekonomika	k1gFnSc7	ekonomika
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
úplně	úplně	k6eAd1	úplně
přestala	přestat	k5eAaPmAgFnS	přestat
používat	používat	k5eAaImF	používat
svou	svůj	k3xOyFgFnSc4	svůj
historickou	historický	k2eAgFnSc4d1	historická
měnu	měna	k1gFnSc4	měna
francouzský	francouzský	k2eAgInSc4d1	francouzský
frank	frank	k1gInSc4	frank
a	a	k8xC	a
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Evropské	evropský	k2eAgFnSc2d1	Evropská
měnové	měnový	k2eAgFnSc2d1	měnová
unie	unie	k1gFnSc2	unie
užívá	užívat	k5eAaImIp3nS	užívat
společnou	společný	k2eAgFnSc4d1	společná
měnu	měna	k1gFnSc4	měna
euro	euro	k1gNnSc1	euro
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
OECD	OECD	kA	OECD
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2007	[number]	k4	2007
je	být	k5eAaImIp3nS	být
Francie	Francie	k1gFnSc1	Francie
5	[number]	k4	5
<g/>
.	.	kIx.	.
největší	veliký	k2eAgMnSc1d3	veliký
exportér	exportér	k1gMnSc1	exportér
výrobků	výrobek	k1gInPc2	výrobek
na	na	k7c6	na
světě	svět	k1gInSc6	svět
po	po	k7c6	po
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Číně	Čína	k1gFnSc3	Čína
<g/>
,	,	kIx,	,
USA	USA	kA	USA
a	a	k8xC	a
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
šestý	šestý	k4xOgMnSc1	šestý
největší	veliký	k2eAgMnSc1d3	veliký
dovozce	dovozce	k1gMnSc1	dovozce
výrobků	výrobek	k1gInPc2	výrobek
za	za	k7c4	za
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Německem	Německo	k1gNnSc7	Německo
<g/>
,	,	kIx,	,
Čínou	Čína	k1gFnSc7	Čína
<g/>
,	,	kIx,	,
Spojeným	spojený	k2eAgNnSc7d1	spojené
královstvím	království	k1gNnSc7	království
a	a	k8xC	a
Japonskem	Japonsko	k1gNnSc7	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
podle	podle	k7c2	podle
statistiky	statistika	k1gFnSc2	statistika
OECD	OECD	kA	OECD
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
Francie	Francie	k1gFnSc2	Francie
na	na	k7c6	na
třetím	třetí	k4xOgNnSc6	třetí
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
získávání	získávání	k1gNnSc6	získávání
přímých	přímý	k2eAgFnPc2d1	přímá
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
investic	investice	k1gFnPc2	investice
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
téměř	téměř	k6eAd1	téměř
158	[number]	k4	158
miliardami	miliarda	k4xCgFnPc7	miliarda
USD	USD	kA	USD
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
investic	investice	k1gFnPc2	investice
se	se	k3xPyFc4	se
Francie	Francie	k1gFnSc1	Francie
zařadila	zařadit	k5eAaPmAgFnS	zařadit
za	za	k7c4	za
USA	USA	kA	USA
(	(	kIx(	(
<g/>
237,5	[number]	k4	237,5
mld.	mld.	k?	mld.
USD	USD	kA	USD
investic	investice	k1gFnPc2	investice
<g/>
)	)	kIx)	)
a	a	k8xC	a
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
185,9	[number]	k4	185,9
mld.	mld.	k?	mld.
USD	USD	kA	USD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
francouzské	francouzský	k2eAgFnSc2d1	francouzská
společnosti	společnost	k1gFnSc2	společnost
investovaly	investovat	k5eAaBmAgFnP	investovat
ve	v	k7c6	v
stejném	stejné	k1gNnSc6	stejné
období	období	k1gNnPc2	období
224,6	[number]	k4	224,6
miliard	miliarda	k4xCgFnPc2	miliarda
USD	USD	kA	USD
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zařazuje	zařazovat	k5eAaImIp3nS	zařazovat
Francii	Francie	k1gFnSc4	Francie
na	na	k7c4	na
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
žebříčku	žebříček	k1gInSc2	žebříček
největších	veliký	k2eAgMnPc2d3	veliký
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
investorů	investor	k1gMnPc2	investor
po	po	k7c6	po
USA	USA	kA	USA
(	(	kIx(	(
<g/>
333,2	[number]	k4	333,2
mld.	mld.	k?	mld.
USD	USD	kA	USD
<g/>
)	)	kIx)	)
a	a	k8xC	a
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
(	(	kIx(	(
<g/>
229,8	[number]	k4	229,8
mld.	mld.	k?	mld.
USD	USD	kA	USD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
je	být	k5eAaImIp3nS	být
také	také	k9	také
druhou	druhý	k4xOgFnSc7	druhý
nejvíce	hodně	k6eAd3	hodně
produktivní	produktivní	k2eAgFnSc7d1	produktivní
zemí	zem	k1gFnSc7	zem
v	v	k7c6	v
OECD	OECD	kA	OECD
(	(	kIx(	(
<g/>
nepočítaje	nepočítaje	k7c4	nepočítaje
Norsko	Norsko	k1gNnSc4	Norsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
hodnoty	hodnota	k1gFnPc1	hodnota
zvýšeny	zvýšen	k2eAgFnPc1d1	zvýšena
uměle	uměle	k6eAd1	uměle
tržbami	tržba	k1gFnPc7	tržba
z	z	k7c2	z
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
a	a	k8xC	a
Lucembursko	Lucembursko	k1gNnSc1	Lucembursko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
s	s	k7c7	s
velkými	velký	k2eAgFnPc7d1	velká
offshorovými	offshorův	k2eAgFnPc7d1	offshorův
investicemi	investice	k1gFnPc7	investice
bank	banka	k1gFnPc2	banka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
byl	být	k5eAaImAgInS	být
průměrný	průměrný	k2eAgInSc1d1	průměrný
hrubý	hrubý	k2eAgInSc1d1	hrubý
domácí	domácí	k2eAgInSc1d1	domácí
produkt	produkt	k1gInSc1	produkt
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
hodinu	hodina	k1gFnSc4	hodina
práce	práce	k1gFnSc2	práce
47,2	[number]	k4	47,2
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
příklady	příklad	k1gInPc7	příklad
technologické	technologický	k2eAgFnSc2d1	technologická
vyspělosti	vyspělost	k1gFnSc2	vyspělost
francouzského	francouzský	k2eAgInSc2d1	francouzský
průmyslu	průmysl	k1gInSc2	průmysl
patří	patřit	k5eAaImIp3nP	patřit
mj.	mj.	kA	mj.
vysokorychlostní	vysokorychlostní	k2eAgInPc1d1	vysokorychlostní
vlaky	vlak	k1gInPc1	vlak
TGV	TGV	kA	TGV
a	a	k8xC	a
letadla	letadlo	k1gNnSc2	letadlo
Airbus	airbus	k1gInSc1	airbus
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
síť	síť	k1gFnSc1	síť
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2008	[number]	k4	2008
spojuje	spojovat	k5eAaImIp3nS	spojovat
29	[number]	k4	29
473	[number]	k4	473
km	km	kA	km
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejdražší	drahý	k2eAgFnSc7d3	nejdražší
železniční	železniční	k2eAgFnSc7d1	železniční
sítí	síť	k1gFnSc7	síť
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
spravuje	spravovat	k5eAaImIp3nS	spravovat
ji	on	k3xPp3gFnSc4	on
společnost	společnost	k1gFnSc4	společnost
RFF	RFF	kA	RFF
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Německu	Německo	k1gNnSc6	Německo
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
druhou	druhý	k4xOgFnSc4	druhý
nejrozsáhlejší	rozsáhlý	k2eAgFnSc4d3	nejrozsáhlejší
železniční	železniční	k2eAgFnSc4d1	železniční
síť	síť	k1gFnSc4	síť
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Vlaky	vlak	k1gInPc1	vlak
osobní	osobní	k2eAgFnSc2d1	osobní
dopravy	doprava	k1gFnSc2	doprava
provozuje	provozovat	k5eAaImIp3nS	provozovat
společnost	společnost	k1gFnSc1	společnost
SNCF	SNCF	kA	SNCF
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
vysokorychlostní	vysokorychlostní	k2eAgNnSc4d1	vysokorychlostní
vlakové	vlakový	k2eAgNnSc4d1	vlakové
spojení	spojení	k1gNnSc4	spojení
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
patří	patřit	k5eAaImIp3nP	patřit
Thalys	Thalysa	k1gFnPc2	Thalysa
<g/>
,	,	kIx,	,
Eurostar	Eurostara	k1gFnPc2	Eurostara
a	a	k8xC	a
TGV	TGV	kA	TGV
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
za	za	k7c2	za
komerčního	komerční	k2eAgNnSc2d1	komerční
využití	využití	k1gNnSc2	využití
na	na	k7c6	na
vysokorychlostních	vysokorychlostní	k2eAgFnPc6d1	vysokorychlostní
tratích	trať	k1gFnPc6	trať
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
rychlosti	rychlost	k1gFnPc1	rychlost
270	[number]	k4	270
-	-	kIx~	-
320	[number]	k4	320
km	km	kA	km
/	/	kIx~	/
h.	h.	k?	h.
Eurostar	Eurostar	k1gInSc1	Eurostar
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
Eurotunnel	Eurotunnel	k1gMnSc1	Eurotunnel
Shuttle	Shuttle	k1gFnPc4	Shuttle
<g/>
,	,	kIx,	,
spojuje	spojovat	k5eAaImIp3nS	spojovat
Francii	Francie	k1gFnSc4	Francie
se	s	k7c7	s
Spojeným	spojený	k2eAgNnSc7d1	spojené
královstvím	království	k1gNnSc7	království
přes	přes	k7c4	přes
Eurotunel	Eurotunel	k1gInSc4	Eurotunel
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
má	mít	k5eAaImIp3nS	mít
železniční	železniční	k2eAgNnSc1d1	železniční
spojení	spojení	k1gNnSc1	spojení
do	do	k7c2	do
všech	všecek	k3xTgInPc2	všecek
sousedních	sousední	k2eAgInPc2d1	sousední
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
Andorry	Andorra	k1gFnSc2	Andorra
<g/>
.	.	kIx.	.
</s>
<s>
Vnitrostátní	vnitrostátní	k2eAgNnSc1d1	vnitrostátní
spojení	spojení	k1gNnSc1	spojení
je	být	k5eAaImIp3nS	být
také	také	k9	také
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozvinuté	rozvinutý	k2eAgNnSc4d1	rozvinuté
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
také	také	k9	také
několik	několik	k4yIc1	několik
podzemních	podzemní	k2eAgFnPc2d1	podzemní
železnic	železnice	k1gFnPc2	železnice
a	a	k8xC	a
tramvají	tramvaj	k1gFnPc2	tramvaj
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
autobusovou	autobusový	k2eAgFnSc4d1	autobusová
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přibližně	přibližně	k6eAd1	přibližně
893	[number]	k4	893
300	[number]	k4	300
kilometrů	kilometr	k1gInPc2	kilometr
funkčních	funkční	k2eAgFnPc2d1	funkční
vozovek	vozovka	k1gFnPc2	vozovka
<g/>
.	.	kIx.	.
</s>
<s>
Dálniční	dálniční	k2eAgFnSc1d1	dálniční
síť	síť	k1gFnSc1	síť
měří	měřit	k5eAaImIp3nS	měřit
přibližně	přibližně	k6eAd1	přibližně
12	[number]	k4	12
000	[number]	k4	000
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
zde	zde	k6eAd1	zde
pravidelné	pravidelný	k2eAgInPc1d1	pravidelný
registrační	registrační	k2eAgInPc1d1	registrační
poplatky	poplatek	k1gInPc1	poplatek
<g/>
,	,	kIx,	,
či	či	k8xC	či
daně	daň	k1gFnPc1	daň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
dálnic	dálnice	k1gFnPc2	dálnice
se	se	k3xPyFc4	se
vybírá	vybírat	k5eAaImIp3nS	vybírat
mýtné	mýtné	k1gNnSc1	mýtné
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
blízkosti	blízkost	k1gFnSc2	blízkost
velkých	velký	k2eAgFnPc2d1	velká
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
novém	nový	k2eAgInSc6d1	nový
trhu	trh	k1gInSc6	trh
s	s	k7c7	s
automobily	automobil	k1gInPc7	automobil
převládají	převládat	k5eAaImIp3nP	převládat
národní	národní	k2eAgFnSc1d1	národní
značky	značka	k1gFnPc1	značka
<g/>
,	,	kIx,	,
jakými	jaký	k3yIgFnPc7	jaký
jsou	být	k5eAaImIp3nP	být
Renault	renault	k1gInSc4	renault
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
%	%	kIx~	%
ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
aut	auto	k1gNnPc2	auto
prodaných	prodaný	k2eAgNnPc2d1	prodané
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Peugeot	peugeot	k1gInSc1	peugeot
(	(	kIx(	(
<g/>
20,1	[number]	k4	20,1
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Citroën	Citroën	k1gMnSc1	Citroën
(	(	kIx(	(
<g/>
13,5	[number]	k4	13,5
<g/>
%	%	kIx~	%
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
70	[number]	k4	70
<g/>
%	%	kIx~	%
nových	nový	k2eAgNnPc2d1	nové
prodaných	prodaný	k2eAgNnPc2d1	prodané
aut	auto	k1gNnPc2	auto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
naftový	naftový	k2eAgInSc4d1	naftový
pohon	pohon	k1gInSc4	pohon
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
znatelně	znatelně	k6eAd1	znatelně
více	hodně	k6eAd2	hodně
než	než	k8xS	než
těch	ten	k3xDgFnPc2	ten
benzínových	benzínový	k2eAgFnPc2d1	benzínová
a	a	k8xC	a
LPG	LPG	kA	LPG
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
se	se	k3xPyFc4	se
také	také	k9	také
nachází	nacházet	k5eAaImIp3nS	nacházet
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
most	most	k1gInSc4	most
na	na	k7c6	na
světě	svět	k1gInSc6	svět
-	-	kIx~	-
viadukt	viadukt	k1gInSc4	viadukt
Millau	Millaa	k1gFnSc4	Millaa
a	a	k8xC	a
několik	několik	k4yIc4	několik
důležitých	důležitý	k2eAgInPc2d1	důležitý
mostů	most	k1gInPc2	most
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Pont	Pont	k1gInSc1	Pont
de	de	k?	de
Normandie	Normandie	k1gFnSc2	Normandie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přibližně	přibližně	k6eAd1	přibližně
477	[number]	k4	477
letišť	letiště	k1gNnPc2	letiště
a	a	k8xC	a
přistávacích	přistávací	k2eAgFnPc2d1	přistávací
ploch	plocha	k1gFnPc2	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgMnSc7d3	veliký
a	a	k8xC	a
nejdůležitějším	důležitý	k2eAgNnSc7d3	nejdůležitější
letištěm	letiště	k1gNnSc7	letiště
je	být	k5eAaImIp3nS	být
letiště	letiště	k1gNnSc1	letiště
Charlese	Charles	k1gMnSc2	Charles
de	de	k?	de
Gaulla	Gaull	k1gMnSc2	Gaull
poblíž	poblíž	k7c2	poblíž
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
také	také	k9	také
centrem	centr	k1gMnSc7	centr
francouzských	francouzský	k2eAgFnPc2d1	francouzská
národních	národní	k2eAgFnPc2d1	národní
aerolinek	aerolinka	k1gFnPc2	aerolinka
-	-	kIx~	-
Air	Air	k1gMnSc2	Air
France	Franc	k1gMnSc2	Franc
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
10	[number]	k4	10
větších	veliký	k2eAgInPc2d2	veliký
přístavů	přístav	k1gInPc2	přístav
-	-	kIx~	-
největší	veliký	k2eAgMnSc1d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
v	v	k7c6	v
Marseille	Marseille	k1gFnSc6	Marseille
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Demografie	demografie	k1gFnSc2	demografie
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
od	od	k7c2	od
pravěku	pravěk	k1gInSc2	pravěk
byla	být	k5eAaImAgFnS	být
Francie	Francie	k1gFnSc1	Francie
křižovatkou	křižovatka	k1gFnSc7	křižovatka
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
migrace	migrace	k1gFnSc1	migrace
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
a	a	k8xC	a
invazí	invaze	k1gFnPc2	invaze
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
území	území	k1gNnSc2	území
Francie	Francie	k1gFnSc2	Francie
obývali	obývat	k5eAaImAgMnP	obývat
Keltové	Kelt	k1gMnPc1	Kelt
(	(	kIx(	(
<g/>
Galie	Galie	k1gFnSc1	Galie
a	a	k8xC	a
Bretaň	Bretaň	k1gFnSc1	Bretaň
<g/>
)	)	kIx)	)
a	a	k8xC	a
Aquitánci	Aquitánek	k1gMnPc1	Aquitánek
(	(	kIx(	(
<g/>
Baskicko	Baskicko	k1gNnSc1	Baskicko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
usazovali	usazovat	k5eAaImAgMnP	usazovat
Římané	Říman	k1gMnPc1	Říman
a	a	k8xC	a
Germáni	Germán	k1gMnPc1	Germán
(	(	kIx(	(
<g/>
Frankové	Frank	k1gMnPc1	Frank
<g/>
,	,	kIx,	,
Vizigótové	Vizigót	k1gMnPc1	Vizigót
<g/>
,	,	kIx,	,
Burgunďané	Burgunďan	k1gMnPc1	Burgunďan
a	a	k8xC	a
Vikingové	Viking	k1gMnPc1	Viking
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
kmeny	kmen	k1gInPc1	kmen
a	a	k8xC	a
národy	národ	k1gInPc1	národ
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
po	po	k7c6	po
staletí	staletí	k1gNnPc2	staletí
mísily	mísit	k5eAaImAgFnP	mísit
a	a	k8xC	a
výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
dnešní	dnešní	k2eAgFnSc1d1	dnešní
populace	populace	k1gFnSc1	populace
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
dávných	dávný	k2eAgFnPc2d1	dávná
dob	doba	k1gFnPc2	doba
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
ještě	ještě	k9	ještě
vlna	vlna	k1gFnSc1	vlna
migrace	migrace	k1gFnSc1	migrace
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
lidí	člověk	k1gMnPc2	člověk
těchto	tento	k3xDgFnPc2	tento
národností	národnost	k1gFnPc2	národnost
:	:	kIx,	:
Belgičané	Belgičan	k1gMnPc1	Belgičan
<g/>
,	,	kIx,	,
Italové	Ital	k1gMnPc1	Ital
<g/>
,	,	kIx,	,
Španělé	Španěl	k1gMnPc1	Španěl
<g/>
,	,	kIx,	,
Portugalci	Portugalec	k1gMnPc1	Portugalec
<g/>
,	,	kIx,	,
Poláci	Polák	k1gMnPc1	Polák
<g/>
,	,	kIx,	,
Arméni	Armén	k1gMnPc1	Armén
<g/>
,	,	kIx,	,
a	a	k8xC	a
Židé	Žid	k1gMnPc1	Žid
z	z	k7c2	z
Východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Maghrebu	Maghreb	k1gInSc2	Maghreb
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
stěhují	stěhovat	k5eAaImIp3nP	stěhovat
Arabové	Arab	k1gMnPc1	Arab
<g/>
,	,	kIx,	,
Berbeři	Berber	k1gMnPc1	Berber
<g/>
,	,	kIx,	,
černoši	černoch	k1gMnPc1	černoch
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Karibiku	Karibik	k1gInSc2	Karibik
<g/>
,	,	kIx,	,
Číňané	Číňan	k1gMnPc1	Číňan
a	a	k8xC	a
Vietnamci	Vietnamec	k1gMnPc1	Vietnamec
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
se	se	k3xPyFc4	se
vracejí	vracet	k5eAaImIp3nP	vracet
také	také	k9	také
francouzští	francouzský	k2eAgMnPc1d1	francouzský
kolonisté	kolonista	k1gMnPc1	kolonista
z	z	k7c2	z
Alžírska	Alžírsko	k1gNnSc2	Alžírsko
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
bývalých	bývalý	k2eAgFnPc2d1	bývalá
kolonií	kolonie	k1gFnPc2	kolonie
tzv.	tzv.	kA	tzv.
Černé	Černá	k1gFnSc2	Černá
nohy	noha	k1gFnSc2	noha
<g/>
.	.	kIx.	.
</s>
<s>
Zjišťovat	zjišťovat	k5eAaImF	zjišťovat
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
etnickou	etnický	k2eAgFnSc4d1	etnická
příslušnost	příslušnost	k1gFnSc4	příslušnost
a	a	k8xC	a
náboženství	náboženství	k1gNnSc4	náboženství
při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
lidu	lid	k1gInSc2	lid
je	být	k5eAaImIp3nS	být
zákonem	zákon	k1gInSc7	zákon
zakázáno	zakázán	k2eAgNnSc4d1	zakázáno
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
odhadováno	odhadován	k2eAgNnSc1d1	odhadováno
<g/>
,	,	kIx,	,
že	že	k8xS	že
40	[number]	k4	40
%	%	kIx~	%
francouzské	francouzský	k2eAgFnSc2d1	francouzská
populace	populace	k1gFnSc2	populace
má	mít	k5eAaImIp3nS	mít
předka	předek	k1gMnSc4	předek
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
nějaké	nějaký	k3yIgFnSc2	nějaký
migrační	migrační	k2eAgFnSc2d1	migrační
vlny	vlna	k1gFnSc2	vlna
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dělá	dělat	k5eAaImIp3nS	dělat
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
etnicky	etnicky	k6eAd1	etnicky
nejrůznorodější	různorodý	k2eAgFnSc4d3	nejrůznorodější
zemi	zem	k1gFnSc4	zem
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
úroveň	úroveň	k1gFnSc1	úroveň
je	být	k5eAaImIp3nS	být
srovnatelná	srovnatelný	k2eAgFnSc1d1	srovnatelná
např.	např.	kA	např.
s	s	k7c7	s
USA	USA	kA	USA
či	či	k8xC	či
Kanadou	Kanada	k1gFnSc7	Kanada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
známa	znám	k2eAgNnPc1d1	známo
data	datum	k1gNnPc1	datum
z	z	k7c2	z
testování	testování	k1gNnSc2	testování
novorozenců	novorozenec	k1gMnPc2	novorozenec
na	na	k7c4	na
srpkovitou	srpkovitý	k2eAgFnSc4d1	srpkovitá
anémii	anémie	k1gFnSc4	anémie
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
původem	původ	k1gInSc7	původ
v	v	k7c6	v
rizikových	rizikový	k2eAgFnPc6d1	riziková
oblastech	oblast	k1gFnPc6	oblast
mimo	mimo	k7c4	mimo
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
těchto	tento	k3xDgInPc2	tento
údajů	údaj	k1gInPc2	údaj
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
34,44	[number]	k4	34,44
<g/>
%	%	kIx~	%
novorozenců	novorozenec	k1gMnPc2	novorozenec
nebílých	bílý	k2eNgMnPc2d1	nebílý
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
představovalo	představovat	k5eAaImAgNnS	představovat
zvýšení	zvýšení	k1gNnSc4	zvýšení
z	z	k7c2	z
27	[number]	k4	27
<g/>
%	%	kIx~	%
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
historický	historický	k2eAgInSc4d1	historický
vývoj	vývoj	k1gInSc4	vývoj
populace	populace	k1gFnSc2	populace
Francie	Francie	k1gFnSc2	Francie
velmi	velmi	k6eAd1	velmi
atypický	atypický	k2eAgMnSc1d1	atypický
oproti	oproti	k7c3	oproti
zbytku	zbytek	k1gInSc3	zbytek
Západního	západní	k2eAgInSc2d1	západní
Světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
zbylé	zbylý	k2eAgFnSc2d1	zbylá
Evropy	Evropa	k1gFnSc2	Evropa
nemá	mít	k5eNaImIp3nS	mít
Francie	Francie	k1gFnSc1	Francie
zkušenosti	zkušenost	k1gFnSc2	zkušenost
se	s	k7c7	s
silným	silný	k2eAgInSc7d1	silný
populačním	populační	k2eAgInSc7d1	populační
růstem	růst	k1gInSc7	růst
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zažila	zažít	k5eAaPmAgFnS	zažít
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc4d2	veliký
populační	populační	k2eAgFnSc4d1	populační
explozi	exploze	k1gFnSc4	exploze
než	než	k8xS	než
kterýkoliv	kterýkoliv	k3yIgInSc1	kterýkoliv
jiný	jiný	k2eAgInSc1d1	jiný
evropský	evropský	k2eAgInSc1d1	evropský
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
čtyřicátých	čtyřicátý	k4xOgNnPc2	čtyřicátý
a	a	k8xC	a
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
přicházely	přicházet	k5eAaImAgFnP	přicházet
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
především	především	k6eAd1	především
italské	italský	k2eAgFnSc2d1	italská
pracovní	pracovní	k2eAgFnSc2d1	pracovní
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
na	na	k7c4	na
630	[number]	k4	630
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
Státní	státní	k2eAgInSc1d1	státní
migrační	migrační	k2eAgInSc1d1	migrační
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
činnost	činnost	k1gFnSc1	činnost
spočívala	spočívat	k5eAaImAgFnS	spočívat
v	v	k7c6	v
náboru	nábor	k1gInSc6	nábor
a	a	k8xC	a
zprostředkování	zprostředkování	k1gNnSc6	zprostředkování
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
pracovních	pracovní	k2eAgFnPc2d1	pracovní
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
dvoustranných	dvoustranný	k2eAgFnPc2d1	dvoustranná
smluv	smlouva	k1gFnPc2	smlouva
se	s	k7c7	s
Španělskem	Španělsko	k1gNnSc7	Španělsko
<g/>
,	,	kIx,	,
Portugalskem	Portugalsko	k1gNnSc7	Portugalsko
<g/>
,	,	kIx,	,
Jugoslávií	Jugoslávie	k1gFnSc7	Jugoslávie
a	a	k8xC	a
zeměmi	zem	k1gFnPc7	zem
bývalých	bývalý	k2eAgFnPc2d1	bývalá
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
Marokem	Maroko	k1gNnSc7	Maroko
a	a	k8xC	a
Tuniskem	Tunisko	k1gNnSc7	Tunisko
<g/>
,	,	kIx,	,
silněji	silně	k6eAd2	silně
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
snaha	snaha	k1gFnSc1	snaha
o	o	k7c6	o
najímání	najímání	k1gNnSc6	najímání
pracovní	pracovní	k2eAgFnSc2d1	pracovní
síly	síla	k1gFnSc2	síla
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
také	také	k9	také
ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
padesátých	padesátý	k4xOgNnPc2	padesátý
a	a	k8xC	a
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
počet	počet	k1gInSc1	počet
úředně	úředně	k6eAd1	úředně
hlášených	hlášený	k2eAgMnPc2d1	hlášený
cizinců	cizinec	k1gMnPc2	cizinec
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
Alžířanů	Alžířan	k1gMnPc2	Alžířan
<g/>
)	)	kIx)	)
čísla	číslo	k1gNnSc2	číslo
2,3	[number]	k4	2,3
milionu	milion	k4xCgInSc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
prudkého	prudký	k2eAgInSc2d1	prudký
rozvoje	rozvoj	k1gInSc2	rozvoj
se	se	k3xPyFc4	se
imigrace	imigrace	k1gFnSc1	imigrace
vymkla	vymknout	k5eAaPmAgFnS	vymknout
kontrole	kontrola	k1gFnSc3	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
Migranti	migrant	k1gMnPc1	migrant
přicházeli	přicházet	k5eAaImAgMnP	přicházet
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
spontánně	spontánně	k6eAd1	spontánně
nebo	nebo	k8xC	nebo
na	na	k7c4	na
osobní	osobní	k2eAgFnPc4d1	osobní
výzvy	výzva	k1gFnPc4	výzva
<g/>
.	.	kIx.	.
</s>
<s>
Ztráta	ztráta	k1gFnSc1	ztráta
kontroly	kontrola	k1gFnSc2	kontrola
nad	nad	k7c7	nad
imigrací	imigrace	k1gFnSc7	imigrace
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
a	a	k8xC	a
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
zvýšený	zvýšený	k2eAgInSc4d1	zvýšený
nárůst	nárůst	k1gInSc4	nárůst
nelegálních	legální	k2eNgInPc2d1	nelegální
pracovních	pracovní	k2eAgInPc2d1	pracovní
poměrů	poměr	k1gInPc2	poměr
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
omezování	omezování	k1gNnSc3	omezování
možností	možnost	k1gFnPc2	možnost
jak	jak	k8xS	jak
přiblížit	přiblížit	k5eAaPmF	přiblížit
způsoby	způsob	k1gInPc4	způsob
zprostředkování	zprostředkování	k1gNnSc2	zprostředkování
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
pracovní	pracovní	k2eAgFnPc4d1	pracovní
či	či	k8xC	či
bytové	bytový	k2eAgFnPc4d1	bytová
možnosti	možnost	k1gFnPc4	možnost
minimálním	minimální	k2eAgInPc3d1	minimální
sociálním	sociální	k2eAgInPc3d1	sociální
požadavkům	požadavek	k1gInPc3	požadavek
<g/>
.	.	kIx.	.
</s>
<s>
Nelegální	legální	k2eNgFnSc1d1	nelegální
práce	práce	k1gFnSc1	práce
a	a	k8xC	a
vysoká	vysoký	k2eAgFnSc1d1	vysoká
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
vedly	vést	k5eAaImAgFnP	vést
ke	k	k7c3	k
stupňování	stupňování	k1gNnSc3	stupňování
napětí	napětí	k1gNnSc2	napětí
mezi	mezi	k7c7	mezi
domácími	domácí	k2eAgMnPc7d1	domácí
obyvateli	obyvatel	k1gMnPc7	obyvatel
a	a	k8xC	a
imigranty	imigrant	k1gMnPc7	imigrant
<g/>
,	,	kIx,	,
především	především	k9	především
severoafrickými	severoafrický	k2eAgMnPc7d1	severoafrický
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
etnicko-kulturními	etnickoulturní	k2eAgFnPc7d1	etnicko-kulturní
skupinami	skupina	k1gFnPc7	skupina
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
začalo	začít	k5eAaPmAgNnS	začít
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
otevřeným	otevřený	k2eAgFnPc3d1	otevřená
vzpourám	vzpoura	k1gFnPc3	vzpoura
a	a	k8xC	a
pouličním	pouliční	k2eAgFnPc3d1	pouliční
bitkám	bitka	k1gFnPc3	bitka
s	s	k7c7	s
policií	policie	k1gFnSc7	policie
<g/>
.	.	kIx.	.
</s>
<s>
Ukončení	ukončení	k1gNnSc1	ukončení
náboru	nábor	k1gInSc2	nábor
pracovních	pracovní	k2eAgFnPc2d1	pracovní
sil	síla	k1gFnPc2	síla
změnilo	změnit	k5eAaPmAgNnS	změnit
charakter	charakter	k1gInSc4	charakter
imigrace	imigrace	k1gFnSc2	imigrace
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
projevil	projevit	k5eAaPmAgInS	projevit
zvýšeným	zvýšený	k2eAgInSc7d1	zvýšený
příchodem	příchod	k1gInSc7	příchod
dalších	další	k2eAgMnPc2d1	další
rodinných	rodinný	k2eAgMnPc2d1	rodinný
příslušníků	příslušník	k1gMnPc2	příslušník
především	především	k9	především
z	z	k7c2	z
mimoevropských	mimoevropský	k2eAgFnPc2d1	mimoevropská
oblastí	oblast	k1gFnPc2	oblast
(	(	kIx(	(
<g/>
ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
odvozovalo	odvozovat	k5eAaImAgNnS	odvozovat
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
55	[number]	k4	55
milionů	milion	k4xCgInPc2	milion
Francouzů	Francouz	k1gMnPc2	Francouz
asi	asi	k9	asi
18	[number]	k4	18
milionů	milion	k4xCgInPc2	milion
alespoň	alespoň	k9	alespoň
částečně	částečně	k6eAd1	částečně
od	od	k7c2	od
rodičů	rodič	k1gMnPc2	rodič
nebo	nebo	k8xC	nebo
prarodičů	prarodič	k1gMnPc2	prarodič
pocházejících	pocházející	k2eAgMnPc2d1	pocházející
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
článku	článek	k1gInSc2	článek
2	[number]	k4	2
ústavy	ústava	k1gFnSc2	ústava
je	být	k5eAaImIp3nS	být
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
Francie	Francie	k1gFnSc2	Francie
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
,	,	kIx,	,
románský	románský	k2eAgInSc1d1	románský
jazyk	jazyk	k1gInSc1	jazyk
odvozený	odvozený	k2eAgInSc1d1	odvozený
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
používána	používán	k2eAgFnSc1d1	používána
němčina	němčina	k1gFnSc1	němčina
a	a	k8xC	a
arabština	arabština	k1gFnSc1	arabština
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
dominantní	dominantní	k2eAgFnSc1d1	dominantní
římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
země	země	k1gFnSc1	země
se	s	k7c7	s
znatelným	znatelný	k2eAgInSc7d1	znatelný
protiklerikálním	protiklerikální	k2eAgInSc7d1	protiklerikální
vlivem	vliv	k1gInSc7	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Svoboda	svoboda	k1gFnSc1	svoboda
náboženství	náboženství	k1gNnSc2	náboženství
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
ústavou	ústava	k1gFnSc7	ústava
<g/>
;	;	kIx,	;
inspirace	inspirace	k1gFnSc1	inspirace
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
Všeobecné	všeobecný	k2eAgFnSc2d1	všeobecná
deklarace	deklarace	k1gFnSc2	deklarace
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Dominantní	dominantní	k2eAgInSc1d1	dominantní
koncept	koncept	k1gInSc1	koncept
vztahu	vztah	k1gInSc2	vztah
mezi	mezi	k7c7	mezi
veřejnou	veřejný	k2eAgFnSc7d1	veřejná
sférou	sféra	k1gFnSc7	sféra
a	a	k8xC	a
věřícími	věřící	k1gFnPc7	věřící
zabezpečuje	zabezpečovat	k5eAaImIp3nS	zabezpečovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
vládní	vládní	k2eAgFnPc1d1	vládní
instituce	instituce	k1gFnPc1	instituce
(	(	kIx(	(
<g/>
jako	jako	k9	jako
například	například	k6eAd1	například
školy	škola	k1gFnSc2	škola
<g/>
)	)	kIx)	)
by	by	kYmCp3nP	by
neměly	mít	k5eNaImAgInP	mít
zasahovat	zasahovat	k5eAaImF	zasahovat
do	do	k7c2	do
věcí	věc	k1gFnPc2	věc
víry	víra	k1gFnSc2	víra
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
církev	církev	k1gFnSc1	církev
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
neměla	mít	k5eNaImAgFnS	mít
žádným	žádný	k1gMnSc7	žádný
způsobem	způsob	k1gInSc7	způsob
angažovat	angažovat	k5eAaBmF	angažovat
v	v	k7c4	v
politické	politický	k2eAgFnPc4d1	politická
činnosti	činnost	k1gFnPc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
samotná	samotný	k2eAgFnSc1d1	samotná
neudržuje	udržovat	k5eNaImIp3nS	udržovat
statistiky	statistika	k1gFnPc1	statistika
o	o	k7c6	o
víře	víra	k1gFnSc6	víra
svých	svůj	k3xOyFgMnPc2	svůj
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Statistiky	statistika	k1gFnPc1	statistika
The	The	k1gFnSc2	The
World	Worlda	k1gFnPc2	Worlda
Factbook	Factbook	k1gInSc1	Factbook
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
následující	následující	k2eAgNnPc4d1	následující
čísla	číslo	k1gNnPc4	číslo
<g/>
:	:	kIx,	:
Římskokatolické	římskokatolický	k2eAgNnSc4d1	římskokatolické
křesťanství	křesťanství	k1gNnSc4	křesťanství
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
88	[number]	k4	88
%	%	kIx~	%
<g/>
,	,	kIx,	,
islám	islám	k1gInSc1	islám
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
%	%	kIx~	%
<g/>
,	,	kIx,	,
protestantismus	protestantismus	k1gInSc1	protestantismus
2	[number]	k4	2
%	%	kIx~	%
<g/>
,	,	kIx,	,
židovské	židovský	k2eAgNnSc4d1	Židovské
náboženství	náboženství	k1gNnSc4	náboženství
1	[number]	k4	1
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
považuje	považovat	k5eAaImIp3nS	považovat
41	[number]	k4	41
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
existenci	existence	k1gFnSc4	existence
boha	bůh	k1gMnSc2	bůh
za	za	k7c4	za
vyloučenou	vyloučený	k2eAgFnSc4d1	vyloučená
<g/>
,	,	kIx,	,
33	[number]	k4	33
%	%	kIx~	%
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
spíše	spíše	k9	spíše
za	za	k7c4	za
ateisty	ateista	k1gMnPc4	ateista
a	a	k8xC	a
51	[number]	k4	51
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
řeklo	říct	k5eAaPmAgNnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
křesťané	křesťan	k1gMnPc1	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
žilo	žít	k5eAaImAgNnS	žít
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
mnoho	mnoho	k6eAd1	mnoho
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
a	a	k8xC	a
řídili	řídit	k5eAaImAgMnP	řídit
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
tradic	tradice	k1gFnPc2	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
století	století	k1gNnSc2	století
nastalo	nastat	k5eAaPmAgNnS	nastat
ale	ale	k8xC	ale
mnoho	mnoho	k4c1	mnoho
změn	změna	k1gFnPc2	změna
<g/>
:	:	kIx,	:
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vylidnění	vylidnění	k1gNnSc3	vylidnění
venkova	venkov	k1gInSc2	venkov
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
odvrátilo	odvrátit	k5eAaPmAgNnS	odvrátit
od	od	k7c2	od
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
mnoha	mnoho	k4c2	mnoho
změnám	změna	k1gFnPc3	změna
ve	v	k7c6	v
společenských	společenský	k2eAgInPc6d1	společenský
zvycích	zvyk	k1gInPc6	zvyk
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
mladých	mladý	k2eAgMnPc2d1	mladý
muslimů	muslim	k1gMnPc2	muslim
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
čelí	čelit	k5eAaImIp3nS	čelit
diskriminaci	diskriminace	k1gFnSc4	diskriminace
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
vysoké	vysoký	k2eAgFnSc2d1	vysoká
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
na	na	k7c6	na
předměstích	předměstí	k1gNnPc6	předměstí
Paříže	Paříž	k1gFnSc2	Paříž
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
přes	přes	k7c4	přes
50	[number]	k4	50
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
komunitu	komunita	k1gFnSc4	komunita
mají	mít	k5eAaImIp3nP	mít
stále	stále	k6eAd1	stále
větší	veliký	k2eAgInSc4d2	veliký
vliv	vliv	k1gInSc4	vliv
radikální	radikální	k2eAgMnPc1d1	radikální
islamističtí	islamistický	k2eAgMnPc1d1	islamistický
salafisté	salafista	k1gMnPc1	salafista
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
analýzy	analýza	k1gFnSc2	analýza
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
zavedení	zavedení	k1gNnSc1	zavedení
islámského	islámský	k2eAgNnSc2d1	islámské
práva	právo	k1gNnSc2	právo
šaría	šarí	k1gInSc2	šarí
podporuje	podporovat	k5eAaImIp3nS	podporovat
72	[number]	k4	72
<g/>
%	%	kIx~	%
francouzských	francouzský	k2eAgMnPc2d1	francouzský
muslimů	muslim	k1gMnPc2	muslim
a	a	k8xC	a
35	[number]	k4	35
<g/>
%	%	kIx~	%
muslimů	muslim	k1gMnPc2	muslim
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
ospravedlnitelné	ospravedlnitelný	k2eAgNnSc4d1	ospravedlnitelné
použití	použití	k1gNnSc4	použití
násilí	násilí	k1gNnSc2	násilí
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vzdělání	vzdělání	k1gNnSc2	vzdělání
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
francouzského	francouzský	k2eAgNnSc2d1	francouzské
vzdělání	vzdělání	k1gNnSc2	vzdělání
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
centralizovaný	centralizovaný	k2eAgInSc1d1	centralizovaný
<g/>
,	,	kIx,	,
organizovaný	organizovaný	k2eAgInSc1d1	organizovaný
a	a	k8xC	a
rozvětvený	rozvětvený	k2eAgInSc1d1	rozvětvený
<g/>
.	.	kIx.	.
</s>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
primární	primární	k2eAgNnSc1d1	primární
vzdělání	vzdělání	k1gNnSc1	vzdělání
(	(	kIx(	(
<g/>
enseignement	enseignement	k1gInSc1	enseignement
primaire	primair	k1gInSc5	primair
<g/>
)	)	kIx)	)
sekundární	sekundární	k2eAgNnSc1d1	sekundární
vzdělání	vzdělání	k1gNnSc1	vzdělání
(	(	kIx(	(
<g/>
enseignement	enseignement	k1gInSc1	enseignement
secondaire	secondair	k1gMnSc5	secondair
<g/>
)	)	kIx)	)
terciární	terciární	k2eAgFnSc7d1	terciární
nebo	nebo	k8xC	nebo
akademické	akademický	k2eAgNnSc1d1	akademické
vzdělání	vzdělání	k1gNnSc1	vzdělání
(	(	kIx(	(
<g/>
enseignement	enseignement	k1gMnSc1	enseignement
supérieur	supérieur	k1gMnSc1	supérieur
<g/>
)	)	kIx)	)
Primární	primární	k2eAgMnSc1d1	primární
a	a	k8xC	a
sekundární	sekundární	k2eAgNnSc1d1	sekundární
vzdělání	vzdělání	k1gNnSc1	vzdělání
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
veřejné	veřejný	k2eAgFnSc2d1	veřejná
(	(	kIx(	(
<g/>
soukromé	soukromý	k2eAgFnSc2d1	soukromá
školy	škola	k1gFnSc2	škola
také	také	k9	také
existují	existovat	k5eAaImIp3nP	existovat
jako	jako	k9	jako
celonárodní	celonárodní	k2eAgFnSc4d1	celonárodní
síť	síť	k1gFnSc4	síť
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
primárních	primární	k2eAgFnPc2d1	primární
a	a	k8xC	a
sekundárních	sekundární	k2eAgFnPc2d1	sekundární
katolických	katolický	k2eAgFnPc2d1	katolická
škol	škola	k1gFnPc2	škola
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
akademické	akademický	k2eAgNnSc1d1	akademické
vzdělání	vzdělání	k1gNnSc1	vzdělání
je	být	k5eAaImIp3nS	být
otázkou	otázka	k1gFnSc7	otázka
státních	státní	k2eAgFnPc2d1	státní
i	i	k8xC	i
soukromých	soukromý	k2eAgFnPc2d1	soukromá
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
kultura	kultura	k1gFnSc1	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
kultura	kultura	k1gFnSc1	kultura
je	být	k5eAaImIp3nS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
rozdílností	rozdílnost	k1gFnSc7	rozdílnost
a	a	k8xC	a
posledními	poslední	k2eAgFnPc7d1	poslední
vlnami	vlna	k1gFnPc7	vlna
imigrace	imigrace	k1gFnSc2	imigrace
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
hrála	hrát	k5eAaImAgFnS	hrát
po	po	k7c4	po
celá	celý	k2eAgNnPc4d1	celé
staletí	staletí	k1gNnPc4	staletí
velmi	velmi	k6eAd1	velmi
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
jako	jako	k8xS	jako
kulturní	kulturní	k2eAgNnSc4d1	kulturní
centrum	centrum	k1gNnSc4	centrum
celé	celý	k2eAgFnSc2d1	celá
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Paříží	Paříž	k1gFnSc7	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
architektonických	architektonický	k2eAgInPc2d1	architektonický
stylů	styl	k1gInPc2	styl
západní	západní	k2eAgFnSc2d1	západní
civilizace	civilizace	k1gFnSc2	civilizace
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
;	;	kIx,	;
většina	většina	k1gFnSc1	většina
výtvarných	výtvarný	k2eAgInPc2d1	výtvarný
stylů	styl	k1gInPc2	styl
novověku	novověk	k1gInSc2	novověk
se	se	k3xPyFc4	se
zrodila	zrodit	k5eAaPmAgFnS	zrodit
v	v	k7c6	v
pařížské	pařížský	k2eAgFnSc6d1	Pařížská
umělecké	umělecký	k2eAgFnSc6d1	umělecká
komunitě	komunita	k1gFnSc6	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Francouzské	francouzský	k2eAgNnSc1d1	francouzské
umění	umění	k1gNnSc1	umění
<g/>
.	.	kIx.	.
</s>
<s>
Francouzské	francouzský	k2eAgNnSc1d1	francouzské
umění	umění	k1gNnSc1	umění
existovalo	existovat	k5eAaImAgNnS	existovat
společně	společně	k6eAd1	společně
s	s	k7c7	s
německým	německý	k2eAgMnSc7d1	německý
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
rozdělení	rozdělení	k1gNnSc2	rozdělení
Franské	franský	k2eAgFnSc2d1	Franská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
karolinské	karolinský	k2eAgNnSc1d1	karolinské
umění	umění	k1gNnSc1	umění
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgInSc4d1	vlastní
vývoj	vývoj	k1gInSc4	vývoj
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
v	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
s	s	k7c7	s
šířením	šíření	k1gNnSc7	šíření
románského	románský	k2eAgInSc2d1	románský
slohu	sloh	k1gInSc2	sloh
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1100	[number]	k4	1100
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
rozkvět	rozkvět	k1gInSc1	rozkvět
architektury	architektura	k1gFnSc2	architektura
a	a	k8xC	a
sochařství	sochařství	k1gNnSc2	sochařství
spojeného	spojený	k2eAgNnSc2d1	spojené
převážně	převážně	k6eAd1	převážně
s	s	k7c7	s
regionem	region	k1gInSc7	region
Provence	Provence	k1gFnSc2	Provence
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
stavitelství	stavitelství	k1gNnSc1	stavitelství
silně	silně	k6eAd1	silně
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
římskou	římský	k2eAgFnSc7d1	římská
kulturou	kultura	k1gFnSc7	kultura
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
padesát	padesát	k4xCc4	padesát
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
se	s	k7c7	s
střediskem	středisko	k1gNnSc7	středisko
umění	umění	k1gNnSc2	umění
stává	stávat	k5eAaImIp3nS	stávat
sever	sever	k1gInSc4	sever
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
oblast	oblast	k1gFnSc1	oblast
Normandie	Normandie	k1gFnSc1	Normandie
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
Burgundsko	Burgundsko	k1gNnSc1	Burgundsko
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
počátky	počátek	k1gInPc1	počátek
gotiky	gotika	k1gFnSc2	gotika
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
ryze	ryze	k6eAd1	ryze
gotickou	gotický	k2eAgFnSc7d1	gotická
stavbou	stavba	k1gFnSc7	stavba
je	být	k5eAaImIp3nS	být
Bazilika	bazilika	k1gFnSc1	bazilika
Saint-Denis	Saint-Denis	k1gFnSc1	Saint-Denis
u	u	k7c2	u
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
umělecká	umělecký	k2eAgFnSc1d1	umělecká
tvorba	tvorba	k1gFnSc1	tvorba
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
Paříží	Paříž	k1gFnSc7	Paříž
a	a	k8xC	a
oblastí	oblast	k1gFnSc7	oblast
Île-de-France	Îlee-France	k1gFnSc2	Île-de-France
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
gotika	gotika	k1gFnSc1	gotika
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
do	do	k7c2	do
celé	celý	k2eAgFnSc2d1	celá
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějšími	významný	k2eAgFnPc7d3	nejvýznamnější
francouzskými	francouzský	k2eAgFnPc7d1	francouzská
památkami	památka	k1gFnPc7	památka
tohoto	tento	k3xDgInSc2	tento
slohu	sloh	k1gInSc2	sloh
jsou	být	k5eAaImIp3nP	být
katedrály	katedrála	k1gFnSc2	katedrála
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
městech	město	k1gNnPc6	město
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Remeš	Remeš	k1gFnSc1	Remeš
<g/>
,	,	kIx,	,
Chartres	Chartres	k1gInSc1	Chartres
<g/>
,	,	kIx,	,
Amiens	Amiens	k1gInSc1	Amiens
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vitráže	vitráž	k1gFnPc4	vitráž
a	a	k8xC	a
sochařská	sochařský	k2eAgNnPc4d1	sochařské
díla	dílo	k1gNnPc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
renesance	renesance	k1gFnSc2	renesance
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
Francie	Francie	k1gFnSc1	Francie
výsadní	výsadní	k2eAgNnSc4d1	výsadní
postavení	postavení	k1gNnSc4	postavení
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgNnSc1d1	typické
je	být	k5eAaImIp3nS	být
přejímání	přejímání	k1gNnSc1	přejímání
vzorů	vzor	k1gInPc2	vzor
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Gotika	gotika	k1gFnSc1	gotika
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
drží	držet	k5eAaImIp3nS	držet
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Burgundsku	Burgundsko	k1gNnSc6	Burgundsko
jako	jako	k8xS	jako
pozdní	pozdní	k2eAgFnSc1d1	pozdní
gotika	gotika	k1gFnSc1	gotika
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
uměleckým	umělecký	k2eAgInSc7d1	umělecký
skvostem	skvost	k1gInSc7	skvost
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
zámek	zámek	k1gInSc4	zámek
ve	v	k7c6	v
Fontainebleau	Fontainebleaus	k1gInSc6	Fontainebleaus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
projevil	projevit	k5eAaPmAgInS	projevit
vliv	vliv	k1gInSc1	vliv
pozdní	pozdní	k2eAgFnSc2d1	pozdní
renesance	renesance	k1gFnSc2	renesance
a	a	k8xC	a
manýrismu	manýrismus	k1gInSc2	manýrismus
<g/>
.	.	kIx.	.
</s>
<s>
Inspiraci	inspirace	k1gFnSc3	inspirace
malbami	malba	k1gFnPc7	malba
<g/>
,	,	kIx,	,
sochami	socha	k1gFnPc7	socha
a	a	k8xC	a
dekoracemi	dekorace	k1gFnPc7	dekorace
lze	lze	k6eAd1	lze
spatřit	spatřit	k5eAaPmF	spatřit
v	v	k7c6	v
Antverpách	Antverpy	k1gFnPc6	Antverpy
<g/>
,	,	kIx,	,
Bruselu	Brusel	k1gInSc6	Brusel
i	i	k8xC	i
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
architektury	architektura	k1gFnSc2	architektura
jsou	být	k5eAaImIp3nP	být
dále	daleko	k6eAd2	daleko
známé	známý	k2eAgInPc1d1	známý
zámky	zámek	k1gInPc1	zámek
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Loiře	Loira	k1gFnSc6	Loira
(	(	kIx(	(
<g/>
Blois	Blois	k1gInSc1	Blois
<g/>
,	,	kIx,	,
Chambord	Chambord	k1gInSc1	Chambord
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zámek	zámek	k1gInSc4	zámek
Fountain	Fountaina	k1gFnPc2	Fountaina
u	u	k7c2	u
Paříže	Paříž	k1gFnSc2	Paříž
a	a	k8xC	a
Lucemburský	lucemburský	k2eAgInSc1d1	lucemburský
palác	palác	k1gInSc1	palác
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
rozvoje	rozvoj	k1gInSc2	rozvoj
baroka	baroko	k1gNnSc2	baroko
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
podpoře	podpora	k1gFnSc3	podpora
francouzských	francouzský	k2eAgMnPc2d1	francouzský
králů	král	k1gMnPc2	král
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
řada	řada	k1gFnSc1	řada
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
opět	opět	k6eAd1	opět
model	model	k1gInSc4	model
umění	umění	k1gNnSc2	umění
pro	pro	k7c4	pro
celou	celý	k2eAgFnSc4d1	celá
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
standard	standard	k1gInSc1	standard
si	se	k3xPyFc3	se
Francie	Francie	k1gFnSc1	Francie
udržela	udržet	k5eAaPmAgFnS	udržet
až	až	k9	až
do	do	k7c2	do
pozdního	pozdní	k2eAgMnSc2d1	pozdní
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vzniká	vznikat	k5eAaImIp3nS	vznikat
klasicismus	klasicismus	k1gInSc1	klasicismus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
francouzské	francouzský	k2eAgFnSc2d1	francouzská
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Typickými	typický	k2eAgMnPc7d1	typický
zástupci	zástupce	k1gMnPc7	zástupce
jsou	být	k5eAaImIp3nP	být
malíři	malíř	k1gMnPc1	malíř
J.	J.	kA	J.
L.	L.	kA	L.
David	David	k1gMnSc1	David
<g/>
,	,	kIx,	,
Gérard	Gérard	k1gMnSc1	Gérard
<g/>
,	,	kIx,	,
Ingres	Ingres	k1gMnSc1	Ingres
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
Delacroix	Delacroix	k1gInSc4	Delacroix
<g/>
,	,	kIx,	,
Géricault	Géricault	k2eAgInSc4d1	Géricault
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
Daumier	Daumier	k1gInSc1	Daumier
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
dílo	dílo	k1gNnSc1	dílo
dalo	dát	k5eAaPmAgNnS	dát
impulz	impulz	k1gInSc4	impulz
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vzniká	vznikat	k5eAaImIp3nS	vznikat
realismus	realismus	k1gInSc1	realismus
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc7	jehož
hlavními	hlavní	k2eAgMnPc7d1	hlavní
představiteli	představitel	k1gMnPc7	představitel
jsou	být	k5eAaImIp3nP	být
Courbet	Courbet	k1gMnSc1	Courbet
a	a	k8xC	a
barbizonská	barbizonský	k2eAgFnSc1d1	barbizonský
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moderním	moderní	k2eAgNnSc6d1	moderní
umění	umění	k1gNnSc6	umění
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
důležitá	důležitý	k2eAgFnSc1d1	důležitá
francouzská	francouzský	k2eAgFnSc1d1	francouzská
secese	secese	k1gFnSc1	secese
(	(	kIx(	(
<g/>
Art	Art	k1gFnSc1	Art
nouveau	nouveau	k5eAaPmIp1nS	nouveau
<g/>
)	)	kIx)	)
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
architektuře	architektura	k1gFnSc6	architektura
(	(	kIx(	(
<g/>
vchody	vchod	k1gInPc1	vchod
metra	metro	k1gNnSc2	metro
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
Guimard	Guimard	k1gMnSc1	Guimard
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
mnoho	mnoho	k4c1	mnoho
malířských	malířský	k2eAgInPc2d1	malířský
směrů	směr	k1gInPc2	směr
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
impresionismem	impresionismus	k1gInSc7	impresionismus
(	(	kIx(	(
<g/>
Manet	manet	k1gInSc1	manet
<g/>
,	,	kIx,	,
Renoir	Renoira	k1gFnPc2	Renoira
<g/>
,	,	kIx,	,
Monet	moneta	k1gFnPc2	moneta
<g/>
,	,	kIx,	,
Pissarro	Pissarro	k1gNnSc1	Pissarro
<g/>
,	,	kIx,	,
Sisley	Sisley	k1gInPc1	Sisley
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
kterému	který	k3yRgInSc3	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
postavil	postavit	k5eAaPmAgMnS	postavit
Cézanne	Cézann	k1gInSc5	Cézann
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
osobitým	osobitý	k2eAgInSc7d1	osobitý
stylem	styl	k1gInSc7	styl
<g/>
.	.	kIx.	.
</s>
<s>
Malíř	malíř	k1gMnSc1	malíř
Gauguin	Gauguin	k1gMnSc1	Gauguin
dále	daleko	k6eAd2	daleko
rozvíjel	rozvíjet	k5eAaImAgMnS	rozvíjet
malbu	malba	k1gFnSc4	malba
s	s	k7c7	s
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
plošnost	plošnost	k1gFnSc4	plošnost
a	a	k8xC	a
jasné	jasný	k2eAgNnSc1d1	jasné
ohraničení	ohraničení	k1gNnSc1	ohraničení
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
barevných	barevný	k2eAgNnPc2d1	barevné
polí	pole	k1gNnPc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
styl	styl	k1gInSc1	styl
byl	být	k5eAaImAgInS	být
dále	daleko	k6eAd2	daleko
rozvíjen	rozvíjet	k5eAaImNgInS	rozvíjet
skupinou	skupina	k1gFnSc7	skupina
Nabis	Nabis	k1gFnPc2	Nabis
a	a	k8xC	a
fauvisty	fauvista	k1gMnPc4	fauvista
(	(	kIx(	(
<g/>
Matisse	Matiss	k1gMnPc4	Matiss
<g/>
,	,	kIx,	,
Derain	Derain	k1gMnSc1	Derain
<g/>
,	,	kIx,	,
Vlaminck	Vlaminck	k1gMnSc1	Vlaminck
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sochařství	sochařství	k1gNnSc6	sochařství
vyniká	vynikat	k5eAaImIp3nS	vynikat
Rodin	rodina	k1gFnPc2	rodina
(	(	kIx(	(
<g/>
hra	hra	k1gFnSc1	hra
světla	světlo	k1gNnSc2	světlo
<g/>
)	)	kIx)	)
a	a	k8xC	a
Maillol	Maillol	k1gInSc4	Maillol
(	(	kIx(	(
<g/>
ženský	ženský	k2eAgInSc4d1	ženský
akt	akt	k1gInSc4	akt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
1907	[number]	k4	1907
se	se	k3xPyFc4	se
nese	nést	k5eAaImIp3nS	nést
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
založení	založení	k1gNnSc4	založení
kubismu	kubismus	k1gInSc2	kubismus
Picassem	Picass	k1gInSc7	Picass
<g/>
,	,	kIx,	,
Braquem	Braqu	k1gInSc7	Braqu
a	a	k8xC	a
Grisem	Gris	k1gInSc7	Gris
(	(	kIx(	(
<g/>
kubismus	kubismus	k1gInSc1	kubismus
převádí	převádět	k5eAaImIp3nS	převádět
předměty	předmět	k1gInPc4	předmět
na	na	k7c4	na
geometrické	geometrický	k2eAgInPc4d1	geometrický
prvky	prvek	k1gInPc4	prvek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
směry	směr	k1gInPc7	směr
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
jsou	být	k5eAaImIp3nP	být
dadaismus	dadaismus	k1gInSc4	dadaismus
a	a	k8xC	a
surrealismus	surrealismus	k1gInSc4	surrealismus
(	(	kIx(	(
<g/>
inspirace	inspirace	k1gFnSc1	inspirace
psychoanalýzou	psychoanalýza	k1gFnSc7	psychoanalýza
Sigmunda	Sigmund	k1gMnSc2	Sigmund
Freuda	Freud	k1gMnSc2	Freud
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgInPc1d1	hlavní
otázky	otázka	k1gFnPc4	otázka
nevědomí	nevědomí	k1gNnPc2	nevědomí
<g/>
)	)	kIx)	)
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Marcelem	Marcel	k1gMnSc7	Marcel
Duchampem	Duchamp	k1gMnSc7	Duchamp
<g/>
,	,	kIx,	,
Maxem	Max	k1gMnSc7	Max
Ernstem	Ernst	k1gMnSc7	Ernst
<g/>
,	,	kIx,	,
Salvadorem	Salvador	k1gMnSc7	Salvador
Dalím	Dalí	k1gMnSc7	Dalí
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
dostává	dostávat	k5eAaImIp3nS	dostávat
abstraktní	abstraktní	k2eAgNnSc4d1	abstraktní
umění	umění	k1gNnSc4	umění
díky	díky	k7c3	díky
Pařížské	pařížský	k2eAgFnSc3d1	Pařížská
škole	škola	k1gFnSc3	škola
(	(	kIx(	(
<g/>
École	École	k1gNnSc1	École
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
<g/>
)	)	kIx)	)
a	a	k8xC	a
akčnímu	akční	k2eAgNnSc3d1	akční
umění	umění	k1gNnSc3	umění
(	(	kIx(	(
<g/>
action	action	k1gInSc4	action
painting	painting	k1gInSc1	painting
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dubuffet	Dubuffet	k1gInSc1	Dubuffet
zavedl	zavést	k5eAaPmAgInS	zavést
do	do	k7c2	do
malby	malba	k1gFnSc2	malba
používání	používání	k1gNnSc2	používání
netradičních	tradiční	k2eNgInPc2d1	netradiční
materiálů	materiál	k1gInPc2	materiál
jako	jako	k8xC	jako
sádra	sádra	k1gFnSc1	sádra
<g/>
,	,	kIx,	,
dehet	dehet	k1gInSc1	dehet
nebo	nebo	k8xC	nebo
štěrk	štěrk	k1gInSc1	štěrk
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
op-art	oprt	k1gInSc4	op-art
s	s	k7c7	s
hlavními	hlavní	k2eAgMnPc7d1	hlavní
představiteli	představitel	k1gMnPc7	představitel
V.	V.	kA	V.
Vasarelym	Vasarelym	k1gInSc4	Vasarelym
a	a	k8xC	a
F.	F.	kA	F.
Morelletem	Morelle	k1gNnSc7	Morelle
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
novějším	nový	k2eAgInPc3d2	novější
směrům	směr	k1gInPc3	směr
patřil	patřit	k5eAaImAgInS	patřit
nouveau	nouveaum	k1gNnSc3	nouveaum
réalisme	réalismus	k1gInSc5	réalismus
(	(	kIx(	(
<g/>
nový	nový	k2eAgInSc4d1	nový
realismus	realismus	k1gInSc4	realismus
<g/>
)	)	kIx)	)
a	a	k8xC	a
pop	pop	k1gMnSc1	pop
art	art	k?	art
(	(	kIx(	(
<g/>
Arman	Arman	k1gMnSc1	Arman
<g/>
,	,	kIx,	,
César	César	k1gMnSc1	César
<g/>
,	,	kIx,	,
Spoerri	Spoerr	k1gFnPc1	Spoerr
<g/>
,	,	kIx,	,
Tinguely	Tinguel	k1gInPc1	Tinguel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
a	a	k8xC	a
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
Francie	Francie	k1gFnSc1	Francie
ztotožňuje	ztotožňovat	k5eAaImIp3nS	ztotožňovat
s	s	k7c7	s
charakterem	charakter	k1gInSc7	charakter
soudobého	soudobý	k2eAgNnSc2d1	soudobé
umění	umění	k1gNnSc2	umění
např.	např.	kA	např.
proudy	proud	k1gInPc1	proud
jako	jako	k9	jako
land	land	k6eAd1	land
art	art	k?	art
<g/>
,	,	kIx,	,
body	bod	k1gInPc4	bod
art	art	k?	art
<g/>
,	,	kIx,	,
minimalismus	minimalismus	k1gInSc1	minimalismus
a	a	k8xC	a
konceptualismus	konceptualismus	k1gInSc1	konceptualismus
(	(	kIx(	(
<g/>
Christian	Christian	k1gMnSc1	Christian
Boltanski	Boltansk	k1gFnSc2	Boltansk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
arte	artat	k5eAaPmIp3nS	artat
povera	povera	k1gFnSc1	povera
<g/>
,	,	kIx,	,
nouveau	nouveau	k6eAd1	nouveau
réalisme	réalismus	k1gInSc5	réalismus
(	(	kIx(	(
<g/>
Erik	Erik	k1gMnSc1	Erik
Dietman	Dietman	k1gMnSc1	Dietman
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neoexpresionismus	neoexpresionismus	k1gInSc1	neoexpresionismus
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
literatura	literatura	k1gFnSc1	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
literatura	literatura	k1gFnSc1	literatura
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k9	jako
pokračování	pokračování	k1gNnSc4	pokračování
latinsky	latinsky	k6eAd1	latinsky
psané	psaný	k2eAgFnSc2d1	psaná
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc7d3	nejstarší
písemnou	písemný	k2eAgFnSc7d1	písemná
památkou	památka	k1gFnSc7	památka
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
Štrasburské	štrasburský	k2eAgFnSc2d1	Štrasburská
přísahy	přísaha	k1gFnSc2	přísaha
z	z	k7c2	z
roku	rok	k1gInSc2	rok
842	[number]	k4	842
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dějin	dějiny	k1gFnPc2	dějiny
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
francouzská	francouzský	k2eAgFnSc1d1	francouzská
literatura	literatura	k1gFnSc1	literatura
bouřlivý	bouřlivý	k2eAgInSc1d1	bouřlivý
rozvoj	rozvoj	k1gInSc4	rozvoj
a	a	k8xC	a
určovala	určovat	k5eAaImAgFnS	určovat
po	po	k7c4	po
mnoho	mnoho	k4c4	mnoho
století	století	k1gNnPc2	století
světový	světový	k2eAgInSc1d1	světový
trend	trend	k1gInSc1	trend
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
literárních	literární	k2eAgInPc2d1	literární
směrů	směr	k1gInPc2	směr
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
(	(	kIx(	(
<g/>
např.	např.	kA	např.
dadaismus	dadaismus	k1gInSc1	dadaismus
<g/>
,	,	kIx,	,
kubismus	kubismus	k1gInSc1	kubismus
<g/>
,	,	kIx,	,
surrealismus	surrealismus	k1gInSc1	surrealismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
uvolněnějšímu	uvolněný	k2eAgNnSc3d2	uvolněnější
prostředí	prostředí	k1gNnSc3	prostředí
zde	zde	k6eAd1	zde
mohlo	moct	k5eAaImAgNnS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
spousta	spousta	k6eAd1	spousta
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
byla	být	k5eAaImAgNnP	být
jinde	jinde	k6eAd1	jinde
zavrhována	zavrhovat	k5eAaImNgNnP	zavrhovat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
USA	USA	kA	USA
díla	dílo	k1gNnPc4	dílo
Henryho	Henry	k1gMnSc2	Henry
Millera	Miller	k1gMnSc2	Miller
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
vliv	vliv	k1gInSc4	vliv
nezapře	zapřít	k5eNaPmIp3nS	zapřít
ani	ani	k8xC	ani
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgMnPc4d3	nejznámější
autory	autor	k1gMnPc4	autor
patří	patřit	k5eAaImIp3nS	patřit
:	:	kIx,	:
François	François	k1gFnSc1	François
Villon	Villon	k1gInSc1	Villon
<g/>
,	,	kIx,	,
Jules	Jules	k1gInSc1	Jules
Verne	Vern	k1gInSc5	Vern
<g/>
,	,	kIx,	,
François	François	k1gFnSc1	François
Rabelais	Rabelais	k1gFnSc1	Rabelais
<g/>
,	,	kIx,	,
Michel	Michel	k1gMnSc1	Michel
de	de	k?	de
Montaigne	Montaign	k1gMnSc5	Montaign
<g/>
,	,	kIx,	,
Pierre	Pierr	k1gMnSc5	Pierr
Corneille	Corneill	k1gMnSc5	Corneill
<g/>
,	,	kIx,	,
Moliè	Moliè	k1gMnSc5	Moliè
<g/>
,	,	kIx,	,
Voltaire	Voltair	k1gMnSc5	Voltair
<g/>
,	,	kIx,	,
Victor	Victor	k1gMnSc1	Victor
Hugo	Hugo	k1gMnSc1	Hugo
<g/>
,	,	kIx,	,
Stendhal	Stendhal	k1gMnSc1	Stendhal
<g/>
,	,	kIx,	,
Gustav	Gustav	k1gMnSc1	Gustav
Flaubert	Flaubert	k1gMnSc1	Flaubert
<g/>
,	,	kIx,	,
Honoré	Honorý	k2eAgFnSc2d1	Honorý
de	de	k?	de
Balzac	Balzac	k1gMnSc1	Balzac
<g/>
,	,	kIx,	,
Romain	Romain	k1gMnSc1	Romain
Rolland	Rolland	k1gMnSc1	Rolland
<g/>
,	,	kIx,	,
Antoine	Antoin	k1gInSc5	Antoin
de	de	k?	de
Saint-Exupéry	Saint-Exupéra	k1gFnPc1	Saint-Exupéra
<g/>
,	,	kIx,	,
Jean-Paul	Jean-Paul	k1gInSc1	Jean-Paul
Sartre	Sartr	k1gInSc5	Sartr
<g/>
,	,	kIx,	,
Albert	Albert	k1gMnSc1	Albert
Camus	Camus	k1gMnSc1	Camus
<g/>
,	,	kIx,	,
Markýz	markýz	k1gMnSc1	markýz
de	de	k?	de
Sade	sad	k1gInSc5	sad
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
hudba	hudba	k1gFnSc1	hudba
a	a	k8xC	a
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
opera	opera	k1gFnSc1	opera
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
byla	být	k5eAaImAgFnS	být
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
centrum	centrum	k1gNnSc4	centrum
evropského	evropský	k2eAgNnSc2d1	Evropské
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
chlubí	chlubit	k5eAaImIp3nS	chlubit
širokým	široký	k2eAgNnSc7d1	široké
spektrem	spektrum	k1gNnSc7	spektrum
tradiční	tradiční	k2eAgFnSc2d1	tradiční
lidové	lidový	k2eAgFnSc2d1	lidová
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
hudbou	hudba	k1gFnSc7	hudba
přinesenou	přinesený	k2eAgFnSc7d1	přinesená
imigranty	imigrant	k1gMnPc4	imigrant
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
<g/>
[	[	kIx(	[
<g/>
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
několik	několik	k4yIc1	několik
legendárních	legendární	k2eAgMnPc2d1	legendární
skladatelů	skladatel	k1gMnPc2	skladatel
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
jako	jako	k8xC	jako
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
řada	řada	k1gFnSc1	řada
umělců	umělec	k1gMnPc2	umělec
hrajících	hrající	k2eAgMnPc2d1	hrající
rock	rock	k1gInSc4	rock
<g/>
,	,	kIx,	,
hip	hip	k0	hip
hop	hop	k0	hop
<g/>
,	,	kIx,	,
techno	techen	k2eAgNnSc1d1	techno
<g/>
,	,	kIx,	,
funk	funk	k1gInSc1	funk
a	a	k8xC	a
pop	pop	k1gInSc1	pop
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
se	se	k3xPyFc4	se
také	také	k9	také
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
zemi	zem	k1gFnSc4	zem
klarinetu	klarinet	k1gInSc2	klarinet
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
kuchyně	kuchyně	k1gFnSc1	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
kuchyně	kuchyně	k1gFnSc1	kuchyně
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
svou	svůj	k3xOyFgFnSc7	svůj
velikou	veliký	k2eAgFnSc7d1	veliká
rozmanitostí	rozmanitost	k1gFnSc7	rozmanitost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejvíce	hodně	k6eAd3	hodně
kultivovaných	kultivovaný	k2eAgInPc2d1	kultivovaný
a	a	k8xC	a
elegantních	elegantní	k2eAgInPc2d1	elegantní
stylů	styl	k1gInPc2	styl
vaření	vaření	k1gNnSc2	vaření
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
světových	světový	k2eAgMnPc2d1	světový
šéfkuchařů	šéfkuchař	k1gMnPc2	šéfkuchař
jako	jako	k8xC	jako
Taillevent	Taillevent	k1gInSc1	Taillevent
<g/>
,	,	kIx,	,
La	la	k0	la
Varenne	Varenn	k1gMnSc5	Varenn
<g/>
,	,	kIx,	,
Carê	Carê	k1gFnPc2	Carê
<g/>
,	,	kIx,	,
Escoffier	Escoffira	k1gFnPc2	Escoffira
nebo	nebo	k8xC	nebo
Bocuse	Bocuse	k1gFnSc2	Bocuse
byli	být	k5eAaImAgMnP	být
mistry	mistr	k1gMnPc4	mistr
této	tento	k3xDgFnSc2	tento
kuchyně	kuchyně	k1gFnSc2	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
francouzské	francouzský	k2eAgFnPc1d1	francouzská
způsoby	způsoba	k1gFnPc1	způsoba
vaření	vaření	k1gNnSc2	vaření
ovlivnily	ovlivnit	k5eAaPmAgFnP	ovlivnit
bezmála	bezmála	k6eAd1	bezmála
celou	celý	k2eAgFnSc4d1	celá
Západní	západní	k2eAgFnSc4d1	západní
Evropu	Evropa	k1gFnSc4	Evropa
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
kulinářských	kulinářský	k2eAgFnPc2d1	kulinářská
škol	škola	k1gFnPc2	škola
užívá	užívat	k5eAaImIp3nS	užívat
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
kuchyni	kuchyně	k1gFnSc4	kuchyně
jako	jako	k8xS	jako
základ	základ	k1gInSc1	základ
všech	všecek	k3xTgFnPc2	všecek
ostatních	ostatní	k2eAgFnPc2d1	ostatní
forem	forma	k1gFnPc2	forma
evropského	evropský	k2eAgNnSc2d1	Evropské
vaření	vaření	k1gNnSc2	vaření
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
Francie	Francie	k1gFnSc2	Francie
trpí	trpět	k5eAaImIp3nP	trpět
na	na	k7c6	na
hemeroidy	hemeroidy	k?	hemeroidy
nejvíce	nejvíce	k6eAd1	nejvíce
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Nejpopulárnějším	populární	k2eAgInSc7d3	nejpopulárnější
sportem	sport	k1gInSc7	sport
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
je	být	k5eAaImIp3nS	být
fotbal	fotbal	k1gInSc4	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
populárními	populární	k2eAgInPc7d1	populární
sporty	sport	k1gInPc7	sport
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
jsou	být	k5eAaImIp3nP	být
cyklistika	cyklistika	k1gFnSc1	cyklistika
(	(	kIx(	(
<g/>
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
pořádá	pořádat	k5eAaImIp3nS	pořádat
světoznámá	světoznámý	k2eAgFnSc1d1	světoznámá
Tour	Tour	k1gInSc1	Tour
de	de	k?	de
France	Franc	k1gMnSc2	Franc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ragby	ragby	k1gNnSc1	ragby
<g/>
,	,	kIx,	,
lehká	lehký	k2eAgFnSc1d1	lehká
atletika	atletika	k1gFnSc1	atletika
<g/>
,	,	kIx,	,
automobilový	automobilový	k2eAgInSc1d1	automobilový
sport	sport	k1gInSc1	sport
(	(	kIx(	(
<g/>
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Francie	Francie	k1gFnSc2	Francie
v	v	k7c4	v
Magny-Cours	Magny-Cours	k1gInSc4	Magny-Cours
<g/>
,	,	kIx,	,
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
Le	Le	k1gFnPc2	Le
Mans	Mans	k1gInSc1	Mans
<g/>
)	)	kIx)	)
a	a	k8xC	a
tenis	tenis	k1gInSc4	tenis
(	(	kIx(	(
<g/>
French	French	k1gMnSc1	French
Open	Open	k1gMnSc1	Open
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejhranějším	hraný	k2eAgInSc7d3	nejhranější
sportem	sport	k1gInSc7	sport
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
je	být	k5eAaImIp3nS	být
Pétanque	Pétanque	k1gFnSc1	Pétanque
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgFnPc1d1	různá
formy	forma	k1gFnPc1	forma
Pétanque	Pétanque	k1gInSc1	Pétanque
jsou	být	k5eAaImIp3nP	být
hrány	hrát	k5eAaImNgInP	hrát
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
přibližně	přibližně	k6eAd1	přibližně
17	[number]	k4	17
miliony	milion	k4xCgInPc1	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Federation	Federation	k1gInSc1	Federation
Française	Française	k1gFnSc2	Française
de	de	k?	de
Pétanque	Pétanqu	k1gFnSc2	Pétanqu
et	et	k?	et
Jeu	Jeu	k1gMnSc1	Jeu
Provençal	Provençal	k1gMnSc1	Provençal
(	(	kIx(	(
<g/>
ffpjp	ffpjp	k1gMnSc1	ffpjp
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dohlíží	dohlížet	k5eAaImIp3nS	dohlížet
na	na	k7c4	na
Pétanque	Pétanque	k1gFnSc4	Pétanque
<g/>
,	,	kIx,	,
eviduje	evidovat	k5eAaImIp3nS	evidovat
480	[number]	k4	480
000	[number]	k4	000
licencovaných	licencovaný	k2eAgMnPc2d1	licencovaný
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
známou	známý	k2eAgFnSc7d1	známá
hrou	hra	k1gFnSc7	hra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
je	být	k5eAaImIp3nS	být
stolní	stolní	k2eAgInSc1d1	stolní
fotbal	fotbal	k1gInSc1	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Francouzi	Francouz	k1gMnPc7	Francouz
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgMnSc1d1	populární
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
dokazují	dokazovat	k5eAaImIp3nP	dokazovat
četnými	četný	k2eAgNnPc7d1	četné
vítězstvími	vítězství	k1gNnPc7	vítězství
na	na	k7c6	na
světových	světový	k2eAgInPc6d1	světový
šampionátech	šampionát	k1gInPc6	šampionát
<g/>
.	.	kIx.	.
</s>
<s>
Francouzi	Francouz	k1gMnPc1	Francouz
několikrát	několikrát	k6eAd1	několikrát
pořádali	pořádat	k5eAaImAgMnP	pořádat
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
<g/>
:	:	kIx,	:
letní	letní	k2eAgInPc4d1	letní
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1900	[number]	k4	1900
a	a	k8xC	a
1924	[number]	k4	1924
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
zimní	zimní	k2eAgFnSc1d1	zimní
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
v	v	k7c6	v
Chamonix	Chamonix	k1gNnSc6	Chamonix
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
v	v	k7c4	v
Grenoble	Grenoble	k1gInSc4	Grenoble
a	a	k8xC	a
1992	[number]	k4	1992
v	v	k7c6	v
Albertville	Albertvilla	k1gFnSc6	Albertvilla
<g/>
.	.	kIx.	.
</s>
