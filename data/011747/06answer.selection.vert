<s>
Nyckelharpa	Nyckelharpa	k1gFnSc1	Nyckelharpa
je	být	k5eAaImIp3nS	být
smyčcový	smyčcový	k2eAgInSc4d1	smyčcový
hudební	hudební	k2eAgInSc4d1	hudební
nástroj	nástroj	k1gInSc4	nástroj
s	s	k7c7	s
klapkami	klapka	k1gFnPc7	klapka
zvanými	zvaný	k2eAgFnPc7d1	zvaná
nycklar	nycklara	k1gFnPc2	nycklara
či	či	k8xC	či
knavrar	knavrara	k1gFnPc2	knavrara
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
svým	svůj	k3xOyFgNnSc7	svůj
stisknutím	stisknutí	k1gNnSc7	stisknutí
zkrátí	zkrátit	k5eAaPmIp3nS	zkrátit
strunu	struna	k1gFnSc4	struna
na	na	k7c4	na
požadovanou	požadovaný	k2eAgFnSc4d1	požadovaná
tónovou	tónový	k2eAgFnSc4d1	tónová
výšku	výška	k1gFnSc4	výška
<g/>
.	.	kIx.	.
</s>
