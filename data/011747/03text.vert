<p>
<s>
Nyckelharpa	Nyckelharpa	k1gFnSc1	Nyckelharpa
je	být	k5eAaImIp3nS	být
smyčcový	smyčcový	k2eAgInSc4d1	smyčcový
hudební	hudební	k2eAgInSc4d1	hudební
nástroj	nástroj	k1gInSc4	nástroj
s	s	k7c7	s
klapkami	klapka	k1gFnPc7	klapka
zvanými	zvaný	k2eAgFnPc7d1	zvaná
nycklar	nycklara	k1gFnPc2	nycklara
či	či	k8xC	či
knavrar	knavrara	k1gFnPc2	knavrara
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
svým	svůj	k3xOyFgNnSc7	svůj
stisknutím	stisknutí	k1gNnSc7	stisknutí
zkrátí	zkrátit	k5eAaPmIp3nS	zkrátit
strunu	struna	k1gFnSc4	struna
na	na	k7c4	na
požadovanou	požadovaný	k2eAgFnSc4d1	požadovaná
tónovou	tónový	k2eAgFnSc4d1	tónová
výšku	výška	k1gFnSc4	výška
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
zvuku	zvuk	k1gInSc2	zvuk
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
smyčec	smyčec	k1gInSc1	smyčec
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
zvuk	zvuk	k1gInSc1	zvuk
nástroje	nástroj	k1gInSc2	nástroj
vzniká	vznikat	k5eAaImIp3nS	vznikat
rezonančními	rezonanční	k2eAgFnPc7d1	rezonanční
strunami	struna	k1gFnPc7	struna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
pod	pod	k7c7	pod
hracími	hrací	k2eAgFnPc7d1	hrací
strunami	struna	k1gFnPc7	struna
<g/>
.	.	kIx.	.
</s>
<s>
Nyckelharpa	Nyckelharpa	k1gFnSc1	Nyckelharpa
je	být	k5eAaImIp3nS	být
při	pře	k1gFnSc4	pře
hraní	hraní	k1gNnSc2	hraní
zavěšená	zavěšený	k2eAgFnSc1d1	zavěšená
na	na	k7c6	na
břichu	břich	k1gInSc6	břich
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
kytara	kytara	k1gFnSc1	kytara
a	a	k8xC	a
smyčcem	smyčec	k1gInSc7	smyčec
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
vertikálně	vertikálně	k6eAd1	vertikálně
proti	proti	k7c3	proti
hráčově	hráčův	k2eAgInSc6d1	hráčův
břichu	břich	k1gInSc6	břich
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nyckelharpa	Nyckelharpa	k1gFnSc1	Nyckelharpa
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
typických	typický	k2eAgInPc2d1	typický
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
švédské	švédský	k2eAgFnSc2d1	švédská
lidové	lidový	k2eAgFnSc2d1	lidová
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
pevné	pevný	k2eAgNnSc4d1	pevné
postavení	postavení	k1gNnSc4	postavení
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Uppsale	Uppsala	k1gFnSc6	Uppsala
a	a	k8xC	a
severním	severní	k2eAgInSc6d1	severní
Upplandu	Uppland	k1gInSc6	Uppland
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
do	do	k7c2	do
celého	celý	k2eAgNnSc2d1	celé
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
německých	německý	k2eAgInPc6d1	německý
slovnících	slovník	k1gInPc6	slovník
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1529	[number]	k4	1529
a	a	k8xC	a
1620	[number]	k4	1620
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
vyobrazení	vyobrazení	k1gNnSc2	vyobrazení
nyckelharpy	nyckelharpa	k1gFnSc2	nyckelharpa
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
starší	starý	k2eAgNnSc4d2	starší
vyobrazení	vyobrazení	k1gNnSc4	vyobrazení
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
dílech	díl	k1gInPc6	díl
švédského	švédský	k2eAgMnSc2d1	švédský
a	a	k8xC	a
dánského	dánský	k2eAgNnSc2d1	dánské
církevního	církevní	k2eAgNnSc2d1	církevní
umění	umění	k1gNnSc2	umění
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1460	[number]	k4	1460
až	až	k9	až
1525	[number]	k4	1525
a	a	k8xC	a
1565	[number]	k4	1565
<g/>
.	.	kIx.	.
</s>
<s>
Jaký	jaký	k3yQgInSc1	jaký
hudební	hudební	k2eAgInSc1d1	hudební
nástroj	nástroj	k1gInSc1	nástroj
drží	držet	k5eAaImIp3nS	držet
hudebník	hudebník	k1gMnSc1	hudebník
vyobrazený	vyobrazený	k2eAgMnSc1d1	vyobrazený
v	v	k7c6	v
portálu	portál	k1gInSc6	portál
kostela	kostel	k1gInSc2	kostel
Källunge	Källung	k1gFnSc2	Källung
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Gotland	Gotlanda	k1gFnPc2	Gotlanda
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
nyckelharpa	nyckelharpa	k1gFnSc1	nyckelharpa
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
má	mít	k5eAaImIp3nS	mít
s	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
západoevropský	západoevropský	k2eAgInSc1d1	západoevropský
původ	původ	k1gInSc1	původ
<g/>
.	.	kIx.	.
</s>
<s>
Žádná	žádný	k3yNgFnSc1	žádný
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
reprodukcí	reprodukce	k1gFnPc2	reprodukce
nezobrazuje	zobrazovat	k5eNaImIp3nS	zobrazovat
tradiční	tradiční	k2eAgFnSc4d1	tradiční
švédskou	švédský	k2eAgFnSc4d1	švédská
nyckelharpu	nyckelharpa	k1gFnSc4	nyckelharpa
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
takový	takový	k3xDgInSc4	takový
obrázek	obrázek	k1gInSc1	obrázek
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
doložen	doložit	k5eAaPmNgInS	doložit
až	až	k9	až
na	na	k7c6	na
konci	konec	k1gInSc6	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
odborníci	odborník	k1gMnPc1	odborník
domnívali	domnívat	k5eAaImAgMnP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nyckelharpa	nyckelharpa	k1gFnSc1	nyckelharpa
do	do	k7c2	do
Švédska	Švédsko	k1gNnSc2	Švédsko
dostala	dostat	k5eAaPmAgFnS	dostat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Valony	Valon	k1gMnPc7	Valon
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
do	do	k7c2	do
Švédska	Švédsko	k1gNnSc2	Švédsko
nedostali	dostat	k5eNaPmAgMnP	dostat
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
a	a	k8xC	a
nástroj	nástroj	k1gInSc1	nástroj
není	být	k5eNaImIp3nS	být
doložen	doložen	k2eAgMnSc1d1	doložen
ani	ani	k8xC	ani
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
domovské	domovský	k2eAgFnSc6d1	domovská
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
16	[number]	k4	16
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
nyckelharpa	nyckelharpa	k1gFnSc1	nyckelharpa
stala	stát	k5eAaPmAgFnS	stát
lidovým	lidový	k2eAgInSc7d1	lidový
hudebním	hudební	k2eAgInSc7d1	hudební
nástrojem	nástroj	k1gInSc7	nástroj
v	v	k7c6	v
Upplandu	Uppland	k1gInSc6	Uppland
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
i	i	k9	i
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
oblastí	oblast	k1gFnPc2	oblast
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Dalarna	Dalarna	k1gFnSc1	Dalarna
<g/>
,	,	kIx,	,
Hälsingland	Hälsingland	k1gInSc1	Hälsingland
a	a	k8xC	a
Ǻ	Ǻ	k?	Ǻ
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
tradičně	tradičně	k6eAd1	tradičně
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
oblastech	oblast	k1gFnPc6	oblast
hrálo	hrát	k5eAaImAgNnS	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Švédský	švédský	k2eAgMnSc1d1	švédský
bard	bard	k1gMnSc1	bard
Carl	Carl	k1gMnSc1	Carl
Michael	Michael	k1gMnSc1	Michael
Bellman	Bellman	k1gMnSc1	Bellman
udává	udávat	k5eAaImIp3nS	udávat
používání	používání	k1gNnSc4	používání
nástroje	nástroj	k1gInSc2	nástroj
ve	v	k7c6	v
stockholmksých	stockholmksý	k2eAgInPc6d1	stockholmksý
hostincích	hostinec	k1gInPc6	hostinec
na	na	k7c6	na
konci	konec	k1gInSc6	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
tradiční	tradiční	k2eAgInSc4d1	tradiční
hudební	hudební	k2eAgInSc4d1	hudební
nástroj	nástroj	k1gInSc4	nástroj
byla	být	k5eAaImAgFnS	být
"	"	kIx"	"
<g/>
gammelharpa	gammelharpa	k1gFnSc1	gammelharpa
<g/>
"	"	kIx"	"
vnímána	vnímán	k2eAgNnPc1d1	vnímáno
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Uppland	Upplanda	k1gFnPc2	Upplanda
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
relativně	relativně	k6eAd1	relativně
velkém	velký	k2eAgInSc6d1	velký
rozsahu	rozsah	k1gInSc6	rozsah
až	až	k9	až
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgFnPc1d2	starší
varianty	varianta	k1gFnPc1	varianta
nyckelharpy	nyckelharpa	k1gFnSc2	nyckelharpa
byly	být	k5eAaImAgFnP	být
rychle	rychle	k6eAd1	rychle
nahrazeny	nahradit	k5eAaPmNgFnP	nahradit
moderním	moderní	k2eAgInSc7d1	moderní
chromatickým	chromatický	k2eAgInSc7d1	chromatický
systémem	systém	k1gInSc7	systém
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vynalezl	vynaleznout	k5eAaPmAgInS	vynaleznout
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
August	August	k1gMnSc1	August
Bohlin	Bohlin	k1gInSc1	Bohlin
a	a	k8xC	a
většího	veliký	k2eAgInSc2d2	veliký
významu	význam	k1gInSc2	význam
nabyl	nabýt	k5eAaPmAgInS	nabýt
během	během	k7c2	během
vlny	vlna	k1gFnSc2	vlna
lidové	lidový	k2eAgFnSc2d1	lidová
hudby	hudba	k1gFnSc2	hudba
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
dochovaná	dochovaný	k2eAgFnSc1d1	dochovaná
nyckelharpa	nyckelharpa	k1gFnSc1	nyckelharpa
neměla	mít	k5eNaImAgFnS	mít
zřejmě	zřejmě	k6eAd1	zřejmě
rezonanční	rezonanční	k2eAgFnPc4d1	rezonanční
struny	struna	k1gFnPc4	struna
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
do	do	k7c2	do
Švédska	Švédsko	k1gNnSc2	Švédsko
dostaly	dostat	k5eAaPmAgFnP	dostat
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
ještě	ještě	k6eAd1	ještě
i	i	k8xC	i
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
nyckelharpa	nyckelharpa	k1gFnSc1	nyckelharpa
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
zavěšuje	zavěšovat	k5eAaImIp3nS	zavěšovat
struníkem	struník	k1gInSc7	struník
obráceným	obrácený	k2eAgInSc7d1	obrácený
nahoru	nahoru	k6eAd1	nahoru
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
než	než	k8xS	než
zde	zde	k6eAd1	zde
na	na	k7c6	na
obrázku	obrázek	k1gInSc6	obrázek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
dochovaná	dochovaný	k2eAgFnSc1d1	dochovaná
nyckelharpa	nyckelharpa	k1gFnSc1	nyckelharpa
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
tzv.	tzv.	kA	tzv.
Moraharpan	Moraharpana	k1gFnPc2	Moraharpana
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
té	ten	k3xDgFnSc6	ten
je	být	k5eAaImIp3nS	být
uveden	uveden	k2eAgInSc1d1	uveden
rok	rok	k1gInSc1	rok
1526	[number]	k4	1526
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podle	podle	k7c2	podle
historických	historický	k2eAgInPc2d1	historický
pramenů	pramen	k1gInPc2	pramen
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
podobného	podobný	k2eAgInSc2d1	podobný
nástroje	nástroj	k1gInSc2	nástroj
z	z	k7c2	z
Älvdalenu	Älvdalen	k2eAgFnSc4d1	Älvdalen
<g/>
,	,	kIx,	,
pravděpodobnější	pravděpodobný	k2eAgFnSc1d2	pravděpodobnější
doba	doba	k1gFnSc1	doba
zhotovení	zhotovení	k1gNnSc2	zhotovení
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1680	[number]	k4	1680
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Interpreti	interpret	k1gMnPc5	interpret
==	==	k?	==
</s>
</p>
<p>
<s>
K	k	k7c3	k
nejznámějším	známý	k2eAgMnPc3d3	nejznámější
interpretům	interpret	k1gMnPc3	interpret
hrajícím	hrající	k2eAgNnPc3d1	hrající
na	na	k7c6	na
nyckelharpu	nyckelharp	k1gInSc6	nyckelharp
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
Byss-Calle	Byss-Calle	k1gFnSc1	Byss-Calle
a	a	k8xC	a
Eric	Eric	k1gFnSc1	Eric
Sahlström	Sahlströma	k1gFnPc2	Sahlströma
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgMnPc3d1	další
hudebníkům	hudebník	k1gMnPc3	hudebník
patří	patřit	k5eAaImIp3nS	patřit
August	August	k1gMnSc1	August
Bohlin	Bohlin	k1gInSc1	Bohlin
<g/>
,	,	kIx,	,
Ceylon	Ceylon	k1gInSc1	Ceylon
Wallin	Wallin	k1gInSc1	Wallin
a	a	k8xC	a
Hasse	Hasse	k1gFnSc1	Hasse
Gille	Gille	k1gFnSc1	Gille
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
švédská	švédský	k2eAgFnSc1d1	švédská
populární	populární	k2eAgFnSc1d1	populární
hudba	hudba	k1gFnSc1	hudba
použila	použít	k5eAaPmAgFnS	použít
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
zvuk	zvuk	k1gInSc4	zvuk
nyckelharpy	nyckelharpa	k1gFnSc2	nyckelharpa
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
skladbách	skladba	k1gFnPc6	skladba
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Hedningarna	Hedningarna	k1gFnSc1	Hedningarna
<g/>
,	,	kIx,	,
Ǻ	Ǻ	k?	Ǻ
Jinder	Jindra	k1gFnPc2	Jindra
<g/>
,	,	kIx,	,
Väsen	Väsna	k1gFnPc2	Väsna
a	a	k8xC	a
Nordman	Nordman	k1gMnSc1	Nordman
<g/>
.	.	kIx.	.
</s>
<s>
Experimentátorem	experimentátor	k1gMnSc7	experimentátor
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
typy	typ	k1gInPc7	typ
nyckelharpy	nyckelharpa	k1gFnSc2	nyckelharpa
je	být	k5eAaImIp3nS	být
Johan	Johan	k1gMnSc1	Johan
Hedin	Hedin	k2eAgMnSc1d1	Hedin
a	a	k8xC	a
Marco	Marco	k6eAd1	Marco
Ambrosini	Ambrosin	k2eAgMnPc1d1	Ambrosin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
celosvětově	celosvětově	k6eAd1	celosvětově
nejznámější	známý	k2eAgFnSc7d3	nejznámější
skladbou	skladba	k1gFnSc7	skladba
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
zazněla	zaznít	k5eAaPmAgFnS	zaznít
nyckelharpa	nyckelharpa	k1gFnSc1	nyckelharpa
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
píseň	píseň	k1gFnSc4	píseň
Dum	duma	k1gFnPc2	duma
dum	duma	k1gFnPc2	duma
diddle	diddle	k6eAd1	diddle
z	z	k7c2	z
desky	deska	k1gFnSc2	deska
Arrival	Arrival	k1gFnSc2	Arrival
od	od	k7c2	od
hudební	hudební	k2eAgFnSc2d1	hudební
skupiny	skupina	k1gFnSc2	skupina
ABBA	ABBA	kA	ABBA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Nyckelharpa	Nyckelharp	k1gMnSc2	Nyckelharp
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
The	The	k?	The
American	Americana	k1gFnPc2	Americana
Nyckelharpa	Nyckelharpa	k1gFnSc1	Nyckelharpa
Association	Association	k1gInSc1	Association
</s>
</p>
<p>
<s>
Information	Information	k1gInSc1	Information
about	about	k1gInSc1	about
the	the	k?	the
nyckelharpa	nyckelharpa	k1gFnSc1	nyckelharpa
(	(	kIx(	(
<g/>
in	in	k?	in
German	German	k1gMnSc1	German
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
International	Internationat	k5eAaImAgInS	Internationat
Days	Days	k1gInSc1	Days
of	of	k?	of
the	the	k?	the
Nyckelharpa	Nyckelharpa	k1gFnSc1	Nyckelharpa
(	(	kIx(	(
<g/>
Germany	German	k1gInPc1	German
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
European	Europeana	k1gFnPc2	Europeana
Nyckelharpa	Nyckelharpa	k1gFnSc1	Nyckelharpa
Training	Training	k1gInSc1	Training
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Cadence	Cadenec	k1gInPc4	Cadenec
<g/>
"	"	kIx"	"
-	-	kIx~	-
International	International	k1gFnSc1	International
cooperation	cooperation	k1gInSc1	cooperation
in	in	k?	in
the	the	k?	the
teaching	teaching	k1gInSc1	teaching
of	of	k?	of
the	the	k?	the
nyckelharpa	nyckelharpa	k1gFnSc1	nyckelharpa
<g/>
,	,	kIx,	,
supported	supported	k1gMnSc1	supported
by	by	k9	by
the	the	k?	the
European	European	k1gInSc1	European
Commission	Commission	k1gInSc1	Commission
</s>
</p>
<p>
<s>
Eric	Eric	k6eAd1	Eric
Sahlström	Sahlström	k1gMnSc1	Sahlström
Institutet	Institutet	k1gMnSc1	Institutet
-	-	kIx~	-
National	National	k1gMnSc1	National
Folkmusic	Folkmusic	k1gMnSc1	Folkmusic
Institute	institut	k1gInSc5	institut
in	in	k?	in
Sweden	Swedna	k1gFnPc2	Swedna
</s>
</p>
<p>
<s>
Bau	Bau	k?	Bau
und	und	k?	und
Verkauf	Verkauf	k1gInSc1	Verkauf
von	von	k1gInSc1	von
Nyckelharpas	Nyckelharpas	k1gInSc1	Nyckelharpas
<g/>
,	,	kIx,	,
sowie	sowie	k1gFnSc1	sowie
Zubehör	Zubehöra	k1gFnPc2	Zubehöra
</s>
</p>
<p>
<s>
Europäische	Europäische	k6eAd1	Europäische
Nyckelharpa	Nyckelharpa	k1gFnSc1	Nyckelharpa
Fortbildung	Fortbildunga	k1gFnPc2	Fortbildunga
</s>
</p>
<p>
<s>
Internationale	Internationale	k6eAd1	Internationale
Nyckelharpa-Tage	Nyckelharpa-Tage	k1gFnSc1	Nyckelharpa-Tage
</s>
</p>
<p>
<s>
Eric	Eric	k6eAd1	Eric
Sahlström	Sahlström	k1gMnSc1	Sahlström
Institutet	Institutet	k1gMnSc1	Institutet
<g/>
,	,	kIx,	,
Schweden	Schweden	k2eAgMnSc1d1	Schweden
</s>
</p>
<p>
<s>
CADENCE-Lernpartnerschaft	CADENCE-Lernpartnerschaft	k1gInSc1	CADENCE-Lernpartnerschaft
<g/>
,	,	kIx,	,
internationale	internationale	k6eAd1	internationale
Kooperation	Kooperation	k1gInSc1	Kooperation
zum	zum	k?	zum
Nyckelharpa-Unterricht	Nyckelharpa-Unterricht	k1gInSc1	Nyckelharpa-Unterricht
</s>
</p>
