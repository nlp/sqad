<s>
Kometa	kometa	k1gFnSc1	kometa
<g/>
,	,	kIx,	,
zastarale	zastarale	k6eAd1	zastarale
vlasatice	vlasatice	k1gFnSc1	vlasatice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
malé	malý	k2eAgNnSc1d1	malé
těleso	těleso	k1gNnSc1	těleso
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
složené	složený	k2eAgNnSc1d1	složené
především	především	k9	především
z	z	k7c2	z
ledu	led	k1gInSc2	led
a	a	k8xC	a
prachu	prach	k1gInSc2	prach
a	a	k8xC	a
obíhající	obíhající	k2eAgFnSc7d1	obíhající
většinou	většina	k1gFnSc7	většina
po	po	k7c6	po
velice	velice	k6eAd1	velice
výstředné	výstředný	k2eAgFnSc6d1	výstředná
(	(	kIx(	(
<g/>
excentrické	excentrický	k2eAgFnSc6d1	excentrická
<g/>
)	)	kIx)	)
eliptické	eliptický	k2eAgFnSc6d1	eliptická
trajektorii	trajektorie	k1gFnSc6	trajektorie
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
