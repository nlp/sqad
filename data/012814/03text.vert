<p>
<s>
Aerodynamika	aerodynamika	k1gFnSc1	aerodynamika
(	(	kIx(	(
<g/>
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
ἀ	ἀ	k?	ἀ
aer	aero	k1gNnPc2	aero
–	–	k?	–
vzduch	vzduch	k1gInSc1	vzduch
+	+	kIx~	+
δ	δ	k?	δ
–	–	k?	–
dynamika	dynamika	k1gFnSc1	dynamika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
obor	obor	k1gInSc4	obor
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
speciálně	speciálně	k6eAd1	speciálně
mechaniky	mechanika	k1gFnPc1	mechanika
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgFnPc1d1	zabývající
se	s	k7c7	s
studiem	studio	k1gNnSc7	studio
pohybu	pohyb	k1gInSc2	pohyb
plynů	plyn	k1gInPc2	plyn
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
vzduchu	vzduch	k1gInSc3	vzduch
<g/>
)	)	kIx)	)
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
interakcí	interakce	k1gFnPc2	interakce
s	s	k7c7	s
pevnými	pevný	k2eAgInPc7d1	pevný
objekty	objekt	k1gInPc7	objekt
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
křídlo	křídlo	k1gNnSc1	křídlo
letadla	letadlo	k1gNnSc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnPc2	součást
aeromechanika	aeromechanika	k1gFnSc1	aeromechanika
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikální	fyzikální	k2eAgInPc1d1	fyzikální
principy	princip	k1gInPc1	princip
jsou	být	k5eAaImIp3nP	být
zcela	zcela	k6eAd1	zcela
identické	identický	k2eAgFnPc1d1	identická
i	i	k8xC	i
v	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
pevných	pevný	k2eAgNnPc2d1	pevné
těles	těleso	k1gNnPc2	těleso
v	v	k7c6	v
plynném	plynný	k2eAgNnSc6d1	plynné
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
principu	princip	k1gInSc6	princip
relativního	relativní	k2eAgInSc2d1	relativní
pohybu	pohyb	k1gInSc2	pohyb
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
provádí	provádět	k5eAaImIp3nS	provádět
analýza	analýza	k1gFnSc1	analýza
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
jevů	jev	k1gInPc2	jev
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
objekt	objekt	k1gInSc4	objekt
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
a	a	k8xC	a
proudí	proudit	k5eAaImIp3nS	proudit
kolem	kolem	k6eAd1	kolem
něj	on	k3xPp3gInSc4	on
vzduch	vzduch	k1gInSc4	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Výměna	výměna	k1gFnSc1	výměna
referenčního	referenční	k2eAgInSc2d1	referenční
stavu	stav	k1gInSc2	stav
je	být	k5eAaImIp3nS	být
aplikována	aplikovat	k5eAaBmNgFnS	aplikovat
v	v	k7c6	v
teoretické	teoretický	k2eAgFnSc6d1	teoretická
aerodynamice	aerodynamika	k1gFnSc6	aerodynamika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
základem	základ	k1gInSc7	základ
většiny	většina	k1gFnSc2	většina
experimentálních	experimentální	k2eAgFnPc2d1	experimentální
metod	metoda	k1gFnPc2	metoda
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
aerodynamického	aerodynamický	k2eAgInSc2d1	aerodynamický
tunelu	tunel	k1gInSc2	tunel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aerodynamika	aerodynamika	k1gFnSc1	aerodynamika
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgInSc4d3	veliký
význam	význam	k1gInSc4	význam
v	v	k7c6	v
letectví	letectví	k1gNnSc6	letectví
<g/>
,	,	kIx,	,
automobilismu	automobilismus	k1gInSc6	automobilismus
a	a	k8xC	a
architektuře	architektura	k1gFnSc6	architektura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aerodynamika	aerodynamika	k1gFnSc1	aerodynamika
je	být	k5eAaImIp3nS	být
podoblast	podoblast	k1gFnSc4	podoblast
dynamiky	dynamika	k1gFnSc2	dynamika
kapalin	kapalina	k1gFnPc2	kapalina
a	a	k8xC	a
dynamiky	dynamika	k1gFnSc2	dynamika
plynů	plyn	k1gInPc2	plyn
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
aspektů	aspekt	k1gInPc2	aspekt
teorie	teorie	k1gFnSc2	teorie
aerodynamiky	aerodynamika	k1gFnSc2	aerodynamika
mají	mít	k5eAaImIp3nP	mít
tyto	tento	k3xDgFnPc4	tento
oblasti	oblast	k1gFnPc4	oblast
společné	společný	k2eAgFnPc4d1	společná
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
aerodynamika	aerodynamika	k1gFnSc1	aerodynamika
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
používána	používán	k2eAgFnSc1d1	používána
jako	jako	k8xS	jako
synonymum	synonymum	k1gNnSc1	synonymum
pro	pro	k7c4	pro
dynamiku	dynamika	k1gFnSc4	dynamika
plynů	plyn	k1gInPc2	plyn
s	s	k7c7	s
tím	ten	k3xDgInSc7	ten
rozdílem	rozdíl	k1gInSc7	rozdíl
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
dynamika	dynamika	k1gFnSc1	dynamika
plynů	plyn	k1gInPc2	plyn
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
aplikována	aplikovat	k5eAaBmNgFnS	aplikovat
na	na	k7c4	na
studium	studium	k1gNnSc4	studium
pohybu	pohyb	k1gInSc2	pohyb
všech	všecek	k3xTgInPc2	všecek
plynů	plyn	k1gInPc2	plyn
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
omezena	omezit	k5eAaPmNgFnS	omezit
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
vzduch	vzduch	k1gInSc4	vzduch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Formální	formální	k2eAgFnSc1d1	formální
studie	studie	k1gFnSc1	studie
aerodynamiky	aerodynamika	k1gFnSc2	aerodynamika
v	v	k7c6	v
moderním	moderní	k2eAgNnSc6d1	moderní
pojetí	pojetí	k1gNnSc6	pojetí
začaly	začít	k5eAaPmAgFnP	začít
v	v	k7c4	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
pozorování	pozorování	k1gNnSc1	pozorování
základních	základní	k2eAgInPc2d1	základní
konceptů	koncept	k1gInPc2	koncept
aerodynamiky	aerodynamika	k1gFnSc2	aerodynamika
jako	jako	k8xC	jako
např.	např.	kA	např.
aerodynamického	aerodynamický	k2eAgInSc2d1	aerodynamický
odporu	odpor	k1gInSc2	odpor
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
zaznamenány	zaznamenat	k5eAaPmNgInP	zaznamenat
mnohem	mnohem	k6eAd1	mnohem
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k6eAd1	mnoho
z	z	k7c2	z
počátečního	počáteční	k2eAgNnSc2d1	počáteční
úsilí	úsilí	k1gNnSc2	úsilí
v	v	k7c6	v
aerodynamice	aerodynamika	k1gFnSc6	aerodynamika
se	se	k3xPyFc4	se
zaměřovalo	zaměřovat	k5eAaImAgNnS	zaměřovat
na	na	k7c4	na
dosažení	dosažení	k1gNnSc4	dosažení
letu	let	k1gInSc2	let
letadla	letadlo	k1gNnSc2	letadlo
těžšího	těžký	k2eAgNnSc2d2	těžší
než	než	k8xS	než
vzduch	vzduch	k1gInSc1	vzduch
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
demonstrován	demonstrovat	k5eAaBmNgInS	demonstrovat
bratry	bratr	k1gMnPc7	bratr
Wrighty	Wrighta	k1gFnSc2	Wrighta
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
využití	využití	k1gNnSc2	využití
aerodynamiky	aerodynamika	k1gFnSc2	aerodynamika
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
matematické	matematický	k2eAgFnSc2d1	matematická
analýzy	analýza	k1gFnSc2	analýza
<g/>
,	,	kIx,	,
empirických	empirický	k2eAgFnPc2d1	empirická
aproximací	aproximace	k1gFnPc2	aproximace
<g/>
,	,	kIx,	,
experimentů	experiment	k1gInPc2	experiment
v	v	k7c6	v
aerodynamických	aerodynamický	k2eAgInPc6d1	aerodynamický
tunelech	tunel	k1gInPc6	tunel
a	a	k8xC	a
počítačových	počítačový	k2eAgFnPc2d1	počítačová
simulací	simulace	k1gFnPc2	simulace
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
vědecký	vědecký	k2eAgInSc4d1	vědecký
základ	základ	k1gInSc4	základ
pro	pro	k7c4	pro
další	další	k2eAgInSc4d1	další
vývoj	vývoj	k1gInSc4	vývoj
letadel	letadlo	k1gNnPc2	letadlo
těžších	těžký	k2eAgNnPc2d2	těžší
než	než	k8xS	než
vzduch	vzduch	k1gInSc1	vzduch
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgFnPc2d1	další
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
směrování	směrování	k1gNnSc1	směrování
aerodynamiky	aerodynamika	k1gFnSc2	aerodynamika
se	se	k3xPyFc4	se
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
na	na	k7c4	na
otázky	otázka	k1gFnPc4	otázka
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
vlivu	vliv	k1gInSc2	vliv
stlačitelnosti	stlačitelnost	k1gFnSc2	stlačitelnost
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
turbulence	turbulence	k1gFnSc2	turbulence
a	a	k8xC	a
mezní	mezní	k2eAgFnSc2d1	mezní
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgInPc1d1	základní
pojmy	pojem	k1gInPc1	pojem
==	==	k?	==
</s>
</p>
<p>
<s>
Pochopení	pochopení	k1gNnSc4	pochopení
pohybu	pohyb	k1gInSc2	pohyb
vzduchu	vzduch	k1gInSc2	vzduch
kolem	kolem	k7c2	kolem
objektu	objekt	k1gInSc2	objekt
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
výpočet	výpočet	k1gInSc4	výpočet
sil	síla	k1gFnPc2	síla
a	a	k8xC	a
momentů	moment	k1gInPc2	moment
působících	působící	k2eAgInPc2d1	působící
na	na	k7c4	na
objekt	objekt	k1gInSc4	objekt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
aerodynamických	aerodynamický	k2eAgInPc6d1	aerodynamický
problémech	problém	k1gInPc6	problém
jsou	být	k5eAaImIp3nP	být
zkoumány	zkoumán	k2eAgFnPc1d1	zkoumána
síly	síla	k1gFnPc1	síla
právě	právě	k9	právě
základními	základní	k2eAgFnPc7d1	základní
silami	síla	k1gFnPc7	síla
letu	let	k1gInSc2	let
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Vztlak	vztlak	k1gInSc1	vztlak
</s>
</p>
<p>
<s>
Aerodynamický	aerodynamický	k2eAgInSc4d1	aerodynamický
odpor	odpor	k1gInSc4	odpor
</s>
</p>
<p>
<s>
Tah	tah	k1gInSc1	tah
</s>
</p>
<p>
<s>
TíhaZ	TíhaZ	k?	TíhaZ
nich	on	k3xPp3gFnPc2	on
jsou	být	k5eAaImIp3nP	být
vztlak	vztlak	k1gInSc1	vztlak
a	a	k8xC	a
odpor	odpor	k1gInSc1	odpor
aerodynamickými	aerodynamický	k2eAgFnPc7d1	aerodynamická
sílami	síla	k1gFnPc7	síla
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
síly	síla	k1gFnPc4	síla
vyvolané	vyvolaný	k2eAgFnPc4d1	vyvolaná
prouděním	proudění	k1gNnSc7	proudění
vzduchu	vzduch	k1gInSc2	vzduch
kolem	kolem	k7c2	kolem
pevného	pevný	k2eAgNnSc2d1	pevné
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Výpočet	výpočet	k1gInSc1	výpočet
těchto	tento	k3xDgFnPc2	tento
veličin	veličina	k1gFnPc2	veličina
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
předpokladu	předpoklad	k1gInSc6	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
pole	pole	k1gNnSc1	pole
proudění	proudění	k1gNnSc2	proudění
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
jako	jako	k9	jako
kontinuum	kontinuum	k1gNnSc1	kontinuum
<g/>
.	.	kIx.	.
</s>
<s>
Pole	pole	k1gFnSc1	pole
proudění	proudění	k1gNnSc2	proudění
kontinua	kontinuum	k1gNnSc2	kontinuum
je	být	k5eAaImIp3nS	být
charakterizováno	charakterizovat	k5eAaBmNgNnS	charakterizovat
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
jako	jako	k8xS	jako
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
,	,	kIx,	,
tlak	tlak	k1gInSc1	tlak
<g/>
,	,	kIx,	,
hustota	hustota	k1gFnSc1	hustota
a	a	k8xC	a
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
funkcí	funkce	k1gFnPc2	funkce
prostorové	prostorový	k2eAgFnSc2d1	prostorová
polohy	poloha	k1gFnSc2	poloha
a	a	k8xC	a
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
vlastnosti	vlastnost	k1gFnPc1	vlastnost
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
přímo	přímo	k6eAd1	přímo
nebo	nebo	k8xC	nebo
nepřímo	přímo	k6eNd1	přímo
měřené	měřený	k2eAgInPc1d1	měřený
v	v	k7c6	v
aerodynamických	aerodynamický	k2eAgInPc6d1	aerodynamický
experimentech	experiment	k1gInPc6	experiment
nebo	nebo	k8xC	nebo
počítány	počítat	k5eAaImNgFnP	počítat
z	z	k7c2	z
rovnic	rovnice	k1gFnPc2	rovnice
pro	pro	k7c4	pro
zachování	zachování	k1gNnSc4	zachování
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
hybnosti	hybnost	k1gFnSc2	hybnost
a	a	k8xC	a
energie	energie	k1gFnSc2	energie
proudu	proud	k1gInSc2	proud
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
<g/>
,	,	kIx,	,
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
,	,	kIx,	,
viskozita	viskozita	k1gFnSc1	viskozita
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
vlastnosti	vlastnost	k1gFnPc1	vlastnost
jsou	být	k5eAaImIp3nP	být
použity	použít	k5eAaPmNgFnP	použít
ke	k	k7c3	k
klasifikací	klasifikace	k1gFnPc2	klasifikace
proudového	proudový	k2eAgNnSc2d1	proudové
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Klasifikace	klasifikace	k1gFnSc1	klasifikace
proudění	proudění	k1gNnSc2	proudění
===	===	k?	===
</s>
</p>
<p>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
proudu	proud	k1gInSc2	proud
vzduchu	vzduch	k1gInSc2	vzduch
je	být	k5eAaImIp3nS	být
použita	použít	k5eAaPmNgFnS	použít
ke	k	k7c3	k
klasifikací	klasifikace	k1gFnPc2	klasifikace
proudění	proudění	k1gNnSc2	proudění
podle	podle	k7c2	podle
režimu	režim	k1gInSc2	režim
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podzvukové	podzvukový	k2eAgNnSc1d1	podzvukové
nebo	nebo	k8xC	nebo
subsonické	subsonický	k2eAgNnSc1d1	subsonické
proudění	proudění	k1gNnSc1	proudění
je	být	k5eAaImIp3nS	být
pole	pole	k1gNnSc4	pole
proudu	proud	k1gInSc2	proud
vzduchu	vzduch	k1gInSc2	vzduch
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
má	mít	k5eAaImIp3nS	mít
vzduch	vzduch	k1gInSc4	vzduch
podél	podél	k7c2	podél
celé	celý	k2eAgFnSc2d1	celá
zkoumané	zkoumaný	k2eAgFnSc2d1	zkoumaná
délky	délka	k1gFnSc2	délka
proudu	proud	k1gInSc2	proud
místní	místní	k2eAgFnSc4d1	místní
rychlost	rychlost	k1gFnSc4	rychlost
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc4	rychlost
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgFnSc1d1	místní
rychlost	rychlost	k1gFnSc1	rychlost
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc4	rychlost
proudu	proud	k1gInSc2	proud
tekutiny	tekutina	k1gFnSc2	tekutina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
měřena	měřit	k5eAaImNgFnS	měřit
v	v	k7c6	v
určitém	určitý	k2eAgNnSc6d1	určité
místě	místo	k1gNnSc6	místo
na	na	k7c6	na
obtékaném	obtékaný	k2eAgInSc6d1	obtékaný
objektu	objekt	k1gInSc6	objekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Transonické	Transonický	k2eAgNnSc1d1	Transonický
nebo	nebo	k8xC	nebo
okolozvukové	okolozvukový	k2eAgNnSc1d1	okolozvukový
proudění	proudění	k1gNnSc1	proudění
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pole	pole	k1gNnSc4	pole
s	s	k7c7	s
podzvukových	podzvukový	k2eAgFnPc2d1	podzvuková
i	i	k8xC	i
nadzvukovým	nadzvukový	k2eAgNnSc7d1	nadzvukové
prouděním	proudění	k1gNnSc7	proudění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nadzvukové	nadzvukový	k2eAgNnSc1d1	nadzvukové
nebo	nebo	k8xC	nebo
suprasonické	suprasonický	k2eAgNnSc1d1	suprasonický
proudění	proudění	k1gNnSc1	proudění
je	být	k5eAaImIp3nS	být
definováno	definovat	k5eAaBmNgNnS	definovat
jako	jako	k9	jako
proudění	proudění	k1gNnSc1	proudění
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc1	rychlost
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
rychlost	rychlost	k1gFnSc1	rychlost
zvuku	zvuk	k1gInSc2	zvuk
podél	podél	k7c2	podél
celého	celý	k2eAgInSc2d1	celý
toku	tok	k1gInSc2	tok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hypersonické	Hypersonický	k2eAgNnSc1d1	Hypersonický
proudění	proudění	k1gNnSc1	proudění
je	být	k5eAaImIp3nS	být
tok	tok	k1gInSc4	tok
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
jsou	být	k5eAaImIp3nP	být
rychlosti	rychlost	k1gFnPc1	rychlost
proudění	proudění	k1gNnSc2	proudění
mnohem	mnohem	k6eAd1	mnohem
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc4	rychlost
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Odborníci	odborník	k1gMnPc1	odborník
v	v	k7c6	v
aerodynamice	aerodynamika	k1gFnSc6	aerodynamika
se	se	k3xPyFc4	se
neshodli	shodnout	k5eNaPmAgMnP	shodnout
na	na	k7c6	na
přesné	přesný	k2eAgFnSc6d1	přesná
definici	definice	k1gFnSc6	definice
hypersonického	hypersonický	k2eAgNnSc2d1	hypersonický
proudění	proudění	k1gNnSc2	proudění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Stlačitelnost	stlačitelnost	k1gFnSc4	stlačitelnost
====	====	k?	====
</s>
</p>
<p>
<s>
Od	od	k7c2	od
stlačitelnosti	stlačitelnost	k1gFnSc2	stlačitelnost
proudění	proudění	k1gNnSc2	proudění
závisí	záviset	k5eAaImIp3nS	záviset
měnící	měnící	k2eAgFnSc1d1	měnící
se	se	k3xPyFc4	se
hustota	hustota	k1gFnSc1	hustota
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nastává	nastávat	k5eAaImIp3nS	nastávat
zmenšení	zmenšení	k1gNnSc4	zmenšení
objemu	objem	k1gInSc2	objem
vzduchu	vzduch	k1gInSc2	vzduch
při	při	k7c6	při
kompresi	komprese	k1gFnSc6	komprese
(	(	kIx(	(
<g/>
zvýšení	zvýšení	k1gNnSc4	zvýšení
tlaku	tlak	k1gInSc2	tlak
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
aerodynamice	aerodynamika	k1gFnSc6	aerodynamika
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
podzvukové	podzvukový	k2eAgNnSc1d1	podzvukové
proudění	proudění	k1gNnSc1	proudění
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
nestlačitelné	stlačitelný	k2eNgNnSc4d1	nestlačitelné
a	a	k8xC	a
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
hustota	hustota	k1gFnSc1	hustota
vzduchu	vzduch	k1gInSc2	vzduch
konstantní	konstantní	k2eAgFnSc1d1	konstantní
<g/>
.	.	kIx.	.
</s>
<s>
Částice	částice	k1gFnSc1	částice
vzduchu	vzduch	k1gInSc2	vzduch
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
v	v	k7c6	v
dráze	dráha	k1gFnSc6	dráha
stejných	stejný	k2eAgFnPc2d1	stejná
proudnic	proudnice	k1gFnPc2	proudnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Okolozvukové	Okolozvukový	k2eAgNnSc1d1	Okolozvukový
a	a	k8xC	a
nadzvukové	nadzvukový	k2eAgNnSc1d1	nadzvukové
proudění	proudění	k1gNnSc1	proudění
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
stlačitelné	stlačitelný	k2eAgNnSc4d1	stlačitelné
a	a	k8xC	a
zanedbání	zanedbání	k1gNnSc4	zanedbání
stlačitelnosti	stlačitelnost	k1gFnSc2	stlačitelnost
prostředí	prostředí	k1gNnSc2	prostředí
se	s	k7c7	s
změnou	změna	k1gFnSc7	změna
hustoty	hustota	k1gFnSc2	hustota
proudu	proud	k1gInSc2	proud
vzduchu	vzduch	k1gInSc2	vzduch
při	při	k7c6	při
provádění	provádění	k1gNnSc6	provádění
výpočtů	výpočet	k1gInPc2	výpočet
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
nepřesným	přesný	k2eNgInPc3d1	nepřesný
výsledkům	výsledek	k1gInPc3	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
stlačitelnosti	stlačitelnost	k1gFnSc2	stlačitelnost
se	se	k3xPyFc4	se
bere	brát	k5eAaImIp3nS	brát
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
od	od	k7c2	od
rychlostí	rychlost	k1gFnPc2	rychlost
vyšších	vysoký	k2eAgFnPc2d2	vyšší
než	než	k8xS	než
500	[number]	k4	500
-	-	kIx~	-
600	[number]	k4	600
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
</s>
</p>
<p>
<s>
====	====	k?	====
Viskozita	viskozita	k1gFnSc1	viskozita
====	====	k?	====
</s>
</p>
<p>
<s>
Viskozita	viskozita	k1gFnSc1	viskozita
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
vnitřním	vnitřní	k2eAgNnSc7d1	vnitřní
třením	tření	k1gNnSc7	tření
v	v	k7c6	v
proudícím	proudící	k2eAgInSc6d1	proudící
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
působí	působit	k5eAaImIp3nS	působit
proti	proti	k7c3	proti
směru	směr	k1gInSc2	směr
proudění	proudění	k1gNnSc2	proudění
částic	částice	k1gFnPc2	částice
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některým	některý	k3yIgNnPc3	některý
proudových	proudový	k2eAgNnPc6d1	proudové
polích	pole	k1gNnPc6	pole
jsou	být	k5eAaImIp3nP	být
vlivy	vliv	k1gInPc1	vliv
viskozity	viskozita	k1gFnSc2	viskozita
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgFnPc1d1	malá
a	a	k8xC	a
řešení	řešení	k1gNnSc1	řešení
výpočtů	výpočet	k1gInPc2	výpočet
je	on	k3xPp3gMnPc4	on
mohou	moct	k5eAaImIp3nP	moct
zanedbávat	zanedbávat	k5eAaImF	zanedbávat
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
aproximace	aproximace	k1gFnPc1	aproximace
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
neviskozní	viskozní	k2eNgInPc1d1	viskozní
proudy	proud	k1gInPc1	proud
<g/>
.	.	kIx.	.
</s>
<s>
Proudy	proud	k1gInPc4	proud
vzduchu	vzduch	k1gInSc2	vzduch
u	u	k7c2	u
kterých	který	k3yRgInPc2	který
není	být	k5eNaImIp3nS	být
zanedbaný	zanedbaný	k2eAgInSc1d1	zanedbaný
vliv	vliv	k1gInSc1	vliv
viskozity	viskozita	k1gFnSc2	viskozita
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
viskózní	viskózní	k2eAgInPc1d1	viskózní
proudy	proud	k1gInPc1	proud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Prostředí	prostředí	k1gNnSc3	prostředí
proudění	proudění	k1gNnSc3	proudění
====	====	k?	====
</s>
</p>
<p>
<s>
Nakonec	nakonec	k6eAd1	nakonec
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
aerodynamické	aerodynamický	k2eAgInPc1d1	aerodynamický
problémy	problém	k1gInPc1	problém
klasifikovány	klasifikovat	k5eAaImNgInP	klasifikovat
prostředím	prostředí	k1gNnSc7	prostředí
proudění	proudění	k1gNnSc1	proudění
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Vnější	vnější	k2eAgFnSc1d1	vnější
aerodynamika	aerodynamika	k1gFnSc1	aerodynamika
je	být	k5eAaImIp3nS	být
studiem	studio	k1gNnSc7	studio
proudění	proudění	k1gNnPc2	proudění
kolem	kolem	k7c2	kolem
pevných	pevný	k2eAgInPc2d1	pevný
objektů	objekt	k1gInPc2	objekt
různých	různý	k2eAgInPc2d1	různý
tvarů	tvar	k1gInPc2	tvar
(	(	kIx(	(
<g/>
např.	např.	kA	např.
okolo	okolo	k7c2	okolo
křídla	křídlo	k1gNnSc2	křídlo
letadla	letadlo	k1gNnSc2	letadlo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
aerodynamika	aerodynamika	k1gFnSc1	aerodynamika
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
studiem	studio	k1gNnSc7	studio
proudu	proud	k1gInSc2	proud
vzduchu	vzduch	k1gInSc2	vzduch
přes	přes	k7c4	přes
průchody	průchod	k1gInPc4	průchod
v	v	k7c6	v
pevných	pevný	k2eAgInPc6d1	pevný
objektech	objekt	k1gInPc6	objekt
(	(	kIx(	(
<g/>
např.	např.	kA	např.
skrz	skrz	k7c4	skrz
proudový	proudový	k2eAgInSc4d1	proudový
motor	motor	k1gInSc4	motor
letadla	letadlo	k1gNnSc2	letadlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Předpoklad	předpoklad	k1gInSc1	předpoklad
kontinua	kontinuum	k1gNnSc2	kontinuum
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
kapalin	kapalina	k1gFnPc2	kapalina
a	a	k8xC	a
pevných	pevný	k2eAgFnPc2d1	pevná
látek	látka	k1gFnPc2	látka
jsou	být	k5eAaImIp3nP	být
plyny	plyn	k1gInPc1	plyn
tvořeny	tvořit	k5eAaImNgInP	tvořit
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
molekulami	molekula	k1gFnPc7	molekula
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zabírají	zabírat	k5eAaImIp3nP	zabírat
pouze	pouze	k6eAd1	pouze
malou	malý	k2eAgFnSc4d1	malá
část	část	k1gFnSc4	část
objemu	objem	k1gInSc2	objem
tvořenou	tvořený	k2eAgFnSc4d1	tvořená
plynem	plyn	k1gInSc7	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
molekulární	molekulární	k2eAgFnSc6d1	molekulární
úrovni	úroveň	k1gFnSc6	úroveň
se	se	k3xPyFc4	se
proudící	proudící	k2eAgNnSc1d1	proudící
pole	pole	k1gNnSc1	pole
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
individuálních	individuální	k2eAgFnPc2d1	individuální
kolizí	kolize	k1gFnPc2	kolize
mezi	mezi	k7c7	mezi
molekulami	molekula	k1gFnPc7	molekula
plynu	plyn	k1gInSc2	plyn
a	a	k8xC	a
pevných	pevný	k2eAgInPc2d1	pevný
povrchů	povrch	k1gInPc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
aerodynamických	aerodynamický	k2eAgFnPc2d1	aerodynamická
aplikací	aplikace	k1gFnPc2	aplikace
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
tato	tento	k3xDgFnSc1	tento
diskrétní	diskrétní	k2eAgFnSc1d1	diskrétní
molekulární	molekulární	k2eAgFnSc1d1	molekulární
podstata	podstata	k1gFnSc1	podstata
plynů	plyn	k1gInPc2	plyn
zanedbaná	zanedbaný	k2eAgFnSc1d1	zanedbaná
a	a	k8xC	a
proudící	proudící	k2eAgNnSc1d1	proudící
pole	pole	k1gNnSc1	pole
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
kontinuum	kontinuum	k1gNnSc4	kontinuum
<g/>
.	.	kIx.	.
<	<	kIx(	<
<g/>
!	!	kIx.	!
</s>
<s>
-	-	kIx~	-
Tento	tento	k3xDgInSc1	tento
předpoklad	předpoklad	k1gInSc1	předpoklad
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
kdekoliv	kdekoliv	k6eAd1	kdekoliv
určit	určit	k5eAaPmF	určit
vlastnosti	vlastnost	k1gFnPc4	vlastnost
kapaliny	kapalina	k1gFnSc2	kapalina
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
proudění	proudění	k1gNnSc2	proudění
jako	jako	k8xC	jako
hustotu	hustota	k1gFnSc4	hustota
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gFnSc1	jeho
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
.	.	kIx.	.
-	-	kIx~	-
<g/>
>	>	kIx)	>
</s>
</p>
<p>
<s>
Platnost	platnost	k1gFnSc1	platnost
předpokladu	předpoklad	k1gInSc2	předpoklad
kontinua	kontinuum	k1gNnSc2	kontinuum
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
hustotě	hustota	k1gFnSc6	hustota
plynu	plyn	k1gInSc2	plyn
a	a	k8xC	a
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
aplikaci	aplikace	k1gFnSc4	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
předpoklad	předpoklad	k1gInSc1	předpoklad
kontinua	kontinuum	k1gNnSc2	kontinuum
platný	platný	k2eAgInSc1d1	platný
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
střední	střední	k2eAgFnSc1d1	střední
volná	volný	k2eAgFnSc1d1	volná
dráha	dráha	k1gFnSc1	dráha
o	o	k7c4	o
dost	dost	k6eAd1	dost
menší	malý	k2eAgMnPc4d2	menší
než	než	k8xS	než
délka	délka	k1gFnSc1	délka
rozsahu	rozsah	k1gInSc2	rozsah
konkrétní	konkrétní	k2eAgFnSc2d1	konkrétní
aplikace	aplikace	k1gFnSc2	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
mnoho	mnoho	k4c4	mnoho
aerodynamických	aerodynamický	k2eAgFnPc2d1	aerodynamická
aplikací	aplikace	k1gFnPc2	aplikace
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
letem	let	k1gInSc7	let
letadla	letadlo	k1gNnSc2	letadlo
v	v	k7c6	v
atmosférických	atmosférický	k2eAgFnPc6d1	atmosférická
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
střední	střední	k2eAgFnSc1d1	střední
volná	volný	k2eAgFnSc1d1	volná
délka	délka	k1gFnSc1	délka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
řádech	řád	k1gInPc6	řád
mikrometrů	mikrometr	k1gInPc2	mikrometr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
délková	délkový	k2eAgFnSc1d1	délková
míra	míra	k1gFnSc1	míra
letadla	letadlo	k1gNnSc2	letadlo
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
několika	několik	k4yIc2	několik
metrů	metr	k1gInPc2	metr
až	až	k6eAd1	až
do	do	k7c2	do
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
o	o	k7c4	o
dost	dost	k6eAd1	dost
větší	veliký	k2eAgFnSc4d2	veliký
délku	délka	k1gFnSc4	délka
než	než	k8xS	než
střední	střední	k2eAgFnSc1d1	střední
volná	volný	k2eAgFnSc1d1	volná
délka	délka	k1gFnSc1	délka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tyto	tento	k3xDgFnPc4	tento
aplikace	aplikace	k1gFnPc4	aplikace
platí	platit	k5eAaImIp3nS	platit
předpoklad	předpoklad	k1gInSc1	předpoklad
kontinua	kontinuum	k1gNnSc2	kontinuum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Předpoklad	předpoklad	k1gInSc1	předpoklad
kontinua	kontinuum	k1gNnSc2	kontinuum
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
opodstatněný	opodstatněný	k2eAgInSc4d1	opodstatněný
pro	pro	k7c4	pro
proudění	proudění	k1gNnSc4	proudění
s	s	k7c7	s
extrémně	extrémně	k6eAd1	extrémně
nízkou	nízký	k2eAgFnSc7d1	nízká
hustotou	hustota	k1gFnSc7	hustota
s	s	k7c7	s
jakou	jaký	k3yIgFnSc7	jaký
se	se	k3xPyFc4	se
letouny	letoun	k1gInPc7	letoun
setkávají	setkávat	k5eAaImIp3nP	setkávat
ve	v	k7c6	v
vysokých	vysoký	k2eAgFnPc6d1	vysoká
hladinách	hladina	k1gFnPc6	hladina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
300	[number]	k4	300
000	[number]	k4	000
ft	ft	k?	ft
nebo	nebo	k8xC	nebo
91,4	[number]	k4	91,4
km	km	kA	km
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
satelity	satelit	k1gInPc1	satelit
na	na	k7c6	na
nízké	nízký	k2eAgFnSc6d1	nízká
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
statistická	statistický	k2eAgFnSc1d1	statistická
mechanika	mechanika	k1gFnSc1	mechanika
k	k	k7c3	k
řešení	řešení	k1gNnSc3	řešení
problémů	problém	k1gInPc2	problém
vhodnější	vhodný	k2eAgFnSc1d2	vhodnější
než	než	k8xS	než
kontinuální	kontinuální	k2eAgFnSc1d1	kontinuální
aerodynamika	aerodynamika	k1gFnSc1	aerodynamika
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
výběru	výběr	k1gInSc3	výběr
mezi	mezi	k7c7	mezi
statistickou	statistický	k2eAgFnSc7d1	statistická
mechanikou	mechanika	k1gFnSc7	mechanika
a	a	k8xC	a
kontinuální	kontinuální	k2eAgFnSc7d1	kontinuální
formulací	formulace	k1gFnSc7	formulace
aerodynamiky	aerodynamika	k1gFnSc2	aerodynamika
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použito	použít	k5eAaPmNgNnS	použít
tzv.	tzv.	kA	tzv.
Knudsenovo	Knudsenův	k2eAgNnSc1d1	Knudsenův
číslo	číslo	k1gNnSc1	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Proudění	proudění	k1gNnSc1	proudění
vzduchu	vzduch	k1gInSc2	vzduch
je	být	k5eAaImIp3nS	být
relativní	relativní	k2eAgFnSc1d1	relativní
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
těleso	těleso	k1gNnSc4	těleso
působí	působit	k5eAaImIp3nP	působit
stále	stále	k6eAd1	stále
stejné	stejný	k2eAgFnPc1d1	stejná
síly	síla	k1gFnPc1	síla
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
těleso	těleso	k1gNnSc4	těleso
vůči	vůči	k7c3	vůči
okolí	okolí	k1gNnSc3	okolí
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
a	a	k8xC	a
vzduch	vzduch	k1gInSc1	vzduch
kolem	kolem	k7c2	kolem
něj	on	k3xPp3gNnSc2	on
obtéká	obtékat	k5eAaImIp3nS	obtékat
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
obtékání	obtékání	k1gNnSc6	obtékání
těles	těleso	k1gNnPc2	těleso
proudem	proud	k1gInSc7	proud
vzduchu	vzduch	k1gInSc2	vzduch
se	se	k3xPyFc4	se
na	na	k7c6	na
jeho	jeho	k3xOp3gInPc6	jeho
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
površích	povrch	k1gInPc6	povrch
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
tento	tento	k3xDgInSc1	tento
vzduch	vzduch	k1gInSc1	vzduch
stlačuje	stlačovat	k5eAaImIp3nS	stlačovat
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
nerovnoměrné	rovnoměrný	k2eNgNnSc1d1	nerovnoměrné
tlakové	tlakový	k2eAgNnSc1d1	tlakové
pole	pole	k1gNnSc1	pole
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
silovou	silový	k2eAgFnSc4d1	silová
nerovnováhu	nerovnováha	k1gFnSc4	nerovnováha
<g/>
.	.	kIx.	.
</s>
<s>
Tlak	tlak	k1gInSc1	tlak
vzduchu	vzduch	k1gInSc2	vzduch
lze	lze	k6eAd1	lze
uvažovat	uvažovat	k5eAaImF	uvažovat
třemi	tři	k4xCgInPc7	tři
způsoby	způsob	k1gInPc7	způsob
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Statický	statický	k2eAgInSc1d1	statický
tlak	tlak	k1gInSc1	tlak
–	–	k?	–
lze	lze	k6eAd1	lze
naměřit	naměřit	k5eAaBmF	naměřit
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
vzduch	vzduch	k1gInSc1	vzduch
nepohybuje	pohybovat	k5eNaImIp3nS	pohybovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dynamický	dynamický	k2eAgInSc1d1	dynamický
tlak	tlak	k1gInSc1	tlak
–	–	k?	–
lze	lze	k6eAd1	lze
naměřit	naměřit	k5eAaBmF	naměřit
za	za	k7c2	za
pohybu	pohyb	k1gInSc2	pohyb
vzduchu	vzduch	k1gInSc2	vzduch
jako	jako	k8xS	jako
pokles	pokles	k1gInSc4	pokles
statického	statický	k2eAgInSc2d1	statický
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
tlak	tlak	k1gInSc1	tlak
–	–	k?	–
je	být	k5eAaImIp3nS	být
součet	součet	k1gInSc1	součet
statického	statický	k2eAgMnSc2d1	statický
a	a	k8xC	a
dynamického	dynamický	k2eAgInSc2d1	dynamický
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
<g/>
Součet	součet	k1gInSc1	součet
statického	statický	k2eAgMnSc2d1	statický
a	a	k8xC	a
dynamického	dynamický	k2eAgInSc2d1	dynamický
tlaku	tlak	k1gInSc2	tlak
je	být	k5eAaImIp3nS	být
konstantní	konstantní	k2eAgFnSc1d1	konstantní
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
těleso	těleso	k1gNnSc1	těleso
vůči	vůči	k7c3	vůči
okolnímu	okolní	k2eAgInSc3d1	okolní
vzduchu	vzduch	k1gInSc3	vzduch
nepohybuje	pohybovat	k5eNaImIp3nS	pohybovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
statický	statický	k2eAgInSc4d1	statický
tlak	tlak	k1gInSc4	tlak
maximální	maximální	k2eAgInSc4d1	maximální
a	a	k8xC	a
dynamický	dynamický	k2eAgInSc4d1	dynamický
tlak	tlak	k1gInSc4	tlak
nulový	nulový	k2eAgInSc4d1	nulový
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
pohybu	pohyb	k1gInSc2	pohyb
pak	pak	k6eAd1	pak
stoupá	stoupat	k5eAaImIp3nS	stoupat
dynamický	dynamický	k2eAgInSc4d1	dynamický
tlak	tlak	k1gInSc4	tlak
a	a	k8xC	a
statický	statický	k2eAgInSc4d1	statický
o	o	k7c4	o
tutéž	týž	k3xTgFnSc4	týž
hodnotu	hodnota	k1gFnSc4	hodnota
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
rychlostí	rychlost	k1gFnSc7	rychlost
roste	růst	k5eAaImIp3nS	růst
i	i	k9	i
dynamický	dynamický	k2eAgInSc4d1	dynamický
tlak	tlak	k1gInSc4	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
ofukované	ofukovaný	k2eAgNnSc4d1	ofukovaný
těleso	těleso	k1gNnSc4	těleso
asymetrické	asymetrický	k2eAgNnSc4d1	asymetrické
<g/>
,	,	kIx,	,
proudění	proudění	k1gNnSc4	proudění
vzduchu	vzduch	k1gInSc2	vzduch
kolem	kolem	k7c2	kolem
něj	on	k3xPp3gInSc2	on
bude	být	k5eAaImBp3nS	být
také	také	k9	také
asymetrické	asymetrický	k2eAgNnSc1d1	asymetrické
a	a	k8xC	a
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
bude	být	k5eAaImBp3nS	být
rychlost	rychlost	k1gFnSc1	rychlost
obtékání	obtékání	k1gNnSc2	obtékání
větší	veliký	k2eAgFnSc2d2	veliký
než	než	k8xS	než
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
druhé	druhý	k4xOgFnSc6	druhý
<g/>
,	,	kIx,	,
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
kde	kde	k6eAd1	kde
bude	být	k5eAaImBp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
rychlost	rychlost	k1gFnSc1	rychlost
bude	být	k5eAaImBp3nS	být
vyšší	vysoký	k2eAgNnSc4d2	vyšší
dynamický	dynamický	k2eAgInSc4d1	dynamický
a	a	k8xC	a
nižší	nízký	k2eAgInSc4d2	nižší
statický	statický	k2eAgInSc4d1	statický
tlak	tlak	k1gInSc4	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Nerovnováha	nerovnováha	k1gFnSc1	nerovnováha
statického	statický	k2eAgInSc2d1	statický
tlaku	tlak	k1gInSc2	tlak
způsobí	způsobit	k5eAaPmIp3nS	způsobit
vznik	vznik	k1gInSc1	vznik
aerodynamické	aerodynamický	k2eAgFnSc2d1	aerodynamická
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Aerodynamická	aerodynamický	k2eAgFnSc1d1	aerodynamická
síla	síla	k1gFnSc1	síla
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
létat	létat	k5eAaImF	létat
letadlům	letadlo	k1gNnPc3	letadlo
těžším	těžký	k2eAgInPc3d2	těžší
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Částice	částice	k1gFnSc1	částice
vzduchu	vzduch	k1gInSc2	vzduch
se	se	k3xPyFc4	se
spojují	spojovat	k5eAaImIp3nP	spojovat
do	do	k7c2	do
proudnic	proudnice	k1gFnPc2	proudnice
(	(	kIx(	(
<g/>
dráha	dráha	k1gFnSc1	dráha
vybrané	vybraný	k2eAgFnSc2d1	vybraná
částice	částice	k1gFnSc2	částice
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
)	)	kIx)	)
a	a	k8xC	a
ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
spojují	spojovat	k5eAaImIp3nP	spojovat
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
proudového	proudový	k2eAgInSc2d1	proudový
svazku	svazek	k1gInSc2	svazek
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
3	[number]	k4	3
typy	typ	k1gInPc7	typ
proudění	proudění	k1gNnSc1	proudění
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
laminární	laminární	k2eAgInPc4d1	laminární
(	(	kIx(	(
<g/>
ustálené	ustálený	k2eAgInPc4d1	ustálený
<g/>
)	)	kIx)	)
–	–	k?	–
proudnice	proudnice	k1gFnPc1	proudnice
jsou	být	k5eAaImIp3nP	být
zhruba	zhruba	k6eAd1	zhruba
rovnoběžné	rovnoběžný	k2eAgFnPc1d1	rovnoběžná
(	(	kIx(	(
<g/>
jejich	jejich	k3xOp3gFnPc1	jejich
dráhy	dráha	k1gFnPc1	dráha
se	se	k3xPyFc4	se
nekříží	křížit	k5eNaImIp3nP	křížit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
částice	částice	k1gFnPc1	částice
se	se	k3xPyFc4	se
posouvají	posouvat	k5eAaImIp3nP	posouvat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nerotují	rotovat	k5eNaImIp3nP	rotovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
turbulentní	turbulentní	k2eAgInPc4d1	turbulentní
(	(	kIx(	(
<g/>
vířivé	vířivý	k2eAgInPc4d1	vířivý
<g/>
)	)	kIx)	)
–	–	k?	–
proudnice	proudnice	k1gFnSc1	proudnice
se	se	k3xPyFc4	se
vlivem	vliv	k1gInSc7	vliv
prostředí	prostředí	k1gNnSc2	prostředí
roztáčejí	roztáčet	k5eAaImIp3nP	roztáčet
a	a	k8xC	a
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
začínají	začínat	k5eAaImIp3nP	začínat
křížit	křížit	k5eAaImF	křížit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
vírové	vírový	k2eAgNnSc1d1	vírové
proudění	proudění	k1gNnSc1	proudění
-	-	kIx~	-
krouživý	krouživý	k2eAgInSc1d1	krouživý
pohyb	pohyb	k1gInSc1	pohyb
tekutiny	tekutina	k1gFnSc2	tekutina
okolo	okolo	k7c2	okolo
určité	určitý	k2eAgFnSc2d1	určitá
křivky	křivka	k1gFnSc2	křivka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
jeho	jeho	k3xOp3gFnSc4	jeho
osu	osa	k1gFnSc4	osa
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
k	k	k7c3	k
ose	osa	k1gFnSc3	osa
víru	vír	k1gInSc2	vír
dochází	docházet	k5eAaImIp3nS	docházet
zpočátku	zpočátku	k6eAd1	zpočátku
k	k	k7c3	k
růstu	růst	k1gInSc3	růst
rychlosti	rychlost	k1gFnSc2	rychlost
a	a	k8xC	a
poklesu	pokles	k1gInSc2	pokles
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
osy	osa	k1gFnSc2	osa
víru	vír	k1gInSc2	vír
(	(	kIx(	(
<g/>
vírového	vírový	k2eAgNnSc2d1	vírové
jádra	jádro	k1gNnSc2	jádro
<g/>
)	)	kIx)	)
dochází	docházet	k5eAaImIp3nS	docházet
vlivem	vliv	k1gInSc7	vliv
vazkosti	vazkost	k1gFnSc2	vazkost
(	(	kIx(	(
<g/>
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
tření	tření	k1gNnSc2	tření
<g/>
)	)	kIx)	)
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ose	osa	k1gFnSc6	osa
víru	vír	k1gInSc2	vír
nulová	nulový	k2eAgFnSc1d1	nulová
<g/>
.	.	kIx.	.
<g/>
Přechod	přechod	k1gInSc1	přechod
mezi	mezi	k7c7	mezi
laminárním	laminární	k2eAgNnSc7d1	laminární
a	a	k8xC	a
turbulentním	turbulentní	k2eAgNnSc7d1	turbulentní
prouděním	proudění	k1gNnSc7	proudění
je	být	k5eAaImIp3nS	být
dán	dán	k2eAgInSc1d1	dán
Reynoldsovým	Reynoldsův	k2eAgNnSc7d1	Reynoldsovo
číslem	číslo	k1gNnSc7	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Aerodynamický	aerodynamický	k2eAgInSc4d1	aerodynamický
tvar	tvar	k1gInSc4	tvar
==	==	k?	==
</s>
</p>
<p>
<s>
Aerodynamický	aerodynamický	k2eAgInSc1d1	aerodynamický
tvar	tvar	k1gInSc1	tvar
má	mít	k5eAaImIp3nS	mít
nejmenší	malý	k2eAgInSc4d3	nejmenší
součinitel	součinitel	k1gInSc4	součinitel
odporu	odpor	k1gInSc2	odpor
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
se	s	k7c7	s
součiniteli	součinitel	k1gInPc7	součinitel
pro	pro	k7c4	pro
jiné	jiný	k2eAgInPc4d1	jiný
tvary	tvar	k1gInPc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
tvarem	tvar	k1gInSc7	tvar
připomíná	připomínat	k5eAaImIp3nS	připomínat
kapku	kapka	k1gFnSc4	kapka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aerodynamický	aerodynamický	k2eAgInSc1d1	aerodynamický
tvar	tvar	k1gInSc1	tvar
je	být	k5eAaImIp3nS	být
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
vpředu	vpředu	k6eAd1	vpředu
zakulacený	zakulacený	k2eAgInSc1d1	zakulacený
a	a	k8xC	a
vzadu	vzadu	k6eAd1	vzadu
do	do	k7c2	do
špičky	špička	k1gFnSc2	špička
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tvar	tvar	k1gInSc1	tvar
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
maximálně	maximálně	k6eAd1	maximálně
možné	možný	k2eAgFnSc6d1	možná
míře	míra	k1gFnSc6	míra
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
laminární	laminární	k2eAgNnSc1d1	laminární
proudění	proudění	k1gNnSc1	proudění
plynu	plyn	k1gInSc2	plyn
nebo	nebo	k8xC	nebo
kapaliny	kapalina	k1gFnSc2	kapalina
kolem	kolem	k6eAd1	kolem
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
odpor	odpor	k1gInSc4	odpor
prostředí	prostředí	k1gNnSc2	prostředí
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgFnPc1d2	menší
než	než	k8xS	než
při	při	k7c6	při
turbulentním	turbulentní	k2eAgNnSc6d1	turbulentní
proudění	proudění	k1gNnSc6	proudění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výpočetní	výpočetní	k2eAgFnPc1d1	výpočetní
metody	metoda	k1gFnPc1	metoda
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Úvod	úvod	k1gInSc1	úvod
===	===	k?	===
</s>
</p>
<p>
<s>
Problémy	problém	k1gInPc1	problém
při	při	k7c6	při
výpočtech	výpočet	k1gInPc6	výpočet
aerodynamiky	aerodynamika	k1gFnSc2	aerodynamika
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
proudění	proudění	k1gNnSc1	proudění
tekutiny	tekutina	k1gFnSc2	tekutina
závisí	záviset	k5eAaImIp3nS	záviset
nejen	nejen	k6eAd1	nejen
od	od	k7c2	od
makroskopického	makroskopický	k2eAgInSc2d1	makroskopický
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
od	od	k7c2	od
mikroskopické	mikroskopický	k2eAgFnSc2d1	mikroskopická
kvality	kvalita	k1gFnSc2	kvalita
povrchu	povrch	k1gInSc2	povrch
tělesa	těleso	k1gNnSc2	těleso
(	(	kIx(	(
<g/>
letadla	letadlo	k1gNnSc2	letadlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
aerodynamika	aerodynamika	k1gFnSc1	aerodynamika
podporu	podpor	k1gInSc2	podpor
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
vědních	vědní	k2eAgInPc2d1	vědní
oborů	obor	k1gInPc2	obor
(	(	kIx(	(
<g/>
např.	např.	kA	např.
chemie	chemie	k1gFnSc2	chemie
pro	pro	k7c4	pro
speciální	speciální	k2eAgInPc4d1	speciální
povrchové	povrchový	k2eAgInPc4d1	povrchový
nátěry	nátěr	k1gInPc4	nátěr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dost	dost	k6eAd1	dost
přesné	přesný	k2eAgInPc1d1	přesný
vzorce	vzorec	k1gInPc1	vzorec
pro	pro	k7c4	pro
výpočet	výpočet	k1gInSc4	výpočet
těchto	tento	k3xDgFnPc2	tento
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
ideální	ideální	k2eAgFnSc4d1	ideální
přesnost	přesnost	k1gFnSc4	přesnost
jsou	být	k5eAaImIp3nP	být
nezbytné	nezbytný	k2eAgFnPc1d1	nezbytná
zkoušky	zkouška	k1gFnPc1	zkouška
v	v	k7c6	v
aerodynamickém	aerodynamický	k2eAgInSc6d1	aerodynamický
tunelu	tunel	k1gInSc6	tunel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
soustavy	soustava	k1gFnSc2	soustava
plyn	plyn	k1gInSc1	plyn
<g/>
/	/	kIx~	/
<g/>
těleso	těleso	k1gNnSc1	těleso
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
podle	podle	k7c2	podle
rychlosti	rychlost	k1gFnSc2	rychlost
proudění	proudění	k1gNnSc2	proudění
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
oblastí	oblast	k1gFnPc2	oblast
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
jiné	jiný	k2eAgInPc1d1	jiný
přibližné	přibližný	k2eAgInPc1d1	přibližný
vzorce	vzorec	k1gInPc1	vzorec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejlepší	dobrý	k2eAgInPc1d3	nejlepší
vzorce	vzorec	k1gInPc1	vzorec
se	se	k3xPyFc4	se
ale	ale	k9	ale
dají	dát	k5eAaPmIp3nP	dát
odvodit	odvodit	k5eAaPmF	odvodit
pomocí	pomocí	k7c2	pomocí
Maxwell-Boltzmannovy	Maxwell-Boltzmannův	k2eAgFnSc2d1	Maxwell-Boltzmannův
statistiky	statistika	k1gFnSc2	statistika
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dokážou	dokázat	k5eAaPmIp3nP	dokázat
popsat	popsat	k5eAaPmF	popsat
i	i	k9	i
vznik	vznik	k1gInSc4	vznik
turbulencí	turbulence	k1gFnPc2	turbulence
a	a	k8xC	a
vztah	vztah	k1gInSc1	vztah
ke	k	k7c3	k
kvalitě	kvalita	k1gFnSc3	kvalita
povrchu	povrch	k1gInSc2	povrch
pevného	pevný	k2eAgNnSc2d1	pevné
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Dají	dát	k5eAaPmIp3nP	dát
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
použít	použít	k5eAaPmF	použít
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
počítačové	počítačový	k2eAgFnPc4d1	počítačová
simulace	simulace	k1gFnPc4	simulace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
počítačů	počítač	k1gInPc2	počítač
se	se	k3xPyFc4	se
okolí	okolí	k1gNnSc1	okolí
tělesa	těleso	k1gNnSc2	těleso
rozdělí	rozdělit	k5eAaPmIp3nS	rozdělit
na	na	k7c4	na
síť	síť	k1gFnSc4	síť
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
každý	každý	k3xTgInSc1	každý
bod	bod	k1gInSc1	bod
při	při	k7c6	při
trojrozměrném	trojrozměrný	k2eAgInSc6d1	trojrozměrný
modelu	model	k1gInSc6	model
matematicky	matematicky	k6eAd1	matematicky
propojený	propojený	k2eAgInSc4d1	propojený
na	na	k7c4	na
6	[number]	k4	6
<g/>
,	,	kIx,	,
při	při	k7c6	při
dvourozměrném	dvourozměrný	k2eAgNnSc6d1	dvourozměrné
jen	jen	k9	jen
na	na	k7c4	na
4	[number]	k4	4
sousední	sousední	k2eAgInPc4d1	sousední
body	bod	k1gInPc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
síť	síť	k1gFnSc1	síť
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
teoreticky	teoreticky	k6eAd1	teoreticky
čtvercová	čtvercový	k2eAgFnSc1d1	čtvercová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prakticky	prakticky	k6eAd1	prakticky
kopíruje	kopírovat	k5eAaImIp3nS	kopírovat
zhruba	zhruba	k6eAd1	zhruba
radiální	radiální	k2eAgInSc4d1	radiální
povrch	povrch	k1gInSc4	povrch
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
sítě	síť	k1gFnSc2	síť
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rovnoměrná	rovnoměrný	k2eAgFnSc1d1	rovnoměrná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
očekávají	očekávat	k5eAaImIp3nP	očekávat
hustší	hustý	k2eAgFnPc1d2	hustší
proudnice	proudnice	k1gFnPc1	proudnice
nebo	nebo	k8xC	nebo
komplikovanější	komplikovaný	k2eAgInPc1d2	komplikovanější
průběhy	průběh	k1gInPc1	průběh
proudění	proudění	k1gNnSc2	proudění
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dělá	dělat	k5eAaImIp3nS	dělat
hustší	hustý	k2eAgFnSc1d2	hustší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podzvuková	podzvukový	k2eAgFnSc1d1	podzvuková
aerodynamika	aerodynamika	k1gFnSc1	aerodynamika
===	===	k?	===
</s>
</p>
<p>
<s>
Podzvuková	podzvukový	k2eAgFnSc1d1	podzvuková
(	(	kIx(	(
<g/>
subsonická	subsonický	k2eAgFnSc1d1	subsonická
<g/>
)	)	kIx)	)
aerodynamika	aerodynamika	k1gFnSc1	aerodynamika
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc4	oblast
aerodynamiky	aerodynamika	k1gFnSc2	aerodynamika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
prouděním	proudění	k1gNnSc7	proudění
plynů	plyn	k1gInPc2	plyn
do	do	k7c2	do
rychlosti	rychlost	k1gFnSc2	rychlost
0,7	[number]	k4	0,7
M.	M.	kA	M.
V	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
těchto	tento	k3xDgFnPc2	tento
rychlostí	rychlost	k1gFnPc2	rychlost
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
neprojevuje	projevovat	k5eNaImIp3nS	projevovat
stlačitelnost	stlačitelnost	k1gFnSc4	stlačitelnost
plynů	plyn	k1gInPc2	plyn
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
při	při	k7c6	při
měření	měření	k1gNnSc6	měření
<g/>
,	,	kIx,	,
výpočtech	výpočet	k1gInPc6	výpočet
a	a	k8xC	a
experimentování	experimentování	k1gNnSc1	experimentování
považovat	považovat	k5eAaImF	považovat
plynné	plynný	k2eAgNnSc4d1	plynné
médium	médium	k1gNnSc4	médium
za	za	k7c4	za
ideální	ideální	k2eAgInSc4d1	ideální
plyn	plyn	k1gInSc4	plyn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
podzvukové	podzvukový	k2eAgFnPc4d1	podzvuková
rychlosti	rychlost	k1gFnPc4	rychlost
a	a	k8xC	a
pro	pro	k7c4	pro
laminární	laminární	k2eAgNnSc4d1	laminární
proudění	proudění	k1gNnSc4	proudění
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
výpočet	výpočet	k1gInSc4	výpočet
vztlaku	vztlak	k1gInSc2	vztlak
dá	dát	k5eAaPmIp3nS	dát
v	v	k7c6	v
jednoduchých	jednoduchý	k2eAgInPc6d1	jednoduchý
případech	případ	k1gInPc6	případ
použít	použít	k5eAaPmF	použít
např.	např.	kA	např.
Bernoulliho	Bernoulli	k1gMnSc2	Bernoulli
rovnice	rovnice	k1gFnSc2	rovnice
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ρ	ρ	k?	ρ
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
l	l	kA	l
</s>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
p_	p_	k?	p_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
p_	p_	k?	p_
<g/>
{	{	kIx(	{
<g/>
celk	celk	k1gMnSc1	celk
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ρ	ρ	k?	ρ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
hustota	hustota	k1gFnSc1	hustota
plynu	plyn	k1gInSc2	plyn
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
[	[	kIx(	[
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
kg	kg	kA	kg
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Možnost	možnost	k1gFnSc1	možnost
vzniku	vznik	k1gInSc2	vznik
turbulencí	turbulence	k1gFnPc2	turbulence
se	se	k3xPyFc4	se
určuje	určovat	k5eAaImIp3nS	určovat
podle	podle	k7c2	podle
Reynoldsova	Reynoldsův	k2eAgNnSc2d1	Reynoldsovo
čísla	číslo	k1gNnSc2	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejaerodynamičtější	aerodynamický	k2eAgInSc1d3	nejaerodynamičtější
tvar	tvar	k1gInSc1	tvar
pro	pro	k7c4	pro
podzvukové	podzvukový	k2eAgFnPc4d1	podzvuková
rychlosti	rychlost	k1gFnPc4	rychlost
je	být	k5eAaImIp3nS	být
kapka	kapka	k1gFnSc1	kapka
-	-	kIx~	-
kapkovitý	kapkovitý	k2eAgInSc1d1	kapkovitý
profil	profil	k1gInSc1	profil
se	se	k3xPyFc4	se
na	na	k7c6	na
letadlech	letadlo	k1gNnPc6	letadlo
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c4	na
cca	cca	kA	cca
90	[number]	k4	90
<g/>
%	%	kIx~	%
průřezech	průřez	k1gInPc6	průřez
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
aerodynamiky	aerodynamika	k1gFnSc2	aerodynamika
je	být	k5eAaImIp3nS	být
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
prozkoumána	prozkoumán	k2eAgFnSc1d1	prozkoumána
a	a	k8xC	a
přesnost	přesnost	k1gFnSc1	přesnost
simulací	simulace	k1gFnPc2	simulace
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
více	hodně	k6eAd2	hodně
než	než	k8xS	než
99	[number]	k4	99
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Supersonická	supersonický	k2eAgFnSc1d1	supersonická
aerodynamika	aerodynamika	k1gFnSc1	aerodynamika
===	===	k?	===
</s>
</p>
<p>
<s>
Zabývá	zabývat	k5eAaImIp3nS	zabývat
se	s	k7c7	s
zkoumáním	zkoumání	k1gNnSc7	zkoumání
proudění	proudění	k1gNnSc2	proudění
plynu	plynout	k5eAaImIp1nS	plynout
při	při	k7c6	při
nadzvukových	nadzvukový	k2eAgFnPc6d1	nadzvuková
rychlostech	rychlost	k1gFnPc6	rychlost
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
(	(	kIx(	(
<g/>
1	[number]	k4	1
Mach	macha	k1gFnPc2	macha
až	až	k8xS	až
Mach	macha	k1gFnPc2	macha
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nadzvuková	nadzvukový	k2eAgNnPc1d1	nadzvukové
tělesa	těleso	k1gNnPc1	těleso
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
poznat	poznat	k5eAaPmF	poznat
podle	podle	k7c2	podle
zašpicatelého	zašpicatelý	k2eAgInSc2d1	zašpicatelý
předku	předek	k1gInSc2	předek
profilu	profil	k1gInSc2	profil
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hypersonická	Hypersonický	k2eAgFnSc1d1	Hypersonický
aerodynamika	aerodynamika	k1gFnSc1	aerodynamika
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
extrémně	extrémně	k6eAd1	extrémně
vysoké	vysoký	k2eAgFnPc4d1	vysoká
rychlosti	rychlost	k1gFnPc4	rychlost
(	(	kIx(	(
<g/>
Mach	Mach	k1gMnSc1	Mach
4	[number]	k4	4
a	a	k8xC	a
více	hodně	k6eAd2	hodně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
již	již	k6eAd1	již
plyn	plyn	k1gInSc1	plyn
ionizuje	ionizovat	k5eAaBmIp3nS	ionizovat
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
může	moct	k5eAaImIp3nS	moct
změnit	změnit	k5eAaPmF	změnit
na	na	k7c4	na
plazma	plazma	k1gNnSc4	plazma
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vysoké	vysoký	k2eAgFnPc4d1	vysoká
teploty	teplota	k1gFnPc4	teplota
se	se	k3xPyFc4	se
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
bere	brát	k5eAaImIp3nS	brát
i	i	k9	i
termodynamika	termodynamika	k1gFnSc1	termodynamika
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
vědou	věda	k1gFnSc7	věda
o	o	k7c6	o
materiálech	materiál	k1gInPc6	materiál
.	.	kIx.	.
</s>
<s>
Běžné	běžný	k2eAgInPc1d1	běžný
materiály	materiál	k1gInPc1	materiál
jako	jako	k8xS	jako
dural	dural	k1gInSc1	dural
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
při	při	k7c6	při
takových	takový	k3xDgFnPc6	takový
rychlostech	rychlost	k1gFnPc6	rychlost
roztaví	roztavit	k5eAaPmIp3nS	roztavit
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
příkladem	příklad	k1gInSc7	příklad
hypersonickému	hypersonický	k2eAgInSc3d1	hypersonický
rychlosti	rychlost	k1gFnSc6	rychlost
jsou	být	k5eAaImIp3nP	být
raketoplány	raketoplán	k1gInPc1	raketoplán
při	při	k7c6	při
průletu	průlet	k1gInSc6	průlet
atmosférou	atmosféra	k1gFnSc7	atmosféra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Mechanika	mechanika	k1gFnSc1	mechanika
tekutin	tekutina	k1gFnPc2	tekutina
</s>
</p>
<p>
<s>
Mechanika	mechanika	k1gFnSc1	mechanika
kontinua	kontinuum	k1gNnSc2	kontinuum
</s>
</p>
<p>
<s>
Hydrodynamika	hydrodynamika	k1gFnSc1	hydrodynamika
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
aerodynamika	aerodynamika	k1gFnSc1	aerodynamika
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
aerodynamika	aerodynamika	k1gFnSc1	aerodynamika
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
