<s>
Aerodynamika	aerodynamika	k1gFnSc1	aerodynamika
(	(	kIx(	(
<g/>
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
ἀ	ἀ	k?	ἀ
aer	aero	k1gNnPc2	aero
–	–	k?	–
vzduch	vzduch	k1gInSc1	vzduch
+	+	kIx~	+
δ	δ	k?	δ
–	–	k?	–
dynamika	dynamika	k1gFnSc1	dynamika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
obor	obor	k1gInSc4	obor
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
speciálně	speciálně	k6eAd1	speciálně
mechaniky	mechanika	k1gFnPc1	mechanika
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgFnPc1d1	zabývající
se	s	k7c7	s
studiem	studio	k1gNnSc7	studio
pohybu	pohyb	k1gInSc2	pohyb
plynů	plyn	k1gInPc2	plyn
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
vzduchu	vzduch	k1gInSc3	vzduch
<g/>
)	)	kIx)	)
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
interakcí	interakce	k1gFnPc2	interakce
s	s	k7c7	s
pevnými	pevný	k2eAgInPc7d1	pevný
objekty	objekt	k1gInPc7	objekt
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
křídlo	křídlo	k1gNnSc1	křídlo
letadla	letadlo	k1gNnSc2	letadlo
<g/>
.	.	kIx.	.
</s>
