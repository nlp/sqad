<s>
Principy	princip	k1gInPc1
víry	víra	k1gFnSc2
v	v	k7c6
judaismu	judaismus	k1gInSc6
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Konkrétní	konkrétní	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
:	:	kIx,
doplnit	doplnit	k5eAaPmF
úvod	úvod	k1gInSc4
</s>
<s>
Židé	Žid	k1gMnPc1
a	a	k8xC
judaismus	judaismus	k1gInSc1
</s>
<s>
Židé	Žid	k1gMnPc1
•	•	k?
Judaismus	judaismus	k1gInSc1
•	•	k?
Kdo	kdo	k3yInSc1,k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
Žid	Žid	k1gMnSc1
</s>
<s>
Ortodoxní	ortodoxní	k2eAgFnPc1d1
•	•	k?
Konzervativní	konzervativní	k2eAgFnPc1d1
(	(	kIx(
<g/>
Masorti	Masort	k1gMnPc1
<g/>
)	)	kIx)
<g/>
Progresivní	progresivní	k2eAgFnPc4d1
(	(	kIx(
<g/>
Reformní	reformní	k2eAgFnPc4d1
-	-	kIx~
Liberální	liberální	k2eAgFnPc4d1
<g/>
)	)	kIx)
•	•	k?
UltraortodoxníSamaritáni	UltraortodoxníSamaritán	k1gMnPc1
•	•	k?
Falašové	Falašové	k2eAgInPc2d1
•	•	k?
Karaité	Karaitý	k2eAgFnSc2d1
</s>
<s>
Etnické	etnický	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
a	a	k8xC
jazyky	jazyk	k1gInPc1
</s>
<s>
Aškenázové	Aškenázové	k2eAgInSc4d1
•	•	k?
Sefardové	Sefardový	k2eAgNnSc1d1
•	•	k?
MizrachimHebrejština	MizrachimHebrejština	k1gFnSc1
•	•	k?
Jidiš	jidiš	k6eAd1
•	•	k?
Ladino	Ladin	k2eAgNnSc1d1
•	•	k?
Ge	Ge	k1gFnPc6
<g/>
'	'	kIx"
<g/>
ez	ez	k?
•	•	k?
Buchori	Buchor	k1gFnSc2
</s>
<s>
Populace	populace	k1gFnSc1
(	(	kIx(
<g/>
vývoj	vývoj	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Evropa	Evropa	k1gFnSc1
•	•	k?
Amerika	Amerika	k1gFnSc1
•	•	k?
Asie	Asie	k1gFnSc2
•	•	k?
Afrika	Afrika	k1gFnSc1
•	•	k?
Austrálie	Austrálie	k1gFnSc2
</s>
<s>
Náboženství	náboženství	k1gNnSc1
</s>
<s>
Bůh	bůh	k1gMnSc1
•	•	k?
Principy	princip	k1gInPc1
víry	víra	k1gFnSc2
•	•	k?
Boží	božit	k5eAaImIp3nS
jména	jméno	k1gNnSc2
<g/>
613	#num#	k4
micvot	micvota	k1gFnPc2
•	•	k?
Halacha	Halacha	k1gMnSc1
•	•	k?
Noachidské	Noachidský	k2eAgMnPc4d1
zákonyMesiáš	zákonyMesiat	k5eAaImIp2nS,k5eAaPmIp2nS,k5eAaBmIp2nS
•	•	k?
Eschatologie	eschatologie	k1gFnSc1
</s>
<s>
Židovské	židovský	k2eAgNnSc1d1
myšlení	myšlení	k1gNnSc1
<g/>
,	,	kIx,
filosofie	filosofie	k1gFnSc1
a	a	k8xC
etika	etika	k1gFnSc1
</s>
<s>
Židovská	židovská	k1gFnSc1
filosofieCdaka	filosofieCdak	k1gMnSc2
•	•	k?
Musar	Musar	k1gInSc1
•	•	k?
VyvolenostChasidismus	VyvolenostChasidismus	k1gInSc1
•	•	k?
Kabala	kabala	k1gFnSc1
•	•	k?
Haskala	Haskal	k1gMnSc2
</s>
<s>
Náboženské	náboženský	k2eAgInPc1d1
texty	text	k1gInPc1
</s>
<s>
Tóra	tóra	k1gFnSc1
•	•	k?
Tanach	Tanach	k1gInSc1
•	•	k?
Mišna	Mišn	k1gInSc2
•	•	k?
Talmud	talmud	k1gInSc1
•	•	k?
MidrašTosefta	MidrašToseft	k1gInSc2
•	•	k?
Mišne	Mišn	k1gInSc5
Tora	Tor	k1gMnSc4
•	•	k?
Šulchan	Šulchan	k1gMnSc1
aruchSidur	aruchSidur	k1gMnSc1
•	•	k?
Machzor	Machzor	k1gMnSc1
•	•	k?
Pijut	Pijut	k1gMnSc1
•	•	k?
Zohar	Zohar	k1gMnSc1
</s>
<s>
Životní	životní	k2eAgInSc1d1
cyklus	cyklus	k1gInSc1
<g/>
,	,	kIx,
tradice	tradice	k1gFnPc1
a	a	k8xC
zvyky	zvyk	k1gInPc1
</s>
<s>
Obřízka	obřízka	k1gFnSc1
•	•	k?
Pidjon	Pidjon	k1gMnSc1
ha-ben	ha-ben	k2eAgMnSc1d1
•	•	k?
Simchat	Simchat	k1gMnSc1
bat	bat	k?
•	•	k?
Bar	bar	k1gInSc1
micvaŠiduch	micvaŠiduch	k1gInSc1
•	•	k?
Svatba	svatba	k1gFnSc1
•	•	k?
Ketuba	Ketuba	k1gFnSc1
•	•	k?
Rozvod	rozvod	k1gInSc1
(	(	kIx(
<g/>
Get	Get	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
PohřebKašrut	PohřebKašrut	k1gInSc1
•	•	k?
Židovský	židovský	k2eAgInSc1d1
kalendář	kalendář	k1gInSc1
•	•	k?
Židovské	židovská	k1gFnSc2
svátkyTalit	svátkyTalit	k5eAaImF,k5eAaPmF
•	•	k?
Tfilin	Tfilin	k2eAgInSc4d1
•	•	k?
Cicit	Cicit	k1gInSc4
•	•	k?
KipaMezuza	KipaMezuz	k1gMnSc2
•	•	k?
Menora	Menor	k1gMnSc2
•	•	k?
Šofar	Šofar	k1gMnSc1
•	•	k?
Sefer	Sefer	k1gMnSc1
Tora	Tor	k1gMnSc2
</s>
<s>
Významné	významný	k2eAgFnPc1d1
postavy	postava	k1gFnPc1
židovství	židovství	k1gNnSc2
</s>
<s>
Abrahám	Abrahám	k1gMnSc1
•	•	k?
Izák	Izák	k1gMnSc1
•	•	k?
Jákob	Jákob	k1gMnSc1
•	•	k?
MojžíšŠalomoun	MojžíšŠalomoun	k1gMnSc1
•	•	k?
David	David	k1gMnSc1
•	•	k?
Elijáš	Elijáš	k1gMnSc1
•	•	k?
ÁronMaimonides	ÁronMaimonides	k1gMnSc1
•	•	k?
Nachmanides	Nachmanides	k1gMnSc1
•	•	k?
RašiBa	RašiBa	k1gMnSc1
<g/>
'	'	kIx"
<g/>
al	ala	k1gFnPc2
Šem	Šem	k1gFnSc2
Tov	Tov	k1gMnSc1
•	•	k?
Ga	Ga	k1gMnSc1
<g/>
'	'	kIx"
<g/>
on	on	k3xPp3gMnSc1
z	z	k7c2
Vilna	Viln	k1gMnSc2
•	•	k?
Maharal	Maharal	k1gMnSc2
</s>
<s>
Náboženské	náboženský	k2eAgFnPc1d1
budovy	budova	k1gFnPc1
a	a	k8xC
instituce	instituce	k1gFnPc1
</s>
<s>
Chrám	chrám	k1gInSc1
•	•	k?
Synagoga	synagoga	k1gFnSc1
•	•	k?
Ješiva	Ješiva	k1gFnSc1
•	•	k?
Bejt	Bejt	k?
midrašRabín	midrašRabín	k1gMnSc1
•	•	k?
Chazan	Chazan	k1gMnSc1
•	•	k?
Dajan	Dajan	k1gMnSc1
•	•	k?
Ga	Ga	k1gMnSc1
<g/>
'	'	kIx"
<g/>
onKohen	onKohen	k2eAgMnSc1d1
(	(	kIx(
<g/>
kněz	kněz	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Mašgiach	Mašgiach	k1gMnSc1
•	•	k?
Gabaj	Gabaj	k1gMnSc1
•	•	k?
ŠochetMohel	ŠochetMohel	k1gMnSc1
•	•	k?
Bejt	Bejt	k?
din	din	k1gInSc1
•	•	k?
Roš	Roš	k1gMnSc2
ješiva	ješiv	k1gMnSc2
</s>
<s>
Židovská	židovský	k2eAgFnSc1d1
liturgie	liturgie	k1gFnSc1
</s>
<s>
Šema	Šema	k1gMnSc1
•	•	k?
Amida	Amida	k1gMnSc1
•	•	k?
KadišMinhag	KadišMinhag	k1gMnSc1
•	•	k?
Minjan	Minjan	k1gMnSc1
•	•	k?
NosachŠacharit	NosachŠacharit	k1gInSc1
•	•	k?
Mincha	Mincha	k1gMnSc1
•	•	k?
Ma	Ma	k1gMnSc1
<g/>
'	'	kIx"
<g/>
ariv	ariv	k1gMnSc1
•	•	k?
Musaf	Musaf	k1gMnSc1
</s>
<s>
Dějiny	dějiny	k1gFnPc1
Židů	Žid	k1gMnPc2
</s>
<s>
starověk	starověk	k1gInSc1
•	•	k?
středověk	středověk	k1gInSc1
•	•	k?
novověk	novověk	k1gInSc1
</s>
<s>
Blízká	blízký	k2eAgNnPc1d1
témata	téma	k1gNnPc1
</s>
<s>
Antisemitismus	antisemitismus	k1gInSc1
•	•	k?
Gój	gój	k1gMnSc1
•	•	k?
Holocaust	holocaust	k1gInSc1
•	•	k?
IzraelFilosemitismus	IzraelFilosemitismus	k1gInSc1
•	•	k?
SionismusAbrahámovská	SionismusAbrahámovská	k1gFnSc1
náboženství	náboženství	k1gNnSc2
</s>
<s>
z	z	k7c2
•	•	k?
d	d	k?
•	•	k?
e	e	k0
</s>
<s>
Víra	víra	k1gFnSc1
v	v	k7c4
Boha	bůh	k1gMnSc4
</s>
<s>
Judaismus	judaismus	k1gInSc1
je	být	k5eAaImIp3nS
monoteistickým	monoteistický	k2eAgNnSc7d1
náboženstvím	náboženství	k1gNnSc7
<g/>
,	,	kIx,
tzn.	tzn.	kA
že	že	k8xS
uctívá	uctívat	k5eAaImIp3nS
jediného	jediný	k2eAgMnSc4d1
Boha	bůh	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uctívání	uctívání	k1gNnSc1
jiných	jiný	k2eAgMnPc2d1
bohů	bůh	k1gMnPc2
je	být	k5eAaImIp3nS
zapovězeno	zapovědět	k5eAaPmNgNnS
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
uctívání	uctívání	k1gNnSc1
soch	socha	k1gFnPc2
<g/>
,	,	kIx,
jiných	jiný	k2eAgInPc2d1
předmětů	předmět	k1gInPc2
či	či	k8xC
míst	místo	k1gNnPc2
nebo	nebo	k8xC
přírodních	přírodní	k2eAgInPc2d1
úkazů	úkaz	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současná	současný	k2eAgFnSc1d1
religionistika	religionistika	k1gFnSc1
se	se	k3xPyFc4
domnívá	domnívat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
židovský	židovský	k2eAgInSc1d1
monoteismus	monoteismus	k1gInSc1
vznikl	vzniknout	k5eAaPmAgInS
synkrezí	synkreze	k1gFnSc7
starších	starý	k2eAgInPc2d2
<g/>
,	,	kIx,
polyteistických	polyteistický	k2eAgInPc2d1
kultů	kult	k1gInPc2
blízkovýchodního	blízkovýchodní	k2eAgInSc2d1
starosemitského	starosemitský	k2eAgInSc2d1
panteonu	panteon	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
byly	být	k5eAaImAgFnP
následně	následně	k6eAd1
sjednoceny	sjednotit	k5eAaPmNgFnP
do	do	k7c2
kultu	kult	k1gInSc2
jediného	jediný	k2eAgMnSc2d1
Boha	bůh	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozůstatky	pozůstatek	k1gInPc1
tohoto	tento	k3xDgNnSc2
dávného	dávný	k2eAgNnSc2d1
mnohobožství	mnohobožství	k1gNnSc2
spatřují	spatřovat	k5eAaImIp3nP
religionisté	religionista	k1gMnPc1
v	v	k7c6
množství	množství	k1gNnSc6
jmen	jméno	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yIgNnPc4,k3yQgNnPc4,k3yRgNnPc4
Bůh	bůh	k1gMnSc1
v	v	k7c6
judaismu	judaismus	k1gInSc6
má	mít	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Jméno	jméno	k1gNnSc1
Boží	boží	k2eAgFnSc2d1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článcích	článek	k1gInPc6
JHVH	JHVH	kA
a	a	k8xC
Boží	boží	k2eAgNnPc1d1
jména	jméno	k1gNnPc1
v	v	k7c6
judaismu	judaismus	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Boží	boží	k2eAgNnSc1d1
Jméno	jméno	k1gNnSc1
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
nejposvátnějších	posvátný	k2eAgFnPc2d3
věcí	věc	k1gFnPc2
v	v	k7c6
judaismu	judaismus	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakládání	nakládání	k1gNnSc1
s	s	k7c7
Božími	boží	k2eAgNnPc7d1
jmény	jméno	k1gNnPc7
vyžaduje	vyžadovat	k5eAaImIp3nS
zvláštní	zvláštní	k2eAgFnSc4d1
úctu	úcta	k1gFnSc4
a	a	k8xC
obezřetnost	obezřetnost	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
3	#num#	k4
<g/>
.	.	kIx.
přikázání	přikázání	k1gNnSc1
z	z	k7c2
Desatera	desatero	k1gNnSc2
nařizuje	nařizovat	k5eAaImIp3nS
"	"	kIx"
<g/>
nebrat	brat	k5eNaPmF,k5eNaImF
jej	on	k3xPp3gInSc2
nadarmo	nadarmo	k6eAd1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
starověku	starověk	k1gInSc6
se	se	k3xPyFc4
věřilo	věřit	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
jméno	jméno	k1gNnSc1
vyjadřuje	vyjadřovat	k5eAaImIp3nS
i	i	k9
charakter	charakter	k1gInSc1
-	-	kIx~
znalost	znalost	k1gFnSc1
jména	jméno	k1gNnSc2
osoby	osoba	k1gFnSc2
tudíž	tudíž	k8xC
znamenala	znamenat	k5eAaImAgFnS
i	i	k8xC
znalost	znalost	k1gFnSc1
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
charakteru	charakter	k1gInSc2
a	a	k8xC
vnitřního	vnitřní	k2eAgNnSc2d1
já	já	k3xPp1nSc1
<g/>
,	,	kIx,
znalost	znalost	k1gFnSc4
jmen	jméno	k1gNnPc2
bohů	bůh	k1gMnPc2
pak	pak	k6eAd1
měla	mít	k5eAaImAgFnS
umožňovat	umožňovat	k5eAaImF
ostatním	ostatní	k2eAgMnPc3d1
bohům	bůh	k1gMnPc3
nebo	nebo	k8xC
i	i	k9
lidem	člověk	k1gMnPc3
tyto	tento	k3xDgMnPc4
bohy	bůh	k1gMnPc4
vzývat	vzývat	k5eAaImF
<g/>
,	,	kIx,
eventuálně	eventuálně	k6eAd1
mohli	moct	k5eAaImAgMnP
svého	svůj	k3xOyFgMnSc4
boha	bůh	k1gMnSc4
"	"	kIx"
<g/>
přemluvit	přemluvit	k5eAaPmF
<g/>
"	"	kIx"
či	či	k8xC
jinak	jinak	k6eAd1
jej	on	k3xPp3gMnSc4
přimět	přimět	k5eAaPmF
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
konal	konat	k5eAaImAgInS
dle	dle	k7c2
jejich	jejich	k3xOp3gNnPc2
přání	přání	k1gNnPc2
-	-	kIx~
báje	báje	k1gFnSc1
o	o	k7c6
moci	moc	k1gFnSc6
božských	božský	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
a	a	k8xC
o	o	k7c6
síle	síla	k1gFnSc6
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
propůjčují	propůjčovat	k5eAaImIp3nP
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
známé	známý	k2eAgFnPc1d1
již	již	k9
ze	z	k7c2
starého	starý	k2eAgInSc2d1
Egypta	Egypt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Definice	definice	k1gFnSc1
víry	víra	k1gFnSc2
v	v	k7c6
judaismu	judaismus	k1gInSc6
</s>
<s>
Judaismus	judaismus	k1gInSc1
odmítá	odmítat	k5eAaImIp3nS
dogmata	dogma	k1gNnPc4
a	a	k8xC
podobné	podobný	k2eAgNnSc4d1
„	„	k?
<g/>
nezvratné	zvratný	k2eNgFnSc2d1
pravdy	pravda	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
dlouhou	dlouhý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
bylo	být	k5eAaImAgNnS
jediným	jediný	k2eAgNnSc7d1
dogmatem	dogma	k1gNnSc7
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
Tóra	tóra	k1gFnSc1
je	být	k5eAaImIp3nS
Boží	boží	k2eAgInSc1d1
Zákon	zákon	k1gInSc1
seslaný	seslaný	k2eAgInSc1d1
Bohem	bůh	k1gMnSc7
a	a	k8xC
Izrael	Izrael	k1gInSc4
jej	on	k3xPp3gMnSc4
následuje	následovat	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
dodržel	dodržet	k5eAaPmAgMnS
podmínky	podmínka	k1gFnPc4
smlouvy	smlouva	k1gFnSc2
s	s	k7c7
Bohem	bůh	k1gMnSc7
uzavřené	uzavřený	k2eAgInPc4d1
<g/>
,	,	kIx,
totiž	totiž	k9
vyvolení	vyvolení	k1gNnSc2
výměnou	výměna	k1gFnSc7
za	za	k7c4
následování	následování	k1gNnPc4
jediného	jediný	k2eAgMnSc4d1
<g/>
,	,	kIx,
pravého	pravý	k2eAgMnSc4d1
Boha	bůh	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Situaci	situace	k1gFnSc4
v	v	k7c6
období	období	k1gNnSc6
na	na	k7c6
přelomu	přelom	k1gInSc6
letopočtů	letopočet	k1gInPc2
dobře	dobře	k6eAd1
ilustruje	ilustrovat	k5eAaBmIp3nS
následující	následující	k2eAgInSc1d1
příběh	příběh	k1gInSc1
z	z	k7c2
Talmudu	talmud	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
Přišel	přijít	k5eAaPmAgMnS
jeden	jeden	k4xCgMnSc1
pohan	pohan	k1gMnSc1
k	k	k7c3
Šamajovi	Šamaj	k1gMnSc3
a	a	k8xC
řekl	říct	k5eAaPmAgInS
mu	on	k3xPp3gMnSc3
<g/>
:	:	kIx,
Stanu	stanout	k5eAaPmIp1nS
se	s	k7c7
židem	žid	k1gMnSc7
<g/>
,	,	kIx,
pokud	pokud	k8xS
mě	já	k3xPp1nSc4
naučíš	naučit	k5eAaPmIp2nS
celému	celý	k2eAgNnSc3d1
učení	učení	k1gNnSc3
Tóry	tóra	k1gFnSc2
po	po	k7c4
dobu	doba	k1gFnSc4
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
vydržím	vydržet	k5eAaPmIp1nS
stát	stát	k5eAaPmF,k5eAaImF
na	na	k7c6
jedné	jeden	k4xCgFnSc6
noze	noha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šamaj	Šamaj	k1gMnSc1
se	se	k3xPyFc4
rozlítil	rozlítit	k5eAaPmAgMnS
<g/>
,	,	kIx,
neboť	neboť	k8xC
to	ten	k3xDgNnSc1
považoval	považovat	k5eAaImAgInS
za	za	k7c4
drzost	drzost	k1gFnSc4
a	a	k8xC
pohana	pohan	k1gMnSc4
vyhnal	vyhnat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tentýž	týž	k3xTgInSc4
pohan	pohan	k1gMnSc1
přišel	přijít	k5eAaPmAgMnS
k	k	k7c3
Hilelovi	Hilel	k1gMnSc3
a	a	k8xC
učinil	učinit	k5eAaImAgMnS,k5eAaPmAgMnS
mu	on	k3xPp3gMnSc3
tentýž	týž	k3xTgInSc4
návrh	návrh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hilel	Hilel	k1gMnSc1
řekl	říct	k5eAaPmAgMnS
<g/>
:	:	kIx,
Co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
nechceš	chtít	k5eNaImIp2nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
druzí	druhý	k4xOgMnPc1
činili	činit	k5eAaImAgMnP
tobě	ty	k3xPp2nSc3
<g/>
,	,	kIx,
nečiň	činit	k5eNaImRp2nS
ty	ty	k3xPp2nSc1
jim	on	k3xPp3gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
podstatou	podstata	k1gFnSc7
Tóry	tóra	k1gFnSc2
<g/>
,	,	kIx,
vše	všechen	k3xTgNnSc1
ostatní	ostatní	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
jen	jen	k9
komentář	komentář	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nyní	nyní	k6eAd1
jdi	jít	k5eAaImRp2nS
a	a	k8xC
uč	učit	k5eAaImRp2nS
se	se	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
V	v	k7c6
talmudickém	talmudický	k2eAgNnSc6d1
období	období	k1gNnSc6
nacházíme	nacházet	k5eAaImIp1nP
další	další	k2eAgInPc4d1
pokusy	pokus	k1gInPc4
zjednodušit	zjednodušit	k5eAaPmF
a	a	k8xC
zestručnit	zestručnit	k5eAaPmF
zásady	zásada	k1gFnPc4
víry	víra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
traktátu	traktát	k1gInSc6
Makot	Makota	k1gFnPc2
23	#num#	k4
<g/>
b-	b-	k?
<g/>
24	#num#	k4
<g/>
a	a	k8xC
se	se	k3xPyFc4
učí	učit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Bůh	bůh	k1gMnSc1
dal	dát	k5eAaPmAgMnS
Mojžíšovi	Mojžíš	k1gMnSc3
613	#num#	k4
přikázání	přikázání	k1gNnSc2
<g/>
,	,	kIx,
David	David	k1gMnSc1
je	být	k5eAaImIp3nS
zestručnil	zestručnit	k5eAaPmAgMnS
na	na	k7c4
11	#num#	k4
(	(	kIx(
<g/>
Žalm	žalm	k1gInSc1
15	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Izajáš	Izajáš	k1gFnSc1
na	na	k7c4
šest	šest	k4xCc4
(	(	kIx(
<g/>
Iz	Iz	k1gFnSc1
33	#num#	k4
<g/>
,	,	kIx,
15	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Micheáš	Micheáš	k1gFnSc4
na	na	k7c4
tři	tři	k4xCgInPc4
(	(	kIx(
<g/>
Mi	já	k3xPp1nSc3
6	#num#	k4
<g/>
,	,	kIx,
8	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Abakuk	Abakuk	k1gInSc1
na	na	k7c4
jedno	jeden	k4xCgNnSc4
a	a	k8xC
to	ten	k3xDgNnSc1
„	„	k?
<g/>
Spravedlivý	spravedlivý	k2eAgInSc1d1
z	z	k7c2
víry	víra	k1gFnSc2
živ	živ	k2eAgMnSc1d1
bude	být	k5eAaImBp3nS
<g/>
“	“	k?
(	(	kIx(
<g/>
Ab	Ab	k1gFnSc1
2	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Maimonidových	Maimonidový	k2eAgInPc2d1
13	#num#	k4
článků	článek	k1gInPc2
víry	víra	k1gFnSc2
</s>
<s>
Nutnost	nutnost	k1gFnSc1
mít	mít	k5eAaImF
stručné	stručný	k2eAgInPc1d1
základy	základ	k1gInPc1
a	a	k8xC
principy	princip	k1gInPc1
víry	víra	k1gFnSc2
se	se	k3xPyFc4
ukazovala	ukazovat	k5eAaImAgFnS
stále	stále	k6eAd1
více	hodně	k6eAd2
po	po	k7c6
nástupu	nástup	k1gInSc6
křesťanství	křesťanství	k1gNnSc2
a	a	k8xC
islámu	islám	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
vzniklo	vzniknout	k5eAaPmAgNnS
mnoho	mnoho	k4c1
pokusů	pokus	k1gInPc2
shrnout	shrnout	k5eAaPmF
učení	učení	k1gNnSc4
judaismu	judaismus	k1gInSc2
do	do	k7c2
několika	několik	k4yIc2
vět	věta	k1gFnPc2
<g/>
,	,	kIx,
nejuznávanějším	uznávaný	k2eAgMnPc3d3
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
Třináct	třináct	k4xCc1
článků	článek	k1gInPc2
víry	víra	k1gFnSc2
(	(	kIx(
<g/>
Šaloš	Šaloš	k1gMnSc1
esre	esr	k1gFnSc2
ikarim	ikarim	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
je	být	k5eAaImIp3nS
Maimonides	Maimonides	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
je	být	k5eAaImIp3nS
sepsal	sepsat	k5eAaPmAgMnS
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
komentáři	komentář	k1gInSc6
k	k	k7c3
10	#num#	k4
<g/>
.	.	kIx.
kapitole	kapitola	k1gFnSc6
Mišny	Mišna	k1gFnSc2
Sanhedrin	sanhedrin	k1gInSc1
(	(	kIx(
<g/>
perek	perko	k1gNnPc2
Chelek	Chelka	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třináct	třináct	k4xCc1
článků	článek	k1gInPc2
ve	v	k7c6
stručnosti	stručnost	k1gFnSc6
zní	znět	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
Bůh	bůh	k1gMnSc1
existuje	existovat	k5eAaImIp3nS
</s>
<s>
Bůh	bůh	k1gMnSc1
je	být	k5eAaImIp3nS
jeden	jeden	k4xCgInSc4
</s>
<s>
Bůh	bůh	k1gMnSc1
je	být	k5eAaImIp3nS
netělesný	tělesný	k2eNgMnSc1d1
</s>
<s>
Bůh	bůh	k1gMnSc1
je	být	k5eAaImIp3nS
věčný	věčný	k2eAgInSc4d1
</s>
<s>
Jedině	jedině	k6eAd1
Bůh	bůh	k1gMnSc1
sám	sám	k3xTgMnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
uctíván	uctívat	k5eAaImNgMnS
</s>
<s>
Proroctví	proroctví	k1gNnPc1
zaznamenaná	zaznamenaný	k2eAgNnPc1d1
v	v	k7c6
Bibli	bible	k1gFnSc6
jsou	být	k5eAaImIp3nP
pravdivá	pravdivý	k2eAgNnPc1d1
</s>
<s>
Mojžíš	Mojžíš	k1gMnSc1
byl	být	k5eAaImAgMnS
největším	veliký	k2eAgMnSc7d3
prorokem	prorok	k1gMnSc7
Izraele	Izrael	k1gInSc2
</s>
<s>
Tóra	tóra	k1gFnSc1
je	být	k5eAaImIp3nS
božského	božský	k2eAgInSc2d1
původu	původ	k1gInSc2
</s>
<s>
Tóra	tóra	k1gFnSc1
je	být	k5eAaImIp3nS
neměnná	neměnný	k2eAgFnSc1d1,k2eNgFnSc1d1
a	a	k8xC
nezměnitelná	změnitelný	k2eNgFnSc1d1
</s>
<s>
Bůh	bůh	k1gMnSc1
je	být	k5eAaImIp3nS
vševědoucí	vševědoucí	k2eAgMnSc1d1
</s>
<s>
Bůh	bůh	k1gMnSc1
trestá	trestat	k5eAaImIp3nS
a	a	k8xC
odměňuje	odměňovat	k5eAaImIp3nS
v	v	k7c6
posmrtném	posmrtný	k2eAgInSc6d1
životě	život	k1gInSc6
</s>
<s>
Mesiáš	Mesiáš	k1gMnSc1
jistě	jistě	k9
přijde	přijít	k5eAaPmIp3nS
</s>
<s>
Nastane	nastat	k5eAaPmIp3nS
vzkříšení	vzkříšení	k1gNnSc1
mrtvých	mrtvý	k1gMnPc2
</s>
<s>
Maimonidových	Maimonidový	k2eAgInPc2d1
třináct	třináct	k4xCc4
článků	článek	k1gInPc2
nebylo	být	k5eNaImAgNnS
vždy	vždy	k6eAd1
bezvýhradně	bezvýhradně	k6eAd1
uznáváno	uznáván	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokonce	dokonce	k9
ani	ani	k9
dnes	dnes	k6eAd1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jsou	být	k5eAaImIp3nP
součástí	součást	k1gFnSc7
většiny	většina	k1gFnSc2
sidurů	sidur	k1gMnPc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
brány	brána	k1gFnPc1
více	hodně	k6eAd2
jako	jako	k8xS,k8xC
doporučení	doporučení	k1gNnSc4
<g/>
,	,	kIx,
než	než	k8xS
jako	jako	k8xS,k8xC
závazné	závazný	k2eAgNnSc1d1
dogma	dogma	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Polemika	polemika	k1gFnSc1
po	po	k7c6
Maimonidovi	Maimonid	k1gMnSc6
</s>
<s>
Kromě	kromě	k7c2
těchto	tento	k3xDgInPc2
Třinácti	třináct	k4xCc2
článků	článek	k1gInPc2
existují	existovat	k5eAaImIp3nP
i	i	k9
další	další	k2eAgFnPc1d1
definice	definice	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
sice	sice	k8xC
nejsou	být	k5eNaImIp3nP
tak	tak	k6eAd1
známé	známý	k2eAgFnPc1d1
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
se	se	k3xPyFc4
týče	týkat	k5eAaImIp3nS
závaznosti	závaznost	k1gFnSc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
na	na	k7c6
stejné	stejný	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
jako	jako	k9
články	článek	k1gInPc4
Maimonidovy	Maimonidův	k2eAgInPc4d1
<g/>
:	:	kIx,
</s>
<s>
Toto	tento	k3xDgNnSc1
jsou	být	k5eAaImIp3nP
tři	tři	k4xCgInPc1
články	článek	k1gInPc1
víry	víra	k1gFnSc2
(	(	kIx(
<g/>
a	a	k8xC
z	z	k7c2
nich	on	k3xPp3gInPc2
vyplývající	vyplývající	k2eAgNnPc1d1
osm	osm	k4xCc1
„	„	k?
<g/>
kořenů	kořen	k1gInPc2
<g/>
“	“	k?
<g/>
)	)	kIx)
od	od	k7c2
Josefa	Josef	k1gMnSc2
Alba	album	k1gNnSc2
(	(	kIx(
<g/>
zemř	zemř	k1gFnSc1
<g/>
.	.	kIx.
1440	#num#	k4
<g/>
)	)	kIx)
z	z	k7c2
jeho	jeho	k3xOp3gFnSc2
knihy	kniha	k1gFnSc2
Sefer	Sefer	k1gMnSc1
ha-ikarim	ha-ikarim	k1gMnSc1
<g/>
:	:	kIx,
</s>
<s>
Víra	víra	k1gFnSc1
v	v	k7c4
Boha	bůh	k1gMnSc4
</s>
<s>
Bůh	bůh	k1gMnSc1
je	být	k5eAaImIp3nS
jeden	jeden	k4xCgInSc4
</s>
<s>
Bůh	bůh	k1gMnSc1
je	být	k5eAaImIp3nS
netělesný	tělesný	k2eNgMnSc1d1
</s>
<s>
Bůh	bůh	k1gMnSc1
je	být	k5eAaImIp3nS
nadčasový	nadčasový	k2eAgMnSc1d1
</s>
<s>
Bůh	bůh	k1gMnSc1
je	být	k5eAaImIp3nS
naprosto	naprosto	k6eAd1
čistý	čistý	k2eAgInSc1d1
(	(	kIx(
<g/>
neposkvrněný	poskvrněný	k2eNgInSc1d1
<g/>
,	,	kIx,
dokonalý	dokonalý	k2eAgInSc1d1
<g/>
)	)	kIx)
</s>
<s>
Víra	víra	k1gFnSc1
v	v	k7c4
božskost	božskost	k1gFnSc4
Tóry	tóra	k1gFnSc2
</s>
<s>
Víra	víra	k1gFnSc1
v	v	k7c4
odměnu	odměna	k1gFnSc4
a	a	k8xC
trest	trest	k1gInSc4
</s>
<s>
Bůh	bůh	k1gMnSc1
je	být	k5eAaImIp3nS
prozřetelný	prozřetelný	k2eAgMnSc1d1
</s>
<s>
Bůh	bůh	k1gMnSc1
je	být	k5eAaImIp3nS
vševědoucí	vševědoucí	k2eAgMnSc1d1
</s>
<s>
Proroctví	proroctví	k1gNnPc1
Boží	boží	k2eAgNnPc1d1
jsou	být	k5eAaImIp3nP
pravdivá	pravdivý	k2eAgNnPc1d1
</s>
<s>
Boží	boží	k2eAgMnPc1d1
proroci	prorok	k1gMnPc1
(	(	kIx(
<g/>
bibličtí	biblický	k2eAgMnPc1d1
<g/>
)	)	kIx)
byli	být	k5eAaImAgMnP
skutečnými	skutečný	k2eAgMnPc7d1
proroky	prorok	k1gMnPc7
</s>
<s>
K	k	k7c3
těmto	tento	k3xDgFnPc3
celkem	celkem	k6eAd1
jedenácti	jedenáct	k4xCc3
zásadám	zásada	k1gFnPc3
předkládá	předkládat	k5eAaImIp3nS
Albo	alba	k1gFnSc5
ještě	ještě	k9
tzv.	tzv.	kA
„	„	k?
<g/>
Šest	šest	k4xCc1
věroučných	věroučný	k2eAgNnPc2d1
mínění	mínění	k1gNnPc2
<g/>
“	“	k?
<g/>
:	:	kIx,
</s>
<s>
Stvoření	stvoření	k1gNnSc1
světa	svět	k1gInSc2
z	z	k7c2
ničeho	nic	k3yNnSc2
(	(	kIx(
<g/>
creatio	creatio	k1gNnSc1
ex	ex	k6eAd1
nihilo	nihit	k5eAaImAgNnS,k5eAaPmAgNnS,k5eAaBmAgNnS
<g/>
)	)	kIx)
</s>
<s>
Mojžíš	Mojžíš	k1gMnSc1
byl	být	k5eAaImAgMnS
největším	veliký	k2eAgInPc3d3
z	z	k7c2
proroků	prorok	k1gMnPc2
</s>
<s>
Tóra	tóra	k1gFnSc1
je	být	k5eAaImIp3nS
nezměnitelná	změnitelný	k2eNgFnSc1d1
</s>
<s>
Dodržování	dodržování	k1gNnSc1
Tóry	tóra	k1gFnSc2
vede	vést	k5eAaImIp3nS
k	k	k7c3
osobnímu	osobní	k2eAgNnSc3d1
spasení	spasení	k1gNnSc3
(	(	kIx(
<g/>
podílu	podíl	k1gInSc2
na	na	k7c6
budoucím	budoucí	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
)	)	kIx)
</s>
<s>
Zmrtvýchvstání	zmrtvýchvstání	k1gNnSc1
</s>
<s>
Mesiáš	Mesiáš	k1gMnSc1
</s>
<s>
Zatímco	zatímco	k8xS
Jicchak	Jicchak	k1gInSc1
Arama	Aram	k1gMnSc2
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
díle	dílo	k1gNnSc6
Akedat	Akedat	k1gMnSc1
Jicchak	Jicchak	k1gMnSc1
předkládá	předkládat	k5eAaImIp3nS
tato	tento	k3xDgNnPc4
<g/>
:	:	kIx,
</s>
<s>
Stvoření	stvoření	k1gNnSc1
světa	svět	k1gInSc2
z	z	k7c2
ničeho	nic	k3yNnSc2
(	(	kIx(
<g/>
ex	ex	k6eAd1
nihilo	nihit	k5eAaBmAgNnS,k5eAaImAgNnS,k5eAaPmAgNnS
<g/>
)	)	kIx)
</s>
<s>
Víra	víra	k1gFnSc1
v	v	k7c4
božskost	božskost	k1gFnSc4
Tóry	tóra	k1gFnSc2
</s>
<s>
Víra	víra	k1gFnSc1
v	v	k7c4
posmrtný	posmrtný	k2eAgInSc4d1
život	život	k1gInSc4
</s>
<s>
Je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
mít	mít	k5eAaImF
na	na	k7c6
paměti	paměť	k1gFnSc6
<g/>
,	,	kIx,
že	že	k8xS
tyto	tento	k3xDgInPc4
pokusy	pokus	k1gInPc4
o	o	k7c4
vytvoření	vytvoření	k1gNnSc4
židovského	židovský	k2eAgNnSc2d1
dogmatu	dogma	k1gNnSc2
probíhaly	probíhat	k5eAaImAgFnP
především	především	k6eAd1
na	na	k7c6
půdě	půda	k1gFnSc6
Španělska	Španělsko	k1gNnSc2
pod	pod	k7c7
vládou	vláda	k1gFnSc7
křesťanů	křesťan	k1gMnPc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
v	v	k7c6
období	období	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Židé	Žid	k1gMnPc1
byli	být	k5eAaImAgMnP
vystaveni	vystavit	k5eAaPmNgMnP
velkému	velký	k2eAgNnSc3d1
pronásledování	pronásledování	k1gNnSc3
a	a	k8xC
nuceni	nutit	k5eAaImNgMnP
k	k	k7c3
tzv.	tzv.	kA
disputacím	disputace	k1gFnPc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
měli	mít	k5eAaImAgMnP
„	„	k?
<g/>
obhajovat	obhajovat	k5eAaImF
<g/>
“	“	k?
své	svůj	k3xOyFgNnSc4
učení	učení	k1gNnSc4
proti	proti	k7c3
křesťanům	křesťan	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
tyto	tento	k3xDgFnPc4
variace	variace	k1gFnPc4
na	na	k7c4
vyznání	vyznání	k1gNnSc4
víry	víra	k1gFnSc2
poměrně	poměrně	k6eAd1
dobře	dobře	k6eAd1
ilustrují	ilustrovat	k5eAaBmIp3nP
základní	základní	k2eAgInPc1d1
body	bod	k1gInPc1
věrouky	věrouka	k1gFnSc2
židovského	židovský	k2eAgNnSc2d1
náboženství	náboženství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
FISHBANE	FISHBANE	kA
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
A.	A.	kA
Judaismus	judaismus	k1gInSc1
<g/>
:	:	kIx,
Zjevení	zjevení	k1gNnSc1
a	a	k8xC
tradice	tradice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Prostor	prostor	k1gInSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
192	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7260	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
86	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
NEWMAN	NEWMAN	kA
<g/>
,	,	kIx,
Ja	Ja	k?
<g/>
'	'	kIx"
<g/>
akov	akov	k1gMnSc1
<g/>
;	;	kIx,
SIVAN	SIVAN	kA
<g/>
,	,	kIx,
Gavri	Gavri	k1gNnSc1
<g/>
'	'	kIx"
<g/>
el.	el.	k?
Judaismus	judaismus	k1gInSc1
od	od	k7c2
A	A	kA
do	do	k7c2
Z.	Z.	kA
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Sefer	Sefer	k1gMnSc1
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
.	.	kIx.
285	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
900895	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
SCHUBERT	Schubert	k1gMnSc1
<g/>
,	,	kIx,
Kurt	Kurt	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Židovské	židovská	k1gFnSc2
náboženství	náboženství	k1gNnPc2
v	v	k7c6
proměnách	proměna	k1gFnPc6
věků	věk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
288	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7021	#num#	k4
<g/>
-	-	kIx~
<g/>
303	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Dílo	dílo	k1gNnSc1
Sichat	Sichat	k1gFnSc2
Jicchak	Jicchak	k1gInSc1
<g/>
/	/	kIx~
<g/>
Třináct	třináct	k4xCc4
článků	článek	k1gInPc2
víry	víra	k1gFnSc2
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hebraistika	hebraistika	k1gFnSc1
|	|	kIx~
Náboženství	náboženství	k1gNnSc1
</s>
