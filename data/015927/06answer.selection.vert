<s>
Křemen	křemen	k1gInSc1
je	být	k5eAaImIp3nS
minerál	minerál	k1gInSc4
s	s	k7c7
chemickým	chemický	k2eAgInSc7d1
vzorcem	vzorec	k1gInSc7
SiO	SiO	kA
<g/>
2	#num#	k4
(	(	kIx(
<g/>
oxid	oxid	k1gInSc1
křemičitý	křemičitý	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hojně	hojně	k6eAd1
se	se	k3xPyFc4
vyskytující	vyskytující	k2eAgMnSc1d1
v	v	k7c6
litosféře	litosféra	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
tvoří	tvořit	k5eAaImIp3nS
jeden	jeden	k4xCgInSc1
z	z	k7c2
nejdůležitějších	důležitý	k2eAgInPc2d3
minerálů	minerál	k1gInPc2
<g/>
.	.	kIx.
</s>