<s>
Daniela	Daniela	k1gFnSc1	Daniela
Hantuchová	Hantuchová	k1gFnSc1	Hantuchová
je	být	k5eAaImIp3nS	být
slovenská	slovenský	k2eAgFnSc1d1	slovenská
tenistka	tenistka	k1gFnSc1	tenistka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
do	do	k7c2	do
profesionálního	profesionální	k2eAgInSc2d1	profesionální
tenisu	tenis	k1gInSc2	tenis
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
čtyři	čtyři	k4xCgInPc4	čtyři
grandslamové	grandslamový	k2eAgInPc4d1	grandslamový
turnaje	turnaj	k1gInPc4	turnaj
ve	v	k7c6	v
smíšené	smíšený	k2eAgFnSc6d1	smíšená
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
Prohrála	prohrát	k5eAaPmAgFnS	prohrát
tři	tři	k4xCgNnPc4	tři
finálová	finálový	k2eAgNnPc4d1	finálové
utkání	utkání	k1gNnPc4	utkání
na	na	k7c4	na
Grand	grand	k1gMnSc1	grand
Slamu	slam	k1gInSc2	slam
v	v	k7c6	v
ženské	ženský	k2eAgFnSc6d1	ženská
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
a	a	k8xC	a
jedno	jeden	k4xCgNnSc4	jeden
ve	v	k7c6	v
smíšené	smíšený	k2eAgFnSc6d1	smíšená
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
WTA	WTA	kA	WTA
dokázala	dokázat	k5eAaPmAgFnS	dokázat
zvítězit	zvítězit	k5eAaPmF	zvítězit
na	na	k7c6	na
3	[number]	k4	3
turnajích	turnaj	k1gInPc6	turnaj
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
a	a	k8xC	a
v	v	k7c6	v
8	[number]	k4	8
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
žebříčku	žebříček	k1gInSc6	žebříček
WTA	WTA	kA	WTA
pro	pro	k7c4	pro
dvouhru	dvouhra	k1gFnSc4	dvouhra
byla	být	k5eAaImAgFnS	být
nejvýše	nejvýše	k6eAd1	nejvýše
klasifikována	klasifikovat	k5eAaImNgFnS	klasifikovat
na	na	k7c4	na
5	[number]	k4	5
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
a	a	k8xC	a
pro	pro	k7c4	pro
čtyřhru	čtyřhra	k1gFnSc4	čtyřhra
na	na	k7c4	na
5	[number]	k4	5
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Daniela	Daniela	k1gFnSc1	Daniela
Hantuchová	Hantuchová	k1gFnSc1	Hantuchová
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
20	[number]	k4	20
zápasů	zápas	k1gInPc2	zápas
ve	v	k7c6	v
Fed	Fed	k1gFnSc6	Fed
Cupu	cup	k1gInSc2	cup
za	za	k7c4	za
tým	tým	k1gInSc4	tým
Slovenska	Slovensko	k1gNnSc2	Slovensko
s	s	k7c7	s
bilancí	bilance	k1gFnSc7	bilance
22-7	[number]	k4	22-7
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
a	a	k8xC	a
4-3	[number]	k4	4-3
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
Stav	stav	k1gInSc1	stav
k	k	k7c3	k
7	[number]	k4	7
<g/>
.	.	kIx.	.
březnu	březen	k1gInSc6	březen
2010	[number]	k4	2010
po	po	k7c6	po
odehraném	odehraný	k2eAgInSc6d1	odehraný
turnaji	turnaj	k1gInSc6	turnaj
Monterrey	Monterrea	k1gFnSc2	Monterrea
Open	Open	k1gNnSc4	Open
v	v	k7c4	v
Monterrey	Monterrea	k1gFnPc4	Monterrea
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc4	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
NH	NH	kA	NH
=	=	kIx~	=
turnaj	turnaj	k1gInSc1	turnaj
se	se	k3xPyFc4	se
nekonal	konat	k5eNaImAgInS	konat
<g/>
.	.	kIx.	.
1	[number]	k4	1
Prohra	prohra	k1gFnSc1	prohra
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
2	[number]	k4	2
Její	její	k3xOp3gFnSc1	její
výhra	výhra	k1gFnSc1	výhra
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
byla	být	k5eAaImAgFnS	být
bez	bez	k7c2	bez
boje	boj	k1gInSc2	boj
(	(	kIx(	(
<g/>
w	w	k?	w
<g/>
/	/	kIx~	/
<g/>
o	o	k7c4	o
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nezapočítává	započítávat	k5eNaImIp3nS	započítávat
do	do	k7c2	do
oficiální	oficiální	k2eAgFnSc2d1	oficiální
statistiky	statistika	k1gFnSc2	statistika
výhry-prohry	výhryrohra	k1gFnSc2	výhry-prohra
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g />
.	.	kIx.	.
</s>
<s>
Výhra	výhra	k1gFnSc1	výhra
dvou	dva	k4xCgFnPc2	dva
kvalifikčních	kvalifikční	k2eAgFnPc2d1	kvalifikční
kol	kola	k1gFnPc2	kola
a	a	k8xC	a
postup	postup	k1gInSc1	postup
do	do	k7c2	do
hlavní	hlavní	k2eAgFnSc2d1	hlavní
soutěže	soutěž	k1gFnSc2	soutěž
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
4	[number]	k4	4
Výhra	výhra	k1gFnSc1	výhra
tří	tři	k4xCgFnPc2	tři
kvalifikačních	kvalifikační	k2eAgFnPc2d1	kvalifikační
kol	kola	k1gFnPc2	kola
a	a	k8xC	a
postup	postup	k1gInSc1	postup
do	do	k7c2	do
hlavní	hlavní	k2eAgFnSc2d1	hlavní
soutěže	soutěž	k1gFnSc2	soutěž
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
5	[number]	k4	5
Její	její	k3xOp3gFnSc1	její
statistika	statistika	k1gFnSc1	statistika
výhry-prohry	výhryrohra	k1gFnSc2	výhry-prohra
(	(	kIx(	(
<g/>
win-loss	winossit	k5eAaPmRp2nS	win-lossit
record	record	k1gInSc1	record
<g/>
)	)	kIx)	)
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
všechny	všechen	k3xTgFnPc4	všechen
utkání	utkání	k1gNnSc1	utkání
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
jsou	být	k5eAaImIp3nP	být
uváděné	uváděný	k2eAgFnPc1d1	uváděná
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
WTA	WTA	kA	WTA
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
zápasů	zápas	k1gInPc2	zápas
na	na	k7c6	na
challengerech	challenger	k1gInPc6	challenger
a	a	k8xC	a
ve	v	k7c6	v
Fed	Fed	k1gFnSc6	Fed
Cupu	cup	k1gInSc2	cup
<g/>
.	.	kIx.	.
</s>
<s>
Stav	stav	k1gInSc1	stav
k	k	k7c3	k
březnu	březen	k1gInSc3	březen
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Uveden	uveden	k2eAgInSc1d1	uveden
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
vyhraných	vyhraný	k2eAgInPc2d1	vyhraný
<g/>
–	–	k?	–
<g/>
prohraných	prohraný	k2eAgInPc2d1	prohraný
zápasů	zápas	k1gInPc2	zápas
Hantuchové	Hantuchový	k2eAgNnSc1d1	Hantuchové
proti	proti	k7c3	proti
hráčkám	hráčka	k1gFnPc3	hráčka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
někdy	někdy	k6eAd1	někdy
klasifikovány	klasifikovat	k5eAaImNgInP	klasifikovat
v	v	k7c6	v
první	první	k4xOgFnSc6	první
desítce	desítka	k1gFnSc6	desítka
žebříčku	žebříček	k1gInSc2	žebříček
WTA	WTA	kA	WTA
<g/>
.	.	kIx.	.
</s>
<s>
Tučně	tučně	k6eAd1	tučně
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgFnP	uvést
světové	světový	k2eAgFnPc1d1	světová
tenisové	tenisový	k2eAgFnPc1d1	tenisová
jedničky	jednička	k1gFnPc1	jednička
<g/>
.	.	kIx.	.
</s>
<s>
Patty	Patt	k1gInPc1	Patt
Schnyderová	Schnyderová	k1gFnSc1	Schnyderová
9	[number]	k4	9
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
Barbara	Barbara	k1gFnSc1	Barbara
Schett	Schetta	k1gFnPc2	Schetta
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
Ai	Ai	k1gFnSc1	Ai
Sugijama	Sugijama	k1gFnSc1	Sugijama
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
Světlana	Světlana	k1gFnSc1	Světlana
Kuzněcovová	Kuzněcovová	k1gFnSc1	Kuzněcovová
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
Marion	Marion	k1gInSc1	Marion
Bartoliová	Bartoliový	k2eAgFnSc1d1	Bartoliová
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
Jelena	Jelena	k1gFnSc1	Jelena
Dokićová	Dokićová	k1gFnSc1	Dokićová
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
Magdalena	Magdalena	k1gFnSc1	Magdalena
Malejevová	Malejevová	k1gFnSc1	Malejevová
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
Martina	Martina	k1gFnSc1	Martina
<g />
.	.	kIx.	.
</s>
<s>
Hingisová	Hingisový	k2eAgFnSc1d1	Hingisová
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
Conchita	Conchita	k1gFnSc1	Conchita
Martínezová	Martínezová	k1gFnSc1	Martínezová
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
Alicia	Alicium	k1gNnSc2	Alicium
Moliková	Molikový	k2eAgFnSc1d1	Moliková
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
Dinara	Dinara	k1gFnSc1	Dinara
Safinová	Safinová	k1gFnSc1	Safinová
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
Jelena	Jelena	k1gFnSc1	Jelena
Dementěvová	Dementěvová	k1gFnSc1	Dementěvová
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
11	[number]	k4	11
Iva	Iva	k1gFnSc1	Iva
Majoliová	Majoliový	k2eAgFnSc1d1	Majoliová
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
Chanda	Chanda	k1gFnSc1	Chanda
Rubinová	Rubinová	k1gFnSc1	Rubinová
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
Amanda	Amanda	k1gFnSc1	Amanda
Coetzerová	Coetzerová	k1gFnSc1	Coetzerová
<g />
.	.	kIx.	.
</s>
<s>
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
Justine	Justin	k1gMnSc5	Justin
Heninová	Heninový	k2eAgFnSc1d1	Heninová
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
Agnieszka	Agnieszka	k1gFnSc1	Agnieszka
Radwańska	Radwańska	k1gFnSc1	Radwańska
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
Anna	Anna	k1gFnSc1	Anna
Čakvetadzeová	Čakvetadzeová	k1gFnSc1	Čakvetadzeová
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
Amélie	Amélie	k1gFnSc1	Amélie
Mauresmová	Mauresmová	k1gFnSc1	Mauresmová
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
Viktoria	Viktoria	k1gFnSc1	Viktoria
Azarenková	Azarenkový	k2eAgFnSc1d1	Azarenková
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
Martina	Martina	k1gFnSc1	Martina
Navrátilová	Navrátilová	k1gFnSc1	Navrátilová
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
Flavia	Flavia	k1gFnSc1	Flavia
Pennettaová	Pennettaová	k1gFnSc1	Pennettaová
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s>
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
Nathalie	Nathalie	k1gFnSc1	Nathalie
Tauziatová	Tauziatový	k2eAgFnSc1d1	Tauziatový
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
Ana	Ana	k1gFnSc1	Ana
Ivanovičová	Ivanovičová	k1gFnSc1	Ivanovičová
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
Jelena	Jelena	k1gFnSc1	Jelena
Jankovićová	Jankovićová	k1gFnSc1	Jankovićová
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
Naděžda	Naděžda	k1gFnSc1	Naděžda
Petrovová	Petrovová	k1gFnSc1	Petrovová
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
Věra	Věra	k1gFnSc1	Věra
Zvonarevová	Zvonarevová	k1gFnSc1	Zvonarevová
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
Nicole	Nicole	k1gFnSc1	Nicole
Vaidišová	Vaidišová	k1gFnSc1	Vaidišová
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
Maria	Maria	k1gFnSc1	Maria
Šarapovová	Šarapovová	k1gFnSc1	Šarapovová
1	[number]	k4	1
<g/>
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
<g/>
7	[number]	k4	7
Serena	Serena	k1gFnSc1	Serena
Williamsová	Williamsová	k1gFnSc1	Williamsová
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
Kimiko	Kimika	k1gFnSc5	Kimika
Dateová	Dateový	k2eAgFnSc1d1	Dateový
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
Jennifer	Jennifero	k1gNnPc2	Jennifero
Capriatiová	Capriatiový	k2eAgFnSc1d1	Capriatiová
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
Anastasija	Anastasija	k1gFnSc1	Anastasija
Myskinová	Myskinová	k1gFnSc1	Myskinová
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
Caroline	Carolin	k1gInSc5	Carolin
Wozniacká	Wozniacký	k2eAgFnSc1d1	Wozniacká
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
Lindsay	Lindsaa	k1gFnSc2	Lindsaa
Davenportová	Davenportová	k1gFnSc1	Davenportová
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
Kim	Kim	k1gFnSc1	Kim
Clijstersová	Clijstersová	k1gFnSc1	Clijstersová
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
Venus	Venus	k1gInSc1	Venus
Williamsová	Williamsová	k1gFnSc1	Williamsová
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
</s>
