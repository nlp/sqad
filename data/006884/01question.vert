<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
slovenská	slovenský	k2eAgFnSc1d1	slovenská
tenistka	tenistka	k1gFnSc1	tenistka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
započala	započnout	k5eAaPmAgFnS	započnout
profesionální	profesionální	k2eAgFnSc4d1	profesionální
dráhu	dráha	k1gFnSc4	dráha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
a	a	k8xC	a
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
čtyři	čtyři	k4xCgInPc4	čtyři
grandslamové	grandslamový	k2eAgInPc4d1	grandslamový
turnaje	turnaj	k1gInPc4	turnaj
ve	v	k7c4	v
smíšení	smíšení	k1gNnSc4	smíšení
čtyřhře	čtyřhra	k1gFnSc3	čtyřhra
<g/>
?	?	kIx.	?
</s>
