<s>
Vlas	vlas	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Průřez	průřez	k1gInSc4
vlasovým	vlasový	k2eAgInSc7d1
váčkem	váček	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysvětlivky	vysvětlivka	k1gFnPc1
<g/>
:	:	kIx,
<g/>
hair	hair	k1gInSc1
–	–	k?
vlas	vlas	k1gInSc1
<g/>
/	/	kIx~
<g/>
chlupskin	chlupskin	k1gInSc1
surface	surface	k1gFnSc2
–	–	k?
pokožkasebum	pokožkasebum	k1gInSc1
–	–	k?
kožní	kožní	k2eAgInSc1d1
mazfollicle	mazfollicle	k1gInSc1
–	–	k?
vlasový	vlasový	k2eAgInSc1d1
folikulsebaceous	folikulsebaceous	k1gInSc1
gland	gland	k1gInSc1
–	–	k?
potní	potní	k2eAgFnSc1d1
žláza	žláza	k1gFnSc1
</s>
<s>
Vlas	vlas	k1gInSc1
a	a	k8xC
chlup	chlup	k1gInSc1
jsou	být	k5eAaImIp3nP
označení	označení	k1gNnSc4
pro	pro	k7c4
dva	dva	k4xCgInPc4
anatomicky	anatomicky	k6eAd1
téměř	téměř	k6eAd1
totožné	totožný	k2eAgInPc1d1
útvary	útvar	k1gInPc1
tvořené	tvořený	k2eAgInPc1d1
keratinem	keratin	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
na	na	k7c4
několik	několik	k4yIc4
výjimek	výjimka	k1gFnPc2
jsou	být	k5eAaImIp3nP
přítomny	přítomen	k2eAgInPc1d1
(	(	kIx(
<g/>
hlavně	hlavně	k9
ve	v	k7c6
formě	forma	k1gFnSc6
srsti	srst	k1gFnSc2
<g/>
)	)	kIx)
u	u	k7c2
všech	všecek	k3xTgMnPc2
savců	savec	k1gMnPc2
a	a	k8xC
jejich	jejich	k3xOp3gFnSc7
nejdůležitější	důležitý	k2eAgFnSc7d3
funkcí	funkce	k1gFnSc7
je	být	k5eAaImIp3nS
termoregulace	termoregulace	k1gFnSc1
(	(	kIx(
<g/>
ať	ať	k8xC,k8xS
už	už	k6eAd1
v	v	k7c6
teplém	teplý	k2eAgNnSc6d1
nebo	nebo	k8xC
chladném	chladný	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Stavba	stavba	k1gFnSc1
</s>
<s>
Lidský	lidský	k2eAgInSc1d1
vlas	vlas	k1gInSc1
<g/>
/	/	kIx~
<g/>
chlup	chlup	k1gInSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
dvou	dva	k4xCgFnPc2
základních	základní	k2eAgFnPc2d1
částí	část	k1gFnPc2
<g/>
:	:	kIx,
vlasový	vlasový	k2eAgInSc4d1
stvol	stvol	k1gInSc4
(	(	kIx(
<g/>
scapus	scapus	k1gInSc4
pili	pít	k5eAaImAgMnP
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
vyčnívá	vyčnívat	k5eAaImIp3nS
nad	nad	k7c4
pokožku	pokožka	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
kořen	kořen	k1gInSc1
vlasu	vlas	k1gInSc2
(	(	kIx(
<g/>
radix	radix	k1gInSc4
pili	pít	k5eAaImAgMnP
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
pod	pod	k7c7
její	její	k3xOp3gFnSc7
úrovní	úroveň	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejhlubší	hluboký	k2eAgFnSc7d3
částí	část	k1gFnSc7
kořene	kořen	k1gInSc2
je	být	k5eAaImIp3nS
tzv.	tzv.	kA
vlasová	vlasový	k2eAgFnSc1d1
cibulka	cibulka	k1gFnSc1
(	(	kIx(
<g/>
bulbus	bulbus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nad	nad	k7c7
ní	on	k3xPp3gFnSc7
následuje	následovat	k5eAaImIp3nS
zbytek	zbytek	k1gInSc4
kořene	kořen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cibulka	cibulka	k1gFnSc1
je	být	k5eAaImIp3nS
obalena	obalit	k5eAaPmNgFnS
vlasovými	vlasový	k2eAgFnPc7d1
pochvami	pochva	k1gFnPc7
(	(	kIx(
<g/>
dvěma	dva	k4xCgInPc7
epitely	epitel	k1gInPc7
a	a	k8xC
jednou	jednou	k6eAd1
vazivovou	vazivový	k2eAgFnSc4d1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
dohromady	dohromady	k6eAd1
se	se	k3xPyFc4
kořenu	kořen	k2eAgFnSc4d1
a	a	k8xC
kolem	kolem	k6eAd1
obalené	obalený	k2eAgFnSc6d1
pochvě	pochva	k1gFnSc6
říká	říkat	k5eAaImIp3nS
vlasový	vlasový	k2eAgInSc4d1
folikul	folikul	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnPc2
součástí	součást	k1gFnPc2
jsou	být	k5eAaImIp3nP
i	i	k9
mazové	mazový	k2eAgFnPc1d1
a	a	k8xC
potní	potní	k2eAgFnPc1d1
žlázy	žláza	k1gFnPc1
a	a	k8xC
cévní	cévní	k2eAgNnSc1d1
zásobení	zásobení	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vlasový	vlasový	k2eAgInSc1d1
stvol	stvol	k1gInSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
tří	tři	k4xCgFnPc2
vrstev	vrstva	k1gFnPc2
<g/>
:	:	kIx,
kutikuly	kutikula	k1gFnSc2
<g/>
,	,	kIx,
kortexu	kortex	k1gInSc2
a	a	k8xC
meduly	medula	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kutikula	kutikula	k1gFnSc1
(	(	kIx(
<g/>
šupinatá	šupinatý	k2eAgFnSc1d1
vrstva	vrstva	k1gFnSc1
<g/>
)	)	kIx)
tvoří	tvořit	k5eAaImIp3nP
přirozenou	přirozený	k2eAgFnSc4d1
vrstvu	vrstva	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
chrání	chránit	k5eAaImIp3nS
před	před	k7c7
vnějším	vnější	k2eAgNnSc7d1
prostředím	prostředí	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
správné	správný	k2eAgFnSc6d1
funkci	funkce	k1gFnSc6
mazové	mazový	k2eAgFnSc2d1
žlázy	žláza	k1gFnSc2
je	být	k5eAaImIp3nS
tato	tento	k3xDgFnSc1
vrstva	vrstva	k1gFnSc1
konzervována	konzervovat	k5eAaBmNgFnS
vlasovým	vlasový	k2eAgInSc7d1
mazem	maz	k1gInSc7
(	(	kIx(
<g/>
tukem	tuk	k1gInSc7
<g/>
)	)	kIx)
a	a	k8xC
vlas	vlas	k1gInSc1
méně	málo	k6eAd2
propouští	propouštět	k5eAaImIp3nS
škodliviny	škodlivina	k1gFnPc4
a	a	k8xC
vodné	vodný	k2eAgInPc4d1
roztoky	roztok	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skládá	skládat	k5eAaImIp3nS
se	se	k3xPyFc4
z	z	k7c2
5	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
souběžně	souběžně	k6eAd1
a	a	k8xC
stříškovitě	stříškovitě	k6eAd1
uspořádaných	uspořádaný	k2eAgFnPc2d1
odumřelých	odumřelý	k2eAgFnPc2d1
buněk	buňka	k1gFnPc2
(	(	kIx(
<g/>
šupin	šupina	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
k	k	k7c3
sobě	se	k3xPyFc3
přiléhají	přiléhat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šupiny	šupina	k1gFnPc1
se	se	k3xPyFc4
překrývají	překrývat	k5eAaImIp3nP
asi	asi	k9
do	do	k7c2
1	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
délky	délka	k1gFnSc2
a	a	k8xC
s	s	k7c7
osou	osa	k1gFnSc7
vlasu	vlas	k1gInSc2
tvoří	tvořit	k5eAaImIp3nP
úhel	úhel	k1gInSc4
asi	asi	k9
18	#num#	k4
<g/>
°	°	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednotlivé	jednotlivý	k2eAgFnPc1d1
vrstvy	vrstva	k1gFnPc1
jsou	být	k5eAaImIp3nP
pokryty	pokrýt	k5eAaPmNgFnP
jemnou	jemný	k2eAgFnSc7d1
membránou	membrána	k1gFnSc7
a	a	k8xC
vzájemně	vzájemně	k6eAd1
jsou	být	k5eAaImIp3nP
spojeny	spojit	k5eAaPmNgFnP
proteinovým	proteinový	k2eAgInSc7d1
a	a	k8xC
lipidovým	lipidový	k2eAgInSc7d1
tmelem	tmel	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Kortex	Kortex	k1gInSc1
(	(	kIx(
<g/>
kůra	kůra	k1gFnSc1
<g/>
/	/	kIx~
<g/>
vláknitá	vláknitý	k2eAgFnSc1d1
vrstva	vrstva	k1gFnSc1
<g/>
)	)	kIx)
zabírá	zabírat	k5eAaImIp3nS
75	#num#	k4
<g/>
–	–	k?
<g/>
80	#num#	k4
%	%	kIx~
vlastní	vlastní	k2eAgFnSc2d1
vlasové	vlasový	k2eAgFnSc2d1
hmoty	hmota	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skládá	skládat	k5eAaImIp3nS
se	se	k3xPyFc4
z	z	k7c2
protáhlých	protáhlý	k2eAgFnPc2d1
neživých	živý	k2eNgFnPc2d1
buněk	buňka	k1gFnPc2
(	(	kIx(
<g/>
vláken	vlákna	k1gFnPc2
keratinu	keratin	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednotlivá	jednotlivý	k2eAgFnSc1d1
vlákna	vlákna	k1gFnSc1
tvoří	tvořit	k5eAaImIp3nS
nejnižší	nízký	k2eAgFnSc4d3
stavební	stavební	k2eAgFnSc4d1
jednotku	jednotka	k1gFnSc4
mikrofibrilu	mikrofibril	k1gInSc2
–	–	k?
ty	ten	k3xDgFnPc1
se	se	k3xPyFc4
kruhovitě	kruhovitě	k6eAd1
seskupují	seskupovat	k5eAaImIp3nP
<g/>
,	,	kIx,
vytvářejí	vytvářet	k5eAaImIp3nP
snopečky	snopeček	k1gInPc7
a	a	k8xC
tím	ten	k3xDgNnSc7
tvoří	tvořit	k5eAaImIp3nS
další	další	k2eAgFnSc4d1
stavební	stavební	k2eAgFnSc4d1
jednotku	jednotka	k1gFnSc4
makrofibrilu	makrofibril	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
vybudovaná	vybudovaný	k2eAgFnSc1d1
část	část	k1gFnSc1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
ortokortex	ortokortex	k1gInSc1
<g/>
,	,	kIx,
jde	jít	k5eAaImIp3nS
o	o	k7c4
pravidelné	pravidelný	k2eAgNnSc4d1
uspořádání	uspořádání	k1gNnSc4
vlasového	vlasový	k2eAgInSc2d1
kortexu	kortex	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
nepravidelném	pravidelný	k2eNgNnSc6d1
uspořádání	uspořádání	k1gNnSc6
kortexu	kortex	k1gInSc2
(	(	kIx(
<g/>
především	především	k9
vlasy	vlas	k1gInPc4
upravované	upravovaný	k2eAgInPc4d1
preparací	preparace	k1gFnSc7
<g/>
,	,	kIx,
barvením	barvení	k1gNnSc7
a	a	k8xC
odbarvováním	odbarvování	k1gNnSc7
<g/>
)	)	kIx)
vzniká	vznikat	k5eAaImIp3nS
parakortex	parakortex	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
obě	dva	k4xCgFnPc1
části	část	k1gFnPc1
se	se	k3xPyFc4
od	od	k7c2
sebe	se	k3xPyFc2
liší	lišit	k5eAaImIp3nP
nejen	nejen	k6eAd1
fyzikálním	fyzikální	k2eAgInSc6d1
a	a	k8xC
chemicky	chemicky	k6eAd1
rozdílným	rozdílný	k2eAgNnSc7d1
složením	složení	k1gNnSc7
aminokyselin	aminokyselina	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
různou	různý	k2eAgFnSc7d1
chemickou	chemický	k2eAgFnSc7d1
reaktivitou	reaktivita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednotlivé	jednotlivý	k2eAgFnPc1d1
vláknité	vláknitý	k2eAgFnPc1d1
makrofibrily	makrofibrila	k1gFnPc1
jsou	být	k5eAaImIp3nP
mezi	mezi	k7c7
sebou	se	k3xPyFc7
spojeny	spojen	k2eAgInPc1d1
buněčnými	buněčný	k2eAgFnPc7d1
membránami	membrána	k1gFnPc7
a	a	k8xC
proteinovým	proteinový	k2eAgInSc7d1
tmelem	tmel	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
kortexu	kortex	k1gInSc6
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
shluky	shluk	k1gInPc1
pigmentových	pigmentový	k2eAgNnPc2d1
zrn	zrno	k1gNnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
jeví	jevit	k5eAaImIp3nP
jako	jako	k9
tmavé	tmavý	k2eAgFnPc1d1
skvrny	skvrna	k1gFnPc1
(	(	kIx(
<g/>
ostrůvky	ostrůvek	k1gInPc1
<g/>
)	)	kIx)
mezi	mezi	k7c7
fibrilami	fibrila	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Medulla	Medulla	k1gFnSc1
(	(	kIx(
<g/>
dřeň	dřeň	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
centrální	centrální	k2eAgFnSc4d1
část	část	k1gFnSc4
vlasu	vlas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ne	ne	k9
u	u	k7c2
všech	všecek	k3xTgInPc2
vlasů	vlas	k1gInPc2
je	být	k5eAaImIp3nS
uprostřed	uprostřed	k6eAd1
<g/>
,	,	kIx,
u	u	k7c2
dlouhých	dlouhý	k2eAgInPc2d1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
zpravidla	zpravidla	k6eAd1
jen	jen	k9
u	u	k7c2
kořínků	kořínek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
dobře	dobře	k6eAd1
vyvinuta	vyvinout	k5eAaPmNgFnS
u	u	k7c2
vousů	vous	k1gInPc2
–	–	k?
má	mít	k5eAaImIp3nS
nepravidelný	pravidelný	k2eNgInSc1d1
hvězdicovitý	hvězdicovitý	k2eAgInSc1d1
tvar	tvar	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Keratin	keratin	k1gInSc1
meduly	medula	k1gFnSc2
má	mít	k5eAaImIp3nS
houbovitou	houbovitý	k2eAgFnSc4d1
strukturu	struktura	k1gFnSc4
s	s	k7c7
mnoha	mnoho	k4c7
dutinkami	dutinka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
asi	asi	k9
3,5	3,5	k4
%	%	kIx~
lipidů	lipid	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Keratin	keratin	k1gInSc1
meduly	medula	k1gFnSc2
má	mít	k5eAaImIp3nS
jiné	jiný	k2eAgNnSc1d1
chemické	chemický	k2eAgNnSc1d1
složení	složení	k1gNnSc1
než	než	k8xS
kortex	kortex	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
zvětšený	zvětšený	k2eAgInSc1d1
vlas	vlas	k1gInSc1
</s>
<s>
Z	z	k7c2
chemického	chemický	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
vlasy	vlas	k1gInPc1
obsahují	obsahovat	k5eAaImIp3nP
uhlík	uhlík	k1gInSc4
(	(	kIx(
<g/>
49	#num#	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vodík	vodík	k1gInSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kyslík	kyslík	k1gInSc1
(	(	kIx(
<g/>
23	#num#	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dusík	dusík	k1gInSc1
(	(	kIx(
<g/>
17	#num#	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
síra	síra	k1gFnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
železo	železo	k1gNnSc1
<g/>
,	,	kIx,
měď	měď	k1gFnSc1
<g/>
,	,	kIx,
zinek	zinek	k1gInSc1
<g/>
,	,	kIx,
jód	jód	k1gInSc1
<g/>
,	,	kIx,
20	#num#	k4
různých	různý	k2eAgInPc2d1
druhů	druh	k1gInPc2
aminokyselin	aminokyselina	k1gFnPc2
<g/>
,	,	kIx,
proteiny	protein	k1gInPc1
<g/>
,	,	kIx,
lipidy	lipid	k1gInPc1
(	(	kIx(
<g/>
např.	např.	kA
cholesterin	cholesterin	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
vodu	voda	k1gFnSc4
(	(	kIx(
<g/>
asi	asi	k9
12	#num#	k4
%	%	kIx~
hmotnosti	hmotnost	k1gFnSc2
vlasu	vlas	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlas	vlas	k1gInSc1
po	po	k7c6
chemické	chemický	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
<g/>
:	:	kIx,
Původně	původně	k6eAd1
měkká	měkký	k2eAgFnSc1d1
buničitá	buničitý	k2eAgFnSc1d1
bílkovinná	bílkovinný	k2eAgFnSc1d1
hmota	hmota	k1gFnSc1
prekeratin	prekeratina	k1gFnPc2
se	se	k3xPyFc4
keratinizací	keratinizace	k1gFnSc7
neboli	neboli	k8xC
rohovatěním	rohovatění	k1gNnSc7
mění	měnit	k5eAaImIp3nS
v	v	k7c4
pevnou	pevný	k2eAgFnSc4d1
hmotu	hmota	k1gFnSc4
keratin	keratin	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomocí	pomocí	k7c2
proteinového	proteinový	k2eAgNnSc2d1
složení	složení	k1gNnSc2
jediného	jediný	k2eAgInSc2d1
vlasu	vlas	k1gInSc2
lze	lze	k6eAd1
identifikovat	identifikovat	k5eAaBmF
člověka	člověk	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vlas	vlas	k1gInSc1
má	mít	k5eAaImIp3nS
podobnou	podobný	k2eAgFnSc4d1
pevnost	pevnost	k1gFnSc4
jako	jako	k8xS,k8xC
kevlar	kevlar	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Jediné	jediný	k2eAgNnSc1d1
vlasové	vlasový	k2eAgNnSc1d1
vlákno	vlákno	k1gNnSc1
dokáže	dokázat	k5eAaPmIp3nS
udržet	udržet	k5eAaPmF
100	#num#	k4
gramové	gramový	k2eAgFnPc4d1
závaží	závaží	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměr	průměr	k1gInSc1
vlasu	vlas	k1gInSc2
je	být	k5eAaImIp3nS
zhruba	zhruba	k6eAd1
18	#num#	k4
µ	µ	k1gInSc4
<g/>
–	–	k?
<g/>
180	#num#	k4
µ	µ	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
fyzikální	fyzikální	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
vlasu	vlas	k1gInSc2
patří	patřit	k5eAaImIp3nS
pevnost	pevnost	k1gFnSc1
<g/>
,	,	kIx,
pružnost	pružnost	k1gFnSc1
<g/>
,	,	kIx,
tažnost	tažnost	k1gFnSc1
<g/>
,	,	kIx,
nasákavost	nasákavost	k1gFnSc1
<g/>
,	,	kIx,
odolnost	odolnost	k1gFnSc1
proti	proti	k7c3
vnějším	vnější	k2eAgInPc3d1
tlakům	tlak	k1gInPc3
<g/>
,	,	kIx,
kapilární	kapilární	k2eAgFnSc1d1
schopnost	schopnost	k1gFnSc1
<g/>
,	,	kIx,
elektrický	elektrický	k2eAgInSc4d1
náboj	náboj	k1gInSc4
a	a	k8xC
citlivost	citlivost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Nasákavost	nasákavost	k1gFnSc1
je	být	k5eAaImIp3nS
určena	určit	k5eAaPmNgFnS
mírou	míra	k1gFnSc7
poškození	poškození	k1gNnSc2
vlasu	vlas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čím	co	k3yQnSc7,k3yRnSc7,k3yInSc7
více	hodně	k6eAd2
je	být	k5eAaImIp3nS
vlas	vlas	k1gInSc4
poškozený	poškozený	k2eAgInSc4d1
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
více	hodně	k6eAd2
vody	voda	k1gFnPc4
nasává	nasávat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Keratin	keratin	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
vlasu	vlas	k1gInSc6
obsažený	obsažený	k2eAgInSc4d1
<g/>
,	,	kIx,
při	při	k7c6
kontaktu	kontakt	k1gInSc6
s	s	k7c7
vodou	voda	k1gFnSc7
bobtná	bobtnat	k5eAaImIp3nS
a	a	k8xC
vlas	vlas	k1gInSc1
tak	tak	k6eAd1
zvětšuje	zvětšovat	k5eAaImIp3nS
svůj	svůj	k3xOyFgInSc4
objem	objem	k1gInSc4
až	až	k9
o	o	k7c4
10	#num#	k4
–	–	k?
15	#num#	k4
<g/>
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s>
Vlas	vlas	k1gInSc1
má	mít	k5eAaImIp3nS
na	na	k7c6
povrchu	povrch	k1gInSc6
záporný	záporný	k2eAgInSc4d1
elektrický	elektrický	k2eAgInSc4d1
náboj	náboj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čím	co	k3yInSc7,k3yRnSc7,k3yQnSc7
je	být	k5eAaImIp3nS
vlas	vlas	k1gInSc1
poškozenější	poškozený	k2eAgInSc1d2
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
je	být	k5eAaImIp3nS
záporný	záporný	k2eAgInSc1d1
náboj	náboj	k1gInSc1
větší	veliký	k2eAgInSc1d2
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
česání	česání	k1gNnSc6
<g/>
,	,	kIx,
kartáčování	kartáčování	k1gNnSc6
nebo	nebo	k8xC
například	například	k6eAd1
tření	třený	k2eAgMnPc1d1
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
silně	silně	k6eAd1
záporně	záporně	k6eAd1
nabít	nabít	k5eAaPmF,k5eAaBmF
všechny	všechen	k3xTgInPc4
vlasy	vlas	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
se	se	k3xPyFc4
pak	pak	k6eAd1
vzájemně	vzájemně	k6eAd1
odpuzují	odpuzovat	k5eAaImIp3nP
-	-	kIx~
elektrizují	elektrizovat	k5eAaBmIp3nP
<g/>
.	.	kIx.
</s>
<s>
Počet	počet	k1gInSc1
</s>
<s>
V	v	k7c6
průměru	průměr	k1gInSc6
mají	mít	k5eAaImIp3nP
na	na	k7c6
hlavě	hlava	k1gFnSc6
nejvíce	hodně	k6eAd3,k6eAd1
vlasů	vlas	k1gInPc2
světlovlasí	světlovlasý	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
(	(	kIx(
<g/>
okolo	okolo	k7c2
140	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nejméně	málo	k6eAd3
zrzaví	zrzavět	k5eAaImIp3nS
(	(	kIx(
<g/>
asi	asi	k9
80	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgInSc1
vlasový	vlasový	k2eAgInSc1d1
kořínek	kořínek	k1gInSc1
má	mít	k5eAaImIp3nS
přitom	přitom	k6eAd1
v	v	k7c6
průběhu	průběh	k1gInSc6
existence	existence	k1gFnSc2
(	(	kIx(
<g/>
postupně	postupně	k6eAd1
<g/>
)	)	kIx)
až	až	k9
12	#num#	k4
vlasů	vlas	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrná	průměrný	k2eAgFnSc1d1
tloušťka	tloušťka	k1gFnSc1
vlasu	vlas	k1gInSc2
je	být	k5eAaImIp3nS
42	#num#	k4
<g/>
–	–	k?
<g/>
95	#num#	k4
mikrometrů	mikrometr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muži	muž	k1gMnPc1
mají	mít	k5eAaImIp3nP
vlasy	vlas	k1gInPc4
nejsilnější	silný	k2eAgInPc4d3
v	v	k7c6
týlu	týl	k1gInSc6
<g/>
,	,	kIx,
avšak	avšak	k8xC
ženské	ženský	k2eAgInPc4d1
vlasy	vlas	k1gInPc4
jsou	být	k5eAaImIp3nP
silnější	silný	k2eAgMnSc1d2
než	než	k8xS
mužské	mužský	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Člověk	člověk	k1gMnSc1
bez	bez	k7c2
rozdílu	rozdíl	k1gInSc2
pohlaví	pohlaví	k1gNnSc2
má	mít	k5eAaImIp3nS
pod	pod	k7c7
pokožkou	pokožka	k1gFnSc7
2	#num#	k4
až	až	k9
5	#num#	k4
milionů	milion	k4xCgInPc2
vlasových	vlasový	k2eAgInPc2d1
váčků	váček	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc4
a	a	k8xC
druh	druh	k1gInSc4
váčků	váček	k1gInPc2
je	být	k5eAaImIp3nS
dán	dát	k5eAaPmNgInS
už	už	k6eAd1
při	při	k7c6
narození	narození	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlas	vlas	k1gInSc1
roste	růst	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
2	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
potom	potom	k6eAd1
vypadne	vypadnout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Denně	denně	k6eAd1
vypadne	vypadnout	k5eAaPmIp3nS
člověku	člověk	k1gMnSc3
asi	asi	k9
70	#num#	k4
<g/>
–	–	k?
<g/>
100	#num#	k4
vlasů	vlas	k1gInPc2
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
nemá	mít	k5eNaImIp3nS
s	s	k7c7
vlasy	vlas	k1gInPc7
žádné	žádný	k3yNgInPc4
problémy	problém	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgMnSc1
den	den	k1gInSc4
vlasy	vlas	k1gInPc1
povyrostou	povyrůst	k5eAaPmIp3nP
o	o	k7c4
0,2	0,2	k4
<g/>
–	–	k?
<g/>
0,45	0,45	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s>
Padání	padání	k1gNnSc1
vlasů	vlas	k1gInPc2
</s>
<s>
Padání	padání	k1gNnSc1
vlasů	vlas	k1gInPc2
se	se	k3xPyFc4
rozděluje	rozdělovat	k5eAaImIp3nS
na	na	k7c4
vratné	vratný	k2eAgNnSc4d1
(	(	kIx(
<g/>
dočasné	dočasný	k2eAgNnSc4d1
<g/>
)	)	kIx)
a	a	k8xC
nevratné	vratný	k2eNgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příčinami	příčina	k1gFnPc7
vratného	vratný	k2eAgNnSc2d1
vypadávání	vypadávání	k1gNnSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
stres	stres	k1gInSc1
<g/>
,	,	kIx,
těhotenství	těhotenství	k1gNnSc1
nebo	nebo	k8xC
například	například	k6eAd1
nedostatek	nedostatek	k1gInSc1
vitamínů	vitamín	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Častou	častý	k2eAgFnSc7d1
příčinou	příčina	k1gFnSc7
padání	padání	k1gNnSc2
vlasů	vlas	k1gInPc2
je	být	k5eAaImIp3nS
užívání	užívání	k1gNnSc1
některých	některý	k3yIgInPc2
léků	lék	k1gInPc2
(	(	kIx(
<g/>
například	například	k6eAd1
léky	lék	k1gInPc4
na	na	k7c6
akné	akné	k1gFnSc6
<g/>
,	,	kIx,
hormonální	hormonální	k2eAgFnSc1d1
terapie	terapie	k1gFnSc1
<g/>
,	,	kIx,
epileptika	epileptik	k1gMnSc2
<g/>
,	,	kIx,
léky	lék	k1gInPc1
potlačující	potlačující	k2eAgInSc1d1
imunitní	imunitní	k2eAgInSc4d1
systém	systém	k1gInSc4
<g/>
,	,	kIx,
léky	lék	k1gInPc4
na	na	k7c4
snížení	snížení	k1gNnSc4
cholesterolu	cholesterol	k1gInSc2
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevratné	vratný	k2eNgNnSc1d1
vypadávání	vypadávání	k1gNnSc4
vlasů	vlas	k1gInPc2
je	být	k5eAaImIp3nS
důsledkem	důsledek	k1gInSc7
činnosti	činnost	k1gFnSc2
mužských	mužský	k2eAgInPc2d1
hormonů	hormon	k1gInPc2
testosteronu	testosteron	k1gInSc2
a	a	k8xC
dihydrotestosteronu	dihydrotestosteron	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
i	i	k9
důsledkem	důsledek	k1gInSc7
léčby	léčba	k1gFnSc2
nádorového	nádorový	k2eAgNnSc2d1
onemocnění	onemocnění	k1gNnSc2
(	(	kIx(
<g/>
radioterapie	radioterapie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotných	samotný	k2eAgFnPc2d1
příčin	příčina	k1gFnPc2
padání	padání	k1gNnSc2
vlasů	vlas	k1gInPc2
však	však	k9
existuje	existovat	k5eAaImIp3nS
mnohem	mnohem	k6eAd1
více	hodně	k6eAd2
a	a	k8xC
při	při	k7c6
diagnostice	diagnostika	k1gFnSc6
je	být	k5eAaImIp3nS
potřeba	potřeba	k6eAd1
brát	brát	k5eAaImF
v	v	k7c4
úvahu	úvaha	k1gFnSc4
rodinnou	rodinný	k2eAgFnSc4d1
anamnézu	anamnéza	k1gFnSc4
<g/>
,	,	kIx,
aktuální	aktuální	k2eAgInSc4d1
zdravotní	zdravotní	k2eAgInSc4d1
stav	stav	k1gInSc4
<g/>
,	,	kIx,
překonané	překonaný	k2eAgNnSc4d1
onemocnění	onemocnění	k1gNnSc4
<g/>
,	,	kIx,
stresovou	stresový	k2eAgFnSc4d1
zátěž	zátěž	k1gFnSc4
<g/>
,	,	kIx,
prostředí	prostředí	k1gNnSc4
<g/>
,	,	kIx,
výživu	výživa	k1gFnSc4
a	a	k8xC
životní	životní	k2eAgInSc4d1
styl	styl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
omezení	omezení	k1gNnSc4
vypadávání	vypadávání	k1gNnSc2
vlasů	vlas	k1gInPc2
je	být	k5eAaImIp3nS
důležitá	důležitý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
faktorů	faktor	k1gInPc2
–	–	k?
zejména	zejména	k9
dostatečné	dostatečný	k2eAgNnSc4d1
prokrvení	prokrvení	k1gNnSc4
pokožky	pokožka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
vypadávání	vypadávání	k1gNnSc4
vlasů	vlas	k1gInPc2
zmírnit	zmírnit	k5eAaPmF
masáží	masáž	k1gFnSc7
hlavy	hlava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
však	však	k9
jen	jen	k9
malá	malý	k2eAgFnSc1d1
část	část	k1gFnSc1
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
potřeba	potřeba	k6eAd1
udělat	udělat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Léčba	léčba	k1gFnSc1
padání	padání	k1gNnSc2
vlasů	vlas	k1gInPc2
je	být	k5eAaImIp3nS
komplikovaná	komplikovaný	k2eAgFnSc1d1
a	a	k8xC
často	často	k6eAd1
s	s	k7c7
nejistými	jistý	k2eNgInPc7d1
výsledky	výsledek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
několik	několik	k4yIc4
látek	látka	k1gFnPc2
testovaných	testovaný	k2eAgFnPc2d1
na	na	k7c6
úrovni	úroveň	k1gFnSc6
klinických	klinický	k2eAgFnPc2d1
studií	studie	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
vykazují	vykazovat	k5eAaImIp3nP
příznivý	příznivý	k2eAgInSc4d1
vliv	vliv	k1gInSc4
na	na	k7c4
hustotu	hustota	k1gFnSc4
a	a	k8xC
růst	růst	k1gInSc4
vlasů	vlas	k1gInPc2
<g/>
:	:	kIx,
minoxidil	minoxidit	k5eAaImAgInS,k5eAaPmAgInS
5	#num#	k4
<g/>
%	%	kIx~
<g/>
,	,	kIx,
fluridil	fluridit	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
,	,	kIx,
aminexil	aminexil	k1gInSc1
<g/>
,	,	kIx,
melatonin	melatonin	k1gInSc1
(	(	kIx(
<g/>
spánkový	spánkový	k2eAgInSc1d1
hormon	hormon	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
i	i	k9
kyselina	kyselina	k1gFnSc1
hyaluronová	hyaluronová	k1gFnSc1
(	(	kIx(
<g/>
klinické	klinický	k2eAgFnPc1d1
studie	studie	k1gFnPc1
jsou	být	k5eAaImIp3nP
ke	k	k7c3
stažení	stažení	k1gNnSc3
v	v	k7c6
odkazu	odkaz	k1gInSc6
níže	nízce	k6eAd2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
vnitřní	vnitřní	k2eAgNnSc4d1
užití	užití	k1gNnSc4
je	být	k5eAaImIp3nS
nutná	nutný	k2eAgFnSc1d1
zvláštní	zvláštní	k2eAgFnSc1d1
kombinace	kombinace	k1gFnSc1
látek	látka	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
respektuje	respektovat	k5eAaImIp3nS
potřeby	potřeba	k1gFnPc4
každého	každý	k3xTgMnSc2
jedince	jedinec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Čím	co	k3yQnSc7,k3yRnSc7,k3yInSc7
dál	daleko	k6eAd2
častější	častý	k2eAgInSc1d2
je	být	k5eAaImIp3nS
také	také	k9
ložiskové	ložiskový	k2eAgNnSc1d1
vypadávání	vypadávání	k1gNnSc1
vlasů	vlas	k1gInPc2
<g/>
,	,	kIx,
zvané	zvaný	k2eAgFnPc1d1
Alopecia	Alopecia	k1gFnSc1
Areata	Areata	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
autoimunitní	autoimunitní	k2eAgNnSc4d1
onemocnění	onemocnění	k1gNnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
imunitní	imunitní	k2eAgInSc4d1
systém	systém	k1gInSc4
začne	začít	k5eAaPmIp3nS
napadat	napadat	k5eAaBmF,k5eAaPmF,k5eAaImF
vlasové	vlasový	k2eAgInPc4d1
folikuly	folikul	k1gInPc4
a	a	k8xC
vytvářet	vytvářet	k5eAaImF
malé	malý	k2eAgFnPc4d1
lysinky	lysinka	k1gFnPc4
velikosti	velikost	k1gFnSc2
mince	mince	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Spouštěčem	spouštěč	k1gInSc7
bývá	bývat	k5eAaImIp3nS
velmi	velmi	k6eAd1
silný	silný	k2eAgInSc1d1
stres	stres	k1gInSc1
nebo	nebo	k8xC
traumatický	traumatický	k2eAgInSc1d1
zážitek	zážitek	k1gInSc1
(	(	kIx(
<g/>
postihuje	postihovat	k5eAaImIp3nS
i	i	k9
malé	malý	k2eAgFnPc4d1
děti	dítě	k1gFnPc4
například	například	k6eAd1
při	při	k7c6
šikaně	šikana	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doporučuje	doporučovat	k5eAaImIp3nS
se	se	k3xPyFc4
používat	používat	k5eAaImF
šetrné	šetrný	k2eAgInPc4d1
kosmetické	kosmetický	k2eAgInPc4d1
přípravky	přípravek	k1gInPc4
<g/>
,	,	kIx,
hydratovat	hydratovat	k5eAaBmF
pokožku	pokožka	k1gFnSc4
a	a	k8xC
hlavně	hlavně	k6eAd1
dodržovat	dodržovat	k5eAaImF
duševní	duševní	k2eAgFnSc4d1
hygienu	hygiena	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
klíčem	klíč	k1gInSc7
k	k	k7c3
návratu	návrat	k1gInSc3
vlasů	vlas	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Zde	zde	k6eAd1
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
poznamenat	poznamenat	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
stav	stav	k1gInSc1
se	se	k3xPyFc4
často	často	k6eAd1
mění	měnit	k5eAaImIp3nS
a	a	k8xC
recidiva	recidiva	k1gFnSc1
<g/>
,	,	kIx,
tedy	tedy	k8xC
návrat	návrat	k1gInSc1
onemocnění	onemocnění	k1gNnSc2
po	po	k7c4
"	"	kIx"
<g/>
vyléčení	vyléčení	k1gNnSc4
<g/>
"	"	kIx"
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
zcela	zcela	k6eAd1
individuální	individuální	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jestliže	jestliže	k8xS
je	být	k5eAaImIp3nS
příčinou	příčina	k1gFnSc7
například	například	k6eAd1
stres	stres	k1gInSc1
a	a	k8xC
organizmus	organizmus	k1gInSc1
se	se	k3xPyFc4
zaměří	zaměřit	k5eAaPmIp3nS
na	na	k7c4
vlastní	vlastní	k2eAgFnPc4d1
buňky	buňka	k1gFnPc4
<g/>
,	,	kIx,
je	on	k3xPp3gFnPc4
třeba	třeba	k6eAd1
se	se	k3xPyFc4
podívat	podívat	k5eAaPmF,k5eAaImF
přímo	přímo	k6eAd1
na	na	k7c4
jádro	jádro	k1gNnSc4
problému	problém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kosmetické	kosmetický	k2eAgInPc1d1
prostředky	prostředek	k1gInPc1
jsou	být	k5eAaImIp3nP
sice	sice	k8xC
v	v	k7c6
pořádku	pořádek	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
problém	problém	k1gInSc1
neřeší	řešit	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
porucha	porucha	k1gFnSc1
imunitního	imunitní	k2eAgInSc2d1
systému	systém	k1gInSc2
má	mít	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
příčinu	příčina	k1gFnSc4
a	a	k8xC
tu	tu	k6eAd1
je	být	k5eAaImIp3nS
potřeba	potřeba	k6eAd1
najít	najít	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
zkušeností	zkušenost	k1gFnPc2
lze	lze	k6eAd1
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
Alopecia	Alopecia	k1gFnSc1
areata	areata	k1gFnSc1
je	být	k5eAaImIp3nS
často	často	k6eAd1
doprovázena	doprovázet	k5eAaImNgFnS
jiným	jiný	k2eAgNnSc7d1
onemocněním	onemocnění	k1gNnSc7
<g/>
,	,	kIx,
například	například	k6eAd1
silnou	silný	k2eAgFnSc7d1
alergií	alergie	k1gFnSc7
<g/>
,	,	kIx,
psychickým	psychický	k2eAgNnSc7d1
onemocněním	onemocnění	k1gNnSc7
(	(	kIx(
<g/>
většinou	většinou	k6eAd1
deprese	deprese	k1gFnPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
onemocněním	onemocnění	k1gNnSc7
zažívacího	zažívací	k2eAgInSc2d1
traktu	trakt	k1gInSc2
atd.	atd.	kA
</s>
<s>
Zbarvení	zbarvení	k1gNnSc1
</s>
<s>
Různé	různý	k2eAgNnSc1d1
zbarvení	zbarvení	k1gNnSc1
vlasů	vlas	k1gInPc2
je	být	k5eAaImIp3nS
dáno	dát	k5eAaPmNgNnS
hlavně	hlavně	k6eAd1
geneticky	geneticky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedině	jedině	k6eAd1
tvar	tvar	k1gInSc1
a	a	k8xC
vnitřní	vnitřní	k2eAgFnSc1d1
skladba	skladba	k1gFnSc1
melaninových	melaninový	k2eAgNnPc2d1
zrn	zrno	k1gNnPc2
rozhodují	rozhodovat	k5eAaImIp3nP
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
zda	zda	k8xS
vlasy	vlas	k1gInPc1
mají	mít	k5eAaImIp3nP
barvu	barva	k1gFnSc4
tmavou	tmavý	k2eAgFnSc4d1
nebo	nebo	k8xC
světlou	světlý	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc1
pigmentových	pigmentový	k2eAgNnPc2d1
zrn	zrno	k1gNnPc2
ovlivňuje	ovlivňovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
intenzitu	intenzita	k1gFnSc4
zbarvení	zbarvení	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Člověk	člověk	k1gMnSc1
samotný	samotný	k2eAgMnSc1d1
většinou	většinou	k6eAd1
klade	klást	k5eAaImIp3nS
důraz	důraz	k1gInSc1
na	na	k7c4
společenskou	společenský	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
vlasů	vlas	k1gInPc2
–	–	k?
upravuje	upravovat	k5eAaImIp3nS
si	se	k3xPyFc3
účes	účes	k1gInSc4
<g/>
,	,	kIx,
barvu	barva	k1gFnSc4
a	a	k8xC
délku	délka	k1gFnSc4
vlasů	vlas	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
85	#num#	k4
<g/>
.	.	kIx.
roku	rok	k1gInSc2
života	život	k1gInSc2
jsou	být	k5eAaImIp3nP
vlasy	vlas	k1gInPc1
téměř	téměř	k6eAd1
u	u	k7c2
všech	všecek	k3xTgMnPc2
lidí	člověk	k1gMnPc2
naprosto	naprosto	k6eAd1
bezbarvé	bezbarvý	k2eAgFnSc2d1
–	–	k?
z	z	k7c2
důvodu	důvod	k1gInSc2
nedostatku	nedostatek	k1gInSc2
melaninu	melanin	k1gInSc2
–	–	k?
látky	látka	k1gFnSc2
tvořené	tvořený	k2eAgInPc1d1
pigmentovými	pigmentový	k2eAgFnPc7d1
buňkami	buňka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
tyto	tento	k3xDgFnPc4
buňky	buňka	k1gFnPc4
přestanou	přestat	k5eAaPmIp3nP
fungovat	fungovat	k5eAaImF
<g/>
,	,	kIx,
vlas	vlas	k1gInSc1
je	být	k5eAaImIp3nS
bílý	bílý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Kulturní	kulturní	k2eAgInSc1d1
a	a	k8xC
historický	historický	k2eAgInSc1d1
postoj	postoj	k1gInSc1
</s>
<s>
Účes	účes	k1gInSc1
vždy	vždy	k6eAd1
hrál	hrát	k5eAaImAgInS
vyznanou	vyznaný	k2eAgFnSc4d1
kulturní	kulturní	k2eAgFnSc4d1
a	a	k8xC
společenskou	společenský	k2eAgFnSc4d1
úlohu	úloha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dobách	doba	k1gFnPc6
středověku	středověk	k1gInSc2
a	a	k8xC
raného	raný	k2eAgInSc2d1
novověku	novověk	k1gInSc2
(	(	kIx(
<g/>
hlavně	hlavně	k9
v	v	k7c6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
běžné	běžný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
muži	muž	k1gMnPc1
i	i	k8xC
ženy	žena	k1gFnPc1
nosili	nosit	k5eAaImAgMnP
dlouhé	dlouhý	k2eAgInPc4d1
vlasy	vlas	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Delší	dlouhý	k2eAgInPc1d2
vlasy	vlas	k1gInPc1
byly	být	k5eAaImAgInP
zejména	zejména	k9
ve	v	k7c6
středověku	středověk	k1gInSc2
jasným	jasný	k2eAgInSc7d1
symbolem	symbol	k1gInSc7
vyššího	vysoký	k2eAgNnSc2d2
společenského	společenský	k2eAgNnSc2d1
postavení	postavení	k1gNnSc2
<g/>
,	,	kIx,
bohatství	bohatství	k1gNnSc2
a	a	k8xC
svobody	svoboda	k1gFnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
kratší	krátký	k2eAgInPc1d2
vlasy	vlas	k1gInPc1
byly	být	k5eAaImAgFnP
především	především	k6eAd1
symbolem	symbol	k1gInSc7
podřízenosti	podřízenost	k1gFnSc2
a	a	k8xC
zejména	zejména	k9
poddanství	poddanství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
počátku	počátek	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
a	a	k8xC
po	po	k7c6
první	první	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
začínají	začínat	k5eAaImIp3nP
muži	muž	k1gMnPc1
v	v	k7c6
západních	západní	k2eAgFnPc6d1
kulturách	kultura	k1gFnPc6
nosit	nosit	k5eAaImF
vlasy	vlas	k1gInPc1
kratší	krátký	k2eAgInPc1d2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
ženy	žena	k1gFnPc1
je	on	k3xPp3gMnPc4
mají	mít	k5eAaImIp3nP
typicky	typicky	k6eAd1
delší	dlouhý	k2eAgFnSc4d2
(	(	kIx(
<g/>
byť	byť	k8xS
na	na	k7c6
začátku	začátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
začala	začít	k5eAaPmAgFnS
určitá	určitý	k2eAgFnSc1d1
část	část	k1gFnSc1
žen	žena	k1gFnPc2
nosit	nosit	k5eAaImF
též	též	k9
i	i	k9
vlasy	vlas	k1gInPc1
kratší	krátký	k2eAgInPc1d2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
to	ten	k3xDgNnSc4
přímý	přímý	k2eAgInSc4d1
důsledek	důsledek	k1gInSc4
válek	válka	k1gFnPc2
<g/>
,	,	kIx,
během	během	k7c2
které	který	k3yIgFnSc2,k3yRgFnSc2,k3yQgFnSc2
se	se	k3xPyFc4
museli	muset	k5eAaImAgMnP
muži	muž	k1gMnPc1
na	na	k7c6
frontě	fronta	k1gFnSc6
vypořádat	vypořádat	k5eAaPmF
s	s	k7c7
řadou	řada	k1gFnSc7
nemocí	nemoc	k1gFnPc2
a	a	k8xC
parazitů	parazit	k1gMnPc2
a	a	k8xC
krátký	krátký	k2eAgInSc4d1
sestřih	sestřih	k1gInSc4
byl	být	k5eAaImAgInS
výhodnější	výhodný	k2eAgInSc1d2
z	z	k7c2
hygienického	hygienický	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nošení	nošení	k1gNnSc1
dlouhých	dlouhý	k2eAgInPc2d1
vlasů	vlas	k1gInPc2
u	u	k7c2
mužů	muž	k1gMnPc2
se	se	k3xPyFc4
začalo	začít	k5eAaPmAgNnS
vracet	vracet	k5eAaImF
v	v	k7c6
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
symbolem	symbol	k1gInSc7
hippies	hippiesa	k1gFnPc2
a	a	k8xC
politického	politický	k2eAgInSc2d1
protestu	protest	k1gInSc2
proti	proti	k7c3
establishmentu	establishment	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
konci	konec	k1gInSc3
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
staly	stát	k5eAaPmAgInP
delší	dlouhý	k2eAgInPc1d2
vlasy	vlas	k1gInPc1
u	u	k7c2
mužů	muž	k1gMnPc2
symbolem	symbol	k1gInSc7
určité	určitý	k2eAgFnPc4d1
snahy	snaha	k1gFnPc4
o	o	k7c4
nonkonformitu	nonkonformita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
SHIMIZU	SHIMIZU	kA
<g/>
,	,	kIx,
Hiroshi	Hiroshi	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Shimizu	Shimiz	k1gInSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Textbook	Textbook	k1gInSc1
of	of	k?
Dermatology	dermatolog	k1gMnPc7
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Hokkaido	Hokkaida	k1gFnSc5
University	universita	k1gFnPc1
Press	Press	k1gInSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
JELÍNEK	Jelínek	k1gMnSc1
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Histologie	histologie	k1gFnSc1
embryologie	embryologie	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
lékařská	lékařský	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
UK	UK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
https://phys.org/news/2019-06-hair-human-body-identification.html	https://phys.org/news/2019-06-hair-human-body-identification.html	k1gInSc1
-	-	kIx~
Study	stud	k1gInPc1
finds	findsa	k1gFnPc2
any	any	k?
single	singl	k1gInSc5
hair	hair	k1gInSc1
from	from	k1gInSc1
the	the	k?
human	human	k1gInSc1
body	bod	k1gInPc1
can	can	k?
be	be	k?
used	usedo	k1gNnPc2
for	forum	k1gNnPc2
identification	identification	k1gInSc1
<g/>
↑	↑	k?
Alopecia	Alopecius	k1gMnSc2
Areata	Areat	k1gMnSc2
–	–	k?
vypadaná	vypadaný	k2eAgNnPc4d1
kolečka	kolečko	k1gNnPc4
vlasů	vlas	k1gInPc2
–	–	k?
příčiny	příčina	k1gFnPc4
a	a	k8xC
řešení	řešení	k1gNnSc4
<g/>
↑	↑	k?
Kapyderm	Kapyderm	k1gInSc1
ukázky	ukázka	k1gFnSc2
Alopecie	Alopecie	k1gFnSc2
Areata	Areat	k1gMnSc2
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Obočí	obočit	k5eAaPmIp3nS
</s>
<s>
Prodlužování	prodlužování	k1gNnSc1
vlasů	vlas	k1gInPc2
</s>
<s>
Řasa	řasa	k1gFnSc1
(	(	kIx(
<g/>
oko	oko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Účes	účes	k1gInSc1
</s>
<s>
Vlasová	vlasový	k2eAgFnSc1d1
kůra	kůra	k1gFnSc1
</s>
<s>
Vlasový	vlasový	k2eAgInSc1d1
vlhkoměr	vlhkoměr	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
vlas	vlas	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
vlas	vlas	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4359582-0	4359582-0	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
14147	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Biologie	biologie	k1gFnSc1
</s>
