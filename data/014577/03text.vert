<s>
Vysokorychlostní	vysokorychlostní	k2eAgInSc1d1
vlak	vlak	k1gInSc1
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
<g/>
:	:	kIx,
Šinkansen	Šinkansen	k1gInSc1
série	série	k1gFnSc2
E	E	kA
<g/>
5	#num#	k4
<g/>
Francie	Francie	k1gFnSc2
<g/>
:	:	kIx,
AGVICE-	AGVICE-	k1gMnSc1
<g/>
3	#num#	k4
<g/>
Španělsko	Španělsko	k1gNnSc1
<g/>
:	:	kIx,
AVE	ave	k1gNnSc1
S-	S-	k1gFnSc2
<g/>
102	#num#	k4
<g/>
/	/	kIx~
<g/>
Talgo	Talgo	k6eAd1
350	#num#	k4
„	„	k?
<g/>
Pato	pata	k1gFnSc5
<g/>
“	“	k?
<g/>
ETR	ETR	kA
500	#num#	k4
„	„	k?
<g/>
Frecciarossa	Frecciarossa	k1gFnSc1
<g/>
“	“	k?
Italských	italský	k2eAgFnPc2d1
drah	draha	k1gFnPc2
v	v	k7c6
MiláněJižní	MiláněJižní	k2eAgFnSc6d1
Korea	Korea	k1gFnSc1
<g/>
:	:	kIx,
KTX	KTX	kA
„	„	k?
<g/>
Sancheon	Sancheon	k1gInSc1
<g/>
“	“	k?
<g/>
Čína	Čína	k1gFnSc1
<g/>
:	:	kIx,
CRH3	CRH3	k1gMnSc1
odvozený	odvozený	k2eAgMnSc1d1
od	od	k7c2
Siemens	siemens	k1gInSc1
Velaro	Velara	k1gFnSc5
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1
vysokorychlostní	vysokorychlostní	k2eAgFnPc1d1
trati	trať	k1gFnPc1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
(	(	kIx(
<g/>
stav	stav	k1gInSc1
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vysokorychlostní	vysokorychlostní	k2eAgInSc1d1
vlak	vlak	k1gInSc1
je	být	k5eAaImIp3nS
vlak	vlak	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
konstrukce	konstrukce	k1gFnSc1
mu	on	k3xPp3gMnSc3
umožňuje	umožňovat	k5eAaImIp3nS
dosahovat	dosahovat	k5eAaImF
rychlostí	rychlost	k1gFnSc7
nejméně	málo	k6eAd3
200	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
prioritně	prioritně	k6eAd1
určen	určit	k5eAaPmNgInS
pro	pro	k7c4
provoz	provoz	k1gInSc4
na	na	k7c6
vysokorychlostní	vysokorychlostní	k2eAgFnSc6d1
trati	trať	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpravidla	zpravidla	k6eAd1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
pohybovat	pohybovat	k5eAaImF
po	po	k7c6
konvenčních	konvenční	k2eAgFnPc6d1
tratích	trať	k1gFnPc6
<g/>
,	,	kIx,
výjimku	výjimka	k1gFnSc4
tvoří	tvořit	k5eAaImIp3nP
nekonvenční	konvenční	k2eNgInPc1d1
systémy	systém	k1gInPc1
(	(	kIx(
<g/>
Maglev	Maglev	k1gFnSc1
<g/>
,	,	kIx,
opuštěný	opuštěný	k2eAgInSc1d1
Aérotrain	Aérotrain	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
rozdílný	rozdílný	k2eAgInSc1d1
rozchod	rozchod	k1gInSc1
(	(	kIx(
<g/>
Japonsko	Japonsko	k1gNnSc1
<g/>
,	,	kIx,
částečně	částečně	k6eAd1
Španělsko	Španělsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1
</s>
<s>
Směrnice	směrnice	k1gFnSc1
evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
a	a	k8xC
rady	rada	k1gFnSc2
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
57	#num#	k4
<g/>
/	/	kIx~
<g/>
ES	ES	kA
o	o	k7c6
interoperabilitě	interoperabilita	k1gFnSc6
železničního	železniční	k2eAgInSc2d1
systému	systém	k1gInSc2
ve	v	k7c6
Společenství	společenství	k1gNnSc6
pracuje	pracovat	k5eAaImIp3nS
s	s	k7c7
definicí	definice	k1gFnSc7
<g/>
:	:	kIx,
</s>
<s>
Transevropský	transevropský	k2eAgInSc1d1
vysokorychlostní	vysokorychlostní	k2eAgInSc1d1
železniční	železniční	k2eAgInSc1d1
systém	systém	k1gInSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
vozidla	vozidlo	k1gNnPc1
určená	určený	k2eAgFnSc1d1
pro	pro	k7c4
provoz	provoz	k1gInSc4
<g/>
:	:	kIx,
</s>
<s>
buď	buď	k8xC
rychlostí	rychlost	k1gFnSc7
alespoň	alespoň	k9
250	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
na	na	k7c6
zvláště	zvláště	k6eAd1
vybudovaných	vybudovaný	k2eAgFnPc6d1
vysokorychlostních	vysokorychlostní	k2eAgFnPc6d1
tratích	trať	k1gFnPc6
a	a	k8xC
za	za	k7c2
vhodných	vhodný	k2eAgFnPc2d1
okolností	okolnost	k1gFnPc2
umožňující	umožňující	k2eAgInSc4d1
provoz	provoz	k1gInSc4
rychlostí	rychlost	k1gFnPc2
vyšší	vysoký	k2eAgFnSc2d2
než	než	k8xS
300	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
,	,	kIx,
</s>
<s>
nebo	nebo	k8xC
rychlostí	rychlost	k1gFnSc7
přibližně	přibližně	k6eAd1
200	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
na	na	k7c6
tratích	trať	k1gFnPc6
modernizovaných	modernizovaný	k2eAgFnPc6d1
<g/>
,	,	kIx,
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
slučitelné	slučitelný	k2eAgNnSc1d1
s	s	k7c7
úrovněmi	úroveň	k1gFnPc7
výkonnosti	výkonnost	k1gFnSc2
na	na	k7c6
těchto	tento	k3xDgFnPc6
tratích	trať	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
dále	daleko	k6eAd2
vozidla	vozidlo	k1gNnPc1
zkonstruovaná	zkonstruovaný	k2eAgNnPc1d1
pro	pro	k7c4
provoz	provoz	k1gInSc1
s	s	k7c7
maximální	maximální	k2eAgFnSc7d1
rychlostí	rychlost	k1gFnSc7
nižší	nízký	k2eAgFnSc7d2
než	než	k8xS
200	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
budou	být	k5eAaImBp3nP
pravděpodobně	pravděpodobně	k6eAd1
provozována	provozovat	k5eAaImNgNnP
na	na	k7c6
celé	celý	k2eAgFnSc6d1
transevropské	transevropský	k2eAgFnSc6d1
vysokorychlostní	vysokorychlostní	k2eAgFnSc6d1
síti	síť	k1gFnSc6
nebo	nebo	k8xC
její	její	k3xOp3gFnSc2
části	část	k1gFnSc2
tam	tam	k6eAd1
<g/>
,	,	kIx,
kde	kde	k6eAd1
jsou	být	k5eAaImIp3nP
slučitelné	slučitelný	k2eAgInPc1d1
s	s	k7c7
výkonnostními	výkonnostní	k2eAgFnPc7d1
úrovněmi	úroveň	k1gFnPc7
této	tento	k3xDgFnSc2
sítě	síť	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vysokorychlostní	vysokorychlostní	k2eAgInPc1d1
vlaky	vlak	k1gInPc1
(	(	kIx(
<g/>
systémy	systém	k1gInPc1
<g/>
)	)	kIx)
podle	podle	k7c2
světadílů	světadíl	k1gInPc2
</s>
<s>
Evropa	Evropa	k1gFnSc1
</s>
<s>
Francie	Francie	k1gFnSc1
–	–	k?
V	v	k7c6
sedmdesátých	sedmdesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
byl	být	k5eAaImAgInS
testován	testován	k2eAgInSc1d1
prototyp	prototyp	k1gInSc1
TGV	TGV	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
poháněný	poháněný	k2eAgInSc1d1
plynovou	plynový	k2eAgFnSc7d1
turbínou	turbína	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
něj	on	k3xPp3gMnSc4
přímo	přímo	k6eAd1
navázaly	navázat	k5eAaPmAgFnP
elektrické	elektrický	k2eAgFnPc1d1
soupravy	souprava	k1gFnPc1
TGV	TGV	kA
(	(	kIx(
<g/>
Train	Train	k1gMnSc1
a	a	k8xC
Grande	grand	k1gMnSc5
Vitesse	Vitess	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgInPc4
jsou	být	k5eAaImIp3nP
dnes	dnes	k6eAd1
stěžejní	stěžejní	k2eAgFnSc7d1
částí	část	k1gFnSc7
vozového	vozový	k2eAgInSc2d1
parku	park	k1gInSc2
SNCF	SNCF	kA
a	a	k8xC
držitelem	držitel	k1gMnSc7
světového	světový	k2eAgInSc2d1
rychlostního	rychlostní	k2eAgInSc2d1
rekordu	rekord	k1gInSc2
kolejového	kolejový	k2eAgNnSc2d1
adhezního	adhezní	k2eAgNnSc2d1
vozidla	vozidlo	k1gNnSc2
(	(	kIx(
<g/>
574,8	574,8	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnSc2
řady	řada	k1gFnSc2
jsou	být	k5eAaImIp3nP
vyráběny	vyrábět	k5eAaImNgInP
speciálně	speciálně	k6eAd1
pro	pro	k7c4
nadnárodní	nadnárodní	k2eAgFnPc4d1
společnosti	společnost	k1gFnPc4
(	(	kIx(
<g/>
Eurostar	Eurostar	k1gInSc1
<g/>
,	,	kIx,
Thalys	Thalysa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
TGV	TGV	kA
navázaly	navázat	k5eAaPmAgInP
vlaky	vlak	k1gInPc1
AGV	AGV	kA
<g/>
,	,	kIx,
TGV	TGV	kA
Duplex	duplex	k1gInSc1
a	a	k8xC
Euroduplex	Euroduplex	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jezdí	jezdit	k5eAaImIp3nS
sem	sem	k6eAd1
už	už	k6eAd1
i	i	k9
vlaky	vlak	k1gInPc1
ICE	ICE	kA
z	z	k7c2
Německa	Německo	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
nejbližších	blízký	k2eAgNnPc6d3
letech	léto	k1gNnPc6
také	také	k9
ETR	ETR	kA
500	#num#	k4
z	z	k7c2
Itálie	Itálie	k1gFnSc2
a	a	k8xC
AVE	ave	k1gNnSc2
ze	z	k7c2
Španělska	Španělsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1981-2003	1981-2003	k4
přepravily	přepravit	k5eAaPmAgInP
vlaky	vlak	k1gInPc1
TGV	TGV	kA
miliardu	miliarda	k4xCgFnSc4
cestujících	cestující	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Německo	Německo	k1gNnSc1
–	–	k?
Vysokorychlostní	vysokorychlostní	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
zajišťují	zajišťovat	k5eAaImIp3nP
jednotky	jednotka	k1gFnPc1
ICE	ICE	kA
(	(	kIx(
<g/>
Intercity-Express	Intercity-Express	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
2000	#num#	k4
ve	v	k7c6
verzi	verze	k1gFnSc6
ICE	ICE	kA
3	#num#	k4
firmy	firma	k1gFnSc2
Siemens	siemens	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Experimentovalo	experimentovat	k5eAaImAgNnS
se	se	k3xPyFc4
s	s	k7c7
naklápěcími	naklápěcí	k2eAgFnPc7d1
a	a	k8xC
dieselovými	dieselový	k2eAgFnPc7d1
jednotkami	jednotka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ty	k3xPp2nSc1
se	se	k3xPyFc4
však	však	k9
neosvědčily	osvědčit	k5eNaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
sousedních	sousední	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
mohou	moct	k5eAaImIp3nP
do	do	k7c2
Německa	Německo	k1gNnSc2
jezdit	jezdit	k5eAaImF
rovněž	rovněž	k9
vlaky	vlak	k1gInPc1
Cisalpino	Cisalpina	k1gFnSc5
a	a	k8xC
Thalys	Thalysa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jezdí	jezdit	k5eAaImIp3nS
sem	sem	k6eAd1
už	už	k6eAd1
i	i	k9
vlaky	vlak	k1gInPc1
TGV	TGV	kA
z	z	k7c2
Francie	Francie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Emslandu	Emsland	k1gInSc6
je	být	k5eAaImIp3nS
vyvíjen	vyvíjen	k2eAgInSc1d1
nekonvenční	konvenční	k2eNgInSc1d1
Transrapid	Transrapid	k1gInSc1
na	na	k7c6
magnetické	magnetický	k2eAgFnSc6d1
bázi	báze	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
Německu	Německo	k1gNnSc6
však	však	k9
ekonomicky	ekonomicky	k6eAd1
neuspěl	uspět	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Polsko	Polsko	k1gNnSc1
-	-	kIx~
Polská	polský	k2eAgFnSc1d1
státní	státní	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
PKP	PKP	kA
Intercity	Intercit	k1gInPc4
provozuje	provozovat	k5eAaImIp3nS
rychlovlaky	rychlovlak	k1gInPc4
ED	ED	kA
<g/>
250	#num#	k4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
v	v	k7c6
běžném	běžný	k2eAgInSc6d1
provozu	provoz	k1gInSc6
dosahují	dosahovat	k5eAaImIp3nP
rychlosti	rychlost	k1gFnSc2
200	#num#	k4
<g/>
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
Od	od	k7c2
roku	rok	k1gInSc2
2023	#num#	k4
je	být	k5eAaImIp3nS
na	na	k7c6
některých	některý	k3yIgFnPc6
tratích	trať	k1gFnPc6
plánováno	plánován	k2eAgNnSc1d1
zvýšení	zvýšení	k1gNnSc1
rychlosti	rychlost	k1gFnSc2
na	na	k7c4
250	#num#	k4
<g/>
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
V	v	k7c6
současnosti	současnost	k1gFnSc6
po	po	k7c6
celém	celý	k2eAgNnSc6d1
Polsku	Polsko	k1gNnSc6
probíhají	probíhat	k5eAaImIp3nP
modernizační	modernizační	k2eAgFnPc1d1
práce	práce	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
po	po	k7c6
dokončení	dokončení	k1gNnSc6
dovolí	dovolit	k5eAaPmIp3nS
provozovat	provozovat	k5eAaImF
rychlost	rychlost	k1gFnSc1
200	#num#	k4
<g/>
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
mezi	mezi	k7c7
téměř	téměř	k6eAd1
všemi	všecek	k3xTgNnPc7
městy	město	k1gNnPc7
nad	nad	k7c4
500	#num#	k4
<g/>
tis	tis	k1gInSc4
<g/>
.	.	kIx.
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Itálie	Itálie	k1gFnSc1
–	–	k?
Italská	italský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Trenitalia	Trenitalium	k1gNnSc2
provozuje	provozovat	k5eAaImIp3nS
převážně	převážně	k6eAd1
naklápěcí	naklápěcí	k2eAgFnPc4d1
soupravy	souprava	k1gFnPc4
s	s	k7c7
označením	označení	k1gNnSc7
Eurostar	Eurostara	k1gFnPc2
Italia	Italium	k1gNnSc2
(	(	kIx(
<g/>
na	na	k7c6
hlavních	hlavní	k2eAgFnPc6d1
tratích	trať	k1gFnPc6
ETR	ETR	kA
500	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
jsou	být	k5eAaImIp3nP
odsud	odsud	k6eAd1
vypravovány	vypravován	k2eAgInPc4d1
vlaky	vlak	k1gInPc4
Cisalpino	Cisalpin	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Milána	Milán	k1gInSc2
jezdí	jezdit	k5eAaImIp3nS
TGV	TGV	kA
z	z	k7c2
Francie	Francie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společnost	společnost	k1gFnSc1
Nuovo	Nuova	k1gFnSc5
Transporto	transporta	k1gFnSc5
Viaggatori	Viaggatori	k1gNnSc2
začala	začít	k5eAaPmAgNnP
v	v	k7c6
dubnu	duben	k1gInSc6
2012	#num#	k4
s	s	k7c7
provozem	provoz	k1gInSc7
25	#num#	k4
jednotek	jednotka	k1gFnPc2
AGV	AGV	kA
pod	pod	k7c7
názvem	název	k1gInSc7
Italo	Italo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
–	–	k?
Viz	vidět	k5eAaImRp2nS
AVE	ave	k1gNnSc1
<g/>
,	,	kIx,
Euromed	Euromed	k1gInSc1
<g/>
,	,	kIx,
Alaris	Alaris	k1gFnSc5
<g/>
,	,	kIx,
Talgo	Talga	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nově	nově	k6eAd1
však	však	k9
Španělé	Španěl	k1gMnPc1
nakoupili	nakoupit	k5eAaPmAgMnP
jednotky	jednotka	k1gFnPc4
ICE	ICE	kA
3	#num#	k4
<g/>
,	,	kIx,
označován	označovat	k5eAaImNgInS
jako	jako	k8xS,k8xC
Velaro	Velara	k1gFnSc5
E.	E.	kA
Překážku	překážka	k1gFnSc4
pro	pro	k7c4
jízdu	jízda	k1gFnSc4
většiny	většina	k1gFnSc2
vysokorychlostních	vysokorychlostní	k2eAgInPc2d1
vlaků	vlak	k1gInPc2
na	na	k7c6
běžných	běžný	k2eAgFnPc6d1
tratích	trať	k1gFnPc6
představuje	představovat	k5eAaImIp3nS
široký	široký	k2eAgInSc1d1
rozchod	rozchod	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
668	#num#	k4
mm	mm	kA
<g/>
)	)	kIx)
těchto	tento	k3xDgFnPc2
tratí	trať	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měnitelný	měnitelný	k2eAgInSc4d1
rozchod	rozchod	k1gInSc4
mají	mít	k5eAaImIp3nP
jenom	jenom	k9
(	(	kIx(
<g/>
téměř	téměř	k6eAd1
všechny	všechen	k3xTgFnPc4
<g/>
)	)	kIx)
soupravy	souprava	k1gFnPc1
Talgo	Talgo	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
–	–	k?
Vedle	vedle	k7c2
vlaků	vlak	k1gInPc2
Eurostar	Eurostara	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
svou	svůj	k3xOyFgFnSc4
jízdu	jízda	k1gFnSc4
z	z	k7c2
Francie	Francie	k1gFnSc2
a	a	k8xC
Belgie	Belgie	k1gFnSc1
končí	končit	k5eAaImIp3nS
v	v	k7c6
Londýně	Londýn	k1gInSc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
vidět	vidět	k5eAaImF
naklápěcí	naklápěcí	k2eAgFnSc2d1
soupravy	souprava	k1gFnSc2
společnosti	společnost	k1gFnSc2
Virgin	Virgin	k2eAgInSc4d1
Trains	Trains	k1gInSc4
Class	Class	k1gInSc1
390	#num#	k4
nebo	nebo	k8xC
InterCity	InterCit	k2eAgInPc1d1
225	#num#	k4
provozované	provozovaný	k2eAgFnSc2d1
společností	společnost	k1gFnSc7
GNER	GNER	kA
<g/>
.	.	kIx.
</s>
<s>
Rusko	Rusko	k1gNnSc1
–	–	k?
Na	na	k7c6
trati	trať	k1gFnSc6
Moskva	Moskva	k1gFnSc1
–	–	k?
Petrohrad	Petrohrad	k1gInSc1
jezdí	jezdit	k5eAaImIp3nP
vlaky	vlak	k1gInPc1
Sapsan	Sapsan	k1gInSc1
(	(	kIx(
<g/>
Siemens	siemens	k1gInSc1
<g/>
)	)	kIx)
rychlostí	rychlost	k1gFnSc7
až	až	k9
250	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
Na	na	k7c6
trati	trať	k1gFnSc6
Moskva	Moskva	k1gFnSc1
-	-	kIx~
Nižnij	Nižnij	k1gFnSc1
Novgorod	Novgorod	k1gInSc1
jezdí	jezdit	k5eAaImIp3nS
rychlovlaky	rychlovlak	k1gInPc4
Talgo	Talgo	k6eAd1
250	#num#	k4
-	-	kIx~
Striž	Striž	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Asie	Asie	k1gFnSc1
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
–	–	k?
Šinkansen	Šinkansna	k1gFnPc2
začal	začít	k5eAaPmAgInS
v	v	k7c6
šedesátých	šedesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
psát	psát	k5eAaImF
historii	historie	k1gFnSc4
vysokorychlostní	vysokorychlostní	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dodnes	dodnes	k6eAd1
je	být	k5eAaImIp3nS
japonský	japonský	k2eAgInSc1d1
železniční	železniční	k2eAgInSc1d1
systém	systém	k1gInSc1
velmi	velmi	k6eAd1
vysoce	vysoce	k6eAd1
ceněn	cenit	k5eAaImNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlaky	vlak	k1gInPc7
mají	mít	k5eAaImIp3nP
standardní	standardní	k2eAgInSc4d1
rozchod	rozchod	k1gInSc4
(	(	kIx(
<g/>
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
konvenčních	konvenční	k2eAgFnPc2d1
tratí	trať	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
mají	mít	k5eAaImIp3nP
rozchod	rozchod	k1gInSc4
1	#num#	k4
067	#num#	k4
mm	mm	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
pohybovat	pohybovat	k5eAaImF
pouze	pouze	k6eAd1
po	po	k7c6
vysokorychlostních	vysokorychlostní	k2eAgFnPc6d1
tratích	trať	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Čína	Čína	k1gFnSc1
–	–	k?
Zde	zde	k6eAd1
se	se	k3xPyFc4
vysokorychlostní	vysokorychlostní	k2eAgInPc1d1
vlaky	vlak	k1gInPc1
dlouho	dlouho	k6eAd1
nevyráběly	vyrábět	k5eNaImAgInP
<g/>
,	,	kIx,
přesto	přesto	k8xC
Čína	Čína	k1gFnSc1
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
průkopníky	průkopník	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
byla	být	k5eAaImAgFnS
v	v	k7c6
Šanghaji	Šanghaj	k1gFnSc6
poprvé	poprvé	k6eAd1
zahájena	zahájen	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
Maglevu	Magleva	k1gFnSc4
<g/>
,	,	kIx,
viz	vidět	k5eAaImRp2nS
Maglev	Maglev	k1gFnSc2
v	v	k7c6
Šanghaji	Šanghaj	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
byla	být	k5eAaImAgFnS
26	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2009	#num#	k4
v	v	k7c6
Číně	Čína	k1gFnSc6
otevřena	otevřen	k2eAgFnSc1d1
968	#num#	k4
km	km	kA
dlouhá	dlouhý	k2eAgFnSc1d1
vysokorychlostní	vysokorychlostní	k2eAgFnSc1d1
trať	trať	k1gFnSc1
Wu-chan	Wu-chan	k1gInSc1
–	–	k?
Kanton	Kanton	k1gInSc1
s	s	k7c7
maximální	maximální	k2eAgFnSc7d1
rychlostí	rychlost	k1gFnSc7
350	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
tak	tak	k9
o	o	k7c4
nejrychlejší	rychlý	k2eAgNnSc4d3
železniční	železniční	k2eAgNnSc4d1
spojení	spojení	k1gNnSc4
v	v	k7c6
komerčním	komerční	k2eAgInSc6d1
provozu	provoz	k1gInSc6
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
neboť	neboť	k8xC
cestovní	cestovní	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
vlaku	vlak	k1gInSc2
je	být	k5eAaImIp3nS
313	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
h	h	k?
(	(	kIx(
<g/>
nejbližším	blízký	k2eAgMnSc7d3
konkurentem	konkurent	k1gMnSc7
je	být	k5eAaImIp3nS
Francie	Francie	k1gFnSc1
s	s	k7c7
nejvyšší	vysoký	k2eAgFnSc7d3
cestovní	cestovní	k2eAgFnSc7d1
rychlostí	rychlost	k1gFnSc7
272	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
v	v	k7c6
úseku	úsek	k1gInSc6
Lorraine	Lorrain	k1gInSc5
–	–	k?
Champagne	Champagn	k1gInSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Jezdí	jezdit	k5eAaImIp3nS
zde	zde	k6eAd1
jednotky	jednotka	k1gFnPc4
CRH3	CRH3	k1gFnSc4
(	(	kIx(
<g/>
odvozené	odvozený	k2eAgInPc1d1
od	od	k7c2
Siemens	siemens	k1gInSc1
Velaro	Velara	k1gFnSc5
<g/>
)	)	kIx)
a	a	k8xC
CHR2	CHR2	k1gFnSc1
(	(	kIx(
<g/>
odvozené	odvozený	k2eAgInPc1d1
od	od	k7c2
Kawasaki	Kawasak	k1gFnSc2
Šinkansen	Šinkansen	k1gInSc1
série	série	k1gFnSc2
E	E	kA
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
1000	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čína	Čína	k1gFnSc1
má	mít	k5eAaImIp3nS
také	také	k9
nejrozsáhlejší	rozsáhlý	k2eAgFnSc4d3
vysokorychlostní	vysokorychlostní	k2eAgFnSc4d1
síť	síť	k1gFnSc4
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
rychle	rychle	k6eAd1
roste	růst	k5eAaImIp3nS
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
podobně	podobně	k6eAd1
roste	růst	k5eAaImIp3nS
počet	počet	k1gInSc4
a	a	k8xC
parametry	parametr	k1gInPc4
jednotek	jednotka	k1gFnPc2
jednotlivých	jednotlivý	k2eAgFnPc2d1
řad	řada	k1gFnPc2
CRH	CRH	kA
(	(	kIx(
<g/>
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
,	,	kIx,
380	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řady	řada	k1gFnSc2
380	#num#	k4
mají	mít	k5eAaImIp3nP
homologaci	homologace	k1gFnSc4
na	na	k7c4
380	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
Jedna	jeden	k4xCgFnSc1
z	z	k7c2
nich	on	k3xPp3gFnPc2
je	být	k5eAaImIp3nS
držitelem	držitel	k1gMnSc7
světového	světový	k2eAgInSc2d1
rekordu	rekord	k1gInSc2
neupravených	upravený	k2eNgFnPc2d1
jednotek	jednotka	k1gFnPc2
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
níže	nízce	k6eAd2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
–	–	k?
Pro	pro	k7c4
vysokorychlostní	vysokorychlostní	k2eAgFnPc4d1
tratě	trať	k1gFnPc4
na	na	k7c6
Korejském	korejský	k2eAgInSc6d1
poloostrově	poloostrov	k1gInSc6
vyrábí	vyrábět	k5eAaImIp3nS
francouzský	francouzský	k2eAgInSc1d1
Alstom	Alstom	k1gInSc1
(	(	kIx(
<g/>
výrobce	výrobce	k1gMnSc1
TGV	TGV	kA
<g/>
)	)	kIx)
jednotky	jednotka	k1gFnSc2
KTX	KTX	kA
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
vynikají	vynikat	k5eAaImIp3nP
svou	svůj	k3xOyFgFnSc7
délkou	délka	k1gFnSc7
a	a	k8xC
dosahují	dosahovat	k5eAaImIp3nP
cestovních	cestovní	k2eAgFnPc2d1
rychlostí	rychlost	k1gFnPc2
okolo	okolo	k7c2
300	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
</s>
<s>
Uzbekistán	Uzbekistán	k1gInSc1
–	–	k?
První	první	k4xOgFnSc4
částečně	částečně	k6eAd1
vysokorychlostní	vysokorychlostní	k2eAgFnSc4d1
trať	trať	k1gFnSc4
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
spojuje	spojovat	k5eAaImIp3nS
uzbecká	uzbecký	k2eAgFnSc1d1
města	město	k1gNnSc2
Taškent	Taškent	k1gInSc1
a	a	k8xC
Samarkand	Samarkand	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Severní	severní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
</s>
<s>
USA	USA	kA
–	–	k?
Především	především	k9
plány	plán	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
dosáhne	dosáhnout	k5eAaPmIp3nS
Acela	Acela	k1gFnSc1
Express	express	k1gInSc1
jedoucí	jedoucí	k2eAgInSc1d1
po	po	k7c6
trati	trať	k1gFnSc6
Severovýchodního	severovýchodní	k2eAgInSc2d1
koridoru	koridor	k1gInSc2
(	(	kIx(
<g/>
z	z	k7c2
Bostonu	Boston	k1gInSc2
přes	přes	k7c4
New	New	k1gFnSc4
York	York	k1gInSc1
<g/>
,	,	kIx,
Philadelphii	Philadelphia	k1gFnSc4
a	a	k8xC
Baltimore	Baltimore	k1gInSc4
do	do	k7c2
Washingtonu	Washington	k1gInSc2
D.	D.	kA
C.	C.	kA
<g/>
)	)	kIx)
na	na	k7c6
dvou	dva	k4xCgInPc6
krátkých	krátký	k2eAgInPc6d1
úsecích	úsek	k1gInPc6
240	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
,	,	kIx,
průměrná	průměrný	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
je	být	k5eAaImIp3nS
však	však	k9
pouze	pouze	k6eAd1
109	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
</s>
<s>
Kanada	Kanada	k1gFnSc1
–	–	k?
Existují	existovat	k5eAaImIp3nP
pouze	pouze	k6eAd1
plány	plán	k1gInPc4
<g/>
,	,	kIx,
výrobcem	výrobce	k1gMnSc7
bude	být	k5eAaImBp3nS
pravděpodobně	pravděpodobně	k6eAd1
Bombardier	Bombardier	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
V	v	k7c6
ostatních	ostatní	k2eAgInPc6d1
světadílech	světadíl	k1gInPc6
tyto	tento	k3xDgInPc1
vlaky	vlak	k1gInPc1
nejsou	být	k5eNaImIp3nP
provozovány	provozovat	k5eAaImNgInP
<g/>
.	.	kIx.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc2d3
rychlosti	rychlost	k1gFnSc2
dosažené	dosažený	k2eAgInPc1d1
vysokorychlostními	vysokorychlostní	k2eAgInPc7d1
vlaky	vlak	k1gInPc7
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
přehledu	přehled	k1gInSc6
jsou	být	k5eAaImIp3nP
uváděny	uvádět	k5eAaImNgInP
i	i	k9
vlaky	vlak	k1gInPc1
s	s	k7c7
magnetickým	magnetický	k2eAgInSc7d1
pohonem	pohon	k1gInSc7
<g/>
,	,	kIx,
nejsou	být	k5eNaImIp3nP
zde	zde	k6eAd1
ale	ale	k8xC
jiná	jiný	k2eAgFnSc1d1
kolejnicová	kolejnicový	k2eAgFnSc1d1
nebo	nebo	k8xC
magnetická	magnetický	k2eAgNnPc1d1
zařízení	zařízení	k1gNnPc1
(	(	kIx(
<g/>
katapulty	katapult	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
(	(	kIx(
<g/>
Pro	pro	k7c4
srovnání	srovnání	k1gNnSc4
<g/>
:	:	kIx,
1938	#num#	k4
–	–	k?
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
–	–	k?
parní	parní	k2eAgFnSc1d1
lokomotiva	lokomotiva	k1gFnSc1
LNER	LNER	kA
A4	A4	k1gFnSc1
Mallard	Mallard	k1gInSc1
–	–	k?
203	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
</s>
<s>
1903	#num#	k4
–	–	k?
Německo	Německo	k1gNnSc1
–	–	k?
experimentální	experimentální	k2eAgInSc4d1
elektrický	elektrický	k2eAgInSc4d1
vůz	vůz	k1gInSc4
Siemens	siemens	k1gInSc1
<g/>
/	/	kIx~
<g/>
AEG	AEG	kA
–	–	k?
206,7	206,7	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1955	#num#	k4
<g/>
,	,	kIx,
29	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
–	–	k?
Francie	Francie	k1gFnSc2
–	–	k?
lokomotiva	lokomotiva	k1gFnSc1
BB	BB	kA
9004	#num#	k4
–	–	k?
331	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
(	(	kIx(
<g/>
po	po	k7c6
8	#num#	k4
minutách	minuta	k1gFnPc6
jízdy	jízda	k1gFnSc2
se	se	k3xPyFc4
3	#num#	k4
vagony	vagon	k1gInPc7
<g/>
,	,	kIx,
výkon	výkon	k1gInSc1
3000	#num#	k4
kW	kW	kA
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1963	#num#	k4
–	–	k?
Japonsko	Japonsko	k1gNnSc4
–	–	k?
Šinkansen	Šinkansen	k1gInSc1
–	–	k?
256	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
1967	#num#	k4
–	–	k?
Francie	Francie	k1gFnSc2
–	–	k?
TGV	TGV	kA
–	–	k?
318	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
(	(	kIx(
<g/>
typ	typ	k1gInSc1
s	s	k7c7
plynovou	plynový	k2eAgFnSc7d1
turbínou	turbína	k1gFnSc7
<g/>
)	)	kIx)
</s>
<s>
1968	#num#	k4
–	–	k?
Německo	Německo	k1gNnSc4
–	–	k?
200	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
1972	#num#	k4
–	–	k?
Japonsko	Japonsko	k1gNnSc4
–	–	k?
Šinkansen	Šinkansen	k1gInSc1
–	–	k?
286	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
1974	#num#	k4
–	–	k?
Německo	Německo	k1gNnSc4
–	–	k?
EET-01	EET-01	k1gFnSc1
–	–	k?
230	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
1974	#num#	k4
–	–	k?
Francie	Francie	k1gFnSc2
–	–	k?
Aérotrain	Aérotrain	k1gInSc1
–	–	k?
430,2	430,2	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
1975	#num#	k4
–	–	k?
Německo	Německo	k1gNnSc4
–	–	k?
Comet	Comet	k1gInSc1
–	–	k?
401,3	401,3	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
1978	#num#	k4
–	–	k?
Japonsko	Japonsko	k1gNnSc4
–	–	k?
HSST01	HSST01	k1gFnSc1
–	–	k?
307,8	307,8	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
1978	#num#	k4
–	–	k?
Japonsko	Japonsko	k1gNnSc4
–	–	k?
HSST02	HSST02	k1gFnSc1
–	–	k?
110	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
1978	#num#	k4
–	–	k?
Itálie	Itálie	k1gFnSc2
–	–	k?
Pendolino	Pendolin	k2eAgNnSc1d1
–	–	k?
250	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
1979	#num#	k4
–	–	k?
Japonsko	Japonsko	k1gNnSc4
–	–	k?
Šinkansen	Šinkansen	k1gInSc1
–	–	k?
319	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
1979	#num#	k4
–	–	k?
Japonsko	Japonsko	k1gNnSc1
–	–	k?
JR-Maglev	JR-Maglev	k1gMnSc1
ML500	ML500	k1gFnSc2
–	–	k?
517	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
1981	#num#	k4
–	–	k?
Francie	Francie	k1gFnSc2
–	–	k?
TGV	TGV	kA
PSE	pes	k1gMnSc5
–	–	k?
380	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
1985	#num#	k4
–	–	k?
Německo	Německo	k1gNnSc1
–	–	k?
ICE	ICE	kA
–	–	k?
300	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
1987	#num#	k4
–	–	k?
Japonsko	Japonsko	k1gNnSc1
–	–	k?
JR-Maglev	JR-Maglev	k1gMnSc1
MLU001	MLU001	k1gFnSc2
–	–	k?
400,8	400,8	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
1988	#num#	k4
–	–	k?
Německo	Německo	k1gNnSc1
–	–	k?
ICE	ICE	kA
–	–	k?
406	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
1988	#num#	k4
–	–	k?
Německo	Německo	k1gNnSc4
–	–	k?
Transrapid	Transrapid	k1gInSc1
06	#num#	k4
–	–	k?
412,6	412,6	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
1989	#num#	k4
–	–	k?
Německo	Německo	k1gNnSc4
–	–	k?
Transrapid	Transrapid	k1gInSc1
07	#num#	k4
–	–	k?
436	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
1990	#num#	k4
–	–	k?
Francie	Francie	k1gFnSc2
–	–	k?
TGV	TGV	kA
Atlantique	Atlantique	k1gInSc1
–	–	k?
515,3	515,3	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1992	#num#	k4
–	–	k?
Japonsko	Japonsko	k1gNnSc4
–	–	k?
Šinkansen	Šinkansen	k1gInSc1
–	–	k?
350	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
1993	#num#	k4
–	–	k?
Japonsko	Japonsko	k1gNnSc4
–	–	k?
Šinkansen	Šinkansen	k1gInSc1
–	–	k?
425	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
1993	#num#	k4
–	–	k?
Německo	Německo	k1gNnSc4
–	–	k?
Transrapid	Transrapid	k1gInSc1
07	#num#	k4
–	–	k?
450	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
1994	#num#	k4
–	–	k?
Japonsko	Japonsko	k1gNnSc1
–	–	k?
JR-Maglev	JR-Maglev	k1gMnSc1
MLU002N	MLU002N	k1gFnSc2
–	–	k?
431	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
1996	#num#	k4
–	–	k?
Japonsko	Japonsko	k1gNnSc4
–	–	k?
Šinkansen	Šinkansen	k1gInSc1
–	–	k?
446	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
1997	#num#	k4
–	–	k?
Japonsko	Japonsko	k1gNnSc1
–	–	k?
JR-Maglev	JR-Maglev	k1gMnSc1
MLX01	MLX01	k1gFnSc2
–	–	k?
550	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
1999	#num#	k4
–	–	k?
Japonsko	Japonsko	k1gNnSc1
–	–	k?
JR-Maglev	JR-Maglev	k1gMnSc1
MLX01	MLX01	k1gFnSc2
–	–	k?
552	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
2003	#num#	k4
–	–	k?
Německo	Německo	k1gNnSc4
–	–	k?
Transrapid	Transrapid	k1gInSc1
08	#num#	k4
–	–	k?
501	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
2003	#num#	k4
–	–	k?
Japonsko	Japonsko	k1gNnSc1
–	–	k?
JR-Maglev	JR-Maglev	k1gMnSc1
MLX01	MLX01	k1gFnSc2
–	–	k?
581	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
2004	#num#	k4
–	–	k?
Korejská	korejský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
–	–	k?
HSR-	HSR-	k1gFnSc1
<g/>
350	#num#	k4
<g/>
x	x	k?
–	–	k?
352.4	352.4	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
2006	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
–	–	k?
Německo	Německo	k1gNnSc1
–	–	k?
ÖBB	ÖBB	kA
1216	#num#	k4
050	#num#	k4
Taurus	Taurus	k1gInSc1
3	#num#	k4
–	–	k?
357	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
2007	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
–	–	k?
Francie	Francie	k1gFnSc2
–	–	k?
TGV	TGV	kA
Duplex	duplex	k1gInSc1
–	–	k?
574,8	574,8	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
(	(	kIx(
<g/>
momentální	momentální	k2eAgMnSc1d1
držitel	držitel	k1gMnSc1
světového	světový	k2eAgInSc2d1
rekordu	rekord	k1gInSc2
modifikovaných	modifikovaný	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
kolo-kolejnice	kolo-kolejnice	k1gFnSc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2008	#num#	k4
–	–	k?
Čína	Čína	k1gFnSc1
–	–	k?
CRH3C	CRH3C	k1gFnSc1
(	(	kIx(
<g/>
Siemens	siemens	k1gInSc1
Velaro	Velara	k1gFnSc5
<g/>
)	)	kIx)
–	–	k?
394,2	394,2	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2010	#num#	k4
–	–	k?
Čína	Čína	k1gFnSc1
–	–	k?
CRH380A	CRH380A	k1gFnSc1
(	(	kIx(
<g/>
Siemens	siemens	k1gInSc1
Velaro	Velara	k1gFnSc5
<g/>
)	)	kIx)
–	–	k?
416,6	416,6	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
2010	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
–	–	k?
Čína	Čína	k1gFnSc1
–	–	k?
CRH380AL	CRH380AL	k1gFnSc1
(	(	kIx(
<g/>
Siemens	siemens	k1gInSc1
Velaro	Velara	k1gFnSc5
<g/>
)	)	kIx)
–	–	k?
486,1	486,1	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
(	(	kIx(
<g/>
nemodifikovaná	modifikovaný	k2eNgFnSc1d1
komerční	komerční	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2011	#num#	k4
<g/>
,	,	kIx,
9	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
–	–	k?
Čína	Čína	k1gFnSc1
–	–	k?
CRH380BL	CRH380BL	k1gFnSc1
(	(	kIx(
<g/>
Siemens	siemens	k1gInSc1
Velaro	Velara	k1gFnSc5
<g/>
)	)	kIx)
–	–	k?
487,3	487,3	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
(	(	kIx(
<g/>
momentální	momentální	k2eAgMnSc1d1
držitel	držitel	k1gMnSc1
světového	světový	k2eAgInSc2d1
rekordu	rekord	k1gInSc2
nemodifikovaných	modifikovaný	k2eNgFnPc2d1
komerčních	komerční	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
kolo-kolejnice	kolo-kolejnice	k1gFnSc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2015	#num#	k4
<g/>
,	,	kIx,
21	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
–	–	k?
Japonsko	Japonsko	k1gNnSc1
–	–	k?
JR-Maglev	JR-Maglev	k1gFnSc1
–	–	k?
603	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
SMĚRNICE	směrnice	k1gFnPc4
EVROPSKÉHO	evropský	k2eAgInSc2d1
PARLAMENTU	parlament	k1gInSc2
A	A	kA
RADY	rada	k1gFnSc2
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
57	#num#	k4
<g/>
/	/	kIx~
<g/>
ES	ES	kA
ze	z	k7c2
dne	den	k1gInSc2
17	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2008	#num#	k4
o	o	k7c6
interoperabilitě	interoperabilita	k1gFnSc6
železničního	železniční	k2eAgInSc2d1
systému	systém	k1gInSc2
ve	v	k7c6
Společenství	společenství	k1gNnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008-06-17	2008-06-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
HINČICA	HINČICA	kA
<g/>
,	,	kIx,
Vít	Vít	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Italo	Italo	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
k-report	k-report	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
<g/>
,	,	kIx,
2012-02-19	2012-02-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
V	v	k7c6
Itálii	Itálie	k1gFnSc6
zahájil	zahájit	k5eAaPmAgMnS
provoz	provoz	k1gInSc4
soukromý	soukromý	k2eAgMnSc1d1
dopravce	dopravce	k1gMnSc1
rychlovlaků	rychlovlak	k1gInPc2
NTV	NTV	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finanční	finanční	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
2012-04-20	2012-04-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BENT	BENT	kA
<g/>
,	,	kIx,
Mike	Mike	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hexie	Hexie	k1gFnSc1
Hao	Hao	k1gFnSc1
Overview	Overview	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Railvolution	Railvolution	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
6	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
22	#num#	k4
<g/>
–	–	k?
<g/>
25	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Čína	Čína	k1gFnSc1
se	se	k3xPyFc4
chvástá	chvástat	k5eAaImIp3nS
<g/>
:	:	kIx,
obyčejný	obyčejný	k2eAgInSc4d1
osobní	osobní	k2eAgInSc4d1
vlak	vlak	k1gInSc4
jel	jet	k5eAaImAgMnS
rekordních	rekordní	k2eAgFnPc2d1
486	#num#	k4
km	km	kA
v	v	k7c6
hodině	hodina	k1gFnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
lidovky	lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2010-12-03	2010-12-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
China	China	k1gFnSc1
hits	hitsa	k1gFnPc2
486	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
in	in	k?
high	high	k1gInSc1
speed	speed	k1gMnSc1
trials	trials	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Railway	Railwaum	k1gNnPc7
Gazette	Gazett	k1gMnSc5
International	International	k1gMnSc5
<g/>
,	,	kIx,
2010-12-03	2010-12-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
((	((	k?
<g/>
anglicky	anglicky	k6eAd1
<g/>
))	))	k?
↑	↑	k?
130	#num#	k4
years	years	k6eAd1
of	of	k?
passenger	passenger	k1gMnSc1
traffic	traffic	k1gMnSc1
with	with	k1gMnSc1
Siemens	siemens	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
siemens	siemens	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2011-09-30	2011-09-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
((	((	k?
<g/>
anglicky	anglicky	k6eAd1
<g/>
))	))	k?
↑	↑	k?
JELEN	Jelen	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
SELLNER	SELLNER	kA
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svět	svět	k1gInSc1
rychlých	rychlý	k2eAgFnPc2d1
kolejí	kolej	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
NADATUR	NADATUR	kA
spol	spol	k1gInSc1
<g/>
.	.	kIx.
s	s	k7c7
r.	r.	kA
<g/>
o.	o.	k?
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
163	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85884	#num#	k4
<g/>
-	-	kIx~
<g/>
76	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Boj	boj	k1gInSc1
pokračuje	pokračovat	k5eAaImIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Rekordy	rekord	k1gInPc7
TGV	TGV	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
parostroj	parostroj	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
1999-06-28	1999-06-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Wuhan	Wuhan	k1gInSc1
–	–	k?
Guangzhou	Guangzha	k1gMnSc7
line	linout	k5eAaImIp3nS
opens	opens	k1gInSc4
at	at	k?
380	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Railway	Railwaum	k1gNnPc7
Gazette	Gazett	k1gMnSc5
International	International	k1gMnSc5
<g/>
,	,	kIx,
2010-12-04	2010-12-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
((	((	k?
<g/>
anglicky	anglicky	k6eAd1
<g/>
))	))	k?
↑	↑	k?
ČT	ČT	kA
<g/>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čínský	čínský	k2eAgInSc1d1
osobní	osobní	k2eAgInSc1d1
vlak	vlak	k1gInSc1
zajel	zajet	k5eAaPmAgInS
rekord	rekord	k1gInSc4
–	–	k?
486	#num#	k4
km	km	kA
za	za	k7c4
hodinu	hodina	k1gFnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
2010-12-03	2010-12-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
J.	J.	kA
<g/>
,	,	kIx,
Filip	Filip	k1gMnSc1
<g/>
;	;	kIx,
VINŠ	Vinš	k1gMnSc1
<g/>
,	,	kIx,
Matouš	Matouš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysokorychlostní	vysokorychlostní	k2eAgInSc4d1
boom	boom	k1gInSc4
v	v	k7c6
Číně	Čína	k1gFnSc6
–	–	k?
současnost	současnost	k1gFnSc1
a	a	k8xC
výhledy	výhled	k1gInPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ŽelPage	ŽelPage	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2011-02-07	2011-02-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
China	China	k1gFnSc1
high-speed	high-speed	k1gInSc1
rail	rail	k1gInSc1
zooms	zooms	k1gInSc1
past	past	k1gFnSc1
old	old	k?
record	record	k1gInSc1
at	at	k?
487	#num#	k4
km	km	kA
per	pero	k1gNnPc2
hour	hour	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ocean	Ocean	k1gInSc1
Shipping	Shipping	k1gInSc4
Communication	Communication	k1gInSc1
China	China	k1gFnSc1
<g/>
,	,	kIx,
2011-01-18	2011-01-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
((	((	k?
<g/>
anglicky	anglicky	k6eAd1
<g/>
))	))	k?
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Vysokorychlostní	vysokorychlostní	k2eAgFnSc1d1
trať	trať	k1gFnSc1
</s>
<s>
Šinkansen	Šinkansno	k1gNnPc2
–	–	k?
Japonské	japonský	k2eAgFnSc2d1
vysokorychlostní	vysokorychlostní	k2eAgFnSc2d1
železnice	železnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
TGV	TGV	kA
–	–	k?
Francouzské	francouzský	k2eAgInPc4d1
vysokorychlostní	vysokorychlostní	k2eAgInPc4d1
vlaky	vlak	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
AGV	AGV	kA
–	–	k?
Francouzské	francouzský	k2eAgInPc4d1
vysokorychlostní	vysokorychlostní	k2eAgInPc4d1
vlaky	vlak	k1gInPc4
nové	nový	k2eAgFnSc2d1
konstrukce	konstrukce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
ICE	ICE	kA
–	–	k?
Německé	německý	k2eAgInPc4d1
vysokorychlostní	vysokorychlostní	k2eAgInPc4d1
vlaky	vlak	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
AVE	ave	k1gNnSc1
–	–	k?
Španělská	španělský	k2eAgFnSc1d1
vysokorychlostní	vysokorychlostní	k2eAgFnSc1d1
železnice	železnice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Eurostar	Eurostar	k1gMnSc1
–	–	k?
Vysokorychlostní	vysokorychlostní	k2eAgNnSc1d1
železniční	železniční	k2eAgNnSc1d1
spojení	spojení	k1gNnSc1
Londýna	Londýn	k1gInSc2
s	s	k7c7
Paříží	Paříž	k1gFnSc7
a	a	k8xC
Bruselem	Brusel	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Cisalpino	Cisalpin	k2eAgNnSc1d1
–	–	k?
Vysokorychlostní	vysokorychlostní	k2eAgNnSc1d1
železniční	železniční	k2eAgNnSc1d1
spojení	spojení	k1gNnSc1
Itálie	Itálie	k1gFnSc2
a	a	k8xC
Švýcarska	Švýcarsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Pendolino	Pendolin	k2eAgNnSc1d1
–	–	k?
Italské	italský	k2eAgInPc4d1
vysokorychlostní	vysokorychlostní	k2eAgInPc4d1
vlaky	vlak	k1gInPc4
s	s	k7c7
aktivním	aktivní	k2eAgNnSc7d1
naklápěním	naklápění	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Korea	Korea	k1gFnSc1
Train	Train	k1gInSc1
Express	express	k1gInSc1
–	–	k?
Jihokorejská	jihokorejský	k2eAgFnSc1d1
vysokorychlostní	vysokorychlostní	k2eAgFnSc1d1
železnice	železnice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
dále	daleko	k6eAd2
<g/>
:	:	kIx,
</s>
<s>
Turbotrain	Turbotrain	k2eAgInSc1d1
–	–	k?
turbínový	turbínový	k2eAgInSc1d1
pohon	pohon	k1gInSc1
<g/>
,	,	kIx,
předchůdce	předchůdce	k1gMnSc1
TGV	TGV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
mimo	mimo	k7c4
provoz	provoz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Aérotrain	Aérotrain	k1gInSc1
–	–	k?
Nekonvenční	konvenčnět	k5eNaImIp3nS
vysokorychlostní	vysokorychlostní	k2eAgInSc1d1
vlak	vlak	k1gInSc1
–	–	k?
vznášedlo	vznášedlo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opuštěný	opuštěný	k2eAgInSc1d1
francouzský	francouzský	k2eAgInSc1d1
experiment	experiment	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Maglev	Maglet	k5eAaPmDgInS
–	–	k?
Nekonvenční	konvenční	k2eNgInSc1d1
vysokorychlostní	vysokorychlostní	k2eAgInSc1d1
vlak	vlak	k1gInSc1
–	–	k?
magnetická	magnetický	k2eAgFnSc1d1
levitace	levitace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Transrapid	Transrapid	k1gInSc1
–	–	k?
Nekonvenční	konvenčnět	k5eNaImIp3nS
vysokorychlostní	vysokorychlostní	k2eAgInSc1d1
vlak	vlak	k1gInSc1
–	–	k?
magnetická	magnetický	k2eAgFnSc1d1
levitace	levitace	k1gFnSc1
v	v	k7c6
německé	německý	k2eAgFnSc6d1
variantě	varianta	k1gFnSc6
</s>
<s>
Trans	trans	k1gInSc1
Europ	Europ	k1gInSc1
Express	express	k1gInSc1
(	(	kIx(
<g/>
TEE	Tea	k1gFnSc6
<g/>
)	)	kIx)
–	–	k?
Někdejší	někdejší	k2eAgFnSc1d1
síť	síť	k1gFnSc1
rychlých	rychlý	k2eAgInPc2d1
vlaků	vlak	k1gInPc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
označované	označovaný	k2eAgFnSc6d1
také	také	k6eAd1
jako	jako	k9
vysokorychlostní	vysokorychlostní	k2eAgFnSc1d1
<g/>
,	,	kIx,
předchůdce	předchůdce	k1gMnSc1
EuroCity	EuroCita	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
vysokorychlostní	vysokorychlostní	k2eAgInSc4d1
vlak	vlak	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
rychlovlak	rychlovlak	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
UIC	UIC	kA
<g/>
:	:	kIx,
General	General	k1gMnSc1
definitions	definitionsa	k1gFnPc2
of	of	k?
higspeed	higspeed	k1gMnSc1
train	train	k1gMnSc1
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
UIC	UIC	kA
<g/>
:	:	kIx,
World	World	k1gMnSc1
High	High	k1gMnSc1
Speed	Speed	k1gMnSc1
Rollink	Rollink	k1gInSc4
Stock	Stock	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přehled	přehled	k1gInSc1
vysokorychlostních	vysokorychlostní	k2eAgFnPc2d1
vlakových	vlakový	k2eAgFnPc2d1
souprav	souprava	k1gFnPc2
k	k	k7c3
listopadu	listopad	k1gInSc3
2011	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Vysokorychlostní	vysokorychlostní	k2eAgInPc1d1
vlaky	vlak	k1gInPc1
>	>	kIx)
<g/>
249	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
AGV	AGV	kA
(	(	kIx(
<g/>
F	F	kA
/	/	kIx~
I	I	kA
<g/>
)	)	kIx)
•	•	k?
AVE	ave	k1gNnSc4
(	(	kIx(
<g/>
E	E	kA
<g/>
)	)	kIx)
•	•	k?
ETR	ETR	kA
401	#num#	k4
(	(	kIx(
<g/>
I	I	kA
<g/>
)	)	kIx)
•	•	k?
ETR	ETR	kA
450	#num#	k4
(	(	kIx(
<g/>
I	I	kA
<g/>
)	)	kIx)
•	•	k?
CRH	CRH	kA
(	(	kIx(
<g/>
CN	CN	kA
<g/>
)	)	kIx)
•	•	k?
ETR	ETR	kA
460	#num#	k4
(	(	kIx(
<g/>
F	F	kA
/	/	kIx~
I	I	kA
<g/>
)	)	kIx)
•	•	k?
ETR	ETR	kA
480	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
I	I	kA
<g/>
)	)	kIx)
•	•	k?
ETR	ETR	kA
500	#num#	k4
(	(	kIx(
<g/>
I	I	kA
<g/>
)	)	kIx)
•	•	k?
ETR	ETR	kA
600	#num#	k4
(	(	kIx(
<g/>
I	I	kA
<g/>
,	,	kIx,
CN	CN	kA
<g/>
)	)	kIx)
•	•	k?
ETR	ETR	kA
610	#num#	k4
(	(	kIx(
<g/>
CH	Ch	kA
/	/	kIx~
I	I	kA
<g/>
)	)	kIx)
•	•	k?
Eurostar	Eurostar	k1gMnSc1
(	(	kIx(
<g/>
UK	UK	kA
/	/	kIx~
F	F	kA
/	/	kIx~
B	B	kA
<g/>
)	)	kIx)
•	•	k?
ICE	ICE	kA
(	(	kIx(
<g/>
D	D	kA
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
F	F	kA
/	/	kIx~
B	B	kA
/	/	kIx~
NL	NL	kA
/	/	kIx~
A	A	kA
/	/	kIx~
CH	Ch	kA
/	/	kIx~
E	E	kA
/	/	kIx~
RU	RU	kA
/	/	kIx~
CN	CN	kA
<g/>
)	)	kIx)
•	•	k?
KTX	KTX	kA
(	(	kIx(
<g/>
KR	KR	kA
<g/>
)	)	kIx)
•	•	k?
RENFE	RENFE	kA
S-120	S-120	k1gFnSc2
(	(	kIx(
<g/>
E	E	kA
/	/	kIx~
TR	TR	kA
<g/>
)	)	kIx)
•	•	k?
Šinkansen	Šinkansen	k1gInSc1
(	(	kIx(
<g/>
J	J	kA
/	/	kIx~
TW	TW	kA
<g/>
,	,	kIx,
CN	CN	kA
<g/>
)	)	kIx)
•	•	k?
Talgo	Talgo	k1gMnSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
E	E	kA
<g/>
)	)	kIx)
•	•	k?
TGV	TGV	kA
(	(	kIx(
<g/>
F	F	kA
/	/	kIx~
D	D	kA
/	/	kIx~
CH	Ch	kA
/	/	kIx~
I	I	kA
/	/	kIx~
E	E	kA
/	/	kIx~
B	B	kA
/	/	kIx~
LX	LX	kA
<g/>
)	)	kIx)
•	•	k?
Thalys	Thalysa	k1gFnPc2
(	(	kIx(
<g/>
F	F	kA
/	/	kIx~
B	B	kA
/	/	kIx~
NL	NL	kA
/	/	kIx~
D	D	kA
<g/>
)	)	kIx)
•	•	k?
V	v	k7c6
250	#num#	k4
(	(	kIx(
<g/>
B	B	kA
/	/	kIx~
NL	NL	kA
<g/>
)	)	kIx)
•	•	k?
Zefiro	Zefiro	k1gNnSc4
(	(	kIx(
<g/>
CN	CN	kA
/	/	kIx~
I	I	kA
<g/>
)	)	kIx)
</s>
<s>
Transrapid	Transrapid	k1gInSc1
<g/>
*	*	kIx~
(	(	kIx(
<g/>
D	D	kA
/	/	kIx~
CN	CN	kA
<g/>
)	)	kIx)
•	•	k?
JR-Maglev	JR-Maglev	k1gFnSc1
<g/>
*	*	kIx~
(	(	kIx(
<g/>
J	J	kA
<g/>
)	)	kIx)
•	•	k?
Swissmetro	Swissmetro	k1gNnSc1
<g/>
**	**	k?
(	(	kIx(
<g/>
CH	Ch	kA
<g/>
)	)	kIx)
*	*	kIx~
<g/>
technologie	technologie	k1gFnSc1
Maglev	Maglev	k1gMnSc1
**	**	k?
<g/>
Maglev	Maglev	k1gMnSc1
ve	v	k7c6
vakuovém	vakuový	k2eAgInSc6d1
tunelu	tunel	k1gInSc6
(	(	kIx(
<g/>
Vactrain	Vactrain	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Železnice	železnice	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4113934-3	4113934-3	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
11012	#num#	k4
</s>
