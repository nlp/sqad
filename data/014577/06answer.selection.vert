<s>
Vysokorychlostní	vysokorychlostní	k2eAgInSc1d1
vlak	vlak	k1gInSc1
je	být	k5eAaImIp3nS
vlak	vlak	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
konstrukce	konstrukce	k1gFnSc1
mu	on	k3xPp3gMnSc3
umožňuje	umožňovat	k5eAaImIp3nS
dosahovat	dosahovat	k5eAaImF
rychlostí	rychlost	k1gFnSc7
nejméně	málo	k6eAd3
200	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
prioritně	prioritně	k6eAd1
určen	určit	k5eAaPmNgInS
pro	pro	k7c4
provoz	provoz	k1gInSc4
na	na	k7c6
vysokorychlostní	vysokorychlostní	k2eAgFnSc6d1
trati	trať	k1gFnSc6
<g/>
.	.	kIx.
</s>