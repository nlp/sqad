<s>
Jakabov	Jakabov	k1gInSc1
palác	palác	k1gInSc1
</s>
<s>
Jakabov	Jakabov	k1gInSc1
palác	palác	k1gInSc1
Základní	základní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Sloh	sloha	k1gFnPc2
</s>
<s>
eklektismus	eklektismus	k1gInSc1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
1899	#num#	k4
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Mlynská	Mlynská	k1gFnSc1
30	#num#	k4
<g/>
,	,	kIx,
040	#num#	k4
01	#num#	k4
Košice	Košice	k1gInPc1
<g/>
,	,	kIx,
Košice	Košice	k1gInPc1
<g/>
,	,	kIx,
Slovensko	Slovensko	k1gNnSc1
Slovensko	Slovensko	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
48	#num#	k4
<g/>
°	°	k?
<g/>
43	#num#	k4
<g/>
′	′	k?
<g/>
18,84	18,84	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
21	#num#	k4
<g/>
°	°	k?
<g/>
15	#num#	k4
<g/>
′	′	k?
<g/>
45,72	45,72	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jakabův	Jakabův	k2eAgInSc1d1
palác	palác	k1gInSc1
</s>
<s>
Jakabův	Jakabův	k2eAgInSc1d1
palác	palác	k1gInSc1
zblízka	zblízka	k6eAd1
</s>
<s>
Jakabov	Jakabov	k1gInSc1
palác	palác	k1gInSc1
je	být	k5eAaImIp3nS
budova	budova	k1gFnSc1
v	v	k7c6
Košicích	Košice	k1gInPc6
stojící	stojící	k2eAgMnSc1d1
u	u	k7c2
koryta	koryto	k1gNnSc2
někdejšího	někdejší	k2eAgInSc2d1
Mlýnského	mlýnský	k2eAgInSc2d1
náhonu	náhon	k1gInSc2
na	na	k7c6
rohu	roh	k1gInSc6
Mlýnské	mlýnský	k2eAgFnSc2d1
a	a	k8xC
Štefánikovy	Štefánikův	k2eAgFnSc2d1
ulice	ulice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Budovu	budova	k1gFnSc4
sám	sám	k3xTgMnSc1
pro	pro	k7c4
sebe	sebe	k3xPyFc4
navrhl	navrhnout	k5eAaPmAgMnS
stavitel	stavitel	k1gMnSc1
Peter	Peter	k1gMnSc1
Jakab	Jakab	k1gMnSc1
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
mnoha	mnoho	k4c2
eklektických	eklektický	k2eAgFnPc2d1
budov	budova	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavba	stavba	k1gFnSc1
byla	být	k5eAaImAgFnS
dokončena	dokončit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1899	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
stavbě	stavba	k1gFnSc6
byly	být	k5eAaImAgFnP
použity	použít	k5eAaPmNgFnP
i	i	k9
vyřazené	vyřazený	k2eAgFnPc1d1
kamenné	kamenný	k2eAgFnPc1d1
části	část	k1gFnPc1
z	z	k7c2
Dómu	dóm	k1gInSc2
svaté	svatý	k2eAgFnSc2d1
Alžběty	Alžběta	k1gFnSc2
<g/>
,	,	kIx,
při	při	k7c6
jehož	jehož	k3xOyRp3gFnSc6
rekonstrukci	rekonstrukce	k1gFnSc6
získal	získat	k5eAaPmAgInS
Jakab	Jakab	k1gInSc1
stavební	stavební	k2eAgInSc4d1
materiál	materiál	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
1968	#num#	k4
palác	palác	k1gInSc1
stál	stát	k5eAaImAgInS
na	na	k7c6
břehu	břeh	k1gInSc6
Mlýnského	mlýnský	k2eAgInSc2d1
náhonu	náhon	k1gInSc2
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
byla	být	k5eAaImAgFnS
vytvořena	vytvořit	k5eAaPmNgFnS
nádherná	nádherný	k2eAgFnSc1d1
idyla	idyla	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náhon	náhon	k1gInSc1
byl	být	k5eAaImAgInS
však	však	k9
v	v	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
zrušen	zrušit	k5eAaPmNgInS
a	a	k8xC
palác	palác	k1gInSc1
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
stojí	stát	k5eAaImIp3nS
na	na	k7c6
okraji	okraj	k1gInSc6
rušné	rušný	k2eAgFnSc2d1
čtyřproudé	čtyřproudý	k2eAgFnSc2d1
komunikace	komunikace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Majitelé	majitel	k1gMnPc1
</s>
<s>
Palác	palác	k1gInSc4
vlastnil	vlastnit	k5eAaImAgMnS
Peter	Peter	k1gMnSc1
Jakab	Jakab	k1gMnSc1
do	do	k7c2
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
(	(	kIx(
<g/>
počátkem	počátkem	k7c2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
Jakabovci	Jakabovec	k1gMnPc1
v	v	k7c6
roce	rok	k1gInSc6
1908	#num#	k4
odstěhovali	odstěhovat	k5eAaPmAgMnP
z	z	k7c2
Košic	Košice	k1gInPc2
<g/>
,	,	kIx,
prodali	prodat	k5eAaPmAgMnP
palác	palác	k1gInSc4
Hugovi	Hugo	k1gMnSc3
Barkányimu	Barkányim	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
palác	palác	k1gInSc1
přešel	přejít	k5eAaPmAgInS
do	do	k7c2
majetku	majetek	k1gInSc2
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
byl	být	k5eAaImAgInS
na	na	k7c4
palác	palác	k1gInSc4
uplatněn	uplatnit	k5eAaPmNgInS
restituční	restituční	k2eAgInSc1d1
nárok	nárok	k1gInSc1
od	od	k7c2
Kataríny	Katarína	k1gFnSc2
Póšové	Póšové	k2eAgFnSc2d1
-	-	kIx~
dcery	dcera	k1gFnSc2
Huga	Hugo	k1gMnSc4
Barkányiho	Barkányi	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
soud	soud	k1gInSc1
rozhodl	rozhodnout	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
Jakabov	Jakabov	k1gInSc1
palác	palác	k1gInSc1
patří	patřit	k5eAaImIp3nS
do	do	k7c2
dědictví	dědictví	k1gNnSc2
po	po	k7c6
Barkányiových	Barkányiový	k2eAgFnPc6d1
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
naopak	naopak	k6eAd1
rozhodl	rozhodnout	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
nepatří	patřit	k5eNaImIp3nS
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
zase	zase	k9
<g/>
,	,	kIx,
že	že	k8xS
patří	patřit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
Košice	Košice	k1gInPc1
rozsudek	rozsudek	k1gInSc4
napadlo	napadnout	k5eAaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roku	rok	k1gInSc6
2005	#num#	k4
soud	soud	k1gInSc1
rozhodl	rozhodnout	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
palác	palác	k1gInSc1
patří	patřit	k5eAaImIp3nS
městu	město	k1gNnSc3
Košice	Košice	k1gInPc1
<g/>
,	,	kIx,
ale	ale	k8xC
údajná	údajný	k2eAgFnSc1d1
majitelka	majitelka	k1gFnSc1
se	se	k3xPyFc4
proti	proti	k7c3
tomuto	tento	k3xDgNnSc3
rozhodnutí	rozhodnutí	k1gNnSc3
odvolala	odvolat	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
osvobození	osvobození	k1gNnSc6
města	město	k1gNnSc2
Sovětskou	sovětský	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
byl	být	k5eAaImAgInS
nějaký	nějaký	k3yIgInSc4
čas	čas	k1gInSc4
sídlem	sídlo	k1gNnSc7
prezidenta	prezident	k1gMnSc2
Československa	Československo	k1gNnSc2
dr	dr	kA
<g/>
.	.	kIx.
Edvarda	Edvard	k1gMnSc2
Beneše	Beneš	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1992	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
v	v	k7c6
budově	budova	k1gFnSc6
sídlila	sídlit	k5eAaImAgFnS
Britská	britský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
(	(	kIx(
<g/>
British	British	k1gMnSc1
Council	Council	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
současnosti	současnost	k1gFnSc6
je	být	k5eAaImIp3nS
využívána	využíván	k2eAgFnSc1d1
k	k	k7c3
významným	významný	k2eAgFnPc3d1
společenským	společenský	k2eAgFnPc3d1
akcím	akce	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Jakabov	Jakabov	k1gInSc1
palác	palác	k1gInSc1
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
http://www.cassovia.sk/korzar/archiv/clanok.php3?sub=12.12.2002/34096K%5B%5D	http://www.cassovia.sk/korzar/archiv/clanok.php3?sub=12.12.2002/34096K%5B%5D	k4
<g/>
↑	↑	k?
http://www.sme.sk/c/3577265/Mestu-Jakabov-palac-este-nepatri.html	http://www.sme.sk/c/3577265/Mestu-Jakabov-palac-este-nepatri.html	k1gMnSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Slovensko	Slovensko	k1gNnSc1
|	|	kIx~
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
</s>
