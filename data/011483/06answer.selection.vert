<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Jan	Jan	k1gMnSc1	Jan
Křtitel	křtitel	k1gMnSc1	křtitel
<g/>
,	,	kIx,	,
v	v	k7c6	v
pravoslavné	pravoslavný	k2eAgFnSc6d1	pravoslavná
tradici	tradice	k1gFnSc6	tradice
zvaný	zvaný	k2eAgMnSc1d1	zvaný
též	též	k6eAd1	též
Předchůdce	předchůdce	k1gMnSc1	předchůdce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
prorok	prorok	k1gMnSc1	prorok
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
figuruje	figurovat	k5eAaImIp3nS	figurovat
ve	v	k7c6	v
vyprávěných	vyprávěný	k2eAgNnPc6d1	vyprávěné
evangeliích	evangelium	k1gNnPc6	evangelium
<g/>
.	.	kIx.	.
</s>
