<s>
Rabi	rabi	k1gMnSc1	rabi
Levi	Lev	k1gFnSc2	Lev
ben	ben	k?	ben
Geršom	Geršom	k1gInSc1	Geršom
(	(	kIx(	(
<g/>
1288	[number]	k4	1288
<g/>
–	–	k?	–
<g/>
1344	[number]	k4	1344
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
řecky	řecky	k6eAd1	řecky
Gersonides	Gersonides	k1gMnSc1	Gersonides
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
také	také	k9	také
pod	pod	k7c7	pod
akronymem	akronymum	k1gNnSc7	akronymum
Ralbag	Ralbaga	k1gFnPc2	Ralbaga
(	(	kIx(	(
<g/>
ר	ר	k?	ר
<g/>
״	״	k?	״
<g/>
ג	ג	k?	ג
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
židovský	židovský	k2eAgMnSc1d1	židovský
rabín	rabín	k1gMnSc1	rabín
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
<g/>
,	,	kIx,	,
astronom	astronom	k1gMnSc1	astronom
a	a	k8xC	a
talmudista	talmudista	k1gMnSc1	talmudista
<g/>
.	.	kIx.	.
</s>
