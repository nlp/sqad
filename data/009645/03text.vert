<p>
<s>
Rabi	rabi	k1gMnSc1	rabi
Levi	Lev	k1gFnSc2	Lev
ben	ben	k?	ben
Geršom	Geršom	k1gInSc1	Geršom
(	(	kIx(	(
<g/>
1288	[number]	k4	1288
<g/>
–	–	k?	–
<g/>
1344	[number]	k4	1344
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
řecky	řecky	k6eAd1	řecky
Gersonides	Gersonides	k1gMnSc1	Gersonides
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
také	také	k9	také
pod	pod	k7c7	pod
akronymem	akronymum	k1gNnSc7	akronymum
Ralbag	Ralbaga	k1gFnPc2	Ralbaga
(	(	kIx(	(
<g/>
ר	ר	k?	ר
<g/>
״	״	k?	״
<g/>
ג	ג	k?	ג
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
židovský	židovský	k2eAgMnSc1d1	židovský
rabín	rabín	k1gMnSc1	rabín
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
<g/>
,	,	kIx,	,
astronom	astronom	k1gMnSc1	astronom
a	a	k8xC	a
talmudista	talmudista	k1gMnSc1	talmudista
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
Bagnols-sur-Cè	Bagnolsur-Cè	k1gFnSc2	Bagnols-sur-Cè
(	(	kIx(	(
<g/>
Languedoc	Languedoc	k1gFnSc1	Languedoc
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
významným	významný	k2eAgMnSc7d1	významný
kritikem	kritik	k1gMnSc7	kritik
některých	některý	k3yIgInPc2	některý
aspektů	aspekt	k1gInPc2	aspekt
maimonidovské	maimonidovský	k2eAgFnSc2d1	maimonidovský
syntézy	syntéza	k1gFnSc2	syntéza
aristotelismu	aristotelismus	k1gInSc2	aristotelismus
a	a	k8xC	a
židovského	židovský	k2eAgNnSc2d1	Židovské
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
výrazně	výrazně	k6eAd1	výrazně
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
komentáři	komentář	k1gInSc3	komentář
ibn	ibn	k?	ibn
Rušda	Rušd	k1gMnSc2	Rušd
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
dílem	díl	k1gInSc7	díl
je	být	k5eAaImIp3nS	být
filosofický	filosofický	k2eAgInSc4d1	filosofický
spis	spis	k1gInSc4	spis
Hospodinovy	Hospodinův	k2eAgInPc1d1	Hospodinův
boje	boj	k1gInPc1	boj
(	(	kIx(	(
<g/>
Milchamot	Milchamot	k1gInSc1	Milchamot
ha-Šem	ha-Šem	k1gInSc1	ha-Šem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Levi	Lev	k1gFnSc2	Lev
ben	ben	k?	ben
Geršom	Geršom	k1gInSc1	Geršom
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
