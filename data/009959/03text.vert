<p>
<s>
Pohoda	pohoda	k1gFnSc1	pohoda
je	být	k5eAaImIp3nS	být
hudební	hudební	k2eAgInSc4d1	hudební
festival	festival	k1gInSc4	festival
konající	konající	k2eAgInSc4d1	konající
se	se	k3xPyFc4	se
v	v	k7c6	v
Trenčíně	Trenčín	k1gInSc6	Trenčín
obyčejně	obyčejně	k6eAd1	obyčejně
koncem	koncem	k7c2	koncem
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
největší	veliký	k2eAgInSc1d3	veliký
hudební	hudební	k2eAgInSc1d1	hudební
festival	festival	k1gInSc1	festival
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Festival	festival	k1gInSc1	festival
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
místem	místo	k1gNnSc7	místo
konání	konání	k1gNnSc2	konání
byl	být	k5eAaImAgInS	být
Městský	městský	k2eAgInSc1d1	městský
stadión	stadión	k1gInSc1	stadión
Trenčín	Trenčín	k1gInSc1	Trenčín
<g/>
.	.	kIx.	.
</s>
<s>
Původním	původní	k2eAgInSc7d1	původní
záměrem	záměr	k1gInSc7	záměr
bylo	být	k5eAaImAgNnS	být
uspořádat	uspořádat	k5eAaPmF	uspořádat
malý	malý	k2eAgInSc4d1	malý
pohodový	pohodový	k2eAgInSc4d1	pohodový
festival	festival	k1gInSc4	festival
jako	jako	k8xS	jako
protiváha	protiváha	k1gFnSc1	protiváha
soudobým	soudobý	k2eAgInPc3d1	soudobý
velkým	velký	k2eAgInPc3d1	velký
koncertům	koncert	k1gInPc3	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgNnSc3	ten
odpovídala	odpovídat	k5eAaImAgFnS	odpovídat
také	také	k9	také
návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
–	–	k?	–
prvního	první	k4xOgInSc2	první
ročníku	ročník	k1gInSc2	ročník
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
zhruba	zhruba	k6eAd1	zhruba
2	[number]	k4	2
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
návštěvníků	návštěvník	k1gMnPc2	návštěvník
ale	ale	k8xC	ale
rok	rok	k1gInSc4	rok
od	od	k7c2	od
roku	rok	k1gInSc2	rok
rostl	růst	k5eAaImAgInS	růst
<g/>
,	,	kIx,	,
a	a	k8xC	a
zatímco	zatímco	k8xS	zatímco
další	další	k2eAgInSc1d1	další
rok	rok	k1gInSc1	rok
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
jen	jen	k9	jen
mírný	mírný	k2eAgInSc1d1	mírný
nárůst	nárůst	k1gInSc1	nárůst
na	na	k7c4	na
3	[number]	k4	3
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
denní	denní	k2eAgFnSc1d1	denní
návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
25	[number]	k4	25
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ročníky	ročník	k1gInPc1	ročník
1998	[number]	k4	1998
až	až	k8xS	až
2003	[number]	k4	2003
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgInP	konat
v	v	k7c6	v
Areálu	areál	k1gInSc6	areál
Pod	pod	k7c7	pod
Sokolicami	Sokolica	k1gFnPc7	Sokolica
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2004	[number]	k4	2004
a	a	k8xC	a
2005	[number]	k4	2005
se	se	k3xPyFc4	se
festival	festival	k1gInSc1	festival
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
na	na	k7c6	na
Vojenském	vojenský	k2eAgNnSc6d1	vojenské
letišti	letiště	k1gNnSc6	letiště
Trenčín	Trenčín	k1gInSc1	Trenčín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
festival	festival	k1gInSc4	festival
přinesl	přinést	k5eAaPmAgInS	přinést
na	na	k7c4	na
Slovensko	Slovensko	k1gNnSc4	Slovensko
skupiny	skupina	k1gFnSc2	skupina
jako	jako	k8xC	jako
například	například	k6eAd1	například
Prodigy	Prodig	k1gInPc4	Prodig
<g/>
,	,	kIx,	,
Garbage	Garbag	k1gInPc4	Garbag
<g/>
,	,	kIx,	,
Stereo	stereo	k6eAd1	stereo
MC	MC	kA	MC
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
Cardigans	Cardigans	k1gInSc1	Cardigans
<g/>
,	,	kIx,	,
Moloko	Moloko	k1gNnSc1	Moloko
<g/>
,	,	kIx,	,
Cesaria	Cesarium	k1gNnSc2	Cesarium
Evora	Evoro	k1gNnSc2	Evoro
nebo	nebo	k8xC	nebo
Fun	Fun	k1gMnSc1	Fun
Lovin	Lovin	k1gMnSc1	Lovin
<g/>
'	'	kIx"	'
Criminals	Criminals	k1gInSc1	Criminals
<g/>
.	.	kIx.	.
</s>
<s>
Festival	festival	k1gInSc1	festival
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
na	na	k7c4	na
world	world	k1gInSc4	world
music	musice	k1gFnPc2	musice
a	a	k8xC	a
alternativní	alternativní	k2eAgFnSc2d1	alternativní
hudební	hudební	k2eAgFnSc2d1	hudební
formy	forma	k1gFnSc2	forma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
silnému	silný	k2eAgInSc3d1	silný
větru	vítr	k1gInSc3	vítr
zhroutil	zhroutit	k5eAaPmAgInS	zhroutit
stan	stan	k1gInSc1	stan
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
člověka	člověk	k1gMnSc4	člověk
zabil	zabít	k5eAaPmAgInS	zabít
a	a	k8xC	a
25	[number]	k4	25
dalších	další	k1gNnPc6	další
zranil	zranit	k5eAaPmAgMnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Festival	festival	k1gInSc1	festival
byl	být	k5eAaImAgInS	být
následně	následně	k6eAd1	následně
předčasně	předčasně	k6eAd1	předčasně
ukončen	ukončit	k5eAaPmNgInS	ukončit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
festivalu	festival	k1gInSc2	festival
</s>
</p>
