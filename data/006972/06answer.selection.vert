<s>
Polonium	polonium	k1gNnSc1	polonium
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Po	Po	kA	Po
<g/>
,	,	kIx,	,
lat.	lat.	k?	lat.
Polonium	polonium	k1gNnSc1	polonium
je	být	k5eAaImIp3nS	být
nestabilní	stabilní	k2eNgInSc4d1	nestabilní
radioaktivní	radioaktivní	k2eAgInSc4d1	radioaktivní
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
nejtěžší	těžký	k2eAgFnSc4d3	nejtěžší
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
chalkogenů	chalkogen	k1gInPc2	chalkogen
<g/>
.	.	kIx.	.
</s>
