<s>
První	první	k4xOgFnSc1
vláda	vláda	k1gFnSc1
Jozefa	Jozef	k1gMnSc2
Tisa	Tisa	k1gFnSc1
</s>
<s>
Předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
Jozef	Jozef	k1gMnSc1
Tiso	Tisa	k1gFnSc5
</s>
<s>
První	první	k4xOgFnSc1
vláda	vláda	k1gFnSc1
Jozefa	Jozef	k1gMnSc2
Tisa	Tis0	k1gMnSc2
existovala	existovat	k5eAaImAgFnS
v	v	k7c6
období	období	k1gNnSc6
od	od	k7c2
7	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
do	do	k7c2
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1938	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
první	první	k4xOgFnSc4
slovenskou	slovenský	k2eAgFnSc4d1
autonomní	autonomní	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
v	v	k7c6
rámci	rámec	k1gInSc6
druhé	druhý	k4xOgFnSc2
československé	československý	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Složení	složení	k1gNnSc1
autonomní	autonomní	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
</s>
<s>
Všichni	všechen	k3xTgMnPc1
členové	člen	k1gMnPc1
vlády	vláda	k1gFnSc2
byli	být	k5eAaImAgMnP
zástupci	zástupce	k1gMnPc1
Hlinkovy	Hlinkův	k2eAgFnSc2d1
slovenské	slovenský	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
strany	strana	k1gFnSc2
-	-	kIx~
Strany	strana	k1gFnSc2
slovenské	slovenský	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
jednoty	jednota	k1gFnSc2
(	(	kIx(
<g/>
HSĽS-SSNJ	HSĽS-SSNJ	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
PortfejMinistrNástup	PortfejMinistrNástup	k1gInSc1
do	do	k7c2
úřaduOdchod	úřaduOdchod	k1gInSc1
z	z	k7c2
úřadu	úřad	k1gInSc2
</s>
<s>
Předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
a	a	k8xC
ministr	ministr	k1gMnSc1
vnitra	vnitro	k1gNnSc2
</s>
<s>
Jozef	Jozef	k1gMnSc1
Tiso	Tisa	k1gFnSc5
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1938	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1938	#num#	k4
</s>
<s>
Ministr	ministr	k1gMnSc1
zemědělství	zemědělství	k1gNnSc2
<g/>
,	,	kIx,
obchodu	obchod	k1gInSc2
<g/>
,	,	kIx,
veřejných	veřejný	k2eAgFnPc2d1
prací	práce	k1gFnPc2
a	a	k8xC
financí	finance	k1gFnPc2
</s>
<s>
Pavol	Pavol	k1gInSc1
Teplanský	Teplanský	k2eAgInSc1d1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1938	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1938	#num#	k4
</s>
<s>
Ministr	ministr	k1gMnSc1
spravedlnosti	spravedlnost	k1gFnSc2
<g/>
,	,	kIx,
sociální	sociální	k2eAgFnSc2d1
péče	péče	k1gFnSc2
a	a	k8xC
zdravotnictví	zdravotnictví	k1gNnSc2
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
Ďurčanský	Ďurčanský	k2eAgMnSc1d1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1938	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1938	#num#	k4
</s>
<s>
Ministr	ministr	k1gMnSc1
pošt	pošta	k1gFnPc2
a	a	k8xC
železnic	železnice	k1gFnPc2
</s>
<s>
Ján	Ján	k1gMnSc1
Lichner	Lichner	k1gMnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1938	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1938	#num#	k4
</s>
<s>
Ministr	ministr	k1gMnSc1
školství	školství	k1gNnSc2
</s>
<s>
Matúš	Matúš	k1gMnSc1
Černák	Černák	k1gMnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1938	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1938	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Žilinská	žilinský	k2eAgFnSc1d1
dohoda	dohoda	k1gFnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
Autonomní	autonomní	k2eAgFnPc1d1
vlády	vláda	k1gFnPc1
Slovenska	Slovensko	k1gNnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Vlády	vláda	k1gFnPc1
Slovenské	slovenský	k2eAgFnSc2d1
krajiny	krajina	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Vlády	vláda	k1gFnPc1
Slovenska	Slovensko	k1gNnSc2
Slovenská	slovenský	k2eAgFnSc1d1
země	země	k1gFnSc1
(	(	kIx(
<g/>
1938	#num#	k4
<g/>
–	–	k?
<g/>
1939	#num#	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
v	v	k7c6
rámci	rámec	k1gInSc6
druhé	druhý	k4xOgFnSc2
republiky	republika	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
1938	#num#	k4
Jozef	Jozef	k1gMnSc1
Tiso	Tisa	k1gFnSc5
I	i	k9
•	•	k?
1938	#num#	k4
<g/>
–	–	k?
<g/>
1939	#num#	k4
Jozef	Jozef	k1gInSc1
Tiso	Tisa	k1gFnSc5
II	II	kA
•	•	k?
1939	#num#	k4
Jozef	Jozef	k1gInSc1
Tiso	Tisa	k1gFnSc5
III	III	kA
•	•	k?
1939	#num#	k4
Jozef	Jozef	k1gMnSc1
Sivák	Sivák	k1gMnSc1
•	•	k?
1939	#num#	k4
Karol	Karol	k1gInSc1
Sidor	Sidor	k1gInSc4
první	první	k4xOgFnSc1
Slovenská	slovenský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
1939	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1939	#num#	k4
Jozef	Jozef	k1gMnSc1
Tiso	Tisa	k1gFnSc5
IV	IV	kA
•	•	k?
1939	#num#	k4
<g/>
–	–	k?
<g/>
1944	#num#	k4
Vojtech	Vojta	k1gMnPc6
Tuka	Tuk	k1gInSc2
•	•	k?
1944	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
Štefan	Štefan	k1gMnSc1
Tiso	Tisa	k1gFnSc5
Slovenská	slovenský	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
v	v	k7c6
rámci	rámec	k1gInSc6
ČSSR	ČSSR	kA
<g/>
)	)	kIx)
</s>
<s>
1969	#num#	k4
<g/>
–	–	k?
<g/>
1971	#num#	k4
Štefan	Štefan	k1gMnSc1
Sádovský	Sádovský	k1gMnSc1
a	a	k8xC
Peter	Peter	k1gMnSc1
Colotka	Colotka	k1gFnSc1
•	•	k?
1976	#num#	k4
<g/>
–	–	k?
<g/>
1981	#num#	k4
Peter	Petra	k1gFnPc2
Colotka	Colotka	k1gFnSc1
I	i	k8xC
•	•	k?
1976	#num#	k4
<g/>
–	–	k?
<g/>
1981	#num#	k4
Peter	Peter	k1gMnSc1
Colotka	Colotka	k1gFnSc1
II	II	kA
•	•	k?
1981	#num#	k4
<g/>
–	–	k?
<g/>
1986	#num#	k4
Peter	Peter	k1gMnSc1
Colotka	Colotka	k1gFnSc1
III	III	kA
•	•	k?
1986	#num#	k4
<g/>
–	–	k?
<g/>
1989	#num#	k4
Peter	Petra	k1gFnPc2
Colotka	Colotka	k1gFnSc1
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
Knotek	Knotek	k1gMnSc1
a	a	k8xC
Pavol	Pavol	k1gInSc1
Hrivnák	Hrivnák	k1gInSc1
•	•	k?
1989	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
Milan	Milana	k1gFnPc2
Čič	Čič	k1gFnPc2
Slovenská	slovenský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
v	v	k7c6
rámci	rámec	k1gInSc6
ČSFR	ČSFR	kA
<g/>
)	)	kIx)
</s>
<s>
1989	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
Milan	Milan	k1gMnSc1
Čič	Čič	k1gMnSc1
•	•	k?
1990	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
Vladimír	Vladimír	k1gMnSc1
Mečiar	Mečiar	k1gMnSc1
I	i	k8xC
•	•	k?
1991	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
Ján	Ján	k1gMnSc1
Čarnogurský	Čarnogurský	k2eAgMnSc1d1
•	•	k?
1992	#num#	k4
<g/>
–	–	k?
<g/>
1994	#num#	k4
Vladimír	Vladimír	k1gMnSc1
Mečiar	Mečiar	k1gMnSc1
II	II	kA
Slovenská	slovenský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
od	od	k7c2
1993	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1992	#num#	k4
<g/>
–	–	k?
<g/>
1994	#num#	k4
Vladimír	Vladimír	k1gMnSc1
Mečiar	Mečiar	k1gMnSc1
II	II	kA
•	•	k?
1994	#num#	k4
Jozef	Jozef	k1gInSc1
Moravčík	Moravčík	k1gInSc4
•	•	k?
1994	#num#	k4
<g/>
–	–	k?
<g/>
1998	#num#	k4
Vladimír	Vladimír	k1gMnSc1
Mečiar	Mečiar	k1gMnSc1
III	III	kA
•	•	k?
1998	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
Mikuláš	mikuláš	k1gInSc1
Dzurinda	Dzurind	k1gMnSc2
I	I	kA
•	•	k?
2002	#num#	k4
<g/>
–	–	k?
<g/>
2006	#num#	k4
Mikuláš	mikuláš	k1gInSc1
Dzurinda	Dzurind	k1gMnSc2
II	II	kA
•	•	k?
2006	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
Robert	Robert	k1gMnSc1
Fico	Fico	k1gMnSc1
I	i	k8xC
•	•	k?
2010	#num#	k4
<g/>
–	–	k?
<g/>
2012	#num#	k4
Iveta	Iveta	k1gFnSc1
Radičová	Radičová	k1gFnSc1
•	•	k?
2012	#num#	k4
<g/>
–	–	k?
<g/>
2016	#num#	k4
Robert	Robert	k1gMnSc1
Fico	Fico	k1gMnSc1
II	II	kA
•	•	k?
2016	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
Robert	Robert	k1gMnSc1
Fico	Fico	k1gMnSc1
III	III	kA
•	•	k?
2018	#num#	k4
<g/>
–	–	k?
<g/>
2020	#num#	k4
Peter	Peter	k1gMnSc1
Pellegrini	Pellegrin	k1gMnPc1
•	•	k?
2020	#num#	k4
<g/>
–	–	k?
<g/>
2021	#num#	k4
Igor	Igor	k1gMnSc1
Matovič	Matovič	k1gMnSc1
•	•	k?
od	od	k7c2
2021	#num#	k4
Eduard	Eduard	k1gMnSc1
Heger	Heger	k1gMnSc1
</s>
<s>
Vlády	vláda	k1gFnPc1
Československa	Československo	k1gNnSc2
a	a	k8xC
vlády	vláda	k1gFnSc2
na	na	k7c6
jeho	jeho	k3xOp3gNnSc6
území	území	k1gNnSc6
1938	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
České	český	k2eAgFnSc2d1
země	zem	k1gFnSc2
</s>
<s>
Druhá	druhý	k4xOgFnSc1
republika	republika	k1gFnSc1
</s>
<s>
1938	#num#	k4
Jan	Jan	k1gMnSc1
Syrový	syrový	k2eAgInSc1d1
II	II	kA
•	•	k?
1938	#num#	k4
<g/>
–	–	k?
<g/>
39	#num#	k4
Rudolf	Rudolf	k1gMnSc1
Beran	Beran	k1gMnSc1
I	i	k9
Protektorát	protektorát	k1gInSc4
</s>
<s>
1939	#num#	k4
Rudolf	Rudolf	k1gMnSc1
Beran	Beran	k1gMnSc1
II	II	kA
•	•	k?
1939	#num#	k4
<g/>
–	–	k?
<g/>
42	#num#	k4
Alois	Alois	k1gMnSc1
Eliáš	Eliáš	k1gMnSc1
•	•	k?
1942	#num#	k4
<g/>
–	–	k?
<g/>
45	#num#	k4
Jaroslav	Jaroslav	k1gMnSc1
Krejčí	Krejčí	k1gMnSc1
•	•	k?
1945	#num#	k4
Richard	Richard	k1gMnSc1
Bienert	Bienert	k1gMnSc1
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
republika	republika	k1gFnSc1
</s>
<s>
1938	#num#	k4
Jozef	Jozef	k1gMnSc1
Tiso	Tisa	k1gFnSc5
I	i	k9
•	•	k?
1938	#num#	k4
<g/>
–	–	k?
<g/>
39	#num#	k4
Jozef	Jozef	k1gInSc1
Tiso	Tisa	k1gFnSc5
II	II	kA
•	•	k?
1939	#num#	k4
Jozef	Jozef	k1gInSc1
Tiso	Tisa	k1gFnSc5
III	III	kA
•	•	k?
1939	#num#	k4
Jozef	Jozef	k1gMnSc1
Sivák	Sivák	k1gMnSc1
•	•	k?
1939	#num#	k4
Karol	Karol	k1gInSc1
Sidor	Sidor	k1gInSc4
První	první	k4xOgFnSc1
Slovenská	slovenský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
1939	#num#	k4
Jozef	Jozef	k1gMnSc1
Tiso	Tisa	k1gFnSc5
IV	IV	kA
•	•	k?
1939	#num#	k4
<g/>
–	–	k?
<g/>
44	#num#	k4
Vojtech	Vojta	k1gMnPc6
Tuka	Tuk	k1gInSc2
•	•	k?
1944	#num#	k4
<g/>
–	–	k?
<g/>
45	#num#	k4
Štefan	Štefan	k1gMnSc1
Tiso	Tisa	k1gFnSc5
</s>
<s>
Podkarpatská	podkarpatský	k2eAgFnSc1d1
Rus	Rus	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
republika	republika	k1gFnSc1
</s>
<s>
1938	#num#	k4
Andrej	Andrej	k1gMnSc1
Bródy	Bróda	k1gFnSc2
•	•	k?
1938	#num#	k4
Augustin	Augustin	k1gMnSc1
Vološin	Vološina	k1gFnPc2
I	i	k9
•	•	k?
1938	#num#	k4
<g/>
–	–	k?
<g/>
39	#num#	k4
Augustin	Augustin	k1gMnSc1
Vološin	Vološina	k1gFnPc2
II	II	kA
•	•	k?
1939	#num#	k4
Augustin	Augustin	k1gMnSc1
Vološin	Vološina	k1gFnPc2
III	III	kA
Karpatská	karpatský	k2eAgFnSc1d1
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
1939	#num#	k4
Július	Július	k1gInSc1
Révay	Révaa	k1gFnSc2
</s>
<s>
exilové	exilový	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
</s>
<s>
Londýn	Londýn	k1gInSc1
</s>
<s>
1940	#num#	k4
<g/>
–	–	k?
<g/>
42	#num#	k4
Jan	Jan	k1gMnSc1
Šrámek	Šrámek	k1gMnSc1
I	i	k8xC
•	•	k?
1942	#num#	k4
<g/>
–	–	k?
<g/>
45	#num#	k4
Jan	Jan	k1gMnSc1
Šrámek	Šrámek	k1gMnSc1
II	II	kA
Moskva	Moskva	k1gFnSc1
</s>
<s>
1945	#num#	k4
Zdeněk	Zdeněk	k1gMnSc1
Fierlinger	Fierlinger	k1gMnSc1
I	i	k9
</s>
