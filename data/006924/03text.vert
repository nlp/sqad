<s>
Igor	Igor	k1gMnSc1	Igor
Valerjevič	Valerjevič	k1gMnSc1	Valerjevič
Andrejev	Andrejev	k1gMnSc1	Andrejev
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
И	И	k?	И
В	В	k?	В
А	А	k?	А
<g/>
,	,	kIx,	,
*	*	kIx~	*
14	[number]	k4	14
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Ruská	ruský	k2eAgFnSc1d1	ruská
federace	federace	k1gFnSc1	federace
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
současný	současný	k2eAgMnSc1d1	současný
ruský	ruský	k2eAgMnSc1d1	ruský
profesionální	profesionální	k2eAgMnSc1d1	profesionální
tenista	tenista	k1gMnSc1	tenista
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
zatím	zatím	k6eAd1	zatím
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
3	[number]	k4	3
turnaje	turnaj	k1gInSc2	turnaj
ATP	atp	kA	atp
World	World	k1gMnSc1	World
Tour	Tour	k1gMnSc1	Tour
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
a	a	k8xC	a
1	[number]	k4	1
turnaj	turnaj	k1gInSc1	turnaj
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
Igor	Igor	k1gMnSc1	Igor
Andrejev	Andrejev	k1gMnSc1	Andrejev
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
12	[number]	k4	12
zápasů	zápas	k1gInPc2	zápas
v	v	k7c6	v
Davisově	Davisův	k2eAgInSc6d1	Davisův
poháru	pohár	k1gInSc6	pohár
za	za	k7c4	za
tým	tým	k1gInSc4	tým
Ruska	Rusko	k1gNnSc2	Rusko
s	s	k7c7	s
bilancí	bilance	k1gFnSc7	bilance
10-7	[number]	k4	10-7
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
a	a	k8xC	a
3-4	[number]	k4	3-4
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
.	.	kIx.	.
www.atpworldtour.com	www.atpworldtour.com	k1gInSc4	www.atpworldtour.com
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Igor	Igor	k1gMnSc1	Igor
Andrejev	Andrejev	k1gMnSc1	Andrejev
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Igor	Igor	k1gMnSc1	Igor
Andrejev	Andrejev	k1gMnSc1	Andrejev
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
ATP	atp	kA	atp
Tour	Tour	k1gInSc4	Tour
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Igor	Igor	k1gMnSc1	Igor
Andrejev	Andrejev	k1gMnSc1	Andrejev
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
tenisové	tenisový	k2eAgFnSc2d1	tenisová
federace	federace	k1gFnSc2	federace
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Igor	Igor	k1gMnSc1	Igor
Andrejev	Andrejev	k1gMnSc1	Andrejev
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Davis	Davis	k1gFnSc2	Davis
Cupu	cup	k1gInSc2	cup
</s>
