<s>
Fjord	fjord	k1gInSc1	fjord
je	být	k5eAaImIp3nS	být
pozůstatek	pozůstatek	k1gInSc4	pozůstatek
po	po	k7c6	po
ledovcové	ledovcový	k2eAgFnSc6d1	ledovcová
činnosti	činnost	k1gFnSc6	činnost
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
ledových	ledový	k2eAgFnPc6d1	ledová
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
(	(	kIx(	(
<g/>
občas	občas	k6eAd1	občas
desítky	desítka	k1gFnPc4	desítka
až	až	k8xS	až
stovky	stovka	k1gFnPc4	stovka
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
)	)	kIx)	)
a	a	k8xC	a
úzký	úzký	k2eAgInSc1d1	úzký
mořský	mořský	k2eAgInSc1d1	mořský
záliv	záliv	k1gInSc1	záliv
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
hornatých	hornatý	k2eAgFnPc6d1	hornatá
oblastech	oblast	k1gFnPc6	oblast
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
moří	moře	k1gNnPc2	moře
a	a	k8xC	a
oceánů	oceán	k1gInPc2	oceán
prohloubením	prohloubení	k1gNnSc7	prohloubení
říčních	říční	k2eAgNnPc2d1	říční
údolí	údolí	k1gNnPc2	údolí
vlivem	vlivem	k7c2	vlivem
tlaků	tlak	k1gInPc2	tlak
postupujících	postupující	k2eAgInPc2d1	postupující
ledovců	ledovec	k1gInPc2	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Fjordy	fjord	k1gInPc1	fjord
jsou	být	k5eAaImIp3nP	být
typické	typický	k2eAgInPc1d1	typický
svými	svůj	k3xOyFgInPc7	svůj
strmými	strmý	k2eAgInPc7d1	strmý
svahy	svah	k1gInPc7	svah
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
ledovci	ledovec	k1gInSc3	ledovec
často	často	k6eAd1	často
erodovány	erodovat	k5eAaImNgInP	erodovat
až	až	k9	až
pod	pod	k7c4	pod
mořskou	mořský	k2eAgFnSc4d1	mořská
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dno	dno	k1gNnSc1	dno
moře	moře	k1gNnSc2	moře
může	moct	k5eAaImIp3nS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
značných	značný	k2eAgFnPc2d1	značná
hloubek	hloubka	k1gFnPc2	hloubka
netypických	typický	k2eNgFnPc2d1	netypická
pro	pro	k7c4	pro
šelfové	šelfový	k2eAgFnPc4d1	šelfová
oblasti	oblast	k1gFnPc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
údolí	údolí	k1gNnSc2	údolí
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k8xS	jako
U.	U.	kA	U.
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
fjordy	fjord	k1gInPc7	fjord
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
ohraničeny	ohraničit	k5eAaPmNgInP	ohraničit
oproti	oproti	k7c3	oproti
moři	moře	k1gNnSc3	moře
podmořským	podmořský	k2eAgInSc7d1	podmořský
skalním	skalní	k2eAgInSc7d1	skalní
svahem	svah	k1gInSc7	svah
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
staronorského	staronorský	k2eAgNnSc2d1	staronorský
slova	slovo	k1gNnSc2	slovo
fjörð	fjörð	k?	fjörð
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
přepisovalo	přepisovat	k5eAaImAgNnS	přepisovat
jako	jako	k9	jako
fjorthr	fjorthr	k1gInSc4	fjorthr
a	a	k8xC	a
z	z	k7c2	z
výslovnosti	výslovnost	k1gFnSc2	výslovnost
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
utvořilo	utvořit	k5eAaPmAgNnS	utvořit
světové	světový	k2eAgNnSc1d1	světové
slovo	slovo	k1gNnSc1	slovo
fjord	fjord	k1gInSc1	fjord
<g/>
.	.	kIx.	.
</s>
<s>
Fjordy	fjord	k1gInPc1	fjord
vznikají	vznikat	k5eAaImIp3nP	vznikat
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
glaciálního	glaciální	k2eAgNnSc2d1	glaciální
zalednění	zalednění	k1gNnSc2	zalednění
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
postupující	postupující	k2eAgInSc1d1	postupující
ledovec	ledovec	k1gInSc1	ledovec
sestupuje	sestupovat	k5eAaImIp3nS	sestupovat
z	z	k7c2	z
karových	karový	k2eAgFnPc2d1	Karová
oblastí	oblast	k1gFnPc2	oblast
po	po	k7c6	po
svahu	svah	k1gInSc6	svah
do	do	k7c2	do
oblastí	oblast	k1gFnPc2	oblast
položených	položený	k2eAgFnPc6d1	položená
níže	nízce	k6eAd2	nízce
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
cesty	cesta	k1gFnSc2	cesta
se	se	k3xPyFc4	se
led	led	k1gInSc1	led
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
lokálních	lokální	k2eAgFnPc6d1	lokální
sníženinách	sníženina	k1gFnPc6	sníženina
(	(	kIx(	(
<g/>
depresích	deprese	k1gFnPc6	deprese
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vlivem	vlivem	k7c2	vlivem
brázdění	brázdění	k1gNnSc2	brázdění
prohlubuje	prohlubovat	k5eAaImIp3nS	prohlubovat
a	a	k8xC	a
zaobluje	zaoblovat	k5eAaImIp3nS	zaoblovat
podloží	podloží	k1gNnSc1	podloží
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
tak	tak	k9	tak
typické	typický	k2eAgNnSc1d1	typické
ledovcové	ledovcový	k2eAgNnSc1d1	ledovcové
údolí	údolí	k1gNnSc1	údolí
zvané	zvaný	k2eAgFnSc2d1	zvaná
trog	trog	k1gInSc4	trog
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
cesty	cesta	k1gFnSc2	cesta
se	se	k3xPyFc4	se
ledovec	ledovec	k1gInSc1	ledovec
dostane	dostat	k5eAaPmIp3nS	dostat
až	až	k9	až
k	k	k7c3	k
moři	moře	k1gNnSc3	moře
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
nasouvat	nasouvat	k5eAaImF	nasouvat
<g/>
,	,	kIx,	,
vlivem	vliv	k1gInSc7	vliv
rozdílné	rozdílný	k2eAgFnSc2d1	rozdílná
teploty	teplota	k1gFnSc2	teplota
<g/>
,	,	kIx,	,
viskozity	viskozita	k1gFnSc2	viskozita
a	a	k8xC	a
hloubky	hloubka	k1gFnSc2	hloubka
se	se	k3xPyFc4	se
však	však	k9	však
začínají	začínat	k5eAaImIp3nP	začínat
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
ledové	ledový	k2eAgFnPc4d1	ledová
části	část	k1gFnPc4	část
odlamovat	odlamovat	k5eAaImF	odlamovat
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
telení	telení	k1gNnSc1	telení
ledovců	ledovec	k1gInPc2	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
ledové	ledový	k2eAgFnSc2d1	ledová
doby	doba	k1gFnSc2	doba
začne	začít	k5eAaPmIp3nS	začít
ledový	ledový	k2eAgInSc4d1	ledový
krunýř	krunýř	k1gInSc4	krunýř
vlivem	vlivem	k7c2	vlivem
odtávání	odtávání	k1gNnSc2	odtávání
pozvolna	pozvolna	k6eAd1	pozvolna
ustupovat	ustupovat	k5eAaImF	ustupovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
určité	určitý	k2eAgFnSc6d1	určitá
době	doba	k1gFnSc6	doba
zůstane	zůstat	k5eAaPmIp3nS	zůstat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
pouze	pouze	k6eAd1	pouze
typické	typický	k2eAgNnSc1d1	typické
ledovcové	ledovcový	k2eAgNnSc1d1	ledovcové
údolí	údolí	k1gNnSc1	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
roztání	roztání	k1gNnSc2	roztání
většího	veliký	k2eAgNnSc2d2	veliký
množství	množství	k1gNnSc2	množství
ledovcové	ledovcový	k2eAgFnSc2d1	ledovcová
pokrývky	pokrývka	k1gFnSc2	pokrývka
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
zvednutí	zvednutí	k1gNnSc3	zvednutí
světové	světový	k2eAgFnSc2d1	světová
hladiny	hladina	k1gFnSc2	hladina
oceánů	oceán	k1gInPc2	oceán
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
částečnému	částečný	k2eAgNnSc3d1	částečné
zatopení	zatopení	k1gNnSc3	zatopení
přímořských	přímořský	k2eAgNnPc2d1	přímořské
údolí	údolí	k1gNnPc2	údolí
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
přidává	přidávat	k5eAaImIp3nS	přidávat
ještě	ještě	k9	ještě
odlehčení	odlehčení	k1gNnSc4	odlehčení
litosférické	litosférický	k2eAgFnSc2d1	litosférická
desky	deska	k1gFnSc2	deska
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
již	již	k6eAd1	již
netlačí	tlačit	k5eNaImIp3nS	tlačit
ohromná	ohromný	k2eAgFnSc1d1	ohromná
tíha	tíha	k1gFnSc1	tíha
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
určité	určitý	k2eAgFnPc1d1	určitá
části	část	k1gFnPc1	část
desky	deska	k1gFnSc2	deska
začnou	začít	k5eAaPmIp3nP	začít
ponořovat	ponořovat	k5eAaImF	ponořovat
do	do	k7c2	do
astenosféry	astenosféra	k1gFnSc2	astenosféra
a	a	k8xC	a
další	další	k2eAgFnSc1d1	další
část	část	k1gFnSc1	část
desky	deska	k1gFnSc2	deska
se	se	k3xPyFc4	se
nadlehčuje	nadlehčovat	k5eAaImIp3nS	nadlehčovat
a	a	k8xC	a
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgInPc1d1	podobný
procesy	proces	k1gInPc1	proces
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
dějí	dít	k5eAaImIp3nP	dít
ve	v	k7c6	v
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
poloostrov	poloostrov	k1gInSc1	poloostrov
vyzvedává	vyzvedávat	k5eAaImIp3nS	vyzvedávat
nad	nad	k7c4	nad
hladinu	hladina	k1gFnSc4	hladina
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnPc4	jeho
přímořské	přímořský	k2eAgFnPc4d1	přímořská
oblasti	oblast	k1gFnPc4	oblast
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Atlantiku	Atlantik	k1gInSc2	Atlantik
se	se	k3xPyFc4	se
oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
pozvolna	pozvolna	k6eAd1	pozvolna
ponořují	ponořovat	k5eAaImIp3nP	ponořovat
pod	pod	k7c4	pod
hladinu	hladina	k1gFnSc4	hladina
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
má	mít	k5eAaImIp3nS	mít
(	(	kIx(	(
<g/>
a	a	k8xC	a
mělo	mít	k5eAaImAgNnS	mít
<g/>
)	)	kIx)	)
za	za	k7c4	za
následek	následek	k1gInSc4	následek
vznik	vznik	k1gInSc4	vznik
fjordů	fjord	k1gInPc2	fjord
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Norska	Norsko	k1gNnSc2	Norsko
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
fjordů	fjord	k1gInPc2	fjord
je	být	k5eAaImIp3nS	být
tedy	tedy	k8xC	tedy
spojen	spojit	k5eAaPmNgInS	spojit
se	s	k7c7	s
změnou	změna	k1gFnSc7	změna
polohy	poloha	k1gFnSc2	poloha
litosférických	litosférický	k2eAgFnPc2d1	litosférická
desek	deska	k1gFnPc2	deska
a	a	k8xC	a
změnou	změna	k1gFnSc7	změna
hladiny	hladina	k1gFnSc2	hladina
světového	světový	k2eAgInSc2d1	světový
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Fjordy	fjord	k1gInPc1	fjord
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
glaciálních	glaciální	k2eAgFnPc6d1	glaciální
dobách	doba	k1gFnPc6	doba
nacházel	nacházet	k5eAaImAgInS	nacházet
kontinentální	kontinentální	k2eAgInSc1d1	kontinentální
ledovec	ledovec	k1gInSc1	ledovec
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
blízko	blízko	k7c2	blízko
mořské	mořský	k2eAgFnSc2d1	mořská
hladiny	hladina	k1gFnSc2	hladina
a	a	k8xC	a
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgNnPc6	který
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgInSc1d2	veliký
výškový	výškový	k2eAgInSc1d1	výškový
rozdíl	rozdíl	k1gInSc1	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
oblastí	oblast	k1gFnSc7	oblast
světa	svět	k1gInSc2	svět
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
pobřeží	pobřeží	k1gNnSc1	pobřeží
Norska	Norsko	k1gNnSc2	Norsko
(	(	kIx(	(
<g/>
vyhlášené	vyhlášený	k2eAgFnPc1d1	vyhlášená
rybáři	rybář	k1gMnPc1	rybář
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
lokalitou	lokalita	k1gFnSc7	lokalita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
fjordy	fjord	k1gInPc1	fjord
lze	lze	k6eAd1	lze
objevit	objevit	k5eAaPmF	objevit
na	na	k7c6	na
území	území	k1gNnSc6	území
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
Dánska	Dánsko	k1gNnSc2	Dánsko
a	a	k8xC	a
několik	několik	k4yIc1	několik
jich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
spatřit	spatřit	k5eAaPmF	spatřit
i	i	k9	i
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
(	(	kIx(	(
<g/>
hrabství	hrabství	k1gNnSc6	hrabství
Mayo	Mayo	k6eAd1	Mayo
u	u	k7c2	u
města	město	k1gNnSc2	město
Westport	Westport	k1gInSc1	Westport
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
se	se	k3xPyFc4	se
mnoho	mnoho	k4c1	mnoho
fjordů	fjord	k1gInPc2	fjord
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
chilské	chilský	k2eAgFnSc6d1	chilská
Patagonii	Patagonie	k1gFnSc6	Patagonie
<g/>
,	,	kIx,	,
v	v	k7c4	v
Kerguelenu	Kerguelen	k2eAgFnSc4d1	Kerguelen
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc2	Francie
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Fiordland	Fiordlanda	k1gFnPc2	Fiordlanda
na	na	k7c6	na
Jižním	jižní	k2eAgInSc6d1	jižní
ostrově	ostrov	k1gInSc6	ostrov
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
fjord	fjord	k1gInSc1	fjord
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
fjord	fjord	k1gInSc1	fjord
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
