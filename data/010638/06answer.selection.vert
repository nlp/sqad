<s>
Český	český	k2eAgInSc1d1	český
hydrometeorologický	hydrometeorologický	k2eAgInSc1d1	hydrometeorologický
ústav	ústav	k1gInSc1	ústav
(	(	kIx(	(
<g/>
ČHMÚ	ČHMÚ	kA	ČHMÚ
<g/>
,	,	kIx,	,
Czech	Czech	k1gMnSc1	Czech
hydrometeorological	hydrometeorologicat	k5eAaPmAgMnS	hydrometeorologicat
Institute	institut	k1gInSc5	institut
<g/>
,	,	kIx,	,
CHMI	CHMI	kA	CHMI
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ústřední	ústřední	k2eAgInSc1d1	ústřední
státní	státní	k2eAgInSc1d1	státní
orgán	orgán	k1gInSc1	orgán
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
pro	pro	k7c4	pro
obory	obor	k1gInPc4	obor
kvality	kvalita	k1gFnSc2	kvalita
ovzduší	ovzduší	k1gNnSc2	ovzduší
<g/>
,	,	kIx,	,
meteorologie	meteorologie	k1gFnSc2	meteorologie
<g/>
,	,	kIx,	,
klimatologie	klimatologie	k1gFnSc2	klimatologie
či	či	k8xC	či
hydrologie	hydrologie	k1gFnSc2	hydrologie
<g/>
.	.	kIx.	.
</s>
