<p>
<s>
Český	český	k2eAgInSc1d1	český
hydrometeorologický	hydrometeorologický	k2eAgInSc1d1	hydrometeorologický
ústav	ústav	k1gInSc1	ústav
(	(	kIx(
<g/>
ČHMÚ	ČHMÚ	kA
<g/>
,	,	kIx,
Czech	Czech	k1gMnSc1	Czech
hydrometeorological	hydrometeorologicat	k5eAaPmAgMnS
Institute	institut	k1gInSc5	institut
<g/>
,	,	kIx,
CHMI	CHMI	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
ústřední	ústřední	k2eAgInSc1d1	ústřední
státní	státní	k2eAgInSc1d1	státní
orgán	orgán	k1gInSc1	orgán
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
pro	pro	k7c4
obory	obor	k1gInPc4	obor
kvality	kvalita	k1gFnSc2	kvalita
ovzduší	ovzduší	k1gNnSc2	ovzduší
<g/>
,	,	kIx,
meteorologie	meteorologie	k1gFnSc2	meteorologie
<g/>
,	,	kIx,
klimatologie	klimatologie	k1gFnSc2	klimatologie
či	či	k8xC
hydrologie	hydrologie	k1gFnSc2	hydrologie
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1	jiné
je	být	k5eAaImIp3nS
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
za	za	k7c4
provoz	provoz	k1gInSc4	provoz
výstražné	výstražný	k2eAgFnSc2d1	výstražná
služby	služba	k1gFnSc2	služba
včetně	včetně	k7c2
Smogového	smogový	k2eAgInSc2d1	smogový
varovného	varovný	k2eAgInSc2d1	varovný
a	a	k8xC
regulačního	regulační	k2eAgInSc2d1	regulační
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
provozu	provoz	k1gInSc2	provoz
staničních	staniční	k2eAgFnPc2d1	staniční
sítí	síť	k1gFnPc2	síť
a	a	k8xC
zajišťování	zajišťování	k1gNnSc6	zajišťování
odborných	odborný	k2eAgFnPc2d1	odborná
služeb	služba	k1gFnPc2	služba
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
také	také	k9
vědecko-výzkumnou	vědeckoýzkumný	k2eAgFnSc7d1	vědecko-výzkumná
činností	činnost	k1gFnSc7	činnost
v	v	k7c6
oblastech	oblast	k1gFnPc6	oblast
zájmu	zájem	k1gInSc2	zájem
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInSc1d1	hlavní
sídlo	sídlo	k1gNnSc4	sídlo
ústavu	ústav	k1gInSc2	ústav
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
Praze-Komořanech	Praze-Komořan	k1gMnPc6	Praze-Komořan
na	na	k7c6
adrese	adresa	k1gFnSc6	adresa
Na	na	k7c6
Šabatce	Šabatka	k1gFnSc6	Šabatka
17	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1	Praha
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
má	mít	k5eAaImIp3nS
ČHMÚ	ČHMÚ	kA
regionální	regionální	k2eAgInSc4d1	regionální
pracoviště	pracoviště	k1gNnPc4	pracoviště
(	(	kIx(
<g/>
pobočku	pobočka	k1gFnSc4	pobočka
<g/>
)	)	kIx)
v	v	k7c6
dalších	další	k2eAgNnPc6d1	další
šesti	šest	k4xCc6
českých	český	k2eAgNnPc6d1	české
městech	město	k1gNnPc6	město
(	(	kIx(
<g/>
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
,	,	kIx,
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
a	a	k8xC
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ředitelem	ředitel	k1gMnSc7	ředitel
ústavu	ústav	k1gInSc2	ústav
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2	rok
2017	#num#	k4
Mgr.	Mgr.	kA
Mark	Mark	k1gMnSc1	Mark
Rieder	Rieder	k1gMnSc1	Rieder
<g/>
.	.	kIx.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?
Status	status	k1gInSc4	status
a	a	k8xC
účel	účel	k1gInSc4	účel
==	==	k?
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
hydrometeorologický	hydrometeorologický	k2eAgInSc1d1	hydrometeorologický
ústav	ústav	k1gInSc1	ústav
je	být	k5eAaImIp3nS
státní	státní	k2eAgFnSc7d1	státní
příspěvkovou	příspěvkový	k2eAgFnSc7d1	příspěvková
organizací	organizace	k1gFnSc7	organizace
zřizovanou	zřizovaný	k2eAgFnSc7d1	zřizovaná
Ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
účelem	účel	k1gInSc7	účel
je	být	k5eAaImIp3nS
poskytování	poskytování	k1gNnSc1	poskytování
odborných	odborný	k2eAgFnPc2d1	odborná
služeb	služba	k1gFnPc2	služba
v	v	k7c6
oborech	obor	k1gInPc6	obor
kvalita	kvalita	k1gFnSc1	kvalita
ovzduší	ovzduší	k1gNnSc2	ovzduší
<g/>
,	,	kIx,
hydrologie	hydrologie	k1gFnSc1	hydrologie
<g/>
,	,	kIx,
jakost	jakost	k1gFnSc1	jakost
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,
klimatologie	klimatologie	k1gFnSc2	klimatologie
a	a	k8xC
meteorologie	meteorologie	k1gFnSc2	meteorologie
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
přednostně	přednostně	k6eAd1
pro	pro	k7c4
státní	státní	k2eAgFnSc4d1	státní
správu	správa	k1gFnSc4	správa
<g/>
.	.	kIx.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?
Historie	historie	k1gFnSc1	historie
==	==	k?
</s>
</p>
<p>
<s>
Základ	základ	k1gInSc1	základ
dnešního	dnešní	k2eAgInSc2d1	dnešní
ústavu	ústav	k1gInSc2	ústav
vznikl	vzniknout	k5eAaPmAgInS
na	na	k7c6
rozhraní	rozhraní	k1gNnSc6	rozhraní
let	léto	k1gNnPc2	léto
1919	#num#	k4
a	a	k8xC
1920	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
Československý	československý	k2eAgInSc1d1	československý
státní	státní	k2eAgInSc1d1	státní
ústav	ústav	k1gInSc1	ústav
meteorologický	meteorologický	k2eAgInSc1d1	meteorologický
v	v	k7c6
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vládním	vládní	k2eAgNnSc7d1	vládní
nařízením	nařízení	k1gNnSc7	nařízení
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
č.	č.	k?
96	#num#	k4
<g/>
/	/	kIx~
<g/>
1953	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
s	s	k7c7
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2	leden
1954	#num#	k4
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
sloučení	sloučení	k1gNnSc3	sloučení
tohoto	tento	k3xDgInSc2
ústavu	ústav	k1gInSc2	ústav
s	s	k7c7
hydrologií	hydrologie	k1gFnSc7	hydrologie
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2	rok
1967	#num#	k4
byl	být	k5eAaImAgInS
do	do	k7c2
ústavu	ústav	k1gInSc2	ústav
začleněn	začlenit	k5eAaPmNgInS
třetí	třetí	k4xOgInSc1
obor	obor	k1gInSc1	obor
–	–	k?
ochrana	ochrana	k1gFnSc1	ochrana
čistoty	čistota	k1gFnSc2	čistota
ovzduší	ovzduší	k1gNnSc2	ovzduší
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4
vzrůstající	vzrůstající	k2eAgInSc4d1	vzrůstající
význam	význam	k1gInSc4	význam
ochrany	ochrana	k1gFnSc2	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.
<g/>
Mezi	mezi	k7c7
roky	rok	k1gInPc7	rok
1979	#num#	k4
až	až	k9
2000	#num#	k4
sloužila	sloužit	k5eAaImAgFnS
ústavu	ústava	k1gFnSc4	ústava
pro	pro	k7c4
pozorování	pozorování	k1gNnSc4	pozorování
oblačnosti	oblačnost	k1gFnSc2	oblačnost
meteorologická	meteorologický	k2eAgFnSc1d1	meteorologická
věž	věž	k1gFnSc1	věž
Libuš	Libuš	k1gInSc1	Libuš
v	v	k7c6
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
k	k	k7c3
tomuto	tento	k3xDgInSc3
účelu	účel	k1gInSc3	účel
slouží	sloužit	k5eAaImIp3nS
meteorologická	meteorologický	k2eAgFnSc1d1	meteorologická
věž	věž	k1gFnSc1	věž
Brdy	Brdy	k1gInPc1	Brdy
postavená	postavený	k2eAgFnSc1d1	postavená
v	v	k7c6
roce	rok	k1gInSc6	rok
2000	#num#	k4
<g/>
.	.	kIx.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?
Pobočky	pobočka	k1gFnSc2	pobočka
==	==	k?
</s>
</p>
<p>
<s>
ČHMÚ	ČHMÚ	kA
má	mít	k5eAaImIp3nS
v	v	k7c6
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
celkem	celkem	k6eAd1
7	#num#	k4
regionálních	regionální	k2eAgFnPc2d1	regionální
poboček	pobočka	k1gFnPc2	pobočka
v	v	k7c6
následujících	následující	k2eAgNnPc6d1	následující
městech	město	k1gNnPc6	město
<g/>
:	:	kIx,
</s>
</p>
<p>
<s>
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~
Na	na	k7c6
Šabatce	Šabatka	k1gFnSc6	Šabatka
17	#num#	k4
<g/>
,	,	kIx,
143	#num#	k4
0	#num#	k4
<g/>
6	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1	Praha
4	#num#	k4
-	-	kIx~
Komořany	Komořan	k1gMnPc7	Komořan
</s>
</p>
<p>
<s>
Brno	Brno	k1gNnSc1	Brno
-	-	kIx~
Kroftova	Kroftův	k2eAgFnSc1d1	Kroftova
43	#num#	k4
<g/>
,	,	kIx,
616	#num#	k4
43	#num#	k4
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1	Brno
-	-	kIx~
Žabovřesky	Žabovřesky	k1gFnPc1	Žabovřesky
</s>
</p>
<p>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
-	-	kIx~
K	k	k7c3
Myslivně	myslivna	k1gFnSc3	myslivna
3	#num#	k4
<g/>
,	,	kIx,
708	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
Ostrava-Poruba	Ostrava-Poruba	k1gFnSc1	Ostrava-Poruba
</s>
</p>
<p>
<s>
Plzeň	Plzeň	k1gFnSc1	Plzeň
-	-	kIx~
Mozartova	Mozartův	k2eAgFnSc1d1	Mozartova
41	#num#	k4
<g/>
,	,	kIx,
323	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
Plzeň	Plzeň	k1gFnSc1	Plzeň
</s>
</p>
<p>
<s>
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7
Labem	Labe	k1gNnSc7	Labe
-	-	kIx~
Kočkovská	Kočkovský	k2eAgFnSc1d1	Kočkovská
18	#num#	k4
<g/>
,	,	kIx,
400	#num#	k4
11	#num#	k4
<g/>
,	,	kIx,
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7
Labem	Labe	k1gNnSc7	Labe
-	-	kIx~
Kočkov	Kočkov	k1gInSc1	Kočkov
</s>
</p>
<p>
<s>
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
-	-	kIx~
Dvorská	Dvorská	k1gFnSc1	Dvorská
410	#num#	k4
<g/>
,	,	kIx,
503	#num#	k4
11	#num#	k4
<g/>
,	,	kIx,
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
-	-	kIx~
Svobodné	svobodný	k2eAgInPc1d1	svobodný
Dvory	Dvůr	k1gInPc1	Dvůr
</s>
</p>
<p>
<s>
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
-	-	kIx~
Antala	Antal	k1gMnSc2	Antal
Staška	Stašek	k1gMnSc2	Stašek
32	#num#	k4
<g/>
,	,	kIx,
370	#num#	k4
0	#num#	k4
<g/>
7	#num#	k4
<g/>
,	,	kIx,
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?
Organizační	organizační	k2eAgFnSc1d1	organizační
struktura	struktura	k1gFnSc1	struktura
==	==	k?
</s>
</p>
<p>
<s>
ČHMÚ	ČHMÚ	kA
sestává	sestávat	k5eAaImIp3nS
ze	z	k7c2
tří	tři	k4xCgInPc2
odborných	odborný	k2eAgInPc2d1	odborný
úseků	úsek	k1gInPc2	úsek
(	(	kIx(
<g/>
Úsek	úsek	k1gInSc1	úsek
meteorologie	meteorologie	k1gFnSc2	meteorologie
a	a	k8xC
klimatologie	klimatologie	k1gFnSc2	klimatologie
<g/>
,	,	kIx,
Úsek	úsek	k1gInSc4	úsek
hydrologie	hydrologie	k1gFnSc2	hydrologie
a	a	k8xC
Úsek	úsek	k1gInSc1	úsek
kvality	kvalita	k1gFnSc2	kvalita
ovzduší	ovzduší	k1gNnSc2	ovzduší
<g/>
)	)	kIx)
,	,	kIx,
dvou	dva	k4xCgInPc2
podpůrných	podpůrný	k2eAgInPc2d1	podpůrný
úseků	úsek	k1gInPc2	úsek
(	(	kIx(
<g/>
Úsek	úsek	k1gInSc1	úsek
ekonomicko-správní	ekonomickoprávní	k2eAgInSc1d1	ekonomicko-správní
a	a	k8xC
Úsek	úsek	k1gInSc1	úsek
informatiky	informatika	k1gFnSc2	informatika
<g/>
)	)	kIx)
a	a	k8xC
odboru	odbor	k1gInSc2	odbor
ředitele	ředitel	k1gMnSc2	ředitel
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
ústředního	ústřední	k2eAgNnSc2d1	ústřední
pracoviště	pracoviště	k1gNnSc2	pracoviště
v	v	k7c6
Praze-Komořanech	Praze-Komořan	k1gMnPc6	Praze-Komořan
má	mít	k5eAaImIp3nS
ČHMÚ	ČHMÚ	kA
regionální	regionální	k2eAgInSc4d1	regionální
pracoviště	pracoviště	k1gNnPc4	pracoviště
(	(	kIx(
<g/>
pobočku	pobočka	k1gFnSc4	pobočka
<g/>
)	)	kIx)
v	v	k7c6
dalších	další	k2eAgNnPc6d1	další
šesti	šest	k4xCc6
českých	český	k2eAgNnPc6d1	české
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
pobočkách	pobočka	k1gFnPc6	pobočka
jsou	být	k5eAaImIp3nP
pak	pak	k6eAd1
zastoupeny	zastoupit	k5eAaPmNgInP
jednotlvé	jednotlvý	k2eAgInPc1d1	jednotlvý
odborné	odborný	k2eAgInPc1d1	odborný
úseky	úsek	k1gInPc1	úsek
jednotlivými	jednotlivý	k2eAgNnPc7d1	jednotlivé
odděleními	oddělení	k1gNnPc7	oddělení
(	(	kIx(
<g/>
ne	ne	k9
na	na	k7c6
každé	každý	k3xTgFnSc6
pobočce	pobočka	k1gFnSc6	pobočka
jsou	být	k5eAaImIp3nP
zastoupeny	zastoupit	k5eAaPmNgInP
všechny	všechen	k3xTgInPc1
úseky	úsek	k1gInPc1	úsek
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?
Reference	reference	k1gFnPc1	reference
===	===	k?
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
fyziky	fyzika	k1gFnSc2	fyzika
atmosféry	atmosféra	k1gFnSc2	atmosféra
AV	AV	kA
ČR	ČR	kA
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC
videa	video	k1gNnSc2	video
k	k	k7c3
tématu	téma	k1gNnSc3	téma
Český	český	k2eAgInSc4d1	český
hydrometeorologický	hydrometeorologický	k2eAgInSc4d1	hydrometeorologický
ústav	ústav	k1gInSc4	ústav
ve	v	k7c4
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
ČHMÚ	ČHMÚ	kA
Brno	Brno	k1gNnSc1	Brno
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
ČHMÚ	ČHMÚ	kA
Ústí	ústit	k5eAaImIp3nP
nad	nad	k7c7
Labem	Labe	k1gNnSc7	Labe
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc4	stránka
odboru	odbor	k1gInSc2	odbor
letecké	letecký	k2eAgFnSc2d1	letecká
meteorologie	meteorologie	k1gFnSc2	meteorologie
provozované	provozovaný	k2eAgFnSc2d1	provozovaná
ČHMÚ	ČHMÚ	kA
</s>
</p>
