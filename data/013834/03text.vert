<s>
Arrondissement	Arrondissement	k1gMnSc1
Namur	Namur	k1gMnSc1
</s>
<s>
Okres	okres	k1gInSc1
Namur	Namur	k1gMnSc1
Arrondissement	Arrondissement	k1gMnSc1
de	de	k?
Namur	Namur	k1gMnSc1
most	most	k1gInSc4
přes	přes	k7c4
Mázu	máz	k1gInSc6
ve	v	k7c6
městě	město	k1gNnSc6
Namur	Namura	k1gFnPc2
Geografie	geografie	k1gFnSc2
</s>
<s>
okres	okres	k1gInSc1
Namur	Namura	k1gFnPc2
na	na	k7c6
mapě	mapa	k1gFnSc6
provincie	provincie	k1gFnSc2
Namur	Namura	k1gFnPc2
</s>
<s>
provincie	provincie	k1gFnSc1
Namur	Namura	k1gFnPc2
na	na	k7c6
mapě	mapa	k1gFnSc6
Belgie	Belgie	k1gFnSc2
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
Namur	Namur	k1gMnSc1
Status	status	k1gInSc1
</s>
<s>
arrondissement	arrondissement	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
30	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
4	#num#	k4
<g/>
°	°	k?
<g/>
54	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
1	#num#	k4
164,85	164,85	k4
km²	km²	k?
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
+1	+1	k4
Obyvatelstvo	obyvatelstvo	k1gNnSc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
110	#num#	k4
335	#num#	k4
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
94,7	94,7	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
regionu	region	k1gInSc2
Nadřazený	nadřazený	k2eAgInSc1d1
celek	celek	k1gInSc1
</s>
<s>
Namur	Namur	k1gMnSc1
Druh	druh	k1gInSc4
celku	celek	k1gInSc2
</s>
<s>
provincie	provincie	k1gFnSc1
Podřízené	podřízená	k1gFnSc2
celky	celek	k1gInPc7
</s>
<s>
16	#num#	k4
obcí	obec	k1gFnPc2
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Namur	Namur	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Arrondissement	Arrondissement	k1gMnSc1
Namur	Namur	k1gMnSc1
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
:	:	kIx,
Arrondissement	Arrondissement	k1gMnSc1
de	de	k?
Namur	Namur	k1gMnSc1
<g/>
;	;	kIx,
nizozemsky	nizozemsky	k6eAd1
<g/>
:	:	kIx,
Arrondissement	Arrondissement	k1gInSc1
Namen	Namen	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jeden	jeden	k4xCgInSc1
ze	z	k7c2
tří	tři	k4xCgInPc2
arrondissementů	arrondissement	k1gInPc2
(	(	kIx(
<g/>
okresů	okres	k1gInPc2
<g/>
)	)	kIx)
v	v	k7c6
provincii	provincie	k1gFnSc6
Namur	Namura	k1gFnPc2
v	v	k7c6
Belgii	Belgie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
politický	politický	k2eAgInSc4d1
a	a	k8xC
zároveň	zároveň	k6eAd1
soudní	soudní	k2eAgInSc1d1
okres	okres	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc3
2017	#num#	k4
činil	činit	k5eAaImAgInS
110	#num#	k4
335	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozloha	rozloha	k1gFnSc1
okresu	okres	k1gInSc2
činí	činit	k5eAaImIp3nS
1	#num#	k4
164,85	164,85	k4
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s>
Obce	obec	k1gFnPc1
</s>
<s>
Okres	okres	k1gInSc1
Namur	Namura	k1gFnPc2
sestává	sestávat	k5eAaImIp3nS
z	z	k7c2
těchto	tento	k3xDgFnPc2
obcí	obec	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
Andenne	Andennout	k5eAaPmIp3nS,k5eAaImIp3nS
</s>
<s>
Assesse	Assesse	k6eAd1
</s>
<s>
Éghezée	Éghezée	k6eAd1
</s>
<s>
Fernelmont	Fernelmont	k1gMnSc1
</s>
<s>
Floreffe	Floreff	k1gMnSc5
</s>
<s>
Fosses-la-Ville	Fosses-la-Ville	k6eAd1
</s>
<s>
Gembloux	Gembloux	k1gInSc1
</s>
<s>
Gesves	Gesves	k1gMnSc1
</s>
<s>
Jemeppe-sur-Sambre	Jemeppe-sur-Sambr	k1gMnSc5
</s>
<s>
La	la	k1gNnSc1
Bruyè	Bruyè	k1gMnSc5
</s>
<s>
Mettet	Mettet	k1gMnSc1
</s>
<s>
Namur	Namur	k1gMnSc1
</s>
<s>
Ohey	Ohea	k1gFnPc1
</s>
<s>
Profondeville	Profondeville	k6eAd1
</s>
<s>
Sambreville	Sambreville	k6eAd1
</s>
<s>
Sombreffe	Sombreff	k1gMnSc5
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Arrondissement	Arrondissement	k1gMnSc1
of	of	k?
Namur	Namur	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Arrondissementy	Arrondissement	k1gInPc1
v	v	k7c6
Belgii	Belgie	k1gFnSc6
Bruselský	bruselský	k2eAgInSc1d1
region	region	k1gInSc1
</s>
<s>
Arrondissement	Arrondissement	k1gMnSc1
Brusel-hlavní	Brusel-hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
Vlámský	vlámský	k2eAgInSc1d1
region	region	k1gInSc1
</s>
<s>
Aalst	Aalst	k1gFnSc1
</s>
<s>
Antverpy	Antverpy	k1gFnPc1
</s>
<s>
Bruggy	Bruggy	k1gFnPc1
</s>
<s>
Dendermonde	Dendermond	k1gMnSc5
</s>
<s>
Diksmuide	Diksmuid	k1gMnSc5
</s>
<s>
Eeklo	Eeklo	k1gNnSc1
</s>
<s>
Gent	Gent	k1gMnSc1
</s>
<s>
Halle-Vilvoorde	Halle-Vilvoorde	k6eAd1
</s>
<s>
Hasselt	Hasselt	k1gMnSc1
</s>
<s>
Kortrijk	Kortrijk	k6eAd1
</s>
<s>
Lovaň	Lovanit	k5eAaPmRp2nS
</s>
<s>
Maaseik	Maaseik	k1gMnSc1
</s>
<s>
Mechelen	Mechelen	k2eAgInSc1d1
</s>
<s>
Ostende	Ostende	k1gNnSc1
</s>
<s>
Oudenaarde	Oudenaard	k1gMnSc5
</s>
<s>
Roeselare	Roeselar	k1gMnSc5
</s>
<s>
Sint-Niklaas	Sint-Niklaas	k1gMnSc1
</s>
<s>
Tielt	Tielt	k1gMnSc1
</s>
<s>
Tongeren	Tongerna	k1gFnPc2
</s>
<s>
Turnhout	Turnhout	k1gMnSc1
</s>
<s>
Veurne	Veurnout	k5eAaPmIp3nS
</s>
<s>
Ieper	Ieper	k1gInSc1
<g/>
/	/	kIx~
<g/>
Ypry	Ypry	k1gInPc1
Valonský	valonský	k2eAgInSc1d1
region	region	k1gInSc4
</s>
<s>
Arlon	Arlon	k1gMnSc1
</s>
<s>
Ath	Ath	k?
</s>
<s>
Bastogne	Bastognout	k5eAaImIp3nS,k5eAaPmIp3nS
</s>
<s>
Charleroi	Charleroi	k6eAd1
</s>
<s>
Dinant	Dinant	k1gMnSc1
</s>
<s>
Huy	Huy	k?
</s>
<s>
Lutych	Lutych	k1gMnSc1
</s>
<s>
Marche-en-Famenne	Marche-en-Famennout	k5eAaImIp3nS,k5eAaPmIp3nS
</s>
<s>
Mons	Mons	k6eAd1
</s>
<s>
Mouscron	Mouscron	k1gMnSc1
</s>
<s>
Namur	Namur	k1gMnSc1
</s>
<s>
Neufchâteau	Neufchâteau	k6eAd1
</s>
<s>
Nivelles	Nivelles	k1gMnSc1
</s>
<s>
Philippeville	Philippeville	k6eAd1
</s>
<s>
Soignies	Soignies	k1gMnSc1
</s>
<s>
Thuin	Thuin	k1gMnSc1
</s>
<s>
Tournai	Tournai	k6eAd1
</s>
<s>
Verviers	Verviers	k6eAd1
</s>
<s>
Virton	Virton	k1gInSc1
</s>
<s>
Waremme	Waremit	k5eAaPmRp1nP
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Belgie	Belgie	k1gFnSc1
|	|	kIx~
Geografie	geografie	k1gFnSc1
</s>
