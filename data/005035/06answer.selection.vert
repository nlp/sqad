<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
lidová	lidový	k2eAgFnSc1d1	lidová
představa	představa	k1gFnSc1	představa
o	o	k7c6	o
Galileovi	Galileus	k1gMnSc6	Galileus
vynalézajícím	vynalézající	k2eAgMnSc6d1	vynalézající
dalekohled	dalekohled	k1gInSc4	dalekohled
není	být	k5eNaImIp3nS	být
přesná	přesný	k2eAgFnSc1d1	přesná
<g/>
,	,	kIx,	,
rozhodně	rozhodně	k6eAd1	rozhodně
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
použil	použít	k5eAaPmAgMnS	použít
dalekohled	dalekohled	k1gInSc4	dalekohled
k	k	k7c3	k
pozorování	pozorování	k1gNnSc3	pozorování
oblohy	obloha	k1gFnSc2	obloha
<g/>
.	.	kIx.	.
</s>
