<s>
Translokace	translokace	k1gFnSc1	translokace
představuje	představovat	k5eAaImIp3nS	představovat
dědičnou	dědičný	k2eAgFnSc4d1	dědičná
formu	forma	k1gFnSc4	forma
<g/>
,	,	kIx,	,
přítomna	přítomen	k2eAgFnSc1d1	přítomna
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
u	u	k7c2	u
čtyř	čtyři	k4xCgNnPc2	čtyři
procent	procento	k1gNnPc2	procento
nemocných	nemocná	k1gFnPc2	nemocná
<g/>
;	;	kIx,	;
ovšem	ovšem	k9	ovšem
ne	ne	k9	ne
u	u	k7c2	u
všech	všecek	k3xTgFnPc2	všecek
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
zděděnou	zděděný	k2eAgFnSc4d1	zděděná
poruchu	porucha	k1gFnSc4	porucha
<g/>
.	.	kIx.	.
</s>
