<s>
Konnex	Konnex	k1gInSc1
</s>
<s>
Konex	Konex	k1gInSc1
je	být	k5eAaImIp3nS
odborný	odborný	k2eAgInSc1d1
přírodovědecký	přírodovědecký	k2eAgInSc1d1
pojem	pojem	k1gInSc1
pocházející	pocházející	k2eAgInSc1d1
z	z	k7c2
oblasti	oblast	k1gFnSc2
biologie	biologie	k1gFnSc2
a	a	k8xC
ekologie	ekologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
jím	on	k3xPp3gNnSc7
vyjádřena	vyjádřen	k2eAgFnSc1d1
přímá	přímý	k2eAgFnSc1d1
a	a	k8xC
zcela	zcela	k6eAd1
nepopiratelná	popiratelný	k2eNgFnSc1d1
vazba	vazba	k1gFnSc1
mezi	mezi	k7c7
jednotlivými	jednotlivý	k2eAgInPc7d1
živočišnými	živočišný	k2eAgInPc7d1
nebo	nebo	k8xC
rostlinnými	rostlinný	k2eAgInPc7d1
druhy	druh	k1gInPc7
(	(	kIx(
<g/>
jednotlivými	jednotlivý	k2eAgInPc7d1
taxony	taxon	k1gInPc7
<g/>
)	)	kIx)
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
biocenózách	biocenóza	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Příklady	příklad	k1gInPc1
</s>
<s>
parekie	parekie	k1gFnSc1
-	-	kIx~
soužití	soužití	k1gNnPc2
dvou	dva	k4xCgInPc2
druhů	druh	k1gInPc2
živočichů	živočich	k1gMnPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jeden	jeden	k4xCgInSc1
druh	druh	k1gInSc1
ochraňuje	ochraňovat	k5eAaImIp3nS
jiný	jiný	k2eAgInSc1d1
druh	druh	k1gInSc1
</s>
<s>
parazitizmus	parazitizmus	k1gInSc1
-	-	kIx~
soužití	soužití	k1gNnSc1
dvou	dva	k4xCgMnPc2
druhů	druh	k1gMnPc2
organizmů	organizmus	k1gInPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jeden	jeden	k4xCgInSc1
druh	druh	k1gInSc1
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
jiný	jiný	k2eAgInSc1d1
druh	druh	k1gInSc1
např.	např.	kA
jako	jako	k8xC,k8xS
zdroj	zdroj	k1gInSc1
potravy	potrava	k1gFnSc2
</s>
<s>
symbióza	symbióza	k1gFnSc1
<g/>
,	,	kIx,
mutualizmus	mutualizmus	k1gInSc1
-	-	kIx~
vzájemné	vzájemný	k2eAgNnSc4d1
soužití	soužití	k1gNnSc4
dvou	dva	k4xCgInPc2
organizmů	organizmus	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yQgInPc4,k3yRgInPc4
je	být	k5eAaImIp3nS
vzájemně	vzájemně	k6eAd1
prospěšné	prospěšný	k2eAgNnSc1d1
pro	pro	k7c4
oba	dva	k4xCgInPc4
druhy	druh	k1gInPc4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
PETRÁČKOVÁ	Petráčková	k1gFnSc1
<g/>
,	,	kIx,
Věra	Věra	k1gFnSc1
<g/>
;	;	kIx,
KRAUS	Kraus	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akademický	akademický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
cizích	cizí	k2eAgNnPc2d1
slov	slovo	k1gNnPc2
A-	A-	k1gMnPc2
<g/>
Ž.	Ž.	kA
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
834	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
607	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Biologie	biologie	k1gFnSc1
</s>
