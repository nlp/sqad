<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
regionální	regionální	k2eAgFnSc1d1	regionální
doprava	doprava	k1gFnSc1	doprava
zajišťována	zajišťovat	k5eAaImNgFnS	zajišťovat
především	především	k9	především
autobusy	autobus	k1gInPc7	autobus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
také	také	k6eAd1	také
železniční	železniční	k2eAgFnSc7d1	železniční
dopravou	doprava	k1gFnSc7	doprava
(	(	kIx(	(
<g/>
přesto	přesto	k8xC	přesto
však	však	k9	však
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
státy	stát	k1gInPc7	stát
s	s	k7c7	s
nejhustší	hustý	k2eAgFnSc7d3	nejhustší
sítí	síť	k1gFnSc7	síť
železničních	železniční	k2eAgFnPc2d1	železniční
tratí	trať	k1gFnPc2	trať
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
