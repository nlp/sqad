<p>
<s>
Regionální	regionální	k2eAgFnSc1d1	regionální
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
neoficiální	neoficiální	k2eAgInSc4d1	neoficiální
název	název	k1gInSc4	název
pro	pro	k7c4	pro
část	část	k1gFnSc4	část
osobní	osobní	k2eAgFnSc2d1	osobní
veřejné	veřejný	k2eAgFnSc2d1	veřejná
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
veřejné	veřejný	k2eAgNnSc1d1	veřejné
<g/>
,	,	kIx,	,
určenou	určený	k2eAgFnSc4d1	určená
k	k	k7c3	k
zajišťování	zajišťování	k1gNnSc3	zajišťování
místní	místní	k2eAgFnSc2d1	místní
dopravní	dopravní	k2eAgFnSc2d1	dopravní
obsluhy	obsluha	k1gFnSc2	obsluha
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
označení	označení	k1gNnSc1	označení
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
větších	veliký	k2eAgFnPc2d2	veliký
oblasti	oblast	k1gFnSc6	oblast
včetně	včetně	k7c2	včetně
měst	město	k1gNnPc2	město
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
nacházejí	nacházet	k5eAaImIp3nP	nacházet
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
pro	pro	k7c4	pro
samotné	samotný	k2eAgInPc4d1	samotný
systémy	systém	k1gInPc4	systém
městské	městský	k2eAgFnSc2d1	městská
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
uvnitř	uvnitř	k7c2	uvnitř
území	území	k1gNnSc2	území
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
regionální	regionální	k2eAgFnSc1d1	regionální
doprava	doprava	k1gFnSc1	doprava
zajišťována	zajišťovat	k5eAaImNgFnS	zajišťovat
především	především	k9	především
autobusy	autobus	k1gInPc7	autobus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
také	také	k6eAd1	také
železniční	železniční	k2eAgFnSc7d1	železniční
dopravou	doprava	k1gFnSc7	doprava
(	(	kIx(	(
<g/>
přesto	přesto	k8xC	přesto
však	však	k9	však
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
státy	stát	k1gInPc7	stát
s	s	k7c7	s
nejhustší	hustý	k2eAgFnSc7d3	nejhustší
sítí	síť	k1gFnSc7	síť
železničních	železniční	k2eAgFnPc2d1	železniční
tratí	trať	k1gFnPc2	trať
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
regionální	regionální	k2eAgFnSc2d1	regionální
autobusové	autobusový	k2eAgFnSc2d1	autobusová
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
spadají	spadat	k5eAaImIp3nP	spadat
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
soukromých	soukromý	k2eAgMnPc2d1	soukromý
dopravců	dopravce	k1gMnPc2	dopravce
zřizovaly	zřizovat	k5eAaImAgFnP	zřizovat
autobusové	autobusový	k2eAgFnPc1d1	autobusová
linky	linka	k1gFnPc1	linka
také	také	k9	také
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
Československé	československý	k2eAgFnSc2d1	Československá
státní	státní	k2eAgFnSc2d1	státní
dráhy	dráha	k1gFnSc2	dráha
nebo	nebo	k8xC	nebo
poštovní	poštovní	k2eAgFnSc1d1	poštovní
správa	správa	k1gFnSc1	správa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
pak	pak	k6eAd1	pak
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jejímu	její	k3xOp3gInSc3	její
velkému	velký	k2eAgInSc3d1	velký
rozvoji	rozvoj	k1gInSc3	rozvoj
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
ČSAD	ČSAD	kA	ČSAD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Regionální	regionální	k2eAgFnSc1d1	regionální
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
také	také	k9	také
pojem	pojem	k1gInSc1	pojem
v	v	k7c6	v
dopravě	doprava	k1gFnSc6	doprava
železniční	železniční	k2eAgFnSc6d1	železniční
<g/>
.	.	kIx.	.
</s>
<s>
Regionální	regionální	k2eAgFnSc1d1	regionální
neboli	neboli	k8xC	neboli
lokální	lokální	k2eAgFnSc1d1	lokální
trať	trať	k1gFnSc1	trať
po	po	k7c4	po
které	který	k3yIgNnSc4	který
jezdí	jezdit	k5eAaImIp3nP	jezdit
menší	malý	k2eAgInPc1d2	menší
motorové	motorový	k2eAgInPc1d1	motorový
vlaky	vlak	k1gInPc1	vlak
či	či	k8xC	či
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
obce	obec	k1gFnPc4	obec
či	či	k8xC	či
malé	malý	k2eAgNnSc4d1	malé
města	město	k1gNnPc4	město
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jsou	být	k5eAaImIp3nP	být
spojena	spojit	k5eAaPmNgFnS	spojit
se	s	k7c7	s
železniční	železniční	k2eAgFnSc7d1	železniční
dopravou	doprava	k1gFnSc7	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
<g/>
:	:	kIx,	:
trať	trať	k1gFnSc1	trať
Frýdek-Místek	Frýdek-Místek	k1gInSc1	Frýdek-Místek
-	-	kIx~	-
Český	český	k2eAgInSc1d1	český
Těšín	Těšín	k1gInSc1	Těšín
<g/>
.	.	kIx.	.
</s>
</p>
