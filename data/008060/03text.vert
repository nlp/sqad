<s>
Elvis	Elvis	k1gMnSc1	Elvis
Aaron	Aaron	k1gMnSc1	Aaron
Presley	Preslea	k1gFnSc2	Preslea
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
také	také	k9	také
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Král	Král	k1gMnSc1	Král
Rock	rock	k1gInSc1	rock
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
Rollu	Roll	k1gMnSc6	Roll
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
zkráceně	zkráceně	k6eAd1	zkráceně
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Král	Král	k1gMnSc1	Král
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
King	King	k1gMnSc1	King
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
Tupelo	Tupela	k1gFnSc5	Tupela
<g/>
,	,	kIx,	,
Mississippi	Mississippi	k1gFnSc5	Mississippi
<g/>
,	,	kIx,	,
USA	USA	kA	USA
–	–	k?	–
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
Memphis	Memphis	k1gFnSc1	Memphis
<g/>
,	,	kIx,	,
Tennessee	Tennessee	k1gFnSc1	Tennessee
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
herec	herec	k1gMnSc1	herec
<g/>
.	.	kIx.	.
</s>
<s>
Dokázal	dokázat	k5eAaPmAgMnS	dokázat
skloubit	skloubit	k5eAaPmF	skloubit
inspiraci	inspirace	k1gFnSc4	inspirace
z	z	k7c2	z
černošského	černošský	k2eAgInSc2d1	černošský
spirituálu	spirituál	k1gInSc2	spirituál
<g/>
,	,	kIx,	,
blues	blues	k1gNnSc2	blues
a	a	k8xC	a
country	country	k2eAgInSc1d1	country
a	a	k8xC	a
stal	stát	k5eAaPmAgInS	stát
se	se	k3xPyFc4	se
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
postavou	postava	k1gFnSc7	postava
amerického	americký	k2eAgInSc2d1	americký
rock	rock	k1gInSc4	rock
and	and	k?	and
rollu	roll	k1gInSc2	roll
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
trvající	trvající	k2eAgFnSc2d1	trvající
kariéry	kariéra	k1gFnSc2	kariéra
překonal	překonat	k5eAaPmAgMnS	překonat
mnoho	mnoho	k4c1	mnoho
rekordů	rekord	k1gInPc2	rekord
prodeje	prodej	k1gInSc2	prodej
desek	deska	k1gFnPc2	deska
a	a	k8xC	a
návštěvnosti	návštěvnost	k1gFnSc2	návštěvnost
koncertů	koncert	k1gInPc2	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Víc	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
jeho	jeho	k3xOp3gInPc2	jeho
singlů	singl	k1gInPc2	singl
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
TOP	topit	k5eAaImRp2nS	topit
40	[number]	k4	40
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
17	[number]	k4	17
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
bylo	být	k5eAaImAgNnS	být
prodáno	prodat	k5eAaPmNgNnS	prodat
přes	přes	k7c4	přes
jednu	jeden	k4xCgFnSc4	jeden
miliardu	miliarda	k4xCgFnSc4	miliarda
kopií	kopie	k1gFnPc2	kopie
jeho	jeho	k3xOp3gFnPc2	jeho
nahrávek	nahrávka	k1gFnPc2	nahrávka
<g/>
.	.	kIx.	.
</s>
<s>
Narozen	narozen	k2eAgMnSc1d1	narozen
v	v	k7c6	v
Tupelu	Tupel	k1gInSc6	Tupel
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1935	[number]	k4	1935
v	v	k7c4	v
3.30	[number]	k4	3.30
jako	jako	k8xS	jako
dvojče	dvojče	k1gNnSc4	dvojče
o	o	k7c4	o
půl	půl	k1xP	půl
hodiny	hodina	k1gFnSc2	hodina
dříve	dříve	k6eAd2	dříve
narozeného	narozený	k2eAgMnSc4d1	narozený
Jesse	Jess	k1gMnSc4	Jess
Garona	Garon	k1gMnSc4	Garon
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
narodil	narodit	k5eAaPmAgMnS	narodit
již	již	k6eAd1	již
mrtvý	mrtvý	k2eAgMnSc1d1	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
Gladys	Gladysa	k1gFnPc2	Gladysa
Presley	Preslea	k1gFnSc2	Preslea
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Smith	Smith	k1gInSc1	Smith
<g/>
,	,	kIx,	,
a	a	k8xC	a
Vernon	Vernon	k1gMnSc1	Vernon
Presley	Preslea	k1gFnSc2	Preslea
pocházeli	pocházet	k5eAaImAgMnP	pocházet
z	z	k7c2	z
velmi	velmi	k6eAd1	velmi
chudých	chudý	k2eAgInPc2d1	chudý
poměrů	poměr	k1gInPc2	poměr
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
white	white	k5eAaPmIp2nP	white
trash	trash	k1gInSc1	trash
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
pro	pro	k7c4	pro
největší	veliký	k2eAgFnSc4d3	veliký
chudinu	chudina	k1gFnSc4	chudina
bílé	bílý	k2eAgFnSc2d1	bílá
pleti	pleť	k1gFnSc2	pleť
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
Elvis	Elvis	k1gMnSc1	Elvis
později	pozdě	k6eAd2	pozdě
popíral	popírat	k5eAaImAgMnS	popírat
jakékoliv	jakýkoliv	k3yIgInPc4	jakýkoliv
náznaky	náznak	k1gInPc4	náznak
dřívější	dřívější	k2eAgFnSc2d1	dřívější
chudoby	chudoba	k1gFnSc2	chudoba
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Vždy	vždy	k6eAd1	vždy
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
bylo	být	k5eAaImAgNnS	být
jídlo	jídlo	k1gNnSc1	jídlo
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
jsem	být	k5eAaImIp1nS	být
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
čem	co	k3yQnSc6	co
chodit	chodit	k5eAaImF	chodit
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
byla	být	k5eAaImAgFnS	být
situace	situace	k1gFnSc1	situace
Presleyových	Presleyových	k2eAgFnSc1d1	Presleyových
velmi	velmi	k6eAd1	velmi
špatná	špatný	k2eAgFnSc1d1	špatná
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
když	když	k8xS	když
Vernon	Vernon	k1gMnSc1	Vernon
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1938	[number]	k4	1938
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
k	k	k7c3	k
dvouletému	dvouletý	k2eAgInSc3d1	dvouletý
žaláři	žalář	k1gInSc3	žalář
za	za	k7c4	za
padělání	padělání	k1gNnSc4	padělání
směnky	směnka	k1gFnSc2	směnka
a	a	k8xC	a
rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
musela	muset	k5eAaImAgFnS	muset
vystěhovat	vystěhovat	k5eAaPmF	vystěhovat
z	z	k7c2	z
bytu	byt	k1gInSc2	byt
pořízeného	pořízený	k2eAgInSc2d1	pořízený
na	na	k7c4	na
splátky	splátka	k1gFnPc4	splátka
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
kritické	kritický	k2eAgFnSc6d1	kritická
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Gladys	Gladys	k1gInSc1	Gladys
s	s	k7c7	s
malým	malý	k1gMnSc7	malý
Elvisem	Elvis	k1gMnSc7	Elvis
putovala	putovat	k5eAaImAgFnS	putovat
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
rodině	rodina	k1gFnSc6	rodina
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nepřirozená	přirozený	k2eNgFnSc1d1	nepřirozená
Elvisova	Elvisův	k2eAgFnSc1d1	Elvisova
fixace	fixace	k1gFnSc1	fixace
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
matku	matka	k1gFnSc4	matka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
se	se	k3xPyFc4	se
Elvis	Elvis	k1gMnSc1	Elvis
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
pěvecké	pěvecký	k2eAgFnPc4d1	pěvecká
soutěže	soutěž	k1gFnPc4	soutěž
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
"	"	kIx"	"
<g/>
Old	Olda	k1gFnPc2	Olda
Shep	Shep	k1gInSc1	Shep
<g/>
"	"	kIx"	"
a	a	k8xC	a
opět	opět	k6eAd1	opět
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
nutné	nutný	k2eAgNnSc1d1	nutné
popřít	popřít	k5eAaPmF	popřít
legendu	legenda	k1gFnSc4	legenda
tvrdící	tvrdící	k2eAgFnSc4d1	tvrdící
<g/>
,	,	kIx,	,
že	že	k8xS	že
malý	malý	k2eAgMnSc1d1	malý
zpěvák	zpěvák	k1gMnSc1	zpěvák
tuto	tento	k3xDgFnSc4	tento
soutěž	soutěž	k1gFnSc4	soutěž
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
skončil	skončit	k5eAaPmAgMnS	skončit
pátý	pátý	k4xOgMnSc1	pátý
a	a	k8xC	a
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
druhou	druhý	k4xOgFnSc4	druhý
cenu	cena	k1gFnSc4	cena
–	–	k?	–
volnou	volný	k2eAgFnSc4d1	volná
celodenní	celodenní	k2eAgFnSc4d1	celodenní
vstupenku	vstupenka	k1gFnSc4	vstupenka
na	na	k7c4	na
kolotoče	kolotoč	k1gInPc4	kolotoč
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
dostává	dostávat	k5eAaImIp3nS	dostávat
k	k	k7c3	k
svým	svůj	k3xOyFgMnPc3	svůj
11	[number]	k4	11
<g/>
.	.	kIx.	.
narozeninám	narozeniny	k1gFnPc3	narozeniny
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
kytaru	kytara	k1gFnSc4	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInPc1d1	základní
akordy	akord	k1gInPc1	akord
Elvise	Elvis	k1gMnSc2	Elvis
naučil	naučit	k5eAaPmAgMnS	naučit
strýc	strýc	k1gMnSc1	strýc
Vester	Vester	k1gMnSc1	Vester
<g/>
,	,	kIx,	,
hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
výuky	výuka	k1gFnSc2	výuka
se	se	k3xPyFc4	se
však	však	k9	však
pro	pro	k7c4	pro
budoucího	budoucí	k2eAgMnSc4d1	budoucí
krále	král	k1gMnSc4	král
rock	rock	k1gInSc4	rock
and	and	k?	and
rollu	roll	k1gInSc2	roll
stal	stát	k5eAaPmAgInS	stát
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
poslech	poslech	k1gInSc1	poslech
bluesových	bluesový	k2eAgFnPc2d1	bluesová
a	a	k8xC	a
country	country	k2eAgFnPc2d1	country
nahrávek	nahrávka	k1gFnPc2	nahrávka
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterým	který	k3yQgFnPc3	který
si	se	k3xPyFc3	se
Elvis	Elvis	k1gMnSc1	Elvis
vypiloval	vypilovat	k5eAaPmAgMnS	vypilovat
bezchybný	bezchybný	k2eAgInSc4d1	bezchybný
hudební	hudební	k2eAgInSc4d1	hudební
sluch	sluch	k1gInSc4	sluch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
rodina	rodina	k1gFnSc1	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Memphisu	Memphis	k1gInSc2	Memphis
s	s	k7c7	s
vyhlídkou	vyhlídka	k1gFnSc7	vyhlídka
na	na	k7c4	na
lepší	dobrý	k2eAgFnSc4d2	lepší
budoucnost	budoucnost	k1gFnSc4	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Elvis	Elvis	k1gMnSc1	Elvis
zde	zde	k6eAd1	zde
neměl	mít	k5eNaImAgMnS	mít
mnoho	mnoho	k4c4	mnoho
přátel	přítel	k1gMnPc2	přítel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hudba	hudba	k1gFnSc1	hudba
ho	on	k3xPp3gInSc4	on
očarovala	očarovat	k5eAaPmAgFnS	očarovat
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
plně	plně	k6eAd1	plně
vynahradila	vynahradit	k5eAaPmAgFnS	vynahradit
absenci	absence	k1gFnSc4	absence
běžných	běžný	k2eAgInPc2d1	běžný
klukovských	klukovský	k2eAgInPc2d1	klukovský
zážitků	zážitek	k1gInPc2	zážitek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1953	[number]	k4	1953
Elvis	Elvis	k1gMnSc1	Elvis
ukončil	ukončit	k5eAaPmAgMnS	ukončit
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jako	jako	k9	jako
šofér	šofér	k1gMnSc1	šofér
do	do	k7c2	do
firmy	firma	k1gFnSc2	firma
Electric	Electrice	k1gFnPc2	Electrice
Co	co	k9	co
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1953	[number]	k4	1953
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
studia	studio	k1gNnSc2	studio
Sun	Sun	kA	Sun
Records	Records	k1gInSc1	Records
<g/>
,	,	kIx,	,
vlastněného	vlastněný	k2eAgInSc2d1	vlastněný
Samem	Samos	k1gInSc7	Samos
Philipsem	Philips	k1gInSc7	Philips
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
nahrál	nahrát	k5eAaPmAgMnS	nahrát
Elvis	Elvis	k1gMnSc1	Elvis
tehdy	tehdy	k6eAd1	tehdy
populární	populární	k2eAgFnSc2d1	populární
balady	balada	k1gFnSc2	balada
My	my	k3xPp1nPc1	my
Happiness	Happinessa	k1gFnPc2	Happinessa
a	a	k8xC	a
That	Thata	k1gFnPc2	Thata
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
When	When	k1gMnSc1	When
Your	Your	k1gMnSc1	Your
Heartaches	Heartaches	k1gMnSc1	Heartaches
Begin	Begin	k1gMnSc1	Begin
v	v	k7c6	v
Sun	Sun	kA	Sun
Records	Records	k1gInSc4	Records
jako	jako	k8xS	jako
dárek	dárek	k1gInSc4	dárek
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
matku	matka	k1gFnSc4	matka
<g/>
.	.	kIx.	.
</s>
<s>
Tahle	tenhle	k3xDgFnSc1	tenhle
legenda	legenda	k1gFnSc1	legenda
se	se	k3xPyFc4	se
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
nezakládá	zakládat	k5eNaImIp3nS	zakládat
na	na	k7c6	na
pravdě	pravda	k1gFnSc6	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Malou	malý	k2eAgFnSc4d1	malá
desku	deska	k1gFnSc4	deska
Elvis	Elvis	k1gMnSc1	Elvis
nahrál	nahrát	k5eAaPmAgMnS	nahrát
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
čtyři	čtyři	k4xCgInPc4	čtyři
měsíce	měsíc	k1gInPc4	měsíc
od	od	k7c2	od
narozenin	narozeniny	k1gFnPc2	narozeniny
jeho	jeho	k3xOp3gFnSc2	jeho
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
Pravý	pravý	k2eAgInSc4d1	pravý
důvod	důvod	k1gInSc4	důvod
byl	být	k5eAaImAgMnS	být
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Elvis	Elvis	k1gMnSc1	Elvis
chtěl	chtít	k5eAaImAgMnS	chtít
otestovat	otestovat	k5eAaPmF	otestovat
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
vlastně	vlastně	k9	vlastně
zní	znět	k5eAaImIp3nS	znět
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
první	první	k4xOgInPc4	první
vydělané	vydělaný	k2eAgInPc4d1	vydělaný
peníze	peníz	k1gInPc4	peníz
koupil	koupit	k5eAaPmAgMnS	koupit
své	svůj	k3xOyFgFnSc3	svůj
matce	matka	k1gFnSc3	matka
růžový	růžový	k2eAgInSc1d1	růžový
Cadillac	cadillac	k1gInSc1	cadillac
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
dárku	dárek	k1gInSc6	dárek
pro	pro	k7c4	pro
matku	matka	k1gFnSc4	matka
si	se	k3xPyFc3	se
později	pozdě	k6eAd2	pozdě
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
Elvisův	Elvisův	k2eAgMnSc1d1	Elvisův
manažér	manažér	k1gMnSc1	manažér
plukovník	plukovník	k1gMnSc1	plukovník
Parker	Parker	k1gMnSc1	Parker
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
Elvise	Elvis	k1gMnPc4	Elvis
při	při	k7c6	při
zpěvu	zpěv	k1gInSc6	zpěv
všiml	všimnout	k5eAaPmAgMnS	všimnout
sám	sám	k3xTgMnSc1	sám
majitel	majitel	k1gMnSc1	majitel
společnosti	společnost	k1gFnSc2	společnost
Sam	Sam	k1gMnSc1	Sam
Phillips	Phillips	k1gInSc1	Phillips
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pravda	pravda	k1gFnSc1	pravda
je	být	k5eAaImIp3nS	být
jiná	jiný	k2eAgFnSc1d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
jeho	on	k3xPp3gInSc4	on
talent	talent	k1gInSc4	talent
byla	být	k5eAaImAgFnS	být
Samova	Samův	k2eAgFnSc1d1	Samova
sekretářka	sekretářka	k1gFnSc1	sekretářka
<g/>
.	.	kIx.	.
</s>
<s>
Phillips	Phillips	k6eAd1	Phillips
tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
hledal	hledat	k5eAaImAgInS	hledat
"	"	kIx"	"
<g/>
bělocha	běloch	k1gMnSc4	běloch
s	s	k7c7	s
černošským	černošský	k2eAgInSc7d1	černošský
hlasem	hlas	k1gInSc7	hlas
<g/>
"	"	kIx"	"
a	a	k8xC	a
rozpoznal	rozpoznat	k5eAaPmAgMnS	rozpoznat
Elvisův	Elvisův	k2eAgInSc4d1	Elvisův
zázračný	zázračný	k2eAgInSc4d1	zázračný
potenciál	potenciál	k1gInSc4	potenciál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1954	[number]	k4	1954
nahráli	nahrát	k5eAaBmAgMnP	nahrát
bluesovou	bluesový	k2eAgFnSc4d1	bluesová
píseň	píseň	k1gFnSc4	píseň
That	Thata	k1gFnPc2	Thata
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
All	All	k1gFnSc7	All
Right	Right	k1gMnSc1	Right
(	(	kIx(	(
<g/>
Mama	mama	k1gFnSc1	mama
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
vysílanou	vysílaný	k2eAgFnSc7d1	vysílaná
poprvé	poprvé	k6eAd1	poprvé
Memfiským	Memfiský	k2eAgInSc7d1	Memfiský
rozhlasem	rozhlas	k1gInSc7	rozhlas
7	[number]	k4	7
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
původně	původně	k6eAd1	původně
bluegrassovou	bluegrassový	k2eAgFnSc4d1	bluegrassová
Blue	Blue	k1gFnSc4	Blue
Moon	Moono	k1gNnPc2	Moono
of	of	k?	of
Kentucky	Kentucka	k1gFnSc2	Kentucka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
ovšem	ovšem	k9	ovšem
velký	velký	k2eAgInSc1d1	velký
problém	problém	k1gInSc1	problém
prorazit	prorazit	k5eAaPmF	prorazit
s	s	k7c7	s
černošskou	černošský	k2eAgFnSc7d1	černošská
hudbou	hudba	k1gFnSc7	hudba
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
časem	čas	k1gInSc7	čas
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
podařilo	podařit	k5eAaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
Elvisovým	Elvisův	k2eAgInSc7d1	Elvisův
singlem	singl	k1gInSc7	singl
bylo	být	k5eAaImAgNnS	být
Good	Good	k1gMnSc1	Good
Rockin	Rockin	k1gMnSc1	Rockin
<g/>
'	'	kIx"	'
Tonight	Tonight	k1gMnSc1	Tonight
společně	společně	k6eAd1	společně
s	s	k7c7	s
I	I	kA	I
Don	Don	k1gInSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Care	car	k1gMnSc5	car
if	if	k?	if
the	the	k?	the
Sun	Sun	kA	Sun
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Shine	Shin	k1gInSc5	Shin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1955	[number]	k4	1955
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc2	jeho
kariéry	kariéra	k1gFnSc2	kariéra
ujal	ujmout	k5eAaPmAgMnS	ujmout
nový	nový	k2eAgMnSc1d1	nový
manažer	manažer	k1gMnSc1	manažer
Tom	Tom	k1gMnSc1	Tom
Parker	Parker	k1gMnSc1	Parker
a	a	k8xC	a
události	událost	k1gFnPc1	událost
nabraly	nabrat	k5eAaPmAgFnP	nabrat
rychlý	rychlý	k2eAgInSc4d1	rychlý
spád	spád	k1gInSc4	spád
<g/>
.	.	kIx.	.
</s>
<s>
Elvis	Elvis	k1gMnSc1	Elvis
přešel	přejít	k5eAaPmAgMnS	přejít
k	k	k7c3	k
velké	velký	k2eAgFnSc3d1	velká
nahrávací	nahrávací	k2eAgFnSc3d1	nahrávací
společnosti	společnost	k1gFnSc3	společnost
RCA	RCA	kA	RCA
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
nazpíval	nazpívat	k5eAaPmAgInS	nazpívat
první	první	k4xOgInSc1	první
singl	singl	k1gInSc1	singl
pro	pro	k7c4	pro
RCA	RCA	kA	RCA
Records	Records	k1gInSc4	Records
Heartbreak	Heartbreak	k1gInSc1	Heartbreak
Hotel	hotel	k1gInSc1	hotel
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
prodalo	prodat	k5eAaPmAgNnS	prodat
přes	přes	k7c4	přes
milion	milion	k4xCgInSc4	milion
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ledna	leden	k1gInSc2	leden
1956	[number]	k4	1956
se	se	k3xPyFc4	se
také	také	k9	také
začal	začít	k5eAaPmAgInS	začít
objevovat	objevovat	k5eAaImF	objevovat
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
známé	známý	k2eAgNnSc1d1	známé
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
vystoupení	vystoupení	k1gNnSc1	vystoupení
před	před	k7c7	před
kamerou	kamera	k1gFnSc7	kamera
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1956	[number]	k4	1956
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
Hound	Hounda	k1gFnPc2	Hounda
Dog	doga	k1gFnPc2	doga
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
pořádně	pořádně	k6eAd1	pořádně
"	"	kIx"	"
<g/>
stát	stát	k5eAaPmF	stát
na	na	k7c6	na
značce	značka	k1gFnSc6	značka
<g/>
"	"	kIx"	"
a	a	k8xC	a
poněkud	poněkud	k6eAd1	poněkud
improvizoval	improvizovat	k5eAaImAgMnS	improvizovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
vystoupil	vystoupit	k5eAaPmAgInS	vystoupit
dokonce	dokonce	k9	dokonce
ve	v	k7c6	v
slavné	slavný	k2eAgFnSc6d1	slavná
Ed	Ed	k1gFnSc6	Ed
Sullivan	Sullivan	k1gMnSc1	Sullivan
Show	show	k1gFnSc1	show
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
prvním	první	k4xOgMnSc7	první
filmem	film	k1gInSc7	film
byl	být	k5eAaImAgInS	být
Love	lov	k1gInSc5	lov
Me	Me	k1gMnSc1	Me
Tender	tender	k1gInSc1	tender
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gInPc4	jeho
nejlepší	dobrý	k2eAgInPc4d3	nejlepší
filmy	film	k1gInPc4	film
patří	patřit	k5eAaImIp3nS	patřit
Jailhouse	Jailhouse	k1gFnSc1	Jailhouse
Rock	rock	k1gInSc1	rock
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
a	a	k8xC	a
King	King	k1gMnSc1	King
Creole	Creole	k1gFnSc2	Creole
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
obdržel	obdržet	k5eAaPmAgInS	obdržet
předvolání	předvolání	k1gNnSc4	předvolání
k	k	k7c3	k
nástupu	nástup	k1gInSc3	nástup
na	na	k7c4	na
vojnu	vojna	k1gFnSc4	vojna
<g/>
.	.	kIx.	.
</s>
<s>
Díky	dík	k1gInPc1	dík
odkladu	odklad	k1gInSc2	odklad
nástupu	nástup	k1gInSc2	nástup
do	do	k7c2	do
března	březen	k1gInSc2	březen
1958	[number]	k4	1958
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
dokončit	dokončit	k5eAaPmF	dokončit
film	film	k1gInSc4	film
King	King	k1gMnSc1	King
Creole	Creole	k1gFnSc2	Creole
a	a	k8xC	a
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1958	[number]	k4	1958
Elvis	Elvis	k1gMnSc1	Elvis
narukoval	narukovat	k5eAaPmAgMnS	narukovat
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
umístěn	umístit	k5eAaPmNgMnS	umístit
do	do	k7c2	do
Friedbergu	Friedberg	k1gInSc2	Friedberg
v	v	k7c6	v
Západním	západní	k2eAgNnSc6d1	západní
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
služby	služba	k1gFnSc2	služba
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
poznal	poznat	k5eAaPmAgMnS	poznat
svoji	svůj	k3xOyFgFnSc4	svůj
budoucí	budoucí	k2eAgFnSc4d1	budoucí
manželku	manželka	k1gFnSc4	manželka
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
čtrnáctiletou	čtrnáctiletý	k2eAgFnSc4d1	čtrnáctiletá
Priscillu	Priscilla	k1gFnSc4	Priscilla
Beaulieu	Beaulieus	k1gInSc2	Beaulieus
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1960	[number]	k4	1960
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
v	v	k7c6	v
témž	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
natáčí	natáčet	k5eAaImIp3nP	natáčet
dva	dva	k4xCgInPc1	dva
celovečerní	celovečerní	k2eAgInPc1d1	celovečerní
filmy	film	k1gInPc1	film
G.	G.	kA	G.
I.	I.	kA	I.
Blues	blues	k1gInSc4	blues
a	a	k8xC	a
Flaming	Flaming	k1gInSc4	Flaming
Star	Star	kA	Star
se	s	k7c7	s
stejnojmennou	stejnojmenný	k2eAgFnSc7d1	stejnojmenná
písní	píseň	k1gFnSc7	píseň
(	(	kIx(	(
<g/>
Flaming	Flaming	k1gInSc1	Flaming
star	star	k1gFnPc2	star
<g/>
,	,	kIx,	,
don	dona	k1gFnPc2	dona
<g/>
́	́	k?	́
<g/>
t	t	k?	t
shining	shining	k1gInSc1	shining
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
vojny	vojna	k1gFnSc2	vojna
je	být	k5eAaImIp3nS	být
rock	rock	k1gInSc4	rock
and	and	k?	and
roll	rolnout	k5eAaPmAgMnS	rolnout
na	na	k7c6	na
ústupu	ústup	k1gInSc6	ústup
a	a	k8xC	a
Presley	Preslea	k1gFnPc1	Preslea
se	se	k3xPyFc4	se
obrací	obracet	k5eAaImIp3nP	obracet
k	k	k7c3	k
pomalým	pomalý	k2eAgFnPc3d1	pomalá
baladám	balada	k1gFnPc3	balada
(	(	kIx(	(
<g/>
It	It	k1gFnSc1	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Now	Now	k1gFnSc7	Now
or	or	k?	or
Never	Never	k1gMnSc1	Never
–	–	k?	–
píseň	píseň	k1gFnSc4	píseň
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
známé	známý	k2eAgFnSc2d1	známá
italské	italský	k2eAgFnSc2d1	italská
O	o	k7c6	o
sole	sol	k1gInSc5	sol
mio	mio	k?	mio
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
NBC	NBC	kA	NBC
nazvaném	nazvaný	k2eAgNnSc6d1	nazvané
Welcome	Welcom	k1gInSc5	Welcom
Home	Hom	k1gMnPc4	Hom
Elvis	Elvis	k1gMnSc1	Elvis
společně	společně	k6eAd1	společně
s	s	k7c7	s
Frankem	frank	k1gInSc7	frank
Sinatrou	Sinatra	k1gFnSc7	Sinatra
a	a	k8xC	a
zpívají	zpívat	k5eAaImIp3nP	zpívat
mix	mix	k1gInSc4	mix
písní	píseň	k1gFnPc2	píseň
Witchcraft	Witchcrafta	k1gFnPc2	Witchcrafta
<g/>
/	/	kIx~	/
<g/>
Love	lov	k1gInSc5	lov
Me	Me	k1gMnSc4	Me
Tender	tender	k1gInSc4	tender
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
v	v	k7c6	v
natáčení	natáčení	k1gNnSc6	natáčení
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
nesplňovaly	splňovat	k5eNaImAgInP	splňovat
Elvisova	Elvisův	k2eAgInSc2d1	Elvisův
očekávaní	očekávaný	k2eAgMnPc1d1	očekávaný
a	a	k8xC	a
staly	stát	k5eAaPmAgInP	stát
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
spíše	spíše	k9	spíše
"	"	kIx"	"
<g/>
lacinější	laciný	k2eAgInPc1d2	lacinější
<g/>
"	"	kIx"	"
muzikály	muzikál	k1gInPc1	muzikál
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
natočil	natočit	k5eAaBmAgMnS	natočit
ještě	ještě	k9	ještě
dalších	další	k2eAgInPc2d1	další
24	[number]	k4	24
celovečerních	celovečerní	k2eAgInPc2d1	celovečerní
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
ale	ale	k8xC	ale
točí	točit	k5eAaImIp3nS	točit
filmy	film	k1gInPc4	film
a	a	k8xC	a
soundtracky	soundtrack	k1gInPc4	soundtrack
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jde	jít	k5eAaImIp3nS	jít
spíše	spíše	k9	spíše
o	o	k7c4	o
kvantitu	kvantita	k1gFnSc4	kvantita
než	než	k8xS	než
o	o	k7c4	o
kvalitu	kvalita	k1gFnSc4	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
popularita	popularita	k1gFnSc1	popularita
Beatles	Beatles	k1gFnPc2	Beatles
a	a	k8xC	a
jiných	jiný	k1gMnPc2	jiný
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
,	,	kIx,	,
Elvisova	Elvisův	k2eAgFnSc1d1	Elvisova
popularita	popularita	k1gFnSc1	popularita
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
proto	proto	k8xC	proto
poněkud	poněkud	k6eAd1	poněkud
ustupoval	ustupovat	k5eAaImAgMnS	ustupovat
ze	z	k7c2	z
slávy	sláva	k1gFnSc2	sláva
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
vrátil	vrátit	k5eAaPmAgMnS	vrátit
na	na	k7c6	na
výsluní	výsluní	k1gNnSc6	výsluní
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
osmileté	osmiletý	k2eAgFnSc6d1	osmiletá
odmlce	odmlka	k1gFnSc6	odmlka
konečně	konečně	k6eAd1	konečně
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
zcela	zcela	k6eAd1	zcela
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
jemu	on	k3xPp3gNnSc3	on
věnovaný	věnovaný	k2eAgInSc4d1	věnovaný
pořad	pořad	k1gInSc4	pořad
"	"	kIx"	"
<g/>
'	'	kIx"	'
<g/>
68	[number]	k4	68
Comeback	Comeback	k1gMnSc1	Comeback
Special	Special	k1gMnSc1	Special
<g/>
"	"	kIx"	"
vysílaný	vysílaný	k2eAgInSc1d1	vysílaný
NBC	NBC	kA	NBC
musel	muset	k5eAaImAgInS	muset
uchvátit	uchvátit	k5eAaPmF	uchvátit
snad	snad	k9	snad
každého	každý	k3xTgNnSc2	každý
<g/>
,	,	kIx,	,
šlo	jít	k5eAaImAgNnS	jít
bez	bez	k7c2	bez
pochyby	pochyba	k1gFnSc2	pochyba
o	o	k7c4	o
Presleyho	Presley	k1gMnSc4	Presley
nejzdařilejší	zdařilý	k2eAgNnSc1d3	nejzdařilejší
vystoupení	vystoupení	k1gNnSc1	vystoupení
před	před	k7c7	před
televizními	televizní	k2eAgFnPc7d1	televizní
kamerami	kamera	k1gFnPc7	kamera
<g/>
.	.	kIx.	.
</s>
<s>
Rázem	rázem	k6eAd1	rázem
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
byl	být	k5eAaImAgMnS	být
opět	opět	k6eAd1	opět
vládce	vládce	k1gMnSc1	vládce
showbyznysu	showbyznys	k1gInSc2	showbyznys
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
byl	být	k5eAaImAgInS	být
jak	jak	k8xC	jak
soundtrack	soundtrack	k1gInSc1	soundtrack
z	z	k7c2	z
jeho	on	k3xPp3gInSc2	on
comebacku	comeback	k1gInSc2	comeback
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
If	If	k1gFnSc1	If
I	i	k8xC	i
Can	Can	k1gFnSc1	Can
Dream	Dream	k1gInSc1	Dream
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
opět	opět	k6eAd1	opět
vystupovat	vystupovat	k5eAaImF	vystupovat
na	na	k7c4	na
živo	živ	k2eAgNnSc4d1	živo
<g/>
,	,	kIx,	,
zkrátka	zkrátka	k6eAd1	zkrátka
návrat	návrat	k1gInSc4	návrat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
patří	patřit	k5eAaImIp3nS	patřit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sedmi	sedm	k4xCc6	sedm
letech	let	k1gInPc6	let
mimo	mimo	k7c4	mimo
žebříčky	žebříček	k1gInPc4	žebříček
popularity	popularita	k1gFnSc2	popularita
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
dostává	dostávat	k5eAaImIp3nS	dostávat
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
Suspicious	Suspicious	k1gMnSc1	Suspicious
Minds	Minds	k1gInSc1	Minds
<g/>
,	,	kIx,	,
v	v	k7c6	v
Západním	západní	k2eAgNnSc6d1	západní
Německu	Německo	k1gNnSc6	Německo
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
In	In	k1gFnPc2	In
the	the	k?	the
Ghetto	ghetto	k1gNnSc4	ghetto
a	a	k8xC	a
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
s	s	k7c7	s
The	The	k1gFnSc7	The
Wonder	Wonder	k1gMnSc1	Wonder
of	of	k?	of
You	You	k1gMnSc1	You
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
nahrává	nahrávat	k5eAaImIp3nS	nahrávat
populární	populární	k2eAgFnSc1d1	populární
píseň	píseň	k1gFnSc1	píseň
Burning	Burning	k1gInSc1	Burning
Love	lov	k1gInSc5	lov
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
hitparády	hitparáda	k1gFnSc2	hitparáda
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1973	[number]	k4	1973
se	se	k3xPyFc4	se
Elvis	Elvis	k1gMnSc1	Elvis
rozvádí	rozvádět	k5eAaImIp3nS	rozvádět
s	s	k7c7	s
Priscillou	Priscilla	k1gFnSc7	Priscilla
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
odchází	odcházet	k5eAaImIp3nP	odcházet
i	i	k9	i
jejich	jejich	k3xOp3gMnPc3	jejich
dcera	dcera	k1gFnSc1	dcera
Lisa	Lisa	k1gFnSc1	Lisa
Marie	Marie	k1gFnSc1	Marie
<g/>
,	,	kIx,	,
přibírá	přibírat	k5eAaImIp3nS	přibírat
na	na	k7c6	na
váze	váha	k1gFnSc6	váha
<g/>
,	,	kIx,	,
přehání	přehánět	k5eAaImIp3nP	přehánět
to	ten	k3xDgNnSc4	ten
s	s	k7c7	s
léky	lék	k1gInPc7	lék
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
stále	stále	k6eAd1	stále
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
a	a	k8xC	a
nahrává	nahrávat	k5eAaImIp3nS	nahrávat
písně	píseň	k1gFnPc4	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1969	[number]	k4	1969
a	a	k8xC	a
1977	[number]	k4	1977
absolvuje	absolvovat	k5eAaPmIp3nS	absolvovat
1094	[number]	k4	1094
zcela	zcela	k6eAd1	zcela
vyprodaných	vyprodaný	k2eAgNnPc2d1	vyprodané
vystoupení	vystoupení	k1gNnPc2	vystoupení
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
prvním	první	k4xOgMnSc7	první
umělcem	umělec	k1gMnSc7	umělec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ve	v	k7c6	v
čtyřech	čtyři	k4xCgNnPc6	čtyři
vystoupeních	vystoupení	k1gNnPc6	vystoupení
za	za	k7c7	za
sebou	se	k3xPyFc7	se
vyprodal	vyprodat	k5eAaPmAgMnS	vyprodat
Madison	Madison	k1gInSc4	Madison
Square	square	k1gInSc1	square
Garden	Gardna	k1gFnPc2	Gardna
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
ve	v	k7c6	v
dnech	den	k1gInPc6	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úterý	úterý	k1gNnSc6	úterý
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1977	[number]	k4	1977
je	být	k5eAaImIp3nS	být
nalezen	nalezen	k2eAgMnSc1d1	nalezen
svojí	svůj	k3xOyFgFnSc7	svůj
snoubenkou	snoubenka	k1gFnSc7	snoubenka
Ginger	Ginger	k1gInSc4	Ginger
Aldenovou	Aldenová	k1gFnSc4	Aldenová
mrtev	mrtev	k2eAgInSc1d1	mrtev
na	na	k7c6	na
podlaze	podlaha	k1gFnSc6	podlaha
koupelny	koupelna	k1gFnSc2	koupelna
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
domě	dům	k1gInSc6	dům
nazvaném	nazvaný	k2eAgNnSc6d1	nazvané
Graceland	Gracelando	k1gNnPc2	Gracelando
v	v	k7c6	v
Memphisu	Memphis	k1gInSc6	Memphis
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
lidé	člověk	k1gMnPc1	člověk
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c6	na
předávkování	předávkování	k1gNnSc6	předávkování
drogami	droga	k1gFnPc7	droga
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
verze	verze	k1gFnSc1	verze
se	se	k3xPyFc4	se
však	však	k9	však
přiklání	přiklánět	k5eAaImIp3nS	přiklánět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
příčinou	příčina	k1gFnSc7	příčina
smrti	smrt	k1gFnSc2	smrt
byl	být	k5eAaImAgInS	být
srdeční	srdeční	k2eAgInSc4d1	srdeční
infarkt	infarkt	k1gInSc4	infarkt
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc1	jeho
sídlo	sídlo	k1gNnSc1	sídlo
Graceland	Gracelanda	k1gFnPc2	Gracelanda
proměnilo	proměnit	k5eAaPmAgNnS	proměnit
v	v	k7c6	v
muzeum	muzeum	k1gNnSc1	muzeum
a	a	k8xC	a
stalo	stát	k5eAaPmAgNnS	stát
se	s	k7c7	s
druhým	druhý	k4xOgInSc7	druhý
nejnavštěvovanějším	navštěvovaný	k2eAgInSc7d3	nejnavštěvovanější
domem	dům	k1gInSc7	dům
v	v	k7c6	v
USA	USA	kA	USA
hned	hned	k6eAd1	hned
po	po	k7c6	po
Bílém	bílý	k2eAgInSc6d1	bílý
domě	dům	k1gInSc6	dům
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
sjíždějí	sjíždět	k5eAaImIp3nP	sjíždět
davy	dav	k1gInPc1	dav
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
,	,	kIx,	,
Elvisovi	Elvisův	k2eAgMnPc1d1	Elvisův
napodobitelé	napodobitel	k1gMnPc1	napodobitel
a	a	k8xC	a
dvojníci	dvojník	k1gMnPc1	dvojník
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
písně	píseň	k1gFnPc1	píseň
vycházejí	vycházet	k5eAaImIp3nP	vycházet
stále	stále	k6eAd1	stále
znovu	znovu	k6eAd1	znovu
a	a	k8xC	a
mnozí	mnohý	k2eAgMnPc1d1	mnohý
lidé	člověk	k1gMnPc1	člověk
dodnes	dodnes	k6eAd1	dodnes
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nezemřel	zemřít	k5eNaPmAgMnS	zemřít
a	a	k8xC	a
jednou	jeden	k4xCgFnSc7	jeden
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nP	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
zpěváků	zpěvák	k1gMnPc2	zpěvák
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgInSc1d1	vlastní
článek	článek	k1gInSc1	článek
<g/>
:	:	kIx,	:
Diskografie	diskografie	k1gFnSc2	diskografie
Elvise	Elvis	k1gMnSc4	Elvis
Presleyho	Presley	k1gMnSc4	Presley
1956	[number]	k4	1956
–	–	k?	–
Love	lov	k1gInSc5	lov
Me	Me	k1gFnSc3	Me
Tender	tender	k1gInSc1	tender
1957	[number]	k4	1957
–	–	k?	–
Jailhouse	Jailhous	k1gInSc6	Jailhous
Rock	rock	k1gInSc1	rock
/	/	kIx~	/
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Vězeňský	vězeňský	k2eAgInSc1d1	vězeňský
Rock	rock	k1gInSc1	rock
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
1957	[number]	k4	1957
–	–	k?	–
I	i	k8xC	i
cant	canto	k1gNnPc2	canto
help	help	k1gInSc4	help
1958	[number]	k4	1958
–	–	k?	–
King	King	k1gInSc1	King
Creole	Creole	k1gFnSc1	Creole
1960	[number]	k4	1960
–	–	k?	–
G.	G.	kA	G.
I.	I.	kA	I.
Blues	blues	k1gFnSc1	blues
1960	[number]	k4	1960
–	–	k?	–
Flaming	Flaming	k1gInSc1	Flaming
Star	Star	kA	Star
1961	[number]	k4	1961
–	–	k?	–
Wild	Wild	k1gMnSc1	Wild
in	in	k?	in
the	the	k?	the
<g />
.	.	kIx.	.
</s>
<s>
Country	country	k2eAgMnSc1d1	country
1961	[number]	k4	1961
–	–	k?	–
Blue	Blue	k1gFnSc1	Blue
Hawaii	Hawaium	k1gNnPc7	Hawaium
/	/	kIx~	/
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Elvis	Elvis	k1gMnSc1	Elvis
Presley	Preslea	k1gFnSc2	Preslea
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Modrý	modrý	k2eAgInSc1d1	modrý
Hawaii	Hawaie	k1gFnSc3	Hawaie
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
1962	[number]	k4	1962
–	–	k?	–
Follow	Follow	k1gMnSc2	Follow
That	That	k2eAgInSc4d1	That
Dream	Dream	k1gInSc4	Dream
/	/	kIx~	/
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Jdi	jít	k5eAaImRp2nS	jít
za	za	k7c7	za
svým	svůj	k3xOyFgInSc7	svůj
snem	sen	k1gInSc7	sen
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
1962	[number]	k4	1962
–	–	k?	–
Kid	Kid	k1gFnSc1	Kid
Galahad	Galahad	k1gInSc1	Galahad
1962	[number]	k4	1962
–	–	k?	–
Girls	girl	k1gFnPc2	girl
<g/>
!	!	kIx.	!
</s>
<s>
Girls	girl	k1gFnPc1	girl
<g/>
!	!	kIx.	!
</s>
<s>
Girls	girl	k1gFnPc1	girl
<g/>
!	!	kIx.	!
</s>
<s>
/	/	kIx~	/
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Elvis	Elvis	k1gMnSc1	Elvis
Presley	Preslea	k1gFnSc2	Preslea
<g/>
:	:	kIx,	:
Girls	girl	k1gFnPc2	girl
<g/>
!	!	kIx.	!
</s>
<s>
Girls	girl	k1gFnPc1	girl
<g/>
!	!	kIx.	!
</s>
<s>
Girls	girl	k1gFnPc1	girl
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
1963	[number]	k4	1963
–	–	k?	–
It	It	k1gMnSc1	It
Happened	Happened	k1gMnSc1	Happened
at	at	k?	at
the	the	k?	the
World	World	k1gInSc1	World
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Fair	fair	k2eAgFnPc7d1	fair
/	/	kIx~	/
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
na	na	k7c6	na
světové	světový	k2eAgFnSc6d1	světová
výstavě	výstava	k1gFnSc6	výstava
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
1963	[number]	k4	1963
–	–	k?	–
Fun	Fun	k1gMnSc1	Fun
in	in	k?	in
Acapulco	Acapulco	k1gMnSc1	Acapulco
/	/	kIx~	/
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Elvis	Elvis	k1gMnSc1	Elvis
Presley	Preslea	k1gMnSc2	Preslea
<g/>
:	:	kIx,	:
Fun	Fun	k1gMnSc1	Fun
in	in	k?	in
Acapulco	Acapulco	k1gMnSc1	Acapulco
<g/>
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
1964	[number]	k4	1964
–	–	k?	–
Kissin	Kissin	k1gInSc1	Kissin
<g/>
'	'	kIx"	'
Cousins	Cousins	k1gInSc1	Cousins
1964	[number]	k4	1964
–	–	k?	–
Viva	Viv	k1gInSc2	Viv
Las	laso	k1gNnPc2	laso
Vegas	Vegas	k1gInSc1	Vegas
/	/	kIx~	/
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Elvis	Elvis	k1gMnSc1	Elvis
<g/>
:	:	kIx,	:
Viva	Viva	k1gMnSc1	Viva
Las	laso	k1gNnPc2	laso
Vegas	Vegas	k1gInSc1	Vegas
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
1964	[number]	k4	1964
–	–	k?	–
Roustabout	Roustabout	k1gMnSc1	Roustabout
/	/	kIx~	/
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Elvis	Elvis	k1gMnSc1	Elvis
Presley	Preslea	k1gMnSc2	Preslea
<g/>
:	:	kIx,	:
Roustabout	Roustabout	k1gMnSc1	Roustabout
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
1965	[number]	k4	1965
–	–	k?	–
Tickle	Tickl	k1gInSc5	Tickl
Me	Me	k1gMnSc4	Me
1965	[number]	k4	1965
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Girl	girl	k1gFnSc1	girl
Happy	Happa	k1gFnSc2	Happa
1965	[number]	k4	1965
–	–	k?	–
Harum	Harum	k1gInSc1	Harum
Scarum	Scarum	k1gInSc1	Scarum
/	/	kIx~	/
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Elvis	Elvis	k1gMnSc1	Elvis
<g/>
:	:	kIx,	:
Harum	Harum	k1gInSc1	Harum
Scarum	Scarum	k1gNnSc1	Scarum
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
1965	[number]	k4	1965
–	–	k?	–
Paradise	Paradise	k1gFnSc1	Paradise
<g/>
,	,	kIx,	,
Hawaiian	Hawaiian	k1gInSc1	Hawaiian
Style	styl	k1gInSc5	styl
/	/	kIx~	/
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Elvis	Elvis	k1gMnSc1	Elvis
Presley	Preslea	k1gMnSc2	Preslea
<g/>
:	:	kIx,	:
Paradise	Paradise	k1gFnSc1	Paradise
Hawaiian	Hawaiiany	k1gInPc2	Hawaiiany
Style	styl	k1gInSc5	styl
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
1966	[number]	k4	1966
–	–	k?	–
Frankie	Frankie	k1gFnPc1	Frankie
a	a	k8xC	a
Johnny	Johnn	k1gInPc1	Johnn
1966	[number]	k4	1966
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Spinout	Spinout	k5eAaPmF	Spinout
/	/	kIx~	/
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Elvis	Elvis	k1gMnSc1	Elvis
Presley	Preslea	k1gFnSc2	Preslea
<g/>
:	:	kIx,	:
Těžké	těžký	k2eAgNnSc1d1	těžké
se	se	k3xPyFc4	se
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
1967	[number]	k4	1967
–	–	k?	–
Clambake	Clambak	k1gInSc2	Clambak
/	/	kIx~	/
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Mejdan	mejdan	k1gInSc1	mejdan
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
1967	[number]	k4	1967
–	–	k?	–
Double	double	k2eAgFnSc1d1	double
Trouble	Trouble	k1gFnSc1	Trouble
/	/	kIx~	/
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Elvis	Elvis	k1gMnSc1	Elvis
Presley	Preslea	k1gMnSc2	Preslea
<g/>
:	:	kIx,	:
Pořádný	pořádný	k2eAgInSc1d1	pořádný
průšvih	průšvih	k1gInSc1	průšvih
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
1967	[number]	k4	1967
–	–	k?	–
Easy	Easa	k1gMnSc2	Easa
Come	Com	k1gMnSc2	Com
<g/>
,	,	kIx,	,
Easy	Easa	k1gMnSc2	Easa
Go	Go	k1gFnSc2	Go
1968	[number]	k4	1968
–	–	k?	–
Stay	Sta	k2eAgInPc1d1	Sta
Away	Away	k1gInPc1	Away
<g/>
,	,	kIx,	,
Joe	Joe	k1gFnSc1	Joe
1968	[number]	k4	1968
–	–	k?	–
Live	Liv	k1gFnSc2	Liv
a	a	k8xC	a
Little	Little	k1gFnSc2	Little
<g/>
,	,	kIx,	,
Love	lov	k1gInSc5	lov
a	a	k8xC	a
Little	Little	k1gFnSc1	Little
1968	[number]	k4	1968
–	–	k?	–
Speedway	Speedwaa	k1gFnSc2	Speedwaa
/	/	kIx~	/
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Elvis	Elvis	k1gMnSc1	Elvis
Presley	Preslea	k1gFnSc2	Preslea
<g/>
:	:	kIx,	:
Okruh	okruh	k1gInSc1	okruh
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
1969	[number]	k4	1969
–	–	k?	–
Charro	Charro	k1gNnSc1	Charro
<g/>
!	!	kIx.	!
</s>
<s>
1969	[number]	k4	1969
–	–	k?	–
Change	change	k1gFnSc2	change
of	of	k?	of
Habit	Habit	k2eAgInSc1d1	Habit
1969	[number]	k4	1969
–	–	k?	–
The	The	k1gMnSc1	The
Trouble	Trouble	k1gMnSc1	Trouble
with	with	k1gMnSc1	with
Girls	girl	k1gFnPc2	girl
/	/	kIx~	/
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Elvis	Elvis	k1gMnSc1	Elvis
<g/>
:	:	kIx,	:
Trable	trabl	k1gInPc1	trabl
s	s	k7c7	s
děvčaty	děvče	k1gNnPc7	děvče
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
