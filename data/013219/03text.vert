<p>
<s>
Jezevec	jezevec	k1gMnSc1	jezevec
lesní	lesní	k2eAgMnSc1d1	lesní
(	(	kIx(	(
<g/>
Meles	Meles	k1gMnSc1	Meles
meles	meles	k1gMnSc1	meles
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
šelma	šelma	k1gFnSc1	šelma
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
lasicovití	lasicovitý	k2eAgMnPc1d1	lasicovitý
<g/>
.	.	kIx.	.
</s>
<s>
Obývá	obývat	k5eAaImIp3nS	obývat
kromě	kromě	k7c2	kromě
severní	severní	k2eAgFnSc2d1	severní
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
celou	celý	k2eAgFnSc4d1	celá
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
Krétu	Kréta	k1gFnSc4	Kréta
<g/>
,	,	kIx,	,
Blízký	blízký	k2eAgInSc4d1	blízký
východ	východ	k1gInSc4	východ
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
až	až	k9	až
na	na	k7c4	na
Dálný	dálný	k2eAgInSc4d1	dálný
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
lovení	lovení	k1gNnSc6	lovení
jezevců	jezevec	k1gMnPc2	jezevec
byla	být	k5eAaImAgNnP	být
vyšlechtěna	vyšlechtěn	k2eAgNnPc1d1	vyšlechtěno
speciální	speciální	k2eAgNnPc1d1	speciální
psí	psí	k2eAgNnPc1d1	psí
plemena	plemeno	k1gNnPc1	plemeno
–	–	k?	–
jezevčíci	jezevčík	k1gMnPc1	jezevčík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stavba	stavba	k1gFnSc1	stavba
těla	tělo	k1gNnSc2	tělo
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
jezevec	jezevec	k1gMnSc1	jezevec
největší	veliký	k2eAgFnSc4d3	veliký
lasicovitou	lasicovitý	k2eAgFnSc4d1	lasicovitá
šelmou	šelma	k1gFnSc7	šelma
<g/>
,	,	kIx,	,
váží	vážit	k5eAaImIp3nS	vážit
10	[number]	k4	10
až	až	k9	až
20	[number]	k4	20
kg	kg	kA	kg
<g/>
,	,	kIx,	,
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
je	být	k5eAaImIp3nS	být
až	až	k9	až
85	[number]	k4	85
cm	cm	kA	cm
a	a	k8xC	a
s	s	k7c7	s
ocasem	ocas	k1gInSc7	ocas
má	mít	k5eAaImIp3nS	mít
metr	metr	k1gInSc4	metr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
lasicovitými	lasicovitý	k2eAgFnPc7d1	lasicovitá
šelmami	šelma	k1gFnPc7	šelma
má	mít	k5eAaImIp3nS	mít
jezevec	jezevec	k1gMnSc1	jezevec
zcela	zcela	k6eAd1	zcela
odlišný	odlišný	k2eAgInSc4d1	odlišný
tvar	tvar	k1gInSc4	tvar
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
zavalitou	zavalitý	k2eAgFnSc4d1	zavalitá
postavu	postava	k1gFnSc4	postava
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
téměř	téměř	k6eAd1	téměř
bílou	bílý	k2eAgFnSc4d1	bílá
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
jen	jen	k9	jen
přes	přes	k7c4	přes
oči	oko	k1gNnPc4	oko
má	mít	k5eAaImIp3nS	mít
široké	široký	k2eAgNnSc1d1	široké
černé	černý	k2eAgInPc1d1	černý
pruhy	pruh	k1gInPc1	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Srst	srst	k1gFnSc1	srst
je	být	k5eAaImIp3nS	být
žlutošedá	žlutošedý	k2eAgFnSc1d1	žlutošedá
s	s	k7c7	s
černými	černý	k2eAgInPc7d1	černý
a	a	k8xC	a
bílými	bílý	k2eAgInPc7d1	bílý
konci	konec	k1gInPc7	konec
<g/>
,	,	kIx,	,
hrubá	hrubá	k1gFnSc1	hrubá
a	a	k8xC	a
štětinatá	štětinatý	k2eAgFnSc1d1	štětinatá
<g/>
.	.	kIx.	.
</s>
<s>
Došlapuje	došlapovat	k5eAaImIp3nS	došlapovat
na	na	k7c4	na
celá	celý	k2eAgNnPc4d1	celé
chodidla	chodidlo	k1gNnPc4	chodidlo
(	(	kIx(	(
<g/>
ploskonožec	ploskonožec	k1gMnSc1	ploskonožec
<g/>
)	)	kIx)	)
a	a	k8xC	a
tlapy	tlapa	k1gFnPc4	tlapa
má	mít	k5eAaImIp3nS	mít
opatřeny	opatřen	k2eAgFnPc4d1	opatřena
pěti	pět	k4xCc7	pět
velkými	velký	k2eAgInPc7d1	velký
drápy	dráp	k1gInPc7	dráp
<g/>
.	.	kIx.	.
</s>
<s>
Uši	ucho	k1gNnPc4	ucho
má	mít	k5eAaImIp3nS	mít
malé	malý	k2eAgNnSc1d1	malé
<g/>
.	.	kIx.	.
</s>
<s>
Pomaleji	pomale	k6eAd2	pomale
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
==	==	k?	==
</s>
</p>
<p>
<s>
Vyhrabává	vyhrabávat	k5eAaImIp3nS	vyhrabávat
si	se	k3xPyFc3	se
noru	nora	k1gFnSc4	nora
ve	v	k7c6	v
vyvýšeném	vyvýšený	k2eAgInSc6d1	vyvýšený
terénu	terén	k1gInSc6	terén
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
hodně	hodně	k6eAd1	hodně
členitá	členitý	k2eAgFnSc1d1	členitá
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
chodbami	chodba	k1gFnPc7	chodba
a	a	k8xC	a
komorou	komora	k1gFnSc7	komora
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
spí	spát	k5eAaImIp3nP	spát
<g/>
.	.	kIx.	.
</s>
<s>
Nora	Nora	k1gFnSc1	Nora
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
až	až	k9	až
5	[number]	k4	5
m	m	kA	m
hluboká	hluboký	k2eAgFnSc1d1	hluboká
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
zimu	zima	k1gFnSc4	zima
omezuje	omezovat	k5eAaImIp3nS	omezovat
svoji	svůj	k3xOyFgFnSc4	svůj
aktivitu	aktivita	k1gFnSc4	aktivita
<g/>
,	,	kIx,	,
hodně	hodně	k6eAd1	hodně
spí	spát	k5eAaImIp3nP	spát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
do	do	k7c2	do
skutečného	skutečný	k2eAgInSc2d1	skutečný
zimního	zimní	k2eAgInSc2d1	zimní
spánku	spánek	k1gInSc2	spánek
se	s	k7c7	s
sníženou	snížený	k2eAgFnSc7d1	snížená
tělesnou	tělesný	k2eAgFnSc7d1	tělesná
teplotou	teplota	k1gFnSc7	teplota
neupadá	upadat	k5eNaPmIp3nS	upadat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
čistotný	čistotný	k2eAgInSc1d1	čistotný
–	–	k?	–
trus	trus	k1gInSc1	trus
ukládá	ukládat	k5eAaImIp3nS	ukládat
mimo	mimo	k7c4	mimo
noru	nora	k1gFnSc4	nora
<g/>
,	,	kIx,	,
neobjevují	objevovat	k5eNaImIp3nP	objevovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
ani	ani	k8xC	ani
zbytky	zbytek	k1gInPc1	zbytek
potravy	potrava	k1gFnSc2	potrava
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
třeba	třeba	k6eAd1	třeba
od	od	k7c2	od
nor	nora	k1gFnPc2	nora
lišek	liška	k1gFnPc2	liška
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	s	k7c7	s
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
,	,	kIx,	,
dešťovkami	dešťovka	k1gFnPc7	dešťovka
<g/>
,	,	kIx,	,
hraboši	hraboš	k1gMnPc7	hraboš
<g/>
,	,	kIx,	,
vejci	vejce	k1gNnSc3	vejce
<g/>
,	,	kIx,	,
semeny	semeno	k1gNnPc7	semeno
<g/>
,	,	kIx,	,
houbami	houba	k1gFnPc7	houba
<g/>
,	,	kIx,	,
kořínky	kořínek	k1gInPc7	kořínek
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
i	i	k9	i
zdechlinou	zdechlina	k1gFnSc7	zdechlina
–	–	k?	–
je	být	k5eAaImIp3nS	být
všežravec	všežravec	k1gMnSc1	všežravec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Jezevčí	jezevčí	k2eAgNnSc1d1	jezevčí
páření	páření	k1gNnSc1	páření
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
chrutí	chrutí	k1gNnSc1	chrutí
<g/>
.	.	kIx.	.
</s>
<s>
Mláďatům	mládě	k1gNnPc3	mládě
<g/>
,	,	kIx,	,
kterých	který	k3yIgInPc2	který
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nS	rodit
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
otevírají	otevírat	k5eAaImIp3nP	otevírat
oči	oko	k1gNnPc4	oko
po	po	k7c6	po
3	[number]	k4	3
týdnech	týden	k1gInPc6	týden
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
kojena	kojen	k2eAgFnSc1d1	kojena
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
týdnů	týden	k1gInPc2	týden
a	a	k8xC	a
po	po	k7c6	po
5	[number]	k4	5
měsících	měsíc	k1gInPc6	měsíc
se	se	k3xPyFc4	se
osamostatňují	osamostatňovat	k5eAaImIp3nP	osamostatňovat
<g/>
.	.	kIx.	.
</s>
<s>
Dospělosti	dospělost	k1gFnPc1	dospělost
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
1,5	[number]	k4	1,5
-	-	kIx~	-
2	[number]	k4	2
let	léto	k1gNnPc2	léto
<g/>
..	..	k?	..
Jezevci	jezevec	k1gMnPc1	jezevec
se	se	k3xPyFc4	se
dožívají	dožívat	k5eAaImIp3nP	dožívat
věku	věk	k1gInSc3	věk
až	až	k9	až
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
zoo	zoo	k1gFnSc6	zoo
až	až	k9	až
20	[number]	k4	20
let	léto	k1gNnPc2	léto
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Početnost	početnost	k1gFnSc4	početnost
==	==	k?	==
</s>
</p>
<p>
<s>
I	i	k9	i
přes	přes	k7c4	přes
jeho	on	k3xPp3gInSc4	on
velký	velký	k2eAgInSc4d1	velký
počet	počet	k1gInSc4	počet
se	se	k3xPyFc4	se
zakládají	zakládat	k5eAaImIp3nP	zakládat
záchranné	záchranný	k2eAgFnSc2d1	záchranná
stanice	stanice	k1gFnSc2	stanice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
chovají	chovat	k5eAaImIp3nP	chovat
tyto	tento	k3xDgMnPc4	tento
jezevce	jezevec	k1gMnPc4	jezevec
a	a	k8xC	a
těch	ten	k3xDgMnPc2	ten
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
asi	asi	k9	asi
50	[number]	k4	50
<g/>
.	.	kIx.	.
</s>
<s>
Jezevec	jezevec	k1gMnSc1	jezevec
je	být	k5eAaImIp3nS	být
těžko	těžko	k6eAd1	těžko
spatřitelný	spatřitelný	k2eAgInSc1d1	spatřitelný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Jezevec	jezevec	k1gMnSc1	jezevec
obývá	obývat	k5eAaImIp3nS	obývat
mírné	mírný	k2eAgNnSc4d1	mírné
pásmo	pásmo	k1gNnSc4	pásmo
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Rozšířen	rozšířen	k2eAgMnSc1d1	rozšířen
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
britských	britský	k2eAgInPc2d1	britský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
)	)	kIx)	)
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
polárního	polární	k2eAgInSc2d1	polární
kruhu	kruh	k1gInSc2	kruh
a	a	k8xC	a
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Asii	Asie	k1gFnSc6	Asie
až	až	k9	až
po	po	k7c4	po
Japonsko	Japonsko	k1gNnSc4	Japonsko
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
po	po	k7c4	po
střední	střední	k2eAgInSc4d1	střední
Ob	Ob	k1gInSc4	Ob
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
po	po	k7c4	po
Izrael	Izrael	k1gInSc4	Izrael
<g/>
,	,	kIx,	,
Írán	Írán	k1gInSc1	Írán
<g/>
,	,	kIx,	,
Tibet	Tibet	k1gInSc1	Tibet
a	a	k8xC	a
jižní	jižní	k2eAgFnSc4d1	jižní
Čínu	Čína	k1gFnSc4	Čína
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Rád	rád	k6eAd1	rád
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
lesy	les	k1gInPc4	les
prostoupené	prostoupený	k2eAgInPc4d1	prostoupený
poli	pole	k1gNnPc7	pole
a	a	k8xC	a
loukami	louka	k1gFnPc7	louka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nachází	nacházet	k5eAaImIp3nS	nacházet
možnosti	možnost	k1gFnPc4	možnost
budovat	budovat	k5eAaImF	budovat
své	svůj	k3xOyFgFnPc4	svůj
složité	složitý	k2eAgFnPc4d1	složitá
nory	nora	k1gFnPc4	nora
a	a	k8xC	a
dostatek	dostatek	k1gInSc4	dostatek
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jezevec	jezevec	k1gMnSc1	jezevec
a	a	k8xC	a
člověk	člověk	k1gMnSc1	člověk
==	==	k?	==
</s>
</p>
<p>
<s>
Jezevci	jezevec	k1gMnPc1	jezevec
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
v	v	k7c6	v
keltské	keltský	k2eAgFnSc6d1	keltská
i	i	k8xC	i
germánské	germánský	k2eAgFnSc6d1	germánská
mytologii	mytologie	k1gFnSc6	mytologie
<g/>
.	.	kIx.	.
</s>
<s>
Jezevec	jezevec	k1gMnSc1	jezevec
byl	být	k5eAaImAgMnS	být
pokládán	pokládat	k5eAaImNgMnS	pokládat
za	za	k7c4	za
symbol	symbol	k1gInSc4	symbol
odvahy	odvaha	k1gFnSc2	odvaha
a	a	k8xC	a
zmužilosti	zmužilost	k1gFnSc2	zmužilost
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgFnSc1d1	tradiční
skotská	skotský	k2eAgFnSc1d1	skotská
tobolka	tobolka	k1gFnSc1	tobolka
zvaná	zvaný	k2eAgFnSc1d1	zvaná
sporran	sporran	k1gInSc4	sporran
bývá	bývat	k5eAaImIp3nS	bývat
vyráběna	vyrábět	k5eAaImNgFnS	vyrábět
z	z	k7c2	z
jezevčí	jezevčí	k2eAgFnSc2d1	jezevčí
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
chlupů	chlup	k1gInPc2	chlup
jezevce	jezevec	k1gMnPc4	jezevec
se	se	k3xPyFc4	se
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
holicí	holicí	k2eAgFnPc1d1	holicí
štětky	štětka	k1gFnPc1	štětka
<g/>
.	.	kIx.	.
</s>
<s>
Jezevčí	jezevčí	k2eAgNnSc1d1	jezevčí
maso	maso	k1gNnSc1	maso
je	být	k5eAaImIp3nS	být
poživatelné	poživatelný	k2eAgNnSc1d1	poživatelné
<g/>
,	,	kIx,	,
oblíbené	oblíbený	k2eAgNnSc1d1	oblíbené
bylo	být	k5eAaImAgNnS	být
hlavně	hlavně	k6eAd1	hlavně
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jezevci	jezevec	k1gMnPc1	jezevec
se	se	k3xPyFc4	se
loví	lovit	k5eAaImIp3nP	lovit
především	především	k6eAd1	především
norováním	norování	k1gNnSc7	norování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
MATYÁŠTÍK	MATYÁŠTÍK	kA	MATYÁŠTÍK
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g/>
;	;	kIx,	;
BIČÍK	bičík	k1gInSc1	bičík
<g/>
,	,	kIx,	,
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
<g/>
;	;	kIx,	;
ŘEHÁK	Řehák	k1gMnSc1	Řehák
<g/>
,	,	kIx,	,
Libor	Libor	k1gMnSc1	Libor
<g/>
.	.	kIx.	.
</s>
<s>
Jezevec	jezevec	k1gMnSc1	jezevec
lesní	lesní	k2eAgFnSc2d1	lesní
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
biologie	biologie	k1gFnSc1	biologie
a	a	k8xC	a
význam	význam	k1gInSc1	význam
v	v	k7c6	v
ekosystému	ekosystém	k1gInSc6	ekosystém
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Venator	Venator	k1gMnSc1	Venator
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
200	[number]	k4	200
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902849	[number]	k4	902849
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svolením	svolení	k1gNnSc7	svolení
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
jezevec	jezevec	k1gMnSc1	jezevec
lesní	lesní	k2eAgMnSc1d1	lesní
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
jezevec	jezevec	k1gMnSc1	jezevec
lesní	lesní	k2eAgMnSc1d1	lesní
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Meles	Meles	k1gInSc1	Meles
meles	meles	k1gInSc1	meles
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Jezevec	jezevec	k1gMnSc1	jezevec
v	v	k7c6	v
ZOO	zoo	k1gFnSc6	zoo
Děčín	Děčín	k1gInSc1	Děčín
</s>
</p>
<p>
<s>
Jezevec	jezevec	k1gMnSc1	jezevec
lesní	lesní	k2eAgMnSc1d1	lesní
v	v	k7c6	v
myslivosti	myslivost	k1gFnSc6	myslivost
</s>
</p>
