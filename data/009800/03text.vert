<p>
<s>
Říjnová	říjnový	k2eAgFnSc1d1	říjnová
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
též	též	k9	též
jako	jako	k8xC	jako
bolševická	bolševický	k2eAgFnSc1d1	bolševická
revoluce	revoluce	k1gFnPc1	revoluce
nebo	nebo	k8xC	nebo
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
samotných	samotný	k2eAgMnPc2d1	samotný
komunistů	komunista	k1gMnPc2	komunista
<g/>
)	)	kIx)	)
Velká	velký	k2eAgFnSc1d1	velká
říjnová	říjnový	k2eAgFnSc1d1	říjnová
socialistická	socialistický	k2eAgFnSc1d1	socialistická
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
VŘSR	VŘSR	kA	VŘSR
či	či	k8xC	či
Velký	velký	k2eAgInSc4d1	velký
říjen	říjen	k1gInSc4	říjen
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
druhá	druhý	k4xOgFnSc1	druhý
fáze	fáze	k1gFnSc1	fáze
ruské	ruský	k2eAgFnSc2d1	ruská
revoluce	revoluce	k1gFnSc2	revoluce
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
následující	následující	k2eAgInSc1d1	následující
po	po	k7c6	po
únorové	únorový	k2eAgFnSc6d1	únorová
revoluci	revoluce	k1gFnSc6	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Svrhla	svrhnout	k5eAaPmAgFnS	svrhnout
středově	středově	k6eAd1	středově
liberální	liberální	k2eAgFnSc4d1	liberální
Prozatímní	prozatímní	k2eAgFnSc4d1	prozatímní
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
byla	být	k5eAaImAgFnS	být
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
jen	jen	k9	jen
umírněná	umírněný	k2eAgFnSc1d1	umírněná
levice	levice	k1gFnSc1	levice
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
socialisté-revolucionáři	socialistéevolucionář	k1gMnPc1	socialisté-revolucionář
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
eseři	eser	k1gMnPc1	eser
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
sociální	sociální	k2eAgMnPc1d1	sociální
demokraté	demokrat	k1gMnPc1	demokrat
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
menševici	menševik	k1gMnPc1	menševik
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
vynesla	vynést	k5eAaPmAgFnS	vynést
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
krajní	krajní	k2eAgFnSc4d1	krajní
levici	levice	k1gFnSc4	levice
vedenou	vedený	k2eAgFnSc4d1	vedená
bolševiky	bolševik	k1gMnPc7	bolševik
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
V.	V.	kA	V.
I.	I.	kA	I.
Leninem	Lenin	k1gMnSc7	Lenin
<g/>
.	.	kIx.	.
</s>
<s>
Bolševici	bolševik	k1gMnPc1	bolševik
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
oficiálně	oficiálně	k6eAd1	oficiálně
nazývaní	nazývaný	k2eAgMnPc1d1	nazývaný
Sociálně	sociálně	k6eAd1	sociálně
demokratická	demokratický	k2eAgFnSc1d1	demokratická
dělnická	dělnický	k2eAgFnSc1d1	Dělnická
strana	strana	k1gFnSc1	strana
Ruska	Ruska	k1gFnSc1	Ruska
(	(	kIx(	(
<g/>
bolševiků	bolševik	k1gMnPc2	bolševik
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
přímými	přímý	k2eAgMnPc7d1	přímý
předchůdci	předchůdce	k1gMnPc7	předchůdce
pozdější	pozdní	k2eAgFnSc2d2	pozdější
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
potlačili	potlačit	k5eAaPmAgMnP	potlačit
své	svůj	k3xOyFgMnPc4	svůj
dosavadní	dosavadní	k2eAgMnPc4d1	dosavadní
levicové	levicový	k2eAgMnPc4d1	levicový
spojence	spojenec	k1gMnPc4	spojenec
<g/>
,	,	kIx,	,
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
následující	následující	k2eAgFnSc4d1	následující
občanskou	občanský	k2eAgFnSc4d1	občanská
válku	válka	k1gFnSc4	válka
a	a	k8xC	a
založili	založit	k5eAaPmAgMnP	založit
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
stabilní	stabilní	k2eAgInSc4d1	stabilní
komunistický	komunistický	k2eAgInSc4d1	komunistický
režim	režim	k1gInSc4	režim
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stal	stát	k5eAaPmAgMnS	stát
supervelmocí	supervelmoc	k1gFnSc7	supervelmoc
<g/>
.	.	kIx.	.
</s>
<s>
Datum	datum	k1gNnSc1	datum
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1917	[number]	k4	1917
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
juliánskému	juliánský	k2eAgInSc3d1	juliánský
kalendáři	kalendář	k1gInSc3	kalendář
platnému	platný	k2eAgInSc3d1	platný
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
zrušenému	zrušený	k2eAgMnSc3d1	zrušený
novou	nový	k2eAgFnSc7d1	nová
bolševickou	bolševický	k2eAgFnSc7d1	bolševická
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zbytek	zbytek	k1gInSc4	zbytek
světa	svět	k1gInSc2	svět
začaly	začít	k5eAaPmAgFnP	začít
události	událost	k1gFnPc1	událost
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
podle	podle	k7c2	podle
gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prozatímní	prozatímní	k2eAgFnSc1d1	prozatímní
vláda	vláda	k1gFnSc1	vláda
trvala	trvat	k5eAaImAgFnS	trvat
na	na	k7c4	na
pokračování	pokračování	k1gNnSc4	pokračování
velmi	velmi	k6eAd1	velmi
nepopulární	populární	k2eNgFnSc2d1	nepopulární
účasti	účast	k1gFnSc2	účast
Ruska	Rusko	k1gNnSc2	Rusko
na	na	k7c6	na
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bránilo	bránit	k5eAaImAgNnS	bránit
provádění	provádění	k1gNnSc4	provádění
hlubokých	hluboký	k2eAgFnPc2d1	hluboká
reforem	reforma	k1gFnPc2	reforma
požadovaných	požadovaný	k2eAgFnPc2d1	požadovaná
občany	občan	k1gMnPc7	občan
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bolševický	bolševický	k2eAgInSc1d1	bolševický
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
vyjádřený	vyjádřený	k2eAgInSc4d1	vyjádřený
hesly	heslo	k1gNnPc7	heslo
"	"	kIx"	"
<g/>
Mír	mír	k1gInSc1	mír
<g/>
,	,	kIx,	,
chléb	chléb	k1gInSc1	chléb
a	a	k8xC	a
půda	půda	k1gFnSc1	půda
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Všechnu	všechen	k3xTgFnSc4	všechen
moc	moc	k1gFnSc4	moc
sovětům	sovět	k1gInPc3	sovět
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
lidovým	lidový	k2eAgFnPc3d1	lidová
radám	rada	k1gFnPc3	rada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
rychle	rychle	k6eAd1	rychle
získával	získávat	k5eAaImAgMnS	získávat
příznivce	příznivec	k1gMnPc4	příznivec
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
krize	krize	k1gFnSc1	krize
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
prohloubila	prohloubit	k5eAaPmAgFnS	prohloubit
<g/>
,	,	kIx,	,
hrozba	hrozba	k1gFnSc1	hrozba
nasazení	nasazení	k1gNnSc2	nasazení
na	na	k7c4	na
frontu	fronta	k1gFnSc4	fronta
pro	pro	k7c4	pro
vojáky	voják	k1gMnPc4	voják
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
rozčarování	rozčarování	k1gNnSc2	rozčarování
z	z	k7c2	z
nedostatečnosti	nedostatečnost	k1gFnSc2	nedostatečnost
vládních	vládní	k2eAgFnPc2d1	vládní
reforem	reforma	k1gFnPc2	reforma
a	a	k8xC	a
podpora	podpora	k1gFnSc1	podpora
Prozatímní	prozatímní	k2eAgFnSc2d1	prozatímní
vlády	vláda	k1gFnSc2	vláda
většinou	většinou	k6eAd1	většinou
ostatních	ostatní	k2eAgFnPc2d1	ostatní
stran	strana	k1gFnPc2	strana
zvýhodňovala	zvýhodňovat	k5eAaImAgFnS	zvýhodňovat
bolševiky	bolševik	k1gMnPc4	bolševik
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
rozpoutali	rozpoutat	k5eAaPmAgMnP	rozpoutat
intenzivní	intenzivní	k2eAgFnSc4d1	intenzivní
propagandistickou	propagandistický	k2eAgFnSc4d1	propagandistická
kampaň	kampaň	k1gFnSc4	kampaň
<g/>
.	.	kIx.	.
</s>
<s>
Nižší	nízký	k2eAgFnPc1d2	nižší
třídy	třída	k1gFnPc1	třída
ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
odmítaly	odmítat	k5eAaImAgFnP	odmítat
obětovat	obětovat	k5eAaBmF	obětovat
pokračováním	pokračování	k1gNnSc7	pokračování
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
tolerováním	tolerování	k1gNnSc7	tolerování
koaliční	koaliční	k2eAgFnSc2d1	koaliční
vlády	vláda	k1gFnSc2	vláda
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
pravicové	pravicový	k2eAgFnSc2d1	pravicová
Konstitučně	konstitučně	k6eAd1	konstitučně
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
kadetů	kadet	k1gMnPc2	kadet
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
po	po	k7c6	po
Kornilovově	Kornilovův	k2eAgInSc6d1	Kornilovův
puči	puč	k1gInSc6	puč
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
zjevné	zjevný	k2eAgFnSc3d1	zjevná
slabosti	slabost	k1gFnSc3	slabost
Prozatímní	prozatímní	k2eAgFnSc2d1	prozatímní
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
před	před	k7c7	před
plánovanou	plánovaný	k2eAgFnSc7d1	plánovaná
revolucí	revoluce	k1gFnSc7	revoluce
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
obyvatelé	obyvatel	k1gMnPc1	obyvatel
odmítnou	odmítnout	k5eAaPmIp3nP	odmítnout
ozbrojené	ozbrojený	k2eAgNnSc4d1	ozbrojené
povstání	povstání	k1gNnSc4	povstání
samotných	samotný	k2eAgMnPc2d1	samotný
bolševiků	bolševik	k1gMnPc2	bolševik
proti	proti	k7c3	proti
Prozatímní	prozatímní	k2eAgFnSc3d1	prozatímní
vládě	vláda	k1gFnSc3	vláda
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgFnSc2	jaký
chtěl	chtít	k5eAaImAgMnS	chtít
Lenin	Lenin	k1gMnSc1	Lenin
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnut	k2eAgNnSc1d1	rozhodnuto
přijmout	přijmout	k5eAaPmF	přijmout
méně	málo	k6eAd2	málo
riskantní	riskantní	k2eAgInSc4d1	riskantní
postup	postup	k1gInSc4	postup
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
hlavně	hlavně	k9	hlavně
Lev	Lev	k1gMnSc1	Lev
Trockij	Trockij	k1gMnSc1	Trockij
a	a	k8xC	a
který	který	k3yIgInSc1	který
spočíval	spočívat	k5eAaImAgInS	spočívat
v	v	k7c6	v
převzetí	převzetí	k1gNnSc6	převzetí
moci	moc	k1gFnSc2	moc
během	během	k7c2	během
připravovaného	připravovaný	k2eAgInSc2d1	připravovaný
Druhého	druhý	k4xOgMnSc2	druhý
sjezdu	sjezd	k1gInSc2	sjezd
sovětů	sovět	k1gInPc2	sovět
<g/>
:	:	kIx,	:
Moc	moc	k6eAd1	moc
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
strhne	strhnout	k5eAaPmIp3nS	strhnout
Petrohradský	petrohradský	k2eAgInSc1d1	petrohradský
sovět	sovět	k1gInSc1	sovět
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
a	a	k8xC	a
jakýkoli	jakýkoli	k3yIgInSc1	jakýkoli
odpor	odpor	k1gInSc1	odpor
vlády	vláda	k1gFnSc2	vláda
bude	být	k5eAaImBp3nS	být
pak	pak	k6eAd1	pak
vypadat	vypadat	k5eAaPmF	vypadat
jako	jako	k9	jako
kontrarevoluce	kontrarevoluce	k1gFnSc1	kontrarevoluce
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
zatím	zatím	k6eAd1	zatím
poslala	poslat	k5eAaPmAgFnS	poslat
část	část	k1gFnSc1	část
vojenské	vojenský	k2eAgFnSc2d1	vojenská
posádky	posádka	k1gFnSc2	posádka
na	na	k7c4	na
blízkou	blízký	k2eAgFnSc4d1	blízká
frontu	fronta	k1gFnSc4	fronta
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
podnítila	podnítit	k5eAaPmAgFnS	podnítit
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
.	.	kIx.	.
<g/>
Nový	nový	k2eAgInSc1d1	nový
Petrohradský	petrohradský	k2eAgInSc1d1	petrohradský
vojenský	vojenský	k2eAgInSc1d1	vojenský
revoluční	revoluční	k2eAgInSc1d1	revoluční
výbor	výbor	k1gInSc1	výbor
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
hájil	hájit	k5eAaImAgMnS	hájit
své	svůj	k3xOyFgInPc4	svůj
kroky	krok	k1gInPc4	krok
jako	jako	k8xS	jako
obranu	obrana	k1gFnSc4	obrana
proti	proti	k7c3	proti
kontrarevoluci	kontrarevoluce	k1gFnSc3	kontrarevoluce
a	a	k8xC	a
který	který	k3yQgInSc4	který
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
řídili	řídit	k5eAaImAgMnP	řídit
bolševici	bolševik	k1gMnPc1	bolševik
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
převzal	převzít	k5eAaPmAgInS	převzít
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
posádkovými	posádkový	k2eAgFnPc7d1	Posádková
jednotkami	jednotka	k1gFnPc7	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
vládou	vláda	k1gFnSc7	vláda
a	a	k8xC	a
Výborem	výbor	k1gInSc7	výbor
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
několika	několik	k4yIc3	několik
nekrvavým	krvavý	k2eNgInPc3d1	nekrvavý
střetům	střet	k1gInPc3	střet
o	o	k7c4	o
ovládnutí	ovládnutí	k1gNnSc4	ovládnutí
strategických	strategický	k2eAgInPc2d1	strategický
bodů	bod	k1gInPc2	bod
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
skončily	skončit	k5eAaPmAgInP	skončit
vítězstvím	vítězství	k1gNnSc7	vítězství
Výboru	výbor	k1gInSc2	výbor
a	a	k8xC	a
izolací	izolace	k1gFnPc2	izolace
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
stěží	stěží	k6eAd1	stěží
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
konečně	konečně	k6eAd1	konečně
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přímému	přímý	k2eAgInSc3d1	přímý
útoku	útok	k1gInSc3	útok
na	na	k7c4	na
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
Lenin	Lenin	k1gMnSc1	Lenin
požadoval	požadovat	k5eAaImAgMnS	požadovat
už	už	k6eAd1	už
týdny	týden	k1gInPc4	týden
a	a	k8xC	a
který	který	k3yIgInSc1	který
skončil	skončit	k5eAaPmAgInS	skončit
zajetím	zajetí	k1gNnSc7	zajetí
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgFnSc2d1	celá
Prozatímní	prozatímní	k2eAgFnSc2d1	prozatímní
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
25	[number]	k4	25
<g/>
.	.	kIx.	.
říjnajul	říjnajout	k5eAaPmAgInS	říjnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
již	již	k6eAd1	již
zasedal	zasedat	k5eAaImAgInS	zasedat
Druhý	druhý	k4xOgInSc1	druhý
sjezd	sjezd	k1gInSc1	sjezd
sovětů	sovět	k1gInPc2	sovět
<g/>
.	.	kIx.	.
</s>
<s>
Opuštění	opuštění	k1gNnSc4	opuštění
sjezdu	sjezd	k1gInSc2	sjezd
umírněnými	umírněný	k2eAgMnPc7d1	umírněný
socialisty	socialist	k1gMnPc7	socialist
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
akci	akce	k1gFnSc3	akce
bolševiků	bolševik	k1gMnPc2	bolševik
usnadnilo	usnadnit	k5eAaPmAgNnS	usnadnit
vytvoření	vytvoření	k1gNnSc1	vytvoření
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
Sovnarkomu	sovnarkom	k1gInSc2	sovnarkom
<g/>
)	)	kIx)	)
výlučně	výlučně	k6eAd1	výlučně
z	z	k7c2	z
bolševiků	bolševik	k1gMnPc2	bolševik
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgNnSc1d1	následující
vyjednávání	vyjednávání	k1gNnSc1	vyjednávání
o	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
koaliční	koaliční	k2eAgFnSc2d1	koaliční
vlády	vláda	k1gFnSc2	vláda
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
socialistických	socialistický	k2eAgFnPc2d1	socialistická
stran	strana	k1gFnPc2	strana
zkrachovala	zkrachovat	k5eAaPmAgFnS	zkrachovat
na	na	k7c6	na
neústupnosti	neústupnost	k1gFnSc6	neústupnost
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
frakcí	frakce	k1gFnPc2	frakce
<g/>
.	.	kIx.	.
</s>
<s>
Pokus	pokus	k1gInSc1	pokus
opozice	opozice	k1gFnSc2	opozice
o	o	k7c4	o
povstání	povstání	k1gNnSc4	povstání
proti	proti	k7c3	proti
bolševikům	bolševik	k1gMnPc3	bolševik
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
ani	ani	k8xC	ani
pochod	pochod	k1gInSc4	pochod
vojsk	vojsko	k1gNnPc2	vojsko
z	z	k7c2	z
fronty	fronta	k1gFnSc2	fronta
na	na	k7c4	na
Petrohrad	Petrohrad	k1gInSc4	Petrohrad
se	se	k3xPyFc4	se
nezdařily	zdařit	k5eNaPmAgFnP	zdařit
<g/>
.	.	kIx.	.
</s>
<s>
Moc	moc	k1gFnSc1	moc
nové	nový	k2eAgFnSc2d1	nová
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
šířila	šířit	k5eAaImAgFnS	šířit
ve	v	k7c6	v
fázích	fáze	k1gFnPc6	fáze
doprovázených	doprovázený	k2eAgFnPc2d1	doprovázená
vážnými	vážný	k2eAgInPc7d1	vážný
střety	střet	k1gInPc7	střet
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byla	být	k5eAaImAgFnS	být
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
slabost	slabost	k1gFnSc1	slabost
opozice	opozice	k1gFnSc1	opozice
a	a	k8xC	a
popularita	popularita	k1gFnSc1	popularita
prvních	první	k4xOgFnPc2	první
opatření	opatření	k1gNnPc4	opatření
bolševiků	bolševik	k1gMnPc2	bolševik
však	však	k9	však
pomáhala	pomáhat	k5eAaImAgFnS	pomáhat
Leninovi	Lenin	k1gMnSc3	Lenin
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc3	jeho
straníkům	straník	k1gMnPc3	straník
<g/>
.	.	kIx.	.
</s>
<s>
Nejradikálnější	radikální	k2eAgFnSc1d3	nejradikálnější
opozice	opozice	k1gFnSc1	opozice
vůči	vůči	k7c3	vůči
bolševickému	bolševický	k2eAgInSc3d1	bolševický
puči	puč	k1gInSc3	puč
byla	být	k5eAaImAgFnS	být
neoblíbená	oblíbený	k2eNgFnSc1d1	neoblíbená
a	a	k8xC	a
umírněná	umírněný	k2eAgFnSc1d1	umírněná
strana	strana	k1gFnSc1	strana
se	se	k3xPyFc4	se
nedokázala	dokázat	k5eNaPmAgFnS	dokázat
prosadit	prosadit	k5eAaPmF	prosadit
v	v	k7c6	v
institucích	instituce	k1gFnPc6	instituce
nového	nový	k2eAgInSc2d1	nový
režimu	režim	k1gInSc2	režim
–	–	k?	–
kvůli	kvůli	k7c3	kvůli
rozpuštění	rozpuštění	k1gNnSc3	rozpuštění
Ústavodárného	ústavodárný	k2eAgNnSc2d1	Ústavodárné
shromáždění	shromáždění	k1gNnSc2	shromáždění
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1918	[number]	k4	1918
a	a	k8xC	a
vyloučení	vyloučení	k1gNnSc1	vyloučení
socialistů	socialist	k1gMnPc2	socialist
ze	z	k7c2	z
sovětů	sovět	k1gInPc2	sovět
následujícího	následující	k2eAgNnSc2d1	následující
jara	jaro	k1gNnSc2	jaro
brzy	brzy	k6eAd1	brzy
propukla	propuknout	k5eAaPmAgFnS	propuknout
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pozadí	pozadí	k1gNnSc1	pozadí
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
1914	[number]	k4	1914
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
první	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
carské	carský	k2eAgNnSc1d1	carské
Rusko	Rusko	k1gNnSc1	Rusko
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
jako	jako	k8xC	jako
součást	součást	k1gFnSc4	součást
Trojdohody	Trojdohoda	k1gFnSc2	Trojdohoda
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
proti	proti	k7c3	proti
Ústředním	ústřední	k2eAgFnPc3d1	ústřední
mocnostem	mocnost	k1gFnPc3	mocnost
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Rakousku-Uhersku	Rakousku-Uhersko	k1gNnSc6	Rakousku-Uhersko
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc3	jejich
spojencům	spojenec	k1gMnPc3	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Válečné	válečný	k2eAgFnPc1d1	válečná
útrapy	útrapa	k1gFnPc1	útrapa
vyvolaly	vyvolat	k5eAaPmAgFnP	vyvolat
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
napětí	napětí	k1gNnSc2	napětí
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
v	v	k7c4	v
únorovou	únorový	k2eAgFnSc4d1	únorová
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
svrhla	svrhnout	k5eAaPmAgFnS	svrhnout
monarchii	monarchie	k1gFnSc4	monarchie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedokázala	dokázat	k5eNaPmAgFnS	dokázat
ukončit	ukončit	k5eAaPmF	ukončit
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc4d1	hlavní
příčinu	příčina	k1gFnSc4	příčina
nespokojenosti	nespokojenost	k1gFnSc2	nespokojenost
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
vzniku	vznik	k1gInSc2	vznik
připravovaného	připravovaný	k2eAgNnSc2d1	připravované
Všeruského	všeruský	k2eAgNnSc2d1	všeruský
ústavodárného	ústavodárný	k2eAgNnSc2d1	Ústavodárné
shromáždění	shromáždění	k1gNnSc2	shromáždění
měla	mít	k5eAaImAgFnS	mít
zemi	zem	k1gFnSc3	zem
řídit	řídit	k5eAaImF	řídit
Prozatimní	Prozatimní	k2eAgFnSc1d1	Prozatimní
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
posledním	poslední	k2eAgMnSc7d1	poslední
předsedou	předseda	k1gMnSc7	předseda
byl	být	k5eAaImAgMnS	být
středolevicový	středolevicový	k2eAgMnSc1d1	středolevicový
právník	právník	k1gMnSc1	právník
Alexandr	Alexandr	k1gMnSc1	Alexandr
Fjodorovič	Fjodorovič	k1gMnSc1	Fjodorovič
Kerenskij	Kerenskij	k1gMnSc1	Kerenskij
<g/>
.	.	kIx.	.
</s>
<s>
Dění	dění	k1gNnSc1	dění
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
výrazně	výrazně	k6eAd1	výrazně
ovlivňovaly	ovlivňovat	k5eAaImAgFnP	ovlivňovat
také	také	k9	také
rady	rada	k1gFnPc1	rada
pracujících	pracující	k1gMnPc2	pracující
a	a	k8xC	a
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgInPc1d1	zvaný
sověty	sovět	k1gInPc1	sovět
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
nejdůležitější	důležitý	k2eAgInSc1d3	nejdůležitější
byl	být	k5eAaImAgInS	být
Petrohradský	petrohradský	k2eAgInSc1d1	petrohradský
sovět	sovět	k1gInSc1	sovět
dělnických	dělnický	k2eAgMnPc2d1	dělnický
a	a	k8xC	a
vojenských	vojenský	k2eAgMnPc2d1	vojenský
delegátů	delegát	k1gMnPc2	delegát
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
v	v	k7c6	v
sovětech	sovět	k1gInPc6	sovět
převažoval	převažovat	k5eAaImAgInS	převažovat
vliv	vliv	k1gInSc1	vliv
demokratické	demokratický	k2eAgFnSc2d1	demokratická
levice	levice	k1gFnSc2	levice
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Strany	strana	k1gFnPc1	strana
socialistů-revolucionářů	socialistůevolucionář	k1gMnPc2	socialistů-revolucionář
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
eserů	eser	k1gMnPc2	eser
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgMnPc3	jenž
patřil	patřit	k5eAaImAgInS	patřit
Kerenský	Kerenský	k2eAgMnSc1d1	Kerenský
<g/>
,	,	kIx,	,
a	a	k8xC	a
umírněné	umírněný	k2eAgFnSc2d1	umírněná
frakce	frakce	k1gFnSc2	frakce
sociální	sociální	k2eAgFnSc2d1	sociální
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
menševiků	menševik	k1gMnPc2	menševik
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
postupně	postupně	k6eAd1	postupně
rostl	růst	k5eAaImAgInS	růst
vliv	vliv	k1gInSc1	vliv
radikálů	radikál	k1gMnPc2	radikál
odštěpených	odštěpený	k2eAgFnPc2d1	odštěpená
jak	jak	k8xS	jak
od	od	k7c2	od
sociální	sociální	k2eAgFnSc2d1	sociální
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
,	,	kIx,	,
nazývaných	nazývaný	k2eAgFnPc2d1	nazývaná
bolševici	bolševik	k1gMnPc1	bolševik
<g/>
,	,	kIx,	,
tak	tak	k9	tak
od	od	k7c2	od
eserů	eser	k1gMnPc2	eser
<g/>
,	,	kIx,	,
zvaných	zvaný	k2eAgMnPc2d1	zvaný
levých	levý	k2eAgMnPc2d1	levý
eserů	eser	k1gMnPc2	eser
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
politickou	politický	k2eAgFnSc4d1	politická
situaci	situace	k1gFnSc4	situace
vyhrotil	vyhrotit	k5eAaPmAgInS	vyhrotit
neúspěšný	úspěšný	k2eNgInSc1d1	neúspěšný
puč	puč	k1gInSc1	puč
generála	generál	k1gMnSc2	generál
Kornilova	Kornilův	k2eAgMnSc2d1	Kornilův
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
zhoršila	zhoršit	k5eAaPmAgFnS	zhoršit
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
situace	situace	k1gFnSc1	situace
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
byla	být	k5eAaImAgFnS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
<g/>
,	,	kIx,	,
nedostatek	nedostatek	k1gInSc1	nedostatek
paliva	palivo	k1gNnSc2	palivo
i	i	k8xC	i
jídla	jídlo	k1gNnSc2	jídlo
a	a	k8xC	a
inflace	inflace	k1gFnSc2	inflace
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
životní	životní	k2eAgFnPc1d1	životní
podmínky	podmínka	k1gFnPc1	podmínka
zhoršily	zhoršit	k5eAaPmAgFnP	zhoršit
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
omezuje	omezovat	k5eAaImIp3nS	omezovat
jen	jen	k9	jen
na	na	k7c4	na
administrativní	administrativní	k2eAgNnSc4d1	administrativní
opatření	opatření	k1gNnSc4	opatření
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
krize	krize	k1gFnSc2	krize
klesla	klesnout	k5eAaPmAgFnS	klesnout
popularita	popularita	k1gFnSc1	popularita
premiéra	premiér	k1gMnSc2	premiér
Kerenského	Kerenský	k2eAgMnSc2d1	Kerenský
<g/>
:	:	kIx,	:
pravice	pravice	k1gFnSc1	pravice
tvrdila	tvrdit	k5eAaImAgFnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zradil	zradit	k5eAaPmAgMnS	zradit
Kornilova	Kornilův	k2eAgMnSc4d1	Kornilův
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
levice	levice	k1gFnSc1	levice
a	a	k8xC	a
petrohradský	petrohradský	k2eAgInSc1d1	petrohradský
lid	lid	k1gInSc1	lid
ho	on	k3xPp3gMnSc4	on
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
spoluviníka	spoluviník	k1gMnSc4	spoluviník
Kornilovova	Kornilovův	k2eAgInSc2d1	Kornilovův
kontrarevolučního	kontrarevoluční	k2eAgInSc2d1	kontrarevoluční
puče	puč	k1gInSc2	puč
<g/>
.	.	kIx.	.
</s>
<s>
Kornilovova	Kornilovův	k2eAgFnSc1d1	Kornilovova
porážka	porážka	k1gFnSc1	porážka
prospěla	prospět	k5eAaPmAgFnS	prospět
hlavně	hlavně	k9	hlavně
bolševikům	bolševik	k1gMnPc3	bolševik
<g/>
;	;	kIx,	;
a	a	k8xC	a
veřejné	veřejný	k2eAgNnSc1d1	veřejné
mínění	mínění	k1gNnSc1	mínění
bylo	být	k5eAaImAgNnS	být
nakloněno	naklonit	k5eAaPmNgNnS	naklonit
vzniku	vznik	k1gInSc3	vznik
nové	nový	k2eAgFnSc2d1	nová
vlády	vláda	k1gFnSc2	vláda
opřené	opřený	k2eAgFnSc2d1	opřená
o	o	k7c4	o
sověty	sovět	k1gInPc4	sovět
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
spojila	spojit	k5eAaPmAgFnS	spojit
různé	různý	k2eAgInPc4d1	různý
socialistické	socialistický	k2eAgInPc4d1	socialistický
proudy	proud	k1gInPc4	proud
včetně	včetně	k7c2	včetně
bolševiků	bolševik	k1gMnPc2	bolševik
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
v	v	k7c6	v
nesčetných	sčetný	k2eNgFnPc6d1	nesčetná
rezolucích	rezoluce	k1gFnPc6	rezoluce
schválených	schválený	k2eAgFnPc2d1	schválená
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Kornilova	Kornilův	k2eAgFnSc1d1	Kornilova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Radikalizace	radikalizace	k1gFnSc1	radikalizace
mas	masa	k1gFnPc2	masa
se	se	k3xPyFc4	se
odrazila	odrazit	k5eAaPmAgFnS	odrazit
ve	v	k7c6	v
ztrátě	ztráta	k1gFnSc6	ztráta
moci	moc	k1gFnSc2	moc
umírněných	umírněný	k2eAgMnPc2d1	umírněný
politiků	politik	k1gMnPc2	politik
nad	nad	k7c7	nad
hlavními	hlavní	k2eAgInPc7d1	hlavní
sověty	sovět	k1gInPc7	sovět
země	zem	k1gFnSc2	zem
<g/>
:	:	kIx,	:
Moskevský	moskevský	k2eAgInSc4d1	moskevský
sovět	sovět	k1gInSc4	sovět
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
bolševici	bolševik	k1gMnPc1	bolševik
5	[number]	k4	5
<g/>
.	.	kIx.	.
záříjul	záříjout	k5eAaPmAgInS	záříjout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
a	a	k8xC	a
petrohradský	petrohradský	k2eAgMnSc1d1	petrohradský
následoval	následovat	k5eAaImAgMnS	následovat
25	[number]	k4	25
<g/>
.	.	kIx.	.
záříjul	záříjout	k5eAaPmAgInS	záříjout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
8	[number]	k4	8
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
po	po	k7c6	po
opakovaných	opakovaný	k2eAgFnPc6d1	opakovaná
porážkách	porážka	k1gFnPc6	porážka
umírněných	umírněný	k2eAgInPc2d1	umírněný
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
hlasováních	hlasování	k1gNnPc6	hlasování
<g/>
.	.	kIx.	.
</s>
<s>
Bolševický	bolševický	k2eAgMnSc1d1	bolševický
politik	politik	k1gMnSc1	politik
Lev	Lev	k1gMnSc1	Lev
Davidovič	Davidovič	k1gMnSc1	Davidovič
Trockij	Trockij	k1gMnSc1	Trockij
<g/>
,	,	kIx,	,
nedávno	nedávno	k6eAd1	nedávno
propuštěný	propuštěný	k2eAgMnSc1d1	propuštěný
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
předsedou	předseda	k1gMnSc7	předseda
sovětu	sovět	k1gInSc2	sovět
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
sto	sto	k4xCgNnSc4	sto
sovětů	sovět	k1gInPc2	sovět
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
země	zem	k1gFnSc2	zem
žádalo	žádat	k5eAaImAgNnS	žádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Všeruský	všeruský	k2eAgInSc1d1	všeruský
ústřední	ústřední	k2eAgInSc1d1	ústřední
výkonný	výkonný	k2eAgInSc1d1	výkonný
výbor	výbor	k1gInSc1	výbor
–	–	k?	–
stále	stále	k6eAd1	stále
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
eserů	eser	k1gMnPc2	eser
a	a	k8xC	a
menševiků	menševik	k1gMnPc2	menševik
–	–	k?	–
převzal	převzít	k5eAaPmAgInS	převzít
moc	moc	k6eAd1	moc
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
významných	významný	k2eAgFnPc6d1	významná
lokalitách	lokalita	k1gFnPc6	lokalita
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
bolševici	bolševik	k1gMnPc1	bolševik
místní	místní	k2eAgMnPc1d1	místní
sověty	sovět	k1gInPc4	sovět
<g/>
.	.	kIx.	.
</s>
<s>
Baltské	baltský	k2eAgNnSc1d1	Baltské
loďstvo	loďstvo	k1gNnSc1	loďstvo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
blízko	blízko	k6eAd1	blízko
od	od	k7c2	od
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
uznávat	uznávat	k5eAaImF	uznávat
Kerenského	Kerenský	k2eAgMnSc4d1	Kerenský
<g/>
.	.	kIx.	.
</s>
<s>
Rolníci	rolník	k1gMnPc1	rolník
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
si	se	k3xPyFc3	se
zvolili	zvolit	k5eAaPmAgMnP	zvolit
bolševického	bolševický	k2eAgMnSc4d1	bolševický
delegáta	delegát	k1gMnSc4	delegát
pro	pro	k7c4	pro
připravovaný	připravovaný	k2eAgInSc4d1	připravovaný
předparlament	předparlament	k1gInSc4	předparlament
(	(	kIx(	(
<g/>
Prozatímní	prozatímní	k2eAgFnSc4d1	prozatímní
radu	rada	k1gFnSc4	rada
Ruské	ruský	k2eAgFnSc2d1	ruská
republiky	republika	k1gFnSc2	republika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
městech	město	k1gNnPc6	město
značně	značně	k6eAd1	značně
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
podpora	podpora	k1gFnSc1	podpora
bolševiků	bolševik	k1gMnPc2	bolševik
<g/>
.	.	kIx.	.
<g/>
Předparlament	Předparlament	k1gMnSc1	Předparlament
se	se	k3xPyFc4	se
sešel	sejít	k5eAaPmAgMnS	sejít
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
diskutoval	diskutovat	k5eAaImAgMnS	diskutovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jaká	jaký	k3yIgFnSc1	jaký
vláda	vláda	k1gFnSc1	vláda
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
nahradit	nahradit	k5eAaPmF	nahradit
nouzový	nouzový	k2eAgInSc4d1	nouzový
kabinet	kabinet	k1gInSc4	kabinet
(	(	kIx(	(
<g/>
direktorium	direktorium	k1gNnSc1	direktorium
<g/>
)	)	kIx)	)
vytvořený	vytvořený	k2eAgInSc1d1	vytvořený
Kerenským	Kerenský	k2eAgInPc3d1	Kerenský
po	po	k7c6	po
Kornilovově	Kornilovův	k2eAgInSc6d1	Kornilovův
puči	puč	k1gInSc6	puč
<g/>
.	.	kIx.	.
</s>
<s>
Bolševici	bolševik	k1gMnPc1	bolševik
prosazovali	prosazovat	k5eAaImAgMnP	prosazovat
konec	konec	k1gInSc4	konec
koalic	koalice	k1gFnPc2	koalice
s	s	k7c7	s
buržoazními	buržoazní	k2eAgFnPc7d1	buržoazní
stranami	strana	k1gFnPc7	strana
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc2	vytvoření
nového	nový	k2eAgNnSc2d1	nové
<g/>
,	,	kIx,	,
výhradně	výhradně	k6eAd1	výhradně
socialistického	socialistický	k2eAgInSc2d1	socialistický
kabinetu	kabinet	k1gInSc2	kabinet
<g/>
;	;	kIx,	;
tuto	tento	k3xDgFnSc4	tento
pozici	pozice	k1gFnSc4	pozice
zastávali	zastávat	k5eAaImAgMnP	zastávat
Kameněv	Kameněv	k1gMnPc1	Kameněv
a	a	k8xC	a
Trockij	Trockij	k1gMnPc1	Trockij
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
rozdíly	rozdíl	k1gInPc4	rozdíl
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
–	–	k?	–
Trockij	Trockij	k1gFnSc2	Trockij
vnímal	vnímat	k5eAaImAgInS	vnímat
novou	nový	k2eAgFnSc4d1	nová
vládu	vláda	k1gFnSc4	vláda
jako	jako	k8xS	jako
první	první	k4xOgInSc4	první
krok	krok	k1gInSc4	krok
k	k	k7c3	k
přenesení	přenesení	k1gNnSc3	přenesení
moci	moc	k1gFnSc2	moc
na	na	k7c4	na
sověty	sovět	k1gInPc4	sovět
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Kameněv	Kameněv	k1gFnSc1	Kameněv
jako	jako	k8xC	jako
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
zajistit	zajistit	k5eAaPmF	zajistit
konání	konání	k1gNnSc4	konání
Ústavodárného	ústavodárný	k2eAgNnSc2d1	Ústavodárné
shromáždění	shromáždění	k1gNnSc2	shromáždění
–	–	k?	–
oba	dva	k4xCgMnPc1	dva
stále	stále	k6eAd1	stále
spoléhali	spoléhat	k5eAaImAgMnP	spoléhat
na	na	k7c4	na
možnost	možnost	k1gFnSc4	možnost
prohloubit	prohloubit	k5eAaPmF	prohloubit
revoluci	revoluce	k1gFnSc4	revoluce
mírovými	mírový	k2eAgInPc7d1	mírový
prostředky	prostředek	k1gInPc7	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
postoj	postoj	k1gInSc1	postoj
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
podporovaný	podporovaný	k2eAgInSc1d1	podporovaný
vůdcem	vůdce	k1gMnSc7	vůdce
bolševiků	bolševik	k1gMnPc2	bolševik
Leninem	Lenin	k1gMnSc7	Lenin
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vážně	vážně	k6eAd1	vážně
ohrožen	ohrozit	k5eAaPmNgInS	ohrozit
náhlou	náhlý	k2eAgFnSc7d1	náhlá
změnou	změna	k1gFnSc7	změna
Leninova	Leninův	k2eAgInSc2d1	Leninův
postoje	postoj	k1gInSc2	postoj
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
se	s	k7c7	s
15	[number]	k4	15
<g/>
.	.	kIx.	.
záříjul	záříjout	k5eAaPmAgInS	záříjout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
promítla	promítnout	k5eAaPmAgFnS	promítnout
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
dopisů	dopis	k1gInPc2	dopis
adresovaných	adresovaný	k2eAgFnPc2d1	adresovaná
ústřednímu	ústřední	k2eAgInSc3d1	ústřední
výboru	výbor	k1gInSc3	výbor
bolševické	bolševický	k2eAgFnSc2d1	bolševická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgNnPc6	který
Lenin	Lenin	k1gMnSc1	Lenin
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
umírněnost	umírněnost	k1gFnSc4	umírněnost
a	a	k8xC	a
požadoval	požadovat	k5eAaImAgMnS	požadovat
okamžité	okamžitý	k2eAgNnSc4d1	okamžité
povstání	povstání	k1gNnSc4	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Ústřední	ústřední	k2eAgInSc1d1	ústřední
výbor	výbor	k1gInSc1	výbor
<g/>
,	,	kIx,	,
překvapený	překvapený	k2eAgInSc1d1	překvapený
novým	nový	k2eAgInSc7d1	nový
postojem	postoj	k1gInSc7	postoj
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
požadavky	požadavek	k1gInPc7	požadavek
Lenina	Lenin	k2eAgNnSc2d1	Lenino
ignorovat	ignorovat	k5eAaImF	ignorovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nebyla	být	k5eNaImAgFnS	být
podkopána	podkopán	k2eAgFnSc1d1	podkopána
shoda	shoda	k1gFnSc1	shoda
levice	levice	k1gFnSc2	levice
nastolená	nastolený	k2eAgFnSc1d1	nastolená
spoluprací	spolupráce	k1gFnSc7	spolupráce
socialistů	socialist	k1gMnPc2	socialist
během	během	k7c2	během
konfrontace	konfrontace	k1gFnSc2	konfrontace
s	s	k7c7	s
Kornilovem	Kornilovo	k1gNnSc7	Kornilovo
<g/>
.	.	kIx.	.
<g/>
Aby	aby	kYmCp3nS	aby
zlomil	zlomit	k5eAaPmAgMnS	zlomit
opozici	opozice	k1gFnSc4	opozice
většiny	většina	k1gFnSc2	většina
bolševického	bolševický	k2eAgInSc2d1	bolševický
ústředního	ústřední	k2eAgInSc2d1	ústřední
výboru	výbor	k1gInSc2	výbor
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
nechtěla	chtít	k5eNaImAgFnS	chtít
přijmout	přijmout	k5eAaPmF	přijmout
požadavek	požadavek	k1gInSc4	požadavek
na	na	k7c4	na
povstání	povstání	k1gNnSc4	povstání
a	a	k8xC	a
uchopení	uchopení	k1gNnSc4	uchopení
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
Lenin	Lenin	k1gMnSc1	Lenin
hrozil	hrozit	k5eAaImAgMnS	hrozit
odstoupením	odstoupení	k1gNnSc7	odstoupení
z	z	k7c2	z
výboru	výbor	k1gInSc2	výbor
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
však	však	k9	však
nemělo	mít	k5eNaImAgNnS	mít
na	na	k7c4	na
postoj	postoj	k1gInSc4	postoj
ostatních	ostatní	k2eAgInPc2d1	ostatní
členů	člen	k1gInPc2	člen
žádný	žádný	k3yNgInSc4	žádný
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
října	říjen	k1gInSc2	říjen
Lenin	Lenin	k1gMnSc1	Lenin
zintenzívnil	zintenzívnit	k5eAaPmAgMnS	zintenzívnit
svou	svůj	k3xOyFgFnSc4	svůj
kampaň	kampaň	k1gFnSc4	kampaň
za	za	k7c4	za
okamžité	okamžitý	k2eAgNnSc4d1	okamžité
převzetí	převzetí	k1gNnSc4	převzetí
moci	moct	k5eAaImF	moct
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k8xS	jak
uvnitř	uvnitř	k7c2	uvnitř
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
u	u	k7c2	u
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Vydal	vydat	k5eAaPmAgMnS	vydat
několik	několik	k4yIc4	několik
spisů	spis	k1gInPc2	spis
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgFnPc6	který
zdůvodňoval	zdůvodňovat	k5eAaImAgMnS	zdůvodňovat
svůj	svůj	k3xOyFgInSc4	svůj
postoj	postoj	k1gInSc4	postoj
vnitrostátní	vnitrostátní	k2eAgFnSc1d1	vnitrostátní
i	i	k8xC	i
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
situací	situace	k1gFnSc7	situace
(	(	kIx(	(
<g/>
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
podpora	podpora	k1gFnSc1	podpora
bolševiků	bolševik	k1gMnPc2	bolševik
<g/>
,	,	kIx,	,
rozmach	rozmach	k1gInSc4	rozmach
revolučních	revoluční	k2eAgFnPc2d1	revoluční
nálad	nálada	k1gFnPc2	nálada
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
<g/>
,	,	kIx,	,
nepokoje	nepokoj	k1gInSc2	nepokoj
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
armádě	armáda	k1gFnSc6	armáda
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Předparlament	Předparlament	k1gInSc1	Předparlament
nakonec	nakonec	k6eAd1	nakonec
umožnil	umožnit	k5eAaPmAgInS	umožnit
Kerenskému	Kerenský	k2eAgInSc3d1	Kerenský
vytvořit	vytvořit	k5eAaPmF	vytvořit
novou	nový	k2eAgFnSc4d1	nová
vládu	vláda	k1gFnSc4	vláda
s	s	k7c7	s
konstitučními	konstituční	k2eAgMnPc7d1	konstituční
demokraty	demokrat	k1gMnPc7	demokrat
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
liberálními	liberální	k2eAgFnPc7d1	liberální
frakcemi	frakce	k1gFnPc7	frakce
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
Lenina	Lenin	k1gMnSc2	Lenin
přimělo	přimět	k5eAaPmAgNnS	přimět
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
znovu	znovu	k6eAd1	znovu
přehodnotil	přehodnotit	k5eAaPmAgMnS	přehodnotit
svůj	svůj	k3xOyFgInSc4	svůj
názor	názor	k1gInSc4	názor
<g/>
.	.	kIx.	.
</s>
<s>
Pokus	pokus	k1gInSc1	pokus
předparlamentu	předparlament	k1gInSc2	předparlament
utvořit	utvořit	k5eAaPmF	utvořit
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
vládu	vláda	k1gFnSc4	vláda
selhal	selhat	k5eAaPmAgMnS	selhat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
naděje	naděje	k1gFnSc1	naděje
většiny	většina	k1gFnSc2	většina
bolševického	bolševický	k2eAgInSc2d1	bolševický
ústředního	ústřední	k2eAgInSc2d1	ústřední
výboru	výbor	k1gInSc2	výbor
upřela	upřít	k5eAaPmAgFnS	upřít
na	na	k7c4	na
příští	příští	k2eAgInSc4d1	příští
sjezd	sjezd	k1gInSc4	sjezd
sovětů	sovět	k1gInPc2	sovět
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnPc1d3	veliký
radikálové	radikál	k1gMnPc1	radikál
doufali	doufat	k5eAaImAgMnP	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
sjezd	sjezd	k1gInSc1	sjezd
mohl	moct	k5eAaImAgInS	moct
přenést	přenést	k5eAaPmF	přenést
moc	moc	k6eAd1	moc
na	na	k7c4	na
krajně	krajně	k6eAd1	krajně
levicovou	levicový	k2eAgFnSc4d1	levicová
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
rychle	rychle	k6eAd1	rychle
zavedla	zavést	k5eAaPmAgFnS	zavést
radikální	radikální	k2eAgNnSc4d1	radikální
opatření	opatření	k1gNnSc4	opatření
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
umírněnější	umírněný	k2eAgMnPc4d2	umírněnější
bolševiky	bolševik	k1gMnPc4	bolševik
by	by	kYmCp3nS	by
nová	nový	k2eAgFnSc1d1	nová
prozatímní	prozatímní	k2eAgFnSc1d1	prozatímní
vláda	vláda	k1gFnSc1	vláda
zaručila	zaručit	k5eAaPmAgFnS	zaručit
volby	volba	k1gFnPc4	volba
do	do	k7c2	do
Ústavodárného	ústavodárný	k2eAgNnSc2d1	Ústavodárné
shromáždění	shromáždění	k1gNnSc2	shromáždění
a	a	k8xC	a
jeho	on	k3xPp3gNnSc2	on
konání	konání	k1gNnSc2	konání
<g/>
.	.	kIx.	.
</s>
<s>
Petrohradský	petrohradský	k2eAgInSc1d1	petrohradský
sovět	sovět	k1gInSc1	sovět
nazval	nazvat	k5eAaPmAgInS	nazvat
nový	nový	k2eAgInSc1d1	nový
kabinet	kabinet	k1gInSc1	kabinet
"	"	kIx"	"
<g/>
vládou	vláda	k1gFnSc7	vláda
pro	pro	k7c4	pro
občanskou	občanský	k2eAgFnSc4d1	občanská
válku	válka	k1gFnSc4	válka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
mu	on	k3xPp3gMnSc3	on
poskytnout	poskytnout	k5eAaPmF	poskytnout
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
podporu	podpora	k1gFnSc4	podpora
a	a	k8xC	a
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
příští	příští	k2eAgInSc1d1	příští
sjezd	sjezd	k1gInSc1	sjezd
sovětů	sovět	k1gInPc2	sovět
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
kabinet	kabinet	k1gInSc4	kabinet
nový	nový	k2eAgInSc4d1	nový
a	a	k8xC	a
"	"	kIx"	"
<g/>
opravdu	opravdu	k6eAd1	opravdu
revoluční	revoluční	k2eAgInSc1d1	revoluční
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Bolševici	bolševik	k1gMnPc1	bolševik
předparlament	předparlament	k1gInSc4	předparlament
opustili	opustit	k5eAaPmAgMnP	opustit
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
agitovat	agitovat	k5eAaImF	agitovat
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
vytvoření	vytvoření	k1gNnSc2	vytvoření
nové	nový	k2eAgFnSc2d1	nová
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
proti	proti	k7c3	proti
Kerenskému	Kerenský	k2eAgInSc3d1	Kerenský
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc3	jeho
příznivcům	příznivec	k1gMnPc3	příznivec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přípravy	příprava	k1gFnPc1	příprava
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
ústředního	ústřední	k2eAgInSc2d1	ústřední
výboru	výbor	k1gInSc2	výbor
a	a	k8xC	a
nedostatky	nedostatek	k1gInPc4	nedostatek
organizace	organizace	k1gFnSc2	organizace
===	===	k?	===
</s>
</p>
<p>
<s>
Navzdory	navzdory	k7c3	navzdory
Leninovu	Leninův	k2eAgNnSc3d1	Leninovo
přání	přání	k1gNnSc3	přání
okamžitého	okamžitý	k2eAgNnSc2d1	okamžité
uchopení	uchopení	k1gNnSc2	uchopení
moci	moc	k1gFnSc2	moc
bolševickou	bolševický	k2eAgFnSc7d1	bolševická
stranou	strana	k1gFnSc7	strana
po	po	k7c6	po
zprávách	zpráva	k1gFnPc6	zpráva
o	o	k7c6	o
postoji	postoj	k1gInSc6	postoj
dělníků	dělník	k1gMnPc2	dělník
a	a	k8xC	a
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
podporujících	podporující	k2eAgNnPc2d1	podporující
přenesení	přenesení	k1gNnPc2	přenesení
moci	moct	k5eAaImF	moct
na	na	k7c4	na
sověty	sovět	k1gInPc4	sovět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikoliv	nikoliv	k9	nikoliv
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
bolševické	bolševický	k2eAgNnSc1d1	bolševické
vedení	vedení	k1gNnSc1	vedení
zaměřilo	zaměřit	k5eAaPmAgNnS	zaměřit
své	své	k1gNnSc4	své
úsilí	úsilí	k1gNnSc2	úsilí
na	na	k7c4	na
Druhý	druhý	k4xOgInSc4	druhý
sjezd	sjezd	k1gInSc4	sjezd
sovětů	sovět	k1gInPc2	sovět
s	s	k7c7	s
přáním	přání	k1gNnSc7	přání
předat	předat	k5eAaPmF	předat
vládní	vládní	k2eAgFnSc4d1	vládní
moc	moc	k1gFnSc4	moc
jemu	on	k3xPp3gMnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
zastáncem	zastánce	k1gMnSc7	zastánce
tohoto	tento	k3xDgInSc2	tento
názoru	názor	k1gInSc2	názor
byl	být	k5eAaImAgInS	být
Trockij	Trockij	k1gFnSc4	Trockij
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
v	v	k7c6	v
ústředním	ústřední	k2eAgInSc6d1	ústřední
výboru	výbor	k1gInSc6	výbor
nemalá	malý	k2eNgFnSc1d1	nemalá
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
vedená	vedený	k2eAgFnSc1d1	vedená
Kameněvem	Kameněv	k1gInSc7	Kameněv
a	a	k8xC	a
Zinovjevem	Zinovjev	k1gInSc7	Zinovjev
<g/>
,	,	kIx,	,
s	s	k7c7	s
obavami	obava	k1gFnPc7	obava
sledovala	sledovat	k5eAaImAgFnS	sledovat
snahu	snaha	k1gFnSc4	snaha
Lenina	Lenin	k1gMnSc2	Lenin
vyvolat	vyvolat	k5eAaPmF	vyvolat
povstání	povstání	k1gNnSc4	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proud	proud	k1gInSc1	proud
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgInS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
situace	situace	k1gFnSc1	situace
není	být	k5eNaImIp3nS	být
k	k	k7c3	k
ozbrojenému	ozbrojený	k2eAgInSc3d1	ozbrojený
povstání	povstání	k1gNnSc6	povstání
příznivá	příznivý	k2eAgFnSc1d1	příznivá
a	a	k8xC	a
raději	rád	k6eAd2	rád
se	se	k3xPyFc4	se
spoléhal	spoléhat	k5eAaImAgInS	spoléhat
na	na	k7c4	na
sověty	sovět	k1gInPc4	sovět
a	a	k8xC	a
na	na	k7c4	na
budoucí	budoucí	k2eAgNnSc4d1	budoucí
ústavodárné	ústavodárný	k2eAgNnSc4d1	Ústavodárné
shromáždění	shromáždění	k1gNnSc4	shromáždění
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
revoluci	revoluce	k1gFnSc6	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
státním	státní	k2eAgInSc6d1	státní
převratu	převrat	k1gInSc6	převrat
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
nové	nový	k2eAgFnSc3d1	nová
vládě	vláda	k1gFnSc3	vláda
spojila	spojit	k5eAaPmAgFnS	spojit
celá	celá	k1gFnSc1	celá
buržoazie	buržoazie	k1gFnSc1	buržoazie
a	a	k8xC	a
že	že	k8xS	že
by	by	kYmCp3nP	by
povstalci	povstalec	k1gMnPc1	povstalec
nebyli	být	k5eNaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
vojensky	vojensky	k6eAd1	vojensky
čelit	čelit	k5eAaImF	čelit
Německu	Německo	k1gNnSc3	Německo
a	a	k8xC	a
nemohli	moct	k5eNaImAgMnP	moct
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
spoléhat	spoléhat	k5eAaImF	spoléhat
na	na	k7c4	na
silnější	silný	k2eAgFnSc4d2	silnější
podporu	podpora	k1gFnSc4	podpora
světového	světový	k2eAgInSc2d1	světový
proletariátu	proletariát	k1gInSc2	proletariát
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Lenin	Lenin	k1gMnSc1	Lenin
nakonec	nakonec	k6eAd1	nakonec
prosadil	prosadit	k5eAaPmAgMnS	prosadit
svůj	svůj	k3xOyFgInSc4	svůj
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
začít	začít	k5eAaPmF	začít
povstání	povstání	k1gNnSc4	povstání
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
stanoveno	stanoven	k2eAgNnSc1d1	stanoveno
datum	datum	k1gNnSc1	datum
a	a	k8xC	a
ústřední	ústřední	k2eAgInSc1d1	ústřední
výbor	výbor	k1gInSc1	výbor
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
<g/>
.	.	kIx.	.
<g/>
Aby	aby	kYmCp3nS	aby
Lenin	Lenin	k1gMnSc1	Lenin
ospravedlnil	ospravedlnit	k5eAaPmAgMnS	ospravedlnit
svůj	svůj	k3xOyFgInSc4	svůj
záměr	záměr	k1gInSc4	záměr
okamžitě	okamžitě	k6eAd1	okamžitě
začít	začít	k5eAaPmF	začít
s	s	k7c7	s
povstáním	povstání	k1gNnSc7	povstání
<g/>
,	,	kIx,	,
předkládal	předkládat	k5eAaImAgInS	předkládat
různá	různý	k2eAgNnPc4d1	různé
odůvodnění	odůvodnění	k1gNnPc4	odůvodnění
<g/>
:	:	kIx,	:
blížící	blížící	k2eAgNnSc4d1	blížící
se	se	k3xPyFc4	se
podepsání	podepsání	k1gNnSc4	podepsání
míru	mír	k1gInSc2	mír
mezi	mezi	k7c7	mezi
stranami	strana	k1gFnPc7	strana
Velké	velká	k1gFnSc2	velká
<g />
.	.	kIx.	.
</s>
<s>
války	válka	k1gFnPc1	válka
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
dohody	dohoda	k1gFnPc4	dohoda
mezi	mezi	k7c7	mezi
imperialistickými	imperialistický	k2eAgFnPc7d1	imperialistická
mocnostmi	mocnost	k1gFnPc7	mocnost
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc4	ten
vnímal	vnímat	k5eAaImAgInS	vnímat
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
že	že	k8xS	že
se	se	k3xPyFc4	se
Kerenskij	Kerenskij	k1gFnSc1	Kerenskij
chystá	chystat	k5eAaImIp3nS	chystat
vydat	vydat	k5eAaPmF	vydat
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Němcům	Němec	k1gMnPc3	Němec
<g/>
;	;	kIx,	;
že	že	k8xS	že
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
další	další	k2eAgInSc1d1	další
pravicový	pravicový	k2eAgInSc1d1	pravicový
převrat	převrat	k1gInSc1	převrat
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byl	být	k5eAaImAgInS	být
Kornilovův	Kornilovův	k2eAgMnSc1d1	Kornilovův
<g/>
;	;	kIx,	;
nebo	nebo	k8xC	nebo
že	že	k8xS	že
triumf	triumf	k1gInSc1	triumf
ruské	ruský	k2eAgFnSc2d1	ruská
a	a	k8xC	a
světové	světový	k2eAgFnSc2d1	světová
revoluce	revoluce	k1gFnSc2	revoluce
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
dosažen	dosáhnout	k5eAaPmNgInS	dosáhnout
jen	jen	k9	jen
po	po	k7c6	po
malém	malý	k2eAgInSc6d1	malý
boji	boj	k1gInSc6	boj
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
Prozatímní	prozatímní	k2eAgFnSc2d1	prozatímní
vlády	vláda	k1gFnSc2	vláda
byla	být	k5eAaImAgFnS	být
opravdu	opravdu	k6eAd1	opravdu
velmi	velmi	k6eAd1	velmi
vážná	vážný	k2eAgFnSc1d1	vážná
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
ztrácela	ztrácet	k5eAaImAgFnS	ztrácet
autoritu	autorita	k1gFnSc4	autorita
<g/>
;	;	kIx,	;
vojenská	vojenský	k2eAgFnSc1d1	vojenská
situace	situace	k1gFnSc1	situace
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
frontě	fronta	k1gFnSc6	fronta
u	u	k7c2	u
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
byla	být	k5eAaImAgFnS	být
katastrofická	katastrofický	k2eAgFnSc1d1	katastrofická
a	a	k8xC	a
Kerenskij	Kerenskij	k1gMnPc7	Kerenskij
neměl	mít	k5eNaImAgInS	mít
žádné	žádný	k3yNgFnPc4	žádný
záruky	záruka	k1gFnPc4	záruka
loajality	loajalita	k1gFnSc2	loajalita
městské	městský	k2eAgFnSc2d1	městská
posádky	posádka	k1gFnSc2	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatek	nedostatek	k1gInSc1	nedostatek
pohonných	pohonný	k2eAgFnPc2d1	pohonná
hmot	hmota	k1gFnPc2	hmota
a	a	k8xC	a
potravin	potravina	k1gFnPc2	potravina
způsobil	způsobit	k5eAaPmAgMnS	způsobit
růst	růst	k1gInSc4	růst
inflace	inflace	k1gFnSc2	inflace
<g/>
.	.	kIx.	.
</s>
<s>
Dosavadní	dosavadní	k2eAgFnSc1d1	dosavadní
apatie	apatie	k1gFnSc1	apatie
populace	populace	k1gFnSc2	populace
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
snadno	snadno	k6eAd1	snadno
proměnit	proměnit	k5eAaPmF	proměnit
v	v	k7c6	v
rebelii	rebelie	k1gFnSc6	rebelie
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
nemohla	moct	k5eNaImAgFnS	moct
spoléhat	spoléhat	k5eAaImF	spoléhat
na	na	k7c4	na
účinnou	účinný	k2eAgFnSc4d1	účinná
podporu	podpora	k1gFnSc4	podpora
Všeruského	všeruský	k2eAgInSc2d1	všeruský
ústředního	ústřední	k2eAgInSc2d1	ústřední
výkonného	výkonný	k2eAgInSc2d1	výkonný
výboru	výbor	k1gInSc2	výbor
<g/>
,	,	kIx,	,
izolovaného	izolovaný	k2eAgInSc2d1	izolovaný
od	od	k7c2	od
mas	maso	k1gNnPc2	maso
<g/>
.	.	kIx.	.
</s>
<s>
Petrohradský	petrohradský	k2eAgInSc1d1	petrohradský
sovět	sovět	k1gInSc1	sovět
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
bolševiků	bolševik	k1gMnPc2	bolševik
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
vedl	vést	k5eAaImAgMnS	vést
Trockij	Trockij	k1gMnSc1	Trockij
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
24	[number]	k4	24
<g/>
.	.	kIx.	.
záříjul	záříjout	k5eAaPmAgInS	záříjout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1917	[number]	k4	1917
<g/>
greg	greg	k1gInSc1	greg
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
bolševici	bolševik	k1gMnPc1	bolševik
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
soustředit	soustředit	k5eAaPmF	soustředit
na	na	k7c4	na
získání	získání	k1gNnSc4	získání
co	co	k9	co
největšího	veliký	k2eAgInSc2d3	veliký
počtu	počet	k1gInSc2	počet
sovětů	sovět	k1gInPc2	sovět
<g/>
,	,	kIx,	,
ovládaných	ovládaný	k2eAgFnPc2d1	ovládaná
od	od	k7c2	od
jara	jaro	k1gNnSc2	jaro
umírněnými	umírněný	k2eAgMnPc7d1	umírněný
socialisty	socialist	k1gMnPc7	socialist
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
právě	právě	k6eAd1	právě
probíhajících	probíhající	k2eAgFnPc2d1	probíhající
voleb	volba	k1gFnPc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Podobnou	podobný	k2eAgFnSc4d1	podobná
strategii	strategie	k1gFnSc4	strategie
přijali	přijmout	k5eAaPmAgMnP	přijmout
i	i	k9	i
leví	levý	k2eAgMnPc1d1	levý
eseři	eser	k1gMnPc1	eser
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
spojenci	spojenec	k1gMnPc1	spojenec
bolševiků	bolševik	k1gMnPc2	bolševik
<g/>
.	.	kIx.	.
</s>
<s>
Mobilizace	mobilizace	k1gFnSc1	mobilizace
sil	síla	k1gFnPc2	síla
radikálních	radikální	k2eAgFnPc2d1	radikální
levicových	levicový	k2eAgFnPc2d1	levicová
stran	strana	k1gFnPc2	strana
v	v	k7c4	v
předvečer	předvečer	k1gInSc4	předvečer
sjezdu	sjezd	k1gInSc2	sjezd
sovětů	sovět	k1gInPc2	sovět
nastala	nastat	k5eAaPmAgFnS	nastat
zčásti	zčásti	k6eAd1	zčásti
kvůli	kvůli	k7c3	kvůli
přetrvávajícím	přetrvávající	k2eAgFnPc3d1	přetrvávající
pověstem	pověst	k1gFnPc3	pověst
o	o	k7c6	o
možné	možný	k2eAgFnSc6d1	možná
kontrarevoluci	kontrarevoluce	k1gFnSc6	kontrarevoluce
nebo	nebo	k8xC	nebo
zrušení	zrušení	k1gNnSc6	zrušení
sjezdu	sjezd	k1gInSc2	sjezd
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
svolání	svolání	k1gNnSc4	svolání
umírnění	umírněný	k2eAgMnPc1d1	umírněný
socialisté	socialist	k1gMnPc1	socialist
přijali	přijmout	k5eAaPmAgMnP	přijmout
jen	jen	k9	jen
neochotně	ochotně	k6eNd1	ochotně
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
říjnajul	říjnajout	k5eAaPmAgInS	říjnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1917	[number]	k4	1917
<g/>
greg	greg	k1gInSc1	greg
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
Lenin	Lenin	k1gMnSc1	Lenin
vrátil	vrátit	k5eAaPmAgMnS	vrátit
z	z	k7c2	z
Finska	Finsko	k1gNnSc2	Finsko
do	do	k7c2	do
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
a	a	k8xC	a
bolševici	bolševik	k1gMnPc1	bolševik
opustili	opustit	k5eAaPmAgMnP	opustit
předparlament	předparlament	k1gInSc4	předparlament
<g/>
.	.	kIx.	.
</s>
<s>
Trockij	Trockít	k5eAaPmRp2nS	Trockít
přijal	přijmout	k5eAaPmAgMnS	přijmout
zásadní	zásadní	k2eAgNnSc4d1	zásadní
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
<g/>
:	:	kIx,	:
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
využije	využít	k5eAaPmIp3nS	využít
pověsti	pověst	k1gFnPc4	pověst
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dočasná	dočasný	k2eAgFnSc1d1	dočasná
vláda	vláda	k1gFnSc1	vláda
chce	chtít	k5eAaImIp3nS	chtít
opustit	opustit	k5eAaPmF	opustit
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
ovládnutí	ovládnutí	k1gNnSc4	ovládnutí
vojenské	vojenský	k2eAgFnSc2d1	vojenská
posádky	posádka	k1gFnSc2	posádka
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
a	a	k8xC	a
rozdání	rozdání	k1gNnPc2	rozdání
zbraní	zbraň	k1gFnPc2	zbraň
dělníkům	dělník	k1gMnPc3	dělník
jako	jako	k8xC	jako
obranná	obranný	k2eAgNnPc1d1	obranné
opatření	opatření	k1gNnPc1	opatření
proti	proti	k7c3	proti
nepřátelům	nepřítel	k1gMnPc3	nepřítel
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
vnějším	vnější	k2eAgNnSc6d1	vnější
(	(	kIx(	(
<g/>
Němci	Němec	k1gMnPc7	Němec
<g/>
)	)	kIx)	)
i	i	k8xC	i
vnitřním	vnitřní	k2eAgMnSc6d1	vnitřní
(	(	kIx(	(
<g/>
kontrarevoluce	kontrarevoluce	k1gFnSc1	kontrarevoluce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
Trockij	Trockij	k1gMnSc1	Trockij
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
svá	svůj	k3xOyFgNnPc4	svůj
podezření	podezření	k1gNnPc4	podezření
na	na	k7c6	na
sjezdu	sjezd	k1gInSc6	sjezd
sovětů	sovět	k1gInPc2	sovět
severní	severní	k2eAgFnSc2d1	severní
oblasti	oblast	k1gFnSc2	oblast
s	s	k7c7	s
úmyslem	úmysl	k1gInSc7	úmysl
získat	získat	k5eAaPmF	získat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
stranu	strana	k1gFnSc4	strana
vojáky	voják	k1gMnPc4	voják
rozmístěné	rozmístěný	k2eAgFnSc3d1	rozmístěná
kolem	kolo	k1gNnSc7	kolo
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
sjezdu	sjezd	k1gInSc6	sjezd
byla	být	k5eAaImAgFnS	být
zřejmá	zřejmý	k2eAgFnSc1d1	zřejmá
ohromná	ohromný	k2eAgFnSc1d1	ohromná
podpora	podpora	k1gFnSc1	podpora
bolševiků	bolševik	k1gMnPc2	bolševik
z	z	k7c2	z
blízkosti	blízkost	k1gFnSc2	blízkost
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
a	a	k8xC	a
vojáků	voják	k1gMnPc2	voják
umístěných	umístěný	k2eAgMnPc2d1	umístěný
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
bolševici	bolševik	k1gMnPc1	bolševik
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nesouhlasili	souhlasit	k5eNaImAgMnP	souhlasit
s	s	k7c7	s
povstáním	povstání	k1gNnSc7	povstání
<g/>
,	,	kIx,	,
rozběhli	rozběhnout	k5eAaPmAgMnP	rozběhnout
kampaň	kampaň	k1gFnSc4	kampaň
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
lidem	člověk	k1gMnPc3	člověk
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
jejich	jejich	k3xOp3gInPc4	jejich
důvody	důvod	k1gInPc4	důvod
<g/>
.	.	kIx.	.
<g/>
Lenin	Lenin	k1gMnSc1	Lenin
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
bolševickým	bolševický	k2eAgInSc7d1	bolševický
ústředním	ústřední	k2eAgInSc7d1	ústřední
výborem	výbor	k1gInSc7	výbor
10	[number]	k4	10
<g/>
.	.	kIx.	.
říjnajul	říjnajout	k5eAaPmAgInS	říjnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
a	a	k8xC	a
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gInSc3	on
prosadit	prosadit	k5eAaPmF	prosadit
názor	názor	k1gInSc4	názor
o	o	k7c6	o
potřebě	potřeba	k1gFnSc6	potřeba
ozbrojeného	ozbrojený	k2eAgNnSc2d1	ozbrojené
povstání	povstání	k1gNnSc2	povstání
proti	proti	k7c3	proti
vládě	vláda	k1gFnSc3	vláda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
ke	k	k7c3	k
konkrétnímu	konkrétní	k2eAgInSc3d1	konkrétní
termínu	termín	k1gInSc3	termín
před	před	k7c7	před
Sjezdem	sjezd	k1gInSc7	sjezd
sovětů	sovět	k1gInPc2	sovět
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
bylo	být	k5eAaImAgNnS	být
nejednoznačné	jednoznačný	k2eNgNnSc1d1	nejednoznačné
a	a	k8xC	a
spíš	spíš	k9	spíš
zdůraznilo	zdůraznit	k5eAaPmAgNnS	zdůraznit
rozdíly	rozdíl	k1gInPc4	rozdíl
mezi	mezi	k7c7	mezi
Leninovými	Leninův	k2eAgInPc7d1	Leninův
radikály	radikál	k1gInPc7	radikál
a	a	k8xC	a
umírněným	umírněný	k2eAgInSc7d1	umírněný
směrem	směr	k1gInSc7	směr
Kameněva	Kameněvo	k1gNnSc2	Kameněvo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dnech	den	k1gInPc6	den
následujících	následující	k2eAgInPc6d1	následující
po	po	k7c6	po
zasedání	zasedání	k1gNnSc6	zasedání
ústředního	ústřední	k2eAgInSc2d1	ústřední
výboru	výbor	k1gInSc2	výbor
nebyla	být	k5eNaImAgFnS	být
prováděna	provádět	k5eAaImNgFnS	provádět
žádná	žádný	k3yNgFnSc1	žádný
příprava	příprava	k1gFnSc1	příprava
na	na	k7c4	na
okamžité	okamžitý	k2eAgNnSc4d1	okamžité
povstání	povstání	k1gNnSc4	povstání
a	a	k8xC	a
bolševické	bolševický	k2eAgInPc4d1	bolševický
kádry	kádr	k1gInPc4	kádr
ani	ani	k8xC	ani
nebyly	být	k5eNaImAgFnP	být
připraveny	připravit	k5eAaPmNgFnP	připravit
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
prováděly	provádět	k5eAaImAgInP	provádět
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
organizace	organizace	k1gFnSc2	organizace
a	a	k8xC	a
vedení	vedení	k1gNnSc2	vedení
<g/>
.	.	kIx.	.
</s>
<s>
Neexistoval	existovat	k5eNaImAgInS	existovat
žádný	žádný	k3yNgInSc1	žádný
plán	plán	k1gInSc1	plán
na	na	k7c4	na
ovládnutí	ovládnutí	k1gNnSc4	ovládnutí
komunikačních	komunikační	k2eAgFnPc2d1	komunikační
a	a	k8xC	a
dopravních	dopravní	k2eAgFnPc2d1	dopravní
sítí	síť	k1gFnPc2	síť
a	a	k8xC	a
Rudé	rudý	k2eAgFnSc2d1	rudá
gardy	garda	k1gFnSc2	garda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
stále	stále	k6eAd1	stále
víc	hodně	k6eAd2	hodně
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
bolševiků	bolševik	k1gMnPc2	bolševik
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
neměly	mít	k5eNaImAgFnP	mít
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
jednotné	jednotný	k2eAgNnSc1d1	jednotné
velení	velení	k1gNnSc1	velení
<g/>
.12	.12	k4	.12
<g/>
.	.	kIx.	.
říjnajul	říjnajout	k5eAaPmAgInS	říjnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1917	[number]	k4	1917
<g/>
greg	greg	k1gInSc1	greg
<g/>
.	.	kIx.	.
výkonný	výkonný	k2eAgInSc1d1	výkonný
výbor	výbor	k1gInSc1	výbor
Petrohradského	petrohradský	k2eAgInSc2d1	petrohradský
sovětu	sovět	k1gInSc2	sovět
schválil	schválit	k5eAaPmAgInS	schválit
vytvoření	vytvoření	k1gNnSc4	vytvoření
orgánu	orgán	k1gInSc2	orgán
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
koordinovat	koordinovat	k5eAaBmF	koordinovat
obranu	obrana	k1gFnSc4	obrana
nadcházejícího	nadcházející	k2eAgInSc2d1	nadcházející
sjezdu	sjezd	k1gInSc2	sjezd
sovětů	sovět	k1gInPc2	sovět
<g/>
,	,	kIx,	,
Vojenského	vojenský	k2eAgInSc2d1	vojenský
revolučního	revoluční	k2eAgInSc2d1	revoluční
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
složen	složit	k5eAaPmNgInS	složit
ze	z	k7c2	z
zástupců	zástupce	k1gMnPc2	zástupce
sovětu	sovět	k1gInSc2	sovět
<g/>
,	,	kIx,	,
zástupců	zástupce	k1gMnPc2	zástupce
loďstva	loďstvo	k1gNnSc2	loďstvo
<g/>
,	,	kIx,	,
odborů	odbor	k1gInPc2	odbor
<g/>
,	,	kIx,	,
továrních	tovární	k2eAgInPc2d1	tovární
výborů	výbor	k1gInPc2	výbor
<g/>
,	,	kIx,	,
vojenských	vojenský	k2eAgFnPc2d1	vojenská
organizací	organizace	k1gFnPc2	organizace
různých	různý	k2eAgFnPc2d1	různá
stran	strana	k1gFnPc2	strana
v	v	k7c6	v
sovětu	sovět	k1gInSc6	sovět
<g/>
,	,	kIx,	,
dělnických	dělnický	k2eAgFnPc2d1	Dělnická
milicí	milice	k1gFnPc2	milice
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Zasedání	zasedání	k1gNnSc1	zasedání
zástupců	zástupce	k1gMnPc2	zástupce
jednotek	jednotka	k1gFnPc2	jednotka
petrohradské	petrohradský	k2eAgFnSc2d1	Petrohradská
posádky	posádka	k1gFnSc2	posádka
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
svoláno	svolán	k2eAgNnSc1d1	svoláno
na	na	k7c4	na
následující	následující	k2eAgInPc4d1	následující
dny	den	k1gInPc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Výbor	výbor	k1gInSc1	výbor
nebyl	být	k5eNaImAgInS	být
kruhem	kruh	k1gInSc7	kruh
spiklenců	spiklenec	k1gMnPc2	spiklenec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
oficiálním	oficiální	k2eAgInSc7d1	oficiální
orgánem	orgán	k1gInSc7	orgán
Petrohradského	petrohradský	k2eAgInSc2d1	petrohradský
sovětu	sovět	k1gInSc2	sovět
<g/>
.	.	kIx.	.
</s>
<s>
Umírnění	umírněný	k2eAgMnPc1d1	umírněný
socialisté	socialist	k1gMnPc1	socialist
Výbor	výbor	k1gInSc4	výbor
brzy	brzy	k6eAd1	brzy
opustili	opustit	k5eAaPmAgMnP	opustit
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
usnadnilo	usnadnit	k5eAaPmAgNnS	usnadnit
jeho	jeho	k3xOp3gNnSc1	jeho
ovládnutí	ovládnutí	k1gNnSc1	ovládnutí
extremisty	extremista	k1gMnSc2	extremista
a	a	k8xC	a
zejména	zejména	k9	zejména
bolševiky	bolševik	k1gMnPc4	bolševik
<g/>
.	.	kIx.	.
<g/>
Čtyři	čtyři	k4xCgInPc1	čtyři
dny	den	k1gInPc1	den
před	před	k7c7	před
předpokládaným	předpokládaný	k2eAgNnSc7d1	předpokládané
zahájením	zahájení	k1gNnSc7	zahájení
sjezdu	sjezd	k1gInSc2	sjezd
sovětů	sovět	k1gInPc2	sovět
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
.	.	kIx.	.
říjnajul	říjnajout	k5eAaPmAgInS	říjnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
další	další	k2eAgNnSc1d1	další
zasedání	zasedání	k1gNnSc1	zasedání
ústředního	ústřední	k2eAgInSc2d1	ústřední
výboru	výbor	k1gInSc2	výbor
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
o	o	k7c4	o
uchopení	uchopení	k1gNnSc4	uchopení
moci	moc	k1gFnSc2	moc
navzdory	navzdory	k7c3	navzdory
tvrdé	tvrdý	k2eAgFnSc3d1	tvrdá
opozici	opozice	k1gFnSc3	opozice
umírněných	umírněný	k2eAgFnPc2d1	umírněná
<g/>
;	;	kIx,	;
Kameněv	Kameněv	k1gMnSc1	Kameněv
hrozil	hrozit	k5eAaImAgMnS	hrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
odstoupí	odstoupit	k5eAaPmIp3nS	odstoupit
<g/>
,	,	kIx,	,
a	a	k8xC	a
k	k	k7c3	k
Leninově	Leninův	k2eAgFnSc3d1	Leninova
nelibosti	nelibost	k1gFnSc3	nelibost
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Zinovjevem	Zinovjev	k1gInSc7	Zinovjev
začali	začít	k5eAaPmAgMnP	začít
veřejně	veřejně	k6eAd1	veřejně
projevovat	projevovat	k5eAaImF	projevovat
svůj	svůj	k3xOyFgInSc4	svůj
opoziční	opoziční	k2eAgInSc4d1	opoziční
názor	názor	k1gInSc4	názor
<g/>
.	.	kIx.	.
</s>
<s>
Nevyhnutná	vyhnutný	k2eNgFnSc1d1	nevyhnutná
příprava	příprava	k1gFnSc1	příprava
na	na	k7c4	na
uskutečnění	uskutečnění	k1gNnSc4	uskutečnění
převratu	převrat	k1gInSc2	převrat
proti	proti	k7c3	proti
Prozatímní	prozatímní	k2eAgFnSc3d1	prozatímní
vládě	vláda	k1gFnSc3	vláda
však	však	k9	však
stále	stále	k6eAd1	stále
nezačala	začít	k5eNaPmAgFnS	začít
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
vedení	vedení	k1gNnSc2	vedení
strany	strana	k1gFnSc2	strana
i	i	k8xC	i
rezoluce	rezoluce	k1gFnSc2	rezoluce
dělníků	dělník	k1gMnPc2	dělník
a	a	k8xC	a
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
nepřestávaly	přestávat	k5eNaImAgInP	přestávat
docházet	docházet	k5eAaImF	docházet
<g/>
,	,	kIx,	,
nadále	nadále	k6eAd1	nadále
hodlaly	hodlat	k5eAaImAgFnP	hodlat
předat	předat	k5eAaPmF	předat
moc	moc	k1gFnSc4	moc
sjezdu	sjezd	k1gInSc2	sjezd
sovětů	sovět	k1gInPc2	sovět
<g/>
.	.	kIx.	.
</s>
<s>
Leví	levý	k2eAgMnPc1d1	levý
eseři	eser	k1gMnPc1	eser
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
i	i	k9	i
menševici	menševice	k1gFnSc4	menševice
internacionalisté	internacionalista	k1gMnPc1	internacionalista
se	se	k3xPyFc4	se
také	také	k9	také
připravovali	připravovat	k5eAaImAgMnP	připravovat
během	během	k7c2	během
sjezdu	sjezd	k1gInSc2	sjezd
vytvořit	vytvořit	k5eAaPmF	vytvořit
novou	nový	k2eAgFnSc4d1	nová
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
vládu	vláda	k1gFnSc4	vláda
složenou	složený	k2eAgFnSc4d1	složená
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stavěli	stavět	k5eAaImAgMnP	stavět
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
okamžitému	okamžitý	k2eAgNnSc3d1	okamžité
převzetí	převzetí	k1gNnSc3	převzetí
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
Trockij	Trockij	k1gMnSc1	Trockij
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
před	před	k7c7	před
několika	několik	k4yIc2	několik
dny	den	k1gInPc7	den
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
novinách	novina	k1gFnPc6	novina
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
o	o	k7c6	o
nás	my	k3xPp1nPc6	my
<g/>
,	,	kIx,	,
že	že	k8xS	že
připravujeme	připravovat	k5eAaImIp1nP	připravovat
organizaci	organizace	k1gFnSc4	organizace
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
chopila	chopit	k5eAaPmAgFnS	chopit
moci	moct	k5eAaImF	moct
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
žádné	žádný	k3yNgNnSc4	žádný
tajemství	tajemství	k1gNnSc4	tajemství
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Konečný	konečný	k2eAgInSc1d1	konečný
cíl	cíl	k1gInSc1	cíl
činnosti	činnost	k1gFnSc2	činnost
Petrohradského	petrohradský	k2eAgInSc2d1	petrohradský
sovětu	sovět	k1gInSc2	sovět
vytyčil	vytyčit	k5eAaPmAgMnS	vytyčit
sám	sám	k3xTgMnSc1	sám
Trockij	Trockij	k1gMnSc1	Trockij
<g/>
:	:	kIx,	:
sjezd	sjezd	k1gInSc1	sjezd
sovětů	sovět	k1gInPc2	sovět
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
chopit	chopit	k5eAaPmF	chopit
vládní	vládní	k2eAgInSc1d1	vládní
moci	moc	k1gFnSc3	moc
<g/>
,	,	kIx,	,
vyhlásit	vyhlásit	k5eAaPmF	vyhlásit
okamžité	okamžitý	k2eAgNnSc4d1	okamžité
příměří	příměří	k1gNnSc4	příměří
a	a	k8xC	a
rozdat	rozdat	k5eAaPmF	rozdat
půdu	půda	k1gFnSc4	půda
rolníkům	rolník	k1gMnPc3	rolník
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Lenina	Lenin	k1gMnSc2	Lenin
však	však	k9	však
moc	moc	k6eAd1	moc
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
převzít	převzít	k5eAaPmF	převzít
před	před	k7c7	před
sjezdem	sjezd	k1gInSc7	sjezd
a	a	k8xC	a
úlohou	úloha	k1gFnSc7	úloha
sjezdu	sjezd	k1gInSc2	sjezd
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
jen	jen	k9	jen
její	její	k3xOp3gNnSc4	její
přijetí	přijetí	k1gNnSc4	přijetí
<g/>
.	.	kIx.	.
<g/>
Rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
umírněných	umírněný	k2eAgMnPc2d1	umírněný
socialistů	socialist	k1gMnPc2	socialist
odložit	odložit	k5eAaPmF	odložit
kongres	kongres	k1gInSc4	kongres
o	o	k7c4	o
pět	pět	k4xCc4	pět
dní	den	k1gInPc2	den
pro	pro	k7c4	pro
nepřítomnost	nepřítomnost	k1gFnSc4	nepřítomnost
delegátů	delegát	k1gMnPc2	delegát
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
bylo	být	k5eAaImAgNnS	být
rozhodující	rozhodující	k2eAgNnSc1d1	rozhodující
<g/>
:	:	kIx,	:
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
bolševikům	bolševik	k1gMnPc3	bolševik
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
zorganizovali	zorganizovat	k5eAaPmAgMnP	zorganizovat
k	k	k7c3	k
převzetí	převzetí	k1gNnSc3	převzetí
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
zmařili	zmařit	k5eAaPmAgMnP	zmařit
pokus	pokus	k1gInSc4	pokus
Kerenského	Kerenský	k2eAgNnSc2d1	Kerenský
před	před	k7c7	před
sjezdem	sjezd	k1gInSc7	sjezd
odzbrojit	odzbrojit	k5eAaPmF	odzbrojit
radikály	radikál	k1gInPc4	radikál
<g/>
.	.	kIx.	.
</s>
<s>
Zpoždění	zpoždění	k1gNnSc1	zpoždění
bylo	být	k5eAaImAgNnS	být
zčásti	zčásti	k6eAd1	zčásti
také	také	k6eAd1	také
důsledkem	důsledek	k1gInSc7	důsledek
projevů	projev	k1gInPc2	projev
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
rozporů	rozpor	k1gInPc2	rozpor
v	v	k7c6	v
bolševické	bolševický	k2eAgFnSc6d1	bolševická
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
umírnění	umírněný	k2eAgMnPc1d1	umírněný
socialisté	socialist	k1gMnPc1	socialist
doufali	doufat	k5eAaImAgMnP	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
sjezdu	sjezd	k1gInSc6	sjezd
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
většina	většina	k1gFnSc1	většina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
postaví	postavit	k5eAaPmIp3nS	postavit
proti	proti	k7c3	proti
Leninově	Leninův	k2eAgFnSc3d1	Leninova
pozici	pozice	k1gFnSc3	pozice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přípravy	příprava	k1gFnPc4	příprava
Prozatímní	prozatímní	k2eAgFnSc2d1	prozatímní
vlády	vláda	k1gFnSc2	vláda
===	===	k?	===
</s>
</p>
<p>
<s>
Uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
schůze	schůze	k1gFnSc1	schůze
kabinetu	kabinet	k1gInSc2	kabinet
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
4	[number]	k4	4
<g/>
.	.	kIx.	.
říjnajul	říjnajout	k5eAaPmAgInS	říjnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
řešila	řešit	k5eAaImAgFnS	řešit
vojenské	vojenský	k2eAgNnSc4d1	vojenské
ohrožení	ohrožení	k1gNnSc4	ohrožení
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
po	po	k7c6	po
okupaci	okupace	k1gFnSc6	okupace
baltských	baltský	k2eAgInPc2d1	baltský
ostrovů	ostrov	k1gInPc2	ostrov
poblíž	poblíž	k7c2	poblíž
Estonska	Estonsko	k1gNnSc2	Estonsko
nepřítelem	nepřítel	k1gMnSc7	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Ministr	ministr	k1gMnSc1	ministr
sociálních	sociální	k2eAgFnPc2d1	sociální
věcí	věc	k1gFnPc2	věc
konstituční	konstituční	k2eAgMnSc1d1	konstituční
demokrat	demokrat	k1gMnSc1	demokrat
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Kiškin	Kiškin	k1gMnSc1	Kiškin
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
přenesení	přenesení	k1gNnSc4	přenesení
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
a	a	k8xC	a
začlenění	začlenění	k1gNnSc2	začlenění
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
do	do	k7c2	do
frontové	frontový	k2eAgFnSc2d1	frontová
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc4	návrh
socialisté	socialist	k1gMnPc1	socialist
kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
by	by	kYmCp3nS	by
snížil	snížit	k5eAaPmAgInS	snížit
vliv	vliv	k1gInSc4	vliv
Petrohradského	petrohradský	k2eAgInSc2d1	petrohradský
sovětu	sovět	k1gInSc2	sovět
a	a	k8xC	a
Celoruského	Celoruský	k2eAgInSc2d1	Celoruský
ústředního	ústřední	k2eAgInSc2d1	ústřední
výkonného	výkonný	k2eAgInSc2d1	výkonný
výboru	výbor	k1gInSc2	výbor
na	na	k7c4	na
vládu	vláda	k1gFnSc4	vláda
a	a	k8xC	a
na	na	k7c4	na
budoucí	budoucí	k2eAgNnSc4d1	budoucí
ústavodárné	ústavodárný	k2eAgNnSc4d1	Ústavodárné
shromáždění	shromáždění	k1gNnSc4	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Tváří	tvářet	k5eAaImIp3nS	tvářet
v	v	k7c4	v
tvář	tvář	k1gFnSc4	tvář
kritice	kritika	k1gFnSc6	kritika
se	se	k3xPyFc4	se
kabinet	kabinet	k1gInSc1	kabinet
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
nepřijmout	přijmout	k5eNaPmF	přijmout
návrh	návrh	k1gInSc1	návrh
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
nebyl	být	k5eNaImAgInS	být
schválen	schválit	k5eAaPmNgInS	schválit
předparlamentem	předparlament	k1gInSc7	předparlament
<g/>
.	.	kIx.	.
</s>
<s>
Diskuse	diskuse	k1gFnSc1	diskuse
unikla	uniknout	k5eAaPmAgFnS	uniknout
ve	v	k7c6	v
zkreslené	zkreslený	k2eAgFnSc6d1	zkreslená
podobě	podoba	k1gFnSc6	podoba
na	na	k7c4	na
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
domnívali	domnívat	k5eAaImAgMnP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vláda	vláda	k1gFnSc1	vláda
snaží	snažit	k5eAaImIp3nS	snažit
použít	použít	k5eAaPmF	použít
Němce	Němec	k1gMnPc4	Němec
k	k	k7c3	k
rozdrcení	rozdrcení	k1gNnSc3	rozdrcení
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
a	a	k8xC	a
tuto	tento	k3xDgFnSc4	tento
fámu	fáma	k1gFnSc4	fáma
použili	použít	k5eAaPmAgMnP	použít
bolševici	bolševik	k1gMnPc1	bolševik
ve	v	k7c4	v
svůj	svůj	k3xOyFgInSc4	svůj
prospěch	prospěch	k1gInSc4	prospěch
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
nikdy	nikdy	k6eAd1	nikdy
neměla	mít	k5eNaImAgFnS	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
vydat	vydat	k5eAaPmF	vydat
město	město	k1gNnSc4	město
nepříteli	nepřítel	k1gMnSc3	nepřítel
<g/>
;	;	kIx,	;
chtěla	chtít	k5eAaImAgFnS	chtít
využít	využít	k5eAaPmF	využít
jeho	jeho	k3xOp3gFnSc4	jeho
blízkost	blízkost	k1gFnSc4	blízkost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zbavila	zbavit	k5eAaPmAgFnS	zbavit
revoltujících	revoltující	k2eAgFnPc2d1	revoltující
částí	část	k1gFnPc2	část
vojenské	vojenský	k2eAgFnSc2d1	vojenská
posádky	posádka	k1gFnSc2	posádka
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
vojenští	vojenský	k2eAgMnPc1d1	vojenský
velitelé	velitel	k1gMnPc1	velitel
nevěřili	věřit	k5eNaImAgMnP	věřit
tomu	ten	k3xDgMnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Němci	Němec	k1gMnPc1	Němec
město	město	k1gNnSc4	město
opravdu	opravdu	k6eAd1	opravdu
ohrožují	ohrožovat	k5eAaImIp3nP	ohrožovat
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
říjnajul	říjnajout	k5eAaPmAgInS	říjnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1917	[number]	k4	1917
<g/>
greg	greg	k1gInSc1	greg
<g/>
.	.	kIx.	.
vojenské	vojenský	k2eAgNnSc1d1	vojenské
velitelství	velitelství	k1gNnSc1	velitelství
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
nařídilo	nařídit	k5eAaPmAgNnS	nařídit
odchod	odchod	k1gInSc4	odchod
třetiny	třetina	k1gFnSc2	třetina
jednotek	jednotka	k1gFnPc2	jednotka
městské	městský	k2eAgFnSc2d1	městská
posádky	posádka	k1gFnSc2	posádka
na	na	k7c4	na
frontu	fronta	k1gFnSc4	fronta
(	(	kIx(	(
<g/>
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
politických	politický	k2eAgInPc2d1	politický
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
velitel	velitel	k1gMnSc1	velitel
severní	severní	k2eAgFnSc2d1	severní
fronty	fronta	k1gFnSc2	fronta
o	o	k7c4	o
posily	posila	k1gFnPc4	posila
z	z	k7c2	z
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
nestál	stát	k5eNaImAgMnS	stát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednotky	jednotka	k1gFnPc1	jednotka
posádky	posádka	k1gFnSc2	posádka
pak	pak	k6eAd1	pak
odmítly	odmítnout	k5eAaPmAgInP	odmítnout
Prozatímní	prozatímní	k2eAgFnSc4d1	prozatímní
vládu	vláda	k1gFnSc4	vláda
a	a	k8xC	a
vyhlásily	vyhlásit	k5eAaPmAgFnP	vyhlásit
loajalitu	loajalita	k1gFnSc4	loajalita
Petrohradskému	petrohradský	k2eAgInSc3d1	petrohradský
sovětu	sovět	k1gInSc3	sovět
<g/>
.	.	kIx.	.
</s>
<s>
Vojáci	voják	k1gMnPc1	voják
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
nevěrnější	věrný	k2eNgMnPc1d2	věrný
během	během	k7c2	během
potlačení	potlačení	k1gNnPc2	potlačení
bolševického	bolševický	k2eAgNnSc2d1	bolševické
červencového	červencový	k2eAgNnSc2d1	červencové
povstání	povstání	k1gNnSc2	povstání
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
kozáků	kozák	k1gInPc2	kozák
<g/>
,	,	kIx,	,
zůstali	zůstat	k5eAaPmAgMnP	zůstat
neutrální	neutrální	k2eAgFnSc4d1	neutrální
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
přidali	přidat	k5eAaPmAgMnP	přidat
k	k	k7c3	k
sovětům	sovět	k1gInPc3	sovět
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
přetrvávajícím	přetrvávající	k2eAgFnPc3d1	přetrvávající
fámám	fáma	k1gFnPc3	fáma
o	o	k7c6	o
dalším	další	k2eAgInSc6d1	další
převratu	převrat	k1gInSc6	převrat
pravice	pravice	k1gFnSc2	pravice
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc3	který
povzbuzovala	povzbuzovat	k5eAaImAgFnS	povzbuzovat
prohlášení	prohlášení	k1gNnSc4	prohlášení
Michaila	Michail	k1gMnSc2	Michail
Rodzjanka	Rodzjanka	k1gFnSc1	Rodzjanka
požadující	požadující	k2eAgNnSc4d1	požadující
přenechání	přenechání	k1gNnSc4	přenechání
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Němcům	Němec	k1gMnPc3	Němec
a	a	k8xC	a
vzpomínky	vzpomínka	k1gFnSc2	vzpomínka
na	na	k7c4	na
podobný	podobný	k2eAgInSc4d1	podobný
rozkaz	rozkaz	k1gInSc4	rozkaz
Kornilova	Kornilův	k2eAgMnSc2d1	Kornilův
během	během	k7c2	během
jeho	on	k3xPp3gInSc2	on
neúspěšného	úspěšný	k2eNgInSc2d1	neúspěšný
puče	puč	k1gInSc2	puč
<g/>
,	,	kIx,	,
rozkaz	rozkaz	k1gInSc1	rozkaz
vojenského	vojenský	k2eAgNnSc2d1	vojenské
velitelství	velitelství	k1gNnSc2	velitelství
alarmoval	alarmovat	k5eAaImAgInS	alarmovat
Petrohradský	petrohradský	k2eAgInSc1d1	petrohradský
sovět	sovět	k1gInSc1	sovět
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
začal	začít	k5eAaPmAgInS	začít
připravovat	připravovat	k5eAaImF	připravovat
protiopatření	protiopatření	k1gNnSc4	protiopatření
<g/>
.13	.13	k4	.13
<g/>
.	.	kIx.	.
říjnajul	říjnajout	k5eAaPmAgInS	říjnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1917	[number]	k4	1917
<g/>
greg	greg	k1gInSc1	greg
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
Kerenskij	Kerenskij	k1gMnSc1	Kerenskij
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
předparlamentu	předparlament	k1gInSc6	předparlament
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
popřel	popřít	k5eAaPmAgMnS	popřít
fámy	fáma	k1gFnPc4	fáma
tvrdící	tvrdící	k2eAgFnPc4d1	tvrdící
<g/>
,	,	kIx,	,
že	že	k8xS	že
připravuje	připravovat	k5eAaImIp3nS	připravovat
přenesení	přenesení	k1gNnSc1	přenesení
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
se	se	k3xPyFc4	se
sešla	sejít	k5eAaPmAgFnS	sejít
vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
se	se	k3xPyFc4	se
apelovat	apelovat	k5eAaImF	apelovat
na	na	k7c4	na
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nepodporovalo	podporovat	k5eNaImAgNnS	podporovat
akce	akce	k1gFnPc4	akce
bolševiků	bolševik	k1gMnPc2	bolševik
a	a	k8xC	a
nechalo	nechat	k5eAaPmAgNnS	nechat
vládu	vláda	k1gFnSc4	vláda
konat	konat	k5eAaImF	konat
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
přitom	přitom	k6eAd1	přitom
doufala	doufat	k5eAaImAgFnS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyplývalo	vyplývat	k5eAaImAgNnS	vyplývat
ze	z	k7c2	z
zprávy	zpráva	k1gFnSc2	zpráva
plukovníka	plukovník	k1gMnSc2	plukovník
Polkovnikova	Polkovnikův	k2eAgMnSc2d1	Polkovnikův
<g/>
,	,	kIx,	,
nedávno	nedávno	k6eAd1	nedávno
jmenovaného	jmenovaný	k2eAgMnSc2d1	jmenovaný
vojenského	vojenský	k2eAgMnSc2d1	vojenský
velitele	velitel	k1gMnSc2	velitel
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
<g/>
Kerenskij	Kerenskij	k1gMnSc1	Kerenskij
pak	pak	k6eAd1	pak
navštívil	navštívit	k5eAaPmAgMnS	navštívit
frontu	fronta	k1gFnSc4	fronta
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
nepřítomen	přítomen	k2eNgInSc4d1	nepřítomen
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
říjnajul	říjnajout	k5eAaPmAgInS	říjnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1917	[number]	k4	1917
<g />
.	.	kIx.	.
</s>
<s>
<g/>
greg	greg	k1gInSc1	greg
<g/>
.	.	kIx.	.
do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
říjnajul	říjnajout	k5eAaPmAgInS	říjnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
..	..	k?	..
Vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
mezitím	mezitím	k6eAd1	mezitím
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
obávala	obávat	k5eAaImAgFnS	obávat
možnosti	možnost	k1gFnPc4	možnost
bolševického	bolševický	k2eAgNnSc2d1	bolševické
povstání	povstání	k1gNnSc2	povstání
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
vojenští	vojenský	k2eAgMnPc1d1	vojenský
velitelé	velitel	k1gMnPc1	velitel
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
ji	on	k3xPp3gFnSc4	on
stále	stále	k6eAd1	stále
ujišťovali	ujišťovat	k5eAaImAgMnP	ujišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgNnP	být
přijata	přijat	k2eAgNnPc1d1	přijato
náležitá	náležitý	k2eAgNnPc1d1	náležité
protiopatření	protiopatření	k1gNnPc1	protiopatření
<g/>
.	.	kIx.	.
</s>
<s>
Kiškin	Kiškin	k1gMnSc1	Kiškin
byl	být	k5eAaImAgMnS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vláda	vláda	k1gFnSc1	vláda
má	mít	k5eAaImIp3nS	mít
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
rozdrtila	rozdrtit	k5eAaPmAgFnS	rozdrtit
možné	možný	k2eAgNnSc4d1	možné
bolševické	bolševický	k2eAgNnSc4d1	bolševické
povstání	povstání	k1gNnSc4	povstání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepodnikal	podnikat	k5eNaImAgMnS	podnikat
preventivní	preventivní	k2eAgNnSc4d1	preventivní
opatření	opatření	k1gNnSc4	opatření
<g/>
.	.	kIx.	.
</s>
<s>
Místopředseda	místopředseda	k1gMnSc1	místopředseda
vlády	vláda	k1gFnSc2	vláda
Alexandr	Alexandr	k1gMnSc1	Alexandr
Konovalov	Konovalov	k1gInSc1	Konovalov
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
požádat	požádat	k5eAaPmF	požádat
o	o	k7c4	o
posily	posila	k1gFnPc4	posila
z	z	k7c2	z
vojenských	vojenský	k2eAgFnPc2d1	vojenská
škol	škola	k1gFnPc2	škola
Oranienbaum	Oranienbaum	k1gNnSc4	Oranienbaum
a	a	k8xC	a
Peterhof	Peterhof	k1gInSc4	Peterhof
<g/>
,	,	kIx,	,
dělostřelectvo	dělostřelectvo	k1gNnSc4	dělostřelectvo
z	z	k7c2	z
dělostřelecké	dělostřelecký	k2eAgFnSc2d1	dělostřelecká
školy	škola	k1gFnSc2	škola
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
a	a	k8xC	a
o	o	k7c6	o
vyslání	vyslání	k1gNnSc6	vyslání
praporu	prapor	k1gInSc2	prapor
cyklistů	cyklista	k1gMnPc2	cyklista
do	do	k7c2	do
Zimního	zimní	k2eAgInSc2d1	zimní
paláce	palác	k1gInSc2	palác
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
posílili	posílit	k5eAaPmAgMnP	posílit
jeho	jeho	k3xOp3gFnSc4	jeho
obranu	obrana	k1gFnSc4	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Konovalov	Konovalov	k1gInSc1	Konovalov
pak	pak	k6eAd1	pak
přesvědčil	přesvědčit	k5eAaPmAgInS	přesvědčit
Kerenského	Kerenský	k2eAgNnSc2d1	Kerenský
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
přijel	přijet	k5eAaPmAgMnS	přijet
do	do	k7c2	do
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
a	a	k8xC	a
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
schůze	schůze	k1gFnSc2	schůze
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
byli	být	k5eAaImAgMnP	být
ministři	ministr	k1gMnPc1	ministr
obrany	obrana	k1gFnSc2	obrana
a	a	k8xC	a
vnitra	vnitro	k1gNnSc2	vnitro
přesvědčeni	přesvědčen	k2eAgMnPc1d1	přesvědčen
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozdrtí	rozdrtit	k5eAaPmIp3nS	rozdrtit
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
vzpouru	vzpoura	k1gFnSc4	vzpoura
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Kerenskij	Kerenskij	k1gMnSc1	Kerenskij
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
touhu	touha	k1gFnSc4	touha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
bolševici	bolševik	k1gMnPc1	bolševik
povstali	povstat	k5eAaPmAgMnP	povstat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
je	on	k3xPp3gNnSc4	on
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
rozdrtit	rozdrtit	k5eAaPmF	rozdrtit
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
noc	noc	k1gFnSc1	noc
další	další	k2eAgFnSc2d1	další
schůze	schůze	k1gFnSc2	schůze
vlády	vláda	k1gFnSc2	vláda
schválila	schválit	k5eAaPmAgFnS	schválit
uplatnění	uplatnění	k1gNnSc4	uplatnění
nových	nový	k2eAgNnPc2d1	nové
opatření	opatření	k1gNnPc2	opatření
k	k	k7c3	k
potlačení	potlačení	k1gNnSc3	potlačení
možného	možný	k2eAgNnSc2d1	možné
povstání	povstání	k1gNnSc2	povstání
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc2	který
Kerenskij	Kerenskij	k1gFnSc2	Kerenskij
dojednal	dojednat	k5eAaPmAgInS	dojednat
s	s	k7c7	s
vojenskými	vojenský	k2eAgMnPc7d1	vojenský
veliteli	velitel	k1gMnPc7	velitel
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
původního	původní	k2eAgInSc2d1	původní
termínu	termín	k1gInSc2	termín
zahájení	zahájení	k1gNnSc2	zahájení
sjezdu	sjezd	k1gInSc2	sjezd
sovětů	sovět	k1gInPc2	sovět
kozáci	kozák	k1gMnPc1	kozák
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
hlídali	hlídat	k5eAaImAgMnP	hlídat
město	město	k1gNnSc4	město
před	před	k7c7	před
možným	možný	k2eAgNnSc7d1	možné
povstáním	povstání	k1gNnSc7	povstání
a	a	k8xC	a
obrana	obrana	k1gFnSc1	obrana
Zimního	zimní	k2eAgInSc2d1	zimní
paláce	palác	k1gInSc2	palác
byla	být	k5eAaImAgFnS	být
posílena	posílit	k5eAaPmNgFnS	posílit
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
ministrů	ministr	k1gMnPc2	ministr
vydala	vydat	k5eAaPmAgFnS	vydat
také	také	k9	také
několik	několik	k4yIc4	několik
prohlášení	prohlášení	k1gNnPc2	prohlášení
vyzývajících	vyzývající	k2eAgMnPc2d1	vyzývající
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
k	k	k7c3	k
udržení	udržení	k1gNnSc3	udržení
klidu	klid	k1gInSc2	klid
<g/>
.	.	kIx.	.
</s>
<s>
Kerenskij	Kerenskít	k5eAaPmRp2nS	Kerenskít
oznámil	oznámit	k5eAaPmAgMnS	oznámit
jak	jak	k8xC	jak
britskému	britský	k2eAgMnSc3d1	britský
velvyslanci	velvyslanec	k1gMnSc3	velvyslanec
<g/>
,	,	kIx,	,
tak	tak	k9	tak
předparlamentu	předparlament	k1gInSc2	předparlament
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
čelit	čelit	k5eAaImF	čelit
bolševikům	bolševik	k1gMnPc3	bolševik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kaluze	Kaluha	k1gFnSc6	Kaluha
u	u	k7c2	u
Moskvy	Moskva	k1gFnSc2	Moskva
dostali	dostat	k5eAaPmAgMnP	dostat
kozáci	kozák	k1gMnPc1	kozák
rozkaz	rozkaz	k1gInSc1	rozkaz
rozpustit	rozpustit	k5eAaPmF	rozpustit
místní	místní	k2eAgInSc4d1	místní
sovět	sovět	k1gInSc4	sovět
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
obnoven	obnovit	k5eAaPmNgInS	obnovit
příkaz	příkaz	k1gInSc1	příkaz
k	k	k7c3	k
zadržení	zadržení	k1gNnSc3	zadržení
Lenina	Lenin	k1gMnSc2	Lenin
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
tato	tento	k3xDgFnSc1	tento
opatření	opatření	k1gNnSc3	opatření
měla	mít	k5eAaImAgFnS	mít
silnou	silný	k2eAgFnSc4d1	silná
podporu	podpora	k1gFnSc4	podpora
menševiků	menševik	k1gMnPc2	menševik
a	a	k8xC	a
eserů	eser	k1gMnPc2	eser
v	v	k7c6	v
Celoruském	Celoruský	k2eAgInSc6d1	Celoruský
ústředním	ústřední	k2eAgInSc6d1	ústřední
výkonném	výkonný	k2eAgInSc6d1	výkonný
výboru	výbor	k1gInSc6	výbor
<g/>
.	.	kIx.	.
<g/>
Pokus	pokus	k1gInSc1	pokus
ústředního	ústřední	k2eAgInSc2d1	ústřední
výkonného	výkonný	k2eAgInSc2d1	výkonný
výboru	výbor	k1gInSc2	výbor
získat	získat	k5eAaPmF	získat
podporu	podpora	k1gFnSc4	podpora
vojenských	vojenský	k2eAgFnPc2d1	vojenská
jednotek	jednotka	k1gFnPc2	jednotka
posádky	posádka	k1gFnSc2	posádka
pro	pro	k7c4	pro
vládu	vláda	k1gFnSc4	vláda
selhal	selhat	k5eAaPmAgInS	selhat
<g/>
:	:	kIx,	:
jednotky	jednotka	k1gFnPc1	jednotka
vyslaly	vyslat	k5eAaPmAgFnP	vyslat
delegáty	delegát	k1gMnPc4	delegát
na	na	k7c4	na
jednání	jednání	k1gNnSc4	jednání
s	s	k7c7	s
plukovníkem	plukovník	k1gMnSc7	plukovník
Polkovnikovem	Polkovnikov	k1gInSc7	Polkovnikov
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zástupci	zástupce	k1gMnPc1	zástupce
ústředního	ústřední	k2eAgInSc2d1	ústřední
výkonného	výkonný	k2eAgInSc2d1	výkonný
výboru	výbor	k1gInSc2	výbor
nepřesvědčili	přesvědčit	k5eNaPmAgMnP	přesvědčit
delegáty	delegát	k1gMnPc7	delegát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
podpořili	podpořit	k5eAaPmAgMnP	podpořit
vládu	vláda	k1gFnSc4	vláda
proti	proti	k7c3	proti
sovětu	sovět	k1gInSc3	sovět
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dnech	den	k1gInPc6	den
před	před	k7c7	před
bolševickým	bolševický	k2eAgNnSc7d1	bolševické
povstáním	povstání	k1gNnSc7	povstání
se	se	k3xPyFc4	se
nedařilo	dařit	k5eNaImAgNnS	dařit
zastavit	zastavit	k5eAaPmF	zastavit
oslabování	oslabování	k1gNnSc1	oslabování
Kerenského	Kerenský	k2eAgInSc2d1	Kerenský
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Přípravy	příprava	k1gFnSc2	příprava
vlády	vláda	k1gFnSc2	vláda
byly	být	k5eAaImAgInP	být
dokončeny	dokončit	k5eAaPmNgInP	dokončit
20	[number]	k4	20
<g/>
.	.	kIx.	.
říjnajul	říjnajout	k5eAaPmAgInS	říjnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
vysláním	vyslání	k1gNnSc7	vyslání
jednotky	jednotka	k1gFnSc2	jednotka
čtyřiceti	čtyřicet	k4xCc2	čtyřicet
cyklistů	cyklista	k1gMnPc2	cyklista
do	do	k7c2	do
Zimního	zimní	k2eAgInSc2d1	zimní
paláce	palác	k1gInSc2	palác
a	a	k8xC	a
zasláním	zaslání	k1gNnSc7	zaslání
žádosti	žádost	k1gFnSc2	žádost
velitelům	velitel	k1gMnPc3	velitel
frontových	frontový	k2eAgFnPc2d1	frontová
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nahradili	nahradit	k5eAaPmAgMnP	nahradit
vojáky	voják	k1gMnPc7	voják
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jsou	být	k5eAaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
blízké	blízký	k2eAgMnPc4d1	blízký
bolševikům	bolševik	k1gMnPc3	bolševik
<g/>
.	.	kIx.	.
</s>
<s>
Vládní	vládní	k2eAgInSc1d1	vládní
plán	plán	k1gInSc1	plán
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
ovládnutí	ovládnutí	k1gNnSc4	ovládnutí
důležitých	důležitý	k2eAgFnPc2d1	důležitá
budov	budova	k1gFnPc2	budova
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
a	a	k8xC	a
mostů	most	k1gInPc2	most
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
umožňovaly	umožňovat	k5eAaImAgFnP	umožňovat
přístup	přístup	k1gInSc4	přístup
z	z	k7c2	z
předměstí	předměstí	k1gNnSc2	předměstí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Předvečer	předvečer	k1gInSc1	předvečer
Druhého	druhý	k4xOgMnSc2	druhý
sjezdu	sjezd	k1gInSc2	sjezd
sovětů	sovět	k1gInPc2	sovět
===	===	k?	===
</s>
</p>
<p>
<s>
21	[number]	k4	21
<g/>
.	.	kIx.	.
říjnajul	říjnajout	k5eAaPmAgInS	říjnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
konference	konference	k1gFnSc1	konference
jednotek	jednotka	k1gFnPc2	jednotka
petrohradské	petrohradský	k2eAgFnSc2d1	Petrohradská
posádky	posádka	k1gFnSc2	posádka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
uspořádal	uspořádat	k5eAaPmAgInS	uspořádat
Petrohradský	petrohradský	k2eAgInSc1d1	petrohradský
revoluční	revoluční	k2eAgInSc1d1	revoluční
vojenský	vojenský	k2eAgInSc1d1	vojenský
výbor	výbor	k1gInSc1	výbor
(	(	kIx(	(
<g/>
VRV	VRV	kA	VRV
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
větší	veliký	k2eAgFnSc1d2	veliký
věrnost	věrnost	k1gFnSc1	věrnost
jednotek	jednotka	k1gFnPc2	jednotka
petrohradskému	petrohradský	k2eAgInSc3d1	petrohradský
sovětu	sovět	k1gInSc3	sovět
než	než	k8xS	než
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Revoluční	revoluční	k2eAgFnSc1d1	revoluční
nálada	nálada	k1gFnSc1	nálada
konečně	konečně	k6eAd1	konečně
povzbudila	povzbudit	k5eAaPmAgFnS	povzbudit
Revoluční	revoluční	k2eAgInSc4d1	revoluční
vojenský	vojenský	k2eAgInSc4d1	vojenský
výbor	výbor	k1gInSc4	výbor
a	a	k8xC	a
Petrohradský	petrohradský	k2eAgInSc1d1	petrohradský
sovět	sovět	k1gInSc1	sovět
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
snažily	snažit	k5eAaImAgFnP	snažit
ujmout	ujmout	k5eAaPmF	ujmout
se	se	k3xPyFc4	se
moci	moct	k5eAaImF	moct
<g/>
,	,	kIx,	,
podepsat	podepsat	k5eAaPmF	podepsat
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
zajistit	zajistit	k5eAaPmF	zajistit
zásobování	zásobování	k1gNnSc4	zásobování
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
potravinami	potravina	k1gFnPc7	potravina
a	a	k8xC	a
schválit	schválit	k5eAaPmF	schválit
agrární	agrární	k2eAgFnSc4d1	agrární
reformu	reforma	k1gFnSc4	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Týž	týž	k3xTgMnSc1	týž
večer	večer	k6eAd1	večer
poslal	poslat	k5eAaPmAgMnS	poslat
VRV	VRV	kA	VRV
zprávu	zpráva	k1gFnSc4	zpráva
vojenskému	vojenský	k2eAgInSc3d1	vojenský
veliteli	velitel	k1gMnPc7	velitel
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
plukovníku	plukovník	k1gMnSc3	plukovník
Polkovnikovovi	Polkovnikova	k1gMnSc3	Polkovnikova
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgInPc4	všechen
následující	následující	k2eAgInPc4d1	následující
rozkazy	rozkaz	k1gInPc4	rozkaz
musejí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
schváleny	schválit	k5eAaPmNgInP	schválit
VRV	VRV	kA	VRV
<g/>
,	,	kIx,	,
na	na	k7c4	na
což	což	k3yRnSc4	což
velitel	velitel	k1gMnSc1	velitel
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
přistoupit	přistoupit	k5eAaPmF	přistoupit
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
VRV	VRV	kA	VRV
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
informoval	informovat	k5eAaBmAgMnS	informovat
jednotky	jednotka	k1gFnPc4	jednotka
městské	městský	k2eAgFnSc2d1	městská
posádky	posádka	k1gFnSc2	posádka
o	o	k7c4	o
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
hrozící	hrozící	k2eAgFnSc2d1	hrozící
kontrarevoluce	kontrarevoluce	k1gFnSc2	kontrarevoluce
a	a	k8xC	a
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgInPc1	všechen
rozkazy	rozkaz	k1gInPc1	rozkaz
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
schváleny	schválit	k5eAaPmNgInP	schválit
VRV	VRV	kA	VRV
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
začal	začít	k5eAaPmAgInS	začít
posílat	posílat	k5eAaImF	posílat
své	svůj	k3xOyFgMnPc4	svůj
vlastní	vlastní	k2eAgMnPc4d1	vlastní
komisaře	komisař	k1gMnPc4	komisař
do	do	k7c2	do
hlavních	hlavní	k2eAgFnPc2d1	hlavní
vojenských	vojenský	k2eAgFnPc2d1	vojenská
jednotek	jednotka	k1gFnPc2	jednotka
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zajistil	zajistit	k5eAaPmAgMnS	zajistit
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
posádkou	posádka	k1gFnSc7	posádka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
odmítala	odmítat	k5eAaImAgFnS	odmítat
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Jednotky	jednotka	k1gFnPc1	jednotka
byly	být	k5eAaImAgFnP	být
obvykle	obvykle	k6eAd1	obvykle
novými	nový	k2eAgMnPc7d1	nový
komisaři	komisař	k1gMnPc7	komisař
VRV	VRV	kA	VRV
nadšeny	nadšen	k2eAgFnPc1d1	nadšena
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
bolševiky	bolševik	k1gMnPc4	bolševik
nedávno	nedávno	k6eAd1	nedávno
propuštěné	propuštěný	k2eAgInPc1d1	propuštěný
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
VRV	VRV	kA	VRV
také	také	k9	také
nařídil	nařídit	k5eAaPmAgInS	nařídit
arzenálům	arzenál	k1gInPc3	arzenál
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nevydávaly	vydávat	k5eNaImAgFnP	vydávat
zbraně	zbraň	k1gFnPc1	zbraň
nebo	nebo	k8xC	nebo
střelivo	střelivo	k1gNnSc1	střelivo
bez	bez	k7c2	bez
jeho	on	k3xPp3gNnSc2	on
svolení	svolení	k1gNnSc2	svolení
<g/>
.22	.22	k4	.22
<g/>
.	.	kIx.	.
říjnajul	říjnajout	k5eAaPmAgInS	říjnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
probíhaly	probíhat	k5eAaImAgFnP	probíhat
demonstrace	demonstrace	k1gFnPc1	demonstrace
<g/>
;	;	kIx,	;
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
"	"	kIx"	"
<g/>
den	den	k1gInSc1	den
Petrohradského	petrohradský	k2eAgInSc2d1	petrohradský
sovětu	sovět	k1gInSc2	sovět
<g/>
"	"	kIx"	"
a	a	k8xC	a
bolševici	bolševik	k1gMnPc1	bolševik
i	i	k8xC	i
leví	levý	k2eAgMnPc1d1	levý
eseři	eser	k1gMnPc1	eser
se	se	k3xPyFc4	se
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
úspěchem	úspěch	k1gInSc7	úspěch
snažili	snažit	k5eAaImAgMnP	snažit
mobilizovat	mobilizovat	k5eAaBmF	mobilizovat
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
předání	předání	k1gNnSc4	předání
moci	moct	k5eAaImF	moct
sovětům	sovět	k1gInPc3	sovět
<g/>
.	.	kIx.	.
</s>
<s>
Řečníci	řečník	k1gMnPc1	řečník
vyslaní	vyslanit	k5eAaPmIp3nP	vyslanit
k	k	k7c3	k
plukům	pluk	k1gInPc3	pluk
VRV	VRV	kA	VRV
vyvolávali	vyvolávat	k5eAaImAgMnP	vyvolávat
nespokojenost	nespokojenost	k1gFnSc4	nespokojenost
s	s	k7c7	s
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
bylo	být	k5eAaImAgNnS	být
cítit	cítit	k5eAaImF	cítit
napětí	napětí	k1gNnSc1	napětí
kvůli	kvůli	k7c3	kvůli
možným	možný	k2eAgInPc3d1	možný
střetům	střet	k1gInPc3	střet
mezi	mezi	k7c7	mezi
demonstranty	demonstrant	k1gMnPc7	demonstrant
a	a	k8xC	a
kozáky	kozák	k1gMnPc7	kozák
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
svolali	svolat	k5eAaPmAgMnP	svolat
vlastenecký	vlastenecký	k2eAgInSc4d1	vlastenecký
pochod	pochod	k1gInSc4	pochod
na	na	k7c4	na
připomenutí	připomenutí	k1gNnSc4	připomenutí
osvobození	osvobození	k1gNnSc2	osvobození
Moskvy	Moskva	k1gFnSc2	Moskva
z	z	k7c2	z
Napoleonových	Napoleonových	k2eAgFnPc2d1	Napoleonových
rukou	ruka	k1gFnPc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
jednotky	jednotka	k1gFnPc1	jednotka
se	se	k3xPyFc4	se
mobilizovaly	mobilizovat	k5eAaBmAgFnP	mobilizovat
a	a	k8xC	a
rozhodly	rozhodnout	k5eAaPmAgFnP	rozhodnout
se	se	k3xPyFc4	se
zůstat	zůstat	k5eAaPmF	zůstat
v	v	k7c6	v
pohotovosti	pohotovost	k1gFnSc6	pohotovost
až	až	k9	až
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
sjezdu	sjezd	k1gInSc2	sjezd
sovětů	sovět	k1gInPc2	sovět
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
jednotky	jednotka	k1gFnPc1	jednotka
rudých	rudý	k2eAgFnPc2d1	rudá
gard	garda	k1gFnPc2	garda
byly	být	k5eAaImAgFnP	být
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
pohotovosti	pohotovost	k1gFnSc6	pohotovost
<g/>
.	.	kIx.	.
</s>
<s>
Náčelník	náčelník	k1gMnSc1	náčelník
štábu	štáb	k1gInSc2	štáb
vojenské	vojenský	k2eAgFnSc2d1	vojenská
oblasti	oblast	k1gFnSc2	oblast
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
generál	generál	k1gMnSc1	generál
Jakov	Jakov	k1gInSc1	Jakov
Bagratuni	Bagratueň	k1gFnSc3	Bagratueň
požádal	požádat	k5eAaPmAgInS	požádat
severní	severní	k2eAgFnSc4d1	severní
frontu	fronta	k1gFnSc4	fronta
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
připravila	připravit	k5eAaPmAgFnS	připravit
několik	několik	k4yIc4	několik
vojenských	vojenský	k2eAgFnPc2d1	vojenská
jednotek	jednotka	k1gFnPc2	jednotka
pro	pro	k7c4	pro
okamžité	okamžitý	k2eAgNnSc4d1	okamžité
odeslání	odeslání	k1gNnSc4	odeslání
do	do	k7c2	do
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
komisař	komisař	k1gMnSc1	komisař
frontu	fronta	k1gFnSc4	fronta
Vojtinskij	Vojtinskij	k1gFnSc2	Vojtinskij
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
takový	takový	k3xDgInSc1	takový
krok	krok	k1gInSc1	krok
je	být	k5eAaImIp3nS	být
nemožný	možný	k2eNgInSc1d1	nemožný
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
vojákům	voják	k1gMnPc3	voják
vysvětlilo	vysvětlit	k5eAaPmAgNnS	vysvětlit
<g/>
,	,	kIx,	,
nač	nač	k6eAd1	nač
se	se	k3xPyFc4	se
připravují	připravovat	k5eAaImIp3nP	připravovat
<g/>
.	.	kIx.	.
</s>
<s>
Kerenskij	Kerenskít	k5eAaPmRp2nS	Kerenskít
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
marně	marně	k6eAd1	marně
zopakoval	zopakovat	k5eAaPmAgMnS	zopakovat
stejnou	stejný	k2eAgFnSc4d1	stejná
žádost	žádost	k1gFnSc4	žádost
<g/>
.	.	kIx.	.
<g/>
Druhý	druhý	k4xOgInSc1	druhý
den	den	k1gInSc1	den
VRV	VRV	kA	VRV
oznámil	oznámit	k5eAaPmAgInS	oznámit
opatření	opatření	k1gNnPc1	opatření
směřující	směřující	k2eAgNnPc1d1	směřující
k	k	k7c3	k
ovládnutí	ovládnutí	k1gNnSc3	ovládnutí
posádky	posádka	k1gFnSc2	posádka
a	a	k8xC	a
strategických	strategický	k2eAgInPc2d1	strategický
bodů	bod	k1gInPc2	bod
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
údajně	údajně	k6eAd1	údajně
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dni	den	k1gInSc6	den
debat	debata	k1gFnPc2	debata
se	se	k3xPyFc4	se
Petropavlovská	petropavlovský	k2eAgFnSc1d1	Petropavlovská
pevnost	pevnost	k1gFnSc1	pevnost
konečně	konečně	k6eAd1	konečně
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
podřídit	podřídit	k5eAaPmF	podřídit
vedení	vedení	k1gNnSc4	vedení
VRV	VRV	kA	VRV
<g/>
.	.	kIx.	.
</s>
<s>
Arzenál	arzenál	k1gInSc1	arzenál
pevnosti	pevnost	k1gFnSc2	pevnost
dovolil	dovolit	k5eAaPmAgInS	dovolit
VRV	VRV	kA	VRV
vyzbrojit	vyzbrojit	k5eAaPmF	vyzbrojit
mnoho	mnoho	k6eAd1	mnoho
rudých	rudý	k2eAgMnPc2d1	rudý
gardistů	gardista	k1gMnPc2	gardista
<g/>
.	.	kIx.	.
</s>
<s>
Autorita	autorita	k1gFnSc1	autorita
vojenských	vojenský	k2eAgMnPc2d1	vojenský
velitelů	velitel	k1gMnPc2	velitel
města	město	k1gNnSc2	město
stále	stále	k6eAd1	stále
klesala	klesat	k5eAaImAgFnS	klesat
<g/>
.	.	kIx.	.
<g/>
Vláda	vláda	k1gFnSc1	vláda
byla	být	k5eAaImAgFnS	být
stále	stále	k6eAd1	stále
přesvědčena	přesvědčit	k5eAaPmNgFnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
dostatek	dostatek	k1gInSc4	dostatek
věrných	věrný	k2eAgMnPc2d1	věrný
vojáků	voják	k1gMnPc2	voják
k	k	k7c3	k
rozdrcení	rozdrcení	k1gNnSc3	rozdrcení
možného	možný	k2eAgNnSc2d1	možné
povstání	povstání	k1gNnSc2	povstání
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
předtím	předtím	k6eAd1	předtím
ministr	ministr	k1gMnSc1	ministr
vnitra	vnitro	k1gNnSc2	vnitro
připustil	připustit	k5eAaPmAgMnS	připustit
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
silný	silný	k2eAgMnSc1d1	silný
k	k	k7c3	k
přímému	přímý	k2eAgInSc3d1	přímý
útoku	útok	k1gInSc3	útok
na	na	k7c4	na
levicové	levicový	k2eAgInPc4d1	levicový
radikály	radikál	k1gInPc4	radikál
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
aktivity	aktivita	k1gFnPc4	aktivita
VRV	VRV	kA	VRV
<g/>
,	,	kIx,	,
přípravy	příprava	k1gFnPc1	příprava
rudých	rudý	k2eAgMnPc2d1	rudý
gardistů	gardista	k1gMnPc2	gardista
a	a	k8xC	a
projevy	projev	k1gInPc1	projev
podpory	podpora	k1gFnSc2	podpora
převzetí	převzetí	k1gNnSc2	převzetí
moci	moct	k5eAaImF	moct
sověty	sovět	k1gInPc4	sovět
vedly	vést	k5eAaImAgFnP	vést
vládu	vláda	k1gFnSc4	vláda
ke	k	k7c3	k
snaze	snaha	k1gFnSc3	snaha
získat	získat	k5eAaPmF	získat
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
posilu	posila	k1gFnSc4	posila
ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
fronty	fronta	k1gFnSc2	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
blízkému	blízký	k2eAgInSc3d1	blízký
zahájení	zahájení	k1gNnSc2	zahájení
Druhého	druhý	k4xOgMnSc2	druhý
sjezdu	sjezd	k1gInSc2	sjezd
sovětů	sovět	k1gInPc2	sovět
<g/>
,	,	kIx,	,
od	od	k7c2	od
něhož	jenž	k3xRgNnSc2	jenž
se	se	k3xPyFc4	se
čekalo	čekat	k5eAaImAgNnS	čekat
její	její	k3xOp3gNnSc1	její
odstranění	odstranění	k1gNnSc1	odstranění
a	a	k8xC	a
převzetí	převzetí	k1gNnSc1	převzetí
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vláda	vláda	k1gFnSc1	vláda
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
pokusit	pokusit	k5eAaPmF	pokusit
provést	provést	k5eAaPmF	provést
preventivní	preventivní	k2eAgNnPc4d1	preventivní
opatření	opatření	k1gNnPc4	opatření
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
říjnajul	říjnajout	k5eAaPmAgInS	říjnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
</s>
<s>
Kerenskij	Kerenskít	k5eAaPmRp2nS	Kerenskít
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
pozatýkat	pozatýkat	k5eAaPmF	pozatýkat
VRV	VRV	kA	VRV
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kabinet	kabinet	k1gInSc1	kabinet
schválil	schválit	k5eAaPmAgInS	schválit
pouze	pouze	k6eAd1	pouze
stíhání	stíhání	k1gNnSc4	stíhání
některých	některý	k3yIgInPc2	některý
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
zastavení	zastavení	k1gNnSc1	zastavení
dvou	dva	k4xCgFnPc2	dva
bolševických	bolševický	k2eAgFnPc2d1	bolševická
novin	novina	k1gFnPc2	novina
a	a	k8xC	a
posílení	posílení	k1gNnSc4	posílení
obrany	obrana	k1gFnSc2	obrana
Zimního	zimní	k2eAgInSc2d1	zimní
paláce	palác	k1gInSc2	palác
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
nepatrná	nepatrný	k2eAgNnPc1d1	nepatrný
a	a	k8xC	a
nedostatečná	dostatečný	k2eNgNnPc1d1	nedostatečné
opatření	opatření	k1gNnPc1	opatření
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
rozpoutala	rozpoutat	k5eAaPmAgFnS	rozpoutat
konfrontaci	konfrontace	k1gFnSc4	konfrontace
a	a	k8xC	a
jako	jako	k9	jako
údajně	údajně	k6eAd1	údajně
"	"	kIx"	"
<g/>
kontrarevoluční	kontrarevoluční	k2eAgFnSc2d1	kontrarevoluční
<g/>
"	"	kIx"	"
činy	čina	k1gFnSc2	čina
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
záminku	záminka	k1gFnSc4	záminka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
očekávali	očekávat	k5eAaImAgMnP	očekávat
protivníci	protivník	k1gMnPc1	protivník
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Kerenskij	Kerenskít	k5eAaPmRp2nS	Kerenskít
dále	daleko	k6eAd2	daleko
nařídil	nařídit	k5eAaPmAgMnS	nařídit
Bagratunimu	Bagratunima	k1gFnSc4	Bagratunima
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
předložil	předložit	k5eAaPmAgMnS	předložit
VRV	VRV	kA	VRV
ultimátum	ultimátum	k1gNnSc4	ultimátum
<g/>
:	:	kIx,	:
má	mít	k5eAaImIp3nS	mít
zrušit	zrušit	k5eAaPmF	zrušit
svůj	svůj	k3xOyFgInSc4	svůj
příkaz	příkaz	k1gInSc4	příkaz
k	k	k7c3	k
ovládnutí	ovládnutí	k1gNnSc3	ovládnutí
posádky	posádka	k1gFnSc2	posádka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
budou	být	k5eAaImBp3nP	být
přijata	přijmout	k5eAaPmNgNnP	přijmout
veškerá	veškerý	k3xTgNnPc1	veškerý
opatření	opatření	k1gNnPc1	opatření
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vláda	vláda	k1gFnSc1	vláda
bude	být	k5eAaImBp3nS	být
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
nutná	nutný	k2eAgNnPc1d1	nutné
k	k	k7c3	k
obnovení	obnovení	k1gNnSc3	obnovení
pořádku	pořádek	k1gInSc2	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
Večer	večer	k6eAd1	večer
se	se	k3xPyFc4	se
VRV	VRV	kA	VRV
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
umírněných	umírněný	k2eAgInPc2d1	umírněný
členů	člen	k1gInPc2	člen
Petrohradského	petrohradský	k2eAgInSc2d1	petrohradský
sovětu	sovět	k1gInSc2	sovět
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
přijmout	přijmout	k5eAaPmF	přijmout
Kerenského	Kerenský	k2eAgMnSc4d1	Kerenský
ultimátum	ultimátum	k1gNnSc4	ultimátum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tento	tento	k3xDgInSc1	tento
ústupek	ústupek	k1gInSc1	ústupek
se	se	k3xPyFc4	se
ukázal	ukázat	k5eAaPmAgInS	ukázat
marný	marný	k2eAgInSc1d1	marný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozložení	rozložení	k1gNnSc1	rozložení
sil	síla	k1gFnPc2	síla
a	a	k8xC	a
postoj	postoj	k1gInSc1	postoj
posádky	posádka	k1gFnSc2	posádka
===	===	k?	===
</s>
</p>
<p>
<s>
Vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
spoléhala	spoléhat	k5eAaImAgFnS	spoléhat
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
studenty	student	k1gMnPc4	student
důstojnických	důstojnický	k2eAgFnPc2d1	důstojnická
škol	škola	k1gFnPc2	škola
(	(	kIx(	(
<g/>
kadety	kadet	k1gMnPc7	kadet
čili	čili	k8xC	čili
junkery	junker	k1gMnPc7	junker
<g/>
)	)	kIx)	)
a	a	k8xC	a
tři	tři	k4xCgInPc4	tři
pluky	pluk	k1gInPc4	pluk
kozácké	kozácký	k2eAgFnSc2d1	kozácká
jízdy	jízda	k1gFnSc2	jízda
umístěné	umístěný	k2eAgFnSc2d1	umístěná
v	v	k7c6	v
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
<g/>
;	;	kIx,	;
bolševici	bolševik	k1gMnPc1	bolševik
se	se	k3xPyFc4	se
opírali	opírat	k5eAaImAgMnP	opírat
o	o	k7c4	o
rudé	rudý	k2eAgFnPc4d1	rudá
gardy	garda	k1gFnPc4	garda
(	(	kIx(	(
<g/>
početné	početný	k2eAgFnPc4d1	početná
a	a	k8xC	a
odhodlané	odhodlaný	k2eAgFnPc4d1	odhodlaná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nezkušené	zkušený	k2eNgFnPc1d1	nezkušená
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
námořníky	námořník	k1gMnPc4	námořník
(	(	kIx(	(
<g/>
nečetné	četný	k2eNgMnPc4d1	četný
<g/>
)	)	kIx)	)
a	a	k8xC	a
vojáky	voják	k1gMnPc4	voják
(	(	kIx(	(
<g/>
zkušené	zkušený	k2eAgFnPc4d1	zkušená
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
početné	početný	k2eAgFnPc1d1	početná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většinou	většina	k1gFnSc7	většina
pasivní	pasivní	k2eAgInSc4d1	pasivní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bolševici	bolševik	k1gMnPc1	bolševik
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
vlichotit	vlichotit	k5eAaPmF	vlichotit
mužstvu	mužstvo	k1gNnSc3	mužstvo
stopadesátitisícové	stopadesátitisícový	k2eAgFnSc2d1	stopadesátitisícová
městské	městský	k2eAgFnSc2d1	městská
posádky	posádka	k1gFnSc2	posádka
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jim	on	k3xPp3gMnPc3	on
pomáhala	pomáhat	k5eAaImAgFnS	pomáhat
nechuť	nechuť	k1gFnSc1	nechuť
vojáků	voják	k1gMnPc2	voják
odjet	odjet	k5eAaPmF	odjet
na	na	k7c4	na
frontu	fronta	k1gFnSc4	fronta
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
si	se	k3xPyFc3	se
přála	přát	k5eAaImAgFnS	přát
vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
velitel	velitel	k1gMnSc1	velitel
severní	severní	k2eAgFnSc2d1	severní
fronty	fronta	k1gFnSc2	fronta
<g/>
.17	.17	k4	.17
<g/>
.	.	kIx.	.
říjnajul	říjnajout	k5eAaPmAgInS	říjnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1917	[number]	k4	1917
<g/>
greg	greg	k1gInSc1	greg
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
Vladimir	Vladimir	k1gMnSc1	Vladimir
Čeremisov	Čeremisov	k1gInSc1	Čeremisov
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
komisař	komisař	k1gMnSc1	komisař
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
bolševik	bolševik	k1gMnSc1	bolševik
Vojtinskij	Vojtinskij	k1gMnSc1	Vojtinskij
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Pskově	Pskov	k1gInSc6	Pskov
sešli	sejít	k5eAaPmAgMnP	sejít
s	s	k7c7	s
vojáky	voják	k1gMnPc7	voják
posádky	posádka	k1gFnSc2	posádka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jim	on	k3xPp3gMnPc3	on
vysvětlili	vysvětlit	k5eAaPmAgMnP	vysvětlit
potřebu	potřeba	k1gFnSc4	potřeba
odjet	odjet	k5eAaPmF	odjet
na	na	k7c4	na
frontu	fronta	k1gFnSc4	fronta
k	k	k7c3	k
obraně	obrana	k1gFnSc3	obrana
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Delegaci	delegace	k1gFnSc4	delegace
posádky	posádka	k1gFnSc2	posádka
tvořili	tvořit	k5eAaImAgMnP	tvořit
většinou	většinou	k6eAd1	většinou
bolševici	bolševik	k1gMnPc1	bolševik
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
podezřívaví	podezřívavý	k2eAgMnPc1d1	podezřívavý
ohledně	ohledně	k7c2	ohledně
motivu	motiv	k1gInSc2	motiv
schůzky	schůzka	k1gFnSc2	schůzka
a	a	k8xC	a
trvali	trvat	k5eAaImAgMnP	trvat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
přesunech	přesun	k1gInPc6	přesun
jednotek	jednotka	k1gFnPc2	jednotka
má	mít	k5eAaImIp3nS	mít
poslední	poslední	k2eAgNnSc4d1	poslední
slovo	slovo	k1gNnSc4	slovo
Petrohradský	petrohradský	k2eAgInSc1d1	petrohradský
sovět	sovět	k1gInSc1	sovět
<g/>
.	.	kIx.	.
</s>
<s>
Strany	strana	k1gFnPc1	strana
se	se	k3xPyFc4	se
nedohodly	dohodnout	k5eNaPmAgFnP	dohodnout
a	a	k8xC	a
o	o	k7c4	o
pět	pět	k4xCc4	pět
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
Čeremisova	Čeremisův	k2eAgFnSc1d1	Čeremisův
5	[number]	k4	5
<g/>
.	.	kIx.	.
armáda	armáda	k1gFnSc1	armáda
zvolila	zvolit	k5eAaPmAgFnS	zvolit
nový	nový	k2eAgInSc4d1	nový
výbor	výbor	k1gInSc4	výbor
s	s	k7c7	s
většinou	většina	k1gFnSc7	většina
bolševiků	bolševik	k1gMnPc2	bolševik
<g/>
.	.	kIx.	.
<g/>
Rovněž	rovněž	k9	rovněž
17	[number]	k4	17
<g/>
.	.	kIx.	.
říjnajul	říjnajout	k5eAaPmAgInS	říjnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1917	[number]	k4	1917
<g/>
greg	greg	k1gInSc1	greg
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
ustavil	ustavit	k5eAaPmAgInS	ustavit
Vojenský	vojenský	k2eAgInSc1d1	vojenský
revoluční	revoluční	k2eAgInSc1d1	revoluční
výbor	výbor	k1gInSc1	výbor
<g/>
,	,	kIx,	,
orgán	orgán	k1gInSc1	orgán
městského	městský	k2eAgInSc2d1	městský
sovětu	sovět	k1gInSc2	sovět
pro	pro	k7c4	pro
řízení	řízení	k1gNnSc4	řízení
vojenských	vojenský	k2eAgFnPc2d1	vojenská
záležitostí	záležitost	k1gFnPc2	záležitost
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
fakticky	fakticky	k6eAd1	fakticky
ovládán	ovládat	k5eAaImNgInS	ovládat
paralelním	paralelní	k2eAgInSc7d1	paralelní
orgánem	orgán	k1gInSc7	orgán
bolševické	bolševický	k2eAgFnSc2d1	bolševická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
po	po	k7c6	po
setkání	setkání	k1gNnSc6	setkání
s	s	k7c7	s
Čeremisovem	Čeremisovo	k1gNnSc7	Čeremisovo
se	se	k3xPyFc4	se
zástupci	zástupce	k1gMnPc1	zástupce
posádky	posádka	k1gFnSc2	posádka
setkali	setkat	k5eAaPmAgMnP	setkat
ve	v	k7c6	v
Smolném	smolný	k2eAgInSc6d1	smolný
a	a	k8xC	a
vyslovili	vyslovit	k5eAaPmAgMnP	vyslovit
nedůvěru	nedůvěra	k1gFnSc4	nedůvěra
vládě	vláda	k1gFnSc3	vláda
a	a	k8xC	a
podporu	podpora	k1gFnSc4	podpora
Petrohradskému	petrohradský	k2eAgInSc3d1	petrohradský
sovětu	sovět	k1gInSc3	sovět
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
Ispolkomu	Ispolkom	k1gInSc2	Ispolkom
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
setkání	setkání	k1gNnPc4	setkání
schválili	schválit	k5eAaPmAgMnP	schválit
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
odešli	odejít	k5eAaPmAgMnP	odejít
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nP	by
byli	být	k5eAaImAgMnP	být
připuštěni	připustit	k5eAaPmNgMnP	připustit
ke	k	k7c3	k
slovu	slovo	k1gNnSc3	slovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Události	událost	k1gFnPc1	událost
v	v	k7c6	v
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Uzavření	uzavření	k1gNnSc1	uzavření
tiskáren	tiskárna	k1gFnPc2	tiskárna
a	a	k8xC	a
první	první	k4xOgInPc1	první
střety	střet	k1gInPc1	střet
===	===	k?	===
</s>
</p>
<p>
<s>
24	[number]	k4	24
<g/>
.	.	kIx.	.
říjnajul	říjnajout	k5eAaPmAgInS	říjnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
kolem	kolem	k7c2	kolem
tří	tři	k4xCgFnPc2	tři
hodin	hodina	k1gFnPc2	hodina
ráno	ráno	k6eAd1	ráno
byl	být	k5eAaImAgInS	být
Kerenskij	Kerenskij	k1gFnSc4	Kerenskij
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
vojenských	vojenský	k2eAgMnPc2d1	vojenský
velitelů	velitel	k1gMnPc2	velitel
v	v	k7c6	v
Zimním	zimní	k2eAgInSc6d1	zimní
paláci	palác	k1gInSc6	palác
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
pracovali	pracovat	k5eAaImAgMnP	pracovat
na	na	k7c6	na
opatřeních	opatření	k1gNnPc6	opatření
proti	proti	k7c3	proti
bolševikům	bolševik	k1gMnPc3	bolševik
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
ultimátum	ultimátum	k1gNnSc4	ultimátum
zaslané	zaslaný	k2eAgFnSc2d1	zaslaná
VRV	VRV	kA	VRV
nepřišla	přijít	k5eNaPmAgFnS	přijít
odpověď	odpověď	k1gFnSc4	odpověď
a	a	k8xC	a
kabinet	kabinet	k1gInSc4	kabinet
krátce	krátce	k6eAd1	krátce
předtím	předtím	k6eAd1	předtím
schválil	schválit	k5eAaPmAgMnS	schválit
zatčení	zatčení	k1gNnSc4	zatčení
vůdců	vůdce	k1gMnPc2	vůdce
VRV	VRV	kA	VRV
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
přišla	přijít	k5eAaPmAgFnS	přijít
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
VRV	VRV	kA	VRV
nakonec	nakonec	k6eAd1	nakonec
přijal	přijmout	k5eAaPmAgMnS	přijmout
požadavky	požadavek	k1gInPc4	požadavek
vojenských	vojenský	k2eAgMnPc2d1	vojenský
velitelů	velitel	k1gMnPc2	velitel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Kerenskij	Kerenskij	k1gMnSc1	Kerenskij
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
opustit	opustit	k5eAaPmF	opustit
svůj	svůj	k3xOyFgInSc4	svůj
plán	plán	k1gInSc4	plán
<g/>
.	.	kIx.	.
</s>
<s>
Generál	generál	k1gMnSc1	generál
Bagratuni	Bagratuň	k1gMnSc3	Bagratuň
dál	daleko	k6eAd2	daleko
žádal	žádat	k5eAaImAgMnS	žádat
o	o	k7c4	o
posily	posila	k1gFnPc4	posila
z	z	k7c2	z
fronty	fronta	k1gFnSc2	fronta
(	(	kIx(	(
<g/>
junkery	junker	k1gMnPc4	junker
z	z	k7c2	z
Oranienbaumu	Oranienbaum	k1gInSc2	Oranienbaum
<g/>
,	,	kIx,	,
útočné	útočný	k2eAgFnPc1d1	útočná
jednotky	jednotka	k1gFnPc1	jednotka
z	z	k7c2	z
Carského	carský	k2eAgNnSc2d1	carské
sela	selo	k1gNnSc2	selo
a	a	k8xC	a
dělostřelce	dělostřelec	k1gMnSc4	dělostřelec
z	z	k7c2	z
Pavlovsku	Pavlovsko	k1gNnSc6	Pavlovsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
plukovník	plukovník	k1gMnSc1	plukovník
Polkovnikov	Polkovnikov	k1gInSc4	Polkovnikov
nařídil	nařídit	k5eAaPmAgMnS	nařídit
zatknout	zatknout	k5eAaPmF	zatknout
komisaře	komisař	k1gMnPc4	komisař
poslané	poslaný	k2eAgMnPc4d1	poslaný
z	z	k7c2	z
VRV	VRV	kA	VRV
k	k	k7c3	k
jednotkám	jednotka	k1gFnPc3	jednotka
posádky	posádka	k1gFnSc2	posádka
a	a	k8xC	a
postavit	postavit	k5eAaPmF	postavit
je	on	k3xPp3gMnPc4	on
před	před	k7c4	před
soud	soud	k1gInSc4	soud
<g/>
.	.	kIx.	.
</s>
<s>
VRV	VRV	kA	VRV
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
souzen	soudit	k5eAaImNgInS	soudit
a	a	k8xC	a
propuštění	propuštěný	k2eAgMnPc1d1	propuštěný
účastníci	účastník	k1gMnPc1	účastník
červnových	červnový	k2eAgInPc2d1	červnový
nepokojů	nepokoj	k1gInPc2	nepokoj
měli	mít	k5eAaImAgMnP	mít
být	být	k5eAaImF	být
zase	zase	k9	zase
uvězněni	uvězněn	k2eAgMnPc1d1	uvězněn
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
podněcování	podněcování	k1gNnSc4	podněcování
nepokojů	nepokoj	k1gInPc2	nepokoj
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
zastaveny	zastaven	k2eAgInPc4d1	zastaven
dva	dva	k4xCgInPc4	dva
bolševické	bolševický	k2eAgInPc4d1	bolševický
deníky	deník	k1gInPc4	deník
<g/>
,	,	kIx,	,
Rabočij	Rabočij	k1gFnPc4	Rabočij
puť	puťa	k1gFnPc2	puťa
a	a	k8xC	a
Soldat	Soldat	k1gFnPc2	Soldat
<g/>
,	,	kIx,	,
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
zdání	zdání	k1gNnSc1	zdání
nestrannosti	nestrannost	k1gFnSc2	nestrannost
<g/>
,	,	kIx,	,
zákaz	zákaz	k1gInSc1	zákaz
měl	mít	k5eAaImAgInS	mít
postihnout	postihnout	k5eAaPmF	postihnout
i	i	k9	i
dvoje	dvoje	k4xRgFnPc4	dvoje
konzervativní	konzervativní	k2eAgFnPc4d1	konzervativní
noviny	novina	k1gFnPc4	novina
<g/>
.	.	kIx.	.
</s>
<s>
Stráž	stráž	k1gFnSc1	stráž
Zimního	zimní	k2eAgInSc2d1	zimní
paláce	palác	k1gInSc2	palác
byla	být	k5eAaImAgFnS	být
opět	opět	k6eAd1	opět
posílena	posílit	k5eAaPmNgFnS	posílit
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vědomím	vědomí	k1gNnSc7	vědomí
<g/>
,	,	kIx,	,
že	že	k8xS	že
politické	politický	k2eAgFnPc1d1	politická
strany	strana	k1gFnPc1	strana
nebudou	být	k5eNaImBp3nP	být
tato	tento	k3xDgNnPc1	tento
opatření	opatření	k1gNnSc1	opatření
vnímat	vnímat	k5eAaImF	vnímat
příznivě	příznivě	k6eAd1	příznivě
<g/>
,	,	kIx,	,
přijal	přijmout	k5eAaPmAgMnS	přijmout
pak	pak	k6eAd1	pak
Kerenskij	Kerenskij	k1gMnSc3	Kerenskij
návrh	návrh	k1gInSc4	návrh
několika	několik	k4yIc2	několik
ministrů	ministr	k1gMnPc2	ministr
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
týž	týž	k3xTgInSc4	týž
den	den	k1gInSc4	den
promluvil	promluvit	k5eAaPmAgMnS	promluvit
o	o	k7c4	o
situaci	situace	k1gFnSc4	situace
v	v	k7c6	v
předparlamentu	předparlament	k1gInSc6	předparlament
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
svítání	svítání	k1gNnSc6	svítání
v	v	k7c4	v
půl	půl	k1xP	půl
šesté	šestý	k4xOgFnSc2	šestý
malé	malý	k2eAgFnSc2d1	malá
jednotky	jednotka	k1gFnSc2	jednotka
kadetů	kadet	k1gMnPc2	kadet
a	a	k8xC	a
milice	milice	k1gFnSc1	milice
<g/>
,	,	kIx,	,
vyslané	vyslaný	k2eAgNnSc1d1	vyslané
vládou	vláda	k1gFnSc7	vláda
<g/>
,	,	kIx,	,
napadly	napadnout	k5eAaPmAgFnP	napadnout
tiskárny	tiskárna	k1gFnPc1	tiskárna
dvou	dva	k4xCgFnPc2	dva
bolševických	bolševický	k2eAgFnPc2d1	bolševická
novin	novina	k1gFnPc2	novina
<g/>
,	,	kIx,	,
zničily	zničit	k5eAaPmAgFnP	zničit
aktuální	aktuální	k2eAgNnSc4d1	aktuální
vydání	vydání	k1gNnSc4	vydání
<g/>
,	,	kIx,	,
poškodily	poškodit	k5eAaPmAgInP	poškodit
vybavení	vybavení	k1gNnSc4	vybavení
a	a	k8xC	a
zastavily	zastavit	k5eAaPmAgFnP	zastavit
tiskařské	tiskařský	k2eAgInPc4d1	tiskařský
stroje	stroj	k1gInPc4	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
odešli	odejít	k5eAaPmAgMnP	odejít
do	do	k7c2	do
blízkého	blízký	k2eAgInSc2d1	blízký
Smolného	smolný	k2eAgInSc2d1	smolný
institutu	institut	k1gInSc2	institut
<g/>
,	,	kIx,	,
sídla	sídlo	k1gNnSc2	sídlo
Petrohradského	petrohradský	k2eAgInSc2d1	petrohradský
sovětu	sovět	k1gInSc2	sovět
<g/>
,	,	kIx,	,
Petrohradského	petrohradský	k2eAgInSc2d1	petrohradský
revolučního	revoluční	k2eAgInSc2d1	revoluční
vojenského	vojenský	k2eAgInSc2d1	vojenský
výboru	výbor	k1gInSc2	výbor
a	a	k8xC	a
bolševické	bolševický	k2eAgFnSc2d1	bolševická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
tam	tam	k6eAd1	tam
o	o	k7c6	o
událostech	událost	k1gFnPc6	událost
informovali	informovat	k5eAaBmAgMnP	informovat
<g/>
.	.	kIx.	.
</s>
<s>
Bolševici	bolševik	k1gMnPc1	bolševik
svolali	svolat	k5eAaPmAgMnP	svolat
krizové	krizový	k2eAgNnSc4d1	krizové
zasedání	zasedání	k1gNnSc4	zasedání
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
zástupců	zástupce	k1gMnPc2	zástupce
sovětu	sovět	k1gInSc2	sovět
<g/>
,	,	kIx,	,
VRV	VRV	kA	VRV
a	a	k8xC	a
levých	levý	k2eAgMnPc2d1	levý
eserů	eser	k1gMnPc2	eser
<g/>
;	;	kIx,	;
akce	akce	k1gFnSc1	akce
vlády	vláda	k1gFnSc2	vláda
byla	být	k5eAaImAgFnS	být
označena	označit	k5eAaPmNgFnS	označit
za	za	k7c4	za
kontrarevoluční	kontrarevoluční	k2eAgMnPc4d1	kontrarevoluční
<g/>
;	;	kIx,	;
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
začaly	začít	k5eAaPmAgFnP	začít
přicházet	přicházet	k5eAaImF	přicházet
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
podezřelých	podezřelý	k2eAgInPc6d1	podezřelý
pohybech	pohyb	k1gInPc6	pohyb
vojenských	vojenský	k2eAgFnPc2d1	vojenská
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
VRV	VRV	kA	VRV
ihned	ihned	k6eAd1	ihned
považoval	považovat	k5eAaImAgMnS	považovat
události	událost	k1gFnPc4	událost
za	za	k7c4	za
kontrarevoluční	kontrarevoluční	k2eAgInSc4d1	kontrarevoluční
útok	útok	k1gInSc4	útok
na	na	k7c4	na
sjezd	sjezd	k1gInSc4	sjezd
sovětů	sovět	k1gInPc2	sovět
a	a	k8xC	a
rozeslal	rozeslat	k5eAaPmAgInS	rozeslat
jednotkám	jednotka	k1gFnPc3	jednotka
"	"	kIx"	"
<g/>
Rozkaz	rozkaz	k1gInSc1	rozkaz
č.	č.	k?	č.
1	[number]	k4	1
<g/>
"	"	kIx"	"
nařizující	nařizující	k2eAgFnSc6d1	nařizující
mobilizaci	mobilizace	k1gFnSc6	mobilizace
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
přání	přání	k1gNnSc4	přání
menšiny	menšina	k1gFnSc2	menšina
v	v	k7c6	v
VRV	VRV	kA	VRV
a	a	k8xC	a
bolševické	bolševický	k2eAgFnSc6d1	bolševická
straně	strana	k1gFnSc6	strana
byla	být	k5eAaImAgFnS	být
odmítnuta	odmítnut	k2eAgFnSc1d1	odmítnuta
myšlenka	myšlenka	k1gFnSc1	myšlenka
okamžitého	okamžitý	k2eAgNnSc2d1	okamžité
povstání	povstání	k1gNnSc2	povstání
a	a	k8xC	a
byla	být	k5eAaImAgNnP	být
přijata	přijmout	k5eAaPmNgNnP	přijmout
pouze	pouze	k6eAd1	pouze
opatření	opatření	k1gNnPc4	opatření
zajišťující	zajišťující	k2eAgNnPc4d1	zajišťující
konání	konání	k1gNnSc4	konání
sjezdu	sjezd	k1gInSc2	sjezd
sovětů	sovět	k1gInPc2	sovět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
deset	deset	k4xCc4	deset
hodin	hodina	k1gFnPc2	hodina
dopoledne	dopoledne	k6eAd1	dopoledne
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
sešel	sejít	k5eAaPmAgInS	sejít
kabinet	kabinet	k1gInSc1	kabinet
a	a	k8xC	a
Kerenskij	Kerenskij	k1gMnSc1	Kerenskij
ho	on	k3xPp3gInSc4	on
informoval	informovat	k5eAaBmAgMnS	informovat
o	o	k7c6	o
opatřeních	opatření	k1gNnPc6	opatření
přijatých	přijatý	k2eAgFnPc2d1	přijatá
ráno	ráno	k6eAd1	ráno
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
pochybnosti	pochybnost	k1gFnPc4	pochybnost
některých	některý	k3yIgMnPc2	některý
ministrů	ministr	k1gMnPc2	ministr
stále	stále	k6eAd1	stále
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
situaci	situace	k1gFnSc4	situace
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
milice	milice	k1gFnSc1	milice
však	však	k9	však
neuposlechla	uposlechnout	k5eNaPmAgFnS	uposlechnout
rozkaz	rozkaz	k1gInSc4	rozkaz
zatknout	zatknout	k5eAaPmF	zatknout
členy	člen	k1gInPc7	člen
VRV	VRV	kA	VRV
a	a	k8xC	a
rozpustit	rozpustit	k5eAaPmF	rozpustit
jednotky	jednotka	k1gFnPc1	jednotka
loajální	loajální	k2eAgFnPc1d1	loajální
bolševikům	bolševik	k1gMnPc3	bolševik
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
podléhala	podléhat	k5eAaImAgFnS	podléhat
sovětu	sovět	k1gInSc3	sovět
a	a	k8xC	a
ne	ne	k9	ne
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následujících	následující	k2eAgInPc2d1	následující
dní	den	k1gInPc2	den
milice	milice	k1gFnSc2	milice
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
policejních	policejní	k2eAgFnPc6d1	policejní
činnostech	činnost	k1gFnPc6	činnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepodílela	podílet	k5eNaImAgFnS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
obraně	obrana	k1gFnSc6	obrana
Prozatímní	prozatímní	k2eAgFnSc2d1	prozatímní
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
opatření	opatření	k1gNnSc2	opatření
nařízených	nařízený	k2eAgInPc2d1	nařízený
Kerenským	Kerenský	k2eAgFnPc3d1	Kerenský
byla	být	k5eAaImAgNnP	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
obranná	obranný	k2eAgNnPc4d1	obranné
<g/>
:	:	kIx,	:
jednotky	jednotka	k1gFnPc1	jednotka
kadetů	kadet	k1gMnPc2	kadet
byly	být	k5eAaImAgFnP	být
vyslány	vyslán	k2eAgFnPc1d1	vyslána
střežit	střežit	k5eAaImF	střežit
nádraží	nádraží	k1gNnSc4	nádraží
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
posílena	posílit	k5eAaPmNgFnS	posílit
stráž	stráž	k1gFnSc1	stráž
Zimního	zimní	k2eAgInSc2d1	zimní
paláce	palác	k1gInSc2	palác
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
zasedání	zasedání	k1gNnSc6	zasedání
vlády	vláda	k1gFnSc2	vláda
premiér	premiér	k1gMnSc1	premiér
Kerenskij	Kerenskij	k1gMnSc1	Kerenskij
dál	daleko	k6eAd2	daleko
neúspěšně	úspěšně	k6eNd1	úspěšně
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
urychlit	urychlit	k5eAaPmF	urychlit
příchod	příchod	k1gInSc4	příchod
jednotek	jednotka	k1gFnPc2	jednotka
loajálních	loajální	k2eAgFnPc2d1	loajální
vládě	vláda	k1gFnSc3	vláda
do	do	k7c2	do
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
<g/>
,	,	kIx,	,
zadržet	zadržet	k5eAaPmF	zadržet
jednotky	jednotka	k1gFnPc4	jednotka
městské	městský	k2eAgFnSc2d1	městská
posádky	posádka	k1gFnSc2	posádka
v	v	k7c6	v
kasárnách	kasárny	k1gFnPc6	kasárny
a	a	k8xC	a
odstranit	odstranit	k5eAaPmF	odstranit
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
komisaře	komisař	k1gMnSc2	komisař
vyslané	vyslaný	k2eAgFnSc2d1	vyslaná
VRV	VRV	kA	VRV
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dopoledne	dopoledne	k1gNnSc2	dopoledne
a	a	k8xC	a
časného	časný	k2eAgNnSc2d1	časné
odpoledne	odpoledne	k1gNnSc2	odpoledne
se	se	k3xPyFc4	se
vyjasnilo	vyjasnit	k5eAaPmAgNnS	vyjasnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
jednotek	jednotka	k1gFnPc2	jednotka
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
poslouchá	poslouchat	k5eAaImIp3nS	poslouchat
pokyny	pokyn	k1gInPc4	pokyn
Petrohradského	petrohradský	k2eAgInSc2d1	petrohradský
sovětu	sovět	k1gInSc2	sovět
a	a	k8xC	a
rozkazy	rozkaz	k1gInPc1	rozkaz
vlády	vláda	k1gFnSc2	vláda
ignoruje	ignorovat	k5eAaImIp3nS	ignorovat
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
obviňovaly	obviňovat	k5eAaImAgFnP	obviňovat
ze	z	k7c2	z
zrady	zrada	k1gFnSc2	zrada
revoluce	revoluce	k1gFnSc2	revoluce
a	a	k8xC	a
pasovaly	pasovat	k5eAaImAgFnP	pasovat
se	se	k3xPyFc4	se
na	na	k7c4	na
její	její	k3xOp3gMnPc4	její
obránce	obránce	k1gMnPc4	obránce
<g/>
.	.	kIx.	.
<g/>
Odpoledne	odpoledne	k6eAd1	odpoledne
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
přijít	přijít	k5eAaPmF	přijít
do	do	k7c2	do
města	město	k1gNnSc2	město
posílit	posílit	k5eAaPmF	posílit
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
přešly	přejít	k5eAaPmAgFnP	přejít
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
sovětu	sovět	k1gInSc2	sovět
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
byly	být	k5eAaImAgInP	být
zdrženy	zdržen	k2eAgInPc1d1	zdržen
jeho	jeho	k3xOp3gMnSc7	jeho
příznivci	příznivec	k1gMnPc7	příznivec
<g/>
.	.	kIx.	.
</s>
<s>
Námořníci	námořník	k1gMnPc1	námořník
křižníku	křižník	k1gInSc2	křižník
Aurora	Aurora	k1gFnSc1	Aurora
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
opravoval	opravovat	k5eAaImAgMnS	opravovat
ve	v	k7c6	v
franko-ruských	frankouský	k2eAgFnPc6d1	franko-ruský
loděnicích	loděnice	k1gFnPc6	loděnice
<g/>
,	,	kIx,	,
povstali	povstat	k5eAaPmAgMnP	povstat
proti	proti	k7c3	proti
svým	svůj	k3xOyFgMnPc3	svůj
důstojníkům	důstojník	k1gMnPc3	důstojník
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
rozkazu	rozkaz	k1gInSc2	rozkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
křižník	křižník	k1gInSc1	křižník
má	mít	k5eAaImIp3nS	mít
opustit	opustit	k5eAaPmF	opustit
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
když	když	k8xS	když
sovět	sovět	k1gInSc1	sovět
loďstva	loďstvo	k1gNnSc2	loďstvo
tento	tento	k3xDgMnSc1	tento
rozkaz	rozkaz	k1gInSc4	rozkaz
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
měla	mít	k5eAaImAgFnS	mít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
jen	jen	k9	jen
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInSc4	tisíc
ozbrojenců	ozbrojenec	k1gMnPc2	ozbrojenec
–	–	k?	–
hlavně	hlavně	k6eAd1	hlavně
důstojníků	důstojník	k1gMnPc2	důstojník
<g/>
,	,	kIx,	,
kozáků	kozák	k1gMnPc2	kozák
<g/>
,	,	kIx,	,
kadetů	kadet	k1gMnPc2	kadet
a	a	k8xC	a
batalion	batalion	k1gInSc1	batalion
žen	žena	k1gFnPc2	žena
–	–	k?	–
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
jasné	jasný	k2eAgFnSc6d1	jasná
početní	početní	k2eAgFnSc6d1	početní
nevýhodě	nevýhoda	k1gFnSc6	nevýhoda
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
dvě	dva	k4xCgFnPc1	dva
stovky	stovka	k1gFnPc1	stovka
se	se	k3xPyFc4	se
jich	on	k3xPp3gNnPc2	on
v	v	k7c4	v
poledne	poledne	k1gNnSc4	poledne
shromáždily	shromáždit	k5eAaPmAgInP	shromáždit
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
a	a	k8xC	a
o	o	k7c4	o
dvě	dva	k4xCgFnPc4	dva
hodiny	hodina	k1gFnPc4	hodina
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
připojilo	připojit	k5eAaPmAgNnS	připojit
68	[number]	k4	68
kadetů	kadet	k1gMnPc2	kadet
z	z	k7c2	z
Michailovského	Michailovský	k2eAgNnSc2d1	Michailovský
dělostřelecké	dělostřelecký	k2eAgFnPc4d1	dělostřelecká
akademie	akademie	k1gFnSc2	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Pokusy	pokus	k1gInPc1	pokus
udržet	udržet	k5eAaPmF	udržet
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
městskou	městský	k2eAgFnSc7d1	městská
posádkou	posádka	k1gFnSc7	posádka
byly	být	k5eAaImAgFnP	být
neúspěšné	úspěšný	k2eNgFnPc1d1	neúspěšná
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
většina	většina	k1gFnSc1	většina
jejích	její	k3xOp3gMnPc2	její
vojáků	voják	k1gMnPc2	voják
neměla	mít	k5eNaImAgFnS	mít
chuť	chuť	k1gFnSc4	chuť
se	se	k3xPyFc4	se
účastnit	účastnit	k5eAaImF	účastnit
bojů	boj	k1gInPc2	boj
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
ochotni	ochoten	k2eAgMnPc1d1	ochoten
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
chtěli	chtít	k5eAaImAgMnP	chtít
bojovat	bojovat	k5eAaImF	bojovat
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
sovětu	sovět	k1gInSc2	sovět
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
jednotek	jednotka	k1gFnPc2	jednotka
tak	tak	k9	tak
zůstala	zůstat	k5eAaPmAgFnS	zůstat
v	v	k7c6	v
kasárnách	kasárny	k1gFnPc6	kasárny
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ty	ten	k3xDgFnPc1	ten
nejvíce	hodně	k6eAd3	hodně
radikalizované	radikalizovaný	k2eAgFnPc1d1	radikalizovaná
přišly	přijít	k5eAaPmAgFnP	přijít
na	na	k7c4	na
pokyn	pokyn	k1gInSc4	pokyn
sovětu	sovět	k1gInSc2	sovět
<g/>
,	,	kIx,	,
a	a	k8xC	a
totéž	týž	k3xTgNnSc1	týž
udělala	udělat	k5eAaPmAgFnS	udělat
většina	většina	k1gFnSc1	většina
rudých	rudý	k2eAgFnPc2d1	rudá
gard	garda	k1gFnPc2	garda
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
dost	dost	k6eAd1	dost
bojovníků	bojovník	k1gMnPc2	bojovník
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zajistili	zajistit	k5eAaPmAgMnP	zajistit
početní	početní	k2eAgFnSc4d1	početní
převahu	převaha	k1gFnSc4	převaha
Petrohradského	petrohradský	k2eAgInSc2d1	petrohradský
sovětu	sovět	k1gInSc2	sovět
nad	nad	k7c7	nad
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ochromení	ochromení	k1gNnPc4	ochromení
vlády	vláda	k1gFnSc2	vláda
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c4	v
poledne	poledne	k1gNnSc4	poledne
začalo	začít	k5eAaPmAgNnS	začít
nové	nový	k2eAgNnSc4d1	nové
zasedání	zasedání	k1gNnSc4	zasedání
předparlamentu	předparlament	k1gInSc2	předparlament
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
řídil	řídit	k5eAaImAgMnS	řídit
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Avksenťjev	Avksenťjev	k1gMnSc1	Avksenťjev
<g/>
.	.	kIx.	.
</s>
<s>
Kerenskij	Kerenskít	k5eAaPmRp2nS	Kerenskít
dorazil	dorazit	k5eAaPmAgMnS	dorazit
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
a	a	k8xC	a
vyžádal	vyžádat	k5eAaPmAgMnS	vyžádat
si	se	k3xPyFc3	se
mimořádný	mimořádný	k2eAgInSc4d1	mimořádný
projev	projev	k1gInSc4	projev
před	před	k7c7	před
shromážděním	shromáždění	k1gNnSc7	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hodinové	hodinový	k2eAgFnSc6d1	hodinová
řeči	řeč	k1gFnSc6	řeč
pronesené	pronesený	k2eAgMnPc4d1	pronesený
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
stylem	styl	k1gInSc7	styl
popsal	popsat	k5eAaPmAgMnS	popsat
události	událost	k1gFnPc4	událost
posledních	poslední	k2eAgFnPc2d1	poslední
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
bezpodmínečnou	bezpodmínečný	k2eAgFnSc4d1	bezpodmínečná
podporu	podpora	k1gFnSc4	podpora
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
shromážděním	shromáždění	k1gNnSc7	shromáždění
zamítnuta	zamítnout	k5eAaPmNgFnS	zamítnout
i	i	k9	i
přes	přes	k7c4	přes
nepřítomnost	nepřítomnost	k1gFnSc4	nepřítomnost
radikální	radikální	k2eAgNnSc4d1	radikální
levicel	levicet	k5eAaPmAgMnS	levicet
a	a	k8xC	a
přes	přes	k7c4	přes
ovace	ovace	k1gFnPc4	ovace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zazněly	zaznít	k5eAaPmAgFnP	zaznít
po	po	k7c6	po
projevu	projev	k1gInSc6	projev
<g/>
.	.	kIx.	.
</s>
<s>
Umírněná	umírněný	k2eAgFnSc1d1	umírněná
levice	levice	k1gFnSc1	levice
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
sešla	sejít	k5eAaPmAgFnS	sejít
v	v	k7c6	v
Všeruském	všeruský	k2eAgInSc6d1	všeruský
ústředním	ústřední	k2eAgInSc6d1	ústřední
výkonném	výkonný	k2eAgInSc6d1	výkonný
výboru	výbor	k1gInSc6	výbor
mezi	mezi	k7c7	mezi
půlnocí	půlnoc	k1gFnSc7	půlnoc
a	a	k8xC	a
čtyřmi	čtyři	k4xCgFnPc7	čtyři
hodinami	hodina	k1gFnPc7	hodina
ráno	ráno	k6eAd1	ráno
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
na	na	k7c6	na
krizovém	krizový	k2eAgNnSc6d1	krizové
zasedání	zasedání	k1gNnSc6	zasedání
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čtyřech	čtyři	k4xCgFnPc6	čtyři
hodinách	hodina	k1gFnPc6	hodina
jednání	jednání	k1gNnSc1	jednání
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
pouze	pouze	k6eAd1	pouze
těsnou	těsný	k2eAgFnSc7d1	těsná
většinou	většina	k1gFnSc7	většina
schválit	schválit	k5eAaPmF	schválit
podporu	podpora	k1gFnSc4	podpora
vlády	vláda	k1gFnSc2	vláda
za	za	k7c2	za
podmínky	podmínka	k1gFnSc2	podmínka
zahájení	zahájení	k1gNnSc2	zahájení
radikálních	radikální	k2eAgFnPc2d1	radikální
reforem	reforma	k1gFnPc2	reforma
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
uspokojili	uspokojit	k5eAaPmAgMnP	uspokojit
příznivci	příznivec	k1gMnPc1	příznivec
bolševiků	bolševik	k1gMnPc2	bolševik
a	a	k8xC	a
uklidnili	uklidnit	k5eAaPmAgMnP	uklidnit
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
reformy	reforma	k1gFnPc4	reforma
žádali	žádat	k5eAaImAgMnP	žádat
již	již	k6eAd1	již
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
ruské	ruský	k2eAgFnSc2d1	ruská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Kerenskij	Kerenskít	k5eAaPmRp2nS	Kerenskít
však	však	k9	však
návrh	návrh	k1gInSc4	návrh
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
v	v	k7c4	v
naději	naděje	k1gFnSc4	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
situaci	situace	k1gFnSc4	situace
zvládne	zvládnout	k5eAaPmIp3nS	zvládnout
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
jen	jen	k9	jen
u	u	k7c2	u
dalšího	další	k2eAgInSc2d1	další
neužitečného	užitečný	k2eNgInSc2d1	neužitečný
apelu	apel	k1gInSc2	apel
ke	k	k7c3	k
klidu	klid	k1gInSc3	klid
a	a	k8xC	a
varování	varování	k1gNnSc4	varování
před	před	k7c7	před
možnou	možný	k2eAgFnSc7d1	možná
kontrarevolucí	kontrarevoluce	k1gFnSc7	kontrarevoluce
v	v	k7c6	v
případě	případ	k1gInSc6	případ
povstání	povstání	k1gNnSc2	povstání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
předparlamentu	předparlament	k1gInSc2	předparlament
se	se	k3xPyFc4	se
Kerenskij	Kerenskij	k1gMnSc1	Kerenskij
odebral	odebrat	k5eAaPmAgMnS	odebrat
na	na	k7c4	na
městské	městský	k2eAgNnSc4d1	Městské
vojenské	vojenský	k2eAgNnSc4d1	vojenské
velitelství	velitelství	k1gNnSc4	velitelství
u	u	k7c2	u
Zimního	zimní	k2eAgInSc2d1	zimní
paláce	palác	k1gInSc2	palác
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
řídil	řídit	k5eAaImAgInS	řídit
opatření	opatření	k1gNnSc4	opatření
proti	proti	k7c3	proti
bolševikům	bolševik	k1gMnPc3	bolševik
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
obranná	obranný	k2eAgFnSc1d1	obranná
<g/>
:	:	kIx,	:
ovládnutí	ovládnutí	k1gNnPc1	ovládnutí
budov	budova	k1gFnPc2	budova
a	a	k8xC	a
strategických	strategický	k2eAgInPc2d1	strategický
bod	bod	k1gInSc4	bod
a	a	k8xC	a
izolace	izolace	k1gFnSc1	izolace
centra	centrum	k1gNnSc2	centrum
pomocí	pomocí	k7c2	pomocí
zvednutí	zvednutí	k1gNnSc2	zvednutí
něvských	něvský	k2eAgInPc2d1	něvský
mostů	most	k1gInPc2	most
<g/>
.	.	kIx.	.
</s>
<s>
Oddíly	oddíl	k1gInPc1	oddíl
kadetů	kadet	k1gMnPc2	kadet
byly	být	k5eAaImAgInP	být
vyslány	vyslán	k2eAgInPc1d1	vyslán
zajistit	zajistit	k5eAaPmF	zajistit
budovy	budova	k1gFnPc4	budova
a	a	k8xC	a
hlídkovat	hlídkovat	k5eAaImF	hlídkovat
na	na	k7c6	na
ulicích	ulice	k1gFnPc6	ulice
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
úřady	úřad	k1gInPc1	úřad
začaly	začít	k5eAaPmAgInP	začít
zavírat	zavírat	k5eAaImF	zavírat
a	a	k8xC	a
v	v	k7c6	v
15	[number]	k4	15
hodin	hodina	k1gFnPc2	hodina
byly	být	k5eAaImAgInP	být
telefony	telefon	k1gInPc1	telefon
Smolného	smolný	k2eAgInSc2d1	smolný
institutu	institut	k1gInSc2	institut
odpojeny	odpojit	k5eAaPmNgInP	odpojit
od	od	k7c2	od
ústředny	ústředna	k1gFnSc2	ústředna
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c4	mezi
14	[number]	k4	14
<g/>
.	.	kIx.	.
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
hodinou	hodina	k1gFnSc7	hodina
kadeti	kadet	k1gMnPc1	kadet
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
Nikolajevský	Nikolajevský	k2eAgInSc4d1	Nikolajevský
most	most	k1gInSc4	most
a	a	k8xC	a
most	most	k1gInSc4	most
u	u	k7c2	u
paláce	palác	k1gInSc2	palác
<g/>
;	;	kIx,	;
dalšímu	další	k1gNnSc3	další
oddílu	oddíl	k1gInSc2	oddíl
dav	dav	k1gInSc1	dav
zabránil	zabránit	k5eAaPmAgInS	zabránit
ovládnout	ovládnout	k5eAaPmF	ovládnout
Litějný	Litějný	k2eAgInSc1d1	Litějný
most	most	k1gInSc1	most
a	a	k8xC	a
tito	tento	k3xDgMnPc1	tento
kadeti	kadet	k1gMnPc1	kadet
byli	být	k5eAaImAgMnP	být
zajati	zajmout	k5eAaPmNgMnP	zajmout
rudými	rudý	k2eAgFnPc7d1	rudá
gardami	garda	k1gFnPc7	garda
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
18	[number]	k4	18
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
Pavlovský	pavlovský	k2eAgInSc1d1	pavlovský
pluk	pluk	k1gInSc1	pluk
<g/>
,	,	kIx,	,
věrný	věrný	k2eAgMnSc1d1	věrný
sovětu	sovět	k1gInSc2	sovět
<g/>
,	,	kIx,	,
obsadil	obsadit	k5eAaPmAgMnS	obsadit
Trojický	trojický	k2eAgInSc4d1	trojický
most	most	k1gInSc4	most
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
předešel	předejít	k5eAaPmAgMnS	předejít
jeho	jeho	k3xOp3gNnSc4	jeho
zabrání	zabrání	k1gNnSc4	zabrání
dalším	další	k2eAgInSc7d1	další
oddílem	oddíl	k1gInSc7	oddíl
kadetů	kadet	k1gMnPc2	kadet
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
komisař	komisař	k1gMnSc1	komisař
petrohradského	petrohradský	k2eAgInSc2d1	petrohradský
pluku	pluk	k1gInSc2	pluk
granátníků	granátník	k1gMnPc2	granátník
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
<g/>
,	,	kIx,	,
vyslal	vyslat	k5eAaPmAgMnS	vyslat
vojáky	voják	k1gMnPc4	voják
obsadit	obsadit	k5eAaPmF	obsadit
mosty	most	k1gInPc1	most
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
než	než	k8xS	než
k	k	k7c3	k
tomu	ten	k3xDgMnSc3	ten
přišel	přijít	k5eAaPmAgMnS	přijít
rozkaz	rozkaz	k1gInSc4	rozkaz
z	z	k7c2	z
VRV	VRV	kA	VRV
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
mosty	most	k1gInPc1	most
tak	tak	k9	tak
v	v	k7c6	v
podvečer	podvečer	k6eAd1	podvečer
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
sovětu	sovět	k1gInSc2	sovět
a	a	k8xC	a
vládě	vláda	k1gFnSc3	vláda
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
jejich	jejich	k3xOp3gNnSc4	jejich
zvednutí	zvednutí	k1gNnSc4	zvednutí
<g/>
.	.	kIx.	.
<g/>
VRV	VRV	kA	VRV
utvořil	utvořit	k5eAaPmAgInS	utvořit
výbor	výbor	k1gInSc1	výbor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
řídit	řídit	k5eAaImF	řídit
konfrontaci	konfrontace	k1gFnSc4	konfrontace
s	s	k7c7	s
vládou	vláda	k1gFnSc7	vláda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInPc4	jeho
rozkazy	rozkaz	k1gInPc4	rozkaz
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
nebyly	být	k5eNaImAgInP	být
nic	nic	k6eAd1	nic
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
vyslání	vyslání	k1gNnSc1	vyslání
dalších	další	k2eAgMnPc2d1	další
komisařů	komisař	k1gMnPc2	komisař
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
na	na	k7c4	na
strategická	strategický	k2eAgNnPc4d1	strategické
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
požadovali	požadovat	k5eAaImAgMnP	požadovat
jejich	jejich	k3xOp3gNnSc4	jejich
podřízení	podřízený	k1gMnPc1	podřízený
VRV	VRV	kA	VRV
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
během	během	k7c2	během
dne	den	k1gInSc2	den
byly	být	k5eAaImAgInP	být
hlavní	hlavní	k2eAgInPc1d1	hlavní
body	bod	k1gInPc1	bod
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
ovládnuty	ovládnout	k5eAaPmNgInP	ovládnout
silami	síla	k1gFnPc7	síla
loajálními	loajální	k2eAgFnPc7d1	loajální
Petrohradskému	petrohradský	k2eAgInSc3d1	petrohradský
sovětu	sovět	k1gInSc3	sovět
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
nekrvavých	krvavý	k2eNgFnPc2d1	nekrvavá
srážek	srážka	k1gFnPc2	srážka
se	s	k7c7	s
silami	síla	k1gFnPc7	síla
loajálními	loajální	k2eAgFnPc7d1	loajální
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
čtyři	čtyři	k4xCgInPc4	čtyři
odpoledne	odpoledne	k6eAd1	odpoledne
se	se	k3xPyFc4	se
cyklisté	cyklista	k1gMnPc1	cyklista
střežící	střežící	k2eAgFnSc2d1	střežící
Zimní	zimní	k2eAgInSc4d1	zimní
palác	palác	k1gInSc4	palác
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
stáhnout	stáhnout	k5eAaPmF	stáhnout
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
hodinu	hodina	k1gFnSc4	hodina
později	pozdě	k6eAd2	pozdě
zabral	zabrat	k5eAaPmAgMnS	zabrat
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
komisařů	komisař	k1gMnPc2	komisař
na	na	k7c4	na
rozkaz	rozkaz	k1gInSc4	rozkaz
VRV	VRV	kA	VRV
telegrafní	telegrafní	k2eAgFnSc4d1	telegrafní
stanici	stanice	k1gFnSc4	stanice
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
vojáků	voják	k1gMnPc2	voják
Kexholmova	Kexholmův	k2eAgInSc2d1	Kexholmův
pluku	pluk	k1gInSc2	pluk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ji	on	k3xPp3gFnSc4	on
hlídali	hlídat	k5eAaImAgMnP	hlídat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgFnPc4	tři
hodiny	hodina	k1gFnPc4	hodina
později	pozdě	k6eAd2	pozdě
dorazil	dorazit	k5eAaPmAgInS	dorazit
oddíl	oddíl	k1gInSc1	oddíl
kadetů	kadet	k1gMnPc2	kadet
zabrat	zabrat	k5eAaPmF	zabrat
budovu	budova	k1gFnSc4	budova
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vojáci	voják	k1gMnPc1	voják
ho	on	k3xPp3gMnSc4	on
nevpustili	vpustit	k5eNaPmAgMnP	vpustit
dovnitř	dovnitř	k6eAd1	dovnitř
a	a	k8xC	a
kadeti	kadet	k1gMnPc1	kadet
se	se	k3xPyFc4	se
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
si	se	k3xPyFc3	se
VRV	VRV	kA	VRV
vyžádal	vyžádat	k5eAaPmAgInS	vyžádat
příchod	příchod	k1gInSc4	příchod
námořníků	námořník	k1gMnPc2	námořník
baltské	baltský	k2eAgFnSc2d1	Baltská
floty	flota	k1gFnSc2	flota
z	z	k7c2	z
Helsinek	Helsinky	k1gFnPc2	Helsinky
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
pak	pak	k6eAd1	pak
pluli	plout	k5eAaImAgMnP	plout
do	do	k7c2	do
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
čtyřmi	čtyři	k4xCgFnPc7	čtyři
minolovkami	minolovka	k1gFnPc7	minolovka
a	a	k8xC	a
potom	potom	k6eAd1	potom
od	od	k7c2	od
tří	tři	k4xCgFnPc2	tři
hodin	hodina	k1gFnPc2	hodina
ráno	ráno	k6eAd1	ráno
příštího	příští	k2eAgInSc2d1	příští
dne	den	k1gInSc2	den
vlakem	vlak	k1gInSc7	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Nemohli	moct	k5eNaImAgMnP	moct
se	se	k3xPyFc4	se
však	však	k9	však
včas	včas	k6eAd1	včas
zapojit	zapojit	k5eAaPmF	zapojit
do	do	k7c2	do
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
vlak	vlak	k1gInSc4	vlak
zdržely	zdržet	k5eAaPmAgInP	zdržet
drážní	drážní	k2eAgInPc1d1	drážní
úřady	úřad	k1gInPc1	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
se	se	k3xPyFc4	se
členové	člen	k1gMnPc1	člen
rudých	rudý	k2eAgFnPc2d1	rudá
gard	garda	k1gFnPc2	garda
mobilizovali	mobilizovat	k5eAaBmAgMnP	mobilizovat
a	a	k8xC	a
přicházeli	přicházet	k5eAaImAgMnP	přicházet
do	do	k7c2	do
Smolného	smolný	k2eAgInSc2d1	smolný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obavách	obava	k1gFnPc6	obava
z	z	k7c2	z
reakce	reakce	k1gFnSc2	reakce
vlády	vláda	k1gFnSc2	vláda
VRV	VRV	kA	VRV
nařídil	nařídit	k5eAaPmAgMnS	nařídit
zaujmout	zaujmout	k5eAaPmF	zaujmout
ve	v	k7c6	v
městě	město	k1gNnSc6	město
nové	nový	k2eAgFnSc2d1	nová
pozice	pozice	k1gFnSc2	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
deváté	devátý	k4xOgFnSc6	devátý
večerní	večerní	k2eAgFnSc6d1	večerní
vzbouřenci	vzbouřenec	k1gMnPc7	vzbouřenec
–	–	k?	–
konkrétně	konkrétně	k6eAd1	konkrétně
Izmailovský	Izmailovský	k2eAgInSc4d1	Izmailovský
gardový	gardový	k2eAgInSc4d1	gardový
pluk	pluk	k1gInSc4	pluk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
první	první	k4xOgMnSc1	první
podpořil	podpořit	k5eAaPmAgMnS	podpořit
vládu	vláda	k1gFnSc4	vláda
–	–	k?	–
obsadili	obsadit	k5eAaPmAgMnP	obsadit
Baltské	baltský	k2eAgNnSc4d1	Baltské
nádraží	nádraží	k1gNnSc4	nádraží
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
odřízli	odříznout	k5eAaPmAgMnP	odříznout
vládu	vláda	k1gFnSc4	vláda
od	od	k7c2	od
možných	možný	k2eAgFnPc2d1	možná
posil	posila	k1gFnPc2	posila
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
21	[number]	k4	21
hodin	hodina	k1gFnPc2	hodina
také	také	k9	také
z	z	k7c2	z
telegrafní	telegrafní	k2eAgFnSc2d1	telegrafní
stanice	stanice	k1gFnSc2	stanice
vyšli	vyjít	k5eAaPmAgMnP	vyjít
obsadit	obsadit	k5eAaPmF	obsadit
blízkou	blízký	k2eAgFnSc4d1	blízká
tiskovou	tiskový	k2eAgFnSc4d1	tisková
agenturu	agentura	k1gFnSc4	agentura
<g/>
.	.	kIx.	.
</s>
<s>
Komisaři	komisar	k1gMnPc1	komisar
byli	být	k5eAaImAgMnP	být
také	také	k6eAd1	také
vysláni	vyslat	k5eAaPmNgMnP	vyslat
obsadit	obsadit	k5eAaPmF	obsadit
telefonní	telefonní	k2eAgFnSc4d1	telefonní
ústřednu	ústředna	k1gFnSc4	ústředna
<g/>
,	,	kIx,	,
elektrárnu	elektrárna	k1gFnSc4	elektrárna
a	a	k8xC	a
zbývající	zbývající	k2eAgNnSc4d1	zbývající
nádraží	nádraží	k1gNnSc4	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
hodinu	hodina	k1gFnSc4	hodina
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
kadeti	kadet	k1gMnPc1	kadet
z	z	k7c2	z
Michajlovského	Michajlovský	k2eAgNnSc2d1	Michajlovský
dělostřelecké	dělostřelecký	k2eAgFnPc4d1	dělostřelecká
školy	škola	k1gFnPc4	škola
pokusili	pokusit	k5eAaPmAgMnP	pokusit
zatknout	zatknout	k5eAaPmF	zatknout
Lenina	Lenin	k1gMnSc4	Lenin
v	v	k7c6	v
bolševické	bolševický	k2eAgFnSc6d1	bolševická
tiskárně	tiskárna	k1gFnSc6	tiskárna
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Vyborg	Vyborg	k1gInSc1	Vyborg
<g/>
;	;	kIx,	;
a	a	k8xC	a
nejen	nejen	k6eAd1	nejen
že	že	k8xS	že
tam	tam	k6eAd1	tam
Lenin	Lenin	k1gMnSc1	Lenin
nebyl	být	k5eNaImAgMnS	být
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dorazila	dorazit	k5eAaPmAgFnS	dorazit
jednotka	jednotka	k1gFnSc1	jednotka
rudé	rudý	k2eAgFnSc2d1	rudá
gardy	garda	k1gFnSc2	garda
a	a	k8xC	a
zadržela	zadržet	k5eAaPmAgFnS	zadržet
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Jednotky	jednotka	k1gFnPc4	jednotka
Petrohradského	petrohradský	k2eAgInSc2d1	petrohradský
sovětu	sovět	k1gInSc2	sovět
večer	večer	k6eAd1	večer
ovládaly	ovládat	k5eAaImAgFnP	ovládat
většinu	většina	k1gFnSc4	většina
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Opatření	opatření	k1gNnSc1	opatření
sovětu	sovět	k1gInSc2	sovět
však	však	k9	však
byla	být	k5eAaImAgFnS	být
i	i	k9	i
nadále	nadále	k6eAd1	nadále
obranná	obranný	k2eAgFnSc1d1	obranná
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
předešlo	předejít	k5eAaPmAgNnS	předejít
možnému	možný	k2eAgInSc3d1	možný
puči	puč	k1gInSc3	puč
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
zajistilo	zajistit	k5eAaPmAgNnS	zajistit
konání	konání	k1gNnSc4	konání
celoruského	celoruský	k2eAgInSc2d1	celoruský
sjezdu	sjezd	k1gInSc2	sjezd
sovětů	sovět	k1gInPc2	sovět
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
provést	provést	k5eAaPmF	provést
přenesení	přenesení	k1gNnSc4	přenesení
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
<g/>
Lenin	Lenin	k1gMnSc1	Lenin
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
skrývající	skrývající	k2eAgFnPc1d1	skrývající
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
s	s	k7c7	s
nelibostí	nelibost	k1gFnSc7	nelibost
sledoval	sledovat	k5eAaImAgInS	sledovat
běh	běh	k1gInSc1	běh
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Nechápal	chápat	k5eNaImAgMnS	chápat
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
jeho	jeho	k3xOp3gMnPc1	jeho
souvěrci	souvěrec	k1gMnPc1	souvěrec
definitivně	definitivně	k6eAd1	definitivně
neskoncují	skoncovat	k5eNaPmIp3nP	skoncovat
s	s	k7c7	s
Kerenského	Kerenský	k2eAgInSc2d1	Kerenský
vládou	vláda	k1gFnSc7	vláda
bez	bez	k7c2	bez
čekání	čekání	k1gNnSc2	čekání
na	na	k7c4	na
sjezd	sjezd	k1gInSc4	sjezd
<g/>
,	,	kIx,	,
a	a	k8xC	a
neúspěšně	úspěšně	k6eNd1	úspěšně
žádal	žádat	k5eAaImAgMnS	žádat
stranu	strana	k1gFnSc4	strana
o	o	k7c4	o
povolení	povolení	k1gNnSc4	povolení
přijít	přijít	k5eAaPmF	přijít
do	do	k7c2	do
Smolného	smolný	k2eAgMnSc2d1	smolný
<g/>
.	.	kIx.	.
</s>
<s>
Zoufalý	zoufalý	k2eAgMnSc1d1	zoufalý
z	z	k7c2	z
pasivity	pasivita	k1gFnSc2	pasivita
ústředního	ústřední	k2eAgInSc2d1	ústřední
výboru	výbor	k1gInSc2	výbor
se	se	k3xPyFc4	se
odpoledne	odpoledne	k6eAd1	odpoledne
pokusil	pokusit	k5eAaPmAgMnS	pokusit
získat	získat	k5eAaPmF	získat
podporu	podpora	k1gFnSc4	podpora
městského	městský	k2eAgInSc2d1	městský
a	a	k8xC	a
okrskového	okrskový	k2eAgInSc2d1	okrskový
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Nebyl	být	k5eNaImAgInS	být
schopen	schopen	k2eAgInSc1d1	schopen
se	se	k3xPyFc4	se
ovládnout	ovládnout	k5eAaPmF	ovládnout
a	a	k8xC	a
navzdory	navzdory	k7c3	navzdory
příkazu	příkaz	k1gInSc3	příkaz
ústředního	ústřední	k2eAgInSc2d1	ústřední
výboru	výbor	k1gInSc2	výbor
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zůstal	zůstat	k5eAaPmAgMnS	zůstat
v	v	k7c6	v
úkrytu	úkryt	k1gInSc6	úkryt
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
přestrojil	přestrojit	k5eAaPmAgMnS	přestrojit
a	a	k8xC	a
tramvají	tramvaj	k1gFnPc2	tramvaj
a	a	k8xC	a
pak	pak	k6eAd1	pak
pěšky	pěšky	k6eAd1	pěšky
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
jediného	jediný	k2eAgMnSc2d1	jediný
tělesného	tělesný	k2eAgMnSc2d1	tělesný
strážce	strážce	k1gMnSc2	strážce
do	do	k7c2	do
Smolného	smolný	k2eAgNnSc2d1	smolné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Příchod	příchod	k1gInSc1	příchod
Lenina	Lenin	k1gMnSc2	Lenin
a	a	k8xC	a
převzetí	převzetí	k1gNnSc1	převzetí
moci	moc	k1gFnSc2	moc
===	===	k?	===
</s>
</p>
<p>
<s>
Leninovi	Lenin	k1gMnSc3	Lenin
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
dorazit	dorazit	k5eAaPmF	dorazit
do	do	k7c2	do
Smolného	smolný	k2eAgMnSc2d1	smolný
kolem	kolem	k7c2	kolem
půlnoci	půlnoc	k1gFnSc2	půlnoc
na	na	k7c4	na
25	[number]	k4	25
<g/>
.	.	kIx.	.
říjnajul	říjnajout	k5eAaPmAgInS	říjnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
<g/>
;	;	kIx,	;
po	po	k7c6	po
cestě	cesta	k1gFnSc6	cesta
ho	on	k3xPp3gMnSc4	on
zastavila	zastavit	k5eAaPmAgFnS	zastavit
hlídka	hlídka	k1gFnSc1	hlídka
kadetů	kadet	k1gMnPc2	kadet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepoznala	poznat	k5eNaPmAgFnS	poznat
ho	on	k3xPp3gNnSc4	on
a	a	k8xC	a
považovala	považovat	k5eAaImAgFnS	považovat
ho	on	k3xPp3gInSc4	on
jen	jen	k6eAd1	jen
za	za	k7c4	za
opilce	opilec	k1gMnPc4	opilec
<g/>
.	.	kIx.	.
</s>
<s>
Okamžitě	okamžitě	k6eAd1	okamžitě
zintenzivnil	zintenzivnit	k5eAaPmAgMnS	zintenzivnit
protivládní	protivládní	k2eAgFnSc4d1	protivládní
činnost	činnost	k1gFnSc4	činnost
ústředního	ústřední	k2eAgInSc2d1	ústřední
výboru	výbor	k1gInSc2	výbor
<g/>
,	,	kIx,	,
přestal	přestat	k5eAaPmAgInS	přestat
uvažovat	uvažovat	k5eAaImF	uvažovat
o	o	k7c6	o
pouhé	pouhý	k2eAgFnSc6d1	pouhá
obraně	obrana	k1gFnSc6	obrana
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
zahájil	zahájit	k5eAaPmAgMnS	zahájit
kroky	krok	k1gInPc4	krok
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
nové	nový	k2eAgFnSc2d1	nová
revoluční	revoluční	k2eAgFnSc2d1	revoluční
vlády	vláda	k1gFnSc2	vláda
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
Druhého	druhý	k4xOgInSc2	druhý
všeruského	všeruský	k2eAgInSc2d1	všeruský
sjezdu	sjezd	k1gInSc2	sjezd
sovětů	sovět	k1gInPc2	sovět
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
mělo	mít	k5eAaImAgNnS	mít
proběhnout	proběhnout	k5eAaPmF	proběhnout
toho	ten	k3xDgInSc2	ten
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Zjevná	zjevný	k2eAgFnSc1d1	zjevná
slabost	slabost	k1gFnSc1	slabost
Kerenského	Kerenský	k2eAgInSc2d1	Kerenský
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
ovládnutí	ovládnutí	k1gNnSc1	ovládnutí
města	město	k1gNnSc2	město
také	také	k9	také
přispěly	přispět	k5eAaPmAgInP	přispět
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
nálady	nálada	k1gFnSc2	nálada
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
sovětu	sovět	k1gInSc2	sovět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Petrohradský	petrohradský	k2eAgInSc1d1	petrohradský
revoluční	revoluční	k2eAgInSc1d1	revoluční
vojenský	vojenský	k2eAgInSc1d1	vojenský
výbor	výbor	k1gInSc1	výbor
začal	začít	k5eAaPmAgInS	začít
připravovat	připravovat	k5eAaImF	připravovat
rozpuštění	rozpuštění	k1gNnSc4	rozpuštění
Prozatímní	prozatímní	k2eAgFnSc2d1	prozatímní
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
její	její	k3xOp3gNnSc4	její
uvěznění	uvěznění	k1gNnSc4	uvěznění
a	a	k8xC	a
ovládnutí	ovládnutí	k1gNnSc4	ovládnutí
posledních	poslední	k2eAgInPc2d1	poslední
zbývajících	zbývající	k2eAgInPc2d1	zbývající
strategických	strategický	k2eAgInPc2d1	strategický
bodů	bod	k1gInPc2	bod
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
půl	půl	k1xP	půl
druhé	druhý	k4xOgFnSc2	druhý
ráno	ráno	k6eAd1	ráno
námořníci	námořník	k1gMnPc1	námořník
<g/>
,	,	kIx,	,
vojáci	voják	k1gMnPc1	voják
Kexholmova	Kexholmův	k2eAgInSc2d1	Kexholmův
pluku	pluk	k1gInSc2	pluk
a	a	k8xC	a
rudí	rudý	k2eAgMnPc1d1	rudý
gardisté	gardista	k1gMnPc1	gardista
obsadili	obsadit	k5eAaPmAgMnP	obsadit
ústřední	ústřední	k2eAgFnSc4d1	ústřední
poštu	pošta	k1gFnSc4	pošta
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvě	dva	k4xCgNnPc1	dva
ráno	ráno	k1gNnSc1	ráno
bylo	být	k5eAaImAgNnS	být
obsazeno	obsadit	k5eAaPmNgNnS	obsadit
Nikolajevské	Nikolajevský	k2eAgNnSc1d1	Nikolajevský
nádraží	nádraží	k1gNnSc1	nádraží
a	a	k8xC	a
hlavní	hlavní	k2eAgFnSc1d1	hlavní
elektrocentrála	elektrocentrála	k1gFnSc1	elektrocentrála
<g/>
.	.	kIx.	.
</s>
<s>
Nikolajevský	Nikolajevský	k2eAgInSc1d1	Nikolajevský
most	most	k1gInSc1	most
byl	být	k5eAaImAgInS	být
dobyt	dobýt	k5eAaPmNgInS	dobýt
kolem	kolem	k7c2	kolem
3.30	[number]	k4	3.30
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
blízkosti	blízkost	k1gFnSc6	blízkost
zakotvila	zakotvit	k5eAaPmAgFnS	zakotvit
Aurora	Aurora	k1gFnSc1	Aurora
<g/>
.	.	kIx.	.
</s>
<s>
Pokus	pokus	k1gInSc1	pokus
malé	malý	k2eAgFnSc2d1	malá
vládní	vládní	k2eAgFnSc2d1	vládní
jednotky	jednotka	k1gFnSc2	jednotka
ho	on	k3xPp3gMnSc4	on
znovu	znovu	k6eAd1	znovu
ovládnout	ovládnout	k5eAaPmF	ovládnout
byl	být	k5eAaImAgInS	být
odražen	odražen	k2eAgMnSc1d1	odražen
<g/>
.	.	kIx.	.
</s>
<s>
Vojáci	voják	k1gMnPc1	voják
Kexholmova	Kexholmův	k2eAgInSc2d1	Kexholmův
pluku	pluk	k1gInSc2	pluk
dostali	dostat	k5eAaPmAgMnP	dostat
rozkaz	rozkaz	k1gInSc4	rozkaz
obsadit	obsadit	k5eAaPmF	obsadit
telefonní	telefonní	k2eAgFnSc4d1	telefonní
ústřednu	ústředna	k1gFnSc4	ústředna
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnSc4d1	státní
banku	banka	k1gFnSc4	banka
a	a	k8xC	a
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
financí	finance	k1gFnPc2	finance
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
šest	šest	k4xCc4	šest
ráno	ráno	k6eAd1	ráno
byla	být	k5eAaImAgFnS	být
obsazena	obsadit	k5eAaPmNgFnS	obsadit
státní	státní	k2eAgFnSc1d1	státní
banka	banka	k1gFnSc1	banka
a	a	k8xC	a
v	v	k7c4	v
sedm	sedm	k4xCc4	sedm
padla	padnout	k5eAaImAgFnS	padnout
telefonní	telefonní	k2eAgFnSc1d1	telefonní
ústředna	ústředna	k1gFnSc1	ústředna
po	po	k7c6	po
chvilkovém	chvilkový	k2eAgInSc6d1	chvilkový
střetu	střet	k1gInSc6	střet
s	s	k7c7	s
kadety	kadet	k1gMnPc7	kadet
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ji	on	k3xPp3gFnSc4	on
hlídali	hlídat	k5eAaImAgMnP	hlídat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
jediného	jediný	k2eAgInSc2d1	jediný
výstřelu	výstřel	k1gInSc2	výstřel
<g/>
.	.	kIx.	.
</s>
<s>
Telefony	telefon	k1gInPc1	telefon
Smolného	smolný	k2eAgNnSc2d1	smolné
byly	být	k5eAaImAgInP	být
znovu	znovu	k6eAd1	znovu
zapojeny	zapojen	k2eAgInPc1d1	zapojen
a	a	k8xC	a
telefony	telefon	k1gInPc1	telefon
Zimního	zimní	k2eAgInSc2d1	zimní
paláce	palác	k1gInSc2	palác
odpojeny	odpojen	k2eAgFnPc1d1	odpojena
<g/>
.	.	kIx.	.
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
financí	finance	k1gFnPc2	finance
bylo	být	k5eAaImAgNnS	být
pak	pak	k6eAd1	pak
převzato	převzít	k5eAaPmNgNnS	převzít
bez	bez	k7c2	bez
potíží	potíž	k1gFnPc2	potíž
<g/>
,	,	kIx,	,
poněvadž	poněvadž	k8xS	poněvadž
ho	on	k3xPp3gMnSc4	on
střežili	střežit	k5eAaImAgMnP	střežit
vojáci	voják	k1gMnPc1	voják
Pavlovského	pavlovský	k2eAgInSc2d1	pavlovský
pluku	pluk	k1gInSc2	pluk
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejvěrnějších	věrný	k2eAgMnPc2d3	nejvěrnější
sovětu	sovět	k1gInSc2	sovět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
osm	osm	k4xCc4	osm
ráno	ráno	k6eAd1	ráno
VRV	VRV	kA	VRV
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
poslední	poslední	k2eAgFnSc4d1	poslední
velkou	velký	k2eAgFnSc4d1	velká
stanici	stanice	k1gFnSc4	stanice
<g/>
,	,	kIx,	,
Varšavské	varšavský	k2eAgNnSc4d1	Varšavské
nádraží	nádraží	k1gNnSc4	nádraží
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
spojovalo	spojovat	k5eAaImAgNnS	spojovat
se	s	k7c7	s
severní	severní	k2eAgFnSc7d1	severní
frontou	fronta	k1gFnSc7	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svítání	svítání	k1gNnSc4	svítání
byla	být	k5eAaImAgFnS	být
většina	většina	k1gFnSc1	většina
města	město	k1gNnSc2	město
vyjma	vyjma	k7c2	vyjma
Zimního	zimní	k2eAgInSc2d1	zimní
paláce	palác	k1gInSc2	palác
v	v	k7c6	v
moci	moc	k1gFnSc6	moc
Petrohradského	petrohradský	k2eAgInSc2d1	petrohradský
sovětu	sovět	k1gInSc2	sovět
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
z	z	k7c2	z
obránců	obránce	k1gMnPc2	obránce
paláce	palác	k1gInSc2	palác
ani	ani	k8xC	ani
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
obléhatelů	obléhatel	k1gMnPc2	obléhatel
si	se	k3xPyFc3	se
však	však	k9	však
nepřál	přát	k5eNaImAgInS	přát
riskovat	riskovat	k5eAaBmF	riskovat
ztráty	ztráta	k1gFnPc4	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
probudilo	probudit	k5eAaPmAgNnS	probudit
s	s	k7c7	s
pozoruhodnou	pozoruhodný	k2eAgFnSc7d1	pozoruhodná
normálností	normálnost	k1gFnSc7	normálnost
<g/>
:	:	kIx,	:
úřady	úřad	k1gInPc1	úřad
<g/>
,	,	kIx,	,
školy	škola	k1gFnPc1	škola
i	i	k8xC	i
veřejná	veřejný	k2eAgFnSc1d1	veřejná
doprava	doprava	k1gFnSc1	doprava
fungovaly	fungovat	k5eAaImAgFnP	fungovat
jako	jako	k9	jako
obyčejně	obyčejně	k6eAd1	obyčejně
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
však	však	k9	však
už	už	k6eAd1	už
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
budově	budova	k1gFnSc6	budova
neměla	mít	k5eNaImAgFnS	mít
světlo	světlo	k1gNnSc4	světlo
ani	ani	k8xC	ani
telefon	telefon	k1gInSc4	telefon
<g/>
.	.	kIx.	.
<g/>
Při	při	k7c6	při
rozednění	rozednění	k1gNnSc6	rozednění
žádali	žádat	k5eAaImAgMnP	žádat
vojenští	vojenský	k2eAgMnPc1d1	vojenský
velitelé	velitel	k1gMnPc1	velitel
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
tři	tři	k4xCgInPc1	tři
městské	městský	k2eAgInPc1d1	městský
kozácké	kozácký	k2eAgInPc1d1	kozácký
pluky	pluk	k1gInPc1	pluk
<g/>
,	,	kIx,	,
ty	ten	k3xDgInPc1	ten
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
odmítly	odmítnout	k5eAaPmAgInP	odmítnout
poskytnout	poskytnout	k5eAaPmF	poskytnout
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vláda	vláda	k1gFnSc1	vláda
nemůže	moct	k5eNaImIp3nS	moct
spolehnout	spolehnout	k5eAaPmF	spolehnout
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
pěchoty	pěchota	k1gFnSc2	pěchota
<g/>
.	.	kIx.	.
</s>
<s>
Velitelé	velitel	k1gMnPc1	velitel
proto	proto	k8xC	proto
vládě	vláda	k1gFnSc6	vláda
sdělili	sdělit	k5eAaPmAgMnP	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemají	mít	k5eNaImIp3nP	mít
vojáky	voják	k1gMnPc4	voják
a	a	k8xC	a
situace	situace	k1gFnPc4	situace
je	být	k5eAaImIp3nS	být
vážná	vážný	k2eAgFnSc1d1	vážná
<g/>
;	;	kIx,	;
a	a	k8xC	a
v	v	k7c4	v
10	[number]	k4	10
hodin	hodina	k1gFnPc2	hodina
to	ten	k3xDgNnSc4	ten
vláda	vláda	k1gFnSc1	vláda
oznámila	oznámit	k5eAaPmAgFnS	oznámit
nejvyššímu	vysoký	k2eAgInSc3d3	Nejvyšší
velení	velení	k1gNnSc2	velení
armády	armáda	k1gFnSc2	armáda
přímou	přímý	k2eAgFnSc7d1	přímá
telefonní	telefonní	k2eAgFnSc7d1	telefonní
linkou	linka	k1gFnSc7	linka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
povstalci	povstalec	k1gMnPc1	povstalec
nepřerušili	přerušit	k5eNaPmAgMnP	přerušit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
11.30	[number]	k4	11.30
Kerenskij	Kerenskij	k1gFnSc6	Kerenskij
zamířil	zamířit	k5eAaPmAgMnS	zamířit
po	po	k7c6	po
silnici	silnice	k1gFnSc6	silnice
na	na	k7c4	na
frontu	fronta	k1gFnSc4	fronta
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tam	tam	k6eAd1	tam
získal	získat	k5eAaPmAgMnS	získat
vojáky	voják	k1gMnPc4	voják
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
by	by	kYmCp3nP	by
povstání	povstání	k1gNnSc4	povstání
potlačili	potlačit	k5eAaPmAgMnP	potlačit
<g/>
.	.	kIx.	.
</s>
<s>
Kadeti	kadet	k1gMnPc1	kadet
bránící	bránící	k2eAgFnSc2d1	bránící
Zimní	zimní	k2eAgInSc4d1	zimní
palác	palác	k1gInSc4	palác
požadovali	požadovat	k5eAaImAgMnP	požadovat
záruky	záruka	k1gFnPc4	záruka
<g/>
,	,	kIx,	,
že	že	k8xS	že
dostanou	dostat	k5eAaPmIp3nP	dostat
posily	posila	k1gFnPc1	posila
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
hrozili	hrozit	k5eAaImAgMnP	hrozit
odchodem	odchod	k1gInSc7	odchod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téže	tenže	k3xDgFnSc6	tenže
době	doba	k1gFnSc6	doba
bolševici	bolševik	k1gMnPc1	bolševik
vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
přenesení	přenesený	k2eAgMnPc1d1	přenesený
moci	moct	k5eAaImF	moct
na	na	k7c4	na
sověty	sovět	k1gInPc4	sovět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zatímco	zatímco	k8xS	zatímco
se	se	k3xPyFc4	se
Kerenskij	Kerenskij	k1gMnSc1	Kerenskij
snažil	snažit	k5eAaImAgMnS	snažit
sehnat	sehnat	k5eAaPmF	sehnat
auto	auto	k1gNnSc4	auto
–	–	k?	–
vlaky	vlak	k1gInPc1	vlak
už	už	k6eAd1	už
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
moci	moc	k1gFnSc6	moc
Petrohradského	petrohradský	k2eAgInSc2d1	petrohradský
sovětu	sovět	k1gInSc2	sovět
a	a	k8xC	a
palácové	palácový	k2eAgInPc4d1	palácový
automobily	automobil	k1gInPc4	automobil
poškodili	poškodit	k5eAaPmAgMnP	poškodit
sabotéři	sabotér	k1gMnPc1	sabotér
–	–	k?	–
Lenin	Lenin	k1gMnSc1	Lenin
napsal	napsat	k5eAaPmAgMnS	napsat
prohlášení	prohlášení	k1gNnSc4	prohlášení
o	o	k7c4	o
sesazení	sesazení	k1gNnSc4	sesazení
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
hned	hned	k6eAd1	hned
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
po	po	k7c6	po
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Občanům	občan	k1gMnPc3	občan
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
Prozatímní	prozatímní	k2eAgFnSc1d1	prozatímní
vláda	vláda	k1gFnSc1	vláda
byly	být	k5eAaImAgFnP	být
sesazena	sesazen	k2eAgMnSc4d1	sesazen
<g/>
.	.	kIx.	.
</s>
<s>
Vládní	vládní	k2eAgFnSc1d1	vládní
moc	moc	k1gFnSc1	moc
byla	být	k5eAaImAgFnS	být
předána	předat	k5eAaPmNgFnS	předat
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
orgánu	orgán	k1gInSc2	orgán
petrohradského	petrohradský	k2eAgInSc2d1	petrohradský
sovětu	sovět	k1gInSc2	sovět
zástupců	zástupce	k1gMnPc2	zástupce
dělníků	dělník	k1gMnPc2	dělník
a	a	k8xC	a
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
vojenského	vojenský	k2eAgInSc2d1	vojenský
revolučního	revoluční	k2eAgInSc2d1	revoluční
výboru	výbor	k1gInSc2	výbor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
petrohradského	petrohradský	k2eAgInSc2d1	petrohradský
proletariátu	proletariát	k1gInSc2	proletariát
a	a	k8xC	a
posádky	posádka	k1gFnSc2	posádka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cíl	cíl	k1gInSc1	cíl
<g/>
,	,	kIx,	,
o	o	k7c4	o
který	který	k3yQgInSc4	který
lid	lid	k1gInSc4	lid
usiloval	usilovat	k5eAaImAgInS	usilovat
–	–	k?	–
okamžité	okamžitý	k2eAgNnSc4d1	okamžité
uzavření	uzavření	k1gNnSc4	uzavření
demokratického	demokratický	k2eAgInSc2d1	demokratický
míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
zrušení	zrušení	k1gNnSc4	zrušení
soukromého	soukromý	k2eAgNnSc2d1	soukromé
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
pozemků	pozemek	k1gInPc2	pozemek
<g/>
,	,	kIx,	,
řízení	řízení	k1gNnSc2	řízení
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
výroby	výroba	k1gFnSc2	výroba
dělníky	dělník	k1gMnPc4	dělník
<g/>
,	,	kIx,	,
vytvoření	vytvoření	k1gNnSc4	vytvoření
sovětské	sovětský	k2eAgFnSc2d1	sovětská
vlády	vláda	k1gFnSc2	vláda
–	–	k?	–
tohoto	tento	k3xDgInSc2	tento
cíle	cíl	k1gInSc2	cíl
bylo	být	k5eAaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ať	ať	k9	ať
žije	žít	k5eAaImIp3nS	žít
revoluce	revoluce	k1gFnSc1	revoluce
dělníků	dělník	k1gMnPc2	dělník
<g/>
,	,	kIx,	,
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
rolníků	rolník	k1gMnPc2	rolník
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
členů	člen	k1gMnPc2	člen
eserského	eserský	k2eAgInSc2d1	eserský
ústředního	ústřední	k2eAgInSc2d1	ústřední
výboru	výbor	k1gInSc2	výbor
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
údajně	údajně	k6eAd1	údajně
spřízněné	spřízněný	k2eAgFnPc4d1	spřízněná
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
bránily	bránit	k5eAaImAgFnP	bránit
Kerenského	Kerenský	k2eAgInSc2d1	Kerenský
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
eserů	eser	k1gMnPc2	eser
na	na	k7c4	na
vojsko	vojsko	k1gNnSc4	vojsko
mizel	mizet	k5eAaImAgMnS	mizet
a	a	k8xC	a
už	už	k6eAd1	už
nestačil	stačit	k5eNaBmAgInS	stačit
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
petrohradské	petrohradský	k2eAgInPc1d1	petrohradský
vojáky	voják	k1gMnPc4	voják
přiměl	přimět	k5eAaPmAgInS	přimět
podpořit	podpořit	k5eAaPmF	podpořit
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Stranická	stranický	k2eAgFnSc1d1	stranická
základna	základna	k1gFnSc1	základna
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
tvořená	tvořený	k2eAgFnSc1d1	tvořená
většinou	většinou	k6eAd1	většinou
dělníky	dělník	k1gMnPc4	dělník
a	a	k8xC	a
vojáky	voják	k1gMnPc4	voják
<g/>
,	,	kIx,	,
nechtěla	chtít	k5eNaImAgFnS	chtít
bránit	bránit	k5eAaImF	bránit
vládní	vládní	k2eAgFnSc3d1	vládní
koalici	koalice	k1gFnSc3	koalice
obsahující	obsahující	k2eAgFnSc2d1	obsahující
buržoazní	buržoazní	k2eAgFnSc2d1	buržoazní
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
dne	den	k1gInSc2	den
VRV	VRV	kA	VRV
přebíral	přebírat	k5eAaImAgMnS	přebírat
poslední	poslední	k2eAgInPc4d1	poslední
významné	významný	k2eAgInPc4d1	významný
objekty	objekt	k1gInPc4	objekt
stále	stále	k6eAd1	stále
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
vlády	vláda	k1gFnSc2	vláda
<g/>
:	:	kIx,	:
věznici	věznice	k1gFnSc4	věznice
Kresty	Kresta	k1gFnSc2	Kresta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
osvobodili	osvobodit	k5eAaPmAgMnP	osvobodit
šest	šest	k4xCc4	šest
zadržených	zadržený	k2eAgInPc2d1	zadržený
agitátorů	agitátor	k1gInPc2	agitátor
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
obklíčen	obklíčen	k2eAgInSc1d1	obklíčen
Mariinský	Mariinský	k2eAgInSc1d1	Mariinský
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
sídlo	sídlo	k1gNnSc1	sídlo
předparlamentu	předparlament	k1gInSc2	předparlament
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
předparlament	předparlament	k1gMnSc1	předparlament
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
formálním	formální	k2eAgInSc6d1	formální
protestu	protest	k1gInSc6	protest
rozpuštěn	rozpuštěn	k2eAgMnSc1d1	rozpuštěn
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdo	nikdo	k3yNnSc1	nikdo
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
členů	člen	k1gMnPc2	člen
nebyl	být	k5eNaImAgMnS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgInPc1d1	státní
úřady	úřad	k1gInPc1	úřad
a	a	k8xC	a
školy	škola	k1gFnPc1	škola
se	se	k3xPyFc4	se
zavíraly	zavírat	k5eAaImAgFnP	zavírat
<g/>
.	.	kIx.	.
<g/>
Odpoledne	odpoledne	k6eAd1	odpoledne
Trockij	Trockij	k1gFnSc1	Trockij
na	na	k7c6	na
zasedání	zasedání	k1gNnSc6	zasedání
Petrohradského	petrohradský	k2eAgInSc2d1	petrohradský
sovětu	sovět	k1gInSc2	sovět
oznámil	oznámit	k5eAaPmAgInS	oznámit
pád	pád	k1gInSc1	pád
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
opatření	opatření	k1gNnSc4	opatření
k	k	k7c3	k
převzetí	převzetí	k1gNnSc3	převzetí
moci	moc	k1gFnSc2	moc
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
červencových	červencový	k2eAgInPc2d1	červencový
dní	den	k1gInPc2	den
před	před	k7c7	před
sovětem	sovět	k1gInSc7	sovět
objevil	objevit	k5eAaPmAgMnS	objevit
Lenin	Lenin	k1gMnSc1	Lenin
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
přivítán	přivítat	k5eAaPmNgInS	přivítat
ovacemi	ovace	k1gFnPc7	ovace
vstoje	vstoje	k6eAd1	vstoje
<g/>
.	.	kIx.	.
</s>
<s>
Sovět	Sovět	k1gMnSc1	Sovět
odsouhlasil	odsouhlasit	k5eAaPmAgMnS	odsouhlasit
převzetí	převzetí	k1gNnSc4	převzetí
moci	moct	k5eAaImF	moct
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
Zimní	zimní	k2eAgInSc1d1	zimní
palác	palác	k1gInSc1	palác
ještě	ještě	k6eAd1	ještě
nepadl	padnout	k5eNaPmAgInS	padnout
<g/>
.	.	kIx.	.
</s>
<s>
Ráno	ráno	k6eAd1	ráno
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
tříčlenný	tříčlenný	k2eAgInSc1d1	tříčlenný
výbor	výbor	k1gInSc1	výbor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
řídit	řídit	k5eAaImF	řídit
jeho	jeho	k3xOp3gNnSc1	jeho
obléhání	obléhání	k1gNnSc1	obléhání
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc4	ten
si	se	k3xPyFc3	se
Lenin	Lenin	k1gMnSc1	Lenin
stále	stále	k6eAd1	stále
přál	přát	k5eAaImAgMnS	přát
ukončit	ukončit	k5eAaPmF	ukončit
ještě	ještě	k9	ještě
před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
sjezdu	sjezd	k1gInSc2	sjezd
sovětů	sovět	k1gInPc2	sovět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dobytí	dobytí	k1gNnSc6	dobytí
Zimního	zimní	k2eAgInSc2d1	zimní
paláce	palác	k1gInSc2	palác
===	===	k?	===
</s>
</p>
<p>
<s>
Plán	plán	k1gInSc1	plán
Petrohradského	petrohradský	k2eAgInSc2d1	petrohradský
revolučního	revoluční	k2eAgInSc2d1	revoluční
vojenského	vojenský	k2eAgInSc2d1	vojenský
výboru	výbor	k1gInSc2	výbor
(	(	kIx(	(
<g/>
VRV	VRV	kA	VRV
<g/>
)	)	kIx)	)
na	na	k7c4	na
dobytí	dobytí	k1gNnSc4	dobytí
Zimního	zimní	k2eAgInSc2d1	zimní
paláce	palác	k1gInSc2	palác
zahrnoval	zahrnovat	k5eAaImAgMnS	zahrnovat
vytvoření	vytvoření	k1gNnSc4	vytvoření
dvou	dva	k4xCgInPc2	dva
soustředných	soustředný	k2eAgInPc2d1	soustředný
kruhů	kruh	k1gInPc2	kruh
jednotek	jednotka	k1gFnPc2	jednotka
kolem	kolem	k7c2	kolem
budovy	budova	k1gFnSc2	budova
<g/>
;	;	kIx,	;
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
(	(	kIx(	(
<g/>
složený	složený	k2eAgInSc4d1	složený
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
vojáků	voják	k1gMnPc2	voják
Pavlovského	pavlovský	k2eAgInSc2d1	pavlovský
pluku	pluk	k1gInSc2	pluk
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
Kexholmova	Kexholmův	k2eAgInSc2d1	Kexholmův
pluku	pluk	k1gInSc2	pluk
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
)	)	kIx)	)
měl	mít	k5eAaImAgMnS	mít
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
tlak	tlak	k1gInSc4	tlak
na	na	k7c4	na
obležené	obležený	k2eAgNnSc4d1	obležené
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
vnější	vnější	k2eAgFnSc1d1	vnější
mě	já	k3xPp1nSc2	já
odrazit	odrazit	k5eAaPmF	odrazit
případné	případný	k2eAgInPc4d1	případný
útoky	útok	k1gInPc4	útok
kadetů	kadet	k1gMnPc2	kadet
nebo	nebo	k8xC	nebo
kozáků	kozák	k1gMnPc2	kozák
<g/>
;	;	kIx,	;
obležení	obležení	k1gNnSc1	obležení
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
Petropavlovskou	petropavlovský	k2eAgFnSc4d1	Petropavlovská
pevnost	pevnost	k1gFnSc4	pevnost
a	a	k8xC	a
křižník	křižník	k1gInSc4	křižník
Aurora	Aurora	k1gFnSc1	Aurora
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
nastavena	nastavit	k5eAaPmNgFnS	nastavit
tak	tak	k6eAd1	tak
komplikovaná	komplikovaný	k2eAgFnSc1d1	komplikovaná
struktura	struktura	k1gFnSc1	struktura
velení	velení	k1gNnSc2	velení
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
pak	pak	k6eAd1	pak
působilo	působit	k5eAaImAgNnS	působit
zdržení	zdržení	k1gNnSc1	zdržení
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
i	i	k8xC	i
Trockij	Trockij	k1gMnSc1	Trockij
<g/>
.	.	kIx.	.
</s>
<s>
Antonov-Ovsejenko	Antonov-Ovsejenka	k1gFnSc5	Antonov-Ovsejenka
<g/>
,	,	kIx,	,
bolševický	bolševický	k2eAgInSc1d1	bolševický
člen	člen	k1gInSc1	člen
VRV	VRV	kA	VRV
<g/>
,	,	kIx,	,
sdělil	sdělit	k5eAaPmAgInS	sdělit
komisařům	komisař	k1gMnPc3	komisař
jednotek	jednotka	k1gFnPc2	jednotka
následující	následující	k2eAgInSc1d1	následující
plán	plán	k1gInSc1	plán
<g/>
:	:	kIx,	:
palác	palác	k1gInSc1	palác
dostane	dostat	k5eAaPmIp3nS	dostat
ultimátum	ultimátum	k1gNnSc4	ultimátum
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
ho	on	k3xPp3gMnSc4	on
včas	včas	k6eAd1	včas
nepřijme	přijmout	k5eNaPmIp3nS	přijmout
<g/>
,	,	kIx,	,
pevnost	pevnost	k1gFnSc4	pevnost
červeným	červený	k2eAgNnSc7d1	červené
světlem	světlo	k1gNnSc7	světlo
oznámí	oznámit	k5eAaPmIp3nS	oznámit
začátek	začátek	k1gInSc4	začátek
ostřelování	ostřelování	k1gNnSc2	ostřelování
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
výstřely	výstřel	k1gInPc1	výstřel
půjdou	jít	k5eAaImIp3nP	jít
z	z	k7c2	z
Aurory	Aurora	k1gFnSc2	Aurora
a	a	k8xC	a
budou	být	k5eAaImBp3nP	být
slepé	slepý	k2eAgInPc1d1	slepý
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
bude	být	k5eAaImBp3nS	být
přesto	přesto	k6eAd1	přesto
odpor	odpor	k1gInSc1	odpor
pokračovat	pokračovat	k5eAaImF	pokračovat
<g/>
,	,	kIx,	,
křižník	křižník	k1gInSc1	křižník
i	i	k8xC	i
pevnost	pevnost	k1gFnSc1	pevnost
zahájí	zahájit	k5eAaPmIp3nS	zahájit
ostrou	ostrý	k2eAgFnSc4d1	ostrá
palbu	palba	k1gFnSc4	palba
<g/>
.	.	kIx.	.
<g/>
Kolem	kolem	k7c2	kolem
13	[number]	k4	13
<g/>
.	.	kIx.	.
hodiny	hodina	k1gFnPc4	hodina
skupina	skupina	k1gFnSc1	skupina
námořníků	námořník	k1gMnPc2	námořník
obsadila	obsadit	k5eAaPmAgFnS	obsadit
Admiralitu	admiralita	k1gFnSc4	admiralita
a	a	k8xC	a
zatkla	zatknout	k5eAaPmAgFnS	zatknout
vrchní	vrchní	k1gFnPc4	vrchní
velení	velení	k1gNnSc2	velení
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgNnSc1	první
zdržení	zdržení	k1gNnSc1	zdržení
plánu	plán	k1gInSc2	plán
nastalo	nastat	k5eAaPmAgNnS	nastat
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
ve	v	k7c4	v
14	[number]	k4	14
hodin	hodina	k1gFnPc2	hodina
nedostavili	dostavit	k5eNaPmAgMnP	dostavit
helsinští	helsinský	k2eAgMnPc1d1	helsinský
námořníci	námořník	k1gMnPc1	námořník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
posílit	posílit	k5eAaPmF	posílit
vojáky	voják	k1gMnPc4	voják
<g/>
.	.	kIx.	.
</s>
<s>
Kronštat	Kronštat	k5eAaImF	Kronštat
právě	právě	k6eAd1	právě
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
města	město	k1gNnSc2	město
s	s	k7c7	s
improvizovanou	improvizovaný	k2eAgFnSc7d1	improvizovaná
flotilou	flotila	k1gFnSc7	flotila
různých	různý	k2eAgFnPc2d1	různá
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Zastaralý	zastaralý	k2eAgInSc1d1	zastaralý
Amur	Amur	k1gInSc1	Amur
zakotvil	zakotvit	k5eAaPmAgInS	zakotvit
u	u	k7c2	u
Aurory	Aurora	k1gFnSc2	Aurora
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ostatní	ostatní	k2eAgFnPc1d1	ostatní
lodě	loď	k1gFnPc1	loď
se	se	k3xPyFc4	se
přiblížily	přiblížit	k5eAaPmAgFnP	přiblížit
k	k	k7c3	k
Admiralitě	admiralita	k1gFnSc3	admiralita
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
tři	tři	k4xCgInPc1	tři
tisíce	tisíc	k4xCgInPc1	tisíc
námořníků	námořník	k1gMnPc2	námořník
z	z	k7c2	z
Kronštatu	Kronštat	k1gInSc2	Kronštat
se	se	k3xPyFc4	se
vylodilo	vylodit	k5eAaPmAgNnS	vylodit
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
obléhání	obléhání	k1gNnSc1	obléhání
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
paláci	palác	k1gInSc6	palác
se	se	k3xPyFc4	se
zatím	zatím	k6eAd1	zatím
sešel	sejít	k5eAaPmAgInS	sejít
kabinet	kabinet	k1gInSc1	kabinet
<g/>
,	,	kIx,	,
jejž	jenž	k3xRgInSc4	jenž
za	za	k7c2	za
nepřítomnosti	nepřítomnost	k1gFnSc2	nepřítomnost
Kerenského	Kerenský	k2eAgInSc2d1	Kerenský
vedl	vést	k5eAaImAgMnS	vést
místopředseda	místopředseda	k1gMnSc1	místopředseda
vlády	vláda	k1gFnSc2	vláda
Konovalov	Konovalov	k1gInSc1	Konovalov
<g/>
.	.	kIx.	.
</s>
<s>
Odvolal	odvolat	k5eAaPmAgInS	odvolat
Polkovnikova	Polkovnikův	k2eAgNnPc4d1	Polkovnikův
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
velení	velení	k1gNnSc1	velení
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
nedostatečné	dostatečný	k2eNgNnSc1d1	nedostatečné
<g/>
,	,	kIx,	,
a	a	k8xC	a
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
konzervativního	konzervativní	k2eAgMnSc4d1	konzervativní
ministra	ministr	k1gMnSc2	ministr
sociálních	sociální	k2eAgFnPc2d1	sociální
věcí	věc	k1gFnPc2	věc
Kiškina	Kiškina	k1gMnSc1	Kiškina
hlavním	hlavní	k2eAgMnSc7d1	hlavní
velitelem	velitel	k1gMnSc7	velitel
obrany	obrana	k1gFnSc2	obrana
s	s	k7c7	s
diktátorskými	diktátorský	k2eAgFnPc7d1	diktátorská
pravomocemi	pravomoc	k1gFnPc7	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
zasedání	zasedání	k1gNnSc6	zasedání
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nedojde	dojít	k5eNaPmIp3nS	dojít
k	k	k7c3	k
záchraně	záchrana	k1gFnSc3	záchrana
jednotkami	jednotka	k1gFnPc7	jednotka
přivolanými	přivolaný	k2eAgFnPc7d1	přivolaná
z	z	k7c2	z
fronty	fronta	k1gFnSc2	fronta
anebo	anebo	k8xC	anebo
k	k	k7c3	k
zajetí	zajetí	k1gNnSc6	zajetí
povstalci	povstalec	k1gMnPc1	povstalec
<g/>
.	.	kIx.	.
</s>
<s>
Skutečné	skutečný	k2eAgNnSc1d1	skutečné
velení	velení	k1gNnSc1	velení
nad	nad	k7c7	nad
jednotkami	jednotka	k1gFnPc7	jednotka
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
měl	mít	k5eAaImAgMnS	mít
poslanec	poslanec	k1gMnSc1	poslanec
P.	P.	kA	P.
I.	I.	kA	I.
Palčinskij	Palčinskij	k1gMnSc1	Palčinskij
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
nezáviděníhodné	nezáviděníhodný	k2eAgFnSc6d1	nezáviděníhodná
situaci	situace	k1gFnSc6	situace
<g/>
:	:	kIx,	:
neměl	mít	k5eNaImAgMnS	mít
potraviny	potravina	k1gFnPc4	potravina
ani	ani	k8xC	ani
plán	plán	k1gInSc4	plán
obrany	obrana	k1gFnSc2	obrana
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
obklopen	obklopit	k5eAaPmNgInS	obklopit
zmatenými	zmatený	k2eAgFnPc7d1	zmatená
funkcionáři	funkcionář	k1gMnPc1	funkcionář
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
vojáci	voják	k1gMnPc1	voják
byli	být	k5eAaImAgMnP	být
demoralizovaní	demoralizovaný	k2eAgMnPc1d1	demoralizovaný
<g/>
.	.	kIx.	.
</s>
<s>
Kiškin	Kiškin	k1gMnSc1	Kiškin
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
sousedního	sousední	k2eAgNnSc2d1	sousední
sídla	sídlo	k1gNnSc2	sídlo
generálního	generální	k2eAgInSc2d1	generální
štábu	štáb	k1gInSc2	štáb
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
odvolal	odvolat	k5eAaPmAgInS	odvolat
Polkovnikova	Polkovnikův	k2eAgMnSc4d1	Polkovnikův
a	a	k8xC	a
místo	místo	k7c2	místo
něho	on	k3xPp3gMnSc2	on
dosadil	dosadit	k5eAaPmAgMnS	dosadit
náčelníka	náčelník	k1gMnSc2	náčelník
štábu	štáb	k1gInSc2	štáb
vojenské	vojenský	k2eAgFnSc2d1	vojenská
oblasti	oblast	k1gFnSc2	oblast
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
generála	generál	k1gMnSc2	generál
Bagratuniho	Bagratuni	k1gMnSc2	Bagratuni
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
zmatek	zmatek	k1gInSc1	zmatek
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
ještě	ještě	k6eAd1	ještě
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
<g/>
.	.	kIx.	.
<g/>
Venku	venku	k6eAd1	venku
probíhaly	probíhat	k5eAaImAgInP	probíhat
jen	jen	k9	jen
chabé	chabý	k2eAgInPc1d1	chabý
pokusy	pokus	k1gInPc1	pokus
vládních	vládní	k2eAgFnPc2d1	vládní
sil	síla	k1gFnPc2	síla
odebrat	odebrat	k5eAaPmF	odebrat
důležité	důležitý	k2eAgFnPc4d1	důležitá
budovy	budova	k1gFnPc4	budova
sovětu	sovět	k1gInSc2	sovět
<g/>
:	:	kIx,	:
vrchní	vrchní	k2eAgMnSc1d1	vrchní
velitel	velitel	k1gMnSc1	velitel
armády	armáda	k1gFnSc2	armáda
Stankevič	Stankevič	k1gMnSc1	Stankevič
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
kadetů	kadet	k1gMnPc2	kadet
znovu	znovu	k6eAd1	znovu
obsadit	obsadit	k5eAaPmF	obsadit
telefonní	telefonní	k2eAgFnSc4d1	telefonní
ústřednu	ústředna	k1gFnSc4	ústředna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
krátkém	krátký	k2eAgInSc6d1	krátký
boji	boj	k1gInSc6	boj
byl	být	k5eAaImAgInS	být
odražen	odrazit	k5eAaPmNgInS	odrazit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
generálního	generální	k2eAgInSc2d1	generální
štábu	štáb	k1gInSc2	štáb
na	na	k7c6	na
Palácovém	palácový	k2eAgNnSc6d1	palácové
náměstí	náměstí	k1gNnSc6	náměstí
informoval	informovat	k5eAaBmAgMnS	informovat
vrchní	vrchní	k2eAgMnSc1d1	vrchní
velení	velení	k1gNnSc4	velení
o	o	k7c6	o
naléhavé	naléhavý	k2eAgFnSc6d1	naléhavá
potřebě	potřeba	k1gFnSc6	potřeba
přislíbených	přislíbený	k2eAgFnPc2d1	přislíbená
posil	posila	k1gFnPc2	posila
a	a	k8xC	a
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
chabé	chabý	k2eAgFnPc1d1	chabá
jednotky	jednotka	k1gFnPc1	jednotka
stále	stále	k6eAd1	stále
loajální	loajální	k2eAgInPc4d1	loajální
vůči	vůči	k7c3	vůči
vládě	vláda	k1gFnSc3	vláda
nebudou	být	k5eNaImBp3nP	být
moci	moct	k5eAaImF	moct
vzdorovat	vzdorovat	k5eAaImF	vzdorovat
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
odpoledne	odpoledne	k1gNnSc2	odpoledne
přicházely	přicházet	k5eAaImAgFnP	přicházet
obléhatelům	obléhatel	k1gMnPc3	obléhatel
posily	posila	k1gFnSc2	posila
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
někteří	některý	k3yIgMnPc1	některý
obránci	obránce	k1gMnPc1	obránce
odcházeli	odcházet	k5eAaImAgMnP	odcházet
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nP	by
jim	on	k3xPp3gMnPc3	on
lidé	člověk	k1gMnPc1	člověk
kolem	kolem	k7c2	kolem
paláce	palác	k1gInSc2	palác
bránili	bránit	k5eAaImAgMnP	bránit
<g/>
.	.	kIx.	.
</s>
<s>
Několika	několik	k4yIc3	několik
jednotkám	jednotka	k1gFnPc3	jednotka
kozáků	kozák	k1gInPc2	kozák
a	a	k8xC	a
skupině	skupina	k1gFnSc3	skupina
kadetů	kadet	k1gMnPc2	kadet
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
paláce	palác	k1gInSc2	palác
také	také	k9	také
bez	bez	k7c2	bez
boje	boj	k1gInSc2	boj
s	s	k7c7	s
obléhateli	obléhatel	k1gMnPc7	obléhatel
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
opět	opět	k6eAd1	opět
posílilo	posílit	k5eAaPmAgNnS	posílit
obranu	obrana	k1gFnSc4	obrana
<g/>
.	.	kIx.	.
<g/>
Lenin	Lenin	k1gMnSc1	Lenin
zatím	zatím	k6eAd1	zatím
naléhal	naléhat	k5eAaImAgMnS	naléhat
na	na	k7c4	na
velitele	velitel	k1gMnPc4	velitel
obléhání	obléhání	k1gNnSc2	obléhání
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ho	on	k3xPp3gMnSc4	on
ukončili	ukončit	k5eAaPmAgMnP	ukončit
co	co	k9	co
nejdřív	dříve	k6eAd3	dříve
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
dalšímu	další	k2eAgInSc3d1	další
odkladu	odklad	k1gInSc3	odklad
zahájení	zahájení	k1gNnSc2	zahájení
sjezdu	sjezd	k1gInSc2	sjezd
a	a	k8xC	a
nervozitě	nervozita	k1gFnSc6	nervozita
delegátů	delegát	k1gMnPc2	delegát
<g/>
.	.	kIx.	.
</s>
<s>
Velitel	velitel	k1gMnSc1	velitel
pevnosti	pevnost	k1gFnSc2	pevnost
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
konečně	konečně	k6eAd1	konečně
odhodlal	odhodlat	k5eAaPmAgMnS	odhodlat
odeslat	odeslat	k5eAaPmF	odeslat
do	do	k7c2	do
generálního	generální	k2eAgInSc2d1	generální
štábu	štáb	k1gInSc2	štáb
dva	dva	k4xCgInPc1	dva
cyklisty	cyklista	k1gMnSc2	cyklista
s	s	k7c7	s
ultimátem	ultimátum	k1gNnSc7	ultimátum
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
datováno	datovat	k5eAaImNgNnS	datovat
k	k	k7c3	k
18	[number]	k4	18
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
vypršet	vypršet	k5eAaPmF	vypršet
v	v	k7c6	v
19.10	[number]	k4	19.10
a	a	k8xC	a
požadovalo	požadovat	k5eAaImAgNnS	požadovat
kapitulaci	kapitulace	k1gFnSc4	kapitulace
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
jejích	její	k3xOp3gFnPc2	její
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Ministři	ministr	k1gMnPc1	ministr
opustili	opustit	k5eAaPmAgMnP	opustit
večeři	večeře	k1gFnSc4	večeře
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
spěšně	spěšně	k6eAd1	spěšně
sešli	sejít	k5eAaPmAgMnP	sejít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
na	na	k7c4	na
ultimátum	ultimátum	k1gNnSc4	ultimátum
neodpovídat	odpovídat	k5eNaImF	odpovídat
a	a	k8xC	a
nevzdávat	vzdávat	k5eNaImF	vzdávat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vypršení	vypršení	k1gNnSc6	vypršení
lhůty	lhůta	k1gFnSc2	lhůta
vojáci	voják	k1gMnPc1	voják
Pavlovského	pavlovský	k2eAgInSc2d1	pavlovský
regimentu	regiment	k1gInSc2	regiment
obsadili	obsadit	k5eAaPmAgMnP	obsadit
generální	generální	k2eAgInSc4d1	generální
štáb	štáb	k1gInSc4	štáb
přes	přes	k7c4	přes
pokusy	pokus	k1gInPc4	pokus
kadetů	kadet	k1gMnPc2	kadet
z	z	k7c2	z
paláce	palác	k1gInSc2	palác
pomoci	pomoct	k5eAaPmF	pomoct
jeho	jeho	k3xOp3gMnPc3	jeho
obráncům	obránce	k1gMnPc3	obránce
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
velitel	velitel	k1gMnSc1	velitel
severní	severní	k2eAgFnSc2d1	severní
fronty	fronta	k1gFnSc2	fronta
generál	generál	k1gMnSc1	generál
Čeremisov	Čeremisov	k1gInSc4	Čeremisov
telefonoval	telefonovat	k5eAaImAgMnS	telefonovat
do	do	k7c2	do
štábu	štáb	k1gInSc2	štáb
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zjistil	zjistit	k5eAaPmAgMnS	zjistit
situaci	situace	k1gFnSc4	situace
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spojení	spojení	k1gNnSc1	spojení
bylo	být	k5eAaImAgNnS	být
přerušeno	přerušit	k5eAaPmNgNnS	přerušit
obléhateli	obléhatel	k1gMnPc7	obléhatel
<g/>
.	.	kIx.	.
</s>
<s>
Bagratuni	Bagratuň	k1gMnSc3	Bagratuň
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jednal	jednat	k5eAaImAgMnS	jednat
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
s	s	k7c7	s
ministry	ministr	k1gMnPc7	ministr
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
odstoupit	odstoupit	k5eAaPmF	odstoupit
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
zajat	zajmout	k5eAaPmNgMnS	zajmout
povstalci	povstalec	k1gMnPc7	povstalec
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c4	mezi
osmou	osmý	k4xOgFnSc4	osmý
a	a	k8xC	a
devátou	devátý	k4xOgFnSc4	devátý
večerní	večerní	k2eAgFnSc1d1	večerní
si	se	k3xPyFc3	se
některé	některý	k3yIgFnSc2	některý
z	z	k7c2	z
jednotek	jednotka	k1gFnPc2	jednotka
bránících	bránící	k2eAgFnPc2d1	bránící
palác	palác	k1gInSc4	palác
vyjednaly	vyjednat	k5eAaPmAgInP	vyjednat
s	s	k7c7	s
obléhateli	obléhatel	k1gMnPc7	obléhatel
odchod	odchod	k1gInSc4	odchod
<g/>
.	.	kIx.	.
</s>
<s>
Důstojníci	důstojník	k1gMnPc1	důstojník
jim	on	k3xPp3gMnPc3	on
nebránili	bránit	k5eNaImAgMnP	bránit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nechtěli	chtít	k5eNaImAgMnP	chtít
dále	daleko	k6eAd2	daleko
snižovat	snižovat	k5eAaImF	snižovat
morálku	morálka	k1gFnSc4	morálka
obránců	obránce	k1gMnPc2	obránce
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
je	on	k3xPp3gInPc4	on
drželi	držet	k5eAaImAgMnP	držet
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
<g/>
.	.	kIx.	.
</s>
<s>
Odešla	odejít	k5eAaPmAgFnS	odejít
jednotka	jednotka	k1gFnSc1	jednotka
útočných	útočný	k2eAgFnPc2d1	útočná
sil	síla	k1gFnPc2	síla
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
kozáci	kozák	k1gMnPc1	kozák
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
obléhatele	obléhatel	k1gMnSc2	obléhatel
posílily	posílit	k5eAaPmAgFnP	posílit
nově	nově	k6eAd1	nově
příchozí	příchozí	k1gFnPc4	příchozí
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
námořníci	námořník	k1gMnPc1	námořník
z	z	k7c2	z
Finska	Finsko	k1gNnSc2	Finsko
a	a	k8xC	a
Kronštatu	Kronštat	k1gInSc2	Kronštat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
pokusech	pokus	k1gInPc6	pokus
o	o	k7c6	o
vyjednávání	vyjednávání	k1gNnSc6	vyjednávání
mezi	mezi	k7c7	mezi
vedením	vedení	k1gNnSc7	vedení
obléhatelů	obléhatel	k1gMnPc2	obléhatel
a	a	k8xC	a
jednotkami	jednotka	k1gFnPc7	jednotka
obránců	obránce	k1gMnPc2	obránce
palác	palác	k1gInSc1	palác
ve	v	k7c6	v
22	[number]	k4	22
hodin	hodina	k1gFnPc2	hodina
opustila	opustit	k5eAaPmAgFnS	opustit
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
polovina	polovina	k1gFnSc1	polovina
kadetů	kadet	k1gMnPc2	kadet
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
doposud	doposud	k6eAd1	doposud
zůstávali	zůstávat	k5eAaImAgMnP	zůstávat
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
určitých	určitý	k2eAgInPc6d1	určitý
zmatcích	zmatek	k1gInPc6	zmatek
<g/>
,	,	kIx,	,
způsobených	způsobený	k2eAgFnPc2d1	způsobená
šířením	šíření	k1gNnSc7	šíření
nepravdivých	pravdivý	k2eNgFnPc2d1	nepravdivá
zpráv	zpráva	k1gFnPc2	zpráva
o	o	k7c4	o
kapitulaci	kapitulace	k1gFnSc4	kapitulace
paláce	palác	k1gInSc2	palác
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
dobývání	dobývání	k1gNnSc1	dobývání
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
pevnosti	pevnost	k1gFnSc6	pevnost
konečně	konečně	k6eAd1	konečně
našli	najít	k5eAaPmAgMnP	najít
lucernu	lucerna	k1gFnSc4	lucerna
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
k	k	k7c3	k
signalizaci	signalizace	k1gFnSc3	signalizace
Auroře	Aurora	k1gFnSc3	Aurora
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
ve	v	k7c6	v
21	[number]	k4	21
<g/>
:	:	kIx,	:
<g/>
35	[number]	k4	35
a	a	k8xC	a
pak	pak	k6eAd1	pak
ještě	ještě	k9	ještě
o	o	k7c4	o
pět	pět	k4xCc4	pět
minut	minuta	k1gFnPc2	minuta
později	pozdě	k6eAd2	pozdě
vystřelila	vystřelit	k5eAaPmAgFnS	vystřelit
výstražnou	výstražný	k2eAgFnSc4d1	výstražná
salvu	salva	k1gFnSc4	salva
ze	z	k7c2	z
svých	svůj	k3xOyFgNnPc2	svůj
šestipalcových	šestipalcový	k2eAgNnPc2d1	šestipalcový
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Palba	palba	k1gFnSc1	palba
vystrašila	vystrašit	k5eAaPmAgFnS	vystrašit
osádku	osádka	k1gFnSc4	osádka
paláce	palác	k1gInSc2	palác
a	a	k8xC	a
zajistila	zajistit	k5eAaPmAgFnS	zajistit
Auroře	Aurora	k1gFnSc3	Aurora
slávu	sláva	k1gFnSc4	sláva
revoluční	revoluční	k2eAgFnSc2d1	revoluční
legendy	legenda	k1gFnSc2	legenda
<g/>
,	,	kIx,	,
dodnes	dodnes	k6eAd1	dodnes
kotvící	kotvící	k2eAgFnSc1d1	kotvící
jako	jako	k8xS	jako
muzeum	muzeum	k1gNnSc1	muzeum
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
Něvy	Něva	k1gFnSc2	Něva
<g/>
.	.	kIx.	.
</s>
<s>
Obléhatelé	obléhatel	k1gMnPc1	obléhatel
dali	dát	k5eAaPmAgMnP	dát
obráncům	obránce	k1gMnPc3	obránce
čas	čas	k1gInSc4	čas
opustit	opustit	k5eAaPmF	opustit
palác	palác	k1gInSc4	palác
<g/>
,	,	kIx,	,
a	a	k8xC	a
pak	pak	k6eAd1	pak
zahájila	zahájit	k5eAaPmAgFnS	zahájit
palbu	palba	k1gFnSc4	palba
pevnost	pevnost	k1gFnSc4	pevnost
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
ostrou	ostrý	k2eAgFnSc7d1	ostrá
municí	munice	k1gFnSc7	munice
<g/>
.	.	kIx.	.
</s>
<s>
Vypálila	vypálit	k5eAaPmAgFnS	vypálit
na	na	k7c4	na
palác	palác	k1gInSc4	palác
asi	asi	k9	asi
třicet	třicet	k4xCc4	třicet
střel	střela	k1gFnPc2	střela
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
jen	jen	k9	jen
dvě	dva	k4xCgFnPc1	dva
nebo	nebo	k8xC	nebo
tři	tři	k4xCgFnPc1	tři
napáchaly	napáchat	k5eAaBmAgInP	napáchat
trochu	trocha	k1gFnSc4	trocha
škody	škoda	k1gFnSc2	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Antonov-Ovsejenko	Antonov-Ovsejenka	k1gFnSc5	Antonov-Ovsejenka
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
dohodu	dohoda	k1gFnSc4	dohoda
dosaženou	dosažený	k2eAgFnSc4d1	dosažená
Chudnovským	Chudnovský	k2eAgInSc7d1	Chudnovský
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
měli	mít	k5eAaImAgMnP	mít
obránci	obránce	k1gMnSc3	obránce
opustit	opustit	k5eAaPmF	opustit
palác	palác	k1gInSc4	palác
se	s	k7c7	s
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
některé	některý	k3yIgFnPc1	některý
skupiny	skupina	k1gFnPc1	skupina
vzdaly	vzdát	k5eAaPmAgFnP	vzdát
bez	bez	k7c2	bez
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
příslušnice	příslušnice	k1gFnSc1	příslušnice
ženského	ženský	k2eAgInSc2d1	ženský
oddílu	oddíl	k1gInSc2	oddíl
odmítaly	odmítat	k5eAaImAgFnP	odmítat
odevzdat	odevzdat	k5eAaPmF	odevzdat
zbraně	zbraň	k1gFnPc1	zbraň
z	z	k7c2	z
obavy	obava	k1gFnSc2	obava
ze	z	k7c2	z
znásilnění	znásilnění	k1gNnSc2	znásilnění
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
paláce	palác	k1gInSc2	palác
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
kontaktovat	kontaktovat	k5eAaImF	kontaktovat
petrohradskou	petrohradský	k2eAgFnSc4d1	Petrohradská
městskou	městský	k2eAgFnSc4d1	městská
radu	rada	k1gFnSc4	rada
<g/>
,	,	kIx,	,
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
delegace	delegace	k1gFnSc1	delegace
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
neúspěšně	úspěšně	k6eNd1	úspěšně
pokusila	pokusit	k5eAaPmAgFnS	pokusit
zastavit	zastavit	k5eAaPmF	zastavit
bombardování	bombardování	k1gNnSc4	bombardování
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
však	však	k9	však
vpuštěna	vpustit	k5eAaPmNgFnS	vpustit
na	na	k7c4	na
Auroru	Aurora	k1gFnSc4	Aurora
ani	ani	k8xC	ani
do	do	k7c2	do
paláce	palác	k1gInSc2	palác
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
přes	přes	k7c4	přes
odpor	odpor	k1gInSc4	odpor
bolševických	bolševický	k2eAgMnPc2d1	bolševický
radních	radní	k1gMnPc2	radní
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
vydat	vydat	k5eAaPmF	vydat
sama	sám	k3xTgMnSc4	sám
k	k	k7c3	k
paláci	palác	k1gInSc3	palác
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
fyzicky	fyzicky	k6eAd1	fyzicky
chránila	chránit	k5eAaImAgFnS	chránit
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
schváleno	schválit	k5eAaPmNgNnS	schválit
Ústředním	ústřední	k2eAgInSc7d1	ústřední
výkonným	výkonný	k2eAgInSc7d1	výkonný
výborem	výbor	k1gInSc7	výbor
rolnických	rolnický	k2eAgInPc2d1	rolnický
sovětů	sovět	k1gInPc2	sovět
<g/>
.	.	kIx.	.
<g/>
Za	za	k7c2	za
soumraku	soumrak	k1gInSc2	soumrak
začaly	začít	k5eAaPmAgFnP	začít
malé	malý	k2eAgFnPc1d1	malá
skupiny	skupina	k1gFnPc1	skupina
obléhatelů	obléhatel	k1gMnPc2	obléhatel
pronikat	pronikat	k5eAaImF	pronikat
do	do	k7c2	do
paláce	palác	k1gInSc2	palác
a	a	k8xC	a
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
jejich	jejich	k3xOp3gInPc4	jejich
počty	počet	k1gInPc4	počet
narůstaly	narůstat	k5eAaImAgFnP	narůstat
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
se	se	k3xPyFc4	se
obráncům	obránce	k1gMnPc3	obránce
dařilo	dařit	k5eAaImAgNnS	dařit
nečetné	četný	k2eNgNnSc1d1	nečetné
vetřelce	vetřelec	k1gMnSc4	vetřelec
odzbrojovat	odzbrojovat	k5eAaImF	odzbrojovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
už	už	k6eAd1	už
byli	být	k5eAaImAgMnP	být
sami	sám	k3xTgMnPc1	sám
odzbrojováni	odzbrojován	k2eAgMnPc1d1	odzbrojován
<g/>
.	.	kIx.	.
</s>
<s>
Odchod	odchod	k1gInSc1	odchod
části	část	k1gFnSc2	část
obránců	obránce	k1gMnPc2	obránce
usnadnil	usnadnit	k5eAaPmAgMnS	usnadnit
vnikání	vnikání	k1gNnSc4	vnikání
obléhatelů	obléhatel	k1gMnPc2	obléhatel
do	do	k7c2	do
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
asi	asi	k9	asi
hodinové	hodinový	k2eAgFnSc6d1	hodinová
přestávce	přestávka	k1gFnSc6	přestávka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
měla	mít	k5eAaImAgFnS	mít
umožnit	umožnit	k5eAaPmF	umožnit
dalším	další	k2eAgMnPc3d1	další
obhájcům	obhájce	k1gMnPc3	obhájce
kapitulovat	kapitulovat	k5eAaBmF	kapitulovat
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
kolem	kolem	k7c2	kolem
23	[number]	k4	23
<g/>
.	.	kIx.	.
hodiny	hodina	k1gFnSc2	hodina
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
ostřelování	ostřelování	k1gNnSc1	ostřelování
paláce	palác	k1gInSc2	palác
i	i	k9	i
přes	přes	k7c4	přes
riziko	riziko	k1gNnSc4	riziko
<g/>
,	,	kIx,	,
že	že	k8xS	že
zasáhne	zasáhnout	k5eAaPmIp3nS	zasáhnout
ty	ten	k3xDgInPc4	ten
z	z	k7c2	z
útočníků	útočník	k1gMnPc2	útočník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
již	již	k6eAd1	již
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
<g/>
.	.	kIx.	.
<g/>
Před	před	k7c7	před
půlnocí	půlnoc	k1gFnSc7	půlnoc
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
více	hodně	k6eAd2	hodně
než	než	k8xS	než
300	[number]	k4	300
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgInSc4d1	zahrnující
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgMnPc4d1	jiný
radní	radní	k1gMnPc4	radní
<g/>
,	,	kIx,	,
poslance	poslanec	k1gMnPc4	poslanec
sjezdu	sjezd	k1gInSc2	sjezd
sovětů	sovět	k1gInPc2	sovět
a	a	k8xC	a
členy	člen	k1gMnPc7	člen
Ústředního	ústřední	k2eAgInSc2d1	ústřední
výkonného	výkonný	k2eAgInSc2d1	výkonný
výboru	výbor	k1gInSc2	výbor
rolnických	rolnický	k2eAgInPc2d1	rolnický
sovětů	sovět	k1gInPc2	sovět
<g/>
,	,	kIx,	,
sešla	sejít	k5eAaPmAgFnS	sejít
na	na	k7c4	na
radnici	radnice	k1gFnSc4	radnice
<g/>
,	,	kIx,	,
a	a	k8xC	a
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
za	za	k7c2	za
zpěvu	zpěv	k1gInSc2	zpěv
Marseillaisy	Marseillaisa	k1gFnSc2	Marseillaisa
vydala	vydat	k5eAaPmAgFnS	vydat
k	k	k7c3	k
paláci	palác	k1gInSc3	palác
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
byli	být	k5eAaImAgMnP	být
Prokopovič	Prokopovič	k1gMnSc1	Prokopovič
<g/>
,	,	kIx,	,
jediný	jediný	k2eAgMnSc1d1	jediný
ministr	ministr	k1gMnSc1	ministr
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
nebyl	být	k5eNaImAgMnS	být
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
<g/>
,	,	kIx,	,
a	a	k8xC	a
eserský	eserský	k2eAgMnSc1d1	eserský
starosta	starosta	k1gMnSc1	starosta
Šreider	Šreider	k1gMnSc1	Šreider
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Kazaňském	kazaňský	k2eAgNnSc6d1	Kazaňské
náměstí	náměstí	k1gNnSc6	náměstí
poblíž	poblíž	k7c2	poblíž
Admirality	admiralita	k1gFnSc2	admiralita
jejich	jejich	k3xOp3gInSc1	jejich
postup	postup	k1gInSc1	postup
zastavil	zastavit	k5eAaPmAgInS	zastavit
oddíl	oddíl	k1gInSc4	oddíl
námořníků	námořník	k1gMnPc2	námořník
a	a	k8xC	a
po	po	k7c6	po
vzrušené	vzrušený	k2eAgFnSc6d1	vzrušená
diskusi	diskuse	k1gFnSc6	diskuse
se	se	k3xPyFc4	se
pochodující	pochodující	k2eAgFnSc1d1	pochodující
spořádaně	spořádaně	k6eAd1	spořádaně
vrátili	vrátit	k5eAaPmAgMnP	vrátit
do	do	k7c2	do
radnice	radnice	k1gFnSc2	radnice
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
něčeho	něco	k3yInSc2	něco
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
<g/>
.	.	kIx.	.
<g/>
Před	před	k7c7	před
druhou	druhý	k4xOgFnSc7	druhý
hodinou	hodina	k1gFnSc7	hodina
ráno	ráno	k6eAd1	ráno
útočníci	útočník	k1gMnPc1	útočník
konečně	konečně	k6eAd1	konečně
našli	najít	k5eAaPmAgMnP	najít
místnost	místnost	k1gFnSc4	místnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
shromážděna	shromážděn	k2eAgFnSc1d1	shromážděna
vláda	vláda	k1gFnSc1	vláda
<g/>
;	;	kIx,	;
ta	ten	k3xDgFnSc1	ten
dala	dát	k5eAaPmAgFnS	dát
své	svůj	k3xOyFgFnSc6	svůj
kadetské	kadetský	k2eAgFnSc6d1	kadetská
stráži	stráž	k1gFnSc6	stráž
rozkaz	rozkaz	k1gInSc1	rozkaz
nepoužít	použít	k5eNaPmF	použít
násilí	násilí	k1gNnSc4	násilí
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
krveprolití	krveprolití	k1gNnSc1	krveprolití
<g/>
.	.	kIx.	.
</s>
<s>
Jednotka	jednotka	k1gFnSc1	jednotka
vedená	vedený	k2eAgFnSc1d1	vedená
Vladimirem	Vladimir	k1gMnSc7	Vladimir
Antonovem-Ovsejenkem	Antonovem-Ovsejenek	k1gMnSc7	Antonovem-Ovsejenek
rychle	rychle	k6eAd1	rychle
obsadila	obsadit	k5eAaPmAgFnS	obsadit
místnost	místnost	k1gFnSc1	místnost
a	a	k8xC	a
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
ministry	ministr	k1gMnPc4	ministr
za	za	k7c4	za
zatčené	zatčený	k1gMnPc4	zatčený
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
2.10	[number]	k4	2.10
26	[number]	k4	26
<g/>
.	.	kIx.	.
říjnajul	říjnajout	k5eAaPmAgInS	říjnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
</s>
<s>
Konovalov	Konovalov	k1gInSc1	Konovalov
jménem	jméno	k1gNnSc7	jméno
vlády	vláda	k1gFnSc2	vláda
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
podrobují	podrobovat	k5eAaImIp3nP	podrobovat
násilí	násilí	k1gNnSc3	násilí
<g/>
.	.	kIx.	.
</s>
<s>
Zadrženi	zadržen	k2eAgMnPc1d1	zadržen
byli	být	k5eAaImAgMnP	být
všichni	všechen	k3xTgMnPc1	všechen
členové	člen	k1gMnPc1	člen
vlády	vláda	k1gFnSc2	vláda
vyjma	vyjma	k7c2	vyjma
Kerenského	Kerenský	k2eAgMnSc2d1	Kerenský
a	a	k8xC	a
Prokopoviče	Prokopovič	k1gMnSc2	Prokopovič
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nebyli	být	k5eNaImAgMnP	být
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
úplně	úplně	k6eAd1	úplně
v	v	k7c6	v
moci	moc	k1gFnSc6	moc
Petrohradského	petrohradský	k2eAgInSc2d1	petrohradský
sovětu	sovět	k1gInSc2	sovět
a	a	k8xC	a
Druhý	druhý	k4xOgInSc1	druhý
sjezd	sjezd	k1gInSc1	sjezd
sovětů	sovět	k1gInPc2	sovět
již	již	k6eAd1	již
začal	začít	k5eAaPmAgInS	začít
<g/>
.	.	kIx.	.
</s>
<s>
Ministry	ministr	k1gMnPc4	ministr
odvedli	odvést	k5eAaPmAgMnP	odvést
do	do	k7c2	do
Petropavlovské	petropavlovský	k2eAgFnSc2d1	Petropavlovská
pevnosti	pevnost	k1gFnSc2	pevnost
a	a	k8xC	a
komisaři	komisar	k1gMnPc1	komisar
VRV	VRV	kA	VRV
jim	on	k3xPp3gMnPc3	on
museli	muset	k5eAaImAgMnP	muset
dát	dát	k5eAaPmF	dát
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
stráž	stráž	k1gFnSc4	stráž
složenou	složený	k2eAgFnSc4d1	složená
z	z	k7c2	z
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
námořníků	námořník	k1gMnPc2	námořník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
je	on	k3xPp3gNnSc4	on
uchránili	uchránit	k5eAaPmAgMnP	uchránit
před	před	k7c7	před
davem	dav	k1gInSc7	dav
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
shromáždil	shromáždit	k5eAaPmAgInS	shromáždit
u	u	k7c2	u
paláce	palác	k1gInSc2	palác
a	a	k8xC	a
hrozil	hrozit	k5eAaImAgMnS	hrozit
jim	on	k3xPp3gNnPc3	on
lynčováním	lynčování	k1gNnPc3	lynčování
zejména	zejména	k6eAd1	zejména
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
rozneslo	roznést	k5eAaPmAgNnS	roznést
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kerenského	Kerenský	k2eAgInSc2d1	Kerenský
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
zadržet	zadržet	k5eAaPmF	zadržet
<g/>
.	.	kIx.	.
<g/>
Filmy	film	k1gInPc4	film
a	a	k8xC	a
jiná	jiný	k2eAgNnPc4d1	jiné
zobrazení	zobrazení	k1gNnPc4	zobrazení
události	událost	k1gFnSc2	událost
často	často	k6eAd1	často
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
veliký	veliký	k2eAgInSc4d1	veliký
útok	útok	k1gInSc4	útok
na	na	k7c4	na
Zimní	zimní	k2eAgInSc4d1	zimní
palác	palác	k1gInSc4	palác
a	a	k8xC	a
krutou	krutý	k2eAgFnSc4d1	krutá
bitvu	bitva	k1gFnSc4	bitva
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
bolševičtí	bolševický	k2eAgMnPc1d1	bolševický
útočníci	útočník	k1gMnPc1	útočník
nenarazili	narazit	k5eNaPmAgMnP	narazit
na	na	k7c4	na
velký	velký	k2eAgInSc4d1	velký
odpor	odpor	k1gInSc4	odpor
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
celkem	celkem	k6eAd1	celkem
snadno	snadno	k6eAd1	snadno
proniknout	proniknout	k5eAaPmF	proniknout
do	do	k7c2	do
budovy	budova	k1gFnSc2	budova
a	a	k8xC	a
obsadit	obsadit	k5eAaPmF	obsadit
ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
petrohradského	petrohradský	k2eAgNnSc2d1	Petrohradské
povstání	povstání	k1gNnSc2	povstání
se	se	k3xPyFc4	se
obešla	obejít	k5eAaPmAgFnS	obejít
bez	bez	k7c2	bez
krveprolití	krveprolití	k1gNnSc2	krveprolití
a	a	k8xC	a
probíhala	probíhat	k5eAaImAgFnS	probíhat
za	za	k7c2	za
poměrně	poměrně	k6eAd1	poměrně
normálního	normální	k2eAgInSc2d1	normální
chodu	chod	k1gInSc2	chod
metropole	metropol	k1gFnSc2	metropol
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
útočníků	útočník	k1gMnPc2	útočník
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
pět	pět	k4xCc1	pět
námořníků	námořník	k1gMnPc2	námořník
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
voják	voják	k1gMnSc1	voják
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
obránci	obránce	k1gMnPc1	obránce
neměli	mít	k5eNaImAgMnP	mít
oběti	oběť	k1gFnPc4	oběť
žádné	žádný	k3yNgNnSc4	žádný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Druhý	druhý	k4xOgInSc1	druhý
sjezd	sjezd	k1gInSc1	sjezd
sovětů	sovět	k1gInPc2	sovět
a	a	k8xC	a
bolševický	bolševický	k2eAgInSc1d1	bolševický
puč	puč	k1gInSc1	puč
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
První	první	k4xOgNnPc1	první
zasedání	zasedání	k1gNnPc1	zasedání
<g/>
:	:	kIx,	:
odchod	odchod	k1gInSc1	odchod
umírněných	umírněný	k2eAgFnPc2d1	umírněná
a	a	k8xC	a
roztržka	roztržka	k1gFnSc1	roztržka
levice	levice	k1gFnSc2	levice
====	====	k?	====
</s>
</p>
<p>
<s>
Zatím	zatím	k6eAd1	zatím
ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
říjnajul	říjnajout	k5eAaPmAgInS	říjnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
ve	v	k7c6	v
22.40	[number]	k4	22.40
hod	hod	k1gInSc1	hod
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
s	s	k7c7	s
téměř	téměř	k6eAd1	téměř
devítihodinovým	devítihodinový	k2eAgNnSc7d1	devítihodinové
zpožděním	zpoždění	k1gNnSc7	zpoždění
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
Druhý	druhý	k4xOgInSc1	druhý
všeruský	všeruský	k2eAgInSc1d1	všeruský
sjezd	sjezd	k1gInSc1	sjezd
sovětů	sovět	k1gInPc2	sovět
dělníků	dělník	k1gMnPc2	dělník
a	a	k8xC	a
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Bolševici	bolševik	k1gMnPc1	bolševik
se	se	k3xPyFc4	se
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
sestavit	sestavit	k5eAaPmF	sestavit
dočasnou	dočasný	k2eAgFnSc4d1	dočasná
vládu	vláda	k1gFnSc4	vláda
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
sjezdu	sjezd	k1gInSc2	sjezd
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
delegáti	delegát	k1gMnPc1	delegát
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
už	už	k6eAd1	už
nemohli	moct	k5eNaImAgMnP	moct
déle	dlouho	k6eAd2	dlouho
čekat	čekat	k5eAaImF	čekat
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
vynutili	vynutit	k5eAaPmAgMnP	vynutit
konec	konec	k1gInSc4	konec
odkladů	odklad	k1gInPc2	odklad
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
sjezd	sjezd	k1gInSc4	sjezd
přišlo	přijít	k5eAaPmAgNnS	přijít
asi	asi	k9	asi
670	[number]	k4	670
zvolených	zvolený	k2eAgMnPc2d1	zvolený
delegátů	delegát	k1gMnPc2	delegát
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
300	[number]	k4	300
bylo	být	k5eAaImAgNnS	být
bolševiků	bolševik	k1gMnPc2	bolševik
a	a	k8xC	a
asi	asi	k9	asi
100	[number]	k4	100
bylo	být	k5eAaImAgNnS	být
levých	levý	k2eAgMnPc2d1	levý
eserů	eser	k1gMnPc2	eser
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
také	také	k9	také
podporovali	podporovat	k5eAaImAgMnP	podporovat
svržení	svržení	k1gNnSc4	svržení
vlády	vláda	k1gFnSc2	vláda
Kerenského	Kerenský	k2eAgInSc2d1	Kerenský
<g/>
.	.	kIx.	.
</s>
<s>
Podpora	podpora	k1gFnSc1	podpora
bolševiků	bolševik	k1gMnPc2	bolševik
se	se	k3xPyFc4	se
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
měsících	měsíc	k1gInPc6	měsíc
před	před	k7c7	před
sjezdem	sjezd	k1gInSc7	sjezd
dramaticky	dramaticky	k6eAd1	dramaticky
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neměli	mít	k5eNaImAgMnP	mít
absolutní	absolutní	k2eAgFnSc4d1	absolutní
většinu	většina	k1gFnSc4	většina
delegátů	delegát	k1gMnPc2	delegát
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
úkolem	úkol	k1gInSc7	úkol
kongresu	kongres	k1gInSc2	kongres
byla	být	k5eAaImAgFnS	být
volba	volba	k1gFnSc1	volba
nového	nový	k2eAgNnSc2d1	nové
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
<g/>
;	;	kIx,	;
bolševici	bolševik	k1gMnPc1	bolševik
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
složení	složený	k2eAgMnPc1d1	složený
se	s	k7c7	s
čtrnácti	čtrnáct	k4xCc7	čtrnáct
bolševiky	bolševik	k1gMnPc7	bolševik
<g/>
,	,	kIx,	,
sedmi	sedm	k4xCc7	sedm
sociálními	sociální	k2eAgMnPc7d1	sociální
revolucionáři	revolucionář	k1gMnPc7	revolucionář
<g/>
,	,	kIx,	,
třemi	tři	k4xCgNnPc7	tři
menševiky	menševik	k1gMnPc4	menševik
a	a	k8xC	a
jedním	jeden	k4xCgInSc7	jeden
menševickým	menševický	k2eAgMnSc7d1	menševický
internacionalistou	internacionalista	k1gMnSc7	internacionalista
(	(	kIx(	(
<g/>
Julius	Julius	k1gMnSc1	Julius
Martov	Martov	k1gInSc1	Martov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
schváleno	schválit	k5eAaPmNgNnS	schválit
v	v	k7c6	v
bouřlivém	bouřlivý	k2eAgNnSc6d1	bouřlivé
hlasování	hlasování	k1gNnSc6	hlasování
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
byl	být	k5eAaImAgMnS	být
Kameněv	Kameněv	k1gMnSc1	Kameněv
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
předsedou	předseda	k1gMnSc7	předseda
sjezdu	sjezd	k1gInSc2	sjezd
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
předložení	předložení	k1gNnSc6	předložení
agendy	agenda	k1gFnSc2	agenda
navržené	navržený	k2eAgMnPc4d1	navržený
bolševiky	bolševik	k1gMnPc4	bolševik
–	–	k?	–
sestavení	sestavení	k1gNnSc4	sestavení
nové	nový	k2eAgFnSc2d1	nová
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
snaha	snaha	k1gFnSc1	snaha
o	o	k7c6	o
ukončení	ukončení	k1gNnSc6	ukončení
války	válka	k1gFnSc2	válka
a	a	k8xC	a
svolání	svolání	k1gNnSc2	svolání
Ústavodárného	ústavodárný	k2eAgNnSc2d1	Ústavodárné
shromáždění	shromáždění	k1gNnSc2	shromáždění
–	–	k?	–
dal	dát	k5eAaPmAgInS	dát
Kameněv	Kameněv	k1gMnPc3	Kameněv
slovo	slovo	k1gNnSc4	slovo
Martovovi	Martova	k1gMnSc3	Martova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
začátku	začátek	k1gInSc6	začátek
zasedání	zasedání	k1gNnSc2	zasedání
uslyšeli	uslyšet	k5eAaPmAgMnP	uslyšet
delegáti	delegát	k1gMnPc1	delegát
střelbu	střelba	k1gFnSc4	střelba
Petropavlovské	petropavlovský	k2eAgFnSc3d1	Petropavlovská
pevnosti	pevnost	k1gFnSc3	pevnost
na	na	k7c4	na
Zimní	zimní	k2eAgInSc4d1	zimní
palác	palác	k1gInSc4	palác
<g/>
.	.	kIx.	.
</s>
<s>
Kongres	kongres	k1gInSc1	kongres
původně	původně	k6eAd1	původně
jednohlasně	jednohlasně	k6eAd1	jednohlasně
schválil	schválit	k5eAaPmAgInS	schválit
návrh	návrh	k1gInSc1	návrh
vůdčího	vůdčí	k2eAgMnSc2d1	vůdčí
menševického	menševický	k2eAgMnSc2d1	menševický
internacionalisty	internacionalista	k1gMnSc2	internacionalista
Julia	Julius	k1gMnSc2	Julius
Martova	Martův	k2eAgMnSc2d1	Martův
jmenovat	jmenovat	k5eAaBmF	jmenovat
demokratickou	demokratický	k2eAgFnSc4d1	demokratická
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
zahrnula	zahrnout	k5eAaPmAgFnS	zahrnout
všechny	všechen	k3xTgFnPc4	všechen
strany	strana	k1gFnPc4	strana
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
součástí	součást	k1gFnSc7	součást
Petrohradského	petrohradský	k2eAgInSc2d1	petrohradský
sovětu	sovět	k1gInSc2	sovět
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
dalšímu	další	k1gNnSc3	další
krveprolití	krveprolití	k1gNnSc2	krveprolití
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
podpořil	podpořit	k5eAaPmAgInS	podpořit
Anatolij	Anatolij	k1gFnSc4	Anatolij
Lunačarskij	Lunačarskij	k1gFnSc3	Lunačarskij
za	za	k7c7	za
bolševiky	bolševik	k1gMnPc7	bolševik
a	a	k8xC	a
Sergej	Sergej	k1gMnSc1	Sergej
Mstislavskij	Mstislavskij	k1gMnSc1	Mstislavskij
za	za	k7c4	za
levé	levý	k2eAgMnPc4d1	levý
esery	eser	k1gMnPc4	eser
<g/>
.	.	kIx.	.
<g/>
Když	když	k8xS	když
však	však	k9	však
byli	být	k5eAaImAgMnP	být
informováni	informovat	k5eAaBmNgMnP	informovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Prozatímní	prozatímní	k2eAgFnSc1d1	prozatímní
vláda	vláda	k1gFnSc1	vláda
byla	být	k5eAaImAgFnS	být
svržena	svrhnout	k5eAaPmNgFnS	svrhnout
a	a	k8xC	a
že	že	k8xS	že
její	její	k3xOp3gMnPc1	její
členové	člen	k1gMnPc1	člen
–	–	k?	–
včetně	včetně	k7c2	včetně
eserských	eserský	k2eAgMnPc2d1	eserský
a	a	k8xC	a
menševických	menševický	k2eAgMnPc2d1	menševický
ministrů	ministr	k1gMnPc2	ministr
–	–	k?	–
byli	být	k5eAaImAgMnP	být
zajati	zajmout	k5eAaPmNgMnP	zajmout
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
delegáti	delegát	k1gMnPc1	delegát
těchto	tento	k3xDgFnPc2	tento
stran	strana	k1gFnPc2	strana
zastoupených	zastoupený	k2eAgFnPc2d1	zastoupená
v	v	k7c6	v
Petrohradském	petrohradský	k2eAgInSc6d1	petrohradský
sovětu	sovět	k1gInSc6	sovět
odsoudili	odsoudit	k5eAaPmAgMnP	odsoudit
tento	tento	k3xDgInSc4	tento
postup	postup	k1gInSc4	postup
a	a	k8xC	a
opustili	opustit	k5eAaPmAgMnP	opustit
na	na	k7c4	na
protest	protest	k1gInSc4	protest
sál	sál	k1gInSc1	sál
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
frakcí	frakce	k1gFnPc2	frakce
eserů	eser	k1gMnPc2	eser
i	i	k8xC	i
menševiků	menševik	k1gMnPc2	menševik
se	se	k3xPyFc4	se
postavila	postavit	k5eAaPmAgFnS	postavit
proti	proti	k7c3	proti
činům	čin	k1gInPc3	čin
bolševiků	bolševik	k1gMnPc2	bolševik
<g/>
;	;	kIx,	;
tito	tento	k3xDgMnPc1	tento
politici	politik	k1gMnPc1	politik
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
připojit	připojit	k5eAaPmF	připojit
k	k	k7c3	k
pochodu	pochod	k1gInSc3	pochod
na	na	k7c4	na
Zimní	zimní	k2eAgInSc4d1	zimní
palác	palác	k1gInSc4	palác
organizovanému	organizovaný	k2eAgMnSc3d1	organizovaný
radou	rada	k1gMnSc7	rada
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ukázali	ukázat	k5eAaPmAgMnP	ukázat
svou	svůj	k3xOyFgFnSc4	svůj
podporu	podpora	k1gFnSc4	podpora
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Odchod	odchod	k1gInSc1	odchod
umírněných	umírněný	k2eAgFnPc2d1	umírněná
podkopal	podkopat	k5eAaPmAgMnS	podkopat
úsilí	úsilí	k1gNnSc3	úsilí
levých	levý	k2eAgMnPc2d1	levý
eserů	eser	k1gMnPc2	eser
<g/>
,	,	kIx,	,
menševiků	menševik	k1gMnPc2	menševik
internacionalistů	internacionalista	k1gMnPc2	internacionalista
a	a	k8xC	a
umírněnějších	umírněný	k2eAgMnPc2d2	umírněnější
politiků	politik	k1gMnPc2	politik
bolševické	bolševický	k2eAgFnSc2d1	bolševická
strany	strana	k1gFnSc2	strana
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
dohody	dohoda	k1gFnSc2	dohoda
socialistických	socialistický	k2eAgFnPc2d1	socialistická
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
usnadnilo	usnadnit	k5eAaPmAgNnS	usnadnit
Leninovi	Lenin	k1gMnSc3	Lenin
vytvořit	vytvořit	k5eAaPmF	vytvořit
novou	nový	k2eAgFnSc4d1	nová
výlučně	výlučně	k6eAd1	výlučně
bolševickou	bolševický	k2eAgFnSc4d1	bolševická
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
<g/>
Martov	Martov	k1gInSc1	Martov
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
zachovat	zachovat	k5eAaPmF	zachovat
konsensus	konsensus	k1gInSc4	konsensus
a	a	k8xC	a
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
vytvořit	vytvořit	k5eAaPmF	vytvořit
novou	nový	k2eAgFnSc4d1	nová
vládu	vláda	k1gFnSc4	vláda
přijatelnou	přijatelný	k2eAgFnSc4d1	přijatelná
pro	pro	k7c4	pro
umírněné	umírněný	k2eAgMnPc4d1	umírněný
i	i	k8xC	i
radikály	radikál	k1gMnPc4	radikál
a	a	k8xC	a
pozastavit	pozastavit	k5eAaPmF	pozastavit
jednání	jednání	k1gNnSc4	jednání
sjezdu	sjezd	k1gInSc2	sjezd
do	do	k7c2	do
jejího	její	k3xOp3gNnSc2	její
sestavení	sestavení	k1gNnSc2	sestavení
<g/>
.	.	kIx.	.
</s>
<s>
Lev	Lev	k1gMnSc1	Lev
Trockij	Trockij	k1gMnSc1	Trockij
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
slovo	slovo	k1gNnSc4	slovo
a	a	k8xC	a
na	na	k7c4	na
adresu	adresa	k1gFnSc4	adresa
frakcí	frakce	k1gFnPc2	frakce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
právě	právě	k6eAd1	právě
opustily	opustit	k5eAaPmAgFnP	opustit
sjezd	sjezd	k1gInSc4	sjezd
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc2	jejich
příznivců	příznivec	k1gMnPc2	příznivec
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byl	být	k5eAaImAgInS	být
Martov	Martov	k1gInSc1	Martov
<g/>
,	,	kIx,	,
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
<g/>
"	"	kIx"	"
<g/>
To	ten	k3xDgNnSc1	ten
co	co	k9	co
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
povstání	povstání	k1gNnSc1	povstání
<g/>
,	,	kIx,	,
ne	ne	k9	ne
spiknutí	spiknutí	k1gNnSc1	spiknutí
<g/>
.	.	kIx.	.
</s>
<s>
Povzbuzujeme	povzbuzovat	k5eAaImIp1nP	povzbuzovat
revoluční	revoluční	k2eAgFnSc4d1	revoluční
energii	energie	k1gFnSc4	energie
dělníků	dělník	k1gMnPc2	dělník
a	a	k8xC	a
vojáků	voják	k1gMnPc2	voják
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
<g/>
;	;	kIx,	;
vykovali	vykovat	k5eAaPmAgMnP	vykovat
jsme	být	k5eAaImIp1nP	být
vůli	vůle	k1gFnSc4	vůle
mas	masa	k1gFnPc2	masa
k	k	k7c3	k
povstání	povstání	k1gNnSc3	povstání
<g/>
,	,	kIx,	,
ne	ne	k9	ne
ke	k	k7c3	k
spiknutí	spiknutí	k1gNnSc3	spiknutí
<g/>
.	.	kIx.	.
</s>
<s>
Lidové	lidový	k2eAgFnPc1d1	lidová
masy	masa	k1gFnPc1	masa
následovaly	následovat	k5eAaImAgFnP	následovat
náš	náš	k3xOp1gInSc4	náš
prapor	prapor	k1gInSc4	prapor
a	a	k8xC	a
naše	náš	k3xOp1gNnSc1	náš
povstání	povstání	k1gNnSc1	povstání
zvítězilo	zvítězit	k5eAaPmAgNnS	zvítězit
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
teď	teď	k6eAd1	teď
nám	my	k3xPp1nPc3	my
říkají	říkat	k5eAaImIp3nP	říkat
<g/>
:	:	kIx,	:
zřekněte	zřeknout	k5eAaPmRp2nP	zřeknout
se	se	k3xPyFc4	se
svého	svůj	k3xOyFgNnSc2	svůj
vítězství	vítězství	k1gNnSc2	vítězství
<g/>
,	,	kIx,	,
udělejte	udělat	k5eAaPmRp2nP	udělat
ústupky	ústupek	k1gInPc4	ústupek
<g/>
,	,	kIx,	,
couvněte	couvnout	k5eAaPmRp2nP	couvnout
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
kým	kdo	k3yInSc7	kdo
<g/>
?	?	kIx.	?
</s>
<s>
Ptám	ptat	k5eAaImIp1nS	ptat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
před	před	k7c7	před
kým	kdo	k3yInSc7	kdo
bychom	by	kYmCp1nP	by
měli	mít	k5eAaImAgMnP	mít
ustoupit	ustoupit	k5eAaPmF	ustoupit
<g/>
?	?	kIx.	?
</s>
<s>
Před	před	k7c7	před
těmi	ten	k3xDgFnPc7	ten
zkrachovanými	zkrachovaný	k2eAgFnPc7d1	zkrachovaná
skupinami	skupina	k1gFnPc7	skupina
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nás	my	k3xPp1nPc4	my
opustily	opustit	k5eAaPmAgInP	opustit
nebo	nebo	k8xC	nebo
před	před	k7c7	před
těmi	ten	k3xDgMnPc7	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
činí	činit	k5eAaImIp3nP	činit
tento	tento	k3xDgInSc4	tento
návrh	návrh	k1gInSc4	návrh
<g/>
?	?	kIx.	?
</s>
<s>
Ale	ale	k9	ale
my	my	k3xPp1nPc1	my
je	on	k3xPp3gMnPc4	on
už	už	k6eAd1	už
známe	znát	k5eAaImIp1nP	znát
<g/>
,	,	kIx,	,
nikdo	nikdo	k3yNnSc1	nikdo
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
nepodporuje	podporovat	k5eNaImIp3nS	podporovat
<g/>
...	...	k?	...
Ne	ne	k9	ne
<g/>
,	,	kIx,	,
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
není	být	k5eNaImIp3nS	být
žádná	žádný	k3yNgFnSc1	žádný
dohoda	dohoda	k1gFnSc1	dohoda
možná	možný	k2eAgFnSc1d1	možná
<g/>
.	.	kIx.	.
</s>
<s>
Těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
odešli	odejít	k5eAaPmAgMnP	odejít
<g/>
,	,	kIx,	,
a	a	k8xC	a
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
nám	my	k3xPp1nPc3	my
to	ten	k3xDgNnSc4	ten
navrhují	navrhovat	k5eAaImIp3nP	navrhovat
<g/>
,	,	kIx,	,
odpovídáme	odpovídat	k5eAaImIp1nP	odpovídat
<g/>
:	:	kIx,	:
zbankrotovali	zbankrotovat	k5eAaPmAgMnP	zbankrotovat
jste	být	k5eAaImIp2nP	být
<g/>
,	,	kIx,	,
vaše	váš	k3xOp2gFnSc1	váš
role	role	k1gFnSc1	role
končí	končit	k5eAaImIp3nS	končit
<g/>
,	,	kIx,	,
jděte	jít	k5eAaImRp2nP	jít
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
patříte	patřit	k5eAaImIp2nP	patřit
<g/>
,	,	kIx,	,
na	na	k7c4	na
smetiště	smetiště	k1gNnSc4	smetiště
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgInS	být
návrh	návrh	k1gInSc1	návrh
spolupráce	spolupráce	k1gFnSc2	spolupráce
mezi	mezi	k7c7	mezi
socialistickými	socialistický	k2eAgFnPc7d1	socialistická
stranami	strana	k1gFnPc7	strana
odmítnut	odmítnut	k2eAgInSc1d1	odmítnut
radikálním	radikální	k2eAgNnSc7d1	radikální
křídlem	křídlo	k1gNnSc7	křídlo
bolševiků	bolševik	k1gMnPc2	bolševik
zastoupeným	zastoupený	k2eAgMnSc7d1	zastoupený
Trockým	Trocký	k1gMnSc7	Trocký
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
Martovových	Martovův	k2eAgMnPc2d1	Martovův
menševických	menševický	k2eAgMnPc2d1	menševický
internacionalistů	internacionalista	k1gMnPc2	internacionalista
také	také	k9	také
opustila	opustit	k5eAaPmAgFnS	opustit
sjezd	sjezd	k1gInSc4	sjezd
<g/>
.	.	kIx.	.
</s>
<s>
Trockij	Trockít	k5eAaPmRp2nS	Trockít
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
okamžitého	okamžitý	k2eAgNnSc2d1	okamžité
odsouzení	odsouzení	k1gNnSc2	odsouzení
umírněných	umírněný	k2eAgMnPc2d1	umírněný
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
právě	právě	k6eAd1	právě
opustili	opustit	k5eAaPmAgMnP	opustit
sjezd	sjezd	k1gInSc4	sjezd
<g/>
,	,	kIx,	,
a	a	k8xC	a
označil	označit	k5eAaPmAgMnS	označit
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
kontrarevolucionáře	kontrarevolucionář	k1gMnPc4	kontrarevolucionář
<g/>
.	.	kIx.	.
</s>
<s>
Leví	levý	k2eAgMnPc1d1	levý
eseři	eser	k1gMnPc1	eser
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgFnPc2d1	jiná
frakcí	frakce	k1gFnPc2	frakce
zůstali	zůstat	k5eAaPmAgMnP	zůstat
a	a	k8xC	a
snažili	snažit	k5eAaImAgMnP	snažit
se	se	k3xPyFc4	se
mírnit	mírnit	k5eAaImF	mírnit
postoje	postoj	k1gInPc4	postoj
bolševiků	bolševik	k1gMnPc2	bolševik
a	a	k8xC	a
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
dohody	dohoda	k1gFnPc4	dohoda
mezi	mezi	k7c7	mezi
různými	různý	k2eAgInPc7d1	různý
socialistickými	socialistický	k2eAgInPc7d1	socialistický
směry	směr	k1gInPc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Zprávy	zpráva	k1gFnPc1	zpráva
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Zimního	zimní	k2eAgInSc2d1	zimní
paláce	palác	k1gInSc2	palác
přicházely	přicházet	k5eAaImAgFnP	přicházet
od	od	k7c2	od
různých	různý	k2eAgFnPc2d1	různá
vojenských	vojenský	k2eAgFnPc2d1	vojenská
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
vyvolaly	vyvolat	k5eAaPmAgFnP	vyvolat
mezi	mezi	k7c4	mezi
delegáty	delegát	k1gMnPc4	delegát
euforii	euforie	k1gFnSc4	euforie
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
říkala	říkat	k5eAaImAgFnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
3	[number]	k4	3
<g/>
.	.	kIx.	.
prapor	prapor	k1gInSc1	prapor
cyklistů	cyklista	k1gMnPc2	cyklista
<g/>
,	,	kIx,	,
povolaný	povolaný	k2eAgInSc4d1	povolaný
Kerenským	Kerenský	k2eAgInSc7d1	Kerenský
z	z	k7c2	z
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
fronty	fronta	k1gFnSc2	fronta
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
podřídil	podřídit	k5eAaPmAgMnS	podřídit
Petrohradskému	petrohradský	k2eAgInSc3d1	petrohradský
sovětu	sovět	k1gInSc3	sovět
a	a	k8xC	a
že	že	k9	že
blízká	blízký	k2eAgFnSc1d1	blízká
posádka	posádka	k1gFnSc1	posádka
v	v	k7c6	v
Carském	carský	k2eAgNnSc6d1	carské
selu	selo	k1gNnSc6	selo
slíbila	slíbit	k5eAaPmAgFnS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
chránit	chránit	k5eAaImF	chránit
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
před	před	k7c7	před
případnými	případný	k2eAgInPc7d1	případný
útoky	útok	k1gInPc7	útok
<g/>
.	.	kIx.	.
</s>
<s>
Krylenko	Krylenka	k1gFnSc5	Krylenka
hlásil	hlásit	k5eAaImAgMnS	hlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
frontě	fronta	k1gFnSc6	fronta
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Revoluční	revoluční	k2eAgInSc1d1	revoluční
vojenský	vojenský	k2eAgInSc1d1	vojenský
výbor	výbor	k1gInSc1	výbor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zrušil	zrušit	k5eAaPmAgInS	zrušit
rozkaz	rozkaz	k1gInSc4	rozkaz
pochodovat	pochodovat	k5eAaImF	pochodovat
proti	proti	k7c3	proti
hlavnímu	hlavní	k2eAgNnSc3d1	hlavní
městu	město	k1gNnSc3	město
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
velitel	velitel	k1gMnSc1	velitel
fronty	fronta	k1gFnSc2	fronta
Čeremisov	Čeremisov	k1gInSc1	Čeremisov
uznal	uznat	k5eAaPmAgInS	uznat
autoritu	autorita	k1gFnSc4	autorita
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Jednotky	jednotka	k1gFnPc1	jednotka
vyslané	vyslaný	k2eAgFnPc1d1	vyslaná
z	z	k7c2	z
fronty	fronta	k1gFnSc2	fronta
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
rozdrtily	rozdrtit	k5eAaPmAgFnP	rozdrtit
povstání	povstání	k1gNnSc4	povstání
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
oznámovaly	oznámovat	k5eAaPmAgFnP	oznámovat
svou	svůj	k3xOyFgFnSc4	svůj
podporu	podpora	k1gFnSc4	podpora
Revolučnímu	revoluční	k2eAgInSc3d1	revoluční
vojenskému	vojenský	k2eAgInSc3d1	vojenský
výboru	výbor	k1gInSc3	výbor
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
<g/>
Pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
přečtena	přečten	k2eAgFnSc1d1	přečtena
proklamace	proklamace	k1gFnSc1	proklamace
o	o	k7c4	o
převzetí	převzetí	k1gNnSc4	převzetí
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
sestavená	sestavený	k2eAgFnSc1d1	sestavená
Leninem	Lenin	k1gMnSc7	Lenin
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
podle	podle	k7c2	podle
Lunačarského	Lunačarský	k2eAgMnSc2d1	Lunačarský
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
chvíli	chvíle	k1gFnSc4	chvíle
ještě	ještě	k9	ještě
na	na	k7c4	na
sjezd	sjezd	k1gInSc4	sjezd
nedorazil	dorazit	k5eNaPmAgMnS	dorazit
<g/>
.	.	kIx.	.
</s>
<s>
Obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
také	také	k9	také
základní	základní	k2eAgInPc4d1	základní
rysy	rys	k1gInPc4	rys
programu	program	k1gInSc2	program
nové	nový	k2eAgFnSc2d1	nová
vlády	vláda	k1gFnSc2	vláda
<g/>
:	:	kIx,	:
<g/>
"	"	kIx"	"
<g/>
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
vláda	vláda	k1gFnSc1	vláda
okamžitě	okamžitě	k6eAd1	okamžitě
navrhne	navrhnout	k5eAaPmIp3nS	navrhnout
demokratický	demokratický	k2eAgInSc1d1	demokratický
mír	mír	k1gInSc1	mír
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgInPc7	všecek
národy	národ	k1gInPc7	národ
a	a	k8xC	a
zahájení	zahájení	k1gNnSc1	zahájení
okamžitého	okamžitý	k2eAgNnSc2d1	okamžité
příměří	příměří	k1gNnSc2	příměří
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
frontách	fronta	k1gFnPc6	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Zajistí	zajistit	k5eAaPmIp3nS	zajistit
bezúplatný	bezúplatný	k2eAgInSc4d1	bezúplatný
převod	převod	k1gInSc4	převod
půdy	půda	k1gFnSc2	půda
statkářů	statkář	k1gMnPc2	statkář
<g/>
,	,	kIx,	,
koruny	koruna	k1gFnSc2	koruna
a	a	k8xC	a
klášterů	klášter	k1gInPc2	klášter
rolnickým	rolnický	k2eAgInPc3d1	rolnický
výborům	výbor	k1gInPc3	výbor
<g/>
;	;	kIx,	;
bude	být	k5eAaImBp3nS	být
chránit	chránit	k5eAaImF	chránit
práva	právo	k1gNnPc4	právo
vojáků	voják	k1gMnPc2	voják
zavedením	zavedení	k1gNnSc7	zavedení
úplné	úplný	k2eAgFnSc2d1	úplná
demokracie	demokracie	k1gFnSc2	demokracie
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
<g/>
;	;	kIx,	;
zavede	zavést	k5eAaPmIp3nS	zavést
řízení	řízení	k1gNnSc4	řízení
produkce	produkce	k1gFnSc2	produkce
dělníky	dělník	k1gMnPc7	dělník
<g/>
;	;	kIx,	;
zajistí	zajistit	k5eAaPmIp3nP	zajistit
konání	konání	k1gNnSc4	konání
Ústavodárného	ústavodárný	k2eAgNnSc2d1	Ústavodárné
shromáždění	shromáždění	k1gNnSc2	shromáždění
ve	v	k7c4	v
stanovený	stanovený	k2eAgInSc4d1	stanovený
čas	čas	k1gInSc4	čas
<g/>
;	;	kIx,	;
bude	být	k5eAaImBp3nS	být
odpovědná	odpovědný	k2eAgFnSc1d1	odpovědná
za	za	k7c4	za
zásobování	zásobování	k1gNnSc4	zásobování
měst	město	k1gNnPc2	město
a	a	k8xC	a
lidí	člověk	k1gMnPc2	člověk
zbožím	zboží	k1gNnSc7	zboží
<g/>
;	;	kIx,	;
zajistí	zajistit	k5eAaPmIp3nS	zajistit
všem	všecek	k3xTgFnPc3	všecek
národnostem	národnost	k1gFnPc3	národnost
Ruska	Rusko	k1gNnSc2	Rusko
právo	právo	k1gNnSc4	právo
na	na	k7c6	na
opravdové	opravdový	k2eAgNnSc4d1	opravdové
sebeurčení	sebeurčení	k1gNnSc4	sebeurčení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sjezd	sjezd	k1gInSc1	sjezd
vyhlásí	vyhlásit	k5eAaPmIp3nS	vyhlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechna	všechen	k3xTgFnSc1	všechen
moc	moc	k1gFnSc1	moc
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
přechází	přecházet	k5eAaImIp3nS	přecházet
na	na	k7c4	na
sověty	sovět	k1gInPc4	sovět
zástupců	zástupce	k1gMnPc2	zástupce
dělníků	dělník	k1gMnPc2	dělník
<g/>
,	,	kIx,	,
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
rolníků	rolník	k1gMnPc2	rolník
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Dekret	dekret	k1gInSc1	dekret
byl	být	k5eAaImAgInS	být
schválen	schválit	k5eAaPmNgInS	schválit
s	s	k7c7	s
jen	jen	k6eAd1	jen
dvěma	dva	k4xCgInPc7	dva
hlasy	hlas	k1gInPc7	hlas
proti	proti	k7c3	proti
a	a	k8xC	a
dvanácti	dvanáct	k4xCc2	dvanáct
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
zdržely	zdržet	k5eAaPmAgFnP	zdržet
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
svítání	svítání	k1gNnSc6	svítání
vyčerpaní	vyčerpaný	k2eAgMnPc1d1	vyčerpaný
delegáti	delegát	k1gMnPc1	delegát
odročili	odročit	k5eAaPmAgMnP	odročit
jednání	jednání	k1gNnSc4	jednání
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
vyspat	vyspat	k5eAaPmF	vyspat
<g/>
.	.	kIx.	.
</s>
<s>
Socialisté	socialist	k1gMnPc1	socialist
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
odešli	odejít	k5eAaPmAgMnP	odejít
ze	z	k7c2	z
sjezdu	sjezd	k1gInSc2	sjezd
a	a	k8xC	a
nesouhlasili	souhlasit	k5eNaImAgMnP	souhlasit
s	s	k7c7	s
převzetím	převzetí	k1gNnSc7	převzetí
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
založili	založit	k5eAaPmAgMnP	založit
Výbor	výbor	k1gInSc4	výbor
pro	pro	k7c4	pro
záchranu	záchrana	k1gFnSc4	záchrana
vlasti	vlast	k1gFnSc2	vlast
a	a	k8xC	a
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc1	první
centrum	centrum	k1gNnSc1	centrum
odporu	odpor	k1gInSc2	odpor
vůči	vůči	k7c3	vůči
nové	nový	k2eAgFnSc3d1	nová
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Výbor	výbor	k1gInSc1	výbor
odsoudil	odsoudit	k5eAaPmAgInS	odsoudit
akce	akce	k1gFnPc4	akce
bolševiků	bolševik	k1gMnPc2	bolševik
<g/>
,	,	kIx,	,
požádal	požádat	k5eAaPmAgMnS	požádat
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
o	o	k7c4	o
podporu	podpora	k1gFnSc4	podpora
a	a	k8xC	a
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
záměr	záměr	k1gInSc4	záměr
sestavit	sestavit	k5eAaPmF	sestavit
novou	nový	k2eAgFnSc4d1	nová
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Konstitučně	konstitučně	k6eAd1	konstitučně
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
kadeti	kadet	k1gMnPc1	kadet
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
svůj	svůj	k3xOyFgInSc4	svůj
odpor	odpor	k1gInSc4	odpor
k	k	k7c3	k
bolševikům	bolševik	k1gMnPc3	bolševik
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
k	k	k7c3	k
nové	nový	k2eAgFnSc3d1	nová
organizaci	organizace	k1gFnSc3	organizace
nepřipojila	připojit	k5eNaPmAgFnS	připojit
a	a	k8xC	a
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c6	v
obraně	obrana	k1gFnSc6	obrana
legitimity	legitimita	k1gFnSc2	legitimita
zrušené	zrušený	k2eAgFnSc2d1	zrušená
Prozatímní	prozatímní	k2eAgFnSc2d1	prozatímní
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Druhé	druhý	k4xOgNnSc1	druhý
zasedání	zasedání	k1gNnSc1	zasedání
<g/>
:	:	kIx,	:
Sovnarkom	sovnarkom	k1gInSc1	sovnarkom
a	a	k8xC	a
revoluční	revoluční	k2eAgInPc1d1	revoluční
dekrety	dekret	k1gInPc1	dekret
====	====	k?	====
</s>
</p>
<p>
<s>
Druhého	druhý	k4xOgInSc2	druhý
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
ospalí	ospalý	k2eAgMnPc1d1	ospalý
delegáti	delegát	k1gMnPc1	delegát
všech	všecek	k3xTgFnPc2	všecek
socialistických	socialistický	k2eAgFnPc2d1	socialistická
stran	strana	k1gFnPc2	strana
vrátili	vrátit	k5eAaPmAgMnP	vrátit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
hovořili	hovořit	k5eAaImAgMnP	hovořit
o	o	k7c6	o
podobě	podoba	k1gFnSc6	podoba
nové	nový	k2eAgFnSc2d1	nová
vlády	vláda	k1gFnSc2	vláda
<g/>
;	;	kIx,	;
bolševický	bolševický	k2eAgInSc1d1	bolševický
ústřední	ústřední	k2eAgInSc1d1	ústřední
výbor	výbor	k1gInSc1	výbor
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
složení	složení	k1gNnSc4	složení
nové	nový	k2eAgFnSc2d1	nová
vlády	vláda	k1gFnSc2	vláda
zvané	zvaný	k2eAgFnSc2d1	zvaná
Rada	rada	k1gFnSc1	rada
lidových	lidový	k2eAgMnPc2d1	lidový
komisařů	komisař	k1gMnPc2	komisař
(	(	kIx(	(
<g/>
ruskou	ruský	k2eAgFnSc7d1	ruská
zkratkou	zkratka	k1gFnSc7	zkratka
Sovnarkom	sovnarkom	k1gInSc1	sovnarkom
<g/>
)	)	kIx)	)
–	–	k?	–
Trockij	Trockij	k1gMnSc4	Trockij
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
nazývat	nazývat	k5eAaImF	nazývat
její	její	k3xOp3gMnPc4	její
členy	člen	k1gMnPc4	člen
komisaři	komisar	k1gMnPc1	komisar
–	–	k?	–
a	a	k8xC	a
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
tři	tři	k4xCgNnPc4	tři
křesla	křeslo	k1gNnPc4	křeslo
levým	levý	k2eAgMnPc3d1	levý
eserům	eser	k1gMnPc3	eser
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
však	však	k9	však
do	do	k7c2	do
vlády	vláda	k1gFnSc2	vláda
nechtěli	chtít	k5eNaImAgMnP	chtít
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
nebyly	být	k5eNaImAgFnP	být
i	i	k9	i
jiné	jiný	k2eAgFnPc1d1	jiná
strany	strana	k1gFnPc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
zasedání	zasedání	k1gNnSc6	zasedání
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
26	[number]	k4	26
<g/>
.	.	kIx.	.
říjnajul	říjnajout	k5eAaPmAgInS	říjnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
</s>
<s>
Lenin	Lenin	k1gMnSc1	Lenin
představil	představit	k5eAaPmAgMnS	představit
několik	několik	k4yIc4	několik
návrhů	návrh	k1gInPc2	návrh
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
upevnil	upevnit	k5eAaPmAgInS	upevnit
svou	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
<g/>
:	:	kIx,	:
Dekret	dekret	k1gInSc4	dekret
o	o	k7c4	o
míru	míra	k1gFnSc4	míra
<g/>
,	,	kIx,	,
Dekret	dekret	k1gInSc4	dekret
o	o	k7c6	o
půdě	půda	k1gFnSc6	půda
a	a	k8xC	a
složení	složení	k1gNnSc6	složení
nové	nový	k2eAgFnSc2d1	nová
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
podobný	podobný	k2eAgInSc1d1	podobný
dřívějším	dřívější	k2eAgInSc7d1	dřívější
návrhům	návrh	k1gInPc3	návrh
ruských	ruský	k2eAgMnPc2d1	ruský
defensistů	defensista	k1gMnPc2	defensista
<g/>
,	,	kIx,	,
požadoval	požadovat	k5eAaImAgMnS	požadovat
příměří	příměří	k1gNnSc4	příměří
a	a	k8xC	a
zahájení	zahájení	k1gNnSc4	zahájení
jednání	jednání	k1gNnSc2	jednání
o	o	k7c6	o
okamžitém	okamžitý	k2eAgNnSc6d1	okamžité
míru	mír	k1gInSc6	mír
mezi	mezi	k7c7	mezi
válčícími	válčící	k2eAgFnPc7d1	válčící
zeměmi	zem	k1gFnPc7	zem
bez	bez	k7c2	bez
reparací	reparace	k1gFnPc2	reparace
a	a	k8xC	a
anexí	anexe	k1gFnPc2	anexe
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
bolševikům	bolševik	k1gMnPc3	bolševik
získat	získat	k5eAaPmF	získat
zejména	zejména	k9	zejména
sympatie	sympatie	k1gFnPc4	sympatie
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Dekret	dekret	k1gInSc1	dekret
o	o	k7c6	o
půdě	půda	k1gFnSc6	půda
legalizoval	legalizovat	k5eAaBmAgMnS	legalizovat
již	již	k9	již
probíhající	probíhající	k2eAgNnSc4d1	probíhající
zabírání	zabírání	k1gNnSc4	zabírání
půdy	půda	k1gFnSc2	půda
aristokracie	aristokracie	k1gFnSc2	aristokracie
a	a	k8xC	a
kulaků	kulak	k1gMnPc2	kulak
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
půdy	půda	k1gFnSc2	půda
bylo	být	k5eAaImAgNnS	být
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
a	a	k8xC	a
půda	půda	k1gFnSc1	půda
zabraná	zabraný	k2eAgFnSc1d1	zabraná
sověty	sovět	k1gInPc7	sovět
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
rolníkům	rolník	k1gMnPc3	rolník
<g/>
.	.	kIx.	.
</s>
<s>
Dekret	dekret	k1gInSc1	dekret
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
programu	program	k1gInSc6	program
levých	levý	k2eAgMnPc2d1	levý
eserů	eser	k1gMnPc2	eser
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
bolševikům	bolševik	k1gMnPc3	bolševik
získat	získat	k5eAaPmF	získat
jejich	jejich	k3xOp3gFnSc4	jejich
podporu	podpora	k1gFnSc4	podpora
a	a	k8xC	a
také	také	k9	také
legitimizovat	legitimizovat	k5eAaBmF	legitimizovat
novou	nový	k2eAgFnSc4d1	nová
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
rolníků	rolník	k1gMnPc2	rolník
<g/>
.	.	kIx.	.
<g/>
Před	před	k7c7	před
ukončením	ukončení	k1gNnSc7	ukončení
sjezd	sjezd	k1gInSc1	sjezd
schválil	schválit	k5eAaPmAgInS	schválit
novou	nový	k2eAgFnSc4d1	nová
vládu	vláda	k1gFnSc4	vláda
a	a	k8xC	a
nový	nový	k2eAgInSc1d1	nový
Všeruský	všeruský	k2eAgInSc1d1	všeruský
ústřední	ústřední	k2eAgInSc1d1	ústřední
výkonný	výkonný	k2eAgInSc1d1	výkonný
výbor	výbor	k1gInSc1	výbor
(	(	kIx(	(
<g/>
ÚVV	ÚVV	kA	ÚVV
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nový	Nový	k1gMnSc1	Nový
ÚVV	ÚVV	kA	ÚVV
byl	být	k5eAaImAgMnS	být
veden	vést	k5eAaImNgMnS	vést
umírněným	umírněný	k2eAgMnSc7d1	umírněný
bolševikem	bolševik	k1gMnSc7	bolševik
Kameněvem	Kameněv	k1gInSc7	Kameněv
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
složen	složit	k5eAaPmNgMnS	složit
z	z	k7c2	z
62	[number]	k4	62
bolševiků	bolševik	k1gMnPc2	bolševik
<g/>
,	,	kIx,	,
29	[number]	k4	29
levých	levý	k2eAgMnPc2d1	levý
eserů	eser	k1gMnPc2	eser
<g/>
,	,	kIx,	,
6	[number]	k4	6
menševiků	menševik	k1gMnPc2	menševik
internacionalistů	internacionalista	k1gMnPc2	internacionalista
a	a	k8xC	a
4	[number]	k4	4
členů	člen	k1gMnPc2	člen
malých	malý	k2eAgFnPc2d1	malá
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Triumfovala	triumfovat	k5eAaBmAgFnS	triumfovat
tak	tak	k9	tak
strategie	strategie	k1gFnSc1	strategie
skupiny	skupina	k1gFnSc2	skupina
bolševických	bolševický	k2eAgMnPc2d1	bolševický
vůdců	vůdce	k1gMnPc2	vůdce
typu	typ	k1gInSc2	typ
Trockého	Trockého	k2eAgMnPc1d1	Trockého
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
názorově	názorově	k6eAd1	názorově
uprostřed	uprostřed	k6eAd1	uprostřed
mezi	mezi	k7c7	mezi
leninisty	leninista	k1gMnPc7	leninista
<g/>
,	,	kIx,	,
požadujícími	požadující	k2eAgMnPc7d1	požadující
okamžité	okamžitý	k2eAgNnSc4d1	okamžité
bolševické	bolševický	k2eAgNnSc4d1	bolševické
povstání	povstání	k1gNnSc4	povstání
<g/>
,	,	kIx,	,
a	a	k8xC	a
umírněnými	umírněný	k2eAgFnPc7d1	umírněná
<g/>
,	,	kIx,	,
upřednostňujícími	upřednostňující	k2eAgFnPc7d1	upřednostňující
vytvoření	vytvoření	k1gNnSc3	vytvoření
socialistické	socialistický	k2eAgFnSc2d1	socialistická
vlády	vláda	k1gFnSc2	vláda
bez	bez	k7c2	bez
liberálů	liberál	k1gMnPc2	liberál
<g/>
;	;	kIx,	;
Trockého	Trockého	k2eAgFnSc7d1	Trockého
strategií	strategie	k1gFnSc7	strategie
bylo	být	k5eAaImAgNnS	být
využít	využít	k5eAaPmF	využít
sověty	sovět	k1gInPc4	sovět
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc4	jejich
prestiž	prestiž	k1gFnSc1	prestiž
a	a	k8xC	a
jim	on	k3xPp3gInPc3	on
podřízené	podřízený	k2eAgFnPc1d1	podřízená
organizace	organizace	k1gFnPc1	organizace
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
bolševici	bolševik	k1gMnPc1	bolševik
mohli	moct	k5eAaImAgMnP	moct
svrhnout	svrhnout	k5eAaPmF	svrhnout
Prozatímní	prozatímní	k2eAgFnSc4d1	prozatímní
vládu	vláda	k1gFnSc4	vláda
a	a	k8xC	a
chopit	chopit	k5eAaPmF	chopit
se	se	k3xPyFc4	se
moci	moct	k5eAaImF	moct
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
5	[number]	k4	5
hodin	hodina	k1gFnPc2	hodina
ráno	ráno	k6eAd1	ráno
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
sjezd	sjezd	k1gInSc1	sjezd
schválil	schválit	k5eAaPmAgInS	schválit
Radu	rada	k1gFnSc4	rada
lidových	lidový	k2eAgMnPc2d1	lidový
komisařů	komisař	k1gMnPc2	komisař
jako	jako	k8xC	jako
základ	základ	k1gInSc1	základ
nové	nový	k2eAgFnSc2d1	nová
vlády	vláda	k1gFnSc2	vláda
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
sejde	sejít	k5eAaPmIp3nS	sejít
Ústavodárné	ústavodárný	k2eAgNnSc1d1	Ústavodárné
shromáždění	shromáždění	k1gNnSc1	shromáždění
<g/>
.	.	kIx.	.
<g/>
Všichni	všechen	k3xTgMnPc1	všechen
noví	nový	k2eAgMnPc1d1	nový
komisaři	komisar	k1gMnPc1	komisar
byli	být	k5eAaImAgMnP	být
členové	člen	k1gMnPc1	člen
bolševické	bolševický	k2eAgFnSc2d1	bolševická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
leví	levý	k2eAgMnPc1d1	levý
eseři	eser	k1gMnPc1	eser
nakonec	nakonec	k6eAd1	nakonec
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
připojit	připojit	k5eAaPmF	připojit
se	se	k3xPyFc4	se
k	k	k7c3	k
nové	nový	k2eAgFnSc3d1	nová
vládě	vláda	k1gFnSc3	vláda
bez	bez	k7c2	bez
dalších	další	k2eAgFnPc2d1	další
socialistických	socialistický	k2eAgFnPc2d1	socialistická
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Novému	nový	k2eAgInSc3d1	nový
Sovnarkomu	sovnarkom	k1gInSc3	sovnarkom
předsedal	předsedat	k5eAaImAgMnS	předsedat
Lenin	Lenin	k1gMnSc1	Lenin
<g/>
,	,	kIx,	,
komisariát	komisariát	k1gInSc1	komisariát
zahraničí	zahraničí	k1gNnSc2	zahraničí
obsadil	obsadit	k5eAaPmAgInS	obsadit
Trockij	Trockij	k1gMnPc4	Trockij
<g/>
,	,	kIx,	,
školství	školství	k1gNnSc4	školství
Lunačarskij	Lunačarskij	k1gFnSc2	Lunačarskij
<g/>
,	,	kIx,	,
vnitra	vnitro	k1gNnSc2	vnitro
Rykov	Rykovo	k1gNnPc2	Rykovo
<g/>
,	,	kIx,	,
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
průmyslu	průmysl	k1gInSc2	průmysl
Nogin	Nogina	k1gFnPc2	Nogina
<g/>
,	,	kIx,	,
práce	práce	k1gFnSc1	práce
Šljapnikov	Šljapnikov	k1gInSc1	Šljapnikov
<g/>
,	,	kIx,	,
zemědělství	zemědělství	k1gNnSc1	zemědělství
Miljutin	Miljutina	k1gFnPc2	Miljutina
<g/>
,	,	kIx,	,
financí	finance	k1gFnPc2	finance
Skvorcov	Skvorcovo	k1gNnPc2	Skvorcovo
<g/>
,	,	kIx,	,
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
Lomov	Lomovo	k1gNnPc2	Lomovo
<g/>
,	,	kIx,	,
zásobování	zásobování	k1gNnSc1	zásobování
Teodorovič	Teodorovič	k1gInSc1	Teodorovič
<g/>
,	,	kIx,	,
pošt	pošta	k1gFnPc2	pošta
a	a	k8xC	a
telegrafů	telegraf	k1gInPc2	telegraf
Avilov	Avilov	k1gInSc1	Avilov
<g/>
,	,	kIx,	,
národností	národnost	k1gFnPc2	národnost
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
,	,	kIx,	,
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
byly	být	k5eAaImAgFnP	být
vedeny	vést	k5eAaImNgFnP	vést
triumvirátem	triumvirát	k1gInSc7	triumvirát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
tvořili	tvořit	k5eAaImAgMnP	tvořit
Antonov-Ovsejenko	Antonov-Ovsejenka	k1gFnSc5	Antonov-Ovsejenka
<g/>
,	,	kIx,	,
Dybenko	Dybenka	k1gFnSc5	Dybenka
a	a	k8xC	a
Krylenko	Krylenka	k1gFnSc5	Krylenka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zvolení	zvolení	k1gNnSc6	zvolení
nových	nový	k2eAgInPc2d1	nový
řídících	řídící	k2eAgInPc2d1	řídící
orgánů	orgán	k1gInPc2	orgán
státu	stát	k1gInSc2	stát
byl	být	k5eAaImAgInS	být
Druhý	druhý	k4xOgInSc1	druhý
sjezd	sjezd	k1gInSc1	sjezd
sovětů	sovět	k1gInPc2	sovět
ukončen	ukončit	k5eAaPmNgInS	ukončit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Boje	boj	k1gInPc1	boj
o	o	k7c4	o
Petrohrad	Petrohrad	k1gInSc4	Petrohrad
a	a	k8xC	a
rozhovory	rozhovor	k1gInPc4	rozhovor
socialistů	socialist	k1gMnPc2	socialist
===	===	k?	===
</s>
</p>
<p>
<s>
I	i	k9	i
když	když	k8xS	když
bolševici	bolševik	k1gMnPc1	bolševik
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInPc4d3	veliký
střety	střet	k1gInPc4	střet
mezi	mezi	k7c7	mezi
zastánci	zastánce	k1gMnPc7	zastánce
a	a	k8xC	a
protivníky	protivník	k1gMnPc7	protivník
revoluce	revoluce	k1gFnSc2	revoluce
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
pěti	pět	k4xCc6	pět
dnech	den	k1gInPc6	den
<g/>
.	.	kIx.	.
</s>
<s>
Takzvané	takzvaný	k2eAgNnSc4d1	takzvané
povstání	povstání	k1gNnSc4	povstání
junkerů	junker	k1gMnPc2	junker
proti	proti	k7c3	proti
nové	nový	k2eAgFnSc3d1	nová
vládě	vláda	k1gFnSc3	vláda
ráno	ráno	k6eAd1	ráno
29	[number]	k4	29
<g/>
.	.	kIx.	.
říjnajul	říjnajout	k5eAaPmAgInS	říjnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
bylo	být	k5eAaImAgNnS	být
odhaleno	odhalit	k5eAaPmNgNnS	odhalit
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
koordinovat	koordinovat	k5eAaBmF	koordinovat
s	s	k7c7	s
povstáním	povstání	k1gNnSc7	povstání
Kerenského-Krasova	Kerenského-Krasův	k2eAgInSc2d1	Kerenského-Krasův
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
útokem	útok	k1gInSc7	útok
jednotek	jednotka	k1gFnPc2	jednotka
věrných	věrný	k2eAgFnPc2d1	věrná
svržené	svržený	k2eAgFnSc3d1	svržená
Prozatímní	prozatímní	k2eAgFnSc3d1	prozatímní
vládě	vláda	k1gFnSc3	vláda
na	na	k7c4	na
Petrohrad	Petrohrad	k1gInSc4	Petrohrad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
ukončení	ukončení	k1gNnSc2	ukončení
Druhého	druhý	k4xOgMnSc2	druhý
sjezdu	sjezd	k1gInSc2	sjezd
sovětů	sovět	k1gInPc2	sovět
<g/>
,	,	kIx,	,
27	[number]	k4	27
<g/>
.	.	kIx.	.
říjnajul	říjnajout	k5eAaPmAgInS	říjnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Lenin	Lenin	k1gMnSc1	Lenin
ve	v	k7c4	v
dvě	dva	k4xCgFnPc4	dva
hodiny	hodina	k1gFnPc4	hodina
ráno	ráno	k6eAd1	ráno
sešel	sejít	k5eAaPmAgMnS	sejít
s	s	k7c7	s
Petrohradským	petrohradský	k2eAgInSc7d1	petrohradský
vojenským	vojenský	k2eAgInSc7d1	vojenský
revolučním	revoluční	k2eAgInSc7d1	revoluční
výborem	výbor	k1gInSc7	výbor
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
řídil	řídit	k5eAaImAgInS	řídit
obranu	obrana	k1gFnSc4	obrana
města	město	k1gNnSc2	město
před	před	k7c7	před
nečetnými	četný	k2eNgFnPc7d1	nečetná
kozáckými	kozácký	k2eAgFnPc7d1	kozácká
jednotkami	jednotka	k1gFnPc7	jednotka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
Kerenskému	Kerenský	k2eAgInSc3d1	Kerenský
podařilo	podařit	k5eAaPmAgNnS	podařit
sehnat	sehnat	k5eAaPmF	sehnat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
táhly	táhnout	k5eAaImAgInP	táhnout
na	na	k7c4	na
Petrohrad	Petrohrad	k1gInSc4	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
slabost	slabost	k1gFnSc4	slabost
postupovaly	postupovat	k5eAaImAgFnP	postupovat
bez	bez	k7c2	bez
odporu	odpor	k1gInSc2	odpor
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
již	již	k6eAd1	již
v	v	k7c6	v
Gačině	Gačina	k1gFnSc6	Gačina
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
své	svůj	k3xOyFgNnSc4	svůj
úsilí	úsilí	k1gNnSc1	úsilí
nebylo	být	k5eNaImAgNnS	být
ruské	ruský	k2eAgNnSc1d1	ruské
vrchní	vrchní	k2eAgNnSc1d1	vrchní
velení	velení	k1gNnSc1	velení
schopno	schopen	k2eAgNnSc1d1	schopno
několika	několik	k4yIc7	několik
stovkám	stovka	k1gFnPc3	stovka
Kerenského	Kerenský	k2eAgInSc2d1	Kerenský
kozáků	kozák	k1gInPc2	kozák
poskytnout	poskytnout	k5eAaPmF	poskytnout
posily	posila	k1gFnPc4	posila
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
bojů	boj	k1gInPc2	boj
o	o	k7c4	o
ovládnutí	ovládnutí	k1gNnSc4	ovládnutí
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
a	a	k8xC	a
Moskvy	Moskva	k1gFnSc2	Moskva
Národní	národní	k2eAgNnSc1d1	národní
železniční	železniční	k2eAgInPc1d1	železniční
odbory	odbor	k1gInPc1	odbor
(	(	kIx(	(
<g/>
Vikžel	Vikžel	k1gFnPc1	Vikžel
<g/>
)	)	kIx)	)
přinutily	přinutit	k5eAaPmAgFnP	přinutit
různé	různý	k2eAgFnPc1d1	různá
socialistické	socialistický	k2eAgFnPc1d1	socialistická
strany	strana	k1gFnPc1	strana
k	k	k7c3	k
vyjednávání	vyjednávání	k1gNnSc3	vyjednávání
o	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
koaliční	koaliční	k2eAgFnSc2d1	koaliční
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
podporovalo	podporovat	k5eAaImAgNnS	podporovat
mnoho	mnoho	k4c1	mnoho
členů	člen	k1gInPc2	člen
Ispolkomu	Ispolkom	k1gInSc2	Ispolkom
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
umírněného	umírněný	k2eAgInSc2d1	umírněný
bolševického	bolševický	k2eAgInSc2d1	bolševický
proudu	proud	k1gInSc2	proud
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Kameněvem	Kameněv	k1gInSc7	Kameněv
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
leví	levý	k2eAgMnPc1d1	levý
eseři	eser	k1gMnPc1	eser
nebo	nebo	k8xC	nebo
Martovovi	Martovův	k2eAgMnPc1d1	Martovův
menševici	menševik	k1gMnPc1	menševik
internacionalisté	internacionalista	k1gMnPc1	internacionalista
<g/>
.	.	kIx.	.
</s>
<s>
Strany	strana	k1gFnPc1	strana
čelily	čelit	k5eAaImAgFnP	čelit
hrozbě	hrozba	k1gFnSc3	hrozba
ochromení	ochromení	k1gNnSc6	ochromení
železnic	železnice	k1gFnPc2	železnice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepodařilo	podařit	k5eNaPmAgNnS	podařit
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
příměří	příměří	k1gNnSc4	příměří
mezi	mezi	k7c7	mezi
znepřátelenými	znepřátelený	k2eAgFnPc7d1	znepřátelená
stranami	strana	k1gFnPc7	strana
<g/>
;	;	kIx,	;
menševici	menševik	k1gMnPc1	menševik
a	a	k8xC	a
eseři	eser	k1gMnPc1	eser
požadovali	požadovat	k5eAaImAgMnP	požadovat
vládu	vláda	k1gFnSc4	vláda
bez	bez	k7c2	bez
Lenina	Lenin	k1gMnSc2	Lenin
a	a	k8xC	a
Trockého	Trocký	k1gMnSc2	Trocký
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
nepřítomnosti	nepřítomnost	k1gFnSc2	nepřítomnost
některých	některý	k3yIgMnPc2	některý
členů	člen	k1gMnPc2	člen
bolševického	bolševický	k2eAgInSc2d1	bolševický
ústředního	ústřední	k2eAgInSc2d1	ústřední
výboru	výbor	k1gInSc2	výbor
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
válečných	válečný	k2eAgFnPc2d1	válečná
povinností	povinnost	k1gFnPc2	povinnost
umírnění	umírněný	k2eAgMnPc1d1	umírněný
bolševici	bolševik	k1gMnPc1	bolševik
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
schválili	schválit	k5eAaPmAgMnP	schválit
podmínky	podmínka	k1gFnPc4	podmínka
opozičních	opoziční	k2eAgFnPc2d1	opoziční
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
vyslali	vyslat	k5eAaPmAgMnP	vyslat
delegáty	delegát	k1gMnPc4	delegát
umírněného	umírněný	k2eAgInSc2d1	umírněný
směru	směr	k1gInSc2	směr
na	na	k7c6	na
jednání	jednání	k1gNnSc6	jednání
<g/>
,	,	kIx,	,
kterých	který	k3yQgFnPc2	který
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
zhruba	zhruba	k6eAd1	zhruba
třicet	třicet	k4xCc1	třicet
socialistů	socialist	k1gMnPc2	socialist
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
byli	být	k5eAaImAgMnP	být
zpočátku	zpočátku	k6eAd1	zpočátku
neoblomní	oblomný	k2eNgMnPc1d1	neoblomný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
věřili	věřit	k5eAaImAgMnP	věřit
v	v	k7c4	v
rychlé	rychlý	k2eAgNnSc4d1	rychlé
svržení	svržení	k1gNnSc4	svržení
bolševiků	bolševik	k1gMnPc2	bolševik
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neúspěchy	neúspěch	k1gInPc1	neúspěch
vojenských	vojenský	k2eAgFnPc2d1	vojenská
akcí	akce	k1gFnPc2	akce
je	on	k3xPp3gMnPc4	on
přiměly	přimět	k5eAaPmAgInP	přimět
k	k	k7c3	k
větší	veliký	k2eAgFnSc3d2	veliký
vstřícnosti	vstřícnost	k1gFnSc3	vstřícnost
<g/>
.	.	kIx.	.
</s>
<s>
Bolševičtí	bolševický	k2eAgMnPc1d1	bolševický
delegáti	delegát	k1gMnPc1	delegát
byli	být	k5eAaImAgMnP	být
ochotni	ochoten	k2eAgMnPc1d1	ochoten
přijmout	přijmout	k5eAaPmF	přijmout
nové	nový	k2eAgInPc4d1	nový
požadavky	požadavek	k1gInPc4	požadavek
opozice	opozice	k1gFnSc2	opozice
<g/>
:	:	kIx,	:
rozšíření	rozšíření	k1gNnSc1	rozšíření
Ispolkomu	Ispolkom	k1gInSc2	Ispolkom
a	a	k8xC	a
konec	konec	k1gInSc4	konec
sporů	spor	k1gInPc2	spor
<g/>
;	;	kIx,	;
ale	ale	k8xC	ale
Lenin	Lenin	k1gMnSc1	Lenin
dal	dát	k5eAaPmAgMnS	dát
Sovnarkomu	sovnarkom	k1gInSc3	sovnarkom
právo	právo	k1gNnSc4	právo
vládnout	vládnout	k5eAaImF	vládnout
pomocí	pomocí	k7c2	pomocí
dekretů	dekret	k1gInPc2	dekret
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
novém	nový	k2eAgNnSc6d1	nové
zasedání	zasedání	k1gNnSc6	zasedání
ústředního	ústřední	k2eAgInSc2d1	ústřední
výboru	výbor	k1gInSc2	výbor
dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadujul	listopadujout	k5eAaPmAgInS	listopadujout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
s	s	k7c7	s
Leninem	Lenin	k1gMnSc7	Lenin
a	a	k8xC	a
Trockým	Trocký	k1gMnSc7	Trocký
opět	opět	k6eAd1	opět
přítomnými	přítomný	k1gMnPc7	přítomný
výbor	výbor	k1gInSc1	výbor
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
Leninův	Leninův	k2eAgInSc4d1	Leninův
návrh	návrh	k1gInSc4	návrh
okamžitě	okamžitě	k6eAd1	okamžitě
přerušit	přerušit	k5eAaPmF	přerušit
rozhovory	rozhovor	k1gInPc4	rozhovor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
schválil	schválit	k5eAaPmAgInS	schválit
návrh	návrh	k1gInSc1	návrh
Trockého	Trockého	k2eAgInSc1d1	Trockého
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
pouze	pouze	k6eAd1	pouze
jedno	jeden	k4xCgNnSc4	jeden
další	další	k2eAgNnSc4d1	další
zasedání	zasedání	k1gNnSc4	zasedání
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
by	by	kYmCp3nS	by
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
zbytečnost	zbytečnost	k1gFnSc4	zbytečnost
rozhovorů	rozhovor	k1gInPc2	rozhovor
<g/>
.	.	kIx.	.
</s>
<s>
Lenin	Lenin	k1gMnSc1	Lenin
napadl	napadnout	k5eAaPmAgMnS	napadnout
umírněné	umírněný	k2eAgMnPc4d1	umírněný
členy	člen	k1gMnPc4	člen
a	a	k8xC	a
vyhrožoval	vyhrožovat	k5eAaImAgMnS	vyhrožovat
jim	on	k3xPp3gNnPc3	on
vyloučením	vyloučení	k1gNnPc3	vyloučení
strany	strana	k1gFnSc2	strana
<g/>
;	;	kIx,	;
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
pět	pět	k4xCc4	pět
umírněných	umírněný	k2eAgMnPc2d1	umírněný
členů	člen	k1gMnPc2	člen
ústředního	ústřední	k2eAgInSc2d1	ústřední
výboru	výbor	k1gInSc2	výbor
odstoupilo	odstoupit	k5eAaPmAgNnS	odstoupit
kvůli	kvůli	k7c3	kvůli
Leninovým	Leninův	k2eAgInPc3d1	Leninův
zamítavým	zamítavý	k2eAgInPc3d1	zamítavý
postojům	postoj	k1gInPc3	postoj
vůči	vůči	k7c3	vůči
koalici	koalice	k1gFnSc3	koalice
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
socialistickými	socialistický	k2eAgFnPc7d1	socialistická
stranami	strana	k1gFnPc7	strana
jak	jak	k8xS	jak
ve	v	k7c6	v
výboru	výbor	k1gInSc6	výbor
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
<g/>
,	,	kIx,	,
Důvodem	důvod	k1gInSc7	důvod
rezignací	rezignace	k1gFnPc2	rezignace
byl	být	k5eAaImAgInS	být
strach	strach	k1gInSc1	strach
<g/>
,	,	kIx,	,
že	že	k8xS	že
jediným	jediné	k1gNnSc7	jediné
<g />
.	.	kIx.	.
</s>
<s>
dalším	další	k2eAgInSc7d1	další
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
si	se	k3xPyFc3	se
udržet	udržet	k5eAaPmF	udržet
moc	moc	k6eAd1	moc
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
teror	teror	k1gInSc1	teror
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
rezignovali	rezignovat	k5eAaBmAgMnP	rezignovat
i	i	k9	i
další	další	k2eAgMnPc1d1	další
funkcionáři	funkcionář	k1gMnPc1	funkcionář
včetně	včetně	k7c2	včetně
levých	levý	k2eAgMnPc2d1	levý
eserů	eser	k1gMnPc2	eser
z	z	k7c2	z
VRV	VRV	kA	VRV
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadujul	listopadujout	k5eAaPmAgInS	listopadujout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
<g/>
greg	greg	k1gInSc1	greg
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
poslední	poslední	k2eAgNnSc1d1	poslední
kolo	kolo	k1gNnSc1	kolo
rozhovorů	rozhovor	k1gInPc2	rozhovor
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgNnSc6	který
byli	být	k5eAaImAgMnP	být
přítomni	přítomen	k2eAgMnPc1d1	přítomen
bolševici	bolševik	k1gMnPc1	bolševik
<g/>
;	;	kIx,	;
tehdy	tehdy	k6eAd1	tehdy
bolševici	bolševik	k1gMnPc1	bolševik
prosadili	prosadit	k5eAaPmAgMnP	prosadit
poměrem	poměr	k1gInSc7	poměr
29	[number]	k4	29
hlasů	hlas	k1gInPc2	hlas
ku	k	k7c3	k
23	[number]	k4	23
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ispolkom	Ispolkom	k1gInSc1	Ispolkom
schvaluje	schvalovat	k5eAaImIp3nS	schvalovat
vládnutí	vládnutí	k1gNnSc4	vládnutí
dekrety	dekret	k1gInPc4	dekret
Sovnarkomu	sovnarkom	k1gInSc2	sovnarkom
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odeznění	odeznění	k1gNnSc6	odeznění
vojenské	vojenský	k2eAgFnSc2d1	vojenská
hrozby	hrozba	k1gFnSc2	hrozba
vůči	vůči	k7c3	vůči
hlavnímu	hlavní	k2eAgNnSc3d1	hlavní
městu	město	k1gNnSc3	město
a	a	k8xC	a
s	s	k7c7	s
Leninem	Lenin	k1gMnSc7	Lenin
a	a	k8xC	a
Trockým	Trocký	k1gMnPc3	Trocký
zpět	zpět	k6eAd1	zpět
v	v	k7c6	v
ústředním	ústřední	k2eAgInSc6d1	ústřední
výboru	výbor	k1gInSc6	výbor
se	se	k3xPyFc4	se
bolševická	bolševický	k2eAgFnSc1d1	bolševická
pozice	pozice	k1gFnSc1	pozice
zatvrdila	zatvrdit	k5eAaPmAgFnS	zatvrdit
a	a	k8xC	a
jednání	jednání	k1gNnPc1	jednání
selhala	selhat	k5eAaPmAgNnP	selhat
<g/>
.	.	kIx.	.
<g/>
Když	když	k8xS	když
Lenin	Lenin	k1gMnSc1	Lenin
ovládl	ovládnout	k5eAaPmAgMnS	ovládnout
Sovnarkom	sovnarkom	k1gInSc4	sovnarkom
<g/>
,	,	kIx,	,
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
<g/>
,	,	kIx,	,
že	že	k8xS	že
učiní	učinit	k5eAaImIp3nS	učinit
ústupky	ústupek	k1gInPc4	ústupek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
znovu	znovu	k6eAd1	znovu
získal	získat	k5eAaPmAgMnS	získat
spojenectví	spojenectví	k1gNnSc4	spojenectví
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
stranami	strana	k1gFnPc7	strana
<g/>
:	:	kIx,	:
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
týdnech	týden	k1gInPc6	týden
rozhovorů	rozhovor	k1gInPc2	rozhovor
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosincejul	prosincejout	k5eAaPmAgInS	prosincejout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1917	[number]	k4	1917
<g/>
greg	greg	k1gInSc1	greg
<g/>
.	.	kIx.	.
leví	levý	k2eAgMnPc1d1	levý
eseři	eser	k1gMnPc1	eser
do	do	k7c2	do
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
získali	získat	k5eAaPmAgMnP	získat
tři	tři	k4xCgMnPc4	tři
komisaře	komisař	k1gMnPc4	komisař
(	(	kIx(	(
<g/>
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
,	,	kIx,	,
pošty	pošta	k1gFnSc2	pošta
a	a	k8xC	a
telegrafů	telegraf	k1gInPc2	telegraf
a	a	k8xC	a
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
dohoda	dohoda	k1gFnSc1	dohoda
způsobila	způsobit	k5eAaPmAgFnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bolševici	bolševik	k1gMnPc1	bolševik
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
předtím	předtím	k6eAd1	předtím
odstoupili	odstoupit	k5eAaPmAgMnP	odstoupit
kvůli	kvůli	k7c3	kvůli
neúspěchu	neúspěch	k1gInSc3	neúspěch
koaličních	koaliční	k2eAgInPc2d1	koaliční
rozhovorů	rozhovor	k1gInPc2	rozhovor
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
vrátili	vrátit	k5eAaPmAgMnP	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Koalice	koalice	k1gFnSc1	koalice
byla	být	k5eAaImAgFnS	být
zachována	zachovat	k5eAaPmNgFnS	zachovat
až	až	k6eAd1	až
do	do	k7c2	do
přijetí	přijetí	k1gNnSc2	přijetí
brestlitevské	brestlitevský	k2eAgFnSc2d1	brestlitevský
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
leví	levý	k2eAgMnPc1d1	levý
eseři	eser	k1gMnPc1	eser
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
konečná	konečný	k2eAgFnSc1d1	konečná
konfrontace	konfrontace	k1gFnSc1	konfrontace
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
stranami	strana	k1gFnPc7	strana
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
až	až	k9	až
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1918.14	[number]	k4	1918.14
<g/>
.	.	kIx.	.
listopadujul	listopadujout	k5eAaPmAgInS	listopadujout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
<g/>
greg	greg	k1gInSc1	greg
<g/>
.	.	kIx.	.
zbytky	zbytek	k1gInPc1	zbytek
Všeruského	všeruský	k2eAgInSc2d1	všeruský
svazu	svaz	k1gInSc2	svaz
rolnických	rolnický	k2eAgInPc2d1	rolnický
sovětů	sovět	k1gInPc2	sovět
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgMnSc4	jenž
odstoupili	odstoupit	k5eAaPmAgMnP	odstoupit
umírnění	umírněný	k2eAgMnPc1d1	umírněný
socialisté	socialist	k1gMnPc1	socialist
<g/>
,	,	kIx,	,
schválily	schválit	k5eAaPmAgInP	schválit
sjednocení	sjednocení	k1gNnSc4	sjednocení
s	s	k7c7	s
Všeruským	všeruský	k2eAgInSc7d1	všeruský
ústředním	ústřední	k2eAgInSc7d1	ústřední
výkonným	výkonný	k2eAgInSc7d1	výkonný
výborem	výbor	k1gInSc7	výbor
sovětů	sovět	k1gInPc2	sovět
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
dělníků	dělník	k1gMnPc2	dělník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Šíření	šíření	k1gNnSc4	šíření
revoluce	revoluce	k1gFnPc1	revoluce
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
industrializovaných	industrializovaný	k2eAgNnPc6d1	industrializované
městech	město	k1gNnPc6	město
a	a	k8xC	a
v	v	k7c6	v
lokalitách	lokalita	k1gFnPc6	lokalita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
radikální	radikální	k2eAgFnPc1d1	radikální
levicové	levicový	k2eAgFnPc1d1	levicová
strany	strana	k1gFnPc1	strana
ovládaly	ovládat	k5eAaImAgFnP	ovládat
místní	místní	k2eAgInPc4d1	místní
sověty	sovět	k1gInPc4	sovět
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
přesun	přesun	k1gInSc1	přesun
moci	moc	k1gFnSc2	moc
proveden	provést	k5eAaPmNgInS	provést
rychle	rychle	k6eAd1	rychle
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
síly	síla	k1gFnPc1	síla
potřebné	potřebný	k2eAgFnPc1d1	potřebná
k	k	k7c3	k
jeho	jeho	k3xOp3gNnPc3	jeho
zajištění	zajištění	k1gNnSc3	zajištění
poskytly	poskytnout	k5eAaPmAgFnP	poskytnout
místní	místní	k2eAgFnPc1d1	místní
jednotky	jednotka	k1gFnPc1	jednotka
rudých	rudý	k2eAgFnPc2d1	rudá
gard	garda	k1gFnPc2	garda
nebo	nebo	k8xC	nebo
vojáků	voják	k1gMnPc2	voják
posádky	posádka	k1gFnSc2	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vzorec	vzorec	k1gInSc4	vzorec
sledovala	sledovat	k5eAaImAgFnS	sledovat
především	především	k6eAd1	především
industrializovaná	industrializovaný	k2eAgNnPc4d1	industrializované
města	město	k1gNnPc4	město
severně	severně	k6eAd1	severně
a	a	k8xC	a
východně	východně	k6eAd1	východně
od	od	k7c2	od
Moskvy	Moskva	k1gFnSc2	Moskva
a	a	k8xC	a
na	na	k7c6	na
Uralu	Ural	k1gInSc6	Ural
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgNnPc6d1	jiné
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
značná	značný	k2eAgFnSc1d1	značná
opozice	opozice	k1gFnSc1	opozice
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
na	na	k7c6	na
středním	střední	k2eAgInSc6d1	střední
toku	tok	k1gInSc6	tok
Volhy	Volha	k1gFnSc2	Volha
<g/>
,	,	kIx,	,
v	v	k7c6	v
samotné	samotný	k2eAgFnSc6d1	samotná
Moskvě	Moskva	k1gFnSc6	Moskva
nebo	nebo	k8xC	nebo
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
jí	on	k3xPp3gFnSc3	on
blízkých	blízký	k2eAgInPc6d1	blízký
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
uznání	uznání	k1gNnSc1	uznání
nové	nový	k2eAgFnSc2d1	nová
vlády	vláda	k1gFnSc2	vláda
pomalejší	pomalý	k2eAgMnSc1d2	pomalejší
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
trvalo	trvat	k5eAaImAgNnS	trvat
téměř	téměř	k6eAd1	téměř
jeden	jeden	k4xCgInSc1	jeden
týden	týden	k1gInSc1	týden
od	od	k7c2	od
událostí	událost	k1gFnPc2	událost
v	v	k7c6	v
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
<g/>
.	.	kIx.	.
<g/>
Několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
po	po	k7c6	po
<g />
.	.	kIx.	.
</s>
<s>
převzetí	převzetí	k1gNnSc1	převzetí
moci	moc	k1gFnSc2	moc
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
uznal	uznat	k5eAaPmAgInS	uznat
novou	nový	k2eAgFnSc4d1	nová
vládu	vláda	k1gFnSc4	vláda
okolní	okolní	k2eAgInSc4d1	okolní
region	region	k1gInSc4	region
<g/>
:	:	kIx,	:
jak	jak	k6eAd1	jak
1	[number]	k4	1
<g/>
.	.	kIx.	.
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
baltští	baltský	k2eAgMnPc1d1	baltský
námořníci	námořník	k1gMnPc1	námořník
<g/>
,	,	kIx,	,
týl	týl	k1gInSc1	týl
severní	severní	k2eAgFnSc2d1	severní
fronty	fronta	k1gFnSc2	fronta
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
západní	západní	k2eAgFnSc2d1	západní
fronty	fronta	k1gFnSc2	fronta
a	a	k8xC	a
části	část	k1gFnSc2	část
Estonska	Estonsko	k1gNnSc2	Estonsko
a	a	k8xC	a
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
získal	získat	k5eAaPmAgInS	získat
bezpečí	bezpečí	k1gNnSc4	bezpečí
a	a	k8xC	a
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
podporu	podpora	k1gFnSc4	podpora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
listopadu	listopad	k1gInSc2	listopad
s	s	k7c7	s
konečným	konečný	k2eAgNnSc7d1	konečné
ovládnutím	ovládnutí	k1gNnSc7	ovládnutí
Moskvy	Moskva	k1gFnSc2	Moskva
Sovnarkom	sovnarkom	k1gInSc1	sovnarkom
ovládal	ovládat	k5eAaImAgInS	ovládat
pás	pás	k1gInSc1	pás
území	území	k1gNnSc2	území
od	od	k7c2	od
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
přes	přes	k7c4	přes
Volhu	Volha	k1gFnSc4	Volha
až	až	k9	až
po	po	k7c4	po
Ural	Ural	k1gInSc4	Ural
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
stále	stále	k6eAd1	stále
existovaly	existovat	k5eAaImAgFnP	existovat
izolované	izolovaný	k2eAgFnPc4d1	izolovaná
oblasti	oblast	k1gFnPc4	oblast
odporu	odpor	k1gInSc2	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
skončila	skončit	k5eAaPmAgFnS	skončit
první	první	k4xOgFnSc1	první
fáze	fáze	k1gFnSc1	fáze
rozšiřování	rozšiřování	k1gNnSc2	rozšiřování
moci	moc	k1gFnSc2	moc
bolševické	bolševický	k2eAgFnSc2d1	bolševická
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
obnovilo	obnovit	k5eAaPmAgNnS	obnovit
jen	jen	k9	jen
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
získání	získání	k1gNnSc6	získání
moci	moc	k1gFnSc2	moc
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
místní	místní	k2eAgInSc1d1	místní
sovět	sovět	k1gInSc1	sovět
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
bolševiků	bolševik	k1gMnPc2	bolševik
a	a	k8xC	a
levých	levý	k2eAgMnPc2d1	levý
eserů	eser	k1gMnPc2	eser
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
druhá	druhý	k4xOgFnSc1	druhý
fáze	fáze	k1gFnSc1	fáze
rozšiřování	rozšiřování	k1gNnSc2	rozšiřování
autority	autorita	k1gFnSc2	autorita
nové	nový	k2eAgFnSc2d1	nová
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
trvala	trvat	k5eAaImAgFnS	trvat
asi	asi	k9	asi
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
poznamenána	poznamenat	k5eAaPmNgFnS	poznamenat
politickými	politický	k2eAgInPc7d1	politický
spory	spor	k1gInPc7	spor
a	a	k8xC	a
vojenskými	vojenský	k2eAgFnPc7d1	vojenská
expedicemi	expedice	k1gFnPc7	expedice
vysílanými	vysílaný	k2eAgFnPc7d1	vysílaná
po	po	k7c6	po
železnici	železnice	k1gFnSc6	železnice
z	z	k7c2	z
Moskvy	Moskva	k1gFnSc2	Moskva
nebo	nebo	k8xC	nebo
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ovládly	ovládnout	k5eAaPmAgInP	ovládnout
nová	nový	k2eAgNnPc4d1	nové
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Boje	boj	k1gInPc1	boj
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
se	se	k3xPyFc4	se
soustředily	soustředit	k5eAaPmAgFnP	soustředit
na	na	k7c6	na
oblasti	oblast	k1gFnSc6	oblast
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
,	,	kIx,	,
na	na	k7c4	na
východní	východní	k2eAgFnSc4d1	východní
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
<g/>
,	,	kIx,	,
na	na	k7c4	na
jihozápadní	jihozápadní	k2eAgFnSc4d1	jihozápadní
frontu	fronta	k1gFnSc4	fronta
a	a	k8xC	a
na	na	k7c6	na
velké	velký	k2eAgFnSc6d1	velká
oblasti	oblast	k1gFnSc6	oblast
Sibiře	Sibiř	k1gFnSc2	Sibiř
a	a	k8xC	a
Střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
<g/>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
disponovala	disponovat	k5eAaBmAgFnS	disponovat
nová	nový	k2eAgFnSc1d1	nová
vláda	vláda	k1gFnSc1	vláda
podporou	podpora	k1gFnSc7	podpora
nebo	nebo	k8xC	nebo
podřízeností	podřízenost	k1gFnSc7	podřízenost
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
kontrolou	kontrola	k1gFnSc7	kontrola
většiny	většina	k1gFnSc2	většina
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
a	a	k8xC	a
provinčních	provinční	k2eAgNnPc2d1	provinční
hlavních	hlavní	k2eAgNnPc2d1	hlavní
měst	město	k1gNnPc2	město
centrální	centrální	k2eAgFnSc2d1	centrální
části	část	k1gFnSc2	část
státu	stát	k1gInSc2	stát
a	a	k8xC	a
teoretickou	teoretický	k2eAgFnSc4d1	teoretická
pravomoc	pravomoc	k1gFnSc4	pravomoc
nad	nad	k7c7	nad
většinou	většina	k1gFnSc7	většina
území	území	k1gNnSc2	území
starého	starý	k2eAgNnSc2d1	staré
impéria	impérium	k1gNnSc2	impérium
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
vliv	vliv	k1gInSc1	vliv
v	v	k7c6	v
odlehlejších	odlehlý	k2eAgFnPc6d2	odlehlejší
provinciích	provincie	k1gFnPc6	provincie
byl	být	k5eAaImAgInS	být
však	však	k9	však
slabý	slabý	k2eAgInSc1d1	slabý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Moskva	Moskva	k1gFnSc1	Moskva
===	===	k?	===
</s>
</p>
<p>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
i	i	k8xC	i
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
byla	být	k5eAaImAgFnS	být
politická	politický	k2eAgFnSc1d1	politická
opozice	opozice	k1gFnSc1	opozice
vůči	vůči	k7c3	vůči
převratu	převrat	k1gInSc3	převrat
vedena	vést	k5eAaImNgFnS	vést
esery	eser	k1gMnPc7	eser
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
boj	boj	k1gInSc1	boj
o	o	k7c4	o
moc	moc	k1gFnSc4	moc
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgMnS	být
mnohem	mnohem	k6eAd1	mnohem
ostřejší	ostrý	k2eAgMnSc1d2	ostřejší
než	než	k8xS	než
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
Boje	boj	k1gInPc1	boj
trvaly	trvat	k5eAaImAgInP	trvat
dlouho	dlouho	k6eAd1	dlouho
a	a	k8xC	a
podle	podle	k7c2	podle
Bucharina	Bucharino	k1gNnSc2	Bucharino
stály	stát	k5eAaImAgFnP	stát
asi	asi	k9	asi
pět	pět	k4xCc4	pět
tisíc	tisíc	k4xCgInPc2	tisíc
životů	život	k1gInPc2	život
<g/>
.	.	kIx.	.
<g/>
Moskevští	moskevský	k2eAgMnPc1d1	moskevský
eseři	eser	k1gMnPc1	eser
<g/>
,	,	kIx,	,
soudržnější	soudržný	k2eAgMnPc1d2	soudržnější
a	a	k8xC	a
konzervativnější	konzervativní	k2eAgMnPc1d2	konzervativnější
i	i	k8xC	i
když	když	k8xS	když
slabší	slabý	k2eAgMnPc1d2	slabší
než	než	k8xS	než
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tvrdě	tvrdě	k6eAd1	tvrdě
postavili	postavit	k5eAaPmAgMnP	postavit
proti	proti	k7c3	proti
bolševickému	bolševický	k2eAgNnSc3d1	bolševické
převzetí	převzetí	k1gNnSc3	převzetí
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Vedl	vést	k5eAaImAgMnS	vést
je	on	k3xPp3gNnSc4	on
starosta	starosta	k1gMnSc1	starosta
města	město	k1gNnSc2	město
Vadim	Vadim	k?	Vadim
Viktorovič	Viktorovič	k1gMnSc1	Viktorovič
Rudněv	Rudněv	k1gMnSc1	Rudněv
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
velkého	velký	k2eAgNnSc2d1	velké
volebního	volební	k2eAgNnSc2d1	volební
vítězství	vítězství	k1gNnSc2	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Bližší	bližší	k1gNnSc4	bližší
liberálům	liberál	k1gMnPc3	liberál
než	než	k8xS	než
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
socialistické	socialistický	k2eAgFnSc6d1	socialistická
revoluční	revoluční	k2eAgFnSc6d1	revoluční
minulosti	minulost	k1gFnSc6	minulost
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Rudněv	Rudněv	k1gFnSc4	Rudněv
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
konzervativních	konzervativní	k2eAgFnPc2d1	konzervativní
stranických	stranický	k2eAgFnPc2d1	stranická
osobností	osobnost	k1gFnPc2	osobnost
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
příznivcem	příznivec	k1gMnSc7	příznivec
koalice	koalice	k1gFnSc2	koalice
s	s	k7c7	s
konstitučními	konstituční	k2eAgMnPc7d1	konstituční
demokraty	demokrat	k1gMnPc7	demokrat
a	a	k8xC	a
podporovatelem	podporovatel	k1gMnSc7	podporovatel
spojenectví	spojenectví	k1gNnSc2	spojenectví
s	s	k7c7	s
Dohodou	dohoda	k1gFnSc7	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgMnPc1d1	místní
bolševici	bolševik	k1gMnPc1	bolševik
nebyli	být	k5eNaImAgMnP	být
připraveni	připravit	k5eAaPmNgMnP	připravit
převzít	převzít	k5eAaPmF	převzít
moc	moc	k6eAd1	moc
<g/>
:	:	kIx,	:
ovládali	ovládat	k5eAaImAgMnP	ovládat
dělnické	dělnický	k2eAgInPc4d1	dělnický
sověty	sovět	k1gInPc4	sovět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
vojáky	voják	k1gMnPc4	voják
<g/>
,	,	kIx,	,
Rudá	rudý	k2eAgFnSc1d1	rudá
garda	garda	k1gFnSc1	garda
také	také	k9	také
nebyla	být	k5eNaImAgFnS	být
připravená	připravený	k2eAgFnSc1d1	připravená
a	a	k8xC	a
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
neexistoval	existovat	k5eNaImAgInS	existovat
vojenský	vojenský	k2eAgInSc1d1	vojenský
revoluční	revoluční	k2eAgInSc1d1	revoluční
výbor	výbor	k1gInSc1	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
hlavní	hlavní	k2eAgMnPc1d1	hlavní
bolševičtí	bolševický	k2eAgMnPc1d1	bolševický
vůdci	vůdce	k1gMnPc1	vůdce
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
,	,	kIx,	,
Alexej	Alexej	k1gMnSc1	Alexej
Rykov	Rykov	k1gInSc1	Rykov
a	a	k8xC	a
Viktor	Viktor	k1gMnSc1	Viktor
Nogin	Nogin	k1gMnSc1	Nogin
<g/>
,	,	kIx,	,
patřili	patřit	k5eAaImAgMnP	patřit
k	k	k7c3	k
umírněnému	umírněný	k2eAgInSc3d1	umírněný
proudu	proud	k1gInSc3	proud
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
převzetí	převzetí	k1gNnSc3	převzetí
moci	moc	k1gFnSc2	moc
stavěl	stavět	k5eAaImAgMnS	stavět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c4	v
předvečer	předvečer	k1gInSc4	předvečer
revoluce	revoluce	k1gFnSc2	revoluce
byla	být	k5eAaImAgFnS	být
moc	moc	k6eAd1	moc
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
mezi	mezi	k7c4	mezi
městskou	městský	k2eAgFnSc4d1	městská
radu	rada	k1gFnSc4	rada
<g/>
,	,	kIx,	,
ovládanou	ovládaný	k2eAgFnSc4d1	ovládaná
esery	eser	k1gMnPc4	eser
<g/>
,	,	kIx,	,
dělnický	dělnický	k2eAgInSc1d1	dělnický
sovět	sovět	k1gInSc1	sovět
s	s	k7c7	s
bolševickou	bolševický	k2eAgFnSc7d1	bolševická
většinou	většina	k1gFnSc7	většina
<g/>
,	,	kIx,	,
vojenský	vojenský	k2eAgInSc1d1	vojenský
sovět	sovět	k1gInSc1	sovět
stále	stále	k6eAd1	stále
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
eserů	eser	k1gMnPc2	eser
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
rostoucím	rostoucí	k2eAgInSc7d1	rostoucí
vlivem	vliv	k1gInSc7	vliv
bolševiků	bolševik	k1gMnPc2	bolševik
<g/>
,	,	kIx,	,
a	a	k8xC	a
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
čtvrtí	čtvrt	k1gFnPc2	čtvrt
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
ovládaná	ovládaný	k2eAgFnSc1d1	ovládaná
bolševiky	bolševik	k1gMnPc7	bolševik
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
přišly	přijít	k5eAaPmAgFnP	přijít
z	z	k7c2	z
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
zprávy	zpráva	k1gFnSc2	zpráva
o	o	k7c4	o
převzetí	převzetí	k1gNnSc4	převzetí
moci	moc	k1gFnSc2	moc
bolševiky	bolševik	k1gMnPc7	bolševik
<g/>
,	,	kIx,	,
strany	strana	k1gFnPc1	strana
nechtěly	chtít	k5eNaImAgFnP	chtít
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
konfrontaci	konfrontace	k1gFnSc6	konfrontace
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jak	jak	k8xC	jak
městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
místní	místní	k2eAgInPc1d1	místní
sověty	sovět	k1gInPc1	sovět
vytvořily	vytvořit	k5eAaPmAgInP	vytvořit
vojenské	vojenský	k2eAgInPc1d1	vojenský
orgány	orgán	k1gInPc1	orgán
(	(	kIx(	(
<g/>
radnice	radnice	k1gFnSc1	radnice
Výbor	výbor	k1gInSc4	výbor
pro	pro	k7c4	pro
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
a	a	k8xC	a
sověty	sovět	k1gInPc4	sovět
Revoluční	revoluční	k2eAgInSc4d1	revoluční
vojenský	vojenský	k2eAgInSc4d1	vojenský
výbor	výbor	k1gInSc4	výbor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
městské	městský	k2eAgFnSc2d1	městská
rady	rada	k1gFnSc2	rada
zahrnoval	zahrnovat	k5eAaImAgMnS	zahrnovat
subjekty	subjekt	k1gInPc4	subjekt
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
eserů	eser	k1gMnPc2	eser
nebo	nebo	k8xC	nebo
menševiků	menševik	k1gMnPc2	menševik
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
výkonné	výkonný	k2eAgInPc4d1	výkonný
výbory	výbor	k1gInPc4	výbor
sovětů	sovět	k1gInPc2	sovět
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
rolníků	rolník	k1gMnPc2	rolník
moskevské	moskevský	k2eAgFnSc2d1	Moskevská
provincie	provincie	k1gFnSc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Výbor	výbor	k1gInSc1	výbor
nechal	nechat	k5eAaPmAgInS	nechat
vojenské	vojenský	k2eAgFnPc4d1	vojenská
operace	operace	k1gFnPc4	operace
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
místního	místní	k2eAgMnSc4d1	místní
vojenského	vojenský	k2eAgMnSc4d1	vojenský
guvernéra	guvernér	k1gMnSc4	guvernér
plukovníka	plukovník	k1gMnSc2	plukovník
K.	K.	kA	K.
I.	I.	kA	I.
Rjabceva	Rjabceva	k1gFnSc1	Rjabceva
a	a	k8xC	a
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
návrh	návrh	k1gInSc1	návrh
jediného	jediné	k1gNnSc2	jediné
ještě	ještě	k6eAd1	ještě
svobodného	svobodný	k2eAgMnSc2d1	svobodný
ministra	ministr	k1gMnSc2	ministr
Prozatímní	prozatímní	k2eAgFnSc2d1	prozatímní
vlády	vláda	k1gFnSc2	vláda
S.	S.	kA	S.
N.	N.	kA	N.
Prokopoviče	Prokopovič	k1gMnSc2	Prokopovič
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
doplnil	doplnit	k5eAaPmAgInS	doplnit
vládní	vládní	k2eAgNnPc4d1	vládní
místa	místo	k1gNnPc4	místo
ministrů	ministr	k1gMnPc2	ministr
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
zajati	zajmout	k5eAaPmNgMnP	zajmout
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
Pokusy	pokus	k1gInPc1	pokus
místních	místní	k2eAgMnPc2d1	místní
eserů	eser	k1gMnPc2	eser
o	o	k7c4	o
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
mobilizaci	mobilizace	k1gFnSc4	mobilizace
svých	svůj	k3xOyFgMnPc2	svůj
stoupenců	stoupenec	k1gMnPc2	stoupenec
selhaly	selhat	k5eAaPmAgFnP	selhat
<g/>
;	;	kIx,	;
strana	strana	k1gFnSc1	strana
ztratila	ztratit	k5eAaPmAgFnS	ztratit
podporu	podpora	k1gFnSc4	podpora
většiny	většina	k1gFnSc2	většina
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
dělníků	dělník	k1gMnPc2	dělník
města	město	k1gNnSc2	město
a	a	k8xC	a
nedokázala	dokázat	k5eNaPmAgFnS	dokázat
si	se	k3xPyFc3	se
vytvořit	vytvořit	k5eAaPmF	vytvořit
spolehlivé	spolehlivý	k2eAgFnPc4d1	spolehlivá
vojenské	vojenský	k2eAgFnPc4d1	vojenská
síly	síla	k1gFnPc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
bojujících	bojující	k2eAgMnPc2d1	bojující
proti	proti	k7c3	proti
bolševikům	bolševik	k1gMnPc3	bolševik
poskytly	poskytnout	k5eAaPmAgFnP	poskytnout
vojenské	vojenský	k2eAgFnPc1d1	vojenská
akademie	akademie	k1gFnPc1	akademie
<g/>
,	,	kIx,	,
důležitější	důležitý	k2eAgFnPc1d2	důležitější
a	a	k8xC	a
soudržnější	soudržný	k2eAgFnPc1d2	soudržnější
než	než	k8xS	než
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
důstojníků	důstojník	k1gMnPc2	důstojník
se	se	k3xPyFc4	se
však	však	k9	však
do	do	k7c2	do
střetu	střet	k1gInSc2	střet
nezapojilo	zapojit	k5eNaPmAgNnS	zapojit
<g/>
.	.	kIx.	.
</s>
<s>
Vůbec	vůbec	k9	vůbec
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
projevila	projevit	k5eAaPmAgFnS	projevit
vůči	vůči	k7c3	vůči
střetu	střet	k1gInSc3	střet
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
stranami	strana	k1gFnPc7	strana
lhostejnost	lhostejnost	k1gFnSc1	lhostejnost
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
policie	policie	k1gFnSc1	policie
obecní	obecní	k2eAgFnSc1d1	obecní
úřady	úřada	k1gMnPc7	úřada
nepodporovala	podporovat	k5eNaImAgFnS	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Bolševici	bolševik	k1gMnPc1	bolševik
mezitím	mezitím	k6eAd1	mezitím
získali	získat	k5eAaPmAgMnP	získat
podporu	podpora	k1gFnSc4	podpora
několika	několik	k4yIc2	několik
tisíc	tisíc	k4xCgInPc2	tisíc
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
z	z	k7c2	z
rudých	rudý	k2eAgFnPc2d1	rudá
gard	garda	k1gFnPc2	garda
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
mladých	mladý	k2eAgMnPc2d1	mladý
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
většiny	většina	k1gFnSc2	většina
posádky	posádka	k1gFnSc2	posádka
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
hodně	hodně	k6eAd1	hodně
vlažnou	vlažný	k2eAgFnSc4d1	vlažná
<g/>
.	.	kIx.	.
</s>
<s>
Žádný	žádný	k3yNgInSc1	žádný
z	z	k7c2	z
pluků	pluk	k1gInPc2	pluk
města	město	k1gNnSc2	město
nepodporoval	podporovat	k5eNaImAgInS	podporovat
Prozatímní	prozatímní	k2eAgFnSc4d1	prozatímní
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Výhoda	výhoda	k1gFnSc1	výhoda
bolševiků	bolševik	k1gMnPc2	bolševik
spočívala	spočívat	k5eAaImAgFnS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
měli	mít	k5eAaImAgMnP	mít
početní	početní	k2eAgFnSc4d1	početní
nadřazenost	nadřazenost	k1gFnSc4	nadřazenost
a	a	k8xC	a
dělostřelectvo	dělostřelectvo	k1gNnSc4	dělostřelectvo
<g/>
.	.	kIx.	.
</s>
<s>
Mohli	moct	k5eAaImAgMnP	moct
se	se	k3xPyFc4	se
také	také	k9	také
spoléhat	spoléhat	k5eAaImF	spoléhat
na	na	k7c4	na
posily	posila	k1gFnPc4	posila
z	z	k7c2	z
průmyslových	průmyslový	k2eAgNnPc2d1	průmyslové
center	centrum	k1gNnPc2	centrum
kolem	kolem	k7c2	kolem
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Síly	síla	k1gFnPc1	síla
radničního	radniční	k2eAgInSc2d1	radniční
výboru	výbor	k1gInSc2	výbor
<g/>
,	,	kIx,	,
pět	pět	k4xCc4	pět
až	až	k9	až
deset	deset	k4xCc4	deset
tisíc	tisíc	k4xCgInPc2	tisíc
mužů	muž	k1gMnPc2	muž
s	s	k7c7	s
četnými	četný	k2eAgInPc7d1	četný
kulomety	kulomet	k1gInPc7	kulomet
<g/>
,	,	kIx,	,
stály	stát	k5eAaImAgFnP	stát
proti	proti	k7c3	proti
čtyřiceti	čtyřicet	k4xCc6	čtyřicet
až	až	k6eAd1	až
padesáti	padesát	k4xCc3	padesát
tisícům	tisíc	k4xCgInPc3	tisíc
mužů	muž	k1gMnPc2	muž
revolučního	revoluční	k2eAgInSc2d1	revoluční
výboru	výbor	k1gInSc2	výbor
a	a	k8xC	a
jejích	její	k3xOp3gFnPc2	její
dělům	dělo	k1gNnPc3	dělo
<g/>
.27	.27	k4	.27
<g/>
.	.	kIx.	.
říjnajul	říjnajout	k5eAaPmAgInS	říjnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
a	a	k8xC	a
následující	následující	k2eAgInSc1d1	následující
den	den	k1gInSc1	den
probíhala	probíhat	k5eAaImAgFnS	probíhat
jednání	jednání	k1gNnSc4	jednání
mezi	mezi	k7c7	mezi
plukovníkem	plukovník	k1gMnSc7	plukovník
Rjabcevem	Rjabcev	k1gInSc7	Rjabcev
a	a	k8xC	a
bolševiky	bolševik	k1gMnPc4	bolševik
<g/>
;	;	kIx,	;
plukovník	plukovník	k1gMnSc1	plukovník
požadoval	požadovat	k5eAaImAgMnS	požadovat
uvolnění	uvolnění	k1gNnSc4	uvolnění
moskevského	moskevský	k2eAgInSc2d1	moskevský
Kremlu	Kreml	k1gInSc2	Kreml
a	a	k8xC	a
rozpuštění	rozpuštění	k1gNnSc1	rozpuštění
Revolučního	revoluční	k2eAgInSc2d1	revoluční
vojenského	vojenský	k2eAgInSc2d1	vojenský
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
25	[number]	k4	25
<g/>
.	.	kIx.	.
říjnajul	říjnajout	k5eAaPmAgInS	říjnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
totiž	totiž	k9	totiž
jednotka	jednotka	k1gFnSc1	jednotka
věrná	věrný	k2eAgFnSc1d1	věrná
bolševikům	bolševik	k1gMnPc3	bolševik
převzala	převzít	k5eAaPmAgFnS	převzít
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
telegrafní	telegrafní	k2eAgFnSc7d1	telegrafní
stanicí	stanice	k1gFnSc7	stanice
<g/>
,	,	kIx,	,
poštou	pošta	k1gFnSc7	pošta
a	a	k8xC	a
Kremlem	Kreml	k1gInSc7	Kreml
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
arzenálem	arzenál	k1gInSc7	arzenál
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
bolševici	bolševik	k1gMnPc1	bolševik
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
plukovníkovy	plukovníkův	k2eAgInPc4d1	plukovníkův
požadavky	požadavek	k1gInPc4	požadavek
<g/>
,	,	kIx,	,
začaly	začít	k5eAaPmAgInP	začít
tvrdé	tvrdý	k2eAgInPc1d1	tvrdý
boje	boj	k1gInPc1	boj
<g/>
.	.	kIx.	.
</s>
<s>
Rjabcev	Rjabcev	k1gFnSc4	Rjabcev
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
na	na	k7c4	na
Kreml	Kreml	k1gInSc4	Kreml
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
27	[number]	k4	27
<g/>
.	.	kIx.	.
říjnajul	říjnajout	k5eAaPmAgInS	říjnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
když	když	k8xS	když
obhájci	obhájce	k1gMnPc1	obhájce
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
vpustit	vpustit	k5eAaPmF	vpustit
do	do	k7c2	do
pevnosti	pevnost	k1gFnSc2	pevnost
stráž	stráž	k1gFnSc4	stráž
složenou	složený	k2eAgFnSc4d1	složená
z	z	k7c2	z
kadetů	kadet	k1gMnPc2	kadet
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
zůstávalo	zůstávat	k5eAaImAgNnS	zůstávat
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
Výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
průmyslová	průmyslový	k2eAgNnPc4d1	průmyslové
předměstí	předměstí	k1gNnPc4	předměstí
ovládali	ovládat	k5eAaImAgMnP	ovládat
bolševici	bolševik	k1gMnPc1	bolševik
<g/>
.28	.28	k4	.28
<g/>
.	.	kIx.	.
říjnajul	říjnajout	k5eAaPmAgInS	říjnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
jednotky	jednotka	k1gFnPc4	jednotka
radničního	radniční	k2eAgInSc2d1	radniční
výboru	výbor	k1gInSc2	výbor
dobyly	dobýt	k5eAaPmAgFnP	dobýt
Kreml	Kreml	k1gInSc4	Kreml
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
obránci	obránce	k1gMnPc1	obránce
byli	být	k5eAaImAgMnP	být
postříleni	postřílet	k5eAaPmNgMnP	postřílet
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
masakrů	masakr	k1gInPc2	masakr
revolučního	revoluční	k2eAgNnSc2d1	revoluční
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Pokusy	pokus	k1gInPc1	pokus
prolomit	prolomit	k5eAaPmF	prolomit
blokádu	blokáda	k1gFnSc4	blokáda
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
selhaly	selhat	k5eAaPmAgFnP	selhat
kvůli	kvůli	k7c3	kvůli
podpoře	podpora	k1gFnSc3	podpora
bolševiků	bolševik	k1gMnPc2	bolševik
dělníky	dělník	k1gMnPc4	dělník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
29	[number]	k4	29
<g/>
.	.	kIx.	.
říjnajul	říjnajout	k5eAaPmAgInS	říjnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
bylo	být	k5eAaImAgNnS	být
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
příměří	příměří	k1gNnSc2	příměří
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
díky	díky	k7c3	díky
Vikželu	Vikžel	k1gInSc3	Vikžel
<g/>
,	,	kIx,	,
národním	národní	k2eAgMnPc3d1	národní
železničním	železniční	k2eAgMnPc3d1	železniční
odborům	odbor	k1gInPc3	odbor
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
přijaly	přijmout	k5eAaPmAgFnP	přijmout
příměří	příměří	k1gNnSc4	příměří
v	v	k7c4	v
naději	naděje	k1gFnSc4	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
přijdou	přijít	k5eAaPmIp3nP	přijít
posily	posila	k1gFnPc1	posila
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jim	on	k3xPp3gMnPc3	on
zajistí	zajistit	k5eAaPmIp3nP	zajistit
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
bolševický	bolševický	k2eAgInSc1d1	bolševický
revoluční	revoluční	k2eAgInSc1d1	revoluční
výbor	výbor	k1gInSc1	výbor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
očekávané	očekávaný	k2eAgFnPc1d1	očekávaná
posily	posila	k1gFnPc1	posila
získal	získat	k5eAaPmAgInS	získat
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jeho	jeho	k3xOp3gMnSc3	jeho
soupeři	soupeř	k1gMnSc3	soupeř
se	se	k3xPyFc4	se
totéž	týž	k3xTgNnSc1	týž
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Eserům	eser	k1gMnPc3	eser
se	se	k3xPyFc4	se
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
nepodařilo	podařit	k5eNaPmAgNnS	podařit
zastavit	zastavit	k5eAaPmF	zastavit
pronikání	pronikání	k1gNnSc1	pronikání
bolševických	bolševický	k2eAgFnPc2d1	bolševická
posil	posila	k1gFnPc2	posila
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
:	:	kIx,	:
Dvanáct	dvanáct	k4xCc1	dvanáct
tisíc	tisíc	k4xCgInPc2	tisíc
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
Michaila	Michail	k1gMnSc2	Michail
Frunzeho	Frunze	k1gMnSc2	Frunze
dorazilo	dorazit	k5eAaPmAgNnS	dorazit
z	z	k7c2	z
provincie	provincie	k1gFnSc2	provincie
Vladimir	Vladimir	k1gInSc1	Vladimir
a	a	k8xC	a
pět	pět	k4xCc1	pět
set	sto	k4xCgNnPc2	sto
námořníků	námořník	k1gMnPc2	námořník
a	a	k8xC	a
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
1200	[number]	k4	1200
námořníků	námořník	k1gMnPc2	námořník
a	a	k8xC	a
rudých	rudý	k2eAgMnPc2d1	rudý
gardistů	gardista	k1gMnPc2	gardista
přijelo	přijet	k5eAaPmAgNnS	přijet
z	z	k7c2	z
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
Sliby	slib	k1gInPc1	slib
podpory	podpora	k1gFnSc2	podpora
radniční	radniční	k2eAgFnSc2d1	radniční
komise	komise	k1gFnSc2	komise
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
velitelů	velitel	k1gMnPc2	velitel
západní	západní	k2eAgFnSc2d1	západní
fronty	fronta	k1gFnSc2	fronta
se	se	k3xPyFc4	se
projevily	projevit	k5eAaPmAgFnP	projevit
pouze	pouze	k6eAd1	pouze
příjezdem	příjezd	k1gInSc7	příjezd
sto	sto	k4xCgNnSc4	sto
sedmdesáti	sedmdesát	k4xCc2	sedmdesát
šesti	šest	k4xCc2	šest
příslušníků	příslušník	k1gMnPc2	příslušník
stíhacího	stíhací	k2eAgInSc2d1	stíhací
praporu	prapor	k1gInSc2	prapor
<g/>
:	:	kIx,	:
Kerenskij	Kerenskij	k1gMnSc1	Kerenskij
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
odvést	odvést	k5eAaPmF	odvést
některé	některý	k3yIgFnPc4	některý
z	z	k7c2	z
posil	posila	k1gFnPc2	posila
do	do	k7c2	do
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
jednotky	jednotka	k1gFnPc1	jednotka
se	se	k3xPyFc4	se
odmítly	odmítnout	k5eAaPmAgFnP	odmítnout
účastnit	účastnit	k5eAaImF	účastnit
a	a	k8xC	a
města	město	k1gNnSc2	město
ovládaná	ovládaný	k2eAgFnSc1d1	ovládaná
bolševiky	bolševik	k1gMnPc7	bolševik
bránila	bránit	k5eAaImAgFnS	bránit
postupu	postup	k1gInSc3	postup
ostatních	ostatní	k2eAgFnPc2d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
příměří	příměří	k1gNnSc1	příměří
prospělo	prospět	k5eAaPmAgNnS	prospět
bolševikům	bolševik	k1gMnPc3	bolševik
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
menševici	menševik	k1gMnPc1	menševik
a	a	k8xC	a
leví	levý	k2eAgMnPc1d1	levý
eseři	eser	k1gMnPc1	eser
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
vytvořit	vytvořit	k5eAaPmF	vytvořit
sílu	síla	k1gFnSc4	síla
nezávislou	závislý	k2eNgFnSc4d1	nezávislá
na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
táborech	tábor	k1gInPc6	tábor
protivníků	protivník	k1gMnPc2	protivník
<g/>
.	.	kIx.	.
<g/>
Navzdory	navzdory	k7c3	navzdory
ústupkům	ústupek	k1gInPc3	ústupek
Výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
,	,	kIx,	,
vynuceným	vynucený	k2eAgInSc7d1	vynucený
Vikželem	Vikžel	k1gInSc7	Vikžel
<g/>
,	,	kIx,	,
Vojenský	vojenský	k2eAgInSc1d1	vojenský
revoluční	revoluční	k2eAgInSc1d1	revoluční
výbor	výbor	k1gInSc1	výbor
ukončil	ukončit	k5eAaPmAgInS	ukončit
příměří	příměří	k1gNnSc4	příměří
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zničil	zničit	k5eAaPmAgInS	zničit
svého	svůj	k3xOyFgMnSc4	svůj
soupeře	soupeř	k1gMnSc4	soupeř
díky	díky	k7c3	díky
svým	svůj	k3xOyFgFnPc3	svůj
posilám	posila	k1gFnPc3	posila
a	a	k8xC	a
díky	díky	k7c3	díky
konečnému	konečný	k2eAgNnSc3d1	konečné
vítězství	vítězství	k1gNnSc3	vítězství
bolševiků	bolševik	k1gMnPc2	bolševik
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
30	[number]	k4	30
<g/>
.	.	kIx.	.
říjnajul	říjnajout	k5eAaPmAgInS	říjnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
boje	boj	k1gInSc2	boj
opět	opět	k6eAd1	opět
vzplály	vzplát	k5eAaPmAgFnP	vzplát
a	a	k8xC	a
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
během	během	k7c2	během
následujících	následující	k2eAgInPc2d1	následující
tří	tři	k4xCgInPc2	tři
dnů	den	k1gInPc2	den
<g/>
;	;	kIx,	;
tentokrát	tentokrát	k6eAd1	tentokrát
byly	být	k5eAaImAgFnP	být
protibolševické	protibolševický	k2eAgFnPc1d1	protibolševická
síly	síla	k1gFnPc1	síla
již	již	k6eAd1	již
v	v	k7c6	v
obraně	obrana	k1gFnSc6	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Odpor	odpor	k1gInSc1	odpor
byl	být	k5eAaImAgInS	být
zarputilý	zarputilý	k2eAgInSc1d1	zarputilý
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc4	jeho
poslední	poslední	k2eAgInPc1d1	poslední
opěrné	opěrný	k2eAgInPc1d1	opěrný
body	bod	k1gInPc1	bod
byly	být	k5eAaImAgFnP	být
radnice	radnice	k1gFnPc1	radnice
<g/>
,	,	kIx,	,
Alexandrovova	Alexandrovův	k2eAgFnSc1d1	Alexandrovův
vojenská	vojenský	k2eAgFnSc1d1	vojenská
akademie	akademie	k1gFnSc1	akademie
a	a	k8xC	a
Kreml	Kreml	k1gInSc1	Kreml
<g/>
.	.	kIx.	.
</s>
<s>
Rudé	rudý	k2eAgFnPc1d1	rudá
gardy	garda	k1gFnPc1	garda
ovládly	ovládnout	k5eAaPmAgFnP	ovládnout
Kreml	Kreml	k1gInSc4	Kreml
útokem	útok	k1gInSc7	útok
z	z	k7c2	z
rána	ráno	k1gNnSc2	ráno
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadujul	listopadujout	k5eAaPmAgInS	listopadujout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
a	a	k8xC	a
lynčovaly	lynčovat	k5eAaBmAgInP	lynčovat
některé	některý	k3yIgFnPc4	některý
obránce	obránce	k1gMnSc1	obránce
v	v	k7c6	v
pomstě	pomsta	k1gFnSc6	pomsta
za	za	k7c4	za
předchozí	předchozí	k2eAgInSc4d1	předchozí
masakr	masakr	k1gInSc4	masakr
<g/>
.	.	kIx.	.
</s>
<s>
Starosta	Starosta	k1gMnSc1	Starosta
nakonec	nakonec	k6eAd1	nakonec
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
kapitulaci	kapitulace	k1gFnSc4	kapitulace
a	a	k8xC	a
poražení	poražený	k1gMnPc1	poražený
byli	být	k5eAaImAgMnP	být
odzbrojeni	odzbrojit	k5eAaPmNgMnP	odzbrojit
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
je	on	k3xPp3gFnPc4	on
pak	pak	k6eAd1	pak
propustili	propustit	k5eAaPmAgMnP	propustit
<g/>
;	;	kIx,	;
Výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
byl	být	k5eAaImAgInS	být
rozpuštěn	rozpuštěn	k2eAgInSc1d1	rozpuštěn
a	a	k8xC	a
všechny	všechen	k3xTgFnPc1	všechen
socialistické	socialistický	k2eAgFnPc1d1	socialistická
strany	strana	k1gFnPc1	strana
vyjednaly	vyjednat	k5eAaPmAgFnP	vyjednat
konečné	konečný	k2eAgFnPc1d1	konečná
podmínky	podmínka	k1gFnPc1	podmínka
kapitulace	kapitulace	k1gFnSc2	kapitulace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
obecně	obecně	k6eAd1	obecně
velkorysé	velkorysý	k2eAgFnPc1d1	velkorysá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sibiř	Sibiř	k1gFnSc4	Sibiř
===	===	k?	===
</s>
</p>
<p>
<s>
S	s	k7c7	s
několika	několik	k4yIc7	několik
výjimkami	výjimka	k1gFnPc7	výjimka
nebyly	být	k5eNaImAgInP	být
zprávy	zpráva	k1gFnSc2	zpráva
o	o	k7c6	o
petrohradském	petrohradský	k2eAgInSc6d1	petrohradský
bolševickém	bolševický	k2eAgInSc6d1	bolševický
převratu	převrat	k1gInSc6	převrat
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
přijímány	přijímat	k5eAaImNgInP	přijímat
dobře	dobře	k6eAd1	dobře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
městech	město	k1gNnPc6	město
(	(	kIx(	(
<g/>
Omsk	Omsk	k1gInSc1	Omsk
<g/>
,	,	kIx,	,
Irkutsk	Irkutsk	k1gInSc1	Irkutsk
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
protesty	protest	k1gInPc4	protest
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
regionu	region	k1gInSc6	region
obecně	obecně	k6eAd1	obecně
vládla	vládnout	k5eAaImAgFnS	vládnout
lhostejnost	lhostejnost	k1gFnSc1	lhostejnost
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
však	však	k9	však
volby	volba	k1gFnPc1	volba
v	v	k7c6	v
místních	místní	k2eAgInPc6d1	místní
sovětech	sovět	k1gInPc6	sovět
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
vyhrávali	vyhrávat	k5eAaImAgMnP	vyhrávat
bolševici	bolševik	k1gMnPc1	bolševik
<g/>
,	,	kIx,	,
umožnily	umožnit	k5eAaPmAgFnP	umožnit
pomalu	pomalu	k6eAd1	pomalu
převzít	převzít	k5eAaPmF	převzít
moc	moc	k6eAd1	moc
i	i	k9	i
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Bolševici	bolševik	k1gMnPc1	bolševik
neřídili	řídit	k5eNaImAgMnP	řídit
některá	některý	k3yIgNnPc4	některý
místa	místo	k1gNnPc4	místo
až	až	k6eAd1	až
do	do	k7c2	do
února	únor	k1gInSc2	únor
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
dosavadní	dosavadní	k2eAgInPc1d1	dosavadní
veřejné	veřejný	k2eAgInPc1d1	veřejný
orgány	orgán	k1gInPc1	orgán
byly	být	k5eAaImAgInP	být
obecně	obecně	k6eAd1	obecně
velmi	velmi	k6eAd1	velmi
kritické	kritický	k2eAgFnPc1d1	kritická
vůči	vůči	k7c3	vůči
převzetí	převzetí	k1gNnSc3	převzetí
vládní	vládní	k2eAgFnSc2d1	vládní
moci	moc	k1gFnSc2	moc
bolševiky	bolševik	k1gMnPc4	bolševik
<g/>
,	,	kIx,	,
neprokázaly	prokázat	k5eNaPmAgFnP	prokázat
účinný	účinný	k2eAgInSc4d1	účinný
odpor	odpor	k1gInSc4	odpor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Oblasti	oblast	k1gFnPc1	oblast
národnostních	národnostní	k2eAgFnPc2d1	národnostní
menšin	menšina	k1gFnPc2	menšina
===	===	k?	===
</s>
</p>
<p>
<s>
Revoluce	revoluce	k1gFnSc1	revoluce
povzbudila	povzbudit	k5eAaPmAgFnS	povzbudit
nacionalistické	nacionalistický	k2eAgFnPc4d1	nacionalistická
snahy	snaha	k1gFnPc4	snaha
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
národností	národnost	k1gFnPc2	národnost
<g/>
:	:	kIx,	:
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
a	a	k8xC	a
prosinci	prosinec	k1gInSc6	prosinec
většina	většina	k1gFnSc1	většina
etnických	etnický	k2eAgFnPc2d1	etnická
skupin	skupina	k1gFnPc2	skupina
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
autonomii	autonomie	k1gFnSc4	autonomie
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
;	;	kIx,	;
toto	tento	k3xDgNnSc1	tento
úsilí	úsilí	k1gNnSc1	úsilí
menšin	menšina	k1gFnPc2	menšina
naráželo	narážet	k5eAaImAgNnS	narážet
na	na	k7c4	na
odpor	odpor	k1gInSc4	odpor
nejen	nejen	k6eAd1	nejen
příznivců	příznivec	k1gMnPc2	příznivec
nové	nový	k2eAgFnSc2d1	nová
sovětské	sovětský	k2eAgFnSc2d1	sovětská
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
často	často	k6eAd1	často
i	i	k9	i
ruských	ruský	k2eAgMnPc2d1	ruský
obyvatel	obyvatel	k1gMnPc2	obyvatel
opačného	opačný	k2eAgInSc2d1	opačný
názoru	názor	k1gInSc2	názor
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g />
.	.	kIx.	.
</s>
<s>
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
nová	nový	k2eAgFnSc1d1	nová
vláda	vláda	k1gFnSc1	vláda
uznala	uznat	k5eAaPmAgFnS	uznat
4	[number]	k4	4
<g/>
.	.	kIx.	.
lednajul	lednajout	k5eAaPmAgInS	lednajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1918	[number]	k4	1918
<g/>
greg	grega	k1gFnPc2	grega
<g/>
..	..	k?	..
V	v	k7c6	v
Lotyšsku	Lotyšsko	k1gNnSc6	Lotyšsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
stranou	strana	k1gFnSc7	strana
probolševičtí	probolševičtět	k5eAaPmIp3nP	probolševičtět
sociální	sociální	k2eAgMnPc1d1	sociální
demokraté	demokrat	k1gMnPc1	demokrat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
o	o	k7c6	o
budoucnosti	budoucnost	k1gFnSc6	budoucnost
regionu	region	k1gInSc2	region
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
<g/>
;	;	kIx,	;
jednotky	jednotka	k1gFnSc2	jednotka
lotyšských	lotyšský	k2eAgInPc2d1	lotyšský
střelců	střelec	k1gMnPc2	střelec
byly	být	k5eAaImAgInP	být
jedněmi	jeden	k4xCgFnPc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
útvarů	útvar	k1gInPc2	útvar
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
podporovaly	podporovat	k5eAaImAgInP	podporovat
Leninovu	Leninův	k2eAgFnSc4d1	Leninova
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Estonsku	Estonsko	k1gNnSc6	Estonsko
bojovali	bojovat	k5eAaImAgMnP	bojovat
nacionalisté	nacionalista	k1gMnPc1	nacionalista
a	a	k8xC	a
prosovětské	prosovětský	k2eAgFnPc1d1	prosovětská
síly	síla	k1gFnPc1	síla
až	až	k9	až
do	do	k7c2	do
obsazení	obsazení	k1gNnSc2	obsazení
území	území	k1gNnSc6	území
německými	německý	k2eAgFnPc7d1	německá
jednotkami	jednotka	k1gFnPc7	jednotka
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
byla	být	k5eAaImAgFnS	být
situace	situace	k1gFnSc1	situace
mimořádně	mimořádně	k6eAd1	mimořádně
složitá	složitý	k2eAgFnSc1d1	složitá
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
,	,	kIx,	,
o	o	k7c4	o
jejíž	jejíž	k3xOyRp3gNnSc4	jejíž
ovládnutí	ovládnutí	k1gNnSc4	ovládnutí
se	se	k3xPyFc4	se
snažilo	snažit	k5eAaImAgNnS	snažit
několik	několik	k4yIc1	několik
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
uzavřely	uzavřít	k5eAaPmAgFnP	uzavřít
probolševické	probolševický	k2eAgFnPc1d1	probolševický
síly	síla	k1gFnPc1	síla
a	a	k8xC	a
Centrální	centrální	k2eAgFnSc1d1	centrální
rada	rada	k1gFnSc1	rada
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
nejistou	jistý	k2eNgFnSc4d1	nejistá
alianci	aliance	k1gFnSc4	aliance
proti	proti	k7c3	proti
svým	svůj	k3xOyFgMnPc3	svůj
oponentům	oponent	k1gMnPc3	oponent
a	a	k8xC	a
Rada	rada	k1gFnSc1	rada
vydala	vydat	k5eAaPmAgFnS	vydat
svou	svůj	k3xOyFgFnSc4	svůj
třetí	třetí	k4xOgFnSc3	třetí
"	"	kIx"	"
<g/>
univerzálu	univerzál	k1gInSc6	univerzál
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
prohlášení	prohlášení	k1gNnSc4	prohlášení
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadujul	listopadujout	k5eAaPmAgInS	listopadujout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
vládou	vláda	k1gFnSc7	vláda
regionu	region	k1gInSc2	region
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ne	ne	k9	ne
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
autonomie	autonomie	k1gFnSc2	autonomie
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
federálního	federální	k2eAgNnSc2d1	federální
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Vyslání	vyslání	k1gNnSc1	vyslání
vládních	vládní	k2eAgFnPc2d1	vládní
jednotek	jednotka	k1gFnPc2	jednotka
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
ovládly	ovládnout	k5eAaPmAgInP	ovládnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
strach	strach	k1gInSc1	strach
petrohradské	petrohradský	k2eAgFnSc2d1	Petrohradská
vlády	vláda	k1gFnSc2	vláda
z	z	k7c2	z
ukrajinsko-kozácké	ukrajinskoozácký	k2eAgFnSc2d1	ukrajinsko-kozácký
aliance	aliance	k1gFnSc2	aliance
zhoršily	zhoršit	k5eAaPmAgInP	zhoršit
vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
Petrohradem	Petrohrad	k1gInSc7	Petrohrad
a	a	k8xC	a
Kyjevem	Kyjev	k1gInSc7	Kyjev
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
nakonec	nakonec	k6eAd1	nakonec
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
nezávislost	nezávislost	k1gFnSc4	nezávislost
9	[number]	k4	9
<g/>
.	.	kIx.	.
lednajul	lednajout	k5eAaPmAgInS	lednajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
22	[number]	k4	22
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1918	[number]	k4	1918
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bolševici	bolševik	k1gMnPc1	bolševik
brzy	brzy	k6eAd1	brzy
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
Kyjev	Kyjev	k1gInSc4	Kyjev
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
nedokázali	dokázat	k5eNaPmAgMnP	dokázat
získat	získat	k5eAaPmF	získat
moc	moc	k6eAd1	moc
nad	nad	k7c7	nad
celým	celý	k2eAgInSc7d1	celý
regionem	region	k1gInSc7	region
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zuřily	zuřit	k5eAaImAgInP	zuřit
boje	boj	k1gInPc1	boj
mezi	mezi	k7c7	mezi
četnými	četný	k2eAgFnPc7d1	četná
stranami	strana	k1gFnPc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Zakavkazsku	Zakavkazsko	k1gNnSc6	Zakavkazsko
většina	většina	k1gFnSc1	většina
arménských	arménský	k2eAgFnPc2d1	arménská
<g/>
,	,	kIx,	,
gruzínských	gruzínský	k2eAgFnPc2d1	gruzínská
a	a	k8xC	a
ázerbájdžánských	ázerbájdžánský	k2eAgMnPc2d1	ázerbájdžánský
vůdců	vůdce	k1gMnPc2	vůdce
nepřijala	přijmout	k5eNaPmAgFnS	přijmout
novou	nový	k2eAgFnSc4d1	nová
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadujul	listopadujout	k5eAaPmAgInS	listopadujout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1917	[number]	k4	1917
<g/>
greg	greg	k1gInSc1	greg
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgInS	být
ustaven	ustavit	k5eAaPmNgInS	ustavit
Komisariát	komisariát	k1gInSc1	komisariát
Zakavkazska	Zakavkazsko	k1gNnSc2	Zakavkazsko
jako	jako	k8xS	jako
dočasná	dočasný	k2eAgFnSc1d1	dočasná
vláda	vláda	k1gFnSc1	vláda
do	do	k7c2	do
svolání	svolání	k1gNnSc2	svolání
ústavodárného	ústavodárný	k2eAgNnSc2d1	Ústavodárné
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
regionech	region	k1gInPc6	region
s	s	k7c7	s
kozáckým	kozácký	k2eAgNnSc7d1	kozácké
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
se	se	k3xPyFc4	se
soustředila	soustředit	k5eAaPmAgFnS	soustředit
důležitá	důležitý	k2eAgFnSc1d1	důležitá
část	část	k1gFnSc1	část
opozice	opozice	k1gFnSc2	opozice
vůči	vůči	k7c3	vůči
Leninově	Leninův	k2eAgFnSc3d1	Leninova
vládě	vláda	k1gFnSc3	vláda
<g/>
;	;	kIx,	;
kozácké	kozácký	k2eAgInPc1d1	kozácký
úřady	úřad	k1gInPc1	úřad
v	v	k7c6	v
Jekatěrinodaru	Jekatěrinodar	k1gInSc6	Jekatěrinodar
<g/>
,	,	kIx,	,
na	na	k7c6	na
Kubáni	Kubáň	k1gFnSc6	Kubáň
a	a	k8xC	a
v	v	k7c6	v
Orenburgu	Orenburg	k1gInSc6	Orenburg
ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
stále	stále	k6eAd1	stále
vzdorovaly	vzdorovat	k5eAaImAgFnP	vzdorovat
bolševické	bolševický	k2eAgFnSc3d1	bolševická
nadvládě	nadvláda	k1gFnSc3	nadvláda
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
neměly	mít	k5eNaImAgInP	mít
velkou	velký	k2eAgFnSc4d1	velká
podporu	podpora	k1gFnSc4	podpora
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
dolního	dolní	k2eAgInSc2d1	dolní
toku	tok	k1gInSc2	tok
Donu	Don	k1gInSc2	Don
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
útočištěm	útočiště	k1gNnSc7	útočiště
pro	pro	k7c4	pro
vojáky	voják	k1gMnPc4	voják
a	a	k8xC	a
politiky	politik	k1gMnPc4	politik
nepřátelské	přátelský	k2eNgFnSc2d1	nepřátelská
vůči	vůči	k7c3	vůči
bolševikům	bolševik	k1gMnPc3	bolševik
a	a	k8xC	a
místem	místo	k1gNnSc7	místo
zrodu	zrod	k1gInSc2	zrod
jejich	jejich	k3xOp3gMnSc2	jejich
nejnebezpečnějšího	bezpečný	k2eNgMnSc2d3	nejnebezpečnější
protivníka	protivník	k1gMnSc2	protivník
v	v	k7c6	v
době	doba	k1gFnSc6	doba
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
Dobrovolnické	dobrovolnický	k2eAgFnSc2d1	dobrovolnická
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
se	se	k3xPyFc4	se
místní	místní	k2eAgMnPc1d1	místní
představitelé	představitel	k1gMnPc1	představitel
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
náboženští	náboženský	k2eAgMnPc1d1	náboženský
a	a	k8xC	a
konzervativní	konzervativní	k2eAgMnPc1d1	konzervativní
<g/>
,	,	kIx,	,
postavili	postavit	k5eAaPmAgMnP	postavit
proti	proti	k7c3	proti
nové	nový	k2eAgFnSc3d1	nová
vládě	vláda	k1gFnSc3	vláda
<g/>
;	;	kIx,	;
v	v	k7c6	v
Taškentu	Taškent	k1gInSc6	Taškent
ruští	ruský	k2eAgMnPc1d1	ruský
radikálové	radikál	k1gMnPc1	radikál
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
podpory	podpora	k1gFnSc2	podpora
domorodého	domorodý	k2eAgNnSc2d1	domorodé
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
)	)	kIx)	)
vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
sovětskou	sovětský	k2eAgFnSc4d1	sovětská
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byli	být	k5eAaImAgMnP	být
izolováni	izolovat	k5eAaBmNgMnP	izolovat
<g/>
.	.	kIx.	.
</s>
<s>
Konzervativní	konzervativní	k2eAgMnPc1d1	konzervativní
vůdci	vůdce	k1gMnPc1	vůdce
založili	založit	k5eAaPmAgMnP	založit
konkurenční	konkurenční	k2eAgFnSc4d1	konkurenční
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
Kokandě	Kokanda	k1gFnSc6	Kokanda
<g/>
.	.	kIx.	.
</s>
<s>
Dál	daleko	k6eAd2	daleko
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
muslimskou	muslimský	k2eAgFnSc7d1	muslimská
populací	populace	k1gFnSc7	populace
<g/>
,	,	kIx,	,
získala	získat	k5eAaPmAgFnS	získat
moc	moc	k6eAd1	moc
nacionalistická	nacionalistický	k2eAgNnPc4d1	nacionalistické
hnutí	hnutí	k1gNnPc4	hnutí
a	a	k8xC	a
vyhlásila	vyhlásit	k5eAaPmAgNnP	vyhlásit
autonomii	autonomie	k1gFnSc4	autonomie
až	až	k9	až
do	do	k7c2	do
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
Ústavodárného	ústavodárný	k2eAgNnSc2d1	Ústavodárné
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
.	.	kIx.	.
<g/>
Obecně	obecně	k6eAd1	obecně
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
moc	moc	k1gFnSc1	moc
nové	nový	k2eAgFnSc2d1	nová
vlády	vláda	k1gFnSc2	vláda
nad	nad	k7c7	nad
územími	území	k1gNnPc7	území
s	s	k7c7	s
menšinami	menšina	k1gFnPc7	menšina
neexistovala	existovat	k5eNaImAgFnS	existovat
nebo	nebo	k8xC	nebo
byla	být	k5eAaImAgFnS	být
omezená	omezený	k2eAgFnSc1d1	omezená
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
se	se	k3xPyFc4	se
soustředila	soustředit	k5eAaPmAgFnS	soustředit
v	v	k7c6	v
několika	několik	k4yIc6	několik
izolovaných	izolovaný	k2eAgNnPc6d1	izolované
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Nacionalistické	nacionalistický	k2eAgFnPc1d1	nacionalistická
organizace	organizace	k1gFnPc1	organizace
převzaly	převzít	k5eAaPmAgFnP	převzít
moc	moc	k6eAd1	moc
nad	nad	k7c7	nad
většinou	většina	k1gFnSc7	většina
území	území	k1gNnSc2	území
a	a	k8xC	a
usilovaly	usilovat	k5eAaImAgFnP	usilovat
především	především	k9	především
o	o	k7c4	o
autonomii	autonomie	k1gFnSc4	autonomie
v	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
federálním	federální	k2eAgInSc6d1	federální
státě	stát	k1gInSc6	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Armáda	armáda	k1gFnSc1	armáda
===	===	k?	===
</s>
</p>
<p>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
obecně	obecně	k6eAd1	obecně
schvalovali	schvalovat	k5eAaImAgMnP	schvalovat
sovětskou	sovětský	k2eAgFnSc4d1	sovětská
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
vojáci	voják	k1gMnPc1	voják
upřednostňovali	upřednostňovat	k5eAaImAgMnP	upřednostňovat
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
koalici	koalice	k1gFnSc4	koalice
a	a	k8xC	a
ne	ne	k9	ne
moc	moc	k6eAd1	moc
jen	jen	k6eAd1	jen
samotných	samotný	k2eAgMnPc2d1	samotný
bolševiků	bolševik	k1gMnPc2	bolševik
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
přijali	přijmout	k5eAaPmAgMnP	přijmout
první	první	k4xOgNnSc4	první
opatření	opatření	k1gNnSc4	opatření
přijatá	přijatý	k2eAgFnSc1d1	přijatá
Leninovou	Leninův	k2eAgFnSc7d1	Leninova
vládou	vláda	k1gFnSc7	vláda
<g/>
;	;	kIx,	;
díky	díky	k7c3	díky
četným	četný	k2eAgFnPc3d1	četná
schůzím	schůze	k1gFnPc3	schůze
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
jednotkách	jednotka	k1gFnPc6	jednotka
se	se	k3xPyFc4	se
bolševikům	bolševik	k1gMnPc3	bolševik
a	a	k8xC	a
levým	levý	k2eAgMnPc3d1	levý
eserům	eser	k1gMnPc3	eser
podařilo	podařit	k5eAaPmAgNnS	podařit
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
a	a	k8xC	a
prosinci	prosinec	k1gInSc6	prosinec
získat	získat	k5eAaPmF	získat
většinovou	většinový	k2eAgFnSc4d1	většinová
podporu	podpora	k1gFnSc4	podpora
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
podpora	podpora	k1gFnSc1	podpora
byla	být	k5eAaImAgFnS	být
silnější	silný	k2eAgFnSc1d2	silnější
na	na	k7c6	na
frontách	fronta	k1gFnPc6	fronta
u	u	k7c2	u
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
než	než	k8xS	než
ve	v	k7c6	v
vzdálenějších	vzdálený	k2eAgFnPc6d2	vzdálenější
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
armáda	armáda	k1gFnSc1	armáda
jako	jako	k8xC	jako
celek	celek	k1gInSc4	celek
přijala	přijmout	k5eAaPmAgFnS	přijmout
novou	nový	k2eAgFnSc4d1	nová
vládu	vláda	k1gFnSc4	vláda
a	a	k8xC	a
její	její	k3xOp3gMnPc1	její
oponenti	oponent	k1gMnPc1	oponent
se	se	k3xPyFc4	se
nemohli	moct	k5eNaImAgMnP	moct
spoléhat	spoléhat	k5eAaImF	spoléhat
na	na	k7c4	na
vojenské	vojenský	k2eAgFnPc4d1	vojenská
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
ní	on	k3xPp3gFnSc3	on
postavily	postavit	k5eAaPmAgFnP	postavit
<g/>
.	.	kIx.	.
<g/>
Bolševičtí	bolševický	k2eAgMnPc1d1	bolševický
vůdci	vůdce	k1gMnPc1	vůdce
si	se	k3xPyFc3	se
byli	být	k5eAaImAgMnP	být
vědomi	vědom	k2eAgMnPc1d1	vědom
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
klíčové	klíčový	k2eAgNnSc1d1	klíčové
ukončit	ukončit	k5eAaPmF	ukončit
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
udrželi	udržet	k5eAaPmAgMnP	udržet
u	u	k7c2	u
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
jiné	jiný	k2eAgFnPc4d1	jiná
země	zem	k1gFnPc4	zem
Sovnarkom	sovnarkom	k1gInSc1	sovnarkom
sjednal	sjednat	k5eAaPmAgInS	sjednat
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadujul	listopadujout	k5eAaPmAgInS	listopadujout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
<g/>
greg	greg	k1gInSc1	greg
<g/>
.	.	kIx.	.
začátek	začátek	k1gInSc1	začátek
jednání	jednání	k1gNnSc2	jednání
o	o	k7c6	o
příměří	příměří	k1gNnSc6	příměří
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
<g/>
;	;	kIx,	;
velící	velící	k2eAgMnSc1d1	velící
velitel	velitel	k1gMnSc1	velitel
<g/>
,	,	kIx,	,
generál	generál	k1gMnSc1	generál
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Dujonin	Dujonina	k1gFnPc2	Dujonina
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
jednat	jednat	k5eAaImF	jednat
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
odvolán	odvolat	k5eAaPmNgMnS	odvolat
<g/>
.	.	kIx.	.
</s>
<s>
Jednotky	jednotka	k1gFnPc1	jednotka
si	se	k3xPyFc3	se
vybíraly	vybírat	k5eAaImAgFnP	vybírat
delegáty	delegát	k1gMnPc4	delegát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zahájili	zahájit	k5eAaPmAgMnP	zahájit
jednání	jednání	k1gNnSc4	jednání
s	s	k7c7	s
jednotkami	jednotka	k1gFnPc7	jednotka
protivníka	protivník	k1gMnSc2	protivník
<g/>
,	,	kIx,	,
Centrálních	centrální	k2eAgFnPc2d1	centrální
mocností	mocnost	k1gFnPc2	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosincejul	prosincejout	k5eAaPmAgInS	prosincejout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1917	[number]	k4	1917
<g/>
greg	greg	k1gInSc4	greg
<g/>
.	.	kIx.	.
podepsáno	podepsán	k2eAgNnSc4d1	podepsáno
oficiální	oficiální	k2eAgNnSc4d1	oficiální
příměří	příměří	k1gNnSc4	příměří
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
jednotek	jednotka	k1gFnPc2	jednotka
již	již	k6eAd1	již
dávno	dávno	k6eAd1	dávno
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
místních	místní	k2eAgFnPc2d1	místní
dohod	dohoda	k1gFnPc2	dohoda
s	s	k7c7	s
blízkými	blízký	k2eAgFnPc7d1	blízká
nepřátelskými	přátelský	k2eNgFnPc7d1	nepřátelská
jednotkami	jednotka	k1gFnPc7	jednotka
<g/>
;	;	kIx,	;
oficiální	oficiální	k2eAgNnSc1d1	oficiální
příměří	příměří	k1gNnSc1	příměří
definitivně	definitivně	k6eAd1	definitivně
ukončilo	ukončit	k5eAaPmAgNnS	ukončit
válku	válka	k1gFnSc4	válka
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
frontě	fronta	k1gFnSc6	fronta
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
legitimizovalo	legitimizovat	k5eAaBmAgNnS	legitimizovat
novou	nový	k2eAgFnSc4d1	nová
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
se	se	k3xPyFc4	se
splnilo	splnit	k5eAaPmAgNnS	splnit
přání	přání	k1gNnSc4	přání
ukončit	ukončit	k5eAaPmF	ukončit
boje	boj	k1gInPc4	boj
a	a	k8xC	a
vrátit	vrátit	k5eAaPmF	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
svých	svůj	k3xOyFgInPc2	svůj
domovů	domov	k1gInPc2	domov
<g/>
;	;	kIx,	;
demobilizace	demobilizace	k1gFnSc1	demobilizace
začala	začít	k5eAaPmAgFnS	začít
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
vláda	vláda	k1gFnSc1	vláda
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
místa	místo	k1gNnPc4	místo
důstojníků	důstojník	k1gMnPc2	důstojník
za	za	k7c4	za
volitelná	volitelný	k2eAgNnPc4d1	volitelné
<g/>
,	,	kIx,	,
zrušila	zrušit	k5eAaPmAgFnS	zrušit
dosavadní	dosavadní	k2eAgInPc4d1	dosavadní
důstojnické	důstojnický	k2eAgInPc4d1	důstojnický
tituly	titul	k1gInPc4	titul
a	a	k8xC	a
nárameníky	nárameník	k1gInPc4	nárameník
a	a	k8xC	a
přenesla	přenést	k5eAaPmAgFnS	přenést
moc	moc	k6eAd1	moc
nad	nad	k7c7	nad
armádou	armáda	k1gFnSc7	armáda
na	na	k7c4	na
volené	volený	k2eAgInPc4d1	volený
výbory	výbor	k1gInPc4	výbor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
První	první	k4xOgInPc1	první
kroky	krok	k1gInPc1	krok
==	==	k?	==
</s>
</p>
<p>
<s>
Proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
i	i	k9	i
sociální	sociální	k2eAgFnSc1d1	sociální
a	a	k8xC	a
politická	politický	k2eAgFnSc1d1	politická
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
;	;	kIx,	;
noví	nový	k2eAgMnPc1d1	nový
komisaři	komisar	k1gMnPc1	komisar
urychleně	urychleně	k6eAd1	urychleně
schválili	schválit	k5eAaPmAgMnP	schválit
pozoruhodný	pozoruhodný	k2eAgInSc4d1	pozoruhodný
počet	počet	k1gInSc4	počet
zákonů	zákon	k1gInPc2	zákon
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
změnily	změnit	k5eAaPmAgFnP	změnit
společnost	společnost	k1gFnSc4	společnost
a	a	k8xC	a
ruskou	ruský	k2eAgFnSc4d1	ruská
ekonomiku	ekonomika	k1gFnSc4	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Dekretu	dekret	k1gInSc6	dekret
o	o	k7c6	o
půdě	půda	k1gFnSc6	půda
následovalo	následovat	k5eAaImAgNnS	následovat
přijetí	přijetí	k1gNnSc1	přijetí
osmihodinového	osmihodinový	k2eAgInSc2d1	osmihodinový
pracovního	pracovní	k2eAgInSc2d1	pracovní
dne	den	k1gInSc2	den
<g/>
,29	,29	k4	,29
<g/>
.	.	kIx.	.
říjnajul	říjnajout	k5eAaPmAgInS	říjnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
což	což	k3yQnSc1	což
splnilo	splnit	k5eAaPmAgNnS	splnit
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
přání	přání	k1gNnSc2	přání
dělníků	dělník	k1gMnPc2	dělník
<g/>
;	;	kIx,	;
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadujul	listopadujout	k5eAaPmAgInS	listopadujout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
</s>
<s>
Deklarace	deklarace	k1gFnSc1	deklarace
práv	právo	k1gNnPc2	právo
národů	národ	k1gInPc2	národ
Ruska	Rusko	k1gNnSc2	Rusko
zrušila	zrušit	k5eAaPmAgFnS	zrušit
diskriminaci	diskriminace	k1gFnSc3	diskriminace
na	na	k7c6	na
etnickém	etnický	k2eAgInSc6d1	etnický
základě	základ	k1gInSc6	základ
a	a	k8xC	a
náboženství	náboženství	k1gNnSc1	náboženství
a	a	k8xC	a
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
sebeurčení	sebeurčení	k1gNnSc4	sebeurčení
<g/>
;	;	kIx,	;
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadujul	listopadujout	k5eAaPmAgInS	listopadujout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
byly	být	k5eAaImAgFnP	být
zrušeny	zrušit	k5eAaPmNgInP	zrušit
šlechtické	šlechtický	k2eAgInPc1d1	šlechtický
a	a	k8xC	a
podobné	podobný	k2eAgInPc1d1	podobný
tituly	titul	k1gInPc1	titul
<g/>
.	.	kIx.	.
</s>
<s>
Převedení	převedení	k1gNnSc1	převedení
náboženských	náboženský	k2eAgFnPc2d1	náboženská
škol	škola	k1gFnPc2	škola
pod	pod	k7c4	pod
pravomoc	pravomoc	k1gFnSc4	pravomoc
komisaře	komisař	k1gMnSc2	komisař
školství	školství	k1gNnSc2	školství
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadujul	listopadujout	k5eAaPmAgInS	listopadujout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
<g/>
;	;	kIx,	;
nahrazení	nahrazení	k1gNnSc4	nahrazení
starých	starý	k2eAgInPc2d1	starý
soudních	soudní	k2eAgInPc2d1	soudní
tribunálů	tribunál	k1gInPc2	tribunál
soudy	soud	k1gInPc4	soud
zvolenými	zvolený	k2eAgInPc7d1	zvolený
nebo	nebo	k8xC	nebo
jmenovanými	jmenovaný	k2eAgInPc7d1	jmenovaný
sověty	sovět	k1gInPc7	sovět
bylo	být	k5eAaImAgNnS	být
schváleno	schválit	k5eAaPmNgNnS	schválit
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadujul	listopadujout	k5eAaPmAgInS	listopadujout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g />
.	.	kIx.	.
</s>
<s>
1917	[number]	k4	1917
<g/>
greg	greg	k1gInSc1	greg
<g/>
.	.	kIx.	.
<g/>
;	;	kIx,	;
a	a	k8xC	a
dekrety	dekret	k1gInPc1	dekret
ze	z	k7c2	z
dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosincejul	prosincejout	k5eAaPmAgInS	prosincejout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosincejul	prosincejout	k5eAaPmAgInS	prosincejout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
převedly	převést	k5eAaPmAgFnP	převést
uzavírání	uzavírání	k1gNnSc4	uzavírání
manželství	manželství	k1gNnSc2	manželství
<g/>
,	,	kIx,	,
rozvody	rozvod	k1gInPc4	rozvod
a	a	k8xC	a
registraci	registrace	k1gFnSc4	registrace
porodů	porod	k1gInPc2	porod
a	a	k8xC	a
úmrtí	úmrtí	k1gNnPc2	úmrtí
z	z	k7c2	z
církví	církev	k1gFnPc2	církev
na	na	k7c4	na
úřady	úřad	k1gInPc4	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
deklarována	deklarován	k2eAgFnSc1d1	deklarována
rovnost	rovnost	k1gFnSc1	rovnost
pohlaví	pohlaví	k1gNnSc2	pohlaví
před	před	k7c7	před
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
bylo	být	k5eAaImAgNnS	být
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
oddělení	oddělení	k1gNnSc1	oddělení
církve	církev	k1gFnSc2	církev
od	od	k7c2	od
státu	stát	k1gInSc2	stát
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
únorajul	únorajout	k5eAaPmAgInS	únorajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1918	[number]	k4	1918
<g/>
greg	greg	k1gInSc1	greg
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
gregoriánský	gregoriánský	k2eAgInSc1d1	gregoriánský
kalendář	kalendář	k1gInSc1	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Náboženským	náboženský	k2eAgNnSc7d1	náboženské
sdružením	sdružení	k1gNnSc7	sdružení
a	a	k8xC	a
církvím	církev	k1gFnPc3	církev
bylo	být	k5eAaImAgNnS	být
zakázáno	zakázán	k2eAgNnSc1d1	zakázáno
vlastnit	vlastnit	k5eAaImF	vlastnit
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
prvních	první	k4xOgInPc2	první
dnů	den	k1gInPc2	den
se	se	k3xPyFc4	se
projevoval	projevovat	k5eAaImAgMnS	projevovat
reformní	reformní	k2eAgMnSc1d1	reformní
duch	duch	k1gMnSc1	duch
<g/>
:	:	kIx,	:
nový	nový	k2eAgMnSc1d1	nový
komisař	komisař	k1gMnSc1	komisař
pro	pro	k7c4	pro
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
Anatolij	Anatolij	k1gMnSc1	Anatolij
Lunačarskij	Lunačarskij	k1gMnSc1	Lunačarskij
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
záměr	záměr	k1gInSc4	záměr
naučit	naučit	k5eAaPmF	naučit
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
v	v	k7c6	v
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
číst	číst	k5eAaImF	číst
a	a	k8xC	a
psát	psát	k5eAaImF	psát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
listopadu	listopad	k1gInSc2	listopad
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vláda	vláda	k1gFnSc1	vláda
zamýšlí	zamýšlet	k5eAaImIp3nS	zamýšlet
zavést	zavést	k5eAaPmF	zavést
systém	systém	k1gInSc1	systém
sociálního	sociální	k2eAgNnSc2d1	sociální
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
zahrnující	zahrnující	k2eAgFnSc4d1	zahrnující
nezaměstnanost	nezaměstnanost	k1gFnSc4	nezaměstnanost
<g/>
,	,	kIx,	,
nemoc	nemoc	k1gFnSc4	nemoc
<g/>
,	,	kIx,	,
důchod	důchod	k1gInSc4	důchod
a	a	k8xC	a
zdravotní	zdravotní	k2eAgNnSc4d1	zdravotní
postižení	postižení	k1gNnSc4	postižení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zásobování	zásobování	k1gNnSc1	zásobování
<g/>
,	,	kIx,	,
správa	správa	k1gFnSc1	správa
a	a	k8xC	a
ekonomika	ekonomika	k1gFnSc1	ekonomika
===	===	k?	===
</s>
</p>
<p>
<s>
Zatímco	zatímco	k8xS	zatímco
ve	v	k7c6	v
městech	město	k1gNnPc6	město
nutnost	nutnost	k1gFnSc1	nutnost
udržet	udržet	k5eAaPmF	udržet
podporu	podpora	k1gFnSc4	podpora
pracovníků	pracovník	k1gMnPc2	pracovník
donutila	donutit	k5eAaPmAgFnS	donutit
vládu	vláda	k1gFnSc4	vláda
urychlit	urychlit	k5eAaPmF	urychlit
ovládnutí	ovládnutí	k1gNnSc4	ovládnutí
průmyslu	průmysl	k1gInSc2	průmysl
(	(	kIx(	(
<g/>
moc	moc	k1gFnSc1	moc
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
nad	nad	k7c7	nad
továrnami	továrna	k1gFnPc7	továrna
byla	být	k5eAaImAgFnS	být
legalizována	legalizován	k2eAgFnSc1d1	legalizována
dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadujul	listopadujout	k5eAaPmAgInS	listopadujout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
většina	většina	k1gFnSc1	většina
bolševických	bolševický	k2eAgMnPc2d1	bolševický
vůdců	vůdce	k1gMnPc2	vůdce
včetně	včetně	k7c2	včetně
samotného	samotný	k2eAgMnSc2d1	samotný
Lenina	Lenin	k1gMnSc2	Lenin
upřednostňovala	upřednostňovat	k5eAaImAgFnS	upřednostňovat
pomalejší	pomalý	k2eAgNnSc4d2	pomalejší
tempo	tempo	k1gNnSc4	tempo
a	a	k8xC	a
napřed	napřed	k6eAd1	napřed
spíše	spíše	k9	spíše
jen	jen	k9	jen
dohled	dohled	k1gInSc4	dohled
nad	nad	k7c7	nad
vedoucími	vedoucí	k2eAgMnPc7d1	vedoucí
pracovníky	pracovník	k1gMnPc7	pracovník
než	než	k8xS	než
jejich	jejich	k3xOp3gNnPc2	jejich
nahrazení	nahrazení	k1gNnPc2	nahrazení
–	–	k?	–
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
Dekret	dekret	k1gInSc1	dekret
o	o	k7c6	o
půdě	půda	k1gFnSc6	půda
legitimoval	legitimovat	k5eAaBmAgInS	legitimovat
a	a	k8xC	a
urychlil	urychlit	k5eAaPmAgInS	urychlit
pozemkovou	pozemkový	k2eAgFnSc4d1	pozemková
reformu	reforma	k1gFnSc4	reforma
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
ponechána	ponechat	k5eAaPmNgFnS	ponechat
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
této	tento	k3xDgFnSc2	tento
skutečnosti	skutečnost	k1gFnSc2	skutečnost
měla	mít	k5eAaImAgFnS	mít
revoluce	revoluce	k1gFnSc1	revoluce
na	na	k7c4	na
ruské	ruský	k2eAgNnSc4d1	ruské
zemědělství	zemědělství	k1gNnSc4	zemědělství
jen	jen	k6eAd1	jen
malý	malý	k2eAgInSc1d1	malý
okamžitý	okamžitý	k2eAgInSc1d1	okamžitý
vliv	vliv	k1gInSc1	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělování	rozdělování	k1gNnSc1	rozdělování
půdy	půda	k1gFnSc2	půda
však	však	k9	však
prohloubilo	prohloubit	k5eAaPmAgNnS	prohloubit
nedostatek	nedostatek	k1gInSc4	nedostatek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
velkostatky	velkostatek	k1gInPc1	velkostatek
byly	být	k5eAaImAgInP	být
hlavními	hlavní	k2eAgMnPc7d1	hlavní
dodavateli	dodavatel	k1gMnPc7	dodavatel
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
produktů	produkt	k1gInPc2	produkt
<g/>
;	;	kIx,	;
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
zlepšení	zlepšení	k1gNnSc4	zlepšení
zásobování	zásobování	k1gNnSc2	zásobování
měst	město	k1gNnPc2	město
cestou	cestou	k7c2	cestou
výměny	výměna	k1gFnSc2	výměna
potravin	potravina	k1gFnPc2	potravina
za	za	k7c4	za
vyráběné	vyráběný	k2eAgNnSc4d1	vyráběné
zboží	zboží	k1gNnSc4	zboží
tento	tento	k3xDgInSc4	tento
problém	problém	k1gInSc4	problém
nevyřešily	vyřešit	k5eNaPmAgFnP	vyřešit
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
musela	muset	k5eAaImAgFnS	muset
vysílat	vysílat	k5eAaImF	vysílat
ozbrojené	ozbrojený	k2eAgFnPc4d1	ozbrojená
jednotky	jednotka	k1gFnPc4	jednotka
na	na	k7c4	na
venkov	venkov	k1gInSc4	venkov
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zvýšily	zvýšit	k5eAaPmAgInP	zvýšit
tok	tok	k1gInSc4	tok
jídla	jídlo	k1gNnSc2	jídlo
do	do	k7c2	do
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
snížila	snížit	k5eAaPmAgFnS	snížit
příděly	příděl	k1gInPc4	příděl
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
to	ten	k3xDgNnSc4	ten
předtím	předtím	k6eAd1	předtím
dělala	dělat	k5eAaImAgFnS	dělat
Prozatímní	prozatímní	k2eAgFnSc1d1	prozatímní
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Zhoršení	zhoršení	k1gNnSc1	zhoršení
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
situace	situace	k1gFnSc2	situace
navzdory	navzdory	k7c3	navzdory
zlepšení	zlepšení	k1gNnSc3	zlepšení
právního	právní	k2eAgNnSc2d1	právní
postavení	postavení	k1gNnSc2	postavení
(	(	kIx(	(
<g/>
zákonný	zákonný	k2eAgInSc1d1	zákonný
dohled	dohled	k1gInSc1	dohled
nad	nad	k7c7	nad
průmyslem	průmysl	k1gInSc7	průmysl
<g/>
)	)	kIx)	)
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
brzkému	brzký	k2eAgNnSc3d1	brzké
zklamání	zklamání	k1gNnSc3	zklamání
městského	městský	k2eAgInSc2d1	městský
proletariátu	proletariát	k1gInSc2	proletariát
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
krize	krize	k1gFnSc1	krize
a	a	k8xC	a
vlastní	vlastní	k2eAgFnPc1d1	vlastní
ideologické	ideologický	k2eAgFnPc1d1	ideologická
preference	preference	k1gFnPc1	preference
vlády	vláda	k1gFnSc2	vláda
upřednostňovaly	upřednostňovat	k5eAaImAgFnP	upřednostňovat
přijímání	přijímání	k1gNnSc1	přijímání
centralizačních	centralizační	k2eAgNnPc2d1	centralizační
a	a	k8xC	a
autoritářských	autoritářský	k2eAgNnPc2d1	autoritářské
opatření	opatření	k1gNnPc2	opatření
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
selhala	selhat	k5eAaPmAgFnS	selhat
celostátní	celostátní	k2eAgFnSc1d1	celostátní
stávka	stávka	k1gFnSc1	stávka
úředníků	úředník	k1gMnPc2	úředník
a	a	k8xC	a
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
bolševici	bolševik	k1gMnPc1	bolševik
již	již	k6eAd1	již
ovládali	ovládat	k5eAaImAgMnP	ovládat
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
správu	správa	k1gFnSc4	správa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
upřednostňovala	upřednostňovat	k5eAaImAgFnS	upřednostňovat
konsolidaci	konsolidace	k1gFnSc4	konsolidace
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
se	se	k3xPyFc4	se
několik	několik	k4yIc1	několik
ministerských	ministerský	k2eAgMnPc2d1	ministerský
tajemníků	tajemník	k1gMnPc2	tajemník
tajně	tajně	k6eAd1	tajně
setkalo	setkat	k5eAaPmAgNnS	setkat
v	v	k7c6	v
bytě	byt	k1gInSc6	byt
Sofie	Sofia	k1gFnSc2	Sofia
Paninové	Paninový	k2eAgFnSc2d1	Paninový
<g/>
,	,	kIx,	,
bývalé	bývalý	k2eAgFnSc2d1	bývalá
náměstkyně	náměstkyně	k1gFnSc2	náměstkyně
ministra	ministr	k1gMnSc2	ministr
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jakási	jakýsi	k3yIgFnSc1	jakýsi
alternativní	alternativní	k2eAgFnSc1d1	alternativní
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
oponovat	oponovat	k5eAaImF	oponovat
opatřením	opatření	k1gNnSc7	opatření
Sovnarkomu	sovnarkom	k1gInSc2	sovnarkom
<g/>
,	,	kIx,	,
zabránit	zabránit	k5eAaPmF	zabránit
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
veřejné	veřejný	k2eAgInPc1d1	veřejný
prostředky	prostředek	k1gInPc1	prostředek
dostaly	dostat	k5eAaPmAgInP	dostat
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
bolševiků	bolševik	k1gMnPc2	bolševik
<g/>
,	,	kIx,	,
a	a	k8xC	a
podporovat	podporovat	k5eAaImF	podporovat
odpor	odpor	k1gInSc4	odpor
úředníků	úředník	k1gMnPc2	úředník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
listopadu	listopad	k1gInSc2	listopad
však	však	k9	však
byla	být	k5eAaImAgFnS	být
většina	většina	k1gFnSc1	většina
členů	člen	k1gMnPc2	člen
bývalé	bývalý	k2eAgFnSc2d1	bývalá
vlády	vláda	k1gFnSc2	vláda
propuštěna	propustit	k5eAaPmNgFnS	propustit
z	z	k7c2	z
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
Sovnarkom	sovnarkom	k1gInSc4	sovnarkom
rozpustil	rozpustit	k5eAaPmAgInS	rozpustit
Výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
záchranu	záchrana	k1gFnSc4	záchrana
vlasti	vlast	k1gFnSc2	vlast
a	a	k8xC	a
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
.2	.2	k4	.2
<g/>
.	.	kIx.	.
prosincejul	prosincejout	k5eAaPmAgInS	prosincejout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
rada	rada	k1gFnSc1	rada
národního	národní	k2eAgNnSc2d1	národní
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
<g />
.	.	kIx.	.
</s>
<s>
koordinovala	koordinovat	k5eAaBmAgFnS	koordinovat
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
činnost	činnost	k1gFnSc4	činnost
státu	stát	k1gInSc2	stát
a	a	k8xC	a
znárodnila	znárodnit	k5eAaPmAgFnS	znárodnit
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosincejul	prosincejout	k5eAaPmAgInS	prosincejout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
27	[number]	k4	27
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
byly	být	k5eAaImAgFnP	být
znárodněny	znárodněn	k2eAgInPc4d1	znárodněn
banky	bank	k1gInPc4	bank
a	a	k8xC	a
následující	následující	k2eAgInSc4d1	následující
den	den	k1gInSc4	den
bylo	být	k5eAaImAgNnS	být
zabaveno	zabaven	k2eAgNnSc1d1	zabaveno
veškeré	veškerý	k3xTgNnSc4	veškerý
zlato	zlato	k1gNnSc4	zlato
v	v	k7c6	v
soukromých	soukromý	k2eAgFnPc6d1	soukromá
rukou	ruka	k1gFnPc6	ruka
a	a	k8xC	a
23	[number]	k4	23
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1917	[number]	k4	1917
<g/>
jul	jul	k?	jul
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
5	[number]	k4	5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1918	[number]	k4	1918
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
vláda	vláda	k1gFnSc1	vláda
zakázala	zakázat	k5eAaPmAgFnS	zakázat
obchodování	obchodování	k1gNnSc4	obchodování
s	s	k7c7	s
akciemi	akcie	k1gFnPc7	akcie
a	a	k8xC	a
vyplácení	vyplácení	k1gNnSc1	vyplácení
dividend	dividenda	k1gFnPc2	dividenda
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
lednajul	lednajout	k5eAaPmAgInS	lednajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
10	[number]	k4	10
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1918	[number]	k4	1918
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
vláda	vláda	k1gFnSc1	vláda
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
splácet	splácet	k5eAaImF	splácet
státní	státní	k2eAgInSc4d1	státní
dluh	dluh	k1gInSc4	dluh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kořeny	kořen	k1gInPc1	kořen
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
převzetí	převzetí	k1gNnSc6	převzetí
moci	moc	k1gFnSc2	moc
Leninova	Leninův	k2eAgFnSc1d1	Leninova
vláda	vláda	k1gFnSc1	vláda
rychle	rychle	k6eAd1	rychle
zahájila	zahájit	k5eAaPmAgNnP	zahájit
represivní	represivní	k2eAgNnPc1d1	represivní
opatření	opatření	k1gNnPc1	opatření
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
hluboce	hluboko	k6eAd1	hluboko
poznamenala	poznamenat	k5eAaPmAgFnS	poznamenat
nové	nový	k2eAgNnSc4d1	nové
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
říjnajul	říjnajout	k5eAaPmAgInS	říjnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
</s>
<s>
Sovnarkom	sovnarkom	k1gInSc1	sovnarkom
schválil	schválit	k5eAaPmAgInS	schválit
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
zákon	zákon	k1gInSc4	zákon
<g/>
:	:	kIx,	:
cenzuru	cenzura	k1gFnSc4	cenzura
tisku	tisk	k1gInSc2	tisk
<g/>
,	,	kIx,	,
zdůvodněnou	zdůvodněný	k2eAgFnSc4d1	zdůvodněná
bojem	boj	k1gInSc7	boj
proti	proti	k7c3	proti
nepřátelům	nepřítel	k1gMnPc3	nepřítel
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
revoluční	revoluční	k2eAgFnSc1d1	revoluční
vojenská	vojenský	k2eAgFnSc1d1	vojenská
komise	komise	k1gFnSc1	komise
používaly	používat	k5eAaImAgFnP	používat
násilí	násilí	k1gNnSc4	násilí
proti	proti	k7c3	proti
oponentům	oponent	k1gMnPc3	oponent
a	a	k8xC	a
lidem	člověk	k1gMnPc3	člověk
podezřelým	podezřelý	k2eAgMnPc3d1	podezřelý
<g/>
,	,	kIx,	,
že	že	k8xS	že
jimi	on	k3xPp3gFnPc7	on
jsou	být	k5eAaImIp3nP	být
<g/>
.	.	kIx.	.
</s>
<s>
Pokusy	pokus	k1gInPc1	pokus
levých	levý	k2eAgMnPc2d1	levý
eserů	eser	k1gMnPc2	eser
a	a	k8xC	a
některých	některý	k3yIgMnPc2	některý
bolševiků	bolševik	k1gMnPc2	bolševik
ukončit	ukončit	k5eAaPmF	ukončit
represe	represe	k1gFnPc4	represe
byly	být	k5eAaImAgInP	být
odmítnuty	odmítnut	k2eAgInPc4d1	odmítnut
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
dočasnému	dočasný	k2eAgInSc3d1	dočasný
odchodu	odchod	k1gInSc3	odchod
levých	levý	k2eAgMnPc2d1	levý
eserů	eser	k1gMnPc2	eser
z	z	k7c2	z
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
k	k	k7c3	k
rezignaci	rezignace	k1gFnSc3	rezignace
čtyř	čtyři	k4xCgMnPc2	čtyři
bolševických	bolševický	k2eAgMnPc2d1	bolševický
komisařů	komisař	k1gMnPc2	komisař
<g/>
.	.	kIx.	.
<g/>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Ústavodárného	ústavodárný	k2eAgNnSc2d1	Ústavodárné
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
bolševici	bolševik	k1gMnPc1	bolševik
získali	získat	k5eAaPmAgMnP	získat
velmi	velmi	k6eAd1	velmi
dobré	dobrý	k2eAgInPc4d1	dobrý
výsledky	výsledek	k1gInPc4	výsledek
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
a	a	k8xC	a
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
celkově	celkově	k6eAd1	celkově
prohráli	prohrát	k5eAaPmAgMnP	prohrát
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
zadržet	zadržet	k5eAaPmF	zadržet
některé	některý	k3yIgInPc4	některý
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
oponentů	oponent	k1gMnPc2	oponent
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
kanceláře	kancelář	k1gFnSc2	kancelář
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
byly	být	k5eAaImAgFnP	být
zničeny	zničit	k5eAaPmNgFnP	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadujul	listopadujout	k5eAaPmAgInS	listopadujout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
vláda	vláda	k1gFnSc1	vláda
nařídila	nařídit	k5eAaPmAgFnS	nařídit
zatčení	zatčení	k1gNnSc4	zatčení
některých	některý	k3yIgMnPc2	některý
prominentních	prominentní	k2eAgMnPc2d1	prominentní
konstitučních	konstituční	k2eAgMnPc2d1	konstituční
demokratů	demokrat	k1gMnPc2	demokrat
<g/>
,	,	kIx,	,
označených	označený	k2eAgMnPc2d1	označený
za	za	k7c4	za
"	"	kIx"	"
<g/>
nepřátele	nepřítel	k1gMnPc4	nepřítel
lidu	lid	k1gInSc2	lid
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
strana	strana	k1gFnSc1	strana
byla	být	k5eAaImAgFnS	být
zakázána	zakázat	k5eAaPmNgFnS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Nárůst	nárůst	k1gInSc1	nárůst
represí	represe	k1gFnPc2	represe
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
neúspěšně	úspěšně	k6eNd1	úspěšně
kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
někteří	některý	k3yIgMnPc1	některý
členové	člen	k1gMnPc1	člen
Všeruského	všeruský	k2eAgInSc2d1	všeruský
ústředního	ústřední	k2eAgInSc2d1	ústřední
výkonného	výkonný	k2eAgInSc2d1	výkonný
výboru	výbor	k1gInSc2	výbor
<g/>
,	,	kIx,	,
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
potřebě	potřeba	k1gFnSc3	potřeba
vytvořit	vytvořit	k5eAaPmF	vytvořit
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
účel	účel	k1gInSc4	účel
specializovaný	specializovaný	k2eAgInSc1d1	specializovaný
orgán	orgán	k1gInSc1	orgán
<g/>
:	:	kIx,	:
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosincejul	prosincejout	k5eAaPmAgInS	prosincejout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
samotný	samotný	k2eAgMnSc1d1	samotný
Lenin	Lenin	k1gMnSc1	Lenin
pověřili	pověřit	k5eAaPmAgMnP	pověřit
Felixe	Felix	k1gMnSc4	Felix
Dzeržinského	Dzeržinský	k2eAgMnSc4d1	Dzeržinský
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
připravil	připravit	k5eAaPmAgInS	připravit
návrhy	návrh	k1gInPc4	návrh
pro	pro	k7c4	pro
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
"	"	kIx"	"
<g/>
sabotérům	sabotér	k1gMnPc3	sabotér
a	a	k8xC	a
kontrarevolucionářům	kontrarevolucionář	k1gMnPc3	kontrarevolucionář
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
následující	následující	k2eAgInSc4d1	následující
den	den	k1gInSc4	den
vláda	vláda	k1gFnSc1	vláda
jeho	jeho	k3xOp3gInPc4	jeho
návrhy	návrh	k1gInPc4	návrh
schválila	schválit	k5eAaPmAgFnS	schválit
a	a	k8xC	a
založila	založit	k5eAaPmAgFnS	založit
Čeku	čeka	k1gFnSc4	čeka
(	(	kIx(	(
<g/>
Mimořádnou	mimořádný	k2eAgFnSc4d1	mimořádná
komisi	komise	k1gFnSc4	komise
pro	pro	k7c4	pro
boj	boj	k1gInSc4	boj
s	s	k7c7	s
kontrarevolucí	kontrarevoluce	k1gFnSc7	kontrarevoluce
a	a	k8xC	a
sabotáží	sabotáž	k1gFnSc7	sabotáž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgInSc1d1	hlavní
nástroj	nástroj	k1gInSc1	nástroj
politického	politický	k2eAgInSc2d1	politický
teroru	teror	k1gInSc2	teror
a	a	k8xC	a
zárodek	zárodek	k1gInSc1	zárodek
budoucí	budoucí	k2eAgFnSc2d1	budoucí
politické	politický	k2eAgFnSc2d1	politická
policie	policie	k1gFnSc2	policie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
trestání	trestání	k1gNnSc3	trestání
zločinů	zločin	k1gInPc2	zločin
považovaných	považovaný	k2eAgFnPc2d1	považovaná
za	za	k7c7	za
kontrarevoluční	kontrarevoluční	k2eAgFnSc7d1	kontrarevoluční
byly	být	k5eAaImAgFnP	být
založeny	založit	k5eAaPmNgInP	založit
revoluční	revoluční	k2eAgInPc1d1	revoluční
tribunály	tribunál	k1gInPc1	tribunál
<g/>
,	,	kIx,	,
tvořené	tvořený	k2eAgNnSc1d1	tvořené
soudcem	soudce	k1gMnSc7	soudce
a	a	k8xC	a
šesti	šest	k4xCc7	šest
porotci	porotce	k1gMnPc7	porotce
zvolenými	zvolený	k2eAgInPc7d1	zvolený
sověty	sovět	k1gInPc7	sovět
<g/>
.	.	kIx.	.
<g/>
Vztahy	vztah	k1gInPc1	vztah
se	s	k7c7	s
spojeneckými	spojenecký	k2eAgFnPc7d1	spojenecká
mocnostmi	mocnost	k1gFnPc7	mocnost
byly	být	k5eAaImAgFnP	být
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
špatné	špatná	k1gFnSc2	špatná
<g/>
;	;	kIx,	;
spojenci	spojenec	k1gMnPc1	spojenec
neuznali	uznat	k5eNaPmAgMnP	uznat
novou	nový	k2eAgFnSc4d1	nová
vládu	vláda	k1gFnSc4	vláda
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
rozhořčeni	rozhořčen	k2eAgMnPc1d1	rozhořčen
Leninovým	Leninův	k2eAgNnPc3d1	Leninovo
odmítnutím	odmítnutí	k1gNnPc3	odmítnutí
platit	platit	k5eAaImF	platit
ruský	ruský	k2eAgInSc4d1	ruský
dluh	dluh	k1gInSc4	dluh
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
a	a	k8xC	a
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
menším	malý	k2eAgInPc3d2	menší
diplomatickým	diplomatický	k2eAgInPc3d1	diplomatický
sporům	spor	k1gInPc3	spor
<g/>
.	.	kIx.	.
<g/>
Počáteční	počáteční	k2eAgFnSc1d1	počáteční
opozice	opozice	k1gFnSc1	opozice
vůči	vůči	k7c3	vůči
nové	nový	k2eAgFnSc3d1	nová
vládě	vláda	k1gFnSc3	vláda
však	však	k9	však
byla	být	k5eAaImAgFnS	být
neorganizovaná	organizovaný	k2eNgFnSc1d1	neorganizovaná
a	a	k8xC	a
neměla	mít	k5eNaImAgFnS	mít
podporu	podpora	k1gFnSc4	podpora
lidovch	lidovcha	k1gFnPc2	lidovcha
mas	masa	k1gFnPc2	masa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nejdříve	dříve	k6eAd3	dříve
podporovaly	podporovat	k5eAaImAgFnP	podporovat
vládu	vláda	k1gFnSc4	vláda
Lenina	Lenin	k1gMnSc2	Lenin
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgFnPc1d1	různá
instituce	instituce	k1gFnPc1	instituce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
postavily	postavit	k5eAaPmAgFnP	postavit
proti	proti	k7c3	proti
bolševické	bolševický	k2eAgFnSc3d1	bolševická
vládě	vláda	k1gFnSc3	vláda
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
poraženy	porazit	k5eAaPmNgInP	porazit
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
kontrarevoluci	kontrarevoluce	k1gFnSc4	kontrarevoluce
<g/>
;	;	kIx,	;
oponenti	oponent	k1gMnPc1	oponent
vlády	vláda	k1gFnSc2	vláda
proto	proto	k8xC	proto
vkládali	vkládat	k5eAaImAgMnP	vkládat
naděje	naděje	k1gFnPc4	naděje
do	do	k7c2	do
Ústavodárného	ústavodárný	k2eAgNnSc2d1	Ústavodárné
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
by	by	kYmCp3nS	by
jim	on	k3xPp3gMnPc3	on
mohlo	moct	k5eAaImAgNnS	moct
umožnit	umožnit	k5eAaPmF	umožnit
odstavit	odstavit	k5eAaPmF	odstavit
bolševiky	bolševik	k1gMnPc4	bolševik
od	od	k7c2	od
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
opakovaně	opakovaně	k6eAd1	opakovaně
obhajovali	obhajovat	k5eAaImAgMnP	obhajovat
jeho	jeho	k3xOp3gNnSc4	jeho
konání	konání	k1gNnSc4	konání
a	a	k8xC	a
odsoudili	odsoudit	k5eAaPmAgMnP	odsoudit
zpoždění	zpoždění	k1gNnSc4	zpoždění
schválená	schválený	k2eAgFnSc1d1	schválená
předchozí	předchozí	k2eAgFnSc7d1	předchozí
vládou	vláda	k1gFnSc7	vláda
<g/>
,	,	kIx,	,
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
předat	předat	k5eAaPmF	předat
moc	moc	k6eAd1	moc
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jednom	jeden	k4xCgNnSc6	jeden
zasedání	zasedání	k1gNnSc6	zasedání
bylo	být	k5eAaImAgNnS	být
shromáždění	shromáždění	k1gNnSc1	shromáždění
vládou	vláda	k1gFnSc7	vláda
násilně	násilně	k6eAd1	násilně
rozpuštěno	rozpuštěn	k2eAgNnSc1d1	rozpuštěno
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
opozici	opozice	k1gFnSc4	opozice
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zvážila	zvážit	k5eAaPmAgFnS	zvážit
opuštění	opuštění	k1gNnSc4	opuštění
zákonných	zákonný	k2eAgFnPc2d1	zákonná
metod	metoda	k1gFnPc2	metoda
odporu	odpor	k1gInSc2	odpor
vůči	vůči	k7c3	vůči
bolševické	bolševický	k2eAgFnSc3d1	bolševická
vládě	vláda	k1gFnSc3	vláda
a	a	k8xC	a
urychlilo	urychlit	k5eAaPmAgNnS	urychlit
první	první	k4xOgInPc1	první
střety	střet	k1gInPc1	střet
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Revolución	Revolución	k1gMnSc1	Revolución
de	de	k?	de
Octubre	Octubr	k1gInSc5	Octubr
na	na	k7c6	na
španělské	španělský	k2eAgFnSc6d1	španělská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
DANIELS	DANIELS	kA	DANIELS
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
V.	V.	kA	V.
Red	Red	k1gMnSc1	Red
October	October	k1gMnSc1	October
:	:	kIx,	:
the	the	k?	the
Bolshevik	Bolshevik	k1gInSc1	Bolshevik
Revolution	Revolution	k1gInSc1	Revolution
of	of	k?	of
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
</s>
<s>
Boston	Boston	k1gInSc1	Boston
<g/>
:	:	kIx,	:
Beacon	Beacon	k1gInSc1	Beacon
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
xiv	xiv	k?	xiv
<g/>
,	,	kIx,	,
269	[number]	k4	269
pages	pagesa	k1gFnPc2	pagesa
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
36	[number]	k4	36
<g/>
]	]	kIx)	]
pages	pages	k1gInSc1	pages
of	of	k?	of
plates	plates	k1gInSc1	plates
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
9780807056455	[number]	k4	9780807056455
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FELSTINSKY	FELSTINSKY	kA	FELSTINSKY
<g/>
,	,	kIx,	,
Yuri	Yuri	k1gNnSc2	Yuri
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Bolsheviks	Bolsheviks	k1gInSc1	Bolsheviks
and	and	k?	and
the	the	k?	the
Left	Leftum	k1gNnPc2	Leftum
SRS	SRS	kA	SRS
<g/>
,	,	kIx,	,
October	October	k1gInSc1	October
1917	[number]	k4	1917
<g/>
-July	-Jula	k1gFnSc2	-Jula
1918	[number]	k4	1918
:	:	kIx,	:
toward	towarda	k1gFnPc2	towarda
a	a	k8xC	a
single-party	singlearta	k1gFnSc2	single-parta
dictatorship	dictatorship	k1gMnSc1	dictatorship
<g/>
.	.	kIx.	.
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
disertace	disertace	k1gFnSc2	disertace
<g/>
.	.	kIx.	.
</s>
<s>
Universidad	Universidad	k6eAd1	Universidad
estatal	estatat	k5eAaImAgInS	estatat
de	de	k?	de
Nueva	Nuev	k1gMnSc2	Nuev
Jersey	Jersea	k1gMnSc2	Jersea
<g/>
.	.	kIx.	.
.	.	kIx.	.
</s>
<s>
Oclc	Oclc	k1gFnSc1	Oclc
<g/>
=	=	kIx~	=
<g/>
42033223	[number]	k4	42033223
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
FIGES	FIGES	kA	FIGES
<g/>
,	,	kIx,	,
Orlando	Orlanda	k1gFnSc5	Orlanda
<g/>
.	.	kIx.	.
</s>
<s>
Lidská	lidský	k2eAgFnSc1d1	lidská
tragédie	tragédie	k1gFnSc1	tragédie
:	:	kIx,	:
ruská	ruský	k2eAgFnSc1d1	ruská
revoluce	revoluce	k1gFnSc1	revoluce
1891	[number]	k4	1891
<g/>
-	-	kIx~	-
<g/>
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
:	:	kIx,	:
Beta-Dobrovský	Beta-Dobrovský	k2eAgMnSc1d1	Beta-Dobrovský
;	;	kIx,	;
Ševčík	Ševčík	k1gMnSc1	Ševčík
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
837	[number]	k4	837
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86278	[number]	k4	86278
<g/>
-	-	kIx~	-
<g/>
72	[number]	k4	72
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7291	[number]	k4	7291
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GEYER	GEYER	kA	GEYER
<g/>
,	,	kIx,	,
Dietrich	Dietrich	k1gInSc1	Dietrich
<g/>
.	.	kIx.	.
</s>
<s>
Revolutionary	Revolutionar	k1gInPc1	Revolutionar
Russia	Russium	k1gNnSc2	Russium
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Harvard	Harvard	k1gInSc1	Harvard
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
269	[number]	k4	269
s.	s.	k?	s.
OCLC	OCLC	kA	OCLC
801869546	[number]	k4	801869546
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
The	The	k1gFnSc2	The
Bolshevik	Bolshevika	k1gFnPc2	Bolshevika
Insurrection	Insurrection	k1gInSc1	Insurrection
in	in	k?	in
Petrograd	Petrograd	k1gInSc1	Petrograd
</s>
<s>
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CHAMBERLIN	CHAMBERLIN	kA	CHAMBERLIN
<g/>
,	,	kIx,	,
William	William	k1gInSc4	William
Henry	henry	k1gInSc2	henry
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Russian	Russian	k1gInSc1	Russian
revolution	revolution	k1gInSc1	revolution
<g/>
,	,	kIx,	,
1917	[number]	k4	1917
<g/>
-	-	kIx~	-
<g/>
1918	[number]	k4	1918
<g/>
:	:	kIx,	:
from	from	k1gInSc1	from
the	the	k?	the
overthrow	overthrow	k?	overthrow
of	of	k?	of
the	the	k?	the
czar	czar	k1gInSc1	czar
to	ten	k3xDgNnSc1	ten
the	the	k?	the
assumption	assumption	k1gInSc1	assumption
of	of	k?	of
power	powra	k1gFnPc2	powra
by	by	k9	by
the	the	k?	the
bolsheviks	bolsheviks	k1gInSc1	bolsheviks
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Grosset	Grosset	k1gInSc1	Grosset
&	&	k?	&
Dunlap	Dunlap	k1gInSc1	Dunlap
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
511	[number]	k4	511
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
9780448001883	[number]	k4	9780448001883
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PEREIRA	PEREIRA	kA	PEREIRA
<g/>
,	,	kIx,	,
Norman	Norman	k1gMnSc1	Norman
G.	G.	kA	G.
O.	O.	kA	O.
White	Whit	k1gInSc5	Whit
Siberia	Siberium	k1gNnPc4	Siberium
:	:	kIx,	:
the	the	k?	the
politics	politics	k6eAd1	politics
of	of	k?	of
civil	civil	k1gMnSc1	civil
war	war	k?	war
<g/>
.	.	kIx.	.
</s>
<s>
Montreal	Montreal	k1gInSc1	Montreal
<g/>
:	:	kIx,	:
McGill-Queen	McGill-Queen	k1gInSc1	McGill-Queen
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
University	universita	k1gFnPc1	universita
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
1	[number]	k4	1
online	onlinout	k5eAaPmIp3nS	onlinout
resource	resourka	k1gFnSc3	resourka
(	(	kIx(	(
<g/>
xii	xii	k?	xii
<g/>
,	,	kIx,	,
261	[number]	k4	261
pages	pagesa	k1gFnPc2	pagesa
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
pages	pages	k1gInSc1	pages
of	of	k?	of
plates	plates	k1gInSc1	plates
<g/>
)	)	kIx)	)
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
9780773513495	[number]	k4	9780773513495
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PEZLAROVÁ	PEZLAROVÁ	kA	PEZLAROVÁ
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
<g/>
.	.	kIx.	.
</s>
<s>
Únorová	únorový	k2eAgFnSc1d1	únorová
revoluce	revoluce	k1gFnSc1	revoluce
1917	[number]	k4	1917
a	a	k8xC	a
vznik	vznik	k1gInSc4	vznik
demokratického	demokratický	k2eAgInSc2d1	demokratický
státu	stát	k1gInSc2	stát
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
magisterská	magisterský	k2eAgFnSc1d1	magisterská
<g/>
.	.	kIx.	.
</s>
<s>
Právnická	právnický	k2eAgFnSc1d1	právnická
fakulta	fakulta	k1gFnSc1	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k1gMnSc1	vedoucí
práce	práce	k1gFnSc2	práce
prof.	prof.	kA	prof.
JUDr.	JUDr.	kA	JUDr.
Eduard	Eduard	k1gMnSc1	Eduard
Vlček	Vlček	k1gMnSc1	Vlček
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
<g/>
,	,	kIx,	,
<g/>
.	.	kIx.	.
s.	s.	k?	s.
46	[number]	k4	46
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PIPES	PIPES	kA	PIPES
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
ruské	ruský	k2eAgFnSc2d1	ruská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
396	[number]	k4	396
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7203	[number]	k4	7203
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
81	[number]	k4	81
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RABINOWITCH	RABINOWITCH	kA	RABINOWITCH
<g/>
,	,	kIx,	,
Alexander	Alexandra	k1gFnPc2	Alexandra
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Bolsheviks	Bolsheviks	k1gInSc1	Bolsheviks
come	comat	k5eAaPmIp3nS	comat
to	ten	k3xDgNnSc4	ten
power	power	k1gMnSc1	power
:	:	kIx,	:
the	the	k?	the
revolution	revolution	k1gInSc1	revolution
of	of	k?	of
1917	[number]	k4	1917
in	in	k?	in
Petrograd	Petrograda	k1gFnPc2	Petrograda
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
W.W.	W.W.	k1gFnSc1	W.W.
Norton	Norton	k1gInSc1	Norton
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
xxxiii	xxxiie	k1gFnSc6	xxxiie
<g/>
,	,	kIx,	,
393	[number]	k4	393
pages	pages	k1gInSc1	pages
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
9780393008937	[number]	k4	9780393008937
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RADKEY	RADKEY	kA	RADKEY
<g/>
,	,	kIx,	,
Oliver	Oliver	k1gMnSc1	Oliver
H.	H.	kA	H.
The	The	k1gMnSc1	The
sickle	sickle	k6eAd1	sickle
under	under	k1gMnSc1	under
the	the	k?	the
hammer	hammer	k1gMnSc1	hammer
<g/>
;	;	kIx,	;
The	The	k1gMnSc1	The
Russian	Russian	k1gMnSc1	Russian
Socialist	socialist	k1gMnSc1	socialist
Revolutionaries	Revolutionaries	k1gMnSc1	Revolutionaries
in	in	k?	in
the	the	k?	the
early	earl	k1gMnPc4	earl
months	monthsa	k1gFnPc2	monthsa
of	of	k?	of
the	the	k?	the
Soviet	Soviet	k1gMnSc1	Soviet
rule	rula	k1gFnSc3	rula
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Columbia	Columbia	k1gFnSc1	Columbia
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
525	[number]	k4	525
s.	s.	k?	s.
OCLC	OCLC	kA	OCLC
422729	[number]	k4	422729
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ROSENBERG	ROSENBERG	kA	ROSENBERG
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
G.	G.	kA	G.
Russian	Russian	k1gInSc1	Russian
Liberals	Liberals	k1gInSc1	Liberals
and	and	k?	and
the	the	k?	the
Bolshevik	Bolshevik	k1gInSc1	Bolshevik
Coup	coup	k1gInSc1	coup
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Journal	Journal	k1gMnSc1	Journal
of	of	k?	of
Modern	Modern	k1gInSc1	Modern
History	Histor	k1gInPc1	Histor
<g/>
.	.	kIx.	.
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
40	[number]	k4	40
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
,	,	kIx,	,
s.	s.	k?	s.
328	[number]	k4	328
<g/>
-	-	kIx~	-
<g/>
347	[number]	k4	347
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
WADE	WADE	k?	WADE
<g/>
,	,	kIx,	,
Rex	Rex	k1gMnSc1	Rex
A.	A.	kA	A.
The	The	k1gMnSc1	The
Russian	Russian	k1gMnSc1	Russian
Revolution	Revolution	k1gInSc1	Revolution
<g/>
,	,	kIx,	,
1917	[number]	k4	1917
(	(	kIx(	(
<g/>
New	New	k1gMnSc1	New
Approaches	Approaches	k1gMnSc1	Approaches
to	ten	k3xDgNnSc4	ten
European	European	k1gInSc1	European
History	Histor	k1gInPc1	Histor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1	Cambridge
<g/>
:	:	kIx,	:
Cambridge	Cambridge	k1gFnSc1	Cambridge
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
xvii	xvii	k1gNnSc1	xvii
<g/>
,	,	kIx,	,
337	[number]	k4	337
pages	pages	k1gInSc1	pages
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
9780521425650	[number]	k4	9780521425650
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Říjnová	říjnový	k2eAgFnSc1d1	říjnová
revoluce	revoluce	k1gFnSc1	revoluce
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Velká	velký	k2eAgFnSc1d1	velká
říjnová	říjnový	k2eAgFnSc1d1	říjnová
socialistická	socialistický	k2eAgFnSc1d1	socialistická
revoluce	revoluce	k1gFnSc1	revoluce
–	–	k?	–
video	video	k1gNnSc1	video
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Historický	historický	k2eAgInSc1d1	historický
magazín	magazín	k1gInSc1	magazín
</s>
</p>
