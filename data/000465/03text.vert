<s>
Olymp	Olymp	k1gInSc1	Olymp
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Ό	Ό	k?	Ό
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pohoří	pohoří	k1gNnSc1	pohoří
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
pohoří	pohoří	k1gNnSc2	pohoří
je	být	k5eAaImIp3nS	být
Mytikas	Mytikas	k1gInSc1	Mytikas
(	(	kIx(	(
<g/>
2917	[number]	k4	2917
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyčí	tyčit	k5eAaImIp3nS	tyčit
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
Soluňským	soluňský	k2eAgInSc7d1	soluňský
zálivem	záliv	k1gInSc7	záliv
mezi	mezi	k7c7	mezi
údolími	údolí	k1gNnPc7	údolí
řek	řek	k1gMnSc1	řek
Pinios	Pinios	k1gMnSc1	Pinios
a	a	k8xC	a
Aliakmon	Aliakmon	k1gMnSc1	Aliakmon
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc4d1	severní
a	a	k8xC	a
západní	západní	k2eAgFnSc4d1	západní
hranici	hranice	k1gFnSc4	hranice
pohoří	pohoří	k1gNnSc2	pohoří
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
tok	tok	k1gInSc1	tok
řeky	řeka	k1gFnSc2	řeka
Mavroneri	Mavroner	k1gFnSc2	Mavroner
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
tak	tak	k6eAd1	tak
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
Olymp	Olymp	k1gInSc4	Olymp
od	od	k7c2	od
masivu	masiv	k1gInSc2	masiv
Pieria	Pierium	k1gNnSc2	Pierium
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
tvoří	tvořit	k5eAaImIp3nP	tvořit
hranici	hranice	k1gFnSc4	hranice
pohoří	pohoří	k1gNnSc2	pohoří
údolí	údolí	k1gNnSc2	údolí
řeky	řeka	k1gFnSc2	řeka
Pleria	Plerium	k1gNnSc2	Plerium
<g/>
,	,	kIx,	,
dělící	dělící	k2eAgFnSc1d1	dělící
nižší	nízký	k2eAgFnSc1d2	nižší
část	část	k1gFnSc1	část
pohoří	pohoří	k1gNnSc2	pohoří
-	-	kIx~	-
Kato	Kato	k1gMnSc1	Kato
Olympos	Olympos	k1gMnSc1	Olympos
-	-	kIx~	-
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
Ossa	Oss	k1gInSc2	Oss
<g/>
.	.	kIx.	.
</s>
<s>
Soluňský	soluňský	k2eAgInSc1d1	soluňský
záliv	záliv	k1gInSc1	záliv
ohraničuje	ohraničovat	k5eAaImIp3nS	ohraničovat
Olymp	Olymp	k1gInSc4	Olymp
od	od	k7c2	od
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Složení	složení	k1gNnSc1	složení
horstva	horstvo	k1gNnSc2	horstvo
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
několika	několik	k4yIc2	několik
plášti	plášť	k1gInPc7	plášť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
krystalické	krystalický	k2eAgFnSc2d1	krystalická
horniny	hornina	k1gFnSc2	hornina
(	(	kIx(	(
<g/>
žula	žula	k1gFnSc1	žula
<g/>
,	,	kIx,	,
břidlice	břidlice	k1gFnSc1	břidlice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
vnějších	vnější	k2eAgFnPc6d1	vnější
vrstvách	vrstva	k1gFnPc6	vrstva
obaleny	obalen	k2eAgInPc1d1	obalen
vápencovými	vápencový	k2eAgFnPc7d1	vápencová
usazeninami	usazenina	k1gFnPc7	usazenina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pohoří	pohoří	k1gNnSc6	pohoří
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
patrno	patrn	k2eAgNnSc1d1	patrno
původní	původní	k2eAgNnSc1d1	původní
zalednění	zalednění	k1gNnSc1	zalednění
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Výrazné	výrazný	k2eAgInPc1d1	výrazný
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
stopy	stopa	k1gFnPc4	stopa
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
silného	silný	k2eAgNnSc2d1	silné
zvětrávání	zvětrávání	k1gNnSc2	zvětrávání
hornin	hornina	k1gFnPc2	hornina
a	a	k8xC	a
svahů	svah	k1gInPc2	svah
<g/>
,	,	kIx,	,
nápadné	nápadný	k2eAgFnPc1d1	nápadná
věže	věž	k1gFnPc1	věž
a	a	k8xC	a
skalní	skalní	k2eAgInPc1d1	skalní
útvary	útvar	k1gInPc1	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Geograficky	geograficky	k6eAd1	geograficky
je	být	k5eAaImIp3nS	být
masiv	masiv	k1gInSc1	masiv
řazen	řadit	k5eAaImNgInS	řadit
do	do	k7c2	do
Thrácko-makedonské	thráckoakedonský	k2eAgFnSc2d1	thrácko-makedonský
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Pohoří	pohoří	k1gNnSc1	pohoří
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
jediným	jediný	k2eAgInSc7d1	jediný
<g/>
,	,	kIx,	,
vějířovitě	vějířovitě	k6eAd1	vějířovitě
tvarovaným	tvarovaný	k2eAgInSc7d1	tvarovaný
hřebenem	hřeben	k1gInSc7	hřeben
skládajícím	skládající	k2eAgInSc7d1	skládající
se	se	k3xPyFc4	se
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
vrcholů	vrchol	k1gInPc2	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
hřeben	hřeben	k1gInSc1	hřeben
výrazně	výrazně	k6eAd1	výrazně
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
blízké	blízký	k2eAgNnSc4d1	blízké
údolí	údolí	k1gNnSc4	údolí
Kania	Kanius	k1gMnSc2	Kanius
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
dominantu	dominanta	k1gFnSc4	dominanta
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
hřeben	hřeben	k1gInSc1	hřeben
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
sedmi	sedm	k4xCc7	sedm
nejvyššími	vysoký	k2eAgInPc7d3	Nejvyšší
vrcholy	vrchol	k1gInPc7	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgInPc2d1	další
46	[number]	k4	46
vrcholů	vrchol	k1gInPc2	vrchol
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Olymp	Olymp	k1gInSc1	Olymp
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
hranici	hranice	k1gFnSc4	hranice
2000	[number]	k4	2000
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
47	[number]	k4	47
má	mít	k5eAaImIp3nS	mít
výšku	výška	k1gFnSc4	výška
mezi	mezi	k7c7	mezi
1000	[number]	k4	1000
<g/>
-	-	kIx~	-
<g/>
2000	[number]	k4	2000
metry	metr	k1gInPc4	metr
<g/>
.	.	kIx.	.
</s>
<s>
Mytikas	Mytikas	k1gInSc1	Mytikas
(	(	kIx(	(
<g/>
2917	[number]	k4	2917
m	m	kA	m
<g/>
)	)	kIx)	)
Stefani	Stefaň	k1gFnSc3	Stefaň
(	(	kIx(	(
<g/>
2911	[number]	k4	2911
m	m	kA	m
<g/>
)	)	kIx)	)
Skolios	skoliosa	k1gFnPc2	skoliosa
(	(	kIx(	(
<g/>
2903	[number]	k4	2903
m	m	kA	m
<g/>
)	)	kIx)	)
Skala	Skala	k1gMnSc1	Skala
(	(	kIx(	(
<g/>
2866	[number]	k4	2866
m	m	kA	m
<g/>
)	)	kIx)	)
Agios	Agios	k1gMnSc1	Agios
Antonis	Antonis	k1gFnSc2	Antonis
(	(	kIx(	(
<g/>
2815	[number]	k4	2815
m	m	kA	m
<g/>
)	)	kIx)	)
-	-	kIx~	-
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
je	být	k5eAaImIp3nS	být
meteorologická	meteorologický	k2eAgFnSc1d1	meteorologická
stanice	stanice	k1gFnSc1	stanice
a	a	k8xC	a
horská	horský	k2eAgFnSc1d1	horská
služba	služba	k1gFnSc1	služba
Profitis	Profitis	k1gFnSc1	Profitis
Ilias	Ilias	k1gFnSc1	Ilias
(	(	kIx(	(
<g/>
2803	[number]	k4	2803
<g />
.	.	kIx.	.
</s>
<s>
m	m	kA	m
<g/>
)	)	kIx)	)
-	-	kIx~	-
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
kaple	kaple	k1gFnSc1	kaple
Toumba	Toumba	k1gFnSc1	Toumba
(	(	kIx(	(
<g/>
2785	[number]	k4	2785
m	m	kA	m
<g/>
)	)	kIx)	)
Okolí	okolí	k1gNnSc1	okolí
hory	hora	k1gFnSc2	hora
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
prohlášeno	prohlásit	k5eAaPmNgNnS	prohlásit
za	za	k7c4	za
národní	národní	k2eAgInSc4d1	národní
park	park	k1gInSc4	park
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
bohaté	bohatý	k2eAgFnSc2d1	bohatá
fauny	fauna	k1gFnSc2	fauna
(	(	kIx(	(
<g/>
vlk	vlk	k1gMnSc1	vlk
<g/>
,	,	kIx,	,
dravci	dravec	k1gMnPc7	dravec
<g/>
)	)	kIx)	)
a	a	k8xC	a
flory	flora	k1gFnPc1	flora
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc2	jenž
jsou	být	k5eAaImIp3nP	být
zastoupeny	zastoupit	k5eAaPmNgInP	zastoupit
i	i	k9	i
endemické	endemický	k2eAgInPc1d1	endemický
druhy	druh	k1gInPc1	druh
(	(	kIx(	(
<g/>
borovice	borovice	k1gFnSc1	borovice
černá	černá	k1gFnSc1	černá
<g/>
,	,	kIx,	,
druhy	druh	k1gInPc1	druh
alpínek	alpínka	k1gFnPc2	alpínka
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
Řeků	Řek	k1gMnPc2	Řek
má	mít	k5eAaImIp3nS	mít
Olympos	Olympos	k1gInSc4	Olympos
až	až	k9	až
posvátný	posvátný	k2eAgInSc4d1	posvátný
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Olymp	Olymp	k1gInSc1	Olymp
je	být	k5eAaImIp3nS	být
také	také	k9	také
známý	známý	k2eAgMnSc1d1	známý
z	z	k7c2	z
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
jako	jako	k8xC	jako
sídlo	sídlo	k1gNnSc1	sídlo
řeckých	řecký	k2eAgMnPc2d1	řecký
bohů	bůh	k1gMnPc2	bůh
<g/>
:	:	kIx,	:
Dia	Dia	k1gFnPc1	Dia
<g/>
,	,	kIx,	,
Afrodíté	Afrodítý	k2eAgFnPc1d1	Afrodítý
<g/>
,	,	kIx,	,
Héra	Héra	k1gFnSc1	Héra
<g/>
,	,	kIx,	,
Athény	Athéna	k1gFnPc1	Athéna
<g/>
,	,	kIx,	,
Herma	Hermes	k1gMnSc2	Hermes
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
zuřila	zuřit	k5eAaImAgFnS	zuřit
válka	válka	k1gFnSc1	válka
mezi	mezi	k7c7	mezi
bohy	bůh	k1gMnPc7	bůh
a	a	k8xC	a
Titány	Titán	k1gMnPc7	Titán
<g/>
,	,	kIx,	,
bohové	bůh	k1gMnPc1	bůh
se	se	k3xPyFc4	se
usadili	usadit	k5eAaPmAgMnP	usadit
na	na	k7c6	na
hoře	hora	k1gFnSc6	hora
Olymp	Olymp	k1gInSc4	Olymp
a	a	k8xC	a
Titáni	Titán	k1gMnPc1	Titán
na	na	k7c6	na
hoře	hora	k1gFnSc6	hora
Othrys	Othrysa	k1gFnPc2	Othrysa
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
novodobý	novodobý	k2eAgInSc1d1	novodobý
zdokumentovaný	zdokumentovaný	k2eAgInSc1d1	zdokumentovaný
výstup	výstup	k1gInSc1	výstup
na	na	k7c4	na
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
bod	bod	k1gInSc4	bod
masivu	masiv	k1gInSc2	masiv
Olympu	Olymp	k1gInSc2	Olymp
Mytikas	Mytikasa	k1gFnPc2	Mytikasa
uskutečnili	uskutečnit	k5eAaPmAgMnP	uskutečnit
v	v	k7c6	v
r.	r.	kA	r.
1913	[number]	k4	1913
Švýcaři	Švýcar	k1gMnPc1	Švýcar
Daniel	Daniel	k1gMnSc1	Daniel
Baud	baud	k1gInSc4	baud
Bovy	Bova	k1gFnSc2	Bova
a	a	k8xC	a
Frédéric	Frédérice	k1gFnPc2	Frédérice
Boissonas	Boissonasa	k1gFnPc2	Boissonasa
<g/>
,	,	kIx,	,
doprovázení	doprovázení	k1gNnPc2	doprovázení
Řekem	Řek	k1gMnSc7	Řek
Christosem	Christos	k1gMnSc7	Christos
Kakalosem	Kakalos	k1gMnSc7	Kakalos
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc1d1	poslední
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
olympských	olympský	k2eAgInPc2d1	olympský
štítů	štít	k1gInPc2	štít
<g/>
,	,	kIx,	,
Stefani	Stefaň	k1gFnSc3	Stefaň
<g/>
,	,	kIx,	,
zdolal	zdolat	k5eAaPmAgMnS	zdolat
v	v	k7c6	v
r.	r.	kA	r.
1921	[number]	k4	1921
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
topograf	topograf	k1gMnSc1	topograf
M.	M.	kA	M.
Kurz	Kurz	k1gMnSc1	Kurz
<g/>
,	,	kIx,	,
doprovázený	doprovázený	k2eAgMnSc1d1	doprovázený
opět	opět	k6eAd1	opět
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
Kakalosem	Kakalos	k1gMnSc7	Kakalos
<g/>
.	.	kIx.	.
</s>
<s>
Řecké	řecký	k2eAgFnPc1d1	řecká
hory	hora	k1gFnPc1	hora
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Olymp	Olymp	k1gInSc1	Olymp
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Olymp	Olymp	k1gInSc1	Olymp
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
