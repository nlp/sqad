<p>
<s>
Royal	Royal	k1gInSc1	Royal
Air	Air	k1gFnSc2	Air
Force	force	k1gFnSc2	force
(	(	kIx(	(
<g/>
RAF	raf	k0	raf
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Královské	královský	k2eAgNnSc1d1	královské
letectvo	letectvo	k1gNnSc1	letectvo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vojenské	vojenský	k2eAgNnSc1d1	vojenské
letectvo	letectvo	k1gNnSc1	letectvo
britských	britský	k2eAgFnPc2d1	britská
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
RAF	raf	k0	raf
==	==	k?	==
</s>
</p>
<p>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejstarší	starý	k2eAgNnSc4d3	nejstarší
nezávislé	závislý	k2eNgNnSc4d1	nezávislé
vojenské	vojenský	k2eAgNnSc4d1	vojenské
letectvo	letectvo	k1gNnSc4	letectvo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1918	[number]	k4	1918
sloučením	sloučení	k1gNnSc7	sloučení
Royal	Royal	k1gInSc1	Royal
Flying	Flying	k1gInSc1	Flying
Corps	corps	k1gInSc1	corps
a	a	k8xC	a
Royal	Royal	k1gInSc1	Royal
Naval	navalit	k5eAaPmRp2nS	navalit
Air	Air	k1gFnPc2	Air
Service	Service	k1gFnPc4	Service
<g/>
.	.	kIx.	.
</s>
<s>
RAF	raf	k0	raf
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
hrálo	hrát	k5eAaImAgNnS	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
v	v	k7c6	v
britské	britský	k2eAgFnSc6d1	britská
vojenské	vojenský	k2eAgFnSc6d1	vojenská
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
především	především	k9	především
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
července	červenec	k1gInSc2	červenec
1940	[number]	k4	1940
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
probíhala	probíhat	k5eAaImAgFnS	probíhat
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
bitva	bitva	k1gFnSc1	bitva
o	o	k7c6	o
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
RAF	raf	k0	raf
se	se	k3xPyFc4	se
ubránila	ubránit	k5eAaPmAgFnS	ubránit
mnohonásobné	mnohonásobný	k2eAgFnPc1d1	mnohonásobná
přesile	přesila	k1gFnSc3	přesila
Luftwaffe	Luftwaff	k1gInSc5	Luftwaff
a	a	k8xC	a
zabránila	zabránit	k5eAaPmAgFnS	zabránit
tak	tak	k9	tak
Hitlerovi	Hitlerův	k2eAgMnPc1d1	Hitlerův
podniknout	podniknout	k5eAaPmF	podniknout
operaci	operace	k1gFnSc3	operace
Lvoun	lvoun	k1gMnSc1	lvoun
-	-	kIx~	-
invazi	invaze	k1gFnSc4	invaze
do	do	k7c2	do
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Bojovalo	bojovat	k5eAaImAgNnS	bojovat
se	se	k3xPyFc4	se
víceméně	víceméně	k9	víceméně
nad	nad	k7c7	nad
jižní	jižní	k2eAgFnSc7d1	jižní
Anglií	Anglie	k1gFnSc7	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
RAF	raf	k0	raf
byly	být	k5eAaImAgFnP	být
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
i	i	k8xC	i
československé	československý	k2eAgFnSc2d1	Československá
perutě	peruť	k1gFnSc2	peruť
-	-	kIx~	-
310	[number]	k4	310
<g/>
.	.	kIx.	.
stíhací	stíhací	k2eAgInPc1d1	stíhací
<g/>
,	,	kIx,	,
312	[number]	k4	312
<g/>
.	.	kIx.	.
stíhací	stíhací	k2eAgInPc1d1	stíhací
<g/>
,	,	kIx,	,
313	[number]	k4	313
<g/>
.	.	kIx.	.
stíhací	stíhací	k2eAgFnSc2d1	stíhací
a	a	k8xC	a
311	[number]	k4	311
<g/>
.	.	kIx.	.
bombardovací	bombardovací	k2eAgFnSc2d1	bombardovací
<g/>
.	.	kIx.	.
</s>
<s>
RAF	raf	k0	raf
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
i	i	k9	i
dalších	další	k2eAgInPc2d1	další
konfliktů	konflikt	k1gInPc2	konflikt
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byla	být	k5eAaImAgFnS	být
např.	např.	kA	např.
korejská	korejský	k2eAgFnSc1d1	Korejská
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
válka	válka	k1gFnSc1	válka
o	o	k7c4	o
Falklandy	Falklanda	k1gFnPc4	Falklanda
či	či	k8xC	či
invaze	invaze	k1gFnPc4	invaze
do	do	k7c2	do
Iráku	Irák	k1gInSc2	Irák
<g/>
.	.	kIx.	.
</s>
<s>
RAF	raf	k0	raf
s	s	k7c7	s
1114	[number]	k4	1114
letouny	letoun	k1gInPc7	letoun
a	a	k8xC	a
46	[number]	k4	46
800	[number]	k4	800
muži	muž	k1gMnPc7	muž
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgFnPc4d3	veliký
letecké	letecký	k2eAgFnPc4d1	letecká
síly	síla	k1gFnPc4	síla
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
technologicky	technologicky	k6eAd1	technologicky
nejvyspělejších	vyspělý	k2eAgFnPc2d3	nejvyspělejší
<g/>
,	,	kIx,	,
tuto	tento	k3xDgFnSc4	tento
pozici	pozice	k1gFnSc4	pozice
významně	významně	k6eAd1	významně
upevnilo	upevnit	k5eAaPmAgNnS	upevnit
nákupem	nákup	k1gInSc7	nákup
232	[number]	k4	232
stíhaček	stíhačka	k1gFnPc2	stíhačka
Eurofighter	Eurofighter	k1gMnSc1	Eurofighter
Typhoon	Typhoon	k1gMnSc1	Typhoon
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gMnPc7	jeho
příslušníky	příslušník	k1gMnPc7	příslušník
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vzniku	vznik	k1gInSc2	vznik
patřil	patřit	k5eAaImAgMnS	patřit
např.	např.	kA	např.
Henry	Henry	k1gMnSc1	Henry
Allingham	Allingham	k1gInSc1	Allingham
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zemřel	zemřít	k5eAaPmAgInS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
113	[number]	k4	113
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Janoušek	Janoušek	k1gMnSc1	Janoušek
byl	být	k5eAaImAgMnS	být
jediným	jediný	k2eAgMnSc7d1	jediný
Čechoslovákem	Čechoslovák	k1gMnSc7	Čechoslovák
který	který	k3yIgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
RAF	raf	k0	raf
hodnost	hodnost	k1gFnSc4	hodnost
maršála	maršál	k1gMnSc2	maršál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Čechoslováci	Čechoslovák	k1gMnPc1	Čechoslovák
v	v	k7c6	v
RAF	raf	k0	raf
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Britském	britský	k2eAgNnSc6d1	Britské
královském	královský	k2eAgNnSc6d1	královské
letectvu	letectvo	k1gNnSc6	letectvo
sloužilo	sloužit	k5eAaImAgNnS	sloužit
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
celkem	celkem	k6eAd1	celkem
2402	[number]	k4	2402
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
Slováků	Slovák	k1gMnPc2	Slovák
<g/>
.	.	kIx.	.
</s>
<s>
Pětina	pětina	k1gFnSc1	pětina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
konce	konec	k1gInSc2	konec
války	válka	k1gFnSc2	válka
nedočkala	dočkat	k5eNaPmAgFnS	dočkat
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
všichni	všechen	k3xTgMnPc1	všechen
ale	ale	k9	ale
byli	být	k5eAaImAgMnP	být
piloty	pilota	k1gFnPc4	pilota
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
známých	známý	k2eAgMnPc2d1	známý
mužů	muž	k1gMnPc2	muž
v	v	k7c6	v
modrém	modré	k1gNnSc6	modré
tam	tam	k6eAd1	tam
byl	být	k5eAaImAgInS	být
pozemní	pozemní	k2eAgInSc1d1	pozemní
personál	personál	k1gInSc1	personál
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc1d1	další
muži	muž	k1gMnPc1	muž
působili	působit	k5eAaImAgMnP	působit
na	na	k7c6	na
administrativních	administrativní	k2eAgFnPc6d1	administrativní
pozicích	pozice	k1gFnPc6	pozice
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
styční	styčný	k2eAgMnPc1d1	styčný
důstojníci	důstojník	k1gMnPc1	důstojník
u	u	k7c2	u
královského	královský	k2eAgNnSc2d1	královské
letectva	letectvo	k1gNnSc2	letectvo
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
výcvikových	výcvikový	k2eAgFnPc6d1	výcviková
jednotkách	jednotka	k1gFnPc6	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
RAF	raf	k0	raf
byli	být	k5eAaImAgMnP	být
také	také	k9	také
letci	letec	k1gMnPc1	letec
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Čechoslováci	Čechoslovák	k1gMnPc1	Čechoslovák
si	se	k3xPyFc3	se
ale	ale	k9	ale
u	u	k7c2	u
britského	britský	k2eAgNnSc2d1	Britské
letectva	letectvo	k1gNnSc2	letectvo
vydobyli	vydobýt	k5eAaPmAgMnP	vydobýt
vynikající	vynikající	k2eAgFnSc4d1	vynikající
pověst	pověst	k1gFnSc4	pověst
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jejich	jejich	k3xOp3gMnPc1	jejich
polští	polský	k2eAgMnPc1d1	polský
kolegové	kolega	k1gMnPc1	kolega
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
letci	letec	k1gMnPc1	letec
se	se	k3xPyFc4	se
po	po	k7c6	po
únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
stali	stát	k5eAaPmAgMnP	stát
oběťmi	oběť	k1gFnPc7	oběť
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Přicházeli	přicházet	k5eAaImAgMnP	přicházet
ze	z	k7c2	z
Západu	západ	k1gInSc2	západ
a	a	k8xC	a
mnozí	mnohý	k2eAgMnPc1d1	mnohý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
měli	mít	k5eAaImAgMnP	mít
za	za	k7c4	za
manželky	manželka	k1gFnPc4	manželka
Angličanky	Angličanka	k1gFnSc2	Angličanka
<g/>
.	.	kIx.	.
</s>
<s>
Dávno	dávno	k6eAd1	dávno
před	před	k7c7	před
komunistickým	komunistický	k2eAgInSc7d1	komunistický
pučem	puč	k1gInSc7	puč
dávali	dávat	k5eAaImAgMnP	dávat
otevřeně	otevřeně	k6eAd1	otevřeně
najevo	najevo	k6eAd1	najevo
nespokojenost	nespokojenost	k1gFnSc4	nespokojenost
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
fungovala	fungovat	k5eAaImAgFnS	fungovat
poválečná	poválečný	k2eAgFnSc1d1	poválečná
limitovaná	limitovaný	k2eAgFnSc1d1	limitovaná
demokracie	demokracie	k1gFnSc1	demokracie
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
za	za	k7c4	za
všechny	všechen	k3xTgFnPc4	všechen
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
známý	známý	k2eAgMnSc1d1	známý
protektorátní	protektorátní	k2eAgMnSc1d1	protektorátní
odbojář	odbojář	k1gMnSc1	odbojář
Jan	Jan	k1gMnSc1	Jan
Smudek	Smudek	k1gMnSc1	Smudek
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Český	český	k2eAgInSc1d1	český
personál	personál	k1gInSc1	personál
sloužící	sloužící	k2eAgInSc1d1	sloužící
u	u	k7c2	u
RAF	raf	k0	raf
připomíná	připomínat	k5eAaImIp3nS	připomínat
pražský	pražský	k2eAgInSc1d1	pražský
Památník	památník	k1gInSc1	památník
Okřídleného	okřídlený	k2eAgMnSc2d1	okřídlený
lva	lev	k1gMnSc2	lev
<g/>
.	.	kIx.	.
</s>
<s>
Padlé	padlý	k2eAgMnPc4d1	padlý
letce	letec	k1gMnPc4	letec
připomíná	připomínat	k5eAaImIp3nS	připomínat
památník	památník	k1gInSc1	památník
v	v	k7c6	v
Praze-Dejvicích	Praze-Dejvice	k1gFnPc6	Praze-Dejvice
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
9.11	[number]	k4	9.11
<g/>
.2015	.2015	k4	.2015
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
na	na	k7c6	na
budově	budova	k1gFnSc6	budova
bývalých	bývalý	k2eAgFnPc2d1	bývalá
kasáren	kasárny	k1gFnPc2	kasárny
odhalena	odhalit	k5eAaPmNgFnS	odhalit
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
věnovaná	věnovaný	k2eAgFnSc1d1	věnovaná
místním	místní	k2eAgMnPc3d1	místní
rodákům	rodák	k1gMnPc3	rodák
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
RAF	raf	k0	raf
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
letadel	letadlo	k1gNnPc2	letadlo
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Royal	Royal	k1gInSc1	Royal
Air	Air	k1gFnSc2	Air
Force	force	k1gFnSc2	force
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Chaz	Chaz	k1gMnSc1	Chaz
Bowyer	Bowyer	k1gMnSc1	Bowyer
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
RAF	raf	k0	raf
<g/>
,	,	kIx,	,
Columbus	Columbus	k1gMnSc1	Columbus
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-901727-4-1	[number]	k4	80-901727-4-1
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Michl	Michl	k1gMnSc1	Michl
<g/>
:	:	kIx,	:
Cizinci	cizinec	k1gMnPc1	cizinec
v	v	k7c6	v
RAF	raf	k0	raf
–	–	k?	–
stíhači	stíhač	k1gMnPc7	stíhač
z	z	k7c2	z
okupované	okupovaný	k2eAgFnSc2d1	okupovaná
Evropy	Evropa	k1gFnSc2	Evropa
od	od	k7c2	od
obrany	obrana	k1gFnSc2	obrana
k	k	k7c3	k
vítězství	vítězství	k1gNnSc3	vítězství
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Naše	náš	k3xOp1gNnSc1	náš
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-206-0930-4	[number]	k4	978-80-206-0930-4
</s>
</p>
<p>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Hurt	Hurt	k1gMnSc1	Hurt
<g/>
:	:	kIx,	:
Češi	Čech	k1gMnPc1	Čech
a	a	k8xC	a
Slováci	Slovák	k1gMnPc1	Slovák
v	v	k7c6	v
RAF	raf	k0	raf
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
Computer	computer	k1gInSc1	computer
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-251-0803-1	[number]	k4	80-251-0803-1
</s>
</p>
<p>
<s>
Alan	Alan	k1gMnSc1	Alan
Hall	Hall	k1gMnSc1	Hall
<g/>
:	:	kIx,	:
RAF	raf	k0	raf
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
Naše	náš	k3xOp1gNnSc1	náš
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-206-1326-4	[number]	k4	978-80-206-1326-4
</s>
</p>
<p>
<s>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Fejfar	Fejfar	k1gMnSc1	Fejfar
<g/>
:	:	kIx,	:
Deník	deník	k1gInSc1	deník
Stíhače	stíhač	k1gMnSc2	stíhač
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Kruh	kruh	k1gInSc1	kruh
<g/>
,	,	kIx,	,
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Army	Army	k1gInPc1	Army
Air	Air	k1gFnSc1	Air
Corps	corps	k1gInSc1	corps
</s>
</p>
<p>
<s>
Fleet	Fleet	k1gMnSc1	Fleet
Air	Air	k1gMnSc1	Air
Arm	Arm	k1gMnSc1	Arm
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
perutí	peruť	k1gFnPc2	peruť
RAF	raf	k0	raf
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Royal	Royal	k1gMnSc1	Royal
Air	Air	k1gFnSc4	Air
Force	force	k1gFnSc2	force
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Royal	Royal	k1gMnSc1	Royal
Air	Air	k1gMnSc2	Air
Force	force	k1gFnSc2	force
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
Royal	Royal	k1gInSc1	Royal
Air	Air	k1gFnSc2	Air
Force	force	k1gFnSc2	force
</s>
</p>
