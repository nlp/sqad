<p>
<s>
Pád	Pád	k1gInSc1	Pád
je	být	k5eAaImIp3nS	být
morfologická	morfologický	k2eAgFnSc1d1	morfologická
kategorie	kategorie	k1gFnSc1	kategorie
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
flexivní	flexivní	k2eAgInPc1d1	flexivní
jazyky	jazyk	k1gInPc1	jazyk
obvykle	obvykle	k6eAd1	obvykle
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
vztah	vztah	k1gInSc4	vztah
jmen	jméno	k1gNnPc2	jméno
(	(	kIx(	(
<g/>
nomin	nomin	k1gInSc1	nomin
<g/>
)	)	kIx)	)
ke	k	k7c3	k
slovesu	sloveso	k1gNnSc3	sloveso
či	či	k8xC	či
jiným	jiný	k2eAgInPc3d1	jiný
větným	větný	k2eAgInPc3d1	větný
členům	člen	k1gInPc3	člen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čeština	čeština	k1gFnSc1	čeština
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
7	[number]	k4	7
pádů	pád	k1gInPc2	pád
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
některé	některý	k3yIgInPc4	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
chybí	chybět	k5eAaImIp3nS	chybět
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
řada	řada	k1gFnSc1	řada
pádů	pád	k1gInPc2	pád
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
nezná	znát	k5eNaImIp3nS	znát
čeština	čeština	k1gFnSc1	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
uváděné	uváděný	k2eAgNnSc1d1	uváděné
pořadí	pořadí	k1gNnSc1	pořadí
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
mluvnicích	mluvnice	k1gFnPc6	mluvnice
jiných	jiný	k2eAgInPc2d1	jiný
jazyků	jazyk	k1gInPc2	jazyk
od	od	k7c2	od
češtiny	čeština	k1gFnSc2	čeština
odlišné	odlišný	k2eAgFnSc2d1	odlišná
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
porovnávání	porovnávání	k1gNnSc4	porovnávání
jazyků	jazyk	k1gInPc2	jazyk
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
než	než	k8xS	než
české	český	k2eAgNnSc1d1	české
číslování	číslování	k1gNnSc1	číslování
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
pád	pád	k1gInSc1	pád
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
.	.	kIx.	.
pád	pád	k1gInSc1	pád
<g/>
)	)	kIx)	)
hodí	hodit	k5eAaImIp3nP	hodit
slovní	slovní	k2eAgInPc1d1	slovní
názvy	název	k1gInPc1	název
pádů	pád	k1gInPc2	pád
odvozené	odvozený	k2eAgInPc1d1	odvozený
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
ani	ani	k8xC	ani
funkce	funkce	k1gFnSc1	funkce
pádu	pád	k1gInSc2	pád
s	s	k7c7	s
konkrétním	konkrétní	k2eAgNnSc7d1	konkrétní
jménem	jméno	k1gNnSc7	jméno
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
podobná	podobný	k2eAgFnSc1d1	podobná
<g/>
,	,	kIx,	,
nebývá	bývat	k5eNaImIp3nS	bývat
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
různých	různý	k2eAgInPc6d1	různý
jazycích	jazyk	k1gInPc6	jazyk
zcela	zcela	k6eAd1	zcela
shodná	shodný	k2eAgFnSc1d1	shodná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
valencí	valence	k1gFnSc7	valence
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
pojmy	pojem	k1gInPc1	pojem
přímý	přímý	k2eAgInSc1d1	přímý
pád	pád	k1gInSc4	pád
a	a	k8xC	a
předložkový	předložkový	k2eAgInSc4d1	předložkový
(	(	kIx(	(
<g/>
nepřímý	přímý	k2eNgInSc4d1	nepřímý
<g/>
)	)	kIx)	)
pád	pád	k1gInSc4	pád
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
výrazy	výraz	k1gInPc1	výraz
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
nadřazené	nadřazený	k2eAgNnSc1d1	nadřazené
sloveso	sloveso	k1gNnSc1	sloveso
(	(	kIx(	(
<g/>
popř.	popř.	kA	popř.
přídavné	přídavný	k2eAgNnSc1d1	přídavné
jméno	jméno	k1gNnSc1	jméno
<g/>
,	,	kIx,	,
podstatné	podstatný	k2eAgNnSc1d1	podstatné
jméno	jméno	k1gNnSc1	jméno
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
příslovce	příslovce	k1gNnSc2	příslovce
<g/>
)	)	kIx)	)
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
použití	použití	k1gNnSc1	použití
daného	daný	k2eAgInSc2d1	daný
pádu	pád	k1gInSc2	pád
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
určitou	určitý	k2eAgFnSc7d1	určitá
předložkou	předložka	k1gFnSc7	předložka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
bez	bez	k7c2	bez
předložky	předložka	k1gFnSc2	předložka
<g/>
,	,	kIx,	,
přímo	přímo	k6eAd1	přímo
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
tedy	tedy	k9	tedy
o	o	k7c4	o
konkrétní	konkrétní	k2eAgNnSc4d1	konkrétní
použití	použití	k1gNnSc4	použití
pádu	pád	k1gInSc2	pád
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
o	o	k7c4	o
dělení	dělení	k1gNnSc4	dělení
pádů	pád	k1gInPc2	pád
do	do	k7c2	do
kategorií	kategorie	k1gFnPc2	kategorie
-	-	kIx~	-
většina	většina	k1gFnSc1	většina
českých	český	k2eAgInPc2d1	český
pádů	pád	k1gInPc2	pád
může	moct	k5eAaImIp3nS	moct
fungovat	fungovat	k5eAaImF	fungovat
jako	jako	k9	jako
přímá	přímý	k2eAgNnPc4d1	přímé
i	i	k8xC	i
předložková	předložkový	k2eAgNnPc4d1	předložkové
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
nominativ	nominativ	k1gInSc1	nominativ
a	a	k8xC	a
vokativ	vokativ	k1gInSc1	vokativ
jsou	být	k5eAaImIp3nP	být
výhradně	výhradně	k6eAd1	výhradně
přímé	přímý	k2eAgFnPc1d1	přímá
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
lokál	lokál	k1gInSc4	lokál
je	být	k5eAaImIp3nS	být
výhradně	výhradně	k6eAd1	výhradně
předložkový	předložkový	k2eAgInSc1d1	předložkový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc4	seznam
českých	český	k2eAgInPc2d1	český
pádů	pád	k1gInPc2	pád
==	==	k?	==
</s>
</p>
<p>
<s>
nominativ	nominativ	k1gInSc1	nominativ
(	(	kIx(	(
<g/>
kdo	kdo	k3yInSc1	kdo
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
genitiv	genitiv	k1gInSc1	genitiv
(	(	kIx(	(
<g/>
koho	kdo	k3yRnSc4	kdo
<g/>
,	,	kIx,	,
čeho	co	k3yInSc2	co
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
dativ	dativ	k1gInSc1	dativ
(	(	kIx(	(
<g/>
komu	kdo	k3yRnSc3	kdo
<g/>
,	,	kIx,	,
čemu	co	k3yInSc3	co
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
akuzativ	akuzativ	k1gInSc1	akuzativ
(	(	kIx(	(
<g/>
koho	kdo	k3yQnSc4	kdo
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vokativ	vokativ	k1gInSc4	vokativ
(	(	kIx(	(
<g/>
oslovujeme	oslovovat	k5eAaImIp1nP	oslovovat
<g/>
,	,	kIx,	,
voláme	volat	k5eAaImIp1nP	volat
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
lokál	lokál	k1gInSc1	lokál
neboli	neboli	k8xC	neboli
lokativ	lokativ	k1gInSc1	lokativ
(	(	kIx(	(
<g/>
o	o	k7c6	o
kom	kdo	k3yRnSc6	kdo
<g/>
,	,	kIx,	,
o	o	k7c6	o
čem	co	k3yQnSc6	co
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
instrumentál	instrumentál	k1gInSc4	instrumentál
(	(	kIx(	(
<g/>
kým	kdo	k3yQnSc7	kdo
<g/>
,	,	kIx,	,
čím	čí	k3xOyRgNnSc7	čí
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
pádů	pád	k1gInPc2	pád
==	==	k?	==
</s>
</p>
<p>
<s>
Historická	historický	k2eAgFnSc1d1	historická
a	a	k8xC	a
komparativní	komparativní	k2eAgFnSc1d1	komparativní
jazykověda	jazykověda	k1gFnSc1	jazykověda
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
jiného	jiný	k2eAgNnSc2d1	jiné
snaží	snažit	k5eAaImIp3nS	snažit
objasnit	objasnit	k5eAaPmF	objasnit
vznik	vznik	k1gInSc4	vznik
pádů	pád	k1gInPc2	pád
v	v	k7c6	v
moderních	moderní	k2eAgInPc6d1	moderní
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
aglutinační	aglutinační	k2eAgInPc1d1	aglutinační
jazyky	jazyk	k1gInPc1	jazyk
a	a	k8xC	a
jazyky	jazyk	k1gInPc1	jazyk
ergativní	ergativní	k2eAgInPc1d1	ergativní
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
často	často	k6eAd1	často
konstrukce	konstrukce	k1gFnPc1	konstrukce
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c2	za
předchůdce	předchůdce	k1gMnSc2	předchůdce
pádů	pád	k1gInPc2	pád
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
proto-cases	protoases	k1gInSc1	proto-cases
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
vzniku	vznik	k1gInSc3	vznik
pádů	pád	k1gInPc2	pád
nutno	nutno	k6eAd1	nutno
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
pády	pád	k1gInPc4	pád
strukturní	strukturní	k2eAgInPc4d1	strukturní
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
fungují	fungovat	k5eAaImIp3nP	fungovat
jako	jako	k8xC	jako
aktant	aktant	k1gInSc1	aktant
slovesa	sloveso	k1gNnSc2	sloveso
<g/>
)	)	kIx)	)
a	a	k8xC	a
pády	pád	k1gInPc1	pád
sémantické	sémantický	k2eAgFnSc2d1	sémantická
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
také	také	k9	také
oblique	obliquat	k5eAaPmIp3nS	obliquat
cases	cases	k1gInSc1	cases
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
prauralština	prauralština	k1gFnSc1	prauralština
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
existence	existence	k1gFnSc1	existence
ovšem	ovšem	k9	ovšem
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
prokázána	prokázat	k5eAaPmNgFnS	prokázat
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
dva	dva	k4xCgInPc1	dva
nebo	nebo	k8xC	nebo
tři	tři	k4xCgInPc1	tři
strukturní	strukturní	k2eAgInPc1d1	strukturní
pády	pád	k1gInPc1	pád
(	(	kIx(	(
<g/>
nominativ	nominativ	k1gInSc1	nominativ
<g/>
,	,	kIx,	,
genitiv	genitiv	k1gInSc1	genitiv
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
jazykovědci	jazykovědec	k1gMnPc1	jazykovědec
uvádí	uvádět	k5eAaImIp3nP	uvádět
ještě	ještě	k9	ještě
akusativ	akusativ	k1gInSc4	akusativ
<g/>
)	)	kIx)	)
a	a	k8xC	a
tři	tři	k4xCgInPc1	tři
pády	pád	k1gInPc1	pád
sémantické	sémantický	k2eAgInPc1d1	sémantický
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
lokativní	lokativní	k2eAgFnSc1d1	lokativní
(	(	kIx(	(
<g/>
lativ	lativo	k1gNnPc2	lativo
<g/>
,	,	kIx,	,
lokativ	lokativ	k1gInSc1	lokativ
a	a	k8xC	a
adesiv	adesit	k5eAaPmDgInS	adesit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
pádů	pád	k1gInPc2	pád
se	se	k3xPyFc4	se
v	v	k7c6	v
moderních	moderní	k2eAgInPc6d1	moderní
ugrofinských	ugrofinský	k2eAgInPc6d1	ugrofinský
a	a	k8xC	a
aleutských	aleutský	k2eAgInPc6d1	aleutský
jazycích	jazyk	k1gInPc6	jazyk
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
jejich	jejich	k3xOp3gFnSc7	jejich
současný	současný	k2eAgInSc1d1	současný
pádový	pádový	k2eAgInSc1d1	pádový
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Polysyntetická	polysyntetický	k2eAgFnSc1d1	polysyntetická
inuitština	inuitština	k1gFnSc1	inuitština
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
tři	tři	k4xCgInPc4	tři
strukturní	strukturní	k2eAgInPc4d1	strukturní
pády	pád	k1gInPc4	pád
(	(	kIx(	(
<g/>
ergativ	ergativ	k1gInSc4	ergativ
<g/>
,	,	kIx,	,
absolutiv	absolutit	k5eAaPmDgInS	absolutit
a	a	k8xC	a
objektiv	objektiv	k1gInSc4	objektiv
<g/>
)	)	kIx)	)
a	a	k8xC	a
pět	pět	k4xCc4	pět
sémantických	sémantický	k2eAgFnPc2d1	sémantická
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
čtyři	čtyři	k4xCgInPc4	čtyři
lokativní	lokativní	k2eAgInPc4d1	lokativní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
pádového	pádový	k2eAgInSc2d1	pádový
systému	systém	k1gInSc2	systém
praindoevropštiny	praindoevropština	k1gFnSc2	praindoevropština
je	být	k5eAaImIp3nS	být
nejasný	jasný	k2eNgInSc1d1	nejasný
<g/>
,	,	kIx,	,
prvotní	prvotní	k2eAgFnSc1d1	prvotní
fáze	fáze	k1gFnSc1	fáze
jistě	jistě	k9	jistě
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
opozice	opozice	k1gFnSc2	opozice
nominativu	nominativ	k1gInSc2	nominativ
a	a	k8xC	a
akuzativu	akuzativ	k1gInSc2	akuzativ
<g/>
,	,	kIx,	,
další	další	k2eAgInPc1d1	další
pády	pád	k1gInPc1	pád
pak	pak	k6eAd1	pak
zřejmě	zřejmě	k6eAd1	zřejmě
vznikaly	vznikat	k5eAaImAgInP	vznikat
začleněním	začlenění	k1gNnSc7	začlenění
adverbiálních	adverbiální	k2eAgFnPc2d1	adverbiální
forem	forma	k1gFnPc2	forma
a	a	k8xC	a
frází	fráze	k1gFnPc2	fráze
do	do	k7c2	do
paradigmatu	paradigma	k1gNnSc2	paradigma
(	(	kIx(	(
<g/>
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
této	tento	k3xDgFnSc2	tento
tendence	tendence	k1gFnSc2	tendence
jsou	být	k5eAaImIp3nP	být
znatelné	znatelný	k2eAgFnPc1d1	znatelná
ještě	ještě	k6eAd1	ještě
např.	např.	kA	např.
v	v	k7c6	v
paradigmatu	paradigma	k1gNnSc6	paradigma
tzv.	tzv.	kA	tzv.
o-kmenů	omen	k1gMnPc2	o-kmen
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ablativní	ablativní	k2eAgInSc1d1	ablativní
sufix	sufix	k1gInSc1	sufix
*	*	kIx~	*
<g/>
-h	-h	k?	-h
<g/>
2	[number]	k4	2
<g/>
ed	ed	k?	ed
(	(	kIx(	(
<g/>
č.	č.	k?	č.
genitivní	genitivní	k2eAgInSc4d1	genitivní
a	a	k8xC	a
sekundárně	sekundárně	k6eAd1	sekundárně
akuzativní	akuzativní	k2eAgFnSc1d1	akuzativní
-a	-a	k?	-a
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
etymologicky	etymologicky	k6eAd1	etymologicky
shodný	shodný	k2eAgInSc1d1	shodný
s	s	k7c7	s
partikulí	partikule	k1gFnSc7	partikule
o	o	k7c6	o
významu	význam	k1gInSc6	význam
"	"	kIx"	"
<g/>
od	od	k7c2	od
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgInPc4d1	podobný
jevy	jev	k1gInPc4	jev
lze	lze	k6eAd1	lze
sledovat	sledovat	k5eAaImF	sledovat
také	také	k6eAd1	také
např.	např.	kA	např.
v	v	k7c6	v
ide	ide	k?	ide
<g/>
.	.	kIx.	.
tocharštině	tocharština	k1gFnSc6	tocharština
či	či	k8xC	či
jazycích	jazyk	k1gInPc6	jazyk
baltských	baltský	k2eAgInPc6d1	baltský
a	a	k8xC	a
sabelůských	sabelůský	k2eAgInPc6d1	sabelůský
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
jazyka	jazyk	k1gInSc2	jazyk
dochází	docházet	k5eAaImIp3nS	docházet
postupně	postupně	k6eAd1	postupně
k	k	k7c3	k
reanalýze	reanalýza	k1gFnSc3	reanalýza
některých	některý	k3yIgInPc2	některý
pádů	pád	k1gInPc2	pád
(	(	kIx(	(
<g/>
rozšiřování	rozšiřování	k1gNnSc1	rozšiřování
či	či	k8xC	či
přenesení	přenesení	k1gNnSc1	přenesení
významu	význam	k1gInSc2	význam
<g/>
)	)	kIx)	)
a	a	k8xC	a
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
ke	k	k7c3	k
gramatikalizaci	gramatikalizace	k1gFnSc3	gramatikalizace
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
ajmarštině	ajmarština	k1gFnSc6	ajmarština
se	se	k3xPyFc4	se
z	z	k7c2	z
illativu	illativ	k1gInSc2	illativ
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
dativ	dativ	k1gInSc1	dativ
<g/>
,	,	kIx,	,
z	z	k7c2	z
lokativu	lokativ	k1gInSc2	lokativ
genitiv	genitiv	k1gInSc1	genitiv
<g/>
,	,	kIx,	,
z	z	k7c2	z
instrumentálu	instrumentál	k1gInSc2	instrumentál
způsob	způsob	k1gInSc4	způsob
vyjádření	vyjádření	k1gNnSc2	vyjádření
konatele	konatel	k1gMnSc2	konatel
v	v	k7c6	v
trpném	trpný	k2eAgInSc6d1	trpný
rodě	rod	k1gInSc6	rod
apod.	apod.	kA	apod.
Obecně	obecně	k6eAd1	obecně
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
objektivní	objektivní	k2eAgFnSc1d1	objektivní
vazba	vazba	k1gFnSc1	vazba
je	být	k5eAaImIp3nS	být
starší	starý	k2eAgMnSc1d2	starší
než	než	k8xS	než
subjektivní	subjektivní	k2eAgMnSc1d1	subjektivní
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
genitivu	genitiv	k1gInSc2	genitiv
se	se	k3xPyFc4	se
často	často	k6eAd1	často
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
ergativ	ergativ	k1gInSc1	ergativ
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zpravidla	zpravidla	k6eAd1	zpravidla
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
posesivního	posesivní	k2eAgNnSc2d1	posesivní
vyjádření	vyjádření	k1gNnSc2	vyjádření
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
indiánských	indiánský	k2eAgInPc6d1	indiánský
jazycích	jazyk	k1gInPc6	jazyk
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
konatele	konatel	k1gMnSc2	konatel
u	u	k7c2	u
příčestí	příčestí	k1gNnSc2	příčestí
i	i	k9	i
v	v	k7c6	v
nepříznakových	příznakový	k2eNgFnPc6d1	nepříznaková
konstrukcích	konstrukce	k1gFnPc6	konstrukce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Skloňování	skloňování	k1gNnSc1	skloňování
</s>
</p>
<p>
<s>
České	český	k2eAgNnSc1d1	české
skloňování	skloňování	k1gNnSc1	skloňování
</s>
</p>
<p>
<s>
Podstatné	podstatný	k2eAgNnSc1d1	podstatné
jméno	jméno	k1gNnSc1	jméno
</s>
</p>
<p>
<s>
Česká	český	k2eAgNnPc1d1	české
podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
pád	pád	k1gInSc1	pád
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Pád	Pád	k1gInSc1	Pád
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
