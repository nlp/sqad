<p>
<s>
Láska	láska	k1gFnSc1	láska
za	za	k7c2	za
časů	čas	k1gInPc2	čas
cholery	cholera	k1gFnSc2	cholera
(	(	kIx(	(
<g/>
španělský	španělský	k2eAgInSc4d1	španělský
originál	originál	k1gInSc4	originál
"	"	kIx"	"
<g/>
El	Ela	k1gFnPc2	Ela
amor	amor	k1gMnSc1	amor
en	en	k?	en
los	los	k1gInSc1	los
tiempos	tiempos	k1gInSc1	tiempos
del	del	k?	del
cólera	cólera	k1gFnSc1	cólera
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgNnPc3d3	nejvýznamnější
dílům	dílo	k1gNnPc3	dílo
Gabriela	Gabriel	k1gMnSc2	Gabriel
Garcíi	Garcíi	k1gNnSc7	Garcíi
Márqueze	Márqueze	k1gFnSc2	Márqueze
<g/>
.	.	kIx.	.
</s>
<s>
Knihu	kniha	k1gFnSc4	kniha
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Blanka	Blanka	k1gFnSc1	Blanka
Stárková	Stárková	k1gFnSc1	Stárková
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
publikována	publikovat	k5eAaBmNgFnS	publikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
a	a	k8xC	a
nedávno	nedávno	k6eAd1	nedávno
se	se	k3xPyFc4	se
dočkala	dočkat	k5eAaPmAgFnS	dočkat
i	i	k8xC	i
svého	svůj	k3xOyFgNnSc2	svůj
filmového	filmový	k2eAgNnSc2d1	filmové
zpracování	zpracování	k1gNnSc2	zpracování
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
50	[number]	k4	50
let	léto	k1gNnPc2	léto
trvajícím	trvající	k2eAgMnSc7d1	trvající
milostným	milostný	k2eAgInSc7d1	milostný
trojúhelníkem	trojúhelník	k1gInSc7	trojúhelník
mezi	mezi	k7c7	mezi
postavami	postava	k1gFnPc7	postava
Fermina	Fermin	k2eAgInSc2d1	Fermin
Daza	Daz	k1gInSc2	Daz
<g/>
,	,	kIx,	,
Florentino	Florentin	k2eAgNnSc1d1	Florentino
Ariza	Ariza	k1gFnSc1	Ariza
a	a	k8xC	a
Doctor	Doctor	k1gInSc1	Doctor
Juvenal	Juvenal	k1gFnPc2	Juvenal
Urbino	Urbino	k1gNnSc4	Urbino
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
v	v	k7c6	v
prvních	první	k4xOgFnPc6	první
dekádách	dekáda	k1gFnPc6	dekáda
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
1880	[number]	k4	1880
<g/>
-	-	kIx~	-
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
myšlenkou	myšlenka	k1gFnSc7	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
</s>
</p>
<p>
<s>
utrpení	utrpení	k1gNnSc4	utrpení
z	z	k7c2	z
neopětované	opětovaný	k2eNgFnSc2d1	neopětovaná
lásky	láska	k1gFnSc2	láska
je	být	k5eAaImIp3nS	být
druhem	druh	k1gInSc7	druh
vznešenosti	vznešenost	k1gFnSc2	vznešenost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
postavy	postava	k1gFnPc1	postava
==	==	k?	==
</s>
</p>
<p>
<s>
Florentino	Florentin	k2eAgNnSc1d1	Florentino
Ariza	Ariz	k1gMnSc4	Ariz
-	-	kIx~	-
přecitlivělý	přecitlivělý	k2eAgInSc4d1	přecitlivělý
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
psychicky	psychicky	k6eAd1	psychicky
nevyrovnaný	vyrovnaný	k2eNgInSc1d1	nevyrovnaný
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
život	život	k1gInSc1	život
ho	on	k3xPp3gMnSc4	on
pronásleduje	pronásledovat	k5eAaImIp3nS	pronásledovat
a	a	k8xC	a
ničí	ničit	k5eAaImIp3nS	ničit
nešťastná	šťastný	k2eNgFnSc1d1	nešťastná
láska	láska	k1gFnSc1	láska
k	k	k7c3	k
Fermině	Fermin	k2eAgFnSc3d1	Fermina
Dazové	Dazová	k1gFnSc3	Dazová
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
utrpení	utrpení	k1gNnSc6	utrpení
se	se	k3xPyFc4	se
mnohdy	mnohdy	k6eAd1	mnohdy
vyžívá	vyžívat	k5eAaImIp3nS	vyžívat
<g/>
,	,	kIx,	,
a	a	k8xC	a
přeje	přát	k5eAaImIp3nS	přát
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Fermina	Fermin	k2eAgFnSc1d1	Fermina
poznala	poznat	k5eAaPmAgFnS	poznat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
moc	moc	k6eAd1	moc
kvůli	kvůli	k7c3	kvůli
ní	on	k3xPp3gFnSc3	on
trpí	trpět	k5eAaImIp3nS	trpět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fermina	Fermin	k2eAgFnSc1d1	Fermina
Dazová	Dazová	k1gFnSc1	Dazová
-	-	kIx~	-
svéhlavá	svéhlavý	k2eAgFnSc1d1	svéhlavá
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
sklony	sklon	k1gInPc4	sklon
k	k	k7c3	k
impulzivnímu	impulzivní	k2eAgNnSc3d1	impulzivní
jednání	jednání	k1gNnSc3	jednání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důležitých	důležitý	k2eAgFnPc6d1	důležitá
záležitostech	záležitost	k1gFnPc6	záležitost
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
hlavně	hlavně	k9	hlavně
rozumem	rozum	k1gInSc7	rozum
a	a	k8xC	a
nebere	brát	k5eNaImIp3nS	brát
příliš	příliš	k6eAd1	příliš
ohledů	ohled	k1gInPc2	ohled
na	na	k7c4	na
ostatní	ostatní	k2eAgMnPc4d1	ostatní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Doktor	doktor	k1gMnSc1	doktor
Juvenal	Juvenal	k1gFnSc2	Juvenal
Urbino	Urbino	k1gNnSc1	Urbino
-	-	kIx~	-
manžel	manžel	k1gMnSc1	manžel
Ferminy	Fermin	k2eAgFnSc2d1	Fermina
Dazové	Dazová	k1gFnSc2	Dazová
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vzdělaný	vzdělaný	k2eAgMnSc1d1	vzdělaný
<g/>
,	,	kIx,	,
racionální	racionální	k2eAgInPc1d1	racionální
a	a	k8xC	a
všude	všude	k6eAd1	všude
vážený	vážený	k2eAgMnSc1d1	vážený
<g/>
.	.	kIx.	.
<g/>
Staví	stavit	k5eAaImIp3nS	stavit
se	se	k3xPyFc4	se
velice	velice	k6eAd1	velice
vstřícně	vstřícně	k6eAd1	vstřícně
k	k	k7c3	k
pokroku	pokrok	k1gInSc3	pokrok
a	a	k8xC	a
vývoji	vývoj	k1gInSc3	vývoj
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rád	rád	k2eAgMnSc1d1	rád
<g/>
,	,	kIx,	,
když	když	k8xS	když
mají	mít	k5eAaImIp3nP	mít
všechny	všechen	k3xTgFnPc1	všechen
věci	věc	k1gFnPc1	věc
svůj	svůj	k3xOyFgInSc4	svůj
řád	řád	k1gInSc4	řád
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lev	Lev	k1gMnSc1	Lev
XII	XII	kA	XII
<g/>
.	.	kIx.	.
-	-	kIx~	-
strýc	strýc	k1gMnSc1	strýc
Florentina	Florentin	k1gMnSc2	Florentin
Arizy	Ariza	k1gFnSc2	Ariza
a	a	k8xC	a
ředitel	ředitel	k1gMnSc1	ředitel
Karibské	karibský	k2eAgFnSc2d1	karibská
říční	říční	k2eAgFnSc2d1	říční
paroplavby	paroplavba	k1gFnSc2	paroplavba
<g/>
.	.	kIx.	.
</s>
<s>
Pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
Florentinovi	Florentin	k1gMnSc3	Florentin
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nP	snažit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
aspoň	aspoň	k9	aspoň
trochu	trochu	k6eAd1	trochu
vynahradit	vynahradit	k5eAaPmF	vynahradit
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
mu	on	k3xPp3gMnSc3	on
jeho	jeho	k3xOp3gFnSc6	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
bratr	bratr	k1gMnSc1	bratr
Lva	lev	k1gInSc2	lev
XII	XII	kA	XII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
nedal	dát	k5eNaPmAgMnS	dát
<g/>
.	.	kIx.	.
</s>
<s>
Zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
Florentina	Florentina	k1gFnSc1	Florentina
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
odchází	odcházet	k5eAaImIp3nS	odcházet
na	na	k7c4	na
odpočinek	odpočinek	k1gInSc4	odpočinek
<g/>
,	,	kIx,	,
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
ho	on	k3xPp3gInSc4	on
svým	svůj	k3xOyFgMnSc7	svůj
nástupcem	nástupce	k1gMnSc7	nástupce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Leona	Leona	k1gFnSc1	Leona
Cassiani	Cassian	k1gMnPc5	Cassian
-	-	kIx~	-
zástupkyně	zástupkyně	k1gFnSc1	zástupkyně
ředitele	ředitel	k1gMnSc2	ředitel
v	v	k7c6	v
Karibské	karibský	k2eAgFnSc6d1	karibská
říční	říční	k2eAgFnSc6d1	říční
paroplavbě	paroplavba	k1gFnSc6	paroplavba
<g/>
.	.	kIx.	.
</s>
<s>
Miluje	milovat	k5eAaImIp3nS	milovat
Florentina	Florentina	k1gFnSc1	Florentina
Arizu	Ariz	k1gInSc2	Ariz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
on	on	k3xPp3gMnSc1	on
nikdy	nikdy	k6eAd1	nikdy
nezradí	zradit	k5eNaPmIp3nS	zradit
svou	svůj	k3xOyFgFnSc4	svůj
lásku	láska	k1gFnSc4	láska
k	k	k7c3	k
Fermině	Fermin	k2eAgFnSc3d1	Fermina
Dazové	Dazová	k1gFnSc3	Dazová
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
mu	on	k3xPp3gMnSc3	on
je	být	k5eAaImIp3nS	být
aspoň	aspoň	k9	aspoň
dobrou	dobrý	k2eAgFnSc7d1	dobrá
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
a	a	k8xC	a
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
mu	on	k3xPp3gMnSc3	on
snášet	snášet	k5eAaImF	snášet
jeho	jeho	k3xOp3gInSc4	jeho
trpký	trpký	k2eAgInSc4d1	trpký
životní	životní	k2eAgInSc4d1	životní
úděl	úděl	k1gInSc4	úděl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lorenzo	Lorenza	k1gFnSc5	Lorenza
Daza	Daza	k1gMnSc1	Daza
-	-	kIx~	-
otec	otec	k1gMnSc1	otec
Ferminy	Fermin	k2eAgFnSc2d1	Fermina
Dazové	Dazová	k1gFnSc2	Dazová
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
výbušný	výbušný	k2eAgInSc1d1	výbušný
a	a	k8xC	a
přísný	přísný	k2eAgInSc1d1	přísný
<g/>
.	.	kIx.	.
</s>
<s>
Svými	svůj	k3xOyFgInPc7	svůj
obchody	obchod	k1gInPc7	obchod
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
nelegálními	legální	k2eNgInPc7d1	nelegální
a	a	k8xC	a
pochybnými	pochybný	k2eAgInPc7d1	pochybný
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
získal	získat	k5eAaPmAgMnS	získat
bohatství	bohatství	k1gNnSc4	bohatství
a	a	k8xC	a
určitý	určitý	k2eAgInSc4d1	určitý
společenský	společenský	k2eAgInSc4d1	společenský
respekt	respekt	k1gInSc4	respekt
(	(	kIx(	(
<g/>
ovšem	ovšem	k9	ovšem
jen	jen	k9	jen
zdánlivý	zdánlivý	k2eAgInSc1d1	zdánlivý
<g/>
,	,	kIx,	,
vysoká	vysoký	k2eAgFnSc1d1	vysoká
společnost	společnost	k1gFnSc1	společnost
jím	jíst	k5eAaImIp1nS	jíst
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
pohrdá	pohrdat	k5eAaImIp3nS	pohrdat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
o	o	k7c4	o
který	který	k3yQgInSc4	který
se	se	k3xPyFc4	se
nechce	chtít	k5eNaImIp3nS	chtít
nechat	nechat	k5eAaPmF	nechat
za	za	k7c4	za
žádnou	žádný	k3yNgFnSc4	žádný
cenu	cena	k1gFnSc4	cena
připravit	připravit	k5eAaPmF	připravit
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
dceřina	dceřin	k2eAgNnSc2d1	dceřino
štěstí	štěstí	k1gNnSc2	štěstí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tránsito	Tránsit	k2eAgNnSc1d1	Tránsito
Arizová	Arizový	k2eAgFnSc1d1	Arizový
-	-	kIx~	-
matka	matka	k1gFnSc1	matka
Florentina	Florentin	k1gMnSc2	Florentin
Arizy	Ariza	k1gFnSc2	Ariza
<g/>
.	.	kIx.	.
</s>
<s>
Svému	svůj	k1gMnSc3	svůj
synovi	syn	k1gMnSc3	syn
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
oporou	opora	k1gFnSc7	opora
<g/>
,	,	kIx,	,
ve	v	k7c6	v
všem	všecek	k3xTgNnSc6	všecek
mu	on	k3xPp3gMnSc3	on
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
a	a	k8xC	a
utěšuje	utěšovat	k5eAaImIp3nS	utěšovat
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
když	když	k8xS	když
propadá	propadat	k5eAaPmIp3nS	propadat
depresím	deprese	k1gFnPc3	deprese
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Příběh	příběh	k1gInSc1	příběh
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
Kolumbii	Kolumbie	k1gFnSc6	Kolumbie
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Cartagena	Cartageno	k1gNnSc2	Cartageno
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
začíná	začínat	k5eAaImIp3nS	začínat
smrtí	smrt	k1gFnSc7	smrt
fotografa	fotograf	k1gMnSc2	fotograf
a	a	k8xC	a
bývalého	bývalý	k2eAgMnSc2d1	bývalý
vojáka	voják	k1gMnSc2	voják
Jeremiaha	Jeremiah	k1gMnSc2	Jeremiah
de	de	k?	de
Saint-Amour	Saint-Amour	k1gMnSc1	Saint-Amour
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ohledání	ohledání	k1gNnSc3	ohledání
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
pozván	pozván	k2eAgMnSc1d1	pozván
jeho	jeho	k3xOp3gMnSc1	jeho
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
přítel	přítel	k1gMnSc1	přítel
<g/>
,	,	kIx,	,
starý	starý	k2eAgMnSc1d1	starý
a	a	k8xC	a
věhlasný	věhlasný	k2eAgMnSc1d1	věhlasný
doktor	doktor	k1gMnSc1	doktor
Juvenal	Juvenal	k1gMnSc2	Juvenal
Urbino	Urbino	k1gNnSc4	Urbino
<g/>
.	.	kIx.	.
</s>
<s>
Zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
Jeremiaha	Jeremiaha	k1gFnSc1	Jeremiaha
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
otřese	otřást	k5eAaPmIp3nS	otřást
víc	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
připustit	připustit	k5eAaPmF	připustit
<g/>
.	.	kIx.	.
</s>
<s>
Začne	začít	k5eAaPmIp3nS	začít
přemýšlet	přemýšlet	k5eAaImF	přemýšlet
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
dosavadním	dosavadní	k2eAgInSc6d1	dosavadní
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
o	o	k7c6	o
minulosti	minulost	k1gFnSc6	minulost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
o	o	k7c6	o
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
by	by	kYmCp3nS	by
tušil	tušit	k5eAaImAgMnS	tušit
její	její	k3xOp3gFnSc4	její
blízkost	blízkost	k1gFnSc4	blízkost
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
osmdesátiletý	osmdesátiletý	k2eAgMnSc1d1	osmdesátiletý
doktor	doktor	k1gMnSc1	doktor
Urbino	Urbino	k1gNnSc4	Urbino
vydá	vydat	k5eAaPmIp3nS	vydat
chytat	chytat	k5eAaImF	chytat
papouška	papoušek	k1gMnSc4	papoušek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jim	on	k3xPp3gMnPc3	on
ulétl	ulétnout	k5eAaPmAgInS	ulétnout
na	na	k7c4	na
vysoký	vysoký	k2eAgInSc4d1	vysoký
strom	strom	k1gInSc4	strom
<g/>
.	.	kIx.	.
</s>
<s>
Neodhadne	odhadnout	k5eNaPmIp3nS	odhadnout
své	svůj	k3xOyFgFnPc4	svůj
schopnosti	schopnost	k1gFnPc4	schopnost
a	a	k8xC	a
zřítí	zřítit	k5eAaPmIp3nS	zřítit
se	se	k3xPyFc4	se
ze	z	k7c2	z
žebříku	žebřík	k1gInSc2	žebřík
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
se	se	k3xPyFc4	se
nedá	dát	k5eNaPmIp3nS	dát
nic	nic	k3yNnSc1	nic
dělat	dělat	k5eAaImF	dělat
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
konstatovat	konstatovat	k5eAaBmF	konstatovat
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
ženy	žena	k1gFnSc2	žena
Ferminy	Fermin	k2eAgFnSc2d1	Fermina
je	být	k5eAaImIp3nS	být
rázem	rázem	k6eAd1	rázem
vdova	vdova	k1gFnSc1	vdova
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
Urbinova	Urbinův	k2eAgInSc2d1	Urbinův
pohřbu	pohřeb	k1gInSc2	pohřeb
se	se	k3xPyFc4	se
na	na	k7c6	na
scéně	scéna	k1gFnSc6	scéna
objevuje	objevovat	k5eAaImIp3nS	objevovat
Florentino	Florentin	k2eAgNnSc1d1	Florentino
Ariza	Ariz	k1gMnSc4	Ariz
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
má	mít	k5eAaImIp3nS	mít
s	s	k7c7	s
Ferminou	Fermin	k2eAgFnSc7d1	Fermina
společného	společný	k2eAgMnSc4d1	společný
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
připustit	připustit	k5eAaPmF	připustit
<g/>
.	.	kIx.	.
</s>
<s>
Kdysi	kdysi	k6eAd1	kdysi
dávno	dávno	k6eAd1	dávno
<g/>
,	,	kIx,	,
skoro	skoro	k6eAd1	skoro
před	před	k7c7	před
šedesáti	šedesát	k4xCc7	šedesát
lety	let	k1gInPc7	let
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
odvíjet	odvíjet	k5eAaImF	odvíjet
jejich	jejich	k3xOp3gInSc1	jejich
příběh	příběh	k1gInSc1	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
poštovní	poštovní	k2eAgMnSc1d1	poštovní
poslíček	poslíček	k1gMnSc1	poslíček
Florentino	Florentin	k2eAgNnSc4d1	Florentino
Ariza	Ariza	k1gFnSc1	Ariza
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
spatří	spatřit	k5eAaPmIp3nP	spatřit
krásnou	krásný	k2eAgFnSc4d1	krásná
Ferminu	Fermin	k2eAgFnSc4d1	Fermina
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
v	v	k7c6	v
parku	park	k1gInSc6	park
pod	pod	k7c7	pod
mandloněmi	mandloň	k1gFnPc7	mandloň
učí	učit	k5eAaImIp3nS	učit
číst	číst	k5eAaImF	číst
svou	svůj	k3xOyFgFnSc4	svůj
tetu	teta	k1gFnSc4	teta
Escolástiku	Escolástika	k1gFnSc4	Escolástika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
jí	on	k3xPp3gFnSc3	on
psát	psát	k5eAaImF	psát
spousty	spousta	k1gFnPc4	spousta
dopisů	dopis	k1gInPc2	dopis
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
jí	jíst	k5eAaImIp3nS	jíst
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
nekonečnou	konečný	k2eNgFnSc4d1	nekonečná
lásku	láska	k1gFnSc4	láska
a	a	k8xC	a
prosí	prosit	k5eAaImIp3nS	prosit
ji	on	k3xPp3gFnSc4	on
o	o	k7c4	o
odpověď	odpověď	k1gFnSc4	odpověď
<g/>
.	.	kIx.	.
</s>
<s>
Fermina	Fermin	k2eAgFnSc1d1	Fermina
ovšem	ovšem	k9	ovšem
jeho	jeho	k3xOp3gInSc1	jeho
cit	cit	k1gInSc1	cit
neopětuje	opětovat	k5eNaImIp3nS	opětovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
přál	přát	k5eAaImAgMnS	přát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
zoufalé	zoufalý	k2eAgInPc4d1	zoufalý
dopisy	dopis	k1gInPc4	dopis
mu	on	k3xPp3gMnSc3	on
odpoví	odpovědět	k5eAaPmIp3nS	odpovědět
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
době	doba	k1gFnSc6	doba
jen	jen	k9	jen
jednou	jeden	k4xCgFnSc7	jeden
<g/>
,	,	kIx,	,
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
hodně	hodně	k6eAd1	hodně
vlažně	vlažně	k6eAd1	vlažně
a	a	k8xC	a
stručně	stručně	k6eAd1	stručně
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
i	i	k9	i
za	za	k7c4	za
tohle	tenhle	k3xDgNnSc4	tenhle
je	být	k5eAaImIp3nS	být
Florentino	Florentin	k2eAgNnSc1d1	Florentino
neskutečně	skutečně	k6eNd1	skutečně
šťastný	šťastný	k2eAgMnSc1d1	šťastný
<g/>
.	.	kIx.	.
</s>
<s>
Snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
pořád	pořád	k6eAd1	pořád
získat	získat	k5eAaPmF	získat
její	její	k3xOp3gFnSc4	její
náklonnost	náklonnost	k1gFnSc4	náklonnost
<g/>
,	,	kIx,	,
složí	složit	k5eAaPmIp3nS	složit
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
valčík	valčík	k1gInSc1	valčík
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
největším	veliký	k2eAgNnSc6d3	veliký
zoufalství	zoufalství	k1gNnSc6	zoufalství
chodí	chodit	k5eAaImIp3nP	chodit
hrát	hrát	k5eAaImF	hrát
pod	pod	k7c4	pod
okno	okno	k1gNnSc4	okno
<g/>
.	.	kIx.	.
</s>
<s>
Neustává	ustávat	k5eNaImIp3nS	ustávat
v	v	k7c6	v
psaní	psaní	k1gNnSc6	psaní
dopisů	dopis	k1gInPc2	dopis
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yRgFnSc6	který
mu	on	k3xPp3gMnSc3	on
Fermina	Fermin	k2eAgNnSc2d1	Fermin
pomalu	pomalu	k6eAd1	pomalu
začíná	začínat	k5eAaImIp3nS	začínat
odpovídat	odpovídat	k5eAaImF	odpovídat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
korespondenci	korespondence	k1gFnSc6	korespondence
dojdou	dojít	k5eAaPmIp3nP	dojít
tak	tak	k6eAd1	tak
daleko	daleko	k6eAd1	daleko
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
začnou	začít	k5eAaPmIp3nP	začít
domlouvat	domlouvat	k5eAaImF	domlouvat
i	i	k9	i
na	na	k7c6	na
svatbě	svatba	k1gFnSc6	svatba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
ale	ale	k9	ale
zasáhnou	zasáhnout	k5eAaPmIp3nP	zasáhnout
okolnosti	okolnost	k1gFnPc1	okolnost
<g/>
.	.	kIx.	.
</s>
<s>
Ferminin	Ferminin	k2eAgMnSc1d1	Ferminin
otec	otec	k1gMnSc1	otec
Lorenzo	Lorenza	k1gFnSc5	Lorenza
Daza	Dazus	k1gMnSc4	Dazus
na	na	k7c4	na
všechno	všechen	k3xTgNnSc4	všechen
přijde	přijít	k5eAaPmIp3nS	přijít
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
se	se	k3xPyFc4	se
rozčílí	rozčílit	k5eAaPmIp3nS	rozčílit
<g/>
.	.	kIx.	.
</s>
<s>
Florentina	Florentina	k1gFnSc1	Florentina
rozhodně	rozhodně	k6eAd1	rozhodně
za	za	k7c4	za
zetě	zeť	k1gMnPc4	zeť
nechce	chtít	k5eNaImIp3nS	chtít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nemá	mít	k5eNaImIp3nS	mít
žádné	žádný	k3yNgInPc4	žádný
peníze	peníz	k1gInPc4	peníz
ani	ani	k8xC	ani
perspektivní	perspektivní	k2eAgFnSc4d1	perspektivní
budoucnost	budoucnost	k1gFnSc4	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Fermině	Fermin	k2eAgFnSc6d1	Fermina
striktně	striktně	k6eAd1	striktně
oznámí	oznámit	k5eAaPmIp3nS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
Florentina	Florentin	k1gMnSc4	Florentin
musí	muset	k5eAaImIp3nS	muset
zapomenout	zapomenout	k5eAaPmF	zapomenout
<g/>
.	.	kIx.	.
</s>
<s>
Jenže	jenže	k8xC	jenže
Fermina	Fermin	k2eAgFnSc1d1	Fermina
je	být	k5eAaImIp3nS	být
tvrdohlavá	tvrdohlavý	k2eAgFnSc1d1	tvrdohlavá
a	a	k8xC	a
otci	otec	k1gMnSc3	otec
neustoupí	ustoupit	k5eNaPmIp3nS	ustoupit
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
mu	on	k3xPp3gMnSc3	on
vyhrožuje	vyhrožovat	k5eAaImIp3nS	vyhrožovat
sebevraždou	sebevražda	k1gFnSc7	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
to	ten	k3xDgNnSc1	ten
Lorenzo	Lorenza	k1gFnSc5	Lorenza
Daza	Daz	k2eAgMnSc4d1	Daz
zkusí	zkusit	k5eAaPmIp3nS	zkusit
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Sejde	sejít	k5eAaPmIp3nS	sejít
se	se	k3xPyFc4	se
s	s	k7c7	s
Florentinem	florentin	k1gInSc7	florentin
a	a	k8xC	a
přesvědčuje	přesvědčovat	k5eAaImIp3nS	přesvědčovat
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
Ferminy	Fermin	k1gInPc4	Fermin
vzdal	vzdát	k5eAaPmAgMnS	vzdát
pro	pro	k7c4	pro
její	její	k3xOp3gNnSc4	její
vlastní	vlastní	k2eAgNnSc4d1	vlastní
dobro	dobro	k1gNnSc4	dobro
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
mu	on	k3xPp3gNnSc3	on
to	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
nic	nic	k6eAd1	nic
platné	platný	k2eAgNnSc1d1	platné
<g/>
,	,	kIx,	,
Florentino	Florentin	k2eAgNnSc1d1	Florentino
totiž	totiž	k9	totiž
vidí	vidět	k5eAaImIp3nS	vidět
ve	v	k7c6	v
Fermině	Fermin	k2eAgFnSc6d1	Fermina
jediný	jediný	k2eAgInSc4d1	jediný
smysl	smysl	k1gInSc4	smysl
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
a	a	k8xC	a
za	za	k7c4	za
žádnou	žádný	k3yNgFnSc4	žádný
cenu	cena	k1gFnSc4	cena
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
nevzdá	vzdát	k5eNaPmIp3nS	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Lorenzo	Lorenza	k1gFnSc5	Lorenza
Daza	Dazus	k1gMnSc4	Dazus
to	ten	k3xDgNnSc1	ten
zkouší	zkoušet	k5eAaImIp3nS	zkoušet
po	po	k7c6	po
dobrém	dobrý	k2eAgNnSc6d1	dobré
i	i	k8xC	i
po	po	k7c6	po
zlém	zlý	k2eAgNnSc6d1	zlé
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nic	nic	k3yNnSc1	nic
nevzmůže	vzmoct	k5eNaPmIp3nS	vzmoct
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
trpělivost	trpělivost	k1gFnSc1	trpělivost
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
konce	konec	k1gInSc2	konec
<g/>
.	.	kIx.	.
</s>
<s>
Sebere	sebrat	k5eAaPmIp3nS	sebrat
Ferminu	Fermin	k1gInSc2	Fermin
a	a	k8xC	a
odjede	odjet	k5eAaPmIp3nS	odjet
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
na	na	k7c4	na
vzdálený	vzdálený	k2eAgInSc4d1	vzdálený
ranč	ranč	k1gInSc4	ranč
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žijí	žít	k5eAaImIp3nP	žít
sourozenci	sourozenec	k1gMnPc1	sourozenec
její	její	k3xOp3gFnSc2	její
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
Fermina	Fermin	k2eAgFnSc1d1	Fermina
je	být	k5eAaImIp3nS	být
zprvu	zprvu	k6eAd1	zprvu
nešťastná	šťastný	k2eNgFnSc1d1	nešťastná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
mnohých	mnohý	k2eAgFnPc2d1	mnohá
sestřenic	sestřenice	k1gFnPc2	sestřenice
<g/>
,	,	kIx,	,
Hildebrandě	Hildebranda	k1gFnSc6	Hildebranda
Sanchézové	Sanchézová	k1gFnSc2	Sanchézová
<g/>
,	,	kIx,	,
najde	najít	k5eAaPmIp3nS	najít
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
přítelkyni	přítelkyně	k1gFnSc4	přítelkyně
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jí	on	k3xPp3gFnSc3	on
umí	umět	k5eAaImIp3nS	umět
se	s	k7c7	s
vším	všecek	k3xTgNnSc7	všecek
pomoci	pomoct	k5eAaPmF	pomoct
<g/>
.	.	kIx.	.
</s>
<s>
Fermina	Fermin	k2eAgFnSc1d1	Fermina
pomalu	pomalu	k6eAd1	pomalu
zapomíná	zapomínat	k5eAaImIp3nS	zapomínat
na	na	k7c4	na
Florentina	Florentin	k1gMnSc4	Florentin
<g/>
,	,	kIx,	,
když	když	k8xS	když
jí	on	k3xPp3gFnSc2	on
sestřenice	sestřenice	k1gFnSc2	sestřenice
tajně	tajně	k6eAd1	tajně
přinesou	přinést	k5eAaPmIp3nP	přinést
telegrafní	telegrafní	k2eAgFnPc4d1	telegrafní
zprávy	zpráva	k1gFnPc4	zpráva
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
jí	jíst	k5eAaImIp3nS	jíst
zoufalý	zoufalý	k2eAgMnSc1d1	zoufalý
Florentino	Florentin	k2eAgNnSc4d1	Florentino
posílal	posílat	k5eAaImAgMnS	posílat
<g/>
.	.	kIx.	.
</s>
<s>
Začnou	začít	k5eAaPmIp3nP	začít
si	se	k3xPyFc3	se
opět	opět	k6eAd1	opět
dopisovat	dopisovat	k5eAaImF	dopisovat
a	a	k8xC	a
po	po	k7c6	po
pár	pár	k4xCyI	pár
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
Fermina	Fermin	k2eAgFnSc1d1	Fermina
strávila	strávit	k5eAaPmAgFnS	strávit
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
domova	domov	k1gInSc2	domov
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
čas	čas	k1gInSc1	čas
návratu	návrat	k1gInSc6	návrat
<g/>
.	.	kIx.	.
</s>
<s>
Fermině	Fermin	k2eAgFnSc3d1	Fermina
je	být	k5eAaImIp3nS	být
čerstvých	čerstvý	k2eAgFnPc2d1	čerstvá
osmnáct	osmnáct	k4xCc4	osmnáct
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
vrátí	vrátit	k5eAaPmIp3nP	vrátit
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Florentino	Florentin	k2eAgNnSc1d1	Florentino
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
<g/>
,	,	kIx,	,
a	a	k8xC	a
okamžitě	okamžitě	k6eAd1	okamžitě
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
vydá	vydat	k5eAaPmIp3nS	vydat
hledat	hledat	k5eAaImF	hledat
do	do	k7c2	do
ulic	ulice	k1gFnPc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Fermina	Fermin	k2eAgFnSc1d1	Fermina
je	být	k5eAaImIp3nS	být
zrovna	zrovna	k6eAd1	zrovna
na	na	k7c6	na
nákupech	nákup	k1gInPc6	nákup
<g/>
,	,	kIx,	,
když	když	k8xS	když
za	za	k7c7	za
sebou	se	k3xPyFc7	se
uslyší	uslyšet	k5eAaPmIp3nP	uslyšet
známý	známý	k2eAgInSc4d1	známý
hlas	hlas	k1gInSc4	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Otočí	otočit	k5eAaPmIp3nS	otočit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
zklamaná	zklamaný	k2eAgFnSc1d1	zklamaná
<g/>
.	.	kIx.	.
</s>
<s>
Uvědomí	uvědomit	k5eAaPmIp3nS	uvědomit
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
ta	ten	k3xDgNnPc4	ten
léta	léto	k1gNnPc4	léto
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
se	se	k3xPyFc4	se
neviděli	vidět	k5eNaImAgMnP	vidět
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
Florentina	Florentina	k1gFnSc1	Florentina
příliš	příliš	k6eAd1	příliš
zidealizovala	zidealizovat	k5eAaPmAgFnS	zidealizovat
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
vlastně	vlastně	k9	vlastně
zamilovala	zamilovat	k5eAaPmAgFnS	zamilovat
jen	jen	k9	jen
do	do	k7c2	do
iluze	iluze	k1gFnSc2	iluze
<g/>
,	,	kIx,	,
ne	ne	k9	ne
do	do	k7c2	do
skutečného	skutečný	k2eAgMnSc2d1	skutečný
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Nechce	chtít	k5eNaImIp3nS	chtít
to	ten	k3xDgNnSc1	ten
příliš	příliš	k6eAd1	příliš
prodlužovat	prodlužovat	k5eAaImF	prodlužovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
svůj	svůj	k3xOyFgInSc4	svůj
dojem	dojem	k1gInSc4	dojem
Florentinovi	Florentin	k1gMnSc3	Florentin
ihned	ihned	k6eAd1	ihned
sdělí	sdělit	k5eAaPmIp3nS	sdělit
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
nejdříve	dříve	k6eAd3	dříve
zmatený	zmatený	k2eAgMnSc1d1	zmatený
<g/>
,	,	kIx,	,
a	a	k8xC	a
potom	potom	k6eAd1	potom
nešťastný	šťastný	k2eNgMnSc1d1	nešťastný
<g/>
.	.	kIx.	.
</s>
<s>
Zhroutí	zhroutit	k5eAaPmIp3nS	zhroutit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
a	a	k8xC	a
uvědomí	uvědomit	k5eAaPmIp3nS	uvědomit
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
co	co	k3yQnSc4	co
žil	žít	k5eAaImAgMnS	žít
je	být	k5eAaImIp3nS	být
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
</s>
<s>
Pořád	pořád	k6eAd1	pořád
si	se	k3xPyFc3	se
ale	ale	k9	ale
dělá	dělat	k5eAaImIp3nS	dělat
falešné	falešný	k2eAgFnPc4d1	falešná
naděje	naděje	k1gFnPc4	naděje
a	a	k8xC	a
nepřipouští	připouštět	k5eNaImIp3nS	připouštět
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
ji	on	k3xPp3gFnSc4	on
mohl	moct	k5eAaImAgMnS	moct
ztratit	ztratit	k5eAaPmF	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Pořád	pořád	k6eAd1	pořád
ji	on	k3xPp3gFnSc4	on
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
<g/>
,	,	kIx,	,
přesvědčuje	přesvědčovat	k5eAaImIp3nS	přesvědčovat
ji	on	k3xPp3gFnSc4	on
a	a	k8xC	a
prosí	prosit	k5eAaImIp3nS	prosit
<g/>
.	.	kIx.	.
</s>
<s>
Fermina	Fermin	k2eAgFnSc1d1	Fermina
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
neoblomná	oblomný	k2eNgFnSc1d1	neoblomná
a	a	k8xC	a
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
neustoupí	ustoupit	k5eNaPmIp3nP	ustoupit
<g/>
.	.	kIx.	.
</s>
<s>
Florentino	Florentin	k2eAgNnSc1d1	Florentino
postupně	postupně	k6eAd1	postupně
poznává	poznávat	k5eAaImIp3nS	poznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
už	už	k6eAd1	už
nemá	mít	k5eNaImIp3nS	mít
šanci	šance	k1gFnSc4	šance
<g/>
.	.	kIx.	.
</s>
<s>
Stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
tělo	tělo	k1gNnSc1	tělo
bez	bez	k7c2	bez
duše	duše	k1gFnSc2	duše
a	a	k8xC	a
utápí	utápět	k5eAaImIp3nS	utápět
se	se	k3xPyFc4	se
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
smutku	smutek	k1gInSc6	smutek
<g/>
.	.	kIx.	.
</s>
<s>
Fermina	Fermin	k2eAgNnPc1d1	Fermin
se	se	k3xPyFc4	se
zatím	zatím	k6eAd1	zatím
seznámí	seznámit	k5eAaPmIp3nP	seznámit
s	s	k7c7	s
dotorem	dotor	k1gMnSc7	dotor
Juvenalem	Juvenal	k1gMnSc7	Juvenal
Urbinem	Urbin	k1gMnSc7	Urbin
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
i	i	k9	i
přes	přes	k7c4	přes
své	svůj	k3xOyFgNnSc4	svůj
mládí	mládí	k1gNnSc4	mládí
všeobecně	všeobecně	k6eAd1	všeobecně
uznávaný	uznávaný	k2eAgMnSc1d1	uznávaný
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
i	i	k9	i
dobrý	dobrý	k2eAgInSc4d1	dobrý
původ	původ	k1gInSc4	původ
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gInSc4	on
otec	otec	k1gMnSc1	otec
býval	bývat	k5eAaImAgMnS	bývat
věhlasným	věhlasný	k2eAgMnSc7d1	věhlasný
lékařem	lékař	k1gMnSc7	lékař
<g/>
,	,	kIx,	,
specializujícím	specializující	k2eAgMnSc7d1	specializující
se	se	k3xPyFc4	se
na	na	k7c4	na
choleru	cholera	k1gFnSc4	cholera
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ho	on	k3xPp3gMnSc4	on
nakonec	nakonec	k6eAd1	nakonec
také	také	k9	také
zahubila	zahubit	k5eAaPmAgFnS	zahubit
<g/>
.	.	kIx.	.
</s>
<s>
Doktoru	doktor	k1gMnSc3	doktor
Urbinovi	Urbin	k1gMnSc3	Urbin
se	se	k3xPyFc4	se
Fermina	Fermin	k2eAgFnSc1d1	Fermina
velice	velice	k6eAd1	velice
líbí	líbit	k5eAaImIp3nS	líbit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ona	onen	k3xDgFnSc1	onen
mu	on	k3xPp3gMnSc3	on
dává	dávat	k5eAaImIp3nS	dávat
okatě	okatě	k6eAd1	okatě
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemá	mít	k5eNaImIp3nS	mít
zájem	zájem	k1gInSc4	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Lorenzo	Lorenza	k1gFnSc5	Lorenza
Daza	Daz	k2eAgNnPc1d1	Daz
ale	ale	k8xC	ale
ihned	ihned	k6eAd1	ihned
vidí	vidět	k5eAaImIp3nS	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
tímto	tento	k3xDgMnSc7	tento
mužem	muž	k1gMnSc7	muž
by	by	k9	by
jeho	jeho	k3xOp3gFnSc4	jeho
dceru	dcera	k1gFnSc4	dcera
čekala	čekat	k5eAaImAgFnS	čekat
skvělá	skvělý	k2eAgFnSc1d1	skvělá
budoucnost	budoucnost	k1gFnSc1	budoucnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
ho	on	k3xPp3gNnSc4	on
před	před	k7c7	před
ní	on	k3xPp3gFnSc7	on
začne	začít	k5eAaPmIp3nS	začít
vychvalovat	vychvalovat	k5eAaImF	vychvalovat
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
jí	on	k3xPp3gFnSc3	on
podstrkává	podstrkávat	k5eAaImIp3nS	podstrkávat
dopisy	dopis	k1gInPc1	dopis
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jí	on	k3xPp3gFnSc3	on
doktor	doktor	k1gMnSc1	doktor
posílá	posílat	k5eAaImIp3nS	posílat
<g/>
.	.	kIx.	.
</s>
<s>
Fermina	Fermin	k2eAgFnSc1d1	Fermina
ho	on	k3xPp3gMnSc4	on
brzy	brzy	k6eAd1	brzy
poznává	poznávat	k5eAaImIp3nS	poznávat
blíž	blízce	k6eAd2	blízce
a	a	k8xC	a
získává	získávat	k5eAaImIp3nS	získávat
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
značnou	značný	k2eAgFnSc4d1	značná
náklonnost	náklonnost	k1gFnSc4	náklonnost
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
začne	začít	k5eAaPmIp3nS	začít
chystat	chystat	k5eAaImF	chystat
svatba	svatba	k1gFnSc1	svatba
<g/>
.	.	kIx.	.
</s>
<s>
Florentinův	Florentinův	k2eAgInSc1d1	Florentinův
smutek	smutek	k1gInSc1	smutek
za	za	k7c4	za
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
ani	ani	k8xC	ani
v	v	k7c6	v
nejmenším	malý	k2eAgMnSc6d3	nejmenší
neochladl	ochladnout	k5eNaPmAgInS	ochladnout
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
ještě	ještě	k6eAd1	ještě
zesílil	zesílit	k5eAaPmAgMnS	zesílit
<g/>
.	.	kIx.	.
</s>
<s>
Florentino	Florentin	k2eAgNnSc1d1	Florentino
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
neštěstí	neštěstí	k1gNnSc6	neštěstí
prchá	prchat	k5eAaImIp3nS	prchat
na	na	k7c4	na
moře	moře	k1gNnSc4	moře
v	v	k7c6	v
lodi	loď	k1gFnSc6	loď
Karibské	karibský	k2eAgFnSc6d1	karibská
říční	říční	k2eAgFnSc1d1	říční
paroplavba	paroplavba	k1gFnSc1	paroplavba
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vede	vést	k5eAaImIp3nS	vést
jeho	jeho	k3xOp3gMnSc7	jeho
strýc	strýc	k1gMnSc1	strýc
Lev	Lev	k1gMnSc1	Lev
XII	XII	kA	XII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
bratr	bratr	k1gMnSc1	bratr
Florentinova	Florentinův	k2eAgMnSc2d1	Florentinův
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
ale	ale	k9	ale
jeho	jeho	k3xOp3gFnSc4	jeho
matku	matka	k1gFnSc4	matka
Tránsito	Tránsit	k2eAgNnSc4d1	Tránsito
nikdy	nikdy	k6eAd1	nikdy
nevzal	vzít	k5eNaPmAgMnS	vzít
a	a	k8xC	a
syna	syn	k1gMnSc4	syn
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
tajil	tajit	k5eAaImAgMnS	tajit
<g/>
.	.	kIx.	.
</s>
<s>
Lev	Lev	k1gMnSc1	Lev
XII	XII	kA	XII
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
aspoň	aspoň	k9	aspoň
trochu	trochu	k6eAd1	trochu
pomáhat	pomáhat	k5eAaImF	pomáhat
svému	svůj	k3xOyFgMnSc3	svůj
synovci	synovec	k1gMnSc3	synovec
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
ho	on	k3xPp3gMnSc4	on
zaměstná	zaměstnat	k5eAaPmIp3nS	zaměstnat
v	v	k7c6	v
Karibské	karibský	k2eAgFnSc6d1	karibská
říční	říční	k2eAgFnSc6d1	říční
paroplavbě	paroplavba	k1gFnSc6	paroplavba
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
Florentino	Florentin	k2eAgNnSc1d1	Florentino
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Fermina	Fermin	k2eAgFnSc1d1	Fermina
vdává	vdávat	k5eAaImIp3nS	vdávat
a	a	k8xC	a
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
s	s	k7c7	s
doktorem	doktor	k1gMnSc7	doktor
Urbinem	Urbin	k1gMnSc7	Urbin
na	na	k7c4	na
svatební	svatební	k2eAgFnSc4d1	svatební
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Florentino	Florentin	k2eAgNnSc1d1	Florentino
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
ale	ale	k8xC	ale
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
zapomenout	zapomenout	k5eAaPmF	zapomenout
ani	ani	k8xC	ani
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Moc	moc	k6eAd1	moc
si	se	k3xPyFc3	se
přeje	přát	k5eAaImIp3nS	přát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Fermina	Fermin	k2eAgFnSc1d1	Fermina
viděla	vidět	k5eAaImAgFnS	vidět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
kvůli	kvůli	k7c3	kvůli
ní	on	k3xPp3gFnSc3	on
trpí	trpět	k5eAaImIp3nS	trpět
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgInPc4d1	celý
dny	den	k1gInPc4	den
je	být	k5eAaImIp3nS	být
duchem	duch	k1gMnSc7	duch
nepřítomen	přítomen	k2eNgMnSc1d1	nepřítomen
a	a	k8xC	a
vyžívá	vyžívat	k5eAaImIp3nS	vyžívat
se	se	k3xPyFc4	se
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
žalu	žal	k1gInSc6	žal
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
ale	ale	k8xC	ale
tato	tento	k3xDgFnSc1	tento
plavba	plavba	k1gFnSc1	plavba
znamená	znamenat	k5eAaImIp3nS	znamenat
zásadní	zásadní	k2eAgInSc4d1	zásadní
obrat	obrat	k1gInSc4	obrat
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
sám	sám	k3xTgMnSc1	sám
v	v	k7c6	v
noční	noční	k2eAgFnSc6d1	noční
kajutě	kajuta	k1gFnSc6	kajuta
<g/>
,	,	kIx,	,
přijde	přijít	k5eAaPmIp3nS	přijít
tam	tam	k6eAd1	tam
za	za	k7c7	za
ním	on	k3xPp3gInSc7	on
neznámá	známý	k2eNgFnSc1d1	neznámá
žena	žena	k1gFnSc1	žena
a	a	k8xC	a
pomiluje	pomilovat	k5eAaPmIp3nS	pomilovat
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
Florentino	Florentin	k2eAgNnSc1d1	Florentino
stačí	stačit	k5eAaBmIp3nS	stačit
neznámou	neznámá	k1gFnSc4	neznámá
ženu	žena	k1gFnSc4	žena
identifikovat	identifikovat	k5eAaBmF	identifikovat
<g/>
,	,	kIx,	,
zase	zase	k9	zase
mu	on	k3xPp3gMnSc3	on
zmizí	zmizet	k5eAaPmIp3nS	zmizet
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
den	den	k1gInSc1	den
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
žádná	žádný	k3yNgFnSc1	žádný
žena	žena	k1gFnSc1	žena
nehlásí	hlásit	k5eNaImIp3nS	hlásit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
Florentino	Florentin	k2eAgNnSc1d1	Florentino
může	moct	k5eAaImIp3nS	moct
jenom	jenom	k9	jenom
hádat	hádat	k5eAaImF	hádat
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
to	ten	k3xDgNnSc4	ten
byla	být	k5eAaImAgFnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Tu	ten	k3xDgFnSc4	ten
noc	noc	k1gFnSc4	noc
si	se	k3xPyFc3	se
ale	ale	k9	ale
uvědomí	uvědomit	k5eAaPmIp3nS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
milostná	milostný	k2eAgNnPc1d1	milostné
dobrodružství	dobrodružství	k1gNnPc1	dobrodružství
mu	on	k3xPp3gNnSc3	on
mohou	moct	k5eAaImIp3nP	moct
částečně	částečně	k6eAd1	částečně
pomoci	pomoct	k5eAaPmF	pomoct
se	se	k3xPyFc4	se
odpoutat	odpoutat	k5eAaPmF	odpoutat
od	od	k7c2	od
neutěšené	utěšený	k2eNgFnSc2d1	neutěšená
reality	realita	k1gFnSc2	realita
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
těch	ten	k3xDgNnPc2	ten
51	[number]	k4	51
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
co	co	k9	co
Florentino	Florentin	k2eAgNnSc1d1	Florentino
čekal	čekat	k5eAaImAgInS	čekat
na	na	k7c4	na
smrt	smrt	k1gFnSc4	smrt
doktora	doktor	k1gMnSc2	doktor
Urbina	Urbin	k1gMnSc2	Urbin
<g/>
,	,	kIx,	,
navázal	navázat	k5eAaPmAgInS	navázat
přes	přes	k7c4	přes
600	[number]	k4	600
milostných	milostný	k2eAgInPc2d1	milostný
poměrů	poměr	k1gInPc2	poměr
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc1	některý
byly	být	k5eAaImAgFnP	být
pouze	pouze	k6eAd1	pouze
jednorázové	jednorázový	k2eAgFnPc1d1	jednorázová
<g/>
,	,	kIx,	,
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
trvaly	trvat	k5eAaImAgFnP	trvat
dosud	dosud	k6eAd1	dosud
<g/>
.	.	kIx.	.
</s>
<s>
Žádná	žádný	k3yNgFnSc1	žádný
z	z	k7c2	z
žen	žena	k1gFnPc2	žena
ale	ale	k8xC	ale
nemohla	moct	k5eNaImAgFnS	moct
počítat	počítat	k5eAaImF	počítat
s	s	k7c7	s
žádným	žádný	k3yNgInSc7	žádný
hlubším	hluboký	k2eAgInSc7d2	hlubší
citem	cit	k1gInSc7	cit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gNnSc1	jeho
srdce	srdce	k1gNnSc1	srdce
nikdy	nikdy	k6eAd1	nikdy
nepřestalo	přestat	k5eNaPmAgNnS	přestat
patřit	patřit	k5eAaImF	patřit
Fermině	Fermin	k2eAgFnSc6d1	Fermina
<g/>
.	.	kIx.	.
</s>
<s>
Florentino	Florentin	k2eAgNnSc1d1	Florentino
za	za	k7c4	za
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
i	i	k9	i
velmi	velmi	k6eAd1	velmi
povýšil	povýšit	k5eAaPmAgMnS	povýšit
v	v	k7c6	v
práci	práce	k1gFnSc6	práce
<g/>
.	.	kIx.	.
</s>
<s>
Starý	starý	k2eAgMnSc1d1	starý
strýc	strýc	k1gMnSc1	strýc
Lev	Lev	k1gMnSc1	Lev
XII	XII	kA	XII
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
odebral	odebrat	k5eAaPmAgInS	odebrat
na	na	k7c4	na
penzi	penze	k1gFnSc4	penze
a	a	k8xC	a
Florentina	Florentin	k1gMnSc4	Florentin
učinil	učinit	k5eAaPmAgMnS	učinit
svým	svůj	k3xOyFgMnSc7	svůj
nástupcem	nástupce	k1gMnSc7	nástupce
na	na	k7c4	na
post	post	k1gInSc4	post
ředitele	ředitel	k1gMnSc2	ředitel
Karibské	karibský	k2eAgFnSc2d1	karibská
říční	říční	k2eAgFnSc2d1	říční
paroplavby	paroplavba	k1gFnSc2	paroplavba
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Florentina	Florentin	k1gMnSc2	Florentin
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
vážený	vážený	k2eAgMnSc1d1	vážený
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInSc1	jeho
osobní	osobní	k2eAgInSc1d1	osobní
život	život	k1gInSc1	život
byl	být	k5eAaImAgInS	být
vždy	vždy	k6eAd1	vždy
trnem	trn	k1gInSc7	trn
v	v	k7c4	v
oku	oka	k1gFnSc4	oka
místním	místní	k2eAgFnPc3d1	místní
drbnám	drbna	k1gFnPc3	drbna
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
neustále	neustále	k6eAd1	neustále
šířily	šířit	k5eAaImAgFnP	šířit
všemožné	všemožný	k2eAgFnPc1d1	všemožná
pomluvy	pomluva	k1gFnPc1	pomluva
<g/>
,	,	kIx,	,
kterým	který	k3yRgFnPc3	který
ale	ale	k9	ale
stejně	stejně	k6eAd1	stejně
nevěnoval	věnovat	k5eNaImAgMnS	věnovat
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Fermina	Fermin	k2eAgFnSc1d1	Fermina
žila	žít	k5eAaImAgFnS	žít
relativně	relativně	k6eAd1	relativně
klidný	klidný	k2eAgInSc4d1	klidný
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
doktorem	doktor	k1gMnSc7	doktor
Urbinem	Urbin	k1gMnSc7	Urbin
měla	mít	k5eAaImAgFnS	mít
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
všichni	všechen	k3xTgMnPc1	všechen
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
prostorné	prostorný	k2eAgFnSc6d1	prostorná
vile	vila	k1gFnSc6	vila
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
od	od	k7c2	od
Urbinovy	Urbinův	k2eAgFnSc2d1	Urbinův
nesnesitelné	snesitelný	k2eNgFnSc2d1	nesnesitelná
<g/>
,	,	kIx,	,
panovačné	panovačný	k2eAgFnSc2d1	panovačná
matky	matka	k1gFnSc2	matka
a	a	k8xC	a
sester	sestra	k1gFnPc2	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
manželem	manžel	k1gMnSc7	manžel
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
doplňovali	doplňovat	k5eAaImAgMnP	doplňovat
a	a	k8xC	a
v	v	k7c6	v
poklidu	poklid	k1gInSc6	poklid
prožívali	prožívat	k5eAaImAgMnP	prožívat
podzim	podzim	k1gInSc4	podzim
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Urbinově	Urbinův	k2eAgFnSc6d1	Urbinův
náhlé	náhlý	k2eAgFnSc6d1	náhlá
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
Fermina	Fermin	k2eAgFnSc1d1	Fermina
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Nedovedla	dovést	k5eNaPmAgFnS	dovést
si	se	k3xPyFc3	se
dobře	dobře	k6eAd1	dobře
představit	představit	k5eAaPmF	představit
svůj	svůj	k3xOyFgInSc4	svůj
další	další	k2eAgInSc4d1	další
život	život	k1gInSc4	život
bez	bez	k7c2	bez
manžela	manžel	k1gMnSc2	manžel
<g/>
.	.	kIx.	.
</s>
<s>
Přichází	přicházet	k5eAaImIp3nS	přicházet
den	den	k1gInSc4	den
pohřbu	pohřeb	k1gInSc2	pohřeb
a	a	k8xC	a
na	na	k7c6	na
mezi	mezi	k7c7	mezi
mnoha	mnoho	k4c7	mnoho
smutečními	smuteční	k2eAgMnPc7d1	smuteční
hosty	host	k1gMnPc7	host
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
Florentino	Florentin	k2eAgNnSc1d1	Florentino
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nechce	chtít	k5eNaImIp3nS	chtít
promarnit	promarnit	k5eAaPmF	promarnit
chvíli	chvíle	k1gFnSc4	chvíle
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yRgFnSc4	který
tak	tak	k6eAd1	tak
dlouho	dlouho	k6eAd1	dlouho
čekal	čekat	k5eAaImAgMnS	čekat
<g/>
.	.	kIx.	.
</s>
<s>
Zachová	zachovat	k5eAaPmIp3nS	zachovat
se	se	k3xPyFc4	se
ale	ale	k9	ale
příliš	příliš	k6eAd1	příliš
zbrkle	zbrkle	k6eAd1	zbrkle
a	a	k8xC	a
bezohledně	bezohledně	k6eAd1	bezohledně
k	k	k7c3	k
situaci	situace	k1gFnSc3	situace
<g/>
,	,	kIx,	,
když	když	k8xS	když
opět	opět	k6eAd1	opět
vyzná	vyznat	k5eAaBmIp3nS	vyznat
Fermině	Fermin	k2eAgFnSc3d1	Fermina
lásku	láska	k1gFnSc4	láska
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
vzteky	vztek	k1gInPc4	vztek
bez	bez	k7c2	bez
sebe	se	k3xPyFc2	se
<g/>
,	,	kIx,	,
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
pochopit	pochopit	k5eAaPmF	pochopit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jí	on	k3xPp3gFnSc3	on
může	moct	k5eAaImIp3nS	moct
tohle	tenhle	k3xDgNnSc4	tenhle
říct	říct	k5eAaPmF	říct
v	v	k7c4	v
den	den	k1gInSc4	den
pohřbu	pohřeb	k1gInSc2	pohřeb
jejího	její	k3xOp3gMnSc2	její
manžela	manžel	k1gMnSc2	manžel
<g/>
.	.	kIx.	.
</s>
<s>
Ostře	ostro	k6eAd1	ostro
ho	on	k3xPp3gMnSc4	on
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
a	a	k8xC	a
vykáže	vykázat	k5eAaPmIp3nS	vykázat
ven	ven	k6eAd1	ven
<g/>
.	.	kIx.	.
</s>
<s>
Florentino	Florentin	k2eAgNnSc1d1	Florentino
je	být	k5eAaImIp3nS	být
nešťastný	šťastný	k2eNgMnSc1d1	nešťastný
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ani	ani	k9	ani
jeho	jeho	k3xOp3gFnSc1	jeho
druhá	druhý	k4xOgFnSc1	druhý
příležitost	příležitost	k1gFnSc1	příležitost
nepovedla	povést	k5eNaPmAgFnS	povést
<g/>
.	.	kIx.	.
</s>
<s>
Nechce	chtít	k5eNaImIp3nS	chtít
se	se	k3xPyFc4	se
ale	ale	k9	ale
vzdát	vzdát	k5eAaPmF	vzdát
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pak	pak	k6eAd1	pak
už	už	k9	už
by	by	kYmCp3nS	by
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
jeho	jeho	k3xOp3gFnSc4	jeho
život	život	k1gInSc1	život
neměl	mít	k5eNaImAgInS	mít
žádnou	žádný	k3yNgFnSc4	žádný
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Odhodlá	odhodlat	k5eAaPmIp3nS	odhodlat
se	se	k3xPyFc4	se
a	a	k8xC	a
napíše	napsat	k5eAaBmIp3nS	napsat
Fermině	Fermin	k2eAgFnSc3d1	Fermina
omluvný	omluvný	k2eAgInSc4d1	omluvný
dopis	dopis	k1gInSc4	dopis
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
mu	on	k3xPp3gMnSc3	on
ale	ale	k8xC	ale
odpoví	odpovědět	k5eAaPmIp3nS	odpovědět
dopisem	dopis	k1gInSc7	dopis
plným	plný	k2eAgInSc7d1	plný
urážek	urážka	k1gFnPc2	urážka
a	a	k8xC	a
nadávek	nadávka	k1gFnPc2	nadávka
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
ale	ale	k9	ale
nevzdá	vzdát	k5eNaPmIp3nS	vzdát
<g/>
,	,	kIx,	,
píše	psát	k5eAaImIp3nS	psát
jí	on	k3xPp3gFnSc3	on
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
ne	ne	k9	ne
žádná	žádný	k3yNgNnPc4	žádný
horoucí	horoucí	k2eAgNnPc4d1	horoucí
a	a	k8xC	a
zoufalá	zoufalý	k2eAgNnPc4d1	zoufalé
vyznání	vyznání	k1gNnPc4	vyznání
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
inteligentní	inteligentní	k2eAgFnPc1d1	inteligentní
úvahy	úvaha	k1gFnPc1	úvaha
o	o	k7c6	o
lásce	láska	k1gFnSc6	láska
a	a	k8xC	a
životě	život	k1gInSc6	život
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Útěchou	útěcha	k1gFnSc7	útěcha
mu	on	k3xPp3gMnSc3	on
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
neotevřené	otevřený	k2eNgInPc1d1	neotevřený
dopisy	dopis	k1gInPc1	dopis
nevrací	vracet	k5eNaImIp3nP	vracet
obratem	obratem	k6eAd1	obratem
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
si	se	k3xPyFc3	se
ale	ale	k9	ale
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
obavách	obava	k1gFnPc6	obava
představuje	představovat	k5eAaImIp3nS	představovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jeho	jeho	k3xOp3gInPc4	jeho
dopisy	dopis	k1gInPc4	dopis
rozezlená	rozezlený	k2eAgFnSc1d1	rozezlená
Fermina	Fermin	k2eAgMnSc4d1	Fermin
hází	házet	k5eAaImIp3nS	házet
do	do	k7c2	do
ohně	oheň	k1gInSc2	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
ale	ale	k9	ale
úplně	úplně	k6eAd1	úplně
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Fermina	Fermin	k2eAgFnSc1d1	Fermina
totiž	totiž	k9	totiž
čekala	čekat	k5eAaImAgFnS	čekat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
Florentino	Florentin	k2eAgNnSc1d1	Florentino
odpoví	odpovědět	k5eAaPmIp3nS	odpovědět
na	na	k7c4	na
její	její	k3xOp3gInSc4	její
urážlivý	urážlivý	k2eAgInSc4d1	urážlivý
dopis	dopis	k1gInSc4	dopis
ve	v	k7c6	v
stejném	stejný	k2eAgMnSc6d1	stejný
duchu	duch	k1gMnSc6	duch
<g/>
,	,	kIx,	,
a	a	k8xC	a
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
víc	hodně	k6eAd2	hodně
je	být	k5eAaImIp3nS	být
překvapena	překvapit	k5eAaPmNgFnS	překvapit
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nestane	stanout	k5eNaPmIp3nS	stanout
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
toho	ten	k3xDgInSc2	ten
okamžiku	okamžik	k1gInSc2	okamžik
si	se	k3xPyFc3	se
každý	každý	k3xTgMnSc1	každý
jeho	jeho	k3xOp3gInSc7	jeho
dopis	dopis	k1gInSc1	dopis
pečlivě	pečlivě	k6eAd1	pečlivě
přečte	přečíst	k5eAaPmIp3nS	přečíst
a	a	k8xC	a
za	za	k7c4	za
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
odhodlá	odhodlat	k5eAaPmIp3nS	odhodlat
i	i	k9	i
k	k	k7c3	k
odpovědi	odpověď	k1gFnSc3	odpověď
<g/>
.	.	kIx.	.
</s>
<s>
Začnou	začít	k5eAaPmIp3nP	začít
si	se	k3xPyFc3	se
dopisovat	dopisovat	k5eAaImF	dopisovat
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
dojde	dojít	k5eAaPmIp3nS	dojít
i	i	k9	i
k	k	k7c3	k
setkání	setkání	k1gNnSc3	setkání
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
stanou	stanout	k5eAaPmIp3nP	stanout
pravidelná	pravidelný	k2eAgFnSc1d1	pravidelná
odpoledne	odpoledne	k6eAd1	odpoledne
u	u	k7c2	u
karet	kareta	k1gFnPc2	kareta
a	a	k8xC	a
kávy	káva	k1gFnSc2	káva
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
Fermina	Fermin	k2eAgInSc2d1	Fermin
zmíní	zmínit	k5eAaPmIp3nS	zmínit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
chtěla	chtít	k5eAaImAgFnS	chtít
pryč	pryč	k6eAd1	pryč
z	z	k7c2	z
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
Florentino	Florentin	k2eAgNnSc1d1	Florentino
okamžitě	okamžitě	k6eAd1	okamžitě
zařídí	zařídit	k5eAaPmIp3nS	zařídit
místa	místo	k1gNnPc4	místo
na	na	k7c6	na
přepychovém	přepychový	k2eAgInSc6d1	přepychový
parníku	parník	k1gInSc6	parník
jeho	jeho	k3xOp3gFnSc2	jeho
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Zapomenou	zapomenout	k5eAaPmIp3nP	zapomenout
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
dosavadní	dosavadní	k2eAgInSc4d1	dosavadní
život	život	k1gInSc4	život
a	a	k8xC	a
prostě	prostě	k6eAd1	prostě
odplují	odplout	k5eAaPmIp3nP	odplout
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
se	se	k3xPyFc4	se
velice	velice	k6eAd1	velice
sblíží	sblížit	k5eAaPmIp3nP	sblížit
a	a	k8xC	a
začnou	začít	k5eAaPmIp3nP	začít
si	se	k3xPyFc3	se
uvědomovat	uvědomovat	k5eAaImF	uvědomovat
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
k	k	k7c3	k
sobě	se	k3xPyFc3	se
navzájem	navzájem	k6eAd1	navzájem
cítí	cítit	k5eAaImIp3nP	cítit
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
ta	ten	k3xDgFnSc1	ten
stará	starý	k2eAgFnSc1d1	stará
zašlá	zašlý	k2eAgFnSc1d1	zašlá
láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
pojila	pojit	k5eAaImAgFnS	pojit
v	v	k7c6	v
dávném	dávný	k2eAgNnSc6d1	dávné
mládí	mládí	k1gNnSc6	mládí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
zbrusu	zbrusu	k6eAd1	zbrusu
nově	nově	k6eAd1	nově
objevená	objevený	k2eAgFnSc1d1	objevená
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vlastně	vlastně	k9	vlastně
pokračování	pokračování	k1gNnSc1	pokračování
jejich	jejich	k3xOp3gFnSc2	jejich
naivní	naivní	k2eAgFnSc2d1	naivní
<g/>
,	,	kIx,	,
mladé	mladý	k2eAgFnSc2d1	mladá
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
přeskočila	přeskočit	k5eAaPmAgNnP	přeskočit
dlouhá	dlouhý	k2eAgNnPc1d1	dlouhé
léta	léto	k1gNnPc1	léto
manželského	manželský	k2eAgInSc2d1	manželský
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
a	a	k8xC	a
nedotčena	dotčen	k2eNgFnSc1d1	nedotčena
teď	teď	k6eAd1	teď
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
jako	jako	k9	jako
ryzí	ryzí	k2eAgInSc1d1	ryzí
a	a	k8xC	a
laskavý	laskavý	k2eAgInSc1d1	laskavý
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
</s>
<s>
Užívají	užívat	k5eAaImIp3nP	užívat
si	se	k3xPyFc3	se
společné	společný	k2eAgInPc4d1	společný
okamžiky	okamžik	k1gInPc4	okamžik
plnými	plný	k2eAgInPc7d1	plný
doušky	doušek	k1gInPc7	doušek
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
loď	loď	k1gFnSc1	loď
přistane	přistat	k5eAaPmIp3nS	přistat
<g/>
,	,	kIx,	,
vyloží	vyložit	k5eAaPmIp3nS	vyložit
cestující	cestující	k1gMnSc1	cestující
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
nabrat	nabrat	k5eAaPmF	nabrat
další	další	k2eAgNnSc1d1	další
na	na	k7c4	na
zpáteční	zpáteční	k2eAgFnSc4d1	zpáteční
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
Florentino	Florentin	k2eAgNnSc4d1	Florentino
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyvěsí	vyvěsit	k5eAaPmIp3nS	vyvěsit
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
žlutou	žlutý	k2eAgFnSc4d1	žlutá
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
značí	značit	k5eAaImIp3nS	značit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
je	být	k5eAaImIp3nS	být
cholera	cholera	k1gFnSc1	cholera
a	a	k8xC	a
že	že	k8xS	že
loď	loď	k1gFnSc1	loď
nesmí	smět	k5eNaImIp3nS	smět
nikde	nikde	k6eAd1	nikde
přistát	přistát	k5eAaImF	přistát
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
také	také	k9	také
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
pouze	pouze	k6eAd1	pouze
Florentino	Florentin	k2eAgNnSc1d1	Florentino
<g/>
,	,	kIx,	,
Fermina	Fermin	k2eAgNnPc1d1	Fermin
a	a	k8xC	a
kapitán	kapitán	k1gMnSc1	kapitán
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
pomalu	pomalu	k6eAd1	pomalu
vrací	vracet	k5eAaImIp3nS	vracet
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
městu	město	k1gNnSc3	město
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
si	se	k3xPyFc3	se
uvědomují	uvědomovat	k5eAaImIp3nP	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
návrat	návrat	k1gInSc1	návrat
to	ten	k3xDgNnSc4	ten
všechno	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
spolu	spolu	k6eAd1	spolu
prožili	prožít	k5eAaPmAgMnP	prožít
sami	sám	k3xTgMnPc1	sám
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
<g/>
,	,	kIx,	,
zničí	zničit	k5eAaPmIp3nS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
Florentino	Florentin	k2eAgNnSc1d1	Florentino
řekne	říct	k5eAaPmIp3nS	říct
kapitánovi	kapitán	k1gMnSc3	kapitán
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nechal	nechat	k5eAaPmAgMnS	nechat
žlutou	žlutý	k2eAgFnSc4d1	žlutá
vlajku	vlajka	k1gFnSc4	vlajka
viset	viset	k5eAaImF	viset
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
loď	loď	k1gFnSc4	loď
otočil	otočit	k5eAaPmAgMnS	otočit
a	a	k8xC	a
místo	místo	k1gNnSc4	místo
do	do	k7c2	do
města	město	k1gNnSc2	město
plul	plout	k5eAaImAgInS	plout
do	do	k7c2	do
dáli	dál	k1gFnSc2	dál
po	po	k7c6	po
řece	řeka	k1gFnSc6	řeka
Magdaleně	Magdalena	k1gFnSc6	Magdalena
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
kapitánovu	kapitánův	k2eAgFnSc4d1	kapitánova
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
dlouho	dlouho	k6eAd1	dlouho
se	se	k3xPyFc4	se
chtějí	chtít	k5eAaImIp3nP	chtít
plavit	plavit	k5eAaImF	plavit
takhle	takhle	k6eAd1	takhle
odnikud	odnikud	k6eAd1	odnikud
nikam	nikam	k6eAd1	nikam
<g/>
,	,	kIx,	,
Florentino	Florentin	k2eAgNnSc1d1	Florentino
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
měl	mít	k5eAaImAgMnS	mít
připravené	připravený	k2eAgNnSc4d1	připravené
celých	celý	k2eAgNnPc2d1	celé
51	[number]	k4	51
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
,,	,,	k?	,,
<g/>
Celý	celý	k2eAgInSc1d1	celý
život	život	k1gInSc1	život
<g/>
.	.	kIx.	.
"	"	kIx"	"
řekl	říct	k5eAaPmAgMnS	říct
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmová	filmový	k2eAgFnSc1d1	filmová
adaptace	adaptace	k1gFnSc1	adaptace
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byl	být	k5eAaImAgMnS	být
podle	podle	k7c2	podle
knihy	kniha	k1gFnSc2	kniha
natočen	natočen	k2eAgInSc4d1	natočen
stejnojmenný	stejnojmenný	k2eAgInSc4d1	stejnojmenný
film	film	k1gInSc4	film
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Mika	Mik	k1gMnSc2	Mik
Newella	Newell	k1gMnSc2	Newell
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
Javier	Javier	k1gMnSc1	Javier
Bardem	bard	k1gMnSc7	bard
<g/>
,	,	kIx,	,
Giovanna	Giovanna	k1gFnSc1	Giovanna
Mezzogiorno	Mezzogiorno	k1gNnSc1	Mezzogiorno
<g/>
,	,	kIx,	,
Laura	Laura	k1gFnSc1	Laura
Harring	Harring	k1gInSc1	Harring
</s>
</p>
