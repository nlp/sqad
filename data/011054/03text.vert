<p>
<s>
Some	Some	k1gFnSc1	Some
Girls	girl	k1gFnPc2	girl
je	být	k5eAaImIp3nS	být
album	album	k1gNnSc1	album
The	The	k1gMnPc2	The
Rolling	Rolling	k1gInSc4	Rolling
Stones	Stonesa	k1gFnPc2	Stonesa
vydané	vydaný	k2eAgFnPc4d1	vydaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
<s>
Kritikou	kritika	k1gFnSc7	kritika
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
nahráli	nahrát	k5eAaBmAgMnP	nahrát
od	od	k7c2	od
vydání	vydání	k1gNnSc2	vydání
alba	album	k1gNnSc2	album
Exile	exil	k1gInSc5	exil
on	on	k3xPp3gMnSc1	on
Main	Main	k1gMnSc1	Main
Street	Street	k1gMnSc1	Street
<g/>
.	.	kIx.	.
</s>
<s>
Some	Some	k1gFnSc1	Some
Girls	girl	k1gFnPc2	girl
je	být	k5eAaImIp3nS	být
také	také	k9	také
nejúspěšnější	úspěšný	k2eAgNnSc1d3	nejúspěšnější
album	album	k1gNnSc1	album
kapely	kapela	k1gFnSc2	kapela
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
prodalo	prodat	k5eAaPmAgNnS	prodat
asi	asi	k9	asi
6	[number]	k4	6
milionů	milion	k4xCgInPc2	milion
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
sice	sice	k8xC	sice
Rolling	Rolling	k1gInSc1	Rolling
Stones	Stones	k1gMnSc1	Stones
zůstávali	zůstávat	k5eAaImAgMnP	zůstávat
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnSc7d1	populární
skupinou	skupina	k1gFnSc7	skupina
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc1	jejich
nahrávky	nahrávka	k1gFnPc1	nahrávka
však	však	k9	však
trpěly	trpět	k5eAaImAgFnP	trpět
Jaggerovou	Jaggerův	k2eAgFnSc7d1	Jaggerova
fascinací	fascinace	k1gFnSc7	fascinace
slávou	sláva	k1gFnSc7	sláva
a	a	k8xC	a
Richardsovou	Richardsová	k1gFnSc7	Richardsová
prohlubující	prohlubující	k2eAgFnSc7d1	prohlubující
se	se	k3xPyFc4	se
závislostí	závislost	k1gFnSc7	závislost
na	na	k7c6	na
drogách	droga	k1gFnPc6	droga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
titulních	titulní	k2eAgFnPc2d1	titulní
stránek	stránka	k1gFnPc2	stránka
vytlačily	vytlačit	k5eAaPmAgInP	vytlačit
punk	punk	k1gInSc4	punk
a	a	k8xC	a
disco	disco	k1gNnSc4	disco
<g/>
,	,	kIx,	,
přišli	přijít	k5eAaPmAgMnP	přijít
s	s	k7c7	s
albem	album	k1gNnSc7	album
Some	Som	k1gInSc2	Som
Girls	girl	k1gFnPc2	girl
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
pádnou	pádný	k2eAgFnSc7d1	pádná
odpovědí	odpověď	k1gFnSc7	odpověď
vkusu	vkus	k1gInSc2	vkus
mladé	mladý	k2eAgFnSc2d1	mladá
generace	generace	k1gFnSc2	generace
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
disco-bluesovou	discoluesový	k2eAgFnSc7d1	disco-bluesový
vypalovačkou	vypalovačka	k1gFnSc7	vypalovačka
"	"	kIx"	"
<g/>
Miss	miss	k1gFnSc1	miss
You	You	k1gFnSc2	You
<g/>
"	"	kIx"	"
a	a	k8xC	a
i	i	k9	i
zbytek	zbytek	k1gInSc1	zbytek
alba	album	k1gNnSc2	album
je	být	k5eAaImIp3nS	být
nabitý	nabitý	k2eAgInSc1d1	nabitý
energií	energie	k1gFnSc7	energie
<g/>
,	,	kIx,	,
jaká	jaký	k3yRgFnSc1	jaký
nebyla	být	k5eNaImAgFnS	být
u	u	k7c2	u
skupiny	skupina	k1gFnSc2	skupina
ke	k	k7c3	k
slyšení	slyšení	k1gNnSc3	slyšení
minimálně	minimálně	k6eAd1	minimálně
od	od	k7c2	od
Exile	exil	k1gInSc5	exil
on	on	k3xPp3gMnSc1	on
Main	Main	k1gMnSc1	Main
St.	st.	kA	st.
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
si	se	k3xPyFc3	se
Rolling	Rolling	k1gInSc4	Rolling
Stones	Stonesa	k1gFnPc2	Stonesa
z	z	k7c2	z
moderních	moderní	k2eAgInPc2d1	moderní
stylů	styl	k1gInPc2	styl
osvojili	osvojit	k5eAaPmAgMnP	osvojit
disco	disco	k1gNnSc7	disco
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
nepřijali	přijmout	k5eNaPmAgMnP	přijmout
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
punk	punk	k1gInSc4	punk
<g/>
,	,	kIx,	,
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
jejich	jejich	k3xOp3gNnSc4	jejich
rockový	rockový	k2eAgInSc1d1	rockový
zvuk	zvuk	k1gInSc1	zvuk
zdrsněl	zdrsnět	k5eAaPmAgInS	zdrsnět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
jak	jak	k6eAd1	jak
zpustlé	zpustlý	k2eAgFnPc1d1	zpustlá
homosexuální	homosexuální	k2eAgFnPc1d1	homosexuální
fantazie	fantazie	k1gFnPc1	fantazie
v	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
"	"	kIx"	"
<g/>
When	When	k1gNnSc1	When
the	the	k?	the
Whip	Whip	k1gMnSc1	Whip
Comes	Comes	k1gMnSc1	Comes
Down	Down	k1gMnSc1	Down
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
bizarní	bizarní	k2eAgFnSc4d1	bizarní
až	až	k8xS	až
mizogynní	mizogynní	k2eAgFnSc4d1	mizogynní
titulní	titulní	k2eAgFnSc4d1	titulní
píseň	píseň	k1gFnSc4	píseň
<g/>
,	,	kIx,	,
Richardsova	Richardsův	k2eAgFnSc1d1	Richardsova
zpověď	zpověď	k1gFnSc1	zpověď
psance	psanec	k1gMnSc2	psanec
"	"	kIx"	"
<g/>
Before	Befor	k1gInSc5	Befor
They	Thea	k1gFnPc4	Thea
Make	Mak	k1gInPc4	Mak
Me	Me	k1gFnSc2	Me
Run	Runa	k1gFnPc2	Runa
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
dekadentní	dekadentní	k2eAgInSc1d1	dekadentní
závěr	závěr	k1gInSc1	závěr
obstaraný	obstaraný	k2eAgInSc1d1	obstaraný
písní	píseň	k1gFnSc7	píseň
"	"	kIx"	"
<g/>
Shattered	Shattered	k1gInSc1	Shattered
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
k	k	k7c3	k
nepoznání	nepoznání	k1gNnSc3	nepoznání
skladba	skladba	k1gFnSc1	skladba
skupiny	skupina	k1gFnSc2	skupina
Temptations	Temptationsa	k1gFnPc2	Temptationsa
"	"	kIx"	"
<g/>
Just	just	k6eAd1	just
My	my	k3xPp1nPc1	my
Imagination	Imagination	k1gInSc1	Imagination
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kapela	kapela	k1gFnSc1	kapela
rozpoutává	rozpoutávat	k5eAaImIp3nS	rozpoutávat
zničující	zničující	k2eAgFnSc4d1	zničující
parodii	parodie	k1gFnSc4	parodie
na	na	k7c4	na
country	country	k2eAgFnSc4d1	country
"	"	kIx"	"
<g/>
Far	fara	k1gFnPc2	fara
Away	Awaa	k1gFnSc2	Awaa
Eyes	Eyes	k1gInSc1	Eyes
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
přispívá	přispívat	k5eAaImIp3nS	přispívat
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
balad	balada	k1gFnPc2	balada
"	"	kIx"	"
<g/>
Beast	Beast	k1gFnSc1	Beast
of	of	k?	of
Burden	Burdna	k1gFnPc2	Burdna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgNnSc3	tento
albu	album	k1gNnSc3	album
možná	možný	k2eAgFnSc1d1	možná
schází	scházet	k5eAaImIp3nS	scházet
ona	onen	k3xDgFnSc1	onen
pouliční	pouliční	k2eAgFnSc1d1	pouliční
agresivita	agresivita	k1gFnSc1	agresivita
stounovských	stounovský	k2eAgNnPc2d1	stounovský
alb	album	k1gNnPc2	album
ze	z	k7c2	z
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
nebo	nebo	k8xC	nebo
zdrogovaná	zdrogovaný	k2eAgFnSc1d1	zdrogovaná
pochmurnost	pochmurnost	k1gFnSc1	pochmurnost
jejich	jejich	k3xOp3gFnPc2	jejich
desek	deska	k1gFnPc2	deska
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
punc	punc	k1gInSc4	punc
ironického	ironický	k2eAgMnSc2d1	ironický
<g/>
,	,	kIx,	,
dekadentního	dekadentní	k2eAgInSc2d1	dekadentní
hard-rocku	hardock	k1gInSc2	hard-rock
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
dělá	dělat	k5eAaImIp3nS	dělat
výjimečnou	výjimečný	k2eAgFnSc4d1	výjimečná
nahrávku	nahrávka	k1gFnSc4	nahrávka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
skladeb	skladba	k1gFnPc2	skladba
==	==	k?	==
</s>
</p>
<p>
<s>
Autory	autor	k1gMnPc7	autor
jsou	být	k5eAaImIp3nP	být
Mick	Mick	k1gMnSc1	Mick
Jagger	Jagger	k1gMnSc1	Jagger
a	a	k8xC	a
Keith	Keith	k1gMnSc1	Keith
Richards	Richardsa	k1gFnPc2	Richardsa
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Miss	miss	k1gFnSc1	miss
You	You	k1gFnSc1	You
<g/>
"	"	kIx"	"
–	–	k?	–
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
48	[number]	k4	48
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
When	When	k1gMnSc1	When
the	the	k?	the
Whip	Whip	k1gMnSc1	Whip
Comes	Comes	k1gMnSc1	Comes
Down	Down	k1gMnSc1	Down
<g/>
"	"	kIx"	"
–	–	k?	–
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
20	[number]	k4	20
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Just	just	k6eAd1	just
My	my	k3xPp1nPc1	my
Imagination	Imagination	k1gInSc1	Imagination
(	(	kIx(	(
<g/>
Running	Running	k1gInSc1	Running
Away	Awaa	k1gFnSc2	Awaa
with	with	k1gMnSc1	with
Me	Me	k1gMnSc1	Me
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Norman	Norman	k1gMnSc1	Norman
Whitfield	Whitfield	k1gMnSc1	Whitfield
<g/>
,	,	kIx,	,
Barrett	Barrett	k1gMnSc1	Barrett
Strong	Strong	k1gMnSc1	Strong
<g/>
)	)	kIx)	)
–	–	k?	–
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
38	[number]	k4	38
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Some	Some	k1gNnPc1	Some
Girls	girl	k1gFnPc2	girl
<g/>
"	"	kIx"	"
–	–	k?	–
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
37	[number]	k4	37
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Lies	Liesa	k1gFnPc2	Liesa
<g/>
"	"	kIx"	"
–	–	k?	–
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
12	[number]	k4	12
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Far	fara	k1gFnPc2	fara
Away	Awaa	k1gFnSc2	Awaa
Eyes	Eyes	k1gInSc1	Eyes
<g/>
"	"	kIx"	"
–	–	k?	–
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
24	[number]	k4	24
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Respectable	Respectable	k1gFnSc6	Respectable
<g/>
"	"	kIx"	"
–	–	k?	–
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Before	Befor	k1gInSc5	Befor
They	Thea	k1gFnPc4	Thea
Make	Mak	k1gInPc4	Mak
Me	Me	k1gFnSc2	Me
Run	Runa	k1gFnPc2	Runa
<g/>
"	"	kIx"	"
–	–	k?	–
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
25	[number]	k4	25
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Beast	Beast	k1gFnSc1	Beast
of	of	k?	of
Burden	Burdna	k1gFnPc2	Burdna
<g/>
"	"	kIx"	"
–	–	k?	–
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
25	[number]	k4	25
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Shattered	Shattered	k1gInSc4	Shattered
<g/>
"	"	kIx"	"
–	–	k?	–
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
47	[number]	k4	47
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
Mick	Mick	k1gMnSc1	Mick
Jagger	Jagger	k1gMnSc1	Jagger
–	–	k?	–
Zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
Doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
Elektrická	elektrický	k2eAgFnSc1d1	elektrická
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
Piano	piano	k1gNnSc1	piano
</s>
</p>
<p>
<s>
Keith	Keith	k1gInSc1	Keith
Richards	Richards	k1gInSc1	Richards
–	–	k?	–
Elektrická	elektrický	k2eAgFnSc1d1	elektrická
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
Doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
Akustická	akustický	k2eAgFnSc1d1	akustická
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
Baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
Zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
Piano	piano	k1gNnSc1	piano
</s>
</p>
<p>
<s>
Charlie	Charlie	k1gMnSc1	Charlie
Watts	Wattsa	k1gFnPc2	Wattsa
–	–	k?	–
Bicí	bicí	k2eAgFnSc2d1	bicí
</s>
</p>
<p>
<s>
Ron	Ron	k1gMnSc1	Ron
Wood	Wood	k1gMnSc1	Wood
–	–	k?	–
Elektrická	elektrický	k2eAgFnSc1d1	elektrická
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
Doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
Akustická	akustický	k2eAgFnSc1d1	akustická
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
Baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Bill	Bill	k1gMnSc1	Bill
Wyman	Wyman	k1gMnSc1	Wyman
–	–	k?	–
Baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
Syntetizér	Syntetizér	k1gInSc1	Syntetizér
</s>
</p>
<p>
<s>
===	===	k?	===
Doprovod	doprovod	k1gInSc4	doprovod
===	===	k?	===
</s>
</p>
<p>
<s>
Mel	mlít	k5eAaImRp2nS	mlít
Collins	Collins	k1gInSc1	Collins
–	–	k?	–
Saxofon	saxofon	k1gInSc1	saxofon
</s>
</p>
<p>
<s>
Ian	Ian	k?	Ian
McLagan	McLagan	k1gInSc1	McLagan
–	–	k?	–
Varhany	varhany	k1gFnPc1	varhany
<g/>
,	,	kIx,	,
Elektrické	elektrický	k2eAgNnSc1d1	elektrické
piano	piano	k1gNnSc1	piano
</s>
</p>
<p>
<s>
Simon	Simon	k1gMnSc1	Simon
Kirke	Kirk	k1gFnSc2	Kirk
–	–	k?	–
Congas	Congas	k1gMnSc1	Congas
</s>
</p>
<p>
<s>
Sugar	Sugar	k1gMnSc1	Sugar
Blue	Blu	k1gFnSc2	Blu
–	–	k?	–
Harmonika	harmonika	k1gFnSc1	harmonika
</s>
</p>
<p>
<s>
Ian	Ian	k?	Ian
Stewart	Stewart	k1gMnSc1	Stewart
-	-	kIx~	-
Piano	piano	k1gNnSc1	piano
</s>
</p>
<p>
<s>
==	==	k?	==
Žebříčky	žebříček	k1gInPc4	žebříček
==	==	k?	==
</s>
</p>
<p>
<s>
Album	album	k1gNnSc1	album
</s>
</p>
<p>
<s>
Singly	singl	k1gInPc1	singl
</s>
</p>
<p>
<s>
==	==	k?	==
Certifikace	certifikace	k1gFnSc1	certifikace
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Some	Som	k1gFnSc2	Som
Girls	girl	k1gFnPc2	girl
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
