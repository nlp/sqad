<s>
Nakonec	nakonec	k6eAd1	nakonec
finanční	finanční	k2eAgFnPc4d1	finanční
potíže	potíž	k1gFnPc4	potíž
a	a	k8xC	a
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
vedením	vedení	k1gNnSc7	vedení
(	(	kIx(	(
<g/>
společnost	společnost	k1gFnSc4	společnost
Tony	Tony	k1gFnSc2	Tony
Iommiho	Iommi	k1gMnSc2	Iommi
IMA	IMA	kA	IMA
<g/>
)	)	kIx)	)
dovedly	dovést	k5eAaPmAgFnP	dovést
k	k	k7c3	k
odchodu	odchod	k1gInSc3	odchod
Alana	Alan	k1gMnSc2	Alan
Atkinse	Atkins	k1gMnSc2	Atkins
a	a	k8xC	a
bubeníka	bubeník	k1gMnSc2	bubeník
Alana	Alan	k1gMnSc2	Alan
Moora	Moor	k1gMnSc2	Moor
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
Ian	Ian	k1gMnSc1	Ian
Hill	Hill	k1gMnSc1	Hill
chodil	chodit	k5eAaImAgMnS	chodit
s	s	k7c7	s
ženou	žena	k1gFnSc7	žena
z	z	k7c2	z
nedalekého	daleký	k2eNgNnSc2d1	nedaleké
města	město	k1gNnSc2	město
Walsall	Walsalla	k1gFnPc2	Walsalla
<g/>
,	,	kIx,	,
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
svého	svůj	k3xOyFgMnSc4	svůj
bratra	bratr	k1gMnSc4	bratr
Roba	roba	k1gFnSc1	roba
Halforda	Halford	k1gMnSc2	Halford
jako	jako	k8xS	jako
možného	možný	k2eAgMnSc2d1	možný
zpěváka	zpěvák	k1gMnSc2	zpěvák
<g/>
.	.	kIx.	.
</s>

