<s>
Borneo	Borneo	k1gNnSc1	Borneo
(	(	kIx(	(
<g/>
v	v	k7c6	v
Indonésii	Indonésie	k1gFnSc6	Indonésie
Kalimantan	Kalimantan	k1gInSc1	Kalimantan
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgInSc1	třetí
největší	veliký	k2eAgInSc1d3	veliký
ostrov	ostrov	k1gInSc1	ostrov
světa	svět	k1gInSc2	svět
s	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
743	[number]	k4	743
330	[number]	k4	330
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
mezi	mezi	k7c7	mezi
Indickým	indický	k2eAgInSc7d1	indický
oceánem	oceán	k1gInSc7	oceán
a	a	k8xC	a
Tichým	tichý	k2eAgInSc7d1	tichý
oceánem	oceán	k1gInSc7	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Borneu	Borneo	k1gNnSc6	Borneo
se	se	k3xPyFc4	se
rozkládal	rozkládat	k5eAaImAgInS	rozkládat
většinou	většinou	k6eAd1	většinou
deštný	deštný	k2eAgInSc1d1	deštný
prales	prales	k1gInSc1	prales
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejstarším	starý	k2eAgInPc3d3	nejstarší
deštným	deštný	k2eAgInPc3d1	deštný
lesům	les	k1gInPc3	les
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
velice	velice	k6eAd1	velice
ohrožen	ohrozit	k5eAaPmNgInS	ohrozit
rychlou	rychlý	k2eAgFnSc7d1	rychlá
deforestací	deforestace	k1gFnSc7	deforestace
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
domov	domov	k1gInSc1	domov
mnoha	mnoho	k4c3	mnoho
druhů	druh	k1gInPc2	druh
živočichů	živočich	k1gMnPc2	živočich
i	i	k8xC	i
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Bornea	Borneo	k1gNnSc2	Borneo
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Gunung	Gunung	k1gInSc1	Gunung
Kinabalu	Kinabal	k1gInSc2	Kinabal
a	a	k8xC	a
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
4095	[number]	k4	4095
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
<s>
Na	na	k7c6	na
Borneu	Borneo	k1gNnSc6	Borneo
je	být	k5eAaImIp3nS	být
také	také	k9	také
největší	veliký	k2eAgInSc1d3	veliký
jeskynní	jeskynní	k2eAgInSc1d1	jeskynní
sál	sál	k1gInSc1	sál
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
Sarawacká	Sarawacký	k2eAgFnSc1d1	Sarawacký
síň	síň	k1gFnSc1	síň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Politicky	politicky	k6eAd1	politicky
je	být	k5eAaImIp3nS	být
rozdělený	rozdělený	k2eAgInSc1d1	rozdělený
na	na	k7c4	na
Indonésii	Indonésie	k1gFnSc4	Indonésie
<g/>
,	,	kIx,	,
Malajsii	Malajsie	k1gFnSc4	Malajsie
(	(	kIx(	(
<g/>
Sarawak	Sarawak	k1gMnSc1	Sarawak
<g/>
,	,	kIx,	,
Sabah	Sabah	k1gMnSc1	Sabah
<g/>
)	)	kIx)	)
a	a	k8xC	a
Brunej	Brunej	k1gFnSc1	Brunej
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Martapura	Martapur	k1gMnSc2	Martapur
je	být	k5eAaImIp3nS	být
proslulé	proslulý	k2eAgNnSc1d1	proslulé
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
svým	svůj	k3xOyFgInSc7	svůj
trhem	trh	k1gInSc7	trh
s	s	k7c7	s
diamanty	diamant	k1gInPc7	diamant
<g/>
,	,	kIx,	,
drahými	drahý	k2eAgInPc7d1	drahý
kameny	kámen	k1gInPc7	kámen
a	a	k8xC	a
zlatem	zlato	k1gNnSc7	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
ostrova	ostrov	k1gInSc2	ostrov
žijí	žít	k5eAaImIp3nP	žít
domorodí	domorodý	k2eAgMnPc1d1	domorodý
Dajákové	Daják	k1gMnPc1	Daják
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
ještě	ještě	k6eAd1	ještě
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc1	století
obávanými	obávaný	k2eAgMnPc7d1	obávaný
lovci	lovec	k1gMnPc7	lovec
lebek	lebka	k1gFnPc2	lebka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
žijí	žít	k5eAaImIp3nP	žít
muslimští	muslimský	k2eAgMnPc1d1	muslimský
Malajci	Malajec	k1gMnPc1	Malajec
a	a	k8xC	a
také	také	k6eAd1	také
etničtí	etnický	k2eAgMnPc1d1	etnický
Číňané	Číňan	k1gMnPc1	Číňan
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
29	[number]	k4	29
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
Sarawaku	Sarawak	k1gInSc2	Sarawak
a	a	k8xC	a
18	[number]	k4	18
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
Západního	západní	k2eAgInSc2d1	západní
Kalimantanu	Kalimantan	k1gInSc2	Kalimantan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
vládního	vládní	k2eAgInSc2d1	vládní
programu	program	k1gInSc2	program
transmigrace	transmigrace	k1gFnSc2	transmigrace
se	se	k3xPyFc4	se
do	do	k7c2	do
indonéské	indonéský	k2eAgFnSc2d1	Indonéská
části	část	k1gFnSc2	část
Bornea	Borneo	k1gNnSc2	Borneo
přistěhovalo	přistěhovat	k5eAaPmAgNnS	přistěhovat
mnoho	mnoho	k4c1	mnoho
migrantů	migrant	k1gMnPc2	migrant
z	z	k7c2	z
přelidněné	přelidněný	k2eAgFnSc2d1	přelidněná
Jávy	Jáva	k1gFnSc2	Jáva
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
konfliktům	konflikt	k1gInPc3	konflikt
s	s	k7c7	s
Dajáky	Daják	k1gMnPc7	Daják
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
pokrytý	pokrytý	k2eAgMnSc1d1	pokrytý
deštným	deštný	k2eAgInSc7d1	deštný
pralesem	prales	k1gInSc7	prales
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
rostou	růst	k5eAaImIp3nP	růst
endemické	endemický	k2eAgFnPc4d1	endemická
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
zde	zde	k6eAd1	zde
více	hodně	k6eAd2	hodně
než	než	k8xS	než
15	[number]	k4	15
000	[number]	k4	000
druhů	druh	k1gInPc2	druh
vyšších	vysoký	k2eAgFnPc2d2	vyšší
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
z	z	k7c2	z
čeho	co	k3yRnSc2	co
je	být	k5eAaImIp3nS	být
3000	[number]	k4	3000
druhů	druh	k1gInPc2	druh
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
221	[number]	k4	221
suchozemských	suchozemský	k2eAgMnPc2d1	suchozemský
savců	savec	k1gMnPc2	savec
<g/>
.	.	kIx.	.
</s>
<s>
Typickými	typický	k2eAgMnPc7d1	typický
představiteli	představitel	k1gMnPc7	představitel
místní	místní	k2eAgFnSc2d1	místní
fauny	fauna	k1gFnSc2	fauna
jsou	být	k5eAaImIp3nP	být
slon	slon	k1gMnSc1	slon
indický	indický	k2eAgMnSc1d1	indický
bornejský	bornejský	k2eAgMnSc1d1	bornejský
(	(	kIx(	(
<g/>
Elephas	Elephas	k1gInSc1	Elephas
maximus	maximus	k1gMnSc1	maximus
borneensis	borneensis	k1gInSc1	borneensis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nosorožec	nosorožec	k1gMnSc1	nosorožec
sumaterský	sumaterský	k2eAgMnSc1d1	sumaterský
a	a	k8xC	a
orangutan	orangutan	k1gMnSc1	orangutan
bornejský	bornejský	k2eAgMnSc1d1	bornejský
(	(	kIx(	(
<g/>
Pongo	Pongo	k1gMnSc1	Pongo
pygmaeus	pygmaeus	k1gMnSc1	pygmaeus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kahau	kahau	k6eAd1	kahau
nosatý	nosatý	k2eAgInSc1d1	nosatý
(	(	kIx(	(
<g/>
Nasalis	Nasalis	k1gInSc1	Nasalis
larvatus	larvatus	k1gInSc1	larvatus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
levhart	levhart	k1gMnSc1	levhart
Diardův	Diardův	k2eAgMnSc1d1	Diardův
/	/	kIx~	/
pardál	pardál	k1gMnSc1	pardál
ostrovní	ostrovní	k2eAgMnSc1d1	ostrovní
(	(	kIx(	(
<g/>
Neofelis	Neofelis	k1gFnSc1	Neofelis
diardi	diardit	k5eAaPmRp2nS	diardit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
trogon	trogon	k1gInSc4	trogon
šedoprsý	šedoprsý	k2eAgInSc4d1	šedoprsý
(	(	kIx(	(
<g/>
Harpactes	Harpactes	k1gInSc4	Harpactes
whiteheadi	whitehead	k1gMnPc1	whitehead
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
několik	několik	k4yIc1	několik
vzácných	vzácný	k2eAgInPc2d1	vzácný
ptačích	ptačí	k2eAgInPc2d1	ptačí
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
dlouhoocasí	dlouhoocasý	k2eAgMnPc1d1	dlouhoocasý
makaci	makak	k1gMnPc1	makak
a	a	k8xC	a
také	také	k6eAd1	také
četné	četný	k2eAgInPc1d1	četný
druhy	druh	k1gInPc1	druh
motýlů	motýl	k1gMnPc2	motýl
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Borneo	Borneo	k1gNnSc1	Borneo
je	být	k5eAaImIp3nS	být
portugalským	portugalský	k2eAgNnSc7d1	portugalské
zkomolením	zkomolení	k1gNnSc7	zkomolení
názvu	název	k1gInSc2	název
Brum	brum	k1gInSc4	brum
<g/>
.	.	kIx.	.
</s>
<s>
Indonéský	indonéský	k2eAgInSc1d1	indonéský
název	název	k1gInSc1	název
zní	znět	k5eAaImIp3nS	znět
Kalimantan	Kalimantan	k1gInSc4	Kalimantan
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
v	v	k7c6	v
Indonésii	Indonésie	k1gFnSc6	Indonésie
je	být	k5eAaImIp3nS	být
ostrov	ostrov	k1gInSc1	ostrov
označován	označovat	k5eAaImNgInS	označovat
výhradně	výhradně	k6eAd1	výhradně
tímto	tento	k3xDgNnSc7	tento
jménem	jméno	k1gNnSc7	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
označuje	označovat	k5eAaImIp3nS	označovat
pojem	pojem	k1gInSc4	pojem
Kalimantan	Kalimantan	k1gInSc4	Kalimantan
pouze	pouze	k6eAd1	pouze
část	část	k1gFnSc4	část
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
Indonésie	Indonésie	k1gFnSc2	Indonésie
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
největších	veliký	k2eAgInPc2d3	veliký
ostrovů	ostrov	k1gInPc2	ostrov
Palmový	palmový	k2eAgInSc4d1	palmový
olej	olej	k1gInSc4	olej
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Borneo	Borneo	k1gNnSc4	Borneo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Borneo	Borneo	k1gNnSc4	Borneo
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
