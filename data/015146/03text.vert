<s>
Jiří	Jiří	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
</s>
<s>
Jiří	Jiří	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
</s>
<s>
král	král	k1gMnSc1
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
</s>
<s>
Jiří	Jiří	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
</s>
<s>
Doba	doba	k1gFnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1936	#num#	k4
–	–	k?
6	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1952	#num#	k4
</s>
<s>
Korunovace	korunovace	k1gFnSc1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1937	#num#	k4
</s>
<s>
Křest	křest	k1gInSc1
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1896	#num#	k4
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1895	#num#	k4
</s>
<s>
York	York	k1gInSc1
Cottage	Cottag	k1gFnSc2
</s>
<s>
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1952	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
56	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Sandringham	Sandringham	k6eAd1
House	house	k1gNnSc1
<g/>
,	,	kIx,
Norfolk	Norfolk	k1gInSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Pohřben	pohřben	k2eAgInSc1d1
</s>
<s>
kaple	kaple	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Jiří	Jiří	k1gMnSc2
<g/>
,	,	kIx,
Windsorský	windsorský	k2eAgInSc4d1
hrad	hrad	k1gInSc4
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Eduard	Eduard	k1gMnSc1
VIII	VIII	kA
<g/>
.	.	kIx.
</s>
<s>
Nástupce	nástupce	k1gMnSc1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Královna	královna	k1gFnSc1
</s>
<s>
Elizabeth	Elizabeth	k1gFnSc1
Bowes-Lyon	Bowes-Lyona	k1gFnPc2
</s>
<s>
Potomci	potomek	k1gMnPc1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
<g/>
,	,	kIx,
Margaret	Margareta	k1gFnPc2
</s>
<s>
Rod	rod	k1gInSc1
</s>
<s>
Windsorská	windsorský	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
</s>
<s>
Otec	otec	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
V.	V.	kA
</s>
<s>
Matka	matka	k1gFnSc1
</s>
<s>
Marie	Marie	k1gFnSc1
z	z	k7c2
Tecku	Teck	k1gInSc2
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jiří	Jiří	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
George	George	k1gInSc1
VI	VI	kA
<g/>
,	,	kIx,
celým	celý	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Albert	Albert	k1gMnSc1
Frederik	Frederik	k1gMnSc1
Artur	Artur	k1gMnSc1
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
14	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1895	#num#	k4
–	–	k?
6	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1952	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
král	král	k1gMnSc1
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
a	a	k8xC
britských	britský	k2eAgFnPc2d1
dominií	dominie	k1gFnPc2
od	od	k7c2
11	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1936	#num#	k4
až	až	k9
do	do	k7c2
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
posledním	poslední	k2eAgMnSc7d1
císařem	císař	k1gMnSc7
Indie	Indie	k1gFnSc2
(	(	kIx(
<g/>
do	do	k7c2
roku	rok	k1gInSc2
1947	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
posledním	poslední	k2eAgMnSc7d1
králem	král	k1gMnSc7
Irska	Irsko	k1gNnSc2
(	(	kIx(
<g/>
do	do	k7c2
roku	rok	k1gInSc2
1949	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
první	první	k4xOgFnSc4
hlavou	hlavý	k2eAgFnSc4d1
Commonwealthu	Commonwealtha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k8xS,k8xC
druhý	druhý	k4xOgMnSc1
syn	syn	k1gMnSc1
panovníka	panovník	k1gMnSc4
Jiřího	Jiří	k1gMnSc4
V.	V.	kA
nebyl	být	k5eNaImAgMnS
připravován	připravován	k2eAgMnSc1d1
na	na	k7c4
převzetí	převzetí	k1gNnSc4
královských	královský	k2eAgFnPc2d1
povinností	povinnost	k1gFnPc2
a	a	k8xC
v	v	k7c6
mládí	mládí	k1gNnSc6
žil	žít	k5eAaImAgMnS
ve	v	k7c6
stínu	stín	k1gInSc6
svého	svůj	k3xOyFgMnSc2
staršího	starý	k2eAgMnSc2d2
bratra	bratr	k1gMnSc2
Eduarda	Eduard	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
sloužil	sloužit	k5eAaImAgInS
u	u	k7c2
královského	královský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1923	#num#	k4
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Elizabeth	Elizabeth	k1gFnSc7
Bowes-Lyon	Bowes-Lyona	k1gFnPc2
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yIgFnSc7,k3yRgFnSc7,k3yQgFnSc7
měl	mít	k5eAaImAgMnS
dvě	dva	k4xCgFnPc4
dcery	dcera	k1gFnPc4
Alžbětu	Alžběta	k1gFnSc4
a	a	k8xC
Margaret	Margareta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Eduard	Eduard	k1gMnSc1
<g/>
,	,	kIx,
starší	starý	k2eAgMnSc1d2
bratr	bratr	k1gMnSc1
Jiřího	Jiří	k1gMnSc2
<g/>
,	,	kIx,
nastoupil	nastoupit	k5eAaPmAgMnS
po	po	k7c6
smrti	smrt	k1gFnSc6
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
roku	rok	k1gInSc2
1936	#num#	k4
na	na	k7c4
britský	britský	k2eAgInSc4d1
trůn	trůn	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
průběhu	průběh	k1gInSc6
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
projevil	projevit	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
zájem	zájem	k1gInSc4
oženit	oženit	k5eAaPmF
se	se	k3xPyFc4
s	s	k7c7
již	již	k6eAd1
dvakrát	dvakrát	k6eAd1
rozvedenou	rozvedený	k2eAgFnSc7d1
Američankou	Američanka	k1gFnSc7
Wallis	Wallis	k1gFnSc7
Simpsonovou	Simpsonová	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
politických	politický	k2eAgInPc2d1
a	a	k8xC
církevních	církevní	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
mu	on	k3xPp3gMnSc3
bylo	být	k5eAaImAgNnS
britským	britský	k2eAgMnSc7d1
premiérem	premiér	k1gMnSc7
Stanleyem	Stanley	k1gMnSc7
Baldwinem	Baldwin	k1gInSc7
sděleno	sdělen	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
nemůže	moct	k5eNaImIp3nS
se	s	k7c7
Simpsonovou	Simpsonová	k1gFnSc7
oženit	oženit	k5eAaPmF
a	a	k8xC
zůstat	zůstat	k5eAaPmF
britským	britský	k2eAgMnSc7d1
králem	král	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
se	se	k3xPyFc4
mohl	moct	k5eAaImAgMnS
se	s	k7c7
Simpsonovou	Simpsonová	k1gFnSc7
oženit	oženit	k5eAaPmF
<g/>
,	,	kIx,
Eduard	Eduard	k1gMnSc1
abdikoval	abdikovat	k5eAaBmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
se	se	k3xPyFc4
pak	pak	k6eAd1
stal	stát	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc7
nástupcem	nástupce	k1gMnSc7
jako	jako	k8xC,k8xS
třetí	třetí	k4xOgMnSc1
panovník	panovník	k1gMnSc1
z	z	k7c2
windsorské	windsorský	k2eAgFnSc2d1
dynastie	dynastie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
průběhu	průběh	k1gInSc6
jednoho	jeden	k4xCgInSc2
dne	den	k1gInSc2
po	po	k7c6
jeho	jeho	k3xOp3gInSc6
nástupu	nástup	k1gInSc6
přijal	přijmout	k5eAaPmAgInS
irský	irský	k2eAgInSc1d1
parlament	parlament	k1gInSc1
zákon	zákon	k1gInSc1
<g/>
,	,	kIx,
kterým	který	k3yQgInSc7,k3yRgInSc7,k3yIgInSc7
zrušil	zrušit	k5eAaPmAgMnS
moc	moc	k6eAd1
monarchie	monarchie	k1gFnSc2
v	v	k7c6
Irsku	Irsko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
po	po	k7c6
nástupu	nástup	k1gInSc6
vstoupila	vstoupit	k5eAaPmAgFnS
království	království	k1gNnSc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc1
byl	být	k5eAaImAgInS
panovníkem	panovník	k1gMnSc7
<g/>
,	,	kIx,
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
Irska	Irsko	k1gNnSc2
<g/>
,	,	kIx,
do	do	k7c2
válečného	válečný	k2eAgInSc2d1
stavu	stav	k1gInSc2
s	s	k7c7
nacistickým	nacistický	k2eAgNnSc7d1
Německem	Německo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následujících	následující	k2eAgNnPc6d1
dvou	dva	k4xCgNnPc6
letech	léto	k1gNnPc6
byla	být	k5eAaImAgFnS
zahájena	zahájen	k2eAgFnSc1d1
válka	válka	k1gFnSc1
i	i	k9
s	s	k7c7
Itálií	Itálie	k1gFnSc7
a	a	k8xC
Japonskem	Japonsko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
Británie	Británie	k1gFnSc1
a	a	k8xC
její	její	k3xOp3gMnPc1
spojenci	spojenec	k1gMnPc1
dosáhli	dosáhnout	k5eAaPmAgMnP
v	v	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
vítězství	vítězství	k1gNnSc2
<g/>
,	,	kIx,
hlavními	hlavní	k2eAgFnPc7d1
světovými	světový	k2eAgFnPc7d1
mocnostmi	mocnost	k1gFnPc7
se	se	k3xPyFc4
staly	stát	k5eAaPmAgInP
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
a	a	k8xC
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
a	a	k8xC
význam	význam	k1gInSc1
Britského	britský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
poklesl	poklesnout	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyhlášení	vyhlášení	k1gNnSc1
nezávislosti	nezávislost	k1gFnSc2
Indie	Indie	k1gFnSc2
a	a	k8xC
Pákistánu	Pákistán	k1gInSc2
roku	rok	k1gInSc2
1947	#num#	k4
a	a	k8xC
vznik	vznik	k1gInSc1
Irské	irský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1949	#num#	k4
zahájil	zahájit	k5eAaPmAgInS
rozpad	rozpad	k1gInSc1
impéria	impérium	k1gNnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
transformaci	transformace	k1gFnSc4
do	do	k7c2
Commonwealthu	Commonwealth	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Mládí	mládí	k1gNnSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	s	k7c7
14	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1895	#num#	k4
jako	jako	k8xS,k8xC
druhý	druhý	k4xOgMnSc1
syn	syn	k1gMnSc1
waleského	waleský	k2eAgMnSc2d1
prince	princ	k1gMnSc2
Jiřího	Jiří	k1gMnSc2
(	(	kIx(
<g/>
později	pozdě	k6eAd2
krále	král	k1gMnSc2
Jiřího	Jiří	k1gMnSc2
V.	V.	kA
<g/>
)	)	kIx)
a	a	k8xC
jeho	jeho	k3xOp3gFnPc1
manželky	manželka	k1gFnPc1
Marie	Maria	k1gFnSc2
z	z	k7c2
Tecku	Teck	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
datum	datum	k1gNnSc1
narození	narození	k1gNnSc2
se	se	k3xPyFc4
shodovalo	shodovat	k5eAaImAgNnS
s	s	k7c7
výročím	výročí	k1gNnSc7
úmrtí	úmrtí	k1gNnSc2
jeho	jeho	k3xOp3gMnSc2,k3xPp3gMnSc2
pradědečka	pradědeček	k1gMnSc2
a	a	k8xC
manžela	manžel	k1gMnSc2
královny	královna	k1gFnSc2
Viktorie	Viktoria	k1gFnSc2
<g/>
,	,	kIx,
prince	princ	k1gMnSc2
Alberta	Albert	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jeho	jeho	k3xOp3gFnSc6
počest	počest	k1gFnSc6
dostal	dostat	k5eAaPmAgMnS
jméno	jméno	k1gNnSc4
Albert	Albert	k1gMnSc1
(	(	kIx(
<g/>
byl	být	k5eAaImAgMnS
pokřtěn	pokřtěn	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
Albert	Albert	k1gMnSc1
Frederik	Frederik	k1gMnSc1
Artur	Artur	k1gMnSc1
Jiří	Jiří	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
„	„	k?
<g/>
Bertie	Bertie	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
jak	jak	k6eAd1
byl	být	k5eAaImAgInS
oslovován	oslovován	k2eAgInSc1d1
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
v	v	k7c6
době	doba	k1gFnSc6
svého	svůj	k3xOyFgNnSc2
narození	narození	k1gNnSc2
čtvrtý	čtvrtý	k4xOgMnSc1
v	v	k7c6
pořadí	pořadí	k1gNnSc6
nástupnictví	nástupnictví	k1gNnSc2
na	na	k7c4
britský	britský	k2eAgInSc4d1
trůn	trůn	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Byl	být	k5eAaImAgMnS
často	často	k6eAd1
nemocen	nemocen	k2eAgMnSc1d1
a	a	k8xC
popisován	popisován	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
„	„	k?
<g/>
lekavý	lekavý	k2eAgInSc1d1
a	a	k8xC
náchylný	náchylný	k2eAgInSc1d1
k	k	k7c3
pláči	pláč	k1gInSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnPc1
rodiče	rodič	k1gMnPc1
se	se	k3xPyFc4
nevěnovali	věnovat	k5eNaImAgMnP
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k9
tomu	ten	k3xDgNnSc3
bylo	být	k5eAaImAgNnS
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
šlechtických	šlechtický	k2eAgFnPc6d1
rodinách	rodina	k1gFnPc6
<g/>
,	,	kIx,
každodenní	každodenní	k2eAgFnSc3d1
výchově	výchova	k1gFnSc3
svých	svůj	k3xOyFgFnPc2
dětí	dítě	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
nucen	nutit	k5eAaImNgMnS
psát	psát	k5eAaImF
pravou	pravá	k1gFnSc4
rukou	ruka	k1gFnPc2
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
byl	být	k5eAaImAgMnS
levák	levák	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
objevilo	objevit	k5eAaPmAgNnS
se	se	k3xPyFc4
u	u	k7c2
něho	on	k3xPp3gNnSc2
koktání	koktání	k1gNnSc2
<g/>
,	,	kIx,
se	s	k7c7
kterým	který	k3yIgMnSc7,k3yQgMnSc7,k3yRgMnSc7
se	se	k3xPyFc4
potýkal	potýkat	k5eAaImAgMnS
až	až	k9
do	do	k7c2
dospělosti	dospělost	k1gFnSc2
–	–	k?
patrně	patrně	k6eAd1
šlo	jít	k5eAaImAgNnS
o	o	k7c4
důsledek	důsledek	k1gInSc4
potlačeného	potlačený	k2eAgNnSc2d1
leváctví	leváctví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Vojenská	vojenský	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1909	#num#	k4
Albert	Albert	k1gMnSc1
navštěvoval	navštěvovat	k5eAaImAgMnS
královskou	královský	k2eAgFnSc4d1
námořnickou	námořnický	k2eAgFnSc4d1
školu	škola	k1gFnSc4
v	v	k7c6
Osborne	Osborn	k1gInSc5
jako	jako	k9
kadet	kadet	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1911	#num#	k4
pokračoval	pokračovat	k5eAaImAgInS
ve	v	k7c6
studiu	studio	k1gNnSc6
na	na	k7c6
škole	škola	k1gFnSc6
královského	královský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
v	v	k7c6
Dartmouthu	Dartmouth	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
Eduard	Eduard	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
roku	rok	k1gInSc2
1910	#num#	k4
zemřel	zemřít	k5eAaPmAgMnS
<g/>
,	,	kIx,
nastoupil	nastoupit	k5eAaPmAgMnS
na	na	k7c4
britský	britský	k2eAgInSc4d1
trůn	trůn	k1gInSc4
Albertův	Albertův	k2eAgMnSc1d1
otec	otec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Albert	Albert	k1gMnSc1
nastoupil	nastoupit	k5eAaPmAgMnS
15	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1913	#num#	k4
do	do	k7c2
služby	služba	k1gFnSc2
jako	jako	k8xC,k8xS
lodní	lodní	k2eAgMnSc1d1
kadet	kadet	k1gMnSc1
a	a	k8xC
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
se	se	k3xPyFc4
zapojil	zapojit	k5eAaPmAgMnS
do	do	k7c2
bojů	boj	k1gInPc2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
lodi	loď	k1gFnSc6
HMS	HMS	kA
Collingwood	Collingwood	k1gInSc1
se	se	k3xPyFc4
účastnil	účastnit	k5eAaImAgInS
námořní	námořní	k2eAgFnPc4d1
bitvy	bitva	k1gFnPc4
u	u	k7c2
Jutska	Jutsko	k1gNnSc2
proti	proti	k7c3
německému	německý	k2eAgNnSc3d1
námořnictvu	námořnictvo	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
dalších	další	k2eAgFnPc2d1
akcí	akce	k1gFnPc2
se	se	k3xPyFc4
z	z	k7c2
důvodu	důvod	k1gInSc2
zdravotních	zdravotní	k2eAgFnPc2d1
potíží	potíž	k1gFnPc2
nezapojil	zapojit	k5eNaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
únoru	únor	k1gInSc6
1918	#num#	k4
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
důstojníkem	důstojník	k1gMnSc7
u	u	k7c2
výcvikové	výcvikový	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
královského	královský	k2eAgNnSc2d1
námořního	námořní	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vytvoření	vytvoření	k1gNnSc6
královského	královský	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
byl	být	k5eAaImAgInS
přeložen	přeložit	k5eAaPmNgInS
k	k	k7c3
jeho	jeho	k3xOp3gFnPc3
jednotkám	jednotka	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
říjnu	říjen	k1gInSc6
1919	#num#	k4
začal	začít	k5eAaPmAgInS
na	na	k7c4
Trinity	Trinit	k2eAgFnPc4d1
College	Colleg	k1gFnPc4
v	v	k7c4
Cambridge	Cambridge	k1gFnPc4
studovat	studovat	k5eAaImF
dějiny	dějiny	k1gFnPc4
<g/>
,	,	kIx,
ekonomiku	ekonomika	k1gFnSc4
a	a	k8xC
občanskou	občanský	k2eAgFnSc4d1
nauku	nauka	k1gFnSc4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1920	#num#	k4
mu	on	k3xPp3gMnSc3
byl	být	k5eAaImAgInS
udělen	udělen	k2eAgInSc4d1
titul	titul	k1gInSc4
vévody	vévoda	k1gMnSc2
z	z	k7c2
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
začal	začít	k5eAaPmAgInS
plnit	plnit	k5eAaImF
královské	královský	k2eAgFnPc4d1
povinnosti	povinnost	k1gFnPc4
<g/>
,	,	kIx,
zastupovat	zastupovat	k5eAaImF
svého	svůj	k3xOyFgMnSc4
otce	otec	k1gMnSc4
a	a	k8xC
podnikal	podnikat	k5eAaImAgMnS
cesty	cesta	k1gFnPc4
po	po	k7c6
dolech	dol	k1gInPc6
a	a	k8xC
továrnách	továrna	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Manželství	manželství	k1gNnSc1
</s>
<s>
manželka	manželka	k1gFnSc1
Jiřího	Jiří	k1gMnSc2
VI	VI	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Alžběta	Alžběta	k1gFnSc1
Bowes-Lyonová	Bowes-Lyonová	k1gFnSc1
</s>
<s>
Roku	rok	k1gInSc2
1920	#num#	k4
se	se	k3xPyFc4
Jiří	Jiří	k1gMnSc1
setkal	setkat	k5eAaPmAgMnS
s	s	k7c7
Alžbětou	Alžběta	k1gFnSc7
Bowes-Lyonovou	Bowes-Lyonová	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
byla	být	k5eAaImAgFnS
potomkem	potomek	k1gMnSc7
skotského	skotský	k2eAgInSc2d1
krále	král	k1gMnSc4
Roberta	Robert	k1gMnSc2
I.	I.	kA
a	a	k8xC
Jindřicha	Jindřich	k1gMnSc2
VII	VII	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
z	z	k7c2
hlediska	hledisko	k1gNnSc2
britského	britský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
nepocházela	pocházet	k5eNaImAgFnS
z	z	k7c2
panovnického	panovnický	k2eAgInSc2d1
rodu	rod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiřího	Jiří	k1gMnSc2
žádost	žádost	k1gFnSc4
o	o	k7c4
ruku	ruka	k1gFnSc4
dvakrát	dvakrát	k6eAd1
odmítla	odmítnout	k5eAaPmAgFnS
a	a	k8xC
ke	k	k7c3
sňatku	sňatek	k1gInSc3
svolila	svolit	k5eAaPmAgFnS
až	až	k9
po	po	k7c6
dvou	dva	k4xCgNnPc6
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1
svatba	svatba	k1gFnSc1
se	se	k3xPyFc4
konala	konat	k5eAaImAgFnS
26	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1923	#num#	k4
ve	v	k7c6
Westminsterském	Westminsterský	k2eAgNnSc6d1
opatství	opatství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sňatek	sňatek	k1gInSc1
s	s	k7c7
ženou	žena	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
nepocházela	pocházet	k5eNaImAgFnS
z	z	k7c2
panovnického	panovnický	k2eAgInSc2d1
rodu	rod	k1gInSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
veřejností	veřejnost	k1gFnSc7
chápán	chápat	k5eAaImNgMnS
jako	jako	k8xS,k8xC
gesto	gesto	k1gNnSc4
modernizace	modernizace	k1gFnSc2
královské	královský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
manželství	manželství	k1gNnSc2
se	se	k3xPyFc4
narodily	narodit	k5eAaPmAgFnP
dvě	dva	k4xCgFnPc1
dcery	dcera	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
*	*	kIx~
21	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1926	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
nastoupila	nastoupit	k5eAaPmAgFnS
po	po	k7c6
svém	svůj	k3xOyFgNnSc6
otci	otec	k1gMnSc3
na	na	k7c4
britský	britský	k2eAgInSc4d1
trůn	trůn	k1gInSc4
jako	jako	k8xS,k8xC
královna	královna	k1gFnSc1
Alžběta	Alžběta	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
provdána	provdán	k2eAgFnSc1d1
<g/>
,	,	kIx,
4	#num#	k4
děti	dítě	k1gFnPc4
</s>
<s>
Markéta	Markéta	k1gFnSc1
(	(	kIx(
<g/>
21	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1930	#num#	k4
–	–	k?
9	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2002	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
provdána	provdán	k2eAgFnSc1d1
<g/>
,	,	kIx,
2	#num#	k4
děti	dítě	k1gFnPc4
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1
rodina	rodina	k1gFnSc1
vedla	vést	k5eAaImAgFnS
relativně	relativně	k6eAd1
klidný	klidný	k2eAgInSc4d1
a	a	k8xC
neokázalý	okázalý	k2eNgInSc4d1
život	život	k1gInSc4
v	v	k7c6
domě	dům	k1gInSc6
na	na	k7c4
Piccadilly	Piccadilla	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednou	jednou	k6eAd1
z	z	k7c2
výjimek	výjimka	k1gFnPc2
byl	být	k5eAaImAgInS
roku	rok	k1gInSc2
1931	#num#	k4
návrh	návrh	k1gInSc1
kanadského	kanadský	k2eAgMnSc2d1
premiéra	premiér	k1gMnSc2
Benneta	Bennet	k1gMnSc2
jmenovat	jmenovat	k5eAaImF,k5eAaBmF
jej	on	k3xPp3gMnSc4
generálním	generální	k2eAgMnSc7d1
guvernérem	guvernér	k1gMnSc7
Kanady	Kanada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
po	po	k7c6
konzultaci	konzultace	k1gFnSc6
s	s	k7c7
vládou	vláda	k1gFnSc7
odmítnuto	odmítnout	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
pomohla	pomoct	k5eAaPmAgFnS
svému	svůj	k3xOyFgMnSc3
manželovi	manžel	k1gMnSc3
překonat	překonat	k5eAaPmF
strach	strach	k1gInSc4
z	z	k7c2
veřejných	veřejný	k2eAgNnPc2d1
vystoupení	vystoupení	k1gNnPc2
(	(	kIx(
<g/>
způsobený	způsobený	k2eAgInSc1d1
koktáním	koktání	k1gNnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zprostředkovala	zprostředkovat	k5eAaPmAgFnS
mu	on	k3xPp3gNnSc3
setkání	setkání	k1gNnSc3
s	s	k7c7
Lionelem	Lionel	k1gMnSc7
Loguem	Logu	k1gMnSc7
<g/>
,	,	kIx,
expertem	expert	k1gMnSc7
na	na	k7c4
jazykový	jazykový	k2eAgInSc4d1
projev	projev	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
s	s	k7c7
ním	on	k3xPp3gMnSc7
prováděl	provádět	k5eAaImAgInS
dechová	dechový	k2eAgNnPc4d1
cvičení	cvičení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
bylo	být	k5eAaImAgNnS
úspěšné	úspěšný	k2eAgNnSc1d1
Jiřího	Jiří	k1gMnSc4
vystoupení	vystoupení	k1gNnSc2
na	na	k7c6
zahájení	zahájení	k1gNnSc6
jednání	jednání	k1gNnSc4
australského	australský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
mu	on	k3xPp3gMnSc3
dodalo	dodat	k5eAaPmAgNnS
sebevědomí	sebevědomí	k1gNnSc4
a	a	k8xC
poté	poté	k6eAd1
byl	být	k5eAaImAgInS
již	již	k6eAd1
schopen	schopen	k2eAgMnSc1d1
absolvovat	absolvovat	k5eAaPmF
veřejná	veřejný	k2eAgNnPc4d1
vystoupení	vystoupení	k1gNnPc4
jen	jen	k6eAd1
s	s	k7c7
malými	malý	k2eAgFnPc7d1
obavami	obava	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Vláda	vláda	k1gFnSc1
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
<g/>
,	,	kIx,
král	král	k1gMnSc1
Jiří	Jiří	k1gMnSc1
<g/>
,	,	kIx,
zemřel	zemřít	k5eAaPmAgMnS
20	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1936	#num#	k4
a	a	k8xC
jeho	jeho	k3xOp3gMnSc7
nástupcem	nástupce	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Albertův	Albertův	k2eAgMnSc1d1
starší	starý	k2eAgMnSc1d2
bratr	bratr	k1gMnSc1
Eduard	Eduard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
11	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
Eduard	Eduard	k1gMnSc1
abdikoval	abdikovat	k5eAaBmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
mohl	moct	k5eAaImAgInS
oženit	oženit	k5eAaPmF
s	s	k7c7
již	již	k9
dvakrát	dvakrát	k6eAd1
rozvedenou	rozvedený	k2eAgFnSc7d1
Američankou	Američanka	k1gFnSc7
Wallis	Wallis	k1gFnSc7
Simpsonovou	Simpsonová	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
pro	pro	k7c4
roli	role	k1gFnSc4
královny	královna	k1gFnSc2
pro	pro	k7c4
britské	britský	k2eAgInPc4d1
zákony	zákon	k1gInPc4
i	i	k9
pro	pro	k7c4
britskou	britský	k2eAgFnSc4d1
veřejnost	veřejnost	k1gFnSc4
nepřijatelná	přijatelný	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
se	se	k3xPyFc4
Albert	Albert	k1gMnSc1
stal	stát	k5eAaPmAgMnS
králem	král	k1gMnSc7
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
se	se	k3xPyFc4
tuto	tento	k3xDgFnSc4
úlohu	úloha	k1gFnSc4
zdráhal	zdráhat	k5eAaImAgInS
přijmout	přijmout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Jiří	Jiří	k1gMnSc1
<g/>
,	,	kIx,
Eleanor	Eleanor	k1gMnSc1
Rooseveltová	Rooseveltová	k1gFnSc1
a	a	k8xC
Jiřího	Jiří	k1gMnSc2
manželka	manželka	k1gFnSc1
Alžběta	Alžběta	k1gFnSc1
</s>
<s>
Albert	Albert	k1gMnSc1
přijal	přijmout	k5eAaPmAgMnS
po	po	k7c6
nástupu	nástup	k1gInSc6
na	na	k7c4
trůn	trůn	k1gInSc4
jméno	jméno	k1gNnSc4
Jiří	Jiří	k1gMnSc2
VI	VI	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
tak	tak	k6eAd1
vyjádřil	vyjádřit	k5eAaPmAgMnS
kontinuitu	kontinuita	k1gFnSc4
s	s	k7c7
vládou	vláda	k1gFnSc7
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
a	a	k8xC
posílil	posílit	k5eAaPmAgInS
důvěru	důvěra	k1gFnSc4
v	v	k7c6
monarchii	monarchie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc3
prvním	první	k4xOgInSc7
počinem	počin	k1gInSc7
v	v	k7c6
novém	nový	k2eAgNnSc6d1
královském	královský	k2eAgNnSc6d1
postavení	postavení	k1gNnSc6
bylo	být	k5eAaImAgNnS
udělení	udělení	k1gNnSc1
titulu	titul	k1gInSc2
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Windsoru	Windsor	k1gInSc2
bratru	bratr	k1gMnSc3
Eduardovi	Eduard	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
svou	svůj	k3xOyFgFnSc7
abdikací	abdikace	k1gFnSc7
ztratil	ztratit	k5eAaPmAgMnS
právo	právo	k1gNnSc4
nosit	nosit	k5eAaImF
královský	královský	k2eAgInSc4d1
titul	titul	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právo	právo	k1gNnSc1
užívat	užívat	k5eAaImF
tento	tento	k3xDgInSc4
titul	titul	k1gInSc4
bylo	být	k5eAaImAgNnS
uděleno	udělit	k5eAaPmNgNnS
pouze	pouze	k6eAd1
Eduardovi	Eduardův	k2eAgMnPc1d1
a	a	k8xC
netýkalo	týkat	k5eNaImAgNnS
se	se	k3xPyFc4
jeho	jeho	k3xOp3gFnSc2
manželky	manželka	k1gFnSc2
ani	ani	k8xC
případných	případný	k2eAgMnPc2d1
potomků	potomek	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
také	také	k9
musel	muset	k5eAaImAgMnS
od	od	k7c2
Eduarda	Eduard	k1gMnSc2
odkoupit	odkoupit	k5eAaPmF
Balmoralský	Balmoralský	k2eAgInSc4d1
hrad	hrad	k1gInSc4
a	a	k8xC
Sandringham	Sandringham	k1gInSc4
House	house	k1gNnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
šlo	jít	k5eAaImAgNnS
o	o	k7c4
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
soukromý	soukromý	k2eAgInSc4d1
majetek	majetek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1
korunovace	korunovace	k1gFnSc1
se	se	k3xPyFc4
konala	konat	k5eAaImAgFnS
12	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1937	#num#	k4
<g/>
,	,	kIx,
v	v	k7c4
den	den	k1gInSc4
<g/>
,	,	kIx,
na	na	k7c4
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
byla	být	k5eAaImAgFnS
plánována	plánován	k2eAgFnSc1d1
Eduardova	Eduardův	k2eAgFnSc1d1
korunovace	korunovace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rozporu	rozpor	k1gInSc6
s	s	k7c7
tradicí	tradice	k1gFnSc7
se	se	k3xPyFc4
korunovačního	korunovační	k2eAgInSc2d1
obřadu	obřad	k1gInSc2
účastnila	účastnit	k5eAaImAgFnS
i	i	k9
jeho	jeho	k3xOp3gFnSc1
matka	matka	k1gFnSc1
Marie	Maria	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mu	on	k3xPp3gMnSc3
tak	tak	k9
vyjádřila	vyjádřit	k5eAaPmAgFnS
podporu	podpora	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Král	Král	k1gMnSc1
byl	být	k5eAaImAgMnS
ústavně	ústavně	k6eAd1
zavázán	zavázat	k5eAaPmNgMnS
podporovat	podporovat	k5eAaImF
premiéra	premiér	k1gMnSc2
Nevilla	Nevill	k1gMnSc2
Chamberlaina	Chamberlain	k1gMnSc2
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
snaze	snaha	k1gFnSc6
o	o	k7c4
appeasement	appeasement	k1gInSc4
s	s	k7c7
Adolfem	Adolf	k1gMnSc7
Hitlerem	Hitler	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
král	král	k1gMnSc1
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
ženou	žena	k1gFnSc7
vítali	vítat	k5eAaImAgMnP
Chamberlaina	Chamberlaina	k1gMnSc1
po	po	k7c6
jeho	jeho	k3xOp3gInSc6
návratu	návrat	k1gInSc6
po	po	k7c6
podpisu	podpis	k1gInSc6
Mnichovské	mnichovský	k2eAgFnSc2d1
dohody	dohoda	k1gFnSc2
<g/>
,	,	kIx,
pozvali	pozvat	k5eAaPmAgMnP
ho	on	k3xPp3gMnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
s	s	k7c7
nimi	on	k3xPp3gMnPc7
ukázal	ukázat	k5eAaPmAgMnS
na	na	k7c6
balkónu	balkón	k1gInSc6
Buckinghamského	buckinghamský	k2eAgInSc2d1
paláce	palác	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veřejné	veřejný	k2eAgFnPc4d1
vystoupení	vystoupení	k1gNnSc6
krále	král	k1gMnSc4
a	a	k8xC
politika	politik	k1gMnSc4
bylo	být	k5eAaImAgNnS
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
nevídané	vídaný	k2eNgFnSc2d1
–	–	k?
prostor	prostor	k1gInSc4
na	na	k7c6
balkónu	balkón	k1gInSc6
byl	být	k5eAaImAgInS
vyhrazen	vyhradit	k5eAaPmNgInS
jen	jen	k9
členům	člen	k1gMnPc3
královské	královský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1939	#num#	k4
podnikl	podniknout	k5eAaPmAgInS
královský	královský	k2eAgInSc1d1
pár	pár	k1gInSc1
cestu	cesta	k1gFnSc4
do	do	k7c2
Kanady	Kanada	k1gFnSc2
s	s	k7c7
krátkou	krátký	k2eAgFnSc7d1
zastávkou	zastávka	k1gFnSc7
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Ottawy	Ottawa	k1gFnSc2
byli	být	k5eAaImAgMnP
doprovázeni	doprovázet	k5eAaImNgMnP
kanadským	kanadský	k2eAgMnSc7d1
premiérem	premiér	k1gMnSc7
<g/>
,	,	kIx,
nikoli	nikoli	k9
britskými	britský	k2eAgMnPc7d1
ministry	ministr	k1gMnPc7
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
znamenalo	znamenat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
jak	jak	k6eAd1
v	v	k7c6
Kanadě	Kanada	k1gFnSc6
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k9
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
vystupovali	vystupovat	k5eAaImAgMnP
jako	jako	k9
král	král	k1gMnSc1
a	a	k8xC
královna	královna	k1gFnSc1
Kanady	Kanada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kanadský	kanadský	k2eAgMnSc1d1
předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
King	King	k1gInSc1
tím	ten	k3xDgNnSc7
chtěl	chtít	k5eAaImAgInS
reálně	reálně	k6eAd1
demonstrovat	demonstrovat	k5eAaBmF
principy	princip	k1gInPc1
definované	definovaný	k2eAgInPc1d1
v	v	k7c6
Westminsterském	Westminsterský	k2eAgInSc6d1
statutu	statut	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
určoval	určovat	k5eAaImAgInS
samosprávu	samospráva	k1gFnSc4
britských	britský	k2eAgFnPc2d1
dominií	dominie	k1gFnPc2
a	a	k8xC
přiznával	přiznávat	k5eAaImAgMnS
každému	každý	k3xTgMnSc3
dominiu	dominion	k1gNnSc3
odděleného	oddělený	k2eAgMnSc2d1
panovníka	panovník	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Záměrem	záměr	k1gInSc7
cesty	cesta	k1gFnSc2
do	do	k7c2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
bylo	být	k5eAaImAgNnS
prolomení	prolomení	k1gNnSc1
izolacionismu	izolacionismus	k1gInSc2
americké	americký	k2eAgFnSc2d1
veřejnosti	veřejnost	k1gFnSc2
s	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c4
hrozbu	hrozba	k1gFnSc4
války	válka	k1gFnSc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
cílem	cíl	k1gInSc7
cesty	cesta	k1gFnSc2
bylo	být	k5eAaImAgNnS
získání	získání	k1gNnSc1
politické	politický	k2eAgFnSc2d1
podpory	podpora	k1gFnSc2
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
v	v	k7c6
případné	případný	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
král	král	k1gMnSc1
s	s	k7c7
manželkou	manželka	k1gFnSc7
vřele	vřele	k6eAd1
vítání	vítání	k1gNnSc2
i	i	k8xC
kanadskou	kanadský	k2eAgFnSc7d1
a	a	k8xC
americkou	americký	k2eAgFnSc7d1
veřejností	veřejnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navštívili	navštívit	k5eAaPmAgMnP
i	i	k9
Světovou	světový	k2eAgFnSc4d1
výstavu	výstava	k1gFnSc4
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
a	a	k8xC
strávili	strávit	k5eAaPmAgMnP
nějaký	nějaký	k3yIgInSc4
čas	čas	k1gInSc4
s	s	k7c7
prezidentem	prezident	k1gMnSc7
Rooseveltem	Roosevelt	k1gMnSc7
v	v	k7c6
Bílém	bílý	k2eAgInSc6d1
domě	dům	k1gInSc6
i	i	k8xC
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
soukromé	soukromý	k2eAgFnSc6d1
rezidenci	rezidence	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
vypuknutí	vypuknutí	k1gNnSc6
války	válka	k1gFnSc2
roku	rok	k1gInSc2
1939	#num#	k4
se	se	k3xPyFc4
Jiří	Jiří	k1gMnPc1
VI	VI	kA
<g/>
.	.	kIx.
i	i	k8xC
jeho	jeho	k3xOp3gFnSc1
rodina	rodina	k1gFnSc1
rozhodli	rozhodnout	k5eAaPmAgMnP
zůstat	zůstat	k5eAaPmF
v	v	k7c6
Londýně	Londýn	k1gInSc6
místo	místo	k7c2
plánovaného	plánovaný	k2eAgInSc2d1
odletu	odlet	k1gInSc2
do	do	k7c2
Kanady	Kanada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Král	Král	k1gMnSc1
i	i	k9
jeho	jeho	k3xOp3gFnSc1
manželka	manželka	k1gFnSc1
oficiálně	oficiálně	k6eAd1
zůstávali	zůstávat	k5eAaImAgMnP
v	v	k7c6
Buckinghamském	buckinghamský	k2eAgInSc6d1
paláci	palác	k1gInSc6
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
z	z	k7c2
bezpečnostních	bezpečnostní	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
odjížděli	odjíždět	k5eAaImAgMnP
na	na	k7c4
noc	noc	k1gFnSc4
na	na	k7c4
Windsorský	windsorský	k2eAgInSc4d1
hrad	hrad	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
i	i	k9
s	s	k7c7
manželkou	manželka	k1gFnSc7
jen	jen	k9
těsně	těsně	k6eAd1
unikli	uniknout	k5eAaPmAgMnP
smrti	smrt	k1gFnSc3
<g/>
,	,	kIx,
když	když	k8xS
dvě	dva	k4xCgFnPc1
německé	německý	k2eAgFnPc1d1
letecké	letecký	k2eAgFnPc1d1
pumy	puma	k1gFnPc1
vybuchly	vybuchnout	k5eAaPmAgFnP
na	na	k7c6
dvoře	dvůr	k1gInSc6
Buckinghamského	buckinghamský	k2eAgInSc2d1
paláce	palác	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
války	válka	k1gFnSc2
král	král	k1gMnSc1
i	i	k8xC
královna	královna	k1gFnSc1
poskytovali	poskytovat	k5eAaImAgMnP
obyvatelstvu	obyvatelstvo	k1gNnSc3
morální	morální	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
při	při	k7c6
návštěvách	návštěva	k1gFnPc6
vybombardovaných	vybombardovaný	k2eAgNnPc2d1
míst	místo	k1gNnPc2
a	a	k8xC
muničních	muniční	k2eAgFnPc2d1
továren	továrna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
jako	jako	k8xC,k8xS
ostatní	ostatní	k2eAgMnPc1d1
obyvatelé	obyvatel	k1gMnPc1
podléhala	podléhat	k5eAaImAgFnS
i	i	k9
královská	královský	k2eAgFnSc1d1
rodina	rodina	k1gFnSc1
přídělovému	přídělový	k2eAgInSc3d1
systému	systém	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
oslavách	oslava	k1gFnPc6
konce	konec	k1gInSc2
války	válka	k1gFnSc2
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
u	u	k7c2
Chamberlaina	Chamberlaino	k1gNnSc2
král	král	k1gMnSc1
a	a	k8xC
královna	královna	k1gFnSc1
pozvali	pozvat	k5eAaPmAgMnP
na	na	k7c6
vystoupení	vystoupení	k1gNnSc6
na	na	k7c6
balkónu	balkón	k1gInSc6
Buckinghamského	buckinghamský	k2eAgInSc2d1
paláce	palác	k1gInSc2
premiéra	premiér	k1gMnSc2
Winstona	Winston	k1gMnSc2
Churchilla	Churchill	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Commonwealth	Commonwealth	k1gMnSc1
</s>
<s>
Král	Král	k1gMnSc1
Jiří	Jiří	k1gMnSc1
a	a	k8xC
kanadský	kanadský	k2eAgMnSc1d1
premiér	premiér	k1gMnSc1
William	William	k1gInSc4
Lyon	Lyon	k1gInSc1
Mackenzie	Mackenzie	k1gFnSc1
King	King	k1gMnSc1
</s>
<s>
Období	období	k1gNnSc1
vlády	vláda	k1gFnSc2
Jiřího	Jiří	k1gMnSc2
VI	VI	kA
<g/>
.	.	kIx.
bylo	být	k5eAaImAgNnS
poznamenáno	poznamenat	k5eAaPmNgNnS
rozpadem	rozpad	k1gInSc7
Britského	britský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
zahájen	zahájit	k5eAaPmNgInS
Balfourovou	Balfourův	k2eAgFnSc7d1
deklarací	deklarace	k1gFnSc7
a	a	k8xC
konferencí	konference	k1gFnSc7
konanou	konaný	k2eAgFnSc7d1
roku	rok	k1gInSc2
1926	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
začal	začít	k5eAaPmAgInS
vznikat	vznikat	k5eAaImF
Commonwealth	Commonwealth	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
dominiím	dominie	k1gFnPc3
bylo	být	k5eAaImAgNnS
přiznáno	přiznán	k2eAgNnSc4d1
právo	právo	k1gNnSc4
vývoje	vývoj	k1gInSc2
v	v	k7c4
samostatné	samostatný	k2eAgInPc4d1
státy	stát	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
bylo	být	k5eAaImAgNnS
formalizováno	formalizovat	k5eAaBmNgNnS
Westminsterským	Westminsterský	k2eAgInSc7d1
statutem	statut	k1gInSc7
roku	rok	k1gInSc2
1931	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Britský	britský	k2eAgInSc1d1
mandát	mandát	k1gInSc1
z	z	k7c2
pověření	pověření	k1gNnSc2
Společnosti	společnost	k1gFnSc2
národů	národ	k1gInPc2
v	v	k7c6
Iráku	Irák	k1gInSc6
skončil	skončit	k5eAaPmAgInS
roku	rok	k1gInSc2
1932	#num#	k4
a	a	k8xC
země	země	k1gFnSc1
získala	získat	k5eAaPmAgFnS
nezávislost	nezávislost	k1gFnSc4
bez	bez	k7c2
členství	členství	k1gNnSc2
v	v	k7c6
Commonwealthu	Commonwealth	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
proces	proces	k1gInSc1
pokračoval	pokračovat	k5eAaImAgInS
i	i	k9
po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Transjordánsko	Transjordánsko	k1gNnSc1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
nezávislým	závislý	k2eNgInSc7d1
roku	rok	k1gInSc2
1946	#num#	k4
<g/>
,	,	kIx,
Barma	Barma	k1gFnSc1
v	v	k7c6
lednu	leden	k1gInSc6
1948	#num#	k4
a	a	k8xC
Palestina	Palestina	k1gFnSc1
(	(	kIx(
<g/>
i	i	k9
když	když	k8xS
rozdělena	rozdělit	k5eAaPmNgFnS
mezi	mezi	k7c4
Izrael	Izrael	k1gInSc4
a	a	k8xC
arabské	arabský	k2eAgInPc4d1
státy	stát	k1gInPc4
<g/>
)	)	kIx)
v	v	k7c6
květnu	květen	k1gInSc6
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
a	a	k8xC
všechny	všechen	k3xTgInPc1
tři	tři	k4xCgInPc1
státy	stát	k1gInPc1
vystoupily	vystoupit	k5eAaPmAgInP
z	z	k7c2
Commonwealthu	Commonwealth	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Irsko	Irsko	k1gNnSc1
opustilo	opustit	k5eAaPmAgNnS
Commonwealth	Commonwealth	k1gInSc4
následující	následující	k2eAgInSc1d1
rok	rok	k1gInSc1
<g/>
,	,	kIx,
poté	poté	k6eAd1
co	co	k9
byla	být	k5eAaImAgFnS
vyhlášena	vyhlásit	k5eAaPmNgFnS
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
části	část	k1gFnSc6
ostrova	ostrov	k1gInSc2
republika	republika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Indie	Indie	k1gFnSc1
byla	být	k5eAaImAgFnS
rozdělena	rozdělit	k5eAaPmNgFnS
na	na	k7c4
dvě	dva	k4xCgNnPc4
nezávislá	závislý	k2eNgNnPc4d1
dominia	dominion	k1gNnPc4
–	–	k?
Indii	Indie	k1gFnSc4
a	a	k8xC
Pákistán	Pákistán	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
se	se	k3xPyFc4
zřekl	zřeknout	k5eAaPmAgMnS
titulu	titul	k1gInSc3
indického	indický	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	s	k7c7
králem	král	k1gMnSc7
Indie	Indie	k1gFnSc2
a	a	k8xC
králem	král	k1gMnSc7
Pákistánu	Pákistán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1950	#num#	k4
mu	on	k3xPp3gMnSc3
byl	být	k5eAaImAgInS
přiznán	přiznán	k2eAgInSc4d1
titul	titul	k1gInSc4
hlavy	hlava	k1gFnSc2
Commonwealthu	Commonwealth	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Pozdní	pozdní	k2eAgNnSc1d1
období	období	k1gNnSc1
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1
zdraví	zdraví	k1gNnSc1
bylo	být	k5eAaImAgNnS
poznamenáno	poznamenat	k5eAaPmNgNnS
napětím	napětí	k1gNnSc7
způsobeným	způsobený	k2eAgNnSc7d1
válkou	válka	k1gFnSc7
a	a	k8xC
zhoršené	zhoršený	k2eAgNnSc4d1
jeho	jeho	k3xOp3gNnSc4
kouřením	kouření	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
vyvolalo	vyvolat	k5eAaPmAgNnS
rakovinu	rakovina	k1gFnSc4
plic	plíce	k1gFnPc2
a	a	k8xC
arteriosklerózu	arterioskleróza	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dědička	dědička	k1gFnSc1
trůnu	trůn	k1gInSc2
<g/>
,	,	kIx,
princezna	princezna	k1gFnSc1
Alžběta	Alžběta	k1gFnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
postupně	postupně	k6eAd1
s	s	k7c7
jeho	jeho	k3xOp3gMnSc7
zhoršujícím	zhoršující	k2eAgInSc7d1
zdravotním	zdravotní	k2eAgInSc7d1
stavem	stav	k1gInSc7
ujímala	ujímat	k5eAaImAgFnS
větší	veliký	k2eAgFnPc4d2
části	část	k1gFnPc4
královských	královský	k2eAgFnPc2d1
povinností	povinnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
září	září	k1gNnSc6
1951	#num#	k4
podstoupil	podstoupit	k5eAaPmAgMnS
Jiří	Jiří	k1gMnSc1
operaci	operace	k1gFnSc4
<g/>
,	,	kIx,
při	při	k7c6
které	který	k3yQgFnSc6,k3yRgFnSc6,k3yIgFnSc6
mu	on	k3xPp3gMnSc3
byla	být	k5eAaImAgFnS
odstraněna	odstraněn	k2eAgFnSc1d1
levá	levý	k2eAgFnSc1d1
polovina	polovina	k1gFnSc1
plic	plíce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
31	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1952	#num#	k4
byl	být	k5eAaImAgMnS
<g/>
,	,	kIx,
přes	přes	k7c4
varování	varování	k1gNnSc4
lékařů	lékař	k1gMnPc2
<g/>
,	,	kIx,
vyprovodit	vyprovodit	k5eAaPmF
svou	svůj	k3xOyFgFnSc4
dceru	dcera	k1gFnSc4
na	na	k7c4
cestu	cesta	k1gFnSc4
do	do	k7c2
Austrálie	Austrálie	k1gFnSc2
a	a	k8xC
Keni	Keňa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
6	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1952	#num#	k4
zemřel	zemřít	k5eAaPmAgMnS
ve	v	k7c6
spánku	spánek	k1gInSc6
na	na	k7c4
infarkt	infarkt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
mu	on	k3xPp3gMnSc3
pouhých	pouhý	k2eAgNnPc2d1
56	#num#	k4
let	léto	k1gNnPc2
–	–	k?
dožil	dožít	k5eAaPmAgMnS
se	se	k3xPyFc4
nejnižšího	nízký	k2eAgInSc2d3
věku	věk	k1gInSc2
ze	z	k7c2
všech	všecek	k3xTgMnPc2
britských	britský	k2eAgMnPc2d1
hannoverských	hannoverský	k2eAgMnPc2d1
nebo	nebo	k8xC
windsorských	windsorský	k2eAgMnPc2d1
králů	král	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
pohřben	pohřbít	k5eAaPmNgMnS
v	v	k7c6
kapli	kaple	k1gFnSc6
svatého	svatý	k2eAgMnSc2d1
Jiří	Jiří	k1gMnSc2
na	na	k7c6
hradě	hrad	k1gInSc6
Windsoru	Windsor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
manželka	manželka	k1gFnSc1
ho	on	k3xPp3gMnSc4
přežila	přežít	k5eAaPmAgFnS
o	o	k7c4
50	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Tituly	titul	k1gInPc1
a	a	k8xC
vyznamenání	vyznamenání	k1gNnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Tituly	titul	k1gInPc1
a	a	k8xC
vyznamenání	vyznamenání	k1gNnSc1
Jiřího	Jiří	k1gMnSc2
VI	VI	kA
<g/>
..	..	k?
</s>
<s>
Zajímavost	zajímavost	k1gFnSc1
</s>
<s>
Po	po	k7c6
své	svůj	k3xOyFgFnSc6
matce	matka	k1gFnSc6
i	i	k8xC
otci	otec	k1gMnPc7
byl	být	k5eAaImAgMnS
prapravnukem	prapravnuk	k1gMnSc7
krále	král	k1gMnSc2
Jiřího	Jiří	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Odrazy	odraz	k1gInPc1
v	v	k7c6
populární	populární	k2eAgFnSc6d1
kultuře	kultura	k1gFnSc6
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
byl	být	k5eAaImAgInS
do	do	k7c2
kin	kino	k1gNnPc2
uveden	uvést	k5eAaPmNgInS
britsko-australsko-americký	britsko-australsko-americký	k2eAgInSc1d1
film	film	k1gInSc1
Králova	Králův	k2eAgFnSc1d1
řeč	řeč	k1gFnSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
pojednává	pojednávat	k5eAaImIp3nS
o	o	k7c4
léčení	léčení	k1gNnSc4
Jiřího	Jiří	k1gMnSc2
koktavosti	koktavost	k1gFnSc2
a	a	k8xC
přátelství	přátelství	k1gNnSc4
s	s	k7c7
jeho	jeho	k3xOp3gMnSc7
logopedem	logoped	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc4d1
roli	role	k1gFnSc4
krále	král	k1gMnSc2
Jiřího	Jiří	k1gMnSc2
VI	VI	kA
<g/>
.	.	kIx.
v	v	k7c6
tomto	tento	k3xDgInSc6
snímku	snímek	k1gInSc6
ztvárnil	ztvárnit	k5eAaPmAgMnS
britský	britský	k2eAgMnSc1d1
herec	herec	k1gMnSc1
Colin	Colin	k1gMnSc1
Firth	Firth	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
byl	být	k5eAaImAgMnS
za	za	k7c4
tuto	tento	k3xDgFnSc4
roli	role	k1gFnSc4
obdržel	obdržet	k5eAaPmAgInS
cenu	cena	k1gFnSc4
BAFTA	BAFTA	kA
<g/>
,	,	kIx,
Zlatý	zlatý	k2eAgInSc1d1
glóbus	glóbus	k1gInSc1
i	i	k9
Oscara	Oscar	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
role	role	k1gFnSc2
Lionela	Lionel	k1gMnSc2
Logua	Loguum	k1gNnSc2
byl	být	k5eAaImAgMnS
obsazen	obsazen	k2eAgMnSc1d1
herec	herec	k1gMnSc1
Geoffrey	Geoffrea	k1gFnSc2
Rush	Rush	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgMnS
za	za	k7c4
tuto	tento	k3xDgFnSc4
roli	role	k1gFnSc4
také	také	k6eAd1
nominován	nominovat	k5eAaBmNgMnS
na	na	k7c4
Oscara	Oscar	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Role	role	k1gFnSc1
manželky	manželka	k1gFnSc2
krále	král	k1gMnSc2
Jiřího	Jiří	k1gMnSc2
VI	VI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elizabeth	Elizabeth	k1gFnSc1
Bowes-Lyonové	Bowes-Lyonová	k1gFnSc2
se	se	k3xPyFc4
ujala	ujmout	k5eAaPmAgFnS
rovněž	rovněž	k9
nominovaná	nominovaný	k2eAgFnSc1d1
Helena	Helena	k1gFnSc1
Bonham	Bonham	k1gInSc1
Carterová	Carterová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Vývod	vývod	k1gInSc1
z	z	k7c2
předků	předek	k1gInPc2
</s>
<s>
Arnošt	Arnošt	k1gMnSc1
I.	I.	kA
Sasko-Kobursko-Gothajský	Sasko-Kobursko-Gothajský	k2eAgInSc4d1
</s>
<s>
Albert	Albert	k1gMnSc1
Sasko-Kobursko-Gothajský	Sasko-Kobursko-Gothajský	k2eAgMnSc1d1
</s>
<s>
Luisa	Luisa	k1gFnSc1
Sasko-Gothajsko-Altenburská	Sasko-Gothajsko-Altenburský	k2eAgFnSc1d1
</s>
<s>
Eduard	Eduard	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s>
Eduard	Eduard	k1gMnSc1
August	August	k1gMnSc1
Hannoverský	hannoverský	k2eAgMnSc1d1
</s>
<s>
královna	královna	k1gFnSc1
Viktorie	Viktoria	k1gFnSc2
</s>
<s>
Viktorie	Viktorie	k1gFnSc1
Sasko-Kobursko-Saalfeldská	Sasko-Kobursko-Saalfeldský	k2eAgFnSc1d1
</s>
<s>
Jiří	Jiří	k1gMnSc1
V.	V.	kA
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
Vilém	Vilém	k1gMnSc1
Šlesvicko-Holštýnsko-Sonderbursko-Glücksburský	Šlesvicko-Holštýnsko-Sonderbursko-Glücksburský	k2eAgMnSc1d1
</s>
<s>
Kristián	Kristián	k1gMnSc1
IX	IX	kA
<g/>
.	.	kIx.
</s>
<s>
Luisa	Luisa	k1gFnSc1
Karolina	Karolinum	k1gNnSc2
Hesensko-Kasselská	hesensko-kasselský	k2eAgFnSc1d1
</s>
<s>
Alexandra	Alexandra	k1gFnSc1
Dánská	dánský	k2eAgFnSc1d1
</s>
<s>
Vilém	Vilém	k1gMnSc1
Hesensko-Kasselský	hesensko-kasselský	k2eAgMnSc1d1
</s>
<s>
Luisa	Luisa	k1gFnSc1
Hesensko-Kasselská	hesensko-kasselský	k2eAgFnSc1d1
</s>
<s>
Luisa	Luisa	k1gFnSc1
Šarlota	Šarlota	k1gFnSc1
Dánská	dánský	k2eAgFnSc1d1
</s>
<s>
Jiří	Jiří	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
Württemberský	Württemberský	k2eAgMnSc1d1
</s>
<s>
Alexandr	Alexandr	k1gMnSc1
Württemberský	Württemberský	k2eAgMnSc1d1
</s>
<s>
Henrietta	Henrietta	k1gFnSc1
Nasavsko-Weilburská	Nasavsko-Weilburský	k2eAgFnSc1d1
</s>
<s>
František	František	k1gMnSc1
z	z	k7c2
Tecku	Teck	k1gInSc2
</s>
<s>
László	László	k?
Rhédy	Rhéd	k1gInPc1
de	de	k?
Kis-Rhéde	Kis-Rhéd	k1gMnSc5
</s>
<s>
Claudine	Claudinout	k5eAaPmIp3nS
Rhédey	Rhéde	k1gMnPc4
von	von	k1gInSc1
Kis-Rhéde	Kis-Rhéd	k1gMnSc5
</s>
<s>
Ágnes	Ágnes	k1gMnSc1
Inczédy	Inczéda	k1gFnSc2
de	de	k?
Nagy-Várad	Nagy-Várad	k1gInSc1
</s>
<s>
Marie	Marie	k1gFnSc1
z	z	k7c2
Tecku	Teck	k1gInSc2
</s>
<s>
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Adolf	Adolf	k1gMnSc1
z	z	k7c2
Cambridge	Cambridge	k1gFnSc2
</s>
<s>
Šarlota	Šarlota	k1gFnSc1
Meklenbursko-Střelická	Meklenbursko-Střelický	k2eAgFnSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Adelaida	Adelaida	k1gFnSc1
z	z	k7c2
Cambridge	Cambridge	k1gFnSc2
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
Hesensko-Kasselský	hesensko-kasselský	k2eAgMnSc1d1
</s>
<s>
Augusta	Augusta	k1gMnSc1
Hesensko-Kaselská	Hesensko-Kaselský	k2eAgNnPc4d1
</s>
<s>
Karolina	Karolinum	k1gNnPc1
Nasavsko-Usingenská	Nasavsko-Usingenský	k2eAgNnPc1d1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
George	Georg	k1gMnSc2
VI	VI	kA
of	of	k?
the	the	k?
United	United	k1gInSc1
Kingdom	Kingdom	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
(	(	kIx(
<g/>
EISB	EISB	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
electronic	electronice	k1gFnPc2
Irish	Irisha	k1gFnPc2
Statute	statut	k1gInSc5
Book	Booka	k1gFnPc2
<g/>
.	.	kIx.
electronic	electronice	k1gFnPc2
Irish	Irish	k1gInSc1
Statute	statut	k1gInSc5
Book	Booka	k1gFnPc2
(	(	kIx(
<g/>
eISB	eISB	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
www.irishstatutebook.ie	www.irishstatutebook.ie	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Jiří	Jiří	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britský	britský	k2eAgInSc4d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Jiří	Jiří	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Zpráva	zpráva	k1gFnSc1
Král	Král	k1gMnSc1
Jiří	Jiří	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
zemřel	zemřít	k5eAaPmAgMnS
<g/>
,	,	kIx,
na	na	k7c6
trůnu	trůn	k1gInSc6
nahrazen	nahrazen	k2eAgInSc4d1
Alžbětou	Alžběta	k1gFnSc7
II	II	kA
<g/>
.	.	kIx.
ve	v	k7c6
Wikizprávách	Wikizpráva	k1gFnPc6
</s>
<s>
http://thepeerage.com/p10068.htm#i100679	http://thepeerage.com/p10068.htm#i100679	k4
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
britské	britský	k2eAgFnSc2d1
královské	královský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
</s>
<s>
Král	Král	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
z	z	k7c2
boje	boj	k1gInSc2
neutekl	utéct	k5eNaPmAgInS
pořad	pořad	k1gInSc1
Českého	český	k2eAgInSc2d1
rozhlasu	rozhlas	k1gInSc2
Plus	plus	k1gInSc1
<g/>
,	,	kIx,
obsahuje	obsahovat	k5eAaImIp3nS
množství	množství	k1gNnSc1
podrobných	podrobný	k2eAgFnPc2d1
informací	informace	k1gFnPc2
o	o	k7c6
životě	život	k1gInSc6
Jiřího	Jiří	k1gMnSc2
VI	VI	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
jeho	jeho	k3xOp3gFnSc6
cestě	cesta	k1gFnSc6
na	na	k7c4
trůn	trůn	k1gInSc4
a	a	k8xC
panování	panování	k1gNnSc4
v	v	k7c6
letech	léto	k1gNnPc6
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
i	i	k9
po	po	k7c6
ní	on	k3xPp3gFnSc6
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Britští	britský	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
Království	království	k1gNnSc1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
(	(	kIx(
<g/>
1707	#num#	k4
<g/>
–	–	k?
<g/>
1801	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Anna	Anna	k1gFnSc1
Stuartovna	Stuartovna	k1gFnSc1
(	(	kIx(
<g/>
1702	#num#	k4
<g/>
–	–	k?
<g/>
1714	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
</s>
<s>
Jiří	Jiří	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1714	#num#	k4
<g/>
–	–	k?
<g/>
1727	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
</s>
<s>
Jiří	Jiří	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1727	#num#	k4
<g/>
–	–	k?
<g/>
1760	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
</s>
<s>
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1760	#num#	k4
<g/>
–	–	k?
<g/>
1801	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Irska	Irsko	k1gNnSc2
(	(	kIx(
<g/>
1801	#num#	k4
<g/>
–	–	k?
<g/>
1927	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1801	#num#	k4
<g/>
–	–	k?
<g/>
1820	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1820	#num#	k4
<g/>
–	–	k?
<g/>
1830	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vilém	Vilém	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britský	britský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1830	#num#	k4
<g/>
–	–	k?
<g/>
1837	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Viktorie	Viktorie	k1gFnSc1
(	(	kIx(
<g/>
1837	#num#	k4
<g/>
–	–	k?
<g/>
1901	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Eduard	Eduard	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1901	#num#	k4
<g/>
–	–	k?
<g/>
1910	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
V.	V.	kA
(	(	kIx(
<g/>
1910	#num#	k4
<g/>
–	–	k?
<g/>
1927	#num#	k4
<g/>
)	)	kIx)
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Severního	severní	k2eAgNnSc2d1
Irska	Irsko	k1gNnSc2
(	(	kIx(
<g/>
od	od	k7c2
r.	r.	kA
1927	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
V.	V.	kA
(	(	kIx(
<g/>
1927	#num#	k4
<g/>
–	–	k?
<g/>
1936	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Eduard	Eduard	k1gMnSc1
VIII	VIII	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
od	od	k7c2
1952	#num#	k4
<g/>
)	)	kIx)
1	#num#	k4
<g/>
Rovněž	rovněž	k9
vládce	vládce	k1gMnSc2
Irska	Irsko	k1gNnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
osa	osa	k1gFnSc1
<g/>
2011627930	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118690477	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0004	#num#	k4
1024	#num#	k4
9563	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
50024182	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500356596	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
8330386	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
50024182	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
