<s>
Botanik	botanik	k1gMnSc1	botanik
Miloslav	Miloslav	k1gMnSc1	Miloslav
Kovanda	Kovanda	k1gMnSc1	Kovanda
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zvonek	zvonek	k1gInSc1	zvonek
jesenický	jesenický	k2eAgInSc1d1	jesenický
popsal	popsat	k5eAaPmAgInS	popsat
jako	jako	k8xC	jako
první	první	k4xOgInSc4	první
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
ho	on	k3xPp3gInSc4	on
dokonce	dokonce	k9	dokonce
později	pozdě	k6eAd2	pozdě
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
poddruh	poddruh	k1gInSc4	poddruh
zvonku	zvonek	k1gInSc2	zvonek
českého	český	k2eAgInSc2d1	český
<g/>
.	.	kIx.	.
</s>
