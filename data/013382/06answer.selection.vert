<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
nejpočetněji	početně	k6eAd3	početně
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
a	a	k8xC	a
teplejších	teplý	k2eAgFnPc6d2	teplejší
pahorkatinách	pahorkatina	k1gFnPc6	pahorkatina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
místy	místy	k6eAd1	místy
i	i	k9	i
vysoko	vysoko	k6eAd1	vysoko
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
<g/>
.	.	kIx.	.
</s>
