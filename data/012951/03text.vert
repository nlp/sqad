<p>
<s>
Bečva	Bečva	k1gFnSc1	Bečva
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc1d3	veliký
levostranný	levostranný	k2eAgInSc1d1	levostranný
přítok	přítok	k1gInSc1	přítok
řeky	řeka	k1gFnSc2	řeka
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
toku	tok	k1gInSc2	tok
řeky	řeka	k1gFnSc2	řeka
činí	činit	k5eAaImIp3nS	činit
61,5	[number]	k4	61,5
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Plocha	plocha	k1gFnSc1	plocha
povodí	povodí	k1gNnSc2	povodí
měří	měřit	k5eAaImIp3nS	měřit
1620,19	[number]	k4	1620,19
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc1	průběh
toku	tok	k1gInSc2	tok
==	==	k?	==
</s>
</p>
<p>
<s>
Řeka	řeka	k1gFnSc1	řeka
vzniká	vznikat	k5eAaImIp3nS	vznikat
soutokem	soutok	k1gInSc7	soutok
Vsetínské	vsetínský	k2eAgFnSc2d1	vsetínská
a	a	k8xC	a
Rožnovské	Rožnovské	k2eAgFnSc2d1	Rožnovské
Bečvy	Bečva	k1gFnSc2	Bečva
ve	v	k7c6	v
Valašském	valašský	k2eAgNnSc6d1	Valašské
Meziříčí	Meziříčí	k1gNnSc6	Meziříčí
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
řeky	řeka	k1gFnPc1	řeka
pramení	pramenit	k5eAaImIp3nP	pramenit
na	na	k7c6	na
svazích	svah	k1gInPc6	svah
Vysoké	vysoká	k1gFnSc2	vysoká
<g/>
,	,	kIx,	,
Vsetínská	vsetínský	k2eAgFnSc1d1	vsetínská
Bečva	Bečva	k1gFnSc1	Bečva
(	(	kIx(	(
<g/>
též	též	k9	též
označována	označován	k2eAgFnSc1d1	označována
jako	jako	k8xS	jako
Horní	horní	k2eAgFnSc1d1	horní
Bečva	Bečva	k1gFnSc1	Bečva
<g/>
)	)	kIx)	)
na	na	k7c6	na
jižních	jižní	k2eAgFnPc6d1	jižní
a	a	k8xC	a
Rožnovská	rožnovský	k2eAgFnSc1d1	Rožnovská
Bečva	Bečva	k1gFnSc1	Bečva
(	(	kIx(	(
<g/>
též	též	k9	též
označována	označován	k2eAgFnSc1d1	označována
jako	jako	k8xS	jako
Dolní	dolní	k2eAgFnSc1d1	dolní
Bečva	Bečva	k1gFnSc1	Bečva
<g/>
)	)	kIx)	)
na	na	k7c6	na
severních	severní	k2eAgFnPc6d1	severní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
rožnovskému	rožnovský	k2eAgInSc3d1	rožnovský
pramenu	pramen	k1gInSc3	pramen
se	se	k3xPyFc4	se
dostaneme	dostat	k5eAaPmIp1nP	dostat
z	z	k7c2	z
horské	horský	k2eAgFnSc2d1	horská
chaty	chata	k1gFnSc2	chata
Třeštík	Třeštík	k1gInSc4	Třeštík
po	po	k7c6	po
červené	červený	k2eAgFnSc6d1	červená
turistické	turistický	k2eAgFnSc6d1	turistická
značce	značka	k1gFnSc6	značka
<g/>
.	.	kIx.	.
</s>
<s>
Odbočka	odbočka	k1gFnSc1	odbočka
k	k	k7c3	k
pramenu	pramen	k1gInSc3	pramen
vede	vést	k5eAaImIp3nS	vést
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
,	,	kIx,	,
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
vrcholu	vrchol	k1gInSc2	vrchol
hory	hora	k1gFnSc2	hora
Vysoká	vysoká	k1gFnSc1	vysoká
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
pramenu	pramen	k1gInSc3	pramen
Vsetínské	vsetínský	k2eAgFnSc2d1	vsetínská
Bečvy	Bečva	k1gFnSc2	Bečva
dojdeme	dojít	k5eAaPmIp1nP	dojít
od	od	k7c2	od
rozhledny	rozhledna	k1gFnSc2	rozhledna
Súkenická	Súkenický	k2eAgNnPc4d1	Súkenický
<g/>
,	,	kIx,	,
také	také	k9	také
nedaleko	nedaleko	k7c2	nedaleko
horské	horský	k2eAgFnSc2d1	horská
chaty	chata	k1gFnSc2	chata
Třeštík	Třeštík	k1gInSc4	Třeštík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Větší	veliký	k2eAgInPc1d2	veliký
přítoky	přítok	k1gInPc1	přítok
===	===	k?	===
</s>
</p>
<p>
<s>
levé	levá	k1gFnPc1	levá
–	–	k?	–
Loučka	loučka	k1gFnSc1	loučka
<g/>
,	,	kIx,	,
Juhyně	Juhyně	k1gFnSc1	Juhyně
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
pravé	pravá	k1gFnPc1	pravá
–	–	k?	–
Mřenka	mřenka	k1gFnSc1	mřenka
<g/>
,	,	kIx,	,
Velička	Velička	k1gFnSc1	Velička
<g/>
,	,	kIx,	,
Jezernice	Jezernice	k1gFnSc1	Jezernice
</s>
</p>
<p>
<s>
==	==	k?	==
Vodní	vodní	k2eAgInSc1d1	vodní
režim	režim	k1gInSc1	režim
==	==	k?	==
</s>
</p>
<p>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
průtok	průtok	k1gInSc1	průtok
u	u	k7c2	u
ústí	ústí	k1gNnSc2	ústí
činí	činit	k5eAaImIp3nS	činit
17,5	[number]	k4	17,5
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Hlásné	hlásný	k2eAgInPc4d1	hlásný
profily	profil	k1gInPc4	profil
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Oba	dva	k4xCgInPc4	dva
prameny	pramen	k1gInPc4	pramen
Bečvy	Bečva	k1gFnSc2	Bečva
zakreslil	zakreslit	k5eAaPmAgInS	zakreslit
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
mapě	mapa	k1gFnSc6	mapa
Moravy	Morava	k1gFnSc2	Morava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1627	[number]	k4	1627
již	již	k6eAd1	již
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Amos	Amos	k1gMnSc1	Amos
Komenský	Komenský	k1gMnSc1	Komenský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
sloužila	sloužit	k5eAaImAgFnS	sloužit
řeka	řeka	k1gFnSc1	řeka
jako	jako	k8xS	jako
dopravní	dopravní	k2eAgFnSc1d1	dopravní
cesta	cesta	k1gFnSc1	cesta
pro	pro	k7c4	pro
plavbu	plavba	k1gFnSc4	plavba
dřeva	dřevo	k1gNnSc2	dřevo
a	a	k8xC	a
také	také	k9	také
jako	jako	k9	jako
zásobárna	zásobárna	k1gFnSc1	zásobárna
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
byly	být	k5eAaImAgFnP	být
obě	dva	k4xCgFnPc1	dva
části	část	k1gFnPc1	část
řeky	řeka	k1gFnSc2	řeka
ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
takřka	takřka	k6eAd1	takřka
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
zregulovány	zregulovat	k5eAaPmNgFnP	zregulovat
<g/>
,	,	kIx,	,
hrozí	hrozit	k5eAaImIp3nS	hrozit
řeka	řeka	k1gFnSc1	řeka
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
záplavami	záplava	k1gFnPc7	záplava
<g/>
.	.	kIx.	.
</s>
<s>
Jedny	jeden	k4xCgFnPc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
postihly	postihnout	k5eAaPmAgFnP	postihnout
okolí	okolí	k1gNnSc4	okolí
řeky	řeka	k1gFnSc2	řeka
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
toku	tok	k1gInSc6	tok
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Vít	Vít	k1gMnSc1	Vít
Ryšánek	Ryšánek	k1gMnSc1	Ryšánek
<g/>
,	,	kIx,	,
Soutoky	soutok	k1gInPc1	soutok
řek	řeka	k1gFnPc2	řeka
na	na	k7c6	na
území	území	k1gNnSc6	území
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
Slezska	Slezsko	k1gNnSc2	Slezsko
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Vodní	vodní	k2eAgInSc1d1	vodní
koridor	koridor	k1gInSc1	koridor
Dunaj	Dunaj	k1gInSc1	Dunaj
<g/>
–	–	k?	–
<g/>
Odra	Odra	k1gFnSc1	Odra
<g/>
–	–	k?	–
<g/>
Labe	Labe	k1gNnSc2	Labe
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bečva	Bečva	k1gFnSc1	Bečva
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Bečva	Bečva	k1gFnSc1	Bečva
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Bečva	Bečva	k1gFnSc1	Bečva
na	na	k7c6	na
portálu	portál	k1gInSc6	portál
Beskydy	Beskyd	k1gInPc1	Beskyd
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Dluhonice	Dluhonice	k1gFnSc1	Dluhonice
–	–	k?	–
aktuální	aktuální	k2eAgInSc4d1	aktuální
vodní	vodní	k2eAgInSc4d1	vodní
stav	stav	k1gInSc4	stav
</s>
</p>
