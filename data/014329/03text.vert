<s>
Vladimir	Vladimir	k1gMnSc1
Klavdijevič	Klavdijevič	k1gMnSc1
Arseňjev	Arseňjev	k1gMnSc1
</s>
<s>
Vladimir	Vladimir	k1gMnSc1
Klavdijevič	Klavdijevič	k1gMnSc1
Arseňjev	Arseňjev	k1gMnSc1
Vladimir	Vladimir	k1gMnSc1
Klavdijevič	Klavdijevič	k1gMnSc1
Arseňjev	Arseňjev	k1gMnSc1
Narození	narození	k1gNnSc4
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
srpnajul	srpnajout	k5eAaPmAgInS
<g/>
.	.	kIx.
/	/	kIx~
10	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1872	#num#	k4
<g/>
greg	grega	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Petrohrad	Petrohrad	k1gInSc1
<g/>
,	,	kIx,
Ruské	ruský	k2eAgNnSc1d1
impérium	impérium	k1gNnSc1
Ruské	ruský	k2eAgNnSc1d1
impérium	impérium	k1gNnSc1
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1930	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
58	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Vladivostok	Vladivostok	k1gInSc1
<g/>
,	,	kIx,
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Sovětský	sovětský	k2eAgInSc4d1
svaz	svaz	k1gInSc4
Příčina	příčina	k1gFnSc1
úmrtí	úmrtí	k1gNnPc2
</s>
<s>
bronchopneumonie	bronchopneumonie	k1gFnSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Naval	navalit	k5eAaPmRp2nS
Cemetery	Cemeter	k1gInPc1
(	(	kIx(
<g/>
Vladivostok	Vladivostok	k1gInSc1
<g/>
)	)	kIx)
Povolání	povolání	k1gNnSc1
</s>
<s>
etnograf	etnograf	k1gMnSc1
<g/>
,	,	kIx,
geograf	geograf	k1gMnSc1
<g/>
,	,	kIx,
cestovatel	cestovatel	k1gMnSc1
a	a	k8xC
spisovatel	spisovatel	k1gMnSc1
Národnost	národnost	k1gFnSc4
</s>
<s>
ruská	ruský	k2eAgFnSc1d1
Stát	stát	k1gInSc1
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
В	В	k?
в	в	k?
у	у	k?
(	(	kIx(
<g/>
1893	#num#	k4
<g/>
–	–	k?
<g/>
1895	#num#	k4
<g/>
)	)	kIx)
Žánr	žánr	k1gInSc1
</s>
<s>
cestopis	cestopis	k1gInSc1
Témata	téma	k1gNnPc4
</s>
<s>
etnografie	etnografie	k1gFnSc1
Významná	významný	k2eAgFnSc1d1
díla	dílo	k1gNnPc4
</s>
<s>
Děrsu	Děrs	k1gInSc2
Uzala	Uzalo	k1gNnSc2
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Medaile	medaile	k1gFnSc1
za	za	k7c4
tažení	tažení	k1gNnSc4
do	do	k7c2
Číny	Čína	k1gFnSc2
(	(	kIx(
<g/>
1901	#num#	k4
<g/>
)	)	kIx)
<g/>
Řád	řád	k1gInSc1
sv.	sv.	kA
Anny	Anna	k1gFnSc2
4	#num#	k4
<g/>
.	.	kIx.
třídy	třída	k1gFnSc2
s	s	k7c7
nápisem	nápis	k1gInSc7
Za	za	k7c4
Chrabrost	chrabrost	k1gFnSc4
(	(	kIx(
<g/>
1904	#num#	k4
<g/>
)	)	kIx)
<g/>
Řád	řád	k1gInSc1
sv.	sv.	kA
Anny	Anna	k1gFnSc2
3	#num#	k4
<g/>
.	.	kIx.
třídy	třída	k1gFnSc2
(	(	kIx(
<g/>
1906	#num#	k4
<g/>
)	)	kIx)
<g/>
Řád	řád	k1gInSc1
sv.	sv.	kA
Stanislava	Stanislava	k1gFnSc1
3	#num#	k4
<g/>
.	.	kIx.
třídy	třída	k1gFnSc2
(	(	kIx(
<g/>
1906	#num#	k4
<g/>
)	)	kIx)
<g/>
Řád	řád	k1gInSc1
sv.	sv.	kA
Stanislava	Stanislava	k1gFnSc1
2	#num#	k4
<g/>
.	.	kIx.
třídy	třída	k1gFnSc2
(	(	kIx(
<g/>
1907	#num#	k4
<g/>
)	)	kIx)
<g/>
…	…	k?
více	hodně	k6eAd2
na	na	k7c6
Wikidatech	Wikidat	k1gInPc6
Podpis	podpis	k1gInSc4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vladimir	Vladimir	k1gMnSc1
Klavdijevič	Klavdijevič	k1gMnSc1
Arseňjev	Arseňjev	k1gMnSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
В	В	k?
К	К	k?
А	А	k?
<g/>
;	;	kIx,
29	#num#	k4
<g/>
.	.	kIx.
srpnajul	srpnajout	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
/	/	kIx~
10	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1872	#num#	k4
<g/>
greg	grega	k1gFnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Petrohrad	Petrohrad	k1gInSc1
–	–	k?
4	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1930	#num#	k4
<g/>
,	,	kIx,
Vladivostok	Vladivostok	k1gInSc4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
ruský	ruský	k2eAgMnSc1d1
sovětský	sovětský	k2eAgMnSc1d1
etnograf	etnograf	k1gMnSc1
<g/>
,	,	kIx,
geograf	geograf	k1gMnSc1
<g/>
,	,	kIx,
cestovatel	cestovatel	k1gMnSc1
a	a	k8xC
spisovatel	spisovatel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Arseňjev	Arseňjet	k5eAaPmDgInS
v	v	k7c6
roce	rok	k1gInSc6
1900	#num#	k4
</s>
<s>
Arseňjevův	Arseňjevův	k2eAgInSc4d1
dekret	dekret	k1gInSc4
člena	člen	k1gMnSc2
Ruské	ruský	k2eAgFnSc2d1
geografické	geografický	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
</s>
<s>
Arseňjevův	Arseňjevův	k2eAgInSc1d1
hrob	hrob	k1gInSc1
ve	v	k7c6
Vladivostoku	Vladivostok	k1gInSc6
</s>
<s>
V	v	k7c6
hlubinách	hlubina	k1gFnPc6
Ussurijského	Ussurijský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
,	,	kIx,
první	první	k4xOgNnSc4
vydání	vydání	k1gNnSc4
z	z	k7c2
roku	rok	k1gInSc2
1926	#num#	k4
</s>
<s>
Lesní	lesní	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
–	–	k?
Udegejci	Udegejec	k1gMnSc6
<g/>
,	,	kIx,
první	první	k4xOgNnSc4
vydání	vydání	k1gNnSc4
z	z	k7c2
roku	rok	k1gInSc2
1926	#num#	k4
</s>
<s>
Arseňjev	Arseňjev	k1gMnSc1xF
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgInS
se	se	k3xPyFc4
v	v	k7c6
Petrohradě	Petrohrad	k1gInSc6
v	v	k7c6
rodině	rodina	k1gFnSc6
železničního	železniční	k2eAgMnSc2d1
zaměstnance	zaměstnanec	k1gMnSc2
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yRgFnSc6
bylo	být	k5eAaImAgNnS
celkem	celkem	k6eAd1
deset	deset	k4xCc1
dětí	dítě	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1892	#num#	k4
<g/>
–	–	k?
<g/>
1895	#num#	k4
absolvoval	absolvovat	k5eAaPmAgInS
petrohradské	petrohradský	k2eAgNnSc4d1
pěchotní	pěchotní	k2eAgNnSc4d1
učiliště	učiliště	k1gNnSc4
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
se	se	k3xPyFc4
začal	začít	k5eAaPmAgMnS
zajímat	zajímat	k5eAaImF
o	o	k7c4
Dálný	dálný	k2eAgInSc4d1
východ	východ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejprve	nejprve	k6eAd1
sloužil	sloužit	k5eAaImAgInS
v	v	k7c6
polském	polský	k2eAgNnSc6d1
městě	město	k1gNnSc6
Łomża	Łomż	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
roku	rok	k1gInSc2
1900	#num#	k4
byl	být	k5eAaImAgInS
na	na	k7c4
vlastní	vlastní	k2eAgFnSc4d1
žádost	žádost	k1gFnSc4
převelen	převelet	k5eAaPmNgMnS
do	do	k7c2
Vladivostoku	Vladivostok	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
letech	let	k1gInPc6
1904	#num#	k4
<g/>
–	–	k?
<g/>
1905	#num#	k4
bojoval	bojovat	k5eAaImAgMnS
v	v	k7c6
rusko-japonské	rusko-japonský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
,	,	kIx,
získal	získat	k5eAaPmAgMnS
tři	tři	k4xCgFnPc4
medaile	medaile	k1gFnPc4
a	a	k8xC
posléze	posléze	k6eAd1
dosáhl	dosáhnout	k5eAaPmAgInS
hodnosti	hodnost	k1gFnSc2
podplukovníka	podplukovník	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Především	především	k6eAd1
se	se	k3xPyFc4
zde	zde	k6eAd1
ale	ale	k8xC
zúčastnil	zúčastnit	k5eAaPmAgMnS
v	v	k7c6
letech	let	k1gInPc6
1902	#num#	k4
<g/>
–	–	k?
<g/>
1906	#num#	k4
řady	řada	k1gFnSc2
vědeckých	vědecký	k2eAgFnPc2d1
expedicí	expedice	k1gFnPc2
v	v	k7c6
povodí	povodí	k1gNnSc6
řeky	řeka	k1gFnSc2
Ussuri	Ussur	k1gFnSc2
<g/>
,	,	kIx,
ze	z	k7c2
kterých	který	k3yRgFnPc2,k3yQgFnPc2,k3yIgFnPc2
vytěžil	vytěžit	k5eAaPmAgMnS
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
zeměpisných	zeměpisný	k2eAgFnPc2d1
<g/>
,	,	kIx,
etnografických	etnografický	k2eAgFnPc2d1
a	a	k8xC
přírodovědeckých	přírodovědecký	k2eAgFnPc2d1
studií	studie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
první	první	k4xOgFnSc6
popsal	popsat	k5eAaPmAgMnS
četné	četný	k2eAgInPc4d1
druhy	druh	k1gInPc4
sibiřské	sibiřský	k2eAgFnSc2d1
flóry	flóra	k1gFnSc2
a	a	k8xC
způsob	způsob	k1gInSc1
života	život	k1gInSc2
původních	původní	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
autorem	autor	k1gMnSc7
osobitých	osobitý	k2eAgFnPc2d1
próz	próza	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc2
spojil	spojit	k5eAaPmAgMnS
pozorování	pozorování	k1gNnSc1
přírody	příroda	k1gFnSc2
se	s	k7c7
zájmem	zájem	k1gInSc7
o	o	k7c4
hrdinské	hrdinský	k2eAgInPc4d1
osudy	osud	k1gInPc4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1906	#num#	k4
<g/>
–	–	k?
<g/>
1910	#num#	k4
prozkoumával	prozkoumávat	k5eAaImAgInS
hory	hora	k1gFnSc2
Sichote-Aliň	Sichote-Aliň	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1909	#num#	k4
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
členem	člen	k1gMnSc7
Ruské	ruský	k2eAgFnSc2d1
geografické	geografický	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1910	#num#	k4
byl	být	k5eAaImAgInS
dekretem	dekret	k1gInSc7
cara	car	k1gMnSc2
Mikuláše	Mikuláš	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
propuštěn	propuštěn	k2eAgMnSc1d1
z	z	k7c2
vojenské	vojenský	k2eAgFnSc2d1
služby	služba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1910	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
vykonával	vykonávat	k5eAaImAgInS
funkci	funkce	k1gFnSc4
ředitele	ředitel	k1gMnSc2
Regionálního	regionální	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
v	v	k7c6
Chabarovsku	Chabarovsk	k1gInSc6
a	a	k8xC
vyučoval	vyučovat	k5eAaImAgMnS
na	na	k7c6
Dálněvýchodní	dálněvýchodní	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1918	#num#	k4
navštívil	navštívit	k5eAaPmAgMnS
Kamčatku	Kamčatka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1919	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
čestným	čestný	k2eAgMnSc7d1
členem	člen	k1gMnSc7
washingtonské	washingtonský	k2eAgFnSc2d1
Národní	národní	k2eAgFnSc2d1
geografické	geografický	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
a	a	k8xC
britské	britský	k2eAgFnSc2d1
Královské	královský	k2eAgFnSc2d1
geografické	geografický	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
(	(	kIx(
<g/>
celkem	celkem	k6eAd1
byl	být	k5eAaImAgMnS
členem	člen	k1gInSc7
dvaceti	dvacet	k4xCc2
národních	národní	k2eAgFnPc2d1
vědeckých	vědecký	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Během	během	k7c2
ruské	ruský	k2eAgFnSc2d1
občanské	občanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
byl	být	k5eAaImAgInS
komisařem	komisař	k1gMnSc7
pro	pro	k7c4
národnostní	národnostní	k2eAgFnPc4d1
menšiny	menšina	k1gFnPc4
nezávislé	závislý	k2eNgFnSc2d1
Dálněvýchodní	dálněvýchodní	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Revoluci	revoluce	k1gFnSc4
však	však	k8xC
vnímal	vnímat	k5eAaImAgInS
jako	jako	k9
nesmyslné	smyslný	k2eNgFnPc4d1
orgie	orgie	k1gFnPc4
násilí	násilí	k1gNnSc2
a	a	k8xC
byl	být	k5eAaImAgMnS
opakovaně	opakovaně	k6eAd1
vyslýchán	vyslýchat	k5eAaImNgMnS
sovětskými	sovětský	k2eAgMnPc7d1
důstojníky	důstojník	k1gMnPc7
bezpečnosti	bezpečnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
byla	být	k5eAaImAgFnS
roku	rok	k1gInSc2
1922	#num#	k4
Dálněvýchodní	dálněvýchodní	k2eAgFnSc1d1
republika	republika	k1gFnSc1
pohlcena	pohltit	k5eAaPmNgFnS
Sovětským	sovětský	k2eAgInSc7d1
svazem	svaz	k1gInSc7
<g/>
,	,	kIx,
Arseňjev	Arseňjev	k1gFnSc7
odmítl	odmítnout	k5eAaPmAgMnS
emigrovat	emigrovat	k5eAaBmF
a	a	k8xC
zůstal	zůstat	k5eAaPmAgMnS
ve	v	k7c6
Vladivostoku	Vladivostok	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Roku	rok	k1gInSc2
1923	#num#	k4
navštívil	navštívit	k5eAaPmAgInS
Komandorské	Komandorský	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1924	#num#	k4
<g/>
–	–	k?
<g/>
1926	#num#	k4
byl	být	k5eAaImAgInS
opět	opět	k6eAd1
ředitelem	ředitel	k1gMnSc7
Regionálního	regionální	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
v	v	k7c6
Chabarovsku	Chabarovsk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1927	#num#	k4
uspořádal	uspořádat	k5eAaPmAgInS
velkou	velký	k2eAgFnSc4d1
expedici	expedice	k1gFnSc4
Sovětskaja	Sovětskaj	k1gInSc2
Gavaň-Chabarovsk	Gavaň-Chabarovsk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lednu	leden	k1gInSc6
roku	rok	k1gInSc2
1930	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
vedoucím	vedoucí	k1gMnSc7
útvaru	útvar	k1gInSc2
pro	pro	k7c4
ekonomický	ekonomický	k2eAgInSc4d1
výzkum	výzkum	k1gInSc4
nových	nový	k2eAgFnPc2d1
železničních	železniční	k2eAgFnPc2d1
tratí	trať	k1gFnPc2
Ussurijské	Ussurijský	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
a	a	k8xC
zároveň	zároveň	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
hlavou	hlava	k1gFnSc7
čtyř	čtyři	k4xCgInPc2
výprav	výprava	k1gFnPc2
mířících	mířící	k2eAgInPc2d1
do	do	k7c2
oblastí	oblast	k1gFnPc2
plánovaných	plánovaný	k2eAgFnPc2d1
železničních	železniční	k2eAgFnPc2d1
tratí	trať	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
oficiálního	oficiální	k2eAgNnSc2d1
vyjádření	vyjádření	k1gNnSc2
se	se	k3xPyFc4
v	v	k7c6
červenci	červenec	k1gInSc6
roku	rok	k1gInSc2
1930	#num#	k4
na	na	k7c6
inspekční	inspekční	k2eAgFnSc6d1
cestě	cesta	k1gFnSc6
nachladil	nachladit	k5eAaPmAgMnS
a	a	k8xC
4	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
zemřel	zemřít	k5eAaPmAgMnS
na	na	k7c4
zápal	zápal	k1gInSc4
plic	plíce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
vyvolalo	vyvolat	k5eAaPmAgNnS
podezření	podezření	k1gNnPc4
u	u	k7c2
mnoha	mnoho	k4c2
jeho	jeho	k3xOp3gMnPc2
současníků	současník	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
bylo	být	k5eAaImAgNnS
posíleno	posílit	k5eAaPmNgNnS
rychlým	rychlý	k2eAgInSc7d1
pohřbem	pohřeb	k1gInSc7
(	(	kIx(
<g/>
kremací	kremace	k1gFnSc7
<g/>
)	)	kIx)
následujícího	následující	k2eAgInSc2d1
dne	den	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Další	další	k2eAgInSc1d1
osud	osud	k1gInSc1
Arseňjevovy	Arseňjevův	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
byl	být	k5eAaImAgInS
tragický	tragický	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
vdova	vdova	k1gFnSc1
Margarita	Margarita	k1gFnSc1
Nikolajevna	Nikolajevna	k1gFnSc1
byla	být	k5eAaImAgFnS
roku	rok	k1gInSc2
1938	#num#	k4
na	na	k7c6
základě	základ	k1gInSc6
falešných	falešný	k2eAgNnPc2d1
obvinění	obvinění	k1gNnPc2
ze	z	k7c2
špionáže	špionáž	k1gFnSc2
zastřelena	zastřelen	k2eAgFnSc1d1
(	(	kIx(
<g/>
roku	rok	k1gInSc2
1958	#num#	k4
byla	být	k5eAaImAgFnS
rehabilitována	rehabilitován	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dcera	dcera	k1gFnSc1
byla	být	k5eAaImAgFnS
odsouzena	odsouzet	k5eAaImNgFnS,k5eAaPmNgFnS
na	na	k7c4
deset	deset	k4xCc4
let	léto	k1gNnPc2
do	do	k7c2
gulagu	gulag	k1gInSc2
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
–	–	k?
<g/>
1951	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
její	její	k3xOp3gMnSc1
bratr	bratr	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgInS
zatčen	zatknout	k5eAaPmNgMnS
v	v	k7c6
roce	rok	k1gInSc6
1937	#num#	k4
<g/>
,	,	kIx,
zmizel	zmizet	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Arseňjev	Arseňjet	k5eAaPmDgInS
se	se	k3xPyFc4
zasloužil	zasloužit	k5eAaPmAgMnS
o	o	k7c4
vznik	vznik	k1gInSc4
první	první	k4xOgFnSc2
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
na	na	k7c6
Dálném	dálný	k2eAgInSc6d1
východě	východ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
etnografického	etnografický	k2eAgInSc2d1
a	a	k8xC
geografického	geografický	k2eAgInSc2d1
výzkumu	výzkum	k1gInSc2
se	se	k3xPyFc4
zabýval	zabývat	k5eAaImAgMnS
také	také	k9
studiem	studio	k1gNnSc7
zvířat	zvíře	k1gNnPc2
<g/>
,	,	kIx,
ptáků	pták	k1gMnPc2
<g/>
,	,	kIx,
ryb	ryba	k1gFnPc2
i	i	k8xC
rostlin	rostlina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
zapojený	zapojený	k2eAgMnSc1d1
do	do	k7c2
obnovy	obnova	k1gFnSc2
rybolovu	rybolov	k1gInSc2
a	a	k8xC
živočišné	živočišný	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
na	na	k7c6
Komandorských	Komandorský	k2eAgInPc6d1
ostrovech	ostrov	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prováděl	provádět	k5eAaImAgMnS
rozsáhlou	rozsáhlý	k2eAgFnSc4d1
organizační	organizační	k2eAgFnSc4d1
a	a	k8xC
pedagogickou	pedagogický	k2eAgFnSc4d1
práci	práce	k1gFnSc4
ve	v	k7c6
vyšších	vysoký	k2eAgFnPc6d2
vzdělávacích	vzdělávací	k2eAgFnPc6d1
institucích	instituce	k1gFnPc6
na	na	k7c6
Dálném	dálný	k2eAgInSc6d1
východě	východ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
svých	svůj	k3xOyFgInPc6
dílech	díl	k1gInPc6
podal	podat	k5eAaPmAgMnS
poetické	poetický	k2eAgNnSc4d1
a	a	k8xC
zároveň	zároveň	k6eAd1
vědecky	vědecky	k6eAd1
přesné	přesný	k2eAgNnSc4d1
zobrazení	zobrazení	k1gNnSc4
života	život	k1gInSc2
v	v	k7c6
ussurijské	ussurijský	k2eAgFnSc6d1
tajze	tajga	k1gFnSc6
(	(	kIx(
<g/>
jako	jako	k8xC,k8xS
spisovatele	spisovatel	k1gMnSc4
jej	on	k3xPp3gMnSc4
objevil	objevit	k5eAaPmAgInS
a	a	k8xC
vysoce	vysoce	k6eAd1
ocenil	ocenit	k5eAaPmAgMnS
Maxim	Maxim	k1gMnSc1
Gorkij	Gorkij	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rukopis	rukopis	k1gInSc1
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
celoživotního	celoživotní	k2eAgNnSc2d1
díla	dílo	k1gNnSc2
С	С	k?
У	У	k?
(	(	kIx(
<g/>
Země	zem	k1gFnPc1
Udegejců	Udegejec	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
pracoval	pracovat	k5eAaImAgMnS
dvacet	dvacet	k4xCc4
sedm	sedm	k4xCc4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
ale	ale	k9
po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
ztratil	ztratit	k5eAaPmAgMnS
a	a	k8xC
doposud	doposud	k6eAd1
nebyl	být	k5eNaImAgInS
nalezen	naleznout	k5eAaPmNgInS,k5eAaBmNgInS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c4
Arseňjevovu	Arseňjevův	k2eAgFnSc4d1
počest	počest	k1gFnSc4
bylo	být	k5eAaImAgNnS
roku	rok	k1gInSc2
1952	#num#	k4
přejmenováno	přejmenovat	k5eAaPmNgNnS
město	město	k1gNnSc1
Semjonovka	Semjonovka	k1gFnSc1
na	na	k7c6
Arseňjev	Arseňjev	k1gFnSc6
a	a	k8xC
roku	rok	k1gInSc2
1972	#num#	k4
řeka	řeka	k1gFnSc1
Daubiche	Daubich	k1gFnSc2
na	na	k7c4
Arseňjevku	Arseňjevka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
po	po	k7c6
něm	on	k3xPp3gInSc6
pojmenován	pojmenovat	k5eAaPmNgInS
ledovec	ledovec	k1gInSc1
na	na	k7c4
na	na	k7c6
severním	severní	k2eAgInSc6d1
svahu	svah	k1gInSc6
Avačinské	Avačinský	k2eAgFnSc2d1
sopky	sopka	k1gFnSc2
a	a	k8xC
jeden	jeden	k4xCgInSc1
z	z	k7c2
vrcholů	vrchol	k1gInPc2
v	v	k7c6
pohoří	pohoří	k1gNnSc6
Sichote-Aliň	Sichote-Aliň	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jeho	jeho	k3xOp3gInSc6
domě	dům	k1gInSc6
ve	v	k7c6
Vladivostoku	Vladivostok	k1gInSc6
bylo	být	k5eAaImAgNnS
otevřeno	otevřít	k5eAaPmNgNnS
muzeum	muzeum	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
К	К	k?
в	в	k?
и	и	k?
в	в	k?
о	о	k?
У	У	k?
к	к	k?
(	(	kIx(
<g/>
1911	#num#	k4
<g/>
,	,	kIx,
Krátká	krátký	k2eAgFnSc1d1
vojensko-geografická	vojensko-geografický	k2eAgFnSc1d1
a	a	k8xC
vojensko-statistická	vojensko-statistický	k2eAgFnSc1d1
studie	studie	k1gFnSc1
Ussurijského	Ussurijský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
К	К	k?
в	в	k?
У	У	k?
к	к	k?
(	(	kIx(
<g/>
1914	#num#	k4
<g/>
,	,	kIx,
Číňané	Číňan	k1gMnPc1
v	v	k7c6
Ussurijském	Ussurijský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
charakteristika	charakteristika	k1gFnSc1
čínské	čínský	k2eAgFnSc2d1
populace	populace	k1gFnSc2
v	v	k7c6
Ussurijském	Ussurijský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
včetně	včetně	k7c2
geografických	geografický	k2eAgInPc2d1
<g/>
,	,	kIx,
etnografických	etnografický	k2eAgInPc2d1
a	a	k8xC
historických	historický	k2eAgInPc2d1
údajů	údaj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Э	Э	k?
п	п	k?
н	н	k?
В	В	k?
С	С	k?
(	(	kIx(
<g/>
1916	#num#	k4
<g/>
,	,	kIx,
Etnologické	etnologický	k2eAgInPc4d1
problémy	problém	k1gInPc4
na	na	k7c6
východě	východ	k1gInSc6
Sibiře	Sibiř	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
П	П	k?
У	У	k?
к	к	k?
(	(	kIx(
<g/>
1921	#num#	k4
<g/>
,	,	kIx,
Ussurijským	Ussurijský	k2eAgInSc7d1
krajem	kraj	k1gInSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zážitky	zážitek	k1gInPc4
a	a	k8xC
poznámky	poznámka	k1gFnPc4
z	z	k7c2
výprav	výprava	k1gFnPc2
v	v	k7c6
letech	let	k1gInPc6
1902	#num#	k4
<g/>
–	–	k?
<g/>
1906	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
kniha	kniha	k1gFnSc1
je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
za	za	k7c4
první	první	k4xOgInSc4
díl	díl	k1gInSc4
vyprávění	vyprávění	k1gNnSc4
o	o	k7c6
domorodém	domorodý	k2eAgMnSc6d1
stopaři	stopař	k1gMnSc6
Děrsu	Děrs	k1gMnSc6
Uzala	Uzala	k1gFnSc1
<g/>
,	,	kIx,
členovi	člen	k1gMnSc6
kmene	kmen	k1gInSc2
Goldů	Gold	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Д	Д	k?
У	У	k?
(	(	kIx(
<g/>
1923	#num#	k4
<g/>
,	,	kIx,
Děrsu	Děrsa	k1gFnSc4
Uzala	Uzalo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nejznámější	známý	k2eAgFnSc1d3
Arseňjevovo	Arseňjevův	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
vyprávějící	vyprávějící	k2eAgNnSc4d1
o	o	k7c6
dalších	další	k2eAgInPc6d1
osudech	osud	k1gInPc6
stopaře	stopař	k1gMnSc4
Děrsu	Děrsa	k1gFnSc4
Uzala	Uzala	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
И	И	k?
ж	ж	k?
в	в	k?
У	У	k?
к	к	k?
(	(	kIx(
<g/>
1925	#num#	k4
<g/>
,	,	kIx,
Hledači	hledač	k1gInPc7
žeňšenu	žeňšen	k2eAgFnSc4d1
v	v	k7c6
Ussurijském	Ussurijský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kniha	kniha	k1gFnSc1
o	o	k7c6
ženšenu	ženšen	k1gInSc6
včetně	včetně	k7c2
pověstí	pověst	k1gFnPc2
o	o	k7c6
jeho	jeho	k3xOp3gInSc6
původu	původ	k1gInSc6
a	a	k8xC
o	o	k7c6
jeho	jeho	k3xOp3gInPc6
čínských	čínský	k2eAgInPc6d1
hledačích	hledač	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
В	В	k?
к	к	k?
в	в	k?
(	(	kIx(
<g/>
1925	#num#	k4
<g/>
,	,	kIx,
V	v	k7c6
kráteru	kráter	k1gInSc6
sopky	sopka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
popis	popis	k1gInSc4
Avačinské	Avačinský	k2eAgFnSc2d1
sopky	sopka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Д	Д	k?
х	х	k?
<g/>
:	:	kIx,
о	о	k?
н	н	k?
с	с	k?
в	в	k?
У	У	k?
к	к	k?
(	(	kIx(
<g/>
1925	#num#	k4
<g/>
,	,	kIx,
Vzácná	vzácný	k2eAgFnSc1d1
šelma	šelma	k1gFnSc1
<g/>
:	:	kIx,
lov	lov	k1gInSc1
na	na	k7c4
sobola	sobol	k1gMnSc4
v	v	k7c6
Ussuriském	Ussuriský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
В	В	k?
д	д	k?
У	У	k?
к	к	k?
(	(	kIx(
<g/>
1926	#num#	k4
<g/>
,	,	kIx,
V	v	k7c6
hlubinách	hlubina	k1gFnPc6
Ussurijského	Ussurijský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přepracované	přepracovaný	k2eAgNnSc1d1
společné	společný	k2eAgNnSc1d1
vydání	vydání	k1gNnSc1
knih	kniha	k1gFnPc2
Ussuriským	Ussuriský	k2eAgInSc7d1
krajem	kraj	k1gInSc7
a	a	k8xC
Děrsu	Děrs	k1gInSc2
Uzala	Uzalo	k1gNnSc2
<g/>
,	,	kIx,
česky	česky	k6eAd1
jako	jako	k8xS,k8xC
Horký	horký	k2eAgInSc1d1
dech	dech	k1gInSc1
tajgy	tajga	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Л	Л	k?
л	л	k?
–	–	k?
у	у	k?
(	(	kIx(
<g/>
1926	#num#	k4
<g/>
,	,	kIx,
Lesní	lesní	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
–	–	k?
Udegejci	Udegejec	k1gMnPc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
etnografická	etnografický	k2eAgFnSc1d1
studie	studie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Т	Т	k?
м	м	k?
(	(	kIx(
<g/>
1927	#num#	k4
<g/>
,	,	kIx,
Mrož	mrož	k1gMnSc1
pacifický	pacifický	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
studie	studie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Б	Б	k?
и	и	k?
х	х	k?
н	н	k?
У	У	k?
к	к	k?
(	(	kIx(
<g/>
1928	#num#	k4
<g/>
,	,	kIx,
Život	život	k1gInSc1
a	a	k8xC
povaha	povaha	k1gFnSc1
národů	národ	k1gInPc2
Ussurijského	Ussurijský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
С	С	k?
т	т	k?
(	(	kIx(
<g/>
1930	#num#	k4
<g/>
,	,	kIx,
Tajgou	tajga	k1gFnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
popis	popis	k1gInSc1
expedice	expedice	k1gFnSc1
Sovětskaja	Sovětskaja	k1gMnSc1
Gavaň-Chabarovsk	Gavaň-Chabarovsk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
В	В	k?
г	г	k?
С	С	k?
(	(	kIx(
<g/>
1937	#num#	k4
<g/>
,	,	kIx,
V	v	k7c6
horách	hora	k1gFnPc6
Sichote-Aliň	Sichote-Aliň	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
posmrtně	posmrtně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Filmové	filmový	k2eAgFnPc1d1
adaptace	adaptace	k1gFnPc1
</s>
<s>
Д	Д	k?
У	У	k?
(	(	kIx(
<g/>
1961	#num#	k4
<g/>
,	,	kIx,
Děrsu	Děrsa	k1gFnSc4
Uzala	Uzalo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ruský	ruský	k2eAgInSc1d1
sovětský	sovětský	k2eAgInSc1d1
film	film	k1gInSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Agasi	Agase	k1gFnSc4
Babajan	Babajany	k1gInPc2
</s>
<s>
Д	Д	k?
У	У	k?
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
,	,	kIx,
Děrsu	Děrsa	k1gFnSc4
Uzala	Uzalo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ruský	ruský	k2eAgInSc1d1
sovětský	sovětský	k2eAgInSc1d1
film	film	k1gInSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Akira	Akira	k1gFnSc1
Kurosawa	Kurosawa	k1gFnSc1
<g/>
,	,	kIx,
film	film	k1gInSc1
kromě	kromě	k7c2
dalších	další	k2eAgFnPc2d1
cen	cena	k1gFnPc2
získal	získat	k5eAaPmAgInS
Oscara	Oscar	k1gMnSc4
za	za	k7c4
nejlepší	dobrý	k2eAgInSc4d3
cizojazyčný	cizojazyčný	k2eAgInSc4d1
film	film	k1gInSc4
za	za	k7c4
rok	rok	k1gInSc4
1975	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Česká	český	k2eAgNnPc1d1
vydání	vydání	k1gNnPc1
</s>
<s>
Stopař	stopař	k1gMnSc1
Děrsu	Děrs	k1gInSc2
Uzala	Uzalo	k1gNnSc2
<g/>
,	,	kIx,
Státní	státní	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1934	#num#	k4
<g/>
,	,	kIx,
přeložil	přeložit	k5eAaPmAgMnS
Václav	Václav	k1gMnSc1
Valenta-Alfa	Valenta-Alf	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Horký	horký	k2eAgInSc1d1
dech	dech	k1gInSc1
tajgy	tajga	k1gFnSc2
<g/>
,	,	kIx,
Rudolf	Rudolf	k1gMnSc1
Škeřík	Škeřík	k1gMnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1941	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Tajgou	tajga	k1gFnSc7
<g/>
,	,	kIx,
Družstevní	družstevní	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1952	#num#	k4
<g/>
,	,	kIx,
přeložil	přeložit	k5eAaPmAgMnS
Petr	Petr	k1gMnSc1
Erml	Erml	k1gMnSc1
<g/>
,	,	kIx,
znovu	znovu	k6eAd1
Osvěta	osvěta	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1953	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Ussurijským	Ussurijský	k2eAgInSc7d1
krajem	kraj	k1gInSc7
<g/>
,	,	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1953	#num#	k4
<g/>
,	,	kIx,
přeložil	přeložit	k5eAaPmAgMnS
Jiří	Jiří	k1gMnSc1
Sixta	Sixta	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Děrsu	Děrsa	k1gFnSc4
Uzala	Uzalo	k1gNnSc2
<g/>
,	,	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1955	#num#	k4
<g/>
,	,	kIx,
přeložil	přeložit	k5eAaPmAgMnS
Jiří	Jiří	k1gMnSc1
Sixta	Sixta	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Stopař	stopař	k1gMnSc1
Děrsu	Děrs	k1gInSc2
Uzala	Uzala	k1gMnSc1
SNDK	SNDK	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1959	#num#	k4
<g/>
,	,	kIx,
přeložil	přeložit	k5eAaPmAgMnS
Jiří	Jiří	k1gMnSc1
Sixta	Sixta	k1gMnSc1
<g/>
,	,	kIx,
znovu	znovu	k6eAd1
1961	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Děrsu	Děrsa	k1gFnSc4
Uzala	Uzalo	k1gNnSc2
<g/>
,	,	kIx,
Triton	triton	k1gMnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2020	#num#	k4
<g/>
,	,	kIx,
přeložil	přeložit	k5eAaPmAgMnS
Konstantin	Konstantin	k1gMnSc1
Šindelář	Šindelář	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Slovník	slovník	k1gInSc4
sovětských	sovětský	k2eAgMnPc2d1
spisovatelů	spisovatel	k1gMnPc2
I.	I.	kA
<g/>
,	,	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1978	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
1621	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
А	А	k?
В	В	k?
К	К	k?
<g/>
↑	↑	k?
А	А	k?
б	б	k?
ц	ц	k?
<g/>
↑	↑	k?
IMDB	IMDB	kA
-	-	kIx~
Dersu	Ders	k1gInSc2
Uzala	Uzala	k1gFnSc1
-	-	kIx~
Awards	Awards	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Vladimir	Vladimira	k1gFnPc2
Klavdijevič	Klavdijevič	k1gInSc4
Arseňjev	Arseňjev	k1gFnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
П	П	k?
У	У	k?
т	т	k?
<g/>
:	:	kIx,
А	А	k?
В	В	k?
К	К	k?
(	(	kIx(
<g/>
1872	#num#	k4
-	-	kIx~
1930	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
П	П	k?
и	и	k?
В	В	k?
К	К	k?
А	А	k?
</s>
<s>
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
П	П	k?
и	и	k?
т	т	k?
В	В	k?
А	А	k?
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
19990210063	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118848208	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2140	#num#	k4
5750	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
85001652	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
77113042	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
85001652	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Rusko	Rusko	k1gNnSc1
</s>
