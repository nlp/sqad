<p>
<s>
LAU	LAU	kA	LAU
je	být	k5eAaImIp3nS	být
zkratka	zkratka	k1gFnSc1	zkratka
anglického	anglický	k2eAgInSc2d1	anglický
Local	Local	k1gMnSc1	Local
administrative	administrativ	k1gInSc5	administrativ
unit	unit	k5eAaPmF	unit
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
místní	místní	k2eAgFnSc1d1	místní
správní	správní	k2eAgFnSc1d1	správní
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Označuje	označovat	k5eAaImIp3nS	označovat
úroveň	úroveň	k1gFnSc4	úroveň
územní	územní	k2eAgFnSc2d1	územní
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
níže	nízce	k6eAd2	nízce
než	než	k8xS	než
provincie	provincie	k1gFnPc1	provincie
<g/>
,	,	kIx,	,
kraje	kraj	k1gInPc1	kraj
a	a	k8xC	a
podobné	podobný	k2eAgFnPc1d1	podobná
velké	velká	k1gFnPc1	velká
správní	správní	k2eAgFnSc2d1	správní
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
(	(	kIx(	(
<g/>
a	a	k8xC	a
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
označují	označovat	k5eAaImIp3nP	označovat
jednotky	jednotka	k1gFnPc1	jednotka
doplňující	doplňující	k2eAgFnPc1d1	doplňující
na	na	k7c6	na
nižší	nízký	k2eAgFnSc6d2	nižší
úrovni	úroveň	k1gFnSc6	úroveň
statistickou	statistický	k2eAgFnSc4d1	statistická
soustavu	soustava	k1gFnSc4	soustava
NUTS	NUTS	kA	NUTS
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
přitom	přitom	k6eAd1	přitom
dva	dva	k4xCgInPc4	dva
stupně	stupeň	k1gInPc4	stupeň
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
LAU	LAU	kA	LAU
1	[number]	k4	1
<g/>
:	:	kIx,	:
dříve	dříve	k6eAd2	dříve
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k8xS	jako
NUTS	NUTS	kA	NUTS
4	[number]	k4	4
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
územní	územní	k2eAgFnPc4d1	územní
jednotky	jednotka	k1gFnPc4	jednotka
zahrnující	zahrnující	k2eAgFnPc4d1	zahrnující
řádově	řádově	k6eAd1	řádově
desítky	desítka	k1gFnPc4	desítka
či	či	k8xC	či
stovky	stovka	k1gFnPc4	stovka
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
užívány	užíván	k2eAgInPc1d1	užíván
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
Estonsku	Estonsko	k1gNnSc6	Estonsko
<g/>
,	,	kIx,	,
Finsku	Finsko	k1gNnSc6	Finsko
<g/>
,	,	kIx,	,
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
,	,	kIx,	,
Kypru	Kypr	k1gInSc6	Kypr
<g/>
,	,	kIx,	,
Litvě	Litva	k1gFnSc6	Litva
<g/>
,	,	kIx,	,
Lotyšsku	Lotyšsko	k1gNnSc6	Lotyšsko
<g/>
,	,	kIx,	,
Lucembursku	Lucembursko	k1gNnSc6	Lucembursko
<g/>
,	,	kIx,	,
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
,	,	kIx,	,
Maltě	Malta	k1gFnSc6	Malta
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
<g/>
,	,	kIx,	,
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
,	,	kIx,	,
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
a	a	k8xC	a
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
jich	on	k3xPp3gMnPc2	on
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
EU	EU	kA	EU
bylo	být	k5eAaImAgNnS	být
3334	[number]	k4	3334
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LAU	LAU	kA	LAU
2	[number]	k4	2
<g/>
:	:	kIx,	:
dříve	dříve	k6eAd2	dříve
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k8xS	jako
NUTS	NUTS	kA	NUTS
5	[number]	k4	5
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
obce	obec	k1gFnSc2	obec
a	a	k8xC	a
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
též	též	k9	též
o	o	k7c6	o
obcím	obec	k1gFnPc3	obec
nepodléhající	podléhající	k2eNgFnSc4d1	nepodléhající
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
EU	EU	kA	EU
bylo	být	k5eAaImAgNnS	být
112	[number]	k4	112
119	[number]	k4	119
jednotek	jednotka	k1gFnPc2	jednotka
LAU	LAU	kA	LAU
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
tabulce	tabulka	k1gFnSc6	tabulka
jsou	být	k5eAaImIp3nP	být
pojmenování	pojmenování	k1gNnSc4	pojmenování
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
jednotek	jednotka	k1gFnPc2	jednotka
soustavy	soustava	k1gFnSc2	soustava
LAU	LAU	kA	LAU
v	v	k7c6	v
úředním	úřední	k2eAgInSc6d1	úřední
jazyce	jazyk	k1gInSc6	jazyk
daného	daný	k2eAgInSc2d1	daný
státu	stát	k1gInSc2	stát
EU	EU	kA	EU
(	(	kIx(	(
<g/>
a	a	k8xC	a
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
obcí	obec	k1gFnPc2	obec
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
podle	podle	k7c2	podle
NUTS	NUTS	kA	NUTS
<g/>
/	/	kIx~	/
<g/>
LAU	LAU	kA	LAU
</s>
</p>
