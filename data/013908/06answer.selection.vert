<s desamb="1">
Ostrov	ostrov	k1gInSc1
byl	být	k5eAaImAgInS
pojmenován	pojmenovat	k5eAaPmNgInS
Elephant	Elephant	k1gInSc1
Island	Island	k1gInSc1
<g/>
,	,	kIx,
protože	protože	k8xS
první	první	k4xOgMnPc4
průzkumníky	průzkumník	k1gMnPc4
napadlo	napadnout	k5eAaPmAgNnS
ostrov	ostrov	k1gInSc4
pojmenovat	pojmenovat	k5eAaPmF
po	po	k7c6
rypouších	rypouš	k1gMnPc6
sloních	sloní	k2eAgInPc2d1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Elephant	Elephant	k1gMnSc1
seal	seal	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1
se	se	k3xPyFc4
tam	tam	k6eAd1
vyvalovali	vyvalovat	k5eAaImAgMnP
na	na	k7c6
břehu	břeh	k1gInSc6
<g/>
,	,	kIx,
když	když	k8xS
připluli	připlout	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>