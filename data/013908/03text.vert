<s>
Sloní	sloní	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
</s>
<s>
Sloní	sloní	k2eAgInSc1d1
ostrovElephant	ostrovElephant	k1gInSc1
Island	Island	k1gInSc1
(	(	kIx(
<g/>
Angličtina	angličtina	k1gFnSc1
<g/>
)	)	kIx)
Satelitní	satelitní	k2eAgInSc1d1
snímek	snímek	k1gInSc1
</s>
<s>
Stát	stát	k1gInSc1
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Topografie	topografie	k1gFnSc1
Rozloha	rozloha	k1gFnSc1
</s>
<s>
558	#num#	k4
km²	km²	k?
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
61	#num#	k4
<g/>
°	°	k?
<g/>
7	#num#	k4
<g/>
′	′	k?
<g/>
48	#num#	k4
<g/>
″	″	k?
j.	j.	k?
š.	š.	k?
<g/>
,	,	kIx,
55	#num#	k4
<g/>
°	°	k?
<g/>
1	#num#	k4
<g/>
′	′	k?
<g/>
12	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Délka	délka	k1gFnSc1
</s>
<s>
40.6	40.6	k4
km	km	kA
Šířka	šířka	k1gFnSc1
</s>
<s>
22.8	22.8	k4
km	km	kA
Nejvyšší	vysoký	k2eAgInSc4d3
vrchol	vrchol	k1gInSc4
</s>
<s>
Pardo	Pardo	k1gNnSc1
Ridge	Ridg	k1gFnSc2
(	(	kIx(
<g/>
853	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
Osídlení	osídlení	k1gNnSc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
trvalých	trvalý	k2eAgInPc2d1
0	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Sloní	sloní	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
je	být	k5eAaImIp3nS
jeden	jeden	k4xCgInSc1
z	z	k7c2
Jižních	jižní	k2eAgInPc2d1
Shetlandských	Shetlandský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
(	(	kIx(
<g/>
nejsevernějších	severní	k2eAgInPc2d3
ostrovů	ostrov	k1gInPc2
Antarktidy	Antarktida	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostrov	ostrov	k1gInSc1
byl	být	k5eAaImAgInS
pojmenován	pojmenovat	k5eAaPmNgInS
Elephant	Elephant	k1gInSc1
Island	Island	k1gInSc1
<g/>
,	,	kIx,
protože	protože	k8xS
první	první	k4xOgMnPc4
průzkumníky	průzkumník	k1gMnPc4
napadlo	napadnout	k5eAaPmAgNnS
ostrov	ostrov	k1gInSc4
pojmenovat	pojmenovat	k5eAaPmF
po	po	k7c6
rypouších	rypouš	k1gMnPc6
sloních	sloní	k2eAgInPc2d1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Elephant	Elephant	k1gMnSc1
seal	seal	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1
se	se	k3xPyFc4
tam	tam	k6eAd1
vyvalovali	vyvalovat	k5eAaImAgMnP
na	na	k7c6
břehu	břeh	k1gInSc6
<g/>
,	,	kIx,
když	když	k8xS
připluli	připlout	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostrov	ostrov	k1gInSc1
je	být	k5eAaImIp3nS
245	#num#	k4
km	km	kA
daleko	daleko	k6eAd1
od	od	k7c2
Antarktického	antarktický	k2eAgInSc2d1
poloostrova	poloostrov	k1gInSc2
a	a	k8xC
885	#num#	k4
km	km	kA
od	od	k7c2
nejjižnějšího	jižní	k2eAgNnSc2d3
místa	místo	k1gNnSc2
Chile	Chile	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
ostrov	ostrov	k1gInSc4
si	se	k3xPyFc3
dělají	dělat	k5eAaImIp3nP
nároky	nárok	k1gInPc4
Brazílie	Brazílie	k1gFnSc2
<g/>
,	,	kIx,
Argentina	Argentina	k1gFnSc1
<g/>
,	,	kIx,
Chile	Chile	k1gNnSc1
a	a	k8xC
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brazílie	Brazílie	k1gFnSc1
má	mít	k5eAaImIp3nS
na	na	k7c6
ostrově	ostrov	k1gInSc6
2	#num#	k4
základny	základna	k1gFnSc2
a	a	k8xC
v	v	k7c6
nich	on	k3xPp3gInPc6
je	být	k5eAaImIp3nS
až	až	k9
6	#num#	k4
průzkumníků	průzkumník	k1gMnPc2
najednou	najednou	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Ztroskotaná	ztroskotaný	k2eAgFnSc1d1
posádka	posádka	k1gFnSc1
na	na	k7c6
ostrově	ostrov	k1gInSc6
</s>
<s>
Ostrov	ostrov	k1gInSc1
náhodou	náhodou	k6eAd1
objevil	objevit	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1916	#num#	k4
Ernest	Ernest	k1gMnSc1
Henry	Henry	k1gMnSc1
Shackleton	Shackleton	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
když	když	k8xS
jeho	jeho	k3xOp3gFnSc1
loď	loď	k1gFnSc1
Endurance	Endurance	k1gFnSc2
ztroskotala	ztroskotat	k5eAaPmAgFnS
v	v	k7c6
Weddellově	Weddellův	k2eAgNnSc6d1
moři	moře	k1gNnSc6
<g/>
,	,	kIx,
on	on	k3xPp3gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
posádka	posádka	k1gFnSc1
vylezli	vylézt	k5eAaPmAgMnP
na	na	k7c4
ledovou	ledový	k2eAgFnSc4d1
kru	kra	k1gFnSc4
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yIgInPc4,k3yQgInPc4,k3yRgInPc4
společně	společně	k6eAd1
s	s	k7c7
dalšími	další	k2eAgNnPc7d1
28	#num#	k4
námořníky	námořník	k1gMnPc4
dopluli	doplout	k5eAaPmAgMnP
na	na	k7c4
tento	tento	k3xDgInSc4
ostrov	ostrov	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
místě	místo	k1gNnSc6
Point	pointa	k1gFnPc2
Wild	Wildo	k1gNnPc2
si	se	k3xPyFc3
založili	založit	k5eAaPmAgMnP
svůj	svůj	k3xOyFgInSc4
tábor	tábor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
táboře	tábor	k1gInSc6
ze	z	k7c2
dvou	dva	k4xCgInPc2
záchranných	záchranný	k2eAgInPc2d1
člunů	člun	k1gInPc2
postavili	postavit	k5eAaPmAgMnP
provizorní	provizorní	k2eAgInPc4d1
přístřešky	přístřešek	k1gInPc4
a	a	k8xC
dál	daleko	k6eAd2
postavili	postavit	k5eAaPmAgMnP
několik	několik	k4yIc4
plátěných	plátěný	k2eAgInPc2d1
stanů	stan	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
osvětlení	osvětlení	k1gNnSc3
používaly	používat	k5eAaImAgFnP
lampy	lampa	k1gFnPc1
na	na	k7c4
rybí	rybí	k2eAgInSc4d1
tuk	tuk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lovili	lovit	k5eAaImAgMnP
tučňáky	tučňák	k1gMnPc4
a	a	k8xC
tuleně	tuleň	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgInSc1d3
problém	problém	k1gInSc1
byl	být	k5eAaImAgInS
na	na	k7c4
podzim	podzim	k1gInSc4
a	a	k8xC
v	v	k7c6
zimě	zima	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
zvířat	zvíře	k1gNnPc2
k	k	k7c3
lovení	lovení	k1gNnSc3
bylo	být	k5eAaImAgNnS
nejméně	málo	k6eAd3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posádku	posádka	k1gFnSc4
sužovaly	sužovat	k5eAaImAgFnP
nemoce	nemoce	k1gFnPc1
<g/>
,	,	kIx,
omrzliny	omrzlina	k1gFnPc1
a	a	k8xC
hlad	hlad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ernest	Ernest	k1gMnSc1
Shackleton	Shackleton	k1gInSc4
si	se	k3xPyFc3
uvědomil	uvědomit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
těžko	těžko	k6eAd1
někdo	někdo	k3yInSc1
na	na	k7c6
ostrově	ostrov	k1gInSc6
nalezne	naleznout	k5eAaPmIp3nS,k5eAaBmIp3nS
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
popluje	poplout	k5eAaPmIp3nS
do	do	k7c2
nejbližší	blízký	k2eAgFnSc2d3
velrybářské	velrybářský	k2eAgFnSc2d1
vesnice	vesnice	k1gFnSc2
na	na	k7c6
Jižní	jižní	k2eAgFnSc6d1
Georgii	Georgie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společně	společně	k6eAd1
s	s	k7c7
dalšími	další	k2eAgMnPc7d1
pěti	pět	k4xCc7
muži	muž	k1gMnPc7
se	se	k3xPyFc4
plavil	plavit	k5eAaImAgInS
1287	#num#	k4
km	km	kA
daleko	daleko	k6eAd1
na	na	k7c6
záchranném	záchranný	k2eAgInSc6d1
člunu	člun	k1gInSc6
pojmenovaném	pojmenovaný	k2eAgInSc6d1
James	James	k1gMnSc1
Caird	Cairdo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
dva	dva	k4xCgInPc4
týdny	týden	k1gInPc4
(	(	kIx(
<g/>
24	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1916	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
konečně	konečně	k6eAd1
doplavili	doplavit	k5eAaPmAgMnP
do	do	k7c2
cíle	cíl	k1gInSc2
a	a	k8xC
uvědomili	uvědomit	k5eAaPmAgMnP
zde	zde	k6eAd1
o	o	k7c6
ztroskotané	ztroskotaný	k2eAgFnSc6d1
posádce	posádka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
byla	být	k5eAaImAgFnS
vyslána	vyslat	k5eAaPmNgFnS
záchranná	záchranný	k2eAgFnSc1d1
loď	loď	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
vyplula	vyplout	k5eAaPmAgFnS
z	z	k7c2
Punta	punto	k1gNnSc2
Arenas	Arenasa	k1gFnPc2
a	a	k8xC
velel	velet	k5eAaImAgMnS
na	na	k7c6
ní	on	k3xPp3gFnSc6
Luis	Luisa	k1gFnPc2
Pardo	Pardo	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
První	první	k4xOgInPc1
tři	tři	k4xCgInPc1
pokusy	pokus	k1gInPc1
nevyšly	vyjít	k5eNaPmAgInP
kvůli	kvůli	k7c3
příliš	příliš	k6eAd1
silnému	silný	k2eAgInSc3d1
ledu	led	k1gInSc3
okolo	okolo	k7c2
ostrova	ostrov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
na	na	k7c4
čtvrtý	čtvrtý	k4xOgInSc4
pokus	pokus	k1gInSc4
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
všechny	všechen	k3xTgFnPc4
odvézt	odvézt	k5eAaPmF
–	–	k?
30	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1916	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Flora	Flora	k1gFnSc1
a	a	k8xC
fauna	fauna	k1gFnSc1
</s>
<s>
Colobanthus	Colobanthus	k1gInSc1
quitensis	quitensis	k1gFnSc2
<g/>
,	,	kIx,
jedna	jeden	k4xCgFnSc1
ze	z	k7c2
zdejších	zdejší	k2eAgFnPc2d1
kvetoucích	kvetoucí	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
</s>
<s>
Na	na	k7c4
první	první	k4xOgInSc4
pohled	pohled	k1gInSc4
na	na	k7c6
ostrově	ostrov	k1gInSc6
neroste	růst	k5eNaImIp3nS
skoro	skoro	k6eAd1
nic	nic	k3yNnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
přestože	přestože	k8xS
je	být	k5eAaImIp3nS
naprostá	naprostý	k2eAgFnSc1d1
většina	většina	k1gFnSc1
ostrova	ostrov	k1gInSc2
pokrytá	pokrytý	k2eAgFnSc1d1
ledovcem	ledovec	k1gInSc7
roste	růst	k5eAaImIp3nS
na	na	k7c6
několika	několik	k4yIc6
místech	místo	k1gNnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
ledovec	ledovec	k1gInSc1
není	být	k5eNaImIp3nS
několik	několik	k4yIc4
druhů	druh	k1gInPc2
rostlin	rostlina	k1gFnPc2
<g/>
,	,	kIx,
dokonce	dokonce	k9
i	i	k9
ty	ten	k3xDgFnPc1
kvetoucí	kvetoucí	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rostou	růst	k5eAaImIp3nP
tu	tu	k6eAd1
mechy	mech	k1gInPc1
<g/>
,	,	kIx,
lišejníky	lišejník	k1gInPc1
<g/>
,	,	kIx,
játrovky	játrovka	k1gFnPc1
<g/>
,	,	kIx,
řasy	řasa	k1gFnPc1
a	a	k8xC
kvetoucí	kvetoucí	k2eAgFnPc1d1
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
jen	jen	k9
2	#num#	k4
druhy	druh	k1gInPc4
<g/>
,	,	kIx,
Colobanthus	Colobanthus	k1gInSc4
quitensis	quitensis	k1gFnSc2
a	a	k8xC
Deschampsia	Deschampsium	k1gNnSc2
antarctica	antarctic	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
expedici	expedice	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1970-1971	1970-1971	k4
bylo	být	k5eAaImAgNnS
na	na	k7c6
ostrově	ostrov	k1gInSc6
nalezeno	nalézt	k5eAaBmNgNnS,k5eAaPmNgNnS
celkově	celkově	k6eAd1
80	#num#	k4
druhů	druh	k1gInPc2
rostlin	rostlina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Žádná	žádný	k3yNgNnPc1
trvale	trvale	k6eAd1
žijící	žijící	k2eAgNnPc1d1
zvířata	zvíře	k1gNnPc1
ani	ani	k8xC
lidi	člověk	k1gMnPc4
ostrov	ostrov	k1gInSc4
nemá	mít	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
často	často	k6eAd1
ostrov	ostrov	k1gInSc4
navštěvují	navštěvovat	k5eAaImIp3nP
migrující	migrující	k2eAgMnPc1d1
tučňáci	tučňák	k1gMnPc1
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
tam	tam	k6eAd1
odchovávají	odchovávat	k5eAaImIp3nP
mladé	mladý	k2eAgFnPc1d1
<g/>
,	,	kIx,
tuleni	tuleň	k1gMnPc1
a	a	k8xC
samozřejmě	samozřejmě	k6eAd1
rypouší	rypoušit	k5eAaPmIp3nS
sloní	sloní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
okolním	okolní	k2eAgNnSc6d1
moři	moře	k1gNnSc6
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
velryby	velryba	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Podnebí	podnebí	k1gNnSc1
</s>
<s>
Počasí	počasí	k1gNnSc1
je	být	k5eAaImIp3nS
dlouhodobě	dlouhodobě	k6eAd1
oblačné	oblačný	k2eAgNnSc1d1
a	a	k8xC
často	často	k6eAd1
sněží	sněžit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podnebí	podnebí	k1gNnPc4
je	být	k5eAaImIp3nS
silně	silně	k6eAd1
oceánské	oceánský	k2eAgNnSc1d1
a	a	k8xC
polární	polární	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrná	průměrný	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
v	v	k7c6
Červenci	červenec	k1gInSc6
je	být	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
-6	-6	k4
<g/>
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
nejnižší	nízký	k2eAgFnPc1d3
zimní	zimní	k2eAgFnPc1d1
teploty	teplota	k1gFnPc1
klesají	klesat	k5eAaImIp3nP
až	až	k9
na	na	k7c4
-20	-20	k4
°	°	k?
<g/>
C.	C.	kA
Průměrná	průměrný	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
v	v	k7c6
Lednu	leden	k1gInSc6
je	být	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
3	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
v	v	k7c6
nejteplejších	teplý	k2eAgInPc6d3
letních	letní	k2eAgInPc6d1
dnech	den	k1gInPc6
můžou	můžou	k?
teploty	teplota	k1gFnPc1
dosáhnout	dosáhnout	k5eAaPmF
až	až	k6eAd1
10	#num#	k4
°	°	k?
<g/>
C.	C.	kA
Sněžení	sněžení	k1gNnSc1
je	být	k5eAaImIp3nS
obvyklé	obvyklý	k2eAgNnSc1d1
i	i	k9
v	v	k7c6
létě	léto	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naprostou	naprostý	k2eAgFnSc4d1
většinu	většina	k1gFnSc4
ostrova	ostrov	k1gInSc2
pokrývá	pokrývat	k5eAaImIp3nS
ledovec	ledovec	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Pamětihodnosti	pamětihodnost	k1gFnPc1
</s>
<s>
V	v	k7c6
Point	pointa	k1gFnPc2
Wild	Wild	k1gInSc4
stojí	stát	k5eAaImIp3nS
památník	památník	k1gInSc1
s	s	k7c7
několika	několik	k4yIc7
plaketami	plaketa	k1gFnPc7
<g/>
,	,	kIx,
věnovaný	věnovaný	k2eAgInSc1d1
Luisovi	Luis	k1gMnSc6
Pardovi	pard	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jihozápadě	jihozápad	k1gInSc6
ostrova	ostrov	k1gInSc2
jsou	být	k5eAaImIp3nP
trosky	troska	k1gFnPc1
jedné	jeden	k4xCgFnSc2
velké	velká	k1gFnSc2
dřevěné	dřevěný	k2eAgFnSc2d1
plachetnice	plachetnice	k1gFnSc2
a	a	k8xC
byly	být	k5eAaImAgFnP
vyhlášeny	vyhlásit	k5eAaPmNgFnP
antarktickou	antarktický	k2eAgFnSc7d1
historickou	historický	k2eAgFnSc7d1
památkou	památka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Ernest	Ernest	k1gMnSc1
Shackleton	Shackleton	k1gInSc4
odplouvá	odplouvat	k5eAaImIp3nS
ze	z	k7c2
Sloního	sloní	k2eAgInSc2d1
ostrova	ostrov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Cesta	cesta	k1gFnSc1
posádky	posádka	k1gFnSc2
po	po	k7c4
potopení	potopení	k1gNnSc4
Endurance	Endurance	k1gFnSc2
<g/>
,	,	kIx,
zeleně	zeleně	k6eAd1
je	být	k5eAaImIp3nS
část	část	k1gFnSc4
plavby	plavba	k1gFnSc2
na	na	k7c6
ledové	ledový	k2eAgFnSc6d1
kře	kra	k1gFnSc6
a	a	k8xC
modře	modro	k6eAd1
plavba	plavba	k1gFnSc1
na	na	k7c6
záchranném	záchranný	k2eAgInSc6d1
člunu	člun	k1gInSc6
James	James	k1gMnSc1
Caird	Caird	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Tučňáci	tučňák	k1gMnPc1
na	na	k7c6
pláži	pláž	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Památník	památník	k1gInSc1
Luisovi	Luis	k1gMnSc3
Pardovi	pard	k1gMnSc3
a	a	k8xC
ztroskotancům	ztroskotanec	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s>
Mlha	mlha	k1gFnSc1
na	na	k7c6
pobřežních	pobřežní	k2eAgFnPc6d1
skálách	skála	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Elephant	Elephant	k1gInSc1
Island	Island	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
http://www.vivabrazil.com/vivabrazil/proantar.htm	http://www.vivabrazil.com/vivabrazil/proantar.htm	k1gInSc1
<g/>
↑	↑	k?
http://www.amnh.org/exhibitions/shackleton/	http://www.amnh.org/exhibitions/shackleton/	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Sloní	sloní	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Rostlinstvo	rostlinstvo	k1gNnSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Přibližné	přibližný	k2eAgNnSc1d1
klima	klima	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
95007199	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
315527994	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
95007199	#num#	k4
</s>
