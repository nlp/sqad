<p>
<s>
Bismarck	Bismarck	k6eAd1	Bismarck
byla	být	k5eAaImAgFnS	být
německá	německý	k2eAgFnSc1d1	německá
bitevní	bitevní	k2eAgFnSc1d1	bitevní
loď	loď	k1gFnSc1	loď
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
po	po	k7c6	po
říšském	říšský	k2eAgMnSc6d1	říšský
kancléři	kancléř	k1gMnSc6	kancléř
Bismarckovi	Bismarcek	k1gMnSc6	Bismarcek
<g/>
.	.	kIx.	.
</s>
<s>
Svého	svůj	k3xOyFgInSc2	svůj
času	čas	k1gInSc2	čas
byla	být	k5eAaImAgFnS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
a	a	k8xC	a
nejmocnějších	mocný	k2eAgFnPc2d3	nejmocnější
bitevních	bitevní	k2eAgFnPc2d1	bitevní
lodí	loď	k1gFnPc2	loď
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
kapitánem	kapitán	k1gMnSc7	kapitán
byl	být	k5eAaImAgMnS	být
Kapitän	Kapitän	k1gMnSc1	Kapitän
zur	zur	k?	zur
See	See	k1gMnSc1	See
(	(	kIx(	(
<g/>
~	~	kIx~	~
námořní	námořní	k2eAgMnSc1d1	námořní
kapitán	kapitán	k1gMnSc1	kapitán
<g/>
)	)	kIx)	)
Ernst	Ernst	k1gMnSc1	Ernst
Lindemann	Lindemann	k1gMnSc1	Lindemann
a	a	k8xC	a
role	role	k1gFnSc1	role
kmotry	kmotra	k1gFnSc2	kmotra
se	se	k3xPyFc4	se
ujala	ujmout	k5eAaPmAgFnS	ujmout
Bismarckova	Bismarckův	k2eAgFnSc1d1	Bismarckova
vnučka	vnučka	k1gFnSc1	vnučka
Dorothea	Dorothe	k1gInSc2	Dorothe
von	von	k1gInSc4	von
Löwenfeld	Löwenfeldo	k1gNnPc2	Löwenfeldo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1941	[number]	k4	1941
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c4	na
svoji	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
a	a	k8xC	a
poslední	poslední	k2eAgFnSc4d1	poslední
plavbu	plavba	k1gFnSc4	plavba
do	do	k7c2	do
Atlantiku	Atlantik	k1gInSc2	Atlantik
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	nízce	k6eAd2	nízce
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
potopil	potopit	k5eAaPmAgInS	potopit
britský	britský	k2eAgInSc1d1	britský
bitevní	bitevní	k2eAgInSc1d1	bitevní
křižník	křižník	k1gInSc1	křižník
HMS	HMS	kA	HMS
Hood	Hood	k1gInSc1	Hood
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několikadenním	několikadenní	k2eAgNnSc6d1	několikadenní
pronásledování	pronásledování	k1gNnSc6	pronásledování
byl	být	k5eAaImAgInS	být
vážně	vážně	k6eAd1	vážně
poškozen	poškodit	k5eAaPmNgInS	poškodit
torpédovými	torpédový	k2eAgInPc7d1	torpédový
bombardéry	bombardér	k1gInPc7	bombardér
Swordfish	Swordfisha	k1gFnPc2	Swordfisha
z	z	k7c2	z
letadlové	letadlový	k2eAgFnSc2d1	letadlová
lodě	loď	k1gFnSc2	loď
HMS	HMS	kA	HMS
Ark	Ark	k1gMnSc1	Ark
Royal	Royal	k1gMnSc1	Royal
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
potopen	potopit	k5eAaPmNgInS	potopit
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
po	po	k7c6	po
souboji	souboj	k1gInSc6	souboj
s	s	k7c7	s
bitevními	bitevní	k2eAgFnPc7d1	bitevní
loděmi	loď	k1gFnPc7	loď
HMS	HMS	kA	HMS
Rodney	Rodnea	k1gFnPc1	Rodnea
<g/>
,	,	kIx,	,
HMS	HMS	kA	HMS
King	King	k1gMnSc1	King
George	Georg	k1gMnSc2	Georg
V	V	kA	V
a	a	k8xC	a
dalšími	další	k2eAgNnPc7d1	další
menšími	malý	k2eAgNnPc7d2	menší
plavidly	plavidlo	k1gNnPc7	plavidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stavba	stavba	k1gFnSc1	stavba
lodi	loď	k1gFnSc2	loď
==	==	k?	==
</s>
</p>
<p>
<s>
Bitevní	bitevní	k2eAgFnSc1d1	bitevní
loď	loď	k1gFnSc1	loď
Bismarck	Bismarcka	k1gFnPc2	Bismarcka
byla	být	k5eAaImAgFnS	být
dílem	dílo	k1gNnSc7	dílo
hamburské	hamburský	k2eAgFnSc2d1	hamburská
firmy	firma	k1gFnSc2	firma
Blohm	Blohm	k1gMnSc1	Blohm
&	&	k?	&
Voss	Voss	k1gInSc1	Voss
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
byl	být	k5eAaImAgInS	být
konstruován	konstruován	k2eAgInSc1d1	konstruován
jako	jako	k8xC	jako
vlajková	vlajkový	k2eAgFnSc1d1	vlajková
loď	loď	k1gFnSc1	loď
německého	německý	k2eAgNnSc2d1	německé
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
byly	být	k5eAaImAgFnP	být
zahájeny	zahájen	k2eAgFnPc1d1	zahájena
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
spouštění	spouštění	k1gNnSc6	spouštění
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1939	[number]	k4	1939
byl	být	k5eAaImAgMnS	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
i	i	k9	i
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
toužil	toužit	k5eAaImAgMnS	toužit
po	po	k7c4	po
ovládnutí	ovládnutí	k1gNnSc4	ovládnutí
Atlantiku	Atlantik	k1gInSc2	Atlantik
a	a	k8xC	a
zničení	zničení	k1gNnSc2	zničení
spojeneckých	spojenecký	k2eAgInPc2d1	spojenecký
zásobovacích	zásobovací	k2eAgInPc2d1	zásobovací
konvojů	konvoj	k1gInPc2	konvoj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bismarck	Bismarck	k1gMnSc1	Bismarck
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
výzbroje	výzbroj	k1gFnSc2	výzbroj
německé	německý	k2eAgFnSc2d1	německá
Kriegsmarine	Kriegsmarin	k1gInSc5	Kriegsmarin
zařazen	zařadit	k5eAaPmNgInS	zařadit
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
stavbu	stavba	k1gFnSc4	stavba
se	se	k3xPyFc4	se
vyšplhaly	vyšplhat	k5eAaPmAgInP	vyšplhat
téměř	téměř	k6eAd1	téměř
na	na	k7c4	na
dvě	dva	k4xCgNnPc4	dva
stě	sto	k4xCgFnPc1	sto
milionů	milion	k4xCgInPc2	milion
říšských	říšský	k2eAgFnPc2d1	říšská
marek	marka	k1gFnPc2	marka
<g/>
.	.	kIx.	.
</s>
<s>
Posádku	posádka	k1gFnSc4	posádka
lodi	loď	k1gFnSc2	loď
tvořilo	tvořit	k5eAaImAgNnS	tvořit
zhruba	zhruba	k6eAd1	zhruba
2000	[number]	k4	2000
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sesterskou	sesterský	k2eAgFnSc7d1	sesterská
lodí	loď	k1gFnSc7	loď
Bismarcka	Bismarcko	k1gNnSc2	Bismarcko
byla	být	k5eAaImAgFnS	být
bitevní	bitevní	k2eAgFnSc1d1	bitevní
loď	loď	k1gFnSc1	loď
Tirpitz	Tirpitza	k1gFnPc2	Tirpitza
o	o	k7c6	o
výtlaku	výtlak	k1gInSc6	výtlak
asi	asi	k9	asi
50	[number]	k4	50
000	[number]	k4	000
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
německá	německý	k2eAgFnSc1d1	německá
bitevní	bitevní	k2eAgFnSc1d1	bitevní
loď	loď	k1gFnSc1	loď
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
udávaný	udávaný	k2eAgInSc1d1	udávaný
výtlak	výtlak	k1gInSc1	výtlak
Bismarcka	Bismarcko	k1gNnSc2	Bismarcko
i	i	k8xC	i
Tirpitze	Tirpitze	k1gFnSc2	Tirpitze
činil	činit	k5eAaImAgInS	činit
35	[number]	k4	35
000	[number]	k4	000
tun	tuna	k1gFnPc2	tuna
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
maximální	maximální	k2eAgInSc1d1	maximální
povolený	povolený	k2eAgInSc1d1	povolený
výtlak	výtlak	k1gInSc1	výtlak
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
německo-britskou	německoritský	k2eAgFnSc7d1	německo-britská
dohodou	dohoda	k1gFnSc7	dohoda
z	z	k7c2	z
28	[number]	k4	28
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Operace	operace	k1gFnSc1	operace
Rheinübung	Rheinübung	k1gMnSc1	Rheinübung
==	==	k?	==
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1941	[number]	k4	1941
vyplul	vyplout	k5eAaPmAgInS	vyplout
Bismarck	Bismarck	k1gInSc1	Bismarck
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
kapitána	kapitán	k1gMnSc2	kapitán
Lindenmanna	Lindenmann	k1gMnSc2	Lindenmann
z	z	k7c2	z
Gdyně	Gdyně	k1gFnSc2	Gdyně
poblíž	poblíž	k7c2	poblíž
Gdaňsku	Gdaňsk	k1gInSc2	Gdaňsk
a	a	k8xC	a
následoval	následovat	k5eAaImAgInS	následovat
tak	tak	k6eAd1	tak
těžký	těžký	k2eAgInSc1d1	těžký
křižník	křižník	k1gInSc1	křižník
Prinz	Prinz	k1gMnSc1	Prinz
Eugen	Eugno	k1gNnPc2	Eugno
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
Norska	Norsko	k1gNnSc2	Norsko
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgFnP	mít
lodě	loď	k1gFnPc1	loď
přesunout	přesunout	k5eAaPmF	přesunout
do	do	k7c2	do
Atlantiku	Atlantik	k1gInSc2	Atlantik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
Bismarcku	Bismarcka	k1gFnSc4	Bismarcka
vztyčil	vztyčit	k5eAaPmAgMnS	vztyčit
svou	svůj	k3xOyFgFnSc4	svůj
vlajku	vlajka	k1gFnSc4	vlajka
viceadmirál	viceadmirál	k1gMnSc1	viceadmirál
Günther	Günthra	k1gFnPc2	Günthra
Lütjens	Lütjens	k1gInSc1	Lütjens
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
pověřen	pověřen	k2eAgInSc1d1	pověřen
taktickým	taktický	k2eAgNnSc7d1	taktické
velením	velení	k1gNnSc7	velení
celého	celý	k2eAgNnSc2d1	celé
uskupení	uskupení	k1gNnSc2	uskupení
<g/>
.	.	kIx.	.
</s>
<s>
Operace	operace	k1gFnSc1	operace
dostala	dostat	k5eAaPmAgFnS	dostat
jméno	jméno	k1gNnSc4	jméno
'	'	kIx"	'
<g/>
Rheinübung	Rheinübung	k1gInSc1	Rheinübung
<g/>
'	'	kIx"	'
a	a	k8xC	a
jejím	její	k3xOp3gInSc7	její
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
proniknutí	proniknutí	k1gNnSc1	proniknutí
obou	dva	k4xCgFnPc2	dva
lodí	loď	k1gFnPc2	loď
do	do	k7c2	do
Atlantiku	Atlantik	k1gInSc2	Atlantik
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
napadání	napadání	k1gNnSc2	napadání
spojeneckých	spojenecký	k2eAgFnPc2d1	spojenecká
lodí	loď	k1gFnPc2	loď
a	a	k8xC	a
konvojů	konvoj	k1gInPc2	konvoj
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
v	v	k7c6	v
Atlantiku	Atlantik	k1gInSc6	Atlantik
rozmístěna	rozmístit	k5eAaPmNgFnS	rozmístit
řada	řada	k1gFnSc1	řada
zásobovacích	zásobovací	k2eAgFnPc2d1	zásobovací
a	a	k8xC	a
cisternových	cisternový	k2eAgFnPc2d1	cisternová
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mohly	moct	k5eAaImAgFnP	moct
zajistit	zajistit	k5eAaPmF	zajistit
obě	dva	k4xCgFnPc1	dva
lodi	loď	k1gFnPc1	loď
v	v	k7c6	v
bojových	bojový	k2eAgFnPc6d1	bojová
akcích	akce	k1gFnPc6	akce
téměř	téměř	k6eAd1	téměř
na	na	k7c4	na
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Začátek	začátek	k1gInSc1	začátek
operace	operace	k1gFnSc2	operace
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
stanoven	stanovit	k5eAaPmNgInS	stanovit
na	na	k7c4	na
duben	duben	k1gInSc4	duben
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
k	k	k7c3	k
Bismarcku	Bismarcko	k1gNnSc3	Bismarcko
připojí	připojit	k5eAaPmIp3nP	připojit
bitevní	bitevní	k2eAgInPc1d1	bitevní
křižníky	křižník	k1gInPc1	křižník
Scharnhorst	Scharnhorst	k1gFnSc4	Scharnhorst
a	a	k8xC	a
Gneisenau	Gneisenaa	k1gFnSc4	Gneisenaa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
však	však	k9	však
Gneisenau	Gneisenaus	k1gInSc3	Gneisenaus
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
škody	škoda	k1gFnPc4	škoda
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
britského	britský	k2eAgNnSc2d1	Britské
bombardování	bombardování	k1gNnSc2	bombardování
brestského	brestský	k2eAgInSc2d1	brestský
přístavu	přístav	k1gInSc2	přístav
a	a	k8xC	a
Scharnhorst	Scharnhorst	k1gInSc1	Scharnhorst
měl	mít	k5eAaImAgInS	mít
potíže	potíž	k1gFnPc4	potíž
s	s	k7c7	s
hnací	hnací	k2eAgFnSc7d1	hnací
soustavou	soustava	k1gFnSc7	soustava
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
německé	německý	k2eAgNnSc1d1	německé
velení	velení	k1gNnSc1	velení
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
v	v	k7c6	v
operaci	operace	k1gFnSc6	operace
nahradit	nahradit	k5eAaPmF	nahradit
oba	dva	k4xCgInPc4	dva
bitevní	bitevní	k2eAgInPc4d1	bitevní
křižníky	křižník	k1gInPc4	křižník
těžkým	těžký	k2eAgInSc7d1	těžký
křižníkem	křižník	k1gInSc7	křižník
Prinz	Prinza	k1gFnPc2	Prinza
Eugen	Eugna	k1gFnPc2	Eugna
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
úspěchům	úspěch	k1gInPc3	úspěch
akcí	akce	k1gFnPc2	akce
německých	německý	k2eAgFnPc2d1	německá
ponorek	ponorka	k1gFnPc2	ponorka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mohly	moct	k5eAaImAgFnP	moct
m.	m.	k?	m.
<g/>
j.	j.	k?	j.
využívat	využívat	k5eAaPmF	využívat
nově	nově	k6eAd1	nově
dobyté	dobytý	k2eAgFnPc4d1	dobytá
norské	norský	k2eAgFnPc4d1	norská
a	a	k8xC	a
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
získané	získaný	k2eAgInPc4d1	získaný
francouzské	francouzský	k2eAgInPc4d1	francouzský
přístavy	přístav	k1gInPc4	přístav
bylo	být	k5eAaImAgNnS	být
britské	britský	k2eAgNnSc1d1	Britské
loďstvo	loďstvo	k1gNnSc1	loďstvo
rozptýleno	rozptýlen	k2eAgNnSc1d1	rozptýleno
<g/>
,	,	kIx,	,
britské	britský	k2eAgFnSc6d1	britská
lodi	loď	k1gFnSc6	loď
se	se	k3xPyFc4	se
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
míře	míra	k1gFnSc6	míra
účastnily	účastnit	k5eAaImAgFnP	účastnit
také	také	k6eAd1	také
bojů	boj	k1gInPc2	boj
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
německé	německý	k2eAgNnSc1d1	německé
velení	velení	k1gNnSc1	velení
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
operaci	operace	k1gFnSc4	operace
odstartovat	odstartovat	k5eAaPmF	odstartovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velící	velící	k2eAgMnSc1d1	velící
důstojník	důstojník	k1gMnSc1	důstojník
celého	celý	k2eAgNnSc2d1	celé
uskupení	uskupení	k1gNnSc2	uskupení
<g/>
,	,	kIx,	,
viceadmirál	viceadmirál	k1gMnSc1	viceadmirál
Lütjens	Lütjensa	k1gFnPc2	Lütjensa
měl	mít	k5eAaImAgMnS	mít
sice	sice	k8xC	sice
obavy	obava	k1gFnPc4	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
bojová	bojový	k2eAgFnSc1d1	bojová
síla	síla	k1gFnSc1	síla
obou	dva	k4xCgFnPc2	dva
lodí	loď	k1gFnPc2	loď
nebude	být	k5eNaImBp3nS	být
proti	proti	k7c3	proti
silám	síla	k1gFnPc3	síla
britského	britský	k2eAgNnSc2d1	Britské
loďstva	loďstvo	k1gNnSc2	loďstvo
dostatečná	dostatečný	k2eAgFnSc1d1	dostatečná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
rozhovoru	rozhovor	k1gInSc6	rozhovor
s	s	k7c7	s
velkoadmirálem	velkoadmirál	k1gMnSc7	velkoadmirál
Raederem	Raeder	k1gMnSc7	Raeder
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
jasně	jasně	k6eAd1	jasně
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
operaci	operace	k1gFnSc4	operace
je	být	k5eAaImIp3nS	být
vhodná	vhodný	k2eAgFnSc1d1	vhodná
doba	doba	k1gFnSc1	doba
a	a	k8xC	a
Hitler	Hitler	k1gMnSc1	Hitler
očekává	očekávat	k5eAaImIp3nS	očekávat
od	od	k7c2	od
loďstva	loďstvo	k1gNnSc2	loďstvo
činy	čina	k1gFnSc2	čina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pronásledování	pronásledování	k1gNnSc3	pronásledování
Bismarcku	Bismarcko	k1gNnSc3	Bismarcko
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Přesun	přesun	k1gInSc1	přesun
lodí	loď	k1gFnPc2	loď
přes	přes	k7c4	přes
Norsko	Norsko	k1gNnSc4	Norsko
do	do	k7c2	do
Dánského	dánský	k2eAgInSc2d1	dánský
průlivu	průliv	k1gInSc2	průliv
===	===	k?	===
</s>
</p>
<p>
<s>
Prinz	Prinz	k1gInSc1	Prinz
Eugen	Eugna	k1gFnPc2	Eugna
vyplul	vyplout	k5eAaPmAgInS	vyplout
z	z	k7c2	z
Gdyně	Gdyně	k1gFnSc2	Gdyně
kolem	kolem	k7c2	kolem
21	[number]	k4	21
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
následován	následován	k2eAgInSc1d1	následován
Bismarckem	Bismarck	k1gInSc7	Bismarck
ve	v	k7c4	v
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
lodi	loď	k1gFnPc1	loď
pluly	plout	k5eAaImAgFnP	plout
provázeny	provázet	k5eAaImNgFnP	provázet
torpédoborci	torpédoborec	k1gInSc3	torpédoborec
separátně	separátně	k6eAd1	separátně
a	a	k8xC	a
setkaly	setkat	k5eAaPmAgFnP	setkat
se	se	k3xPyFc4	se
u	u	k7c2	u
mysu	mys	k1gInSc2	mys
Arkona	Arkon	k1gMnSc2	Arkon
nedaleko	nedaleko	k7c2	nedaleko
ostrova	ostrov	k1gInSc2	ostrov
Rujána	Rujána	k1gFnSc1	Rujána
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
místa	místo	k1gNnSc2	místo
setkání	setkání	k1gNnSc2	setkání
pak	pak	k8xC	pak
společně	společně	k6eAd1	společně
propluly	proplout	k5eAaPmAgFnP	proplout
přes	přes	k7c4	přes
Kattegat	Kattegat	k1gInSc4	Kattegat
a	a	k8xC	a
Skagerrak	Skagerrak	k1gInSc4	Skagerrak
do	do	k7c2	do
norských	norský	k2eAgFnPc2d1	norská
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
německé	německý	k2eAgNnSc1d1	německé
velení	velení	k1gNnSc1	velení
učinilo	učinit	k5eAaImAgNnS	učinit
řadu	řada	k1gFnSc4	řada
maskovacích	maskovací	k2eAgInPc2d1	maskovací
kroků	krok	k1gInPc2	krok
a	a	k8xC	a
německá	německý	k2eAgFnSc1d1	německá
Luftwaffe	Luftwaff	k1gInSc5	Luftwaff
bránila	bránit	k5eAaImAgFnS	bránit
leteckému	letecký	k2eAgInSc3d1	letecký
průzkumu	průzkum	k1gInSc3	průzkum
<g/>
,	,	kIx,	,
angličtí	anglický	k2eAgMnPc1d1	anglický
agenti	agent	k1gMnPc1	agent
v	v	k7c6	v
norském	norský	k2eAgNnSc6d1	norské
hnutí	hnutí	k1gNnSc6	hnutí
odporu	odpor	k1gInSc2	odpor
oznámili	oznámit	k5eAaPmAgMnP	oznámit
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
připlutí	připlutí	k1gNnSc2	připlutí
Bismarcka	Bismarcko	k1gNnSc2	Bismarcko
do	do	k7c2	do
Grimstadfjordu	Grimstadfjord	k1gInSc2	Grimstadfjord
poblíž	poblíž	k7c2	poblíž
Bergenu	Bergen	k1gInSc2	Bergen
ještě	ještě	k9	ještě
ten	ten	k3xDgInSc4	ten
samý	samý	k3xTgInSc4	samý
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Britové	Brit	k1gMnPc1	Brit
také	také	k9	také
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
zprávu	zpráva	k1gFnSc4	zpráva
švédského	švédský	k2eAgInSc2d1	švédský
křižníku	křižník	k1gInSc2	křižník
Gotland	Gotland	k1gInSc1	Gotland
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
německé	německý	k2eAgFnPc1d1	německá
lodi	loď	k1gFnPc1	loď
při	při	k7c6	při
opouštění	opouštění	k1gNnSc6	opouštění
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
spatřil	spatřit	k5eAaPmAgMnS	spatřit
a	a	k8xC	a
informoval	informovat	k5eAaBmAgMnS	informovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
velitelství	velitelství	k1gNnSc6	velitelství
švédského	švédský	k2eAgNnSc2d1	švédské
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přes	přes	k7c4	přes
úsilí	úsilí	k1gNnSc4	úsilí
německého	německý	k2eAgNnSc2d1	německé
letectva	letectvo	k1gNnSc2	letectvo
se	s	k7c7	s
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
pilotovi	pilot	k1gMnSc3	pilot
průzkumného	průzkumný	k2eAgInSc2d1	průzkumný
Spitfiru	Spitfir	k1gInSc2	Spitfir
RAF	raf	k0	raf
podařilo	podařit	k5eAaPmAgNnS	podařit
Bismarcka	Bismarcka	k1gFnSc1	Bismarcka
v	v	k7c6	v
Grimstafjordu	Grimstafjord	k1gInSc6	Grimstafjord
vyfotografovat	vyfotografovat	k5eAaPmF	vyfotografovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
doplnění	doplnění	k1gNnSc6	doplnění
paliva	palivo	k1gNnSc2	palivo
odpoledne	odpoledne	k6eAd1	odpoledne
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
kolem	kolem	k7c2	kolem
17	[number]	k4	17
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
však	však	k9	však
obě	dva	k4xCgFnPc1	dva
lodi	loď	k1gFnPc1	loď
zvedly	zvednout	k5eAaPmAgFnP	zvednout
kotvy	kotva	k1gFnPc4	kotva
a	a	k8xC	a
včetně	včetně	k7c2	včetně
doprovodu	doprovod	k1gInSc2	doprovod
torpédoborců	torpédoborec	k1gMnPc2	torpédoborec
vypluly	vyplout	k5eAaPmAgFnP	vyplout
na	na	k7c4	na
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
hodinách	hodina	k1gFnPc6	hodina
admirál	admirál	k1gMnSc1	admirál
Lütjens	Lütjens	k1gInSc4	Lütjens
odeslal	odeslat	k5eAaPmAgMnS	odeslat
doprovodné	doprovodný	k2eAgInPc4d1	doprovodný
torpédoborce	torpédoborec	k1gInPc4	torpédoborec
zpět	zpět	k6eAd1	zpět
a	a	k8xC	a
obě	dva	k4xCgFnPc1	dva
lodi	loď	k1gFnPc1	loď
zamířily	zamířit	k5eAaPmAgFnP	zamířit
k	k	k7c3	k
Dánskému	dánský	k2eAgInSc3d1	dánský
průlivu	průliv	k1gInSc3	průliv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Britské	britský	k2eAgNnSc1d1	Britské
velení	velení	k1gNnSc1	velení
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
znepokojeno	znepokojit	k5eAaPmNgNnS	znepokojit
možností	možnost	k1gFnSc7	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
německé	německý	k2eAgFnPc1d1	německá
bitevní	bitevní	k2eAgFnPc1d1	bitevní
lodě	loď	k1gFnPc1	loď
mohly	moct	k5eAaImAgFnP	moct
operovat	operovat	k5eAaImF	operovat
ve	v	k7c6	v
vodách	voda	k1gFnPc6	voda
Atlantiku	Atlantik	k1gInSc2	Atlantik
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
by	by	kYmCp3nP	by
byly	být	k5eAaImAgFnP	být
ohroženy	ohrozit	k5eAaPmNgFnP	ohrozit
životně	životně	k6eAd1	životně
důležité	důležitý	k2eAgFnPc1d1	důležitá
trasy	trasa	k1gFnPc1	trasa
zásobovacích	zásobovací	k2eAgFnPc2d1	zásobovací
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
den	den	k1gInSc1	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
však	však	k9	však
nepříznivé	příznivý	k2eNgNnSc1d1	nepříznivé
počasí	počasí	k1gNnSc1	počasí
znemožňovalo	znemožňovat	k5eAaImAgNnS	znemožňovat
letecký	letecký	k2eAgInSc4d1	letecký
průzkum	průzkum	k1gInSc4	průzkum
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
Britové	Brit	k1gMnPc1	Brit
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
pro	pro	k7c4	pro
riskantní	riskantní	k2eAgFnSc4d1	riskantní
akci	akce	k1gFnSc4	akce
a	a	k8xC	a
vyslali	vyslat	k5eAaPmAgMnP	vyslat
letadlo	letadlo	k1gNnSc4	letadlo
Martin	Martina	k1gFnPc2	Martina
Maryland	Marylanda	k1gFnPc2	Marylanda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
v	v	k7c6	v
nízké	nízký	k2eAgFnSc6d1	nízká
výšce	výška	k1gFnSc6	výška
prolétlo	prolétnout	k5eAaPmAgNnS	prolétnout
nad	nad	k7c7	nad
Grimstafjordem	Grimstafjord	k1gInSc7	Grimstafjord
a	a	k8xC	a
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
potvrzena	potvrzen	k2eAgFnSc1d1	potvrzena
domněnka	domněnka	k1gFnSc1	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Bismarck	Bismarck	k1gMnSc1	Bismarck
již	již	k6eAd1	již
vydal	vydat	k5eAaPmAgMnS	vydat
na	na	k7c4	na
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velitel	velitel	k1gMnSc1	velitel
Home	Home	k1gNnSc2	Home
Fleet	Fleet	k1gMnSc1	Fleet
admirál	admirál	k1gMnSc1	admirál
John	John	k1gMnSc1	John
Tovey	Tovea	k1gFnPc4	Tovea
tak	tak	k9	tak
již	již	k6eAd1	již
dále	daleko	k6eAd2	daleko
neváhal	váhat	k5eNaImAgMnS	váhat
a	a	k8xC	a
ve	v	k7c4	v
23	[number]	k4	23
hodin	hodina	k1gFnPc2	hodina
opustily	opustit	k5eAaPmAgFnP	opustit
lodě	loď	k1gFnPc1	loď
Home	Home	k1gNnSc2	Home
Fleet	Fleeta	k1gFnPc2	Fleeta
<g/>
,	,	kIx,	,
rozčleněné	rozčleněný	k2eAgFnPc1d1	rozčleněná
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
svazů	svaz	k1gInPc2	svaz
základnu	základna	k1gFnSc4	základna
loďstva	loďstvo	k1gNnSc2	loďstvo
ve	v	k7c4	v
Scapa	Scap	k1gMnSc4	Scap
Flow	Flow	k1gFnSc6	Flow
<g/>
,	,	kIx,	,
propluly	proplout	k5eAaPmAgFnP	proplout
zálivem	záliv	k1gInSc7	záliv
Pentland	Pentland	k1gInSc1	Pentland
Firth	Firtha	k1gFnPc2	Firtha
a	a	k8xC	a
pluly	plout	k5eAaImAgFnP	plout
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
svaz	svaz	k1gInSc4	svaz
plul	plout	k5eAaImAgMnS	plout
pod	pod	k7c7	pod
přímým	přímý	k2eAgNnSc7d1	přímé
velením	velení	k1gNnSc7	velení
admirála	admirál	k1gMnSc2	admirál
Toveye	Tovey	k1gFnSc2	Tovey
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
viceadmirála	viceadmirál	k1gMnSc2	viceadmirál
Hollanda	Hollanda	k1gFnSc1	Hollanda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ranních	ranní	k2eAgFnPc6d1	ranní
hodinách	hodina	k1gFnPc6	hodina
se	se	k3xPyFc4	se
ke	k	k7c3	k
svazu	svaz	k1gInSc3	svaz
admirála	admirál	k1gMnSc2	admirál
Toveye	Tovey	k1gMnSc2	Tovey
připojil	připojit	k5eAaPmAgInS	připojit
bitevní	bitevní	k2eAgInSc1d1	bitevní
křižník	křižník	k1gInSc1	křižník
HMS	HMS	kA	HMS
Repulse	repulse	k1gFnSc1	repulse
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
torpédoborci	torpédoborec	k1gInPc7	torpédoborec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vyplul	vyplout	k5eAaPmAgInS	vyplout
o	o	k7c4	o
den	den	k1gInSc4	den
dříve	dříve	k6eAd2	dříve
z	z	k7c2	z
Clydu	Clyd	k1gInSc2	Clyd
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Dánském	dánský	k2eAgInSc6d1	dánský
průlivu	průliv	k1gInSc6	průliv
již	již	k6eAd1	již
hlídkovaly	hlídkovat	k5eAaImAgFnP	hlídkovat
britské	britský	k2eAgInPc4d1	britský
křižníky	křižník	k1gInPc4	křižník
s	s	k7c7	s
úkolem	úkol	k1gInSc7	úkol
případný	případný	k2eAgInSc1d1	případný
průnik	průnik	k1gInSc1	průnik
německých	německý	k2eAgFnPc2d1	německá
lodí	loď	k1gFnPc2	loď
do	do	k7c2	do
Atlantiku	Atlantik	k1gInSc2	Atlantik
tímto	tento	k3xDgInSc7	tento
průlivem	průliv	k1gInSc7	průliv
odhalit	odhalit	k5eAaPmF	odhalit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Bitva	bitva	k1gFnSc1	bitva
v	v	k7c6	v
Dánském	dánský	k2eAgInSc6d1	dánský
průlivu	průliv	k1gInSc6	průliv
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Dánském	dánský	k2eAgInSc6d1	dánský
průlivu	průliv	k1gInSc6	průliv
hlídkující	hlídkující	k2eAgFnSc1d1	hlídkující
skupina	skupina	k1gFnSc1	skupina
těžkých	těžký	k2eAgInPc2d1	těžký
křižníků	křižník	k1gInPc2	křižník
HMS	HMS	kA	HMS
Suffolk	Suffolk	k1gInSc1	Suffolk
a	a	k8xC	a
HMS	HMS	kA	HMS
Norfolk	Norfolk	k1gInSc1	Norfolk
pod	pod	k7c7	pod
společným	společný	k2eAgNnSc7d1	společné
velením	velení	k1gNnSc7	velení
kontradmirála	kontradmirál	k1gMnSc2	kontradmirál
Wake-Walkera	Wake-Walker	k1gMnSc2	Wake-Walker
opravdu	opravdu	k6eAd1	opravdu
pokus	pokus	k1gInSc4	pokus
Bismarcka	Bismarcka	k1gFnSc1	Bismarcka
a	a	k8xC	a
Prinze	Prinze	k1gFnSc1	Prinze
Eugena	Eugena	k1gFnSc1	Eugena
odhalila	odhalit	k5eAaPmAgFnS	odhalit
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
německou	německý	k2eAgFnSc4d1	německá
eskadru	eskadra	k1gFnSc4	eskadra
sledovala	sledovat	k5eAaImAgFnS	sledovat
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
nepřiblíží	přiblížit	k5eNaPmIp3nP	přiblížit
další	další	k2eAgFnPc1d1	další
síly	síla	k1gFnPc1	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Britské	britský	k2eAgNnSc1d1	Britské
velení	velení	k1gNnSc1	velení
pro	pro	k7c4	pro
přímý	přímý	k2eAgInSc4d1	přímý
boj	boj	k1gInSc4	boj
s	s	k7c7	s
německou	německý	k2eAgFnSc7d1	německá
eskadrou	eskadra	k1gFnSc7	eskadra
vyslalo	vyslat	k5eAaPmAgNnS	vyslat
největší	veliký	k2eAgFnSc4d3	veliký
britskou	britský	k2eAgFnSc4d1	britská
válečnou	válečný	k2eAgFnSc4d1	válečná
loď	loď	k1gFnSc4	loď
<g/>
,	,	kIx,	,
bitevní	bitevní	k2eAgInSc4d1	bitevní
křižník	křižník	k1gInSc4	křižník
HMS	HMS	kA	HMS
Hood	Hood	k1gInSc1	Hood
a	a	k8xC	a
bitevní	bitevní	k2eAgFnSc1d1	bitevní
loď	loď	k1gFnSc1	loď
HMS	HMS	kA	HMS
Prince	princ	k1gMnSc2	princ
of	of	k?	of
Wales	Wales	k1gInSc1	Wales
doprovázené	doprovázený	k2eAgInPc1d1	doprovázený
šesti	šest	k4xCc6	šest
torpédoborci	torpédoborec	k1gMnSc6	torpédoborec
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
viceadmirála	viceadmirál	k1gMnSc2	viceadmirál
Lancelota	Lancelot	k1gMnSc2	Lancelot
Ernesta	Ernest	k1gMnSc2	Ernest
Hollanda	Hollanda	k1gFnSc1	Hollanda
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1941	[number]	k4	1941
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
bitvě	bitva	k1gFnSc3	bitva
v	v	k7c6	v
Dánském	dánský	k2eAgInSc6d1	dánský
průlivu	průliv	k1gInSc6	průliv
<g/>
.	.	kIx.	.
</s>
<s>
Hood	Hood	k1gInSc1	Hood
byl	být	k5eAaImAgInS	být
potopen	potopit	k5eAaPmNgInS	potopit
<g/>
,	,	kIx,	,
Prince	princa	k1gFnSc3	princa
of	of	k?	of
Wales	Wales	k1gInSc1	Wales
poškozen	poškodit	k5eAaPmNgInS	poškodit
a	a	k8xC	a
zahnán	zahnat	k5eAaPmNgInS	zahnat
na	na	k7c4	na
útěk	útěk	k1gInSc4	útěk
<g/>
.	.	kIx.	.
</s>
<s>
Bitevní	bitevní	k2eAgFnSc1d1	bitevní
loď	loď	k1gFnSc1	loď
Bismarck	Bismarcka	k1gFnPc2	Bismarcka
byla	být	k5eAaImAgFnS	být
poškozena	poškodit	k5eAaPmNgFnS	poškodit
a	a	k8xC	a
uniklo	uniknout	k5eAaPmAgNnS	uniknout
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
asi	asi	k9	asi
tisíc	tisíc	k4xCgInSc1	tisíc
tun	tuna	k1gFnPc2	tuna
paliva	palivo	k1gNnSc2	palivo
<g/>
,	,	kIx,	,
další	další	k2eAgNnSc1d1	další
palivo	palivo	k1gNnSc1	palivo
bylo	být	k5eAaImAgNnS	být
dále	daleko	k6eAd2	daleko
znehodnoceno	znehodnotit	k5eAaPmNgNnS	znehodnotit
mořskou	mořský	k2eAgFnSc7d1	mořská
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
ztrátě	ztráta	k1gFnSc6	ztráta
největší	veliký	k2eAgFnSc2d3	veliký
lodi	loď	k1gFnSc2	loď
Royal	Royal	k1gMnSc1	Royal
Navy	Navy	k?	Navy
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
také	také	k9	také
vzbudila	vzbudit	k5eAaPmAgFnS	vzbudit
velký	velký	k2eAgInSc4d1	velký
ohlas	ohlas	k1gInSc4	ohlas
a	a	k8xC	a
rozčarování	rozčarování	k1gNnSc1	rozčarování
britského	britský	k2eAgNnSc2d1	Britské
veřejného	veřejný	k2eAgNnSc2d1	veřejné
mínění	mínění	k1gNnSc2	mínění
se	se	k3xPyFc4	se
dostižení	dostižení	k1gNnSc1	dostižení
a	a	k8xC	a
zničení	zničení	k1gNnSc1	zničení
Bismarcka	Bismarcko	k1gNnSc2	Bismarcko
stalo	stát	k5eAaPmAgNnS	stát
prestižním	prestižní	k2eAgInSc7d1	prestižní
úkolem	úkol	k1gInSc7	úkol
a	a	k8xC	a
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
prioritou	priorita	k1gFnSc7	priorita
celého	celý	k2eAgNnSc2d1	celé
britského	britský	k2eAgNnSc2d1	Britské
loďstva	loďstvo	k1gNnSc2	loďstvo
<g/>
.	.	kIx.	.
</s>
<s>
Admiralita	admiralita	k1gFnSc1	admiralita
obdržela	obdržet	k5eAaPmAgFnS	obdržet
od	od	k7c2	od
ministerského	ministerský	k2eAgMnSc2d1	ministerský
předsedy	předseda	k1gMnSc2	předseda
Winstona	Winston	k1gMnSc2	Winston
Churchilla	Churchill	k1gMnSc2	Churchill
následující	následující	k2eAgInSc1d1	následující
rozkaz	rozkaz	k1gInSc1	rozkaz
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Vaším	váš	k3xOp2gInSc7	váš
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
potopit	potopit	k5eAaPmF	potopit
Bismarcka	Bismarcka	k1gFnSc1	Bismarcka
<g/>
.	.	kIx.	.
</s>
<s>
Nic	nic	k3yNnSc1	nic
není	být	k5eNaImIp3nS	být
v	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
chvíli	chvíle	k1gFnSc4	chvíle
důležitější	důležitý	k2eAgFnSc4d2	důležitější
<g/>
.	.	kIx.	.
</s>
<s>
Nařizuji	nařizovat	k5eAaImIp1nS	nařizovat
vám	vy	k3xPp2nPc3	vy
učinit	učinit	k5eAaImF	učinit
všechny	všechen	k3xTgInPc4	všechen
kroky	krok	k1gInPc4	krok
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tento	tento	k3xDgInSc1	tento
úkol	úkol	k1gInSc1	úkol
byl	být	k5eAaImAgInS	být
splněn	splnit	k5eAaPmNgInS	splnit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mobilizace	mobilizace	k1gFnSc1	mobilizace
sil	síla	k1gFnPc2	síla
pro	pro	k7c4	pro
stíhání	stíhání	k1gNnSc4	stíhání
===	===	k?	===
</s>
</p>
<p>
<s>
Britská	britský	k2eAgFnSc1d1	britská
admiralita	admiralita	k1gFnSc1	admiralita
proto	proto	k8xC	proto
mobilizovala	mobilizovat	k5eAaBmAgFnS	mobilizovat
mnoho	mnoho	k4c4	mnoho
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zahradila	zahradit	k5eAaPmAgFnS	zahradit
Bismarcku	Bismarcko	k1gNnSc3	Bismarcko
únikové	únikový	k2eAgFnSc2d1	úniková
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
přímého	přímý	k2eAgNnSc2d1	přímé
stíhání	stíhání	k1gNnSc2	stíhání
se	se	k3xPyFc4	se
pustil	pustit	k5eAaPmAgInS	pustit
operační	operační	k2eAgInSc1d1	operační
svaz	svaz	k1gInSc1	svaz
admirála	admirál	k1gMnSc2	admirál
sira	sir	k1gMnSc2	sir
Johna	John	k1gMnSc2	John
Toveye	Tovey	k1gMnSc2	Tovey
<g/>
,	,	kIx,	,
velitele	velitel	k1gMnSc2	velitel
Home	Hom	k1gMnSc2	Hom
Fleet	Fleet	k1gInSc1	Fleet
složený	složený	k2eAgInSc1d1	složený
z	z	k7c2	z
bitevní	bitevní	k2eAgFnSc2d1	bitevní
lodi	loď	k1gFnSc2	loď
HMS	HMS	kA	HMS
King	King	k1gMnSc1	King
George	Georg	k1gMnSc2	Georg
V	V	kA	V
<g/>
,	,	kIx,	,
bitevního	bitevní	k2eAgInSc2d1	bitevní
křižníku	křižník	k1gInSc2	křižník
Repulse	repulse	k1gFnSc2	repulse
<g/>
,	,	kIx,	,
letadlové	letadlový	k2eAgFnSc2d1	letadlová
lodi	loď	k1gFnSc2	loď
HMS	HMS	kA	HMS
Victorious	Victorious	k1gMnSc1	Victorious
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
eskadry	eskadra	k1gFnSc2	eskadra
lehkých	lehký	k2eAgInPc2d1	lehký
křižníků	křižník	k1gInPc2	křižník
HMS	HMS	kA	HMS
Galatea	Galateus	k1gMnSc2	Galateus
<g/>
,	,	kIx,	,
HMS	HMS	kA	HMS
Aurora	Aurora	k1gFnSc1	Aurora
<g/>
,	,	kIx,	,
HMS	HMS	kA	HMS
Kenya	Kenya	k1gFnSc1	Kenya
a	a	k8xC	a
HMS	HMS	kA	HMS
Hermione	Hermion	k1gInSc5	Hermion
a	a	k8xC	a
devíti	devět	k4xCc2	devět
torpédoborců	torpédoborec	k1gInPc2	torpédoborec
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
východu	východ	k1gInSc2	východ
připlouvala	připlouvat	k5eAaImAgFnS	připlouvat
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
kapitána	kapitán	k1gMnSc2	kapitán
Darlymple-Hamiltona	Darlymple-Hamilton	k1gMnSc2	Darlymple-Hamilton
bitevní	bitevní	k2eAgFnSc1d1	bitevní
loď	loď	k1gFnSc1	loď
HMS	HMS	kA	HMS
Rodney	Rodnea	k1gFnPc1	Rodnea
doprovázená	doprovázený	k2eAgFnSc1d1	doprovázená
třemi	tři	k4xCgInPc7	tři
torpédoborci	torpédoborec	k1gInPc7	torpédoborec
<g/>
,	,	kIx,	,
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
se	se	k3xPyFc4	se
blížila	blížit	k5eAaImAgFnS	blížit
bitevní	bitevní	k2eAgFnSc1d1	bitevní
loď	loď	k1gFnSc1	loď
HMS	HMS	kA	HMS
Ramillies	Ramilliesa	k1gFnPc2	Ramilliesa
<g/>
,	,	kIx,	,
z	z	k7c2	z
Gibraltaru	Gibraltar	k1gInSc2	Gibraltar
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
vyplul	vyplout	k5eAaPmAgInS	vyplout
"	"	kIx"	"
<g/>
svaz	svaz	k1gInSc1	svaz
H	H	kA	H
<g/>
"	"	kIx"	"
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
viceadmirála	viceadmirál	k1gMnSc2	viceadmirál
sira	sir	k1gMnSc2	sir
Jamese	Jamese	k1gFnSc1	Jamese
Sommervilla	Sommervilla	k1gFnSc1	Sommervilla
s	s	k7c7	s
bitevním	bitevní	k2eAgInSc7d1	bitevní
křižníkem	křižník	k1gInSc7	křižník
HMS	HMS	kA	HMS
Renown	Renown	k1gNnSc1	Renown
<g/>
,	,	kIx,	,
letadlovou	letadlový	k2eAgFnSc7d1	letadlová
lodí	loď	k1gFnSc7	loď
HMS	HMS	kA	HMS
Ark	Ark	k1gMnSc1	Ark
Royal	Royal	k1gMnSc1	Royal
<g/>
,	,	kIx,	,
lehkým	lehký	k2eAgInSc7d1	lehký
křižníkem	křižník	k1gInSc7	křižník
HMS	HMS	kA	HMS
Sheffield	Sheffield	k1gInSc1	Sheffield
a	a	k8xC	a
šesti	šest	k4xCc7	šest
torpédoborci	torpédoborec	k1gInPc7	torpédoborec
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
byl	být	k5eAaImAgInS	být
povolán	povolán	k2eAgInSc1d1	povolán
křižník	křižník	k1gInSc1	křižník
HMS	HMS	kA	HMS
Edinburgh	Edinburgh	k1gInSc4	Edinburgh
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
bitevní	bitevní	k2eAgFnSc1d1	bitevní
loď	loď	k1gFnSc1	loď
HMS	HMS	kA	HMS
Revenge	Reveng	k1gFnSc2	Reveng
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
přístavu	přístav	k1gInSc6	přístav
Halifax	Halifax	k1gInSc4	Halifax
dostala	dostat	k5eAaPmAgFnS	dostat
rozkaz	rozkaz	k1gInSc4	rozkaz
připravit	připravit	k5eAaPmF	připravit
se	se	k3xPyFc4	se
k	k	k7c3	k
vyplutí	vyplutí	k1gNnSc3	vyplutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
admirála	admirál	k1gMnSc2	admirál
Hollanda	Hollando	k1gNnSc2	Hollando
přešel	přejít	k5eAaPmAgMnS	přejít
Prince	princa	k1gFnSc3	princa
of	of	k?	of
Wales	Wales	k1gInSc1	Wales
a	a	k8xC	a
torpédoborce	torpédoborec	k1gInPc1	torpédoborec
pod	pod	k7c4	pod
velení	velení	k1gNnSc4	velení
kontradmirála	kontradmirál	k1gMnSc2	kontradmirál
Wake-Walkera	Wake-Walker	k1gMnSc2	Wake-Walker
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
však	však	k9	však
dospěl	dochvít	k5eAaPmAgMnS	dochvít
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
se	s	k7c7	s
všemi	všecek	k3xTgFnPc7	všecek
zbývajícími	zbývající	k2eAgFnPc7d1	zbývající
loděmi	loď	k1gFnPc7	loď
není	být	k5eNaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
silný	silný	k2eAgInSc1d1	silný
aby	aby	k9	aby
na	na	k7c4	na
německé	německý	k2eAgFnPc4d1	německá
lodě	loď	k1gFnPc4	loď
znovu	znovu	k6eAd1	znovu
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
sledování	sledování	k1gNnSc6	sledování
<g/>
.	.	kIx.	.
</s>
<s>
Bismarck	Bismarck	k6eAd1	Bismarck
po	po	k7c6	po
potopení	potopení	k1gNnSc6	potopení
Hooda	Hoodo	k1gNnSc2	Hoodo
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
dále	daleko	k6eAd2	daleko
rychlostí	rychlost	k1gFnSc7	rychlost
28	[number]	k4	28
uzlů	uzel	k1gInPc2	uzel
a	a	k8xC	a
až	až	k9	až
po	po	k7c6	po
půldruhé	půldruhý	k2eAgFnSc6d1	půldruhá
hodině	hodina	k1gFnSc6	hodina
zpomalil	zpomalit	k5eAaPmAgInS	zpomalit
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
potíží	potíž	k1gFnPc2	potíž
s	s	k7c7	s
palivem	palivo	k1gNnSc7	palivo
na	na	k7c4	na
22	[number]	k4	22
uzlů	uzel	k1gInPc2	uzel
a	a	k8xC	a
v	v	k7c6	v
odpoledních	odpolední	k2eAgFnPc6d1	odpolední
hodinách	hodina	k1gFnPc6	hodina
odbočil	odbočit	k5eAaPmAgMnS	odbočit
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
přibližovat	přibližovat	k5eAaImF	přibližovat
ke	k	k7c3	k
svazu	svaz	k1gInSc3	svaz
admirála	admirál	k1gMnSc2	admirál
Johna	John	k1gMnSc2	John
Toveye	Tovey	k1gMnSc2	Tovey
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Bismarck	Bismarck	k1gInSc1	Bismarck
nemohl	moct	k5eNaImAgInS	moct
během	během	k7c2	během
nočních	noční	k2eAgFnPc2d1	noční
hodin	hodina	k1gFnPc2	hodina
uniknout	uniknout	k5eAaPmF	uniknout
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
napadení	napadení	k1gNnSc6	napadení
letadly	letadlo	k1gNnPc7	letadlo
z	z	k7c2	z
Victorious	Victorious	k1gInSc4	Victorious
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
měl	mít	k5eAaImAgInS	mít
nálet	nálet	k1gInSc1	nálet
lepší	dobrý	k2eAgFnSc2d2	lepší
vyhlídky	vyhlídka	k1gFnSc2	vyhlídka
na	na	k7c4	na
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgInS	dát
Tovey	Tovea	k1gFnSc2	Tovea
rozkaz	rozkaz	k1gInSc1	rozkaz
Victorious	Victorious	k1gMnSc1	Victorious
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
křižníků	křižník	k1gInPc2	křižník
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
viceadmirála	viceadmirál	k1gMnSc2	viceadmirál
Curteise	Curteise	k1gFnSc2	Curteise
plout	plout	k5eAaImF	plout
napřed	napřed	k6eAd1	napřed
a	a	k8xC	a
zkrátit	zkrátit	k5eAaPmF	zkrátit
co	co	k3yInSc1	co
nejdříve	dříve	k6eAd3	dříve
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
k	k	k7c3	k
Bismarcku	Bismarck	k1gInSc3	Bismarck
na	na	k7c4	na
100	[number]	k4	100
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Odpoutání	odpoutání	k1gNnSc3	odpoutání
Prinze	Prinze	k1gFnSc2	Prinze
Eugena	Eugeno	k1gNnSc2	Eugeno
===	===	k?	===
</s>
</p>
<p>
<s>
Wake-Walker	Wake-Walker	k1gMnSc1	Wake-Walker
zatím	zatím	k6eAd1	zatím
kvůli	kvůli	k7c3	kvůli
mlhavému	mlhavý	k2eAgNnSc3d1	mlhavé
počasí	počasí	k1gNnSc3	počasí
udržoval	udržovat	k5eAaImAgInS	udržovat
s	s	k7c7	s
německými	německý	k2eAgFnPc7d1	německá
loděmi	loď	k1gFnPc7	loď
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
často	často	k6eAd1	často
ztrácely	ztrácet	k5eAaImAgInP	ztrácet
v	v	k7c6	v
mlze	mlha	k1gFnSc6	mlha
i	i	k9	i
z	z	k7c2	z
obrazovek	obrazovka	k1gFnPc2	obrazovka
radaru	radar	k1gInSc2	radar
<g/>
,	,	kIx,	,
kontakt	kontakt	k1gInSc4	kontakt
jen	jen	k9	jen
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
nejvyššího	vysoký	k2eAgNnSc2d3	nejvyšší
úsilí	úsilí	k1gNnSc2	úsilí
a	a	k8xC	a
sporadických	sporadický	k2eAgFnPc2d1	sporadická
přestřelek	přestřelka	k1gFnPc2	přestřelka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Admirál	admirál	k1gMnSc1	admirál
Lütjens	Lütjens	k1gInSc4	Lütjens
na	na	k7c6	na
můstku	můstek	k1gInSc6	můstek
Bismarcka	Bismarcko	k1gNnSc2	Bismarcko
v	v	k7c6	v
18	[number]	k4	18
<g/>
:	:	kIx,	:
<g/>
20	[number]	k4	20
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Bismarck	Bismarck	k1gMnSc1	Bismarck
oddělí	oddělit	k5eAaPmIp3nS	oddělit
od	od	k7c2	od
Prinze	Prinze	k1gFnSc2	Prinze
Eugena	Eugen	k1gMnSc2	Eugen
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
kursu	kurs	k1gInSc6	kurs
na	na	k7c4	na
původně	původně	k6eAd1	původně
plánované	plánovaný	k2eAgNnSc4d1	plánované
setkání	setkání	k1gNnSc4	setkání
s	s	k7c7	s
cisternovou	cisternový	k2eAgFnSc7d1	cisternová
lodí	loď	k1gFnSc7	loď
a	a	k8xC	a
Bismarck	Bismarck	k1gMnSc1	Bismarck
odbočí	odbočit	k5eAaPmIp3nS	odbočit
na	na	k7c4	na
jihovýchod	jihovýchod	k1gInSc4	jihovýchod
vydá	vydat	k5eAaPmIp3nS	vydat
se	s	k7c7	s
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
St.	st.	kA	st.
Nazaire	Nazair	k1gInSc5	Nazair
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
chtěl	chtít	k5eAaImAgMnS	chtít
admirál	admirál	k1gMnSc1	admirál
Lütjens	Lütjensa	k1gFnPc2	Lütjensa
také	také	k9	také
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
svést	svést	k5eAaPmF	svést
britské	britský	k2eAgFnSc2d1	britská
lodi	loď	k1gFnSc2	loď
ze	z	k7c2	z
stopy	stopa	k1gFnSc2	stopa
a	a	k8xC	a
nechat	nechat	k5eAaPmF	nechat
je	být	k5eAaImIp3nS	být
pronásledovat	pronásledovat	k5eAaImF	pronásledovat
Prinze	Prinze	k1gFnSc1	Prinze
Eugena	Eugen	k2eAgFnSc1d1	Eugena
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
však	však	k9	však
v	v	k7c6	v
mlze	mlha	k1gFnSc6	mlha
zpočátku	zpočátku	k6eAd1	zpočátku
vůbec	vůbec	k9	vůbec
nezaznamenali	zaznamenat	k5eNaPmAgMnP	zaznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
německé	německý	k2eAgFnPc1d1	německá
lodi	loď	k1gFnPc1	loď
rozdělily	rozdělit	k5eAaPmAgFnP	rozdělit
a	a	k8xC	a
nadále	nadále	k6eAd1	nadále
sledovali	sledovat	k5eAaImAgMnP	sledovat
pouze	pouze	k6eAd1	pouze
Bismarcka	Bismarcka	k1gFnSc1	Bismarcka
-	-	kIx~	-
Prinz	Prinz	k1gInSc1	Prinz
Eugen	Eugen	k2eAgInSc1d1	Eugen
nakonec	nakonec	k6eAd1	nakonec
po	po	k7c6	po
řadě	řada	k1gFnSc6	řada
peripetií	peripetie	k1gFnPc2	peripetie
doplul	doplout	k5eAaPmAgMnS	doplout
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
do	do	k7c2	do
Brestu	Brest	k1gInSc2	Brest
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Útok	útok	k1gInSc1	útok
letadel	letadlo	k1gNnPc2	letadlo
Victorious	Victorious	k1gInSc1	Victorious
===	===	k?	===
</s>
</p>
<p>
<s>
Zatím	zatím	k6eAd1	zatím
se	se	k3xPyFc4	se
lodě	loď	k1gFnPc1	loď
admirála	admirál	k1gMnSc2	admirál
Curteise	Curteise	k1gFnSc1	Curteise
s	s	k7c7	s
letadlovou	letadlový	k2eAgFnSc7d1	letadlová
lodí	loď	k1gFnSc7	loď
Victorious	Victorious	k1gMnSc1	Victorious
k	k	k7c3	k
pozici	pozice	k1gFnSc3	pozice
Bismarcka	Bismarcko	k1gNnSc2	Bismarcko
přibližovaly	přibližovat	k5eAaImAgFnP	přibližovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
10	[number]	k4	10
hodin	hodina	k1gFnPc2	hodina
večer	večer	k6eAd1	večer
byl	být	k5eAaImAgInS	být
ještě	ještě	k9	ještě
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
120	[number]	k4	120
mil	míle	k1gFnPc2	míle
od	od	k7c2	od
pozice	pozice	k1gFnSc2	pozice
německých	německý	k2eAgFnPc2d1	německá
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
bombardéry	bombardér	k1gInPc1	bombardér
stihly	stihnout	k5eAaPmAgInP	stihnout
zaútočit	zaútočit	k5eAaPmF	zaútočit
ještě	ještě	k9	ještě
před	před	k7c7	před
setměním	setmění	k1gNnSc7	setmění
(	(	kIx(	(
<g/>
západ	západ	k1gInSc1	západ
slunce	slunce	k1gNnSc1	slunce
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
daných	daný	k2eAgFnPc6d1	daná
souřadnicích	souřadnice	k1gFnPc6	souřadnice
50	[number]	k4	50
minut	minuta	k1gFnPc2	minuta
po	po	k7c6	po
půlnoci	půlnoc	k1gFnSc6	půlnoc
<g/>
)	)	kIx)	)
a	a	k8xC	a
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
zhoršovalo	zhoršovat	k5eAaImAgNnS	zhoršovat
počasí	počasí	k1gNnSc3	počasí
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
velitel	velitel	k1gMnSc1	velitel
Victorious	Victorious	k1gMnSc1	Victorious
kapitán	kapitán	k1gMnSc1	kapitán
Bovell	Bovell	k1gMnSc1	Bovell
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
útok	útok	k1gInSc4	útok
již	již	k9	již
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
komandéra	komandér	k1gMnSc2	komandér
Eugena	Eugen	k1gMnSc2	Eugen
Esmonda	Esmond	k1gMnSc2	Esmond
odstartovalo	odstartovat	k5eAaPmAgNnS	odstartovat
devět	devět	k4xCc1	devět
letounů	letoun	k1gInPc2	letoun
typu	typ	k1gInSc2	typ
Fairey	Fairea	k1gFnPc4	Fairea
Swordfish	Swordfisha	k1gFnPc2	Swordfisha
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
typu	typ	k1gInSc2	typ
Fairey	Fairea	k1gFnPc4	Fairea
Fulmar	Fulmara	k1gFnPc2	Fulmara
<g/>
.	.	kIx.	.
</s>
<s>
Bohužel	bohužel	k9	bohužel
však	však	k9	však
piloti	pilot	k1gMnPc1	pilot
po	po	k7c6	po
přeletu	přelet	k1gInSc6	přelet
Wake-Walkerových	Wake-Walkerův	k2eAgFnPc2d1	Wake-Walkerův
lodí	loď	k1gFnPc2	loď
omylem	omylem	k6eAd1	omylem
téměř	téměř	k6eAd1	téměř
zahájili	zahájit	k5eAaPmAgMnP	zahájit
útok	útok	k1gInSc4	útok
na	na	k7c4	na
plavidlo	plavidlo	k1gNnSc4	plavidlo
americké	americký	k2eAgFnSc2d1	americká
pobřežní	pobřežní	k2eAgFnSc2d1	pobřežní
stráže	stráž	k1gFnSc2	stráž
USCGC	USCGC	kA	USCGC
Modoc	Modoc	k1gFnSc1	Modoc
o	o	k7c6	o
jehož	jehož	k3xOyRp3gFnSc6	jehož
přítomnosti	přítomnost	k1gFnSc6	přítomnost
nebylo	být	k5eNaImAgNnS	být
Britům	Brit	k1gMnPc3	Brit
nic	nic	k3yNnSc4	nic
známo	znám	k2eAgNnSc1d1	známo
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
omyl	omyl	k1gInSc1	omyl
na	na	k7c6	na
můstku	můstek	k1gInSc6	můstek
Bismarcka	Bismarcko	k1gNnSc2	Bismarcko
zaznamenali	zaznamenat	k5eAaPmAgMnP	zaznamenat
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
loď	loď	k1gFnSc1	loď
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
varována	varován	k2eAgFnSc1d1	varována
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
čas	čas	k1gInSc4	čas
se	se	k3xPyFc4	se
na	na	k7c4	na
letecký	letecký	k2eAgInSc4d1	letecký
útok	útok	k1gInSc4	útok
připravit	připravit	k5eAaPmF	připravit
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
velké	velký	k2eAgNnSc4d1	velké
úsilí	úsilí	k1gNnSc4	úsilí
britských	britský	k2eAgMnPc2d1	britský
letců	letec	k1gMnPc2	letec
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
podařilo	podařit	k5eAaPmAgNnS	podařit
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
pouze	pouze	k6eAd1	pouze
zásah	zásah	k1gInSc4	zásah
torpédem	torpédo	k1gNnSc7	torpédo
doprostřed	doprostřed	k7c2	doprostřed
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
chromniklová	chromniklový	k2eAgFnSc1d1	chromniklová
ocel	ocel	k1gFnSc1	ocel
však	však	k9	však
vydržela	vydržet	k5eAaPmAgFnS	vydržet
a	a	k8xC	a
Bismarcku	Bismarcka	k1gFnSc4	Bismarcka
tak	tak	k6eAd1	tak
nebyla	být	k5eNaImAgFnS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
žádná	žádný	k3yNgFnSc1	žádný
patrná	patrný	k2eAgFnSc1d1	patrná
škoda	škoda	k1gFnSc1	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
útoku	útok	k1gInSc6	útok
však	však	k9	však
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
exploze	exploze	k1gFnSc2	exploze
zemřel	zemřít	k5eAaPmAgInS	zemřít
první	první	k4xOgInSc1	první
člen	člen	k1gInSc1	člen
posádky	posádka	k1gFnSc2	posádka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ztráta	ztráta	k1gFnSc1	ztráta
kontaktu	kontakt	k1gInSc2	kontakt
===	===	k?	===
</s>
</p>
<p>
<s>
Britské	britský	k2eAgInPc4d1	britský
křižníky	křižník	k1gInPc4	křižník
nadále	nadále	k6eAd1	nadále
Bismarcka	Bismarcko	k1gNnSc2	Bismarcko
sledovaly	sledovat	k5eAaImAgInP	sledovat
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
25	[number]	k4	25
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
před	před	k7c7	před
čtvrtou	čtvrtý	k4xOgFnSc7	čtvrtý
hodinou	hodina	k1gFnSc7	hodina
ranní	ranní	k2eAgInSc4d1	ranní
kontakt	kontakt	k1gInSc4	kontakt
ztratily	ztratit	k5eAaPmAgFnP	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
tohoto	tento	k3xDgInSc2	tento
faktu	fakt	k1gInSc2	fakt
a	a	k8xC	a
ubývajících	ubývající	k2eAgFnPc2d1	ubývající
pohonných	pohonný	k2eAgFnPc2d1	pohonná
hmot	hmota	k1gFnPc2	hmota
museli	muset	k5eAaImAgMnP	muset
Britové	Brit	k1gMnPc1	Brit
své	svůj	k3xOyFgFnSc2	svůj
lodě	loď	k1gFnSc2	loď
přeskupit	přeskupit	k5eAaPmF	přeskupit
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
začít	začít	k5eAaPmF	začít
s	s	k7c7	s
hledáním	hledání	k1gNnSc7	hledání
<g/>
.	.	kIx.	.
</s>
<s>
Křižníky	křižník	k1gInPc1	křižník
Suffolk	Suffolka	k1gFnPc2	Suffolka
a	a	k8xC	a
Norfolk	Norfolka	k1gFnPc2	Norfolka
se	se	k3xPyFc4	se
rozdělily	rozdělit	k5eAaPmAgFnP	rozdělit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohly	moct	k5eAaImAgFnP	moct
propátrat	propátrat	k5eAaPmF	propátrat
větší	veliký	k2eAgFnSc4d2	veliký
oblast	oblast	k1gFnSc4	oblast
<g/>
,	,	kIx,	,
Prince	princa	k1gFnSc3	princa
of	of	k?	of
Wales	Wales	k1gInSc1	Wales
byl	být	k5eAaImAgInS	být
odeslán	odeslat	k5eAaPmNgInS	odeslat
ke	k	k7c3	k
svazu	svaz	k1gInSc3	svaz
admirála	admirál	k1gMnSc2	admirál
Toveye	Tovey	k1gMnSc2	Tovey
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
již	již	k6eAd1	již
před	před	k7c7	před
delší	dlouhý	k2eAgFnSc7d2	delší
dobou	doba	k1gFnSc7	doba
odpoutal	odpoutat	k5eAaPmAgInS	odpoutat
kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
pohonných	pohonný	k2eAgFnPc2d1	pohonná
hmot	hmota	k1gFnPc2	hmota
bitevní	bitevní	k2eAgInSc4d1	bitevní
křižník	křižník	k1gInSc4	křižník
Repulse	repulse	k1gFnSc2	repulse
<g/>
.	.	kIx.	.
</s>
<s>
Bitevní	bitevní	k2eAgFnSc1d1	bitevní
loď	loď	k1gFnSc1	loď
Rodney	Rodnea	k1gFnSc2	Rodnea
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
plující	plující	k2eAgInPc1d1	plující
z	z	k7c2	z
Clyde	Clyd	k1gInSc5	Clyd
jako	jako	k9	jako
průvodce	průvodce	k1gMnSc1	průvodce
parníku	parník	k1gInSc2	parník
Britannic	Britannice	k1gFnPc2	Britannice
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Bostonu	Boston	k1gInSc2	Boston
se	se	k3xPyFc4	se
zatím	zatím	k6eAd1	zatím
kolem	kolem	k7c2	kolem
6	[number]	k4	6
<g/>
.	.	kIx.	.
hodiny	hodina	k1gFnSc2	hodina
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
ve	v	k7c6	v
výhodné	výhodný	k2eAgFnSc6d1	výhodná
pozici	pozice	k1gFnSc6	pozice
asi	asi	k9	asi
350	[number]	k4	350
mil	míle	k1gFnPc2	míle
před	před	k7c7	před
Bismarckem	Bismarcko	k1gNnSc7	Bismarcko
<g/>
,	,	kIx,	,
cca	cca	kA	cca
100	[number]	k4	100
mil	míle	k1gFnPc2	míle
od	od	k7c2	od
předpokládané	předpokládaný	k2eAgFnSc2d1	předpokládaná
trasy	trasa	k1gFnSc2	trasa
do	do	k7c2	do
St.	st.	kA	st.
Nazaire	Nazair	k1gMnSc5	Nazair
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
však	však	k9	však
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
netušili	tušit	k5eNaImAgMnP	tušit
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Bismarck	Bismarck	k1gMnSc1	Bismarck
přesně	přesně	k6eAd1	přesně
nachází	nacházet	k5eAaImIp3nS	nacházet
a	a	k8xC	a
Bismarck	Bismarck	k1gMnSc1	Bismarck
měl	mít	k5eAaImAgMnS	mít
stále	stále	k6eAd1	stále
ještě	ještě	k9	ještě
reálnou	reálný	k2eAgFnSc4d1	reálná
šanci	šance	k1gFnSc4	šance
pronásledování	pronásledování	k1gNnPc4	pronásledování
uniknout	uniknout	k5eAaPmF	uniknout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Admirál	admirál	k1gMnSc1	admirál
Lütjens	Lütjensa	k1gFnPc2	Lütjensa
se	se	k3xPyFc4	se
však	však	k9	však
za	za	k7c2	za
této	tento	k3xDgFnSc2	tento
situace	situace	k1gFnSc2	situace
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
odvysílat	odvysílat	k5eAaPmF	odvysílat
na	na	k7c4	na
námořní	námořní	k2eAgNnSc4d1	námořní
velitelství	velitelství	k1gNnSc4	velitelství
"	"	kIx"	"
<g/>
Západ	západ	k1gInSc4	západ
<g/>
"	"	kIx"	"
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
depeši	depeše	k1gFnSc4	depeše
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgMnPc4	který
informoval	informovat	k5eAaBmAgInS	informovat
o	o	k7c6	o
proběhlých	proběhlý	k2eAgFnPc6d1	proběhlá
akcích	akce	k1gFnPc6	akce
a	a	k8xC	a
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
situaci	situace	k1gFnSc6	situace
své	svůj	k3xOyFgFnSc2	svůj
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
neměl	mít	k5eNaImAgInS	mít
ani	ani	k8xC	ani
tušení	tušení	k1gNnSc1	tušení
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
britským	britský	k2eAgFnPc3d1	britská
lodím	loď	k1gFnPc3	loď
Bismarck	Bismarcka	k1gFnPc2	Bismarcka
ztratil	ztratit	k5eAaPmAgMnS	ztratit
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Britové	Brit	k1gMnPc1	Brit
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
jeho	jeho	k3xOp3gFnSc4	jeho
polohu	poloha	k1gFnSc4	poloha
znají	znát	k5eAaImIp3nP	znát
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
lodní	lodní	k2eAgInSc4d1	lodní
deník	deník	k1gInSc4	deník
Bismarcka	Bismarcko	k1gNnSc2	Bismarcko
nedochoval	dochovat	k5eNaPmAgMnS	dochovat
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
posádky	posádka	k1gFnSc2	posádka
zahynula	zahynout	k5eAaPmAgFnS	zahynout
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
ho	on	k3xPp3gMnSc4	on
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
dojmu	dojem	k1gInSc3	dojem
vedlo	vést	k5eAaImAgNnS	vést
-	-	kIx~	-
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
zřejmě	zřejmě	k6eAd1	zřejmě
antény	anténa	k1gFnPc4	anténa
Bismarcka	Bismarcka	k1gFnSc1	Bismarcka
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
zachycovaly	zachycovat	k5eAaImAgFnP	zachycovat
záření	záření	k1gNnSc4	záření
z	z	k7c2	z
britských	britský	k2eAgInPc2d1	britský
radarů	radar	k1gInPc2	radar
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
ale	ale	k9	ale
nemělo	mít	k5eNaImAgNnS	mít
již	již	k6eAd1	již
takovou	takový	k3xDgFnSc4	takový
intenzitu	intenzita	k1gFnSc4	intenzita
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
k	k	k7c3	k
britským	britský	k2eAgFnPc3d1	britská
lodím	loď	k1gFnPc3	loď
vrátilo	vrátit	k5eAaPmAgNnS	vrátit
a	a	k8xC	a
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolem	kolem	k7c2	kolem
deváté	devátý	k4xOgFnSc2	devátý
hodiny	hodina	k1gFnSc2	hodina
pak	pak	k6eAd1	pak
na	na	k7c4	na
Bismarck	Bismarck	k1gInSc4	Bismarck
dorazila	dorazit	k5eAaPmAgFnS	dorazit
depeše	depeše	k1gFnSc1	depeše
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
německého	německý	k2eAgInSc2d1	německý
odposlechu	odposlech	k1gInSc2	odposlech
britské	britský	k2eAgFnSc2d1	britská
námořní	námořní	k2eAgFnSc2d1	námořní
komunikace	komunikace	k1gFnSc2	komunikace
plyne	plynout	k5eAaImIp3nS	plynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Britové	Brit	k1gMnPc1	Brit
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
Bismarckem	Bismarck	k1gInSc7	Bismarck
ztratili	ztratit	k5eAaPmAgMnP	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nepochopitelných	pochopitelný	k2eNgInPc2d1	nepochopitelný
důvodů	důvod	k1gInPc2	důvod
se	se	k3xPyFc4	se
admirál	admirál	k1gMnSc1	admirál
Lütjens	Lütjensa	k1gFnPc2	Lütjensa
nepokusil	pokusit	k5eNaPmAgMnS	pokusit
této	tento	k3xDgFnSc2	tento
informace	informace	k1gFnSc2	informace
využít	využít	k5eAaPmF	využít
a	a	k8xC	a
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
vyšší	vysoký	k2eAgFnSc7d2	vyšší
rychlostí	rychlost	k1gFnSc7	rychlost
do	do	k7c2	do
dosahu	dosah	k1gInSc2	dosah
letounů	letoun	k1gInPc2	letoun
a	a	k8xC	a
ponorek	ponorka	k1gFnPc2	ponorka
z	z	k7c2	z
Brestu	Brest	k1gInSc2	Brest
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
německé	německý	k2eAgFnPc4d1	německá
lodi	loď	k1gFnPc4	loď
významně	významně	k6eAd1	významně
pomoci	pomoct	k5eAaPmF	pomoct
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
by	by	k9	by
tím	ten	k3xDgNnSc7	ten
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
zkomplikoval	zkomplikovat	k5eAaPmAgInS	zkomplikovat
situaci	situace	k1gFnSc4	situace
britských	britský	k2eAgFnPc2d1	britská
lodí	loď	k1gFnPc2	loď
ohledně	ohledně	k7c2	ohledně
nedostatku	nedostatek	k1gInSc2	nedostatek
paliva	palivo	k1gNnSc2	palivo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lütjensovo	Lütjensův	k2eAgNnSc1d1	Lütjensův
vysílání	vysílání	k1gNnSc1	vysílání
na	na	k7c6	na
Britských	britský	k2eAgInPc6d1	britský
ostrovech	ostrov	k1gInPc6	ostrov
zaznamenali	zaznamenat	k5eAaPmAgMnP	zaznamenat
a	a	k8xC	a
odvodili	odvodit	k5eAaPmAgMnP	odvodit
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
přibližnou	přibližný	k2eAgFnSc4d1	přibližná
polohu	poloha	k1gFnSc4	poloha
německé	německý	k2eAgFnSc2d1	německá
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
však	však	k9	však
ze	z	k7c2	z
zachyceného	zachycený	k2eAgNnSc2d1	zachycené
vysílání	vysílání	k1gNnSc2	vysílání
určili	určit	k5eAaPmAgMnP	určit
nejprve	nejprve	k6eAd1	nejprve
polohu	poloha	k1gFnSc4	poloha
Bismarcka	Bismarcko	k1gNnSc2	Bismarcko
nesprávně	správně	k6eNd1	správně
<g/>
,	,	kIx,	,
zdálo	zdát	k5eAaImAgNnS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
míří	mířit	k5eAaImIp3nS	mířit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
dostal	dostat	k5eAaPmAgInS	dostat
křižník	křižník	k1gInSc1	křižník
Suffolk	Suffolko	k1gNnPc2	Suffolko
rozkaz	rozkaz	k1gInSc4	rozkaz
plout	plout	k5eAaImF	plout
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Islandu	Island	k1gInSc3	Island
<g/>
,	,	kIx,	,
bitevní	bitevní	k2eAgFnSc1d1	bitevní
loď	loď	k1gFnSc1	loď
Prince	princ	k1gMnSc2	princ
of	of	k?	of
Wales	Wales	k1gInSc1	Wales
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
vracet	vracet	k5eAaImF	vracet
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Dánskému	dánský	k2eAgInSc3d1	dánský
průlivu	průliv	k1gInSc3	průliv
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
eskadra	eskadra	k1gFnSc1	eskadra
lehkých	lehký	k2eAgInPc2d1	lehký
křižníků	křižník	k1gInPc2	křižník
s	s	k7c7	s
letadlovou	letadlový	k2eAgFnSc7d1	letadlová
lodí	loď	k1gFnSc7	loď
Victorious	Victorious	k1gInSc4	Victorious
měla	mít	k5eAaImAgFnS	mít
pokrýt	pokrýt	k5eAaPmF	pokrýt
prostor	prostor	k1gInSc4	prostor
mezi	mezi	k7c7	mezi
Islandem	Island	k1gInSc7	Island
a	a	k8xC	a
Faerskými	Faerský	k2eAgInPc7d1	Faerský
ostrovy	ostrov	k1gInPc7	ostrov
a	a	k8xC	a
k	k	k7c3	k
Severnímu	severní	k2eAgNnSc3d1	severní
moři	moře	k1gNnSc3	moře
obrátila	obrátit	k5eAaPmAgFnS	obrátit
také	také	k9	také
bitevní	bitevní	k2eAgFnSc1d1	bitevní
loď	loď	k1gFnSc1	loď
King	King	k1gMnSc1	King
George	Georg	k1gInSc2	Georg
V.	V.	kA	V.
Pouze	pouze	k6eAd1	pouze
Rodney	Rodnea	k1gFnPc1	Rodnea
a	a	k8xC	a
Norfolk	Norfolk	k1gInSc1	Norfolk
se	se	k3xPyFc4	se
všeobecného	všeobecný	k2eAgInSc2d1	všeobecný
přesunu	přesun	k1gInSc2	přesun
severním	severní	k2eAgInSc7d1	severní
směrem	směr	k1gInSc7	směr
nezúčastnili	zúčastnit	k5eNaPmAgMnP	zúčastnit
<g/>
.	.	kIx.	.
</s>
<s>
Rodney	Rodney	k1gInPc1	Rodney
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
nízké	nízký	k2eAgFnSc3d1	nízká
rychlosti	rychlost	k1gFnSc3	rychlost
neměl	mít	k5eNaImAgInS	mít
šanci	šance	k1gFnSc4	šance
Bismarcka	Bismarcko	k1gNnSc2	Bismarcko
v	v	k7c6	v
případě	případ	k1gInSc6	případ
severního	severní	k2eAgInSc2d1	severní
kursu	kurs	k1gInSc2	kurs
šanci	šance	k1gFnSc4	šance
dostihnout	dostihnout	k5eAaPmF	dostihnout
a	a	k8xC	a
kontradmirál	kontradmirál	k1gMnSc1	kontradmirál
Wake-Walker	Wake-Walker	k1gMnSc1	Wake-Walker
na	na	k7c6	na
křižníku	křižník	k1gInSc6	křižník
Norfolk	Norfolk	k1gInSc1	Norfolk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bismarck	Bismarck	k1gMnSc1	Bismarck
směřuje	směřovat	k5eAaImIp3nS	směřovat
do	do	k7c2	do
Biskajského	biskajský	k2eAgInSc2d1	biskajský
zálivu	záliv	k1gInSc2	záliv
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
aby	aby	kYmCp3nS	aby
neodporoval	odporovat	k5eNaImAgMnS	odporovat
přímému	přímý	k2eAgInSc3d1	přímý
rozkazu	rozkaz	k1gInSc3	rozkaz
<g/>
,	,	kIx,	,
zvolil	zvolit	k5eAaPmAgInS	zvolit
kompromisní	kompromisní	k2eAgNnSc4d1	kompromisní
řešení	řešení	k1gNnSc4	řešení
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc4	jeho
loď	loď	k1gFnSc1	loď
plula	plout	k5eAaImAgFnS	plout
spíše	spíše	k9	spíše
k	k	k7c3	k
Irsku	Irsko	k1gNnSc3	Irsko
než	než	k8xS	než
k	k	k7c3	k
Islandu	Island	k1gInSc3	Island
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
směřovala	směřovat	k5eAaImAgFnS	směřovat
většina	většina	k1gFnSc1	většina
ostatních	ostatní	k2eAgFnPc2d1	ostatní
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uvedený	uvedený	k2eAgInSc1d1	uvedený
stav	stav	k1gInSc1	stav
trval	trvat	k5eAaImAgInS	trvat
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Admiralita	admiralita	k1gFnSc1	admiralita
však	však	k9	však
nadále	nadále	k6eAd1	nadále
analyzovala	analyzovat	k5eAaImAgFnS	analyzovat
zachycené	zachycený	k2eAgInPc4d1	zachycený
signály	signál	k1gInPc4	signál
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
za	za	k7c2	za
spolupráce	spolupráce	k1gFnSc2	spolupráce
předních	přední	k2eAgMnPc2d1	přední
admiralitních	admiralitní	k2eAgMnPc2d1	admiralitní
stratégů	stratég	k1gMnPc2	stratég
<g/>
,	,	kIx,	,
kapitánů	kapitán	k1gMnPc2	kapitán
C.	C.	kA	C.
S.	S.	kA	S.
Danielse	Danielse	k1gFnSc1	Danielse
a	a	k8xC	a
R.	R.	kA	R.
A.	A.	kA	A.
B.	B.	kA	B.
Edwarse	Edwarse	k1gFnSc1	Edwarse
také	také	k9	také
možné	možný	k2eAgFnPc1d1	možná
strategie	strategie	k1gFnPc1	strategie
úniku	únik	k1gInSc2	únik
Bismarcka	Bismarcko	k1gNnSc2	Bismarcko
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
ještě	ještě	k6eAd1	ještě
zaslala	zaslat	k5eAaPmAgFnS	zaslat
kapitánu	kapitán	k1gMnSc3	kapitán
Rodneye	Rodney	k1gInSc2	Rodney
<g/>
,	,	kIx,	,
Dalrymple-Hamiltonovi	Dalrymple-Hamiltonův	k2eAgMnPc1d1	Dalrymple-Hamiltonův
příkaz	příkaz	k1gInSc4	příkaz
nadále	nadále	k6eAd1	nadále
střežit	střežit	k5eAaImF	střežit
přístupy	přístup	k1gInPc4	přístup
k	k	k7c3	k
francouzským	francouzský	k2eAgInPc3d1	francouzský
přístavům	přístav	k1gInPc3	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
15	[number]	k4	15
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
obdržel	obdržet	k5eAaPmAgMnS	obdržet
sir	sir	k1gMnSc1	sir
John	John	k1gMnSc1	John
Tovey	Tovea	k1gFnSc2	Tovea
konečný	konečný	k2eAgInSc1d1	konečný
výsledek	výsledek	k1gInSc1	výsledek
jejich	jejich	k3xOp3gFnPc2	jejich
analýz	analýza	k1gFnPc2	analýza
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bismarck	Bismarck	k1gMnSc1	Bismarck
přece	přece	k9	přece
jen	jen	k9	jen
pluje	plout	k5eAaImIp3nS	plout
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
nejpravděpodobnější	pravděpodobný	k2eAgFnSc1d3	nejpravděpodobnější
poloha	poloha	k1gFnSc1	poloha
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
okruhu	okruh	k1gInSc6	okruh
padesáti	padesát	k4xCc2	padesát
mil	míle	k1gFnPc2	míle
od	od	k7c2	od
pozice	pozice	k1gFnSc2	pozice
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vlajkové	vlajkový	k2eAgFnSc6d1	vlajková
lodi	loď	k1gFnSc6	loď
začali	začít	k5eAaPmAgMnP	začít
také	také	k9	také
přezkoumávat	přezkoumávat	k5eAaImF	přezkoumávat
dříve	dříve	k6eAd2	dříve
určenou	určený	k2eAgFnSc4d1	určená
polohu	poloha	k1gFnSc4	poloha
a	a	k8xC	a
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
původní	původní	k2eAgNnSc4d1	původní
určení	určení	k1gNnSc4	určení
polohy	poloha	k1gFnSc2	poloha
bylo	být	k5eAaImAgNnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
chybné	chybný	k2eAgNnSc1d1	chybné
<g/>
,	,	kIx,	,
na	na	k7c4	na
chybu	chyba	k1gFnSc4	chyba
ale	ale	k8xC	ale
nepřišel	přijít	k5eNaPmAgMnS	přijít
ani	ani	k8xC	ani
nikdo	nikdo	k3yNnSc1	nikdo
v	v	k7c6	v
admiralitě	admiralita	k1gFnSc6	admiralita
<g/>
.	.	kIx.	.
</s>
<s>
Příčina	příčina	k1gFnSc1	příčina
chyby	chyba	k1gFnSc2	chyba
ležela	ležet	k5eAaImAgFnS	ležet
v	v	k7c6	v
nekompatibilitě	nekompatibilita	k1gFnSc6	nekompatibilita
map	mapa	k1gFnPc2	mapa
<g/>
,	,	kIx,	,
používaných	používaný	k2eAgFnPc2d1	používaná
radisty	radista	k1gMnPc7	radista
a	a	k8xC	a
lodními	lodní	k2eAgMnPc7d1	lodní
navigátory	navigátor	k1gMnPc7	navigátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Admirál	admirál	k1gMnSc1	admirál
Tovey	Tovea	k1gFnSc2	Tovea
se	se	k3xPyFc4	se
dotázal	dotázat	k5eAaPmAgInS	dotázat
na	na	k7c4	na
další	další	k2eAgInSc4d1	další
postup	postup	k1gInSc4	postup
admirality	admiralita	k1gFnSc2	admiralita
a	a	k8xC	a
když	když	k8xS	když
stále	stále	k6eAd1	stále
nedostal	dostat	k5eNaPmAgMnS	dostat
odpověď	odpověď	k1gFnSc4	odpověď
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
se	se	k3xPyFc4	se
v	v	k7c6	v
18	[number]	k4	18
<g/>
:	:	kIx,	:
<g/>
10	[number]	k4	10
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
lodě	loď	k1gFnSc2	loď
obrátit	obrátit	k5eAaPmF	obrátit
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
19	[number]	k4	19
<g/>
:	:	kIx,	:
<g/>
15	[number]	k4	15
pak	pak	k6eAd1	pak
dostal	dostat	k5eAaPmAgMnS	dostat
oficiální	oficiální	k2eAgNnSc4d1	oficiální
potvrzení	potvrzení	k1gNnSc4	potvrzení
admirality	admiralita	k1gFnSc2	admiralita
o	o	k7c6	o
jižní	jižní	k2eAgFnSc6d1	jižní
poloze	poloha	k1gFnSc6	poloha
německé	německý	k2eAgFnSc2d1	německá
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Britské	britský	k2eAgFnPc1d1	britská
lodi	loď	k1gFnPc1	loď
tak	tak	k6eAd1	tak
téměř	téměř	k6eAd1	téměř
7	[number]	k4	7
hodin	hodina	k1gFnPc2	hodina
pluly	plout	k5eAaImAgInP	plout
nesprávným	správný	k2eNgInSc7d1	nesprávný
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Díky	dík	k1gInPc1	dík
této	tento	k3xDgFnSc2	tento
skutečnosti	skutečnost	k1gFnSc2	skutečnost
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
nedostatek	nedostatek	k1gInSc1	nedostatek
paliva	palivo	k1gNnSc2	palivo
neúnosný	únosný	k2eNgInSc1d1	neúnosný
na	na	k7c6	na
dalších	další	k2eAgFnPc6d1	další
britských	britský	k2eAgFnPc6d1	britská
lodích	loď	k1gFnPc6	loď
-	-	kIx~	-
křižník	křižník	k1gInSc4	křižník
Suffolk	Suffolka	k1gFnPc2	Suffolka
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgInS	muset
obrátit	obrátit	k5eAaPmF	obrátit
pro	pro	k7c4	pro
palivo	palivo	k1gNnSc4	palivo
k	k	k7c3	k
Islandu	Island	k1gInSc3	Island
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
2	[number]	k4	2
<g/>
.	.	kIx.	.
křižníková	křižníkový	k2eAgFnSc1d1	křižníkový
eskadra	eskadra	k1gFnSc1	eskadra
a	a	k8xC	a
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
i	i	k9	i
letadlová	letadlový	k2eAgFnSc1d1	letadlová
loď	loď	k1gFnSc1	loď
Victorious	Victorious	k1gInSc1	Victorious
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
torpédoborce	torpédoborec	k1gInPc4	torpédoborec
musel	muset	k5eAaImAgInS	muset
odeslat	odeslat	k5eAaPmF	odeslat
k	k	k7c3	k
doplnění	doplnění	k1gNnSc3	doplnění
pohonných	pohonný	k2eAgFnPc2d1	pohonná
hmot	hmota	k1gFnPc2	hmota
i	i	k9	i
kapitán	kapitán	k1gMnSc1	kapitán
Darlymple-Hamilton	Darlymple-Hamilton	k1gInSc1	Darlymple-Hamilton
na	na	k7c4	na
Rodney	Rodne	k1gMnPc4	Rodne
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obnovení	obnovení	k1gNnSc1	obnovení
kontaktu	kontakt	k1gInSc2	kontakt
a	a	k8xC	a
útok	útok	k1gInSc4	útok
torpédy	torpédo	k1gNnPc7	torpédo
===	===	k?	===
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
britské	britský	k2eAgFnPc1d1	britská
lodi	loď	k1gFnPc1	loď
blížily	blížit	k5eAaImAgFnP	blížit
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
zvýšeného	zvýšený	k2eAgInSc2d1	zvýšený
výskytu	výskyt	k1gInSc2	výskyt
německých	německý	k2eAgFnPc2d1	německá
ponorek	ponorka	k1gFnPc2	ponorka
<g/>
,	,	kIx,	,
začínal	začínat	k5eAaImAgMnS	začínat
na	na	k7c4	na
britské	britský	k2eAgMnPc4d1	britský
velitele	velitel	k1gMnPc4	velitel
doléhat	doléhat	k5eAaImF	doléhat
citelně	citelně	k6eAd1	citelně
nedostatek	nedostatek	k1gInSc4	nedostatek
torpédoborců	torpédoborec	k1gInPc2	torpédoborec
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
admiralita	admiralita	k1gFnSc1	admiralita
dala	dát	k5eAaPmAgFnS	dát
rozkaz	rozkaz	k1gInSc4	rozkaz
torpédoborci	torpédoborec	k1gMnSc3	torpédoborec
HMS	HMS	kA	HMS
Jupiter	Jupiter	k1gInSc1	Jupiter
<g/>
,	,	kIx,	,
plujícímu	plující	k2eAgInSc3d1	plující
v	v	k7c6	v
Irském	irský	k2eAgNnSc6d1	irské
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
k	k	k7c3	k
hlavním	hlavní	k2eAgFnPc3d1	hlavní
silám	síla	k1gFnPc3	síla
připojil	připojit	k5eAaPmAgMnS	připojit
<g/>
.	.	kIx.	.
</s>
<s>
Uvolnila	uvolnit	k5eAaPmAgFnS	uvolnit
dále	daleko	k6eAd2	daleko
5	[number]	k4	5
torpédoborců	torpédoborec	k1gInPc2	torpédoborec
4	[number]	k4	4
<g/>
.	.	kIx.	.
torpédoborcové	torpédoborcový	k2eAgFnSc2d1	torpédoborcový
flotily	flotila	k1gFnSc2	flotila
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
kapitána	kapitán	k1gMnSc2	kapitán
Philippa	Philipp	k1gMnSc2	Philipp
Viana	Viana	k1gFnSc1	Viana
sestájící	sestájící	k1gFnSc1	sestájící
z	z	k7c2	z
torpédoborců	torpédoborec	k1gInPc2	torpédoborec
HMS	HMS	kA	HMS
Cossack	Cossack	k1gInSc1	Cossack
<g/>
,	,	kIx,	,
HMS	HMS	kA	HMS
Maori	Maori	k1gNnSc1	Maori
<g/>
,	,	kIx,	,
HMS	HMS	kA	HMS
Zulu	Zulu	k1gMnSc1	Zulu
<g/>
,	,	kIx,	,
HMS	HMS	kA	HMS
Sikh	sikh	k1gMnSc1	sikh
a	a	k8xC	a
polského	polský	k2eAgInSc2d1	polský
Piorunu	Piorun	k1gInSc2	Piorun
z	z	k7c2	z
doprovodu	doprovod	k1gInSc2	doprovod
konvoje	konvoj	k1gInSc2	konvoj
WS	WS	kA	WS
8B	[number]	k4	8B
plujícího	plující	k2eAgInSc2d1	plující
z	z	k7c2	z
Británie	Británie	k1gFnSc2	Británie
na	na	k7c4	na
Blízký	blízký	k2eAgInSc4d1	blízký
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
tak	tak	k6eAd1	tak
ve	v	k7c4	v
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
odpluly	odplout	k5eAaPmAgFnP	odplout
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
zhruba	zhruba	k6eAd1	zhruba
300	[number]	k4	300
mil	míle	k1gFnPc2	míle
vzdálené	vzdálený	k2eAgFnSc2d1	vzdálená
oblasti	oblast	k1gFnSc2	oblast
pravděpodobného	pravděpodobný	k2eAgInSc2d1	pravděpodobný
výskytu	výskyt	k1gInSc2	výskyt
německé	německý	k2eAgFnSc2d1	německá
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
admirál	admirál	k1gMnSc1	admirál
Sommerville	Sommerville	k1gFnSc2	Sommerville
<g/>
,	,	kIx,	,
velitel	velitel	k1gMnSc1	velitel
Svazu	svaz	k1gInSc2	svaz
H	H	kA	H
s	s	k7c7	s
letadlovou	letadlový	k2eAgFnSc7d1	letadlová
lodí	loď	k1gFnSc7	loď
Ark	Ark	k1gMnSc1	Ark
Royal	Royal	k1gMnSc1	Royal
také	také	k9	také
snažil	snažit	k5eAaImAgMnS	snažit
přispět	přispět	k5eAaPmF	přispět
k	k	k7c3	k
nalezení	nalezení	k1gNnSc3	nalezení
Bismarcka	Bismarcko	k1gNnSc2	Bismarcko
<g/>
.	.	kIx.	.
</s>
<s>
Lijáky	liják	k1gInPc1	liják
<g/>
,	,	kIx,	,
rozbouřené	rozbouřený	k2eAgNnSc1d1	rozbouřené
moře	moře	k1gNnSc1	moře
a	a	k8xC	a
nízko	nízko	k6eAd1	nízko
se	se	k3xPyFc4	se
táhnoucí	táhnoucí	k2eAgFnSc1d1	táhnoucí
hradba	hradba	k1gFnSc1	hradba
mraků	mrak	k1gInPc2	mrak
znemožnily	znemožnit	k5eAaPmAgInP	znemožnit
start	start	k1gInSc4	start
letadel	letadlo	k1gNnPc2	letadlo
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
den	den	k1gInSc4	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
a	a	k8xC	a
tak	tak	k9	tak
z	z	k7c2	z
paluby	paluba	k1gFnSc2	paluba
Ark	Ark	k1gFnSc4	Ark
Royal	Royal	k1gInSc4	Royal
vzlétlo	vzlétnout	k5eAaPmAgNnS	vzlétnout
10	[number]	k4	10
průzkumných	průzkumný	k2eAgInPc2d1	průzkumný
letounů	letoun	k1gInPc2	letoun
až	až	k9	až
26	[number]	k4	26
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
kolem	kolem	k7c2	kolem
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
ráno	ráno	k6eAd1	ráno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgFnPc6	dva
hodinách	hodina	k1gFnPc6	hodina
čekání	čekání	k1gNnSc2	čekání
co	co	k8xS	co
průzkumné	průzkumný	k2eAgInPc1d1	průzkumný
letouny	letoun	k1gInPc1	letoun
naleznou	naleznout	k5eAaPmIp3nP	naleznout
<g/>
,	,	kIx,	,
náhle	náhle	k6eAd1	náhle
britští	britský	k2eAgMnPc1d1	britský
radisté	radista	k1gMnPc1	radista
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
hlášení	hlášení	k1gNnSc2	hlášení
oznamující	oznamující	k2eAgFnSc7d1	oznamující
neznámou	neznámá	k1gFnSc7	neznámá
bitevní	bitevní	k2eAgFnSc4d1	bitevní
loď	loď	k1gFnSc4	loď
na	na	k7c6	na
souřadnicích	souřadnice	k1gFnPc6	souřadnice
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
provedení	provedení	k1gNnSc6	provedení
výpočtů	výpočet	k1gInPc2	výpočet
ohledně	ohledně	k7c2	ohledně
aktuálních	aktuální	k2eAgFnPc2d1	aktuální
poloh	poloha	k1gFnPc2	poloha
britských	britský	k2eAgFnPc2d1	britská
lodí	loď	k1gFnPc2	loď
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
o	o	k7c4	o
žádnou	žádný	k3yNgFnSc4	žádný
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
nejedná	jednat	k5eNaImIp3nS	jednat
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
nebylo	být	k5eNaImAgNnS	být
na	na	k7c6	na
britských	britský	k2eAgFnPc6d1	britská
lodích	loď	k1gFnPc6	loď
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
o	o	k7c4	o
hlášení	hlášení	k1gNnSc4	hlášení
jakého	jaký	k3yIgInSc2	jaký
letounu	letoun	k1gInSc2	letoun
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
-	-	kIx~	-
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
hlášení	hlášení	k1gNnSc4	hlášení
letounu	letoun	k1gInSc2	letoun
Catalina	Catalin	k2eAgFnSc1d1	Catalina
Mk	Mk	k1gFnSc1	Mk
<g/>
.	.	kIx.	.
<g/>
I	I	kA	I
(	(	kIx(	(
<g/>
WQ-Z	WQ-Z	k1gMnSc1	WQ-Z
<g/>
,	,	kIx,	,
W	W	kA	W
<g/>
8406	[number]	k4	8406
<g/>
)	)	kIx)	)
od	od	k7c2	od
209	[number]	k4	209
<g/>
.	.	kIx.	.
squadrony	squadron	k1gInPc1	squadron
RAF	raf	k0	raf
vyslaného	vyslaný	k2eAgInSc2d1	vyslaný
Pobřežním	pobřežní	k2eAgNnSc7d1	pobřežní
leteckým	letecký	k2eAgNnSc7d1	letecké
velitelstvím	velitelství	k1gNnSc7	velitelství
ze	z	k7c2	z
severoirské	severoirský	k2eAgFnSc2d1	severoirská
základny	základna	k1gFnSc2	základna
poblíž	poblíž	k6eAd1	poblíž
Lough	Lough	k1gInSc4	Lough
Erne	Ern	k1gFnSc2	Ern
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
letoun	letoun	k1gInSc1	letoun
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
však	však	k9	však
po	po	k7c6	po
následném	následný	k2eAgNnSc6d1	následné
přiblížení	přiblížení	k1gNnSc6	přiblížení
k	k	k7c3	k
německé	německý	k2eAgFnSc3d1	německá
lodi	loď	k1gFnSc3	loď
zasažen	zasáhnout	k5eAaPmNgMnS	zasáhnout
protiletadlovou	protiletadlový	k2eAgFnSc7d1	protiletadlová
palbou	palba	k1gFnSc7	palba
a	a	k8xC	a
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
poškození	poškození	k1gNnSc2	poškození
vysílačky	vysílačka	k1gFnSc2	vysílačka
nemohl	moct	k5eNaImAgMnS	moct
své	svůj	k3xOyFgNnSc4	svůj
hlášení	hlášení	k1gNnSc4	hlášení
upřesnit	upřesnit	k5eAaPmF	upřesnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c4	o
půl	půl	k1xP	půl
hodiny	hodina	k1gFnSc2	hodina
později	pozdě	k6eAd2	pozdě
polohu	poloha	k1gFnSc4	poloha
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
průzkumných	průzkumný	k2eAgInPc2d1	průzkumný
letounů	letoun	k1gInPc2	letoun
Ark	Ark	k1gMnSc1	Ark
Royal	Royal	k1gMnSc1	Royal
<g/>
,	,	kIx,	,
oznámil	oznámit	k5eAaPmAgMnS	oznámit
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
křižník	křižník	k1gInSc4	křižník
a	a	k8xC	a
proto	proto	k8xC	proto
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
obavy	obava	k1gFnPc1	obava
<g/>
,	,	kIx,	,
zdali	zdali	k8xS	zdali
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
Prinz	Prinz	k1gInSc4	Prinz
Eugen	Eugen	k2eAgInSc4d1	Eugen
-	-	kIx~	-
obě	dva	k4xCgFnPc1	dva
lodi	loď	k1gFnPc1	loď
měly	mít	k5eAaImAgFnP	mít
velmi	velmi	k6eAd1	velmi
podobnou	podobný	k2eAgFnSc4d1	podobná
konstrukci	konstrukce	k1gFnSc4	konstrukce
i	i	k8xC	i
umístění	umístění	k1gNnSc4	umístění
a	a	k8xC	a
počet	počet	k1gInSc4	počet
dělových	dělový	k2eAgFnPc2d1	dělová
věží	věž	k1gFnPc2	věž
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
osudné	osudný	k2eAgFnSc3d1	osudná
chybě	chyba	k1gFnSc3	chyba
admirála	admirál	k1gMnSc2	admirál
Hollanda	Hollanda	k1gFnSc1	Hollanda
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
vedení	vedení	k1gNnSc2	vedení
bitvy	bitva	k1gFnSc2	bitva
v	v	k7c6	v
Dánském	dánský	k2eAgInSc6d1	dánský
průlivu	průliv	k1gInSc6	průliv
<g/>
.	.	kIx.	.
</s>
<s>
Ark	Ark	k?	Ark
Royal	Royal	k1gMnSc1	Royal
proto	proto	k8xC	proto
vyslal	vyslat	k5eAaPmAgMnS	vyslat
dvě	dva	k4xCgNnPc1	dva
další	další	k2eAgNnPc1d1	další
letadla	letadlo	k1gNnPc1	letadlo
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
nejasně	jasně	k6eNd1	jasně
identifikovanou	identifikovaný	k2eAgFnSc4d1	identifikovaná
loď	loď	k1gFnSc4	loď
nespustit	spustit	k5eNaPmF	spustit
z	z	k7c2	z
dohledu	dohled	k1gInSc2	dohled
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
vracela	vracet	k5eAaImAgFnS	vracet
na	na	k7c4	na
palubu	paluba	k1gFnSc4	paluba
dříve	dříve	k6eAd2	dříve
vyslaná	vyslaný	k2eAgNnPc4d1	vyslané
letadla	letadlo	k1gNnPc4	letadlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
přímý	přímý	k2eAgInSc4d1	přímý
rozhovor	rozhovor	k1gInSc4	rozhovor
s	s	k7c7	s
letci	letec	k1gMnPc7	letec
po	po	k7c6	po
přistání	přistání	k1gNnSc6	přistání
nepřinesl	přinést	k5eNaPmAgInS	přinést
vyjasnění	vyjasnění	k1gNnSc4	vyjasnění
situace	situace	k1gFnSc2	situace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
Bismarck	Bismarck	k1gMnSc1	Bismarck
<g/>
,	,	kIx,	,
za	za	k7c4	za
který	který	k3yIgInSc1	který
na	na	k7c6	na
britských	britský	k2eAgFnPc6d1	britská
lodích	loď	k1gFnPc6	loď
přece	přece	k9	přece
jen	jen	k9	jen
nejasně	jasně	k6eNd1	jasně
identifikovanou	identifikovaný	k2eAgFnSc4d1	identifikovaná
loď	loď	k1gFnSc4	loď
pokládali	pokládat	k5eAaImAgMnP	pokládat
<g/>
,	,	kIx,	,
nacházel	nacházet	k5eAaImAgMnS	nacházet
zhruba	zhruba	k6eAd1	zhruba
50	[number]	k4	50
mil	míle	k1gFnPc2	míle
před	před	k7c7	před
hlavními	hlavní	k2eAgFnPc7d1	hlavní
silami	síla	k1gFnPc7	síla
Home	Home	k1gNnSc2	Home
Fleet	Fleeta	k1gFnPc2	Fleeta
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
na	na	k7c4	na
Brest	Brest	k1gInSc4	Brest
<g/>
,	,	kIx,	,
vypočetli	vypočíst	k5eAaPmAgMnP	vypočíst
Britové	Brit	k1gMnPc1	Brit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
do	do	k7c2	do
setmění	setmění	k1gNnSc2	setmění
snížit	snížit	k5eAaPmF	snížit
jeho	jeho	k3xOp3gFnSc4	jeho
rychlost	rychlost	k1gFnSc4	rychlost
na	na	k7c4	na
nejvýše	vysoce	k6eAd3	vysoce
15	[number]	k4	15
uzlů	uzel	k1gInPc2	uzel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
za	za	k7c4	za
rozednění	rozednění	k1gNnSc4	rozednění
britské	britský	k2eAgFnSc2d1	britská
lodi	loď	k1gFnSc2	loď
neocitly	ocitnout	k5eNaPmAgFnP	ocitnout
v	v	k7c6	v
doletu	dolet	k1gInSc6	dolet
německých	německý	k2eAgNnPc2d1	německé
letadel	letadlo	k1gNnPc2	letadlo
<g/>
,	,	kIx,	,
startujících	startující	k2eAgFnPc2d1	startující
z	z	k7c2	z
letišť	letiště	k1gNnPc2	letiště
na	na	k7c6	na
francouzském	francouzský	k2eAgNnSc6d1	francouzské
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediné	k1gNnSc1	jediné
jak	jak	k6eAd1	jak
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
přimět	přimět	k5eAaPmF	přimět
Bismarcka	Bismarcko	k1gNnPc4	Bismarcko
ke	k	k7c3	k
zpomalení	zpomalení	k1gNnSc3	zpomalení
byl	být	k5eAaImAgInS	být
torpédový	torpédový	k2eAgInSc4d1	torpédový
útok	útok	k1gInSc4	útok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
šanci	šance	k1gFnSc4	šance
měly	mít	k5eAaImAgInP	mít
letouny	letoun	k1gInPc1	letoun
z	z	k7c2	z
Ark	Ark	k1gFnSc2	Ark
Royal	Royal	k1gInSc1	Royal
a	a	k8xC	a
ty	ten	k3xDgInPc1	ten
vzlétly	vzlétnout	k5eAaPmAgInP	vzlétnout
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
paluby	paluba	k1gFnSc2	paluba
za	za	k7c2	za
velmi	velmi	k6eAd1	velmi
obtížných	obtížný	k2eAgFnPc2d1	obtížná
podmínek	podmínka	k1gFnPc2	podmínka
v	v	k7c6	v
silném	silný	k2eAgNnSc6d1	silné
vlnobití	vlnobití	k1gNnSc6	vlnobití
kolem	kolem	k7c2	kolem
14	[number]	k4	14
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
13	[number]	k4	13
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
však	však	k8xC	však
admirál	admirál	k1gMnSc1	admirál
Somerville	Somerville	k1gNnSc2	Somerville
dal	dát	k5eAaPmAgMnS	dát
rozkaz	rozkaz	k1gInSc4	rozkaz
křižníku	křižník	k1gInSc2	křižník
Sheffield	Sheffield	k1gMnSc1	Sheffield
aby	aby	kYmCp3nS	aby
po	po	k7c6	po
německé	německý	k2eAgFnSc6d1	německá
lodi	loď	k1gFnSc6	loď
pátral	pátrat	k5eAaImAgMnS	pátrat
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
průzkumných	průzkumný	k2eAgInPc6d1	průzkumný
letounech	letoun	k1gInPc6	letoun
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
zpráva	zpráva	k1gFnSc1	zpráva
dorazila	dorazit	k5eAaPmAgFnS	dorazit
na	na	k7c4	na
Ark	Ark	k1gFnPc4	Ark
Royal	Royal	k1gInSc4	Royal
kolem	kolem	k7c2	kolem
14	[number]	k4	14
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
její	její	k3xOp3gNnSc1	její
rozšifrovávání	rozšifrovávání	k1gNnSc1	rozšifrovávání
se	se	k3xPyFc4	se
však	však	k9	však
zdrželo	zdržet	k5eAaPmAgNnS	zdržet
a	a	k8xC	a
zpráva	zpráva	k1gFnSc1	zpráva
tak	tak	k6eAd1	tak
dorazila	dorazit	k5eAaPmAgFnS	dorazit
na	na	k7c4	na
můstek	můstek	k1gInSc4	můstek
až	až	k6eAd1	až
po	po	k7c6	po
vzletu	vzlet	k1gInSc6	vzlet
bombardérů	bombardér	k1gInPc2	bombardér
-	-	kIx~	-
kolem	kolem	k7c2	kolem
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
-	-	kIx~	-
takže	takže	k8xS	takže
letci	letec	k1gMnPc1	letec
nebyli	být	k5eNaImAgMnP	být
o	o	k7c4	o
možné	možný	k2eAgFnPc4d1	možná
vlastní	vlastní	k2eAgFnPc4d1	vlastní
lodi	loď	k1gFnPc4	loď
poblíž	poblíž	k6eAd1	poblíž
cílové	cílový	k2eAgFnPc4d1	cílová
oblasti	oblast	k1gFnPc4	oblast
informováni	informován	k2eAgMnPc1d1	informován
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
již	již	k6eAd1	již
měl	mít	k5eAaImAgInS	mít
útok	útok	k1gInSc1	útok
na	na	k7c4	na
Bismarcka	Bismarcko	k1gNnPc4	Bismarcko
začít	začít	k5eAaPmF	začít
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
z	z	k7c2	z
paluby	paluba	k1gFnSc2	paluba
Ark	Ark	k1gFnSc4	Ark
Royal	Royal	k1gInSc4	Royal
odešlo	odejít	k5eAaPmAgNnS	odejít
varování	varování	k1gNnSc1	varování
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
řeči	řeč	k1gFnSc6	řeč
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pozor	pozor	k1gInSc1	pozor
na	na	k7c4	na
Sheffield	Sheffield	k1gInSc4	Sheffield
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
však	však	k9	však
již	již	k6eAd1	již
pozdě	pozdě	k6eAd1	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Sheffield	Sheffield	k1gMnSc1	Sheffield
sloužil	sloužit	k5eAaImAgMnS	sloužit
letcům	letec	k1gMnPc3	letec
z	z	k7c2	z
Ark	Ark	k1gFnSc2	Ark
Royal	Royal	k1gInSc1	Royal
často	často	k6eAd1	často
jako	jako	k9	jako
cvičný	cvičný	k2eAgInSc4d1	cvičný
cíl	cíl	k1gInSc4	cíl
<g/>
,	,	kIx,	,
i	i	k8xC	i
tak	tak	k8xS	tak
ho	on	k3xPp3gMnSc4	on
letci	letec	k1gMnPc1	letec
zaměnili	zaměnit	k5eAaPmAgMnP	zaměnit
za	za	k7c4	za
německou	německý	k2eAgFnSc4d1	německá
bitevní	bitevní	k2eAgFnSc4d1	bitevní
loď	loď	k1gFnSc4	loď
a	a	k8xC	a
provedli	provést	k5eAaPmAgMnP	provést
na	na	k7c4	na
něj	on	k3xPp3gInSc4	on
nálet	nálet	k1gInSc4	nálet
<g/>
.	.	kIx.	.
</s>
<s>
Sheffield	Sheffield	k6eAd1	Sheffield
se	se	k3xPyFc4	se
všem	všecek	k3xTgMnPc3	všecek
torpédům	torpédo	k1gNnPc3	torpédo
vyhnul	vyhnout	k5eAaPmAgMnS	vyhnout
a	a	k8xC	a
při	při	k7c6	při
té	ten	k3xDgFnSc6	ten
příležitosti	příležitost	k1gFnSc6	příležitost
se	se	k3xPyFc4	se
projevily	projevit	k5eAaPmAgFnP	projevit
technické	technický	k2eAgFnPc1d1	technická
chyby	chyba	k1gFnPc1	chyba
nově	nově	k6eAd1	nově
instalovaných	instalovaný	k2eAgInPc2d1	instalovaný
magnetických	magnetický	k2eAgInPc2d1	magnetický
detonátorů	detonátor	k1gInPc2	detonátor
torpéd	torpédo	k1gNnPc2	torpédo
-	-	kIx~	-
řada	řada	k1gFnSc1	řada
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
explodovala	explodovat	k5eAaBmAgFnS	explodovat
už	už	k9	už
při	při	k7c6	při
dopadu	dopad	k1gInSc6	dopad
na	na	k7c4	na
hladinu	hladina	k1gFnSc4	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
bombardérů	bombardér	k1gInPc2	bombardér
na	na	k7c4	na
loď	loď	k1gFnSc4	loď
započalo	započnout	k5eAaPmAgNnS	započnout
ihned	ihned	k6eAd1	ihned
znovuvyzbrojování	znovuvyzbrojování	k1gNnSc4	znovuvyzbrojování
letadel	letadlo	k1gNnPc2	letadlo
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
byly	být	k5eAaImAgInP	být
také	také	k9	také
magnetické	magnetický	k2eAgInPc1d1	magnetický
zápalníky	zápalník	k1gInPc1	zápalník
vyměňovány	vyměňován	k2eAgInPc1d1	vyměňován
za	za	k7c4	za
kontaktní	kontaktní	k2eAgNnSc4d1	kontaktní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
18	[number]	k4	18
hodinou	hodina	k1gFnSc7	hodina
obnovil	obnovit	k5eAaPmAgInS	obnovit
Sheffield	Sheffield	k1gInSc1	Sheffield
vizuální	vizuální	k2eAgInSc4d1	vizuální
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
německou	německý	k2eAgFnSc7d1	německá
bitevní	bitevní	k2eAgFnSc7d1	bitevní
lodí	loď	k1gFnSc7	loď
a	a	k8xC	a
v	v	k7c6	v
19	[number]	k4	19
<g/>
:	:	kIx,	:
<g/>
19	[number]	k4	19
z	z	k7c2	z
paluby	paluba	k1gFnSc2	paluba
Ark	Ark	k1gFnSc1	Ark
Royal	Royal	k1gInSc4	Royal
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
další	další	k2eAgFnSc1d1	další
útočná	útočný	k2eAgFnSc1d1	útočná
vlna	vlna	k1gFnSc1	vlna
15	[number]	k4	15
torpédových	torpédový	k2eAgInPc2d1	torpédový
bombardérů	bombardér	k1gInPc2	bombardér
(	(	kIx(	(
<g/>
všech	všecek	k3xTgInPc2	všecek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
útok	útok	k1gInSc1	útok
se	se	k3xPyFc4	se
britským	britský	k2eAgMnPc3d1	britský
letcům	letec	k1gMnPc3	letec
zdařil	zdařit	k5eAaPmAgInS	zdařit
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
při	při	k7c6	při
něm	on	k3xPp3gNnSc6	on
zaznamenány	zaznamenat	k5eAaPmNgInP	zaznamenat
dva	dva	k4xCgInPc1	dva
zásahy	zásah	k1gInPc1	zásah
-	-	kIx~	-
podle	podle	k7c2	podle
německých	německý	k2eAgInPc2d1	německý
údajů	údaj	k1gInPc2	údaj
ve	v	k7c6	v
21	[number]	k4	21
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
jeden	jeden	k4xCgInSc1	jeden
do	do	k7c2	do
středu	střed	k1gInSc2	střed
lodi	loď	k1gFnSc2	loď
a	a	k8xC	a
druhý	druhý	k4xOgInSc4	druhý
ve	v	k7c6	v
21	[number]	k4	21
<g/>
:	:	kIx,	:
<g/>
15	[number]	k4	15
na	na	k7c6	na
zádi	záď	k1gFnSc6	záď
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
Bismarck	Bismarck	k1gInSc1	Bismarck
přestal	přestat	k5eAaPmAgInS	přestat
být	být	k5eAaImF	být
ovladatelný	ovladatelný	k2eAgInSc1d1	ovladatelný
<g/>
,	,	kIx,	,
po	po	k7c6	po
zásahu	zásah	k1gInSc6	zásah
opsal	opsat	k5eAaPmAgMnS	opsat
dva	dva	k4xCgInPc4	dva
velké	velký	k2eAgInPc4d1	velký
kruhy	kruh	k1gInPc4	kruh
a	a	k8xC	a
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
dále	daleko	k6eAd2	daleko
směrem	směr	k1gInSc7	směr
na	na	k7c6	na
SSZ	SSZ	kA	SSZ
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
změna	změna	k1gFnSc1	změna
kurzu	kurz	k1gInSc2	kurz
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
Sheffieldu	Sheffielda	k1gFnSc4	Sheffielda
i	i	k9	i
na	na	k7c6	na
sledujících	sledující	k2eAgNnPc6d1	sledující
letadlech	letadlo	k1gNnPc6	letadlo
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
nebyla	být	k5eNaImAgFnS	být
uspokojivě	uspokojivě	k6eAd1	uspokojivě
vysvětlena	vysvětlen	k2eAgFnSc1d1	vysvětlena
-	-	kIx~	-
první	první	k4xOgNnPc4	první
hlášení	hlášení	k1gNnPc4	hlášení
letců	letec	k1gMnPc2	letec
vracejících	vracející	k2eAgMnPc2d1	vracející
se	se	k3xPyFc4	se
na	na	k7c6	na
Ark	Ark	k1gFnSc6	Ark
Royal	Royal	k1gMnSc1	Royal
mluvila	mluvit	k5eAaImAgFnS	mluvit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
žádný	žádný	k3yNgInSc1	žádný
zásah	zásah	k1gInSc1	zásah
nepodařil	podařit	k5eNaPmAgInS	podařit
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
o	o	k7c6	o
půlnoci	půlnoc	k1gFnSc6	půlnoc
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
Toveyovu	Toveyův	k2eAgFnSc4d1	Toveyův
vlajkovou	vlajkový	k2eAgFnSc4d1	vlajková
loď	loď	k1gFnSc4	loď
doručena	doručit	k5eAaPmNgFnS	doručit
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
úspěchu	úspěch	k1gInSc6	úspěch
náletu	nálet	k1gInSc2	nálet
<g/>
.	.	kIx.	.
</s>
<s>
Toveyova	Toveyův	k2eAgFnSc1d1	Toveyův
skupina	skupina	k1gFnSc1	skupina
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
posílena	posílit	k5eAaPmNgFnS	posílit
o	o	k7c4	o
bitevní	bitevní	k2eAgFnSc4d1	bitevní
loď	loď	k1gFnSc4	loď
Rodney	Rodnea	k1gFnSc2	Rodnea
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
ke	k	k7c3	k
svazu	svaz	k1gInSc3	svaz
připojila	připojit	k5eAaPmAgFnS	připojit
kolem	kolem	k7c2	kolem
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zatímco	zatímco	k8xS	zatímco
probíhaly	probíhat	k5eAaImAgInP	probíhat
nálety	nálet	k1gInPc1	nálet
<g/>
,	,	kIx,	,
kapitán	kapitán	k1gMnSc1	kapitán
Vian	Vian	k1gMnSc1	Vian
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgInPc7	svůj
torpédoborci	torpédoborec	k1gInPc7	torpédoborec
se	se	k3xPyFc4	se
iniciativně	iniciativně	k6eAd1	iniciativně
-	-	kIx~	-
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
bez	bez	k7c2	bez
přímých	přímý	k2eAgInPc2d1	přímý
rozkazů	rozkaz	k1gInPc2	rozkaz
(	(	kIx(	(
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
-	-	kIx~	-
přiblížil	přiblížit	k5eAaPmAgMnS	přiblížit
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
možnou	možný	k2eAgFnSc7d1	možná
rychlostí	rychlost	k1gFnSc7	rychlost
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
Bismarck	Bismarck	k1gMnSc1	Bismarck
nacházel	nacházet	k5eAaImAgMnS	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
končil	končit	k5eAaImAgInS	končit
druhý	druhý	k4xOgInSc4	druhý
nálet	nálet	k1gInSc4	nálet
letadel	letadlo	k1gNnPc2	letadlo
z	z	k7c2	z
Ark	Ark	k1gFnSc2	Ark
Royal	Royal	k1gInSc1	Royal
a	a	k8xC	a
dostal	dostat	k5eAaPmAgInS	dostat
rozkaz	rozkaz	k1gInSc1	rozkaz
sledovat	sledovat	k5eAaImF	sledovat
německou	německý	k2eAgFnSc4d1	německá
loď	loď	k1gFnSc4	loď
celou	celý	k2eAgFnSc4d1	celá
noc	noc	k1gFnSc4	noc
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	k9	aby
příštího	příští	k2eAgInSc2d1	příští
dne	den	k1gInSc2	den
ráno	ráno	k6eAd1	ráno
mohlo	moct	k5eAaImAgNnS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
bitvě	bitva	k1gFnSc3	bitva
s	s	k7c7	s
hlavními	hlavní	k2eAgFnPc7d1	hlavní
silami	síla	k1gFnPc7	síla
Toveyova	Toveyův	k2eAgInSc2d1	Toveyův
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
zpozorován	zpozorován	k2eAgMnSc1d1	zpozorován
byl	být	k5eAaImAgInS	být
Bismarck	Bismarck	k1gInSc1	Bismarck
na	na	k7c6	na
polském	polský	k2eAgInSc6d1	polský
torpédoborci	torpédoborec	k1gInSc6	torpédoborec
Piorun	Piorun	k1gInSc1	Piorun
ve	v	k7c6	v
22	[number]	k4	22
<g/>
:	:	kIx,	:
<g/>
37	[number]	k4	37
<g/>
,	,	kIx,	,
Bismarck	Bismarck	k1gMnSc1	Bismarck
poté	poté	k6eAd1	poté
zahájil	zahájit	k5eAaPmAgMnS	zahájit
na	na	k7c4	na
torpédoborce	torpédoborec	k1gMnSc4	torpédoborec
palbu	palba	k1gFnSc4	palba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
žádný	žádný	k3yNgInSc4	žádný
torpédoborec	torpédoborec	k1gInSc4	torpédoborec
nezasáhla	zasáhnout	k5eNaPmAgFnS	zasáhnout
a	a	k8xC	a
4	[number]	k4	4
torpédoborce	torpédoborec	k1gInSc2	torpédoborec
se	se	k3xPyFc4	se
rozmístily	rozmístit	k5eAaPmAgFnP	rozmístit
do	do	k7c2	do
čtverce	čtverec	k1gInSc2	čtverec
kolem	kolem	k7c2	kolem
německé	německý	k2eAgFnSc2d1	německá
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
loď	loď	k1gFnSc1	loď
velícího	velící	k2eAgMnSc2d1	velící
důstojníka	důstojník	k1gMnSc2	důstojník
kapitána	kapitán	k1gMnSc2	kapitán
Viana	Viana	k1gFnSc1	Viana
Cossack	Cossack	k1gInSc1	Cossack
následovala	následovat	k5eAaImAgFnS	následovat
Bismarcka	Bismarcko	k1gNnPc4	Bismarcko
za	za	k7c4	za
jeho	jeho	k3xOp3gNnSc4	jeho
zádí	zádí	k1gNnSc4	zádí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vian	Vian	k1gMnSc1	Vian
nařídil	nařídit	k5eAaPmAgMnS	nařídit
před	před	k7c4	před
půlnoci	půlnoc	k1gFnPc4	půlnoc
v	v	k7c6	v
téměř	téměř	k6eAd1	téměř
úplné	úplný	k2eAgFnSc3d1	úplná
tmě	tma	k1gFnSc3	tma
koordinovaný	koordinovaný	k2eAgInSc4d1	koordinovaný
útok	útok	k1gInSc4	útok
na	na	k7c4	na
německou	německý	k2eAgFnSc4d1	německá
loď	loď	k1gFnSc4	loď
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
nezdařil	zdařit	k5eNaPmAgMnS	zdařit
<g/>
.	.	kIx.	.
</s>
<s>
Bismarck	Bismarck	k1gMnSc1	Bismarck
zahnal	zahnat	k5eAaPmAgMnS	zahnat
torpédoborce	torpédoborec	k1gInPc4	torpédoborec
na	na	k7c4	na
útěk	útěk	k1gInSc4	útěk
<g/>
.	.	kIx.	.
</s>
<s>
Vian	Vian	k1gMnSc1	Vian
povolil	povolit	k5eAaPmAgMnS	povolit
jednotlivým	jednotlivý	k2eAgMnPc3d1	jednotlivý
lodím	lodit	k5eAaImIp1nS	lodit
své	svůj	k3xOyFgFnSc2	svůj
skupiny	skupina	k1gFnSc2	skupina
útoky	útok	k1gInPc4	útok
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
pěst	pěst	k1gFnSc4	pěst
a	a	k8xC	a
tak	tak	k9	tak
torpédoborce	torpédoborec	k1gInPc1	torpédoborec
prováděly	provádět	k5eAaImAgInP	provádět
samostatné	samostatný	k2eAgInPc4d1	samostatný
nájezdy	nájezd	k1gInPc4	nájezd
až	až	k9	až
do	do	k7c2	do
třetí	třetí	k4xOgFnSc2	třetí
hodiny	hodina	k1gFnSc2	hodina
ranní	ranní	k2eAgFnSc2d1	ranní
<g/>
.	.	kIx.	.
</s>
<s>
Britským	britský	k2eAgMnPc3d1	britský
námořníkům	námořník	k1gMnPc3	námořník
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
zaznamenali	zaznamenat	k5eAaPmAgMnP	zaznamenat
několik	několik	k4yIc4	několik
zásahů	zásah	k1gInPc2	zásah
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Bismarck	Bismarck	k1gMnSc1	Bismarck
ani	ani	k8xC	ani
nezpomalil	zpomalit	k5eNaPmAgMnS	zpomalit
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
záměny	záměna	k1gFnPc4	záměna
explozí	exploze	k1gFnPc2	exploze
torpéd	torpédo	k1gNnPc2	torpédo
s	s	k7c7	s
explozemi	exploze	k1gFnPc7	exploze
granátů	granát	k1gInPc2	granát
a	a	k8xC	a
zajatí	zajatý	k2eAgMnPc1d1	zajatý
němečtí	německý	k2eAgMnPc1d1	německý
námořníci	námořník	k1gMnPc1	námořník
také	také	k9	také
shodně	shodně	k6eAd1	shodně
vypověděli	vypovědět	k5eAaPmAgMnP	vypovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
noci	noc	k1gFnSc2	noc
k	k	k7c3	k
torpédovému	torpédový	k2eAgInSc3d1	torpédový
zásahu	zásah	k1gInSc3	zásah
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
5	[number]	k4	5
hodin	hodina	k1gFnPc2	hodina
dostal	dostat	k5eAaPmAgMnS	dostat
Piorun	Piorun	k1gInSc4	Piorun
(	(	kIx(	(
<g/>
jediná	jediný	k2eAgFnSc1d1	jediná
loď	loď	k1gFnSc1	loď
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ještě	ještě	k9	ještě
na	na	k7c4	na
Bismarcka	Bismarcko	k1gNnPc4	Bismarcko
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
nezaútočila	zaútočit	k5eNaPmAgFnS	zaútočit
<g/>
)	)	kIx)	)
rozkaz	rozkaz	k1gInSc4	rozkaz
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
pro	pro	k7c4	pro
ztenčující	ztenčující	k2eAgNnSc4d1	ztenčující
se	se	k3xPyFc4	se
množství	množství	k1gNnSc4	množství
pohonných	pohonný	k2eAgFnPc2d1	pohonná
hmot	hmota	k1gFnPc2	hmota
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
šesté	šestý	k4xOgFnSc2	šestý
hodiny	hodina	k1gFnSc2	hodina
ranní	ranní	k1gFnSc2	ranní
zaútočil	zaútočit	k5eAaPmAgInS	zaútočit
posledními	poslední	k2eAgMnPc7d1	poslední
torpédy	torpédo	k1gNnPc7	torpédo
torpédoborec	torpédoborec	k1gInSc1	torpédoborec
Maori	Maor	k1gFnPc1	Maor
avšak	avšak	k8xC	avšak
torpéda	torpédo	k1gNnPc1	torpédo
cíl	cíl	k1gInSc4	cíl
netrefila	trefit	k5eNaPmAgFnS	trefit
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
zbývající	zbývající	k2eAgInPc1d1	zbývající
torpédoborce	torpédoborec	k1gInPc1	torpédoborec
opět	opět	k6eAd1	opět
rozmístily	rozmístit	k5eAaPmAgInP	rozmístit
do	do	k7c2	do
čtvercové	čtvercový	k2eAgFnSc2d1	čtvercová
formace	formace	k1gFnSc2	formace
kolem	kolem	k7c2	kolem
Bismarcka	Bismarcko	k1gNnSc2	Bismarcko
a	a	k8xC	a
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
sledování	sledování	k1gNnSc6	sledování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Závěrečná	závěrečný	k2eAgFnSc1d1	závěrečná
bitva	bitva	k1gFnSc1	bitva
===	===	k?	===
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
osud	osud	k1gInSc1	osud
lodi	loď	k1gFnSc2	loď
bude	být	k5eAaImBp3nS	být
brzy	brzy	k6eAd1	brzy
zpečetěn	zpečetit	k5eAaPmNgInS	zpečetit
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
šesté	šestý	k4xOgFnSc2	šestý
hodiny	hodina	k1gFnSc2	hodina
ranní	ranní	k1gFnSc2	ranní
učinil	učinit	k5eAaImAgMnS	učinit
německý	německý	k2eAgMnSc1d1	německý
velitel	velitel	k1gMnSc1	velitel
pokus	pokus	k1gInSc4	pokus
o	o	k7c6	o
vypuštění	vypuštění	k1gNnSc6	vypuštění
hydroplánu	hydroplán	k1gInSc2	hydroplán
Arado	Arado	k1gNnSc4	Arado
Ar	ar	k1gInSc1	ar
196	[number]	k4	196
s	s	k7c7	s
úkolem	úkol	k1gInSc7	úkol
odletět	odletět	k5eAaPmF	odletět
na	na	k7c4	na
pevninu	pevnina	k1gFnSc4	pevnina
s	s	k7c7	s
lodním	lodní	k2eAgInSc7d1	lodní
deníkem	deník	k1gInSc7	deník
vlajkové	vlajkový	k2eAgFnSc2d1	vlajková
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
poškození	poškození	k1gNnSc3	poškození
katapultu	katapult	k1gInSc2	katapult
však	však	k8xC	však
letadla	letadlo	k1gNnSc2	letadlo
nemohla	moct	k5eNaImAgFnS	moct
vzlétnout	vzlétnout	k5eAaPmF	vzlétnout
a	a	k8xC	a
žádost	žádost	k1gFnSc4	žádost
Lütjense	Lütjense	k1gFnSc1	Lütjense
německému	německý	k2eAgNnSc3d1	německé
velitelství	velitelství	k1gNnSc3	velitelství
o	o	k7c6	o
vyslání	vyslání	k1gNnSc6	vyslání
ponorky	ponorka	k1gFnSc2	ponorka
pro	pro	k7c4	pro
převzetí	převzetí	k1gNnSc4	převzetí
lodního	lodní	k2eAgInSc2d1	lodní
deníku	deník	k1gInSc2	deník
také	také	k9	také
nepřineslo	přinést	k5eNaPmAgNnS	přinést
žádný	žádný	k3yNgInSc4	žádný
efekt	efekt	k1gInSc4	efekt
-	-	kIx~	-
kordón	kordón	k1gInSc4	kordón
ponorek	ponorka	k1gFnPc2	ponorka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
Němci	Němec	k1gMnPc1	Němec
vyslali	vyslat	k5eAaPmAgMnP	vyslat
lodi	loď	k1gFnPc4	loď
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
příliš	příliš	k6eAd1	příliš
pozdě	pozdě	k6eAd1	pozdě
byl	být	k5eAaImAgInS	být
ještě	ještě	k6eAd1	ještě
příliš	příliš	k6eAd1	příliš
vzdálen	vzdálit	k5eAaPmNgMnS	vzdálit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zatím	zatím	k6eAd1	zatím
se	se	k3xPyFc4	se
britské	britský	k2eAgFnPc1d1	britská
lodi	loď	k1gFnPc1	loď
přibližovaly	přibližovat	k5eAaImAgFnP	přibližovat
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgFnSc1	první
z	z	k7c2	z
větších	veliký	k2eAgFnPc2d2	veliký
lodí	loď	k1gFnPc2	loď
navázal	navázat	k5eAaPmAgInS	navázat
vizuální	vizuální	k2eAgInSc1d1	vizuální
kontakt	kontakt	k1gInSc1	kontakt
s	s	k7c7	s
Bismarckem	Bismarck	k1gInSc7	Bismarck
kolem	kolem	k7c2	kolem
8	[number]	k4	8
hodiny	hodina	k1gFnSc2	hodina
křižník	křižník	k1gInSc1	křižník
Norfolk	Norfolk	k1gInSc1	Norfolk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
brzy	brzy	k6eAd1	brzy
spatřil	spatřit	k5eAaPmAgMnS	spatřit
i	i	k9	i
hlavní	hlavní	k2eAgFnPc4d1	hlavní
síly	síla	k1gFnPc4	síla
Home	Hom	k1gFnSc2	Hom
Fleet	Fleeta	k1gFnPc2	Fleeta
a	a	k8xC	a
informoval	informovat	k5eAaBmAgInS	informovat
nadále	nadále	k6eAd1	nadále
Toveye	Toveye	k1gInSc1	Toveye
o	o	k7c6	o
poloze	poloha	k1gFnSc6	poloha
německé	německý	k2eAgFnSc2d1	německá
vlajkové	vlajkový	k2eAgFnSc2d1	vlajková
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Rodney	Rodnea	k1gFnPc1	Rodnea
a	a	k8xC	a
King	King	k1gMnSc1	King
George	Georg	k1gMnSc2	Georg
V	v	k7c6	v
měly	mít	k5eAaImAgFnP	mít
Bismarcka	Bismarcko	k1gNnPc1	Bismarcko
na	na	k7c4	na
dohled	dohled	k1gInSc4	dohled
v	v	k7c6	v
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
43	[number]	k4	43
a	a	k8xC	a
o	o	k7c4	o
4	[number]	k4	4
minuty	minuta	k1gFnSc2	minuta
později	pozdě	k6eAd2	pozdě
začala	začít	k5eAaPmAgFnS	začít
střílet	střílet	k5eAaImF	střílet
děla	dělo	k1gNnSc2	dělo
Rodney	Rodnea	k1gFnSc2	Rodnea
a	a	k8xC	a
poté	poté	k6eAd1	poté
i	i	k9	i
King	King	k1gMnSc1	King
George	Georg	k1gMnSc2	Georg
V.	V.	kA	V.
V	v	k7c6	v
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
57	[number]	k4	57
byl	být	k5eAaImAgInS	být
Bismarck	Bismarck	k1gInSc1	Bismarck
poprvé	poprvé	k6eAd1	poprvé
zasažen	zasažen	k2eAgMnSc1d1	zasažen
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
406	[number]	k4	406
<g/>
mm	mm	kA	mm
granátem	granát	k1gInSc7	granát
z	z	k7c2	z
Rodney	Rodnea	k1gFnSc2	Rodnea
<g/>
.	.	kIx.	.
</s>
<s>
Bismarck	Bismarck	k1gMnSc1	Bismarck
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
mířit	mířit	k5eAaImF	mířit
palbou	palba	k1gFnSc7	palba
na	na	k7c4	na
nejstarší	starý	k2eAgInPc4d3	nejstarší
Rodney	Rodney	k1gInPc4	Rodney
<g/>
,	,	kIx,	,
<g/>
protože	protože	k8xS	protože
doufal	doufat	k5eAaImAgMnS	doufat
<g/>
,	,	kIx,	,
<g/>
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
dřív	dříve	k6eAd2	dříve
vyřadí	vyřadit	k5eAaPmIp3nS	vyřadit
z	z	k7c2	z
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
Tovey	Tovea	k1gFnPc1	Tovea
s	s	k7c7	s
King	King	k1gMnSc1	King
George	George	k1gNnSc2	George
V	V	kA	V
přiblížil	přiblížit	k5eAaPmAgMnS	přiblížit
na	na	k7c4	na
kratší	krátký	k2eAgFnSc4d2	kratší
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
a	a	k8xC	a
německá	německý	k2eAgFnSc1d1	německá
loď	loď	k1gFnSc1	loď
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
vystavena	vystavit	k5eAaPmNgFnS	vystavit
bočním	boční	k2eAgFnPc3d1	boční
salvám	salva	k1gFnPc3	salva
obou	dva	k4xCgFnPc2	dva
těžkých	těžký	k2eAgFnPc2d1	těžká
britských	britský	k2eAgFnPc2d1	britská
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
zaznamenali	zaznamenat	k5eAaPmAgMnP	zaznamenat
další	další	k2eAgInPc4d1	další
zásahy	zásah	k1gInPc4	zásah
Bismarcka	Bismarcko	k1gNnSc2	Bismarcko
těžkými	těžký	k2eAgInPc7d1	těžký
granáty	granát	k1gInPc7	granát
v	v	k7c4	v
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
a	a	k8xC	a
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
ze	z	k7c2	z
SV	sv	kA	sv
směru	směr	k1gInSc2	směr
k	k	k7c3	k
Bismarcku	Bismarck	k1gInSc3	Bismarck
přiblížil	přiblížit	k5eAaPmAgMnS	přiblížit
i	i	k9	i
křižník	křižník	k1gInSc4	křižník
Norfolk	Norfolka	k1gFnPc2	Norfolka
a	a	k8xC	a
palba	palba	k1gFnSc1	palba
Bismarcka	Bismarcka	k1gFnSc1	Bismarcka
začala	začít	k5eAaPmAgFnS	začít
ztrácet	ztrácet	k5eAaImF	ztrácet
na	na	k7c6	na
přesnosti	přesnost	k1gFnSc6	přesnost
-	-	kIx~	-
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
granátů	granát	k1gInPc2	granát
vyřadil	vyřadit	k5eAaPmAgInS	vyřadit
centrální	centrální	k2eAgNnPc4d1	centrální
stanoviště	stanoviště	k1gNnPc4	stanoviště
řízení	řízení	k1gNnSc2	řízení
palby	palba	k1gFnSc2	palba
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc1d1	další
salva	salva	k1gFnSc1	salva
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
admirálský	admirálský	k2eAgInSc4d1	admirálský
můstek	můstek	k1gInSc4	můstek
a	a	k8xC	a
zabila	zabít	k5eAaPmAgFnS	zabít
velícího	velící	k2eAgMnSc4d1	velící
admirála	admirál	k1gMnSc4	admirál
Lütjense	Lütjens	k1gMnSc4	Lütjens
a	a	k8xC	a
celý	celý	k2eAgInSc4d1	celý
jeho	jeho	k3xOp3gInSc4	jeho
štáb	štáb	k1gInSc4	štáb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
15	[number]	k4	15
minut	minuta	k1gFnPc2	minuta
se	se	k3xPyFc4	se
připojila	připojit	k5eAaPmAgNnP	připojit
děla	dělo	k1gNnPc1	dělo
dalšího	další	k2eAgInSc2d1	další
britského	britský	k2eAgInSc2d1	britský
těžkého	těžký	k2eAgInSc2d1	těžký
křižníku	křižník	k1gInSc2	křižník
HMS	HMS	kA	HMS
Dorsetshire	Dorsetshir	k1gInSc5	Dorsetshir
jehož	jehož	k3xOyRp3gNnSc3	jehož
velitel	velitel	k1gMnSc1	velitel
B.	B.	kA	B.
C.	C.	kA	C.
S.	S.	kA	S.
Martin	Martin	k1gMnSc1	Martin
po	po	k7c6	po
hlášení	hlášení	k1gNnSc6	hlášení
Cataliny	Catalina	k1gFnSc2	Catalina
bez	bez	k7c2	bez
rozkazu	rozkaz	k1gInSc2	rozkaz
(	(	kIx(	(
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
opustil	opustit	k5eAaPmAgInS	opustit
konvoj	konvoj	k1gInSc1	konvoj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
provázel	provázet	k5eAaImAgInS	provázet
z	z	k7c2	z
Freetownu	Freetown	k1gInSc2	Freetown
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
spěchal	spěchat	k5eAaImAgMnS	spěchat
se	se	k3xPyFc4	se
připojit	připojit	k5eAaPmF	připojit
k	k	k7c3	k
závěrečné	závěrečný	k2eAgFnSc3d1	závěrečná
bitvě	bitva	k1gFnSc3	bitva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolem	kolem	k7c2	kolem
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
z	z	k7c2	z
Ark	Ark	k1gFnSc2	Ark
Royal	Royal	k1gInSc4	Royal
další	další	k2eAgFnSc1d1	další
skupina	skupina	k1gFnSc1	skupina
letadel	letadlo	k1gNnPc2	letadlo
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
pomoci	pomoct	k5eAaPmF	pomoct
německou	německý	k2eAgFnSc4d1	německá
loď	loď	k1gFnSc4	loď
dorazit	dorazit	k5eAaPmF	dorazit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
letadla	letadlo	k1gNnSc2	letadlo
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
britských	britský	k2eAgFnPc6d1	britská
lodích	loď	k1gFnPc6	loď
zaměněna	zaměnit	k5eAaPmNgFnS	zaměnit
za	za	k7c2	za
německá	německý	k2eAgFnSc1d1	německá
<g/>
,	,	kIx,	,
přivítána	přivítán	k2eAgFnSc1d1	přivítána
protiletadlovou	protiletadlový	k2eAgFnSc7d1	protiletadlová
palbou	palba	k1gFnSc7	palba
britských	britský	k2eAgFnPc2d1	britská
lodí	loď	k1gFnPc2	loď
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
raději	rád	k6eAd2	rád
vrátila	vrátit	k5eAaPmAgFnS	vrátit
zpět	zpět	k6eAd1	zpět
bez	bez	k7c2	bez
pokusu	pokus	k1gInSc2	pokus
o	o	k7c4	o
útok	útok	k1gInSc4	útok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
King	King	k1gInSc4	King
George	Georg	k1gFnSc2	Georg
V	V	kA	V
se	se	k3xPyFc4	se
po	po	k7c6	po
půldruhé	půldruhý	k2eAgFnSc6d1	půldruhá
hodině	hodina	k1gFnSc6	hodina
boje	boj	k1gInSc2	boj
objevily	objevit	k5eAaPmAgInP	objevit
podobné	podobný	k2eAgInPc1d1	podobný
problémy	problém	k1gInPc1	problém
jako	jako	k8xC	jako
nedávno	nedávno	k6eAd1	nedávno
na	na	k7c6	na
Prince	princa	k1gFnSc6	princa
of	of	k?	of
Wales	Wales	k1gInSc1	Wales
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
děla	dít	k5eAaImAgFnS	dít
se	se	k3xPyFc4	se
na	na	k7c4	na
čas	čas	k1gInSc4	čas
odmlčela	odmlčet	k5eAaPmAgFnS	odmlčet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Bismarck	Bismarck	k1gInSc4	Bismarck
dopadlo	dopadnout	k5eAaPmAgNnS	dopadnout
tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
již	již	k6eAd1	již
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
granátů	granát	k1gInPc2	granát
všech	všecek	k3xTgFnPc2	všecek
ráží	ráže	k1gFnPc2	ráže
a	a	k8xC	a
kolem	kolem	k7c2	kolem
10	[number]	k4	10
hodiny	hodina	k1gFnSc2	hodina
přestala	přestat	k5eAaPmAgFnS	přestat
střílet	střílet	k5eAaImF	střílet
poslední	poslední	k2eAgFnSc1d1	poslední
dělová	dělový	k2eAgFnSc1d1	dělová
věž	věž	k1gFnSc1	věž
jeho	jeho	k3xOp3gFnSc2	jeho
hlavní	hlavní	k2eAgFnSc2d1	hlavní
výzbroje	výzbroj	k1gFnSc2	výzbroj
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
nicméně	nicméně	k8xC	nicméně
stále	stále	k6eAd1	stále
nejevila	jevit	k5eNaImAgFnS	jevit
tendenci	tendence	k1gFnSc4	tendence
k	k	k7c3	k
potopení	potopení	k1gNnSc3	potopení
a	a	k8xC	a
proto	proto	k8xC	proto
dal	dát	k5eAaPmAgMnS	dát
Tovey	Tovea	k1gFnPc1	Tovea
příkaz	příkaz	k1gInSc4	příkaz
potopit	potopit	k5eAaPmF	potopit
loď	loď	k1gFnSc4	loď
torpédy	torpédo	k1gNnPc7	torpédo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Bismarcka	Bismarcko	k1gNnPc4	Bismarcko
je	on	k3xPp3gInPc4	on
vystřelily	vystřelit	k5eAaPmAgInP	vystřelit
Rodney	Rodney	k1gInPc4	Rodney
i	i	k8xC	i
Norfolk	Norfolk	k1gInSc4	Norfolk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
jedna	jeden	k4xCgFnSc1	jeden
loď	loď	k1gFnSc1	loď
nezaznamenala	zaznamenat	k5eNaPmAgFnS	zaznamenat
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Tovey	Tovey	k1gInPc4	Tovey
již	již	k6eAd1	již
nemínil	mínit	k5eNaImAgInS	mínit
dále	daleko	k6eAd2	daleko
riskovat	riskovat	k5eAaBmF	riskovat
napadení	napadení	k1gNnSc4	napadení
lodí	loď	k1gFnSc7	loď
svazu	svaz	k1gInSc2	svaz
německou	německý	k2eAgFnSc7d1	německá
ponorkou	ponorka	k1gFnSc7	ponorka
a	a	k8xC	a
dal	dát	k5eAaPmAgInS	dát
v	v	k7c6	v
10	[number]	k4	10
<g/>
:	:	kIx,	:
<g/>
15	[number]	k4	15
rozkaz	rozkaz	k1gInSc1	rozkaz
k	k	k7c3	k
odplutí	odplutí	k1gNnSc3	odplutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
Bismarcka	Bismarcko	k1gNnSc2	Bismarcko
zůstal	zůstat	k5eAaPmAgInS	zůstat
pouze	pouze	k6eAd1	pouze
Dorsetshire	Dorsetshir	k1gInSc5	Dorsetshir
s	s	k7c7	s
torpédoborcem	torpédoborec	k1gInSc7	torpédoborec
Maori	Maor	k1gFnSc2	Maor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
10	[number]	k4	10
<g/>
:	:	kIx,	:
<g/>
20	[number]	k4	20
vyslal	vyslat	k5eAaPmAgMnS	vyslat
Dorsetshire	Dorsetshir	k1gInSc5	Dorsetshir
na	na	k7c4	na
Bismarck	Bismarck	k1gInSc4	Bismarck
2	[number]	k4	2
torpéda	torpédo	k1gNnSc2	torpédo
do	do	k7c2	do
pravého	pravý	k2eAgInSc2d1	pravý
boku	bok	k1gInSc2	bok
a	a	k8xC	a
poté	poté	k6eAd1	poté
ještě	ještě	k9	ještě
3	[number]	k4	3
torpéda	torpédo	k1gNnPc1	torpédo
do	do	k7c2	do
levého	levý	k2eAgInSc2d1	levý
boku	bok	k1gInSc2	bok
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
5	[number]	k4	5
torpéd	torpédo	k1gNnPc2	torpédo
3	[number]	k4	3
cíl	cíl	k1gInSc4	cíl
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c4	po
hodinu	hodina	k1gFnSc4	hodina
a	a	k8xC	a
půl	půl	k6eAd1	půl
trvající	trvající	k2eAgFnSc6d1	trvající
bitvě	bitva	k1gFnSc6	bitva
byl	být	k5eAaImAgInS	být
Bismarck	Bismarck	k1gInSc1	Bismarck
natolik	natolik	k6eAd1	natolik
poškozen	poškodit	k5eAaPmNgInS	poškodit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
rozkaz	rozkaz	k1gInSc1	rozkaz
k	k	k7c3	k
otevření	otevření	k1gNnSc3	otevření
dnových	dnový	k2eAgInPc2d1	dnový
ventilů	ventil	k1gInPc2	ventil
a	a	k8xC	a
odpálení	odpálení	k1gNnSc2	odpálení
náloží	nálož	k1gFnPc2	nálož
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
loď	loď	k1gFnSc1	loď
nepadla	padnout	k5eNaPmAgFnS	padnout
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Spojenců	spojenec	k1gMnPc2	spojenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
torpédových	torpédový	k2eAgInPc6d1	torpédový
útocích	útok	k1gInPc6	útok
Dorsetshire	Dorsetshir	k1gInSc5	Dorsetshir
dal	dát	k5eAaPmAgMnS	dát
kapitán	kapitán	k1gMnSc1	kapitán
Lindenmann	Lindenmann	k1gMnSc1	Lindenmann
rozkaz	rozkaz	k1gInSc4	rozkaz
k	k	k7c3	k
opuštění	opuštění	k1gNnSc3	opuštění
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
10	[number]	k4	10
<g/>
:	:	kIx,	:
<g/>
45	[number]	k4	45
se	se	k3xPyFc4	se
Bismarck	Bismarck	k1gMnSc1	Bismarck
na	na	k7c6	na
pozici	pozice	k1gFnSc6	pozice
převrátil	převrátit	k5eAaPmAgMnS	převrátit
a	a	k8xC	a
potopil	potopit	k5eAaPmAgMnS	potopit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Těžký	těžký	k2eAgInSc1d1	těžký
křižník	křižník	k1gInSc1	křižník
Dorsetshire	Dorsetshir	k1gInSc5	Dorsetshir
a	a	k8xC	a
torpédoborec	torpédoborec	k1gInSc1	torpédoborec
Maori	Maor	k1gFnSc2	Maor
vzaly	vzít	k5eAaPmAgInP	vzít
na	na	k7c4	na
palubu	paluba	k1gFnSc4	paluba
110	[number]	k4	110
námořníků	námořník	k1gMnPc2	námořník
a	a	k8xC	a
lodního	lodní	k2eAgMnSc2d1	lodní
kocoura	kocour	k1gMnSc2	kocour
Oscara	Oscar	k1gMnSc2	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
Britové	Brit	k1gMnPc1	Brit
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
kvůli	kvůli	k7c3	kvůli
obavám	obava	k1gFnPc3	obava
z	z	k7c2	z
možného	možný	k2eAgInSc2d1	možný
ponorkového	ponorkový	k2eAgInSc2d1	ponorkový
útoku	útok	k1gInSc2	útok
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
asi	asi	k9	asi
pět	pět	k4xCc1	pět
set	sto	k4xCgNnPc2	sto
trosečníků	trosečník	k1gMnPc2	trosečník
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
později	pozdě	k6eAd2	pozdě
nalezla	naleznout	k5eAaPmAgFnS	naleznout
ponorka	ponorka	k1gFnSc1	ponorka
U	u	k7c2	u
74	[number]	k4	74
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
velel	velet	k5eAaImAgMnS	velet
Kapitänleutnant	Kapitänleutnant	k1gMnSc1	Kapitänleutnant
Kentrat	Kentrat	k1gMnSc1	Kentrat
<g/>
,	,	kIx,	,
tři	tři	k4xCgMnPc4	tři
přeživší	přeživší	k2eAgMnPc4d1	přeživší
muže	muž	k1gMnPc4	muž
na	na	k7c6	na
voru	vor	k1gInSc6	vor
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
zachránila	zachránit	k5eAaPmAgFnS	zachránit
meteorologická	meteorologický	k2eAgFnSc1d1	meteorologická
loď	loď	k1gFnSc1	loď
Sachsenwald	Sachsenwald	k1gInSc4	Sachsenwald
další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
členy	člen	k1gInPc4	člen
posádky	posádka	k1gFnSc2	posádka
Bismarcku	Bismarck	k1gInSc2	Bismarck
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
2092	[number]	k4	2092
mužů	muž	k1gMnPc2	muž
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
tedy	tedy	k9	tedy
přežilo	přežít	k5eAaPmAgNnS	přežít
pouhých	pouhý	k2eAgInPc2d1	pouhý
115	[number]	k4	115
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přes	přes	k7c4	přes
obavy	obava	k1gFnPc4	obava
Britů	Brit	k1gMnPc2	Brit
z	z	k7c2	z
ponorek	ponorka	k1gFnPc2	ponorka
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
lodí	loď	k1gFnPc2	loď
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
základny	základna	k1gFnPc4	základna
k	k	k7c3	k
ponorkovému	ponorkový	k2eAgInSc3d1	ponorkový
útoku	útok	k1gInSc3	útok
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Objevily	objevit	k5eAaPmAgInP	objevit
se	se	k3xPyFc4	se
však	však	k9	však
dálkové	dálkový	k2eAgInPc4d1	dálkový
bombardéry	bombardér	k1gInPc4	bombardér
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
potopily	potopit	k5eAaPmAgFnP	potopit
torpédoborec	torpédoborec	k1gInSc4	torpédoborec
HMS	HMS	kA	HMS
Mashona	Mashon	k1gMnSc2	Mashon
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
kvůli	kvůli	k7c3	kvůli
potížím	potíž	k1gFnPc3	potíž
s	s	k7c7	s
palivem	palivo	k1gNnSc7	palivo
musel	muset	k5eAaImAgMnS	muset
plout	plout	k5eAaImF	plout
sníženou	snížený	k2eAgFnSc7d1	snížená
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nález	nález	k1gInSc1	nález
vraku	vrak	k1gInSc2	vrak
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1989	[number]	k4	1989
byl	být	k5eAaImAgInS	být
vrak	vrak	k1gInSc1	vrak
Bismarcku	Bismarck	k1gInSc2	Bismarck
nalezen	nalézt	k5eAaBmNgInS	nalézt
oceánografem	oceánograf	k1gMnSc7	oceánograf
Robertem	Robert	k1gMnSc7	Robert
Ballardem	Ballard	k1gMnSc7	Ballard
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
4800	[number]	k4	4800
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Pořízené	pořízený	k2eAgFnPc1d1	pořízená
fotografie	fotografia	k1gFnPc1	fotografia
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
trup	trup	k1gInSc1	trup
lodi	loď	k1gFnSc2	loď
téměř	téměř	k6eAd1	téměř
nebyl	být	k5eNaImAgInS	být
poškozen	poškodit	k5eAaPmNgInS	poškodit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rovněž	rovněž	k9	rovněž
expedice	expedice	k1gFnSc1	expedice
Jamese	Jamese	k1gFnSc1	Jamese
Camerona	Camerona	k1gFnSc1	Camerona
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
příčinou	příčina	k1gFnSc7	příčina
potopení	potopení	k1gNnSc2	potopení
lodi	loď	k1gFnSc2	loď
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
spíše	spíše	k9	spíše
odpálení	odpálení	k1gNnSc4	odpálení
náloží	nálož	k1gFnPc2	nálož
a	a	k8xC	a
otevření	otevření	k1gNnSc6	otevření
ventilů	ventil	k1gInPc2	ventil
než	než	k8xS	než
zásah	zásah	k1gInSc1	zásah
torpédy	torpédo	k1gNnPc7	torpédo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
trupu	trup	k1gInSc6	trup
byly	být	k5eAaImAgFnP	být
napočítány	napočítat	k5eAaPmNgFnP	napočítat
pouze	pouze	k6eAd1	pouze
čtyři	čtyři	k4xCgInPc4	čtyři
průstřely	průstřel	k1gInPc4	průstřel
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
verzi	verze	k1gFnSc3	verze
ovšem	ovšem	k9	ovšem
naprosto	naprosto	k6eAd1	naprosto
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
svědectví	svědectví	k1gNnSc4	svědectví
přeživších	přeživší	k2eAgMnPc2d1	přeživší
členů	člen	k1gMnPc2	člen
posádky	posádka	k1gFnSc2	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
tomu	ten	k3xDgNnSc3	ten
neodpovídají	odpovídat	k5eNaImIp3nP	odpovídat
lodní	lodní	k2eAgInPc1d1	lodní
deníky	deník	k1gInPc1	deník
britských	britský	k2eAgFnPc2d1	britská
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
popisující	popisující	k2eAgFnSc1d1	popisující
klesající	klesající	k2eAgFnSc1d1	klesající
schopnost	schopnost	k1gFnSc1	schopnost
lodi	loď	k1gFnSc2	loď
vést	vést	k5eAaImF	vést
palbu	palba	k1gFnSc4	palba
<g/>
,	,	kIx,	,
požáry	požár	k1gInPc1	požár
a	a	k8xC	a
viditelná	viditelný	k2eAgNnPc1d1	viditelné
poškození	poškození	k1gNnPc1	poškození
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmové	filmový	k2eAgNnSc1d1	filmové
zpracování	zpracování	k1gNnSc1	zpracování
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
natočen	natočit	k5eAaBmNgInS	natočit
film	film	k1gInSc1	film
Potopení	potopení	k1gNnSc2	potopení
lodi	loď	k1gFnSc2	loď
Bismarck	Bismarck	k1gInSc1	Bismarck
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
scénář	scénář	k1gInSc1	scénář
byl	být	k5eAaImAgInS	být
napsán	napsat	k5eAaPmNgInS	napsat
podle	podle	k7c2	podle
knihy	kniha	k1gFnSc2	kniha
C.	C.	kA	C.
S.	S.	kA	S.
Forestera	Forester	k1gMnSc2	Forester
Last	Last	k1gMnSc1	Last
Nine	Nin	k1gMnSc2	Nin
Days	Days	k1gInSc1	Days
of	of	k?	of
the	the	k?	the
Bismarck	Bismarcka	k1gFnPc2	Bismarcka
(	(	kIx(	(
<g/>
Devět	devět	k4xCc1	devět
posledních	poslední	k2eAgInPc2d1	poslední
dní	den	k1gInPc2	den
Bismarcka	Bismarcko	k1gNnSc2	Bismarcko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc4	tento
film	film	k1gInSc4	film
sice	sice	k8xC	sice
řadu	řada	k1gFnSc4	řada
událostí	událost	k1gFnPc2	událost
honu	hon	k1gInSc2	hon
na	na	k7c4	na
německou	německý	k2eAgFnSc4d1	německá
loď	loď	k1gFnSc4	loď
velmi	velmi	k6eAd1	velmi
zjednodušuje	zjednodušovat	k5eAaImIp3nS	zjednodušovat
<g/>
,	,	kIx,	,
na	na	k7c4	na
svoji	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
byl	být	k5eAaImAgMnS	být
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
věrným	věrný	k2eAgNnSc7d1	věrné
ztvárněním	ztvárnění	k1gNnSc7	ztvárnění
námořních	námořní	k2eAgMnPc2d1	námořní
bojů	boj	k1gInPc2	boj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hudební	hudební	k2eAgNnSc1d1	hudební
zpracování	zpracování	k1gNnSc1	zpracování
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2019	[number]	k4	2019
vydala	vydat	k5eAaPmAgFnS	vydat
švédská	švédský	k2eAgFnSc1d1	švédská
power	power	k1gInSc1	power
metalová	metalový	k2eAgFnSc1d1	metalová
skupina	skupina	k1gFnSc1	skupina
Sabaton	Sabaton	k1gInSc1	Sabaton
píseň	píseň	k1gFnSc1	píseň
s	s	k7c7	s
názvem	název	k1gInSc7	název
Bismarck	Bismarcka	k1gFnPc2	Bismarcka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
právě	právě	k9	právě
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
lodi	loď	k1gFnSc6	loď
a	a	k8xC	a
jaký	jaký	k9	jaký
měla	mít	k5eAaImAgFnS	mít
význam	význam	k1gInSc4	význam
během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
HUBÁČEK	Hubáček	k1gMnSc1	Hubáček
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
.	.	kIx.	.
</s>
<s>
Moře	moře	k1gNnSc1	moře
v	v	k7c6	v
plamenech	plamen	k1gInPc6	plamen
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Panorama	panorama	k1gNnSc1	panorama
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
11-069-83	[number]	k4	11-069-83
13	[number]	k4	13
<g/>
/	/	kIx~	/
<g/>
33	[number]	k4	33
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Zkáza	zkáza	k1gFnSc1	zkáza
vlajkové	vlajkový	k2eAgFnSc2d1	vlajková
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
s.	s.	k?	s.
320	[number]	k4	320
<g/>
,	,	kIx,	,
32	[number]	k4	32
stran	strana	k1gFnPc2	strana
obrazových	obrazový	k2eAgFnPc2d1	obrazová
příloh	příloha	k1gFnPc2	příloha
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HUBÁČEK	Hubáček	k1gMnSc1	Hubáček
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
.	.	kIx.	.
</s>
<s>
Moře	moře	k1gNnSc1	moře
v	v	k7c6	v
plamenech	plamen	k1gInPc6	plamen
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
286	[number]	k4	286
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
350	[number]	k4	350
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
KENNEDY	KENNEDY	kA	KENNEDY
<g/>
,	,	kIx,	,
Ludovic	Ludovice	k1gFnPc2	Ludovice
<g/>
.	.	kIx.	.
</s>
<s>
Pronásledování	pronásledování	k1gNnSc1	pronásledování
bitevní	bitevní	k2eAgFnSc2d1	bitevní
lodi	loď	k1gFnSc2	loď
Bismarck	Bismarck	k1gMnSc1	Bismarck
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
245	[number]	k4	245
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
323	[number]	k4	323
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RICO	RICO	kA	RICO
<g/>
,	,	kIx,	,
José	José	k1gNnSc2	José
M.	M.	kA	M.
The	The	k1gMnSc1	The
Battleship	Battleship	k1gMnSc1	Battleship
Bismarck	Bismarck	k1gMnSc1	Bismarck
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Complete	Comple	k1gNnSc2	Comple
History	Histor	k1gInPc4	Histor
of	of	k?	of
a	a	k8xC	a
Legendary	Legendara	k1gFnSc2	Legendara
Ship	Ship	k1gMnSc1	Ship
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Filmové	filmový	k2eAgInPc1d1	filmový
dokumenty	dokument	k1gInPc1	dokument
===	===	k?	===
</s>
</p>
<p>
<s>
Válečné	válečný	k2eAgInPc4d1	válečný
projekty	projekt	k1gInPc4	projekt
Třetí	třetí	k4xOgFnSc2	třetí
říše	říš	k1gFnSc2	říš
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Nacistické	nacistický	k2eAgFnPc1d1	nacistická
bitevní	bitevní	k2eAgFnPc1d1	bitevní
lodě	loď	k1gFnPc1	loď
<g/>
;	;	kIx,	;
britský	britský	k2eAgInSc1d1	britský
dokument	dokument	k1gInSc1	dokument
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
44	[number]	k4	44
<g/>
'	'	kIx"	'
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Bitva	bitva	k1gFnSc1	bitva
v	v	k7c6	v
Dánském	dánský	k2eAgInSc6d1	dánský
průlivu	průliv	k1gInSc6	průliv
</s>
</p>
<p>
<s>
Tirpitz	Tirpitz	k1gInSc1	Tirpitz
–	–	k?	–
sesterská	sesterský	k2eAgFnSc1d1	sesterská
loď	loď	k1gFnSc1	loď
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
německých	německý	k2eAgFnPc2d1	německá
bitevních	bitevní	k2eAgFnPc2d1	bitevní
lodí	loď	k1gFnPc2	loď
</s>
</p>
<p>
<s>
Nepotopitelný	potopitelný	k2eNgMnSc1d1	nepotopitelný
Sam	Sam	k1gMnSc1	Sam
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bismarck	Bismarcka	k1gFnPc2	Bismarcka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Bismarck	Bismarck	k1gMnSc1	Bismarck
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Film	film	k1gInSc4	film
natočený	natočený	k2eAgInSc4d1	natočený
během	během	k7c2	během
spouštění	spouštění	k1gNnPc2	spouštění
Bismarcku	Bismarcka	k1gFnSc4	Bismarcka
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
vytvořený	vytvořený	k2eAgInSc4d1	vytvořený
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
potopení	potopení	k1gNnSc2	potopení
Bismarcku	Bismarcka	k1gFnSc4	Bismarcka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
vidět	vidět	k5eAaImF	vidět
většinu	většina	k1gFnSc4	většina
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
do	do	k7c2	do
útoků	útok	k1gInPc2	útok
na	na	k7c4	na
Bismarcka	Bismarcko	k1gNnPc4	Bismarcko
přímo	přímo	k6eAd1	přímo
zasáhly	zasáhnout	k5eAaPmAgFnP	zasáhnout
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
některé	některý	k3yIgNnSc1	některý
z	z	k7c2	z
velitelů	velitel	k1gMnPc2	velitel
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
několik	několik	k4yIc4	několik
sekund	sekunda	k1gFnPc2	sekunda
záběrů	záběr	k1gInPc2	záběr
ze	z	k7c2	z
závěrečného	závěrečný	k2eAgInSc2d1	závěrečný
dělostřeleckého	dělostřelecký	k2eAgInSc2d1	dělostřelecký
souboje	souboj	k1gInSc2	souboj
před	před	k7c7	před
potopením	potopení	k1gNnSc7	potopení
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Dokumentární	dokumentární	k2eAgInSc1d1	dokumentární
film	film	k1gInSc1	film
s	s	k7c7	s
digitální	digitální	k2eAgFnSc7d1	digitální
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
událostí	událost	k1gFnPc2	událost
spojených	spojený	k2eAgFnPc2d1	spojená
s	s	k7c7	s
plavbou	plavba	k1gFnSc7	plavba
Bismarcku	Bismarck	k1gInSc2	Bismarck
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
lodích	loď	k1gFnPc6	loď
třídy	třída	k1gFnSc2	třída
Bismarck	Bismarck	k1gInSc1	Bismarck
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Loď	loď	k1gFnSc1	loď
Bismarck	Bismarck	k1gMnSc1	Bismarck
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Posádka	posádka	k1gFnSc1	posádka
Bismarcku	Bismarck	k1gInSc2	Bismarck
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Bismarck@sketchup.google.com	Bismarck@sketchup.google.com	k1gInSc1	Bismarck@sketchup.google.com
(	(	kIx(	(
<g/>
Google	Google	k1gFnSc1	Google
SketchUp	SketchUp	k1gMnSc1	SketchUp
model	model	k1gInSc1	model
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Bismarck	Bismarck	k1gMnSc1	Bismarck
Battleship@sketchup.google.com	Battleship@sketchup.google.com	k1gInSc1	Battleship@sketchup.google.com
(	(	kIx(	(
<g/>
Google	Google	k1gFnSc1	Google
SketchUp	SketchUp	k1gMnSc1	SketchUp
model	model	k1gInSc1	model
<g/>
)	)	kIx)	)
</s>
</p>
