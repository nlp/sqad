<s>
Soul	Soul	k1gInSc1	Soul
(	(	kIx(	(
<g/>
korejsky	korejsky	k6eAd1	korejsky
<g/>
:	:	kIx,	:
서	서	k?	서
<g/>
,	,	kIx,	,
Sŏ	Sŏ	k1gMnSc1	Sŏ
Tchŭ	Tchŭ	k1gMnSc1	Tchŭ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
(	(	kIx(	(
<g/>
Korejské	korejský	k2eAgFnSc2d1	Korejská
republiky	republika	k1gFnSc2	republika
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejlidnatější	lidnatý	k2eAgNnSc1d3	nejlidnatější
město	město	k1gNnSc1	město
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Protéká	protékat	k5eAaImIp3nS	protékat
jím	on	k3xPp3gNnSc7	on
řeka	řeka	k1gFnSc1	řeka
Hangang	Hanganga	k1gFnPc2	Hanganga
a	a	k8xC	a
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
se	se	k3xPyFc4	se
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
605	[number]	k4	605
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přes	přes	k7c4	přes
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
v	v	k7c6	v
aglomeraci	aglomerace	k1gFnSc6	aglomerace
přes	přes	k7c4	přes
24	[number]	k4	24
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
z	z	k7c2	z
něho	on	k3xPp3gInSc2	on
dělá	dělat	k5eAaImIp3nS	dělat
2	[number]	k4	2
<g/>
.	.	kIx.	.
nejlidnatější	lidnatý	k2eAgNnSc1d3	nejlidnatější
město	město	k1gNnSc1	město
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
po	po	k7c6	po
Tokiu	Tokio	k1gNnSc6	Tokio
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
osídlení	osídlení	k1gNnSc6	osídlení
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
18	[number]	k4	18
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
za	za	k7c4	za
celou	celá	k1gFnSc4	celá
svou	svůj	k3xOyFgFnSc4	svůj
historii	historie	k1gFnSc4	historie
město	město	k1gNnSc4	město
změnilo	změnit	k5eAaPmAgNnS	změnit
mnohokrát	mnohokrát	k6eAd1	mnohokrát
název	název	k1gInSc4	název
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
jej	on	k3xPp3gMnSc4	on
napadli	napadnout	k5eAaPmAgMnP	napadnout
a	a	k8xC	a
poškodili	poškodit	k5eAaPmAgMnP	poškodit
Japonci	Japonec	k1gMnPc1	Japonec
a	a	k8xC	a
vpád	vpád	k1gInSc1	vpád
Mandžuů	Mandžu	k1gMnPc2	Mandžu
znamenal	znamenat	k5eAaImAgInS	znamenat
připojení	připojení	k1gNnSc4	připojení
Koreje	Korea	k1gFnSc2	Korea
k	k	k7c3	k
říši	říš	k1gFnSc3	říš
Čching	Čching	k1gInSc4	Čching
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
čínsko-japonské	čínskoaponský	k2eAgFnSc2d1	čínsko-japonská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1895	[number]	k4	1895
podepsána	podepsán	k2eAgFnSc1d1	podepsána
Šimonosecká	Šimonosecký	k2eAgFnSc1d1	Šimonosecký
mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
skončila	skončit	k5eAaPmAgFnS	skončit
nadvláda	nadvláda	k1gFnSc1	nadvláda
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
Korejské	korejský	k2eAgNnSc1d1	korejské
císařství	císařství	k1gNnSc1	císařství
s	s	k7c7	s
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Soulem	Soul	k1gInSc7	Soul
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
hostilo	hostit	k5eAaImAgNnS	hostit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
letní	letní	k2eAgFnSc2d1	letní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
a	a	k8xC	a
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
sem	sem	k6eAd1	sem
proudilo	proudit	k5eAaImAgNnS	proudit
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
pracovníků	pracovník	k1gMnPc2	pracovník
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
ze	z	k7c2	z
sousedních	sousední	k2eAgFnPc2d1	sousední
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
z	z	k7c2	z
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Rozmach	rozmach	k1gInSc1	rozmach
dopravy	doprava	k1gFnSc2	doprava
začal	začít	k5eAaPmAgInS	začít
již	již	k6eAd1	již
za	za	k7c2	za
Korejského	korejský	k2eAgNnSc2d1	korejské
Impéria	impérium	k1gNnSc2	impérium
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
např.	např.	kA	např.
rychlovlak	rychlovlak	k1gInSc4	rychlovlak
KTX	KTX	kA	KTX
nebo	nebo	k8xC	nebo
12	[number]	k4	12
linek	linka	k1gFnPc2	linka
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
dvě	dva	k4xCgNnPc1	dva
letiště	letiště	k1gNnPc1	letiště
<g/>
,	,	kIx,	,
Inčchon	Inčchon	k1gInSc1	Inčchon
a	a	k8xC	a
Kimpcho	Kimpcha	k1gFnSc5	Kimpcha
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc4	počátek
prvního	první	k4xOgNnSc2	první
osídlení	osídlení	k1gNnSc2	osídlení
se	se	k3xPyFc4	se
datují	datovat	k5eAaImIp3nP	datovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
18	[number]	k4	18
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Město	město	k1gNnSc1	město
změnilo	změnit	k5eAaPmAgNnS	změnit
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
celé	celá	k1gFnSc2	celá
své	svůj	k3xOyFgFnSc2	svůj
historie	historie	k1gFnSc2	historie
několikrát	několikrát	k6eAd1	několikrát
název	název	k1gInSc4	název
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
říkalo	říkat	k5eAaImAgNnS	říkat
Hanjang	Hanjang	k1gInSc4	Hanjang
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
:	:	kIx,	:
Na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
opevněné	opevněný	k2eAgFnSc2d1	opevněná
hory	hora	k1gFnSc2	hora
Pukhansan	Pukhansana	k1gFnPc2	Pukhansana
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1104	[number]	k4	1104
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
obec	obec	k1gFnSc1	obec
Jangdzu	Jangdz	k1gInSc2	Jangdz
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgMnPc4	který
byl	být	k5eAaImAgInS	být
postaven	postaven	k2eAgInSc1d1	postaven
palác	palác	k1gInSc1	palác
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgInSc2d1	dnešní
středu	střed	k1gInSc2	střed
města	město	k1gNnSc2	město
a	a	k8xC	a
sloužil	sloužit	k5eAaImAgInS	sloužit
jako	jako	k9	jako
občasné	občasný	k2eAgNnSc4d1	občasné
sídlo	sídlo	k1gNnSc4	sídlo
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
roku	rok	k1gInSc2	rok
1308	[number]	k4	1308
získalo	získat	k5eAaPmAgNnS	získat
město	město	k1gNnSc1	město
status	status	k1gInSc4	status
vojenského	vojenský	k2eAgNnSc2d1	vojenské
a	a	k8xC	a
administrativního	administrativní	k2eAgNnSc2d1	administrativní
střediska	středisko	k1gNnSc2	středisko
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c6	na
Hanjang-bu	Hanjang	k1gInSc6	Hanjang-b
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
království	království	k1gNnSc2	království
Čoson	Čoson	k1gInSc1	Čoson
a	a	k8xC	a
dostalo	dostat	k5eAaPmAgNnS	dostat
název	název	k1gInSc4	název
Hansongbu	Hansongb	k1gInSc2	Hansongb
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
napadeno	napaden	k2eAgNnSc1d1	napadeno
a	a	k8xC	a
poničeno	poničen	k2eAgNnSc1d1	poničeno
Japonci	Japonec	k1gMnPc1	Japonec
<g/>
.	.	kIx.	.
</s>
<s>
Následný	následný	k2eAgInSc1d1	následný
vpád	vpád	k1gInSc1	vpád
Mandžuů	Mandžu	k1gMnPc2	Mandžu
znamenal	znamenat	k5eAaImAgInS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Korea	Korea	k1gFnSc1	Korea
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
říše	říš	k1gFnSc2	říš
Čching	Čching	k1gInSc1	Čching
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
čínsko-japonské	čínskoaponský	k2eAgFnSc2d1	čínsko-japonská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
podepsána	podepsán	k2eAgFnSc1d1	podepsána
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
Šimonosecká	Šimonosecký	k2eAgFnSc1d1	Šimonosecký
mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zemi	zem	k1gFnSc4	zem
osvobodila	osvobodit	k5eAaPmAgFnS	osvobodit
od	od	k7c2	od
nadvlády	nadvláda	k1gFnSc2	nadvláda
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
Korejské	korejský	k2eAgNnSc1d1	korejské
císařství	císařství	k1gNnSc1	císařství
s	s	k7c7	s
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Soulem	Soul	k1gInSc7	Soul
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
začátku	začátek	k1gInSc2	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zemi	zem	k1gFnSc3	zem
opět	opět	k6eAd1	opět
obsadili	obsadit	k5eAaPmAgMnP	obsadit
Japonci	Japonec	k1gMnPc1	Japonec
a	a	k8xC	a
okupace	okupace	k1gFnSc1	okupace
trvala	trvat	k5eAaImAgFnS	trvat
až	až	k9	až
do	do	k7c2	do
skončení	skončení	k1gNnSc2	skončení
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1948	[number]	k4	1948
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
Korejská	korejský	k2eAgFnSc1d1	Korejská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
konaly	konat	k5eAaImAgFnP	konat
letní	letní	k2eAgFnPc1d1	letní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
příliv	příliv	k1gInSc1	příliv
pracovníků	pracovník	k1gMnPc2	pracovník
z	z	k7c2	z
ciziny	cizina	k1gFnSc2	cizina
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
statisíce	statisíce	k1gInPc4	statisíce
lidí	člověk	k1gMnPc2	člověk
přijížděli	přijíždět	k5eAaImAgMnP	přijíždět
do	do	k7c2	do
města	město	k1gNnSc2	město
hledat	hledat	k5eAaImF	hledat
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
o	o	k7c4	o
lidi	člověk	k1gMnPc4	člověk
z	z	k7c2	z
okolních	okolní	k2eAgFnPc2d1	okolní
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
početná	početný	k2eAgFnSc1d1	početná
skupina	skupina	k1gFnSc1	skupina
přijela	přijet	k5eAaPmAgFnS	přijet
i	i	k9	i
z	z	k7c2	z
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgNnSc1d1	dnešní
město	město	k1gNnSc1	město
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejmodernější	moderní	k2eAgFnSc4d3	nejmodernější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
také	také	k9	také
nejdražší	drahý	k2eAgFnSc1d3	nejdražší
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgNnSc1d1	původní
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
obkrouženo	obkroužit	k5eAaPmNgNnS	obkroužit
čtyřmi	čtyři	k4xCgInPc7	čtyři
hlavními	hlavní	k2eAgInPc7d1	hlavní
vrcholky	vrcholek	k1gInPc7	vrcholek
regionu	region	k1gInSc2	region
<g/>
:	:	kIx,	:
Mt	Mt	k1gFnSc1	Mt
<g/>
.	.	kIx.	.
</s>
<s>
Bugaksan	Bugaksan	k1gMnSc1	Bugaksan
<g/>
,	,	kIx,	,
Mt	Mt	k1gMnSc1	Mt
<g/>
.	.	kIx.	.
</s>
<s>
Naksan	Naksan	k1gMnSc1	Naksan
<g/>
,	,	kIx,	,
Mt	Mt	k1gMnSc1	Mt
<g/>
.	.	kIx.	.
</s>
<s>
Inwangsan	Inwangsan	k1gMnSc1	Inwangsan
a	a	k8xC	a
Mt	Mt	k1gMnSc1	Mt
<g/>
.	.	kIx.	.
</s>
<s>
Namsan	Namsan	k1gMnSc1	Namsan
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
hora	hora	k1gFnSc1	hora
oplývá	oplývat	k5eAaImIp3nS	oplývat
svoji	svůj	k3xOyFgFnSc4	svůj
specifickou	specifický	k2eAgFnSc4d1	specifická
krásou	krása	k1gFnSc7	krása
a	a	k8xC	a
nabízí	nabízet	k5eAaImIp3nS	nabízet
okázalý	okázalý	k2eAgInSc4d1	okázalý
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Soul	Soul	k1gInSc1	Soul
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
řeka	řeka	k1gFnSc1	řeka
Hangang	Hanganga	k1gFnPc2	Hanganga
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pramení	pramenit	k5eAaImIp3nS	pramenit
v	v	k7c6	v
horském	horský	k2eAgInSc6d1	horský
hřbetu	hřbet	k1gInSc6	hřbet
Tchebek	Tchebek	k1gInSc1	Tchebek
a	a	k8xC	a
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
zálivu	záliv	k1gInSc2	záliv
Kanchvaman	Kanchvaman	k1gMnSc1	Kanchvaman
ve	v	k7c6	v
Žlutém	žlutý	k2eAgNnSc6d1	žluté
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
80	[number]	k4	80
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
50	[number]	k4	50
km	km	kA	km
od	od	k7c2	od
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Korejskou	korejský	k2eAgFnSc7d1	Korejská
lidově	lidově	k6eAd1	lidově
demokratickou	demokratický	k2eAgFnSc7d1	demokratická
republikou	republika	k1gFnSc7	republika
<g/>
,	,	kIx,	,
což	což	k9	což
mnozí	mnohý	k2eAgMnPc1d1	mnohý
tamní	tamní	k2eAgMnPc1d1	tamní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
mrzuté	mrzutý	k2eAgNnSc4d1	mrzuté
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgFnPc7	tento
dvěma	dva	k4xCgFnPc7	dva
zeměmi	zem	k1gFnPc7	zem
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
napjaté	napjatý	k2eAgNnSc1d1	napjaté
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgNnSc1d1	zdejší
klima	klima	k1gNnSc1	klima
je	být	k5eAaImIp3nS	být
hodnoceno	hodnocen	k2eAgNnSc1d1	hodnoceno
jako	jako	k8xS	jako
mírné	mírný	k2eAgNnSc1d1	mírné
<g/>
,	,	kIx,	,
se	s	k7c7	s
čtyřmi	čtyři	k4xCgNnPc7	čtyři
zřetelnými	zřetelný	k2eAgNnPc7d1	zřetelné
obdobími	období	k1gNnPc7	období
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
teplotami	teplota	k1gFnPc7	teplota
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
a	a	k8xC	a
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
jsou	být	k5eAaImIp3nP	být
extrémní	extrémní	k2eAgFnPc1d1	extrémní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
ohřívají	ohřívat	k5eAaImIp3nP	ohřívat
vzduch	vzduch	k1gInSc4	vzduch
proudy	proud	k1gInPc1	proud
větrů	vítr	k1gInPc2	vítr
od	od	k7c2	od
Jižního	jižní	k2eAgInSc2d1	jižní
Pacifiku	Pacifik	k1gInSc2	Pacifik
a	a	k8xC	a
teploty	teplota	k1gFnSc2	teplota
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
až	až	k9	až
35	[number]	k4	35
°	°	k?	°
<g/>
C.	C.	kA	C.
Naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
zde	zde	k6eAd1	zde
vane	vanout	k5eAaImIp3nS	vanout
studený	studený	k2eAgInSc1d1	studený
vítr	vítr	k1gInSc1	vítr
ze	z	k7c2	z
Sibiře	Sibiř	k1gFnSc2	Sibiř
a	a	k8xC	a
teplota	teplota	k1gFnSc1	teplota
může	moct	k5eAaImIp3nS	moct
klesnout	klesnout	k5eAaPmF	klesnout
i	i	k9	i
k	k	k7c3	k
mínus	mínus	k6eAd1	mínus
13	[number]	k4	13
°	°	k?	°
<g/>
C.	C.	kA	C.
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
červencové	červencový	k2eAgFnPc1d1	červencová
teploty	teplota	k1gFnPc1	teplota
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
okolo	okolo	k7c2	okolo
25	[number]	k4	25
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
okolo	okolo	k7c2	okolo
-4	-4	k4	-4
°	°	k?	°
<g/>
C.	C.	kA	C.
Průměrné	průměrný	k2eAgNnSc1d1	průměrné
množství	množství	k1gNnSc1	množství
srážek	srážka	k1gFnPc2	srážka
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
činí	činit	k5eAaImIp3nS	činit
369	[number]	k4	369
mm	mm	kA	mm
<g/>
,	,	kIx,	,
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
22	[number]	k4	22
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
kontinentální	kontinentální	k2eAgNnSc1d1	kontinentální
<g/>
,	,	kIx,	,
typické	typický	k2eAgFnPc1d1	typická
pro	pro	k7c4	pro
monzunové	monzunový	k2eAgFnPc4d1	monzunová
oblasti	oblast	k1gFnPc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
průzkumů	průzkum	k1gInPc2	průzkum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
za	za	k7c4	za
poslední	poslední	k2eAgInPc4d1	poslední
2	[number]	k4	2
roky	rok	k1gInPc4	rok
klesl	klesnout	k5eAaPmAgInS	klesnout
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
10,4	[number]	k4	10,4
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
jich	on	k3xPp3gNnPc2	on
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
10,6	[number]	k4	10,6
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Pokles	pokles	k1gInSc1	pokles
byl	být	k5eAaImAgInS	být
zapříčiněn	zapříčinit	k5eAaPmNgInS	zapříčinit
vyšším	vysoký	k2eAgInSc7d2	vyšší
odlivem	odliv	k1gInSc7	odliv
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
nežli	nežli	k8xS	nežli
jejich	jejich	k3xOp3gInSc7	jejich
přílivem	příliv	k1gInSc7	příliv
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
statistik	statistika	k1gFnPc2	statistika
město	město	k1gNnSc1	město
opustilo	opustit	k5eAaPmAgNnS	opustit
596	[number]	k4	596
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
487	[number]	k4	487
000	[number]	k4	000
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
přistěhovalo	přistěhovat	k5eAaPmAgNnS	přistěhovat
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
narozených	narozený	k2eAgFnPc2d1	narozená
dětí	dítě	k1gFnPc2	dítě
pak	pak	k6eAd1	pak
činil	činit	k5eAaImAgInS	činit
95	[number]	k4	95
000	[number]	k4	000
a	a	k8xC	a
počet	počet	k1gInSc4	počet
úmrtí	úmrtí	k1gNnSc2	úmrtí
41	[number]	k4	41
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
značně	značně	k6eAd1	značně
klesl	klesnout	k5eAaPmAgInS	klesnout
počet	počet	k1gInSc1	počet
etnických	etnický	k2eAgMnPc2d1	etnický
Korejců	Korejec	k1gMnPc2	Korejec
<g/>
,	,	kIx,	,
Američanů	Američan	k1gMnPc2	Američan
a	a	k8xC	a
Rusů	Rus	k1gMnPc2	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Přírůstek	přírůstek	k1gInSc4	přírůstek
zaznamenaly	zaznamenat	k5eAaPmAgInP	zaznamenat
počty	počet	k1gInPc1	počet
Číňanů	Číňan	k1gMnPc2	Číňan
<g/>
,	,	kIx,	,
Japonců	Japonec	k1gMnPc2	Japonec
a	a	k8xC	a
Vietnamců	Vietnamec	k1gMnPc2	Vietnamec
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
zastoupení	zastoupení	k1gNnSc1	zastoupení
mají	mít	k5eAaImIp3nP	mít
Číňané	Číňan	k1gMnPc1	Číňan
korejského	korejský	k2eAgInSc2d1	korejský
i	i	k8xC	i
nekorejského	korejský	k2eNgInSc2d1	korejský
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
přes	přes	k7c4	přes
225	[number]	k4	225
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
a	a	k8xC	a
Američané	Američan	k1gMnPc1	Američan
(	(	kIx(	(
<g/>
cca	cca	kA	cca
10	[number]	k4	10
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
průměrného	průměrný	k2eAgInSc2d1	průměrný
života	život	k1gInSc2	život
roste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
lidí	člověk	k1gMnPc2	člověk
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
nad	nad	k7c4	nad
65	[number]	k4	65
let	léto	k1gNnPc2	léto
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
o	o	k7c4	o
68	[number]	k4	68
000	[number]	k4	000
na	na	k7c4	na
celkových	celkový	k2eAgInPc2d1	celkový
1,1	[number]	k4	1,1
milionu	milion	k4xCgInSc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
ekonomicky	ekonomicky	k6eAd1	ekonomicky
činných	činný	k2eAgMnPc2d1	činný
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
věk	věk	k1gInSc1	věk
od	od	k7c2	od
15	[number]	k4	15
do	do	k7c2	do
64	[number]	k4	64
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
čítá	čítat	k5eAaImIp3nS	čítat
7,8	[number]	k4	7,8
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
Soul	Soul	k1gInSc1	Soul
zabírá	zabírat	k5eAaImIp3nS	zabírat
jen	jen	k9	jen
0,6	[number]	k4	0,6
procenta	procento	k1gNnSc2	procento
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
<g/>
,	,	kIx,	,
sám	sám	k3xTgInSc1	sám
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
21	[number]	k4	21
<g/>
%	%	kIx~	%
HDP	HDP	kA	HDP
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
ředitelství	ředitelství	k1gNnSc4	ředitelství
zde	zde	k6eAd1	zde
mají	mít	k5eAaImIp3nP	mít
firmy	firma	k1gFnPc1	firma
jako	jako	k8xS	jako
Samsung	Samsung	kA	Samsung
<g/>
,	,	kIx,	,
LG	LG	kA	LG
<g/>
,	,	kIx,	,
Hyundai	Hyundai	k1gNnSc2	Hyundai
nebo	nebo	k8xC	nebo
Kia	Kia	k1gFnSc2	Kia
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
významným	významný	k2eAgNnSc7d1	významné
obchodním	obchodní	k2eAgNnSc7d1	obchodní
centrem	centrum	k1gNnSc7	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
až	až	k9	až
neuvěřitelné	uvěřitelný	k2eNgNnSc1d1	neuvěřitelné
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
vzchopila	vzchopit	k5eAaPmAgFnS	vzchopit
<g/>
.	.	kIx.	.
</s>
<s>
Vždyť	vždyť	k9	vždyť
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
korejské	korejský	k2eAgFnSc2d1	Korejská
války	válka	k1gFnSc2	válka
patřila	patřit	k5eAaImAgFnS	patřit
mezi	mezi	k7c4	mezi
nejchudší	chudý	k2eAgFnPc4d3	nejchudší
země	zem	k1gFnPc4	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
HDP	HDP	kA	HDP
na	na	k7c4	na
osobu	osoba	k1gFnSc4	osoba
činilo	činit	k5eAaImAgNnS	činit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
pouhých	pouhý	k2eAgInPc2d1	pouhý
79	[number]	k4	79
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
se	se	k3xPyFc4	se
chopil	chopit	k5eAaPmAgMnS	chopit
moci	moct	k5eAaImF	moct
diktátor	diktátor	k1gMnSc1	diktátor
Pak	pak	k6eAd1	pak
Čong-hui	Čongu	k1gFnSc2	Čong-hu
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
radikální	radikální	k2eAgFnSc2d1	radikální
reformy	reforma	k1gFnSc2	reforma
položily	položit	k5eAaPmAgInP	položit
základy	základ	k1gInPc1	základ
pro	pro	k7c4	pro
silnou	silný	k2eAgFnSc4d1	silná
exportní	exportní	k2eAgFnSc4d1	exportní
ekonomiku	ekonomika	k1gFnSc4	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
činí	činit	k5eAaImIp3nS	činit
HDP	HDP	kA	HDP
na	na	k7c4	na
osobu	osoba	k1gFnSc4	osoba
31	[number]	k4	31
800	[number]	k4	800
<g/>
$	$	kIx~	$
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
minimální	minimální	k2eAgFnSc1d1	minimální
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
(	(	kIx(	(
<g/>
3,3	[number]	k4	3,3
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dopravní	dopravní	k2eAgFnSc1d1	dopravní
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
v	v	k7c6	v
Soulu	Soul	k1gInSc6	Soul
patři	patřit	k5eAaImRp2nS	patřit
mezi	mezi	k7c4	mezi
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
dopravy	doprava	k1gFnSc2	doprava
se	se	k3xPyFc4	se
traduje	tradovat	k5eAaImIp3nS	tradovat
do	do	k7c2	do
dob	doba	k1gFnPc2	doba
Korejského	korejský	k2eAgNnSc2d1	korejské
císařství	císařství	k1gNnSc2	císařství
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
první	první	k4xOgFnPc1	první
tramvajové	tramvajový	k2eAgFnPc1d1	tramvajová
linky	linka	k1gFnPc1	linka
a	a	k8xC	a
železnice	železnice	k1gFnPc1	železnice
spojující	spojující	k2eAgFnPc1d1	spojující
Soul	Soul	k1gInSc4	Soul
a	a	k8xC	a
Inčchon	Inčchon	k1gInSc4	Inčchon
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc4d3	veliký
problém	problém	k1gInSc4	problém
činí	činit	k5eAaImIp3nP	činit
přeplněné	přeplněný	k2eAgFnPc1d1	přeplněná
silnice	silnice	k1gFnPc1	silnice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
ochromující	ochromující	k2eAgFnSc2d1	ochromující
dopravní	dopravní	k2eAgFnSc2d1	dopravní
zácpy	zácpa	k1gFnSc2	zácpa
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
se	se	k3xPyFc4	se
čemu	co	k3yQnSc3	co
divit	divit	k5eAaImF	divit
<g/>
.	.	kIx.	.
</s>
<s>
Vždyť	vždyť	k9	vždyť
podle	podle	k7c2	podle
statistiky	statistika	k1gFnSc2	statistika
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Soulu	Soul	k1gInSc6	Soul
registrováno	registrován	k2eAgNnSc1d1	registrováno
na	na	k7c4	na
3	[number]	k4	3
miliony	milion	k4xCgInPc7	milion
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Vlaková	vlakový	k2eAgFnSc1d1	vlaková
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Soulu	Soul	k1gInSc6	Soul
a	a	k8xC	a
i	i	k9	i
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Železnice	železnice	k1gFnSc1	železnice
spravuje	spravovat	k5eAaImIp3nS	spravovat
státní	státní	k2eAgInSc4d1	státní
podnik	podnik	k1gInSc4	podnik
KORAIL	KORAIL	kA	KORAIL
založený	založený	k2eAgInSc4d1	založený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
kolejích	kolej	k1gFnPc6	kolej
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
prohánějí	prohánět	k5eAaImIp3nP	prohánět
rychlovlaky	rychlovlak	k1gInPc1	rychlovlak
(	(	kIx(	(
<g/>
KTX	KTX	kA	KTX
<g/>
)	)	kIx)	)
rychlostí	rychlost	k1gFnSc7	rychlost
přes	přes	k7c4	přes
300	[number]	k4	300
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Zajímavé	zajímavý	k2eAgMnPc4d1	zajímavý
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlaky	vlak	k1gInPc1	vlak
jezdí	jezdit	k5eAaImIp3nP	jezdit
na	na	k7c6	na
dvoukolejných	dvoukolejný	k2eAgFnPc6d1	dvoukolejná
tratích	trať	k1gFnPc6	trať
vlevo	vlevo	k6eAd1	vlevo
a	a	k8xC	a
průvodčí	průvodčí	k1gFnSc1	průvodčí
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
starají	starat	k5eAaImIp3nP	starat
o	o	k7c4	o
cestující	cestující	k1gMnPc4	cestující
<g/>
,	,	kIx,	,
než	než	k8xS	než
o	o	k7c6	o
kontrolování	kontrolování	k1gNnSc6	kontrolování
jízdenek	jízdenka	k1gFnPc2	jízdenka
<g/>
.	.	kIx.	.
</s>
<s>
Pohodlí	pohodlí	k1gNnSc1	pohodlí
ve	v	k7c6	v
vozech	vůz	k1gInPc6	vůz
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
i	i	k9	i
možnost	možnost	k1gFnSc1	možnost
otočení	otočení	k1gNnSc2	otočení
sedadel	sedadlo	k1gNnPc2	sedadlo
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
jízdy	jízda	k1gFnSc2	jízda
<g/>
.	.	kIx.	.
</s>
<s>
Metro	metro	k1gNnSc1	metro
v	v	k7c6	v
Soulu	Soul	k1gInSc6	Soul
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejvytíženější	vytížený	k2eAgInSc4d3	nejvytíženější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
délkou	délka	k1gFnSc7	délka
406	[number]	k4	406
km	km	kA	km
je	být	k5eAaImIp3nS	být
3	[number]	k4	3
<g/>
.	.	kIx.	.
nejdelším	dlouhý	k2eAgNnSc7d3	nejdelší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
srovnání	srovnání	k1gNnPc4	srovnání
<g/>
,	,	kIx,	,
linky	linka	k1gFnPc1	linka
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
dohromady	dohromady	k6eAd1	dohromady
měří	měřit	k5eAaImIp3nS	měřit
59,4	[number]	k4	59,4
kilometru	kilometr	k1gInSc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Čítá	čítat	k5eAaImIp3nS	čítat
9	[number]	k4	9
linek	linka	k1gFnPc2	linka
a	a	k8xC	a
denně	denně	k6eAd1	denně
přepraví	přepravit	k5eAaPmIp3nS	přepravit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
8	[number]	k4	8
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
metra	metro	k1gNnSc2	metro
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
a	a	k8xC	a
každým	každý	k3xTgInSc7	každý
rokem	rok	k1gInSc7	rok
se	se	k3xPyFc4	se
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
průzkumů	průzkum	k1gInPc2	průzkum
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nejmodernější	moderní	k2eAgNnSc4d3	nejmodernější
metro	metro	k1gNnSc4	metro
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
vozů	vůz	k1gInPc2	vůz
je	být	k5eAaImIp3nS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
digitálními	digitální	k2eAgFnPc7d1	digitální
plochými	plochý	k2eAgFnPc7d1	plochá
obrazovkami	obrazovka	k1gFnPc7	obrazovka
a	a	k8xC	a
vyhřívanými	vyhřívaný	k2eAgFnPc7d1	vyhřívaná
sedačkami	sedačka	k1gFnPc7	sedačka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
automaticky	automaticky	k6eAd1	automaticky
vytápěny	vytápěn	k2eAgFnPc1d1	vytápěna
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejekonomičtější	ekonomický	k2eAgFnSc4d3	nejekonomičtější
a	a	k8xC	a
snadno	snadno	k6eAd1	snadno
dostupnou	dostupný	k2eAgFnSc4d1	dostupná
formu	forma	k1gFnSc4	forma
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Autobusy	autobus	k1gInPc1	autobus
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
podle	podle	k7c2	podle
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Modrou	modrý	k2eAgFnSc4d1	modrá
barvu	barva	k1gFnSc4	barva
mají	mít	k5eAaImIp3nP	mít
autobusy	autobus	k1gInPc1	autobus
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jezdí	jezdit	k5eAaImIp3nP	jezdit
relativně	relativně	k6eAd1	relativně
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Zelené	Zelené	k2eAgInPc1d1	Zelené
autobusy	autobus	k1gInPc1	autobus
jezdí	jezdit	k5eAaImIp3nP	jezdit
na	na	k7c6	na
kratších	krátký	k2eAgFnPc6d2	kratší
linkách	linka	k1gFnPc6	linka
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
mají	mít	k5eAaImIp3nP	mít
zastávky	zastávka	k1gFnPc4	zastávka
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
přestoupit	přestoupit	k5eAaPmF	přestoupit
na	na	k7c4	na
metro	metro	k1gNnSc4	metro
nebo	nebo	k8xC	nebo
jiný	jiný	k2eAgInSc4d1	jiný
autobus	autobus	k1gInSc4	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Červené	Červené	k2eAgInPc1d1	Červené
autobusy	autobus	k1gInPc1	autobus
jsou	být	k5eAaImIp3nP	být
expresní	expresní	k2eAgInPc1d1	expresní
a	a	k8xC	a
jezdí	jezdit	k5eAaImIp3nP	jezdit
do	do	k7c2	do
příměstských	příměstský	k2eAgFnPc2d1	příměstská
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Žluté	žlutý	k2eAgInPc1d1	žlutý
autobusy	autobus	k1gInPc1	autobus
operují	operovat	k5eAaImIp3nP	operovat
v	v	k7c6	v
uzavřeném	uzavřený	k2eAgInSc6d1	uzavřený
okruhu	okruh	k1gInSc6	okruh
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Soulu	Soul	k1gInSc2	Soul
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
silnic	silnice	k1gFnPc2	silnice
ve	v	k7c6	v
městě	město	k1gNnSc6	město
má	mít	k5eAaImIp3nS	mít
vyhrazený	vyhrazený	k2eAgInSc1d1	vyhrazený
pruh	pruh	k1gInSc1	pruh
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
autobusy	autobus	k1gInPc4	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
2	[number]	k4	2
letiště	letiště	k1gNnSc2	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
Inčchon	Inčchona	k1gFnPc2	Inčchona
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgNnSc4d3	veliký
letiště	letiště	k1gNnSc4	letiště
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
a	a	k8xC	a
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vzdáleno	vzdálit	k5eAaPmNgNnS	vzdálit
52	[number]	k4	52
km	km	kA	km
západně	západně	k6eAd1	západně
od	od	k7c2	od
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Soulu	Soul	k1gInSc2	Soul
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
bylo	být	k5eAaImAgNnS	být
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
Inčchon	Inčchona	k1gFnPc2	Inčchona
zvoleno	zvolit	k5eAaPmNgNnS	zvolit
nejlepším	dobrý	k2eAgNnSc7d3	nejlepší
letištěm	letiště	k1gNnSc7	letiště
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
svoji	svůj	k3xOyFgFnSc4	svůj
velikost	velikost	k1gFnSc4	velikost
je	být	k5eAaImIp3nS	být
letiště	letiště	k1gNnSc1	letiště
přehledné	přehledný	k2eAgNnSc1d1	přehledné
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
standardních	standardní	k2eAgFnPc2d1	standardní
služeb	služba	k1gFnPc2	služba
můžete	moct	k5eAaImIp2nP	moct
využít	využít	k5eAaPmF	využít
třeba	třeba	k6eAd1	třeba
luxusní	luxusní	k2eAgNnSc4d1	luxusní
golfové	golfový	k2eAgNnSc4d1	golfové
hřiště	hřiště	k1gNnSc4	hřiště
nebo	nebo	k8xC	nebo
lázně	lázeň	k1gFnPc4	lázeň
"	"	kIx"	"
<g/>
Spa	Spa	k1gMnSc2	Spa
on	on	k3xPp3gMnSc1	on
Air	Air	k1gMnSc1	Air
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
letiště	letiště	k1gNnSc1	letiště
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Kimpcho	Kimpcha	k1gFnSc5	Kimpcha
a	a	k8xC	a
dříve	dříve	k6eAd2	dříve
sloužilo	sloužit	k5eAaImAgNnS	sloužit
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgNnSc4d1	hlavní
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
letiště	letiště	k1gNnSc4	letiště
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
nevystřídalo	vystřídat	k5eNaPmAgNnS	vystřídat
letiště	letiště	k1gNnSc1	letiště
Incheon	Incheon	k1gInSc1	Incheon
(	(	kIx(	(
<g/>
Inčchon	Inčchon	k1gInSc1	Inčchon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
však	však	k9	však
přepravuje	přepravovat	k5eAaImIp3nS	přepravovat
mnoho	mnoho	k4c4	mnoho
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
přes	přes	k7c4	přes
18	[number]	k4	18
milionů	milion	k4xCgInPc2	milion
pasažérů	pasažér	k1gMnPc2	pasažér
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
CJ	CJ	kA	CJ
Seoul	Seoul	k1gInSc1	Seoul
Tower	Tower	k1gInSc1	Tower
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
pod	pod	k7c4	pod
názvy	název	k1gInPc4	název
Soul	Soul	k1gInSc4	Soul
Tower	Tower	k1gMnSc1	Tower
nebo	nebo	k8xC	nebo
Namsan	Namsan	k1gMnSc1	Namsan
Tower	Tower	k1gMnSc1	Tower
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc1	věž
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
na	na	k7c6	na
hoře	hora	k1gFnSc6	hora
Namsan	Namsany	k1gInPc2	Namsany
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Soulu	Soul	k1gInSc2	Soul
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
základů	základ	k1gInPc2	základ
měří	měřit	k5eAaImIp3nS	měřit
236	[number]	k4	236
m	m	kA	m
a	a	k8xC	a
vrchol	vrchol	k1gInSc1	vrchol
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
483	[number]	k4	483
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
dělá	dělat	k5eAaImIp3nS	dělat
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
vysílač	vysílač	k1gInSc1	vysílač
a	a	k8xC	a
rozhledna	rozhledna	k1gFnSc1	rozhledna
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
poslední	poslední	k2eAgFnSc6d1	poslední
vyhlídkové	vyhlídkový	k2eAgFnSc6d1	vyhlídková
plošině	plošina	k1gFnSc6	plošina
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
otočná	otočný	k2eAgFnSc1d1	otočná
restaurace	restaurace	k1gFnSc1	restaurace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
otočí	otočit	k5eAaPmIp3nS	otočit
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
48	[number]	k4	48
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Večer	večer	k1gInSc1	večer
září	září	k1gNnSc2	září
v	v	k7c6	v
modrých	modrý	k2eAgFnPc6d1	modrá
barvách	barva	k1gFnPc6	barva
<g/>
.	.	kIx.	.
</s>
<s>
Kjongbokkung	Kjongbokkung	k1gInSc1	Kjongbokkung
<g/>
,	,	kIx,	,
také	také	k9	také
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
Gyeongbokgung	Gyeongbokgung	k1gMnSc1	Gyeongbokgung
Palace	Palace	k1gFnSc2	Palace
nebo	nebo	k8xC	nebo
Gyeongbok	Gyeongbok	k1gInSc1	Gyeongbok
Palace	Palace	k1gFnSc2	Palace
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
královský	královský	k2eAgInSc1d1	královský
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Soulu	Soul	k1gInSc2	Soul
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
konstrukce	konstrukce	k1gFnSc1	konstrukce
paláce	palác	k1gInSc2	palác
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1395	[number]	k4	1395
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
však	však	k9	však
byla	být	k5eAaImAgFnS	být
spálena	spálen	k2eAgFnSc1d1	spálena
a	a	k8xC	a
na	na	k7c4	na
téměř	téměř	k6eAd1	téměř
3	[number]	k4	3
století	století	k1gNnPc2	století
opuštěna	opuštěn	k2eAgFnSc1d1	opuštěna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1867	[number]	k4	1867
byl	být	k5eAaImAgInS	být
přestavěn	přestavět	k5eAaPmNgInS	přestavět
a	a	k8xC	a
stal	stát	k5eAaPmAgInS	stát
se	se	k3xPyFc4	se
největším	veliký	k2eAgInSc7d3	veliký
palácem	palác	k1gInSc7	palác
postaveným	postavený	k2eAgInSc7d1	postavený
dynastií	dynastie	k1gFnSc7	dynastie
Čoson	Čoson	k1gNnSc4	Čoson
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
paláce	palác	k1gInSc2	palác
poškozená	poškozený	k2eAgFnSc1d1	poškozená
Japonci	Japonec	k1gMnPc1	Japonec
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
palácový	palácový	k2eAgInSc1d1	palácový
komplex	komplex	k1gInSc1	komplex
restaurován	restaurován	k2eAgInSc1d1	restaurován
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
původní	původní	k2eAgFnSc2d1	původní
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
stále	stále	k6eAd1	stále
stojí	stát	k5eAaImIp3nS	stát
zhruba	zhruba	k6eAd1	zhruba
40	[number]	k4	40
<g/>
%	%	kIx~	%
z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
počtu	počet	k1gInSc2	počet
zámeckých	zámecký	k2eAgFnPc2d1	zámecká
budov	budova	k1gFnPc2	budova
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
rekonstruovány	rekonstruován	k2eAgFnPc1d1	rekonstruována
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrť	čtvrť	k1gFnSc1	čtvrť
Mjong-dong	Mjongonga	k1gFnPc2	Mjong-donga
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
jasné	jasný	k2eAgNnSc1d1	jasné
město	město	k1gNnSc1	město
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
samotném	samotný	k2eAgNnSc6d1	samotné
srdci	srdce	k1gNnSc6	srdce
Soulu	Soul	k1gInSc2	Soul
<g/>
.	.	kIx.	.
</s>
<s>
Mjong-dong	Mjongong	k1gInSc1	Mjong-dong
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
centrum	centrum	k1gNnSc4	centrum
nakupování	nakupování	k1gNnSc2	nakupování
a	a	k8xC	a
módy	móda	k1gFnSc2	móda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
centrální	centrální	k2eAgFnSc1d1	centrální
pošta	pošta	k1gFnSc1	pošta
<g/>
,	,	kIx,	,
čínská	čínský	k2eAgFnSc1d1	čínská
ambasáda	ambasáda	k1gFnSc1	ambasáda
a	a	k8xC	a
několik	několik	k4yIc1	několik
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
obchodních	obchodní	k2eAgInPc2d1	obchodní
domů	dům	k1gInPc2	dům
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Koreji	Korea	k1gFnSc6	Korea
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
zajímavé	zajímavý	k2eAgFnPc4d1	zajímavá
restaurace	restaurace	k1gFnPc4	restaurace
nabízející	nabízející	k2eAgFnSc2d1	nabízející
speciality	specialita	k1gFnSc2	specialita
místní	místní	k2eAgFnSc2d1	místní
kuchyně	kuchyně	k1gFnSc2	kuchyně
nebo	nebo	k8xC	nebo
i	i	k9	i
kuchyně	kuchyně	k1gFnSc1	kuchyně
západních	západní	k2eAgFnPc2d1	západní
civilizací	civilizace	k1gFnPc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
můžete	moct	k5eAaImIp2nP	moct
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
navštívit	navštívit	k5eAaPmF	navštívit
bary	bar	k1gInPc1	bar
<g/>
,	,	kIx,	,
kina	kino	k1gNnPc1	kino
<g/>
,	,	kIx,	,
galerie	galerie	k1gFnPc1	galerie
a	a	k8xC	a
zábavní	zábavní	k2eAgInPc1d1	zábavní
podniky	podnik	k1gInPc1	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Olympijský	olympijský	k2eAgInSc1d1	olympijský
park	park	k1gInSc1	park
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
k	k	k7c3	k
hostování	hostování	k1gNnSc3	hostování
letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
Songpa-gu	Songpa	k1gInSc6	Songpa-g
<g/>
,	,	kIx,	,
Bangi-dong	Bangiong	k1gInSc1	Bangi-dong
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
olympijská	olympijský	k2eAgFnSc1d1	olympijská
hala	hala	k1gFnSc1	hala
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
na	na	k7c4	na
koncertní	koncertní	k2eAgFnSc4d1	koncertní
síň	síň	k1gFnSc4	síň
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přestavbě	přestavba	k1gFnSc6	přestavba
přibylo	přibýt	k5eAaPmAgNnS	přibýt
2	[number]	k4	2
452	[number]	k4	452
sedadel	sedadlo	k1gNnPc2	sedadlo
v	v	k7c6	v
hlavním	hlavní	k2eAgInSc6d1	hlavní
sále	sál	k1gInSc6	sál
<g/>
,	,	kIx,	,
výstavní	výstavní	k2eAgFnSc4d1	výstavní
místnost	místnost	k1gFnSc4	místnost
s	s	k7c7	s
historií	historie	k1gFnSc7	historie
korejského	korejský	k2eAgInSc2d1	korejský
popu	pop	k1gInSc2	pop
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
až	až	k6eAd1	až
po	po	k7c4	po
současnost	současnost	k1gFnSc4	současnost
a	a	k8xC	a
malé	malý	k2eAgNnSc4d1	malé
divadlo	divadlo	k1gNnSc4	divadlo
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
240	[number]	k4	240
míst	místo	k1gNnPc2	místo
pro	pro	k7c4	pro
indické	indický	k2eAgMnPc4d1	indický
hudebníky	hudebník	k1gMnPc4	hudebník
a	a	k8xC	a
nové	nový	k2eAgMnPc4d1	nový
umělce	umělec	k1gMnPc4	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
stadion	stadion	k1gInSc1	stadion
postavený	postavený	k2eAgInSc1d1	postavený
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
také	také	k9	také
často	často	k6eAd1	často
nazýván	nazývat	k5eAaImNgInS	nazývat
jako	jako	k9	jako
Sangam	Sangam	k1gInSc1	Sangam
Stadium	stadium	k1gNnSc1	stadium
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
dokončen	dokončen	k2eAgInSc1d1	dokončen
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
a	a	k8xC	a
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
ho	on	k3xPp3gMnSc4	on
v	v	k7c4	v
Seongsan-dong	Seongsanong	k1gInSc4	Seongsan-dong
<g/>
,	,	kIx,	,
Mapo-gu	Mapoa	k1gFnSc4	Mapo-ga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
2	[number]	k4	2
<g/>
.	.	kIx.	.
největší	veliký	k2eAgInSc1d3	veliký
stadion	stadion	k1gInSc1	stadion
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
66	[number]	k4	66
806	[number]	k4	806
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
816	[number]	k4	816
míst	místo	k1gNnPc2	místo
pro	pro	k7c4	pro
VIP	VIP	kA	VIP
<g/>
,	,	kIx,	,
754	[number]	k4	754
míst	místo	k1gNnPc2	místo
k	k	k7c3	k
sezení	sezení	k1gNnSc3	sezení
pro	pro	k7c4	pro
novináře	novinář	k1gMnPc4	novinář
a	a	k8xC	a
75	[number]	k4	75
soukromých	soukromý	k2eAgInPc2d1	soukromý
pokojů	pokoj	k1gInPc2	pokoj
Sky	Sky	k1gFnSc1	Sky
Box	box	k1gInSc1	box
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
pro	pro	k7c4	pro
12	[number]	k4	12
až	až	k9	až
29	[number]	k4	29
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
stadionu	stadion	k1gInSc2	stadion
vyšla	vyjít	k5eAaPmAgFnS	vyjít
na	na	k7c4	na
200	[number]	k4	200
milionů	milion	k4xCgInPc2	milion
$	$	kIx~	$
<g/>
.	.	kIx.	.
</s>
<s>
Čchangdokkung	Čchangdokkung	k1gInSc1	Čchangdokkung
<g/>
,	,	kIx,	,
také	také	k9	také
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
Changdeokgung	Changdeokgung	k1gMnSc1	Changdeokgung
Palace	Palace	k1gFnSc2	Palace
nebo	nebo	k8xC	nebo
Changdeok	Changdeok	k1gInSc1	Changdeok
Palace	Palace	k1gFnSc2	Palace
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
zasazen	zasadit	k5eAaPmNgInS	zasadit
do	do	k7c2	do
rozlehlého	rozlehlý	k2eAgInSc2d1	rozlehlý
parku	park	k1gInSc2	park
v	v	k7c6	v
Jongno-gu	Jongno	k1gInSc6	Jongno-g
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
paláců	palác	k1gInPc2	palác
postavených	postavený	k2eAgFnPc2d1	postavená
dynastií	dynastie	k1gFnPc2	dynastie
Čoson	Čosona	k1gFnPc2	Čosona
(	(	kIx(	(
<g/>
1392	[number]	k4	1392
<g/>
-	-	kIx~	-
<g/>
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
východně	východně	k6eAd1	východně
od	od	k7c2	od
paláce	palác	k1gInSc2	palác
Kjongbok	Kjongbok	k1gInSc4	Kjongbok
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
nazýván	nazývat	k5eAaImNgInS	nazývat
také	také	k9	také
jako	jako	k8xS	jako
východní	východní	k2eAgInSc4d1	východní
palác	palác	k1gInSc4	palác
<g/>
.	.	kIx.	.
</s>
<s>
Palác	palác	k1gInSc1	palác
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1412	[number]	k4	1412
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nečekala	čekat	k5eNaImAgFnS	čekat
ho	on	k3xPp3gNnSc4	on
příznivá	příznivý	k2eAgFnSc1d1	příznivá
budoucnost	budoucnost	k1gFnSc1	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
století	století	k1gNnSc6	století
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1592	[number]	k4	1592
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vypálen	vypálit	k5eAaPmNgInS	vypálit
při	při	k7c6	při
japonské	japonský	k2eAgFnSc6d1	japonská
invazi	invaze	k1gFnSc6	invaze
a	a	k8xC	a
rekonstruován	rekonstruován	k2eAgMnSc1d1	rekonstruován
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1609	[number]	k4	1609
králem	král	k1gMnSc7	král
Sondžem	Sondž	k1gMnSc7	Sondž
a	a	k8xC	a
králem	král	k1gMnSc7	král
Kwanghegunem	Kwanghegun	k1gMnSc7	Kwanghegun
<g/>
.	.	kIx.	.
</s>
<s>
Palác	palác	k1gInSc1	palác
byl	být	k5eAaImAgInS	být
zapálen	zapálit	k5eAaPmNgInS	zapálit
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vždy	vždy	k6eAd1	vždy
si	se	k3xPyFc3	se
zachoval	zachovat	k5eAaPmAgInS	zachovat
svůj	svůj	k3xOyFgInSc4	svůj
originální	originální	k2eAgInSc4d1	originální
vzhled	vzhled	k1gInSc4	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
korejský	korejský	k2eAgMnSc1d1	korejský
císař	císař	k1gMnSc1	císař
Sundžong	Sundžong	k1gMnSc1	Sundžong
zde	zde	k6eAd1	zde
žil	žít	k5eAaImAgMnS	žít
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
existuje	existovat	k5eAaImIp3nS	existovat
13	[number]	k4	13
původních	původní	k2eAgFnPc2d1	původní
budov	budova	k1gFnPc2	budova
a	a	k8xC	a
28	[number]	k4	28
pavilónů	pavilón	k1gInPc2	pavilón
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
45	[number]	k4	45
hektarů	hektar	k1gInPc2	hektar
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
ostatní	ostatní	k2eAgInPc1d1	ostatní
paláce	palác	k1gInPc1	palác
<g/>
,	,	kIx,	,
i	i	k9	i
on	on	k3xPp3gMnSc1	on
byl	být	k5eAaImAgInS	být
značně	značně	k6eAd1	značně
poničen	poničit	k5eAaPmNgInS	poničit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
japonské	japonský	k2eAgFnSc2d1	japonská
okupace	okupace	k1gFnSc2	okupace
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Soul	Soul	k1gInSc1	Soul
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
25	[number]	k4	25
městských	městský	k2eAgFnPc2d1	městská
čtvrtí	čtvrt	k1gFnPc2	čtvrt
nazývaných	nazývaný	k2eAgFnPc2d1	nazývaná
gu	gu	k?	gu
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
na	na	k7c4	na
522	[number]	k4	522
dongů	dong	k1gInPc2	dong
<g/>
,	,	kIx,	,
ty	ten	k3xDgInPc4	ten
na	na	k7c4	na
13	[number]	k4	13
787	[number]	k4	787
tongů	tong	k1gMnPc2	tong
a	a	k8xC	a
ty	ten	k3xDgInPc4	ten
na	na	k7c4	na
102	[number]	k4	102
796	[number]	k4	796
banů	ban	k1gInPc2	ban
<g/>
.	.	kIx.	.
</s>
<s>
Dongdaemun-gu	Dongdaemun	k1gMnSc3	Dongdaemun-g
Dongjak-gu	Dongjak	k1gMnSc3	Dongjak-g
Eunpyeong-gu	Eunpyeong	k1gMnSc3	Eunpyeong-g
Gangbuk-gu	Gangbuk	k1gMnSc3	Gangbuk-g
Gangdong-gu	Gangdong	k1gMnSc3	Gangdong-g
Gangnam-gu	Gangnam	k1gMnSc3	Gangnam-g
Gangseo-gu	Gangseo	k1gMnSc3	Gangseo-g
Geumcheon-gu	Geumcheon	k1gMnSc3	Geumcheon-g
Guro-gu	Guro	k1gMnSc3	Guro-g
Gwanak-gu	Gwanak	k1gMnSc3	Gwanak-g
Gwangjin-gu	Gwangjin	k1gMnSc3	Gwangjin-g
Jangcheon-gu	Jangcheon	k1gMnSc3	Jangcheon-g
Jeongdeungpo-gu	Jeongdeungpo	k1gMnSc3	Jeongdeungpo-g
Jongsan-gu	Jongsan	k1gMnSc3	Jongsan-g
Jongno-gu	Jongno	k1gMnSc3	Jongno-g
Jung-gu	Jung	k1gMnSc3	Jung-g
Jungnang-gu	Jungnang	k1gMnSc3	Jungnang-g
Mapo-gu	Mapo	k1gMnSc3	Mapo-g
Nowon-gu	Nowon	k1gMnSc3	Nowon-g
Seocho-gu	Seocho	k1gMnSc3	Seocho-g
Seodaemun-gu	Seodaemun	k1gMnSc3	Seodaemun-g
Seongbuk-gu	Seongbuk	k1gMnSc3	Seongbuk-g
Seongdong-gu	Seongdong	k1gMnSc3	Seongdong-g
Songpa-gu	Songpa	k1gMnSc3	Songpa-g
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Soul	Soul	k1gInSc1	Soul
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
