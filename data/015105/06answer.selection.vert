<s>
Vzdušné	vzdušný	k2eAgFnPc4d1
síly	síla	k1gFnPc4
Armády	armáda	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
VzS	VzS	k1gFnSc2
AČR	AČR	kA
<g/>
)	)	kIx)
představují	představovat	k5eAaImIp3nP
vojenské	vojenský	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
české	český	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
působící	působící	k2eAgFnSc2d1
v	v	k7c6
rámci	rámec	k1gInSc6
Velitelství	velitelství	k1gNnSc2
vzdušných	vzdušný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Letectvo	letectvo	k1gNnSc1
zajišťuje	zajišťovat	k5eAaImIp3nS
společně	společně	k6eAd1
s	s	k7c7
pozemním	pozemní	k2eAgNnSc7d1
vojskem	vojsko	k1gNnSc7
hlavní	hlavní	k2eAgFnSc4d1
bojovou	bojový	k2eAgFnSc4d1
sílu	síla	k1gFnSc4
AČR	AČR	kA
<g/>
.	.	kIx.
</s>