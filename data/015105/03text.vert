<s>
Vzdušné	vzdušný	k2eAgFnPc4d1
síly	síla	k1gFnPc4
Armády	armáda	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Vzdušné	vzdušný	k2eAgFnPc4d1
síly	síla	k1gFnPc4
Armády	armáda	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
Znak	znak	k1gInSc4
VzS	VzS	k1gFnSc2
AČR	AČR	kA
Znak	znak	k1gInSc4
VzS	VzS	k1gFnPc2
AČR	AČR	kA
Země	zem	k1gFnSc2
</s>
<s>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Existence	existence	k1gFnSc1
</s>
<s>
1918	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
čs	čs	kA
<g/>
.	.	kIx.
letectvo	letectvo	k1gNnSc1
Vznik	vznik	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1993	#num#	k4
Typ	typ	k1gInSc1
</s>
<s>
letectvo	letectvo	k1gNnSc1
a	a	k8xC
protivzdušná	protivzdušný	k2eAgFnSc1d1
obrana	obrana	k1gFnSc1
Funkce	funkce	k1gFnSc1
</s>
<s>
ochrana	ochrana	k1gFnSc1
vzdušného	vzdušný	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
–	–	k?
NATINAMDS	NATINAMDS	kA
<g/>
,	,	kIx,
podpora	podpora	k1gFnSc1
pozemních	pozemní	k2eAgFnPc2d1
sil	síla	k1gFnPc2
a	a	k8xC
IZS	IZS	kA
Velikost	velikost	k1gFnSc1
</s>
<s>
53	#num#	k4
letounů	letoun	k1gInPc2
<g/>
50	#num#	k4
vrtulníků	vrtulník	k1gInPc2
<g/>
5400	#num#	k4
vojáků	voják	k1gMnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
Velitelé	velitel	k1gMnPc1
</s>
<s>
genmjr.	genmjr.	k?
Petr	Petr	k1gMnSc1
Mikulenka	Mikulenka	k1gFnSc1
Nadřazené	nadřazený	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
</s>
<s>
Generální	generální	k2eAgInSc1d1
štáb	štáb	k1gInSc1
AČR	AČR	kA
–	–	k?
Velitelství	velitelství	k1gNnSc2
vzdušných	vzdušný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Účast	účast	k1gFnSc1
Války	válka	k1gFnSc2
</s>
<s>
Válka	válka	k1gFnSc1
v	v	k7c6
Afghánistánu	Afghánistán	k1gInSc6
Mise	mise	k1gFnSc2
</s>
<s>
IFOR	IFOR	kA
<g/>
,	,	kIx,
SFOR	SFOR	kA
a	a	k8xC
SFOR	SFOR	kA
II	II	kA
<g/>
,	,	kIx,
ISAF	ISAF	kA
<g/>
,	,	kIx,
RSM	RSM	kA
<g/>
,	,	kIx,
MFO	MFO	kA
<g/>
,	,	kIx,
Air	Air	k1gFnSc1
Policing	Policing	k1gInSc1
(	(	kIx(
<g/>
Pobaltí	Pobaltí	k1gNnSc1
<g/>
,	,	kIx,
Island	Island	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Inherent	Inherent	k1gMnSc1
Resolve	Resolev	k1gFnSc2
(	(	kIx(
<g/>
Irák	Irák	k1gInSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Insignie	insignie	k1gFnPc1
Znak	znak	k1gInSc4
</s>
<s>
Znak	znak	k1gInSc4
na	na	k7c4
SOP	sopit	k5eAaImRp2nS
</s>
<s>
(	(	kIx(
<g/>
pouze	pouze	k6eAd1
na	na	k7c6
dopravních	dopravní	k2eAgInPc6d1
letounech	letoun	k1gInPc6
<g/>
)	)	kIx)
Letouny	letoun	k1gInPc1
Bitevní	bitevní	k2eAgFnSc2d1
</s>
<s>
L-159A	L-159A	k4
Průzkumné	průzkumný	k2eAgFnPc4d1
</s>
<s>
L-410FG	L-410FG	k4
Stíhací	stíhací	k2eAgInSc1d1
</s>
<s>
JAS-	JAS-	k?
<g/>
39	#num#	k4
<g/>
C	C	kA
<g/>
/	/	kIx~
<g/>
D	D	kA
Cvičné	cvičný	k2eAgInPc1d1
</s>
<s>
L-	L-	k?
<g/>
159	#num#	k4
<g/>
T	T	kA
<g/>
1	#num#	k4
<g/>
,	,	kIx,
L-159T2	L-159T2	k1gFnSc1
Dopravní	dopravní	k2eAgFnSc1d1
</s>
<s>
A	a	k9
<g/>
319	#num#	k4
<g/>
CJ	CJ	kA
<g/>
,	,	kIx,
CL-601	CL-601	k1gFnSc1
Transportní	transportní	k2eAgFnSc2d1
</s>
<s>
C-	C-	k?
<g/>
295	#num#	k4
<g/>
M	M	kA
<g/>
,	,	kIx,
L-410UVP-E	L-410UVP-E	k1gFnPc1
Vrtulníky	vrtulník	k1gInPc1
Bitevní	bitevní	k2eAgInSc1d1
</s>
<s>
Mi-	Mi-	k?
<g/>
24	#num#	k4
<g/>
V	v	k7c6
Transportní	transportní	k2eAgFnSc6d1
</s>
<s>
Mi-	Mi-	k?
<g/>
8	#num#	k4
<g/>
,	,	kIx,
Mi-	Mi-	k1gFnSc1
<g/>
17	#num#	k4
<g/>
,	,	kIx,
Mi-	Mi-	k1gFnSc1
<g/>
171	#num#	k4
<g/>
Š	Š	kA
Víceúčelové	víceúčelový	k2eAgFnSc2d1
</s>
<s>
W-3A	W-3A	k4
</s>
<s>
Vzdušné	vzdušný	k2eAgFnPc4d1
síly	síla	k1gFnPc4
Armády	armáda	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
VzS	VzS	k1gFnSc2
AČR	AČR	kA
<g/>
)	)	kIx)
představují	představovat	k5eAaImIp3nP
vojenské	vojenský	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
české	český	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
působící	působící	k2eAgFnSc2d1
v	v	k7c6
rámci	rámec	k1gInSc6
Velitelství	velitelství	k1gNnSc2
vzdušných	vzdušný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Letectvo	letectvo	k1gNnSc1
zajišťuje	zajišťovat	k5eAaImIp3nS
společně	společně	k6eAd1
s	s	k7c7
pozemním	pozemní	k2eAgNnSc7d1
vojskem	vojsko	k1gNnSc7
hlavní	hlavní	k2eAgFnSc4d1
bojovou	bojový	k2eAgFnSc4d1
sílu	síla	k1gFnSc4
AČR	AČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
nástupce	nástupce	k1gMnSc4
československého	československý	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
existovalo	existovat	k5eAaImAgNnS
v	v	k7c6
letech	léto	k1gNnPc6
1918	#num#	k4
až	až	k9
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současným	současný	k2eAgNnSc7d1
velitelem	velitel	k1gMnSc7
vzdušných	vzdušný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
je	být	k5eAaImIp3nS
od	od	k7c2
2	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2020	#num#	k4
generálmajor	generálmajor	k1gMnSc1
Petr	Petr	k1gMnSc1
Mikulenka	Mikulenka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vzdušné	vzdušný	k2eAgFnPc1d1
síly	síla	k1gFnPc1
AČR	AČR	kA
jsou	být	k5eAaImIp3nP
tvořeny	tvořit	k5eAaImNgFnP
letectvem	letectvo	k1gNnSc7
taktickým	taktický	k2eAgInSc7d1
(	(	kIx(
<g/>
21	#num#	k4
<g/>
.	.	kIx.
základna	základna	k1gFnSc1
v	v	k7c6
Čáslavi	Čáslav	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vrtulníkovým	vrtulníkový	k2eAgInSc7d1
(	(	kIx(
<g/>
22	#num#	k4
<g/>
.	.	kIx.
základna	základna	k1gFnSc1
u	u	k7c2
Náměště	Náměšť	k1gFnSc2
nad	nad	k7c7
Oslavou	oslava	k1gFnSc7
<g/>
)	)	kIx)
a	a	k8xC
dopravním	dopravní	k2eAgInSc6d1
(	(	kIx(
<g/>
24	#num#	k4
<g/>
.	.	kIx.
základna	základna	k1gFnSc1
v	v	k7c6
pražských	pražský	k2eAgInPc6d1
Kbelích	Kbely	k1gInPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dále	daleko	k6eAd2
vojskem	vojsko	k1gNnSc7
pozemní	pozemní	k2eAgFnSc2d1
protivzdušné	protivzdušný	k2eAgFnSc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
obrany	obrana	k1gFnSc2
a	a	k8xC
silami	síla	k1gFnPc7
a	a	k8xC
prostředky	prostředek	k1gInPc7
velení	velení	k1gNnSc2
<g/>
,	,	kIx,
řízení	řízení	k1gNnSc2
a	a	k8xC
průzkumu	průzkum	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Ke	k	k7c3
dni	den	k1gInSc3
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2018	#num#	k4
disponovala	disponovat	k5eAaBmAgFnS
AČR	AČR	kA
celkem	celkem	k6eAd1
103	#num#	k4
letadly	letadlo	k1gNnPc7
<g/>
:	:	kIx,
38	#num#	k4
bojovými	bojový	k2eAgInPc7d1
a	a	k8xC
cvičně-bojovými	cvičně-bojový	k2eAgInPc7d1
letouny	letoun	k1gInPc7
<g/>
,	,	kIx,
15	#num#	k4
dopravními	dopravní	k2eAgInPc7d1
<g/>
,	,	kIx,
transportními	transportní	k2eAgInPc7d1
a	a	k8xC
speciálními	speciální	k2eAgInPc7d1
letouny	letoun	k1gInPc7
a	a	k8xC
50	#num#	k4
vrtulníky	vrtulník	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Stroje	stroj	k1gInPc4
určené	určený	k2eAgInPc4d1
k	k	k7c3
výcviku	výcvik	k1gInSc3
vojenských	vojenský	k2eAgMnPc2d1
pilotů	pilot	k1gMnPc2
provozuje	provozovat	k5eAaImIp3nS
státní	státní	k2eAgInSc1d1
podnik	podnik	k1gInSc1
LOM	lom	k1gInSc1
Praha	Praha	k1gFnSc1
v	v	k7c6
Centru	centrum	k1gNnSc6
leteckého	letecký	k2eAgInSc2d1
výcviku	výcvik	k1gInSc2
na	na	k7c6
pardubickém	pardubický	k2eAgNnSc6d1
letišti	letiště	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bezpilotními	bezpilotní	k2eAgInPc7d1
letouny	letoun	k1gInPc7
disponuje	disponovat	k5eAaBmIp3nS
533	#num#	k4
<g/>
.	.	kIx.
prapor	prapor	k1gInSc1
bezpilotních	bezpilotní	k2eAgInPc2d1
systémů	systém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
hlavní	hlavní	k2eAgInPc4d1
úkoly	úkol	k1gInPc4
vzdušných	vzdušný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
patří	patřit	k5eAaImIp3nS
zajištění	zajištění	k1gNnSc1
nedotknutelnosti	nedotknutelnost	k1gFnSc2
vzdušného	vzdušný	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
pomocí	pomocí	k7c2
prostředků	prostředek	k1gInPc2
vyčleňovaných	vyčleňovaný	k2eAgInPc2d1
pro	pro	k7c4
alianční	alianční	k2eAgInSc4d1
systém	systém	k1gInSc4
NATINAMDS	NATINAMDS	kA
či	či	k8xC
prostředků	prostředek	k1gInPc2
národního	národní	k2eAgInSc2d1
posilového	posilový	k2eAgInSc2d1
systému	systém	k1gInSc2
(	(	kIx(
<g/>
NaPoSy	NaPoSa	k1gFnSc2
<g/>
)	)	kIx)
protivzdušné	protivzdušný	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdušné	vzdušný	k2eAgFnPc4d1
síly	síla	k1gFnPc4
také	také	k9
poskytují	poskytovat	k5eAaImIp3nP
leteckou	letecký	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
pozemním	pozemní	k2eAgFnPc3d1
silám	síla	k1gFnPc3
<g/>
,	,	kIx,
zabezpečují	zabezpečovat	k5eAaImIp3nP
mobilitu	mobilita	k1gFnSc4
vojsk	vojsko	k1gNnPc2
nebo	nebo	k8xC
umožňují	umožňovat	k5eAaImIp3nP
provedení	provedení	k1gNnSc4
rychlého	rychlý	k2eAgInSc2d1
manévru	manévr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mírovém	mírový	k2eAgInSc6d1
stavu	stav	k1gInSc6
se	se	k3xPyFc4
podílejí	podílet	k5eAaImIp3nP
například	například	k6eAd1
na	na	k7c6
činnosti	činnost	k1gFnSc6
letecké	letecký	k2eAgFnSc2d1
záchranné	záchranný	k2eAgFnSc2d1
služby	služba	k1gFnSc2
a	a	k8xC
letecké	letecký	k2eAgFnSc2d1
pátrací	pátrací	k2eAgFnSc2d1
a	a	k8xC
záchranné	záchranný	k2eAgFnSc2d1
služby	služba	k1gFnSc2
<g/>
,	,	kIx,
působí	působit	k5eAaImIp3nS
v	v	k7c6
rámci	rámec	k1gInSc6
Integrovaného	integrovaný	k2eAgInSc2d1
záchranného	záchranný	k2eAgInSc2d1
systému	systém	k1gInSc2
a	a	k8xC
plní	plnit	k5eAaImIp3nP
úkoly	úkol	k1gInPc1
vyplývající	vyplývající	k2eAgInPc1d1
z	z	k7c2
přijatých	přijatý	k2eAgInPc2d1
zákonů	zákon	k1gInPc2
a	a	k8xC
mezirezortních	mezirezortní	k2eAgFnPc2d1
dohod	dohoda	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Počátky	počátek	k1gInPc4
českého	český	k2eAgNnSc2d1
vojenského	vojenský	k2eAgNnSc2d1
letectví	letectví	k1gNnSc2
</s>
<s>
Prvním	první	k4xOgInSc7
českým	český	k2eAgInSc7d1
vojenským	vojenský	k2eAgInSc7d1
pilotem	pilot	k1gInSc7
se	se	k3xPyFc4
ještě	ještě	k9
před	před	k7c7
vypuknutím	vypuknutí	k1gNnSc7
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
stal	stát	k5eAaPmAgMnS
Rudolf	Rudolf	k1gMnSc1
Holeka	Holeka	k1gMnSc1
<g/>
,	,	kIx,
poručík	poručík	k1gMnSc1
rakousko-uherské	rakousko-uherský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
války	válka	k1gFnSc2
pak	pak	k6eAd1
v	v	k7c6
rakousko-uherském	rakousko-uherský	k2eAgNnSc6d1
letectvu	letectvo	k1gNnSc6
sloužilo	sloužit	k5eAaImAgNnS
kolem	kolem	k7c2
500	#num#	k4
českých	český	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
a	a	k8xC
několik	několik	k4yIc1
letců	letec	k1gMnPc2
působilo	působit	k5eAaImAgNnS
také	také	k9
v	v	k7c6
dohodových	dohodový	k2eAgNnPc6d1
letectvech	letectvo	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Rusku	Rusko	k1gNnSc6
byl	být	k5eAaImAgMnS
koncem	koncem	k7c2
února	únor	k1gInSc2
1918	#num#	k4
zřízen	zřízen	k2eAgInSc4d1
1	#num#	k4
<g/>
.	.	kIx.
československý	československý	k2eAgInSc1d1
letecký	letecký	k2eAgInSc1d1
a	a	k8xC
automobilní	automobilní	k2eAgInSc1d1
oddíl	oddíl	k1gInSc1
čs	čs	kA
<g/>
.	.	kIx.
legií	legie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
po	po	k7c6
vzniku	vznik	k1gInSc6
samostatného	samostatný	k2eAgNnSc2d1
Československa	Československo	k1gNnSc2
byl	být	k5eAaImAgInS
dne	den	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1918	#num#	k4
založen	založen	k2eAgInSc1d1
Letecký	letecký	k2eAgInSc1d1
sbor	sbor	k1gInSc1
československé	československý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Československé	československý	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
(	(	kIx(
<g/>
1918	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Československé	československý	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Zleva	zleva	k6eAd1
doprava	doprava	k1gFnSc1
<g/>
:	:	kIx,
<g/>
SPAD	spad	k1gInSc1
S.	S.	kA
<g/>
VII	VII	kA
Leteckého	letecký	k2eAgMnSc4d1
sboruAvia	sboruAvius	k1gMnSc4
B-	B-	k1gMnSc4
<g/>
534	#num#	k4
<g/>
,	,	kIx,
stíhací	stíhací	k2eAgInSc1d1
letoun	letoun	k1gInSc1
30	#num#	k4
<g/>
.	.	kIx.
letHurricane	letHurricanout	k5eAaPmIp3nS
310	#num#	k4
<g/>
.	.	kIx.
perutě	peruť	k1gFnPc1
–	–	k?
bitva	bitva	k1gFnSc1
o	o	k7c4
BritániiSpitfiry	BritániiSpitfir	k1gMnPc4
na	na	k7c6
přehlídce	přehlídka	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
</s>
<s>
Vojenské	vojenský	k2eAgInPc1d1
letouny	letoun	k1gInPc1
Leteckého	letecký	k2eAgInSc2d1
sboru	sbor	k1gInSc2
byly	být	k5eAaImAgFnP
použity	použít	k5eAaPmNgFnP
k	k	k7c3
leteckému	letecký	k2eAgInSc3d1
průzkumu	průzkum	k1gInSc3
v	v	k7c6
tzv.	tzv.	kA
sedmidenní	sedmidenní	k2eAgFnSc6d1
válce	válka	k1gFnSc6
s	s	k7c7
Polskem	Polsko	k1gNnSc7
o	o	k7c4
Těšínsko	Těšínsko	k1gNnSc4
a	a	k8xC
také	také	k6eAd1
nasazeny	nasadit	k5eAaPmNgFnP
v	v	k7c6
bojích	boj	k1gInPc6
s	s	k7c7
Maďarskem	Maďarsko	k1gNnSc7
během	během	k7c2
maďarsko-československé	maďarsko-československý	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
letech	léto	k1gNnPc6
1918	#num#	k4
až	až	k9
1919	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Mnichovská	mnichovský	k2eAgFnSc1d1
dohoda	dohoda	k1gFnSc1
a	a	k8xC
následná	následný	k2eAgFnSc1d1
okupace	okupace	k1gFnSc1
Čech	Čechy	k1gFnPc2
a	a	k8xC
Moravy	Morava	k1gFnPc1
znamenaly	znamenat	k5eAaImAgFnP
dočasný	dočasný	k2eAgInSc4d1
zánik	zánik	k1gInSc4
letectva	letectvo	k1gNnSc2
na	na	k7c6
území	území	k1gNnSc6
Československa	Československo	k1gNnSc2
<g/>
,	,	kIx,
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
Vzdušných	vzdušný	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
Slovenského	slovenský	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Mnozí	mnohý	k2eAgMnPc1d1
příslušníci	příslušník	k1gMnPc1
bývalého	bývalý	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
první	první	k4xOgFnSc2
republiky	republika	k1gFnSc2
bojovali	bojovat	k5eAaImAgMnP
za	za	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
exilu	exil	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Francii	Francie	k1gFnSc6
byla	být	k5eAaImAgFnS
1	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1940	#num#	k4
uzavřena	uzavřen	k2eAgFnSc1d1
dohoda	dohoda	k1gFnSc1
o	o	k7c6
zřízení	zřízení	k1gNnSc6
samostatného	samostatný	k2eAgNnSc2d1
československého	československý	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
kapitulaci	kapitulace	k1gFnSc6
Francie	Francie	k1gFnSc2
vzniklo	vzniknout	k5eAaPmAgNnS
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
několik	několik	k4yIc4
československých	československý	k2eAgFnPc2d1
perutí	peruť	k1gFnPc2
v	v	k7c6
rámci	rámec	k1gInSc6
RAF	raf	k0
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
byly	být	k5eAaImAgFnP
nasazeny	nasadit	k5eAaPmNgFnP
v	v	k7c6
bitvě	bitva	k1gFnSc6
o	o	k7c6
Británii	Británie	k1gFnSc6
<g/>
,	,	kIx,
bitvě	bitva	k1gFnSc6
o	o	k7c4
Atlantik	Atlantik	k1gInSc4
nebo	nebo	k8xC
bitvě	bitva	k1gFnSc6
o	o	k7c6
Normandii	Normandie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
Slovenském	slovenský	k2eAgNnSc6d1
národním	národní	k2eAgNnSc6d1
povstání	povstání	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
1944	#num#	k4
bojovali	bojovat	k5eAaImAgMnP
příslušníci	příslušník	k1gMnPc1
1	#num#	k4
<g/>
.	.	kIx.
československého	československý	k2eAgInSc2d1
stíhacího	stíhací	k2eAgInSc2d1
leteckého	letecký	k2eAgInSc2d1
pluku	pluk	k1gInSc2
spolu	spolu	k6eAd1
s	s	k7c7
povstaleckými	povstalecký	k2eAgMnPc7d1
letci	letec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Sovětském	sovětský	k2eAgInSc6d1
svazu	svaz	k1gInSc6
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
stala	stát	k5eAaPmAgFnS
největší	veliký	k2eAgFnSc7d3
jednotkou	jednotka	k1gFnSc7
československého	československý	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
na	na	k7c6
východní	východní	k2eAgFnSc6d1
frontě	fronta	k1gFnSc6
1	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
smíšená	smíšený	k2eAgFnSc1d1
letecká	letecký	k2eAgFnSc1d1
divize	divize	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
zúčastnila	zúčastnit	k5eAaPmAgFnS
například	například	k6eAd1
Ostravské	ostravský	k2eAgFnPc4d1
operace	operace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
skončení	skončení	k1gNnSc6
války	válka	k1gFnSc2
bylo	být	k5eAaImAgNnS
letectvo	letectvo	k1gNnSc1
na	na	k7c6
českém	český	k2eAgInSc6d1
území	území	k1gNnSc2
obnoveno	obnoven	k2eAgNnSc1d1
a	a	k8xC
po	po	k7c6
únoru	únor	k1gInSc6
1948	#num#	k4
se	se	k3xPyFc4
dostalo	dostat	k5eAaPmAgNnS
pod	pod	k7c4
kontrolu	kontrola	k1gFnSc4
komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1955	#num#	k4
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
součástí	součást	k1gFnSc7
sil	silo	k1gNnPc2
Varšavské	varšavský	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
a	a	k8xC
bylo	být	k5eAaImAgNnS
přezbrojeno	přezbrojit	k5eAaPmNgNnS
na	na	k7c4
sovětskou	sovětský	k2eAgFnSc4d1
leteckou	letecký	k2eAgFnSc4d1
techniku	technika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Československé	československý	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
zaniklo	zaniknout	k5eAaPmAgNnS
s	s	k7c7
rozpadem	rozpad	k1gInSc7
federace	federace	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Vznik	vznik	k1gInSc1
českého	český	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
–	–	k?
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Historie	historie	k1gFnSc2
letecké	letecký	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
AČR	AČR	kA
<g/>
.	.	kIx.
</s>
<s>
Suchoj	Suchoj	k1gInSc1
Su-	Su-	k1gFnSc2
<g/>
25	#num#	k4
československého	československý	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
sloužil	sloužit	k5eAaImAgInS
v	v	k7c6
Armádě	armáda	k1gFnSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
MiG-	MiG-	k?
<g/>
21	#num#	k4
<g/>
PF	PF	kA
(	(	kIx(
<g/>
0	#num#	k4
<g/>
308	#num#	k4
<g/>
)	)	kIx)
vyřazený	vyřazený	k2eAgMnSc1d1
z	z	k7c2
výzbroje	výzbroj	k1gFnSc2
letectva	letectvo	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
Leteckém	letecký	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
Kbely	Kbely	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Vzdušné	vzdušný	k2eAgFnPc1d1
síly	síla	k1gFnPc1
samostatné	samostatný	k2eAgFnSc2d1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
byly	být	k5eAaImAgFnP
vytvořeny	vytvořit	k5eAaPmNgFnP
po	po	k7c6
rozpadu	rozpad	k1gInSc6
České	český	k2eAgFnSc2d1
a	a	k8xC
Slovenské	slovenský	k2eAgFnSc2d1
Federativní	federativní	k2eAgFnSc2d1
Republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letectvo	letectvo	k1gNnSc1
a	a	k8xC
protivzdušná	protivzdušný	k2eAgFnSc1d1
obrana	obrana	k1gFnSc1
(	(	kIx(
<g/>
PVO	PVO	kA
<g/>
)	)	kIx)
byly	být	k5eAaImAgInP
poté	poté	k6eAd1
tvořeny	tvořit	k5eAaImNgInP
smíšeným	smíšený	k2eAgInSc7d1
leteckým	letecký	k2eAgInSc7d1
sborem	sbor	k1gInSc7
<g/>
,	,	kIx,
dvěma	dva	k4xCgFnPc7
divizemi	divize	k1gFnPc7
PVO	PVO	kA
<g/>
,	,	kIx,
leteckým	letecký	k2eAgInSc7d1
školním	školní	k2eAgInSc7d1
plukem	pluk	k1gInSc7
a	a	k8xC
dopravním	dopravní	k2eAgInSc7d1
leteckým	letecký	k2eAgInSc7d1
plukem	pluk	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jejich	jejich	k3xOp3gInSc6
výzbroji	výzbroj	k1gInSc6
se	se	k3xPyFc4
nacházely	nacházet	k5eAaImAgInP
bojové	bojový	k2eAgInPc1d1
letouny	letoun	k1gInPc1
MiG-	MiG-	k1gFnSc1
<g/>
21	#num#	k4
<g/>
,	,	kIx,
MiG-	MiG-	k1gFnSc1
<g/>
23	#num#	k4
<g/>
,	,	kIx,
MiG-	MiG-	k1gFnSc1
<g/>
29	#num#	k4
<g/>
,	,	kIx,
Su-	Su-	k1gFnSc1
<g/>
22	#num#	k4
a	a	k8xC
Su-	Su-	k1gMnSc1
<g/>
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cvičné	cvičný	k2eAgInPc1d1
letouny	letoun	k1gInPc1
představovaly	představovat	k5eAaImAgInP
domácí	domácí	k2eAgInPc1d1
stroje	stroj	k1gInPc1
L-29	L-29	k1gFnPc2
a	a	k8xC
L-	L-	k1gFnPc2
<g/>
39	#num#	k4
<g/>
,	,	kIx,
vrtulníky	vrtulník	k1gInPc1
byly	být	k5eAaImAgInP
typu	typa	k1gFnSc4
Mi-	Mi-	k1gFnSc2
<g/>
24	#num#	k4
<g/>
,	,	kIx,
Mi-	Mi-	k1gFnSc1
<g/>
2	#num#	k4
<g/>
,	,	kIx,
Mi-	Mi-	k1gFnSc1
<g/>
8	#num#	k4
a	a	k8xC
Mi-	Mi-	k1gMnSc1
<g/>
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
službě	služba	k1gFnSc6
byly	být	k5eAaImAgInP
také	také	k9
dopravní	dopravní	k2eAgInPc1d1
a	a	k8xC
transportní	transportní	k2eAgInPc1d1
letouny	letoun	k1gInPc1
Tu-	Tu-	k1gFnSc1
<g/>
154	#num#	k4
<g/>
,	,	kIx,
Tu-	Tu-	k1gFnSc1
<g/>
134	#num#	k4
<g/>
,	,	kIx,
An-	An-	k1gFnSc1
<g/>
12	#num#	k4
<g/>
,	,	kIx,
An-	An-	k1gFnSc1
<g/>
26	#num#	k4
<g/>
,	,	kIx,
An-	An-	k1gFnSc1
<g/>
30	#num#	k4
a	a	k8xC
L-	L-	k1gMnSc3
<g/>
410	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1997	#num#	k4
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
sloučení	sloučení	k1gNnSc3
3	#num#	k4
<g/>
.	.	kIx.
sboru	sbor	k1gInSc3
taktického	taktický	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
a	a	k8xC
4	#num#	k4
<g/>
.	.	kIx.
sboru	sbor	k1gInSc2
protivzdušné	protivzdušný	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
a	a	k8xC
vytvoření	vytvoření	k1gNnSc4
Velitelství	velitelství	k1gNnSc2
vzdušných	vzdušný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
ve	v	k7c6
Staré	Staré	k2eAgFnSc6d1
Boleslavi	Boleslaev	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letectvo	letectvo	k1gNnSc1
tak	tak	k6eAd1
začalo	začít	k5eAaPmAgNnS
fungovat	fungovat	k5eAaImF
jako	jako	k9
samostatná	samostatný	k2eAgFnSc1d1
složka	složka	k1gFnSc1
Armády	armáda	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
v	v	k7c6
následujících	následující	k2eAgNnPc6d1
letech	léto	k1gNnPc6
prošlo	projít	k5eAaPmAgNnS
celou	celý	k2eAgFnSc7d1
řadou	řada	k1gFnSc7
dalších	další	k2eAgFnPc2d1
organizačních	organizační	k2eAgFnPc2d1
změn	změna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Příslušníci	příslušník	k1gMnPc1
vzdušných	vzdušný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
se	se	k3xPyFc4
po	po	k7c6
vstupu	vstup	k1gInSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
do	do	k7c2
NATO	NATO	kA
dne	den	k1gInSc2
12	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1999	#num#	k4
zapojili	zapojit	k5eAaPmAgMnP
jako	jako	k9
vůbec	vůbec	k9
první	první	k4xOgMnPc1
čeští	český	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
do	do	k7c2
činnosti	činnost	k1gFnSc2
aliance	aliance	k1gFnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
od	od	k7c2
této	tento	k3xDgFnSc2
doby	doba	k1gFnSc2
nepřetržitě	přetržitě	k6eNd1
zabezpečují	zabezpečovat	k5eAaImIp3nP
český	český	k2eAgInSc4d1
vzdušný	vzdušný	k2eAgInSc4d1
prostor	prostor	k1gInSc4
v	v	k7c6
rámci	rámec	k1gInSc6
hotovostního	hotovostní	k2eAgInSc2d1
systému	systém	k1gInSc2
NATINAMDS	NATINAMDS	kA
(	(	kIx(
<g/>
NATO	NATO	kA
Integrated	Integrated	k1gMnSc1
Air	Air	k1gMnSc1
and	and	k?
Missile	Missila	k1gFnSc3
Defence	Defenec	k1gInSc2
System	Systo	k1gNnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Společné	společný	k2eAgFnPc1d1
síly	síla	k1gFnPc1
AČR	AČR	kA
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Znak	znak	k1gInSc1
Společných	společný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
AČR	AČR	kA
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Společné	společný	k2eAgFnSc2d1
síly	síla	k1gFnSc2
AČR	AČR	kA
<g/>
.	.	kIx.
</s>
<s>
Existence	existence	k1gFnSc1
letectva	letectvo	k1gNnSc2
v	v	k7c6
podobě	podoba	k1gFnSc6
samostatné	samostatný	k2eAgFnSc2d1
složky	složka	k1gFnSc2
armády	armáda	k1gFnSc2
skončila	skončit	k5eAaPmAgFnS
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2003	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
spolu	spolu	k6eAd1
s	s	k7c7
pozemními	pozemní	k2eAgFnPc7d1
silami	síla	k1gFnPc7
stalo	stát	k5eAaPmAgNnS
součástí	součást	k1gFnSc7
nově	nově	k6eAd1
vzniklých	vzniklý	k2eAgFnPc2d1
Společných	společný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
AČR	AČR	kA
s	s	k7c7
velitelstvím	velitelství	k1gNnSc7
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velitel	velitel	k1gMnSc1
vzdušných	vzdušný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
byl	být	k5eAaImAgInS
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
zároveň	zároveň	k6eAd1
jedním	jeden	k4xCgMnSc7
ze	z	k7c2
zástupců	zástupce	k1gMnPc2
velitele	velitel	k1gMnSc2
Společných	společný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Zrušení	zrušení	k1gNnSc1
Velitelství	velitelství	k1gNnPc2
vzdušných	vzdušný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
ve	v	k7c6
Staré	Staré	k2eAgFnSc6d1
Boleslavi	Boleslaev	k1gFnSc6
a	a	k8xC
zřízení	zřízení	k1gNnSc6
Společných	společný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
se	se	k3xPyFc4
setkalo	setkat	k5eAaPmAgNnS
s	s	k7c7
kritikou	kritika	k1gFnSc7
řady	řada	k1gFnSc2
odborníků	odborník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
brigádního	brigádní	k2eAgMnSc2d1
generála	generál	k1gMnSc2
Libora	Libor	k1gMnSc2
Štefánika	Štefánik	k1gMnSc2
patřily	patřit	k5eAaImAgFnP
mezi	mezi	k7c4
největší	veliký	k2eAgInPc4d3
nedostatky	nedostatek	k1gInPc4
koncepce	koncepce	k1gFnSc2
Společných	společný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
například	například	k6eAd1
chybějící	chybějící	k2eAgInPc1d1
štáby	štáb	k1gInPc1
pro	pro	k7c4
velitele	velitel	k1gMnSc4
obou	dva	k4xCgFnPc2
složek	složka	k1gFnPc2
armády	armáda	k1gFnSc2
nebo	nebo	k8xC
komplikované	komplikovaný	k2eAgNnSc4d1
rozdělování	rozdělování	k1gNnSc4
finančních	finanční	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
byly	být	k5eAaImAgInP
vyčleňovány	vyčleňovat	k5eAaImNgInP
společně	společně	k6eAd1
pro	pro	k7c4
letectvo	letectvo	k1gNnSc4
i	i	k8xC
pozemní	pozemní	k2eAgFnPc4d1
síly	síla	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Samostatné	samostatný	k2eAgFnPc1d1
vzdušné	vzdušný	k2eAgFnPc1d1
síly	síla	k1gFnPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2013	#num#	k4
došlo	dojít	k5eAaPmAgNnS
na	na	k7c6
základě	základ	k1gInSc6
závěrů	závěr	k1gInPc2
Bílé	bílý	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
o	o	k7c6
obraně	obrana	k1gFnSc6
k	k	k7c3
obnovení	obnovení	k1gNnSc3
samostatných	samostatný	k2eAgNnPc2d1
velitelství	velitelství	k1gNnPc2
pozemních	pozemní	k2eAgFnPc2d1
a	a	k8xC
vzdušných	vzdušný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společné	společný	k2eAgFnPc4d1
síly	síla	k1gFnPc4
<g/>
,	,	kIx,
resp.	resp.	kA
jejich	jejich	k3xOp3gNnSc4
velitelství	velitelství	k1gNnSc4
<g/>
,	,	kIx,
byly	být	k5eAaImAgInP
zrušeny	zrušit	k5eAaPmNgInP
31	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgInSc7
z	z	k7c2
důvodů	důvod	k1gInPc2
této	tento	k3xDgFnSc2
reformy	reforma	k1gFnSc2
byla	být	k5eAaImAgFnS
snaha	snaha	k1gFnSc1
o	o	k7c4
úsporu	úspora	k1gFnSc4
zdrojů	zdroj	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Další	další	k2eAgFnSc7d1
organizační	organizační	k2eAgFnSc7d1
změnou	změna	k1gFnSc7
bylo	být	k5eAaImAgNnS
ke	k	k7c3
dni	den	k1gInSc3
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2014	#num#	k4
sloučení	sloučení	k1gNnSc4
22	#num#	k4
<g/>
.	.	kIx.
základny	základna	k1gFnPc1
letectva	letectvo	k1gNnSc2
a	a	k8xC
23	#num#	k4
<g/>
.	.	kIx.
základny	základna	k1gFnSc2
vrtulníkového	vrtulníkový	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInPc1
útvary	útvar	k1gInPc1
<g/>
,	,	kIx,
personál	personál	k1gInSc1
a	a	k8xC
technika	technika	k1gFnSc1
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
základem	základ	k1gInSc7
pro	pro	k7c4
22	#num#	k4
<g/>
.	.	kIx.
základnu	základna	k1gFnSc4
vrtulníkového	vrtulníkový	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
v	v	k7c6
Sedlci	Sedlec	k1gInSc6
<g/>
,	,	kIx,
Vícenicích	Vícenice	k1gFnPc6
u	u	k7c2
Náměště	Náměšť	k1gFnSc2
nad	nad	k7c7
Oslavou	oslava	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zahraniční	zahraniční	k2eAgFnSc1d1
mise	mise	k1gFnSc1
</s>
<s>
major	major	k1gMnSc1
Milan	Milan	k1gMnSc1
Nykodym	Nykodym	k1gInSc1
<g/>
,	,	kIx,
zástupce	zástupce	k1gMnSc1
velitele	velitel	k1gMnSc2
mise	mise	k1gFnSc2
na	na	k7c6
Islandu	Island	k1gInSc6
v	v	k7c6
r.	r.	kA
2015	#num#	k4
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Zahraniční	zahraniční	k2eAgFnSc2d1
mise	mise	k1gFnSc2
českého	český	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Vzdušné	vzdušný	k2eAgFnPc1d1
síly	síla	k1gFnPc1
AČR	AČR	kA
jsou	být	k5eAaImIp3nP
nasazovány	nasazovat	k5eAaImNgFnP
i	i	k9
ve	v	k7c4
prospěch	prospěch	k1gInSc4
mírových	mírový	k2eAgFnPc2d1
sil	síla	k1gFnPc2
OSN	OSN	kA
<g/>
,	,	kIx,
Severoatlantické	severoatlantický	k2eAgFnSc2d1
aliance	aliance	k1gFnSc2
nebo	nebo	k8xC
dalších	další	k2eAgMnPc2d1
zahraničních	zahraniční	k2eAgMnPc2d1
partnerů	partner	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
se	se	k3xPyFc4
účastnilo	účastnit	k5eAaImAgNnS
misí	mise	k1gFnSc7
IFOR	IFOR	kA
<g/>
,	,	kIx,
SFOR	SFOR	kA
a	a	k8xC
SFOR	SFOR	kA
II	II	kA
v	v	k7c6
Bosně	Bosna	k1gFnSc6
a	a	k8xC
Hercegovině	Hercegovina	k1gFnSc6
<g/>
,	,	kIx,
operace	operace	k1gFnPc1
ISAF	ISAF	kA
v	v	k7c6
Afghánistánu	Afghánistán	k1gInSc6
<g/>
,	,	kIx,
mise	mise	k1gFnSc1
Baltic	Baltice	k1gFnPc2
Air	Air	k1gFnSc1
Policing	Policing	k1gInSc1
v	v	k7c6
letech	léto	k1gNnPc6
2009	#num#	k4
a	a	k8xC
2012	#num#	k4
a	a	k8xC
mise	mise	k1gFnPc1
Icelandic	Icelandice	k1gInPc2
Air	Air	k1gFnSc1
Policing	Policing	k1gInSc1
v	v	k7c6
letech	léto	k1gNnPc6
2014	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
a	a	k8xC
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2013	#num#	k4
působí	působit	k5eAaImIp3nS
letecký	letecký	k2eAgInSc1d1
kontingent	kontingent	k1gInSc1
Vzdušných	vzdušný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
AČR	AČR	kA
v	v	k7c6
misi	mise	k1gFnSc6
Multinational	Multinational	k1gFnSc2
Force	force	k1gFnSc2
and	and	k?
Observers	Observers	k1gInSc1
(	(	kIx(
<g/>
MFO	MFO	kA
<g/>
)	)	kIx)
na	na	k7c6
Sinajském	sinajský	k2eAgInSc6d1
poloostrově	poloostrov	k1gInSc6
a	a	k8xC
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2015	#num#	k4
pokračuje	pokračovat	k5eAaImIp3nS
působení	působení	k1gNnSc4
české	český	k2eAgFnSc2d1
letecké	letecký	k2eAgFnSc2d1
poradní	poradní	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
AAT	AAT	kA
(	(	kIx(
<g/>
Air	Air	k1gFnSc1
Advisor	Advisor	k1gMnSc1
Team	team	k1gInSc1
<g/>
)	)	kIx)
na	na	k7c6
mezinárodním	mezinárodní	k2eAgNnSc6d1
letišti	letiště	k1gNnSc6
KAIA	KAIA	kA
v	v	k7c6
Kábulu	Kábul	k1gInSc6
pod	pod	k7c7
hlavičkou	hlavička	k1gFnSc7
Resolute	Resolut	k1gInSc5
Support	support	k1gInSc1
Mission	Mission	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červnu	červen	k1gInSc6
2016	#num#	k4
začal	začít	k5eAaPmAgMnS
na	na	k7c6
letecké	letecký	k2eAgFnSc6d1
základně	základna	k1gFnSc6
Balad	balada	k1gFnPc2
v	v	k7c6
Iráku	Irák	k1gInSc6
působit	působit	k5eAaImF
tzv.	tzv.	kA
Letecký	letecký	k2eAgInSc1d1
poradní	poradní	k2eAgInSc1d1
tým	tým	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
úkolem	úkol	k1gInSc7
je	být	k5eAaImIp3nS
výcvik	výcvik	k1gInSc1
iráckých	irácký	k2eAgMnPc2d1
pilotů	pilot	k1gMnPc2
a	a	k8xC
mechaniků	mechanik	k1gMnPc2
letounů	letoun	k1gInPc2
L-	L-	k1gFnSc3
<g/>
159	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Struktura	struktura	k1gFnSc1
</s>
<s>
Velitelství	velitelství	k1gNnSc1
</s>
<s>
Vzdušné	vzdušný	k2eAgFnPc1d1
síly	síla	k1gFnPc1
AČR	AČR	kA
jsou	být	k5eAaImIp3nP
podřízeny	podřídit	k5eAaPmNgInP
Velitelství	velitelství	k1gNnSc3
vzdušných	vzdušný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
respektive	respektive	k9
Generálnímu	generální	k2eAgInSc3d1
štábu	štáb	k1gInSc3
AČR	AČR	kA
<g/>
.	.	kIx.
</s>
<s>
Jednotky	jednotka	k1gFnPc1
a	a	k8xC
výzbroj	výzbroj	k1gFnSc1
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1
útvary	útvar	k1gInPc1
Vzdušných	vzdušný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
AČR	AČR	kA
a	a	k8xC
jejich	jejich	k3xOp3gFnSc4
výzbroj	výzbroj	k1gFnSc4
(	(	kIx(
<g/>
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
srpnu	srpen	k1gInSc3
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[[	[[	k?
<g/>
file	file	k1gNnPc6
<g/>
:	:	kIx,
<g/>
Czech	Czech	k1gInSc1
Republic	Republice	k1gFnPc2
adm	adm	k?
location	location	k1gInSc1
map	mapa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
350	#num#	k4
<g/>
px	px	k?
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
<g/>
Vzdušné	vzdušný	k2eAgFnSc2d1
síly	síla	k1gFnSc2
AČR	AČR	kA
<g/>
|	|	kIx~
<g/>
Vzdušné	vzdušný	k2eAgFnPc1d1
síly	síla	k1gFnPc1
AČR	AČR	kA
<g/>
|	|	kIx~
<g/>
]]	]]	k?
<g/>
Čáslav	Čáslav	k1gFnSc1
</s>
<s>
Sedlec	Sedlec	k1gInSc1
</s>
<s>
Praha-Kbely	Praha-Kbela	k1gFnPc1
</s>
<s>
Strakonice	Strakonice	k1gFnPc1
</s>
<s>
Stará	starat	k5eAaImIp3nS
Boleslav	Boleslav	k1gMnSc1
</s>
<s>
Pardubice	Pardubice	k1gInPc1
</s>
<s>
Bojové	bojový	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
<g/>
:	:	kIx,
21	#num#	k4
<g/>
.	.	kIx.
základna	základna	k1gFnSc1
taktického	taktický	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
22	#num#	k4
<g/>
.	.	kIx.
základna	základna	k1gFnSc1
vrtulníkového	vrtulníkový	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
</s>
<s>
Jednotky	jednotka	k1gFnPc1
bojové	bojový	k2eAgFnSc2d1
podpory	podpora	k1gFnSc2
<g/>
:	:	kIx,
24	#num#	k4
<g/>
.	.	kIx.
základna	základna	k1gFnSc1
dopravního	dopravní	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
25	#num#	k4
<g/>
.	.	kIx.
protiletadlový	protiletadlový	k2eAgInSc1d1
raketový	raketový	k2eAgInSc1d1
pluk	pluk	k1gInSc1
26	#num#	k4
<g/>
.	.	kIx.
pluk	pluk	k1gInSc1
velení	velení	k1gNnSc2
<g/>
,	,	kIx,
řízení	řízení	k1gNnSc2
a	a	k8xC
průzkumu	průzkum	k1gInSc2
</s>
<s>
Jednotky	jednotka	k1gFnPc1
bojového	bojový	k2eAgNnSc2d1
zabezpečení	zabezpečení	k1gNnSc2
<g/>
:	:	kIx,
Správa	správa	k1gFnSc1
letiště	letiště	k1gNnSc2
Pardubice	Pardubice	k1gInPc1
</s>
<s>
JednotkaVýzbroj	JednotkaVýzbroj	k1gInSc1
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
základna	základna	k1gFnSc1
taktického	taktický	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
<g/>
,	,	kIx,
Čáslav	Čáslav	k1gFnSc1
</s>
<s>
211	#num#	k4
<g/>
.	.	kIx.
taktická	taktický	k2eAgFnSc1d1
letka	letka	k1gFnSc1
</s>
<s>
12	#num#	k4
JAS-39C	JAS-39C	k1gMnSc1
Gripen	Gripen	k2eAgMnSc1d1
<g/>
,	,	kIx,
2	#num#	k4
JAS-39D	JAS-39D	k1gFnPc2
Gripen	Gripen	k2eAgMnSc1d1
</s>
<s>
212	#num#	k4
<g/>
.	.	kIx.
taktická	taktický	k2eAgFnSc1d1
letka	letka	k1gFnSc1
</s>
<s>
14	#num#	k4
L-159A	L-159A	k1gFnSc1
ALCA	ALCA	kA
<g/>
,	,	kIx,
2	#num#	k4
L-	L-	k1gFnSc1
<g/>
159	#num#	k4
<g/>
T	T	kA
<g/>
1	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
213	#num#	k4
<g/>
.	.	kIx.
výcviková	výcvikový	k2eAgFnSc1d1
letka	letka	k1gFnSc1
</s>
<s>
2	#num#	k4
L-159A	L-159A	k1gFnSc1
ALCA	ALCA	kA
<g/>
,	,	kIx,
3	#num#	k4
L-	L-	k1gFnSc1
<g/>
159	#num#	k4
<g/>
T	T	kA
<g/>
1	#num#	k4
<g/>
+	+	kIx~
<g/>
,	,	kIx,
3	#num#	k4
L-159T2	L-159T2	k1gFnPc2
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
základna	základna	k1gFnSc1
vrtulníkového	vrtulníkový	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
<g/>
,	,	kIx,
Sedlec	Sedlec	k1gInSc1
</s>
<s>
221	#num#	k4
<g/>
.	.	kIx.
vrtulníková	vrtulníkový	k2eAgFnSc1d1
letka	letka	k1gFnSc1
</s>
<s>
17	#num#	k4
Mi-	Mi-	k1gFnSc1
<g/>
24	#num#	k4
<g/>
V	V	kA
<g/>
,	,	kIx,
4	#num#	k4
Mi-	Mi-	k1gFnSc1
<g/>
171	#num#	k4
<g/>
Š	Š	kA
</s>
<s>
222	#num#	k4
<g/>
.	.	kIx.
vrtulníková	vrtulníkový	k2eAgFnSc1d1
letka	letka	k1gFnSc1
</s>
<s>
11	#num#	k4
Mi-	Mi-	k1gFnSc1
<g/>
171	#num#	k4
<g/>
Š	Š	kA
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
základna	základna	k1gFnSc1
dopravního	dopravní	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
<g/>
,	,	kIx,
Praha-Kbely	Praha-Kbela	k1gFnSc2
</s>
<s>
241	#num#	k4
<g/>
.	.	kIx.
dopravní	dopravní	k2eAgFnSc1d1
letka	letka	k1gFnSc1
</s>
<s>
2	#num#	k4
A	a	k8xC
<g/>
319	#num#	k4
<g/>
CJ	CJ	kA
<g/>
,	,	kIx,
1	#num#	k4
CL-601	CL-601	k1gFnPc2
Challenger	Challengra	k1gFnPc2
</s>
<s>
242	#num#	k4
<g/>
.	.	kIx.
transportní	transportní	k2eAgFnSc1d1
a	a	k8xC
speciální	speciální	k2eAgFnSc1d1
letka	letka	k1gFnSc1
</s>
<s>
4	#num#	k4
C-	C-	k1gFnSc1
<g/>
295	#num#	k4
<g/>
M	M	kA
<g/>
,	,	kIx,
4	#num#	k4
L-	L-	k1gFnPc2
<g/>
410	#num#	k4
<g/>
UVP-E	UVP-E	k1gFnPc2
<g/>
,	,	kIx,
2	#num#	k4
L-410FG	L-410FG	k1gFnPc2
</s>
<s>
243	#num#	k4
<g/>
.	.	kIx.
vrtulníková	vrtulníkový	k2eAgFnSc1d1
letka	letka	k1gFnSc1
</s>
<s>
3	#num#	k4
Mi-	Mi-	k1gFnSc1
<g/>
8	#num#	k4
<g/>
S	s	k7c7
<g/>
,	,	kIx,
5	#num#	k4
Mi-	Mi-	k1gFnPc2
<g/>
17	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
W-3A	W-3A	k1gMnPc2
Sokol	Sokol	k1gMnSc1
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
protiletadlový	protiletadlový	k2eAgInSc1d1
raketový	raketový	k2eAgInSc1d1
pluk	pluk	k1gInSc1
<g/>
,	,	kIx,
Strakonice	Strakonice	k1gFnPc1
</s>
<s>
251	#num#	k4
<g/>
.	.	kIx.
protiletadlový	protiletadlový	k2eAgInSc1d1
raketový	raketový	k2eAgInSc1d1
oddíl	oddíl	k1gInSc1
</s>
<s>
4	#num#	k4
baterie	baterie	k1gFnSc1
2K12	2K12	k4
KUB	Kuba	k1gFnPc2
(	(	kIx(
<g/>
1	#num#	k4
baterii	baterie	k1gFnSc4
tvoří	tvořit	k5eAaImIp3nS
1	#num#	k4
komplet	komplet	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
252	#num#	k4
<g/>
.	.	kIx.
protiletadlový	protiletadlový	k2eAgInSc1d1
raketový	raketový	k2eAgInSc1d1
oddíl	oddíl	k1gInSc1
</s>
<s>
2	#num#	k4
baterie	baterie	k1gFnSc1
S-10M	S-10M	k1gFnSc1
a	a	k8xC
2	#num#	k4
baterie	baterie	k1gFnPc4
RBS-70	RBS-70	k1gFnSc2
(	(	kIx(
<g/>
8	#num#	k4
kompletů	komplet	k1gInPc2
na	na	k7c4
baterii	baterie	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
pluk	pluk	k1gInSc1
velení	velení	k1gNnSc2
<g/>
,	,	kIx,
řízení	řízení	k1gNnSc2
a	a	k8xC
průzkumu	průzkum	k1gInSc2
<g/>
,	,	kIx,
Stará	starý	k2eAgFnSc1d1
Boleslavradiolokační	Boleslavradiolokační	k2eAgFnSc1d1
výzbroj	výzbroj	k1gFnSc1
</s>
<s>
Správa	správa	k1gFnSc1
letiště	letiště	k1gNnSc2
Pardubiceletadla	Pardubiceletadlo	k1gNnSc2
CLV	CLV	kA
provozovaná	provozovaný	k2eAgFnSc1d1
s.	s.	k?
p.	p.	k?
LOM	lom	k1gInSc1
Praha	Praha	k1gFnSc1
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Letecké	letecký	k2eAgFnPc1d1
základny	základna	k1gFnPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
letišť	letiště	k1gNnPc2
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Gripen	Gripen	k2eAgInSc1d1
startující	startující	k2eAgInSc1d1
z	z	k7c2
čáslavské	čáslavský	k2eAgFnSc2d1
základny	základna	k1gFnSc2
</s>
<s>
Vojenská	vojenský	k2eAgNnPc1d1
letiště	letiště	k1gNnPc1
</s>
<s>
Letiště	letiště	k1gNnSc1
Čáslav	Čáslav	k1gFnSc1
(	(	kIx(
<g/>
Chotusice	Chotusice	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
sídlem	sídlo	k1gNnSc7
21	#num#	k4
<g/>
.	.	kIx.
základny	základna	k1gFnSc2
taktického	taktický	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
„	„	k?
<g/>
Zvolenské	zvolenský	k2eAgFnSc2d1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letiště	letiště	k1gNnSc1
bylo	být	k5eAaImAgNnS
vybudováno	vybudovat	k5eAaPmNgNnS
v	v	k7c6
letech	léto	k1gNnPc6
1952	#num#	k4
až	až	k9
1958	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
významu	význam	k1gInSc3
této	tento	k3xDgFnSc2
jediné	jediný	k2eAgFnSc2d1
základny	základna	k1gFnSc2
bojových	bojový	k2eAgInPc2d1
letounů	letoun	k1gInPc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
,	,	kIx,
zabezpečující	zabezpečující	k2eAgFnSc6d1
ochranu	ochrana	k1gFnSc4
vzdušného	vzdušný	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
v	v	k7c6
rámci	rámec	k1gInSc6
hotovostního	hotovostní	k2eAgInSc2d1
systému	systém	k1gInSc2
NATINAMDS	NATINAMDS	kA
<g/>
,	,	kIx,
prošlo	projít	k5eAaPmAgNnS
letiště	letiště	k1gNnSc1
rozsáhlou	rozsáhlý	k2eAgFnSc7d1
modernizací	modernizace	k1gFnSc7
v	v	k7c6
souladu	soulad	k1gInSc6
se	s	k7c7
standardy	standard	k1gInPc7
Severoatlantické	severoatlantický	k2eAgFnSc2d1
aliance	aliance	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Infrastruktura	infrastruktura	k1gFnSc1
základny	základna	k1gFnSc2
je	být	k5eAaImIp3nS
uzpůsobena	uzpůsobit	k5eAaPmNgFnS
pro	pro	k7c4
provoz	provoz	k1gInSc4
nadzvukových	nadzvukový	k2eAgInPc2d1
letounů	letoun	k1gInPc2
Gripen	Gripen	k2eAgMnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
včetně	včetně	k7c2
simulačních	simulační	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
a	a	k8xC
opravárenských	opravárenský	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
letišti	letiště	k1gNnSc6
Náměšť	Náměšť	k1gFnSc1
nad	nad	k7c7
Oslavou	oslava	k1gFnSc7
(	(	kIx(
<g/>
Sedlec	Sedlec	k1gInSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
22	#num#	k4
<g/>
.	.	kIx.
základna	základna	k1gFnSc1
vrtulníkového	vrtulníkový	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
„	„	k?
<g/>
Biskajská	biskajský	k2eAgFnSc1d1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letiště	letiště	k1gNnSc1
vzniklo	vzniknout	k5eAaPmAgNnS
v	v	k7c6
letech	léto	k1gNnPc6
1956	#num#	k4
až	až	k9
1960	#num#	k4
jako	jako	k8xS,k8xC
záložní	záložní	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
a	a	k8xC
následně	následně	k6eAd1
z	z	k7c2
něj	on	k3xPp3gInSc2
působily	působit	k5eAaImAgFnP
jednotky	jednotka	k1gFnPc1
bitevního	bitevní	k2eAgNnSc2d1
a	a	k8xC
bombardovacího	bombardovací	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
Československé	československý	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vstupu	vstup	k1gInSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
do	do	k7c2
NATO	NATO	kA
byla	být	k5eAaImAgFnS
zastaralá	zastaralý	k2eAgFnSc1d1
infrastruktura	infrastruktura	k1gFnSc1
tehdejší	tehdejší	k2eAgFnSc1d1
22	#num#	k4
<g/>
.	.	kIx.
základny	základna	k1gFnSc2
letectva	letectvo	k1gNnSc2
modernizována	modernizován	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
zde	zde	k6eAd1
působila	působit	k5eAaImAgFnS
vrtulníková	vrtulníkový	k2eAgFnSc1d1
letka	letka	k1gFnSc1
se	s	k7c7
stroji	stroj	k1gInPc7
Mil	míle	k1gFnPc2
Mi-	Mi-	k1gFnSc4
<g/>
24	#num#	k4
a	a	k8xC
výcviková	výcvikový	k2eAgFnSc1d1
letka	letka	k1gFnSc1
s	s	k7c7
letouny	letoun	k1gInPc7
Aero	aero	k1gNnSc4
L-39	L-39	k1gMnSc1
Albatros	albatros	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počínaje	počínaje	k7c7
prosincem	prosinec	k1gInSc7
2013	#num#	k4
provozuje	provozovat	k5eAaImIp3nS
základna	základna	k1gFnSc1
pouze	pouze	k6eAd1
vrtulníky	vrtulník	k1gInPc7
<g/>
,	,	kIx,
neboť	neboť	k8xC
Albatrosy	albatros	k1gMnPc7
byly	být	k5eAaImAgFnP
přesunuty	přesunut	k2eAgFnPc4d1
do	do	k7c2
Čáslavi	Čáslav	k1gFnSc2
a	a	k8xC
v	v	k7c6
Náměšti	Náměšť	k1gFnSc6
je	on	k3xPp3gInPc4
vystřídaly	vystřídat	k5eAaPmAgInP
vrtulníky	vrtulník	k1gInPc4
Mi-	Mi-	k1gFnSc2
<g/>
171	#num#	k4
<g/>
Š	Š	kA
z	z	k7c2
rušené	rušený	k2eAgFnSc2d1
základny	základna	k1gFnSc2
v	v	k7c6
Přerově	Přerov	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Letiště	letiště	k1gNnSc1
Pardubice	Pardubice	k1gInPc1
</s>
<s>
Letiště	letiště	k1gNnSc1
Pardubice	Pardubice	k1gInPc1
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
Centrum	centrum	k1gNnSc4
leteckého	letecký	k2eAgInSc2d1
výcviku	výcvik	k1gInSc2
státního	státní	k2eAgInSc2d1
podniku	podnik	k1gInSc2
LOM	lom	k1gInSc1
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
od	od	k7c2
dubna	duben	k1gInSc2
2004	#num#	k4
provádí	provádět	k5eAaImIp3nS
kompletní	kompletní	k2eAgInSc4d1
výcvik	výcvik	k1gInSc4
pilotů	pilot	k1gMnPc2
Vzdušných	vzdušný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
Armády	armáda	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dráhový	dráhový	k2eAgInSc1d1
systém	systém	k1gInSc1
základny	základna	k1gFnSc2
se	se	k3xPyFc4
vyznačuje	vyznačovat	k5eAaImIp3nS
značnou	značný	k2eAgFnSc7d1
únosností	únosnost	k1gFnSc7
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
umožňuje	umožňovat	k5eAaImIp3nS
přistání	přistání	k1gNnSc4
velkokapacitních	velkokapacitní	k2eAgInPc2d1
transportních	transportní	k2eAgInPc2d1
letounů	letoun	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letiště	letiště	k1gNnSc1
rovněž	rovněž	k6eAd1
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
záložní	záložní	k2eAgInSc1d1
v	v	k7c6
případě	případ	k1gInSc6
omezení	omezení	k1gNnSc4
provozu	provoz	k1gInSc2
v	v	k7c6
Čáslavi	Čáslav	k1gFnSc6
nebo	nebo	k8xC
Náměšti	Náměšť	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zabezpečení	zabezpečení	k1gNnSc3
letového	letový	k2eAgInSc2d1
provozu	provoz	k1gInSc2
zajišťuje	zajišťovat	k5eAaImIp3nS
Správa	správa	k1gFnSc1
letiště	letiště	k1gNnSc4
Pardubice	Pardubice	k1gInPc4
a	a	k8xC
v	v	k7c6
případě	případ	k1gInSc6
potřeby	potřeba	k1gFnSc2
i	i	k9
pardubický	pardubický	k2eAgInSc1d1
14	#num#	k4
<g/>
.	.	kIx.
pluk	pluk	k1gInSc1
logistické	logistický	k2eAgFnSc2d1
podpory	podpora	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Z	z	k7c2
letiště	letiště	k1gNnSc2
Praha-Kbely	Praha-Kbela	k1gFnSc2
operují	operovat	k5eAaImIp3nP
stroje	stroj	k1gInPc1
24	#num#	k4
<g/>
.	.	kIx.
základny	základna	k1gFnSc2
dopravního	dopravní	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
„	„	k?
<g/>
T.	T.	kA
G.	G.	kA
Masaryka	Masaryk	k1gMnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historické	historický	k2eAgInPc1d1
letiště	letiště	k1gNnPc4
bylo	být	k5eAaImAgNnS
ve	v	k7c6
Kbelích	Kbely	k1gInPc6
vybudováno	vybudovat	k5eAaPmNgNnS
již	již	k9
v	v	k7c6
roce	rok	k1gInSc6
1918	#num#	k4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1945	#num#	k4
zde	zde	k6eAd1
působí	působit	k5eAaImIp3nS
především	především	k9
dopravní	dopravní	k2eAgInPc4d1
letouny	letoun	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
hlavní	hlavní	k2eAgInPc4d1
úkoly	úkol	k1gInPc4
základny	základna	k1gFnSc2
patří	patřit	k5eAaImIp3nP
transport	transport	k1gInSc4
vojenských	vojenský	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
a	a	k8xC
materiálu	materiál	k1gInSc2
<g/>
,	,	kIx,
přeprava	přeprava	k1gFnSc1
ústavních	ústavní	k2eAgMnPc2d1
činitelů	činitel	k1gMnPc2
a	a	k8xC
působení	působení	k1gNnSc2
ve	v	k7c4
prospěch	prospěch	k1gInSc4
IZS	IZS	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Základna	základna	k1gFnSc1
sice	sice	k8xC
neprošla	projít	k5eNaPmAgFnS
zásadní	zásadní	k2eAgFnSc7d1
modernizací	modernizace	k1gFnSc7
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
vzhledem	vzhledem	k7c3
k	k	k7c3
její	její	k3xOp3gFnSc3
strategické	strategický	k2eAgFnSc3d1
poloze	poloha	k1gFnSc3
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
s	s	k7c7
ní	on	k3xPp3gFnSc2
armáda	armáda	k1gFnSc1
počítá	počítat	k5eAaImIp3nS
i	i	k9
do	do	k7c2
budoucna	budoucno	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Záložní	záložní	k2eAgNnSc1d1
letiště	letiště	k1gNnSc1
</s>
<s>
Kromě	kromě	k7c2
čtyř	čtyři	k4xCgNnPc2
aktivních	aktivní	k2eAgNnPc2d1
vojenských	vojenský	k2eAgNnPc2d1
letišť	letiště	k1gNnPc2
v	v	k7c6
Čáslavi	Čáslav	k1gFnSc6
<g/>
,	,	kIx,
Náměšti	Náměšť	k1gFnSc6
nad	nad	k7c7
Oslavou	oslava	k1gFnSc7
<g/>
,	,	kIx,
v	v	k7c6
Praze	Praha	k1gFnSc6
Kbelích	Kbely	k1gInPc6
a	a	k8xC
v	v	k7c6
Pardubicích	Pardubice	k1gInPc6
armáda	armáda	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
evidovala	evidovat	k5eAaImAgFnS
dalších	další	k2eAgFnPc2d1
pět	pět	k4xCc1
bývalých	bývalý	k2eAgFnPc2d1
vojenských	vojenský	k2eAgFnPc2d1
základen	základna	k1gFnPc2
jako	jako	k8xS,k8xC
záložní	záložní	k2eAgNnSc4d1
letiště	letiště	k1gNnSc4
–	–	k?
Bechyně	Bechyně	k1gFnSc1
<g/>
,	,	kIx,
Chrudim	Chrudim	k1gFnSc1
<g/>
,	,	kIx,
Líně	líně	k6eAd1
<g/>
,	,	kIx,
Prostějov	Prostějov	k1gInSc1
a	a	k8xC
Přerov	Přerov	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
říjnu	říjen	k1gInSc6
2017	#num#	k4
informovali	informovat	k5eAaBmAgMnP
armádní	armádní	k2eAgMnSc1d1
generál	generál	k1gMnSc1
Josef	Josef	k1gMnSc1
Bečvář	Bečvář	k1gMnSc1
a	a	k8xC
ministr	ministr	k1gMnSc1
obrany	obrana	k1gFnSc2
Martin	Martin	k1gMnSc1
Stropnický	stropnický	k2eAgMnSc1d1
o	o	k7c6
návratu	návrat	k1gInSc6
armády	armáda	k1gFnSc2
na	na	k7c6
letiště	letiště	k1gNnSc2
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
<g/>
,	,	kIx,
se	s	k7c7
kterým	který	k3yIgInSc7,k3yRgInSc7,k3yQgInSc7
AČR	AČR	kA
počítá	počítat	k5eAaImIp3nS
jako	jako	k9
s	s	k7c7
jedním	jeden	k4xCgMnSc7
ze	z	k7c2
záložních	záložní	k2eAgNnPc2d1
letišť	letiště	k1gNnPc2
s	s	k7c7
detašovaným	detašovaný	k2eAgNnSc7d1
pracovištěm	pracoviště	k1gNnSc7
vzdušných	vzdušný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stanoviště	stanoviště	k1gNnSc1
letecké	letecký	k2eAgFnSc2d1
záchranné	záchranný	k2eAgFnSc2d1
služby	služba	k1gFnSc2
</s>
<s>
Část	část	k1gFnSc1
letiště	letiště	k1gNnSc2
Plzeň-Líně	Plzeň-Lín	k1gInSc6
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
243	#num#	k4
<g/>
.	.	kIx.
vrtulníková	vrtulníkový	k2eAgFnSc1d1
letka	letka	k1gFnSc1
pro	pro	k7c4
stanoviště	stanoviště	k1gNnPc4
letecké	letecký	k2eAgFnSc2d1
záchranné	záchranný	k2eAgFnSc2d1
služby	služba	k1gFnSc2
(	(	kIx(
<g/>
LZS	LZS	kA
<g/>
)	)	kIx)
s	s	k7c7
vrtulníkem	vrtulník	k1gInSc7
W-3A	W-3A	k1gFnSc2
–	–	k?
tzv.	tzv.	kA
Kryštof	Kryštof	k1gMnSc1
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Útvar	útvar	k1gInSc1
zajišťuje	zajišťovat	k5eAaImIp3nS
LZS	LZS	kA
ve	v	k7c6
dne	den	k1gInSc2
pro	pro	k7c4
Plzeňský	plzeňský	k2eAgInSc4d1
a	a	k8xC
Karlovarský	karlovarský	k2eAgInSc4d1
kraj	kraj	k1gInSc4
a	a	k8xC
v	v	k7c6
noci	noc	k1gFnSc6
pro	pro	k7c4
celé	celý	k2eAgNnSc4d1
území	území	k1gNnSc4
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
základě	základ	k1gInSc6
rozhodnutí	rozhodnutí	k1gNnSc2
vlády	vláda	k1gFnSc2
zabezpečují	zabezpečovat	k5eAaImIp3nP
vzdušné	vzdušný	k2eAgFnPc1d1
síly	síla	k1gFnPc1
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2017	#num#	k4
LZS	LZS	kA
i	i	k8xC
v	v	k7c6
Jihočeském	jihočeský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
namísto	namísto	k7c2
dosavadního	dosavadní	k2eAgNnSc2d1
stanoviště	stanoviště	k1gNnSc2
vrtulníku	vrtulník	k1gInSc2
Kryštof	Kryštof	k1gMnSc1
13	#num#	k4
na	na	k7c6
heliportu	heliport	k1gInSc6
letiště	letiště	k1gNnSc2
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
armáda	armáda	k1gFnSc1
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
letiště	letiště	k1gNnSc4
Bechyně	Bechyně	k1gFnSc2
v	v	k7c6
areálu	areál	k1gInSc6
posádky	posádka	k1gFnSc2
15	#num#	k4
<g/>
.	.	kIx.
ženijního	ženijní	k2eAgInSc2d1
pluku	pluk	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
1.1	1.1	k4
<g/>
.2021	.2021	k4
převezme	převzít	k5eAaPmIp3nS
Kryštof	Kryštof	k1gMnSc1
13	#num#	k4
soukromá	soukromý	k2eAgFnSc1d1
služba	služba	k1gFnSc1
DSA	DSA	kA
z	z	k7c2
Letiště	letiště	k1gNnSc2
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Využívat	využívat	k5eAaPmF,k5eAaImF
bude	být	k5eAaImBp3nS
vrtulník	vrtulník	k1gInSc1
Eurocopter	Eurocopter	k1gInSc4
EC	EC	kA
135	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Letecká	letecký	k2eAgFnSc1d1
technika	technika	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článcích	článek	k1gInPc6
Historie	historie	k1gFnSc2
letecké	letecký	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
AČR	AČR	kA
a	a	k8xC
Seznam	seznam	k1gInSc4
vojenských	vojenský	k2eAgNnPc2d1
letadel	letadlo	k1gNnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Stíhací	stíhací	k2eAgInPc1d1
letouny	letoun	k1gInPc1
</s>
<s>
Jediným	jediný	k2eAgInSc7d1
typem	typ	k1gInSc7
nadzvukového	nadzvukový	k2eAgInSc2d1
stíhacího	stíhací	k2eAgInSc2d1
letounu	letoun	k1gInSc2
ve	v	k7c6
výzbroji	výzbroj	k1gFnSc6
českého	český	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
Saab	Saab	k1gInSc1
JAS-39	JAS-39	k1gFnSc2
Gripen	Gripen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
výzbroji	výzbroj	k1gInSc6
211	#num#	k4
<g/>
.	.	kIx.
taktické	taktický	k2eAgFnSc2d1
letky	letka	k1gFnSc2
21	#num#	k4
<g/>
.	.	kIx.
základny	základna	k1gFnSc2
taktického	taktický	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
v	v	k7c6
Čáslavi	Čáslav	k1gFnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
14	#num#	k4
Gripenů	Gripen	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
12	#num#	k4
jednomístných	jednomístný	k2eAgFnPc2d1
bojových	bojový	k2eAgFnPc2d1
JAS-39C	JAS-39C	k1gFnPc2
a	a	k8xC
2	#num#	k4
dvoumístné	dvoumístný	k2eAgFnSc2d1
cvičně-bojové	cvičně-bojový	k2eAgFnSc2d1
JAS-	JAS-	k1gFnSc2
<g/>
39	#num#	k4
<g/>
D.	D.	kA
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
má	mít	k5eAaImIp3nS
tyto	tento	k3xDgFnPc4
stíhačky	stíhačka	k1gFnPc4
v	v	k7c6
pronájmu	pronájem	k1gInSc6
od	od	k7c2
Švédska	Švédsko	k1gNnSc2
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
2027	#num#	k4
a	a	k8xC
dle	dle	k7c2
smlouvy	smlouva	k1gFnSc2
je	on	k3xPp3gFnPc4
může	moct	k5eAaImIp3nS
používat	používat	k5eAaImF
k	k	k7c3
mírovým	mírový	k2eAgInPc3d1
i	i	k8xC
bojovým	bojový	k2eAgInPc3d1
účelům	účel	k1gInPc3
<g/>
,	,	kIx,
včetně	včetně	k7c2
plnění	plnění	k1gNnSc2
závazků	závazek	k1gInPc2
v	v	k7c6
rámci	rámec	k1gInSc6
NATO	NATO	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
Stroje	stroj	k1gInPc1
byly	být	k5eAaImAgInP
původně	původně	k6eAd1
pořízeny	pořídit	k5eAaPmNgInP
pouze	pouze	k6eAd1
v	v	k7c6
konfiguraci	konfigurace	k1gFnSc6
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
vzdušného	vzdušný	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
Air	Air	k1gFnSc1
Policing	Policing	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Operační	operační	k2eAgFnPc4d1
schopnosti	schopnost	k1gFnPc4
českých	český	k2eAgMnPc2d1
Gripenů	Gripen	k1gMnPc2
však	však	k9
byly	být	k5eAaImAgInP
postupně	postupně	k6eAd1
navyšovány	navyšovat	k5eAaImNgInP
a	a	k8xC
v	v	k7c6
březnu	březen	k1gInSc6
2018	#num#	k4
byla	být	k5eAaImAgFnS
dokončena	dokončit	k5eAaPmNgFnS
jejich	jejich	k3xOp3gFnSc1
modernizace	modernizace	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
zahrnovala	zahrnovat	k5eAaImAgFnS
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
integraci	integrace	k1gFnSc3
laserem	laser	k1gInSc7
naváděných	naváděný	k2eAgFnPc2d1
pum	puma	k1gFnPc2
a	a	k8xC
zaměřovacích	zaměřovací	k2eAgInPc2d1
kontejnerů	kontejner	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
JAS-39C	JAS-39C	k4
Gripen	Gripen	k2eAgInSc1d1
</s>
<s>
JAS-39D	JAS-39D	k4
Gripen	Gripen	k2eAgInSc1d1
</s>
<s>
Lehké	Lehké	k2eAgInPc4d1
bojové	bojový	k2eAgInPc4d1
a	a	k8xC
cvičné	cvičný	k2eAgInPc4d1
letouny	letoun	k1gInPc4
</s>
<s>
Nejpočetnějšími	početní	k2eAgMnPc7d3
bojovými	bojový	k2eAgMnPc7d1
letouny	letoun	k1gMnPc7
českých	český	k2eAgFnPc2d1
vzdušných	vzdušný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
jsou	být	k5eAaImIp3nP
lehké	lehký	k2eAgInPc1d1
bitevní	bitevní	k2eAgInPc1d1
letouny	letoun	k1gInPc1
Aero	aero	k1gNnSc4
L-159	L-159	k1gFnSc2
ALCA	ALCA	kA
<g/>
,	,	kIx,
zavedené	zavedený	k2eAgFnPc4d1
do	do	k7c2
výzbroje	výzbroj	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
a	a	k8xC
dodané	dodaná	k1gFnPc1
v	v	k7c6
počtu	počet	k1gInSc6
72	#num#	k4
kusů	kus	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
byla	být	k5eAaImAgFnS
většina	většina	k1gFnSc1
odprodána	odprodat	k5eAaPmNgFnS
nebo	nebo	k8xC
přestavěna	přestavět	k5eAaPmNgFnS
na	na	k7c4
dvoumístné	dvoumístný	k2eAgInPc4d1
stroje	stroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čáslavská	Čáslavská	k1gFnSc1
212	#num#	k4
<g/>
.	.	kIx.
taktická	taktický	k2eAgFnSc1d1
letka	letka	k1gFnSc1
provozuje	provozovat	k5eAaImIp3nS
14	#num#	k4
jednomístných	jednomístný	k2eAgInPc2d1
letounů	letoun	k1gInPc2
verze	verze	k1gFnSc1
L-159A	L-159A	k1gFnSc1
(	(	kIx(
<g/>
další	další	k2eAgMnPc1d1
2	#num#	k4
jsou	být	k5eAaImIp3nP
zařazeny	zařadit	k5eAaPmNgFnP
u	u	k7c2
výcvikové	výcvikový	k2eAgFnSc2d1
letky	letka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
prošly	projít	k5eAaPmAgFnP
dílčí	dílčí	k2eAgInPc4d1
modernizací	modernizace	k1gFnSc7
spočívající	spočívající	k2eAgFnSc7d1
především	především	k6eAd1
ve	v	k7c6
vylepšení	vylepšení	k1gNnSc6
přístrojového	přístrojový	k2eAgNnSc2d1
vybavení	vybavení	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
K	k	k7c3
pokračovacímu	pokračovací	k2eAgMnSc3d1
a	a	k8xC
bojovému	bojový	k2eAgInSc3d1
výcviku	výcvik	k1gInSc3
pilotů	pilot	k1gMnPc2
taktického	taktický	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
slouží	sloužit	k5eAaImIp3nP
dvoumístné	dvoumístný	k2eAgFnPc1d1
cvičné	cvičný	k2eAgFnPc1d1
verze	verze	k1gFnPc1
letounů	letoun	k1gInPc2
ALCA	ALCA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
stavu	stav	k1gInSc6
213	#num#	k4
<g/>
.	.	kIx.
výcvikové	výcvikový	k2eAgFnSc2d1
letky	letka	k1gFnSc2
na	na	k7c6
čáslavské	čáslavský	k2eAgFnSc6d1
základně	základna	k1gFnSc6
se	se	k3xPyFc4
kromě	kromě	k7c2
2	#num#	k4
strojů	stroj	k1gInPc2
L-159A	L-159A	k1gFnPc2
nachází	nacházet	k5eAaImIp3nS
3	#num#	k4
letouny	letoun	k1gMnPc7
L-159T1	L-159T1	k1gFnSc2
(	(	kIx(
<g/>
vznikly	vzniknout	k5eAaPmAgFnP
přestavbou	přestavba	k1gFnSc7
nadpočetných	nadpočetný	k2eAgFnPc2d1
L-	L-	k1gFnPc2
<g/>
159	#num#	k4
<g/>
A	A	kA
<g/>
;	;	kIx,
průběžně	průběžně	k6eAd1
modernizovány	modernizován	k2eAgInPc1d1
na	na	k7c4
verzi	verze	k1gFnSc4
L-	L-	k1gFnSc2
<g/>
159	#num#	k4
<g/>
T	T	kA
<g/>
1	#num#	k4
<g/>
+	+	kIx~
<g/>
)	)	kIx)
a	a	k8xC
3	#num#	k4
letouny	letoun	k1gInPc7
L-159T2	L-159T2	k1gMnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
L-159T1	L-159T1	k1gFnSc2
si	se	k3xPyFc3
zachovávají	zachovávat	k5eAaImIp3nP
schopnosti	schopnost	k1gFnPc4
lehkého	lehký	k2eAgInSc2d1
bojového	bojový	k2eAgInSc2d1
letounu	letoun	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Letouny	letoun	k1gInPc1
L-159T2	L-159T2	k1gFnSc2
byly	být	k5eAaImAgInP
oficiálně	oficiálně	k6eAd1
předány	předán	k2eAgInPc4d1
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2019	#num#	k4
jako	jako	k8xC,k8xS
náhrada	náhrada	k1gFnSc1
posledních	poslední	k2eAgInPc2d1
3	#num#	k4
provozuschopných	provozuschopný	k2eAgInPc2d1
Aero	aero	k1gNnSc4
L-39ZA	L-39ZA	k1gFnPc2
Albatros	albatros	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
Poslední	poslední	k2eAgInSc4d1
let	let	k1gInSc4
L-39ZA	L-39ZA	k1gFnPc2
u	u	k7c2
213	#num#	k4
<g/>
.	.	kIx.
výcvikové	výcvikový	k2eAgFnSc2d1
letky	letka	k1gFnSc2
se	se	k3xPyFc4
uskutečnil	uskutečnit	k5eAaPmAgInS
19	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
a	a	k8xC
poté	poté	k6eAd1
byly	být	k5eAaImAgFnP
Albatrosy	albatros	k1gMnPc4
definitivně	definitivně	k6eAd1
odstaveny	odstaven	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Základní	základní	k2eAgInSc1d1
letecký	letecký	k2eAgInSc1d1
výcvik	výcvik	k1gInSc1
vojenských	vojenský	k2eAgMnPc2d1
pilotů	pilot	k1gMnPc2
zajišťuje	zajišťovat	k5eAaImIp3nS
vlastními	vlastní	k2eAgInPc7d1
prostředky	prostředek	k1gInPc7
a	a	k8xC
leteckou	letecký	k2eAgFnSc7d1
technikou	technika	k1gFnSc7
Centrum	centrum	k1gNnSc1
leteckého	letecký	k2eAgInSc2d1
výcviku	výcvik	k1gInSc2
státního	státní	k2eAgInSc2d1
podniku	podnik	k1gInSc2
LOM	lom	k1gInSc1
Praha	Praha	k1gFnSc1
na	na	k7c6
letišti	letiště	k1gNnSc6
Pardubice	Pardubice	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
L-159A	L-159A	k4
ALCA	ALCA	kA
</s>
<s>
L-159T1	L-159T1	k4
</s>
<s>
Dopravní	dopravní	k2eAgInPc1d1
a	a	k8xC
transportní	transportní	k2eAgInPc1d1
letouny	letoun	k1gInPc1
</s>
<s>
Na	na	k7c4
kbelské	kbelský	k2eAgFnPc4d1
24	#num#	k4
<g/>
.	.	kIx.
základně	základně	k6eAd1
dopravního	dopravní	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
slouží	sloužit	k5eAaImIp3nS
v	v	k7c6
řadách	řada	k1gFnPc6
241	#num#	k4
<g/>
.	.	kIx.
dopravní	dopravní	k2eAgFnSc2d1
letky	letka	k1gFnSc2
2	#num#	k4
dopravní	dopravní	k2eAgInPc4d1
letouny	letoun	k1gInPc4
Airbus	airbus	k1gInSc1
A319CJ	A319CJ	k1gMnPc2
a	a	k8xC
1	#num#	k4
dopravní	dopravní	k2eAgInSc4d1
letoun	letoun	k1gInSc4
Bombardier	Bombardier	k1gMnSc1
CL-601	CL-601	k1gMnSc1
Challenger	Challenger	k1gMnSc1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc7
hlavním	hlavní	k2eAgInSc7d1
úkolem	úkol	k1gInSc7
je	být	k5eAaImIp3nS
zabezpečení	zabezpečení	k1gNnSc1
letecké	letecký	k2eAgFnSc2d1
přepravy	přeprava	k1gFnSc2
příslušníků	příslušník	k1gMnPc2
AČR	AČR	kA
<g/>
,	,	kIx,
ústavních	ústavní	k2eAgInPc2d1
činitelů	činitel	k1gInPc2
<g/>
,	,	kIx,
zahraničních	zahraniční	k2eAgFnPc2d1
delegací	delegace	k1gFnPc2
nebo	nebo	k8xC
pacientů	pacient	k1gMnPc2
v	v	k7c6
režimu	režim	k1gInSc6
MEDEVAC	MEDEVAC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
dvěma	dva	k4xCgInPc7
dopravními	dopravní	k2eAgInPc7d1
letouny	letoun	k1gInPc7
Jakovlev	Jakovlev	k1gFnSc2
Jak-	Jak-	k1gFnSc2
<g/>
40	#num#	k4
se	se	k3xPyFc4
vzdušné	vzdušný	k2eAgFnPc1d1
síly	síla	k1gFnPc1
slavnostně	slavnostně	k6eAd1
rozloučily	rozloučit	k5eAaPmAgFnP
2	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2020	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podle	podle	k7c2
Koncepce	koncepce	k1gFnSc2
výstavby	výstavba	k1gFnSc2
AČR	AČR	kA
mají	mít	k5eAaImIp3nP
být	být	k5eAaImF
do	do	k7c2
roku	rok	k1gInSc2
2025	#num#	k4
Jakovlevy	Jakovleva	k1gFnSc2
nahrazeny	nahradit	k5eAaPmNgFnP
typem	typ	k1gInSc7
schopným	schopný	k2eAgMnPc3d1
rozšířit	rozšířit	k5eAaPmF
přepravní	přepravní	k2eAgFnPc4d1
kapacity	kapacita	k1gFnPc4
ve	v	k7c4
prospěch	prospěch	k1gInSc4
pozemních	pozemní	k2eAgFnPc2d1
sil	síla	k1gFnPc2
a	a	k8xC
budovaného	budovaný	k2eAgInSc2d1
výsadkového	výsadkový	k2eAgInSc2d1
pluku	pluk	k1gInSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
konkrétně	konkrétně	k6eAd1
dvěma	dva	k4xCgInPc7
stroji	stroj	k1gInPc7
Airbus	airbus	k1gInSc1
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
CASA	CASA	kA
<g/>
)	)	kIx)
C-	C-	k1gMnSc1
<g/>
295	#num#	k4
<g/>
MW	MW	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Společnost	společnost	k1gFnSc1
Airbus	airbus	k1gInSc1
Defence	Defence	k1gFnSc1
and	and	k?
Space	Space	k1gFnSc1
nabídla	nabídnout	k5eAaPmAgFnS
možnost	možnost	k1gFnSc4
nákupu	nákup	k1gInSc2
dvou	dva	k4xCgInPc2
nových	nový	k2eAgInPc2d1
skladových	skladový	k2eAgInPc2d1
letounů	letoun	k1gInPc2
C-295MW	C-295MW	k1gFnPc2
v	v	k7c6
letech	let	k1gInPc6
2020	#num#	k4
<g/>
–	–	k?
<g/>
2021	#num#	k4
za	za	k7c4
2,7	2,7	k4
mld.	mld.	k?
Kč	Kč	kA
s	s	k7c7
DPH	DPH	kA
<g/>
;	;	kIx,
smlouva	smlouva	k1gFnSc1
byla	být	k5eAaImAgFnS
podepsána	podepsán	k2eAgFnSc1d1
16	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
Ministerstvo	ministerstvo	k1gNnSc1
obrany	obrana	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
rovněž	rovněž	k9
plánovalo	plánovat	k5eAaImAgNnS
akvizici	akvizice	k1gFnSc4
letounu	letoun	k1gInSc2
kategorie	kategorie	k1gFnSc2
business	business	k1gInSc1
jet	jet	k2eAgInSc1d1
ve	v	k7c6
VIP	VIP	kA
konfiguraci	konfigurace	k1gFnSc4
pro	pro	k7c4
přepravu	přeprava	k1gFnSc4
ústavních	ústavní	k2eAgMnPc2d1
a	a	k8xC
vládních	vládní	k2eAgMnPc2d1
činitelů	činitel	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
tuto	tento	k3xDgFnSc4
smlouvu	smlouva	k1gFnSc4
se	se	k3xPyFc4
nepodařilo	podařit	k5eNaPmAgNnS
podepsat	podepsat	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
A319CJ	A319CJ	k4
</s>
<s>
CL-601	CL-601	k4
Challenger	Challenger	k1gInSc1
</s>
<s>
Transportní	transportní	k2eAgInPc1d1
letouny	letoun	k1gInPc1
českého	český	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
představují	představovat	k5eAaImIp3nP
4	#num#	k4
letouny	letoun	k1gInPc7
CASA	CASA	kA
C-295M	C-295M	k1gMnPc2
a	a	k8xC
4	#num#	k4
letouny	letoun	k1gMnPc7
L-410UVP-E	L-410UVP-E	k1gFnSc2
Turbolet	Turbolet	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
zajišťují	zajišťovat	k5eAaImIp3nP
přepravu	přeprava	k1gFnSc4
vojsk	vojsko	k1gNnPc2
a	a	k8xC
materiálu	materiál	k1gInSc2
<g/>
,	,	kIx,
vzdušné	vzdušný	k2eAgFnPc1d1
výsadky	výsadka	k1gFnPc1
nebo	nebo	k8xC
odsun	odsun	k1gInSc4
raněných	raněný	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
stavu	stav	k1gInSc6
242	#num#	k4
<g/>
.	.	kIx.
transportní	transportní	k2eAgFnSc2d1
a	a	k8xC
speciální	speciální	k2eAgFnSc2d1
letky	letka	k1gFnSc2
z	z	k7c2
letiště	letiště	k1gNnSc2
Praha-Kbely	Praha-Kbela	k1gFnSc2
je	být	k5eAaImIp3nS
doplňují	doplňovat	k5eAaImIp3nP
2	#num#	k4
speciální	speciální	k2eAgInSc4d1
(	(	kIx(
<g/>
fotogrammetrické	fotogrammetrický	k2eAgInPc4d1
<g/>
)	)	kIx)
letouny	letoun	k1gInPc4
L-410FG	L-410FG	k1gFnPc2
pro	pro	k7c4
letecké	letecký	k2eAgNnSc4d1
snímkování	snímkování	k1gNnSc4
<g/>
,	,	kIx,
vzdušný	vzdušný	k2eAgInSc4d1
průzkum	průzkum	k1gInSc4
a	a	k8xC
kalibrování	kalibrování	k1gNnSc4
radiotechnických	radiotechnický	k2eAgInPc2d1
systémů	systém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Dva	dva	k4xCgInPc1
Turbolety	Turbolet	k1gInPc1
transportní	transportní	k2eAgFnSc2d1
verze	verze	k1gFnSc2
UVP-T	UVP-T	k1gFnPc2
byly	být	k5eAaImAgFnP
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
odstaveny	odstaven	k2eAgInPc4d1
z	z	k7c2
provozu	provoz	k1gInSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
načež	načež	k6eAd1
jejich	jejich	k3xOp3gMnPc2
místo	místo	k6eAd1
zaujaly	zaujmout	k5eAaPmAgInP
dva	dva	k4xCgInPc1
letouny	letoun	k1gInPc1
verze	verze	k1gFnSc2
UVP-E	UVP-E	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
byly	být	k5eAaImAgFnP
za	za	k7c7
tímto	tento	k3xDgInSc7
účelem	účel	k1gInSc7
opatřeny	opatřen	k2eAgInPc1d1
širšími	široký	k2eAgInPc7d2
bočními	boční	k2eAgInPc7d1
vraty	vrat	k1gInPc7
<g/>
,	,	kIx,
podélnými	podélný	k2eAgFnPc7d1
lavicemi	lavice	k1gFnPc7
a	a	k8xC
systémem	systém	k1gInSc7
pro	pro	k7c4
seskoky	seskok	k1gInPc4
výsadkářů	výsadkář	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbylé	zbylý	k2eAgInPc1d1
dva	dva	k4xCgInPc1
stroje	stroj	k1gInPc1
UVP-E	UVP-E	k1gFnPc2
létají	létat	k5eAaImIp3nP
v	v	k7c6
konfiguraci	konfigurace	k1gFnSc6
pro	pro	k7c4
přepravu	přeprava	k1gFnSc4
VIP	VIP	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Letouny	letoun	k1gInPc1
C-295M	C-295M	k1gFnSc2
podstoupí	podstoupit	k5eAaPmIp3nP
v	v	k7c6
letech	léto	k1gNnPc6
2019	#num#	k4
až	až	k9
2021	#num#	k4
modernizační	modernizační	k2eAgInSc4d1
program	program	k1gInSc4
na	na	k7c4
standard	standard	k1gInSc4
shodný	shodný	k2eAgInSc4d1
s	s	k7c7
novými	nový	k2eAgMnPc7d1
C-	C-	k1gMnPc7
<g/>
295	#num#	k4
<g/>
MW	MW	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
C-295M	C-295M	k4
</s>
<s>
L-410UVP-E	L-410UVP-E	k4
</s>
<s>
L-410FG	L-410FG	k4
</s>
<s>
Vrtulníky	vrtulník	k1gInPc1
</s>
<s>
Vrtulníky	vrtulník	k1gInPc1
pro	pro	k7c4
bojovou	bojový	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
pozemních	pozemní	k2eAgFnPc2d1
sil	síla	k1gFnPc2
operují	operovat	k5eAaImIp3nP
z	z	k7c2
22	#num#	k4
<g/>
.	.	kIx.
základny	základna	k1gFnSc2
vrtulníkového	vrtulníkový	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gNnSc7
sídlem	sídlo	k1gNnSc7
je	být	k5eAaImIp3nS
Sedlec	Sedlec	k1gInSc1
<g/>
,	,	kIx,
Vícenice	Vícenice	k1gFnSc1
u	u	k7c2
Náměště	Náměšť	k1gFnSc2
nad	nad	k7c7
Oslavou	oslava	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc1
vrtulníky	vrtulník	k1gInPc1
provozované	provozovaný	k2eAgInPc1d1
22	#num#	k4
<g/>
.	.	kIx.
zVrL	zVrL	k?
byly	být	k5eAaImAgInP
dodány	dodat	k5eAaPmNgInP
v	v	k7c6
rámci	rámec	k1gInSc6
deblokace	deblokace	k1gFnSc2
ruského	ruský	k2eAgInSc2d1
dluhu	dluh	k1gInSc2
v	v	k7c6
letech	léto	k1gNnPc6
2003	#num#	k4
až	až	k9
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smíšená	smíšený	k2eAgFnSc1d1
výzbroj	výzbroj	k1gFnSc1
221	#num#	k4
<g/>
.	.	kIx.
vrtulníkové	vrtulníkový	k2eAgFnSc2d1
letky	letka	k1gFnSc2
zahrnuje	zahrnovat	k5eAaImIp3nS
17	#num#	k4
bitevních	bitevní	k2eAgInPc2d1
vrtulníků	vrtulník	k1gInPc2
Mil	míle	k1gFnPc2
Mi-	Mi-	k1gFnSc4
<g/>
24	#num#	k4
<g/>
V	V	kA
(	(	kIx(
<g/>
Mi-	Mi-	k1gFnSc1
<g/>
35	#num#	k4
dle	dle	k7c2
ruského	ruský	k2eAgNnSc2d1
exportního	exportní	k2eAgNnSc2d1
označení	označení	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
je	být	k5eAaImIp3nS
aktivně	aktivně	k6eAd1
používáno	používat	k5eAaImNgNnS
jen	jen	k9
10	#num#	k4
strojů	stroj	k1gInPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
4	#num#	k4
transportní	transportní	k2eAgInPc1d1
vrtulníky	vrtulník	k1gInPc1
Mil	míle	k1gFnPc2
Mi-	Mi-	k1gFnSc4
<g/>
171	#num#	k4
<g/>
Š	Š	kA
ve	v	k7c6
verzi	verze	k1gFnSc6
s	s	k7c7
dvoudílnými	dvoudílný	k2eAgInPc7d1
vraty	vrat	k1gInPc7
nákladní	nákladní	k2eAgFnSc2d1
kabiny	kabina	k1gFnSc2
<g/>
.	.	kIx.
222	#num#	k4
<g/>
.	.	kIx.
vrtulníková	vrtulníkový	k2eAgFnSc1d1
letka	letka	k1gFnSc1
disponuje	disponovat	k5eAaBmIp3nS
11	#num#	k4
Mi-	Mi-	k1gFnPc2
<g/>
171	#num#	k4
<g/>
Š	Š	kA
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
4	#num#	k4
ve	v	k7c6
verzi	verze	k1gFnSc6
s	s	k7c7
vraty	vrata	k1gNnPc7
a	a	k8xC
7	#num#	k4
se	s	k7c7
sklopnou	sklopný	k2eAgFnSc7d1
rampou	rampa	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Rampové	rampový	k2eAgFnSc2d1
<g/>
“	“	k?
vrtulníky	vrtulník	k1gInPc1
byly	být	k5eAaImAgInP
komplexně	komplexně	k6eAd1
modernizovány	modernizován	k2eAgInPc1d1
(	(	kIx(
<g/>
na	na	k7c6
verzi	verze	k1gFnSc6
Mi-	Mi-	k1gMnSc2
<g/>
171	#num#	k4
<g/>
ŠM	ŠM	kA
<g/>
)	)	kIx)
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
nasazením	nasazení	k1gNnSc7
v	v	k7c6
Afghánistánu	Afghánistán	k1gInSc6
a	a	k8xC
působením	působení	k1gNnSc7
ve	v	k7c4
prospěch	prospěch	k1gInSc4
Leteckého	letecký	k2eAgInSc2d1
odřadu	odřad	k1gInSc2
speciálních	speciální	k2eAgFnPc2d1
operací	operace	k1gFnPc2
(	(	kIx(
<g/>
LOSO	LOSO	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
222	#num#	k4
<g/>
.	.	kIx.
vrtulníkové	vrtulníkový	k2eAgFnSc2d1
letky	letka	k1gFnSc2
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
Pro	pro	k7c4
splnění	splnění	k1gNnSc4
podmínek	podmínka	k1gFnPc2
Eurocontrol	Eurocontrol	k1gInSc4
čeká	čekat	k5eAaImIp3nS
po	po	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
upgrade	upgrade	k1gInSc4
avionických	avionický	k2eAgInPc2d1
a	a	k8xC
radionavigačních	radionavigační	k2eAgInPc2d1
systémů	systém	k1gInPc2
i	i	k8xC
vrtulníky	vrtulník	k1gInPc1
Mi-	Mi-	k1gFnSc2
<g/>
171	#num#	k4
<g/>
Š	Š	kA
ve	v	k7c6
verzi	verze	k1gFnSc6
s	s	k7c7
vraty	vrata	k1gNnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
Zatímco	zatímco	k8xS
s	s	k7c7
ukončením	ukončení	k1gNnSc7
provozu	provoz	k1gInSc2
strojů	stroj	k1gInPc2
Mi-	Mi-	k1gFnSc2
<g/>
171	#num#	k4
<g/>
Š	Š	kA
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
ŠM	ŠM	kA
armáda	armáda	k1gFnSc1
počítá	počítat	k5eAaImIp3nS
až	až	k9
v	v	k7c6
letech	let	k1gInPc6
2030	#num#	k4
<g/>
–	–	k?
<g/>
2035	#num#	k4
<g/>
,	,	kIx,
zastarávající	zastarávající	k2eAgInPc1d1
bitevní	bitevní	k2eAgInPc1d1
vrtulníky	vrtulník	k1gInPc1
Mi-	Mi-	k1gFnSc1
<g/>
24	#num#	k4
<g/>
V	V	kA
by	by	kYmCp3nS
měly	mít	k5eAaImAgFnP
být	být	k5eAaImF
kolem	kolem	k7c2
roku	rok	k1gInSc2
2023	#num#	k4
nahrazeny	nahradit	k5eAaPmNgFnP
kombinovanou	kombinovaný	k2eAgFnSc7d1
letkou	letka	k1gFnSc7
8	#num#	k4
víceúčelových	víceúčelový	k2eAgFnPc2d1
UH-1Y	UH-1Y	k1gFnPc2
Venom	Venom	k1gInSc1
a	a	k8xC
4	#num#	k4
bitevních	bitevní	k2eAgFnPc2d1
AH-1Z	AH-1Z	k1gFnPc2
Viper	Viper	k1gInSc4
americké	americký	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mi-	Mi-	k?
<g/>
24	#num#	k4
<g/>
V	v	k7c6
</s>
<s>
Mi-	Mi-	k?
<g/>
171	#num#	k4
<g/>
Š	Š	kA
(	(	kIx(
<g/>
s	s	k7c7
vraty	vrat	k1gInPc7
<g/>
)	)	kIx)
</s>
<s>
Mi-	Mi-	k?
<g/>
171	#num#	k4
<g/>
ŠM	ŠM	kA
(	(	kIx(
<g/>
s	s	k7c7
rampou	rampa	k1gFnSc7
<g/>
)	)	kIx)
</s>
<s>
Letecký	letecký	k2eAgInSc1d1
arzenál	arzenál	k1gInSc1
243	#num#	k4
<g/>
.	.	kIx.
vrtulníkové	vrtulníkový	k2eAgFnSc2d1
letky	letka	k1gFnSc2
24	#num#	k4
<g/>
.	.	kIx.
základny	základna	k1gFnSc2
dopravního	dopravní	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
představují	představovat	k5eAaImIp3nP
3	#num#	k4
„	„	k?
<g/>
salónní	salónní	k2eAgFnSc1d1
<g/>
“	“	k?
vrtulníky	vrtulník	k1gInPc4
Mil	míle	k1gFnPc2
Mi-	Mi-	k1gFnSc4
<g/>
8	#num#	k4
<g/>
S	s	k7c7
<g/>
,	,	kIx,
5	#num#	k4
transportních	transportní	k2eAgInPc2d1
vrtulníků	vrtulník	k1gInPc2
Mil	míle	k1gFnPc2
Mi-	Mi-	k1gFnSc4
<g/>
17	#num#	k4
a	a	k8xC
10	#num#	k4
víceúčelových	víceúčelový	k2eAgInPc2d1
vrtulníků	vrtulník	k1gInPc2
PZL	PZL	kA
W-3A	W-3A	k1gFnSc1
Sokol	Sokol	k1gMnSc1
(	(	kIx(
<g/>
4	#num#	k4
v	v	k7c4
transportní	transportní	k2eAgFnSc4d1
a	a	k8xC
6	#num#	k4
v	v	k7c6
záchranné	záchranný	k2eAgFnSc6d1
konfiguraci	konfigurace	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
přepravních	přepravní	k2eAgInPc2d1
letů	let	k1gInPc2
zajišťuje	zajišťovat	k5eAaImIp3nS
243	#num#	k4
<g/>
.	.	kIx.
vrtulníková	vrtulníkový	k2eAgFnSc1d1
letka	letka	k1gFnSc1
leteckou	letecký	k2eAgFnSc4d1
záchrannou	záchranný	k2eAgFnSc4d1
službu	služba	k1gFnSc4
s	s	k7c7
vrtulníkem	vrtulník	k1gInSc7
W-3A	W-3A	k1gFnSc2
na	na	k7c6
letišti	letiště	k1gNnSc6
Plzeň-Líně	Plzeň-Lína	k1gFnSc3
(	(	kIx(
<g/>
stanice	stanice	k1gFnSc1
Kryštof	Kryštof	k1gMnSc1
0	#num#	k4
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
na	na	k7c6
letišti	letiště	k1gNnSc6
Praha-Kbely	Praha-Kbela	k1gFnSc2
drží	držet	k5eAaImIp3nS
nepřetržitou	přetržitý	k2eNgFnSc4d1
službu	služba	k1gFnSc4
pátrání	pátrání	k1gNnSc2
a	a	k8xC
záchrany	záchrana	k1gFnSc2
(	(	kIx(
<g/>
SAR	SAR	kA
<g/>
)	)	kIx)
s	s	k7c7
vrtulníkem	vrtulník	k1gInSc7
Mi-	Mi-	k1gFnSc1
<g/>
17	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
Vrtulníky	vrtulník	k1gInPc1
letky	letka	k1gFnSc2
rovněž	rovněž	k9
zabezpečují	zabezpečovat	k5eAaImIp3nP
pohotovost	pohotovost	k1gFnSc4
k	k	k7c3
přepravě	přeprava	k1gFnSc3
pro	pro	k7c4
Institut	institut	k1gInSc4
klinické	klinický	k2eAgFnSc2d1
a	a	k8xC
experimentální	experimentální	k2eAgFnSc2d1
medicíny	medicína	k1gFnSc2
a	a	k8xC
pro	pro	k7c4
nasazení	nasazení	k1gNnSc4
příslušníků	příslušník	k1gMnPc2
Vojenské	vojenský	k2eAgFnSc2d1
policie	policie	k1gFnSc2
nebo	nebo	k8xC
Policie	policie	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
Všech	všecek	k3xTgInPc2
pět	pět	k4xCc4
vrtulníků	vrtulník	k1gInPc2
Mi-	Mi-	k1gFnSc2
<g/>
17	#num#	k4
243	#num#	k4
<g/>
.	.	kIx.
letky	letka	k1gFnSc2
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
podle	podle	k7c2
smlouvy	smlouva	k1gFnSc2
s	s	k7c7
LOM	lom	k1gInSc1
Praha	Praha	k1gFnSc1
do	do	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
2018	#num#	k4
zmodernizováno	zmodernizovat	k5eAaPmNgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mi-	Mi-	k?
<g/>
8	#num#	k4
<g/>
S	s	k7c7
</s>
<s>
Mi-	Mi-	k?
<g/>
17	#num#	k4
</s>
<s>
W-3A	W-3A	k4
Sokol	Sokol	k1gMnSc1
</s>
<s>
Pozemní	pozemní	k2eAgFnSc1d1
protivzdušná	protivzdušný	k2eAgFnSc1d1
obrana	obrana	k1gFnSc1
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Odpalovací	odpalovací	k2eAgNnSc1d1
zařízení	zařízení	k1gNnSc1
kompletu	komplet	k1gInSc2
Kub	Kuba	k1gFnPc2
na	na	k7c6
přehlídce	přehlídka	k1gFnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
9	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1985	#num#	k4
</s>
<s>
Po	po	k7c6
zániku	zánik	k1gInSc6
ČSFR	ČSFR	kA
se	se	k3xPyFc4
redukce	redukce	k1gFnSc1
početních	početní	k2eAgInPc2d1
stavů	stav	k1gInPc2
armády	armáda	k1gFnSc2
nevyhnula	vyhnout	k5eNaPmAgFnS
ani	ani	k8xC
pozemní	pozemní	k2eAgFnSc3d1
protivzdušné	protivzdušný	k2eAgFnSc3d1
obraně	obrana	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
postupnému	postupný	k2eAgNnSc3d1
snižování	snižování	k1gNnSc3
počtů	počet	k1gInPc2
systémů	systém	k1gInPc2
<g/>
,	,	kIx,
opouštění	opouštění	k1gNnSc1
posádek	posádka	k1gFnPc2
a	a	k8xC
rušení	rušení	k1gNnSc1
celých	celý	k2eAgInPc2d1
útvarů	útvar	k1gInPc2
a	a	k8xC
jejich	jejich	k3xOp3gFnSc2
raketové	raketový	k2eAgFnSc2d1
výzbroje	výzbroj	k1gFnSc2
sovětské	sovětský	k2eAgFnSc2d1
provenience	provenience	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
protiletadlové	protiletadlový	k2eAgInPc4d1
raketové	raketový	k2eAgInPc4d1
komplety	komplet	k1gInPc4
2K11	2K11	k4
Krug	Kruga	k1gFnPc2
(	(	kIx(
<g/>
vyřazeny	vyřazen	k2eAgInPc4d1
v	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
S-200	S-200	k1gMnSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
S-125	S-125	k1gMnSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
S-75	S-75	k1gMnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
9K33	9K33	k4
Osa	osa	k1gFnSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Armáda	armáda	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
vyřadila	vyřadit	k5eAaPmAgFnS
také	také	k9
všechny	všechen	k3xTgInPc4
protiletadlové	protiletadlový	k2eAgInPc4d1
kanóny	kanón	k1gInPc4
<g/>
:	:	kIx,
57	#num#	k4
<g/>
mm	mm	kA
S-60	S-60	k1gFnPc2
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
30	#num#	k4
<g/>
mm	mm	kA
„	„	k?
<g/>
ještěrky	ještěrka	k1gFnPc4
<g/>
“	“	k?
vz.	vz.	k?
53	#num#	k4
<g/>
/	/	kIx~
<g/>
59	#num#	k4
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
souvislosti	souvislost	k1gFnSc6
se	s	k7c7
vstupem	vstup	k1gInSc7
do	do	k7c2
NATO	NATO	kA
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
byla	být	k5eAaImAgFnS
opuštěna	opuštěn	k2eAgFnSc1d1
koncepce	koncepce	k1gFnSc1
vlastní	vlastní	k2eAgFnSc2d1
protivzdušné	protivzdušný	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
celého	celý	k2eAgNnSc2d1
území	území	k1gNnSc2
státu	stát	k1gInSc2
ve	v	k7c4
prospěch	prospěch	k1gInSc4
aliančního	alianční	k2eAgInSc2d1
systému	systém	k1gInSc2
NATINAMDS	NATINAMDS	kA
a	a	k8xC
evropského	evropský	k2eAgInSc2d1
projektu	projekt	k1gInSc2
jednotného	jednotný	k2eAgNnSc2d1
nebe	nebe	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
začátku	začátek	k1gInSc6
roku	rok	k1gInSc2
2000	#num#	k4
byla	být	k5eAaImAgFnS
vytvořena	vytvořen	k2eAgFnSc1d1
43	#num#	k4
<g/>
.	.	kIx.
protiletadlová	protiletadlový	k2eAgFnSc1d1
raketová	raketový	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
ve	v	k7c6
Strakonicích	Strakonice	k1gFnPc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
přejmenována	přejmenovat	k5eAaPmNgFnS
na	na	k7c4
25	#num#	k4
<g/>
.	.	kIx.
protiletadlovou	protiletadlový	k2eAgFnSc4d1
raketovou	raketový	k2eAgFnSc4d1
brigádu	brigáda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
jednotka	jednotka	k1gFnSc1
se	se	k3xPyFc4
v	v	k7c6
období	období	k1gNnSc6
let	léto	k1gNnPc2
2003	#num#	k4
až	až	k8xS
2006	#num#	k4
skládala	skládat	k5eAaImAgFnS
z	z	k7c2
251	#num#	k4
<g/>
.	.	kIx.
protiletadlové	protiletadlový	k2eAgFnSc2d1
raketové	raketový	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
vyzbrojené	vyzbrojený	k2eAgFnSc2d1
PLRK	PLRK	kA
2K12	2K12	k4
Kub	Kuba	k1gFnPc2
<g/>
,	,	kIx,
252	#num#	k4
<g/>
.	.	kIx.
protiletadlové	protiletadlový	k2eAgFnSc2d1
raketové	raketový	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
vyzbrojené	vyzbrojený	k2eAgFnSc2d1
9K33	9K33	k4
Osa	osa	k1gFnSc1
a	a	k8xC
9K35	9K35	k4
Strela-	Strela-	k1gFnSc1
<g/>
10	#num#	k4
a	a	k8xC
253	#num#	k4
<g/>
.	.	kIx.
praporu	prapor	k1gInSc2
zabezpečení	zabezpečení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komplet	komplet	k2eAgFnSc1d1
Osa	osa	k1gFnSc1
byl	být	k5eAaImAgInS
v	v	k7c6
červnu	červen	k1gInSc6
2006	#num#	k4
vyřazen	vyřadit	k5eAaPmNgInS
a	a	k8xC
nahrazen	nahradit	k5eAaPmNgInS
švédským	švédský	k2eAgInSc7d1
přenosným	přenosný	k2eAgInSc7d1
kompletem	komplet	k1gInSc7
RBS-	RBS-	k1gFnSc1
<g/>
70	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Modernizace	modernizace	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
disponoval	disponovat	k5eAaBmAgInS
25	#num#	k4
<g/>
.	.	kIx.
protiletadlový	protiletadlový	k2eAgInSc1d1
raketový	raketový	k2eAgInSc1d1
pluk	pluk	k1gInSc1
Vzdušných	vzdušný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
AČR	AČR	kA
protiletadlovými	protiletadlový	k2eAgInPc7d1
raketovými	raketový	k2eAgInPc7d1
komplety	komplet	k1gInPc7
2K12	2K12	k4
M2	M2	k1gFnSc4
KUB	Kuba	k1gFnPc2
(	(	kIx(
<g/>
v	v	k7c6
kódu	kód	k1gInSc6
NATO	NATO	kA
SA-6	SA-6	k1gFnPc2
Gainful	Gainfula	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
protiletadlovými	protiletadlový	k2eAgInPc7d1
raketovými	raketový	k2eAgInPc7d1
komplety	komplet	k1gInPc7
9K35M	9K35M	k4
Strela-	Strela-	k1gFnSc1
<g/>
10	#num#	k4
<g/>
M	M	kA
(	(	kIx(
<g/>
SA-	SA-	k1gFnPc2
<g/>
13	#num#	k4
Gopher	Gophra	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
přenosnými	přenosný	k2eAgInPc7d1
PLRK	PLRK	kA
RBS-70	RBS-70	k1gFnSc1
Saab	Saab	k1gInSc1
Bofors	Bofors	k1gInSc1
Dynamics	Dynamicsa	k1gFnPc2
a	a	k8xC
9K32M	9K32M	k4
Strela	Strela	k1gFnSc1
2	#num#	k4
<g/>
M.	M.	kA
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
Životnost	životnost	k1gFnSc1
raketového	raketový	k2eAgInSc2d1
kompletu	komplet	k1gInSc2
2K12	2K12	k4
KUB	Kuba	k1gFnPc2
končí	končit	k5eAaImIp3nS
letech	léto	k1gNnPc6
2021	#num#	k4
až	až	k9
2022	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
informací	informace	k1gFnPc2
ministerstva	ministerstvo	k1gNnSc2
obrany	obrana	k1gFnSc2
z	z	k7c2
listopadu	listopad	k1gInSc2
2016	#num#	k4
mělo	mít	k5eAaImAgNnS
proto	proto	k6eAd1
dojít	dojít	k5eAaPmF
k	k	k7c3
jejich	jejich	k3xOp3gFnSc3
náhradě	náhrada	k1gFnSc3
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
uváděnými	uváděný	k2eAgMnPc7d1
kandidáty	kandidát	k1gMnPc7
byly	být	k5eAaImAgInP
systémy	systém	k1gInPc1
Barak	Barak	k1gInSc1
8	#num#	k4
<g/>
,	,	kIx,
MICA	MICA	kA
VL	VL	kA
<g/>
,	,	kIx,
EMADS	EMADS	kA
<g/>
,	,	kIx,
MEADS	MEADS	kA
<g/>
,	,	kIx,
IRIS-T	IRIS-T	k1gFnSc1
SL	SL	kA
<g/>
,	,	kIx,
SPYDER	SPYDER	kA
a	a	k8xC
NASAMS	NASAMS	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
Plánováno	plánovat	k5eAaImNgNnS
je	být	k5eAaImIp3nS
rovněž	rovněž	k9
vyřazení	vyřazení	k1gNnSc1
kompletů	komplet	k1gInPc2
S-10M	S-10M	k1gFnSc2
a	a	k8xC
v	v	k7c6
letech	léto	k1gNnPc6
2018	#num#	k4
až	až	k9
2025	#num#	k4
jejich	jejich	k3xOp3gNnPc2
nahrazení	nahrazení	k1gNnSc2
typem	typ	k1gInSc7
RBS-70	RBS-70	k1gMnSc2
NG	NG	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
září	září	k1gNnSc6
2020	#num#	k4
začala	začít	k5eAaPmAgFnS
česká	český	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
vyjednávat	vyjednávat	k5eAaImF
s	s	k7c7
výrobcem	výrobce	k1gMnSc7
izraelského	izraelský	k2eAgInSc2d1
SPYDERu	SPYDERus	k1gInSc2
o	o	k7c6
zakoupení	zakoupení	k1gNnSc6
4	#num#	k4
baterií	baterie	k1gFnPc2
tohoto	tento	k3xDgInSc2
systému	systém	k1gInSc2
(	(	kIx(
<g/>
celkem	celkem	k6eAd1
16	#num#	k4
mobilních	mobilní	k2eAgNnPc2d1
odpalovacích	odpalovací	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
<g/>
,	,	kIx,
4	#num#	k4
radiolokátory	radiolokátor	k1gInPc1
a	a	k8xC
4	#num#	k4
velitelské	velitelský	k2eAgInPc4d1
moduly	modul	k1gInPc4
na	na	k7c6
podvozku	podvozek	k1gInSc6
Tatra	Tatra	k1gFnSc1
815-7	815-7	k4
8	#num#	k4
<g/>
x	x	k?
<g/>
8	#num#	k4
<g/>
)	)	kIx)
o	o	k7c6
celkové	celkový	k2eAgFnSc6d1
hodnotě	hodnota	k1gFnSc6
10	#num#	k4
miliard	miliarda	k4xCgFnPc2
Kč	Kč	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Radiotechnické	radiotechnický	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
</s>
<s>
Působnost	působnost	k1gFnSc1
</s>
<s>
3D	3D	k4
radiolokátor	radiolokátor	k1gInSc1
ST-68U	ST-68U	k1gFnSc2
CZ	CZ	kA
</s>
<s>
Radiotechnické	radiotechnický	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
zabezpečuje	zabezpečovat	k5eAaImIp3nS
nepřetržité	přetržitý	k2eNgNnSc1d1
radiolokační	radiolokační	k2eAgNnSc1d1
pokrytí	pokrytí	k1gNnSc1
vzdušného	vzdušný	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
do	do	k7c2
výšky	výška	k1gFnSc2
30	#num#	k4
km	km	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
úkol	úkol	k1gInSc1
zajišťuje	zajišťovat	k5eAaImIp3nS
zejména	zejména	k9
26	#num#	k4
<g/>
.	.	kIx.
pluk	pluk	k1gInSc1
velení	velení	k1gNnSc2
<g/>
,	,	kIx,
řízení	řízení	k1gNnSc2
a	a	k8xC
průzkumu	průzkum	k1gInSc2
s	s	k7c7
velitelstvím	velitelství	k1gNnSc7
ve	v	k7c6
Staré	Staré	k2eAgFnSc6d1
Boleslavi	Boleslaev	k1gFnSc6
<g/>
,	,	kIx,
jemuž	jenž	k3xRgMnSc3
je	být	k5eAaImIp3nS
podřízeno	podřídit	k5eAaPmNgNnS
celkem	celkem	k6eAd1
sedm	sedm	k4xCc1
radiotechnických	radiotechnický	k2eAgFnPc2d1
rot	rota	k1gFnPc2
monitorujících	monitorující	k2eAgInPc2d1
celé	celý	k2eAgNnSc4d1
území	území	k1gNnSc4
<g />
.	.	kIx.
</s>
<s hack="1">
České	český	k2eAgFnPc4d1
republiky	republika	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
Cíle	cíl	k1gInSc2
ve	v	k7c6
výškách	výška	k1gFnPc6
nad	nad	k7c7
3000	#num#	k4
m	m	kA
sleduje	sledovat	k5eAaImIp3nS
především	především	k6eAd1
(	(	kIx(
<g/>
nikoli	nikoli	k9
výhradně	výhradně	k6eAd1
<g/>
)	)	kIx)
dvojice	dvojice	k1gFnSc1
stacionárních	stacionární	k2eAgInPc2d1
radarů	radar	k1gInPc2
RAT-31	RAT-31	k1gFnSc2
DL	DL	kA
v	v	k7c6
rámci	rámec	k1gInSc6
páteřní	páteřní	k2eAgFnSc2d1
radiolokační	radiolokační	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
integrovaného	integrovaný	k2eAgInSc2d1
systému	systém	k1gInSc2
protivzdušné	protivzdušný	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
NATINAMDS	NATINAMDS	kA
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
vzdušný	vzdušný	k2eAgInSc4d1
prostor	prostor	k1gInSc4
pod	pod	k7c7
touto	tento	k3xDgFnSc7
hranicí	hranice	k1gFnSc7
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
zóně	zóna	k1gFnSc6
odpovědnosti	odpovědnost	k1gFnSc2
ČR	ČR	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výstupy	výstup	k1gInPc1
z	z	k7c2
vojenských	vojenský	k2eAgInPc2d1
i	i	k8xC
civilních	civilní	k2eAgInPc2d1
radiolokátorů	radiolokátor	k1gInPc2
a	a	k8xC
vojenských	vojenský	k2eAgInPc2d1
pasivních	pasivní	k2eAgInPc2d1
sledovacích	sledovací	k2eAgInPc2d1
systémů	systém	k1gInPc2
(	(	kIx(
<g/>
např.	např.	kA
Věra	Věra	k1gFnSc1
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
zpracovávány	zpracovávat	k5eAaImNgInP
ve	v	k7c6
Staré	Staré	k2eAgFnSc6d1
Boleslavi	Boleslaev	k1gFnSc6
ve	v	k7c6
středisku	středisko	k1gNnSc6
řízení	řízení	k1gNnSc2
a	a	k8xC
uvědomování	uvědomování	k1gNnSc2
(	(	kIx(
<g/>
Control	Control	k1gInSc1
and	and	k?
Reporting	Reporting	k1gInSc1
Centre	centr	k1gInSc5
<g/>
)	)	kIx)
a	a	k8xC
výsledné	výsledný	k2eAgFnPc4d1
informace	informace	k1gFnPc4
o	o	k7c6
vzdušné	vzdušný	k2eAgFnSc6d1
situaci	situace	k1gFnSc6
jsou	být	k5eAaImIp3nP
posílány	posílán	k2eAgInPc1d1
na	na	k7c4
nadřízené	nadřízený	k2eAgInPc4d1
stupně	stupeň	k1gInPc4
velení	velení	k1gNnSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
aliančního	alianční	k2eAgNnSc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
systému	systém	k1gInSc2
NATINAMDS	NATINAMDS	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
Vzdušné	vzdušný	k2eAgFnSc2d1
síly	síla	k1gFnSc2
AČR	AČR	kA
také	také	k6eAd1
využívají	využívat	k5eAaImIp3nP,k5eAaPmIp3nP
schopnosti	schopnost	k1gFnPc4
vzdušného	vzdušný	k2eAgInSc2d1
průzkumu	průzkum	k1gInSc2
systému	systém	k1gInSc2
AWACS	AWACS	kA
v	v	k7c6
rámci	rámec	k1gInSc6
programu	program	k1gInSc2
NATO	NATO	kA
Airborne	Airborn	k1gInSc5
Early	earl	k1gMnPc4
Warning	Warning	k1gInSc4
and	and	k?
Control	Control	k1gInSc1
(	(	kIx(
<g/>
NAEW	NAEW	kA
<g/>
&	&	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c6
němž	jenž	k3xRgInSc6
se	se	k3xPyFc4
od	od	k7c2
roku	rok	k1gInSc2
2011	#num#	k4
podílí	podílet	k5eAaImIp3nS
i	i	k9
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Modernizace	modernizace	k1gFnSc1
</s>
<s>
Schopnost	schopnost	k1gFnSc1
radiolokačního	radiolokační	k2eAgNnSc2d1
pokrytí	pokrytí	k1gNnSc2
pod	pod	k7c4
3	#num#	k4
km	km	kA
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
podle	podle	k7c2
Koncepce	koncepce	k1gFnSc2
výstavby	výstavba	k1gFnSc2
AČR	AČR	kA
2025	#num#	k4
rozšířena	rozšířit	k5eAaPmNgFnS
pořízením	pořízení	k1gNnSc7
mobilních	mobilní	k2eAgInPc2d1
3D	3D	k4
radiolokátorů	radiolokátor	k1gInPc2
nové	nový	k2eAgFnSc2d1
generace	generace	k1gFnSc2
(	(	kIx(
<g/>
MADR	MADR	kA
<g/>
)	)	kIx)
v	v	k7c6
nepřetržitém	přetržitý	k2eNgInSc6d1
provozu	provoz	k1gInSc6
a	a	k8xC
odolných	odolný	k2eAgFnPc6d1
proti	proti	k7c3
rušení	rušení	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
původních	původní	k2eAgInPc2d1
plánů	plán	k1gInPc2
ministerstva	ministerstvo	k1gNnSc2
obrany	obrana	k1gFnSc2
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
do	do	k7c2
poloviny	polovina	k1gFnSc2
roku	rok	k1gInSc2
2015	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
vypsána	vypsán	k2eAgFnSc1d1
veřejná	veřejný	k2eAgFnSc1d1
obchodní	obchodní	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
v	v	k7c6
hodnotě	hodnota	k1gFnSc6
cca	cca	kA
3,5	3,5	k4
mld.	mld.	k?
Kč	Kč	kA
s	s	k7c7
cílem	cíl	k1gInSc7
pořídit	pořídit	k5eAaPmF
10	#num#	k4
systémů	systém	k1gInPc2
MADR	MADR	kA
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
však	však	k9
ministerstvo	ministerstvo	k1gNnSc1
počítalo	počítat	k5eAaImAgNnS
s	s	k7c7
nákupem	nákup	k1gInSc7
(	(	kIx(
<g/>
uzavřeným	uzavřený	k2eAgInSc7d1
na	na	k7c6
mezivládní	mezivládní	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
<g/>
)	)	kIx)
celkem	celkem	k6eAd1
8	#num#	k4
systémů	systém	k1gInPc2
MADR	MADR	kA
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
5	#num#	k4
radarů	radar	k1gInPc2
mělo	mít	k5eAaImAgNnS
sloužit	sloužit	k5eAaImF
pro	pro	k7c4
sledování	sledování	k1gNnSc4
vzdušného	vzdušný	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
státu	stát	k1gInSc2
a	a	k8xC
3	#num#	k4
jako	jako	k8xS,k8xC
záložní	záložní	k2eAgInSc1d1
nebo	nebo	k8xC
pro	pro	k7c4
raketovou	raketový	k2eAgFnSc4d1
PVO	PVO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červenci	červenec	k1gInSc6
2015	#num#	k4
byla	být	k5eAaImAgFnS
potenciálním	potenciální	k2eAgMnPc3d1
dodavatelům	dodavatel	k1gMnPc3
zaslána	zaslat	k5eAaPmNgFnS
oficiální	oficiální	k2eAgFnSc1d1
žádost	žádost	k1gFnSc1
o	o	k7c4
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záměr	záměr	k1gInSc1
ucházet	ucházet	k5eAaImF
se	se	k3xPyFc4
o	o	k7c4
kontrakt	kontrakt	k1gInSc4
projevil	projevit	k5eAaPmAgInS
britský	britský	k2eAgInSc1d1
BAE	BAE	kA
Systems	Systems	k1gInSc1
(	(	kIx(
<g/>
s	s	k7c7
radarem	radar	k1gInSc7
Commander	Commandra	k1gFnPc2
SL	SL	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
izraelská	izraelský	k2eAgFnSc1d1
ELTA	ELTA	kA
(	(	kIx(
<g/>
ELM-	ELM-	k1gMnSc1
<g/>
2084	#num#	k4
MMR	MMR	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
švédský	švédský	k2eAgInSc1d1
Saab	Saab	k1gInSc1
(	(	kIx(
<g/>
Giraffe	Giraff	k1gInSc5
4	#num#	k4
<g/>
A	A	kA
<g/>
)	)	kIx)
a	a	k8xC
francouzský	francouzský	k2eAgInSc1d1
ThalesRaytheonSystems	ThalesRaytheonSystems	k1gInSc1
(	(	kIx(
<g/>
Ground	Ground	k1gMnSc1
Master	master	k1gMnSc1
400	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dodávky	dodávka	k1gFnSc2
radarů	radar	k1gInPc2
s	s	k7c7
požadovanou	požadovaný	k2eAgFnSc7d1
logistickou	logistický	k2eAgFnSc7d1
podporou	podpora	k1gFnSc7
do	do	k7c2
roku	rok	k1gInSc2
2041	#num#	k4
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
proběhnout	proběhnout	k5eAaPmF
v	v	k7c6
letech	léto	k1gNnPc6
2017	#num#	k4
až	až	k9
2021	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
Ministerstvo	ministerstvo	k1gNnSc1
obrany	obrana	k1gFnSc2
vyzvalo	vyzvat	k5eAaPmAgNnS
v	v	k7c6
květnu	květen	k1gInSc6
2016	#num#	k4
vlády	vláda	k1gFnSc2
Francie	Francie	k1gFnSc2
<g/>
,	,	kIx,
Izraele	Izrael	k1gInSc2
<g/>
,	,	kIx,
Švédska	Švédsko	k1gNnSc2
a	a	k8xC
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
k	k	k7c3
podání	podání	k1gNnSc3
nabídek	nabídka	k1gFnPc2
na	na	k7c4
dodávku	dodávka	k1gFnSc4
8	#num#	k4
radiolokátorů	radiolokátor	k1gInPc2
MADR	MADR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
nicméně	nicméně	k8xC
oznámila	oznámit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
již	již	k6eAd1
soutěže	soutěž	k1gFnSc2
dále	daleko	k6eAd2
nebude	být	k5eNaImBp3nS
účastnit	účastnit	k5eAaImF
<g/>
,	,	kIx,
protože	protože	k8xS
britské	britský	k2eAgNnSc4d1
řešení	řešení	k1gNnSc4
ne	ne	k9
zcela	zcela	k6eAd1
odpovídá	odpovídat	k5eAaImIp3nS
požadavkům	požadavek	k1gInPc3
AČR	AČR	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
Komise	komise	k1gFnSc1
ministerstva	ministerstvo	k1gNnSc2
obrany	obrana	k1gFnSc2
upřednostňovala	upřednostňovat	k5eAaImAgFnS
podle	podle	k7c2
mediálních	mediální	k2eAgFnPc2d1
zpráv	zpráva	k1gFnPc2
z	z	k7c2
konce	konec	k1gInSc2
listopadu	listopad	k1gInSc2
2016	#num#	k4
nabídku	nabídka	k1gFnSc4
izraelské	izraelský	k2eAgFnSc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
společnosti	společnost	k1gFnSc2
IAI	IAI	kA
Elta	Elta	k1gFnSc1
Systems	Systems	k1gInSc1
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
Czechoslovak	Czechoslovak	k1gMnSc1
Group	Group	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
2017	#num#	k4
však	však	k9
nebyla	být	k5eNaImAgFnS
smlouva	smlouva	k1gFnSc1
uzavřena	uzavřít	k5eAaPmNgFnS
a	a	k8xC
nově	nově	k6eAd1
jmenovaná	jmenovaný	k2eAgFnSc1d1
ministryně	ministryně	k1gFnSc1
obrany	obrana	k1gFnSc2
Karla	Karla	k1gFnSc1
Šlechtová	Šlechtová	k1gFnSc1
plánovala	plánovat	k5eAaImAgFnS
nákup	nákup	k1gInSc4
radarů	radar	k1gInPc2
MADR	MADR	kA
prověřit	prověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
Šlechtová	Šlechtová	k1gFnSc1
následně	následně	k6eAd1
oznámila	oznámit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
ministerská	ministerský	k2eAgFnSc1d1
inspekce	inspekce	k1gFnSc1
zjistila	zjistit	k5eAaPmAgFnS
u	u	k7c2
některých	některý	k3yIgFnPc2
dosud	dosud	k6eAd1
neukončených	ukončený	k2eNgFnPc2d1
zakázek	zakázka	k1gFnPc2
závažná	závažný	k2eAgNnPc4d1
pochybení	pochybení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
bezpečnostního	bezpečnostní	k2eAgInSc2d1
výboru	výbor	k1gInSc2
NATO	NATO	kA
nemohou	moct	k5eNaImIp3nP
být	být	k5eAaImF
izraelské	izraelský	k2eAgInPc1d1
radary	radar	k1gInPc1
zapojeny	zapojen	k2eAgInPc1d1
do	do	k7c2
aliančního	alianční	k2eAgInSc2d1
systému	systém	k1gInSc2
PVO	PVO	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Koncepce	koncepce	k1gFnSc1
a	a	k8xC
plány	plán	k1gInPc1
</s>
<s>
Koncepce	koncepce	k1gFnSc1
dopravního	dopravní	k2eAgNnSc2d1
a	a	k8xC
vrtulníkového	vrtulníkový	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
</s>
<s>
Vláda	vláda	k1gFnSc1
Jiřího	Jiří	k1gMnSc2
Rusnoka	Rusnoek	k1gMnSc2
schválila	schválit	k5eAaPmAgFnS
dne	den	k1gInSc2
27	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc6
2013	#num#	k4
Koncepci	koncepce	k1gFnSc4
dopravního	dopravní	k2eAgNnSc2d1
a	a	k8xC
vrtulníkového	vrtulníkový	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
s	s	k7c7
výhledem	výhled	k1gInSc7
do	do	k7c2
roku	rok	k1gInSc2
2025	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
Koncepce	koncepce	k1gFnSc1
se	se	k3xPyFc4
kriticky	kriticky	k6eAd1
vyjadřovala	vyjadřovat	k5eAaImAgFnS
ke	k	k7c3
stavu	stav	k1gInSc3
letectva	letectvo	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
v	v	k7c6
důsledku	důsledek	k1gInSc6
úsporných	úsporný	k2eAgNnPc2d1
opatření	opatření	k1gNnPc2
nemělo	mít	k5eNaImAgNnS
prostředky	prostředek	k1gInPc4
na	na	k7c4
nezbytné	nezbytný	k2eAgFnPc4d1,k2eNgFnPc4d1
opravy	oprava	k1gFnPc4
vrtulníků	vrtulník	k1gInPc2
nebo	nebo	k8xC
na	na	k7c4
rekonstrukci	rekonstrukce	k1gFnSc4
leteckých	letecký	k2eAgFnPc2d1
základen	základna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
241	#num#	k4
<g/>
.	.	kIx.
dopravní	dopravní	k2eAgFnSc4d1
letku	letka	k1gFnSc4
dokument	dokument	k1gInSc1
navrhoval	navrhovat	k5eAaImAgInS
pořízení	pořízení	k1gNnSc4
dalšího	další	k2eAgInSc2d1
letounu	letoun	k1gInSc2
Airbus	airbus	k1gInSc4
A	a	k9
<g/>
319	#num#	k4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
by	by	kYmCp3nS
nahradil	nahradit	k5eAaPmAgMnS
dva	dva	k4xCgInPc4
sovětské	sovětský	k2eAgInPc4d1
stroje	stroj	k1gInPc4
Jakovlev	Jakovlev	k1gFnSc2
Jak-	Jak-	k1gFnSc7
<g/>
40	#num#	k4
s	s	k7c7
končící	končící	k2eAgFnSc7d1
technickou	technický	k2eAgFnSc7d1
životností	životnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dopravní	dopravní	k2eAgInSc4d1
letectvo	letectvo	k1gNnSc1
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
po	po	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
modernizováno	modernizovat	k5eAaBmNgNnS
též	též	k9
nákupem	nákup	k1gInSc7
dvou	dva	k4xCgNnPc2
středních	střední	k2eAgNnPc2d1
dopravních	dopravní	k2eAgNnPc2d1
letadel	letadlo	k1gNnPc2
<g/>
:	:	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
se	se	k3xPyFc4
uvažovalo	uvažovat	k5eAaImAgNnS
například	například	k6eAd1
o	o	k7c6
brazilském	brazilský	k2eAgInSc6d1
Embraer	Embraer	k1gInSc1
KC-	KC-	k1gFnSc1
<g/>
390	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
na	na	k7c6
jejichž	jejichž	k3xOyRp3gFnSc6
výrobě	výroba	k1gFnSc6
se	se	k3xPyFc4
podílí	podílet	k5eAaImIp3nS
též	též	k9
Aero	aero	k1gNnSc4
Vodochody	Vodochod	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
Přestože	přestože	k8xS
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
podepsala	podepsat	k5eAaPmAgFnS
předběžnou	předběžný	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
o	o	k7c6
koupi	koupě	k1gFnSc6
dvou	dva	k4xCgMnPc2
těchto	tento	k3xDgInPc2
strojů	stroj	k1gInPc2
<g/>
,	,	kIx,
ministr	ministr	k1gMnSc1
obrany	obrana	k1gFnSc2
Martin	Martin	k1gMnSc1
Stropnický	stropnický	k2eAgMnSc1d1
na	na	k7c4
jednání	jednání	k1gNnPc4
v	v	k7c6
Brazílii	Brazílie	k1gFnSc6
v	v	k7c6
dubnu	duben	k1gInSc6
2015	#num#	k4
akvizici	akvizice	k1gFnSc6
nepotvrdil	potvrdit	k5eNaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
velitele	velitel	k1gMnSc2
vzdušných	vzdušný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
Petra	Petr	k1gMnSc2
Hromka	Hromek	k1gMnSc2
(	(	kIx(
<g/>
ve	v	k7c6
funkci	funkce	k1gFnSc6
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2018	#num#	k4
<g/>
)	)	kIx)
není	být	k5eNaImIp3nS
akvizice	akvizice	k1gFnSc1
KC-390	KC-390	k1gFnSc1
s	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c4
finanční	finanční	k2eAgFnPc4d1
možnosti	možnost	k1gFnPc4
na	na	k7c6
pořadu	pořad	k1gInSc6
dne	den	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Konec	konec	k1gInSc1
životnosti	životnost	k1gFnSc2
bitevních	bitevní	k2eAgInPc2d1
vrtulníků	vrtulník	k1gInPc2
Mi-	Mi-	k1gFnSc2
<g/>
24	#num#	k4
<g/>
V	V	kA
po	po	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
a	a	k8xC
omezenou	omezený	k2eAgFnSc4d1
provozuschopnost	provozuschopnost	k1gFnSc4
záchranných	záchranný	k2eAgInPc2d1
vrtulníků	vrtulník	k1gInPc2
PZL	PZL	kA
W-3A	W-3A	k1gFnSc1
Sokol	Sokol	k1gMnSc1
(	(	kIx(
<g/>
z	z	k7c2
letky	letka	k1gFnSc2
deseti	deset	k4xCc2
Sokolů	Sokol	k1gMnPc2
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
v	v	k7c6
provozu	provoz	k1gInSc6
zhruba	zhruba	k6eAd1
polovina	polovina	k1gFnSc1
<g/>
)	)	kIx)
měla	mít	k5eAaImAgFnS
vyřešit	vyřešit	k5eAaPmF
akvizice	akvizice	k1gFnSc1
víceúčelových	víceúčelový	k2eAgFnPc2d1
helikoptér	helikoptéra	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
využitelné	využitelný	k2eAgInPc1d1
jak	jak	k8xS,k8xC
pro	pro	k7c4
účely	účel	k1gInPc4
bojových	bojový	k2eAgFnPc2d1
operací	operace	k1gFnPc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
pro	pro	k7c4
leteckou	letecký	k2eAgFnSc4d1
záchrannou	záchranný	k2eAgFnSc4d1
službu	služba	k1gFnSc4
a	a	k8xC
službu	služba	k1gFnSc4
pátrání	pátrání	k1gNnSc2
a	a	k8xC
záchrany	záchrana	k1gFnSc2
na	na	k7c6
území	území	k1gNnSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obměna	obměna	k1gFnSc1
vrtulníků	vrtulník	k1gInPc2
měla	mít	k5eAaImAgFnS
v	v	k7c6
budoucnu	budoucno	k1gNnSc6
proběhnout	proběhnout	k5eAaPmF
i	i	k9
u	u	k7c2
243	#num#	k4
<g/>
.	.	kIx.
vrtulníkové	vrtulníkový	k2eAgFnSc2d1
letky	letka	k1gFnSc2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
prozatím	prozatím	k6eAd1
se	se	k3xPyFc4
počítalo	počítat	k5eAaImAgNnS
se	s	k7c7
zachováním	zachování	k1gNnSc7
helikoptéry	helikoptéra	k1gFnSc2
Mi-	Mi-	k1gFnSc7
<g/>
8	#num#	k4
ve	v	k7c6
VIP	VIP	kA
úpravě	úprava	k1gFnSc6
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
cestuje	cestovat	k5eAaImIp3nS
prezident	prezident	k1gMnSc1
či	či	k8xC
ministři	ministr	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokument	dokument	k1gInSc1
také	také	k6eAd1
upozorňoval	upozorňovat	k5eAaImAgInS
<g/>
,	,	kIx,
že	že	k8xS
nedostatek	nedostatek	k1gInSc1
peněz	peníze	k1gInPc2
špatně	špatně	k6eAd1
motivuje	motivovat	k5eAaBmIp3nS
vojáky	voják	k1gMnPc4
k	k	k7c3
dalšímu	další	k2eAgNnSc3d1
setrvání	setrvání	k1gNnSc3
v	v	k7c6
armádě	armáda	k1gFnSc6
a	a	k8xC
letectvo	letectvo	k1gNnSc1
se	se	k3xPyFc4
tak	tak	k6eAd1
potýká	potýkat	k5eAaImIp3nS
s	s	k7c7
nedostatkem	nedostatek	k1gInSc7
kvalitně	kvalitně	k6eAd1
vycvičeného	vycvičený	k2eAgInSc2d1
personálu	personál	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Záměr	záměr	k1gInSc4
nahradit	nahradit	k5eAaPmF
dosluhující	dosluhující	k2eAgInPc4d1
letouny	letoun	k1gInPc4
Jakovlev	Jakovlev	k1gFnSc2
Jak-	Jak-	k1gFnSc3
<g/>
40	#num#	k4
třetím	třetí	k4xOgMnSc6
strojem	stroj	k1gInSc7
Airbus	airbus	k1gInSc1
A319	A319	k1gFnPc2
označil	označit	k5eAaPmAgMnS
ministr	ministr	k1gMnSc1
obrany	obrana	k1gFnSc2
Martin	Martin	k1gInSc1
Stropnický	stropnický	k2eAgInSc1d1
po	po	k7c6
jeho	jeho	k3xOp3gInSc6
nástupu	nástup	k1gInSc6
do	do	k7c2
funkce	funkce	k1gFnSc2
za	za	k7c4
„	„	k?
<g/>
docela	docela	k6eAd1
šílený	šílený	k2eAgInSc4d1
plán	plán	k1gInSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
hodlal	hodlat	k5eAaImAgMnS
více	hodně	k6eAd2
využívat	využívat	k5eAaPmF,k5eAaImF
komerční	komerční	k2eAgFnPc1d1
linky	linka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
ministra	ministr	k1gMnSc2
by	by	kYmCp3nP
také	také	k9
některé	některý	k3yIgFnPc1
delegace	delegace	k1gFnPc1
mohly	moct	k5eAaImAgFnP
cestovat	cestovat	k5eAaImF
transportními	transportní	k2eAgInPc7d1
letouny	letoun	k1gInPc7
CASA	CASA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
důsledku	důsledek	k1gInSc6
války	válka	k1gFnSc2
na	na	k7c6
východní	východní	k2eAgFnSc6d1
Ukrajině	Ukrajina	k1gFnSc6
se	se	k3xPyFc4
však	však	k9
podle	podle	k7c2
Stropnického	stropnický	k2eAgInSc2d1
změnilo	změnit	k5eAaPmAgNnS
bezpečnostní	bezpečnostní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
v	v	k7c6
sousedství	sousedství	k1gNnSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
proto	proto	k8xC
se	se	k3xPyFc4
znovu	znovu	k6eAd1
uvažovalo	uvažovat	k5eAaImAgNnS
o	o	k7c6
třetím	třetí	k4xOgInSc6
letounu	letoun	k1gInSc6
pro	pro	k7c4
„	„	k?
<g/>
vládní	vládní	k2eAgFnSc4d1
letku	letka	k1gFnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
Ministr	ministr	k1gMnSc1
také	také	k9
oznámil	oznámit	k5eAaPmAgMnS
úmysl	úmysl	k1gInSc4
vypsat	vypsat	k5eAaPmF
tendr	tendr	k1gInSc4
až	až	k9
do	do	k7c2
výše	výše	k1gFnSc2,k1gFnSc2wB
7	#num#	k4
miliard	miliarda	k4xCgFnPc2
korun	koruna	k1gFnPc2
na	na	k7c4
nepravidelnou	pravidelný	k2eNgFnSc4d1
leteckou	letecký	k2eAgFnSc4d1
<g/>
,	,	kIx,
lodní	lodní	k2eAgFnSc4d1
a	a	k8xC
železniční	železniční	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
po	po	k7c4
dobu	doba	k1gFnSc4
sedmi	sedm	k4xCc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Koncepce	koncepce	k1gFnSc1
výstavby	výstavba	k1gFnSc2
AČR	AČR	kA
2025	#num#	k4
</s>
<s>
Airbus	airbus	k1gInSc1
A319	A319	k1gFnSc2
a	a	k8xC
JAS-39	JAS-39	k1gFnSc2
</s>
<s>
Tento	tento	k3xDgInSc1
strategický	strategický	k2eAgInSc1d1
dokument	dokument	k1gInSc1
<g/>
,	,	kIx,
schválený	schválený	k2eAgInSc1d1
vládou	vláda	k1gFnSc7
dne	den	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2015	#num#	k4
<g/>
,	,	kIx,
definuje	definovat	k5eAaBmIp3nS
požadovaný	požadovaný	k2eAgInSc4d1
stav	stav	k1gInSc4
Armády	armáda	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2025	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdušné	vzdušný	k2eAgFnPc1d1
síly	síla	k1gFnPc1
by	by	kYmCp3nP
podle	podle	k7c2
této	tento	k3xDgFnSc2
koncepce	koncepce	k1gFnSc2
měly	mít	k5eAaImAgFnP
ve	v	k7c6
stanoveném	stanovený	k2eAgNnSc6d1
období	období	k1gNnSc6
<g/>
:	:	kIx,
</s>
<s>
získat	získat	k5eAaPmF
schopnost	schopnost	k1gFnSc4
provádět	provádět	k5eAaImF
údery	úder	k1gInPc4
nadzvukovým	nadzvukový	k2eAgNnSc7d1
taktickým	taktický	k2eAgNnSc7d1
letectvem	letectvo	k1gNnSc7
na	na	k7c4
pozemní	pozemní	k2eAgInPc4d1
cíle	cíl	k1gInPc4
přesně	přesně	k6eAd1
naváděnou	naváděný	k2eAgFnSc7d1
municí	munice	k1gFnSc7
<g/>
;	;	kIx,
</s>
<s>
rozhodnout	rozhodnout	k5eAaPmF
o	o	k7c6
dalším	další	k2eAgInSc6d1
pronájmu	pronájem	k1gInSc6
nebo	nebo	k8xC
akvizici	akvizice	k1gFnSc6
nadzvukových	nadzvukový	k2eAgInPc2d1
stíhacích	stíhací	k2eAgInPc2d1
letounů	letoun	k1gInPc2
<g/>
;	;	kIx,
</s>
<s>
zabezpečit	zabezpečit	k5eAaPmF
výcvik	výcvik	k1gInSc4
pilotů	pilot	k1gMnPc2
taktického	taktický	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
na	na	k7c6
strojích	stroj	k1gInPc6
L-39	L-39	k1gFnSc1
a	a	k8xC
L-	L-	k1gFnSc1
<g/>
159	#num#	k4
<g/>
;	;	kIx,
</s>
<s>
pořídit	pořídit	k5eAaPmF
víceúčelový	víceúčelový	k2eAgInSc4d1
vrtulník	vrtulník	k1gInSc4
se	s	k7c7
schopností	schopnost	k1gFnSc7
palby	palba	k1gFnSc2
pomocí	pomocí	k7c2
přesně	přesně	k6eAd1
naváděné	naváděný	k2eAgFnSc2d1
munice	munice	k1gFnSc2
<g/>
;	;	kIx,
</s>
<s>
rozhodnout	rozhodnout	k5eAaPmF
o	o	k7c6
potřebě	potřeba	k1gFnSc6
pořízení	pořízení	k1gNnSc2
bitevních	bitevní	k2eAgInPc2d1
vrtulníků	vrtulník	k1gInPc2
<g/>
;	;	kIx,
</s>
<s>
dokončit	dokončit	k5eAaPmF
projekt	projekt	k1gInSc4
modernizace	modernizace	k1gFnSc2
vrtulníků	vrtulník	k1gInPc2
Mi-	Mi-	k1gFnSc2
<g/>
171	#num#	k4
<g/>
Š	Š	kA
<g/>
;	;	kIx,
</s>
<s>
před	před	k7c7
ukončením	ukončení	k1gNnSc7
technické	technický	k2eAgFnSc2d1
životnosti	životnost	k1gFnSc2
vrtulníků	vrtulník	k1gInPc2
Mi-	Mi-	k1gFnSc2
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
rozhodnout	rozhodnout	k5eAaPmF
o	o	k7c6
jejich	jejich	k3xOp3gFnSc6
náhradě	náhrada	k1gFnSc6
4	#num#	k4
novými	nový	k2eAgInPc7d1
transportními	transportní	k2eAgInPc7d1
vrtulníky	vrtulník	k1gInPc7
<g/>
;	;	kIx,
</s>
<s>
získat	získat	k5eAaPmF
schopnost	schopnost	k1gFnSc4
přepravy	přeprava	k1gFnSc2
materiálu	materiál	k1gInSc2
pořízením	pořízení	k1gNnSc7
2	#num#	k4
středních	střední	k2eAgInPc2d1
transportních	transportní	k2eAgInPc2d1
letounů	letoun	k1gInPc2
<g/>
;	;	kIx,
</s>
<s>
nahradit	nahradit	k5eAaPmF
letouny	letoun	k1gInPc4
Jak-	Jak-	k1gFnSc2
<g/>
40	#num#	k4
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c4
ukončení	ukončení	k1gNnSc4
jejich	jejich	k3xOp3gInSc2
provozu	provoz	k1gInSc2
typem	typ	k1gInSc7
schopným	schopný	k2eAgInSc7d1
rozšířit	rozšířit	k5eAaPmF
přepravní	přepravní	k2eAgFnPc4d1
kapacity	kapacita	k1gFnPc4
ve	v	k7c4
prospěch	prospěch	k1gInSc4
pozemních	pozemní	k2eAgFnPc2d1
sil	síla	k1gFnPc2
<g/>
;	;	kIx,
</s>
<s>
pořídit	pořídit	k5eAaPmF
protiletadlové	protiletadlový	k2eAgInPc4d1
raketové	raketový	k2eAgInPc4d1
systémy	systém	k1gInPc4
krátkého	krátký	k2eAgInSc2d1
a	a	k8xC
středního	střední	k2eAgInSc2d1
dosahu	dosah	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Modernizace	modernizace	k1gFnSc1
AČR	AČR	kA
do	do	k7c2
roku	rok	k1gInSc2
2027	#num#	k4
</s>
<s>
Náčelník	náčelník	k1gMnSc1
Generální	generální	k2eAgMnSc1d1
štábu	štáb	k1gInSc2
generálporučík	generálporučík	k1gMnSc1
Aleš	Aleš	k1gMnSc1
Opata	opat	k1gMnSc4
představil	představit	k5eAaPmAgMnS
na	na	k7c6
setkání	setkání	k1gNnSc6
s	s	k7c7
novináři	novinář	k1gMnPc7
dne	den	k1gInSc2
18	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2018	#num#	k4
program	program	k1gInSc1
modernizace	modernizace	k1gFnSc2
Armády	armáda	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
2027	#num#	k4
má	mít	k5eAaImIp3nS
armáda	armáda	k1gFnSc1
na	na	k7c4
nákup	nákup	k1gInSc4
a	a	k8xC
modernizaci	modernizace	k1gFnSc4
techniky	technika	k1gFnSc2
vynaložit	vynaložit	k5eAaPmF
více	hodně	k6eAd2
než	než	k8xS
100	#num#	k4
miliard	miliarda	k4xCgFnPc2
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
Pro	pro	k7c4
vzdušné	vzdušný	k2eAgFnPc4d1
síly	síla	k1gFnPc4
se	se	k3xPyFc4
předpokládá	předpokládat	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
náhrada	náhrada	k1gFnSc1
Mi-	Mi-	k1gFnSc1
<g/>
24	#num#	k4
<g/>
V	V	kA
<g/>
/	/	kIx~
<g/>
35	#num#	k4
novými	nový	k2eAgInPc7d1
víceúčelovými	víceúčelový	k2eAgInPc7d1
vrtulníky	vrtulník	k1gInPc7
<g/>
;	;	kIx,
</s>
<s>
modernizace	modernizace	k1gFnSc1
letounů	letoun	k1gInPc2
CASA	CASA	kA
C-	C-	k1gFnSc1
<g/>
295	#num#	k4
<g/>
M	M	kA
<g/>
;	;	kIx,
</s>
<s>
nákup	nákup	k1gInSc1
2	#num#	k4
letounů	letoun	k1gInPc2
C-	C-	k1gFnSc2
<g/>
295	#num#	k4
<g/>
W	W	kA
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
zabezpečí	zabezpečit	k5eAaPmIp3nP
i	i	k9
přepravu	přeprava	k1gFnSc4
ústavních	ústavní	k2eAgInPc2d1
a	a	k8xC
vládních	vládní	k2eAgInPc2d1
činitelů	činitel	k1gInPc2
a	a	k8xC
nahradí	nahradit	k5eAaPmIp3nS
tak	tak	k6eAd1
v	v	k7c6
plnění	plnění	k1gNnSc6
úkolů	úkol	k1gInPc2
letouny	letoun	k1gInPc1
Jak-	Jak-	k1gFnSc1
<g/>
40	#num#	k4
<g/>
;	;	kIx,
</s>
<s>
pro	pro	k7c4
letouny	letoun	k1gInPc4
JAS	Jasy	k1gInPc2
39	#num#	k4
Gripen	Gripen	k2eAgInSc4d1
<g/>
:	:	kIx,
</s>
<s>
do	do	k7c2
roku	rok	k1gInSc2
2020	#num#	k4
<g/>
:	:	kIx,
schopnost	schopnost	k1gFnSc1
ničení	ničení	k1gNnSc2
pozemních	pozemní	k2eAgInPc2d1
cílů	cíl	k1gInPc2
přesně	přesně	k6eAd1
naváděnou	naváděný	k2eAgFnSc7d1
municí	munice	k1gFnSc7
<g/>
,	,	kIx,
modernizace	modernizace	k1gFnSc1
identifikačního	identifikační	k2eAgInSc2d1
módu	mód	k1gInSc2
odpovídače	odpovídač	k1gInSc2
na	na	k7c4
standard	standard	k1gInSc4
IFF	IFF	kA
mode	modus	k1gInSc5
5	#num#	k4
<g/>
,	,	kIx,
pořízení	pořízení	k1gNnSc4
závěsníků	závěsník	k1gInPc2
standardu	standard	k1gInSc2
NATO	NATO	kA
<g/>
,	,	kIx,
laserového	laserový	k2eAgInSc2d1
průzkumného	průzkumný	k2eAgInSc2d1
a	a	k8xC
zaměřovacího	zaměřovací	k2eAgInSc2d1
kontejneru	kontejner	k1gInSc2
<g/>
,	,	kIx,
brýlí	brýle	k1gFnPc2
nočního	noční	k2eAgNnSc2d1
vidění	vidění	k1gNnSc2
a	a	k8xC
utajovaného	utajovaný	k2eAgNnSc2d1
hlasového	hlasový	k2eAgNnSc2d1
a	a	k8xC
datového	datový	k2eAgNnSc2d1
spojení	spojení	k1gNnSc2
<g/>
;	;	kIx,
</s>
<s>
do	do	k7c2
roku	rok	k1gInSc2
2025	#num#	k4
<g/>
:	:	kIx,
integrace	integrace	k1gFnSc2
nových	nový	k2eAgInPc2d1
typů	typ	k1gInPc2
protiletadlových	protiletadlový	k2eAgFnPc2d1
řízených	řízený	k2eAgFnPc2d1
střel	střela	k1gFnPc2
krátkého	krátký	k2eAgInSc2d1
i	i	k8xC
středního	střední	k2eAgInSc2d1
dosahu	dosah	k1gInSc2
a	a	k8xC
navýšení	navýšení	k1gNnSc2
schopnosti	schopnost	k1gFnSc2
podpory	podpora	k1gFnSc2
pozemních	pozemní	k2eAgFnPc2d1
sil	síla	k1gFnPc2
v	v	k7c6
ničení	ničení	k1gNnSc6
pozemních	pozemní	k2eAgInPc2d1
cílů	cíl	k1gInPc2
zavedením	zavedení	k1gNnSc7
letecké	letecký	k2eAgFnSc2d1
munice	munice	k1gFnSc2
naváděné	naváděný	k2eAgFnSc2d1
GPS	GPS	kA
<g/>
;	;	kIx,
</s>
<s>
akvizice	akvizice	k1gFnSc1
protiletadlových	protiletadlový	k2eAgInPc2d1
kompletů	komplet	k1gInPc2
kategorie	kategorie	k1gFnSc2
SHORAD	SHORAD	kA
(	(	kIx(
<g/>
Short	Short	k1gInSc1
Range	Rang	k1gMnSc2
Air	Air	k1gMnSc2
Defence	Defenec	k1gMnSc2
<g/>
)	)	kIx)
<g/>
;	;	kIx,
</s>
<s>
pořízení	pořízení	k1gNnSc4
radiolokátorů	radiolokátor	k1gInPc2
typu	typ	k1gInSc2
MADR	MADR	kA
(	(	kIx(
<g/>
Mobile	mobile	k1gNnSc4
Air	Air	k1gFnSc2
Defence	Defenec	k1gInSc2
Radar	radar	k1gInSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
</s>
<s>
náhrada	náhrada	k1gFnSc1
radiolokační	radiolokační	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
na	na	k7c6
vojenských	vojenský	k2eAgNnPc6d1
letištích	letiště	k1gNnPc6
<g/>
,	,	kIx,
včetně	včetně	k7c2
zabezpečení	zabezpečení	k1gNnSc2
přenosu	přenos	k1gInSc2
a	a	k8xC
distribuce	distribuce	k1gFnSc2
radarových	radarový	k2eAgNnPc2d1
dat	datum	k1gNnPc2
pro	pro	k7c4
systémy	systém	k1gInPc4
řízení	řízení	k1gNnSc2
letového	letový	k2eAgInSc2d1
provozu	provoz	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Personál	personál	k1gInSc1
</s>
<s>
Výcvik	výcvik	k1gInSc1
</s>
<s>
Příslušníci	příslušník	k1gMnPc1
létajícího	létající	k2eAgInSc2d1
personálu	personál	k1gInSc2
Vzdušných	vzdušný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
AČR	AČR	kA
začínají	začínat	k5eAaImIp3nP
svou	svůj	k3xOyFgFnSc4
kariéru	kariéra	k1gFnSc4
studiem	studio	k1gNnSc7
na	na	k7c6
Univerzitě	univerzita	k1gFnSc6
obrany	obrana	k1gFnSc2
a	a	k8xC
po	po	k7c6
absolvování	absolvování	k1gNnSc6
základního	základní	k2eAgInSc2d1
testovacího	testovací	k2eAgInSc2d1
výcviku	výcvik	k1gInSc2
v	v	k7c6
Centru	centrum	k1gNnSc6
leteckého	letecký	k2eAgInSc2d1
výcviku	výcvik	k1gInSc2
v	v	k7c6
Pardubicích	Pardubice	k1gInPc6
jsou	být	k5eAaImIp3nP
studenti	student	k1gMnPc1
rozřazeni	rozřadit	k5eAaPmNgMnP
k	k	k7c3
jednotlivým	jednotlivý	k2eAgInPc3d1
typům	typ	k1gInPc3
letecké	letecký	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příslušnost	příslušnost	k1gFnSc1
nových	nový	k2eAgMnPc2d1
pilotů	pilot	k1gMnPc2
k	k	k7c3
taktickému	taktický	k2eAgNnSc3d1
<g/>
,	,	kIx,
dopravnímu	dopravní	k2eAgNnSc3d1
nebo	nebo	k8xC
vrtulníkovému	vrtulníkový	k2eAgNnSc3d1
letectvu	letectvo	k1gNnSc3
určují	určovat	k5eAaImIp3nP
kromě	kromě	k7c2
hodnocení	hodnocení	k1gNnSc2
instruktorů	instruktor	k1gMnPc2
především	především	k6eAd1
aktuální	aktuální	k2eAgFnSc2d1
potřeby	potřeba	k1gFnSc2
vzdušných	vzdušný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
dané	daný	k2eAgFnSc2d1
Odborem	odbor	k1gInSc7
vojenského	vojenský	k2eAgNnSc2d1
letectví	letectví	k1gNnSc2
ministerstva	ministerstvo	k1gNnSc2
obrany	obrana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letci	letec	k1gMnPc1
jsou	být	k5eAaImIp3nP
následně	následně	k6eAd1
formálně	formálně	k6eAd1
zařazeni	zařadit	k5eAaPmNgMnP
k	k	k7c3
jednotlivým	jednotlivý	k2eAgFnPc3d1
letkám	letka	k1gFnPc3
a	a	k8xC
v	v	k7c6
Pardubicích	Pardubice	k1gInPc6
jsou	být	k5eAaImIp3nP
přeškoleni	přeškolit	k5eAaPmNgMnP
na	na	k7c4
konkrétní	konkrétní	k2eAgInSc4d1
typ	typ	k1gInSc4
letadla	letadlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Piloti	pilot	k1gMnPc1
taktického	taktický	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
postupují	postupovat	k5eAaImIp3nP
k	k	k7c3
pokračovacímu	pokračovací	k2eAgInSc3d1
výcviku	výcvik	k1gInSc3
u	u	k7c2
213	#num#	k4
<g/>
.	.	kIx.
letky	letka	k1gFnSc2
na	na	k7c6
základně	základna	k1gFnSc6
v	v	k7c6
Čáslavi	Čáslav	k1gFnSc6
na	na	k7c6
letounech	letoun	k1gInPc6
L-39	L-39	k1gMnSc1
a	a	k8xC
L-	L-	k1gMnSc1
<g/>
159	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
ukončení	ukončení	k1gNnSc4
tohoto	tento	k3xDgInSc2
výcviku	výcvik	k1gInSc2
si	se	k3xPyFc3
nejlepší	dobrý	k2eAgMnPc1d3
piloti	pilot	k1gMnPc1
mohou	moct	k5eAaImIp3nP
vybrat	vybrat	k5eAaPmF
<g/>
,	,	kIx,
zda	zda	k8xS
chtějí	chtít	k5eAaImIp3nP
zůstat	zůstat	k5eAaPmF
na	na	k7c6
bitevníku	bitevník	k1gInSc6
ALCA	ALCA	kA
nebo	nebo	k8xC
přejít	přejít	k5eAaPmF
na	na	k7c4
nadzvukové	nadzvukový	k2eAgInPc4d1
letouny	letoun	k1gInPc4
JAS-39	JAS-39	k1gMnPc2
Gripen	Gripen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
takovém	takový	k3xDgInSc6
případě	případ	k1gInSc6
jim	on	k3xPp3gMnPc3
je	být	k5eAaImIp3nS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
32	#num#	k4
až	až	k9
33	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
převyšuje	převyšovat	k5eAaImIp3nS
průměr	průměr	k1gInSc4
zemí	zem	k1gFnPc2
NATO	NATO	kA
a	a	k8xC
velení	velení	k1gNnSc2
letectva	letectvo	k1gNnSc2
proto	proto	k8xC
plánuje	plánovat	k5eAaImIp3nS
do	do	k7c2
roku	rok	k1gInSc2
2025	#num#	k4
systém	systém	k1gInSc1
výcviku	výcvik	k1gInSc2
stíhacích	stíhací	k2eAgInPc2d1
pilotů	pilot	k1gInPc2
změnit	změnit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mužstvo	mužstvo	k1gNnSc1
<g/>
,	,	kIx,
poddůstojníci	poddůstojník	k1gMnPc1
a	a	k8xC
praporčíci	praporčík	k1gMnPc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Vojenské	vojenský	k2eAgFnSc2d1
hodnosti	hodnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nadrotmistr	nadrotmistr	k1gMnSc1
z	z	k7c2
řad	řada	k1gFnPc2
pozemního	pozemní	k2eAgInSc2d1
personálu	personál	k1gInSc2
čáslavské	čáslavský	k2eAgFnSc2d1
základny	základna	k1gFnSc2
</s>
<s>
Brig	briga	k1gFnPc2
<g/>
.	.	kIx.
gen.	gen.	kA
Jiří	Jiří	k1gMnSc1
Verner	Verner	k1gMnSc1
<g/>
,	,	kIx,
velitel	velitel	k1gMnSc1
VzS	VzS	k1gFnSc2
AČR	AČR	kA
v	v	k7c6
letech	let	k1gInPc6
2009	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
</s>
<s>
Mužstvo	mužstvo	k1gNnSc1
</s>
<s>
Poddůstojníci	poddůstojník	k1gMnPc1
</s>
<s>
Praporčíci	praporčík	k1gMnPc1
</s>
<s>
VzS	VzS	k?
</s>
<s>
vojínsvobodníkdesátníkčetařrotný	vojínsvobodníkdesátníkčetařrotný	k2eAgInSc1d1
</s>
<s>
rotmistrnadrotmistrpraporčíknadpraporčíkštábní	rotmistrnadrotmistrpraporčíknadpraporčíkštábnit	k5eAaPmIp3nS
praporčík	praporčík	k1gMnSc1
</s>
<s>
OR-1OR-2OR-3OR-4OR-5OR-6OR-7OR-8OR-9	OR-1OR-2OR-3OR-4OR-5OR-6OR-7OR-8OR-9	k4
</s>
<s>
Důstojníci	důstojník	k1gMnPc1
a	a	k8xC
generálové	generál	k1gMnPc1
</s>
<s>
Nižší	nízký	k2eAgMnPc1d2
důstojníci	důstojník	k1gMnPc1
</s>
<s>
Vyšší	vysoký	k2eAgMnPc1d2
důstojníci	důstojník	k1gMnPc1
</s>
<s>
Generálové	generál	k1gMnPc1
</s>
<s>
VzS	VzS	k?
</s>
<s>
poručíknadporučíkkapitán	poručíknadporučíkkapitán	k1gMnSc1
</s>
<s>
majorpodplukovníkplukovník	majorpodplukovníkplukovník	k1gMnSc1
</s>
<s>
brigádní	brigádní	k2eAgInSc4d1
generálgenerálmajorgenerálporučík	generálgenerálmajorgenerálporučík	k1gInSc4
</s>
<s>
OF-1OF-2OF-3OF-4OF-5OF-6OF-7OF-8	OF-1OF-2OF-3OF-4OF-5OF-6OF-7OF-8	k4
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Prvním	první	k4xOgInSc7
českým	český	k2eAgInSc7d1
pilotem	pilot	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
absolvoval	absolvovat	k5eAaPmAgInS
přeškolení	přeškolení	k1gNnSc4
a	a	k8xC
výcvik	výcvik	k1gInSc4
na	na	k7c4
letoun	letoun	k1gInSc4
AWACS	AWACS	kA
Boeing	boeing	k1gInSc1
E-3	E-3	k1gFnSc1
Sentry	Sentr	k1gInPc1
v	v	k7c6
rámci	rámec	k1gInSc6
české	český	k2eAgFnSc2d1
účasti	účast	k1gFnSc2
v	v	k7c6
programu	program	k1gInSc6
NAEW	NAEW	kA
<g/>
&	&	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
2	#num#	k4
<g/>
]	]	kIx)
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
stal	stát	k5eAaPmAgMnS
major	major	k1gMnSc1
Milan	Milan	k1gMnSc1
Vojáček	Vojáček	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Během	během	k7c2
mise	mise	k1gFnSc2
Baltic	Baltice	k1gFnPc2
Air	Air	k1gFnSc1
Policing	Policing	k1gInSc1
2012	#num#	k4
se	se	k3xPyFc4
kapitán	kapitán	k1gMnSc1
Pavel	Pavel	k1gMnSc1
Pavlík	Pavlík	k1gMnSc1
z	z	k7c2
211	#num#	k4
<g/>
.	.	kIx.
taktické	taktický	k2eAgFnSc2d1
letky	letka	k1gFnSc2
stal	stát	k5eAaPmAgMnS
prvním	první	k4xOgMnSc7
aliančním	alianční	k2eAgMnSc7d1
stíhacím	stíhací	k2eAgMnSc7d1
pilotem	pilot	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
od	od	k7c2
dob	doba	k1gFnPc2
stažení	stažení	k1gNnSc2
sovětských	sovětský	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
z	z	k7c2
Estonska	Estonsko	k1gNnSc2
přistál	přistát	k5eAaPmAgInS,k5eAaImAgInS
s	s	k7c7
nadzvukovým	nadzvukový	k2eAgMnSc7d1
letounem	letoun	k1gMnSc7
na	na	k7c6
nově	nově	k6eAd1
zrekonstruovaném	zrekonstruovaný	k2eAgNnSc6d1
letišti	letiště	k1gNnSc6
estonské	estonský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
v	v	k7c6
Ämari	Ämar	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
První	první	k4xOgFnSc7
pilotkou	pilotka	k1gFnSc7
proudových	proudový	k2eAgInPc2d1
bojových	bojový	k2eAgInPc2d1
letounů	letoun	k1gInPc2
u	u	k7c2
českých	český	k2eAgFnPc2d1
vzdušných	vzdušný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
nadporučice	nadporučice	k1gFnSc1
Kateřina	Kateřina	k1gFnSc1
Hlavsová	Hlavsová	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
prošla	projít	k5eAaPmAgFnS
přeškolením	přeškolení	k1gNnSc7
z	z	k7c2
letounu	letoun	k1gInSc2
L-39	L-39	k1gMnSc1
Albatros	albatros	k1gMnSc1
na	na	k7c4
bitevník	bitevník	k1gInSc4
L-159	L-159	k1gMnSc2
Alca	Alcus	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pilot	pilot	k1gMnSc1
L-159	L-159	k1gMnSc1
nadporučík	nadporučík	k1gMnSc1
Zbyněk	Zbyněk	k1gMnSc1
Abel	Abel	k1gMnSc1
byl	být	k5eAaImAgMnS
vyznamenán	vyznamenat	k5eAaPmNgMnS
armádní	armádní	k2eAgFnSc7d1
medailí	medaile	k1gFnSc7
za	za	k7c4
zásluhy	zásluha	k1gFnPc4
za	za	k7c4
rozhodnost	rozhodnost	k1gFnSc4
a	a	k8xC
mimořádnou	mimořádný	k2eAgFnSc4d1
odvahu	odvaha	k1gFnSc4
<g/>
,	,	kIx,
když	když	k8xS
28	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2015	#num#	k4
bezpečně	bezpečně	k6eAd1
přistál	přistát	k5eAaImAgInS,k5eAaPmAgInS
s	s	k7c7
letounem	letoun	k1gInSc7
vážně	vážně	k6eAd1
poškozeným	poškozený	k2eAgInSc7d1
srážkou	srážka	k1gFnSc7
se	s	k7c7
supem	sup	k1gMnSc7
během	během	k7c2
cvičení	cvičení	k1gNnSc2
ve	v	k7c6
Španělsku	Španělsko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Znaky	znak	k1gInPc1
a	a	k8xC
insignie	insignie	k1gFnPc1
</s>
<s>
Vlevo	vlevo	k6eAd1
<g/>
:	:	kIx,
Kokarda	kokarda	k1gFnSc1
používaná	používaný	k2eAgFnSc1d1
od	od	k7c2
roku	rok	k1gInSc2
1926	#num#	k4
<g/>
Vpravo	vpravo	k6eAd1
<g/>
:	:	kIx,
Taktická	taktický	k2eAgFnSc1d1
varianta	varianta	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1999	#num#	k4
</s>
<s>
Výsostné	výsostný	k2eAgInPc1d1
znaky	znak	k1gInPc1
Vzdušných	vzdušný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
AČR	AČR	kA
se	se	k3xPyFc4
shodují	shodovat	k5eAaImIp3nP
s	s	k7c7
konečnou	konečný	k2eAgFnSc7d1
verzí	verze	k1gFnSc7
československé	československý	k2eAgFnSc2d1
vojenské	vojenský	k2eAgFnSc2d1
kokardy	kokarda	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
od	od	k7c2
prosince	prosinec	k1gInSc2
1926	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dělí	dělit	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
tři	tři	k4xCgInPc4
stejné	stejný	k2eAgInPc4d1
klíny	klín	k1gInPc4
bílé	bílý	k2eAgFnSc2d1
<g/>
,	,	kIx,
červené	červený	k2eAgFnSc2d1
a	a	k8xC
modré	modrý	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
modrý	modrý	k2eAgInSc1d1
je	být	k5eAaImIp3nS
vždy	vždy	k6eAd1
vepředu	vepředu	k6eAd1
a	a	k8xC
bílý	bílý	k2eAgInSc1d1
směřuje	směřovat	k5eAaImIp3nS
nahoru	nahoru	k6eAd1
nebo	nebo	k8xC
od	od	k7c2
středu	střed	k1gInSc2
označovaného	označovaný	k2eAgInSc2d1
stroje	stroj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lemování	lemování	k1gNnSc1
znaku	znak	k1gInSc2
je	být	k5eAaImIp3nS
většinou	většinou	k6eAd1
bílé	bílý	k2eAgNnSc1d1
nebo	nebo	k8xC
modré	modrý	k2eAgNnSc1d1
<g/>
,	,	kIx,
červené	červený	k2eAgNnSc1d1
není	být	k5eNaImIp3nS
příliš	příliš	k6eAd1
obvyklé	obvyklý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
se	se	k3xPyFc4
tato	tento	k3xDgFnSc1
kokarda	kokarda	k1gFnSc1
rozšířila	rozšířit	k5eAaPmAgFnS
i	i	k9
na	na	k7c4
pozemní	pozemní	k2eAgFnSc4d1
techniku	technika	k1gFnSc4
a	a	k8xC
dodnes	dodnes	k6eAd1
je	být	k5eAaImIp3nS
jako	jako	k9
tzv.	tzv.	kA
národní	národní	k2eAgInSc1d1
rozlišovací	rozlišovací	k2eAgInSc1d1
znak	znak	k1gInSc1
užívána	užívat	k5eAaImNgFnS
Armádou	armáda	k1gFnSc7
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
5	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1999	#num#	k4
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
i	i	k9
nebarevná	barevný	k2eNgFnSc1d1
stínovaná	stínovaný	k2eAgFnSc1d1
neboli	neboli	k8xC
taktická	taktický	k2eAgFnSc1d1
varianta	varianta	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
má	mít	k5eAaImIp3nS
méně	málo	k6eAd2
demaskující	demaskující	k2eAgInSc1d1
efekt	efekt	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
Symbolika	symbolika	k1gFnSc1
leteckých	letecký	k2eAgFnPc2d1
základen	základna	k1gFnPc2
Vzdušných	vzdušný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
AČR	AČR	kA
vychází	vycházet	k5eAaImIp3nS
ze	z	k7c2
znaků	znak	k1gInPc2
<g />
.	.	kIx.
</s>
<s hack="1">
leteckých	letecký	k2eAgInPc2d1
pluků	pluk	k1gInPc2
československé	československý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
Podobnost	podobnost	k1gFnSc1
s	s	k7c7
označením	označení	k1gNnSc7
z	z	k7c2
období	období	k1gNnSc2
první	první	k4xOgFnSc2
republiky	republika	k1gFnSc2
se	se	k3xPyFc4
projevuje	projevovat	k5eAaImIp3nS
také	také	k9
na	na	k7c6
znaku	znak	k1gInSc6
Správy	správa	k1gFnSc2
letiště	letiště	k1gNnSc2
Pardubice	Pardubice	k1gInPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
byla	být	k5eAaImAgFnS
tradiční	tradiční	k2eAgFnSc1d1
symbolika	symbolika	k1gFnSc1
skloubena	sklouben	k2eAgFnSc1d1
se	s	k7c7
symbolikou	symbolika	k1gFnSc7
typickou	typický	k2eAgFnSc7d1
pro	pro	k7c4
Pardubice	Pardubice	k1gInPc4
<g/>
,	,	kIx,
tj.	tj.	kA
přední	přední	k2eAgFnSc7d1
polovinou	polovina	k1gFnSc7
koně	kůň	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
počet	počet	k1gInSc4
tabulkových	tabulkový	k2eAgNnPc2d1
míst	místo	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
podle	podle	k7c2
armádního	armádní	k2eAgMnSc2d1
generála	generál	k1gMnSc2
Josefa	Josef	k1gMnSc2
Bečváře	Bečvář	k1gMnSc2
v	v	k7c6
srpnu	srpen	k1gInSc6
2016	#num#	k4
naplněna	naplnit	k5eAaPmNgFnS
ze	z	k7c2
79	#num#	k4
procent	procento	k1gNnPc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
NATO	NATO	kA
Airborne	Airborn	k1gInSc5
Early	earl	k1gMnPc4
Warning	Warning	k1gInSc4
&	&	k?
Control	Control	k1gInSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Letci	letec	k1gMnPc1
mají	mít	k5eAaImIp3nP
nového	nový	k2eAgMnSc4d1
velitele	velitel	k1gMnSc4
<g/>
,	,	kIx,
končícího	končící	k2eAgMnSc2d1
Štefánika	Štefánik	k1gMnSc2
vyprovázely	vyprovázet	k5eAaImAgFnP
gripeny	gripen	k2eAgFnPc1d1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-08-09	2016-08-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Velitelství	velitelství	k1gNnPc2
vzdušných	vzdušný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
acr	acr	k?
<g/>
.	.	kIx.
<g/>
army	arma	k1gMnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2015-02-05	2015-02-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
LETECKÝ	letecký	k2eAgInSc1d1
PORADNÍ	poradní	k2eAgInSc1d1
TÝM	tým	k1gInSc1
<g/>
,	,	kIx,
IRÁK	Irák	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
mise	mise	k1gFnSc2
<g/>
.	.	kIx.
<g/>
army	arma	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2018-05-03	2018-05-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Velení	velení	k1gNnSc4
Vzdušných	vzdušný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
převzal	převzít	k5eAaPmAgMnS
Mikulenka	Mikulenka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
přiletěl	přiletět	k5eAaPmAgMnS
s	s	k7c7
gripenem	gripeno	k1gNnSc7
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-11-02	2020-11-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Koncepce	koncepce	k1gFnSc1
výstavby	výstavba	k1gFnSc2
Armády	armáda	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
2025	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Armáda	armáda	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
Ministerstvo	ministerstvo	k1gNnSc1
obrany	obrana	k1gFnSc2
ČR	ČR	kA
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
3.2	3.2	k4
<g/>
.2	.2	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdušné	vzdušný	k2eAgFnPc4d1
síly	síla	k1gFnPc4
<g/>
,	,	kIx,
s.	s.	k?
14	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
HOTTMAR	HOTTMAR	kA
<g/>
,	,	kIx,
Aleš	Aleš	k1gMnSc1
<g/>
;	;	kIx,
ŠPALEK	špalek	k1gInSc1
<g/>
,	,	kIx,
Radim	Radim	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdušné	vzdušný	k2eAgFnPc4d1
síly	síla	k1gFnPc4
Armády	armáda	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
–	–	k?
100	#num#	k4
let	léto	k1gNnPc2
od	od	k7c2
svého	svůj	k3xOyFgInSc2
vzniku	vznik	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
czechairforce	czechairforka	k1gFnSc3
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2018-07-08	2018-07-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
SOUŠEK	souška	k1gFnPc2
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdušné	vzdušný	k2eAgFnPc4d1
síly	síla	k1gFnPc4
Armády	armáda	k1gFnSc2
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdušné	vzdušný	k2eAgFnPc4d1
síly	síla	k1gFnPc4
AČR	AČR	kA
-	-	kIx~
Ročenka	ročenka	k1gFnSc1
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srpen	srpen	k1gInSc1
2012	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
4	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
24	#num#	k4
<g/>
-	-	kIx~
<g/>
156	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ČADIL	čadit	k5eAaImAgMnS
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdušné	vzdušný	k2eAgFnPc4d1
síly	síla	k1gFnPc4
Armády	armáda	k1gFnSc2
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Den	den	k1gInSc1
otevřených	otevřený	k2eAgFnPc2d1
dveří	dveře	k1gFnPc2
letiště	letiště	k1gNnSc2
Čáslav	Čáslav	k1gFnSc1
2011	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
24	#num#	k4
<g/>
-	-	kIx~
<g/>
1156	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ČADIL	čadit	k5eAaImAgMnS
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdušné	vzdušný	k2eAgFnPc4d1
síly	síla	k1gFnPc4
AČR	AČR	kA
-	-	kIx~
Ročenka	ročenka	k1gFnSc1
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
MAGNET	magnet	k1gInSc1
PRESS	PRESS	kA
<g/>
,	,	kIx,
SLOVAKIA	SLOVAKIA	kA
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
89169	#num#	k4
<g/>
-	-	kIx~
<g/>
59	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
100	#num#	k4
let	léto	k1gNnPc2
letectva	letectvo	k1gNnSc2
<g/>
,	,	kIx,
s.	s.	k?
38	#num#	k4
<g/>
-	-	kIx~
<g/>
40	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
MACOUN	MACOUN	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgNnSc1d1
vojenské	vojenský	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Computer	computer	k1gInSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
80	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
251	#num#	k4
<g/>
-	-	kIx~
<g/>
1825	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Čeští	český	k2eAgMnPc1d1
váleční	váleční	k2eAgMnPc1d1
piloti	pilot	k1gMnPc1
1911	#num#	k4
<g/>
-	-	kIx~
<g/>
1918	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
5	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Československý	československý	k2eAgInSc1d1
letecký	letecký	k2eAgInSc1d1
a	a	k8xC
automobilní	automobilní	k2eAgInSc1d1
oddíl	oddíl	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
<g/>
air_war	air_war	k1gInSc1
<g/>
.	.	kIx.
<g/>
sweb	sweb	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
MACOUN	MACOUN	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgNnSc1d1
vojenské	vojenský	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Computer	computer	k1gInSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
251	#num#	k4
<g/>
-	-	kIx~
<g/>
1825	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
V	v	k7c6
boji	boj	k1gInSc6
za	za	k7c4
svobodu	svoboda	k1gFnSc4
a	a	k8xC
území	území	k1gNnSc4
1918	#num#	k4
<g/>
–	–	k?
<g/>
1921	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
RAJNINEC	RAJNINEC	kA
<g/>
,	,	kIx,
Juraj	Juraj	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovenské	slovenský	k2eAgInPc1d1
letectvo	letectvo	k1gNnSc1
1939	#num#	k4
<g/>
/	/	kIx~
<g/>
1944	#num#	k4
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
<g/>
:	:	kIx,
Magnet-Press	Magnet-Press	k1gInSc1
Slovakia	Slovakius	k1gMnSc2
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
158	#num#	k4
s.	s.	k?
↑	↑	k?
KŘEPELKOVÁ	KŘEPELKOVÁ	kA
<g/>
,	,	kIx,
Jitka	Jitka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Českoslovenští	československý	k2eAgMnPc1d1
letci	letec	k1gMnPc1
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
1940	#num#	k4
<g/>
-	-	kIx~
<g/>
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Muzeum	muzeum	k1gNnSc1
odboje	odboj	k1gInSc2
<g/>
,	,	kIx,
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
73	#num#	k4
s.	s.	k?
↑	↑	k?
Rok	rok	k1gInSc1
1993	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
army	army	k1gInPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2013-03-11	2013-03-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Nový	nový	k2eAgMnSc1d1
velitel	velitel	k1gMnSc1
Vzdušných	vzdušný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
AČR	AČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdušné	vzdušný	k2eAgFnPc4d1
síly	síla	k1gFnPc4
AČR	AČR	kA
-	-	kIx~
Ročenka	ročenka	k1gFnSc1
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
22	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
24	#num#	k4
<g/>
-	-	kIx~
<g/>
156	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
FEDORKOVÁ	FEDORKOVÁ	kA
<g/>
,	,	kIx,
Jolana	Jolana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Olomouci	Olomouc	k1gFnSc6
se	se	k3xPyFc4
s	s	k7c7
vojáky	voják	k1gMnPc7
rozloučil	rozloučit	k5eAaPmAgMnS
velitel	velitel	k1gMnSc1
vzdušných	vzdušný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
www.acr.army.cz	www.acr.army.cz	k1gMnSc1
<g/>
,	,	kIx,
2013-05-06	2013-05-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
FEDORKOVÁ	FEDORKOVÁ	kA
<g/>
,	,	kIx,
Jolana	Jolana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tmavomodrý	tmavomodrý	k2eAgInSc1d1
svět	svět	k1gInSc1
plukovníka	plukovník	k1gMnSc2
Štefánika	Štefánik	k1gMnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
www.acr.army.cz	www.acr.army.cz	k1gMnSc1
<g/>
,	,	kIx,
2013-06-18	2013-06-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
DANDA	Danda	k1gMnSc1
<g/>
,	,	kIx,
Oldřich	Oldřich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pilot	pilota	k1gFnPc2
gripenu	gripen	k2eAgFnSc4d1
<g/>
:	:	kIx,
Nad	nad	k7c7
Islandem	Island	k1gInSc7
jsme	být	k5eAaImIp1nP
létali	létat	k5eAaImAgMnP
nízko	nízko	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
neměl	mít	k5eNaImAgMnS
si	se	k3xPyFc3
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
stěžovat	stěžovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-09-03	2015-09-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
MARUŠČÁK	MARUŠČÁK	kA
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
gripenech	gripeno	k1gNnPc6
dorazili	dorazit	k5eAaPmAgMnP
na	na	k7c4
Island	Island	k1gInSc4
i	i	k9
zbylí	zbylý	k2eAgMnPc1d1
příslušníci	příslušník	k1gMnPc1
mise	mise	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
mise	mise	k1gFnSc2
<g/>
.	.	kIx.
<g/>
army	arma	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2015-07-24	2015-07-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
ČADIL	čadit	k5eAaImAgMnS
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdušné	vzdušný	k2eAgFnPc4d1
síly	síla	k1gFnPc4
AČR	AČR	kA
-	-	kIx~
Ročenka	ročenka	k1gFnSc1
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
<g/>
:	:	kIx,
Magnet	magnet	k1gInSc1
Press	Press	k1gInSc1
<g/>
,	,	kIx,
Slovakia	Slovakia	k1gFnSc1
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
89169	#num#	k4
<g/>
-	-	kIx~
<g/>
66	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
4	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
ČADIL	čadit	k5eAaImAgMnS
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdušné	vzdušný	k2eAgFnPc4d1
síly	síla	k1gFnPc4
AČR	AČR	kA
-	-	kIx~
Ročenka	ročenka	k1gFnSc1
2015	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
MAGNET	magnet	k1gInSc1
PRESS	PRESS	kA
<g/>
,	,	kIx,
SLOVAKIA	SLOVAKIA	kA
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
89169	#num#	k4
<g/>
-	-	kIx~
<g/>
33	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Vzdušné	vzdušný	k2eAgFnSc2d1
síly	síla	k1gFnSc2
Armády	armáda	k1gFnSc2
ČR	ČR	kA
<g/>
,	,	kIx,
s.	s.	k?
4	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
SOUŠEK	souška	k1gFnPc2
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozpočet	rozpočet	k1gInSc1
příliš	příliš	k6eAd1
možností	možnost	k1gFnSc7
nedává	dávat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdušné	vzdušný	k2eAgFnPc4d1
síly	síla	k1gFnPc4
AČR	AČR	kA
-	-	kIx~
Ročenka	ročenka	k1gFnSc1
2011	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
30	#num#	k4
<g/>
-	-	kIx~
<g/>
34	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Armáda	armáda	k1gFnSc1
chce	chtít	k5eAaImIp3nS
používat	používat	k5eAaImF
letiště	letiště	k1gNnSc4
v	v	k7c6
Přerově	Přerov	k1gInSc6
jako	jako	k8xC,k8xS
vojenské	vojenský	k2eAgFnPc4d1
záložní	záložní	k2eAgNnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-05-18	2016-05-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Na	na	k7c4
karlovarské	karlovarský	k2eAgNnSc4d1
letiště	letiště	k1gNnSc4
se	se	k3xPyFc4
po	po	k7c6
desítkách	desítka	k1gFnPc6
let	léto	k1gNnPc2
vrací	vracet	k5eAaImIp3nS
armáda	armáda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pilotinfo	Pilotinfo	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-10-10	2017-10-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
LZS	LZS	kA
letiště	letiště	k1gNnSc2
Plzeň-Líně	Plzeň-Lín	k1gInSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
30.11	30.11	k4
<g/>
.16	.16	k4
<g/>
:	:	kIx,
Prezentace	prezentace	k1gFnSc1
připravenosti	připravenost	k1gFnSc2
Letecké	letecký	k2eAgFnSc2d1
záchranné	záchranný	k2eAgFnSc2d1
služby	služba	k1gFnSc2
AČR	AČR	kA
pro	pro	k7c4
Jihočeský	jihočeský	k2eAgInSc4d1
kraj	kraj	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Generální	generální	k2eAgInSc1d1
štáb	štáb	k1gInSc1
AČR	AČR	kA
<g/>
,	,	kIx,
2016-11-30	2016-11-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Předpis	předpis	k1gInSc1
č.	č.	k?
80	#num#	k4
<g/>
/	/	kIx~
<g/>
2004	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
m.	m.	k?
s.	s.	k?
ze	z	k7c2
dne	den	k1gInSc2
16	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2004	#num#	k4
<g/>
,	,	kIx,
Memorandum	memorandum	k1gNnSc1
o	o	k7c6
porozumění	porozumění	k1gNnSc6
mezi	mezi	k7c7
vládou	vláda	k1gFnSc7
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
vládou	vláda	k1gFnSc7
Švédského	švédský	k2eAgNnSc2d1
království	království	k1gNnSc2
o	o	k7c6
použití	použití	k1gNnSc6
JAS	jas	k1gInSc1
39	#num#	k4
GRIPEN	GRIPEN	kA
Českou	český	k2eAgFnSc7d1
republikou	republika	k1gFnSc7
<g/>
,	,	kIx,
Článek	článek	k1gInSc1
3	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SOUŠEK	souška	k1gFnPc2
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Litening	Litening	k1gInSc1
4	#num#	k4
<g/>
i	i	k8xC
pro	pro	k7c4
české	český	k2eAgFnPc4d1
Gripeny	Gripen	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
L	L	kA
<g/>
+	+	kIx~
<g/>
K.	K.	kA
2018	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
94	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
11	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
12	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
24	#num#	k4
<g/>
-	-	kIx~
<g/>
1156	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
HOTTMAR	HOTTMAR	kA
<g/>
,	,	kIx,
Aleš	Aleš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Modernizace	modernizace	k1gFnSc1
letounů	letoun	k1gInPc2
L-159A	L-159A	k1gFnSc4
pokračuje	pokračovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Czech	Czech	k1gInSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-12-27	2017-12-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČADIL	čadit	k5eAaImAgMnS
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Modernizace	modernizace	k1gFnPc1
i	i	k8xC
oslavy	oslava	k1gFnPc1
-	-	kIx~
Čáslavská	Čáslavská	k1gFnSc1
21	#num#	k4
<g/>
.	.	kIx.
základna	základna	k1gFnSc1
taktického	taktický	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
L	L	kA
<g/>
+	+	kIx~
<g/>
K.	K.	kA
2017	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
93	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
12	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
14	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
24	#num#	k4
<g/>
-	-	kIx~
<g/>
1156	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
SOUŠEK	souška	k1gFnPc2
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Labutí	labutí	k2eAgFnSc1d1
píseň	píseň	k1gFnSc1
pro	pro	k7c4
L-39	L-39	k1gFnSc4
Albatros	albatros	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
ČADIL	čadit	k5eAaImAgMnS
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdušné	vzdušný	k2eAgFnPc4d1
síly	síla	k1gFnPc4
AČR	AČR	kA
-	-	kIx~
Ročenka	ročenka	k1gFnSc1
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Aeromedia	Aeromedium	k1gNnPc4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
10	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
LANG	Lang	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nová	nový	k2eAgFnSc1d1
letka	letka	k1gFnSc1
pro	pro	k7c4
výcvik	výcvik	k1gInSc4
taktického	taktický	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
se	se	k3xPyFc4
zabydluje	zabydlovat	k5eAaImIp3nS
v	v	k7c6
Čáslavi	Čáslav	k1gFnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
acr	acr	k?
<g/>
.	.	kIx.
<g/>
army	arma	k1gMnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2014-05-06	2014-05-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Vzdušné	vzdušný	k2eAgFnPc1d1
síly	síla	k1gFnPc1
AČR	AČR	kA
získaly	získat	k5eAaPmAgFnP
tři	tři	k4xCgFnPc4
L-159T2	L-159T2	k1gFnPc4
a	a	k8xC
vyřazují	vyřazovat	k5eAaImIp3nP
L-39ZA	L-39ZA	k1gMnSc1
Albatros	albatros	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ATM	ATM	kA
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
51	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
7	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
60	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1802	#num#	k4
<g/>
-	-	kIx~
<g/>
4823	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Konec	konec	k1gInSc1
L-39ZA	L-39ZA	k1gMnSc1
Albatros	albatros	k1gMnSc1
ve	v	k7c6
Vzdušných	vzdušný	k2eAgFnPc6d1
silách	síla	k1gFnPc6
AČR	AČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
ATM	ATM	kA
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
51	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
9	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
58	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1802	#num#	k4
<g/>
-	-	kIx~
<g/>
4823	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Jakovlev	Jakovlev	k1gFnPc2
Jak-	Jak-	k1gFnSc4
<g/>
40	#num#	k4
ve	v	k7c6
Vzdušných	vzdušný	k2eAgFnPc6d1
silách	síla	k1gFnPc6
AČR	AČR	kA
dolétal	dolétat	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ATM	ATM	kA
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
52	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
10	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
58	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1802	#num#	k4
<g/>
-	-	kIx~
<g/>
4823	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Koncepce	koncepce	k1gFnSc1
výstavby	výstavba	k1gFnSc2
Armády	armáda	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
2025	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ministerstvo	ministerstvo	k1gNnSc1
obrany	obrana	k1gFnSc2
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
30	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
3.2	3.2	k4
<g/>
.2	.2	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdušné	vzdušný	k2eAgFnPc4d1
síly	síla	k1gFnPc4
<g/>
,	,	kIx,
s.	s.	k?
14	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
MAREK	marka	k1gFnPc2
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prioritou	priorita	k1gFnSc7
armády	armáda	k1gFnSc2
jsou	být	k5eAaImIp3nP
projekty	projekt	k1gInPc1
okamžitého	okamžitý	k2eAgInSc2d1
dopadu	dopad	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
výstavba	výstavba	k1gFnSc1
sil	síla	k1gFnPc2
a	a	k8xC
nábor	nábor	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
acr	acr	k?
<g/>
.	.	kIx.
<g/>
army	arma	k1gMnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2018-06-18	2018-06-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
HOTTMAR	HOTTMAR	kA
<g/>
,	,	kIx,
Aleš	Aleš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kbelská	kbelský	k2eAgFnSc1d1
„	„	k?
<g/>
dopravka	dopravka	k1gFnSc1
<g/>
“	“	k?
zřejmě	zřejmě	k6eAd1
získá	získat	k5eAaPmIp3nS
nový	nový	k2eAgInSc4d1
stroj	stroj	k1gInSc4
pro	pro	k7c4
přepravu	přeprava	k1gFnSc4
ústavních	ústavní	k2eAgMnPc2d1
a	a	k8xC
vládních	vládní	k2eAgMnPc2d1
činitelů	činitel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Czech	Czech	k1gInSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-08-11	2019-08-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
http://czechairforce.com/news/kbelska-dopravka-zrejme-ziska-novy-stroj-pro-prepravu-ustavnich-a-vladnich-cinitelu/.	http://czechairforce.com/news/kbelska-dopravka-zrejme-ziska-novy-stroj-pro-prepravu-ustavnich-a-vladnich-cinitelu/.	k?
1	#num#	k4
2	#num#	k4
SÝKORA	Sýkora	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zástupci	zástupce	k1gMnPc1
ministerstva	ministerstvo	k1gNnSc2
obrany	obrana	k1gFnSc2
podepsali	podepsat	k5eAaPmAgMnP
smlouvu	smlouva	k1gFnSc4
na	na	k7c4
dva	dva	k4xCgInPc4
nové	nový	k2eAgInPc4d1
letouny	letoun	k1gInPc4
CASA	CASA	kA
a	a	k8xC
modernizaci	modernizace	k1gFnSc4
čtyř	čtyři	k4xCgInPc2
stávajících	stávající	k2eAgInPc2d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
mocr	mocr	k1gInSc1
<g/>
.	.	kIx.
<g/>
army	arma	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2019-12-16	2019-12-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČADIL	čadit	k5eAaImAgMnS
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdušné	vzdušný	k2eAgFnPc4d1
síly	síla	k1gFnPc4
AČR	AČR	kA
-	-	kIx~
Ročenka	ročenka	k1gFnSc1
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
MAGNET	magnet	k1gInSc1
PRESS	PRESS	kA
<g/>
,	,	kIx,
SLOVAKIA	SLOVAKIA	kA
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
redakcí	redakce	k1gFnSc7
L	L	kA
<g/>
+	+	kIx~
<g/>
K	K	kA
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
89169	#num#	k4
<g/>
-	-	kIx~
<g/>
33	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
L-410	L-410	k1gFnSc1
znovu	znovu	k6eAd1
pro	pro	k7c4
výsadky	výsadka	k1gFnPc4
<g/>
,	,	kIx,
s.	s.	k?
40	#num#	k4
<g/>
-	-	kIx~
<g/>
44	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
SOUŠEK	souška	k1gFnPc2
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Provoz	provoz	k1gInSc1
Mi-	Mi-	k1gFnSc2
<g/>
24	#num#	k4
v	v	k7c6
AČR	AČR	kA
zajištěn	zajistit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
L	L	kA
<g/>
+	+	kIx~
<g/>
K.	K.	kA
2018	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
94	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
30	#num#	k4
<g/>
-	-	kIx~
<g/>
33	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
24	#num#	k4
<g/>
-	-	kIx~
<g/>
1156	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Letecký	letecký	k2eAgInSc1d1
odřad	odřad	k1gInSc1
speciálních	speciální	k2eAgFnPc2d1
operací	operace	k1gFnPc2
(	(	kIx(
<g/>
LOSO	LOSO	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ředitelství	ředitelství	k1gNnSc1
speciálních	speciální	k2eAgFnPc2d1
sil	síla	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
HOTTMAR	HOTTMAR	kA
<g/>
,	,	kIx,
Aleš	Aleš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrtulníky	vrtulník	k1gInPc1
Mi-	Mi-	k1gMnSc1
<g/>
171	#num#	k4
<g/>
ŠM	ŠM	kA
určené	určený	k2eAgInPc4d1
pro	pro	k7c4
SOATU	SOATU	kA
čeká	čekat	k5eAaImIp3nS
další	další	k2eAgFnSc1d1
etapa	etapa	k1gFnSc1
modernizace	modernizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Czech	Czech	k1gInSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
HOTTMAR	HOTTMAR	kA
<g/>
,	,	kIx,
Aleš	Aleš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Armáda	armáda	k1gFnSc1
nakonec	nakonec	k6eAd1
koupí	koupit	k5eAaPmIp3nP
americké	americký	k2eAgInPc1d1
vrtulníky	vrtulník	k1gInPc1
UH-1Y	UH-1Y	k1gFnSc1
Venom	Venom	k1gInSc1
a	a	k8xC
AH-1Z	AH-1Z	k1gMnSc1
Viper	Viper	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Czech	Czech	k1gInSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-08-22	2019-08-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Současnost	současnost	k1gFnSc4
Letecké	letecký	k2eAgFnSc2d1
záchranné	záchranný	k2eAgFnSc2d1
služby	služba	k1gFnSc2
v	v	k7c6
Západočeském	západočeský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
LZS	LZS	kA
letiště	letiště	k1gNnSc2
Plzeň-Líně	Plzeň-Lín	k1gInSc6
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
LANG	Lang	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrtulníková	vrtulníkový	k2eAgFnSc1d1
letka	letka	k1gFnSc1
s	s	k7c7
osobitou	osobitý	k2eAgFnSc7d1
pozicí	pozice	k1gFnSc7
ve	v	k7c6
Vzdušných	vzdušný	k2eAgFnPc6d1
silách	síla	k1gFnPc6
Armády	armáda	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
acr	acr	k?
<g/>
.	.	kIx.
<g/>
army	arma	k1gMnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2013-12-18	2013-12-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
LOM	lom	k1gInSc1
získal	získat	k5eAaPmAgInS
armádní	armádní	k2eAgFnSc4d1
zakázku	zakázka	k1gFnSc4
na	na	k7c4
modernizaci	modernizace	k1gFnSc4
pěti	pět	k4xCc2
vrtulníků	vrtulník	k1gInPc2
za	za	k7c4
168	#num#	k4
milionů	milion	k4xCgInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
E	E	kA
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-10-04	2016-10-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KRÁTKÝ	Krátký	k1gMnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protivzdušná	protivzdušný	k2eAgFnSc1d1
obrana	obrana	k1gFnSc1
–	–	k?
20	#num#	k4
let	léto	k1gNnPc2
od	od	k7c2
rozdělení	rozdělení	k1gNnSc2
Československa	Československo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojenské	vojenský	k2eAgInPc1d1
rozhledy	rozhled	k1gInPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
2336	#num#	k4
<g/>
-	-	kIx~
<g/>
2995	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Dělostřelecká	dělostřelecký	k2eAgFnSc1d1
a	a	k8xC
PVO	PVO	kA
technika	technika	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
acr	acr	k?
<g/>
.	.	kIx.
<g/>
army	arma	k1gMnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
PETERKA	Peterka	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strakoničtí	strakonický	k2eAgMnPc1d1
rakeťáci	rakeťák	k1gMnPc1
odstříleli	odstřílet	k5eAaPmAgMnP
navzdory	navzdory	k7c3
nepříznivému	příznivý	k2eNgNnSc3d1
počasí	počasí	k1gNnSc3
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
acr	acr	k?
<g/>
.	.	kIx.
<g/>
army	arma	k1gMnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2015-06-12	2015-06-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
GROHMANN	GROHMANN	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Modernizace	modernizace	k1gFnSc1
PVO	PVO	kA
Armády	armáda	k1gFnSc2
ČR	ČR	kA
<g/>
:	:	kIx,
V	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
se	se	k3xPyFc4
vybere	vybrat	k5eAaPmIp3nS
vítěz	vítěz	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Armádní	armádní	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-11-16	2016-11-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SAMCOVÁ	SAMCOVÁ	kA
<g/>
,	,	kIx,
Jana	Jana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strakoničtí	strakonický	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
poprvé	poprvé	k6eAd1
stříleli	střílet	k5eAaImAgMnP
z	z	k7c2
protiletadlového	protiletadlový	k2eAgInSc2d1
kompletu	komplet	k1gInSc2
nové	nový	k2eAgFnSc2d1
generace	generace	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
acr	acr	k?
<g/>
.	.	kIx.
<g/>
army	arma	k1gMnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2015-10-29	2015-10-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
GROHMANN	GROHMANN	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Armáda	armáda	k1gFnSc1
ČR	ČR	kA
získá	získat	k5eAaPmIp3nS
izraelské	izraelský	k2eAgInPc4d1
protivzdušné	protivzdušný	k2eAgInPc4d1
systémy	systém	k1gInPc4
SPYDER	SPYDER	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Armádní	armádní	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2020-09-25	2020-09-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Koncepce	koncepce	k1gFnSc1
výstavby	výstavba	k1gFnSc2
Armády	armáda	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
2025	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ministerstvo	ministerstvo	k1gNnSc1
obrany	obrana	k1gFnSc2
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
30	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
3.2	3.2	k4
<g/>
.2	.2	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdušné	vzdušný	k2eAgFnPc4d1
síly	síla	k1gFnPc4
<g/>
,	,	kIx,
s.	s.	k?
15	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
SOUŠEK	souška	k1gFnPc2
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radary	radar	k1gInPc7
protivzdušné	protivzdušný	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdušné	vzdušný	k2eAgFnPc4d1
síly	síla	k1gFnPc4
AČR	AČR	kA
-	-	kIx~
Ročenka	ročenka	k1gFnSc1
2013	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
46	#num#	k4
<g/>
-	-	kIx~
<g/>
50	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
24	#num#	k4
<g/>
-	-	kIx~
<g/>
156	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
VISINGR	VISINGR	kA
<g/>
,	,	kIx,
Lukáš	Lukáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protivzdušná	protivzdušný	k2eAgFnSc1d1
obrana	obrana	k1gFnSc1
<g/>
:	:	kIx,
nezbytná	zbytný	k2eNgFnSc1d1,k2eAgFnSc1d1
součást	součást	k1gFnSc1
suverenity	suverenita	k1gFnSc2
a	a	k8xC
bezpečnosti	bezpečnost	k1gFnSc2
země	zem	k1gFnSc2
<g/>
.	.	kIx.
natoaktual	natoaktual	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jagello	Jagello	k1gNnSc1
2000	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ZDOBINSKÝ	ZDOBINSKÝ	kA
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nové	k2eAgInPc1d1
programy	program	k1gInPc1
pro	pro	k7c4
AČR	AČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
ATM	ATM	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
47	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1802	#num#	k4
<g/>
-	-	kIx~
<g/>
4823	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
SOUŠEK	souška	k1gFnPc2
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radary	radar	k1gInPc1
MADR	MADR	kA
již	již	k6eAd1
brzy	brzy	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ATM	ATM	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
48	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
/	/	kIx~
<g/>
2016	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1802	#num#	k4
<g/>
-	-	kIx~
<g/>
4823	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
13.07	13.07	k4
<g/>
.16	.16	k4
<g/>
:	:	kIx,
TZ	TZ	kA
–	–	k?
na	na	k7c4
žádost	žádost	k1gFnSc4
uchazečů	uchazeč	k1gMnPc2
se	se	k3xPyFc4
lhůta	lhůta	k1gFnSc1
pro	pro	k7c4
podání	podání	k1gNnSc4
nabídek	nabídka	k1gFnPc2
na	na	k7c4
radiolokátory	radiolokátor	k1gInPc4
MADR	MADR	kA
posouvá	posouvat	k5eAaImIp3nS
do	do	k7c2
poloviny	polovina	k1gFnSc2
srpna	srpen	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tiskové	tiskový	k2eAgFnPc4d1
oddělení	oddělení	k1gNnPc4
MO	MO	kA
<g/>
,	,	kIx,
2016-07-13	2016-07-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Radary	radar	k1gInPc4
za	za	k7c2
miliardy	miliarda	k4xCgFnSc2
chtějí	chtít	k5eAaImIp3nP
armádě	armáda	k1gFnSc3
dodat	dodat	k5eAaPmF
Francie	Francie	k1gFnPc4
<g/>
,	,	kIx,
Izrael	Izrael	k1gInSc1
a	a	k8xC
Švédsko	Švédsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-08-18	2016-08-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
OTTO	Otto	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
3D	3D	k4
radary	radar	k1gInPc7
dodají	dodat	k5eAaPmIp3nP
Česku	Česko	k1gNnSc6
Izraelci	Izraelec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
E15	E15	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-11-30	2016-11-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
STRATILÍK	STRATILÍK	kA
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rok	rok	k1gInSc1
bez	bez	k7c2
radaru	radar	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
se	se	k3xPyFc4
zastavila	zastavit	k5eAaPmAgFnS
pětimiliardová	pětimiliardový	k2eAgFnSc1d1
zakázka	zakázka	k1gFnSc1
<g/>
.	.	kIx.
euro	euro	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
2017-12-14	2017-12-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
INC	INC	kA
NATOAKTUAL	NATOAKTUAL	kA
<g/>
.	.	kIx.
<g/>
CZ	CZ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radary	radar	k1gInPc7
z	z	k7c2
Izraele	Izrael	k1gInSc2
nesmíte	smět	k5eNaImIp2nP
zapojit	zapojit	k5eAaPmF
do	do	k7c2
systému	systém	k1gInSc2
NATO	NATO	kA
<g/>
,	,	kIx,
vzkázal	vzkázat	k5eAaPmAgInS
výbor	výbor	k1gInSc1
Aliance	aliance	k1gFnSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2018-06-08	2018-06-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
PEJŠEK	PEJŠEK	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vláda	vláda	k1gFnSc1
schválila	schválit	k5eAaPmAgFnS
Koncepci	koncepce	k1gFnSc4
dopravního	dopravní	k2eAgNnSc2d1
a	a	k8xC
vrtulníkového	vrtulníkový	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
www.mocr.army.cz/,	www.mocr.army.cz/,	k?
2013-11-27	2013-11-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
HOLE	hole	k1gFnSc1
<g/>
,	,	kIx,
Craig	Craig	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Czech	Czech	k1gMnSc1
Republic	Republice	k1gFnPc2
outlines	outlines	k1gMnSc1
need	need	k1gMnSc1
for	forum	k1gNnPc2
two	two	k?
KC-	KC-	k1gFnSc2
<g/>
390	#num#	k4
<g/>
s	s	k7c7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Flightglobal	Flightglobal	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2010-09-14	2010-09-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KC-390	KC-390	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aero	aero	k1gNnSc1
Vodochody	Vodochod	k1gInPc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Stropnický	stropnický	k2eAgInSc1d1
v	v	k7c6
Brazílii	Brazílie	k1gFnSc6
jednal	jednat	k5eAaImAgMnS
o	o	k7c6
spolupráci	spolupráce	k1gFnSc6
při	při	k7c6
výcviku	výcvik	k1gInSc6
pilotů	pilot	k1gMnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
2015-04-15	2015-04-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČADIL	čadit	k5eAaImAgMnS
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdušné	vzdušný	k2eAgFnPc4d1
síly	síla	k1gFnPc4
AČR	AČR	kA
-	-	kIx~
Ročenka	ročenka	k1gFnSc1
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
MAGNET	magnet	k1gInSc1
PRESS	PRESS	kA
<g/>
,	,	kIx,
SLOVAKIA	SLOVAKIA	kA
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
89169	#num#	k4
<g/>
-	-	kIx~
<g/>
59	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Letectvo	letectvo	k1gNnSc4
směřuje	směřovat	k5eAaImIp3nS
k	k	k7c3
omlazení	omlazení	k1gNnSc3
<g/>
,	,	kIx,
s.	s.	k?
16	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Armáda	armáda	k1gFnSc1
obnoví	obnovit	k5eAaPmIp3nS
letectvo	letectvo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakoupí	nakoupit	k5eAaPmIp3nP
nové	nový	k2eAgInPc1d1
vrtulníky	vrtulník	k1gInPc1
a	a	k8xC
další	další	k2eAgInSc1d1
Airbus	airbus	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
zpravy	zprava	k1gFnPc1
<g/>
.	.	kIx.
<g/>
e	e	k0
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
/	/	kIx~
<g/>
,	,	kIx,
2013-11-27	2013-11-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ŠNÍDL	ŠNÍDL	kA
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stropnického	stropnický	k2eAgInSc2d1
první	první	k4xOgFnPc4
úspory	úspora	k1gFnPc4
<g/>
:	:	kIx,
Začnu	začít	k5eAaPmIp1nS
létat	létat	k5eAaImF
běžnou	běžný	k2eAgFnSc7d1
linkou	linka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebo	nebo	k8xC
nepohodlným	pohodlný	k2eNgMnSc7d1
strojem	stroj	k1gInSc7
CASA	CASA	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
zpravy	zprava	k1gFnPc4
<g/>
.	.	kIx.
<g/>
ihned	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2014-02-19	2014-02-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ŠNÍDL	ŠNÍDL	kA
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dva	dva	k4xCgInPc1
vládní	vládní	k2eAgInPc1d1
airbusy	airbus	k1gInPc1
nestačí	stačit	k5eNaBmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ministr	ministr	k1gMnSc1
Stropnický	stropnický	k2eAgMnSc1d1
zvažuje	zvažovat	k5eAaImIp3nS
nákup	nákup	k1gInSc4
třetího	třetí	k4xOgNnSc2
letadla	letadlo	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
http://zpravy.ihned.cz/,	http://zpravy.ihned.cz/,	k?
2014-07-24	2014-07-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
MENSCHIK	MENSCHIK	kA
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
;	;	kIx,
CECHL	CECHL	kA
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Armáda	armáda	k1gFnSc1
bez	bez	k7c2
letadel	letadlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soukromníkům	soukromník	k1gMnPc3
zaplatí	zaplatit	k5eAaPmIp3nS
až	až	k6eAd1
sedm	sedm	k4xCc4
miliard	miliarda	k4xCgFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
tyden	tyden	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2014-02-27	2014-02-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Koncepce	koncepce	k1gFnSc1
výstavby	výstavba	k1gFnSc2
Armády	armáda	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
2025	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ministerstvo	ministerstvo	k1gNnSc1
obrany	obrana	k1gFnSc2
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
30	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
GROHMANN	GROHMANN	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klíčové	klíčový	k2eAgInPc4d1
modernizační	modernizační	k2eAgInPc4d1
projekty	projekt	k1gInPc4
Armády	armáda	k1gFnSc2
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Armádní	armádní	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-06-19	2018-06-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
MARUŠČÁK	MARUŠČÁK	kA
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letectvo	letectvo	k1gNnSc1
Armády	armáda	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
čekají	čekat	k5eAaImIp3nP
velké	velký	k2eAgFnPc1d1
změny	změna	k1gFnPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
acr	acr	k?
<g/>
.	.	kIx.
<g/>
army	arma	k1gMnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2015-12-04	2015-12-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SOUŠEK	souška	k1gFnPc2
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výcvik	výcvik	k1gInSc1
v	v	k7c6
CLV	CLV	kA
Pardubice	Pardubice	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
ČADIL	čadit	k5eAaImAgMnS
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdušné	vzdušný	k2eAgFnPc4d1
síly	síla	k1gFnPc4
AČR	AČR	kA
-	-	kIx~
Ročenka	ročenka	k1gFnSc1
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Aeromedia	Aeromedium	k1gNnPc4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
24	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
SOUŠEK	souška	k1gFnPc2
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgNnSc1d1
střídání	střídání	k1gNnSc1
u	u	k7c2
AWACSu	AWACSus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
ČADIL	čadit	k5eAaImAgMnS
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdušné	vzdušný	k2eAgFnPc4d1
síly	síla	k1gFnPc4
AČR	AČR	kA
-	-	kIx~
Ročenka	ročenka	k1gFnSc1
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Magnet	magnet	k1gInSc1
Press	Pressa	k1gFnPc2
Slovakia	Slovakium	k1gNnSc2
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
redakcí	redakce	k1gFnSc7
L	L	kA
<g/>
+	+	kIx~
<g/>
K	K	kA
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
89169	#num#	k4
<g/>
-	-	kIx~
<g/>
33	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
22	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Opět	opět	k6eAd1
úspěšně	úspěšně	k6eAd1
–	–	k?
podruhé	podruhé	k6eAd1
v	v	k7c6
Pobaltí	Pobaltí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdušné	vzdušný	k2eAgFnPc4d1
síly	síla	k1gFnPc4
AČR	AČR	kA
–	–	k?
Ročenka	ročenka	k1gFnSc1
2013	#num#	k4
<g/>
,	,	kIx,
speciál	speciál	k1gInSc1
časopisu	časopis	k1gInSc2
Letectví	letectví	k1gNnSc2
<g/>
+	+	kIx~
<g/>
kosmonautika	kosmonautika	k1gFnSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
40	#num#	k4
<g/>
-	-	kIx~
<g/>
44	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
24	#num#	k4
<g/>
-	-	kIx~
<g/>
156	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
SOUŠEK	souška	k1gFnPc2
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kateřina	Kateřina	k1gFnSc1
Hlavsová	Hlavsová	k1gFnSc1
létá	létat	k5eAaImIp3nS
na	na	k7c4
L-159	L-159	k1gFnSc4
Alca	Alc	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
ČADIL	čadit	k5eAaImAgMnS
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdušné	vzdušný	k2eAgFnPc4d1
síly	síla	k1gFnPc4
AČR	AČR	kA
-	-	kIx~
Ročenka	ročenka	k1gFnSc1
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Aeromedia	Aeromedium	k1gNnPc4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
14	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
FEDORKOVÁ	FEDORKOVÁ	kA
<g/>
,	,	kIx,
Jolana	Jolana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
ženách	žena	k1gFnPc6
v	v	k7c6
letectvu	letectvo	k1gNnSc6
se	se	k3xPyFc4
bude	být	k5eAaImBp3nS
mluvit	mluvit	k5eAaImF
v	v	k7c6
Národním	národní	k2eAgNnSc6d1
technickém	technický	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
acr	acr	k?
<g/>
.	.	kIx.
<g/>
army	arma	k1gMnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2012-10-26	2012-10-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Pilot	pilot	k1gMnSc1
stroje	stroj	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
poškodila	poškodit	k5eAaPmAgFnS
srážka	srážka	k1gFnSc1
se	s	k7c7
supem	sup	k1gMnSc7
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
vyznamenán	vyznamenat	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
VIDEO	video	k1gNnSc1
<g/>
:	:	kIx,
Kamera	kamera	k1gFnSc1
v	v	k7c6
kokpitu	kokpit	k1gInSc6
zachytila	zachytit	k5eAaPmAgFnS
srážku	srážka	k1gFnSc4
českého	český	k2eAgInSc2d1
bitevníku	bitevník	k1gInSc2
se	s	k7c7
supem	sup	k1gMnSc7
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-11-24	2015-11-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KOLLER	KOLLER	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Označování	označování	k1gNnSc1
letadel	letadlo	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yRgNnPc1,k3yQgNnPc1
mohou	moct	k5eAaImIp3nP
působit	působit	k5eAaImF
v	v	k7c6
prostoru	prostor	k1gInSc6
konfliktu	konflikt	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
acr	acr	k?
<g/>
.	.	kIx.
<g/>
army	arma	k1gMnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2011-03-24	2011-03-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SEDLÁČEK	Sedláček	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
SYMBOLY	symbol	k1gInPc1
REPUBLIKY	republika	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úřad	úřad	k1gInSc1
vlády	vláda	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
MACOUN	MACOUN	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgNnSc1d1
vojenské	vojenský	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Computer	computer	k1gInSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
251	#num#	k4
<g/>
-	-	kIx~
<g/>
1825	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
6	#num#	k4
<g/>
,	,	kIx,
17	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
MAREK	Marek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
24	#num#	k4
<g/>
.	.	kIx.
základna	základna	k1gFnSc1
dopravního	dopravní	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
„	„	k?
<g/>
T.	T.	kA
G.	G.	kA
Masaryka	Masaryk	k1gMnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
report	report	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
11	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Rukávové	rukávový	k2eAgInPc4d1
znaky	znak	k1gInPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
VHU	VHU	kA
Praha	Praha	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Československé	československý	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
</s>
<s>
Vzdušné	vzdušný	k2eAgFnPc1d1
síly	síla	k1gFnPc1
Slovenské	slovenský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Letecké	letecký	k2eAgFnPc1d1
nehody	nehoda	k1gFnPc1
vojenských	vojenský	k2eAgInPc2d1
strojů	stroj	k1gInPc2
České	český	k2eAgFnSc2d1
a	a	k8xC
Československé	československý	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Letecká	letecký	k2eAgFnSc1d1
služba	služba	k1gFnSc1
Federálního	federální	k2eAgInSc2d1
policejního	policejní	k2eAgInSc2d1
sboru	sbor	k1gInSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Vzdušné	vzdušný	k2eAgFnSc2d1
síly	síla	k1gFnSc2
Armády	armáda	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Stránky	stránka	k1gFnPc1
o	o	k7c6
Vzdušných	vzdušný	k2eAgFnPc6d1
silách	síla	k1gFnPc6
AČR	AČR	kA
a	a	k8xC
historii	historie	k1gFnSc6
českého	český	k2eAgNnSc2d1
a	a	k8xC
československého	československý	k2eAgNnSc2d1
vojenského	vojenský	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
základna	základna	k1gFnSc1
taktického	taktický	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
Čáslav	Čáslav	k1gFnSc1
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
základna	základna	k1gFnSc1
vrtulníkového	vrtulníkový	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
Sedlec	Sedlec	k1gInSc1
<g/>
,	,	kIx,
Vícenice	Vícenice	k1gFnSc1
u	u	k7c2
Náměště	Náměšť	k1gFnSc2
nad	nad	k7c7
Oslavou	oslava	k1gFnSc7
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
základna	základna	k1gFnSc1
dopravního	dopravní	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
Praha	Praha	k1gFnSc1
-	-	kIx~
Kbely	Kbely	k1gInPc1
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
protiletadlový	protiletadlový	k2eAgInSc1d1
raketový	raketový	k2eAgInSc1d1
pluk	pluk	k1gInSc1
Strakonice	Strakonice	k1gFnPc1
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
pluk	pluk	k1gInSc1
velení	velení	k1gNnSc2
<g/>
,	,	kIx,
řízení	řízení	k1gNnSc2
a	a	k8xC
průzkumu	průzkum	k1gInSc2
Stará	starat	k5eAaImIp3nS
Boleslav	Boleslav	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Vojenská	vojenský	k2eAgNnPc1d1
letadla	letadlo	k1gNnPc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
po	po	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
Stíhací	stíhací	k2eAgInSc4d1
a	a	k8xC
víceúčelové	víceúčelový	k2eAgInPc4d1
bojové	bojový	k2eAgInPc4d1
letouny	letoun	k1gInPc4
</s>
<s>
JAS-39	JAS-39	k4
</s>
<s>
MiG-	MiG-	k?
<g/>
21	#num#	k4
</s>
<s>
MiG-	MiG-	k?
<g/>
23	#num#	k4
<g/>
MF	MF	kA
<g/>
/	/	kIx~
<g/>
ML	ml	kA
</s>
<s>
MiG-	MiG-	k?
<g/>
29	#num#	k4
Bitevní	bitevní	k2eAgInPc4d1
letouny	letoun	k1gInPc4
a	a	k8xC
bombardéry	bombardér	k1gInPc4
</s>
<s>
L-159	L-159	k4
</s>
<s>
MiG-	MiG-	k?
<g/>
23	#num#	k4
<g/>
BN	BN	kA
</s>
<s>
Su-	Su-	k?
<g/>
22	#num#	k4
</s>
<s>
Su-	Su-	k?
<g/>
25	#num#	k4
Dopravní	dopravní	k2eAgInPc1d1
a	a	k8xC
transportní	transportní	k2eAgInPc1d1
letouny	letoun	k1gInPc1
</s>
<s>
A319	A319	k4
</s>
<s>
An-	An-	k?
<g/>
24	#num#	k4
</s>
<s>
An-	An-	k?
<g/>
26	#num#	k4
</s>
<s>
C-295	C-295	k4
</s>
<s>
CL-601	CL-601	k4
</s>
<s>
Jak-	Jak-	k?
<g/>
40	#num#	k4
</s>
<s>
L-410	L-410	k4
</s>
<s>
Tu-	Tu-	k?
<g/>
134	#num#	k4
</s>
<s>
Tu-	Tu-	k?
<g/>
154	#num#	k4
Vrtulníky	vrtulník	k1gInPc5
</s>
<s>
Enstrom	Enstrom	k1gInSc1
480	#num#	k4
</s>
<s>
Mi-	Mi-	k?
<g/>
2	#num#	k4
</s>
<s>
Mi-	Mi-	k?
<g/>
8	#num#	k4
</s>
<s>
Mi-	Mi-	k?
<g/>
17	#num#	k4
</s>
<s>
Mi-	Mi-	k?
<g/>
24	#num#	k4
<g/>
/	/	kIx~
<g/>
Mi-	Mi-	k1gFnSc2
<g/>
35	#num#	k4
</s>
<s>
W-3	W-3	k4
Cvičné	cvičný	k2eAgInPc1d1
letouny	letoun	k1gInPc1
</s>
<s>
EV-97	EV-97	k4
</s>
<s>
L-29	L-29	k4
</s>
<s>
L-39	L-39	k4
</s>
<s>
L-39MS	L-39MS	k4
</s>
<s>
L-159T	L-159T	k4
</s>
<s>
Z-43	Z-43	k4
</s>
<s>
Z-142	Z-142	k4
Speciální	speciální	k2eAgInPc1d1
letouny	letoun	k1gInPc1
</s>
<s>
An-	An-	k?
<g/>
30	#num#	k4
</s>
<s>
Av-	Av-	k?
<g/>
14	#num#	k4
<g/>
FG	FG	kA
</s>
<s>
L-410FG	L-410FG	k4
Bezpilotní	bezpilotní	k2eAgInSc1d1
letadla	letadlo	k1gNnPc1
</s>
<s>
Raven	Raven	k1gInSc1
</s>
<s>
ScanEagle	ScanEagle	k6eAd1
</s>
<s>
Skylark	Skylark	k1gInSc1
</s>
<s>
Sojka	Sojka	k1gMnSc1
III	III	kA
</s>
<s>
Wasp	Wasp	k1gInSc4
Experimentální	experimentální	k2eAgNnPc4d1
letadla	letadlo	k1gNnPc4
a	a	k8xC
prototypy	prototyp	k1gInPc4
</s>
<s>
L-39NG	L-39NG	k4
</s>
<s>
L-610	L-610	k4
Související	související	k2eAgInSc1d1
<g/>
:	:	kIx,
</s>
<s>
Centrum	centrum	k1gNnSc1
leteckého	letecký	k2eAgInSc2d1
výcviku	výcvik	k1gInSc2
</s>
<s>
Historie	historie	k1gFnSc1
letecké	letecký	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
AČR	AČR	kA
</s>
<s>
Vzdušné	vzdušný	k2eAgFnPc1d1
síly	síla	k1gFnPc1
AČR	AČR	kA
</s>
<s>
Seznam	seznam	k1gInSc1
vzdušných	vzdušný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
Nezávislé	závislý	k2eNgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
Afghánistán	Afghánistán	k1gInSc1
•	•	k?
Albánie	Albánie	k1gFnSc1
•	•	k?
Alžírsko	Alžírsko	k1gNnSc1
•	•	k?
Angola	Angola	k1gFnSc1
•	•	k?
Argentina	Argentina	k1gFnSc1
•	•	k?
Arménie	Arménie	k1gFnSc2
•	•	k?
Austrálie	Austrálie	k1gFnSc2
•	•	k?
Ázerbájdžán	Ázerbájdžán	k1gInSc1
•	•	k?
Bahamy	Bahamy	k1gFnPc4
•	•	k?
Bahrajn	Bahrajn	k1gMnSc1
•	•	k?
Bangladéš	Bangladéš	k1gInSc1
•	•	k?
Belgie	Belgie	k1gFnSc2
•	•	k?
Belize	Belize	k1gFnSc2
•	•	k?
Benin	Benin	k1gMnSc1
•	•	k?
Bělorusko	Bělorusko	k1gNnSc4
•	•	k?
Bolívie	Bolívie	k1gFnSc1
•	•	k?
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
•	•	k?
Botswana	Botswana	k1gFnSc1
•	•	k?
Brazílie	Brazílie	k1gFnSc2
•	•	k?
Brunej	Brunej	k1gFnSc2
•	•	k?
Bulharsko	Bulharsko	k1gNnSc1
•	•	k?
Burkina	Burkina	k1gMnSc1
Faso	Faso	k1gMnSc1
•	•	k?
Čad	Čad	k1gInSc1
•	•	k?
Černá	černat	k5eAaImIp3nS
Hora	hora	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Česko	Česko	k1gNnSc1
•	•	k?
Čína	Čína	k1gFnSc1
•	•	k?
Dánsko	Dánsko	k1gNnSc1
•	•	k?
DR	dr	kA
Kongo	Kongo	k1gNnSc1
•	•	k?
Dominikánská	dominikánský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
Džibutsko	Džibutsko	k1gNnSc4
•	•	k?
Egypt	Egypt	k1gInSc1
•	•	k?
Ekvádor	Ekvádor	k1gInSc1
•	•	k?
Eritrea	Eritrea	k1gFnSc1
•	•	k?
Estonsko	Estonsko	k1gNnSc1
•	•	k?
Etiopie	Etiopie	k1gFnSc1
•	•	k?
Filipíny	Filipíny	k1gFnPc4
•	•	k?
Finsko	Finsko	k1gNnSc1
•	•	k?
Francie	Francie	k1gFnSc2
•	•	k?
Ghana	Ghana	k1gFnSc1
•	•	k?
Gruzie	Gruzie	k1gFnSc1
•	•	k?
Guatemala	Guatemala	k1gFnSc1
•	•	k?
Honduras	Honduras	k1gInSc1
•	•	k?
Chile	Chile	k1gNnSc2
•	•	k?
Chorvatsko	Chorvatsko	k1gNnSc1
•	•	k?
Indie	Indie	k1gFnSc2
•	•	k?
Indonésie	Indonésie	k1gFnSc2
•	•	k?
Irák	Irák	k1gInSc1
•	•	k?
Írán	Írán	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Irsko	Irsko	k1gNnSc4
•	•	k?
Island	Island	k1gInSc1
•	•	k?
Itálie	Itálie	k1gFnSc2
•	•	k?
Izrael	Izrael	k1gInSc1
•	•	k?
Jamajka	Jamajka	k1gFnSc1
•	•	k?
Japonsko	Japonsko	k1gNnSc4
•	•	k?
Jemen	Jemen	k1gInSc1
•	•	k?
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
Jižní	jižní	k2eAgInSc1d1
Súdán	Súdán	k1gInSc1
•	•	k?
Jordánsko	Jordánsko	k1gNnSc1
•	•	k?
Kambodža	Kambodža	k1gFnSc1
•	•	k?
Kamerun	Kamerun	k1gInSc1
•	•	k?
Kanada	Kanada	k1gFnSc1
•	•	k?
Katar	katar	k1gInSc1
•	•	k?
Kazachstán	Kazachstán	k1gInSc1
•	•	k?
Keňa	Keňa	k1gFnSc1
•	•	k?
Kolumbie	Kolumbie	k1gFnSc1
•	•	k?
Kongo	Kongo	k1gNnSc1
•	•	k?
Kuba	Kuba	k1gFnSc1
•	•	k?
Kuvajt	Kuvajt	k1gInSc1
•	•	k?
Kypr	Kypr	k1gInSc1
•	•	k?
Kyrgyzstán	Kyrgyzstán	k1gInSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Laos	Laos	k1gInSc1
•	•	k?
Libanon	Libanon	k1gInSc1
•	•	k?
Libye	Libye	k1gFnSc1
•	•	k?
Litva	Litva	k1gFnSc1
•	•	k?
Lotyšsko	Lotyšsko	k1gNnSc1
•	•	k?
Lucembursko	Lucembursko	k1gNnSc1
•	•	k?
Maďarsko	Maďarsko	k1gNnSc1
•	•	k?
Malajsie	Malajsie	k1gFnSc1
•	•	k?
Malawi	Malawi	k1gNnSc2
•	•	k?
Mali	Mali	k1gNnSc2
•	•	k?
Malta	Malta	k1gFnSc1
•	•	k?
Maroko	Maroko	k1gNnSc1
•	•	k?
Mauritánie	Mauritánie	k1gFnSc1
•	•	k?
Mexiko	Mexiko	k1gNnSc1
•	•	k?
Moldavsko	Moldavsko	k1gNnSc1
•	•	k?
Mongolsko	Mongolsko	k1gNnSc4
•	•	k?
Mosambik	Mosambik	k1gInSc1
•	•	k?
Myanmar	Myanmar	k1gInSc1
•	•	k?
Namibie	Namibie	k1gFnSc2
•	•	k?
Německo	Německo	k1gNnSc4
•	•	k?
Niger	Niger	k1gInSc1
•	•	k?
Nigérie	Nigérie	k1gFnSc1
•	•	k?
Nikaragua	Nikaragua	k1gFnSc1
•	•	k?
Nizozemsko	Nizozemsko	k1gNnSc1
•	•	k?
Norsko	Norsko	k1gNnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
•	•	k?
Omán	Omán	k1gInSc1
•	•	k?
Pákistán	Pákistán	k1gInSc1
•	•	k?
Paraguay	Paraguay	k1gFnSc1
•	•	k?
Peru	prát	k5eAaImIp1nS
•	•	k?
Polsko	Polsko	k1gNnSc1
•	•	k?
Portugalsko	Portugalsko	k1gNnSc1
•	•	k?
Rakousko	Rakousko	k1gNnSc1
•	•	k?
Rumunsko	Rumunsko	k1gNnSc1
•	•	k?
Rusko	Rusko	k1gNnSc1
•	•	k?
Řecko	Řecko	k1gNnSc1
•	•	k?
Salvador	Salvador	k1gMnSc1
•	•	k?
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
•	•	k?
Senegal	Senegal	k1gInSc1
•	•	k?
Severní	severní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
Severní	severní	k2eAgFnSc1d1
Makedonie	Makedonie	k1gFnSc1
•	•	k?
Singapur	Singapur	k1gInSc1
•	•	k?
Slovensko	Slovensko	k1gNnSc1
•	•	k?
Slovinsko	Slovinsko	k1gNnSc1
•	•	k?
Spojené	spojený	k2eAgInPc4d1
arabské	arabský	k2eAgInPc4d1
emiráty	emirát	k1gInPc4
•	•	k?
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
•	•	k?
Spojené	spojený	k2eAgFnSc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
•	•	k?
Srbsko	Srbsko	k1gNnSc4
•	•	k?
Srí	Srí	k1gMnSc1
Lanka	lanko	k1gNnSc2
•	•	k?
Středoafrická	středoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
Surinam	Surinam	k1gInSc1
•	•	k?
Súdán	Súdán	k1gInSc1
•	•	k?
Sýrie	Sýrie	k1gFnSc1
•	•	k?
Španělsko	Španělsko	k1gNnSc1
•	•	k?
Švédsko	Švédsko	k1gNnSc1
•	•	k?
Švýcarsko	Švýcarsko	k1gNnSc4
•	•	k?
Tádžikistán	Tádžikistán	k1gInSc1
•	•	k?
Tanzanie	Tanzanie	k1gFnSc2
•	•	k?
Thajsko	Thajsko	k1gNnSc1
•	•	k?
Togo	Togo	k1gNnSc1
•	•	k?
Tunisko	Tunisko	k1gNnSc1
•	•	k?
Turecko	Turecko	k1gNnSc4
•	•	k?
Turkmenistán	Turkmenistán	k1gInSc1
•	•	k?
Uganda	Uganda	k1gFnSc1
•	•	k?
Ukrajina	Ukrajina	k1gFnSc1
•	•	k?
Uruguay	Uruguay	k1gFnSc1
•	•	k?
Uzbekistán	Uzbekistán	k1gInSc1
•	•	k?
Venezuela	Venezuela	k1gFnSc1
•	•	k?
Vietnam	Vietnam	k1gInSc1
•	•	k?
Zambie	Zambie	k1gFnSc2
•	•	k?
Zimbabwe	Zimbabwe	k1gFnSc1
Státy	stát	k1gInPc4
s	s	k7c7
omezeným	omezený	k2eAgNnSc7d1
uznáním	uznání	k1gNnSc7
</s>
<s>
Abcházie	Abcházie	k1gFnSc1
•	•	k?
Arcach	Arcach	k1gMnSc1
•	•	k?
Podněstří	Podněstří	k1gMnSc1
•	•	k?
Tchaj-wan	Tchaj-wan	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20040910006	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
134173632	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Letectví	letectví	k1gNnSc1
|	|	kIx~
Česko	Česko	k1gNnSc1
</s>
