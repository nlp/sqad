<p>
<s>
Hudební	hudební	k2eAgInSc1d1	hudební
nástroj	nástroj	k1gInSc1	nástroj
je	být	k5eAaImIp3nS	být
zařízení	zařízení	k1gNnSc1	zařízení
(	(	kIx(	(
<g/>
nástroj	nástroj	k1gInSc1	nástroj
<g/>
)	)	kIx)	)
k	k	k7c3	k
vydávání	vydávání	k1gNnSc3	vydávání
tónů	tón	k1gInPc2	tón
a	a	k8xC	a
zvuků	zvuk	k1gInPc2	zvuk
používaných	používaný	k2eAgInPc2d1	používaný
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
kultura	kultura	k1gFnSc1	kultura
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgInPc4	svůj
hudební	hudební	k2eAgInPc4d1	hudební
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
patří	patřit	k5eAaImIp3nS	patřit
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
i	i	k9	i
jako	jako	k9	jako
výtvarné	výtvarný	k2eAgInPc4d1	výtvarný
unikáty	unikát	k1gInPc4	unikát
(	(	kIx(	(
<g/>
varhany	varhany	k1gInPc4	varhany
Johanna	Johann	k1gMnSc4	Johann
Gottfrieda	Gottfried	k1gMnSc2	Gottfried
Silbermanna	Silbermanno	k1gNnSc2	Silbermanno
<g/>
,	,	kIx,	,
housle	housle	k1gFnPc1	housle
Antonia	Antonio	k1gMnSc2	Antonio
Stradivaria	Stradivarium	k1gNnSc2	Stradivarium
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
i	i	k8xC	i
jako	jako	k9	jako
doplněk	doplněk	k1gInSc1	doplněk
rituálů	rituál	k1gInPc2	rituál
a	a	k8xC	a
tanců	tanec	k1gInPc2	tanec
(	(	kIx(	(
<g/>
nástroje	nástroj	k1gInPc1	nástroj
přírodních	přírodní	k2eAgInPc2d1	přírodní
národů	národ	k1gInPc2	národ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
vydává	vydávat	k5eAaPmIp3nS	vydávat
zvuk	zvuk	k1gInSc4	zvuk
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použito	použít	k5eAaPmNgNnS	použít
jako	jako	k8xS	jako
hudební	hudební	k2eAgInSc1d1	hudební
nástroj	nástroj	k1gInSc1	nástroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nástroje	nástroj	k1gInPc1	nástroj
lze	lze	k6eAd1	lze
třídit	třídit	k5eAaImF	třídit
podle	podle	k7c2	podle
různých	různý	k2eAgNnPc2d1	různé
hledisek	hledisko	k1gNnPc2	hledisko
<g/>
,	,	kIx,	,
např.	např.	kA	např.
podle	podle	k7c2	podle
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
principu	princip	k1gInSc2	princip
vytváření	vytváření	k1gNnSc2	vytváření
zvuku	zvuk	k1gInSc2	zvuk
</s>
</p>
<p>
<s>
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
jsou	být	k5eAaImIp3nP	být
vyrobeny	vyroben	k2eAgInPc1d1	vyroben
</s>
</p>
<p>
<s>
způsobu	způsob	k1gInSc2	způsob
hryJedním	hryJednit	k5eAaPmIp1nS	hryJednit
ze	z	k7c2	z
způsobů	způsob	k1gInPc2	způsob
klasifikace	klasifikace	k1gFnSc2	klasifikace
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
je	být	k5eAaImIp3nS	být
Hornbostelova	Hornbostelův	k2eAgFnSc1d1	Hornbostelův
a	a	k8xC	a
Sachsova	Sachsův	k2eAgFnSc1d1	Sachsova
systematika	systematika	k1gFnSc1	systematika
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
systematika	systematika	k1gFnSc1	systematika
byla	být	k5eAaImAgFnS	být
navržena	navrhnout	k5eAaPmNgFnS	navrhnout
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
folkloristické	folkloristický	k2eAgFnPc4d1	folkloristická
potřeby	potřeba	k1gFnPc4	potřeba
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
tak	tak	k6eAd1	tak
i	i	k9	i
skupiny	skupina	k1gFnSc2	skupina
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
již	již	k6eAd1	již
nejsou	být	k5eNaImIp3nP	být
běžně	běžně	k6eAd1	běžně
používány	používán	k2eAgInPc1d1	používán
nebo	nebo	k8xC	nebo
které	který	k3yRgInPc1	který
nejsou	být	k5eNaImIp3nP	být
vlastní	vlastní	k2eAgFnSc3d1	vlastní
euroamerické	euroamerický	k2eAgFnSc3d1	euroamerická
kultuře	kultura	k1gFnSc3	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
forma	forma	k1gFnSc1	forma
původně	původně	k6eAd1	původně
evropské	evropský	k2eAgFnSc2d1	Evropská
systematizace	systematizace	k1gFnSc2	systematizace
nástrojů	nástroj	k1gInPc2	nástroj
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
dělení	dělení	k1gNnSc2	dělení
do	do	k7c2	do
základních	základní	k2eAgFnPc2d1	základní
pěti	pět	k4xCc2	pět
skupin	skupina	k1gFnPc2	skupina
podle	podle	k7c2	podle
principu	princip	k1gInSc2	princip
vytváření	vytváření	k1gNnSc2	vytváření
zvuku	zvuk	k1gInSc2	zvuk
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Samozvučné	Samozvučný	k2eAgInPc4d1	Samozvučný
-	-	kIx~	-
idiofony	idiofon	k1gInPc4	idiofon
==	==	k?	==
</s>
</p>
<p>
<s>
U	u	k7c2	u
samozvučných	samozvučný	k2eAgInPc2d1	samozvučný
nástrojů	nástroj	k1gInPc2	nástroj
vzniká	vznikat	k5eAaImIp3nS	vznikat
tón	tón	k1gInSc4	tón
chvěním	chvění	k1gNnSc7	chvění
celého	celý	k2eAgInSc2d1	celý
nástroje	nástroj	k1gInSc2	nástroj
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gFnPc2	jeho
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
součástí	součást	k1gFnPc2	součást
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
nejrůznější	různý	k2eAgInPc4d3	nejrůznější
typy	typ	k1gInPc4	typ
zvonů	zvon	k1gInPc2	zvon
<g/>
,	,	kIx,	,
gong	gong	k1gInSc1	gong
<g/>
,	,	kIx,	,
xylofon	xylofon	k1gInSc1	xylofon
<g/>
,	,	kIx,	,
vibrafon	vibrafon	k1gInSc1	vibrafon
<g/>
,	,	kIx,	,
triangl	triangl	k1gInSc1	triangl
<g/>
,	,	kIx,	,
kastaněty	kastaněty	k1gFnPc1	kastaněty
<g/>
,	,	kIx,	,
kabasa	kabasa	k1gFnSc1	kabasa
<g/>
,	,	kIx,	,
vozembouch	vozembouch	k1gInSc1	vozembouch
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
==	==	k?	==
Blanozvučné	Blanozvučný	k2eAgInPc4d1	Blanozvučný
-	-	kIx~	-
membranofony	membranofon	k1gInPc4	membranofon
==	==	k?	==
</s>
</p>
<p>
<s>
U	u	k7c2	u
blanozvučných	blanozvučný	k2eAgInPc2d1	blanozvučný
nástrojů	nástroj	k1gInPc2	nástroj
se	se	k3xPyFc4	se
tóny	tón	k1gInPc1	tón
tvoří	tvořit	k5eAaImIp3nP	tvořit
rozechvěním	rozechvění	k1gNnSc7	rozechvění
blány	blána	k1gFnSc2	blána
(	(	kIx(	(
<g/>
vyrobené	vyrobený	k2eAgFnPc1d1	vyrobená
nejčastěji	často	k6eAd3	často
ze	z	k7c2	z
zvířecí	zvířecí	k2eAgFnSc2d1	zvířecí
kůže	kůže	k1gFnSc2	kůže
nebo	nebo	k8xC	nebo
z	z	k7c2	z
plastů	plast	k1gInPc2	plast
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takovými	takový	k3xDgFnPc7	takový
jsou	být	k5eAaImIp3nP	být
nejrůznější	různý	k2eAgFnPc4d3	nejrůznější
typy	typa	k1gFnPc4	typa
bubnů	buben	k1gInPc2	buben
a	a	k8xC	a
bubínků	bubínek	k1gInPc2	bubínek
<g/>
,	,	kIx,	,
tympány	tympán	k1gInPc1	tympán
<g/>
,	,	kIx,	,
tamburína	tamburína	k1gFnSc1	tamburína
<g/>
,	,	kIx,	,
darbuka	darbuk	k1gMnSc4	darbuk
<g/>
,	,	kIx,	,
tabla	tablo	k1gNnSc2	tablo
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
==	==	k?	==
Strunné	strunný	k2eAgInPc4d1	strunný
-	-	kIx~	-
chordofony	chordofon	k1gInPc4	chordofon
==	==	k?	==
</s>
</p>
<p>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
tón	tón	k1gInSc1	tón
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
rozechvěním	rozechvění	k1gNnSc7	rozechvění
struny	struna	k1gFnSc2	struna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
napjata	napnout	k5eAaPmNgFnS	napnout
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
pevnými	pevný	k2eAgInPc7d1	pevný
body	bod	k1gInPc7	bod
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
čím	co	k3yRnSc7	co
se	se	k3xPyFc4	se
strunné	strunný	k2eAgInPc1d1	strunný
nástroje	nástroj	k1gInPc1	nástroj
rozechvívají	rozechvívat	k5eAaImIp3nP	rozechvívat
<g/>
,	,	kIx,	,
dělíme	dělit	k5eAaImIp1nP	dělit
je	on	k3xPp3gNnSc4	on
na	na	k7c4	na
</s>
</p>
<p>
<s>
smyčcové	smyčcový	k2eAgFnPc4d1	smyčcová
(	(	kIx(	(
<g/>
housle	housle	k1gFnPc4	housle
<g/>
,	,	kIx,	,
viola	viola	k1gFnSc1	viola
<g/>
,	,	kIx,	,
violoncello	violoncello	k1gNnSc1	violoncello
<g/>
,	,	kIx,	,
kontrabas	kontrabas	k1gInSc1	kontrabas
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kolové	kolová	k1gFnPc1	kolová
(	(	kIx(	(
<g/>
niněra	niněra	k1gFnSc1	niněra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
drnkací	drnkací	k2eAgFnSc1d1	drnkací
s	s	k7c7	s
hmatníkem	hmatník	k1gInSc7	hmatník
(	(	kIx(	(
<g/>
loutna	loutna	k1gFnSc1	loutna
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
citera	citera	k1gFnSc1	citera
<g/>
,	,	kIx,	,
mandolína	mandolína	k1gFnSc1	mandolína
<g/>
,	,	kIx,	,
balalajka	balalajka	k1gFnSc1	balalajka
<g/>
,	,	kIx,	,
banjo	banjo	k1gNnSc1	banjo
<g/>
,	,	kIx,	,
saz	saz	k?	saz
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
nástrojů	nástroj	k1gInPc2	nástroj
nejrůznějších	různý	k2eAgNnPc2d3	nejrůznější
etnik	etnikum	k1gNnPc2	etnikum
na	na	k7c6	na
stejném	stejný	k2eAgInSc6d1	stejný
principu	princip	k1gInSc6	princip
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
drnkací	drnkací	k2eAgMnPc1d1	drnkací
bez	bez	k7c2	bez
hmatníku	hmatník	k1gInSc2	hmatník
(	(	kIx(	(
<g/>
harfa	harfa	k1gFnSc1	harfa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
klávesové	klávesový	k2eAgFnPc1d1	klávesová
(	(	kIx(	(
<g/>
klavír	klavír	k1gInSc1	klavír
<g/>
,	,	kIx,	,
cembalo	cembalo	k1gNnSc1	cembalo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
úderné	úderný	k2eAgFnPc1d1	úderná
(	(	kIx(	(
<g/>
cimbál	cimbál	k1gInSc1	cimbál
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Dechové	dechový	k2eAgInPc4d1	dechový
-	-	kIx~	-
aerofony	aerofon	k1gInPc4	aerofon
==	==	k?	==
</s>
</p>
<p>
<s>
U	u	k7c2	u
těchto	tento	k3xDgInPc2	tento
nástrojů	nástroj	k1gInPc2	nástroj
vzniká	vznikat	k5eAaImIp3nS	vznikat
tón	tón	k1gInSc1	tón
</s>
</p>
<p>
<s>
nárazem	náraz	k1gInSc7	náraz
výdechu	výdech	k1gInSc2	výdech
na	na	k7c4	na
hranu	hrana	k1gFnSc4	hrana
otvoru	otvor	k1gInSc2	otvor
(	(	kIx(	(
<g/>
flétna	flétna	k1gFnSc1	flétna
<g/>
,	,	kIx,	,
pikola	pikola	k1gFnSc1	pikola
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
rozechvěním	rozechvění	k1gNnSc7	rozechvění
plátku	plátek	k1gInSc2	plátek
nebo	nebo	k8xC	nebo
jednoduchého	jednoduchý	k2eAgInSc2d1	jednoduchý
jazýčku	jazýček	k1gInSc2	jazýček
(	(	kIx(	(
<g/>
klarinet	klarinet	k1gInSc1	klarinet
<g/>
,	,	kIx,	,
saxofon	saxofon	k1gInSc1	saxofon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
rozechvěním	rozechvění	k1gNnSc7	rozechvění
plátku	plátek	k1gInSc2	plátek
či	či	k8xC	či
dvojitého	dvojitý	k2eAgInSc2d1	dvojitý
jazýčku	jazýček	k1gInSc2	jazýček
(	(	kIx(	(
<g/>
hoboj	hoboj	k1gInSc1	hoboj
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgInSc1d1	anglický
roh	roh	k1gInSc1	roh
<g/>
,	,	kIx,	,
fagot	fagot	k1gInSc1	fagot
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
rozechvěním	rozechvění	k1gNnSc7	rozechvění
vzduchu	vzduch	k1gInSc2	vzduch
přímo	přímo	k6eAd1	přímo
rty	ret	k1gInPc1	ret
<g/>
,	,	kIx,	,
na	na	k7c4	na
něž	jenž	k3xRgMnPc4	jenž
se	se	k3xPyFc4	se
nasazuje	nasazovat	k5eAaImIp3nS	nasazovat
nátrubek	nátrubek	k1gInSc1	nátrubek
(	(	kIx(	(
<g/>
lesní	lesní	k2eAgInSc4d1	lesní
roh	roh	k1gInSc4	roh
<g/>
,	,	kIx,	,
trubka	trubka	k1gFnSc1	trubka
<g/>
,	,	kIx,	,
pozoun	pozoun	k1gInSc1	pozoun
<g/>
,	,	kIx,	,
tuba	tuba	k1gFnSc1	tuba
<g/>
,	,	kIx,	,
eufonium	eufonium	k1gNnSc1	eufonium
<g/>
)	)	kIx)	)
-	-	kIx~	-
tyto	tento	k3xDgInPc1	tento
nástroje	nástroj	k1gInPc1	nástroj
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
žesťovéK	žesťovéK	k?	žesťovéK
dechovým	dechový	k2eAgInPc3d1	dechový
nástrojům	nástroj	k1gInPc3	nástroj
řadíme	řadit	k5eAaImIp1nP	řadit
též	též	k9	též
</s>
</p>
<p>
<s>
nástroje	nástroj	k1gInPc1	nástroj
vícehlasé	vícehlasý	k2eAgInPc1d1	vícehlasý
bez	bez	k7c2	bez
klaviatury	klaviatura	k1gFnSc2	klaviatura
(	(	kIx(	(
<g/>
foukací	foukací	k2eAgFnSc1d1	foukací
harmonika	harmonika	k1gFnSc1	harmonika
<g/>
,	,	kIx,	,
Panova	Panův	k2eAgFnSc1d1	Panova
flétna	flétna	k1gFnSc1	flétna
<g/>
)	)	kIx)	)
a	a	k8xC	a
</s>
</p>
<p>
<s>
vícehlasé	vícehlasý	k2eAgFnPc1d1	vícehlasá
s	s	k7c7	s
klaviaturou	klaviatura	k1gFnSc7	klaviatura
a	a	k8xC	a
měchem	měch	k1gInSc7	měch
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vzduch	vzduch	k1gInSc1	vzduch
není	být	k5eNaImIp3nS	být
do	do	k7c2	do
nástroje	nástroj	k1gInSc2	nástroj
vháněn	vháněn	k2eAgInSc4d1	vháněn
lidským	lidský	k2eAgInSc7d1	lidský
dechem	dech	k1gInSc7	dech
(	(	kIx(	(
<g/>
varhany	varhany	k1gInPc1	varhany
<g/>
,	,	kIx,	,
harmonium	harmonium	k1gNnSc1	harmonium
<g/>
,	,	kIx,	,
akordeon	akordeon	k1gInSc1	akordeon
<g/>
,	,	kIx,	,
dudy	dudy	k1gFnPc1	dudy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Elektrické	elektrický	k2eAgInPc4d1	elektrický
-	-	kIx~	-
elektrofony	elektrofon	k1gInPc4	elektrofon
==	==	k?	==
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgInPc1	tento
nástroje	nástroj	k1gInPc1	nástroj
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
až	až	k9	až
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Společným	společný	k2eAgInSc7d1	společný
znakem	znak	k1gInSc7	znak
všech	všecek	k3xTgFnPc2	všecek
je	být	k5eAaImIp3nS	být
přeměna	přeměna	k1gFnSc1	přeměna
elektrických	elektrický	k2eAgInPc2d1	elektrický
kmitů	kmit	k1gInPc2	kmit
v	v	k7c4	v
kmity	kmit	k1gInPc4	kmit
akustické	akustický	k2eAgInPc4d1	akustický
(	(	kIx(	(
<g/>
zvukové	zvukový	k2eAgInPc4d1	zvukový
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Elektrické	elektrický	k2eAgInPc4d1	elektrický
kmity	kmit	k1gInPc4	kmit
lze	lze	k6eAd1	lze
generovat	generovat	k5eAaImF	generovat
např.	např.	kA	např.
různými	různý	k2eAgInPc7d1	různý
elektronickými	elektronický	k2eAgInPc7d1	elektronický
oscilátory	oscilátor	k1gInPc7	oscilátor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
mohu	moct	k5eAaImIp1nS	moct
být	být	k5eAaImF	být
elektronkové	elektronkový	k2eAgInPc4d1	elektronkový
<g/>
,	,	kIx,	,
tranzistorové	tranzistorový	k2eAgInPc4d1	tranzistorový
<g/>
,	,	kIx,	,
doutnavkové	doutnavkový	k2eAgInPc4d1	doutnavkový
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
i	i	k9	i
generátory	generátor	k1gInPc4	generátor
fotoelektrické	fotoelektrický	k2eAgInPc4d1	fotoelektrický
<g/>
,	,	kIx,	,
elektrostatické	elektrostatický	k2eAgInPc4d1	elektrostatický
<g/>
,	,	kIx,	,
magneto-elektrické	magnetolektrický	k2eAgInPc4d1	magneto-elektrický
apod.	apod.	kA	apod.
Pomocí	pomocí	k7c2	pomocí
mikrofonu	mikrofon	k1gInSc2	mikrofon
nebo	nebo	k8xC	nebo
snímače	snímač	k1gInSc2	snímač
lze	lze	k6eAd1	lze
na	na	k7c4	na
elektrické	elektrický	k2eAgInPc4d1	elektrický
kmity	kmit	k1gInPc4	kmit
převést	převést	k5eAaPmF	převést
i	i	k9	i
kmity	kmit	k1gInPc1	kmit
mechanické	mechanický	k2eAgInPc1d1	mechanický
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zpracování	zpracování	k1gNnSc6	zpracování
elektrického	elektrický	k2eAgInSc2d1	elektrický
signálu	signál	k1gInSc2	signál
frekvenčními	frekvenční	k2eAgInPc7d1	frekvenční
filtry	filtr	k1gInPc7	filtr
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
obvody	obvod	k1gInPc7	obvod
a	a	k8xC	a
zesílení	zesílení	k1gNnSc2	zesílení
zesilovači	zesilovač	k1gInSc6	zesilovač
je	být	k5eAaImIp3nS	být
elektrická	elektrický	k2eAgFnSc1d1	elektrická
energie	energie	k1gFnSc1	energie
přeměněna	přeměnit	k5eAaPmNgFnS	přeměnit
v	v	k7c4	v
akustickou	akustický	k2eAgFnSc4d1	akustická
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
vyzářena	vyzářit	k5eAaPmNgFnS	vyzářit
do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
pomocí	pomocí	k7c2	pomocí
reproduktorů	reproduktor	k1gInPc2	reproduktor
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vzniku	vznik	k1gInSc2	vznik
tónu	tón	k1gInSc2	tón
lze	lze	k6eAd1	lze
elektrofony	elektrofon	k1gInPc4	elektrofon
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
mechanicko-elektrické	mechanickolektrický	k2eAgFnPc1d1	mechanicko-elektrický
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
vybaveny	vybavit	k5eAaPmNgInP	vybavit
elektromechanickými	elektromechanický	k2eAgInPc7d1	elektromechanický
generátory	generátor	k1gInPc7	generátor
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgNnPc6	jenž
vznikají	vznikat	k5eAaImIp3nP	vznikat
elektrické	elektrický	k2eAgInPc1d1	elektrický
kmity	kmit	k1gInPc1	kmit
<g/>
.	.	kIx.	.
</s>
<s>
Principy	princip	k1gInPc1	princip
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgInPc1d1	různý
(	(	kIx(	(
<g/>
elektromagnetické	elektromagnetický	k2eAgInPc1d1	elektromagnetický
<g/>
,	,	kIx,	,
elektrooptické	elektrooptický	k2eAgInPc1d1	elektrooptický
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejslavnějším	slavný	k2eAgInPc3d3	nejslavnější
nástrojům	nástroj	k1gInPc3	nástroj
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
patří	patřit	k5eAaImIp3nS	patřit
Mellotron	Mellotron	k1gInSc1	Mellotron
(	(	kIx(	(
<g/>
60	[number]	k4	60
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
<g/>
)	)	kIx)	)
a	a	k8xC	a
Hammondovy	Hammondův	k2eAgFnPc1d1	Hammondova
varhany	varhany	k1gFnPc1	varhany
(	(	kIx(	(
<g/>
vyráběny	vyrábět	k5eAaImNgFnP	vyrábět
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
elektroakustické	elektroakustický	k2eAgFnPc1d1	elektroakustická
-	-	kIx~	-
zesilují	zesilovat	k5eAaImIp3nP	zesilovat
a	a	k8xC	a
upravují	upravovat	k5eAaImIp3nP	upravovat
mechanicky	mechanicky	k6eAd1	mechanicky
vytvořené	vytvořený	k2eAgInPc1d1	vytvořený
akustické	akustický	k2eAgInPc1d1	akustický
kmity	kmit	k1gInPc1	kmit
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
tvarem	tvar	k1gInSc7	tvar
a	a	k8xC	a
způsobem	způsob	k1gInSc7	způsob
hry	hra	k1gFnSc2	hra
se	se	k3xPyFc4	se
mnohdy	mnohdy	k6eAd1	mnohdy
podobají	podobat	k5eAaImIp3nP	podobat
různým	různý	k2eAgInPc3d1	různý
druhům	druh	k1gInPc3	druh
tradičních	tradiční	k2eAgInPc2d1	tradiční
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
opatřeny	opatřen	k2eAgInPc1d1	opatřen
jen	jen	k6eAd1	jen
snímačem	snímač	k1gInSc7	snímač
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
např.	např.	kA	např.
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
klavír	klavír	k1gInSc1	klavír
<g/>
,	,	kIx,	,
akordeon	akordeon	k1gInSc1	akordeon
<g/>
,	,	kIx,	,
cembalo	cembalo	k1gNnSc1	cembalo
<g/>
,	,	kIx,	,
housle	housle	k1gFnPc1	housle
<g/>
,	,	kIx,	,
violoncello	violoncello	k1gNnSc1	violoncello
aj.	aj.	kA	aj.
Činnost	činnost	k1gFnSc1	činnost
hudebníka	hudebník	k1gMnSc2	hudebník
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
jako	jako	k9	jako
u	u	k7c2	u
nástrojů	nástroj	k1gInPc2	nástroj
tradičních	tradiční	k2eAgInPc2d1	tradiční
<g/>
,	,	kIx,	,
zvuková	zvukový	k2eAgFnSc1d1	zvuková
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
dynamické	dynamický	k2eAgNnSc1d1	dynamické
rozpětí	rozpětí	k1gNnSc4	rozpětí
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
zvukové	zvukový	k2eAgFnPc4d1	zvuková
vlastnosti	vlastnost	k1gFnPc4	vlastnost
i	i	k8xC	i
některé	některý	k3yIgFnPc1	některý
techniky	technika	k1gFnPc1	technika
hry	hra	k1gFnSc2	hra
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Nejrozšířenějšími	rozšířený	k2eAgInPc7d3	nejrozšířenější
elektroakustickými	elektroakustický	k2eAgInPc7d1	elektroakustický
nástroji	nástroj	k1gInPc7	nástroj
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
elektrické	elektrický	k2eAgFnPc1d1	elektrická
kytary	kytara	k1gFnPc1	kytara
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
čistě	čistě	k6eAd1	čistě
elektrické	elektrický	k2eAgNnSc1d1	elektrické
-	-	kIx~	-
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nástroje	nástroj	k1gInPc4	nástroj
elektronické	elektronický	k2eAgInPc4d1	elektronický
<g/>
,	,	kIx,	,
pracující	pracující	k2eAgInPc4d1	pracující
na	na	k7c6	na
analogovém	analogový	k2eAgInSc6d1	analogový
nebo	nebo	k8xC	nebo
digitálním	digitální	k2eAgInSc6d1	digitální
principu	princip	k1gInSc6	princip
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
elektronické	elektronický	k2eAgInPc1d1	elektronický
nástroje	nástroj	k1gInPc1	nástroj
mohou	moct	k5eAaImIp3nP	moct
napodobit	napodobit	k5eAaPmF	napodobit
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
i	i	k9	i
nahradit	nahradit	k5eAaPmF	nahradit
zvuk	zvuk	k1gInSc4	zvuk
tradičních	tradiční	k2eAgInPc2d1	tradiční
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
především	především	k6eAd1	především
samplerů	sampler	k1gInPc2	sampler
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
zaznamenávají	zaznamenávat	k5eAaImIp3nP	zaznamenávat
originální	originální	k2eAgInSc4d1	originální
zvuk	zvuk	k1gInSc4	zvuk
nástroje	nástroj	k1gInSc2	nástroj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
následně	následně	k6eAd1	následně
reprodukují	reprodukovat	k5eAaBmIp3nP	reprodukovat
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
úlohou	úloha	k1gFnSc7	úloha
nástrojů	nástroj	k1gInPc2	nástroj
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
vytváření	vytváření	k1gNnSc1	vytváření
nových	nový	k2eAgInPc2d1	nový
zvuků	zvuk	k1gInPc2	zvuk
a	a	k8xC	a
tónových	tónový	k2eAgFnPc2d1	tónová
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Proslavenými	proslavený	k2eAgInPc7d1	proslavený
historickými	historický	k2eAgInPc7d1	historický
elektronickými	elektronický	k2eAgInPc7d1	elektronický
nástroji	nástroj	k1gInPc7	nástroj
jsou	být	k5eAaImIp3nP	být
Martenotovy	Martenotův	k2eAgFnPc1d1	Martenotův
vlny	vlna	k1gFnPc1	vlna
<g/>
,	,	kIx,	,
Theremin	Theremin	k1gInSc1	Theremin
a	a	k8xC	a
Trautonium	Trautonium	k1gNnSc1	Trautonium
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
léta	léto	k1gNnPc4	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
nejrozšířenější	rozšířený	k2eAgInPc4d3	nejrozšířenější
klávesové	klávesový	k2eAgInPc4d1	klávesový
syntetizéry	syntetizér	k1gInPc4	syntetizér
<g/>
,	,	kIx,	,
samplery	sampler	k1gInPc4	sampler
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
elektronické	elektronický	k2eAgInPc4d1	elektronický
bicí	bicí	k2eAgInPc4d1	bicí
nástroje	nástroj	k1gInPc4	nástroj
atd.	atd.	kA	atd.
Většinu	většina	k1gFnSc4	většina
současných	současný	k2eAgInPc2d1	současný
elektronických	elektronický	k2eAgInPc2d1	elektronický
nástrojů	nástroj	k1gInPc2	nástroj
lze	lze	k6eAd1	lze
pomocí	pomocí	k7c2	pomocí
MIDI	MIDI	kA	MIDI
systému	systém	k1gInSc2	systém
vzájemně	vzájemně	k6eAd1	vzájemně
propojovat	propojovat	k5eAaImF	propojovat
<g/>
,	,	kIx,	,
dálkově	dálkově	k6eAd1	dálkově
ovládat	ovládat	k5eAaImF	ovládat
<g/>
,	,	kIx,	,
programovat	programovat	k5eAaImF	programovat
nebo	nebo	k8xC	nebo
i	i	k9	i
řídit	řídit	k5eAaImF	řídit
počítačem	počítač	k1gInSc7	počítač
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
jako	jako	k8xC	jako
hudební	hudební	k2eAgInSc1d1	hudební
nástroj	nástroj	k1gInSc1	nástroj
též	též	k9	též
samotný	samotný	k2eAgInSc1d1	samotný
počítač	počítač	k1gInSc1	počítač
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
hudebník	hudebník	k1gMnSc1	hudebník
nemusí	muset	k5eNaImIp3nS	muset
podílet	podílet	k5eAaImF	podílet
na	na	k7c6	na
procesu	proces	k1gInSc6	proces
vytváření	vytváření	k1gNnSc4	vytváření
zvuku	zvuk	k1gInSc2	zvuk
standardní	standardní	k2eAgFnSc7d1	standardní
hráčskou	hráčský	k2eAgFnSc7d1	hráčská
technikou	technika	k1gFnSc7	technika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ovládáním	ovládání	k1gNnSc7	ovládání
příslušného	příslušný	k2eAgInSc2d1	příslušný
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
MODR	MODR	kA	MODR
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgInPc1d1	hudební
nástroje	nástroj	k1gInPc1	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Supraphon	supraphon	k1gInSc1	supraphon
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RYCHLÍK	Rychlík	k1gMnSc1	Rychlík
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
instrumentace	instrumentace	k1gFnSc1	instrumentace
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Panton	Panton	k1gInSc1	Panton
1959	[number]	k4	1959
<g/>
-	-	kIx~	-
<g/>
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZICH	Zich	k1gMnSc1	Zich
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Instrumentační	instrumentační	k2eAgFnPc1d1	instrumentační
práce	práce	k1gFnPc1	práce
se	s	k7c7	s
skupinami	skupina	k1gFnPc7	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
hudební	hudební	k2eAgInSc4d1	hudební
nástroj	nástroj	k1gInSc4	nástroj
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
hudební	hudební	k2eAgInSc1d1	hudební
nástroj	nástroj	k1gInSc1	nástroj
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
