<s>
Publius	Publius	k1gMnSc1	Publius
Septimius	Septimius	k1gMnSc1	Septimius
Geta	Geta	k1gMnSc1	Geta
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
či	či	k8xC	či
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
189	[number]	k4	189
Mediolanum	Mediolanum	k1gInSc1	Mediolanum
-	-	kIx~	-
19	[number]	k4	19
<g/>
.	.	kIx.	.
či	či	k8xC	či
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
211	[number]	k4	211
Řím	Řím	k1gInSc1	Řím
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
vládnoucí	vládnoucí	k2eAgMnSc1d1	vládnoucí
od	od	k7c2	od
září	září	k1gNnSc2	září
/	/	kIx~	/
října	říjen	k1gInSc2	říjen
209	[number]	k4	209
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
211	[number]	k4	211
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
otcem	otec	k1gMnSc7	otec
Septimiem	Septimius	k1gMnSc7	Septimius
Severem	sever	k1gInSc7	sever
a	a	k8xC	a
bratrem	bratr	k1gMnSc7	bratr
Caracallou	Caracalla	k1gMnSc7	Caracalla
<g/>
,	,	kIx,	,
od	od	k7c2	od
února	únor	k1gInSc2	únor
211	[number]	k4	211
pak	pak	k6eAd1	pak
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
Caracallou	Caracalla	k1gFnSc7	Caracalla
<g/>
.	.	kIx.	.
</s>
<s>
Geta	Geta	k1gMnSc1	Geta
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
roku	rok	k1gInSc2	rok
189	[number]	k4	189
jako	jako	k8xC	jako
druhorozený	druhorozený	k2eAgMnSc1d1	druhorozený
syn	syn	k1gMnSc1	syn
senátora	senátor	k1gMnSc2	senátor
Septimia	Septimium	k1gNnSc2	Septimium
Severa	Severa	k1gMnSc1	Severa
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
tehdy	tehdy	k6eAd1	tehdy
správcem	správce	k1gMnSc7	správce
Sicílie	Sicílie	k1gFnSc2	Sicílie
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
189	[number]	k4	189
<g/>
-	-	kIx~	-
<g/>
190	[number]	k4	190
<g/>
)	)	kIx)	)
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
již	již	k6eAd1	již
za	za	k7c7	za
sebou	se	k3xPyFc7	se
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
kariéru	kariéra	k1gFnSc4	kariéra
ve	v	k7c6	v
státních	státní	k2eAgFnPc6d1	státní
službách	služba	k1gFnPc6	služba
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
matkou	matka	k1gFnSc7	matka
byla	být	k5eAaImAgFnS	být
Iulia	Iulius	k1gMnSc4	Iulius
Domna	Domna	k1gFnSc1	Domna
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
z	z	k7c2	z
kněžského	kněžský	k2eAgInSc2d1	kněžský
rodu	rod	k1gInSc2	rod
ze	z	k7c2	z
syrské	syrský	k2eAgFnSc2d1	Syrská
Emesy	Emesa	k1gFnSc2	Emesa
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
příjmení	příjmení	k1gNnSc4	příjmení
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Geta	Geta	k1gMnSc1	Geta
po	po	k7c6	po
dědovi	děd	k1gMnSc6	děd
či	či	k8xC	či
strýci	strýc	k1gMnPc7	strýc
z	z	k7c2	z
otcovy	otcův	k2eAgFnSc2d1	otcova
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
oba	dva	k4xCgMnPc1	dva
jmenovali	jmenovat	k5eAaImAgMnP	jmenovat
shodně	shodně	k6eAd1	shodně
a	a	k8xC	a
nesli	nést	k5eAaImAgMnP	nést
také	také	k9	také
praenomen	praenomen	k2eAgInSc4d1	praenomen
Publius	Publius	k1gInSc4	Publius
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
193	[number]	k4	193
<g/>
,	,	kIx,	,
když	když	k8xS	když
byly	být	k5eAaImAgFnP	být
Getovi	Geta	k1gMnSc3	Geta
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Septimius	Septimius	k1gMnSc1	Septimius
Severus	Severus	k1gMnSc1	Severus
provolán	provolat	k5eAaPmNgMnS	provolat
císařem	císař	k1gMnSc7	císař
<g/>
,	,	kIx,	,
o	o	k7c4	o
vládu	vláda	k1gFnSc4	vláda
však	však	k9	však
musel	muset	k5eAaImAgInS	muset
ještě	ještě	k9	ještě
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
bojovat	bojovat	k5eAaImF	bojovat
s	s	k7c7	s
třemi	tři	k4xCgMnPc7	tři
dalšími	další	k2eAgMnPc7d1	další
pretendenty	pretendent	k1gMnPc7	pretendent
trůnu	trůn	k1gInSc2	trůn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmi	osm	k4xCc6	osm
letech	léto	k1gNnPc6	léto
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
Geta	Get	k2eAgMnSc4d1	Get
otce	otec	k1gMnSc4	otec
na	na	k7c6	na
tažení	tažení	k1gNnSc6	tažení
proti	proti	k7c3	proti
Parthům	Parth	k1gInPc3	Parth
a	a	k8xC	a
patrně	patrně	k6eAd1	patrně
po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
u	u	k7c2	u
Ktésifóntu	Ktésifónt	k1gInSc2	Ktésifónt
byl	být	k5eAaImAgInS	být
jmenován	jmenovat	k5eAaBmNgInS	jmenovat
caesarem	caesar	k1gInSc7	caesar
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gInSc1	jeho
titul	titul	k1gInSc1	titul
zněl	znět	k5eAaImAgInS	znět
P.	P.	kA	P.
SEPTIMIUS	SEPTIMIUS	kA	SEPTIMIUS
GETA	GETA	kA	GETA
NOBILISSIMUS	NOBILISSIMUS	kA	NOBILISSIMUS
CAESAR	Caesar	k1gMnSc1	Caesar
<g/>
,	,	kIx,	,
PRINCEPS	PRINCEPS	kA	PRINCEPS
IUVENTUTIS	IUVENTUTIS	kA	IUVENTUTIS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
205	[number]	k4	205
se	se	k3xPyFc4	se
mladý	mladý	k2eAgMnSc1d1	mladý
princ	princ	k1gMnSc1	princ
stal	stát	k5eAaPmAgMnS	stát
konzulem	konzul	k1gMnSc7	konzul
<g/>
,	,	kIx,	,
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
správcem	správce	k1gMnSc7	správce
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
roku	rok	k1gInSc2	rok
209	[number]	k4	209
nakonec	nakonec	k6eAd1	nakonec
plnoprávným	plnoprávný	k2eAgMnSc7d1	plnoprávný
císařem	císař	k1gMnSc7	císař
-	-	kIx~	-
augustem	august	k1gMnSc7	august
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
útlého	útlý	k2eAgNnSc2d1	útlé
dětství	dětství	k1gNnSc2	dětství
panovala	panovat	k5eAaImAgFnS	panovat
mezi	mezi	k7c7	mezi
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
starším	starší	k1gMnSc7	starší
bratrem	bratr	k1gMnSc7	bratr
Caracallou	Caracalla	k1gMnSc7	Caracalla
silná	silný	k2eAgFnSc1d1	silná
řevnivost	řevnivost	k1gFnSc1	řevnivost
<g/>
.	.	kIx.	.
</s>
<s>
Cassius	Cassius	k1gMnSc1	Cassius
Dio	Dio	k1gMnSc1	Dio
se	se	k3xPyFc4	se
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
vozatajských	vozatajský	k2eAgInPc6d1	vozatajský
závodech	závod	k1gInPc6	závod
poníků	poník	k1gMnPc2	poník
řídili	řídit	k5eAaImAgMnP	řídit
oba	dva	k4xCgMnPc1	dva
své	svůj	k3xOyFgMnPc4	svůj
spřežení	spřežení	k1gNnSc1	spřežení
tak	tak	k6eAd1	tak
zběsile	zběsile	k6eAd1	zběsile
<g/>
,	,	kIx,	,
až	až	k9	až
závod	závod	k1gInSc1	závod
skončil	skončit	k5eAaPmAgInS	skončit
zlomenou	zlomený	k2eAgFnSc7d1	zlomená
Caracallovou	Caracallův	k2eAgFnSc7d1	Caracallova
nohou	noha	k1gFnSc7	noha
<g/>
.	.	kIx.	.
</s>
<s>
Severus	Severus	k1gInSc1	Severus
si	se	k3xPyFc3	se
všímal	všímat	k5eAaImAgInS	všímat
rivality	rivalita	k1gFnPc4	rivalita
mezi	mezi	k7c7	mezi
svými	svůj	k3xOyFgMnPc7	svůj
syny	syn	k1gMnPc7	syn
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
všemožně	všemožně	k6eAd1	všemožně
tlumit	tlumit	k5eAaImF	tlumit
a	a	k8xC	a
přivést	přivést	k5eAaPmF	přivést
je	být	k5eAaImIp3nS	být
ke	k	k7c3	k
smíru	smír	k1gInSc3	smír
a	a	k8xC	a
shodě	shoda	k1gFnSc3	shoda
<g/>
;	;	kIx,	;
roku	rok	k1gInSc2	rok
208	[number]	k4	208
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
na	na	k7c4	na
znamení	znamení	k1gNnSc4	znamení
svornosti	svornost	k1gFnSc2	svornost
konzuly	konzul	k1gMnPc7	konzul
oba	dva	k4xCgInPc4	dva
dva	dva	k4xCgInPc1	dva
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
však	však	k9	však
v	v	k7c6	v
roce	rok	k1gInSc6	rok
211	[number]	k4	211
císař	císař	k1gMnSc1	císař
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
nebylo	být	k5eNaImAgNnS	být
nic	nic	k3yNnSc1	nic
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
by	by	kYmCp3nS	by
brzdilo	brzdit	k5eAaImAgNnS	brzdit
jejich	jejich	k3xOp3gFnSc4	jejich
vzájemnou	vzájemný	k2eAgFnSc4d1	vzájemná
nenávist	nenávist	k1gFnSc4	nenávist
<g/>
.	.	kIx.	.
</s>
<s>
Caracalla	Caracalla	k6eAd1	Caracalla
se	se	k3xPyFc4	se
údajně	údajně	k6eAd1	údajně
snažil	snažit	k5eAaImAgMnS	snažit
Getu	Geta	k1gFnSc4	Geta
odstranit	odstranit	k5eAaPmF	odstranit
již	již	k6eAd1	již
za	za	k7c2	za
Severova	Severův	k2eAgInSc2d1	Severův
života	život	k1gInSc2	život
a	a	k8xC	a
těsně	těsně	k6eAd1	těsně
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
však	však	k9	však
byly	být	k5eAaImAgInP	být
jeho	jeho	k3xOp3gInPc1	jeho
pokusy	pokus	k1gInPc1	pokus
zmařeny	zmařen	k2eAgInPc1d1	zmařen
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
Severus	Severus	k1gMnSc1	Severus
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
snažili	snažit	k5eAaImAgMnP	snažit
se	se	k3xPyFc4	se
oba	dva	k4xCgMnPc1	dva
bratři	bratr	k1gMnPc1	bratr
dostat	dostat	k5eAaPmF	dostat
co	co	k9	co
nejrychleji	rychle	k6eAd3	rychle
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
zajistili	zajistit	k5eAaPmAgMnP	zajistit
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
vzájemné	vzájemný	k2eAgFnSc3d1	vzájemná
podezíravosti	podezíravost	k1gFnSc3	podezíravost
však	však	k9	však
cestovali	cestovat	k5eAaImAgMnP	cestovat
každý	každý	k3xTgMnSc1	každý
sám	sám	k3xTgMnSc1	sám
a	a	k8xC	a
i	i	k8xC	i
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
si	se	k3xPyFc3	se
oba	dva	k4xCgMnPc1	dva
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
císařský	císařský	k2eAgInSc4d1	císařský
palác	palác	k1gInSc4	palác
každý	každý	k3xTgMnSc1	každý
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
část	část	k1gFnSc4	část
a	a	k8xC	a
utěsnili	utěsnit	k5eAaPmAgMnP	utěsnit
všechny	všechen	k3xTgInPc4	všechen
průchody	průchod	k1gInPc4	průchod
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
chránili	chránit	k5eAaImAgMnP	chránit
před	před	k7c7	před
intrikami	intrika	k1gFnPc7	intrika
druhého	druhý	k4xOgMnSc4	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
si	se	k3xPyFc3	se
hleděl	hledět	k5eAaImAgMnS	hledět
získat	získat	k5eAaPmF	získat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
stranu	strana	k1gFnSc4	strana
vlivné	vlivný	k2eAgMnPc4d1	vlivný
občany	občan	k1gMnPc4	občan
a	a	k8xC	a
vzájemně	vzájemně	k6eAd1	vzájemně
si	se	k3xPyFc3	se
činili	činit	k5eAaImAgMnP	činit
všemožné	všemožný	k2eAgInPc4d1	všemožný
úklady	úklad	k1gInPc4	úklad
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
Iulia	Iulius	k1gMnSc2	Iulius
Domna	Domn	k1gMnSc2	Domn
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
snažila	snažit	k5eAaImAgFnS	snažit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
čeho	co	k3yInSc2	co
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
jejich	jejich	k3xOp3gMnSc6	jejich
otci	otec	k1gMnSc6	otec
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
se	s	k7c7	s
stejným	stejný	k2eAgInSc7d1	stejný
výsledkem	výsledek	k1gInSc7	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
nesnášenlivost	nesnášenlivost	k1gFnSc1	nesnášenlivost
zrodila	zrodit	k5eAaPmAgFnS	zrodit
vůbec	vůbec	k9	vůbec
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
říše	říš	k1gFnSc2	říš
myšlenku	myšlenka	k1gFnSc4	myšlenka
na	na	k7c4	na
její	její	k3xOp3gNnSc4	její
rozdělení	rozdělení	k1gNnSc4	rozdělení
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
Evropa	Evropa	k1gFnSc1	Evropa
a	a	k8xC	a
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
,	,	kIx,	,
měly	mít	k5eAaImAgFnP	mít
připadnout	připadnout	k5eAaPmF	připadnout
Caracallovi	Caracallův	k2eAgMnPc1d1	Caracallův
a	a	k8xC	a
východ	východ	k1gInSc1	východ
<g/>
,	,	kIx,	,
Asie	Asie	k1gFnSc1	Asie
a	a	k8xC	a
Egypt	Egypt	k1gInSc1	Egypt
<g/>
,	,	kIx,	,
Getovi	Getův	k2eAgMnPc1d1	Getův
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
návrh	návrh	k1gInSc1	návrh
byl	být	k5eAaImAgInS	být
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
zamítnut	zamítnout	k5eAaPmNgInS	zamítnout
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
tuto	tento	k3xDgFnSc4	tento
situaci	situace	k1gFnSc4	situace
vyřeší	vyřešit	k5eAaPmIp3nS	vyřešit
pouze	pouze	k6eAd1	pouze
smrt	smrt	k1gFnSc1	smrt
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
císařů	císař	k1gMnPc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
Geta	Geta	k1gMnSc1	Geta
byl	být	k5eAaImAgMnS	být
všeobecně	všeobecně	k6eAd1	všeobecně
oblíbenější	oblíbený	k2eAgMnSc1d2	oblíbenější
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
byl	být	k5eAaImAgInS	být
údajně	údajně	k6eAd1	údajně
laskavější	laskavý	k2eAgInSc1d2	laskavější
a	a	k8xC	a
mírnější	mírný	k2eAgInSc1d2	mírnější
než	než	k8xS	než
Caracalla	Caracalla	k1gFnSc1	Caracalla
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
Caracalla	Caracalla	k1gMnSc1	Caracalla
uchýlil	uchýlit	k5eAaPmAgMnS	uchýlit
ke	k	k7c3	k
zbabělé	zbabělý	k2eAgFnSc3d1	zbabělá
lsti	lest	k1gFnSc3	lest
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
záminkou	záminka	k1gFnSc7	záminka
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
chce	chtít	k5eAaImIp3nS	chtít
usmířit	usmířit	k5eAaPmF	usmířit
<g/>
,	,	kIx,	,
vylákal	vylákat	k5eAaPmAgMnS	vylákat
neozbrojeného	ozbrojený	k2eNgMnSc4d1	neozbrojený
Getu	Geta	k1gMnSc4	Geta
k	k	k7c3	k
matce	matka	k1gFnSc3	matka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jej	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
takřka	takřka	k6eAd1	takřka
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
klíně	klín	k1gInSc6	klín
<g/>
,	,	kIx,	,
zavraždili	zavraždit	k5eAaPmAgMnP	zavraždit
ukrytí	ukrytý	k2eAgMnPc1d1	ukrytý
vojáci	voják	k1gMnPc1	voják
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
silná	silný	k2eAgFnSc1d1	silná
nenávist	nenávist	k1gFnSc1	nenávist
panovala	panovat	k5eAaImAgFnS	panovat
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
bratry	bratr	k1gMnPc7	bratr
<g/>
,	,	kIx,	,
výmluvně	výmluvně	k6eAd1	výmluvně
ilustruje	ilustrovat	k5eAaBmIp3nS	ilustrovat
Caracallovo	Caracallův	k2eAgNnSc4d1	Caracallův
chování	chování	k1gNnSc4	chování
po	po	k7c6	po
bratrově	bratrův	k2eAgFnSc6d1	bratrova
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Geta	Geta	k1gMnSc1	Geta
byl	být	k5eAaImAgMnS	být
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
veřejným	veřejný	k2eAgMnSc7d1	veřejný
nepřítelem	nepřítel	k1gMnSc7	nepřítel
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc3	jeho
jméno	jméno	k1gNnSc1	jméno
bylo	být	k5eAaImAgNnS	být
vymazáno	vymazat	k5eAaPmNgNnS	vymazat
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
oficiálních	oficiální	k2eAgInPc2d1	oficiální
dokumentů	dokument	k1gInPc2	dokument
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
přátelé	přítel	k1gMnPc1	přítel
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
měl	mít	k5eAaImAgInS	mít
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
oblibě	obliba	k1gFnSc6	obliba
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
popraveni	popravit	k5eAaPmNgMnP	popravit
nebo	nebo	k8xC	nebo
jinak	jinak	k6eAd1	jinak
postiženi	postihnout	k5eAaPmNgMnP	postihnout
<g/>
.	.	kIx.	.
</s>
<s>
GRANT	grant	k1gInSc1	grant
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
<g/>
.	.	kIx.	.
</s>
<s>
Římští	římský	k2eAgMnPc1d1	římský
císařové	císař	k1gMnPc1	císař
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
BB	BB	kA	BB
art	art	k?	art
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
387	[number]	k4	387
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7257	[number]	k4	7257
<g/>
-	-	kIx~	-
<g/>
731	[number]	k4	731
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
HÉRÓDIANOS	HÉRÓDIANOS	kA	HÉRÓDIANOS
<g/>
.	.	kIx.	.
</s>
<s>
Řím	Řím	k1gInSc1	Řím
po	po	k7c6	po
Marku	Marek	k1gMnSc6	Marek
Aureliovi	Aurelius	k1gMnSc6	Aurelius
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
314	[number]	k4	314
s.	s.	k?	s.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
PETRÁŇ	Petráň	k1gMnSc1	Petráň
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
;	;	kIx,	;
FRIDRICHOVSKÝ	FRIDRICHOVSKÝ	kA	FRIDRICHOVSKÝ
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
římských	římský	k2eAgMnPc2d1	římský
císařů	císař	k1gMnPc2	císař
a	a	k8xC	a
císařoven	císařovna	k1gFnPc2	císařovna
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
jejich	jejich	k3xOp3gFnPc2	jejich
mincí	mince	k1gFnPc2	mince
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
365	[number]	k4	365
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
267	[number]	k4	267
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Portréty	portrét	k1gInPc1	portrét
světovládců	světovládce	k1gMnPc2	světovládce
I	i	k8xC	i
(	(	kIx(	(
<g/>
od	od	k7c2	od
Hadriana	Hadrian	k1gMnSc2	Hadrian
po	po	k7c4	po
Alexandra	Alexandr	k1gMnSc4	Alexandr
Severa	Severa	k1gMnSc1	Severa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
356	[number]	k4	356
s.	s.	k?	s.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Geta	Get	k1gInSc2	Get
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Getova	Getův	k2eAgFnSc1d1	Getův
biografie	biografie	k1gFnSc1	biografie
</s>
