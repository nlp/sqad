<p>
<s>
Auri	Auri	k6eAd1	Auri
je	být	k5eAaImIp3nS	být
připravované	připravovaný	k2eAgNnSc1d1	připravované
debutové	debutový	k2eAgNnSc1d1	debutové
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
finsko-britské	finskoritský	k2eAgFnSc2d1	finsko-britský
hudební	hudební	k2eAgFnSc2d1	hudební
skupiny	skupina	k1gFnSc2	skupina
Auri	Aur	k1gFnSc2	Aur
<g/>
.	.	kIx.	.
</s>
<s>
Vydáno	vydat	k5eAaPmNgNnS	vydat
bylo	být	k5eAaImAgNnS	být
23	[number]	k4	23
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2018	[number]	k4	2018
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
společnosti	společnost	k1gFnSc2	společnost
Nuclear	Nuclear	k1gMnSc1	Nuclear
Blast	Blast	k1gMnSc1	Blast
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávání	nahrávání	k1gNnSc1	nahrávání
probíhalo	probíhat	k5eAaImAgNnS	probíhat
v	v	k7c6	v
britském	britský	k2eAgNnSc6d1	Britské
studiu	studio	k1gNnSc6	studio
Real	Real	k1gInSc1	Real
World	Worlda	k1gFnPc2	Worlda
Studio	studio	k1gNnSc1	studio
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
zvukového	zvukový	k2eAgMnSc2d1	zvukový
technika	technik	k1gMnSc2	technik
Tima	Timus	k1gMnSc2	Timus
Olivera	Oliver	k1gMnSc2	Oliver
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
Aphrodite	Aphrodit	k1gInSc5	Aphrodit
Rising	Rising	k1gInSc1	Rising
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc7	první
písní	píseň	k1gFnSc7	píseň
kapely	kapela	k1gFnSc2	kapela
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
napsána	napsat	k5eAaBmNgFnS	napsat
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
skladeb	skladba	k1gFnPc2	skladba
byl	být	k5eAaImAgInS	být
poté	poté	k6eAd1	poté
dopsán	dopsat	k5eAaPmNgInS	dopsat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
měli	mít	k5eAaImAgMnP	mít
Tuomas	Tuomas	k1gInSc4	Tuomas
Holopainen	Holopainna	k1gFnPc2	Holopainna
a	a	k8xC	a
Troy	Troa	k1gFnPc4	Troa
Donockley	Donockle	k2eAgFnPc4d1	Donockle
pauzu	pauza	k1gFnSc4	pauza
s	s	k7c7	s
jejich	jejich	k3xOp3gInPc7	jejich
hlavních	hlavní	k2eAgInPc2d1	hlavní
kapelou	kapela	k1gFnSc7	kapela
Nightwish	Nightwish	k1gInSc4	Nightwish
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
hudbě	hudba	k1gFnSc3	hudba
Nightwish	Nightwisha	k1gFnPc2	Nightwisha
se	se	k3xPyFc4	se
tito	tento	k3xDgMnPc1	tento
dva	dva	k4xCgMnPc1	dva
hudebníci	hudebník	k1gMnPc1	hudebník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
na	na	k7c4	na
album	album	k1gNnSc4	album
skládali	skládat	k5eAaImAgMnP	skládat
písně	píseň	k1gFnPc4	píseň
<g/>
,	,	kIx,	,
oprostili	oprostit	k5eAaPmAgMnP	oprostit
od	od	k7c2	od
symfonického	symfonický	k2eAgInSc2d1	symfonický
metalu	metal	k1gInSc2	metal
a	a	k8xC	a
ve	v	k7c6	v
skladbách	skladba	k1gFnPc6	skladba
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
keltské	keltský	k2eAgInPc4d1	keltský
<g/>
,	,	kIx,	,
folkové	folkový	k2eAgInPc4d1	folkový
<g/>
,	,	kIx,	,
etnické	etnický	k2eAgInPc4d1	etnický
či	či	k8xC	či
filmové	filmový	k2eAgInPc4d1	filmový
motivy	motiv	k1gInPc4	motiv
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
angličtiny	angličtina	k1gFnSc2	angličtina
zpívá	zpívat	k5eAaImIp3nS	zpívat
Johanna	Johanen	k2eAgFnSc1d1	Johanna
Kurkela	Kurkela	k1gFnSc1	Kurkela
místy	místy	k6eAd1	místy
také	také	k9	také
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
skladeb	skladba	k1gFnPc2	skladba
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
Johanna	Johanen	k2eAgFnSc1d1	Johanna
Kurkela	Kurkela	k1gFnSc1	Kurkela
–	–	k?	–
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
Tuomas	Tuomas	k1gInSc1	Tuomas
Holopainen	Holopainen	k1gInSc1	Holopainen
–	–	k?	–
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
,	,	kIx,	,
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
Troy	Tro	k2eAgFnPc1d1	Tro
Donockley	Donockle	k2eAgFnPc1d1	Donockle
–	–	k?	–
kytary	kytara	k1gFnPc1	kytara
<g/>
,	,	kIx,	,
buzuki	buzuk	k1gFnPc1	buzuk
<g/>
,	,	kIx,	,
irské	irský	k2eAgFnPc1d1	irská
dudy	dudy	k1gFnPc1	dudy
<g/>
,	,	kIx,	,
irská	irský	k2eAgFnSc1d1	irská
píšťala	píšťala	k1gFnSc1	píšťala
<g/>
,	,	kIx,	,
aerofon	aerofon	k1gInSc1	aerofon
<g/>
,	,	kIx,	,
bodhrán	bodhrán	k2eAgInSc1d1	bodhrán
<g/>
,	,	kIx,	,
klávesyTechnická	klávesyTechnický	k2eAgFnSc1d1	klávesyTechnický
podporaTim	podporaTim	k6eAd1	podporaTim
Oliver	Oliver	k1gMnSc1	Oliver
–	–	k?	–
zvukový	zvukový	k2eAgMnSc1d1	zvukový
inženýr	inženýr	k1gMnSc1	inženýr
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
