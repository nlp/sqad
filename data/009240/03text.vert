<p>
<s>
Santa	Santa	k1gFnSc1	Santa
María	Marí	k1gInSc2	Marí
byla	být	k5eAaImAgFnS	být
největší	veliký	k2eAgFnSc1d3	veliký
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
lodí	loď	k1gFnPc2	loď
použitých	použitý	k2eAgFnPc2d1	použitá
Kryštofem	Kryštof	k1gMnSc7	Kryštof
Kolumbem	Kolumbus	k1gMnSc7	Kolumbus
při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
první	první	k4xOgFnSc6	první
cestě	cesta	k1gFnSc6	cesta
přes	přes	k7c4	přes
Atlantský	atlantský	k2eAgInSc4d1	atlantský
oceán	oceán	k1gInSc4	oceán
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1492	[number]	k4	1492
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
majitel	majitel	k1gMnSc1	majitel
byl	být	k5eAaImAgMnS	být
Juan	Juan	k1gMnSc1	Juan
de	de	k?	de
la	la	k1gNnSc4	la
Cosa	Cos	k1gInSc2	Cos
<g/>
.	.	kIx.	.
</s>
<s>
Výpravy	výprava	k1gFnPc1	výprava
se	se	k3xPyFc4	se
zúčastnily	zúčastnit	k5eAaPmAgFnP	zúčastnit
ještě	ještě	k9	ještě
dvě	dva	k4xCgFnPc1	dva
karavely	karavela	k1gFnPc1	karavela
Pinta	pinta	k1gFnSc1	pinta
a	a	k8xC	a
Niñ	Niñ	k1gFnSc1	Niñ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Santa	Santa	k1gFnSc1	Santa
María	María	k1gFnSc1	María
byla	být	k5eAaImAgFnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
malá	malá	k1gFnSc1	malá
karaka	karak	k1gMnSc2	karak
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nao	nao	k?	nao
(	(	kIx(	(
<g/>
nau	nau	k?	nau
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
velící	velící	k2eAgFnSc1d1	velící
a	a	k8xC	a
zásobovací	zásobovací	k2eAgFnSc1d1	zásobovací
loď	loď	k1gFnSc1	loď
pro	pro	k7c4	pro
Kolumbovu	Kolumbův	k2eAgFnSc4d1	Kolumbova
expedici	expedice	k1gFnSc4	expedice
<g/>
.	.	kIx.	.
</s>
<s>
Posádku	posádka	k1gFnSc4	posádka
tvořilo	tvořit	k5eAaImAgNnS	tvořit
40	[number]	k4	40
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Santa	Santa	k1gMnSc1	Santa
María	María	k1gMnSc1	María
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
La	la	k1gNnSc7	la
Gallega	Galleg	k1gMnSc2	Galleg
<g/>
,	,	kIx,	,
jejím	její	k3xOp3gMnPc3	její
námořníkům	námořník	k1gMnPc3	námořník
byla	být	k5eAaImAgFnS	být
ale	ale	k8xC	ale
možná	možná	k9	možná
známa	znám	k2eAgFnSc1d1	známa
také	také	k9	také
jako	jako	k8xS	jako
Marigalante	Marigalant	k1gMnSc5	Marigalant
(	(	kIx(	(
<g/>
Galantní	galantní	k2eAgFnSc2d1	galantní
Marie	Maria	k1gFnSc2	Maria
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bartolomé	Bartolomý	k2eAgInPc1d1	Bartolomý
de	de	k?	de
Las	laso	k1gNnPc2	laso
Casas	Casasa	k1gFnPc2	Casasa
nikdy	nikdy	k6eAd1	nikdy
nepoužil	použít	k5eNaPmAgMnS	použít
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
dílech	dílo	k1gNnPc6	dílo
název	název	k1gInSc1	název
La	la	k1gNnSc1	la
Gallega	Galleg	k1gMnSc2	Galleg
<g/>
,	,	kIx,	,
Marigalante	Marigalant	k1gMnSc5	Marigalant
či	či	k8xC	či
Santa	Santo	k1gNnPc4	Santo
María	Marí	k2eAgNnPc4d1	Marí
<g/>
,	,	kIx,	,
používal	používat	k5eAaImAgInS	používat
spíše	spíše	k9	spíše
la	la	k1gNnSc4	la
Capitana	Capitana	k1gFnSc1	Capitana
nebo	nebo	k8xC	nebo
La	la	k1gNnSc1	la
Nao	Nao	k1gFnSc2	Nao
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Santa	Santa	k1gFnSc1	Santa
María	Marí	k1gInSc2	Marí
byla	být	k5eAaImAgFnS	být
sice	sice	k8xC	sice
vlajkovou	vlajkový	k2eAgFnSc4d1	vlajková
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejpomalejší	pomalý	k2eAgInPc1d3	nejpomalejší
z	z	k7c2	z
Kolumbových	Kolumbův	k2eAgFnPc2d1	Kolumbova
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
měla	mít	k5eAaImAgFnS	mít
kompletní	kompletní	k2eAgFnSc4d1	kompletní
palubu	paluba	k1gFnSc4	paluba
(	(	kIx(	(
<g/>
menší	malý	k2eAgFnPc1d2	menší
lodě	loď	k1gFnPc1	loď
měly	mít	k5eAaImAgFnP	mít
pouze	pouze	k6eAd1	pouze
částečně	částečně	k6eAd1	částečně
podpalubí	podpalubí	k1gNnSc1	podpalubí
<g/>
)	)	kIx)	)
a	a	k8xC	a
tři	tři	k4xCgInPc4	tři
stěžně	stěžeň	k1gInPc4	stěžeň
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
lodi	loď	k1gFnSc2	loď
není	být	k5eNaImIp3nS	být
nic	nic	k3yNnSc1	nic
bližšího	bližší	k1gNnSc2	bližší
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
když	když	k8xS	když
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
její	její	k3xOp3gFnSc2	její
existence	existence	k1gFnSc2	existence
se	se	k3xPyFc4	se
nedochovala	dochovat	k5eNaPmAgFnS	dochovat
žádná	žádný	k3yNgFnSc1	žádný
dokumentace	dokumentace	k1gFnSc1	dokumentace
a	a	k8xC	a
ani	ani	k8xC	ani
pouhé	pouhý	k2eAgNnSc1d1	pouhé
zobrazení	zobrazení	k1gNnSc1	zobrazení
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Rozměry	rozměr	k1gInPc1	rozměr
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
pouze	pouze	k6eAd1	pouze
odhadovány	odhadovat	k5eAaImNgFnP	odhadovat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
byla	být	k5eAaImAgFnS	být
loď	loď	k1gFnSc1	loď
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
asi	asi	k9	asi
40	[number]	k4	40
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
široká	široký	k2eAgFnSc1d1	široká
8	[number]	k4	8
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
Podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
měla	mít	k5eAaImAgFnS	mít
loď	loď	k1gFnSc1	loď
délku	délka	k1gFnSc4	délka
22,6	[number]	k4	22,6
metru	metr	k1gInSc2	metr
<g/>
,	,	kIx,	,
šířku	šířka	k1gFnSc4	šířka
7,6	[number]	k4	7,6
metru	metr	k1gInSc2	metr
a	a	k8xC	a
ponor	ponor	k1gInSc1	ponor
2,1	[number]	k4	2,1
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ztroskotání	ztroskotání	k1gNnSc2	ztroskotání
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1492	[number]	k4	1492
se	se	k3xPyFc4	se
loď	loď	k1gFnSc1	loď
plavila	plavit	k5eAaImAgFnS	plavit
podél	podél	k7c2	podél
Haiti	Haiti	k1gNnSc2	Haiti
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
u	u	k7c2	u
kormidla	kormidlo	k1gNnSc2	kormidlo
zbyl	zbýt	k5eAaPmAgMnS	zbýt
pouze	pouze	k6eAd1	pouze
plavčík	plavčík	k1gMnSc1	plavčík
<g/>
;	;	kIx,	;
ostatní	ostatní	k2eAgMnPc1d1	ostatní
členové	člen	k1gMnPc1	člen
posádky	posádka	k1gFnSc2	posádka
odpočívali	odpočívat	k5eAaImAgMnP	odpočívat
po	po	k7c6	po
bouřlivých	bouřlivý	k2eAgFnPc6d1	bouřlivá
vánočních	vánoční	k2eAgFnPc6d1	vánoční
oslavách	oslava	k1gFnPc6	oslava
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
tak	tak	k9	tak
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
najela	najet	k5eAaPmAgFnS	najet
na	na	k7c4	na
mělčinu	mělčina	k1gFnSc4	mělčina
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
dnešního	dnešní	k2eAgNnSc2d1	dnešní
města	město	k1gNnSc2	město
Limonade	Limonad	k1gInSc5	Limonad
a	a	k8xC	a
dalšího	další	k2eAgInSc2d1	další
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
potopila	potopit	k5eAaPmAgFnS	potopit
<g/>
.	.	kIx.	.
</s>
<s>
Posádka	posádka	k1gFnSc1	posádka
se	se	k3xPyFc4	se
zachránila	zachránit	k5eAaPmAgFnS	zachránit
<g/>
,	,	kIx,	,
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
posádky	posádka	k1gFnSc2	posádka
založila	založit	k5eAaPmAgFnS	založit
kolonii	kolonie	k1gFnSc4	kolonie
La	la	k1gNnSc2	la
Navidad	Navidad	k1gInSc1	Navidad
(	(	kIx(	(
<g/>
Vánoce	Vánoce	k1gFnPc1	Vánoce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vrak	vrak	k1gInSc1	vrak
lodi	loď	k1gFnSc2	loď
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
velké	velký	k2eAgFnSc2d1	velká
míry	míra	k1gFnSc2	míra
rozebrán	rozebrat	k5eAaPmNgMnS	rozebrat
a	a	k8xC	a
použit	použít	k5eAaPmNgInS	použít
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
La	la	k1gNnSc2	la
Navidad	Navidad	k1gInSc1	Navidad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kotva	kotva	k1gFnSc1	kotva
lodi	loď	k1gFnSc2	loď
se	se	k3xPyFc4	se
dochovala	dochovat	k5eAaPmAgFnS	dochovat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vystavena	vystavit	k5eAaPmNgFnS	vystavit
v	v	k7c6	v
Musée	Musée	k1gNnSc6	Musée
du	du	k?	du
Panthéon	Panthéon	k1gMnSc1	Panthéon
National	National	k1gMnSc1	National
Haï	Haï	k1gMnSc1	Haï
(	(	kIx(	(
<g/>
MUPANAH	MUPANAH	kA	MUPANAH
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
haitském	haitský	k2eAgInSc6d1	haitský
Port-au-Prince	Portu-Prinec	k1gInPc1	Port-au-Prinec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2014	[number]	k4	2014
podmořský	podmořský	k2eAgMnSc1d1	podmořský
archeolog	archeolog	k1gMnSc1	archeolog
Barry	Barra	k1gFnSc2	Barra
Clifton	Clifton	k1gInSc1	Clifton
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc4	jeho
tým	tým	k1gInSc4	tým
ztotožnili	ztotožnit	k5eAaPmAgMnP	ztotožnit
vrak	vrak	k1gInSc4	vrak
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
s	s	k7c7	s
Kolumbovou	Kolumbův	k2eAgFnSc7d1	Kolumbova
vlajkovou	vlajkový	k2eAgFnSc7d1	vlajková
lodí	loď	k1gFnSc7	loď
Santa	Sant	k1gMnSc2	Sant
Maria	Mario	k1gMnSc2	Mario
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
expertů	expert	k1gMnPc2	expert
UNESCO	UNESCO	kA	UNESCO
<g/>
,	,	kIx,	,
vyslaný	vyslaný	k2eAgMnSc1d1	vyslaný
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
haitskou	haitský	k2eAgFnSc7d1	Haitská
vládou	vláda	k1gFnSc7	vláda
<g/>
,	,	kIx,	,
však	však	k9	však
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2014	[number]	k4	2014
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
Kolumbovu	Kolumbův	k2eAgFnSc4d1	Kolumbova
loď	loď	k1gFnSc4	loď
a	a	k8xC	a
datoval	datovat	k5eAaImAgInS	datovat
objevený	objevený	k2eAgInSc1d1	objevený
vrak	vrak	k1gInSc1	vrak
na	na	k7c4	na
konec	konec	k1gInSc4	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
či	či	k8xC	či
začátek	začátek	k1gInSc1	začátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
stolení	stolení	k1gNnSc6	stolení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Repliky	replika	k1gFnSc2	replika
==	==	k?	==
</s>
</p>
<p>
<s>
Repliky	replika	k1gFnPc1	replika
lodi	loď	k1gFnSc2	loď
Santa	Santa	k1gMnSc1	Santa
María	María	k1gMnSc1	María
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
I	i	k9	i
přes	přes	k7c4	přes
nejasný	jasný	k2eNgInSc4d1	nejasný
vzhled	vzhled	k1gInSc4	vzhled
a	a	k8xC	a
rozměry	rozměr	k1gInPc4	rozměr
lodi	loď	k1gFnSc2	loď
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
hned	hned	k6eAd1	hned
několik	několik	k4yIc1	několik
jejích	její	k3xOp3gFnPc2	její
replik	replika	k1gFnPc2	replika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
replik	replika	k1gFnPc2	replika
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
pro	pro	k7c4	pro
španělskou	španělský	k2eAgFnSc4d1	španělská
vládu	vláda	k1gFnSc4	vláda
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1892	[number]	k4	1892
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
z	z	k7c2	z
replik	replika	k1gFnPc2	replika
byla	být	k5eAaImAgFnS	být
vyrobena	vyrobit	k5eAaPmNgFnS	vyrobit
pro	pro	k7c4	pro
výstavu	výstava	k1gFnSc4	výstava
Expo	Expo	k1gNnSc1	Expo
86	[number]	k4	86
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
nákupním	nákupní	k2eAgNnSc6d1	nákupní
centru	centrum	k1gNnSc6	centrum
v	v	k7c6	v
Edmontonu	Edmonton	k1gInSc6	Edmonton
<g/>
.	.	kIx.	.
</s>
<s>
Jinou	jiný	k2eAgFnSc4d1	jiná
repliku	replika	k1gFnSc4	replika
si	se	k3xPyFc3	se
nechalo	nechat	k5eAaPmAgNnS	nechat
postavit	postavit	k5eAaPmF	postavit
americké	americký	k2eAgNnSc1d1	americké
město	město	k1gNnSc1	město
Columbus	Columbus	k1gInSc1	Columbus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
replika	replika	k1gFnSc1	replika
byla	být	k5eAaImAgFnS	být
vyrobena	vyrobit	k5eAaPmNgFnS	vyrobit
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
oslavy	oslava	k1gFnSc2	oslava
500	[number]	k4	500
let	léto	k1gNnPc2	léto
objevení	objevení	k1gNnSc2	objevení
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
kotví	kotvit	k5eAaImIp3nP	kotvit
ve	v	k7c6	v
španělském	španělský	k2eAgInSc6d1	španělský
Palos	Palos	k1gInSc1	Palos
de	de	k?	de
la	la	k1gNnSc2	la
Frontera	Fronter	k1gMnSc2	Fronter
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
replika	replika	k1gFnSc1	replika
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1997	[number]	k4	1997
a	a	k8xC	a
1998	[number]	k4	1998
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Madeira	Madeira	k1gFnSc1	Madeira
a	a	k8xC	a
kotví	kotvit	k5eAaImIp3nS	kotvit
ve	v	k7c6	v
Funchalu	Funchal	k1gInSc6	Funchal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Karavela	karavela	k1gFnSc1	karavela
</s>
</p>
<p>
<s>
Zámořské	zámořský	k2eAgInPc4d1	zámořský
objevy	objev	k1gInPc4	objev
</s>
</p>
