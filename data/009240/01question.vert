<s>
Byla	být	k5eAaImAgFnS	být
Santa	Santa	k1gFnSc1	Santa
María	María	k1gMnSc1	María
největší	veliký	k2eAgMnSc1d3	veliký
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
lodí	loď	k1gFnPc2	loď
použitých	použitý	k2eAgFnPc2d1	použitá
Kryštofem	Kryštof	k1gMnSc7	Kryštof
Kolumbem	Kolumbus	k1gMnSc7	Kolumbus
při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
první	první	k4xOgFnSc6	první
cestě	cesta	k1gFnSc6	cesta
přes	přes	k7c4	přes
Atlantský	atlantský	k2eAgInSc4d1	atlantský
oceán	oceán	k1gInSc4	oceán
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1492	[number]	k4	1492
<g/>
?	?	kIx.	?
</s>
