<s desamb="1">
Tento	tento	k3xDgInSc1
nejstarší	starý	k2eAgMnSc1d3
známý	známý	k2eAgMnSc1d1
archosaur	archosaur	k1gMnSc1
z	z	k7c2
„	„	k?
<g/>
ptačí	ptačí	k2eAgFnSc2d1
vývojové	vývojový	k2eAgFnSc2d1
linie	linie	k1gFnSc2
<g/>
“	“	k?
žil	žít	k5eAaImAgMnS
zhruba	zhruba	k6eAd1
před	před	k7c7
243	#num#	k4
miliony	milion	k4xCgInPc7
let	rok	k1gInPc2
(	(	kIx(
<g/>
období	období	k1gNnSc1
středního	střední	k2eAgInSc2d1
triasu	trias	k1gInSc2
<g/>
,	,	kIx,
věk	věk	k1gInSc1
anis	anis	k1gInSc1
<g/>
)	)	kIx)
na	na	k7c6
území	území	k1gNnSc6
dnešní	dnešní	k2eAgFnSc2d1
Tanzanie	Tanzanie	k1gFnSc2
<g/>
.	.	kIx.
</s>