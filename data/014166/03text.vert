<s>
Asilisaurus	Asilisaurus	k1gMnSc1
</s>
<s>
AsilisaurusStratigrafický	AsilisaurusStratigrafický	k2eAgInSc1d1
výskyt	výskyt	k1gInSc1
<g/>
:	:	kIx,
Střední	střední	k2eAgInSc1d1
trias	trias	k1gInSc1
<g/>
,	,	kIx,
asi	asi	k9
před	před	k7c7
243	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
Rekonstrukce	rekonstrukce	k1gFnSc1
druhu	druh	k1gInSc2
Asilisaurus	Asilisaurus	k1gInSc4
kongwe	kongwe	k6eAd1
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
plazi	plaz	k1gMnPc1
(	(	kIx(
<g/>
Sauropsida	Sauropsida	k1gFnSc1
<g/>
)	)	kIx)
Infratřída	Infratřída	k1gFnSc1
</s>
<s>
Archosauromorpha	Archosauromorpha	k1gFnSc1
Nadřád	nadřád	k1gInSc1
</s>
<s>
archosauři	archosauř	k1gFnSc3
(	(	kIx(
<g/>
Archosauria	Archosaurium	k1gNnSc2
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
Silesauridae	Silesauridae	k6eAd1
Rod	rod	k1gInSc1
</s>
<s>
Asilisaurus	Asilisaurus	k1gMnSc1
Binomické	binomický	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Asilisaurus	Asilisaurus	k1gMnSc1
kongweNesbitt	kongweNesbitta	k1gFnPc2
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2010	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Asilisaurus	Asilisaurus	k1gInSc1
(	(	kIx(
<g/>
"	"	kIx"
<g/>
ještěr	ještěr	k1gMnSc1
předchůdce	předchůdce	k1gMnSc1
<g/>
"	"	kIx"
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
rod	rod	k1gInSc1
silesauridního	silesauridní	k2eAgMnSc2d1
archosaura	archosaur	k1gMnSc2
(	(	kIx(
<g/>
dinosauriforma	dinosauriform	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
nejstarší	starý	k2eAgMnSc1d3
známý	známý	k2eAgMnSc1d1
archosaur	archosaur	k1gMnSc1
z	z	k7c2
„	„	k?
<g/>
ptačí	ptačí	k2eAgFnSc2d1
vývojové	vývojový	k2eAgFnSc2d1
linie	linie	k1gFnSc2
<g/>
“	“	k?
žil	žít	k5eAaImAgMnS
zhruba	zhruba	k6eAd1
před	před	k7c7
243	#num#	k4
miliony	milion	k4xCgInPc7
let	rok	k1gInPc2
(	(	kIx(
<g/>
období	období	k1gNnSc1
středního	střední	k2eAgInSc2d1
triasu	trias	k1gInSc2
<g/>
,	,	kIx,
věk	věk	k1gInSc1
anis	anis	k1gInSc1
<g/>
)	)	kIx)
na	na	k7c6
území	území	k1gNnSc6
dnešní	dnešní	k2eAgFnSc2d1
Tanzanie	Tanzanie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
popsán	popsat	k5eAaPmNgInS
podle	podle	k7c2
fosilií	fosilie	k1gFnPc2
asi	asi	k9
14	#num#	k4
jedinců	jedinec	k1gMnPc2
mezinárodním	mezinárodní	k2eAgInSc7d1
týmem	tým	k1gInSc7
paleontologů	paleontolog	k1gMnPc2
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typovým	typový	k2eAgInSc7d1
druhem	druh	k1gInSc7
je	být	k5eAaImIp3nS
A.	A.	kA
kongwe	kongwe	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Asilisaurus	Asilisaurus	k1gMnSc1
dosahoval	dosahovat	k5eAaImAgMnS
délky	délka	k1gFnPc4
asi	asi	k9
1	#num#	k4
až	až	k9
3	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
výšky	výška	k1gFnPc1
v	v	k7c6
kyčlích	kyčel	k1gInPc6
0,5	0,5	k4
až	až	k9
1	#num#	k4
metru	metr	k1gInSc2
a	a	k8xC
hmotnosti	hmotnost	k1gFnSc2
od	od	k7c2
10	#num#	k4
do	do	k7c2
30	#num#	k4
kilogramů	kilogram	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
jiných	jiný	k2eAgInPc2d1
odhadů	odhad	k1gInPc2
byl	být	k5eAaImAgInS
však	však	k9
dlouhý	dlouhý	k2eAgInSc1d1
jen	jen	k9
kolem	kolem	k7c2
1,2	1,2	k4
metru	metr	k1gInSc2
a	a	k8xC
dosahoval	dosahovat	k5eAaImAgMnS
hmotnosti	hmotnost	k1gFnPc4
asi	asi	k9
do	do	k7c2
tří	tři	k4xCgInPc2
kilogramů	kilogram	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
starším	starý	k2eAgMnPc3d2
příbuzným	příbuzný	k1gMnPc3
zhruba	zhruba	k6eAd1
stejně	stejně	k6eAd1
velikého	veliký	k2eAgMnSc2d1
silesaura	silesaur	k1gMnSc2
z	z	k7c2
polského	polský	k2eAgInSc2d1
Krasiejowa	Krasiejow	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Asilisaurus	Asilisaurus	k1gInSc1
kongwe	kongwe	k1gInSc1
-	-	kIx~
kostra	kostra	k1gFnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Holtz	Holtz	k1gMnSc1
<g/>
,	,	kIx,
Thomas	Thomas	k1gMnSc1
R.	R.	kA
<g/>
,	,	kIx,
Jr	Jr	k1gFnSc1
<g/>
.	.	kIx.
<g/>
;	;	kIx,
Rey	Rea	k1gFnSc2
<g/>
,	,	kIx,
Luis	Luisa	k1gFnPc2
V.	V.	kA
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dinosaurs	Dinosaurs	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Most	most	k1gInSc4
Complete	Comple	k1gNnSc2
<g/>
,	,	kIx,
Up-to-Date	Up-to-Dat	k1gInSc5
Encyclopedia	Encyclopedium	k1gNnPc1
for	forum	k1gNnPc2
Dinosaur	Dinosaura	k1gFnPc2
Lovers	Loversa	k1gFnPc2
of	of	k?
All	All	k1gFnSc2
Ages	Agesa	k1gFnPc2
(	(	kIx(
<g/>
Aktualizovaný	aktualizovaný	k2eAgInSc1d1
internetový	internetový	k2eAgInSc1d1
dodatek	dodatek	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Random	Random	k1gInSc1
House	house	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
375	#num#	k4
<g/>
-	-	kIx~
<g/>
82419	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Nesbitt	Nesbitt	k1gMnSc1
<g/>
,	,	kIx,
S.	S.	kA
J.	J.	kA
<g/>
;	;	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
“	“	k?
<g/>
Ecologically	Ecologicalla	k1gFnSc2
distinct	distinct	k1gMnSc1
dinosaurian	dinosaurian	k1gMnSc1
sister	sister	k1gMnSc1
group	group	k1gMnSc1
shows	shows	k6eAd1
early	earl	k1gMnPc4
diversification	diversification	k1gInSc1
of	of	k?
Ornithodira	Ornithodir	k1gInSc2
<g/>
”	”	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
464	#num#	k4
(	(	kIx(
<g/>
7285	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
95	#num#	k4
<g/>
–	–	k?
<g/>
98	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Nesbitt	Nesbitt	k1gMnSc1
<g/>
,	,	kIx,
S.	S.	kA
J.	J.	kA
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
early	earl	k1gMnPc4
evolution	evolution	k1gInSc4
of	of	k?
archosaurs	archosaurs	k1gInSc1
<g/>
:	:	kIx,
relationships	relationships	k1gInSc1
and	and	k?
the	the	k?
origin	origin	k1gMnSc1
of	of	k?
major	major	k1gMnSc1
clades	clades	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bulletin	bulletin	k1gInSc1
of	of	k?
the	the	k?
American	American	k1gMnSc1
Museum	museum	k1gNnSc1
of	of	k?
Natural	Natural	k?
History	Histor	k1gMnPc7
<g/>
,	,	kIx,
353	#num#	k4
<g/>
:	:	kIx,
1	#num#	k4
<g/>
-	-	kIx~
<g/>
292	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Griffin	Griffin	k1gMnSc1
<g/>
,	,	kIx,
C.	C.	kA
T.	T.	kA
<g/>
;	;	kIx,
Nesbitt	Nesbitt	k1gMnSc1
<g/>
,	,	kIx,
Sterling	sterling	k1gInSc1
J.	J.	kA
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
The	The	k1gMnSc1
femoral	femorat	k5eAaImAgMnS,k5eAaPmAgMnS
ontogeny	ontogen	k1gInPc4
and	and	k?
long	long	k1gInSc1
bone	bon	k1gInSc5
histology	histolog	k1gMnPc4
of	of	k?
the	the	k?
Middle	Middle	k1gMnSc1
Triassic	Triassic	k1gMnSc1
(	(	kIx(
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
late	lat	k1gInSc5
Anisian	Anisian	k1gInSc1
<g/>
)	)	kIx)
dinosauriform	dinosauriform	k1gInSc1
Asilisaurus	Asilisaurus	k1gMnSc1
kongwe	kongwe	k1gFnPc2
and	and	k?
implications	implicationsa	k1gFnPc2
for	forum	k1gNnPc2
the	the	k?
growth	growth	k1gMnSc1
of	of	k?
early	earl	k1gMnPc4
dinosaurs	dinosaursa	k1gFnPc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Vertebrate	Vertebrat	k1gInSc5
Paleontology	paleontolog	k1gMnPc7
<g/>
.	.	kIx.
36	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
e	e	k0
<g/>
1111224	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
10.108	10.108	k4
<g/>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2724634.2016	2724634.2016	k4
<g/>
.1111224	.1111224	k4
</s>
<s>
Sterling	sterling	k1gInSc1
J.	J.	kA
Nesbitt	Nesbitt	k1gInSc1
<g/>
,	,	kIx,
Max	Max	k1gMnSc1
C.	C.	kA
Langer	Langer	k1gMnSc1
&	&	k?
Martin	Martin	k1gMnSc1
D.	D.	kA
Ezcurra	Ezcurra	k1gMnSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Anatomy	anatom	k1gMnPc4
of	of	k?
Asilisaurus	Asilisaurus	k1gInSc1
kongwe	kongwat	k5eAaPmIp3nS
<g/>
,	,	kIx,
a	a	k8xC
Dinosauriform	Dinosauriform	k1gInSc1
from	from	k1gMnSc1
the	the	k?
Lifua	Lifua	k1gMnSc1
Member	Member	k1gMnSc1
of	of	k?
the	the	k?
Manda	Manda	k1gFnSc1
Beds	Beds	k1gInSc1
(	(	kIx(
<g/>
~	~	kIx~
<g/>
Middle	Middle	k1gMnSc1
Triassic	Triassic	k1gMnSc1
<g/>
)	)	kIx)
of	of	k?
Africa	Africa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Anatomical	Anatomical	k1gMnSc1
Record	Record	k1gMnSc1
(	(	kIx(
<g/>
advance	advance	k1gFnSc1
online	onlinout	k5eAaPmIp3nS
publication	publication	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1002/ar.24287	https://doi.org/10.1002/ar.24287	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Informace	informace	k1gFnPc1
na	na	k7c6
webu	web	k1gInSc6
Prehistoric	Prehistorice	k1gFnPc2
Wildlife	Wildlif	k1gInSc5
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Profil	profil	k1gInSc1
asilisaura	asilisaur	k1gMnSc2
na	na	k7c6
webu	web	k1gInSc6
Fossilworks	Fossilworksa	k1gFnPc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
byl	být	k5eAaImAgMnS
prvním	první	k4xOgMnSc7
dinosaurem	dinosaurus	k1gMnSc7
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSEL	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
