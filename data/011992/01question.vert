<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
obor	obor	k1gInSc1	obor
zabývající	zabývající	k2eAgInSc1d1	zabývající
se	s	k7c7	s
studií	studie	k1gFnSc7	studie
rostlin	rostlina	k1gFnPc2	rostlina
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
pěstováním	pěstování	k1gNnSc7	pěstování
rostlin	rostlina	k1gFnPc2	rostlina
pro	pro	k7c4	pro
uspokojení	uspokojení	k1gNnSc4	uspokojení
materiálních	materiální	k2eAgFnPc2d1	materiální
potřeb	potřeba	k1gFnPc2	potřeba
či	či	k8xC	či
z	z	k7c2	z
estetických	estetický	k2eAgInPc2d1	estetický
důvodů	důvod	k1gInPc2	důvod
<g/>
?	?	kIx.	?
</s>
