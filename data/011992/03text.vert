<p>
<s>
Zahradnictví	zahradnictví	k1gNnSc1	zahradnictví
je	být	k5eAaImIp3nS	být
obor	obor	k1gInSc1	obor
zabývající	zabývající	k2eAgInSc1d1	zabývající
se	s	k7c7	s
studií	studie	k1gFnSc7	studie
rostlin	rostlina	k1gFnPc2	rostlina
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
pěstování	pěstování	k1gNnSc3	pěstování
rostlin	rostlina	k1gFnPc2	rostlina
pro	pro	k7c4	pro
uspokojení	uspokojení	k1gNnSc4	uspokojení
materiálních	materiální	k2eAgFnPc2d1	materiální
potřeb	potřeba	k1gFnPc2	potřeba
(	(	kIx(	(
<g/>
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
,	,	kIx,	,
zeleniny	zelenina	k1gFnSc2	zelenina
<g/>
,	,	kIx,	,
koření	koření	k1gNnSc2	koření
<g/>
,	,	kIx,	,
bylin	bylina	k1gFnPc2	bylina
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ze	z	k7c2	z
sociálních	sociální	k2eAgInPc2d1	sociální
a	a	k8xC	a
estetických	estetický	k2eAgInPc2d1	estetický
důvodů	důvod	k1gInPc2	důvod
(	(	kIx(	(
<g/>
okrasné	okrasný	k2eAgFnPc1d1	okrasná
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
je	být	k5eAaImIp3nS	být
zahradnictví	zahradnictví	k1gNnSc1	zahradnictví
propojeno	propojit	k5eAaPmNgNnS	propojit
nejen	nejen	k6eAd1	nejen
s	s	k7c7	s
obory	obor	k1gInPc7	obor
technickými	technický	k2eAgInPc7d1	technický
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
s	s	k7c7	s
obory	obor	k1gInPc7	obor
humanitními	humanitní	k2eAgInPc7d1	humanitní
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
se	s	k7c7	s
zemědělstvím	zemědělství	k1gNnSc7	zemědělství
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
obvykle	obvykle	k6eAd1	obvykle
pěstováním	pěstování	k1gNnSc7	pěstování
na	na	k7c6	na
omezené	omezený	k2eAgFnSc6d1	omezená
ploše	plocha	k1gFnSc6	plocha
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaPmIp3nS	využívat
manuální	manuální	k2eAgFnSc1d1	manuální
práce	práce	k1gFnSc1	práce
pro	pro	k7c4	pro
kulinářské	kulinářský	k2eAgInPc4d1	kulinářský
a	a	k8xC	a
okrasné	okrasný	k2eAgInPc4d1	okrasný
výpěstky	výpěstek	k1gInPc4	výpěstek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vymezení	vymezení	k1gNnSc2	vymezení
pojmu	pojem	k1gInSc2	pojem
==	==	k?	==
</s>
</p>
<p>
<s>
Zahradnictví	zahradnictví	k1gNnSc1	zahradnictví
je	být	k5eAaImIp3nS	být
činnost	činnost	k1gFnSc1	činnost
prováděná	prováděný	k2eAgFnSc1d1	prováděná
v	v	k7c6	v
zahradách	zahrada	k1gFnPc6	zahrada
<g/>
,	,	kIx,	,
parcích	park	k1gInPc6	park
nebo	nebo	k8xC	nebo
zahradnictvích	zahradnictví	k1gNnPc6	zahradnictví
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
zahradnictví	zahradnictví	k1gNnSc1	zahradnictví
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
označen	označit	k5eAaPmNgInS	označit
druh	druh	k1gInSc1	druh
služby	služba	k1gFnSc2	služba
nebo	nebo	k8xC	nebo
o	o	k0	o
specializovaná	specializovaný	k2eAgFnSc1d1	specializovaná
firma	firma	k1gFnSc1	firma
zabývající	zabývající	k2eAgMnPc4d1	zabývající
se	se	k3xPyFc4	se
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
kroky	krok	k1gInPc7	krok
v	v	k7c6	v
pěstováním	pěstování	k1gNnPc3	pěstování
a	a	k8xC	a
prodeji	prodej	k1gInSc3	prodej
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Zaměstnanec	zaměstnanec	k1gMnSc1	zaměstnanec
takovéto	takovýto	k3xDgFnPc4	takovýto
firmy	firma	k1gFnPc4	firma
obvykle	obvykle	k6eAd1	obvykle
pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
zahradník	zahradník	k1gMnSc1	zahradník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
An	An	k?	An
Encyclopæ	Encyclopæ	k1gMnSc1	Encyclopæ
of	of	k?	of
Gardening	Gardening	k1gInSc1	Gardening
z	z	k7c2	z
devatenáctého	devatenáctý	k4xOgNnSc2	devatenáctý
století	století	k1gNnSc2	století
popisuje	popisovat	k5eAaImIp3nS	popisovat
zahradnictví	zahradnictví	k1gNnSc4	zahradnictví
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
vytváření	vytváření	k1gNnSc6	vytváření
<g/>
,	,	kIx,	,
vylepšování	vylepšování	k1gNnSc6	vylepšování
a	a	k8xC	a
využívání	využívání	k1gNnSc6	využívání
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
,	,	kIx,	,
zeleniny	zelenina	k1gFnSc2	zelenina
<g/>
,	,	kIx,	,
květin	květina	k1gFnPc2	květina
a	a	k8xC	a
okrasných	okrasný	k2eAgFnPc2d1	okrasná
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
botaniky	botanika	k1gFnSc2	botanika
zahradnictví	zahradnictví	k1gNnSc2	zahradnictví
spojuje	spojovat	k5eAaImIp3nS	spojovat
vědu	věda	k1gFnSc4	věda
s	s	k7c7	s
estetikou	estetika	k1gFnSc7	estetika
<g/>
.	.	kIx.	.
</s>
<s>
Zahradnictvím	zahradnictví	k1gNnSc7	zahradnictví
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
jednotlivci	jednotlivec	k1gMnPc1	jednotlivec
i	i	k8xC	i
nadnárodních	nadnárodní	k2eAgMnPc2d1	nadnárodní
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zahradnictví	zahradnictví	k1gNnSc1	zahradnictví
je	být	k5eAaImIp3nS	být
různorodý	různorodý	k2eAgInSc4d1	různorodý
obor	obor	k1gInSc4	obor
lidské	lidský	k2eAgFnSc2d1	lidská
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
jednak	jednak	k8xC	jednak
použití	použití	k1gNnSc4	použití
rostlin	rostlina	k1gFnPc2	rostlina
jako	jako	k8xC	jako
potraviny	potravina	k1gFnPc4	potravina
(	(	kIx(	(
<g/>
ovoce	ovoce	k1gNnSc1	ovoce
<g/>
,	,	kIx,	,
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
,	,	kIx,	,
houby	houba	k1gFnPc1	houba
a	a	k8xC	a
byliny	bylina	k1gFnPc1	bylina
pro	pro	k7c4	pro
kuchařské	kuchařský	k2eAgInPc4d1	kuchařský
účely	účel	k1gInPc4	účel
<g/>
)	)	kIx)	)
a	a	k8xC	a
ozdoby	ozdoba	k1gFnPc1	ozdoba
(	(	kIx(	(
<g/>
květiny	květina	k1gFnPc1	květina
<g/>
,	,	kIx,	,
stromy	strom	k1gInPc1	strom
<g/>
,	,	kIx,	,
keře	keř	k1gInPc1	keř
<g/>
,	,	kIx,	,
chmel	chmel	k1gInSc1	chmel
<g/>
,	,	kIx,	,
trávníky	trávník	k1gInPc1	trávník
<g/>
)	)	kIx)	)
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
ochranu	ochrana	k1gFnSc4	ochrana
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
rekultivaci	rekultivace	k1gFnSc4	rekultivace
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
projektování	projektování	k1gNnSc2	projektování
<g/>
,	,	kIx,	,
zhotovování	zhotovování	k1gNnSc2	zhotovování
a	a	k8xC	a
údržba	údržba	k1gFnSc1	údržba
parků	park	k1gInPc2	park
a	a	k8xC	a
zahrad	zahrada	k1gFnPc2	zahrada
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Zahradnictví	zahradnictví	k1gNnSc1	zahradnictví
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
většina	většina	k1gFnSc1	většina
jiných	jiný	k2eAgNnPc2d1	jiné
řemesel	řemeslo	k1gNnPc2	řemeslo
a	a	k8xC	a
umění	umění	k1gNnPc2	umění
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
už	už	k9	už
v	v	k7c6	v
primitivních	primitivní	k2eAgNnPc6d1	primitivní
lidských	lidský	k2eAgNnPc6d1	lidské
společenstvích	společenství	k1gNnPc6	společenství
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
mimo	mimo	k7c4	mimo
sběr	sběr	k1gInSc4	sběr
plodů	plod	k1gInPc2	plod
začali	začít	k5eAaPmAgMnP	začít
živit	živit	k5eAaImF	živit
pěstováním	pěstování	k1gNnSc7	pěstování
těchto	tento	k3xDgInPc2	tento
plodů	plod	k1gInPc2	plod
<g/>
.	.	kIx.	.
</s>
<s>
Antropologie	antropologie	k1gFnSc1	antropologie
i	i	k9	i
u	u	k7c2	u
dnes	dnes	k6eAd1	dnes
žijících	žijící	k2eAgInPc2d1	žijící
kmenů	kmen	k1gInPc2	kmen
rozeznává	rozeznávat	k5eAaImIp3nS	rozeznávat
skupiny	skupina	k1gFnPc4	skupina
zahradníků	zahradník	k1gMnPc2	zahradník
nazývaných	nazývaný	k2eAgFnPc2d1	nazývaná
antropologií	antropologie	k1gFnPc2	antropologie
hortikulturalisté	hortikulturalista	k1gMnPc1	hortikulturalista
jako	jako	k8xC	jako
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
typů	typ	k1gInPc2	typ
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
antropologických	antropologický	k2eAgNnPc2d1	antropologické
pozorování	pozorování	k1gNnPc2	pozorování
mají	mít	k5eAaImIp3nP	mít
sklon	sklon	k1gInSc4	sklon
tendenci	tendence	k1gFnSc4	tendence
se	se	k3xPyFc4	se
organizovat	organizovat	k5eAaBmF	organizovat
do	do	k7c2	do
kmenových	kmenový	k2eAgNnPc2d1	kmenové
společenství	společenství	k1gNnPc2	společenství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Záhony	záhon	k1gInPc1	záhon
s	s	k7c7	s
bylinkami	bylinka	k1gFnPc7	bylinka
<g/>
,	,	kIx,	,
keři	keř	k1gInPc7	keř
a	a	k8xC	a
zeleninou	zelenina	k1gFnSc7	zelenina
před	před	k7c7	před
dveřmi	dveře	k1gFnPc7	dveře
chatrče	chatrč	k1gFnSc2	chatrč
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
společného	společný	k2eAgInSc2d1	společný
majetku	majetek	k1gInSc2	majetek
se	se	k3xPyFc4	se
během	běh	k1gInSc7	běh
tisíců	tisíc	k4xCgInPc2	tisíc
let	léto	k1gNnPc2	léto
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
v	v	k7c4	v
několik	několik	k4yIc4	několik
kilometrů	kilometr	k1gInPc2	kilometr
velký	velký	k2eAgInSc1d1	velký
park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc1	jehož
hranice	hranice	k1gFnPc1	hranice
se	se	k3xPyFc4	se
stírají	stírat	k5eAaImIp3nP	stírat
v	v	k7c6	v
lese	les	k1gInSc6	les
a	a	k8xC	a
v	v	k7c6	v
polích	pole	k1gFnPc6	pole
u	u	k7c2	u
vesnic	vesnice	k1gFnPc2	vesnice
okolo	okolo	k7c2	okolo
paláce	palác	k1gInSc2	palác
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Starověk	starověk	k1gInSc4	starověk
===	===	k?	===
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgFnPc1d3	nejstarší
zmiňované	zmiňovaný	k2eAgFnPc1d1	zmiňovaná
zahrady	zahrada	k1gFnPc1	zahrada
starověku	starověk	k1gInSc2	starověk
jsou	být	k5eAaImIp3nP	být
omezeny	omezit	k5eAaPmNgFnP	omezit
na	na	k7c4	na
báječné	báječný	k2eAgInPc4d1	báječný
výtvory	výtvor	k1gInPc4	výtvor
fantazie	fantazie	k1gFnSc2	fantazie
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
údajných	údajný	k2eAgFnPc2d1	údajná
inscenací	inscenace	k1gFnPc2	inscenace
princů	princ	k1gMnPc2	princ
</s>
</p>
<p>
<s>
a	a	k8xC	a
válečníci	válečník	k1gMnPc1	válečník
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
zdroje	zdroj	k1gInPc1	zdroj
jsou	být	k5eAaImIp3nP	být
legendy	legenda	k1gFnPc1	legenda
jenž	jenž	k3xRgMnSc1	jenž
zmiňují	zmiňovat	k5eAaImIp3nP	zmiňovat
rajskou	rajský	k2eAgFnSc4d1	rajská
zahradu	zahrada	k1gFnSc4	zahrada
a	a	k8xC	a
zahrady	zahrada	k1gFnPc4	zahrada
Hesperidek	Hesperidka	k1gFnPc2	Hesperidka
<g/>
,	,	kIx,	,
zahrady	zahrada	k1gFnSc2	zahrada
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
Babyloňanů	Babyloňan	k1gMnPc2	Babyloňan
<g/>
,	,	kIx,	,
Peršanů	Peršan	k1gMnPc2	Peršan
a	a	k8xC	a
Řeků	Řek	k1gMnPc2	Řek
<g/>
.	.	kIx.	.
<g/>
Pohádkové	pohádkový	k2eAgFnSc2d1	pohádková
zahrady	zahrada	k1gFnSc2	zahrada
starověku	starověk	k1gInSc2	starověk
jsou	být	k5eAaImIp3nP	být
spojeny	spojen	k2eAgInPc1d1	spojen
s	s	k7c7	s
náboženstvím	náboženství	k1gNnSc7	náboženství
těch	ten	k3xDgInPc2	ten
časů	čas	k1gInPc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
náboženství	náboženství	k1gNnSc2	náboženství
byly	být	k5eAaImAgInP	být
uspořádány	uspořádán	k2eAgMnPc4d1	uspořádán
filozofy	filozof	k1gMnPc4	filozof
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
směrech	směr	k1gInPc6	směr
barbarství	barbarství	k1gNnSc2	barbarství
<g/>
,	,	kIx,	,
skytská	skytský	k2eAgFnSc1d1	Skytská
filosofie	filosofie	k1gFnSc1	filosofie
<g/>
,	,	kIx,	,
a	a	k8xC	a
helénismus	helénismus	k1gInSc1	helénismus
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
posledně	posledně	k6eAd1	posledně
jmenované	jmenovaný	k2eAgFnSc3d1	jmenovaná
filosofii	filosofie	k1gFnSc3	filosofie
patří	patřit	k5eAaImIp3nS	patřit
hebrejská	hebrejský	k2eAgFnSc1d1	hebrejská
<g/>
,	,	kIx,	,
řecká	řecký	k2eAgFnSc1d1	řecká
a	a	k8xC	a
muslimská	muslimský	k2eAgFnSc1d1	muslimská
víra	víra	k1gFnSc1	víra
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
ideologií	ideologie	k1gFnPc2	ideologie
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
systém	systém	k1gInSc4	systém
stvoření	stvoření	k1gNnSc2	stvoření
<g/>
,	,	kIx,	,
nebe	nebe	k1gNnSc2	nebe
a	a	k8xC	a
peklo	peklo	k1gNnSc4	peklo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
směrů	směr	k1gInPc2	směr
má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
zahradu	zahrada	k1gFnSc4	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Zahrada	zahrada	k1gFnSc1	zahrada
a	a	k8xC	a
ráj	ráj	k1gInSc1	ráj
bývá	bývat	k5eAaImIp3nS	bývat
slíbenou	slíbený	k2eAgFnSc7d1	slíbená
odměnou	odměna	k1gFnSc7	odměna
za	za	k7c4	za
dobré	dobrý	k2eAgNnSc4d1	dobré
chování	chování	k1gNnSc4	chování
během	během	k7c2	během
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c4	mezi
starověké	starověký	k2eAgFnPc4d1	starověká
civilizace	civilizace	k1gFnPc4	civilizace
jsou	být	k5eAaImIp3nP	být
řazeny	řadit	k5eAaImNgFnP	řadit
první	první	k4xOgFnPc1	první
vyspělé	vyspělý	k2eAgFnPc1d1	vyspělá
lidské	lidský	k2eAgFnPc1d1	lidská
společnosti	společnost	k1gFnPc1	společnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
rozkvétaly	rozkvétat	k5eAaImAgFnP	rozkvétat
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
Sýrie	Sýrie	k1gFnSc2	Sýrie
<g/>
,	,	kIx,	,
Palestiny	Palestina	k1gFnSc2	Palestina
<g/>
,	,	kIx,	,
Fénicie	Fénicie	k1gFnSc2	Fénicie
<g/>
,	,	kIx,	,
Malé	Malé	k2eAgFnSc2d1	Malé
Asie	Asie	k1gFnSc2	Asie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
<g/>
,	,	kIx,	,
Íránské	íránský	k2eAgFnSc2d1	íránská
vysočiny	vysočina	k1gFnSc2	vysočina
<g/>
,	,	kIx,	,
Egypta	Egypt	k1gInSc2	Egypt
a	a	k8xC	a
Arábie	Arábie	k1gFnSc2	Arábie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
je	být	k5eAaImIp3nS	být
ohraničeno	ohraničit	k5eAaPmNgNnS	ohraničit
koncem	koncem	k7c2	koncem
pravěku	pravěk	k1gInSc2	pravěk
ve	v	k7c4	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
vpádem	vpád	k1gInSc7	vpád
Arabů	Arab	k1gMnPc2	Arab
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
zemědělci	zemědělec	k1gMnPc1	zemědělec
se	se	k3xPyFc4	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Předního	přední	k2eAgInSc2d1	přední
východu	východ	k1gInSc2	východ
usadili	usadit	k5eAaPmAgMnP	usadit
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
a	a	k8xC	a
9	[number]	k4	9
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
7700	[number]	k4	7700
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
utvářet	utvářet	k5eAaImF	utvářet
první	první	k4xOgFnPc1	první
usedlé	usedlý	k2eAgFnPc1d1	usedlá
agrární	agrární	k2eAgFnPc1d1	agrární
společnosti	společnost	k1gFnPc1	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
zde	zde	k6eAd1	zde
nejstarší	starý	k2eAgFnSc1d3	nejstarší
známá	známá	k1gFnSc1	známá
města	město	k1gNnSc2	město
světa	svět	k1gInSc2	svět
a	a	k8xC	a
také	také	k6eAd1	také
vznikaly	vznikat	k5eAaImAgInP	vznikat
první	první	k4xOgFnPc4	první
zahrady	zahrada	k1gFnPc4	zahrada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Visuté	visutý	k2eAgFnPc1d1	visutá
zahrady	zahrada	k1gFnPc1	zahrada
Semiramidiny	Semiramidin	k2eAgFnPc1d1	Semiramidin
i	i	k8xC	i
nad	nad	k7c7	nad
většími	veliký	k2eAgFnPc7d2	veliký
a	a	k8xC	a
nákladnějšími	nákladný	k2eAgFnPc7d2	nákladnější
stavbami	stavba	k1gFnPc7	stavba
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
podle	podle	k7c2	podle
popisů	popis	k1gInPc2	popis
vynikají	vynikat	k5eAaImIp3nP	vynikat
originalitou	originalita	k1gFnSc7	originalita
<g/>
,	,	kIx,	,
jedinečností	jedinečnost	k1gFnSc7	jedinečnost
<g/>
,	,	kIx,	,
technikou	technika	k1gFnSc7	technika
<g/>
,	,	kIx,	,
řešením	řešení	k1gNnSc7	řešení
stavby	stavba	k1gFnSc2	stavba
<g/>
,	,	kIx,	,
důmyslností	důmyslnost	k1gFnPc2	důmyslnost
čerpacího	čerpací	k2eAgNnSc2d1	čerpací
zařízení	zařízení	k1gNnSc2	zařízení
a	a	k8xC	a
krásou	krása	k1gFnSc7	krása
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Egyptská	egyptský	k2eAgFnSc1d1	egyptská
zahrada	zahrada	k1gFnSc1	zahrada
je	být	k5eAaImIp3nS	být
poprvé	poprvé	k6eAd1	poprvé
zmiňována	zmiňovat	k5eAaImNgFnS	zmiňovat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
asi	asi	k9	asi
4000	[number]	k4	4000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
se	se	k3xPyFc4	se
na	na	k7c6	na
malbách	malba	k1gFnPc6	malba
v	v	k7c6	v
hrobkách	hrobka	k1gFnPc6	hrobka
nachází	nacházet	k5eAaImIp3nS	nacházet
zobrazení	zobrazení	k1gNnSc1	zobrazení
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
vzhled	vzhled	k1gInSc1	vzhled
je	být	k5eAaImIp3nS	být
podrobně	podrobně	k6eAd1	podrobně
propracovaný	propracovaný	k2eAgInSc1d1	propracovaný
<g/>
.	.	kIx.	.
</s>
<s>
Stěny	stěna	k1gFnPc1	stěna
obsahovaly	obsahovat	k5eAaImAgFnP	obsahovat
i	i	k8xC	i
obrazy	obraz	k1gInPc7	obraz
sběru	sběr	k1gInSc2	sběr
ovoce	ovoce	k1gNnSc2	ovoce
a	a	k8xC	a
květinových	květinový	k2eAgFnPc2d1	květinová
dekorací	dekorace	k1gFnPc2	dekorace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
bylo	být	k5eAaImAgNnS	být
běžné	běžný	k2eAgNnSc1d1	běžné
aranžování	aranžování	k1gNnSc1	aranžování
květin	květina	k1gFnPc2	květina
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
zahrady	zahrada	k1gFnSc2	zahrada
býval	bývat	k5eAaImAgInS	bývat
zavlažovací	zavlažovací	k2eAgInSc1d1	zavlažovací
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
bazén	bazén	k1gInSc1	bazén
nebo	nebo	k8xC	nebo
také	také	k9	také
vodotrysky	vodotrysk	k1gInPc1	vodotrysk
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
egyptských	egyptský	k2eAgFnPc6d1	egyptská
zahradách	zahrada	k1gFnPc6	zahrada
byly	být	k5eAaImAgInP	být
vysázeny	vysázen	k2eAgInPc1d1	vysázen
i	i	k8xC	i
cizokrajné	cizokrajný	k2eAgInPc1d1	cizokrajný
stromy	strom	k1gInPc1	strom
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgMnPc7	jenž
byly	být	k5eAaImAgFnP	být
patrně	patrně	k6eAd1	patrně
i	i	k8xC	i
vzácné	vzácný	k2eAgInPc1d1	vzácný
druhy	druh	k1gInPc1	druh
<g/>
.	.	kIx.	.
</s>
<s>
Ovocné	ovocný	k2eAgInPc1d1	ovocný
druhy	druh	k1gInPc1	druh
byly	být	k5eAaImAgInP	být
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pěstovány	pěstován	k2eAgFnPc1d1	pěstována
a	a	k8xC	a
šlechtěny	šlechtěn	k2eAgFnPc1d1	šlechtěna
byly	být	k5eAaImAgFnP	být
i	i	k9	i
okrasné	okrasný	k2eAgInPc4d1	okrasný
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Staří	starý	k2eAgMnPc1d1	starý
Židé	Žid	k1gMnPc1	Žid
pěstovali	pěstovat	k5eAaImAgMnP	pěstovat
aromatické	aromatický	k2eAgFnPc4d1	aromatická
a	a	k8xC	a
léčivé	léčivý	k2eAgFnPc4d1	léčivá
byliny	bylina	k1gFnPc4	bylina
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
mandragoru	mandragora	k1gFnSc4	mandragora
<g/>
.	.	kIx.	.
</s>
<s>
Peršané	Peršan	k1gMnPc1	Peršan
a	a	k8xC	a
staří	starý	k2eAgMnPc1d1	starý
Řekové	Řek	k1gMnPc1	Řek
s	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
pěstovali	pěstovat	k5eAaImAgMnP	pěstovat
narcisy	narcisa	k1gFnPc4	narcisa
<g/>
,	,	kIx,	,
fialky	fialka	k1gFnPc4	fialka
<g/>
,	,	kIx,	,
břečťan	břečťan	k1gInSc4	břečťan
a	a	k8xC	a
růže	růž	k1gFnPc4	růž
<g/>
,	,	kIx,	,
obdivovali	obdivovat	k5eAaImAgMnP	obdivovat
také	také	k9	také
cypřiše	cypřiš	k1gFnSc2	cypřiš
a	a	k8xC	a
borovice	borovice	k1gFnSc2	borovice
<g/>
.	.	kIx.	.
</s>
<s>
Řekové	Řek	k1gMnPc1	Řek
používali	používat	k5eAaImAgMnP	používat
kytice	kytice	k1gFnPc4	kytice
<g/>
,	,	kIx,	,
věnce	věnec	k1gInPc4	věnec
a	a	k8xC	a
girlandy	girlanda	k1gFnPc4	girlanda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Římské	římský	k2eAgNnSc1d1	římské
zahradnictví	zahradnictví	k1gNnSc1	zahradnictví
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
znalo	znát	k5eAaImAgNnS	znát
nejen	nejen	k6eAd1	nejen
hnojení	hnojení	k1gNnSc1	hnojení
a	a	k8xC	a
používání	používání	k1gNnSc1	používání
různých	různý	k2eAgFnPc2d1	různá
zemin	zemina	k1gFnPc2	zemina
a	a	k8xC	a
příměsí	příměs	k1gFnPc2	příměs
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
systémy	systém	k1gInPc1	systém
vytápění	vytápění	k1gNnSc2	vytápění
<g/>
,	,	kIx,	,
chlazení	chlazení	k1gNnSc2	chlazení
<g/>
,	,	kIx,	,
rychlení	rychlení	k1gNnSc2	rychlení
<g/>
.	.	kIx.	.
</s>
<s>
Římští	římský	k2eAgMnPc1d1	římský
zahradníci	zahradník	k1gMnPc1	zahradník
chránili	chránit	k5eAaImAgMnP	chránit
teplomilné	teplomilný	k2eAgInPc4d1	teplomilný
druhy	druh	k1gInPc4	druh
rostlin	rostlina	k1gFnPc2	rostlina
před	před	k7c7	před
namrznutím	namrznutí	k1gNnSc7	namrznutí
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
meristémového	meristémový	k2eAgMnSc4d1	meristémový
množení	množení	k1gNnSc3	množení
znali	znát	k5eAaImAgMnP	znát
a	a	k8xC	a
používali	používat	k5eAaImAgMnP	používat
všechny	všechen	k3xTgInPc4	všechen
druhy	druh	k1gInPc4	druh
vegetativního	vegetativní	k2eAgNnSc2d1	vegetativní
množení	množení	k1gNnSc2	množení
včetně	včetně	k7c2	včetně
štěpování	štěpování	k1gNnSc2	štěpování
<g/>
,	,	kIx,	,
průklesty	průklest	k1gInPc1	průklest
<g/>
,	,	kIx,	,
řez	řez	k1gInSc1	řez
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ochranu	ochrana	k1gFnSc4	ochrana
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Pěstovali	pěstovat	k5eAaImAgMnP	pěstovat
množství	množství	k1gNnSc3	množství
odrůd	odrůda	k1gFnPc2	odrůda
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
ovoce	ovoce	k1gNnSc2	ovoce
a	a	k8xC	a
zeleniny	zelenina	k1gFnSc2	zelenina
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
ale	ale	k9	ale
révy	réva	k1gFnSc2	réva
<g/>
,	,	kIx,	,
slivoní	slivoň	k1gFnPc2	slivoň
<g/>
,	,	kIx,	,
třešní	třešeň	k1gFnPc2	třešeň
<g/>
,	,	kIx,	,
tykvovité	tykvovitý	k2eAgFnSc2d1	tykvovitá
a	a	k8xC	a
lilkovité	lilkovitý	k2eAgFnSc2d1	lilkovitá
zeleniny	zelenina	k1gFnSc2	zelenina
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
,	,	kIx,	,
oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
odrůdy	odrůda	k1gFnPc1	odrůda
měly	mít	k5eAaImAgFnP	mít
pojmenování	pojmenování	k1gNnSc4	pojmenování
<g/>
.	.	kIx.	.
</s>
<s>
Obliba	obliba	k1gFnSc1	obliba
květin	květina	k1gFnPc2	květina
dala	dát	k5eAaPmAgFnS	dát
základ	základ	k1gInSc1	základ
florimánii	florimánie	k1gFnSc4	florimánie
<g/>
.	.	kIx.	.
</s>
<s>
Římská	římský	k2eAgFnSc1d1	římská
záliba	záliba	k1gFnSc1	záliba
v	v	k7c6	v
zahradničení	zahradničený	k2eAgMnPc1d1	zahradničený
byla	být	k5eAaImAgNnP	být
odsuzována	odsuzován	k2eAgFnSc1d1	odsuzována
jako	jako	k8xS	jako
přepjatá	přepjatý	k2eAgFnSc1d1	přepjatá
<g/>
.	.	kIx.	.
<g/>
Vysokou	vysoká	k1gFnSc4	vysoká
estetickou	estetický	k2eAgFnSc4d1	estetická
úroveň	úroveň	k1gFnSc4	úroveň
postupně	postupně	k6eAd1	postupně
nabyly	nabýt	k5eAaPmAgFnP	nabýt
malebné	malebný	k2eAgFnPc1d1	malebná
čínské	čínský	k2eAgFnPc1d1	čínská
<g/>
,	,	kIx,	,
korejské	korejský	k2eAgFnPc1d1	Korejská
a	a	k8xC	a
japonské	japonský	k2eAgFnPc1d1	japonská
zahrady	zahrada	k1gFnPc1	zahrada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Středověk	středověk	k1gInSc1	středověk
===	===	k?	===
</s>
</p>
<p>
<s>
Barbarství	barbarství	k1gNnSc1	barbarství
zvítězilo	zvítězit	k5eAaPmAgNnS	zvítězit
nad	nad	k7c7	nad
člověkem	člověk	k1gMnSc7	člověk
a	a	k8xC	a
uměním	umění	k1gNnSc7	umění
<g/>
,	,	kIx,	,
popisuje	popisovat	k5eAaImIp3nS	popisovat
J.C.	J.C.	k1gMnSc1	J.C.
Loudon	Loudon	k1gMnSc1	Loudon
pád	pád	k1gInSc4	pád
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pádu	pád	k1gInSc6	pád
zkorumpované	zkorumpovaný	k2eAgFnSc2d1	zkorumpovaná
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
byly	být	k5eAaImAgInP	být
zničeny	zničit	k5eAaPmNgInP	zničit
a	a	k8xC	a
vypleněny	vypleněn	k2eAgInPc1d1	vypleněn
paláce	palác	k1gInPc1	palác
i	i	k8xC	i
domy	dům	k1gInPc1	dům
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
provincie	provincie	k1gFnSc1	provincie
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Zahrady	zahrada	k1gFnPc1	zahrada
zpustly	zpustnout	k5eAaPmAgFnP	zpustnout
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
období	období	k1gNnSc6	období
středověku	středověk	k1gInSc2	středověk
se	se	k3xPyFc4	se
zahradnictví	zahradnictví	k1gNnSc1	zahradnictví
rozvíjelo	rozvíjet	k5eAaImAgNnS	rozvíjet
pouze	pouze	k6eAd1	pouze
velmi	velmi	k6eAd1	velmi
omezeně	omezeně	k6eAd1	omezeně
v	v	k7c6	v
klášterních	klášterní	k2eAgFnPc6d1	klášterní
zahradách	zahrada	k1gFnPc6	zahrada
nebo	nebo	k8xC	nebo
na	na	k7c6	na
sídlech	sídlo	k1gNnPc6	sídlo
nejbohatších	bohatý	k2eAgMnPc2d3	nejbohatší
vládců	vládce	k1gMnPc2	vládce
a	a	k8xC	a
bohatých	bohatý	k2eAgMnPc2d1	bohatý
náboženských	náboženský	k2eAgMnPc2d1	náboženský
činovníků	činovník	k1gMnPc2	činovník
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
ale	ale	k8xC	ale
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
stále	stále	k6eAd1	stále
evropským	evropský	k2eAgNnSc7d1	Evropské
centrem	centrum	k1gNnSc7	centrum
vědění	vědění	k1gNnSc2	vědění
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Znalosti	znalost	k1gFnSc2	znalost
technologií	technologie	k1gFnPc2	technologie
které	který	k3yQgNnSc1	který
románská	románský	k2eAgFnSc1d1	románská
kultura	kultura	k1gFnSc1	kultura
používala	používat	k5eAaImAgFnS	používat
se	se	k3xPyFc4	se
šířily	šířit	k5eAaImAgInP	šířit
do	do	k7c2	do
zbytku	zbytek	k1gInSc2	zbytek
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
často	často	k6eAd1	často
také	také	k9	také
upadaly	upadat	k5eAaPmAgFnP	upadat
v	v	k7c6	v
zapomenutí	zapomenutí	k1gNnSc6	zapomenutí
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
středověku	středověk	k1gInSc2	středověk
do	do	k7c2	do
východní	východní	k2eAgFnSc2d1	východní
a	a	k8xC	a
severní	severní	k2eAgFnSc2d1	severní
Evropy	Evropa	k1gFnSc2	Evropa
pronikaly	pronikat	k5eAaImAgInP	pronikat
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
ovocných	ovocný	k2eAgInPc2d1	ovocný
a	a	k8xC	a
okrasných	okrasný	k2eAgFnPc2d1	okrasná
dřevin	dřevina	k1gFnPc2	dřevina
(	(	kIx(	(
<g/>
mišpule	mišpule	k1gFnSc1	mišpule
<g/>
,	,	kIx,	,
réva	réva	k1gFnSc1	réva
<g/>
)	)	kIx)	)
a	a	k8xC	a
znalosti	znalost	k1gFnPc4	znalost
o	o	k7c6	o
způsobech	způsob	k1gInPc6	způsob
jejich	jejich	k3xOp3gNnSc2	jejich
pěstování	pěstování	k1gNnSc2	pěstování
<g/>
.	.	kIx.	.
</s>
<s>
Výsadu	výsada	k1gFnSc4	výsada
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
,	,	kIx,	,
přístup	přístup	k1gInSc1	přístup
ke	k	k7c3	k
kultuře	kultura	k1gFnSc3	kultura
<g/>
,	,	kIx,	,
dostává	dostávat	k5eAaImIp3nS	dostávat
jen	jen	k9	jen
malé	malý	k2eAgNnSc1d1	malé
procento	procento	k1gNnSc1	procento
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Věda	věda	k1gFnSc1	věda
je	být	k5eAaImIp3nS	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
věroukou	věrouka	k1gFnSc7	věrouka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pierre	Pierr	k1gMnSc5	Pierr
de	de	k?	de
Crescent	Crescent	k1gInSc1	Crescent
popisuje	popisovat	k5eAaImIp3nS	popisovat
italské	italský	k2eAgFnSc2d1	italská
zahrady	zahrada	k1gFnSc2	zahrada
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
dělí	dělit	k5eAaImIp3nS	dělit
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
tříd	třída	k1gFnPc2	třída
<g/>
:	:	kIx,	:
zahrady	zahrada	k1gFnSc2	zahrada
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
pouze	pouze	k6eAd1	pouze
malým	malý	k2eAgNnSc7d1	malé
jměním	jmění	k1gNnSc7	jmění
<g/>
,	,	kIx,	,
osob	osoba	k1gFnPc2	osoba
z	z	k7c2	z
lepších	dobrý	k2eAgInPc2d2	lepší
poměrů	poměr	k1gInPc2	poměr
<g/>
,	,	kIx,	,
knížat	kníže	k1gMnPc2wR	kníže
a	a	k8xC	a
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Popisuje	popisovat	k5eAaImIp3nS	popisovat
tvorbu	tvorba	k1gFnSc4	tvorba
a	a	k8xC	a
zdobení	zdobení	k1gNnSc4	zdobení
těchto	tento	k3xDgFnPc2	tento
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Královské	královský	k2eAgFnPc1d1	královská
zahrady	zahrada	k1gFnPc1	zahrada
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
mít	mít	k5eAaImF	mít
zvěřinec	zvěřinec	k1gMnSc1	zvěřinec
a	a	k8xC	a
voliéru	voliéra	k1gFnSc4	voliéra
<g/>
,	,	kIx,	,
altánek	altánek	k1gInSc4	altánek
<g/>
,	,	kIx,	,
a	a	k8xC	a
vinnou	vinný	k2eAgFnSc4d1	vinná
révu	réva	k1gFnSc4	réva
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
vyšších	vysoký	k2eAgFnPc2d2	vyšší
tříd	třída	k1gFnPc2	třída
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
ozdobeny	ozdobit	k5eAaPmNgInP	ozdobit
trávníkem	trávník	k1gInSc7	trávník
<g/>
,	,	kIx,	,
keři	keř	k1gInPc7	keř
<g/>
,	,	kIx,	,
a	a	k8xC	a
aromatickými	aromatický	k2eAgFnPc7d1	aromatická
květinami	květina	k1gFnPc7	květina
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
území	území	k1gNnSc6	území
jižní	jižní	k2eAgFnSc2d1	jižní
Evropy	Evropa	k1gFnSc2	Evropa
vznikají	vznikat	k5eAaImIp3nP	vznikat
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
islámu	islám	k1gInSc2	islám
maurské	maurský	k2eAgFnSc2d1	maurská
zahrady	zahrada	k1gFnSc2	zahrada
(	(	kIx(	(
<g/>
Alhambra	Alhambra	k1gMnSc1	Alhambra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
jsou	být	k5eAaImIp3nP	být
vytvářeny	vytvářit	k5eAaPmNgInP	vytvářit
stylizované	stylizovaný	k2eAgInPc1d1	stylizovaný
japonské	japonský	k2eAgInPc1d1	japonský
zahrady	zahrada	k1gFnPc4	zahrada
a	a	k8xC	a
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
se	se	k3xPyFc4	se
styly	styl	k1gInPc1	styl
umění	umění	k1gNnSc4	umění
ikebany	ikebana	k1gFnSc2	ikebana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Renesance	renesance	k1gFnSc2	renesance
===	===	k?	===
</s>
</p>
<p>
<s>
Zahradnictví	zahradnictví	k1gNnSc1	zahradnictví
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ostatní	ostatní	k2eAgNnSc4d1	ostatní
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
oživeno	oživen	k2eAgNnSc1d1	oživeno
a	a	k8xC	a
sponzorováno	sponzorován	k2eAgNnSc1d1	sponzorováno
rodem	rod	k1gInSc7	rod
Medicejských	Medicejský	k2eAgInPc2d1	Medicejský
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
šestnáctého	šestnáctý	k4xOgNnSc2	šestnáctý
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šestnáctém	šestnáctý	k4xOgInSc6	šestnáctý
století	století	k1gNnSc6	století
zahrady	zahrada	k1gFnSc2	zahrada
podléhají	podléhat	k5eAaImIp3nP	podléhat
přísnějšímu	přísný	k2eAgNnSc3d2	přísnější
geometrickému	geometrický	k2eAgNnSc3d1	geometrické
formování	formování	k1gNnSc3	formování
<g/>
,	,	kIx,	,
přichází	přicházet	k5eAaImIp3nS	přicházet
objev	objev	k1gInSc1	objev
vlivu	vliv	k1gInSc2	vliv
architektury	architektura	k1gFnSc2	architektura
rostlin	rostlina	k1gFnPc2	rostlina
na	na	k7c4	na
kvality	kvalita	k1gFnPc4	kvalita
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
objevovány	objevován	k2eAgInPc4d1	objevován
vlivy	vliv	k1gInPc4	vliv
a	a	k8xC	a
pravidla	pravidlo	k1gNnPc4	pravidlo
stavebních	stavební	k2eAgInPc2d1	stavební
a	a	k8xC	a
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
prvky	prvek	k1gInPc7	prvek
jako	jako	k8xS	jako
casino	casino	k1gNnSc1	casino
<g/>
,	,	kIx,	,
grotta	grotta	k1gFnSc1	grotta
s	s	k7c7	s
kaskádou	kaskáda	k1gFnSc7	kaskáda
a	a	k8xC	a
fontány	fontán	k1gInPc1	fontán
v	v	k7c6	v
osové	osový	k2eAgFnSc6d1	Osová
souměrnsti	souměrnst	k1gFnSc6	souměrnst
<g/>
,	,	kIx,	,
theatron	theatron	k1gInSc1	theatron
<g/>
,	,	kIx,	,
rybníčky	rybníček	k1gInPc1	rybníček
<g/>
,	,	kIx,	,
štěpnice	štěpnice	k1gFnPc1	štěpnice
<g/>
,	,	kIx,	,
ptáčnice	ptáčnice	k1gFnPc1	ptáčnice
<g/>
,	,	kIx,	,
zvěřinec	zvěřinec	k1gMnSc1	zvěřinec
a	a	k8xC	a
stromovky	stromovka	k1gFnPc1	stromovka
<g/>
.	.	kIx.	.
</s>
<s>
Prostory	prostora	k1gFnPc1	prostora
propojují	propojovat	k5eAaImIp3nP	propojovat
loubí	loubí	k1gNnSc1	loubí
<g/>
,	,	kIx,	,
pergoly	pergola	k1gFnPc1	pergola
<g/>
,	,	kIx,	,
aleje	alej	k1gFnPc1	alej
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
příkladů	příklad	k1gInPc2	příklad
italské	italský	k2eAgFnSc2d1	italská
zahrady	zahrada	k1gFnSc2	zahrada
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jsou	být	k5eAaImIp3nP	být
zahrady	zahrada	k1gFnPc4	zahrada
Boboli	Bobole	k1gFnSc4	Bobole
nebo	nebo	k8xC	nebo
Isola	Isola	k1gMnSc1	Isola
Bella	Bella	k1gMnSc1	Bella
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Itallské	Itallský	k2eAgNnSc1d1	Itallský
zahradnictví	zahradnictví	k1gNnSc1	zahradnictví
je	být	k5eAaImIp3nS	být
oceňováno	oceňován	k2eAgNnSc1d1	oceňováno
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
pěstování	pěstování	k1gNnSc1	pěstování
zeleniny	zelenina	k1gFnSc2	zelenina
i	i	k8xC	i
květin	květina	k1gFnPc2	květina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obdivovány	obdivován	k2eAgInPc1d1	obdivován
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
botanické	botanický	k2eAgFnPc4d1	botanická
zahrady	zahrada	k1gFnPc4	zahrada
<g/>
,	,	kIx,	,
ovocné	ovocný	k2eAgFnPc4d1	ovocná
sady	sada	k1gFnPc4	sada
<g/>
,	,	kIx,	,
oranžérie	oranžérie	k1gFnPc4	oranžérie
<g/>
,	,	kIx,	,
a	a	k8xC	a
skleníky	skleník	k1gInPc1	skleník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Novověk	novověk	k1gInSc4	novověk
===	===	k?	===
</s>
</p>
<p>
<s>
K	k	k7c3	k
většímu	veliký	k2eAgInSc3d2	veliký
rozvoji	rozvoj	k1gInSc3	rozvoj
oboru	obor	k1gInSc2	obor
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
rozmachu	rozmach	k1gInSc2	rozmach
<g/>
,	,	kIx,	,
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
humanismu	humanismus	k1gInSc2	humanismus
<g/>
.	.	kIx.	.
</s>
<s>
Vyčlenil	vyčlenit	k5eAaPmAgInS	vyčlenit
se	se	k3xPyFc4	se
obor	obor	k1gInSc1	obor
zahradní	zahradní	k2eAgFnSc2d1	zahradní
architektury	architektura	k1gFnSc2	architektura
<g/>
,	,	kIx,	,
při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
módy	móda	k1gFnSc2	móda
u	u	k7c2	u
úpravy	úprava	k1gFnSc2	úprava
zahrad	zahrada	k1gFnPc2	zahrada
hrály	hrát	k5eAaImAgFnP	hrát
úlohu	úloha	k1gFnSc4	úloha
významné	významný	k2eAgFnSc2d1	významná
osobnosti	osobnost	k1gFnSc2	osobnost
i	i	k8xC	i
veřejné	veřejný	k2eAgNnSc4d1	veřejné
mínění	mínění	k1gNnSc4	mínění
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
spolků	spolek	k1gInPc2	spolek
a	a	k8xC	a
zahradnictví	zahradnictví	k1gNnSc2	zahradnictví
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
prostředkem	prostředek	k1gInSc7	prostředek
sociálního	sociální	k2eAgInSc2d1	sociální
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
,	,	kIx,	,
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
,	,	kIx,	,
i	i	k9	i
vzájemného	vzájemný	k2eAgNnSc2d1	vzájemné
srovnávání	srovnávání	k1gNnSc2	srovnávání
<g/>
.	.	kIx.	.
</s>
<s>
Vznikaly	vznikat	k5eAaImAgFnP	vznikat
první	první	k4xOgFnPc1	první
zahradnické	zahradnický	k2eAgFnPc1d1	zahradnická
školy	škola	k1gFnPc1	škola
<g/>
,	,	kIx,	,
odborné	odborný	k2eAgInPc1d1	odborný
zahradnické	zahradnický	k2eAgInPc1d1	zahradnický
časopisy	časopis	k1gInPc1	časopis
<g/>
,	,	kIx,	,
šlechtění	šlechtění	k1gNnSc1	šlechtění
rostlin	rostlina	k1gFnPc2	rostlina
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
systematickým	systematický	k2eAgMnSc7d1	systematický
i	i	k8xC	i
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
pokrok	pokrok	k1gInSc4	pokrok
v	v	k7c6	v
botanice	botanika	k1gFnSc6	botanika
a	a	k8xC	a
poznatky	poznatek	k1gInPc4	poznatek
v	v	k7c6	v
genetice	genetika	k1gFnSc6	genetika
<g/>
.	.	kIx.	.
</s>
<s>
Zdokonalila	zdokonalit	k5eAaPmAgFnS	zdokonalit
se	se	k3xPyFc4	se
ochrana	ochrana	k1gFnSc1	ochrana
a	a	k8xC	a
výživa	výživa	k1gFnSc1	výživa
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
obchodu	obchod	k1gInSc2	obchod
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
významnému	významný	k2eAgInSc3d1	významný
šířením	šíření	k1gNnPc3	šíření
chorob	choroba	k1gFnPc2	choroba
a	a	k8xC	a
škůdců	škůdce	k1gMnPc2	škůdce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
poválečných	poválečný	k2eAgNnPc6d1	poválečné
letech	léto	k1gNnPc6	léto
pak	pak	k6eAd1	pak
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
specializovaných	specializovaný	k2eAgFnPc2d1	specializovaná
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
v	v	k7c6	v
ČSSR	ČSSR	kA	ČSSR
dochází	docházet	k5eAaImIp3nS	docházet
po	po	k7c6	po
znárodňování	znárodňování	k1gNnSc6	znárodňování
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
národních	národní	k2eAgInPc2d1	národní
podniků	podnik	k1gInPc2	podnik
<g/>
,	,	kIx,	,
městských	městský	k2eAgInPc2d1	městský
podniků	podnik	k1gInPc2	podnik
zabývajících	zabývající	k2eAgInPc2d1	zabývající
se	se	k3xPyFc4	se
pokrytím	pokrytí	k1gNnSc7	pokrytí
místní	místní	k2eAgFnSc2d1	místní
poptávky	poptávka	k1gFnSc2	poptávka
a	a	k8xC	a
službami	služba	k1gFnPc7	služba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
v	v	k7c6	v
ČR	ČR	kA	ČR
roste	růst	k5eAaImIp3nS	růst
význam	význam	k1gInSc4	význam
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
obchodu	obchod	k1gInSc6	obchod
se	s	k7c7	s
zahradnickými	zahradnický	k2eAgInPc7d1	zahradnický
produkty	produkt	k1gInPc7	produkt
má	mít	k5eAaImIp3nS	mít
významné	významný	k2eAgNnSc1d1	významné
postavení	postavení	k1gNnSc1	postavení
Holandsko	Holandsko	k1gNnSc1	Holandsko
<g/>
.	.	kIx.	.
</s>
<s>
Vznikají	vznikat	k5eAaImIp3nP	vznikat
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
organizace	organizace	k1gFnSc2	organizace
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
šířením	šíření	k1gNnSc7	šíření
chorob	choroba	k1gFnPc2	choroba
a	a	k8xC	a
škůdců	škůdce	k1gMnPc2	škůdce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rostliny	rostlina	k1gFnPc1	rostlina
v	v	k7c6	v
zahradnictví	zahradnictví	k1gNnSc6	zahradnictví
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Použití	použití	k1gNnSc3	použití
===	===	k?	===
</s>
</p>
<p>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgFnP	používat
v	v	k7c4	v
zahradnictví	zahradnictví	k1gNnSc4	zahradnictví
jako	jako	k8xS	jako
základní	základní	k2eAgInSc4d1	základní
materiál	materiál	k1gInSc4	materiál
k	k	k7c3	k
více	hodně	k6eAd2	hodně
účelům	účel	k1gInPc3	účel
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
oborů	obor	k1gInPc2	obor
jako	jako	k8xS	jako
potravina	potravina	k1gFnSc1	potravina
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
dekorace	dekorace	k1gFnPc4	dekorace
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
rostliny	rostlina	k1gFnPc1	rostlina
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
léčivé	léčivý	k2eAgFnPc1d1	léčivá
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
pro	pro	k7c4	pro
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
výrobu	výroba	k1gFnSc4	výroba
léčiv	léčivo	k1gNnPc2	léčivo
ve	v	k7c6	v
specializovaných	specializovaný	k2eAgInPc6d1	specializovaný
oborech	obor	k1gInPc6	obor
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pojmenování	pojmenování	k1gNnPc1	pojmenování
rostlin	rostlina	k1gFnPc2	rostlina
používaná	používaný	k2eAgFnSc1d1	používaná
v	v	k7c6	v
zahradnictví	zahradnictví	k1gNnSc6	zahradnictví
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Komerční	komerční	k2eAgNnSc1d1	komerční
označení	označení	k1gNnSc1	označení
====	====	k?	====
</s>
</p>
<p>
<s>
Prodejci	prodejce	k1gMnPc1	prodejce
zahradnických	zahradnický	k2eAgInPc2d1	zahradnický
výpěstků	výpěstek	k1gInPc2	výpěstek
a	a	k8xC	a
výrobků	výrobek	k1gInPc2	výrobek
často	často	k6eAd1	často
používají	používat	k5eAaImIp3nP	používat
lidová	lidový	k2eAgNnPc1d1	lidové
jména	jméno	k1gNnPc1	jméno
nebo	nebo	k8xC	nebo
komerční	komerční	k2eAgInPc1d1	komerční
názvy	název	k1gInPc1	název
rostlin	rostlina	k1gFnPc2	rostlina
pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
atraktivity	atraktivita	k1gFnSc2	atraktivita
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
přístup	přístup	k1gInSc1	přístup
často	často	k6eAd1	často
mate	mást	k5eAaImIp3nS	mást
zákazníka	zákazník	k1gMnSc4	zákazník
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
rostlina	rostlina	k1gFnSc1	rostlina
pod	pod	k7c7	pod
pouze	pouze	k6eAd1	pouze
novým	nový	k2eAgInSc7d1	nový
názvem	název	k1gInSc7	název
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
prodávána	prodávat	k5eAaImNgFnS	prodávat
jako	jako	k8xS	jako
odlišné	odlišný	k2eAgNnSc1d1	odlišné
zboží	zboží	k1gNnSc1	zboží
nebo	nebo	k8xC	nebo
novinka	novinka	k1gFnSc1	novinka
a	a	k8xC	a
často	často	k6eAd1	často
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
nedorozuměním	nedorozumění	k1gNnPc3	nedorozumění
(	(	kIx(	(
<g/>
Nordmanova	Nordmanův	k2eAgFnSc1d1	Nordmanův
jedle	jedle	k1gFnSc1	jedle
<g/>
,	,	kIx,	,
jedle	jedle	k6eAd1	jedle
normanská	normanský	k2eAgFnSc1d1	normanská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnohý	mnohý	k2eAgMnSc1d1	mnohý
zákazník	zákazník	k1gMnSc1	zákazník
však	však	k9	však
dá	dát	k5eAaPmIp3nS	dát
přesto	přesto	k8xC	přesto
přednost	přednost	k1gFnSc4	přednost
použití	použití	k1gNnSc2	použití
jednoslovného	jednoslovný	k2eAgInSc2d1	jednoslovný
názvu	název	k1gInSc2	název
snadno	snadno	k6eAd1	snadno
zapamatovatelného	zapamatovatelný	k2eAgInSc2d1	zapamatovatelný
názvu	název	k1gInSc2	název
v	v	k7c6	v
jím	on	k3xPp3gMnSc7	on
používaném	používaný	k2eAgNnSc6d1	používané
jazyce	jazyk	k1gInSc6	jazyk
před	před	k7c7	před
binomickým	binomický	k2eAgNnSc7d1	binomické
nebo	nebo	k8xC	nebo
trinomickým	trinomický	k2eAgNnSc7d1	trinomický
vědeckým	vědecký	k2eAgNnSc7d1	vědecké
pojmenováním	pojmenování	k1gNnSc7	pojmenování
cizím	cizit	k5eAaImIp1nS	cizit
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
beztoho	beztoho	k9	beztoho
také	také	k9	také
mění	měnit	k5eAaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
pojmenování	pojmenování	k1gNnSc1	pojmenování
rostlin	rostlina	k1gFnPc2	rostlina
není	být	k5eNaImIp3nS	být
samoúčelné	samoúčelný	k2eAgNnSc1d1	samoúčelné
<g/>
.	.	kIx.	.
</s>
<s>
Používaná	používaný	k2eAgNnPc1d1	používané
jména	jméno	k1gNnPc1	jméno
mají	mít	k5eAaImIp3nP	mít
sloužit	sloužit	k5eAaImF	sloužit
především	především	k9	především
k	k	k7c3	k
účelu	účel	k1gInSc3	účel
identifikace	identifikace	k1gFnSc2	identifikace
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Lidové	lidový	k2eAgInPc1d1	lidový
názvy	název	k1gInPc1	název
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
zahradničení	zahradničený	k2eAgMnPc1d1	zahradničený
obyvatel	obyvatel	k1gMnSc1	obyvatel
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
nejsou	být	k5eNaImIp3nP	být
profesionálními	profesionální	k2eAgMnPc7d1	profesionální
zahradníky	zahradník	k1gMnPc7	zahradník
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
škály	škála	k1gFnSc2	škála
starších	starý	k2eAgInPc2d2	starší
lidových	lidový	k2eAgInPc2d1	lidový
názvů	název	k1gInPc2	název
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
lidová	lidový	k2eAgNnPc1d1	lidové
pojmenování	pojmenování	k1gNnPc1	pojmenování
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
mnohem	mnohem	k6eAd1	mnohem
starší	starý	k2eAgNnPc4d2	starší
než	než	k8xS	než
používaná	používaný	k2eAgNnPc4d1	používané
taxonomická	taxonomický	k2eAgNnPc4d1	taxonomické
pojmenování	pojmenování	k1gNnPc4	pojmenování
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
dříve	dříve	k6eAd2	dříve
používaných	používaný	k2eAgInPc2d1	používaný
vědeckých	vědecký	k2eAgInPc2d1	vědecký
názvů	název	k1gInPc2	název
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
lidových	lidový	k2eAgNnPc2d1	lidové
jmen	jméno	k1gNnPc2	jméno
byla	být	k5eAaImAgFnS	být
obvykle	obvykle	k6eAd1	obvykle
používána	používán	k2eAgFnSc1d1	používána
fantazie	fantazie	k1gFnSc1	fantazie
vykreslující	vykreslující	k2eAgFnSc4d1	vykreslující
podobu	podoba	k1gFnSc4	podoba
rostliny	rostlina	k1gFnSc2	rostlina
(	(	kIx(	(
<g/>
růže	růže	k1gFnSc1	růže
z	z	k7c2	z
Jericha	Jericho	k1gNnSc2	Jericho
–	–	k?	–
Lonicera	Lonicera	k1gFnSc1	Lonicera
periclymenum	periclymenum	k1gInSc1	periclymenum
ale	ale	k8xC	ale
také	také	k9	také
Selaginella	Selaginella	k1gMnSc1	Selaginella
lepidophylla	lepidophylla	k1gMnSc1	lepidophylla
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
název	název	k1gInSc1	název
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
k	k	k7c3	k
vlastnostem	vlastnost	k1gFnPc3	vlastnost
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Nověji	nově	k6eAd2	nově
také	také	k9	také
bývalo	bývat	k5eAaImAgNnS	bývat
používáno	používán	k2eAgNnSc4d1	používáno
ideologické	ideologický	k2eAgNnSc4d1	ideologické
pojmenování	pojmenování	k1gNnSc4	pojmenování
–	–	k?	–
Slzičky	slzička	k1gFnSc2	slzička
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
(	(	kIx(	(
<g/>
různé	různý	k2eAgInPc1d1	různý
menší	malý	k2eAgInPc1d2	menší
druhy	druh	k1gInPc1	druh
hvozdíků	hvozdík	k1gInPc2	hvozdík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rukavice	rukavice	k1gFnSc1	rukavice
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
ruce	ruka	k1gFnPc4	ruka
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
zvonečky	zvoneček	k1gInPc4	zvoneček
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
mnohdy	mnohdy	k6eAd1	mnohdy
snazší	snadný	k2eAgNnSc1d2	snazší
vyslovení	vyslovení	k1gNnSc1	vyslovení
názvu	název	k1gInSc2	název
<g/>
,	,	kIx,	,
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
že	že	k8xS	že
i	i	k9	i
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
regionu	region	k1gInSc2	region
se	se	k3xPyFc4	se
názvy	název	k1gInPc7	název
mohou	moct	k5eAaImIp3nP	moct
lišit	lišit	k5eAaImF	lišit
a	a	k8xC	a
národní	národní	k2eAgInPc4d1	národní
názvy	název	k1gInPc4	název
jsou	být	k5eAaImIp3nP	být
zcela	zcela	k6eAd1	zcela
různé	různý	k2eAgFnPc1d1	různá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Vědecké	vědecký	k2eAgInPc1d1	vědecký
názvy	název	k1gInPc1	název
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
profesionálním	profesionální	k2eAgNnSc6d1	profesionální
zahradnictví	zahradnictví	k1gNnSc6	zahradnictví
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgInP	používat
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
jsou	být	k5eAaImIp3nP	být
předmětem	předmět	k1gInSc7	předmět
pěstování	pěstování	k1gNnSc2	pěstování
<g/>
,	,	kIx,	,
vědecké	vědecký	k2eAgInPc4d1	vědecký
názvy	název	k1gInPc4	název
s	s	k7c7	s
připojením	připojení	k1gNnSc7	připojení
názvu	název	k1gInSc2	název
kultivaru	kultivar	k1gInSc2	kultivar
<g/>
.	.	kIx.	.
</s>
<s>
Řídí	řídit	k5eAaImIp3nS	řídit
se	se	k3xPyFc4	se
botanickou	botanický	k2eAgFnSc7d1	botanická
nomenklaturou	nomenklatura	k1gFnSc7	nomenklatura
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
pravidel	pravidlo	k1gNnPc2	pravidlo
tvorby	tvorba	k1gFnSc2	tvorba
jmen	jméno	k1gNnPc2	jméno
cultonů	culton	k1gInPc2	culton
a	a	k8xC	a
zohledňuje	zohledňovat	k5eAaImIp3nS	zohledňovat
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
taxonomii	taxonomie	k1gFnSc6	taxonomie
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
pojmenování	pojmenování	k1gNnPc1	pojmenování
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
mezinárodně	mezinárodně	k6eAd1	mezinárodně
respektovaná	respektovaný	k2eAgFnSc1d1	respektovaná
a	a	k8xC	a
tedy	tedy	k9	tedy
shodná	shodný	k2eAgNnPc4d1	shodné
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
velkou	velký	k2eAgFnSc7d1	velká
výhodou	výhoda	k1gFnSc7	výhoda
v	v	k7c6	v
obchodním	obchodní	k2eAgInSc6d1	obchodní
styku	styk	k1gInSc6	styk
<g/>
,	,	kIx,	,
komunikaci	komunikace	k1gFnSc4	komunikace
a	a	k8xC	a
čerpání	čerpání	k1gNnSc4	čerpání
z	z	k7c2	z
odborných	odborný	k2eAgInPc2d1	odborný
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
komunikaci	komunikace	k1gFnSc6	komunikace
u	u	k7c2	u
často	často	k6eAd1	často
užívaných	užívaný	k2eAgInPc2d1	užívaný
druhů	druh	k1gInPc2	druh
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
u	u	k7c2	u
druhu	druh	k1gInSc2	druh
o	o	k7c6	o
kterém	který	k3yRgInSc6	který
již	již	k9	již
byla	být	k5eAaImAgFnS	být
konkrétně	konkrétně	k6eAd1	konkrétně
řeč	řeč	k1gFnSc1	řeč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
někdy	někdy	k6eAd1	někdy
pojmenování	pojmenování	k1gNnSc4	pojmenování
zkráceno	zkrácen	k2eAgNnSc4d1	zkráceno
na	na	k7c4	na
název	název	k1gInSc4	název
kultivaru	kultivar	k1gInSc2	kultivar
(	(	kIx(	(
<g/>
'	'	kIx"	'
<g/>
Dubáček	Dubáček	k1gInSc1	Dubáček
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
Skyrocket	Skyrocket	k1gInSc1	Skyrocket
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
Spartan	Spartan	k?	Spartan
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
Gloria	Gloria	k1gFnSc1	Gloria
Dei	Dei	k1gFnSc1	Dei
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
někdy	někdy	k6eAd1	někdy
zkomoleno	zkomolen	k2eAgNnSc1d1	zkomoleno
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
druhy	druh	k1gInPc4	druh
plevele	plevel	k1gFnSc2	plevel
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
pěstovány	pěstován	k2eAgFnPc4d1	pěstována
jako	jako	k8xC	jako
kulturní	kulturní	k2eAgFnPc4d1	kulturní
rostliny	rostlina	k1gFnPc4	rostlina
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
komunikaci	komunikace	k1gFnSc6	komunikace
používáno	používán	k2eAgNnSc1d1	používáno
vědeckých	vědecký	k2eAgNnPc2d1	vědecké
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lidových	lidový	k2eAgNnPc2d1	lidové
jmen	jméno	k1gNnPc2	jméno
běžných	běžný	k2eAgNnPc2d1	běžné
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
lokalitě	lokalita	k1gFnSc6	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
desikaci	desikace	k1gFnSc6	desikace
plevele	plevel	k1gFnSc2	plevel
tak	tak	k9	tak
zahradník	zahradník	k1gMnSc1	zahradník
neničí	ničit	k5eNaImIp3nS	ničit
pesticidem	pesticid	k1gInSc7	pesticid
Taraxacum	Taraxacum	k1gNnSc1	Taraxacum
vulgare	vulgar	k1gMnSc5	vulgar
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pampelišky	pampeliška	k1gFnPc1	pampeliška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odborné	odborný	k2eAgNnSc4d1	odborné
<g/>
,	,	kIx,	,
vědecké	vědecký	k2eAgNnSc4d1	vědecké
<g/>
,	,	kIx,	,
latinské	latinský	k2eAgNnSc4d1	latinské
pojmenování	pojmenování	k1gNnSc4	pojmenování
používané	používaný	k2eAgNnSc4d1	používané
v	v	k7c6	v
komunikaci	komunikace	k1gFnSc6	komunikace
se	se	k3xPyFc4	se
však	však	k9	však
během	během	k7c2	během
let	léto	k1gNnPc2	léto
v	v	k7c6	v
taxonomii	taxonomie	k1gFnSc6	taxonomie
také	také	k9	také
mění	měnit	k5eAaImIp3nS	měnit
(	(	kIx(	(
<g/>
Brugmansia	Brugmansius	k1gMnSc4	Brugmansius
na	na	k7c4	na
Datura	Datur	k1gMnSc4	Datur
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
Brugmansia	Brugmansius	k1gMnSc4	Brugmansius
<g/>
)	)	kIx)	)
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
nedorozumění	nedorozumění	k1gNnSc3	nedorozumění
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
tyto	tento	k3xDgFnPc4	tento
změny	změna	k1gFnPc1	změna
činí	činit	k5eAaImIp3nP	činit
i	i	k9	i
při	při	k7c6	při
práci	práce	k1gFnSc6	práce
s	s	k7c7	s
odbornou	odborný	k2eAgFnSc7d1	odborná
literaturou	literatura	k1gFnSc7	literatura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
EPPO	EPPO	kA	EPPO
kód	kód	k1gInSc4	kód
====	====	k?	====
</s>
</p>
<p>
<s>
Z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
častých	častý	k2eAgFnPc2d1	častá
změn	změna	k1gFnPc2	změna
názvů	název	k1gInPc2	název
je	být	k5eAaImIp3nS	být
nově	nově	k6eAd1	nově
v	v	k7c6	v
rostlinolékařství	rostlinolékařství	k1gNnSc6	rostlinolékařství
šířen	šířen	k2eAgInSc1d1	šířen
neměnný	měnný	k2eNgInSc1d1	neměnný
kód	kód	k1gInSc1	kód
pro	pro	k7c4	pro
používané	používaný	k2eAgFnPc4d1	používaná
a	a	k8xC	a
známé	známý	k2eAgFnPc4d1	známá
rostliny	rostlina	k1gFnPc4	rostlina
a	a	k8xC	a
živočichy	živočich	k1gMnPc4	živočich
který	který	k3yIgMnSc1	který
nebude	být	k5eNaImBp3nS	být
podléhat	podléhat	k5eAaImF	podléhat
změnám	změna	k1gFnPc3	změna
v	v	k7c6	v
taxonomii	taxonomie	k1gFnSc6	taxonomie
<g/>
,	,	kIx,	,
eppo	eppo	k6eAd1	eppo
kód	kód	k1gInSc4	kód
<g/>
,	,	kIx,	,
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
tak	tak	k6eAd1	tak
usnadní	usnadnit	k5eAaPmIp3nS	usnadnit
určení	určení	k1gNnSc4	určení
rostliny	rostlina	k1gFnSc2	rostlina
v	v	k7c6	v
komunikaci	komunikace	k1gFnSc6	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
někdy	někdy	k6eAd1	někdy
těžko	těžko	k6eAd1	těžko
vyslovitelný	vyslovitelný	k2eAgInSc1d1	vyslovitelný
a	a	k8xC	a
těžko	těžko	k6eAd1	těžko
bude	být	k5eAaImBp3nS	být
běžně	běžně	k6eAd1	běžně
používán	používat	k5eAaImNgInS	používat
jako	jako	k8xS	jako
obchodní	obchodní	k2eAgInSc1d1	obchodní
název	název	k1gInSc1	název
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odvětví	odvětví	k1gNnSc3	odvětví
zahradnictví	zahradnictví	k1gNnSc3	zahradnictví
==	==	k?	==
</s>
</p>
<p>
<s>
Základní	základní	k2eAgNnSc1d1	základní
rozdělení	rozdělení	k1gNnSc1	rozdělení
zahradnictví	zahradnictví	k1gNnSc2	zahradnictví
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
několik	několik	k4yIc4	několik
základních	základní	k2eAgNnPc2d1	základní
odvětví	odvětví	k1gNnPc2	odvětví
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ty	ten	k3xDgInPc4	ten
základní	základní	k2eAgInPc4d1	základní
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
zelinářství	zelinářství	k1gNnSc1	zelinářství
</s>
</p>
<p>
<s>
ovocnářství	ovocnářství	k1gNnSc1	ovocnářství
</s>
</p>
<p>
<s>
sadovnictví	sadovnictví	k1gNnSc1	sadovnictví
</s>
</p>
<p>
<s>
květinářství	květinářství	k1gNnSc4	květinářství
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgFnPc4d1	další
funkce	funkce	k1gFnPc4	funkce
==	==	k?	==
</s>
</p>
<p>
<s>
Zahradnictví	zahradnictví	k1gNnSc1	zahradnictví
lze	lze	k6eAd1	lze
také	také	k9	také
dělit	dělit	k5eAaImF	dělit
na	na	k7c4	na
další	další	k2eAgInPc4d1	další
obory	obor	k1gInPc4	obor
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
arboristika	arboristika	k1gFnSc1	arboristika
<g/>
,	,	kIx,	,
výživa	výživa	k1gFnSc1	výživa
a	a	k8xC	a
ochrana	ochrana	k1gFnSc1	ochrana
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
botanika	botanika	k1gFnSc1	botanika
</s>
</p>
<p>
<s>
dendrologie	dendrologie	k1gFnSc1	dendrologie
</s>
</p>
<p>
<s>
park	park	k1gInSc1	park
</s>
</p>
<p>
<s>
zahrada	zahrada	k1gFnSc1	zahrada
a	a	k8xC	a
botanická	botanický	k2eAgFnSc1d1	botanická
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
šlechtitelství	šlechtitelství	k1gNnSc1	šlechtitelství
</s>
</p>
<p>
<s>
semenářství	semenářství	k1gNnSc1	semenářství
</s>
</p>
<p>
<s>
údržba	údržba	k1gFnSc1	údržba
zeleně	zeleň	k1gFnSc2	zeleň
</s>
</p>
<p>
<s>
vinařství	vinařství	k1gNnSc1	vinařství
</s>
</p>
<p>
<s>
vinohradnictví	vinohradnictví	k1gNnSc1	vinohradnictví
</s>
</p>
<p>
<s>
zahradní	zahradní	k2eAgFnSc1d1	zahradní
architektura	architektura	k1gFnSc1	architektura
</s>
</p>
<p>
<s>
zahradní	zahradní	k2eAgNnSc1d1	zahradní
centrum	centrum	k1gNnSc1	centrum
</s>
</p>
<p>
<s>
ovocná	ovocný	k2eAgFnSc1d1	ovocná
školka	školka	k1gFnSc1	školka
</s>
</p>
<p>
<s>
okrasná	okrasný	k2eAgFnSc1d1	okrasná
školka	školka	k1gFnSc1	školka
</s>
</p>
<p>
<s>
zahradník	zahradník	k1gMnSc1	zahradník
</s>
</p>
<p>
<s>
zahrádkářství	zahrádkářství	k1gNnSc1	zahrádkářství
</s>
</p>
<p>
<s>
zahradní	zahradní	k2eAgNnSc1d1	zahradní
centrum	centrum	k1gNnSc1	centrum
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
zahradnictví	zahradnictví	k1gNnSc2	zahradnictví
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
