<s>
Zahradnictví	zahradnictví	k1gNnSc1	zahradnictví
je	být	k5eAaImIp3nS	být
obor	obor	k1gInSc1	obor
zabývající	zabývající	k2eAgInSc1d1	zabývající
se	s	k7c7	s
studií	studie	k1gFnSc7	studie
rostlin	rostlina	k1gFnPc2	rostlina
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
pěstování	pěstování	k1gNnSc3	pěstování
rostlin	rostlina	k1gFnPc2	rostlina
pro	pro	k7c4	pro
uspokojení	uspokojení	k1gNnSc4	uspokojení
materiálních	materiální	k2eAgFnPc2d1	materiální
potřeb	potřeba	k1gFnPc2	potřeba
(	(	kIx(	(
<g/>
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
,	,	kIx,	,
zeleniny	zelenina	k1gFnSc2	zelenina
<g/>
,	,	kIx,	,
koření	koření	k1gNnSc2	koření
<g/>
,	,	kIx,	,
bylin	bylina	k1gFnPc2	bylina
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ze	z	k7c2	z
sociálních	sociální	k2eAgInPc2d1	sociální
a	a	k8xC	a
estetických	estetický	k2eAgInPc2d1	estetický
důvodů	důvod	k1gInPc2	důvod
(	(	kIx(	(
<g/>
okrasné	okrasný	k2eAgFnPc1d1	okrasná
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
