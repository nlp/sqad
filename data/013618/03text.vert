<s>
Orionův	Orionův	k2eAgInSc1d1
pás	pás	k1gInSc1
</s>
<s>
Hvězdy	hvězda	k1gFnPc1
Orionova	Orionův	k2eAgInSc2d1
pásu	pás	k1gInSc2
</s>
<s>
Orionův	Orionův	k2eAgInSc1d1
pás	pás	k1gInSc1
nebo	nebo	k8xC
také	také	k9
pás	pás	k1gInSc1
Orionu	orion	k1gInSc2
je	být	k5eAaImIp3nS
asterismus	asterismus	k1gInSc1
tří	tři	k4xCgFnPc2
hvězd	hvězda	k1gFnPc2
v	v	k7c6
souhvězdí	souhvězdí	k1gNnSc6
Orionu	orion	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pás	pás	k1gInSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
těsně	těsně	k6eAd1
pod	pod	k7c7
nebeským	nebeský	k2eAgInSc7d1
rovníkem	rovník	k1gInSc7
a	a	k8xC
rozděluje	rozdělovat	k5eAaImIp3nS
Oriona	Oriona	k1gFnSc1
na	na	k7c4
severní	severní	k2eAgFnSc4d1
a	a	k8xC
jižní	jižní	k2eAgFnSc4d1
část	část	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Nejzápadnější	západní	k2eAgMnSc1d3
z	z	k7c2
trojice	trojice	k1gFnSc2
hvězd	hvězda	k1gFnPc2
nese	nést	k5eAaImIp3nS
název	název	k1gInSc1
Alnitak	Alnitak	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Bayerově	Bayerův	k2eAgNnSc6d1
označení	označení	k1gNnSc6
nese	nést	k5eAaImIp3nS
název	název	k1gInSc1
zéta	zét	k2eAgFnSc1d1
Orionis	Orionis	k1gFnSc1
(	(	kIx(
<g/>
ζ	ζ	k?
Ori	Ori	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
Alnitak	Alnitak	k1gInSc4
je	být	k5eAaImIp3nS
z	z	k7c2
arabského	arabský	k2eAgInSc2d1
al-nitáq	al-nitáq	k?
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
znamená	znamenat	k5eAaImIp3nS
opasek	opasek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alnitak	Alnitak	k1gInSc1
je	být	k5eAaImIp3nS
trojhvězda	trojhvězda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
obloze	obloha	k1gFnSc6
svítí	svítit	k5eAaImIp3nP
jasností	jasnost	k1gFnSc7
1,71	1,71	k4
<g/>
m.	m.	k?
Hlavní	hlavní	k2eAgFnSc1d1
hvězda	hvězda	k1gFnSc1
je	být	k5eAaImIp3nS
modrý	modrý	k2eAgMnSc1d1
veleobr	veleobr	k1gMnSc1
spektrálního	spektrální	k2eAgInSc2d1
typu	typ	k1gInSc2
O9	O9	k1gMnSc4
vzdálený	vzdálený	k2eAgMnSc1d1
od	od	k7c2
Slunce	slunce	k1gNnSc2
asi	asi	k9
800	#num#	k4
světelných	světelný	k2eAgNnPc2d1
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prostřední	prostřední	k2eAgFnSc1d1
hvězda	hvězda	k1gFnSc1
pásu	pás	k1gInSc2
Alnilam	Alnilam	k1gInSc1
je	být	k5eAaImIp3nS
také	také	k9
modrý	modrý	k2eAgMnSc1d1
veleobr	veleobr	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
má	mít	k5eAaImIp3nS
jasnost	jasnost	k1gFnSc4
1,69	1,69	k4
<g/>
m.	m.	k?
Jeho	jeho	k3xOp3gFnSc4
vzdálenost	vzdálenost	k1gFnSc4
od	od	k7c2
Slunce	slunce	k1gNnSc2
je	být	k5eAaImIp3nS
asi	asi	k9
1300	#num#	k4
světelných	světelný	k2eAgNnPc2d1
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
arabštině	arabština	k1gFnSc6
znamená	znamenat	k5eAaImIp3nS
slovo	slovo	k1gNnSc1
al-nizám	al-nizat	k5eAaPmIp1nS,k5eAaImIp1nS
šňůra	šňůra	k1gFnSc1
perel	perla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bayerovo	Bayerův	k2eAgNnSc1d1
označení	označení	k1gNnSc1
je	být	k5eAaImIp3nS
epsilon	epsilon	k1gNnSc4
Orionis	Orionis	k1gFnSc2
(	(	kIx(
<g/>
ε	ε	k?
Ori	Ori	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
Flamsteedovo	Flamsteedův	k2eAgNnSc1d1
označení	označení	k1gNnSc1
je	být	k5eAaImIp3nS
46	#num#	k4
Orionis	Orionis	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejseverněji	severně	k6eAd3
a	a	k8xC
nejvýchodněji	východně	k6eAd3
postavená	postavený	k2eAgFnSc1d1
hvězda	hvězda	k1gFnSc1
pásu	pás	k1gInSc2
se	se	k3xPyFc4
jmenuje	jmenovat	k5eAaImIp3nS,k5eAaBmIp3nS
Mintaka	Mintak	k1gMnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
také	také	k9
delta	delta	k1gFnSc1
Orionis	Orionis	k1gFnSc1
(	(	kIx(
<g/>
δ	δ	k?
Ori	Ori	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
modrobílá	modrobílý	k2eAgFnSc1d1
hvězda	hvězda	k1gFnSc1
(	(	kIx(
<g/>
spektrální	spektrální	k2eAgInSc1d1
typ	typ	k1gInSc1
B	B	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
všech	všecek	k3xTgFnPc2
tří	tři	k4xCgFnPc2
hvězd	hvězda	k1gFnPc2
má	mít	k5eAaImIp3nS
nejmenší	malý	k2eAgFnSc4d3
jasnost	jasnost	k1gFnSc4
(	(	kIx(
<g/>
2,21	2,21	k4
<g/>
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
Slunce	slunce	k1gNnSc2
je	být	k5eAaImIp3nS
vzdálená	vzdálený	k2eAgFnSc1d1
900	#num#	k4
světelných	světelný	k2eAgNnPc2d1
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Orientace	orientace	k1gFnSc1
na	na	k7c6
noční	noční	k2eAgFnSc6d1
obloze	obloha	k1gFnSc6
pomocí	pomocí	k7c2
Oriona	Orion	k1gMnSc2
</s>
<s>
Orionův	Orionův	k2eAgInSc1d1
pás	pás	k1gInSc1
bývá	bývat	k5eAaImIp3nS
na	na	k7c6
noční	noční	k2eAgFnSc6d1
obloze	obloha	k1gFnSc6
dobře	dobře	k6eAd1
viditelný	viditelný	k2eAgInSc1d1
a	a	k8xC
dá	dát	k5eAaPmIp3nS
se	se	k3xPyFc4
pomocí	pomocí	k7c2
něj	on	k3xPp3gMnSc2
snadno	snadno	k6eAd1
orientovat	orientovat	k5eAaBmF
na	na	k7c6
obloze	obloha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
severozápad	severozápad	k1gInSc4
od	od	k7c2
pásu	pás	k1gInSc2
je	být	k5eAaImIp3nS
hvězda	hvězda	k1gFnSc1
Aldebaran	Aldebarana	k1gFnPc2
ze	z	k7c2
souhvězdí	souhvězdí	k1gNnPc2
Býka	býk	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
jihozápad	jihozápad	k1gInSc4
je	být	k5eAaImIp3nS
nejjasnější	jasný	k2eAgFnSc1d3
hvězda	hvězda	k1gFnSc1
oblohy	obloha	k1gFnSc2
(	(	kIx(
<g/>
nepočítáme	počítat	k5eNaImIp1nP
<g/>
-li	-li	k?
Slunce	slunce	k1gNnSc1
<g/>
)	)	kIx)
Sirius	Sirius	k1gInSc1
z	z	k7c2
Velkého	velký	k2eAgMnSc2d1
psa	pes	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
anglickojazyčné	anglickojazyčný	k2eAgInPc4d1
tradiční	tradiční	k2eAgInPc4d1
názvy	název	k1gInPc4
Orionova	Orionův	k2eAgInSc2d1
pásu	pás	k1gInSc2
patří	patřit	k5eAaImIp3nS
např.	např.	kA
"	"	kIx"
<g/>
Three	Three	k1gFnSc1
Kings	Kingsa	k1gFnPc2
<g/>
"	"	kIx"
(	(	kIx(
<g/>
tři	tři	k4xCgFnPc1
králové	králová	k1gFnPc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
"	"	kIx"
<g/>
Three	Three	k1gNnSc1
Sisters	Sistersa	k1gFnPc2
<g/>
"	"	kIx"
(	(	kIx(
<g/>
tři	tři	k4xCgFnPc1
sestry	sestra	k1gFnPc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
v	v	k7c6
latinskoamerickém	latinskoamerický	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
bývá	bývat	k5eAaImIp3nS
nazýván	nazývat	k5eAaImNgInS
"	"	kIx"
<g/>
Tres	tresa	k1gFnPc2
Marías	Marías	k1gInSc1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
Tři	tři	k4xCgFnPc1
Marie	Maria	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Orionův	Orionův	k2eAgInSc4d1
pás	pás	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Orion	orion	k1gInSc1
Důležité	důležitý	k2eAgFnSc2d1
hvězdy	hvězda	k1gFnSc2
</s>
<s>
Betelgeuze	Betelgeuze	k1gFnSc1
•	•	k?
Rigel	Rigel	k1gInSc1
•	•	k?
Bellatrix	Bellatrix	k1gInSc1
•	•	k?
Alnilam	Alnilam	k1gInSc1
•	•	k?
Alnitak	Alnitak	k1gInSc1
•	•	k?
Mintaka	Mintak	k1gMnSc2
•	•	k?
Saiph	Saiph	k1gInSc1
•	•	k?
Hatysa	Hatys	k1gMnSc2
•	•	k?
Meissa	Meiss	k1gMnSc2
(	(	kIx(
<g/>
Heka	Hekus	k1gMnSc2
<g/>
)	)	kIx)
Vzdálený	vzdálený	k2eAgInSc1d1
vesmír	vesmír	k1gInSc1
</s>
<s>
Mlhovina	mlhovina	k1gFnSc1
v	v	k7c6
Orionu	orion	k1gInSc6
•	•	k?
Mlhovina	mlhovina	k1gFnSc1
Koňská	koňský	k2eAgFnSc1d1
hlava	hlava	k1gFnSc1
•	•	k?
Messier	Messier	k1gInSc1
43	#num#	k4
•	•	k?
Messier	Messier	k1gMnSc1
78	#num#	k4
•	•	k?
NGC	NGC	kA
1977	#num#	k4
•	•	k?
NGC	NGC	kA
1981	#num#	k4
•	•	k?
Orion	orion	k1gInSc1
OB1	OB1	k1gFnPc2
Ostatní	ostatní	k2eAgMnSc1d1
</s>
<s>
Orionidy	orionida	k1gFnPc1
•	•	k?
Zimní	zimní	k2eAgInSc1d1
trojúhelník	trojúhelník	k1gInSc1
•	•	k?
Zimní	zimní	k2eAgInSc1d1
šestiúhelník	šestiúhelník	k1gInSc1
•	•	k?
Orionův	Orionův	k2eAgInSc1d1
pás	pás	k1gInSc1
•	•	k?
Orionův	Orionův	k2eAgInSc1d1
meč	meč	k1gInSc1
•	•	k?
Orion	orion	k1gInSc1
(	(	kIx(
<g/>
mytologie	mytologie	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Astronomie	astronomie	k1gFnSc1
</s>
