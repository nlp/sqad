<s>
No	no	k9	no
Name	Name	k1gFnSc1	Name
je	být	k5eAaImIp3nS	být
šestičlenná	šestičlenný	k2eAgFnSc1d1	šestičlenná
slovenská	slovenský	k2eAgFnSc1d1	slovenská
pop-rocková	popockový	k2eAgFnSc1d1	pop-rocková
skupina	skupina	k1gFnSc1	skupina
založená	založený	k2eAgFnSc1d1	založená
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1996	[number]	k4	1996
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgInSc2	ten
dne	den	k1gInSc2	den
zároveň	zároveň	k6eAd1	zároveň
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
soutěži	soutěž	k1gFnSc6	soutěž
kapel	kapela	k1gFnPc2	kapela
zvané	zvaný	k2eAgFnSc2d1	zvaná
Košický	košický	k2eAgInSc4d1	košický
zlatý	zlatý	k1gInSc4	zlatý
poklad	poklad	k1gInSc1	poklad
́	́	k?	́
<g/>
96	[number]	k4	96
<g/>
.	.	kIx.	.
</s>
<s>
Zakládajícími	zakládající	k2eAgMnPc7d1	zakládající
členy	člen	k1gMnPc7	člen
byli	být	k5eAaImAgMnP	být
Viliam	Viliam	k1gMnSc1	Viliam
Gutray	Gutraa	k1gFnSc2	Gutraa
a	a	k8xC	a
trojice	trojice	k1gFnSc2	trojice
bratrů	bratr	k1gMnPc2	bratr
Timkových	Timkův	k2eAgMnPc2d1	Timkův
-	-	kIx~	-
Igor	Igor	k1gMnSc1	Igor
<g/>
,	,	kIx,	,
Roman	Roman	k1gMnSc1	Roman
a	a	k8xC	a
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
.	.	kIx.	.
</s>
<s>
Opravdový	opravdový	k2eAgInSc1d1	opravdový
průlom	průlom	k1gInSc1	průlom
přišel	přijít	k5eAaPmAgInS	přijít
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
s	s	k7c7	s
albem	album	k1gNnSc7	album
Počkám	počkat	k5eAaPmIp1nS	počkat
si	se	k3xPyFc3	se
na	na	k7c4	na
zázrak	zázrak	k1gInSc4	zázrak
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
díky	díky	k7c3	díky
písním	píseň	k1gFnPc3	píseň
"	"	kIx"	"
<g/>
Ty	ten	k3xDgFnPc1	ten
a	a	k8xC	a
Tvoja	Tvoja	k?	Tvoja
sestra	sestra	k1gFnSc1	sestra
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Žily	žít	k5eAaImAgFnP	žít
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgFnP	následovat
úspěšné	úspěšný	k2eAgFnPc1d1	úspěšná
desky	deska	k1gFnPc1	deska
Oslávme	Oslávme	k1gMnPc2	Oslávme
si	se	k3xPyFc3	se
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
Slová	Slová	k1gFnSc1	Slová
do	do	k7c2	do
tmy	tma	k1gFnSc2	tma
a	a	k8xC	a
Čím	čí	k3xOyQgNnSc7	čí
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Leden	leden	k1gInSc1	leden
2008	[number]	k4	2008
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
natáčení	natáčení	k1gNnSc2	natáčení
nové	nový	k2eAgFnPc1d1	nová
desky	deska	k1gFnPc1	deska
V	v	k7c6	v
rovnováhe	rovnováhe	k1gFnSc6	rovnováhe
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
natáčela	natáčet	k5eAaImAgFnS	natáčet
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
Propast	propast	k1gFnSc1	propast
hudebníka	hudebník	k1gMnSc2	hudebník
Petra	Petr	k1gMnSc2	Petr
Jandy	Janda	k1gMnSc2	Janda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
vydali	vydat	k5eAaPmAgMnP	vydat
No	no	k9	no
Name	Name	k1gInSc4	Name
desku	deska	k1gFnSc4	deska
S	s	k7c7	s
láskou	láska	k1gFnSc7	láska
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
na	na	k7c6	na
hudebním	hudební	k2eAgInSc6d1	hudební
webu	web	k1gInSc6	web
iReport	iReport	k1gInSc1	iReport
obdržela	obdržet	k5eAaPmAgFnS	obdržet
hodnocení	hodnocení	k1gNnSc4	hodnocení
čtyř	čtyři	k4xCgFnPc2	čtyři
hvězdiček	hvězdička	k1gFnPc2	hvězdička
z	z	k7c2	z
pěti	pět	k4xCc2	pět
<g/>
.	.	kIx.	.
</s>
<s>
Igor	Igor	k1gMnSc1	Igor
Timko	Timko	k1gNnSc1	Timko
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
Roman	Romana	k1gFnPc2	Romana
Timko	Timko	k1gNnSc1	Timko
-	-	kIx~	-
sólová	sólový	k2eAgFnSc1d1	sólová
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
vokály	vokál	k1gInPc1	vokál
Dušan	Dušan	k1gMnSc1	Dušan
Timko	Timko	k1gNnSc1	Timko
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
Viliam	Viliam	k1gMnSc1	Viliam
Gutray	Gutraa	k1gMnSc2	Gutraa
-	-	kIx~	-
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
Zoli	Zol	k1gFnSc2	Zol
Sallai	Salla	k1gFnSc2	Salla
-	-	kIx~	-
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
,	,	kIx,	,
vokály	vokál	k1gInPc4	vokál
Ivan	Ivan	k1gMnSc1	Ivan
Timko	Timko	k1gNnSc4	Timko
-	-	kIx~	-
bicí	bicí	k2eAgNnSc4d1	bicí
<g/>
,	,	kIx,	,
vokály	vokál	k1gInPc4	vokál
manager	manager	k1gMnSc1	manager
-	-	kIx~	-
Lubomír	Lubomír	k1gMnSc1	Lubomír
Novotný	Novotný	k1gMnSc1	Novotný
produkce	produkce	k1gFnSc1	produkce
-	-	kIx~	-
Věra	Věra	k1gFnSc1	Věra
Novotná	Novotná	k1gFnSc1	Novotná
zvuk	zvuk	k1gInSc1	zvuk
-	-	kIx~	-
Peter	Peter	k1gMnSc1	Peter
Gajdoš	Gajdoš	k1gMnSc1	Gajdoš
technik	technik	k1gMnSc1	technik
-	-	kIx~	-
Ondra	Ondra	k1gMnSc1	Ondra
<g />
.	.	kIx.	.
</s>
<s>
Nowas	Nowas	k1gMnSc1	Nowas
Novotný	Novotný	k1gMnSc1	Novotný
1998	[number]	k4	1998
<g/>
:	:	kIx,	:
No	no	k9	no
Name	Name	k1gInSc1	Name
2000	[number]	k4	2000
<g/>
:	:	kIx,	:
Počkám	počkat	k5eAaPmIp1nS	počkat
si	se	k3xPyFc3	se
na	na	k7c4	na
zázrak	zázrak	k1gInSc4	zázrak
2001	[number]	k4	2001
<g/>
:	:	kIx,	:
Oslávme	Oslávme	k1gFnSc1	Oslávme
si	se	k3xPyFc3	se
život	život	k1gInSc4	život
2003	[number]	k4	2003
<g/>
:	:	kIx,	:
Slová	Slová	k1gFnSc1	Slová
do	do	k7c2	do
tmy	tma	k1gFnSc2	tma
2005	[number]	k4	2005
<g/>
:	:	kIx,	:
Čím	čí	k3xOyRgNnSc7	čí
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
V	v	k7c6	v
rovnováhe	rovnováhe	k1gNnPc6	rovnováhe
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
Best	Best	k1gInSc1	Best
of	of	k?	of
2011	[number]	k4	2011
<g/>
:	:	kIx,	:
Nový	Nový	k1gMnSc1	Nový
album	album	k1gNnSc4	album
2012	[number]	k4	2012
<g/>
:	:	kIx,	:
Love	lov	k1gInSc5	lov
songs	songs	k1gInSc1	songs
2016	[number]	k4	2016
<g/>
:	:	kIx,	:
S	s	k7c7	s
láskou	láska	k1gFnSc7	láska
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
Live	Liv	k1gInSc2	Liv
in	in	k?	in
Prague	Prague	k1gInSc1	Prague
-	-	kIx~	-
Tour	Tour	k1gInSc1	Tour
2006	[number]	k4	2006
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
Ná	Ná	k1gFnSc1	Ná
Ná	Ná	k1gFnSc1	Ná
Ná	Ná	k1gFnSc1	Ná
+	+	kIx~	+
Chinaski	Chinask	k1gMnPc1	Chinask
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
No	no	k9	no
Name	Nam	k1gInPc1	Nam
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
skupiny	skupina	k1gFnSc2	skupina
Oficiální	oficiální	k2eAgInSc1d1	oficiální
facebook	facebook	k1gInSc1	facebook
stránka	stránka	k1gFnSc1	stránka
skupiny	skupina	k1gFnSc2	skupina
</s>
