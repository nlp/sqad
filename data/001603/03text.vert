<s>
Felix	Felix	k1gMnSc1	Felix
Bloch	Bloch	k1gMnSc1	Bloch
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
Curych	Curych	k1gInSc1	Curych
-	-	kIx~	-
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
tamtéž	tamtéž	k6eAd1	tamtéž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
obdržel	obdržet	k5eAaPmAgMnS	obdržet
za	za	k7c4	za
rozvoj	rozvoj	k1gInSc4	rozvoj
nových	nový	k2eAgFnPc2d1	nová
metod	metoda	k1gFnPc2	metoda
pro	pro	k7c4	pro
přesná	přesný	k2eAgNnPc4d1	přesné
měření	měření	k1gNnSc4	měření
jaderného	jaderný	k2eAgInSc2d1	jaderný
magnetismu	magnetismus	k1gInSc2	magnetismus
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
spojené	spojený	k2eAgInPc4d1	spojený
objevy	objev	k1gInPc4	objev
společně	společně	k6eAd1	společně
s	s	k7c7	s
Edwardem	Edward	k1gMnSc7	Edward
Millsem	Mills	k1gMnSc7	Mills
Purcellem	Purcell	k1gMnSc7	Purcell
<g/>
.	.	kIx.	.
</s>
<s>
Lubomír	Lubomír	k1gMnSc1	Lubomír
Sodomka	Sodomka	k1gFnSc1	Sodomka
<g/>
,	,	kIx,	,
Magdalena	Magdalena	k1gFnSc1	Magdalena
Sodomková	Sodomková	k1gFnSc1	Sodomková
<g/>
,	,	kIx,	,
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
SET	set	k1gInSc1	set
OUT	OUT	kA	OUT
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-902058-5-2	[number]	k4	80-902058-5-2
Physics	Physics	k1gInSc4	Physics
Today	Todaa	k1gFnSc2	Todaa
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
37	[number]	k4	37
<g/>
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pp	pp	k?	pp
<g/>
.	.	kIx.	.
115	[number]	k4	115
<g/>
-	-	kIx~	-
<g/>
116	[number]	k4	116
<g/>
.	.	kIx.	.
</s>
<s>
Nature	Natur	k1gMnSc5	Natur
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
170	[number]	k4	170
<g/>
,	,	kIx,	,
pp	pp	k?	pp
<g/>
.	.	kIx.	.
911	[number]	k4	911
<g/>
-	-	kIx~	-
<g/>
912	[number]	k4	912
<g/>
.	.	kIx.	.
</s>
<s>
Nature	Natur	k1gMnSc5	Natur
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
174	[number]	k4	174
<g/>
,	,	kIx,	,
pp	pp	k?	pp
<g/>
.	.	kIx.	.
774	[number]	k4	774
<g/>
-	-	kIx~	-
<g/>
775	[number]	k4	775
<g/>
.	.	kIx.	.
</s>
<s>
McGraw-Hill	McGraw-Hill	k1gMnSc1	McGraw-Hill
Modern	Modern	k1gMnSc1	Modern
Men	Men	k1gMnSc1	Men
of	of	k?	of
Science	Science	k1gFnSc1	Science
<g/>
,	,	kIx,	,
McGraw-Hill	McGraw-Hill	k1gInSc1	McGraw-Hill
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
,	,	kIx,	,
pp	pp	k?	pp
<g/>
.	.	kIx.	.
45	[number]	k4	45
<g/>
-	-	kIx~	-
<g/>
46	[number]	k4	46
<g/>
.	.	kIx.	.
</s>
<s>
National	Nationat	k5eAaPmAgMnS	Nationat
Cyclopaedia	Cyclopaedium	k1gNnPc4	Cyclopaedium
of	of	k?	of
American	Americany	k1gInPc2	Americany
Biography	Biographa	k1gFnSc2	Biographa
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
T.	T.	kA	T.
White	Whit	k1gInSc5	Whit
&	&	k?	&
Co	co	k3yQnSc4	co
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1921	[number]	k4	1921
<g/>
-	-	kIx~	-
<g/>
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
<g/>
,	,	kIx,	,
pp	pp	k?	pp
<g/>
.	.	kIx.	.
310	[number]	k4	310
<g/>
-	-	kIx~	-
<g/>
312	[number]	k4	312
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Felix	Felix	k1gMnSc1	Felix
Bloch	Blocha	k1gFnPc2	Blocha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Felix	Felix	k1gMnSc1	Felix
Bloch	Bloch	k1gMnSc1	Bloch
Official	Official	k1gMnSc1	Official
Nobel	Nobel	k1gMnSc1	Nobel
site	sitat	k5eAaPmIp3nS	sitat
</s>
