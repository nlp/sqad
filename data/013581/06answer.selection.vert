<s>
Ťažký	Ťažký	k2eAgInSc1d1
štít	štít	k1gInSc1
(	(	kIx(
<g/>
česky	česky	k6eAd1
Těžký	těžký	k2eAgInSc1d1
štít	štít	k1gInSc1
<g/>
,	,	kIx,
polsky	polsky	k6eAd1
Ciężki	Ciężk	k1gMnSc3
Szczyt	Szczyt	k1gInSc4
<g/>
,	,	kIx,
maďarsky	maďarsky	k6eAd1
Róth	Róth	k1gInSc1
Márton-csúcs	Márton-csúcsa	k1gFnPc2
<g/>
,	,	kIx,
německy	německy	k6eAd1
Martin-Róth-Spitze	Martin-Róth-Spitze	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
Český	český	k2eAgInSc1d1
štít	štít	k1gInSc1
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
Czeski	Czeski	k1gNnSc1
Szczyt	Szczyt	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
hora	hora	k1gFnSc1
s	s	k7c7
vrcholem	vrchol	k1gInSc7
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
2520	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
ve	v	k7c6
Vysokých	vysoký	k2eAgFnPc6d1
Tatrách	Tatra	k1gFnPc6
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
<g/>
.	.	kIx.
</s>