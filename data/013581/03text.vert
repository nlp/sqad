<s>
Ťažký	Ťažký	k2eAgInSc1d1
štít	štít	k1gInSc1
</s>
<s>
Ťažký	Ťažký	k2eAgInSc1d1
štít	štít	k1gInSc1
v	v	k7c6
Koruně	koruna	k1gFnSc6
Tater	Tatra	k1gFnPc2
Koruna	koruna	k1gFnSc1
Tater	Tatra	k1gFnPc2
<g/>
,	,	kIx,
Ťažký	Ťažký	k2eAgInSc1d1
štít	štít	k1gInSc1
zcela	zcela	k6eAd1
vlevo	vlevo	k6eAd1
</s>
<s>
Vrchol	vrchol	k1gInSc1
</s>
<s>
2520	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Prominence	prominence	k1gFnSc2
</s>
<s>
35	#num#	k4
m	m	kA
↓	↓	k?
sedlo	sedlo	k1gNnSc1
s	s	k7c7
Vysokou	vysoká	k1gFnSc7
Izolace	izolace	k1gFnSc2
</s>
<s>
0,2	0,2	k4
km	km	kA
→	→	k?
Vysoká	vysoký	k2eAgFnSc1d1
Poloha	poloha	k1gFnSc1
Světadíl	světadíl	k1gInSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
Slovensko	Slovensko	k1gNnSc4
Pohoří	pohořet	k5eAaPmIp3nS
</s>
<s>
Vysoké	vysoký	k2eAgFnPc1d1
Tatry	Tatra	k1gFnPc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
10	#num#	k4
<g/>
′	′	k?
<g/>
25	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
20	#num#	k4
<g/>
°	°	k?
<g/>
5	#num#	k4
<g/>
′	′	k?
<g/>
29	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Ťažký	Ťažký	k2eAgInSc1d1
štít	štít	k1gInSc1
</s>
<s>
Prvovýstup	prvovýstup	k1gInSc1
</s>
<s>
1904	#num#	k4
Ernest	Ernest	k1gMnSc1
Dubke	Dubk	k1gInSc2
a	a	k8xC
Johann	Johann	k1gMnSc1
Franz	Franz	k1gMnSc1
st.	st.	kA
Hornina	hornina	k1gFnSc1
</s>
<s>
Žula	žula	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ťažký	Ťažký	k2eAgInSc1d1
štít	štít	k1gInSc1
(	(	kIx(
<g/>
česky	česky	k6eAd1
Těžký	těžký	k2eAgInSc1d1
štít	štít	k1gInSc1
<g/>
,	,	kIx,
polsky	polsky	k6eAd1
Ciężki	Ciężk	k1gMnSc3
Szczyt	Szczyt	k1gInSc4
<g/>
,	,	kIx,
maďarsky	maďarsky	k6eAd1
Róth	Róth	k1gInSc1
Márton-csúcs	Márton-csúcsa	k1gFnPc2
<g/>
,	,	kIx,
německy	německy	k6eAd1
Martin-Róth-Spitze	Martin-Róth-Spitze	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
Český	český	k2eAgInSc1d1
štít	štít	k1gInSc1
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
Czeski	Czeski	k1gNnSc1
Szczyt	Szczyt	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
hora	hora	k1gFnSc1
s	s	k7c7
vrcholem	vrchol	k1gInSc7
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
2520	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
ve	v	k7c6
Vysokých	vysoký	k2eAgFnPc6d1
Tatrách	Tatra	k1gFnPc6
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Poloha	poloha	k1gFnSc1
</s>
<s>
Hora	hora	k1gFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
hlavním	hlavní	k2eAgInSc6d1
hřebeni	hřeben	k1gInSc6
Vysokých	vysoký	k2eAgFnPc2d1
Tater	Tatra	k1gFnPc2
v	v	k7c6
masivu	masiv	k1gInSc6
Vysoké	vysoká	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
vrcholu	vrchol	k1gInSc2
Vysoké	vysoká	k1gFnSc2
na	na	k7c6
východě	východ	k1gInSc6
jej	on	k3xPp3gInSc4
odděluje	oddělovat	k5eAaImIp3nS
štrbina	štrbin	k2eAgFnSc1d1
pod	pod	k7c7
Ťažkým	Ťažký	k1gMnSc7
štítom	štítom	k1gInSc4
<g/>
,	,	kIx,
od	od	k7c2
Rysů	rys	k1gInPc2
na	na	k7c6
severu	sever	k1gInSc6
sedlo	sedlo	k1gNnSc1
Váha	váha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jihozápadním	jihozápadní	k2eAgInSc7d1
směrem	směr	k1gInSc7
vybíhá	vybíhat	k5eAaImIp3nS
hřeben	hřeben	k1gInSc1
Dračí	dračí	k2eAgFnSc2d1
pazúriky	pazúrika	k1gFnSc2
spojující	spojující	k2eAgInSc1d1
Ťažký	Ťažký	k2eAgInSc1d1
štít	štít	k1gInSc1
a	a	k8xC
Kôpky	Kôpka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Západní	západní	k2eAgInPc1d1
<g/>
,	,	kIx,
220	#num#	k4
m	m	kA
vysoká	vysoký	k2eAgFnSc1d1
stěna	stěna	k1gFnSc1
spadá	spadat	k5eAaImIp3nS,k5eAaPmIp3nS
do	do	k7c2
doliny	dolina	k1gFnSc2
Pod	pod	k7c7
Váhou	váha	k1gFnSc7
<g/>
,	,	kIx,
severovýchodní	severovýchodní	k2eAgFnSc1d1
stěna	stěna	k1gFnSc1
do	do	k7c2
Ťažké	Ťažký	k2eAgFnSc2d1
doliny	dolina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jméno	jméno	k1gNnSc1
</s>
<s>
Do	do	k7c2
poloviny	polovina	k1gFnSc2
roku	rok	k1gInSc2
2006	#num#	k4
nesl	nést	k5eAaImAgInS
oficiální	oficiální	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
Český	český	k2eAgInSc1d1
štít	štít	k1gInSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
i	i	k8xC
nadále	nadále	k6eAd1
Czeski	Czeski	k1gNnSc1
Szczyt	Szczyt	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
dvě	dva	k4xCgNnPc4
vysvětlení	vysvětlení	k1gNnPc4
původu	původ	k1gInSc2
tohoto	tento	k3xDgInSc2
názvu	název	k1gInSc2
-	-	kIx~
slovanský	slovanský	k2eAgInSc1d1
původ	původ	k1gInSc1
preferuje	preferovat	k5eAaImIp3nS
slovenská	slovenský	k2eAgFnSc1d1
názvoslovná	názvoslovný	k2eAgFnSc1d1
komise	komise	k1gFnSc1
<g/>
,	,	kIx,
germánský	germánský	k2eAgInSc4d1
původ	původ	k1gInSc4
prosazuje	prosazovat	k5eAaImIp3nS
Anton	Anton	k1gMnSc1
Marec	Marec	k1gMnSc1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
knize	kniha	k1gFnSc6
Po	po	k7c6
stopách	stopa	k1gFnPc6
tatranských	tatranský	k2eAgInPc2d1
názvov	názvov	k1gInSc4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pojmenování	pojmenování	k1gNnSc1
mohlo	moct	k5eAaImAgNnS
vzniknout	vzniknout	k5eAaPmF
chybným	chybný	k2eAgNnSc7d1
porozuměním	porozumění	k1gNnSc7
polského	polský	k2eAgMnSc2d1
či	či	k8xC
goralského	goralský	k2eAgNnSc2d1
jména	jméno	k1gNnSc2
ze	z	k7c2
strany	strana	k1gFnSc2
pruského	pruský	k2eAgMnSc2d1
topografa	topograf	k1gMnSc2
Albrechta	Albrecht	k1gMnSc2
Wilhelma	Wilhelma	k1gFnSc1
von	von	k1gInSc1
Sydow	Sydow	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Goralové	goral	k1gMnPc1
totiž	totiž	k9
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
nářečí	nářečí	k1gNnSc6
označovali	označovat	k5eAaImAgMnP
tato	tento	k3xDgNnPc1
těžko	těžko	k6eAd1
dostupná	dostupný	k2eAgNnPc1d1
místa	místo	k1gNnPc1
slovem	slovem	k6eAd1
"	"	kIx"
<g/>
čenška	čenška	k1gFnSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
"	"	kIx"
<g/>
čynška	čynška	k1gFnSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
tedy	tedy	k9
"	"	kIx"
<g/>
těžká	těžký	k2eAgFnSc1d1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sydow	Sydow	k1gMnSc1
touto	tento	k3xDgFnSc7
oblastí	oblast	k1gFnSc7
v	v	k7c6
roce	rok	k1gInSc6
1827	#num#	k4
cestoval	cestovat	k5eAaImAgInS
a	a	k8xC
později	pozdě	k6eAd2
vydal	vydat	k5eAaPmAgInS
cestopis	cestopis	k1gInSc1
s	s	k7c7
mapou	mapa	k1gFnSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
uvedl	uvést	k5eAaPmAgMnS
Ťažkou	Ťažký	k2eAgFnSc4d1
dolinu	dolina	k1gFnSc4
jako	jako	k8xS,k8xC
Böhmisches	Böhmisches	k1gInSc4
Tal	Tal	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejné	stejné	k1gNnSc4
pojmenování	pojmenování	k1gNnSc2
zapsal	zapsat	k5eAaPmAgMnS
již	již	k6eAd1
před	před	k7c7
ním	on	k3xPp3gMnSc7
Juraj	Juraj	k1gMnSc1
Buchholtz	Buchholtz	k1gMnSc1
starší	starší	k1gMnSc1
roku	rok	k1gInSc2
1719	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
podkladem	podklad	k1gInSc7
dalších	další	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
a	a	k8xC
zpětným	zpětný	k2eAgInSc7d1
překladem	překlad	k1gInSc7
vznikla	vzniknout	k5eAaPmAgFnS
jména	jméno	k1gNnPc1
jako	jako	k8xS,k8xC
Český	český	k2eAgInSc1d1
štít	štít	k1gInSc1
(	(	kIx(
<g/>
nyní	nyní	k6eAd1
Ťažký	Ťažký	k2eAgInSc1d1
štít	štít	k1gInSc1
<g/>
,	,	kIx,
čes.	čes.	k?
Těžký	těžký	k2eAgInSc1d1
štít	štít	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Česká	český	k2eAgFnSc1d1
dolina	dolina	k1gFnSc1
(	(	kIx(
<g/>
nyní	nyní	k6eAd1
Ťažká	Ťažký	k2eAgFnSc1d1
dolina	dolina	k1gFnSc1
<g/>
,	,	kIx,
čes.	čes.	k?
Těžká	těžkat	k5eAaImIp3nS
dolina	dolina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
České	český	k2eAgNnSc1d1
pleso	pleso	k1gNnSc1
(	(	kIx(
<g/>
nyní	nyní	k6eAd1
Ťažké	Ťažký	k2eAgNnSc1d1
pleso	pleso	k1gNnSc1
<g/>
,	,	kIx,
čes.	čes.	k?
Těžké	těžký	k2eAgNnSc4d1
pleso	pleso	k1gNnSc4
<g/>
)	)	kIx)
nebo	nebo	k8xC
Česká	český	k2eAgFnSc1d1
veža	veža	k1gFnSc1
(	(	kIx(
<g/>
nyní	nyní	k6eAd1
Ťažká	Ťažký	k2eAgFnSc1d1
veža	veža	k1gFnSc1
<g/>
,	,	kIx,
čes.	čes.	k?
Těžká	těžkat	k5eAaImIp3nS
věž	věž	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
Český	český	k2eAgInSc1d1
štít	štít	k1gInSc1
ovšem	ovšem	k9
také	také	k9
mohl	moct	k5eAaImAgMnS
pocházet	pocházet	k5eAaImF
z	z	k7c2
německého	německý	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
zechen	zechna	k1gFnPc2
<g/>
,	,	kIx,
čili	čili	k8xC
kopání	kopání	k1gNnSc1
rudy	ruda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
potvrzuje	potvrzovat	k5eAaImIp3nS
i	i	k9
jiný	jiný	k2eAgInSc1d1
název	název	k1gInSc1
doliny	dolina	k1gFnSc2
Sucha	sucho	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
najdeme	najít	k5eAaPmIp1nP
na	na	k7c6
Sydowově	Sydowův	k2eAgFnSc6d1
mapě	mapa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dolina	dolina	k1gFnSc1
se	s	k7c7
dvěma	dva	k4xCgInPc7
velkými	velký	k2eAgInPc7d1
plesy	ples	k1gInPc7
<g/>
,	,	kIx,
protékaná	protékaný	k2eAgFnSc1d1
potokem	potok	k1gInSc7
s	s	k7c7
mohutným	mohutný	k2eAgInSc7d1
vodopádem	vodopád	k1gInSc7
na	na	k7c6
dolním	dolní	k2eAgInSc6d1
konci	konec	k1gInSc6
a	a	k8xC
celoročním	celoroční	k2eAgNnSc7d1
firnoviskem	firnovisko	k1gNnSc7
na	na	k7c6
horním	horní	k2eAgInSc6d1
konci	konec	k1gInSc6
nemohla	moct	k5eNaImAgFnS
být	být	k5eAaImF
považována	považován	k2eAgFnSc1d1
za	za	k7c7
bezvodou	bezvodý	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
už	už	k9
tehdy	tehdy	k6eAd1
o	o	k7c4
špatný	špatný	k2eAgInSc4d1
překlad	překlad	k1gInSc4
německého	německý	k2eAgMnSc2d1
Durtal	Durtal	k1gMnSc1
<g/>
,	,	kIx,
tedy	tedy	k9
údolí	údolí	k1gNnPc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
dolovalo	dolovat	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Suchý	Suchý	k1gMnSc1
je	být	k5eAaImIp3nS
totiž	totiž	k9
německy	německy	k6eAd1
dürr	dürr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Podobné	podobný	k2eAgInPc4d1
údajné	údajný	k2eAgInPc4d1
omyly	omyl	k1gInPc4
v	v	k7c6
toponymech	toponymum	k1gNnPc6
ve	v	k7c6
Vysokých	vysoký	k2eAgFnPc6d1
Tatrách	Tatra	k1gFnPc6
na	na	k7c6
pomezí	pomezí	k1gNnSc6
polského	polský	k2eAgInSc2d1
<g/>
,	,	kIx,
maďarského	maďarský	k2eAgInSc2d1
<g/>
,	,	kIx,
německého	německý	k2eAgInSc2d1
a	a	k8xC
slovenského	slovenský	k2eAgInSc2d1
živlu	živel	k1gInSc2
jsou	být	k5eAaImIp3nP
běžné	běžný	k2eAgNnSc1d1
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
mimo	mimo	k7c4
Ťažkého	Ťažký	k1gMnSc4
štítu	štít	k1gInSc2
je	být	k5eAaImIp3nS
Slovenská	slovenský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
neřeší	řešit	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Významné	významný	k2eAgInPc1d1
výstupy	výstup	k1gInPc1
</s>
<s>
1904	#num#	k4
Prvovýstup	prvovýstup	k1gInSc1
Ernest	Ernest	k1gMnSc1
Dubke	Dubke	k1gInSc1
a	a	k8xC
Johann	Johann	k1gNnSc1
Franz	Franza	k1gFnPc2
st.	st.	kA
<g/>
,	,	kIx,
severozápadním	severozápadní	k2eAgInSc7d1
hřebenem	hřeben	k1gInSc7
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
sestup	sestup	k1gInSc4
jihovýchodním	jihovýchodní	k2eAgInSc7d1
hřebenem	hřeben	k1gInSc7
<g/>
,	,	kIx,
II-III	II-III	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
1909	#num#	k4
Jadwiga	Jadwiga	k1gFnSc1
Roguská	Roguský	k2eAgFnSc1d1
a	a	k8xC
Jacek	Jacek	k1gInSc1
Żuławski	Żuławsk	k1gFnSc2
<g/>
,	,	kIx,
z	z	k7c2
jihu	jih	k1gInSc2
z	z	k7c2
Dračí	dračí	k2eAgFnSc2d1
doliny	dolina	k1gFnSc2
<g/>
,	,	kIx,
II	II	kA
<g/>
,	,	kIx,
normální	normální	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
1932	#num#	k4
Wieslaw	Wieslaw	k1gFnSc1
Stanislawski	Stanislawsk	k1gFnSc2
a	a	k8xC
Wawrzyniec	Wawrzyniec	k1gInSc4
Żuławski	Żuławsk	k1gFnSc2
severovýchodním	severovýchodní	k2eAgInSc7d1
žlabem	žlab	k1gInSc7
z	z	k7c2
Ťažké	Ťažký	k2eAgFnSc2d1
doliny	dolina	k1gFnSc2
<g/>
,	,	kIx,
V.	V.	kA
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Ciężki	Ciężki	k1gNnSc1
Szczyt	Szczyt	k1gInSc1
na	na	k7c6
polské	polský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Arno	Arno	k1gMnSc1
Puškáš	Puškáš	k1gMnSc1
<g/>
:	:	kIx,
Vysoké	vysoký	k2eAgFnSc2d1
Tatry	Tatra	k1gFnSc2
<g/>
,	,	kIx,
Horolezecký	horolezecký	k2eAgMnSc1d1
sprievodca	sprievodca	k1gMnSc1
<g/>
,	,	kIx,
Diel	Diel	k1gMnSc1
V	V	kA
<g/>
,	,	kIx,
vydavateľstvo	vydavateľstvo	k1gNnSc1
Šport	Športa	k1gFnPc2
<g/>
,	,	kIx,
Bratislava	Bratislava	k1gFnSc1
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
↑	↑	k?
Klub	klub	k1gInSc1
přátel	přítel	k1gMnPc2
Vysokých	vysoký	k2eAgFnPc2d1
Tater	Tatra	k1gFnPc2
-	-	kIx~
Článek	článek	k1gInSc1
o	o	k7c4
přejmenování	přejmenování	k1gNnSc4
Českého	český	k2eAgInSc2d1
štítu	štít	k1gInSc2
<g/>
.	.	kIx.
www.tatrys.cz	www.tatrys.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Po	po	k7c6
stopách	stopa	k1gFnPc6
tatranských	tatranský	k2eAgInPc2d1
názvov	názvov	k1gInSc4
-	-	kIx~
recenze	recenze	k1gFnPc1
<g/>
↑	↑	k?
Ivan	Ivan	k1gMnSc1
Bohuš	Bohuš	k1gMnSc1
<g/>
:	:	kIx,
Omyly	omyl	k1gInPc1
v	v	k7c6
názvosloví	názvosloví	k1gNnSc6
Vysokých	vysoký	k2eAgFnPc2d1
Tater	Tatra	k1gFnPc2
<g/>
↑	↑	k?
Severozápadní	severozápadní	k2eAgInSc4d1
hřeben	hřeben	k1gInSc4
na	na	k7c4
Ťažký	Ťažký	k2eAgInSc4d1
štít	štít	k1gInSc4
-	-	kIx~
horolezecký	horolezecký	k2eAgMnSc1d1
průvodce	průvodce	k1gMnSc1
<g/>
↑	↑	k?
Normální	normální	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
na	na	k7c4
Ťažký	Ťažký	k2eAgInSc4d1
štít	štít	k1gInSc4
-	-	kIx~
turistický	turistický	k2eAgMnSc1d1
průvodce	průvodce	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Ťažký	Ťažký	k2eAgInSc4d1
štít	štít	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Tatry	Tatra	k1gFnPc1
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
také	také	k9
Ochrana	ochrana	k1gFnSc1
přírody	příroda	k1gFnSc2
v	v	k7c6
Tatrách	Tatra	k1gFnPc6
<g/>
)	)	kIx)
Podcelky	Podcelek	k1gInPc4
</s>
<s>
Západní	západní	k2eAgFnPc1d1
Tatry	Tatra	k1gFnPc1
(	(	kIx(
<g/>
Červené	Červené	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
•	•	k?
Kasprov	Kasprov	k1gInSc1
vrch	vrch	k1gInSc1
•	•	k?
Liptovské	liptovský	k2eAgInPc1d1
kopy	kop	k1gInPc1
•	•	k?
Liptovské	liptovský	k2eAgFnSc2d1
Tatry	Tatra	k1gFnSc2
•	•	k?
Lúčna	Lúčn	k1gInSc2
•	•	k?
Ornak	Ornak	k1gMnSc1
•	•	k?
Osobitá	osobitý	k2eAgFnSc1d1
•	•	k?
Roháče	roháč	k1gMnSc2
•	•	k?
Sivý	sivý	k2eAgInSc4d1
vrch	vrch	k1gInSc4
<g/>
)	)	kIx)
<g/>
Východní	východní	k2eAgFnSc1d1
Tatry	Tatra	k1gFnPc1
(	(	kIx(
<g/>
Belianské	Belianský	k2eAgFnPc1d1
Tatry	Tatra	k1gFnPc1
•	•	k?
Vysoké	vysoký	k2eAgFnPc1d1
Tatry	Tatra	k1gFnPc1
<g/>
)	)	kIx)
Vybrané	vybraný	k2eAgInPc1d1
vrcholy	vrchol	k1gInPc1
Východních	východní	k2eAgFnPc2d1
Tater	Tatra	k1gFnPc2
</s>
<s>
Gerlachovský	Gerlachovský	k2eAgMnSc1d1
(	(	kIx(
<g/>
2654	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Gerlachovská	Gerlachovský	k2eAgFnSc1d1
veža	veža	k1gFnSc1
(	(	kIx(
<g/>
2642	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Lomnický	lomnický	k2eAgMnSc1d1
(	(	kIx(
<g/>
2633	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ľadový	Ľadový	k2eAgMnSc1d1
(	(	kIx(
<g/>
2627	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pyšný	pyšný	k2eAgMnSc1d1
(	(	kIx(
<g/>
2621	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Zadný	Zadný	k1gMnSc1
Gerlach	Gerlach	k1gMnSc1
(	(	kIx(
<g/>
2616	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Lavínový	Lavínový	k2eAgMnSc1d1
(	(	kIx(
<g/>
2606	#num#	k4
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Malý	Malý	k1gMnSc1
Ľadový	Ľadový	k2eAgMnSc1d1
(	(	kIx(
<g/>
2602	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Kotlový	kotlový	k2eAgMnSc1d1
(	(	kIx(
<g/>
2601	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Lavínová	Lavínový	k2eAgFnSc1d1
veža	veža	k1gFnSc1
(	(	kIx(
<g/>
2600	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Malý	Malý	k1gMnSc1
Pyšný	pyšný	k2eAgMnSc1d1
(	(	kIx(
<g/>
2590	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Veľká	Veľký	k2eAgFnSc1d1
Litvorová	Litvorový	k2eAgFnSc1d1
veža	veža	k1gFnSc1
(	(	kIx(
<g/>
2581	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Strapatá	strapatý	k2eAgFnSc1d1
veža	veža	k1gFnSc1
(	(	kIx(
<g/>
2565	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Kežmarský	kežmarský	k2eAgInSc4d1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
2556	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vysoká	vysoká	k1gFnSc1
(	(	kIx(
<g/>
2547	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Malá	malý	k2eAgFnSc1d1
Litvorova	Litvorův	k2eAgFnSc1d1
veža	veža	k1gFnSc1
(	(	kIx(
<g/>
2547	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Supia	Supia	k1gFnSc1
veža	veža	k1gFnSc1
(	(	kIx(
<g/>
2540	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Končistá	Končistý	k2eAgFnSc1d1
(	(	kIx(
<g/>
2537	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Baranie	Baranie	k1gFnSc1
rohy	roh	k1gInPc4
(	(	kIx(
<g/>
2526	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Dračí	dračí	k2eAgNnSc1d1
(	(	kIx(
<g/>
2523	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Veľká	Veľký	k2eAgFnSc1d1
Vidlová	vidlový	k2eAgFnSc1d1
veža	veža	k1gFnSc1
(	(	kIx(
<g/>
2522	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Veterný	Veterný	k2eAgMnSc1d1
(	(	kIx(
<g/>
2515	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Malý	Malý	k1gMnSc1
Kežmarský	kežmarský	k2eAgMnSc1d1
(	(	kIx(
<g/>
2514	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Zadný	Zadný	k1gMnSc1
Ľadový	Ľadový	k2eAgMnSc1d1
(	(	kIx(
<g/>
2512	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Rysy	rys	k1gMnPc7
(	(	kIx(
<g/>
2503	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Kriváň	Kriváň	k1gInSc1
(	(	kIx(
<g/>
2495	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Snehový	Snehový	k2eAgInSc4d1
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
2465	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Slavkovský	slavkovský	k2eAgMnSc1d1
(	(	kIx(
<g/>
2452	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Východná	Východný	k2eAgFnSc1d1
Vysoká	vysoká	k1gFnSc1
(	(	kIx(
<g/>
2429	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Kôprovský	Kôprovský	k2eAgMnSc1d1
(	(	kIx(
<g/>
2363	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Kozi	Kozi	k1gNnSc1
Wierch	Wierch	k1gMnSc1
(	(	kIx(
<g/>
2291	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jahňací	Jahňací	k1gFnSc1
(	(	kIx(
<g/>
2230	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Havran	Havran	k1gMnSc1
(	(	kIx(
<g/>
2151	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ždiarska	Ždiarska	k1gFnSc1
vidla	vidla	k1gFnSc1
(	(	kIx(
<g/>
2141	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Hlúpy	Hlúpa	k1gFnSc2
(	(	kIx(
<g/>
2061	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Zadné	Zadný	k2eAgFnSc2d1
Jatky	jatka	k1gFnSc2
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
Sedla	sedlo	k1gNnSc2
Východních	východní	k2eAgFnPc2d1
Tater	Tatra	k1gFnPc2
</s>
<s>
Bystré	bystrý	k2eAgInPc4d1
(	(	kIx(
<g/>
2314	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
Bystrá	bystrý	k2eAgFnSc1d1
lávka	lávka	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Kopské	Kopské	k1gNnSc4
(	(	kIx(
<g/>
1750	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ľaliové	Ľalius	k1gMnPc1
(	(	kIx(
<g/>
1952	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Lomnické	lomnický	k2eAgNnSc1d1
(	(	kIx(
<g/>
2190	#num#	k4
<g/>
)	)	kIx)
•	•	k?
pod	pod	k7c7
Ostrvou	ostrva	k1gFnSc7
(	(	kIx(
<g/>
1966	#num#	k4
<g/>
)	)	kIx)
•	•	k?
pod	pod	k7c7
Svišťovkou	Svišťovka	k1gFnSc7
(	(	kIx(
<g/>
2023	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Poľský	Poľský	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
hrebeň	hrebeň	k1gFnSc1
(	(	kIx(
<g/>
2200	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Predné	Predný	k2eAgFnSc2d1
Kopské	Kopská	k1gFnSc2
(	(	kIx(
<g/>
1780	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Priečne	Priečn	k1gInSc5
(	(	kIx(
<g/>
2353	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Prielom	Prielom	k1gInSc1
(	(	kIx(
<g/>
2290	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sedielko	Sedielka	k1gFnSc5
(	(	kIx(
<g/>
2376	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Široké	Široká	k1gFnSc2
(	(	kIx(
<g/>
1826	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Váha	váha	k1gFnSc1
(	(	kIx(
<g/>
2340	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vyšné	vyšný	k2eAgNnSc1d1
Kôprovské	Kôprovský	k2eAgNnSc1d1
(	(	kIx(
<g/>
2180	#num#	k4
<g/>
)	)	kIx)
Vybrané	vybraný	k2eAgInPc1d1
vrcholy	vrchol	k1gInPc1
v	v	k7c6
Západních	západní	k2eAgFnPc6d1
Tatrách	Tatra	k1gFnPc6
</s>
<s>
Bystrá	bystrý	k2eAgFnSc1d1
(	(	kIx(
<g/>
2248	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jakubina	Jakubina	k1gFnSc1
(	(	kIx(
<g/>
2194	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Baranec	Baranec	k1gInSc1
(	(	kIx(
<g/>
2185	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Baníkov	Baníkov	k1gInSc1
(	(	kIx(
<g/>
2178	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Plačlivé	plačlivý	k2eAgNnSc1d1
(	(	kIx(
<g/>
2125	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ostrý	ostrý	k2eAgInSc4d1
Roháč	roháč	k1gInSc4
(	(	kIx(
<g/>
2088	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Volovec	Volovec	k1gInSc1
(	(	kIx(
<g/>
2063	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Brestová	Brestová	k1gFnSc1
(	(	kIx(
<g/>
1903	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Wielki	Wielki	k1gNnSc1
Giewont	Giewont	k1gMnSc1
(	(	kIx(
<g/>
1895	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sivý	sivý	k2eAgInSc4d1
vrch	vrch	k1gInSc4
(	(	kIx(
<g/>
1805	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Osobitá	osobitý	k2eAgFnSc1d1
(	(	kIx(
<g/>
1687	#num#	k4
<g/>
)	)	kIx)
Sedla	sednout	k5eAaPmAgFnS
v	v	k7c6
Západních	západní	k2eAgFnPc6d1
Tatrách	Tatra	k1gFnPc6
</s>
<s>
Baníkovské	Baníkovský	k2eAgInPc4d1
(	(	kIx(
<g/>
2045	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bystré	bystrý	k2eAgNnSc1d1
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Gáborovo	Gáborův	k2eAgNnSc1d1
(	(	kIx(
<g/>
1890	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jamnícke	Jamnícke	k1gInSc1
(	(	kIx(
<g/>
1908	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pálenica	Pálenica	k1gFnSc1
(	(	kIx(
<g/>
1570	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Parichvost	Parichvost	k1gFnSc1
(	(	kIx(
<g/>
1870	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pyšné	pyšný	k2eAgNnSc1d1
(	(	kIx(
<g/>
1791	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Račkovo	Račkův	k2eAgNnSc1d1
(	(	kIx(
<g/>
1958	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Smutné	Smutná	k1gFnSc2
(	(	kIx(
<g/>
1965	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Tomanovské	Tomanovský	k2eAgNnSc1d1
(	(	kIx(
<g/>
1686	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Žiarske	Žiarske	k1gInSc1
(	(	kIx(
<g/>
1917	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Slovensko	Slovensko	k1gNnSc1
</s>
