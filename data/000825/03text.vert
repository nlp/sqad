<s>
Mehmed	Mehmed	k1gMnSc1	Mehmed
II	II	kA	II
<g/>
.	.	kIx.	.
Fatih	Fatih	k1gMnSc1	Fatih
(	(	kIx(	(
<g/>
Dobyvatel	dobyvatel	k1gMnSc1	dobyvatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1432	[number]	k4	1432
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1481	[number]	k4	1481
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
turecký	turecký	k2eAgMnSc1d1	turecký
sultán	sultán	k1gMnSc1	sultán
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
Osmanů	Osman	k1gMnPc2	Osman
<g/>
,	,	kIx,	,
vládl	vládnout	k5eAaImAgInS	vládnout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1451	[number]	k4	1451
<g/>
-	-	kIx~	-
<g/>
1481	[number]	k4	1481
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
nástupu	nástup	k1gInSc2	nástup
Mehmeda	Mehmeda	k1gMnSc1	Mehmeda
II	II	kA	II
<g/>
.	.	kIx.	.
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
se	se	k3xPyFc4	se
osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
rozkládala	rozkládat	k5eAaImAgFnS	rozkládat
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Malé	Malé	k2eAgFnSc2d1	Malé
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
na	na	k7c6	na
Balkánském	balkánský	k2eAgInSc6d1	balkánský
poloostrově	poloostrov	k1gInSc6	poloostrov
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
<g/>
,	,	kIx,	,
Makedonie	Makedonie	k1gFnSc2	Makedonie
a	a	k8xC	a
severního	severní	k2eAgNnSc2d1	severní
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
obklopena	obklopen	k2eAgFnSc1d1	obklopena
řadou	řada	k1gFnSc7	řada
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
závislých	závislý	k2eAgFnPc2d1	závislá
území	území	k1gNnPc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
středu	střed	k1gInSc6	střed
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgInP	nacházet
zbytky	zbytek	k1gInPc1	zbytek
byzantské	byzantský	k2eAgFnSc2d1	byzantská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
tvořené	tvořený	k2eAgFnSc6d1	tvořená
už	už	k6eAd1	už
jen	jen	k9	jen
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc7	jeho
okolím	okolí	k1gNnSc7	okolí
a	a	k8xC	a
několika	několik	k4yIc7	několik
územími	území	k1gNnPc7	území
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
roku	rok	k1gInSc2	rok
1453	[number]	k4	1453
oblehlo	oblehnout	k5eAaPmAgNnS	oblehnout
vojsko	vojsko	k1gNnSc1	vojsko
Mehmeda	Mehmed	k1gMnSc2	Mehmed
II	II	kA	II
<g/>
.	.	kIx.	.
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
150	[number]	k4	150
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
145	[number]	k4	145
lodí	loď	k1gFnPc2	loď
Konstantinopol	Konstantinopol	k1gInSc1	Konstantinopol
<g/>
.	.	kIx.	.
</s>
<s>
Byzantský	byzantský	k2eAgMnSc1d1	byzantský
císař	císař	k1gMnSc1	císař
Konstantin	Konstantin	k1gMnSc1	Konstantin
XI	XI	kA	XI
<g/>
.	.	kIx.	.
</s>
<s>
Dragases	Dragases	k1gInSc1	Dragases
mohl	moct	k5eAaImAgInS	moct
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
města	město	k1gNnSc2	město
postavit	postavit	k5eAaPmF	postavit
pouze	pouze	k6eAd1	pouze
12	[number]	k4	12
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
26	[number]	k4	26
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Obléhání	obléhání	k1gNnSc1	obléhání
trvalo	trvat	k5eAaImAgNnS	trvat
necelé	celý	k2eNgInPc4d1	necelý
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
<g/>
,	,	kIx,	,
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1453	[number]	k4	1453
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
dobyto	dobýt	k5eAaPmNgNnS	dobýt
a	a	k8xC	a
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
pleněno	pleněn	k2eAgNnSc1d1	pleněno
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
definitivně	definitivně	k6eAd1	definitivně
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
byzantská	byzantský	k2eAgFnSc1d1	byzantská
říše	říše	k1gFnSc1	říše
a	a	k8xC	a
Konstantinopol	Konstantinopol	k1gInSc1	Konstantinopol
přejmenovaná	přejmenovaný	k2eAgFnSc1d1	přejmenovaná
na	na	k7c4	na
Istanbul	Istanbul	k1gInSc4	Istanbul
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
centrem	centr	k1gMnSc7	centr
nové	nový	k2eAgFnSc2d1	nová
osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
pozornost	pozornost	k1gFnSc1	pozornost
sultána	sultána	k1gFnSc1	sultána
obrátila	obrátit	k5eAaPmAgFnS	obrátit
na	na	k7c4	na
Balkánský	balkánský	k2eAgInSc4d1	balkánský
poloostrov	poloostrov	k1gInSc4	poloostrov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
upevňoval	upevňovat	k5eAaImAgMnS	upevňovat
osmanskou	osmanský	k2eAgFnSc4d1	Osmanská
moc	moc	k1gFnSc4	moc
postupným	postupný	k2eAgNnSc7d1	postupné
začleňováním	začleňování	k1gNnSc7	začleňování
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jen	jen	k9	jen
závislých	závislý	k2eAgFnPc2d1	závislá
území	území	k1gNnSc4	území
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
byl	být	k5eAaImAgMnS	být
jeho	on	k3xPp3gInSc4	on
postup	postup	k1gInSc4	postup
v	v	k7c6	v
Srbsku	Srbsko	k1gNnSc6	Srbsko
a	a	k8xC	a
Bosně	Bosna	k1gFnSc6	Bosna
zastaven	zastaven	k2eAgInSc1d1	zastaven
roku	rok	k1gInSc2	rok
1456	[number]	k4	1456
spojenými	spojený	k2eAgInPc7d1	spojený
vojsky	vojsko	k1gNnPc7	vojsko
mnicha	mnich	k1gMnSc2	mnich
Jana	Jan	k1gMnSc2	Jan
Kapistránského	Kapistránský	k2eAgMnSc2d1	Kapistránský
a	a	k8xC	a
uherského	uherský	k2eAgMnSc2d1	uherský
magnáta	magnát	k1gMnSc2	magnát
Jana	Jan	k1gMnSc2	Jan
Hunyadiho	Hunyadi	k1gMnSc2	Hunyadi
u	u	k7c2	u
Bělehradu	Bělehrad	k1gInSc2	Bělehrad
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1459	[number]	k4	1459
byla	být	k5eAaImAgFnS	být
ze	z	k7c2	z
Srbska	Srbsko	k1gNnSc2	Srbsko
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
osmanská	osmanský	k2eAgFnSc1d1	Osmanská
provincie	provincie	k1gFnSc1	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
moc	moc	k6eAd1	moc
Mehmed	Mehmed	k1gMnSc1	Mehmed
dále	daleko	k6eAd2	daleko
rozšiřoval	rozšiřovat	k5eAaImAgMnS	rozšiřovat
i	i	k9	i
v	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
dobyty	dobyt	k2eAgFnPc4d1	dobyta
Atény	Atény	k1gFnPc4	Atény
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
přičlenil	přičlenit	k5eAaPmAgMnS	přičlenit
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
říši	říš	k1gFnSc3	říš
trapezuntské	trapezuntský	k2eAgNnSc1d1	Trapezuntské
císařství	císařství	k1gNnSc1	císařství
a	a	k8xC	a
území	území	k1gNnSc1	území
Karamanovců	Karamanovec	k1gMnPc2	Karamanovec
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1475	[number]	k4	1475
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
i	i	k8xC	i
krymský	krymský	k2eAgInSc1d1	krymský
chanát	chanát	k1gInSc1	chanát
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Mehmeda	Mehmed	k1gMnSc2	Mehmed
II	II	kA	II
<g/>
.	.	kIx.	.
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
základních	základní	k2eAgInPc6d1	základní
rysech	rys	k1gInPc6	rys
dokončena	dokončit	k5eAaPmNgFnS	dokončit
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
organizace	organizace	k1gFnSc1	organizace
osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
správní	správní	k2eAgFnSc1d1	správní
i	i	k8xC	i
vojenská	vojenský	k2eAgFnSc1d1	vojenská
<g/>
.	.	kIx.	.
</s>
<s>
Světské	světský	k2eAgNnSc1d1	světské
právo	právo	k1gNnSc1	právo
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
v	v	k7c6	v
Kanunnáme	Kanunnáme	k1gFnSc6	Kanunnáme
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Kniha	kniha	k1gFnSc1	kniha
zákonů	zákon	k1gInPc2	zákon
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
zahrnující	zahrnující	k2eAgFnPc1d1	zahrnující
dynastické	dynastický	k2eAgFnPc1d1	dynastická
<g/>
,	,	kIx,	,
administrativní	administrativní	k2eAgFnPc1d1	administrativní
i	i	k8xC	i
soudní	soudní	k2eAgFnPc1d1	soudní
praktiky	praktika	k1gFnPc1	praktika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
docházelo	docházet	k5eAaImAgNnS	docházet
také	také	k9	také
k	k	k7c3	k
postupnému	postupný	k2eAgNnSc3d1	postupné
ovlivňování	ovlivňování	k1gNnSc3	ovlivňování
islámské	islámský	k2eAgFnPc1d1	islámská
<g/>
,	,	kIx,	,
arabsko-perské	arabskoerský	k2eAgFnPc1d1	arabsko-perský
kultury	kultura	k1gFnPc1	kultura
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
si	se	k3xPyFc3	se
Turci	Turek	k1gMnPc1	Turek
přinesli	přinést	k5eAaPmAgMnP	přinést
z	z	k7c2	z
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
místní	místní	k2eAgFnSc7d1	místní
byzantskou	byzantský	k2eAgFnSc7d1	byzantská
tradicí	tradice	k1gFnSc7	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
sultán	sultán	k1gMnSc1	sultán
tento	tento	k3xDgInSc4	tento
proces	proces	k1gInSc4	proces
podporoval	podporovat	k5eAaImAgInS	podporovat
<g/>
,	,	kIx,	,
uvědomoval	uvědomovat	k5eAaImAgInS	uvědomovat
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
dobytí	dobytí	k1gNnSc6	dobytí
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
něho	on	k3xPp3gMnSc4	on
nejen	nejen	k6eAd1	nejen
místním	místní	k2eAgNnSc7d1	místní
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
západní	západní	k2eAgFnSc7d1	západní
Evropou	Evropa	k1gFnSc7	Evropa
pohlíženo	pohlížen	k2eAgNnSc1d1	pohlíženo
jako	jako	k9	jako
na	na	k7c4	na
dědice	dědic	k1gMnPc4	dědic
byzantských	byzantský	k2eAgInPc2d1	byzantský
císařů	císař	k1gMnPc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
byl	být	k5eAaImAgMnS	být
zbožný	zbožný	k2eAgMnSc1d1	zbožný
muslim	muslim	k1gMnSc1	muslim
<g/>
,	,	kIx,	,
obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
mimo	mimo	k7c4	mimo
islámského	islámský	k2eAgMnSc4d1	islámský
také	také	k6eAd1	také
antické	antický	k2eAgNnSc4d1	antické
a	a	k8xC	a
italské	italský	k2eAgNnSc4d1	italské
umění	umění	k1gNnSc4	umění
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
paláci	palác	k1gInSc6	palác
měl	mít	k5eAaImAgMnS	mít
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
sbírku	sbírka	k1gFnSc4	sbírka
soudobého	soudobý	k2eAgNnSc2d1	soudobé
italského	italský	k2eAgNnSc2d1	italské
malířství	malířství	k1gNnSc2	malířství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
Mehmeda	Mehmeda	k1gMnSc1	Mehmeda
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
nástupců	nástupce	k1gMnPc2	nástupce
žila	žít	k5eAaImAgFnS	žít
řada	řada	k1gFnSc1	řada
významných	významný	k2eAgMnPc2d1	významný
tureckých	turecký	k2eAgMnPc2d1	turecký
literátů	literát	k1gMnPc2	literát
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
zálibou	záliba	k1gFnSc7	záliba
bylo	být	k5eAaImAgNnS	být
zahradnictví	zahradnictví	k1gNnSc1	zahradnictví
a	a	k8xC	a
rostliny	rostlina	k1gFnPc1	rostlina
v	v	k7c6	v
palácových	palácový	k2eAgFnPc6d1	palácová
zahradách	zahrada	k1gFnPc6	zahrada
dokonce	dokonce	k9	dokonce
sám	sám	k3xTgMnSc1	sám
sázel	sázet	k5eAaImAgMnS	sázet
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
povahu	povaha	k1gFnSc4	povaha
<g/>
,	,	kIx,	,
Mehmed	Mehmed	k1gInSc1	Mehmed
byl	být	k5eAaImAgInS	být
prý	prý	k9	prý
zvídavý	zvídavý	k2eAgMnSc1d1	zvídavý
<g/>
,	,	kIx,	,
inteligentní	inteligentní	k2eAgMnSc1d1	inteligentní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
prchlivý	prchlivý	k2eAgInSc1d1	prchlivý
a	a	k8xC	a
krutý	krutý	k2eAgInSc1d1	krutý
<g/>
.	.	kIx.	.
</s>
<s>
Mehmed	Mehmed	k1gMnSc1	Mehmed
II	II	kA	II
<g/>
.	.	kIx.	.
zemřel	zemřít	k5eAaPmAgMnS	zemřít
po	po	k7c6	po
několikaleté	několikaletý	k2eAgFnSc6d1	několikaletá
nemoci	nemoc	k1gFnSc6	nemoc
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
trpěl	trpět	k5eAaImAgMnS	trpět
dnou	dna	k1gFnSc7	dna
a	a	k8xC	a
vodnatelností	vodnatelnost	k1gFnSc7	vodnatelnost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pochován	pochován	k2eAgMnSc1d1	pochován
v	v	k7c6	v
Istanbulu	Istanbul	k1gInSc6	Istanbul
poblíž	poblíž	k6eAd1	poblíž
džámi	džá	k1gFnPc7	džá
<g/>
'	'	kIx"	'
<g/>
e	e	k0	e
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
založil	založit	k5eAaPmAgMnS	založit
<g/>
.	.	kIx.	.
</s>
<s>
TAUER	TAUER	kA	TAUER
<g/>
,	,	kIx,	,
Felix	Felix	k1gMnSc1	Felix
<g/>
,	,	kIx,	,
Svět	svět	k1gInSc1	svět
islámu	islám	k1gInSc2	islám
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
Altun	Altuna	k1gFnPc2	Altuna
</s>
