<s>
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Richard	Richard	k1gMnSc1	Richard
Wagner	Wagner	k1gMnSc1	Wagner
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1813	[number]	k4	1813
<g/>
,	,	kIx,	,
Lipsko	Lipsko	k1gNnSc1	Lipsko
–	–	k?	–
13	[number]	k4	13
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1883	[number]	k4	1883
<g/>
,	,	kIx,	,
Benátky	Benátky	k1gFnPc1	Benátky
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
výrazný	výrazný	k2eAgMnSc1d1	výrazný
představitel	představitel	k1gMnSc1	představitel
hudebního	hudební	k2eAgInSc2d1	hudební
romantismu	romantismus	k1gInSc2	romantismus
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	on	k3xPp3gMnSc4	on
nejznámější	známý	k2eAgNnPc1d3	nejznámější
díla	dílo	k1gNnPc1	dílo
patří	patřit	k5eAaImIp3nP	patřit
opery	opera	k1gFnPc1	opera
Rienzi	Rienze	k1gFnSc3	Rienze
<g/>
,	,	kIx,	,
Bludný	bludný	k2eAgMnSc1d1	bludný
Holanďan	Holanďan	k1gMnSc1	Holanďan
<g/>
,	,	kIx,	,
Tannhäuser	Tannhäuser	k1gMnSc1	Tannhäuser
<g/>
,	,	kIx,	,
Lohengrin	Lohengrin	k1gInSc1	Lohengrin
<g/>
,	,	kIx,	,
Tristan	Tristan	k1gInSc1	Tristan
a	a	k8xC	a
Isolda	Isolda	k1gFnSc1	Isolda
<g/>
,	,	kIx,	,
Mistři	mistr	k1gMnPc1	mistr
pěvci	pěvec	k1gMnPc1	pěvec
norimberští	norimberský	k2eAgMnPc1d1	norimberský
<g/>
,	,	kIx,	,
operní	operní	k2eAgFnSc1d1	operní
tetralogie	tetralogie	k1gFnSc1	tetralogie
Prsten	prsten	k1gInSc4	prsten
Nibelungův	Nibelungův	k2eAgInSc1d1	Nibelungův
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
poslední	poslední	k2eAgFnSc1d1	poslední
opera	opera	k1gFnSc1	opera
Parsifal	Parsifal	k1gFnSc2	Parsifal
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
let	léto	k1gNnPc2	léto
žil	žíla	k1gFnPc2	žíla
mimo	mimo	k7c4	mimo
Německo	Německo	k1gNnSc4	Německo
–	–	k?	–
především	především	k6eAd1	především
v	v	k7c6	v
Lotyšsku	Lotyšsko	k1gNnSc6	Lotyšsko
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Rakousku	Rakousko	k1gNnSc6	Rakousko
a	a	k8xC	a
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Nejdelší	dlouhý	k2eAgNnSc4d3	nejdelší
období	období	k1gNnSc4	období
však	však	k9	však
strávil	strávit	k5eAaPmAgMnS	strávit
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
usadil	usadit	k5eAaPmAgMnS	usadit
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
když	když	k8xS	když
po	po	k7c6	po
nezdařené	zdařený	k2eNgFnSc6d1	nezdařená
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
musel	muset	k5eAaImAgMnS	muset
uprchnout	uprchnout	k5eAaPmF	uprchnout
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Několikrát	několikrát	k6eAd1	několikrát
navštívil	navštívit	k5eAaPmAgMnS	navštívit
i	i	k9	i
Čechy	Čech	k1gMnPc4	Čech
–	–	k?	–
především	především	k6eAd1	především
Prahu	Praha	k1gFnSc4	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
uvedl	uvést	k5eAaPmAgMnS	uvést
některá	některý	k3yIgNnPc4	některý
svá	svůj	k3xOyFgNnPc4	svůj
díla	dílo	k1gNnPc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
strávil	strávit	k5eAaPmAgInS	strávit
i	i	k9	i
v	v	k7c6	v
Teplicích	Teplice	k1gFnPc6	Teplice
<g/>
;	;	kIx,	;
výlet	výlet	k1gInSc4	výlet
na	na	k7c4	na
hrad	hrad	k1gInSc4	hrad
Střekov	Střekov	k1gInSc1	Střekov
jej	on	k3xPp3gMnSc4	on
inspiroval	inspirovat	k5eAaBmAgInS	inspirovat
ke	k	k7c3	k
složení	složení	k1gNnSc3	složení
opery	opera	k1gFnSc2	opera
Tannhäuser	Tannhäusra	k1gFnPc2	Tannhäusra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bayreuthu	Bayreuth	k1gInSc6	Bayreuth
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1873	[number]	k4	1873
umožněno	umožnit	k5eAaPmNgNnS	umožnit
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
bavorského	bavorský	k2eAgMnSc2d1	bavorský
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
II	II	kA	II
<g/>
.	.	kIx.	.
postavit	postavit	k5eAaPmF	postavit
pro	pro	k7c4	pro
inscenace	inscenace	k1gFnPc4	inscenace
svých	svůj	k3xOyFgFnPc2	svůj
oper	opera	k1gFnPc2	opera
divadlo	divadlo	k1gNnSc1	divadlo
s	s	k7c7	s
tehdy	tehdy	k6eAd1	tehdy
vynikající	vynikající	k2eAgFnSc7d1	vynikající
akustikou	akustika	k1gFnSc7	akustika
a	a	k8xC	a
založit	založit	k5eAaPmF	založit
zde	zde	k6eAd1	zde
tradici	tradice	k1gFnSc4	tradice
Hudebních	hudební	k2eAgFnPc2d1	hudební
slavností	slavnost	k1gFnPc2	slavnost
v	v	k7c6	v
Bayreuthu	Bayreuth	k1gInSc6	Bayreuth
(	(	kIx(	(
<g/>
Bayreuther	Bayreuthra	k1gFnPc2	Bayreuthra
Festspiele	Festspiel	k1gInSc2	Festspiel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
si	se	k3xPyFc3	se
svoji	svůj	k3xOyFgFnSc4	svůj
světovou	světový	k2eAgFnSc4d1	světová
proslulost	proslulost	k1gFnSc4	proslulost
udržují	udržovat	k5eAaImIp3nP	udržovat
až	až	k9	až
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
skladatelovy	skladatelův	k2eAgInPc4d1	skladatelův
protižidovské	protižidovský	k2eAgInPc4d1	protižidovský
názory	názor	k1gInPc4	názor
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
již	již	k9	již
za	za	k7c2	za
Německého	německý	k2eAgNnSc2d1	německé
císařství	císařství	k1gNnSc2	císařství
jednostranně	jednostranně	k6eAd1	jednostranně
posuzováno	posuzován	k2eAgNnSc1d1	posuzováno
a	a	k8xC	a
zejména	zejména	k9	zejména
v	v	k7c6	v
období	období	k1gNnSc6	období
nacismu	nacismus	k1gInSc2	nacismus
přímo	přímo	k6eAd1	přímo
zneužito	zneužít	k5eAaPmNgNnS	zneužít
<g/>
.	.	kIx.	.
</s>
<s>
Wagner	Wagner	k1gMnSc1	Wagner
bývá	bývat	k5eAaImIp3nS	bývat
(	(	kIx(	(
<g/>
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
logicky	logicky	k6eAd1	logicky
<g/>
)	)	kIx)	)
spojován	spojován	k2eAgMnSc1d1	spojován
s	s	k7c7	s
Adolfem	Adolf	k1gMnSc7	Adolf
Hitlerem	Hitler	k1gMnSc7	Hitler
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
Wagnerova	Wagnerův	k2eAgFnSc1d1	Wagnerova
snacha	snacha	k1gFnSc1	snacha
Winifred	Winifred	k1gInSc1	Winifred
Marjorie	Marjorie	k1gFnSc1	Marjorie
Wagner	Wagner	k1gMnSc1	Wagner
(	(	kIx(	(
<g/>
rozená	rozený	k2eAgFnSc1d1	rozená
Williamsová	Williamsová	k1gFnSc1	Williamsová
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
dlouholetou	dlouholetý	k2eAgFnSc7d1	dlouholetá
Hitlerovou	Hitlerův	k2eAgFnSc7d1	Hitlerova
osobní	osobní	k2eAgFnSc7d1	osobní
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1813	[number]	k4	1813
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
jako	jako	k8xC	jako
osmé	osmý	k4xOgNnSc4	osmý
dítě	dítě	k1gNnSc4	dítě
policejního	policejní	k2eAgMnSc2d1	policejní
úředníka	úředník	k1gMnSc2	úředník
Carla	Carl	k1gMnSc2	Carl
Friedricha	Friedrich	k1gMnSc2	Friedrich
Wagnera	Wagner	k1gMnSc2	Wagner
(	(	kIx(	(
<g/>
1770	[number]	k4	1770
<g/>
–	–	k?	–
<g/>
1813	[number]	k4	1813
<g/>
)	)	kIx)	)
a	a	k8xC	a
dcery	dcera	k1gFnPc4	dcera
pekaře	pekař	k1gMnSc2	pekař
Johanny	Johanna	k1gMnSc2	Johanna
Rosine	Rosin	k1gInSc5	Rosin
Wagnerové	Wagnerové	k2eAgInPc6d1	Wagnerové
<g/>
,	,	kIx,	,
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Pätzové	Pätzová	k1gFnPc1	Pätzová
(	(	kIx(	(
<g/>
1774	[number]	k4	1774
<g/>
–	–	k?	–
<g/>
1848	[number]	k4	1848
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Šest	šest	k4xCc1	šest
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
narození	narození	k1gNnSc6	narození
<g/>
,	,	kIx,	,
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1813	[number]	k4	1813
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
na	na	k7c4	na
tyfus	tyfus	k1gInSc4	tyfus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1814	[number]	k4	1814
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
znovu	znovu	k6eAd1	znovu
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c2	za
herce	herec	k1gMnSc2	herec
a	a	k8xC	a
básníka	básník	k1gMnSc2	básník
Ludwiga	Ludwig	k1gMnSc2	Ludwig
Geyera	Geyer	k1gMnSc2	Geyer
(	(	kIx(	(
<g/>
1780	[number]	k4	1780
<g/>
–	–	k?	–
<g/>
1821	[number]	k4	1821
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
celé	celý	k2eAgFnSc2d1	celá
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
kterého	který	k3yQgInSc2	který
si	se	k3xPyFc3	se
Wagner	Wagner	k1gMnSc1	Wagner
sám	sám	k3xTgMnSc1	sám
velice	velice	k6eAd1	velice
vážil	vážit	k5eAaImAgMnS	vážit
<g/>
.	.	kIx.	.
</s>
<s>
Johanna	Johanna	k6eAd1	Johanna
Rosine	Rosin	k1gInSc5	Rosin
Wagnerová	Wagnerová	k1gFnSc1	Wagnerová
měla	mít	k5eAaImAgFnS	mít
s	s	k7c7	s
Geyerem	Geyer	k1gInSc7	Geyer
ještě	ještě	k6eAd1	ještě
dceru	dcera	k1gFnSc4	dcera
Cäcilii	Cäcilie	k1gFnSc4	Cäcilie
(	(	kIx(	(
<g/>
1815	[number]	k4	1815
<g/>
–	–	k?	–
<g/>
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Objevily	objevit	k5eAaPmAgInP	objevit
se	se	k3xPyFc4	se
také	také	k9	také
spekulace	spekulace	k1gFnPc1	spekulace
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
Geyer	Geyer	k1gInSc1	Geyer
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
dokonce	dokonce	k9	dokonce
jeho	jeho	k3xOp3gMnSc7	jeho
biologickým	biologický	k2eAgMnSc7d1	biologický
otcem	otec	k1gMnSc7	otec
<g/>
,	,	kIx,	,
nebyly	být	k5eNaImAgFnP	být
ale	ale	k9	ale
nikdy	nikdy	k6eAd1	nikdy
potvrzeny	potvrdit	k5eAaPmNgInP	potvrdit
ani	ani	k8xC	ani
vyvráceny	vyvrátit	k5eAaPmNgInP	vyvrátit
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
domněnky	domněnka	k1gFnPc1	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
Geyer	Geyer	k1gInSc4	Geyer
byl	být	k5eAaImAgMnS	být
Žid	Žid	k1gMnSc1	Žid
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
vyvráceny	vyvrácen	k2eAgFnPc1d1	vyvrácena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1814	[number]	k4	1814
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Drážďan	Drážďany	k1gInPc2	Drážďany
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1817	[number]	k4	1817
byl	být	k5eAaImAgMnS	být
zapsán	zapsat	k5eAaPmNgMnS	zapsat
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
jeho	jeho	k3xOp3gMnSc1	jeho
otčím	otčím	k1gMnSc1	otčím
Ludwig	Ludwig	k1gMnSc1	Ludwig
Geyer	Geyer	k1gMnSc1	Geyer
onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
a	a	k8xC	a
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1821	[number]	k4	1821
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byl	být	k5eAaImAgMnS	být
Richard	Richard	k1gMnSc1	Richard
Wagner	Wagner	k1gMnSc1	Wagner
dán	dát	k5eAaPmNgMnS	dát
na	na	k7c4	na
výchovu	výchova	k1gFnSc4	výchova
strýci	strýc	k1gMnSc3	strýc
Karlu	Karel	k1gMnSc3	Karel
Geyerovi	Geyer	k1gMnSc3	Geyer
do	do	k7c2	do
Eislebenu	Eisleben	k2eAgFnSc4d1	Eisleben
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Richard	Richard	k1gMnSc1	Richard
Geyer	Geyer	k1gMnSc1	Geyer
církevní	církevní	k2eAgFnSc4d1	církevní
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Drážďanech	Drážďany	k1gInPc6	Drážďany
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1826	[number]	k4	1826
poprvé	poprvé	k6eAd1	poprvé
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Wagner	Wagner	k1gMnSc1	Wagner
Prahu	Praha	k1gFnSc4	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jeho	jeho	k3xOp3gFnPc1	jeho
sestry	sestra	k1gFnPc1	sestra
Rosalie	Rosalie	k1gFnSc1	Rosalie
a	a	k8xC	a
Klára	Klára	k1gFnSc1	Klára
získaly	získat	k5eAaPmAgFnP	získat
angažmá	angažmá	k1gNnSc4	angažmá
u	u	k7c2	u
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Prahu	Praha	k1gFnSc4	Praha
navštívil	navštívit	k5eAaPmAgMnS	navštívit
jako	jako	k8xS	jako
dítě	dítě	k1gNnSc4	dítě
ještě	ještě	k6eAd1	ještě
několikrát	několikrát	k6eAd1	několikrát
a	a	k8xC	a
velice	velice	k6eAd1	velice
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zalíbila	zalíbit	k5eAaPmAgFnS	zalíbit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
pamětech	paměť	k1gFnPc6	paměť
vzpomínal	vzpomínat	k5eAaImAgMnS	vzpomínat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
oblíbeného	oblíbený	k2eAgNnSc2d1	oblíbené
města	město	k1gNnSc2	město
šel	jít	k5eAaImAgMnS	jít
i	i	k9	i
pěšky	pěšky	k6eAd1	pěšky
<g/>
.	.	kIx.	.
</s>
<s>
Richard	Richard	k1gMnSc1	Richard
však	však	k9	však
zůstal	zůstat	k5eAaPmAgMnS	zůstat
v	v	k7c6	v
Drážďanech	Drážďany	k1gInPc6	Drážďany
a	a	k8xC	a
rodinu	rodina	k1gFnSc4	rodina
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Vánoc	Vánoce	k1gFnPc2	Vánoce
1827	[number]	k4	1827
byl	být	k5eAaImAgInS	být
opět	opět	k6eAd1	opět
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rodinou	rodina	k1gFnSc7	rodina
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
<g/>
.	.	kIx.	.
</s>
<s>
Tady	tady	k6eAd1	tady
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Wagner	Wagner	k1gMnSc1	Wagner
školu	škola	k1gFnSc4	škola
sv.	sv.	kA	sv.
Mikuláše	Mikuláš	k1gMnSc4	Mikuláš
a	a	k8xC	a
školu	škola	k1gFnSc4	škola
sv.	sv.	kA	sv.
Tomáše	Tomáš	k1gMnSc2	Tomáš
(	(	kIx(	(
<g/>
1828	[number]	k4	1828
<g/>
–	–	k?	–
<g/>
1830	[number]	k4	1830
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgInSc2	ten
času	čas	k1gInSc2	čas
byl	být	k5eAaImAgMnS	být
pod	pod	k7c7	pod
silným	silný	k2eAgInSc7d1	silný
vlivem	vliv	k1gInSc7	vliv
svého	svůj	k3xOyFgMnSc2	svůj
strýce	strýc	k1gMnSc2	strýc
Adolfa	Adolf	k1gMnSc2	Adolf
Wagnera	Wagner	k1gMnSc2	Wagner
<g/>
,	,	kIx,	,
četl	číst	k5eAaImAgMnS	číst
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
a	a	k8xC	a
"	"	kIx"	"
<g/>
romantiky	romantika	k1gFnSc2	romantika
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
E.	E.	kA	E.
T.	T.	kA	T.
A.	A.	kA	A.
Hoffmanna	Hoffmann	k1gMnSc2	Hoffmann
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šestnácti	šestnáct	k4xCc6	šestnáct
letech	let	k1gInPc6	let
slyšel	slyšet	k5eAaImAgMnS	slyšet
zpívat	zpívat	k5eAaImF	zpívat
operní	operní	k2eAgFnSc4d1	operní
pěvkyni	pěvkyně	k1gFnSc4	pěvkyně
Wilhelminu	Wilhelmin	k2eAgFnSc4d1	Wilhelmina
Schröder-Devrientovou	Schröder-Devrientový	k2eAgFnSc4d1	Schröder-Devrientový
v	v	k7c6	v
Beethovenově	Beethovenův	k2eAgFnSc6d1	Beethovenova
opeře	opera	k1gFnSc6	opera
Fidelio	Fidelio	k6eAd1	Fidelio
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
chvíle	chvíle	k1gFnSc2	chvíle
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
chce	chtít	k5eAaImIp3nS	chtít
být	být	k5eAaImF	být
muzikantem	muzikant	k1gMnSc7	muzikant
<g/>
.	.	kIx.	.
</s>
<s>
Složil	Složil	k1gMnSc1	Složil
první	první	k4xOgFnSc2	první
sonáty	sonáta	k1gFnSc2	sonáta
a	a	k8xC	a
první	první	k4xOgInSc4	první
nedokončený	dokončený	k2eNgInSc4d1	nedokončený
operní	operní	k2eAgInSc4d1	operní
pokus	pokus	k1gInSc4	pokus
Hochzeit	Hochzeit	k2eAgInSc4d1	Hochzeit
(	(	kIx(	(
<g/>
Svatba	svatba	k1gFnSc1	svatba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1831	[number]	k4	1831
studoval	studovat	k5eAaImAgMnS	studovat
hudbu	hudba	k1gFnSc4	hudba
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
se	se	k3xPyFc4	se
učil	učít	k5eAaPmAgMnS	učít
u	u	k7c2	u
Christiana	Christian	k1gMnSc2	Christian
Theodora	Theodor	k1gMnSc2	Theodor
Weinlinga	Weinling	k1gMnSc2	Weinling
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
také	také	k6eAd1	také
věnoval	věnovat	k5eAaPmAgMnS	věnovat
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
dílo	dílo	k1gNnSc4	dílo
–	–	k?	–
klavírní	klavírní	k2eAgFnSc4d1	klavírní
sonátu	sonáta	k1gFnSc4	sonáta
B	B	kA	B
dur	dur	k1gNnSc1	dur
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1832	[number]	k4	1832
zkomponoval	zkomponovat	k5eAaPmAgInS	zkomponovat
symfonii	symfonie	k1gFnSc4	symfonie
C	C	kA	C
dur	dur	k1gNnSc1	dur
a	a	k8xC	a
podnikl	podniknout	k5eAaPmAgMnS	podniknout
svou	svůj	k3xOyFgFnSc4	svůj
třetí	třetí	k4xOgFnSc4	třetí
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1833	[number]	k4	1833
byl	být	k5eAaImAgInS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
spisovatelem	spisovatel	k1gMnSc7	spisovatel
a	a	k8xC	a
publicistou	publicista	k1gMnSc7	publicista
Heinrichem	Heinrich	k1gMnSc7	Heinrich
Laubem	Laub	k1gMnSc7	Laub
a	a	k8xC	a
ideami	idea	k1gFnPc7	idea
revolučně	revolučně	k6eAd1	revolučně
orientovaným	orientovaný	k2eAgNnSc7d1	orientované
hnutím	hnutí	k1gNnSc7	hnutí
"	"	kIx"	"
<g/>
Junges	Junges	k1gInSc1	Junges
Deutschland	Deutschland	k1gInSc1	Deutschland
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
začal	začít	k5eAaPmAgInS	začít
komponovat	komponovat	k5eAaImF	komponovat
operu	opera	k1gFnSc4	opera
Die	Die	k1gFnSc2	Die
Feen	Feen	k1gInSc1	Feen
(	(	kIx(	(
<g/>
Víly	víla	k1gFnPc1	víla
<g/>
)	)	kIx)	)
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
sbormistrem	sbormistr	k1gMnSc7	sbormistr
divadla	divadlo	k1gNnSc2	divadlo
ve	v	k7c6	v
Würzburgu	Würzburg	k1gInSc6	Würzburg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Laubeho	Laube	k1gMnSc4	Laube
"	"	kIx"	"
<g/>
Zeitung	Zeitung	k1gMnSc1	Zeitung
für	für	k?	für
die	die	k?	die
eleganten	eleganten	k2eAgInSc4d1	eleganten
Welt	Welt	k1gInSc4	Welt
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Noviny	novina	k1gFnPc1	novina
pro	pro	k7c4	pro
elegantní	elegantní	k2eAgInSc4d1	elegantní
svět	svět	k1gInSc4	svět
<g/>
)	)	kIx)	)
mu	on	k3xPp3gMnSc3	on
vyšel	vyjít	k5eAaPmAgInS	vyjít
článek	článek	k1gInSc1	článek
"	"	kIx"	"
<g/>
Die	Die	k1gFnSc1	Die
deutsche	deutsch	k1gFnSc2	deutsch
Oper	opera	k1gFnPc2	opera
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Německá	německý	k2eAgFnSc1d1	německá
opera	opera	k1gFnSc1	opera
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
podnikl	podniknout	k5eAaPmAgMnS	podniknout
s	s	k7c7	s
Theodorem	Theodor	k1gMnSc7	Theodor
Apelem	apel	k1gInSc7	apel
svou	svůj	k3xOyFgFnSc4	svůj
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
hudebním	hudební	k2eAgMnSc7d1	hudební
vedoucím	vedoucí	k1gMnSc7	vedoucí
pro	pro	k7c4	pro
letní	letní	k2eAgFnSc4d1	letní
sezónu	sezóna	k1gFnSc4	sezóna
v	v	k7c6	v
Bad	Bad	k1gFnSc6	Bad
Lauchstädtu	Lauchstädt	k1gInSc2	Lauchstädt
a	a	k8xC	a
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Magdeburku	Magdeburk	k1gInSc6	Magdeburk
a	a	k8xC	a
tam	tam	k6eAd1	tam
také	také	k6eAd1	také
potkal	potkat	k5eAaPmAgInS	potkat
svou	svůj	k3xOyFgFnSc4	svůj
budoucí	budoucí	k2eAgFnSc4d1	budoucí
ženu	žena	k1gFnSc4	žena
<g/>
,	,	kIx,	,
herečku	herečka	k1gFnSc4	herečka
Minnu	Minen	k2eAgFnSc4d1	Minna
Planerovou	Planerová	k1gFnSc4	Planerová
(	(	kIx(	(
<g/>
*	*	kIx~	*
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1809	[number]	k4	1809
v	v	k7c6	v
Oederanu	Oederan	k1gInSc6	Oederan
<g/>
,	,	kIx,	,
†	†	k?	†
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1866	[number]	k4	1866
v	v	k7c6	v
Drážďanech	Drážďany	k1gInPc6	Drážďany
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1835	[number]	k4	1835
začal	začít	k5eAaPmAgMnS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
opeře	opera	k1gFnSc6	opera
Das	Das	k1gMnSc1	Das
Liebesverbot	Liebesverbot	k1gMnSc1	Liebesverbot
(	(	kIx(	(
<g/>
Zakázaná	zakázaný	k2eAgFnSc1d1	zakázaná
láska	láska	k1gFnSc1	láska
<g/>
)	)	kIx)	)
a	a	k8xC	a
zahájil	zahájit	k5eAaPmAgMnS	zahájit
druhou	druhý	k4xOgFnSc4	druhý
sezonu	sezona	k1gFnSc4	sezona
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
Magdeburku	Magdeburk	k1gInSc6	Magdeburk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
také	také	k9	také
29	[number]	k4	29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1836	[number]	k4	1836
konala	konat	k5eAaImAgFnS	konat
premiéra	premiéra	k1gFnSc1	premiéra
Zakázané	zakázaný	k2eAgFnSc2d1	zakázaná
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1836	[number]	k4	1836
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Minnou	Minný	k2eAgFnSc7d1	Minný
Planerovou	Planerová	k1gFnSc7	Planerová
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1837	[number]	k4	1837
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jako	jako	k9	jako
hudební	hudební	k2eAgMnSc1d1	hudební
ředitel	ředitel	k1gMnSc1	ředitel
v	v	k7c6	v
Královci	Královec	k1gInSc6	Královec
(	(	kIx(	(
<g/>
Königsberg	Königsberg	k1gInSc1	Königsberg
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Kalinigrad	Kalinigrad	k1gInSc1	Kalinigrad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
ale	ale	k8xC	ale
podnik	podnik	k1gInSc4	podnik
zkrachoval	zkrachovat	k5eAaPmAgMnS	zkrachovat
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
ocitl	ocitnout	k5eAaPmAgMnS	ocitnout
v	v	k7c6	v
dluzích	dluh	k1gInPc6	dluh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1837	[number]	k4	1837
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
místo	místo	k1gNnSc1	místo
dirigenta	dirigent	k1gMnSc2	dirigent
v	v	k7c6	v
Rize	Riga	k1gFnSc6	Riga
a	a	k8xC	a
dostal	dostat	k5eAaPmAgMnS	dostat
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
z	z	k7c2	z
dosahu	dosah	k1gInSc2	dosah
německých	německý	k2eAgMnPc2d1	německý
věřitelů	věřitel	k1gMnPc2	věřitel
<g/>
.	.	kIx.	.
</s>
<s>
Tady	tady	k6eAd1	tady
začal	začít	k5eAaPmAgInS	začít
vznikat	vznikat	k5eAaImF	vznikat
text	text	k1gInSc4	text
a	a	k8xC	a
začátek	začátek	k1gInSc4	začátek
partitury	partitura	k1gFnPc1	partitura
opery	opera	k1gFnSc2	opera
Rienzi	Rienze	k1gFnSc4	Rienze
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1837	[number]	k4	1837
zemřela	zemřít	k5eAaPmAgFnS	zemřít
jeho	jeho	k3xOp3gFnSc1	jeho
sestra	sestra	k1gFnSc1	sestra
Rosalie	Rosalie	k1gFnSc2	Rosalie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1839	[number]	k4	1839
ztratil	ztratit	k5eAaPmAgMnS	ztratit
své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Rize	Riga	k1gFnSc6	Riga
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
strachu	strach	k1gInSc2	strach
z	z	k7c2	z
věřitelů	věřitel	k1gMnPc2	věřitel
svých	svůj	k3xOyFgInPc2	svůj
velkých	velký	k2eAgInPc2d1	velký
dluhů	dluh	k1gInPc2	dluh
překročil	překročit	k5eAaPmAgMnS	překročit
tajně	tajně	k6eAd1	tajně
rusko-rakouskou	ruskoakouský	k2eAgFnSc4d1	rusko-rakouská
hranici	hranice	k1gFnSc4	hranice
a	a	k8xC	a
odplul	odplout	k5eAaPmAgMnS	odplout
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
Minnou	Minný	k2eAgFnSc7d1	Minný
na	na	k7c6	na
malé	malý	k2eAgFnSc6d1	malá
plachetnici	plachetnice	k1gFnSc6	plachetnice
na	na	k7c6	na
rozbouřeném	rozbouřený	k2eAgNnSc6d1	rozbouřené
moři	moře	k1gNnSc6	moře
až	až	k9	až
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
cestě	cesta	k1gFnSc6	cesta
načerpal	načerpat	k5eAaPmAgMnS	načerpat
inspiraci	inspirace	k1gFnSc4	inspirace
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
opeře	opera	k1gFnSc3	opera
Der	drát	k5eAaImRp2nS	drát
fliegende	fliegend	k1gInSc5	fliegend
Holländer	Holländer	k1gMnSc1	Holländer
(	(	kIx(	(
<g/>
Bludný	bludný	k2eAgMnSc1d1	bludný
Holanďan	Holanďan	k1gMnSc1	Holanďan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgMnS	vydat
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Léta	léto	k1gNnSc2	léto
1840	[number]	k4	1840
<g/>
–	–	k?	–
<g/>
1841	[number]	k4	1841
strávil	strávit	k5eAaPmAgInS	strávit
za	za	k7c2	za
stísněných	stísněný	k2eAgFnPc2d1	stísněná
podmínek	podmínka	k1gFnPc2	podmínka
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
dokončil	dokončit	k5eAaPmAgMnS	dokončit
zde	zde	k6eAd1	zde
Rienziho	Rienzi	k1gMnSc4	Rienzi
(	(	kIx(	(
<g/>
1840	[number]	k4	1840
<g/>
)	)	kIx)	)
a	a	k8xC	a
Bludného	bludný	k2eAgMnSc2d1	bludný
Holanďana	Holanďan	k1gMnSc2	Holanďan
(	(	kIx(	(
<g/>
1841	[number]	k4	1841
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
s	s	k7c7	s
Heinrichem	Heinrich	k1gMnSc7	Heinrich
Heinem	Hein	k1gMnSc7	Hein
a	a	k8xC	a
Franzem	Franz	k1gMnSc7	Franz
Lisztem	Liszt	k1gMnSc7	Liszt
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgInSc2	ten
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
též	též	k9	též
zabýval	zabývat	k5eAaImAgMnS	zabývat
ateistickou	ateistický	k2eAgFnSc7d1	ateistická
filosofií	filosofie	k1gFnSc7	filosofie
Ludwiga	Ludwig	k1gMnSc2	Ludwig
Feuerbacha	Feuerbach	k1gMnSc2	Feuerbach
a	a	k8xC	a
teorií	teorie	k1gFnSc7	teorie
moderního	moderní	k2eAgInSc2d1	moderní
anarchismu	anarchismus	k1gInSc2	anarchismus
Pierra	Pierr	k1gMnSc2	Pierr
Josepha	Joseph	k1gMnSc2	Joseph
Proudhona	Proudhon	k1gMnSc2	Proudhon
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
tak	tak	k6eAd1	tak
přišel	přijít	k5eAaPmAgMnS	přijít
na	na	k7c4	na
myšlenku	myšlenka	k1gFnSc4	myšlenka
zpracovat	zpracovat	k5eAaPmF	zpracovat
drama	drama	k1gNnSc4	drama
Nibelungů	Nibelung	k1gMnPc2	Nibelung
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1842	[number]	k4	1842
opustil	opustit	k5eAaPmAgMnS	opustit
Paříž	Paříž	k1gFnSc4	Paříž
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Drážďan	Drážďany	k1gInPc2	Drážďany
<g/>
.	.	kIx.	.
</s>
<s>
Červen	červen	k1gInSc1	červen
strávil	strávit	k5eAaPmAgInS	strávit
v	v	k7c6	v
Teplicích	Teplice	k1gFnPc6	Teplice
<g/>
,	,	kIx,	,
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Střekov	Střekov	k1gInSc1	Střekov
získal	získat	k5eAaPmAgInS	získat
první	první	k4xOgFnSc4	první
inspiraci	inspirace	k1gFnSc4	inspirace
k	k	k7c3	k
opeře	opera	k1gFnSc3	opera
Tannhäuser	Tannhäusra	k1gFnPc2	Tannhäusra
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1842	[number]	k4	1842
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
Drážďanech	Drážďany	k1gInPc6	Drážďany
premiéra	premiéra	k1gFnSc1	premiéra
opery	opera	k1gFnSc2	opera
Rienzi	Rienh	k1gMnPc1	Rienh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1843	[number]	k4	1843
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
kapelníkem	kapelník	k1gMnSc7	kapelník
Drážďanské	drážďanský	k2eAgFnSc2d1	Drážďanská
opery	opera	k1gFnSc2	opera
(	(	kIx(	(
<g/>
Semperoper	Semperoper	k1gInSc1	Semperoper
<g/>
)	)	kIx)	)
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
tam	tam	k6eAd1	tam
mělo	mít	k5eAaImAgNnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
jeho	jeho	k3xOp3gNnSc1	jeho
stěžejní	stěžejní	k2eAgNnSc1d1	stěžejní
dílo	dílo	k1gNnSc1	dílo
Bludný	bludný	k2eAgMnSc1d1	bludný
Holanďan	Holanďan	k1gMnSc1	Holanďan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Drážďanech	Drážďany	k1gInPc6	Drážďany
se	se	k3xPyFc4	se
poznal	poznat	k5eAaPmAgMnS	poznat
s	s	k7c7	s
Antonem	Anton	k1gMnSc7	Anton
Pusinellim	Pusinelli	k1gNnSc7	Pusinelli
a	a	k8xC	a
Augustem	August	k1gMnSc7	August
Röckelem	Röckel	k1gMnSc7	Röckel
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
vedl	vést	k5eAaImAgMnS	vést
především	především	k6eAd1	především
politické	politický	k2eAgInPc4d1	politický
rozhovory	rozhovor	k1gInPc4	rozhovor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1845	[number]	k4	1845
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
do	do	k7c2	do
Mariánských	mariánský	k2eAgFnPc2d1	Mariánská
Lázní	lázeň	k1gFnPc2	lázeň
a	a	k8xC	a
v	v	k7c6	v
tamějším	tamější	k2eAgNnSc6d1	tamější
klidném	klidný	k2eAgNnSc6d1	klidné
prostředí	prostředí	k1gNnSc6	prostředí
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
opeře	opera	k1gFnSc6	opera
Lohengrin	Lohengrin	k1gInSc1	Lohengrin
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1845	[number]	k4	1845
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
Drážďanech	Drážďany	k1gInPc6	Drážďany
premiéra	premiér	k1gMnSc2	premiér
Tannhäusera	Tannhäuser	k1gMnSc2	Tannhäuser
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1846	[number]	k4	1846
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
Beethovenovu	Beethovenův	k2eAgFnSc4d1	Beethovenova
devátou	devátý	k4xOgFnSc4	devátý
symfonii	symfonie	k1gFnSc4	symfonie
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1848	[number]	k4	1848
zemřela	zemřít	k5eAaPmAgFnS	zemřít
Wagnerova	Wagnerův	k2eAgFnSc1d1	Wagnerova
matka	matka	k1gFnSc1	matka
<g/>
.	.	kIx.	.
</s>
<s>
Navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
Liszta	Liszta	k1gMnSc1	Liszta
ve	v	k7c6	v
Výmaru	Výmar	k1gInSc6	Výmar
<g/>
,	,	kIx,	,
podpořil	podpořit	k5eAaPmAgInS	podpořit
březnovou	březnový	k2eAgFnSc4d1	březnová
revoluci	revoluce	k1gFnSc4	revoluce
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
koncepci	koncepce	k1gFnSc4	koncepce
pro	pro	k7c4	pro
Der	drát	k5eAaImRp2nS	drát
Ring	ring	k1gInSc1	ring
des	des	k1gNnSc6	des
Nibelungen	Nibelungen	k1gInSc1	Nibelungen
(	(	kIx(	(
<g/>
Prsten	prsten	k1gInSc1	prsten
Nibelungův	Nibelungův	k2eAgInSc1d1	Nibelungův
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
a	a	k8xC	a
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
květnového	květnový	k2eAgNnSc2d1	květnové
povstání	povstání	k1gNnSc2	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Poznal	poznat	k5eAaPmAgMnS	poznat
tehdy	tehdy	k6eAd1	tehdy
ruského	ruský	k2eAgMnSc4d1	ruský
anarchistu	anarchista	k1gMnSc4	anarchista
Michaila	Michail	k1gMnSc4	Michail
Bakunina	Bakunin	k2eAgFnSc1d1	Bakunina
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
vydán	vydán	k2eAgInSc1d1	vydán
zatykač	zatykač	k1gInSc1	zatykač
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
na	na	k7c4	na
jeho	jeho	k3xOp3gMnSc4	jeho
přítele	přítel	k1gMnSc4	přítel
<g/>
,	,	kIx,	,
známého	známý	k1gMnSc4	známý
architekta	architekt	k1gMnSc4	architekt
Gottfrieda	Gottfried	k1gMnSc4	Gottfried
Sempera	Semper	k1gMnSc4	Semper
<g/>
,	,	kIx,	,
autora	autor	k1gMnSc4	autor
operní	operní	k2eAgFnSc2d1	operní
budovy	budova	k1gFnSc2	budova
v	v	k7c6	v
Drážďanech	Drážďany	k1gInPc6	Drážďany
<g/>
.	.	kIx.	.
</s>
<s>
Musel	muset	k5eAaImAgMnS	muset
proto	proto	k8xC	proto
uprchnout	uprchnout	k5eAaPmF	uprchnout
a	a	k8xC	a
po	po	k7c6	po
krátkém	krátký	k2eAgInSc6d1	krátký
pobytu	pobyt	k1gInSc6	pobyt
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
se	se	k3xPyFc4	se
usadil	usadit	k5eAaPmAgMnS	usadit
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
v	v	k7c6	v
Curychu	Curych	k1gInSc6	Curych
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
francouzském	francouzský	k2eAgNnSc6d1	francouzské
Bordeaux	Bordeaux	k1gNnSc6	Bordeaux
prožil	prožít	k5eAaPmAgInS	prožít
románek	románek	k1gInSc1	románek
s	s	k7c7	s
Jessií	Jessie	k1gFnSc7	Jessie
Laussotovou	Laussotův	k2eAgFnSc7d1	Laussotův
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
měl	mít	k5eAaImAgMnS	mít
ve	v	k7c6	v
Weimaru	Weimar	k1gInSc6	Weimar
bez	bez	k7c2	bez
jeho	jeho	k3xOp3gFnSc2	jeho
přítomnosti	přítomnost	k1gFnSc2	přítomnost
premiéru	premiéra	k1gFnSc4	premiéra
Lohengrin	Lohengrin	k1gInSc1	Lohengrin
<g/>
.	.	kIx.	.
</s>
<s>
Dokončil	dokončit	k5eAaPmAgInS	dokončit
také	také	k9	také
spis	spis	k1gInSc1	spis
Oper	opera	k1gFnPc2	opera
und	und	k?	und
Drama	drama	k1gNnSc1	drama
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1852	[number]	k4	1852
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Ottou	Otta	k1gMnSc7	Otta
a	a	k8xC	a
Mathildou	Mathilda	k1gMnSc7	Mathilda
Wesendonckovými	Wesendoncková	k1gFnPc7	Wesendoncková
a	a	k8xC	a
dokončil	dokončit	k5eAaPmAgMnS	dokončit
básně	báseň	k1gFnPc4	báseň
k	k	k7c3	k
Nibelungům	Nibelung	k1gMnPc3	Nibelung
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
pořádal	pořádat	k5eAaImAgMnS	pořádat
koncerty	koncert	k1gInPc4	koncert
v	v	k7c6	v
Curychu	Curych	k1gInSc6	Curych
<g/>
.	.	kIx.	.
</s>
<s>
Navštívili	navštívit	k5eAaPmAgMnP	navštívit
ho	on	k3xPp3gNnSc4	on
tam	tam	k6eAd1	tam
Ferenc	Ferenc	k1gMnSc1	Ferenc
Liszt	Liszt	k1gMnSc1	Liszt
a	a	k8xC	a
básník-revolucionář	básníkevolucionář	k1gMnSc1	básník-revolucionář
Georg	Georg	k1gMnSc1	Georg
Herwegh	Herwegh	k1gMnSc1	Herwegh
<g/>
.	.	kIx.	.
</s>
<s>
Zkoncipoval	zkoncipovat	k5eAaPmAgMnS	zkoncipovat
předehru	předehra	k1gFnSc4	předehra
k	k	k7c3	k
opeře	opera	k1gFnSc3	opera
Das	Das	k1gFnSc1	Das
Rheingold	Rheingold	k1gMnSc1	Rheingold
(	(	kIx(	(
<g/>
Zlato	zlato	k1gNnSc1	zlato
Rýna	Rýn	k1gInSc2	Rýn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1852	[number]	k4	1852
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Liszta	Liszta	k1gMnSc1	Liszta
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
nemanželskou	manželský	k2eNgFnSc7d1	nemanželská
dcerou	dcera	k1gFnSc7	dcera
Cosimou	Cosima	k1gFnSc7	Cosima
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
Cosima	Cosima	k1gFnSc1	Cosima
de	de	k?	de
Flavigny	Flavigna	k1gFnSc2	Flavigna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
o	o	k7c4	o
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
jeho	jeho	k3xOp3gFnSc7	jeho
druhou	druhý	k4xOgFnSc7	druhý
ženou	žena	k1gFnSc7	žena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1854	[number]	k4	1854
si	se	k3xPyFc3	se
přečetl	přečíst	k5eAaPmAgMnS	přečíst
hlavní	hlavní	k2eAgNnSc4d1	hlavní
dílo	dílo	k1gNnSc4	dílo
německého	německý	k2eAgMnSc2d1	německý
filosofa	filosof	k1gMnSc2	filosof
Arthura	Arthur	k1gMnSc2	Arthur
Schopenhauera	Schopenhauer	k1gMnSc2	Schopenhauer
Die	Die	k1gMnSc1	Die
Welt	Welt	k1gMnSc1	Welt
als	als	k?	als
Wille	Wille	k1gInSc1	Wille
und	und	k?	und
Vorstellung	Vorstellung	k1gInSc1	Vorstellung
(	(	kIx(	(
<g/>
Svět	svět	k1gInSc1	svět
jako	jako	k8xC	jako
vůle	vůle	k1gFnSc1	vůle
a	a	k8xC	a
představa	představa	k1gFnSc1	představa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
opeře	opera	k1gFnSc6	opera
Tristan	Tristan	k1gInSc1	Tristan
a	a	k8xC	a
Isolda	Isolda	k1gFnSc1	Isolda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1855	[number]	k4	1855
pořádal	pořádat	k5eAaImAgMnS	pořádat
koncerty	koncert	k1gInPc7	koncert
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1856	[number]	k4	1856
požádal	požádat	k5eAaPmAgMnS	požádat
saského	saský	k2eAgMnSc4d1	saský
krále	král	k1gMnSc4	král
Jana	Jan	k1gMnSc2	Jan
o	o	k7c4	o
milost	milost	k1gFnSc4	milost
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
příležitostně	příležitostně	k6eAd1	příležitostně
pobýval	pobývat	k5eAaImAgMnS	pobývat
na	na	k7c6	na
"	"	kIx"	"
<g/>
Zeleném	zelený	k2eAgInSc6d1	zelený
vršku	vršek	k1gInSc6	vršek
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Grüner	Grüner	k1gMnSc1	Grüner
Hügel	Hügel	k1gMnSc1	Hügel
<g/>
)	)	kIx)	)
vedle	vedle	k7c2	vedle
vily	vila	k1gFnSc2	vila
Wesedoncků	Wesedonck	k1gInPc2	Wesedonck
a	a	k8xC	a
zkomponoval	zkomponovat	k5eAaPmAgMnS	zkomponovat
zde	zde	k6eAd1	zde
známé	známý	k2eAgInPc4d1	známý
Wesendonck-Lieder	Wesendonck-Lieder	k1gInSc4	Wesendonck-Lieder
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Písně	píseň	k1gFnPc1	píseň
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
Mathildu	Mathilda	k1gMnSc4	Mathilda
<g/>
)	)	kIx)	)
Wesendonck	Wesendonck	k1gMnSc1	Wesendonck
<g/>
.	.	kIx.	.
</s>
<s>
Přerušil	přerušit	k5eAaPmAgInS	přerušit
kompoziční	kompoziční	k2eAgFnSc4d1	kompoziční
práci	práce	k1gFnSc4	práce
na	na	k7c6	na
operách	opera	k1gFnPc6	opera
cyklu	cyklus	k1gInSc2	cyklus
Prsten	prsten	k1gInSc4	prsten
Nibelungův	Nibelungův	k2eAgInSc4d1	Nibelungův
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
k	k	k7c3	k
práci	práce	k1gFnSc3	práce
na	na	k7c6	na
Tristanovi	Tristan	k1gMnSc6	Tristan
a	a	k8xC	a
Isoldě	Isolda	k1gFnSc6	Isolda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1858	[number]	k4	1858
si	se	k3xPyFc3	se
zkomplikoval	zkomplikovat	k5eAaPmAgMnS	zkomplikovat
situaci	situace	k1gFnSc4	situace
románkem	románek	k1gInSc7	románek
s	s	k7c7	s
Mathildou	Mathilda	k1gFnSc7	Mathilda
Wesendonckovou	Wesendoncková	k1gFnSc7	Wesendoncková
<g/>
.	.	kIx.	.
</s>
<s>
Manželka	manželka	k1gFnSc1	manželka
Minna	Minna	k1gFnSc1	Minna
odjela	odjet	k5eAaPmAgFnS	odjet
do	do	k7c2	do
Drážďan	Drážďany	k1gInPc2	Drážďany
a	a	k8xC	a
na	na	k7c4	na
čas	čas	k1gInSc4	čas
se	se	k3xPyFc4	se
od	od	k7c2	od
něj	on	k3xPp3gNnSc2	on
odloučila	odloučit	k5eAaPmAgFnS	odloučit
<g/>
.	.	kIx.	.
</s>
<s>
Wagner	Wagner	k1gMnSc1	Wagner
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Benátek	Benátky	k1gFnPc2	Benátky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1859	[number]	k4	1859
se	se	k3xPyFc4	se
zdržoval	zdržovat	k5eAaImAgInS	zdržovat
v	v	k7c4	v
Luzernu	Luzerna	k1gFnSc4	Luzerna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dokončil	dokončit	k5eAaPmAgMnS	dokončit
Tristana	Tristan	k1gMnSc4	Tristan
<g/>
.	.	kIx.	.
</s>
<s>
Vydal	vydat	k5eAaPmAgInS	vydat
se	se	k3xPyFc4	se
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
ho	on	k3xPp3gNnSc4	on
následovala	následovat	k5eAaImAgFnS	následovat
i	i	k9	i
Minna	Minen	k2eAgFnSc1d1	Minna
<g/>
.	.	kIx.	.
</s>
<s>
Pořádal	pořádat	k5eAaImAgMnS	pořádat
tam	tam	k6eAd1	tam
koncerty	koncert	k1gInPc4	koncert
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
vydal	vydat	k5eAaPmAgMnS	vydat
saský	saský	k2eAgMnSc1d1	saský
král	král	k1gMnSc1	král
Jan	Jan	k1gMnSc1	Jan
částečnou	částečný	k2eAgFnSc4d1	částečná
amnestii	amnestie	k1gFnSc4	amnestie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
vztahovala	vztahovat	k5eAaImAgFnS	vztahovat
i	i	k9	i
na	na	k7c4	na
Richarda	Richard	k1gMnSc4	Richard
Wagnera	Wagner	k1gMnSc4	Wagner
<g/>
,	,	kIx,	,
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgMnS	moct
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
době	doba	k1gFnSc6	doba
zase	zase	k9	zase
podívat	podívat	k5eAaImF	podívat
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1861	[number]	k4	1861
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
skandální	skandální	k2eAgNnSc4d1	skandální
uvedení	uvedení	k1gNnSc4	uvedení
opery	opera	k1gFnSc2	opera
Tannhäuser	Tannhäusra	k1gFnPc2	Tannhäusra
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
Wagner	Wagner	k1gMnSc1	Wagner
zdržoval	zdržovat	k5eAaImAgMnS	zdržovat
střídavě	střídavě	k6eAd1	střídavě
v	v	k7c6	v
Karlsruhe	Karlsruhe	k1gFnSc6	Karlsruhe
<g/>
,	,	kIx,	,
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
Benátkách	Benátky	k1gFnPc6	Benátky
<g/>
,	,	kIx,	,
Mohuči	Mohuč	k1gFnSc3	Mohuč
a	a	k8xC	a
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
opeře	opera	k1gFnSc6	opera
Mistři	mistr	k1gMnPc1	mistr
pěvci	pěvec	k1gMnPc1	pěvec
norimberští	norimberský	k2eAgMnPc1d1	norimberský
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
opustil	opustit	k5eAaPmAgMnS	opustit
Paříž	Paříž	k1gFnSc4	Paříž
<g/>
,	,	kIx,	,
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
Biebrichu	Biebrich	k1gInSc6	Biebrich
<g/>
,	,	kIx,	,
dnešním	dnešní	k2eAgNnSc6d1	dnešní
předměstí	předměstí	k1gNnSc6	předměstí
hessenského	hessenský	k2eAgNnSc2d1	hessenský
města	město	k1gNnSc2	město
Wiesbaden	Wiesbaden	k1gInSc1	Wiesbaden
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Biebrichu	Biebrich	k1gInSc2	Biebrich
přijela	přijet	k5eAaPmAgFnS	přijet
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
jeho	on	k3xPp3gMnSc2	on
manželka	manželka	k1gFnSc1	manželka
Minna	Minno	k1gNnSc2	Minno
a	a	k8xC	a
zde	zde	k6eAd1	zde
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
došlo	dojít	k5eAaPmAgNnS	dojít
začátkem	začátek	k1gInSc7	začátek
roku	rok	k1gInSc2	rok
1862	[number]	k4	1862
k	k	k7c3	k
definitivní	definitivní	k2eAgFnSc3d1	definitivní
rozluce	rozluka	k1gFnSc3	rozluka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
saský	saský	k2eAgMnSc1d1	saský
král	král	k1gMnSc1	král
plnou	plný	k2eAgFnSc4d1	plná
amnestii	amnestie	k1gFnSc4	amnestie
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
prožil	prožít	k5eAaPmAgMnS	prožít
Wagner	Wagner	k1gMnSc1	Wagner
vztahy	vztah	k1gInPc1	vztah
k	k	k7c3	k
Mathildě	Mathilda	k1gFnSc3	Mathilda
Maierové	Maierová	k1gFnSc2	Maierová
a	a	k8xC	a
Friederike	Friederik	k1gFnSc2	Friederik
Meyerové	Meyerová	k1gFnSc2	Meyerová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
vynikajícím	vynikající	k2eAgMnSc7d1	vynikající
klavíristou	klavírista	k1gMnSc7	klavírista
a	a	k8xC	a
také	také	k6eAd1	také
dirigentem	dirigent	k1gMnSc7	dirigent
Hansem	Hans	k1gMnSc7	Hans
von	von	k1gInSc4	von
Bülow	Bülow	k1gFnSc2	Bülow
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
mezitím	mezitím	k6eAd1	mezitím
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Lisztovou	Lisztový	k2eAgFnSc7d1	Lisztový
dcerou	dcera	k1gFnSc7	dcera
Cosimou	Cosimý	k2eAgFnSc7d1	Cosimý
<g/>
,	,	kIx,	,
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgMnS	zůstat
na	na	k7c4	na
čas	čas	k1gInSc4	čas
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1863	[number]	k4	1863
pořádal	pořádat	k5eAaImAgMnS	pořádat
koncerty	koncert	k1gInPc7	koncert
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
<g/>
,	,	kIx,	,
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
,	,	kIx,	,
Budapešti	Budapešť	k1gFnSc6	Budapešť
<g/>
,	,	kIx,	,
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
Karlsruhe	Karlsruhe	k1gFnSc6	Karlsruhe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1864	[number]	k4	1864
opustil	opustit	k5eAaPmAgMnS	opustit
Vídeň	Vídeň	k1gFnSc4	Vídeň
a	a	k8xC	a
navštívil	navštívit	k5eAaPmAgMnS	navštívit
německou	německý	k2eAgFnSc4d1	německá
spisovatelku	spisovatelka	k1gFnSc4	spisovatelka
Elisu	Elis	k1gInSc2	Elis
Wille	Wille	k1gFnSc2	Wille
v	v	k7c6	v
Mariafeldu	Mariafeld	k1gInSc6	Mariafeld
u	u	k7c2	u
Curychu	Curych	k1gInSc2	Curych
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1863	[number]	k4	1863
ho	on	k3xPp3gInSc4	on
pozval	pozvat	k5eAaPmAgMnS	pozvat
do	do	k7c2	do
Mnichova	Mnichov	k1gInSc2	Mnichov
jeho	jeho	k3xOp3gFnSc4	jeho
veliký	veliký	k2eAgMnSc1d1	veliký
obdivovatel	obdivovatel	k1gMnSc1	obdivovatel
<g/>
,	,	kIx,	,
bavorský	bavorský	k2eAgMnSc1d1	bavorský
"	"	kIx"	"
<g/>
pohádkový	pohádkový	k2eAgMnSc1d1	pohádkový
<g/>
"	"	kIx"	"
král	král	k1gMnSc1	král
Ludvík	Ludvík	k1gMnSc1	Ludvík
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
a	a	k8xC	a
i	i	k9	i
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
okolností	okolnost	k1gFnPc2	okolnost
nucen	nutit	k5eAaImNgMnS	nutit
Wagnera	Wagner	k1gMnSc4	Wagner
vyhostit	vyhostit	k5eAaPmF	vyhostit
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
ho	on	k3xPp3gMnSc4	on
až	až	k9	až
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
štědře	štědro	k6eAd1	štědro
podporoval	podporovat	k5eAaImAgInS	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Wagnerovo	Wagnerův	k2eAgNnSc1d1	Wagnerovo
opětné	opětný	k2eAgNnSc1d1	opětné
setkání	setkání	k1gNnSc1	setkání
s	s	k7c7	s
Cosimou	Cosima	k1gFnSc7	Cosima
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
provdanou	provdaný	k2eAgFnSc4d1	provdaná
von	von	k1gInSc1	von
Bülow	Bülow	k1gFnSc3	Bülow
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
oba	dva	k4xCgInPc4	dva
osudové	osudový	k2eAgInPc4d1	osudový
<g/>
.	.	kIx.	.
</s>
<s>
Cosima	Cosima	k1gFnSc1	Cosima
byla	být	k5eAaImAgFnS	být
nemanželskou	manželský	k2eNgFnSc7d1	nemanželská
dcerou	dcera	k1gFnSc7	dcera
proslulého	proslulý	k2eAgInSc2d1	proslulý
klavírního	klavírní	k2eAgInSc2d1	klavírní
virtuóza	virtuóza	k1gFnSc1	virtuóza
a	a	k8xC	a
hudebního	hudební	k2eAgMnSc2d1	hudební
skladatele	skladatel	k1gMnSc2	skladatel
Franze	Franze	k1gFnSc2	Franze
Liszta	Liszt	k1gMnSc2	Liszt
a	a	k8xC	a
francouzské	francouzský	k2eAgFnSc2d1	francouzská
hraběnky	hraběnka	k1gFnSc2	hraběnka
Marie	Maria	k1gFnSc2	Maria
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Agoult	Agoult	k1gMnSc1	Agoult
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
původu	původ	k1gInSc2	původ
své	svůj	k3xOyFgFnSc2	svůj
matky	matka	k1gFnSc2	matka
z	z	k7c2	z
vysoké	vysoký	k2eAgFnSc2d1	vysoká
šlechty	šlechta	k1gFnSc2	šlechta
se	se	k3xPyFc4	se
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
Cosima	Cosima	k1gFnSc1	Cosima
de	de	k?	de
Flavigny	Flavigna	k1gFnSc2	Flavigna
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc6	listopad
1863	[number]	k4	1863
si	se	k3xPyFc3	se
Wagner	Wagner	k1gMnSc1	Wagner
a	a	k8xC	a
Cosima	Cosima	k1gFnSc1	Cosima
von	von	k1gInSc4	von
Bülow	Bülow	k1gFnSc2	Bülow
vyznali	vyznat	k5eAaPmAgMnP	vyznat
lásku	láska	k1gFnSc4	láska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
a	a	k8xC	a
červenci	červenec	k1gInSc6	červenec
1864	[number]	k4	1864
bydleli	bydlet	k5eAaImAgMnP	bydlet
Bülowovi	Bülowův	k2eAgMnPc1d1	Bülowův
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
vile	vila	k1gFnSc6	vila
Pellet	Pellet	k1gInSc4	Pellet
na	na	k7c6	na
Starnberském	Starnberský	k2eAgNnSc6d1	Starnberský
jezeře	jezero	k1gNnSc6	jezero
v	v	k7c6	v
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
Wagnerův	Wagnerův	k2eAgInSc1d1	Wagnerův
utajovaný	utajovaný	k2eAgInSc1d1	utajovaný
vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
Cosimou	Cosima	k1gFnSc7	Cosima
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1865	[number]	k4	1865
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
Isolda	Isolda	k1gFnSc1	Isolda
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
Hans	Hans	k1gMnSc1	Hans
von	von	k1gInSc4	von
Bülow	Bülow	k1gFnSc2	Bülow
tehdy	tehdy	k6eAd1	tehdy
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
vlastní	vlastní	k2eAgNnSc4d1	vlastní
dítě	dítě	k1gNnSc4	dítě
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1865	[number]	k4	1865
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
slavnostní	slavnostní	k2eAgFnSc1d1	slavnostní
premiéra	premiéra	k1gFnSc1	premiéra
Tristana	Tristana	k1gFnSc1	Tristana
a	a	k8xC	a
Isoldy	Isold	k1gInPc1	Isold
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
II	II	kA	II
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
začal	začít	k5eAaPmAgMnS	začít
Wagner	Wagner	k1gMnSc1	Wagner
psát	psát	k5eAaImF	psát
svou	svůj	k3xOyFgFnSc4	svůj
autobiografii	autobiografie	k1gFnSc4	autobiografie
Mein	Mein	k1gNnSc4	Mein
Leben	Leben	k1gInSc1	Leben
(	(	kIx(	(
<g/>
Můj	můj	k3xOp1gInSc1	můj
život	život	k1gInSc1	život
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1866	[number]	k4	1866
zemřela	zemřít	k5eAaPmAgFnS	zemřít
Wagnerova	Wagnerův	k2eAgFnSc1d1	Wagnerova
první	první	k4xOgFnSc1	první
manželka	manželka	k1gFnSc1	manželka
Minna	Minn	k1gInSc2	Minn
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Planer	Planer	k1gMnSc1	Planer
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgInSc2	ten
času	čas	k1gInSc2	čas
pobýval	pobývat	k5eAaImAgMnS	pobývat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Cosimou	Cosima	k1gFnSc7	Cosima
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
usadili	usadit	k5eAaPmAgMnP	usadit
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Tribschen	Tribschna	k1gFnPc2	Tribschna
blízko	blízko	k6eAd1	blízko
Luzernu	Luzerna	k1gFnSc4	Luzerna
<g/>
,	,	kIx,	,
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
romantického	romantický	k2eAgNnSc2d1	romantické
Lucernského	Lucernský	k2eAgNnSc2d1	Lucernský
jezera	jezero	k1gNnSc2	jezero
(	(	kIx(	(
<g/>
Vierwaldstätter	Vierwaldstätter	k1gMnSc1	Vierwaldstätter
See	See	k1gMnSc1	See
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
tam	tam	k6eAd1	tam
narodila	narodit	k5eAaPmAgFnS	narodit
druhá	druhý	k4xOgFnSc1	druhý
dcera	dcera	k1gFnSc1	dcera
Eva	Eva	k1gFnSc1	Eva
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1868	[number]	k4	1868
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
premiéra	premiéra	k1gFnSc1	premiéra
opery	opera	k1gFnSc2	opera
Mistři	mistr	k1gMnPc1	mistr
pěvci	pěvec	k1gMnSc6	pěvec
<g />
.	.	kIx.	.
</s>
<s>
norimberští	norimberský	k2eAgMnPc1d1	norimberský
(	(	kIx(	(
<g/>
Die	Die	k1gMnSc1	Die
Meistersinger	Meistersingra	k1gFnPc2	Meistersingra
von	von	k1gInSc1	von
Nürnberg	Nürnberg	k1gMnSc1	Nürnberg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
se	se	k3xPyFc4	se
Wagner	Wagner	k1gMnSc1	Wagner
poprvé	poprvé	k6eAd1	poprvé
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Friedrichem	Friedrich	k1gMnSc7	Friedrich
Nietzschem	Nietzsch	k1gMnSc7	Nietzsch
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
proslavil	proslavit	k5eAaPmAgMnS	proslavit
jako	jako	k8xC	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
filosofů	filosof	k1gMnPc2	filosof
a	a	k8xC	a
myslitelů	myslitel	k1gMnPc2	myslitel
a	a	k8xC	a
kterého	který	k3yQgMnSc4	který
Wagner	Wagner	k1gMnSc1	Wagner
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
k	k	k7c3	k
napsání	napsání	k1gNnSc3	napsání
jeho	on	k3xPp3gNnSc2	on
díla	dílo	k1gNnSc2	dílo
Die	Die	k1gFnSc4	Die
Geburt	Geburta	k1gFnPc2	Geburta
der	drát	k5eAaImRp2nS	drát
Tragödie	Tragödie	k1gFnSc1	Tragödie
(	(	kIx(	(
<g/>
Zrození	zrození	k1gNnSc1	zrození
tragédie	tragédie	k1gFnSc2	tragédie
<g/>
)	)	kIx)	)
a	a	k8xC	a
několika	několik	k4yIc2	několik
dalších	další	k2eAgNnPc2d1	další
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Nietzsche	Nietzsche	k6eAd1	Nietzsche
byl	být	k5eAaImAgMnS	být
toho	ten	k3xDgInSc2	ten
času	čas	k1gInSc2	čas
častým	častý	k2eAgMnSc7d1	častý
hostem	host	k1gMnSc7	host
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
vile	vila	k1gFnSc6	vila
a	a	k8xC	a
Wagner	Wagner	k1gMnSc1	Wagner
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
počítal	počítat	k5eAaImAgInS	počítat
jako	jako	k8xS	jako
s	s	k7c7	s
osobním	osobní	k2eAgMnSc7d1	osobní
učitelem	učitel	k1gMnSc7	učitel
svého	svůj	k1gMnSc2	svůj
syna	syn	k1gMnSc2	syn
Siegfrieda	Siegfried	k1gMnSc2	Siegfried
Wagnera	Wagner	k1gMnSc2	Wagner
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
jemu	on	k3xPp3gMnSc3	on
a	a	k8xC	a
Cosimě	Cosima	k1gFnSc6	Cosima
narodil	narodit	k5eAaPmAgInS	narodit
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1869	[number]	k4	1869
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
tři	tři	k4xCgFnPc4	tři
děti	dítě	k1gFnPc4	dítě
Richarda	Richard	k1gMnSc2	Richard
Wagnera	Wagner	k1gMnSc2	Wagner
a	a	k8xC	a
Cosimy	Cosima	k1gFnSc2	Cosima
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
všechny	všechen	k3xTgFnPc1	všechen
narodily	narodit	k5eAaPmAgFnP	narodit
jako	jako	k9	jako
nemanželské	manželský	k2eNgFnPc1d1	nemanželská
<g/>
.	.	kIx.	.
</s>
<s>
Manželství	manželství	k1gNnSc1	manželství
Cosimy	Cosima	k1gFnSc2	Cosima
s	s	k7c7	s
Hansem	Hans	k1gMnSc7	Hans
von	von	k1gInSc4	von
Bülow	Bülow	k1gFnSc2	Bülow
bylo	být	k5eAaImAgNnS	být
rozvedeno	rozvést	k5eAaPmNgNnS	rozvést
teprve	teprve	k6eAd1	teprve
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1870	[number]	k4	1870
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1870	[number]	k4	1870
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
Richard	Richard	k1gMnSc1	Richard
Wagner	Wagner	k1gMnSc1	Wagner
a	a	k8xC	a
Cosima	Cosima	k1gFnSc1	Cosima
v	v	k7c4	v
Luzernu	Luzerna	k1gFnSc4	Luzerna
manželství	manželství	k1gNnSc2	manželství
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1869	[number]	k4	1869
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
premiéra	premiéra	k1gFnSc1	premiéra
Wagnerovy	Wagnerův	k2eAgFnSc2d1	Wagnerova
opery	opera	k1gFnSc2	opera
Zlato	zlato	k1gNnSc1	zlato
Rýna	Rýn	k1gInSc2	Rýn
(	(	kIx(	(
<g/>
Rheingold	Rheingold	k1gInSc1	Rheingold
<g/>
)	)	kIx)	)
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1870	[number]	k4	1870
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
premiéru	premiéra	k1gFnSc4	premiéra
opera	opera	k1gFnSc1	opera
Walküre	Walkür	k1gInSc5	Walkür
(	(	kIx(	(
<g/>
Valkýra	valkýra	k1gFnSc1	valkýra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1870	[number]	k4	1870
uspořádal	uspořádat	k5eAaPmAgMnS	uspořádat
Wagner	Wagner	k1gMnSc1	Wagner
soukromou	soukromý	k2eAgFnSc4d1	soukromá
premiéru	premiéra	k1gFnSc4	premiéra
svého	svůj	k3xOyFgNnSc2	svůj
díla	dílo	k1gNnSc2	dílo
"	"	kIx"	"
<g/>
Siegfried-Idylle	Siegfried-Idylle	k1gNnSc2	Siegfried-Idylle
<g/>
"	"	kIx"	"
na	na	k7c6	na
schodech	schod	k1gInPc6	schod
své	svůj	k3xOyFgFnSc2	svůj
vily	vila	k1gFnSc2	vila
v	v	k7c4	v
Luzernu	Luzerna	k1gFnSc4	Luzerna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1871	[number]	k4	1871
si	se	k3xPyFc3	se
Wagner	Wagner	k1gMnSc1	Wagner
vybral	vybrat	k5eAaPmAgMnS	vybrat
Bayreuth	Bayreuth	k1gInSc4	Bayreuth
jako	jako	k8xC	jako
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
II	II	kA	II
<g/>
.	.	kIx.	.
mohl	moct	k5eAaImAgMnS	moct
tento	tento	k3xDgInSc4	tento
projekt	projekt	k1gInSc4	projekt
realizovat	realizovat	k5eAaBmF	realizovat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgMnS	být
také	také	k6eAd1	také
pozván	pozvat	k5eAaPmNgMnS	pozvat
německým	německý	k2eAgMnSc7d1	německý
říšským	říšský	k2eAgMnSc7d1	říšský
kancléřem	kancléř	k1gMnSc7	kancléř
Bismarckem	Bismarck	k1gInSc7	Bismarck
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1872	[number]	k4	1872
se	se	k3xPyFc4	se
definitivně	definitivně	k6eAd1	definitivně
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
do	do	k7c2	do
Bayreuthu	Bayreuth	k1gInSc2	Bayreuth
a	a	k8xC	a
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
zde	zde	k6eAd1	zde
položen	položit	k5eAaPmNgInS	položit
základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
jeho	on	k3xPp3gNnSc2	on
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
neviditelný	viditelný	k2eNgInSc1d1	neviditelný
orchestr	orchestr	k1gInSc1	orchestr
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zůstal	zůstat	k5eAaPmAgInS	zůstat
skrytý	skrytý	k2eAgInSc1d1	skrytý
pod	pod	k7c7	pod
podiem	podium	k1gNnSc7	podium
<g/>
,	,	kIx,	,
a	a	k8xC	a
diváci	divák	k1gMnPc1	divák
tak	tak	k6eAd1	tak
měli	mít	k5eAaImAgMnP	mít
možnost	možnost	k1gFnSc4	možnost
vidět	vidět	k5eAaImF	vidět
jen	jen	k9	jen
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
na	na	k7c6	na
jevišti	jeviště	k1gNnSc6	jeviště
<g/>
.	.	kIx.	.
</s>
<s>
Veliký	veliký	k2eAgInSc1d1	veliký
důraz	důraz	k1gInSc1	důraz
byl	být	k5eAaImAgInS	být
též	též	k6eAd1	též
kladen	klást	k5eAaImNgInS	klást
na	na	k7c4	na
kvalitu	kvalita	k1gFnSc4	kvalita
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Mimořádné	mimořádný	k2eAgFnPc1d1	mimořádná
akustiky	akustika	k1gFnPc1	akustika
bylo	být	k5eAaImAgNnS	být
dosažena	dosáhnout	k5eAaPmNgFnS	dosáhnout
i	i	k9	i
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
celá	celý	k2eAgFnSc1d1	celá
stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebyly	být	k5eNaImAgInP	být
žádné	žádný	k3yNgFnPc4	žádný
lóže	lóže	k1gFnPc4	lóže
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
a	a	k8xC	a
všechny	všechen	k3xTgFnPc1	všechen
sedačky	sedačka	k1gFnPc1	sedačka
byly	být	k5eAaImAgFnP	být
bez	bez	k7c2	bez
čalounění	čalounění	k1gNnSc2	čalounění
<g/>
.	.	kIx.	.
</s>
<s>
Tenhle	tenhle	k3xDgInSc1	tenhle
nápad	nápad	k1gInSc1	nápad
dostal	dostat	k5eAaPmAgInS	dostat
už	už	k6eAd1	už
za	za	k7c2	za
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
Rize	Riga	k1gFnSc6	Riga
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
musel	muset	k5eAaImAgInS	muset
někdy	někdy	k6eAd1	někdy
dirigovat	dirigovat	k5eAaImF	dirigovat
koncerty	koncert	k1gInPc4	koncert
i	i	k9	i
ve	v	k7c6	v
stodolách	stodola	k1gFnPc6	stodola
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
překvapen	překvapit	k5eAaPmNgMnS	překvapit
a	a	k8xC	a
nadšen	nadšen	k2eAgMnSc1d1	nadšen
jejich	jejich	k3xOp3gFnSc7	jejich
akustikou	akustika	k1gFnSc7	akustika
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1873	[number]	k4	1873
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
otevření	otevření	k1gNnSc1	otevření
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
se	se	k3xPyFc4	se
nastěhovali	nastěhovat	k5eAaPmAgMnP	nastěhovat
Richard	Richard	k1gMnSc1	Richard
a	a	k8xC	a
Cosima	Cosima	k1gFnSc1	Cosima
Wagnerovi	Wagnerův	k2eAgMnPc1d1	Wagnerův
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
vily	vila	k1gFnSc2	vila
v	v	k7c6	v
Bayreuthu	Bayreuth	k1gInSc6	Bayreuth
<g/>
,	,	kIx,	,
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
zámecké	zámecký	k2eAgFnSc2d1	zámecká
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
stavěla	stavět	k5eAaImAgFnS	stavět
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Wagner	Wagner	k1gMnSc1	Wagner
ji	on	k3xPp3gFnSc4	on
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
"	"	kIx"	"
<g/>
Haus	Haus	k1gInSc1	Haus
Wahnfried	Wahnfried	k1gInSc1	Wahnfried
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
domu	dům	k1gInSc2	dům
osvětluje	osvětlovat	k5eAaImIp3nS	osvětlovat
nápis	nápis	k1gInSc1	nápis
vyrytý	vyrytý	k2eAgInSc1d1	vyrytý
na	na	k7c6	na
čelní	čelní	k2eAgFnSc6d1	čelní
straně	strana	k1gFnSc6	strana
budovy	budova	k1gFnSc2	budova
<g/>
:	:	kIx,	:
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1876	[number]	k4	1876
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
císaře	císař	k1gMnSc2	císař
Viléma	Vilém	k1gMnSc2	Vilém
I.	I.	kA	I.
a	a	k8xC	a
bavorského	bavorský	k2eAgMnSc2d1	bavorský
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
II	II	kA	II
<g/>
.	.	kIx.	.
první	první	k4xOgFnSc2	první
Hudební	hudební	k2eAgFnSc2d1	hudební
slavnosti	slavnost	k1gFnSc2	slavnost
v	v	k7c6	v
Bayreuthu	Bayreuth	k1gInSc6	Bayreuth
(	(	kIx(	(
<g/>
Bayreuthský	Bayreuthský	k2eAgInSc4d1	Bayreuthský
festival	festival	k1gInSc4	festival
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yRgInPc6	který
byl	být	k5eAaImAgInS	být
uveden	uveden	k2eAgInSc1d1	uveden
celý	celý	k2eAgInSc1d1	celý
Prsten	prsten	k1gInSc1	prsten
Nibelungův	Nibelungův	k2eAgInSc1d1	Nibelungův
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1876	[number]	k4	1876
podnikl	podniknout	k5eAaPmAgMnS	podniknout
Wagner	Wagner	k1gMnSc1	Wagner
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
naposledy	naposledy	k6eAd1	naposledy
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
v	v	k7c6	v
Sorrentu	Sorrent	k1gInSc6	Sorrent
<g/>
)	)	kIx)	)
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Nietzschem	Nietzsch	k1gInSc7	Nietzsch
<g/>
.	.	kIx.	.
</s>
<s>
Nietzsche	Nietzschat	k5eAaPmIp3nS	Nietzschat
Wagnerovi	Wagner	k1gMnSc3	Wagner
vyčítal	vyčítat	k5eAaImAgMnS	vyčítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgMnS	přidat
k	k	k7c3	k
říšským	říšský	k2eAgMnPc3d1	říšský
Němcům	Němec	k1gMnPc3	Němec
<g/>
,	,	kIx,	,
a	a	k8xC	a
především	především	k6eAd1	především
mu	on	k3xPp3gInSc3	on
jako	jako	k9	jako
zapřisáhlý	zapřisáhlý	k2eAgMnSc1d1	zapřisáhlý
nepřítel	nepřítel	k1gMnSc1	nepřítel
křesťanství	křesťanství	k1gNnSc2	křesťanství
zazlíval	zazlívat	k5eAaImAgMnS	zazlívat
operu	opera	k1gFnSc4	opera
Parsifal	Parsifal	k1gFnSc2	Parsifal
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
oslavoval	oslavovat	k5eAaImAgMnS	oslavovat
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
tradice	tradice	k1gFnSc2	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Wagner	Wagner	k1gMnSc1	Wagner
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
požadoval	požadovat	k5eAaImAgMnS	požadovat
po	po	k7c6	po
svých	svůj	k3xOyFgMnPc6	svůj
přátelích	přítel	k1gMnPc6	přítel
bezmeznou	bezmezný	k2eAgFnSc4d1	bezmezná
oddanost	oddanost	k1gFnSc4	oddanost
<g/>
,	,	kIx,	,
Nietzschemu	Nietzschema	k1gFnSc4	Nietzschema
naopak	naopak	k6eAd1	naopak
vyčítal	vyčítat	k5eAaImAgMnS	vyčítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Menschliches	Menschlichesa	k1gFnPc2	Menschlichesa
<g/>
,	,	kIx,	,
alzu	alzu	k5eAaPmIp1nS	alzu
menschliches	menschliches	k1gInSc1	menschliches
(	(	kIx(	(
<g/>
Lidské	lidský	k2eAgInPc1d1	lidský
<g/>
,	,	kIx,	,
příliš	příliš	k6eAd1	příliš
lidské	lidský	k2eAgNnSc4d1	lidské
<g/>
)	)	kIx)	)
odklonil	odklonit	k5eAaPmAgMnS	odklonit
od	od	k7c2	od
jeho	on	k3xPp3gNnSc2	on
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
wagneriánského	wagneriánský	k2eAgInSc2d1	wagneriánský
myšlenkového	myšlenkový	k2eAgInSc2d1	myšlenkový
směru	směr	k1gInSc2	směr
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgInS	mít
Nietzsche	Nietzsche	k1gInSc1	Nietzsche
četné	četný	k2eAgFnSc2d1	četná
židovské	židovská	k1gFnSc2	židovská
přátele	přítel	k1gMnPc4	přítel
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gNnSc1	jejich
přátelství	přátelství	k1gNnSc1	přátelství
rozpadlo	rozpadnout	k5eAaPmAgNnS	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
ho	on	k3xPp3gMnSc4	on
přijala	přijmout	k5eAaPmAgFnS	přijmout
anglická	anglický	k2eAgFnSc1d1	anglická
královna	královna	k1gFnSc1	královna
Viktorie	Viktoria	k1gFnSc2	Viktoria
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
odcestovali	odcestovat	k5eAaPmAgMnP	odcestovat
Wagnerovi	Wagnerův	k2eAgMnPc1d1	Wagnerův
opět	opět	k6eAd1	opět
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
navštívili	navštívit	k5eAaPmAgMnP	navštívit
střídavě	střídavě	k6eAd1	střídavě
města	město	k1gNnPc1	město
Neapol	Neapol	k1gFnSc1	Neapol
<g/>
,	,	kIx,	,
Ravello	Ravello	k1gNnSc1	Ravello
a	a	k8xC	a
Siena	Siena	k1gFnSc1	Siena
a	a	k8xC	a
pobývali	pobývat	k5eAaImAgMnP	pobývat
také	také	k9	také
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1881	[number]	k4	1881
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Bayreuthu	Bayreuth	k1gInSc6	Bayreuth
uveden	uveden	k2eAgInSc1d1	uveden
Prsten	prsten	k1gInSc1	prsten
Nibelungův	Nibelungův	k2eAgInSc1d1	Nibelungův
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
Wagner	Wagner	k1gMnSc1	Wagner
zdržoval	zdržovat	k5eAaImAgMnS	zdržovat
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1882	[number]	k4	1882
dokončil	dokončit	k5eAaPmAgMnS	dokončit
v	v	k7c6	v
Palermu	Palermo	k1gNnSc6	Palermo
svou	svůj	k3xOyFgFnSc4	svůj
poslední	poslední	k2eAgFnSc4d1	poslední
operu	opera	k1gFnSc4	opera
Parsifal	Parsifal	k1gFnSc2	Parsifal
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
Bayreuthském	Bayreuthský	k2eAgInSc6d1	Bayreuthský
festivalu	festival	k1gInSc6	festival
26	[number]	k4	26
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1882	[number]	k4	1882
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1882	[number]	k4	1882
pobývali	pobývat	k5eAaImAgMnP	pobývat
Richard	Richard	k1gMnSc1	Richard
a	a	k8xC	a
Cosima	Cosima	k1gFnSc1	Cosima
Wagnerovi	Wagnerův	k2eAgMnPc1d1	Wagnerův
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
Vendramin	Vendramin	k1gInSc1	Vendramin
(	(	kIx(	(
<g/>
plným	plný	k2eAgInSc7d1	plný
názvem	název	k1gInSc7	název
Palazzo	Palazza	k1gFnSc5	Palazza
Vendramin-Calergi	Vendramin-Calergi	k1gNnPc6	Vendramin-Calergi
<g/>
)	)	kIx)	)
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Velkého	velký	k2eAgInSc2d1	velký
kanálu	kanál	k1gInSc2	kanál
(	(	kIx(	(
<g/>
Canale	Canal	k1gMnSc5	Canal
Grande	grand	k1gMnSc5	grand
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
<g/>
,	,	kIx,	,
možná	možná	k9	možná
po	po	k7c6	po
manželské	manželský	k2eAgFnSc6d1	manželská
hádce	hádka	k1gFnSc6	hádka
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Wagner	Wagner	k1gMnSc1	Wagner
dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1883	[number]	k4	1883
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
byl	být	k5eAaImAgMnS	být
převezen	převézt	k5eAaPmNgMnS	převézt
do	do	k7c2	do
domovského	domovský	k2eAgInSc2d1	domovský
Bayreuthu	Bayreuth	k1gInSc2	Bayreuth
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
byl	být	k5eAaImAgMnS	být
pochován	pochován	k2eAgMnSc1d1	pochován
v	v	k7c6	v
malém	malý	k2eAgInSc6d1	malý
parku	park	k1gInSc6	park
u	u	k7c2	u
své	svůj	k3xOyFgFnSc2	svůj
vily	vila	k1gFnSc2	vila
Wahnfried	Wahnfried	k1gInSc1	Wahnfried
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
za	za	k7c2	za
německého	německý	k2eAgNnSc2d1	německé
císařství	císařství	k1gNnSc2	císařství
a	a	k8xC	a
především	především	k9	především
později	pozdě	k6eAd2	pozdě
za	za	k7c2	za
dob	doba	k1gFnPc2	doba
nacionálního	nacionální	k2eAgInSc2d1	nacionální
socialismu	socialismus	k1gInSc2	socialismus
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
velmi	velmi	k6eAd1	velmi
různě	různě	k6eAd1	různě
a	a	k8xC	a
mnohdy	mnohdy	k6eAd1	mnohdy
jednostranně	jednostranně	k6eAd1	jednostranně
posuzováno	posuzovat	k5eAaImNgNnS	posuzovat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
antisemitismus	antisemitismus	k1gInSc1	antisemitismus
se	se	k3xPyFc4	se
projevil	projevit	k5eAaPmAgInS	projevit
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
"	"	kIx"	"
<g/>
Das	Das	k1gFnSc1	Das
Judenthum	Judenthum	k1gInSc1	Judenthum
in	in	k?	in
der	drát	k5eAaImRp2nS	drát
Musik	musika	k1gFnPc2	musika
(	(	kIx(	(
<g/>
Židovství	židovství	k1gNnSc1	židovství
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
vydal	vydat	k5eAaPmAgInS	vydat
nejprve	nejprve	k6eAd1	nejprve
anonymně	anonymně	k6eAd1	anonymně
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
si	se	k3xPyFc3	se
stěžuje	stěžovat	k5eAaImIp3nS	stěžovat
na	na	k7c4	na
"	"	kIx"	"
<g/>
požidovštěné	požidovštěný	k2eAgNnSc4d1	požidovštěný
<g/>
"	"	kIx"	"
umění	umění	k1gNnSc4	umění
a	a	k8xC	a
použil	použít	k5eAaPmAgMnS	použít
tezi	teze	k1gFnSc4	teze
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Židovství	židovství	k1gNnSc1	židovství
samo	sám	k3xTgNnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
je	být	k5eAaImIp3nS	být
neschopné	schopný	k2eNgInPc4d1	neschopný
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
svým	svůj	k3xOyFgInSc7	svůj
zevnějškem	zevnějšek	k1gInSc7	zevnějšek
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc7	svůj
řečí	řeč	k1gFnSc7	řeč
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
svým	svůj	k3xOyFgInSc7	svůj
zpěvem	zpěv	k1gInSc7	zpěv
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
odvažuje	odvažovat	k5eAaImIp3nS	odvažovat
nazývat	nazývat	k5eAaImF	nazývat
uměním	umění	k1gNnSc7	umění
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
stati	stať	k1gFnSc6	stať
Was	Was	k1gMnSc1	Was
ist	ist	k?	ist
deutsch	deutsch	k1gMnSc1	deutsch
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
Co	co	k3yInSc1	co
je	být	k5eAaImIp3nS	být
německé	německý	k2eAgNnSc1d1	německé
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1865	[number]	k4	1865
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
zdůvodnit	zdůvodnit	k5eAaPmF	zdůvodnit
ztroskotání	ztroskotání	k1gNnSc4	ztroskotání
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
že	že	k8xS	že
se	se	k3xPyFc4	se
praví	pravý	k2eAgMnPc1d1	pravý
Němci	Němec	k1gMnPc1	Němec
náhle	náhle	k6eAd1	náhle
ocitli	ocitnout	k5eAaPmAgMnP	ocitnout
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
jim	on	k3xPp3gMnPc3	on
bylo	být	k5eAaImAgNnS	být
úplně	úplně	k6eAd1	úplně
cizí	cizí	k2eAgMnSc1d1	cizí
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
bayreuthského	bayreuthský	k2eAgInSc2d1	bayreuthský
spolku	spolek	k1gInSc2	spolek
Richarda	Richard	k1gMnSc2	Richard
a	a	k8xC	a
Cosimy	Cosim	k1gInPc7	Cosim
Wagnerových	Wagnerová	k1gFnPc2	Wagnerová
přišel	přijít	k5eAaPmAgMnS	přijít
do	do	k7c2	do
německé	německý	k2eAgFnSc2d1	německá
společnosti	společnost	k1gFnSc2	společnost
úplně	úplně	k6eAd1	úplně
nový	nový	k2eAgInSc4d1	nový
druh	druh	k1gInSc4	druh
biologického	biologický	k2eAgInSc2d1	biologický
rasismu	rasismus	k1gInSc2	rasismus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
Francouzem	Francouz	k1gMnSc7	Francouz
Arthurem	Arthur	k1gMnSc7	Arthur
de	de	k?	de
Gobineau	Gobineaa	k1gFnSc4	Gobineaa
a	a	k8xC	a
poněmčeným	poněmčený	k2eAgMnSc7d1	poněmčený
Angličanem	Angličan	k1gMnSc7	Angličan
a	a	k8xC	a
zastáncem	zastánce	k1gMnSc7	zastánce
eugeniky	eugenika	k1gFnSc2	eugenika
Houstonem	Houston	k1gInSc7	Houston
Stewartem	Stewart	k1gMnSc7	Stewart
Chamberlainem	Chamberlain	k1gMnSc7	Chamberlain
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
hlásali	hlásat	k5eAaImAgMnP	hlásat
nadřazenost	nadřazenost	k1gFnSc4	nadřazenost
árijské	árijský	k2eAgFnSc2d1	árijská
rasy	rasa	k1gFnSc2	rasa
nad	nad	k7c7	nad
židovskou	židovská	k1gFnSc7	židovská
<g/>
.	.	kIx.	.
</s>
<s>
Chamberlain	Chamberlain	k1gMnSc1	Chamberlain
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
oženil	oženit	k5eAaPmAgInS	oženit
s	s	k7c7	s
Wagnerovou	Wagnerův	k2eAgFnSc7d1	Wagnerova
dcerou	dcera	k1gFnSc7	dcera
Evou	Eva	k1gFnSc7	Eva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
měl	mít	k5eAaImAgMnS	mít
paradoxně	paradoxně	k6eAd1	paradoxně
mezi	mezi	k7c4	mezi
Židy	Žid	k1gMnPc4	Žid
několik	několik	k4yIc4	několik
přátel	přítel	k1gMnPc2	přítel
a	a	k8xC	a
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
Karla	Karel	k1gMnSc2	Karel
Tausiga	Tausig	k1gMnSc2	Tausig
<g/>
,	,	kIx,	,
Josepha	Joseph	k1gMnSc2	Joseph
Rubinsteina	Rubinstein	k1gMnSc2	Rubinstein
<g/>
,	,	kIx,	,
Angelo	Angela	k1gFnSc5	Angela
Neumanna	Neumann	k1gMnSc4	Neumann
<g/>
,	,	kIx,	,
Hermanna	Hermann	k1gMnSc4	Hermann
Leviho	Levi	k1gMnSc4	Levi
a	a	k8xC	a
slavnou	slavný	k2eAgFnSc4d1	slavná
zpěvačku	zpěvačka	k1gFnSc4	zpěvačka
Lilli	Lille	k1gFnSc4	Lille
Lehmann	Lehmanna	k1gFnPc2	Lehmanna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
vždy	vždy	k6eAd1	vždy
přijímáno	přijímat	k5eAaImNgNnS	přijímat
s	s	k7c7	s
rozpaky	rozpak	k1gInPc7	rozpak
<g/>
,	,	kIx,	,
nejednou	jednou	k6eNd1	jednou
skončilo	skončit	k5eAaPmAgNnS	skončit
představení	představení	k1gNnSc3	představení
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
fiaskem	fiasko	k1gNnSc7	fiasko
a	a	k8xC	a
několikrát	několikrát	k6eAd1	několikrát
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
protestů	protest	k1gInPc2	protest
"	"	kIx"	"
<g/>
Spolku	spolek	k1gInSc2	spolek
přeživších	přeživší	k2eAgInPc2d1	přeživší
holocaust	holocaust	k1gInSc4	holocaust
<g/>
"	"	kIx"	"
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
písní	píseň	k1gFnPc2	píseň
na	na	k7c4	na
slova	slovo	k1gNnPc4	slovo
Mathildy	Mathilda	k1gMnSc2	Mathilda
Wesendonckové	Wesendonckové	k2eAgMnSc2d1	Wesendonckové
(	(	kIx(	(
<g/>
Fünf	Fünf	k1gInSc1	Fünf
Gedichte	Gedicht	k1gInSc5	Gedicht
für	für	k?	für
eine	ein	k1gMnSc2	ein
Frauenstimme	Frauenstimme	k1gMnSc2	Frauenstimme
–	–	k?	–
"	"	kIx"	"
<g/>
Wesendonck	Wesendonck	k1gMnSc1	Wesendonck
Lieder	Lieder	k1gMnSc1	Lieder
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Písně	píseň	k1gFnSc2	píseň
<g/>
:	:	kIx,	:
Anděl	Anděla	k1gFnPc2	Anděla
(	(	kIx(	(
<g/>
Der	drát	k5eAaImRp2nS	drát
Engel	Engel	k1gInSc1	Engel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Stůj	stát	k5eAaImRp2nS	stát
tiše	tiš	k1gFnPc1	tiš
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
Stehe	Stehe	k1gNnSc7	Stehe
still	still	k1gMnSc1	still
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ve	v	k7c6	v
skleníku	skleník	k1gInSc6	skleník
(	(	kIx(	(
<g/>
Im	Im	k1gMnSc1	Im
Treibhaus	Treibhaus	k1gMnSc1	Treibhaus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bolesti	bolestit	k5eAaImRp2nS	bolestit
(	(	kIx(	(
<g/>
Schmerzen	Schmerzen	k2eAgInSc1d1	Schmerzen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sny	sen	k1gInPc1	sen
(	(	kIx(	(
<g/>
Träume	Träum	k1gInSc5	Träum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Písně	píseň	k1gFnPc1	píseň
byly	být	k5eAaImAgFnP	být
složeny	složit	k5eAaPmNgFnP	složit
v	v	k7c6	v
letech	let	k1gInPc6	let
1857	[number]	k4	1857
<g/>
–	–	k?	–
<g/>
1858	[number]	k4	1858
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Wagner	Wagner	k1gMnSc1	Wagner
psal	psát	k5eAaImAgMnS	psát
opery	opera	k1gFnPc4	opera
Valkýra	valkýra	k1gFnSc1	valkýra
a	a	k8xC	a
Siegfried	Siegfried	k1gInSc1	Siegfried
<g/>
.	.	kIx.	.
</s>
<s>
Cyklus	cyklus	k1gInSc1	cyklus
jako	jako	k8xS	jako
celek	celek	k1gInSc1	celek
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
uveden	uvést	k5eAaPmNgInS	uvést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1862	[number]	k4	1862
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
všem	všecek	k3xTgFnPc3	všecek
svým	svůj	k3xOyFgFnPc3	svůj
operám	opera	k1gFnPc3	opera
si	se	k3xPyFc3	se
Wagner	Wagner	k1gMnSc1	Wagner
napsal	napsat	k5eAaPmAgMnS	napsat
libreta	libreto	k1gNnPc4	libreto
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Libreto	libreto	k1gNnSc4	libreto
k	k	k7c3	k
nedokončené	dokončený	k2eNgFnSc3d1	nedokončená
opeře	opera	k1gFnSc3	opera
Die	Die	k1gMnSc1	Die
Hochzeit	Hochzeit	k1gMnSc1	Hochzeit
zničil	zničit	k5eAaPmAgMnS	zničit
a	a	k8xC	a
libreto	libreto	k1gNnSc4	libreto
k	k	k7c3	k
Rienzimu	Rienzim	k1gInSc3	Rienzim
daroval	darovat	k5eAaPmAgMnS	darovat
bavorskému	bavorský	k2eAgMnSc3d1	bavorský
králi	král	k1gMnSc3	král
Ludvíku	Ludvík	k1gMnSc6	Ludvík
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
přivlastnil	přivlastnit	k5eAaPmAgMnS	přivlastnit
libreta	libreto	k1gNnSc2	libreto
k	k	k7c3	k
operám	opera	k1gFnPc3	opera
Rienzi	Rienze	k1gFnSc4	Rienze
<g/>
,	,	kIx,	,
Die	Die	k1gMnSc1	Die
Feen	Feen	k1gMnSc1	Feen
a	a	k8xC	a
Das	Das	k1gMnSc1	Das
Liebesverbot	Liebesverbota	k1gFnPc2	Liebesverbota
oder	odra	k1gFnPc2	odra
Novize	Novize	k1gFnSc2	Novize
von	von	k1gInSc1	von
Palermo	Palermo	k1gNnSc1	Palermo
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
tři	tři	k4xCgFnPc1	tři
partitury	partitura	k1gFnPc1	partitura
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
bohužel	bohužel	k6eAd1	bohužel
ztracené	ztracený	k2eAgFnPc4d1	ztracená
<g/>
.	.	kIx.	.
</s>
<s>
Die	Die	k?	Die
Hochzeit	Hochzeit	k1gInSc1	Hochzeit
(	(	kIx(	(
<g/>
Svatba	svatba	k1gFnSc1	svatba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1832	[number]	k4	1832
-	-	kIx~	-
nedokončeno	dokončit	k5eNaPmNgNnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Die	Die	k?	Die
Feen	Feen	k1gInSc1	Feen
(	(	kIx(	(
<g/>
Víly	víla	k1gFnPc1	víla
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1834	[number]	k4	1834
–	–	k?	–
premiéra	premiéra	k1gFnSc1	premiéra
29	[number]	k4	29
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1888	[number]	k4	1888
v	v	k7c6	v
Královském	královský	k2eAgNnSc6d1	královské
dvorním	dvorní	k2eAgNnSc6d1	dvorní
a	a	k8xC	a
národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
(	(	kIx(	(
<g/>
Königliches	Königliches	k1gMnSc1	Königliches
Hof-	Hof-	k1gMnSc1	Hof-
und	und	k?	und
Nationaltheater	Nationaltheater	k1gMnSc1	Nationaltheater
<g/>
)	)	kIx)	)
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
.	.	kIx.	.
</s>
<s>
Das	Das	k?	Das
Liebesverbot	Liebesverbot	k1gInSc1	Liebesverbot
oder	odra	k1gFnPc2	odra
Novize	Novize	k1gFnSc2	Novize
von	von	k1gInSc1	von
Palermo	Palermo	k1gNnSc1	Palermo
(	(	kIx(	(
<g/>
Zákaz	zákaz	k1gInSc1	zákaz
lásky	láska	k1gFnSc2	láska
aneb	aneb	k?	aneb
Novicka	novicka	k1gFnSc1	novicka
z	z	k7c2	z
Palerma	Palermo	k1gNnSc2	Palermo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1834	[number]	k4	1834
<g/>
–	–	k?	–
<g/>
1836	[number]	k4	1836
–	–	k?	–
premiéra	premiéra	k1gFnSc1	premiéra
29	[number]	k4	29
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1836	[number]	k4	1836
v	v	k7c6	v
Městském	městský	k2eAgNnSc6d1	Městské
divadle	divadlo	k1gNnSc6	divadlo
(	(	kIx(	(
<g/>
Stadttheater	Stadttheater	k1gMnSc1	Stadttheater
<g/>
)	)	kIx)	)
v	v	k7c6	v
Magdeburgu	Magdeburg	k1gInSc6	Magdeburg
<g/>
.	.	kIx.	.
</s>
<s>
Rienzi	Rienze	k1gFnSc4	Rienze
<g/>
,	,	kIx,	,
der	drát	k5eAaImRp2nS	drát
Letzte	Letzte	k1gMnSc1	Letzte
der	drát	k5eAaImRp2nS	drát
Tribunen	Tribunen	k1gInSc1	Tribunen
(	(	kIx(	(
<g/>
Rienzi	Rienh	k1gMnPc1	Rienh
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgMnPc1d1	poslední
z	z	k7c2	z
tribunů	tribun	k1gMnPc2	tribun
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1837	[number]	k4	1837
<g/>
–	–	k?	–
<g/>
1840	[number]	k4	1840
–	–	k?	–
premiéra	premiéra	k1gFnSc1	premiéra
20	[number]	k4	20
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1842	[number]	k4	1842
v	v	k7c6	v
Královském	královský	k2eAgNnSc6d1	královské
saském	saský	k2eAgNnSc6d1	Saské
dvorním	dvorní	k2eAgNnSc6d1	dvorní
divadle	divadlo	k1gNnSc6	divadlo
(	(	kIx(	(
<g/>
Königlich	Königlich	k1gMnSc1	Königlich
Sächsisches	Sächsisches	k1gMnSc1	Sächsisches
Hoftheater	Hoftheater	k1gMnSc1	Hoftheater
<g/>
)	)	kIx)	)
v	v	k7c6	v
Drážďanech	Drážďany	k1gInPc6	Drážďany
<g/>
.	.	kIx.	.
</s>
<s>
Der	drát	k5eAaImRp2nS	drát
fliegende	fliegend	k1gMnSc5	fliegend
Holländer	Holländer	k1gMnSc1	Holländer
(	(	kIx(	(
<g/>
Bludný	bludný	k2eAgMnSc1d1	bludný
Holanďan	Holanďan	k1gMnSc1	Holanďan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1840	[number]	k4	1840
<g/>
–	–	k?	–
<g/>
1841	[number]	k4	1841
–	–	k?	–
premiéra	premiéra	k1gFnSc1	premiéra
2	[number]	k4	2
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1843	[number]	k4	1843
v	v	k7c6	v
Královském	královský	k2eAgNnSc6d1	královské
saském	saský	k2eAgNnSc6d1	Saské
dvorním	dvorní	k2eAgNnSc6d1	dvorní
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Drážďanech	Drážďany	k1gInPc6	Drážďany
<g/>
.	.	kIx.	.
</s>
<s>
Tannhäuser	Tannhäuser	k1gInSc1	Tannhäuser
und	und	k?	und
der	drát	k5eAaImRp2nS	drát
Sängerkrieg	Sängerkrieg	k1gInSc1	Sängerkrieg
auf	auf	k?	auf
Wartburg	Wartburg	k1gInSc1	Wartburg
(	(	kIx(	(
<g/>
Tannhäuser	Tannhäuser	k1gInSc1	Tannhäuser
aneb	aneb	k?	aneb
Zápas	zápas	k1gInSc4	zápas
pěvců	pěvec	k1gMnPc2	pěvec
na	na	k7c6	na
Wartburgu	Wartburg	k1gInSc6	Wartburg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1842	[number]	k4	1842
<g/>
–	–	k?	–
<g/>
1845	[number]	k4	1845
–	–	k?	–
premiéra	premiéra	k1gFnSc1	premiéra
19	[number]	k4	19
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1845	[number]	k4	1845
v	v	k7c6	v
Královském	královský	k2eAgNnSc6d1	královské
saském	saský	k2eAgNnSc6d1	Saské
dvorním	dvorní	k2eAgNnSc6d1	dvorní
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Drážďanech	Drážďany	k1gInPc6	Drážďany
<g/>
.	.	kIx.	.
</s>
<s>
Lohengrin	Lohengrin	k1gInSc1	Lohengrin
<g/>
,	,	kIx,	,
1845	[number]	k4	1845
<g/>
–	–	k?	–
<g/>
1848	[number]	k4	1848
–	–	k?	–
premiéra	premiéra	k1gFnSc1	premiéra
28	[number]	k4	28
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1850	[number]	k4	1850
ve	v	k7c6	v
Velkovévodském	Velkovévodský	k2eAgNnSc6d1	Velkovévodský
dvorním	dvorní	k2eAgNnSc6d1	dvorní
divadle	divadlo	k1gNnSc6	divadlo
(	(	kIx(	(
<g/>
Großherzogliches	Großherzogliches	k1gMnSc1	Großherzogliches
Hoftheater	Hoftheater	k1gMnSc1	Hoftheater
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Výmaru	Výmar	k1gInSc6	Výmar
<g/>
.	.	kIx.	.
</s>
<s>
Tristan	Tristan	k1gInSc1	Tristan
und	und	k?	und
Isolde	Isold	k1gInSc5	Isold
(	(	kIx(	(
<g/>
Tristan	Tristan	k1gInSc1	Tristan
a	a	k8xC	a
Isolda	Isolda	k1gFnSc1	Isolda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1856	[number]	k4	1856
<g/>
–	–	k?	–
<g/>
1859	[number]	k4	1859
–	–	k?	–
premiéra	premiéra	k1gFnSc1	premiéra
10	[number]	k4	10
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1865	[number]	k4	1865
v	v	k7c6	v
Královském	královský	k2eAgNnSc6d1	královské
dvorním	dvorní	k2eAgNnSc6d1	dvorní
a	a	k8xC	a
národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
.	.	kIx.	.
</s>
<s>
Die	Die	k?	Die
Meistersinger	Meistersinger	k1gInSc1	Meistersinger
von	von	k1gInSc1	von
Nürnberg	Nürnberg	k1gInSc1	Nürnberg
(	(	kIx(	(
<g/>
Mistři	mistr	k1gMnPc1	mistr
pěvci	pěvec	k1gMnPc1	pěvec
norimberští	norimberský	k2eAgMnPc1d1	norimberský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1845	[number]	k4	1845
<g/>
–	–	k?	–
<g/>
1867	[number]	k4	1867
–	–	k?	–
premiéra	premiéra	k1gFnSc1	premiéra
21	[number]	k4	21
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1868	[number]	k4	1868
v	v	k7c6	v
Královském	královský	k2eAgNnSc6d1	královské
dvorním	dvorní	k2eAgNnSc6d1	dvorní
a	a	k8xC	a
národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
.	.	kIx.	.
</s>
<s>
Der	drát	k5eAaImRp2nS	drát
Ring	ring	k1gInSc1	ring
des	des	k1gNnSc6	des
Nibelungen	Nibelungen	k1gInSc1	Nibelungen
(	(	kIx(	(
<g/>
Prsten	prsten	k1gInSc1	prsten
Nibelungův	Nibelungův	k2eAgInSc1d1	Nibelungův
<g/>
)	)	kIx)	)
–	–	k?	–
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgFnPc4	čtyři
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
Das	Das	k1gMnSc1	Das
Rheingold	Rheingold	k1gMnSc1	Rheingold
(	(	kIx(	(
<g/>
Zlato	zlato	k1gNnSc1	zlato
Rýna	Rýn	k1gInSc2	Rýn
nebo	nebo	k8xC	nebo
též	též	k9	též
Rýnské	rýnský	k2eAgNnSc4d1	rýnské
zlato	zlato	k1gNnSc4	zlato
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1851	[number]	k4	1851
<g/>
–	–	k?	–
<g/>
1854	[number]	k4	1854
premiéra	premiéra	k1gFnSc1	premiéra
22.9	[number]	k4	22.9
<g/>
.	.	kIx.	.
1869	[number]	k4	1869
v	v	k7c6	v
Královském	královský	k2eAgNnSc6d1	královské
dvorním	dvorní	k2eAgNnSc6d1	dvorní
a	a	k8xC	a
národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
.	.	kIx.	.
</s>
<s>
Die	Die	k?	Die
Walküre	Walkür	k1gMnSc5	Walkür
(	(	kIx(	(
<g/>
Valkýra	valkýra	k1gFnSc1	valkýra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1851	[number]	k4	1851
<g/>
–	–	k?	–
<g/>
1856	[number]	k4	1856
–	–	k?	–
premiéra	premiéra	k1gFnSc1	premiéra
26	[number]	k4	26
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1870	[number]	k4	1870
v	v	k7c6	v
Královském	královský	k2eAgNnSc6d1	královské
dvorním	dvorní	k2eAgNnSc6d1	dvorní
a	a	k8xC	a
národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
.	.	kIx.	.
</s>
<s>
Siegfried	Siegfried	k1gMnSc1	Siegfried
<g/>
,	,	kIx,	,
1851	[number]	k4	1851
<g/>
–	–	k?	–
<g/>
1871	[number]	k4	1871
–	–	k?	–
premiéra	premiéra	k1gFnSc1	premiéra
16	[number]	k4	16
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1876	[number]	k4	1876
ve	v	k7c6	v
Festivalovém	festivalový	k2eAgInSc6d1	festivalový
domě	dům	k1gInSc6	dům
(	(	kIx(	(
<g/>
Festspielhaus	Festspielhaus	k1gMnSc1	Festspielhaus
<g/>
)	)	kIx)	)
v	v	k7c6	v
Bayreuthu	Bayreuth	k1gInSc6	Bayreuth
<g/>
.	.	kIx.	.
</s>
<s>
Götterdämmerung	Götterdämmerung	k1gInSc1	Götterdämmerung
(	(	kIx(	(
<g/>
Soumrak	soumrak	k1gInSc1	soumrak
bohů	bůh	k1gMnPc2	bůh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1848	[number]	k4	1848
<g/>
–	–	k?	–
<g/>
1874	[number]	k4	1874
–	–	k?	–
premiéra	premiéra	k1gFnSc1	premiéra
17	[number]	k4	17
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1876	[number]	k4	1876
ve	v	k7c6	v
Festivalovém	festivalový	k2eAgInSc6d1	festivalový
domě	dům	k1gInSc6	dům
v	v	k7c6	v
Bayreuthu	Bayreuth	k1gInSc6	Bayreuth
<g/>
.	.	kIx.	.
</s>
<s>
Parsifal	Parsifat	k5eAaPmAgMnS	Parsifat
<g/>
,	,	kIx,	,
1865	[number]	k4	1865
<g/>
–	–	k?	–
<g/>
1882	[number]	k4	1882
–	–	k?	–
premiéra	premiéra	k1gFnSc1	premiéra
26	[number]	k4	26
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1882	[number]	k4	1882
ve	v	k7c6	v
Festivalovém	festivalový	k2eAgInSc6d1	festivalový
domě	dům	k1gInSc6	dům
v	v	k7c6	v
Bayreuthu	Bayreuth	k1gInSc6	Bayreuth
<g/>
.	.	kIx.	.
</s>
<s>
Nepočítáme	počítat	k5eNaImIp1nP	počítat
<g/>
-li	i	k?	-li
nedokončenou	dokončený	k2eNgFnSc4d1	nedokončená
operu	opera	k1gFnSc4	opera
Die	Die	k1gMnSc1	Die
Hochzeit	Hochzeit	k1gMnSc1	Hochzeit
(	(	kIx(	(
<g/>
Svatba	svatba	k1gFnSc1	svatba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
uvedeny	uvést	k5eAaPmNgFnP	uvést
všechny	všechen	k3xTgFnPc1	všechen
Wagnerovy	Wagnerův	k2eAgFnPc1d1	Wagnerova
opery	opera	k1gFnPc1	opera
kromě	kromě	k7c2	kromě
Das	Das	k1gMnPc2	Das
Liebesverbot	Liebesverbot	k1gInSc4	Liebesverbot
oder	odra	k1gFnPc2	odra
Der	drát	k5eAaImRp2nS	drát
Novize	Novize	k1gFnPc4	Novize
von	von	k1gInSc4	von
Palermo	Palermo	k1gNnSc1	Palermo
(	(	kIx(	(
<g/>
Zápověď	zápověď	k1gFnSc1	zápověď
lásky	láska	k1gFnSc2	láska
aneb	aneb	k?	aneb
Novicka	novicka	k1gFnSc1	novicka
z	z	k7c2	z
Palerma	Palermo	k1gNnSc2	Palermo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnPc1	jejich
první	první	k4xOgMnPc1	první
uvedení	uvedený	k2eAgMnPc1d1	uvedený
vždy	vždy	k6eAd1	vždy
zrealizovaly	zrealizovat	k5eAaPmAgFnP	zrealizovat
německé	německý	k2eAgFnPc1d1	německá
scény	scéna	k1gFnPc1	scéna
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1888	[number]	k4	1888
bylo	být	k5eAaImAgNnS	být
operou	opera	k1gFnSc7	opera
Die	Die	k1gFnSc2	Die
Meistersinger	Meistersingra	k1gFnPc2	Meistersingra
von	von	k1gInSc1	von
Nürnberg	Nürnberg	k1gMnSc1	Nürnberg
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
Nové	Nové	k2eAgNnSc1d1	Nové
německé	německý	k2eAgNnSc1d1	německé
divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
soubory	soubor	k1gInPc1	soubor
uváděly	uvádět	k5eAaImAgInP	uvádět
Wagnerovy	Wagnerův	k2eAgFnPc4d1	Wagnerova
opery	opera	k1gFnSc2	opera
až	až	k9	až
později	pozdě	k6eAd2	pozdě
<g/>
:	:	kIx,	:
Doposud	doposud	k6eAd1	doposud
nejvíce	nejvíce	k6eAd1	nejvíce
hranými	hraný	k2eAgFnPc7d1	hraná
Wagnerovými	Wagnerův	k2eAgFnPc7d1	Wagnerova
operami	opera	k1gFnPc7	opera
na	na	k7c6	na
území	území	k1gNnSc6	území
Česka	Česko	k1gNnSc2	Česko
jsou	být	k5eAaImIp3nP	být
Bludný	bludný	k2eAgMnSc1d1	bludný
Holanďan	Holanďan	k1gMnSc1	Holanďan
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
70	[number]	k4	70
inscenací	inscenace	k1gFnPc2	inscenace
<g/>
)	)	kIx)	)
a	a	k8xC	a
Lohengrin	Lohengrin	k1gInSc1	Lohengrin
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
60	[number]	k4	60
inscenací	inscenace	k1gFnPc2	inscenace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lohengrin	Lohengrin	k1gInSc1	Lohengrin
<g/>
:	:	kIx,	:
1885	[number]	k4	1885
(	(	kIx(	(
<g/>
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
)	)	kIx)	)
Tannhäuser	Tannhäuser	k1gInSc1	Tannhäuser
<g/>
:	:	kIx,	:
1888	[number]	k4	1888
(	(	kIx(	(
<g/>
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
)	)	kIx)	)
Die	Die	k1gFnSc1	Die
Meistersinger	Meistersinger	k1gInSc4	Meistersinger
von	von	k1gInSc1	von
Nürnberg	Nürnberg	k1gInSc1	Nürnberg
<g/>
:	:	kIx,	:
1894	[number]	k4	1894
(	(	kIx(	(
<g/>
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
)	)	kIx)	)
Der	drát	k5eAaImRp2nS	drát
fliegende	fliegend	k1gMnSc5	fliegend
Holländer	Holländer	k1gMnSc1	Holländer
<g/>
:	:	kIx,	:
1906	[number]	k4	1906
(	(	kIx(	(
<g/>
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
)	)	kIx)	)
Tristan	Tristan	k1gInSc1	Tristan
und	und	k?	und
Isolde	Isold	k1gInSc5	Isold
<g/>
:	:	kIx,	:
1913	[number]	k4	1913
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
)	)	kIx)	)
Rienzi	Rienh	k1gMnPc1	Rienh
<g/>
:	:	kIx,	:
1914	[number]	k4	1914
(	(	kIx(	(
<g/>
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
)	)	kIx)	)
Das	Das	k1gMnSc1	Das
Rheingold	Rheingold	k1gMnSc1	Rheingold
<g/>
:	:	kIx,	:
1915	[number]	k4	1915
(	(	kIx(	(
<g/>
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
)	)	kIx)	)
Die	Die	k1gMnSc5	Die
Walküre	Walkür	k1gMnSc5	Walkür
<g/>
:	:	kIx,	:
1916	[number]	k4	1916
(	(	kIx(	(
<g/>
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
)	)	kIx)	)
Siegfried	Siegfried	k1gInSc1	Siegfried
<g/>
:	:	kIx,	:
1931	[number]	k4	1931
(	(	kIx(	(
<g/>
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
Wagnerova	Wagnerův	k2eAgFnSc1d1	Wagnerova
opera	opera	k1gFnSc1	opera
Die	Die	k1gMnSc1	Die
Feen	Feen	k1gInSc1	Feen
(	(	kIx(	(
<g/>
Víly	víla	k1gFnSc2	víla
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
nastudovala	nastudovat	k5eAaBmAgFnS	nastudovat
zatím	zatím	k6eAd1	zatím
jen	jen	k9	jen
jednou	jednou	k6eAd1	jednou
a	a	k8xC	a
premiéra	premiéra	k1gFnSc1	premiéra
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1893	[number]	k4	1893
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
německém	německý	k2eAgNnSc6d1	německé
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Stavovské	stavovský	k2eAgNnSc1d1	Stavovské
divadlo	divadlo	k1gNnSc1	divadlo
bylo	být	k5eAaImAgNnS	být
první	první	k4xOgNnSc1	první
divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Habsburské	habsburský	k2eAgFnSc6d1	habsburská
monarchii	monarchie	k1gFnSc6	monarchie
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
uvedlo	uvést	k5eAaPmAgNnS	uvést
Wagnerovu	Wagnerův	k2eAgFnSc4d1	Wagnerova
operu	opera	k1gFnSc4	opera
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jí	jíst	k5eAaImIp3nS	jíst
Tannhäuser	Tannhäuser	k1gInSc4	Tannhäuser
<g/>
.	.	kIx.	.
</s>
<s>
Wagnerovy	Wagnerův	k2eAgFnSc2d1	Wagnerova
opery	opera	k1gFnSc2	opera
velice	velice	k6eAd1	velice
propagoval	propagovat	k5eAaImAgMnS	propagovat
František	František	k1gMnSc1	František
Škroup	Škroup	k1gMnSc1	Škroup
<g/>
.	.	kIx.	.
</s>
<s>
Tannhäuser	Tannhäuser	k1gInSc1	Tannhäuser
<g/>
:	:	kIx,	:
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1854	[number]	k4	1854
<g/>
.	.	kIx.	.
</s>
<s>
Lohengrin	Lohengrin	k1gInSc1	Lohengrin
<g/>
:	:	kIx,	:
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1856	[number]	k4	1856
<g/>
.	.	kIx.	.
</s>
<s>
Der	drát	k5eAaImRp2nS	drát
fliegende	fliegend	k1gMnSc5	fliegend
Holländer	Holländer	k1gMnSc1	Holländer
<g/>
:	:	kIx,	:
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1856	[number]	k4	1856
<g/>
.	.	kIx.	.
</s>
<s>
Rienzi	Rienh	k1gMnPc1	Rienh
<g/>
:	:	kIx,	:
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1859	[number]	k4	1859
<g/>
.	.	kIx.	.
</s>
<s>
Die	Die	k?	Die
Meistersinger	Meistersinger	k1gInSc1	Meistersinger
von	von	k1gInSc1	von
Nürnberg	Nürnberg	k1gInSc1	Nürnberg
<g/>
:	:	kIx,	:
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1871	[number]	k4	1871
<g/>
.	.	kIx.	.
</s>
<s>
Das	Das	k?	Das
Rheingold	Rheingold	k1gInSc1	Rheingold
<g/>
:	:	kIx,	:
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1885	[number]	k4	1885
<g/>
.	.	kIx.	.
</s>
<s>
Die	Die	k?	Die
Walküre	Walkür	k1gMnSc5	Walkür
<g/>
:	:	kIx,	:
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1885	[number]	k4	1885
<g/>
.	.	kIx.	.
</s>
<s>
Tristan	Tristan	k1gInSc1	Tristan
und	und	k?	und
Isolde	Isold	k1gInSc5	Isold
<g/>
:	:	kIx,	:
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1886	[number]	k4	1886
<g/>
.	.	kIx.	.
</s>
<s>
Siegfried	Siegfried	k1gInSc1	Siegfried
<g/>
:	:	kIx,	:
9	[number]	k4	9
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1887	[number]	k4	1887
<g/>
.	.	kIx.	.
</s>
<s>
Götterdämmerung	Götterdämmerung	k1gInSc1	Götterdämmerung
<g/>
:	:	kIx,	:
10	[number]	k4	10
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1887	[number]	k4	1887
<g/>
.	.	kIx.	.
</s>
<s>
Parsifal	Parsifat	k5eAaPmAgMnS	Parsifat
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1914	[number]	k4	1914
<g/>
.	.	kIx.	.
</s>
<s>
Lohengrin	Lohengrin	k1gInSc1	Lohengrin
<g/>
:	:	kIx,	:
12	[number]	k4	12
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1885	[number]	k4	1885
(	(	kIx(	(
<g/>
režisér	režisér	k1gMnSc1	režisér
František	František	k1gMnSc1	František
Hynek	Hynek	k1gMnSc1	Hynek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tannhäuser	Tannhäuser	k1gInSc1	Tannhäuser
<g/>
:	:	kIx,	:
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1891	[number]	k4	1891
(	(	kIx(	(
<g/>
režiséři	režisér	k1gMnPc1	režisér
František	František	k1gMnSc1	František
Hynek	Hynek	k1gMnSc1	Hynek
a	a	k8xC	a
Robert	Robert	k1gMnSc1	Robert
Polák	Polák	k1gMnSc1	Polák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Die	Die	k?	Die
Meistersinger	Meistersinger	k1gInSc1	Meistersinger
von	von	k1gInSc1	von
Nürnberg	Nürnberg	k1gInSc1	Nürnberg
<g/>
:	:	kIx,	:
7	[number]	k4	7
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1894	[number]	k4	1894
(	(	kIx(	(
<g/>
režisér	režisér	k1gMnSc1	režisér
František	František	k1gMnSc1	František
Hynek	Hynek	k1gMnSc1	Hynek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Der	drát	k5eAaImRp2nS	drát
fliegende	fliegend	k1gMnSc5	fliegend
Holländer	Holländer	k1gMnSc1	Holländer
<g/>
:	:	kIx,	:
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1907	[number]	k4	1907
(	(	kIx(	(
<g/>
režisér	režisér	k1gMnSc1	režisér
Robert	Robert	k1gMnSc1	Robert
Polák	Polák	k1gMnSc1	Polák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tristan	Tristan	k1gInSc1	Tristan
und	und	k?	und
Isolde	Isold	k1gInSc5	Isold
<g/>
:	:	kIx,	:
10	[number]	k4	10
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1913	[number]	k4	1913
(	(	kIx(	(
<g/>
režisér	režisér	k1gMnSc1	režisér
Robert	Robert	k1gMnSc1	Robert
Polák	Polák	k1gMnSc1	Polák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Parsifal	Parsifat	k5eAaPmAgMnS	Parsifat
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1914	[number]	k4	1914
(	(	kIx(	(
<g/>
režisér	režisér	k1gMnSc1	režisér
Robert	Robert	k1gMnSc1	Robert
Polák	Polák	k1gMnSc1	Polák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Das	Das	k?	Das
Rheingold	Rheingold	k1gInSc1	Rheingold
<g/>
:	:	kIx,	:
19	[number]	k4	19
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1915	[number]	k4	1915
(	(	kIx(	(
<g/>
režisér	režisér	k1gMnSc1	režisér
Robert	Robert	k1gMnSc1	Robert
Polák	Polák	k1gMnSc1	Polák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Die	Die	k?	Die
Walküre	Walkür	k1gMnSc5	Walkür
<g/>
:	:	kIx,	:
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1916	[number]	k4	1916
(	(	kIx(	(
<g/>
režisér	režisér	k1gMnSc1	režisér
Robert	Robert	k1gMnSc1	Robert
Polák	Polák	k1gMnSc1	Polák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Siegfried	Siegfried	k1gInSc1	Siegfried
<g/>
:	:	kIx,	:
23	[number]	k4	23
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1932	[number]	k4	1932
(	(	kIx(	(
<g/>
režisér	režisér	k1gMnSc1	režisér
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Pujman	Pujman	k1gMnSc1	Pujman
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rienzi	Rienh	k1gMnPc1	Rienh
<g/>
:	:	kIx,	:
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1991	[number]	k4	1991
(	(	kIx(	(
<g/>
ve	v	k7c6	v
Smetanově	Smetanův	k2eAgNnSc6d1	Smetanovo
divadle	divadlo	k1gNnSc6	divadlo
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
Ladislav	Ladislav	k1gMnSc1	Ladislav
Štros	Štros	k1gMnSc1	Štros
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Götterdämmerung	Götterdämmerung	k1gInSc1	Götterdämmerung
<g/>
:	:	kIx,	:
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2005	[number]	k4	2005
(	(	kIx(	(
<g/>
režisér	režisér	k1gMnSc1	režisér
Kurt	Kurt	k1gMnSc1	Kurt
Horres	Horres	k1gMnSc1	Horres
<g/>
)	)	kIx)	)
Richard	Richard	k1gMnSc1	Richard
Wagner	Wagner	k1gMnSc1	Wagner
napsal	napsat	k5eAaPmAgMnS	napsat
také	také	k9	také
řadu	řada	k1gFnSc4	řada
různých	různý	k2eAgFnPc2d1	různá
polemických	polemický	k2eAgFnPc2d1	polemická
statí	stať	k1gFnPc2	stať
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
textů	text	k1gInPc2	text
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
zaměřoval	zaměřovat	k5eAaImAgInS	zaměřovat
na	na	k7c4	na
vlastní	vlastní	k2eAgInSc4d1	vlastní
ideál	ideál	k1gInSc4	ideál
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gNnPc6	jeho
dílech	dílo	k1gNnPc6	dílo
se	se	k3xPyFc4	se
také	také	k9	také
často	často	k6eAd1	často
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
oblast	oblast	k1gFnSc4	oblast
politickou	politický	k2eAgFnSc4d1	politická
a	a	k8xC	a
sociální	sociální	k2eAgFnSc4d1	sociální
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	on	k3xPp3gMnSc4	on
nejznámější	známý	k2eAgNnPc1d3	nejznámější
díla	dílo	k1gNnPc1	dílo
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Die	Die	k1gFnSc1	Die
Revolution	Revolution	k1gInSc1	Revolution
(	(	kIx(	(
<g/>
1849	[number]	k4	1849
<g/>
)	)	kIx)	)
Die	Die	k1gMnSc1	Die
Kunst	Kunst	k1gMnSc1	Kunst
und	und	k?	und
die	die	k?	die
Revolution	Revolution	k1gInSc1	Revolution
(	(	kIx(	(
<g/>
1849	[number]	k4	1849
<g/>
)	)	kIx)	)
Das	Das	k1gFnSc1	Das
Kunstwerk	Kunstwerk	k1gInSc1	Kunstwerk
der	drát	k5eAaImRp2nS	drát
Zukunft	Zukunft	k1gMnSc1	Zukunft
(	(	kIx(	(
<g/>
1850	[number]	k4	1850
<g/>
)	)	kIx)	)
Das	Das	k1gFnSc1	Das
Judenthum	Judenthum	k1gInSc1	Judenthum
in	in	k?	in
der	drát	k5eAaImRp2nS	drát
Musik	musika	k1gFnPc2	musika
(	(	kIx(	(
<g/>
1850	[number]	k4	1850
<g/>
)	)	kIx)	)
Oper	opera	k1gFnPc2	opera
und	und	k?	und
Drama	drama	k1gNnSc1	drama
(	(	kIx(	(
<g/>
1851	[number]	k4	1851
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Česky	česky	k6eAd1	česky
Opera	opera	k1gFnSc1	opera
a	a	k8xC	a
drama	drama	k1gNnSc1	drama
<g/>
.	.	kIx.	.
</s>
<s>
Wagner	Wagner	k1gMnSc1	Wagner
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
v	v	k7c6	v
jakém	jaký	k3yRgInSc6	jaký
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
dobová	dobový	k2eAgFnSc1d1	dobová
hudba	hudba	k1gFnSc1	hudba
a	a	k8xC	a
opera	opera	k1gFnSc1	opera
a	a	k8xC	a
předkládá	předkládat	k5eAaImIp3nS	předkládat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
vizi	vize	k1gFnSc4	vize
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
hudební	hudební	k2eAgNnPc1d1	hudební
díla	dílo	k1gNnPc1	dílo
reformovat	reformovat	k5eAaBmF	reformovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
spěla	spět	k5eAaImAgFnS	spět
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
představě	představa	k1gFnSc3	představa
tzv.	tzv.	kA	tzv.
Gesamtkunstwerku	Gesamtkunstwerk	k1gInSc6	Gesamtkunstwerk
<g/>
.	.	kIx.	.
</s>
<s>
Česky	česky	k6eAd1	česky
vyšla	vyjít	k5eAaPmAgFnS	vyjít
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
.	.	kIx.	.
</s>
<s>
Über	Über	k1gInSc1	Über
Staat	Staat	k1gInSc1	Staat
und	und	k?	und
Religion	religion	k1gInSc1	religion
(	(	kIx(	(
<g/>
1864	[number]	k4	1864
<g/>
)	)	kIx)	)
Beethoven	Beethoven	k1gMnSc1	Beethoven
(	(	kIx(	(
<g/>
1870	[number]	k4	1870
<g/>
)	)	kIx)	)
Was	Was	k1gFnSc1	Was
ist	ist	k?	ist
deutsch	deutsch	k1gInSc1	deutsch
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
1878	[number]	k4	1878
<g/>
)	)	kIx)	)
Religion	religion	k1gInSc1	religion
und	und	k?	und
Kunst	Kunst	k1gInSc1	Kunst
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
)	)	kIx)	)
</s>
