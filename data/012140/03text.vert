<p>
<s>
Mount	Mount	k1gMnSc1	Mount
Cook	Cook	k1gMnSc1	Cook
nebo	nebo	k8xC	nebo
Aoraki	Aoraki	k1gNnSc1	Aoraki
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
3724	[number]	k4	3724
m	m	kA	m
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vrchol	vrchol	k1gInSc1	vrchol
v	v	k7c6	v
Jižních	jižní	k2eAgFnPc6d1	jižní
Alpách	Alpy	k1gFnPc6	Alpy
<g/>
,	,	kIx,	,
pohoří	pohoří	k1gNnSc6	pohoří
táhnoucím	táhnoucí	k2eAgNnSc6d1	táhnoucí
se	se	k3xPyFc4	se
podél	podél	k7c2	podél
západního	západní	k2eAgNnSc2d1	západní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Jižního	jižní	k2eAgInSc2d1	jižní
ostrova	ostrov	k1gInSc2	ostrov
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
populární	populární	k2eAgFnSc7d1	populární
turistickou	turistický	k2eAgFnSc7d1	turistická
i	i	k8xC	i
horolezeckou	horolezecký	k2eAgFnSc7d1	horolezecká
destinací	destinace	k1gFnSc7	destinace
<g/>
.	.	kIx.	.
</s>
<s>
Lemují	lemovat	k5eAaImIp3nP	lemovat
ji	on	k3xPp3gFnSc4	on
Tasmanův	Tasmanův	k2eAgInSc1d1	Tasmanův
a	a	k8xC	a
Hookerův	Hookerův	k2eAgInSc1d1	Hookerův
ledovec	ledovec	k1gInSc1	ledovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Umístění	umístění	k1gNnSc1	umístění
==	==	k?	==
</s>
</p>
<p>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
Národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
Aoraki	Aorak	k1gFnSc2	Aorak
<g/>
/	/	kIx~	/
<g/>
Mount	Mount	k1gMnSc1	Mount
Cook	Cook	k1gMnSc1	Cook
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
byl	být	k5eAaImAgInS	být
oficiálně	oficiálně	k6eAd1	oficiálně
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
Národním	národní	k2eAgInSc7d1	národní
parkem	park	k1gInSc7	park
Westland	Westlanda	k1gFnPc2	Westlanda
a	a	k8xC	a
několika	několik	k4yIc7	několik
dalšími	další	k2eAgFnPc7d1	další
chráněnými	chráněný	k2eAgFnPc7d1	chráněná
oblastmi	oblast	k1gFnPc7	oblast
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
zapsán	zapsat	k5eAaPmNgMnS	zapsat
na	na	k7c6	na
Seznamu	seznam	k1gInSc6	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
společným	společný	k2eAgNnSc7d1	společné
označením	označení	k1gNnSc7	označení
Te	Te	k1gFnSc2	Te
Wahipounamu	Wahipounam	k1gInSc2	Wahipounam
–	–	k?	–
South	South	k1gMnSc1	South
West	West	k1gMnSc1	West
New	New	k1gMnSc1	New
Zealand	Zealanda	k1gFnPc2	Zealanda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Park	park	k1gInSc1	park
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
140	[number]	k4	140
vrcholů	vrchol	k1gInPc2	vrchol
<g/>
,	,	kIx,	,
tyčících	tyčící	k2eAgInPc2d1	tyčící
se	se	k3xPyFc4	se
výš	vysoce	k6eAd2	vysoce
nežli	nežli	k8xS	nežli
2	[number]	k4	2
000	[number]	k4	000
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
a	a	k8xC	a
72	[number]	k4	72
pojmenovaných	pojmenovaný	k2eAgInPc2d1	pojmenovaný
ledovců	ledovec	k1gInPc2	ledovec
<g/>
,	,	kIx,	,
pokrývajících	pokrývající	k2eAgInPc2d1	pokrývající
40	[number]	k4	40
%	%	kIx~	%
ze	z	k7c2	z
700	[number]	k4	700
kilometrů	kilometr	k1gInPc2	kilometr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
zde	zde	k6eAd1	zde
přes	přes	k7c4	přes
léto	léto	k1gNnSc4	léto
trénuje	trénovat	k5eAaImIp3nS	trénovat
rakouská	rakouský	k2eAgFnSc1d1	rakouská
lyžařská	lyžařský	k2eAgFnSc1d1	lyžařská
reprezentace	reprezentace	k1gFnSc1	reprezentace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vesnice	vesnice	k1gFnSc1	vesnice
Mount	Mount	k1gInSc1	Mount
Cook	Cook	k1gMnSc1	Cook
Village	Village	k1gInSc1	Village
(	(	kIx(	(
<g/>
také	také	k9	také
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
The	The	k1gFnSc1	The
Hermitage	Hermitage	k1gFnSc1	Hermitage
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Poustevna	poustevna	k1gFnSc1	poustevna
<g/>
)	)	kIx)	)
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
turistické	turistický	k2eAgNnSc1d1	turistické
centrum	centrum	k1gNnSc1	centrum
a	a	k8xC	a
základní	základní	k2eAgInSc1d1	základní
kemp	kemp	k1gInSc1	kemp
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
4	[number]	k4	4
km	km	kA	km
od	od	k7c2	od
úpatí	úpatí	k1gNnSc2	úpatí
Tasmánského	tasmánský	k2eAgInSc2d1	tasmánský
ledovce	ledovec	k1gInSc2	ledovec
<g/>
,	,	kIx,	,
12	[number]	k4	12
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
vrcholu	vrchol	k1gInSc2	vrchol
Aoraki	Aorak	k1gFnSc2	Aorak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jméno	jméno	k1gNnSc1	jméno
==	==	k?	==
</s>
</p>
<p>
<s>
Aoraki	Aoraki	k6eAd1	Aoraki
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
napichovač	napichovač	k1gInSc4	napichovač
mraků	mrak	k1gInPc2	mrak
<g/>
"	"	kIx"	"
v	v	k7c6	v
Kai	Kai	k1gFnSc6	Kai
Tahu	tah	k1gInSc2	tah
dialektu	dialekt	k1gInSc2	dialekt
maorštiny	maorština	k1gFnSc2	maorština
<g/>
.	.	kIx.	.
</s>
<s>
Anglické	anglický	k2eAgNnSc1d1	anglické
jméno	jméno	k1gNnSc1	jméno
oslavuje	oslavovat	k5eAaImIp3nS	oslavovat
kapitána	kapitán	k1gMnSc2	kapitán
James	James	k1gMnSc1	James
Cooka	Cooka	k1gMnSc1	Cooka
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
první	první	k4xOgMnSc1	první
objevil	objevit	k5eAaPmAgMnS	objevit
a	a	k8xC	a
obeplul	obeplout	k5eAaPmAgInS	obeplout
ostrovy	ostrov	k1gInPc4	ostrov
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1770	[number]	k4	1770
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mount	Mounta	k1gFnPc2	Mounta
Cook	Cooko	k1gNnPc2	Cooko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
