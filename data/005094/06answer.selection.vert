<s>
Čínské	čínský	k2eAgNnSc1d1	čínské
písmo	písmo	k1gNnSc1	písmo
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
starověká	starověký	k2eAgNnPc4d1	starověké
písma	písmo	k1gNnPc4	písmo
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejdéle	dlouho	k6eAd3	dlouho
soustavně	soustavně	k6eAd1	soustavně
používané	používaný	k2eAgNnSc4d1	používané
písmo	písmo	k1gNnSc4	písmo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
