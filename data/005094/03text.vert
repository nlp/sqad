<s>
Čínské	čínský	k2eAgInPc1d1	čínský
znaky	znak	k1gInPc1	znak
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
<g/>
:	:	kIx,	:
漢	漢	k?	漢
<g/>
,	,	kIx,	,
汉	汉	k?	汉
<g/>
,	,	kIx,	,
pinyin	pinyin	k1gInSc1	pinyin
<g/>
:	:	kIx,	:
Hà	Hà	k1gFnSc1	Hà
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
přepis	přepis	k1gInSc1	přepis
<g/>
:	:	kIx,	:
Chan-c	Chan	k1gInSc1	Chan-c
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
Kandži	Kandž	k1gFnSc3	Kandž
漢	漢	k?	漢
<g/>
,	,	kIx,	,
korejsky	korejsky	k6eAd1	korejsky
<g/>
:	:	kIx,	:
Hanča	Hanča	k1gFnSc1	Hanča
한	한	k?	한
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
písemná	písemný	k2eAgFnSc1d1	písemná
soustava	soustava	k1gFnSc1	soustava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
používána	používat	k5eAaImNgFnS	používat
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
čínských	čínský	k2eAgInPc2d1	čínský
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
čínštiny	čínština	k1gFnPc1	čínština
<g/>
,	,	kIx,	,
kantonštiny	kantonština	k1gFnPc1	kantonština
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
japonštiny	japonština	k1gFnSc2	japonština
<g/>
,	,	kIx,	,
příležitostně	příležitostně	k6eAd1	příležitostně
korejštiny	korejština	k1gFnSc2	korejština
a	a	k8xC	a
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
též	tenž	k3xDgFnSc2	tenž
vietnamštiny	vietnamština	k1gFnSc2	vietnamština
<g/>
.	.	kIx.	.
</s>
<s>
Čínské	čínský	k2eAgNnSc1d1	čínské
písmo	písmo	k1gNnSc1	písmo
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
starověká	starověký	k2eAgNnPc4d1	starověké
písma	písmo	k1gNnPc4	písmo
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejdéle	dlouho	k6eAd3	dlouho
soustavně	soustavně	k6eAd1	soustavně
používané	používaný	k2eAgNnSc4d1	používané
písmo	písmo	k1gNnSc4	písmo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
Cchang-ťieovi	Cchang-ťieus	k1gMnSc6	Cchang-ťieus
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
jeho	on	k3xPp3gInSc4	on
vznik	vznik	k1gInSc4	vznik
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
verze	verze	k1gFnSc2	verze
této	tento	k3xDgFnSc2	tento
legendy	legenda	k1gFnSc2	legenda
byly	být	k5eAaImAgInP	být
čínské	čínský	k2eAgInPc1d1	čínský
znaky	znak	k1gInPc1	znak
vynalezeny	vynalezen	k2eAgInPc1d1	vynalezen
<g/>
,	,	kIx,	,
když	když	k8xS	když
Cchang-ťie	Cchang-ťie	k1gFnSc1	Cchang-ťie
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
2650	[number]	k4	2650
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byrokrat	byrokrat	k1gMnSc1	byrokrat
za	za	k7c4	za
legendárního	legendární	k2eAgMnSc4d1	legendární
Žlutého	žlutý	k2eAgMnSc4d1	žlutý
císaře	císař	k1gMnSc4	císař
<g/>
,	,	kIx,	,
na	na	k7c6	na
hoře	hora	k1gFnSc6	hora
Jang-sü	Jangü	k1gFnSc2	Jang-sü
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Šan-si	Šane	k1gFnSc6	Šan-se
<g/>
)	)	kIx)	)
lovil	lovit	k5eAaImAgMnS	lovit
zvěř	zvěř	k1gFnSc4	zvěř
<g/>
,	,	kIx,	,
když	když	k8xS	když
uviděl	uvidět	k5eAaPmAgMnS	uvidět
želvu	želva	k1gFnSc4	želva
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc2	jejíž
žíly	žíla	k1gFnSc2	žíla
přitáhly	přitáhnout	k5eAaPmAgFnP	přitáhnout
jeho	jeho	k3xOp3gFnSc4	jeho
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
ho	on	k3xPp3gNnSc4	on
možná	možný	k2eAgFnSc1d1	možná
logika	logika	k1gFnSc1	logika
skrytá	skrytý	k2eAgFnSc1d1	skrytá
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
žílách	žíla	k1gFnPc6	žíla
a	a	k8xC	a
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
studovat	studovat	k5eAaImF	studovat
přírodu	příroda	k1gFnSc4	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Prozkoumal	prozkoumat	k5eAaPmAgMnS	prozkoumat
zvířata	zvíře	k1gNnPc4	zvíře
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
krajiny	krajina	k1gFnSc2	krajina
světa	svět	k1gInSc2	svět
i	i	k8xC	i
hvězdy	hvězda	k1gFnSc2	hvězda
na	na	k7c6	na
nebi	nebe	k1gNnSc6	nebe
a	a	k8xC	a
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
první	první	k4xOgInSc4	první
symbolický	symbolický	k2eAgInSc4d1	symbolický
systém	systém	k1gInSc4	systém
zvaný	zvaný	k2eAgInSc4d1	zvaný
C	C	kA	C
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
字	字	k?	字
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc6	první
čínským	čínský	k2eAgNnSc7d1	čínské
písmem	písmo	k1gNnSc7	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
toho	ten	k3xDgInSc2	ten
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
znaky	znak	k1gInPc7	znak
zrodily	zrodit	k5eAaPmAgInP	zrodit
<g/>
,	,	kIx,	,
čínský	čínský	k2eAgInSc1d1	čínský
lid	lid	k1gInSc1	lid
slyšel	slyšet	k5eAaImAgInS	slyšet
truchlení	truchlení	k1gNnSc4	truchlení
ďábla	ďábel	k1gMnSc2	ďábel
a	a	k8xC	a
viděl	vidět	k5eAaImAgMnS	vidět
obilí	obilí	k1gNnSc4	obilí
padat	padat	k5eAaImF	padat
z	z	k7c2	z
nebes	nebesa	k1gNnPc2	nebesa
jako	jako	k8xS	jako
déšť	déšť	k1gInSc1	déšť
–	–	k?	–
to	ten	k3xDgNnSc1	ten
označovalo	označovat	k5eAaImAgNnS	označovat
"	"	kIx"	"
<g/>
druhý	druhý	k4xOgInSc1	druhý
vznik	vznik	k1gInSc1	vznik
světa	svět	k1gInSc2	svět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čínských	čínský	k2eAgInPc6d1	čínský
znacích	znak	k1gInPc6	znak
se	se	k3xPyFc4	se
neodráží	odrážet	k5eNaImIp3nS	odrážet
výslovnost	výslovnost	k1gFnSc1	výslovnost
tak	tak	k6eAd1	tak
přímým	přímý	k2eAgInSc7d1	přímý
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
latinky	latinka	k1gFnSc2	latinka
v	v	k7c6	v
případech	případ	k1gInPc6	případ
mnoha	mnoho	k4c2	mnoho
jazyků	jazyk	k1gMnPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
morfém	morfém	k1gInSc4	morfém
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
slova	slovo	k1gNnSc2	slovo
<g/>
)	)	kIx)	)
existuje	existovat	k5eAaImIp3nS	existovat
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
grafém	grafém	k1gInSc1	grafém
(	(	kIx(	(
<g/>
grafický	grafický	k2eAgInSc1d1	grafický
prvek	prvek	k1gInSc1	prvek
-	-	kIx~	-
znak	znak	k1gInSc1	znak
<g/>
,	,	kIx,	,
znaménko	znaménko	k1gNnSc1	znaménko
<g/>
,	,	kIx,	,
značka	značka	k1gFnSc1	značka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhody	nevýhoda	k1gFnPc1	nevýhoda
Tento	tento	k3xDgInSc4	tento
fakt	fakt	k1gInSc4	fakt
samozřejmě	samozřejmě	k6eAd1	samozřejmě
značně	značně	k6eAd1	značně
ztěžuje	ztěžovat	k5eAaImIp3nS	ztěžovat
výuku	výuka	k1gFnSc4	výuka
čtení	čtení	k1gNnSc3	čtení
a	a	k8xC	a
psaní	psaní	k1gNnSc3	psaní
jak	jak	k8xS	jak
u	u	k7c2	u
cizinců	cizinec	k1gMnPc2	cizinec
<g/>
,	,	kIx,	,
tak	tak	k9	tak
u	u	k7c2	u
Číňanů	Číňan	k1gMnPc2	Číňan
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
velkou	velký	k2eAgFnSc7d1	velká
brzdou	brzda	k1gFnSc7	brzda
gramotnosti	gramotnost	k1gFnSc2	gramotnost
<g/>
:	:	kIx,	:
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
evropských	evropský	k2eAgInPc2d1	evropský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
počet	počet	k1gInSc1	počet
znaků	znak	k1gInPc2	znak
obvykle	obvykle	k6eAd1	obvykle
nepřesahuje	přesahovat	k5eNaImIp3nS	přesahovat
50	[number]	k4	50
<g/>
,	,	kIx,	,
čínských	čínský	k2eAgMnPc2d1	čínský
znaků	znak	k1gInPc2	znak
jsou	být	k5eAaImIp3nP	být
tisíce	tisíc	k4xCgInPc1	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
přibližně	přibližně	k6eAd1	přibližně
5000	[number]	k4	5000
<g/>
–	–	k?	–
<g/>
6000	[number]	k4	6000
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc1d3	veliký
slovníky	slovník	k1gInPc1	slovník
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
kolem	kolem	k7c2	kolem
80	[number]	k4	80
000	[number]	k4	000
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
drtivá	drtivý	k2eAgFnSc1d1	drtivá
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
však	však	k9	však
dávno	dávno	k6eAd1	dávno
nepoužívána	používán	k2eNgFnSc1d1	nepoužívána
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
varianty	varianta	k1gFnPc4	varianta
(	(	kIx(	(
<g/>
až	až	k9	až
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výhody	výhoda	k1gFnPc1	výhoda
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
i	i	k9	i
nesporné	sporný	k2eNgFnPc4d1	nesporná
výhody	výhoda	k1gFnPc4	výhoda
<g/>
:	:	kIx,	:
v	v	k7c6	v
jazykově	jazykově	k6eAd1	jazykově
roztříštěné	roztříštěný	k2eAgFnSc6d1	roztříštěná
Číně	Čína	k1gFnSc6	Čína
se	se	k3xPyFc4	se
spousta	spousta	k1gFnSc1	spousta
vzájemně	vzájemně	k6eAd1	vzájemně
nesrozumitelných	srozumitelný	k2eNgFnPc2d1	nesrozumitelná
(	(	kIx(	(
<g/>
byť	byť	k8xS	byť
příbuzných	příbuzný	k2eAgInPc2d1	příbuzný
<g/>
)	)	kIx)	)
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
dialektů	dialekt	k1gInPc2	dialekt
píše	psát	k5eAaImIp3nS	psát
těmi	ten	k3xDgMnPc7	ten
samými	samý	k3xTgInPc7	samý
čínskými	čínský	k2eAgInPc7d1	čínský
znaky	znak	k1gInPc7	znak
<g/>
.	.	kIx.	.
</s>
<s>
Mluvčí	mluvčí	k1gMnSc1	mluvčí
kantonštiny	kantonština	k1gFnSc2	kantonština
se	se	k3xPyFc4	se
nedomluví	domluvit	k5eNaPmIp3nS	domluvit
s	s	k7c7	s
mluvčím	mluvčí	k1gMnSc7	mluvčí
standardní	standardní	k2eAgFnSc2d1	standardní
čínštiny	čínština	k1gFnSc2	čínština
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
oba	dva	k4xCgMnPc1	dva
gramotní	gramotný	k2eAgMnPc1d1	gramotný
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
si	se	k3xPyFc3	se
psát	psát	k5eAaImF	psát
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
podobného	podobný	k2eAgInSc2d1	podobný
důvodu	důvod	k1gInSc2	důvod
také	také	k6eAd1	také
čínské	čínský	k2eAgFnPc1d1	čínská
televizní	televizní	k2eAgFnPc1d1	televizní
stanice	stanice	k1gFnPc1	stanice
vysílají	vysílat	k5eAaImIp3nP	vysílat
filmy	film	k1gInPc4	film
v	v	k7c6	v
čínském	čínský	k2eAgNnSc6d1	čínské
znění	znění	k1gNnSc6	znění
s	s	k7c7	s
čínskými	čínský	k2eAgInPc7d1	čínský
titulky	titulek	k1gInPc7	titulek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obdobné	obdobný	k2eAgFnSc6d1	obdobná
funkci	funkce	k1gFnSc6	funkce
jako	jako	k8xS	jako
lingua	lingu	k2eAgFnSc1d1	lingua
franca	franca	k1gFnSc1	franca
(	(	kIx(	(
<g/>
zprostředkující	zprostředkující	k2eAgInSc1d1	zprostředkující
jazyk	jazyk	k1gInSc1	jazyk
<g/>
)	)	kIx)	)
řadu	řad	k1gInSc2	řad
staletí	staletí	k1gNnSc2	staletí
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
užívána	užíván	k2eAgFnSc1d1	užívána
latina	latina	k1gFnSc1	latina
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
takto	takto	k6eAd1	takto
slouží	sloužit	k5eAaImIp3nS	sloužit
celosvětově	celosvětově	k6eAd1	celosvětově
angličtina	angličtina	k1gFnSc1	angličtina
a	a	k8xC	a
v	v	k7c6	v
muslimských	muslimský	k2eAgFnPc6d1	muslimská
zemích	zem	k1gFnPc6	zem
arabština	arabština	k1gFnSc1	arabština
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
obdobnou	obdobný	k2eAgFnSc4d1	obdobná
funkci	funkce	k1gFnSc4	funkce
plní	plnit	k5eAaImIp3nP	plnit
běžné	běžný	k2eAgInPc1d1	běžný
moderní	moderní	k2eAgInPc1d1	moderní
piktogramy	piktogram	k1gInPc1	piktogram
-	-	kIx~	-
symboly	symbol	k1gInPc4	symbol
<g/>
,	,	kIx,	,
emotikony	emotikon	k1gInPc4	emotikon
<g/>
,	,	kIx,	,
dopravní	dopravní	k2eAgFnSc2d1	dopravní
značky	značka	k1gFnSc2	značka
<g/>
,	,	kIx,	,
znázornění	znázornění	k1gNnSc2	znázornění
času	čas	k1gInSc2	čas
hodinami	hodina	k1gFnPc7	hodina
<g/>
,	,	kIx,	,
mezinárodně	mezinárodně	k6eAd1	mezinárodně
zavedené	zavedený	k2eAgInPc1d1	zavedený
oborové	oborový	k2eAgInPc1d1	oborový
znaky	znak	k1gInPc1	znak
a	a	k8xC	a
zkratky	zkratka	k1gFnPc1	zkratka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
měr	míra	k1gFnPc2	míra
a	a	k8xC	a
vah	váha	k1gFnPc2	váha
<g/>
,	,	kIx,	,
matematické	matematický	k2eAgInPc4d1	matematický
operátory	operátor	k1gInPc4	operátor
a	a	k8xC	a
číslice	číslice	k1gFnPc4	číslice
<g/>
,	,	kIx,	,
interpunkční	interpunkční	k2eAgInPc4d1	interpunkční
znaky	znak	k1gInPc4	znak
<g/>
,	,	kIx,	,
latinské	latinský	k2eAgNnSc1d1	latinské
názvosloví	názvosloví	k1gNnSc1	názvosloví
v	v	k7c6	v
biologii	biologie	k1gFnSc6	biologie
a	a	k8xC	a
lékařství	lékařství	k1gNnSc2	lékařství
atp.	atp.	kA	atp.
A	a	k8xC	a
totéž	týž	k3xTgNnSc1	týž
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
o	o	k7c6	o
srozumitelnosti	srozumitelnost	k1gFnSc6	srozumitelnost
dějinné	dějinný	k2eAgFnSc6d1	dějinná
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgInPc1d1	historický
varianty	variant	k1gInPc1	variant
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
liší	lišit	k5eAaImIp3nP	lišit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
navazují	navazovat	k5eAaImIp3nP	navazovat
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
zachovávají	zachovávat	k5eAaImIp3nP	zachovávat
si	se	k3xPyFc3	se
srozumitelnost	srozumitelnost	k1gFnSc4	srozumitelnost
alespoň	alespoň	k9	alespoň
částečnou	částečný	k2eAgFnSc4d1	částečná
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čínském	čínský	k2eAgNnSc6d1	čínské
písmu	písmo	k1gNnSc6	písmo
se	se	k3xPyFc4	se
nedělají	dělat	k5eNaImIp3nP	dělat
mezery	mezera	k1gFnPc1	mezera
mezi	mezi	k7c7	mezi
slovy	slovo	k1gNnPc7	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
slovo	slovo	k1gNnSc1	slovo
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
čínském	čínský	k2eAgNnSc6d1	čínské
prostředí	prostředí	k1gNnSc6	prostředí
jinou	jiný	k2eAgFnSc4d1	jiná
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
:	:	kIx,	:
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
koncept	koncept	k1gInSc4	koncept
čistě	čistě	k6eAd1	čistě
jazykový	jazykový	k2eAgMnSc1d1	jazykový
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
pravopisný	pravopisný	k2eAgMnSc1d1	pravopisný
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
znakovém	znakový	k2eAgInSc6d1	znakový
zápisu	zápis	k1gInSc6	zápis
je	být	k5eAaImIp3nS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
jednotkou	jednotka	k1gFnSc7	jednotka
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
věta	věta	k1gFnSc1	věta
znak	znak	k1gInSc1	znak
<g/>
;	;	kIx,	;
ten	ten	k3xDgMnSc1	ten
ve	v	k7c6	v
výslovnosti	výslovnost	k1gFnSc6	výslovnost
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
jedné	jeden	k4xCgFnSc3	jeden
slabice	slabika	k1gFnSc3	slabika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
významové	významový	k2eAgFnSc6d1	významová
úrovni	úroveň	k1gFnSc6	úroveň
to	ten	k3xDgNnSc1	ten
pak	pak	k6eAd1	pak
zpravidla	zpravidla	k6eAd1	zpravidla
bývá	bývat	k5eAaImIp3nS	bývat
morfém	morfém	k1gInSc1	morfém
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
to	ten	k3xDgNnSc1	ten
zdaleka	zdaleka	k6eAd1	zdaleka
není	být	k5eNaImIp3nS	být
univerzální	univerzální	k2eAgNnSc1d1	univerzální
pravidlo	pravidlo	k1gNnSc1	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
řada	řada	k1gFnSc1	řada
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
samy	sám	k3xTgInPc1	sám
o	o	k7c4	o
sobě	se	k3xPyFc3	se
žádný	žádný	k3yNgInSc4	žádný
význam	význam	k1gInSc4	význam
nenesou	nést	k5eNaImIp3nP	nést
a	a	k8xC	a
získávají	získávat	k5eAaImIp3nP	získávat
jej	on	k3xPp3gInSc4	on
až	až	k9	až
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
se	s	k7c7	s
znakem	znak	k1gInSc7	znak
jiným	jiný	k2eAgInSc7d1	jiný
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
znaky	znak	k1gInPc1	znak
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgNnPc6	jenž
jsou	být	k5eAaImIp3nP	být
obsaženy	obsažen	k2eAgInPc1d1	obsažen
morfémy	morfém	k1gInPc1	morfém
dva	dva	k4xCgInPc1	dva
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
čínština	čínština	k1gFnSc1	čínština
již	již	k6eAd1	již
dávno	dávno	k6eAd1	dávno
není	být	k5eNaImIp3nS	být
jazykem	jazyk	k1gInSc7	jazyk
jednoslabičným	jednoslabičný	k2eAgInSc7d1	jednoslabičný
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
slov	slovo	k1gNnPc2	slovo
je	být	k5eAaImIp3nS	být
dvojslabičná	dvojslabičný	k2eAgFnSc1d1	dvojslabičná
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
čínské	čínský	k2eAgNnSc1d1	čínské
písmo	písmo	k1gNnSc1	písmo
není	být	k5eNaImIp3nS	být
písmem	písmo	k1gNnSc7	písmo
hláskovým	hláskový	k2eAgNnSc7d1	hláskové
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
obtížné	obtížný	k2eAgNnSc1d1	obtížné
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
zachytit	zachytit	k5eAaPmF	zachytit
výslovnost	výslovnost	k1gFnSc1	výslovnost
cizích	cizí	k2eAgNnPc2d1	cizí
slov	slovo	k1gNnPc2	slovo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
osobních	osobní	k2eAgNnPc2d1	osobní
jmen	jméno	k1gNnPc2	jméno
a	a	k8xC	a
zeměpisných	zeměpisný	k2eAgInPc2d1	zeměpisný
názvů	název	k1gInPc2	název
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výslovnost	výslovnost	k1gFnSc1	výslovnost
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
napodobit	napodobit	k5eAaPmF	napodobit
pomocí	pomocí	k7c2	pomocí
slabik	slabika	k1gFnPc2	slabika
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
standardní	standardní	k2eAgFnSc1d1	standardní
čínština	čínština	k1gFnSc1	čínština
<g/>
,	,	kIx,	,
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
fonetický	fonetický	k2eAgInSc1d1	fonetický
systém	systém	k1gInSc1	systém
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
kontextu	kontext	k1gInSc6	kontext
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
velmi	velmi	k6eAd1	velmi
omezený	omezený	k2eAgInSc1d1	omezený
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
jako	jako	k9	jako
např.	např.	kA	např.
česká	český	k2eAgFnSc1d1	Česká
soustava	soustava	k1gFnSc1	soustava
hlásek	hláska	k1gFnPc2	hláska
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nedostatečná	dostatečný	k2eNgFnSc1d1	nedostatečná
pro	pro	k7c4	pro
napodobení	napodobení	k1gNnSc4	napodobení
čínštiny	čínština	k1gFnSc2	čínština
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
dotyčné	dotyčný	k2eAgFnSc2d1	dotyčná
posloupnosti	posloupnost	k1gFnSc2	posloupnost
slabik	slabika	k1gFnPc2	slabika
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vyberou	vybrat	k5eAaPmIp3nP	vybrat
vhodné	vhodný	k2eAgInPc4d1	vhodný
znaky	znak	k1gInPc4	znak
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgFnSc3	každý
slabice	slabika	k1gFnSc3	slabika
sice	sice	k8xC	sice
v	v	k7c6	v
čínském	čínský	k2eAgNnSc6d1	čínské
písmu	písmo	k1gNnSc6	písmo
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
mnoho	mnoho	k4c1	mnoho
různých	různý	k2eAgInPc2d1	různý
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc4	některý
znaky	znak	k1gInPc4	znak
lze	lze	k6eAd1	lze
ale	ale	k8xC	ale
prohlásit	prohlásit	k5eAaPmF	prohlásit
za	za	k7c4	za
typické	typický	k2eAgInPc4d1	typický
prostředky	prostředek	k1gInPc4	prostředek
k	k	k7c3	k
fonetické	fonetický	k2eAgFnSc3d1	fonetická
transkripci	transkripce	k1gFnSc3	transkripce
cizích	cizí	k2eAgNnPc2d1	cizí
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
加	加	k?	加
=	=	kIx~	=
Ťia-na-ta	Ťiaaa	k1gMnSc1	Ťia-na-ta
=	=	kIx~	=
"	"	kIx"	"
<g/>
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
澳	澳	k?	澳
=	=	kIx~	=
Ao-ta	Aoa	k1gMnSc1	Ao-ta
<g/>
-li	i	k?	-li
<g/>
-ja	a	k?	-ja
=	=	kIx~	=
"	"	kIx"	"
<g/>
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
význam	význam	k1gInSc1	význam
znaků	znak	k1gInPc2	znak
přitom	přitom	k6eAd1	přitom
nedává	dávat	k5eNaImIp3nS	dávat
smysl	smysl	k1gInSc4	smysl
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
zátoka	zátoka	k1gFnSc1	zátoka
<g/>
+	+	kIx~	+
<g/>
velký	velký	k2eAgInSc1d1	velký
<g/>
+	+	kIx~	+
<g/>
zisk	zisk	k1gInSc1	zisk
<g/>
+	+	kIx~	+
<g/>
Asie	Asie	k1gFnSc2	Asie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
čínské	čínský	k2eAgInPc1d1	čínský
znaky	znak	k1gInPc1	znak
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
z	z	k7c2	z
obrázkového	obrázkový	k2eAgNnSc2d1	obrázkové
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
každý	každý	k3xTgInSc1	každý
znak	znak	k1gInSc1	znak
opravdu	opravdu	k6eAd1	opravdu
vystihoval	vystihovat	k5eAaImAgInS	vystihovat
zjednodušenou	zjednodušený	k2eAgFnSc4d1	zjednodušená
podobu	podoba	k1gFnSc4	podoba
pojmu	pojem	k1gInSc2	pojem
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zapisoval	zapisovat	k5eAaImAgInS	zapisovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
současném	současný	k2eAgNnSc6d1	současné
písmu	písmo	k1gNnSc6	písmo
takových	takový	k3xDgInPc2	takový
znaků	znak	k1gInPc2	znak
zbyla	zbýt	k5eAaPmAgFnS	zbýt
jen	jen	k9	jen
hrstka	hrstka	k1gFnSc1	hrstka
(	(	kIx(	(
<g/>
a	a	k8xC	a
i	i	k9	i
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
obtížné	obtížný	k2eAgNnSc1d1	obtížné
původní	původní	k2eAgNnSc1d1	původní
obrázek	obrázek	k1gInSc4	obrázek
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Drtivá	drtivý	k2eAgFnSc1d1	drtivá
většina	většina	k1gFnSc1	většina
dnes	dnes	k6eAd1	dnes
používaných	používaný	k2eAgInPc2d1	používaný
znaků	znak	k1gInPc2	znak
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
determinativu	determinativ	k1gInSc2	determinativ
a	a	k8xC	a
z	z	k7c2	z
fonetika	fonetik	k1gMnSc2	fonetik
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
přes	přes	k7c4	přes
500	[number]	k4	500
různých	různý	k2eAgInPc2d1	různý
determinativů	determinativ	k1gInPc2	determinativ
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
determinativ	determinativ	k1gInSc1	determinativ
zaměňován	zaměňován	k2eAgInSc1d1	zaměňován
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
radikálem	radikál	k1gInSc7	radikál
<g/>
.	.	kIx.	.
</s>
<s>
Radikály	radikál	k1gInPc1	radikál
jsou	být	k5eAaImIp3nP	být
prvky	prvek	k1gInPc4	prvek
znaku	znak	k1gInSc2	znak
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nichž	jenž	k3xRgFnPc2	jenž
jsou	být	k5eAaImIp3nP	být
znaky	znak	k1gInPc1	znak
řazeny	řazen	k2eAgInPc1d1	řazen
ve	v	k7c6	v
slovníku	slovník	k1gInSc6	slovník
<g/>
,	,	kIx,	,
a	a	k8xC	a
díky	díky	k7c3	díky
dějinným	dějinný	k2eAgFnPc3d1	dějinná
okolnostem	okolnost	k1gFnPc3	okolnost
se	se	k3xPyFc4	se
velice	velice	k6eAd1	velice
často	často	k6eAd1	často
s	s	k7c7	s
determinativem	determinativ	k1gInSc7	determinativ
kryje	krýt	k5eAaImIp3nS	krýt
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
však	však	k9	však
najdeme	najít	k5eAaPmIp1nP	najít
mnoho	mnoho	k4c4	mnoho
případů	případ	k1gInPc2	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
radikálem	radikál	k1gInSc7	radikál
zcela	zcela	k6eAd1	zcela
jiná	jiný	k2eAgFnSc1d1	jiná
složka	složka	k1gFnSc1	složka
znaku	znak	k1gInSc2	znak
než	než	k8xS	než
determinativ	determinativ	k1gInSc4	determinativ
<g/>
.	.	kIx.	.
</s>
<s>
Znaky	znak	k1gInPc1	znak
se	s	k7c7	s
stejným	stejný	k2eAgInSc7d1	stejný
determinativem	determinativ	k1gInSc7	determinativ
obvykle	obvykle	k6eAd1	obvykle
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
příbuzné	příbuzný	k2eAgInPc1d1	příbuzný
pojmy	pojem	k1gInPc1	pojem
<g/>
:	:	kIx,	:
např.	např.	kA	např.
znaky	znak	k1gInPc1	znak
pro	pro	k7c4	pro
řeku	řeka	k1gFnSc4	řeka
(	(	kIx(	(
<g/>
河	河	k?	河
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jezero	jezero	k1gNnSc1	jezero
(	(	kIx(	(
<g/>
湖	湖	k?	湖
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
moře	moře	k1gNnSc1	moře
(	(	kIx(	(
<g/>
海	海	k?	海
<g/>
)	)	kIx)	)
i	i	k8xC	i
zátoku	zátoka	k1gFnSc4	zátoka
(	(	kIx(	(
<g/>
灣	灣	k?	灣
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
determinativ	determinativ	k1gInSc4	determinativ
(	(	kIx(	(
<g/>
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
radikálem	radikál	k1gInSc7	radikál
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
zjednodušením	zjednodušení	k1gNnSc7	zjednodušení
znaku	znak	k1gInSc2	znak
pro	pro	k7c4	pro
vodu	voda	k1gFnSc4	voda
(	(	kIx(	(
<g/>
水	水	k?	水
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Sám	sám	k3xTgInSc1	sám
znak	znak	k1gInSc1	znak
vody	voda	k1gFnSc2	voda
se	se	k3xPyFc4	se
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
také	také	k9	také
počítá	počítat	k5eAaImIp3nS	počítat
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
chybí	chybět	k5eAaImIp3nS	chybět
fonetická	fonetický	k2eAgFnSc1d1	fonetická
složka	složka	k1gFnSc1	složka
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Fonetiku	fonetika	k1gFnSc4	fonetika
(	(	kIx(	(
<g/>
fonetickou	fonetický	k2eAgFnSc4d1	fonetická
složku	složka	k1gFnSc4	složka
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nP	tvořit
jiný	jiný	k2eAgInSc4d1	jiný
znak	znak	k1gInSc4	znak
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
výslovnost	výslovnost	k1gFnSc1	výslovnost
je	být	k5eAaImIp3nS	být
nebo	nebo	k8xC	nebo
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byla	být	k5eAaImAgFnS	být
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
blízká	blízký	k2eAgFnSc1d1	blízká
výslednému	výsledný	k2eAgInSc3d1	výsledný
složenému	složený	k2eAgInSc3d1	složený
znaku	znak	k1gInSc3	znak
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
však	však	k9	však
výslovnost	výslovnost	k1gFnSc1	výslovnost
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
Číny	Čína	k1gFnSc2	Čína
dosti	dosti	k6eAd1	dosti
odlišná	odlišný	k2eAgFnSc1d1	odlišná
<g/>
,	,	kIx,	,
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
případů	případ	k1gInPc2	případ
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
obtížné	obtížný	k2eAgInPc1d1	obtížný
fonetickou	fonetický	k2eAgFnSc4d1	fonetická
podobnost	podobnost	k1gFnSc4	podobnost
vysledovat	vysledovat	k5eAaImF	vysledovat
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
znaky	znak	k1gInPc1	znak
tvořící	tvořící	k2eAgFnSc4d1	tvořící
fonetickou	fonetický	k2eAgFnSc4d1	fonetická
složku	složka	k1gFnSc4	složka
jsou	být	k5eAaImIp3nP	být
dost	dost	k6eAd1	dost
složité	složitý	k2eAgInPc1d1	složitý
a	a	k8xC	a
samy	sám	k3xTgInPc1	sám
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
radikálu	radikál	k1gInSc2	radikál
a	a	k8xC	a
své	svůj	k3xOyFgFnSc2	svůj
fonetické	fonetický	k2eAgFnSc2d1	fonetická
složky	složka	k1gFnSc2	složka
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
se	se	k3xPyFc4	se
znaky	znak	k1gInPc1	znak
kladly	klást	k5eAaImAgInP	klást
do	do	k7c2	do
sloupce	sloupec	k1gInSc2	sloupec
shora	shora	k6eAd1	shora
dolů	dolů	k6eAd1	dolů
poměrně	poměrně	k6eAd1	poměrně
těsně	těsně	k6eAd1	těsně
<g/>
,	,	kIx,	,
sloupce	sloupec	k1gInPc1	sloupec
se	se	k3xPyFc4	se
řadily	řadit	k5eAaImAgInP	řadit
zprava	zprava	k6eAd1	zprava
doleva	doleva	k6eAd1	doleva
s	s	k7c7	s
malým	malý	k2eAgInSc7d1	malý
odstupem	odstup	k1gInSc7	odstup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moderních	moderní	k2eAgInPc6d1	moderní
textech	text	k1gInPc6	text
je	být	k5eAaImIp3nS	být
běžné	běžný	k2eAgNnSc1d1	běžné
totéž	týž	k3xTgNnSc4	týž
řazení	řazení	k1gNnSc4	řazení
jako	jako	k9	jako
v	v	k7c6	v
latince	latinka	k1gFnSc6	latinka
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
do	do	k7c2	do
řádků	řádek	k1gInPc2	řádek
zleva	zleva	k6eAd1	zleva
doprava	doprava	k6eAd1	doprava
<g/>
,	,	kIx,	,
řádky	řádek	k1gInPc1	řádek
přibývají	přibývat	k5eAaImIp3nP	přibývat
shora	shora	k6eAd1	shora
dolů	dolů	k6eAd1	dolů
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
složitost	složitost	k1gFnSc4	složitost
znaku	znak	k1gInSc2	znak
se	se	k3xPyFc4	se
všechny	všechen	k3xTgFnPc1	všechen
grafické	grafický	k2eAgFnPc1d1	grafická
složky	složka	k1gFnPc1	složka
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
znaku	znak	k1gInSc6	znak
musí	muset	k5eAaImIp3nS	muset
vejít	vejít	k5eAaPmF	vejít
do	do	k7c2	do
čtverce	čtverec	k1gInSc2	čtverec
o	o	k7c6	o
jednotné	jednotný	k2eAgFnSc6d1	jednotná
velikosti	velikost	k1gFnSc6	velikost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
by	by	kYmCp3nP	by
byly	být	k5eAaImAgFnP	být
vpisovány	vpisovat	k5eAaImNgFnP	vpisovat
do	do	k7c2	do
čtverečkovaného	čtverečkovaný	k2eAgInSc2d1	čtverečkovaný
rastru	rastr	k1gInSc2	rastr
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nalezení	nalezení	k1gNnSc4	nalezení
čínského	čínský	k2eAgInSc2d1	čínský
znaku	znak	k1gInSc2	znak
ve	v	k7c6	v
slovníku	slovník	k1gInSc6	slovník
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
umět	umět	k5eAaImF	umět
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
dvě	dva	k4xCgFnPc1	dva
okolnosti	okolnost	k1gFnPc1	okolnost
<g/>
:	:	kIx,	:
tzv.	tzv.	kA	tzv.
radikál	radikál	k1gInSc4	radikál
znaku	znak	k1gInSc2	znak
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
základní	základní	k2eAgInSc1d1	základní
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
znak	znak	k1gInSc1	znak
<g/>
,	,	kIx,	,
a	a	k8xC	a
umět	umět	k5eAaImF	umět
poznat	poznat	k5eAaPmF	poznat
<g/>
,	,	kIx,	,
kolika	kolik	k4yQc2	kolik
tahy	tah	k1gInPc4	tah
se	se	k3xPyFc4	se
znak	znak	k1gInSc1	znak
píše	psát	k5eAaImIp3nS	psát
-	-	kIx~	-
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgInPc2	který
se	se	k3xPyFc4	se
čínský	čínský	k2eAgInSc1d1	čínský
znak	znak	k1gInSc1	znak
skládá	skládat	k5eAaImIp3nS	skládat
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
jednotný	jednotný	k2eAgInSc4d1	jednotný
počet	počet	k1gInSc4	počet
tahů	tah	k1gInPc2	tah
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
se	se	k3xPyFc4	se
mají	mít	k5eAaImIp3nP	mít
psát	psát	k5eAaImF	psát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
slovníku	slovník	k1gInSc2	slovník
je	být	k5eAaImIp3nS	být
tabulka	tabulka	k1gFnSc1	tabulka
radikálů	radikál	k1gMnPc2	radikál
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
uspořádané	uspořádaný	k2eAgFnPc1d1	uspořádaná
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
tahů	tah	k1gInPc2	tah
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
každého	každý	k3xTgInSc2	každý
radikálu	radikál	k1gInSc2	radikál
je	být	k5eAaImIp3nS	být
uveden	uveden	k2eAgInSc1d1	uveden
seznam	seznam	k1gInSc1	seznam
znaků	znak	k1gInPc2	znak
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
radikálem	radikál	k1gInSc7	radikál
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
slovník	slovník	k1gInSc1	slovník
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
<g/>
;	;	kIx,	;
tyto	tento	k3xDgInPc1	tento
znaky	znak	k1gInPc1	znak
jsou	být	k5eAaImIp3nP	být
uspořádány	uspořádat	k5eAaPmNgInP	uspořádat
opět	opět	k6eAd1	opět
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
tahů	tah	k1gInPc2	tah
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
i	i	k9	i
znaků	znak	k1gInPc2	znak
se	s	k7c7	s
stejným	stejný	k2eAgInSc7d1	stejný
radikálem	radikál	k1gInSc7	radikál
a	a	k8xC	a
počtem	počet	k1gInSc7	počet
tahů	tah	k1gInPc2	tah
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
hodně	hodně	k6eAd1	hodně
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
příslušná	příslušný	k2eAgFnSc1d1	příslušná
část	část	k1gFnSc1	část
tabulky	tabulka	k1gFnSc2	tabulka
nadepsaná	nadepsaný	k2eAgFnSc1d1	nadepsaná
čínskou	čínský	k2eAgFnSc7d1	čínská
číslovkou	číslovka	k1gFnSc7	číslovka
<g/>
,	,	kIx,	,
vyjadřující	vyjadřující	k2eAgInSc1d1	vyjadřující
počet	počet	k1gInSc1	počet
tahů	tah	k1gInPc2	tah
fonetické	fonetický	k2eAgFnSc2d1	fonetická
složky	složka	k1gFnSc2	složka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
každého	každý	k3xTgInSc2	každý
znaku	znak	k1gInSc2	znak
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
radikálů	radikál	k1gMnPc2	radikál
je	být	k5eAaImIp3nS	být
nakonec	nakonec	k6eAd1	nakonec
odkaz	odkaz	k1gInSc4	odkaz
na	na	k7c4	na
příslušné	příslušný	k2eAgNnSc4d1	příslušné
heslo	heslo	k1gNnSc4	heslo
ve	v	k7c6	v
slovníku	slovník	k1gInSc6	slovník
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
jít	jít	k5eAaImF	jít
o	o	k7c4	o
číslo	číslo	k1gNnSc4	číslo
stránky	stránka	k1gFnSc2	stránka
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
je	být	k5eAaImIp3nS	být
odkazem	odkaz	k1gInSc7	odkaz
přepis	přepis	k1gInSc1	přepis
výslovnosti	výslovnost	k1gFnSc2	výslovnost
znaku	znak	k1gInSc2	znak
–	–	k?	–
hesla	heslo	k1gNnSc2	heslo
jsou	být	k5eAaImIp3nP	být
totiž	totiž	k9	totiž
ve	v	k7c6	v
slovnících	slovník	k1gInPc6	slovník
řazena	řazen	k2eAgFnSc1d1	řazena
ne	ne	k9	ne
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
tahů	tah	k1gInPc2	tah
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
právě	právě	k9	právě
podle	podle	k7c2	podle
přepisu	přepis	k1gInSc2	přepis
výslovnosti	výslovnost	k1gFnSc2	výslovnost
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
čínských	čínský	k2eAgInPc2d1	čínský
slovníků	slovník	k1gInPc2	slovník
vydaných	vydaný	k2eAgInPc2d1	vydaný
v	v	k7c6	v
ČLR	ČLR	kA	ČLR
a	a	k8xC	a
kdekoli	kdekoli	k6eAd1	kdekoli
mimo	mimo	k7c4	mimo
Čínu	Čína	k1gFnSc4	Čína
se	se	k3xPyFc4	se
výslovnost	výslovnost	k1gFnSc1	výslovnost
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
pomocí	pomocí	k7c2	pomocí
pchin-jinu	pchinin	k1gInSc2	pchin-jin
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Tchaj-wanu	Tchajan	k1gInSc2	Tchaj-wan
stejnému	stejný	k2eAgInSc3d1	stejný
účelu	účel	k1gInSc3	účel
slouží	sloužit	k5eAaImIp3nS	sloužit
starší	starý	k2eAgFnSc1d2	starší
čínská	čínský	k2eAgFnSc1d1	čínská
abeceda	abeceda	k1gFnSc1	abeceda
vymyšlená	vymyšlený	k2eAgFnSc1d1	vymyšlená
speciálně	speciálně	k6eAd1	speciálně
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
výslovnosti	výslovnost	k1gFnSc2	výslovnost
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
ču-jin	čuin	k1gInSc4	ču-jin
fu-chao	fuhao	k1gNnSc1	fu-chao
(	(	kIx(	(
<g/>
též	též	k9	též
Bopomofo	Bopomofo	k6eAd1	Bopomofo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řazení	řazení	k1gNnSc1	řazení
podle	podle	k7c2	podle
pchin-jinu	pchinin	k1gInSc2	pchin-jin
není	být	k5eNaImIp3nS	být
stejné	stejný	k2eAgNnSc1d1	stejné
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgNnSc1	jaký
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
evropských	evropský	k2eAgInPc6d1	evropský
jazycích	jazyk	k1gInPc6	jazyk
píšících	píšící	k2eAgFnPc2d1	píšící
latinkou	latinka	k1gFnSc7	latinka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Koreji	Korea	k1gFnSc6	Korea
se	se	k3xPyFc4	se
obdobně	obdobně	k6eAd1	obdobně
používá	používat	k5eAaImIp3nS	používat
hangul	hangul	k1gInSc4	hangul
a	a	k8xC	a
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
kana	kanout	k5eAaImSgInS	kanout
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
přiblížit	přiblížit	k5eAaPmF	přiblížit
písmo	písmo	k1gNnSc4	písmo
širším	široký	k2eAgFnPc3d2	širší
vrstvám	vrstva	k1gFnPc3	vrstva
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
ČLR	ČLR	kA	ČLR
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
postupná	postupný	k2eAgFnSc1d1	postupná
reforma	reforma	k1gFnSc1	reforma
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
řadu	řada	k1gFnSc4	řada
znaků	znak	k1gInPc2	znak
zjednodušila	zjednodušit	k5eAaPmAgFnS	zjednodušit
<g/>
.	.	kIx.	.
</s>
<s>
Kritici	kritik	k1gMnPc1	kritik
ovšem	ovšem	k9	ovšem
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
narušila	narušit	k5eAaPmAgFnS	narušit
informace	informace	k1gFnSc1	informace
o	o	k7c6	o
původu	původ	k1gInSc6	původ
a	a	k8xC	a
příbuznosti	příbuznost	k1gFnSc3	příbuznost
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Reformy	reforma	k1gFnPc1	reforma
však	však	k9	však
byly	být	k5eAaImAgFnP	být
plánovány	plánovat	k5eAaImNgFnP	plánovat
již	již	k6eAd1	již
od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
vlnou	vlna	k1gFnSc7	vlna
modernizace	modernizace	k1gFnSc2	modernizace
Číny	Čína	k1gFnSc2	Čína
(	(	kIx(	(
<g/>
Májové	májový	k2eAgNnSc1d1	Májové
hnutí	hnutí	k1gNnSc1	hnutí
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
mnohem	mnohem	k6eAd1	mnohem
dalekosáhlejší	dalekosáhlý	k2eAgFnPc1d2	dalekosáhlejší
reformy	reforma	k1gFnPc1	reforma
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zastřely	zastřít	k5eAaPmAgFnP	zastřít
původní	původní	k2eAgFnPc1d1	původní
podoby	podoba	k1gFnPc1	podoba
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc4	jejich
stavbu	stavba	k1gFnSc4	stavba
<g/>
,	,	kIx,	,
etymologie	etymologie	k1gFnPc4	etymologie
i	i	k8xC	i
příbuznosti	příbuznost	k1gFnPc4	příbuznost
a	a	k8xC	a
oddělily	oddělit	k5eAaPmAgFnP	oddělit
průměrně	průměrně	k6eAd1	průměrně
gramotné	gramotný	k2eAgMnPc4d1	gramotný
Číňany	Číňan	k1gMnPc4	Číňan
od	od	k7c2	od
starší	starý	k2eAgFnSc2d2	starší
tradice	tradice	k1gFnSc2	tradice
<g/>
,	,	kIx,	,
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
letech	let	k1gInPc6	let
300-0	[number]	k4	300-0
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Původní	původní	k2eAgMnPc1d1	původní
znaky	znak	k1gInPc1	znak
ilustrují	ilustrovat	k5eAaBmIp3nP	ilustrovat
úzké	úzký	k2eAgNnSc4d1	úzké
spojení	spojení	k1gNnSc4	spojení
mezi	mezi	k7c7	mezi
významy	význam	k1gInPc7	význam
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
znaků	znak	k1gInPc2	znak
a	a	k8xC	a
vírou	víra	k1gFnSc7	víra
prastarých	prastarý	k2eAgMnPc2d1	prastarý
Číňanů	Číňan	k1gMnPc2	Číňan
v	v	k7c4	v
nebesa	nebesa	k1gNnPc4	nebesa
<g/>
.	.	kIx.	.
</s>
<s>
Zjednodušené	zjednodušený	k2eAgInPc1d1	zjednodušený
znaky	znak	k1gInPc1	znak
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
standardem	standard	k1gInSc7	standard
v	v	k7c6	v
ČLR	ČLR	kA	ČLR
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Hongkongu	Hongkong	k1gInSc2	Hongkong
a	a	k8xC	a
Macaa	Macao	k1gNnSc2	Macao
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Singapuru	Singapur	k1gInSc6	Singapur
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Tchaj-wanu	Tchajan	k1gInSc6	Tchaj-wan
se	se	k3xPyFc4	se
nadále	nadále	k6eAd1	nadále
používají	používat	k5eAaImIp3nP	používat
tradiční	tradiční	k2eAgInPc4d1	tradiční
znaky	znak	k1gInPc4	znak
<g/>
.	.	kIx.	.
</s>
<s>
Zjednodušení	zjednodušení	k1gNnSc1	zjednodušení
nejvíce	hodně	k6eAd3	hodně
vynikne	vyniknout	k5eAaPmIp3nS	vyniknout
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
zjednodušen	zjednodušen	k2eAgInSc1d1	zjednodušen
radikál	radikál	k1gInSc1	radikál
<g/>
,	,	kIx,	,
a	a	k8xC	a
zásah	zásah	k1gInSc1	zásah
se	se	k3xPyFc4	se
tudíž	tudíž	k8xC	tudíž
dotkl	dotknout	k5eAaPmAgMnS	dotknout
velké	velký	k2eAgFnPc4d1	velká
skupiny	skupina	k1gFnPc4	skupina
znaků	znak	k1gInPc2	znak
(	(	kIx(	(
<g/>
např.	např.	kA	např.
radikál	radikál	k1gInSc1	radikál
ve	v	k7c6	v
slově	slovo	k1gNnSc6	slovo
jü	jü	k?	jü
–	–	k?	–
"	"	kIx"	"
<g/>
jazyk	jazyk	k1gInSc1	jazyk
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
語	語	k?	語
→	→	k?	→
语	语	k?	语
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
dnes	dnes	k6eAd1	dnes
používané	používaný	k2eAgInPc1d1	používaný
čínské	čínský	k2eAgInPc1d1	čínský
znaky	znak	k1gInPc1	znak
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
skupiny	skupina	k1gFnPc4	skupina
<g/>
:	:	kIx,	:
zjednodušené	zjednodušený	k2eAgInPc4d1	zjednodušený
znaky	znak	k1gInPc4	znak
(	(	kIx(	(
<g/>
mají	mít	k5eAaImIp3nP	mít
protějšek	protějšek	k1gInSc4	protějšek
mezi	mezi	k7c7	mezi
tradičními	tradiční	k2eAgInPc7d1	tradiční
znaky	znak	k1gInPc7	znak
<g/>
)	)	kIx)	)
tradiční	tradiční	k2eAgInPc4d1	tradiční
znaky	znak	k1gInPc4	znak
(	(	kIx(	(
<g/>
v	v	k7c6	v
ČLR	ČLR	kA	ČLR
byly	být	k5eAaImAgFnP	být
nahrazeny	nahrazen	k2eAgMnPc4d1	nahrazen
zjednodušeným	zjednodušený	k2eAgInSc7d1	zjednodušený
protějškem	protějšek	k1gInSc7	protějšek
<g/>
)	)	kIx)	)
znaky	znak	k1gInPc1	znak
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nebyly	být	k5eNaImAgInP	být
zjednodušovány	zjednodušovat	k5eAaImNgInP	zjednodušovat
<g/>
,	,	kIx,	,
vypadají	vypadat	k5eAaPmIp3nP	vypadat
tedy	tedy	k9	tedy
v	v	k7c6	v
ČLR	ČLR	kA	ČLR
i	i	k9	i
na	na	k7c6	na
Tchaj-wanu	Tchajan	k1gInSc6	Tchaj-wan
stejně	stejně	k6eAd1	stejně
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
je	být	k5eAaImIp3nS	být
situace	situace	k1gFnSc1	situace
složitější	složitý	k2eAgFnSc1d2	složitější
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgFnPc1d1	různá
varianty	varianta	k1gFnPc1	varianta
téhož	týž	k3xTgInSc2	týž
znaku	znak	k1gInSc2	znak
existovaly	existovat	k5eAaImAgInP	existovat
dávno	dávno	k6eAd1	dávno
před	před	k7c7	před
vznikem	vznik	k1gInSc7	vznik
ČLR	ČLR	kA	ČLR
<g/>
,	,	kIx,	,
ke	k	k7c3	k
zjednodušování	zjednodušování	k1gNnSc3	zjednodušování
docházelo	docházet	k5eAaImAgNnS	docházet
také	také	k9	také
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
v	v	k7c6	v
podstatně	podstatně	k6eAd1	podstatně
menší	malý	k2eAgFnSc3d2	menší
míře	míra	k1gFnSc3	míra
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
znak	znak	k1gInSc1	znak
tchaj	tchaj	k1gInSc1	tchaj
ve	v	k7c6	v
slově	slovo	k1gNnSc6	slovo
Tchaj-wan	Tchajan	k1gInSc1	Tchaj-wan
má	mít	k5eAaImIp3nS	mít
jednoduchou	jednoduchý	k2eAgFnSc4d1	jednoduchá
(	(	kIx(	(
<g/>
台	台	k?	台
<g/>
)	)	kIx)	)
a	a	k8xC	a
složitou	složitý	k2eAgFnSc4d1	složitá
(	(	kIx(	(
<g/>
臺	臺	k?	臺
<g/>
)	)	kIx)	)
variantu	varianta	k1gFnSc4	varianta
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
i	i	k9	i
na	na	k7c6	na
Tchaj-wanu	Tchajan	k1gInSc6	Tchaj-wan
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
v	v	k7c6	v
doméně	doména	k1gFnSc6	doména
tradičních	tradiční	k2eAgInPc2d1	tradiční
znaků	znak	k1gInPc2	znak
<g/>
)	)	kIx)	)
používá	používat	k5eAaImIp3nS	používat
ta	ten	k3xDgFnSc1	ten
jednodušší	jednoduchý	k2eAgFnSc1d2	jednodušší
<g/>
.	.	kIx.	.
</s>
<s>
Své	své	k1gNnSc1	své
vlastní	vlastní	k2eAgFnSc2d1	vlastní
reformy	reforma	k1gFnSc2	reforma
provedli	provést	k5eAaPmAgMnP	provést
i	i	k9	i
Japonci	Japonec	k1gMnPc1	Japonec
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
i	i	k9	i
když	když	k8xS	když
kandži	kandzat	k5eAaPmIp1nS	kandzat
lze	lze	k6eAd1	lze
spíše	spíše	k9	spíše
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
tradiční	tradiční	k2eAgInPc4d1	tradiční
znaky	znak	k1gInPc4	znak
<g/>
,	,	kIx,	,
např.	např.	kA	např.
znak	znak	k1gInSc1	znak
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
süe	süe	k?	süe
<g/>
,	,	kIx,	,
japonsky	japonsky	k6eAd1	japonsky
gaku	gaku	k5eAaPmIp1nS	gaku
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
shoduje	shodovat	k5eAaImIp3nS	shodovat
s	s	k7c7	s
tvarem	tvar	k1gInSc7	tvar
v	v	k7c6	v
ČLR	ČLR	kA	ČLR
(	(	kIx(	(
<g/>
学	学	k?	学
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
Tchaj-wanu	Tchajan	k1gInSc6	Tchaj-wan
a	a	k8xC	a
v	v	k7c6	v
Hongkongu	Hongkong	k1gInSc6	Hongkong
je	být	k5eAaImIp3nS	být
složitější	složitý	k2eAgFnSc1d2	složitější
(	(	kIx(	(
<g/>
學	學	k?	學
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
