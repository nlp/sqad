<p>
<s>
Basil	Basil	k1gMnSc1	Basil
Hume	Hum	k1gMnSc2	Hum
OSB	OSB	kA	OSB
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
Newcastle	Newcastle	k1gFnSc1	Newcastle
upon	upon	k1gMnSc1	upon
Tyne	Tyne	k1gInSc1	Tyne
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
anglický	anglický	k2eAgMnSc1d1	anglický
mnich	mnich	k1gMnSc1	mnich
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Anglické	anglický	k2eAgFnSc2d1	anglická
benediktinské	benediktinský	k2eAgFnSc2d1	benediktinská
kongregace	kongregace	k1gFnSc2	kongregace
(	(	kIx(	(
<g/>
English	English	k1gInSc1	English
Benedictine	Benedictin	k1gInSc5	Benedictin
Congregation	Congregation	k1gInSc1	Congregation
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
13	[number]	k4	13
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgMnS	být
jejím	její	k3xOp3gMnSc7	její
opatem	opat	k1gMnSc7	opat
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
z	z	k7c2	z
Westminsteru	Westminster	k1gInSc2	Westminster
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
nejvýše	vysoce	k6eAd3	vysoce
postaveným	postavený	k2eAgMnSc7d1	postavený
duchovním	duchovní	k1gMnSc7	duchovní
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
Walesu	Wales	k1gInSc2	Wales
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
kardinálem	kardinál	k1gMnSc7	kardinál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
jako	jako	k9	jako
George	Georg	k1gFnPc1	Georg
Haliburton	Haliburton	k1gInSc1	Haliburton
Hume	Hum	k1gFnSc2	Hum
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
Sir	sir	k1gMnSc1	sir
William	William	k1gInSc1	William
Errington	Errington	k1gInSc4	Errington
Hume	Hum	k1gFnSc2	Hum
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
protestant	protestant	k1gMnSc1	protestant
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
Marie	Maria	k1gFnSc2	Maria
Elizabeth	Elizabeth	k1gFnSc2	Elizabeth
byla	být	k5eAaImAgFnS	být
katolička	katolička	k1gFnSc1	katolička
<g/>
.	.	kIx.	.
</s>
<s>
Vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
sestrami	sestra	k1gFnPc7	sestra
a	a	k8xC	a
bratrem	bratr	k1gMnSc7	bratr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
18	[number]	k4	18
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
do	do	k7c2	do
noviciátu	noviciát	k1gInSc2	noviciát
v	v	k7c6	v
benediktinském	benediktinský	k2eAgNnSc6d1	benediktinské
opatství	opatství	k1gNnSc6	opatství
Ampleforth	Amplefortha	k1gFnPc2	Amplefortha
<g/>
.	.	kIx.	.
</s>
<s>
Teologii	teologie	k1gFnSc4	teologie
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
Freiburské	freiburský	k2eAgFnSc6d1	Freiburská
univerzitě	univerzita	k1gFnSc6	univerzita
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
kněze	kněz	k1gMnPc4	kněz
byl	být	k5eAaImAgMnS	být
vysvěcen	vysvěcen	k2eAgMnSc1d1	vysvěcen
23	[number]	k4	23
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1976	[number]	k4	1976
jej	on	k3xPp3gInSc4	on
papež	papež	k1gMnSc1	papež
Pavel	Pavel	k1gMnSc1	Pavel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
Westminsteru	Westminster	k1gInSc2	Westminster
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
kardinálem	kardinál	k1gMnSc7	kardinál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
76	[number]	k4	76
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
pochován	pochovat	k5eAaPmNgInS	pochovat
ve	v	k7c6	v
Westminsterské	Westminsterský	k2eAgFnSc6d1	Westminsterská
katedrále	katedrála	k1gFnSc6	katedrála
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
Basil	Basil	k1gMnSc1	Basil
Hume	Hum	k1gInSc2	Hum
byl	být	k5eAaImAgMnS	být
držitelem	držitel	k1gMnSc7	držitel
britského	britský	k2eAgInSc2d1	britský
řádu	řád	k1gInSc2	řád
Za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
(	(	kIx(	(
<g/>
Order	Order	k1gMnSc1	Order
of	of	k?	of
Merit	meritum	k1gNnPc2	meritum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Basil	Basila	k1gFnPc2	Basila
Hume	Hum	k1gFnSc2	Hum
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
na	na	k7c4	na
catholic-hierarchy	catholicierarcha	k1gFnPc4	catholic-hierarcha
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
v	v	k7c6	v
biografickém	biografický	k2eAgInSc6d1	biografický
slovníku	slovník	k1gInSc6	slovník
kardinálů	kardinál	k1gMnPc2	kardinál
Salvadora	Salvador	k1gMnSc4	Salvador
Mirandy	Miranda	k1gFnSc2	Miranda
</s>
</p>
