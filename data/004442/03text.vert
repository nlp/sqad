<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
správním	správní	k2eAgInSc7d1	správní
soudem	soud	k1gInSc7	soud
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
článkem	článek	k1gInSc7	článek
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
soudů	soud	k1gInPc2	soud
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
hlavním	hlavní	k2eAgInSc7d1	hlavní
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
zajišťování	zajišťování	k1gNnSc1	zajišťování
jednoty	jednota	k1gFnSc2	jednota
a	a	k8xC	a
zákonnosti	zákonnost	k1gFnSc2	zákonnost
rozhodovací	rozhodovací	k2eAgFnSc2d1	rozhodovací
praxe	praxe	k1gFnSc2	praxe
českých	český	k2eAgInPc2d1	český
soudů	soud	k1gInPc2	soud
v	v	k7c6	v
trestním	trestní	k2eAgNnSc6d1	trestní
řízení	řízení	k1gNnSc6	řízení
a	a	k8xC	a
v	v	k7c6	v
občanském	občanský	k2eAgNnSc6d1	občanské
soudním	soudní	k2eAgNnSc6d1	soudní
řízení	řízení	k1gNnSc6	řízení
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k6eAd1	především
rozhodováním	rozhodování	k1gNnSc7	rozhodování
o	o	k7c6	o
mimořádných	mimořádný	k2eAgInPc6d1	mimořádný
opravných	opravný	k2eAgInPc6d1	opravný
prostředcích	prostředek	k1gInPc6	prostředek
proti	proti	k7c3	proti
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
soudů	soud	k1gInPc2	soud
nižších	nízký	k2eAgInPc2d2	nižší
stupňů	stupeň	k1gInPc2	stupeň
a	a	k8xC	a
zaujímáním	zaujímání	k1gNnSc7	zaujímání
sjednocujících	sjednocující	k2eAgNnPc2d1	sjednocující
stanovisek	stanovisko	k1gNnPc2	stanovisko
<g/>
.	.	kIx.	.
</s>
<s>
Soud	soud	k1gInSc1	soud
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
vznikem	vznik	k1gInSc7	vznik
samostatné	samostatný	k2eAgFnSc2d1	samostatná
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
transformací	transformace	k1gFnPc2	transformace
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
Federativní	federativní	k2eAgFnSc2d1	federativní
Republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
z	z	k7c2	z
dosavadního	dosavadní	k2eAgInSc2d1	dosavadní
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
existujícího	existující	k2eAgMnSc2d1	existující
od	od	k7c2	od
uskutečnění	uskutečnění	k1gNnSc2	uskutečnění
federalizace	federalizace	k1gFnSc2	federalizace
Československa	Československo	k1gNnSc2	Československo
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Vrchní	vrchní	k2eAgInSc1d1	vrchní
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Soud	soud	k1gInSc1	soud
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
památkově	památkově	k6eAd1	památkově
chráněné	chráněný	k2eAgFnSc3d1	chráněná
budově	budova	k1gFnSc3	budova
postavené	postavený	k2eAgFnPc1d1	postavená
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
sídle	sídlo	k1gNnSc6	sídlo
Všeobecného	všeobecný	k2eAgInSc2d1	všeobecný
penzijního	penzijní	k2eAgInSc2d1	penzijní
ústavu	ústav	k1gInSc2	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
činnosti	činnost	k1gFnSc2	činnost
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
je	být	k5eAaImIp3nS	být
rozhodování	rozhodování	k1gNnSc4	rozhodování
o	o	k7c6	o
mimořádných	mimořádný	k2eAgInPc6d1	mimořádný
opravných	opravný	k2eAgInPc6d1	opravný
prostředcích	prostředek	k1gInPc6	prostředek
proti	proti	k7c3	proti
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
odvolacích	odvolací	k2eAgInPc2d1	odvolací
(	(	kIx(	(
<g/>
krajských	krajský	k2eAgInPc2d1	krajský
nebo	nebo	k8xC	nebo
vrchních	vrchní	k2eAgInPc2d1	vrchní
<g/>
)	)	kIx)	)
soudů	soud	k1gInPc2	soud
–	–	k?	–
o	o	k7c6	o
civilních	civilní	k2eAgInPc6d1	civilní
a	a	k8xC	a
trestněprávních	trestněprávní	k2eAgInPc6d1	trestněprávní
dovoláních	dovolání	k1gNnPc6	dovolání
<g/>
,	,	kIx,	,
v	v	k7c6	v
trestním	trestní	k2eAgNnSc6d1	trestní
řízení	řízení	k1gNnSc6	řízení
také	také	k9	také
o	o	k7c6	o
stížnostech	stížnost	k1gFnPc6	stížnost
pro	pro	k7c4	pro
porušení	porušení	k1gNnSc4	porušení
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
plní	plnit	k5eAaImIp3nS	plnit
svou	svůj	k3xOyFgFnSc4	svůj
hlavní	hlavní	k2eAgFnSc4d1	hlavní
úlohu	úloha	k1gFnSc4	úloha
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
je	být	k5eAaImIp3nS	být
sjednocování	sjednocování	k1gNnSc1	sjednocování
české	český	k2eAgFnSc2d1	Česká
judikatury	judikatura	k1gFnSc2	judikatura
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
Nejvyššímu	vysoký	k2eAgInSc3d3	Nejvyšší
soudu	soud	k1gInSc3	soud
přísluší	příslušet	k5eAaImIp3nS	příslušet
určitá	určitý	k2eAgFnSc1d1	určitá
speciální	speciální	k2eAgFnSc1d1	speciální
agenda	agenda	k1gFnSc1	agenda
–	–	k?	–
uznání	uznání	k1gNnSc1	uznání
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
soudů	soud	k1gInPc2	soud
<g/>
,	,	kIx,	,
povolení	povolení	k1gNnSc4	povolení
průvozu	průvoz	k1gInSc2	průvoz
osoby	osoba	k1gFnSc2	osoba
podle	podle	k7c2	podle
evropského	evropský	k2eAgInSc2d1	evropský
zatýkacího	zatýkací	k2eAgInSc2d1	zatýkací
rozkazu	rozkaz	k1gInSc2	rozkaz
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
i	i	k9	i
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
vydávání	vydávání	k1gNnSc2	vydávání
do	do	k7c2	do
ciziny	cizina	k1gFnSc2	cizina
<g/>
,	,	kIx,	,
přezkum	přezkum	k1gInSc1	přezkum
příkazů	příkaz	k1gInPc2	příkaz
k	k	k7c3	k
odposlechu	odposlech	k1gInSc3	odposlech
a	a	k8xC	a
záznamu	záznam	k1gInSc3	záznam
nebo	nebo	k8xC	nebo
zjištění	zjištění	k1gNnSc3	zjištění
údajů	údaj	k1gInPc2	údaj
o	o	k7c6	o
telekomunikačním	telekomunikační	k2eAgInSc6d1	telekomunikační
provozu	provoz	k1gInSc6	provoz
<g/>
,	,	kIx,	,
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
v	v	k7c6	v
pochybnostech	pochybnost	k1gFnPc6	pochybnost
o	o	k7c6	o
vynětí	vynětí	k1gNnSc6	vynětí
z	z	k7c2	z
pravomoci	pravomoc	k1gFnSc2	pravomoc
orgánů	orgán	k1gInPc2	orgán
činných	činný	k2eAgInPc2d1	činný
v	v	k7c6	v
trestním	trestní	k2eAgNnSc6d1	trestní
řízení	řízení	k1gNnSc6	řízení
<g/>
,	,	kIx,	,
přikázání	přikázání	k1gNnSc1	přikázání
věci	věc	k1gFnSc2	věc
jinému	jiný	k1gMnSc3	jiný
než	než	k8xS	než
místně	místně	k6eAd1	místně
příslušnému	příslušný	k2eAgInSc3d1	příslušný
soudu	soud	k1gInSc3	soud
mezi	mezi	k7c7	mezi
obvody	obvod	k1gInPc7	obvod
vrchních	vrchní	k2eAgInPc2d1	vrchní
soudů	soud	k1gInPc2	soud
nebo	nebo	k8xC	nebo
určení	určení	k1gNnSc2	určení
místní	místní	k2eAgFnSc2d1	místní
příslušnosti	příslušnost	k1gFnSc2	příslušnost
v	v	k7c6	v
civilním	civilní	k2eAgNnSc6d1	civilní
soudnictví	soudnictví	k1gNnSc6	soudnictví
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
takové	takový	k3xDgNnSc4	takový
určení	určení	k1gNnSc4	určení
chybí	chybět	k5eAaImIp3nS	chybět
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
nelze	lze	k6eNd1	lze
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
.	.	kIx.	.
</s>
<s>
Jednání	jednání	k1gNnSc1	jednání
soudu	soud	k1gInSc2	soud
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
přístupná	přístupný	k2eAgFnSc1d1	přístupná
<g/>
,	,	kIx,	,
ledaže	ledaže	k8xS	ledaže
je	být	k5eAaImIp3nS	být
veřejnost	veřejnost	k1gFnSc1	veřejnost
z	z	k7c2	z
důležitých	důležitý	k2eAgInPc2d1	důležitý
důvodů	důvod	k1gInPc2	důvod
vyloučena	vyloučit	k5eAaPmNgFnS	vyloučit
<g/>
,	,	kIx,	,
veřejných	veřejný	k2eAgNnPc2d1	veřejné
zasedání	zasedání	k1gNnPc2	zasedání
v	v	k7c6	v
trestním	trestní	k2eAgNnSc6d1	trestní
řízení	řízení	k1gNnSc6	řízení
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nP	účastnit
státní	státní	k2eAgMnPc1d1	státní
zástupci	zástupce	k1gMnPc1	zástupce
Nejvyššího	vysoký	k2eAgNnSc2d3	nejvyšší
státního	státní	k2eAgNnSc2d1	státní
zastupitelství	zastupitelství	k1gNnSc2	zastupitelství
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
buď	buď	k8xC	buď
standardně	standardně	k6eAd1	standardně
ve	v	k7c6	v
specializovaných	specializovaný	k2eAgInPc6d1	specializovaný
senátech	senát	k1gInPc6	senát
složených	složený	k2eAgInPc6d1	složený
z	z	k7c2	z
předsedy	předseda	k1gMnSc2	předseda
a	a	k8xC	a
dvou	dva	k4xCgMnPc2	dva
dalších	další	k2eAgMnPc2d1	další
soudců	soudce	k1gMnPc2	soudce
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
velkých	velký	k2eAgInPc2d1	velký
senátů	senát	k1gInPc2	senát
kolegií	kolegium	k1gNnPc2	kolegium
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
předsedy	předseda	k1gMnSc2	předseda
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
alespoň	alespoň	k9	alespoň
osmi	osm	k4xCc2	osm
soudců	soudce	k1gMnPc2	soudce
daného	daný	k2eAgNnSc2d1	dané
kolegia	kolegium	k1gNnSc2	kolegium
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgInPc1d1	velký
senáty	senát	k1gInPc1	senát
rozhodují	rozhodovat	k5eAaImIp3nP	rozhodovat
jen	jen	k9	jen
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
tříčlenný	tříčlenný	k2eAgInSc1d1	tříčlenný
senát	senát	k1gInSc1	senát
dospěl	dochvít	k5eAaPmAgInS	dochvít
k	k	k7c3	k
právnímu	právní	k2eAgInSc3d1	právní
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
odlišný	odlišný	k2eAgInSc1d1	odlišný
od	od	k7c2	od
dosavadní	dosavadní	k2eAgFnSc2d1	dosavadní
judikatury	judikatura	k1gFnSc2	judikatura
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
běžné	běžný	k2eAgFnSc2d1	běžná
rozhodovací	rozhodovací	k2eAgFnSc2d1	rozhodovací
činnosti	činnost	k1gFnSc2	činnost
také	také	k9	také
sleduje	sledovat	k5eAaImIp3nS	sledovat
a	a	k8xC	a
vyhodnocuje	vyhodnocovat	k5eAaImIp3nS	vyhodnocovat
pravomocná	pravomocný	k2eAgNnPc4d1	pravomocné
rozhodnutí	rozhodnutí	k1gNnPc4	rozhodnutí
ostatních	ostatní	k2eAgInPc2d1	ostatní
soudů	soud	k1gInPc2	soud
v	v	k7c6	v
občanském	občanský	k2eAgNnSc6d1	občanské
soudním	soudní	k2eAgNnSc6d1	soudní
řízení	řízení	k1gNnSc6	řízení
i	i	k8xC	i
v	v	k7c6	v
trestním	trestní	k2eAgNnSc6d1	trestní
řízení	řízení	k1gNnSc6	řízení
a	a	k8xC	a
na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
základě	základ	k1gInSc6	základ
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
jednotného	jednotný	k2eAgNnSc2d1	jednotné
rozhodování	rozhodování	k1gNnSc2	rozhodování
soudů	soud	k1gInPc2	soud
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
příslušného	příslušný	k2eAgNnSc2d1	příslušné
kolegia	kolegium	k1gNnSc2	kolegium
nebo	nebo	k8xC	nebo
v	v	k7c6	v
plénu	plénum	k1gNnSc6	plénum
sjednocující	sjednocující	k2eAgNnPc1d1	sjednocující
stanoviska	stanovisko	k1gNnPc1	stanovisko
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
stanoviska	stanovisko	k1gNnPc1	stanovisko
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
svá	svůj	k3xOyFgNnPc4	svůj
klasická	klasický	k2eAgNnPc4d1	klasické
rozhodnutí	rozhodnutí	k1gNnPc4	rozhodnutí
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
uveřejňuje	uveřejňovat	k5eAaImIp3nS	uveřejňovat
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
internetové	internetový	k2eAgFnSc6d1	internetová
stránce	stránka	k1gFnSc6	stránka
v	v	k7c6	v
anonymizované	anonymizovaný	k2eAgFnSc6d1	anonymizovaná
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
vydává	vydávat	k5eAaImIp3nS	vydávat
i	i	k9	i
tištěnou	tištěný	k2eAgFnSc4d1	tištěná
Sbírku	sbírka	k1gFnSc4	sbírka
soudních	soudní	k2eAgNnPc2d1	soudní
rozhodnutí	rozhodnutí	k1gNnPc2	rozhodnutí
a	a	k8xC	a
stanovisek	stanovisko	k1gNnPc2	stanovisko
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
jsou	být	k5eAaImIp3nP	být
publikována	publikován	k2eAgNnPc1d1	Publikováno
všechna	všechen	k3xTgNnPc4	všechen
sjednocující	sjednocující	k2eAgNnPc4d1	sjednocující
stanoviska	stanovisko	k1gNnPc4	stanovisko
a	a	k8xC	a
některá	některý	k3yIgNnPc4	některý
významná	významný	k2eAgNnPc4d1	významné
rozhodnutí	rozhodnutí	k1gNnPc4	rozhodnutí
nejen	nejen	k6eAd1	nejen
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
předsedy	předseda	k1gMnSc2	předseda
a	a	k8xC	a
místopředsedů	místopředseda	k1gMnPc2	místopředseda
<g/>
,	,	kIx,	,
ty	ten	k3xDgMnPc4	ten
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
ze	z	k7c2	z
soudců	soudce	k1gMnPc2	soudce
na	na	k7c4	na
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
10	[number]	k4	10
let	léto	k1gNnPc2	léto
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
z	z	k7c2	z
předsedů	předseda	k1gMnPc2	předseda
kolegií	kolegium	k1gNnPc2	kolegium
<g/>
,	,	kIx,	,
předsedů	předseda	k1gMnPc2	předseda
senátů	senát	k1gInPc2	senát
a	a	k8xC	a
z	z	k7c2	z
dalších	další	k2eAgMnPc2d1	další
soudců	soudce	k1gMnPc2	soudce
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
přidělení	přidělení	k1gNnSc4	přidělení
nebo	nebo	k8xC	nebo
přeložení	přeložení	k1gNnSc4	přeložení
k	k	k7c3	k
Nejvyššímu	vysoký	k2eAgInSc3d3	Nejvyšší
soudu	soud	k1gInSc3	soud
musí	muset	k5eAaImIp3nS	muset
soudce	soudce	k1gMnSc2	soudce
splňovat	splňovat	k5eAaImF	splňovat
podmínku	podmínka	k1gFnSc4	podmínka
alespoň	alespoň	k9	alespoň
desetileté	desetiletý	k2eAgFnSc2d1	desetiletá
odborné	odborný	k2eAgFnSc2d1	odborná
praxe	praxe	k1gFnSc2	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Soudci	soudce	k1gMnPc1	soudce
tvoří	tvořit	k5eAaImIp3nP	tvořit
dvě	dva	k4xCgNnPc1	dva
kolegia	kolegium	k1gNnPc1	kolegium
<g/>
,	,	kIx,	,
trestní	trestní	k2eAgNnSc4d1	trestní
kolegium	kolegium	k1gNnSc4	kolegium
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
třetina	třetina	k1gFnSc1	třetina
soudců	soudce	k1gMnPc2	soudce
<g/>
)	)	kIx)	)
a	a	k8xC	a
občanskoprávní	občanskoprávní	k2eAgNnSc4d1	občanskoprávní
a	a	k8xC	a
obchodní	obchodní	k2eAgNnSc4d1	obchodní
kolegium	kolegium	k1gNnSc4	kolegium
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
dvě	dva	k4xCgFnPc4	dva
třetiny	třetina	k1gFnPc1	třetina
soudců	soudce	k1gMnPc2	soudce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
spojením	spojení	k1gNnSc7	spojení
původního	původní	k2eAgNnSc2d1	původní
občanskoprávního	občanskoprávní	k2eAgNnSc2d1	občanskoprávní
kolegia	kolegium	k1gNnSc2	kolegium
a	a	k8xC	a
obchodního	obchodní	k2eAgNnSc2d1	obchodní
kolegia	kolegium	k1gNnSc2	kolegium
<g/>
.	.	kIx.	.
</s>
<s>
Předsedy	předseda	k1gMnPc4	předseda
kolegií	kolegium	k1gNnPc2	kolegium
a	a	k8xC	a
předsedy	předseda	k1gMnSc2	předseda
senátů	senát	k1gInPc2	senát
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
dva	dva	k4xCgInPc4	dva
členy	člen	k1gInPc4	člen
Rady	rada	k1gFnSc2	rada
Justiční	justiční	k2eAgFnSc2d1	justiční
akademie	akademie	k1gFnSc2	akademie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
garantuje	garantovat	k5eAaBmIp3nS	garantovat
její	její	k3xOp3gFnSc4	její
výchovnou	výchovný	k2eAgFnSc4d1	výchovná
a	a	k8xC	a
vzdělávací	vzdělávací	k2eAgFnSc4d1	vzdělávací
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
tři	tři	k4xCgInPc4	tři
členy	člen	k1gInPc4	člen
zvláštního	zvláštní	k2eAgInSc2d1	zvláštní
senátu	senát	k1gInSc2	senát
v	v	k7c6	v
sídle	sídlo	k1gNnSc6	sídlo
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
správního	správní	k2eAgInSc2d1	správní
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
kompetenční	kompetenční	k2eAgInPc4d1	kompetenční
spory	spor	k1gInPc4	spor
o	o	k7c4	o
pravomoc	pravomoc	k1gFnSc4	pravomoc
nebo	nebo	k8xC	nebo
věcnou	věcný	k2eAgFnSc4d1	věcná
příslušnost	příslušnost	k1gFnSc4	příslušnost
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
věci	věc	k1gFnSc6	věc
<g/>
)	)	kIx)	)
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
předseda	předseda	k1gMnSc1	předseda
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
také	také	k6eAd1	také
každému	každý	k3xTgMnSc3	každý
soudci	soudce	k1gMnPc1	soudce
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
návrh	návrh	k1gInSc1	návrh
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
alespoň	alespoň	k9	alespoň
jednoho	jeden	k4xCgMnSc4	jeden
asistenta	asistent	k1gMnSc4	asistent
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
pak	pak	k6eAd1	pak
z	z	k7c2	z
pověření	pověření	k1gNnSc2	pověření
soudce	soudce	k1gMnSc2	soudce
činí	činit	k5eAaImIp3nS	činit
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
úkony	úkon	k1gInPc4	úkon
soudního	soudní	k2eAgNnSc2d1	soudní
řízení	řízení	k1gNnSc2	řízení
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
svůj	svůj	k3xOyFgInSc4	svůj
poradní	poradní	k2eAgInSc4d1	poradní
orgán	orgán	k1gInSc4	orgán
má	mít	k5eAaImIp3nS	mít
předseda	předseda	k1gMnSc1	předseda
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
pětičlennou	pětičlenný	k2eAgFnSc4d1	pětičlenná
soudcovskou	soudcovský	k2eAgFnSc4d1	soudcovská
radu	rada	k1gFnSc4	rada
<g/>
,	,	kIx,	,
složenou	složený	k2eAgFnSc4d1	složená
ze	z	k7c2	z
zvolených	zvolený	k2eAgMnPc2d1	zvolený
soudců	soudce	k1gMnPc2	soudce
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
JUDr.	JUDr.	kA	JUDr.
Otakar	Otakar	k1gMnSc1	Otakar
Motejl	Motejl	k1gMnSc1	Motejl
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
JUDr.	JUDr.	kA	JUDr.
Eliška	Eliška	k1gFnSc1	Eliška
Wagnerová	Wagnerová	k1gFnSc1	Wagnerová
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
JUDr.	JUDr.	kA	JUDr.
Iva	Iva	k1gFnSc1	Iva
Brožová	Brožová	k1gFnSc1	Brožová
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
–	–	k?	–
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
prof.	prof.	kA	prof.
JUDr.	JUDr.	kA	JUDr.
Pavel	Pavel	k1gMnSc1	Pavel
Šámal	Šámal	k1gMnSc1	Šámal
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
(	(	kIx(	(
<g/>
od	od	k7c2	od
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
JUDr.	JUDr.	kA	JUDr.
Pavel	Pavel	k1gMnSc1	Pavel
Kučera	Kučera	k1gMnSc1	Kučera
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
JUDr.	JUDr.	kA	JUDr.
Roman	Roman	k1gMnSc1	Roman
Fiala	Fiala	k1gMnSc1	Fiala
(	(	kIx(	(
<g/>
od	od	k7c2	od
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soudní	soudní	k2eAgInSc1d1	soudní
a	a	k8xC	a
kasační	kasační	k2eAgInSc1d1	kasační
dvůr	dvůr	k1gInSc1	dvůr
a	a	k8xC	a
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
brzy	brzy	k6eAd1	brzy
nato	nato	k6eAd1	nato
byl	být	k5eAaImAgInS	být
zásluhou	zásluha	k1gFnSc7	zásluha
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
prvního	první	k4xOgNnSc2	první
děkana	děkan	k1gMnSc2	děkan
brněnské	brněnský	k2eAgFnSc2d1	brněnská
právnické	právnický	k2eAgFnSc2d1	právnická
fakulty	fakulta	k1gFnSc2	fakulta
a	a	k8xC	a
poslance	poslanec	k1gMnSc2	poslanec
Františka	František	k1gMnSc2	František
Weyra	Weyra	k1gMnSc1	Weyra
přesunut	přesunut	k2eAgMnSc1d1	přesunut
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1919	[number]	k4	1919
zahájil	zahájit	k5eAaPmAgMnS	zahájit
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc7	svůj
činností	činnost	k1gFnSc7	činnost
přímo	přímo	k6eAd1	přímo
navazoval	navazovat	k5eAaImAgInS	navazovat
na	na	k7c4	na
vídeňský	vídeňský	k2eAgInSc4d1	vídeňský
Nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
soudní	soudní	k2eAgInSc4d1	soudní
a	a	k8xC	a
kasační	kasační	k2eAgInSc4d1	kasační
dvůr	dvůr	k1gInSc4	dvůr
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
také	také	k9	také
rozhodoval	rozhodovat	k5eAaImAgInS	rozhodovat
jako	jako	k9	jako
poslední	poslední	k2eAgFnPc4d1	poslední
instance	instance	k1gFnPc4	instance
v	v	k7c6	v
civilních	civilní	k2eAgFnPc6d1	civilní
a	a	k8xC	a
trestních	trestní	k2eAgFnPc6d1	trestní
věcech	věc	k1gFnPc6	věc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
letech	let	k1gInPc6	let
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
i	i	k8xC	i
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
Protektorátu	protektorát	k1gInSc2	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
část	část	k1gFnSc1	část
soudců	soudce	k1gMnPc2	soudce
a	a	k8xC	a
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
zahynula	zahynout	k5eAaPmAgFnS	zahynout
při	při	k7c6	při
bombardování	bombardování	k1gNnSc6	bombardování
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1944	[number]	k4	1944
(	(	kIx(	(
<g/>
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
na	na	k7c6	na
nároží	nároží	k1gNnSc6	nároží
budovy	budova	k1gFnSc2	budova
jeho	jeho	k3xOp3gNnSc2	jeho
dřívějšího	dřívější	k2eAgNnSc2d1	dřívější
detašovaného	detašovaný	k2eAgNnSc2d1	detašované
pracoviště	pracoviště	k1gNnSc2	pracoviště
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
Morava	Morava	k1gFnSc1	Morava
na	na	k7c4	na
Malinovského	Malinovský	k2eAgMnSc4d1	Malinovský
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
byla	být	k5eAaImAgFnS	být
obnovena	obnoven	k2eAgFnSc1d1	obnovena
kontinuita	kontinuita	k1gFnSc1	kontinuita
československého	československý	k2eAgInSc2d1	československý
právního	právní	k2eAgInSc2d1	právní
řádu	řád	k1gInSc2	řád
a	a	k8xC	a
státních	státní	k2eAgInPc2d1	státní
orgánů	orgán	k1gInPc2	orgán
<g/>
,	,	kIx,	,
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
byl	být	k5eAaImAgInS	být
ale	ale	k9	ale
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc6	leden
1950	[number]	k4	1950
přesunut	přesunout	k5eAaPmNgInS	přesunout
do	do	k7c2	do
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
federalizace	federalizace	k1gFnSc1	federalizace
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
pak	pak	k6eAd1	pak
vedle	vedle	k7c2	vedle
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
Československé	československý	k2eAgFnSc2d1	Československá
socialistické	socialistický	k2eAgFnSc2d1	socialistická
republiky	republika	k1gFnSc2	republika
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
kromě	kromě	k7c2	kromě
bratislavského	bratislavský	k2eAgInSc2d1	bratislavský
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
pro	pro	k7c4	pro
slovenskou	slovenský	k2eAgFnSc4d1	slovenská
část	část	k1gFnSc4	část
federace	federace	k1gFnSc2	federace
i	i	k9	i
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
České	český	k2eAgFnSc2d1	Česká
socialistické	socialistický	k2eAgFnSc2d1	socialistická
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
také	také	k9	také
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
sídlo	sídlo	k1gNnSc1	sídlo
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
Federativní	federativní	k2eAgFnSc2d1	federativní
Republiky	republika	k1gFnSc2	republika
nejpozději	pozdě	k6eAd3	pozdě
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
přesune	přesunout	k5eAaPmIp3nS	přesunout
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
ale	ale	k9	ale
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
už	už	k6eAd1	už
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
federální	federální	k2eAgInSc1d1	federální
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
transformován	transformován	k2eAgInSc1d1	transformován
na	na	k7c4	na
Nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
soud	soud	k1gInSc4	soud
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
dosavadní	dosavadní	k2eAgInSc1d1	dosavadní
republikový	republikový	k2eAgInSc1d1	republikový
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
prošel	projít	k5eAaPmAgMnS	projít
transformací	transformace	k1gFnSc7	transformace
ve	v	k7c4	v
Vrchní	vrchní	k2eAgInSc4d1	vrchní
soud	soud	k1gInSc4	soud
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
byl	být	k5eAaImAgInS	být
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zmocnění	zmocnění	k1gNnSc2	zmocnění
čl	čl	kA	čl
<g/>
.	.	kIx.	.
91	[number]	k4	91
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
1	[number]	k4	1
Ústavy	ústava	k1gFnSc2	ústava
zákonem	zákon	k1gInSc7	zákon
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k9	jako
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
bývalý	bývalý	k2eAgInSc1d1	bývalý
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
české	český	k2eAgFnSc2d1	Česká
části	část	k1gFnSc2	část
ČSFR	ČSFR	kA	ČSFR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
působí	působit	k5eAaImIp3nS	působit
už	už	k6eAd1	už
pouze	pouze	k6eAd1	pouze
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
(	(	kIx(	(
<g/>
v	v	k7c6	v
občanském	občanský	k2eAgInSc6d1	občanský
soudním	soudní	k2eAgInSc6d1	soudní
řádu	řád	k1gInSc6	řád
a	a	k8xC	a
trestním	trestní	k2eAgInSc6d1	trestní
řádu	řád	k1gInSc6	řád
však	však	k9	však
stále	stále	k6eAd1	stále
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
rozhodoval	rozhodovat	k5eAaImAgInS	rozhodovat
o	o	k7c6	o
mimořádných	mimořádný	k2eAgInPc6d1	mimořádný
opravných	opravný	k2eAgInPc6d1	opravný
prostředcích	prostředek	k1gInPc6	prostředek
i	i	k9	i
pražský	pražský	k2eAgInSc1d1	pražský
vrchní	vrchní	k2eAgInSc1d1	vrchní
soud	soud	k1gInSc1	soud
<g/>
;	;	kIx,	;
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
rozhodoval	rozhodovat	k5eAaImAgInS	rozhodovat
jen	jen	k9	jen
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vrchní	vrchní	k2eAgInSc1d1	vrchní
soud	soud	k1gInSc1	soud
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
o	o	k7c6	o
odvoláních	odvolání	k1gNnPc6	odvolání
proti	proti	k7c3	proti
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
krajských	krajský	k2eAgMnPc2d1	krajský
soudů	soud	k1gInPc2	soud
jako	jako	k8xC	jako
soudů	soud	k1gInPc2	soud
prvního	první	k4xOgInSc2	první
stupně	stupeň	k1gInSc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
však	však	k9	však
tato	tento	k3xDgFnSc1	tento
agenda	agenda	k1gFnSc1	agenda
přešla	přejít	k5eAaPmAgFnS	přejít
zcela	zcela	k6eAd1	zcela
na	na	k7c4	na
Nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
soud	soud	k1gInSc4	soud
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
definitivně	definitivně	k6eAd1	definitivně
stal	stát	k5eAaPmAgMnS	stát
vrcholnou	vrcholný	k2eAgFnSc7d1	vrcholná
soudní	soudní	k2eAgFnSc7d1	soudní
institucí	instituce	k1gFnSc7	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
docházelo	docházet	k5eAaImAgNnS	docházet
ke	k	k7c3	k
konfliktům	konflikt	k1gInPc3	konflikt
mezi	mezi	k7c7	mezi
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
soudem	soud	k1gInSc7	soud
a	a	k8xC	a
Ústavním	ústavní	k2eAgInSc7d1	ústavní
soudem	soud	k1gInSc7	soud
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
válce	válka	k1gFnSc6	válka
soudů	soud	k1gInPc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
opakovaně	opakovaně	k6eAd1	opakovaně
odsuzoval	odsuzovat	k5eAaImAgInS	odsuzovat
odpírače	odpírač	k1gMnSc4	odpírač
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnSc2	služba
za	za	k7c4	za
nenastoupení	nenastoupení	k1gNnSc4	nenastoupení
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
už	už	k9	už
nálezem	nález	k1gInSc7	nález
sp	sp	k?	sp
<g/>
.	.	kIx.	.
zn.	zn.	kA	zn.
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
ÚS	ÚS	kA	ÚS
81	[number]	k4	81
<g/>
/	/	kIx~	/
<g/>
95	[number]	k4	95
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
nelze	lze	k6eNd1	lze
za	za	k7c4	za
de	de	k?	de
facto	facto	k1gNnSc4	facto
jeden	jeden	k4xCgInSc1	jeden
čin	čin	k1gInSc1	čin
trestat	trestat	k5eAaImF	trestat
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
jeho	jeho	k3xOp3gInPc4	jeho
další	další	k2eAgInPc4d1	další
podobné	podobný	k2eAgInPc4d1	podobný
nálezy	nález	k1gInPc4	nález
nerespektoval	respektovat	k5eNaImAgMnS	respektovat
až	až	k6eAd1	až
do	do	k7c2	do
léta	léto	k1gNnSc2	léto
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
silnější	silný	k2eAgNnSc4d2	silnější
postavení	postavení	k1gNnSc4	postavení
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
uznal	uznat	k5eAaPmAgInS	uznat
a	a	k8xC	a
ustoupil	ustoupit	k5eAaPmAgInS	ustoupit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
zřídila	zřídit	k5eAaPmAgFnS	zřídit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
předsedkyně	předsedkyně	k1gFnSc1	předsedkyně
Eliška	Eliška	k1gFnSc1	Eliška
Wagnerová	Wagnerová	k1gFnSc1	Wagnerová
ještě	ještě	k9	ještě
bez	bez	k7c2	bez
zákonné	zákonný	k2eAgFnSc2d1	zákonná
úpravy	úprava	k1gFnSc2	úprava
pozice	pozice	k1gFnSc2	pozice
asistentů	asistent	k1gMnPc2	asistent
soudců	soudce	k1gMnPc2	soudce
občanskoprávního	občanskoprávní	k2eAgNnSc2d1	občanskoprávní
kolegia	kolegium	k1gNnSc2	kolegium
a	a	k8xC	a
následně	následně	k6eAd1	následně
iniciovala	iniciovat	k5eAaBmAgFnS	iniciovat
jejich	jejich	k3xOp3gNnSc4	jejich
zákonné	zákonný	k2eAgNnSc4d1	zákonné
zakotvení	zakotvení	k1gNnSc4	zakotvení
a	a	k8xC	a
uznávání	uznávání	k1gNnSc4	uznávání
jejich	jejich	k3xOp3gFnSc2	jejich
praxe	praxe	k1gFnSc2	praxe
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
postupně	postupně	k6eAd1	postupně
upraveno	upravit	k5eAaPmNgNnS	upravit
v	v	k7c6	v
zákoně	zákon	k1gInSc6	zákon
o	o	k7c6	o
soudech	soud	k1gInPc6	soud
a	a	k8xC	a
soudcích	soudek	k1gInPc6	soudek
<g/>
,	,	kIx,	,
občanském	občanský	k2eAgInSc6d1	občanský
soudním	soudní	k2eAgInSc6d1	soudní
řádu	řád	k1gInSc6	řád
<g/>
,	,	kIx,	,
trestním	trestní	k2eAgInSc6d1	trestní
řádu	řád	k1gInSc6	řád
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
zákonech	zákon	k1gInPc6	zákon
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
právnických	právnický	k2eAgFnPc2d1	právnická
povolání	povolání	k1gNnPc2	povolání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
odvolal	odvolat	k5eAaPmAgMnS	odvolat
bez	bez	k7c2	bez
bližšího	blízký	k2eAgNnSc2d2	bližší
zdůvodnění	zdůvodnění	k1gNnSc2	zdůvodnění
prezident	prezident	k1gMnSc1	prezident
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
předsedkyni	předsedkyně	k1gFnSc4	předsedkyně
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
Ivu	Iva	k1gFnSc4	Iva
Brožovou	Brožová	k1gFnSc4	Brožová
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
odvolání	odvolání	k1gNnSc3	odvolání
bránila	bránit	k5eAaImAgFnS	bránit
u	u	k7c2	u
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
její	její	k3xOp3gNnSc4	její
odvolání	odvolání	k1gNnSc4	odvolání
zrušil	zrušit	k5eAaPmAgMnS	zrušit
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
zrušil	zrušit	k5eAaPmAgMnS	zrušit
tu	ten	k3xDgFnSc4	ten
část	část	k1gFnSc4	část
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
soudech	soud	k1gInPc6	soud
a	a	k8xC	a
soudcích	soudek	k1gInPc6	soudek
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
odvolání	odvolání	k1gNnSc4	odvolání
předsedů	předseda	k1gMnPc2	předseda
a	a	k8xC	a
místopředsedů	místopředseda	k1gMnPc2	místopředseda
soudů	soud	k1gInPc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
přidělení	přidělení	k1gNnSc6	přidělení
k	k	k7c3	k
Nejvyššímu	vysoký	k2eAgInSc3d3	Nejvyšší
soudu	soud	k1gInSc3	soud
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
prezident	prezident	k1gMnSc1	prezident
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Bureše	Bureš	k1gMnSc2	Bureš
místopředsedou	místopředseda	k1gMnSc7	místopředseda
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
toto	tento	k3xDgNnSc4	tento
přidělení	přidělení	k1gNnSc4	přidělení
a	a	k8xC	a
jmenování	jmenování	k1gNnSc4	jmenování
zrušil	zrušit	k5eAaPmAgMnS	zrušit
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
Brožové	Brožová	k1gFnSc2	Brožová
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
s	s	k7c7	s
odůvodněním	odůvodnění	k1gNnSc7	odůvodnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
předsedu	předseda	k1gMnSc4	předseda
a	a	k8xC	a
místopředsedu	místopředseda	k1gMnSc4	místopředseda
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
lze	lze	k6eAd1	lze
jmenovat	jmenovat	k5eAaBmF	jmenovat
jen	jen	k9	jen
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
soudců	soudce	k1gMnPc2	soudce
tohoto	tento	k3xDgInSc2	tento
soudu	soud	k1gInSc2	soud
a	a	k8xC	a
ne	ne	k9	ne
všech	všecek	k3xTgMnPc2	všecek
soudců	soudce	k1gMnPc2	soudce
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
JUDr.	JUDr.	kA	JUDr.
Bureš	Bureš	k1gMnSc1	Bureš
nebyl	být	k5eNaImAgMnS	být
jako	jako	k9	jako
soudce	soudce	k1gMnSc1	soudce
platně	platně	k6eAd1	platně
přidělen	přidělit	k5eAaPmNgMnS	přidělit
k	k	k7c3	k
Nejvyššímu	vysoký	k2eAgInSc3d3	Nejvyšší
soudu	soud	k1gInSc3	soud
pro	pro	k7c4	pro
absenci	absence	k1gFnSc4	absence
povinného	povinný	k2eAgInSc2d1	povinný
souhlasu	souhlas	k1gInSc2	souhlas
jeho	jeho	k3xOp3gFnSc2	jeho
předsedkyně	předsedkyně	k1gFnSc2	předsedkyně
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
tomuto	tento	k3xDgInSc3	tento
výroku	výrok	k1gInSc3	výrok
jako	jako	k8xS	jako
neústavnímu	ústavní	k2eNgInSc3d1	neústavní
protestovala	protestovat	k5eAaBmAgFnS	protestovat
část	část	k1gFnSc1	část
ústavních	ústavní	k2eAgMnPc2d1	ústavní
soudců	soudce	k1gMnPc2	soudce
i	i	k8xC	i
další	další	k2eAgFnSc2d1	další
právnické	právnický	k2eAgFnSc2d1	právnická
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Ústava	ústava	k1gFnSc1	ústava
v	v	k7c6	v
čl	čl	kA	čl
<g/>
.	.	kIx.	.
62	[number]	k4	62
písm	písmo	k1gNnPc2	písmo
<g/>
.	.	kIx.	.
f	f	k?	f
<g/>
)	)	kIx)	)
stanoví	stanovit	k5eAaPmIp3nS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
předseda	předseda	k1gMnSc1	předseda
a	a	k8xC	a
místopředsedové	místopředseda	k1gMnPc1	místopředseda
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
jsou	být	k5eAaImIp3nP	být
jmenováni	jmenován	k2eAgMnPc1d1	jmenován
"	"	kIx"	"
<g/>
ze	z	k7c2	z
soudců	soudce	k1gMnPc2	soudce
<g/>
"	"	kIx"	"
bez	bez	k7c2	bez
výslovné	výslovný	k2eAgFnSc2d1	výslovná
bližší	blízký	k2eAgFnSc2d2	bližší
specifikace	specifikace	k1gFnSc2	specifikace
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
právní	právní	k2eAgFnSc2d1	právní
veřejnosti	veřejnost	k1gFnSc2	veřejnost
naopak	naopak	k6eAd1	naopak
výsledek	výsledek	k1gInSc4	výsledek
celého	celý	k2eAgInSc2d1	celý
sporu	spor	k1gInSc2	spor
mezi	mezi	k7c7	mezi
předsedkyní	předsedkyně	k1gFnSc7	předsedkyně
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
a	a	k8xC	a
prezidentem	prezident	k1gMnSc7	prezident
republiky	republika	k1gFnSc2	republika
přivítala	přivítat	k5eAaPmAgFnS	přivítat
<g/>
.	.	kIx.	.
</s>
<s>
Československý	československý	k2eAgInSc1d1	československý
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
měl	mít	k5eAaImAgInS	mít
původně	původně	k6eAd1	původně
sídlit	sídlit	k5eAaImF	sídlit
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k9	už
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
byl	být	k5eAaImAgInS	být
přesunut	přesunout	k5eAaPmNgInS	přesunout
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sídlil	sídlit	k5eAaImAgInS	sídlit
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Vrchním	vrchní	k2eAgInSc7d1	vrchní
zemským	zemský	k2eAgInSc7d1	zemský
soudem	soud	k1gInSc7	soud
<g/>
,	,	kIx,	,
Zemským	zemský	k2eAgInSc7d1	zemský
soudem	soud	k1gInSc7	soud
pro	pro	k7c4	pro
věci	věc	k1gFnPc4	věc
civilní	civilní	k2eAgFnPc4d1	civilní
<g/>
,	,	kIx,	,
Okresním	okresní	k2eAgInSc7d1	okresní
soudem	soud	k1gInSc7	soud
Brno-město	Brnoěsta	k1gMnSc5	Brno-města
pro	pro	k7c4	pro
věci	věc	k1gFnPc4	věc
civilní	civilní	k2eAgNnSc4d1	civilní
a	a	k8xC	a
Okresním	okresní	k2eAgInSc7d1	okresní
soudem	soud	k1gInSc7	soud
Brno-venkov	Brnoenkov	k1gInSc1	Brno-venkov
pro	pro	k7c4	pro
věci	věc	k1gFnPc4	věc
civilní	civilní	k2eAgFnPc4d1	civilní
<g/>
)	)	kIx)	)
v	v	k7c6	v
justičním	justiční	k2eAgInSc6d1	justiční
paláci	palác	k1gInSc6	palác
na	na	k7c6	na
Rooseveltově	Rooseveltův	k2eAgFnSc6d1	Rooseveltova
ulici	ulice	k1gFnSc6	ulice
(	(	kIx(	(
<g/>
nakonec	nakonec	k9	nakonec
sídlo	sídlo	k1gNnSc1	sídlo
pouze	pouze	k6eAd1	pouze
brněnského	brněnský	k2eAgInSc2d1	brněnský
krajského	krajský	k2eAgInSc2d1	krajský
soudu	soud	k1gInSc2	soud
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
ovšem	ovšem	k9	ovšem
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
tyto	tento	k3xDgFnPc4	tento
instituce	instituce	k1gFnPc4	instituce
kapacitně	kapacitně	k6eAd1	kapacitně
nepostačoval	postačovat	k5eNaImAgMnS	postačovat
<g/>
,	,	kIx,	,
detašované	detašovaný	k2eAgNnSc4d1	detašované
pracoviště	pracoviště	k1gNnSc4	pracoviště
v	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
zemském	zemský	k2eAgInSc6d1	zemský
domě	dům	k1gInSc6	dům
nebylo	být	k5eNaImAgNnS	být
vyhovující	vyhovující	k2eAgNnSc1d1	vyhovující
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
o	o	k7c6	o
výstavbě	výstavba	k1gFnSc6	výstavba
samostatné	samostatný	k2eAgFnSc2d1	samostatná
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Vybráno	vybrán	k2eAgNnSc1d1	vybráno
bylo	být	k5eAaImAgNnS	být
místo	místo	k1gNnSc1	místo
na	na	k7c6	na
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
Akademickém	akademický	k2eAgNnSc6d1	akademické
náměstí	náměstí	k1gNnSc6	náměstí
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Veveří	veveří	k2eAgFnSc6d1	veveří
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měly	mít	k5eAaImAgFnP	mít
vyrůst	vyrůst	k5eAaPmF	vyrůst
i	i	k9	i
budovy	budova	k1gFnPc1	budova
české	český	k2eAgFnSc2d1	Česká
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
sporům	spor	k1gInPc3	spor
o	o	k7c4	o
staveniště	staveniště	k1gNnSc4	staveniště
a	a	k8xC	a
politické	politický	k2eAgFnSc3d1	politická
situaci	situace	k1gFnSc3	situace
na	na	k7c6	na
konci	konec	k1gInSc6	konec
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
však	však	k9	však
k	k	k7c3	k
realizaci	realizace	k1gFnSc3	realizace
záměru	záměr	k1gInSc2	záměr
nakonec	nakonec	k6eAd1	nakonec
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
se	se	k3xPyFc4	se
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
přesunul	přesunout	k5eAaPmAgInS	přesunout
do	do	k7c2	do
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
a	a	k8xC	a
umístěn	umístěn	k2eAgMnSc1d1	umístěn
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
justičním	justiční	k2eAgInSc6d1	justiční
paláci	palác	k1gInSc6	palác
na	na	k7c4	na
náměstí	náměstí	k1gNnSc4	náměstí
Hrdinů	Hrdina	k1gMnPc2	Hrdina
na	na	k7c6	na
Pankráci	Pankrác	k1gFnSc6	Pankrác
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
původně	původně	k6eAd1	původně
sídlil	sídlit	k5eAaImAgInS	sídlit
zemský	zemský	k2eAgInSc1d1	zemský
trestní	trestní	k2eAgInSc1d1	trestní
soud	soud	k1gInSc1	soud
(	(	kIx(	(
<g/>
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
sídlo	sídlo	k1gNnSc4	sídlo
pražského	pražský	k2eAgInSc2d1	pražský
vrchního	vrchní	k2eAgInSc2d1	vrchní
soudu	soud	k1gInSc2	soud
a	a	k8xC	a
vrchního	vrchní	k2eAgNnSc2d1	vrchní
státního	státní	k2eAgNnSc2d1	státní
zastupitelství	zastupitelství	k1gNnSc2	zastupitelství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
opět	opět	k6eAd1	opět
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
funkcionalistická	funkcionalistický	k2eAgFnSc1d1	funkcionalistická
budova	budova	k1gFnSc1	budova
původně	původně	k6eAd1	původně
Všeobecného	všeobecný	k2eAgInSc2d1	všeobecný
penzijního	penzijní	k2eAgInSc2d1	penzijní
ústavu	ústav	k1gInSc2	ústav
<g/>
,	,	kIx,	,
postavená	postavený	k2eAgFnSc1d1	postavená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
podle	podle	k7c2	podle
projektu	projekt	k1gInSc2	projekt
Emila	Emil	k1gMnSc2	Emil
Králíka	Králík	k1gMnSc2	Králík
<g/>
,	,	kIx,	,
profesora	profesor	k1gMnSc2	profesor
brněnské	brněnský	k2eAgFnSc2d1	brněnská
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
technické	technický	k2eAgFnSc2d1	technická
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
šestipodlažní	šestipodlažní	k2eAgFnSc1d1	šestipodlažní
budova	budova	k1gFnSc1	budova
s	s	k7c7	s
železobetonovým	železobetonový	k2eAgInSc7d1	železobetonový
skeletem	skelet	k1gInSc7	skelet
uzavírá	uzavírat	k5eAaPmIp3nS	uzavírat
a	a	k8xC	a
spojuje	spojovat	k5eAaImIp3nS	spojovat
řady	řada	k1gFnSc2	řada
domů	domů	k6eAd1	domů
dvou	dva	k4xCgFnPc2	dva
ulic	ulice	k1gFnPc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
administrativní	administrativní	k2eAgInSc1d1	administrativní
blok	blok	k1gInSc1	blok
je	být	k5eAaImIp3nS	být
oddělen	oddělen	k2eAgInSc1d1	oddělen
od	od	k7c2	od
bočních	boční	k2eAgFnPc2d1	boční
obytných	obytný	k2eAgFnPc2d1	obytná
částí	část	k1gFnPc2	část
prosklenými	prosklený	k2eAgNnPc7d1	prosklené
schodišti	schodiště	k1gNnPc7	schodiště
<g/>
.	.	kIx.	.
</s>
<s>
Prosklený	prosklený	k2eAgInSc1d1	prosklený
vchod	vchod	k1gInSc1	vchod
vede	vést	k5eAaImIp3nS	vést
do	do	k7c2	do
velkoryse	velkoryse	k6eAd1	velkoryse
pojatého	pojatý	k2eAgInSc2d1	pojatý
vestibulu	vestibul	k1gInSc2	vestibul
obloženého	obložený	k2eAgInSc2d1	obložený
mramorem	mramor	k1gInSc7	mramor
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zde	zde	k6eAd1	zde
sídlil	sídlit	k5eAaImAgInS	sídlit
Krajský	krajský	k2eAgInSc1d1	krajský
výbor	výbor	k1gInSc1	výbor
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
podle	podle	k7c2	podle
projektu	projekt	k1gInSc2	projekt
Milana	Milana	k1gFnSc1	Milana
Steinhausera	Steinhausera	k1gFnSc1	Steinhausera
nadstavba	nadstavba	k1gFnSc1	nadstavba
mansardového	mansardový	k2eAgNnSc2d1	mansardové
patra	patro	k1gNnSc2	patro
a	a	k8xC	a
do	do	k7c2	do
nádvoří	nádvoří	k1gNnSc2	nádvoří
byl	být	k5eAaImAgInS	být
vestavěn	vestavěn	k2eAgInSc1d1	vestavěn
sál	sál	k1gInSc1	sál
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
chráněna	chránit	k5eAaImNgFnS	chránit
jako	jako	k9	jako
nemovitá	movitý	k2eNgFnSc1d1	nemovitá
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1994	[number]	k4	1994
<g/>
–	–	k?	–
<g/>
1999	[number]	k4	1999
zde	zde	k6eAd1	zde
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
soudem	soud	k1gInSc7	soud
sídlila	sídlit	k5eAaImAgFnS	sídlit
i	i	k9	i
tehdy	tehdy	k6eAd1	tehdy
nově	nově	k6eAd1	nově
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
Fakulta	fakulta	k1gFnSc1	fakulta
informatiky	informatika	k1gFnSc2	informatika
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
však	však	k9	však
nebyla	být	k5eNaImAgFnS	být
stavěna	stavit	k5eAaImNgFnS	stavit
pro	pro	k7c4	pro
justiční	justiční	k2eAgInPc4d1	justiční
účely	účel	k1gInPc4	účel
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
Nejvyššímu	vysoký	k2eAgInSc3d3	Nejvyšší
soudu	soud	k1gInSc3	soud
kapacitně	kapacitně	k6eAd1	kapacitně
nedostačuje	dostačovat	k5eNaImIp3nS	dostačovat
a	a	k8xC	a
také	také	k9	také
její	její	k3xOp3gInSc1	její
technický	technický	k2eAgInSc1d1	technický
stav	stav	k1gInSc1	stav
není	být	k5eNaImIp3nS	být
vyhovující	vyhovující	k2eAgInSc1d1	vyhovující
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
prostorovým	prostorový	k2eAgInPc3d1	prostorový
problémům	problém	k1gInPc3	problém
soud	soud	k1gInSc1	soud
zakoupil	zakoupit	k5eAaPmAgInS	zakoupit
sousedící	sousedící	k2eAgFnSc4d1	sousedící
budovu	budova	k1gFnSc4	budova
na	na	k7c4	na
ulici	ulice	k1gFnSc4	ulice
Bayerova	Bayerův	k2eAgFnSc1d1	Bayerova
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
její	její	k3xOp3gFnSc1	její
nutná	nutný	k2eAgFnSc1d1	nutná
adaptace	adaptace	k1gFnSc1	adaptace
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
několik	několik	k4yIc4	několik
let	let	k1gInSc4	let
připravovanou	připravovaný	k2eAgFnSc7d1	připravovaná
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
hlavní	hlavní	k2eAgFnSc2d1	hlavní
budovy	budova	k1gFnSc2	budova
byla	být	k5eAaImAgFnS	být
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nedostatku	nedostatek	k1gInSc3	nedostatek
financí	finance	k1gFnPc2	finance
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
odložena	odložit	k5eAaPmNgFnS	odložit
na	na	k7c4	na
neurčito	neurčito	k1gNnSc4	neurčito
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
další	další	k2eAgNnSc4d1	další
řešení	řešení	k1gNnSc4	řešení
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
projekt	projekt	k1gInSc1	projekt
zcela	zcela	k6eAd1	zcela
nové	nový	k2eAgFnSc2d1	nová
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
navíc	navíc	k6eAd1	navíc
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
soudem	soud	k1gInSc7	soud
sídlilo	sídlit	k5eAaImAgNnS	sídlit
i	i	k9	i
Nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
státní	státní	k2eAgNnSc1d1	státní
zastupitelství	zastupitelství	k1gNnSc1	zastupitelství
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
Ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
přislíbilo	přislíbit	k5eAaPmAgNnS	přislíbit
dostatek	dostatek	k1gInSc4	dostatek
finančních	finanční	k2eAgInPc2d1	finanční
prostředků	prostředek	k1gInPc2	prostředek
na	na	k7c4	na
odstranění	odstranění	k1gNnSc4	odstranění
již	již	k6eAd1	již
zcela	zcela	k6eAd1	zcela
zchátralého	zchátralý	k2eAgInSc2d1	zchátralý
bývalého	bývalý	k2eAgInSc2d1	bývalý
bytového	bytový	k2eAgInSc2d1	bytový
domu	dům	k1gInSc2	dům
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Bayerova	Bayerův	k2eAgNnPc1d1	Bayerovo
a	a	k8xC	a
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
nové	nový	k2eAgFnSc2d1	nová
přístavby	přístavba	k1gFnSc2	přístavba
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
