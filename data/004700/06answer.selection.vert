<s>
Adamov	Adamov	k1gInSc1	Adamov
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
:	:	kIx,	:
Adamsthal	Adamsthal	k1gMnSc1	Adamsthal
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Blansko	Blansko	k1gNnSc4	Blansko
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
12	[number]	k4	12
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
Svitavy	Svitava	k1gFnSc2	Svitava
a	a	k8xC	a
Křtinského	Křtinský	k2eAgInSc2d1	Křtinský
potoka	potok	k1gInSc2	potok
v	v	k7c6	v
lesnatém	lesnatý	k2eAgNnSc6d1	lesnaté
údolí	údolí	k1gNnSc6	údolí
<g/>
.	.	kIx.	.
</s>
