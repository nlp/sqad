<s>
Arabské	arabský	k2eAgNnSc1d1	arabské
písmo	písmo	k1gNnSc1	písmo
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
řecké	řecký	k2eAgNnSc1d1	řecké
nebo	nebo	k8xC	nebo
hebrejské	hebrejský	k2eAgNnSc1d1	hebrejské
založeno	založen	k2eAgNnSc1d1	založeno
na	na	k7c6	na
fénickém	fénický	k2eAgInSc6d1	fénický
typu	typ	k1gInSc6	typ
písma	písmo	k1gNnSc2	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
latinky	latinka	k1gFnSc2	latinka
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
psanou	psaný	k2eAgFnSc4d1	psaná
formu	forma	k1gFnSc4	forma
písma	písmo	k1gNnSc2	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Nemá	mít	k5eNaImIp3nS	mít
sice	sice	k8xC	sice
velká	velký	k2eAgFnSc1d1	velká
a	a	k8xC	a
malá	malý	k2eAgNnPc1d1	malé
písmena	písmeno	k1gNnPc1	písmeno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zato	zato	k6eAd1	zato
většina	většina	k1gFnSc1	většina
písmen	písmeno	k1gNnPc2	písmeno
má	mít	k5eAaImIp3nS	mít
4	[number]	k4	4
podoby	podoba	k1gFnSc2	podoba
<g/>
:	:	kIx,	:
pro	pro	k7c4	pro
výskyt	výskyt	k1gInSc4	výskyt
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
nebo	nebo	k8xC	nebo
samostatně	samostatně	k6eAd1	samostatně
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
hebrejština	hebrejština	k1gFnSc1	hebrejština
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
zprava	zprava	k6eAd1	zprava
doleva	doleva	k6eAd1	doleva
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
nezaznamenává	zaznamenávat	k5eNaImIp3nS	zaznamenávat
krátké	krátký	k2eAgFnPc4d1	krátká
samohlásky	samohláska	k1gFnPc4	samohláska
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
náboženské	náboženský	k2eAgInPc1d1	náboženský
texty	text	k1gInPc1	text
<g/>
,	,	kIx,	,
učebnice	učebnice	k1gFnPc1	učebnice
a	a	k8xC	a
slova	slovo	k1gNnPc1	slovo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
záměně	záměna	k1gFnSc3	záměna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takových	takový	k3xDgInPc6	takový
textech	text	k1gInPc6	text
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
speciální	speciální	k2eAgFnPc1d1	speciální
značky	značka	k1gFnPc1	značka
pro	pro	k7c4	pro
samohlásky	samohláska	k1gFnPc4	samohláska
a	a	k8xC	a
hovoříme	hovořit	k5eAaImIp1nP	hovořit
o	o	k7c6	o
takových	takový	k3xDgInPc6	takový
textech	text	k1gInPc6	text
jako	jako	k8xS	jako
o	o	k7c6	o
vokalizovaných	vokalizovaný	k2eAgFnPc6d1	vokalizovaná
<g/>
.	.	kIx.	.
</s>
<s>
Přejatá	přejatý	k2eAgNnPc1d1	přejaté
cizí	cizí	k2eAgNnPc1d1	cizí
slova	slovo	k1gNnPc1	slovo
se	se	k3xPyFc4	se
často	často	k6eAd1	často
píší	psát	k5eAaImIp3nP	psát
s	s	k7c7	s
dlouhými	dlouhý	k2eAgFnPc7d1	dlouhá
samohláskami	samohláska	k1gFnPc7	samohláska
jen	jen	k6eAd1	jen
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
naznačena	naznačit	k5eAaPmNgFnS	naznačit
jejich	jejich	k3xOp3gFnSc1	jejich
výslovnost	výslovnost	k1gFnSc1	výslovnost
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
jinak	jinak	k6eAd1	jinak
u	u	k7c2	u
arabských	arabský	k2eAgNnPc2d1	arabské
slov	slovo	k1gNnPc2	slovo
Arab	Arab	k1gMnSc1	Arab
pozná	poznat	k5eAaPmIp3nS	poznat
z	z	k7c2	z
kontextu	kontext	k1gInSc2	kontext
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Arabské	arabský	k2eAgNnSc1d1	arabské
písmo	písmo	k1gNnSc1	písmo
se	se	k3xPyFc4	se
šířilo	šířit	k5eAaImAgNnS	šířit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
islámem	islám	k1gInSc7	islám
a	a	k8xC	a
přijaly	přijmout	k5eAaPmAgInP	přijmout
ho	on	k3xPp3gNnSc4	on
i	i	k9	i
další	další	k2eAgInPc1d1	další
jazyky	jazyk	k1gInPc1	jazyk
<g/>
,	,	kIx,	,
s	s	k7c7	s
arabštinou	arabština	k1gFnSc7	arabština
zcela	zcela	k6eAd1	zcela
nepříbuzné	příbuzný	k2eNgNnSc1d1	nepříbuzné
<g/>
:	:	kIx,	:
perština	perština	k1gFnSc1	perština
<g/>
,	,	kIx,	,
paštština	paštština	k1gFnSc1	paštština
<g/>
,	,	kIx,	,
balúčština	balúčština	k1gFnSc1	balúčština
<g/>
,	,	kIx,	,
urdština	urdština	k1gFnSc1	urdština
<g/>
,	,	kIx,	,
ujgurština	ujgurština	k1gFnSc1	ujgurština
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc4	některý
texty	text	k1gInPc4	text
psané	psaný	k2eAgInPc4d1	psaný
v	v	k7c6	v
staré	starý	k2eAgFnSc6d1	stará
albánštině	albánština	k1gFnSc6	albánština
aj.	aj.	kA	aj.
Pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
jazyky	jazyk	k1gInPc4	jazyk
se	se	k3xPyFc4	se
v	v	k7c6	v
písmu	písmo	k1gNnSc6	písmo
objevily	objevit	k5eAaPmAgInP	objevit
další	další	k2eAgInPc1d1	další
znaky	znak	k1gInPc1	znak
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
arabština	arabština	k1gFnSc1	arabština
nevyužívá	využívat	k5eNaImIp3nS	využívat
<g/>
,	,	kIx,	,
např.	např.	kA	např.
P	P	kA	P
<g/>
,	,	kIx,	,
Č	Č	kA	Č
<g/>
,	,	kIx,	,
V.	V.	kA	V.
Některé	některý	k3yIgInPc4	některý
jazyky	jazyk	k1gInPc4	jazyk
(	(	kIx(	(
<g/>
např.	např.	kA	např.
turečtina	turečtina	k1gFnSc1	turečtina
či	či	k8xC	či
kazaština	kazaština	k1gFnSc1	kazaština
<g/>
)	)	kIx)	)
později	pozdě	k6eAd2	pozdě
vyměnily	vyměnit	k5eAaPmAgFnP	vyměnit
arabské	arabský	k2eAgNnSc4d1	arabské
písmo	písmo	k1gNnSc4	písmo
za	za	k7c4	za
latinku	latinka	k1gFnSc4	latinka
nebo	nebo	k8xC	nebo
cyrilici	cyrilice	k1gFnSc4	cyrilice
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInPc1d1	základní
znaky	znak	k1gInPc1	znak
používané	používaný	k2eAgInPc1d1	používaný
pro	pro	k7c4	pro
arabštinu	arabština	k1gFnSc4	arabština
v	v	k7c6	v
osamoceném	osamocený	k2eAgInSc6d1	osamocený
tvaru	tvar	k1gInSc6	tvar
jsou	být	k5eAaImIp3nP	být
následující	následující	k2eAgNnPc1d1	následující
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Pořadí	pořadí	k1gNnSc1	pořadí
znaků	znak	k1gInPc2	znak
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
znázorněno	znázornit	k5eAaPmNgNnS	znázornit
arabsky	arabsky	k6eAd1	arabsky
zprava	zprava	k6eAd1	zprava
doleva	doleva	k6eAd1	doleva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
evropsky	evropsky	k6eAd1	evropsky
zleva	zleva	k6eAd1	zleva
doprava	doprava	k6eAd1	doprava
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Znak	znak	k1gInSc1	znak
hamza	hamz	k1gMnSc2	hamz
označuje	označovat	k5eAaImIp3nS	označovat
ráz	ráz	k1gInSc4	ráz
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zvuk	zvuk	k1gInSc1	zvuk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
předchází	předcházet	k5eAaImIp3nS	předcházet
samohlásce	samohláska	k1gFnSc3	samohláska
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
např.	např.	kA	např.
mezi	mezi	k7c7	mezi
e	e	k0	e
a	a	k8xC	a
u	u	k7c2	u
ve	v	k7c6	v
slově	slovo	k1gNnSc6	slovo
"	"	kIx"	"
<g/>
neumím	umět	k5eNaImIp1nS	umět
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Hamza	Hamza	k1gFnSc1	Hamza
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
objevit	objevit	k5eAaPmF	objevit
i	i	k9	i
na	na	k7c6	na
konci	konec	k1gInSc6	konec
slova	slovo	k1gNnSc2	slovo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
názvu	název	k1gInSc6	název
ا	ا	k?	ا
ا	ا	k?	ا
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
pak	pak	k6eAd1	pak
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
ve	v	k7c4	v
výslovnosti	výslovnost	k1gFnPc4	výslovnost
slyšitelná	slyšitelný	k2eAgFnSc1d1	slyšitelná
<g/>
,	,	kIx,	,
začne	začít	k5eAaPmIp3nS	začít
být	být	k5eAaImF	být
důležitá	důležitý	k2eAgNnPc4d1	důležité
např.	např.	kA	např.
při	při	k7c6	při
připojení	připojení	k1gNnSc6	připojení
pádové	pádový	k2eAgFnSc2d1	pádová
koncovky	koncovka	k1gFnSc2	koncovka
-u	-u	k?	-u
(	(	kIx(	(
<g/>
vyslovíme	vyslovit	k5eAaPmIp1nP	vyslovit
bajdá	bajdat	k5eAaImIp3nS	bajdat
<g/>
'	'	kIx"	'
<g/>
u	u	k7c2	u
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
bajdau	bajdau	k6eAd1	bajdau
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
hamza	hamza	k1gFnSc1	hamza
objevit	objevit	k5eAaPmF	objevit
před	před	k7c7	před
alifem	alif	k1gInSc7	alif
<g/>
,	,	kIx,	,
píše	psát	k5eAaImIp3nS	psát
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
vokalizační	vokalizační	k2eAgFnSc1d1	vokalizační
značka	značka	k1gFnSc1	značka
nad	nad	k7c4	nad
něj	on	k3xPp3gMnSc4	on
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
následuje	následovat	k5eAaImIp3nS	následovat
samohláska	samohláska	k1gFnSc1	samohláska
a	a	k8xC	a
nebo	nebo	k8xC	nebo
u	u	k7c2	u
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
pod	pod	k7c4	pod
něj	on	k3xPp3gMnSc4	on
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
následuje	následovat	k5eAaImIp3nS	následovat
i	i	k9	i
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
můžou	můžou	k?	můžou
stát	stát	k5eAaPmF	stát
nositeli	nositel	k1gMnSc3	nositel
hamzy	hamz	k1gInPc4	hamz
i	i	k8xC	i
písmena	písmeno	k1gNnPc4	písmeno
wáw	wáw	k?	wáw
a	a	k8xC	a
ya	ya	k?	ya
<g/>
.	.	kIx.	.
</s>
<s>
Slova	slovo	k1gNnPc1	slovo
začínající	začínající	k2eAgMnSc1d1	začínající
samohláskou	samohláska	k1gFnSc7	samohláska
se	se	k3xPyFc4	se
proto	proto	k6eAd1	proto
často	často	k6eAd1	často
píší	psát	k5eAaImIp3nP	psát
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
s	s	k7c7	s
alifem	alif	k1gInSc7	alif
a	a	k8xC	a
hamzou	hamza	k1gFnSc7	hamza
<g/>
.	.	kIx.	.
</s>
<s>
Hamza	Hamza	k1gFnSc1	Hamza
odpadá	odpadat	k5eAaImIp3nS	odpadat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
slovo	slovo	k1gNnSc1	slovo
má	mít	k5eAaImIp3nS	mít
podle	podle	k7c2	podle
pravidel	pravidlo	k1gNnPc2	pravidlo
ve	v	k7c6	v
výslovnosti	výslovnost	k1gFnSc6	výslovnost
navázat	navázat	k5eAaPmF	navázat
na	na	k7c4	na
předcházející	předcházející	k2eAgNnSc4d1	předcházející
slovo	slovo	k1gNnSc4	slovo
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
hamza	hamza	k1gFnSc1	hamza
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
nad	nad	k7c7	nad
nejčastějším	častý	k2eAgInSc7d3	nejčastější
začátkem	začátek	k1gInSc7	začátek
arabských	arabský	k2eAgNnPc2d1	arabské
jmen	jméno	k1gNnPc2	jméno
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
nad	nad	k7c7	nad
členem	člen	k1gMnSc7	člen
al-	al-	k?	al-
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Tato	tento	k3xDgNnPc1	tento
pravidla	pravidlo	k1gNnPc1	pravidlo
ovšem	ovšem	k9	ovšem
nejsou	být	k5eNaImIp3nP	být
vždy	vždy	k6eAd1	vždy
dodržována	dodržovat	k5eAaImNgNnP	dodržovat
striktně	striktně	k6eAd1	striktně
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
autor	autor	k1gMnSc1	autor
hamzu	hamz	k1gInSc2	hamz
vynechá	vynechat	k5eAaPmIp3nS	vynechat
i	i	k9	i
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
<g/>
.	.	kIx.	.
</s>
<s>
Znak	znak	k1gInSc1	znak
madda	maddo	k1gNnSc2	maddo
nad	nad	k7c7	nad
alifem	alif	k1gInSc7	alif
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
následoval	následovat	k5eAaImAgInS	následovat
alif	alif	k1gInSc1	alif
s	s	k7c7	s
hamzou	hamza	k1gFnSc7	hamza
a	a	k8xC	a
alif	alif	k1gMnSc1	alif
bez	bez	k7c2	bez
hamzy	hamza	k1gFnSc2	hamza
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
á	á	k0	á
s	s	k7c7	s
rázem	ráz	k1gInSc7	ráz
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
slova	slovo	k1gNnSc2	slovo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
arabštiny	arabština	k1gFnSc2	arabština
ho	on	k3xPp3gMnSc4	on
lze	lze	k6eAd1	lze
relativně	relativně	k6eAd1	relativně
často	často	k6eAd1	často
pozorovat	pozorovat	k5eAaImF	pozorovat
v	v	k7c6	v
perských	perský	k2eAgInPc6d1	perský
a	a	k8xC	a
kurdských	kurdský	k2eAgInPc6d1	kurdský
zeměpisných	zeměpisný	k2eAgInPc6d1	zeměpisný
názvech	název	k1gInPc6	název
(	(	kIx(	(
<g/>
např.	např.	kA	např.
آ	آ	k?	آ
Ázerbájdžán	Ázerbájdžán	k1gInSc1	Ázerbájdžán
<g/>
,	,	kIx,	,
آ	آ	k?	آ
ك	ك	k?	ك
Ázád	Ázád	k1gInSc1	Ázád
Kašmír	Kašmír	k1gInSc1	Kašmír
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
Pro	pro	k7c4	pro
krátké	krátký	k2eAgFnPc4d1	krátká
samohlásky	samohláska	k1gFnPc4	samohláska
existují	existovat	k5eAaImIp3nP	existovat
vokalizační	vokalizační	k2eAgFnPc4d1	vokalizační
značky	značka	k1gFnPc4	značka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
píší	psát	k5eAaImIp3nP	psát
nad	nad	k7c4	nad
-	-	kIx~	-
a	a	k8xC	a
<g/>
,	,	kIx,	,
u	u	k7c2	u
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pod	pod	k7c4	pod
-	-	kIx~	-
i	i	k8xC	i
souhlásku	souhláska	k1gFnSc4	souhláska
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yRgFnSc4	který
samohláska	samohláska	k1gFnSc1	samohláska
následuje	následovat	k5eAaImIp3nS	následovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
samohlásky	samohláska	k1gFnSc2	samohláska
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
přidá	přidat	k5eAaPmIp3nS	přidat
znak	znak	k1gInSc1	znak
alif	alif	k1gInSc1	alif
v	v	k7c6	v
případě	případ	k1gInSc6	případ
á	á	k0	á
<g/>
,	,	kIx,	,
já	já	k3xPp1nSc1	já
v	v	k7c6	v
případě	případ	k1gInSc6	případ
í	í	k0	í
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
wáw	wáw	k?	wáw
v	v	k7c6	v
případě	případ	k1gInSc6	případ
ú	ú	k0	ú
jako	jako	k9	jako
prodloužení	prodloužení	k1gNnSc3	prodloužení
samohlásky	samohláska	k1gFnSc2	samohláska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nevokalizovaných	vokalizovaný	k2eNgInPc6d1	vokalizovaný
textech	text	k1gInPc6	text
tak	tak	k6eAd1	tak
ve	v	k7c6	v
výsledku	výsledek	k1gInSc6	výsledek
ze	z	k7c2	z
samohlásek	samohláska	k1gFnPc2	samohláska
zůstanou	zůstat	k5eAaPmIp3nP	zůstat
jen	jen	k9	jen
prodlužující	prodlužující	k2eAgInPc4d1	prodlužující
znaky	znak	k1gInPc4	znak
alif	alif	k1gMnSc1	alif
<g/>
,	,	kIx,	,
já	já	k3xPp1nSc1	já
a	a	k8xC	a
wáw	wáw	k?	wáw
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
existuje	existovat	k5eAaImIp3nS	existovat
ještě	ještě	k6eAd1	ještě
vokalizační	vokalizační	k2eAgFnSc1d1	vokalizační
značka	značka	k1gFnSc1	značka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c7	za
danou	daný	k2eAgFnSc7d1	daná
souhláskou	souhláska	k1gFnSc7	souhláska
žádná	žádný	k3yNgFnSc1	žádný
samohláska	samohláska	k1gFnSc1	samohláska
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
znaménko	znaménko	k1gNnSc1	znaménko
se	se	k3xPyFc4	se
nepoužije	použít	k5eNaPmIp3nS	použít
nad	nad	k7c7	nad
souhláskou	souhláska	k1gFnSc7	souhláska
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yIgFnSc4	který
následuje	následovat	k5eAaImIp3nS	následovat
tatáž	týž	k3xTgFnSc1	týž
souhláska	souhláska	k1gFnSc1	souhláska
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
použije	použít	k5eAaPmIp3nS	použít
vokalizační	vokalizační	k2eAgFnSc1d1	vokalizační
značka	značka	k1gFnSc1	značka
šadda	šaddo	k1gNnSc2	šaddo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
označuje	označovat	k5eAaImIp3nS	označovat
zdvojení	zdvojení	k1gNnSc4	zdvojení
souhlásky	souhláska	k1gFnSc2	souhláska
<g/>
.	.	kIx.	.
</s>
<s>
Vokalizační	vokalizační	k2eAgFnSc1d1	vokalizační
značka	značka	k1gFnSc1	značka
pro	pro	k7c4	pro
a	a	k8xC	a
<g/>
,	,	kIx,	,
i	i	k8xC	i
a	a	k8xC	a
u	u	k7c2	u
se	se	k3xPyFc4	se
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
arabském	arabský	k2eAgInSc6d1	arabský
textu	text	k1gInSc6	text
neobjevuje	objevovat	k5eNaImIp3nS	objevovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
čtenář	čtenář	k1gMnSc1	čtenář
znalý	znalý	k2eAgMnSc1d1	znalý
arabštiny	arabština	k1gFnPc4	arabština
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
dokáže	dokázat	k5eAaPmIp3nS	dokázat
domyslet	domyslet	k5eAaPmF	domyslet
z	z	k7c2	z
kontextu	kontext	k1gInSc2	kontext
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
v	v	k7c6	v
písmu	písmo	k1gNnSc6	písmo
objeví	objevit	k5eAaPmIp3nS	objevit
dvě	dva	k4xCgFnPc4	dva
stejné	stejná	k1gFnPc4	stejná
souhlásky	souhláska	k1gFnSc2	souhláska
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
máme	mít	k5eAaImIp1nP	mít
jistotu	jistota	k1gFnSc4	jistota
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
nějaká	nějaký	k3yIgFnSc1	nějaký
samohláska	samohláska	k1gFnSc1	samohláska
(	(	kIx(	(
<g/>
např.	např.	kA	např.
م	م	k?	م
mamlaka	mamlak	k1gMnSc2	mamlak
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
království	království	k1gNnSc2	království
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
značky	značka	k1gFnPc1	značka
jako	jako	k8xS	jako
třeba	třeba	k9	třeba
šadda	šaddo	k1gNnSc2	šaddo
(	(	kIx(	(
<g/>
označující	označující	k2eAgNnPc1d1	označující
zdvojení	zdvojení	k1gNnPc1	zdvojení
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
i	i	k9	i
v	v	k7c6	v
nevokalizovaném	vokalizovaný	k2eNgInSc6d1	vokalizovaný
textu	text	k1gInSc6	text
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Unicodu	Unicod	k1gInSc6	Unicod
jsou	být	k5eAaImIp3nP	být
arabská	arabský	k2eAgNnPc4d1	arabské
písmena	písmeno	k1gNnPc4	písmeno
od	od	k7c2	od
U	U	kA	U
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
60	[number]	k4	60
<g/>
C	C	kA	C
do	do	k7c2	do
U	U	kA	U
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
F	F	kA	F
<g/>
9	[number]	k4	9
a	a	k8xC	a
od	od	k7c2	od
U	U	kA	U
<g/>
+	+	kIx~	+
<g/>
FB	FB	kA	FB
<g/>
56	[number]	k4	56
do	do	k7c2	do
U	U	kA	U
<g/>
+	+	kIx~	+
<g/>
FEFC	FEFC	kA	FEFC
<g/>
.	.	kIx.	.
</s>
<s>
Samohlásky	samohláska	k1gFnPc1	samohláska
jsou	být	k5eAaImIp3nP	být
vyznačovány	vyznačovat	k5eAaImNgFnP	vyznačovat
pomocí	pomocí	k7c2	pomocí
vokalizačních	vokalizační	k2eAgFnPc2d1	vokalizační
značek	značka	k1gFnPc2	značka
<g/>
.	.	kIx.	.
</s>
<s>
Arabština	arabština	k1gFnSc1	arabština
Seznam	seznam	k1gInSc1	seznam
písem	písmo	k1gNnPc2	písmo
(	(	kIx(	(
<g/>
abecedně	abecedně	k6eAd1	abecedně
<g/>
)	)	kIx)	)
Seznam	seznam	k1gInSc1	seznam
písem	písmo	k1gNnPc2	písmo
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
skupin	skupina	k1gFnPc2	skupina
<g/>
)	)	kIx)	)
</s>
