<p>
<s>
Oliver	Oliver	k1gInSc1	Oliver
Dohnányi	Dohnány	k1gFnSc2	Dohnány
(	(	kIx(	(
<g/>
Oliver	Oliver	k1gInSc1	Oliver
von	von	k1gInSc1	von
Dohnányi	Dohnány	k1gFnSc2	Dohnány
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
*	*	kIx~	*
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
Trenčín	Trenčín	k1gInSc1	Trenčín
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
operní	operní	k2eAgMnSc1d1	operní
a	a	k8xC	a
symfonický	symfonický	k2eAgMnSc1d1	symfonický
dirigent	dirigent	k1gMnSc1	dirigent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
šéfdirigentem	šéfdirigent	k1gMnSc7	šéfdirigent
Státního	státní	k2eAgNnSc2d1	státní
divadla	divadlo	k1gNnSc2	divadlo
opery	opera	k1gFnSc2	opera
a	a	k8xC	a
baletu	balet	k1gInSc2	balet
v	v	k7c6	v
Jekatěrinburgu	Jekatěrinburg	k1gInSc6	Jekatěrinburg
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
inscenace	inscenace	k1gFnSc1	inscenace
opery	opera	k1gFnSc2	opera
Philipa	Philipa	k1gFnSc1	Philipa
Glasse	Glasse	k1gFnSc1	Glasse
Satyagraha	Satyagraha	k1gFnSc1	Satyagraha
se	s	k7c7	s
souborem	soubor	k1gInSc7	soubor
Opery	opera	k1gFnSc2	opera
z	z	k7c2	z
Jekatěrinburgu	Jekatěrinburg	k1gInSc2	Jekatěrinburg
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
dvě	dva	k4xCgFnPc1	dva
Zlaté	zlatý	k2eAgFnPc1d1	zlatá
masky	maska	k1gFnPc1	maska
na	na	k7c6	na
celoruském	celoruský	k2eAgInSc6d1	celoruský
prestižním	prestižní	k2eAgInSc6d1	prestižní
divadelním	divadelní	k2eAgInSc6d1	divadelní
festivalu	festival	k1gInSc6	festival
(	(	kIx(	(
<g/>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
maska	maska	k1gFnSc1	maska
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
na	na	k7c6	na
stejném	stejný	k2eAgInSc6d1	stejný
festivalu	festival	k1gInSc6	festival
získal	získat	k5eAaPmAgMnS	získat
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
masku	maska	k1gFnSc4	maska
za	za	k7c4	za
inscenaci	inscenace	k1gFnSc4	inscenace
opery	opera	k1gFnSc2	opera
Mečislava	Mečislava	k1gFnSc1	Mečislava
Weinberga	Weinberga	k1gFnSc1	Weinberga
Pasažérka	pasažérka	k1gFnSc1	pasažérka
"	"	kIx"	"
<g/>
Za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
práci	práce	k1gFnSc4	práce
dirigenta	dirigent	k1gMnSc2	dirigent
v	v	k7c6	v
opeře	opera	k1gFnSc6	opera
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
výběru	výběr	k1gInSc6	výběr
bylo	být	k5eAaImAgNnS	být
zhruba	zhruba	k6eAd1	zhruba
85	[number]	k4	85
inscenací	inscenace	k1gFnPc2	inscenace
a	a	k8xC	a
dirigentů	dirigent	k1gMnPc2	dirigent
z	z	k7c2	z
celého	celý	k2eAgNnSc2d1	celé
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
skladbu	skladba	k1gFnSc4	skladba
<g/>
,	,	kIx,	,
dirigovaní	dirigovaný	k2eAgMnPc1d1	dirigovaný
a	a	k8xC	a
houslovou	houslový	k2eAgFnSc4d1	houslová
hru	hra	k1gFnSc4	hra
na	na	k7c6	na
bratislavské	bratislavský	k2eAgFnSc6d1	Bratislavská
Konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
dirigování	dirigování	k1gNnSc1	dirigování
u	u	k7c2	u
Václava	Václav	k1gMnSc2	Václav
Neumanna	Neumann	k1gMnSc2	Neumann
a	a	k8xC	a
Aloise	Alois	k1gMnSc2	Alois
Klímy	Klíma	k1gMnSc2	Klíma
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
Akademii	akademie	k1gFnSc6	akademie
múzických	múzický	k2eAgNnPc2d1	múzické
umění	umění	k1gNnPc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
studiích	studio	k1gNnPc6	studio
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
u	u	k7c2	u
dirigenta	dirigent	k1gMnSc2	dirigent
Otmara	Otmar	k1gMnSc2	Otmar
Suitnera	Suitner	k1gMnSc2	Suitner
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
hudebních	hudební	k2eAgNnPc2d1	hudební
a	a	k8xC	a
dramatických	dramatický	k2eAgNnPc2d1	dramatické
umění	umění	k1gNnPc2	umění
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
a	a	k8xC	a
navštívil	navštívit	k5eAaPmAgMnS	navštívit
mistrovské	mistrovský	k2eAgInPc4d1	mistrovský
kurzy	kurz	k1gInPc4	kurz
u	u	k7c2	u
Igora	Igor	k1gMnSc2	Igor
Markeviče	Markevič	k1gMnSc2	Markevič
<g/>
,	,	kIx,	,
Arvida	Arvid	k1gMnSc2	Arvid
Jansonse	Jansons	k1gMnSc2	Jansons
a	a	k8xC	a
Franca	Franca	k?	Franca
Ferrary	Ferrara	k1gFnSc2	Ferrara
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
finalistou	finalista	k1gMnSc7	finalista
významných	významný	k2eAgFnPc2d1	významná
dirigentských	dirigentský	k2eAgFnPc2d1	dirigentská
soutěží	soutěž	k1gFnPc2	soutěž
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Dirigentské	dirigentský	k2eAgFnPc4d1	dirigentská
soutěže	soutěž	k1gFnPc4	soutěž
Maďarské	maďarský	k2eAgFnSc2d1	maďarská
televize	televize	k1gFnSc2	televize
nebo	nebo	k8xC	nebo
Talichovy	Talichův	k2eAgFnSc2d1	Talichova
soutěže	soutěž	k1gFnSc2	soutěž
Pražského	pražský	k2eAgNnSc2d1	Pražské
jara	jaro	k1gNnSc2	jaro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
hostující	hostující	k2eAgMnSc1d1	hostující
dirigent	dirigent	k1gMnSc1	dirigent
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
světových	světový	k2eAgInPc6d1	světový
operních	operní	k2eAgInPc6d1	operní
domech	dům	k1gInPc6	dům
a	a	k8xC	a
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
významné	významný	k2eAgInPc4d1	významný
orchestry	orchestr	k1gInPc4	orchestr
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1993	[number]	k4	1993
–	–	k?	–
1996	[number]	k4	1996
a	a	k8xC	a
2004	[number]	k4	2004
–	–	k?	–
2007	[number]	k4	2007
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
šéfdirigent	šéfdirigent	k1gMnSc1	šéfdirigent
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
nastudoval	nastudovat	k5eAaBmAgMnS	nastudovat
a	a	k8xC	a
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
tyto	tento	k3xDgFnPc4	tento
inscenace	inscenace	k1gFnPc4	inscenace
<g/>
:	:	kIx,	:
Libuše	Libuše	k1gFnSc1	Libuše
<g/>
,	,	kIx,	,
Prodaná	prodaný	k2eAgFnSc1d1	prodaná
nevěsta	nevěsta	k1gFnSc1	nevěsta
(	(	kIx(	(
<g/>
Smetana	Smetana	k1gMnSc1	Smetana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Romeo	Romeo	k1gMnSc1	Romeo
a	a	k8xC	a
Julie	Julie	k1gFnSc1	Julie
(	(	kIx(	(
<g/>
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
Gounod	Gounod	k1gInSc1	Gounod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
La	la	k0	la
bohéme	bohém	k1gMnSc5	bohém
<g/>
,	,	kIx,	,
Tosca	Tosca	k1gFnSc1	Tosca
(	(	kIx(	(
<g/>
Puccini	Puccin	k1gMnPc1	Puccin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Aida	Aida	k1gFnSc1	Aida
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Rigoletto	Rigoletto	k1gNnSc1	Rigoletto
(	(	kIx(	(
<g/>
G.	G.	kA	G.
Verdi	Verd	k1gMnPc1	Verd
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Samson	Samson	k1gMnSc1	Samson
a	a	k8xC	a
Dalila	Dalila	k1gFnSc1	Dalila
(	(	kIx(	(
<g/>
C.	C.	kA	C.
Saint-Saëns	Saint-Saëns	k1gInSc1	Saint-Saëns
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Norma	Norma	k1gFnSc1	Norma
(	(	kIx(	(
<g/>
V.	V.	kA	V.
Bellini	Bellin	k2eAgMnPc1d1	Bellin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
balety	balet	k1gInPc1	balet
Rómeo	Rómeo	k6eAd1	Rómeo
a	a	k8xC	a
Júlie	Júlie	k1gFnSc1	Júlie
<g/>
,	,	kIx,	,
Popelka	Popelka	k1gMnSc1	Popelka
(	(	kIx(	(
<g/>
S.	S.	kA	S.
Prokofjev	Prokofjev	k1gFnSc2	Prokofjev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kouzelná	kouzelný	k2eAgFnSc1d1	kouzelná
flétna	flétna	k1gFnSc1	flétna
<g/>
,	,	kIx,	,
Don	dona	k1gFnPc2	dona
Giovanni	Giovann	k1gMnPc1	Giovann
a	a	k8xC	a
Figarova	Figarův	k2eAgFnSc1d1	Figarova
svatba	svatba	k1gFnSc1	svatba
(	(	kIx(	(
<g/>
Mozart	Mozart	k1gMnSc1	Mozart
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
posledně	posledně	k6eAd1	posledně
jmenovanými	jmenovaný	k2eAgInPc7d1	jmenovaný
díly	díl	k1gInPc7	díl
slavil	slavit	k5eAaImAgInS	slavit
významné	významný	k2eAgInPc4d1	významný
úspěchy	úspěch	k1gInPc4	úspěch
na	na	k7c6	na
pravidelných	pravidelný	k2eAgNnPc6d1	pravidelné
turné	turné	k1gNnPc6	turné
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Státní	státní	k2eAgFnSc4d1	státní
operu	opera	k1gFnSc4	opera
Praha	Praha	k1gFnSc1	Praha
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
opery	opera	k1gFnSc2	opera
Lazebník	lazebník	k1gMnSc1	lazebník
sevillský	sevillský	k2eAgMnSc1d1	sevillský
a	a	k8xC	a
Turek	Turek	k1gMnSc1	Turek
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
(	(	kIx(	(
<g/>
Rossini	Rossin	k2eAgMnPc1d1	Rossin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Anglické	anglický	k2eAgFnSc6d1	anglická
národní	národní	k2eAgFnSc6d1	národní
opeře	opera	k1gFnSc6	opera
(	(	kIx(	(
<g/>
ENO	ENO	kA	ENO
<g/>
)	)	kIx)	)
uvedl	uvést	k5eAaPmAgMnS	uvést
opery	opera	k1gFnSc2	opera
Mefistofele	Mefistofeles	k1gMnSc5	Mefistofeles
(	(	kIx(	(
<g/>
A.	A.	kA	A.
Boito	Boito	k1gNnSc4	Boito
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Falstaff	Falstaff	k1gMnSc1	Falstaff
(	(	kIx(	(
<g/>
G.	G.	kA	G.
Verdi	Verd	k1gMnPc1	Verd
<g/>
)	)	kIx)	)
a	a	k8xC	a
Veselou	veselý	k2eAgFnSc4d1	veselá
vdovu	vdova	k1gFnSc4	vdova
(	(	kIx(	(
<g/>
Lehár	Lehár	k1gInSc4	Lehár
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
koncertní	koncertní	k2eAgNnSc4d1	koncertní
provedení	provedení	k1gNnSc4	provedení
Smetanovy	Smetanův	k2eAgFnSc2d1	Smetanova
Libuše	Libuše	k1gFnSc2	Libuše
na	na	k7c6	na
Edinburském	Edinburský	k2eAgInSc6d1	Edinburský
festivalu	festival	k1gInSc6	festival
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
dlouholetým	dlouholetý	k2eAgMnSc7d1	dlouholetý
hostujícím	hostující	k2eAgMnSc7d1	hostující
dirigentem	dirigent	k1gMnSc7	dirigent
Opery	opera	k1gFnSc2	opera
North	Northa	k1gFnPc2	Northa
a	a	k8xC	a
orchestru	orchestr	k1gInSc2	orchestr
Northern	Northern	k1gMnSc1	Northern
Philharmony	Philharmona	k1gFnSc2	Philharmona
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
Leedsu	Leeds	k1gInSc6	Leeds
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nastudoval	nastudovat	k5eAaBmAgInS	nastudovat
opery	opera	k1gFnSc2	opera
Carmen	Carmen	k1gInSc1	Carmen
(	(	kIx(	(
<g/>
G.	G.	kA	G.
Bizet	Bizet	k1gInSc1	Bizet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hamlet	Hamlet	k1gMnSc1	Hamlet
(	(	kIx(	(
<g/>
A.	A.	kA	A.
Thomas	Thomas	k1gMnSc1	Thomas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
Gioconda	Gioconda	k1gFnSc1	Gioconda
(	(	kIx(	(
<g/>
A.	A.	kA	A.
Ponchielli	Ponchielle	k1gFnSc6	Ponchielle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Norma	Norma	k1gFnSc1	Norma
(	(	kIx(	(
<g/>
V.	V.	kA	V.
Bellini	Bellin	k2eAgMnPc1d1	Bellin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Andea	Andea	k1gMnSc1	Andea
Chénier	Chénier	k1gMnSc1	Chénier
(	(	kIx(	(
<g/>
U.	U.	kA	U.
<g/>
Giordano	Giordana	k1gFnSc5	Giordana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
českého	český	k2eAgInSc2d1	český
operního	operní	k2eAgInSc2d1	operní
repertoáru	repertoár	k1gInSc2	repertoár
zde	zde	k6eAd1	zde
uvedl	uvést	k5eAaPmAgMnS	uvést
Smetanovu	Smetanův	k2eAgFnSc4d1	Smetanova
Prodanou	prodaný	k2eAgFnSc4d1	prodaná
nevěstu	nevěsta	k1gFnSc4	nevěsta
a	a	k8xC	a
Dvořákovu	Dvořákův	k2eAgFnSc4d1	Dvořákova
Rusalku	rusalka	k1gFnSc4	rusalka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
<g/>
1999	[number]	k4	1999
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
v	v	k7c4	v
Badisches	Badisches	k1gInSc4	Badisches
Staatstheater	Staatstheatra	k1gFnPc2	Staatstheatra
Karlsruhe	Karlsruh	k1gInSc2	Karlsruh
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nastudoval	nastudovat	k5eAaBmAgInS	nastudovat
opery	opera	k1gFnSc2	opera
Macbeth	Macbeth	k1gInSc1	Macbeth
(	(	kIx(	(
<g/>
G.	G.	kA	G.
Verdi	Verd	k1gMnPc1	Verd
<g/>
)	)	kIx)	)
a	a	k8xC	a
Romeo	Romeo	k1gMnSc1	Romeo
a	a	k8xC	a
Julie	Julie	k1gFnSc1	Julie
(	(	kIx(	(
<g/>
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
Gounod	Gounod	k1gInSc1	Gounod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Mannheimu	Mannheim	k1gInSc6	Mannheim
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
Dona	Don	k1gMnSc4	Don
Giovanniho	Giovanni	k1gMnSc4	Giovanni
(	(	kIx(	(
<g/>
W.A.	W.A.	k1gMnSc1	W.A.
<g/>
Mozart	Mozart	k1gMnSc1	Mozart
<g/>
)	)	kIx)	)
a	a	k8xC	a
Toscu	Tosca	k1gFnSc4	Tosca
(	(	kIx(	(
<g/>
G.	G.	kA	G.
<g/>
Puccini	Puccin	k1gMnPc1	Puccin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
ve	v	k7c6	v
Výmaru	Výmar	k1gInSc6	Výmar
Donna	donna	k1gFnSc1	donna
Giovanniho	Giovanni	k1gMnSc2	Giovanni
(	(	kIx(	(
<g/>
W.A.	W.A.	k1gMnSc1	W.A.
<g/>
Mozart	Mozart	k1gMnSc1	Mozart
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
jeho	jeho	k3xOp3gFnSc7	jeho
americký	americký	k2eAgInSc1d1	americký
debut	debut	k1gInSc1	debut
s	s	k7c7	s
orchestrem	orchestr	k1gInSc7	orchestr
Chicago	Chicago	k1gNnSc1	Chicago
Sinfonietta	Sinfoniett	k1gInSc2	Sinfoniett
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nahrávku	nahrávka	k1gFnSc4	nahrávka
televizní	televizní	k2eAgFnSc2d1	televizní
opery	opera	k1gFnSc2	opera
slovenského	slovenský	k2eAgMnSc2d1	slovenský
skladatele	skladatel	k1gMnSc2	skladatel
Juraja	Jurajus	k1gMnSc2	Jurajus
Filase	Filas	k1gInSc6	Filas
Memento	memento	k1gNnSc4	memento
Mori	Mor	k1gFnSc2	Mor
se	s	k7c7	s
Symfonickým	symfonický	k2eAgInSc7d1	symfonický
orchestrem	orchestr	k1gInSc7	orchestr
FOK	FOK	kA	FOK
získal	získat	k5eAaPmAgInS	získat
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
cenu	cena	k1gFnSc4	cena
na	na	k7c6	na
Televizním	televizní	k2eAgInSc6d1	televizní
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
rakouském	rakouský	k2eAgInSc6d1	rakouský
Salzburgu	Salzburg	k1gInSc6	Salzburg
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
pravidelně	pravidelně	k6eAd1	pravidelně
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
s	s	k7c7	s
Bavorskou	bavorský	k2eAgFnSc7d1	bavorská
státní	státní	k2eAgFnSc7d1	státní
operou	opera	k1gFnSc7	opera
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
úspěchem	úspěch	k1gInSc7	úspěch
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
Mozartovu	Mozartův	k2eAgFnSc4d1	Mozartova
Kouzelnou	kouzelný	k2eAgFnSc4d1	kouzelná
flétnu	flétna	k1gFnSc4	flétna
ve	v	k7c6	v
slavném	slavný	k2eAgMnSc6d1	slavný
Teatro	Teatro	k1gNnSc4	Teatro
Colón	colón	k1gInSc4	colón
v	v	k7c4	v
Buenos	Buenos	k1gInSc4	Buenos
Aires	Airesa	k1gFnPc2	Airesa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2006-2010	[number]	k4	2006-2010
působil	působit	k5eAaImAgInS	působit
též	též	k9	též
jako	jako	k9	jako
umělecký	umělecký	k2eAgMnSc1d1	umělecký
šéf	šéf	k1gMnSc1	šéf
opery	opera	k1gFnSc2	opera
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
moravskoslezského	moravskoslezský	k2eAgNnSc2d1	moravskoslezské
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
a	a	k8xC	a
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
opery	opera	k1gFnPc4	opera
<g/>
:	:	kIx,	:
La	la	k1gNnSc1	la
traviata	traviata	k1gFnSc1	traviata
(	(	kIx(	(
<g/>
G.	G.	kA	G.
<g/>
Verdi	Verd	k1gMnPc1	Verd
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vita	vit	k2eAgFnSc1d1	Vita
(	(	kIx(	(
<g/>
M.	M.	kA	M.
<g/>
Tuttino	Tuttin	k2eAgNnSc1d1	Tuttin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Faust	Faust	k1gMnSc1	Faust
(	(	kIx(	(
<g/>
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
Gounod	Gounod	k1gInSc1	Gounod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Carmen	Carmen	k1gInSc1	Carmen
(	(	kIx(	(
<g/>
G.	G.	kA	G.
Bizet	Bizet	k1gInSc1	Bizet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Don	Don	k1gMnSc1	Don
Giovanni	Giovanen	k2eAgMnPc1d1	Giovanen
a	a	k8xC	a
Figarova	Figarův	k2eAgFnSc1d1	Figarova
svatba	svatba	k1gFnSc1	svatba
(	(	kIx(	(
<g/>
W.	W.	kA	W.
A.	A.	kA	A.
Mozart	Mozart	k1gMnSc1	Mozart
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Prodaná	prodaný	k2eAgFnSc1d1	prodaná
nevěsta	nevěsta	k1gFnSc1	nevěsta
a	a	k8xC	a
Dalibor	Dalibor	k1gMnSc1	Dalibor
(	(	kIx(	(
<g/>
B.	B.	kA	B.
Smetana	Smetana	k1gMnSc1	Smetana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Její	její	k3xOp3gFnSc1	její
pastorkyňa	pastorkyňa	k1gFnSc1	pastorkyňa
(	(	kIx(	(
<g/>
L.	L.	kA	L.
Janáček	Janáček	k1gMnSc1	Janáček
<g/>
)	)	kIx)	)
a	a	k8xC	a
balet	balet	k1gInSc1	balet
Romeo	Romeo	k1gMnSc1	Romeo
a	a	k8xC	a
Julie	Julie	k1gFnSc1	Julie
(	(	kIx(	(
<g/>
S.	S.	kA	S.
Prokofjev	Prokofjev	k1gFnSc2	Prokofjev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2006	[number]	k4	2006
byl	být	k5eAaImAgMnS	být
vybrán	vybrat	k5eAaPmNgMnS	vybrat
na	na	k7c4	na
post	post	k1gInSc4	post
šéfdirigenta	šéfdirigent	k1gMnSc2	šéfdirigent
Symfonického	symfonický	k2eAgInSc2d1	symfonický
orchestru	orchestr	k1gInSc2	orchestr
slovenského	slovenský	k2eAgInSc2d1	slovenský
rozhlasu	rozhlas	k1gInSc2	rozhlas
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
a	a	k8xC	a
od	od	k7c2	od
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgMnS	být
také	také	k6eAd1	také
stálým	stálý	k2eAgMnSc7d1	stálý
hostujícím	hostující	k2eAgMnSc7d1	hostující
dirigentem	dirigent	k1gMnSc7	dirigent
Záhřebské	záhřebský	k2eAgFnSc2d1	Záhřebská
filharmonie	filharmonie	k1gFnSc2	filharmonie
a	a	k8xC	a
Symfonického	symfonický	k2eAgInSc2d1	symfonický
orchestru	orchestr	k1gInSc2	orchestr
Chorvatského	chorvatský	k2eAgInSc2d1	chorvatský
rádia	rádius	k1gInSc2	rádius
v	v	k7c6	v
Záhřebu	Záhřeb	k1gInSc6	Záhřeb
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
stálým	stálý	k2eAgMnSc7d1	stálý
hostujícím	hostující	k2eAgMnSc7d1	hostující
dirigentem	dirigent	k1gMnSc7	dirigent
opery	opera	k1gFnSc2	opera
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
Stuttgartu	Stuttgart	k1gInSc6	Stuttgart
<g/>
,	,	kIx,	,
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
nastudoval	nastudovat	k5eAaBmAgMnS	nastudovat
Smetanovu	Smetanův	k2eAgFnSc4d1	Smetanova
Prodanou	prodaný	k2eAgFnSc4d1	prodaná
nevěstu	nevěsta	k1gFnSc4	nevěsta
se	s	k7c7	s
souborem	soubor	k1gInSc7	soubor
opery	opera	k1gFnSc2	opera
v	v	k7c6	v
Baltimoru	Baltimore	k1gInSc6	Baltimore
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
úspěšně	úspěšně	k6eAd1	úspěšně
uvedl	uvést	k5eAaPmAgMnS	uvést
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandě	Zéland	k1gInSc6	Zéland
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
Aucklandu	Auckland	k1gInSc6	Auckland
a	a	k8xC	a
Wellingtonu	Wellington	k1gInSc6	Wellington
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
Mascagniho	Mascagni	k1gMnSc4	Mascagni
Sedláka	Sedlák	k1gMnSc4	Sedlák
kavalíra	kavalír	k1gMnSc4	kavalír
a	a	k8xC	a
Leoncavallovy	Leoncavallův	k2eAgMnPc4d1	Leoncavallův
Komedianty	komediant	k1gMnPc4	komediant
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
tam	tam	k6eAd1	tam
uvedl	uvést	k5eAaPmAgMnS	uvést
také	také	k9	také
Smetanovu	Smetanův	k2eAgFnSc4d1	Smetanova
Prodanou	prodaný	k2eAgFnSc4d1	prodaná
nevěstu	nevěsta	k1gFnSc4	nevěsta
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
Bizetovu	Bizetův	k2eAgFnSc4d1	Bizetova
Carmen	Carmen	k2eAgInSc4d1	Carmen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2013	[number]	k4	2013
a	a	k8xC	a
2014	[number]	k4	2014
dirigoval	dirigovat	k5eAaImAgInS	dirigovat
mimořádně	mimořádně	k6eAd1	mimořádně
úspěšné	úspěšný	k2eAgInPc4d1	úspěšný
novoroční	novoroční	k2eAgInPc4d1	novoroční
Gala-koncerty	Galaoncert	k1gInPc4	Gala-koncert
s	s	k7c7	s
Dallas	Dallas	k1gInSc4	Dallas
Symphony	Symphona	k1gFnSc2	Symphona
Orchestra	orchestra	k1gFnSc1	orchestra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
sezony	sezona	k1gFnSc2	sezona
2015	[number]	k4	2015
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
hlavním	hlavní	k2eAgMnSc7d1	hlavní
dirigentem	dirigent	k1gMnSc7	dirigent
Uralské	uralský	k2eAgFnSc2d1	Uralská
opery	opera	k1gFnSc2	opera
v	v	k7c6	v
Jekatěrinburgu	Jekatěrinburg	k1gInSc6	Jekatěrinburg
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
získal	získat	k5eAaPmAgMnS	získat
svou	svůj	k3xOyFgFnSc7	svůj
první	první	k4xOgFnSc7	první
inscenací	inscenace	k1gFnSc7	inscenace
opery	opera	k1gFnSc2	opera
Philipa	Philip	k1gMnSc2	Philip
Glasse	Glass	k1gMnSc2	Glass
"	"	kIx"	"
<g/>
Satyagraha	Satyagrah	k1gMnSc2	Satyagrah
<g/>
"	"	kIx"	"
dvě	dva	k4xCgFnPc1	dva
Zlaté	zlatý	k2eAgFnPc1d1	zlatá
masky	maska	k1gFnPc1	maska
-	-	kIx~	-
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
celoruské	celoruský	k2eAgNnSc1d1	celoruský
divadelní	divadelní	k2eAgNnSc1d1	divadelní
ocenění	ocenění	k1gNnSc1	ocenění
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
ceny	cena	k1gFnSc2	cena
poroty	porota	k1gFnSc2	porota
<g/>
.	.	kIx.	.
</s>
<s>
Produkce	produkce	k1gFnSc1	produkce
byla	být	k5eAaImAgFnS	být
představena	představit	k5eAaPmNgFnS	představit
v	v	k7c6	v
Moskevském	moskevský	k2eAgNnSc6d1	moskevské
Velkém	velký	k2eAgNnSc6d1	velké
divadle	divadlo	k1gNnSc6	divadlo
(	(	kIx(	(
<g/>
Bolshoi	Bolshoi	k1gNnSc1	Bolshoi
Theater	Theatra	k1gFnPc2	Theatra
<g/>
)	)	kIx)	)
během	během	k7c2	během
festivalu	festival	k1gInSc2	festival
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
maska	maska	k1gFnSc1	maska
a	a	k8xC	a
okamžitě	okamžitě	k6eAd1	okamžitě
přitáhla	přitáhnout	k5eAaPmAgFnS	přitáhnout
pozornost	pozornost	k1gFnSc1	pozornost
ruských	ruský	k2eAgMnPc2d1	ruský
i	i	k8xC	i
mezinárodních	mezinárodní	k2eAgMnPc2d1	mezinárodní
kritiků	kritik	k1gMnPc2	kritik
i	i	k8xC	i
odborníků	odborník	k1gMnPc2	odborník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
debutoval	debutovat	k5eAaBmAgMnS	debutovat
operou	opera	k1gFnSc7	opera
Leoše	Leoš	k1gMnSc2	Leoš
Janáčka	Janáček	k1gMnSc2	Janáček
Kátja	Kátjus	k1gMnSc2	Kátjus
Kabanová	Kabanová	k1gFnSc1	Kabanová
s	s	k7c7	s
americkým	americký	k2eAgInSc7d1	americký
souborem	soubor	k1gInSc7	soubor
Opera	opera	k1gFnSc1	opera
Seattle	Seattle	k1gFnSc1	Seattle
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
argentinského	argentinský	k2eAgInSc2d1	argentinský
Teatro	Teatro	k1gNnSc4	Teatro
Colon	colon	k1gNnPc2	colon
v	v	k7c4	v
Buenos	Buenos	k1gInSc4	Buenos
Aires	Airesa	k1gFnPc2	Airesa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
uvedl	uvést	k5eAaPmAgMnS	uvést
Wagnerovu	Wagnerův	k2eAgFnSc4d1	Wagnerova
prvotinu	prvotina	k1gFnSc4	prvotina
Zákaz	zákaz	k1gInSc4	zákaz
lásky	láska	k1gFnSc2	láska
(	(	kIx(	(
<g/>
Das	Das	k1gMnSc1	Das
Liebesverbot	Liebesverbot	k1gMnSc1	Liebesverbot
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
operu	opera	k1gFnSc4	opera
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2014-15	[number]	k4	2014-15
v	v	k7c4	v
Teatro	Teatro	k1gNnSc4	Teatro
Verdi	Verd	k1gMnPc1	Verd
<g/>
,	,	kIx,	,
Trieste	Triest	k1gInSc5	Triest
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2018	[number]	k4	2018
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Oliver	Oliver	k1gMnSc1	Oliver
Dohnányi	Dohnány	k1gFnSc2	Dohnány
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
Masku	maska	k1gFnSc4	maska
-	-	kIx~	-
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
cenu	cena	k1gFnSc4	cena
ruského	ruský	k2eAgNnSc2d1	ruské
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
"	"	kIx"	"
<g/>
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
práce	práce	k1gFnSc1	práce
dirigenta	dirigent	k1gMnSc2	dirigent
v	v	k7c6	v
opeře	opera	k1gFnSc6	opera
<g/>
"	"	kIx"	"
mezi	mezi	k7c4	mezi
cca	cca	kA	cca
<g/>
.	.	kIx.	.
85	[number]	k4	85
dirigentů	dirigent	k1gMnPc2	dirigent
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
pracují	pracovat	k5eAaImIp3nP	pracovat
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Slovenský	slovenský	k2eAgMnSc1d1	slovenský
dirigent	dirigent	k1gMnSc1	dirigent
Oliver	Oliver	k1gMnSc1	Oliver
Dohnányi	Dohnány	k1gMnSc3	Dohnány
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vede	vést	k5eAaImIp3nS	vést
soubor	soubor	k1gInSc4	soubor
jekatěrinburgské	jekatěrinburgský	k2eAgFnSc2d1	jekatěrinburgský
Opery	opera	k1gFnSc2	opera
(	(	kIx(	(
<g/>
Ural	Ural	k1gInSc4	Ural
Opera	opera	k1gFnSc1	opera
Balet	balet	k1gInSc1	balet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
oceněn	ocenit	k5eAaPmNgInS	ocenit
jako	jako	k8xC	jako
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
dirigent	dirigent	k1gMnSc1	dirigent
v	v	k7c6	v
opeře	opera	k1gFnSc6	opera
<g/>
.	.	kIx.	.
</s>
<s>
Operu	opera	k1gFnSc4	opera
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
provedl	provést	k5eAaPmAgInS	provést
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Passenger	Passengra	k1gFnPc2	Passengra
<g/>
"	"	kIx"	"
od	od	k7c2	od
Mieczyslawa	Mieczyslawus	k1gMnSc2	Mieczyslawus
Weinberga	Weinberg	k1gMnSc2	Weinberg
(	(	kIx(	(
<g/>
v	v	k7c6	v
ruské	ruský	k2eAgFnSc6d1	ruská
premiéře	premiéra	k1gFnSc6	premiéra
<g/>
)	)	kIx)	)
o	o	k7c6	o
přeživším	přeživší	k2eAgInSc6d1	přeživší
holocaustu	holocaust	k1gInSc6	holocaust
a	a	k8xC	a
jejím	její	k3xOp3gMnSc7	její
mučiteli	mučitel	k1gMnPc7	mučitel
<g/>
,	,	kIx,	,
získala	získat	k5eAaPmAgFnS	získat
další	další	k2eAgFnSc4d1	další
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
ženskou	ženská	k1gFnSc4	ženská
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
představení	představení	k1gNnSc1	představení
bylo	být	k5eAaImAgNnS	být
natočeno	natočit	k5eAaBmNgNnS	natočit
na	na	k7c4	na
DVD	DVD	kA	DVD
<g/>
.	.	kIx.	.
http://sputnikimages.com/hier_rubric/photo/5476301.html	[url]	k1gMnSc1	http://sputnikimages.com/hier_rubric/photo/5476301.html
</s>
</p>
<p>
<s>
Za	za	k7c4	za
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc1	působení
v	v	k7c6	v
Jekatěrinburgu	Jekatěrinburg	k1gInSc6	Jekatěrinburg
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
povedlo	povést	k5eAaPmAgNnS	povést
proměnit	proměnit	k5eAaPmF	proměnit
poměrně	poměrně	k6eAd1	poměrně
nevýrazný	výrazný	k2eNgInSc4d1	nevýrazný
oblastní	oblastní	k2eAgInSc4d1	oblastní
soubor	soubor	k1gInSc4	soubor
v	v	k7c4	v
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgNnPc2d3	nejlepší
operních	operní	k2eAgNnPc2d1	operní
divadel	divadlo	k1gNnPc2	divadlo
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gFnPc4	jeho
premiéry	premiéra	k1gFnPc4	premiéra
se	se	k3xPyFc4	se
sjíždějí	sjíždět	k5eAaImIp3nP	sjíždět
znalci	znalec	k1gMnPc1	znalec
i	i	k8xC	i
operní	operní	k2eAgMnPc1d1	operní
kritici	kritik	k1gMnPc1	kritik
nejen	nejen	k6eAd1	nejen
z	z	k7c2	z
celého	celý	k2eAgNnSc2d1	celé
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
již	již	k6eAd1	již
spomínaných	spomínaný	k2eAgFnPc2d1	spomínaná
oper	opera	k1gFnPc2	opera
uvedl	uvést	k5eAaPmAgMnS	uvést
v	v	k7c6	v
ruské	ruský	k2eAgFnSc6d1	ruská
premiéře	premiéra	k1gFnSc6	premiéra
taky	taky	k6eAd1	taky
Řecké	řecký	k2eAgFnSc2d1	řecká
pašije	pašije	k1gFnSc2	pašije
Bohuslava	Bohuslava	k1gFnSc1	Bohuslava
Martinů	Martinů	k1gFnSc1	Martinů
a	a	k8xC	a
dílo	dílo	k1gNnSc1	dílo
soudobého	soudobý	k2eAgMnSc2d1	soudobý
maďarského	maďarský	k2eAgMnSc2d1	maďarský
skladatele	skladatel	k1gMnSc2	skladatel
Petra	Petr	k1gMnSc2	Petr
Eotvose	Eotvosa	k1gFnSc6	Eotvosa
Tři	tři	k4xCgFnPc4	tři
sestry	sestra	k1gFnPc1	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
klasického	klasický	k2eAgInSc2d1	klasický
repertoáru	repertoár	k1gInSc2	repertoár
zde	zde	k6eAd1	zde
diriguje	dirigovat	k5eAaImIp3nS	dirigovat
zejména	zejména	k9	zejména
opery	opera	k1gFnPc1	opera
Turandot	Turandot	k1gInSc1	Turandot
<g/>
,	,	kIx,	,
Kouzelná	kouzelný	k2eAgFnSc1d1	kouzelná
flétna	flétna	k1gFnSc1	flétna
<g/>
,	,	kIx,	,
Rusalka	rusalka	k1gFnSc1	rusalka
<g/>
,	,	kIx,	,
Carmen	Carmen	k1gInSc1	Carmen
<g/>
,	,	kIx,	,
Bludný	bludný	k2eAgMnSc1d1	bludný
Holanďan	Holanďan	k1gMnSc1	Holanďan
<g/>
,	,	kIx,	,
Otello	Otello	k1gNnSc1	Otello
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
bohéme	bohém	k1gMnSc5	bohém
či	či	k8xC	či
Evžen	Evžen	k1gMnSc1	Evžen
Oněgin	Oněgin	k1gMnSc1	Oněgin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2019	[number]	k4	2019
se	se	k3xPyFc4	se
představil	představit	k5eAaPmAgMnS	představit
australským	australský	k2eAgMnSc7d1	australský
divákům	divák	k1gMnPc3	divák
inscenací	inscenace	k1gFnPc2	inscenace
Bizetovy	Bizetův	k2eAgInPc4d1	Bizetův
Carmen	Carmen	k1gInSc4	Carmen
v	v	k7c6	v
Adelaide	Adelaid	k1gInSc5	Adelaid
se	se	k3xPyFc4	se
Státní	státní	k2eAgMnPc1d1	státní
operou	oprat	k5eAaPmIp3nP	oprat
South	Southa	k1gFnPc2	Southa
Australia	Australium	k1gNnSc2	Australium
a	a	k8xC	a
v	v	k7c6	v
zápětí	zápětí	k1gNnSc6	zápětí
s	s	k7c7	s
Operou	opera	k1gFnSc7	opera
Queensland	Queenslanda	k1gFnPc2	Queenslanda
v	v	k7c6	v
Brisbane	Brisban	k1gMnSc5	Brisban
s	s	k7c7	s
vysoce	vysoce	k6eAd1	vysoce
oceňovanou	oceňovaný	k2eAgFnSc7d1	oceňovaná
produkcí	produkce	k1gFnSc7	produkce
Pucciniho	Puccini	k1gMnSc2	Puccini
Toscy	Tosca	k1gMnSc2	Tosca
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2019	[number]	k4	2019
uvedl	uvést	k5eAaPmAgMnS	uvést
koncertní	koncertní	k2eAgNnSc4d1	koncertní
provedení	provedení	k1gNnSc4	provedení
Dvořákovy	Dvořákův	k2eAgFnSc2d1	Dvořákova
Rusalky	rusalka	k1gFnSc2	rusalka
na	na	k7c4	na
Copenhagen	Copenhagen	k1gInSc4	Copenhagen
Opera	opera	k1gFnSc1	opera
Festival	festival	k1gInSc1	festival
s	s	k7c7	s
orchestrem	orchestr	k1gInSc7	orchestr
Slovenského	slovenský	k2eAgNnSc2d1	slovenské
národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
špičkovými	špičkový	k2eAgInPc7d1	špičkový
světovými	světový	k2eAgInPc7d1	světový
orchestry	orchestr	k1gInPc7	orchestr
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
Yomiuri	Yomiur	k1gMnPc1	Yomiur
Nippon	Nippon	k1gMnSc1	Nippon
SO	So	kA	So
Tokio	Tokio	k1gNnSc1	Tokio
<g/>
,	,	kIx,	,
Royal	Royal	k1gInSc1	Royal
Liverpool	Liverpool	k1gInSc1	Liverpool
Philharmonic	Philharmonice	k1gFnPc2	Philharmonice
<g/>
,	,	kIx,	,
Petrohradská	petrohradský	k2eAgFnSc1d1	Petrohradská
filharmonie	filharmonie	k1gFnSc1	filharmonie
<g/>
,	,	kIx,	,
Maďarská	maďarský	k2eAgFnSc1d1	maďarská
národní	národní	k2eAgFnSc1d1	národní
filharmonie	filharmonie	k1gFnSc1	filharmonie
<g/>
,	,	kIx,	,
Chicago	Chicago	k1gNnSc1	Chicago
Sinfonietta	Sinfoniett	k1gInSc2	Sinfoniett
<g/>
,	,	kIx,	,
Portugalský	portugalský	k2eAgInSc1d1	portugalský
symfonický	symfonický	k2eAgInSc1d1	symfonický
orchestr	orchestr	k1gInSc1	orchestr
<g/>
,	,	kIx,	,
Státní	státní	k2eAgFnSc2d1	státní
filharmonie	filharmonie	k1gFnSc2	filharmonie
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
filharmonie	filharmonie	k1gFnSc1	filharmonie
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
English	English	k1gMnSc1	English
Chamber	Chamber	k1gMnSc1	Chamber
Orchestra	orchestra	k1gFnSc1	orchestra
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
Mozart	Mozart	k1gMnSc1	Mozart
Players	Players	k1gInSc4	Players
<g/>
,	,	kIx,	,
English	English	k1gInSc4	English
Northern	Northerna	k1gFnPc2	Northerna
Philharmonia	Philharmonium	k1gNnSc2	Philharmonium
<g/>
,	,	kIx,	,
RSO	RSO	kA	RSO
Lugano	Lugana	k1gFnSc5	Lugana
<g/>
,	,	kIx,	,
I	i	k9	i
Solisti	Solist	k1gMnPc1	Solist
di	di	k?	di
Napoli	Napole	k1gFnSc3	Napole
<g/>
,	,	kIx,	,
RSO	RSO	kA	RSO
Saarbrücken	Saarbrücken	k1gInSc1	Saarbrücken
<g/>
,	,	kIx,	,
RSO	RSO	kA	RSO
Basel	Basel	k1gInSc1	Basel
<g/>
,	,	kIx,	,
Porto	porto	k1gNnSc1	porto
Philharmonic	Philharmonice	k1gFnPc2	Philharmonice
<g/>
,	,	kIx,	,
Irská	irský	k2eAgFnSc1d1	irská
národní	národní	k2eAgFnSc1d1	národní
filharmonie	filharmonie	k1gFnSc1	filharmonie
<g/>
,	,	kIx,	,
Ulster	Ulster	k1gInSc1	Ulster
Orchestra	orchestra	k1gFnSc1	orchestra
<g/>
,	,	kIx,	,
BBC	BBC	kA	BBC
Scottish	Scottish	k1gInSc4	Scottish
Symphony	Symphona	k1gFnSc2	Symphona
Orchestra	orchestra	k1gFnSc1	orchestra
Glasgow	Glasgow	k1gInSc1	Glasgow
<g/>
,	,	kIx,	,
Adelaide	Adelaid	k1gInSc5	Adelaid
Symphony	Symphona	k1gFnPc1	Symphona
Orchestra	orchestra	k1gFnSc1	orchestra
<g/>
,	,	kIx,	,
Queensland	Queensland	k1gInSc1	Queensland
Symphony	Symphona	k1gFnSc2	Symphona
Orchestra	orchestra	k1gFnSc1	orchestra
<g/>
,	,	kIx,	,
Orquesta	Orquesta	k1gMnSc1	Orquesta
Filarmónica	Filarmónicum	k1gNnSc2	Filarmónicum
Buenos	Buenos	k1gMnSc1	Buenos
Aires	Aires	k1gMnSc1	Aires
a	a	k8xC	a
New	New	k1gFnSc1	New
Zealand	Zealando	k1gNnPc2	Zealando
Symphony	Symphona	k1gFnSc2	Symphona
Orchestra	orchestra	k1gFnSc1	orchestra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Současnost	současnost	k1gFnSc4	současnost
===	===	k?	===
</s>
</p>
<p>
<s>
Dirigent	dirigent	k1gMnSc1	dirigent
Copenhagen	Copenhagen	k1gInSc4	Copenhagen
Opera	opera	k1gFnSc1	opera
Festival	festival	k1gInSc1	festival
(	(	kIx(	(
<g/>
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
od	od	k7c2	od
2019	[number]	k4	2019
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hostující	hostující	k2eAgMnSc1d1	hostující
dirigent	dirigent	k1gMnSc1	dirigent
Opera	opera	k1gFnSc1	opera
Queensland	Queenslando	k1gNnPc2	Queenslando
<g/>
,	,	kIx,	,
Brisbane	Brisban	k1gMnSc5	Brisban
(	(	kIx(	(
<g/>
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
od	od	k7c2	od
2019	[number]	k4	2019
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hostující	hostující	k2eAgMnSc1d1	hostující
dirigent	dirigent	k1gMnSc1	dirigent
State	status	k1gInSc5	status
Opera	opera	k1gFnSc1	opera
South	South	k1gInSc1	South
Australia	Australia	k1gFnSc1	Australia
<g/>
,	,	kIx,	,
Adelaide	Adelaid	k1gMnSc5	Adelaid
(	(	kIx(	(
<g/>
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
od	od	k7c2	od
2019	[number]	k4	2019
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hostující	hostující	k2eAgMnSc1d1	hostující
dirigent	dirigent	k1gMnSc1	dirigent
opery	opera	k1gFnSc2	opera
v	v	k7c6	v
Seattle	Seattle	k1gFnSc1	Seattle
(	(	kIx(	(
<g/>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
<g/>
A.	A.	kA	A.
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
od	od	k7c2	od
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hostující	hostující	k2eAgMnSc1d1	hostující
dirigent	dirigent	k1gMnSc1	dirigent
Teatro	Teatro	k1gNnSc4	Teatro
Colon	colon	k1gNnPc2	colon
<g/>
,	,	kIx,	,
Buenos	Buenos	k1gMnSc1	Buenos
Aires	Aires	k1gMnSc1	Aires
(	(	kIx(	(
<g/>
od	od	k7c2	od
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hostující	hostující	k2eAgMnSc1d1	hostující
dirigent	dirigent	k1gMnSc1	dirigent
Maďarské	maďarský	k2eAgFnSc2d1	maďarská
státní	státní	k2eAgFnSc2d1	státní
opery	opera	k1gFnSc2	opera
(	(	kIx(	(
<g/>
od	od	k7c2	od
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Šéfdirigent	šéfdirigent	k1gMnSc1	šéfdirigent
Státního	státní	k2eAgNnSc2d1	státní
divadla	divadlo	k1gNnSc2	divadlo
opery	opera	k1gFnSc2	opera
a	a	k8xC	a
baletu	balet	k1gInSc2	balet
<g/>
,	,	kIx,	,
Jekatěrinburg	Jekatěrinburg	k1gInSc1	Jekatěrinburg
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
od	od	k7c2	od
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Stálý	stálý	k2eAgMnSc1d1	stálý
hostující	hostující	k2eAgMnSc1d1	hostující
dirigent	dirigent	k1gMnSc1	dirigent
Opera	opera	k1gFnSc1	opera
North	North	k1gInSc1	North
<g/>
,	,	kIx,	,
Leeds	Leeds	k1gInSc1	Leeds
(	(	kIx(	(
<g/>
od	od	k7c2	od
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Stálý	stálý	k2eAgMnSc1d1	stálý
hostující	hostující	k2eAgMnSc1d1	hostující
dirigent	dirigent	k1gMnSc1	dirigent
Slovak	Slovak	k1gMnSc1	Slovak
Sinfonietta	Sinfonietta	k1gMnSc1	Sinfonietta
<g/>
,	,	kIx,	,
Žilina	Žilina	k1gFnSc1	Žilina
ŠKO	ŠKO	kA	ŠKO
(	(	kIx(	(
<g/>
od	od	k7c2	od
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Působení	působení	k1gNnSc2	působení
===	===	k?	===
</s>
</p>
<p>
<s>
Šéfdirigent	šéfdirigent	k1gMnSc1	šéfdirigent
orchestru	orchestr	k1gInSc2	orchestr
Divadla	divadlo	k1gNnSc2	divadlo
J.	J.	kA	J.
K.	K.	kA	K.
Tyla	Tyl	k1gMnSc2	Tyl
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
-	-	kIx~	-
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Šéf	šéf	k1gMnSc1	šéf
opery	opera	k1gFnSc2	opera
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
moravskoslezského	moravskoslezský	k2eAgNnSc2d1	moravskoslezské
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
-	-	kIx~	-
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Šéfdirigent	šéfdirigent	k1gMnSc1	šéfdirigent
opery	opera	k1gFnSc2	opera
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
-	-	kIx~	-
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Šéfdirigent	šéfdirigent	k1gMnSc1	šéfdirigent
Symfonického	symfonický	k2eAgInSc2d1	symfonický
orchestru	orchestr	k1gInSc2	orchestr
Slovenského	slovenský	k2eAgInSc2d1	slovenský
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
-2007	-2007	k4	-2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Šéfdirigent	šéfdirigent	k1gMnSc1	šéfdirigent
Slovak	Slovak	k1gMnSc1	Slovak
Sinfonietta	Sinfonietta	k1gMnSc1	Sinfonietta
<g/>
,	,	kIx,	,
Žilina	Žilina	k1gFnSc1	Žilina
ŠKO	ŠKO	kA	ŠKO
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
-	-	kIx~	-
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Stálý	stálý	k2eAgMnSc1d1	stálý
hostující	hostující	k2eAgMnSc1d1	hostující
dirigent	dirigent	k1gMnSc1	dirigent
Bavorské	bavorský	k2eAgFnSc2d1	bavorská
státní	státní	k2eAgFnSc2d1	státní
opery	opera	k1gFnSc2	opera
<g/>
,	,	kIx,	,
Mnichově	Mnichov	k1gInSc6	Mnichov
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
-	-	kIx~	-
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Stálý	stálý	k2eAgMnSc1d1	stálý
hostující	hostující	k2eAgMnSc1d1	hostující
dirigent	dirigent	k1gMnSc1	dirigent
Yomiuri	Yomiur	k1gFnSc2	Yomiur
Nippon	Nippon	k1gMnSc1	Nippon
Symphonic	Symphonice	k1gFnPc2	Symphonice
Orchestra	orchestra	k1gFnSc1	orchestra
<g/>
,	,	kIx,	,
Tokio	Tokio	k1gNnSc1	Tokio
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
-	-	kIx~	-
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Stálý	stálý	k2eAgMnSc1d1	stálý
hostující	hostující	k2eAgMnSc1d1	hostující
dirigent	dirigent	k1gMnSc1	dirigent
English	English	k1gMnSc1	English
National	National	k1gMnSc1	National
Opera	opera	k1gFnSc1	opera
ENO	ENO	kA	ENO
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
-	-	kIx~	-
</s>
</p>
<p>
<s>
Stálý	stálý	k2eAgMnSc1d1	stálý
hostující	hostující	k2eAgMnSc1d1	hostující
dirigent	dirigent	k1gMnSc1	dirigent
Opera	opera	k1gFnSc1	opera
North	North	k1gMnSc1	North
and	and	k?	and
Northern	Northern	k1gMnSc1	Northern
Philharmonia	Philharmonium	k1gNnSc2	Philharmonium
<g/>
,	,	kIx,	,
Leeds	Leeds	k1gInSc1	Leeds
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
-	-	kIx~	-
</s>
</p>
<p>
<s>
Šéfdirigent	šéfdirigent	k1gMnSc1	šéfdirigent
opery	opera	k1gFnSc2	opera
Slovenského	slovenský	k2eAgNnSc2d1	slovenské
národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
-	-	kIx~	-
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dirigent	dirigent	k1gMnSc1	dirigent
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
filharmonie	filharmonie	k1gFnSc2	filharmonie
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
-	-	kIx~	-
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dirigent	dirigent	k1gMnSc1	dirigent
Symfonického	symfonický	k2eAgInSc2d1	symfonický
orchestru	orchestr	k1gInSc2	orchestr
Slovenského	slovenský	k2eAgInSc2d1	slovenský
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
-	-	kIx~	-
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
http://www.snd.sk/?detail-2&	[url]	k?	http://www.snd.sk/?detail-2&
</s>
</p>
<p>
<s>
http://www.nd.cz/default.aspx?jz=cs&	[url]	k?	http://www.nd.cz/default.aspx?jz=cs&
</s>
</p>
<p>
<s>
https://web.archive.org/web/20070425173251/http://www.ndm.cz/clen_detail.php?clen=135&	[url]	k?	https://web.archive.org/web/20070425173251/http://www.ndm.cz/clen_detail.php?clen=135&
</s>
</p>
