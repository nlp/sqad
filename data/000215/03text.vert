<s>
Brno	Brno	k1gNnSc1	Brno
hlavní	hlavní	k2eAgNnSc1d1	hlavní
nádraží	nádraží	k1gNnSc1	nádraží
je	být	k5eAaImIp3nS	být
nejdůležitější	důležitý	k2eAgNnSc4d3	nejdůležitější
osobní	osobní	k2eAgNnSc4d1	osobní
nádraží	nádraží	k1gNnSc4	nádraží
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1839	[number]	k4	1839
a	a	k8xC	a
s	s	k7c7	s
nádražím	nádraží	k1gNnSc7	nádraží
v	v	k7c6	v
Břeclavi	Břeclav	k1gFnSc6	Břeclav
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc1d3	nejstarší
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
výhodné	výhodný	k2eAgFnSc6d1	výhodná
poloze	poloha	k1gFnSc6	poloha
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
okraji	okraj	k1gInSc6	okraj
historického	historický	k2eAgNnSc2d1	historické
centra	centrum	k1gNnSc2	centrum
<g/>
,	,	kIx,	,
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
Nádražní	nádražní	k2eAgFnSc2d1	nádražní
ulice	ulice	k1gFnSc2	ulice
<g/>
,	,	kIx,	,
při	při	k7c6	při
ústí	ústí	k1gNnSc6	ústí
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
ulice	ulice	k1gFnSc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
prochází	procházet	k5eAaImIp3nS	procházet
trasa	trasa	k1gFnSc1	trasa
prvního	první	k4xOgInSc2	první
železničního	železniční	k2eAgInSc2d1	železniční
koridoru	koridor	k1gInSc2	koridor
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zaústěny	zaústěn	k2eAgFnPc4d1	zaústěn
železniční	železniční	k2eAgFnPc4d1	železniční
tratě	trať	k1gFnPc4	trať
celkově	celkově	k6eAd1	celkově
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
směrů	směr	k1gInPc2	směr
<g/>
.	.	kIx.	.
</s>
<s>
Nádraží	nádraží	k1gNnSc1	nádraží
má	mít	k5eAaImIp3nS	mít
šest	šest	k4xCc4	šest
nástupišť	nástupiště	k1gNnPc2	nástupiště
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
dopravním	dopravní	k2eAgInSc7d1	dopravní
uzlem	uzel	k1gInSc7	uzel
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
před	před	k7c7	před
ním	on	k3xPp3gMnSc7	on
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
tramvajový	tramvajový	k2eAgInSc1d1	tramvajový
uzel	uzel	k1gInSc1	uzel
s	s	k7c7	s
nabídkou	nabídka	k1gFnSc7	nabídka
přímého	přímý	k2eAgNnSc2d1	přímé
a	a	k8xC	a
komfortního	komfortní	k2eAgNnSc2d1	komfortní
spojení	spojení	k1gNnSc2	spojení
do	do	k7c2	do
většiny	většina	k1gFnSc2	většina
částí	část	k1gFnPc2	část
města	město	k1gNnSc2	město
po	po	k7c6	po
sedmi	sedm	k4xCc6	sedm
tramvajových	tramvajový	k2eAgFnPc6d1	tramvajová
linkách	linka	k1gFnPc6	linka
(	(	kIx(	(
<g/>
14	[number]	k4	14
ramen	rameno	k1gNnPc2	rameno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
nádražní	nádražní	k2eAgFnSc1d1	nádražní
budova	budova	k1gFnSc1	budova
je	být	k5eAaImIp3nS	být
chráněna	chránit	k5eAaImNgFnS	chránit
jako	jako	k8xS	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
240	[number]	k4	240
<g/>
:	:	kIx,	:
Brno	Brno	k1gNnSc1	Brno
<g/>
-	-	kIx~	-
<g/>
Jihlava	Jihlava	k1gFnSc1	Jihlava
244	[number]	k4	244
<g/>
:	:	kIx,	:
Brno	Brno	k1gNnSc1	Brno
-	-	kIx~	-
Hrušovany	Hrušovany	k1gInPc1	Hrušovany
nad	nad	k7c7	nad
Jevišovkou	Jevišovka	k1gFnSc7	Jevišovka
250	[number]	k4	250
<g/>
:	:	kIx,	:
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
-	-	kIx~	-
Břeclav	Břeclav	k1gFnSc1	Břeclav
260	[number]	k4	260
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
Třebová	Třebová	k1gFnSc1	Třebová
-	-	kIx~	-
Brno	Brno	k1gNnSc1	Brno
300	[number]	k4	300
<g/>
:	:	kIx,	:
Brno	Brno	k1gNnSc1	Brno
<g/>
-	-	kIx~	-
<g/>
Přerov	Přerov	k1gInSc1	Přerov
340	[number]	k4	340
<g/>
:	:	kIx,	:
Brno	Brno	k1gNnSc1	Brno
-	-	kIx~	-
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
Původ	původ	k1gInSc1	původ
nádraží	nádraží	k1gNnSc2	nádraží
sahá	sahat	k5eAaImIp3nS	sahat
do	do	k7c2	do
konce	konec	k1gInSc2	konec
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
třetím	třetí	k4xOgMnSc6	třetí
nejstarším	starý	k2eAgNnSc7d3	nejstarší
větším	veliký	k2eAgNnSc7d2	veliký
nádražím	nádraží	k1gNnSc7	nádraží
v	v	k7c6	v
Rakousko-Uhersku	Rakousko-Uhersek	k1gInSc6	Rakousko-Uhersek
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
výpravní	výpravní	k2eAgFnSc1d1	výpravní
budova	budova	k1gFnSc1	budova
pro	pro	k7c4	pro
nádraží	nádraží	k1gNnSc4	nádraží
soukromé	soukromý	k2eAgFnSc2d1	soukromá
drážní	drážní	k2eAgFnSc2d1	drážní
společnosti	společnost	k1gFnSc2	společnost
Severní	severní	k2eAgFnSc2d1	severní
dráhy	dráha	k1gFnSc2	dráha
Ferdinandovy	Ferdinandův	k2eAgFnSc2d1	Ferdinandova
byla	být	k5eAaImAgNnP	být
stavěna	stavit	k5eAaImNgNnP	stavit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1838	[number]	k4	1838
a	a	k8xC	a
stála	stát	k5eAaImAgFnS	stát
kolmo	kolmo	k6eAd1	kolmo
k	k	k7c3	k
dnešnímu	dnešní	k2eAgNnSc3d1	dnešní
jihovýchodnímu	jihovýchodní	k2eAgNnSc3d1	jihovýchodní
křídlu	křídlo	k1gNnSc3	křídlo
zhruba	zhruba	k6eAd1	zhruba
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
výpravních	výpravní	k2eAgFnPc2d1	výpravní
budov	budova	k1gFnPc2	budova
a	a	k8xC	a
budovy	budova	k1gFnSc2	budova
pošty	pošta	k1gFnSc2	pošta
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
ukázkový	ukázkový	k2eAgInSc1d1	ukázkový
vlak	vlak	k1gInSc1	vlak
přijel	přijet	k5eAaPmAgInS	přijet
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
již	již	k6eAd1	již
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1838	[number]	k4	1838
z	z	k7c2	z
Rajhradu	Rajhrad	k1gInSc2	Rajhrad
<g/>
,	,	kIx,	,
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
provoz	provoz	k1gInSc1	provoz
mezi	mezi	k7c7	mezi
Vídní	Vídeň	k1gFnSc7	Vídeň
a	a	k8xC	a
Brnem	Brno	k1gNnSc7	Brno
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1839	[number]	k4	1839
<g/>
.	.	kIx.	.
</s>
<s>
Kolmo	kolmo	k6eAd1	kolmo
proto	proto	k8xC	proto
že	že	k8xS	že
původní	původní	k2eAgNnPc4d1	původní
nádraží	nádraží	k1gNnPc4	nádraží
bylo	být	k5eAaImAgNnS	být
hlavové	hlavový	k2eAgNnSc1d1	hlavové
(	(	kIx(	(
<g/>
koncové	koncový	k2eAgInPc1d1	koncový
<g/>
)	)	kIx)	)
a	a	k8xC	a
výpravní	výpravní	k2eAgFnSc1d1	výpravní
budova	budova	k1gFnSc1	budova
stála	stát	k5eAaImAgFnS	stát
na	na	k7c6	na
konci	konec	k1gInSc6	konec
kusých	kusý	k2eAgFnPc2d1	kusá
kolejí	kolej	k1gFnPc2	kolej
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
brzo	brzo	k6eAd1	brzo
změnila	změnit	k5eAaPmAgFnS	změnit
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1841	[number]	k4	1841
byla	být	k5eAaImAgFnS	být
přistavována	přistavován	k2eAgFnSc1d1	přistavována
nová	nový	k2eAgFnSc1d1	nová
budova	budova	k1gFnSc1	budova
již	již	k6eAd1	již
podél	podél	k7c2	podél
kolejí	kolej	k1gFnPc2	kolej
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
budova	budova	k1gFnSc1	budova
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
obsažena	obsáhnout	k5eAaPmNgFnS	obsáhnout
v	v	k7c6	v
tělese	těleso	k1gNnSc6	těleso
jihovýchodního	jihovýchodní	k2eAgNnSc2d1	jihovýchodní
křídla	křídlo	k1gNnSc2	křídlo
nádražních	nádražní	k2eAgFnPc2d1	nádražní
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Počínaje	počínaje	k7c7	počínaje
rokem	rok	k1gInSc7	rok
1843	[number]	k4	1843
byla	být	k5eAaImAgFnS	být
budována	budován	k2eAgFnSc1d1	budována
Severní	severní	k2eAgFnSc1d1	severní
státní	státní	k2eAgFnSc1d1	státní
dráha	dráha	k1gFnSc1	dráha
Brno-Česká	Brno-Český	k2eAgFnSc1d1	Brno-Česká
Třebová	Třebová	k1gFnSc1	Třebová
-	-	kIx~	-
její	její	k3xOp3gInSc4	její
úsek	úsek	k1gInSc4	úsek
Brno-Semín	Brno-Semín	k1gInSc1	Brno-Semín
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
trať	trať	k1gFnSc1	trať
č.	č.	k?	č.
260	[number]	k4	260
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
začínala	začínat	k5eAaImAgFnS	začínat
na	na	k7c6	na
severovýchodním	severovýchodní	k2eAgInSc6d1	severovýchodní
okraji	okraj	k1gInSc6	okraj
areálu	areál	k1gInSc6	areál
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nádraží	nádraží	k1gNnSc2	nádraží
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
dvojnádraží	dvojnádraží	k1gNnSc1	dvojnádraží
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
hlavovými	hlavový	k2eAgFnPc7d1	hlavová
konci	konec	k1gInSc3	konec
kolejišť	kolejiště	k1gNnPc2	kolejiště
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
dvěma	dva	k4xCgFnPc7	dva
výpravními	výpravní	k2eAgFnPc7d1	výpravní
budovami	budova	k1gFnPc7	budova
se	s	k7c7	s
společným	společný	k2eAgInSc7d1	společný
středním	střední	k2eAgInSc7d1	střední
vestibulem	vestibul	k1gInSc7	vestibul
<g/>
.	.	kIx.	.
</s>
<s>
Přestavbu	přestavba	k1gFnSc4	přestavba
nádraží	nádraží	k1gNnSc2	nádraží
řídil	řídit	k5eAaImAgMnS	řídit
železniční	železniční	k2eAgMnSc1d1	železniční
inženýr	inženýr	k1gMnSc1	inženýr
Anton	Anton	k1gMnSc1	Anton
Jüngling	Jüngling	k1gInSc4	Jüngling
<g/>
.	.	kIx.	.
</s>
<s>
Provoz	provoz	k1gInSc1	provoz
státní	státní	k2eAgFnSc2d1	státní
dráhy	dráha	k1gFnSc2	dráha
do	do	k7c2	do
České	český	k2eAgFnSc2d1	Česká
Třebové	Třebová	k1gFnSc2	Třebová
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1849	[number]	k4	1849
<g/>
.	.	kIx.	.
</s>
<s>
Nádraží	nádraží	k1gNnSc1	nádraží
bylo	být	k5eAaImAgNnS	být
propojeno	propojit	k5eAaPmNgNnS	propojit
a	a	k8xC	a
proměněno	proměnit	k5eAaPmNgNnS	proměnit
na	na	k7c4	na
průběžné	průběžný	k2eAgNnSc4d1	průběžné
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
byl	být	k5eAaImAgInS	být
dosavadní	dosavadní	k2eAgInSc1d1	dosavadní
vestibul	vestibul	k1gInSc1	vestibul
přebudován	přebudován	k2eAgInSc1d1	přebudován
na	na	k7c4	na
velkou	velký	k2eAgFnSc4d1	velká
střední	střední	k2eAgFnSc4d1	střední
halu	hala	k1gFnSc4	hala
s	s	k7c7	s
věžemi	věž	k1gFnPc7	věž
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
projektu	projekt	k1gInSc2	projekt
architekta	architekt	k1gMnSc2	architekt
Josefa	Josef	k1gMnSc2	Josef
Oehma	Oehm	k1gMnSc2	Oehm
a	a	k8xC	a
konstruktéra	konstruktér	k1gMnSc2	konstruktér
Franze	Franze	k1gFnSc2	Franze
Uhla	Uhl	k1gInSc2	Uhl
<g/>
,	,	kIx,	,
hlavním	hlavní	k2eAgMnSc7d1	hlavní
stavitelem	stavitel	k1gMnSc7	stavitel
byl	být	k5eAaImAgMnS	být
Josef	Josef	k1gMnSc1	Josef
Nebehosteny	Nebehosten	k2eAgMnPc4d1	Nebehosten
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
vybudována	vybudován	k2eAgFnSc1d1	vybudována
koněspřežná	koněspřežný	k2eAgFnSc1d1	koněspřežná
tramvaj	tramvaj	k1gFnSc1	tramvaj
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
nádraží	nádraží	k1gNnSc1	nádraží
přímo	přímo	k6eAd1	přímo
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
částmi	část	k1gFnPc7	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
dále	daleko	k6eAd2	daleko
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Okolní	okolní	k2eAgInPc1d1	okolní
veřejné	veřejný	k2eAgInPc1d1	veřejný
prostory	prostor	k1gInPc1	prostor
před	před	k7c7	před
nádražím	nádraží	k1gNnSc7	nádraží
byly	být	k5eAaImAgFnP	být
od	od	k7c2	od
konce	konec	k1gInSc2	konec
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
urbanisticky	urbanisticky	k6eAd1	urbanisticky
přetvářeny	přetvářet	k5eAaImNgFnP	přetvářet
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
okružní	okružní	k2eAgFnSc2d1	okružní
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgFnPc1d1	zdejší
budovy	budova	k1gFnPc1	budova
byly	být	k5eAaImAgFnP	být
stavěny	stavit	k5eAaImNgFnP	stavit
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
stylu	styl	k1gInSc6	styl
jako	jako	k8xS	jako
na	na	k7c6	na
Ringstraße	Ringstraße	k1gFnSc6	Ringstraße
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
hlavní	hlavní	k2eAgNnSc1d1	hlavní
nádraží	nádraží	k1gNnSc1	nádraží
je	být	k5eAaImIp3nS	být
průjezdnou	průjezdný	k2eAgFnSc4d1	průjezdná
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
smíšenou	smíšený	k2eAgFnSc7d1	smíšená
stanicí	stanice	k1gFnSc7	stanice
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
jsou	být	k5eAaImIp3nP	být
čtyři	čtyři	k4xCgNnPc1	čtyři
nástupiště	nástupiště	k1gNnPc1	nástupiště
se	s	k7c7	s
šesti	šest	k4xCc2	šest
nástupními	nástupní	k2eAgFnPc7d1	nástupní
hranami	hrana	k1gFnPc7	hrana
průjezdná	průjezdný	k2eAgNnPc4d1	průjezdné
(	(	kIx(	(
<g/>
traťová	traťový	k2eAgNnPc4d1	traťové
<g/>
)	)	kIx)	)
a	a	k8xC	a
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
straně	strana	k1gFnSc6	strana
přidruženy	přidružit	k5eAaPmNgFnP	přidružit
tři	tři	k4xCgFnPc1	tři
kusé	kusý	k2eAgFnPc1d1	kusá
koleje	kolej	k1gFnPc1	kolej
na	na	k7c6	na
dvou	dva	k4xCgNnPc6	dva
nástupištích	nástupiště	k1gNnPc6	nástupiště
5	[number]	k4	5
<g/>
.	.	kIx.	.
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
jsou	být	k5eAaImIp3nP	být
vypravovány	vypravován	k2eAgInPc1d1	vypravován
regionální	regionální	k2eAgInPc1d1	regionální
vlaky	vlak	k1gInPc1	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Průjezdná	průjezdný	k2eAgFnSc1d1	průjezdná
část	část	k1gFnSc1	část
nádraží	nádraží	k1gNnSc2	nádraží
dominuje	dominovat	k5eAaImIp3nS	dominovat
a	a	k8xC	a
dává	dávat	k5eAaImIp3nS	dávat
mu	on	k3xPp3gMnSc3	on
charakter	charakter	k1gInSc4	charakter
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
je	být	k5eAaImIp3nS	být
nápadně	nápadně	k6eAd1	nápadně
archaická	archaický	k2eAgFnSc1d1	archaická
a	a	k8xC	a
zanedbaná	zanedbaný	k2eAgFnSc1d1	zanedbaná
<g/>
.	.	kIx.	.
</s>
<s>
Těleso	těleso	k1gNnSc1	těleso
průjezdné	průjezdný	k2eAgFnSc2d1	průjezdná
části	část	k1gFnSc2	část
je	být	k5eAaImIp3nS	být
zakřiveno	zakřiven	k2eAgNnSc1d1	zakřiveno
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
mírně	mírně	k6eAd1	mírně
prohnutého	prohnutý	k2eAgNnSc2d1	prohnuté
obráceného	obrácený	k2eAgNnSc2d1	obrácené
S	s	k7c7	s
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
přímé	přímý	k2eAgNnSc1d1	přímé
(	(	kIx(	(
<g/>
rovné	rovný	k2eAgInPc1d1	rovný
<g/>
)	)	kIx)	)
úseky	úsek	k1gInPc1	úsek
nejsou	být	k5eNaImIp3nP	být
o	o	k7c4	o
mnoho	mnoho	k6eAd1	mnoho
delší	dlouhý	k2eAgFnSc4d2	delší
než	než	k8xS	než
110	[number]	k4	110
m	m	kA	m
(	(	kIx(	(
<g/>
u	u	k7c2	u
každého	každý	k3xTgInSc2	každý
z	z	k7c2	z
nástupišť	nástupiště	k1gNnPc2	nástupiště
se	se	k3xPyFc4	se
délka	délka	k1gFnSc1	délka
mírně	mírně	k6eAd1	mírně
liší	lišit	k5eAaImIp3nS	lišit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nástupiště	nástupiště	k1gNnPc1	nástupiště
sledují	sledovat	k5eAaImIp3nP	sledovat
přibližně	přibližně	k6eAd1	přibližně
tvar	tvar	k1gInSc4	tvar
odbavovacích	odbavovací	k2eAgFnPc2d1	odbavovací
a	a	k8xC	a
provozních	provozní	k2eAgFnPc2d1	provozní
budov	budova	k1gFnPc2	budova
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
dvou	dva	k4xCgFnPc2	dva
různých	různý	k2eAgFnPc2d1	různá
a	a	k8xC	a
jen	jen	k9	jen
obtížně	obtížně	k6eAd1	obtížně
spolupracujících	spolupracující	k2eAgFnPc2d1	spolupracující
drážních	drážní	k2eAgFnPc2d1	drážní
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
průjezdné	průjezdný	k2eAgFnSc2d1	průjezdná
části	část	k1gFnSc2	část
stanice	stanice	k1gFnSc2	stanice
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
zhruba	zhruba	k6eAd1	zhruba
300	[number]	k4	300
m	m	kA	m
<g/>
,	,	kIx,	,
určit	určit	k5eAaPmF	určit
jednoznačně	jednoznačně	k6eAd1	jednoznačně
konec	konec	k1gInSc1	konec
nástupišť	nástupiště	k1gNnPc2	nástupiště
je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
některá	některý	k3yIgFnSc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
na	na	k7c6	na
jihovýchodním	jihovýchodní	k2eAgInSc6d1	jihovýchodní
konci	konec	k1gInSc6	konec
příliš	příliš	k6eAd1	příliš
zužují	zužovat	k5eAaImIp3nP	zužovat
<g/>
.	.	kIx.	.
</s>
<s>
Šířka	šířka	k1gFnSc1	šířka
drážního	drážní	k2eAgNnSc2d1	drážní
tělesa	těleso	k1gNnSc2	těleso
od	od	k7c2	od
jižního	jižní	k2eAgInSc2d1	jižní
pláště	plášť	k1gInSc2	plášť
výpravních	výpravní	k2eAgFnPc2d1	výpravní
budov	budova	k1gFnPc2	budova
po	po	k7c6	po
hranu	hran	k1gInSc6	hran
terasy	terasa	k1gFnSc2	terasa
je	být	k5eAaImIp3nS	být
proměnná	proměnná	k1gFnSc1	proměnná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zhruba	zhruba	k6eAd1	zhruba
60	[number]	k4	60
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Průjezdovou	průjezdový	k2eAgFnSc7d1	průjezdová
kolejí	kolej	k1gFnSc7	kolej
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
na	na	k7c4	na
Českou	český	k2eAgFnSc4d1	Česká
Třebovou	Třebová	k1gFnSc4	Třebová
je	být	k5eAaImIp3nS	být
kolej	kolej	k1gFnSc1	kolej
6	[number]	k4	6
a	a	k8xC	a
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
na	na	k7c4	na
Břeclav	Břeclav	k1gFnSc4	Břeclav
kolej	kolej	k1gFnSc1	kolej
3	[number]	k4	3
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
ostrovním	ostrovní	k2eAgNnSc6d1	ostrovní
nástupišti	nástupiště	k1gNnSc6	nástupiště
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgNnPc4	dva
ostrovní	ostrovní	k2eAgNnPc4d1	ostrovní
nástupiště	nástupiště	k1gNnPc4	nástupiště
široká	široký	k2eAgNnPc4d1	široké
přibližně	přibližně	k6eAd1	přibližně
9	[number]	k4	9
m.	m.	k?	m.
Nástupní	nástupní	k2eAgFnSc1d1	nástupní
hrana	hrana	k1gFnSc1	hrana
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
350	[number]	k4	350
mm	mm	kA	mm
nad	nad	k7c7	nad
hlavou	hlava	k1gFnSc7	hlava
kolejnice	kolejnice	k1gFnSc2	kolejnice
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc4	který
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
normě	norma	k1gFnSc3	norma
ale	ale	k8xC	ale
pro	pro	k7c4	pro
budoucnost	budoucnost	k1gFnSc4	budoucnost
jsou	být	k5eAaImIp3nP	být
považována	považován	k2eAgNnPc1d1	považováno
jako	jako	k8xS	jako
méně	málo	k6eAd2	málo
vhodná	vhodný	k2eAgFnSc1d1	vhodná
<g/>
.	.	kIx.	.
</s>
<s>
Nástupiště	nástupiště	k1gNnPc1	nástupiště
jsou	být	k5eAaImIp3nP	být
kryta	krýt	k5eAaImNgFnS	krýt
zastaralými	zastaralý	k2eAgInPc7d1	zastaralý
přístřešky	přístřešek	k1gInPc7	přístřešek
dvojitého	dvojitý	k2eAgNnSc2d1	dvojité
zalomení	zalomení	k1gNnSc2	zalomení
s	s	k7c7	s
litinovou	litinový	k2eAgFnSc7d1	litinová
nosnou	nosný	k2eAgFnSc7d1	nosná
konstrukcí	konstrukce	k1gFnSc7	konstrukce
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nástupiště	nástupiště	k1gNnSc1	nástupiště
u	u	k7c2	u
kusých	kusý	k2eAgFnPc2d1	kusá
kolejí	kolej	k1gFnPc2	kolej
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jsou	být	k5eAaImIp3nP	být
náznakem	náznak	k1gInSc7	náznak
hlavového	hlavový	k2eAgNnSc2d1	hlavové
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
kryta	kryt	k2eAgNnPc1d1	kryto
přístřešky	přístřešek	k1gInPc4	přístřešek
novější	nový	k2eAgFnSc2d2	novější
konstrukce	konstrukce	k1gFnSc2	konstrukce
(	(	kIx(	(
<g/>
tzv	tzv	kA	tzv
"	"	kIx"	"
<g/>
vlaštovkami	vlaštovka	k1gFnPc7	vlaštovka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
délky	délka	k1gFnSc2	délka
nástupiště	nástupiště	k1gNnSc2	nástupiště
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
nástupiště	nástupiště	k1gNnPc1	nástupiště
jsou	být	k5eAaImIp3nP	být
novější	nový	k2eAgNnPc1d2	novější
<g/>
,	,	kIx,	,
postavená	postavený	k2eAgNnPc1d1	postavené
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
v	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
úseku	úsek	k1gInSc6	úsek
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
300	[number]	k4	300
m.	m.	k?	m.
Pod	pod	k7c7	pod
nástupišti	nástupiště	k1gNnSc6	nástupiště
průjezdné	průjezdný	k2eAgFnSc2d1	průjezdná
části	část	k1gFnSc2	část
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc1	dva
tunely	tunel	k1gInPc1	tunel
(	(	kIx(	(
<g/>
podchody	podchod	k1gInPc1	podchod
<g/>
)	)	kIx)	)
-	-	kIx~	-
jeden	jeden	k4xCgMnSc1	jeden
původně	původně	k6eAd1	původně
jen	jen	k6eAd1	jen
nástupní	nástupní	k2eAgInSc4d1	nástupní
široký	široký	k2eAgInSc4d1	široký
4,8	[number]	k4	4,8
m	m	kA	m
vede	vést	k5eAaImIp3nS	vést
v	v	k7c6	v
ose	osa	k1gFnSc6	osa
hlavní	hlavní	k2eAgFnSc2d1	hlavní
dvorany	dvorana	k1gFnSc2	dvorana
napříč	napříč	k6eAd1	napříč
přes	přes	k7c4	přes
všechna	všechen	k3xTgNnPc4	všechen
nástupiště	nástupiště	k1gNnPc4	nástupiště
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
modernizován	modernizovat	k5eAaBmNgInS	modernizovat
také	také	k9	také
těsně	těsně	k6eAd1	těsně
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
obložen	obložen	k2eAgInSc4d1	obložen
travertinem	travertin	k1gInSc7	travertin
a	a	k8xC	a
na	na	k7c6	na
podlaze	podlaha	k1gFnSc6	podlaha
libereckou	liberecký	k2eAgFnSc7d1	liberecká
žulou	žula	k1gFnSc7	žula
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přelomu	přelom	k1gInSc6	přelom
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
bylo	být	k5eAaImAgNnS	být
každé	každý	k3xTgNnSc4	každý
z	z	k7c2	z
nástupišť	nástupiště	k1gNnPc2	nástupiště
vybaveno	vybavit	k5eAaPmNgNnS	vybavit
jedním	jeden	k4xCgInSc7	jeden
výtahem	výtah	k1gInSc7	výtah
pro	pro	k7c4	pro
přístup	přístup	k1gInSc4	přístup
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
pohybovým	pohybový	k2eAgNnSc7d1	pohybové
omezením	omezení	k1gNnSc7	omezení
<g/>
,	,	kIx,	,
ústících	ústící	k2eAgFnPc2d1	ústící
až	až	k9	až
v	v	k7c6	v
koncové	koncový	k2eAgFnSc6d1	koncová
části	část	k1gFnSc6	část
nástupišť	nástupiště	k1gNnPc2	nástupiště
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
tunel	tunel	k1gInSc1	tunel
-	-	kIx~	-
původně	původně	k6eAd1	původně
jen	jen	k9	jen
výstupní	výstupní	k2eAgMnSc1d1	výstupní
o	o	k7c6	o
šířce	šířka	k1gFnSc6	šířka
5,2	[number]	k4	5,2
m	m	kA	m
vede	vést	k5eAaImIp3nS	vést
o	o	k7c4	o
59	[number]	k4	59
m	m	kA	m
východněji	východně	k6eAd2	východně
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
poválečná	poválečný	k2eAgFnSc1d1	poválečná
úprava	úprava	k1gFnSc1	úprava
byla	být	k5eAaImAgFnS	být
skromnější	skromný	k2eAgFnSc1d2	skromnější
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
tunely	tunel	k1gInPc7	tunel
vede	vést	k5eAaImIp3nS	vést
ještě	ještě	k9	ještě
další	další	k2eAgInSc4d1	další
širší	široký	k2eAgInSc4d2	širší
tunel	tunel	k1gInSc4	tunel
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ale	ale	k9	ale
není	být	k5eNaImIp3nS	být
součástí	součást	k1gFnSc7	součást
stanice	stanice	k1gFnSc2	stanice
a	a	k8xC	a
spojuje	spojovat	k5eAaImIp3nS	spojovat
přednádraží	přednádraží	k1gNnSc4	přednádraží
a	a	k8xC	a
zanádraží	zanádraží	k1gNnSc4	zanádraží
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jako	jako	k8xC	jako
součást	součást	k1gFnSc4	součást
veřejného	veřejný	k2eAgInSc2d1	veřejný
podchodu	podchod	k1gInSc2	podchod
před	před	k7c7	před
nádražím	nádraží	k1gNnSc7	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
Niveleta	niveleta	k1gFnSc1	niveleta
jeho	jeho	k3xOp3gFnSc2	jeho
podlahy	podlaha	k1gFnSc2	podlaha
leží	ležet	k5eAaImIp3nS	ležet
níže	nízce	k6eAd2	nízce
než	než	k8xS	než
podlahy	podlaha	k1gFnSc2	podlaha
obou	dva	k4xCgInPc2	dva
staničních	staniční	k2eAgInPc2d1	staniční
tunelů	tunel	k1gInPc2	tunel
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
úplnost	úplnost	k1gFnSc4	úplnost
-	-	kIx~	-
pod	pod	k7c7	pod
stanicí	stanice	k1gFnSc7	stanice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ještě	ještě	k9	ještě
čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
zanedbaný	zanedbaný	k2eAgInSc1d1	zanedbaný
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
nepoužívaný	používaný	k2eNgInSc1d1	nepoužívaný
<g/>
)	)	kIx)	)
tunel	tunel	k1gInSc1	tunel
<g/>
,	,	kIx,	,
lidově	lidově	k6eAd1	lidově
zvaný	zvaný	k2eAgInSc1d1	zvaný
"	"	kIx"	"
<g/>
Myší	myší	k2eAgFnSc1d1	myší
díra	díra	k1gFnSc1	díra
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
souboru	soubor	k1gInSc2	soubor
nádraží	nádraží	k1gNnSc2	nádraží
je	být	k5eAaImIp3nS	být
nádražní	nádražní	k2eAgFnSc1d1	nádražní
pošta	pošta	k1gFnSc1	pošta
-	-	kIx~	-
dodnes	dodnes	k6eAd1	dodnes
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
modernistické	modernistický	k2eAgFnSc2d1	modernistická
stavby	stavba	k1gFnSc2	stavba
od	od	k7c2	od
architekta	architekt	k1gMnSc2	architekt
Bohuslava	Bohuslav	k1gMnSc2	Bohuslav
Fuchse	Fuchs	k1gMnSc2	Fuchs
-	-	kIx~	-
která	který	k3yRgFnSc1	který
předstupuje	předstupovat	k5eAaImIp3nS	předstupovat
před	před	k7c4	před
křídla	křídlo	k1gNnPc4	křídlo
starých	starý	k2eAgFnPc2d1	stará
provozních	provozní	k2eAgFnPc2d1	provozní
budov	budova	k1gFnPc2	budova
na	na	k7c4	na
sever	sever	k1gInSc4	sever
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
uličního	uliční	k2eAgInSc2d1	uliční
prostoru	prostor	k1gInSc2	prostor
a	a	k8xC	a
v	v	k7c6	v
drážní	drážní	k2eAgFnSc6d1	drážní
části	část	k1gFnSc6	část
je	být	k5eAaImIp3nS	být
obsloužena	obsloužen	k2eAgFnSc1d1	obsloužena
dvěma	dva	k4xCgFnPc7	dva
kusými	kusý	k2eAgFnPc7d1	kusá
kolejemi	kolej	k1gFnPc7	kolej
<g/>
,	,	kIx,	,
ležícími	ležící	k2eAgMnPc7d1	ležící
mezi	mezi	k7c7	mezi
průjezdnou	průjezdný	k2eAgFnSc7d1	průjezdná
a	a	k8xC	a
hlavovou	hlavový	k2eAgFnSc7d1	hlavová
částí	část	k1gFnSc7	část
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
úpravě	úprava	k1gFnSc3	úprava
nepříliš	příliš	k6eNd1	příliš
vhodného	vhodný	k2eAgNnSc2d1	vhodné
schématu	schéma	k1gNnSc2	schéma
kolejišť	kolejiště	k1gNnPc2	kolejiště
dosud	dosud	k6eAd1	dosud
nikdy	nikdy	k6eAd1	nikdy
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
již	již	k6eAd1	již
od	od	k7c2	od
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
vedou	vést	k5eAaImIp3nP	vést
odborné	odborný	k2eAgFnPc4d1	odborná
diskuze	diskuze	k1gFnPc4	diskuze
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
modernizovat	modernizovat	k5eAaBmF	modernizovat
a	a	k8xC	a
přestavět	přestavět	k5eAaPmF	přestavět
nádraží	nádraží	k1gNnSc4	nádraží
v	v	k7c6	v
dosavadní	dosavadní	k2eAgFnSc6d1	dosavadní
poloze	poloha	k1gFnSc6	poloha
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
mírně	mírně	k6eAd1	mírně
posunout	posunout	k5eAaPmF	posunout
jeho	jeho	k3xOp3gNnSc4	jeho
těžiště	těžiště	k1gNnSc4	těžiště
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zda	zda	k8xS	zda
vybudovat	vybudovat	k5eAaPmF	vybudovat
nádraží	nádraží	k1gNnSc4	nádraží
jinde	jinde	k6eAd1	jinde
<g/>
,	,	kIx,	,
v	v	k7c6	v
některé	některý	k3yIgFnSc6	některý
z	z	k7c2	z
poloh	poloha	k1gFnPc2	poloha
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
současného	současný	k2eAgNnSc2d1	současné
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
ofenzivě	ofenziva	k1gFnSc6	ofenziva
tendence	tendence	k1gFnSc2	tendence
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
odsunu	odsunout	k5eAaPmIp1nS	odsunout
nádraží	nádraží	k1gNnSc1	nádraží
<g/>
,	,	kIx,	,
a	a	k8xC	a
do	do	k7c2	do
aktuálním	aktuální	k2eAgInSc6d1	aktuální
územního	územní	k2eAgInSc2d1	územní
plánu	plán	k1gInSc2	plán
je	být	k5eAaImIp3nS	být
zanesena	zanesen	k2eAgFnSc1d1	zanesena
varianta	varianta	k1gFnSc1	varianta
v	v	k7c6	v
poloze	poloha	k1gFnSc6	poloha
přibližně	přibližně	k6eAd1	přibližně
1100	[number]	k4	1100
metrů	metr	k1gInPc2	metr
jihozápadním	jihozápadní	k2eAgInSc7d1	jihozápadní
směrem	směr	k1gInSc7	směr
(	(	kIx(	(
<g/>
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
těžištěm	těžiště	k1gNnSc7	těžiště
starého	starý	k2eAgMnSc2d1	starý
a	a	k8xC	a
nového	nový	k2eAgNnSc2d1	nové
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ale	ale	k8xC	ale
tato	tento	k3xDgFnSc1	tento
varianta	varianta	k1gFnSc1	varianta
dosud	dosud	k6eAd1	dosud
nezískala	získat	k5eNaPmAgFnS	získat
platné	platný	k2eAgNnSc4d1	platné
územní	územní	k2eAgNnSc4d1	územní
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
<g/>
.	.	kIx.	.
</s>
<s>
Prostorová	prostorový	k2eAgFnSc1d1	prostorová
situace	situace	k1gFnSc1	situace
a	a	k8xC	a
struktura	struktura	k1gFnSc1	struktura
města	město	k1gNnSc2	město
vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
případnou	případný	k2eAgFnSc4d1	případná
geometrickou	geometrický	k2eAgFnSc4d1	geometrická
úpravu	úprava	k1gFnSc4	úprava
kolejišť	kolejiště	k1gNnPc2	kolejiště
a	a	k8xC	a
nástupišť	nástupiště	k1gNnPc2	nástupiště
dosavadní	dosavadní	k2eAgFnSc2d1	dosavadní
stanice	stanice	k1gFnSc2	stanice
s	s	k7c7	s
výrazným	výrazný	k2eAgNnSc7d1	výrazné
prodloužením	prodloužení	k1gNnSc7	prodloužení
přímých	přímý	k2eAgMnPc2d1	přímý
úseků	úsek	k1gInPc2	úsek
<g/>
,	,	kIx,	,
eventuálně	eventuálně	k6eAd1	eventuálně
s	s	k7c7	s
přidáním	přidání	k1gNnSc7	přidání
jednoho	jeden	k4xCgNnSc2	jeden
dalšího	další	k2eAgNnSc2d1	další
nástupiště	nástupiště	k1gNnSc2	nástupiště
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc1	takový
případné	případný	k2eAgFnPc1d1	případná
změny	změna	k1gFnPc1	změna
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
promítly	promítnout	k5eAaPmAgInP	promítnout
do	do	k7c2	do
menší	malý	k2eAgFnSc2d2	menší
změny	změna	k1gFnSc2	změna
urbanistické	urbanistický	k2eAgFnSc2d1	urbanistická
struktury	struktura	k1gFnSc2	struktura
města	město	k1gNnSc2	město
bezprostředně	bezprostředně	k6eAd1	bezprostředně
přilehlého	přilehlý	k2eAgInSc2d1	přilehlý
k	k	k7c3	k
jižní	jižní	k2eAgFnSc3d1	jižní
hraně	hrana	k1gFnSc3	hrana
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
Nádraží	nádraží	k1gNnSc1	nádraží
bylo	být	k5eAaImAgNnS	být
opravováno	opravovat	k5eAaImNgNnS	opravovat
od	od	k7c2	od
konce	konec	k1gInSc2	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
jen	jen	k9	jen
jedenkrát	jedenkrát	k6eAd1	jedenkrát
-	-	kIx~	-
na	na	k7c6	na
konci	konec	k1gInSc6	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Oprava	oprava	k1gFnSc1	oprava
a	a	k8xC	a
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
se	se	k3xPyFc4	se
týkala	týkat	k5eAaImAgFnS	týkat
jen	jen	k6eAd1	jen
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
výpravních	výpravní	k2eAgFnPc2d1	výpravní
a	a	k8xC	a
správních	správní	k2eAgFnPc2d1	správní
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Nikoliv	nikoliv	k9	nikoliv
kolejišť	kolejiště	k1gNnPc2	kolejiště
nástupišť	nástupiště	k1gNnPc2	nástupiště
a	a	k8xC	a
kolejových	kolejový	k2eAgNnPc2d1	kolejové
zhlaví	zhlaví	k1gNnPc2	zhlaví
<g/>
.	.	kIx.	.
</s>
<s>
Zastánci	zastánce	k1gMnPc1	zastánce
zachování	zachování	k1gNnSc2	zachování
polohy	poloha	k1gFnSc2	poloha
nádraží	nádraží	k1gNnSc2	nádraží
v	v	k7c6	v
dosavadní	dosavadní	k2eAgFnSc6d1	dosavadní
poloze	poloha	k1gFnSc6	poloha
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yIgFnPc4	který
patří	patřit	k5eAaImIp3nS	patřit
řada	řada	k1gFnSc1	řada
respektovaných	respektovaný	k2eAgMnPc2d1	respektovaný
odborníků	odborník	k1gMnPc2	odborník
<g/>
,	,	kIx,	,
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,,	,,	k?	,,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
je	on	k3xPp3gInPc4	on
upravit	upravit	k5eAaPmF	upravit
do	do	k7c2	do
přijatelné	přijatelný	k2eAgFnSc2d1	přijatelná
zcela	zcela	k6eAd1	zcela
moderní	moderní	k2eAgFnSc2d1	moderní
podoby	podoba	k1gFnSc2	podoba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
zachovány	zachován	k2eAgFnPc4d1	zachována
přednosti	přednost	k1gFnPc4	přednost
jeho	jeho	k3xOp3gFnSc2	jeho
polohy	poloha	k1gFnSc2	poloha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
aby	aby	kYmCp3nS	aby
stanice	stanice	k1gFnSc1	stanice
zároveň	zároveň	k6eAd1	zároveň
získala	získat	k5eAaPmAgFnS	získat
soudobý	soudobý	k2eAgInSc4d1	soudobý
<g/>
,	,	kIx,	,
komfortní	komfortní	k2eAgInSc4d1	komfortní
a	a	k8xC	a
plynulý	plynulý	k2eAgInSc4d1	plynulý
provoz	provoz	k1gInSc4	provoz
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
desetiletí	desetiletí	k1gNnSc6	desetiletí
podařilo	podařit	k5eAaPmAgNnS	podařit
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
nádražích	nádraží	k1gNnPc6	nádraží
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
např	např	kA	např
na	na	k7c6	na
hlavním	hlavní	k2eAgNnSc6d1	hlavní
nádraží	nádraží	k1gNnSc6	nádraží
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
nebo	nebo	k8xC	nebo
na	na	k7c6	na
nádraží	nádraží	k1gNnSc6	nádraží
Wien	Wien	k1gMnSc1	Wien
Praterstern	Praterstern	k1gMnSc1	Praterstern
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Nádraží	nádraží	k1gNnSc2	nádraží
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
a	a	k8xC	a
Referendum	referendum	k1gNnSc4	referendum
o	o	k7c6	o
poloze	poloha	k1gFnSc6	poloha
hlavního	hlavní	k2eAgNnSc2d1	hlavní
nádraží	nádraží	k1gNnSc2	nádraží
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
zvažována	zvažován	k2eAgFnSc1d1	zvažována
idea	idea	k1gFnSc1	idea
změny	změna	k1gFnSc2	změna
polohy	poloha	k1gFnSc2	poloha
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
Výhodná	výhodný	k2eAgFnSc1d1	výhodná
poloha	poloha	k1gFnSc1	poloha
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
nejživějšího	živý	k2eAgInSc2d3	nejživější
pohybu	pohyb	k1gInSc2	pohyb
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
má	mít	k5eAaImIp3nS	mít
odvrácenou	odvrácený	k2eAgFnSc4d1	odvrácená
stránku	stránka	k1gFnSc4	stránka
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
se	s	k7c7	s
ztíženou	ztížený	k2eAgFnSc7d1	ztížená
možností	možnost	k1gFnSc7	možnost
drážního	drážní	k2eAgInSc2d1	drážní
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
parního	parní	k2eAgInSc2d1	parní
provozu	provoz	k1gInSc2	provoz
se	se	k3xPyFc4	se
negativně	negativně	k6eAd1	negativně
projevovalo	projevovat	k5eAaImAgNnS	projevovat
znečistění	znečistěný	k2eAgMnPc1d1	znečistěný
kouřem	kouř	k1gInSc7	kouř
a	a	k8xC	a
hlukem	hluk	k1gInSc7	hluk
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
zdlouhavost	zdlouhavost	k1gFnSc1	zdlouhavost
cesty	cesta	k1gFnSc2	cesta
lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
do	do	k7c2	do
výtopen	výtopna	k1gFnPc2	výtopna
i	i	k8xC	i
nedostatečnost	nedostatečnost	k1gFnSc4	nedostatečnost
seřaďovacího	seřaďovací	k2eAgNnSc2d1	seřaďovací
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
komplikovala	komplikovat	k5eAaBmAgFnS	komplikovat
provoz	provoz	k1gInSc4	provoz
v	v	k7c6	v
uzlu	uzel	k1gInSc6	uzel
i	i	k9	i
spojka	spojka	k1gFnSc1	spojka
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
rosickým	rosický	k2eAgNnSc7d1	rosické
a	a	k8xC	a
hlavním	hlavní	k2eAgNnSc7d1	hlavní
nádražím	nádraží	k1gNnSc7	nádraží
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
již	již	k6eAd1	již
zrušená	zrušený	k2eAgFnSc1d1	zrušená
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
soutěž	soutěž	k1gFnSc1	soutěž
na	na	k7c4	na
řešení	řešení	k1gNnSc4	řešení
problému	problém	k1gInSc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
cena	cena	k1gFnSc1	cena
nebyla	být	k5eNaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
cena	cena	k1gFnSc1	cena
byla	být	k5eAaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
autorům	autor	k1gMnPc3	autor
architektu	architekt	k1gMnSc3	architekt
Bohuslavu	Bohuslav	k1gMnSc3	Bohuslav
Fuchsovi	Fuchs	k1gMnSc3	Fuchs
a	a	k8xC	a
železničnímu	železniční	k2eAgMnSc3d1	železniční
expertu	expert	k1gMnSc3	expert
ing.	ing.	kA	ing.
Františku	František	k1gMnSc3	František
Sklenářovi	Sklenář	k1gMnSc3	Sklenář
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
vešla	vejít	k5eAaPmAgFnS	vejít
ve	v	k7c4	v
všeobecnou	všeobecný	k2eAgFnSc4d1	všeobecná
známost	známost	k1gFnSc4	známost
(	(	kIx(	(
<g/>
Tangenta	tangenta	k1gFnSc1	tangenta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Soutěže	soutěž	k1gFnPc1	soutěž
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
ještě	ještě	k6eAd1	ještě
opakovány	opakovat	k5eAaImNgFnP	opakovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
postoje	postoj	k1gInSc2	postoj
veřejné	veřejný	k2eAgFnSc2d1	veřejná
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
desetiletí	desetiletí	k1gNnSc2	desetiletí
padlo	padnout	k5eAaImAgNnS	padnout
několik	několik	k4yIc1	několik
rozhodnutí	rozhodnutí	k1gNnPc2	rozhodnutí
o	o	k7c6	o
poloze	poloha	k1gFnSc6	poloha
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1927	[number]	k4	1927
<g/>
-	-	kIx~	-
<g/>
1969	[number]	k4	1969
byly	být	k5eAaImAgFnP	být
zvažovány	zvažovat	k5eAaImNgInP	zvažovat
polohy	poloh	k1gInPc1	poloh
jak	jak	k8xS	jak
na	na	k7c6	na
dosavadním	dosavadní	k2eAgNnSc6d1	dosavadní
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
poloze	poloha	k1gFnSc6	poloha
odsunuté	odsunutý	k2eAgFnSc6d1	odsunutá
o	o	k7c4	o
600	[number]	k4	600
metrů	metr	k1gInPc2	metr
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
<s>
Tendence	tendence	k1gFnSc1	tendence
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
polohy	poloha	k1gFnSc2	poloha
narážely	narážet	k5eAaPmAgFnP	narážet
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
obdobích	období	k1gNnPc6	období
na	na	k7c4	na
rozhodný	rozhodný	k2eAgInSc4d1	rozhodný
odpor	odpor	k1gInSc4	odpor
politický	politický	k2eAgInSc4d1	politický
i	i	k8xC	i
odborný	odborný	k2eAgInSc4d1	odborný
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastějším	častý	k2eAgInSc7d3	nejčastější
argumentem	argument	k1gInSc7	argument
proti	proti	k7c3	proti
odsunu	odsun	k1gInSc3	odsun
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zpřetrhání	zpřetrhání	k1gNnSc3	zpřetrhání
nejcennějších	cenný	k2eAgFnPc2d3	nejcennější
vazeb	vazba	k1gFnPc2	vazba
ve	v	k7c6	v
městě	město	k1gNnSc6	město
a	a	k8xC	a
nádraží	nádraží	k1gNnSc1	nádraží
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
odděleno	oddělit	k5eAaPmNgNnS	oddělit
a	a	k8xC	a
příliš	příliš	k6eAd1	příliš
vzdáleno	vzdáleno	k1gNnSc1	vzdáleno
od	od	k7c2	od
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
aktivit	aktivita	k1gFnPc2	aktivita
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mezidobí	mezidobí	k1gNnSc6	mezidobí
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
seřaďovací	seřaďovací	k2eAgNnSc1d1	seřaďovací
nádraží	nádraží	k1gNnSc1	nádraží
v	v	k7c6	v
Maloměřicích	Maloměřice	k1gFnPc6	Maloměřice
<g/>
,	,	kIx,	,
vybudováno	vybudován	k2eAgNnSc1d1	vybudováno
nádraží	nádraží	k1gNnSc1	nádraží
v	v	k7c6	v
Židenicích	Židenice	k1gFnPc6	Židenice
a	a	k8xC	a
podstatně	podstatně	k6eAd1	podstatně
zmodernizováno	zmodernizován	k2eAgNnSc4d1	zmodernizováno
nádraží	nádraží	k1gNnSc4	nádraží
v	v	k7c6	v
Králově	Králův	k2eAgNnSc6d1	Královo
Poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgInPc1d1	současný
návrhy	návrh	k1gInPc1	návrh
překládají	překládat	k5eAaImIp3nP	překládat
nádraží	nádraží	k1gNnSc4	nádraží
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
1	[number]	k4	1
km	km	kA	km
jižně	jižně	k6eAd1	jižně
<g/>
.	.	kIx.	.
</s>
<s>
Projektu	projekt	k1gInSc3	projekt
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
dostává	dostávat	k5eAaImIp3nS	dostávat
nemalé	malý	k2eNgFnSc2d1	nemalá
kritiky	kritika	k1gFnSc2	kritika
a	a	k8xC	a
naráží	narážet	k5eAaImIp3nS	narážet
na	na	k7c4	na
rozhodný	rozhodný	k2eAgInSc4d1	rozhodný
odpor	odpor	k1gInSc4	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
vytýkáno	vytýkán	k2eAgNnSc1d1	vytýkáno
zejména	zejména	k6eAd1	zejména
-	-	kIx~	-
oproti	oproti	k7c3	oproti
současnému	současný	k2eAgInSc3d1	současný
stavu	stav	k1gInSc3	stav
-	-	kIx~	-
horší	horšit	k5eAaImIp3nS	horšit
pěší	pěší	k2eAgFnSc1d1	pěší
dostupnost	dostupnost	k1gFnSc1	dostupnost
centra	centrum	k1gNnSc2	centrum
<g/>
,	,	kIx,	,
nedostatečné	dostatečný	k2eNgNnSc1d1	nedostatečné
napojení	napojení	k1gNnSc1	napojení
na	na	k7c6	na
MHD	MHD	kA	MHD
a	a	k8xC	a
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
se	se	k3xPyFc4	se
i	i	k9	i
na	na	k7c4	na
určité	určitý	k2eAgInPc4d1	určitý
technické	technický	k2eAgInPc4d1	technický
nedostatky	nedostatek	k1gInPc4	nedostatek
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
některými	některý	k3yIgFnPc7	některý
skupinami	skupina	k1gFnPc7	skupina
a	a	k8xC	a
jednotlivci	jednotlivec	k1gMnPc1	jednotlivec
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
několik	několik	k4yIc1	několik
alternativních	alternativní	k2eAgFnPc2d1	alternativní
variant	varianta	k1gFnPc2	varianta
modernizace	modernizace	k1gFnSc2	modernizace
či	či	k8xC	či
úpravy	úprava	k1gFnSc2	úprava
současného	současný	k2eAgNnSc2d1	současné
nádraží	nádraží	k1gNnSc2	nádraží
včetně	včetně	k7c2	včetně
řešení	řešení	k1gNnSc2	řešení
celého	celý	k2eAgInSc2d1	celý
železničního	železniční	k2eAgInSc2d1	železniční
uzlu	uzel	k1gInSc2	uzel
<g/>
.	.	kIx.	.
</s>
<s>
Spektrum	spektrum	k1gNnSc1	spektrum
těchto	tento	k3xDgFnPc2	tento
alternativních	alternativní	k2eAgFnPc2d1	alternativní
variant	varianta	k1gFnPc2	varianta
sahá	sahat	k5eAaImIp3nS	sahat
od	od	k7c2	od
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
dosavadního	dosavadní	k2eAgNnSc2d1	dosavadní
nádraží	nádraží	k1gNnSc2	nádraží
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
odlehčení	odlehčení	k1gNnSc1	odlehčení
zavedením	zavedení	k1gNnSc7	zavedení
vlakotramvají	vlakotramvat	k5eAaImIp3nP	vlakotramvat
až	až	k9	až
k	k	k7c3	k
faktické	faktický	k2eAgFnSc3d1	faktická
novostavbě	novostavba	k1gFnSc3	novostavba
přibližně	přibližně	k6eAd1	přibližně
na	na	k7c6	na
stejném	stejný	k2eAgNnSc6d1	stejné
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgInS	objevit
se	se	k3xPyFc4	se
i	i	k9	i
kompromisní	kompromisní	k2eAgInSc1d1	kompromisní
návrh	návrh	k1gInSc1	návrh
městské	městský	k2eAgFnSc2d1	městská
rychlodráhy	rychlodráha	k1gFnSc2	rychlodráha
s	s	k7c7	s
ponecháním	ponechání	k1gNnSc7	ponechání
starého	starý	k2eAgNnSc2d1	staré
nádraží	nádraží	k1gNnSc2	nádraží
jen	jen	k9	jen
pro	pro	k7c4	pro
spoje	spoj	k1gInPc4	spoj
regionální	regionální	k2eAgFnSc2d1	regionální
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
však	však	k9	však
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2009	[number]	k4	2009
odmítnut	odmítnout	k5eAaPmNgInS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgNnSc6	tento
rozhodnutí	rozhodnutí	k1gNnSc6	rozhodnutí
se	se	k3xPyFc4	se
zase	zase	k9	zase
intenzivněji	intenzivně	k6eAd2	intenzivně
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
o	o	k7c6	o
dřívější	dřívější	k2eAgFnSc6d1	dřívější
výstavbě	výstavba	k1gFnSc6	výstavba
severojižního	severojižní	k2eAgInSc2d1	severojižní
kolejového	kolejový	k2eAgInSc2d1	kolejový
diametru	diametr	k1gInSc2	diametr
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
referendum	referendum	k1gNnSc1	referendum
o	o	k7c6	o
přesunu	přesun	k1gInSc6	přesun
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
iniciováno	iniciován	k2eAgNnSc1d1	iniciováno
koalicí	koalice	k1gFnSc7	koalice
Nádraží	nádraží	k1gNnSc4	nádraží
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
85	[number]	k4	85
%	%	kIx~	%
hlasujících	hlasující	k2eAgFnPc2d1	hlasující
se	se	k3xPyFc4	se
vyslovilo	vyslovit	k5eAaPmAgNnS	vyslovit
proti	proti	k7c3	proti
přesunu	přesun	k1gInSc3	přesun
<g/>
,	,	kIx,	,
referendum	referendum	k1gNnSc1	referendum
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
platné	platný	k2eAgInPc1d1	platný
kvůli	kvůli	k7c3	kvůli
menší	malý	k2eAgFnSc3d2	menší
než	než	k8xS	než
předepsané	předepsaný	k2eAgFnSc3d1	předepsaná
účasti	účast	k1gFnSc3	účast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2009	[number]	k4	2009
bylo	být	k5eAaImAgNnS	být
také	také	k6eAd1	také
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c6	o
pozastavení	pozastavení	k1gNnSc6	pozastavení
financování	financování	k1gNnSc2	financování
přestavby	přestavba	k1gFnSc2	přestavba
železničního	železniční	k2eAgInSc2d1	železniční
uzlu	uzel	k1gInSc2	uzel
Brno	Brno	k1gNnSc1	Brno
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
stabilizace	stabilizace	k1gFnSc2	stabilizace
rozpočtu	rozpočet	k1gInSc2	rozpočet
a	a	k8xC	a
snížení	snížení	k1gNnSc2	snížení
rozpočtového	rozpočtový	k2eAgInSc2d1	rozpočtový
schodku	schodek	k1gInSc2	schodek
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c6	o
pozastavení	pozastavení	k1gNnSc6	pozastavení
veškerých	veškerý	k3xTgFnPc2	veškerý
aktivit	aktivita	k1gFnPc2	aktivita
s	s	k7c7	s
přesunem	přesun	k1gInSc7	přesun
souvisejících	související	k2eAgMnPc2d1	související
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2011	[number]	k4	2011
brněnský	brněnský	k2eAgInSc1d1	brněnský
magistrát	magistrát	k1gInSc1	magistrát
zrušil	zrušit	k5eAaPmAgInS	zrušit
územní	územní	k2eAgNnSc4d1	územní
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
na	na	k7c4	na
soubor	soubor	k1gInSc4	soubor
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
odsun	odsun	k1gInSc1	odsun
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
věc	věc	k1gFnSc4	věc
vrátil	vrátit	k5eAaPmAgMnS	vrátit
stavebnímu	stavební	k2eAgInSc3d1	stavební
úřadu	úřad	k1gInSc3	úřad
Brno-střed	Brnotřed	k1gInSc4	Brno-střed
k	k	k7c3	k
novému	nový	k2eAgNnSc3d1	nové
projednání	projednání	k1gNnSc3	projednání
<g/>
.	.	kIx.	.
</s>
<s>
Magistrát	magistrát	k1gInSc1	magistrát
rozhodoval	rozhodovat	k5eAaImAgInS	rozhodovat
o	o	k7c6	o
odvoláních	odvolání	k1gNnPc6	odvolání
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
firem	firma	k1gFnPc2	firma
i	i	k8xC	i
občanských	občanský	k2eAgNnPc2d1	občanské
sdružení	sdružení	k1gNnPc2	sdružení
proti	proti	k7c3	proti
záměru	záměr	k1gInSc3	záměr
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2011	[number]	k4	2011
skupina	skupina	k1gFnSc1	skupina
architektů	architekt	k1gMnPc2	architekt
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
svůj	svůj	k3xOyFgInSc4	svůj
návrh	návrh	k1gInSc4	návrh
jako	jako	k8xC	jako
alternativu	alternativa	k1gFnSc4	alternativa
k	k	k7c3	k
plánům	plán	k1gInPc3	plán
brněnského	brněnský	k2eAgInSc2d1	brněnský
magistrátu	magistrát	k1gInSc2	magistrát
na	na	k7c4	na
odsun	odsun	k1gInSc4	odsun
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
Inspirovali	inspirovat	k5eAaBmAgMnP	inspirovat
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
aktivitou	aktivita	k1gFnSc7	aktivita
koalice	koalice	k1gFnSc2	koalice
Nádraží	nádraží	k1gNnSc2	nádraží
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
představil	představit	k5eAaPmAgMnS	představit
ing.	ing.	kA	ing.
Procházka	Procházka	k1gMnSc1	Procházka
údajně	údajně	k6eAd1	údajně
nejlevnější	levný	k2eAgNnSc4d3	nejlevnější
řešení	řešení	k1gNnSc4	řešení
<g/>
,	,	kIx,	,
s	s	k7c7	s
rovnými	rovný	k2eAgNnPc7d1	rovné
nástupišti	nástupiště	k1gNnPc7	nástupiště
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
města	město	k1gNnSc2	město
přímo	přímo	k6eAd1	přímo
u	u	k7c2	u
tramvají	tramvaj	k1gFnPc2	tramvaj
a	a	k8xC	a
s	s	k7c7	s
autobusovým	autobusový	k2eAgNnSc7d1	autobusové
nádražím	nádraží	k1gNnSc7	nádraží
pod	pod	k7c7	pod
kolejištěm	kolejiště	k1gNnSc7	kolejiště
na	na	k7c6	na
Nových	Nových	k2eAgInPc6d1	Nových
sadech	sad	k1gInPc6	sad
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2015	[number]	k4	2015
bylo	být	k5eAaImAgNnS	být
Českou	český	k2eAgFnSc7d1	Česká
komorou	komora	k1gFnSc7	komora
architektů	architekt	k1gMnPc2	architekt
v	v	k7c6	v
součinnosti	součinnost	k1gFnSc6	součinnost
s	s	k7c7	s
Českou	český	k2eAgFnSc7d1	Česká
komorou	komora	k1gFnSc7	komora
inženýrů	inženýr	k1gMnPc2	inženýr
uspořádáno	uspořádat	k5eAaPmNgNnS	uspořádat
v	v	k7c6	v
brněnské	brněnský	k2eAgFnSc6d1	brněnská
vile	vila	k1gFnSc6	vila
Stiassni	Stiassni	k1gFnSc2	Stiassni
jednání	jednání	k1gNnSc2	jednání
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
mezi	mezi	k7c4	mezi
odborníky	odborník	k1gMnPc4	odborník
převážil	převážit	k5eAaPmAgInS	převážit
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
přesun	přesun	k1gInSc1	přesun
nádraží	nádraží	k1gNnSc2	nádraží
do	do	k7c2	do
polohy	poloha	k1gFnSc2	poloha
"	"	kIx"	"
<g/>
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
<g/>
"	"	kIx"	"
není	být	k5eNaImIp3nS	být
vhodný	vhodný	k2eAgMnSc1d1	vhodný
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ledna	leden	k1gInSc2	leden
do	do	k7c2	do
července	červenec	k1gInSc2	červenec
2014	[number]	k4	2014
probíhal	probíhat	k5eAaImAgInS	probíhat
sběr	sběr	k1gInSc1	sběr
podpisů	podpis	k1gInPc2	podpis
k	k	k7c3	k
uspořádání	uspořádání	k1gNnSc2	uspořádání
druhého	druhý	k4xOgNnSc2	druhý
referenda	referendum	k1gNnSc2	referendum
o	o	k7c6	o
poloze	poloha	k1gFnSc6	poloha
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
iniciovala	iniciovat	k5eAaBmAgFnS	iniciovat
občanská	občanský	k2eAgFnSc1d1	občanská
aliance	aliance	k1gFnSc1	aliance
Referendum	referendum	k1gNnSc1	referendum
2014	[number]	k4	2014
a	a	k8xC	a
které	který	k3yIgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
původně	původně	k6eAd1	původně
proběhnout	proběhnout	k5eAaPmF	proběhnout
v	v	k7c6	v
termínu	termín	k1gInSc6	termín
pořádání	pořádání	k1gNnSc2	pořádání
obecních	obecní	k2eAgFnPc2d1	obecní
voleb	volba	k1gFnPc2	volba
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
původní	původní	k2eAgFnSc2d1	původní
otázky	otázka	k1gFnSc2	otázka
vyžadující	vyžadující	k2eAgInSc4d1	vyžadující
souhlas	souhlas	k1gInSc4	souhlas
nebo	nebo	k8xC	nebo
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
s	s	k7c7	s
navrhovanou	navrhovaný	k2eAgFnSc7d1	navrhovaná
polohou	poloha	k1gFnSc7	poloha
nádraží	nádraží	k1gNnSc2	nádraží
přidali	přidat	k5eAaPmAgMnP	přidat
iniciátoři	iniciátor	k1gMnPc1	iniciátor
také	také	k9	také
otázku	otázka	k1gFnSc4	otázka
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
si	se	k3xPyFc3	se
občané	občan	k1gMnPc1	občan
přejí	přát	k5eAaImIp3nP	přát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
konkrétní	konkrétní	k2eAgNnSc1d1	konkrétní
řešení	řešení	k1gNnSc1	řešení
vzešlo	vzejít	k5eAaPmAgNnS	vzejít
z	z	k7c2	z
otevřené	otevřený	k2eAgFnSc2d1	otevřená
návrhové	návrhový	k2eAgFnSc2d1	návrhová
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
vyhýbá	vyhýbat	k5eAaImIp3nS	vyhýbat
<g/>
.	.	kIx.	.
</s>
<s>
Magistrát	magistrát	k1gInSc1	magistrát
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
21	[number]	k4	21
000	[number]	k4	000
předložených	předložený	k2eAgInPc2d1	předložený
podpisů	podpis	k1gInPc2	podpis
bylo	být	k5eAaImAgNnS	být
platných	platný	k2eAgMnPc2d1	platný
pouze	pouze	k6eAd1	pouze
13	[number]	k4	13
257	[number]	k4	257
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
aktivisty	aktivista	k1gMnPc4	aktivista
k	k	k7c3	k
nápravě	náprava	k1gFnSc3	náprava
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
je	být	k5eAaImIp3nS	být
nutných	nutný	k2eAgNnPc2d1	nutné
alespoň	alespoň	k9	alespoň
18	[number]	k4	18
508	[number]	k4	508
podpisů	podpis	k1gInPc2	podpis
-	-	kIx~	-
6	[number]	k4	6
%	%	kIx~	%
oprávněných	oprávněný	k2eAgMnPc2d1	oprávněný
voličů	volič	k1gMnPc2	volič
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Přípravný	přípravný	k2eAgInSc1d1	přípravný
výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
referenda	referendum	k1gNnSc2	referendum
s	s	k7c7	s
údajnou	údajný	k2eAgFnSc7d1	údajná
chybností	chybnost	k1gFnSc7	chybnost
podpisů	podpis	k1gInPc2	podpis
a	a	k8xC	a
s	s	k7c7	s
další	další	k2eAgFnSc7d1	další
výtkou	výtka	k1gFnSc7	výtka
úřadu	úřad	k1gInSc2	úřad
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
a	a	k8xC	a
podal	podat	k5eAaPmAgMnS	podat
na	na	k7c4	na
brněnský	brněnský	k2eAgInSc4d1	brněnský
magistrát	magistrát	k1gInSc4	magistrát
u	u	k7c2	u
příslušného	příslušný	k2eAgInSc2d1	příslušný
soudu	soud	k1gInSc2	soud
žalobu	žaloba	k1gFnSc4	žaloba
<g/>
.	.	kIx.	.
</s>
<s>
Krajský	krajský	k2eAgInSc1d1	krajský
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
v	v	k7c6	v
usnesení	usnesení	k1gNnSc6	usnesení
vydaném	vydaný	k2eAgNnSc6d1	vydané
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2014	[number]	k4	2014
konstatoval	konstatovat	k5eAaBmAgInS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
návrh	návrh	k1gInSc1	návrh
přípravného	přípravný	k2eAgInSc2d1	přípravný
výboru	výbor	k1gInSc2	výbor
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
doplněný	doplněný	k2eAgInSc1d1	doplněný
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
neměl	mít	k5eNaImAgMnS	mít
nedostatky	nedostatek	k1gInPc4	nedostatek
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
plyne	plynout	k5eAaImIp3nS	plynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
referendum	referendum	k1gNnSc4	referendum
měl	mít	k5eAaImAgInS	mít
brněnský	brněnský	k2eAgInSc1d1	brněnský
magistrát	magistrát	k1gInSc1	magistrát
vyhlásit	vyhlásit	k5eAaPmF	vyhlásit
současně	současně	k6eAd1	současně
s	s	k7c7	s
termínem	termín	k1gInSc7	termín
obecních	obecní	k2eAgFnPc2d1	obecní
voleb	volba	k1gFnPc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c4	na
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
soudu	soud	k1gInSc2	soud
se	se	k3xPyFc4	se
přípravný	přípravný	k2eAgInSc1d1	přípravný
výbor	výbor	k1gInSc1	výbor
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
využít	využít	k5eAaPmF	využít
pro	pro	k7c4	pro
uspořádání	uspořádání	k1gNnSc4	uspořádání
referenda	referendum	k1gNnSc2	referendum
nejbližší	blízký	k2eAgInSc4d3	Nejbližší
termín	termín	k1gInSc4	termín
dalších	další	k2eAgFnPc2d1	další
voleb	volba	k1gFnPc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
volby	volba	k1gFnPc1	volba
do	do	k7c2	do
krajských	krajský	k2eAgFnPc2d1	krajská
zastupitelstev	zastupitelstvo	k1gNnPc2	zastupitelstvo
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
7	[number]	k4	7
<g/>
.	.	kIx.	.
a	a	k8xC	a
8	[number]	k4	8
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgInSc3	tento
požadavku	požadavek	k1gInSc3	požadavek
vyhovělo	vyhovět	k5eAaPmAgNnS	vyhovět
zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
tohoto	tento	k3xDgNnSc2	tento
referenda	referendum	k1gNnSc2	referendum
se	se	k3xPyFc4	se
však	však	k9	však
nezúčastnil	zúčastnit	k5eNaPmAgMnS	zúčastnit
dostatečný	dostatečný	k2eAgInSc4d1	dostatečný
počet	počet	k1gInSc4	počet
hlasujících	hlasující	k1gMnPc2	hlasující
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
platné	platný	k2eAgNnSc1d1	platné
<g/>
.	.	kIx.	.
</s>
