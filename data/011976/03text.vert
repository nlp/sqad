<p>
<s>
Paul	Paul	k1gMnSc1	Paul
Thomas	Thomas	k1gMnSc1	Thomas
Mann	Mann	k1gMnSc1	Mann
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1875	[number]	k4	1875
<g/>
,	,	kIx,	,
Lübeck	Lübeck	k1gInSc1	Lübeck
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
Curych	Curych	k1gInSc1	Curych
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
prozaik	prozaik	k1gMnSc1	prozaik
a	a	k8xC	a
esejista	esejista	k1gMnSc1	esejista
<g/>
,	,	kIx,	,
držitel	držitel	k1gMnSc1	držitel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příbuzenstvo	příbuzenstvo	k1gNnSc1	příbuzenstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
bratra	bratr	k1gMnSc4	bratr
spisovatele	spisovatel	k1gMnSc2	spisovatel
Luize	Luize	k1gFnSc2	Luize
Heinricha	Heinrich	k1gMnSc2	Heinrich
Manna	Mann	k1gMnSc2	Mann
(	(	kIx(	(
<g/>
1871	[number]	k4	1871
<g/>
–	–	k?	–
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
a	a	k8xC	a
ekonoma	ekonom	k1gMnSc2	ekonom
Karla	Karel	k1gMnSc2	Karel
Viktora	Viktor	k1gMnSc2	Viktor
Manna	Mann	k1gMnSc2	Mann
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
–	–	k?	–
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
otce	otec	k1gMnSc2	otec
spisovatele	spisovatel	k1gMnSc2	spisovatel
Klause	Klaus	k1gMnSc2	Klaus
Manna	Mann	k1gMnSc2	Mann
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
–	–	k?	–
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
a	a	k8xC	a
historika	historik	k1gMnSc2	historik
a	a	k8xC	a
politologa	politolog	k1gMnSc2	politolog
Golo	Golo	k6eAd1	Golo
Manna	Mann	k1gMnSc4	Mann
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Známými	známá	k1gFnPc7	známá
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
i	i	k9	i
jeho	jeho	k3xOp3gFnPc1	jeho
dcery	dcera	k1gFnPc1	dcera
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
kabaretiérka	kabaretiérka	k1gFnSc1	kabaretiérka
a	a	k8xC	a
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Erika	Erika	k1gFnSc1	Erika
Mannová	Mannová	k1gFnSc1	Mannová
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
–	–	k?	–
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Monika	Monika	k1gFnSc1	Monika
Mannová	Mannová	k1gFnSc1	Mannová
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
a	a	k8xC	a
ekoložka	ekoložka	k1gFnSc1	ekoložka
a	a	k8xC	a
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Elisabeth	Elisabeth	k1gInSc1	Elisabeth
Mannová-Borgeseová	Mannová-Borgeseová	k1gFnSc1	Mannová-Borgeseová
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
jeho	jeho	k3xOp3gMnSc1	jeho
vnuk	vnuk	k1gMnSc1	vnuk
<g/>
,	,	kIx,	,
psycholog	psycholog	k1gMnSc1	psycholog
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
Fridolin	Fridolina	k1gFnPc2	Fridolina
Mann	Mann	k1gMnSc1	Mann
(	(	kIx(	(
<g/>
*	*	kIx~	*
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Řada	řada	k1gFnSc1	řada
rodinných	rodinný	k2eAgMnPc2d1	rodinný
příslušníků	příslušník	k1gMnPc2	příslušník
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
vzorem	vzor	k1gInSc7	vzor
pro	pro	k7c4	pro
postavy	postava	k1gFnPc4	postava
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
jeho	jeho	k3xOp3gInSc6	jeho
dílech	díl	k1gInPc6	díl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Dětství	dětství	k1gNnSc4	dětství
a	a	k8xC	a
mládí	mládí	k1gNnSc4	mládí
===	===	k?	===
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Lübecku	Lübeck	k1gInSc6	Lübeck
v	v	k7c6	v
patricijské	patricijský	k2eAgFnSc6d1	patricijská
rodině	rodina	k1gFnSc6	rodina
obchodníka	obchodník	k1gMnSc2	obchodník
s	s	k7c7	s
obilím	obilí	k1gNnSc7	obilí
Thomase	Thomas	k1gMnSc2	Thomas
Johanna	Johann	k1gMnSc2	Johann
Heinricha	Heinrich	k1gMnSc2	Heinrich
Manna	Mann	k1gMnSc2	Mann
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
hudebně	hudebně	k6eAd1	hudebně
nadaná	nadaný	k2eAgFnSc1d1	nadaná
Júlie	Júlie	k1gFnSc1	Júlie
da	da	k?	da
Silva	Silva	k1gFnSc1	Silva
Bruhns	Bruhnsa	k1gFnPc2	Bruhnsa
<g/>
,	,	kIx,	,
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
Brazílie	Brazílie	k1gFnSc2	Brazílie
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
sobě	se	k3xPyFc3	se
kreolskou	kreolský	k2eAgFnSc4d1	kreolská
krev	krev	k1gFnSc4	krev
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
otcově	otcův	k2eAgFnSc6d1	otcova
smrti	smrt	k1gFnSc6	smrt
roku	rok	k1gInSc2	rok
1893	[number]	k4	1893
a	a	k8xC	a
rozpadu	rozpad	k1gInSc3	rozpad
firmy	firma	k1gFnSc2	firma
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Mnichova	Mnichov	k1gInSc2	Mnichov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Thomas	Thomas	k1gMnSc1	Thomas
Mann	Mann	k1gMnSc1	Mann
žil	žít	k5eAaImAgMnS	žít
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
volontér	volontér	k1gMnSc1	volontér
pojišťovací	pojišťovací	k2eAgFnSc2d1	pojišťovací
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
rok	rok	k1gInSc4	rok
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
redakci	redakce	k1gFnSc6	redakce
satirického	satirický	k2eAgInSc2d1	satirický
časopisu	časopis	k1gInSc2	časopis
Simplicissimus	Simplicissimus	k1gMnSc1	Simplicissimus
<g/>
,	,	kIx,	,
poslouchal	poslouchat	k5eAaImAgMnS	poslouchat
přednášky	přednáška	k1gFnPc4	přednáška
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
1895	[number]	k4	1895
<g/>
–	–	k?	–
<g/>
1898	[number]	k4	1898
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Heinrichem	Heinrich	k1gMnSc7	Heinrich
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
vlastními	vlastní	k2eAgInPc7d1	vlastní
literárními	literární	k2eAgInPc7d1	literární
pokusy	pokus	k1gInPc7	pokus
začal	začít	k5eAaPmAgMnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1893	[number]	k4	1893
(	(	kIx(	(
<g/>
črta	črta	k1gFnSc1	črta
Vize	vize	k1gFnSc1	vize
<g/>
)	)	kIx)	)
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
debutoval	debutovat	k5eAaBmAgMnS	debutovat
svou	svůj	k3xOyFgFnSc4	svůj
novelou	novela	k1gFnSc7	novela
Gefallen	Gefallen	k1gInSc4	Gefallen
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
velké	velký	k2eAgNnSc1d1	velké
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
Buddenbrookovi	Buddenbrook	k1gMnSc3	Buddenbrook
<g/>
,	,	kIx,	,
vyšel	vyjít	k5eAaPmAgMnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
sklidila	sklidit	k5eAaPmAgFnS	sklidit
okamžitý	okamžitý	k2eAgInSc4d1	okamžitý
úspěch	úspěch	k1gInSc4	úspěch
a	a	k8xC	a
z	z	k7c2	z
Manna	Mann	k1gMnSc2	Mann
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
téměř	téměř	k6eAd1	téměř
přes	přes	k7c4	přes
noc	noc	k1gFnSc4	noc
proslulý	proslulý	k2eAgMnSc1d1	proslulý
německý	německý	k2eAgMnSc1d1	německý
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
vzdělaného	vzdělaný	k2eAgMnSc2d1	vzdělaný
židovského	židovský	k2eAgMnSc2d1	židovský
obchodníka	obchodník	k1gMnSc2	obchodník
Katiou	Katiý	k2eAgFnSc4d1	Katiý
Pringsheimovou	Pringsheimová	k1gFnSc4	Pringsheimová
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
měl	mít	k5eAaImAgMnS	mít
šest	šest	k4xCc4	šest
dětí	dítě	k1gFnPc2	dítě
(	(	kIx(	(
<g/>
Eriku	Erika	k1gFnSc4	Erika
<g/>
,	,	kIx,	,
Klause	Klaus	k1gMnSc4	Klaus
<g/>
,	,	kIx,	,
Gola	Golus	k1gMnSc4	Golus
<g/>
,	,	kIx,	,
Moniku	Monika	k1gFnSc4	Monika
<g/>
,	,	kIx,	,
Elisabeth	Elisabeth	k1gInSc1	Elisabeth
a	a	k8xC	a
Michaela	Michaela	k1gFnSc1	Michaela
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
výrazněji	výrazně	k6eAd2	výrazně
proslavily	proslavit	k5eAaPmAgFnP	proslavit
dvě	dva	k4xCgFnPc1	dva
<g/>
:	:	kIx,	:
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
stal	stát	k5eAaPmAgMnS	stát
spisovatelem	spisovatel	k1gMnSc7	spisovatel
<g/>
,	,	kIx,	,
a	a	k8xC	a
Golo	Golo	k1gMnSc1	Golo
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgInS	věnovat
historii	historie	k1gFnSc3	historie
<g/>
.	.	kIx.	.
</s>
<s>
Fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
spojil	spojit	k5eAaPmAgMnS	spojit
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
s	s	k7c7	s
ženou	žena	k1gFnSc7	žena
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
jediná	jediný	k2eAgFnSc1d1	jediná
možnost	možnost	k1gFnSc1	možnost
veřejného	veřejný	k2eAgNnSc2d1	veřejné
partnerského	partnerský	k2eAgNnSc2d1	partnerské
soužití	soužití	k1gNnSc2	soužití
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnSc4	jeho
okolí	okolí	k1gNnSc4	okolí
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
překvapující	překvapující	k2eAgMnSc1d1	překvapující
<g/>
.	.	kIx.	.
</s>
<s>
Nacházel	nacházet	k5eAaImAgMnS	nacházet
totiž	totiž	k9	totiž
větší	veliký	k2eAgNnSc4d2	veliký
zalíbení	zalíbení	k1gNnSc4	zalíbení
v	v	k7c6	v
mladých	mladý	k2eAgMnPc6d1	mladý
mužích	muž	k1gMnPc6	muž
než	než	k8xS	než
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jeho	jeho	k3xOp3gInSc1	jeho
homosexuální	homosexuální	k2eAgInSc1d1	homosexuální
či	či	k8xC	či
snad	snad	k9	snad
bisexuální	bisexuální	k2eAgFnSc1d1	bisexuální
orientace	orientace	k1gFnSc1	orientace
se	se	k3xPyFc4	se
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
až	až	k9	až
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgInP	být
odtajněny	odtajnit	k5eAaPmNgInP	odtajnit
jeho	jeho	k3xOp3gInPc1	jeho
soukromé	soukromý	k2eAgInPc1d1	soukromý
deníky	deník	k1gInPc1	deník
<g/>
.	.	kIx.	.
</s>
<s>
Homosexuální	homosexuální	k2eAgInPc4d1	homosexuální
motivy	motiv	k1gInPc4	motiv
jsou	být	k5eAaImIp3nP	být
časté	častý	k2eAgInPc1d1	častý
i	i	k9	i
v	v	k7c6	v
Mannově	Mannův	k2eAgNnSc6d1	Mannovo
díle	dílo	k1gNnSc6	dílo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
románu	román	k1gInSc6	román
Buddenbrookovi	Buddenbrookův	k2eAgMnPc1d1	Buddenbrookův
či	či	k8xC	či
v	v	k7c6	v
novelách	novela	k1gFnPc6	novela
Smrt	smrt	k1gFnSc1	smrt
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
a	a	k8xC	a
Tonio	Tonio	k6eAd1	Tonio
Kröger	Krögra	k1gFnPc2	Krögra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dospělost	dospělost	k1gFnSc4	dospělost
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
onemocněla	onemocnět	k5eAaPmAgFnS	onemocnět
Katia	Katia	k1gFnSc1	Katia
tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
a	a	k8xC	a
pobývala	pobývat	k5eAaImAgFnS	pobývat
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
v	v	k7c6	v
plicním	plicní	k2eAgNnSc6d1	plicní
sanatoriu	sanatorium	k1gNnSc6	sanatorium
ve	v	k7c6	v
švýcarském	švýcarský	k2eAgInSc6d1	švýcarský
Davosu	Davos	k1gInSc6	Davos
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
léčba	léčba	k1gFnSc1	léčba
<g/>
,	,	kIx,	,
zážitky	zážitek	k1gInPc1	zážitek
ze	z	k7c2	z
sanatoria	sanatorium	k1gNnSc2	sanatorium
i	i	k8xC	i
obavy	obava	k1gFnPc1	obava
z	z	k7c2	z
možné	možný	k2eAgFnSc2d1	možná
smrti	smrt	k1gFnSc2	smrt
inspirovaly	inspirovat	k5eAaBmAgFnP	inspirovat
Thomase	Thomas	k1gMnSc4	Thomas
Manna	Mann	k1gMnSc4	Mann
k	k	k7c3	k
druhému	druhý	k4xOgInSc3	druhý
významnému	významný	k2eAgInSc3d1	významný
románu	román	k1gInSc3	román
<g/>
,	,	kIx,	,
snovému	snový	k2eAgNnSc3d1	snové
vyprávění	vyprávění	k1gNnSc3	vyprávění
z	z	k7c2	z
luxusní	luxusní	k2eAgFnSc2d1	luxusní
plicní	plicní	k2eAgFnSc2d1	plicní
ozdravovny	ozdravovna	k1gFnSc2	ozdravovna
Kouzelný	kouzelný	k2eAgInSc1d1	kouzelný
vrch	vrch	k1gInSc1	vrch
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
a	a	k8xC	a
dokončil	dokončit	k5eAaPmAgInS	dokončit
ho	on	k3xPp3gMnSc4	on
roku	rok	k1gInSc3	rok
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
,	,	kIx,	,
publikován	publikován	k2eAgMnSc1d1	publikován
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
v	v	k7c6	v
esejích	esej	k1gFnPc6	esej
Úvahy	úvaha	k1gFnSc2	úvaha
nepolitického	politický	k2eNgMnSc4d1	nepolitický
člověka	člověk	k1gMnSc4	člověk
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
zásadních	zásadní	k2eAgInPc2d1	zásadní
rozporů	rozpor	k1gInPc2	rozpor
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
Heinrichem	Heinrich	k1gMnSc7	Heinrich
<g/>
,	,	kIx,	,
když	když	k8xS	když
válku	válka	k1gFnSc4	válka
neodmítl	odmítnout	k5eNaPmAgMnS	odmítnout
a	a	k8xC	a
zastal	zastat	k5eAaPmAgMnS	zastat
se	se	k3xPyFc4	se
prušáctví	prušáctví	k1gNnSc1	prušáctví
a	a	k8xC	a
německého	německý	k2eAgInSc2d1	německý
nacionalismu	nacionalismus	k1gInSc2	nacionalismus
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
však	však	k9	však
doznal	doznat	k5eAaPmAgInS	doznat
svůj	svůj	k3xOyFgInSc4	svůj
omyl	omyl	k1gInSc4	omyl
a	a	k8xC	a
přiklonil	přiklonit	k5eAaPmAgMnS	přiklonit
se	se	k3xPyFc4	se
k	k	k7c3	k
demokracii	demokracie	k1gFnSc3	demokracie
a	a	k8xC	a
humanismu	humanismus	k1gInSc2	humanismus
jako	jako	k9	jako
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
již	již	k6eAd1	již
stýkal	stýkat	k5eAaImAgInS	stýkat
s	s	k7c7	s
předními	přední	k2eAgMnPc7d1	přední
umělci	umělec	k1gMnPc7	umělec
a	a	k8xC	a
spisovateli	spisovatel	k1gMnPc7	spisovatel
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
např.	např.	kA	např.
s	s	k7c7	s
Hermannem	Hermann	k1gMnSc7	Hermann
Brochem	Broch	k1gMnSc7	Broch
či	či	k8xC	či
Robertem	Robert	k1gMnSc7	Robert
Musilem	Musil	k1gMnSc7	Musil
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
výrazně	výrazně	k6eAd1	výrazně
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
finanční	finanční	k2eAgFnSc6d1	finanční
tísni	tíseň	k1gFnSc6	tíseň
a	a	k8xC	a
jehož	jenž	k3xRgNnSc2	jenž
díla	dílo	k1gNnSc2	dílo
si	se	k3xPyFc3	se
velmi	velmi	k6eAd1	velmi
vážil	vážit	k5eAaImAgMnS	vážit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
pak	pak	k6eAd1	pak
získal	získat	k5eAaPmAgMnS	získat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
"	"	kIx"	"
<g/>
zejména	zejména	k9	zejména
za	za	k7c4	za
jeho	on	k3xPp3gMnSc4	on
mohutný	mohutný	k2eAgInSc1d1	mohutný
román	román	k1gInSc1	román
Buddenbrookovi	Buddenbrook	k1gMnSc3	Buddenbrook
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
během	během	k7c2	během
let	léto	k1gNnPc2	léto
získával	získávat	k5eAaImAgInS	získávat
stále	stále	k6eAd1	stále
vyšší	vysoký	k2eAgNnSc4d2	vyšší
uznání	uznání	k1gNnSc4	uznání
jako	jako	k8xS	jako
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
klasických	klasický	k2eAgNnPc2d1	klasické
děl	dělo	k1gNnPc2	dělo
současné	současný	k2eAgFnSc2d1	současná
literatury	literatura	k1gFnSc2	literatura
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
citace	citace	k1gFnPc1	citace
z	z	k7c2	z
odůvodnění	odůvodnění	k1gNnSc2	odůvodnění
Švédské	švédský	k2eAgFnSc2d1	švédská
akademie	akademie	k1gFnSc2	akademie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
kandidátů	kandidát	k1gMnPc2	kandidát
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
ocenění	ocenění	k1gNnSc4	ocenění
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
značné	značný	k2eAgFnSc2d1	značná
finanční	finanční	k2eAgFnSc2d1	finanční
sumy	suma	k1gFnSc2	suma
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
obdržel	obdržet	k5eAaPmAgInS	obdržet
<g/>
,	,	kIx,	,
umořoval	umořovat	k5eAaImAgInS	umořovat
vysoké	vysoký	k2eAgInPc4d1	vysoký
dluhy	dluh	k1gInPc4	dluh
svých	svůj	k3xOyFgFnPc2	svůj
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Válka	válka	k1gFnSc1	válka
a	a	k8xC	a
exil	exil	k1gInSc1	exil
===	===	k?	===
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
s	s	k7c7	s
obavami	obava	k1gFnPc7	obava
sledoval	sledovat	k5eAaImAgInS	sledovat
hrozící	hrozící	k2eAgInSc1d1	hrozící
nástup	nástup	k1gInSc1	nástup
nacismu	nacismus	k1gInSc2	nacismus
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
řeči	řeč	k1gFnSc6	řeč
Apel	apel	k1gInSc1	apel
na	na	k7c4	na
rozum	rozum	k1gInSc4	rozum
pronesené	pronesený	k2eAgNnSc1d1	pronesené
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
jako	jako	k8xS	jako
tzv.	tzv.	kA	tzv.
Německý	německý	k2eAgInSc1d1	německý
projev	projev	k1gInSc1	projev
varoval	varovat	k5eAaImAgInS	varovat
před	před	k7c7	před
touto	tento	k3xDgFnSc7	tento
hrozbou	hrozba	k1gFnSc7	hrozba
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
nadšené	nadšený	k2eAgNnSc4d1	nadšené
přijetí	přijetí	k1gNnSc4	přijetí
nebylo	být	k5eNaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
varování	varování	k1gNnSc1	varování
vyslyšeno	vyslyšen	k2eAgNnSc1d1	vyslyšeno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
opustil	opustit	k5eAaPmAgInS	opustit
proto	proto	k8xC	proto
s	s	k7c7	s
ženou	žena	k1gFnSc7	žena
Katiou	Katiý	k2eAgFnSc7d1	Katiý
přes	přes	k7c4	přes
Paříž	Paříž	k1gFnSc4	Paříž
Německo	Německo	k1gNnSc1	Německo
a	a	k8xC	a
uchýlil	uchýlit	k5eAaPmAgMnS	uchýlit
se	se	k3xPyFc4	se
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Küsnachtu	Küsnacht	k1gInSc6	Küsnacht
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Curychu	Curych	k1gInSc2	Curych
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1934	[number]	k4	1934
a	a	k8xC	a
1935	[number]	k4	1935
podnikl	podniknout	k5eAaPmAgMnS	podniknout
první	první	k4xOgFnPc4	první
cesty	cesta	k1gFnPc4	cesta
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
získal	získat	k5eAaPmAgMnS	získat
díky	díky	k7c3	díky
udělení	udělení	k1gNnSc3	udělení
domovského	domovský	k2eAgNnSc2d1	domovské
práva	právo	k1gNnSc2	právo
městečkem	městečko	k1gNnSc7	městečko
Proseč	Proseč	k1gFnSc1	Proseč
československé	československý	k2eAgNnSc4d1	Československé
státní	státní	k2eAgNnSc4d1	státní
občanství	občanství	k1gNnSc4	občanství
a	a	k8xC	a
cestovní	cestovní	k2eAgInSc4d1	cestovní
pas	pas	k1gInSc4	pas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
USA	USA	kA	USA
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
občanem	občan	k1gMnSc7	občan
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
pracoval	pracovat	k5eAaImAgMnS	pracovat
Mann	Mann	k1gMnSc1	Mann
na	na	k7c6	na
rozsáhlé	rozsáhlý	k2eAgFnSc6d1	rozsáhlá
románové	románový	k2eAgFnSc6d1	románová
tetralogii	tetralogie	k1gFnSc6	tetralogie
Josef	Josef	k1gMnSc1	Josef
a	a	k8xC	a
bratří	bratřit	k5eAaImIp3nS	bratřit
jeho	jeho	k3xOp3gNnSc1	jeho
<g/>
,	,	kIx,	,
napsané	napsaný	k2eAgNnSc1d1	napsané
na	na	k7c4	na
biblický	biblický	k2eAgInSc4d1	biblický
námět	námět	k1gInSc4	námět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
žil	žít	k5eAaImAgMnS	žít
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
vydal	vydat	k5eAaPmAgMnS	vydat
své	svůj	k3xOyFgNnSc4	svůj
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejznámější	známý	k2eAgNnSc4d3	nejznámější
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
Doktor	doktor	k1gMnSc1	doktor
Faustus	Faustus	k1gMnSc1	Faustus
<g/>
,	,	kIx,	,
napsaný	napsaný	k2eAgMnSc1d1	napsaný
za	za	k7c2	za
jeho	on	k3xPp3gInSc2	on
pobytu	pobyt	k1gInSc2	pobyt
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Faustovská	faustovský	k2eAgFnSc1d1	faustovská
inspirace	inspirace	k1gFnSc1	inspirace
<g/>
,	,	kIx,	,
přenesená	přenesený	k2eAgFnSc1d1	přenesená
do	do	k7c2	do
soudobého	soudobý	k2eAgNnSc2d1	soudobé
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
spojuje	spojovat	k5eAaImIp3nS	spojovat
v	v	k7c6	v
postavě	postava	k1gFnSc6	postava
skladatele	skladatel	k1gMnSc2	skladatel
Adriana	Adrian	k1gMnSc2	Adrian
Leverkühna	Leverkühn	k1gMnSc2	Leverkühn
otázky	otázka	k1gFnSc2	otázka
umělecké	umělecký	k2eAgFnSc2d1	umělecká
tvorby	tvorba	k1gFnSc2	tvorba
a	a	k8xC	a
svobody	svoboda	k1gFnSc2	svoboda
duše	duše	k1gFnSc2	duše
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
pobytu	pobyt	k1gInSc6	pobyt
v	v	k7c6	v
nizozemských	nizozemský	k2eAgFnPc6d1	nizozemská
přímořských	přímořský	k2eAgFnPc6d1	přímořská
lázních	lázeň	k1gFnPc6	lázeň
v	v	k7c4	v
Nordwijku	Nordwijka	k1gFnSc4	Nordwijka
onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
arteriosklerózou	arterioskleróza	k1gFnSc7	arterioskleróza
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Curychu	Curych	k1gInSc2	Curych
byl	být	k5eAaImAgInS	být
hospitalizován	hospitalizován	k2eAgInSc1d1	hospitalizován
a	a	k8xC	a
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1955	[number]	k4	1955
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
80	[number]	k4	80
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
mu	on	k3xPp3gMnSc3	on
již	již	k6eAd1	již
nedovolila	dovolit	k5eNaPmAgFnS	dovolit
dopsat	dopsat	k5eAaPmF	dopsat
román	román	k1gInSc4	román
o	o	k7c6	o
životě	život	k1gInSc6	život
virtuózního	virtuózní	k2eAgMnSc2d1	virtuózní
podvodníka	podvodník	k1gMnSc2	podvodník
Zpověď	zpověď	k1gFnSc1	zpověď
hochštaplera	hochštapler	k1gMnSc4	hochštapler
Felixe	Felix	k1gMnSc4	Felix
Krulla	Krull	k1gMnSc4	Krull
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pochován	pochován	k2eAgMnSc1d1	pochován
ve	v	k7c6	v
švýcarském	švýcarský	k2eAgInSc6d1	švýcarský
Kilchbergu	Kilchberg	k1gInSc6	Kilchberg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Novely	novela	k1gFnPc1	novela
a	a	k8xC	a
povídky	povídka	k1gFnPc1	povídka
===	===	k?	===
</s>
</p>
<p>
<s>
Vision	vision	k1gInSc1	vision
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
,	,	kIx,	,
Vize	vize	k1gFnSc1	vize
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
črta	črta	k1gFnSc1	črta
</s>
</p>
<p>
<s>
Gefallen	Gefallen	k1gInSc1	Gefallen
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
větší	veliký	k2eAgFnSc1d2	veliký
Mannova	Mannův	k2eAgFnSc1d1	Mannova
práce	práce	k1gFnSc1	práce
</s>
</p>
<p>
<s>
Der	drát	k5eAaImRp2nS	drát
Wille	Wille	k1gInSc1	Wille
zum	zum	k?	zum
Glück	Glück	k1gInSc1	Glück
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vůle	vůle	k1gFnSc1	vůle
ke	k	k7c3	k
štěstí	štěstí	k1gNnSc3	štěstí
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Enttäuschung	Enttäuschung	k1gInSc1	Enttäuschung
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
Zklamání	zklamání	k1gNnSc4	zklamání
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Der	drát	k5eAaImRp2nS	drát
Tod	Tod	k1gFnSc1	Tod
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
,	,	kIx,	,
Smrt	smrt	k1gFnSc1	smrt
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Der	drát	k5eAaImRp2nS	drát
kleine	kleinout	k5eAaPmIp3nS	kleinout
Herr	Herr	k1gMnSc1	Herr
Friedemann	Friedemann	k1gMnSc1	Friedemann
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
,	,	kIx,	,
Malý	malý	k2eAgMnSc1d1	malý
pan	pan	k1gMnSc1	pan
Friedemann	Friedemann	k1gMnSc1	Friedemann
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
novela	novela	k1gFnSc1	novela
líčící	líčící	k2eAgNnSc4d1	líčící
zhroucení	zhroucení	k1gNnSc4	zhroucení
klidného	klidný	k2eAgInSc2d1	klidný
a	a	k8xC	a
pečlivě	pečlivě	k6eAd1	pečlivě
nalinkovaného	nalinkovaný	k2eAgInSc2d1	nalinkovaný
života	život	k1gInSc2	život
malého	malý	k2eAgMnSc2d1	malý
a	a	k8xC	a
nehezkého	hezký	k2eNgMnSc2d1	nehezký
pana	pan	k1gMnSc2	pan
Friedemanna	Friedemann	k1gMnSc2	Friedemann
kvůli	kvůli	k7c3	kvůli
pro	pro	k7c4	pro
něho	on	k3xPp3gMnSc2	on
nedosažitelné	dosažitelný	k2eNgFnSc3d1	nedosažitelná
ženě	žena	k1gFnSc3	žena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Der	drát	k5eAaImRp2nS	drát
Bajazzo	bajazzo	k1gMnSc5	bajazzo
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
,	,	kIx,	,
Klaun	klaun	k1gMnSc1	klaun
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tobias	Tobias	k1gMnSc1	Tobias
Mindernickel	Mindernickel	k1gMnSc1	Mindernickel
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Der	drát	k5eAaImRp2nS	drát
Kleiderschrank	Kleiderschrank	k1gMnSc1	Kleiderschrank
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
Šatník	šatník	k1gInSc1	šatník
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Der	drát	k5eAaImRp2nS	drát
Weg	Weg	k1gFnSc2	Weg
zum	zum	k?	zum
Friedhof	Friedhof	k1gMnSc1	Friedhof
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
Cesta	cesta	k1gFnSc1	cesta
ke	k	k7c3	k
hřitovu	hřitův	k2eAgMnSc3d1	hřitův
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Gladius	Gladius	k1gMnSc1	Gladius
Dei	Dei	k1gMnSc1	Dei
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
titul	titul	k1gInSc4	titul
novely	novela	k1gFnSc2	novela
znamená	znamenat	k5eAaImIp3nS	znamenat
latinsky	latinsky	k6eAd1	latinsky
Meč	meč	k1gInSc1	meč
boží	boží	k2eAgInSc1d1	boží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tristan	Tristan	k1gInSc1	Tristan
und	und	k?	und
andere	andrat	k5eAaPmIp3nS	andrat
Novellen	Novellen	k1gInSc1	Novellen
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
,	,	kIx,	,
Tristan	Tristan	k1gInSc4	Tristan
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
novely	novela	k1gFnPc4	novela
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
impresivních	impresivní	k2eAgFnPc2d1	impresivní
psychologických	psychologický	k2eAgFnPc2d1	psychologická
novel	novela	k1gFnPc2	novela
zabývajících	zabývající	k2eAgFnPc2d1	zabývající
se	s	k7c7	s
protikladem	protiklad	k1gInSc7	protiklad
mezi	mezi	k7c7	mezi
obyčejným	obyčejný	k2eAgInSc7d1	obyčejný
životem	život	k1gInSc7	život
a	a	k8xC	a
trýzní	trýzeň	k1gFnSc7	trýzeň
umělecké	umělecký	k2eAgFnSc2d1	umělecká
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
nejvýznamnější	významný	k2eAgMnPc4d3	nejvýznamnější
jsou	být	k5eAaImIp3nP	být
Tristan	Tristan	k1gInSc1	Tristan
a	a	k8xC	a
Tonio	Tonio	k1gMnSc1	Tonio
Kröger	Kröger	k1gMnSc1	Kröger
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ein	Ein	k?	Ein
Glück	Glück	k1gInSc1	Glück
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
Štěstí	štěstí	k1gNnSc1	štěstí
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Beim	Beim	k6eAd1	Beim
Propheten	Propheten	k2eAgMnSc1d1	Propheten
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
U	u	k7c2	u
proroka	prorok	k1gMnSc2	prorok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
líčící	líčící	k2eAgFnSc7d1	líčící
bezcharakterního	bezcharakterní	k2eAgMnSc4d1	bezcharakterní
intelektuálního	intelektuální	k2eAgMnSc4d1	intelektuální
sektáře	sektář	k1gMnSc4	sektář
parazitujícího	parazitující	k2eAgMnSc4d1	parazitující
na	na	k7c6	na
životě	život	k1gInSc6	život
jiných	jiný	k1gMnPc2	jiný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Schwere	Schwer	k1gMnSc5	Schwer
Stunde	Stund	k1gMnSc5	Stund
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
Těžká	těžký	k2eAgFnSc1d1	těžká
hodina	hodina	k1gFnSc1	hodina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgFnPc4	který
autor	autor	k1gMnSc1	autor
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
portrét	portrét	k1gInSc1	portrét
německého	německý	k2eAgMnSc2d1	německý
básníka	básník	k1gMnSc2	básník
a	a	k8xC	a
dramatika	dramatik	k1gMnSc2	dramatik
Friedricha	Friedrich	k1gMnSc2	Friedrich
Schillera	Schiller	k1gMnSc2	Schiller
jako	jako	k8xC	jako
umělce	umělec	k1gMnSc2	umělec
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
síla	síla	k1gFnSc1	síla
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
poznání	poznání	k1gNnSc6	poznání
a	a	k8xC	a
pro	pro	k7c4	pro
nějž	jenž	k3xRgInSc4	jenž
je	být	k5eAaImIp3nS	být
tvorba	tvorba	k1gFnSc1	tvorba
zápasem	zápas	k1gInSc7	zápas
se	s	k7c7	s
světem	svět	k1gInSc7	svět
jevů	jev	k1gInPc2	jev
a	a	k8xC	a
bolestným	bolestný	k2eAgNnSc7d1	bolestné
úsilím	úsilí	k1gNnSc7	úsilí
o	o	k7c4	o
vyjádření	vyjádření	k1gNnSc4	vyjádření
myšlenky	myšlenka	k1gFnSc2	myšlenka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Anekdote	Anekdot	k1gMnSc5	Anekdot
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
Anekdota	anekdota	k1gFnSc1	anekdota
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Das	Das	k?	Das
Eisenbahnunglück	Eisenbahnunglück	k1gInSc1	Eisenbahnunglück
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
Železniční	železniční	k2eAgFnSc1d1	železniční
nehoda	nehoda	k1gFnSc1	nehoda
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Wie	Wie	k?	Wie
Jappe	Japp	k1gMnSc5	Japp
und	und	k?	und
Do	do	k7c2	do
Escobar	Escobara	k1gFnPc2	Escobara
sich	sich	k1gMnSc1	sich
prügelten	prügelten	k2eAgMnSc1d1	prügelten
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
poprali	poprat	k5eAaPmAgMnP	poprat
Jappe	Japp	k1gInSc5	Japp
a	a	k8xC	a
Do	do	k7c2	do
Escobar	Escobara	k1gFnPc2	Escobara
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Der	drát	k5eAaImRp2nS	drát
Tod	Tod	k1gFnSc2	Tod
in	in	k?	in
Venedig	Venedig	k1gMnSc1	Venedig
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
Smrt	smrt	k1gFnSc1	smrt
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slavná	slavný	k2eAgFnSc1d1	slavná
novela	novela	k1gFnSc1	novela
s	s	k7c7	s
tématem	téma	k1gNnSc7	téma
vztahu	vztah	k1gInSc2	vztah
umělce	umělec	k1gMnSc2	umělec
k	k	k7c3	k
světu	svět	k1gInSc3	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Das	Das	k?	Das
Wunderkind	Wunderkind	k1gInSc1	Wunderkind
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
Zázračné	zázračný	k2eAgNnSc1d1	zázračné
dítě	dítě	k1gNnSc1	dítě
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
novela	novela	k1gFnSc1	novela
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	se	k3xPyFc4	se
uměleckým	umělecký	k2eAgNnSc7d1	umělecké
šejdířstvím	šejdířství	k1gNnSc7	šejdířství
a	a	k8xC	a
šarlatánstvím	šarlatánství	k1gNnSc7	šarlatánství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Herr	Herr	k1gMnSc1	Herr
und	und	k?	und
Hund	Hund	k1gMnSc1	Hund
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
Pán	pán	k1gMnSc1	pán
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
pes	pes	k1gMnSc1	pes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
idylická	idylický	k2eAgFnSc1d1	idylická
novela	novela	k1gFnSc1	novela
líčící	líčící	k2eAgInPc1d1	líčící
zážitky	zážitek	k1gInPc4	zážitek
ze	z	k7c2	z
společného	společný	k2eAgInSc2d1	společný
života	život	k1gInSc2	život
se	s	k7c7	s
psem	pes	k1gMnSc7	pes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gesang	Gesang	k1gInSc1	Gesang
vom	vom	k?	vom
Kindchen	Kindchen	k1gInSc1	Kindchen
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
Dětský	dětský	k2eAgInSc1d1	dětský
zpěv	zpěv	k1gInSc1	zpěv
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Wälsungenblut	Wälsungenblut	k1gInSc1	Wälsungenblut
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
Krev	krev	k1gFnSc1	krev
Wälsungů	Wälsung	k1gInPc2	Wälsung
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
novela	novela	k1gFnSc1	novela
o	o	k7c6	o
incestním	incestní	k2eAgInSc6d1	incestní
vztahu	vztah	k1gInSc6	vztah
dvojčat	dvojče	k1gNnPc2	dvojče
z	z	k7c2	z
bohaté	bohatý	k2eAgFnSc2d1	bohatá
židovské	židovský	k2eAgFnSc2d1	židovská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Unordnung	Unordnung	k1gMnSc1	Unordnung
und	und	k?	und
frühes	frühes	k1gMnSc1	frühes
Leid	Leid	k1gMnSc1	Leid
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
Zmatek	zmatek	k1gInSc1	zmatek
a	a	k8xC	a
raný	raný	k2eAgInSc1d1	raný
žal	žal	k1gInSc1	žal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
melancholická	melancholický	k2eAgFnSc1d1	melancholická
novelaodehrávající	novelaodehrávající	k2eAgFnSc1d1	novelaodehrávající
se	se	k3xPyFc4	se
v	v	k7c6	v
době	doba	k1gFnSc6	doba
inflačního	inflační	k2eAgInSc2d1	inflační
chaosu	chaos	k1gInSc2	chaos
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
líčící	líčící	k2eAgInSc4d1	líčící
první	první	k4xOgInSc4	první
citový	citový	k2eAgInSc4d1	citový
otřes	otřes	k1gInSc4	otřes
mladičké	mladičký	k2eAgFnSc2d1	mladičká
dívenky	dívenka	k1gFnSc2	dívenka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mario	Mario	k1gMnSc1	Mario
und	und	k?	und
der	drát	k5eAaImRp2nS	drát
Zauberer	Zauberer	k1gMnSc1	Zauberer
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
Mario	Mario	k1gMnSc1	Mario
a	a	k8xC	a
kouzelník	kouzelník	k1gMnSc1	kouzelník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
novela	novela	k1gFnSc1	novela
varující	varující	k2eAgFnSc1d1	varující
před	před	k7c7	před
fašismem	fašismus	k1gInSc7	fašismus
zobrazením	zobrazení	k1gNnSc7	zobrazení
davové	davový	k2eAgFnSc2d1	davová
psychózy	psychóza	k1gFnSc2	psychóza
vyvolané	vyvolaný	k2eAgFnSc2d1	vyvolaná
demagogickým	demagogický	k2eAgMnSc7d1	demagogický
kejklířem	kejklíř	k1gMnSc7	kejklíř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Die	Die	k?	Die
vertauschten	vertauschten	k2eAgMnSc1d1	vertauschten
Köpfe	Köpf	k1gInSc5	Köpf
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
Vyměněné	vyměněný	k2eAgFnSc2d1	vyměněná
hlavy	hlava	k1gFnSc2	hlava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
humorný	humorný	k2eAgMnSc1d1	humorný
i	i	k9	i
krvavě	krvavě	k6eAd1	krvavě
drastický	drastický	k2eAgInSc4d1	drastický
indický	indický	k2eAgInSc4d1	indický
pohádkový	pohádkový	k2eAgInSc4d1	pohádkový
příběh	příběh	k1gInSc4	příběh
o	o	k7c6	o
neukojitelnosti	neukojitelnost	k1gFnSc6	neukojitelnost
lidské	lidský	k2eAgFnSc2d1	lidská
touhy	touha	k1gFnSc2	touha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Das	Das	k?	Das
Gesetz	Gesetz	k1gInSc1	Gesetz
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
Zákon	zákon	k1gInSc1	zákon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
novela	novela	k1gFnSc1	novela
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
hrdinou	hrdina	k1gMnSc7	hrdina
je	být	k5eAaImIp3nS	být
biblický	biblický	k2eAgMnSc1d1	biblický
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
dílem	dílo	k1gNnSc7	dílo
přispěl	přispět	k5eAaPmAgMnS	přispět
Thomas	Thomas	k1gMnSc1	Thomas
Mann	Mann	k1gMnSc1	Mann
do	do	k7c2	do
protifašistického	protifašistický	k2eAgInSc2d1	protifašistický
sborníku	sborník	k1gInSc2	sborník
deseti	deset	k4xCc2	deset
světových	světový	k2eAgMnPc2d1	světový
spisovatelů	spisovatel	k1gMnPc2	spisovatel
Desatero	desatero	k1gNnSc4	desatero
přikázání	přikázání	k1gNnPc2	přikázání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Die	Die	k?	Die
Betrogene	Betrogen	k1gInSc5	Betrogen
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
Podvedená	podvedený	k2eAgFnSc1d1	podvedená
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
novela	novela	k1gFnSc1	novela
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
starší	starý	k2eAgFnSc1d2	starší
hrdinka	hrdinka	k1gFnSc1	hrdinka
považuje	považovat	k5eAaImIp3nS	považovat
své	svůj	k3xOyFgNnSc4	svůj
obnovené	obnovený	k2eAgNnSc4d1	obnovené
krvácení	krvácení	k1gNnSc4	krvácení
za	za	k7c4	za
zázračné	zázračný	k2eAgNnSc4d1	zázračné
navrácení	navrácení	k1gNnSc4	navrácení
se	se	k3xPyFc4	se
k	k	k7c3	k
přirozeným	přirozený	k2eAgFnPc3d1	přirozená
ženským	ženský	k2eAgFnPc3d1	ženská
funkcím	funkce	k1gFnPc3	funkce
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
milostné	milostný	k2eAgFnSc3d1	milostná
vášni	vášeň	k1gFnSc3	vášeň
k	k	k7c3	k
mladému	mladé	k1gNnSc3	mladé
muži	muž	k1gMnPc1	muž
(	(	kIx(	(
<g/>
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
však	však	k9	však
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
symptom	symptom	k1gInSc4	symptom
zhoubné	zhoubný	k2eAgFnSc2d1	zhoubná
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
===	===	k?	===
</s>
</p>
<p>
<s>
Fiorenza	Fiorenza	k1gFnSc1	Fiorenza
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Romány	román	k1gInPc4	román
===	===	k?	===
</s>
</p>
<p>
<s>
Buddenbrooks	Buddenbrooks	k1gInSc1	Buddenbrooks
<g/>
:	:	kIx,	:
Verfall	Verfall	k1gInSc1	Verfall
einer	einer	k1gInSc1	einer
Familie	Familie	k1gFnSc1	Familie
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
Buddenbrookovi	Buddenbrookův	k2eAgMnPc1d1	Buddenbrookův
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
o	o	k7c6	o
postupném	postupný	k2eAgInSc6d1	postupný
úpadku	úpadek	k1gInSc6	úpadek
čtyř	čtyři	k4xCgFnPc2	čtyři
generací	generace	k1gFnPc2	generace
měšťanské	měšťanský	k2eAgFnSc2d1	měšťanská
rodiny	rodina	k1gFnSc2	rodina
z	z	k7c2	z
Lübecku	Lübeck	k1gInSc2	Lübeck
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
založil	založit	k5eAaPmAgInS	založit
Mannovu	Mannův	k2eAgFnSc4d1	Mannova
světovou	světový	k2eAgFnSc4d1	světová
proslulost	proslulost	k1gFnSc4	proslulost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Königliche	Königliche	k1gFnSc1	Königliche
Hoheit	Hoheita	k1gFnPc2	Hoheita
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
,	,	kIx,	,
Královská	královský	k2eAgFnSc1d1	královská
výsost	výsost	k1gFnSc1	výsost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ironická	ironický	k2eAgFnSc1d1	ironická
a	a	k8xC	a
symbolická	symbolický	k2eAgFnSc1d1	symbolická
"	"	kIx"	"
<g/>
pohádka	pohádka	k1gFnSc1	pohádka
<g/>
"	"	kIx"	"
o	o	k7c4	o
uzdravení	uzdravení	k1gNnSc4	uzdravení
nefungující	fungující	k2eNgFnSc1d1	nefungující
a	a	k8xC	a
beznadějně	beznadějně	k6eAd1	beznadějně
zadlužené	zadlužený	k2eAgFnSc2d1	zadlužená
monarchie	monarchie	k1gFnSc2	monarchie
kapitálem	kapitál	k1gInSc7	kapitál
a	a	k8xC	a
láskou	láska	k1gFnSc7	láska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Der	drát	k5eAaImRp2nS	drát
Zauberberg	Zauberberg	k1gMnSc1	Zauberberg
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
Kouzelný	kouzelný	k2eAgInSc1d1	kouzelný
vrch	vrch	k1gInSc1	vrch
nebo	nebo	k8xC	nebo
Čarovná	čarovný	k2eAgFnSc1d1	čarovná
hora	hora	k1gFnSc1	hora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc4	román
odehrávající	odehrávající	k2eAgInSc4d1	odehrávající
se	se	k3xPyFc4	se
ve	v	k7c6	v
švýcarském	švýcarský	k2eAgNnSc6d1	švýcarské
plicním	plicní	k2eAgNnSc6d1	plicní
sanatoriu	sanatorium	k1gNnSc6	sanatorium
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
v	v	k7c6	v
slovních	slovní	k2eAgInPc6d1	slovní
soubojích	souboj	k1gInPc6	souboj
protagonistů	protagonista	k1gMnPc2	protagonista
uskutečňuje	uskutečňovat	k5eAaImIp3nS	uskutečňovat
světonázorová	světonázorový	k2eAgFnSc1d1	světonázorová
diskuse	diskuse	k1gFnSc1	diskuse
o	o	k7c6	o
zániku	zánik	k1gInSc6	zánik
staré	starý	k2eAgFnSc2d1	stará
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
současně	současně	k6eAd1	současně
probíhá	probíhat	k5eAaImIp3nS	probíhat
i	i	k9	i
morální	morální	k2eAgFnSc1d1	morální
a	a	k8xC	a
duchovní	duchovní	k2eAgFnSc1d1	duchovní
příprava	příprava	k1gFnSc1	příprava
na	na	k7c4	na
lepší	dobrý	k2eAgFnSc4d2	lepší
budoucnost	budoucnost	k1gFnSc4	budoucnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Joseph	Joseph	k1gInSc1	Joseph
und	und	k?	und
seine	seinout	k5eAaImIp3nS	seinout
Brüder	Brüder	k1gInSc1	Brüder
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
-	-	kIx~	-
<g/>
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
a	a	k8xC	a
bratří	bratřit	k5eAaImIp3nS	bratřit
jeho	jeho	k3xOp3gFnSc4	jeho
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
románová	románový	k2eAgFnSc1d1	románová
tetralogie	tetralogie	k1gFnSc1	tetralogie
napsaná	napsaný	k2eAgFnSc1d1	napsaná
na	na	k7c4	na
biblický	biblický	k2eAgInSc4d1	biblický
námět	námět	k1gInSc4	námět
<g/>
.	.	kIx.	.
</s>
<s>
Tetralogie	tetralogie	k1gFnSc1	tetralogie
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
dílů	díl	k1gInPc2	díl
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Die	Die	k?	Die
Geschichten	Geschichten	k2eAgInSc1d1	Geschichten
Jaakobs	Jaakobs	k1gInSc1	Jaakobs
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
Příběhy	příběh	k1gInPc1	příběh
Jákobovy	Jákobův	k2eAgFnSc2d1	Jákobova
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Der	drát	k5eAaImRp2nS	drát
junge	junge	k1gNnSc4	junge
Joseph	Joseph	k1gInSc1	Joseph
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
Mladý	mladý	k2eAgMnSc1d1	mladý
Josef	Josef	k1gMnSc1	Josef
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Joseph	Joseph	k1gMnSc1	Joseph
in	in	k?	in
Ägypten	Ägyptno	k1gNnPc2	Ägyptno
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Joseph	Joseph	k1gMnSc1	Joseph
<g/>
,	,	kIx,	,
der	drát	k5eAaImRp2nS	drát
Ernährer	Ernährer	k1gMnSc1	Ernährer
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Živitel	živitel	k1gMnSc1	živitel
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lotte	Lotte	k5eAaPmIp2nP	Lotte
in	in	k?	in
Weimar	Weimar	k1gInSc1	Weimar
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
Lotta	Lotta	k1gFnSc1	Lotta
ve	v	k7c6	v
Výmaru	Výmar	k1gInSc6	Výmar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgNnSc6	který
Thomas	Thomas	k1gMnSc1	Thomas
Mann	Mann	k1gMnSc1	Mann
postavil	postavit	k5eAaPmAgMnS	postavit
proti	proti	k7c3	proti
nacismu	nacismus	k1gInSc2	nacismus
humanistické	humanistický	k2eAgNnSc4d1	humanistické
klasické	klasický	k2eAgNnSc4d1	klasické
dědictví	dědictví	k1gNnSc4	dědictví
Goethovo	Goethův	k2eAgNnSc4d1	Goethovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Doktor	doktor	k1gMnSc1	doktor
Faustus	Faustus	k1gMnSc1	Faustus
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vrcholný	vrcholný	k2eAgInSc1d1	vrcholný
Mannův	Mannův	k2eAgInSc1d1	Mannův
román	román	k1gInSc1	román
inspirovaný	inspirovaný	k2eAgInSc1d1	inspirovaný
legendou	legenda	k1gFnSc7	legenda
o	o	k7c6	o
Faustovi	Fausta	k1gMnSc6	Fausta
vypovídá	vypovídat	k5eAaPmIp3nS	vypovídat
o	o	k7c6	o
osudu	osud	k1gInSc6	osud
Německa	Německo	k1gNnSc2	Německo
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
o	o	k7c4	o
postavení	postavení	k1gNnSc4	postavení
umělce	umělec	k1gMnSc2	umělec
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Der	drát	k5eAaImRp2nS	drát
Erwählte	Erwählt	k1gInSc5	Erwählt
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
Vyvolený	vyvolený	k1gMnSc1	vyvolený
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnSc4d1	poslední
autorovo	autorův	k2eAgNnSc4d1	autorovo
dokončené	dokončený	k2eAgNnSc4d1	dokončené
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgFnSc1d1	moderní
románová	románový	k2eAgFnSc1d1	románová
parafráze	parafráze	k1gFnSc1	parafráze
středověkého	středověký	k2eAgInSc2d1	středověký
příběhu	příběh	k1gInSc2	příběh
o	o	k7c6	o
životě	život	k1gInSc6	život
papeže	papež	k1gMnSc2	papež
Řehoře	Řehoř	k1gMnSc2	Řehoř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bekenntnisse	Bekenntnisse	k6eAd1	Bekenntnisse
des	des	k1gNnSc1	des
Hochstaplers	Hochstaplers	k1gInSc1	Hochstaplers
Felix	Felix	k1gMnSc1	Felix
Krull	Krull	k1gMnSc1	Krull
<g/>
,	,	kIx,	,
1954	[number]	k4	1954
(	(	kIx(	(
<g/>
Zpověď	zpověď	k1gFnSc1	zpověď
hochštaplera	hochštapler	k1gMnSc2	hochštapler
Felixe	Felix	k1gMnSc2	Felix
Krulla	Krull	k1gMnSc2	Krull
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nedokončený	dokončený	k2eNgInSc1d1	nedokončený
autorův	autorův	k2eAgInSc1d1	autorův
román	román	k1gInSc1	román
o	o	k7c6	o
životě	život	k1gInSc6	život
virtuózního	virtuózní	k2eAgMnSc2d1	virtuózní
podvodníka	podvodník	k1gMnSc2	podvodník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Eseje	esej	k1gInPc4	esej
===	===	k?	===
</s>
</p>
<p>
<s>
Friedrich	Friedrich	k1gMnSc1	Friedrich
und	und	k?	und
die	die	k?	die
große	großat	k5eAaPmIp3nS	großat
Koalition	Koalition	k1gInSc1	Koalition
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
,	,	kIx,	,
Friedrich	Friedrich	k1gMnSc1	Friedrich
a	a	k8xC	a
velká	velký	k2eAgFnSc1d1	velká
koalice	koalice	k1gFnSc1	koalice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Betrachtungen	Betrachtungen	k1gInSc1	Betrachtungen
eines	einesa	k1gFnPc2	einesa
Unpolitischen	Unpolitischna	k1gFnPc2	Unpolitischna
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
Úvahy	úvaha	k1gFnPc1	úvaha
nepolitického	politický	k2eNgMnSc2d1	nepolitický
člověka	člověk	k1gMnSc2	člověk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
eseje	esej	k1gInPc1	esej
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
se	se	k3xPyFc4	se
Thomas	Thomas	k1gMnSc1	Thomas
Mann	Mann	k1gMnSc1	Mann
zastal	zastat	k5eAaPmAgMnS	zastat
války	válka	k1gFnPc4	válka
<g/>
,	,	kIx,	,
prušáctví	prušáctví	k1gNnSc4	prušáctví
a	a	k8xC	a
německého	německý	k2eAgInSc2d1	německý
nacionalismu	nacionalismus	k1gInSc2	nacionalismus
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
roztržce	roztržka	k1gFnSc3	roztržka
s	s	k7c7	s
jeho	jeho	k3xOp3gMnSc7	jeho
bratrem	bratr	k1gMnSc7	bratr
Heinrichem	Heinrich	k1gMnSc7	Heinrich
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rede	Rede	k1gFnSc1	Rede
und	und	k?	und
Antwort	Antwort	k1gInSc1	Antwort
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
Řeč	řeč	k1gFnSc1	řeč
a	a	k8xC	a
odpověď	odpověď	k1gFnSc1	odpověď
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Goethe	Goethe	k6eAd1	Goethe
und	und	k?	und
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
Goethe	Goethe	k1gFnSc1	Goethe
a	a	k8xC	a
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Von	von	k1gInSc1	von
deutscher	deutschra	k1gFnPc2	deutschra
Republik	republika	k1gFnPc2	republika
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
O	o	k7c6	o
německé	německý	k2eAgFnSc6d1	německá
republice	republika	k1gFnSc6	republika
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Deutsche	Deutsche	k1gFnSc1	Deutsche
Ansprache	Ansprache	k1gFnSc1	Ansprache
<g/>
:	:	kIx,	:
Ein	Ein	k1gMnSc1	Ein
Apell	Apell	k1gMnSc1	Apell
an	an	k?	an
die	die	k?	die
Vernunft	Vernunft	k1gInSc1	Vernunft
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
Německý	německý	k2eAgInSc1d1	německý
projev	projev	k1gInSc1	projev
<g/>
:	:	kIx,	:
Apel	apel	k1gInSc1	apel
na	na	k7c4	na
rozum	rozum	k1gInSc4	rozum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odmítnutí	odmítnutí	k1gNnSc1	odmítnutí
fanatického	fanatický	k2eAgInSc2d1	fanatický
fašismu	fašismus	k1gInSc2	fašismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Die	Die	k?	Die
Forderung	Forderung	k1gMnSc1	Forderung
des	des	k1gNnSc1	des
Tages	Tages	k1gMnSc1	Tages
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
Požadavek	požadavek	k1gInSc1	požadavek
dne	den	k1gInSc2	den
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Leiden	Leidno	k1gNnPc2	Leidno
und	und	k?	und
Größe	Größ	k1gFnSc2	Größ
der	drát	k5eAaImRp2nS	drát
Meister	Meister	k1gMnSc1	Meister
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
Utrpení	utrpení	k1gNnSc1	utrpení
a	a	k8xC	a
velikost	velikost	k1gFnSc1	velikost
mistrů	mistr	k1gMnPc2	mistr
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Freud	Freud	k1gInSc1	Freud
und	und	k?	und
die	die	k?	die
Zukunft	Zukunft	k1gInSc1	Zukunft
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
Freud	Freud	k1gInSc1	Freud
a	a	k8xC	a
budoucnost	budoucnost	k1gFnSc1	budoucnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Freud	Freud	k1gInSc1	Freud
<g/>
,	,	kIx,	,
Goethe	Goethe	k1gInSc1	Goethe
<g/>
,	,	kIx,	,
Wagner	Wagner	k1gMnSc1	Wagner
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dieser	Diesrat	k5eAaPmRp2nS	Diesrat
Friede	Fried	k1gMnSc5	Fried
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
Tento	tento	k3xDgInSc1	tento
mír	mír	k1gInSc1	mír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odsouzení	odsouzení	k1gNnSc4	odsouzení
Mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
</s>
</p>
<p>
<s>
Schopenhauer	Schopenhauer	k1gInSc1	Schopenhauer
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Achtung	Achtung	k1gInSc1	Achtung
<g/>
,	,	kIx,	,
Europa	Europa	k1gFnSc1	Europa
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
Evropo	Evropa	k1gFnSc5	Evropa
<g/>
,	,	kIx,	,
pozor	pozor	k1gInSc4	pozor
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Deutschland	Deutschland	k1gInSc1	Deutschland
und	und	k?	und
die	die	k?	die
Deutschen	Deutschen	k1gInSc1	Deutschen
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
a	a	k8xC	a
Němci	Němec	k1gMnPc1	Němec
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nietzsches	Nietzsches	k1gMnSc1	Nietzsches
Philosophie	Philosophie	k1gFnSc2	Philosophie
im	im	k?	im
Lichte	Licht	k1gInSc5	Licht
unserer	unserrat	k5eAaPmRp2nS	unserrat
Erfahrung	Erfahrung	k1gMnSc1	Erfahrung
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
Nietzscheho	Nietzsche	k1gMnSc2	Nietzsche
filozofie	filozofie	k1gFnSc2	filozofie
ve	v	k7c6	v
světle	světlo	k1gNnSc6	světlo
naší	náš	k3xOp1gFnSc2	náš
zkušenosti	zkušenost	k1gFnSc2	zkušenost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Esej	esej	k1gFnSc1	esej
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
Mann	Mann	k1gMnSc1	Mann
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
rehabilitovat	rehabilitovat	k5eAaBmF	rehabilitovat
Nietzscheho	Nietzsche	k1gMnSc4	Nietzsche
filozofii	filozofie	k1gFnSc4	filozofie
po	po	k7c6	po
zkušenosti	zkušenost	k1gFnSc6	zkušenost
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Nietzsche	Nietzsche	k1gFnSc1	Nietzsche
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
vykreslen	vykreslen	k2eAgInSc1d1	vykreslen
jako	jako	k8xS	jako
bytostný	bytostný	k2eAgMnSc1d1	bytostný
ironik	ironik	k1gMnSc1	ironik
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
provokativní	provokativní	k2eAgNnSc4d1	provokativní
dílo	dílo	k1gNnSc4	dílo
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
chápat	chápat	k5eAaImF	chápat
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
19	[number]	k4	19
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
hrůzami	hrůza	k1gFnPc7	hrůza
tohoto	tento	k3xDgNnSc2	tento
století	století	k1gNnSc2	století
Nietzscheho	Nietzsche	k1gMnSc2	Nietzsche
dílo	dílo	k1gNnSc1	dílo
podle	podle	k7c2	podle
Manna	Mann	k1gMnSc2	Mann
varuje	varovat	k5eAaImIp3nS	varovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
nelze	lze	k6eNd1	lze
spatřovat	spatřovat	k5eAaImF	spatřovat
jejich	jejich	k3xOp3gNnSc4	jejich
myšlenkové	myšlenkový	k2eAgNnSc4d1	myšlenkové
pozadí	pozadí	k1gNnSc4	pozadí
<g/>
,	,	kIx,	,
natož	natož	k6eAd1	natož
ho	on	k3xPp3gMnSc4	on
zneužívat	zneužívat	k5eAaImF	zneužívat
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
ospravedlnění	ospravedlnění	k1gNnSc3	ospravedlnění
<g/>
.	.	kIx.	.
</s>
<s>
Biografický	biografický	k2eAgInSc4d1	biografický
ráz	ráz	k1gInSc4	ráz
eseje	esej	k1gFnSc2	esej
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
duchovní	duchovní	k2eAgFnSc4d1	duchovní
spřízněnost	spřízněnost	k1gFnSc4	spřízněnost
Nietzscheho	Nietzsche	k1gMnSc2	Nietzsche
nemoci	nemoc	k1gFnSc2	nemoc
a	a	k8xC	a
geniality	genialita	k1gFnSc2	genialita
činí	činit	k5eAaImIp3nS	činit
z	z	k7c2	z
textu	text	k1gInSc2	text
důležitý	důležitý	k2eAgInSc4d1	důležitý
pandán	pandán	k1gInSc4	pandán
k	k	k7c3	k
románu	román	k1gInSc3	román
Doktor	doktor	k1gMnSc1	doktor
Faustus	Faustus	k1gMnSc1	Faustus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Die	Die	k?	Die
Entstehung	Entstehung	k1gMnSc1	Entstehung
des	des	k1gNnSc2	des
Doktor	doktor	k1gMnSc1	doktor
Faustus	Faustus	k1gMnSc1	Faustus
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
Jak	jak	k6eAd1	jak
jsem	být	k5eAaImIp1nS	být
psal	psát	k5eAaImAgMnS	psát
Doktora	doktor	k1gMnSc4	doktor
Fausta	Faust	k1gMnSc4	Faust
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výjimečná	výjimečný	k2eAgFnSc1d1	výjimečná
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgFnPc4	který
romanopisec	romanopisec	k1gMnSc1	romanopisec
provází	provázet	k5eAaImIp3nS	provázet
čtenáře	čtenář	k1gMnPc4	čtenář
svou	svůj	k3xOyFgFnSc7	svůj
duševní	duševní	k2eAgFnSc7d1	duševní
dílnou	dílna	k1gFnSc7	dílna
a	a	k8xC	a
seznamuje	seznamovat	k5eAaImIp3nS	seznamovat
ho	on	k3xPp3gMnSc4	on
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
zajímavých	zajímavý	k2eAgFnPc2d1	zajímavá
podrobností	podrobnost	k1gFnPc2	podrobnost
vzniku	vznik	k1gInSc2	vznik
stěžejního	stěžejní	k2eAgNnSc2d1	stěžejní
díla	dílo	k1gNnSc2	dílo
svého	svůj	k3xOyFgNnSc2	svůj
stáří	stáří	k1gNnSc2	stáří
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
skutečností	skutečnost	k1gFnPc2	skutečnost
svého	svůj	k3xOyFgInSc2	svůj
osobního	osobní	k2eAgInSc2d1	osobní
života	život	k1gInSc2	život
a	a	k8xC	a
osobnostmi	osobnost	k1gFnPc7	osobnost
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
se	se	k3xPyFc4	se
stýkal	stýkat	k5eAaImAgMnS	stýkat
<g/>
,	,	kIx,	,
i	i	k8xC	i
se	s	k7c7	s
soudobým	soudobý	k2eAgNnSc7d1	soudobé
děním	dění	k1gNnSc7	dění
světovým	světový	k2eAgNnSc7d1	světové
<g/>
,	,	kIx,	,
politickým	politický	k2eAgNnSc7d1	politické
i	i	k8xC	i
kulturním	kulturní	k2eAgNnSc7d1	kulturní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Meine	Meinout	k5eAaImIp3nS	Meinout
Zeit	Zeit	k1gInSc1	Zeit
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
Moje	můj	k3xOp1gFnSc1	můj
doba	doba	k1gFnSc1	doba
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Versuch	Versuch	k1gMnSc1	Versuch
über	über	k1gMnSc1	über
Tschechow	Tschechow	k1gMnSc1	Tschechow
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
O	o	k7c6	o
Čechovovi	Čechov	k1gMnSc6	Čechov
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Česká	český	k2eAgNnPc1d1	české
vydání	vydání	k1gNnPc1	vydání
===	===	k?	===
</s>
</p>
<p>
<s>
Královská	královský	k2eAgFnSc1d1	královská
výsost	výsost	k1gFnSc1	výsost
<g/>
,	,	kIx,	,
A.	A.	kA	A.
Hajn	Hajn	k1gMnSc1	Hajn
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
J.	J.	kA	J.
Hanousek	Hanousek	k1gMnSc1	Hanousek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Malý	Malý	k1gMnSc1	Malý
pan	pan	k1gMnSc1	pan
Friedemann	Friedemann	k1gMnSc1	Friedemann
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
Vobrubová	Vobrubová	k1gFnSc1	Vobrubová
a	a	k8xC	a
Antonín	Antonín	k1gMnSc1	Antonín
Veselý	Veselý	k1gMnSc1	Veselý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Novelly	Novella	k1gFnPc1	Novella
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Zdeňka	Zdeněk	k1gMnSc4	Zdeněk
Hostinská	hostinská	k1gFnSc1	hostinská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Budenbrookové	Budenbrookový	k2eAgInPc4d1	Budenbrookový
<g/>
,	,	kIx,	,
Národní	národní	k2eAgInPc4d1	národní
listy	list	k1gInPc4	list
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1913	[number]	k4	1913
<g/>
-	-	kIx~	-
<g/>
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
B.	B.	kA	B.
Prusík	Prusík	k1gMnSc1	Prusík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Smrt	smrt	k1gFnSc1	smrt
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
<g/>
,	,	kIx,	,
Kvasnička	Kvasnička	k1gMnSc1	Kvasnička
a	a	k8xC	a
Hampl	Hampl	k1gMnSc1	Hampl
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Rudolf	Rudolf	k1gMnSc1	Rudolf
Pazderník	pazderník	k1gMnSc1	pazderník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tonio	Tonio	k1gNnSc1	Tonio
Kröger	Krögra	k1gFnPc2	Krögra
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
novely	novela	k1gFnSc2	novela
<g/>
,	,	kIx,	,
B.	B.	kA	B.
Krejčí	Krejčí	k1gMnSc1	Krejčí
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Zdenka	Zdenka	k1gFnSc1	Zdenka
Hostinská	hostinská	k1gFnSc1	hostinská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Malý	Malý	k1gMnSc1	Malý
pan	pan	k1gMnSc1	pan
Friedemann	Friedemann	k1gMnSc1	Friedemann
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
Synek	Synek	k1gMnSc1	Synek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Růžena	Růžena	k1gFnSc1	Růžena
Thonová	Thonová	k1gFnSc1	Thonová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Buddenbrookovi	Buddenbrook	k1gMnSc3	Buddenbrook
<g/>
,	,	kIx,	,
Melantrich	Melantrich	k1gInSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Skalický	Skalický	k1gMnSc1	Skalický
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1932	[number]	k4	1932
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kouzelný	kouzelný	k2eAgInSc1d1	kouzelný
vrch	vrch	k1gInSc1	vrch
<g/>
,	,	kIx,	,
Melantrich	Melantrich	k1gInSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Jitka	Jitka	k1gFnSc1	Jitka
Fučíková	Fučíková	k1gFnSc1	Fučíková
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Levit	levit	k1gMnSc1	levit
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Zahradníček	Zahradníček	k1gMnSc1	Zahradníček
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1935	[number]	k4	1935
<g/>
-	-	kIx~	-
<g/>
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
1958	[number]	k4	1958
a	a	k8xC	a
Odeon	odeon	k1gInSc1	odeon
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Královská	královský	k2eAgFnSc1d1	královská
výsost	výsost	k1gFnSc1	výsost
<g/>
,	,	kIx,	,
Melantrich	Melantrich	k1gInSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Bedřich	Bedřich	k1gMnSc1	Bedřich
Vaníček	Vaníček	k1gMnSc1	Vaníček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mario	Mario	k1gMnSc1	Mario
a	a	k8xC	a
kouzelník	kouzelník	k1gMnSc1	kouzelník
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
novely	novela	k1gFnPc4	novela
<g/>
,	,	kIx,	,
Melantrich	Melantrich	k1gInSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Jitka	Jitka	k1gFnSc1	Jitka
Fučíková	Fučíková	k1gFnSc1	Fučíková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Povídky	povídka	k1gFnPc1	povídka
o	o	k7c6	o
životě	život	k1gInSc6	život
a	a	k8xC	a
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
Melantrich	Melantrich	k1gInSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jan	Jan	k1gMnSc1	Jan
Zahradníček	Zahradníček	k1gMnSc1	Zahradníček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
a	a	k8xC	a
bratří	bratřit	k5eAaImIp3nS	bratřit
jeho	jeho	k3xOp3gInSc4	jeho
I.	I.	kA	I.
-	-	kIx~	-
Příběhy	příběh	k1gInPc1	příběh
Jákobovy	Jákobův	k2eAgInPc1d1	Jákobův
<g/>
,	,	kIx,	,
Melantrich	Melantrich	k1gInSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Ivan	Ivan	k1gMnSc1	Ivan
Olbracht	Olbracht	k1gMnSc1	Olbracht
a	a	k8xC	a
Helena	Helena	k1gFnSc1	Helena
Malířová	Malířová	k1gFnSc1	Malířová
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
a	a	k8xC	a
bratří	bratřit	k5eAaImIp3nS	bratřit
jeho	on	k3xPp3gInSc4	on
II	II	kA	II
<g/>
.	.	kIx.	.
-	-	kIx~	-
Mladý	mladý	k2eAgMnSc1d1	mladý
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Ivan	Ivan	k1gMnSc1	Ivan
Olbracht	Olbracht	k1gMnSc1	Olbracht
a	a	k8xC	a
Helena	Helena	k1gFnSc1	Helena
Malířová	Malířová	k1gFnSc1	Malířová
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Utrpení	utrpení	k1gNnSc1	utrpení
a	a	k8xC	a
velikost	velikost	k1gFnSc1	velikost
mistrů	mistr	k1gMnPc2	mistr
<g/>
,	,	kIx,	,
Melantrich	Melantricha	k1gFnPc2	Melantricha
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Pavel	Pavel	k1gMnSc1	Pavel
Eisner	Eisner	k1gMnSc1	Eisner
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
a	a	k8xC	a
bratří	bratřit	k5eAaImIp3nS	bratřit
jeho	jeho	k3xOp3gInSc4	jeho
III	III	kA	III
<g/>
.	.	kIx.	.
-	-	kIx~	-
Josef	Josef	k1gMnSc1	Josef
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
Melantrich	Melantrich	k1gInSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Ivan	Ivan	k1gMnSc1	Ivan
Olbracht	Olbracht	k1gMnSc1	Olbracht
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Freud	Freud	k1gInSc1	Freud
a	a	k8xC	a
budoucnost	budoucnost	k1gFnSc1	budoucnost
<g/>
,	,	kIx,	,
Julius	Julius	k1gMnSc1	Julius
Albert	Albert	k1gMnSc1	Albert
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Pavel	Pavel	k1gMnSc1	Pavel
Eisner	Eisner	k1gMnSc1	Eisner
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Desatero	desatero	k1gNnSc1	desatero
přikázání	přikázání	k1gNnPc2	přikázání
<g/>
,	,	kIx,	,
Družstevní	družstevní	k2eAgFnSc1d1	družstevní
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Pavel	Pavel	k1gMnSc1	Pavel
Eisner	Eisner	k1gMnSc1	Eisner
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sborník	sborník	k1gInSc1	sborník
protifašistických	protifašistický	k2eAgFnPc2d1	protifašistická
novel	novela	k1gFnPc2	novela
od	od	k7c2	od
deseti	deset	k4xCc2	deset
světových	světový	k2eAgMnPc2d1	světový
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
Mannova	Mannův	k2eAgFnSc1d1	Mannova
Zákon	zákon	k1gInSc1	zákon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Schopenhauer	Schopenhauer	k1gMnSc1	Schopenhauer
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Borový	borový	k2eAgMnSc1d1	borový
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jan	Jan	k1gMnSc1	Jan
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Votobia	Votobia	k1gFnSc1	Votobia
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Doktor	doktor	k1gMnSc1	doktor
Faustus	Faustus	k1gMnSc1	Faustus
<g/>
,	,	kIx,	,
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Pavel	Pavel	k1gMnSc1	Pavel
Eisner	Eisner	k1gMnSc1	Eisner
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
SNKLU	SNKLU	kA	SNKLU
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lota	Lota	k1gFnSc1	Lota
ve	v	k7c6	v
Výmaru	Výmar	k1gInSc6	Výmar
<g/>
,	,	kIx,	,
Melantrich	Melantrich	k1gInSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Pavel	Pavel	k1gMnSc1	Pavel
Eisner	Eisner	k1gMnSc1	Eisner
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Buddenbrookovi	Buddenbrook	k1gMnSc3	Buddenbrook
<g/>
,	,	kIx,	,
Melantrich	Melantrich	k1gInSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Pavel	Pavel	k1gMnSc1	Pavel
Eisner	Eisner	k1gMnSc1	Eisner
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
1955	[number]	k4	1955
a	a	k8xC	a
1956	[number]	k4	1956
a	a	k8xC	a
Odeon	odeon	k1gInSc1	odeon
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
a	a	k8xC	a
bratří	bratr	k1gMnPc1	bratr
jeho	jeho	k3xOp3gFnPc2	jeho
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
-	-	kIx~	-
Josef	Josef	k1gMnSc1	Josef
Živitel	živitel	k1gMnSc1	živitel
<g/>
,	,	kIx,	,
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Pavel	Pavel	k1gMnSc1	Pavel
Eisner	Eisner	k1gMnSc1	Eisner
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tonio	Tonio	k1gMnSc1	Tonio
Kröger	Kröger	k1gMnSc1	Kröger
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Jitka	Jitka	k1gFnSc1	Jitka
Fučíková	Fučíková	k1gFnSc1	Fučíková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
a	a	k8xC	a
bratří	bratřit	k5eAaImIp3nS	bratřit
jeho	jeho	k3xOp3gInSc4	jeho
I.	I.	kA	I.
-	-	kIx~	-
Příběhy	příběh	k1gInPc1	příběh
Jákobovy	Jákobův	k2eAgFnSc2d1	Jákobova
a	a	k8xC	a
Mladý	mladý	k2eAgMnSc1d1	mladý
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Ivan	Ivan	k1gMnSc1	Ivan
Olbracht	Olbracht	k1gMnSc1	Olbracht
a	a	k8xC	a
Helena	Helena	k1gFnSc1	Helena
Malířová	Malířová	k1gFnSc1	Malířová
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Bonus	bonus	k1gInSc1	bonus
A	A	kA	A
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
a	a	k8xC	a
bratří	bratřit	k5eAaImIp3nS	bratřit
jeho	on	k3xPp3gInSc4	on
II	II	kA	II
<g/>
.	.	kIx.	.
-	-	kIx~	-
Josef	Josef	k1gMnSc1	Josef
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Ivan	Ivan	k1gMnSc1	Ivan
Olbracht	Olbracht	k1gMnSc1	Olbracht
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
a	a	k8xC	a
bratří	bratřit	k5eAaImIp3nS	bratřit
jeho	on	k3xPp3gInSc4	on
III	III	kA	III
<g/>
.	.	kIx.	.
-	-	kIx~	-
Josef	Josef	k1gMnSc1	Josef
Živitel	živitel	k1gMnSc1	živitel
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Pavel	Pavel	k1gMnSc1	Pavel
Eisner	Eisner	k1gMnSc1	Eisner
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
Čechovovi	Čechov	k1gMnSc6	Čechov
<g/>
,	,	kIx,	,
Československý	československý	k2eAgMnSc1d1	československý
spisovatel	spisovatel	k1gMnSc1	spisovatel
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Milan	Milan	k1gMnSc1	Milan
Messiereur	Messiereur	k1gMnSc1	Messiereur
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zpověď	zpověď	k1gFnSc1	zpověď
hochštaplera	hochštapler	k1gMnSc2	hochštapler
Felixe	Felix	k1gMnSc2	Felix
Krulla	Krull	k1gMnSc2	Krull
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Pavel	Pavel	k1gMnSc1	Pavel
Eisner	Eisner	k1gMnSc1	Eisner
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
SNKLU	SNKLU	kA	SNKLU
1964	[number]	k4	1964
a	a	k8xC	a
Odeon	odeon	k1gInSc1	odeon
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fiorenza	Fiorenza	k1gFnSc1	Fiorenza
<g/>
,	,	kIx,	,
Dillia	Dillia	k1gFnSc1	Dillia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Ludmila	Ludmila	k1gFnSc1	Ludmila
Kopečná	Kopečná	k1gFnSc1	Kopečná
a	a	k8xC	a
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Dufek	Dufek	k1gMnSc1	Dufek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Novely	novela	k1gFnPc1	novela
a	a	k8xC	a
povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
Naše	náš	k3xOp1gNnSc1	náš
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Pavel	Pavel	k1gMnSc1	Pavel
Eisner	Eisner	k1gMnSc1	Eisner
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc1	dva
svazky	svazek	k1gInPc1	svazek
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnPc1d1	obsahující
třináct	třináct	k4xCc4	třináct
autorových	autorův	k2eAgFnPc2d1	autorova
próz	próza	k1gFnPc2	próza
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
i	i	k8xC	i
původní	původní	k2eAgInSc4d1	původní
fragment	fragment	k1gInSc4	fragment
románu	román	k1gInSc2	román
Zpověď	zpověď	k1gFnSc1	zpověď
hochštaplera	hochštapler	k1gMnSc2	hochštapler
Felixe	Felix	k1gMnSc2	Felix
Krulla	Krull	k1gMnSc2	Krull
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
jsem	být	k5eAaImIp1nS	být
psal	psát	k5eAaImAgMnS	psát
doktora	doktor	k1gMnSc4	doktor
Fausta	Faust	k1gMnSc4	Faust
<g/>
,	,	kIx,	,
Československý	československý	k2eAgMnSc1d1	československý
spisovatel	spisovatel	k1gMnSc1	spisovatel
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Dagmar	Dagmar	k1gFnSc1	Dagmar
Eisnerová	Eisnerová	k1gFnSc1	Eisnerová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Novely	novela	k1gFnPc1	novela
<g/>
,	,	kIx,	,
SNKLU	SNKLU	kA	SNKLU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Pavel	Pavel	k1gMnSc1	Pavel
a	a	k8xC	a
Dagmar	Dagmar	k1gFnSc4	Dagmar
Eisnerovi	Eisnerův	k2eAgMnPc1d1	Eisnerův
<g/>
,	,	kIx,	,
Jitka	Jitka	k1gFnSc1	Jitka
Fučíková	Fučíková	k1gFnSc1	Fučíková
a	a	k8xC	a
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Sekal	Sekal	k1gMnSc1	Sekal
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
díla	dílo	k1gNnPc4	dílo
Malý	Malý	k1gMnSc1	Malý
pan	pan	k1gMnSc1	pan
Friedemann	Friedemann	k1gMnSc1	Friedemann
<g/>
,	,	kIx,	,
Tonio	Tonio	k1gMnSc1	Tonio
Kröger	Kröger	k1gMnSc1	Kröger
<g/>
,	,	kIx,	,
Těžká	těžký	k2eAgFnSc1d1	těžká
hodina	hodina	k1gFnSc1	hodina
<g/>
,	,	kIx,	,
Smrt	smrt	k1gFnSc1	smrt
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
<g/>
,	,	kIx,	,
Tristan	Tristan	k1gInSc1	Tristan
<g/>
,	,	kIx,	,
Zázračné	zázračný	k2eAgNnSc1d1	zázračné
dítě	dítě	k1gNnSc1	dítě
<g/>
,	,	kIx,	,
Zmatek	zmatek	k1gInSc1	zmatek
a	a	k8xC	a
raný	raný	k2eAgInSc1d1	raný
žal	žal	k1gInSc1	žal
<g/>
,	,	kIx,	,
Pán	pán	k1gMnSc1	pán
a	a	k8xC	a
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
Mario	Mario	k1gMnSc1	Mario
a	a	k8xC	a
kouzelník	kouzelník	k1gMnSc1	kouzelník
a	a	k8xC	a
Vyměněné	vyměněný	k2eAgFnPc4d1	vyměněná
hlavy	hlava	k1gFnPc4	hlava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Exulant	exulant	k1gMnSc1	exulant
Thomas	Thomas	k1gMnSc1	Thomas
Mann	Mann	k1gMnSc1	Mann
<g/>
,	,	kIx,	,
Východočeské	východočeský	k2eAgNnSc1d1	Východočeské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Dagmar	Dagmar	k1gFnSc1	Dagmar
Eisnerová	Eisnerová	k1gFnSc1	Eisnerová
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc1	tři
politické	politický	k2eAgInPc1d1	politický
projevy	projev	k1gInPc1	projev
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1937	[number]	k4	1937
<g/>
-	-	kIx~	-
<g/>
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Smrt	smrt	k1gFnSc1	smrt
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Pavel	Pavel	k1gMnSc1	Pavel
Eisner	Eisner	k1gMnSc1	Eisner
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
Tábor	Tábor	k1gInSc1	Tábor
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyvolený	vyvolený	k1gMnSc1	vyvolený
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Jitka	Jitka	k1gFnSc1	Jitka
Fučíková	Fučíková	k1gFnSc1	Fučíková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Novely	novela	k1gFnPc1	novela
a	a	k8xC	a
povídky	povídka	k1gFnPc1	povídka
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Jitka	Jitka	k1gFnSc1	Jitka
Fučíková	Fučíková	k1gFnSc1	Fučíková
a	a	k8xC	a
Dagmar	Dagmar	k1gFnSc1	Dagmar
Steinová	Steinová	k1gFnSc1	Steinová
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
díla	dílo	k1gNnPc4	dílo
Malý	Malý	k1gMnSc1	Malý
pan	pan	k1gMnSc1	pan
Friedemann	Friedemann	k1gMnSc1	Friedemann
<g/>
,	,	kIx,	,
Šatník	šatník	k1gInSc1	šatník
<g/>
,	,	kIx,	,
Tristan	Tristan	k1gInSc1	Tristan
<g/>
,	,	kIx,	,
Tonio	Tonio	k1gMnSc1	Tonio
Kröger	Kröger	k1gMnSc1	Kröger
<g/>
,	,	kIx,	,
Zázračné	zázračný	k2eAgNnSc1d1	zázračné
dítě	dítě	k1gNnSc1	dítě
<g/>
,	,	kIx,	,
Těžká	těžký	k2eAgFnSc1d1	těžká
hodina	hodina	k1gFnSc1	hodina
<g/>
,	,	kIx,	,
Smrt	smrt	k1gFnSc1	smrt
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
<g/>
,	,	kIx,	,
Pán	pán	k1gMnSc1	pán
a	a	k8xC	a
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
Krev	krev	k1gFnSc1	krev
Wälsungů	Wälsung	k1gInPc2	Wälsung
<g/>
,	,	kIx,	,
Zmatek	zmatek	k1gInSc1	zmatek
a	a	k8xC	a
raný	raný	k2eAgInSc1d1	raný
žal	žal	k1gInSc1	žal
<g/>
,	,	kIx,	,
Mario	Mario	k1gMnSc1	Mario
a	a	k8xC	a
kouzelník	kouzelník	k1gMnSc1	kouzelník
<g/>
,	,	kIx,	,
Zaměněné	zaměněný	k2eAgFnPc1d1	zaměněná
hlavy	hlava	k1gFnPc1	hlava
a	a	k8xC	a
Zákon	zákon	k1gInSc1	zákon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zpověď	zpověď	k1gFnSc1	zpověď
hochštaplera	hochštapler	k1gMnSc2	hochštapler
Felixe	Felix	k1gMnSc2	Felix
Krulla	Krull	k1gMnSc2	Krull
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Anna	Anna	k1gFnSc1	Anna
Siebenscheinová	Siebenscheinová	k1gFnSc1	Siebenscheinová
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Buddenbrookovi	Buddenbrook	k1gMnSc3	Buddenbrook
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Jitka	Jitka	k1gFnSc1	Jitka
Fučíková	Fučíková	k1gFnSc1	Fučíková
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Práce	práce	k1gFnPc1	práce
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lotta	Lotta	k1gFnSc1	Lotta
ve	v	k7c6	v
Výmaru	Výmar	k1gInSc6	Výmar
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Anna	Anna	k1gFnSc1	Anna
Siebenscheinová	Siebenscheinová	k1gFnSc1	Siebenscheinová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Doktor	doktor	k1gMnSc1	doktor
Faustus	Faustus	k1gMnSc1	Faustus
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Hanuš	Hanuš	k1gMnSc1	Hanuš
Karlach	Karlach	k1gMnSc1	Karlach
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Královská	královský	k2eAgFnSc1d1	královská
výsost	výsost	k1gFnSc1	výsost
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Anna	Anna	k1gFnSc1	Anna
Siebenscheinová	Siebenscheinová	k1gFnSc1	Siebenscheinová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Malý	Malý	k1gMnSc1	Malý
pan	pan	k1gMnSc1	pan
Friedemann	Friedemann	k1gMnSc1	Friedemann
<g/>
,	,	kIx,	,
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Václav	Václav	k1gMnSc1	Václav
Bahník	bahník	k1gMnSc1	bahník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Smrt	smrt	k1gFnSc1	smrt
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
<g/>
,	,	kIx,	,
Aulos	Aulos	k1gInSc1	Aulos
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Jitka	Jitka	k1gFnSc1	Jitka
Fučíková	Fučíková	k1gFnSc1	Fučíková
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Konec	konec	k1gInSc1	konec
měšťanské	měšťanský	k2eAgFnSc2d1	měšťanská
epochy	epocha	k1gFnSc2	epocha
<g/>
.	.	kIx.	.
</s>
<s>
Eseje	esej	k1gFnPc1	esej
o	o	k7c6	o
literatuře	literatura	k1gFnSc6	literatura
<g/>
,	,	kIx,	,
hudbě	hudba	k1gFnSc6	hudba
a	a	k8xC	a
filozofii	filozofie	k1gFnSc4	filozofie
<g/>
,	,	kIx,	,
Dauphin	dauphin	k1gMnSc1	dauphin
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jan	Jan	k1gMnSc1	Jan
Hon	hon	k1gInSc4	hon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Smrt	smrt	k1gFnSc1	smrt
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Vratislav	Vratislav	k1gMnSc1	Vratislav
Slezák	Slezák	k1gMnSc1	Slezák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
<g/>
:	:	kIx,	:
autobiografické	autobiografický	k2eAgInPc4d1	autobiografický
spisy	spis	k1gInPc4	spis
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Milan	Milan	k1gMnSc1	Milan
Váňa	Váňa	k1gMnSc1	Váňa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Doktor	doktor	k1gMnSc1	doktor
Faustus	Faustus	k1gMnSc1	Faustus
<g/>
:	:	kIx,	:
život	život	k1gInSc1	život
německého	německý	k2eAgMnSc2d1	německý
hudebního	hudební	k2eAgMnSc2d1	hudební
skladatele	skladatel	k1gMnSc2	skladatel
Adriana	Adrian	k1gMnSc2	Adrian
Leverkühna	Leverkühn	k1gMnSc2	Leverkühn
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
jeho	jeho	k3xOp3gMnSc1	jeho
přítel	přítel	k1gMnSc1	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Hanuš	Hanuš	k1gMnSc1	Hanuš
Karlach	Karlach	k1gMnSc1	Karlach
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgFnPc1	druhý
<g/>
,	,	kIx,	,
revidované	revidovaný	k2eAgNnSc1d1	revidované
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
v	v	k7c6	v
Academii	academia	k1gFnSc6	academia
první	první	k4xOgFnSc6	první
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
687	[number]	k4	687
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
2436	[number]	k4	2436
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čarovná	čarovný	k2eAgFnSc1d1	čarovná
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Vratislav	Vratislav	k1gMnSc1	Vratislav
Slezák	Slezák	k1gMnSc1	Slezák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Filmové	filmový	k2eAgFnSc2d1	filmová
adaptace	adaptace	k1gFnSc2	adaptace
===	===	k?	===
</s>
</p>
<p>
<s>
Die	Die	k?	Die
Buddenbrooks	Buddenbrooks	k1gInSc1	Buddenbrooks
(	(	kIx(	(
<g/>
Buddenbrookovi	Buddenbrookův	k2eAgMnPc1d1	Buddenbrookův
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Gerhard	Gerhard	k1gMnSc1	Gerhard
Lamprecht	Lamprecht	k1gInSc1	Lamprecht
<g/>
,	,	kIx,	,
němý	němý	k2eAgInSc1d1	němý
film	film	k1gInSc1	film
</s>
</p>
<p>
<s>
Königliche	Königliche	k1gInSc1	Königliche
Hoheit	Hoheit	k1gInSc1	Hoheit
(	(	kIx(	(
<g/>
Královská	královský	k2eAgFnSc1d1	královská
výsost	výsost	k1gFnSc1	výsost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Harald	Harald	k1gMnSc1	Harald
Braun	Braun	k1gMnSc1	Braun
</s>
</p>
<p>
<s>
Bekenntnisse	Bekenntnisse	k6eAd1	Bekenntnisse
des	des	k1gNnSc1	des
Hochstaplers	Hochstaplers	k1gInSc1	Hochstaplers
Felix	Felix	k1gMnSc1	Felix
Krull	Krull	k1gMnSc1	Krull
(	(	kIx(	(
<g/>
Zpověď	zpověď	k1gFnSc1	zpověď
hochštaplera	hochštapler	k1gMnSc2	hochštapler
Felixe	Felix	k1gMnSc2	Felix
Krulla	Krull	k1gMnSc2	Krull
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Kurt	Kurt	k1gMnSc1	Kurt
Hoffmann	Hoffmann	k1gMnSc1	Hoffmann
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Horst	Horst	k1gMnSc1	Horst
Buchholz	Buchholz	k1gMnSc1	Buchholz
</s>
</p>
<p>
<s>
Les	les	k1gInSc1	les
Tê	Tê	k1gFnSc2	Tê
interverties	intervertiesa	k1gFnPc2	intervertiesa
(	(	kIx(	(
<g/>
Vyměněné	vyměněný	k2eAgFnSc2d1	vyměněná
hlavy	hlava	k1gFnSc2	hlava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Alejandro	Alejandra	k1gFnSc5	Alejandra
Jodorowsky	Jodorowska	k1gFnPc5	Jodorowska
</s>
</p>
<p>
<s>
Buddenbrooks	Buddenbrooks	k1gInSc1	Buddenbrooks
(	(	kIx(	(
<g/>
Buddenbrookovi	Buddenbrookův	k2eAgMnPc1d1	Buddenbrookův
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Alfred	Alfred	k1gMnSc1	Alfred
Weidenmann	Weidenmann	k1gMnSc1	Weidenmann
</s>
</p>
<p>
<s>
Tonio	Tonio	k1gMnSc1	Tonio
Kröger	Kröger	k1gMnSc1	Kröger
<g/>
,	,	kIx,	,
Francie-Německo	Francie-Německo	k1gNnSc1	Francie-Německo
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Rolf	Rolf	k1gMnSc1	Rolf
Thiele	Thiel	k1gInSc2	Thiel
</s>
</p>
<p>
<s>
Wälsungenblut	Wälsungenblut	k1gInSc1	Wälsungenblut
(	(	kIx(	(
<g/>
Krev	krev	k1gFnSc1	krev
Wälsungů	Wälsung	k1gInPc2	Wälsung
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Rolf	Rolf	k1gMnSc1	Rolf
Thiele	Thiel	k1gInSc2	Thiel
</s>
</p>
<p>
<s>
Buddenbrooks	Buddenbrooks	k1gInSc1	Buddenbrooks
(	(	kIx(	(
<g/>
Buddenbrookovi	Buddenbrookův	k2eAgMnPc1d1	Buddenbrookův
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Michael	Michael	k1gMnSc1	Michael
Imison	Imison	k1gInSc1	Imison
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
</s>
</p>
<p>
<s>
Der	drát	k5eAaImRp2nS	drát
Zauberberg	Zauberberg	k1gInSc1	Zauberberg
(	(	kIx(	(
<g/>
Kouzelný	kouzelný	k2eAgInSc1d1	kouzelný
vrch	vrch	k1gInSc1	vrch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Ludwig	Ludwig	k1gMnSc1	Ludwig
Cremer	Cremer	k1gInSc1	Cremer
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
</s>
</p>
<p>
<s>
Morte	Morte	k5eAaPmIp2nP	Morte
a	a	k8xC	a
Venezia	Venezia	k1gFnSc1	Venezia
(	(	kIx(	(
<g/>
Smrt	smrt	k1gFnSc1	smrt
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Luchino	Luchino	k1gNnSc1	Luchino
Visconti	Visconť	k1gFnSc2	Visconť
</s>
</p>
<p>
<s>
Lotte	Lotte	k5eAaPmIp2nP	Lotte
in	in	k?	in
Weimar	Weimar	k1gInSc1	Weimar
(	(	kIx(	(
<g/>
Lotta	Lotta	k1gFnSc1	Lotta
ve	v	k7c6	v
Výmaru	Výmar	k1gInSc6	Výmar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Egon	Egon	k1gMnSc1	Egon
Günther	Günthra	k1gFnPc2	Günthra
</s>
</p>
<p>
<s>
Tristan	Tristan	k1gInSc1	Tristan
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Herbert	Herbert	k1gMnSc1	Herbert
Ballmann	Ballmann	k1gInSc1	Ballmann
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
</s>
</p>
<p>
<s>
Unordnung	Unordnung	k1gMnSc1	Unordnung
und	und	k?	und
frühes	frühes	k1gMnSc1	frühes
Leid	Leid	k1gMnSc1	Leid
(	(	kIx(	(
<g/>
Zmatek	zmatek	k1gInSc1	zmatek
a	a	k8xC	a
raný	raný	k2eAgInSc1d1	raný
žal	žal	k1gInSc1	žal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Franz	Franz	k1gMnSc1	Franz
Seitz	Seitz	k1gMnSc1	Seitz
</s>
</p>
<p>
<s>
Die	Die	k?	Die
Buddenbrooks	Buddenbrooks	k1gInSc1	Buddenbrooks
(	(	kIx(	(
<g/>
Buddenbrookovi	Buddenbrookův	k2eAgMnPc1d1	Buddenbrookův
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Franz	Franz	k1gMnSc1	Franz
Peter	Peter	k1gMnSc1	Peter
Wirth	Wirth	k1gMnSc1	Wirth
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
</s>
</p>
<p>
<s>
Death	Death	k1gInSc1	Death
in	in	k?	in
Venice	Venice	k1gFnSc1	Venice
(	(	kIx(	(
<g/>
Smrt	smrt	k1gFnSc1	smrt
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Tony	Tony	k1gMnSc1	Tony
Palmer	Palmer	k1gMnSc1	Palmer
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
</s>
</p>
<p>
<s>
Bekenntnisse	Bekenntnisse	k6eAd1	Bekenntnisse
des	des	k1gNnSc1	des
Hochstaplers	Hochstaplers	k1gInSc1	Hochstaplers
Felix	Felix	k1gMnSc1	Felix
Krull	Krull	k1gMnSc1	Krull
(	(	kIx(	(
<g/>
Zpověď	zpověď	k1gFnSc1	zpověď
hochštaplera	hochštapler	k1gMnSc2	hochštapler
Felixe	Felix	k1gMnSc2	Felix
Krulla	Krull	k1gMnSc2	Krull
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Bernhard	Bernhard	k1gMnSc1	Bernhard
Sinkel	Sinkel	k1gInSc1	Sinkel
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
</s>
</p>
<p>
<s>
Der	drát	k5eAaImRp2nS	drát
Zauberberg	Zauberberg	k1gInSc1	Zauberberg
(	(	kIx(	(
<g/>
Kouzelný	kouzelný	k2eAgInSc1d1	kouzelný
vrch	vrch	k1gInSc1	vrch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Hans	Hans	k1gMnSc1	Hans
W.	W.	kA	W.
Geißendörfer	Geißendörfer	k1gMnSc1	Geißendörfer
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
Rod	rod	k1gInSc4	rod
Steiger	Steiger	k1gMnSc1	Steiger
a	a	k8xC	a
Charles	Charles	k1gMnSc1	Charles
Aznavour	Aznavour	k1gMnSc1	Aznavour
</s>
</p>
<p>
<s>
Doktor	doktor	k1gMnSc1	doktor
Faustus	Faustus	k1gMnSc1	Faustus
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Franz	Franz	k1gMnSc1	Franz
Seitz	Seitz	k1gMnSc1	Seitz
</s>
</p>
<p>
<s>
Death	Death	k1gInSc1	Death
in	in	k?	in
Venice	Venice	k1gFnSc1	Venice
(	(	kIx(	(
<g/>
Smrt	smrt	k1gFnSc1	smrt
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Robin	Robina	k1gFnPc2	Robina
Lough	Lougha	k1gFnPc2	Lougha
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
</s>
</p>
<p>
<s>
Le	Le	k?	Le
Miragle	Miragle	k1gInSc1	Miragle
(	(	kIx(	(
<g/>
Přelud	přelud	k1gInSc1	přelud
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Jean-Claude	Jean-Claud	k1gInSc5	Jean-Claud
Guiguet	Guigueta	k1gFnPc2	Guigueta
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
novely	novela	k1gFnSc2	novela
Podvedená	podvedený	k2eAgFnSc1d1	podvedená
</s>
</p>
<p>
<s>
Mario	Mario	k1gMnSc1	Mario
und	und	k?	und
der	drát	k5eAaImRp2nS	drát
Zauberer	Zauberer	k1gMnSc1	Zauberer
(	(	kIx(	(
<g/>
Mario	Mario	k1gMnSc1	Mario
a	a	k8xC	a
kouzelník	kouzelník	k1gMnSc1	kouzelník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Klaus	Klaus	k1gMnSc1	Klaus
Maria	Mario	k1gMnSc2	Mario
Brandauer	Brandauer	k1gMnSc1	Brandauer
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Erstausgaben	Erstausgaben	k2eAgMnSc1d1	Erstausgaben
Thomas	Thomas	k1gMnSc1	Thomas
Manns	Mannsa	k1gFnPc2	Mannsa
<g/>
.	.	kIx.	.
</s>
<s>
Ein	Ein	k?	Ein
bibliographischer	bibliographischra	k1gFnPc2	bibliographischra
Atlas	Atlas	k1gMnSc1	Atlas
<g/>
.	.	kIx.	.
</s>
<s>
Hans-Peter	Hans-Peter	k1gMnSc1	Hans-Peter
Haack	Haack	k1gMnSc1	Haack
(	(	kIx(	(
<g/>
Hrsg	Hrsg	k1gMnSc1	Hrsg
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mitarbeit	Mitarbeit	k2eAgMnSc1d1	Mitarbeit
Sebastian	Sebastian	k1gMnSc1	Sebastian
Kiwitt	Kiwitt	k1gMnSc1	Kiwitt
<g/>
,	,	kIx,	,
Antiquariat	Antiquariat	k2eAgMnSc1d1	Antiquariat
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Haack	Haack	k1gMnSc1	Haack
<g/>
,	,	kIx,	,
Leipzig	Leipzig	k1gMnSc1	Leipzig
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-3-00-031653-1	[number]	k4	978-3-00-031653-1
</s>
</p>
<p>
<s>
GOLDSTÜCKER	GOLDSTÜCKER	kA	GOLDSTÜCKER
<g/>
,	,	kIx,	,
Eduard	Eduard	k1gMnSc1	Eduard
<g/>
.	.	kIx.	.
</s>
<s>
Sestup	sestup	k1gInSc1	sestup
do	do	k7c2	do
hlubin	hlubina	k1gFnPc2	hlubina
<g/>
.	.	kIx.	.
</s>
<s>
Literární	literární	k2eAgFnPc1d1	literární
noviny	novina	k1gFnPc1	novina
<g/>
.	.	kIx.	.
1965	[number]	k4	1965
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
14	[number]	k4	14
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
33	[number]	k4	33
<g/>
,	,	kIx,	,
s.	s.	k?	s.
1	[number]	k4	1
a	a	k8xC	a
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MANN	Mann	k1gMnSc1	Mann
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
<g/>
:	:	kIx,	:
O	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
<g/>
:	:	kIx,	:
autobiografické	autobiografický	k2eAgInPc4d1	autobiografický
spisy	spis	k1gInPc4	spis
<g/>
.	.	kIx.	.
</s>
<s>
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
537	[number]	k4	537
s.	s.	k?	s.
Paměť	paměť	k1gFnSc1	paměť
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
67	[number]	k4	67
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-200-2280-6	[number]	k4	978-80-200-2280-6
</s>
</p>
<p>
<s>
REICH-RANICKI	REICH-RANICKI	k?	REICH-RANICKI
<g/>
,	,	kIx,	,
Marcel	Marcel	k1gMnSc1	Marcel
<g/>
:	:	kIx,	:
Mannovi	Mann	k1gMnSc3	Mann
<g/>
:	:	kIx,	:
Thomas	Thomas	k1gMnSc1	Thomas
Mann	Mann	k1gMnSc1	Mann
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Jinočany	Jinočan	k1gMnPc7	Jinočan
<g/>
:	:	kIx,	:
H	H	kA	H
&	&	k?	&
H	H	kA	H
Vyšehradská	vyšehradský	k2eAgFnSc1d1	Vyšehradská
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
293	[number]	k4	293
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978-80-7319-097-2	[number]	k4	978-80-7319-097-2
</s>
</p>
<p>
<s>
ŠŤÁHLAVSKÝ	ŠŤÁHLAVSKÝ	kA	ŠŤÁHLAVSKÝ
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
:	:	kIx,	:
Výpravy	výprava	k1gFnSc2	výprava
opačným	opačný	k2eAgInSc7d1	opačný
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Pobaltí	Pobaltí	k1gNnSc1	Pobaltí
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Radioservis	Radioservis	k1gInSc1	Radioservis
a	a	k8xC	a
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
181	[number]	k4	181
s.	s.	k?	s.
(	(	kIx(	(
<g/>
s.	s.	k?	s.
39	[number]	k4	39
<g/>
)	)	kIx)	)
ISBN	ISBN	kA	ISBN
80-86212-26-2	[number]	k4	80-86212-26-2
</s>
</p>
<p>
<s>
Thomas-Mann-Handbuch	Thomas-Mann-Handbuch	k1gMnSc1	Thomas-Mann-Handbuch
<g/>
.	.	kIx.	.
</s>
<s>
Leben	Leben	k1gInSc1	Leben
-	-	kIx~	-
Werk	Werk	k1gMnSc1	Werk
-	-	kIx~	-
Wirkung	Wirkung	k1gMnSc1	Wirkung
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Andreas	Andreas	k1gMnSc1	Andreas
Blödorn	Blödorn	k1gMnSc1	Blödorn
a	a	k8xC	a
Friedhelm	Friedhelm	k1gMnSc1	Friedhelm
Marx	Marx	k1gMnSc1	Marx
<g/>
.	.	kIx.	.
</s>
<s>
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
<g/>
:	:	kIx,	:
Metzler	Metzler	k1gInSc1	Metzler
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
425	[number]	k4	425
s.	s.	k?	s.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Německá	německý	k2eAgFnSc1d1	německá
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Rodina	rodina	k1gFnSc1	rodina
Mannů	Mann	k1gMnPc2	Mann
</s>
</p>
<p>
<s>
Heinrich	Heinrich	k1gMnSc1	Heinrich
Mann	Mann	k1gMnSc1	Mann
</s>
</p>
<p>
<s>
Klaus	Klaus	k1gMnSc1	Klaus
Mann	Mann	k1gMnSc1	Mann
</s>
</p>
<p>
<s>
Golo	Golo	k1gMnSc1	Golo
Mann	Mann	k1gMnSc1	Mann
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Thomas	Thomas	k1gMnSc1	Thomas
Mann	Mann	k1gMnSc1	Mann
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Thomas	Thomas	k1gMnSc1	Thomas
Mann	Mann	k1gMnSc1	Mann
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Thomas	Thomas	k1gMnSc1	Thomas
Mann	Mann	k1gMnSc1	Mann
</s>
</p>
<p>
<s>
Biografie	biografie	k1gFnSc1	biografie
Thomase	Thomas	k1gMnSc2	Thomas
Manna	Mann	k1gMnSc2	Mann
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bibliografie	bibliografie	k1gFnSc1	bibliografie
Thomase	Thomas	k1gMnSc2	Thomas
Manna	Mann	k1gMnSc2	Mann
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
o	o	k7c6	o
Thomasi	Thomas	k1gMnSc6	Thomas
Mannovi	Mann	k1gMnSc6	Mann
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Literární	literární	k2eAgInSc1d1	literární
lexikon	lexikon	k1gInSc1	lexikon
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
První	první	k4xOgNnSc1	první
vydání	vydání	k1gNnSc1	vydání
děl	dělo	k1gNnPc2	dělo
Thomase	Thomas	k1gMnSc2	Thomas
Manna	Mann	k1gMnSc2	Mann
(	(	kIx(	(
<g/>
Sbírka	sbírka	k1gFnSc1	sbírka
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Haacka	Haacka	k1gFnSc1	Haacka
<g/>
,	,	kIx,	,
Lipsko	Lipsko	k1gNnSc1	Lipsko
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
Mann	Mann	k1gMnSc1	Mann
na	na	k7c4	na
databazeknih	databazeknih	k1gInSc4	databazeknih
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
