<s>
Metalurgie	metalurgie	k1gFnSc1	metalurgie
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
též	též	k9	též
hutnictví	hutnictví	k1gNnSc2	hutnictví
<g/>
;	;	kIx,	;
z	z	k7c2	z
řeckého	řecký	k2eAgMnSc2d1	řecký
metallourgos	metallourgos	k1gMnSc1	metallourgos
<g/>
,	,	kIx,	,
pracovník	pracovník	k1gMnSc1	pracovník
s	s	k7c7	s
kovem	kov	k1gInSc7	kov
<	<	kIx(	<
metallon	metallon	k1gInSc1	metallon
<g/>
,	,	kIx,	,
kov	kov	k1gInSc1	kov
+	+	kIx~	+
ergon	ergon	k1gInSc1	ergon
<g/>
,	,	kIx,	,
práce	práce	k1gFnSc1	práce
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
a	a	k8xC	a
výrobní	výrobní	k2eAgNnPc1d1	výrobní
odvětví	odvětví	k1gNnPc1	odvětví
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
získáváním	získávání	k1gNnSc7	získávání
a	a	k8xC	a
zpracováním	zpracování	k1gNnSc7	zpracování
kovů	kov	k1gInPc2	kov
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
slitin	slitina	k1gFnPc2	slitina
<g/>
.	.	kIx.	.
</s>
