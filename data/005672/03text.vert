<s>
Metalurgie	metalurgie	k1gFnSc1	metalurgie
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
též	též	k9	též
hutnictví	hutnictví	k1gNnSc2	hutnictví
<g/>
;	;	kIx,	;
z	z	k7c2	z
řeckého	řecký	k2eAgMnSc2d1	řecký
metallourgos	metallourgos	k1gMnSc1	metallourgos
<g/>
,	,	kIx,	,
pracovník	pracovník	k1gMnSc1	pracovník
s	s	k7c7	s
kovem	kov	k1gInSc7	kov
<	<	kIx(	<
metallon	metallon	k1gInSc1	metallon
<g/>
,	,	kIx,	,
kov	kov	k1gInSc1	kov
+	+	kIx~	+
ergon	ergon	k1gInSc1	ergon
<g/>
,	,	kIx,	,
práce	práce	k1gFnSc1	práce
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
a	a	k8xC	a
výrobní	výrobní	k2eAgNnPc1d1	výrobní
odvětví	odvětví	k1gNnPc1	odvětví
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
získáváním	získávání	k1gNnSc7	získávání
a	a	k8xC	a
zpracováním	zpracování	k1gNnSc7	zpracování
kovů	kov	k1gInPc2	kov
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
slitin	slitina	k1gFnPc2	slitina
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
výrobu	výroba	k1gFnSc4	výroba
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
oceli	ocel	k1gFnSc2	ocel
<g/>
,	,	kIx,	,
výrobu	výroba	k1gFnSc4	výroba
barevných	barevný	k2eAgInPc2d1	barevný
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
následné	následný	k2eAgNnSc4d1	následné
zpracování	zpracování	k1gNnSc4	zpracování
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
úlohu	úloha	k1gFnSc4	úloha
při	při	k7c6	při
pochopení	pochopení	k1gNnSc1	pochopení
a	a	k8xC	a
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
dějů	děj	k1gInPc2	děj
probíhajících	probíhající	k2eAgInPc2d1	probíhající
v	v	k7c6	v
kovech	kov	k1gInPc6	kov
zde	zde	k6eAd1	zde
sehrává	sehrávat	k5eAaImIp3nS	sehrávat
fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
metalurgie	metalurgie	k1gFnPc4	metalurgie
a	a	k8xC	a
též	též	k9	též
také	také	k9	také
fyzika	fyzika	k1gFnSc1	fyzika
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
člověkem	člověk	k1gMnSc7	člověk
opracovaným	opracovaný	k2eAgInSc7d1	opracovaný
kovem	kov	k1gInSc7	kov
bylo	být	k5eAaImAgNnS	být
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
zlato	zlato	k1gNnSc4	zlato
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeskyních	jeskyně	k1gFnPc6	jeskyně
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgNnP	být
nalezena	nalezen	k2eAgNnPc1d1	Nalezeno
malá	malý	k2eAgNnPc1d1	malé
množství	množství	k1gNnPc1	množství
metalurgicky	metalurgicky	k6eAd1	metalurgicky
nezpracovaného	zpracovaný	k2eNgNnSc2d1	nezpracované
zlata	zlato	k1gNnSc2	zlato
ze	z	k7c2	z
starší	starý	k2eAgFnSc2d2	starší
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
eneolitu	eneolit	k1gInSc6	eneolit
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
zpracování	zpracování	k1gNnSc1	zpracování
mědi	měď	k1gFnSc2	měď
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Metalurgie	metalurgie	k1gFnSc1	metalurgie
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
podél	podél	k7c2	podél
řek	řeka	k1gFnPc2	řeka
a	a	k8xC	a
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Používání	používání	k1gNnSc1	používání
metalurgických	metalurgický	k2eAgInPc2d1	metalurgický
postupů	postup	k1gInPc2	postup
při	při	k7c6	při
zpracovávání	zpracovávání	k1gNnSc6	zpracovávání
kovů	kov	k1gInPc2	kov
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
doloženo	doložit	k5eAaPmNgNnS	doložit
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
kultur	kultura	k1gFnPc2	kultura
a	a	k8xC	a
civilizací	civilizace	k1gFnPc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
antická	antický	k2eAgNnPc4d1	antické
a	a	k8xC	a
středověká	středověký	k2eAgNnPc4d1	středověké
království	království	k1gNnPc4	království
<g/>
,	,	kIx,	,
říše	říše	k1gFnSc1	říše
Středního	střední	k2eAgInSc2d1	střední
a	a	k8xC	a
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
<g/>
,	,	kIx,	,
antický	antický	k2eAgInSc1d1	antický
Egypt	Egypt	k1gInSc1	Egypt
<g/>
,	,	kIx,	,
Anatolie	Anatolie	k1gFnSc1	Anatolie
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgNnSc1d1	dnešní
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Incká	incký	k2eAgFnSc1d1	incká
říše	říše	k1gFnSc1	říše
<g/>
,	,	kIx,	,
Řecká	řecký	k2eAgFnSc1d1	řecká
i	i	k8xC	i
Římská	římský	k2eAgFnSc1d1	římská
říše	říše	k1gFnSc1	říše
v	v	k7c6	v
antické	antický	k2eAgFnSc6d1	antická
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
středověká	středověký	k2eAgFnSc1d1	středověká
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
staré	starý	k2eAgNnSc1d1	staré
i	i	k8xC	i
středověké	středověký	k2eAgNnSc1d1	středověké
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
a	a	k8xC	a
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Dlouho	dlouho	k6eAd1	dlouho
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
běžnými	běžný	k2eAgFnPc7d1	běžná
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
mnohé	mnohý	k2eAgInPc1d1	mnohý
výrobní	výrobní	k2eAgInPc1d1	výrobní
postupy	postup	k1gInPc1	postup
a	a	k8xC	a
zařízení	zařízení	k1gNnSc1	zařízení
používané	používaný	k2eAgNnSc1d1	používané
v	v	k7c6	v
metalurgii	metalurgie	k1gFnSc6	metalurgie
známé	známý	k2eAgFnSc6d1	známá
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
(	(	kIx(	(
<g/>
jde	jít	k5eAaImIp3nS	jít
například	například	k6eAd1	například
o	o	k7c4	o
vysokou	vysoký	k2eAgFnSc4d1	vysoká
pec	pec	k1gFnSc4	pec
<g/>
,	,	kIx,	,
lití	lití	k1gNnSc4	lití
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
výrobu	výroba	k1gFnSc4	výroba
oceli	ocel	k1gFnSc2	ocel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
hydraulické	hydraulický	k2eAgInPc1d1	hydraulický
kovací	kovací	k2eAgInPc1d1	kovací
lisy	lis	k1gInPc1	lis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kovy	kov	k1gInPc1	kov
se	se	k3xPyFc4	se
zpracovávají	zpracovávat	k5eAaImIp3nP	zpracovávat
litím	lití	k1gNnSc7	lití
<g/>
,	,	kIx,	,
kováním	kování	k1gNnSc7	kování
<g/>
,	,	kIx,	,
tvářením	tváření	k1gNnSc7	tváření
za	za	k7c2	za
studena	studeno	k1gNnSc2	studeno
<g/>
,	,	kIx,	,
válcováním	válcování	k1gNnSc7	válcování
<g/>
,	,	kIx,	,
protlačováním	protlačování	k1gNnSc7	protlačování
<g/>
,	,	kIx,	,
slinováním	slinování	k1gNnSc7	slinování
<g/>
,	,	kIx,	,
kovoobráběním	kovoobrábění	k1gNnSc7	kovoobrábění
<g/>
,	,	kIx,	,
stříháním	stříhání	k1gNnSc7	stříhání
a	a	k8xC	a
tvářením	tváření	k1gNnSc7	tváření
<g/>
.	.	kIx.	.
</s>
<s>
Lití	lití	k1gNnSc4	lití
Roztavený	roztavený	k2eAgInSc1d1	roztavený
kov	kov	k1gInSc1	kov
se	se	k3xPyFc4	se
nalije	nalít	k5eAaPmIp3nS	nalít
do	do	k7c2	do
připravené	připravený	k2eAgFnSc2d1	připravená
formy	forma	k1gFnSc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
i	i	k9	i
pří	přít	k5eAaImIp3nS	přít
výrobě	výroba	k1gFnSc3	výroba
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
oceli	ocel	k1gFnSc2	ocel
i	i	k8xC	i
barevných	barevný	k2eAgInPc2d1	barevný
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
hutní	hutní	k2eAgFnSc1d1	hutní
druhovýroba	druhovýroba	k1gFnSc1	druhovýroba
(	(	kIx(	(
<g/>
větší	veliký	k2eAgInPc1d2	veliký
výrobky	výrobek	k1gInPc1	výrobek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
řada	řada	k1gFnSc1	řada
druhů	druh	k1gInPc2	druh
lití	lití	k1gNnSc2	lití
<g/>
,	,	kIx,	,
nejběžnější	běžný	k2eAgInPc4d3	nejběžnější
způsoby	způsob	k1gInPc4	způsob
představuje	představovat	k5eAaImIp3nS	představovat
lití	lití	k1gNnSc1	lití
do	do	k7c2	do
pískových	pískový	k2eAgFnPc2d1	písková
nebo	nebo	k8xC	nebo
voskových	voskový	k2eAgFnPc2d1	vosková
forem	forma	k1gFnPc2	forma
<g/>
,	,	kIx,	,
tlakové	tlakový	k2eAgNnSc1d1	tlakové
lití	lití	k1gNnSc1	lití
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
plynulé	plynulý	k2eAgNnSc1d1	plynulé
lití	lití	k1gNnSc1	lití
(	(	kIx(	(
<g/>
kontilití	kontilití	k1gNnSc1	kontilití
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kování	kování	k1gNnSc4	kování
Rozpálený	rozpálený	k2eAgInSc1d1	rozpálený
kus	kus	k1gInSc1	kus
kovu	kov	k1gInSc2	kov
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
náboj	náboj	k1gInSc1	náboj
<g/>
,	,	kIx,	,
vytvaruje	vytvarovat	k5eAaPmIp3nS	vytvarovat
nárazy	náraz	k1gInPc4	náraz
kladiva	kladivo	k1gNnSc2	kladivo
či	či	k8xC	či
v	v	k7c6	v
kovacím	kovací	k2eAgInSc6d1	kovací
lisu	lis	k1gInSc6	lis
(	(	kIx(	(
<g/>
bucharu	buchar	k1gInSc2	buchar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Válcování	válcování	k1gNnSc1	válcování
Při	při	k7c6	při
procesu	proces	k1gInSc6	proces
válcování	válcování	k1gNnSc2	válcování
se	se	k3xPyFc4	se
náboj	náboj	k1gInSc1	náboj
posouvá	posouvat	k5eAaImIp3nS	posouvat
mezi	mezi	k7c7	mezi
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
položenými	položený	k2eAgInPc7d1	položený
válci	válec	k1gInPc7	válec
<g/>
.	.	kIx.	.
</s>
<s>
Válcování	válcování	k1gNnSc1	válcování
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
provádět	provádět	k5eAaImF	provádět
za	za	k7c2	za
studena	studeno	k1gNnSc2	studeno
i	i	k9	i
za	za	k7c2	za
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Odstup	odstup	k1gInSc1	odstup
mezi	mezi	k7c7	mezi
válci	válec	k1gInPc7	válec
je	být	k5eAaImIp3nS	být
postupně	postupně	k6eAd1	postupně
snižován	snižovat	k5eAaImNgInS	snižovat
<g/>
,	,	kIx,	,
až	až	k9	až
se	se	k3xPyFc4	se
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
(	(	kIx(	(
<g/>
plechu	plech	k1gInSc2	plech
<g/>
,	,	kIx,	,
kolejnice	kolejnice	k1gFnSc2	kolejnice
<g/>
,	,	kIx,	,
traverzy	traverza	k1gFnSc2	traverza
<g/>
)	)	kIx)	)
požadované	požadovaný	k2eAgFnSc2d1	požadovaná
tloušťky	tloušťka	k1gFnSc2	tloušťka
vývalku	vývalek	k1gInSc2	vývalek
<g/>
.	.	kIx.	.
</s>
<s>
Válcováním	válcování	k1gNnSc7	válcování
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
například	například	k6eAd1	například
i	i	k9	i
bezešvé	bezešvý	k2eAgFnPc4d1	bezešvá
trubky	trubka	k1gFnPc4	trubka
<g/>
.	.	kIx.	.
</s>
<s>
Protlačování	protlačování	k1gNnSc4	protlačování
U	u	k7c2	u
této	tento	k3xDgFnSc2	tento
metody	metoda	k1gFnSc2	metoda
dosáhneme	dosáhnout	k5eAaPmIp1nP	dosáhnout
požadovaného	požadovaný	k2eAgInSc2d1	požadovaný
tvaru	tvar	k1gInSc2	tvar
protlačováním	protlačování	k1gNnSc7	protlačování
rozpáleného	rozpálený	k2eAgNnSc2d1	rozpálené
temperovaného	temperovaný	k2eAgNnSc2d1	temperované
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
kujného	kujný	k2eAgInSc2d1	kujný
<g/>
)	)	kIx)	)
kovu	kov	k1gInSc2	kov
přes	přes	k7c4	přes
matrici	matrice	k1gFnSc4	matrice
<g/>
.	.	kIx.	.
</s>
<s>
Slinování	slinování	k1gNnSc1	slinování
Jako	jako	k8xS	jako
slinování	slinování	k1gNnSc1	slinování
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
metoda	metoda	k1gFnSc1	metoda
stlačování	stlačování	k1gNnPc2	stlačování
kovového	kovový	k2eAgInSc2d1	kovový
prášku	prášek	k1gInSc2	prášek
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
za	za	k7c2	za
vysokých	vysoký	k2eAgFnPc2d1	vysoká
teplot	teplota	k1gFnPc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Kovoobrábění	Kovoobrábění	k1gNnSc1	Kovoobrábění
Kovoobráběcí	kovoobráběcí	k2eAgInPc1d1	kovoobráběcí
stroje	stroj	k1gInPc1	stroj
jsou	být	k5eAaImIp3nP	být
soustruhy	soustruh	k1gInPc4	soustruh
<g/>
,	,	kIx,	,
frézky	frézka	k1gFnPc4	frézka
a	a	k8xC	a
vrtačky	vrtačka	k1gFnPc4	vrtačka
<g/>
.	.	kIx.	.
</s>
<s>
Kovoobráběcí	kovoobráběcí	k2eAgInPc1d1	kovoobráběcí
postupy	postup	k1gInPc1	postup
se	se	k3xPyFc4	se
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
pro	pro	k7c4	pro
tvarování	tvarování	k1gNnSc4	tvarování
kovů	kov	k1gInPc2	kov
pouze	pouze	k6eAd1	pouze
za	za	k7c2	za
studena	studeno	k1gNnSc2	studeno
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
řezáním	řezání	k1gNnSc7	řezání
a	a	k8xC	a
broušením	broušení	k1gNnSc7	broušení
<g/>
.	.	kIx.	.
</s>
<s>
Lisování	lisování	k1gNnPc4	lisování
Tvar	tvar	k1gInSc1	tvar
výrobku	výrobek	k1gInSc2	výrobek
je	být	k5eAaImIp3nS	být
vytvářen	vytvářet	k5eAaImNgInS	vytvářet
za	za	k7c2	za
studena	studeno	k1gNnSc2	studeno
lisováním	lisování	k1gNnSc7	lisování
pomocí	pomocí	k7c2	pomocí
tvářecích	tvářecí	k2eAgInPc2d1	tvářecí
strojů	stroj	k1gInPc2	stroj
-	-	kIx~	-
lisů	lis	k1gInPc2	lis
nebo	nebo	k8xC	nebo
bucharů	buchar	k1gInPc2	buchar
<g/>
.	.	kIx.	.
</s>
<s>
Stříhání	stříhání	k1gNnSc1	stříhání
a	a	k8xC	a
ohýbání	ohýbání	k1gNnSc1	ohýbání
Plechy	plech	k1gInPc1	plech
a	a	k8xC	a
tyče	tyč	k1gFnPc1	tyč
jsou	být	k5eAaImIp3nP	být
stříhány	stříhat	k5eAaImNgFnP	stříhat
na	na	k7c6	na
lisech	lis	k1gInPc6	lis
nebo	nebo	k8xC	nebo
hydraulických	hydraulický	k2eAgFnPc6d1	hydraulická
nůžkách	nůžky	k1gFnPc6	nůžky
a	a	k8xC	a
ohýbány	ohýbat	k5eAaImNgFnP	ohýbat
do	do	k7c2	do
požadovaného	požadovaný	k2eAgInSc2d1	požadovaný
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Zpracování	zpracování	k1gNnSc1	zpracování
kovů	kov	k1gInPc2	kov
za	za	k7c4	za
studena	studeno	k1gNnPc4	studeno
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
válcování	válcování	k1gNnSc1	válcování
<g/>
,	,	kIx,	,
lisování	lisování	k1gNnSc1	lisování
a	a	k8xC	a
ohýbání	ohýbání	k1gNnSc1	ohýbání
<g/>
,	,	kIx,	,
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
tvrdost	tvrdost	k1gFnSc1	tvrdost
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
materiálu	materiál	k1gInSc6	materiál
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
mikroskopickým	mikroskopický	k2eAgFnPc3d1	mikroskopická
změnám	změna	k1gFnPc3	změna
<g/>
,	,	kIx,	,
defektům	defekt	k1gInPc3	defekt
<g/>
,	,	kIx,	,
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
tak	tak	k9	tak
stává	stávat	k5eAaImIp3nS	stávat
odolnějším	odolný	k2eAgInSc7d2	odolnější
proti	proti	k7c3	proti
dalšímu	další	k2eAgNnSc3d1	další
tvarování	tvarování	k1gNnSc3	tvarování
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
kov	kov	k1gInSc1	kov
zpracován	zpracovat	k5eAaPmNgInS	zpracovat
za	za	k7c2	za
tepla	teplo	k1gNnSc2	teplo
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
žíháním	žíhání	k1gNnSc7	žíhání
<g/>
,	,	kIx,	,
kalením	kalení	k1gNnSc7	kalení
<g/>
,	,	kIx,	,
temperováním	temperování	k1gNnSc7	temperování
<g/>
,	,	kIx,	,
alitováním	alitování	k1gNnSc7	alitování
a	a	k8xC	a
cementováním	cementování	k1gNnSc7	cementování
<g/>
,	,	kIx,	,
ovlivní	ovlivnit	k5eAaPmIp3nS	ovlivnit
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc7	jeho
odolnost	odolnost	k1gFnSc1	odolnost
proti	proti	k7c3	proti
korozi	koroze	k1gFnSc3	koroze
i	i	k9	i
mechanické	mechanický	k2eAgFnPc4d1	mechanická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
tvrdost	tvrdost	k1gFnSc1	tvrdost
a	a	k8xC	a
houževnatost	houževnatost	k1gFnSc1	houževnatost
<g/>
.	.	kIx.	.
</s>
<s>
Žíhání	žíhání	k1gNnSc1	žíhání
kov	kov	k1gInSc1	kov
změkčuje	změkčovat	k5eAaImIp3nS	změkčovat
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
jej	on	k3xPp3gMnSc4	on
činí	činit	k5eAaImIp3nS	činit
houževnatějším	houževnatý	k2eAgInSc7d2	houževnatější
<g/>
.	.	kIx.	.
</s>
<s>
Kalení	kalení	k1gNnSc2	kalení
a	a	k8xC	a
cementování	cementování	k1gNnSc2	cementování
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
tvrdost	tvrdost	k1gFnSc4	tvrdost
<g/>
.	.	kIx.	.
</s>
<s>
Zakalený	zakalený	k2eAgInSc1d1	zakalený
kov	kov	k1gInSc1	kov
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
a	a	k8xC	a
křehký	křehký	k2eAgInSc1d1	křehký
<g/>
.	.	kIx.	.
</s>
<s>
Temperování	temperování	k1gNnSc1	temperování
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
až	až	k9	až
po	po	k7c6	po
kalení	kalení	k1gNnSc6	kalení
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
křehkosti	křehkost	k1gFnSc2	křehkost
a	a	k8xC	a
celkovému	celkový	k2eAgNnSc3d1	celkové
zlepšení	zlepšení	k1gNnSc3	zlepšení
vlastností	vlastnost	k1gFnPc2	vlastnost
kovu	kov	k1gInSc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
technika	technika	k1gFnSc1	technika
používaná	používaný	k2eAgFnSc1d1	používaná
ke	k	k7c3	k
spojování	spojování	k1gNnSc3	spojování
některých	některý	k3yIgInPc2	některý
železných	železný	k2eAgInPc2d1	železný
kovů	kov	k1gInPc2	kov
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
slitin	slitina	k1gFnPc2	slitina
hliníku	hliník	k1gInSc2	hliník
jako	jako	k8xS	jako
svarového	svarový	k2eAgInSc2d1	svarový
kovu	kov	k1gInSc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Spojované	spojovaný	k2eAgInPc1d1	spojovaný
materiály	materiál	k1gInPc1	materiál
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
vzájemně	vzájemně	k6eAd1	vzájemně
podobné	podobný	k2eAgFnPc4d1	podobná
slitiny	slitina	k1gFnPc4	slitina
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdé	Tvrdé	k2eAgNnSc1d1	Tvrdé
pájení	pájení	k1gNnSc1	pájení
je	být	k5eAaImIp3nS	být
technika	technika	k1gFnSc1	technika
používaná	používaný	k2eAgFnSc1d1	používaná
při	při	k7c6	při
spojování	spojování	k1gNnSc6	spojování
železných	železný	k2eAgInPc2d1	železný
i	i	k8xC	i
neželezných	železný	k2eNgInPc2d1	neželezný
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Svarovým	svarový	k2eAgInSc7d1	svarový
kovem	kov	k1gInSc7	kov
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
slitina	slitina	k1gFnSc1	slitina
mědi	měď	k1gFnSc2	měď
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
mosaz	mosaz	k1gFnSc4	mosaz
nebo	nebo	k8xC	nebo
bronz	bronz	k1gInSc4	bronz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokovování	pokovování	k1gNnSc1	pokovování
je	být	k5eAaImIp3nS	být
nejpoužívanější	používaný	k2eAgFnSc1d3	nejpoužívanější
metoda	metoda	k1gFnSc1	metoda
povrchové	povrchový	k2eAgFnSc2d1	povrchová
úpravy	úprava	k1gFnSc2	úprava
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Metoda	metoda	k1gFnSc1	metoda
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
nanášení	nanášení	k1gNnSc6	nanášení
tenké	tenký	k2eAgFnSc2d1	tenká
vrstvy	vrstva	k1gFnSc2	vrstva
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
chromu	chrom	k1gInSc2	chrom
<g/>
,	,	kIx,	,
zinku	zinek	k1gInSc2	zinek
nebo	nebo	k8xC	nebo
jiného	jiný	k2eAgInSc2d1	jiný
kovu	kov	k1gInSc2	kov
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
produktu	produkt	k1gInSc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Pokovování	pokovování	k1gNnSc1	pokovování
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
korozivzdornosti	korozivzdornost	k1gFnSc2	korozivzdornost
a	a	k8xC	a
zlepšení	zlepšení	k1gNnSc2	zlepšení
estetických	estetický	k2eAgFnPc2d1	estetická
vlastností	vlastnost	k1gFnPc2	vlastnost
produktu	produkt	k1gInSc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Hutnictví	hutnictví	k1gNnSc1	hutnictví
je	být	k5eAaImIp3nS	být
samostatný	samostatný	k2eAgInSc4d1	samostatný
průmyslový	průmyslový	k2eAgInSc4d1	průmyslový
obor	obor	k1gInSc4	obor
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
výrobou	výroba	k1gFnSc7	výroba
surových	surový	k2eAgInPc2d1	surový
kovů	kov	k1gInPc2	kov
a	a	k8xC	a
výrobků	výrobek	k1gInPc2	výrobek
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
pojmem	pojem	k1gInSc7	pojem
hutnictví	hutnictví	k1gNnSc2	hutnictví
spadá	spadat	k5eAaImIp3nS	spadat
veškerá	veškerý	k3xTgFnSc1	veškerý
hutní	hutní	k2eAgFnSc1d1	hutní
výroba	výroba	k1gFnSc1	výroba
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
včetně	včetně	k7c2	včetně
výroby	výroba	k1gFnSc2	výroba
neželezných	železný	k2eNgInPc2d1	neželezný
respektive	respektive	k9	respektive
barevných	barevný	k2eAgInPc2d1	barevný
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
podoborem	podobor	k1gInSc7	podobor
hutnictví	hutnictví	k1gNnSc2	hutnictví
je	být	k5eAaImIp3nS	být
obor	obor	k1gInSc1	obor
hutnictví	hutnictví	k1gNnSc2	hutnictví
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Hutnictví	hutnictví	k1gNnSc1	hutnictví
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
starým	starý	k2eAgInSc7d1	starý
oborem	obor	k1gInSc7	obor
lidské	lidský	k2eAgFnSc2d1	lidská
činnosti	činnost	k1gFnSc2	činnost
a	a	k8xC	a
jako	jako	k9	jako
takové	takový	k3xDgNnSc1	takový
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
už	už	k6eAd1	už
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
bronzové	bronzový	k2eAgFnSc2d1	bronzová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hutnictví	hutnictví	k1gNnSc6	hutnictví
často	často	k6eAd1	často
navazuje	navazovat	k5eAaImIp3nS	navazovat
příbuzný	příbuzný	k2eAgInSc4d1	příbuzný
obor	obor	k1gInSc4	obor
slévárenství	slévárenství	k1gNnSc2	slévárenství
respektive	respektive	k9	respektive
těžké	těžký	k2eAgNnSc1d1	těžké
strojírenství	strojírenství	k1gNnSc1	strojírenství
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
obory	obor	k1gInPc1	obor
lidské	lidský	k2eAgFnSc2d1	lidská
činnosti	činnost	k1gFnSc2	činnost
jako	jako	k8xS	jako
např.	např.	kA	např.
koksárenství	koksárenství	k1gNnSc1	koksárenství
<g/>
,	,	kIx,	,
plynárenství	plynárenství	k1gNnSc1	plynárenství
popřípadě	popřípadě	k6eAd1	popřípadě
i	i	k8xC	i
chemický	chemický	k2eAgInSc1d1	chemický
průmysl	průmysl	k1gInSc1	průmysl
<g/>
.	.	kIx.	.
suroviny	surovina	k1gFnPc1	surovina
:	:	kIx,	:
železná	železný	k2eAgFnSc1d1	železná
ruda	ruda	k1gFnSc1	ruda
<g/>
,	,	kIx,	,
koks	koks	k1gInSc1	koks
<g/>
,	,	kIx,	,
vápenec	vápenec	k1gInSc1	vápenec
<g/>
,	,	kIx,	,
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
kovový	kovový	k2eAgInSc1d1	kovový
šrot	šrot	k1gInSc1	šrot
<g/>
,	,	kIx,	,
vzduch	vzduch	k1gInSc1	vzduch
nejčastější	častý	k2eAgInPc1d3	nejčastější
výrobky	výrobek	k1gInPc7	výrobek
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
trubky	trubka	k1gFnPc4	trubka
<g/>
,	,	kIx,	,
roury	roura	k1gFnPc4	roura
<g/>
,	,	kIx,	,
ocel	ocel	k1gFnSc4	ocel
<g/>
,	,	kIx,	,
kolejnice	kolejnice	k1gFnPc1	kolejnice
<g/>
,	,	kIx,	,
traverzy	traverza	k1gFnPc1	traverza
<g/>
,	,	kIx,	,
strojní	strojní	k2eAgFnPc1d1	strojní
součásti	součást	k1gFnPc1	součást
<g/>
,	,	kIx,	,
odlitky	odlitek	k1gInPc1	odlitek
<g/>
,	,	kIx,	,
dráty	drát	k1gInPc1	drát
<g/>
,	,	kIx,	,
plechy	plech	k1gInPc1	plech
<g/>
,	,	kIx,	,
ocelové	ocelový	k2eAgInPc1d1	ocelový
profily	profil	k1gInPc1	profil
a	a	k8xC	a
nosníky	nosník	k1gInPc1	nosník
staví	stavit	k5eAaImIp3nP	stavit
se	s	k7c7	s
:	:	kIx,	:
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
těžby	těžba	k1gFnSc2	těžba
železné	železný	k2eAgFnSc2d1	železná
rudy	ruda	k1gFnSc2	ruda
nebo	nebo	k8xC	nebo
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
,	,	kIx,	,
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
dopravních	dopravní	k2eAgFnPc2d1	dopravní
cest	cesta	k1gFnPc2	cesta
rozmístění	rozmístění	k1gNnSc2	rozmístění
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Třinec	Třinec	k1gInSc1	Třinec
<g/>
,	,	kIx,	,
Frýdek-Místek	Frýdek-Místek	k1gInSc1	Frýdek-Místek
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
Bohumín	Bohumín	k1gInSc1	Bohumín
a	a	k8xC	a
okolí	okolí	k1gNnSc1	okolí
<g/>
,	,	kIx,	,
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
též	též	k9	též
Kladno	Kladno	k1gNnSc1	Kladno
obvyklé	obvyklý	k2eAgFnSc2d1	obvyklá
technologie	technologie	k1gFnSc2	technologie
:	:	kIx,	:
kontinuální	kontinuální	k2eAgNnPc4d1	kontinuální
lití	lití	k1gNnPc4	lití
<g/>
,	,	kIx,	,
válcování	válcování	k1gNnSc4	válcování
za	za	k7c2	za
tepla	teplo	k1gNnSc2	teplo
<g/>
,	,	kIx,	,
válcování	válcování	k1gNnSc2	válcování
za	za	k7c2	za
studena	studeno	k1gNnSc2	studeno
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
odlitků	odlitek	k1gInPc2	odlitek
(	(	kIx(	(
<g/>
slévárny	slévárna	k1gFnPc1	slévárna
<g/>
)	)	kIx)	)
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
obory	obor	k1gInPc1	obor
:	:	kIx,	:
koksárenství	koksárenství	k1gNnSc1	koksárenství
<g/>
,	,	kIx,	,
plynárenství	plynárenství	k1gNnSc1	plynárenství
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
slévárenství	slévárenství	k1gNnSc1	slévárenství
<g/>
,	,	kIx,	,
těžké	těžký	k2eAgNnSc1d1	těžké
strojírenství	strojírenství	k1gNnSc1	strojírenství
Bain	Bain	k1gMnSc1	Bain
<g/>
,	,	kIx,	,
Edgar	Edgar	k1gMnSc1	Edgar
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
–	–	k?	–
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
Martens	Martens	k1gInSc1	Martens
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
(	(	kIx(	(
<g/>
1850	[number]	k4	1850
<g/>
–	–	k?	–
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
Roberts-Austen	Roberts-Austen	k2eAgInSc1d1	Roberts-Austen
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
Chandler	Chandler	k1gInSc1	Chandler
(	(	kIx(	(
<g/>
1843	[number]	k4	1843
<g/>
–	–	k?	–
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
Ledebur	Ledebur	k1gMnSc1	Ledebur
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
(	(	kIx(	(
<g/>
1837	[number]	k4	1837
<g/>
–	–	k?	–
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
Nejvýznamnější	významný	k2eAgNnPc4d3	nejvýznamnější
povolání	povolání	k1gNnPc4	povolání
a	a	k8xC	a
činnosti	činnost	k1gFnPc4	činnost
v	v	k7c6	v
metalurgii	metalurgie	k1gFnSc6	metalurgie
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Metalurgie	metalurgie	k1gFnSc2	metalurgie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
