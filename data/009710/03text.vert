<p>
<s>
Bali	Bali	k6eAd1	Bali
je	být	k5eAaImIp3nS	být
indonéský	indonéský	k2eAgInSc4d1	indonéský
ostrov	ostrov	k1gInSc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejzápadnější	západní	k2eAgMnSc1d3	nejzápadnější
z	z	k7c2	z
Malých	Malý	k1gMnPc2	Malý
Sund	sund	k1gInSc4	sund
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
mezi	mezi	k7c7	mezi
Jávou	Jáva	k1gFnSc7	Jáva
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
Lombokem	Lombok	k1gMnSc7	Lombok
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Bali	Bali	k6eAd1	Bali
tvoří	tvořit	k5eAaImIp3nP	tvořit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
několika	několik	k4yIc7	několik
malými	malý	k2eAgInPc7d1	malý
ostrovy	ostrov	k1gInPc7	ostrov
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
33	[number]	k4	33
indonéských	indonéský	k2eAgFnPc2d1	Indonéská
provincií	provincie	k1gFnPc2	provincie
<g/>
,	,	kIx,	,
hlavním	hlavní	k2eAgNnSc7d1	hlavní
a	a	k8xC	a
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
půlmilionový	půlmilionový	k2eAgInSc1d1	půlmilionový
Denpasar	Denpasar	k1gInSc1	Denpasar
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Bali	Bal	k1gInSc6	Bal
žije	žít	k5eAaImIp3nS	žít
většina	většina	k1gFnSc1	většina
příslušníků	příslušník	k1gMnPc2	příslušník
indonéské	indonéský	k2eAgFnSc2d1	Indonéská
hinduistické	hinduistický	k2eAgFnSc2d1	hinduistická
menšiny	menšina	k1gFnSc2	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Bali	Bal	k1gFnSc2	Bal
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
směs	směs	k1gFnSc4	směs
hinduismu	hinduismus	k1gInSc2	hinduismus
a	a	k8xC	a
místní	místní	k2eAgFnSc2d1	místní
tradiční	tradiční	k2eAgFnSc2d1	tradiční
víry	víra	k1gFnSc2	víra
<g/>
,	,	kIx,	,
hovoříme	hovořit	k5eAaImIp1nP	hovořit
o	o	k7c6	o
balijském	balijský	k2eAgInSc6d1	balijský
hinduismu	hinduismus	k1gInSc6	hinduismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
také	také	k9	také
největší	veliký	k2eAgFnSc7d3	veliký
turistickou	turistický	k2eAgFnSc7d1	turistická
destinací	destinace	k1gFnSc7	destinace
v	v	k7c4	v
Indonésii	Indonésie	k1gFnSc4	Indonésie
<g/>
,	,	kIx,	,
známou	známá	k1gFnSc4	známá
uměním	umění	k1gNnSc7	umění
tance	tanec	k1gInSc2	tanec
<g/>
,	,	kIx,	,
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
sochařstvím	sochařství	k1gNnSc7	sochařství
<g/>
,	,	kIx,	,
malbami	malba	k1gFnPc7	malba
<g/>
,	,	kIx,	,
výrobky	výrobek	k1gInPc1	výrobek
z	z	k7c2	z
kůže	kůže	k1gFnSc2	kůže
a	a	k8xC	a
kovotepectvím	kovotepectví	k1gNnSc7	kovotepectví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Východně	východně	k6eAd1	východně
od	od	k7c2	od
Bali	Bali	k1gNnSc2	Bali
prochází	procházet	k5eAaImIp3nS	procházet
Wallaceova	Wallaceův	k2eAgFnSc1d1	Wallaceova
linie	linie	k1gFnSc1	linie
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
biogeografická	biogeografický	k2eAgFnSc1d1	biogeografická
hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
oblastmi	oblast	k1gFnPc7	oblast
rozšíření	rozšíření	k1gNnSc2	rozšíření
asijských	asijský	k2eAgInPc2d1	asijský
a	a	k8xC	a
australských	australský	k2eAgInPc2d1	australský
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Starověk	starověk	k1gInSc4	starověk
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Bali	Bal	k1gFnSc2	Bal
dorazili	dorazit	k5eAaPmAgMnP	dorazit
první	první	k4xOgMnPc1	první
obyvatelé	obyvatel	k1gMnPc1	obyvatel
už	už	k6eAd1	už
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
př.n.l.	př.n.l.	k?	př.n.l.
Byli	být	k5eAaImAgMnP	být
to	ten	k3xDgNnSc1	ten
australo-asiaté	australosiat	k1gMnPc1	australo-asiat
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
přicházeli	přicházet	k5eAaImAgMnP	přicházet
z	z	k7c2	z
Tchaj-wanu	Tchajan	k1gInSc2	Tchaj-wan
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Nálezy	nález	k1gInPc4	nález
datované	datovaný	k2eAgInPc4d1	datovaný
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
kamenné	kamenný	k2eAgInPc1d1	kamenný
nástroje	nástroj	k1gInPc1	nástroj
<g/>
,	,	kIx,	,
lodě	loď	k1gFnPc1	loď
i	i	k8xC	i
kostry	kostra	k1gFnPc1	kostra
skrčených	skrčený	k2eAgNnPc2d1	skrčené
těl	tělo	k1gNnPc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
také	také	k9	také
objevují	objevovat	k5eAaImIp3nP	objevovat
první	první	k4xOgInPc4	první
psané	psaný	k2eAgInPc4d1	psaný
texty	text	k1gInPc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
vyspělá	vyspělý	k2eAgFnSc1d1	vyspělá
civilizace	civilizace	k1gFnSc1	civilizace
se	s	k7c7	s
zaměřením	zaměření	k1gNnSc7	zaměření
na	na	k7c4	na
pěstování	pěstování	k1gNnSc4	pěstování
rýže	rýže	k1gFnSc2	rýže
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgInSc1d1	zdejší
unikátní	unikátní	k2eAgInSc1d1	unikátní
vodohospodářský	vodohospodářský	k2eAgInSc1d1	vodohospodářský
systém	systém	k1gInSc1	systém
zavlažování	zavlažování	k1gNnSc2	zavlažování
terasovitých	terasovitý	k2eAgFnPc2d1	terasovitá
rýžových	rýžový	k2eAgFnPc2d1	rýžová
polí	pole	k1gFnPc2	pole
-	-	kIx~	-
Subak	Subak	k1gInSc1	Subak
-	-	kIx~	-
společně	společně	k6eAd1	společně
s	s	k7c7	s
několika	několik	k4yIc7	několik
chrámy	chrám	k1gInPc7	chrám
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
zapsán	zapsat	k5eAaPmNgMnS	zapsat
mezi	mezi	k7c4	mezi
lokality	lokalita	k1gFnPc4	lokalita
světového	světový	k2eAgNnSc2d1	světové
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
šířit	šířit	k5eAaImF	šířit
náboženství	náboženství	k1gNnSc1	náboženství
-	-	kIx~	-
hinduismus	hinduismus	k1gInSc1	hinduismus
a	a	k8xC	a
buddhismus	buddhismus	k1gInSc1	buddhismus
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
Bali	Bal	k1gFnSc3	Bal
jediný	jediný	k2eAgInSc4d1	jediný
ostrov	ostrov	k1gInSc4	ostrov
v	v	k7c6	v
Indonésii	Indonésie	k1gFnSc6	Indonésie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
hinduismus	hinduismus	k1gInSc1	hinduismus
většinu	většina	k1gFnSc4	většina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
hovoříme	hovořit	k5eAaImIp1nP	hovořit
o	o	k7c6	o
balijském	balijský	k2eAgInSc6d1	balijský
hinduismu	hinduismus	k1gInSc6	hinduismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vliv	vliv	k1gInSc1	vliv
Jávské	jávský	k2eAgFnSc2d1	jávská
dynastie	dynastie	k1gFnSc2	dynastie
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
-	-	kIx~	-
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
dvanáctého	dvanáctý	k4xOgNnSc2	dvanáctý
století	století	k1gNnSc2	století
začíná	začínat	k5eAaImIp3nS	začínat
mít	mít	k5eAaImF	mít
sílící	sílící	k2eAgInSc1d1	sílící
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
Bali	Bale	k1gFnSc4	Bale
sousední	sousední	k2eAgInSc4d1	sousední
ostrov	ostrov	k1gInSc4	ostrov
Jáva	Jáva	k1gFnSc1	Jáva
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtrnáctém	čtrnáctý	k4xOgInSc6	čtrnáctý
století	století	k1gNnSc6	století
si	se	k3xPyFc3	se
Bali	Bal	k1gMnPc1	Bal
podmaní	podmanit	k5eAaPmIp3nP	podmanit
jávský	jávský	k2eAgMnSc1d1	jávský
vládce	vládce	k1gMnSc1	vládce
Mandžapahit	Mandžapahit	k1gMnSc1	Mandžapahit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobí	způsobit	k5eAaPmIp3nS	způsobit
výrazné	výrazný	k2eAgFnPc4d1	výrazná
společenské	společenský	k2eAgFnPc4d1	společenská
i	i	k8xC	i
kulturní	kulturní	k2eAgFnPc4d1	kulturní
změny	změna	k1gFnPc4	změna
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
zavede	zavést	k5eAaPmIp3nS	zavést
na	na	k7c4	na
Bali	Bale	k1gFnSc4	Bale
kastovní	kastovní	k2eAgInSc4d1	kastovní
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
všichni	všechen	k3xTgMnPc1	všechen
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Bali	Bal	k1gFnSc2	Bal
se	se	k3xPyFc4	se
chtěli	chtít	k5eAaImAgMnP	chtít
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
těmto	tento	k3xDgFnPc3	tento
novotám	novota	k1gFnPc3	novota
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
celé	celý	k2eAgFnPc1d1	celá
skupiny	skupina	k1gFnPc1	skupina
odebírají	odebírat	k5eAaImIp3nP	odebírat
do	do	k7c2	do
odlehlých	odlehlý	k2eAgInPc2d1	odlehlý
pralesů	prales	k1gInPc2	prales
a	a	k8xC	a
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zde	zde	k6eAd1	zde
založily	založit	k5eAaPmAgFnP	založit
nové	nový	k2eAgFnPc4d1	nová
vesnice	vesnice	k1gFnPc4	vesnice
a	a	k8xC	a
ubránily	ubránit	k5eAaPmAgFnP	ubránit
se	se	k3xPyFc4	se
změnám	změna	k1gFnPc3	změna
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
žijí	žít	k5eAaImIp3nP	žít
zcela	zcela	k6eAd1	zcela
odlišným	odlišný	k2eAgInSc7d1	odlišný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
zachovávajícím	zachovávající	k2eAgMnSc7d1	zachovávající
tradiční	tradiční	k2eAgInPc4d1	tradiční
zákony	zákon	k1gInPc4	zákon
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
pobřežních	pobřežní	k2eAgFnPc2d1	pobřežní
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Madžapahita	Madžapahitum	k1gNnSc2	Madžapahitum
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c6	na
Jávě	Jáva	k1gFnSc6	Jáva
vzmáhat	vzmáhat	k5eAaImF	vzmáhat
islám	islám	k1gInSc4	islám
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
doslova	doslova	k6eAd1	doslova
zaplavuje	zaplavovat	k5eAaImIp3nS	zaplavovat
celý	celý	k2eAgInSc4d1	celý
ostrov	ostrov	k1gInSc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tehdejší	tehdejší	k2eAgFnSc3d1	tehdejší
síle	síla	k1gFnSc3	síla
islámu	islám	k1gInSc2	islám
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
s	s	k7c7	s
podivem	podiv	k1gInSc7	podiv
<g/>
,	,	kIx,	,
že	že	k8xS	že
ostrov	ostrov	k1gInSc1	ostrov
Bali	Bal	k1gFnSc2	Bal
zůstal	zůstat	k5eAaPmAgInS	zůstat
výrazně	výrazně	k6eAd1	výrazně
hinduistický	hinduistický	k2eAgInSc1d1	hinduistický
a	a	k8xC	a
buddhistický	buddhistický	k2eAgInSc1d1	buddhistický
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Bali	Bal	k1gFnSc6	Bal
uprchla	uprchnout	k5eAaPmAgFnS	uprchnout
i	i	k9	i
královská	královský	k2eAgFnSc1d1	královská
rodina	rodina	k1gFnSc1	rodina
před	před	k7c7	před
islámem	islám	k1gInSc7	islám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgInSc1	první
kontakt	kontakt	k1gInSc1	kontakt
s	s	k7c7	s
Evropany	Evropan	k1gMnPc7	Evropan
a	a	k8xC	a
ovládnutí	ovládnutí	k1gNnSc3	ovládnutí
Nizozemskem	Nizozemsko	k1gNnSc7	Nizozemsko
===	===	k?	===
</s>
</p>
<p>
<s>
Koncem	koncem	k7c2	koncem
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
na	na	k7c6	na
Bali	Bal	k1gFnSc6	Bal
začínají	začínat	k5eAaImIp3nP	začínat
vyloďovat	vyloďovat	k5eAaImF	vyloďovat
nizozemské	nizozemský	k2eAgFnSc2d1	nizozemská
expedice	expedice	k1gFnSc2	expedice
a	a	k8xC	a
velice	velice	k6eAd1	velice
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
zabydlují	zabydlovat	k5eAaImIp3nP	zabydlovat
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
s	s	k7c7	s
nápadem	nápad	k1gInSc7	nápad
kolonizace	kolonizace	k1gFnSc2	kolonizace
přicházejí	přicházet	k5eAaImIp3nP	přicházet
až	až	k9	až
o	o	k7c4	o
dvě	dva	k4xCgNnPc4	dva
století	století	k1gNnPc4	století
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Přináší	přinášet	k5eAaImIp3nS	přinášet
křesťanství	křesťanství	k1gNnSc4	křesťanství
a	a	k8xC	a
otevírají	otevírat	k5eAaImIp3nP	otevírat
trh	trh	k1gInSc4	trh
s	s	k7c7	s
rýží	rýže	k1gFnSc7	rýže
<g/>
,	,	kIx,	,
opiem	opium	k1gNnSc7	opium
<g/>
,	,	kIx,	,
otroky	otrok	k1gMnPc7	otrok
a	a	k8xC	a
ženami	žena	k1gFnPc7	žena
<g/>
.	.	kIx.	.
</s>
<s>
Trh	trh	k1gInSc1	trh
kvete	kvést	k5eAaImIp3nS	kvést
a	a	k8xC	a
ostrovu	ostrov	k1gInSc3	ostrov
si	se	k3xPyFc3	se
poprvé	poprvé	k6eAd1	poprvé
všímá	všímat	k5eAaImIp3nS	všímat
i	i	k9	i
Británie	Británie	k1gFnSc1	Británie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1816	[number]	k4	1816
Británie	Británie	k1gFnSc1	Británie
ovládne	ovládnout	k5eAaPmIp3nS	ovládnout
okolní	okolní	k2eAgInPc4d1	okolní
ostrovy	ostrov	k1gInPc4	ostrov
a	a	k8xC	a
vzpouzející	vzpouzející	k2eAgNnSc1d1	vzpouzející
se	se	k3xPyFc4	se
Bali	Bali	k1gNnSc1	Bali
přitáhne	přitáhnout	k5eAaPmIp3nS	přitáhnout
pod	pod	k7c4	pod
vlastní	vlastní	k2eAgNnPc4d1	vlastní
křídla	křídlo	k1gNnPc4	křídlo
ozbrojenými	ozbrojený	k2eAgFnPc7d1	ozbrojená
silami	síla	k1gFnPc7	síla
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
kolonii	kolonie	k1gFnSc4	kolonie
Singapur	Singapur	k1gInSc4	Singapur
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
městský	městský	k2eAgInSc1d1	městský
stát	stát	k1gInSc1	stát
Hongkong	Hongkong	k1gInSc1	Hongkong
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
však	však	k9	však
ostrov	ostrov	k1gInSc1	ostrov
vrátí	vrátit	k5eAaPmIp3nS	vrátit
Nizozemsku	Nizozemsko	k1gNnSc3	Nizozemsko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
čím	co	k3yInSc7	co
dál	daleko	k6eAd2	daleko
větší	veliký	k2eAgFnSc4d2	veliký
sílu	síla	k1gFnSc4	síla
na	na	k7c4	na
podmanění	podmanění	k1gNnSc4	podmanění
tradic	tradice	k1gFnPc2	tradice
a	a	k8xC	a
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
okupuje	okupovat	k5eAaBmIp3nS	okupovat
celou	celý	k2eAgFnSc4d1	celá
Indonésii	Indonésie	k1gFnSc4	Indonésie
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nezávislost	nezávislost	k1gFnSc4	nezávislost
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
se	se	k3xPyFc4	se
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
pokusilo	pokusit	k5eAaPmAgNnS	pokusit
získat	získat	k5eAaPmF	získat
zpět	zpět	k6eAd1	zpět
své	svůj	k3xOyFgFnPc4	svůj
kolonie	kolonie	k1gFnPc4	kolonie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zmatku	zmatek	k1gInSc6	zmatek
využil	využít	k5eAaPmAgMnS	využít
první	první	k4xOgMnSc1	první
indonéský	indonéský	k2eAgMnSc1d1	indonéský
prezident	prezident	k1gMnSc1	prezident
Sukarno	Sukarno	k1gNnSc1	Sukarno
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
hbitě	hbitě	k6eAd1	hbitě
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
byla	být	k5eAaImAgFnS	být
Indonésie	Indonésie	k1gFnSc1	Indonésie
uznána	uznat	k5eAaPmNgFnS	uznat
samostatnou	samostatný	k2eAgFnSc7d1	samostatná
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Novodobá	novodobý	k2eAgFnSc1d1	novodobá
historie	historie	k1gFnSc1	historie
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
vybuchla	vybuchnout	k5eAaPmAgFnS	vybuchnout
sopka	sopka	k1gFnSc1	sopka
Gunung	Gunung	k1gMnSc1	Gunung
Agung	Agung	k1gMnSc1	Agung
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
mohutná	mohutný	k2eAgFnSc1d1	mohutná
erupce	erupce	k1gFnSc1	erupce
zabila	zabít	k5eAaPmAgFnS	zabít
tisíce	tisíc	k4xCgInPc4	tisíc
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
zdevastovala	zdevastovat	k5eAaPmAgFnS	zdevastovat
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rozmachu	rozmach	k1gInSc3	rozmach
turistiky	turistika	k1gFnSc2	turistika
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
formuje	formovat	k5eAaImIp3nS	formovat
Bali	Bale	k1gFnSc4	Bale
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jej	on	k3xPp3gMnSc4	on
známe	znát	k5eAaImIp1nP	znát
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Bali	Bali	k6eAd1	Bali
leží	ležet	k5eAaImIp3nS	ležet
východně	východně	k6eAd1	východně
od	od	k7c2	od
ostrova	ostrov	k1gInSc2	ostrov
Jáva	Jáva	k1gFnSc1	Jáva
(	(	kIx(	(
<g/>
odděleno	oddělit	k5eAaPmNgNnS	oddělit
3,2	[number]	k4	3,2
km	km	kA	km
širokým	široký	k2eAgInSc7d1	široký
Balijským	Balijský	k2eAgInSc7d1	Balijský
průlivem	průliv	k1gInSc7	průliv
<g/>
)	)	kIx)	)
a	a	k8xC	a
přibližně	přibližně	k6eAd1	přibližně
8	[number]	k4	8
stupňů	stupeň	k1gInPc2	stupeň
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
rovníku	rovník	k1gInSc2	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ostrova	ostrov	k1gInSc2	ostrov
Lombok	Lombok	k1gInSc4	Lombok
na	na	k7c6	na
východě	východ	k1gInSc6	východ
je	být	k5eAaImIp3nS	být
Bali	Bali	k1gNnSc1	Bali
odděleno	oddělit	k5eAaPmNgNnS	oddělit
Lombockým	Lombocký	k2eAgInSc7d1	Lombocký
průlivem	průliv	k1gInSc7	průliv
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgInPc4d1	severní
břehy	břeh	k1gInPc4	břeh
omývá	omývat	k5eAaImIp3nS	omývat
Jávské	jávský	k2eAgNnSc1d1	jávské
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgInSc1d1	jižní
pak	pak	k6eAd1	pak
Indický	indický	k2eAgInSc1d1	indický
oceán	oceán	k1gInSc1	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
východu	východ	k1gInSc2	východ
k	k	k7c3	k
západu	západ	k1gInSc3	západ
je	být	k5eAaImIp3nS	být
ostrov	ostrov	k1gInSc4	ostrov
asi	asi	k9	asi
153	[number]	k4	153
km	km	kA	km
široký	široký	k2eAgMnSc1d1	široký
<g/>
,	,	kIx,	,
od	od	k7c2	od
severu	sever	k1gInSc2	sever
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
asi	asi	k9	asi
112	[number]	k4	112
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
činí	činit	k5eAaImIp3nS	činit
5632	[number]	k4	5632
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
je	být	k5eAaImIp3nS	být
aktivní	aktivní	k2eAgInSc4d1	aktivní
vulkán	vulkán	k1gInSc4	vulkán
Mount	Mount	k1gMnSc1	Mount
Agung	Agung	k1gMnSc1	Agung
(	(	kIx(	(
<g/>
3142	[number]	k4	3142
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
naposledy	naposledy	k6eAd1	naposledy
soptil	soptit	k5eAaImAgMnS	soptit
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Vulkán	vulkán	k1gInSc1	vulkán
Mount	Mount	k1gMnSc1	Mount
Batur	Batur	k1gMnSc1	Batur
je	být	k5eAaImIp3nS	být
také	také	k9	také
stále	stále	k6eAd1	stále
aktivní	aktivní	k2eAgMnSc1d1	aktivní
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
asi	asi	k9	asi
30	[number]	k4	30
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
prodělal	prodělat	k5eAaPmAgMnS	prodělat
katastrofickou	katastrofický	k2eAgFnSc4d1	katastrofická
erupci	erupce	k1gFnSc4	erupce
-	-	kIx~	-
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
vulkanických	vulkanický	k2eAgFnPc2d1	vulkanická
událostí	událost	k1gFnPc2	událost
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Hory	hora	k1gFnPc1	hora
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
střed	střed	k1gInSc4	střed
ostrova	ostrov	k1gInSc2	ostrov
a	a	k8xC	a
táhnou	táhnout	k5eAaImIp3nP	táhnout
se	se	k3xPyFc4	se
na	na	k7c4	na
východní	východní	k2eAgFnSc4d1	východní
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
krajina	krajina	k1gFnSc1	krajina
sestupuje	sestupovat	k5eAaImIp3nS	sestupovat
do	do	k7c2	do
formy	forma	k1gFnSc2	forma
nánosové	nánosový	k2eAgFnSc2d1	nánosová
roviny	rovina	k1gFnSc2	rovina
zavlažované	zavlažovaný	k2eAgInPc1d1	zavlažovaný
mělkými	mělký	k2eAgFnPc7d1	mělká
řekami	řeka	k1gFnPc7	řeka
<g/>
,	,	kIx,	,
suchými	suchý	k2eAgFnPc7d1	suchá
v	v	k7c4	v
období	období	k1gNnSc4	období
sucha	sucho	k1gNnSc2	sucho
a	a	k8xC	a
rozlévajícími	rozlévající	k2eAgFnPc7d1	rozlévající
se	se	k3xPyFc4	se
v	v	k7c6	v
období	období	k1gNnSc6	období
dešťů	dešť	k1gInPc2	dešť
<g/>
.	.	kIx.	.
</s>
<s>
Nejdelší	dlouhý	k2eAgFnSc7d3	nejdelší
řekou	řeka	k1gFnSc7	řeka
ostrova	ostrov	k1gInSc2	ostrov
je	být	k5eAaImIp3nS	být
Ayung	Ayung	k1gInSc1	Ayung
(	(	kIx(	(
<g/>
75	[number]	k4	75
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Mount	Mounto	k1gNnPc2	Mounto
Agung	Agunga	k1gFnPc2	Agunga
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
ještě	ještě	k9	ještě
sopka	sopka	k1gFnSc1	sopka
Mount	Mount	k1gMnSc1	Mount
Batur	Batur	k1gMnSc1	Batur
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
také	také	k9	také
stále	stále	k6eAd1	stále
aktivní	aktivní	k2eAgMnSc1d1	aktivní
a	a	k8xC	a
neméně	málo	k6eNd2	málo
zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
středu	střed	k1gInSc2	střed
na	na	k7c4	na
východ	východ	k1gInSc4	východ
ostrova	ostrov	k1gInSc2	ostrov
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
pohoří	pohoří	k1gNnSc3	pohoří
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
je	být	k5eAaImIp3nS	být
krajina	krajina	k1gFnSc1	krajina
rovinatá	rovinatý	k2eAgFnSc1d1	rovinatá
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
sezónních	sezónní	k2eAgFnPc2d1	sezónní
řek	řeka	k1gFnPc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
pobřeží	pobřeží	k1gNnSc1	pobřeží
ostrova	ostrov	k1gInSc2	ostrov
Bali	Bal	k1gFnSc2	Bal
omývá	omývat	k5eAaImIp3nS	omývat
Indický	indický	k2eAgInSc1d1	indický
oceán	oceán	k1gInSc1	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
pláží	pláž	k1gFnSc7	pláž
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
ostrova	ostrov	k1gInSc2	ostrov
jsou	být	k5eAaImIp3nP	být
bílo-zlatavé	bílolatavý	k2eAgFnPc1d1	bílo-zlatavý
<g/>
,	,	kIx,	,
v	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
částech	část	k1gFnPc6	část
ostrova	ostrov	k1gInSc2	ostrov
převážně	převážně	k6eAd1	převážně
šedočerné	šedočerný	k2eAgFnPc1d1	šedočerná
vulkanické	vulkanický	k2eAgFnPc1d1	vulkanická
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
jsou	být	k5eAaImIp3nP	být
chrámy	chrám	k1gInPc1	chrám
obklopené	obklopený	k2eAgInPc1d1	obklopený
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
také	také	k9	také
mořské	mořský	k2eAgFnPc1d1	mořská
rezervace	rezervace	k1gFnPc1	rezervace
s	s	k7c7	s
bílým	bílý	k2eAgInSc7d1	bílý
pískem	písek	k1gInSc7	písek
a	a	k8xC	a
spoustou	spousta	k1gFnSc7	spousta
korálových	korálový	k2eAgInPc2d1	korálový
útesů	útes	k1gInPc2	útes
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
lákají	lákat	k5eAaImIp3nP	lákat
k	k	k7c3	k
potápění	potápění	k1gNnSc3	potápění
a	a	k8xC	a
šnorchlování	šnorchlování	k1gNnSc3	šnorchlování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jazyk	jazyk	k1gInSc1	jazyk
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
především	především	k9	především
balijštinou	balijština	k1gFnSc7	balijština
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
odlišná	odlišný	k2eAgFnSc1d1	odlišná
od	od	k7c2	od
oficiálního	oficiální	k2eAgInSc2d1	oficiální
indonéského	indonéský	k2eAgInSc2d1	indonéský
jazyka	jazyk	k1gInSc2	jazyk
Bahasa	Bahas	k1gMnSc4	Bahas
Indonesia	Indonesius	k1gMnSc4	Indonesius
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
na	na	k7c6	na
Lomboku	Lombok	k1gInSc6	Lombok
místní	místní	k2eAgFnSc1d1	místní
kromě	kromě	k7c2	kromě
oficiálního	oficiální	k2eAgInSc2d1	oficiální
jazyka	jazyk	k1gInSc2	jazyk
používají	používat	k5eAaImIp3nP	používat
svůj	svůj	k3xOyFgInSc4	svůj
dialekt	dialekt	k1gInSc4	dialekt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
turistických	turistický	k2eAgFnPc6d1	turistická
oblastech	oblast	k1gFnPc6	oblast
se	se	k3xPyFc4	se
však	však	k9	však
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
domluvíte	domluvit	k5eAaPmIp2nP	domluvit
anglicky	anglicky	k6eAd1	anglicky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bali	Bali	k1gNnPc4	Bali
jako	jako	k8xC	jako
provincie	provincie	k1gFnPc4	provincie
==	==	k?	==
</s>
</p>
<p>
<s>
Provincie	provincie	k1gFnSc1	provincie
Bali	Bal	k1gFnSc2	Bal
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
kromě	kromě	k7c2	kromě
ostrova	ostrov	k1gInSc2	ostrov
Bali	Bali	k1gNnSc2	Bali
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
rozlohou	rozloha	k1gFnSc7	rozloha
i	i	k9	i
počtem	počet	k1gInSc7	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
tvoří	tvořit	k5eAaImIp3nS	tvořit
její	její	k3xOp3gFnSc4	její
rozhodující	rozhodující	k2eAgFnSc4d1	rozhodující
část	část	k1gFnSc4	část
<g/>
,	,	kIx,	,
i	i	k9	i
několik	několik	k4yIc4	několik
menších	malý	k2eAgInPc2d2	menší
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgInPc7d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
jsou	být	k5eAaImIp3nP	být
Nusa	Nus	k2eAgMnSc4d1	Nus
Penida	Penid	k1gMnSc4	Penid
(	(	kIx(	(
<g/>
203	[number]	k4	203
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nusa	Nusa	k1gMnSc1	Nusa
Lembongan	Lembongan	k1gMnSc1	Lembongan
(	(	kIx(	(
<g/>
8	[number]	k4	8
km2	km2	k4	km2
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nusa	Nusa	k1gFnSc1	Nusa
Ceningan	Ceningana	k1gFnPc2	Ceningana
<g/>
,	,	kIx,	,
tyto	tento	k3xDgFnPc1	tento
3	[number]	k4	3
ostrovy	ostrov	k1gInPc7	ostrov
najdeme	najít	k5eAaPmIp1nP	najít
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Bali	Bal	k1gFnSc2	Bal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejbližšími	blízký	k2eAgFnPc7d3	nejbližší
sousedními	sousední	k2eAgFnPc7d1	sousední
provinciemi	provincie	k1gFnPc7	provincie
jsou	být	k5eAaImIp3nP	být
Východní	východní	k2eAgFnSc1d1	východní
Jáva	Jáva	k1gFnSc1	Jáva
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
Západní	západní	k2eAgFnSc1d1	západní
Nusa	Nus	k2eAgFnSc1d1	Nusa
Tenggara	Tenggara	k1gFnSc1	Tenggara
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Měna	měna	k1gFnSc1	měna
==	==	k?	==
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgNnSc1d1	oficiální
indonéskou	indonéský	k2eAgFnSc7d1	Indonéská
měnou	měna	k1gFnSc7	měna
je	být	k5eAaImIp3nS	být
indonéská	indonéský	k2eAgFnSc1d1	Indonéská
rupie	rupie	k1gFnSc1	rupie
(	(	kIx(	(
<g/>
IDR	IDR	kA	IDR
<g/>
,	,	kIx,	,
Rp	Rp	k1gFnSc1	Rp
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cizí	cizí	k2eAgFnPc4d1	cizí
měny	měna	k1gFnPc4	měna
<g/>
,	,	kIx,	,
cestovní	cestovní	k2eAgInPc4d1	cestovní
šeky	šek	k1gInPc4	šek
<g/>
,	,	kIx,	,
směnky	směnka	k1gFnPc4	směnka
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
bankovní	bankovní	k2eAgInPc4d1	bankovní
nástroje	nástroj	k1gInPc4	nástroj
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
dováženy	dovážet	k5eAaImNgFnP	dovážet
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
bez	bez	k7c2	bez
omezení	omezení	k1gNnSc2	omezení
<g/>
.	.	kIx.	.
</s>
<s>
Omezení	omezení	k1gNnSc1	omezení
platí	platit	k5eAaImIp3nS	platit
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
vývoz	vývoz	k1gInSc4	vývoz
rupie	rupie	k1gFnSc2	rupie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
a	a	k8xC	a
náboženství	náboženství	k1gNnSc1	náboženství
==	==	k?	==
</s>
</p>
<p>
<s>
Momentální	momentální	k2eAgFnSc1d1	momentální
populace	populace	k1gFnSc1	populace
ostrova	ostrov	k1gInSc2	ostrov
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
3	[number]	k4	3
miliony	milion	k4xCgInPc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Majoritou	majorita	k1gFnSc7	majorita
jsou	být	k5eAaImIp3nP	být
Indonésané	Indonésan	k1gMnPc1	Indonésan
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
městech	město	k1gNnPc6	město
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
narazit	narazit	k5eAaPmF	narazit
také	také	k9	také
na	na	k7c4	na
čínské	čínský	k2eAgMnPc4d1	čínský
a	a	k8xC	a
indické	indický	k2eAgMnPc4d1	indický
obchodníky	obchodník	k1gMnPc4	obchodník
<g/>
.	.	kIx.	.
</s>
<s>
Permanentně	permanentně	k6eAd1	permanentně
stoupá	stoupat	k5eAaImIp3nS	stoupat
počet	počet	k1gInSc1	počet
západních	západní	k2eAgMnPc2d1	západní
turistů	turist	k1gMnPc2	turist
trvale	trvale	k6eAd1	trvale
se	se	k3xPyFc4	se
usazujících	usazující	k2eAgFnPc2d1	usazující
na	na	k7c4	na
Bali	Bale	k1gFnSc4	Bale
<g/>
.	.	kIx.	.
</s>
<s>
Dohled	dohled	k1gInSc1	dohled
nad	nad	k7c7	nad
demografií	demografie	k1gFnSc7	demografie
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
priorit	priorita	k1gFnPc2	priorita
indonéské	indonéský	k2eAgFnSc2d1	Indonéská
vlády	vláda	k1gFnSc2	vláda
organizující	organizující	k2eAgFnSc4d1	organizující
kampaň	kampaň	k1gFnSc4	kampaň
pro	pro	k7c4	pro
plánované	plánovaný	k2eAgNnSc4d1	plánované
rodičovství	rodičovství	k1gNnSc4	rodičovství
se	s	k7c7	s
sloganem	slogan	k1gInSc7	slogan
"	"	kIx"	"
<g/>
Two	Two	k1gFnSc1	Two
is	is	k?	is
enough	enough	k1gInSc1	enough
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Dvě	dva	k4xCgFnPc1	dva
děti	dítě	k1gFnPc1	dítě
stačí	stačit	k5eAaBmIp3nP	stačit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
akční	akční	k2eAgInSc1d1	akční
program	program	k1gInSc1	program
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
setkává	setkávat	k5eAaImIp3nS	setkávat
s	s	k7c7	s
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
mladé	mladý	k2eAgFnPc1d1	mladá
rodiny	rodina	k1gFnPc1	rodina
mají	mít	k5eAaImIp3nP	mít
momentálně	momentálně	k6eAd1	momentálně
průměrně	průměrně	k6eAd1	průměrně
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
potomky	potomek	k1gMnPc7	potomek
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
předešlých	předešlý	k2eAgFnPc2d1	předešlá
generací	generace	k1gFnPc2	generace
bylo	být	k5eAaImAgNnS	být
běžných	běžný	k2eAgFnPc2d1	běžná
i	i	k8xC	i
9	[number]	k4	9
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Převážná	převážný	k2eAgFnSc1d1	převážná
část	část	k1gFnSc1	část
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
části	část	k1gFnSc6	část
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
také	také	k9	také
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
největší	veliký	k2eAgFnSc4d3	veliký
část	část	k1gFnSc4	část
turistického	turistický	k2eAgInSc2d1	turistický
ruchu	ruch	k1gInSc2	ruch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
indonéským	indonéský	k2eAgNnSc7d1	indonéské
náboženstvím	náboženství	k1gNnSc7	náboženství
islám	islám	k1gInSc1	islám
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
na	na	k7c6	na
Bali	Bal	k1gFnSc6	Bal
až	až	k9	až
90	[number]	k4	90
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
ke	k	k7c3	k
speciální	speciální	k2eAgFnSc3d1	speciální
větvi	větev	k1gFnSc3	větev
balijského	balijský	k2eAgInSc2d1	balijský
hinduismu	hinduismus	k1gInSc2	hinduismus
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
muslimských	muslimský	k2eAgMnPc2d1	muslimský
věřících	věřící	k1gMnPc2	věřící
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
5	[number]	k4	5
%	%	kIx~	%
a	a	k8xC	a
pozvolna	pozvolna	k6eAd1	pozvolna
stoupá	stoupat	k5eAaImIp3nS	stoupat
s	s	k7c7	s
přílivem	příliv	k1gInSc7	příliv
pracovní	pracovní	k2eAgFnSc2d1	pracovní
síly	síla	k1gFnSc2	síla
z	z	k7c2	z
ostrovů	ostrov	k1gInPc2	ostrov
Jáva	Jáva	k1gFnSc1	Jáva
a	a	k8xC	a
Lombok	Lombok	k1gInSc1	Lombok
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
1	[number]	k4	1
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
křesťanství	křesťanství	k1gNnSc4	křesťanství
nebo	nebo	k8xC	nebo
buddhismus	buddhismus	k1gInSc4	buddhismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Správní	správní	k2eAgNnSc4d1	správní
členění	členění	k1gNnSc4	členění
===	===	k?	===
</s>
</p>
<p>
<s>
Provincie	provincie	k1gFnSc1	provincie
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
8	[number]	k4	8
okresů	okres	k1gInPc2	okres
(	(	kIx(	(
<g/>
kabupaten	kabupaten	k2eAgMnSc1d1	kabupaten
<g/>
)	)	kIx)	)
a	a	k8xC	a
1	[number]	k4	1
město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
kota	kot	k1gMnSc4	kot
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Badung	Badung	k1gMnSc1	Badung
</s>
</p>
<p>
<s>
Bangli	Bangl	k1gMnPc1	Bangl
</s>
</p>
<p>
<s>
Buleleng	Buleleng	k1gMnSc1	Buleleng
</s>
</p>
<p>
<s>
Denpasar	Denpasar	k1gInSc1	Denpasar
(	(	kIx(	(
<g/>
město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Gianyar	Gianyar	k1gMnSc1	Gianyar
</s>
</p>
<p>
<s>
Jembrana	Jembrana	k1gFnSc1	Jembrana
</s>
</p>
<p>
<s>
Karangasem	Karangas	k1gInSc7	Karangas
</s>
</p>
<p>
<s>
Klungkung	Klungkung	k1gMnSc1	Klungkung
</s>
</p>
<p>
<s>
Tabanan	Tabanan	k1gMnSc1	Tabanan
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Balijci	Balijce	k1gMnPc1	Balijce
</s>
</p>
<p>
<s>
Barong	Barong	k1gMnSc1	Barong
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bali	Bal	k1gFnSc2	Bal
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Bali	Bal	k1gFnSc2	Bal
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Balijština	Balijština	k1gFnSc1	Balijština
Stručná	stručný	k2eAgFnSc1d1	stručná
česko	česko	k6eAd1	česko
-	-	kIx~	-
balijská	balijský	k2eAgFnSc1d1	balijská
konverzace	konverzace	k1gFnSc1	konverzace
volně	volně	k6eAd1	volně
k	k	k7c3	k
tisku	tisk	k1gInSc3	tisk
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
internetových	internetový	k2eAgFnPc2d1	internetová
stránek	stránka	k1gFnPc2	stránka
nebo	nebo	k8xC	nebo
online	onlinout	k5eAaPmIp3nS	onlinout
i	i	k9	i
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
jazyky	jazyk	k1gInPc7	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
