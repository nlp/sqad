<s>
Dětské	dětský	k2eAgNnSc4d1
hřiště	hřiště	k1gNnSc4
je	být	k5eAaImIp3nS
vymezené	vymezený	k2eAgNnSc1d1
prostranství	prostranství	k1gNnSc1
v	v	k7c6
určitém	určitý	k2eAgInSc6d1
areálu	areál	k1gInSc6
(	(	kIx(
<g/>
často	často	k6eAd1
sídliště	sídliště	k1gNnSc1
<g/>
,	,	kIx,
veřejný	veřejný	k2eAgInSc1d1
park	park	k1gInSc1
<g/>
,	,	kIx,
náměstí	náměstí	k1gNnSc1
<g/>
,	,	kIx,
zahrada	zahrada	k1gFnSc1
<g/>
,	,	kIx,
mateřská	mateřský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
či	či	k8xC
jesle	jesle	k1gFnPc1
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
děti	dítě	k1gFnPc1
volně	volně	k6eAd1
pohybovat	pohybovat	k5eAaImF
a	a	k8xC
využívat	využívat	k5eAaImF,k5eAaPmF
všech	všecek	k3xTgInPc2
statických	statický	k2eAgInPc2d1
nemovitých	movitý	k2eNgInPc2d1
objektů	objekt	k1gInPc2
(	(	kIx(
<g/>
prolézačky	prolézačka	k1gFnPc1
<g/>
,	,	kIx,
pískoviště	pískoviště	k1gNnPc1
<g/>
,	,	kIx,
houpačky	houpačka	k1gFnPc1
<g/>
,	,	kIx,
lavičky	lavička	k1gFnPc1
pro	pro	k7c4
rodiče	rodič	k1gMnPc4
nebo	nebo	k8xC
jiný	jiný	k2eAgInSc1d1
doprovod	doprovod	k1gInSc1
<g/>
,	,	kIx,
veřejné	veřejný	k2eAgInPc1d1
záchodky	záchodek	k1gInPc1
<g/>
,	,	kIx,
pítka	pítko	k1gNnPc1
apod.	apod.	kA
<g/>
)	)	kIx)
pro	pro	k7c4
uspokojení	uspokojení	k1gNnSc4
všech	všecek	k3xTgFnPc2
momentálních	momentální	k2eAgFnPc2d1
nálad	nálada	k1gFnPc2
a	a	k8xC
nápadů	nápad	k1gInPc2
realizovatelných	realizovatelný	k2eAgInPc2d1
v	v	k7c6
rámci	rámec	k1gInSc6
možností	možnost	k1gFnPc2
a	a	k8xC
určení	určení	k1gNnSc2
hřiště	hřiště	k1gNnSc2
<g/>
.	.	kIx.
</s>