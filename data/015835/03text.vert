<s>
Dětské	dětský	k2eAgNnSc4d1
hřiště	hřiště	k1gNnSc4
</s>
<s>
Plastová	plastový	k2eAgFnSc1d1
prolézačka	prolézačka	k1gFnSc1
</s>
<s>
Dětské	dětský	k2eAgNnSc4d1
hřiště	hřiště	k1gNnSc4
je	být	k5eAaImIp3nS
vymezené	vymezený	k2eAgNnSc1d1
prostranství	prostranství	k1gNnSc1
v	v	k7c6
určitém	určitý	k2eAgInSc6d1
areálu	areál	k1gInSc6
(	(	kIx(
<g/>
často	často	k6eAd1
sídliště	sídliště	k1gNnSc1
<g/>
,	,	kIx,
veřejný	veřejný	k2eAgInSc1d1
park	park	k1gInSc1
<g/>
,	,	kIx,
náměstí	náměstí	k1gNnSc1
<g/>
,	,	kIx,
zahrada	zahrada	k1gFnSc1
<g/>
,	,	kIx,
mateřská	mateřský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
či	či	k8xC
jesle	jesle	k1gFnPc1
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
děti	dítě	k1gFnPc1
volně	volně	k6eAd1
pohybovat	pohybovat	k5eAaImF
a	a	k8xC
využívat	využívat	k5eAaImF,k5eAaPmF
všech	všecek	k3xTgInPc2
statických	statický	k2eAgInPc2d1
nemovitých	movitý	k2eNgInPc2d1
objektů	objekt	k1gInPc2
(	(	kIx(
<g/>
prolézačky	prolézačka	k1gFnPc1
<g/>
,	,	kIx,
pískoviště	pískoviště	k1gNnPc1
<g/>
,	,	kIx,
houpačky	houpačka	k1gFnPc1
<g/>
,	,	kIx,
lavičky	lavička	k1gFnPc1
pro	pro	k7c4
rodiče	rodič	k1gMnPc4
nebo	nebo	k8xC
jiný	jiný	k2eAgInSc1d1
doprovod	doprovod	k1gInSc1
<g/>
,	,	kIx,
veřejné	veřejný	k2eAgInPc1d1
záchodky	záchodek	k1gInPc1
<g/>
,	,	kIx,
pítka	pítko	k1gNnPc1
apod.	apod.	kA
<g/>
)	)	kIx)
pro	pro	k7c4
uspokojení	uspokojení	k1gNnSc4
všech	všecek	k3xTgFnPc2
momentálních	momentální	k2eAgFnPc2d1
nálad	nálada	k1gFnPc2
a	a	k8xC
nápadů	nápad	k1gInPc2
realizovatelných	realizovatelný	k2eAgInPc2d1
v	v	k7c6
rámci	rámec	k1gInSc6
možností	možnost	k1gFnPc2
a	a	k8xC
určení	určení	k1gNnSc2
hřiště	hřiště	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svobodný	svobodný	k2eAgInSc1d1
pohyb	pohyb	k1gInSc1
po	po	k7c6
prostranství	prostranství	k1gNnSc6
a	a	k8xC
možnost	možnost	k1gFnSc4
využívání	využívání	k1gNnSc2
veškerého	veškerý	k3xTgNnSc2
vybavení	vybavení	k1gNnSc2
a	a	k8xC
součástí	součást	k1gFnSc7
hřiště	hřiště	k1gNnSc1
je	být	k5eAaImIp3nS
samozřejmostí	samozřejmost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hřiště	hřiště	k1gNnSc1
bývá	bývat	k5eAaImIp3nS
někdy	někdy	k6eAd1
ohraničeno	ohraničit	k5eAaPmNgNnS
drátěným	drátěný	k2eAgInSc7d1
<g/>
,	,	kIx,
dřevěným	dřevěný	k2eAgInSc7d1
či	či	k8xC
jiným	jiný	k2eAgInSc7d1
plotem	plot	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umisťuje	umisťovat	k5eAaImIp3nS
se	se	k3xPyFc4
co	co	k9
nejdále	daleko	k6eAd3
od	od	k7c2
komunikaci	komunikace	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
nehrozilo	hrozit	k5eNaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
dítě	dítě	k1gNnSc1
vběhne	vběhnout	k5eAaPmIp3nS
do	do	k7c2
silnice	silnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hřiště	hřiště	k1gNnPc1
jsou	být	k5eAaImIp3nP
stavěna	stavit	k5eAaImNgNnP
na	na	k7c4
slunném	slunný	k2eAgNnSc6d1
<g/>
,	,	kIx,
rovném	rovný	k2eAgNnSc6d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
mírně	mírně	k6eAd1
členitém	členitý	k2eAgInSc6d1
terénu	terén	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hřiště	hřiště	k1gNnSc1
využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
často	často	k6eAd1
také	také	k9
travnatých	travnatý	k2eAgFnPc2d1
ploch	plocha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Dětská	dětský	k2eAgNnPc1d1
hřiště	hřiště	k1gNnPc1
se	se	k3xPyFc4
vyrábějí	vyrábět	k5eAaImIp3nP
ze	z	k7c2
všech	všecek	k3xTgInPc2
možných	možný	k2eAgInPc2d1
materiálů	materiál	k1gInPc2
<g/>
,	,	kIx,
ať	ať	k8xS,k8xC
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
různé	různý	k2eAgFnPc4d1
dřeviny	dřevina	k1gFnPc4
(	(	kIx(
<g/>
smrk	smrk	k1gInSc1
<g/>
,	,	kIx,
akát	akát	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
kovy	kov	k1gInPc1
<g/>
,	,	kIx,
plasty	plast	k1gInPc1
<g/>
,	,	kIx,
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
se	se	k3xPyFc4
dokonce	dokonce	k9
i	i	k9
různý	různý	k2eAgInSc4d1
odpad	odpad	k1gInSc4
např.	např.	kA
ojeté	ojetý	k2eAgFnSc2d1
pneumatiky	pneumatika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
starší	starý	k2eAgFnPc4d2
děti	dítě	k1gFnPc4
je	být	k5eAaImIp3nS
určeno	určen	k2eAgNnSc1d1
robinsonádní	robinsonádní	k2eAgNnSc1d1
hřiště	hřiště	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Součásti	součást	k1gFnPc1
a	a	k8xC
vybavení	vybavení	k1gNnSc1
dětského	dětský	k2eAgNnSc2d1
hřiště	hřiště	k1gNnSc2
</s>
<s>
Dřevěná	dřevěný	k2eAgFnSc1d1
konstrukce	konstrukce	k1gFnSc1
se	s	k7c7
skluzavkou	skluzavka	k1gFnSc7
a	a	k8xC
závěsnou	závěsný	k2eAgFnSc7d1
houpačkou	houpačka	k1gFnSc7
z	z	k7c2
plastu	plast	k1gInSc2
</s>
<s>
pískoviště	pískoviště	k1gNnPc1
</s>
<s>
lavičky	lavička	k1gFnPc1
</s>
<s>
prolézačky	prolézačka	k1gFnPc1
</s>
<s>
skluzavky	skluzavka	k1gFnPc1
</s>
<s>
houpačky	houpačka	k1gFnPc1
</s>
<s>
houpačky	houpačka	k1gFnPc1
nízké	nízký	k2eAgFnSc2d1
ukotvené	ukotvený	k2eAgFnSc2d1
</s>
<s>
basketbalové	basketbalový	k2eAgInPc4d1
koše	koš	k1gInPc4
</s>
<s>
malé	malý	k2eAgFnPc1d1
branky	branka	k1gFnPc1
</s>
<s>
provazové	provazový	k2eAgFnPc1d1
atrakce	atrakce	k1gFnPc1
</s>
<s>
speciální	speciální	k2eAgFnPc1d1
atrakce	atrakce	k1gFnPc1
(	(	kIx(
<g/>
výkyvné	výkyvný	k2eAgFnPc1d1
sedačky	sedačka	k1gFnPc1
na	na	k7c4
pérách	pérách	k?
apod.	apod.	kA
<g/>
)	)	kIx)
</s>
<s>
vodní	vodní	k2eAgInPc1d1
prvky	prvek	k1gInPc1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
brouzdaliště	brouzdaliště	k1gNnSc1
<g/>
,	,	kIx,
sprchy	sprcha	k1gFnPc1
<g/>
,	,	kIx,
…	…	k?
</s>
<s>
lehký	lehký	k2eAgInSc1d1
přístřešek	přístřešek	k1gInSc1
(	(	kIx(
<g/>
jako	jako	k8xS,k8xC
ochrana	ochrana	k1gFnSc1
před	před	k7c7
deštěm	dešť	k1gInSc7
nebo	nebo	k8xC
sluníčkem	sluníčko	k1gNnSc7
<g/>
)	)	kIx)
</s>
<s>
kolotoče	kolotoč	k1gInPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
dětské	dětský	k2eAgFnSc2d1
hřiště	hřiště	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4056237-2	4056237-2	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
14043	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85103387	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85103387	#num#	k4
</s>
