<s>
Karel	Karel	k1gMnSc1	Karel
V.	V.	kA	V.
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1500	[number]	k4	1500
v	v	k7c6	v
Gentu	Gento	k1gNnSc6	Gento
-	-	kIx~	-
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1558	[number]	k4	1558
v	v	k7c6	v
San	San	k1gFnSc6	San
Jerónimo	Jerónima	k1gFnSc5	Jerónima
de	de	k?	de
Yuste	Yust	k1gMnSc5	Yust
<g/>
,	,	kIx,	,
Extremadura	Extremadura	k1gFnSc1	Extremadura
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
králem	král	k1gMnSc7	král
španělským	španělský	k2eAgFnPc3d1	španělská
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
Carlos	Carlos	k1gMnSc1	Carlos
I.	I.	kA	I.
<g/>
,	,	kIx,	,
1516	[number]	k4	1516
<g/>
-	-	kIx~	-
<g/>
1556	[number]	k4	1556
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
římským	římský	k2eAgMnSc7d1	římský
králem	král	k1gMnSc7	král
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
