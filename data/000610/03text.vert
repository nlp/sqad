<s>
Karel	Karel	k1gMnSc1	Karel
V.	V.	kA	V.
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1500	[number]	k4	1500
v	v	k7c6	v
Gentu	Gento	k1gNnSc6	Gento
-	-	kIx~	-
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1558	[number]	k4	1558
v	v	k7c6	v
San	San	k1gFnSc6	San
Jerónimo	Jerónima	k1gFnSc5	Jerónima
de	de	k?	de
Yuste	Yust	k1gMnSc5	Yust
<g/>
,	,	kIx,	,
Extremadura	Extremadura	k1gFnSc1	Extremadura
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
králem	král	k1gMnSc7	král
španělským	španělský	k2eAgFnPc3d1	španělská
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
Carlos	Carlos	k1gMnSc1	Carlos
I.	I.	kA	I.
<g/>
,	,	kIx,	,
1516	[number]	k4	1516
<g/>
-	-	kIx~	-
<g/>
1556	[number]	k4	1556
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
římským	římský	k2eAgMnSc7d1	římský
králem	král	k1gMnSc7	král
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
od	od	k7c2	od
1519	[number]	k4	1519
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
císařem	císař	k1gMnSc7	císař
římským	římský	k2eAgInSc7d1	římský
(	(	kIx(	(
<g/>
1531	[number]	k4	1531
<g/>
-	-	kIx~	-
<g/>
1556	[number]	k4	1556
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vévodou	vévoda	k1gMnSc7	vévoda
burgundským	burgundský	k2eAgInSc7d1	burgundský
(	(	kIx(	(
<g/>
1506	[number]	k4	1506
<g/>
-	-	kIx~	-
<g/>
1556	[number]	k4	1556
<g/>
)	)	kIx)	)
a	a	k8xC	a
arcivévodou	arcivévoda	k1gMnSc7	arcivévoda
rakouským	rakouský	k2eAgInSc7d1	rakouský
(	(	kIx(	(
<g/>
1519	[number]	k4	1519
<g/>
-	-	kIx~	-
<g/>
1521	[number]	k4	1521
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1556	[number]	k4	1556
postoupil	postoupit	k5eAaPmAgInS	postoupit
svému	svůj	k3xOyFgMnSc3	svůj
synu	syn	k1gMnSc3	syn
Filipovi	Filip	k1gMnSc3	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
španělský	španělský	k2eAgInSc4d1	španělský
trůn	trůn	k1gInSc4	trůn
a	a	k8xC	a
svému	svůj	k3xOyFgMnSc3	svůj
bratru	bratr	k1gMnSc3	bratr
Ferdinandovi	Ferdinand	k1gMnSc3	Ferdinand
císařskou	císařský	k2eAgFnSc4d1	císařská
hodnost	hodnost	k1gFnSc4	hodnost
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
novým	nový	k2eAgFnPc3d1	nová
državám	država	k1gFnPc3	država
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
získalo	získat	k5eAaPmAgNnS	získat
Španělsko	Španělsko	k1gNnSc1	Španělsko
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgMnS	moct
Karel	Karel	k1gMnSc1	Karel
V.	V.	kA	V.
prohlašovat	prohlašovat	k5eAaImF	prohlašovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nad	nad	k7c7	nad
mojí	můj	k3xOp1gFnSc7	můj
říší	říš	k1gFnSc7	říš
Slunce	slunce	k1gNnSc2	slunce
nezapadá	zapadat	k5eNaImIp3nS	zapadat
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
En	En	k1gFnSc1	En
mi	já	k3xPp1nSc3	já
imperio	imperio	k6eAd1	imperio
<g/>
,	,	kIx,	,
no	no	k9	no
se	se	k3xPyFc4	se
pone	ponat	k5eAaPmIp3nS	ponat
nunca	nuncum	k1gNnSc2	nuncum
el	ela	k1gFnPc2	ela
sol	sol	k1gNnSc2	sol
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
)	)	kIx)	)
Byl	být	k5eAaImAgMnS	být
posledním	poslední	k2eAgMnSc7d1	poslední
císařem	císař	k1gMnSc7	císař
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
korunován	korunovat	k5eAaBmNgMnS	korunovat
papežem	papež	k1gMnSc7	papež
<g/>
.	.	kIx.	.
</s>
<s>
Korunovace	korunovace	k1gFnSc1	korunovace
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
provedl	provést	k5eAaPmAgMnS	provést
Klement	Klement	k1gMnSc1	Klement
VII	VII	kA	VII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
neproběhla	proběhnout	k5eNaPmAgFnS	proběhnout
však	však	k9	však
již	již	k6eAd1	již
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
bylo	být	k5eAaImAgNnS	být
nezbytné	nezbytný	k2eAgNnSc1d1	nezbytný
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
Bologni	Bologna	k1gFnSc6	Bologna
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
V.	V.	kA	V.
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Habsburků	Habsburk	k1gMnPc2	Habsburk
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
španělským	španělský	k2eAgMnSc7d1	španělský
králem	král	k1gMnSc7	král
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
druhorozeným	druhorozený	k2eAgNnSc7d1	druhorozené
dítětem	dítě	k1gNnSc7	dítě
Filipa	Filip	k1gMnSc2	Filip
Sličného	sličný	k2eAgMnSc2d1	sličný
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc2	syn
císaře	císař	k1gMnSc2	císař
Maxmiliána	Maxmilián	k1gMnSc2	Maxmilián
I.	I.	kA	I.
<g/>
,	,	kIx,	,
a	a	k8xC	a
španělské	španělský	k2eAgFnSc2d1	španělská
infantky	infantka	k1gFnSc2	infantka
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgFnSc2d2	pozdější
kastilské	kastilský	k2eAgFnSc2d1	Kastilská
královny	královna	k1gFnSc2	královna
Jany	Jana	k1gFnSc2	Jana
I.	I.	kA	I.
Kastilské	kastilský	k2eAgFnPc4d1	Kastilská
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
předčasné	předčasný	k2eAgFnSc6d1	předčasná
smrti	smrt	k1gFnSc6	smrt
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
následovalo	následovat	k5eAaImAgNnS	následovat
propuknutí	propuknutí	k1gNnSc1	propuknutí
duševní	duševní	k2eAgFnSc2d1	duševní
choroby	choroba	k1gFnSc2	choroba
u	u	k7c2	u
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
dědicem	dědic	k1gMnSc7	dědic
burgundského	burgundský	k2eAgNnSc2d1	burgundské
vévodství	vévodství	k1gNnSc2	vévodství
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
především	především	k9	především
sedmnáct	sedmnáct	k4xCc1	sedmnáct
nizozemských	nizozemský	k2eAgFnPc2d1	nizozemská
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hospodářsky	hospodářsky	k6eAd1	hospodářsky
i	i	k8xC	i
kulturně	kulturně	k6eAd1	kulturně
vyspělém	vyspělý	k2eAgNnSc6d1	vyspělé
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
<g/>
,	,	kIx,	,
ve	v	k7c6	v
městech	město	k1gNnPc6	město
Gentu	Gent	k1gInSc6	Gent
a	a	k8xC	a
Bruselu	Brusel	k1gInSc6	Brusel
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
také	také	k6eAd1	také
vychován	vychovat	k5eAaPmNgMnS	vychovat
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
svojí	svůj	k3xOyFgFnSc2	svůj
tety	teta	k1gFnSc2	teta
Markéty	Markéta	k1gFnSc2	Markéta
Rakouské	rakouský	k2eAgFnSc2d1	rakouská
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1516	[number]	k4	1516
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Karlův	Karlův	k2eAgMnSc1d1	Karlův
dědeček	dědeček	k1gMnSc1	dědeček
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Aragonský	aragonský	k2eAgMnSc1d1	aragonský
a	a	k8xC	a
mladý	mladý	k2eAgMnSc1d1	mladý
Habsburk	Habsburk	k1gMnSc1	Habsburk
byl	být	k5eAaImAgMnS	být
povolán	povolat	k5eAaPmNgMnS	povolat
na	na	k7c4	na
španělský	španělský	k2eAgInSc4d1	španělský
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
si	se	k3xPyFc3	se
zesnulý	zesnulý	k1gMnSc1	zesnulý
král	král	k1gMnSc1	král
nástup	nástup	k1gInSc4	nástup
Habsburků	Habsburk	k1gMnPc2	Habsburk
nepřál	přát	k5eNaImAgMnS	přát
<g/>
.	.	kIx.	.
</s>
<s>
Skutečnou	skutečný	k2eAgFnSc7d1	skutečná
dědičkou	dědička	k1gFnSc7	dědička
Kastilie	Kastilie	k1gFnSc2	Kastilie
a	a	k8xC	a
Aragonu	Aragon	k1gInSc3	Aragon
byla	být	k5eAaImAgFnS	být
sice	sice	k8xC	sice
Karlova	Karlův	k2eAgFnSc1d1	Karlova
matka	matka	k1gFnSc1	matka
Jana	Jan	k1gMnSc2	Jan
(	(	kIx(	(
<g/>
Juana	Juan	k1gMnSc2	Juan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
však	však	k9	však
byla	být	k5eAaImAgFnS	být
kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgInSc3	svůj
duševnímu	duševní	k2eAgInSc3d1	duševní
stavu	stav	k1gInSc3	stav
již	již	k6eAd1	již
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
internovaná	internovaný	k2eAgFnSc1d1	internovaná
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Maxmiliána	Maxmilián	k1gMnSc2	Maxmilián
I.	I.	kA	I.
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Karel	Karel	k1gMnSc1	Karel
zvolen	zvolit	k5eAaPmNgMnS	zvolit
římským	římský	k2eAgMnSc7d1	římský
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
zdědil	zdědit	k5eAaPmAgInS	zdědit
tzv.	tzv.	kA	tzv.
dědičné	dědičný	k2eAgFnSc2d1	dědičná
habsburské	habsburský	k2eAgFnSc2d1	habsburská
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	on	k3xPp3gNnSc2	on
mládí	mládí	k1gNnSc2	mládí
byli	být	k5eAaImAgMnP	být
jeho	jeho	k3xOp3gMnPc7	jeho
vychovateli	vychovatel	k1gMnPc7	vychovatel
Vilém	Vilém	k1gMnSc1	Vilém
z	z	k7c2	z
Croy	Croa	k1gFnSc2	Croa
<g/>
,	,	kIx,	,
pán	pán	k1gMnSc1	pán
z	z	k7c2	z
Chiévres	Chiévresa	k1gFnPc2	Chiévresa
<g/>
,	,	kIx,	,
a	a	k8xC	a
Adrian	Adrian	k1gMnSc1	Adrian
Floriszoon	Floriszoon	k1gMnSc1	Floriszoon
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
papež	papež	k1gMnSc1	papež
Hadrián	Hadrián	k1gMnSc1	Hadrián
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1515	[number]	k4	1515
byl	být	k5eAaImAgMnS	být
Karel	Karel	k1gMnSc1	Karel
prohlášen	prohlášen	k2eAgMnSc1d1	prohlášen
formálně	formálně	k6eAd1	formálně
za	za	k7c4	za
plnoletého	plnoletý	k2eAgMnSc4d1	plnoletý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
za	za	k7c4	za
něho	on	k3xPp3gMnSc4	on
vládl	vládnout	k5eAaImAgMnS	vládnout
Croy	Croy	k1gInPc7	Croy
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
domáhat	domáhat	k5eAaImF	domáhat
na	na	k7c6	na
svém	svůj	k3xOyFgMnSc6	svůj
dědovi	děd	k1gMnSc6	děd
<g/>
,	,	kIx,	,
králi	král	k1gMnSc6	král
Ferdinandovi	Ferdinand	k1gMnSc6	Ferdinand
<g/>
,	,	kIx,	,
kastilské	kastilský	k2eAgFnSc2d1	Kastilská
koruny	koruna	k1gFnSc2	koruna
jakožto	jakožto	k8xS	jakožto
dědictví	dědictví	k1gNnSc1	dědictví
své	svůj	k3xOyFgFnSc2	svůj
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
než	než	k8xS	než
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
dohodě	dohoda	k1gFnSc3	dohoda
<g/>
,	,	kIx,	,
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
roku	rok	k1gInSc2	rok
1516	[number]	k4	1516
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
poté	poté	k6eAd1	poté
zdědil	zdědit	k5eAaPmAgMnS	zdědit
korunu	koruna	k1gFnSc4	koruna
kastilskou	kastilský	k2eAgFnSc4d1	Kastilská
i	i	k8xC	i
aragonskou	aragonský	k2eAgFnSc4d1	Aragonská
se	s	k7c7	s
vším	všecek	k3xTgNnSc7	všecek
příslušenstvím	příslušenství	k1gNnSc7	příslušenství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
Kastilii	Kastilie	k1gFnSc6	Kastilie
byl	být	k5eAaImAgInS	být
dlouho	dlouho	k6eAd1	dlouho
formálně	formálně	k6eAd1	formálně
pouze	pouze	k6eAd1	pouze
spoluvládcem	spoluvládce	k1gMnSc7	spoluvládce
své	svůj	k3xOyFgFnSc2	svůj
choromyslné	choromyslný	k2eAgFnSc2d1	choromyslná
matky	matka	k1gFnSc2	matka
(	(	kIx(	(
<g/>
Jana	Jan	k1gMnSc2	Jan
zemřela	zemřít	k5eAaPmAgFnS	zemřít
roku	rok	k1gInSc2	rok
1555	[number]	k4	1555
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Panovníkem	panovník	k1gMnSc7	panovník
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jako	jako	k9	jako
člověk	člověk	k1gMnSc1	člověk
nehotový	hotový	k2eNgMnSc1d1	nehotový
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
od	od	k7c2	od
mládí	mládí	k1gNnSc2	mládí
slabého	slabý	k2eAgNnSc2d1	slabé
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
zvolna	zvolna	k6eAd1	zvolna
dospíval	dospívat	k5eAaImAgMnS	dospívat
na	na	k7c6	na
duchu	duch	k1gMnSc6	duch
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
povaha	povaha	k1gFnSc1	povaha
se	se	k3xPyFc4	se
teprve	teprve	k6eAd1	teprve
během	během	k7c2	během
času	čas	k1gInSc2	čas
ustálila	ustálit	k5eAaPmAgFnS	ustálit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1517	[number]	k4	1517
přijel	přijet	k5eAaPmAgMnS	přijet
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
a	a	k8xC	a
hned	hned	k6eAd1	hned
vzbudil	vzbudit	k5eAaPmAgInS	vzbudit
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
nevoli	nevole	k1gFnSc4	nevole
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
všude	všude	k6eAd1	všude
dával	dávat	k5eAaImAgMnS	dávat
přednost	přednost	k1gFnSc4	přednost
Nizozemcům	Nizozemec	k1gMnPc3	Nizozemec
před	před	k7c7	před
Španěly	Španěl	k1gMnPc7	Španěl
<g/>
.	.	kIx.	.
</s>
<s>
Nespokojenost	nespokojenost	k1gFnSc1	nespokojenost
živená	živený	k2eAgFnSc1d1	živená
také	také	k9	také
jinými	jiný	k2eAgFnPc7d1	jiná
příčinami	příčina	k1gFnPc7	příčina
stále	stále	k6eAd1	stále
rostla	růst	k5eAaImAgFnS	růst
<g/>
,	,	kIx,	,
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1520	[number]	k4	1520
<g/>
,	,	kIx,	,
právě	právě	k9	právě
když	když	k8xS	když
Karel	Karel	k1gMnSc1	Karel
odjížděl	odjíždět	k5eAaImAgMnS	odjíždět
do	do	k7c2	do
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnPc1d1	římská
<g/>
,	,	kIx,	,
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
povstání	povstání	k1gNnSc1	povstání
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
záhy	záhy	k6eAd1	záhy
obrátilo	obrátit	k5eAaPmAgNnS	obrátit
v	v	k7c4	v
boj	boj	k1gInSc4	boj
měst	město	k1gNnPc2	město
proti	proti	k7c3	proti
vyšší	vysoký	k2eAgFnSc3d2	vyšší
šlechtě	šlechta	k1gFnSc3	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
se	se	k3xPyFc4	se
do	do	k7c2	do
svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
vypravil	vypravit	k5eAaPmAgMnS	vypravit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1519	[number]	k4	1519
zemřel	zemřít	k5eAaPmAgMnS	zemřít
jeho	jeho	k3xOp3gMnSc1	jeho
děd	děd	k1gMnSc1	děd
císař	císař	k1gMnSc1	císař
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
I.	I.	kA	I.
a	a	k8xC	a
kurfiřtové	kurfiřt	k1gMnPc1	kurfiřt
přes	přes	k7c4	přes
všechny	všechen	k3xTgFnPc4	všechen
snahy	snaha	k1gFnPc1	snaha
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
Františka	František	k1gMnSc2	František
I.	I.	kA	I.
o	o	k7c4	o
vlastní	vlastní	k2eAgFnSc4d1	vlastní
volbu	volba	k1gFnSc4	volba
zvolili	zvolit	k5eAaPmAgMnP	zvolit
císařem	císař	k1gMnSc7	císař
Karla	Karel	k1gMnSc2	Karel
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1519	[number]	k4	1519
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
i	i	k9	i
Karel	Karel	k1gMnSc1	Karel
musel	muset	k5eAaImAgMnS	muset
přinést	přinést	k5eAaPmF	přinést
značné	značný	k2eAgFnPc4d1	značná
oběti	oběť	k1gFnPc4	oběť
<g/>
;	;	kIx,	;
sama	sám	k3xTgFnSc1	sám
kandidatura	kandidatura	k1gFnSc1	kandidatura
-	-	kIx~	-
též	též	k9	též
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
příčin	příčina	k1gFnPc2	příčina
nespokojenosti	nespokojenost	k1gFnSc2	nespokojenost
Španělů	Španěl	k1gMnPc2	Španěl
-	-	kIx~	-
jej	on	k3xPp3gMnSc4	on
stála	stát	k5eAaImAgFnS	stát
velké	velký	k2eAgInPc4d1	velký
peníze	peníz	k1gInPc4	peníz
a	a	k8xC	a
uvedla	uvést	k5eAaPmAgFnS	uvést
jej	on	k3xPp3gInSc4	on
do	do	k7c2	do
dluhů	dluh	k1gInPc2	dluh
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
musel	muset	k5eAaImAgMnS	muset
přistoupit	přistoupit	k5eAaPmF	přistoupit
na	na	k7c4	na
volební	volební	k2eAgFnSc4d1	volební
kapitulaci	kapitulace	k1gFnSc4	kapitulace
toho	ten	k3xDgInSc2	ten
smyslu	smysl	k1gInSc2	smysl
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
důležitějších	důležitý	k2eAgFnPc6d2	důležitější
věcech	věc	k1gFnPc6	věc
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
řídit	řídit	k5eAaImF	řídit
radou	rada	k1gFnSc7	rada
kurfiřtů	kurfiřt	k1gMnPc2	kurfiřt
a	a	k8xC	a
že	že	k8xS	že
obnoví	obnovit	k5eAaPmIp3nS	obnovit
říšský	říšský	k2eAgInSc4d1	říšský
regiment	regiment	k1gInSc4	regiment
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1520	[number]	k4	1520
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Cáchách	Cáchy	k1gFnPc6	Cáchy
korunován	korunován	k2eAgInSc4d1	korunován
a	a	k8xC	a
tituloval	titulovat	k5eAaImAgMnS	titulovat
se	se	k3xPyFc4	se
hned	hned	k6eAd1	hned
císařem	císař	k1gMnSc7	císař
<g/>
.	.	kIx.	.
</s>
<s>
Veřejné	veřejný	k2eAgNnSc1d1	veřejné
mínění	mínění	k1gNnSc1	mínění
ve	v	k7c6	v
svaté	svatý	k2eAgFnSc6d1	svatá
říši	říš	k1gFnSc6	říš
jej	on	k3xPp3gMnSc4	on
přijalo	přijmout	k5eAaPmAgNnS	přijmout
příznivě	příznivě	k6eAd1	příznivě
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ani	ani	k8xC	ani
neuměl	umět	k5eNaImAgMnS	umět
německy	německy	k6eAd1	německy
a	a	k8xC	a
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
docela	docela	k6eAd1	docela
dobře	dobře	k6eAd1	dobře
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
cizince	cizinec	k1gMnPc4	cizinec
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k6eAd1	zejména
zástupci	zástupce	k1gMnPc7	zástupce
nového	nový	k2eAgNnSc2d1	nové
náboženského	náboženský	k2eAgNnSc2d1	náboženské
hnutí	hnutí	k1gNnSc2	hnutí
do	do	k7c2	do
něho	on	k3xPp3gNnSc2	on
kladli	klást	k5eAaImAgMnP	klást
velké	velký	k2eAgFnPc4d1	velká
naděje	naděje	k1gFnPc4	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
chopí	chopit	k5eAaPmIp3nS	chopit
reformace	reformace	k1gFnSc1	reformace
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
příznivou	příznivý	k2eAgFnSc4d1	příznivá
okolnost	okolnost	k1gFnSc4	okolnost
se	se	k3xPyFc4	se
pokládalo	pokládat	k5eAaImAgNnS	pokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
papež	papež	k1gMnSc1	papež
Lev	Lev	k1gMnSc1	Lev
X.	X.	kA	X.
nepřál	přát	k5eNaImAgMnS	přát
jeho	jeho	k3xOp3gFnSc3	jeho
kandidatuře	kandidatura	k1gFnSc3	kandidatura
na	na	k7c4	na
římský	římský	k2eAgInSc4d1	římský
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
Karel	Karel	k1gMnSc1	Karel
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterého	který	k3yQgMnSc4	který
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
věci	věc	k1gFnSc6	věc
vliv	vliv	k1gInSc4	vliv
jeho	jeho	k3xOp3gMnSc1	jeho
zpovědník	zpovědník	k1gMnSc1	zpovědník
<g/>
,	,	kIx,	,
františkán	františkán	k1gMnSc1	františkán
Glapion	Glapion	k1gInSc1	Glapion
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
sice	sice	k8xC	sice
přítelem	přítel	k1gMnSc7	přítel
reformace	reformace	k1gFnSc2	reformace
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
by	by	kYmCp3nP	by
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
s	s	k7c7	s
omezením	omezení	k1gNnSc7	omezení
papežské	papežský	k2eAgFnSc2d1	Papežská
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nelíbilo	líbit	k5eNaImAgNnS	líbit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zpochybňování	zpochybňování	k1gNnSc2	zpochybňování
dogmatu	dogma	k1gNnSc2	dogma
a	a	k8xC	a
reforma	reforma	k1gFnSc1	reforma
prováděná	prováděný	k2eAgFnSc1d1	prováděná
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
nižších	nízký	k2eAgFnPc2d2	nižší
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
,	,	kIx,	,
a	a	k8xC	a
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
nepřátelské	přátelský	k2eNgNnSc4d1	nepřátelské
postavení	postavení	k1gNnSc4	postavení
vůči	vůči	k7c3	vůči
Lutherovu	Lutherův	k2eAgNnSc3d1	Lutherovo
reformnímu	reformní	k2eAgNnSc3d1	reformní
hnutí	hnutí	k1gNnSc3	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
negativní	negativní	k2eAgInSc4d1	negativní
dopad	dopad	k1gInSc4	dopad
jak	jak	k8xC	jak
pro	pro	k7c4	pro
něho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
pro	pro	k7c4	pro
říši	říše	k1gFnSc4	říše
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
prvním	první	k4xOgInSc6	první
sněmu	sněm	k1gInSc6	sněm
ve	v	k7c4	v
Wormsu	Wormsa	k1gFnSc4	Wormsa
dal	dát	k5eAaPmAgMnS	dát
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1521	[number]	k4	1521
přijmout	přijmout	k5eAaPmF	přijmout
edikt	edikt	k1gInSc4	edikt
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
uvalil	uvalit	k5eAaPmAgInS	uvalit
na	na	k7c4	na
Luthera	Luther	k1gMnSc4	Luther
říšskou	říšský	k2eAgFnSc4d1	říšská
klatbu	klatba	k1gFnSc4	klatba
a	a	k8xC	a
šíření	šíření	k1gNnSc1	šíření
jeho	jeho	k3xOp3gNnSc2	jeho
učení	učení	k1gNnSc2	učení
bylo	být	k5eAaImAgNnS	být
zapovězeno	zapovědět	k5eAaPmNgNnS	zapovědět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
témž	týž	k3xTgInSc6	týž
sněmu	sněm	k1gInSc6	sněm
uspořádal	uspořádat	k5eAaPmAgMnS	uspořádat
Karel	Karel	k1gMnSc1	Karel
také	také	k9	také
některé	některý	k3yIgFnPc4	některý
věci	věc	k1gFnPc4	věc
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
správy	správa	k1gFnSc2	správa
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
odevzdal	odevzdat	k5eAaPmAgMnS	odevzdat
alpské	alpský	k2eAgFnPc4d1	alpská
země	zem	k1gFnPc4	zem
svému	svůj	k3xOyFgMnSc3	svůj
bratru	bratr	k1gMnSc3	bratr
Ferdinandovi	Ferdinand	k1gMnSc3	Ferdinand
(	(	kIx(	(
<g/>
čímž	což	k3yRnSc7	což
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
rodu	rod	k1gInSc2	rod
na	na	k7c4	na
španělskou	španělský	k2eAgFnSc4d1	španělská
a	a	k8xC	a
rakouskou	rakouský	k2eAgFnSc4d1	rakouská
větev	větev	k1gFnSc4	větev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
odjel	odjet	k5eAaPmAgMnS	odjet
roku	rok	k1gInSc2	rok
1522	[number]	k4	1522
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
zatím	zatím	k6eAd1	zatím
potlačeno	potlačen	k2eAgNnSc1d1	potlačeno
povstání	povstání	k1gNnSc1	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
za	za	k7c2	za
jeho	jeho	k3xOp3gInSc2	jeho
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
francouzským	francouzský	k2eAgMnSc7d1	francouzský
králem	král	k1gMnSc7	král
Františkem	František	k1gMnSc7	František
I.	I.	kA	I.
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
stát	stát	k5eAaImF	stát
počátkem	počátek	k1gInSc7	počátek
dlouholetého	dlouholetý	k2eAgNnSc2d1	dlouholeté
nepřátelství	nepřátelství	k1gNnSc2	nepřátelství
mezi	mezi	k7c7	mezi
Habsburky	Habsburk	k1gMnPc7	Habsburk
a	a	k8xC	a
Francií	Francie	k1gFnSc7	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
zlepšil	zlepšit	k5eAaPmAgMnS	zlepšit
roku	rok	k1gInSc2	rok
1516	[number]	k4	1516
vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
Františkem	František	k1gMnSc7	František
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
pánem	pán	k1gMnSc7	pán
tak	tak	k8xS	tak
ohromných	ohromný	k2eAgFnPc2d1	ohromná
říší	říš	k1gFnPc2	říš
<g/>
,	,	kIx,	,
cítil	cítit	k5eAaImAgMnS	cítit
se	se	k3xPyFc4	se
František	František	k1gMnSc1	František
jeho	jeho	k3xOp3gFnSc7	jeho
mocí	moc	k1gFnSc7	moc
ohrožen	ohrozit	k5eAaPmNgMnS	ohrozit
a	a	k8xC	a
někdejší	někdejší	k2eAgNnSc4d1	někdejší
přátelství	přátelství	k1gNnSc4	přátelství
se	se	k3xPyFc4	se
proměnilo	proměnit	k5eAaPmAgNnS	proměnit
v	v	k7c4	v
opak	opak	k1gInSc4	opak
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
příčin	příčina	k1gFnPc2	příčina
došlo	dojít	k5eAaPmAgNnS	dojít
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
roku	rok	k1gInSc2	rok
1521	[number]	k4	1521
k	k	k7c3	k
první	první	k4xOgFnSc6	první
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1526	[number]	k4	1526
ke	k	k7c3	k
druhé	druhý	k4xOgFnSc3	druhý
válce	válka	k1gFnSc3	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
měl	mít	k5eAaImAgMnS	mít
Karel	Karel	k1gMnSc1	Karel
za	za	k7c4	za
spojence	spojenec	k1gMnPc4	spojenec
Anglii	Anglie	k1gFnSc4	Anglie
<g/>
,	,	kIx,	,
papeže	papež	k1gMnSc4	papež
a	a	k8xC	a
krom	krom	k7c2	krom
Benátek	Benátky	k1gFnPc2	Benátky
ostatní	ostatní	k2eAgNnSc1d1	ostatní
knížata	kníže	k1gNnPc1	kníže
italská	italský	k2eAgNnPc1d1	italské
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
této	tento	k3xDgFnSc2	tento
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1525	[number]	k4	1525
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
Karlova	Karlův	k2eAgFnSc1d1	Karlova
armáda	armáda	k1gFnSc1	armáda
velkého	velký	k2eAgInSc2d1	velký
úspěchu	úspěch	k1gInSc2	úspěch
u	u	k7c2	u
Pavie	Pavie	k1gFnSc2	Pavie
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
se	se	k3xPyFc4	se
papež	papež	k1gMnSc1	papež
Klement	Klement	k1gMnSc1	Klement
VII	VII	kA	VII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
obávaje	obávat	k5eAaImSgInS	obávat
se	se	k3xPyFc4	se
císařovy	císařův	k2eAgFnSc2d1	císařova
rostoucí	rostoucí	k2eAgFnSc2d1	rostoucí
moci	moc	k1gFnSc2	moc
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
spojil	spojit	k5eAaPmAgMnS	spojit
roku	rok	k1gInSc2	rok
1526	[number]	k4	1526
s	s	k7c7	s
Františkem	František	k1gMnSc7	František
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
smíření	smíření	k1gNnSc3	smíření
mezi	mezi	k7c7	mezi
císařem	císař	k1gMnSc7	císař
a	a	k8xC	a
papežem	papež	k1gMnSc7	papež
došlo	dojít	k5eAaPmAgNnS	dojít
ještě	ještě	k9	ještě
před	před	k7c7	před
dojednáním	dojednání	k1gNnSc7	dojednání
míru	mír	k1gInSc2	mír
v	v	k7c4	v
Cambrai	Cambrae	k1gFnSc4	Cambrae
smlouvou	smlouva	k1gFnSc7	smlouva
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1529	[number]	k4	1529
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
Karel	Karel	k1gMnSc1	Karel
přijel	přijet	k5eAaPmAgMnS	přijet
ze	z	k7c2	z
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
až	až	k9	až
dosud	dosud	k6eAd1	dosud
zdržoval	zdržovat	k5eAaImAgMnS	zdržovat
<g/>
,	,	kIx,	,
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
učinil	učinit	k5eAaImAgMnS	učinit
s	s	k7c7	s
papežem	papež	k1gMnSc7	papež
a	a	k8xC	a
jinými	jiný	k2eAgMnPc7d1	jiný
italskými	italský	k2eAgMnPc7d1	italský
státy	stát	k1gInPc1	stát
velkou	velký	k2eAgFnSc4d1	velká
ligu	liga	k1gFnSc4	liga
<g/>
,	,	kIx,	,
vlastně	vlastně	k9	vlastně
jakousi	jakýsi	k3yIgFnSc4	jakýsi
hegemonii	hegemonie	k1gFnSc4	hegemonie
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1530	[number]	k4	1530
byl	být	k5eAaImAgInS	být
papežem	papež	k1gMnSc7	papež
v	v	k7c6	v
Bologni	Bologna	k1gFnSc6	Bologna
korunován	korunován	k2eAgInSc4d1	korunován
korunou	koruna	k1gFnSc7	koruna
lombardskou	lombardský	k2eAgFnSc7d1	Lombardská
i	i	k8xC	i
císařskou	císařský	k2eAgFnSc7d1	císařská
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
poslední	poslední	k2eAgFnSc1d1	poslední
císařská	císařský	k2eAgFnSc1d1	císařská
korunovace	korunovace	k1gFnSc1	korunovace
vykonaná	vykonaný	k2eAgFnSc1d1	vykonaná
papežem	papež	k1gMnSc7	papež
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
odebral	odebrat	k5eAaPmAgMnS	odebrat
Karel	Karel	k1gMnSc1	Karel
do	do	k7c2	do
svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
jej	on	k3xPp3gMnSc4	on
důrazně	důrazně	k6eAd1	důrazně
volalo	volat	k5eAaImAgNnS	volat
stálé	stálý	k2eAgNnSc1d1	stálé
šíření	šíření	k1gNnSc1	šíření
protestantství	protestantství	k1gNnSc2	protestantství
<g/>
,	,	kIx,	,
a	a	k8xC	a
svolal	svolat	k5eAaPmAgInS	svolat
sněm	sněm	k1gInSc1	sněm
do	do	k7c2	do
Augšpurku	Augšpurk	k1gInSc2	Augšpurk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
hodlal	hodlat	k5eAaImAgMnS	hodlat
zasadit	zasadit	k5eAaPmF	zasadit
o	o	k7c6	o
odstranění	odstranění	k1gNnSc6	odstranění
církevní	církevní	k2eAgFnSc2d1	církevní
roztržky	roztržka	k1gFnSc2	roztržka
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
pomýšlel	pomýšlet	k5eAaImAgInS	pomýšlet
na	na	k7c4	na
mírné	mírný	k2eAgNnSc4d1	mírné
dohodnutí	dohodnutí	k1gNnSc4	dohodnutí
s	s	k7c7	s
protestantskými	protestantský	k2eAgInPc7d1	protestantský
stavy	stav	k1gInPc7	stav
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yQnSc3	což
radil	radit	k5eAaImAgMnS	radit
i	i	k8xC	i
jeho	jeho	k3xOp3gMnSc1	jeho
kancléř	kancléř	k1gMnSc1	kancléř
Gattinara	Gattinara	k1gFnSc1	Gattinara
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
protestanti	protestant	k1gMnPc1	protestant
nechtěli	chtít	k5eNaImAgMnP	chtít
od	od	k7c2	od
augšpurské	augšpurský	k2eAgFnSc2d1	augšpurská
konfese	konfese	k1gFnSc2	konfese
ustoupit	ustoupit	k5eAaPmF	ustoupit
<g/>
,	,	kIx,	,
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
usnesení	usnesení	k1gNnSc4	usnesení
sněmovní	sněmovní	k2eAgFnSc2d1	sněmovní
majority	majorita	k1gFnSc2	majorita
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
obnoven	obnovit	k5eAaPmNgInS	obnovit
edikt	edikt	k1gInSc1	edikt
wormský	wormský	k2eAgInSc1d1	wormský
<g/>
.	.	kIx.	.
</s>
<s>
Protestantští	protestantský	k2eAgMnPc1d1	protestantský
stavové	stavový	k2eAgFnSc3d1	stavová
však	však	k9	však
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
učinili	učinit	k5eAaPmAgMnP	učinit
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1531	[number]	k4	1531
šmalkaldský	šmalkaldský	k2eAgInSc1d1	šmalkaldský
spolek	spolek	k1gInSc1	spolek
<g/>
,	,	kIx,	,
a	a	k8xC	a
jelikož	jelikož	k8xS	jelikož
zároveň	zároveň	k6eAd1	zároveň
byla	být	k5eAaImAgFnS	být
moc	moc	k1gFnSc1	moc
Habsburků	Habsburk	k1gMnPc2	Habsburk
ohrožena	ohrožen	k2eAgFnSc1d1	ohrožena
Turky	turek	k1gInPc4	turek
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Karel	Karel	k1gMnSc1	Karel
nucen	nutit	k5eAaImNgMnS	nutit
prohlásit	prohlásit	k5eAaPmF	prohlásit
v	v	k7c6	v
Norimberku	Norimberk	k1gInSc6	Norimberk
náboženský	náboženský	k2eAgInSc1d1	náboženský
mír	mír	k1gInSc1	mír
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
něhož	jenž	k3xRgMnSc2	jenž
zatím	zatím	k6eAd1	zatím
měl	mít	k5eAaImAgMnS	mít
potrvat	potrvat	k5eAaPmF	potrvat
stav	stav	k1gInSc4	stav
dosavadní	dosavadní	k2eAgInSc4d1	dosavadní
až	až	k9	až
do	do	k7c2	do
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
příštího	příští	k2eAgInSc2d1	příští
koncilu	koncil	k1gInSc2	koncil
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
svolání	svolání	k1gNnSc6	svolání
koncilu	koncil	k1gInSc2	koncil
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
se	se	k3xPyFc4	se
protestanti	protestant	k1gMnPc1	protestant
stále	stále	k6eAd1	stále
dovolávali	dovolávat	k5eAaImAgMnP	dovolávat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Karel	Karel	k1gMnSc1	Karel
velmi	velmi	k6eAd1	velmi
horlivě	horlivě	k6eAd1	horlivě
zasazoval	zasazovat	k5eAaImAgMnS	zasazovat
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
roku	rok	k1gInSc2	rok
1532	[number]	k4	1532
podnikl	podniknout	k5eAaPmAgMnS	podniknout
výpravu	výprava	k1gFnSc4	výprava
proti	proti	k7c3	proti
Turkům	turek	k1gInPc3	turek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
donutil	donutit	k5eAaPmAgMnS	donutit
k	k	k7c3	k
ústupu	ústup	k1gInSc3	ústup
z	z	k7c2	z
Ferdinandových	Ferdinandův	k2eAgFnPc2d1	Ferdinandova
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
sjezdu	sjezd	k1gInSc6	sjezd
s	s	k7c7	s
papežem	papež	k1gMnSc7	papež
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1532	[number]	k4	1532
v	v	k7c6	v
Bologni	Bologna	k1gFnSc6	Bologna
vyjednával	vyjednávat	k5eAaImAgMnS	vyjednávat
o	o	k7c4	o
svolání	svolání	k1gNnSc4	svolání
koncilu	koncil	k1gInSc2	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
obával	obávat	k5eAaImAgMnS	obávat
ztenčení	ztenčení	k1gNnSc4	ztenčení
své	svůj	k3xOyFgFnSc2	svůj
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
neměl	mít	k5eNaImAgMnS	mít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
mnoho	mnoho	k6eAd1	mnoho
chuti	chuť	k1gFnSc2	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Sice	sice	k8xC	sice
slíbil	slíbit	k5eAaPmAgMnS	slíbit
svolání	svolání	k1gNnSc4	svolání
koncilu	koncil	k1gInSc2	koncil
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žádné	žádný	k3yNgInPc4	žádný
kroky	krok	k1gInPc4	krok
ke	k	k7c3	k
svolání	svolání	k1gNnSc3	svolání
nepodnikl	podniknout	k5eNaPmAgInS	podniknout
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
byl	být	k5eAaImAgMnS	být
potom	potom	k6eAd1	potom
po	po	k7c4	po
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
zaměstnán	zaměstnat	k5eAaPmNgInS	zaměstnat
na	na	k7c6	na
jiných	jiný	k2eAgFnPc6d1	jiná
stranách	strana	k1gFnPc6	strana
a	a	k8xC	a
nemohl	moct	k5eNaImAgMnS	moct
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
věnovat	věnovat	k5eAaImF	věnovat
církevním	církevní	k2eAgFnPc3d1	církevní
záležitostem	záležitost	k1gFnPc3	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1535	[number]	k4	1535
podnikl	podniknout	k5eAaPmAgMnS	podniknout
ze	z	k7c2	z
Španělska	Španělsko	k1gNnSc2	Španělsko
velikou	veliký	k2eAgFnSc4d1	veliká
výpravu	výprava	k1gFnSc4	výprava
proti	proti	k7c3	proti
Tunisu	Tunis	k1gInSc3	Tunis
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vlády	vláda	k1gFnSc2	vláda
zmocnil	zmocnit	k5eAaPmAgMnS	zmocnit
poturčenec	poturčenec	k1gMnSc1	poturčenec
Chajruddín	Chajruddín	k1gMnSc1	Chajruddín
Barbarossa	Barbarossa	k1gMnSc1	Barbarossa
a	a	k8xC	a
pod	pod	k7c7	pod
ochranou	ochrana	k1gFnSc7	ochrana
sultána	sultán	k1gMnSc2	sultán
cařihradského	cařihradský	k2eAgMnSc2d1	cařihradský
provozoval	provozovat	k5eAaImAgMnS	provozovat
námořní	námořní	k2eAgNnSc4d1	námořní
loupežnictví	loupežnictví	k1gNnSc4	loupežnictví
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
dobyl	dobýt	k5eAaPmAgMnS	dobýt
Goletty	Goletta	k1gFnPc4	Goletta
<g/>
,	,	kIx,	,
porazil	porazit	k5eAaPmAgMnS	porazit
Chajruddína	Chajruddín	k1gInSc2	Chajruddín
a	a	k8xC	a
dosadil	dosadit	k5eAaPmAgMnS	dosadit
jím	jíst	k5eAaImIp1nS	jíst
zapuzeného	zapuzený	k2eAgMnSc4d1	zapuzený
panovníka	panovník	k1gMnSc4	panovník
Muley	Mulea	k1gMnSc2	Mulea
Hasana	Hasan	k1gMnSc2	Hasan
<g/>
.	.	kIx.	.
</s>
<s>
Osvobození	osvobození	k1gNnSc1	osvobození
20	[number]	k4	20
000	[number]	k4	000
křesťanských	křesťanský	k2eAgMnPc2d1	křesťanský
otroků	otrok	k1gMnPc2	otrok
korunovalo	korunovat	k5eAaBmAgNnS	korunovat
tuto	tento	k3xDgFnSc4	tento
zdařilou	zdařilý	k2eAgFnSc4d1	zdařilá
výpravu	výprava	k1gFnSc4	výprava
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
získala	získat	k5eAaPmAgFnS	získat
císaři	císař	k1gMnSc3	císař
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
velikou	veliký	k2eAgFnSc4d1	veliká
úctu	úcta	k1gFnSc4	úcta
jako	jako	k8xS	jako
přednímu	přední	k2eAgMnSc3d1	přední
obhájci	obhájce	k1gMnSc3	obhájce
křesťanství	křesťanství	k1gNnSc2	křesťanství
proti	proti	k7c3	proti
islámu	islám	k1gInSc3	islám
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
již	již	k6eAd1	již
také	také	k9	také
vznikalo	vznikat	k5eAaImAgNnS	vznikat
nové	nový	k2eAgNnSc1d1	nové
nepřátelství	nepřátelství	k1gNnSc1	nepřátelství
s	s	k7c7	s
Františkem	františek	k1gInSc7	františek
I.	I.	kA	I.
Spor	spor	k1gInSc1	spor
o	o	k7c4	o
Milán	Milán	k1gInSc4	Milán
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
roku	rok	k1gInSc2	rok
1535	[number]	k4	1535
vymřel	vymřít	k5eAaPmAgInS	vymřít
rod	rod	k1gInSc1	rod
Sforzů	Sforz	k1gMnPc2	Sforz
<g/>
,	,	kIx,	,
a	a	k8xC	a
o	o	k7c4	o
Savojsko	Savojsko	k1gNnSc4	Savojsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gFnSc6	jehož
některé	některý	k3yIgFnSc6	některý
části	část	k1gFnSc6	část
si	se	k3xPyFc3	se
František	František	k1gMnSc1	František
činil	činit	k5eAaImAgMnS	činit
nároky	nárok	k1gInPc4	nárok
<g/>
,	,	kIx,	,
vedl	vést	k5eAaImAgMnS	vést
roku	rok	k1gInSc2	rok
1536	[number]	k4	1536
k	k	k7c3	k
třetí	třetí	k4xOgFnSc3	třetí
válce	válka	k1gFnSc3	válka
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgMnPc7	dva
panovníky	panovník	k1gMnPc7	panovník
<g/>
,	,	kIx,	,
až	až	k9	až
roku	rok	k1gInSc2	rok
1538	[number]	k4	1538
papež	papež	k1gMnSc1	papež
zprostředkoval	zprostředkovat	k5eAaPmAgMnS	zprostředkovat
příměří	příměří	k1gNnSc4	příměří
v	v	k7c6	v
Nizze	Nizza	k1gFnSc6	Nizza
na	na	k7c4	na
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
podržel	podržet	k5eAaPmAgMnS	podržet
Milán	Milán	k1gInSc4	Milán
<g/>
,	,	kIx,	,
Francouzi	Francouz	k1gMnPc1	Francouz
Savojsko	Savojsko	k1gNnSc5	Savojsko
<g/>
.	.	kIx.	.
</s>
<s>
Nákladné	nákladný	k2eAgFnPc1d1	nákladná
Karlovy	Karlův	k2eAgFnPc1d1	Karlova
války	válka	k1gFnPc1	válka
byly	být	k5eAaImAgFnP	být
spojeny	spojit	k5eAaPmNgFnP	spojit
s	s	k7c7	s
velikým	veliký	k2eAgInSc7d1	veliký
finančním	finanční	k2eAgInSc7d1	finanční
útiskem	útisk	k1gInSc7	útisk
jeho	jeho	k3xOp3gFnPc2	jeho
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
snahy	snaha	k1gFnSc2	snaha
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
o	o	k7c4	o
zvýšení	zvýšení	k1gNnSc4	zvýšení
moci	moc	k1gFnSc2	moc
panovnické	panovnický	k2eAgNnSc4d1	panovnické
a	a	k8xC	a
omezení	omezení	k1gNnSc4	omezení
svobod	svoboda	k1gFnPc2	svoboda
stavovských	stavovský	k2eAgFnPc2d1	stavovská
i	i	k8xC	i
zemských	zemský	k2eAgFnPc2d1	zemská
budilo	budit	k5eAaImAgNnS	budit
nespokojenost	nespokojenost	k1gFnSc4	nespokojenost
nejen	nejen	k6eAd1	nejen
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Karel	Karel	k1gMnSc1	Karel
zatím	zatím	k6eAd1	zatím
zcela	zcela	k6eAd1	zcela
zdomácněl	zdomácnět	k5eAaPmAgInS	zdomácnět
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
i	i	k9	i
v	v	k7c6	v
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1539	[number]	k4	1539
tu	tu	k6eAd1	tu
dokonce	dokonce	k9	dokonce
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
vzbouření	vzbouření	k1gNnSc3	vzbouření
města	město	k1gNnSc2	město
Gentu	Gent	k1gInSc2	Gent
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
se	se	k3xPyFc4	se
vypravil	vypravit	k5eAaPmAgMnS	vypravit
osobně	osobně	k6eAd1	osobně
do	do	k7c2	do
Nizozemí	Nizozemí	k1gNnSc2	Nizozemí
<g/>
,	,	kIx,	,
přemohl	přemoct	k5eAaPmAgInS	přemoct
vzpouru	vzpoura	k1gFnSc4	vzpoura
a	a	k8xC	a
město	město	k1gNnSc4	město
přísně	přísně	k6eAd1	přísně
potrestal	potrestat	k5eAaPmAgInS	potrestat
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
přijel	přijet	k5eAaPmAgMnS	přijet
po	po	k7c6	po
devítileté	devítiletý	k2eAgFnSc6d1	devítiletá
nepřítomnosti	nepřítomnost	k1gFnSc6	nepřítomnost
do	do	k7c2	do
Němec	Němec	k1gMnSc1	Němec
a	a	k8xC	a
vypsal	vypsat	k5eAaPmAgInS	vypsat
sněm	sněm	k1gInSc1	sněm
do	do	k7c2	do
Řezna	Řezno	k1gNnSc2	Řezno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
jednat	jednat	k5eAaImF	jednat
o	o	k7c6	o
návratu	návrat	k1gInSc6	návrat
protestantů	protestant	k1gMnPc2	protestant
do	do	k7c2	do
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
tyto	tento	k3xDgFnPc1	tento
snahy	snaha	k1gFnPc1	snaha
se	se	k3xPyFc4	se
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
marnými	marný	k2eAgFnPc7d1	marná
<g/>
,	,	kIx,	,
pročež	pročež	k6eAd1	pročež
byl	být	k5eAaImAgInS	být
prozatím	prozatím	k6eAd1	prozatím
obnoven	obnovit	k5eAaPmNgInS	obnovit
nábožensky	nábožensky	k6eAd1	nábožensky
norimberský	norimberský	k2eAgInSc1d1	norimberský
mír	mír	k1gInSc1	mír
(	(	kIx(	(
<g/>
1541	[number]	k4	1541
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
k	k	k7c3	k
povolnosti	povolnost	k1gFnPc4	povolnost
vůči	vůči	k7c3	vůči
protestantům	protestant	k1gMnPc3	protestant
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
touto	tento	k3xDgFnSc7	tento
dobou	doba	k1gFnSc7	doba
Turci	Turek	k1gMnPc1	Turek
ohrožovali	ohrožovat	k5eAaImAgMnP	ohrožovat
Ferdinandovy	Ferdinandův	k2eAgFnPc4d1	Ferdinandova
země	zem	k1gFnPc4	zem
a	a	k8xC	a
také	také	k9	také
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
nastávalo	nastávat	k5eAaImAgNnS	nastávat
nové	nový	k2eAgNnSc1d1	nové
nepřátelství	nepřátelství	k1gNnSc1	nepřátelství
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
čtvrté	čtvrtá	k1gFnSc3	čtvrtá
válce	válka	k1gFnSc3	válka
(	(	kIx(	(
<g/>
1542	[number]	k4	1542
<g/>
-	-	kIx~	-
<g/>
44	[number]	k4	44
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
oslabil	oslabit	k5eAaPmAgInS	oslabit
tureckou	turecký	k2eAgFnSc4d1	turecká
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
podnikl	podniknout	k5eAaPmAgMnS	podniknout
Karel	Karel	k1gMnSc1	Karel
roku	rok	k1gInSc2	rok
1541	[number]	k4	1541
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
výpravu	výprava	k1gFnSc4	výprava
proti	proti	k7c3	proti
Alžíru	Alžír	k1gInSc3	Alžír
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dosud	dosud	k6eAd1	dosud
držel	držet	k5eAaImAgInS	držet
Chajruddín	Chajruddín	k1gInSc1	Chajruddín
pod	pod	k7c7	pod
vrchním	vrchní	k2eAgNnSc7d1	vrchní
tureckým	turecký	k2eAgNnSc7d1	turecké
panstvím	panství	k1gNnSc7	panství
a	a	k8xC	a
neustával	ustávat	k5eNaImAgMnS	ustávat
v	v	k7c6	v
námořním	námořní	k2eAgNnSc6d1	námořní
loupežnictví	loupežnictví	k1gNnSc6	loupežnictví
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
výprava	výprava	k1gFnSc1	výprava
byla	být	k5eAaImAgFnS	být
zmařena	zmařit	k5eAaPmNgFnS	zmařit
mořskými	mořský	k2eAgFnPc7d1	mořská
bouřemi	bouř	k1gFnPc7	bouř
a	a	k8xC	a
Turci	Turek	k1gMnPc1	Turek
činili	činit	k5eAaImAgMnP	činit
potom	potom	k6eAd1	potom
jako	jako	k8xS	jako
spojenci	spojenec	k1gMnPc1	spojenec
Františka	František	k1gMnSc2	František
I.	I.	kA	I.
nemalé	malý	k2eNgFnSc2d1	nemalá
škody	škoda	k1gFnSc2	škoda
na	na	k7c6	na
italském	italský	k2eAgNnSc6d1	italské
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Šťastněji	šťastně	k6eAd2	šťastně
byla	být	k5eAaImAgFnS	být
vedena	veden	k2eAgFnSc1d1	vedena
válka	válka	k1gFnSc1	válka
proti	proti	k7c3	proti
Františkovi	František	k1gMnSc3	František
samému	samý	k3xTgMnSc3	samý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
míru	mír	k1gInSc6	mír
z	z	k7c2	z
Crespy	Crespa	k1gFnSc2	Crespa
(	(	kIx(	(
<g/>
1544	[number]	k4	1544
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zřekl	zřeknout	k5eAaPmAgMnS	zřeknout
František	František	k1gMnSc1	František
nároků	nárok	k1gInPc2	nárok
na	na	k7c4	na
Neapolsko	Neapolsko	k1gNnSc4	Neapolsko
a	a	k8xC	a
vrchního	vrchní	k2eAgNnSc2d1	vrchní
práva	právo	k1gNnSc2	právo
lenního	lenní	k2eAgInSc2d1	lenní
nad	nad	k7c4	nad
Flandry	Flandry	k1gInPc4	Flandry
a	a	k8xC	a
Artoiskem	Artoisko	k1gNnSc7	Artoisko
a	a	k8xC	a
slíbil	slíbit	k5eAaPmAgMnS	slíbit
ústně	ústně	k6eAd1	ústně
i	i	k9	i
pomoc	pomoc	k1gFnSc4	pomoc
proti	proti	k7c3	proti
protestantům	protestant	k1gMnPc3	protestant
<g/>
.	.	kIx.	.
</s>
<s>
Milán	Milán	k1gInSc1	Milán
zůstal	zůstat	k5eAaPmAgInS	zůstat
císaři	císař	k1gMnSc3	císař
nejprve	nejprve	k6eAd1	nejprve
prozatímně	prozatímně	k6eAd1	prozatímně
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
nadobro	nadobro	k6eAd1	nadobro
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgMnS	moct
Karel	Karel	k1gMnSc1	Karel
se	s	k7c7	s
vším	všecek	k3xTgInSc7	všecek
důrazem	důraz	k1gInSc7	důraz
věnovat	věnovat	k5eAaPmF	věnovat
uspořádání	uspořádání	k1gNnSc4	uspořádání
náboženských	náboženský	k2eAgInPc2d1	náboženský
poměrů	poměr	k1gInPc2	poměr
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Přese	přese	k7c4	přese
všechny	všechen	k3xTgInPc4	všechen
nezdařené	zdařený	k2eNgInPc4d1	nezdařený
pokusy	pokus	k1gInPc4	pokus
nepřestal	přestat	k5eNaPmAgInS	přestat
dosud	dosud	k6eAd1	dosud
doufat	doufat	k5eAaImF	doufat
v	v	k7c4	v
příznivý	příznivý	k2eAgInSc4d1	příznivý
výsledek	výsledek	k1gInSc4	výsledek
mírného	mírný	k2eAgNnSc2d1	mírné
jednání	jednání	k1gNnSc2	jednání
a	a	k8xC	a
stále	stále	k6eAd1	stále
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c4	o
svolání	svolání	k1gNnSc4	svolání
koncilu	koncil	k1gInSc2	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
konečně	konečně	k6eAd1	konečně
po	po	k7c6	po
dlouhém	dlouhý	k2eAgNnSc6d1	dlouhé
jednání	jednání	k1gNnSc6	jednání
a	a	k8xC	a
po	po	k7c6	po
mnohých	mnohý	k2eAgInPc6d1	mnohý
odkladech	odklad	k1gInPc6	odklad
papež	papež	k1gMnSc1	papež
Pavel	Pavel	k1gMnSc1	Pavel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
v	v	k7c6	v
září	září	k1gNnSc6	září
1544	[number]	k4	1544
svolal	svolat	k5eAaPmAgMnS	svolat
na	na	k7c4	na
rok	rok	k1gInSc4	rok
1545	[number]	k4	1545
tridentský	tridentský	k2eAgInSc4d1	tridentský
koncil	koncil	k1gInSc4	koncil
a	a	k8xC	a
protestanti	protestant	k1gMnPc1	protestant
jej	on	k3xPp3gMnSc4	on
odepřeli	odepřít	k5eAaPmAgMnP	odepřít
obeslat	obeslat	k5eAaPmF	obeslat
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
Karel	Karel	k1gMnSc1	Karel
užít	užít	k5eAaPmF	užít
zbraně	zbraň	k1gFnPc1	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Šmalkaldská	Šmalkaldský	k2eAgFnSc1d1	Šmalkaldská
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Maje	Maja	k1gFnSc3	Maja
mír	mír	k1gInSc4	mír
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
<g/>
,	,	kIx,	,
s	s	k7c7	s
Turky	turek	k1gInPc7	turek
a	a	k8xC	a
přátelství	přátelství	k1gNnSc1	přátelství
s	s	k7c7	s
Anglií	Anglie	k1gFnSc7	Anglie
mohl	moct	k5eAaImAgMnS	moct
se	se	k3xPyFc4	se
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
mocí	moc	k1gFnSc7	moc
obrátit	obrátit	k5eAaPmF	obrátit
proti	proti	k7c3	proti
německým	německý	k2eAgMnPc3d1	německý
protestantům	protestant	k1gMnPc3	protestant
<g/>
.	.	kIx.	.
</s>
<s>
Nadto	nadto	k6eAd1	nadto
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
rozdělit	rozdělit	k5eAaPmF	rozdělit
jejich	jejich	k3xOp3gFnSc4	jejich
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Tajně	tajně	k6eAd1	tajně
přivedl	přivést	k5eAaPmAgMnS	přivést
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
stranu	strana	k1gFnSc4	strana
saského	saský	k2eAgMnSc4d1	saský
vévodu	vévoda	k1gMnSc4	vévoda
Mořice	Mořic	k1gMnSc4	Mořic
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
slíbil	slíbit	k5eAaPmAgInS	slíbit
saské	saský	k2eAgNnSc4d1	Saské
kurfiřtství	kurfiřtství	k1gNnSc4	kurfiřtství
<g/>
,	,	kIx,	,
a	a	k8xC	a
některá	některý	k3yIgNnPc1	některý
menší	malý	k2eAgNnPc1d2	menší
knížata	kníže	k1gNnPc1	kníže
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
pohnul	pohnout	k5eAaPmAgMnS	pohnout
kurfiřty	kurfiřt	k1gMnPc7	kurfiřt
falckého	falcký	k2eAgNnSc2d1	falcké
a	a	k8xC	a
braniborského	braniborský	k2eAgNnSc2d1	braniborské
k	k	k7c3	k
neutralitě	neutralita	k1gFnSc3	neutralita
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
šmalkaldský	šmalkaldský	k2eAgInSc1d1	šmalkaldský
spolek	spolek	k1gInSc1	spolek
byl	být	k5eAaImAgInS	být
osamocen	osamotit	k5eAaPmNgInS	osamotit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
spolčil	spolčit	k5eAaPmAgMnS	spolčit
s	s	k7c7	s
papežem	papež	k1gMnSc7	papež
a	a	k8xC	a
s	s	k7c7	s
Ferdinandem	Ferdinand	k1gMnSc7	Ferdinand
<g/>
,	,	kIx,	,
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
Karel	Karel	k1gMnSc1	Karel
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1546	[number]	k4	1546
acht	acht	k1gInSc1	acht
na	na	k7c4	na
náčelníky	náčelník	k1gMnPc4	náčelník
spolku	spolek	k1gInSc2	spolek
<g/>
,	,	kIx,	,
kurfiřta	kurfiřt	k1gMnSc2	kurfiřt
saského	saský	k2eAgMnSc2d1	saský
Jana	Jan	k1gMnSc2	Jan
Fridricha	Fridrich	k1gMnSc2	Fridrich
a	a	k8xC	a
hesenského	hesenský	k2eAgMnSc2d1	hesenský
lankraběte	lankrabě	k1gMnSc2	lankrabě
Filipa	Filip	k1gMnSc2	Filip
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
však	však	k9	však
byli	být	k5eAaImAgMnP	být
připravení	připravený	k2eAgMnPc1d1	připravený
<g/>
,	,	kIx,	,
předešli	předejít	k5eAaPmAgMnP	předejít
jej	on	k3xPp3gNnSc4	on
a	a	k8xC	a
vtrhli	vtrhnout	k5eAaPmAgMnP	vtrhnout
na	na	k7c4	na
jih	jih	k1gInSc4	jih
svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
jen	jen	k9	jen
jejich	jejich	k3xOp3gFnSc1	jejich
nerozhodnost	nerozhodnost	k1gFnSc1	nerozhodnost
byla	být	k5eAaImAgFnS	být
příčinou	příčina	k1gFnSc7	příčina
<g/>
,	,	kIx,	,
že	že	k8xS	že
nedosáhli	dosáhnout	k5eNaPmAgMnP	dosáhnout
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Tu	ten	k3xDgFnSc4	ten
však	však	k9	však
vpadl	vpadnout	k5eAaPmAgMnS	vpadnout
Mořic	Mořic	k1gMnSc1	Mořic
s	s	k7c7	s
Ferdinandem	Ferdinand	k1gMnSc7	Ferdinand
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1546	[number]	k4	1546
nenadále	nenadále	k6eAd1	nenadále
do	do	k7c2	do
kurfiřtství	kurfiřtství	k1gNnSc2	kurfiřtství
saského	saský	k2eAgNnSc2d1	Saské
a	a	k8xC	a
přinutili	přinutit	k5eAaPmAgMnP	přinutit
spojence	spojenec	k1gMnSc4	spojenec
k	k	k7c3	k
ústupu	ústup	k1gInSc3	ústup
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
se	se	k3xPyFc4	se
jihoněmecká	jihoněmecký	k2eAgNnPc1d1	jihoněmecké
protestantská	protestantský	k2eAgNnPc1d1	protestantské
města	město	k1gNnPc1	město
musela	muset	k5eAaImAgNnP	muset
vzdát	vzdát	k5eAaPmF	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
pak	pak	k6eAd1	pak
sám	sám	k3xTgMnSc1	sám
vtrhl	vtrhnout	k5eAaPmAgInS	vtrhnout
do	do	k7c2	do
Saska	Sasko	k1gNnSc2	Sasko
a	a	k8xC	a
vítězstvím	vítězství	k1gNnSc7	vítězství
u	u	k7c2	u
Mülhberka	Mülhberka	k1gFnSc1	Mülhberka
24	[number]	k4	24
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1547	[number]	k4	1547
boj	boj	k1gInSc1	boj
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Fridrich	Fridrich	k1gMnSc1	Fridrich
byl	být	k5eAaImAgMnS	být
zajat	zajmout	k5eAaPmNgMnS	zajmout
a	a	k8xC	a
Filip	Filip	k1gMnSc1	Filip
se	se	k3xPyFc4	se
poddal	poddat	k5eAaPmAgMnS	poddat
na	na	k7c4	na
milost	milost	k1gFnSc4	milost
i	i	k8xC	i
nemilost	nemilost	k1gFnSc4	nemilost
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
však	však	k9	však
papež	papež	k1gMnSc1	papež
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
ulekl	uleknout	k5eAaPmAgMnS	uleknout
rychlého	rychlý	k2eAgInSc2d1	rychlý
vzrůstu	vzrůst	k1gInSc2	vzrůst
císařovy	císařův	k2eAgFnSc2d1	císařova
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
obával	obávat	k5eAaImAgMnS	obávat
jeho	jeho	k3xOp3gNnSc4	jeho
zasahování	zasahování	k1gNnSc4	zasahování
do	do	k7c2	do
jednání	jednání	k1gNnSc2	jednání
koncilu	koncil	k1gInSc2	koncil
<g/>
,	,	kIx,	,
přenesl	přenést	k5eAaPmAgInS	přenést
sněm	sněm	k1gInSc1	sněm
proti	proti	k7c3	proti
vůli	vůle	k1gFnSc3	vůle
císařově	císařův	k2eAgFnSc3d1	císařova
do	do	k7c2	do
Bologně	Bologn	k1gInSc6	Bologn
<g/>
,	,	kIx,	,
uspořádal	uspořádat	k5eAaPmAgMnS	uspořádat
Karel	Karel	k1gMnSc1	Karel
náboženské	náboženský	k2eAgFnSc2d1	náboženská
poměry	poměra	k1gFnSc2	poměra
ve	v	k7c6	v
svaté	svatý	k2eAgFnSc6d1	svatá
říši	říš	k1gFnSc6	říš
prozatímně	prozatímně	k6eAd1	prozatímně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1548	[number]	k4	1548
tzv.	tzv.	kA	tzv.
interimem	interim	k1gInSc7	interim
v	v	k7c6	v
Augšpurku	Augšpurk	k1gInSc6	Augšpurk
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
byly	být	k5eAaImAgFnP	být
protestantům	protestant	k1gMnPc3	protestant
povoleny	povolit	k5eAaPmNgFnP	povolit
některé	některý	k3yIgFnPc1	některý
koncese	koncese	k1gFnPc1	koncese
<g/>
,	,	kIx,	,
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
se	se	k3xPyFc4	se
však	však	k9	však
měli	mít	k5eAaImAgMnP	mít
navrátit	navrátit	k5eAaPmF	navrátit
ke	k	k7c3	k
katolickému	katolický	k2eAgNnSc3d1	katolické
vyznání	vyznání	k1gNnSc3	vyznání
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
interim	interim	k1gInSc4	interim
nejen	nejen	k6eAd1	nejen
že	že	k8xS	že
nemělo	mít	k5eNaImAgNnS	mít
účinku	účinek	k1gInSc2	účinek
na	na	k7c4	na
protestanty	protestant	k1gMnPc4	protestant
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
podrobili	podrobit	k5eAaPmAgMnP	podrobit
jen	jen	k9	jen
zdánlivě	zdánlivě	k6eAd1	zdánlivě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pobouřilo	pobouřit	k5eAaPmAgNnS	pobouřit
i	i	k9	i
katolíky	katolík	k1gMnPc7	katolík
<g/>
.	.	kIx.	.
</s>
<s>
Nadto	nadto	k6eAd1	nadto
se	se	k3xPyFc4	se
Karel	Karel	k1gMnSc1	Karel
rozešel	rozejít	k5eAaPmAgMnS	rozejít
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
i	i	k9	i
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
Ferdinandem	Ferdinand	k1gMnSc7	Ferdinand
<g/>
,	,	kIx,	,
zvoleným	zvolený	k2eAgMnSc7d1	zvolený
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1531	[number]	k4	1531
německým	německý	k2eAgMnSc7d1	německý
králem	král	k1gMnSc7	král
-	-	kIx~	-
tedy	tedy	k8xC	tedy
nástupcem	nástupce	k1gMnSc7	nástupce
císaře	císař	k1gMnSc2	císař
-	-	kIx~	-
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
chtěl	chtít	k5eAaImAgMnS	chtít
za	za	k7c4	za
jeho	on	k3xPp3gMnSc4	on
nástupce	nástupce	k1gMnSc4	nástupce
dát	dát	k5eAaPmF	dát
zvolit	zvolit	k5eAaPmF	zvolit
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
Filipa	Filip	k1gMnSc4	Filip
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
vůbec	vůbec	k9	vůbec
veliký	veliký	k2eAgInSc4d1	veliký
vzrůst	vzrůst	k1gInSc4	vzrůst
císařovy	císařův	k2eAgFnSc2d1	císařova
moci	moc	k1gFnSc2	moc
vzbudil	vzbudit	k5eAaPmAgInS	vzbudit
odpor	odpor	k1gInSc1	odpor
všech	všecek	k3xTgMnPc2	všecek
jeho	jeho	k3xOp3gMnPc2	jeho
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Mořic	Mořic	k1gMnSc1	Mořic
Saský	saský	k2eAgMnSc1d1	saský
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
spojenec	spojenec	k1gMnSc1	spojenec
<g/>
,	,	kIx,	,
učinil	učinit	k5eAaImAgMnS	učinit
proti	proti	k7c3	proti
němu	on	k3xPp3gNnSc3	on
tajně	tajně	k6eAd1	tajně
spolek	spolek	k1gInSc1	spolek
s	s	k7c7	s
několika	několik	k4yIc7	několik
protestantskými	protestantský	k2eAgMnPc7d1	protestantský
knížaty	kníže	k1gMnPc7wR	kníže
a	a	k8xC	a
spojil	spojit	k5eAaPmAgMnS	spojit
se	se	k3xPyFc4	se
s	s	k7c7	s
novým	nový	k2eAgMnSc7d1	nový
francouzským	francouzský	k2eAgMnSc7d1	francouzský
králem	král	k1gMnSc7	král
Jindřichem	Jindřich	k1gMnSc7	Jindřich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
se	se	k3xPyFc4	se
konečně	konečně	k6eAd1	konečně
dohodl	dohodnout	k5eAaPmAgMnS	dohodnout
s	s	k7c7	s
papežem	papež	k1gMnSc7	papež
Juliem	Julius	k1gMnSc7	Julius
III	III	kA	III
<g/>
.	.	kIx.	.
na	na	k7c6	na
novém	nový	k2eAgNnSc6d1	nové
svolání	svolání	k1gNnSc6	svolání
koncilu	koncil	k1gInSc2	koncil
do	do	k7c2	do
Tridentu	Trident	k1gMnSc3	Trident
a	a	k8xC	a
pozval	pozvat	k5eAaPmAgInS	pozvat
protestanty	protestant	k1gMnPc4	protestant
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
koncil	koncil	k1gInSc4	koncil
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
1552	[number]	k4	1552
Jindřich	Jindřich	k1gMnSc1	Jindřich
začal	začít	k5eAaPmAgMnS	začít
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
dobyl	dobýt	k5eAaPmAgMnS	dobýt
Mety	met	k1gInPc4	met
<g/>
,	,	kIx,	,
Toul	toul	k1gInSc4	toul
a	a	k8xC	a
Verdun	Verdun	k1gInSc4	Verdun
a	a	k8xC	a
současně	současně	k6eAd1	současně
Mořic	Mořic	k1gMnSc1	Mořic
vtrhl	vtrhnout	k5eAaPmAgMnS	vtrhnout
do	do	k7c2	do
Tyrolska	Tyrolsko	k1gNnSc2	Tyrolsko
<g/>
.	.	kIx.	.
</s>
<s>
Churavý	churavý	k2eAgMnSc1d1	churavý
císař	císař	k1gMnSc1	císař
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
neměl	mít	k5eNaImAgInS	mít
tušení	tušení	k1gNnSc4	tušení
o	o	k7c6	o
Mořicově	Mořicův	k2eAgFnSc6d1	Mořicův
zradě	zrada	k1gFnSc6	zrada
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgInS	muset
spasit	spasit	k5eAaPmF	spasit
rychlým	rychlý	k2eAgInSc7d1	rychlý
útěkem	útěk	k1gInSc7	útěk
z	z	k7c2	z
Innsbrucku	Innsbruck	k1gInSc2	Innsbruck
do	do	k7c2	do
Villachu	Villach	k1gInSc2	Villach
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
neočekávaný	očekávaný	k2eNgInSc1d1	neočekávaný
obrat	obrat	k1gInSc1	obrat
působil	působit	k5eAaImAgInS	působit
velmi	velmi	k6eAd1	velmi
nepříznivě	příznivě	k6eNd1	příznivě
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
mysl	mysl	k1gFnSc4	mysl
<g/>
.	.	kIx.	.
</s>
<s>
Nezbývalo	zbývat	k5eNaImAgNnS	zbývat
než	než	k8xS	než
s	s	k7c7	s
protestanty	protestant	k1gMnPc7	protestant
vyjednávat	vyjednávat	k5eAaImF	vyjednávat
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
prostředníkem	prostředník	k1gInSc7	prostředník
byl	být	k5eAaImAgMnS	být
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zbavil	zbavit	k5eAaPmAgInS	zbavit
alespoň	alespoň	k9	alespoň
jednoho	jeden	k4xCgMnSc4	jeden
protivníka	protivník	k1gMnSc4	protivník
<g/>
,	,	kIx,	,
obrátil	obrátit	k5eAaPmAgMnS	obrátit
se	se	k3xPyFc4	se
Karel	Karel	k1gMnSc1	Karel
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
mocí	moc	k1gFnSc7	moc
proti	proti	k7c3	proti
Francii	Francie	k1gFnSc3	Francie
a	a	k8xC	a
oblehl	oblehnout	k5eAaPmAgMnS	oblehnout
Mety	Mety	k1gFnPc4	Mety
<g/>
,	,	kIx,	,
nepořídil	pořídit	k5eNaPmAgMnS	pořídit
však	však	k9	však
ničeho	nic	k3yNnSc2	nic
a	a	k8xC	a
také	také	k6eAd1	také
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
dařilo	dařit	k5eAaImAgNnS	dařit
se	se	k3xPyFc4	se
Francouzům	Francouz	k1gMnPc3	Francouz
dobře	dobře	k6eAd1	dobře
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
protestanty	protestant	k1gMnPc7	protestant
pak	pak	k6eAd1	pak
podepsal	podepsat	k5eAaPmAgMnS	podepsat
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
roku	rok	k1gInSc2	rok
1555	[number]	k4	1555
augšpurský	augšpurský	k2eAgInSc1d1	augšpurský
mír	mír	k1gInSc1	mír
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
byly	být	k5eAaImAgFnP	být
všechny	všechen	k3xTgFnPc4	všechen
Karlovy	Karlův	k2eAgFnPc4d1	Karlova
snahy	snaha	k1gFnPc4	snaha
o	o	k7c4	o
univerzalismus	univerzalismus	k1gInSc4	univerzalismus
zmařeny	zmařen	k2eAgInPc1d1	zmařen
<g/>
.	.	kIx.	.
</s>
<s>
Císařův	Císařův	k2eAgInSc1d1	Císařův
plán	plán	k1gInSc1	plán
ztroskotal	ztroskotat	k5eAaPmAgInS	ztroskotat
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
zhoršoval	zhoršovat	k5eAaImAgMnS	zhoršovat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
roku	rok	k1gInSc2	rok
1554	[number]	k4	1554
zorganizoval	zorganizovat	k5eAaPmAgMnS	zorganizovat
sňatek	sňatek	k1gInSc4	sňatek
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
Filipa	Filip	k1gMnSc2	Filip
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
<g/>
,	,	kIx,	,
královnou	královna	k1gFnSc7	královna
anglickou	anglický	k2eAgFnSc7d1	anglická
<g/>
,	,	kIx,	,
s	s	k7c7	s
čímž	což	k3yQnSc7	což
spojoval	spojovat	k5eAaImAgMnS	spojovat
velké	velký	k2eAgFnPc4d1	velká
naděje	naděje	k1gFnPc4	naděje
do	do	k7c2	do
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
počal	počnout	k5eAaPmAgMnS	počnout
provádět	provádět	k5eAaImF	provádět
svůj	svůj	k3xOyFgInSc4	svůj
záměr	záměr	k1gInSc4	záměr
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
Filipovi	Filip	k1gMnSc3	Filip
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
Nizozemí	Nizozemí	k1gNnSc2	Nizozemí
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1556	[number]	k4	1556
i	i	k8xC	i
říši	říše	k1gFnSc4	říše
Španělskou	španělský	k2eAgFnSc4d1	španělská
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1556	[number]	k4	1556
zřekl	zřeknout	k5eAaPmAgMnS	zřeknout
se	se	k3xPyFc4	se
také	také	k9	také
vlády	vláda	k1gFnPc1	vláda
ve	v	k7c6	v
svaté	svatý	k2eAgFnSc6d1	svatá
říši	říš	k1gFnSc6	říš
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
bratra	bratr	k1gMnSc2	bratr
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
došlo	dojít	k5eAaPmAgNnS	dojít
definitivně	definitivně	k6eAd1	definitivně
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
habsburského	habsburský	k2eAgInSc2d1	habsburský
rodu	rod	k1gInSc2	rod
na	na	k7c4	na
španělskou	španělský	k2eAgFnSc4d1	španělská
a	a	k8xC	a
rakouskou	rakouský	k2eAgFnSc4d1	rakouská
větev	větev	k1gFnSc4	větev
a	a	k8xC	a
uchýlil	uchýlit	k5eAaPmAgMnS	uchýlit
se	se	k3xPyFc4	se
do	do	k7c2	do
blízkosti	blízkost	k1gFnSc2	blízkost
kláštera	klášter	k1gInSc2	klášter
sv.	sv.	kA	sv.
Jeronýma	Jeroným	k1gMnSc2	Jeroným
zvaného	zvaný	k2eAgMnSc2d1	zvaný
Juste	Just	k1gInSc5	Just
v	v	k7c6	v
Extremaduře	Extremadura	k1gFnSc6	Extremadura
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
žil	žít	k5eAaImAgInS	žít
ještě	ještě	k6eAd1	ještě
dvě	dva	k4xCgNnPc4	dva
léta	léto	k1gNnPc4	léto
ve	v	k7c6	v
vybraně	vybraně	k6eAd1	vybraně
zařízeném	zařízený	k2eAgInSc6d1	zařízený
venkovském	venkovský	k2eAgInSc6d1	venkovský
domě	dům	k1gInSc6	dům
a	a	k8xC	a
se	s	k7c7	s
zájmem	zájem	k1gInSc7	zájem
sledoval	sledovat	k5eAaImAgInS	sledovat
vývoj	vývoj	k1gInSc4	vývoj
politické	politický	k2eAgFnSc2d1	politická
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
si	se	k3xPyFc3	se
užíval	užívat	k5eAaImAgMnS	užívat
světských	světský	k2eAgFnPc2d1	světská
radostí	radost	k1gFnPc2	radost
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
byl	být	k5eAaImAgMnS	být
slabé	slabý	k2eAgFnSc2d1	slabá
tělesné	tělesný	k2eAgFnSc2d1	tělesná
konstituce	konstituce	k1gFnSc2	konstituce
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
chorobný	chorobný	k2eAgInSc1d1	chorobný
zevnějšek	zevnějšek	k1gInSc1	zevnějšek
<g/>
;	;	kIx,	;
v	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
době	doba	k1gFnSc6	doba
ho	on	k3xPp3gInSc2	on
sužovala	sužovat	k5eAaImAgFnS	sužovat
dna	dna	k1gFnSc1	dna
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
neduhy	neduh	k1gInPc1	neduh
<g/>
,	,	kIx,	,
zapříčiněné	zapříčiněný	k2eAgInPc1d1	zapříčiněný
prý	prý	k9	prý
zhýralým	zhýralý	k2eAgInSc7d1	zhýralý
způsobem	způsob	k1gInSc7	způsob
života	život	k1gInSc2	život
v	v	k7c6	v
mladších	mladý	k2eAgNnPc6d2	mladší
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
jeho	jeho	k3xOp3gFnSc7	jeho
chladnou	chladný	k2eAgFnSc7d1	chladná
fasádou	fasáda	k1gFnSc7	fasáda
a	a	k8xC	a
uzavřeností	uzavřenost	k1gFnSc7	uzavřenost
se	se	k3xPyFc4	se
skrývala	skrývat	k5eAaImAgFnS	skrývat
popudlivost	popudlivost	k1gFnSc1	popudlivost
a	a	k8xC	a
mstivost	mstivost	k1gFnSc1	mstivost
<g/>
;	;	kIx,	;
nikdy	nikdy	k6eAd1	nikdy
nezapomínal	zapomínat	k5eNaImAgMnS	zapomínat
urážky	urážka	k1gFnSc2	urážka
a	a	k8xC	a
nechával	nechávat	k5eAaImAgMnS	nechávat
se	se	k3xPyFc4	se
strhnout	strhnout	k5eAaPmF	strhnout
hněvem	hněv	k1gInSc7	hněv
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1533	[number]	k4	1533
nechal	nechat	k5eAaPmAgMnS	nechat
španělský	španělský	k2eAgMnSc1d1	španělský
conquistador	conquistador	k1gMnSc1	conquistador
Francisco	Francisco	k1gMnSc1	Francisco
Pizarro	Pizarro	k1gNnSc4	Pizarro
<g/>
,	,	kIx,	,
Karlův	Karlův	k2eAgMnSc1d1	Karlův
poddaný	poddaný	k1gMnSc1	poddaný
<g/>
,	,	kIx,	,
popravit	popravit	k5eAaPmF	popravit
inckého	incký	k2eAgMnSc4d1	incký
vládce	vládce	k1gMnSc4	vládce
Atahualpu	Atahualp	k1gInSc2	Atahualp
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
mu	on	k3xPp3gMnSc3	on
napsal	napsat	k5eAaPmAgMnS	napsat
rozzlobený	rozzlobený	k2eAgInSc4d1	rozzlobený
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
svůj	svůj	k3xOyFgInSc4	svůj
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
s	s	k7c7	s
popravou	poprava	k1gFnSc7	poprava
monarchy	monarcha	k1gMnSc2	monarcha
a	a	k8xC	a
také	také	k9	také
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Pizarro	Pizarro	k1gNnSc4	Pizarro
zaštiťoval	zaštiťovat	k5eAaImAgInS	zaštiťovat
právem	právem	k6eAd1	právem
<g/>
.	.	kIx.	.
</s>
<s>
Obměkčit	obměkčit	k5eAaPmF	obměkčit
se	se	k3xPyFc4	se
Karel	Karel	k1gMnSc1	Karel
nechal	nechat	k5eAaPmAgInS	nechat
až	až	k9	až
pokladem	poklad	k1gInSc7	poklad
z	z	k7c2	z
nově	nově	k6eAd1	nově
dobytého	dobytý	k2eAgNnSc2d1	dobyté
území	území	k1gNnSc2	území
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
světě	svět	k1gInSc6	svět
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
2	[number]	k4	2
milionů	milion	k4xCgInPc2	milion
zlatých	zlatý	k2eAgInPc2d1	zlatý
dukátů	dukát	k1gInPc2	dukát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
použil	použít	k5eAaPmAgInS	použít
na	na	k7c6	na
financování	financování	k1gNnSc6	financování
tuniského	tuniský	k2eAgNnSc2d1	tuniské
tažení	tažení	k1gNnSc2	tažení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1535	[number]	k4	1535
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
plány	plán	k1gInPc4	plán
sledoval	sledovat	k5eAaImAgMnS	sledovat
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
vytrvalostí	vytrvalost	k1gFnSc7	vytrvalost
a	a	k8xC	a
houževnatostí	houževnatost	k1gFnSc7	houževnatost
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k3yInSc1	co
si	se	k3xPyFc3	se
umínil	umínit	k5eAaPmAgMnS	umínit
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
rozvážil	rozvážit	k5eAaPmAgInS	rozvážit
své	svůj	k3xOyFgInPc4	svůj
cíle	cíl	k1gInPc4	cíl
<g/>
,	,	kIx,	,
sledoval	sledovat	k5eAaImAgMnS	sledovat
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
zvolna	zvolna	k6eAd1	zvolna
a	a	k8xC	a
opatrně	opatrně	k6eAd1	opatrně
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
nedával	dávat	k5eNaImAgInS	dávat
se	se	k3xPyFc4	se
od	od	k7c2	od
nich	on	k3xPp3gInPc2	on
odvrátit	odvrátit	k5eAaPmF	odvrátit
žádnými	žádný	k3yNgFnPc7	žádný
překážkami	překážka	k1gFnPc7	překážka
či	či	k8xC	či
nezdarem	nezdar	k1gInSc7	nezdar
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
vládní	vládní	k2eAgFnSc2d1	vládní
rutiny	rutina	k1gFnSc2	rutina
se	se	k3xPyFc4	se
vpravoval	vpravovat	k5eAaImAgMnS	vpravovat
zvolna	zvolna	k6eAd1	zvolna
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c6	po
několika	několik	k4yIc6	několik
letech	léto	k1gNnPc6	léto
panování	panování	k1gNnSc2	panování
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
následně	následně	k6eAd1	následně
vždy	vždy	k6eAd1	vždy
bedlivě	bedlivě	k6eAd1	bedlivě
dbal	dbát	k5eAaImAgMnS	dbát
svých	svůj	k3xOyFgFnPc2	svůj
povinností	povinnost	k1gFnPc2	povinnost
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
státnické	státnický	k2eAgNnSc4d1	státnické
nadání	nadání	k1gNnSc4	nadání
a	a	k8xC	a
nebyly	být	k5eNaImAgInP	být
mu	on	k3xPp3gMnSc3	on
cizí	cizí	k2eAgInPc1d1	cizí
politické	politický	k2eAgInPc1d1	politický
úskoky	úskok	k1gInPc1	úskok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInPc4	jeho
plány	plán	k1gInPc4	plán
na	na	k7c4	na
obnovení	obnovení	k1gNnSc4	obnovení
císařství	císařství	k1gNnSc2	císařství
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
středověké	středověký	k2eAgFnSc2d1	středověká
svrchovanosti	svrchovanost	k1gFnSc2	svrchovanost
nad	nad	k7c7	nad
celým	celý	k2eAgInSc7d1	celý
světem	svět	k1gInSc7	svět
se	se	k3xPyFc4	se
již	již	k9	již
nehodily	hodit	k5eNaPmAgFnP	hodit
do	do	k7c2	do
jeho	jeho	k3xOp3gMnPc2	jeho
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
zase	zase	k9	zase
neměl	mít	k5eNaImAgMnS	mít
porozumění	porozumění	k1gNnSc4	porozumění
pro	pro	k7c4	pro
zájmy	zájem	k1gInPc4	zájem
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
národů	národ	k1gInPc2	národ
a	a	k8xC	a
částí	část	k1gFnSc7	část
svého	své	k1gNnSc2	své
rozsáhlého	rozsáhlý	k2eAgNnSc2d1	rozsáhlé
panství	panství	k1gNnSc2	panství
<g/>
.	.	kIx.	.
</s>
<s>
Určit	určit	k5eAaPmF	určit
jeho	jeho	k3xOp3gFnSc4	jeho
národnost	národnost	k1gFnSc4	národnost
je	být	k5eAaImIp3nS	být
nesnadné	snadný	k2eNgNnSc1d1	nesnadné
<g/>
:	:	kIx,	:
první	první	k4xOgFnSc1	první
řeč	řeč	k1gFnSc1	řeč
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
mluvil	mluvit	k5eAaImAgMnS	mluvit
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
francouzština	francouzština	k1gFnSc1	francouzština
a	a	k8xC	a
vedle	vedle	k7c2	vedle
španělštiny	španělština	k1gFnSc2	španělština
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
přiučil	přiučit	k5eAaPmAgMnS	přiučit
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
sloužila	sloužit	k5eAaImAgFnS	sloužit
do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
jako	jako	k8xS	jako
řeč	řeč	k1gFnSc4	řeč
nejoblíbenější	oblíbený	k2eAgFnSc4d3	nejoblíbenější
<g/>
;	;	kIx,	;
německy	německy	k6eAd1	německy
se	se	k3xPyFc4	se
zato	zato	k6eAd1	zato
nikdy	nikdy	k6eAd1	nikdy
pořádně	pořádně	k6eAd1	pořádně
nenaučil	naučit	k5eNaPmAgMnS	naučit
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
sám	sám	k3xTgMnSc1	sám
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Španělsky	španělsky	k6eAd1	španělsky
mluvím	mluvit	k5eAaImIp1nS	mluvit
k	k	k7c3	k
Bohu	bůh	k1gMnSc3	bůh
<g/>
,	,	kIx,	,
italsky	italsky	k6eAd1	italsky
k	k	k7c3	k
ženám	žena	k1gFnPc3	žena
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
k	k	k7c3	k
mužům	muž	k1gMnPc3	muž
a	a	k8xC	a
německy	německy	k6eAd1	německy
ke	k	k7c3	k
svému	svůj	k3xOyFgMnSc3	svůj
koni	kůň	k1gMnSc3	kůň
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
