<p>
<s>
Italská	italský	k2eAgFnSc1d1	italská
literatura	literatura	k1gFnSc1	literatura
je	být	k5eAaImIp3nS	být
literatura	literatura	k1gFnSc1	literatura
psaná	psaný	k2eAgFnSc1d1	psaná
v	v	k7c6	v
italštině	italština	k1gFnSc6	italština
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
území	území	k1gNnSc6	území
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
zařazujeme	zařazovat	k5eAaImIp1nP	zařazovat
literaturu	literatura	k1gFnSc4	literatura
psanou	psaný	k2eAgFnSc4d1	psaná
Italy	Ital	k1gMnPc4	Ital
nebo	nebo	k8xC	nebo
literaturu	literatura	k1gFnSc4	literatura
napsanou	napsaný	k2eAgFnSc4d1	napsaná
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
v	v	k7c6	v
těch	ten	k3xDgInPc6	ten
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
úzce	úzko	k6eAd1	úzko
spjaty	spjat	k2eAgFnPc4d1	spjata
s	s	k7c7	s
moderní	moderní	k2eAgFnSc7d1	moderní
italštinou	italština	k1gFnSc7	italština
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
příkladů	příklad	k1gInPc2	příklad
italské	italský	k2eAgFnSc2d1	italská
literatury	literatura	k1gFnSc2	literatura
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgFnSc1d1	tradiční
lyrická	lyrický	k2eAgFnSc1d1	lyrická
poezie	poezie	k1gFnSc1	poezie
psaná	psaný	k2eAgFnSc1d1	psaná
v	v	k7c6	v
okcitánštině	okcitánština	k1gFnSc6	okcitánština
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
pronikla	proniknout	k5eAaPmAgFnS	proniknout
do	do	k7c2	do
konce	konec	k1gInSc2	konec
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1230	[number]	k4	1230
se	se	k3xPyFc4	se
sicilská	sicilský	k2eAgFnSc1d1	sicilská
básnická	básnický	k2eAgFnSc1d1	básnická
škola	škola	k1gFnSc1	škola
stala	stát	k5eAaPmAgFnS	stát
pozoruhodnou	pozoruhodný	k2eAgFnSc4d1	pozoruhodná
jako	jako	k8xC	jako
první	první	k4xOgInSc4	první
ve	v	k7c6	v
standardní	standardní	k2eAgFnSc6d1	standardní
italštině	italština	k1gFnSc6	italština
<g/>
.	.	kIx.	.
</s>
<s>
Dante	Dante	k1gMnSc1	Dante
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
italských	italský	k2eAgMnPc2d1	italský
básníků	básník	k1gMnPc2	básník
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pozoruhodný	pozoruhodný	k2eAgInSc1d1	pozoruhodný
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
Božské	božský	k2eAgFnSc3d1	božská
komedii	komedie	k1gFnSc3	komedie
<g/>
.	.	kIx.	.
</s>
<s>
Francesco	Francesco	k1gMnSc1	Francesco
Petrarca	Petrarca	k1gMnSc1	Petrarca
prováděl	provádět	k5eAaImAgMnS	provádět
klasický	klasický	k2eAgInSc4d1	klasický
výzkum	výzkum	k1gInSc4	výzkum
a	a	k8xC	a
psal	psát	k5eAaImAgMnS	psát
lyriku	lyrika	k1gFnSc4	lyrika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vpadli	vpadnout	k5eAaPmAgMnP	vpadnout
severští	severský	k2eAgMnPc1d1	severský
dobyvatelé	dobyvatel	k1gMnPc1	dobyvatel
na	na	k7c6	na
území	území	k1gNnSc6	území
rozpadající	rozpadající	k2eAgFnSc1d1	rozpadající
se	se	k3xPyFc4	se
Římského	římský	k2eAgNnSc2d1	římské
impéria	impérium	k1gNnSc2	impérium
<g/>
.	.	kIx.	.
</s>
<s>
Římská	římský	k2eAgFnSc1d1	římská
říše	říše	k1gFnSc1	říše
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
latinská	latinský	k2eAgFnSc1d1	Latinská
tradice	tradice	k1gFnSc1	tradice
se	se	k3xPyFc4	se
držela	držet	k5eAaImAgFnS	držet
při	při	k7c6	při
životě	život	k1gInSc6	život
díky	díky	k7c3	díky
Cassiodoriovi	Cassiodorius	k1gMnSc3	Cassiodorius
<g/>
,	,	kIx,	,
Boetiovi	Boetius	k1gMnSc3	Boetius
<g/>
,	,	kIx,	,
Symmachovi	Symmach	k1gMnSc3	Symmach
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Ravenně	Ravenně	k1gFnSc6	Ravenně
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
pod	pod	k7c4	pod
Theodorich	Theodorich	k1gInSc4	Theodorich
Velkým	velký	k2eAgInSc7d1	velký
nové	nový	k2eAgNnSc1d1	nové
království	království	k1gNnSc3	království
Gótů	Gót	k1gMnPc2	Gót
<g/>
.	.	kIx.	.
</s>
<s>
Svobodná	svobodný	k2eAgNnPc1d1	svobodné
umění	umění	k1gNnPc1	umění
vzkvétala	vzkvétat	k5eAaImAgNnP	vzkvétat
<g/>
,	,	kIx,	,
gótští	gótský	k2eAgMnPc1d1	gótský
králové	král	k1gMnPc1	král
se	se	k3xPyFc4	se
obklopovali	obklopovat	k5eAaImAgMnP	obklopovat
mistry	mistr	k1gMnPc7	mistr
rétoriky	rétorika	k1gFnSc2	rétorika
a	a	k8xC	a
gramatiky	gramatika	k1gFnSc2	gramatika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
také	také	k9	také
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
některé	některý	k3yIgFnPc1	některý
školy	škola	k1gFnPc1	škola
ateistické	ateistický	k2eAgFnPc1d1	ateistická
a	a	k8xC	a
právě	právě	k9	právě
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
vyrostlo	vyrůst	k5eAaPmAgNnS	vyrůst
několik	několik	k4yIc1	několik
skutečně	skutečně	k6eAd1	skutečně
výjimečných	výjimečný	k2eAgMnPc2d1	výjimečný
lidí	člověk	k1gMnPc2	člověk
jako	jako	k9	jako
Magnus	Magnus	k1gMnSc1	Magnus
Felix	Felix	k1gMnSc1	Felix
Ennodius	Ennodius	k1gMnSc1	Ennodius
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
spíše	spíše	k9	spíše
pohanský	pohanský	k2eAgMnSc1d1	pohanský
než	než	k8xS	než
křesťanský	křesťanský	k2eAgMnSc1d1	křesťanský
<g/>
,	,	kIx,	,
Arator	Arator	k1gMnSc1	Arator
<g/>
,	,	kIx,	,
Venantius	Venantius	k1gMnSc1	Venantius
Fortunatus	Fortunatus	k1gMnSc1	Fortunatus
<g/>
,	,	kIx,	,
Venantius	Venantius	k1gMnSc1	Venantius
Jovannicius	Jovannicius	k1gMnSc1	Jovannicius
<g/>
,	,	kIx,	,
Felix	Felix	k1gMnSc1	Felix
Gramatik	gramatik	k1gMnSc1	gramatik
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
z	z	k7c2	z
Pisy	Pisa	k1gFnSc2	Pisa
<g/>
,	,	kIx,	,
Paulinus	Paulinus	k1gMnSc1	Paulinus
z	z	k7c2	z
Aquilei	Aquilei	k1gNnSc2	Aquilei
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
jiných	jiný	k2eAgInPc2d1	jiný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Italové	Ital	k1gMnPc1	Ital
si	se	k3xPyFc3	se
nikdy	nikdy	k6eAd1	nikdy
nelibovali	libovat	k5eNaImAgMnP	libovat
v	v	k7c6	v
teologickém	teologický	k2eAgNnSc6d1	teologické
studiu	studio	k1gNnSc6	studio
a	a	k8xC	a
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
záviseli	záviset	k5eAaImAgMnP	záviset
<g/>
,	,	kIx,	,
preferovali	preferovat	k5eAaImAgMnP	preferovat
Paříž	Paříž	k1gFnSc4	Paříž
před	před	k7c7	před
Itálií	Itálie	k1gFnSc7	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Italy	Ital	k1gMnPc4	Ital
přitahovalo	přitahovat	k5eAaImAgNnS	přitahovat
něco	něco	k6eAd1	něco
praktičtější	praktický	k2eAgMnSc1d2	praktičtější
<g/>
,	,	kIx,	,
pozitivnější	pozitivní	k2eAgMnSc1d2	pozitivnější
-	-	kIx~	-
obzvláště	obzvláště	k6eAd1	obzvláště
studium	studium	k1gNnSc1	studium
římského	římský	k2eAgNnSc2d1	římské
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
přispělo	přispět	k5eAaPmAgNnS	přispět
pozdějšímu	pozdní	k2eAgNnSc3d2	pozdější
založení	založení	k1gNnSc3	založení
středověkých	středověký	k2eAgFnPc2d1	středověká
univerzit	univerzita	k1gFnPc2	univerzita
ve	v	k7c6	v
městech	město	k1gNnPc6	město
Bologna	Bologna	k1gFnSc1	Bologna
<g/>
,	,	kIx,	,
Padova	Padova	k1gFnSc1	Padova
<g/>
,	,	kIx,	,
Vicenza	Vicenza	k1gFnSc1	Vicenza
<g/>
,	,	kIx,	,
Neapol	Neapol	k1gFnSc1	Neapol
<g/>
,	,	kIx,	,
Salerno	Salerna	k1gFnSc5	Salerna
<g/>
,	,	kIx,	,
Modena	Modena	k1gFnSc1	Modena
a	a	k8xC	a
Parma	Parma	k1gFnSc1	Parma
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
zase	zase	k9	zase
pomáhaly	pomáhat	k5eAaImAgFnP	pomáhat
šířit	šířit	k5eAaImF	šířit
kulturu	kultura	k1gFnSc4	kultura
a	a	k8xC	a
připravovat	připravovat	k5eAaImF	připravovat
půdu	půda	k1gFnSc4	půda
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
nové	nový	k2eAgFnSc2d1	nová
literatury	literatura	k1gFnSc2	literatura
v	v	k7c6	v
lidové	lidový	k2eAgFnSc6d1	lidová
řeči	řeč	k1gFnSc6	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Přetrvávání	přetrvávání	k1gNnSc1	přetrvávání
klasických	klasický	k2eAgFnPc2d1	klasická
tradic	tradice	k1gFnPc2	tradice
<g/>
,	,	kIx,	,
zanícení	zanícení	k1gNnPc2	zanícení
pro	pro	k7c4	pro
památky	památka	k1gFnPc4	památka
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
politické	politický	k2eAgInPc4d1	politický
zájmy	zájem	k1gInPc4	zájem
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
projevily	projevit	k5eAaPmAgInP	projevit
ve	v	k7c6	v
válkách	válka	k1gFnPc6	válka
lombarských	lombarský	k2eAgFnPc2d1	lombarský
komun	komuna	k1gFnPc2	komuna
proti	proti	k7c3	proti
říši	říš	k1gFnSc3	říš
Štaufů	Štauf	k1gMnPc2	Štauf
<g/>
,	,	kIx,	,
duch	duch	k1gMnSc1	duch
inklinující	inklinující	k2eAgFnSc2d1	inklinující
více	hodně	k6eAd2	hodně
k	k	k7c3	k
praxi	praxe	k1gFnSc3	praxe
než	než	k8xS	než
k	k	k7c3	k
teorii	teorie	k1gFnSc3	teorie
-	-	kIx~	-
toto	tento	k3xDgNnSc1	tento
vše	všechen	k3xTgNnSc1	všechen
mělo	mít	k5eAaImAgNnS	mít
silný	silný	k2eAgInSc4d1	silný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
osud	osud	k1gInSc4	osud
italské	italský	k2eAgFnSc2d1	italská
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zatímco	zatímco	k8xS	zatímco
se	se	k3xPyFc4	se
legendy	legenda	k1gFnPc1	legenda
<g/>
,	,	kIx,	,
příběhy	příběh	k1gInPc1	příběh
<g/>
,	,	kIx,	,
epické	epický	k2eAgFnPc1d1	epická
básně	báseň	k1gFnPc1	báseň
a	a	k8xC	a
satira	satira	k1gFnSc1	satira
objevovaly	objevovat	k5eAaImAgFnP	objevovat
a	a	k8xC	a
šířily	šířit	k5eAaImAgFnP	šířit
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
Itálii	Itálie	k1gFnSc6	Itálie
byly	být	k5eAaImAgFnP	být
tato	tento	k3xDgNnPc1	tento
hnutí	hnutí	k1gNnPc1	hnutí
buď	buď	k8xC	buď
cizí	cizí	k2eAgFnPc1d1	cizí
nebo	nebo	k8xC	nebo
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
hrály	hrát	k5eAaImAgFnP	hrát
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Známe	znát	k5eAaImIp1nP	znát
například	například	k6eAd1	například
čím	co	k3yQnSc7	co
byly	být	k5eAaImAgFnP	být
trojské	trojský	k2eAgFnPc4d1	Trojská
tradice	tradice	k1gFnPc4	tradice
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
-	-	kIx~	-
a	a	k8xC	a
měli	mít	k5eAaImAgMnP	mít
bychom	by	kYmCp1nP	by
si	se	k3xPyFc3	se
myslet	myslet	k5eAaImF	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
-	-	kIx~	-
zemi	zem	k1gFnSc6	zem
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
přetrvávající	přetrvávající	k2eAgFnSc2d1	přetrvávající
památky	památka	k1gFnSc2	památka
Aeneas	Aeneas	k1gMnSc1	Aeneas
<g/>
,	,	kIx,	,
Vergilius	Vergilius	k1gMnSc1	Vergilius
-	-	kIx~	-
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
obzvlášť	obzvlášť	k6eAd1	obzvlášť
rozvinuté	rozvinutý	k2eAgFnPc4d1	rozvinutá
<g/>
.	.	kIx.	.
</s>
<s>
Nebylo	být	k5eNaImAgNnS	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
tak	tak	k6eAd1	tak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
nikdo	nikdo	k3yNnSc1	nikdo
přesně	přesně	k6eAd1	přesně
neví	vědět	k5eNaImIp3nS	vědět
kdy	kdy	k6eAd1	kdy
<g/>
,	,	kIx,	,
Historia	Historium	k1gNnSc2	Historium
de	de	k?	de
excidio	excidio	k1gMnSc1	excidio
Troiae	Troia	k1gInSc2	Troia
napsána	napsán	k2eAgFnSc1d1	napsána
údajně	údajně	k6eAd1	údajně
jistým	jistý	k2eAgInSc7d1	jistý
Darésem	Darés	k1gInSc7	Darés
<g/>
,	,	kIx,	,
očitým	očitý	k2eAgMnSc7d1	očitý
svědkem	svědek	k1gMnSc7	svědek
trojské	trojský	k2eAgFnSc2d1	Trojská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
tato	tento	k3xDgFnSc1	tento
kniha	kniha	k1gFnSc1	kniha
základem	základ	k1gInSc7	základ
mnoha	mnoho	k4c2	mnoho
prací	práce	k1gFnPc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Benoît	Benoît	k1gInSc1	Benoît
de	de	k?	de
Sainte-Maure	Sainte-Maur	k1gMnSc5	Sainte-Maur
sestavil	sestavit	k5eAaPmAgMnS	sestavit
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
základě	základ	k1gInSc6	základ
velmi	velmi	k6eAd1	velmi
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
báseň	báseň	k1gFnSc4	báseň
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zase	zase	k9	zase
stala	stát	k5eAaPmAgFnS	stát
zdrojem	zdroj	k1gInSc7	zdroj
pro	pro	k7c4	pro
jiné	jiný	k2eAgMnPc4d1	jiný
básníky	básník	k1gMnPc4	básník
jako	jako	k8xS	jako
Herbort	Herbort	k1gInSc4	Herbort
von	von	k1gInSc1	von
Fritzlar	Fritzlar	k1gInSc1	Fritzlar
a	a	k8xC	a
Konrad	Konrad	k1gInSc1	Konrad
von	von	k1gInSc1	von
Würzburg	Würzburg	k1gInSc1	Würzburg
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
k	k	k7c3	k
italskému	italský	k2eAgInSc3d1	italský
fenoménu	fenomén	k1gInSc3	fenomén
-	-	kIx~	-
zatímo	zatímo	k6eAd1	zatímo
Benoît	Benoît	k1gInSc1	Benoît
de	de	k?	de
Sainte-More	Sainte-Mor	k1gInSc5	Sainte-Mor
napsal	napsat	k5eAaBmAgInS	napsat
svou	svůj	k3xOyFgFnSc4	svůj
báseň	báseň	k1gFnSc4	báseň
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
<g/>
,	,	kIx,	,
berouc	brát	k5eAaImSgFnS	brát
materiál	materiál	k1gInSc4	materiál
z	z	k7c2	z
latinské	latinský	k2eAgFnSc2d1	Latinská
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
dva	dva	k4xCgMnPc1	dva
němečtí	německý	k2eAgMnPc1d1	německý
spisovatelé	spisovatel	k1gMnPc1	spisovatel
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
jazyce	jazyk	k1gInSc6	jazyk
téměř	téměř	k6eAd1	téměř
originální	originální	k2eAgNnSc4d1	originální
dílo	dílo	k1gNnSc4	dílo
-	-	kIx~	-
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
Ital	Ital	k1gMnSc1	Ital
podle	podle	k7c2	podle
Benoita	Benoitum	k1gNnSc2	Benoitum
sestavil	sestavit	k5eAaPmAgMnS	sestavit
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
Historia	Historium	k1gNnSc2	Historium
Destruction	Destruction	k1gInSc4	Destruction
Troiae	Troiae	k1gNnSc2	Troiae
<g/>
;	;	kIx,	;
byl	být	k5eAaImAgMnS	být
jím	jíst	k5eAaImIp1nS	jíst
Guido	Guido	k1gNnSc4	Guido
delle	delle	k1gFnSc2	delle
Colonne	Colonn	k1gInSc5	Colonn
z	z	k7c2	z
Messiny	Messina	k1gFnSc2	Messina
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
básníků	básník	k1gMnPc2	básník
Sicilské	sicilský	k2eAgFnSc2d1	sicilská
školy	škola	k1gFnSc2	škola
píšících	píšící	k2eAgMnPc2d1	píšící
v	v	k7c6	v
lidovém	lidový	k2eAgInSc6d1	lidový
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Guido	Guido	k1gNnSc4	Guido
byl	být	k5eAaImAgMnS	být
imitátor	imitátor	k1gMnSc1	imitátor
provensálštiny	provensálština	k1gFnSc2	provensálština
<g/>
;	;	kIx,	;
rozuměl	rozumět	k5eAaImAgMnS	rozumět
francouzsky	francouzsky	k6eAd1	francouzsky
a	a	k8xC	a
přesto	přesto	k8xC	přesto
napsal	napsat	k5eAaBmAgMnS	napsat
svou	svůj	k3xOyFgFnSc4	svůj
knihu	kniha	k1gFnSc4	kniha
latinsky	latinsky	k6eAd1	latinsky
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
změnil	změnit	k5eAaPmAgInS	změnit
trubadúrskou	trubadúrský	k2eAgFnSc4d1	trubadúrská
romanci	romance	k1gFnSc4	romance
na	na	k7c6	na
seriózní	seriózní	k2eAgFnSc6d1	seriózní
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stejně	stejně	k6eAd1	stejně
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
velkými	velký	k2eAgFnPc7d1	velká
legendami	legenda	k1gFnPc7	legenda
<g/>
.	.	kIx.	.
</s>
<s>
Legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
Alexandrovi	Alexandr	k1gMnSc6	Alexandr
Velkém	velký	k2eAgMnSc6d1	velký
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
jeho	jeho	k3xOp3gNnSc1	jeho
latinské	latinský	k2eAgNnSc1d1	latinské
dvojče	dvojče	k1gNnSc1	dvojče
Qualichino	Qualichin	k2eAgNnSc1d1	Qualichin
z	z	k7c2	z
Arezza	Arezz	k1gMnSc2	Arezz
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
Evropa	Evropa	k1gFnSc1	Evropa
byla	být	k5eAaImAgFnS	být
plná	plný	k2eAgFnSc1d1	plná
Krále	Král	k1gMnSc2	Král
Artuše	Artuš	k1gMnSc2	Artuš
<g/>
.	.	kIx.	.
</s>
<s>
Italové	Ital	k1gMnPc1	Ital
se	se	k3xPyFc4	se
spokojili	spokojit	k5eAaPmAgMnP	spokojit
s	s	k7c7	s
překladem	překlad	k1gInSc7	překlad
a	a	k8xC	a
zkrácením	zkrácení	k1gNnSc7	zkrácení
francouzských	francouzský	k2eAgInPc2d1	francouzský
románů	román	k1gInPc2	román
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
i	i	k9	i
náboženské	náboženský	k2eAgFnPc1d1	náboženská
legendy	legenda	k1gFnPc1	legenda
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
pouze	pouze	k6eAd1	pouze
slabé	slabý	k2eAgInPc4d1	slabý
kořeny	kořen	k1gInPc4	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Jacobus	Jacobus	k1gInSc1	Jacobus
de	de	k?	de
Voragine	Voragin	k1gInSc5	Voragin
sbírající	sbírající	k2eAgInPc4d1	sbírající
své	svůj	k3xOyFgInPc4	svůj
životy	život	k1gInPc4	život
svatých	svatá	k1gFnPc2	svatá
zůstal	zůstat	k5eAaPmAgInS	zůstat
pouze	pouze	k6eAd1	pouze
historikem	historik	k1gMnSc7	historik
<g/>
,	,	kIx,	,
učencům	učenec	k1gMnPc3	učenec
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
kritikem	kritik	k1gMnSc7	kritik
pochybujícím	pochybující	k2eAgFnPc3d1	pochybující
o	o	k7c6	o
věcech	věc	k1gFnPc6	věc
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
popisoval	popisovat	k5eAaImAgMnS	popisovat
<g/>
.	.	kIx.	.
</s>
<s>
Itálie	Itálie	k1gFnSc2	Itálie
nemělo	mít	k5eNaImAgNnS	mít
žádnou	žádný	k3yNgFnSc7	žádný
z	z	k7c2	z
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgFnPc6	který
je	být	k5eAaImIp3nS	být
středověk	středověk	k1gInSc1	středověk
tak	tak	k6eAd1	tak
zvláštně	zvláštně	k6eAd1	zvláštně
zobrazen	zobrazit	k5eAaPmNgInS	zobrazit
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k9	už
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
asketickém	asketický	k2eAgNnSc6d1	asketické
nebo	nebo	k8xC	nebo
rytířském	rytířský	k2eAgNnSc6d1	rytířské
pojetí	pojetí	k1gNnSc6	pojetí
<g/>
.	.	kIx.	.
</s>
<s>
Intelektuální	intelektuální	k2eAgInSc1d1	intelektuální
život	život	k1gInSc1	život
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
se	se	k3xPyFc4	se
projevil	projevit	k5eAaPmAgInS	projevit
ve	v	k7c4	v
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
<g/>
,	,	kIx,	,
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
vědecké	vědecký	k2eAgFnSc6d1	vědecká
formě	forma	k1gFnSc6	forma
<g/>
,	,	kIx,	,
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
římského	římský	k2eAgNnSc2d1	římské
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
v	v	k7c6	v
záznamech	záznam	k1gInPc6	záznam
Farfa	Farf	k1gMnSc2	Farf
<g/>
,	,	kIx,	,
Marsicana	Marsican	k1gMnSc2	Marsican
a	a	k8xC	a
jiných	jiný	k2eAgMnPc2d1	jiný
<g/>
,	,	kIx,	,
v	v	k7c6	v
překladech	překlad	k1gInPc6	překlad
Aristotela	Aristoteles	k1gMnSc2	Aristoteles
<g/>
,	,	kIx,	,
v	v	k7c6	v
učeních	učení	k1gNnPc6	učení
salernské	salernský	k2eAgFnSc2d1	salernský
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
v	v	k7c6	v
Milionu	milion	k4xCgInSc6	milion
Marca	Marca	k1gMnSc1	Marca
Pola	pola	k1gFnSc1	pola
<g/>
,	,	kIx,	,
v	v	k7c6	v
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
sérii	série	k1gFnSc6	série
faktů	fakt	k1gInPc2	fakt
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
zdají	zdát	k5eAaImIp3nP	zdát
být	být	k5eAaImF	být
odděleny	oddělit	k5eAaPmNgInP	oddělit
od	od	k7c2	od
faktů	fakt	k1gInPc2	fakt
středověku	středověk	k1gInSc2	středověk
a	a	k8xC	a
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
sjednocené	sjednocený	k2eAgFnSc2d1	sjednocená
s	s	k7c7	s
klasickým	klasický	k2eAgInSc7d1	klasický
Římem	Řím	k1gInSc7	Řím
a	a	k8xC	a
renesancí	renesance	k1gFnSc7	renesance
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Latina	latina	k1gFnSc1	latina
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
měla	mít	k5eAaImAgFnS	mít
neměnnost	neměnnost	k1gFnSc1	neměnnost
a	a	k8xC	a
vývoj	vývoj	k1gInSc1	vývoj
nového	nový	k2eAgInSc2d1	nový
národního	národní	k2eAgInSc2d1	národní
jazyka	jazyk	k1gInSc2	jazyk
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
pomalý	pomalý	k2eAgMnSc1d1	pomalý
<g/>
,	,	kIx,	,
předcházely	předcházet	k5eAaImAgFnP	předcházet
mu	on	k3xPp3gMnSc3	on
dvě	dva	k4xCgNnPc4	dva
období	období	k1gNnPc2	období
italské	italský	k2eAgFnSc2d1	italská
literatury	literatura	k1gFnSc2	literatura
v	v	k7c6	v
cizích	cizí	k2eAgInPc6d1	cizí
jazycích	jazyk	k1gInPc6	jazyk
a	a	k8xC	a
francouzštině	francouzština	k1gFnSc6	francouzština
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
Italové	Ital	k1gMnPc1	Ital
jako	jako	k8xS	jako
Albert	Albert	k1gMnSc1	Albert
Malaspina	Malaspina	k1gFnSc1	Malaspina
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ferrarino	Ferrarino	k1gNnSc1	Ferrarino
Trogni	Trogn	k1gMnPc1	Trogn
da	da	k?	da
Ferrara	Ferrara	k1gFnSc1	Ferrara
<g/>
,	,	kIx,	,
Lanfranc	Lanfranc	k1gFnSc1	Lanfranc
Cigala	Cigala	k1gFnSc1	Cigala
<g/>
,	,	kIx,	,
Bertolome	Bertolom	k1gInSc5	Bertolom
Zorzi	Zorze	k1gFnSc6	Zorze
<g/>
,	,	kIx,	,
Sordello	Sordello	k1gNnSc1	Sordello
<g/>
,	,	kIx,	,
Rambertino	Rambertin	k2eAgNnSc1d1	Rambertin
Buvalelli	Buvalelle	k1gFnSc4	Buvalelle
<g/>
,	,	kIx,	,
Nicoletto	Nicolett	k2eAgNnSc1d1	Nicolett
da	da	k?	da
Torino	Torino	k1gNnSc1	Torino
psali	psát	k5eAaImAgMnP	psát
básně	báseň	k1gFnPc4	báseň
v	v	k7c6	v
provensálštině	provensálština	k1gFnSc6	provensálština
opěvující	opěvující	k2eAgFnSc4d1	opěvující
válku	válka	k1gFnSc4	válka
a	a	k8xC	a
lásku	láska	k1gFnSc4	láska
<g/>
.	.	kIx.	.
</s>
<s>
Žili	žít	k5eAaImAgMnP	žít
na	na	k7c6	na
dvorech	dvůr	k1gInPc6	dvůr
nebo	nebo	k8xC	nebo
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
navykajíc	navykat	k5eAaImSgFnS	navykat
je	on	k3xPp3gFnPc4	on
na	na	k7c4	na
nové	nový	k2eAgInPc4d1	nový
zvuky	zvuk	k1gInPc4	zvuk
a	a	k8xC	a
harmonie	harmonie	k1gFnPc4	harmonie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
téže	tenže	k3xDgFnSc6	tenže
době	doba	k1gFnSc6	doba
existovala	existovat	k5eAaImAgFnS	existovat
i	i	k9	i
jiná	jiný	k2eAgFnSc1d1	jiná
poezie	poezie	k1gFnSc1	poezie
epického	epický	k2eAgInSc2d1	epický
druhu	druh	k1gInSc2	druh
psaná	psaný	k2eAgFnSc1d1	psaná
v	v	k7c6	v
smíšeném	smíšený	k2eAgInSc6d1	smíšený
jazyce	jazyk	k1gInSc6	jazyk
založeném	založený	k2eAgInSc6d1	založený
na	na	k7c6	na
francouzštině	francouzština	k1gFnSc6	francouzština
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
tvary	tvar	k1gInPc7	tvar
a	a	k8xC	a
slovy	slovo	k1gNnPc7	slovo
z	z	k7c2	z
italských	italský	k2eAgInPc2d1	italský
dialektů	dialekt	k1gInPc2	dialekt
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
soustavně	soustavně	k6eAd1	soustavně
míchaly	míchat	k5eAaImAgFnP	míchat
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
to	ten	k3xDgNnSc1	ten
pozorovat	pozorovat	k5eAaImF	pozorovat
na	na	k7c6	na
hybridních	hybridní	k2eAgNnPc6d1	hybridní
slovech	slovo	k1gNnPc6	slovo
podle	podle	k7c2	podle
zvuků	zvuk	k1gInPc2	zvuk
podle	podle	k7c2	podle
pravidel	pravidlo	k1gNnPc2	pravidlo
obou	dva	k4xCgInPc2	dva
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
francouzská	francouzský	k2eAgNnPc4d1	francouzské
slova	slovo	k1gNnPc4	slovo
s	s	k7c7	s
italskými	italský	k2eAgFnPc7d1	italská
koncovkami	koncovka	k1gFnPc7	koncovka
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
vokalizace	vokalizace	k1gFnSc2	vokalizace
ve	v	k7c6	v
slovech	slovo	k1gNnPc6	slovo
blížící	blížící	k2eAgFnSc2d1	blížící
se	se	k3xPyFc4	se
italo-latinskému	italoatinský	k2eAgNnSc3d1	italo-latinský
použití	použití	k1gNnSc3	použití
<g/>
,	,	kIx,	,
něco	něco	k3yInSc1	něco
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
patří	patřit	k5eAaImIp3nS	patřit
najednou	najednou	k6eAd1	najednou
oběma	dva	k4xCgMnPc3	dva
jazykům	jazyk	k1gMnPc3	jazyk
<g/>
,	,	kIx,	,
jakoby	jakoby	k8xS	jakoby
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
vzájemné	vzájemný	k2eAgNnSc4d1	vzájemné
překlenutí	překlenutí	k1gNnSc4	překlenutí
při	při	k7c6	při
splynutí	splynutí	k1gNnSc6	splynutí
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
sem	sem	k6eAd1	sem
chansons	chansons	k1gInSc1	chansons
de	de	k?	de
geste	gest	k1gInSc5	gest
<g/>
,	,	kIx,	,
Macaire	Macair	k1gMnSc5	Macair
<g/>
,	,	kIx,	,
Entre	Entr	k1gMnSc5	Entr
en	en	k?	en
Espagne	Espagn	k1gMnSc5	Espagn
<g/>
,	,	kIx,	,
Prise	Pris	k1gInSc6	Pris
de	de	k?	de
Pampelune	Pampelun	k1gInSc5	Pampelun
(	(	kIx(	(
<g/>
autor	autor	k1gMnSc1	autor
Niccolò	Niccolò	k1gMnSc1	Niccolò
da	da	k?	da
Verona	Verona	k1gFnSc1	Verona
<g/>
)	)	kIx)	)
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
předcházelo	předcházet	k5eAaImAgNnS	předcházet
příchodu	příchod	k1gInSc6	příchod
čistě	čistě	k6eAd1	čistě
italské	italský	k2eAgFnSc2d1	italská
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
lidové	lidový	k2eAgFnSc2d1	lidová
literatury	literatura	k1gFnSc2	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
boj	boj	k1gInSc1	boj
mezi	mezi	k7c7	mezi
francouzštinou	francouzština	k1gFnSc7	francouzština
a	a	k8xC	a
italštinou	italština	k1gFnSc7	italština
<g/>
,	,	kIx,	,
francouzština	francouzština	k1gFnSc1	francouzština
prohrávala	prohrávat	k5eAaImAgFnS	prohrávat
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
objevovaly	objevovat	k5eAaImAgInP	objevovat
hybridní	hybridní	k2eAgInPc1d1	hybridní
tvary	tvar	k1gInPc1	tvar
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k9	už
neprevažovalxy	neprevažovalx	k1gInPc4	neprevažovalx
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dílech	díl	k1gInPc6	díl
Buovo	Buovo	k1gNnSc1	Buovo
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Antona	Anton	k1gMnSc2	Anton
a	a	k8xC	a
Rainaldo	Rainaldo	k1gNnSc1	Rainaldo
e	e	k0	e
Lesengrino	Lesengrin	k2eAgNnSc1d1	Lesengrin
je	on	k3xPp3gInPc4	on
jasně	jasně	k6eAd1	jasně
cítit	cítit	k5eAaImF	cítit
benátský	benátský	k2eAgInSc4d1	benátský
dialekt	dialekt	k1gInSc4	dialekt
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
francouzskými	francouzský	k2eAgInPc7d1	francouzský
tvary	tvar	k1gInPc7	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
Graziadio	Graziadio	k6eAd1	Graziadio
Isaia	Isaia	k1gFnSc1	Isaia
Ascoli	Ascole	k1gFnSc4	Ascole
nazval	nazvat	k5eAaBmAgMnS	nazvat
miste	mísit	k5eAaImRp2nP	mísit
(	(	kIx(	(
<g/>
smíšené	smíšený	k2eAgFnSc6d1	smíšená
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přímo	přímo	k6eAd1	přímo
předcházely	předcházet	k5eAaImAgFnP	předcházet
čistě	čistě	k6eAd1	čistě
italskou	italský	k2eAgFnSc4d1	italská
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
určitý	určitý	k2eAgInSc1d1	určitý
druh	druh	k1gInSc1	druh
literatury	literatura	k1gFnSc2	literatura
existoval	existovat	k5eAaImAgInS	existovat
již	již	k6eAd1	již
před	před	k7c7	před
13	[number]	k4	13
<g/>
.	.	kIx.	.
stoletím	století	k1gNnSc7	století
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Ritmo	Ritmo	k6eAd1	Ritmo
cassinese	cassinést	k5eAaPmIp3nS	cassinést
<g/>
,	,	kIx,	,
Ritmo	Ritma	k1gFnSc5	Ritma
su	su	k?	su
Sant	Sant	k1gMnSc1	Sant
<g/>
'	'	kIx"	'
<g/>
Alessio	Alessio	k1gMnSc1	Alessio
<g/>
,	,	kIx,	,
Laudes	Laudes	k1gMnSc1	Laudes
creaturarum	creaturarum	k1gInSc1	creaturarum
<g/>
,	,	kIx,	,
Ritmo	Ritma	k1gFnSc5	Ritma
Lucchese	Lucchese	k1gFnSc5	Lucchese
<g/>
,	,	kIx,	,
Ritmo	Ritma	k1gFnSc5	Ritma
Laurenziana	Laurenzian	k1gMnSc2	Laurenzian
<g/>
,	,	kIx,	,
Ritmo	Ritma	k1gFnSc5	Ritma
Bellunese	Bellunese	k1gFnPc1	Bellunese
klasifikuje	klasifikovat	k5eAaImIp3nS	klasifikovat
Segre	Segr	k1gInSc5	Segr
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Archaická	archaický	k2eAgNnPc4d1	archaické
díla	dílo	k1gNnPc4	dílo
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Componimenti	Componiment	k1gMnPc1	Componiment
Arcaici	Arcaice	k1gFnSc6	Arcaice
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
první	první	k4xOgNnPc1	první
literární	literární	k2eAgNnPc1d1	literární
díla	dílo	k1gNnPc1	dílo
v	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
italštině	italština	k1gFnSc6	italština
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
datují	datovat	k5eAaImIp3nP	datovat
od	od	k7c2	od
posledních	poslední	k2eAgFnPc2d1	poslední
dekád	dekáda	k1gFnPc2	dekáda
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
časných	časný	k2eAgFnPc2d1	časná
dekád	dekáda	k1gFnPc2	dekáda
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Segre	Segr	k1gMnSc5	Segr
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
jak	jak	k6eAd1	jak
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
literatura	literatura	k1gFnSc1	literatura
nemá	mít	k5eNaImIp3nS	mít
zatím	zatím	k6eAd1	zatím
jednotné	jednotný	k2eAgFnPc4d1	jednotná
stylistické	stylistický	k2eAgFnPc4d1	stylistická
nebo	nebo	k8xC	nebo
lingvistické	lingvistický	k2eAgFnPc4d1	lingvistická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
raný	raný	k2eAgInSc1d1	raný
vývoj	vývoj	k1gInSc1	vývoj
však	však	k9	však
probíhal	probíhat	k5eAaImAgInS	probíhat
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
,	,	kIx,	,
rozdíl	rozdíl	k1gInSc1	rozdíl
byl	být	k5eAaImAgInS	být
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
předmětu	předmět	k1gInSc6	předmět
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
byly	být	k5eAaImAgInP	být
poeti	poet	k1gMnPc1	poet
Giacomino	Giacomin	k2eAgNnSc1d1	Giacomin
da	da	k?	da
Verona	Verona	k1gFnSc1	Verona
a	a	k8xC	a
Bonvicino	Bonvicin	k2eAgNnSc1d1	Bonvicin
da	da	k?	da
Riva	Riv	k1gInSc2	Riv
zvláště	zvláště	k6eAd1	zvláště
zbožní	zbožnit	k5eAaPmIp3nP	zbožnit
a	a	k8xC	a
měli	mít	k5eAaImAgMnP	mít
být	být	k5eAaImF	být
lidemi	lide	k1gFnPc7	lide
recitováni	recitovat	k5eAaImNgMnP	recitovat
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
psány	psát	k5eAaImNgFnP	psát
v	v	k7c6	v
dialektu	dialekt	k1gInSc6	dialekt
smíšeného	smíšený	k2eAgNnSc2d1	smíšené
z	z	k7c2	z
milánštiny	milánština	k1gFnSc2	milánština
a	a	k8xC	a
benátštiny	benátština	k1gFnSc2	benátština
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
styl	styl	k1gInSc1	styl
nesl	nést	k5eAaImAgInS	nést
silné	silný	k2eAgInPc4d1	silný
znaky	znak	k1gInPc4	znak
francouzské	francouzský	k2eAgFnSc2d1	francouzská
popisné	popisný	k2eAgFnSc2d1	popisná
poezie	poezie	k1gFnSc2	poezie
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
populární	populární	k2eAgFnSc2d1	populární
poezie	poezie	k1gFnSc2	poezie
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
smyslu	smysl	k1gInSc6	smysl
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
kompozice	kompozice	k1gFnSc2	kompozice
byl	být	k5eAaImAgInS	být
možná	možná	k9	možná
povzbuzen	povzbudit	k5eAaPmNgInS	povzbudit
starým	starý	k2eAgInSc7d1	starý
severoitalským	severoitalský	k2eAgInSc7d1	severoitalský
zvykem	zvyk	k1gInSc7	zvyk
poslouchat	poslouchat	k5eAaImF	poslouchat
na	na	k7c6	na
náměstích	náměstí	k1gNnPc6	náměstí
a	a	k8xC	a
při	při	k7c6	při
cestách	cesta	k1gFnPc6	cesta
písním	píseň	k1gFnPc3	píseň
žonglérů	žonglér	k1gMnPc2	žonglér
<g/>
.	.	kIx.	.
</s>
<s>
Stejným	stejný	k2eAgInSc7d1	stejný
davem	dav	k1gInSc7	dav
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
těšily	těšit	k5eAaImAgFnP	těšit
romantické	romantický	k2eAgInPc4d1	romantický
příběhy	příběh	k1gInPc4	příběh
a	a	k8xC	a
které	který	k3yRgNnSc4	který
poslouchali	poslouchat	k5eAaImAgMnP	poslouchat
příběhy	příběh	k1gInPc4	příběh
zkaženosti	zkaženost	k1gFnSc2	zkaženost
Macaire	Macair	k1gInSc5	Macair
a	a	k8xC	a
neštěstí	neštěstí	k1gNnSc4	neštěstí
Floris	Floris	k1gFnSc2	Floris
a	a	k8xC	a
Blancheflour	Blancheflour	k1gMnSc1	Blancheflour
<g/>
,	,	kIx,	,
jiný	jiný	k2eAgMnSc1d1	jiný
žongléř	žongléř	k1gMnSc1	žongléř
zpíval	zpívat	k5eAaImAgMnS	zpívat
o	o	k7c6	o
hrůzách	hrůza	k1gFnPc6	hrůza
Babilonia	Babilonium	k1gNnSc2	Babilonium
Infernale	Infernale	k1gFnSc2	Infernale
a	a	k8xC	a
požehnanosti	požehnanost	k1gFnSc2	požehnanost
Gerusalemme	Gerusalemme	k1gMnSc1	Gerusalemme
Eccles	Eccles	k1gMnSc1	Eccles
<g/>
,	,	kIx,	,
a	a	k8xC	a
zpěváci	zpěvák	k1gMnPc1	zpěvák
zbožné	zbožný	k2eAgFnSc2d1	zbožná
poezie	poezie	k1gFnSc2	poezie
soutěžili	soutěžit	k5eAaImAgMnP	soutěžit
v	v	k7c4	v
chansons	chansons	k1gInSc4	chansons
de	de	k?	de
geste	gest	k1gInSc5	gest
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sicilská	sicilský	k2eAgFnSc1d1	sicilská
škola	škola	k1gFnSc1	škola
==	==	k?	==
</s>
</p>
<p>
<s>
Literatura	literatura	k1gFnSc1	literatura
s	s	k7c7	s
uniformnejšími	uniformnejší	k2eAgInPc7d1	uniformnejší
znaky	znak	k1gInPc7	znak
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1230	[number]	k4	1230
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
označuje	označovat	k5eAaImIp3nS	označovat
začátek	začátek	k1gInSc4	začátek
tzv.	tzv.	kA	tzv.
Sicilské	sicilský	k2eAgFnSc2d1	sicilská
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
význam	význam	k1gInSc1	význam
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
(	(	kIx(	(
<g/>
vytvoření	vytvoření	k1gNnSc6	vytvoření
první	první	k4xOgFnSc2	první
standardní	standardní	k2eAgFnSc2d1	standardní
italštiny	italština	k1gFnSc2	italština
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
v	v	k7c6	v
předmětu	předmět	k1gInSc2	předmět
-	-	kIx~	-
písně	píseň	k1gFnPc1	píseň
o	o	k7c6	o
lásce	láska	k1gFnSc6	láska
z	z	k7c2	z
části	část	k1gFnSc2	část
tvořeny	tvořen	k2eAgInPc4d1	tvořen
na	na	k7c6	na
základě	základ	k1gInSc6	základ
poezie	poezie	k1gFnSc2	poezie
v	v	k7c6	v
provensálštině	provensálština	k1gFnSc6	provensálština
importované	importovaný	k2eAgFnPc1d1	importovaná
na	na	k7c4	na
jih	jih	k1gInSc4	jih
Normany	Norman	k1gMnPc4	Norman
a	a	k8xC	a
Šváby	Šváb	k1gMnPc4	Šváb
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Fridricha	Fridrich	k1gMnSc2	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
může	moct	k5eAaImIp3nS	moct
vypadat	vypadat	k5eAaPmF	vypadat
uměle	uměle	k6eAd1	uměle
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
poezie	poezie	k1gFnSc1	poezie
liší	lišit	k5eAaImIp3nS	lišit
od	od	k7c2	od
francouzské	francouzský	k2eAgFnSc2d1	francouzská
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
pojetí	pojetí	k1gNnSc6	pojetí
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
určitě	určitě	k6eAd1	určitě
platoničtějším	platonický	k2eAgMnPc3d2	platonický
a	a	k8xC	a
méně	málo	k6eAd2	málo
erotickém	erotický	k2eAgInSc6d1	erotický
<g/>
.	.	kIx.	.
</s>
<s>
Znak	znak	k1gInSc1	znak
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
Dolce	dolce	k6eAd1	dolce
stil	stil	k1gInSc1	stil
nově	nově	k6eAd1	nově
později	pozdě	k6eAd2	pozdě
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Bologni	Bologna	k1gFnSc6	Bologna
a	a	k8xC	a
Florencii	Florencie	k1gFnSc6	Florencie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
italských	italský	k2eAgMnPc2d1	italský
trubadurů	trubadur	k1gMnPc2	trubadur
není	být	k5eNaImIp3nS	být
ani	ani	k8xC	ani
řádek	řádek	k1gInSc1	řádek
napsán	napsat	k5eAaBmNgInS	napsat
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
<g/>
.	.	kIx.	.
</s>
<s>
Namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
běžný	běžný	k2eAgInSc4d1	běžný
repertoár	repertoár	k1gInSc4	repertoár
rytířských	rytířský	k2eAgInPc2d1	rytířský
termínů	termín	k1gInPc2	termín
přizpůsobil	přizpůsobit	k5eAaPmAgMnS	přizpůsobit
italské	italský	k2eAgFnSc3d1	italská
fonetice	fonetika	k1gFnSc3	fonetika
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nová	nový	k2eAgFnSc1d1	nová
italské	italský	k2eAgFnPc4d1	italská
slova	slovo	k1gNnPc4	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Francouzské	francouzský	k2eAgFnPc1d1	francouzská
přípony	přípona	k1gFnPc1	přípona
-iÀ	À	k?	-iÀ
a	a	k8xC	a
-ce	e	k?	-ce
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
stovky	stovka	k1gFnPc4	stovka
italských	italský	k2eAgNnPc2d1	italské
slov	slovo	k1gNnPc2	slovo
končících	končící	k2eAgNnPc2d1	končící
na	na	k7c6	na
-iera	era	k1gMnSc1	-iera
a	a	k8xC	a
-za	a	k?	-za
jako	jako	k8xS	jako
riv-íra	riv-ír	k1gMnSc4	riv-ír
<g/>
,	,	kIx,	,
costan-za	costan	k1gMnSc4	costan-z
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc4	takový
přípony	přípona	k1gFnPc4	přípona
a	a	k8xC	a
předpony	předpona	k1gFnPc4	předpona
si	se	k3xPyFc3	se
přisvojili	přisvojit	k5eAaPmAgMnP	přisvojit
Dante	Dante	k1gMnSc1	Dante
jeho	jeho	k3xOp3gInPc2	jeho
současníci	současník	k1gMnPc1	současník
a	a	k8xC	a
převzala	převzít	k5eAaPmAgFnS	převzít
je	on	k3xPp3gFnPc4	on
další	další	k2eAgFnPc4d1	další
generace	generace	k1gFnPc4	generace
italských	italský	k2eAgMnPc2d1	italský
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
této	tento	k3xDgFnSc3	tento
"	"	kIx"	"
<g/>
škole	škola	k1gFnSc6	škola
<g/>
"	"	kIx"	"
patřil	patřit	k5eAaImAgMnS	patřit
Enzo	Enzo	k1gMnSc1	Enzo
<g/>
,	,	kIx,	,
sardinský	sardinský	k2eAgMnSc1d1	sardinský
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
Pier	pier	k1gInSc1	pier
delle	dell	k1gMnSc5	dell
Vigne	Vign	k1gMnSc5	Vign
<g/>
,	,	kIx,	,
Inghilfredi	Inghilfred	k1gMnPc1	Inghilfred
<g/>
,	,	kIx,	,
Guido	Guido	k1gNnSc1	Guido
a	a	k8xC	a
Odo	Odo	k1gMnSc5	Odo
delle	dell	k1gMnSc5	dell
Colonne	Colonn	k1gMnSc5	Colonn
<g/>
,	,	kIx,	,
Jacopo	Jacopa	k1gFnSc5	Jacopa
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Aquino	Aquino	k1gNnSc1	Aquino
<g/>
,	,	kIx,	,
Rugieri	Rugieri	k1gNnSc1	Rugieri
Pugliese	Pugliese	k1gFnSc2	Pugliese
<g/>
,	,	kIx,	,
Giacomo	Giacoma	k1gFnSc5	Giacoma
da	da	k?	da
Lentini	Lentin	k2eAgMnPc1d1	Lentin
<g/>
,	,	kIx,	,
Arrigo	Arrigo	k6eAd1	Arrigo
Testa	testa	k1gFnSc1	testa
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
No	no	k9	no
m	m	kA	m
<g/>
'	'	kIx"	'
<g/>
aggio	aggio	k1gMnSc1	aggio
posto	posta	k1gFnSc5	posta
in	in	k?	in
core	cor	k1gFnPc4	cor
od	od	k7c2	od
Giacoma	Giacom	k1gMnSc2	Giacom
da	da	k?	da
Lentiniho	Lentini	k1gMnSc2	Lentini
<g/>
,	,	kIx,	,
hlavy	hlava	k1gFnSc2	hlava
hnutí	hnutí	k1gNnPc2	hnutí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
poezie	poezie	k1gFnSc1	poezie
napsaná	napsaný	k2eAgFnSc1d1	napsaná
samotným	samotný	k2eAgMnSc7d1	samotný
Fridrichem	Fridrich	k1gMnSc7	Fridrich
<g/>
.	.	kIx.	.
</s>
<s>
Giacomo	Giacomo	k6eAd1	Giacomo
da	da	k?	da
Lentinimu	Lentinim	k1gInSc2	Lentinim
se	se	k3xPyFc4	se
také	také	k9	také
připisuje	připisovat	k5eAaImIp3nS	připisovat
vynalezení	vynalezení	k1gNnSc4	vynalezení
Sonetu	sonet	k1gInSc2	sonet
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
později	pozdě	k6eAd2	pozdě
zdokonalil	zdokonalit	k5eAaPmAgMnS	zdokonalit
Dante	Dante	k1gMnSc1	Dante
a	a	k8xC	a
Petrarca	Petrarca	k1gMnSc1	Petrarca
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
poezie	poezie	k1gFnSc2	poezie
sicilské	sicilský	k2eAgFnSc2d1	sicilská
školy	škola	k1gFnSc2	škola
byla	být	k5eAaImAgFnS	být
cenzura	cenzura	k1gFnSc1	cenzura
nastolená	nastolený	k2eAgFnSc1d1	nastolená
Fridrichem	Fridrich	k1gMnSc7	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Do	do	k7c2	do
literatury	literatura	k1gFnSc2	literatura
nesměla	smět	k5eNaImAgFnS	smět
vstoupit	vstoupit	k5eAaPmF	vstoupit
žádná	žádný	k3yNgNnPc4	žádný
politická	politický	k2eAgNnPc4d1	politické
témata	téma	k1gNnPc4	téma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
byla	být	k5eAaImAgFnS	být
severní	severní	k2eAgFnSc1d1	severní
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
rozdělena	rozdělit	k5eAaPmNgNnP	rozdělit
do	do	k7c2	do
komun	komuna	k1gFnPc2	komuna
nebo	nebo	k8xC	nebo
městských	městský	k2eAgInPc2d1	městský
států	stát	k1gInPc2	stát
s	s	k7c7	s
poměrně	poměrně	k6eAd1	poměrně
demokratickou	demokratický	k2eAgFnSc7d1	demokratická
vládou	vláda	k1gFnSc7	vláda
schopna	schopen	k2eAgFnSc1d1	schopna
poskytovat	poskytovat	k5eAaImF	poskytovat
nové	nový	k2eAgFnPc4d1	nová
myšlenky	myšlenka	k1gFnPc4	myšlenka
<g/>
,	,	kIx,	,
a	a	k8xC	a
přesně	přesně	k6eAd1	přesně
to	ten	k3xDgNnSc4	ten
dělal	dělat	k5eAaImAgInS	dělat
žánr	žánr	k1gInSc1	žánr
sirventés	sirventés	k6eAd1	sirventés
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Danteho	Dante	k1gMnSc4	Dante
komedie	komedie	k1gFnSc1	komedie
<g/>
:	:	kIx,	:
jeho	jeho	k3xOp3gInPc1	jeho
řádky	řádek	k1gInPc1	řádek
jsou	být	k5eAaImIp3nP	být
plné	plný	k2eAgInPc1d1	plný
invektiv	invektiva	k1gFnPc2	invektiva
současných	současný	k2eAgMnPc2d1	současný
politických	politický	k2eAgMnPc2d1	politický
vůdců	vůdce	k1gMnPc2	vůdce
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
papežů	papež	k1gMnPc2	papež
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
během	během	k7c2	během
vlády	vláda	k1gFnSc2	vláda
Fridricha	Fridrich	k1gMnSc2	Fridrich
(	(	kIx(	(
<g/>
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Manfrediho	Manfredi	k1gMnSc2	Manfredi
<g/>
)	)	kIx)	)
převažovaly	převažovat	k5eAaImAgFnP	převažovat
konvenční	konvenční	k2eAgFnPc1d1	konvenční
písně	píseň	k1gFnPc1	píseň
o	o	k7c6	o
lásce	láska	k1gFnSc6	láska
<g/>
,	,	kIx,	,
v	v	k7c6	v
contraste	contrasit	k5eAaPmRp2nP	contrasit
(	(	kIx(	(
<g/>
za	za	k7c2	za
autora	autor	k1gMnSc2	autor
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
Cielo	Cielo	k1gNnSc1	Cielo
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Alcamo	Alcama	k1gFnSc5	Alcama
<g/>
)	)	kIx)	)
nacházíme	nacházet	k5eAaImIp1nP	nacházet
zajímavého	zajímavý	k2eAgMnSc4d1	zajímavý
reprezentanta	reprezentant	k1gMnSc4	reprezentant
spontánnější	spontánní	k2eAgFnSc2d2	spontánnější
poezie	poezie	k1gFnSc2	poezie
o	o	k7c6	o
níž	jenž	k3xRgFnSc6	jenž
samotní	samotnět	k5eAaImIp3nS	samotnět
moderní	moderní	k2eAgMnPc1d1	moderní
italští	italský	k2eAgMnPc1d1	italský
kritici	kritik	k1gMnPc1	kritik
mnoho	mnoho	k6eAd1	mnoho
napsali	napsat	k5eAaBmAgMnP	napsat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
contraste	contrasit	k5eAaPmRp2nP	contrasit
(	(	kIx(	(
<g/>
rozpor	rozpor	k1gInSc1	rozpor
<g/>
)	)	kIx)	)
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
milenci	milenec	k1gMnPc7	milenec
v	v	k7c6	v
sicilském	sicilský	k2eAgInSc6d1	sicilský
dialektu	dialekt	k1gInSc6	dialekt
jistě	jistě	k9	jistě
není	být	k5eNaImIp3nS	být
třeba	třeba	k6eAd1	třeba
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
nejstarší	starý	k2eAgFnSc4d3	nejstarší
či	či	k8xC	či
jedinou	jediný	k2eAgFnSc4d1	jediná
báseň	báseň	k1gFnSc4	báseň
populárního	populární	k2eAgInSc2d1	populární
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Bezpochyby	bezpochyby	k6eAd1	bezpochyby
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
dob	doba	k1gFnPc2	doba
vládce	vládce	k1gMnSc2	vládce
Fridricha	Fridrich	k1gMnSc2	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Ne	ne	k9	ne
později	pozdě	k6eAd2	pozdě
než	než	k8xS	než
roku	rok	k1gInSc2	rok
1250	[number]	k4	1250
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
významná	významný	k2eAgFnSc1d1	významná
jako	jako	k8xC	jako
důkaz	důkaz	k1gInSc1	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
existovala	existovat	k5eAaImAgFnS	existovat
populární	populární	k2eAgFnSc1d1	populární
poezie	poezie	k1gFnSc1	poezie
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
od	od	k7c2	od
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
kritiků	kritik	k1gMnPc2	kritik
shoduje	shodovat	k5eAaImIp3nS	shodovat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
contraste	contrasit	k5eAaImRp2nP	contrasit
Ciela	Ciel	k1gMnSc4	Ciel
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Alcamo	Alcama	k1gFnSc5	Alcama
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
učenecké	učenecký	k2eAgNnSc1d1	učenecké
přepracování	přepracování	k1gNnSc1	přepracování
některého	některý	k3yIgInSc2	některý
ztraceného	ztracený	k2eAgInSc2d1	ztracený
lidového	lidový	k2eAgInSc2d1	lidový
rýmu	rým	k1gInSc2	rým
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
nejblíže	blízce	k6eAd3	blízce
k	k	k7c3	k
druhu	druh	k1gInSc3	druh
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
nebo	nebo	k8xC	nebo
snad	snad	k9	snad
byla	být	k5eAaImAgFnS	být
pohlcena	pohltit	k5eAaPmNgFnS	pohltit
starou	starý	k2eAgFnSc7d1	stará
sicilskou	sicilský	k2eAgFnSc7d1	sicilská
literaturou	literatura	k1gFnSc7	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
rozlišovacím	rozlišovací	k2eAgNnSc7d1	rozlišovací
znamením	znamení	k1gNnSc7	znamení
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
měla	mít	k5eAaImAgFnS	mít
právě	právě	k6eAd1	právě
opačné	opačný	k2eAgFnSc2d1	opačná
kvality	kvalita	k1gFnSc2	kvalita
než	než	k8xS	než
poezie	poezie	k1gFnSc2	poezie
básníků	básník	k1gMnPc2	básník
sicilské	sicilský	k2eAgFnSc2d1	sicilská
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
diskutuje	diskutovat	k5eAaImIp3nS	diskutovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gInSc1	její
styl	styl	k1gInSc1	styl
zrazuje	zrazovat	k5eAaImIp3nS	zrazovat
hloubku	hloubka	k1gFnSc4	hloubka
znalostí	znalost	k1gFnPc2	znalost
Fridrichovy	Fridrichův	k2eAgFnSc2d1	Fridrichova
poezie	poezie	k1gFnSc2	poezie
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zde	zde	k6eAd1	zde
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
satirický	satirický	k2eAgInSc4d1	satirický
záměr	záměr	k1gInSc4	záměr
v	v	k7c6	v
mysli	mysl	k1gFnSc6	mysl
anonymní	anonymní	k2eAgFnSc6d1	anonymní
básníka	básník	k1gMnSc4	básník
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
energická	energický	k2eAgFnSc1d1	energická
ve	v	k7c6	v
vyjadřování	vyjadřování	k1gNnSc6	vyjadřování
pocitů	pocit	k1gInPc2	pocit
a	a	k8xC	a
zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejím	její	k3xOp3gInSc7	její
zdrojem	zdroj	k1gInSc7	zdroj
je	být	k5eAaImIp3nS	být
skutečný	skutečný	k2eAgInSc4d1	skutečný
sentiment	sentiment	k1gInSc4	sentiment
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
cílovém	cílový	k2eAgInSc6d1	cílový
contraste	contrasit	k5eAaPmRp2nP	contrasit
je	být	k5eAaImIp3nS	být
vše	všechen	k3xTgNnSc1	všechen
původní	původní	k2eAgMnSc1d1	původní
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
konvenčnost	konvenčnost	k1gFnSc4	konvenčnost
zde	zde	k6eAd1	zde
není	být	k5eNaImIp3nS	být
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
poznamenána	poznamenat	k5eAaPmNgFnS	poznamenat
jižanskou	jižanský	k2eAgFnSc7d1	jižanská
smyslností	smyslnost	k1gFnSc7	smyslnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bibliografie	bibliografie	k1gFnSc2	bibliografie
==	==	k?	==
</s>
</p>
<p>
<s>
Důležitými	důležitý	k2eAgMnPc7d1	důležitý
jsou	být	k5eAaImIp3nP	být
německé	německý	k2eAgFnSc2d1	německá
práce	práce	k1gFnSc2	práce
od	od	k7c2	od
Wilse	Wilse	k1gFnSc2	Wilse
a	a	k8xC	a
Percopo	Percopa	k1gFnSc5	Percopa
(	(	kIx(	(
<g/>
ilustrované	ilustrovaný	k2eAgMnPc4d1	ilustrovaný
<g/>
;	;	kIx,	;
Leipzig	Leipzig	k1gInSc1	Leipzig
<g/>
,	,	kIx,	,
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
Tommaso	Tommasa	k1gFnSc5	Tommasa
Casini	Casin	k2eAgMnPc1d1	Casin
(	(	kIx(	(
<g/>
in	in	k?	in
Grober	Grober	k1gInSc1	Grober
'	'	kIx"	'
<g/>
s	s	k7c7	s
Grundr	Grundr	k1gInSc1	Grundr
<g/>
.	.	kIx.	.
</s>
<s>
Der	drát	k5eAaImRp2nS	drát
rem	rem	k?	rem
<g/>
.	.	kIx.	.
</s>
<s>
Phil	Phil	k1gMnSc1	Phil
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Strassburg	Strassburg	k1gMnSc1	Strassburg
<g/>
,	,	kIx,	,
18961899	[number]	k4	18961899
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Anglofonní	anglofonní	k2eAgMnPc4d1	anglofonní
studenty	student	k1gMnPc4	student
odkazujeme	odkazovat	k5eAaImIp1nP	odkazovat
na	na	k7c4	na
Symondsovu	Symondsův	k2eAgFnSc4d1	Symondsův
Renaissance	Renaissance	k1gFnPc4	Renaissance
in	in	k?	in
Italy	Ital	k1gMnPc7	Ital
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
výlučně	výlučně	k6eAd1	výlučně
svazky	svazek	k1gInPc4	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Iv	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
v	v	k7c6	v
.	.	kIx.	.
<g/>
;	;	kIx,	;
nové	nový	k2eAgNnSc1d1	nové
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
<g/>
,	,	kIx,	,
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
R.	R.	kA	R.
Garnettovu	Garnettův	k2eAgFnSc4d1	Garnettův
History	Histor	k1gInPc1	Histor
of	of	k?	of
Italian	Italian	k1gInSc1	Italian
Literature	Literatur	k1gMnSc5	Literatur
(	(	kIx(	(
<g/>
London	London	k1gMnSc1	London
<g/>
,	,	kIx,	,
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Www.StoriaDellaLetteratura.it	Www.StoriaDellaLetteratura.it	k1gInSc1	Www.StoriaDellaLetteratura.it
(	(	kIx(	(
<g/>
Storia	Storium	k1gNnSc2	Storium
della	dello	k1gNnSc2	dello
letteratura	letteratura	k1gFnSc1	letteratura
italiana	italiana	k1gFnSc1	italiana
<g/>
,	,	kIx,	,
di	di	k?	di
Antonio	Antonio	k1gMnSc5	Antonio
Piromalli	Piromall	k1gMnSc5	Piromall
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
AA	AA	kA	AA
<g/>
.	.	kIx.	.
<g/>
VV	VV	kA	VV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Antologia	Antologia	k1gFnSc1	Antologia
della	della	k1gFnSc1	della
Poesia	Poesia	k1gFnSc1	Poesia
italiana	italiana	k1gFnSc1	italiana
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
C.	C.	kA	C.
Segre	Segr	k1gInSc5	Segr
and	and	k?	and
C.	C.	kA	C.
Ossola	Ossola	k1gFnSc1	Ossola
<g/>
.	.	kIx.	.
</s>
<s>
Torino	Torino	k1gNnSc1	Torino
<g/>
,	,	kIx,	,
Einaudi	Einaud	k1gMnPc1	Einaud
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
</s>
</p>
<p>
<s>
Giudice	Giudice	k1gFnSc1	Giudice
<g/>
,	,	kIx,	,
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Bruni	Bruen	k2eAgMnPc1d1	Bruen
<g/>
,	,	kIx,	,
G.	G.	kA	G.
<g/>
,	,	kIx,	,
Problemi	Proble	k1gFnPc7	Proble
e	e	k0	e
scrittori	scrittor	k1gMnPc1	scrittor
della	della	k1gFnSc1	della
letteratura	letteratura	k1gFnSc1	letteratura
italiana	italiana	k1gFnSc1	italiana
<g/>
.	.	kIx.	.
</s>
<s>
Torino	Torino	k1gNnSc1	Torino
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
</s>
</p>
<p>
<s>
Bruni	Brun	k1gMnPc1	Brun
F.	F.	kA	F.
<g/>
,	,	kIx,	,
Testi	test	k1gMnPc1	test
e	e	k0	e
documenti	document	k1gMnPc1	document
<g/>
.	.	kIx.	.
</s>
<s>
Torino	Torina	k1gMnSc5	Torina
<g/>
,	,	kIx,	,
Utete	Utet	k1gMnSc5	Utet
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
</s>
</p>
<p>
<s>
Bruni	Bruen	k2eAgMnPc1d1	Bruen
<g/>
,	,	kIx,	,
F.	F.	kA	F.
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Italiano	Italiana	k1gFnSc5	Italiana
nelle	nelle	k1gNnSc2	nelle
regioni	regioň	k1gFnSc3	regioň
<g/>
.	.	kIx.	.
</s>
<s>
Torino	Torina	k1gMnSc5	Torina
<g/>
,	,	kIx,	,
Utete	Utet	k1gMnSc5	Utet
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
</s>
</p>
<p>
<s>
Křesálková	Křesálková	k1gFnSc1	Křesálková
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
,	,	kIx,	,
Italská	italský	k2eAgFnSc1d1	italská
literatura	literatura	k1gFnSc1	literatura
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
bibliografie	bibliografie	k1gFnSc1	bibliografie
italských	italský	k2eAgNnPc2d1	italské
literárních	literární	k2eAgNnPc2d1	literární
děl	dělo	k1gNnPc2	dělo
přeložených	přeložený	k2eAgNnPc2d1	přeložené
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
a	a	k8xC	a
slovenštiny	slovenština	k1gFnSc2	slovenština
<g/>
,	,	kIx,	,
vydaných	vydaný	k2eAgFnPc2d1	vydaná
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
knihtisku	knihtisk	k1gInSc2	knihtisk
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
přeložených	přeložený	k2eAgFnPc2d1	přeložená
netištěných	tištěný	k2eNgFnPc2d1	netištěná
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
a	a	k8xC	a
operních	operní	k2eAgNnPc2d1	operní
libret	libreto	k1gNnPc2	libreto
inscenovaných	inscenovaný	k2eAgNnPc2d1	inscenované
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
<g/>
,	,	kIx,	,
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-7308-720-3	[number]	k4	978-80-7308-720-3
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Sicilská	sicilský	k2eAgFnSc1d1	sicilská
škola	škola	k1gFnSc1	škola
</s>
</p>
<p>
<s>
Dolce	dolce	k6eAd1	dolce
stil	stinout	k5eAaPmAgMnS	stinout
novo	nova	k1gFnSc5	nova
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
italských	italský	k2eAgMnPc2d1	italský
spisovatelů	spisovatel	k1gMnPc2	spisovatel
</s>
</p>
<p>
<s>
Lidová	lidový	k2eAgFnSc1d1	lidová
latina	latina	k1gFnSc1	latina
</s>
</p>
<p>
<s>
==	==	k?	==
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
==	==	k?	==
</s>
</p>
<p>
<s>
1906	[number]	k4	1906
<g/>
-Giosuè	-Giosuè	k?	-Giosuè
Carducci	Carducce	k1gFnSc4	Carducce
</s>
</p>
<p>
<s>
1926	[number]	k4	1926
<g/>
-Grazia	-Grazia	k1gFnSc1	-Grazia
Deledda	Deledda	k1gFnSc1	Deledda
</s>
</p>
<p>
<s>
1934	[number]	k4	1934
<g/>
-Luigi	-Luigi	k1gNnSc1	-Luigi
Pirandello	Pirandella	k1gFnSc5	Pirandella
</s>
</p>
<p>
<s>
1959	[number]	k4	1959
<g/>
-Salvatore	-Salvator	k1gMnSc5	-Salvator
Quasimodo	Quasimodo	k1gMnSc1	Quasimodo
</s>
</p>
<p>
<s>
1975	[number]	k4	1975
<g/>
-Eugenio	-Eugenio	k1gNnSc1	-Eugenio
Montale	Montal	k1gMnSc5	Montal
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
<g/>
-Dario	-Dario	k1gMnSc1	-Dario
Fo	Fo	k1gMnSc1	Fo
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Italian	Italiana	k1gFnPc2	Italiana
literature	literatur	k1gMnSc5	literatur
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Talianska	Taliansko	k1gNnSc2	Taliansko
literatúra	literatúr	k1gInSc2	literatúr
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
