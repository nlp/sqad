<s>
Kilián	Kilián	k1gMnSc1	Kilián
Ignác	Ignác	k1gMnSc1	Ignác
Dientzenhofer	Dientzenhofer	k1gMnSc1	Dientzenhofer
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1689	[number]	k4	1689
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1751	[number]	k4	1751
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
architekt	architekt	k1gMnSc1	architekt
a	a	k8xC	a
stavitel	stavitel	k1gMnSc1	stavitel
německého	německý	k2eAgInSc2d1	německý
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
představitel	představitel	k1gMnSc1	představitel
vrcholného	vrcholný	k2eAgNnSc2d1	vrcholné
baroka	baroko	k1gNnSc2	baroko
<g/>
,	,	kIx,	,
pátý	pátý	k4xOgMnSc1	pátý
syn	syn	k1gMnSc1	syn
stavitele	stavitel	k1gMnSc2	stavitel
Kryštofa	Kryštof	k1gMnSc2	Kryštof
Dientzenhofera	Dientzenhofer	k1gMnSc2	Dientzenhofer
<g/>
.	.	kIx.	.
</s>
