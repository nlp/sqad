<p>
<s>
Kilián	Kilián	k1gMnSc1	Kilián
Ignác	Ignác	k1gMnSc1	Ignác
Dientzenhofer	Dientzenhofer	k1gMnSc1	Dientzenhofer
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1689	[number]	k4	1689
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1751	[number]	k4	1751
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
architekt	architekt	k1gMnSc1	architekt
a	a	k8xC	a
stavitel	stavitel	k1gMnSc1	stavitel
německého	německý	k2eAgInSc2d1	německý
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
představitel	představitel	k1gMnSc1	představitel
vrcholného	vrcholný	k2eAgNnSc2d1	vrcholné
baroka	baroko	k1gNnSc2	baroko
<g/>
,	,	kIx,	,
pátý	pátý	k4xOgMnSc1	pátý
syn	syn	k1gMnSc1	syn
stavitele	stavitel	k1gMnSc2	stavitel
Kryštofa	Kryštof	k1gMnSc2	Kryštof
Dientzenhofera	Dientzenhofer	k1gMnSc2	Dientzenhofer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
jezuitské	jezuitský	k2eAgNnSc4d1	jezuitské
gymnázium	gymnázium	k1gNnSc4	gymnázium
na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
ukončení	ukončení	k1gNnSc6	ukončení
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
univerzitě	univerzita	k1gFnSc6	univerzita
filozofii	filozofie	k1gFnSc4	filozofie
a	a	k8xC	a
matematiku	matematika	k1gFnSc4	matematika
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
se	se	k3xPyFc4	se
vyučil	vyučit	k5eAaPmAgMnS	vyučit
stavitelství	stavitelství	k1gNnSc4	stavitelství
a	a	k8xC	a
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1707	[number]	k4	1707
odešel	odejít	k5eAaPmAgMnS	odejít
na	na	k7c4	na
zkušenou	zkušená	k1gFnSc4	zkušená
do	do	k7c2	do
ciziny	cizina	k1gFnSc2	cizina
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1719	[number]	k4	1719
se	se	k3xPyFc4	se
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Jiljí	Jiljí	k1gMnSc2	Jiljí
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
městě	město	k1gNnSc6	město
oženil	oženit	k5eAaPmAgMnS	oženit
za	za	k7c4	za
Annu	Anna	k1gFnSc4	Anna
Cecilii	Cecilie	k1gFnSc4	Cecilie
Popelovou	Popelová	k1gFnSc4	Popelová
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
barokní	barokní	k2eAgFnSc7d1	barokní
tvorbou	tvorba	k1gFnSc7	tvorba
architekta	architekt	k1gMnSc2	architekt
Johanna	Johann	k1gMnSc2	Johann
Lucase	Lucasa	k1gFnSc6	Lucasa
von	von	k1gInSc4	von
Hildebrandta	Hildebrandto	k1gNnSc2	Hildebrandto
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
však	však	k9	však
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
také	také	k9	také
F.	F.	kA	F.
Borrominim	Borrominim	k1gInSc1	Borrominim
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
stavitelem	stavitel	k1gMnSc7	stavitel
J.	J.	kA	J.
B.	B.	kA	B.
Santinim-Aichlem	Santinim-Aichl	k1gInSc7	Santinim-Aichl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdní	pozdní	k2eAgFnSc6d1	pozdní
fázi	fáze	k1gFnSc6	fáze
své	svůj	k3xOyFgFnSc2	svůj
tvorby	tvorba	k1gFnSc2	tvorba
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
zetěm	zeť	k1gMnSc7	zeť
Anselmem	Anselm	k1gMnSc7	Anselm
Luragem	Lurag	k1gMnSc7	Lurag
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pracoval	pracovat	k5eAaImAgInS	pracovat
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
zakázkách	zakázka	k1gFnPc6	zakázka
šlechty	šlechta	k1gFnSc2	šlechta
a	a	k8xC	a
církevních	církevní	k2eAgFnPc2d1	církevní
institucí	instituce	k1gFnPc2	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
byly	být	k5eAaImAgInP	být
pojmenovány	pojmenovat	k5eAaPmNgInP	pojmenovat
např.	např.	kA	např.
Dienzenhoferovy	Dienzenhoferův	k2eAgFnPc4d1	Dienzenhoferova
sady	sada	k1gFnPc4	sada
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
===	===	k?	===
</s>
</p>
<p>
<s>
Břevnovský	břevnovský	k2eAgInSc1d1	břevnovský
klášter	klášter	k1gInSc1	klášter
<g/>
,	,	kIx,	,
konvent	konvent	k1gInSc1	konvent
s	s	k7c7	s
prelaturou	prelatura	k1gFnSc7	prelatura
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
1717	[number]	k4	1717
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
Karlov	Karlov	k1gInSc1	Karlov
<g/>
,	,	kIx,	,
Letohrádek	letohrádek	k1gInSc1	letohrádek
Michny	Michna	k1gMnSc2	Michna
z	z	k7c2	z
Vacínova	Vacínův	k2eAgNnSc2d1	Vacínův
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
zvaný	zvaný	k2eAgInSc1d1	zvaný
Vila	vila	k1gFnSc1	vila
Amerika	Amerika	k1gFnSc1	Amerika
(	(	kIx(	(
<g/>
1717	[number]	k4	1717
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
Vodičkova	Vodičkův	k2eAgFnSc1d1	Vodičkova
35	[number]	k4	35
–	–	k?	–
Měšťanský	měšťanský	k2eAgInSc4d1	měšťanský
dům	dům	k1gInSc4	dům
U	u	k7c2	u
červeného	červený	k2eAgNnSc2d1	červené
pole	pole	k1gNnSc2	pole
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
1720	[number]	k4	1720
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hradčany	Hradčany	k1gInPc1	Hradčany
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
a	a	k8xC	a
klášter	klášter	k1gInSc1	klášter
voršilek	voršilka	k1gFnPc2	voršilka
(	(	kIx(	(
<g/>
1720	[number]	k4	1720
<g/>
–	–	k?	–
<g/>
28	[number]	k4	28
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hradčany	Hradčany	k1gInPc1	Hradčany
<g/>
,	,	kIx,	,
Loreta	Loreto	k1gNnPc1	Loreto
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnPc1d1	hlavní
průčelí	průčelí	k1gNnPc1	průčelí
(	(	kIx(	(
<g/>
1723	[number]	k4	1723
<g/>
;	;	kIx,	;
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
autor	autor	k1gMnSc1	autor
plánů	plán	k1gInPc2	plán
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
Klementinum	Klementinum	k1gNnSc1	Klementinum
<g/>
,	,	kIx,	,
Zrcadlová	zrcadlový	k2eAgFnSc1d1	zrcadlová
kaple	kaple	k1gFnSc1	kaple
(	(	kIx(	(
<g/>
1724	[number]	k4	1724
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Břevnovský	břevnovský	k2eAgInSc1d1	břevnovský
klášter	klášter	k1gInSc1	klášter
<g/>
,	,	kIx,	,
rozšíření	rozšíření	k1gNnSc1	rozšíření
Vojtěšky	Vojtěška	k1gFnSc2	Vojtěška
(	(	kIx(	(
<g/>
1724	[number]	k4	1724
<g/>
–	–	k?	–
<g/>
26	[number]	k4	26
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Malá	malý	k2eAgFnSc1d1	malá
Strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
Tomášské	tomášský	k2eAgFnSc6d1	Tomášská
ulici	ulice	k1gFnSc6	ulice
čp.	čp.	k?	čp.
26	[number]	k4	26
<g/>
,	,	kIx,	,
dům	dům	k1gInSc1	dům
U	u	k7c2	u
Zlatého	zlatý	k2eAgMnSc2d1	zlatý
jelena	jelen	k1gMnSc2	jelen
(	(	kIx(	(
<g/>
1725	[number]	k4	1725
<g/>
–	–	k?	–
<g/>
26	[number]	k4	26
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Malá	malý	k2eAgFnSc1d1	malá
Strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Tomáše	Tomáš	k1gMnSc2	Tomáš
<g/>
,	,	kIx,	,
přestavba	přestavba	k1gFnSc1	přestavba
(	(	kIx(	(
<g/>
1725	[number]	k4	1725
<g/>
–	–	k?	–
<g/>
31	[number]	k4	31
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Kateřiny	Kateřina	k1gFnSc2	Kateřina
</s>
</p>
<p>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
Na	na	k7c6	na
Slupi	slup	k1gFnSc6	slup
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
u	u	k7c2	u
alžbětinek	alžbětinka	k1gFnPc2	alžbětinka
(	(	kIx(	(
<g/>
1725	[number]	k4	1725
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Smíchov	Smíchov	k1gInSc1	Smíchov
<g/>
,	,	kIx,	,
vila	vila	k1gFnSc1	vila
později	pozdě	k6eAd2	pozdě
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Portheimka	Portheimka	k1gFnSc1	Portheimka
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
rodinu	rodina	k1gFnSc4	rodina
(	(	kIx(	(
<g/>
1725	[number]	k4	1725
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Malá	malý	k2eAgFnSc1d1	malá
Strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
Dům	dům	k1gInSc1	dům
U	u	k7c2	u
Dvou	dva	k4xCgFnPc2	dva
hrdliček	hrdlička	k1gFnPc2	hrdlička
<g/>
,	,	kIx,	,
přestavba	přestavba	k1gFnSc1	přestavba
domu	dům	k1gInSc2	dům
pro	pro	k7c4	pro
královského	královský	k2eAgMnSc4d1	královský
tesařského	tesařský	k2eAgMnSc4d1	tesařský
mistra	mistr	k1gMnSc4	mistr
Josefa	Josef	k1gMnSc4	Josef
Lefflera	Leffler	k1gMnSc4	Leffler
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
1726	[number]	k4	1726
</s>
</p>
<p>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
na	na	k7c6	na
Skalce	skalka	k1gFnSc6	skalka
(	(	kIx(	(
<g/>
1730	[number]	k4	1730
<g/>
–	–	k?	–
<g/>
39	[number]	k4	39
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Karla	Karel	k1gMnSc4	Karel
Boromejského	Boromejský	k2eAgMnSc4d1	Boromejský
–	–	k?	–
dnes	dnes	k6eAd1	dnes
pravoslavný	pravoslavný	k2eAgInSc1d1	pravoslavný
chrám	chrám	k1gInSc1	chrám
sv.	sv.	kA	sv.
Cyrila	Cyril	k1gMnSc2	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc2	Metoděj
(	(	kIx(	(
<g/>
1730	[number]	k4	1730
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Bartoloměje	Bartoloměj	k1gMnSc2	Bartoloměj
(	(	kIx(	(
<g/>
do	do	k7c2	do
1731	[number]	k4	1731
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karlín	Karlín	k1gInSc1	Karlín
<g/>
,	,	kIx,	,
Invalidovna	invalidovna	k1gFnSc1	invalidovna
–	–	k?	–
jeho	jeho	k3xOp3gInSc4	jeho
nejrozsáhlejší	rozsáhlý	k2eAgFnSc1d3	nejrozsáhlejší
světská	světský	k2eAgFnSc1d1	světská
stavba	stavba	k1gFnSc1	stavba
(	(	kIx(	(
<g/>
1731	[number]	k4	1731
<g/>
–	–	k?	–
<g/>
37	[number]	k4	37
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
s	s	k7c7	s
benediktinským	benediktinský	k2eAgInSc7d1	benediktinský
klášterem	klášter	k1gInSc7	klášter
(	(	kIx(	(
<g/>
1732	[number]	k4	1732
<g/>
–	–	k?	–
<g/>
35	[number]	k4	35
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Malá	malý	k2eAgFnSc1d1	malá
Strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
<g/>
,	,	kIx,	,
dostavba	dostavba	k1gFnSc1	dostavba
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
kněžiště	kněžiště	k1gNnSc1	kněžiště
a	a	k8xC	a
velkolepá	velkolepý	k2eAgFnSc1d1	velkolepá
kupole	kupole	k1gFnSc1	kupole
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c6	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
jezuitských	jezuitský	k2eAgFnPc2d1	jezuitská
zakázek	zakázka	k1gFnPc2	zakázka
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
na	na	k7c6	na
kostele	kostel	k1gInSc6	kostel
pracoval	pracovat	k5eAaImAgMnS	pracovat
snad	snad	k9	snad
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Kryštof	Kryštof	k1gMnSc1	Kryštof
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1737	[number]	k4	1737
<g/>
–	–	k?	–
<g/>
51	[number]	k4	51
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
Příkopy	příkop	k1gInPc1	příkop
<g/>
,	,	kIx,	,
Piccolominiho	Piccolomini	k1gMnSc2	Piccolomini
palác	palác	k1gInSc4	palác
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
palác	palác	k1gInSc1	palác
Sylva-Taroucca	Sylva-Tarouccum	k1gNnSc2	Sylva-Tarouccum
byl	být	k5eAaImAgInS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
posledních	poslední	k2eAgFnPc2d1	poslední
jeho	jeho	k3xOp3gFnPc2	jeho
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
jediná	jediný	k2eAgFnSc1d1	jediná
palácová	palácový	k2eAgFnSc1d1	palácová
stavba	stavba	k1gFnSc1	stavba
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
(	(	kIx(	(
<g/>
1743	[number]	k4	1743
<g/>
–	–	k?	–
<g/>
51	[number]	k4	51
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Mimo	mimo	k7c4	mimo
Prahu	Praha	k1gFnSc4	Praha
===	===	k?	===
</s>
</p>
<p>
<s>
Plánice	plánice	k1gFnSc1	plánice
na	na	k7c6	na
Klatovsku	Klatovsko	k1gNnSc6	Klatovsko
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
Nicov	Nicov	k1gInSc1	Nicov
<g/>
,	,	kIx,	,
poutní	poutní	k2eAgInSc1d1	poutní
kostel	kostel	k1gInSc1	kostel
Narození	narození	k1gNnSc2	narození
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
(	(	kIx(	(
<g/>
1721	[number]	k4	1721
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
jezuitská	jezuitský	k2eAgFnSc1d1	jezuitská
kolej	kolej	k1gFnSc1	kolej
na	na	k7c6	na
Nám.	Nám.	k1gFnSc6	Nám.
Republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgInSc1d1	jižní
trakt	trakt	k1gInSc1	trakt
a	a	k8xC	a
fasáda	fasáda	k1gFnSc1	fasáda
(	(	kIx(	(
<g/>
1720	[number]	k4	1720
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Plasy	plasa	k1gFnPc4	plasa
<g/>
,	,	kIx,	,
cisterciácké	cisterciácký	k2eAgNnSc4d1	cisterciácké
opatství	opatství	k1gNnSc4	opatství
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
dokončení	dokončení	k1gNnSc4	dokončení
budovy	budova	k1gFnSc2	budova
konventu	konvent	k1gInSc2	konvent
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Jana	Jan	k1gMnSc2	Jan
Blažeje	Blažej	k1gMnSc2	Blažej
Santiniho	Santini	k1gMnSc2	Santini
(	(	kIx(	(
<g/>
po	po	k7c6	po
1723	[number]	k4	1723
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Heřmánkovice	Heřmánkovice	k1gFnSc1	Heřmánkovice
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
Všech	všecek	k3xTgFnPc2	všecek
svatých	svatá	k1gFnPc2	svatá
na	na	k7c6	na
půdorysu	půdorys	k1gInSc6	půdorys
protáhlého	protáhlý	k2eAgInSc2d1	protáhlý
osmiúhelníku	osmiúhelník	k1gInSc2	osmiúhelník
(	(	kIx(	(
<g/>
1722	[number]	k4	1722
<g/>
–	–	k?	–
<g/>
26	[number]	k4	26
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Počaply	Počapnout	k5eAaPmAgInP	Počapnout
u	u	k7c2	u
Terezína	Terezín	k1gInSc2	Terezín
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
(	(	kIx(	(
<g/>
do	do	k7c2	do
1726	[number]	k4	1726
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Otovice	Otovice	k1gFnSc1	Otovice
na	na	k7c4	na
Náchodsku	Náchodska	k1gFnSc4	Náchodska
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Barbory	Barbora	k1gFnSc2	Barbora
(	(	kIx(	(
<g/>
1725	[number]	k4	1725
<g/>
–	–	k?	–
<g/>
26	[number]	k4	26
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bezděkov	Bezděkov	k1gInSc1	Bezděkov
nad	nad	k7c7	nad
Metují	Metuje	k1gFnSc7	Metuje
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Prokopa	Prokop	k1gMnSc2	Prokop
(	(	kIx(	(
<g/>
1724	[number]	k4	1724
<g/>
–	–	k?	–
<g/>
27	[number]	k4	27
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vižňov	Vižňov	k1gInSc1	Vižňov
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Anny	Anna	k1gFnSc2	Anna
(	(	kIx(	(
<g/>
1725	[number]	k4	1725
<g/>
–	–	k?	–
<g/>
27	[number]	k4	27
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Šonov	Šonov	k1gInSc1	Šonov
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Markéty	Markéta	k1gFnSc2	Markéta
(	(	kIx(	(
<g/>
1726	[number]	k4	1726
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Máří	Máří	k?	Máří
Magdaleny	Magdalena	k1gFnSc2	Magdalena
(	(	kIx(	(
<g/>
1729	[number]	k4	1729
<g/>
–	–	k?	–
<g/>
1730	[number]	k4	1730
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Okres	okres	k1gInSc1	okres
Legnica	Legnica	k1gFnSc1	Legnica
<g/>
,	,	kIx,	,
Legnickie	Legnickie	k1gFnSc1	Legnickie
Pole	pole	k1gFnSc1	pole
(	(	kIx(	(
<g/>
pruský	pruský	k2eAgInSc1d1	pruský
Wahlstatt	Wahlstatt	k1gInSc1	Wahlstatt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Hedviky	Hedvika	k1gFnSc2	Hedvika
a	a	k8xC	a
klášter	klášter	k1gInSc1	klášter
pro	pro	k7c4	pro
benediktinské	benediktinský	k2eAgNnSc4d1	benediktinské
opatství	opatství	k1gNnSc4	opatství
(	(	kIx(	(
<g/>
1727	[number]	k4	1727
<g/>
–	–	k?	–
<g/>
31	[number]	k4	31
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kladruby	Kladruby	k1gInPc4	Kladruby
u	u	k7c2	u
Stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
kladrubský	kladrubský	k2eAgInSc1d1	kladrubský
klášter	klášter	k1gInSc1	klášter
<g/>
,	,	kIx,	,
dokončení	dokončení	k1gNnSc1	dokončení
budov	budova	k1gFnPc2	budova
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Jana	Jan	k1gMnSc2	Jan
Santiniho-Aichela	Santiniho-Aichel	k1gMnSc2	Santiniho-Aichel
(	(	kIx(	(
<g/>
do	do	k7c2	do
1732	[number]	k4	1732
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Broumov	Broumov	k1gInSc1	Broumov
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
-	-	kIx~	-
předsíň	předsíň	k1gFnSc1	předsíň
a	a	k8xC	a
dlažba	dlažba	k1gFnSc1	dlažba
<g/>
,	,	kIx,	,
celý	celý	k2eAgInSc1d1	celý
konvent	konvent	k1gInSc1	konvent
benediktinského	benediktinský	k2eAgInSc2d1	benediktinský
kláštera	klášter	k1gInSc2	klášter
(	(	kIx(	(
<g/>
1726	[number]	k4	1726
<g/>
–	–	k?	–
<g/>
33	[number]	k4	33
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Křinice	Křinice	k1gFnSc1	Křinice
u	u	k7c2	u
Náchoda	Náchod	k1gInSc2	Náchod
<g/>
,	,	kIx,	,
nedaleká	daleký	k2eNgFnSc1d1	nedaleká
kaple	kaple	k1gFnSc1	kaple
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
na	na	k7c6	na
Hvězdě	hvězda	k1gFnSc6	hvězda
(	(	kIx(	(
<g/>
1732	[number]	k4	1732
<g/>
–	–	k?	–
<g/>
33	[number]	k4	33
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Opařany	Opařan	k1gMnPc4	Opařan
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Františka	František	k1gMnSc2	František
Xaverského	xaverský	k2eAgMnSc2d1	xaverský
(	(	kIx(	(
<g/>
1732	[number]	k4	1732
<g/>
–	–	k?	–
<g/>
35	[number]	k4	35
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Odolena	Odolen	k2eAgFnSc1d1	Odolena
Voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Klimenta	Kliment	k1gMnSc2	Kliment
(	(	kIx(	(
<g/>
1733	[number]	k4	1733
<g/>
–	–	k?	–
<g/>
35	[number]	k4	35
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dobrá	dobrý	k2eAgFnSc1d1	dobrá
Voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Bolestné	bolestný	k2eAgFnSc2d1	bolestná
(	(	kIx(	(
<g/>
1733	[number]	k4	1733
<g/>
–	–	k?	–
<g/>
1735	[number]	k4	1735
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
klášter	klášter	k1gInSc1	klášter
voršilek	voršilka	k1gFnPc2	voršilka
(	(	kIx(	(
<g/>
1735	[number]	k4	1735
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Březno	Březno	k1gNnSc1	Březno
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc4	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc4	Pavel
(	(	kIx(	(
<g/>
po	po	k7c6	po
1737	[number]	k4	1737
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Božanov	Božanov	k1gInSc1	Božanov
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Máří	Máří	k?	Máří
Magdaleny	Magdalena	k1gFnSc2	Magdalena
(	(	kIx(	(
<g/>
1733	[number]	k4	1733
<g/>
–	–	k?	–
<g/>
38	[number]	k4	38
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nepomuk	Nepomuk	k1gInSc1	Nepomuk
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
(	(	kIx(	(
<g/>
1734	[number]	k4	1734
<g/>
–	–	k?	–
<g/>
38	[number]	k4	38
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Stará	starat	k5eAaImIp3nS	starat
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
,	,	kIx,	,
kaple	kaple	k1gFnSc1	kaple
blahoslaveného	blahoslavený	k2eAgInSc2d1	blahoslavený
Podivena	podiven	k2eAgFnSc1d1	Podivena
(	(	kIx(	(
<g/>
1738	[number]	k4	1738
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kladenský	kladenský	k2eAgInSc1d1	kladenský
zámek	zámek	k1gInSc1	zámek
<g/>
,	,	kIx,	,
návrh	návrh	k1gInSc1	návrh
přestavby	přestavba	k1gFnSc2	přestavba
na	na	k7c6	na
rezidenci	rezidence	k1gFnSc6	rezidence
břevnovských	břevnovský	k2eAgMnPc2d1	břevnovský
a	a	k8xC	a
broumovských	broumovský	k2eAgMnPc2d1	broumovský
benediktinů	benediktin	k1gMnPc2	benediktin
(	(	kIx(	(
<g/>
1739	[number]	k4	1739
<g/>
–	–	k?	–
<g/>
1740	[number]	k4	1740
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kladno	Kladno	k1gNnSc1	Kladno
<g/>
,	,	kIx,	,
Mariánské	mariánský	k2eAgNnSc1d1	Mariánské
sousoší	sousoší	k1gNnSc1	sousoší
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
se	s	k7c7	s
sochařem	sochař	k1gMnSc7	sochař
K.	K.	kA	K.
J.	J.	kA	J.
Hiernlem	Hiernl	k1gInSc7	Hiernl
(	(	kIx(	(
<g/>
realizace	realizace	k1gFnSc1	realizace
1739	[number]	k4	1739
<g/>
–	–	k?	–
<g/>
1741	[number]	k4	1741
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Přeštice	Přeštice	k1gFnPc1	Přeštice
<g/>
,	,	kIx,	,
stavba	stavba	k1gFnSc1	stavba
děkanství	děkanství	k1gNnSc1	děkanství
a	a	k8xC	a
návrh	návrh	k1gInSc1	návrh
kostela	kostel	k1gInSc2	kostel
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
(	(	kIx(	(
<g/>
po	po	k7c6	po
1745	[number]	k4	1745
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hrdly	hrdla	k1gFnPc1	hrdla
<g/>
,	,	kIx,	,
zámek	zámek	k1gInSc1	zámek
(	(	kIx(	(
<g/>
1746	[number]	k4	1746
<g/>
–	–	k?	–
<g/>
1747	[number]	k4	1747
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sázavský	sázavský	k2eAgInSc1d1	sázavský
klášter	klášter	k1gInSc1	klášter
<g/>
,	,	kIx,	,
práce	práce	k1gFnSc1	práce
pro	pro	k7c4	pro
benediktiny	benediktin	k1gMnPc4	benediktin
(	(	kIx(	(
<g/>
po	po	k7c6	po
1747	[number]	k4	1747
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hořice	Hořice	k1gFnPc1	Hořice
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
Narození	narození	k1gNnSc2	narození
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
(	(	kIx(	(
<g/>
1741	[number]	k4	1741
<g/>
–	–	k?	–
<g/>
48	[number]	k4	48
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Stará	starat	k5eAaImIp3nS	starat
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
(	(	kIx(	(
<g/>
Stará	starat	k5eAaImIp3nS	starat
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
západní	západní	k2eAgNnPc4d1	západní
průčelí	průčelí	k1gNnPc4	průčelí
(	(	kIx(	(
<g/>
1736	[number]	k4	1736
<g/>
)	)	kIx)	)
a	a	k8xC	a
jižní	jižní	k2eAgFnSc1d1	jižní
věž	věž	k1gFnSc1	věž
(	(	kIx(	(
<g/>
1748	[number]	k4	1748
<g/>
-	-	kIx~	-
<g/>
1749	[number]	k4	1749
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Paštiky	paštika	k1gFnPc1	paštika
u	u	k7c2	u
Blatné	blatný	k2eAgFnSc2d1	Blatná
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
(	(	kIx(	(
<g/>
1748	[number]	k4	1748
<g/>
–	–	k?	–
<g/>
49	[number]	k4	49
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dolní	dolní	k2eAgInSc1d1	dolní
Ročov	Ročov	k1gInSc1	Ročov
<g/>
,	,	kIx,	,
klášterní	klášterní	k2eAgInSc1d1	klášterní
kostel	kostel	k1gInSc1	kostel
a	a	k8xC	a
konvent	konvent	k1gInSc1	konvent
(	(	kIx(	(
<g/>
1746	[number]	k4	1746
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zápy	Zápa	k1gFnPc1	Zápa
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jakuba	Jakub	k1gMnSc2	Jakub
Staršího	starší	k1gMnSc2	starší
(	(	kIx(	(
<g/>
Zápy	Zápa	k1gMnSc2	Zápa
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1750	[number]	k4	1750
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kladno	Kladno	k1gNnSc1	Kladno
<g/>
,	,	kIx,	,
Kaple	kaple	k1gFnSc1	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Floriána	Florián	k1gMnSc2	Florián
<g/>
,	,	kIx,	,
s	s	k7c7	s
opatem	opat	k1gMnSc7	opat
Benno	Benno	k6eAd1	Benno
Löblem	Löbl	k1gMnSc7	Löbl
položil	položit	k5eAaPmAgInS	položit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1751	[number]	k4	1751
základní	základní	k2eAgInSc4d1	základní
kámen	kámen	k1gInSc4	kámen
centrální	centrální	k2eAgFnSc2d1	centrální
kaple	kaple	k1gFnSc2	kaple
<g/>
.	.	kIx.	.
</ref>
<g/>
Mestokladno	Mestokladno	k1gNnSc1	Mestokladno
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
:	:	kIx,	:
osobnosti	osobnost	k1gFnSc3	osobnost
kladna	kladno	k1gNnSc2	kladno
</ref>
</s>
</p>
<p>
<s>
Chválenice	Chválenice	k1gFnSc1	Chválenice
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Martina	Martin	k1gMnSc2	Martin
(	(	kIx(	(
<g/>
1747	[number]	k4	1747
<g/>
–	–	k?	–
<g/>
1752	[number]	k4	1752
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zlonice	Zlonice	k1gFnSc1	Zlonice
<g/>
,	,	kIx,	,
fara	fara	k1gFnSc1	fara
(	(	kIx(	(
<g/>
plány	plán	k1gInPc1	plán
1747	[number]	k4	1747
<g/>
,	,	kIx,	,
realizace	realizace	k1gFnSc1	realizace
1750	[number]	k4	1750
<g/>
–	–	k?	–
<g/>
1753	[number]	k4	1753
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Červený	červený	k2eAgInSc1d1	červený
Kostelec	Kostelec	k1gInSc1	Kostelec
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Jakuba	Jakub	k1gMnSc2	Jakub
Většího	veliký	k2eAgMnSc2d2	veliký
–	–	k?	–
redukovaný	redukovaný	k2eAgInSc1d1	redukovaný
oproti	oproti	k7c3	oproti
plánům	plán	k1gInPc3	plán
(	(	kIx(	(
<g/>
1744	[number]	k4	1744
<g/>
-	-	kIx~	-
<g/>
54	[number]	k4	54
<g/>
)	)	kIx)	)
<g/>
Stavby	stavba	k1gFnSc2	stavba
K.	K.	kA	K.
I.	I.	kA	I.
Dientzenhofera	Dientzenhofer	k1gMnSc2	Dientzenhofer
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
kolektiv	kolektiv	k1gInSc1	kolektiv
autorů	autor	k1gMnPc2	autor
<g/>
:	:	kIx,	:
Kilián	Kilián	k1gMnSc1	Kilián
Ignác	Ignác	k1gMnSc1	Ignác
Dientzenhofer	Dientzenhofer	k1gMnSc1	Dientzenhofer
a	a	k8xC	a
umělci	umělec	k1gMnPc1	umělec
jeho	jeho	k3xOp3gInSc3	jeho
okruhu	okruh	k1gInSc3	okruh
<g/>
,	,	kIx,	,
k	k	k7c3	k
300	[number]	k4	300
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
narození	narození	k1gNnSc2	narození
K.	K.	kA	K.
I.	I.	kA	I.
Dientzenhofera	Dientzenhofero	k1gNnPc4	Dientzenhofero
<g/>
,	,	kIx,	,
katalog	katalog	k1gInSc4	katalog
výstavy	výstava	k1gFnSc2	výstava
ve	v	k7c6	v
Valdštejnské	valdštejnský	k2eAgFnSc6d1	Valdštejnská
jízdárně	jízdárna	k1gFnSc6	jízdárna
<g/>
,	,	kIx,	,
Národní	národní	k2eAgFnSc1d1	národní
galerie	galerie	k1gFnSc1	galerie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
edice	edice	k1gFnSc1	edice
katalogy	katalog	k1gInPc4	katalog
38	[number]	k4	38
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Broumovská	broumovský	k2eAgFnSc1d1	Broumovská
skupina	skupina	k1gFnSc1	skupina
kostelů	kostel	k1gInPc2	kostel
Kryštofa	Kryštof	k1gMnSc2	Kryštof
a	a	k8xC	a
Kiliána	Kilián	k1gMnSc2	Kilián
Ignáce	Ignác	k1gMnSc2	Ignác
Dientzenhofera	Dientzenhofer	k1gMnSc2	Dientzenhofer
<g/>
,	,	kIx,	,
stručný	stručný	k2eAgMnSc1d1	stručný
průvodce	průvodce	k1gMnSc1	průvodce
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Collegium	Collegium	k1gNnSc1	Collegium
pro	pro	k7c4	pro
arte	arte	k1gFnSc4	arte
antiqua	antiqu	k1gInSc2	antiqu
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
ISBN	ISBN	kA	ISBN
80-239-4365-0	[number]	k4	80-239-4365-0
(	(	kIx(	(
<g/>
v	v	k7c6	v
brožuře	brožura	k1gFnSc6	brožura
neuvedeno	uveden	k2eNgNnSc1d1	neuvedeno
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mojmír	Mojmír	k1gMnSc1	Mojmír
Horyna	Horyna	k1gMnSc1	Horyna
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Kateřiny	Kateřina	k1gFnSc2	Kateřina
na	na	k7c6	na
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
Pražském	pražský	k2eAgNnSc6d1	Pražské
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-246-1487-8	[number]	k4	978-80-246-1487-8
</s>
</p>
<p>
<s>
KUBÍN	Kubín	k1gMnSc1	Kubín
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Dinzenhoferové	Dinzenhoferová	k1gFnPc4	Dinzenhoferová
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc4	jejich
slavná	slavný	k2eAgNnPc4d1	slavné
díla	dílo	k1gNnPc4	dílo
v	v	k7c6	v
království	království	k1gNnSc6	království
Českém	český	k2eAgNnSc6d1	české
:	:	kIx,	:
s	s	k7c7	s
26	[number]	k4	26
obrazovými	obrazový	k2eAgFnPc7d1	obrazová
přílohami	příloha	k1gFnPc7	příloha
a	a	k8xC	a
současnou	současný	k2eAgFnSc7d1	současná
podobiznou	podobizna	k1gFnSc7	podobizna
Kiliána	Kilián	k1gMnSc2	Kilián
Dinzenhofera	Dinzenhofer	k1gMnSc2	Dinzenhofer
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Stivín	Stivín	k1gMnSc1	Stivín
<g/>
,	,	kIx,	,
1908	[number]	k4	1908
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MÁDL	MÁDL	kA	MÁDL
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Boromejský	Boromejský	k2eAgMnSc1d1	Boromejský
<g/>
.	.	kIx.	.
</s>
<s>
K.	K.	kA	K.
I.	I.	kA	I.
Dienzenhofera	Dienzenhofera	k1gFnSc1	Dienzenhofera
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Topič	topič	k1gInSc1	topič
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
1897	[number]	k4	1897
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KŘÍŽENECKÝ	kříženecký	k2eAgMnSc1d1	kříženecký
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
.	.	kIx.	.
</s>
<s>
Kilian	Kilian	k1gMnSc1	Kilian
Ignác	Ignác	k1gMnSc1	Ignác
Dientzenhofer	Dientzenhofer	k1gMnSc1	Dientzenhofer
a	a	k8xC	a
článkování	článkování	k1gNnSc1	článkování
architektonické	architektonický	k2eAgFnSc2d1	architektonická
letohrádku	letohrádek	k1gInSc6	letohrádek
hr	hr	k2eAgInPc1d1	hr
<g/>
.	.	kIx.	.
</s>
<s>
Michny	Michna	k1gMnSc2	Michna
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
villy	villa	k1gFnPc1	villa
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
a	a	k8xC	a
praelatury	praelatura	k1gFnPc1	praelatura
u	u	k7c2	u
sv.	sv.	kA	sv.
Mikuláše	mikuláš	k1gInSc2	mikuláš
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
městě	město	k1gNnSc6	město
Pražském	pražský	k2eAgNnSc6d1	Pražské
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
vl	vl	k?	vl
<g/>
.	.	kIx.	.
<g/>
n.	n.	k?	n.
<g/>
,	,	kIx,	,
1899	[number]	k4	1899
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Pojednání	pojednání	k1gNnSc1	pojednání
o	o	k7c6	o
architektonických	architektonický	k2eAgInPc6d1	architektonický
prvcích	prvek	k1gInPc6	prvek
dvou	dva	k4xCgFnPc6	dva
pražských	pražský	k2eAgFnPc2d1	Pražská
barokních	barokní	k2eAgFnPc2d1	barokní
staveb	stavba	k1gFnPc2	stavba
dokumentované	dokumentovaný	k2eAgInPc4d1	dokumentovaný
15	[number]	k4	15
snímky	snímek	k1gInPc4	snímek
a	a	k8xC	a
řezy	řez	k1gInPc4	řez
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc4	vznik
a	a	k8xC	a
osudy	osud	k1gInPc4	osud
letohrádku	letohrádek	k1gInSc2	letohrádek
do	do	k7c2	do
r.	r.	kA	r.
1793	[number]	k4	1793
doložené	doložená	k1gFnSc3	doložená
citacemi	citace	k1gFnPc7	citace
z	z	k7c2	z
archivních	archivní	k2eAgInPc2d1	archivní
dokumentů	dokument	k1gInPc2	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Srovnání	srovnání	k1gNnSc1	srovnání
architektonických	architektonický	k2eAgInPc2d1	architektonický
detailů	detail	k1gInPc2	detail
exteriéru	exteriér	k1gInSc2	exteriér
(	(	kIx(	(
<g/>
portály	portál	k1gInPc1	portál
a	a	k8xC	a
okenní	okenní	k2eAgInPc1d1	okenní
římsy	říms	k1gInPc1	říms
<g/>
)	)	kIx)	)
letohrádku	letohrádek	k1gInSc2	letohrádek
a	a	k8xC	a
během	během	k7c2	během
asanace	asanace	k1gFnSc2	asanace
zbořené	zbořený	k2eAgFnSc2d1	zbořená
budovy	budova	k1gFnSc2	budova
prelatury	prelatura	k1gFnSc2	prelatura
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
Městě	město	k1gNnSc6	město
<g/>
..	..	k?	..
</s>
</p>
<p>
<s>
Osobnosti	osobnost	k1gFnPc1	osobnost
-	-	kIx~	-
Česko	Česko	k1gNnSc1	Česko
:	:	kIx,	:
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
823	[number]	k4	823
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7360	[number]	k4	7360
<g/>
-	-	kIx~	-
<g/>
796	[number]	k4	796
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
120	[number]	k4	120
<g/>
–	–	k?	–
<g/>
121	[number]	k4	121
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VOŠAHLÍKOVÁ	Vošahlíková	k1gFnSc1	Vošahlíková
<g/>
,	,	kIx,	,
Pavla	Pavla	k1gFnSc1	Pavla
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
:	:	kIx,	:
12	[number]	k4	12
<g/>
.	.	kIx.	.
sešit	sešit	k1gInSc1	sešit
:	:	kIx,	:
D	D	kA	D
<g/>
–	–	k?	–
<g/>
Die	Die	k1gMnSc1	Die
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
105	[number]	k4	105
<g/>
–	–	k?	–
<g/>
215	[number]	k4	215
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
415	[number]	k4	415
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
202	[number]	k4	202
<g/>
–	–	k?	–
<g/>
204	[number]	k4	204
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kilián	Kilián	k1gMnSc1	Kilián
Ignác	Ignác	k1gMnSc1	Ignác
Dientzenhofer	Dientzenhofra	k1gFnPc2	Dientzenhofra
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Kilián	Kilián	k1gMnSc1	Kilián
Ignác	Ignác	k1gMnSc1	Ignác
Dientzenhofer	Dientzenhofer	k1gMnSc1	Dientzenhofer
</s>
</p>
<p>
<s>
Kilián	Kilián	k1gMnSc1	Kilián
Ignác	Ignác	k1gMnSc1	Ignác
Dientzenhofer	Dientzenhofer	k1gMnSc1	Dientzenhofer
v	v	k7c6	v
Kdo	kdo	k3yInSc1	kdo
byl	být	k5eAaImAgMnS	být
kdo	kdo	k3yInSc1	kdo
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
dějinách	dějiny	k1gFnPc6	dějiny
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
</s>
</p>
<p>
<s>
Kilián	Kilián	k1gMnSc1	Kilián
Ignác	Ignác	k1gMnSc1	Ignác
Dientzenhofer	Dientzenhofer	k1gMnSc1	Dientzenhofer
–	–	k?	–
architektonický	architektonický	k2eAgMnSc1d1	architektonický
virtuos	virtuos	k1gMnSc1	virtuos
pozdního	pozdní	k2eAgNnSc2d1	pozdní
baroka	baroko	k1gNnSc2	baroko
<g/>
,	,	kIx,	,
ČT	ČT	kA	ČT
<g/>
24	[number]	k4	24
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
</s>
</p>
