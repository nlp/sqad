<s>
Křížové	Křížové	k2eAgFnPc1d1
výpravy	výprava	k1gFnPc1
</s>
<s>
Na	na	k7c4
tento	tento	k3xDgInSc4
článek	článek	k1gInSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc4d1
heslo	heslo	k1gNnSc4
Křižáci	křižák	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
výpravách	výprava	k1gFnPc6
obecně	obecně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránkách	stránka	k1gFnPc6
Křížové	Křížové	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
Křižáci	křižák	k1gMnPc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Modlící	modlící	k2eAgMnSc1d1
se	se	k3xPyFc4
křižácký	křižácký	k2eAgMnSc1d1
rytíř	rytíř	k1gMnSc1
na	na	k7c6
dobové	dobový	k2eAgFnSc6d1
miniatuře	miniatura	k1gFnSc6
(	(	kIx(
<g/>
BL	BL	kA
MS	MS	kA
Royal	Royal	k1gInSc4
2A	2A	k4
XXII	XXII	kA
f.	f.	k?
220	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Křížové	Kříž	k1gMnPc1
výpravy	výprava	k1gFnSc2
<g/>
,	,	kIx,
starším	starý	k2eAgInSc7d2
výrazem	výraz	k1gInSc7
kruciáty	kruciáta	k1gFnSc2
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
vojenské	vojenský	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
z	z	k7c2
dob	doba	k1gFnPc2
středověku	středověk	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yQgInPc4,k3yRgInPc4
vyhlašoval	vyhlašovat	k5eAaImAgMnS
papež	papež	k1gMnSc1
proti	proti	k7c3
muslimům	muslim	k1gMnPc3
<g/>
,	,	kIx,
pohanům	pohan	k1gMnPc3
a	a	k8xC
kacířům	kacíř	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dřívější	dřívější	k2eAgFnSc1d1
definice	definice	k1gFnSc1
historiků	historik	k1gMnPc2
tyto	tento	k3xDgFnPc4
výpravy	výprava	k1gFnPc4
omezovala	omezovat	k5eAaImAgFnS
pouze	pouze	k6eAd1
na	na	k7c4
výpravy	výprava	k1gFnPc4
k	k	k7c3
Jeruzalému	Jeruzalém	k1gInSc3
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
se	se	k3xPyFc4
však	však	k9
význam	význam	k1gInSc1
kruciát	kruciáta	k1gFnPc2
přenesl	přenést	k5eAaPmAgInS
na	na	k7c4
všechny	všechen	k3xTgMnPc4
papežem	papež	k1gMnSc7
vyhlášené	vyhlášený	k2eAgFnSc2d1
války	válka	k1gFnSc2
proti	proti	k7c3
nepřátelům	nepřítel	k1gMnPc3
křesťanstva	křesťanstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účastníci	účastník	k1gMnPc1
před	před	k7c7
zahájením	zahájení	k1gNnSc7
výpravy	výprava	k1gFnSc2
skládali	skládat	k5eAaImAgMnP
slib	slib	k1gInSc4
a	a	k8xC
byli	být	k5eAaImAgMnP
označeni	označit	k5eAaPmNgMnP
znamením	znamení	k1gNnSc7
kříže	kříž	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
si	se	k3xPyFc3
našívali	našívat	k5eAaImAgMnP
na	na	k7c4
šaty	šat	k1gInPc4
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
se	se	k3xPyFc4
nazývali	nazývat	k5eAaImAgMnP
křižáci	křižák	k1gMnPc1
(	(	kIx(
<g/>
latinsky	latinsky	k6eAd1
crucesignati	crucesignat	k5eAaPmF,k5eAaImF
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byli	být	k5eAaImAgMnP
dobrovolníky	dobrovolník	k1gMnPc7
<g/>
,	,	kIx,
měli	mít	k5eAaImAgMnP
za	za	k7c4
účast	účast	k1gFnSc4
na	na	k7c6
výpravě	výprava	k1gFnSc6
přislíbeno	přislíben	k2eAgNnSc4d1
odpuštění	odpuštění	k1gNnSc4
hříchů	hřích	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc4
křížovou	křížový	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
vyhlásil	vyhlásit	k5eAaPmAgMnS
papež	papež	k1gMnSc1
Urban	Urban	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
roku	rok	k1gInSc2
1095	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gInSc7
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
dobýt	dobýt	k5eAaPmF
Svatou	svatý	k2eAgFnSc4d1
zemi	zem	k1gFnSc4
a	a	k8xC
osvobodit	osvobodit	k5eAaPmF
místa	místo	k1gNnPc4
posvátná	posvátný	k2eAgNnPc4d1
pro	pro	k7c4
křesťany	křesťan	k1gMnPc4
z	z	k7c2
rukou	ruka	k1gFnPc2
muslimů	muslim	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
výpravě	výprava	k1gFnSc3
<g/>
,	,	kIx,
složené	složený	k2eAgInPc1d1
především	především	k9
z	z	k7c2
francouzských	francouzský	k2eAgMnPc2d1
a	a	k8xC
normanských	normanský	k2eAgMnPc2d1
rytířů	rytíř	k1gMnPc2
<g/>
,	,	kIx,
nakonec	nakonec	k6eAd1
podařilo	podařit	k5eAaPmAgNnS
<g/>
,	,	kIx,
a	a	k8xC
ve	v	k7c6
Svaté	svatý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
tak	tak	k9
Evropané	Evropan	k1gMnPc1
založili	založit	k5eAaPmAgMnP
křižácké	křižácký	k2eAgInPc4d1
státy	stát	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
se	se	k3xPyFc4
zde	zde	k6eAd1
udržely	udržet	k5eAaPmAgInP
téměř	téměř	k6eAd1
dvě	dva	k4xCgNnPc4
stě	sto	k4xCgFnPc1
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původním	původní	k2eAgInSc7d1
cílem	cíl	k1gInSc7
výprav	výprava	k1gFnPc2
proti	proti	k7c3
muslimům	muslim	k1gMnPc3
bylo	být	k5eAaImAgNnS
osvobození	osvobození	k1gNnSc4
Božího	boží	k2eAgInSc2d1
hrobu	hrob	k1gInSc2
v	v	k7c6
Jeruzalémě	Jeruzalém	k1gInSc6
<g/>
,	,	kIx,
pozdější	pozdní	k2eAgNnSc4d2
tažení	tažení	k1gNnSc4
však	však	k8xC
mířila	mířit	k5eAaImAgFnS
i	i	k9
jinam	jinam	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlo	jít	k5eAaImAgNnS
především	především	k6eAd1
o	o	k7c4
Egypt	Egypt	k1gInSc4
a	a	k8xC
Řecko	Řecko	k1gNnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
účastníci	účastník	k1gMnPc1
čtvrté	čtvrtá	k1gFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
dobyli	dobýt	k5eAaPmAgMnP
značnou	značný	k2eAgFnSc4d1
část	část	k1gFnSc4
Byzantské	byzantský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
a	a	k8xC
založili	založit	k5eAaPmAgMnP
křižácké	křižácký	k2eAgInPc4d1
státy	stát	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnPc4d1
křížová	křížový	k2eAgNnPc4d1
tažení	tažení	k1gNnPc4
mířila	mířit	k5eAaImAgFnS
do	do	k7c2
Pobaltí	Pobaltí	k1gNnSc2
a	a	k8xC
Skandinávie	Skandinávie	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
zvláště	zvláště	k6eAd1
němečtí	německý	k2eAgMnPc1d1
a	a	k8xC
švédští	švédský	k2eAgMnPc1d1
křižáci	křižák	k1gMnPc1
bojovali	bojovat	k5eAaImAgMnP
proti	proti	k7c3
pohanským	pohanský	k2eAgMnPc3d1
Prusům	Prus	k1gMnPc3
<g/>
,	,	kIx,
Slovanům	Slovan	k1gMnPc3
a	a	k8xC
Finům	Fin	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řád	řád	k1gInSc4
německých	německý	k2eAgMnPc2d1
rytířů	rytíř	k1gMnPc2
na	na	k7c6
břehu	břeh	k1gInSc6
Baltského	baltský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
založil	založit	k5eAaPmAgMnS
svůj	svůj	k3xOyFgMnSc1
stát	stát	k5eAaImF,k5eAaPmF
Prusy	Prus	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
pádu	pád	k1gInSc6
posledního	poslední	k2eAgNnSc2d1
křižáckého	křižácký	k2eAgNnSc2d1
města	město	k1gNnSc2
v	v	k7c6
Palestině	Palestina	k1gFnSc6
již	již	k6eAd1
žádný	žádný	k3yNgMnSc1
papež	papež	k1gMnSc1
vojenskou	vojenský	k2eAgFnSc4d1
akci	akce	k1gFnSc4
k	k	k7c3
osvobození	osvobození	k1gNnSc3
Jeruzaléma	Jeruzalém	k1gInSc2
nevyhlásil	vyhlásit	k5eNaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křížové	Křížové	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
na	na	k7c6
evropském	evropský	k2eAgInSc6d1
kontinentu	kontinent	k1gInSc6
však	však	k9
pokračovaly	pokračovat	k5eAaImAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
tažení	tažení	k1gNnSc2
proti	proti	k7c3
pohanům	pohan	k1gMnPc3
to	ten	k3xDgNnSc1
byly	být	k5eAaImAgInP
války	válek	k1gInPc1
proti	proti	k7c3
křesťanským	křesťanský	k2eAgMnPc3d1
kacířům	kacíř	k1gMnPc3
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
největší	veliký	k2eAgFnSc7d3
byly	být	k5eAaImAgFnP
výpravy	výprava	k1gMnPc4
proti	proti	k7c3
jihofrancouzským	jihofrancouzský	k2eAgInPc3d1
katarům	katar	k1gInPc3
a	a	k8xC
proti	proti	k7c3
českým	český	k2eAgMnPc3d1
husitům	husita	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc4d1
kruciátu	kruciáta	k1gFnSc4
se	se	k3xPyFc4
bezúspěšně	bezúspěšně	k6eAd1
snažil	snažit	k5eAaImAgMnS
zorganizovat	zorganizovat	k5eAaPmF
počátkem	počátkem	k7c2
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
Lev	Lev	k1gMnSc1
X.	X.	kA
proti	proti	k7c3
osmanským	osmanský	k2eAgMnPc3d1
Turkům	Turek	k1gMnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výpravy	výprava	k1gFnPc4
proti	proti	k7c3
muslimům	muslim	k1gMnPc3
</s>
<s>
Pozadí	pozadí	k1gNnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článcích	článek	k1gInPc6
Islámská	islámský	k2eAgFnSc1d1
expanze	expanze	k1gFnSc1
<g/>
,	,	kIx,
Byzantsko-arabské	byzantsko-arabský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
Byzantsko-seldžucké	Byzantsko-seldžucký	k2eAgFnSc2d1
války	válka	k1gFnSc2
a	a	k8xC
Bitva	bitva	k1gFnSc1
u	u	k7c2
Mantzikertu	Mantzikert	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Území	území	k1gNnSc1
ovládané	ovládaný	k2eAgInPc1d1
Seldžuky	Seldžuk	k1gInPc1
v	v	k7c6
době	doba	k1gFnSc6
smrti	smrt	k1gFnSc2
sultána	sultán	k1gMnSc2
Malikšáha	Malikšáh	k1gMnSc2
(	(	kIx(
<g/>
1092	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
polovině	polovina	k1gFnSc6
11	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
na	na	k7c4
Blízký	blízký	k2eAgInSc4d1
východ	východ	k1gInSc4
začaly	začít	k5eAaPmAgFnP
pronikat	pronikat	k5eAaImF
ze	z	k7c2
střední	střední	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
kočovné	kočovný	k2eAgNnSc1d1
turecké	turecký	k2eAgInPc1d1
kmeny	kmen	k1gInPc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
začaly	začít	k5eAaPmAgFnP
povážlivě	povážlivě	k6eAd1
ohrožovat	ohrožovat	k5eAaImF
byzantskou	byzantský	k2eAgFnSc4d1
moc	moc	k1gFnSc4
v	v	k7c6
Anatolii	Anatolie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byzantský	byzantský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
Roman	Roman	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diogenes	Diogenes	k1gMnSc1
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgMnS
s	s	k7c7
Turky	turek	k1gInPc7
utkat	utkat	k5eAaPmF
v	v	k7c6
otevřené	otevřený	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
byl	být	k5eAaImAgInS
v	v	k7c6
srpnu	srpen	k1gInSc6
roku	rok	k1gInSc2
1071	#num#	k4
poražen	porazit	k5eAaPmNgInS
u	u	k7c2
Mantzikertu	Mantzikert	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Turci	Turek	k1gMnPc1
postupně	postupně	k6eAd1
obsadili	obsadit	k5eAaPmAgMnP
Sýrii	Sýrie	k1gFnSc4
<g/>
,	,	kIx,
Palestinu	Palestina	k1gFnSc4
a	a	k8xC
téměř	téměř	k6eAd1
celou	celý	k2eAgFnSc4d1
Malou	malý	k2eAgFnSc4d1
Asii	Asie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1078	#num#	k4
dobyli	dobýt	k5eAaPmAgMnP
také	také	k9
Jeruzalém	Jeruzalém	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
dosud	dosud	k6eAd1
ovládaný	ovládaný	k2eAgInSc1d1
šíitskými	šíitský	k2eAgInPc7d1
Fátimovci	Fátimovec	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
byla	být	k5eAaImAgFnS
křesťanská	křesťanský	k2eAgFnSc1d1
církev	církev	k1gFnSc1
rozdělena	rozdělit	k5eAaPmNgFnS
schizmatem	schizma	k1gNnSc7
<g/>
,	,	kIx,
zrodila	zrodit	k5eAaPmAgFnS
se	se	k3xPyFc4
myšlenka	myšlenka	k1gFnSc1
na	na	k7c4
pomoc	pomoc	k1gFnSc4
západních	západní	k2eAgMnPc2d1
křesťanů	křesťan	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Byzantský	byzantský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
Alexios	Alexiosa	k1gFnPc2
I.	I.	kA
toužil	toužit	k5eAaImAgInS
získat	získat	k5eAaPmF
nazpět	nazpět	k6eAd1
území	území	k1gNnPc4
dobytá	dobytý	k2eAgNnPc4d1
Seldžuky	Seldžuk	k1gInPc1
s	s	k7c7
pomocí	pomoc	k1gFnSc7
evropských	evropský	k2eAgMnPc2d1
žoldnéřů	žoldnéř	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snažil	snažit	k5eAaImAgMnS
se	se	k3xPyFc4
tedy	tedy	k9
zlepšit	zlepšit	k5eAaPmF
vztahy	vztah	k1gInPc4
mezi	mezi	k7c7
východní	východní	k2eAgFnSc7d1
a	a	k8xC
západní	západní	k2eAgFnSc7d1
církví	církev	k1gFnSc7
a	a	k8xC
vedl	vést	k5eAaImAgInS
čilou	čilý	k2eAgFnSc4d1
diplomatickou	diplomatický	k2eAgFnSc4d1
korespondenci	korespondence	k1gFnSc4
s	s	k7c7
papežem	papež	k1gMnSc7
Urbanem	Urban	k1gMnSc7
II	II	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Papež	Papež	k1gMnSc1
v	v	k7c6
době	doba	k1gFnSc6
boje	boj	k1gInSc2
o	o	k7c4
investituru	investitura	k1gFnSc4
s	s	k7c7
císařem	císař	k1gMnSc7
Svaté	svatý	k2eAgFnSc2d1
říše	říš	k1gFnSc2
římské	římský	k2eAgFnSc2d1
Jindřichem	Jindřich	k1gMnSc7
IV	IV	kA
<g/>
.	.	kIx.
také	také	k9
neměl	mít	k5eNaImAgInS
zájem	zájem	k1gInSc1
prohlubovat	prohlubovat	k5eAaImF
spory	spor	k1gInPc4
s	s	k7c7
ortodoxní	ortodoxní	k2eAgFnSc7d1
Byzancí	Byzanc	k1gFnSc7
a	a	k8xC
vyhlášením	vyhlášení	k1gNnSc7
kruciáty	kruciáta	k1gFnSc2
ukázal	ukázat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
papež	papež	k1gMnSc1
je	být	k5eAaImIp3nS
ten	ten	k3xDgMnSc1
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
řídí	řídit	k5eAaImIp3nS
západní	západní	k2eAgNnSc4d1
křesťanstvo	křesťanstvo	k1gNnSc4
a	a	k8xC
nikoli	nikoli	k9
císař	císař	k1gMnSc1
Svaté	svatý	k2eAgFnSc2d1
říše	říš	k1gFnSc2
římské	římský	k2eAgFnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Roku	rok	k1gInSc2
1095	#num#	k4
byl	být	k5eAaImAgInS
Jeruzalém	Jeruzalém	k1gInSc1
napaden	napaden	k2eAgInSc1d1
Araby	Arab	k1gMnPc7
z	z	k7c2
Egypta	Egypt	k1gInSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
město	město	k1gNnSc4
vydrancovali	vydrancovat	k5eAaPmAgMnP
<g/>
,	,	kIx,
muslimy	muslim	k1gMnPc4
pobili	pobít	k5eAaPmAgMnP
a	a	k8xC
zbylé	zbylý	k2eAgNnSc4d1
křesťanské	křesťanský	k2eAgNnSc4d1
obyvatelstvo	obyvatelstvo	k1gNnSc4
vyhnali	vyhnat	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
západní	západní	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
11	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
mezitím	mezitím	k6eAd1
začal	začít	k5eAaPmAgInS
pod	pod	k7c7
vlivem	vliv	k1gInSc7
clunyjské	clunyjský	k2eAgFnSc2d1
reformy	reforma	k1gFnSc2
a	a	k8xC
zintenzívnění	zintenzívnění	k1gNnSc4
náboženského	náboženský	k2eAgInSc2d1
života	život	k1gInSc2
růst	růst	k5eAaImF
zájem	zájem	k1gInSc4
o	o	k7c4
svatá	svatý	k2eAgNnPc4d1
místa	místo	k1gNnPc4
<g/>
,	,	kIx,
především	především	k9
o	o	k7c4
Jeruzalém	Jeruzalém	k1gInSc4
<g/>
,	,	kIx,
Řím	Řím	k1gInSc4
a	a	k8xC
Compostelu	Compostela	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
zájem	zájem	k1gInSc1
vycházel	vycházet	k5eAaImAgInS
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
z	z	k7c2
intenzivnějšího	intenzivní	k2eAgNnSc2d2
eschatologického	eschatologický	k2eAgNnSc2d1
očekávání	očekávání	k1gNnSc2
konce	konec	k1gInSc2
světa	svět	k1gInSc2
a	a	k8xC
z	z	k7c2
pocitu	pocit	k1gInSc2
viny	vina	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
Boží	boží	k2eAgInSc1d1
hrob	hrob	k1gInSc1
<g/>
,	,	kIx,
pro	pro	k7c4
křesťany	křesťan	k1gMnPc4
místo	místo	k7c2
Ježíšova	Ježíšův	k2eAgNnSc2d1
vzkříšení	vzkříšení	k1gNnSc2
<g/>
,	,	kIx,
zůstává	zůstávat	k5eAaImIp3nS
v	v	k7c6
rukou	ruka	k1gFnPc6
muslimů	muslim	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
navíc	navíc	k6eAd1
poutníky	poutník	k1gMnPc7
ve	v	k7c6
Svaté	svatý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
ohrožují	ohrožovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
se	se	k3xPyFc4
prosadilo	prosadit	k5eAaPmAgNnS
přesvědčení	přesvědčení	k1gNnSc3
<g/>
,	,	kIx,
že	že	k8xS
ofenzivní	ofenzivní	k2eAgInSc1d1
boj	boj	k1gInSc1
za	za	k7c4
získání	získání	k1gNnSc4
svatých	svatý	k2eAgNnPc2d1
míst	místo	k1gNnPc2
a	a	k8xC
s	s	k7c7
tím	ten	k3xDgNnSc7
související	související	k2eAgNnSc1d1
zajištění	zajištění	k1gNnSc1
ochrany	ochrana	k1gFnSc2
křesťanských	křesťanský	k2eAgMnPc2d1
poutníků	poutník	k1gMnPc2
je	být	k5eAaImIp3nS
spravedlivý	spravedlivý	k2eAgInSc1d1
a	a	k8xC
měl	mít	k5eAaImAgInS
by	by	kYmCp3nS
se	se	k3xPyFc4
stát	stát	k1gInSc1
povinností	povinnost	k1gFnPc2
každého	každý	k3xTgMnSc2
rytíře	rytíř	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
11	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
iniciativy	iniciativa	k1gFnSc2
ujali	ujmout	k5eAaPmAgMnP
papežové	papež	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Formování	formování	k1gNnSc3
ideologie	ideologie	k1gFnSc2
křížových	křížový	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
podpořil	podpořit	k5eAaPmAgMnS
zvláště	zvláště	k6eAd1
papež	papež	k1gMnSc1
Řehoř	Řehoř	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Západoevropská	západoevropský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
také	také	k9
byla	být	k5eAaImAgFnS
již	již	k6eAd1
plně	plně	k6eAd1
feudalizována	feudalizován	k2eAgFnSc1d1
<g/>
,	,	kIx,
majetkové	majetkový	k2eAgFnPc1d1
poměry	poměra	k1gFnPc1
se	se	k3xPyFc4
upevnily	upevnit	k5eAaPmAgFnP
a	a	k8xC
pro	pro	k7c4
feudální	feudální	k2eAgFnSc4d1
třídu	třída	k1gFnSc4
se	se	k3xPyFc4
stávala	stávat	k5eAaImAgFnS
akutní	akutní	k2eAgFnSc1d1
otázka	otázka	k1gFnSc1
získání	získání	k1gNnSc2
nové	nový	k2eAgFnSc2d1
půdy	půda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Léna	Léna	k1gFnSc1
totiž	totiž	k9
mohla	moct	k5eAaImAgFnS
přecházet	přecházet	k5eAaImF
pouze	pouze	k6eAd1
na	na	k7c4
nejstarší	starý	k2eAgMnPc4d3
potomky	potomek	k1gMnPc4
<g/>
,	,	kIx,
mladší	mladý	k2eAgMnSc1d2
synové	syn	k1gMnPc1
zůstávali	zůstávat	k5eAaImAgMnP
nezaopatřeni	zaopatřit	k5eNaPmNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rytíři	rytíř	k1gMnPc1
potřebovali	potřebovat	k5eAaImAgMnP
nová	nový	k2eAgNnPc4d1
léna	léno	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yIgNnPc4,k3yRgNnPc4,k3yQgNnPc4
mohli	moct	k5eAaImAgMnP
získat	získat	k5eAaPmF
výbojem	výboj	k1gInSc7
na	na	k7c4
východ	východ	k1gInSc4
<g/>
,	,	kIx,
tehdy	tehdy	k6eAd1
legendární	legendární	k2eAgMnSc1d1
<g/>
,	,	kIx,
bohatstvím	bohatství	k1gNnPc3
oplývající	oplývající	k2eAgFnSc4d1
zem	zem	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tažení	tažení	k1gNnSc1
na	na	k7c4
východ	východ	k1gInSc4
tak	tak	k6eAd1
umožňovalo	umožňovat	k5eAaImAgNnS
spojit	spojit	k5eAaPmF
poslání	poslání	k1gNnSc1
rytíře	rytíř	k1gMnSc2
s	s	k7c7
nadějí	naděje	k1gFnSc7
na	na	k7c4
hmotné	hmotný	k2eAgInPc4d1
zisky	zisk	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
</s>
<s>
Papež	Papež	k1gMnSc1
Urban	Urban	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
na	na	k7c6
koncilu	koncil	k1gInSc6
v	v	k7c4
Clermontuiluminace	Clermontuiluminace	k1gFnPc4
z	z	k7c2
Livre	livr	k1gInSc5
des	des	k1gNnSc7
Passages	Passages	k1gInSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Outre-mer	Outre-mer	k1gMnSc1
<g/>
,	,	kIx,
kolem	kolem	k7c2
roku	rok	k1gInSc2
1490	#num#	k4
(	(	kIx(
<g/>
Bibliothè	Bibliothè	k1gInSc2
Nationale	Nationale	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článcích	článek	k1gInPc6
Lidová	lidový	k2eAgFnSc1d1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
<g/>
,	,	kIx,
První	první	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
<g/>
,	,	kIx,
Jeruzalémské	jeruzalémský	k2eAgNnSc1d1
království	království	k1gNnSc1
a	a	k8xC
Křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
roku	rok	k1gInSc2
1101	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
březnu	březen	k1gInSc6
1095	#num#	k4
přijelo	přijet	k5eAaPmAgNnS
na	na	k7c4
synodu	synoda	k1gFnSc4
do	do	k7c2
Piacenzy	Piacenza	k1gFnSc2
byzantské	byzantský	k2eAgNnSc1d1
poselstvo	poselstvo	k1gNnSc1
císaře	císař	k1gMnSc2
Alexia	Alexius	k1gMnSc2
I.	I.	kA
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
s	s	k7c7
žádostí	žádost	k1gFnSc7
o	o	k7c4
pomoc	pomoc	k1gFnSc4
proti	proti	k7c3
seldžucké	seldžucký	k2eAgFnSc3d1
expanzi	expanze	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
listopadu	listopad	k1gInSc6
1095	#num#	k4
svolal	svolat	k5eAaPmAgMnS
Řehořův	Řehořův	k2eAgMnSc1d1
žák	žák	k1gMnSc1
Urban	Urban	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
koncil	koncil	k1gInSc1
do	do	k7c2
francouzského	francouzský	k2eAgInSc2d1
Clermontu	Clermont	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
závěr	závěr	k1gInSc4
sněmu	sněm	k1gInSc2
pronesl	pronést	k5eAaPmAgInS
strhující	strhující	k2eAgInSc1d1
proslov	proslov	k1gInSc1
o	o	k7c4
utrpení	utrpení	k1gNnSc4
východních	východní	k2eAgMnPc2d1
křesťanů	křesťan	k1gMnPc2
i	i	k8xC
poutníků	poutník	k1gMnPc2
ze	z	k7c2
Západu	západ	k1gInSc2
a	a	k8xC
zakončil	zakončit	k5eAaPmAgInS
jej	on	k3xPp3gMnSc4
výzvou	výzva	k1gFnSc7
pomoci	pomoct	k5eAaPmF
východním	východní	k2eAgMnPc3d1
křesťanům	křesťan	k1gMnPc3
sužovaným	sužovaný	k2eAgMnPc3d1
nevěřícími	věřící	k2eNgMnPc7d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Kromě	kromě	k7c2
obrany	obrana	k1gFnSc2
Božího	boží	k2eAgInSc2d1
hrobu	hrob	k1gInSc2
měl	mít	k5eAaImAgMnS
Urban	Urban	k1gMnSc1
také	také	k9
mocenské	mocenský	k2eAgFnSc2d1
a	a	k8xC
církevně	církevně	k6eAd1
politické	politický	k2eAgInPc4d1
důvody	důvod	k1gInPc4
<g/>
:	:	kIx,
jednak	jednak	k8xC
probíhal	probíhat	k5eAaImAgInS
boj	boj	k1gInSc1
o	o	k7c4
investituru	investitura	k1gFnSc4
<g />
.	.	kIx.
</s>
<s hack="1">
s	s	k7c7
císařem	císař	k1gMnSc7
Jindřichem	Jindřich	k1gMnSc7
IV	IV	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
jehož	jenž	k3xRgMnSc4,k3xOyRp3gMnSc4
exponent	exponent	k1gMnSc1
vzdoropapež	vzdoropapež	k1gMnSc1
Kliment	Kliment	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
se	se	k3xPyFc4
právě	právě	k9
zmocnil	zmocnit	k5eAaPmAgMnS
papežského	papežský	k2eAgInSc2d1
stolce	stolec	k1gInSc2
v	v	k7c6
Římě	Řím	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Navíc	navíc	k6eAd1
byl	být	k5eAaImAgMnS
Alexios	Alexios	k1gInSc4
I.	I.	kA
ochoten	ochoten	k2eAgMnSc1d1
odstranit	odstranit	k5eAaPmF
rozkol	rozkol	k1gInSc4
mezi	mezi	k7c7
Byzancí	Byzanc	k1gFnSc7
a	a	k8xC
západní	západní	k2eAgFnSc7d1
církví	církev	k1gFnSc7
<g/>
,	,	kIx,
vyjde	vyjít	k5eAaPmIp3nS
<g/>
-li	-li	k?
mu	on	k3xPp3gNnSc3
papež	papež	k1gMnSc1
vstříc	vstříc	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přestože	přestože	k8xS
byla	být	k5eAaImAgFnS
Urbanova	Urbanův	k2eAgFnSc1d1
výzva	výzva	k1gFnSc1
adresována	adresovat	k5eAaBmNgFnS
šlechtě	šlechta	k1gFnSc3
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
přišla	přijít	k5eAaPmAgFnS
okamžitá	okamžitý	k2eAgFnSc1d1
reakce	reakce	k1gFnSc1
ze	z	k7c2
strany	strana	k1gFnSc2
chudiny	chudina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
dojmem	dojem	k1gInSc7
vystoupení	vystoupení	k1gNnSc2
entuziastických	entuziastický	k2eAgMnPc2d1
kazatelů	kazatel	k1gMnPc2
se	se	k3xPyFc4
shromáždily	shromáždit	k5eAaPmAgInP
početné	početný	k2eAgInPc1d1
chudinské	chudinský	k2eAgInPc1d1
houfy	houf	k1gInPc1
a	a	k8xC
bez	bez	k7c2
přípravy	příprava	k1gFnSc2
se	se	k3xPyFc4
vydaly	vydat	k5eAaPmAgFnP
na	na	k7c4
cestu	cesta	k1gFnSc4
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvýraznější	výrazný	k2eAgFnSc1d3
z	z	k7c2
těchto	tento	k3xDgMnPc2
kazatelů	kazatel	k1gMnPc2
byl	být	k5eAaImAgMnS
Petr	Petr	k1gMnSc1
Poustevník	poustevník	k1gMnSc1
<g/>
,	,	kIx,
charismatický	charismatický	k2eAgMnSc1d1
demagog	demagog	k1gMnSc1
s	s	k7c7
fanatickou	fanatický	k2eAgFnSc7d1
nenávistí	nenávist	k1gFnSc7
k	k	k7c3
Turkům	turek	k1gInPc3
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
kázal	kázat	k5eAaImAgInS
především	především	k6eAd1
v	v	k7c6
severní	severní	k2eAgFnSc6d1
Francii	Francie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1096	#num#	k4
vyrazil	vyrazit	k5eAaPmAgMnS
Petr	Petr	k1gMnSc1
i	i	k9
se	s	k7c7
svými	svůj	k3xOyFgFnPc7
asi	asi	k9
20	#num#	k4
000	#num#	k4
následovníky	následovník	k1gMnPc7
ze	z	k7c2
severní	severní	k2eAgFnSc2d1
Francie	Francie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Téměř	téměř	k6eAd1
souběžně	souběžně	k6eAd1
s	s	k7c7
nimi	on	k3xPp3gMnPc7
táhl	táhnout	k5eAaImAgMnS
menší	malý	k2eAgNnSc4d2
chudinský	chudinský	k2eAgInSc4d1
houf	houf	k1gInSc4
pod	pod	k7c7
velením	velení	k1gNnSc7
Gautiera	Gautiero	k1gNnSc2
Sans-Avoir	Sans-Avoir	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Výpravy	výprava	k1gFnPc1
táhly	táhnout	k5eAaImAgFnP
většinou	většina	k1gFnSc7
odděleně	odděleně	k6eAd1
na	na	k7c4
východ	východ	k1gInSc4
<g/>
,	,	kIx,
přes	přes	k7c4
Svatou	svatý	k2eAgFnSc4d1
říši	říše	k1gFnSc4
římskou	římský	k2eAgFnSc4d1
do	do	k7c2
Uherska	Uhersko	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
na	na	k7c6
uhersko-byzantské	uhersko-byzantský	k2eAgFnSc6d1
hranici	hranice	k1gFnSc6
začaly	začít	k5eAaPmAgInP
z	z	k7c2
nedostatku	nedostatek	k1gInSc2
zásob	zásoba	k1gFnPc2
drancovat	drancovat	k5eAaImF
široké	široký	k2eAgNnSc4d1
okolí	okolí	k1gNnSc4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1096	#num#	k4
poutníci	poutník	k1gMnPc1
loupili	loupit	k5eAaImAgMnP
i	i	k9
na	na	k7c6
předměstí	předměstí	k1gNnSc6
Konstantinopole	Konstantinopol	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Císař	Císař	k1gMnSc1
Alexios	Alexios	k1gMnSc1
dal	dát	k5eAaPmAgMnS
výpravu	výprava	k1gFnSc4
urychleně	urychleně	k6eAd1
přepravit	přepravit	k5eAaPmF
na	na	k7c4
asijskou	asijský	k2eAgFnSc4d1
pevninu	pevnina	k1gFnSc4
do	do	k7c2
opevněného	opevněný	k2eAgInSc2d1
tábora	tábor	k1gInSc2
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
městečka	městečko	k1gNnSc2
Kibotos	Kibotosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chabě	chabě	k6eAd1
vyzbrojení	vyzbrojený	k2eAgMnPc1d1
poutníci	poutník	k1gMnPc1
podnikali	podnikat	k5eAaImAgMnP
z	z	k7c2
tábora	tábor	k1gInSc2
loupežné	loupežný	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
do	do	k7c2
okolí	okolí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
pochodu	pochod	k1gInSc6
k	k	k7c3
Nikáji	Nikáj	k1gInSc3
se	se	k3xPyFc4
poutníci	poutník	k1gMnPc1
zmocnili	zmocnit	k5eAaPmAgMnP
pevnůstky	pevnůstka	k1gFnPc4
Xerigordon	Xerigordona	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
neměla	mít	k5eNaImAgFnS
vlastní	vlastní	k2eAgInSc4d1
zdroj	zdroj	k1gInSc4
vody	voda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Turci	Turek	k1gMnPc1
pod	pod	k7c7
velením	velení	k1gNnSc7
Kiliče	Kilič	k1gMnSc2
Arslana	Arslan	k1gMnSc2
Xerigordon	Xerigordon	k1gInSc4
obklíčili	obklíčit	k5eAaPmAgMnP
a	a	k8xC
nechali	nechat	k5eAaPmAgMnP
pracovat	pracovat	k5eAaImF
žízeň	žízeň	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pevnost	pevnost	k1gFnSc1
na	na	k7c6
počátku	počátek	k1gInSc6
října	říjen	k1gInSc2
1096	#num#	k4
padla	padnout	k5eAaImAgFnS,k5eAaPmAgFnS
a	a	k8xC
Turci	Turek	k1gMnPc1
vyvraždili	vyvraždit	k5eAaPmAgMnP
na	na	k7c4
6	#num#	k4
tisíc	tisíc	k4xCgInPc2
poutníků	poutník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Turci	Turek	k1gMnPc1
v	v	k7c6
dalším	další	k2eAgInSc6d1
pochodu	pochod	k1gInSc6
na	na	k7c6
Nikáju	Nikáju	k1gFnSc6
vyvraždili	vyvraždit	k5eAaPmAgMnP
zbytek	zbytek	k1gInSc4
poutníků	poutník	k1gMnPc2
a	a	k8xC
tábor	tábor	k1gInSc1
u	u	k7c2
Kibotu	Kibot	k1gInSc2
vydrancovali	vydrancovat	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dva	dva	k4xCgInPc1
až	až	k9
tři	tři	k4xCgInPc1
tisíce	tisíc	k4xCgInPc1
zachráněných	zachráněný	k2eAgMnPc2d1
poutníků	poutník	k1gMnPc2
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
byzantské	byzantský	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
evakuovalo	evakuovat	k5eAaBmAgNnS
zpět	zpět	k6eAd1
do	do	k7c2
Konstantinopole	Konstantinopol	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Godefroy	Godefroy	k1gInPc1
z	z	k7c2
Bouillonu	Bouillon	k1gInSc2
jako	jako	k8xC,k8xS
vůdce	vůdce	k1gMnSc1
výpravyHistoria	výpravyHistorium	k1gNnSc2
rerum	rerum	k1gInSc1
in	in	k?
partibus	partibus	k1gInSc1
transmarinis	transmarinis	k1gInSc1
gestarum	gestarum	k1gInSc1
(	(	kIx(
<g/>
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
Evropě	Evropa	k1gFnSc6
se	se	k3xPyFc4
zatím	zatím	k6eAd1
zformovalo	zformovat	k5eAaPmAgNnS
několik	několik	k4yIc1
dalších	další	k2eAgFnPc2d1
chudinských	chudinský	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
zavinily	zavinit	k5eAaPmAgFnP
brutální	brutální	k2eAgInPc4d1
pogromy	pogrom	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Porýnští	porýnský	k2eAgMnPc1d1
chudí	chudý	k1gMnPc1
vedení	vedení	k1gNnSc2
hrabětem	hrabě	k1gMnSc7
Emerichem	Emerich	k1gMnSc7
z	z	k7c2
Leisingnenu	Leisingnen	k1gInSc2
v	v	k7c6
Mohuči	Mohuč	k1gFnSc6
i	i	k8xC
v	v	k7c6
dalších	další	k2eAgNnPc6d1
městech	město	k1gNnPc6
pobili	pobít	k5eAaPmAgMnP
tisíce	tisíc	k4xCgInSc2
Židů	Žid	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
Lotrinska	Lotrinsko	k1gNnSc2
se	se	k3xPyFc4
vydala	vydat	k5eAaPmAgFnS
na	na	k7c4
cestu	cesta	k1gFnSc4
výprava	výprava	k1gFnSc1
vedená	vedený	k2eAgFnSc1d1
knězem	kněz	k1gMnSc7
Gottschalkem	Gottschalek	k1gMnSc7
a	a	k8xC
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
postavil	postavit	k5eAaPmAgMnS
menší	malý	k2eAgFnSc4d2
skupinu	skupina	k1gFnSc4
kněz	kněz	k1gMnSc1
jménem	jméno	k1gNnSc7
Volkmar	Volkmara	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
rozpoutal	rozpoutat	k5eAaPmAgInS
pogromy	pogrom	k1gInPc4
také	také	k9
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
navzdory	navzdory	k7c3
snahám	snaha	k1gFnPc3
biskupa	biskup	k1gInSc2
Kosmy	Kosma	k1gMnPc4
tomu	ten	k3xDgNnSc3
zabránit	zabránit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
uherských	uherský	k2eAgFnPc6d1
hranicích	hranice	k1gFnPc6
byly	být	k5eAaImAgFnP
tyto	tento	k3xDgFnPc1
výpravy	výprava	k1gFnPc1
rozprášeny	rozprášen	k2eAgFnPc1d1
vojskem	vojsko	k1gNnSc7
krále	král	k1gMnSc2
Kolomana	Koloman	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Dobytí	dobytí	k1gNnSc1
Jeruzaléma	Jeruzalém	k1gInSc2
křižáky	křižák	k1gMnPc7
ze	z	k7c2
středověkého	středověký	k2eAgInSc2d1
rukopisu	rukopis	k1gInSc2
</s>
<s>
Rytíři	rytíř	k1gMnPc1
se	se	k3xPyFc4
na	na	k7c4
cestu	cesta	k1gFnSc4
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
vydali	vydat	k5eAaPmAgMnP
až	až	k9
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
severní	severní	k2eAgFnSc2d1
Francie	Francie	k1gFnSc2
vytáhla	vytáhnout	k5eAaPmAgFnS
výprava	výprava	k1gFnSc1
hraběte	hrabě	k1gMnSc2
Roberta	Robert	k1gMnSc2
Normandského	normandský	k2eAgMnSc2d1
a	a	k8xC
Roberta	Robert	k1gMnSc2
Flanderského	flanderský	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
střední	střední	k2eAgFnSc2d1
Francie	Francie	k1gFnSc2
se	se	k3xPyFc4
přidal	přidat	k5eAaPmAgMnS
hrabě	hrabě	k1gMnSc1
Štěpán	Štěpán	k1gMnSc1
z	z	k7c2
Blois	Blois	k1gFnSc2
a	a	k8xC
Hugo	Hugo	k1gMnSc1
z	z	k7c2
Vermandois	Vermandois	k1gFnSc2
<g/>
,	,	kIx,
bratr	bratr	k1gMnSc1
krále	král	k1gMnSc2
Filipa	Filip	k1gMnSc2
I.	I.	kA
Lotrinské	lotrinský	k2eAgNnSc4d1
vojsko	vojsko	k1gNnSc4
vedli	vést	k5eAaImAgMnP
bratři	bratr	k1gMnPc1
z	z	k7c2
Boulogne	Boulogn	k1gInSc5
Godefroy	Godefroy	k1gInPc7
<g/>
,	,	kIx,
Eustach	Eustach	k1gMnSc1
a	a	k8xC
Balduin	Balduin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oddíly	oddíl	k1gInPc7
z	z	k7c2
Provence	Provence	k1gFnSc2
vedl	vést	k5eAaImAgMnS
toulouský	toulouský	k2eAgMnSc1d1
hrabě	hrabě	k1gMnSc1
Raimond	Raimond	k1gMnSc1
de	de	k?
Saint-Gilles	Saint-Gilles	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
cestě	cesta	k1gFnSc6
Huga	Hugo	k1gMnSc2
z	z	k7c2
Vermandois	Vermandois	k1gFnSc2
jižní	jižní	k2eAgFnSc7d1
Itálií	Itálie	k1gFnSc7
se	se	k3xPyFc4
k	k	k7c3
první	první	k4xOgFnSc3
křížové	křížový	k2eAgFnSc3d1
výpravě	výprava	k1gFnSc3
přidal	přidat	k5eAaPmAgMnS
další	další	k2eAgMnSc1d1
normanský	normanský	k2eAgMnSc1d1
válečník	válečník	k1gMnSc1
<g/>
,	,	kIx,
kníže	kníže	k1gMnSc1
Bohemund	Bohemund	k1gMnSc1
z	z	k7c2
Tarentu	Tarent	k1gInSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
synovec	synovec	k1gMnSc1
Tankred	Tankred	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
přelomu	přelom	k1gInSc6
let	léto	k1gNnPc2
1096	#num#	k4
<g/>
–	–	k?
<g/>
1097	#num#	k4
velmoži	velmož	k1gMnPc1
dorazili	dorazit	k5eAaPmAgMnP
do	do	k7c2
Konstantinopole	Konstantinopol	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
císaři	císař	k1gMnPc1
složili	složit	k5eAaPmAgMnP
lenní	lenní	k2eAgFnSc4d1
přísahu	přísaha	k1gFnSc4
a	a	k8xC
císař	císař	k1gMnSc1
na	na	k7c4
oplátku	oplátka	k1gFnSc4
přislíbil	přislíbit	k5eAaPmAgMnS
výpravě	výprava	k1gFnSc3
podporu	podpora	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Prvním	první	k4xOgInSc7
cílem	cíl	k1gInSc7
křižáckých	křižácký	k2eAgMnPc2d1
rytířů	rytíř	k1gMnPc2
se	se	k3xPyFc4
na	na	k7c6
jaře	jaro	k1gNnSc6
1097	#num#	k4
stala	stát	k5eAaPmAgFnS
Nikája	Nikája	k1gFnSc1
v	v	k7c6
Anatolii	Anatolie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
byla	být	k5eAaImAgFnS
po	po	k7c6
sedmitýdenním	sedmitýdenní	k2eAgNnSc6d1
obléhání	obléhání	k1gNnSc6
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
dobyta	dobýt	k5eAaPmNgFnS
a	a	k8xC
křižáci	křižák	k1gMnPc1
se	se	k3xPyFc4
vydali	vydat	k5eAaPmAgMnP
k	k	k7c3
východu	východ	k1gInSc3
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
roku	rok	k1gInSc2
1097	#num#	k4
porazili	porazit	k5eAaPmAgMnP
křižáci	křižák	k1gMnPc1
v	v	k7c6
údolí	údolí	k1gNnSc6
u	u	k7c2
Dorylea	Doryleus	k1gMnSc2
vojska	vojsko	k1gNnSc2
Kiliče	Kilič	k1gMnSc2
Arslana	Arslan	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
únoru	únor	k1gInSc6
1098	#num#	k4
obsadil	obsadit	k5eAaPmAgMnS
Balduin	Balduin	k1gMnSc1
z	z	k7c2
Boulogne	Boulogn	k1gInSc5
Edessu	Edess	k1gMnSc6
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
založil	založit	k5eAaPmAgMnS
první	první	k4xOgInSc4
křižácký	křižácký	k2eAgInSc4d1
stát	stát	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojsko	vojsko	k1gNnSc1
táhlo	táhnout	k5eAaImAgNnS
na	na	k7c4
jih	jih	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
oblehlo	oblehnout	k5eAaPmAgNnS
Antiochii	Antiochie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
ročním	roční	k2eAgNnSc6d1
obléhání	obléhání	k1gNnSc6
se	s	k7c7
3	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1098	#num#	k4
Bohemundovi	Bohemund	k1gMnSc6
z	z	k7c2
Tarentu	Tarent	k1gInSc2
podařilo	podařit	k5eAaPmAgNnS
město	město	k1gNnSc1
díky	díky	k7c3
zradě	zrada	k1gFnSc3
zbrojíře	zbrojíř	k1gMnSc2
Fírúze	Fírúze	k1gFnSc2
obsadit	obsadit	k5eAaPmF
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
po	po	k7c6
odražení	odražení	k1gNnSc6
odvetného	odvetný	k2eAgInSc2d1
útoku	útok	k1gInSc2
atabega	atabeg	k1gMnSc2
Kerbogy	Kerboga	k1gFnSc2
z	z	k7c2
Mosulu	Mosul	k1gInSc2
se	s	k7c7
vládcem	vládce	k1gMnSc7
města	město	k1gNnSc2
prohlásil	prohlásit	k5eAaPmAgMnS
Bohemund	Bohemund	k1gInSc4
z	z	k7c2
Tarentu	Tarent	k1gInSc2
(	(	kIx(
<g/>
založil	založit	k5eAaPmAgInS
tak	tak	k6eAd1
další	další	k2eAgInSc1d1
stát	stát	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbylí	zbylý	k2eAgMnPc1d1
křižáci	křižák	k1gMnPc1
táhli	táhnout	k5eAaImAgMnP
dál	daleko	k6eAd2
do	do	k7c2
Palestiny	Palestina	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
7	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
oblehli	oblehnout	k5eAaPmAgMnP
Jeruzalém	Jeruzalém	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
15	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1099	#num#	k4
dobyli	dobýt	k5eAaPmAgMnP
a	a	k8xC
drtivou	drtivý	k2eAgFnSc4d1
většinu	většina	k1gFnSc4
obyvatelstva	obyvatelstvo	k1gNnSc2
povraždili	povraždit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masakr	masakr	k1gInSc1
přežil	přežít	k5eAaPmAgMnS
jen	jen	k9
místodržící	místodržící	k1gMnSc1
Iftíchar	Iftíchar	k1gInSc4
ad-Daula	ad-Daulum	k1gNnSc2
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
gardou	garda	k1gFnSc7
a	a	k8xC
několik	několik	k4yIc1
civilistů	civilista	k1gMnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
poté	poté	k6eAd1
nalezli	nalézt	k5eAaBmAgMnP,k5eAaPmAgMnP
azyl	azyl	k1gInSc4
v	v	k7c6
Damašku	Damašek	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
Vládcem	vládce	k1gMnSc7
Jeruzaléma	Jeruzalém	k1gInSc2
byl	být	k5eAaImAgMnS
zvolen	zvolen	k2eAgMnSc1d1
vévoda	vévoda	k1gMnSc1
Godefroy	Godefroa	k1gFnSc2
z	z	k7c2
Bouillonu	Bouillon	k1gInSc2
s	s	k7c7
titulem	titul	k1gInSc7
„	„	k?
<g/>
ochránce	ochránce	k1gMnSc1
Božího	boží	k2eAgInSc2d1
hrobu	hrob	k1gInSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Roku	rok	k1gInSc2
1100	#num#	k4
byly	být	k5eAaImAgFnP
do	do	k7c2
Svaté	svatá	k1gFnSc2
země	zem	k1gFnPc1
vypraveny	vypraven	k2eAgInPc4d1
pomocné	pomocný	k2eAgInPc4d1
křížové	křížový	k2eAgInPc4d1
sbory	sbor	k1gInPc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
menší	malý	k2eAgNnPc1d2
<g/>
,	,	kIx,
špatně	špatně	k6eAd1
koordinované	koordinovaný	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
pokusily	pokusit	k5eAaPmAgFnP
posílit	posílit	k5eAaPmF
nově	nově	k6eAd1
založené	založený	k2eAgInPc1d1
křižácké	křižácký	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1101	#num#	k4
však	však	k9
byly	být	k5eAaImAgInP
rozbity	rozbit	k2eAgInPc1d1
Turky	turek	k1gInPc1
v	v	k7c6
Anatolii	Anatolie	k1gFnSc6
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
zůstala	zůstat	k5eAaPmAgFnS
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
až	až	k9
do	do	k7c2
konce	konec	k1gInSc2
křížových	křížový	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
téměř	téměř	k6eAd1
nepřekonatelnou	překonatelný	k2eNgFnSc7d1
překážkou	překážka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Norská	norský	k2eAgFnSc1d1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
</s>
<s>
Král	Král	k1gMnSc1
Sigurd	Sigurd	k1gMnSc1
I.	I.	kA
ve	v	k7c6
společnosti	společnost	k1gFnSc6
jeruzalémského	jeruzalémský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Balduina	Balduino	k1gNnSc2
I.	I.	kA
<g/>
,	,	kIx,
ilustrace	ilustrace	k1gFnPc1
Gerharda	Gerhard	k1gMnSc2
Munthea	Muntheus	k1gMnSc2
</s>
<s>
Výprava	výprava	k1gMnSc1
Sigurda	Sigurd	k1gMnSc2
I.	I.	kA
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článcích	článek	k1gInPc6
Norská	norský	k2eAgFnSc1d1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
<g/>
,	,	kIx,
Sigurd	Sigurd	k1gMnSc1
I.	I.	kA
Jorsalfar	Jorsalfar	k1gInSc1
a	a	k8xC
Obléhání	obléhání	k1gNnSc1
Sidónu	Sidón	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c7
lety	léto	k1gNnPc7
1107	#num#	k4
a	a	k8xC
1110	#num#	k4
proběhla	proběhnout	k5eAaPmAgFnS
jako	jako	k9
dozvuk	dozvuk	k1gInSc4
první	první	k4xOgFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
samostatná	samostatný	k2eAgFnSc1d1
norská	norský	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
vedená	vedený	k2eAgFnSc1d1
králem	král	k1gMnSc7
Sigurdem	Sigurd	k1gInSc7
I.	I.	kA
<g/>
,	,	kIx,
ten	ten	k3xDgInSc1
byl	být	k5eAaImAgInS
fakticky	fakticky	k6eAd1
prvním	první	k4xOgMnSc7
skandinávským	skandinávský	k2eAgMnSc7d1
a	a	k8xC
zároveň	zároveň	k6eAd1
i	i	k9
evropským	evropský	k2eAgMnSc7d1
králem	král	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
vydal	vydat	k5eAaPmAgMnS
na	na	k7c4
výpravu	výprava	k1gFnSc4
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účelem	účel	k1gInSc7
bylo	být	k5eAaImAgNnS
podpořit	podpořit	k5eAaPmF
křižácké	křižácký	k2eAgInPc4d1
státy	stát	k1gInPc4
<g/>
,	,	kIx,
nově	nově	k6eAd1
vzniklé	vzniklý	k2eAgInPc1d1
po	po	k7c6
první	první	k4xOgFnSc6
křížové	křížový	k2eAgFnSc6d1
výpravě	výprava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sigurdova	Sigurdův	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
vedla	vést	k5eAaImAgFnS
po	po	k7c6
moři	moře	k1gNnSc6
a	a	k8xC
probíhala	probíhat	k5eAaImAgFnS
ve	v	k7c6
stylu	styl	k1gInSc6
starých	starý	k2eAgFnPc2d1
vikingských	vikingský	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Daleká	daleký	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
seveřanů	seveřan	k1gMnPc2
k	k	k7c3
břehům	břeh	k1gInPc3
cílové	cílový	k2eAgFnSc2d1
Palestiny	Palestina	k1gFnSc2
provázely	provázet	k5eAaImAgFnP
krátké	krátká	k1gFnPc1
zastávky	zastávka	k1gFnSc2
v	v	k7c6
podobě	podoba	k1gFnSc6
rychlých	rychlý	k2eAgInPc2d1
výpadů	výpad	k1gInPc2
z	z	k7c2
moře	moře	k1gNnSc2
proti	proti	k7c3
državám	država	k1gFnPc3
Almorávidů	Almorávid	k1gMnPc2
<g/>
,	,	kIx,
plenění	plenění	k1gNnSc1
Iberského	iberský	k2eAgInSc2d1
poloostrova	poloostrov	k1gInSc2
a	a	k8xC
nájezdů	nájezd	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
zastávce	zastávka	k1gFnSc6
v	v	k7c6
Lisabonu	Lisabon	k1gInSc6
směřovala	směřovat	k5eAaImAgFnS
plavba	plavba	k1gFnSc1
k	k	k7c3
Baleárským	baleárský	k2eAgInPc3d1
ostrovům	ostrov	k1gInPc3
a	a	k8xC
dál	daleko	k6eAd2
na	na	k7c6
Sicílii	Sicílie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Palermu	Palermo	k1gNnSc6
byl	být	k5eAaImAgMnS
Sigurd	Sigurd	k1gMnSc1
přivítán	přivítat	k5eAaPmNgMnS
sicilským	sicilský	k2eAgMnSc7d1
hrabětem	hrabě	k1gMnSc7
Rogerem	Roger	k1gMnSc7
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
poté	poté	k6eAd1
se	se	k3xPyFc4
výprava	výprava	k1gFnSc1
krátce	krátce	k6eAd1
zastavila	zastavit	k5eAaPmAgFnS
dokonce	dokonce	k9
i	i	k9
v	v	k7c6
Konstantinopoli	Konstantinopol	k1gInSc6
<g/>
,	,	kIx,
teprve	teprve	k6eAd1
až	až	k9
posléze	posléze	k6eAd1
směřovala	směřovat	k5eAaImAgFnS
cesta	cesta	k1gFnSc1
do	do	k7c2
Jeruzaléma	Jeruzalém	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
Sigurd	Sigurd	k1gMnSc1
setkal	setkat	k5eAaPmAgMnS
a	a	k8xC
vojensky	vojensky	k6eAd1
spojil	spojit	k5eAaPmAgMnS
s	s	k7c7
jeruzalémským	jeruzalémský	k2eAgMnSc7d1
králem	král	k1gMnSc7
Balduinem	Balduin	k1gMnSc7
I.	I.	kA
V	v	k7c6
roce	rok	k1gInSc6
1110	#num#	k4
se	se	k3xPyFc4
seveřané	seveřan	k1gMnPc1
zúčastnili	zúčastnit	k5eAaPmAgMnP
obléhání	obléhání	k1gNnSc4
přístavního	přístavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Sidónu	Sidón	k1gInSc2
po	po	k7c6
boku	bok	k1gInSc6
křižáckých	křižácký	k2eAgMnPc2d1
Franků	Frank	k1gMnPc2
vedených	vedený	k2eAgMnPc2d1
Balduinem	Balduin	k1gInSc7
I.	I.	kA
a	a	k8xC
Benátčanů	Benátčan	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
bylo	být	k5eAaImAgNnS
úspěšně	úspěšně	k6eAd1
zakončeno	zakončit	k5eAaPmNgNnS
ovládnutím	ovládnutí	k1gNnSc7
města	město	k1gNnSc2
a	a	k8xC
založením	založení	k1gNnSc7
Sidónského	Sidónský	k2eAgNnSc2d1
panství	panství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sigurd	Sigurd	k1gMnSc1
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
do	do	k7c2
Norska	Norsko	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1111	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
Palestiny	Palestina	k1gFnSc2
si	se	k3xPyFc3
přivezl	přivézt	k5eAaPmAgMnS
relikvii	relikvie	k1gFnSc4
(	(	kIx(
<g/>
třísku	tříska	k1gFnSc4
z	z	k7c2
Pravého	pravý	k2eAgInSc2d1
kříže	kříž	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
získal	získat	k5eAaPmAgMnS
od	od	k7c2
Balduina	Balduino	k1gNnSc2
a	a	k8xC
pro	pro	k7c4
niž	jenž	k3xRgFnSc4
nechal	nechat	k5eAaPmAgMnS
zbudovat	zbudovat	k5eAaPmF
hrad	hrad	k1gInSc4
ve	v	k7c6
městě	město	k1gNnSc6
Konghelle	Konghelle	k1gFnSc2
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgInSc1d1
Kungälv	Kungälv	k1gInSc1
ve	v	k7c6
Švédsku	Švédsko	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1123	#num#	k4
podnikl	podniknout	k5eAaPmAgMnS
Sigurd	Sigurd	k1gMnSc1
jinou	jiný	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
vedla	vést	k5eAaImAgFnS
tentokrát	tentokrát	k6eAd1
do	do	k7c2
nedalekého	daleký	k2eNgInSc2d1
švédského	švédský	k2eAgInSc2d1
Små	Små	k1gInSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnPc1
obyvatelé	obyvatel	k1gMnPc1
se	se	k3xPyFc4
odvrátili	odvrátit	k5eAaPmAgMnP
od	od	k7c2
křesťanství	křesťanství	k1gNnSc2
zpět	zpět	k6eAd1
k	k	k7c3
víře	víra	k1gFnSc3
ve	v	k7c4
staré	starý	k2eAgMnPc4d1
pohanské	pohanský	k2eAgMnPc4d1
bohy	bůh	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sigurd	Sigurd	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1130	#num#	k4
a	a	k8xC
za	za	k7c4
dobu	doba	k1gFnSc4
jeho	jeho	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
v	v	k7c6
Norsku	Norsko	k1gNnSc6
se	se	k3xPyFc4
upevnila	upevnit	k5eAaPmAgFnS
moc	moc	k1gFnSc1
latinské	latinský	k2eAgFnSc2d1
církve	církev	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Druhá	druhý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
</s>
<s>
Opat	opat	k1gMnSc1
Bernard	Bernard	k1gMnSc1
z	z	k7c2
Clairvaux	Clairvaux	k1gInSc1
vyzývá	vyzývat	k5eAaImIp3nS
k	k	k7c3
zahájení	zahájení	k1gNnSc3
druhé	druhý	k4xOgFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
<g/>
,	,	kIx,
<g/>
(	(	kIx(
<g/>
Salles	Salles	k1gInSc1
des	des	k1gNnSc2
Croisades	Croisadesa	k1gFnPc2
<g/>
,	,	kIx,
Versailles	Versailles	k1gFnPc2
<g/>
,	,	kIx,
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Vyobrazení	vyobrazení	k1gNnSc1
příjezdu	příjezd	k1gInSc2
křižáků	křižák	k1gMnPc2
do	do	k7c2
Konstantinopole	Konstantinopol	k1gInSc2
</s>
<s>
Kníže	kníže	k1gMnSc1
Raimond	Raimond	k1gMnSc1
z	z	k7c2
Poitiers	Poitiersa	k1gFnPc2
vítá	vítat	k5eAaImIp3nS
v	v	k7c6
Antiochii	Antiochie	k1gFnSc6
krále	král	k1gMnSc2
Ludvíka	Ludvík	k1gMnSc2
</s>
<s>
Středověká	středověký	k2eAgFnSc1d1
miniatura	miniatura	k1gFnSc1
znázorňující	znázorňující	k2eAgMnPc4d1
templáře	templář	k1gMnPc4
při	při	k7c6
jejich	jejich	k3xOp3gInSc6
útoku	útok	k1gInSc6
na	na	k7c4
Nábulus	Nábulus	k1gInSc4
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článcích	článek	k1gInPc6
Obléhání	obléhání	k1gNnSc2
Edessy	Edessa	k1gFnSc2
a	a	k8xC
Druhá	druhý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
Štědrý	štědrý	k2eAgInSc4d1
den	den	k1gInSc4
roku	rok	k1gInSc2
1144	#num#	k4
turecký	turecký	k2eAgInSc1d1
Atabeg	Atabeg	k1gInSc1
Zengí	Zengí	k2eAgInSc1d1
z	z	k7c2
Mosulu	Mosul	k1gInSc2
dobyl	dobýt	k5eAaPmAgInS
Edessu	Edessa	k1gFnSc4
a	a	k8xC
křižáci	křižák	k1gMnPc1
tak	tak	k6eAd1
ztratili	ztratit	k5eAaPmAgMnP
velká	velký	k2eAgNnPc4d1
území	území	k1gNnPc4
na	na	k7c4
východ	východ	k1gInSc4
od	od	k7c2
řeky	řeka	k1gFnSc2
Eufrat	Eufrat	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pád	Pád	k1gInSc1
Edessy	Edessa	k1gFnSc2
otřásl	otřást	k5eAaPmAgInS
Evropou	Evropa	k1gFnSc7
a	a	k8xC
papež	papež	k1gMnSc1
Evžen	Evžen	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
vydal	vydat	k5eAaPmAgInS
bulu	bula	k1gFnSc4
Quantum	Quantum	k1gNnSc4
praedecessores	praedecessores	k1gInSc1
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
energicky	energicky	k6eAd1
se	se	k3xPyFc4
pustil	pustit	k5eAaPmAgMnS
do	do	k7c2
organizování	organizování	k1gNnSc2
další	další	k2eAgFnSc2d1
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
Bula	bula	k1gFnSc1
byla	být	k5eAaImAgFnS
adresována	adresovat	k5eAaBmNgFnS
především	především	k6eAd1
francouzskému	francouzský	k2eAgMnSc3d1
králi	král	k1gMnSc3
Ludvíkovi	Ludvík	k1gMnSc3
VII	VII	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
již	již	k6eAd1
o	o	k7c6
Vánocích	Vánoce	k1gFnPc6
roku	rok	k1gInSc2
1145	#num#	k4
ohlásil	ohlásit	k5eAaPmAgInS
svou	svůj	k3xOyFgFnSc4
účast	účast	k1gFnSc4
na	na	k7c6
výpravě	výprava	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
organizování	organizování	k1gNnSc6
kruciáty	kruciáta	k1gFnSc2
se	se	k3xPyFc4
podílel	podílet	k5eAaImAgMnS
i	i	k9
cisterciácký	cisterciácký	k2eAgMnSc1d1
myslitel	myslitel	k1gMnSc1
Bernard	Bernard	k1gMnSc1
z	z	k7c2
Clairvaux	Clairvaux	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
s	s	k7c7
žádostí	žádost	k1gFnSc7
o	o	k7c4
podporu	podpora	k1gFnSc4
obrátil	obrátit	k5eAaPmAgMnS
na	na	k7c4
evropské	evropský	k2eAgInPc4d1
královské	královský	k2eAgInPc4d1
dvory	dvůr	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncem	koncem	k7c2
roku	rok	k1gInSc2
1146	#num#	k4
získal	získat	k5eAaPmAgInS
i	i	k9
podporu	podpora	k1gFnSc4
římského	římský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Konráda	Konrád	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Italské	italský	k2eAgInPc1d1
městské	městský	k2eAgInPc1d1
státy	stát	k1gInPc1
se	se	k3xPyFc4
od	od	k7c2
výpravy	výprava	k1gFnSc2
distancovaly	distancovat	k5eAaBmAgFnP
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
jihoitalští	jihoitalský	k2eAgMnPc1d1
Normané	Norman	k1gMnPc1
byli	být	k5eAaImAgMnP
znepřáteleni	znepřátelen	k2eAgMnPc1d1
s	s	k7c7
papežem	papež	k1gMnSc7
i	i	k8xC
byzantským	byzantský	k2eAgMnSc7d1
císařem	císař	k1gMnSc7
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
angličtí	anglický	k2eAgMnPc1d1
i	i	k8xC
španělští	španělský	k2eAgMnPc1d1
velmoži	velmož	k1gMnPc1
byli	být	k5eAaImAgMnP
příliš	příliš	k6eAd1
zaměstnáni	zaměstnat	k5eAaPmNgMnP
svými	svůj	k3xOyFgFnPc7
vlastními	vlastní	k2eAgFnPc7d1
válkami	válka	k1gFnPc7
a	a	k8xC
k	k	k7c3
výpravě	výprava	k1gFnSc3
se	se	k3xPyFc4
nepřipojili	připojit	k5eNaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
této	tento	k3xDgFnSc6
křížové	křížový	k2eAgFnSc6d1
výpravě	výprava	k1gFnSc6
byla	být	k5eAaImAgFnS
daleko	daleko	k6eAd1
menší	malý	k2eAgFnSc1d2
účast	účast	k1gFnSc1
lidových	lidový	k2eAgMnPc2d1
kazatelů	kazatel	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
by	by	kYmCp3nP
podněcovali	podněcovat	k5eAaImAgMnP
prosté	prostý	k2eAgInPc4d1
davy	dav	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
například	například	k6eAd1
mnich	mnich	k1gMnSc1
Rudolf	Rudolf	k1gMnSc1
v	v	k7c6
Porýní	Porýní	k1gNnSc6
rozdmýchával	rozdmýchávat	k5eAaImAgMnS
protižidovské	protižidovský	k2eAgFnPc4d1
nálady	nálada	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proti	proti	k7c3
tomu	ten	k3xDgMnSc3
zasáhl	zasáhnout	k5eAaPmAgMnS
osobně	osobně	k6eAd1
Bernard	Bernard	k1gMnSc1
z	z	k7c2
Clairvaux	Clairvaux	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Koncem	koncem	k7c2
května	květen	k1gInSc2
vytáhla	vytáhnout	k5eAaPmAgFnS
z	z	k7c2
Řezna	Řezno	k1gNnSc2
Konrádova	Konrádův	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
čítající	čítající	k2eAgFnSc1d1
asi	asi	k9
20	#num#	k4
000	#num#	k4
vojáků	voják	k1gMnPc2
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
z	z	k7c2
německých	německý	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
Lotrinska	Lotrinsko	k1gNnSc2
i	i	k8xC
Čech	Čechy	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červnu	červen	k1gInSc6
vyrazilo	vyrazit	k5eAaPmAgNnS
z	z	k7c2
Met	Mety	k1gFnPc2
na	na	k7c4
pochod	pochod	k1gInSc4
i	i	k8xC
menší	malý	k2eAgNnSc4d2
francouzské	francouzský	k2eAgNnSc4d1
vojsko	vojsko	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
Anglie	Anglie	k1gFnSc2
vyplula	vyplout	k5eAaPmAgFnS
třetí	třetí	k4xOgFnSc1
skupina	skupina	k1gFnSc1
křižáků	křižák	k1gMnPc2
na	na	k7c6
jaře	jaro	k1gNnSc6
1147	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byli	být	k5eAaImAgMnP
to	ten	k3xDgNnSc1
neurození	urozený	k2eNgMnPc1d1
bojovníci	bojovník	k1gMnPc1
a	a	k8xC
námořníci	námořník	k1gMnPc1
z	z	k7c2
Anglie	Anglie	k1gFnSc2
i	i	k8xC
Flander	Flandry	k1gInPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
po	po	k7c6
cestě	cesta	k1gFnSc6
zakotvili	zakotvit	k5eAaPmAgMnP
v	v	k7c6
Portugalsku	Portugalsko	k1gNnSc6
a	a	k8xC
pomohli	pomoct	k5eAaPmAgMnP
portugalskému	portugalský	k2eAgMnSc3d1
králi	král	k1gMnSc3
dobýt	dobýt	k5eAaPmF
Lisabon	Lisabon	k1gInSc4
(	(	kIx(
<g/>
dodnes	dodnes	k6eAd1
se	se	k3xPyFc4
zachoval	zachovat	k5eAaPmAgInS
unikátní	unikátní	k2eAgInSc1d1
Dopis	dopis	k1gInSc1
křižáka	křižák	k1gMnSc2
<g/>
,	,	kIx,
podrobně	podrobně	k6eAd1
popisující	popisující	k2eAgInSc1d1
průběh	průběh	k1gInSc1
dobývání	dobývání	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vůdcové	vůdce	k1gMnPc1
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
Ludvík	Ludvík	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Konrád	Konrád	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
a	a	k8xC
Balduin	Balduin	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeruzalémský	jeruzalémský	k2eAgInSc1d1
</s>
<s>
V	v	k7c6
září	září	k1gNnSc6
1147	#num#	k4
dorazili	dorazit	k5eAaPmAgMnP
křižáci	křižák	k1gMnPc1
do	do	k7c2
Konstantinopole	Konstantinopol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vztahy	vztah	k1gInPc4
mezi	mezi	k7c7
armádami	armáda	k1gFnPc7
i	i	k8xC
panovníky	panovník	k1gMnPc7
byly	být	k5eAaImAgFnP
napjaté	napjatý	k2eAgFnPc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
Němci	Němec	k1gMnPc1
se	se	k3xPyFc4
přepravili	přepravit	k5eAaPmAgMnP
na	na	k7c4
asijskou	asijský	k2eAgFnSc4d1
pevninu	pevnina	k1gFnSc4
a	a	k8xC
od	od	k7c2
Nikáje	Nikáj	k1gFnSc2
postupovali	postupovat	k5eAaImAgMnP
dále	daleko	k6eAd2
na	na	k7c4
východ	východ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konrád	Konrád	k1gMnSc1
rozdělil	rozdělit	k5eAaPmAgMnS
své	svůj	k3xOyFgFnPc4
síly	síla	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Méně	málo	k6eAd2
ozbrojená	ozbrojený	k2eAgFnSc1d1
část	část	k1gFnSc1
armády	armáda	k1gFnSc2
pod	pod	k7c7
velením	velení	k1gNnSc7
biskupa	biskup	k1gMnSc2
Oty	Ota	k1gMnSc2
z	z	k7c2
Freisingu	Freising	k1gInSc2
táhla	táhlo	k1gNnSc2
podél	podél	k7c2
pobřeží	pobřeží	k1gNnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
král	král	k1gMnSc1
s	s	k7c7
rytíři	rytíř	k1gMnPc7
se	se	k3xPyFc4
vydal	vydat	k5eAaPmAgInS
vnitrozemím	vnitrozemí	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
U	u	k7c2
Dorylaea	Dorylae	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
kdysi	kdysi	k6eAd1
vojska	vojsko	k1gNnSc2
první	první	k4xOgFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
zvítězila	zvítězit	k5eAaPmAgFnS
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
německá	německý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
přepadena	přepaden	k2eAgFnSc1d1
seldžuckými	seldžucký	k2eAgMnPc7d1
Turky	Turek	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
výpravy	výprava	k1gFnSc2
byla	být	k5eAaImAgFnS
vyhlazena	vyhlazen	k2eAgFnSc1d1
a	a	k8xC
králi	král	k1gMnSc3
se	se	k3xPyFc4
s	s	k7c7
desetinou	desetina	k1gFnSc7
svých	svůj	k3xOyFgInPc2
původních	původní	k2eAgInPc2d1
oddílů	oddíl	k1gInPc2
podařilo	podařit	k5eAaPmAgNnS
probít	probít	k5eAaPmF
zpět	zpět	k6eAd1
do	do	k7c2
Nikáje	Nikáj	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
Druhá	druhý	k4xOgFnSc1
část	část	k1gFnSc1
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
vedená	vedený	k2eAgFnSc1d1
biskupem	biskup	k1gInSc7
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
napadena	napaden	k2eAgFnSc1d1
a	a	k8xC
téměř	téměř	k6eAd1
pobita	pobít	k5eAaPmNgFnS
Turky	Turek	k1gMnPc7
u	u	k7c2
Laodikeje	Laodikej	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbylá	zbylý	k2eAgFnSc1d1
hrstka	hrstka	k1gFnSc1
vojáků	voják	k1gMnPc2
s	s	k7c7
biskupem	biskup	k1gMnSc7
Otou	Ota	k1gMnSc7
pronikla	proniknout	k5eAaPmAgFnS
do	do	k7c2
Sýrie	Sýrie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zbytky	zbytek	k1gInPc1
německé	německý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
se	se	k3xPyFc4
v	v	k7c6
Nikáji	Nikáj	k1gFnSc6
spojily	spojit	k5eAaPmAgFnP
s	s	k7c7
francouzským	francouzský	k2eAgNnSc7d1
vojskem	vojsko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzi	Francouz	k1gMnPc1
byli	být	k5eAaImAgMnP
ukázněnější	ukázněný	k2eAgMnPc1d2
<g/>
,	,	kIx,
Ludvík	Ludvík	k1gMnSc1
byl	být	k5eAaImAgMnS
však	však	k9
jako	jako	k9
vojevůdce	vojevůdce	k1gMnSc1
stejně	stejně	k6eAd1
špatný	špatný	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
Konrád	Konrád	k1gMnSc1
a	a	k8xC
navíc	navíc	k6eAd1
nerozhodný	rozhodný	k2eNgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
Spojená	spojený	k2eAgFnSc1d1
křižácká	křižácký	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
táhla	táhnout	k5eAaImAgFnS
podél	podél	k7c2
pobřeží	pobřeží	k1gNnSc2
na	na	k7c4
východ	východ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Efesu	Efes	k1gInSc6
král	král	k1gMnSc1
Konrád	Konrád	k1gMnSc1
onemocněl	onemocnět	k5eAaPmAgMnS
a	a	k8xC
vrátil	vrátit	k5eAaPmAgMnS
se	se	k3xPyFc4
do	do	k7c2
Konstantinopole	Konstantinopol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
jeho	jeho	k3xOp3gMnPc2
vojáků	voják	k1gMnPc2
táhla	táhnout	k5eAaImAgFnS
dále	daleko	k6eAd2
s	s	k7c7
Francouzi	Francouz	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
pisidijské	pisidijský	k2eAgFnSc2d1
Antiochie	Antiochie	k1gFnSc2
(	(	kIx(
<g/>
říkalo	říkat	k5eAaImAgNnS
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
též	též	k9
Antiochetta	Antiochetto	k1gNnSc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
křižáci	křižák	k1gMnPc1
střetli	střetnout	k5eAaPmAgMnP
s	s	k7c7
Turky	turek	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yQgInPc4,k3yRgInPc4
donutili	donutit	k5eAaPmAgMnP
k	k	k7c3
ústupu	ústup	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
křižácká	křižácký	k2eAgNnPc1d1
vojska	vojsko	k1gNnPc1
překročila	překročit	k5eAaPmAgNnP
pod	pod	k7c7
neustálou	neustálý	k2eAgFnSc7d1
tureckou	turecký	k2eAgFnSc7d1
hrozbou	hrozba	k1gFnSc7
Taurus	Taurus	k1gInSc1
a	a	k8xC
v	v	k7c6
únoru	únor	k1gInSc6
1148	#num#	k4
dorazila	dorazit	k5eAaPmAgFnS
k	k	k7c3
řeckému	řecký	k2eAgNnSc3d1
městu	město	k1gNnSc3
Attaleia	Attaleium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
výpravy	výprava	k1gFnSc2
odplula	odplout	k5eAaPmAgFnS
a	a	k8xC
armáda	armáda	k1gFnSc1
pěšáků	pěšák	k1gMnPc2
a	a	k8xC
poutníků	poutník	k1gMnPc2
zůstala	zůstat	k5eAaPmAgFnS
na	na	k7c4
pospas	pospas	k1gInSc4
osudu	osud	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Ludvík	Ludvík	k1gMnSc1
Francouzský	francouzský	k2eAgMnSc1d1
byl	být	k5eAaImAgInS
po	po	k7c6
vylodění	vylodění	k1gNnSc6
v	v	k7c6
Antiochiii	Antiochiie	k1gFnSc6
vřele	vřele	k6eAd1
přivítán	přivítat	k5eAaPmNgInS
knížetem	kníže	k1gMnSc7
Raimondem	Raimond	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navzdory	navzdory	k6eAd1
všem	všecek	k3xTgFnPc3
útrapám	útrapa	k1gFnPc3
a	a	k8xC
ztrátám	ztráta	k1gFnPc3
<g/>
,	,	kIx,
jakými	jaký	k3yIgFnPc7,k3yQgFnPc7,k3yRgFnPc7
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
doposud	doposud	k6eAd1
prošla	projít	k5eAaPmAgFnS
<g/>
,	,	kIx,
představoval	představovat	k5eAaImAgInS
Ludvíkův	Ludvíkův	k2eAgInSc1d1
kontingent	kontingent	k1gInSc1
významnou	významný	k2eAgFnSc4d1
pomoc	pomoc	k1gFnSc4
pro	pro	k7c4
křižácké	křižácký	k2eAgInPc4d1
státy	stát	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Ludvík	Ludvík	k1gMnSc1
se	se	k3xPyFc4
pak	pak	k6eAd1
vydal	vydat	k5eAaPmAgMnS
do	do	k7c2
Jeruzaléma	Jeruzalém	k1gInSc2
<g/>
,	,	kIx,
kam	kam	k6eAd1
již	již	k6eAd1
mezitím	mezitím	k6eAd1
z	z	k7c2
Konstantinopole	Konstantinopol	k1gInSc2
dorazil	dorazit	k5eAaPmAgMnS
král	král	k1gMnSc1
Konrád	Konrád	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Obléhání	obléhání	k1gNnSc1
DamaškuLes	DamaškuLesa	k1gFnPc2
Passages	Passagesa	k1gFnPc2
Faits	Faits	k1gInSc1
Outremer	Outremer	k1gMnSc1
par	para	k1gFnPc2
les	les	k1gInSc4
Français	Français	k1gFnSc4
contre	contr	k1gInSc5
les	les	k1gInSc1
Turcs	Turcs	k1gInSc1
et	et	k?
Autres	Autres	k1gInSc1
Sarazaines	Sarazaines	k1gInSc1
et	et	k?
Maures	Maures	k1gInSc1
Outremarins	Outremarinsa	k1gFnPc2
<g/>
,	,	kIx,
1490	#num#	k4
</s>
<s>
V	v	k7c6
Jeruzalémě	Jeruzalém	k1gInSc6
se	se	k3xPyFc4
oba	dva	k4xCgMnPc1
panovníci	panovník	k1gMnPc1
setkali	setkat	k5eAaPmAgMnP
i	i	k9
s	s	k7c7
jeruzalémským	jeruzalémský	k2eAgMnSc7d1
králem	král	k1gMnSc7
Balduinem	Balduin	k1gMnSc7
III	III	kA
<g/>
.	.	kIx.
a	a	k8xC
rozhodli	rozhodnout	k5eAaPmAgMnP
se	se	k3xPyFc4
dobýt	dobýt	k5eAaPmF
Damašek	Damašek	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
spojencem	spojenec	k1gMnSc7
Jeruzalémského	jeruzalémský	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
,	,	kIx,
přestože	přestože	k8xS
původním	původní	k2eAgInSc7d1
cílem	cíl	k1gInSc7
druhé	druhý	k4xOgFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
bylo	být	k5eAaImAgNnS
znovudobytí	znovudobytí	k1gNnSc1
Edessy	Edessa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Damašek	Damašek	k1gInSc1
napadli	napadnout	k5eAaPmAgMnP
24	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1148	#num#	k4
<g/>
,	,	kIx,
před	před	k7c7
dobytím	dobytí	k1gNnSc7
jej	on	k3xPp3gMnSc4
však	však	k9
zachránila	zachránit	k5eAaPmAgFnS
velká	velký	k2eAgFnSc1d1
turecká	turecký	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
z	z	k7c2
Aleppa	Alepp	k1gMnSc2
vedená	vedený	k2eAgFnSc1d1
sultánem	sultán	k1gMnSc7
Núr	Núr	k1gMnSc7
ad-Dínem	ad-Dín	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Neslavný	slavný	k2eNgInSc1d1
konec	konec	k1gInSc1
výpravy	výprava	k1gFnSc2
jen	jen	k9
prohloubil	prohloubit	k5eAaPmAgMnS
spory	spor	k1gInPc4
mezi	mezi	k7c7
francouzským	francouzský	k2eAgMnSc7d1
a	a	k8xC
římským	římský	k2eAgMnSc7d1
králem	král	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konrád	Konrád	k1gMnSc1
odjel	odjet	k5eAaPmAgMnS
již	již	k6eAd1
na	na	k7c4
Vánoce	Vánoce	k1gFnPc4
1148	#num#	k4
do	do	k7c2
Konstantinopole	Konstantinopol	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
uzavřel	uzavřít	k5eAaPmAgInS
spojenectví	spojenectví	k1gNnSc4
s	s	k7c7
císařem	císař	k1gMnSc7
Manuelem	Manuel	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ludvík	Ludvík	k1gMnSc1
pobyl	pobýt	k5eAaPmAgMnS
v	v	k7c6
Palestině	Palestina	k1gFnSc6
a	a	k8xC
při	při	k7c6
návratu	návrat	k1gInSc6
domů	dům	k1gInPc2
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
Itálii	Itálie	k1gFnSc6
navázal	navázat	k5eAaPmAgMnS
přátelské	přátelský	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
se	s	k7c7
sicilským	sicilský	k2eAgMnSc7d1
králem	král	k1gMnSc7
Rogerem	Roger	k1gMnSc7
II	II	kA
<g/>
.	.	kIx.
–	–	k?
úhlavním	úhlavní	k2eAgMnSc7d1
nepřítelem	nepřítel	k1gMnSc7
Byzance	Byzanc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Damašek	Damašek	k1gInSc1
byl	být	k5eAaImAgInS
obsazen	obsadit	k5eAaPmNgInS
Núr	Núr	k1gMnSc7
ad-Dínem	ad-Dín	k1gMnSc7
a	a	k8xC
muslimové	muslim	k1gMnPc1
tak	tak	k6eAd1
byli	být	k5eAaImAgMnP
proti	proti	k7c3
křižákům	křižák	k1gInPc3
opět	opět	k6eAd1
o	o	k7c4
něco	něco	k3yInSc4
jednotnější	jednotný	k2eAgNnSc1d2
<g/>
.	.	kIx.
</s>
<s>
Třetí	třetí	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článcích	článek	k1gInPc6
Obléhání	obléhání	k1gNnSc2
Jeruzaléma	Jeruzalém	k1gInSc2
(	(	kIx(
<g/>
1187	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Třetí	třetí	k4xOgFnSc4
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
katastrofické	katastrofický	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
u	u	k7c2
Hattínu	Hattín	k1gInSc2
4	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1187	#num#	k4
<g/>
,	,	kIx,
dobytí	dobytí	k1gNnSc4
Jeruzaléma	Jeruzalém	k1gInSc2
na	na	k7c4
podzim	podzim	k1gInSc4
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
a	a	k8xC
následné	následný	k2eAgFnSc6d1
ztrátě	ztráta	k1gFnSc6
téměř	téměř	k6eAd1
celého	celý	k2eAgNnSc2d1
území	území	k1gNnSc2
Jeruzalémského	jeruzalémský	k2eAgNnSc2d1
království	království	k1gNnSc2
ve	v	k7c4
prospěch	prospěch	k1gInSc4
sultána	sultána	k1gFnSc1
Saladina	Saladina	k1gFnSc1
vyhlásil	vyhlásit	k5eAaPmAgMnS
papež	papež	k1gMnSc1
Řehoř	Řehoř	k1gMnSc1
VIII	VIII	kA
<g/>
.	.	kIx.
bulou	bula	k1gFnSc7
Audita	Audit	k1gMnSc2
tremendi	tremend	k1gMnPc1
z	z	k7c2
29	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1187	#num#	k4
třetí	třetí	k4xOgFnSc4
křížovou	křížový	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
Vyslancům	vyslanec	k1gMnPc3
kurie	kurie	k1gFnPc4
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
dokonce	dokonce	k9
zprostředkovat	zprostředkovat	k5eAaPmF
mír	mír	k1gInSc4
mezi	mezi	k7c7
francouzským	francouzský	k2eAgMnSc7d1
králem	král	k1gMnSc7
Filipem	Filip	k1gMnSc7
Augustem	August	k1gMnSc7
a	a	k8xC
anglickým	anglický	k2eAgMnSc7d1
králem	král	k1gMnSc7
Richardem	Richard	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svou	svůj	k3xOyFgFnSc4
účast	účast	k1gFnSc4
na	na	k7c6
výpravě	výprava	k1gFnSc6
přislíbil	přislíbit	k5eAaPmAgMnS
také	také	k9
římský	římský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
Barbarossa	Barbarossa	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
S	s	k7c7
císařem	císař	k1gMnSc7
se	se	k3xPyFc4
na	na	k7c4
křížovou	křížový	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
vypravil	vypravit	k5eAaPmAgInS
také	také	k9
silný	silný	k2eAgInSc1d1
oddíl	oddíl	k1gInSc1
Čechů	Čech	k1gMnPc2
pod	pod	k7c7
vedením	vedení	k1gNnSc7
mladého	mladý	k2eAgMnSc2d1
Přemyslovce	Přemyslovec	k1gMnSc2
Děpolta	Děpolt	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
při	při	k7c6
tažení	tažení	k1gNnSc6
zahynul	zahynout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Králové	Králové	k2eAgMnSc1d1
Richard	Richard	k1gMnSc1
Lví	lví	k2eAgNnSc4d1
srdce	srdce	k1gNnSc4
a	a	k8xC
Filip	Filip	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
přijímají	přijímat	k5eAaImIp3nP
kapitulaci	kapitulace	k1gFnSc4
Akkonu	Akkon	k1gInSc2
<g/>
(	(	kIx(
<g/>
Grandes	Grandes	k1gInSc1
Chroniques	Chroniques	k1gInSc1
de	de	k?
France	Franc	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
Jako	jako	k9
první	první	k4xOgMnSc1
se	se	k3xPyFc4
do	do	k7c2
Svaté	svatá	k1gFnSc2
země	zem	k1gFnPc1
vydali	vydat	k5eAaPmAgMnP
sicilští	sicilský	k2eAgMnPc1d1
Normané	Norman	k1gMnPc1
a	a	k8xC
vzápětí	vzápětí	k6eAd1
i	i	k9
menší	malý	k2eAgInPc1d2
námořní	námořní	k2eAgInPc1d1
kontingenty	kontingent	k1gInPc1
z	z	k7c2
Anglie	Anglie	k1gFnSc2
<g/>
,	,	kIx,
Dánska	Dánsko	k1gNnSc2
a	a	k8xC
Flander	Flandry	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
Fridrich	Fridrich	k1gMnSc1
I.	I.	kA
Barbarossa	Barbarossa	k1gMnSc1
se	se	k3xPyFc4
vydal	vydat	k5eAaPmAgMnS
na	na	k7c4
výpravu	výprava	k1gFnSc4
z	z	k7c2
Řezna	Řezno	k1gNnSc2
na	na	k7c6
jaře	jaro	k1gNnSc6
1188	#num#	k4
a	a	k8xC
po	po	k7c6
cestě	cesta	k1gFnSc6
začal	začít	k5eAaPmAgInS
na	na	k7c6
byzantském	byzantský	k2eAgNnSc6d1
území	území	k1gNnSc6
drancovat	drancovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Císař	Císař	k1gMnSc1
Izák	Izák	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
kvůli	kvůli	k7c3
možným	možný	k2eAgFnPc3d1
násilnostem	násilnost	k1gFnPc3
raději	rád	k6eAd2
povolil	povolit	k5eAaPmAgInS
cestu	cesta	k1gFnSc4
dále	daleko	k6eAd2
na	na	k7c4
východ	východ	k1gInSc4
přes	přes	k7c4
Helespont	Helespont	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
Němci	Němec	k1gMnPc1
přezimovali	přezimovat	k5eAaBmAgMnP
v	v	k7c6
Adrianopoli	Adrianopole	k1gFnSc6
a	a	k8xC
na	na	k7c6
jaře	jaro	k1gNnSc6
1190	#num#	k4
se	se	k3xPyFc4
opět	opět	k6eAd1
vydali	vydat	k5eAaPmAgMnP
na	na	k7c4
cestu	cesta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
Ikonia	Ikonium	k1gNnSc2
je	on	k3xPp3gMnPc4
napadli	napadnout	k5eAaPmAgMnP
seldžučtí	seldžucký	k2eAgMnPc1d1
Turci	Turek	k1gMnPc1
sultána	sultán	k1gMnSc2
Kilič	Kilič	k1gMnSc1
Arslana	Arslan	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
byli	být	k5eAaImAgMnP
Barbarossou	Barbarossa	k1gMnSc7
poraženi	porazit	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Ikonie	Ikonie	k1gFnSc2
i	i	k9
v	v	k7c6
dalších	další	k2eAgFnPc6d1
srážkách	srážka	k1gFnPc6
s	s	k7c7
Turky	Turek	k1gMnPc7
se	se	k3xPyFc4
vyznamenali	vyznamenat	k5eAaPmAgMnP
udatností	udatnost	k1gFnPc2
Češi	Čech	k1gMnPc1
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
Děpoltem	Děpolt	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
Při	při	k7c6
přechodu	přechod	k1gInSc6
řeky	řeka	k1gFnSc2
Salef	Salef	k1gMnSc1
však	však	k8xC
Fridrich	Fridrich	k1gMnSc1
I.	I.	kA
utonul	utonout	k5eAaPmAgMnS
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
výprava	výprava	k1gFnSc1
se	se	k3xPyFc4
rozpadla	rozpadnout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
bojovníků	bojovník	k1gMnPc2
pokračovala	pokračovat	k5eAaImAgFnS
dále	daleko	k6eAd2
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
a	a	k8xC
část	část	k1gFnSc1
se	se	k3xPyFc4
vrátila	vrátit	k5eAaPmAgFnS
domů	domů	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Římská	římský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
se	se	k3xPyFc4
tak	tak	k6eAd1
výpravy	výprava	k1gFnSc2
významněji	významně	k6eAd2
nezúčastnila	zúčastnit	k5eNaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1190	#num#	k4
vyrazila	vyrazit	k5eAaPmAgFnS
z	z	k7c2
Vézélay	Vézélaa	k1gFnSc2
francouzská	francouzský	k2eAgNnPc1d1
i	i	k8xC
anglická	anglický	k2eAgNnPc1d1
vojska	vojsko	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Richarda	Richard	k1gMnSc2
I.	I.	kA
a	a	k8xC
Filipa	Filip	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
přezimovala	přezimovat	k5eAaBmAgFnS
na	na	k7c6
Sicílii	Sicílie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jaře	jaro	k1gNnSc6
1191	#num#	k4
křižáci	křižák	k1gMnPc1
vypluli	vyplout	k5eAaPmAgMnP
vstříc	vstříc	k6eAd1
Svaté	svatý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
a	a	k8xC
12	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
dobyli	dobýt	k5eAaPmAgMnP
město	město	k1gNnSc4
Akkon	Akkon	k1gMnSc1
a	a	k8xC
francouzský	francouzský	k2eAgMnSc1d1
král	král	k1gMnSc1
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
zpět	zpět	k6eAd1
do	do	k7c2
vlasti	vlast	k1gFnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Angličané	Angličan	k1gMnPc1
a	a	k8xC
zbytek	zbytek	k1gInSc1
francouzských	francouzský	k2eAgMnPc2d1
křižáků	křižák	k1gMnPc2
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
králem	král	k1gMnSc7
Richardem	Richard	k1gMnSc7
bojovali	bojovat	k5eAaImAgMnP
se	s	k7c7
Saladinem	Saladino	k1gNnSc7
ještě	ještě	k9
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velkým	velký	k2eAgInSc7d1
křižáckým	křižácký	k2eAgInSc7d1
úspěchem	úspěch	k1gInSc7
byla	být	k5eAaImAgFnS
bitva	bitva	k1gFnSc1
u	u	k7c2
Arsufu	Arsuf	k1gInSc2
a	a	k8xC
obsazení	obsazení	k1gNnSc2
Jaffy	Jaffa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
marném	marný	k2eAgInSc6d1
pokusu	pokus	k1gInSc6
o	o	k7c4
dobytí	dobytí	k1gNnSc4
Jeruzaléma	Jeruzalém	k1gInSc2
Saladin	Saladina	k1gFnPc2
předložil	předložit	k5eAaPmAgMnS
Richardovi	Richard	k1gMnSc3
mírový	mírový	k2eAgInSc4d1
návrh	návrh	k1gInSc4
<g/>
,	,	kIx,
podle	podle	k7c2
něhož	jenž	k3xRgMnSc2
zpřístupní	zpřístupnit	k5eAaPmIp3nS
Boží	boží	k2eAgInSc1d1
hrob	hrob	k1gInSc1
západním	západní	k2eAgMnPc3d1
poutníkům	poutník	k1gMnPc3
a	a	k8xC
uzná	uznat	k5eAaPmIp3nS
hranice	hranice	k1gFnSc1
stávajících	stávající	k2eAgFnPc2d1
křižáckých	křižácký	k2eAgFnPc2d1
držav	država	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
půlročním	půlroční	k2eAgNnSc6d1
váhání	váhání	k1gNnSc6
plném	plný	k2eAgNnSc6d1
šarvátek	šarvátka	k1gFnPc2
byl	být	k5eAaImAgInS
mír	mír	k1gInSc1
2	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
potvrzen	potvrdit	k5eAaPmNgMnS
a	a	k8xC
Richard	Richard	k1gMnSc1
se	se	k3xPyFc4
vydal	vydat	k5eAaPmAgMnS
na	na	k7c4
cestu	cesta	k1gFnSc4
domů	dům	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Čtvrtá	čtvrtý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
</s>
<s>
Palma	palma	k1gFnSc1
Le	Le	k1gMnSc5
Jeune	Jeun	k1gMnSc5
<g/>
:	:	kIx,
Útok	útok	k1gInSc1
křižáků	křižák	k1gMnPc2
na	na	k7c4
Konstantinopol	Konstantinopol	k1gInSc4
<g/>
(	(	kIx(
<g/>
Les	les	k1gInSc4
croisades	croisadesa	k1gFnPc2
<g/>
,	,	kIx,
origines	originesa	k1gFnPc2
et	et	k?
consequences	consequences	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článcích	článek	k1gInPc6
Čtvrtá	čtvrtá	k1gFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
a	a	k8xC
Latinské	latinský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
přelomu	přelom	k1gInSc6
let	léto	k1gNnPc2
1199	#num#	k4
<g/>
–	–	k?
<g/>
1200	#num#	k4
papež	papež	k1gMnSc1
Inocenc	Inocenc	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
požehnal	požehnat	k5eAaPmAgMnS
snaze	snaha	k1gFnSc3
francouzských	francouzský	k2eAgMnPc2d1
velmožů	velmož	k1gMnPc2
uspořádat	uspořádat	k5eAaPmF
tažení	tažení	k1gNnSc4
na	na	k7c4
Blízký	blízký	k2eAgInSc4d1
východ	východ	k1gInSc4
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
zároveň	zároveň	k6eAd1
zdůraznil	zdůraznit	k5eAaPmAgMnS
dosavadní	dosavadní	k2eAgFnSc4d1
neschopnost	neschopnost	k1gFnSc4
evropských	evropský	k2eAgMnPc2d1
králů	král	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
Výprava	výprava	k1gFnSc1
<g/>
,	,	kIx,
organizovaná	organizovaný	k2eAgFnSc1d1
výborem	výbor	k1gInSc7
navzájem	navzájem	k6eAd1
si	se	k3xPyFc3
rovných	rovný	k2eAgMnPc2d1
rytířů	rytíř	k1gMnPc2
a	a	k8xC
vedená	vedený	k2eAgFnSc1d1
hrabětem	hrabě	k1gMnSc7
Theobaldem	Theobald	k1gMnSc7
ze	z	k7c2
Champagne	Champagn	k1gInSc5
<g/>
,	,	kIx,
postupovala	postupovat	k5eAaImAgNnP
do	do	k7c2
severní	severní	k2eAgFnSc2d1
Itálie	Itálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
si	se	k3xPyFc3
na	na	k7c4
úvěr	úvěr	k1gInSc4
najala	najmout	k5eAaPmAgFnS
lodě	loď	k1gFnPc1
u	u	k7c2
Benátčanů	Benátčan	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
si	se	k3xPyFc3
tak	tak	k6eAd1
zajistili	zajistit	k5eAaPmAgMnP
vliv	vliv	k1gInSc4
na	na	k7c4
výpravu	výprava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
jaře	jaro	k1gNnSc6
roku	rok	k1gInSc2
1201	#num#	k4
zemřel	zemřít	k5eAaPmAgMnS
hrabě	hrabě	k1gMnSc1
Theobald	Theobald	k1gMnSc1
a	a	k8xC
vedení	vedení	k1gNnSc4
převzal	převzít	k5eAaPmAgMnS
Bonifác	Bonifác	k1gMnSc1
z	z	k7c2
Montferratu	Montferrat	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
výpravy	výprava	k1gFnSc2
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
Egypt	Egypt	k1gInSc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
nelíbilo	líbit	k5eNaImAgNnS
Benátčanům	Benátčan	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
tam	tam	k6eAd1
měli	mít	k5eAaImAgMnP
obchodní	obchodní	k2eAgInPc4d1
zájmy	zájem	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
Požadovali	požadovat	k5eAaImAgMnP
proto	proto	k8xC
od	od	k7c2
vůdců	vůdce	k1gMnPc2
výpravy	výprava	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
křižáci	křižák	k1gMnPc1
pro	pro	k7c4
Benátky	Benátky	k1gFnPc4
vybojovali	vybojovat	k5eAaPmAgMnP
od	od	k7c2
Uherska	Uhersko	k1gNnSc2
město	město	k1gNnSc1
Zadar	Zadar	k1gInSc1
<g/>
,	,	kIx,
za	za	k7c4
což	což	k3yQnSc4,k3yRnSc4
jim	on	k3xPp3gMnPc3
budou	být	k5eAaImBp3nP
odloženy	odložen	k2eAgFnPc1d1
splátky	splátka	k1gFnPc1
dluhů	dluh	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
Útok	útok	k1gInSc1
na	na	k7c4
katolický	katolický	k2eAgInSc4d1
Zadar	Zadar	k1gInSc4
byl	být	k5eAaImAgInS
pod	pod	k7c7
trestem	trest	k1gInSc7
exkomunikace	exkomunikace	k1gFnSc2
papežem	papež	k1gMnSc7
zakázán	zakázat	k5eAaPmNgMnS
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
křižáci	křižák	k1gMnPc1
přesto	přesto	k8xC
nátlaku	nátlak	k1gInSc3
benátského	benátský	k2eAgNnSc2d1
dóžete	dóže	k1gNnSc2wR
ustoupili	ustoupit	k5eAaPmAgMnP
a	a	k8xC
v	v	k7c6
listopadu	listopad	k1gInSc6
1202	#num#	k4
Zadar	Zadar	k1gInSc4
dobyli	dobýt	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Křižácké	křižácký	k2eAgInPc4d1
státy	stát	k1gInPc4
a	a	k8xC
řecké	řecký	k2eAgInPc4d1
nástupnickými	nástupnický	k2eAgInPc7d1
státy	stát	k1gInPc7
v	v	k7c6
době	doba	k1gFnSc6
po	po	k7c6
čtvrté	čtvrtý	k4xOgFnSc6
křížové	křížový	k2eAgFnSc6d1
výpravě	výprava	k1gFnSc6
v	v	k7c6
Řecku	Řecko	k1gNnSc6
</s>
<s>
Krátce	krátce	k6eAd1
poté	poté	k6eAd1
křižáky	křižák	k1gMnPc4
požádal	požádat	k5eAaPmAgMnS
římský	římský	k2eAgMnSc1d1
král	král	k1gMnSc1
Filip	Filip	k1gMnSc1
Švábský	švábský	k2eAgInSc4d1
o	o	k7c4
pomoc	pomoc	k1gFnSc4
při	při	k7c6
dosazení	dosazení	k1gNnSc6
svého	své	k1gNnSc2
švagra	švagr	k1gMnSc2
Alexia	Alexius	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
na	na	k7c4
byzantský	byzantský	k2eAgInSc4d1
trůn	trůn	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alexios	Alexios	k1gInSc1
křižákům	křižák	k1gMnPc3
nabízel	nabízet	k5eAaImAgInS
uznání	uznání	k1gNnSc4
papežovy	papežův	k2eAgFnSc2d1
autority	autorita	k1gFnSc2
nad	nad	k7c7
východní	východní	k2eAgFnSc7d1
církví	církev	k1gFnSc7
<g/>
,	,	kIx,
tolik	tolik	k6eAd1
potřebné	potřebný	k2eAgInPc4d1
peníze	peníz	k1gInPc4
<g/>
,	,	kIx,
zásoby	zásoba	k1gFnPc4
a	a	k8xC
vojenskou	vojenský	k2eAgFnSc4d1
pomoc	pomoc	k1gFnSc4
na	na	k7c6
křížové	křížový	k2eAgFnSc6d1
výpravě	výprava	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
červenci	červenec	k1gInSc6
1203	#num#	k4
proto	proto	k8xC
křižáci	křižák	k1gMnPc1
zaútočili	zaútočit	k5eAaPmAgMnP
na	na	k7c4
další	další	k2eAgNnSc4d1
město	město	k1gNnSc4
a	a	k8xC
to	ten	k3xDgNnSc1
samotné	samotný	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
celé	celý	k2eAgFnSc2d1
Byzantské	byzantský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
Konstantinopol	Konstantinopol	k1gInSc1
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
nakonec	nakonec	k6eAd1
po	po	k7c6
dlouhém	dlouhý	k2eAgInSc6d1
boji	boj	k1gInSc6
a	a	k8xC
nerozhodnosti	nerozhodnost	k1gFnSc6
byzantského	byzantský	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
dobyli	dobýt	k5eAaPmAgMnP
a	a	k8xC
brutálně	brutálně	k6eAd1
vyplenili	vyplenit	k5eAaPmAgMnP
a	a	k8xC
na	na	k7c4
trůn	trůn	k1gInSc4
dosadili	dosadit	k5eAaPmAgMnP
prince	princ	k1gMnSc2
Alexia	Alexius	k1gMnSc2
a	a	k8xC
jeho	on	k3xPp3gMnSc2,k3xOp3gMnSc2
otce	otec	k1gMnSc2
Izáka	Izák	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těm	ten	k3xDgMnPc3
se	se	k3xPyFc4
však	však	k8xC
sliby	slib	k1gInPc1
dané	daný	k2eAgInPc1d1
křižákům	křižák	k1gMnPc3
nepodařilo	podařit	k5eNaPmAgNnS
splnit	splnit	k5eAaPmF
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
enormním	enormní	k2eAgInPc3d1
nárokům	nárok	k1gInPc3
křižáků	křižák	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obyvatelstvo	obyvatelstvo	k1gNnSc1
je	být	k5eAaImIp3nS
brzy	brzy	k6eAd1
svrhlo	svrhnout	k5eAaPmAgNnS
a	a	k8xC
zabilo	zabít	k5eAaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
Křižáci	křižák	k1gMnPc1
na	na	k7c4
město	město	k1gNnSc4
zaútočili	zaútočit	k5eAaPmAgMnP
znovu	znovu	k6eAd1
a	a	k8xC
12	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1204	#num#	k4
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
podařilo	podařit	k5eAaPmAgNnS
byzantskou	byzantský	k2eAgFnSc4d1
obranu	obrana	k1gFnSc4
zlomit	zlomit	k5eAaPmF
a	a	k8xC
vniknout	vniknout	k5eAaPmF
do	do	k7c2
města	město	k1gNnSc2
<g/>
;	;	kIx,
následovalo	následovat	k5eAaImAgNnS
třídenní	třídenní	k2eAgNnSc1d1
drancování	drancování	k1gNnSc1
a	a	k8xC
vraždění	vraždění	k1gNnSc1
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
Čtvrtá	čtvrtý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
již	již	k6eAd1
do	do	k7c2
Egypta	Egypt	k1gInSc2
ani	ani	k8xC
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
nepokračovala	pokračovat	k5eNaImAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Novým	nový	k2eAgMnSc7d1
císařem	císař	k1gMnSc7
Latinského	latinský	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
vzniklého	vzniklý	k2eAgNnSc2d1
na	na	k7c6
troskách	troska	k1gFnPc6
Byzance	Byzanc	k1gFnSc2
byl	být	k5eAaImAgInS
zvolen	zvolit	k5eAaPmNgMnS
flanderský	flanderský	k2eAgMnSc1d1
hrabě	hrabě	k1gMnSc1
Balduin	Balduin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nově	nově	k6eAd1
vzniklé	vzniklý	k2eAgNnSc4d1
Soluňské	soluňský	k2eAgNnSc4d1
království	království	k1gNnSc4
získal	získat	k5eAaPmAgMnS
Bonifác	Bonifác	k1gMnSc1
z	z	k7c2
Montferratu	Montferrat	k1gInSc2
a	a	k8xC
Achajské	Achajský	k2eAgNnSc1d1
knížectví	knížectví	k1gNnSc1
a	a	k8xC
Athénské	athénský	k2eAgNnSc1d1
vévodství	vévodství	k1gNnSc1
připadlo	připadnout	k5eAaPmAgNnS
francouzským	francouzský	k2eAgMnSc7d1
velmožům	velmož	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Benátčané	Benátčan	k1gMnPc1
získali	získat	k5eAaPmAgMnP
obchodní	obchodní	k2eAgInSc4d1
monopol	monopol	k1gInSc4
a	a	k8xC
Naxos	Naxos	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byzantská	byzantský	k2eAgFnSc1d1
císařská	císařský	k2eAgFnSc1d1
rodina	rodina	k1gFnSc1
si	se	k3xPyFc3
zachovala	zachovat	k5eAaPmAgFnS
část	část	k1gFnSc1
svých	svůj	k3xOyFgFnPc2
držav	država	k1gFnPc2
na	na	k7c6
východě	východ	k1gInSc6
a	a	k8xC
založila	založit	k5eAaPmAgFnS
zde	zde	k6eAd1
Nikájské	Nikájský	k2eAgNnSc4d1
císařství	císařství	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
hlavním	hlavní	k2eAgMnSc7d1
nepřítelem	nepřítel	k1gMnSc7
křižáků	křižák	k1gMnPc2
v	v	k7c6
Konstantinopoli	Konstantinopol	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
Další	další	k2eAgMnPc1d1
členové	člen	k1gMnPc1
rodu	rod	k1gInSc2
Komnenů	Komnen	k1gMnPc2
založili	založit	k5eAaPmAgMnP
na	na	k7c6
pobřeží	pobřeží	k1gNnSc6
Černého	Černého	k2eAgNnSc2d1
moře	moře	k1gNnSc2
Trapezuntské	trapezuntský	k2eAgNnSc4d1
císařství	císařství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posledním	poslední	k2eAgInSc7d1
řeckým	řecký	k2eAgInSc7d1
státem	stát	k1gInSc7
byl	být	k5eAaImAgInS
Epirský	Epirský	k2eAgInSc1d1
despotát	despotát	k1gInSc1
ležící	ležící	k2eAgInSc1d1
v	v	k7c6
Epiru	Epir	k1gInSc6
na	na	k7c6
pobřeží	pobřeží	k1gNnSc6
Jónského	jónský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc1
tyto	tento	k3xDgInPc1
státy	stát	k1gInPc1
se	se	k3xPyFc4
považovaly	považovat	k5eAaImAgInP
za	za	k7c4
legitimní	legitimní	k2eAgFnSc4d1
nástupce	nástupce	k1gMnSc1
Byzantské	byzantský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
a	a	k8xC
vedly	vést	k5eAaImAgFnP
válku	válka	k1gFnSc4
proti	proti	k7c3
latinským	latinský	k2eAgInPc3d1
křižáckým	křižácký	k2eAgInPc3d1
státům	stát	k1gInPc3
<g/>
.	.	kIx.
</s>
<s>
Legenda	legenda	k1gFnSc1
o	o	k7c6
výpravě	výprava	k1gFnSc6
dětí	dítě	k1gFnPc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Dětské	dětský	k2eAgFnSc2d1
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Gustave	Gustav	k1gMnSc5
Doré	Dorý	k2eAgFnSc3d1
<g/>
:	:	kIx,
Křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
dětí	dítě	k1gFnPc2
</s>
<s>
Prameny	pramen	k1gInPc1
hovoří	hovořit	k5eAaImIp3nP
také	také	k9
o	o	k7c6
údajné	údajný	k2eAgFnSc6d1
francouzské	francouzský	k2eAgFnSc3d1
a	a	k8xC
německé	německý	k2eAgFnSc3d1
křížové	křížový	k2eAgFnSc3d1
výpravě	výprava	k1gFnSc3
dětí	dítě	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
zorganizované	zorganizovaný	k2eAgNnSc1d1
prý	prý	k9
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
věřilo	věřit	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
neúspěchy	neúspěch	k1gInPc1
dosavadních	dosavadní	k2eAgNnPc2d1
tažení	tažení	k1gNnSc2
zapříčiňuje	zapříčiňovat	k5eAaImIp3nS
hříšnost	hříšnost	k1gFnSc1
jejich	jejich	k3xOp3gMnPc2
dospělých	dospělý	k2eAgMnPc2d1
účastníků	účastník	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
Původcem	původce	k1gMnSc7
měl	mít	k5eAaImAgMnS
být	být	k5eAaImF
jakýsi	jakýsi	k3yIgInSc4
francouzský	francouzský	k2eAgInSc4d1
<g />
.	.	kIx.
</s>
<s hack="1">
pasáček	pasáček	k1gMnSc1
Štěpán	Štěpán	k1gMnSc1
<g/>
,	,	kIx,
jemuž	jenž	k3xRgMnSc3
se	se	k3xPyFc4
prý	prý	k9
roku	rok	k1gInSc2
1212	#num#	k4
zjevil	zjevit	k5eAaPmAgMnS
Kristus	Kristus	k1gMnSc1
a	a	k8xC
vyzval	vyzvat	k5eAaPmAgInS
ho	on	k3xPp3gMnSc4
ke	k	k7c3
křížové	křížový	k2eAgFnSc3d1
výpravě	výprava	k1gFnSc3
proti	proti	k7c3
nevěřícím	nevěřící	k1gFnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
Štěpán	Štěpán	k1gMnSc1
proto	proto	k8xC
věřil	věřit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
moře	moře	k1gNnSc1
před	před	k7c7
ním	on	k3xPp3gInSc7
rozestoupí	rozestoupit	k5eAaPmIp3nP
a	a	k8xC
cesta	cesta	k1gFnSc1
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
bude	být	k5eAaImBp3nS
možná	možná	k6eAd1
suchou	suchý	k2eAgFnSc7d1
nohou	noha	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
strastiplné	strastiplný	k2eAgFnSc6d1
cestě	cesta	k1gFnSc6
prý	prý	k9
několik	několik	k4yIc4
tisíc	tisíc	k4xCgInPc2
dětí	dítě	k1gFnPc2
dorazilo	dorazit	k5eAaPmAgNnS
do	do	k7c2
Marseille	Marseille	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
neúspěšně	úspěšně	k6eNd1
pokusily	pokusit	k5eAaPmAgFnP
projít	projít	k5eAaPmF
Středozemním	středozemní	k2eAgNnSc7d1
mořem	moře	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dva	dva	k4xCgMnPc1
obchodníci	obchodník	k1gMnPc1
<g/>
,	,	kIx,
Hugo	Hugo	k1gMnSc1
Železo	železo	k1gNnSc4
a	a	k8xC
Vilém	Vilém	k1gMnSc1
Prase	prase	k1gNnSc1
<g/>
,	,	kIx,
poté	poté	k6eAd1
měli	mít	k5eAaImAgMnP
nabídnout	nabídnout	k5eAaPmF
výpravě	výprava	k1gFnSc3
zdarma	zdarma	k6eAd1
přepravu	přeprava	k1gFnSc4
do	do	k7c2
Palestiny	Palestina	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
děti	dítě	k1gFnPc4
odvézt	odvézt	k5eAaPmF
do	do	k7c2
Alžíru	Alžír	k1gInSc2
a	a	k8xC
později	pozdě	k6eAd2
do	do	k7c2
Alexandrie	Alexandrie	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	on	k3xPp3gMnPc4
rozprodali	rozprodat	k5eAaPmAgMnP
do	do	k7c2
otroctví	otroctví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobná	podobný	k2eAgFnSc1d1
německá	německý	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
<g/>
,	,	kIx,
již	jenž	k3xRgFnSc4
vedl	vést	k5eAaImAgMnS
mladík	mladík	k1gMnSc1
jménem	jméno	k1gNnSc7
Mikuláš	Mikuláš	k1gMnSc1
<g/>
,	,	kIx,
údajně	údajně	k6eAd1
vyšla	vyjít	k5eAaPmAgFnS
z	z	k7c2
Porýní	Porýní	k1gNnSc2
a	a	k8xC
přes	přes	k7c4
Alpy	Alpy	k1gFnPc4
doputovala	doputovat	k5eAaPmAgFnS
do	do	k7c2
Janova	Janov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
poutníků	poutník	k1gMnPc2
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
janovskými	janovský	k2eAgMnPc7d1
občany	občan	k1gMnPc7
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
část	část	k1gFnSc1
putovala	putovat	k5eAaImAgFnS
dál	daleko	k6eAd2
do	do	k7c2
Pisy	Pisa	k1gFnSc2
a	a	k8xC
Říma	Řím	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
Mikuláše	Mikuláš	k1gMnSc4
přijal	přijmout	k5eAaPmAgMnS
sám	sám	k3xTgMnSc1
papež	papež	k1gMnSc1
a	a	k8xC
podařilo	podařit	k5eAaPmAgNnS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
přesvědčit	přesvědčit	k5eAaPmF
část	část	k1gFnSc4
dětí	dítě	k1gFnPc2
k	k	k7c3
návratu	návrat	k1gInSc3
domů	dům	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
Při	při	k7c6
zpáteční	zpáteční	k2eAgFnSc6d1
cestě	cesta	k1gFnSc6
jich	on	k3xPp3gMnPc2
mnoho	mnoho	k4c1
zemřelo	zemřít	k5eAaPmAgNnS
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimi	on	k3xPp3gMnPc7
i	i	k9
Mikuláš	Mikuláš	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Středověké	středověký	k2eAgInPc1d1
prameny	pramen	k1gInPc1
popisující	popisující	k2eAgFnSc4d1
křížovou	křížový	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
dětí	dítě	k1gFnPc2
jsou	být	k5eAaImIp3nP
však	však	k9
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
zpochybňovány	zpochybňován	k2eAgMnPc4d1
a	a	k8xC
celý	celý	k2eAgInSc4d1
popis	popis	k1gInSc4
dětské	dětský	k2eAgFnSc2d1
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
je	být	k5eAaImIp3nS
považován	považován	k2eAgInSc1d1
za	za	k7c4
omyl	omyl	k1gInSc4
vzniklý	vzniklý	k2eAgInSc4d1
třicet	třicet	k4xCc4
let	léto	k1gNnPc2
po	po	k7c6
popisovaných	popisovaný	k2eAgFnPc6d1
událostech	událost	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
Oba	dva	k4xCgInPc1
pochody	pochod	k1gInPc1
měly	mít	k5eAaImAgInP
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
proběhnout	proběhnout	k5eAaPmF
<g/>
,	,	kIx,
ale	ale	k8xC
nemělo	mít	k5eNaImAgNnS
se	se	k3xPyFc4
jednat	jednat	k5eAaImF
o	o	k7c4
pochody	pochod	k1gInPc4
dětí	dítě	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedávný	dávný	k2eNgInSc1d1
výzkum	výzkum	k1gInSc1
naznačuje	naznačovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
kronikáři	kronikář	k1gMnSc3
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
špatně	špatně	k6eAd1
pochopili	pochopit	k5eAaPmAgMnP
význam	význam	k1gInSc4
latinského	latinský	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
„	„	k?
<g/>
pueri	pueri	k1gNnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
v	v	k7c6
překladu	překlad	k1gInSc6
znamená	znamenat	k5eAaImIp3nS
„	„	k?
<g/>
chlapci	chlapec	k1gMnPc1
<g/>
“	“	k?
nebo	nebo	k8xC
také	také	k6eAd1
„	„	k?
<g/>
děti	dítě	k1gFnPc4
<g/>
“	“	k?
<g/>
,	,	kIx,
ale	ale	k8xC
zároveň	zároveň	k6eAd1
se	se	k3xPyFc4
tímto	tento	k3xDgInSc7
termínem	termín	k1gInSc7
v	v	k7c6
dobách	doba	k1gFnPc6
výprav	výprava	k1gFnPc2
označovali	označovat	k5eAaImAgMnP
poddaní	poddaný	k1gMnPc1
z	z	k7c2
nejchudších	chudý	k2eAgFnPc2d3
vrstev	vrstva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
Giovanni	Giovaneň	k1gFnSc6
Miccoli	Miccole	k1gFnSc6
a	a	k8xC
Peter	Peter	k1gMnSc1
Raedts	Raedts	k1gInSc1
si	se	k3xPyFc3
ve	v	k7c6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
všimli	všimnout	k5eAaPmAgMnP
nesrovnalostí	nesrovnalost	k1gFnPc2
a	a	k8xC
argumentovali	argumentovat	k5eAaImAgMnP
nedorozumění	nedorozumění	k1gNnSc4
těchto	tento	k3xDgMnPc2
kronikářů	kronikář	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moderní	moderní	k2eAgFnSc1d1
studie	studie	k1gFnSc1
také	také	k9
naznačuje	naznačovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
tehdejší	tehdejší	k2eAgFnSc1d1
špatná	špatný	k2eAgFnSc1d1
ekonomická	ekonomický	k2eAgFnSc1d1
situace	situace	k1gFnSc1
donutila	donutit	k5eAaPmAgFnS
některé	některý	k3yIgMnPc4
obyvatele	obyvatel	k1gMnPc4
opustit	opustit	k5eAaPmF
své	svůj	k3xOyFgInPc4
domovy	domov	k1gInPc4
a	a	k8xC
prodat	prodat	k5eAaPmF
svoje	svůj	k3xOyFgInPc4
pozemky	pozemek	k1gInPc4
a	a	k8xC
přestěhovat	přestěhovat	k5eAaPmF
se	se	k3xPyFc4
jinam	jinam	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Migrovat	migrovat	k5eAaImF
měly	mít	k5eAaImAgInP
prý	prý	k9
až	až	k9
stovky	stovka	k1gFnPc1
rodin	rodina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Pátá	pátý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Pátá	pátý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1215	#num#	k4
papež	papež	k1gMnSc1
Inocenc	Inocenc	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
svolal	svolat	k5eAaPmAgInS
Čtvrtý	čtvrtý	k4xOgInSc4
lateránský	lateránský	k2eAgInSc4d1
koncil	koncil	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgInSc7
z	z	k7c2
důsledků	důsledek	k1gInPc2
koncilu	koncil	k1gInSc2
byla	být	k5eAaImAgFnS
pátá	pátý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
<g/>
,	,	kIx,
jejíchž	jejíž	k3xOyRp3gFnPc2
příprav	příprava	k1gFnPc2
se	se	k3xPyFc4
energicky	energicky	k6eAd1
chopil	chopit	k5eAaPmAgMnS
Inocencův	Inocencův	k2eAgMnSc1d1
nástupce	nástupce	k1gMnSc1
Honorius	Honorius	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účast	účast	k1gFnSc1
na	na	k7c6
výpravě	výprava	k1gFnSc6
přislíbil	přislíbit	k5eAaPmAgMnS
uherský	uherský	k2eAgMnSc1d1
král	král	k1gMnSc1
Ondřej	Ondřej	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
norský	norský	k2eAgMnSc1d1
král	král	k1gMnSc1
Inge	Inge	k1gFnSc2
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
však	však	k9
během	během	k7c2
příprav	příprava	k1gFnPc2
zemřel	zemřít	k5eAaPmAgMnS
a	a	k8xC
norští	norský	k2eAgMnPc1d1
křižáci	křižák	k1gMnPc1
tak	tak	k6eAd1
nakonec	nakonec	k6eAd1
zůstali	zůstat	k5eAaPmAgMnP
stranou	stranou	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
rakouský	rakouský	k2eAgMnSc1d1
vévoda	vévoda	k1gMnSc1
Leopold	Leopold	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojska	vojsko	k1gNnSc2
měly	mít	k5eAaImAgInP
na	na	k7c4
Blízký	blízký	k2eAgInSc4d1
východ	východ	k1gInSc4
přepravit	přepravit	k5eAaPmF
frízské	frízský	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
Frísové	Frís	k1gMnPc1
se	se	k3xPyFc4
však	však	k9
zdrželi	zdržet	k5eAaPmAgMnP
v	v	k7c6
Portugalsku	Portugalsko	k1gNnSc6
dobýváním	dobývání	k1gNnSc7
maurského	maurský	k2eAgInSc2d1
hradu	hrad	k1gInSc2
Alcácer	Alcácer	k1gInSc1
do	do	k7c2
Sal	Sal	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
tak	tak	k6eAd1
dorazila	dorazit	k5eAaPmAgFnS
jen	jen	k9
část	část	k1gFnSc1
křižáků	křižák	k1gInPc2
<g/>
,	,	kIx,
ke	k	k7c3
kterým	který	k3yRgFnPc3,k3yIgFnPc3,k3yQgFnPc3
se	se	k3xPyFc4
přidaly	přidat	k5eAaPmAgInP
kyperské	kyperský	k2eAgInPc1d1
oddíly	oddíl	k1gInPc1
krále	král	k1gMnSc2
Huga	Hugo	k1gMnSc2
I.	I.	kA
a	a	k8xC
antiochijská	antiochijský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
knížete	kníže	k1gMnSc2
Bohemunda	Bohemund	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
Křižáci	křižák	k1gMnPc1
v	v	k7c6
Zajordánsku	Zajordánsko	k1gNnSc6
nenašli	najít	k5eNaPmAgMnP
žádného	žádný	k3yNgMnSc4
protivníka	protivník	k1gMnSc4
<g/>
,	,	kIx,
vydrancovali	vydrancovat	k5eAaPmAgMnP
široké	široký	k2eAgNnSc4d1
okolí	okolí	k1gNnSc4
a	a	k8xC
s	s	k7c7
kořistí	kořist	k1gFnSc7
se	se	k3xPyFc4
vrátili	vrátit	k5eAaPmAgMnP
do	do	k7c2
Akkonu	Akkon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Král	Král	k1gMnSc1
Ondřej	Ondřej	k1gMnSc1
odjel	odjet	k5eAaPmAgMnS
domů	domů	k6eAd1
a	a	k8xC
zbylí	zbylý	k2eAgMnPc1d1
křižáci	křižák	k1gMnPc1
se	se	k3xPyFc4
chystali	chystat	k5eAaImAgMnP
po	po	k7c6
příjezdu	příjezd	k1gInSc6
francouzských	francouzský	k2eAgFnPc2d1
posil	posila	k1gFnPc2
k	k	k7c3
útoku	útok	k1gInSc3
na	na	k7c4
egyptský	egyptský	k2eAgInSc4d1
přístav	přístav	k1gInSc4
Damietta	Damiett	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Cornelis	Cornelis	k1gFnSc1
Claesz	Claesza	k1gFnPc2
van	vana	k1gFnPc2
Wieringen	Wieringen	k1gInSc1
<g/>
:	:	kIx,
Fríští	fríský	k2eAgMnPc1d1
křižáci	křižák	k1gMnPc1
útočí	útočit	k5eAaImIp3nP
na	na	k7c4
Damiettu	Damietta	k1gFnSc4
</s>
<s>
V	v	k7c6
květnu	květen	k1gInSc6
1218	#num#	k4
křižáci	křižák	k1gMnPc1
dobyli	dobýt	k5eAaPmAgMnP
obrannou	obranný	k2eAgFnSc4d1
věž	věž	k1gFnSc4
chránící	chránící	k2eAgFnSc4d1
ústí	ústí	k1gNnSc4
Nilu	Nil	k1gInSc2
a	a	k8xC
částečně	částečně	k6eAd1
oblehli	oblehnout	k5eAaPmAgMnP
samotné	samotný	k2eAgNnSc4d1
město	město	k1gNnSc4
Damiettu	Damiett	k1gInSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
po	po	k7c6
téměř	téměř	k6eAd1
dvouletém	dvouletý	k2eAgNnSc6d1
obléhání	obléhání	k1gNnSc6
v	v	k7c6
listopadu	listopad	k1gInSc6
1219	#num#	k4
dobyli	dobýt	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vládu	vláda	k1gFnSc4
nad	nad	k7c7
městem	město	k1gNnSc7
si	se	k3xPyFc3
proti	proti	k7c3
vůli	vůle	k1gFnSc3
kardinála	kardinál	k1gMnSc2
Pelagia	Pelagius	k1gMnSc2
nárokoval	nárokovat	k5eAaImAgMnS
jeruzalémský	jeruzalémský	k2eAgMnSc1d1
král	král	k1gMnSc1
Jan	Jan	k1gMnSc1
z	z	k7c2
Brienne	Brienn	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
dočasným	dočasný	k2eAgMnSc7d1
vládcem	vládce	k1gMnSc7
Damietty	Damietta	k1gFnSc2
až	až	k8xS
do	do	k7c2
předpokládaného	předpokládaný	k2eAgInSc2d1
příjezdu	příjezd	k1gInSc2
císaře	císař	k1gMnSc2
Fridricha	Fridrich	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
Město	město	k1gNnSc1
bylo	být	k5eAaImAgNnS
křižáky	křižák	k1gMnPc7
přetvořeno	přetvořen	k2eAgNnSc1d1
v	v	k7c4
křesťanské	křesťanský	k2eAgNnSc4d1
sídlo	sídlo	k1gNnSc4
s	s	k7c7
vlastním	vlastní	k2eAgMnSc7d1
arcibiskupem	arcibiskup	k1gMnSc7
Pierrem	Pierr	k1gMnSc7
des	des	k1gNnSc2
Roches	Roches	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
příjezdu	příjezd	k1gInSc6
říšských	říšský	k2eAgInPc2d1
křižáků	křižák	k1gInPc2
měl	mít	k5eAaImAgInS
Pelagio	Pelagio	k6eAd1
pocit	pocit	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
vojsko	vojsko	k1gNnSc1
je	být	k5eAaImIp3nS
dostatečně	dostatečně	k6eAd1
silné	silný	k2eAgNnSc1d1
na	na	k7c4
dobytí	dobytí	k1gNnSc4
celého	celý	k2eAgInSc2d1
Egypta	Egypt	k1gInSc2
a	a	k8xC
navzdory	navzdory	k7c3
císařskému	císařský	k2eAgInSc3d1
rozkazu	rozkaz	k1gInSc3
se	se	k3xPyFc4
s	s	k7c7
armádou	armáda	k1gFnSc7
vydal	vydat	k5eAaPmAgInS
k	k	k7c3
jihu	jih	k1gInSc3
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
bez	bez	k7c2
císaře	císař	k1gMnSc2
s	s	k7c7
cílem	cíl	k1gInSc7
dobýt	dobýt	k5eAaPmF
Káhiru	Káhira	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výprava	výprava	k1gFnSc1
neslavně	slavně	k6eNd1
skončila	skončit	k5eAaPmAgFnS
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
se	se	k3xPyFc4
vojáci	voják	k1gMnPc1
utábořili	utábořit	k5eAaPmAgMnP
mezi	mezi	k7c7
dvěma	dva	k4xCgNnPc7
rameny	rameno	k1gNnPc7
Nilu	Nil	k1gInSc2
a	a	k8xC
Egypťané	Egypťan	k1gMnPc1
vypustili	vypustit	k5eAaPmAgMnP
říční	říční	k2eAgFnPc4d1
hráze	hráz	k1gFnPc4
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
křižáky	křižák	k1gMnPc7
odřízli	odříznout	k5eAaPmAgMnP
od	od	k7c2
pevniny	pevnina	k1gFnSc2
a	a	k8xC
donutili	donutit	k5eAaPmAgMnP
kapitulovat	kapitulovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sultán	sultána	k1gFnPc2
al-Kamil	al-Kamit	k5eAaPmAgInS
vyjednal	vyjednat	k5eAaPmAgInS
odchod	odchod	k1gInSc1
křižáků	křižák	k1gInPc2
z	z	k7c2
Damietty	Damietta	k1gFnSc2
a	a	k8xC
osmileté	osmiletý	k2eAgNnSc4d1
příměří	příměří	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
Propuštění	propuštěný	k2eAgMnPc1d1
křižáci	křižák	k1gMnPc1
odtáhli	odtáhnout	k5eAaPmAgMnP
do	do	k7c2
Akkonu	Akkon	k1gInSc2
a	a	k8xC
na	na	k7c6
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
vzdali	vzdát	k5eAaPmAgMnP
<g/>
,	,	kIx,
dal	dát	k5eAaPmAgMnS
sultán	sultán	k1gMnSc1
na	na	k7c4
počest	počest	k1gFnSc4
vítězství	vítězství	k1gNnSc4
vystavět	vystavět	k5eAaPmF
nové	nový	k2eAgNnSc4d1
město	město	k1gNnSc4
al-Mansúra	al-Mansúr	k1gInSc2
–	–	k?
Vítězné	vítězný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Císař	Císař	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Štaufský	Štaufský	k2eAgInSc4d1
na	na	k7c4
setkání	setkání	k1gNnSc4
se	s	k7c7
sultánem	sultán	k1gMnSc7
al-Kamilem	al-Kamil	k1gMnSc7
</s>
<s>
Šestá	šestý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Šestá	šestý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
září	září	k1gNnSc6
roku	rok	k1gInSc2
1227	#num#	k4
vyplula	vyplout	k5eAaPmAgFnS
z	z	k7c2
Brindisi	Brindisi	k1gNnSc2
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
velká	velký	k2eAgFnSc1d1
říšská	říšský	k2eAgFnSc1d1
flotila	flotila	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedl	vést	k5eAaImAgMnS
ji	on	k3xPp3gFnSc4
císař	císař	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
kterého	který	k3yIgMnSc4,k3yQgMnSc4,k3yRgMnSc4
k	k	k7c3
vyplutí	vyplutí	k1gNnSc3
donutila	donutit	k5eAaPmAgFnS
po	po	k7c6
devíti	devět	k4xCc6
letech	léto	k1gNnPc6
slibů	slib	k1gInPc2
až	až	k6eAd1
hrozba	hrozba	k1gFnSc1
exkomunikace	exkomunikace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
po	po	k7c6
vyplutí	vyplutí	k1gNnSc6
však	však	k9
na	na	k7c6
lodích	loď	k1gFnPc6
vypukla	vypuknout	k5eAaPmAgFnS
epidemie	epidemie	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
nemocný	nemocný	k2eAgMnSc1d1,k2eNgMnSc1d1
císař	císař	k1gMnSc1
zastavil	zastavit	k5eAaPmAgMnS
v	v	k7c6
Otrantu	Otrant	k1gInSc6
a	a	k8xC
podezřívavý	podezřívavý	k2eAgMnSc1d1
papež	papež	k1gMnSc1
Řehoř	Řehoř	k1gMnSc1
IX	IX	kA
<g/>
.	.	kIx.
ho	on	k3xPp3gNnSc4
exkomunikoval	exkomunikovat	k5eAaBmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fridrich	Fridrich	k1gMnSc1
<g/>
,	,	kIx,
jehož	jenž	k3xRgMnSc4,k3xOyRp3gMnSc4
zbožnost	zbožnost	k1gFnSc1
byla	být	k5eAaImAgFnS
vždy	vždy	k6eAd1
pochybná	pochybný	k2eAgFnSc1d1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
to	ten	k3xDgNnSc4
ignoroval	ignorovat	k5eAaImAgMnS
a	a	k8xC
po	po	k7c6
uzdravení	uzdravení	k1gNnSc6
vyrazil	vyrazit	k5eAaPmAgInS
dále	daleko	k6eAd2
na	na	k7c4
východ	východ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
září	září	k1gNnSc6
1228	#num#	k4
přistál	přistát	k5eAaImAgInS,k5eAaPmAgInS
v	v	k7c6
Akkonu	Akkon	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
již	již	k6eAd1
čekalo	čekat	k5eAaImAgNnS
křižácké	křižácký	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Císař	Císař	k1gMnSc1
neměl	mít	k5eNaImAgMnS
zájem	zájem	k1gInSc4
vést	vést	k5eAaImF
dlouhou	dlouhý	k2eAgFnSc4d1
nákladnou	nákladný	k2eAgFnSc4d1
válku	válka	k1gFnSc4
v	v	k7c6
Palestině	Palestina	k1gFnSc6
a	a	k8xC
rozhodl	rozhodnout	k5eAaPmAgMnS
se	se	k3xPyFc4
pro	pro	k7c4
diplomacii	diplomacie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sultán	sultán	k1gMnSc1
al-Kamil	al-Kamit	k5eAaPmAgMnS
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
nacházel	nacházet	k5eAaImAgMnS
uprostřed	uprostřed	k7c2
mocenského	mocenský	k2eAgInSc2d1
zápasu	zápas	k1gInSc2
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
bratry	bratr	k1gMnPc7
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
k	k	k7c3
jednání	jednání	k1gNnSc3
svolný	svolný	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fridrich	Fridrich	k1gMnSc1
však	však	k9
v	v	k7c6
Palestině	Palestina	k1gFnSc6
neměl	mít	k5eNaImAgMnS
pevnou	pevný	k2eAgFnSc4d1
pozici	pozice	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
jeruzalémský	jeruzalémský	k2eAgMnSc1d1
patriarcha	patriarcha	k1gMnSc1
i	i	k8xC
rytířské	rytířský	k2eAgInPc1d1
řády	řád	k1gInPc1
johanitů	johanita	k1gMnPc2
a	a	k8xC
templářů	templář	k1gMnPc2
ho	on	k3xPp3gMnSc4
jako	jako	k8xS,k8xC
exkomunikovaného	exkomunikovaný	k2eAgMnSc4d1
nepodporovaly	podporovat	k5eNaImAgInP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
Akkonští	Akkonský	k2eAgMnPc1d1
baroni	baron	k1gMnPc1
si	se	k3xPyFc3
zase	zase	k9
zakládali	zakládat	k5eAaImAgMnP
na	na	k7c6
své	svůj	k3xOyFgFnSc6
svobodě	svoboda	k1gFnSc6
a	a	k8xC
nechtěli	chtít	k5eNaImAgMnP
se	se	k3xPyFc4
císaři	císař	k1gMnPc1
podrobit	podrobit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
Pouze	pouze	k6eAd1
příslušníci	příslušník	k1gMnPc1
Řádu	řád	k1gInSc2
německých	německý	k2eAgMnPc2d1
rytířů	rytíř	k1gMnPc2
stáli	stát	k5eAaImAgMnP
pevně	pevně	k6eAd1
při	při	k7c6
císaři	císař	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
této	tento	k3xDgFnSc2
nejednoty	nejednota	k1gFnSc2
mezi	mezi	k7c7
křesťany	křesťan	k1gMnPc7
al-Kamil	al-Kamit	k5eAaPmAgMnS
těžil	těžit	k5eAaImAgMnS
<g/>
,	,	kIx,
jednání	jednání	k1gNnSc3
protahoval	protahovat	k5eAaImAgMnS
a	a	k8xC
Fridrich	Fridrich	k1gMnSc1
začal	začít	k5eAaPmAgMnS
slevovat	slevovat	k5eAaImF
ze	z	k7c2
svých	svůj	k3xOyFgInPc2
požadavků	požadavek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1229	#num#	k4
byla	být	k5eAaImAgFnS
dohoda	dohoda	k1gFnSc1
stvrzena	stvrdit	k5eAaPmNgFnS
<g/>
,	,	kIx,
křesťané	křesťan	k1gMnPc1
získali	získat	k5eAaPmAgMnP
nazpět	nazpět	k6eAd1
Betlém	Betlém	k1gInSc4
<g/>
,	,	kIx,
Nazaret	Nazaret	k1gInSc4
<g/>
,	,	kIx,
Galileu	Galilea	k1gFnSc4
a	a	k8xC
především	především	k9
Jeruzalém	Jeruzalém	k1gInSc1
<g/>
,	,	kIx,
do	do	k7c2
kterého	který	k3yIgNnSc2,k3yRgNnSc2,k3yQgNnSc2
však	však	k9
měli	mít	k5eAaImAgMnP
muslimové	muslim	k1gMnPc1
svobodný	svobodný	k2eAgInSc4d1
přístup	přístup	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
Poté	poté	k6eAd1
Fridrich	Fridrich	k1gMnSc1
vstoupil	vstoupit	k5eAaPmAgMnS
do	do	k7c2
Jeruzaléma	Jeruzalém	k1gInSc2
a	a	k8xC
protiprávně	protiprávně	k6eAd1
se	se	k3xPyFc4
nechal	nechat	k5eAaPmAgMnS
korunovat	korunovat	k5eAaBmF
králem	král	k1gMnSc7
jeruzalémským	jeruzalémský	k2eAgMnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právoplatným	právoplatný	k2eAgMnSc7d1
králem	král	k1gMnSc7
měl	mít	k5eAaImAgMnS
být	být	k5eAaImF
Fridrichův	Fridrichův	k2eAgMnSc1d1
nedospělý	nedospělý	k1gMnSc1
syn	syn	k1gMnSc1
Konrád	Konrád	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
květnu	květen	k1gInSc6
1229	#num#	k4
Fridrich	Fridrich	k1gMnSc1
ze	z	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
odjel	odjet	k5eAaPmAgInS
a	a	k8xC
při	při	k7c6
naloďování	naloďování	k1gNnSc6
v	v	k7c6
Akkonu	Akkon	k1gInSc6
po	po	k7c6
něm	on	k3xPp3gInSc6
místní	místní	k2eAgMnPc1d1
občané	občan	k1gMnPc1
házeli	házet	k5eAaImAgMnP
odpadky	odpadek	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1244	#num#	k4
byl	být	k5eAaImAgInS
neopevněný	opevněný	k2eNgInSc1d1
Jeruzalém	Jeruzalém	k1gInSc1
dobyt	dobyt	k2eAgInSc1d1
chórezmskými	chórezmský	k2eAgMnPc7d1
vojsky	vojsky	k6eAd1
ve	v	k7c6
službách	služba	k1gFnPc6
Egypta	Egypt	k1gInSc2
a	a	k8xC
křesťané	křesťan	k1gMnPc1
ho	on	k3xPp3gMnSc4
již	již	k9
nikdy	nikdy	k6eAd1
nezískali	získat	k5eNaPmAgMnP
<g/>
.	.	kIx.
</s>
<s>
Émile	Émile	k1gInSc1
Signol	Signol	k1gInSc1
<g/>
:	:	kIx,
Ludvík	Ludvík	k1gMnSc1
IX	IX	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svatý	svatý	k1gMnSc1
na	na	k7c6
křížové	křížový	k2eAgFnSc6d1
výpravě	výprava	k1gFnSc6
(	(	kIx(
<g/>
Salles	Salles	k1gInSc1
des	des	k1gNnSc1
Croisades	Croisades	k1gInSc1
<g/>
,	,	kIx,
Versailles	Versailles	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Sedmá	sedmý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Sedmá	sedmý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
srpnu	srpen	k1gInSc6
roku	rok	k1gInSc2
1248	#num#	k4
se	se	k3xPyFc4
na	na	k7c4
křížovou	křížový	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
vydal	vydat	k5eAaPmAgMnS
francouzský	francouzský	k2eAgMnSc1d1
král	král	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
IX	IX	kA
<g/>
..	..	k?
Vojsko	vojsko	k1gNnSc1
vyplulo	vyplout	k5eAaPmAgNnS
z	z	k7c2
nově	nově	k6eAd1
postaveného	postavený	k2eAgInSc2d1
přístavu	přístav	k1gInSc2
v	v	k7c4
Aigues-Mortes	Aigues-Mortes	k1gInSc4
a	a	k8xC
byla	být	k5eAaImAgFnS
to	ten	k3xDgNnSc4
největší	veliký	k2eAgFnSc1d3
námořní	námořní	k2eAgFnSc1d1
operace	operace	k1gFnSc1
v	v	k7c6
dějinách	dějiny	k1gFnPc6
křížových	křížový	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
mezipřistání	mezipřistání	k1gNnSc2
na	na	k7c6
Kypru	Kypr	k1gInSc6
byl	být	k5eAaImAgMnS
Ludvík	Ludvík	k1gMnSc1
místními	místní	k2eAgFnPc7d1
přesvědčen	přesvědčit	k5eAaPmNgMnS
<g/>
,	,	kIx,
že	že	k8xS
ideálním	ideální	k2eAgInSc7d1
cílem	cíl	k1gInSc7
útoku	útok	k1gInSc2
bude	být	k5eAaImBp3nS
Egypt	Egypt	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červnu	červen	k1gInSc6
1249	#num#	k4
se	se	k3xPyFc4
křižáci	křižák	k1gMnPc1
vylodili	vylodit	k5eAaPmAgMnP
před	před	k7c7
egyptskou	egyptský	k2eAgFnSc7d1
Damiettou	Damietta	k1gFnSc7
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc7,k3yIgFnSc7,k3yQgFnSc7
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
podařilo	podařit	k5eAaPmAgNnS
získat	získat	k5eAaPmF
bez	bez	k7c2
boje	boj	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
listopadu	listopad	k1gInSc6
1249	#num#	k4
se	se	k3xPyFc4
francouzské	francouzský	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
vydalo	vydat	k5eAaPmAgNnS
na	na	k7c4
pochod	pochod	k1gInSc4
do	do	k7c2
egyptského	egyptský	k2eAgNnSc2d1
vnitrozemí	vnitrozemí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Výjevy	výjev	k1gInPc4
ze	z	k7c2
sedmé	sedmý	k4xOgFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
<g/>
,	,	kIx,
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
zemřel	zemřít	k5eAaPmAgMnS
egyptský	egyptský	k2eAgMnSc1d1
sultán	sultán	k1gMnSc1
as-Sálih	as-Sálih	k1gMnSc1
Ajjúb	Ajjúb	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
vdova	vdova	k1gFnSc1
Šagrat	Šagrat	k1gInSc1
Al	ala	k1gFnPc2
Durr	Durr	k1gInSc4
smrt	smrt	k1gFnSc4
zatajila	zatajit	k5eAaPmAgFnS
a	a	k8xC
spolu	spolu	k6eAd1
s	s	k7c7
ministrem	ministr	k1gMnSc7
Fakrh	Fakrha	k1gFnPc2
ad-Dínem	ad-Dín	k1gInSc7
zemi	zem	k1gFnSc4
a	a	k8xC
armádu	armáda	k1gFnSc4
řídila	řídit	k5eAaImAgFnS
sama	sám	k3xTgMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
Egyptské	egyptský	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
na	na	k7c4
křižáky	křižák	k1gInPc4
čekalo	čekat	k5eAaImAgNnS
u	u	k7c2
pevnosti	pevnost	k1gFnSc2
al-Mansúra	al-Mansúro	k1gNnSc2
a	a	k8xC
ostřelovalo	ostřelovat	k5eAaImAgNnS
je	být	k5eAaImIp3nS
přes	přes	k7c4
řeku	řeka	k1gFnSc4
řeckým	řecký	k2eAgInSc7d1
ohněm	oheň	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křižáci	křižák	k1gMnPc1
objevili	objevit	k5eAaPmAgMnP
brod	brod	k1gInSc4
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
v	v	k7c6
únoru	únor	k1gInSc6
roku	rok	k1gInSc2
1250	#num#	k4
jejich	jejich	k3xOp3gNnSc1
předvoj	předvoj	k1gInSc1
vedený	vedený	k2eAgInSc1d1
Ludvíkovým	Ludvíkův	k2eAgMnSc7d1
bratrem	bratr	k1gMnSc7
Robertem	Robert	k1gMnSc7
zaútočil	zaútočit	k5eAaPmAgMnS
na	na	k7c4
egyptské	egyptský	k2eAgNnSc4d1
ležení	ležení	k1gNnSc4
před	před	k7c7
al-Mansúrou	al-Mansúra	k1gFnSc7
a	a	k8xC
mamlúky	mamlúky	k6eAd1
zahnal	zahnat	k5eAaPmAgMnS
do	do	k7c2
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Robert	Robert	k1gMnSc1
<g/>
,	,	kIx,
opojený	opojený	k2eAgInSc1d1
úspěchem	úspěch	k1gInSc7
<g/>
,	,	kIx,
zaútočil	zaútočit	k5eAaPmAgMnS
na	na	k7c4
město	město	k1gNnSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
v	v	k7c6
jeho	jeho	k3xOp3gFnPc6
ulicích	ulice	k1gFnPc6
se	se	k3xPyFc4
křižáci	křižák	k1gMnPc1
rozptýlili	rozptýlit	k5eAaPmAgMnP
a	a	k8xC
většinou	většinou	k6eAd1
byli	být	k5eAaImAgMnP
pobiti	pobit	k2eAgMnPc1d1
<g/>
,	,	kIx,
včetně	včetně	k7c2
Roberta	Robert	k1gMnSc2
z	z	k7c2
Artois	Artois	k1gFnSc2
nebo	nebo	k8xC
hraběte	hrabě	k1gMnSc2
Viléma	Vilém	k1gMnSc2
ze	z	k7c2
Salisbury	Salisbura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Egypťané	Egypťan	k1gMnPc1
se	se	k3xPyFc4
zatím	zatím	k6eAd1
přeskupili	přeskupit	k5eAaPmAgMnP
a	a	k8xC
po	po	k7c6
příchodu	příchod	k1gInSc6
hlavní	hlavní	k2eAgFnSc2d1
části	část	k1gFnSc2
křižácké	křižácký	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
se	se	k3xPyFc4
strhla	strhnout	k5eAaPmAgFnS
vyrovnaná	vyrovnaný	k2eAgFnSc1d1
bitva	bitva	k1gFnSc1
trvající	trvající	k2eAgFnSc2d1
celý	celý	k2eAgInSc4d1
den	den	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postup	postup	k1gInSc1
se	se	k3xPyFc4
zastavil	zastavit	k5eAaPmAgInS
a	a	k8xC
Egypťané	Egypťan	k1gMnPc1
přivolali	přivolat	k5eAaPmAgMnP
posily	posila	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
pomoc	pomoc	k1gFnSc4
jim	on	k3xPp3gMnPc3
přišel	přijít	k5eAaPmAgMnS
syn	syn	k1gMnSc1
zemřelého	zemřelý	k1gMnSc2
sultána	sultán	k1gMnSc2
Turanšáh	Turanšáh	k1gMnSc1
a	a	k8xC
pomohl	pomoct	k5eAaPmAgInS
křižáky	křižák	k1gMnPc4
odříznout	odříznout	k5eAaPmF
od	od	k7c2
nilských	nilský	k2eAgFnPc2d1
zásobovacích	zásobovací	k2eAgFnPc2d1
tras	trasa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vypuknutí	vypuknutí	k1gNnSc6
hladomoru	hladomor	k1gInSc2
musel	muset	k5eAaImAgMnS
dát	dát	k5eAaPmF
Ludvík	Ludvík	k1gMnSc1
v	v	k7c6
březnu	březen	k1gInSc6
rozkaz	rozkaz	k1gInSc1
k	k	k7c3
ústupu	ústup	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Turanšáhovi	Turanšáhův	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
začali	začít	k5eAaPmAgMnP
křižáky	křižák	k1gMnPc4
pronásledovat	pronásledovat	k5eAaImF
a	a	k8xC
ti	ten	k3xDgMnPc1
se	se	k3xPyFc4
po	po	k7c6
ochoření	ochoření	k1gNnSc6
samotného	samotný	k2eAgMnSc2d1
Ludvíka	Ludvík	k1gMnSc2
rozhodli	rozhodnout	k5eAaPmAgMnP
kapitulovat	kapitulovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celá	celý	k2eAgFnSc1d1
křižácká	křižácký	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
skončila	skončit	k5eAaPmAgFnS
v	v	k7c6
zajetí	zajetí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Damietta	Damietta	k1gFnSc1
zůstala	zůstat	k5eAaPmAgFnS
prozatím	prozatím	k6eAd1
zachována	zachovat	k5eAaPmNgFnS
díky	díky	k7c3
královně	královna	k1gFnSc3
Markétě	Markéta	k1gFnSc3
Provensálské	provensálský	k2eAgFnSc3d1
a	a	k8xC
jejím	její	k3xOp3gInSc7
janovským	janovský	k2eAgMnSc7d1
a	a	k8xC
pisánským	pisánský	k2eAgMnPc3d1
žoldnéřům	žoldnéř	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Egypťané	Egypťan	k1gMnPc1
za	za	k7c4
propuštění	propuštění	k1gNnSc4
krále	král	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
armády	armáda	k1gFnSc2
požadovali	požadovat	k5eAaImAgMnP
vydání	vydání	k1gNnSc4
Damietty	Damietta	k1gFnSc2
a	a	k8xC
zaplacení	zaplacení	k1gNnSc4
800	#num#	k4
000	#num#	k4
zlatých	zlatý	k2eAgInPc2d1
bezantů	bezant	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
Křižáci	křižák	k1gMnPc1
neměli	mít	k5eNaImAgMnP
dostatek	dostatek	k1gInSc4
hotovosti	hotovost	k1gFnSc2
<g/>
,	,	kIx,
propuštěn	propustit	k5eAaPmNgMnS
byl	být	k5eAaImAgMnS
proto	proto	k8xC
zprvu	zprvu	k6eAd1
jen	jen	k9
král	král	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
a	a	k8xC
část	část	k1gFnSc1
rytířů	rytíř	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Propuštěnému	propuštěný	k2eAgMnSc3d1
panovníkovi	panovník	k1gMnSc3
se	se	k3xPyFc4
během	během	k7c2
dalších	další	k2eAgNnPc2d1
čtyř	čtyři	k4xCgNnPc2
let	léto	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yRgNnPc4,k3yIgNnPc4,k3yQgNnPc4
strávil	strávit	k5eAaPmAgMnS
v	v	k7c6
Akkonu	Akkon	k1gInSc6
<g/>
,	,	kIx,
podařilo	podařit	k5eAaPmAgNnS
dosáhnout	dosáhnout	k5eAaPmF
propuštění	propuštění	k1gNnSc1
zbytku	zbytek	k1gInSc2
zajatců	zajatec	k1gMnPc2
a	a	k8xC
teprve	teprve	k6eAd1
24	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1254	#num#	k4
opustil	opustit	k5eAaPmAgMnS
Svatou	svatý	k2eAgFnSc4d1
zemi	zem	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Osmá	osmý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Osmá	osmý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1267	#num#	k4
vytáhl	vytáhnout	k5eAaPmAgMnS
francouzský	francouzský	k2eAgMnSc1d1
král	král	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
IX	IX	kA
<g/>
.	.	kIx.
na	na	k7c4
další	další	k2eAgFnSc4d1
křížovou	křížový	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zprvu	zprvu	k6eAd1
marně	marně	k6eAd1
hledal	hledat	k5eAaImAgMnS
společníky	společník	k1gMnPc4
<g/>
,	,	kIx,
nakonec	nakonec	k6eAd1
ho	on	k3xPp3gMnSc4
podpořil	podpořit	k5eAaPmAgMnS
bratr	bratr	k1gMnSc1
<g/>
,	,	kIx,
sicilský	sicilský	k2eAgMnSc1d1
král	král	k1gMnSc1
Karel	Karel	k1gMnSc1
z	z	k7c2
Anjou	Anjý	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
však	však	k9
chtěl	chtít	k5eAaImAgMnS
cíl	cíl	k1gInSc4
výpravy	výprava	k1gFnSc2
stočit	stočit	k5eAaPmF
ze	z	k7c2
Sýrie	Sýrie	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
mezitím	mezitím	k6eAd1
sultán	sultán	k1gMnSc1
Bajbars	Bajbarsa	k1gFnPc2
dobyl	dobýt	k5eAaPmAgMnS
jak	jak	k8xS,k8xC
Haifu	Haifa	k1gFnSc4
<g/>
,	,	kIx,
Toronu	toron	k1gInSc2
a	a	k8xC
Nazaret	Nazaret	k1gInSc4
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k9
křižáckou	křižácký	k2eAgFnSc4d1
baštu	bašta	k1gFnSc4
Antiochii	Antiochie	k1gFnSc4
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yQgNnSc4,k3yRgNnSc4,k3yIgNnSc4
prý	prý	k9
působil	působit	k5eAaImAgMnS
Pavel	Pavel	k1gMnSc1
z	z	k7c2
Tarsu	Tars	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
držení	držení	k1gNnSc6
křižáků	křižák	k1gInPc2
už	už	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1097	#num#	k4
<g/>
,	,	kIx,
na	na	k7c4
Tunis	Tunis	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
měl	mít	k5eAaImAgInS
ekonomické	ekonomický	k2eAgInPc4d1
a	a	k8xC
politické	politický	k2eAgInPc4d1
zájmy	zájem	k1gInPc4
<g/>
,	,	kIx,
a	a	k8xC
Ludvík	Ludvík	k1gMnSc1
se	se	k3xPyFc4
nechal	nechat	k5eAaPmAgMnS
přesvědčit	přesvědčit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Tunisu	Tunis	k1gInSc2
se	se	k3xPyFc4
chtěl	chtít	k5eAaImAgMnS
vydat	vydat	k5eAaPmF
na	na	k7c4
Egypt	Egypt	k1gInSc4
<g/>
,	,	kIx,
pak	pak	k6eAd1
přejít	přejít	k5eAaPmF
Sinaj	Sinaj	k1gInSc4
a	a	k8xC
z	z	k7c2
jihu	jih	k1gInSc2
znovu	znovu	k6eAd1
dobýt	dobýt	k5eAaPmF
Jeruzalém	Jeruzalém	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Křižáci	křižák	k1gMnPc1
obléhají	obléhat	k5eAaImIp3nP
Tunis	Tunis	k1gInSc4
<g/>
(	(	kIx(
<g/>
Biblioteque	Biblioteque	k1gFnSc4
nationale	nationale	k6eAd1
de	de	k?
France	Franc	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
červenci	červenec	k1gInSc6
1270	#num#	k4
se	se	k3xPyFc4
křižáci	křižák	k1gMnPc1
vylodili	vylodit	k5eAaPmAgMnP
na	na	k7c6
africkém	africký	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
obléhání	obléhání	k1gNnSc2
Tunisu	Tunis	k1gInSc2
se	se	k3xPyFc4
ve	v	k7c6
vojsku	vojsko	k1gNnSc6
křižáků	křižák	k1gInPc2
rozšířila	rozšířit	k5eAaPmAgFnS
epidemie	epidemie	k1gFnSc1
<g/>
,	,	kIx,
také	také	k6eAd1
způsobena	způsobit	k5eAaPmNgFnS
horkým	horký	k2eAgNnSc7d1
počasím	počasí	k1gNnSc7
a	a	k8xC
nedostatkem	nedostatek	k1gInSc7
pitné	pitný	k2eAgFnSc2d1
vody	voda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
nemocnými	nemocná	k1gFnPc7
byl	být	k5eAaImAgMnS
i	i	k9
král	král	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
v	v	k7c6
Kartágu	Kartágo	k1gNnSc6
při	při	k7c6
čekání	čekání	k1gNnSc6
na	na	k7c4
posily	posila	k1gFnPc4
z	z	k7c2
Anglie	Anglie	k1gFnSc2
<g/>
,	,	kIx,
Aragonska	Aragonsko	k1gNnSc2
a	a	k8xC
Navarry	Navarra	k1gFnSc2
zemřel	zemřít	k5eAaPmAgMnS
na	na	k7c4
úplavici	úplavice	k1gFnSc4
(	(	kIx(
<g/>
některé	některý	k3yIgInPc1
zdroje	zdroj	k1gInPc1
zmiňují	zmiňovat	k5eAaImIp3nP
i	i	k9
mor	mor	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Králem	Král	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
také	také	k9
nemocný	mocný	k2eNgMnSc1d1,k2eAgMnSc1d1
následník	následník	k1gMnSc1
trůnu	trůn	k1gInSc2
Filip	Filip	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velení	velení	k1gNnSc1
vojenských	vojenský	k2eAgFnPc2d1
operací	operace	k1gFnPc2
převzal	převzít	k5eAaPmAgMnS
Karel	Karel	k1gMnSc1
z	z	k7c2
Anjou	Anjý	k2eAgFnSc7d1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
jemuž	jenž	k3xRgMnSc3
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
vyjednat	vyjednat	k5eAaPmF
s	s	k7c7
tuniským	tuniský	k2eAgMnSc7d1
emírem	emír	k1gMnSc7
Muhammadem	Muhammad	k1gInSc7
al-Mustansirem	al-Mustansirem	k6eAd1
volný	volný	k2eAgInSc4d1
obchod	obchod	k1gInSc4
s	s	k7c7
městem	město	k1gNnSc7
Tunis	Tunis	k1gInSc1
<g/>
,	,	kIx,
mír	mír	k1gInSc1
a	a	k8xC
volný	volný	k2eAgInSc1d1
odchod	odchod	k1gInSc1
křižáků	křižák	k1gInPc2
z	z	k7c2
Afriky	Afrika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
do	do	k7c2
Tunisu	Tunis	k1gInSc2
dorazil	dorazit	k5eAaPmAgMnS
anglický	anglický	k2eAgMnSc1d1
princ	princ	k1gMnSc1
Eduard	Eduard	k1gMnSc1
s	s	k7c7
malým	malý	k2eAgInSc7d1
křižáckým	křižácký	k2eAgInSc7d1
sborem	sbor	k1gInSc7
z	z	k7c2
Anglie	Anglie	k1gFnSc2
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
již	již	k6eAd1
po	po	k7c6
boji	boj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
Francouzi	Francouz	k1gMnPc1
se	se	k3xPyFc4
začali	začít	k5eAaPmAgMnP
pod	pod	k7c7
velením	velení	k1gNnSc7
Karla	Karel	k1gMnSc2
z	z	k7c2
Anjou	Anja	k1gMnSc7
stahovat	stahovat	k5eAaImF
zpět	zpět	k6eAd1
do	do	k7c2
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
Angličané	Angličan	k1gMnPc1
hodlali	hodlat	k5eAaImAgMnP
pokračovat	pokračovat	k5eAaImF
v	v	k7c6
křížové	křížový	k2eAgFnSc6d1
výpravě	výprava	k1gFnSc6
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Devátá	devátý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Devátá	devátý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
neúspěchu	neúspěch	k1gInSc6
osmé	osmý	k4xOgFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
se	se	k3xPyFc4
Angličané	Angličan	k1gMnPc1
rozhodli	rozhodnout	k5eAaPmAgMnP
vydat	vydat	k5eAaPmF
do	do	k7c2
Sýrie	Sýrie	k1gFnSc2
ohrožované	ohrožovaný	k2eAgFnSc2d1
mamlúckým	mamlúcký	k2eAgMnSc7d1
sultánem	sultán	k1gMnSc7
Bajbarsem	Bajbars	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eduard	Eduard	k1gMnSc1
s	s	k7c7
anglickou	anglický	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
přezimoval	přezimovat	k5eAaBmAgInS
na	na	k7c6
Sicílii	Sicílie	k1gFnSc6
a	a	k8xC
v	v	k7c6
květnu	květen	k1gInSc6
1271	#num#	k4
se	se	k3xPyFc4
vylodil	vylodit	k5eAaPmAgInS
v	v	k7c6
Akkonu	Akkon	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
Eduardovy	Eduardův	k2eAgFnPc1d1
síly	síla	k1gFnPc1
byly	být	k5eAaImAgFnP
slabé	slabý	k2eAgFnPc1d1
<g/>
,	,	kIx,
asi	asi	k9
jen	jen	k9
tisíc	tisíc	k4xCgInSc1
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
ale	ale	k8xC
měl	mít	k5eAaImAgMnS
v	v	k7c6
úmyslu	úmysl	k1gInSc6
sjednotit	sjednotit	k5eAaPmF
rytíře	rytíř	k1gMnPc4
ve	v	k7c6
Svaté	svatý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
a	a	k8xC
na	na	k7c6
Kypru	Kypr	k1gInSc6
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
uzavřít	uzavřít	k5eAaPmF
dohodu	dohoda	k1gFnSc4
s	s	k7c7
Mongoly	Mongol	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
ohrožovali	ohrožovat	k5eAaImAgMnP
i	i	k9
muslimská	muslimský	k2eAgNnPc4d1
území	území	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
příjezdu	příjezd	k1gInSc6
do	do	k7c2
Akkonu	Akkon	k1gInSc2
Eduard	Eduard	k1gMnSc1
zjistil	zjistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
situace	situace	k1gFnSc1
křesťanů	křesťan	k1gMnPc2
na	na	k7c6
Předním	přední	k2eAgInSc6d1
východě	východ	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
mamlúcké	mamlúcký	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
ovládlo	ovládnout	k5eAaPmAgNnS
v	v	k7c6
letech	let	k1gInPc6
1265	#num#	k4
<g/>
–	–	k?
<g/>
1271	#num#	k4
většinu	většina	k1gFnSc4
území	území	k1gNnSc1
Jeruzalémského	jeruzalémský	k2eAgNnSc2d1
království	království	k1gNnSc2
a	a	k8xC
Antiochijského	antiochijský	k2eAgNnSc2d1
knížectví	knížectví	k1gNnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
mnohem	mnohem	k6eAd1
horší	zlý	k2eAgMnSc1d2
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
v	v	k7c6
západní	západní	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
domnívali	domnívat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kyperští	kyperský	k2eAgMnPc1d1
rytíři	rytíř	k1gMnPc1
odmítli	odmítnout	k5eAaPmAgMnP
bojovat	bojovat	k5eAaImF
ve	v	k7c6
Svaté	svatý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
a	a	k8xC
Benátčané	Benátčan	k1gMnPc1
s	s	k7c7
Janovany	Janovan	k1gMnPc7
začali	začít	k5eAaPmAgMnP
s	s	k7c7
mamlúky	mamlúek	k1gMnPc7
obchodovat	obchodovat	k5eAaImF
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
se	se	k3xPyFc4
souhlasem	souhlas	k1gInSc7
akkonských	akkonský	k2eAgMnPc2d1
baronů	baron	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Eduard	Eduard	k1gMnSc1
se	se	k3xPyFc4
svého	svůj	k3xOyFgInSc2
plánu	plán	k1gInSc2
nevzdal	vzdát	k5eNaPmAgMnS
a	a	k8xC
doufal	doufat	k5eAaImAgMnS
v	v	k7c4
možnost	možnost	k1gFnSc4
vyjednávání	vyjednávání	k1gNnSc2
o	o	k7c4
případnou	případný	k2eAgFnSc4d1
pomoc	pomoc	k1gFnSc4
s	s	k7c7
mongolským	mongolský	k2eAgInSc7d1
ílchanátem	ílchanát	k1gInSc7
v	v	k7c6
Persii	Persie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měl	mít	k5eAaImAgMnS
úspěch	úspěch	k1gInSc4
a	a	k8xC
ílchán	ílchán	k2eAgInSc4d1
Abaga	Abag	k1gMnSc2
vyslal	vyslat	k5eAaPmAgMnS
desetitisícovou	desetitisícový	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
dobyla	dobýt	k5eAaPmAgFnS
muslimské	muslimský	k2eAgNnSc4d1
Aleppo	Aleppa	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jakmile	jakmile	k8xS
však	však	k9
egyptský	egyptský	k2eAgMnSc1d1
sultán	sultán	k1gMnSc1
Bajbars	Bajbarsa	k1gFnPc2
vytáhl	vytáhnout	k5eAaPmAgMnS
z	z	k7c2
Damašku	Damašek	k1gInSc2
na	na	k7c4
sever	sever	k1gInSc4
<g/>
,	,	kIx,
Mongolové	Mongol	k1gMnPc1
před	před	k7c7
ním	on	k3xPp3gNnSc7
ustoupili	ustoupit	k5eAaPmAgMnP
a	a	k8xC
mamlúci	mamlúce	k1gMnPc1
město	město	k1gNnSc4
získali	získat	k5eAaPmAgMnP
zpět	zpět	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eduard	Eduard	k1gMnSc1
také	také	k9
podnikl	podniknout	k5eAaPmAgMnS
tažení	tažení	k1gNnSc4
do	do	k7c2
vnitrozemí	vnitrozemí	k1gNnSc2
<g/>
,	,	kIx,
akkonští	akkonský	k2eAgMnPc1d1
rytíři	rytíř	k1gMnPc1
však	však	k9
odmítli	odmítnout	k5eAaPmAgMnP
bránit	bránit	k5eAaImF
případně	případně	k6eAd1
dobytá	dobytý	k2eAgNnPc1d1
území	území	k1gNnPc1
a	a	k8xC
křižáci	křižák	k1gMnPc1
se	se	k3xPyFc4
bezvýsledně	bezvýsledně	k6eAd1
vrátili	vrátit	k5eAaPmAgMnP
do	do	k7c2
Akkonu	Akkon	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
Ten	ten	k3xDgInSc1
se	se	k3xPyFc4
ještě	ještě	k6eAd1
roku	rok	k1gInSc2
1263	#num#	k4
přepadení	přepadení	k1gNnSc2
ubránil	ubránit	k5eAaPmAgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
nedokázal	dokázat	k5eNaPmAgMnS
zastavit	zastavit	k5eAaPmF
postupné	postupný	k2eAgNnSc4d1
ztrácení	ztrácení	k1gNnSc4
území	území	k1gNnSc2
Jeruzalémského	jeruzalémský	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
princ	princ	k1gMnSc1
zjistil	zjistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
marné	marný	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gNnSc1
úsilí	úsilí	k1gNnSc1
<g/>
,	,	kIx,
vrátil	vrátit	k5eAaPmAgInS
se	se	k3xPyFc4
v	v	k7c6
září	září	k1gNnSc6
roku	rok	k1gInSc2
1272	#num#	k4
do	do	k7c2
Anglie	Anglie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Učinil	učinit	k5eAaImAgMnS,k5eAaPmAgMnS
tak	tak	k9
nedlouho	dlouho	k6eNd1
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
málem	málem	k6eAd1
zahynul	zahynout	k5eAaPmAgMnS
při	při	k7c6
pokusu	pokus	k1gInSc6
o	o	k7c4
atentát	atentát	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
při	při	k7c6
odjezdu	odjezd	k1gInSc6
měl	mít	k5eAaImAgInS
v	v	k7c6
úmyslu	úmysl	k1gInSc6
vrátit	vrátit	k5eAaPmF
se	se	k3xPyFc4
na	na	k7c4
Blízký	blízký	k2eAgInSc4d1
východ	východ	k1gInSc4
v	v	k7c6
čele	čelo	k1gNnSc6
veliké	veliký	k2eAgFnSc2d1
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
<g/>
,	,	kIx,
politická	politický	k2eAgFnSc1d1
situace	situace	k1gFnSc1
v	v	k7c6
Anglii	Anglie	k1gFnSc6
mu	on	k3xPp3gMnSc3
v	v	k7c6
tom	ten	k3xDgNnSc6
zabránila	zabránit	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Křižácké	křižácký	k2eAgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
Křižácké	křižácký	k2eAgFnPc1d1
državy	država	k1gFnPc1
ve	v	k7c6
Svaté	svatý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
těsně	těsně	k6eAd1
po	po	k7c6
završení	završení	k1gNnSc6
první	první	k4xOgFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
roku	rok	k1gInSc2
1102	#num#	k4
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Křižácké	křižácký	k2eAgInPc4d1
státy	stát	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Křižácká	křižácký	k2eAgNnPc1d1
panství	panství	k1gNnSc1
vzniklá	vzniklý	k2eAgFnSc1d1
během	během	k7c2
kruciát	kruciáta	k1gFnPc2
byla	být	k5eAaImAgFnS
budována	budovat	k5eAaImNgFnS
v	v	k7c6
duchu	duch	k1gMnSc6
západoevropského	západoevropský	k2eAgInSc2d1
feudalismu	feudalismus	k1gInSc2
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
vládnoucí	vládnoucí	k2eAgFnSc4d1
třídu	třída	k1gFnSc4
představovali	představovat	k5eAaImAgMnP
především	především	k9
osadníci	osadník	k1gMnPc1
z	z	k7c2
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
Původní	původní	k2eAgMnPc1d1
obyvatelé	obyvatel	k1gMnPc1
<g/>
,	,	kIx,
především	především	k6eAd1
arménští	arménský	k2eAgMnPc1d1
monofyzité	monofyzita	k1gMnPc1
<g/>
,	,	kIx,
východní	východní	k2eAgMnPc1d1
pravoslavní	pravoslavný	k2eAgMnPc1d1
křesťané	křesťan	k1gMnPc1
a	a	k8xC
muslimové	muslim	k1gMnPc1
Franky	Franky	k1gInPc4
(	(	kIx(
<g/>
jak	jak	k8xC,k8xS
orientálci	orientálec	k1gMnPc1
označovali	označovat	k5eAaImAgMnP
křižáky	křižák	k1gInPc4
<g/>
)	)	kIx)
v	v	k7c6
zemi	zem	k1gFnSc6
početně	početně	k6eAd1
převažovali	převažovat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Domorodci	domorodec	k1gMnPc1
byli	být	k5eAaImAgMnP
svými	svůj	k3xOyFgMnPc7
křižáckými	křižácký	k2eAgMnPc7d1
pány	pan	k1gMnPc7
tolerováni	tolerovat	k5eAaImNgMnP
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
Frankové	Frank	k1gMnPc1
nebyli	být	k5eNaImAgMnP
vůči	vůči	k7c3
svým	svůj	k3xOyFgMnPc3
jinověrným	jinověrný	k2eAgMnPc3d1
poddaným	poddaný	k1gMnPc3
nábožensky	nábožensky	k6eAd1
tak	tak	k6eAd1
benevolentní	benevolentní	k2eAgMnPc1d1
jako	jako	k9
Arabové	Arab	k1gMnPc1
před	před	k7c7
nimi	on	k3xPp3gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Arabský	arabský	k2eAgMnSc1d1
cestovatel	cestovatel	k1gMnSc1
z	z	k7c2
Al-Andalusu	Al-Andalus	k1gInSc2
Ibn	Ibn	k1gMnSc1
Džubajr	Džubajr	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgMnS
nepřítelem	nepřítel	k1gMnSc7
křižáků	křižák	k1gInPc2
<g/>
,	,	kIx,
při	při	k7c6
svých	svůj	k3xOyFgFnPc6
cestách	cesta	k1gFnPc6
po	po	k7c6
Jeruzalémské	jeruzalémský	k2eAgNnSc4d1
království	království	k1gNnSc4
v	v	k7c6
roce	rok	k1gInSc6
1184	#num#	k4
ke	k	k7c3
své	svůj	k3xOyFgFnSc3
nelibosti	nelibost	k1gFnSc3
zjistil	zjistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
zemi	zem	k1gFnSc6
usedlí	usedlý	k2eAgMnPc1d1
křesťané	křesťan	k1gMnPc1
a	a	k8xC
muslimové	muslim	k1gMnPc1
spolu	spolu	k6eAd1
vycházejí	vycházet	k5eAaImIp3nP
dobře	dobře	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
Jeruzalémském	jeruzalémský	k2eAgNnSc6d1
království	království	k1gNnSc6
žilo	žít	k5eAaImAgNnS
ve	v	k7c6
městech	město	k1gNnPc6
asi	asi	k9
120	#num#	k4
000	#num#	k4
Franků	Frank	k1gMnPc2
(	(	kIx(
<g/>
osadníků	osadník	k1gMnPc2
z	z	k7c2
Evropy	Evropa	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
100	#num#	k4
000	#num#	k4
muslimů	muslim	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
na	na	k7c6
venkově	venkov	k1gInSc6
žilo	žít	k5eAaImAgNnS
dalších	další	k2eAgNnPc2d1
250	#num#	k4
000	#num#	k4
muslimů	muslim	k1gMnPc2
a	a	k8xC
východních	východní	k2eAgMnPc2d1
křesťanů	křesťan	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
první	první	k4xOgFnSc6
křížové	křížový	k2eAgFnSc6d1
výpravě	výprava	k1gFnSc6
vznikly	vzniknout	k5eAaPmAgInP
na	na	k7c6
Blízkém	blízký	k2eAgInSc6d1
východě	východ	k1gInSc6
čtyři	čtyři	k4xCgInPc4
křižácké	křižácký	k2eAgInPc4d1
státy	stát	k1gInPc4
<g/>
:	:	kIx,
Edesské	Edesský	k2eAgNnSc4d1
hrabství	hrabství	k1gNnSc4
<g/>
,	,	kIx,
Antiochijské	antiochijský	k2eAgNnSc4d1
knížectví	knížectví	k1gNnSc4
<g/>
,	,	kIx,
Hrabství	hrabství	k1gNnSc4
Tripolis	Tripolis	k1gInSc1
a	a	k8xC
Jeruzalémské	jeruzalémský	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vládci	vládce	k1gMnPc1
tří	tři	k4xCgFnPc2
prvních	první	k4xOgNnPc6
byli	být	k5eAaImAgMnP
formálně	formálně	k6eAd1
vazaly	vazal	k1gMnPc7
jeruzalémského	jeruzalémský	k2eAgInSc2d1
krále	král	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
třetí	třetí	k4xOgFnSc6
křížové	křížový	k2eAgFnSc6d1
výpravě	výprava	k1gFnSc6
na	na	k7c6
Kypru	Kypr	k1gInSc6
vzniklo	vzniknout	k5eAaPmAgNnS
Kyperské	kyperský	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gNnSc1
prvním	první	k4xOgMnSc7
panovníkem	panovník	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
bývalý	bývalý	k2eAgMnSc1d1
král	král	k1gMnSc1
jeruzalémský	jeruzalémský	k2eAgMnSc1d1
Guy	Guy	k1gMnSc1
de	de	k?
Lusignan	Lusignan	k1gMnSc1
a	a	k8xC
celá	celý	k2eAgFnSc1d1
další	další	k2eAgFnSc1d1
existence	existence	k1gFnSc1
tohoto	tento	k3xDgInSc2
státu	stát	k1gInSc2
byla	být	k5eAaImAgFnS
svázána	svázat	k5eAaPmNgFnS
s	s	k7c7
rodem	rod	k1gInSc7
Lusignanů	Lusignan	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnPc2
Kypr	Kypr	k1gInSc1
utvořil	utvořit	k5eAaPmAgInS
s	s	k7c7
Jeruzalémským	jeruzalémský	k2eAgNnSc7d1
královstvím	království	k1gNnSc7
unii	unie	k1gFnSc4
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
titulárním	titulární	k2eAgMnSc7d1
jeruzalémským	jeruzalémský	k2eAgMnSc7d1
králem	král	k1gMnSc7
stal	stát	k5eAaPmAgMnS
kyperský	kyperský	k2eAgMnSc1d1
král	král	k1gMnSc1
Hugo	Hugo	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Dobytí	dobytí	k1gNnSc4
Tripolisu	Tripolis	k1gInSc2
roku	rok	k1gInSc2
1289	#num#	k4
<g/>
(	(	kIx(
<g/>
Ms	Ms	k1gFnSc1
Add	Add	k1gFnSc2
27695	#num#	k4
Fol	Fol	k1gFnPc2
<g/>
.	.	kIx.
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Po	po	k7c6
čtvrté	čtvrtý	k4xOgFnSc6
křížové	křížový	k2eAgFnSc6d1
výpravě	výprava	k1gFnSc6
a	a	k8xC
dobytí	dobytí	k1gNnSc4
Byzance	Byzanc	k1gFnSc2
vznikly	vzniknout	k5eAaPmAgFnP
v	v	k7c6
Řecku	Řecko	k1gNnSc6
další	další	k2eAgInPc1d1
čtyři	čtyři	k4xCgInPc1
křižácké	křižácký	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
:	:	kIx,
Latinské	latinský	k2eAgNnSc4d1
císařství	císařství	k1gNnSc4
a	a	k8xC
na	na	k7c6
něm	on	k3xPp3gMnSc6
formálně	formálně	k6eAd1
závislé	závislý	k2eAgNnSc1d1
Achajské	Achajský	k2eAgNnSc1d1
knížectví	knížectví	k1gNnSc1
<g/>
,	,	kIx,
Athénské	athénský	k2eAgNnSc1d1
vévodství	vévodství	k1gNnSc1
a	a	k8xC
Soluňské	soluňský	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc1d1
území	území	k1gNnSc4
<g/>
,	,	kIx,
především	především	k9
ostrovy	ostrov	k1gInPc1
<g/>
,	,	kIx,
pro	pro	k7c4
sebe	sebe	k3xPyFc4
dobyli	dobýt	k5eAaPmAgMnP
Benátčané	Benátčan	k1gMnPc1
a	a	k8xC
johanité	johanita	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
po	po	k7c6
pádu	pád	k1gInSc6
levantských	levantský	k2eAgFnPc2d1
držav	država	k1gFnPc2
usadili	usadit	k5eAaPmAgMnP
na	na	k7c6
Rhodu	Rhodos	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Kykladských	kykladský	k2eAgInPc6d1
ostrovech	ostrov	k1gInPc6
benátský	benátský	k2eAgMnSc1d1
dobrodruh	dobrodruh	k1gMnSc1
Marco	Marco	k1gMnSc1
Sanudo	Sanudo	k1gNnSc4
založil	založit	k5eAaPmAgMnS
Vévodství	vévodství	k1gNnSc4
Naxos	Naxos	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
Janované	Janovan	k1gMnPc1
se	se	k3xPyFc4
pokusili	pokusit	k5eAaPmAgMnP
získat	získat	k5eAaPmF
křižácké	křižácký	k2eAgFnPc4d1
državy	država	k1gFnPc4
<g/>
,	,	kIx,
když	když	k8xS
janovský	janovský	k2eAgMnSc1d1
admirál	admirál	k1gMnSc1
a	a	k8xC
kaper	kaper	k1gMnSc1
Enrico	Enrico	k1gMnSc1
Pescatore	Pescator	k1gMnSc5
obsadil	obsadit	k5eAaPmAgMnS
Krétu	Kréta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
několika	několik	k4yIc2
letech	let	k1gInPc6
byl	být	k5eAaImAgInS
z	z	k7c2
ostrova	ostrov	k1gInSc2
vytlačen	vytlačit	k5eAaPmNgMnS
Benátčany	Benátčan	k1gMnPc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
takže	takže	k8xS
Janovská	janovský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
žádná	žádný	k3yNgFnSc1
území	území	k1gNnSc3
nezískala	získat	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křižáci	křižák	k1gMnPc1
v	v	k7c6
Řecku	Řecko	k1gNnSc6
vybudovali	vybudovat	k5eAaPmAgMnP
podobné	podobný	k2eAgInPc4d1
feudální	feudální	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
sto	sto	k4xCgNnSc1
let	léto	k1gNnPc2
před	před	k7c7
nimi	on	k3xPp3gMnPc7
křižáci	křižák	k1gMnPc1
v	v	k7c6
Palestině	Palestina	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Řecku	Řecko	k1gNnSc6
se	se	k3xPyFc4
houfně	houfně	k6eAd1
usazovali	usazovat	k5eAaImAgMnP
západní	západní	k2eAgMnPc1d1
rytíři	rytíř	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
toužili	toužit	k5eAaImAgMnP
získat	získat	k5eAaPmF
půdu	půda	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
89	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pád	Pád	k1gInSc1
Akry	akr	k1gInPc1
v	v	k7c6
roce	rok	k1gInSc6
1291	#num#	k4
po	po	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
následoval	následovat	k5eAaImAgInS
masakr	masakr	k1gInSc1
křesťanských	křesťanský	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
Křižácké	křižácký	k2eAgInPc1d1
státy	stát	k1gInPc1
v	v	k7c6
Palestině	Palestina	k1gFnSc6
zanikly	zaniknout	k5eAaPmAgFnP
koncem	koncem	k7c2
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1268	#num#	k4
byla	být	k5eAaImAgFnS
mamlúky	mamlúky	k6eAd1
dobyta	dobýt	k5eAaPmNgFnS
a	a	k8xC
zničena	zničit	k5eAaPmNgFnS
Antiochie	Antiochie	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
90	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
roce	rok	k1gInSc6
1271	#num#	k4
se	se	k3xPyFc4
Bajbars	Bajbars	k1gInSc1
zmocnil	zmocnit	k5eAaPmAgMnS
křižáckého	křižácký	k2eAgInSc2d1
hradu	hrad	k1gInSc2
Krak	Krak	k?
des	des	k1gNnSc2
Chevaliers	Chevaliersa	k1gFnPc2
a	a	k8xC
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
postupně	postupně	k6eAd1
likvidoval	likvidovat	k5eAaBmAgMnS
poslední	poslední	k2eAgInPc4d1
zbytky	zbytek	k1gInPc4
křižáckých	křižácký	k2eAgFnPc2d1
držav	država	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1289	#num#	k4
byl	být	k5eAaImAgInS
dobyt	dobyt	k2eAgInSc1d1
a	a	k8xC
vypleněn	vypleněn	k2eAgInSc1d1
Tripolis	Tripolis	k1gInSc1
a	a	k8xC
18	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1291	#num#	k4
padl	padnout	k5eAaImAgInS,k5eAaPmAgInS
po	po	k7c6
obléhání	obléhání	k1gNnSc6
Akkon	Akkona	k1gFnPc2
s	s	k7c7
posledními	poslední	k2eAgInPc7d1
zbytky	zbytek	k1gInPc7
křižáckých	křižácký	k2eAgFnPc2d1
držav	država	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křesťané	křesťan	k1gMnPc1
<g/>
,	,	kIx,
kterým	který	k3yRgMnPc3,k3yQgMnPc3,k3yIgMnPc3
se	se	k3xPyFc4
nepodařilo	podařit	k5eNaPmAgNnS
uniknout	uniknout	k5eAaPmF
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
vyvražděni	vyvraždit	k5eAaPmNgMnP
či	či	k8xC
prodáni	prodat	k5eAaPmNgMnP
do	do	k7c2
otroctví	otroctví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přístavní	přístavní	k2eAgInSc1d1
města	město	k1gNnSc2
byla	být	k5eAaImAgFnS
zbořena	zbořen	k2eAgFnSc1d1
a	a	k8xC
nová	nový	k2eAgNnPc1d1
centra	centrum	k1gNnPc1
byla	být	k5eAaImAgNnP
později	pozdě	k6eAd2
postavena	postavit	k5eAaPmNgNnP
dál	daleko	k6eAd2
od	od	k7c2
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
nemohla	moct	k5eNaImAgFnS
sloužit	sloužit	k5eAaImF
jako	jako	k9
základna	základna	k1gFnSc1
pro	pro	k7c4
další	další	k2eAgFnSc4d1
křesťanskou	křesťanský	k2eAgFnSc4d1
ofenzívu	ofenzíva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
pádu	pád	k1gInSc6
Akkonu	Akkon	k1gInSc2
latinské	latinský	k2eAgFnSc2d1
državy	država	k1gFnSc2
přetrvaly	přetrvat	k5eAaPmAgFnP
pouze	pouze	k6eAd1
na	na	k7c6
Kypru	Kypr	k1gInSc6
a	a	k8xC
v	v	k7c6
Řecku	Řecko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Zatímco	zatímco	k8xS
v	v	k7c6
Levantě	Levanta	k1gFnSc6
křižácké	křižácký	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
,	,	kIx,
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
Edesského	Edesský	k2eAgNnSc2d1
hrabství	hrabství	k1gNnSc2
<g/>
,	,	kIx,
zanikly	zaniknout	k5eAaPmAgFnP
takřka	takřka	k6eAd1
najednou	najednou	k6eAd1
<g/>
,	,	kIx,
v	v	k7c6
Řecku	Řecko	k1gNnSc6
zanikaly	zanikat	k5eAaImAgFnP
po	po	k7c4
dobu	doba	k1gFnSc4
tří	tři	k4xCgNnPc2
set	sto	k4xCgNnPc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgInSc7
státem	stát	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
podlehl	podlehnout	k5eAaPmAgMnS
nepřátelům	nepřítel	k1gMnPc3
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
Soluňské	soluňský	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
roku	rok	k1gInSc2
1225	#num#	k4
<g/>
,	,	kIx,
po	po	k7c6
dvaceti	dvacet	k4xCc6
letech	léto	k1gNnPc6
od	od	k7c2
založení	založení	k1gNnSc2
<g/>
,	,	kIx,
dobyl	dobýt	k5eAaPmAgMnS
epirský	epirský	k2eAgMnSc1d1
despota	despota	k1gMnSc1
Michael	Michael	k1gMnSc1
Komnenos	Komnenos	k1gMnSc1
Dukas	Dukas	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Latinské	latinský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
bylo	být	k5eAaImAgNnS
od	od	k7c2
počátku	počátek	k1gInSc2
hospodářsky	hospodářsky	k6eAd1
i	i	k9
vojensky	vojensky	k6eAd1
slabé	slabý	k2eAgNnSc1d1
a	a	k8xC
roku	rok	k1gInSc2
1261	#num#	k4
nikájský	nikájský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
Michael	Michael	k1gMnSc1
VIII	VIII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Palaiologos	Palaiologos	k1gInSc1
dobyl	dobýt	k5eAaPmAgInS
nazpět	nazpět	k6eAd1
Konstantinopol	Konstantinopol	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
Poslední	poslední	k2eAgInPc1d1
dva	dva	k4xCgInPc1
křižácké	křižácký	k2eAgInPc1d1
státy	stát	k1gInPc1
na	na	k7c6
řecké	řecký	k2eAgFnSc6d1
pevnině	pevnina	k1gFnSc6
<g/>
,	,	kIx,
Achajské	Achajský	k2eAgNnSc4d1
knížectví	knížectví	k1gNnSc4
a	a	k8xC
Athénské	athénský	k2eAgNnSc4d1
vévodství	vévodství	k1gNnSc4
<g/>
,	,	kIx,
byzantskému	byzantský	k2eAgInSc3d1
protiútoku	protiútok	k1gInSc3
odolávaly	odolávat	k5eAaImAgInP
dalších	další	k2eAgNnPc6d1
dvě	dva	k4xCgFnPc4
stě	sto	k4xCgFnPc1
let	léto	k1gNnPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Achajské	Achajský	k2eAgNnSc1d1
knížectví	knížectví	k1gNnSc1
bylo	být	k5eAaImAgNnS
Morejským	Morejský	k2eAgInSc7d1
despotátem	despotát	k1gInSc7
dobyto	dobyt	k2eAgNnSc4d1
roku	rok	k1gInSc2
1432	#num#	k4
a	a	k8xC
Athény	Athéna	k1gFnPc1
až	až	k9
roku	rok	k1gInSc2
1453	#num#	k4
obsadil	obsadit	k5eAaPmAgMnS
osmanský	osmanský	k2eAgMnSc1d1
sultán	sultán	k1gMnSc1
Mehmed	Mehmed	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Ostrovní	ostrovní	k2eAgInPc1d1
státy	stát	k1gInPc1
měly	mít	k5eAaImAgFnP
silnější	silný	k2eAgFnPc1d2
vazby	vazba	k1gFnPc1
na	na	k7c6
evropské	evropský	k2eAgFnSc6d1
námořní	námořní	k2eAgFnSc6d1
mocnosti	mocnost	k1gFnSc6
<g/>
,	,	kIx,
zejména	zejména	k9
Benátky	Benátky	k1gFnPc1
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
útokům	útok	k1gInPc3
dokázaly	dokázat	k5eAaPmAgFnP
mnohem	mnohem	k6eAd1
úspěšněji	úspěšně	k6eAd2
čelit	čelit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kyperské	kyperský	k2eAgNnSc1d1
království	království	k1gNnSc1
bylo	být	k5eAaImAgNnS
od	od	k7c2
roku	rok	k1gInSc2
1426	#num#	k4
přinuceno	přinutit	k5eAaPmNgNnS
platit	platit	k5eAaImF
egyptským	egyptský	k2eAgMnPc3d1
mamlúkům	mamlúek	k1gMnPc3
výpalné	výpalné	k1gNnSc4
<g/>
,	,	kIx,
až	až	k9
roku	rok	k1gInSc2
1489	#num#	k4
královna	královna	k1gFnSc1
Caterina	Caterina	k1gFnSc1
Cornaro	Cornara	k1gFnSc5
ostrov	ostrov	k1gInSc4
prodala	prodat	k5eAaPmAgFnS
Benátčanům	Benátčan	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
ho	on	k3xPp3gMnSc4
udrželi	udržet	k5eAaPmAgMnP
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1571	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vévodství	vévodství	k1gNnSc1
Naxos	Naxos	k1gInSc1
se	se	k3xPyFc4
kvůli	kvůli	k7c3
růstu	růst	k1gInSc3
moci	moc	k1gFnSc2
Osmanské	osmanský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
v	v	k7c6
Egejském	egejský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
dostalo	dostat	k5eAaPmAgNnS
pod	pod	k7c4
osmanský	osmanský	k2eAgInSc4d1
vliv	vliv	k1gInSc4
a	a	k8xC
sultán	sultán	k1gMnSc1
Selim	Selim	k?
II	II	kA
<g/>
.	.	kIx.
roku	rok	k1gInSc2
1579	#num#	k4
většinu	většina	k1gFnSc4
Kyklad	Kyklady	k1gFnPc2
obsadil	obsadit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
ostrovy	ostrov	k1gInPc4
nicméně	nicméně	k8xC
ještě	ještě	k6eAd1
nějakou	nějaký	k3yIgFnSc4
dobu	doba	k1gFnSc4
ubránili	ubránit	k5eAaPmAgMnP
místní	místní	k2eAgMnPc1d1
šlechtici	šlechtic	k1gMnPc1
z	z	k7c2
rodu	rod	k1gInSc2
Gozzadini	Gozzadin	k2eAgMnPc1d1
a	a	k8xC
Benátská	benátský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
ztratila	ztratit	k5eAaPmAgFnS
několik	několik	k4yIc4
egejských	egejský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
a	a	k8xC
řeckých	řecký	k2eAgInPc2d1
přístavů	přístav	k1gInPc2
až	až	k9
v	v	k7c6
letech	let	k1gInPc6
1714	#num#	k4
<g/>
–	–	k?
<g/>
1718	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Rytířské	rytířský	k2eAgInPc4d1
řády	řád	k1gInPc4
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Rytířské	rytířský	k2eAgInPc4d1
řády	řád	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Templářští	templářský	k2eAgMnPc1d1
rytíři	rytíř	k1gMnPc1
hrající	hrající	k2eAgMnPc1d1
šachy	šach	k1gInPc4
<g/>
,	,	kIx,
Libro	libra	k1gFnSc5
de	de	k?
los	los	k1gInSc1
juegos	juegosa	k1gFnPc2
<g/>
,	,	kIx,
1283	#num#	k4
</s>
<s>
Křížové	Křížové	k2eAgFnPc1d1
výpravy	výprava	k1gFnPc1
podstatně	podstatně	k6eAd1
přispěly	přispět	k5eAaPmAgFnP
ke	k	k7c3
vzniku	vznik	k1gInSc3
rytířských	rytířský	k2eAgInPc2d1
řádů	řád	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
v	v	k7c6
sobě	se	k3xPyFc3
spojovaly	spojovat	k5eAaImAgInP
vojenský	vojenský	k2eAgInSc4d1
charakter	charakter	k1gInSc4
se	s	k7c7
snahou	snaha	k1gFnSc7
o	o	k7c4
nápodobu	nápodoba	k1gFnSc4
klášterního	klášterní	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k9
první	první	k4xOgMnSc1
byl	být	k5eAaImAgInS
v	v	k7c6
Jeruzalémě	Jeruzalém	k1gInSc6
založen	založit	k5eAaPmNgInS
řád	řád	k1gInSc1
johanitů	johanita	k1gMnPc2
(	(	kIx(
<g/>
později	pozdě	k6eAd2
známý	známý	k2eAgInSc1d1
jako	jako	k8xC,k8xS
řád	řád	k1gInSc1
maltézských	maltézský	k2eAgMnPc2d1
rytířů	rytíř	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
uskupení	uskupení	k1gNnSc1
původně	původně	k6eAd1
řádem	řád	k1gInSc7
nebylo	být	k5eNaImAgNnS
a	a	k8xC
existovalo	existovat	k5eAaImAgNnS
v	v	k7c6
Jeruzalémě	Jeruzalém	k1gInSc6
již	již	k6eAd1
před	před	k7c7
první	první	k4xOgFnSc7
křížovou	křížový	k2eAgFnSc7d1
výpravou	výprava	k1gFnSc7
jen	jen	k9
jako	jako	k8xC,k8xS
bratrstvo	bratrstvo	k1gNnSc4
<g/>
,	,	kIx,
takže	takže	k8xS
muslimští	muslimský	k2eAgMnPc1d1
vládcové	vládce	k1gMnPc1
tuto	tento	k3xDgFnSc4
křesťanskou	křesťanský	k2eAgFnSc4d1
organizaci	organizace	k1gFnSc4
tolerovali	tolerovat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původním	původní	k2eAgNnSc7d1
posláním	poslání	k1gNnSc7
bratrstva	bratrstvo	k1gNnSc2
před	před	k7c7
první	první	k4xOgFnSc7
křížovou	křížový	k2eAgFnSc7d1
výpravou	výprava	k1gFnSc7
byla	být	k5eAaImAgFnS
špitální	špitální	k2eAgFnSc1d1
péče	péče	k1gFnSc1
o	o	k7c4
křesťanské	křesťanský	k2eAgMnPc4d1
poutníky	poutník	k1gMnPc4
putujících	putující	k2eAgInPc2d1
do	do	k7c2
Palestiny	Palestina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dobytí	dobytí	k1gNnSc6
Jeruzaléma	Jeruzalém	k1gInSc2
Godefroi	Godefro	k1gFnSc2
z	z	k7c2
Bouillonu	Bouillon	k1gInSc2
začal	začít	k5eAaPmAgInS
bratrstvo	bratrstvo	k1gNnSc4
podporovat	podporovat	k5eAaImF
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
tak	tak	k9
mohlo	moct	k5eAaImAgNnS
sílit	sílit	k5eAaImF
co	co	k9
do	do	k7c2
majetku	majetek	k1gInSc2
a	a	k8xC
vlivu	vliv	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
špitální	špitální	k2eAgNnSc1d1
bratrstvo	bratrstvo	k1gNnSc1
bylo	být	k5eAaImAgNnS
uznáno	uznat	k5eAaPmNgNnS
jako	jako	k8xS,k8xC
církevní	církevní	k2eAgInSc1d1
řád	řád	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
řádem	řád	k1gInSc7
vojenským	vojenský	k2eAgInSc7d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
91	#num#	k4
<g/>
]	]	kIx)
Johanité	johanita	k1gMnPc1
si	se	k3xPyFc3
i	i	k9
nadále	nadále	k6eAd1
zachovávali	zachovávat	k5eAaImAgMnP
svou	svůj	k3xOyFgFnSc4
původní	původní	k2eAgFnSc4d1
náplň	náplň	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
jako	jako	k9
řád	řád	k1gInSc1
vojenský	vojenský	k2eAgInSc1d1
a	a	k8xC
rytířský	rytířský	k2eAgMnSc1d1
získali	získat	k5eAaPmAgMnP
nové	nový	k2eAgFnPc4d1
povinnosti	povinnost	k1gFnPc4
jako	jako	k8xC,k8xS
byla	být	k5eAaImAgFnS
ochrana	ochrana	k1gFnSc1
poutníků	poutník	k1gMnPc2
a	a	k8xC
obrana	obrana	k1gFnSc1
Kristova	Kristův	k2eAgInSc2d1
hrobu	hrob	k1gInSc2
(	(	kIx(
<g/>
a	a	k8xC
křesťanského	křesťanský	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Řád	řád	k1gInSc4
templářů	templář	k1gMnPc2
byl	být	k5eAaImAgMnS
organizací	organizace	k1gFnSc7
od	od	k7c2
počátku	počátek	k1gInSc2
ryze	ryze	k6eAd1
vojenského	vojenský	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Založil	založit	k5eAaPmAgMnS
ho	on	k3xPp3gInSc2
roku	rok	k1gInSc2
1119	#num#	k4
chudý	chudý	k1gMnSc1
rytíř	rytíř	k1gMnSc1
Hugues	Hugues	k1gMnSc1
de	de	k?
Payens	Payens	k1gInSc1
spolu	spolu	k6eAd1
se	s	k7c7
sedmi	sedm	k4xCc7
druhy	druh	k1gMnPc7
na	na	k7c4
ochranu	ochrana	k1gFnSc4
poutníků	poutník	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
přicházeli	přicházet	k5eAaImAgMnP
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
Často	často	k6eAd1
se	se	k3xPyFc4
dostávali	dostávat	k5eAaImAgMnP
do	do	k7c2
konfliktu	konflikt	k1gInSc2
s	s	k7c7
konkurenčním	konkurenční	k2eAgInSc7d1
řádem	řád	k1gInSc7
johanitů	johanita	k1gMnPc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
měl	mít	k5eAaImAgInS
podobné	podobný	k2eAgFnPc4d1
povinnosti	povinnost	k1gFnPc4
a	a	k8xC
privilegia	privilegium	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řád	řád	k1gInSc1
neměl	mít	k5eNaImAgInS
charitativní	charitativní	k2eAgFnSc4d1
povahu	povaha	k1gFnSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
jednou	jeden	k4xCgFnSc7
za	za	k7c4
čas	čas	k1gInSc4
obdarovával	obdarovávat	k5eAaImAgMnS
chudé	chudý	k2eAgNnSc4d1
almužnou	almužna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Templářský	templářský	k2eAgInSc1d1
řád	řád	k1gInSc1
navíc	navíc	k6eAd1
umožňoval	umožňovat	k5eAaImAgInS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
si	se	k3xPyFc3
šlechta	šlechta	k1gFnSc1
uschovávala	uschovávat	k5eAaImAgFnS
peníze	peníz	k1gInPc4
a	a	k8xC
cenné	cenný	k2eAgInPc4d1
předměty	předmět	k1gInPc4
v	v	k7c6
templářských	templářský	k2eAgInPc6d1
domech	dům	k1gInPc6
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
u	u	k7c2
řádu	řád	k1gInSc2
zapříčinilo	zapříčinit	k5eAaPmAgNnS
vytvoření	vytvoření	k1gNnSc1
rozsáhlé	rozsáhlý	k2eAgFnSc2d1
bankovní	bankovní	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peníze	peníz	k1gInSc2
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
si	se	k3xPyFc3
v	v	k7c6
řádovém	řádový	k2eAgInSc6d1
domě	dům	k1gInSc6
uložit	uložit	k5eAaPmF
a	a	k8xC
poté	poté	k6eAd1
si	se	k3xPyFc3
je	být	k5eAaImIp3nS
vyzvednout	vyzvednout	k5eAaPmF
ve	v	k7c6
Svaté	svatý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
bylo	být	k5eAaImAgNnS
praktické	praktický	k2eAgNnSc1d1
a	a	k8xC
výhodné	výhodný	k2eAgNnSc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
šlechtici	šlechtic	k1gMnPc1
mohli	moct	k5eAaImAgMnP
být	být	k5eAaImF
o	o	k7c4
svůj	svůj	k3xOyFgInSc4
majetek	majetek	k1gInSc4
připraveni	připravit	k5eAaPmNgMnP
bandity	bandita	k1gMnPc4
během	během	k7c2
svého	svůj	k3xOyFgNnSc2
putování	putování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
postupem	postupem	k7c2
času	čas	k1gInSc2
získávali	získávat	k5eAaImAgMnP
templáři	templář	k1gMnPc1
majetek	majetek	k1gInSc4
a	a	k8xC
moc	moc	k6eAd1
a	a	k8xC
v	v	k7c6
polovině	polovina	k1gFnSc6
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
tvořili	tvořit	k5eAaImAgMnP
asi	asi	k9
třetinu	třetina	k1gFnSc4
všech	všecek	k3xTgMnPc2
rytířů	rytíř	k1gMnPc2
v	v	k7c6
Jeruzalémském	jeruzalémský	k2eAgNnSc6d1
království	království	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
poslední	poslední	k2eAgFnPc1d1
křižácké	křižácký	k2eAgFnPc1d1
državy	država	k1gFnPc1
již	již	k6eAd1
byly	být	k5eAaImAgFnP
v	v	k7c6
rozkladu	rozklad	k1gInSc6
<g/>
,	,	kIx,
disponovaly	disponovat	k5eAaBmAgInP
rytířské	rytířský	k2eAgInPc1d1
řády	řád	k1gInPc1
jako	jako	k8xC,k8xS
jediné	jediný	k2eAgInPc1d1
ukázněnou	ukázněný	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yRgNnPc1,k3yIgNnPc1
bránila	bránit	k5eAaImAgNnP
křižácká	křižácký	k2eAgNnPc1d1
města	město	k1gNnPc1
před	před	k7c7
muslimy	muslim	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byli	být	k5eAaImAgMnP
to	ten	k3xDgNnSc1
templáři	templář	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
hájili	hájit	k5eAaImAgMnP
citadelu	citadela	k1gFnSc4
v	v	k7c6
Akkonu	Akkon	k1gInSc6
<g/>
[	[	kIx(
<g/>
94	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Sidónu	Sidóna	k1gFnSc4
i	i	k8xC
poslední	poslední	k2eAgFnSc4d1
pevnost	pevnost	k1gFnSc4
Ruád	Ruáda	k1gFnPc2
před	před	k7c7
přístavním	přístavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
Tortosa	Tortosa	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
dobyta	dobýt	k5eAaPmNgFnS
až	až	k9
roku	rok	k1gInSc2
1303	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
95	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rytíř	Rytíř	k1gMnSc1
Řádu	řád	k1gInSc2
mečových	mečový	k2eAgMnPc2d1
bratří	bratr	k1gMnPc2
z	z	k7c2
Pobaltí	Pobaltí	k1gNnSc2
</s>
<s>
Templáři	templář	k1gMnPc1
a	a	k8xC
johanité	johanita	k1gMnPc1
byly	být	k5eAaImAgFnP
natolik	natolik	k6eAd1
úspěšní	úspěšný	k2eAgMnPc1d1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
rozšířili	rozšířit	k5eAaPmAgMnP
téměř	téměř	k6eAd1
po	po	k7c6
celé	celý	k2eAgFnSc6d1
západní	západní	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
včetně	včetně	k7c2
českého	český	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
privilegia	privilegium	k1gNnPc4
daná	daný	k2eAgFnSc1d1
papežem	papež	k1gMnSc7
z	z	k7c2
nich	on	k3xPp3gMnPc2
činila	činit	k5eAaImAgFnS
nezávislé	závislý	k2eNgInPc4d1
subjekty	subjekt	k1gInPc4
<g/>
,	,	kIx,
staly	stát	k5eAaPmAgInP
se	se	k3xPyFc4
z	z	k7c2
nich	on	k3xPp3gInPc2
mezinárodní	mezinárodní	k2eAgFnSc4d1
organizace	organizace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Papež	Papež	k1gMnSc1
byl	být	k5eAaImAgMnS
jejich	jejich	k3xOp3gFnSc7
jedinou	jediný	k2eAgFnSc7d1
autoritou	autorita	k1gFnSc7
a	a	k8xC
do	do	k7c2
politiky	politika	k1gFnSc2
řádů	řád	k1gInPc2
zasahoval	zasahovat	k5eAaImAgInS
jen	jen	k9
výjimečně	výjimečně	k6eAd1
a	a	k8xC
v	v	k7c6
krajních	krajní	k2eAgInPc6d1
případech	případ	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmistři	velmistr	k1gMnPc1
řádů	řád	k1gInPc2
si	se	k3xPyFc3
počínali	počínat	k5eAaImAgMnP
jako	jako	k9
samostatní	samostatný	k2eAgMnPc1d1
vládcové	vládce	k1gMnPc1
a	a	k8xC
těšili	těšit	k5eAaImAgMnP
se	se	k3xPyFc4
knížecích	knížecí	k2eAgInPc2d1
titulů	titul	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rytířské	rytířský	k2eAgInPc1d1
řády	řád	k1gInPc1
měli	mít	k5eAaImAgMnP
volné	volný	k2eAgNnSc4d1
pole	pole	k1gNnSc4
působnosti	působnost	k1gFnSc2
a	a	k8xC
světští	světský	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
ani	ani	k8xC
duchovní	duchovní	k2eAgMnPc1d1
nemohli	moct	k5eNaImAgMnP
do	do	k7c2
působení	působení	k1gNnSc2
řádů	řád	k1gInPc2
zasahovat	zasahovat	k5eAaImF
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
zapříčinilo	zapříčinit	k5eAaPmAgNnS
mnohé	mnohý	k2eAgInPc4d1
konflikty	konflikt	k1gInPc4
nejen	nejen	k6eAd1
se	s	k7c7
světskými	světský	k2eAgMnPc7d1
panovníky	panovník	k1gMnPc7
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
s	s	k7c7
papežským	papežský	k2eAgInSc7d1
stolcem	stolec	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
byl	být	k5eAaImAgInS
také	také	k9
jeden	jeden	k4xCgInSc1
z	z	k7c2
důvodů	důvod	k1gInPc2
proč	proč	k6eAd1
začala	začít	k5eAaPmAgFnS
popularita	popularita	k1gFnSc1
rytířských	rytířský	k2eAgInPc2d1
řádů	řád	k1gInPc2
klesat	klesat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vše	všechen	k3xTgNnSc1
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS
nařčením	nařčení	k1gNnSc7
templářského	templářský	k2eAgInSc2d1
řádu	řád	k1gInSc2
z	z	k7c2
kacířství	kacířství	k1gNnSc2
a	a	k8xC
jejich	jejich	k3xOp3gNnSc7
následným	následný	k2eAgNnSc7d1
zrušením	zrušení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
rytířské	rytířský	k2eAgInPc1d1
řády	řád	k1gInPc1
neměly	mít	k5eNaImAgInP
stejná	stejný	k2eAgNnPc1d1
privilegia	privilegium	k1gNnPc1
jako	jako	k8xS,k8xC
templáři	templář	k1gMnPc1
nebo	nebo	k8xC
johanité	johanita	k1gMnPc1
<g/>
,	,	kIx,
takže	takže	k8xS
se	se	k3xPyFc4
jen	jen	k9
stěží	stěží	k6eAd1
prosazovali	prosazovat	k5eAaImAgMnP
a	a	k8xC
jejich	jejich	k3xOp3gInPc1
úspěchy	úspěch	k1gInPc1
byly	být	k5eAaImAgInP
spíše	spíše	k9
dílčího	dílčí	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řád	řád	k1gInSc4
lazariánů	lazarián	k1gInPc2
neboli	neboli	k8xC
Řád	řád	k1gInSc1
sv.	sv.	kA
Lazara	Lazar	k1gMnSc2
působil	působit	k5eAaImAgMnS
již	již	k6eAd1
jako	jako	k8xC,k8xS
bratrstvo	bratrstvo	k1gNnSc4
v	v	k7c6
Jeruzalémě	Jeruzalém	k1gInSc6
společně	společně	k6eAd1
s	s	k7c7
johanitským	johanitský	k2eAgNnSc7d1
bratrstvem	bratrstvo	k1gNnSc7
<g/>
,	,	kIx,
se	s	k7c7
kterým	který	k3yQgMnSc7,k3yRgMnSc7,k3yIgMnSc7
se	se	k3xPyFc4
sdružoval	sdružovat	k5eAaImAgMnS
do	do	k7c2
koalice	koalice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řád	řád	k1gInSc1
spravoval	spravovat	k5eAaImAgInS
lazarit	lazarit	k1gInSc4
pro	pro	k7c4
malomocné	malomocný	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
vojenským	vojenský	k2eAgInSc7d1
řádem	řád	k1gInSc7
<g/>
,	,	kIx,
neboť	neboť	k8xC
pod	pod	k7c7
sebou	se	k3xPyFc7
začal	začít	k5eAaPmAgInS
sdružoval	sdružovat	k5eAaImAgInS
nejen	nejen	k6eAd1
leprou	lepra	k1gFnSc7
nakažené	nakažený	k2eAgMnPc4d1
křižáky	křižák	k1gMnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
templáře	templář	k1gMnPc4
a	a	k8xC
johanity	johanita	k1gMnPc4
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
malomocenstvím	malomocenství	k1gNnSc7
onemocněli	onemocnět	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgInPc7d1
řády	řád	k1gInPc7
byly	být	k5eAaImAgFnP
Řád	řád	k1gInSc1
rytířů	rytíř	k1gMnPc2
z	z	k7c2
Montjoie	Montjoie	k1gFnSc2
(	(	kIx(
<g/>
sdružoval	sdružovat	k5eAaImAgInS
křižáky	křižák	k1gMnPc4
z	z	k7c2
Pyrenejského	pyrenejský	k2eAgInSc2d1
poloostrova	poloostrov	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
nebyl	být	k5eNaImAgInS
úspěšný	úspěšný	k2eAgMnSc1d1
<g/>
)	)	kIx)
a	a	k8xC
Řád	řád	k1gInSc1
rytířů	rytíř	k1gMnPc2
sv.	sv.	kA
Tomáše	Tomáš	k1gMnSc4
z	z	k7c2
Akry	akr	k1gInPc4
(	(	kIx(
<g/>
v	v	k7c6
Akře	Akře	k1gFnSc6
<g/>
,	,	kIx,
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
město	město	k1gNnSc1
bylo	být	k5eAaImAgNnS
dobyto	dobýt	k5eAaPmNgNnS
Richardem	Richard	k1gMnSc7
Lví	lví	k2eAgNnPc1d1
Srdce	srdce	k1gNnPc1
<g/>
,	,	kIx,
sdružoval	sdružovat	k5eAaImAgInS
anglicky	anglicky	k6eAd1
hovořící	hovořící	k2eAgInSc1d1
<g/>
,	,	kIx,
zejména	zejména	k9
křižáky	křižák	k1gMnPc7
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
přišli	přijít	k5eAaPmAgMnP
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
s	s	k7c7
Richardem	Richard	k1gMnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
se	se	k3xPyFc4
nedokázali	dokázat	k5eNaPmAgMnP
významněji	významně	k6eAd2
prosadit	prosadit	k5eAaPmF
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
johanitech	johanita	k1gMnPc6
a	a	k8xC
templářích	templář	k1gMnPc6
se	s	k7c7
třetím	třetí	k4xOgInSc7
nejsilnějším	silný	k2eAgInSc7d3
řádem	řád	k1gInSc7
stal	stát	k5eAaPmAgInS
až	až	k9
Řád	řád	k1gInSc1
německých	německý	k2eAgMnPc2d1
rytířů	rytíř	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řehole	řehole	k1gFnSc1
řádu	řád	k1gInSc2
byla	být	k5eAaImAgFnS
odvozena	odvozen	k2eAgFnSc1d1
od	od	k7c2
templářů	templář	k1gMnPc2
i	i	k8xC
johanitů	johanita	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
založen	založit	k5eAaPmNgMnS
ve	v	k7c6
Svaté	svatý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
jako	jako	k8xS,k8xC
reakce	reakce	k1gFnSc1
na	na	k7c4
silný	silný	k2eAgInSc4d1
příliv	příliv	k1gInSc4
křižáků	křižák	k1gInPc2
z	z	k7c2
německých	německý	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křižácké	křižácký	k2eAgInPc1d1
státy	stát	k1gInPc1
byly	být	k5eAaImAgInP
obydlené	obydlený	k2eAgInPc4d1
převážně	převážně	k6eAd1
Franky	Frank	k1gMnPc7
<g/>
,	,	kIx,
německy	německy	k6eAd1
hovořící	hovořící	k2eAgMnPc1d1
křižáci	křižák	k1gMnPc1
se	se	k3xPyFc4
ve	v	k7c6
Svaté	svatý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
špatně	špatně	k6eAd1
dorozumívali	dorozumívat	k5eAaImAgMnP
a	a	k8xC
neradi	nerad	k2eAgMnPc1d1
vstupovali	vstupovat	k5eAaImAgMnP
k	k	k7c3
frankofonním	frankofonní	k2eAgMnPc3d1
templářům	templář	k1gMnPc3
nebo	nebo	k8xC
johanitům	johanita	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Raději	rád	k6eAd2
vstupovali	vstupovat	k5eAaImAgMnP
do	do	k7c2
německého	německý	k2eAgNnSc2d1
uskupení	uskupení	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
původně	původně	k6eAd1
působilo	působit	k5eAaImAgNnS
jen	jen	k9
jako	jako	k8xS,k8xC
polní	polní	k2eAgInSc4d1
lazaret	lazaret	k1gInSc4
řádu	řád	k1gInSc2
johanitů	johanita	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
se	se	k3xPyFc4
roku	rok	k1gInSc2
1190	#num#	k4
osamostatnili	osamostatnit	k5eAaPmAgMnP
a	a	k8xC
roku	rok	k1gInSc2
1196	#num#	k4
byli	být	k5eAaImAgMnP
papežem	papež	k1gMnSc7
uznáni	uznat	k5eAaPmNgMnP
jako	jako	k8xC,k8xS
samostatný	samostatný	k2eAgInSc4d1
řád	řád	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
96	#num#	k4
<g/>
]	]	kIx)
takže	takže	k8xS
začali	začít	k5eAaPmAgMnP
fungovat	fungovat	k5eAaImF
také	také	k9
jako	jako	k9
prostředníci	prostředník	k1gMnPc1
mezi	mezi	k7c7
Jeruzalémským	jeruzalémský	k2eAgNnSc7d1
královstvím	království	k1gNnSc7
a	a	k8xC
římsko-německými	římsko-německý	k2eAgMnPc7d1
císaři	císař	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řád	řád	k1gInSc1
získal	získat	k5eAaPmAgInS
stejná	stejný	k2eAgNnPc4d1
privilegia	privilegium	k1gNnPc4
jako	jako	k8xC,k8xS
měli	mít	k5eAaImAgMnP
templáři	templář	k1gMnPc1
a	a	k8xC
johanité	johanita	k1gMnPc1
<g/>
,	,	kIx,
takže	takže	k8xS
se	se	k3xPyFc4
velice	velice	k6eAd1
brzy	brzy	k6eAd1
stali	stát	k5eAaPmAgMnP
významnou	významný	k2eAgFnSc7d1
silou	síla	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Svaté	svatý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
vlastnili	vlastnit	k5eAaImAgMnP
několik	několik	k4yIc4
hradů	hrad	k1gInPc2
a	a	k8xC
v	v	k7c6
Akkonu	Akkon	k1gInSc6
měli	mít	k5eAaImAgMnP
jednu	jeden	k4xCgFnSc4
citadelu	citadela	k1gFnSc4
<g/>
,	,	kIx,
ovšem	ovšem	k9
víc	hodně	k6eAd2
se	se	k3xPyFc4
v	v	k7c6
Levantě	Levanta	k1gFnSc6
uchytit	uchytit	k5eAaPmF
nedokázal	dokázat	k5eNaPmAgMnS
<g/>
,	,	kIx,
neboť	neboť	k8xC
většina	většina	k1gFnSc1
půdy	půda	k1gFnSc2
a	a	k8xC
majetku	majetek	k1gInSc2
byla	být	k5eAaImAgFnS
již	již	k6eAd1
rozebrána	rozebrat	k5eAaPmNgFnS
mezi	mezi	k7c4
řády	řád	k1gInPc4
templářů	templář	k1gMnPc2
a	a	k8xC
johanitů	johanita	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
Řád	řád	k1gInSc1
německých	německý	k2eAgMnPc2d1
rytířů	rytíř	k1gMnPc2
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
významně	významně	k6eAd1
angažovat	angažovat	k5eAaBmF
také	také	k9
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
východní	východní	k2eAgFnSc1d1
hranice	hranice	k1gFnSc1
křesťanských	křesťanský	k2eAgInPc2d1
států	stát	k1gInPc2
byly	být	k5eAaImAgFnP
ohrožovány	ohrožován	k2eAgFnPc1d1
pohany	pohana	k1gFnPc1
a	a	k8xC
tzv.	tzv.	kA
schizmatiky	schizmatik	k1gMnPc4
(	(	kIx(
<g/>
pravoslavnými	pravoslavný	k2eAgInPc7d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Templáři	templář	k1gMnPc7
a	a	k8xC
johanité	johanita	k1gMnPc1
se	se	k3xPyFc4
v	v	k7c6
evropských	evropský	k2eAgInPc6d1
bojích	boj	k1gInPc6
výjimečně	výjimečně	k6eAd1
také	také	k9
účastnili	účastnit	k5eAaImAgMnP
<g/>
,	,	kIx,
ale	ale	k8xC
jejich	jejich	k3xOp3gNnSc1
poslání	poslání	k1gNnSc1
se	se	k3xPyFc4
silně	silně	k6eAd1
vztahovalo	vztahovat	k5eAaImAgNnS
hlavně	hlavně	k9
na	na	k7c6
obraně	obrana	k1gFnSc6
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
její	její	k3xOp3gFnSc6
ztrátě	ztráta	k1gFnSc6
přesunuli	přesunout	k5eAaPmAgMnP
templáři	templář	k1gMnPc1
své	svůj	k3xOyFgNnSc4
sídlo	sídlo	k1gNnSc4
na	na	k7c4
Kypr	Kypr	k1gInSc4
a	a	k8xC
později	pozdě	k6eAd2
do	do	k7c2
Francie	Francie	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
začali	začít	k5eAaPmAgMnP
hromadit	hromadit	k5eAaImF
majetek	majetek	k1gInSc4
údajně	údajně	k6eAd1
pro	pro	k7c4
financování	financování	k1gNnSc4
další	další	k2eAgFnSc2d1
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Johanité	johanita	k1gMnPc1
přesunuli	přesunout	k5eAaPmAgMnP
hlavní	hlavní	k2eAgNnSc4d1
pole	pole	k1gNnSc4
působnosti	působnost	k1gFnSc2
do	do	k7c2
Středomoří	středomoří	k1gNnSc2
(	(	kIx(
<g/>
patrně	patrně	k6eAd1
s	s	k7c7
vidinou	vidina	k1gFnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
znovu	znovu	k6eAd1
vrátí	vrátit	k5eAaPmIp3nS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hlavním	hlavní	k2eAgNnSc7d1
sídlem	sídlo	k1gNnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
ostrov	ostrov	k1gInSc1
Rhodos	Rhodos	k1gInSc1
a	a	k8xC
později	pozdě	k6eAd2
Malta	Malta	k1gFnSc1
(	(	kIx(
<g/>
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
začali	začít	k5eAaPmAgMnP
být	být	k5eAaImF
označováni	označován	k2eAgMnPc1d1
jako	jako	k8xS,k8xC
maltézští	maltézský	k2eAgMnPc1d1
rytíři	rytíř	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Prusy	Prus	k1gMnPc4
a	a	k8xC
Livonsko	Livonsko	k1gNnSc4
dobyté	dobytý	k2eAgInPc4d1
křižáky	křižák	k1gInPc4
na	na	k7c6
počátku	počátek	k1gInSc6
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
<g/>
Mapa	mapa	k1gFnSc1
zobrazuje	zobrazovat	k5eAaImIp3nS
území	území	k1gNnSc1
ovládané	ovládaný	k2eAgNnSc1d1
Řádem	řád	k1gInSc7
německých	německý	k2eAgMnPc2d1
rytířů	rytíř	k1gMnPc2
a	a	k8xC
Řádem	řád	k1gInSc7
mečových	mečový	k2eAgMnPc2d1
bratří	bratr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Výpravy	výprava	k1gFnPc4
proti	proti	k7c3
Osmanům	Osman	k1gMnPc3
v	v	k7c6
pozdním	pozdní	k2eAgInSc6d1
středověku	středověk	k1gInSc6
</s>
<s>
Poté	poté	k6eAd1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Osmané	Osman	k1gMnPc1
pronikli	proniknout	k5eAaPmAgMnP
na	na	k7c4
Balkán	Balkán	k1gInSc4
<g/>
,	,	kIx,
začalo	začít	k5eAaPmAgNnS
si	se	k3xPyFc3
křesťanstvo	křesťanstvo	k1gNnSc4
uvědomovat	uvědomovat	k5eAaImF
hrozbu	hrozba	k1gFnSc4
Osmanské	osmanský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
začali	začít	k5eAaPmAgMnP
plánovat	plánovat	k5eAaImF
a	a	k8xC
projektovat	projektovat	k5eAaBmF
výpravy	výprava	k1gFnPc4
na	na	k7c4
záchranu	záchrana	k1gFnSc4
křesťanství	křesťanství	k1gNnSc2
–	–	k?
ať	ať	k9
už	už	k6eAd1
skomírající	skomírající	k2eAgFnSc2d1
Byzance	Byzanc	k1gFnSc2
a	a	k8xC
kulturního	kulturní	k2eAgNnSc2d1
střediska	středisko	k1gNnSc2
Konstantinopole	Konstantinopol	k1gInSc2
nebo	nebo	k8xC
rytířských	rytířský	k2eAgInPc2d1
řádů	řád	k1gInPc2
na	na	k7c6
Kypru	Kypr	k1gInSc6
a	a	k8xC
v	v	k7c6
oblasti	oblast	k1gFnSc6
Egejského	egejský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Osmané	Osman	k1gMnPc1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
</s>
<s>
Expanze	expanze	k1gFnSc1
Osmanů	Osman	k1gMnPc2
do	do	k7c2
Evropy	Evropa	k1gFnSc2
začala	začít	k5eAaPmAgFnS
roku	rok	k1gInSc2
1354	#num#	k4
dobytím	dobytí	k1gNnSc7
Gallipoli	Gallipoli	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1361	#num#	k4
dobyl	dobýt	k5eAaPmAgInS
Adrianopol	Adrianopol	k1gInSc1
Murad	Murad	k1gInSc4
I.	I.	kA
<g/>
,	,	kIx,
přejmenoval	přejmenovat	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
na	na	k7c6
Edirne	Edirn	k1gInSc5
a	a	k8xC
učinil	učinit	k5eAaPmAgMnS,k5eAaImAgMnS
z	z	k7c2
ní	on	k3xPp3gFnSc2
sídlo	sídlo	k1gNnSc4
říše	říš	k1gFnSc2
Osmanů	Osman	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Papežské	papežský	k2eAgInPc4d1
projekty	projekt	k1gInPc4
v	v	k7c6
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Projekt	projekt	k1gInSc1
Evžena	Evžen	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
z	z	k7c2
let	léto	k1gNnPc2
1444	#num#	k4
<g/>
–	–	k?
<g/>
1445	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Na	na	k7c6
jihovýchodě	jihovýchod	k1gInSc6
Osmanské	osmanský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
povstal	povstat	k5eAaPmAgMnS
vládce	vládce	k1gMnSc1
Karamanu	Karaman	k1gInSc2
Ibrahim	Ibrahim	k1gMnSc1
bej	bej	k1gMnSc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
přinutilo	přinutit	k5eAaPmAgNnS
osmanského	osmanský	k2eAgInSc2d1
vládce	vládce	k1gMnSc1
Murada	Murada	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
k	k	k7c3
tažení	tažení	k1gNnSc3
právě	právě	k9
do	do	k7c2
této	tento	k3xDgFnSc2
oblasti	oblast	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Této	tento	k3xDgFnSc2
situace	situace	k1gFnSc2
chtěl	chtít	k5eAaImAgMnS
využít	využít	k5eAaPmF
papež	papež	k1gMnSc1
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
zablokoval	zablokovat	k5eAaPmAgMnS
Dardanely	Dardanely	k1gFnPc4
loďstvem	loďstvo	k1gNnSc7
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
by	by	kYmCp3nP
znemožnil	znemožnit	k5eAaPmAgInS
pohyb	pohyb	k1gInSc4
osmanského	osmanský	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
–	–	k?
poté	poté	k6eAd1
by	by	kYmCp3nP
stačilo	stačit	k5eAaBmAgNnS
zahájit	zahájit	k5eAaPmF
operace	operace	k1gFnPc4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
spojenci	spojenec	k1gMnPc7
<g/>
,	,	kIx,
osmanskými	osmanský	k2eAgMnPc7d1
sousedními	sousední	k2eAgMnPc7d1
panovníky	panovník	k1gMnPc7
(	(	kIx(
<g/>
byzantským	byzantský	k2eAgMnSc7d1
Konstantinem	Konstantin	k1gMnSc7
XI	XI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Palaiologem	Palaiolog	k1gMnSc7
<g/>
,	,	kIx,
albánským	albánský	k2eAgMnSc7d1
Skanderbegem	Skanderbeg	k1gMnSc7
<g/>
,	,	kIx,
srbským	srbský	k2eAgMnSc7d1
Jiřím	Jiří	k1gMnSc7
Brankovićem	Branković	k1gMnSc7
<g/>
,	,	kIx,
především	především	k9
však	však	k9
uherským	uherský	k2eAgMnSc7d1
Janem	Jan	k1gMnSc7
Hunyadim	Hunyadi	k1gNnSc7
a	a	k8xC
poté	poté	k6eAd1
polským	polský	k2eAgMnSc7d1
a	a	k8xC
uherským	uherský	k2eAgMnSc7d1
králem	král	k1gMnSc7
Vladislavem	Vladislav	k1gMnSc7
–	–	k?
papež	papež	k1gMnSc1
měl	mít	k5eAaImAgMnS
i	i	k8xC
další	další	k2eAgMnPc4d1
spojence	spojenec	k1gMnPc4
<g/>
:	:	kIx,
Benátské	benátský	k2eAgNnSc4d1
loďstvo	loďstvo	k1gNnSc4
a	a	k8xC
Burgunďany	Burgunďan	k1gMnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Papež	Papež	k1gMnSc1
jmenoval	jmenovat	k5eAaImAgMnS,k5eAaBmAgMnS
dva	dva	k4xCgInPc4
legáty	legát	k1gInPc4
<g/>
:	:	kIx,
pro	pro	k7c4
pozemní	pozemní	k2eAgFnPc4d1
síly	síla	k1gFnPc4
Giuliana	Giulian	k1gMnSc2
Cesariniho	Cesarini	k1gMnSc2
a	a	k8xC
pro	pro	k7c4
loďstvo	loďstvo	k1gNnSc4
svého	svůj	k3xOyFgMnSc2
synovce	synovec	k1gMnSc2
Francesca	Francescum	k1gNnSc2
Condulmaria	Condulmarium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgMnSc1
zmiňovaný	zmiňovaný	k2eAgMnSc1d1
měl	mít	k5eAaImAgMnS
vést	vést	k5eAaImF
blokádu	blokáda	k1gFnSc4
Dardanel	Dardanely	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Murad	Murad	k1gInSc1
II	II	kA
<g/>
.	.	kIx.
zlomil	zlomit	k5eAaPmAgMnS
odpor	odpor	k1gInSc4
Ibrahima	Ibrahim	k1gMnSc2
beje	bej	k1gMnSc2
dříve	dříve	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
předpokládalo	předpokládat	k5eAaImAgNnS
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
vydal	vydat	k5eAaPmAgInS
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
armádou	armáda	k1gFnSc7
zpět	zpět	k6eAd1
do	do	k7c2
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
kam	kam	k6eAd1
dorazil	dorazit	k5eAaPmAgMnS
ještě	ještě	k6eAd1
dříve	dříve	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
Condulmario	Condulmario	k1gMnSc1
stačil	stačit	k5eAaBmAgMnS
Dardanely	Dardanely	k1gFnPc4
zablokovat	zablokovat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozemní	pozemní	k2eAgFnPc4d1
síly	síla	k1gFnPc4
vedené	vedený	k2eAgNnSc1d1
Cesarinim	Cesarinim	k1gInSc4
poté	poté	k6eAd1
rozdrtil	rozdrtit	k5eAaPmAgMnS
u	u	k7c2
Varny	Varna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
této	tento	k3xDgFnSc6
porážce	porážka	k1gFnSc6
se	se	k3xPyFc4
pokusil	pokusit	k5eAaPmAgMnS
Condulmario	Condulmario	k6eAd1
o	o	k7c4
další	další	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
1445	#num#	k4
<g/>
,	,	kIx,
ta	ten	k3xDgFnSc1
však	však	k9
byla	být	k5eAaImAgFnS
příliš	příliš	k6eAd1
zdlouhavá	zdlouhavý	k2eAgFnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
se	se	k3xPyFc4
raději	rád	k6eAd2
stáhl	stáhnout	k5eAaPmAgMnS
z	z	k7c2
osmanského	osmanský	k2eAgNnSc2d1
území	území	k1gNnSc2
před	před	k7c7
přicházející	přicházející	k2eAgFnSc7d1
zimou	zima	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Balkán	Balkán	k1gInSc1
a	a	k8xC
okolí	okolí	k1gNnSc1
v	v	k7c6
roce	rok	k1gInSc6
1400	#num#	k4
AD	ad	k7c4
</s>
<s>
Projekt	projekt	k1gInSc1
Kalixta	Kalixt	k1gInSc2
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
z	z	k7c2
let	léto	k1gNnPc2
1455	#num#	k4
<g/>
–	–	k?
<g/>
1458	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Roku	rok	k1gInSc2
1453	#num#	k4
dobyli	dobýt	k5eAaPmAgMnP
Osmané	Osman	k1gMnPc1
Konstantinopol	Konstantinopol	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osmané	Osman	k1gMnPc1
tak	tak	k9
mohli	moct	k5eAaImAgMnP
přecházet	přecházet	k5eAaImF
mezi	mezi	k7c7
částmi	část	k1gFnPc7
říše	říš	k1gFnSc2
nejen	nejen	k6eAd1
přes	přes	k7c4
Dardanely	Dardanely	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
přes	přes	k7c4
Bospor	Bospor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Papež	Papež	k1gMnSc1
roku	rok	k1gInSc2
1455	#num#	k4
jmenoval	jmenovat	k5eAaBmAgMnS,k5eAaImAgMnS
legátem	legát	k1gMnSc7
Lodovica	Lodovic	k1gInSc2
Trevisana	Trevisana	k1gFnSc1
a	a	k8xC
svěřil	svěřit	k5eAaPmAgInS
mu	on	k3xPp3gMnSc3
papežské	papežský	k2eAgNnSc1d1
loďstvo	loďstvo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trevisano	Trevisana	k1gFnSc5
měl	mít	k5eAaImAgInS
za	za	k7c4
úkol	úkol	k1gInSc4
napadnout	napadnout	k5eAaPmF
asijskou	asijský	k2eAgFnSc4d1
část	část	k1gFnSc4
Osmanské	osmanský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
z	z	k7c2
ostrova	ostrov	k1gInSc2
Rhodos	Rhodos	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
evropské	evropský	k2eAgFnSc6d1
pevnině	pevnina	k1gFnSc6
operovali	operovat	k5eAaImAgMnP
Jan	Jan	k1gMnSc1
Hunyadi	Hunyad	k1gMnPc1
a	a	k8xC
kardinál	kardinál	k1gMnSc1
Juan	Juan	k1gMnSc1
Carvajal	Carvajal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trevisano	Trevisana	k1gFnSc5
však	však	k9
nedokázal	dokázat	k5eNaPmAgMnS
donutit	donutit	k5eAaPmF
svými	svůj	k3xOyFgInPc7
útoky	útok	k1gInPc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
Osmané	Osman	k1gMnPc1
přesunuli	přesunout	k5eAaPmAgMnP
část	část	k1gFnSc4
svého	svůj	k3xOyFgNnSc2
vojska	vojsko	k1gNnSc2
do	do	k7c2
Anatolie	Anatolie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Papež	Papež	k1gMnSc1
Kalixt	Kalixt	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
zemřel	zemřít	k5eAaPmAgMnS
roku	rok	k1gInSc2
1458	#num#	k4
a	a	k8xC
tím	ten	k3xDgNnSc7
skončily	skončit	k5eAaPmAgInP
i	i	k9
tyto	tento	k3xDgFnPc4
snahy	snaha	k1gFnPc4
rozdělit	rozdělit	k5eAaPmF
osmanské	osmanský	k2eAgNnSc4d1
vojsko	vojsko	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
98	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Projekt	projekt	k1gInSc1
Pia	Pius	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
z	z	k7c2
let	léto	k1gNnPc2
1463	#num#	k4
<g/>
–	–	k?
<g/>
1464	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Roku	rok	k1gInSc2
1463	#num#	k4
se	se	k3xPyFc4
papež	papež	k1gMnSc1
rozhodl	rozhodnout	k5eAaPmAgMnS
postavit	postavit	k5eAaPmF
se	se	k3xPyFc4
do	do	k7c2
čela	čelo	k1gNnSc2
výpravy	výprava	k1gFnSc2
proti	proti	k7c3
Osmanům	Osman	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počítal	počítat	k5eAaImAgMnS
přitom	přitom	k6eAd1
s	s	k7c7
pomocí	pomoc	k1gFnSc7
spojenců	spojenec	k1gMnPc2
na	na	k7c6
evropské	evropský	k2eAgFnSc6d1
pevnině	pevnina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uherský	uherský	k2eAgMnSc1d1
král	král	k1gMnSc1
Matyáš	Matyáš	k1gMnSc1
Korvín	Korvín	k1gMnSc1
měl	mít	k5eAaImAgMnS
zaútočit	zaútočit	k5eAaPmF
ze	z	k7c2
severu	sever	k1gInSc2
na	na	k7c4
Osmanskou	osmanský	k2eAgFnSc4d1
říši	říše	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
tehdy	tehdy	k6eAd1
vládl	vládnout	k5eAaImAgMnS
sultán	sultán	k1gMnSc1
Mehmed	Mehmed	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
buď	buď	k8xC
přes	přes	k7c4
dunajskou	dunajský	k2eAgFnSc4d1
nížinu	nížina	k1gFnSc4
nebo	nebo	k8xC
přes	přes	k7c4
Niš	Niš	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
západě	západ	k1gInSc6
měl	mít	k5eAaImAgMnS
zaútočit	zaútočit	k5eAaPmF
albánský	albánský	k2eAgMnSc1d1
vládce	vládce	k1gMnSc1
Skanderbeg	Skanderbeg	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Papež	Papež	k1gMnSc1
měl	mít	k5eAaImAgMnS
poté	poté	k6eAd1
zaútočit	zaútočit	k5eAaPmF
z	z	k7c2
jihu	jih	k1gInSc2
–	–	k?
z	z	k7c2
Peloponésu	Peloponés	k1gInSc2
–	–	k?
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
spojenci	spojenec	k1gMnPc7
z	z	k7c2
Burgundska	Burgundsko	k1gNnSc2
a	a	k8xC
s	s	k7c7
dalšími	další	k2eAgMnPc7d1
evropskými	evropský	k2eAgMnPc7d1
křižáky	křižák	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
nimi	on	k3xPp3gInPc7
se	se	k3xPyFc4
měl	mít	k5eAaImAgInS
setkat	setkat	k5eAaPmF
v	v	k7c6
Anconě	Ancon	k1gInSc6
a	a	k8xC
vyplout	vyplout	k5eAaPmF
na	na	k7c4
Peloponés	Peloponés	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Papežské	papežský	k2eAgInPc1d1
a	a	k8xC
benátské	benátský	k2eAgNnSc1d1
loďstvo	loďstvo	k1gNnSc1
poté	poté	k6eAd1
mělo	mít	k5eAaImAgNnS
operovat	operovat	k5eAaImF
v	v	k7c6
Egejském	egejský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1464	#num#	k4
však	však	k9
Pius	Pius	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
skonal	skonat	k5eAaPmAgMnS
v	v	k7c6
Anconě	Ancon	k1gInSc6
a	a	k8xC
tento	tento	k3xDgInSc1
plán	plán	k1gInSc1
se	se	k3xPyFc4
tedy	tedy	k9
nemohl	moct	k5eNaImAgMnS
uskutečnit	uskutečnit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
99	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
evropském	evropský	k2eAgInSc6d1
kontinentě	kontinent	k1gInSc6
</s>
<s>
Papež	Papež	k1gMnSc1
Inocent	Inocent	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
exkomunikuje	exkomunikovat	k5eAaBmIp3nS
katary	katar	k1gInPc4
z	z	k7c2
jižní	jižní	k2eAgFnSc2d1
Francie	Francie	k1gFnSc2
(	(	kIx(
<g/>
vlevo	vlevo	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Masakr	masakr	k1gInSc1
katarů	katar	k1gMnPc2
křižáky	křižák	k1gInPc7
</s>
<s>
Křížové	Křížové	k2eAgFnPc1d1
výpravy	výprava	k1gFnPc1
na	na	k7c6
evropském	evropský	k2eAgInSc6d1
kontinentě	kontinent	k1gInSc6
byly	být	k5eAaImAgFnP
dvojí	dvojí	k4xRgFnPc1
povahy	povaha	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednak	jednak	k8xC
to	ten	k3xDgNnSc1
byly	být	k5eAaImAgFnP
tažení	tažení	k1gNnSc4
proti	proti	k7c3
pohanům	pohan	k1gMnPc3
<g/>
,	,	kIx,
a	a	k8xC
jednak	jednak	k8xC
proti	proti	k7c3
křesťanským	křesťanský	k2eAgMnPc3d1
kacířům	kacíř	k1gMnPc3
<g/>
,	,	kIx,
kam	kam	k6eAd1
patří	patřit	k5eAaImIp3nS
i	i	k9
křížové	křížový	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
proti	proti	k7c3
„	„	k?
<g/>
neposlušným	poslušný	k2eNgFnPc3d1
<g/>
“	“	k?
panovníkům	panovník	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s>
Proti	proti	k7c3
pohanům	pohan	k1gMnPc3
–	–	k?
severní	severní	k2eAgFnSc2d1
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
</s>
<s>
Venedská	Venedský	k2eAgFnSc1d1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
</s>
<s>
Výpravy	výprava	k1gFnPc4
proti	proti	k7c3
pohanům	pohan	k1gMnPc3
žijícím	žijící	k2eAgFnPc3d1
u	u	k7c2
Baltského	baltský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
účastnili	účastnit	k5eAaImAgMnP
rytíři	rytíř	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
odmítli	odmítnout	k5eAaPmAgMnP
táhnout	táhnout	k5eAaImF
s	s	k7c7
druhou	druhý	k4xOgFnSc7
křížovou	křížový	k2eAgFnSc7d1
výpravou	výprava	k1gFnSc7
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
a	a	k8xC
raději	rád	k6eAd2
v	v	k7c6
polovině	polovina	k1gFnSc6
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
uspořádali	uspořádat	k5eAaPmAgMnP
tažení	tažený	k2eAgMnPc1d1
proti	proti	k7c3
polabským	polabský	k2eAgMnPc3d1
Slovanům	Slovan	k1gMnPc3
a	a	k8xC
do	do	k7c2
Pomořan	Pomořany	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Proti	proti	k7c3
slovanským	slovanský	k2eAgInPc3d1
Obodritům	Obodrit	k1gInPc3
se	se	k3xPyFc4
roku	rok	k1gInSc2
1147	#num#	k4
uskutečnila	uskutečnit	k5eAaPmAgFnS
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
saského	saský	k2eAgMnSc2d1
vévody	vévoda	k1gMnSc2
Jindřicha	Jindřich	k1gMnSc2
Lva	Lev	k1gMnSc2
a	a	k8xC
Knuta	knuta	k1gFnSc1
V.	V.	kA
Dánského	dánský	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s>
Livonská	Livonský	k2eAgFnSc1d1
a	a	k8xC
pruská	pruský	k2eAgFnSc1d1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článcích	článek	k1gInPc6
Pruská	pruský	k2eAgFnSc1d1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
<g/>
,	,	kIx,
Livonská	Livonský	k2eAgFnSc1d1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
a	a	k8xC
Livonský	Livonský	k2eAgInSc1d1
řád	řád	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Křížové	Křížové	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
proti	proti	k7c3
pohanům	pohan	k1gMnPc3
v	v	k7c6
Pobaltí	Pobaltí	k1gNnSc6
na	na	k7c6
počátku	počátek	k1gInSc6
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
měly	mít	k5eAaImAgFnP
trvalý	trvalý	k2eAgInSc4d1
efekt	efekt	k1gInSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
křižáci	křižák	k1gMnPc1
dobytá	dobytý	k2eAgNnPc4d1
území	území	k1gNnPc4
konsolidovali	konsolidovat	k5eAaBmAgMnP
<g/>
,	,	kIx,
když	když	k8xS
ho	on	k3xPp3gMnSc4
dokázali	dokázat	k5eAaPmAgMnP
osídlit	osídlit	k5eAaPmF
<g/>
,	,	kIx,
christianizovat	christianizovat	k5eAaImF
a	a	k8xC
částečně	částečně	k6eAd1
germanizovat	germanizovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
těchto	tento	k3xDgFnPc6
válkách	válka	k1gFnPc6
se	se	k3xPyFc4
angažoval	angažovat	k5eAaBmAgInS
především	především	k9
Řád	řád	k1gInSc1
německých	německý	k2eAgMnPc2d1
rytířů	rytíř	k1gMnPc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
v	v	k7c6
Pobaltí	Pobaltí	k1gNnSc6
založil	založit	k5eAaPmAgInS
vlastní	vlastní	k2eAgInSc1d1
řádový	řádový	k2eAgInSc1d1
stát	stát	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řád	řád	k1gInSc4
německých	německý	k2eAgMnPc2d1
rytířů	rytíř	k1gMnPc2
vedl	vést	k5eAaImAgMnS
od	od	k7c2
roku	rok	k1gInSc2
1226	#num#	k4
války	válka	k1gFnPc4
proti	proti	k7c3
Prusům	Prus	k1gMnPc3
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgMnS
násilně	násilně	k6eAd1
christianizovat	christianizovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křížových	Křížová	k1gFnPc2
výprav	výprava	k1gFnPc2
proti	proti	k7c3
pohanským	pohanský	k2eAgMnPc3d1
Prusům	Prus	k1gMnPc3
a	a	k8xC
Litevcům	Litevec	k1gMnPc3
se	se	k3xPyFc4
zúčastnili	zúčastnit	k5eAaPmAgMnP
také	také	k9
čeští	český	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
Přemysl	Přemysl	k1gMnSc1
Otakar	Otakar	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
ten	ten	k3xDgInSc1
zde	zde	k6eAd1
zakládá	zakládat	k5eAaImIp3nS
r.	r.	kA
1256	#num#	k4
město	město	k1gNnSc1
Královec	Královec	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
Jan	Jan	k1gMnSc1
Lucemburský	lucemburský	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Francouzský	francouzský	k2eAgMnSc1d1
král	král	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
VIII	VIII	kA
<g/>
.	.	kIx.
obléhá	obléhat	k5eAaImIp3nS
Avignon	Avignon	k1gInSc1
<g/>
(	(	kIx(
<g/>
miniatura	miniatura	k1gFnSc1
Jeana	Jean	k1gMnSc2
Fouqueta	Fouquet	k1gMnSc2
<g/>
,	,	kIx,
1455	#num#	k4
<g/>
–	–	k?
<g/>
1460	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Stranou	strana	k1gFnSc7
křížových	křížový	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
nezůstali	zůstat	k5eNaPmAgMnP
ani	ani	k8xC
skandinávští	skandinávský	k2eAgMnPc1d1
vládci	vládce	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
vzdálenost	vzdálenost	k1gFnSc1
jim	on	k3xPp3gMnPc3
ztěžovala	ztěžovat	k5eAaImAgFnS
účast	účast	k1gFnSc1
na	na	k7c6
křížových	křížový	k2eAgFnPc6d1
výpravách	výprava	k1gFnPc6
na	na	k7c4
Blízký	blízký	k2eAgInSc4d1
východ	východ	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Přesto	přesto	k8xC
byli	být	k5eAaImAgMnP
Švédové	Švédová	k1gFnSc3
a	a	k8xC
Norové	Norové	k2eAgInPc7d1
horlivými	horlivý	k2eAgInPc7d1
křižáky	křižák	k1gInPc7
(	(	kIx(
<g/>
král	král	k1gMnSc1
Sigurd	Sigurd	k1gMnSc1
I.	I.	kA
byl	být	k5eAaImAgMnS
prvním	první	k4xOgMnSc7
evropským	evropský	k2eAgMnSc7d1
panovníkem	panovník	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
navštívil	navštívit	k5eAaPmAgMnS
Jeruzalém	Jeruzalém	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Švédské	švédský	k2eAgFnPc1d1
křížové	křížový	k2eAgFnPc1d1
výpravy	výprava	k1gFnPc1
</s>
<s>
Švédové	Švéd	k1gMnPc1
již	již	k6eAd1
na	na	k7c6
počátku	počátek	k1gInSc6
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
podnikli	podniknout	k5eAaPmAgMnP
první	první	k4xOgNnSc4
z	z	k7c2
celkem	celkem	k6eAd1
tří	tři	k4xCgFnPc2
křížových	křížový	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
do	do	k7c2
Finska	Finsko	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
měly	mít	k5eAaImAgInP
za	za	k7c4
následek	následek	k1gInSc4
christianizaci	christianizace	k1gFnSc4
Finů	Fin	k1gMnPc2
<g/>
,	,	kIx,
švédskou	švédský	k2eAgFnSc4d1
kolonizaci	kolonizace	k1gFnSc4
Finska	Finsko	k1gNnSc2
i	i	k8xC
vymezení	vymezení	k1gNnSc2
sféry	sféra	k1gFnSc2
vlivu	vliv	k1gInSc2
mezi	mezi	k7c7
katolickým	katolický	k2eAgNnSc7d1
Švédskem	Švédsko	k1gNnSc7
a	a	k8xC
pravoslavným	pravoslavný	k2eAgInSc7d1
novgorodským	novgorodský	k2eAgInSc7d1
státem	stát	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Proti	proti	k7c3
kacířům	kacíř	k1gMnPc3
</s>
<s>
Albigenská	albigenský	k2eAgFnSc1d1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc7
křížovou	křížový	k2eAgFnSc7d1
výpravou	výprava	k1gFnSc7
vyhlášenou	vyhlášený	k2eAgFnSc7d1
proti	proti	k7c3
evropským	evropský	k2eAgMnPc3d1
kacířům	kacíř	k1gMnPc3
byla	být	k5eAaImAgFnS
Albigenská	albigenský	k2eAgFnSc1d1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trvala	trvat	k5eAaImAgFnS
dvacet	dvacet	k4xCc4
let	léto	k1gNnPc2
(	(	kIx(
<g/>
1209	#num#	k4
<g/>
–	–	k?
<g/>
1229	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
jejím	její	k3xOp3gInSc7
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
vymýtit	vymýtit	k5eAaPmF
katarskou	katarský	k2eAgFnSc4d1
herezi	hereze	k1gFnSc4
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
Francii	Francie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
oběť	oběť	k1gFnSc4
jí	on	k3xPp3gFnSc3
padlo	padnout	k5eAaPmAgNnS,k5eAaImAgNnS
200	#num#	k4
000	#num#	k4
až	až	k9
1	#num#	k4
000	#num#	k4
000	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
100	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
101	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bosenská	bosenský	k2eAgFnSc1d1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1235	#num#	k4
<g/>
–	–	k?
<g/>
1241	#num#	k4
se	se	k3xPyFc4
odehrála	odehrát	k5eAaPmAgFnS
bosenská	bosenský	k2eAgFnSc1d1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
pořádaná	pořádaný	k2eAgFnSc1d1
Uherskem	Uhersko	k1gNnSc7
proti	proti	k7c3
Bosenskému	bosenský	k2eAgInSc3d1
banátu	banát	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedl	vést	k5eAaImAgMnS
ji	on	k3xPp3gFnSc4
Koloman	Koloman	k1gMnSc1
Haličský	haličský	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křižáci	křižák	k1gMnPc1
se	se	k3xPyFc4
však	však	k9
nedostali	dostat	k5eNaPmAgMnP
dál	daleko	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
jen	jen	k9
do	do	k7c2
pohraničí	pohraničí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tou	ten	k3xDgFnSc7
dobou	doba	k1gFnSc7
čelilo	čelit	k5eAaImAgNnS
Uhersko	Uhersko	k1gNnSc1
vpádu	vpád	k1gInSc2
tatarů	tatar	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záminkou	záminka	k1gFnSc7
k	k	k7c3
pořádání	pořádání	k1gNnSc3
výpravy	výprava	k1gFnSc2
bylo	být	k5eAaImAgNnS
hnutí	hnutí	k1gNnSc1
bogomilů	bogomil	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgNnPc2
někteří	některý	k3yIgMnPc1
padli	padnout	k5eAaPmAgMnP,k5eAaImAgMnP
za	za	k7c4
oběť	oběť	k1gFnSc4
při	při	k7c6
násilnostech	násilnost	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Aragonská	aragonský	k2eAgFnSc1d1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1284	#num#	k4
<g/>
–	–	k?
<g/>
1285	#num#	k4
se	se	k3xPyFc4
odehrála	odehrát	k5eAaPmAgFnS
Aragonská	aragonský	k2eAgFnSc1d1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Papež	Papež	k1gMnSc1
Martin	Martin	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
ji	on	k3xPp3gFnSc4
vyhlásil	vyhlásit	k5eAaPmAgMnS
proti	proti	k7c3
exkomunikovanému	exkomunikovaný	k2eAgMnSc3d1
aragonskému	aragonský	k2eAgMnSc3d1
králi	král	k1gMnSc3
Petrovi	Petr	k1gMnSc3
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křížová	Křížová	k1gFnSc1
výprava	výprava	k1gFnSc1
byla	být	k5eAaImAgFnS
jen	jen	k9
součástí	součást	k1gFnSc7
vleklé	vleklý	k2eAgFnSc2d1
války	válka	k1gFnSc2
zvané	zvaný	k2eAgFnSc2d1
sicilské	sicilský	k2eAgFnSc2d1
nešpory	nešpora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Papež	Papež	k1gMnSc1
udělil	udělit	k5eAaPmAgMnS
Petrem	Petr	k1gMnSc7
dobytou	dobytý	k2eAgFnSc4d1
Sicílii	Sicílie	k1gFnSc4
v	v	k7c4
léno	léno	k1gNnSc4
Karlovi	Karel	k1gMnSc3
z	z	k7c2
Anjou	Anjý	k2eAgFnSc7d1
<g/>
,	,	kIx,
a	a	k8xC
ten	ten	k3xDgMnSc1
vytáhl	vytáhnout	k5eAaPmAgMnS
proti	proti	k7c3
Petrovi	Petr	k1gMnSc3
s	s	k7c7
francouzským	francouzský	k2eAgNnSc7d1
vojskem	vojsko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzi	Francouz	k1gMnPc1
však	však	k9
byli	být	k5eAaImAgMnP
poraženi	poražen	k2eAgMnPc1d1
a	a	k8xC
Sicílie	Sicílie	k1gFnSc1
zůstala	zůstat	k5eAaPmAgFnS
pod	pod	k7c7
vládou	vláda	k1gFnSc7
aragonských	aragonský	k2eAgMnPc2d1
králů	král	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Italský	italský	k2eAgMnSc1d1
válečník	válečník	k1gMnSc1
Pippo	Pippa	k1gFnSc5
z	z	k7c2
Ozory	ozora	k1gMnSc2
bojoval	bojovat	k5eAaImAgMnS
ve	v	k7c6
službách	služba	k1gFnPc6
císaře	císař	k1gMnSc2
Zikmunda	Zikmund	k1gMnSc2
proti	proti	k7c3
Turkům	Turek	k1gMnPc3
i	i	k8xC
proti	proti	k7c3
husitům	husita	k1gMnPc3
</s>
<s>
Čtyři	čtyři	k4xCgFnPc1
křížové	křížový	k2eAgFnPc1d1
výpravy	výprava	k1gFnPc1
proti	proti	k7c3
husitům	husita	k1gMnPc3
</s>
<s>
Posledními	poslední	k2eAgFnPc7d1
kruciátami	kruciáta	k1gFnPc7
byly	být	k5eAaImAgFnP
čtyři	čtyři	k4xCgFnPc4
křížové	křížový	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
proti	proti	k7c3
husitům	husita	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
proti	proti	k7c3
kališnickým	kališnický	k2eAgMnPc3d1
Čechům	Čech	k1gMnPc3
byla	být	k5eAaImAgNnP
</s>
<s>
svolána	svolán	k2eAgFnSc1d1
roku	rok	k1gInSc2
1420	#num#	k4
papežem	papež	k1gMnSc7
Martinem	Martin	k1gMnSc7
V.	V.	kA
<g/>
[	[	kIx(
<g/>
102	#num#	k4
<g/>
]	]	kIx)
Tažení	tažení	k1gNnSc1
bylo	být	k5eAaImAgNnS
veřejně	veřejně	k6eAd1
vyhlášeno	vyhlásit	k5eAaPmNgNnS
17	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1420	#num#	k4
ve	v	k7c6
Vratislavi	Vratislav	k1gFnSc6
a	a	k8xC
velení	velení	k1gNnSc6
se	se	k3xPyFc4
chopil	chopit	k5eAaPmAgMnS
římsko-německý	římsko-německý	k2eAgMnSc1d1
císař	císař	k1gMnSc1
Zikmund	Zikmund	k1gMnSc1
Lucemburský	lucemburský	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dubnu	duben	k1gInSc6
se	se	k3xPyFc4
30	#num#	k4
000	#num#	k4
křižáků	křižák	k1gInPc2
<g/>
[	[	kIx(
<g/>
102	#num#	k4
<g/>
]	]	kIx)
shromáždilo	shromáždit	k5eAaPmAgNnS
ve	v	k7c6
slezské	slezský	k2eAgFnSc6d1
Svídnici	Svídnice	k1gFnSc6
a	a	k8xC
poté	poté	k6eAd1
vstoupilo	vstoupit	k5eAaPmAgNnS
do	do	k7c2
Čech	Čechy	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křižáci	křižák	k1gMnPc1
3	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
obsadili	obsadit	k5eAaPmAgMnP
Hradec	Hradec	k1gInSc4
Králové	Králová	k1gFnSc2
a	a	k8xC
7	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
získali	získat	k5eAaPmAgMnP
Pražský	pražský	k2eAgInSc4d1
hrad	hrad	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
102	#num#	k4
<g/>
]	]	kIx)
Pražané	Pražan	k1gMnPc1
však	však	k9
odmítli	odmítnout	k5eAaPmAgMnP
kapitulovat	kapitulovat	k5eAaBmF
a	a	k8xC
povolali	povolat	k5eAaPmAgMnP
na	na	k7c4
pomoc	pomoc	k1gFnSc4
venkovské	venkovský	k2eAgFnSc2d1
husity	husita	k1gMnSc2
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Jana	Jan	k1gMnSc2
Žižky	Žižka	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
20	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
dorazil	dorazit	k5eAaPmAgMnS
do	do	k7c2
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červnu	červen	k1gInSc6
křižáci	křižák	k1gMnPc1
zahájili	zahájit	k5eAaPmAgMnP
útoky	útok	k1gInPc4
na	na	k7c4
opevněné	opevněný	k2eAgFnPc4d1
pozice	pozice	k1gFnPc4
husitů	husita	k1gMnPc2
na	na	k7c6
Vítkově	Vítkov	k1gInSc6
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gInPc1
výpady	výpad	k1gInPc1
však	však	k9
byly	být	k5eAaImAgInP
vždy	vždy	k6eAd1
odraženy	odražen	k2eAgInPc4d1
<g/>
.	.	kIx.
19	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
vypukl	vypuknout	k5eAaPmAgMnS
v	v	k7c6
táboře	tábor	k1gInSc6
křižáků	křižák	k1gInPc2
požár	požár	k1gInSc1
a	a	k8xC
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
se	se	k3xPyFc4
rozpadla	rozpadnout	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
102	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
neúspěchu	neúspěch	k1gInSc6
první	první	k4xOgFnSc2
výpravy	výprava	k1gFnSc2
němečtí	německý	k2eAgMnPc1d1
kurfiřti	kurfiřt	k1gMnPc1
svolali	svolat	k5eAaPmAgMnP
do	do	k7c2
Chebu	Cheb	k1gInSc2
na	na	k7c4
23	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
druhou	druhý	k4xOgFnSc4
křížovou	křížový	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
proti	proti	k7c3
husitům	husita	k1gMnPc3
a	a	k8xC
28	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1421	#num#	k4
vstoupili	vstoupit	k5eAaPmAgMnP
křižáci	křižák	k1gMnPc1
do	do	k7c2
Čech	Čechy	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
vlně	vlna	k1gFnSc6
velel	velet	k5eAaImAgMnS
falckrabě	falckrabě	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
dobyl	dobýt	k5eAaPmAgInS
hrad	hrad	k1gInSc4
Mašťov	Mašťov	k1gInSc1
<g/>
[	[	kIx(
<g/>
103	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
u	u	k7c2
Kadaně	Kadaň	k1gFnSc2
se	se	k3xPyFc4
spojil	spojit	k5eAaPmAgMnS
s	s	k7c7
druhou	druhý	k4xOgFnSc7
křižáckou	křižácký	k2eAgFnSc7d1
vlnou	vlna	k1gFnSc7
vedenou	vedený	k2eAgFnSc4d1
arcibiskupy	arcibiskup	k1gMnPc4
z	z	k7c2
Kolína	Kolín	k1gInSc2
nad	nad	k7c7
Rýnem	Rýn	k1gInSc7
a	a	k8xC
Trevíru	Trevír	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křižáci	křižák	k1gMnPc1
dobyli	dobýt	k5eAaPmAgMnP
Kadaň	Kadaň	k1gFnSc4
a	a	k8xC
poté	poté	k6eAd1
část	část	k1gFnSc1
výpravy	výprava	k1gFnSc2
obsadila	obsadit	k5eAaPmAgFnS
i	i	k9
Chomutov	Chomutov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInSc1d1
voj	voj	k1gInSc1
táhl	táhnout	k5eAaImAgInS
na	na	k7c4
Žatec	Žatec	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
obležen	oblehnout	k5eAaPmNgInS
10	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
1421	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žatec	Žatec	k1gInSc1
vzdoroval	vzdorovat	k5eAaImAgInS
a	a	k8xC
mezi	mezi	k7c7
křižáky	křižák	k1gMnPc7
se	se	k3xPyFc4
začaly	začít	k5eAaPmAgInP
množit	množit	k5eAaImF
spory	spor	k1gInPc1
a	a	k8xC
docházet	docházet	k5eAaImF
zásoby	zásoba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
vypukl	vypuknout	k5eAaPmAgInS
v	v	k7c6
ležení	ležení	k1gNnSc6
požár	požár	k1gInSc4
a	a	k8xC
zvěsti	zvěst	k1gFnPc4
o	o	k7c6
postupu	postup	k1gInSc6
husitských	husitský	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
na	na	k7c4
pomoc	pomoc	k1gFnSc4
Žatci	Žatec	k1gInSc6
vyvolaly	vyvolat	k5eAaPmAgInP
paniku	panika	k1gFnSc4
<g/>
,	,	kIx,
takže	takže	k8xS
se	s	k7c7
2	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
křižáci	křižák	k1gMnPc1
rozutekli	rozutéct	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
103	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Papežský	papežský	k2eAgMnSc1d1
legát	legát	k1gMnSc1
Jindřich	Jindřich	k1gMnSc1
Beaufort	Beaufort	k1gInSc4
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nP
zastavit	zastavit	k5eAaPmF
prchající	prchající	k2eAgInPc4d1
křižáky	křižák	k1gInPc4
po	po	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Tachova	Tachov	k1gInSc2
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1427	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
Norimberku	Norimberk	k1gInSc6
shromáždila	shromáždit	k5eAaPmAgFnS
vojska	vojsko	k1gNnPc4
německých	německý	k2eAgMnPc2d1
šlechticů	šlechtic	k1gMnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
vyrazila	vyrazit	k5eAaPmAgFnS
na	na	k7c4
třetí	třetí	k4xOgFnSc4
křížovou	křížový	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
do	do	k7c2
českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrchním	vrchní	k1gMnSc7
velitelem	velitel	k1gMnSc7
přibližně	přibližně	k6eAd1
25	#num#	k4
000	#num#	k4
ozbrojenců	ozbrojenec	k1gMnPc2
<g/>
[	[	kIx(
<g/>
104	#num#	k4
<g/>
]	]	kIx)
byl	být	k5eAaImAgMnS
zvolen	zvolen	k2eAgMnSc1d1
trevírský	trevírský	k2eAgMnSc1d1
biskup	biskup	k1gMnSc1
Ota	Ota	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
23	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
oblehl	oblehnout	k5eAaPmAgMnS
Stříbro	stříbro	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Husitští	husitský	k2eAgMnPc1d1
předáci	předák	k1gMnPc1
se	se	k3xPyFc4
dohodli	dohodnout	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
Stříbru	stříbro	k1gNnSc3
pomohou	pomoct	k5eAaPmIp3nP
a	a	k8xC
husité	husita	k1gMnPc1
se	se	k3xPyFc4
začali	začít	k5eAaPmAgMnP
shromažďovat	shromažďovat	k5eAaImF
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celek	k1gInSc7
se	se	k3xPyFc4
sešlo	sejít	k5eAaPmAgNnS
až	až	k9
15	#num#	k4
000	#num#	k4
pěšců	pěšec	k1gMnPc2
<g/>
,	,	kIx,
1500	#num#	k4
jezdců	jezdec	k1gMnPc2
a	a	k8xC
200	#num#	k4
vozů	vůz	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
104	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
druhé	druhý	k4xOgFnSc6
půli	půle	k1gFnSc6
července	červenec	k1gInSc2
se	se	k3xPyFc4
husité	husita	k1gMnPc1
dali	dát	k5eAaPmAgMnP
na	na	k7c4
pochod	pochod	k1gInSc4
ke	k	k7c3
Stříbru	stříbro	k1gNnSc3
a	a	k8xC
křižáci	křižák	k1gMnPc1
jim	on	k3xPp3gFnPc3
počátkem	počátkem	k7c2
srpna	srpen	k1gInSc2
vyslali	vyslat	k5eAaPmAgMnP
naproti	naproti	k6eAd1
3000	#num#	k4
<g/>
[	[	kIx(
<g/>
104	#num#	k4
<g/>
]	]	kIx)
jezdců	jezdec	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
však	však	k9
neodvážili	odvážit	k5eNaPmAgMnP
husity	husita	k1gMnPc4
napadnout	napadnout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
zbabělost	zbabělost	k1gFnSc1
vyvolala	vyvolat	k5eAaPmAgFnS
strach	strach	k1gInSc4
ze	z	k7c2
síly	síla	k1gFnSc2
nepřítele	nepřítel	k1gMnSc2
<g/>
,	,	kIx,
takže	takže	k8xS
se	se	k3xPyFc4
křižácké	křižácký	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
místo	místo	k1gNnSc1
bitvy	bitva	k1gFnSc2
dalo	dát	k5eAaPmAgNnS
v	v	k7c6
panice	panika	k1gFnSc6
na	na	k7c4
útěk	útěk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1431	#num#	k4
byla	být	k5eAaImAgFnS
svolána	svolat	k5eAaPmNgFnS
další	další	k2eAgFnSc1d1
mnohonárodnostní	mnohonárodnostní	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
proti	proti	k7c3
kališníkům	kališník	k1gMnPc3
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gNnSc7
velením	velení	k1gNnSc7
byl	být	k5eAaImAgInS
pověřen	pověřit	k5eAaPmNgMnS
markrabě	markrabě	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
Braniborský	braniborský	k2eAgMnSc1d1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
překročili	překročit	k5eAaPmAgMnP
křižáci	křižák	k1gMnPc1
české	český	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
směrem	směr	k1gInSc7
na	na	k7c4
Tachov	Tachov	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
napadena	napaden	k2eAgFnSc1d1
Plzeň	Plzeň	k1gFnSc1
<g/>
,	,	kIx,
cílem	cíl	k1gInSc7
se	se	k3xPyFc4
však	však	k9
nakonec	nakonec	k6eAd1
staly	stát	k5eAaPmAgFnP
Domažlice	Domažlice	k1gFnPc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
105	#num#	k4
<g/>
]	]	kIx)
jimž	jenž	k3xRgNnPc3
husité	husita	k1gMnPc1
14	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
vyrazili	vyrazit	k5eAaPmAgMnP
na	na	k7c4
pomoc	pomoc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fridrich	Fridrich	k1gMnSc1
dal	dát	k5eAaPmAgMnS
části	část	k1gFnPc4
křižáků	křižák	k1gInPc2
povel	povel	k1gInSc4
k	k	k7c3
přesunu	přesun	k1gInSc3
ke	k	k7c3
Kdyni	Kdyně	k1gFnSc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
vybudována	vybudován	k2eAgFnSc1d1
vozová	vozový	k2eAgFnSc1d1
hradba	hradba	k1gFnSc1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
saská	saský	k2eAgFnSc1d1
část	část	k1gFnSc1
výpravy	výprava	k1gFnSc2
měla	mít	k5eAaImAgFnS
zůstat	zůstat	k5eAaPmF
na	na	k7c6
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Papežský	papežský	k2eAgInSc4d1
legát	legát	k1gInSc4
Giuliano	Giuliana	k1gFnSc5
Cesarini	Cesarin	k2eAgMnPc1d1
vyslal	vyslat	k5eAaPmAgMnS
italskou	italský	k2eAgFnSc4d1
jízdu	jízda	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
chránila	chránit	k5eAaImAgFnS
ustupujícího	ustupující	k2eAgMnSc4d1
Fridricha	Fridrich	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cesty	cesta	k1gFnPc1
však	však	k9
byly	být	k5eAaImAgFnP
úzké	úzký	k2eAgFnPc1d1
<g/>
,	,	kIx,
mezi	mezi	k7c7
křižáky	křižák	k1gInPc7
propukl	propuknout	k5eAaPmAgInS
zmatek	zmatek	k1gInSc1
<g/>
,	,	kIx,
což	což	k3yQnSc4,k3yRnSc4
způsobilo	způsobit	k5eAaPmAgNnS
zdržení	zdržení	k1gNnSc1
a	a	k8xC
husité	husita	k1gMnPc1
dorazili	dorazit	k5eAaPmAgMnP
na	na	k7c4
bojiště	bojiště	k1gNnSc4
dříve	dříve	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
křižáci	křižák	k1gMnPc1
stačili	stačit	k5eAaBmAgMnP
zformovat	zformovat	k5eAaPmF
k	k	k7c3
bitvě	bitva	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
105	#num#	k4
<g/>
]	]	kIx)
Křižáci	křižák	k1gMnPc1
včetně	včetně	k7c2
vůdců	vůdce	k1gMnPc2
a	a	k8xC
kardinála	kardinál	k1gMnSc2
Cesariniho	Cesarini	k1gMnSc2
začali	začít	k5eAaPmAgMnP
utíkat	utíkat	k5eAaImF
a	a	k8xC
vojsko	vojsko	k1gNnSc1
se	se	k3xPyFc4
rozpadlo	rozpadnout	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prchající	prchající	k2eAgFnSc1d1
před	před	k7c7
pronásledováním	pronásledování	k1gNnSc7
ochránila	ochránit	k5eAaPmAgFnS
vozová	vozový	k2eAgFnSc1d1
hradba	hradba	k1gFnSc1
bráněná	bráněný	k2eAgFnSc1d1
italskými	italský	k2eAgMnPc7d1
žoldnéři	žoldnéř	k1gMnPc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
husity	husita	k1gMnPc4
na	na	k7c4
čas	čas	k1gInSc4
zadržela	zadržet	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
křižáků	křižák	k1gInPc2
si	se	k3xPyFc3
tak	tak	k6eAd1
zachránila	zachránit	k5eAaPmAgFnS
holý	holý	k2eAgInSc4d1
život	život	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
husitům	husita	k1gMnPc3
padla	padnout	k5eAaPmAgFnS,k5eAaImAgFnS
do	do	k7c2
rukou	ruka	k1gFnPc2
velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
vojenského	vojenský	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
proti	proti	k7c3
Jiřímu	Jiří	k1gMnSc3
z	z	k7c2
Poděbrad	Poděbrady	k1gInPc2
</s>
<s>
Roku	rok	k1gInSc2
1462	#num#	k4
papež	papež	k1gMnSc1
Pius	Pius	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
zrušil	zrušit	k5eAaPmAgInS
Basilejská	basilejský	k2eAgNnPc4d1
kompaktáta	kompaktáta	k1gNnPc4
a	a	k8xC
uvrhl	uvrhnout	k5eAaPmAgInS
Jiřího	Jiří	k1gMnSc4
z	z	k7c2
Poděbrad	Poděbrady	k1gInPc2
do	do	k7c2
klatby	klatba	k1gFnSc2
<g/>
.	.	kIx.
23	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1466	#num#	k4
vyhlásil	vyhlásit	k5eAaPmAgMnS
papež	papež	k1gMnSc1
Pavel	Pavel	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
zrušení	zrušení	k1gNnSc4
nároku	nárok	k1gInSc2
Jiřího	Jiří	k1gMnSc2
z	z	k7c2
Poděbrad	Poděbrady	k1gInPc2
na	na	k7c4
český	český	k2eAgInSc4d1
trůn	trůn	k1gInSc4
<g/>
,	,	kIx,
kterého	který	k3yQgMnSc4,k3yRgMnSc4,k3yIgMnSc4
označil	označit	k5eAaPmAgMnS
za	za	k7c2
husitského	husitský	k2eAgMnSc2d1
kacíře	kacíř	k1gMnSc2
a	a	k8xC
vyhlásil	vyhlásit	k5eAaPmAgMnS
proti	proti	k7c3
kališníkům	kališník	k1gMnPc3
v	v	k7c6
Čechách	Čechy	k1gFnPc6
novou	nový	k2eAgFnSc4d1
křížovou	křížový	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jaře	jaro	k1gNnSc6
roku	rok	k1gInSc2
1468	#num#	k4
vtrhl	vtrhnout	k5eAaPmAgMnS
v	v	k7c6
čele	čelo	k1gNnSc6
křižáckých	křižácký	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
do	do	k7c2
českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
uherský	uherský	k2eAgMnSc1d1
král	král	k1gMnSc1
Matyáš	Matyáš	k1gMnSc1
Korvín	Korvín	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
soupeřil	soupeřit	k5eAaImAgMnS
s	s	k7c7
Jiřím	Jiří	k1gMnSc7
o	o	k7c4
český	český	k2eAgInSc4d1
trůn	trůn	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matyáš	Matyáš	k1gMnSc1
Korvín	Korvín	k1gMnSc1
byl	být	k5eAaImAgMnS
22	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1469	#num#	k4
vojskem	vojsko	k1gNnSc7
Jiřího	Jiří	k1gMnSc2
z	z	k7c2
Poděbrad	Poděbrady	k1gInPc2
obklíčen	obklíčit	k5eAaPmNgMnS
u	u	k7c2
Vilémova	Vilémův	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matyáš	Matyáš	k1gMnSc1
byl	být	k5eAaImAgMnS
z	z	k7c2
obklíčení	obklíčení	k1gNnSc2
propuštěn	propuštěn	k2eAgMnSc1d1
na	na	k7c6
základě	základ	k1gInSc6
slibu	slib	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
už	už	k6eAd1
nebude	být	k5eNaImBp3nS
nadále	nadále	k6eAd1
usilovat	usilovat	k5eAaImF
o	o	k7c4
českou	český	k2eAgFnSc4d1
korunu	koruna	k1gFnSc4
<g/>
,	,	kIx,
tento	tento	k3xDgInSc4
slib	slib	k1gInSc4
ovšem	ovšem	k9
později	pozdě	k6eAd2
nedodržel	dodržet	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzájemné	vzájemný	k2eAgFnPc4d1
potyčky	potyčka	k1gFnPc4
mezi	mezi	k7c7
oběma	dva	k4xCgMnPc7
panovníky	panovník	k1gMnPc7
trvaly	trvat	k5eAaImAgFnP
až	až	k9
do	do	k7c2
smrti	smrt	k1gFnSc2
Jiřího	Jiří	k1gMnSc2
z	z	k7c2
Poděbrad	Poděbrady	k1gInPc2
22	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
roku	rok	k1gInSc2
1471	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
106	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
107	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Důsledky	důsledek	k1gInPc1
a	a	k8xC
hodnocení	hodnocení	k1gNnSc1
</s>
<s>
Ruský	ruský	k2eAgMnSc1d1
kníže	kníže	k1gMnSc1
Alexandr	Alexandr	k1gMnSc1
Něvský	něvský	k2eAgMnSc1d1
poráží	porážet	k5eAaImIp3nS
v	v	k7c6
bitvě	bitva	k1gFnSc6
na	na	k7c6
Čudském	čudský	k2eAgNnSc6d1
jezeře	jezero	k1gNnSc6
v	v	k7c6
dubnu	duben	k1gInSc6
1242	#num#	k4
Řád	řád	k1gInSc4
německých	německý	k2eAgMnPc2d1
rytířů	rytíř	k1gMnPc2
</s>
<s>
Hodnocení	hodnocení	k1gNnSc1
křížových	křížový	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
jsou	být	k5eAaImIp3nP
velmi	velmi	k6eAd1
rozporuplná	rozporuplný	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Starší	starý	k2eAgFnSc1d2
historiografie	historiografie	k1gFnSc1
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS
jejich	jejich	k3xOp3gInSc4
přínos	přínos	k1gInSc4
pro	pro	k7c4
vzájemnou	vzájemný	k2eAgFnSc4d1
kulturní	kulturní	k2eAgFnSc4d1
výměnu	výměna	k1gFnSc4
mezi	mezi	k7c7
západní	západní	k2eAgFnSc7d1
křesťanskou	křesťanský	k2eAgFnSc7d1
civilizací	civilizace	k1gFnSc7
a	a	k8xC
arabským	arabský	k2eAgInSc7d1
světem	svět	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soudobé	soudobý	k2eAgNnSc4d1
historické	historický	k2eAgNnSc4d1
bádání	bádání	k1gNnSc4
je	být	k5eAaImIp3nS
často	často	k6eAd1
výrazně	výrazně	k6eAd1
skeptičtější	skeptický	k2eAgFnSc1d2
a	a	k8xC
připouští	připouštět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
důsledky	důsledek	k1gInPc4
křížových	křížový	k2eAgFnPc2d1
tažení	tažení	k1gNnSc4
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
pro	pro	k7c4
evropskou	evropský	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
byly	být	k5eAaImAgInP
spíše	spíše	k9
negativní	negativní	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
108	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rytíř-trubadúr	Rytíř-trubadúr	k1gInSc1
Bertran	Bertran	k1gInSc1
de	de	k?
Born	Born	k1gInSc1
<g/>
(	(	kIx(
<g/>
Bibliothè	Bibliothè	k1gFnSc1
Nationale	Nationale	k1gFnSc1
Française	Française	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Středověká	středověký	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
ve	v	k7c6
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
věřila	věřit	k5eAaImAgFnS
církevní	církevní	k2eAgFnSc1d1
propagandě	propaganda	k1gFnSc3
a	a	k8xC
na	na	k7c4
křížové	křížový	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
pohlížela	pohlížet	k5eAaImAgFnS
se	s	k7c7
sympatiemi	sympatie	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zejména	zejména	k6eAd1
s	s	k7c7
rozvojem	rozvoj	k1gInSc7
renesance	renesance	k1gFnSc2
<g/>
,	,	kIx,
humanismu	humanismus	k1gInSc2
a	a	k8xC
osvícenství	osvícenství	k1gNnSc2
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
kritika	kritika	k1gFnSc1
výprav	výprava	k1gFnPc2
jako	jako	k8xC,k8xS
projevů	projev	k1gInPc2
středověkého	středověký	k2eAgInSc2d1
náboženského	náboženský	k2eAgInSc2d1
fanatismu	fanatismus	k1gInSc2
a	a	k8xC
teokracie	teokracie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
s	s	k7c7
jeho	jeho	k3xOp3gInSc7
nacionalismem	nacionalismus	k1gInSc7
a	a	k8xC
kolonialismem	kolonialismus	k1gInSc7
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
romantické	romantický	k2eAgFnSc3d1
představě	představa	k1gFnSc3
o	o	k7c6
křižácích	křižák	k1gMnPc6
jako	jako	k8xC,k8xS
předchůdcích	předchůdce	k1gMnPc6
koloniální	koloniální	k2eAgFnSc1d1
expanze	expanze	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
109	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
tehdejším	tehdejší	k2eAgNnSc6d1
pojetí	pojetí	k1gNnSc6
křížových	křížový	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
se	se	k3xPyFc4
také	také	k6eAd1
odrážely	odrážet	k5eAaImAgFnP
tehdejší	tehdejší	k2eAgInPc4d1
nacionalistické	nacionalistický	k2eAgInPc4d1
spory	spor	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
již	již	k6eAd1
diskuse	diskuse	k1gFnSc2
o	o	k7c6
podílech	podíl	k1gInPc6
jednotlivých	jednotlivý	k2eAgInPc2d1
národů	národ	k1gInPc2
na	na	k7c6
křížových	křížový	k2eAgFnPc6d1
výpravách	výprava	k1gFnPc6
ustoupily	ustoupit	k5eAaPmAgFnP
a	a	k8xC
zkoumá	zkoumat	k5eAaImIp3nS
se	se	k3xPyFc4
ideologie	ideologie	k1gFnSc2
a	a	k8xC
cíle	cíl	k1gInSc2
křížových	křížový	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
110	#num#	k4
<g/>
]	]	kIx)
Zformoval	zformovat	k5eAaPmAgMnS
se	se	k3xPyFc4
i	i	k9
vyhraněně	vyhraněně	k6eAd1
kritický	kritický	k2eAgInSc1d1
pohled	pohled	k1gInSc1
na	na	k7c4
křížové	křížový	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
z	z	k7c2
hlediska	hledisko	k1gNnSc2
marxistů	marxista	k1gMnPc2
a	a	k8xC
po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
také	také	k6eAd1
muslimů	muslim	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Křížové	Kříž	k1gMnPc1
výpravy	výprava	k1gFnSc2
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
značně	značně	k6eAd1
prohloubily	prohloubit	k5eAaPmAgFnP
propast	propast	k1gFnSc4
mezi	mezi	k7c7
západními	západní	k2eAgMnPc7d1
a	a	k8xC
východními	východní	k2eAgMnPc7d1
křesťany	křesťan	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byzantinci	Byzantinec	k1gMnPc7
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
nevzali	vzít	k5eNaPmAgMnP
za	za	k7c4
své	své	k1gNnSc4
<g/>
,	,	kIx,
válku	válka	k1gFnSc4
považovali	považovat	k5eAaImAgMnP
za	za	k7c4
barbarskou	barbarský	k2eAgFnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
111	#num#	k4
<g/>
]	]	kIx)
Nepřátelství	nepřátelství	k1gNnSc4
mezi	mezi	k7c7
ortodoxními	ortodoxní	k2eAgMnPc7d1
Byzantinci	Byzantinec	k1gMnPc7
a	a	k8xC
západními	západní	k2eAgMnPc7d1
katolíky	katolík	k1gMnPc7
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS
dobytím	dobytí	k1gNnSc7
a	a	k8xC
vypleněním	vyplenění	k1gNnSc7
Konstantinopole	Konstantinopol	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1204	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Papež	Papež	k1gMnSc1
účastníky	účastník	k1gMnPc4
této	tento	k3xDgFnSc2
akce	akce	k1gFnSc2
odsoudil	odsoudit	k5eAaPmAgMnS
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
vzniklý	vzniklý	k2eAgInSc1d1
konflikt	konflikt	k1gInSc1
ale	ale	k8xC
uhašen	uhašen	k2eAgMnSc1d1
nebyl	být	k5eNaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prohloubily	prohloubit	k5eAaPmAgInP
se	se	k3xPyFc4
také	také	k9
rozpory	rozpor	k1gInPc1
mezi	mezi	k7c7
křižáky	křižák	k1gInPc7
z	z	k7c2
různých	různý	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
a	a	k8xC
jejich	jejich	k3xOp3gMnPc7
veliteli	velitel	k1gMnPc7
<g/>
,	,	kIx,
například	například	k6eAd1
mezi	mezi	k7c7
Němci	Němec	k1gMnPc7
a	a	k8xC
Francouzi	Francouz	k1gMnPc7
nebo	nebo	k8xC
mezi	mezi	k7c7
panovníky	panovník	k1gMnPc7
Filipem	Filip	k1gMnSc7
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Augustem	August	k1gMnSc7
a	a	k8xC
Richardem	Richard	k1gMnSc7
I.	I.	kA
Lví	lví	k2eAgNnSc1d1
srdce	srdce	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uskutečnění	uskutečnění	k1gNnSc1
křížových	křížový	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
na	na	k7c6
druhé	druhý	k4xOgFnSc6
straně	strana	k1gFnSc6
umožnilo	umožnit	k5eAaPmAgNnS
zformování	zformování	k1gNnSc1
nových	nový	k2eAgInPc2d1
a	a	k8xC
velmi	velmi	k6eAd1
významných	významný	k2eAgFnPc2d1
ekonomických	ekonomický	k2eAgFnPc2d1
vazeb	vazba	k1gFnPc2
mezi	mezi	k7c7
Západem	západ	k1gInSc7
a	a	k8xC
Orientem	Orient	k1gInSc7
<g/>
,	,	kIx,
zabezpečovaných	zabezpečovaný	k2eAgMnPc2d1
především	především	k6eAd1
italskými	italský	k2eAgMnPc7d1
městy	město	k1gNnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
112	#num#	k4
<g/>
]	]	kIx)
Ta	ten	k3xDgFnSc1
bohatla	bohatnout	k5eAaImAgFnS
z	z	k7c2
pronájmu	pronájem	k1gInSc2
lodí	loď	k1gFnPc2
a	a	k8xC
půjček	půjčka	k1gFnPc2
křižákům	křižák	k1gMnPc3
a	a	k8xC
získala	získat	k5eAaPmAgFnS
převahu	převaha	k1gFnSc4
ve	v	k7c6
Středomoří	středomoří	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obchod	obchod	k1gInSc1
v	v	k7c6
levantských	levantský	k2eAgNnPc6d1
městech	město	k1gNnPc6
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
přetrval	přetrvat	k5eAaPmAgMnS
i	i	k9
po	po	k7c6
skončení	skončení	k1gNnSc6
výprav	výprava	k1gFnPc2
<g/>
,	,	kIx,
však	však	k9
pro	pro	k7c4
ně	on	k3xPp3gMnPc4
nebyl	být	k5eNaImAgInS
tak	tak	k6eAd1
důležitý	důležitý	k2eAgInSc1d1
jako	jako	k8xC,k8xS
vazba	vazba	k1gFnSc1
na	na	k7c4
Egypt	Egypt	k1gInSc4
a	a	k8xC
Byzanc	Byzanc	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
108	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Válka	válka	k1gFnSc1
proti	proti	k7c3
terorismu	terorismus	k1gInSc3
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
označována	označován	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
„	„	k?
<g/>
desátá	desátá	k1gFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
<g/>
“	“	k?
</s>
<s>
Přestože	přestože	k8xS
většina	většina	k1gFnSc1
kulturní	kulturní	k2eAgFnSc2d1
výměny	výměna	k1gFnSc2
mezi	mezi	k7c7
arabským	arabský	k2eAgInSc7d1
a	a	k8xC
křesťanským	křesťanský	k2eAgInSc7d1
světem	svět	k1gInSc7
probíhala	probíhat	k5eAaImAgFnS
v	v	k7c6
době	doba	k1gFnSc6
reconquisty	reconquista	k1gMnSc2
na	na	k7c6
Pyrenejském	pyrenejský	k2eAgInSc6d1
poloostrově	poloostrov	k1gInSc6
<g/>
,	,	kIx,
dostaly	dostat	k5eAaPmAgFnP
se	se	k3xPyFc4
některé	některý	k3yIgFnPc1
plodiny	plodina	k1gFnPc1
do	do	k7c2
Evropy	Evropa	k1gFnSc2
prostřednictvím	prostřednictvím	k7c2
křesťanů	křesťan	k1gMnPc2
na	na	k7c6
Předním	přední	k2eAgInSc6d1
východě	východ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
to	ten	k3xDgNnSc4
například	například	k6eAd1
cukrová	cukrový	k2eAgFnSc1d1
třtina	třtina	k1gFnSc1
a	a	k8xC
poznatky	poznatek	k1gInPc1
o	o	k7c6
výrobě	výroba	k1gFnSc6
cukru	cukr	k1gInSc2
<g/>
,	,	kIx,
látek	látka	k1gFnPc2
i	i	k8xC
skla	sklo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křižáci	křižák	k1gMnPc1
se	se	k3xPyFc4
seznámili	seznámit	k5eAaPmAgMnP
s	s	k7c7
mnoha	mnoho	k4c7
vymoženostmi	vymoženost	k1gFnPc7
životního	životní	k2eAgInSc2d1
stylu	styl	k1gInSc2
Východu	východ	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
osvojili	osvojit	k5eAaPmAgMnP
si	se	k3xPyFc3
je	on	k3xPp3gInPc4
většinou	většinou	k6eAd1
jen	jen	k9
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
se	se	k3xPyFc4
na	na	k7c6
Východě	východ	k1gInSc6
usadili	usadit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Evropě	Evropa	k1gFnSc6
se	se	k3xPyFc4
výdobytky	výdobytek	k1gInPc7
orientální	orientální	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
přejímaly	přejímat	k5eAaImAgFnP
především	především	k9
prostřednictvím	prostřednictvím	k7c2
Byzance	Byzanc	k1gFnSc2
<g/>
,	,	kIx,
Sicílie	Sicílie	k1gFnSc2
a	a	k8xC
Pyrenejského	pyrenejský	k2eAgInSc2d1
poloostrova	poloostrov	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
108	#num#	k4
<g/>
]	]	kIx)
Přesto	přesto	k8xC
však	však	k9
měly	mít	k5eAaImAgFnP
výpravy	výprava	k1gFnPc1
křižáků	křižák	k1gMnPc2
v	v	k7c6
domácím	domácí	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
zaznamenatelný	zaznamenatelný	k2eAgInSc4d1
sociální	sociální	k2eAgInSc4d1
dopad	dopad	k1gInSc4
<g/>
,	,	kIx,
často	často	k6eAd1
je	být	k5eAaImIp3nS
v	v	k7c6
této	tento	k3xDgFnSc6
souvislosti	souvislost	k1gFnSc6
zmiňován	zmiňován	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
dvorské	dvorský	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
literatuře	literatura	k1gFnSc6
např.	např.	kA
rytířské	rytířský	k2eAgFnSc2d1
poezie	poezie	k1gFnSc2
(	(	kIx(
<g/>
francouzské	francouzský	k2eAgFnPc1d1
chansons	chansons	k6eAd1
de	de	k?
geste	gest	k1gInSc5
ad	ad	k7c4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křižácká	křižácký	k2eAgFnSc1d1
ideologie	ideologie	k1gFnSc1
nezanikla	zaniknout	k5eNaPmAgFnS
dobytím	dobytí	k1gNnSc7
Jeruzaléma	Jeruzalém	k1gInSc2
ani	ani	k8xC
pádem	pád	k1gInSc7
Akkonu	Akkon	k1gInSc2
roku	rok	k1gInSc2
1291	#num#	k4
a	a	k8xC
křižácké	křižácký	k2eAgNnSc4d1
hnutí	hnutí	k1gNnSc4
nebylo	být	k5eNaImAgNnS
formálně	formálně	k6eAd1
nikdy	nikdy	k6eAd1
ukončeno	ukončit	k5eAaPmNgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
113	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
hlediska	hledisko	k1gNnSc2
blízkovýchodní	blízkovýchodní	k2eAgFnSc2d1
muslimské	muslimský	k2eAgFnSc2d1
civilizace	civilizace	k1gFnSc2
po	po	k7c6
sobě	se	k3xPyFc3
křižáci	křižák	k1gMnPc1
nezanechali	zanechat	k5eNaPmAgMnP
trvalé	trvalý	k2eAgFnPc4d1
stopy	stopa	k1gFnPc4
a	a	k8xC
byli	být	k5eAaImAgMnP
jen	jen	k9
„	„	k?
<g/>
epizodou	epizoda	k1gFnSc7
na	na	k7c6
okraji	okraj	k1gInSc6
dějin	dějiny	k1gFnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
114	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podle	podle	k7c2
tvrzení	tvrzení	k1gNnSc2
<g/>
,	,	kIx,
že	že	k8xS
současná	současný	k2eAgFnSc1d1
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
a	a	k8xC
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
touží	toužit	k5eAaImIp3nP
získat	získat	k5eAaPmF
moc	moc	k6eAd1
nad	nad	k7c7
zeměmi	zem	k1gFnPc7
Blízkého	blízký	k2eAgInSc2d1
východu	východ	k1gInSc2
<g/>
,	,	kIx,
bývá	bývat	k5eAaImIp3nS
někdy	někdy	k6eAd1
jejich	jejich	k3xOp3gNnSc1
tažení	tažení	k1gNnSc1
označováno	označován	k2eAgNnSc1d1
jako	jako	k8xS,k8xC
„	„	k?
<g/>
desátá	desátá	k1gFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
prý	prý	k9
začala	začít	k5eAaPmAgFnS
roku	rok	k1gInSc2
1983	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
americký	americký	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Ronald	Ronald	k1gMnSc1
Reagan	Reagan	k1gMnSc1
vyslal	vyslat	k5eAaPmAgMnS
jednotky	jednotka	k1gFnPc4
do	do	k7c2
Libanonu	Libanon	k1gInSc2
na	na	k7c4
podporu	podpora	k1gFnSc4
izraelské	izraelský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
tam	tam	k6eAd1
provedla	provést	k5eAaPmAgFnS
invazi	invaze	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
115	#num#	k4
<g/>
]	]	kIx)
Rovněž	rovněž	k9
se	se	k3xPyFc4
někdy	někdy	k6eAd1
jako	jako	k9
„	„	k?
<g/>
desátá	desátá	k1gFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
<g/>
“	“	k?
označuje	označovat	k5eAaImIp3nS
válka	válka	k1gFnSc1
proti	proti	k7c3
terorismu	terorismus	k1gInSc3
prezidenta	prezident	k1gMnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
USA	USA	kA
George	George	k1gFnPc2
W.	W.	kA
Bushe	Bush	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
116	#num#	k4
<g/>
]	]	kIx)
Američané	Američan	k1gMnPc1
a	a	k8xC
jejich	jejich	k3xOp3gMnPc1
spojenci	spojenec	k1gMnPc1
bojující	bojující	k2eAgMnPc1d1
v	v	k7c6
Iráku	Irák	k1gInSc6
a	a	k8xC
Afghánistánu	Afghánistán	k1gInSc6
pak	pak	k6eAd1
často	často	k6eAd1
bývají	bývat	k5eAaImIp3nP
islámskými	islámský	k2eAgInPc7d1
radikály	radikál	k1gInPc7
označováni	označovat	k5eAaImNgMnP
za	za	k7c4
„	„	k?
<g/>
křižáky	křižák	k1gInPc4
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
117	#num#	k4
<g/>
]	]	kIx)
Západ	západ	k1gInSc1
a	a	k8xC
především	především	k6eAd1
údajné	údajný	k2eAgNnSc4d1
spojenectví	spojenectví	k1gNnSc4
sionistů	sionista	k1gMnPc2
a	a	k8xC
křižáků	křižák	k1gMnPc2
podle	podle	k7c2
Usámy	Usáma	k1gFnSc2
bin	bin	k?
Ládina	Ládin	k2eAgFnSc1d1
a	a	k8xC
dalších	další	k2eAgMnPc2d1
muslimů	muslim	k1gMnPc2
vede	vést	k5eAaImIp3nS
křižácké	křižácký	k2eAgNnSc1d1
tažení	tažení	k1gNnSc1
proti	proti	k7c3
islámu	islám	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
118	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Pope	pop	k1gMnSc5
Leo	Leo	k1gMnSc1
X	X	kA
(	(	kIx(
<g/>
1475	#num#	k4
-	-	kIx~
1521	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Luminarium	Luminarium	k1gNnSc1
<g/>
:	:	kIx,
Encyclopedia	Encyclopedium	k1gNnPc1
Project	Projecta	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HROCHOVÁ	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
Věra	Věra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křižáci	křižák	k1gMnPc1
v	v	k7c6
Levantě	Levanta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
25	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Hrochová	Hrochová	k1gFnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
DUGGAN	DUGGAN	kA
<g/>
,	,	kIx,
Alfred	Alfred	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křižácké	křižácký	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Orbis	orbis	k1gInSc1
<g/>
,	,	kIx,
1973	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
19	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Duggan	Duggan	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
BRIDGE	BRIDGE	kA
<g/>
,	,	kIx,
Antony	anton	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křížové	Křížové	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
512	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
25	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Bridge	Bridge	k1gFnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Bridge	Bridge	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
28.1	28.1	k4
2	#num#	k4
Bridge	Bridge	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
27	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KOVAŘÍK	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Meč	meč	k1gInSc4
a	a	k8xC
kříž	kříž	k1gInSc4
<g/>
,	,	kIx,
rytířské	rytířský	k2eAgFnPc1d1
bitvy	bitva	k1gFnPc1
a	a	k8xC
osudy	osud	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
123	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Kovařík	Kovařík	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Bridge	Bridge	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
15	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Duggan	Duggan	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
22.1	22.1	k4
2	#num#	k4
Duggan	Duggana	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
23	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
14	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bridge	Bridge	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
29	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bridge	Bridge	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
32	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
69	#num#	k4
<g/>
↑	↑	k?
Duggan	Duggan	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
26.1	26.1	k4
2	#num#	k4
Bridge	Bridge	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
36	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
↑	↑	k?
Bridge	Bridge	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
35	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
20	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
116	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
47	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
str	str	kA
<g/>
.	.	kIx.
91	#num#	k4
<g/>
–	–	k?
<g/>
92	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Duggan	Duggan	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
63	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
133	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
63	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bridge	Bridge	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
83	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
80	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
111.1	111.1	k4
2	#num#	k4
3	#num#	k4
Duggan	Duggana	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
92	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
112	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
113	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bridge	Bridge	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
112	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bridge	Bridge	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
113.1	113.1	k4
2	#num#	k4
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
114	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Duggan	Duggan	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
95.1	95.1	k4
2	#num#	k4
Bridge	Bridge	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
114	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
116	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bridge	Bridge	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
115.1	115.1	k4
2	#num#	k4
Duggan	Duggana	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
100	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
122	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
241	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
171	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
173.1	173.1	k4
2	#num#	k4
Duggan	Duggana	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
150	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ŠOLLE	ŠOLLE	kA
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
stopách	stopa	k1gFnPc6
přemyslovských	přemyslovský	k2eAgInPc2d1
Děpolticů	Děpoltiec	k1gInPc2
:	:	kIx,
příspěvek	příspěvek	k1gInSc1
ke	k	k7c3
genezi	geneze	k1gFnSc3
města	město	k1gNnSc2
Kouřimě	Kouřim	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7021	#num#	k4
<g/>
-	-	kIx~
<g/>
343	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
64	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
KOLÁČEK	Koláček	k1gMnSc1
<g/>
,	,	kIx,
Luboš	Luboš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křížové	Křížové	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
History	Histor	k1gMnPc7
revue	revue	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Duben	duben	k1gInSc1
2008	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
7	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
52	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1803	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
440	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
175	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Duggan	Duggan	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
170	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
211	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bridge	Bridge	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
171	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
215	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
216.1	216.1	k4
2	#num#	k4
Bridge	Bridge	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
172	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
217	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bridge	Bridge	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
175	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bridge	Bridge	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
176.1	176.1	k4
2	#num#	k4
3	#num#	k4
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
235.1	235.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
KOLÁČEK	Koláček	k1gMnSc1
<g/>
,	,	kIx,
Luboš	Luboš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křížové	Křížové	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
History	Histor	k1gMnPc7
revue	revue	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Duben	duben	k1gInSc1
2008	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
7	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
54	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1803	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
440	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Bridge	Bridge	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
180	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
240.1	240.1	k4
2	#num#	k4
3	#num#	k4
Bridge	Bridge	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
181	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Duggan	Duggan	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
178	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
–	–	k?
<g/>
179	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
243	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Duggan	Duggan	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
183	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Duggan	Duggan	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
184	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bridge	Bridge	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
188	#num#	k4
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
↑	↑	k?
Duggan	Duggan	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
186.1	186.1	k4
2	#num#	k4
Bridge	Bridge	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
189	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
252	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Duggan	Duggan	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
188	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Duggan	Duggan	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
189	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
252	#num#	k4
<g/>
–	–	k?
<g/>
253	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Duggan	Duggan	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
190	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HARLANSSON	HARLANSSON	kA
<g/>
,	,	kIx,
Kali	Kali	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Crusades	Crusades	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1997	#num#	k4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2004-11-24	2004-11-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Bridge	Bridge	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
201	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Duggan	Duggan	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
196	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
JONES	JONES	kA
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
C.	C.	kA
The	The	k1gMnSc1
Crusades	Crusades	k1gMnSc1
<g/>
:	:	kIx,
A	a	k9
Brief	Brief	k1gInSc1
History	Histor	k1gInPc1
(	(	kIx(
<g/>
1095	#num#	k4
<g/>
-	-	kIx~
<g/>
1291	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Acworth	Acworth	k1gInSc1
<g/>
,	,	kIx,
Georgia	Georgia	k1gFnSc1
<g/>
:	:	kIx,
2004	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
RICKARD	RICKARD	kA
<g/>
,	,	kIx,
J.	J.	kA
Eighth	Eighth	k1gInSc1
Crusade	Crusad	k1gInSc5
<g/>
,	,	kIx,
1270	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2001-3-25	2001-3-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Duggan	Duggan	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
203.1	203.1	k4
2	#num#	k4
3	#num#	k4
Duggan	Duggana	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
204	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
269	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bridge	Bridge	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
214	#num#	k4
<g/>
–	–	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
215.1	215.1	k4
2	#num#	k4
Duggan	Duggana	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
205	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
90	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
198	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BROADHURST	BROADHURST	kA
<g/>
,	,	kIx,
Roland	Roland	k1gInSc1
J.C.	J.C.	k1gFnSc1
(	(	kIx(
<g/>
transl	transl	k1gInSc1
<g/>
.	.	kIx.
and	and	k?
ed	ed	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Travels	Travelsa	k1gFnPc2
of	of	k?
Ibn	Ibn	k1gMnSc1
Jubayr	Jubayr	k1gMnSc1
<g/>
:	:	kIx,
being	being	k1gMnSc1
the	the	k?
chronicle	chronicle	k1gInSc1
of	of	k?
a	a	k8xC
mediaeval	mediaevat	k5eAaBmAgMnS,k5eAaImAgMnS,k5eAaPmAgMnS
Spanish	Spanish	k1gMnSc1
Moor	Moor	k1gMnSc1
concerning	concerning	k1gInSc4
his	his	k1gNnSc2
journey	journea	k1gFnSc2
to	ten	k3xDgNnSc1
the	the	k?
Egypt	Egypt	k1gInSc1
of	of	k?
Saladin	Saladina	k1gFnPc2
<g/>
,	,	kIx,
the	the	k?
holy	hola	k1gFnSc2
cities	cities	k1gMnSc1
of	of	k?
Arabia	Arabia	k1gFnSc1
<g/>
,	,	kIx,
Baghdad	Baghdad	k1gInSc1
the	the	k?
city	city	k1gFnSc2
of	of	k?
the	the	k?
Caliphs	Caliphs	k1gInSc1
<g/>
,	,	kIx,
the	the	k?
Latin	Latin	k1gMnSc1
kingdom	kingdom	k1gInSc1
of	of	k?
Jerusalem	Jerusalem	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
the	the	k?
Norman	Norman	k1gMnSc1
kingdom	kingdom	k1gInSc1
of	of	k?
Sicily	Sicila	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Cape	capat	k5eAaImIp3nS
<g/>
,	,	kIx,
1952	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
316	#num#	k4
<g/>
.	.	kIx.
.	.	kIx.
<g/>
↑	↑	k?
Benjamin	Benjamin	k1gMnSc1
Z.	Z.	kA
Kedar	Kedar	k1gMnSc1
<g/>
,	,	kIx,
"	"	kIx"
<g/>
The	The	k1gFnSc1
Subjected	Subjected	k1gMnSc1
Muslims	Muslims	k1gInSc1
of	of	k?
the	the	k?
Frankish	Frankish	k1gInSc1
Levant	Levanta	k1gFnPc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
in	in	k?
Muslims	Muslims	k1gInSc1
Under	Under	k1gMnSc1
Latin	Latin	k1gMnSc1
Rule	rula	k1gFnSc6
<g/>
,	,	kIx,
1100	#num#	k4
<g/>
–	–	k?
<g/>
1300	#num#	k4
<g/>
,	,	kIx,
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
James	James	k1gInSc1
M.	M.	kA
Powell	Powell	k1gInSc1
<g/>
,	,	kIx,
Princeton	Princeton	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
,	,	kIx,
st.	st.	kA
148	#num#	k4
<g/>
;	;	kIx,
reprinted	reprinted	k1gMnSc1
in	in	k?
The	The	k1gMnSc1
Crusades	Crusades	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Essential	Essential	k1gMnSc1
Readings	Readings	k1gInSc1
<g/>
,	,	kIx,
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Thomas	Thomas	k1gMnSc1
F.	F.	kA
Madden	Maddna	k1gFnPc2
<g/>
,	,	kIx,
Blackwell	Blackwella	k1gFnPc2
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
,	,	kIx,
pg	pg	k?
<g/>
.	.	kIx.
244	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kedar	Kedar	k1gMnSc1
quotes	quotes	k1gMnSc1
his	his	k1gNnPc2
numbers	numbers	k6eAd1
from	from	k6eAd1
Joshua	Joshua	k1gMnSc1
Prawer	Prawer	k1gMnSc1
<g/>
,	,	kIx,
Histoire	Histoir	k1gInSc5
du	du	k?
royaume	royaum	k1gInSc5
latin	latina	k1gFnPc2
de	de	k?
Jérusalem	Jérusal	k1gInSc7
<g/>
,	,	kIx,
tr	tr	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
G.	G.	kA
Nahon	Nahon	k1gMnSc1
<g/>
,	,	kIx,
Paris	Paris	k1gMnSc1
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
,	,	kIx,
vol	vol	k6eAd1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
st.	st.	kA
498	#num#	k4
<g/>
,	,	kIx,
568	#num#	k4
<g/>
–	–	k?
<g/>
72	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
104	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HISTORY	HISTORY	kA
OF	OF	kA
CRETE	CRETE	kA
&	&	k?
THE	THE	kA
REGION	region	k1gInSc4
OF	OF	kA
SFAKIA	SFAKIA	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Duggan	Duggan	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
176	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KOLÁČEK	Koláček	k1gMnSc1
<g/>
,	,	kIx,
Luboš	Luboš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křížové	Křížové	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
History	Histor	k1gMnPc7
revue	revue	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Duben	duben	k1gInSc1
2008	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
7	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
55	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1803	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
440	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Bridge	Bridge	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
97	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
102	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Duggan	Duggan	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
114	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
TATE	TATE	kA
<g/>
,	,	kIx,
Georges	Georges	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křižáci	křižák	k1gMnPc1
v	v	k7c6
Orientu	Orient	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovart	Slovart	k1gInSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
126	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Tate	Tate	k1gFnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
275	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ŠKVRŇÁK	ŠKVRŇÁK	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řád	řád	k1gInSc4
německých	německý	k2eAgMnPc2d1
rytířů	rytíř	k1gMnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2006-4-26	2006-4-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SOUKUP	Soukup	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
;	;	kIx,
SVÁTEK	Svátek	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křížové	Křížové	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
v	v	k7c6
pozdním	pozdní	k2eAgInSc6d1
středověku	středověk	k1gInSc6
:	:	kIx,
kapitoly	kapitola	k1gFnSc2
z	z	k7c2
dějin	dějiny	k1gFnPc2
náboženských	náboženský	k2eAgInPc2d1
konfliktů	konflikt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
NLN	NLN	kA
Nakladatelství	nakladatelství	k1gNnPc4
lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
245	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7422	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
55	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Strategie	strategie	k1gFnSc1
války	válka	k1gFnSc2
s	s	k7c7
nevěřícími	nevěřící	k1gMnPc7
<g/>
,	,	kIx,
s.	s.	k?
36	#num#	k4
<g/>
–	–	k?
<g/>
39	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Soukup	Soukup	k1gMnSc1
<g/>
,	,	kIx,
Svátek	Svátek	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Soukup	Soukup	k1gMnSc1
<g/>
,	,	kIx,
Svátek	Svátek	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
39	#num#	k4
<g/>
–	–	k?
<g/>
40	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Soukup	Soukup	k1gMnSc1
<g/>
,	,	kIx,
Svátek	Svátek	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
40	#num#	k4
<g/>
–	–	k?
<g/>
41	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Massacre	Massacr	k1gInSc5
of	of	k?
the	the	k?
Pure	Pure	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Time	Tim	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Duben	duben	k1gInSc1
1961	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
WHITE	WHITE	kA
<g/>
,	,	kIx,
Matthew	Matthew	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Selected	Selected	k1gInSc1
Death	Death	k1gInSc4
Tolls	Tolls	k1gInSc4
for	forum	k1gNnPc2
Wars	Warsa	k1gFnPc2
<g/>
,	,	kIx,
Massacres	Massacresa	k1gFnPc2
and	and	k?
Atrocities	Atrocities	k1gMnSc1
Before	Befor	k1gInSc5
the	the	k?
20	#num#	k4
<g/>
th	th	k?
Century	Centura	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2005	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2006-8	2006-8	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
I.	I.	kA
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
-	-	kIx~
Bitva	bitva	k1gFnSc1
na	na	k7c6
Vítkově	Vítkův	k2eAgFnSc6d1
hoře	hora	k1gFnSc6
(	(	kIx(
<g/>
14.07	14.07	k4
<g/>
.1420	.1420	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
husitství	husitství	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
II	II	kA
<g/>
.	.	kIx.
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
(	(	kIx(
<g/>
1421	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
husitství	husitství	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
III	III	kA
<g/>
.	.	kIx.
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
(	(	kIx(
<g/>
1427	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
husitství	husitství	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
IV	Iva	k1gFnPc2
<g/>
.	.	kIx.
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
-	-	kIx~
Bitva	bitva	k1gFnSc1
u	u	k7c2
Domažlic	Domažlice	k1gFnPc2
(	(	kIx(
<g/>
14.08	14.08	k4
<g/>
.1431	.1431	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
husitství	husitství	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
HARENBERG	HARENBERG	kA
<g/>
,	,	kIx,
Bodo	Bodo	k1gNnSc1
<g/>
,	,	kIx,
Brigitte	Brigitte	k1gFnSc1
Beier	Beier	k1gInSc1
<g/>
,	,	kIx,
Raphaela	Raphaela	k1gFnSc1
Drexhage	Drexhage	k1gFnSc1
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
Erbe	erb	k1gInSc5
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kronika	kronika	k1gFnSc1
lidstva	lidstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetí	třetí	k4xOgInSc1
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Fortuna	Fortuna	k1gFnSc1
Libri	Libr	k1gFnSc2
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
1320	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85873	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
364	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
KALOUS	Kalous	k1gMnSc1
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matyáš	Matyáš	k1gMnSc1
Korvín	Korvín	k1gMnSc1
<g/>
:	:	kIx,
uherský	uherský	k2eAgMnSc1d1
a	a	k8xC
český	český	k2eAgMnSc1d1
král	král	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
:	:	kIx,
Veduta	veduta	k1gFnSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
512	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86829	#num#	k4
<g/>
-	-	kIx~
<g/>
48	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
290	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
286	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
287	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Duggan	Duggan	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
32	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vrcholný	vrcholný	k2eAgInSc1d1
středověk	středověk	k1gInSc1
<g/>
,	,	kIx,
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Petr	Petr	k1gMnSc1
Heller	Heller	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Duggan	Duggan	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
210	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
292	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
O	O	kA
<g/>
'	'	kIx"
<g/>
BRIEN	BRIEN	kA
<g/>
,	,	kIx,
Denis	Denisa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Tenth	Tenth	k1gMnSc1
Crusade	Crusad	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mendocino	Mendocino	k1gNnSc1
Environmental	Environmental	k1gMnSc1
Center	centrum	k1gNnPc2
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2002-10-24	2002-10-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
COCKBURN	COCKBURN	kA
<g/>
,	,	kIx,
Alexander	Alexandra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Tenth	Tenth	k1gMnSc1
Crusade	Crusad	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc2
nation	nation	k1gInSc4
<g/>
,	,	kIx,
2002-9-5	2002-9-5	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Křížové	Křížové	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
<g/>
:	:	kIx,
Dnešní	dnešní	k2eAgInSc1d1
Západ	západ	k1gInSc1
jako	jako	k8xS,k8xC
křižáci	křižák	k1gMnPc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Bin	Bin	k1gMnSc1
Ládin	Ládin	k2eAgMnSc1d1
opět	opět	k6eAd1
obvinil	obvinit	k5eAaPmAgInS
Západ	západ	k1gInSc1
z	z	k7c2
křižácké	křižácký	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2006-4-23	2006-4-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
BALDWIN	BALDWIN	kA
<g/>
,	,	kIx,
Marshall	Marshall	k1gInSc1
W.	W.	kA
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
History	Histor	k1gInPc1
of	of	k?
the	the	k?
Crusades	Crusades	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vol	vol	k6eAd1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
The	The	k1gMnSc1
first	first	k1gMnSc1
hundred	hundred	k1gMnSc1
years	years	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Madison	Madison	k1gInSc1
<g/>
:	:	kIx,
University	universita	k1gFnSc2
of	of	k?
Wisconsin	Wisconsin	k2eAgInSc1d1
Press	Press	k1gInSc1
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
.	.	kIx.
707	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
BARBER	BARBER	kA
<g/>
,	,	kIx,
Malcolm	Malcolm	k1gMnSc1
<g/>
;	;	kIx,
BATE	BATE	kA
<g/>
,	,	kIx,
Keith	Keith	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letters	Letters	k1gInSc1
from	from	k1gInSc4
the	the	k?
East	East	k1gInSc1
:	:	kIx,
crusaders	crusaders	k1gInSc1
<g/>
,	,	kIx,
pilgrims	pilgrims	k1gInSc1
and	and	k?
settlers	settlers	k1gInSc1
in	in	k?
the	the	k?
12	#num#	k4
<g/>
th-	th-	k?
<g/>
13	#num#	k4
<g/>
th	th	k?
centuries	centuries	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Farnham	Farnham	k1gInSc4
<g/>
:	:	kIx,
Ashgate	Ashgat	k1gMnSc5
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
188	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7546	#num#	k4
<g/>
-	-	kIx~
<g/>
6356	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
BARBER	BARBER	kA
<g/>
,	,	kIx,
Malcolm	Malcolm	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Noví	nový	k2eAgMnPc1d1
rytíři	rytíř	k1gMnPc1
:	:	kIx,
dějiny	dějiny	k1gFnPc4
templářského	templářský	k2eAgInSc2d1
řádu	řád	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Argo	Argo	k1gNnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
433	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7203	#num#	k4
<g/>
-	-	kIx~
<g/>
764	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
BRIDGE	BRIDGE	kA
<g/>
,	,	kIx,
Antony	anton	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křížové	Křížové	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
228	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
512	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
DUGGAN	DUGGAN	kA
<g/>
,	,	kIx,
Alfred	Alfred	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křižácké	křižácký	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Orbis	orbis	k1gInSc1
<g/>
,	,	kIx,
1973	#num#	k4
<g/>
.	.	kIx.
214	#num#	k4
s.	s.	k?
</s>
<s>
GABRIELI	Gabriel	k1gMnSc5
<g/>
,	,	kIx,
Francesco	Francesco	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křížové	Křížové	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
očima	oko	k1gNnPc7
arabských	arabský	k2eAgInPc2d1
kronikářů	kronikář	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Argo	Argo	k1gNnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
344	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
257	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
333	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HAZARD	hazard	k1gInSc1
<g/>
,	,	kIx,
Harry	Harr	k1gInPc1
W.	W.	kA
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
History	Histor	k1gInPc1
of	of	k?
the	the	k?
Crusades	Crusades	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vol	vol	k6eAd1
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
The	The	k1gMnSc1
fourteenth	fourteenth	k1gMnSc1
and	and	k?
fifteenth	fifteenth	k1gMnSc1
centuries	centuries	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Madison	Madison	k1gInSc1
<g/>
:	:	kIx,
University	universita	k1gFnSc2
of	of	k?
Wisconsin	Wisconsin	k2eAgInSc1d1
Press	Press	k1gInSc1
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
.	.	kIx.
813	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
299	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6670	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
HROCHOVÁ	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
Věra	Věra	k1gFnSc1
<g/>
;	;	kIx,
HROCH	Hroch	k1gMnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křižáci	křižák	k1gMnPc1
ve	v	k7c6
Svaté	svatý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
289	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
204	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
621	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HROCHOVÁ	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
Věra	Věra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křížové	Křížové	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
ve	v	k7c6
světle	světlo	k1gNnSc6
soudobých	soudobý	k2eAgFnPc2d1
kronik	kronika	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Státní	státní	k2eAgNnSc1d1
pedagogické	pedagogický	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
.	.	kIx.
255	#num#	k4
s.	s.	k?
</s>
<s>
IBN	IBN	kA
MUNKIZ	MUNKIZ	kA
<g/>
,	,	kIx,
Usáma	Usámum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kniha	kniha	k1gFnSc1
zkušeností	zkušenost	k1gFnPc2
arabského	arabský	k2eAgMnSc2d1
bojovníka	bojovník	k1gMnSc2
s	s	k7c7
křižáky	křižák	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
348	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
1814	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
JOINVILLE	JOINVILLE	kA
<g/>
,	,	kIx,
Jean	Jean	k1gMnSc1
de	de	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paměti	paměť	k1gFnSc2
křižákovy	křižákův	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Státní	státní	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
krásné	krásný	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
a	a	k8xC
umění	umění	k1gNnSc1
<g/>
,	,	kIx,
1965	#num#	k4
<g/>
.	.	kIx.
206	#num#	k4
s.	s.	k?
</s>
<s>
KOMNÉNA	KOMNÉNA	kA
<g/>
,	,	kIx,
Anna	Anna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paměti	paměť	k1gFnPc4
byzantské	byzantský	k2eAgFnSc2d1
princezny	princezna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
565	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
207	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
527	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KOVAŘÍK	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Meč	meč	k1gInSc4
a	a	k8xC
kříž	kříž	k1gInSc4
:	:	kIx,
(	(	kIx(
<g/>
1066	#num#	k4
<g/>
-	-	kIx~
<g/>
1214	#num#	k4
<g/>
)	)	kIx)
:	:	kIx,
rytířské	rytířský	k2eAgFnPc1d1
bitvy	bitva	k1gFnPc1
a	a	k8xC
osudy	osud	k1gInPc1
I.	I.	kA
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
278	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
204	#num#	k4
<g/>
-	-	kIx~
<g/>
1289	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
LE	LE	kA
GOFF	GOFF	kA
<g/>
,	,	kIx,
Jacques	Jacques	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Saint	Saint	k1gMnSc1
Louis	Louis	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paris	Paris	k1gMnSc1
<g/>
:	:	kIx,
Gallimard	Gallimard	k1gMnSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
976	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
2070733696	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
LOWER	LOWER	kA
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Barons	Baronsa	k1gFnPc2
<g/>
'	'	kIx"
Crusade	Crusad	k1gInSc5
:	:	kIx,
A	a	k8xC
Call	Call	k1gInSc4
to	ten	k3xDgNnSc1
Arms	Arms	k1gInSc1
and	and	k?
Its	Its	k1gMnSc1
Consequences	Consequences	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Philadelphia	Philadelphia	k1gFnSc1
<g/>
:	:	kIx,
University	universita	k1gFnPc1
of	of	k?
Pennsylvania	Pennsylvanium	k1gNnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
272	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
8122	#num#	k4
<g/>
-	-	kIx~
<g/>
3873	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
MAYER	Mayer	k1gMnSc1
<g/>
,	,	kIx,
Hans	Hans	k1gMnSc1
Eberhard	Eberhard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
křížových	křížový	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Jiří	Jiří	k1gMnSc1
Knap	Knap	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Argo	Argo	k1gNnSc1
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
404	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
257	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
933	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
NICOLLE	NICOLLE	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
1096-99	1096-99	k4
:	:	kIx,
dobytí	dobytí	k1gNnSc4
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
96	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
247	#num#	k4
<g/>
-	-	kIx~
<g/>
1896	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
NICOLLE	NICOLLE	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhá	druhý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
1148	#num#	k4
:	:	kIx,
pohroma	pohroma	k1gFnSc1
před	před	k7c7
branami	brána	k1gFnPc7
Damašku	Damašek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
96	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
247	#num#	k4
<g/>
-	-	kIx~
<g/>
3413	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
NICOLLE	NICOLLE	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetí	třetí	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
1191	#num#	k4
:	:	kIx,
Richard	Richard	k1gMnSc1
Lví	lví	k2eAgNnSc4d1
srdce	srdce	k1gNnSc4
<g/>
,	,	kIx,
Saladin	Saladin	k2eAgInSc4d1
a	a	k8xC
zápas	zápas	k1gInSc4
o	o	k7c4
Jeruzalém	Jeruzalém	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
247	#num#	k4
<g/>
-	-	kIx~
<g/>
2382	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
O	O	kA
<g/>
'	'	kIx"
<g/>
CALLAGHAN	CALLAGHAN	kA
<g/>
,	,	kIx,
Joseph	Joseph	k1gMnSc1
F.	F.	kA
Reconquest	Reconquest	k1gMnSc1
and	and	k?
Crusade	Crusad	k1gInSc5
in	in	k?
Medieval	medieval	k1gInSc4
Spain	Spain	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Philadelphia	Philadelphia	k1gFnSc1
<g/>
:	:	kIx,
University	universita	k1gFnPc1
of	of	k?
Pennsylvania	Pennsylvanium	k1gNnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
344	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
8122	#num#	k4
<g/>
-	-	kIx~
<g/>
1889	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
RAJLICH	RAJLICH	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křížové	Křížové	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
a	a	k8xC
papežská	papežský	k2eAgFnSc1d1
politika	politika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historický	historický	k2eAgInSc1d1
obzor	obzor	k1gInSc1
<g/>
.	.	kIx.
1997	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
8	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
57	#num#	k4
<g/>
-	-	kIx~
<g/>
64	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
RILEY-SMITH	RILEY-SMITH	k?
<g/>
,	,	kIx,
Jonathan	Jonathan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
First	First	k1gMnSc1
Crusade	Crusad	k1gInSc5
and	and	k?
the	the	k?
idea	idea	k1gFnSc1
of	of	k?
crusading	crusading	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Philadelphia	Philadelphia	k1gFnSc1
<g/>
:	:	kIx,
University	universita	k1gFnPc1
of	of	k?
Pennsylvania	Pennsylvanium	k1gNnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
.	.	kIx.
227	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
81	#num#	k4
<g/>
-	-	kIx~
<g/>
228026	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
RILEY-SMITH	RILEY-SMITH	k?
<g/>
,	,	kIx,
Jonathan	Jonathan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Oxford	Oxford	k1gInSc4
illustrated	illustrated	k1gInSc1
history	histor	k1gInPc1
of	of	k?
the	the	k?
Crusades	Crusades	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oxford	Oxford	k1gInSc1
<g/>
:	:	kIx,
Oxford	Oxford	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
436	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
-	-	kIx~
<g/>
820435	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
RUNCIMAN	RUNCIMAN	kA
<g/>
,	,	kIx,
Steven	Steven	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
history	histor	k1gInPc1
of	of	k?
the	the	k?
Crusades	Crusades	k1gInSc1
<g/>
.	.	kIx.
vol	vol	k6eAd1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
The	The	k1gMnSc1
first	first	k1gMnSc1
Crusades	Crusades	k1gMnSc1
and	and	k?
the	the	k?
foundation	foundation	k1gInSc1
of	of	k?
the	the	k?
Kingdom	Kingdom	k1gInSc1
of	of	k?
Jerusalem	Jerusalem	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cambridge	Cambridge	k1gFnSc1
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
376	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
521	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6161	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
RUNCIMAN	RUNCIMAN	kA
<g/>
,	,	kIx,
Steven	Steven	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
history	histor	k1gInPc1
of	of	k?
the	the	k?
crusades	crusades	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vol	vol	k6eAd1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
The	The	k1gFnSc1
Kingdom	Kingdom	k1gInSc1
of	of	k?
Jerusalem	Jerusalem	k1gInSc1
and	and	k?
the	the	k?
Frankish	Frankish	k1gMnSc1
East	East	k1gMnSc1
:	:	kIx,
1100	#num#	k4
<g/>
-	-	kIx~
<g/>
1187	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Penguin	Penguin	k2eAgInSc1d1
Books	Books	k1gInSc1
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
.	.	kIx.
376	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
13704	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
RUNCIMAN	RUNCIMAN	kA
<g/>
,	,	kIx,
Steven	Steven	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
history	histor	k1gInPc1
of	of	k?
the	the	k?
Crusades	Crusades	k1gInSc1
<g/>
.	.	kIx.
vol	vol	k6eAd1
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
The	The	k1gFnSc1
kingdom	kingdom	k1gInSc1
of	of	k?
Acre	Acre	k1gInSc1
and	and	k?
the	the	k?
later	later	k1gMnSc1
crusades	crusades	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
;	;	kIx,
New-York	New-York	k1gInSc1
;	;	kIx,
Toronto	Toronto	k1gNnSc1
<g/>
:	:	kIx,
Penguin	Penguin	k2eAgInSc1d1
Books	Books	k1gInSc1
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
.	.	kIx.
529	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
13705	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
SMAIL	SMAIL	kA
<g/>
,	,	kIx,
Raymond	Raymond	k1gMnSc1
Charles	Charles	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Crusading	Crusading	k1gInSc1
Warfare	Warfar	k1gMnSc5
(	(	kIx(
<g/>
1097	#num#	k4
<g/>
-	-	kIx~
<g/>
1193	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Barnes	Barnes	k1gInSc1
&	&	k?
Noble	Noble	k1gInSc1
Books	Booksa	k1gFnPc2
<g/>
,	,	kIx,
1956	#num#	k4
<g/>
.	.	kIx.
272	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
56619	#num#	k4
<g/>
-	-	kIx~
<g/>
769	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
SOUKUP	Soukup	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetí	třetí	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
dle	dle	k7c2
kronikáře	kronikář	k1gMnSc2
Ansberta	Ansbert	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příbram	Příbram	k1gFnSc1
<g/>
:	:	kIx,
Knihovna	knihovna	k1gFnSc1
Jana	Jan	k1gMnSc2
Drdy	Drda	k1gMnSc2
v	v	k7c6
Příbrami	Příbram	k1gFnSc6
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
151	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86240	#num#	k4
<g/>
-	-	kIx~
<g/>
67	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
SOUKUP	Soukup	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
;	;	kIx,
SVÁTEK	Svátek	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křížové	Křížové	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
v	v	k7c6
pozdním	pozdní	k2eAgInSc6d1
středověku	středověk	k1gInSc6
:	:	kIx,
kapitoly	kapitola	k1gFnSc2
z	z	k7c2
dějin	dějiny	k1gFnPc2
náboženských	náboženský	k2eAgInPc2d1
konfliktů	konflikt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
NLN	NLN	kA
Nakladatelství	nakladatelství	k1gNnPc4
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
245	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7422	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
55	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
TYERMAN	TYERMAN	kA
<g/>
,	,	kIx,
Christopher	Christophra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svaté	svatý	k2eAgInPc1d1
války	válek	k1gInPc1
:	:	kIx,
dějiny	dějiny	k1gFnPc1
křížových	křížový	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
926	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7422	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
91	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
WOLFF	WOLFF	kA
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
L.	L.	kA
<g/>
;	;	kIx,
HAZARD	hazard	k1gMnSc1
<g/>
,	,	kIx,
Harry	Harr	k1gMnPc4
W.	W.	kA
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
History	Histor	k1gInPc1
of	of	k?
the	the	k?
Crusades	Crusades	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vol	vol	k6eAd1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
The	The	k1gMnSc1
later	later	k1gMnSc1
Crusades	Crusades	k1gMnSc1
<g/>
,	,	kIx,
1189	#num#	k4
<g/>
-	-	kIx~
<g/>
1311	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Madison	Madison	k1gInSc1
<g/>
:	:	kIx,
University	universita	k1gFnSc2
of	of	k?
Wisconsin	Wisconsin	k2eAgInSc1d1
Press	Press	k1gInSc1
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
.	.	kIx.
871	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
ZÁSTĚROVÁ	Zástěrová	k1gFnSc1
<g/>
,	,	kIx,
Bohumila	Bohumila	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
Byzance	Byzanc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
.	.	kIx.
529	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
454	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Švédské	švédský	k2eAgFnPc1d1
křížové	křížový	k2eAgFnPc1d1
výpravy	výprava	k1gFnPc1
</s>
<s>
Křížové	Křížové	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
proti	proti	k7c3
husitům	husita	k1gMnPc3
</s>
<s>
Křižácká	křižácký	k2eAgNnPc1d1
tažení	tažení	k1gNnPc1
do	do	k7c2
Egypta	Egypt	k1gInSc2
</s>
<s>
Alexandrijská	alexandrijský	k2eAgFnSc1d1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1
heslo	heslo	k1gNnSc4
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc7
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Křížové	Křížové	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
</s>
<s>
David	David	k1gMnSc1
Zbíral	Zbíral	k1gMnSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Křížové	Křížové	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
<g/>
:	:	kIx,
stručný	stručný	k2eAgInSc4d1
přehled	přehled	k1gInSc4
<g/>
“	“	k?
–	–	k?
religionistická	religionistický	k2eAgFnSc1d1
seminární	seminární	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
;	;	kIx,
od	od	k7c2
téhož	týž	k3xTgMnSc2
autora	autor	k1gMnSc2
„	„	k?
<g/>
Kristovy	Kristův	k2eAgInPc4d1
ostatky	ostatek	k1gInPc4
jako	jako	k8xS,k8xC
zbraň	zbraň	k1gFnSc4
křižácké	křižácký	k2eAgFnSc2d1
propagandy	propaganda	k1gFnSc2
<g/>
:	:	kIx,
případ	případ	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1247	#num#	k4
<g/>
“	“	k?
</s>
<s>
Války	válek	k1gInPc4
křížové	křížový	k2eAgInPc4d1
–	–	k?
shrnutí	shrnutí	k1gNnSc1
z	z	k7c2
knihy	kniha	k1gFnSc2
Průvodce	průvodce	k1gMnSc1
po	po	k7c6
dějinách	dějiny	k1gFnPc6
<g/>
,	,	kIx,
1893	#num#	k4
</s>
<s>
Rozhovor	rozhovor	k1gInSc1
s	s	k7c7
historikem	historik	k1gMnSc7
T.	T.	kA
F.	F.	kA
Maddenem	Madden	k1gInSc7
obhajujícím	obhajující	k2eAgMnSc6d1
křížové	křížový	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
proti	proti	k7c3
soudobé	soudobý	k2eAgFnSc3d1
kritice	kritika	k1gFnSc3
</s>
<s>
Účast	účast	k1gFnSc1
Čechů	Čech	k1gMnPc2
na	na	k7c6
křížových	křížový	k2eAgFnPc6d1
výpravách	výprava	k1gFnPc6
</s>
<s>
Čeští	český	k2eAgMnPc1d1
"	"	kIx"
<g/>
křižáci	křižák	k1gMnPc1
<g/>
"	"	kIx"
na	na	k7c6
konci	konec	k1gInSc6
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Křížové	Křížové	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravyproti	výpravyprot	k1gMnPc1
muslimům	muslim	k1gMnPc3
</s>
<s>
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
(	(	kIx(
<g/>
1095	#num#	k4
<g/>
–	–	k?
<g/>
1291	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Lidová	lidový	k2eAgFnSc1d1
(	(	kIx(
<g/>
1096	#num#	k4
<g/>
)	)	kIx)
•	•	k?
První	první	k4xOgFnSc2
(	(	kIx(
<g/>
1095	#num#	k4
<g/>
–	–	k?
<g/>
1099	#num#	k4
<g/>
)	)	kIx)
•	•	k?
výprava	výprava	k1gFnSc1
roku	rok	k1gInSc2
1101	#num#	k4
(	(	kIx(
<g/>
1101	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Norská	norský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1107	#num#	k4
<g/>
–	–	k?
<g/>
1110	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Benátská	benátský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1122	#num#	k4
<g/>
–	–	k?
<g/>
1124	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Damašská	damašský	k2eAgFnSc1d1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1129	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Druhá	druhý	k4xOgFnSc1
(	(	kIx(
<g/>
1147	#num#	k4
<g/>
–	–	k?
<g/>
1149	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Amauryho	Amaury	k1gMnSc2
tažení	tažení	k1gNnSc1
do	do	k7c2
Egypta	Egypt	k1gInSc2
(	(	kIx(
<g/>
1154	#num#	k4
<g/>
–	–	k?
<g/>
1169	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Třetí	třetí	k4xOgFnSc2
(	(	kIx(
<g/>
1189	#num#	k4
<g/>
–	–	k?
<g/>
1192	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Německá	německý	k2eAgFnSc1d1
(	(	kIx(
<g/>
1197	#num#	k4
<g/>
–	–	k?
<g/>
1198	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Čtvrtá	čtvrtá	k1gFnSc1
(	(	kIx(
<g/>
1202	#num#	k4
<g/>
–	–	k?
<g/>
1204	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Dětské	dětský	k2eAgNnSc1d1
(	(	kIx(
<g/>
1212	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pátá	pátá	k1gFnSc1
(	(	kIx(
<g/>
1217	#num#	k4
<g/>
–	–	k?
<g/>
1221	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Šestá	šestý	k4xOgFnSc1
(	(	kIx(
<g/>
1228	#num#	k4
<g/>
–	–	k?
<g/>
1229	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Baronská	baronský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1239	#num#	k4
<g/>
–	–	k?
<g/>
1241	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Pastýřská	pastýřská	k1gFnSc1
(	(	kIx(
<g/>
1251	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sedmá	sedmý	k4xOgFnSc1
(	(	kIx(
<g/>
1248	#num#	k4
<g/>
–	–	k?
<g/>
1254	#num#	k4
<g/>
)	)	kIx)
•	•	k?
výprava	výprava	k1gFnSc1
roku	rok	k1gInSc2
1267	#num#	k4
•	•	k?
Osmá	osmý	k4xOgFnSc1
(	(	kIx(
<g/>
1270	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Devátá	devátý	k4xOgFnSc1
(	(	kIx(
<g/>
1271	#num#	k4
<g/>
–	–	k?
<g/>
1272	#num#	k4
<g/>
)	)	kIx)
Reconquista	Reconquista	k1gMnSc1
<g/>
(	(	kIx(
<g/>
718	#num#	k4
<g/>
–	–	k?
<g/>
1492	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Reconquista	Reconquista	k1gMnSc1
(	(	kIx(
<g/>
718	#num#	k4
<g/>
–	–	k?
<g/>
1492	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Barbastro	Barbastro	k1gNnSc4
(	(	kIx(
<g/>
1063	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Mallorka	Mallorka	k1gFnSc1
(	(	kIx(
<g/>
1313	#num#	k4
<g/>
–	–	k?
<g/>
1315	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Las	laso	k1gNnPc2
Navas	Navas	k1gInSc1
de	de	k?
Tolosa	Tolosa	k1gFnSc1
(	(	kIx(
<g/>
1212	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pastýřská	pastýřská	k1gFnSc1
(	(	kIx(
<g/>
1320	#num#	k4
<g/>
)	)	kIx)
po	po	k7c6
roce	rok	k1gInSc6
1291	#num#	k4
</s>
<s>
Chudinská	chudinský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1309	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Smyrenské	smyrenský	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
(	(	kIx(
<g/>
1343	#num#	k4
<g/>
–	–	k?
<g/>
1351	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Alexandrijská	alexandrijský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1365	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Mahdijská	Mahdijský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1390	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Modlící	modlící	k2eAgMnSc1d1
se	se	k3xPyFc4
křižácký	křižácký	k2eAgMnSc1d1
rytíř	rytíř	k1gMnSc1
na	na	k7c6
dobové	dobový	k2eAgFnSc6d1
miniatuře	miniatura	k1gFnSc6
(	(	kIx(
<g/>
BL	BL	kA
MS	MS	kA
Royal	Royal	k1gInSc4
2A	2A	k4
XXII	XXII	kA
f.	f.	k?
220	#num#	k4
<g/>
)	)	kIx)
severní	severní	k2eAgFnSc2d1
křížové	křížový	k2eAgFnSc2d1
výpravyproti	výpravyprot	k1gMnPc1
pohanům	pohan	k1gMnPc3
</s>
<s>
Kalmarská	Kalmarský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1123	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Venedská	Venedský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1147	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Švédské	švédský	k2eAgNnSc1d1
(	(	kIx(
<g/>
tři	tři	k4xCgFnPc1
<g/>
)	)	kIx)
•	•	k?
Livonská	Livonský	k2eAgFnSc1d1
(	(	kIx(
<g/>
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
•	•	k?
Pruská	pruský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1217	#num#	k4
<g/>
–	–	k?
<g/>
1274	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Litevská	litevský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1283	#num#	k4
<g/>
–	–	k?
<g/>
1410	#num#	k4
<g/>
)	)	kIx)
křížové	křížový	k2eAgFnSc2d1
výpravyproti	výpravyprot	k1gMnPc1
kacířům	kacíř	k1gMnPc3
</s>
<s>
proti	proti	k7c3
kacířům	kacíř	k1gMnPc3
aproti	aprot	k1gMnPc1
neposlušnýmpanovníkům	neposlušnýmpanovník	k1gInPc3
</s>
<s>
Albigenská	albigenský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1209	#num#	k4
<g/>
–	–	k?
<g/>
1229	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Drentská	Drentský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1228	#num#	k4
<g/>
–	–	k?
<g/>
1232	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Stedingerská	Stedingerský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1233	#num#	k4
<g/>
–	–	k?
<g/>
1234	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bosenská	bosenský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1235	#num#	k4
<g/>
–	–	k?
<g/>
1241	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Novgorodská	novgorodský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1241	#num#	k4
<g/>
–	–	k?
<g/>
1242	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Aragonská	aragonský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1284	#num#	k4
<g/>
–	–	k?
<g/>
1285	#num#	k4
<g/>
)	)	kIx)
•	•	k?
do	do	k7c2
Čech	Čechy	k1gFnPc2
proti	proti	k7c3
valdenským	valdenští	k1gMnPc3
(	(	kIx(
<g/>
1340	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Despenserova	Despenserův	k2eAgInSc2d1
(	(	kIx(
<g/>
1382	#num#	k4
<g/>
–	–	k?
<g/>
1383	#num#	k4
<g/>
)	)	kIx)
proti	proti	k7c3
husitům	husita	k1gMnPc3
</s>
<s>
První	první	k4xOgMnSc1
(	(	kIx(
<g/>
1420	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Druhá	druhý	k4xOgFnSc1
(	(	kIx(
<g/>
1421	#num#	k4
<g/>
–	–	k?
<g/>
1422	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Třetí	třetí	k4xOgFnSc2
(	(	kIx(
<g/>
1427	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Čtvrtá	čtvrtá	k1gFnSc1
(	(	kIx(
<g/>
1431	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Česko-uherské	česko-uherský	k2eAgFnSc2d1
války	válka	k1gFnSc2
(	(	kIx(
<g/>
1468	#num#	k4
<g/>
-	-	kIx~
<g/>
1478	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
pozdní	pozdní	k2eAgInPc1d1
křížové	křížový	k2eAgInPc1d1
výpravyproti	výpravyprot	k1gMnPc1
Osmanským	osmanský	k2eAgInSc7d1
Turkům	Turek	k1gMnPc3
</s>
<s>
Savojská	savojský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1366	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Nikopolská	Nikopolský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1396	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Varnská	Varnská	k1gFnSc1
(	(	kIx(
<g/>
1444	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Projekt	projekt	k1gInSc1
Evžena	Evžen	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1444	#num#	k4
<g/>
–	–	k?
<g/>
1445	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Projekt	projekt	k1gInSc1
Kalixta	Kalixta	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1455	#num#	k4
<g/>
–	–	k?
<g/>
1458	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Projekt	projekt	k1gInSc1
Pia	Pius	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1463	#num#	k4
<g/>
–	–	k?
<g/>
1464	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Portugalská	portugalský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1481	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Křesťanství	křesťanství	k1gNnSc1
|	|	kIx~
Křížové	Křížové	k2eAgFnPc1d1
výpravy	výprava	k1gFnPc1
|	|	kIx~
Byzantská	byzantský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
|	|	kIx~
Válka	válka	k1gFnSc1
|	|	kIx~
Středověk	středověk	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ph	ph	kA
<g/>
122162	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4073802-4	4073802-4	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
7857	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85034380	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85034380	#num#	k4
</s>
