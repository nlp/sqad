<p>
<s>
Josi	Josi	k1gNnSc1	Josi
Kac	Kac	k1gFnSc2	Kac
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
י	י	k?	י
כ	כ	k?	כ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
izraelský	izraelský	k2eAgMnSc1d1	izraelský
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
poslanec	poslanec	k1gMnSc1	poslanec
Knesetu	Kneset	k1gInSc2	Kneset
za	za	k7c4	za
Stranu	strana	k1gFnSc4	strana
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1949	[number]	k4	1949
v	v	k7c6	v
Haifě	Haifa	k1gFnSc6	Haifa
<g/>
.	.	kIx.	.
</s>
<s>
Sloužil	sloužit	k5eAaImAgMnS	sloužit
v	v	k7c6	v
izraelské	izraelský	k2eAgFnSc6d1	izraelská
armádě	armáda	k1gFnSc6	armáda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získal	získat	k5eAaPmAgMnS	získat
hodnost	hodnost	k1gFnSc4	hodnost
Sergeant	Sergeant	k1gMnSc1	Sergeant
Major	major	k1gMnSc1	major
(	(	kIx(	(
<g/>
Rav	Rav	k1gMnSc1	Rav
Samal	Samal	k1gMnSc1	Samal
Mitkadem	Mitkad	k1gInSc7	Mitkad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
právo	právo	k1gNnSc1	právo
na	na	k7c6	na
Hebrejské	hebrejský	k2eAgFnSc6d1	hebrejská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
právník	právník	k1gMnSc1	právník
<g/>
.	.	kIx.	.
</s>
<s>
Hovoří	hovořit	k5eAaImIp3nS	hovořit
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
a	a	k8xC	a
německy	německy	k6eAd1	německy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politická	politický	k2eAgFnSc1d1	politická
dráha	dráha	k1gFnSc1	dráha
==	==	k?	==
</s>
</p>
<p>
<s>
Působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
právní	právní	k2eAgMnSc1d1	právní
poradce	poradce	k1gMnSc1	poradce
odborové	odborový	k2eAgFnSc2d1	odborová
centrály	centrála	k1gFnSc2	centrála
Histadrut	Histadrut	k1gInSc1	Histadrut
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Haify	Haifa	k1gFnSc2	Haifa
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
výboru	výbor	k1gInSc2	výbor
Histadrutu	Histadrut	k1gInSc2	Histadrut
pro	pro	k7c4	pro
pracovní	pracovní	k2eAgNnSc4d1	pracovní
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Zasedal	zasedat	k5eAaImAgMnS	zasedat
v	v	k7c6	v
samosprávě	samospráva	k1gFnSc6	samospráva
města	město	k1gNnSc2	město
Kirjat	Kirjat	k1gInSc4	Kirjat
Tiv	Tiv	k1gFnSc2	Tiv
<g/>
'	'	kIx"	'
<g/>
on	on	k3xPp3gMnSc1	on
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
izraelském	izraelský	k2eAgInSc6d1	izraelský
parlamentu	parlament	k1gInSc6	parlament
zasedl	zasednout	k5eAaPmAgMnS	zasednout
poprvé	poprvé	k6eAd1	poprvé
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
za	za	k7c4	za
Stranu	strana	k1gFnSc4	strana
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
parlamentním	parlamentní	k2eAgInSc6d1	parlamentní
výboru	výbor	k1gInSc6	výbor
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
sociálních	sociální	k2eAgFnPc2d1	sociální
věcí	věc	k1gFnPc2	věc
(	(	kIx(	(
<g/>
tomu	ten	k3xDgNnSc3	ten
předsedal	předsedat	k5eAaImAgMnS	předsedat
<g/>
)	)	kIx)	)
a	a	k8xC	a
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
ústavu	ústava	k1gFnSc4	ústava
<g/>
,	,	kIx,	,
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
spravedlnost	spravedlnost	k1gFnSc4	spravedlnost
<g/>
.	.	kIx.	.
</s>
<s>
Předsedal	předsedat	k5eAaImAgMnS	předsedat
podvýboru	podvýbor	k1gInSc2	podvýbor
pro	pro	k7c4	pro
penze	penze	k1gFnPc4	penze
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
Kneset	Kneset	k1gInSc4	Kneset
byl	být	k5eAaImAgMnS	být
vyslancem	vyslanec	k1gMnSc7	vyslanec
Izraele	Izrael	k1gInSc2	Izrael
při	při	k7c6	při
Radě	rada	k1gFnSc6	rada
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
mandát	mandát	k1gInSc1	mandát
obhájil	obhájit	k5eAaPmAgInS	obhájit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
následného	následný	k2eAgNnSc2d1	následné
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
působil	působit	k5eAaImAgMnS	působit
ve	v	k7c6	v
výboru	výbor	k1gInSc6	výbor
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
sociálních	sociální	k2eAgFnPc2d1	sociální
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
výboru	výbor	k1gInSc6	výbor
finančním	finanční	k2eAgInSc6d1	finanční
a	a	k8xC	a
výboru	výbor	k1gInSc6	výbor
etickém	etický	k2eAgInSc6d1	etický
<g/>
.	.	kIx.	.
</s>
<s>
Předsedal	předsedat	k5eAaImAgInS	předsedat
výboru	výbor	k1gInSc3	výbor
státní	státní	k2eAgFnSc2d1	státní
kontroly	kontrola	k1gFnSc2	kontrola
<g/>
.	.	kIx.	.
<g/>
Opětovně	opětovně	k6eAd1	opětovně
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
ale	ale	k8xC	ale
za	za	k7c4	za
střechovou	střechový	k2eAgFnSc4d1	střechová
organizaci	organizace	k1gFnSc4	organizace
Jeden	jeden	k4xCgInSc4	jeden
Izrael	Izrael	k1gInSc4	Izrael
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yQgFnSc2	který
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
sloučilo	sloučit	k5eAaPmAgNnS	sloučit
několik	několik	k4yIc1	několik
politických	politický	k2eAgInPc2d1	politický
subjektů	subjekt	k1gInPc2	subjekt
včetně	včetně	k7c2	včetně
jeho	jeho	k3xOp3gFnSc2	jeho
domovské	domovský	k2eAgFnSc2d1	domovská
Strany	strana	k1gFnSc2	strana
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Zasedal	zasedat	k5eAaImAgMnS	zasedat
v	v	k7c6	v
parlamentním	parlamentní	k2eAgInSc6d1	parlamentní
výboru	výbor	k1gInSc6	výbor
pro	pro	k7c4	pro
status	status	k1gInSc4	status
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
zahraniční	zahraniční	k2eAgFnPc4d1	zahraniční
záležitosti	záležitost	k1gFnPc4	záležitost
a	a	k8xC	a
obranu	obrana	k1gFnSc4	obrana
<g/>
,	,	kIx,	,
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
ústavu	ústava	k1gFnSc4	ústava
<g/>
,	,	kIx,	,
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
spravedlnost	spravedlnost	k1gFnSc4	spravedlnost
a	a	k8xC	a
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
imigraci	imigrace	k1gFnSc4	imigrace
<g/>
,	,	kIx,	,
absorpci	absorpce	k1gFnSc4	absorpce
a	a	k8xC	a
záležitosti	záležitost	k1gFnPc4	záležitost
diaspory	diaspora	k1gFnSc2	diaspora
<g/>
.	.	kIx.	.
</s>
<s>
Předsedal	předsedat	k5eAaImAgMnS	předsedat
výboru	výbor	k1gInSc3	výbor
House	house	k1gNnSc4	house
Committee	Committe	k1gInSc2	Committe
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zároveň	zároveň	k6eAd1	zároveň
předsedou	předseda	k1gMnSc7	předseda
podvýboru	podvýbor	k1gInSc2	podvýbor
pro	pro	k7c4	pro
neobnovování	neobnovování	k1gNnSc4	neobnovování
mimořádné	mimořádný	k2eAgFnSc2d1	mimořádná
situace	situace	k1gFnSc2	situace
a	a	k8xC	a
speciálního	speciální	k2eAgInSc2d1	speciální
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
diskuzi	diskuze	k1gFnSc4	diskuze
o	o	k7c6	o
zákonu	zákon	k1gInSc6	zákon
o	o	k7c6	o
bezpečnostních	bezpečnostní	k2eAgFnPc6d1	bezpečnostní
službách	služba	k1gFnPc6	služba
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
mandát	mandát	k1gInSc1	mandát
neobhájil	obhájit	k5eNaPmAgInS	obhájit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Kneset	Kneset	k1gInSc1	Kneset
–	–	k?	–
Josi	Jos	k1gFnSc2	Jos
Kac	Kac	k1gFnSc2	Kac
</s>
</p>
