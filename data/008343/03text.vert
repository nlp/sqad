<p>
<s>
Okres	okres	k1gInSc1	okres
Trutnov	Trutnov	k1gInSc1	Trutnov
je	být	k5eAaImIp3nS	být
okresem	okres	k1gInSc7	okres
v	v	k7c6	v
Královéhradeckém	královéhradecký	k2eAgInSc6d1	královéhradecký
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
dřívějším	dřívější	k2eAgNnSc7d1	dřívější
sídlem	sídlo	k1gNnSc7	sídlo
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
Trutnov	Trutnov	k1gInSc1	Trutnov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
královéhradeckých	královéhradecký	k2eAgInPc2d1	královéhradecký
okresů	okres	k1gInPc2	okres
sousedí	sousedit	k5eAaImIp3nS	sousedit
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
s	s	k7c7	s
okresem	okres	k1gInSc7	okres
Jičín	Jičín	k1gInSc1	Jičín
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
kousek	kousek	k1gInSc1	kousek
s	s	k7c7	s
okresem	okres	k1gInSc7	okres
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
a	a	k8xC	a
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
a	a	k8xC	a
východě	východ	k1gInSc6	východ
pak	pak	k6eAd1	pak
s	s	k7c7	s
okresem	okres	k1gInSc7	okres
Náchod	Náchod	k1gInSc1	Náchod
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
na	na	k7c6	na
západě	západ	k1gInSc6	západ
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
okresem	okres	k1gInSc7	okres
Semily	Semily	k1gInPc4	Semily
Libereckého	liberecký	k2eAgInSc2d1	liberecký
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
severu	sever	k1gInSc2	sever
je	být	k5eAaImIp3nS	být
okres	okres	k1gInSc1	okres
vymezen	vymezit	k5eAaPmNgInS	vymezit
státní	státní	k2eAgFnSc7d1	státní
hranicí	hranice	k1gFnSc7	hranice
s	s	k7c7	s
polským	polský	k2eAgNnSc7d1	polské
Slezskem	Slezsko	k1gNnSc7	Slezsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přírodní	přírodní	k2eAgInPc1d1	přírodní
poměry	poměr	k1gInPc1	poměr
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
okrese	okres	k1gInSc6	okres
Trutnov	Trutnov	k1gInSc1	Trutnov
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
je	být	k5eAaImIp3nS	být
vrchol	vrchol	k1gInSc1	vrchol
Sněžky	Sněžka	k1gFnSc2	Sněžka
(	(	kIx(	(
<g/>
1603	[number]	k4	1603
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Okres	okres	k1gInSc1	okres
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
geomorfologických	geomorfologický	k2eAgInPc6d1	geomorfologický
celcích	celek	k1gInPc6	celek
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
<g/>
,	,	kIx,	,
Krkonošské	krkonošský	k2eAgNnSc1d1	Krkonošské
podhůří	podhůří	k1gNnSc1	podhůří
a	a	k8xC	a
Jičínská	jičínský	k2eAgFnSc1d1	Jičínská
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Území	území	k1gNnSc1	území
odvodňuje	odvodňovat	k5eAaImIp3nS	odvodňovat
na	na	k7c6	na
západě	západ	k1gInSc6	západ
řeka	řeka	k1gFnSc1	řeka
Labe	Labe	k1gNnSc2	Labe
na	na	k7c6	na
východě	východ	k1gInSc6	východ
řeka	řeka	k1gFnSc1	řeka
Úpa	Úpa	k1gFnSc1	Úpa
<g/>
,	,	kIx,	,
menší	malý	k2eAgFnSc1d2	menší
část	část	k1gFnSc1	část
při	při	k7c6	při
severní	severní	k2eAgFnSc6d1	severní
hranici	hranice	k1gFnSc6	hranice
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
je	být	k5eAaImIp3nS	být
odvodňováno	odvodňován	k2eAgNnSc1d1	odvodňován
Bobrem	bobr	k1gMnSc7	bobr
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
Černým	černý	k2eAgInSc7d1	černý
potokem	potok	k1gInSc7	potok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
okrese	okres	k1gInSc6	okres
Trutnov	Trutnov	k1gInSc1	Trutnov
leží	ležet	k5eAaImIp3nS	ležet
část	část	k1gFnSc4	část
území	území	k1gNnSc2	území
Krkonošského	krkonošský	k2eAgInSc2d1	krkonošský
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
a	a	k8xC	a
Chráněné	chráněný	k2eAgFnSc2d1	chráněná
krajinné	krajinný	k2eAgFnSc2d1	krajinná
oblasti	oblast	k1gFnSc2	oblast
Broumovsko	Broumovsko	k1gNnSc1	Broumovsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hospodářství	hospodářství	k1gNnSc1	hospodářství
==	==	k?	==
</s>
</p>
<p>
<s>
Významná	významný	k2eAgFnSc1d1	významná
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
hlubinná	hlubinný	k2eAgFnSc1d1	hlubinná
těžba	těžba	k1gFnSc1	těžba
černého	černý	k2eAgNnSc2d1	černé
uhlí	uhlí	k1gNnSc2	uhlí
na	na	k7c4	na
Žacléřsku	Žacléřska	k1gFnSc4	Žacléřska
a	a	k8xC	a
Svatoňovicku	Svatoňovicka	k1gFnSc4	Svatoňovicka
(	(	kIx(	(
<g/>
vnitrosudetská	vnitrosudetský	k2eAgFnSc1d1	vnitrosudetská
pánev	pánev	k1gFnSc1	pánev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
doluje	dolovat	k5eAaImIp3nS	dolovat
povrchově	povrchově	k6eAd1	povrchově
u	u	k7c2	u
Žacléře	Žacléř	k1gInSc2	Žacléř
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Těžba	těžba	k1gFnSc1	těžba
dolomit	dolomit	k5eAaPmF	dolomit
v	v	k7c6	v
Lánově	lánově	k6eAd1	lánově
a	a	k8xC	a
vápence	vápenec	k1gInSc2	vápenec
krkonošsko-jizerského	krkonošskoizerský	k2eAgNnSc2d1	krkonošsko-jizerské
krystalinika	krystalinikum	k1gNnSc2	krystalinikum
v	v	k7c6	v
Černém	černý	k2eAgInSc6d1	černý
dole	dol	k1gInSc6	dol
(	(	kIx(	(
<g/>
Krkonošské	krkonošský	k2eAgFnPc1d1	Krkonošská
vápenky	vápenka	k1gFnPc1	vápenka
Kunčice	Kunčice	k1gFnPc1	Kunčice
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Struktura	struktura	k1gFnSc1	struktura
povrchu	povrch	k1gInSc2	povrch
==	==	k?	==
</s>
</p>
<p>
<s>
K	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc3	prosinec
2003	[number]	k4	2003
měl	mít	k5eAaImAgInS	mít
okres	okres	k1gInSc1	okres
celkovou	celkový	k2eAgFnSc4d1	celková
plochu	plocha	k1gFnSc4	plocha
1	[number]	k4	1
146,78	[number]	k4	146,78
km2	km2	k4	km2
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
43,88	[number]	k4	43,88
%	%	kIx~	%
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
pozemků	pozemek	k1gInPc2	pozemek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
z	z	k7c2	z
55,26	[number]	k4	55,26
%	%	kIx~	%
tvoří	tvořit	k5eAaImIp3nS	tvořit
orná	orný	k2eAgFnSc1d1	orná
půda	půda	k1gFnSc1	půda
(	(	kIx(	(
<g/>
24,25	[number]	k4	24,25
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
okresu	okres	k1gInSc2	okres
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
56,12	[number]	k4	56,12
%	%	kIx~	%
ostatní	ostatní	k2eAgInPc1d1	ostatní
pozemky	pozemek	k1gInPc1	pozemek
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
82,96	[number]	k4	82,96
%	%	kIx~	%
lesy	les	k1gInPc1	les
(	(	kIx(	(
<g/>
46,56	[number]	k4	46,56
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
okresu	okres	k1gInSc2	okres
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Demografické	demografický	k2eAgInPc1d1	demografický
údaje	údaj	k1gInPc1	údaj
==	==	k?	==
</s>
</p>
<p>
<s>
Data	datum	k1gNnPc1	datum
k	k	k7c3	k
30	[number]	k4	30
<g/>
.	.	kIx.	.
červnu	červen	k1gInSc3	červen
2005	[number]	k4	2005
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
<g/>
:	:	kIx,	:
105	[number]	k4	105
ob.	ob.	k?	ob.
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
</s>
</p>
<p>
<s>
69,16	[number]	k4	69,16
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
městech	město	k1gNnPc6	město
</s>
</p>
<p>
<s>
===	===	k?	===
Zaměstnanost	zaměstnanost	k1gFnSc1	zaměstnanost
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Školství	školství	k1gNnSc2	školství
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Zdravotnictví	zdravotnictví	k1gNnSc1	zdravotnictví
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Zdroj	zdroj	k1gInSc1	zdroj
===	===	k?	===
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
statistický	statistický	k2eAgInSc1d1	statistický
úřad	úřad	k1gInSc1	úřad
</s>
</p>
<p>
<s>
==	==	k?	==
Silniční	silniční	k2eAgFnSc1d1	silniční
doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Okresem	okres	k1gInSc7	okres
prochází	procházet	k5eAaImIp3nS	procházet
silnice	silnice	k1gFnSc1	silnice
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
<g/>
,	,	kIx,	,
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
a	a	k8xC	a
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
37	[number]	k4	37
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Silnice	silnice	k1gFnSc1	silnice
II	II	kA	II
<g/>
.	.	kIx.	.
třídy	třída	k1gFnPc1	třída
jsou	být	k5eAaImIp3nP	být
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
252	[number]	k4	252
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
284	[number]	k4	284
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
285	[number]	k4	285
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
295	[number]	k4	295
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
296	[number]	k4	296
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
297	[number]	k4	297
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
299	[number]	k4	299
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
300	[number]	k4	300
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
301	[number]	k4	301
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
304	[number]	k4	304
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
307	[number]	k4	307
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
325	[number]	k4	325
a	a	k8xC	a
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
567	[number]	k4	567
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc4	seznam
obcí	obec	k1gFnPc2	obec
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
částí	část	k1gFnPc2	část
==	==	k?	==
</s>
</p>
<p>
<s>
Města	město	k1gNnPc1	město
jsou	být	k5eAaImIp3nP	být
uvedena	uvést	k5eAaPmNgNnP	uvést
tučně	tučně	k6eAd1	tučně
<g/>
,	,	kIx,	,
městyse	městys	k1gInPc1	městys
kurzívou	kurzíva	k1gFnSc7	kurzíva
<g/>
,	,	kIx,	,
části	část	k1gFnSc3	část
obcí	obec	k1gFnPc2	obec
malince	malinko	k6eAd1	malinko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Batňovice	Batňovice	k1gFnSc1	Batňovice
•	•	k?	•
</s>
</p>
<p>
<s>
Bernartice	Bernartice	k1gFnSc1	Bernartice
(	(	kIx(	(
<g/>
Křenov	Křenov	k1gInSc1	Křenov
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
Třemešná	Třemešná	k1gFnSc1	Třemešná
(	(	kIx(	(
<g/>
Nové	Nové	k2eAgInPc1d1	Nové
Lesy	les	k1gInPc1	les
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Bílé	bílý	k2eAgMnPc4d1	bílý
Poličany	Poličan	k1gMnPc4	Poličan
•	•	k?	•
</s>
</p>
<p>
<s>
Borovnice	borovnice	k1gFnSc1	borovnice
•	•	k?	•
</s>
</p>
<p>
<s>
Borovnička	Borovnička	k1gFnSc1	Borovnička
•	•	k?	•
</s>
</p>
<p>
<s>
Čermná	Čermný	k2eAgFnSc1d1	Čermná
•	•	k?	•
</s>
</p>
<p>
<s>
Černý	černý	k2eAgInSc1d1	černý
Důl	důl	k1gInSc1	důl
(	(	kIx(	(
<g/>
Čistá	čistá	k1gFnSc1	čistá
v	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
•	•	k?	•
Fořt	Fořt	k1gMnSc1	Fořt
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Dolní	dolní	k2eAgFnSc1d1	dolní
Branná	branný	k2eAgFnSc1d1	Branná
•	•	k?	•
</s>
</p>
<p>
<s>
Dolní	dolní	k2eAgFnSc1d1	dolní
Brusnice	brusnice	k1gFnSc1	brusnice
•	•	k?	•
</s>
</p>
<p>
<s>
Dolní	dolní	k2eAgInSc1d1	dolní
Dvůr	Dvůr	k1gInSc1	Dvůr
•	•	k?	•
</s>
</p>
<p>
<s>
Dolní	dolní	k2eAgFnSc1d1	dolní
Kalná	Kalná	k1gFnSc1	Kalná
(	(	kIx(	(
<g/>
Slemeno	slemeno	k1gNnSc1	slemeno
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Dolní	dolní	k2eAgInSc1d1	dolní
Lánov	Lánov	k1gInSc1	Lánov
•	•	k?	•
</s>
</p>
<p>
<s>
Dolní	dolní	k2eAgFnSc1d1	dolní
Olešnice	Olešnice	k1gFnSc1	Olešnice
•	•	k?	•
</s>
</p>
<p>
<s>
Doubravice	Doubravice	k1gFnSc1	Doubravice
(	(	kIx(	(
<g/>
Velehrádek	Velehrádek	k1gInSc1	Velehrádek
•	•	k?	•
Zálesí	Zálesí	k1gNnSc1	Zálesí
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Dubenec	Dubenec	k1gInSc1	Dubenec
•	•	k?	•
</s>
</p>
<p>
<s>
Dvůr	Dvůr	k1gInSc1	Dvůr
Králové	Králová	k1gFnSc2	Králová
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
(	(	kIx(	(
<g/>
Lipnice	Lipnice	k1gFnSc2	Lipnice
•	•	k?	•
Verdek	Verdek	k1gMnSc1	Verdek
•	•	k?	•
Zboží	zboží	k1gNnSc4	zboží
•	•	k?	•
Žireč	Žireč	k1gInSc1	Žireč
•	•	k?	•
Žirecká	Žirecká	k1gFnSc1	Žirecká
Podstráň	Podstráň	k1gFnSc1	Podstráň
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Hajnice	hajnice	k1gFnSc1	hajnice
(	(	kIx(	(
<g/>
Horní	horní	k2eAgInSc1d1	horní
Žďár	Žďár	k1gInSc1	Žďár
•	•	k?	•
Výšinka	výšinka	k1gFnSc1	výšinka
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Havlovice	Havlovice	k1gFnSc1	Havlovice
•	•	k?	•
</s>
</p>
<p>
<s>
Horní	horní	k2eAgFnSc1d1	horní
Brusnice	brusnice	k1gFnSc1	brusnice
•	•	k?	•
</s>
</p>
<p>
<s>
Horní	horní	k2eAgFnSc1d1	horní
Kalná	Kalná	k1gFnSc1	Kalná
•	•	k?	•
</s>
</p>
<p>
<s>
Horní	horní	k2eAgInSc1d1	horní
Maršov	Maršov	k1gInSc1	Maršov
(	(	kIx(	(
<g/>
Dolní	dolní	k2eAgFnPc1d1	dolní
Albeřice	Albeřice	k1gFnPc1	Albeřice
•	•	k?	•
Dolní	dolní	k2eAgFnPc1d1	dolní
Lysečiny	Lysečina	k1gFnPc1	Lysečina
•	•	k?	•
Horní	horní	k2eAgFnSc2d1	horní
Albeřice	Albeřice	k1gFnSc2	Albeřice
•	•	k?	•
Horní	horní	k2eAgFnSc2d1	horní
Lysečiny	Lysečina	k1gFnSc2	Lysečina
•	•	k?	•
Temný	temný	k2eAgInSc1d1	temný
Důl	důl	k1gInSc1	důl
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Horní	horní	k2eAgFnSc1d1	horní
Olešnice	Olešnice	k1gFnSc1	Olešnice
(	(	kIx(	(
<g/>
Ždírnice	Ždírnice	k1gFnSc1	Ždírnice
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Hostinné	hostinný	k2eAgMnPc4d1	hostinný
•	•	k?	•
</s>
</p>
<p>
<s>
Hřibojedy	Hřibojed	k1gMnPc4	Hřibojed
(	(	kIx(	(
<g/>
Hvězda	Hvězda	k1gMnSc1	Hvězda
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Chotěvice	Chotěvice	k1gFnSc1	Chotěvice
•	•	k?	•
</s>
</p>
<p>
<s>
Choustníkovo	Choustníkův	k2eAgNnSc1d1	Choustníkovo
Hradiště	Hradiště	k1gNnSc1	Hradiště
•	•	k?	•
</s>
</p>
<p>
<s>
Chvaleč	Chvaleč	k1gInSc1	Chvaleč
(	(	kIx(	(
<g/>
Petříkovice	Petříkovice	k1gFnSc1	Petříkovice
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Janské	janský	k2eAgFnPc1d1	Janská
Lázně	lázeň	k1gFnPc1	lázeň
•	•	k?	•
</s>
</p>
<p>
<s>
Jívka	jívka	k1gFnSc1	jívka
•	•	k?	•
</s>
</p>
<p>
<s>
Klášterská	klášterský	k2eAgFnSc1d1	Klášterská
Lhota	Lhota	k1gFnSc1	Lhota
•	•	k?	•
</s>
</p>
<p>
<s>
Kocbeře	Kocbera	k1gFnSc3	Kocbera
(	(	kIx(	(
<g/>
Nová	nový	k2eAgFnSc1d1	nová
Ves	ves	k1gFnSc1	ves
•	•	k?	•
Nové	Nové	k2eAgFnSc3d1	Nové
Kocbeře	Kocbera	k1gFnSc3	Kocbera
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Kohoutov	Kohoutov	k1gInSc1	Kohoutov
•	•	k?	•
</s>
</p>
<p>
<s>
Královec	Královec	k1gInSc1	Královec
•	•	k?	•
</s>
</p>
<p>
<s>
Kuks	Kuks	k1gInSc1	Kuks
(	(	kIx(	(
<g/>
Kašov	Kašov	k1gInSc1	Kašov
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Kunčice	Kunčice	k1gFnPc1	Kunčice
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
•	•	k?	•
</s>
</p>
<p>
<s>
Lampertice	Lampertice	k1gFnSc1	Lampertice
•	•	k?	•
</s>
</p>
<p>
<s>
Lánov	Lánov	k1gInSc1	Lánov
(	(	kIx(	(
<g/>
Horní	horní	k2eAgInSc1d1	horní
Lánov	Lánov	k1gInSc1	Lánov
•	•	k?	•
Prostřední	prostřední	k2eAgInSc1d1	prostřední
Lánov	Lánov	k1gInSc1	Lánov
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Lanžov	Lanžov	k1gInSc1	Lanžov
(	(	kIx(	(
<g/>
Lhotka	Lhotka	k1gFnSc1	Lhotka
•	•	k?	•
Miřejov	Miřejov	k1gInSc1	Miřejov
•	•	k?	•
Sedlec	Sedlec	k1gInSc1	Sedlec
•	•	k?	•
Záborov	Záborov	k1gInSc1	Záborov
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Libňatov	Libňatov	k1gInSc1	Libňatov
•	•	k?	•
</s>
</p>
<p>
<s>
Libotov	Libotov	k1gInSc1	Libotov
•	•	k?	•
</s>
</p>
<p>
<s>
Litíč	Litíč	k1gFnSc1	Litíč
(	(	kIx(	(
<g/>
Nouzov	Nouzov	k1gInSc1	Nouzov
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Malá	malý	k2eAgFnSc1d1	malá
Úpa	Úpa	k1gFnSc1	Úpa
(	(	kIx(	(
<g/>
Dolní	dolní	k2eAgFnSc1d1	dolní
Malá	malý	k2eAgFnSc1d1	malá
Úpa	Úpa	k1gFnSc1	Úpa
•	•	k?	•
Horní	horní	k2eAgFnSc1d1	horní
Malá	malý	k2eAgFnSc1d1	malá
Úpa	Úpa	k1gFnSc1	Úpa
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Malé	Malé	k2eAgFnPc1d1	Malé
Svatoňovice	Svatoňovice	k1gFnPc1	Svatoňovice
(	(	kIx(	(
<g/>
Odolov	Odolov	k1gInSc1	Odolov
•	•	k?	•
Petrovice	Petrovice	k1gFnSc2	Petrovice
•	•	k?	•
Strážkovice	Strážkovice	k1gFnSc2	Strážkovice
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Maršov	Maršov	k1gInSc1	Maršov
u	u	k7c2	u
Úpice	Úpice	k1gFnSc2	Úpice
•	•	k?	•
</s>
</p>
<p>
<s>
Mladé	mladý	k2eAgInPc1d1	mladý
Buky	buk	k1gInPc1	buk
(	(	kIx(	(
<g/>
Hertvíkovice	Hertvíkovice	k1gFnSc1	Hertvíkovice
•	•	k?	•
Kalná	kalný	k2eAgFnSc1d1	kalná
Voda	voda	k1gFnSc1	voda
•	•	k?	•
Sklenářovice	Sklenářovice	k1gFnSc1	Sklenářovice
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Mostek	mostek	k1gInSc1	mostek
(	(	kIx(	(
<g/>
Debrné	Debrný	k2eAgFnSc6d1	Debrný
•	•	k?	•
Souvrať	souvrať	k1gFnSc4	souvrať
•	•	k?	•
Zadní	zadní	k2eAgInSc4d1	zadní
Mostek	mostek	k1gInSc4	mostek
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Nemojov	Nemojov	k1gInSc1	Nemojov
(	(	kIx(	(
<g/>
Dolní	dolní	k2eAgInSc1d1	dolní
Nemojov	Nemojov	k1gInSc1	Nemojov
•	•	k?	•
Horní	horní	k2eAgInSc1d1	horní
Nemojov	Nemojov	k1gInSc1	Nemojov
•	•	k?	•
Nový	nový	k2eAgInSc1d1	nový
Nemojov	Nemojov	k1gInSc1	Nemojov
•	•	k?	•
Starobucké	Starobucký	k2eAgFnSc3d1	Starobucká
Debrné	Debrný	k2eAgFnSc3d1	Debrný
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Pec	Pec	k1gFnSc1	Pec
pod	pod	k7c7	pod
Sněžkou	Sněžka	k1gFnSc7	Sněžka
(	(	kIx(	(
<g/>
Velká	velký	k2eAgFnSc1d1	velká
Úpa	Úpa	k1gFnSc1	Úpa
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Pilníkov	Pilníkov	k1gInSc1	Pilníkov
•	•	k?	•
</s>
</p>
<p>
<s>
Prosečné	Prosečný	k2eAgInPc1d1	Prosečný
•	•	k?	•
</s>
</p>
<p>
<s>
Radvanice	Radvanice	k1gFnSc1	Radvanice
•	•	k?	•
</s>
</p>
<p>
<s>
Rtyně	Rtyně	k6eAd1	Rtyně
v	v	k7c6	v
Podkrkonoší	Podkrkonoší	k1gNnSc6	Podkrkonoší
•	•	k?	•
</s>
</p>
<p>
<s>
Rudník	rudník	k1gMnSc1	rudník
(	(	kIx(	(
<g/>
Arnultovice	Arnultovice	k1gFnSc1	Arnultovice
•	•	k?	•
Javorník	Javorník	k1gInSc1	Javorník
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Stanovice	Stanovice	k1gFnSc1	Stanovice
•	•	k?	•
</s>
</p>
<p>
<s>
Staré	Staré	k2eAgInPc1d1	Staré
Buky	buk	k1gInPc1	buk
(	(	kIx(	(
<g/>
Dolní	dolní	k2eAgInPc1d1	dolní
Staré	Staré	k2eAgInPc1d1	Staré
Buky	buk	k1gInPc1	buk
•	•	k?	•
Horní	horní	k2eAgInPc1d1	horní
Staré	Staré	k2eAgInPc1d1	Staré
Buky	buk	k1gInPc1	buk
•	•	k?	•
Prostřední	prostřednět	k5eAaImIp3nP	prostřednět
Staré	Staré	k2eAgInPc1d1	Staré
Buky	buk	k1gInPc1	buk
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Strážné	strážná	k1gFnSc3	strážná
•	•	k?	•
</s>
</p>
<p>
<s>
Suchovršice	Suchovršice	k1gFnSc1	Suchovršice
•	•	k?	•
</s>
</p>
<p>
<s>
Svoboda	Svoboda	k1gMnSc1	Svoboda
nad	nad	k7c7	nad
Úpou	Úpa	k1gFnSc7	Úpa
•	•	k?	•
</s>
</p>
<p>
<s>
Špindlerův	Špindlerův	k2eAgInSc1d1	Špindlerův
Mlýn	mlýn	k1gInSc1	mlýn
(	(	kIx(	(
<g/>
Bedřichov	Bedřichov	k1gInSc1	Bedřichov
•	•	k?	•
Labská	labský	k2eAgFnSc1d1	Labská
•	•	k?	•
Přední	přední	k2eAgFnSc1d1	přední
Labská	labský	k2eAgFnSc1d1	Labská
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Trotina	Trotina	k1gFnSc1	Trotina
•	•	k?	•
</s>
</p>
<p>
<s>
Trutnov	Trutnov	k1gInSc1	Trutnov
(	(	kIx(	(
<g/>
Adamov	Adamov	k1gInSc1	Adamov
•	•	k?	•
Babí	babit	k5eAaImIp3nS	babit
•	•	k?	•
Bohuslavice	Bohuslavice	k1gFnPc4	Bohuslavice
•	•	k?	•
Bojiště	bojiště	k1gNnSc1	bojiště
•	•	k?	•
Dolní	dolní	k2eAgNnSc1d1	dolní
Předměstí	předměstí	k1gNnSc1	předměstí
•	•	k?	•
Dolní	dolní	k2eAgNnSc1d1	dolní
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
•	•	k?	•
Horní	horní	k2eAgNnSc1d1	horní
Předměstí	předměstí	k1gNnSc1	předměstí
•	•	k?	•
Horní	horní	k2eAgFnSc2d1	horní
Staré	Stará	k1gFnSc2	Stará
Město	město	k1gNnSc1	město
•	•	k?	•
Kryblice	Kryblice	k1gFnSc2	Kryblice
•	•	k?	•
Lhota	Lhota	k1gMnSc1	Lhota
•	•	k?	•
Libeč	Libeč	k1gInSc1	Libeč
•	•	k?	•
Nový	nový	k2eAgInSc1d1	nový
Rokytník	Rokytník	k1gInSc1	Rokytník
•	•	k?	•
Oblanov	Oblanov	k1gInSc1	Oblanov
•	•	k?	•
Poříčí	Poříčí	k1gNnSc2	Poříčí
•	•	k?	•
Starý	starý	k2eAgInSc1d1	starý
Rokytník	Rokytník	k1gInSc1	Rokytník
•	•	k?	•
Střední	střední	k2eAgNnSc1d1	střední
Předměstí	předměstí	k1gNnSc1	předměstí
•	•	k?	•
Střítež	Střítež	k1gFnSc1	Střítež
•	•	k?	•
Studenec	Studenec	k1gMnSc1	Studenec
•	•	k?	•
Vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
Město	město	k1gNnSc1	město
•	•	k?	•
Volanov	Volanovo	k1gNnPc2	Volanovo
•	•	k?	•
Voletiny	Voletin	k2eAgFnPc1d1	Voletin
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Třebihošť	Třebihoštit	k5eAaPmRp2nS	Třebihoštit
(	(	kIx(	(
<g/>
Dolní	dolní	k2eAgInSc1d1	dolní
Dehtov	Dehtov	k1gInSc1	Dehtov
•	•	k?	•
Horní	horní	k2eAgInSc1d1	horní
Dehtov	Dehtov	k1gInSc1	Dehtov
•	•	k?	•
Zvičina	Zvičina	k1gFnSc1	Zvičina
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Úpice	Úpice	k1gFnSc1	Úpice
(	(	kIx(	(
<g/>
Radeč	Radeč	k1gInSc1	Radeč
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Velké	velký	k2eAgFnPc1d1	velká
Svatoňovice	Svatoňovice	k1gFnPc1	Svatoňovice
(	(	kIx(	(
<g/>
Markoušovice	Markoušovice	k1gFnSc1	Markoušovice
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Velký	velký	k2eAgInSc1d1	velký
Vřešťov	Vřešťov	k1gInSc1	Vřešťov
•	•	k?	•
</s>
</p>
<p>
<s>
Vilantice	Vilantice	k1gFnSc1	Vilantice
(	(	kIx(	(
<g/>
Chotěborky	Chotěborek	k1gInPc1	Chotěborek
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Vítězná	vítězný	k2eAgFnSc1d1	vítězná
(	(	kIx(	(
<g/>
Bukovina	Bukovina	k1gFnSc1	Bukovina
•	•	k?	•
Hájemství	hájemství	k1gNnSc2	hájemství
•	•	k?	•
Huntířov	Huntířov	k1gInSc1	Huntířov
•	•	k?	•
Kocléřov	Kocléřov	k1gInSc1	Kocléřov
•	•	k?	•
Komárov	Komárov	k1gInSc1	Komárov
•	•	k?	•
Nové	Nová	k1gFnSc2	Nová
Záboří	Záboří	k1gNnSc2	Záboří
•	•	k?	•
Záboří	Záboří	k1gNnSc2	Záboří
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Vlčice	vlčice	k1gFnSc1	vlčice
•	•	k?	•
</s>
</p>
<p>
<s>
Vlčkovice	Vlčkovice	k1gFnSc1	Vlčkovice
v	v	k7c6	v
Podkrkonoší	Podkrkonoší	k1gNnSc6	Podkrkonoší
(	(	kIx(	(
<g/>
Dolní	dolní	k2eAgFnSc1d1	dolní
Vlčkovice	Vlčkovice	k1gFnSc1	Vlčkovice
•	•	k?	•
Horní	horní	k2eAgFnSc1d1	horní
Vlčkovice	Vlčkovice	k1gFnSc1	Vlčkovice
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
(	(	kIx(	(
<g/>
Hořejší	Hořejší	k2eAgNnSc1d1	Hořejší
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
•	•	k?	•
Podhůří	Podhůří	k1gNnSc1	Podhůří
•	•	k?	•
Vrchlabí	Vrchlabí	k1gNnPc2	Vrchlabí
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Zábřezí-Řečice	Zábřezí-Řečice	k1gFnSc1	Zábřezí-Řečice
(	(	kIx(	(
<g/>
Řečice	Řečice	k1gFnSc1	Řečice
•	•	k?	•
Zábřezí	Zábřeze	k1gFnPc2	Zábřeze
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Zdobín	Zdobín	k1gInSc1	Zdobín
•	•	k?	•
</s>
</p>
<p>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
Olešnice	Olešnice	k1gFnSc1	Olešnice
•	•	k?	•
</s>
</p>
<p>
<s>
Žacléř	Žacléř	k1gInSc1	Žacléř
(	(	kIx(	(
<g/>
Bobr	bobr	k1gMnSc1	bobr
•	•	k?	•
Prkenný	prkenný	k2eAgInSc4d1	prkenný
Důl	důl	k1gInSc4	důl
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Trutnov	Trutnov	k1gInSc1	Trutnov
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
chráněných	chráněný	k2eAgNnPc2d1	chráněné
území	území	k1gNnPc2	území
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Trutnov	Trutnov	k1gInSc1	Trutnov
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
památných	památný	k2eAgInPc2d1	památný
stromů	strom	k1gInPc2	strom
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Trutnov	Trutnov	k1gInSc1	Trutnov
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Trutnov	Trutnov	k1gInSc1	Trutnov
</s>
</p>
<p>
<s>
Senátní	senátní	k2eAgInSc1d1	senátní
obvod	obvod	k1gInSc1	obvod
č.	č.	k?	č.
39	[number]	k4	39
-	-	kIx~	-
Trutnov	Trutnov	k1gInSc1	Trutnov
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
okres	okres	k1gInSc1	okres
Trutnov	Trutnov	k1gInSc4	Trutnov
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
