<s>
Bříza	bříza	k1gFnSc1
(	(	kIx(
<g/>
Všestary	Všestar	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
Bříza	bříza	k1gFnSc1
Bříza	bříza	k1gFnSc1
čp.	čp.	k?
9	#num#	k4
<g/>
Lokalita	lokalita	k1gFnSc1
Charakter	charakter	k1gInSc1
</s>
<s>
vesnice	vesnice	k1gFnSc1
Obec	obec	k1gFnSc1
</s>
<s>
Všestary	Všestara	k1gFnPc1
Okres	okres	k1gInSc1
</s>
<s>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
Kraj	kraj	k7c2
</s>
<s>
Královéhradecký	královéhradecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Historická	historický	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Čechy	Čechy	k1gFnPc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
14	#num#	k4
<g/>
′	′	k?
<g/>
29	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
15	#num#	k4
<g/>
°	°	k?
<g/>
45	#num#	k4
<g/>
′	′	k?
<g/>
53	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
238	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Katastrální	katastrální	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Bříza	bříza	k1gFnSc1
u	u	k7c2
Všestar	Všestara	k1gFnPc2
(	(	kIx(
<g/>
2,28	2,28	k4
km²	km²	k?
<g/>
)	)	kIx)
PSČ	PSČ	kA
</s>
<s>
503	#num#	k4
12	#num#	k4
Počet	počet	k1gInSc1
domů	domů	k6eAd1
</s>
<s>
72	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bříza	bříza	k1gFnSc1
</s>
<s>
Další	další	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Kód	kód	k1gInSc4
části	část	k1gFnPc1
obce	obec	k1gFnSc2
</s>
<s>
187402	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Zdroje	zdroj	k1gInSc2
k	k	k7c3
infoboxu	infobox	k1gInSc3
a	a	k8xC
českým	český	k2eAgNnPc3d1
sídlům	sídlo	k1gNnPc3
<g/>
.	.	kIx.
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Bříza	bříza	k1gFnSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Birk	Birk	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vesnice	vesnice	k1gFnSc1
<g/>
,	,	kIx,
část	část	k1gFnSc1
obce	obec	k1gFnSc2
Všestary	Všestara	k1gFnSc2
v	v	k7c6
okrese	okres	k1gInSc6
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
asi	asi	k9
2	#num#	k4
km	km	kA
na	na	k7c4
jih	jih	k1gInSc4
od	od	k7c2
Všestar	Všestary	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
zde	zde	k6eAd1
bylo	být	k5eAaImAgNnS
evidováno	evidovat	k5eAaImNgNnS
86	#num#	k4
adres	adresa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
zde	zde	k6eAd1
trvale	trvale	k6eAd1
žilo	žít	k5eAaImAgNnS
231	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bříza	bříza	k1gFnSc1
leží	ležet	k5eAaImIp3nS
v	v	k7c6
katastrálním	katastrální	k2eAgNnSc6d1
území	území	k1gNnSc6
Bříza	bříza	k1gFnSc1
u	u	k7c2
Všestar	Všestara	k1gFnPc2
o	o	k7c6
rozloze	rozloha	k1gFnSc6
2,28	2,28	k4
km	km	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Vesnice	vesnice	k1gFnSc1
se	se	k3xPyFc4
připomíná	připomínat	k5eAaImIp3nS
roku	rok	k1gInSc2
1376	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
vlastnicky	vlastnicky	k6eAd1
spojena	spojit	k5eAaPmNgFnS
sr	sr	k?
sídlem	sídlo	k1gNnSc7
a	a	k8xC
tvrzí	tvrz	k1gFnSc7
v	v	k7c6
Plotišti	Plotiště	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgMnSc1
majitel	majitel	k1gMnSc1
se	se	k3xPyFc4
připomíná	připomínat	k5eAaImIp3nS
v	v	k7c6
letech	léto	k1gNnPc6
1383	#num#	k4
<g/>
-	-	kIx~
<g/>
1411	#num#	k4
<g/>
,	,	kIx,
jmenoval	jmenovat	k5eAaBmAgMnS,k5eAaImAgMnS
se	se	k3xPyFc4
Prokop	Prokop	k1gMnSc1
Roblík	Roblík	k1gMnSc1
nebo	nebo	k8xC
Rohlík	Rohlík	k1gMnSc1
a	a	k8xC
založil	založit	k5eAaPmAgMnS
ve	v	k7c6
vsi	ves	k1gFnSc6
tvrz	tvrz	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
husitské	husitský	k2eAgFnSc2d1
ji	on	k3xPp3gFnSc4
vlastnil	vlastnit	k5eAaImAgMnS
jeho	jeho	k3xOp3gMnSc1
syn	syn	k1gMnSc1
Mikeš	Mikeš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1486	#num#	k4
ji	on	k3xPp3gFnSc4
odúmrti	odúmrť	k1gFnPc4
získal	získat	k5eAaPmAgMnS
král	král	k1gMnSc1
Vladislav	Vladislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jagellonský	jagellonský	k2eAgInSc4d1
a	a	k8xC
vzápětí	vzápětí	k6eAd1
ji	on	k3xPp3gFnSc4
daroval	darovat	k5eAaPmAgMnS
Mikuláši	Mikuláš	k1gMnPc1
mladšímu	mladý	k2eAgMnSc3d2
Trčkovi	Trčka	k1gMnSc3
z	z	k7c2
Lípy	lípa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvrz	tvrz	k1gFnSc1
zanikla	zaniknout	k5eAaPmAgFnS
beze	beze	k7c2
stop	stopa	k1gFnPc2
a	a	k8xC
nebyla	být	k5eNaImAgFnS
nalezena	nalezen	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Historický	historický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
–	–	k?
1869	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
<g/>
.	.	kIx.
21	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Ministerstvo	ministerstvo	k1gNnSc1
vnitra	vnitro	k1gNnSc2
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Adresy	adresa	k1gFnSc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009-10-10	2009-10-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historický	historický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
ČR	ČR	kA
1869	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2007-03-03	2007-03-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
464	#num#	k4
<g/>
,	,	kIx,
465	#num#	k4
<g/>
,	,	kIx,
záznam	záznam	k1gInSc1
97	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Územně	územně	k6eAd1
identifikační	identifikační	k2eAgInSc4d1
registr	registr	k1gInSc4
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Územně	územně	k6eAd1
identifikační	identifikační	k2eAgInSc1d1
registr	registr	k1gInSc1
ČR	ČR	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1999-01-01	1999-01-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
<g/>
.	.	kIx.
↑	↑	k?
Hrady	hrad	k1gInPc1
<g/>
,	,	kIx,
zámky	zámek	k1gInPc1
a	a	k8xC
tvrze	tvrz	k1gFnPc1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
na	na	k7c6
Moravě	Morava	k1gFnSc6
a	a	k8xC
ve	v	k7c6
Slezsku	Slezsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tomáš	Tomáš	k1gMnSc1
Šimek	Šimek	k1gMnSc1
(	(	kIx(
<g/>
editor	editor	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
kolewkjtiv	kolewkjtiv	k1gInSc1
<g/>
.	.	kIx.
díl	díl	k1gInSc1
6	#num#	k4
<g/>
,	,	kIx,
Svoboda	svoboda	k1gFnSc1
Praha	Praha	k1gFnSc1
1989	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
59	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Bříza	bříza	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
katastru	katastr	k1gInSc2
Bříza	bříza	k1gFnSc1
u	u	k7c2
Všestar	Všestara	k1gFnPc2
na	na	k7c6
webu	web	k1gInSc6
ČÚZK	ČÚZK	kA
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Části	část	k1gFnPc1
obce	obec	k1gFnSc2
Všestary	Všestara	k1gFnSc2
Části	část	k1gFnSc2
obce	obec	k1gFnSc2
</s>
<s>
BřízaChlumLípaRosniceRozběřiceVšestary	BřízaChlumLípaRosniceRozběřiceVšestara	k1gFnPc1
</s>
