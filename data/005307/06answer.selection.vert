<s>
Má	můj	k3xOp1gFnSc1	můj
vlast	vlast	k1gFnSc1	vlast
je	být	k5eAaImIp3nS	být
cyklus	cyklus	k1gInSc4	cyklus
šesti	šest	k4xCc2	šest
symfonických	symfonický	k2eAgFnPc2d1	symfonická
básní	báseň	k1gFnPc2	báseň
s	s	k7c7	s
mimohudební	mimohudební	k2eAgFnSc7d1	mimohudební
tematikou	tematika	k1gFnSc7	tematika
českého	český	k2eAgMnSc2d1	český
skladatele	skladatel	k1gMnSc2	skladatel
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Smetany	Smetana	k1gMnSc2	Smetana
<g/>
,	,	kIx,	,
inspirovaný	inspirovaný	k2eAgInSc1d1	inspirovaný
českou	český	k2eAgFnSc7d1	Česká
historií	historie	k1gFnSc7	historie
<g/>
,	,	kIx,	,
legendami	legenda	k1gFnPc7	legenda
a	a	k8xC	a
krajinou	krajina	k1gFnSc7	krajina
<g/>
.	.	kIx.	.
</s>
