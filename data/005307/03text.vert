<s>
Má	můj	k3xOp1gFnSc1	můj
vlast	vlast	k1gFnSc1	vlast
je	být	k5eAaImIp3nS	být
cyklus	cyklus	k1gInSc4	cyklus
šesti	šest	k4xCc2	šest
symfonických	symfonický	k2eAgFnPc2d1	symfonická
básní	báseň	k1gFnPc2	báseň
s	s	k7c7	s
mimohudební	mimohudební	k2eAgFnSc7d1	mimohudební
tematikou	tematika	k1gFnSc7	tematika
českého	český	k2eAgMnSc2d1	český
skladatele	skladatel	k1gMnSc2	skladatel
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Smetany	Smetana	k1gMnSc2	Smetana
<g/>
,	,	kIx,	,
inspirovaný	inspirovaný	k2eAgInSc1d1	inspirovaný
českou	český	k2eAgFnSc7d1	Česká
historií	historie	k1gFnSc7	historie
<g/>
,	,	kIx,	,
legendami	legenda	k1gFnPc7	legenda
a	a	k8xC	a
krajinou	krajina	k1gFnSc7	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
části	část	k1gFnPc1	část
Mé	můj	k3xOp1gFnSc2	můj
vlasti	vlast	k1gFnSc2	vlast
jsou	být	k5eAaImIp3nP	být
Vyšehrad	Vyšehrad	k1gInSc4	Vyšehrad
<g/>
,	,	kIx,	,
Vltava	Vltava	k1gFnSc1	Vltava
<g/>
,	,	kIx,	,
Šárka	Šárka	k1gFnSc1	Šárka
<g/>
,	,	kIx,	,
Z	z	k7c2	z
českých	český	k2eAgInPc2d1	český
luhů	luh	k1gInPc2	luh
a	a	k8xC	a
hájů	háj	k1gInPc2	háj
<g/>
,	,	kIx,	,
Tábor	Tábor	k1gInSc1	Tábor
a	a	k8xC	a
Blaník	Blaník	k1gInSc1	Blaník
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
Smetanovo	Smetanův	k2eAgNnSc4d1	Smetanovo
vrcholné	vrcholný	k2eAgNnSc4d1	vrcholné
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1874	[number]	k4	1874
až	až	k9	až
1879	[number]	k4	1879
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
zcela	zcela	k6eAd1	zcela
hluchý	hluchý	k2eAgMnSc1d1	hluchý
<g/>
.	.	kIx.	.
</s>
<s>
Smetana	Smetana	k1gMnSc1	Smetana
začal	začít	k5eAaPmAgMnS	začít
komponovat	komponovat	k5eAaImF	komponovat
první	první	k4xOgFnSc4	první
skladbu	skladba	k1gFnSc4	skladba
cyklu	cyklus	k1gInSc2	cyklus
–	–	k?	–
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
–	–	k?	–
koncem	koncem	k7c2	koncem
září	září	k1gNnSc2	září
1874	[number]	k4	1874
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ještě	ještě	k6eAd1	ještě
nebyl	být	k5eNaImAgMnS	být
zbaven	zbavit	k5eAaPmNgMnS	zbavit
sluchu	sluch	k1gInSc2	sluch
úplně	úplně	k6eAd1	úplně
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
během	během	k7c2	během
října	říjen	k1gInSc2	říjen
ztratil	ztratit	k5eAaPmAgInS	ztratit
i	i	k9	i
jeho	jeho	k3xOp3gInPc4	jeho
poslední	poslední	k2eAgInPc4d1	poslední
zbytky	zbytek	k1gInPc4	zbytek
<g/>
.	.	kIx.	.
</s>
<s>
Skladbu	skladba	k1gFnSc4	skladba
dokončil	dokončit	k5eAaPmAgMnS	dokončit
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
již	již	k6eAd1	již
jako	jako	k8xC	jako
úplně	úplně	k6eAd1	úplně
neslyšící	slyšící	k2eNgNnPc1d1	neslyšící
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
ztráta	ztráta	k1gFnSc1	ztráta
sluchu	sluch	k1gInSc3	sluch
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
Smetanu	smetana	k1gFnSc4	smetana
skličující	skličující	k2eAgFnSc1d1	skličující
událost	událost	k1gFnSc1	událost
<g/>
,	,	kIx,	,
nějak	nějak	k6eAd1	nějak
podepsala	podepsat	k5eAaPmAgFnS	podepsat
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1874	[number]	k4	1874
ještě	ještě	k6eAd1	ještě
zkomponoval	zkomponovat	k5eAaPmAgMnS	zkomponovat
Vltavu	Vltava	k1gFnSc4	Vltava
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc4d1	další
dvě	dva	k4xCgFnPc4	dva
básně	báseň	k1gFnPc4	báseň
napsal	napsat	k5eAaBmAgInS	napsat
následující	následující	k2eAgInSc1d1	následující
rok	rok	k1gInSc1	rok
<g/>
.	.	kIx.	.
</s>
<s>
Zaměstnán	zaměstnán	k2eAgInSc1d1	zaměstnán
prací	práce	k1gFnSc7	práce
na	na	k7c6	na
jiných	jiný	k2eAgNnPc6d1	jiné
dílech	dílo	k1gNnPc6	dílo
Smetana	smetana	k1gFnSc1	smetana
na	na	k7c4	na
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
od	od	k7c2	od
tvorby	tvorba	k1gFnSc2	tvorba
symfonických	symfonický	k2eAgFnPc2d1	symfonická
básní	báseň	k1gFnPc2	báseň
s	s	k7c7	s
českou	český	k2eAgFnSc7d1	Česká
národnostní	národnostní	k2eAgFnSc7d1	národnostní
tematikou	tematika	k1gFnSc7	tematika
upustil	upustit	k5eAaPmAgMnS	upustit
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
až	až	k6eAd1	až
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1878	[number]	k4	1878
<g/>
/	/	kIx~	/
<g/>
79	[number]	k4	79
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
poslední	poslední	k2eAgInPc1d1	poslední
dvě	dva	k4xCgFnPc1	dva
skladby	skladba	k1gFnPc1	skladba
Tábor	Tábor	k1gInSc1	Tábor
a	a	k8xC	a
Blaník	Blaník	k1gInSc1	Blaník
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
úzce	úzko	k6eAd1	úzko
navazují	navazovat	k5eAaImIp3nP	navazovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
vzniku	vznik	k1gInSc2	vznik
prvních	první	k4xOgNnPc2	první
děl	dělo	k1gNnPc2	dělo
přitom	přitom	k6eAd1	přitom
Smetana	Smetana	k1gMnSc1	Smetana
ještě	ještě	k6eAd1	ještě
neměl	mít	k5eNaImAgMnS	mít
jasnou	jasný	k2eAgFnSc4d1	jasná
představu	představa	k1gFnSc4	představa
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jakou	jaký	k3yIgFnSc4	jaký
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
cyklus	cyklus	k1gInSc1	cyklus
konečnou	konečný	k2eAgFnSc4d1	konečná
formu	forma	k1gFnSc4	forma
<g/>
;	;	kIx,	;
tu	ten	k3xDgFnSc4	ten
dostal	dostat	k5eAaPmAgMnS	dostat
až	až	k9	až
se	s	k7c7	s
vznikem	vznik	k1gInSc7	vznik
posledních	poslední	k2eAgFnPc2d1	poslední
kompozic	kompozice	k1gFnPc2	kompozice
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
souborné	souborný	k2eAgNnSc1d1	souborné
provedení	provedení	k1gNnSc1	provedení
celého	celý	k2eAgInSc2d1	celý
cyklu	cyklus	k1gInSc2	cyklus
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1882	[number]	k4	1882
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Žofíně	Žofín	k1gInSc6	Žofín
<g/>
,	,	kIx,	,
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
symfonické	symfonický	k2eAgFnPc1d1	symfonická
básně	báseň	k1gFnPc1	báseň
však	však	k9	však
byly	být	k5eAaImAgFnP	být
nacvičeny	nacvičit	k5eAaBmNgInP	nacvičit
a	a	k8xC	a
hrány	hrát	k5eAaImNgInP	hrát
i	i	k9	i
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
době	doba	k1gFnSc6	doba
dozvuků	dozvuk	k1gInPc2	dozvuk
českého	český	k2eAgNnSc2d1	české
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
<g/>
,	,	kIx,	,
a	a	k8xC	a
i	i	k9	i
když	když	k8xS	když
jej	on	k3xPp3gMnSc4	on
zpočátku	zpočátku	k6eAd1	zpočátku
část	část	k1gFnSc1	část
české	český	k2eAgFnSc2d1	Česká
veřejnosti	veřejnost	k1gFnSc2	veřejnost
nepřijala	přijmout	k5eNaPmAgFnS	přijmout
zcela	zcela	k6eAd1	zcela
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
stalo	stát	k5eAaPmAgNnS	stát
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
vrcholných	vrcholný	k2eAgNnPc2d1	vrcholné
děl	dělo	k1gNnPc2	dělo
české	český	k2eAgFnSc2d1	Česká
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Má	můj	k3xOp1gFnSc1	můj
vlast	vlast	k1gFnSc1	vlast
byla	být	k5eAaImAgFnS	být
vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc1	první
nahrávka	nahrávka	k1gFnSc1	nahrávka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
Česká	český	k2eAgFnSc1d1	Česká
filharmonie	filharmonie	k1gFnSc1	filharmonie
na	na	k7c4	na
gramofonovou	gramofonový	k2eAgFnSc4d1	gramofonová
desku	deska	k1gFnSc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
výročí	výročí	k1gNnSc2	výročí
Smetanova	Smetanův	k2eAgNnSc2d1	Smetanovo
úmrtí	úmrtí	k1gNnSc2	úmrtí
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
první	první	k4xOgNnSc1	první
zahajovací	zahajovací	k2eAgNnSc1d1	zahajovací
dílo	dílo	k1gNnSc1	dílo
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
Pražské	pražský	k2eAgNnSc1d1	Pražské
jaro	jaro	k1gNnSc1	jaro
a	a	k8xC	a
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
v	v	k7c4	v
den	den	k1gInSc4	den
české	český	k2eAgFnSc2d1	Česká
státnosti	státnost	k1gFnSc2	státnost
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
polovina	polovina	k1gFnSc1	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
nesla	nést	k5eAaImAgFnS	nést
ve	v	k7c4	v
znamení	znamení	k1gNnSc4	znamení
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
hlavní	hlavní	k2eAgMnPc1d1	hlavní
představitelé	představitel	k1gMnPc1	představitel
jako	jako	k8xC	jako
Josef	Josef	k1gMnSc1	Josef
Dobrovský	Dobrovský	k1gMnSc1	Dobrovský
či	či	k8xC	či
Josef	Josef	k1gMnSc1	Josef
Jungmann	Jungmann	k1gMnSc1	Jungmann
si	se	k3xPyFc3	se
kladli	klást	k5eAaImAgMnP	klást
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
mimo	mimo	k7c4	mimo
oživení	oživení	k1gNnSc4	oživení
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
také	také	k9	také
zvýšení	zvýšení	k1gNnSc1	zvýšení
míry	míra	k1gFnSc2	míra
národnostního	národnostní	k2eAgNnSc2d1	národnostní
uvědomění	uvědomění	k1gNnSc2	uvědomění
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
fáze	fáze	k1gFnSc1	fáze
českého	český	k2eAgNnSc2d1	české
obrození	obrození	k1gNnSc2	obrození
sice	sice	k8xC	sice
probíhaly	probíhat	k5eAaImAgFnP	probíhat
zejména	zejména	k9	zejména
v	v	k7c6	v
desetiletích	desetiletí	k1gNnPc6	desetiletí
kolem	kolem	k7c2	kolem
přelomu	přelom	k1gInSc2	přelom
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
<g/>
,	,	kIx,	,
otázky	otázka	k1gFnPc1	otázka
ohledně	ohledně	k7c2	ohledně
národnostního	národnostní	k2eAgNnSc2d1	národnostní
sebeurčení	sebeurčení	k1gNnSc2	sebeurčení
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
se	se	k3xPyFc4	se
však	však	k9	však
objevovaly	objevovat	k5eAaImAgFnP	objevovat
po	po	k7c6	po
celé	celá	k1gFnSc6	celá
století	století	k1gNnSc2	století
devatenácté	devatenáctý	k4xOgFnSc2	devatenáctý
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
hudební	hudební	k2eAgFnSc6d1	hudební
scéně	scéna	k1gFnSc6	scéna
byla	být	k5eAaImAgFnS	být
diskutována	diskutován	k2eAgFnSc1d1	diskutována
otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInSc7	jaký
způsobem	způsob	k1gInSc7	způsob
hudebně	hudebně	k6eAd1	hudebně
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
stále	stále	k6eAd1	stále
sílící	sílící	k2eAgNnSc1d1	sílící
národnostní	národnostní	k2eAgNnSc1d1	národnostní
uvědomění	uvědomění	k1gNnSc1	uvědomění
Čechů	Čech	k1gMnPc2	Čech
<g/>
;	;	kIx,	;
většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
toho	ten	k3xDgMnSc4	ten
lze	lze	k6eAd1	lze
nejlépe	dobře	k6eAd3	dobře
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
pomocí	pomocí	k7c2	pomocí
tvorby	tvorba	k1gFnSc2	tvorba
hudebních	hudební	k2eAgNnPc2d1	hudební
děl	dělo	k1gNnPc2	dělo
na	na	k7c4	na
lidové	lidový	k2eAgInPc4d1	lidový
motivy	motiv	k1gInPc4	motiv
a	a	k8xC	a
písně	píseň	k1gFnPc4	píseň
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
představiteli	představitel	k1gMnPc7	představitel
tohoto	tento	k3xDgInSc2	tento
názoru	názor	k1gInSc2	názor
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
ztotožnil	ztotožnit	k5eAaPmAgMnS	ztotožnit
i	i	k9	i
Smetana	Smetana	k1gMnSc1	Smetana
<g/>
.	.	kIx.	.
</s>
<s>
Přirozeným	přirozený	k2eAgMnSc7d1	přirozený
hudebním	hudební	k2eAgMnSc7d1	hudební
centrem	centr	k1gMnSc7	centr
české	český	k2eAgFnSc2d1	Česká
hudby	hudba	k1gFnSc2	hudba
byla	být	k5eAaImAgFnS	být
tehdy	tehdy	k6eAd1	tehdy
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1863	[number]	k4	1863
založena	založen	k2eAgFnSc1d1	založena
Umělecká	umělecký	k2eAgFnSc1d1	umělecká
beseda	beseda	k1gFnSc1	beseda
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
členové	člen	k1gMnPc1	člen
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
klasičtí	klasický	k2eAgMnPc1d1	klasický
umělci	umělec	k1gMnPc1	umělec
jako	jako	k8xC	jako
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Hálek	hálka	k1gFnPc2	hálka
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Mánes	Mánes	k1gMnSc1	Mánes
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Evangelista	evangelista	k1gMnSc1	evangelista
Purkyně	Purkyně	k1gFnSc1	Purkyně
nebo	nebo	k8xC	nebo
Bedřich	Bedřich	k1gMnSc1	Bedřich
Smetana	Smetana	k1gMnSc1	Smetana
<g/>
,	,	kIx,	,
zásadním	zásadní	k2eAgInSc7d1	zásadní
způsobem	způsob	k1gInSc7	způsob
přispěli	přispět	k5eAaPmAgMnP	přispět
k	k	k7c3	k
utváření	utváření	k1gNnSc3	utváření
identity	identita	k1gFnSc2	identita
českého	český	k2eAgNnSc2d1	české
národního	národní	k2eAgNnSc2d1	národní
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
existovalo	existovat	k5eAaImAgNnS	existovat
kolem	kolem	k7c2	kolem
dvou	dva	k4xCgNnPc2	dva
set	sto	k4xCgNnPc2	sto
pěveckých	pěvecký	k2eAgMnPc2d1	pěvecký
spolků	spolek	k1gInPc2	spolek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
roku	rok	k1gInSc3	rok
1862	[number]	k4	1862
uspořádaly	uspořádat	k5eAaPmAgFnP	uspořádat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
pěvecký	pěvecký	k2eAgInSc4d1	pěvecký
festival	festival	k1gInSc4	festival
s	s	k7c7	s
ryze	ryze	k6eAd1	ryze
českou	český	k2eAgFnSc7d1	Česká
hudbou	hudba	k1gFnSc7	hudba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
opětovného	opětovný	k2eAgNnSc2d1	opětovné
nabývání	nabývání	k1gNnSc2	nabývání
národnostního	národnostní	k2eAgNnSc2d1	národnostní
uvědomění	uvědomění	k1gNnSc2	uvědomění
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
i	i	k9	i
myšlenka	myšlenka	k1gFnSc1	myšlenka
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
českého	český	k2eAgNnSc2d1	české
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
účelem	účel	k1gInSc7	účel
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Sbor	sbor	k1gInSc1	sbor
pro	pro	k7c4	pro
zbudování	zbudování	k1gNnSc4	zbudování
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
začal	začít	k5eAaPmAgInS	začít
shromažďovat	shromažďovat	k5eAaImF	shromažďovat
potřebné	potřebný	k2eAgInPc4d1	potřebný
finanční	finanční	k2eAgInPc4d1	finanční
prostředky	prostředek	k1gInPc4	prostředek
na	na	k7c4	na
tak	tak	k9	tak
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
stavbu	stavba	k1gFnSc4	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Naléhavá	naléhavý	k2eAgFnSc1d1	naléhavá
potřeba	potřeba	k1gFnSc1	potřeba
zbudovat	zbudovat	k5eAaPmF	zbudovat
hlavní	hlavní	k2eAgNnSc4d1	hlavní
české	český	k2eAgNnSc4d1	české
divadlo	divadlo	k1gNnSc4	divadlo
si	se	k3xPyFc3	se
však	však	k9	však
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
postavení	postavení	k1gNnSc4	postavení
Prozatímního	prozatímní	k2eAgNnSc2d1	prozatímní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
dočkalo	dočkat	k5eAaPmAgNnS	dočkat
otevření	otevření	k1gNnSc1	otevření
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1862	[number]	k4	1862
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
orchestru	orchestr	k1gInSc3	orchestr
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
při	při	k7c6	při
operní	operní	k2eAgFnSc6d1	operní
scéně	scéna	k1gFnSc6	scéna
Prozatímního	prozatímní	k2eAgNnSc2d1	prozatímní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
provádět	provádět	k5eAaImF	provádět
operní	operní	k2eAgFnSc1d1	operní
či	či	k8xC	či
další	další	k2eAgNnPc1d1	další
velká	velký	k2eAgNnPc1d1	velké
orchestrální	orchestrální	k2eAgNnPc1d1	orchestrální
představení	představení	k1gNnPc1	představení
<g/>
,	,	kIx,	,
kterých	který	k3yRgInPc6	který
pěvecké	pěvecký	k2eAgInPc1d1	pěvecký
spolky	spolek	k1gInPc1	spolek
nebyly	být	k5eNaImAgInP	být
schopny	schopen	k2eAgInPc1d1	schopen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1866	[number]	k4	1866
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
po	po	k7c6	po
otevření	otevření	k1gNnSc6	otevření
Prozatímního	prozatímní	k2eAgNnSc2d1	prozatímní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ředitelem	ředitel	k1gMnSc7	ředitel
opery	opera	k1gFnSc2	opera
Bedřich	Bedřich	k1gMnSc1	Bedřich
Smetana	Smetana	k1gMnSc1	Smetana
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dnes	dnes	k6eAd1	dnes
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
vrcholným	vrcholný	k2eAgMnPc3d1	vrcholný
představitelům	představitel	k1gMnPc3	představitel
české	český	k2eAgFnSc2d1	Česká
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
zakladatelům	zakladatel	k1gMnPc3	zakladatel
české	český	k2eAgFnSc2d1	Česká
hudby	hudba	k1gFnSc2	hudba
národní	národní	k2eAgFnSc2d1	národní
<g/>
.	.	kIx.	.
</s>
<s>
Cyklus	cyklus	k1gInSc1	cyklus
šesti	šest	k4xCc2	šest
symfonických	symfonický	k2eAgFnPc2d1	symfonická
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nakonec	nakonec	k6eAd1	nakonec
Smetana	Smetana	k1gMnSc1	Smetana
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
Má	mít	k5eAaImIp3nS	mít
vlast	vlast	k1gFnSc4	vlast
<g/>
,	,	kIx,	,
vznikal	vznikat	k5eAaImAgMnS	vznikat
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1874	[number]	k4	1874
<g/>
–	–	k?	–
<g/>
1879	[number]	k4	1879
<g/>
.	.	kIx.	.
</s>
<s>
Smetana	Smetana	k1gMnSc1	Smetana
jej	on	k3xPp3gMnSc4	on
tvořil	tvořit	k5eAaImAgMnS	tvořit
postupně	postupně	k6eAd1	postupně
a	a	k8xC	a
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
neměl	mít	k5eNaImAgInS	mít
přesný	přesný	k2eAgInSc1d1	přesný
plán	plán	k1gInSc1	plán
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jakou	jaký	k3yIgFnSc4	jaký
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
konečnou	konečný	k2eAgFnSc4d1	konečná
formu	forma	k1gFnSc4	forma
či	či	k8xC	či
kolik	kolik	k9	kolik
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
čtyři	čtyři	k4xCgFnPc4	čtyři
symfonické	symfonický	k2eAgFnPc4d1	symfonická
skladby	skladba	k1gFnPc4	skladba
přitom	přitom	k6eAd1	přitom
vznikaly	vznikat	k5eAaImAgInP	vznikat
nezávisle	závisle	k6eNd1	závisle
na	na	k7c4	na
sobě	se	k3xPyFc3	se
a	a	k8xC	a
až	až	k6eAd1	až
poslední	poslední	k2eAgFnPc1d1	poslední
dvě	dva	k4xCgFnPc1	dva
skladby	skladba	k1gFnPc1	skladba
byly	být	k5eAaImAgFnP	být
psány	psát	k5eAaImNgFnP	psát
cíleně	cíleně	k6eAd1	cíleně
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
dotvoření	dotvoření	k1gNnSc1	dotvoření
cyklu	cyklus	k1gInSc2	cyklus
o	o	k7c6	o
šesti	šest	k4xCc6	šest
symfonických	symfonický	k2eAgFnPc6d1	symfonická
básních	báseň	k1gFnPc6	báseň
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
symfonická	symfonický	k2eAgFnSc1d1	symfonická
báseň	báseň	k1gFnSc1	báseň
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1874	[number]	k4	1874
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
vznikala	vznikat	k5eAaImAgFnS	vznikat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
Smetana	Smetana	k1gMnSc1	Smetana
procházel	procházet	k5eAaImAgMnS	procházet
těžkým	těžký	k2eAgNnSc7d1	těžké
obdobím	období	k1gNnSc7	období
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1866	[number]	k4	1866
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
vůdčí	vůdčí	k2eAgFnSc4d1	vůdčí
osobnost	osobnost	k1gFnSc4	osobnost
opery	opera	k1gFnSc2	opera
Prozatímního	prozatímní	k2eAgNnSc2d1	prozatímní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Smetana	Smetana	k1gMnSc1	Smetana
zamýšlel	zamýšlet	k5eAaImAgMnS	zamýšlet
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
operní	operní	k2eAgFnSc2d1	operní
scény	scéna	k1gFnSc2	scéna
udělat	udělat	k5eAaPmF	udělat
operu	opera	k1gFnSc4	opera
světového	světový	k2eAgInSc2d1	světový
významu	význam	k1gInSc2	význam
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
postupně	postupně	k6eAd1	postupně
do	do	k7c2	do
operního	operní	k2eAgInSc2d1	operní
repertoáru	repertoár	k1gInSc2	repertoár
začleňoval	začleňovat	k5eAaImAgInS	začleňovat
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
díla	dílo	k1gNnSc2	dílo
operní	operní	k2eAgFnPc4d1	operní
literatury	literatura	k1gFnSc2	literatura
jako	jako	k9	jako
byla	být	k5eAaImAgFnS	být
díla	dílo	k1gNnSc2	dílo
Carla	Carl	k1gMnSc2	Carl
Marii	Maria	k1gFnSc3	Maria
Webera	Weber	k1gMnSc2	Weber
<g/>
,	,	kIx,	,
Wolfganga	Wolfgang	k1gMnSc2	Wolfgang
Amadea	Amadeus	k1gMnSc2	Amadeus
Mozarta	Mozart	k1gMnSc2	Mozart
<g/>
,	,	kIx,	,
Ludwiga	Ludwig	k1gMnSc2	Ludwig
van	vana	k1gFnPc2	vana
Beethovena	Beethoven	k1gMnSc2	Beethoven
<g/>
,	,	kIx,	,
Gioacchina	Gioacchin	k1gMnSc2	Gioacchin
Rossiniho	Rossini	k1gMnSc2	Rossini
nebo	nebo	k8xC	nebo
Daniela	Daniel	k1gMnSc2	Daniel
Aubera	Auber	k1gMnSc2	Auber
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
dozvuků	dozvuk	k1gInPc2	dozvuk
českého	český	k2eAgNnSc2d1	české
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
však	však	k9	však
začalo	začít	k5eAaPmAgNnS	začít
být	být	k5eAaImF	být
Smetanovi	Smetana	k1gMnSc3	Smetana
postupně	postupně	k6eAd1	postupně
vyčítáno	vyčítat	k5eAaImNgNnS	vyčítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
světové	světový	k2eAgFnPc4d1	světová
opery	opera	k1gFnPc4	opera
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
raději	rád	k6eAd2	rád
snažit	snažit	k5eAaImF	snažit
začlenit	začlenit	k5eAaPmF	začlenit
do	do	k7c2	do
repertoáru	repertoár	k1gInSc2	repertoár
operní	operní	k2eAgFnSc2d1	operní
scény	scéna	k1gFnSc2	scéna
Prozatímního	prozatímní	k2eAgNnSc2d1	prozatímní
díla	dílo	k1gNnSc2	dílo
národnostního	národnostní	k2eAgInSc2d1	národnostní
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
Smetanových	Smetanových	k2eAgMnPc2d1	Smetanových
odpůrců	odpůrce	k1gMnPc2	odpůrce
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
František	František	k1gMnSc1	František
Pivoda	Pivoda	k1gMnSc1	Pivoda
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
předních	přední	k2eAgMnPc2d1	přední
českých	český	k2eAgMnPc2d1	český
hudebních	hudební	k2eAgMnPc2d1	hudební
kritiků	kritik	k1gMnPc2	kritik
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
Smetanovi	Smetana	k1gMnSc3	Smetana
vyčítal	vyčítat	k5eAaImAgMnS	vyčítat
mimo	mimo	k6eAd1	mimo
nedostatku	nedostatek	k1gInSc2	nedostatek
národnostního	národnostní	k2eAgNnSc2d1	národnostní
uvědomění	uvědomění	k1gNnSc2	uvědomění
i	i	k9	i
jeho	jeho	k3xOp3gInSc1	jeho
příklon	příklon	k1gInSc1	příklon
k	k	k7c3	k
mladočechům	mladočech	k1gMnPc3	mladočech
<g/>
,	,	kIx,	,
údajnou	údajný	k2eAgFnSc4d1	údajná
Smetanovu	Smetanův	k2eAgFnSc4d1	Smetanova
žárlivost	žárlivost	k1gFnSc4	žárlivost
na	na	k7c4	na
jiné	jiný	k2eAgMnPc4d1	jiný
skladatele	skladatel	k1gMnPc4	skladatel
či	či	k8xC	či
samotné	samotný	k2eAgFnPc4d1	samotná
Smetanovy	Smetanův	k2eAgFnPc4d1	Smetanova
skladby	skladba	k1gFnPc4	skladba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
podle	podle	k7c2	podle
Pivody	Pivoda	k1gFnSc2	Pivoda
často	často	k6eAd1	často
nebyly	být	k5eNaImAgFnP	být
valné	valný	k2eAgFnPc4d1	valná
kvality	kvalita	k1gFnPc4	kvalita
a	a	k8xC	a
až	až	k9	až
na	na	k7c4	na
Prodanou	prodaný	k2eAgFnSc4d1	prodaná
nevěstu	nevěsta	k1gFnSc4	nevěsta
postrádaly	postrádat	k5eAaImAgFnP	postrádat
národnostní	národnostní	k2eAgInSc4d1	národnostní
náboj	náboj	k1gInSc4	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
kritiků	kritik	k1gMnPc2	kritik
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
Smetana	Smetana	k1gMnSc1	Smetana
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1872	[number]	k4	1872
vzdal	vzdát	k5eAaPmAgInS	vzdát
funkce	funkce	k1gFnSc2	funkce
ředitele	ředitel	k1gMnSc2	ředitel
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
Vyjádření	vyjádření	k1gNnSc1	vyjádření
nesouhlasu	nesouhlas	k1gInSc2	nesouhlas
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
krokem	krok	k1gInSc7	krok
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
předních	přední	k2eAgMnPc2d1	přední
českých	český	k2eAgMnPc2d1	český
umělců	umělec	k1gMnPc2	umělec
však	však	k9	však
způsobila	způsobit	k5eAaPmAgFnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k9	již
počátkem	počátkem	k7c2	počátkem
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgMnS	být
Smetana	Smetana	k1gMnSc1	Smetana
opět	opět	k6eAd1	opět
dosazen	dosadit	k5eAaPmNgMnS	dosadit
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
vedoucího	vedoucí	k1gMnSc2	vedoucí
operní	operní	k2eAgFnPc1d1	operní
scény	scéna	k1gFnPc1	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
nová	nový	k2eAgFnSc1d1	nová
komická	komický	k2eAgFnSc1d1	komická
opera	opera	k1gFnSc1	opera
Dvě	dva	k4xCgFnPc1	dva
vdovy	vdova	k1gFnPc1	vdova
však	však	k9	však
kromě	kromě	k7c2	kromě
nadšených	nadšený	k2eAgMnPc2d1	nadšený
ohlasů	ohlas	k1gInPc2	ohlas
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
i	i	k9	i
další	další	k2eAgFnSc4d1	další
vlnu	vlna	k1gFnSc4	vlna
kritiky	kritika	k1gFnSc2	kritika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
často	často	k6eAd1	často
přesouvala	přesouvat	k5eAaImAgFnS	přesouvat
až	až	k9	až
do	do	k7c2	do
osobní	osobní	k2eAgFnSc2d1	osobní
roviny	rovina	k1gFnSc2	rovina
výpadů	výpad	k1gInPc2	výpad
proti	proti	k7c3	proti
Smetanovi	Smetana	k1gMnSc3	Smetana
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
náročné	náročný	k2eAgFnSc2d1	náročná
sezony	sezona	k1gFnSc2	sezona
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1873	[number]	k4	1873
<g/>
/	/	kIx~	/
<g/>
1874	[number]	k4	1874
byl	být	k5eAaImAgMnS	být
Smetana	Smetana	k1gMnSc1	Smetana
i	i	k9	i
následkem	následkem	k7c2	následkem
vln	vlna	k1gFnPc2	vlna
osobních	osobní	k2eAgInPc2d1	osobní
útoků	útok	k1gInPc2	útok
značně	značně	k6eAd1	značně
vyčerpán	vyčerpat	k5eAaPmNgMnS	vyčerpat
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
jej	on	k3xPp3gMnSc4	on
však	však	k9	však
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
další	další	k2eAgFnSc1d1	další
rána	rána	k1gFnSc1	rána
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
relativně	relativně	k6eAd1	relativně
náhlé	náhlý	k2eAgFnPc4d1	náhlá
ztráty	ztráta	k1gFnPc4	ztráta
sluchu	sluch	k1gInSc2	sluch
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgNnSc2	ten
léta	léto	k1gNnSc2	léto
si	se	k3xPyFc3	se
Smetana	Smetana	k1gMnSc1	Smetana
všiml	všimnout	k5eAaPmAgMnS	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
každé	každý	k3xTgNnSc1	každý
ucho	ucho	k1gNnSc1	ucho
vnímá	vnímat	k5eAaImIp3nS	vnímat
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
tóny	tón	k1gInPc4	tón
jinak	jinak	k6eAd1	jinak
<g/>
;	;	kIx,	;
později	pozdě	k6eAd2	pozdě
mu	on	k3xPp3gMnSc3	on
začalo	začít	k5eAaPmAgNnS	začít
pískat	pískat	k5eAaImF	pískat
v	v	k7c6	v
levém	levý	k2eAgNnSc6d1	levé
uchu	ucho	k1gNnSc6	ucho
a	a	k8xC	a
sluch	sluch	k1gInSc1	sluch
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
neustále	neustále	k6eAd1	neustále
zhoršoval	zhoršovat	k5eAaImAgMnS	zhoršovat
<g/>
,	,	kIx,	,
až	až	k8xS	až
přes	přes	k7c4	přes
noc	noc	k1gFnSc4	noc
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
na	na	k7c4	na
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1874	[number]	k4	1874
ohluchl	ohluchnout	k5eAaPmAgInS	ohluchnout
úplně	úplně	k6eAd1	úplně
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
ještě	ještě	k9	ještě
19	[number]	k4	19
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
večer	večer	k6eAd1	večer
navštívil	navštívit	k5eAaPmAgMnS	navštívit
operu	opera	k1gFnSc4	opera
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
schopen	schopen	k2eAgMnSc1d1	schopen
ji	on	k3xPp3gFnSc4	on
slyšet	slyšet	k5eAaImF	slyšet
<g/>
.	.	kIx.	.
</s>
<s>
Smetana	Smetana	k1gMnSc1	Smetana
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
vzdal	vzdát	k5eAaPmAgMnS	vzdát
svých	svůj	k3xOyFgFnPc2	svůj
funkcí	funkce	k1gFnPc2	funkce
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
a	a	k8xC	a
již	již	k6eAd1	již
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
obsadil	obsadit	k5eAaPmAgMnS	obsadit
místo	místo	k1gNnSc4	místo
ředitele	ředitel	k1gMnSc2	ředitel
opery	opera	k1gFnSc2	opera
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
hlavních	hlavní	k2eAgMnPc2d1	hlavní
odpůrců	odpůrce	k1gMnPc2	odpůrce
<g/>
,	,	kIx,	,
staročech	staročech	k1gMnSc1	staročech
Jan	Jan	k1gMnSc1	Jan
Nepomuk	Nepomuk	k1gMnSc1	Nepomuk
Maýr	Maýr	k1gMnSc1	Maýr
<g/>
.	.	kIx.	.
</s>
<s>
Smetana	Smetana	k1gMnSc1	Smetana
<g/>
,	,	kIx,	,
prožívaje	prožívat	k5eAaImSgMnS	prožívat
těžké	těžký	k2eAgNnSc4d1	těžké
životní	životní	k2eAgNnSc4d1	životní
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
paradoxně	paradoxně	k6eAd1	paradoxně
získal	získat	k5eAaPmAgMnS	získat
více	hodně	k6eAd2	hodně
času	čas	k1gInSc2	čas
na	na	k7c4	na
skladatelskou	skladatelský	k2eAgFnSc4d1	skladatelská
činnost	činnost	k1gFnSc4	činnost
a	a	k8xC	a
nebyl	být	k5eNaImAgInS	být
tolik	tolik	k6eAd1	tolik
rozptylován	rozptylován	k2eAgInSc1d1	rozptylován
veřejným	veřejný	k2eAgNnSc7d1	veřejné
děním	dění	k1gNnSc7	dění
a	a	k8xC	a
činnostmi	činnost	k1gFnPc7	činnost
souvisejícími	související	k2eAgFnPc7d1	související
s	s	k7c7	s
vedoucím	vedoucí	k2eAgNnSc7d1	vedoucí
postavením	postavení	k1gNnSc7	postavení
pražské	pražský	k2eAgFnSc2d1	Pražská
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
Ponořil	ponořit	k5eAaPmAgMnS	ponořit
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
do	do	k7c2	do
tvoření	tvoření	k1gNnSc2	tvoření
partitury	partitura	k1gFnSc2	partitura
symfonické	symfonický	k2eAgFnSc2d1	symfonická
básně	báseň	k1gFnSc2	báseň
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
a	a	k8xC	a
již	již	k6eAd1	již
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc6	listopad
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
dílem	díl	k1gInSc7	díl
hotov	hotov	k2eAgInSc1d1	hotov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Vyšehradu	Vyšehrad	k1gInSc2	Vyšehrad
přitom	přitom	k6eAd1	přitom
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
koncem	koncem	k7c2	koncem
září	září	k1gNnSc2	září
1874	[number]	k4	1874
<g/>
.	.	kIx.	.
</s>
<s>
Premiéru	premiéra	k1gFnSc4	premiéra
měla	mít	k5eAaImAgFnS	mít
tato	tento	k3xDgFnSc1	tento
symfonická	symfonický	k2eAgFnSc1d1	symfonická
báseň	báseň	k1gFnSc1	báseň
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1875	[number]	k4	1875
<g/>
.	.	kIx.	.
</s>
<s>
Tvůrčí	tvůrčí	k2eAgInSc1d1	tvůrčí
rozmach	rozmach	k1gInSc1	rozmach
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Smetany	Smetana	k1gMnSc2	Smetana
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
již	již	k9	již
úplně	úplně	k6eAd1	úplně
hluchého	hluchý	k2eAgNnSc2d1	hluché
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
Vyšehradu	Vyšehrad	k1gInSc3	Vyšehrad
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
nové	nový	k2eAgFnSc6d1	nová
symfonické	symfonický	k2eAgFnSc6d1	symfonická
básni	báseň	k1gFnSc6	báseň
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
nazval	nazvat	k5eAaPmAgMnS	nazvat
Vltava	Vltava	k1gFnSc1	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
básni	báseň	k1gFnSc6	báseň
se	se	k3xPyFc4	se
Smetana	Smetana	k1gMnSc1	Smetana
snaží	snažit	k5eAaImIp3nS	snažit
hudbou	hudba	k1gFnSc7	hudba
vykreslit	vykreslit	k5eAaPmF	vykreslit
podobu	podoba	k1gFnSc4	podoba
největší	veliký	k2eAgFnSc2d3	veliký
české	český	k2eAgFnSc2d1	Česká
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
symfonickou	symfonický	k2eAgFnSc7d1	symfonická
básní	báseň	k1gFnSc7	báseň
byl	být	k5eAaImAgInS	být
hotov	hotov	k2eAgInSc1d1	hotov
již	již	k9	již
8	[number]	k4	8
<g/>
.	.	kIx.	.
dne	den	k1gInSc2	den
následujícího	následující	k2eAgInSc2d1	následující
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Premiéru	premiéra	k1gFnSc4	premiéra
měla	mít	k5eAaImAgFnS	mít
Vltava	Vltava	k1gFnSc1	Vltava
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1875	[number]	k4	1875
<g/>
.	.	kIx.	.
</s>
<s>
Byť	byť	k8xS	byť
Smetana	Smetana	k1gMnSc1	Smetana
zkomponoval	zkomponovat	k5eAaPmAgMnS	zkomponovat
první	první	k4xOgFnPc4	první
dvě	dva	k4xCgFnPc4	dva
básně	báseň	k1gFnPc4	báseň
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
Má	mít	k5eAaImIp3nS	mít
vlast	vlast	k1gFnSc4	vlast
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1874	[number]	k4	1874
<g/>
,	,	kIx,	,
náměty	námět	k1gInPc4	námět
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
vytvoření	vytvoření	k1gNnSc4	vytvoření
nosil	nosit	k5eAaImAgInS	nosit
v	v	k7c6	v
hlavě	hlava	k1gFnSc6	hlava
už	už	k6eAd1	už
nejméně	málo	k6eAd3	málo
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
dokládá	dokládat	k5eAaImIp3nS	dokládat
zmínka	zmínka	k1gFnSc1	zmínka
v	v	k7c6	v
Hudebních	hudební	k2eAgInPc6d1	hudební
listech	list	k1gInPc6	list
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Otakara	Otakar	k1gMnSc2	Otakar
Šourka	Šourek	k1gMnSc2	Šourek
však	však	k9	však
lze	lze	k6eAd1	lze
"	"	kIx"	"
<g/>
hudební	hudební	k2eAgInSc1d1	hudební
zárodek	zárodek	k1gInSc1	zárodek
<g/>
"	"	kIx"	"
cyklu	cyklus	k1gInSc2	cyklus
najít	najít	k5eAaPmF	najít
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
motiv	motiv	k1gInSc4	motiv
sestupně	sestupně	k6eAd1	sestupně
a	a	k8xC	a
vzestupně	vzestupně	k6eAd1	vzestupně
rozloženého	rozložený	k2eAgInSc2d1	rozložený
sextakordu	sextakord	k1gInSc2	sextakord
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
ve	v	k7c6	v
Vyšehradě	Vyšehrad	k1gInSc6	Vyšehrad
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
už	už	k6eAd1	už
i	i	k9	i
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
dějství	dějství	k1gNnSc6	dějství
Libuše	Libuše	k1gFnSc2	Libuše
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
symfonická	symfonický	k2eAgFnSc1d1	symfonická
báseň	báseň	k1gFnSc1	báseň
s	s	k7c7	s
názvem	název	k1gInSc7	název
Šárka	Šárka	k1gFnSc1	Šárka
byla	být	k5eAaImAgFnS	být
dokončena	dokončen	k2eAgFnSc1d1	dokončena
20	[number]	k4	20
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1875	[number]	k4	1875
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
po	po	k7c6	po
bojovnici	bojovnice	k1gFnSc6	bojovnice
Šárce	Šárka	k1gFnSc6	Šárka
z	z	k7c2	z
české	český	k2eAgFnSc2d1	Česká
pověsti	pověst	k1gFnSc2	pověst
Dívčí	dívčí	k2eAgFnSc1d1	dívčí
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
tři	tři	k4xCgFnPc4	tři
symfonické	symfonický	k2eAgFnPc4d1	symfonická
básně	báseň	k1gFnPc4	báseň
Smetana	Smetana	k1gMnSc1	Smetana
skládal	skládat	k5eAaImAgMnS	skládat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
symfonickou	symfonický	k2eAgFnSc4d1	symfonická
báseň	báseň	k1gFnSc4	báseň
Z	z	k7c2	z
českých	český	k2eAgInPc2d1	český
luhů	luh	k1gInPc2	luh
a	a	k8xC	a
hájů	háj	k1gInPc2	háj
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
Smetana	Smetana	k1gMnSc1	Smetana
komponoval	komponovat	k5eAaImAgMnS	komponovat
v	v	k7c6	v
myslivně	myslivna	k1gFnSc6	myslivna
v	v	k7c6	v
Jabkenicích	Jabkenice	k1gFnPc6	Jabkenice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
symfonické	symfonický	k2eAgFnSc2d1	symfonická
básně	báseň	k1gFnSc2	báseň
Smetana	Smetana	k1gMnSc1	Smetana
spojil	spojit	k5eAaPmAgMnS	spojit
svorkou	svorka	k1gFnSc7	svorka
partitury	partitura	k1gFnPc4	partitura
všech	všecek	k3xTgFnPc2	všecek
čtyř	čtyři	k4xCgFnPc2	čtyři
básní	báseň	k1gFnPc2	báseň
a	a	k8xC	a
nadepsal	nadepsat	k5eAaPmAgMnS	nadepsat
je	být	k5eAaImIp3nS	být
titulem	titul	k1gInSc7	titul
"	"	kIx"	"
<g/>
Na	na	k7c6	na
Vlasť	Vlasť	k1gFnSc6	Vlasť
–	–	k?	–
tvoří	tvořit	k5eAaImIp3nS	tvořit
celek	celek	k1gInSc1	celek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
tak	tak	k9	tak
Smetana	Smetana	k1gMnSc1	Smetana
vyplodil	vyplodit	k5eAaPmAgMnS	vyplodit
čtyři	čtyři	k4xCgFnPc4	čtyři
symfonické	symfonický	k2eAgFnPc4d1	symfonická
básně	báseň	k1gFnPc4	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Dobový	dobový	k2eAgInSc1d1	dobový
tisk	tisk	k1gInSc1	tisk
o	o	k7c6	o
Smetanových	Smetanových	k2eAgFnPc6d1	Smetanových
čtyřech	čtyři	k4xCgFnPc6	čtyři
básních	báseň	k1gFnPc6	báseň
psal	psát	k5eAaImAgInS	psát
jako	jako	k9	jako
o	o	k7c4	o
tetralogii	tetralogie	k1gFnSc4	tetralogie
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
sám	sám	k3xTgMnSc1	sám
Smetana	Smetana	k1gMnSc1	Smetana
zamýšlel	zamýšlet	k5eAaImAgMnS	zamýšlet
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
ještě	ještě	k9	ještě
celek	celek	k1gInSc1	celek
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
uzavřít	uzavřít	k5eAaPmF	uzavřít
a	a	k8xC	a
dodat	dodat	k5eAaPmF	dodat
mu	on	k3xPp3gMnSc3	on
myšlenkové	myšlenkový	k2eAgNnSc1d1	myšlenkové
vyvrcholení	vyvrcholení	k1gNnSc1	vyvrcholení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nastávajícím	nastávající	k2eAgInSc6d1	nastávající
čase	čas	k1gInSc6	čas
však	však	k9	však
byl	být	k5eAaImAgMnS	být
Smetana	Smetana	k1gMnSc1	Smetana
zaměstnán	zaměstnat	k5eAaPmNgMnS	zaměstnat
prací	práce	k1gFnSc7	práce
na	na	k7c6	na
jiných	jiný	k2eAgInPc6d1	jiný
projektech	projekt	k1gInPc6	projekt
jako	jako	k8xS	jako
byly	být	k5eAaImAgInP	být
např.	např.	kA	např.
opery	opera	k1gFnSc2	opera
Hubička	hubička	k1gFnSc1	hubička
a	a	k8xC	a
Tajemství	tajemství	k1gNnSc1	tajemství
či	či	k8xC	či
smyčcový	smyčcový	k2eAgInSc1d1	smyčcový
kvartet	kvartet	k1gInSc1	kvartet
Z	z	k7c2	z
mého	můj	k3xOp1gInSc2	můj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
sepsání	sepsání	k1gNnSc3	sepsání
těchto	tento	k3xDgNnPc2	tento
děl	dělo	k1gNnPc2	dělo
ho	on	k3xPp3gMnSc2	on
hnaly	hnát	k5eAaImAgInP	hnát
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
finanční	finanční	k2eAgInPc4d1	finanční
důvody	důvod	k1gInPc4	důvod
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
divadla	divadlo	k1gNnSc2	divadlo
Smetana	Smetana	k1gMnSc1	Smetana
neměl	mít	k5eNaImAgMnS	mít
stálé	stálý	k2eAgNnSc4d1	stálé
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
a	a	k8xC	a
cestovní	cestovní	k2eAgFnSc2d1	cestovní
výlohy	výloha	k1gFnSc2	výloha
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
výdaji	výdaj	k1gInPc7	výdaj
za	za	k7c4	za
léčebné	léčebný	k2eAgInPc4d1	léčebný
pobyty	pobyt	k1gInPc4	pobyt
a	a	k8xC	a
návštěvy	návštěva	k1gFnPc1	návštěva
doktorů	doktor	k1gMnPc2	doktor
byly	být	k5eAaImAgFnP	být
značně	značně	k6eAd1	značně
vysoké	vysoký	k2eAgFnPc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tematice	tematika	k1gFnSc3	tematika
symfonických	symfonický	k2eAgFnPc2d1	symfonická
básní	báseň	k1gFnPc2	báseň
na	na	k7c4	na
národnostní	národnostní	k2eAgNnSc4d1	národnostní
téma	téma	k1gNnSc4	téma
se	se	k3xPyFc4	se
proto	proto	k6eAd1	proto
vrátil	vrátit	k5eAaPmAgInS	vrátit
až	až	k9	až
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
několika	několik	k4yIc2	několik
jiných	jiný	k2eAgFnPc2d1	jiná
prací	práce	k1gFnPc2	práce
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1878	[number]	k4	1878
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
pátá	pátý	k4xOgFnSc1	pátý
symfonická	symfonický	k2eAgFnSc1d1	symfonická
báseň	báseň	k1gFnSc1	báseň
Tábor	Tábor	k1gInSc1	Tábor
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
13	[number]	k4	13
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1878	[number]	k4	1878
<g/>
,	,	kIx,	,
premiéru	premiér	k1gMnSc3	premiér
měla	mít	k5eAaImAgFnS	mít
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1880	[number]	k4	1880
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
po	po	k7c6	po
jihočeském	jihočeský	k2eAgNnSc6d1	Jihočeské
městě	město	k1gNnSc6	město
Tábor	Tábor	k1gInSc1	Tábor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1420	[number]	k4	1420
založili	založit	k5eAaPmAgMnP	založit
husité	husita	k1gMnPc1	husita
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
během	během	k7c2	během
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
sloužilo	sloužit	k5eAaImAgNnS	sloužit
jako	jako	k9	jako
hlavní	hlavní	k2eAgNnSc1d1	hlavní
centrum	centrum	k1gNnSc1	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Smetanu	smetana	k1gFnSc4	smetana
při	při	k7c6	při
sepsání	sepsání	k1gNnSc6	sepsání
táto	táta	k1gMnSc5	táta
básně	báseň	k1gFnPc4	báseň
inspirovaly	inspirovat	k5eAaBmAgFnP	inspirovat
právě	právě	k6eAd1	právě
husitské	husitský	k2eAgFnPc1d1	husitská
války	válka	k1gFnPc1	válka
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yQnSc6	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
i	i	k9	i
přítomný	přítomný	k2eAgInSc1d1	přítomný
nápěv	nápěv	k1gInSc1	nápěv
husitského	husitský	k2eAgInSc2d1	husitský
chorálu	chorál	k1gInSc2	chorál
Ktož	Ktož	k1gFnSc1	Ktož
jsú	jsú	k?	jsú
boží	boží	k2eAgMnPc1d1	boží
bojovníci	bojovník	k1gMnPc1	bojovník
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
celé	celá	k1gFnSc6	celá
básní	báseň	k1gFnPc2	báseň
dominuje	dominovat	k5eAaImIp3nS	dominovat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
symfonického	symfonický	k2eAgInSc2d1	symfonický
cyklu	cyklus	k1gInSc2	cyklus
je	být	k5eAaImIp3nS	být
mohutnou	mohutný	k2eAgFnSc7d1	mohutná
fantazií	fantazie	k1gFnSc7	fantazie
na	na	k7c4	na
bitevní	bitevní	k2eAgInSc4d1	bitevní
zpěv	zpěv	k1gInSc4	zpěv
husitů	husita	k1gMnPc2	husita
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
bezprostředně	bezprostředně	k6eAd1	bezprostředně
pojí	pojíst	k5eAaPmIp3nS	pojíst
následující	následující	k2eAgFnSc1d1	následující
<g/>
,	,	kIx,	,
šestá	šestý	k4xOgFnSc1	šestý
a	a	k8xC	a
poslední	poslední	k2eAgFnSc1d1	poslední
symfonická	symfonický	k2eAgFnSc1d1	symfonická
báseň	báseň	k1gFnSc1	báseň
s	s	k7c7	s
názvem	název	k1gInSc7	název
Blaník	Blaník	k1gInSc1	Blaník
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
dokončena	dokončen	k2eAgFnSc1d1	dokončena
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1879	[number]	k4	1879
a	a	k8xC	a
premiéru	premiéra	k1gFnSc4	premiéra
měla	mít	k5eAaImAgFnS	mít
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1880	[number]	k4	1880
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
po	po	k7c6	po
středočeském	středočeský	k2eAgInSc6d1	středočeský
vrchu	vrch	k1gInSc6	vrch
Blaník	Blaník	k1gInSc1	Blaník
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
podle	podle	k7c2	podle
staročeské	staročeský	k2eAgFnSc2d1	staročeská
pověsti	pověst	k1gFnSc2	pověst
spí	spát	k5eAaImIp3nS	spát
vojsko	vojsko	k1gNnSc1	vojsko
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
se	s	k7c7	s
svatým	svatý	k1gMnSc7	svatý
Václavem	Václav	k1gMnSc7	Václav
a	a	k8xC	a
čeká	čekat	k5eAaImIp3nS	čekat
<g/>
,	,	kIx,	,
až	až	k9	až
bude	být	k5eAaImBp3nS	být
českému	český	k2eAgInSc3d1	český
národu	národ	k1gInSc3	národ
nejhůře	zle	k6eAd3	zle
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
Blaníku	Blaník	k1gInSc2	Blaník
Smetana	Smetana	k1gMnSc1	Smetana
připojil	připojit	k5eAaPmAgMnS	připojit
posledně	posledně	k6eAd1	posledně
dvě	dva	k4xCgNnPc4	dva
komponovaná	komponovaný	k2eAgNnPc4d1	komponované
díla	dílo	k1gNnPc4	dílo
–	–	k?	–
Tábor	Tábor	k1gInSc1	Tábor
a	a	k8xC	a
Blaník	Blaník	k1gInSc1	Blaník
–	–	k?	–
k	k	k7c3	k
předcházejícím	předcházející	k2eAgInPc3d1	předcházející
čtyřem	čtyři	k4xCgInPc3	čtyři
symfonickým	symfonický	k2eAgMnPc3d1	symfonický
básním	básnit	k5eAaImIp1nS	básnit
a	a	k8xC	a
celek	celek	k1gInSc4	celek
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
Vlast	vlast	k1gFnSc4	vlast
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
byl	být	k5eAaImAgMnS	být
celý	celý	k2eAgInSc4d1	celý
soubor	soubor	k1gInSc4	soubor
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
zmínku	zmínka	k1gFnSc4	zmínka
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
cyklu	cyklus	k1gInSc2	cyklus
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
dostalo	dostat	k5eAaPmAgNnS	dostat
pojmenování	pojmenování	k1gNnSc1	pojmenování
Má	mít	k5eAaImIp3nS	mít
vlast	vlast	k1gFnSc4	vlast
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
přinesl	přinést	k5eAaPmAgInS	přinést
časopis	časopis	k1gInSc1	časopis
Dalibor	Dalibor	k1gMnSc1	Dalibor
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1879	[number]	k4	1879
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
rovněž	rovněž	k9	rovněž
zmiňoval	zmiňovat	k5eAaImAgInS	zmiňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Smetana	Smetana	k1gMnSc1	Smetana
celý	celý	k2eAgInSc4d1	celý
cyklus	cyklus	k1gInSc4	cyklus
věnoval	věnovat	k5eAaImAgMnS	věnovat
Praze	Praha	k1gFnSc3	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
Smetana	Smetana	k1gMnSc1	Smetana
celý	celý	k2eAgInSc1d1	celý
cyklus	cyklus	k1gInSc1	cyklus
zakončil	zakončit	k5eAaPmAgInS	zakončit
<g/>
,	,	kIx,	,
zkomponoval	zkomponovat	k5eAaPmAgInS	zkomponovat
ještě	ještě	k9	ještě
verzi	verze	k1gFnSc4	verze
pro	pro	k7c4	pro
čtyřruční	čtyřruční	k2eAgInSc4d1	čtyřruční
klavír	klavír	k1gInSc4	klavír
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
komponování	komponování	k1gNnSc6	komponování
této	tento	k3xDgFnSc2	tento
verze	verze	k1gFnSc2	verze
Mé	můj	k3xOp1gFnSc2	můj
vlasti	vlast	k1gFnSc2	vlast
byl	být	k5eAaImAgMnS	být
Smetana	Smetana	k1gMnSc1	Smetana
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
omezen	omezit	k5eAaPmNgInS	omezit
technickou	technický	k2eAgFnSc7d1	technická
povahou	povaha	k1gFnSc7	povaha
díla	dílo	k1gNnSc2	dílo
(	(	kIx(	(
<g/>
technické	technický	k2eAgFnSc2d1	technická
možnosti	možnost	k1gFnSc2	možnost
čtyřručních	čtyřruční	k2eAgFnPc2d1	čtyřruční
klavírních	klavírní	k2eAgFnPc2d1	klavírní
skladeb	skladba	k1gFnPc2	skladba
jsou	být	k5eAaImIp3nP	být
přirozeně	přirozeně	k6eAd1	přirozeně
menší	malý	k2eAgFnPc1d2	menší
než	než	k8xS	než
u	u	k7c2	u
skladeb	skladba	k1gFnPc2	skladba
komponovaných	komponovaný	k2eAgFnPc2d1	komponovaná
pro	pro	k7c4	pro
klavíry	klavír	k1gInPc4	klavír
dva	dva	k4xCgInPc4	dva
<g/>
)	)	kIx)	)
i	i	k9	i
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc1	některý
pasáže	pasáž	k1gFnPc1	pasáž
cyklu	cyklus	k1gInSc2	cyklus
jsou	být	k5eAaImIp3nP	být
vyloženě	vyloženě	k6eAd1	vyloženě
orchestrálního	orchestrální	k2eAgInSc2d1	orchestrální
charakteru	charakter	k1gInSc2	charakter
<g/>
,	,	kIx,	,
a	a	k8xC	a
nebylo	být	k5eNaImAgNnS	být
proto	proto	k8xC	proto
možné	možný	k2eAgNnSc1d1	možné
je	být	k5eAaImIp3nS	být
převést	převést	k5eAaPmF	převést
do	do	k7c2	do
verze	verze	k1gFnSc2	verze
čtyřručního	čtyřruční	k2eAgInSc2d1	čtyřruční
klavíru	klavír	k1gInSc2	klavír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
situacích	situace	k1gFnPc6	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ve	v	k7c6	v
skladbě	skladba	k1gFnSc6	skladba
v	v	k7c6	v
orchestrální	orchestrální	k2eAgFnSc6d1	orchestrální
verzi	verze	k1gFnSc6	verze
nastávají	nastávat	k5eAaImIp3nP	nastávat
části	část	k1gFnPc1	část
s	s	k7c7	s
několikanásobnými	několikanásobný	k2eAgFnPc7d1	několikanásobná
hudebními	hudební	k2eAgFnPc7d1	hudební
linkami	linka	k1gFnPc7	linka
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Smetana	Smetana	k1gMnSc1	Smetana
nucen	nutit	k5eAaImNgMnS	nutit
vybrat	vybrat	k5eAaPmF	vybrat
jen	jen	k9	jen
některou	některý	k3yIgFnSc7	některý
či	či	k8xC	či
některé	některý	k3yIgFnPc4	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Lukáše	Lukáš	k1gMnSc2	Lukáš
Matouška	Matoušek	k1gMnSc2	Matoušek
může	moct	k5eAaImIp3nS	moct
skutečnost	skutečnost	k1gFnSc4	skutečnost
být	být	k5eAaImF	být
klíčem	klíč	k1gInSc7	klíč
pro	pro	k7c4	pro
určení	určení	k1gNnSc4	určení
původně	původně	k6eAd1	původně
zamýšlené	zamýšlený	k2eAgFnSc2d1	zamýšlená
Smetanovy	Smetanův	k2eAgFnSc2d1	Smetanova
interpretace	interpretace	k1gFnSc2	interpretace
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
celý	celý	k2eAgInSc1d1	celý
cyklus	cyklus	k1gInSc1	cyklus
Má	mít	k5eAaImIp3nS	mít
vlast	vlast	k1gFnSc4	vlast
postupně	postupně	k6eAd1	postupně
vznikal	vznikat	k5eAaImAgInS	vznikat
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
symfonické	symfonický	k2eAgFnPc1d1	symfonická
básně	báseň	k1gFnPc1	báseň
nacvičovány	nacvičovat	k5eAaImNgFnP	nacvičovat
i	i	k8xC	i
hrány	hrát	k5eAaImNgFnP	hrát
různými	různý	k2eAgFnPc7d1	různá
orchestry	orchestr	k1gInPc1	orchestr
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
většinou	většina	k1gFnSc7	většina
pouze	pouze	k6eAd1	pouze
jednotlivě	jednotlivě	k6eAd1	jednotlivě
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
uvedení	uvedení	k1gNnSc1	uvedení
Mé	můj	k3xOp1gFnSc2	můj
vlasti	vlast	k1gFnSc2	vlast
jakožto	jakožto	k8xS	jakožto
šestidílného	šestidílný	k2eAgInSc2d1	šestidílný
cyklu	cyklus	k1gInSc2	cyklus
nastalo	nastat	k5eAaPmAgNnS	nastat
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1882	[number]	k4	1882
<g/>
;	;	kIx,	;
premiéra	premiéra	k1gFnSc1	premiéra
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
v	v	k7c6	v
sále	sál	k1gInSc6	sál
na	na	k7c6	na
Žofíně	Žofín	k1gInSc6	Žofín
<g/>
,	,	kIx,	,
cyklus	cyklus	k1gInSc1	cyklus
byl	být	k5eAaImAgInS	být
nacvičen	nacvičit	k5eAaBmNgInS	nacvičit
Výpomocným	výpomocný	k2eAgInSc7d1	výpomocný
spolkem	spolek	k1gInSc7	spolek
členů	člen	k1gMnPc2	člen
sboru	sbor	k1gInSc2	sbor
a	a	k8xC	a
orchestru	orchestr	k1gInSc2	orchestr
královského	královský	k2eAgNnSc2d1	královské
zemského	zemský	k2eAgNnSc2d1	zemské
českého	český	k2eAgNnSc2d1	české
divadla	divadlo	k1gNnSc2	divadlo
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
dirigenta	dirigent	k1gMnSc2	dirigent
Adolfa	Adolf	k1gMnSc2	Adolf
Čecha	Čech	k1gMnSc2	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Představení	představení	k1gNnSc4	představení
navštívil	navštívit	k5eAaPmAgMnS	navštívit
i	i	k9	i
sám	sám	k3xTgMnSc1	sám
Bedřich	Bedřich	k1gMnSc1	Bedřich
Smetana	Smetana	k1gMnSc1	Smetana
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgFnSc6	první
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
poslední	poslední	k2eAgNnSc1d1	poslední
souborné	souborný	k2eAgNnSc1d1	souborné
uvedení	uvedení	k1gNnSc1	uvedení
celého	celý	k2eAgInSc2d1	celý
cyklu	cyklus	k1gInSc2	cyklus
<g/>
,	,	kIx,	,
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
dožil	dožít	k5eAaPmAgMnS	dožít
(	(	kIx(	(
<g/>
zemřel	zemřít	k5eAaPmAgMnS	zemřít
o	o	k7c4	o
2	[number]	k4	2
roky	rok	k1gInPc7	rok
později	pozdě	k6eAd2	pozdě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
nacvičování	nacvičování	k1gNnSc1	nacvičování
a	a	k8xC	a
předvádění	předvádění	k1gNnSc1	předvádění
Mé	můj	k3xOp1gFnSc2	můj
vlasti	vlast	k1gFnSc2	vlast
se	se	k3xPyFc4	se
dělo	dít	k5eAaBmAgNnS	dít
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
sporadicky	sporadicky	k6eAd1	sporadicky
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
vysokými	vysoký	k2eAgInPc7d1	vysoký
požadavky	požadavek	k1gInPc7	požadavek
na	na	k7c4	na
technické	technický	k2eAgFnPc4d1	technická
a	a	k8xC	a
umělecké	umělecký	k2eAgFnPc4d1	umělecká
dovednosti	dovednost	k1gFnPc4	dovednost
orchestru	orchestr	k1gInSc2	orchestr
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
souborné	souborný	k2eAgNnSc1d1	souborné
představení	představení	k1gNnSc1	představení
Mé	můj	k3xOp1gFnSc2	můj
vlasti	vlast	k1gFnSc2	vlast
nastalo	nastat	k5eAaPmAgNnS	nastat
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1887	[number]	k4	1887
k	k	k7c3	k
příležitosti	příležitost	k1gFnSc3	příležitost
třetího	třetí	k4xOgNnSc2	třetí
výročí	výročí	k1gNnSc2	výročí
Smetanovy	Smetanův	k2eAgFnSc2d1	Smetanova
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
častějšímu	častý	k2eAgNnSc3d2	častější
provedení	provedení	k1gNnSc3	provedení
cyklu	cyklus	k1gInSc2	cyklus
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
výrazně	výrazně	k6eAd1	výrazně
přispěl	přispět	k5eAaPmAgInS	přispět
vznik	vznik	k1gInSc1	vznik
České	český	k2eAgFnSc2d1	Česká
filharmonie	filharmonie	k1gFnSc2	filharmonie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
tak	tak	k9	tak
náročné	náročný	k2eAgNnSc1d1	náročné
dílo	dílo	k1gNnSc1	dílo
nastudovat	nastudovat	k5eAaBmF	nastudovat
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
České	český	k2eAgFnSc3d1	Česká
filharmonii	filharmonie	k1gFnSc3	filharmonie
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
začaly	začít	k5eAaPmAgInP	začít
pořádat	pořádat	k5eAaImF	pořádat
koncerty	koncert	k1gInPc1	koncert
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
mnohem	mnohem	k6eAd1	mnohem
častěji	často	k6eAd2	často
než	než	k8xS	než
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
i	i	k9	i
Má	můj	k3xOp1gFnSc1	můj
vlast	vlast	k1gFnSc1	vlast
se	se	k3xPyFc4	se
dočkala	dočkat	k5eAaPmAgFnS	dočkat
stále	stále	k6eAd1	stále
častějších	častý	k2eAgNnPc2d2	častější
provedení	provedení	k1gNnPc2	provedení
<g/>
.	.	kIx.	.
</s>
<s>
Má	můj	k3xOp1gFnSc1	můj
vlast	vlast	k1gFnSc1	vlast
se	se	k3xPyFc4	se
poměrně	poměrně	k6eAd1	poměrně
brzy	brzy	k6eAd1	brzy
hrála	hrát	k5eAaImAgFnS	hrát
i	i	k9	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
Německo	Německo	k1gNnSc1	Německo
a	a	k8xC	a
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
)	)	kIx)	)
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
i	i	k8xC	i
tam	tam	k6eAd1	tam
byly	být	k5eAaImAgFnP	být
nejdříve	dříve	k6eAd3	dříve
uvedeny	uvést	k5eAaPmNgFnP	uvést
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
básně	báseň	k1gFnPc1	báseň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
byla	být	k5eAaImAgFnS	být
Má	můj	k3xOp1gFnSc1	můj
vlast	vlast	k1gFnSc1	vlast
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
na	na	k7c4	na
gramofonovou	gramofonový	k2eAgFnSc4d1	gramofonová
desku	deska	k1gFnSc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávka	nahrávka	k1gFnSc1	nahrávka
byla	být	k5eAaImAgFnS	být
pořízena	pořídit	k5eAaPmNgFnS	pořídit
Českou	český	k2eAgFnSc7d1	Česká
filharmonií	filharmonie	k1gFnSc7	filharmonie
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Václava	Václav	k1gMnSc2	Václav
Talicha	Talich	k1gMnSc2	Talich
pro	pro	k7c4	pro
společnost	společnost	k1gFnSc4	společnost
His	his	k1gNnSc1	his
Master	master	k1gMnSc1	master
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Voice	Voice	k1gMnPc4	Voice
<g/>
;	;	kIx,	;
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc4	první
zaznamenanou	zaznamenaný	k2eAgFnSc4d1	zaznamenaná
nahrávku	nahrávka	k1gFnSc4	nahrávka
České	český	k2eAgFnSc2d1	Česká
filharmonie	filharmonie	k1gFnSc2	filharmonie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
letech	let	k1gInPc6	let
Česká	český	k2eAgFnSc1d1	Česká
filharmonie	filharmonie	k1gFnSc1	filharmonie
Mou	můj	k3xOp1gFnSc4	můj
vlast	vlast	k1gFnSc4	vlast
propagovala	propagovat	k5eAaImAgFnS	propagovat
na	na	k7c6	na
českém	český	k2eAgMnSc6d1	český
i	i	k8xC	i
zahraničním	zahraniční	k2eAgNnSc6d1	zahraniční
území	území	k1gNnSc6	území
a	a	k8xC	a
po	po	k7c6	po
nedlouhé	dlouhý	k2eNgFnSc6d1	nedlouhá
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
zvykem	zvyk	k1gInSc7	zvyk
cyklus	cyklus	k1gInSc1	cyklus
hrát	hrát	k5eAaImF	hrát
při	při	k7c6	při
nejrůznějších	různý	k2eAgFnPc6d3	nejrůznější
státních	státní	k2eAgFnPc6d1	státní
příležitostech	příležitost	k1gFnPc6	příležitost
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
považována	považován	k2eAgFnSc1d1	považována
ze	z	k7c2	z
vrcholné	vrcholný	k2eAgFnSc2d1	vrcholná
dílo	dílo	k1gNnSc4	dílo
nejen	nejen	k6eAd1	nejen
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Smetany	Smetana	k1gMnSc2	Smetana
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
české	český	k2eAgFnSc2d1	Česká
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Má	můj	k3xOp1gFnSc1	můj
vlast	vlast	k1gFnSc1	vlast
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
každoročně	každoročně	k6eAd1	každoročně
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
výročí	výročí	k1gNnSc2	výročí
Smetanova	Smetanův	k2eAgNnSc2d1	Smetanovo
úmrtí	úmrtí	k1gNnSc2	úmrtí
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
první	první	k4xOgNnSc1	první
zahajovací	zahajovací	k2eAgNnSc1d1	zahajovací
dílo	dílo	k1gNnSc1	dílo
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
Pražské	pražský	k2eAgNnSc1d1	Pražské
jaro	jaro	k1gNnSc1	jaro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
cyklus	cyklus	k1gInSc1	cyklus
na	na	k7c6	na
zahajovacím	zahajovací	k2eAgInSc6d1	zahajovací
koncertě	koncert	k1gInSc6	koncert
vůbec	vůbec	k9	vůbec
poprvé	poprvé	k6eAd1	poprvé
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
dirigent	dirigent	k1gMnSc1	dirigent
mimoevropského	mimoevropský	k2eAgInSc2d1	mimoevropský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
Japonec	Japonec	k1gMnSc1	Japonec
Ken-ičiró	Kenčiró	k1gMnSc1	Ken-ičiró
Kobajaši	Kobajaše	k1gFnSc4	Kobajaše
<g/>
.	.	kIx.	.
</s>
<s>
Báseň	báseň	k1gFnSc1	báseň
začíná	začínat	k5eAaImIp3nS	začínat
zvuky	zvuk	k1gInPc4	zvuk
harf	harfa	k1gFnPc2	harfa
středověkého	středověký	k2eAgMnSc2d1	středověký
barda	bard	k1gMnSc2	bard
Lumíra	Lumír	k1gMnSc2	Lumír
a	a	k8xC	a
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c4	v
tóny	tón	k1gInPc4	tón
hradní	hradní	k2eAgFnSc2d1	hradní
výzbroje	výzbroj	k1gFnSc2	výzbroj
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
představuje	představovat	k5eAaImIp3nS	představovat
hlavní	hlavní	k2eAgInPc4d1	hlavní
motivy	motiv	k1gInPc4	motiv
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
použity	použít	k5eAaPmNgInP	použít
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
částech	část	k1gFnPc6	část
cyklu	cyklus	k1gInSc2	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Čtyřtónový	čtyřtónový	k2eAgInSc1d1	čtyřtónový
motiv	motiv	k1gInSc1	motiv
(	(	kIx(	(
<g/>
b-es-d-b	bs	k1gInSc1	b-es-d-b
<g/>
)	)	kIx)	)
představuje	představovat	k5eAaImIp3nS	představovat
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
a	a	k8xC	a
zazní	zaznět	k5eAaImIp3nS	zaznět
znovu	znovu	k6eAd1	znovu
na	na	k7c6	na
konci	konec	k1gInSc6	konec
Vltavy	Vltava	k1gFnSc2	Vltava
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
při	při	k7c6	při
vyvrcholení	vyvrcholení	k1gNnSc6	vyvrcholení
celého	celý	k2eAgInSc2d1	celý
cyklu	cyklus	k1gInSc2	cyklus
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
Blaníku	Blaník	k1gInSc2	Blaník
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Smetana	Smetana	k1gMnSc1	Smetana
o	o	k7c6	o
skladbě	skladba	k1gFnSc6	skladba
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Harfy	harfa	k1gFnPc1	harfa
věštců	věštec	k1gMnPc2	věštec
začnou	začít	k5eAaPmIp3nP	začít
<g/>
;	;	kIx,	;
zpěv	zpěv	k1gInSc1	zpěv
věštců	věštec	k1gMnPc2	věštec
o	o	k7c6	o
dějích	děj	k1gInPc6	děj
na	na	k7c6	na
Vyšehradě	Vyšehrad	k1gInSc6	Vyšehrad
o	o	k7c6	o
slávě	sláva	k1gFnSc6	sláva
<g/>
,	,	kIx,	,
lesku	lesk	k1gInSc6	lesk
<g/>
,	,	kIx,	,
turnajích	turnaj	k1gInPc6	turnaj
<g/>
,	,	kIx,	,
bojích	boj	k1gInPc6	boj
až	až	k8xS	až
konečné	konečná	k1gFnSc3	konečná
úpadku	úpadek	k1gInSc2	úpadek
a	a	k8xC	a
zříceninách	zřícenina	k1gFnPc6	zřícenina
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
končí	končit	k5eAaImIp3nS	končit
v	v	k7c6	v
elegickém	elegický	k2eAgInSc6d1	elegický
tónu	tón	k1gInSc6	tón
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
Pro	pro	k7c4	pro
úvodní	úvodní	k2eAgNnSc4d1	úvodní
arpeggio	arpeggio	k1gNnSc4	arpeggio
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
báseň	báseň	k1gFnSc1	báseň
otevírá	otevírat	k5eAaImIp3nS	otevírat
<g/>
,	,	kIx,	,
partitura	partitura	k1gFnSc1	partitura
požaduje	požadovat	k5eAaImIp3nS	požadovat
dvě	dva	k4xCgFnPc4	dva
harfy	harfa	k1gFnPc4	harfa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dominantním	dominantní	k2eAgInSc6d1	dominantní
septakordu	septakord	k1gInSc6	septakord
přijdou	přijít	k5eAaPmIp3nP	přijít
na	na	k7c4	na
řadu	řada	k1gFnSc4	řada
dechové	dechový	k2eAgInPc4d1	dechový
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
následované	následovaný	k2eAgInPc4d1	následovaný
smyčci	smyčec	k1gInPc7	smyčec
<g/>
,	,	kIx,	,
po	po	k7c6	po
nichž	jenž	k3xRgInPc6	jenž
celý	celý	k2eAgInSc4d1	celý
orchestr	orchestr	k1gInSc4	orchestr
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
vrcholu	vrchol	k1gInSc2	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
další	další	k2eAgFnSc6d1	další
části	část	k1gFnSc6	část
Smetana	Smetana	k1gMnSc1	Smetana
připomíná	připomínat	k5eAaImIp3nS	připomínat
příběh	příběh	k1gInSc4	příběh
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
při	při	k7c6	při
čemž	což	k3yQnSc6	což
používá	používat	k5eAaImIp3nS	používat
rychlejší	rychlý	k2eAgNnSc4d2	rychlejší
tempo	tempo	k1gNnSc4	tempo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
připomíná	připomínat	k5eAaImIp3nS	připomínat
pochod	pochod	k1gInSc1	pochod
vítězství	vítězství	k1gNnSc2	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Zdánlivě	zdánlivě	k6eAd1	zdánlivě
triumfální	triumfální	k2eAgNnSc1d1	triumfální
vyvrcholení	vyvrcholení	k1gNnSc1	vyvrcholení
je	být	k5eAaImIp3nS	být
přerušeno	přerušit	k5eAaPmNgNnS	přerušit
krátkou	krátký	k2eAgFnSc7d1	krátká
sestupnou	sestupný	k2eAgFnSc7d1	sestupná
pasáží	pasáž	k1gFnSc7	pasáž
<g/>
,	,	kIx,	,
zachycující	zachycující	k2eAgInPc1d1	zachycující
zničení	zničení	k1gNnSc4	zničení
Vyšehradu	Vyšehrad	k1gInSc2	Vyšehrad
husity	husita	k1gMnSc2	husita
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
znovu	znovu	k6eAd1	znovu
zazní	zaznít	k5eAaPmIp3nS	zaznít
úvodní	úvodní	k2eAgFnSc4d1	úvodní
pasáž	pasáž	k1gFnSc4	pasáž
harf	harfa	k1gFnPc2	harfa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
opět	opět	k6eAd1	opět
připomíná	připomínat	k5eAaImIp3nS	připomínat
krásu	krása	k1gFnSc4	krása
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
proměnil	proměnit	k5eAaPmAgMnS	proměnit
v	v	k7c4	v
trosky	troska	k1gFnPc4	troska
<g/>
.	.	kIx.	.
</s>
<s>
Báseň	báseň	k1gFnSc1	báseň
končí	končit	k5eAaImIp3nS	končit
tiše	tiš	k1gFnPc4	tiš
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
řeku	řeka	k1gFnSc4	řeka
Vltavu	Vltava	k1gFnSc4	Vltava
protékající	protékající	k2eAgFnPc4d1	protékající
pod	pod	k7c7	pod
hradem	hrad	k1gInSc7	hrad
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Vltava	Vltava	k1gFnSc1	Vltava
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
dvanáct	dvanáct	k4xCc4	dvanáct
minut	minuta	k1gFnPc2	minuta
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tónině	tónina	k1gFnSc6	tónina
e	e	k0	e
moll	moll	k1gNnSc6	moll
<g/>
.	.	kIx.	.
</s>
<s>
Smetana	Smetana	k1gMnSc1	Smetana
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Skladba	skladba	k1gFnSc1	skladba
líčí	líčit	k5eAaImIp3nS	líčit
běh	běh	k1gInSc4	běh
Vltavy	Vltava	k1gFnSc2	Vltava
<g/>
,	,	kIx,	,
začínaje	začínaje	k7c7	začínaje
od	od	k7c2	od
prvních	první	k4xOgInPc2	první
obou	dva	k4xCgInPc2	dva
praménků	pramének	k1gInPc2	pramének
<g/>
,	,	kIx,	,
chladná	chladný	k2eAgFnSc1d1	chladná
a	a	k8xC	a
teplá	teplý	k2eAgFnSc1d1	teplá
Vltava	Vltava	k1gFnSc1	Vltava
<g/>
,	,	kIx,	,
spojení	spojení	k1gNnSc2	spojení
obou	dva	k4xCgInPc2	dva
potůčků	potůček	k1gInPc2	potůček
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
proudu	proud	k1gInSc2	proud
<g/>
;	;	kIx,	;
pak	pak	k6eAd1	pak
tok	tok	k1gInSc1	tok
Vltavy	Vltava	k1gFnSc2	Vltava
v	v	k7c6	v
hájích	háj	k1gInPc6	háj
a	a	k8xC	a
po	po	k7c6	po
lučinách	lučina	k1gFnPc6	lučina
<g/>
,	,	kIx,	,
krajinami	krajina	k1gFnPc7	krajina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zrovna	zrovna	k6eAd1	zrovna
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nP	slavit
veselé	veselý	k2eAgInPc1d1	veselý
hody	hod	k1gInPc1	hod
<g/>
;	;	kIx,	;
při	při	k7c6	při
noční	noční	k2eAgFnSc6d1	noční
záři	zář	k1gFnSc6	zář
lůny	lůno	k1gNnPc7	lůno
rej	rej	k1gFnSc1	rej
rusalek	rusalka	k1gFnPc2	rusalka
<g/>
;	;	kIx,	;
na	na	k7c6	na
blízkých	blízký	k2eAgFnPc6d1	blízká
skalách	skála	k1gFnPc6	skála
vypínají	vypínat	k5eAaImIp3nP	vypínat
se	se	k3xPyFc4	se
pyšně	pyšně	k6eAd1	pyšně
hrady	hrad	k1gInPc4	hrad
<g/>
,	,	kIx,	,
zámky	zámek	k1gInPc4	zámek
a	a	k8xC	a
zříceniny	zřícenina	k1gFnPc4	zřícenina
<g/>
,	,	kIx,	,
Vltava	Vltava	k1gFnSc1	Vltava
víří	vířit	k5eAaImIp3nS	vířit
v	v	k7c6	v
proudech	proud	k1gInPc6	proud
Svatojánských	svatojánský	k2eAgInPc2d1	svatojánský
<g/>
;	;	kIx,	;
teče	téct	k5eAaImIp3nS	téct
v	v	k7c6	v
širokém	široký	k2eAgInSc6d1	široký
toku	tok	k1gInSc6	tok
dále	daleko	k6eAd2	daleko
ku	k	k7c3	k
Praze	Praha	k1gFnSc3	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
<g/>
,	,	kIx,	,
konečně	konečně	k6eAd1	konečně
mizí	mizet	k5eAaImIp3nS	mizet
v	v	k7c6	v
dálce	dálka	k1gFnSc6	dálka
v	v	k7c6	v
majestátním	majestátní	k2eAgInSc6d1	majestátní
toku	tok	k1gInSc6	tok
svém	svůj	k3xOyFgMnSc6	svůj
v	v	k7c6	v
Labi	Labe	k1gNnSc6	Labe
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Vltava	Vltava	k1gFnSc1	Vltava
je	být	k5eAaImIp3nS	být
adaptací	adaptace	k1gFnSc7	adaptace
italské	italský	k2eAgFnSc2d1	italská
lidové	lidový	k2eAgFnSc2d1	lidová
písně	píseň	k1gFnSc2	píseň
renesančního	renesanční	k2eAgInSc2d1	renesanční
původu	původ	k1gInSc2	původ
La	la	k1gNnSc2	la
Mantovana	Mantovan	k1gMnSc2	Mantovan
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
motivy	motiv	k1gInPc4	motiv
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
i	i	k9	i
v	v	k7c6	v
izraelské	izraelský	k2eAgFnSc3d1	izraelská
státní	státní	k2eAgFnSc3d1	státní
hymně	hymna	k1gFnSc3	hymna
Hatikvě	Hatikva	k1gFnSc3	Hatikva
(	(	kIx(	(
<g/>
palestinský	palestinský	k2eAgInSc1d1	palestinský
rozhlas	rozhlas	k1gInSc1	rozhlas
dokonce	dokonce	k9	dokonce
vysílal	vysílat	k5eAaImAgInS	vysílat
shodnou	shodný	k2eAgFnSc4d1	shodná
část	část	k1gFnSc4	část
Vltavy	Vltava	k1gFnSc2	Vltava
namísto	namísto	k7c2	namísto
hatikvy	hatikva	k1gFnSc2	hatikva
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
státní	státní	k2eAgFnSc1d1	státní
hymna	hymna	k1gFnSc1	hymna
za	za	k7c2	za
britské	britský	k2eAgFnSc2d1	britská
nadvlády	nadvláda	k1gFnSc2	nadvláda
v	v	k7c6	v
Palestině	Palestina	k1gFnSc6	Palestina
zakázána	zakázat	k5eAaPmNgFnS	zakázat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
durové	durový	k2eAgFnSc6d1	durová
tónině	tónina	k1gFnSc6	tónina
se	se	k3xPyFc4	se
podobná	podobný	k2eAgFnSc1d1	podobná
melodie	melodie	k1gFnSc1	melodie
také	také	k9	také
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
lidové	lidový	k2eAgFnSc6d1	lidová
písni	píseň	k1gFnSc6	píseň
"	"	kIx"	"
<g/>
Kočka	kočka	k1gFnSc1	kočka
leze	lézt	k5eAaImIp3nS	lézt
dírou	díra	k1gFnSc7	díra
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Slovy	slovo	k1gNnPc7	slovo
autora	autor	k1gMnSc2	autor
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
skladbě	skladba	k1gFnSc6	skladba
se	se	k3xPyFc4	se
nemíní	mínit	k5eNaImIp3nS	mínit
krajina	krajina	k1gFnSc1	krajina
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
děj	děj	k1gInSc1	děj
<g/>
,	,	kIx,	,
bajka	bajka	k1gFnSc1	bajka
o	o	k7c6	o
dívce	dívka	k1gFnSc6	dívka
Šárce	Šárka	k1gFnSc6	Šárka
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
počne	počnout	k5eAaPmIp3nS	počnout
líčením	líčení	k1gNnSc7	líčení
rozzuřené	rozzuřený	k2eAgFnSc2d1	rozzuřená
dívky	dívka	k1gFnSc2	dívka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
pomstu	pomsta	k1gFnSc4	pomsta
přísahá	přísahat	k5eAaImIp3nS	přísahat
za	za	k7c4	za
nevěrnost	nevěrnost	k1gFnSc4	nevěrnost
milence	milenec	k1gMnSc4	milenec
celému	celý	k2eAgNnSc3d1	celé
mužskému	mužský	k2eAgNnSc3d1	mužské
pokolení	pokolení	k1gNnSc3	pokolení
<g/>
.	.	kIx.	.
</s>
<s>
Zdáli	zdáli	k6eAd1	zdáli
je	být	k5eAaImIp3nS	být
slyšet	slyšet	k5eAaImF	slyšet
příchod	příchod	k1gInSc4	příchod
Ctirada	Ctirad	k1gMnSc2	Ctirad
s	s	k7c7	s
jeho	jeho	k3xOp3gMnPc7	jeho
zbrojnoši	zbrojnoš	k1gMnPc7	zbrojnoš
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
táhne	táhnout	k5eAaImIp3nS	táhnout
na	na	k7c4	na
pokoření	pokoření	k1gNnSc4	pokoření
a	a	k8xC	a
potrestání	potrestání	k1gNnSc4	potrestání
dívek	dívka	k1gFnPc2	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
zdáli	zdáli	k6eAd1	zdáli
zaslechnou	zaslechnout	k5eAaPmIp3nP	zaslechnout
nářek	nářek	k1gInSc4	nářek
(	(	kIx(	(
<g/>
ač	ač	k8xS	ač
lstivý	lstivý	k2eAgMnSc1d1	lstivý
<g/>
)	)	kIx)	)
přivázané	přivázaný	k2eAgFnPc1d1	přivázaná
dívky	dívka	k1gFnPc1	dívka
u	u	k7c2	u
stromu	strom	k1gInSc2	strom
<g/>
,	,	kIx,	,
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
obdivuje	obdivovat	k5eAaImIp3nS	obdivovat
Ctirad	Ctirad	k1gMnSc1	Ctirad
krásu	krása	k1gFnSc4	krása
její	její	k3xOp3gFnSc3	její
–	–	k?	–
zahoří	zahořet	k5eAaPmIp3nP	zahořet
milostnými	milostný	k2eAgFnPc7d1	milostná
city	city	k1gFnPc7	city
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
osvobodí	osvobodit	k5eAaPmIp3nP	osvobodit
ji	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
ona	onen	k3xDgFnSc1	onen
připraveným	připravený	k2eAgInSc7d1	připravený
nápojem	nápoj	k1gInSc7	nápoj
obveselí	obveselit	k5eAaPmIp3nP	obveselit
a	a	k8xC	a
opojí	opojit	k5eAaPmIp3nP	opojit
jak	jak	k8xC	jak
Ctirada	Ctirad	k1gMnSc4	Ctirad
tak	tak	k8xS	tak
zbrojnoše	zbrojnoš	k1gMnSc4	zbrojnoš
až	až	k9	až
k	k	k7c3	k
spánku	spánek	k1gInSc3	spánek
<g/>
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
Na	na	k7c6	na
dané	daný	k2eAgFnSc6d1	daná
znamení	znamení	k1gNnSc6	znamení
lesním	lesní	k2eAgInSc7d1	lesní
rohem	roh	k1gInSc7	roh
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
v	v	k7c6	v
dálce	dálka	k1gFnSc6	dálka
ukryté	ukrytý	k2eAgFnSc2d1	ukrytá
dívky	dívka	k1gFnSc2	dívka
zodpoví	zodpovědět	k5eAaPmIp3nS	zodpovědět
<g/>
,	,	kIx,	,
vyhrnou	vyhrnout	k5eAaPmIp3nP	vyhrnout
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc4	tento
ku	k	k7c3	k
krvavému	krvavý	k2eAgInSc3d1	krvavý
činu	čin	k1gInSc3	čin
<g/>
,	,	kIx,	,
hrůza	hrůza	k1gFnSc1	hrůza
všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
vraždění	vraždění	k1gNnSc2	vraždění
<g/>
,	,	kIx,	,
vzteklost	vzteklost	k1gFnSc1	vzteklost
nasycené	nasycený	k2eAgFnSc2d1	nasycená
pomsty	pomsta	k1gFnSc2	pomsta
Šárky	Šárka	k1gFnSc2	Šárka
–	–	k?	–
toť	toť	k?	toť
konec	konec	k1gInSc4	konec
skladby	skladba	k1gFnSc2	skladba
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Skladbu	skladba	k1gFnSc4	skladba
otevírá	otevírat	k5eAaImIp3nS	otevírat
úvodní	úvodní	k2eAgFnSc1d1	úvodní
dynamická	dynamický	k2eAgFnSc1d1	dynamická
pasáž	pasáž	k1gFnSc1	pasáž
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
brzy	brzy	k6eAd1	brzy
střídá	střídat	k5eAaImIp3nS	střídat
střídmější	střídmý	k2eAgFnSc1d2	střídmější
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
líčí	líčit	k5eAaImIp3nS	líčit
pochod	pochod	k1gInSc4	pochod
Ctiradovy	Ctiradův	k2eAgFnSc2d1	Ctiradova
družiny	družina	k1gFnSc2	družina
<g/>
.	.	kIx.	.
</s>
<s>
Nenadálé	nenadálý	k2eAgInPc4d1	nenadálý
"	"	kIx"	"
<g/>
výbuchy	výbuch	k1gInPc1	výbuch
<g/>
"	"	kIx"	"
orchestru	orchestr	k1gInSc2	orchestr
potom	potom	k6eAd1	potom
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
spatření	spatření	k1gNnSc4	spatření
Šárky	Šárka	k1gFnSc2	Šárka
Ctiradem	Ctirad	k1gMnSc7	Ctirad
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
oddíl	oddíl	k1gInSc4	oddíl
Moderato	moderato	k6eAd1	moderato
<g/>
,	,	kIx,	,
ma	ma	k?	ma
con	con	k?	con
calore	calor	k1gMnSc5	calor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
barvitě	barvitě	k6eAd1	barvitě
vykresluje	vykreslovat	k5eAaImIp3nS	vykreslovat
Ctiradovo	Ctiradův	k2eAgNnSc1d1	Ctiradovo
milostné	milostný	k2eAgNnSc1d1	milostné
vzplanutí	vzplanutí	k1gNnSc1	vzplanutí
k	k	k7c3	k
Šárce	Šárka	k1gFnSc3	Šárka
a	a	k8xC	a
podle	podle	k7c2	podle
Václava	Václav	k1gMnSc2	Václav
Holzknechta	Holzknecht	k1gMnSc2	Holzknecht
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejerotičtějších	erotický	k2eAgFnPc2d3	nejerotičtější
Smetanových	Smetanových	k2eAgFnPc2d1	Smetanových
scén	scéna	k1gFnPc2	scéna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
básni	báseň	k1gFnSc6	báseň
se	se	k3xPyFc4	se
nevypráví	vyprávět	k5eNaImIp3nS	vyprávět
žádný	žádný	k3yNgInSc1	žádný
reálný	reálný	k2eAgInSc1d1	reálný
příběh	příběh	k1gInSc1	příběh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
oslavuje	oslavovat	k5eAaImIp3nS	oslavovat
se	se	k3xPyFc4	se
krása	krása	k1gFnSc1	krása
české	český	k2eAgFnSc2d1	Česká
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
části	část	k1gFnSc6	část
vykresluje	vykreslovat	k5eAaImIp3nS	vykreslovat
vznešenost	vznešenost	k1gFnSc1	vznešenost
lesa	les	k1gInSc2	les
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
následuje	následovat	k5eAaImIp3nS	následovat
popis	popis	k1gInSc1	popis
vesnické	vesnický	k2eAgFnSc2d1	vesnická
slavnosti	slavnost	k1gFnSc2	slavnost
<g/>
.	.	kIx.	.
</s>
<s>
Inspirací	inspirace	k1gFnSc7	inspirace
byla	být	k5eAaImAgFnS	být
autorovi	autor	k1gMnSc3	autor
krajina	krajina	k1gFnSc1	krajina
kolem	kolem	k7c2	kolem
Jabkenic	Jabkenice	k1gFnPc2	Jabkenice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
pobýval	pobývat	k5eAaImAgMnS	pobývat
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
básni	báseň	k1gFnSc6	báseň
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Toť	Toť	k?	Toť
všeobecné	všeobecný	k2eAgNnSc4d1	všeobecné
kreslení	kreslení	k1gNnSc4	kreslení
citů	cit	k1gInPc2	cit
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
na	na	k7c4	na
českou	český	k2eAgFnSc4d1	Česká
zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
stran	strana	k1gFnPc2	strana
tu	tu	k6eAd1	tu
zazní	zaznít	k5eAaPmIp3nS	zaznít
zpěv	zpěv	k1gInSc1	zpěv
plno	plno	k6eAd1	plno
vroucnosti	vroucnost	k1gFnSc2	vroucnost
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
veselý	veselý	k2eAgMnSc1d1	veselý
tak	tak	k8xS	tak
melancholický	melancholický	k2eAgMnSc1d1	melancholický
z	z	k7c2	z
hájů	háj	k1gInPc2	háj
a	a	k8xC	a
luhů	luh	k1gInPc2	luh
<g/>
.	.	kIx.	.
</s>
<s>
Lesní	lesní	k2eAgInPc1d1	lesní
kraje	kraj	k1gInPc1	kraj
v	v	k7c6	v
sólech	sólo	k1gNnPc6	sólo
hornistů	hornista	k1gMnPc2	hornista
–	–	k?	–
a	a	k8xC	a
vesele	vesele	k6eAd1	vesele
ourodné	ourodný	k2eAgFnPc4d1	ourodná
nížiny	nížina	k1gFnPc4	nížina
labské	labský	k2eAgNnSc4d1	Labské
a	a	k8xC	a
jiné	jiný	k2eAgNnSc4d1	jiné
a	a	k8xC	a
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
,	,	kIx,	,
vše	všechen	k3xTgNnSc1	všechen
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
opěvuje	opěvovat	k5eAaImIp3nS	opěvovat
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
může	moct	k5eAaImIp3nS	moct
ze	z	k7c2	z
skladby	skladba	k1gFnSc2	skladba
té	ten	k3xDgFnSc2	ten
si	se	k3xPyFc3	se
vykreslit	vykreslit	k5eAaPmF	vykreslit
<g/>
,	,	kIx,	,
co	co	k9	co
mu	on	k3xPp3gMnSc3	on
libo	libo	k6eAd1	libo
–	–	k?	–
básník	básník	k1gMnSc1	básník
má	mít	k5eAaImIp3nS	mít
volnou	volný	k2eAgFnSc4d1	volná
cestu	cesta	k1gFnSc4	cesta
před	před	k7c7	před
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
arciť	arciť	k9	arciť
musí	muset	k5eAaImIp3nS	muset
skladbu	skladba	k1gFnSc4	skladba
v	v	k7c6	v
jednotlivostech	jednotlivost	k1gFnPc6	jednotlivost
sledovat	sledovat	k5eAaImF	sledovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Smetana	Smetana	k1gMnSc1	Smetana
považoval	považovat	k5eAaImAgMnS	považovat
dobu	doba	k1gFnSc4	doba
husitství	husitství	k1gNnSc2	husitství
za	za	k7c4	za
vrcholné	vrcholný	k2eAgNnSc4d1	vrcholné
období	období	k1gNnSc4	období
českých	český	k2eAgFnPc2d1	Česká
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Tábor	Tábor	k1gInSc1	Tábor
je	být	k5eAaImIp3nS	být
oslavou	oslava	k1gFnSc7	oslava
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Podkladem	podklad	k1gInSc7	podklad
básně	báseň	k1gFnSc2	báseň
je	být	k5eAaImIp3nS	být
husitský	husitský	k2eAgInSc1d1	husitský
chorál	chorál	k1gInSc1	chorál
Ktož	Ktož	k1gFnSc1	Ktož
jsú	jsú	k?	jsú
boží	boží	k2eAgMnPc1d1	boží
bojovníci	bojovník	k1gMnPc1	bojovník
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
podobách	podoba	k1gFnPc6	podoba
objevuje	objevovat	k5eAaImIp3nS	objevovat
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
skladby	skladba	k1gFnSc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Smetana	Smetana	k1gMnSc1	Smetana
si	se	k3xPyFc3	se
o	o	k7c6	o
Táboru	tábor	k1gInSc6	tábor
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Motto	motto	k1gNnSc1	motto
<g/>
:	:	kIx,	:
Kdož	kdož	k3yRnSc1	kdož
jste	být	k5eAaImIp2nP	být
boží	boží	k2eAgMnPc1d1	boží
bojovníci	bojovník	k1gMnPc1	bojovník
<g/>
!	!	kIx.	!
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
velebné	velebný	k2eAgFnSc2d1	velebná
písně	píseň	k1gFnSc2	píseň
pozůstává	pozůstávat	k5eAaImIp3nS	pozůstávat
celá	celý	k2eAgFnSc1d1	celá
stavba	stavba	k1gFnSc1	stavba
skladby	skladba	k1gFnSc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sídle	sídlo	k1gNnSc6	sídlo
hlavním	hlavní	k2eAgNnSc6d1	hlavní
–	–	k?	–
v	v	k7c6	v
Táboře	Tábor	k1gInSc6	Tábor
–	–	k?	–
zazněl	zaznít	k5eAaPmAgInS	zaznít
tento	tento	k3xDgInSc1	tento
zpěv	zpěv	k1gInSc1	zpěv
zajisté	zajisté	k9	zajisté
nejmohutněji	mohutně	k6eAd3	mohutně
a	a	k8xC	a
nejčastěji	často	k6eAd3	často
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
líčí	líčit	k5eAaImIp3nS	líčit
též	též	k9	též
pevnou	pevný	k2eAgFnSc4d1	pevná
vůli	vůle	k1gFnSc4	vůle
<g/>
,	,	kIx,	,
vítězné	vítězný	k2eAgInPc4d1	vítězný
boje	boj	k1gInPc4	boj
a	a	k8xC	a
vytrvalost	vytrvalost	k1gFnSc4	vytrvalost
<g/>
,	,	kIx,	,
a	a	k8xC	a
tvrdošíjnou	tvrdošíjný	k2eAgFnSc4d1	tvrdošíjná
neústupnost	neústupnost	k1gFnSc4	neústupnost
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
skladba	skladba	k1gFnSc1	skladba
též	též	k6eAd1	též
také	také	k9	také
končí	končit	k5eAaImIp3nS	končit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
detailu	detail	k1gInSc2	detail
se	se	k3xPyFc4	se
nedá	dát	k5eNaPmIp3nS	dát
rozdrobit	rozdrobit	k5eAaPmF	rozdrobit
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
zahrne	zahrnout	k5eAaPmIp3nS	zahrnout
všeobecnou	všeobecný	k2eAgFnSc4d1	všeobecná
slávu	sláva	k1gFnSc4	sláva
a	a	k8xC	a
chválu	chvála	k1gFnSc4	chvála
husitských	husitský	k2eAgInPc2d1	husitský
bojů	boj	k1gInPc2	boj
a	a	k8xC	a
nezlomnost	nezlomnost	k1gFnSc4	nezlomnost
povahy	povaha	k1gFnSc2	povaha
husitův	husitův	k2eAgMnSc1d1	husitův
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Hudebně	hudebně	k6eAd1	hudebně
začíná	začínat	k5eAaImIp3nS	začínat
Blaník	Blaník	k1gInSc1	Blaník
přesně	přesně	k6eAd1	přesně
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
končí	končit	k5eAaImIp3nS	končit
Tábor	Tábor	k1gInSc4	Tábor
a	a	k8xC	a
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
i	i	k9	i
obdobné	obdobný	k2eAgInPc4d1	obdobný
motivy	motiv	k1gInPc4	motiv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
básni	báseň	k1gFnSc6	báseň
například	například	k6eAd1	například
opět	opět	k6eAd1	opět
zazní	zaznět	k5eAaImIp3nS	zaznět
husitský	husitský	k2eAgInSc4d1	husitský
chorál	chorál	k1gInSc4	chorál
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
tentokrát	tentokrát	k6eAd1	tentokrát
jiná	jiný	k2eAgFnSc1d1	jiná
pasáž	pasáž	k1gFnSc1	pasáž
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
v	v	k7c6	v
závěrečné	závěrečný	k2eAgFnSc6d1	závěrečná
části	část	k1gFnSc6	část
Blaníku	Blaník	k1gInSc2	Blaník
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
text	text	k1gInSc1	text
převzaté	převzatý	k2eAgFnSc2d1	převzatá
pasáže	pasáž	k1gFnSc2	pasáž
husitského	husitský	k2eAgInSc2d1	husitský
chorálu	chorál	k1gInSc2	chorál
"	"	kIx"	"
<g/>
že	že	k8xS	že
konečně	konečně	k6eAd1	konečně
vždycky	vždycky	k6eAd1	vždycky
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
zvítězíte	zvítězit	k5eAaPmIp2nP	zvítězit
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
odkazem	odkaz	k1gInSc7	odkaz
ke	k	k7c3	k
vzkříšení	vzkříšení	k1gNnSc3	vzkříšení
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc3	jeho
vítěznému	vítězný	k2eAgInSc3d1	vítězný
vzestupu	vzestup	k1gInSc3	vzestup
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnPc1d1	poslední
dvě	dva	k4xCgFnPc1	dva
básně	báseň	k1gFnPc1	báseň
cyklu	cyklus	k1gInSc2	cyklus
tvoří	tvořit	k5eAaImIp3nP	tvořit
soudržný	soudržný	k2eAgInSc4d1	soudržný
pár	pár	k1gInSc4	pár
<g/>
,	,	kIx,	,
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k8xC	jako
první	první	k4xOgFnPc1	první
dvě	dva	k4xCgFnPc1	dva
básně	báseň	k1gFnPc1	báseň
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
a	a	k8xC	a
Vltava	Vltava	k1gFnSc1	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Slovy	slovo	k1gNnPc7	slovo
autora	autor	k1gMnSc2	autor
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jest	být	k5eAaImIp3nS	být
pokračování	pokračování	k1gNnSc4	pokračování
předešlé	předešlý	k2eAgFnSc2d1	předešlá
skladby	skladba	k1gFnSc2	skladba
<g/>
:	:	kIx,	:
Tábor	Tábor	k1gInSc1	Tábor
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přemožení	přemožení	k1gNnSc6	přemožení
reků	rek	k1gMnPc2	rek
husitských	husitský	k2eAgInPc2d1	husitský
skryli	skrýt	k5eAaPmAgMnP	skrýt
se	se	k3xPyFc4	se
tito	tento	k3xDgMnPc1	tento
v	v	k7c6	v
Blaníku	Blaník	k1gInSc6	Blaník
a	a	k8xC	a
čekají	čekat	k5eAaImIp3nP	čekat
v	v	k7c6	v
těžkém	těžký	k2eAgInSc6d1	těžký
spánku	spánek	k1gInSc6	spánek
na	na	k7c4	na
okamžik	okamžik	k1gInSc4	okamžik
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vlasti	vlast	k1gFnSc2	vlast
mají	mít	k5eAaImIp3nP	mít
přijíti	přijít	k5eAaPmF	přijít
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
ty	ten	k3xDgInPc4	ten
samé	samý	k3xTgInPc4	samý
motivy	motiv	k1gInPc4	motiv
jako	jako	k8xS	jako
v	v	k7c6	v
Táboře	Tábor	k1gInSc6	Tábor
slouží	sloužit	k5eAaImIp3nS	sloužit
v	v	k7c6	v
Blaníku	Blaník	k1gInSc6	Blaník
za	za	k7c4	za
podklad	podklad	k1gInSc4	podklad
stavby	stavba	k1gFnSc2	stavba
<g/>
:	:	kIx,	:
'	'	kIx"	'
<g/>
Kdo	kdo	k3yQnSc1	kdo
jste	být	k5eAaImIp2nP	být
boží	boží	k2eAgMnPc1d1	boží
bojovníci	bojovník	k1gMnPc1	bojovník
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
podkladě	podklad	k1gInSc6	podklad
této	tento	k3xDgFnSc2	tento
melodie	melodie	k1gFnSc2	melodie
(	(	kIx(	(
<g/>
tohoto	tento	k3xDgInSc2	tento
hus	husa	k1gFnPc2	husa
<g/>
.	.	kIx.	.
principu	princip	k1gInSc2	princip
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyvine	vyvinout	k5eAaPmIp3nS	vyvinout
vzkříšení	vzkříšení	k1gNnSc1	vzkříšení
národa	národ	k1gInSc2	národ
českého	český	k2eAgInSc2d1	český
<g/>
,	,	kIx,	,
budoucí	budoucí	k2eAgNnSc1d1	budoucí
štěstí	štěstí	k1gNnSc1	štěstí
a	a	k8xC	a
sláva	sláva	k1gFnSc1	sláva
<g/>
!	!	kIx.	!
</s>
<s>
kterým	který	k3yQgInSc7	který
vítězným	vítězný	k2eAgInSc7d1	vítězný
hymnusem	hymnus	k1gInSc7	hymnus
<g/>
,	,	kIx,	,
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
pochodu	pochod	k1gInSc2	pochod
<g/>
,	,	kIx,	,
skončí	skončit	k5eAaPmIp3nS	skončit
skladba	skladba	k1gFnSc1	skladba
a	a	k8xC	a
tak	tak	k6eAd1	tak
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
symf	symf	k1gMnSc1	symf
<g/>
.	.	kIx.	.
básní	báseň	k1gFnPc2	báseň
"	"	kIx"	"
<g/>
Vlasť	Vlasť	k1gFnSc1	Vlasť
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k9	co
malé	malý	k2eAgNnSc1d1	malé
intermezzo	intermezzo	k1gNnSc1	intermezzo
zazní	zaznít	k5eAaPmIp3nS	zaznít
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
skladbě	skladba	k1gFnSc6	skladba
též	též	k9	též
kratinká	kratinký	k2eAgFnSc1d1	kratinká
idyla	idyla	k1gFnSc1	idyla
<g/>
:	:	kIx,	:
kresba	kresba	k1gFnSc1	kresba
polohy	poloha	k1gFnSc2	poloha
Blaníku	Blaník	k1gInSc2	Blaník
<g/>
,	,	kIx,	,
malý	malý	k2eAgMnSc1d1	malý
pastucha	pastucha	k1gMnSc1	pastucha
si	se	k3xPyFc3	se
huláká	hulákat	k5eAaImIp3nS	hulákat
a	a	k8xC	a
hraje	hrát	k5eAaImIp3nS	hrát
a	a	k8xC	a
ozvěna	ozvěna	k1gFnSc1	ozvěna
mu	on	k3xPp3gMnSc3	on
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
