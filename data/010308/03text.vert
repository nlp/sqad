<p>
<s>
Hlásnice	hlásnice	k1gFnSc1	hlásnice
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Wachteldorf	Wachteldorf	k1gInSc1	Wachteldorf
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vesnice	vesnice	k1gFnSc1	vesnice
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Svitavy	Svitava	k1gFnSc2	Svitava
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
asi	asi	k9	asi
2	[number]	k4	2
km	km	kA	km
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
Trpínu	Trpín	k1gInSc2	Trpín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
evidováno	evidovat	k5eAaImNgNnS	evidovat
62	[number]	k4	62
adres	adresa	k1gFnPc2	adresa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
zde	zde	k6eAd1	zde
trvale	trvale	k6eAd1	trvale
žilo	žít	k5eAaImAgNnS	žít
64	[number]	k4	64
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
<g/>
Hlásnice	hlásnice	k1gFnSc1	hlásnice
je	být	k5eAaImIp3nS	být
také	také	k9	také
název	název	k1gInSc1	název
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
3,22	[number]	k4	3,22
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1437	[number]	k4	1437
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Památky	památka	k1gFnPc1	památka
==	==	k?	==
</s>
</p>
<p>
<s>
pomník	pomník	k1gInSc4	pomník
obětem	oběť	k1gFnPc3	oběť
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
u	u	k7c2	u
požární	požární	k2eAgFnSc2d1	požární
zbrojnice	zbrojnice	k1gFnSc2	zbrojnice
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
<g/>
:	:	kIx,	:
1914	[number]	k4	1914
–	–	k?	–
1918	[number]	k4	1918
/	/	kIx~	/
Vzpomeňte	vzpomenout	k5eAaPmRp2nP	vzpomenout
na	na	k7c4	na
nás	my	k3xPp1nPc4	my
/	/	kIx~	/
padli	padnout	k5eAaImAgMnP	padnout
jsme	být	k5eAaImIp1nP	být
za	za	k7c4	za
Vás	vy	k3xPp2nPc4	vy
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
Bureš	Bureš	k1gMnSc1	Bureš
Vilém	Vilém	k1gMnSc1	Vilém
č.	č.	k?	č.
42	[number]	k4	42
<g/>
,	,	kIx,	,
Bureš	Bureš	k1gMnSc1	Bureš
Václav	Václav	k1gMnSc1	Václav
č.	č.	k?	č.
5	[number]	k4	5
<g/>
,	,	kIx,	,
Brtoun	Brtoun	k1gMnSc1	Brtoun
Josef	Josef	k1gMnSc1	Josef
č.	č.	k?	č.
52	[number]	k4	52
<g/>
,	,	kIx,	,
Mach	Mach	k1gMnSc1	Mach
Antonín	Antonín	k1gMnSc1	Antonín
č.	č.	k?	č.
28	[number]	k4	28
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Mach	Mach	k1gMnSc1	Mach
Václav	Václav	k1gMnSc1	Václav
č.	č.	k?	č.
28	[number]	k4	28
<g/>
,	,	kIx,	,
Král	Král	k1gMnSc1	Král
Václav	Václav	k1gMnSc1	Václav
č.	č.	k?	č.
47	[number]	k4	47
<g/>
,	,	kIx,	,
Peterka	Peterka	k1gMnSc1	Peterka
Josef	Josef	k1gMnSc1	Josef
č.	č.	k?	č.
9	[number]	k4	9
<g/>
,	,	kIx,	,
Peterka	Peterka	k1gMnSc1	Peterka
Antonín	Antonín	k1gMnSc1	Antonín
č.	č.	k?	č.
10	[number]	k4	10
<g/>
,	,	kIx,	,
Peterka	Peterka	k1gMnSc1	Peterka
Václav	Václav	k1gMnSc1	Václav
č.	č.	k?	č.
11	[number]	k4	11
<g/>
,	,	kIx,	,
Peterka	Peterka	k1gMnSc1	Peterka
Václav	Václav	k1gMnSc1	Václav
č.	č.	k?	č.
11	[number]	k4	11
<g/>
,	,	kIx,	,
Peterka	Peterka	k1gMnSc1	Peterka
Antonín	Antonín	k1gMnSc1	Antonín
č.	č.	k?	č.
14	[number]	k4	14
<g/>
,	,	kIx,	,
Peterka	Peterka	k1gMnSc1	Peterka
Václav	Václav	k1gMnSc1	Václav
č.	č.	k?	č.
28	[number]	k4	28
<g/>
,	,	kIx,	,
Peterka	Peterka	k1gMnSc1	Peterka
Antonín	Antonín	k1gMnSc1	Antonín
č.	č.	k?	č.
32	[number]	k4	32
<g/>
,	,	kIx,	,
Peterka	Peterka	k1gMnSc1	Peterka
Leopold	Leopolda	k1gFnPc2	Leopolda
č.	č.	k?	č.
32	[number]	k4	32
<g/>
.	.	kIx.	.
</s>
<s>
Peterka	Peterka	k1gMnSc1	Peterka
Adolf	Adolf	k1gMnSc1	Adolf
č.	č.	k?	č.
36	[number]	k4	36
<g/>
,	,	kIx,	,
Peterka	Peterka	k1gMnSc1	Peterka
Antonín	Antonín	k1gMnSc1	Antonín
č.	č.	k?	č.
<g/>
36	[number]	k4	36
</s>
</p>
<p>
<s>
mramorový	mramorový	k2eAgInSc1d1	mramorový
kříž	kříž	k1gInSc1	kříž
před	před	k7c7	před
kaplí	kaple	k1gFnSc7	kaple
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
<g/>
:	:	kIx,	:
Postaven	postaven	k2eAgMnSc1d1	postaven
v	v	k7c6	v
jubilejním	jubilejní	k2eAgInSc6d1	jubilejní
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
nákladem	náklad	k1gInSc7	náklad
manželů	manžel	k1gMnPc2	manžel
Peterkových	Peterkových	k2eAgMnPc2d1	Peterkových
z	z	k7c2	z
Hlásnice	hlásnice	k1gFnSc2	hlásnice
č.	č.	k?	č.
35	[number]	k4	35
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hlásnice	hlásnice	k1gFnSc2	hlásnice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Katastrální	katastrální	k2eAgFnSc1d1	katastrální
mapa	mapa	k1gFnSc1	mapa
katastru	katastr	k1gInSc2	katastr
Hlásnice	hlásnice	k1gFnSc2	hlásnice
na	na	k7c6	na
webu	web	k1gInSc6	web
ČÚZK	ČÚZK	kA	ČÚZK
</s>
</p>
