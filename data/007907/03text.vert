<s>
Kamila	Kamila	k1gFnSc1	Kamila
je	být	k5eAaImIp3nS	být
ženské	ženský	k2eAgNnSc4d1	ženské
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
latinského	latinský	k2eAgInSc2d1	latinský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
přechýlené	přechýlený	k2eAgNnSc1d1	přechýlené
z	z	k7c2	z
mužského	mužský	k2eAgNnSc2d1	mužské
Kamil	Kamil	k1gMnSc1	Kamil
<g/>
.	.	kIx.	.
</s>
<s>
Svátek	svátek	k1gInSc4	svátek
v	v	k7c6	v
ČR	ČR	kA	ČR
slaví	slavit	k5eAaImIp3nS	slavit
podle	podle	k7c2	podle
občanského	občanský	k2eAgInSc2d1	občanský
kalendáře	kalendář	k1gInSc2	kalendář
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
latinského	latinský	k2eAgNnSc2d1	latinské
camilla	camillo	k1gNnSc2	camillo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
význam	význam	k1gInSc1	význam
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
osoba	osoba	k1gFnSc1	osoba
vznešeného	vznešený	k2eAgInSc2d1	vznešený
původu	původ	k1gInSc2	původ
vhodná	vhodný	k2eAgFnSc1d1	vhodná
pro	pro	k7c4	pro
kněžskou	kněžský	k2eAgFnSc4d1	kněžská
službu	služba	k1gFnSc4	služba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
četnost	četnost	k1gFnSc4	četnost
jména	jméno	k1gNnSc2	jméno
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
pořadí	pořadí	k1gNnSc2	pořadí
mezi	mezi	k7c7	mezi
ženskými	ženský	k2eAgNnPc7d1	ženské
jmény	jméno	k1gNnPc7	jméno
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
dvou	dva	k4xCgInPc2	dva
roků	rok	k1gInPc2	rok
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgInPc4	který
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
údaje	údaj	k1gInPc1	údaj
MV	MV	kA	MV
ČR	ČR	kA	ČR
–	–	k?	–
lze	lze	k6eAd1	lze
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
tedy	tedy	k9	tedy
vysledovat	vysledovat	k5eAaImF	vysledovat
trend	trend	k1gInSc4	trend
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
:	:	kIx,	:
Změna	změna	k1gFnSc1	změna
procentního	procentní	k2eAgNnSc2d1	procentní
zastoupení	zastoupení	k1gNnSc2	zastoupení
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
mezi	mezi	k7c7	mezi
žijícími	žijící	k2eAgFnPc7d1	žijící
ženami	žena	k1gFnPc7	žena
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
procentní	procentní	k2eAgFnSc1d1	procentní
změna	změna	k1gFnSc1	změna
se	se	k3xPyFc4	se
započítáním	započítání	k1gNnSc7	započítání
celkového	celkový	k2eAgInSc2d1	celkový
úbytku	úbytek	k1gInSc2	úbytek
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
ČR	ČR	kA	ČR
za	za	k7c4	za
sledované	sledovaný	k2eAgInPc4d1	sledovaný
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
+2,3	+2,3	k4	+2,3
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byla	být	k5eAaImAgFnS	být
Kamila	Kamila	k1gFnSc1	Kamila
43	[number]	k4	43
<g/>
.	.	kIx.	.
nejčetnějším	četný	k2eAgNnSc7d3	nejčetnější
jménem	jméno	k1gNnSc7	jméno
mezi	mezi	k7c7	mezi
novorozenci	novorozenec	k1gMnPc7	novorozenec
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ilustraci	ilustrace	k1gFnSc4	ilustrace
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
uvedeno	uvést	k5eAaPmNgNnS	uvést
několik	několik	k4yIc1	několik
variant	varianta	k1gFnPc2	varianta
zdrobnělin	zdrobnělina	k1gFnPc2	zdrobnělina
jména	jméno	k1gNnSc2	jméno
<g/>
:	:	kIx,	:
Kamilka	Kamilka	k1gFnSc1	Kamilka
<g/>
,	,	kIx,	,
Kamča	Kamča	k?	Kamča
<g/>
,	,	kIx,	,
Kakamila	Kakamila	k1gFnSc1	Kakamila
<g/>
,	,	kIx,	,
Kamilečka	Kamilečka	k1gFnSc1	Kamilečka
<g/>
,	,	kIx,	,
Milka	Milka	k1gFnSc1	Milka
<g/>
,	,	kIx,	,
Kamuška	Kamuška	k1gFnSc1	Kamuška
<g/>
,	,	kIx,	,
Kamísek	Kamísek	k1gInSc1	Kamísek
<g/>
,	,	kIx,	,
Kamičátko	Kamičátko	k1gNnSc1	Kamičátko
<g/>
,	,	kIx,	,
Kamoušek	Kamoušek	k1gMnSc1	Kamoušek
<g/>
,	,	kIx,	,
Kami	Kami	k1gNnSc1	Kami
<g/>
,	,	kIx,	,
Kamílek	Kamílek	k1gInSc1	Kamílek
<g/>
,	,	kIx,	,
Kamčátko	Kamčátko	k1gNnSc1	Kamčátko
<g/>
,	,	kIx,	,
Kaminka	Kaminka	k1gFnSc1	Kaminka
<g/>
,	,	kIx,	,
Míla	Míla	k1gFnSc1	Míla
<g/>
.	.	kIx.	.
</s>
<s>
Camila	Camít	k5eAaPmAgFnS	Camít
Cabello	Cabello	k1gNnSc4	Cabello
-	-	kIx~	-
Anglická	anglický	k2eAgFnSc1d1	anglická
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Camilla	Camilla	k1gMnSc1	Camilla
Parker-Bowles	Parker-Bowles	k1gMnSc1	Parker-Bowles
–	–	k?	–
hraběnka	hraběnka	k1gFnSc1	hraběnka
z	z	k7c2	z
Cornwallu	Cornwall	k1gInSc2	Cornwall
Kamila	Kamila	k1gFnSc1	Kamila
Berndorffová	Berndorffový	k2eAgFnSc1d1	Berndorffová
-	-	kIx~	-
současná	současný	k2eAgFnSc1d1	současná
česká	český	k2eAgFnSc1d1	Česká
reportážní	reportážní	k2eAgFnSc1d1	reportážní
a	a	k8xC	a
dokumentární	dokumentární	k2eAgFnSc1d1	dokumentární
fotografka	fotografka	k1gFnSc1	fotografka
Kamila	Kamila	k1gFnSc1	Kamila
Bezpalcová	Bezpalcová	k1gFnSc1	Bezpalcová
-	-	kIx~	-
česká	český	k2eAgFnSc1d1	Česká
modelka	modelka	k1gFnSc1	modelka
<g/>
,	,	kIx,	,
fotomodelka	fotomodelka	k1gFnSc1	fotomodelka
<g/>
,	,	kIx,	,
hosteska	hosteska	k1gFnSc1	hosteska
<g/>
,	,	kIx,	,
komparzistka	komparzistka	k1gFnSc1	komparzistka
<g/>
,	,	kIx,	,
personalistka	personalistka	k1gFnSc1	personalistka
a	a	k8xC	a
finalistka	finalistka	k1gFnSc1	finalistka
České	český	k2eAgFnSc2d1	Česká
Miss	miss	k1gFnSc2	miss
2014	[number]	k4	2014
Kamila	Kamila	k1gFnSc1	Kamila
Doležalová	Doležalová	k1gFnSc1	Doležalová
-	-	kIx~	-
česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
československá	československý	k2eAgFnSc1d1	Československá
politička	politička	k1gFnSc1	politička
Kamila	Kamila	k1gFnSc1	Kamila
<g />
.	.	kIx.	.
</s>
<s>
Gasiuk-Pihowicz	Gasiuk-Pihowicz	k1gInSc1	Gasiuk-Pihowicz
-	-	kIx~	-
polská	polský	k2eAgFnSc1d1	polská
právnička	právnička	k1gFnSc1	právnička
a	a	k8xC	a
politička	politička	k1gFnSc1	politička
Kamila	Kamila	k1gFnSc1	Kamila
Hájková	Hájková	k1gFnSc1	Hájková
-	-	kIx~	-
česká	český	k2eAgFnSc1d1	Česká
krasobruslařka	krasobruslařka	k1gFnSc1	krasobruslařka
Kamila	Kamila	k1gFnSc1	Kamila
Chudziková	Chudzikový	k2eAgFnSc1d1	Chudzikový
-	-	kIx~	-
polská	polský	k2eAgFnSc1d1	polská
atletka	atletka	k1gFnSc1	atletka
Kamila	Kamila	k1gFnSc1	Kamila
Klugarová	Klugarová	k1gFnSc1	Klugarová
-	-	kIx~	-
česká	český	k2eAgFnSc1d1	Česká
varhanice	varhanice	k1gFnSc1	varhanice
Kamila	Kamila	k1gFnSc1	Kamila
Lićwinková	Lićwinkový	k2eAgFnSc1d1	Lićwinkový
-	-	kIx~	-
polská	polský	k2eAgFnSc1d1	polská
atletka	atletka	k1gFnSc1	atletka
Kamila	Kamila	k1gFnSc1	Kamila
Magálová	Magálový	k2eAgFnSc1d1	Magálová
-	-	kIx~	-
slovenská	slovenský	k2eAgFnSc1d1	slovenská
herečka	herečka	k1gFnSc1	herečka
Kamila	Kamila	k1gFnSc1	Kamila
Moučková	Moučková	k1gFnSc1	Moučková
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
televizní	televizní	k2eAgFnSc1d1	televizní
hlasatelka	hlasatelka	k1gFnSc1	hlasatelka
(	(	kIx(	(
<g/>
rok	rok	k1gInSc1	rok
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
a	a	k8xC	a
moderátorka	moderátorka	k1gFnSc1	moderátorka
Kamila	Kamila	k1gFnSc1	Kamila
Neumannová	Neumannová	k1gFnSc1	Neumannová
-	-	kIx~	-
česká	český	k2eAgFnSc1d1	Česká
nakladatelka	nakladatelka	k1gFnSc1	nakladatelka
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
choť	choť	k1gMnSc1	choť
S.	S.	kA	S.
K.	K.	kA	K.
Neumanna	Neumann	k1gMnSc2	Neumann
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
herce	herec	k1gMnSc2	herec
Stanislava	Stanislav	k1gMnSc2	Stanislav
Neumanna	Neumann	k1gMnSc2	Neumann
a	a	k8xC	a
překladatelky	překladatelka	k1gFnSc2	překladatelka
Kamily	Kamila	k1gFnSc2	Kamila
Značkovské-Neumannové	Značkovské-Neumannová	k1gFnSc2	Značkovské-Neumannová
Kamila	Kamila	k1gFnSc1	Kamila
Nývltová	Nývltová	k1gFnSc1	Nývltová
-	-	kIx~	-
česká	český	k2eAgFnSc1d1	Česká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Kamila	Kamila	k1gFnSc1	Kamila
Pešková	Pešková	k1gFnSc1	Pešková
-	-	kIx~	-
česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
československá	československý	k2eAgFnSc1d1	Československá
lékařka	lékařka	k1gFnSc1	lékařka
Kamila	Kamila	k1gFnSc1	Kamila
Rajdlová	Rajdlový	k2eAgFnSc1d1	Rajdlová
-	-	kIx~	-
česká	český	k2eAgFnSc1d1	Česká
běžkyně	běžkyně	k1gFnSc1	běžkyně
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
Kamila	Kamila	k1gFnSc1	Kamila
Skolimowska	Skolimowska	k1gFnSc1	Skolimowska
-	-	kIx~	-
polská	polský	k2eAgFnSc1d1	polská
atletka	atletka	k1gFnSc1	atletka
Kamila	Kamila	k1gFnSc1	Kamila
Sojková	Sojková	k1gFnSc1	Sojková
-	-	kIx~	-
česká	český	k2eAgFnSc1d1	Česká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Kamila	Kamila	k1gFnSc1	Kamila
Stösslová	Stösslový	k2eAgFnSc1d1	Stösslová
-	-	kIx~	-
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
a	a	k8xC	a
pozdní	pozdní	k2eAgFnSc1d1	pozdní
láska	láska	k1gFnSc1	láska
Leoše	Leoš	k1gMnSc2	Leoš
Janáčka	Janáček	k1gMnSc2	Janáček
Kamila	Kamil	k1gMnSc2	Kamil
Špráchalová	Špráchalový	k2eAgFnSc1d1	Špráchalový
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
Kamila	Kamila	k1gFnSc1	Kamila
Thompson	Thompson	k1gInSc1	Thompson
-	-	kIx~	-
anglická	anglický	k2eAgFnSc1d1	anglická
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Kamila	Kamila	k1gFnSc1	Kamila
Značkovská-Neumannová	Značkovská-Neumannová	k1gFnSc1	Značkovská-Neumannová
-	-	kIx~	-
česká	český	k2eAgFnSc1d1	Česká
překladatelka	překladatelka	k1gFnSc1	překladatelka
Kamila	Kamila	k1gFnSc1	Kamila
Ženatá	ženatý	k2eAgFnSc1d1	Ženatá
-	-	kIx~	-
česká	český	k2eAgFnSc1d1	Česká
výtvarná	výtvarný	k2eAgFnSc1d1	výtvarná
umělkyně	umělkyně	k1gFnSc1	umělkyně
</s>
