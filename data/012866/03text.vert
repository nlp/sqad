<p>
<s>
Papír	papír	k1gInSc1	papír
je	být	k5eAaImIp3nS	být
tenký	tenký	k2eAgInSc4d1	tenký
<g/>
,	,	kIx,	,
hladký	hladký	k2eAgInSc4d1	hladký
materiál	materiál	k1gInSc4	materiál
vyráběný	vyráběný	k2eAgInSc4d1	vyráběný
zhutněním	zhutnění	k1gNnPc3	zhutnění
vlákna	vlákno	k1gNnSc2	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Použitá	použitý	k2eAgNnPc1d1	Použité
vlákna	vlákno	k1gNnPc1	vlákno
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
přírodní	přírodní	k2eAgFnPc1d1	přírodní
a	a	k8xC	a
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
celulóze	celulóza	k1gFnSc6	celulóza
<g/>
.	.	kIx.	.
</s>
<s>
Nejobvyklejší	obvyklý	k2eAgInSc1d3	nejobvyklejší
materiál	materiál	k1gInSc1	materiál
je	být	k5eAaImIp3nS	být
buničina	buničina	k1gFnSc1	buničina
vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
(	(	kIx(	(
<g/>
většinou	většina	k1gFnSc7	většina
smrku	smrk	k1gInSc2	smrk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
ze	z	k7c2	z
sekundárních	sekundární	k2eAgNnPc2d1	sekundární
vláken	vlákno	k1gNnPc2	vlákno
(	(	kIx(	(
<g/>
sběrový	sběrový	k2eAgInSc1d1	sběrový
papír	papír	k1gInSc1	papír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
použity	použít	k5eAaPmNgInP	použít
i	i	k9	i
jiné	jiný	k2eAgInPc1d1	jiný
rostlinné	rostlinný	k2eAgInPc1d1	rostlinný
vláknité	vláknitý	k2eAgInPc1d1	vláknitý
materiály	materiál	k1gInPc1	materiál
jako	jako	k8xC	jako
bavlna	bavlna	k1gFnSc1	bavlna
<g/>
,	,	kIx,	,
a	a	k8xC	a
konopí	konopí	k1gNnSc1	konopí
<g/>
,	,	kIx,	,
vlákna	vlákna	k1gFnSc1	vlákna
bource	bourec	k1gMnSc2	bourec
morušového	morušový	k2eAgMnSc2d1	morušový
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jiné	jiný	k2eAgFnSc2d1	jiná
alternativní	alternativní	k2eAgFnSc2d1	alternativní
suroviny	surovina	k1gFnSc2	surovina
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
(	(	kIx(	(
<g/>
třeba	třeba	k6eAd1	třeba
na	na	k7c4	na
Srí	Srí	k1gFnSc4	Srí
Lance	lance	k1gNnSc2	lance
<g/>
)	)	kIx)	)
ze	z	k7c2	z
sloního	sloní	k2eAgInSc2d1	sloní
trusu	trus	k1gInSc2	trus
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
Teplota	teplota	k1gFnSc1	teplota
vzníceni	vznícen	k2eAgMnPc1d1	vznícen
nad	nad	k7c4	nad
185	[number]	k4	185
°	°	k?	°
<g/>
C	C	kA	C
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hromadě	hromada	k1gFnSc3	hromada
několika	několik	k4yIc2	několik
stovek	stovka	k1gFnPc2	stovka
archů	arch	k1gInPc2	arch
papíru	papír	k1gInSc2	papír
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
rys	rys	k1gInSc1	rys
papíru	papír	k1gInSc2	papír
(	(	kIx(	(
<g/>
480	[number]	k4	480
archů	arch	k1gInPc2	arch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Papírové	papírový	k2eAgInPc1d1	papírový
archy	arch	k1gInPc1	arch
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
chovat	chovat	k5eAaImF	chovat
jako	jako	k9	jako
velmi	velmi	k6eAd1	velmi
ostré	ostrý	k2eAgFnSc2d1	ostrá
žiletky	žiletka	k1gFnSc2	žiletka
a	a	k8xC	a
způsobit	způsobit	k5eAaPmF	způsobit
papírové	papírový	k2eAgNnSc4d1	papírové
říznutí	říznutí	k1gNnSc4	říznutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etymologie	etymologie	k1gFnSc2	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
papíru	papír	k1gInSc2	papír
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
slova	slovo	k1gNnSc2	slovo
papyrus	papyrus	k1gInSc1	papyrus
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnSc4	jeho
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
městem	město	k1gNnSc7	město
Byblos	Byblosa	k1gFnPc2	Byblosa
tak	tak	k9	tak
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
i	i	k9	i
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
knihu	kniha	k1gFnSc4	kniha
(	(	kIx(	(
<g/>
bible	bible	k1gFnSc2	bible
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
užití	užití	k1gNnSc2	užití
papíru	papír	k1gInSc2	papír
==	==	k?	==
</s>
</p>
<p>
<s>
Papír	papír	k1gInSc1	papír
byl	být	k5eAaImAgInS	být
vynalezen	vynaleznout	k5eAaPmNgInS	vynaleznout
někdy	někdy	k6eAd1	někdy
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
vyráběn	vyrábět	k5eAaImNgInS	vyrábět
z	z	k7c2	z
konopí	konopí	k1gNnSc2	konopí
a	a	k8xC	a
až	až	k9	až
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
papír	papír	k1gInSc1	papír
vyrábět	vyrábět	k5eAaImF	vyrábět
z	z	k7c2	z
hedvábných	hedvábný	k2eAgInPc2d1	hedvábný
a	a	k8xC	a
lněných	lněný	k2eAgInPc2d1	lněný
hadrů	hadr	k1gInPc2	hadr
<g/>
.	.	kIx.	.
</s>
<s>
Papír	papír	k1gInSc1	papír
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ho	on	k3xPp3gMnSc4	on
známe	znát	k5eAaImIp1nP	znát
dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vynalezen	vynaleznout	k5eAaPmNgInS	vynaleznout
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
asi	asi	k9	asi
roku	rok	k1gInSc2	rok
105	[number]	k4	105
n.	n.	k?	n.
l.	l.	k?	l.
Do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Arabů	Arab	k1gMnPc2	Arab
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
papírny	papírna	k1gFnPc1	papírna
proto	proto	k8xC	proto
vznikaly	vznikat	k5eAaImAgFnP	vznikat
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
šířily	šířit	k5eAaImAgFnP	šířit
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
Francie	Francie	k1gFnSc2	Francie
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
-	-	kIx~	-
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
začaly	začít	k5eAaPmAgFnP	začít
objevovat	objevovat	k5eAaImF	objevovat
papírny	papírna	k1gFnPc1	papírna
i	i	k8xC	i
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
Zbraslav	Zbraslav	k1gFnSc1	Zbraslav
<g/>
,	,	kIx,	,
Turnov	Turnov	k1gInSc1	Turnov
<g/>
,	,	kIx,	,
Frýdlant	Frýdlant	k1gInSc1	Frýdlant
<g/>
,	,	kIx,	,
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
pražské	pražský	k2eAgNnSc1d1	Pražské
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Papír	papír	k1gInSc1	papír
byl	být	k5eAaImAgInS	být
sice	sice	k8xC	sice
méně	málo	k6eAd2	málo
kvalitní	kvalitní	k2eAgMnSc1d1	kvalitní
než	než	k8xS	než
pergamen	pergamen	k1gInSc1	pergamen
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
byl	být	k5eAaImAgMnS	být
mnohem	mnohem	k6eAd1	mnohem
levnější	levný	k2eAgMnSc1d2	levnější
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
začal	začít	k5eAaPmAgInS	začít
pergamen	pergamen	k1gInSc1	pergamen
vytlačovat	vytlačovat	k5eAaImF	vytlačovat
<g/>
,	,	kIx,	,
až	až	k8xS	až
nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
papír	papír	k1gInSc1	papír
převládl	převládnout	k5eAaPmAgInS	převládnout
(	(	kIx(	(
<g/>
na	na	k7c4	na
pergamen	pergamen	k1gInSc4	pergamen
byly	být	k5eAaImAgFnP	být
psány	psán	k2eAgFnPc1d1	psána
jen	jen	k6eAd1	jen
významné	významný	k2eAgFnPc1d1	významná
listiny	listina	k1gFnPc1	listina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Ruční	ruční	k2eAgInSc4d1	ruční
papír	papír	k1gInSc4	papír
===	===	k?	===
</s>
</p>
<p>
<s>
Papír	papír	k1gInSc1	papír
byl	být	k5eAaImAgInS	být
vyráběn	vyrábět	k5eAaImNgInS	vyrábět
původně	původně	k6eAd1	původně
ze	z	k7c2	z
lněného	lněný	k2eAgInSc2d1	lněný
nebo	nebo	k8xC	nebo
bavlněného	bavlněný	k2eAgInSc2d1	bavlněný
odpadu	odpad	k1gInSc2	odpad
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
vzrůstající	vzrůstající	k2eAgFnSc7d1	vzrůstající
potřebou	potřeba	k1gFnSc7	potřeba
papíru	papír	k1gInSc2	papír
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
potřeba	potřeba	k6eAd1	potřeba
hledat	hledat	k5eAaImF	hledat
nové	nový	k2eAgFnPc4d1	nová
technologie	technologie	k1gFnPc4	technologie
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
využívání	využívání	k1gNnSc3	využívání
pilin	pilina	k1gFnPc2	pilina
<g/>
,	,	kIx,	,
slámy	sláma	k1gFnSc2	sláma
a	a	k8xC	a
starého	starý	k2eAgInSc2d1	starý
papíru	papír	k1gInSc2	papír
(	(	kIx(	(
<g/>
jeho	on	k3xPp3gNnSc2	on
využívání	využívání	k1gNnSc2	využívání
vedlo	vést	k5eAaImAgNnS	vést
mnohdy	mnohdy	k6eAd1	mnohdy
k	k	k7c3	k
ničení	ničení	k1gNnSc3	ničení
archiválií	archiválie	k1gFnPc2	archiválie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výroba	výroba	k1gFnSc1	výroba
<g/>
:	:	kIx,	:
nabírání	nabírání	k1gNnSc1	nabírání
papíroviny	papírovina	k1gFnSc2	papírovina
z	z	k7c2	z
kádě	káď	k1gFnSc2	káď
na	na	k7c4	na
čerpací	čerpací	k2eAgFnSc4d1	čerpací
formu	forma	k1gFnSc4	forma
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc4d1	původní
čerpací	čerpací	k2eAgFnSc4d1	čerpací
formu	forma	k1gFnSc4	forma
tvořil	tvořit	k5eAaImAgInS	tvořit
laťový	laťový	k2eAgInSc1d1	laťový
rám	rám	k1gInSc1	rám
vyztužený	vyztužený	k2eAgInSc1d1	vyztužený
žebry	žebr	k1gInPc4	žebr
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yRgInPc6	který
bylo	být	k5eAaImAgNnS	být
upevněno	upevnit	k5eAaPmNgNnS	upevnit
síto	síto	k1gNnSc1	síto
<g/>
.	.	kIx.	.
</s>
<s>
Forma	forma	k1gFnSc1	forma
měla	mít	k5eAaImAgFnS	mít
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
-	-	kIx~	-
síto	síto	k1gNnSc1	síto
a	a	k8xC	a
snímatelný	snímatelný	k2eAgInSc1d1	snímatelný
rám	rám	k1gInSc1	rám
-	-	kIx~	-
jeho	jeho	k3xOp3gFnSc7	jeho
výškou	výška	k1gFnSc7	výška
se	se	k3xPyFc4	se
řídila	řídit	k5eAaImAgFnS	řídit
vrstva	vrstva	k1gFnSc1	vrstva
papíroviny	papírovina	k1gFnSc2	papírovina
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
gramáž	gramáž	k1gFnSc4	gramáž
papíru	papír	k1gInSc2	papír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Formy	forma	k1gFnPc1	forma
si	se	k3xPyFc3	se
mohl	moct	k5eAaImAgMnS	moct
papírník	papírník	k1gMnSc1	papírník
vyrábět	vyrábět	k5eAaImF	vyrábět
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyráběli	vyrábět	k5eAaImAgMnP	vyrábět
je	on	k3xPp3gNnPc4	on
také	také	k9	také
formaři	formař	k1gMnPc7	formař
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
ručním	ruční	k2eAgInSc6d1	ruční
papíru	papír	k1gInSc6	papír
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
stopy	stopa	k1gFnPc1	stopa
síta	síto	k1gNnSc2	síto
(	(	kIx(	(
<g/>
žebrování	žebrování	k1gNnSc1	žebrování
<g/>
)	)	kIx)	)
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
vergé	vergá	k1gFnSc3	vergá
[	[	kIx(	[
<g/>
veržé	veržé	k6eAd1	veržé
<g/>
]	]	kIx)	]
a	a	k8xC	a
vodotisky	vodotisk	k1gInPc4	vodotisk
<g/>
.	.	kIx.	.
</s>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
papíry	papír	k1gInPc7	papír
bez	bez	k7c2	bez
stop	stop	k2eAgNnSc2d1	stop
síta	síto	k1gNnSc2	síto
na	na	k7c6	na
papíru	papír	k1gInSc6	papír
a	a	k8xC	a
bez	bez	k7c2	bez
stop	stop	k2eAgNnSc2d1	stop
síta	síto	k1gNnSc2	síto
v	v	k7c6	v
průsvitu	průsvit	k1gInSc6	průsvit
-	-	kIx~	-
síto	síto	k1gNnSc1	síto
bývalo	bývat	k5eAaImAgNnS	bývat
velice	velice	k6eAd1	velice
husté	hustý	k2eAgNnSc1d1	husté
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
papír	papír	k1gInSc1	papír
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
velin	velin	k1gInSc1	velin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Moderní	moderní	k2eAgFnSc1d1	moderní
výroba	výroba	k1gFnSc1	výroba
===	===	k?	===
</s>
</p>
<p>
<s>
Moderní	moderní	k2eAgInSc1d1	moderní
výrobní	výrobní	k2eAgInSc1d1	výrobní
proces	proces	k1gInSc1	proces
papíru	papír	k1gInSc2	papír
na	na	k7c6	na
papírenském	papírenský	k2eAgInSc6d1	papírenský
stroji	stroj	k1gInSc6	stroj
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byl	být	k5eAaImAgInS	být
vynalezen	vynaleznout	k5eAaPmNgInS	vynaleznout
panem	pan	k1gMnSc7	pan
Fourdrinierem	Fourdrinier	k1gMnSc7	Fourdrinier
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
tyto	tento	k3xDgFnPc4	tento
fáze	fáze	k1gFnPc4	fáze
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
====	====	k?	====
Výroba	výroba	k1gFnSc1	výroba
vláknoviny	vláknovina	k1gFnSc2	vláknovina
-	-	kIx~	-
buničiny	buničina	k1gFnSc2	buničina
====	====	k?	====
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
vstupního	vstupní	k2eAgInSc2d1	vstupní
materiálu	materiál	k1gInSc2	materiál
(	(	kIx(	(
<g/>
dřevěné	dřevěný	k2eAgFnPc1d1	dřevěná
štěpky	štěpka	k1gFnPc1	štěpka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejprve	nejprve	k6eAd1	nejprve
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
buničina	buničina	k1gFnSc1	buničina
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
dřevovina	dřevovina	k1gFnSc1	dřevovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Příprava	příprava	k1gFnSc1	příprava
====	====	k?	====
</s>
</p>
<p>
<s>
Získaná	získaný	k2eAgFnSc1d1	získaná
vláknovina	vláknovina	k1gFnSc1	vláknovina
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
upravuje	upravovat	k5eAaImIp3nS	upravovat
podle	podle	k7c2	podle
typu	typ	k1gInSc2	typ
vyráběného	vyráběný	k2eAgInSc2d1	vyráběný
papíru	papír	k1gInSc2	papír
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
mechanicky	mechanicky	k6eAd1	mechanicky
a	a	k8xC	a
chemicky	chemicky	k6eAd1	chemicky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mechanické	mechanický	k2eAgFnPc1d1	mechanická
úpravy	úprava	k1gFnPc1	úprava
-	-	kIx~	-
mletí	mletí	k1gNnSc1	mletí
<g/>
.	.	kIx.	.
</s>
<s>
Vláknina	vláknina	k1gFnSc1	vláknina
se	se	k3xPyFc4	se
ve	v	k7c6	v
vodní	vodní	k2eAgFnSc6d1	vodní
suspenzi	suspenze	k1gFnSc6	suspenze
mele	mlít	k5eAaImIp3nS	mlít
kontinuálně	kontinuálně	k6eAd1	kontinuálně
v	v	k7c6	v
diskových	diskový	k2eAgInPc6d1	diskový
mlýnech	mlýn	k1gInPc6	mlýn
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
měkké	měkký	k2eAgInPc4d1	měkký
savé	savý	k2eAgInPc4d1	savý
papíry	papír	k1gInPc4	papír
se	se	k3xPyFc4	se
vláknina	vláknina	k1gFnSc1	vláknina
mele	mlít	k5eAaImIp3nS	mlít
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pro	pro	k7c4	pro
tukotěsné	tukotěsný	k2eAgInPc4d1	tukotěsný
papíry	papír	k1gInPc4	papír
(	(	kIx(	(
<g/>
pergamenová	pergamenový	k2eAgFnSc1d1	pergamenová
náhrada	náhrada	k1gFnSc1	náhrada
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stupeň	stupeň	k1gInSc4	stupeň
mletí	mletí	k1gNnSc2	mletí
velmi	velmi	k6eAd1	velmi
vysoký	vysoký	k2eAgMnSc1d1	vysoký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chemické	chemický	k2eAgFnPc4d1	chemická
úpravy	úprava	k1gFnPc4	úprava
-	-	kIx~	-
do	do	k7c2	do
vlákniny	vláknina	k1gFnSc2	vláknina
se	se	k3xPyFc4	se
přidává	přidávat	k5eAaImIp3nS	přidávat
kationický	kationický	k2eAgInSc1d1	kationický
škrob	škrob	k1gInSc1	škrob
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
pevností	pevnost	k1gFnSc7	pevnost
papíru	papír	k1gInSc2	papír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
barvy	barva	k1gFnPc1	barva
<g/>
,	,	kIx,	,
klížidlo	klížidlo	k1gNnSc1	klížidlo
(	(	kIx(	(
<g/>
proti	proti	k7c3	proti
rozpíjení	rozpíjení	k1gNnSc3	rozpíjení
tiskových	tiskový	k2eAgFnPc2d1	tisková
barev	barva	k1gFnPc2	barva
nebo	nebo	k8xC	nebo
inkoustu	inkoust	k1gInSc2	inkoust
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plniva	plnivo	k1gNnPc1	plnivo
-	-	kIx~	-
kaolin	kaolin	k1gInSc1	kaolin
nebo	nebo	k8xC	nebo
uhličitan	uhličitan	k1gInSc1	uhličitan
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
(	(	kIx(	(
<g/>
aby	aby	kYmCp3nS	aby
nebyl	být	k5eNaImAgMnS	být
papír	papír	k1gInSc4	papír
průsvitný	průsvitný	k2eAgInSc4d1	průsvitný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
retenční	retenční	k2eAgInPc1d1	retenční
prostředky	prostředek	k1gInPc1	prostředek
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
výtěžnosti	výtěžnost	k1gFnSc2	výtěžnost
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
chemické	chemický	k2eAgInPc4d1	chemický
prostředky	prostředek	k1gInPc4	prostředek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Papírenský	papírenský	k2eAgInSc1d1	papírenský
stroj	stroj	k1gInSc1	stroj
====	====	k?	====
</s>
</p>
<p>
<s>
Mokrá	mokrý	k2eAgFnSc1d1	mokrá
část	část	k1gFnSc1	část
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vláknitá	vláknitý	k2eAgFnSc1d1	vláknitá
suspenze	suspenze	k1gFnSc1	suspenze
natéká	natékat	k5eAaImIp3nS	natékat
na	na	k7c4	na
podélné	podélný	k2eAgNnSc4d1	podélné
nekonečné	konečný	k2eNgNnSc4d1	nekonečné
síto	síto	k1gNnSc4	síto
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgNnSc6	jenž
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
odvodnění	odvodnění	k1gNnSc3	odvodnění
vlákniny	vláknina	k1gFnSc2	vláknina
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlákna	vlákno	k1gNnPc1	vlákno
se	se	k3xPyFc4	se
usazují	usazovat	k5eAaImIp3nP	usazovat
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
síta	síto	k1gNnSc2	síto
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
protéká	protékat	k5eAaImIp3nS	protékat
do	do	k7c2	do
sběrné	sběrný	k2eAgFnSc2d1	sběrná
vany	vana	k1gFnSc2	vana
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
voda	voda	k1gFnSc1	voda
ze	z	k7c2	z
spodní	spodní	k2eAgFnSc2d1	spodní
strany	strana	k1gFnSc2	strana
síta	síto	k1gNnSc2	síto
odsát	odsát	k5eAaPmNgInS	odsát
sacími	sací	k2eAgFnPc7d1	sací
skříněmi	skříň	k1gFnPc7	skříň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lisová	Lisový	k2eAgFnSc1d1	Lisový
část	část	k1gFnSc1	část
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Papírový	papírový	k2eAgInSc1d1	papírový
list	list	k1gInSc1	list
se	se	k3xPyFc4	se
snímá	snímat	k5eAaImIp3nS	snímat
ze	z	k7c2	z
síta	síto	k1gNnSc2	síto
pomocí	pomocí	k7c2	pomocí
plstěnce	plstěnec	k1gInSc2	plstěnec
do	do	k7c2	do
lisové	lisové	k2eAgFnSc2d1	lisové
části	část	k1gFnSc2	část
(	(	kIx(	(
<g/>
několik	několik	k4yIc4	několik
válcových	válcový	k2eAgInPc2d1	válcový
lisů	lis	k1gInPc2	lis
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
lisováním	lisování	k1gNnSc7	lisování
odstraní	odstranit	k5eAaPmIp3nS	odstranit
další	další	k2eAgFnSc1d1	další
voda	voda	k1gFnSc1	voda
z	z	k7c2	z
papírového	papírový	k2eAgInSc2d1	papírový
listu	list	k1gInSc2	list
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sušící	sušící	k2eAgFnSc1d1	sušící
část	část	k1gFnSc1	část
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
mechanicky	mechanicky	k6eAd1	mechanicky
odstranit	odstranit	k5eAaPmF	odstranit
zbývající	zbývající	k2eAgFnSc4d1	zbývající
vodu	voda	k1gFnSc4	voda
v	v	k7c6	v
papírovém	papírový	k2eAgInSc6d1	papírový
listu	list	k1gInSc6	list
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
papírový	papírový	k2eAgInSc1d1	papírový
list	list	k1gInSc1	list
sušit	sušit	k5eAaImF	sušit
na	na	k7c6	na
válcích	válec	k1gInPc6	válec
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
vytápěny	vytápět	k5eAaImNgInP	vytápět
párou	pára	k1gFnSc7	pára
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středu	střed	k1gInSc6	střed
sušící	sušící	k2eAgFnSc2d1	sušící
části	část	k1gFnSc2	část
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
umístěno	umístěn	k2eAgNnSc4d1	umístěno
natírací	natírací	k2eAgNnSc4d1	natírací
zařízení	zařízení	k1gNnSc4	zařízení
pro	pro	k7c4	pro
povrchové	povrchový	k2eAgFnPc4d1	povrchová
úpravy	úprava	k1gFnPc4	úprava
papíru	papír	k1gInSc2	papír
(	(	kIx(	(
<g/>
povrchové	povrchový	k2eAgNnSc1d1	povrchové
klížení	klížení	k1gNnSc1	klížení
nebo	nebo	k8xC	nebo
natíraní	natíraný	k2eAgMnPc1d1	natíraný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
papírenského	papírenský	k2eAgInSc2d1	papírenský
stroje	stroj	k1gInSc2	stroj
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
kalandr	kalandr	k1gInSc4	kalandr
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
lis	lis	k1gInSc1	lis
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
papír	papír	k1gInSc1	papír
povrchově	povrchově	k6eAd1	povrchově
uhlazuje	uhlazovat	k5eAaImIp3nS	uhlazovat
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
následuje	následovat	k5eAaImIp3nS	následovat
navíječ	navíječ	k1gInSc1	navíječ
<g/>
,	,	kIx,	,
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
se	se	k3xPyFc4	se
papírový	papírový	k2eAgInSc1d1	papírový
list	list	k1gInSc1	list
navíjí	navíjet	k5eAaImIp3nS	navíjet
do	do	k7c2	do
rolí	role	k1gFnPc2	role
nebo	nebo	k8xC	nebo
stůl	stůl	k1gInSc4	stůl
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
papír	papír	k1gInSc1	papír
řeže	řezat	k5eAaImIp3nS	řezat
na	na	k7c4	na
formáty	formát	k1gInPc4	formát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Formování	formování	k1gNnPc4	formování
do	do	k7c2	do
archů	arch	k1gInPc2	arch
====	====	k?	====
</s>
</p>
<p>
<s>
Směs	směs	k1gFnSc1	směs
celulózy	celulóza	k1gFnSc2	celulóza
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
ředí	ředit	k5eAaImIp3nP	ředit
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
jemný	jemný	k2eAgInSc1d1	jemný
neusazený	usazený	k2eNgInSc1d1	neusazený
kal	kal	k1gInSc1	kal
(	(	kIx(	(
<g/>
suspenze	suspenze	k1gFnSc1	suspenze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
zředěná	zředěný	k2eAgFnSc1d1	zředěná
suspenze	suspenze	k1gFnSc1	suspenze
se	se	k3xPyFc4	se
cedí	cedit	k5eAaImIp3nS	cedit
přes	přes	k7c4	přes
jemné	jemný	k2eAgNnSc4d1	jemné
<g/>
,	,	kIx,	,
pohybující	pohybující	k2eAgNnSc4d1	pohybující
se	se	k3xPyFc4	se
síto	síto	k1gNnSc4	síto
<g/>
,	,	kIx,	,
v	v	k7c4	v
nekonečný	konečný	k2eNgInSc4d1	nekonečný
pás	pás	k1gInSc4	pás
papíroviny	papírovina	k1gFnSc2	papírovina
<g/>
.	.	kIx.	.
</s>
<s>
Vodotisk	vodotisk	k1gInSc1	vodotisk
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vložen	vložit	k5eAaPmNgInS	vložit
do	do	k7c2	do
papíru	papír	k1gInSc2	papír
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
procesu	proces	k1gInSc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pohybující	pohybující	k2eAgMnSc1d1	pohybující
se	se	k3xPyFc4	se
pás	pás	k1gInSc1	pás
je	být	k5eAaImIp3nS	být
stlačen	stlačit	k5eAaPmNgInS	stlačit
a	a	k8xC	a
sušen	sušit	k5eAaImNgInS	sušit
do	do	k7c2	do
spojitého	spojitý	k2eAgInSc2d1	spojitý
pásu	pás	k1gInSc2	pás
papíru	papír	k1gInSc2	papír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
formového	formový	k2eAgInSc2d1	formový
procesu	proces	k1gInSc2	proces
<g/>
,	,	kIx,	,
množství	množství	k1gNnSc1	množství
celulózy	celulóza	k1gFnSc2	celulóza
je	být	k5eAaImIp3nS	být
nabráno	nabrán	k2eAgNnSc1d1	nabráno
do	do	k7c2	do
formy	forma	k1gFnSc2	forma
s	s	k7c7	s
drátově-sítovým	drátověítův	k2eAgInSc7d1	drátově-sítův
základem	základ	k1gInSc7	základ
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
jiným	jiný	k2eAgNnSc7d1	jiné
cedicím	cedicí	k2eAgNnSc7d1	cedicí
zařízením	zařízení	k1gNnSc7	zařízení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
vlákna	vlákno	k1gNnPc1	vlákno
jsou	být	k5eAaImIp3nP	být
položená	položený	k2eAgNnPc1d1	položené
na	na	k7c6	na
sítu	síto	k1gNnSc6	síto
<g/>
,	,	kIx,	,
a	a	k8xC	a
nadbytečná	nadbytečný	k2eAgFnSc1d1	nadbytečná
voda	voda	k1gFnSc1	voda
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
odceděna	odceděn	k2eAgNnPc1d1	odceděn
mimo	mimo	k7c4	mimo
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
tlak	tlak	k1gInSc4	tlak
na	na	k7c4	na
odstranění	odstranění	k1gNnSc4	odstranění
přebytečné	přebytečný	k2eAgFnSc2d1	přebytečná
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Papír	papír	k1gInSc1	papír
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
odebrán	odebrat	k5eAaPmNgInS	odebrat
z	z	k7c2	z
formy	forma	k1gFnSc2	forma
<g/>
,	,	kIx,	,
vlhký	vlhký	k2eAgInSc4d1	vlhký
nebo	nebo	k8xC	nebo
suchý	suchý	k2eAgInSc4d1	suchý
<g/>
,	,	kIx,	,
a	a	k8xC	a
lze	lze	k6eAd1	lze
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
dalším	další	k2eAgNnSc6d1	další
zpracování	zpracování	k1gNnSc6	zpracování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
hromadně	hromadně	k6eAd1	hromadně
vyráběného	vyráběný	k2eAgInSc2d1	vyráběný
papíru	papír	k1gInSc2	papír
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
použitím	použití	k1gNnSc7	použití
spojitého	spojitý	k2eAgInSc2d1	spojitý
(	(	kIx(	(
<g/>
Fourdrinierova	Fourdrinierův	k2eAgInSc2d1	Fourdrinierův
<g/>
)	)	kIx)	)
procesu	proces	k1gInSc2	proces
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
listu	list	k1gInSc2	list
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vysušení	vysušení	k1gNnSc6	vysušení
tento	tento	k3xDgInSc4	tento
spojitý	spojitý	k2eAgInSc4d1	spojitý
list	list	k1gInSc4	list
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nařezán	nařezat	k5eAaPmNgMnS	nařezat
podélně	podélně	k6eAd1	podélně
a	a	k8xC	a
příčně	příčně	k6eAd1	příčně
na	na	k7c4	na
požadované	požadovaný	k2eAgInPc4d1	požadovaný
rozměry	rozměr	k1gInPc4	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Standardní	standardní	k2eAgInPc1d1	standardní
rozměry	rozměr	k1gInPc1	rozměr
archů	arch	k1gInPc2	arch
jsou	být	k5eAaImIp3nP	být
předepsané	předepsaný	k2eAgFnPc4d1	předepsaná
regulačními	regulační	k2eAgInPc7d1	regulační
orgány	orgán	k1gInPc7	orgán
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
organizace	organizace	k1gFnSc2	organizace
pro	pro	k7c4	pro
standardizaci	standardizace	k1gFnSc4	standardizace
(	(	kIx(	(
<g/>
International	International	k1gFnSc1	International
Organization	Organization	k1gInSc1	Organization
for	forum	k1gNnPc2	forum
Standardization	Standardization	k1gInSc1	Standardization
-	-	kIx~	-
ISO	ISO	kA	ISO
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Další	další	k2eAgFnPc1d1	další
přísady	přísada	k1gFnPc1	přísada
====	====	k?	====
</s>
</p>
<p>
<s>
Neupravený	upravený	k2eNgInSc1d1	neupravený
papír	papír	k1gInSc1	papír
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jen	jen	k9	jen
stlačenou	stlačený	k2eAgFnSc4d1	stlačená
a	a	k8xC	a
usušenou	usušený	k2eAgFnSc4d1	usušená
celulózu	celulóza	k1gFnSc4	celulóza
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
savý	savý	k2eAgMnSc1d1	savý
(	(	kIx(	(
<g/>
např.	např.	kA	např.
savý	savý	k2eAgInSc1d1	savý
papír	papír	k1gInSc1	papír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
neposkytuje	poskytovat	k5eNaImIp3nS	poskytovat
dobrý	dobrý	k2eAgInSc4d1	dobrý
povrch	povrch	k1gInSc4	povrch
pro	pro	k7c4	pro
psaní	psaní	k1gNnSc4	psaní
nebo	nebo	k8xC	nebo
tisk	tisk	k1gInSc4	tisk
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
papíru	papír	k1gInSc6	papír
použito	použit	k2eAgNnSc4d1	použito
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
přísad	přísada	k1gFnPc2	přísada
na	na	k7c6	na
dodání	dodání	k1gNnSc6	dodání
požadovaných	požadovaný	k2eAgFnPc2d1	požadovaná
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
jsou	být	k5eAaImIp3nP	být
použity	použít	k5eAaPmNgInP	použít
v	v	k7c6	v
povrchové	povrchový	k2eAgFnSc6d1	povrchová
vrstvě	vrstva	k1gFnSc6	vrstva
zvané	zvaný	k2eAgFnPc1d1	zvaná
apretura	apretura	k1gFnSc1	apretura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přísady	přísada	k1gFnPc1	přísada
apretury	apretura	k1gFnSc2	apretura
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
polymery	polymer	k1gInPc1	polymer
navrženy	navržen	k2eAgInPc1d1	navržen
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
poskytovaly	poskytovat	k5eAaImAgFnP	poskytovat
lepší	dobrý	k2eAgInSc4d2	lepší
povrch	povrch	k1gInSc4	povrch
pro	pro	k7c4	pro
tisk	tisk	k1gInSc4	tisk
<g/>
.	.	kIx.	.
</s>
<s>
Škroby	škrob	k1gInPc1	škrob
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
polyvinylacetát	polyvinylacetát	k1gInSc1	polyvinylacetát
(	(	kIx(	(
<g/>
polyvinyl	polyvinyl	k1gInSc1	polyvinyl
acetate	acetat	k1gInSc5	acetat
-	-	kIx~	-
PVA	PVA	kA	PVA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
používané	používaný	k2eAgInPc1d1	používaný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kolik	kolik	k4yQc4	kolik
je	být	k5eAaImIp3nS	být
typů	typ	k1gInPc2	typ
papíru	papír	k1gInSc2	papír
<g/>
,	,	kIx,	,
tolik	tolik	k4yIc4	tolik
typů	typ	k1gInPc2	typ
apretur	apretura	k1gFnPc2	apretura
je	být	k5eAaImIp3nS	být
použito	použít	k5eAaPmNgNnS	použít
<g/>
.	.	kIx.	.
</s>
<s>
Povrchové	povrchový	k2eAgFnPc1d1	povrchová
vrstvy	vrstva	k1gFnPc1	vrstva
též	též	k9	též
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
hlazeny	hladit	k5eAaImNgInP	hladit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
na	na	k7c4	na
papír	papír	k1gInSc4	papír
lépe	dobře	k6eAd2	dobře
psalo	psát	k5eAaImAgNnS	psát
<g/>
.	.	kIx.	.
</s>
<s>
Struktura	struktura	k1gFnSc1	struktura
neupraveného	upravený	k2eNgInSc2d1	neupravený
papíru	papír	k1gInSc2	papír
je	být	k5eAaImIp3nS	být
drsná	drsný	k2eAgFnSc1d1	drsná
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
dosažení	dosažení	k1gNnSc4	dosažení
větší	veliký	k2eAgFnSc2d2	veliký
hladkosti	hladkost	k1gFnSc2	hladkost
používají	používat	k5eAaImIp3nP	používat
povrchové	povrchový	k2eAgFnPc1d1	povrchová
vrstvy	vrstva	k1gFnPc1	vrstva
sestávající	sestávající	k2eAgFnPc1d1	sestávající
z	z	k7c2	z
latexu	latex	k1gInSc2	latex
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgNnPc2d1	jiné
pojiv	pojivo	k1gNnPc2	pojivo
a	a	k8xC	a
plnidel	plnidlo	k1gNnPc2	plnidlo
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
kaolin	kaolin	k1gInSc4	kaolin
nebo	nebo	k8xC	nebo
uhličitan	uhličitan	k1gInSc4	uhličitan
vápenatý	vápenatý	k2eAgInSc4d1	vápenatý
<g/>
.	.	kIx.	.
</s>
<s>
Lesklé	lesklý	k2eAgInPc4d1	lesklý
<g/>
,	,	kIx,	,
hedvábné	hedvábný	k2eAgInPc4d1	hedvábný
nebo	nebo	k8xC	nebo
kamínkové	kamínkový	k2eAgInPc4d1	kamínkový
papíry	papír	k1gInPc4	papír
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
časopisecký	časopisecký	k2eAgInSc1d1	časopisecký
papír	papír	k1gInSc1	papír
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
vnitřní	vnitřní	k2eAgFnPc4d1	vnitřní
stránky	stránka	k1gFnPc4	stránka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Lesklého	lesklý	k2eAgInSc2d1	lesklý
efektu	efekt	k1gInSc2	efekt
(	(	kIx(	(
<g/>
např.	např.	kA	např.
na	na	k7c6	na
obálkách	obálka	k1gFnPc6	obálka
módních	módní	k2eAgInPc2d1	módní
časopisů	časopis	k1gInPc2	časopis
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
na	na	k7c6	na
konci	konec	k1gInSc6	konec
tiskového	tiskový	k2eAgInSc2d1	tiskový
procesu	proces	k1gInSc2	proces
<g/>
,	,	kIx,	,
lakováním	lakování	k1gNnSc7	lakování
nebo	nebo	k8xC	nebo
laminováním	laminování	k1gNnSc7	laminování
<g/>
,	,	kIx,	,
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
tedy	tedy	k9	tedy
vlastností	vlastnost	k1gFnSc7	vlastnost
papíru	papír	k1gInSc2	papír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
přísady	přísada	k1gFnPc1	přísada
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
rozšíření	rozšíření	k1gNnSc4	rozšíření
různých	různý	k2eAgFnPc2d1	různá
vlastností	vlastnost	k1gFnPc2	vlastnost
papíru	papír	k1gInSc2	papír
<g/>
,	,	kIx,	,
nejčastějšími	častý	k2eAgInPc7d3	nejčastější
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
jsou	být	k5eAaImIp3nP	být
optické	optický	k2eAgInPc1d1	optický
zjasňovače	zjasňovač	k1gInPc1	zjasňovač
(	(	kIx(	(
<g/>
OZP	OZP	kA	OZP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
dodávají	dodávat	k5eAaImIp3nP	dodávat
papíru	papír	k1gInSc2	papír
modrý	modrý	k2eAgInSc1d1	modrý
odstín	odstín	k1gInSc1	odstín
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
přísadami	přísada	k1gFnPc7	přísada
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
plnidla	plnidlo	k1gNnPc4	plnidlo
<g/>
,	,	kIx,	,
klížidla	klížidlo	k1gNnPc4	klížidlo
<g/>
,	,	kIx,	,
retenční	retenční	k2eAgInPc4d1	retenční
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
<g/>
odpěňovače	odpěňovač	k1gInPc4	odpěňovač
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
dodávají	dodávat	k5eAaImIp3nP	dodávat
papíru	papír	k1gInSc3	papír
vzhled	vzhled	k1gInSc4	vzhled
a	a	k8xC	a
požadované	požadovaný	k2eAgFnPc4d1	požadovaná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Granitový	granitový	k2eAgInSc1d1	granitový
papír	papír	k1gInSc1	papír
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
papírové	papírový	k2eAgFnSc2d1	papírová
suroviny	surovina	k1gFnSc2	surovina
obsahující	obsahující	k2eAgFnSc2d1	obsahující
velmi	velmi	k6eAd1	velmi
jemná	jemný	k2eAgNnPc1d1	jemné
obarvená	obarvený	k2eAgNnPc1d1	obarvené
vlákna	vlákno	k1gNnPc1	vlákno
buď	buď	k8xC	buď
z	z	k7c2	z
látky	látka	k1gFnSc2	látka
nebo	nebo	k8xC	nebo
z	z	k7c2	z
papíru	papír	k1gInSc2	papír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Sušení	sušení	k1gNnSc2	sušení
====	====	k?	====
</s>
</p>
<p>
<s>
Papír	papír	k1gInSc1	papír
se	se	k3xPyFc4	se
suší	sušit	k5eAaImIp3nS	sušit
i	i	k9	i
několikrát	několikrát	k6eAd1	několikrát
během	během	k7c2	během
výroby	výroba	k1gFnSc2	výroba
(	(	kIx(	(
<g/>
suchý	suchý	k2eAgInSc1d1	suchý
papír	papír	k1gInSc1	papír
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
pevnější	pevný	k2eAgMnSc1d2	pevnější
než	než	k8xS	než
vlhký	vlhký	k2eAgMnSc1d1	vlhký
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
lepší	dobrý	k2eAgNnSc1d2	lepší
papír	papír	k1gInSc4	papír
usušit	usušit	k5eAaPmF	usušit
a	a	k8xC	a
předejít	předejít	k5eAaPmF	předejít
tak	tak	k9	tak
jeho	jeho	k3xOp3gNnSc4	jeho
protržení	protržení	k1gNnSc4	protržení
a	a	k8xC	a
zastavení	zastavení	k1gNnSc4	zastavení
produkční	produkční	k2eAgFnSc2d1	produkční
linky	linka	k1gFnSc2	linka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Recyklace	recyklace	k1gFnSc2	recyklace
papíru	papír	k1gInSc2	papír
==	==	k?	==
</s>
</p>
<p>
<s>
Papír	papír	k1gInSc1	papír
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
recyklovatelné	recyklovatelný	k2eAgInPc4d1	recyklovatelný
materiály	materiál	k1gInPc4	materiál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
třídění	třídění	k1gNnSc4	třídění
používá	používat	k5eAaImIp3nS	používat
modrý	modrý	k2eAgInSc1d1	modrý
kontejner	kontejner	k1gInSc1	kontejner
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
označení	označení	k1gNnSc6	označení
papírových	papírový	k2eAgInPc2d1	papírový
obalů	obal	k1gInPc2	obal
vhodných	vhodný	k2eAgFnPc2d1	vhodná
k	k	k7c3	k
recyklaci	recyklace	k1gFnSc3	recyklace
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
standardní	standardní	k2eAgInSc1d1	standardní
recyklační	recyklační	k2eAgInSc1d1	recyklační
symbol	symbol	k1gInSc1	symbol
s	s	k7c7	s
číslem	číslo	k1gNnSc7	číslo
uvnitř	uvnitř	k6eAd1	uvnitř
nebo	nebo	k8xC	nebo
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
PAP	PAP	kA	PAP
podle	podle	k7c2	podle
normy	norma	k1gFnSc2	norma
ČSN	ČSN	kA	ČSN
77	[number]	k4	77
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
52	[number]	k4	52
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vlnitou	vlnitý	k2eAgFnSc4d1	vlnitá
lepenku	lepenka	k1gFnSc4	lepenka
(	(	kIx(	(
<g/>
číslo	číslo	k1gNnSc4	číslo
20	[number]	k4	20
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hladkou	hladký	k2eAgFnSc4d1	hladká
lepenku	lepenka	k1gFnSc4	lepenka
(	(	kIx(	(
<g/>
číslo	číslo	k1gNnSc4	číslo
21	[number]	k4	21
<g/>
)	)	kIx)	)
a	a	k8xC	a
papír	papír	k1gInSc1	papír
(	(	kIx(	(
<g/>
číslo	číslo	k1gNnSc1	číslo
22	[number]	k4	22
<g/>
-	-	kIx~	-
<g/>
39	[number]	k4	39
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Papíry	papír	k1gInPc1	papír
vhodné	vhodný	k2eAgInPc1d1	vhodný
k	k	k7c3	k
recyklaci	recyklace	k1gFnSc3	recyklace
jsou	být	k5eAaImIp3nP	být
kancelářský	kancelářský	k2eAgInSc1d1	kancelářský
papír	papír	k1gInSc1	papír
<g/>
,	,	kIx,	,
sešity	sešit	k1gInPc1	sešit
<g/>
,	,	kIx,	,
noviny	novina	k1gFnPc1	novina
<g/>
,	,	kIx,	,
časopisy	časopis	k1gInPc1	časopis
<g/>
,	,	kIx,	,
reklamní	reklamní	k2eAgInPc1d1	reklamní
letáky	leták	k1gInPc1	leták
<g/>
,	,	kIx,	,
krabice	krabice	k1gFnPc1	krabice
<g/>
,	,	kIx,	,
kartony	karton	k1gInPc1	karton
a	a	k8xC	a
lepenka	lepenka	k1gFnSc1	lepenka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Naopak	naopak	k6eAd1	naopak
nevhodné	vhodný	k2eNgNnSc1d1	nevhodné
k	k	k7c3	k
recyklaci	recyklace	k1gFnSc3	recyklace
jsou	být	k5eAaImIp3nP	být
mokré	mokrý	k2eAgInPc1d1	mokrý
<g/>
,	,	kIx,	,
mastné	mastný	k2eAgInPc1d1	mastný
a	a	k8xC	a
znečištěné	znečištěný	k2eAgInPc1d1	znečištěný
papíry	papír	k1gInPc1	papír
<g/>
,	,	kIx,	,
voskový	voskový	k2eAgInSc1d1	voskový
a	a	k8xC	a
uhlový	uhlový	k2eAgInSc1d1	uhlový
papír	papír	k1gInSc1	papír
(	(	kIx(	(
<g/>
kopírák	kopírák	k1gInSc1	kopírák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
termopapír	termopapír	k1gInSc1	termopapír
<g/>
,	,	kIx,	,
použité	použitý	k2eAgInPc1d1	použitý
papírové	papírový	k2eAgInPc1d1	papírový
kapesníky	kapesník	k1gInPc1	kapesník
<g/>
,	,	kIx,	,
hygienické	hygienický	k2eAgFnPc1d1	hygienická
vložky	vložka	k1gFnPc1	vložka
<g/>
,	,	kIx,	,
obvazy	obvaz	k1gInPc1	obvaz
<g/>
,	,	kIx,	,
obaly	obal	k1gInPc1	obal
ze	z	k7c2	z
směsi	směs	k1gFnSc2	směs
papíru	papír	k1gInSc2	papír
a	a	k8xC	a
jiného	jiný	k2eAgInSc2d1	jiný
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
např.	např.	kA	např.
obaly	obal	k1gInPc1	obal
tetra-pak	tetraak	k6eAd1	tetra-pak
obsahující	obsahující	k2eAgMnSc1d1	obsahující
také	také	k9	také
hliníkovou	hliníkový	k2eAgFnSc4d1	hliníková
a	a	k8xC	a
polyetylenovou	polyetylenový	k2eAgFnSc4d1	polyetylenová
fólii	fólie	k1gFnSc4	fólie
<g/>
.	.	kIx.	.
<g/>
Papír	papír	k1gInSc1	papír
lze	lze	k6eAd1	lze
recyklovat	recyklovat	k5eAaBmF	recyklovat
i	i	k9	i
opakovaně	opakovaně	k6eAd1	opakovaně
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
sedminásobná	sedminásobný	k2eAgFnSc1d1	sedminásobná
recyklace	recyklace	k1gFnSc1	recyklace
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
technických	technický	k2eAgFnPc2d1	technická
možností	možnost	k1gFnPc2	možnost
a	a	k8xC	a
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
této	tento	k3xDgFnSc2	tento
hranice	hranice	k1gFnSc2	hranice
nedosahuje	dosahovat	k5eNaImIp3nS	dosahovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
HLAVÁČEK	Hlaváček	k1gMnSc1	Hlaváček
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
-	-	kIx~	-
KAŠPAR	Kašpar	k1gMnSc1	Kašpar
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
-	-	kIx~	-
NOVÝ	Nový	k1gMnSc1	Nový
<g/>
,	,	kIx,	,
Rostislav	Rostislav	k1gMnSc1	Rostislav
<g/>
.	.	kIx.	.
</s>
<s>
Vademecum	Vademecum	k1gInSc1	Vademecum
pomocných	pomocný	k2eAgFnPc2d1	pomocná
věd	věda	k1gFnPc2	věda
historických	historický	k2eAgInPc2d1	historický
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Jinočany	Jinočan	k1gMnPc7	Jinočan
<g/>
:	:	kIx,	:
H	H	kA	H
<g/>
&	&	k?	&
<g/>
H	H	kA	H
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85467	[number]	k4	85467
<g/>
-	-	kIx~	-
<g/>
47	[number]	k4	47
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
s.	s.	k?	s.
29-32	[number]	k4	29-32
</s>
</p>
<p>
<s>
KAŠPAR	Kašpar	k1gMnSc1	Kašpar
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
novověké	novověký	k2eAgFnSc2d1	novověká
latinské	latinský	k2eAgFnSc2d1	Latinská
paleografie	paleografie	k1gFnSc2	paleografie
se	s	k7c7	s
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
zřetelem	zřetel	k1gInSc7	zřetel
k	k	k7c3	k
českým	český	k2eAgFnPc3d1	Česká
zemím	zem	k1gFnPc3	zem
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
SPN	SPN	kA	SPN
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
s.	s.	k?	s.
25-30	[number]	k4	25-30
</s>
</p>
<p>
<s>
WINTER	Winter	k1gMnSc1	Winter
<g/>
,	,	kIx,	,
Zikmund	Zikmund	k1gMnSc1	Zikmund
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
průmysl	průmysl	k1gInSc1	průmysl
a	a	k8xC	a
obchod	obchod	k1gInSc1	obchod
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
akademie	akademie	k1gFnSc1	akademie
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
pro	pro	k7c4	pro
vědy	věda	k1gFnPc4	věda
<g/>
,	,	kIx,	,
slovesnost	slovesnost	k1gFnSc1	slovesnost
a	a	k8xC	a
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Papír	papír	k1gInSc1	papír
<g/>
,	,	kIx,	,
s.	s.	k?	s.
595	[number]	k4	595
<g/>
-	-	kIx~	-
<g/>
598	[number]	k4	598
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
nedostupný	dostupný	k2eNgInSc1d1	nedostupný
zdroj	zdroj	k1gInSc1	zdroj
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Arch	arch	k1gInSc1	arch
papíru	papír	k1gInSc2	papír
</s>
</p>
<p>
<s>
Brusný	brusný	k2eAgInSc1d1	brusný
papír	papír	k1gInSc1	papír
</s>
</p>
<p>
<s>
Formát	formát	k1gInSc1	formát
papíru	papír	k1gInSc2	papír
</s>
</p>
<p>
<s>
Kopírovací	kopírovací	k2eAgInSc1d1	kopírovací
papír	papír	k1gInSc1	papír
</s>
</p>
<p>
<s>
Origami	Origa	k1gFnPc7	Origa
</s>
</p>
<p>
<s>
Paleografie	paleografie	k1gFnSc1	paleografie
</s>
</p>
<p>
<s>
Papyrus	papyrus	k1gInSc1	papyrus
</s>
</p>
<p>
<s>
Pergamen	pergamen	k1gInSc1	pergamen
</s>
</p>
<p>
<s>
Toaletní	toaletní	k2eAgInSc1d1	toaletní
papír	papír	k1gInSc1	papír
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
papír	papír	k1gInSc1	papír
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
papír	papír	k1gInSc1	papír
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Papír	papír	k1gInSc1	papír
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Papír	papír	k1gInSc1	papír
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
co	co	k9	co
dělá	dělat	k5eAaImIp3nS	dělat
<g/>
:	:	kIx,	:
Papír	papír	k1gInSc1	papír
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Výroba	výroba	k1gFnSc1	výroba
papíru	papír	k1gInSc2	papír
<g/>
;	;	kIx,	;
celulóza	celulóza	k1gFnSc1	celulóza
a	a	k8xC	a
lignin	lignin	k1gInSc1	lignin
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Dráhy	dráha	k1gFnSc2	dráha
papíru	papír	k1gInSc2	papír
-	-	kIx~	-
směr	směr	k1gInSc1	směr
vláken	vlákno	k1gNnPc2	vlákno
</s>
</p>
