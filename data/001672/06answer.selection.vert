<s>
Hannes	Hannes	k1gMnSc1	Hannes
Olof	Olof	k1gMnSc1	Olof
Gösta	Gösta	k1gMnSc1	Gösta
Alfvén	Alfvén	k1gInSc1	Alfvén
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
Norrköping	Norrköping	k1gInSc1	Norrköping
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
Djursholm	Djursholm	k1gInSc1	Djursholm
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
astrofyzik	astrofyzik	k1gMnSc1	astrofyzik
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
zejména	zejména	k9	zejména
kosmickou	kosmický	k2eAgFnSc7d1	kosmická
elektrodynamikou	elektrodynamika	k1gFnSc7	elektrodynamika
<g/>
.	.	kIx.	.
</s>
