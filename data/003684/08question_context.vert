<s>
William	William	k1gInSc1
Shakespeare	Shakespeare	k1gMnSc1
(	(	kIx(
<g/>
pokřtěn	pokřtěn	k2eAgMnSc1d1
26	[number]	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1564	[number]	k4
–	–	k?
zemřel	zemřít	k5eAaPmAgInS
23	[number]	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1616	[number]	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
anglický	anglický	k2eAgMnSc1d1
básník	básník	k1gMnSc1
<g/>
,	,	kIx,
dramatik	dramatik	k1gMnSc1
a	a	k8xC
herec	herec	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1
je	být	k5eAaImIp3nS
široce	široko	k6eAd1
považován	považován	k2eAgMnSc1d1
za	za	k7c4
největšího	veliký	k2eAgMnSc4d3
anglicky	anglicky	k6eAd1
píšícího	píšící	k2eAgMnSc2d1
spisovatele	spisovatel	k1gMnSc2
a	a	k8xC
celosvětově	celosvětově	k6eAd1
nejpřednějšího	přední	k2eAgMnSc2d3
dramatika	dramatik	k1gMnSc2
<g/>
.	.	kIx.
</s>