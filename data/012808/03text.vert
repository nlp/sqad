<p>
<s>
Skalní	skalní	k2eAgInSc1d1	skalní
potok	potok	k1gInSc1	potok
je	být	k5eAaImIp3nS	být
potok	potok	k1gInSc4	potok
v	v	k7c6	v
Ústeckém	ústecký	k2eAgInSc6d1	ústecký
kraji	kraj	k1gInSc6	kraj
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
2,18	[number]	k4	2,18
km	km	kA	km
<g/>
,	,	kIx,	,
plocha	plocha	k1gFnSc1	plocha
povodí	povodí	k1gNnSc2	povodí
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
1,8	[number]	k4	1,8
km2	km2	k4	km2
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
vlévá	vlévat	k5eAaImIp3nS	vlévat
se	se	k3xPyFc4	se
do	do	k7c2	do
Jílovského	jílovský	k2eAgInSc2d1	jílovský
potoka	potok	k1gInSc2	potok
v	v	k7c6	v
Libouchci	Libouchec	k1gInSc6	Libouchec
jako	jako	k9	jako
jeho	on	k3xPp3gInSc4	on
levostranný	levostranný	k2eAgInSc4d1	levostranný
přítok	přítok	k1gInSc4	přítok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc1	průběh
toku	tok	k1gInSc2	tok
==	==	k?	==
</s>
</p>
<p>
<s>
Pramení	pramenit	k5eAaImIp3nS	pramenit
na	na	k7c6	na
jihovýchodním	jihovýchodní	k2eAgInSc6d1	jihovýchodní
svahu	svah	k1gInSc6	svah
vrchu	vrch	k1gInSc2	vrch
Nad	nad	k7c7	nad
stěnami	stěna	k1gFnPc7	stěna
(	(	kIx(	(
<g/>
623	[number]	k4	623
m	m	kA	m
<g/>
)	)	kIx)	)
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
490	[number]	k4	490
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Teče	téct	k5eAaImIp3nS	téct
zejména	zejména	k9	zejména
jižním	jižní	k2eAgInSc7d1	jižní
<g/>
,	,	kIx,	,
místy	místy	k6eAd1	místy
jihozápadním	jihozápadní	k2eAgInSc7d1	jihozápadní
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Napájí	napájet	k5eAaImIp3nP	napájet
tři	tři	k4xCgInPc1	tři
malé	malý	k2eAgInPc1d1	malý
rybníky	rybník	k1gInPc1	rybník
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
horní	horní	k2eAgFnSc4d1	horní
dva	dva	k4xCgInPc1	dva
patřící	patřící	k2eAgInPc1d1	patřící
do	do	k7c2	do
chráněné	chráněný	k2eAgFnSc2d1	chráněná
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
Libouchecké	Libouchecký	k2eAgInPc1d1	Libouchecký
rybníčky	rybníček	k1gInPc1	rybníček
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnPc1	jeho
vody	voda	k1gFnPc1	voda
vlévají	vlévat	k5eAaImIp3nP	vlévat
do	do	k7c2	do
Jílovského	jílovský	k2eAgInSc2d1	jílovský
potoka	potok	k1gInSc2	potok
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
307	[number]	k4	307
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Libouchecké	Libouchecký	k2eAgInPc1d1	Libouchecký
rybníčky	rybníček	k1gInPc1	rybníček
==	==	k?	==
</s>
</p>
<p>
<s>
Skalní	skalní	k2eAgInSc1d1	skalní
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
třetinách	třetina	k1gFnPc6	třetina
své	svůj	k3xOyFgFnSc2	svůj
délky	délka	k1gFnSc2	délka
od	od	k7c2	od
pramene	pramen	k1gInSc2	pramen
<g/>
,	,	kIx,	,
napájí	napájet	k5eAaImIp3nP	napájet
dva	dva	k4xCgInPc1	dva
menší	malý	k2eAgInPc1d2	menší
rybníky	rybník	k1gInPc1	rybník
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
hladina	hladina	k1gFnSc1	hladina
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
kótě	kóta	k1gFnSc6	kóta
328	[number]	k4	328
m	m	kA	m
a	a	k8xC	a
324	[number]	k4	324
m	m	kA	m
<g/>
,	,	kIx,	,
a	a	k8xC	a
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
2,16	[number]	k4	2,16
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
spravuje	spravovat	k5eAaImIp3nS	spravovat
AOPK	AOPK	kA	AOPK
ČR	ČR	kA	ČR
Správa	správa	k1gFnSc1	správa
CHKO	CHKO	kA	CHKO
Labské	labský	k2eAgInPc1d1	labský
pískovce	pískovec	k1gInPc1	pískovec
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
ochrany	ochrana	k1gFnSc2	ochrana
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
lokalit	lokalita	k1gFnPc2	lokalita
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
obojživelníků	obojživelník	k1gMnPc2	obojživelník
na	na	k7c6	na
území	území	k1gNnSc6	území
CHKO	CHKO	kA	CHKO
Labské	labský	k2eAgInPc1d1	labský
pískovce	pískovec	k1gInPc1	pískovec
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
vzácné	vzácný	k2eAgInPc4d1	vzácný
a	a	k8xC	a
ohrožené	ohrožený	k2eAgInPc4d1	ohrožený
druhy	druh	k1gInPc4	druh
z	z	k7c2	z
fauny	fauna	k1gFnSc2	fauna
patří	patřit	k5eAaImIp3nS	patřit
blatnice	blatnice	k1gFnSc1	blatnice
skvrnitá	skvrnitý	k2eAgFnSc1d1	skvrnitá
<g/>
,	,	kIx,	,
kuňka	kuňka	k1gFnSc1	kuňka
obecná	obecná	k1gFnSc1	obecná
<g/>
,	,	kIx,	,
skokan	skokan	k1gMnSc1	skokan
štíhlý	štíhlý	k2eAgMnSc1d1	štíhlý
<g/>
,	,	kIx,	,
skokan	skokan	k1gMnSc1	skokan
hnědý	hnědý	k2eAgMnSc1d1	hnědý
<g/>
,	,	kIx,	,
čolek	čolek	k1gMnSc1	čolek
obecný	obecný	k2eAgMnSc1d1	obecný
<g/>
,	,	kIx,	,
ropucha	ropucha	k1gFnSc1	ropucha
obecná	obecná	k1gFnSc1	obecná
a	a	k8xC	a
z	z	k7c2	z
flóry	flóra	k1gFnSc2	flóra
sítina	sítina	k1gFnSc1	sítina
sivá	sivá	k1gFnSc1	sivá
<g/>
,	,	kIx,	,
ostřice	ostřice	k1gFnSc1	ostřice
prosová	prosový	k2eAgFnSc1d1	Prosová
a	a	k8xC	a
rdesno	rdesno	k1gNnSc1	rdesno
obojživelné	obojživelný	k2eAgNnSc1d1	obojživelné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rybník	rybník	k1gInSc1	rybník
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
revitalizován	revitalizovat	k5eAaImNgMnS	revitalizovat
a	a	k8xC	a
odbahněn	odbahnit	k5eAaPmNgMnS	odbahnit
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
byly	být	k5eAaImAgInP	být
odstraněny	odstranit	k5eAaPmNgInP	odstranit
přemnožené	přemnožený	k2eAgInPc1d1	přemnožený
vodní	vodní	k2eAgInPc1d1	vodní
rostliny	rostlina	k1gFnPc4	rostlina
a	a	k8xC	a
náletové	náletový	k2eAgFnPc4d1	náletová
dřeviny	dřevina	k1gFnPc4	dřevina
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
prosperity	prosperita	k1gFnSc2	prosperita
některých	některý	k3yIgInPc2	některý
obojživelných	obojživelný	k2eAgInPc2d1	obojživelný
druhů	druh	k1gInPc2	druh
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vybudování	vybudování	k1gNnSc3	vybudování
tůně	tůně	k1gFnSc2	tůně
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
upraveného	upravený	k2eAgNnSc2d1	upravené
prostředí	prostředí	k1gNnSc2	prostředí
byla	být	k5eAaImAgFnS	být
následně	následně	k6eAd1	následně
vysazena	vysadit	k5eAaPmNgFnS	vysadit
střevle	střevle	k1gFnSc1	střevle
potoční	potoční	k2eAgFnSc1d1	potoční
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přírodní	přírodní	k2eAgFnSc1d1	přírodní
rezervace	rezervace	k1gFnSc1	rezervace
byla	být	k5eAaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
vyhláškou	vyhláška	k1gFnSc7	vyhláška
číslo	číslo	k1gNnSc1	číslo
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
96	[number]	k4	96
Správou	správa	k1gFnSc7	správa
CHKO	CHKO	kA	CHKO
Labské	labský	k2eAgInPc1d1	labský
pískovce	pískovec	k1gInPc1	pískovec
ve	v	k7c6	v
dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
natočila	natočit	k5eAaBmAgFnS	natočit
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
lokalitě	lokalita	k1gFnSc6	lokalita
krátkou	krátký	k2eAgFnSc4d1	krátká
reportáž	reportáž	k1gFnSc4	reportáž
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
pořadu	pořad	k1gInSc2	pořad
Minuta	minuta	k1gFnSc1	minuta
z	z	k7c2	z
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nedaleko	nedaleko	k7c2	nedaleko
rybníků	rybník	k1gInPc2	rybník
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Skalní	skalní	k2eAgInSc1d1	skalní
potok	potok	k1gInSc1	potok
vlévá	vlévat	k5eAaImIp3nS	vlévat
ještě	ještě	k9	ještě
do	do	k7c2	do
rezervace	rezervace	k1gFnSc2	rezervace
nepatřícího	patřící	k2eNgInSc2d1	nepatřící
Ptačího	ptačí	k2eAgInSc2d1	ptačí
rybníka	rybník	k1gInSc2	rybník
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
rozloha	rozloha	k1gFnSc1	rozloha
je	být	k5eAaImIp3nS	být
dvakrát	dvakrát	k6eAd1	dvakrát
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
rozloha	rozloha	k1gFnSc1	rozloha
obou	dva	k4xCgFnPc2	dva
předchozích	předchozí	k2eAgFnPc2d1	předchozí
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
<s>
Hladina	hladina	k1gFnSc1	hladina
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
320	[number]	k4	320
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přítoky	přítok	k1gInPc4	přítok
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
dva	dva	k4xCgInPc4	dva
přítoky	přítok	k1gInPc4	přítok
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
první	první	k4xOgFnSc4	první
je	být	k5eAaImIp3nS	být
bezejmenný	bezejmenný	k2eAgInSc1d1	bezejmenný
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
pramenem	pramen	k1gInSc7	pramen
je	být	k5eAaImIp3nS	být
odtok	odtok	k1gInSc1	odtok
z	z	k7c2	z
vrtu	vrt	k1gInSc2	vrt
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
má	mít	k5eAaImIp3nS	mít
tento	tento	k3xDgInSc1	tento
přítok	přítok	k1gInSc1	přítok
větší	veliký	k2eAgInSc4d2	veliký
průtok	průtok	k1gInSc4	průtok
než	než	k8xS	než
samotný	samotný	k2eAgInSc4d1	samotný
Skalní	skalní	k2eAgInSc4d1	skalní
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
kratší	krátký	k2eAgFnSc1d2	kratší
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
je	být	k5eAaImIp3nS	být
drobný	drobný	k2eAgInSc4d1	drobný
potůček	potůček	k1gInSc4	potůček
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ústí	ústit	k5eAaImIp3nS	ústit
pod	pod	k7c7	pod
přírodní	přírodní	k2eAgFnSc7d1	přírodní
rezervací	rezervace	k1gFnSc7	rezervace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
