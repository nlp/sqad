<s>
Walter	Walter	k1gMnSc1	Walter
"	"	kIx"	"
<g/>
Walt	Walt	k1gMnSc1	Walt
<g/>
"	"	kIx"	"
Whitman	Whitman	k1gMnSc1	Whitman
[	[	kIx(	[
<g/>
volt	volt	k1gInSc1	volt
vitmen	vitmen	k1gInSc1	vitmen
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1819	[number]	k4	1819
<g/>
,	,	kIx,	,
West	West	k2eAgInSc1d1	West
Hills	Hills	k1gInSc1	Hills
<g/>
,	,	kIx,	,
Long	Long	k1gMnSc1	Long
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
-	-	kIx~	-
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1892	[number]	k4	1892
Camden	Camdna	k1gFnPc2	Camdna
<g/>
,	,	kIx,	,
New	New	k1gFnSc2	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
novinář	novinář	k1gMnSc1	novinář
<g/>
;	;	kIx,	;
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
moderní	moderní	k2eAgFnSc2d1	moderní
americké	americký	k2eAgFnSc2d1	americká
poezie	poezie	k1gFnSc2	poezie
<g/>
,	,	kIx,	,
průkopník	průkopník	k1gMnSc1	průkopník
civilismu	civilismus	k1gInSc2	civilismus
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
na	na	k7c4	na
Long	Long	k1gInSc4	Long
Islandu	Island	k1gInSc2	Island
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
v	v	k7c6	v
chudé	chudý	k2eAgFnSc6d1	chudá
quakerské	quakerský	k2eAgFnSc6d1	quakerský
rodině	rodina	k1gFnSc6	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
těžké	těžký	k2eAgFnSc3d1	těžká
ekonomické	ekonomický	k2eAgFnSc3d1	ekonomická
situaci	situace	k1gFnSc3	situace
rodiny	rodina	k1gFnSc2	rodina
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
dětství	dětství	k1gNnSc1	dětství
spíše	spíše	k9	spíše
problematické	problematický	k2eAgNnSc1d1	problematické
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
finančně	finančně	k6eAd1	finančně
podpořit	podpořit	k5eAaPmF	podpořit
svou	svůj	k3xOyFgFnSc4	svůj
rodinu	rodina	k1gFnSc4	rodina
<g/>
,	,	kIx,	,
opustil	opustit	k5eAaPmAgMnS	opustit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1830	[number]	k4	1830
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
poslíček	poslíček	k1gMnSc1	poslíček
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1832	[number]	k4	1832
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
tiskařský	tiskařský	k2eAgMnSc1d1	tiskařský
učeň	učeň	k1gMnSc1	učeň
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
jako	jako	k9	jako
sazeč	sazeč	k1gMnSc1	sazeč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
letech	léto	k1gNnPc6	léto
(	(	kIx(	(
<g/>
1837	[number]	k4	1837
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
venkovským	venkovský	k2eAgMnSc7d1	venkovský
učitelem	učitel	k1gMnSc7	učitel
a	a	k8xC	a
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
1839	[number]	k4	1839
<g/>
)	)	kIx)	)
vydavatelem	vydavatel	k1gMnSc7	vydavatel
<g/>
,	,	kIx,	,
redaktorem	redaktor	k1gMnSc7	redaktor
a	a	k8xC	a
tiskařem	tiskař	k1gMnSc7	tiskař
malých	malý	k2eAgFnPc2d1	malá
novin	novina	k1gFnPc2	novina
Long	Long	k1gMnSc1	Long
Islander	Islander	k1gMnSc1	Islander
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
1841	[number]	k4	1841
<g/>
)	)	kIx)	)
musel	muset	k5eAaImAgInS	muset
tyto	tento	k3xDgFnPc4	tento
noviny	novina	k1gFnPc4	novina
opustit	opustit	k5eAaPmF	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
začal	začít	k5eAaPmAgInS	začít
zveřejňovat	zveřejňovat	k5eAaImF	zveřejňovat
své	svůj	k3xOyFgFnPc4	svůj
první	první	k4xOgFnPc4	první
básně	báseň	k1gFnPc4	báseň
a	a	k8xC	a
povídky	povídka	k1gFnPc4	povídka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
také	také	k9	také
začal	začít	k5eAaPmAgInS	začít
zajímat	zajímat	k5eAaImF	zajímat
o	o	k7c4	o
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
příznivcem	příznivec	k1gMnSc7	příznivec
levého	levý	k2eAgNnSc2d1	levé
křídla	křídlo	k1gNnSc2	křídlo
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1846	[number]	k4	1846
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
redaktorem	redaktor	k1gMnSc7	redaktor
brooklynského	brooklynský	k2eAgInSc2d1	brooklynský
časopisu	časopis	k1gInSc2	časopis
Daily	Daila	k1gMnSc2	Daila
Eagle	Eagl	k1gMnSc2	Eagl
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
napsal	napsat	k5eAaPmAgMnS	napsat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
úvodních	úvodní	k2eAgFnPc2d1	úvodní
kritik	kritika	k1gFnPc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
radikální	radikální	k2eAgInPc4d1	radikální
názory	názor	k1gInPc4	názor
však	však	k9	však
byl	být	k5eAaImAgInS	být
propuštěn	propuštěn	k2eAgInSc1d1	propuštěn
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
vedl	vést	k5eAaImAgInS	vést
noviny	novina	k1gFnPc4	novina
s	s	k7c7	s
názvem	název	k1gInSc7	název
Freeman	Freeman	k1gMnSc1	Freeman
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
odpovídaly	odpovídat	k5eAaImAgFnP	odpovídat
jeho	jeho	k3xOp3gInSc2	jeho
radikalismu	radikalismus	k1gInSc2	radikalismus
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
bojoval	bojovat	k5eAaImAgMnS	bojovat
proti	proti	k7c3	proti
otrokářství	otrokářství	k1gNnSc3	otrokářství
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
americké	americký	k2eAgFnSc2d1	americká
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
války	válka	k1gFnSc2	válka
Severu	sever	k1gInSc2	sever
proti	proti	k7c3	proti
Jihu	jih	k1gInSc3	jih
<g/>
)	)	kIx)	)
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
dobrovolný	dobrovolný	k2eAgMnSc1d1	dobrovolný
ošetřovatel	ošetřovatel	k1gMnSc1	ošetřovatel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
získal	získat	k5eAaPmAgMnS	získat
místo	místo	k7c2	místo
úředníka	úředník	k1gMnSc2	úředník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
opět	opět	k6eAd1	opět
byl	být	k5eAaImAgInS	být
propuštěn	propuštěn	k2eAgInSc1d1	propuštěn
pro	pro	k7c4	pro
svoje	svůj	k3xOyFgInPc4	svůj
radikální	radikální	k2eAgInPc4d1	radikální
postoje	postoj	k1gInPc4	postoj
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
prodělal	prodělat	k5eAaPmAgMnS	prodělat
mrtvici	mrtvice	k1gFnSc4	mrtvice
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byl	být	k5eAaImAgInS	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
upoután	upoutat	k5eAaPmNgInS	upoutat
na	na	k7c4	na
lůžko	lůžko	k1gNnSc4	lůžko
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
homosexuál	homosexuál	k1gMnSc1	homosexuál
či	či	k8xC	či
bisexuál	bisexuál	k1gMnSc1	bisexuál
<g/>
.	.	kIx.	.
</s>
<s>
Whitman	Whitman	k1gMnSc1	Whitman
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
básník	básník	k1gMnSc1	básník
vidí	vidět	k5eAaImIp3nS	vidět
nejdál	daleko	k6eAd3	daleko
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
jeho	jeho	k3xOp3gFnSc1	jeho
"	"	kIx"	"
<g/>
víra	víra	k1gFnSc1	víra
je	být	k5eAaImIp3nS	být
nejpevnější	pevný	k2eAgFnSc1d3	nejpevnější
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
myšlenky	myšlenka	k1gFnPc1	myšlenka
jsou	být	k5eAaImIp3nP	být
chvalozpěvem	chvalozpěv	k1gInSc7	chvalozpěv
na	na	k7c4	na
všechno	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Přiklonil	přiklonit	k5eAaPmAgMnS	přiklonit
se	se	k3xPyFc4	se
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
nazvat	nazvat	k5eAaPmF	nazvat
kosmickým	kosmický	k2eAgNnSc7d1	kosmické
vědomím	vědomí	k1gNnSc7	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
inspirace	inspirace	k1gFnSc1	inspirace
povede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
novému	nový	k2eAgInSc3d1	nový
řádu	řád	k1gInSc3	řád
básníků	básník	k1gMnPc2	básník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nahradí	nahradit	k5eAaPmIp3nP	nahradit
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
předpojal	předpojal	k1gInSc1	předpojal
tzv.	tzv.	kA	tzv.
Beat	beat	k1gInSc1	beat
generation	generation	k1gInSc1	generation
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
nový	nový	k2eAgMnSc1d1	nový
básník	básník	k1gMnSc1	básník
bude	být	k5eAaImBp3nS	být
"	"	kIx"	"
<g/>
lidský	lidský	k2eAgMnSc1d1	lidský
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
neuzná	uznat	k5eNaPmIp3nS	uznat
za	za	k7c4	za
vhodné	vhodný	k2eAgNnSc4d1	vhodné
obhajovat	obhajovat	k5eAaImF	obhajovat
nesmrtelnost	nesmrtelnost	k1gFnSc4	nesmrtelnost
nebo	nebo	k8xC	nebo
Boha	bůh	k1gMnSc4	bůh
nebo	nebo	k8xC	nebo
dokonalost	dokonalost	k1gFnSc4	dokonalost
věcí	věc	k1gFnPc2	věc
nebo	nebo	k8xC	nebo
znamenitou	znamenitý	k2eAgFnSc4d1	znamenitá
krásu	krása	k1gFnSc4	krása
a	a	k8xC	a
skutečnost	skutečnost	k1gFnSc4	skutečnost
duše	duše	k1gFnSc2	duše
<g/>
.	.	kIx.	.
</s>
<s>
Povstanou	povstat	k5eAaPmIp3nP	povstat
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
najdou	najít	k5eAaPmIp3nP	najít
odezvu	odezva	k1gFnSc4	odezva
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
ostatních	ostatní	k2eAgInPc2d1	ostatní
koutů	kout	k1gInPc2	kout
Země	zem	k1gFnSc2	zem
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zamyšlení	zamyšlení	k1gNnSc3	zamyšlení
je	být	k5eAaImIp3nS	být
Whitmanovo	Whitmanův	k2eAgNnSc1d1	Whitmanův
filozofické	filozofický	k2eAgNnSc1d1	filozofické
prohlášení	prohlášení	k1gNnSc1	prohlášení
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Takto	takto	k6eAd1	takto
bys	by	kYmCp2nS	by
měl	mít	k5eAaImAgMnS	mít
konat	konat	k5eAaImF	konat
<g/>
:	:	kIx,	:
Miluj	milovat	k5eAaImRp2nS	milovat
zemi	zem	k1gFnSc4	zem
a	a	k8xC	a
slunce	slunce	k1gNnSc4	slunce
a	a	k8xC	a
zvěř	zvěř	k1gFnSc4	zvěř
<g/>
,	,	kIx,	,
opovrhuj	opovrhovat	k5eAaImRp2nS	opovrhovat
bohatstvím	bohatství	k1gNnPc3	bohatství
<g/>
,	,	kIx,	,
dávej	dávat	k5eAaImRp2nS	dávat
almužnu	almužna	k1gFnSc4	almužna
každému	každý	k3xTgMnSc3	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
si	se	k3xPyFc3	se
o	o	k7c4	o
ni	on	k3xPp3gFnSc4	on
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
zastávej	zastávat	k5eAaImRp2nS	zastávat
se	se	k3xPyFc4	se
hlupců	hlupec	k1gMnPc2	hlupec
a	a	k8xC	a
bláznů	blázen	k1gMnPc2	blázen
<g/>
,	,	kIx,	,
věnuj	věnovat	k5eAaImRp2nS	věnovat
své	svůj	k3xOyFgInPc4	svůj
příjmy	příjem	k1gInPc4	příjem
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
práci	práce	k1gFnSc4	práce
druhým	druhý	k4xOgInSc7	druhý
<g/>
,	,	kIx,	,
nenáviď	nenávidět	k5eAaImRp2nS	nenávidět
tyrany	tyran	k1gMnPc4	tyran
<g/>
,	,	kIx,	,
nepři	přít	k5eNaImRp2nS	přít
se	se	k3xPyFc4	se
o	o	k7c4	o
Bohu	bůh	k1gMnSc3	bůh
<g/>
,	,	kIx,	,
k	k	k7c3	k
lidem	člověk	k1gMnPc3	člověk
buď	buď	k8xC	buď
trpělivý	trpělivý	k2eAgMnSc1d1	trpělivý
a	a	k8xC	a
shovívavý	shovívavý	k2eAgMnSc1d1	shovívavý
<g/>
,	,	kIx,	,
neskláněj	sklánět	k5eNaImRp2nS	sklánět
se	se	k3xPyFc4	se
před	před	k7c7	před
známým	známý	k2eAgMnSc7d1	známý
ani	ani	k8xC	ani
neznámým	známý	k2eNgMnSc7d1	neznámý
nebo	nebo	k8xC	nebo
před	před	k7c7	před
mužem	muž	k1gMnSc7	muž
či	či	k8xC	či
muži	muž	k1gMnPc7	muž
<g/>
,	,	kIx,	,
jednej	jednat	k5eAaImRp2nS	jednat
upřímně	upřímně	k6eAd1	upřímně
se	s	k7c7	s
silnými	silný	k2eAgFnPc7d1	silná
nevzdělanými	vzdělaný	k2eNgFnPc7d1	nevzdělaná
osobami	osoba	k1gFnPc7	osoba
a	a	k8xC	a
matkami	matka	k1gFnPc7	matka
od	od	k7c2	od
rodin	rodina	k1gFnPc2	rodina
<g/>
,	,	kIx,	,
čti	číst	k5eAaImRp2nS	číst
tyto	tento	k3xDgMnPc4	tento
<g />
.	.	kIx.	.
</s>
<s>
stránky	stránka	k1gFnPc1	stránka
na	na	k7c6	na
otevřeném	otevřený	k2eAgNnSc6d1	otevřené
povětří	povětří	k1gNnSc6	povětří
v	v	k7c6	v
každém	každý	k3xTgNnSc6	každý
období	období	k1gNnSc1	období
každého	každý	k3xTgInSc2	každý
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
přebírej	přebírat	k5eAaImRp2nS	přebírat
si	se	k3xPyFc3	se
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k9	co
tě	ty	k3xPp2nSc4	ty
učili	učit	k5eAaImAgMnP	učit
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
nebo	nebo	k8xC	nebo
kostele	kostel	k1gInSc6	kostel
<g/>
,	,	kIx,	,
neb	neb	k8xC	neb
co	co	k3yRnSc1	co
ti	ty	k3xPp2nSc3	ty
řekly	říct	k5eAaPmAgFnP	říct
knihy	kniha	k1gFnPc1	kniha
<g/>
,	,	kIx,	,
opusť	opustit	k5eAaPmRp2nS	opustit
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
zraňuje	zraňovat	k5eAaImIp3nS	zraňovat
tvou	tvůj	k3xOp2gFnSc4	tvůj
duši	duše	k1gFnSc4	duše
a	a	k8xC	a
ty	ty	k3xPp2nSc1	ty
sám	sám	k3xTgMnSc1	sám
budeš	být	k5eAaImBp2nS	být
velkou	velká	k1gFnSc7	velká
básní	báseň	k1gFnPc2	báseň
a	a	k8xC	a
dosáhneš	dosáhnout	k5eAaPmIp2nS	dosáhnout
nejbohatší	bohatý	k2eAgFnPc4d3	nejbohatší
plynulosti	plynulost	k1gFnPc4	plynulost
nejen	nejen	k6eAd1	nejen
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
tichých	tichý	k2eAgFnPc2d1	tichá
linek	linka	k1gFnPc2	linka
rtů	ret	k1gInPc2	ret
a	a	k8xC	a
tváře	tvář	k1gFnSc2	tvář
a	a	k8xC	a
řas	řasa	k1gFnPc2	řasa
a	a	k8xC	a
každičkého	každičký	k3xTgInSc2	každičký
pohybu	pohyb	k1gInSc2	pohyb
tvého	tvůj	k3xOp2gNnSc2	tvůj
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Whitman	Whitman	k1gMnSc1	Whitman
ovlivnil	ovlivnit	k5eAaPmAgMnS	ovlivnit
mnoho	mnoho	k4c4	mnoho
amerických	americký	k2eAgMnPc2d1	americký
básníků	básník	k1gMnPc2	básník
<g/>
.	.	kIx.	.
</s>
<s>
William	William	k1gInSc1	William
Carlos	Carlos	k1gMnSc1	Carlos
Williams	Williams	k1gInSc1	Williams
ho	on	k3xPp3gMnSc4	on
označil	označit	k5eAaPmAgInS	označit
za	za	k7c4	za
básníka	básník	k1gMnSc4	básník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
"	"	kIx"	"
<g/>
prorazil	prorazit	k5eAaPmAgMnS	prorazit
mrtvolností	mrtvolnost	k1gFnSc7	mrtvolnost
kopírovaných	kopírovaný	k2eAgFnPc2d1	kopírovaná
forem	forma	k1gFnPc2	forma
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
dusily	dusit	k5eAaImAgFnP	dusit
člověka	člověk	k1gMnSc4	člověk
nashromážděnou	nashromážděný	k2eAgFnSc7d1	nashromážděná
vahou	váha	k1gFnSc7	váha
tyranií	tyranie	k1gFnPc2	tyranie
minulosti	minulost	k1gFnSc2	minulost
<g/>
,	,	kIx,	,
těch	ten	k3xDgFnPc2	ten
tyranií	tyranie	k1gFnPc2	tyranie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
snažíme	snažit	k5eAaImIp1nP	snažit
umenšit	umenšit	k5eAaPmF	umenšit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
radikálně	radikálně	k6eAd1	radikálně
levicové	levicový	k2eAgNnSc1d1	levicové
<g/>
.	.	kIx.	.
</s>
<s>
Mělo	mít	k5eAaImAgNnS	mít
značný	značný	k2eAgInSc4d1	značný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
celosvětové	celosvětový	k2eAgFnSc2d1	celosvětová
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Stébla	stéblo	k1gNnPc1	stéblo
trávy	tráva	k1gFnSc2	tráva
-	-	kIx~	-
jeho	jeho	k3xOp3gFnSc1	jeho
jediná	jediný	k2eAgFnSc1d1	jediná
básnická	básnický	k2eAgFnSc1d1	básnická
sbírka	sbírka	k1gFnSc1	sbírka
(	(	kIx(	(
<g/>
396	[number]	k4	396
básní	báseň	k1gFnPc2	báseň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
doplňoval	doplňovat	k5eAaImAgMnS	doplňovat
a	a	k8xC	a
přepracovával	přepracovávat	k5eAaImAgMnS	přepracovávat
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
zde	zde	k6eAd1	zde
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
svoji	svůj	k3xOyFgFnSc4	svůj
víru	víra	k1gFnSc4	víra
v	v	k7c4	v
rovnost	rovnost	k1gFnSc4	rovnost
všech	všecek	k3xTgMnPc2	všecek
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
ras	rasa	k1gFnPc2	rasa
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
věří	věřit	k5eAaImIp3nS	věřit
v	v	k7c4	v
demokracii	demokracie	k1gFnSc4	demokracie
a	a	k8xC	a
humanismus	humanismus	k1gInSc4	humanismus
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
metaforu	metafora	k1gFnSc4	metafora
a	a	k8xC	a
volný	volný	k2eAgInSc4d1	volný
verš	verš	k1gInSc4	verš
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
době	doba	k1gFnSc6	doba
znamenal	znamenat	k5eAaImAgInS	znamenat
převrat	převrat	k1gInSc1	převrat
a	a	k8xC	a
uvolnění	uvolnění	k1gNnSc1	uvolnění
v	v	k7c6	v
básnické	básnický	k2eAgFnSc6d1	básnická
tvorbě	tvorba	k1gFnSc6	tvorba
v	v	k7c6	v
celosvětovém	celosvětový	k2eAgNnSc6d1	celosvětové
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
tato	tento	k3xDgFnSc1	tento
sbírka	sbírka	k1gFnSc1	sbírka
básní	básnit	k5eAaImIp3nS	básnit
např.	např.	kA	např.
S.	S.	kA	S.
K.	K.	kA	K.
Neumanna	Neumann	k1gMnSc2	Neumann
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
byla	být	k5eAaImAgFnS	být
sbírka	sbírka	k1gFnSc1	sbírka
přijata	přijmout	k5eAaPmNgFnS	přijmout
s	s	k7c7	s
nepochopením	nepochopení	k1gNnSc7	nepochopení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgNnSc6	první
vydání	vydání	k1gNnSc6	vydání
(	(	kIx(	(
<g/>
1855	[number]	k4	1855
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
výrazně	výrazně	k6eAd1	výrazně
kratší	krátký	k2eAgFnSc1d2	kratší
<g/>
,	,	kIx,	,
do	do	k7c2	do
dalších	další	k2eAgNnPc2d1	další
vydání	vydání	k1gNnPc2	vydání
byly	být	k5eAaImAgFnP	být
přidávány	přidáván	k2eAgFnPc1d1	přidávána
nové	nový	k2eAgFnPc1d1	nová
básně	báseň	k1gFnPc1	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Zpěv	zpěv	k1gInSc1	zpěv
o	o	k7c6	o
mně	já	k3xPp1nSc6	já
-	-	kIx~	-
úvodní	úvodní	k2eAgFnSc1d1	úvodní
báseň	báseň	k1gFnSc1	báseň
<g/>
,	,	kIx,	,
oslava	oslava	k1gFnSc1	oslava
demokracie	demokracie	k1gFnSc2	demokracie
Rány	Rána	k1gFnSc2	Rána
na	na	k7c4	na
buben	buben	k1gInSc4	buben
-	-	kIx~	-
významná	významný	k2eAgFnSc1d1	významná
část	část	k1gFnSc1	část
sbírky	sbírka	k1gFnSc2	sbírka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
původně	původně	k6eAd1	původně
vyjít	vyjít	k5eAaPmF	vyjít
samostatně	samostatně	k6eAd1	samostatně
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
popisuje	popisovat	k5eAaImIp3nS	popisovat
Whitman	Whitman	k1gMnSc1	Whitman
své	svůj	k3xOyFgInPc4	svůj
zážitky	zážitek	k1gInPc4	zážitek
z	z	k7c2	z
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dodatku	dodatek	k1gInSc6	dodatek
jsou	být	k5eAaImIp3nP	být
básně	báseň	k1gFnPc1	báseň
o	o	k7c6	o
smrti	smrt	k1gFnSc6	smrt
prezidenta	prezident	k1gMnSc2	prezident
Abrahama	Abraham	k1gMnSc2	Abraham
Lincolna	Lincoln	k1gMnSc2	Lincoln
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Whitmanovy	Whitmanův	k2eAgFnSc2d1	Whitmanova
patrně	patrně	k6eAd1	patrně
nejslavnější	slavný	k2eAgFnSc2d3	nejslavnější
básně	báseň	k1gFnSc2	báseň
<g/>
,	,	kIx,	,
elegie	elegie	k1gFnSc2	elegie
O	o	k7c4	o
Captain	Captain	k1gInSc4	Captain
<g/>
!	!	kIx.	!
</s>
<s>
My	my	k3xPp1nPc1	my
Captain	Captain	k1gInSc4	Captain
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyhlídky	vyhlídka	k1gFnPc1	vyhlídka
demokracie	demokracie	k1gFnSc2	demokracie
(	(	kIx(	(
<g/>
1871	[number]	k4	1871
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Příkladné	příkladný	k2eAgInPc1d1	příkladný
dny	den	k1gInPc1	den
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
-	-	kIx~	-
výbor	výbor	k1gInSc1	výbor
drobných	drobný	k2eAgFnPc2d1	drobná
próz	próza	k1gFnPc2	próza
<g/>
.	.	kIx.	.
</s>
<s>
Demokracie	demokracie	k1gFnSc5	demokracie
<g/>
,	,	kIx,	,
ženo	žena	k1gFnSc5	žena
má	mít	k5eAaImIp3nS	mít
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
:	:	kIx,	:
výbor	výbor	k1gInSc1	výbor
ze	z	k7c2	z
Stébel	stéblo	k1gNnPc2	stéblo
trávy	tráva	k1gFnSc2	tráva
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
-	-	kIx~	-
překlad	překlad	k1gInSc1	překlad
Pavel	Pavel	k1gMnSc1	Pavel
Eisner	Eisner	k1gMnSc1	Eisner
</s>
