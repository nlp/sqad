<s>
Satoši	Satoš	k1gMnPc1	Satoš
Kon	kon	k1gInSc1	kon
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1963	[number]	k4	1963
–	–	k?	–
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
japonský	japonský	k2eAgMnSc1d1	japonský
režisér	režisér	k1gMnSc1	režisér
anime	animat	k5eAaPmIp3nS	animat
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInPc6	jehož
filmech	film	k1gInPc6	film
se	se	k3xPyFc4	se
realita	realita	k1gFnSc1	realita
často	často	k6eAd1	často
mísila	mísit	k5eAaImAgFnS	mísit
se	s	k7c7	s
sny	sen	k1gInPc7	sen
a	a	k8xC	a
vzpomínkami	vzpomínka	k1gFnPc7	vzpomínka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
byl	být	k5eAaImAgInS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
literaturou	literatura	k1gFnSc7	literatura
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
sci-fi	scii	k1gFnSc2	sci-fi
a	a	k8xC	a
mystiky	mystika	k1gFnSc2	mystika
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
od	od	k7c2	od
Philipa	Philip	k1gMnSc2	Philip
K.	K.	kA	K.
Dicka	Dicek	k1gMnSc4	Dicek
nebo	nebo	k8xC	nebo
Jasutaka	Jasutak	k1gMnSc4	Jasutak
Cucui	Cucu	k1gFnSc2	Cucu
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Cucuiho	Cucui	k1gMnSc2	Cucui
předlohy	předloha	k1gFnSc2	předloha
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
film	film	k1gInSc1	film
Paprika	paprika	k1gFnSc1	paprika
<g/>
.	.	kIx.	.
</s>
<s>
Konovými	Konův	k2eAgInPc7d1	Konův
filmovými	filmový	k2eAgInPc7d1	filmový
vzory	vzor	k1gInPc7	vzor
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
kinematografii	kinematografie	k1gFnSc6	kinematografie
byly	být	k5eAaImAgInP	být
například	například	k6eAd1	například
filmy	film	k1gInPc4	film
Jatka	jatka	k1gFnSc1	jatka
č.	č.	k?	č.
5	[number]	k4	5
George	Georg	k1gMnSc2	Georg
Roye	Roy	k1gMnSc2	Roy
Hilla	Hill	k1gMnSc2	Hill
<g/>
,	,	kIx,	,
Město	město	k1gNnSc1	město
ztacených	ztacený	k2eAgFnPc2d1	ztacená
dětí	dítě	k1gFnPc2	dítě
Marco	Marco	k6eAd1	Marco
Cara	car	k1gMnSc4	car
a	a	k8xC	a
Brazil	Brazil	k1gFnSc4	Brazil
<g/>
,	,	kIx,	,
Zloději	zloděj	k1gMnPc1	zloděj
času	čas	k1gInSc2	čas
či	či	k8xC	či
Dobrodružství	dobrodružství	k1gNnSc2	dobrodružství
Barona	baron	k1gMnSc2	baron
Prášila	Prášil	k1gMnSc2	Prášil
Terry	Terra	k1gMnSc2	Terra
Gilliama	Gilliam	k1gMnSc2	Gilliam
<g/>
.	.	kIx.	.
</s>
<s>
Satoši	Satoš	k1gMnPc1	Satoš
Kon	kon	k1gInSc1	kon
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
46	[number]	k4	46
let	léto	k1gNnPc2	léto
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
filmu	film	k1gInSc6	film
Jume	Jum	k1gFnSc2	Jum
miru	mir	k1gInSc2	mir
kikai	kika	k1gFnSc2	kika
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Dream	Dream	k1gInSc1	Dream
Machine	Machin	k1gInSc5	Machin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Magnetická	magnetický	k2eAgFnSc1d1	magnetická
růže	růže	k1gFnSc1	růže
(	(	kIx(	(
<g/>
povídka	povídka	k1gFnSc1	povídka
z	z	k7c2	z
filmu	film	k1gInSc2	film
Memories	Memoriesa	k1gFnPc2	Memoriesa
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Herečka	herečka	k1gFnSc1	herečka
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Tokijští	tokijský	k2eAgMnPc1d1	tokijský
kmotři	kmotr	k1gMnPc1	kmotr
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Paprika	paprika	k1gFnSc1	paprika
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Perfect	Perfect	k2eAgInSc1d1	Perfect
Blue	Blue	k1gInSc1	Blue
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Herečka	herečka	k1gFnSc1	herečka
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Tokijští	tokijský	k2eAgMnPc1d1	tokijský
<g />
.	.	kIx.	.
</s>
<s>
kmotři	kmotr	k1gMnPc1	kmotr
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Paranoia	paranoia	k1gFnSc1	paranoia
Agent	agent	k1gMnSc1	agent
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Paprika	paprika	k1gFnSc1	paprika
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Dobré	dobrý	k2eAgNnSc1d1	dobré
ráno	ráno	k1gNnSc1	ráno
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
*	*	kIx~	*
Ani	ani	k8xC	ani
Kuri	Kuri	k1gNnSc1	Kuri
<g/>
15	[number]	k4	15
)	)	kIx)	)
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Dreaming	Dreaming	k1gInSc1	Dreaming
Machine	Machin	k1gInSc5	Machin
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Roujin	Roujin	k1gInSc1	Roujin
Z	z	k7c2	z
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Perfect	Perfect	k2eAgInSc1d1	Perfect
Blue	Blue	k1gInSc1	Blue
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Herečka	herečka	k1gFnSc1	herečka
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Tokijští	tokijský	k2eAgMnPc1d1	tokijský
kmotři	kmotr	k1gMnPc1	kmotr
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Portrét	portrét	k1gInSc1	portrét
na	na	k7c4	na
rejže	rejže	k?	rejže
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Článek	článek	k1gInSc1	článek
manga	mango	k1gNnSc2	mango
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Satoši	Satoš	k1gMnPc1	Satoš
Kon	kon	k1gInSc4	kon
(	(	kIx(	(
<g/>
profil	profil	k1gInSc4	profil
<g/>
)	)	kIx)	)
na	na	k7c6	na
REANIMATED	REANIMATED	kA	REANIMATED
Satoši	Satoš	k1gMnSc3	Satoš
Kon	kon	k1gInSc1	kon
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
Satoši	Satoš	k1gMnPc1	Satoš
Kon	kon	k1gInSc4	kon
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
