<s>
Satoši	Satoš	k1gMnPc1	Satoš
Kon	kon	k1gInSc1	kon
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1963	[number]	k4	1963
–	–	k?	–
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
japonský	japonský	k2eAgMnSc1d1	japonský
režisér	režisér	k1gMnSc1	režisér
anime	animat	k5eAaPmIp3nS	animat
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInPc6	jehož
filmech	film	k1gInPc6	film
se	se	k3xPyFc4	se
realita	realita	k1gFnSc1	realita
často	často	k6eAd1	často
mísila	mísit	k5eAaImAgFnS	mísit
se	s	k7c7	s
sny	sen	k1gInPc7	sen
a	a	k8xC	a
vzpomínkami	vzpomínka	k1gFnPc7	vzpomínka
<g/>
.	.	kIx.	.
</s>
