<p>
<s>
Finské	finský	k2eAgNnSc1d1	finské
velkoknížectví	velkoknížectví	k1gNnSc1	velkoknížectví
(	(	kIx(	(
<g/>
finsky	finsky	k6eAd1	finsky
Suomen	Suomen	k1gInSc1	Suomen
suuriruhtinaskunta	suuriruhtinaskunto	k1gNnSc2	suuriruhtinaskunto
<g/>
;	;	kIx,	;
rusky	rusky	k6eAd1	rusky
В	В	k?	В
к	к	k?	к
Ф	Ф	k?	Ф
<g/>
,	,	kIx,	,
Velikoje	Velikoj	k1gInPc1	Velikoj
kňažestvo	kňažestvo	k1gNnSc1	kňažestvo
Finljandskoje	Finljandskoj	k1gInSc2	Finljandskoj
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
státní	státní	k2eAgInSc1d1	státní
útvar	útvar	k1gInSc1	útvar
v	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
Evropě	Evropa	k1gFnSc6	Evropa
ustavený	ustavený	k2eAgMnSc1d1	ustavený
po	po	k7c6	po
finské	finský	k2eAgFnSc6d1	finská
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1809	[number]	k4	1809
a	a	k8xC	a
existující	existující	k2eAgFnPc1d1	existující
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
jakožto	jakožto	k8xS	jakožto
autonomní	autonomní	k2eAgFnSc1d1	autonomní
součást	součást	k1gFnSc1	součást
Ruské	ruský	k2eAgFnSc2d1	ruská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
velkoknížectví	velkoknížectví	k1gNnSc2	velkoknížectví
bylo	být	k5eAaImAgNnS	být
o	o	k7c4	o
22	[number]	k4	22
000	[number]	k4	000
km2	km2	k4	km2
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
rozloha	rozloha	k1gFnSc1	rozloha
Moravy	Morava	k1gFnSc2	Morava
<g/>
)	)	kIx)	)
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
území	území	k1gNnSc4	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Finské	finský	k2eAgFnSc2d1	finská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ta	ten	k3xDgFnSc1	ten
ztratila	ztratit	k5eAaPmAgFnS	ztratit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
většinu	většina	k1gFnSc4	většina
své	svůj	k3xOyFgFnSc2	svůj
části	část	k1gFnSc2	část
Karélie	Karélie	k1gFnSc2	Karélie
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
koridorem	koridor	k1gInSc7	koridor
Pečenga	Pečenga	k1gFnSc1	Pečenga
<g/>
;	;	kIx,	;
nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
původních	původní	k2eAgFnPc2d1	původní
hranic	hranice	k1gFnPc2	hranice
velkoknížectví	velkoknížectví	k1gNnSc2	velkoknížectví
se	se	k3xPyFc4	se
kryje	krýt	k5eAaImIp3nS	krýt
s	s	k7c7	s
těmi	ten	k3xDgMnPc7	ten
dnešními	dnešní	k2eAgMnPc7d1	dnešní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dřívější	dřívější	k2eAgMnPc1d1	dřívější
vládci	vládce	k1gMnPc1	vládce
Finska	Finsko	k1gNnSc2	Finsko
<g/>
,	,	kIx,	,
švédští	švédský	k2eAgMnPc1d1	švédský
králové	král	k1gMnPc1	král
<g/>
,	,	kIx,	,
sice	sice	k8xC	sice
přijali	přijmout	k5eAaPmAgMnP	přijmout
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1556	[number]	k4	1556
titul	titul	k1gInSc4	titul
vévoda	vévoda	k1gMnSc1	vévoda
finský	finský	k2eAgMnSc1d1	finský
(	(	kIx(	(
<g/>
od	od	k7c2	od
r.	r.	kA	r.
1581	[number]	k4	1581
pak	pak	k6eAd1	pak
velkovévoda	velkovévoda	k1gMnSc1	velkovévoda
finský	finský	k2eAgMnSc1d1	finský
<g/>
,	,	kIx,	,
karelský	karelský	k2eAgMnSc1d1	karelský
<g/>
,	,	kIx,	,
ingrijský	ingrijský	k2eAgMnSc1d1	ingrijský
a	a	k8xC	a
votský	votský	k2eAgMnSc1d1	votský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
Finsko	Finsko	k1gNnSc4	Finsko
jako	jako	k8xC	jako
takové	takový	k3xDgNnSc1	takový
bylo	být	k5eAaImAgNnS	být
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
švédské	švédský	k2eAgFnSc2d1	švédská
vlády	vláda	k1gFnSc2	vláda
integrální	integrální	k2eAgFnSc7d1	integrální
součástí	součást	k1gFnSc7	součást
Švédska	Švédsko	k1gNnSc2	Švédsko
a	a	k8xC	a
netvořilo	tvořit	k5eNaImAgNnS	tvořit
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
správní	správní	k2eAgFnSc4d1	správní
jednotku	jednotka	k1gFnSc4	jednotka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velkoknížectví	velkoknížectví	k1gNnSc1	velkoknížectví
bylo	být	k5eAaImAgNnS	být
prvním	první	k4xOgMnSc7	první
samosprávným	samosprávný	k2eAgNnSc7d1	samosprávné
finským	finský	k2eAgNnSc7d1	finské
územím	území	k1gNnSc7	území
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
,	,	kIx,	,
státoprávní	státoprávní	k2eAgFnPc1d1	státoprávní
a	a	k8xC	a
hospodářské	hospodářský	k2eAgFnPc1d1	hospodářská
podmínky	podmínka	k1gFnPc1	podmínka
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
panující	panující	k2eAgInPc1d1	panující
výrazně	výrazně	k6eAd1	výrazně
napomohly	napomoct	k5eAaPmAgInP	napomoct
vzniku	vznik	k1gInSc3	vznik
novodobého	novodobý	k2eAgInSc2d1	novodobý
finského	finský	k2eAgInSc2d1	finský
národa	národ	k1gInSc2	národ
a	a	k8xC	a
zkušenosti	zkušenost	k1gFnSc2	zkušenost
ze	z	k7c2	z
"	"	kIx"	"
<g/>
stýkání	stýkání	k1gNnSc2	stýkání
a	a	k8xC	a
potýkání	potýkání	k1gNnSc2	potýkání
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
s	s	k7c7	s
Rusy	Rus	k1gMnPc7	Rus
determinovaly	determinovat	k5eAaBmAgFnP	determinovat
dějiny	dějiny	k1gFnPc1	dějiny
samostatného	samostatný	k2eAgNnSc2d1	samostatné
Finska	Finsko	k1gNnSc2	Finsko
a	a	k8xC	a
postoje	postoj	k1gInSc2	postoj
Finů	Fin	k1gMnPc2	Fin
i	i	k8xC	i
v	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
epoše	epocha	k1gFnSc6	epocha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Švédska	Švédsko	k1gNnSc2	Švédsko
ve	v	k7c6	v
finské	finský	k2eAgFnSc6d1	finská
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1809	[number]	k4	1809
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Finska	Finsko	k1gNnSc2	Finsko
obsazeno	obsadit	k5eAaPmNgNnS	obsadit
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Smlouvou	smlouva	k1gFnSc7	smlouva
v	v	k7c6	v
Hamině	Hamina	k1gFnSc6	Hamina
(	(	kIx(	(
<g/>
Fredrikshamn	Fredrikshamn	k1gNnSc1	Fredrikshamn
<g/>
)	)	kIx)	)
švédská	švédský	k2eAgFnSc1d1	švédská
říše	říše	k1gFnSc1	říše
byla	být	k5eAaImAgFnS	být
nucena	nucen	k2eAgFnSc1d1	nucena
vzdát	vzdát	k5eAaPmF	vzdát
se	se	k3xPyFc4	se
Finska	Finsko	k1gNnSc2	Finsko
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
t.	t.	k?	t.
r.	r.	kA	r.
<g/>
,	,	kIx,	,
ruský	ruský	k2eAgMnSc1d1	ruský
car	car	k1gMnSc1	car
Alexandr	Alexandr	k1gMnSc1	Alexandr
I.	I.	kA	I.
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
na	na	k7c6	na
stavovském	stavovský	k2eAgNnSc6d1	Stavovské
shromáždění	shromáždění	k1gNnSc6	shromáždění
v	v	k7c6	v
městě	město	k1gNnSc6	město
Porvoo	Porvoo	k6eAd1	Porvoo
Finsku	Finsko	k1gNnSc6	Finsko
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
privilegií	privilegium	k1gNnPc2	privilegium
a	a	k8xC	a
práv	právo	k1gNnPc2	právo
pocházejících	pocházející	k2eAgMnPc2d1	pocházející
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
švédské	švédský	k2eAgFnSc2d1	švédská
nadvlády	nadvláda	k1gFnSc2	nadvláda
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
byla	být	k5eAaImAgFnS	být
například	například	k6eAd1	například
existence	existence	k1gFnSc1	existence
stavovského	stavovský	k2eAgInSc2d1	stavovský
sněmu	sněm	k1gInSc2	sněm
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgInPc4d1	vlastní
zákony	zákon	k1gInPc4	zákon
<g/>
)	)	kIx)	)
a	a	k8xC	a
ustavil	ustavit	k5eAaPmAgMnS	ustavit
Finské	finský	k2eAgNnSc4d1	finské
velkoknížectví	velkoknížectví	k1gNnSc4	velkoknížectví
jakožto	jakožto	k8xS	jakožto
autonomní	autonomní	k2eAgFnSc4d1	autonomní
provincii	provincie	k1gFnSc4	provincie
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1816	[number]	k4	1816
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
car	car	k1gMnSc1	car
zavázal	zavázat	k5eAaPmAgMnS	zavázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
státoprávní	státoprávní	k2eAgInSc1d1	státoprávní
statut	statut	k1gInSc1	statut
darovaný	darovaný	k2eAgInSc1d1	darovaný
Finsku	Finsko	k1gNnSc6	Finsko
bude	být	k5eAaImBp3nS	být
dodržován	dodržovat	k5eAaImNgInS	dodržovat
i	i	k9	i
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
jeho	jeho	k3xOp3gMnPc2	jeho
následníků	následník	k1gMnPc2	následník
na	na	k7c6	na
trůnu	trůn	k1gInSc6	trůn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Rusové	Rus	k1gMnPc1	Rus
snažili	snažit	k5eAaImAgMnP	snažit
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
potlačit	potlačit	k5eAaPmF	potlačit
vliv	vliv	k1gInSc1	vliv
místních	místní	k2eAgFnPc2d1	místní
švédských	švédský	k2eAgFnPc2d1	švédská
elit	elita	k1gFnPc2	elita
a	a	k8xC	a
omezit	omezit	k5eAaPmF	omezit
tak	tak	k6eAd1	tak
další	další	k2eAgFnSc4d1	další
politickou	politický	k2eAgFnSc4d1	politická
a	a	k8xC	a
kulturní	kulturní	k2eAgFnSc4d1	kulturní
orientaci	orientace	k1gFnSc4	orientace
země	zem	k1gFnSc2	zem
na	na	k7c4	na
Švédsko	Švédsko	k1gNnSc4	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
této	tento	k3xDgFnSc2	tento
politiky	politika	k1gFnSc2	politika
paradoxně	paradoxně	k6eAd1	paradoxně
podporovali	podporovat	k5eAaImAgMnP	podporovat
finské	finský	k2eAgNnSc4d1	finské
národní	národní	k2eAgNnSc4d1	národní
hnutí	hnutí	k1gNnSc4	hnutí
a	a	k8xC	a
finský	finský	k2eAgInSc4d1	finský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1828	[number]	k4	1828
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
Helsinské	helsinský	k2eAgFnSc6d1	Helsinská
univerzitě	univerzita	k1gFnSc6	univerzita
otevřena	otevřít	k5eAaPmNgFnS	otevřít
katedra	katedra	k1gFnSc1	katedra
finského	finský	k2eAgInSc2d1	finský
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
ustavena	ustaven	k2eAgFnSc1d1	ustavena
profesura	profesura	k1gFnSc1	profesura
finštiny	finština	k1gFnSc2	finština
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Alexandra	Alexandr	k1gMnSc2	Alexandr
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1863	[number]	k4	1863
byla	být	k5eAaImAgFnS	být
finština	finština	k1gFnSc1	finština
zrovnoprávněna	zrovnoprávnit	k5eAaPmNgFnS	zrovnoprávnit
jako	jako	k8xS	jako
oficiální	oficiální	k2eAgInSc4d1	oficiální
úřední	úřední	k2eAgInSc4d1	úřední
jazyk	jazyk	k1gInSc4	jazyk
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
imperiální	imperiální	k2eAgFnSc1d1	imperiální
moc	moc	k1gFnSc1	moc
tak	tak	k9	tak
přímo	přímo	k6eAd1	přímo
i	i	k9	i
nepřímo	přímo	k6eNd1	přímo
podporovala	podporovat	k5eAaImAgFnS	podporovat
finizaci	finizace	k1gFnSc4	finizace
Finska	Finsko	k1gNnSc2	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
stejných	stejný	k2eAgInPc2d1	stejný
důvodů	důvod	k1gInPc2	důvod
bylo	být	k5eAaImAgNnS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
přesunuto	přesunout	k5eAaPmNgNnS	přesunout
z	z	k7c2	z
Turku	turek	k1gInSc6	turek
(	(	kIx(	(
<g/>
Å	Å	k?	Å
<g/>
)	)	kIx)	)
do	do	k7c2	do
doposud	doposud	k6eAd1	doposud
nevýznamných	významný	k2eNgFnPc2d1	nevýznamná
Helsinek	Helsinky	k1gFnPc2	Helsinky
(	(	kIx(	(
<g/>
Helsingfors	Helsingfors	k1gInSc1	Helsingfors
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
bylo	být	k5eAaImAgNnS	být
Finsko	Finsko	k1gNnSc1	Finsko
viděno	vidět	k5eAaImNgNnS	vidět
očima	oko	k1gNnPc7	oko
Rusů	Rus	k1gMnPc2	Rus
jako	jako	k9	jako
nevýznamná	významný	k2eNgFnSc1d1	nevýznamná
okrajina	okrajina	k1gFnSc1	okrajina
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
vhodná	vhodný	k2eAgFnSc1d1	vhodná
destinace	destinace	k1gFnSc1	destinace
pro	pro	k7c4	pro
odpočinkové	odpočinkový	k2eAgInPc4d1	odpočinkový
pobyty	pobyt	k1gInPc4	pobyt
ruské	ruský	k2eAgFnSc2d1	ruská
šlechty	šlechta	k1gFnSc2	šlechta
i	i	k8xC	i
carské	carský	k2eAgFnSc2d1	carská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Především	především	k9	především
od	od	k7c2	od
konce	konec	k1gInSc2	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
konci	konec	k1gInSc6	konec
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
postoje	postoj	k1gInSc2	postoj
ruského	ruský	k2eAgMnSc2d1	ruský
centra	centr	k1gMnSc2	centr
k	k	k7c3	k
Finsku	Finsko	k1gNnSc3	Finsko
měnily	měnit	k5eAaImAgFnP	měnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rusifikačních	rusifikační	k2eAgFnPc2d1	rusifikační
snah	snaha	k1gFnPc2	snaha
byla	být	k5eAaImAgFnS	být
postupně	postupně	k6eAd1	postupně
prosazována	prosazovat	k5eAaImNgFnS	prosazovat
politika	politika	k1gFnSc1	politika
větší	veliký	k2eAgFnSc2d2	veliký
integrace	integrace	k1gFnSc2	integrace
Finska	Finsko	k1gNnSc2	Finsko
do	do	k7c2	do
Ruské	ruský	k2eAgFnSc2d1	ruská
říše	říš	k1gFnSc2	říš
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
osobnostmi	osobnost	k1gFnPc7	osobnost
gubernátorů	gubernátor	k1gMnPc2	gubernátor
Nikolaje	Nikolaj	k1gMnSc2	Nikolaj
Ivanoviče	Ivanovič	k1gMnSc2	Ivanovič
Bobrikova	Bobrikův	k2eAgMnSc2d1	Bobrikův
v	v	k7c6	v
letech	let	k1gInPc6	let
1898	[number]	k4	1898
<g/>
–	–	k?	–
<g/>
1904	[number]	k4	1904
a	a	k8xC	a
Franze	Franze	k1gFnSc1	Franze
Alberta	Albert	k1gMnSc2	Albert
Seyna	Seyn	k1gMnSc2	Seyn
v	v	k7c6	v
letech	let	k1gInPc6	let
1909	[number]	k4	1909
<g/>
–	–	k?	–
<g/>
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
posledním	poslední	k2eAgMnSc7d1	poslední
generálním	generální	k2eAgMnSc7d1	generální
guvernérem	guvernér	k1gMnSc7	guvernér
Finska	Finsko	k1gNnSc2	Finsko
byl	být	k5eAaImAgMnS	být
Konstituční	konstituční	k2eAgMnSc1d1	konstituční
demokrat	demokrat	k1gMnSc1	demokrat
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Vissarionovič	Vissarionovič	k1gMnSc1	Vissarionovič
Někrasov	Někrasovo	k1gNnPc2	Někrasovo
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
nebyl	být	k5eNaImAgMnS	být
zastáncem	zastánce	k1gMnSc7	zastánce
rusifikace	rusifikace	k1gFnSc2	rusifikace
a	a	k8xC	a
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
Finsku	Finsko	k1gNnSc6	Finsko
větší	veliký	k2eAgFnSc4d2	veliký
autonomii	autonomie	k1gFnSc4	autonomie
<g/>
,	,	kIx,	,
v	v	k7c6	v
lepším	dobrý	k2eAgInSc6d2	lepší
případě	případ	k1gInSc6	případ
úplnou	úplný	k2eAgFnSc4d1	úplná
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tu	tu	k6eAd1	tu
Finsko	Finsko	k1gNnSc1	Finsko
za	za	k7c2	za
změněných	změněný	k2eAgFnPc2d1	změněná
podmínek	podmínka	k1gFnPc2	podmínka
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1917	[number]	k4	1917
a	a	k8xC	a
definitivně	definitivně	k6eAd1	definitivně
obdrželo	obdržet	k5eAaPmAgNnS	obdržet
(	(	kIx(	(
<g/>
uznáním	uznání	k1gNnSc7	uznání
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
nově	nově	k6eAd1	nově
vzniknuvší	vzniknuvší	k2eAgFnSc2d1	vzniknuvší
Ruské	ruský	k2eAgFnSc2d1	ruská
sovětské	sovětský	k2eAgFnSc2d1	sovětská
republiky	republika	k1gFnSc2	republika
<g/>
)	)	kIx)	)
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Správní	správní	k2eAgInSc4d1	správní
vývoj	vývoj	k1gInSc4	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
Ruský	ruský	k2eAgMnSc1d1	ruský
car	car	k1gMnSc1	car
vládl	vládnout	k5eAaImAgMnS	vládnout
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
jako	jako	k8xS	jako
finský	finský	k2eAgMnSc1d1	finský
velkokníže	velkokníže	k1gMnSc1	velkokníže
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
země	země	k1gFnSc1	země
měla	mít	k5eAaImAgFnS	mít
jistou	jistý	k2eAgFnSc4d1	jistá
administrativní	administrativní	k2eAgFnSc4d1	administrativní
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
,	,	kIx,	,
nestala	stát	k5eNaPmAgFnS	stát
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
gubernií	gubernie	k1gFnSc7	gubernie
Ruské	ruský	k2eAgFnSc2d1	ruská
říše	říš	k1gFnSc2	říš
jako	jako	k8xC	jako
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
(	(	kIx(	(
<g/>
Malá	malý	k2eAgFnSc1d1	malá
Rus	Rus	k1gFnSc1	Rus
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Polské	polský	k2eAgNnSc1d1	polské
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1863	[number]	k4	1863
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
představitelem	představitel	k1gMnSc7	představitel
ruské	ruský	k2eAgFnSc2d1	ruská
moci	moc	k1gFnSc2	moc
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
byl	být	k5eAaImAgMnS	být
gubernátor	gubernátor	k1gMnSc1	gubernátor
(	(	kIx(	(
<g/>
generální	generální	k2eAgMnSc1d1	generální
guvernér	guvernér	k1gMnSc1	guvernér
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jemu	on	k3xPp3gNnSc3	on
po	po	k7c6	po
boku	bok	k1gInSc6	bok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
menšími	malý	k2eAgFnPc7d2	menší
pravomocemi	pravomoc	k1gFnPc7	pravomoc
<g/>
,	,	kIx,	,
stál	stát	k5eAaImAgInS	stát
Carský	carský	k2eAgInSc1d1	carský
finský	finský	k2eAgInSc1d1	finský
senát	senát	k1gInSc1	senát
<g/>
,	,	kIx,	,
doprovázený	doprovázený	k2eAgInSc1d1	doprovázený
u	u	k7c2	u
carského	carský	k2eAgInSc2d1	carský
dvora	dvůr	k1gInSc2	dvůr
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
svým	svůj	k3xOyFgInSc7	svůj
ekvivalentem	ekvivalent	k1gInSc7	ekvivalent
<g/>
,	,	kIx,	,
Výborem	výbor	k1gInSc7	výbor
pro	pro	k7c4	pro
finské	finský	k2eAgFnPc4d1	finská
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc4	jehož
předsedové	předseda	k1gMnPc1	předseda
měli	mít	k5eAaImAgMnP	mít
přímý	přímý	k2eAgInSc4d1	přímý
styk	styk	k1gInSc4	styk
s	s	k7c7	s
carem	car	k1gMnSc7	car
a	a	k8xC	a
předkládali	předkládat	k5eAaImAgMnP	předkládat
mu	on	k3xPp3gMnSc3	on
návrhy	návrh	k1gInPc1	návrh
a	a	k8xC	a
doporučení	doporučení	k1gNnSc4	doporučení
senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
Mocenskou	mocenský	k2eAgFnSc7d1	mocenská
protiváhou	protiváha	k1gFnSc7	protiváha
senátu	senát	k1gInSc2	senát
byl	být	k5eAaImAgInS	být
finský	finský	k2eAgInSc1d1	finský
zemský	zemský	k2eAgInSc1d1	zemský
sněm	sněm	k1gInSc1	sněm
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
začal	začít	k5eAaPmAgMnS	začít
scházet	scházet	k5eAaImF	scházet
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
holdovacího	holdovací	k2eAgInSc2d1	holdovací
sněmu	sněm	k1gInSc2	sněm
v	v	k7c6	v
Porvoo	Porvoo	k6eAd1	Porvoo
<g/>
)	)	kIx)	)
až	až	k6eAd1	až
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1863	[number]	k4	1863
<g/>
.	.	kIx.	.
</s>
<s>
Sněm	sněm	k1gInSc1	sněm
byl	být	k5eAaImAgInS	být
čtyřkomorový	čtyřkomorový	k2eAgInSc1d1	čtyřkomorový
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
zreformovaný	zreformovaný	k2eAgInSc4d1	zreformovaný
na	na	k7c4	na
jednokomorový	jednokomorový	k2eAgInSc4d1	jednokomorový
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
zcela	zcela	k6eAd1	zcela
moderní	moderní	k2eAgInSc1d1	moderní
parlament	parlament	k1gInSc1	parlament
(	(	kIx(	(
<g/>
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
jako	jako	k9	jako
první	první	k4xOgFnSc7	první
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
mohly	moct	k5eAaImAgFnP	moct
volit	volit	k5eAaImF	volit
a	a	k8xC	a
být	být	k5eAaImF	být
voleny	volen	k2eAgFnPc4d1	volena
i	i	k8xC	i
ženy	žena	k1gFnPc4	žena
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
měl	mít	k5eAaImAgMnS	mít
malé	malý	k2eAgFnPc4d1	malá
zákonodárné	zákonodárný	k2eAgFnPc4d1	zákonodárná
pravomoci	pravomoc	k1gFnPc4	pravomoc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
jeho	jeho	k3xOp3gInSc2	jeho
souhlasu	souhlas	k1gInSc2	souhlas
nemohli	moct	k5eNaImAgMnP	moct
carové	car	k1gMnPc1	car
vydávat	vydávat	k5eAaPmF	vydávat
pro	pro	k7c4	pro
Finsko	Finsko	k1gNnSc4	Finsko
zákony	zákon	k1gInPc1	zákon
–	–	k?	–
krom	krom	k7c2	krom
těch	ten	k3xDgFnPc2	ten
málo	málo	k4c1	málo
oblastí	oblast	k1gFnPc2	oblast
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
nimiž	jenž	k3xRgInPc7	jenž
měli	mít	k5eAaImAgMnP	mít
neomezenou	omezený	k2eNgFnSc4d1	neomezená
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
například	například	k6eAd1	například
cla	clo	k1gNnPc4	clo
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
některé	některý	k3yIgInPc4	některý
pokusy	pokus	k1gInPc4	pokus
nebylo	být	k5eNaImAgNnS	být
Finsko	Finsko	k1gNnSc1	Finsko
nikdy	nikdy	k6eAd1	nikdy
spravováno	spravovat	k5eAaImNgNnS	spravovat
v	v	k7c6	v
ruském	ruský	k2eAgInSc6d1	ruský
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mělo	mít	k5eAaImAgNnS	mít
vlastní	vlastní	k2eAgInPc4d1	vlastní
úřední	úřední	k2eAgInSc4d1	úřední
jazyky	jazyk	k1gInPc4	jazyk
<g/>
:	:	kIx,	:
švédštinu	švédština	k1gFnSc4	švédština
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1883	[number]	k4	1883
finštinu	finština	k1gFnSc4	finština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
vlastních	vlastní	k2eAgInPc2d1	vlastní
zákonů	zákon	k1gInPc2	zákon
a	a	k8xC	a
zákonodárství	zákonodárství	k1gNnSc1	zákonodárství
mělo	mít	k5eAaImAgNnS	mít
Finské	finský	k2eAgNnSc1d1	finské
velkoknížectví	velkoknížectví	k1gNnSc1	velkoknížectví
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
armádu	armáda	k1gFnSc4	armáda
a	a	k8xC	a
kadetní	kadetní	k2eAgFnPc4d1	kadetní
školy	škola	k1gFnPc4	škola
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
finští	finský	k2eAgMnPc1d1	finský
občané	občan	k1gMnPc1	občan
nebyli	být	k5eNaImAgMnP	být
nuceni	nutit	k5eAaImNgMnP	nutit
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
měli	mít	k5eAaImAgMnP	mít
tu	ten	k3xDgFnSc4	ten
možnost	možnost	k1gFnSc4	možnost
<g/>
)	)	kIx)	)
sloužit	sloužit	k5eAaImF	sloužit
v	v	k7c6	v
ruské	ruský	k2eAgFnSc6d1	ruská
carské	carský	k2eAgFnSc6d1	carská
armádě	armáda	k1gFnSc6	armáda
a	a	k8xC	a
mimo	mimo	k7c4	mimo
území	území	k1gNnSc4	území
Finska	Finsko	k1gNnSc2	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
ústředním	ústřední	k2eAgInSc7d1	ústřední
bodem	bod	k1gInSc7	bod
konfliktu	konflikt	k1gInSc2	konflikt
mezi	mezi	k7c7	mezi
Finy	Fin	k1gMnPc7	Fin
a	a	k8xC	a
carskou	carský	k2eAgFnSc7d1	carská
vládou	vláda	k1gFnSc7	vláda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
snažila	snažit	k5eAaImAgFnS	snažit
systematicky	systematicky	k6eAd1	systematicky
zrušit	zrušit	k5eAaPmF	zrušit
samostatnost	samostatnost	k1gFnSc4	samostatnost
finské	finský	k2eAgFnSc2d1	finská
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
podařilo	podařit	k5eAaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Finsko	Finsko	k1gNnSc1	Finsko
mělo	mít	k5eAaImAgNnS	mít
také	také	k9	také
autonomní	autonomní	k2eAgNnSc1d1	autonomní
školství	školství	k1gNnSc1	školství
s	s	k7c7	s
nepovinnou	povinný	k2eNgFnSc7d1	nepovinná
výukou	výuka	k1gFnSc7	výuka
ruského	ruský	k2eAgInSc2d1	ruský
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
vlastními	vlastní	k2eAgInPc7d1	vlastní
učebními	učební	k2eAgInPc7d1	učební
texty	text	k1gInPc7	text
<g/>
.	.	kIx.	.
</s>
<s>
Autonomie	autonomie	k1gFnSc1	autonomie
se	se	k3xPyFc4	se
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
také	také	k6eAd1	také
týkala	týkat	k5eAaImAgFnS	týkat
pošty	pošta	k1gFnSc2	pošta
a	a	k8xC	a
cel	clo	k1gNnPc2	clo
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
oblasti	oblast	k1gFnPc1	oblast
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
integračních	integrační	k2eAgFnPc2d1	integrační
snah	snaha	k1gFnPc2	snaha
trnem	trn	k1gInSc7	trn
v	v	k7c6	v
oku	oko	k1gNnSc6	oko
petrohradských	petrohradský	k2eAgInPc2d1	petrohradský
vládních	vládní	k2eAgInPc2d1	vládní
kruhů	kruh	k1gInPc2	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
protifinským	protifinský	k2eAgInSc7d1	protifinský
argumentem	argument	k1gInSc7	argument
se	se	k3xPyFc4	se
i	i	k9	i
po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
stal	stát	k5eAaPmAgInS	stát
prerogativ	prerogativ	k1gInSc1	prerogativ
o	o	k7c6	o
jednotě	jednota	k1gFnSc6	jednota
a	a	k8xC	a
nedělitelnosti	nedělitelnost	k1gFnSc6	nedělitelnost
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
zavedení	zavedení	k1gNnSc2	zavedení
povinné	povinný	k2eAgFnPc4d1	povinná
vojenské	vojenský	k2eAgFnPc4d1	vojenská
služby	služba	k1gFnPc4	služba
v	v	k7c6	v
ruské	ruský	k2eAgFnSc6d1	ruská
armádě	armáda	k1gFnSc6	armáda
a	a	k8xC	a
zrušení	zrušení	k1gNnSc6	zrušení
nezávislého	závislý	k2eNgInSc2d1	nezávislý
statusu	status	k1gInSc2	status
finské	finský	k2eAgFnSc2d1	finská
armády	armáda	k1gFnSc2	armáda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
další	další	k2eAgFnSc7d1	další
ranou	rána	k1gFnSc7	rána
finské	finský	k2eAgFnSc3d1	finská
autonomii	autonomie	k1gFnSc3	autonomie
zákon	zákon	k1gInSc4	zákon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
umožňoval	umožňovat	k5eAaImAgMnS	umožňovat
ruským	ruský	k2eAgMnSc7d1	ruský
občanům	občan	k1gMnPc3	občan
službu	služba	k1gFnSc4	služba
v	v	k7c6	v
aparátu	aparát	k1gInSc6	aparát
finské	finský	k2eAgFnSc2d1	finská
administrativy	administrativa	k1gFnSc2	administrativa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
měla	mít	k5eAaImAgFnS	mít
ruská	ruský	k2eAgFnSc1d1	ruská
vláda	vláda	k1gFnSc1	vláda
připravený	připravený	k2eAgInSc1d1	připravený
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
rusifikační	rusifikační	k2eAgInSc4d1	rusifikační
program	program	k1gInSc4	program
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nasvědčoval	nasvědčovat	k5eAaImAgInS	nasvědčovat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
s	s	k7c7	s
finskými	finský	k2eAgNnPc7d1	finské
specifiky	specifikon	k1gNnPc7	specifikon
skoncovat	skoncovat	k5eAaPmF	skoncovat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
program	program	k1gInSc1	program
se	se	k3xPyFc4	se
týkal	týkat	k5eAaImAgInS	týkat
především	především	k9	především
finských	finský	k2eAgMnPc2d1	finský
zákonů	zákon	k1gInPc2	zákon
a	a	k8xC	a
celého	celý	k2eAgInSc2d1	celý
právního	právní	k2eAgInSc2d1	právní
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
pošt	pošta	k1gFnPc2	pošta
a	a	k8xC	a
komunikací	komunikace	k1gFnSc7	komunikace
vůbec	vůbec	k9	vůbec
nebo	nebo	k8xC	nebo
zavedení	zavedení	k1gNnSc1	zavedení
dohledu	dohled	k1gInSc2	dohled
nad	nad	k7c7	nad
Helsinskou	helsinský	k2eAgFnSc7d1	Helsinská
univerzitou	univerzita	k1gFnSc7	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Díky	dík	k1gInPc1	dík
průběhu	průběh	k1gInSc2	průběh
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
program	program	k1gInSc1	program
nepodařilo	podařit	k5eNaPmAgNnS	podařit
nikdy	nikdy	k6eAd1	nikdy
realizovat	realizovat	k5eAaBmF	realizovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Finska	Finsko	k1gNnSc2	Finsko
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
finských	finský	k2eAgMnPc2d1	finský
panovníků	panovník	k1gMnPc2	panovník
</s>
</p>
<p>
<s>
Generální	generální	k2eAgMnSc1d1	generální
guvernér	guvernér	k1gMnSc1	guvernér
Finska	Finsko	k1gNnSc2	Finsko
</s>
</p>
<p>
<s>
Finské	finský	k2eAgNnSc1d1	finské
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
KLINGE	KLINGE	kA	KLINGE
Matti	Matti	k1gNnSc1	Matti
<g/>
.	.	kIx.	.
</s>
<s>
Keisarin	Keisarin	k1gInSc1	Keisarin
Suomi	Suo	k1gFnPc7	Suo
<g/>
.	.	kIx.	.
</s>
<s>
Espoo	Espoo	k6eAd1	Espoo
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
951	[number]	k4	951
<g/>
-	-	kIx~	-
<g/>
50	[number]	k4	50
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
682	[number]	k4	682
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Ruský	ruský	k2eAgInSc1d1	ruský
překlad	překlad	k1gInSc1	překlad
<g/>
:	:	kIx,	:
И	И	k?	И
Ф	Ф	k?	Ф
<g/>
.	.	kIx.	.
</s>
<s>
St.	st.	kA	st.
Petersburg	Petersburg	k1gInSc4	Petersburg
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
901841	[number]	k4	901841
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JUSSILA	JUSSILA	kA	JUSSILA
<g/>
,	,	kIx,	,
Osmo	osma	k1gFnSc5	osma
<g/>
;	;	kIx,	;
HENTILÄ	HENTILÄ	kA	HENTILÄ
<g/>
,	,	kIx,	,
Seppo	Seppa	k1gFnSc5	Seppa
<g/>
;	;	kIx,	;
NEVAKIVI	NEVAKIVI	kA	NEVAKIVI
<g/>
,	,	kIx,	,
Jukka	Jukko	k1gNnSc2	Jukko
<g/>
.	.	kIx.	.
</s>
<s>
From	From	k1gMnSc1	From
Grand	grand	k1gMnSc1	grand
Duchy	duch	k1gMnPc4	duch
to	ten	k3xDgNnSc4	ten
a	a	k8xC	a
modern	modern	k1gNnSc4	modern
state	status	k1gInSc5	status
<g/>
:	:	kIx,	:
a	a	k8xC	a
political	politicat	k5eAaPmAgInS	politicat
history	histor	k1gInPc4	histor
of	of	k?	of
Finland	Finland	k1gInSc1	Finland
since	since	k1gFnSc1	since
1809	[number]	k4	1809
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
Hurst	Hurst	k1gMnSc1	Hurst
&	&	k?	&
Company	Compana	k1gFnSc2	Compana
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
383	[number]	k4	383
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
85065	[number]	k4	85065
<g/>
-	-	kIx~	-
<g/>
421	[number]	k4	421
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KMOCHOVÁ	KMOCHOVÁ	kA	KMOCHOVÁ
<g/>
,	,	kIx,	,
Pavla	Pavla	k1gFnSc1	Pavla
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc4	některý
aspekty	aspekt	k1gInPc4	aspekt
života	život	k1gInSc2	život
autonomního	autonomní	k2eAgNnSc2d1	autonomní
Finska	Finsko	k1gNnSc2	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Jinočany	Jinočan	k1gMnPc7	Jinočan
<g/>
:	:	kIx,	:
H	H	kA	H
<g/>
&	&	k?	&
<g/>
H	H	kA	H
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LAINOVÁ	LAINOVÁ	kA	LAINOVÁ
<g/>
,	,	kIx,	,
Radka	Radka	k1gFnSc1	Radka
<g/>
.	.	kIx.	.
</s>
<s>
Finské	finský	k2eAgNnSc1d1	finské
velkoknížectví	velkoknížectví	k1gNnSc1	velkoknížectví
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
carského	carský	k2eAgNnSc2d1	carské
Ruska	Rusko	k1gNnSc2	Rusko
1809	[number]	k4	1809
<g/>
–	–	k?	–
<g/>
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
obzor	obzor	k1gInSc1	obzor
<g/>
.	.	kIx.	.
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
s.	s.	k?	s.
158	[number]	k4	158
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1210	[number]	k4	1210
<g/>
-	-	kIx~	-
<g/>
6097	[number]	k4	6097
<g/>
.	.	kIx.	.
</s>
</p>
