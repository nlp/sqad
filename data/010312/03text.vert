<p>
<s>
Purpurová	purpurový	k2eAgFnSc1d1	purpurová
nebo	nebo	k8xC	nebo
nachová	nachovat	k5eAaBmIp3nS	nachovat
označuje	označovat	k5eAaImIp3nS	označovat
červenou	červený	k2eAgFnSc4d1	červená
barvu	barva	k1gFnSc4	barva
s	s	k7c7	s
výrazným	výrazný	k2eAgInSc7d1	výrazný
podílem	podíl	k1gInSc7	podíl
fialové	fialový	k2eAgNnSc1d1	fialové
<g/>
.	.	kIx.	.
</s>
<s>
Červenější	červený	k2eAgFnSc1d2	červenější
je	být	k5eAaImIp3nS	být
barva	barva	k1gFnSc1	barva
magenta	magenta	k1gFnSc1	magenta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
také	také	k9	také
překládá	překládat	k5eAaImIp3nS	překládat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
purpurová	purpurový	k2eAgFnSc1d1	purpurová
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
u	u	k7c2	u
barevného	barevný	k2eAgInSc2d1	barevný
modelu	model	k1gInSc2	model
CMYK	CMYK	kA	CMYK
a	a	k8xC	a
v	v	k7c6	v
podobném	podobný	k2eAgNnSc6d1	podobné
technickém	technický	k2eAgNnSc6d1	technické
názvosloví	názvosloví	k1gNnSc6	názvosloví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
lat.	lat.	k?	lat.
purpura	purpura	k1gFnSc1	purpura
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
nach	nach	k1gInSc1	nach
<g/>
)	)	kIx)	)
označovalo	označovat	k5eAaImAgNnS	označovat
barvivo	barvivo	k1gNnSc1	barvivo
získávané	získávaný	k2eAgNnSc1d1	získávané
z	z	k7c2	z
některých	některý	k3yIgMnPc2	některý
mořských	mořský	k2eAgMnPc2d1	mořský
plžů	plž	k1gMnPc2	plž
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
ostrankovití	ostrankovitý	k2eAgMnPc1d1	ostrankovitý
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
z	z	k7c2	z
ostranky	ostranka	k1gFnSc2	ostranka
jaderské	jaderský	k2eAgFnSc2d1	Jaderská
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
produkcí	produkce	k1gFnSc7	produkce
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
proslulá	proslulý	k2eAgFnSc1d1	proslulá
syro-palestinská	syroalestinský	k2eAgFnSc1d1	syro-palestinský
pobřežní	pobřežní	k2eAgFnSc1d1	pobřežní
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
podle	podle	k7c2	podle
akkadského	akkadský	k2eAgMnSc2d1	akkadský
a	a	k8xC	a
řeckého	řecký	k2eAgNnSc2d1	řecké
pojmenování	pojmenování	k1gNnSc2	pojmenování
barvy	barva	k1gFnSc2	barva
získala	získat	k5eAaPmAgFnS	získat
názvy	název	k1gInPc1	název
Kanaán	Kanaán	k1gInSc1	Kanaán
a	a	k8xC	a
Foiníkie	Foiníkie	k1gFnSc1	Foiníkie
<g/>
.	.	kIx.	.
</s>
<s>
Nejkvalitnější	kvalitní	k2eAgInSc1d3	nejkvalitnější
týrský	týrský	k2eAgInSc1d1	týrský
purpur	purpur	k1gInSc1	purpur
(	(	kIx(	(
<g/>
císařský	císařský	k2eAgInSc1d1	císařský
purpur	purpur	k1gInSc1	purpur
<g/>
)	)	kIx)	)
oblékala	oblékat	k5eAaImAgFnS	oblékat
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
elita	elita	k1gFnSc1	elita
jako	jako	k8xS	jako
symbol	symbol	k1gInSc1	symbol
společenského	společenský	k2eAgNnSc2d1	společenské
postavení	postavení	k1gNnSc2	postavení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
mluvě	mluva	k1gFnSc6	mluva
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
termín	termín	k1gInSc4	termín
purpurová	purpurový	k2eAgFnSc1d1	purpurová
použit	použít	k5eAaPmNgInS	použít
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
většího	veliký	k2eAgInSc2d2	veliký
počtu	počet	k1gInSc2	počet
barevných	barevný	k2eAgInPc2d1	barevný
odstínů	odstín	k1gInPc2	odstín
<g/>
.	.	kIx.	.
</s>
<s>
Purpurová	purpurový	k2eAgFnSc1d1	purpurová
není	být	k5eNaImIp3nS	být
spektrální	spektrální	k2eAgFnSc1d1	spektrální
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
extraspektrální	extraspektrální	k2eAgFnSc1d1	extraspektrální
barva	barva	k1gFnSc1	barva
–	–	k?	–
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
generována	generovat	k5eAaImNgFnS	generovat
světlem	světlo	k1gNnSc7	světlo
jediné	jediný	k2eAgFnSc2d1	jediná
vlnové	vlnový	k2eAgFnSc2d1	vlnová
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kardinálský	kardinálský	k2eAgInSc1d1	kardinálský
purpur	purpur	k1gInSc1	purpur
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgNnSc4d1	tradiční
symbolické	symbolický	k2eAgNnSc4d1	symbolické
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
hodnost	hodnost	k1gFnSc4	hodnost
kardinála	kardinál	k1gMnSc2	kardinál
podle	podle	k7c2	podle
barvy	barva	k1gFnSc2	barva
šatu	šat	k1gInSc2	šat
(	(	kIx(	(
<g/>
mozetta	mozetta	k1gFnSc1	mozetta
<g/>
,	,	kIx,	,
klobouk	klobouk	k1gInSc1	klobouk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
červené	červený	k2eAgFnPc1d1	červená
<g/>
.	.	kIx.	.
</s>
</p>
