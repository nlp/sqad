<s>
Slon	slon	k1gMnSc1	slon
indický	indický	k2eAgMnSc1d1	indický
(	(	kIx(	(
<g/>
Elephas	Elephas	k1gMnSc1	Elephas
maximus	maximus	k1gMnSc1	maximus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jiho-	jiho-	k?	jiho-
a	a	k8xC	a
jihovýchodoasijský	jihovýchodoasijský	k2eAgMnSc1d1	jihovýchodoasijský
slon	slon	k1gMnSc1	slon
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
3,5	[number]	k4	3,5
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
tří	tři	k4xCgMnPc2	tři
žijících	žijící	k2eAgMnPc2d1	žijící
druhů	druh	k1gMnPc2	druh
slona	slon	k1gMnSc2	slon
a	a	k8xC	a
jediný	jediný	k2eAgInSc1d1	jediný
nevymřelý	vymřelý	k2eNgInSc1d1	vymřelý
druh	druh	k1gInSc1	druh
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Elephas	Elephasa	k1gFnPc2	Elephasa
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
největšího	veliký	k2eAgMnSc4d3	veliký
suchozemského	suchozemský	k2eAgMnSc4d1	suchozemský
živočicha	živočich	k1gMnSc4	živočich
žijícího	žijící	k2eAgMnSc4d1	žijící
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
Bangladéši	Bangladéš	k1gInSc6	Bangladéš
<g/>
,	,	kIx,	,
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
na	na	k7c6	na
Srí	Srí	k1gFnSc6	Srí
Lance	lance	k1gNnSc2	lance
<g/>
,	,	kIx,	,
v	v	k7c6	v
Indočíně	Indočína	k1gFnSc6	Indočína
a	a	k8xC	a
v	v	k7c6	v
částech	část	k1gFnPc6	část
Nepálu	Nepál	k1gInSc2	Nepál
<g/>
,	,	kIx,	,
Indonésie	Indonésie	k1gFnSc2	Indonésie
a	a	k8xC	a
Thajska	Thajsko	k1gNnSc2	Thajsko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
,	,	kIx,	,
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
žije	žít	k5eAaImIp3nS	žít
odhadem	odhad	k1gInSc7	odhad
40-50	[number]	k4	40-50
tisíc	tisíc	k4xCgInPc2	tisíc
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
menší	malý	k2eAgMnSc1d2	menší
než	než	k8xS	než
jeho	jeho	k3xOp3gMnSc1	jeho
africký	africký	k2eAgMnSc1d1	africký
příbuzný	příbuzný	k1gMnSc1	příbuzný
<g/>
,	,	kIx,	,
nejjednodušším	jednoduchý	k2eAgInSc7d3	nejjednodušší
rozlišovacím	rozlišovací	k2eAgInSc7d1	rozlišovací
znakem	znak	k1gInSc7	znak
je	být	k5eAaImIp3nS	být
velikost	velikost	k1gFnSc1	velikost
uší	ucho	k1gNnPc2	ucho
-	-	kIx~	-
slon	slon	k1gMnSc1	slon
indický	indický	k2eAgMnSc1d1	indický
je	být	k5eAaImIp3nS	být
má	mít	k5eAaImIp3nS	mít
výrazně	výrazně	k6eAd1	výrazně
menší	malý	k2eAgFnSc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Stoličky	stolička	k1gFnPc1	stolička
mají	mít	k5eAaImIp3nP	mít
tvar	tvar	k1gInSc4	tvar
podobný	podobný	k2eAgInSc4d1	podobný
vymřelým	vymřelý	k2eAgMnPc3d1	vymřelý
mamutům	mamut	k1gMnPc3	mamut
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
na	na	k7c4	na
bližší	blízký	k2eAgFnSc4d2	bližší
příbuznost	příbuznost	k1gFnSc4	příbuznost
těchto	tento	k3xDgInPc2	tento
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Slon	slon	k1gMnSc1	slon
indický	indický	k2eAgMnSc1d1	indický
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
chobotu	chobot	k1gInSc6	chobot
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc1	jeden
prstík	prstík	k1gInSc1	prstík
(	(	kIx(	(
<g/>
výběžek	výběžek	k1gInSc1	výběžek
na	na	k7c6	na
konci	konec	k1gInSc6	konec
chobotu	chobot	k1gInSc2	chobot
sloužící	sloužící	k1gFnSc2	sloužící
k	k	k7c3	k
uchopení	uchopení	k1gNnSc3	uchopení
menších	malý	k2eAgInPc2d2	menší
předmětů	předmět	k1gInPc2	předmět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slon	slon	k1gMnSc1	slon
indický	indický	k2eAgMnSc1d1	indický
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
klenutou	klenutý	k2eAgFnSc4d1	klenutá
páteř	páteř	k1gFnSc4	páteř
než	než	k8xS	než
slon	slon	k1gMnSc1	slon
africký	africký	k2eAgMnSc1d1	africký
<g/>
,	,	kIx,	,
na	na	k7c6	na
každém	každý	k3xTgNnSc6	každý
zadním	zadní	k2eAgNnSc6d1	zadní
chodidle	chodidlo	k1gNnSc6	chodidlo
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgInPc4	čtyři
nehty	nehet	k1gInPc4	nehet
místo	místo	k7c2	místo
tří	tři	k4xCgFnPc2	tři
<g/>
,	,	kIx,	,
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
dvě	dva	k4xCgFnPc1	dva
vybouleniny	vyboulenina	k1gFnPc1	vyboulenina
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
19	[number]	k4	19
párů	pár	k1gInPc2	pár
žeber	žebro	k1gNnPc2	žebro
(	(	kIx(	(
<g/>
slon	slon	k1gMnSc1	slon
africký	africký	k2eAgMnSc1d1	africký
jich	on	k3xPp3gMnPc2	on
má	mít	k5eAaImIp3nS	mít
21	[number]	k4	21
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samicím	samice	k1gFnPc3	samice
obvykle	obvykle	k6eAd1	obvykle
nenarůstají	narůstat	k5eNaImIp3nP	narůstat
kly	kel	k1gInPc1	kel
a	a	k8xC	a
když	když	k8xS	když
ano	ano	k9	ano
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
stěží	stěží	k6eAd1	stěží
viditelné	viditelný	k2eAgFnPc1d1	viditelná
i	i	k8xC	i
když	když	k8xS	když
slon	slon	k1gMnSc1	slon
otevře	otevřít	k5eAaPmIp3nS	otevřít
tlamu	tlama	k1gFnSc4	tlama
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
také	také	k9	také
mohou	moct	k5eAaImIp3nP	moct
postrádat	postrádat	k5eAaImF	postrádat
kly	kel	k1gInPc1	kel
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
populaci	populace	k1gFnSc6	populace
na	na	k7c6	na
Srí	Srí	k1gFnSc6	Srí
Lance	lance	k1gNnSc2	lance
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
slona	slon	k1gMnSc2	slon
afrického	africký	k2eAgInSc2d1	africký
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
používá	používat	k5eAaImIp3nS	používat
přední	přední	k2eAgFnPc4d1	přední
končetiny	končetina	k1gFnPc4	končetina
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
k	k	k7c3	k
manipulaci	manipulace	k1gFnSc3	manipulace
se	se	k3xPyFc4	se
zeminou	zemina	k1gFnSc7	zemina
<g/>
,	,	kIx,	,
umí	umět	k5eAaImIp3nS	umět
společně	společně	k6eAd1	společně
s	s	k7c7	s
trupem	trup	k1gInSc7	trup
lépe	dobře	k6eAd2	dobře
využívat	využívat	k5eAaPmF	využívat
přední	přední	k2eAgFnPc4d1	přední
končetiny	končetina	k1gFnPc4	končetina
k	k	k7c3	k
manipulaci	manipulace	k1gFnSc3	manipulace
s	s	k7c7	s
objekty	objekt	k1gInPc7	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Slon	slon	k1gMnSc1	slon
indický	indický	k2eAgMnSc1d1	indický
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
2-4	[number]	k4	2-4
metry	metr	k1gInPc4	metr
a	a	k8xC	a
váží	vážit	k5eAaImIp3nP	vážit
3-5	[number]	k4	3-5
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
ocas	ocas	k1gInSc1	ocas
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
délky	délka	k1gFnSc2	délka
1	[number]	k4	1
m	m	kA	m
až	až	k9	až
1,5	[number]	k4	1,5
m.	m.	k?	m.
Velikost	velikost	k1gFnSc1	velikost
slonů	slon	k1gMnPc2	slon
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
zveličována	zveličován	k2eAgFnSc1d1	zveličována
<g/>
.	.	kIx.	.
</s>
<s>
Nejtěžší	těžký	k2eAgMnSc1d3	nejtěžší
zaznamenaný	zaznamenaný	k2eAgMnSc1d1	zaznamenaný
jedinec	jedinec	k1gMnSc1	jedinec
vážil	vážit	k5eAaImAgMnS	vážit
8	[number]	k4	8
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
vysoký	vysoký	k2eAgMnSc1d1	vysoký
3,35	[number]	k4	3,35
metru	metr	k1gInSc2	metr
a	a	k8xC	a
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
8,06	[number]	k4	8,06
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
menších	malý	k2eAgFnPc6d2	menší
skupinách	skupina	k1gFnPc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Traduje	tradovat	k5eAaImIp3nS	tradovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
slon	slon	k1gMnSc1	slon
má	mít	k5eAaImIp3nS	mít
výbornou	výborný	k2eAgFnSc4d1	výborná
paměť	paměť	k1gFnSc4	paměť
<g/>
,	,	kIx,	,
pamatuje	pamatovat	k5eAaImIp3nS	pamatovat
si	se	k3xPyFc3	se
i	i	k9	i
po	po	k7c6	po
několika	několik	k4yIc6	několik
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
našel	najít	k5eAaPmAgMnS	najít
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
pokusy	pokus	k1gInPc1	pokus
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
neprokázaly	prokázat	k5eNaPmAgFnP	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
lepší	dobrý	k2eAgFnSc4d2	lepší
paměť	paměť	k1gFnSc4	paměť
než	než	k8xS	než
např.	např.	kA	např.
pes	pes	k1gMnSc1	pes
nebo	nebo	k8xC	nebo
kočka	kočka	k1gFnSc1	kočka
<g/>
.	.	kIx.	.
</s>
<s>
Stáda	stádo	k1gNnPc1	stádo
slonů	slon	k1gMnPc2	slon
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
migrují	migrovat	k5eAaImIp3nP	migrovat
po	po	k7c6	po
přesně	přesně	k6eAd1	přesně
určených	určený	k2eAgFnPc6d1	určená
trasách	trasa	k1gFnPc6	trasa
<g/>
.	.	kIx.	.
</s>
<s>
Úkolem	úkol	k1gInSc7	úkol
nejstaršího	starý	k2eAgMnSc4d3	nejstarší
slona	slon	k1gMnSc4	slon
je	být	k5eAaImIp3nS	být
pamatovat	pamatovat	k5eAaImF	pamatovat
si	se	k3xPyFc3	se
a	a	k8xC	a
vyhledat	vyhledat	k5eAaPmF	vyhledat
tradiční	tradiční	k2eAgFnPc4d1	tradiční
migrační	migrační	k2eAgFnPc4d1	migrační
trasy	trasa	k1gFnPc4	trasa
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářstvím	hospodářství	k1gNnSc7	hospodářství
umístěným	umístěný	k2eAgNnSc7d1	umístěné
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
trase	trasa	k1gFnSc6	trasa
vzniká	vznikat	k5eAaImIp3nS	vznikat
škoda	škoda	k1gFnSc1	škoda
na	na	k7c6	na
úrodě	úroda	k1gFnSc6	úroda
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
,	,	kIx,	,
že	že	k8xS	že
slon	slon	k1gMnSc1	slon
zemře	zemřít	k5eAaPmIp3nS	zemřít
při	při	k7c6	při
bojích	boj	k1gInPc6	boj
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
jedinci	jedinec	k1gMnPc1	jedinec
jinak	jinak	k6eAd1	jinak
nemají	mít	k5eNaImIp3nP	mít
žádné	žádný	k3yNgMnPc4	žádný
predátory	predátor	k1gMnPc4	predátor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
stát	stát	k5eAaPmF	stát
kořistí	kořist	k1gFnSc7	kořist
tygrů	tygr	k1gMnPc2	tygr
<g/>
.	.	kIx.	.
</s>
<s>
Sloni	slon	k1gMnPc1	slon
žijí	žít	k5eAaImIp3nP	žít
průměrně	průměrně	k6eAd1	průměrně
60	[number]	k4	60
let	léto	k1gNnPc2	léto
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
a	a	k8xC	a
80	[number]	k4	80
let	léto	k1gNnPc2	léto
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Denně	denně	k6eAd1	denně
zkonzumují	zkonzumovat	k5eAaPmIp3nP	zkonzumovat
potravu	potrava	k1gFnSc4	potrava
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
10	[number]	k4	10
%	%	kIx~	%
svého	svůj	k3xOyFgNnSc2	svůj
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
u	u	k7c2	u
dospělých	dospělí	k1gMnPc2	dospělí
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
170-230	[number]	k4	170-230
kilogramů	kilogram	k1gInPc2	kilogram
<g/>
.	.	kIx.	.
</s>
<s>
Potřebují	potřebovat	k5eAaImIp3nP	potřebovat
denně	denně	k6eAd1	denně
80-200	[number]	k4	80-200
litrů	litr	k1gInPc2	litr
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
využijí	využít	k5eAaPmIp3nP	využít
ke	k	k7c3	k
koupání	koupání	k1gNnSc3	koupání
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
komunikaci	komunikace	k1gFnSc3	komunikace
využívají	využívat	k5eAaImIp3nP	využívat
infrazvuk	infrazvuk	k1gInSc1	infrazvuk
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc4	tento
fakt	fakt	k1gInSc4	fakt
poprvé	poprvé	k6eAd1	poprvé
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
indický	indický	k2eAgMnSc1d1	indický
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
Madhaviah	Madhaviah	k1gMnSc1	Madhaviah
Krishnan	Krishnan	k1gMnSc1	Krishnan
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
tento	tento	k3xDgInSc4	tento
jev	jev	k1gInSc4	jev
studovala	studovat	k5eAaImAgFnS	studovat
Katherine	Katherin	k1gInSc5	Katherin
Payne	Payn	k1gInSc5	Payn
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
samotáři	samotář	k1gMnPc1	samotář
a	a	k8xC	a
v	v	k7c6	v
době	doba	k1gFnSc6	doba
říje	říje	k1gFnSc2	říje
bojují	bojovat	k5eAaImIp3nP	bojovat
o	o	k7c4	o
samice	samice	k1gFnPc4	samice
<g/>
.	.	kIx.	.
</s>
<s>
Mladší	mladý	k2eAgMnPc1d2	mladší
samci	samec	k1gMnPc1	samec
mohou	moct	k5eAaImIp3nP	moct
vytvářet	vytvářet	k5eAaImF	vytvářet
menší	malý	k2eAgFnPc4d2	menší
skupinky	skupinka	k1gFnPc4	skupinka
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
pohlavně	pohlavně	k6eAd1	pohlavně
dospívají	dospívat	k5eAaImIp3nP	dospívat
v	v	k7c6	v
patnácti	patnáct	k4xCc6	patnáct
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
dostávají	dostávat	k5eAaImIp3nP	dostávat
do	do	k7c2	do
říje	říje	k1gFnSc2	říje
zvané	zvaný	k2eAgFnSc2d1	zvaná
"	"	kIx"	"
<g/>
musth	mustha	k1gFnPc2	mustha
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
je	být	k5eAaImIp3nS	být
hladina	hladina	k1gFnSc1	hladina
testosteronu	testosteron	k1gInSc2	testosteron
60	[number]	k4	60
<g/>
krát	krát	k6eAd1	krát
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
normálně	normálně	k6eAd1	normálně
a	a	k8xC	a
slon	slon	k1gMnSc1	slon
je	být	k5eAaImIp3nS	být
extrémně	extrémně	k6eAd1	extrémně
agresivní	agresivní	k2eAgNnSc1d1	agresivní
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
spánkových	spánkový	k2eAgFnPc2d1	spánková
potních	potní	k2eAgFnPc2d1	potní
žláz	žláza	k1gFnPc2	žláza
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
sekret	sekret	k1gInSc1	sekret
obsahující	obsahující	k2eAgInPc1d1	obsahující
feromony	feromon	k1gInPc1	feromon
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
skupinách	skupina	k1gFnPc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
matriarchální	matriarchální	k2eAgFnSc1d1	matriarchální
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
je	být	k5eAaImIp3nS	být
vedena	vést	k5eAaImNgFnS	vést
nejstarší	starý	k2eAgFnSc7d3	nejstarší
samicí	samice	k1gFnSc7	samice
<g/>
.	.	kIx.	.
</s>
<s>
Dospívají	dospívat	k5eAaImIp3nP	dospívat
v	v	k7c6	v
9-15	[number]	k4	9-15
roku	rok	k1gInSc2	rok
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Březost	březost	k1gFnSc1	březost
trvá	trvat	k5eAaImIp3nS	trvat
18-22	[number]	k4	18-22
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
rodí	rodit	k5eAaImIp3nS	rodit
jedno	jeden	k4xCgNnSc1	jeden
nebo	nebo	k8xC	nebo
výjimečně	výjimečně	k6eAd1	výjimečně
dvě	dva	k4xCgNnPc4	dva
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Mládě	mládě	k1gNnSc1	mládě
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
vyvinuté	vyvinutý	k2eAgFnPc4d1	vyvinutá
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
měsíci	měsíc	k1gInSc6	měsíc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
v	v	k7c6	v
děloze	děloha	k1gFnSc6	děloha
aby	aby	kYmCp3nS	aby
vyrostlo	vyrůst	k5eAaPmAgNnS	vyrůst
a	a	k8xC	a
matka	matka	k1gFnSc1	matka
jej	on	k3xPp3gMnSc4	on
mohla	moct	k5eAaImAgFnS	moct
krmit	krmit	k5eAaImF	krmit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
narození	narození	k1gNnSc6	narození
mládě	mládě	k1gNnSc1	mládě
váží	vážit	k5eAaImIp3nP	vážit
kolem	kolem	k7c2	kolem
100	[number]	k4	100
kg	kg	kA	kg
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
kojeno	kojit	k5eAaImNgNnS	kojit
2-3	[number]	k4	2-3
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
ve	v	k7c6	v
stádě	stádo	k1gNnSc6	stádo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dospělí	dospělý	k2eAgMnPc1d1	dospělý
samci	samec	k1gMnPc1	samec
jsou	být	k5eAaImIp3nP	být
odháněni	odhánět	k5eAaImNgMnP	odhánět
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
feromony	feromon	k1gInPc4	feromon
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc7d1	hlavní
složkou	složka	k1gFnSc7	složka
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
Z	z	k7c2	z
<g/>
)	)	kIx)	)
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
-dodecen-	odecen-	k?	-dodecen-
<g/>
1	[number]	k4	1
<g/>
-yl	l	k?	-yl
acetát	acetát	k1gInSc1	acetát
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
látka	látka	k1gFnSc1	látka
byla	být	k5eAaImAgFnS	být
nalezena	naleznout	k5eAaPmNgFnS	naleznout
také	také	k9	také
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
druhů	druh	k1gInPc2	druh
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Slona	slon	k1gMnSc2	slon
indického	indický	k2eAgMnSc2d1	indický
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
chovají	chovat	k5eAaImIp3nP	chovat
<g/>
:	:	kIx,	:
ZOO	zoo	k1gFnSc1	zoo
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
ZOO	zoo	k1gFnSc1	zoo
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
ZOO	zoo	k1gFnSc1	zoo
Liberec	Liberec	k1gInSc1	Liberec
a	a	k8xC	a
ZOO	zoo	k1gFnSc1	zoo
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Rozmnožování	rozmnožování	k1gNnSc1	rozmnožování
slonů	slon	k1gMnPc2	slon
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
několika	několik	k4yIc2	několik
důvodů	důvod	k1gInPc2	důvod
<g/>
:	:	kIx,	:
Je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
obtížné	obtížný	k2eAgNnSc1d1	obtížné
sestavit	sestavit	k5eAaPmF	sestavit
věkově	věkově	k6eAd1	věkově
vyváženou	vyvážený	k2eAgFnSc4d1	vyvážená
skupinu	skupina	k1gFnSc4	skupina
Také	také	k9	také
je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
získat	získat	k5eAaPmF	získat
vhodného	vhodný	k2eAgInSc2d1	vhodný
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
plodného	plodný	k2eAgMnSc4d1	plodný
samce	samec	k1gMnSc4	samec
Také	také	k9	také
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
slonice	slonice	k1gFnSc1	slonice
zabřezla	zabřeznout	k5eAaPmAgFnS	zabřeznout
nejpozději	pozdě	k6eAd3	pozdě
ve	v	k7c6	v
dvaceti	dvacet	k4xCc6	dvacet
letech	let	k1gInPc6	let
V	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
SR	SR	kA	SR
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
podařilo	podařit	k5eAaPmAgNnS	podařit
odchovat	odchovat	k5eAaPmF	odchovat
mládě	mládě	k1gNnSc1	mládě
slona	slon	k1gMnSc2	slon
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
po	po	k7c6	po
umělé	umělý	k2eAgFnSc6d1	umělá
inseminaci	inseminace	k1gFnSc6	inseminace
v	v	k7c6	v
ZOO	zoo	k1gFnSc6	zoo
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
ale	ale	k8xC	ale
mládě	mládě	k1gNnSc1	mládě
se	se	k3xPyFc4	se
narodilo	narodit	k5eAaPmAgNnS	narodit
mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnPc4	první
živá	živý	k2eAgNnPc4d1	živé
slůňata	slůně	k1gNnPc4	slůně
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
v	v	k7c6	v
zoologické	zoologický	k2eAgFnSc6d1	zoologická
zahradě	zahrada	k1gFnSc6	zahrada
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
samicím	samice	k1gFnPc3	samice
Vishesh	Vishesh	k1gInSc4	Vishesh
(	(	kIx(	(
<g/>
14	[number]	k4	14
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
porod	porod	k1gInSc1	porod
11	[number]	k4	11
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
)	)	kIx)	)
a	a	k8xC	a
Johti	Johť	k1gFnSc2	Johť
(	(	kIx(	(
<g/>
44	[number]	k4	44
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
porod	porod	k1gInSc1	porod
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mládě	mládě	k1gNnSc1	mládě
od	od	k7c2	od
samice	samice	k1gFnSc2	samice
Vishesh	Vishesh	k1gMnSc1	Vishesh
-	-	kIx~	-
sameček	sameček	k1gMnSc1	sameček
však	však	k9	však
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
po	po	k7c6	po
2	[number]	k4	2
měsících	měsíc	k1gInPc6	měsíc
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
oslabení	oslabení	k1gNnSc2	oslabení
imunity	imunita	k1gFnSc2	imunita
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
období	období	k1gNnSc2	období
roku	rok	k1gInSc2	rok
jsou	být	k5eAaImIp3nP	být
sloni	slon	k1gMnPc1	slon
indičtí	indický	k2eAgMnPc1d1	indický
plaší	plašit	k5eAaImIp3nP	plašit
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
utečou	utéct	k5eAaPmIp3nP	utéct
než	než	k8xS	než
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
pouštěli	pouštět	k5eAaImAgMnP	pouštět
do	do	k7c2	do
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Osamělí	osamělý	k2eAgMnPc1d1	osamělý
tuláci	tulák	k1gMnPc1	tulák
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
výjimkou	výjimka	k1gFnSc7	výjimka
tohoto	tento	k3xDgNnSc2	tento
pravidla	pravidlo	k1gNnSc2	pravidlo
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
neočekávaně	očekávaně	k6eNd1	očekávaně
útočí	útočit	k5eAaImIp3nS	útočit
na	na	k7c4	na
kolemjdoucí	kolemjdoucí	k1gMnPc4	kolemjdoucí
<g/>
.	.	kIx.	.
</s>
<s>
Samotářští	samotářský	k2eAgMnPc1d1	samotářský
sloni	slon	k1gMnPc1	slon
si	se	k3xPyFc3	se
někdy	někdy	k6eAd1	někdy
najdou	najít	k5eAaPmIp3nP	najít
místo	místo	k1gNnSc4	místo
blízko	blízko	k7c2	blízko
silnice	silnice	k1gFnSc2	silnice
a	a	k8xC	a
způsobí	způsobit	k5eAaPmIp3nS	způsobit
její	její	k3xOp3gFnSc4	její
neprůjezdnost	neprůjezdnost	k1gFnSc4	neprůjezdnost
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
s	s	k7c7	s
potomky	potomek	k1gMnPc7	potomek
jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
nebezpečné	bezpečný	k2eNgFnPc1d1	nebezpečná
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
někdo	někdo	k3yInSc1	někdo
přiblíží	přiblížit	k5eAaPmIp3nS	přiblížit
<g/>
.	.	kIx.	.
</s>
<s>
Slon	slon	k1gMnSc1	slon
indický	indický	k2eAgMnSc1d1	indický
útočí	útočit	k5eAaImIp3nS	útočit
s	s	k7c7	s
chobotem	chobot	k1gInSc7	chobot
pevně	pevně	k6eAd1	pevně
zkrouceným	zkroucený	k2eAgMnPc3d1	zkroucený
nahoru	nahoru	k6eAd1	nahoru
<g/>
.	.	kIx.	.
</s>
<s>
Útočí	útočit	k5eAaImIp3nS	útočit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
nepřítele	nepřítel	k1gMnSc4	nepřítel
udupe	udupat	k5eAaPmIp3nS	udupat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přitlačí	přitlačit	k5eAaPmIp3nS	přitlačit
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
pomocí	pomoc	k1gFnPc2	pomoc
klů	kel	k1gInPc2	kel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
říje	říje	k1gFnSc2	říje
je	být	k5eAaImIp3nS	být
slon	slon	k1gMnSc1	slon
velmi	velmi	k6eAd1	velmi
nebezpečný	bezpečný	k2eNgInSc4d1	nebezpečný
nejen	nejen	k6eAd1	nejen
k	k	k7c3	k
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ke	k	k7c3	k
svým	svůj	k3xOyFgMnPc3	svůj
společníkům	společník	k1gMnPc3	společník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
zpozorování	zpozorování	k1gNnSc6	zpozorování
tohoto	tento	k3xDgNnSc2	tento
chování	chování	k1gNnSc2	chování
pevně	pevně	k6eAd1	pevně
zajištěni	zajištěn	k2eAgMnPc1d1	zajištěn
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
neštěstí	neštěstí	k1gNnSc3	neštěstí
<g/>
.	.	kIx.	.
</s>
<s>
Bývají	bývat	k5eAaImIp3nP	bývat
využívána	využíván	k2eAgNnPc4d1	využíváno
anestetika	anestetikum	k1gNnPc4	anestetikum
<g/>
.	.	kIx.	.
</s>
<s>
Sloni	slon	k1gMnPc1	slon
byli	být	k5eAaImAgMnP	být
zkroceni	zkrotit	k5eAaPmNgMnP	zkrotit
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
jejich	jejich	k3xOp3gFnSc3	jejich
schopnosti	schopnost	k1gFnSc3	schopnost
pracovat	pracovat	k5eAaImF	pracovat
podle	podle	k7c2	podle
příkazu	příkaz	k1gInSc2	příkaz
jsou	být	k5eAaImIp3nP	být
obzvláště	obzvláště	k6eAd1	obzvláště
užiteční	užitečný	k2eAgMnPc1d1	užitečný
pro	pro	k7c4	pro
přenášení	přenášení	k1gNnSc4	přenášení
těžkých	těžký	k2eAgFnPc2d1	těžká
věcí	věc	k1gFnPc2	věc
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
je	on	k3xPp3gFnPc4	on
využívali	využívat	k5eAaImAgMnP	využívat
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
přenášení	přenášení	k1gNnSc4	přenášení
dřeva	dřevo	k1gNnSc2	dřevo
v	v	k7c6	v
pralese	prales	k1gInSc6	prales
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
práce	práce	k1gFnSc2	práce
byli	být	k5eAaImAgMnP	být
využíváni	využívat	k5eAaPmNgMnP	využívat
také	také	k9	také
ve	v	k7c6	v
válkách	válka	k1gFnPc6	válka
<g/>
,	,	kIx,	,
při	při	k7c6	při
obřadech	obřad	k1gInPc6	obřad
a	a	k8xC	a
jako	jako	k9	jako
dopravní	dopravní	k2eAgInSc4d1	dopravní
prostředek	prostředek	k1gInSc4	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Lovci	lovec	k1gMnPc1	lovec
je	on	k3xPp3gMnPc4	on
využívali	využívat	k5eAaPmAgMnP	využívat
pro	pro	k7c4	pro
pohyb	pohyb	k1gInSc4	pohyb
po	po	k7c6	po
rozličném	rozličný	k2eAgInSc6d1	rozličný
terénu	terén	k1gInSc6	terén
<g/>
,	,	kIx,	,
představovali	představovat	k5eAaImAgMnP	představovat
lepší	dobrý	k2eAgInSc4d2	lepší
prostředek	prostředek	k1gInSc4	prostředek
k	k	k7c3	k
lovu	lov	k1gInSc3	lov
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
záznamy	záznam	k1gInPc1	záznam
o	o	k7c4	o
domestikaci	domestikace	k1gFnSc4	domestikace
slonů	slon	k1gMnPc2	slon
indických	indický	k2eAgFnPc2d1	indická
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
harappské	harappský	k2eAgFnSc2d1	harappská
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Sloni	slon	k1gMnPc1	slon
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
historie	historie	k1gFnSc2	historie
stali	stát	k5eAaPmAgMnP	stát
obléhacím	obléhací	k2eAgInSc7d1	obléhací
prostředkem	prostředek	k1gInSc7	prostředek
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
osedláni	osedlat	k5eAaPmNgMnP	osedlat
ve	v	k7c6	v
válkách	válka	k1gFnPc6	válka
a	a	k8xC	a
stali	stát	k5eAaPmAgMnP	stát
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
pracovníci	pracovník	k1gMnPc1	pracovník
<g/>
.	.	kIx.	.
</s>
<s>
Hrají	hrát	k5eAaImIp3nP	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
kontinentu	kontinent	k1gInSc2	kontinent
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
uváděni	uvádět	k5eAaImNgMnP	uvádět
v	v	k7c6	v
Džátacích	Džátace	k1gFnPc6	Džátace
a	a	k8xC	a
Panchatantře	Panchatantra	k1gFnSc6	Panchatantra
<g/>
.	.	kIx.	.
</s>
<s>
Hinduistický	hinduistický	k2eAgMnSc1d1	hinduistický
bůh	bůh	k1gMnSc1	bůh
Ganéša	Ganéša	k1gMnSc1	Ganéša
má	mít	k5eAaImIp3nS	mít
hlavu	hlava	k1gFnSc4	hlava
slona	slon	k1gMnSc2	slon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kérale	Kérala	k1gFnSc6	Kérala
byli	být	k5eAaImAgMnP	být
využíváni	využíván	k2eAgMnPc1d1	využíván
při	při	k7c6	při
průvodech	průvod	k1gInPc6	průvod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
ozdobeni	ozdoben	k2eAgMnPc1d1	ozdoben
slavnostním	slavnostní	k2eAgInSc7d1	slavnostní
výstrojem	výstroj	k1gInSc7	výstroj
<g/>
.	.	kIx.	.
</s>
<s>
Skoro	skoro	k6eAd1	skoro
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
indických	indický	k2eAgFnPc6d1	indická
válkách	válka	k1gFnPc6	válka
byli	být	k5eAaImAgMnP	být
využíváni	využíván	k2eAgMnPc1d1	využíván
jako	jako	k8xS	jako
váleční	váleční	k2eAgMnPc1d1	váleční
sloni	slon	k1gMnPc1	slon
<g/>
.	.	kIx.	.
</s>
