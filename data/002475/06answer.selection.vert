<s>
Jean	Jean	k1gMnSc1	Jean
Arthur	Arthur	k1gMnSc1	Arthur
Nicolas	Nicolas	k1gMnSc1	Nicolas
Rimbaud	Rimbaud	k1gMnSc1	Rimbaud
[	[	kIx(	[
<g/>
rembo	remba	k1gFnSc5	remba
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1854	[number]	k4	1854
Charleville-Méziè	Charleville-Méziè	k1gFnPc2	Charleville-Méziè
-	-	kIx~	-
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1891	[number]	k4	1891
Marseille	Marseille	k1gFnSc1	Marseille
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
tzv.	tzv.	kA	tzv.
prokletých	prokletý	k2eAgMnPc2d1	prokletý
básníků	básník	k1gMnPc2	básník
<g/>
.	.	kIx.	.
</s>
