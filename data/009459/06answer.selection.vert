<s>
Západní	západní	k2eAgFnPc1d1	západní
polokoule	polokoule	k1gFnPc1	polokoule
nebo	nebo	k8xC	nebo
západní	západní	k2eAgFnSc1d1	západní
hemisféra	hemisféra	k1gFnSc1	hemisféra
je	být	k5eAaImIp3nS	být
pojem	pojem	k1gInSc1	pojem
označující	označující	k2eAgInSc1d1	označující
polovinu	polovina	k1gFnSc4	polovina
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
západně	západně	k6eAd1	západně
od	od	k7c2	od
nultého	nultý	k4xOgInSc2	nultý
poledníku	poledník	k1gInSc2	poledník
<g/>
.	.	kIx.	.
</s>
