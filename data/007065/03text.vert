<s>
Divadlo	divadlo	k1gNnSc1	divadlo
Globe	globus	k1gInSc5	globus
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1599	[number]	k4	1599
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
spjato	spjat	k2eAgNnSc1d1	spjato
s	s	k7c7	s
osobou	osoba	k1gFnSc7	osoba
Williama	William	k1gMnSc2	William
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
členem	člen	k1gMnSc7	člen
a	a	k8xC	a
podílníkem	podílník	k1gMnSc7	podílník
divadelní	divadelní	k2eAgFnSc2d1	divadelní
společnosti	společnost	k1gFnSc2	společnost
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
Služebníci	služebník	k1gMnPc1	služebník
lorda	lord	k1gMnSc2	lord
komořího	komoří	k1gMnSc2	komoří
<g/>
.	.	kIx.	.
</s>
<s>
Svá	svůj	k3xOyFgNnPc4	svůj
představení	představení	k1gNnPc4	představení
hráli	hrát	k5eAaImAgMnP	hrát
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
Jamese	Jamese	k1gFnSc2	Jamese
Burbage	Burbag	k1gFnSc2	Burbag
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1597	[number]	k4	1597
jim	on	k3xPp3gMnPc3	on
však	však	k9	však
nájem	nájem	k1gInSc4	nájem
na	na	k7c4	na
divadlo	divadlo	k1gNnSc4	divadlo
vypršel	vypršet	k5eAaPmAgInS	vypršet
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
velké	velký	k2eAgFnPc4d1	velká
potíže	potíž	k1gFnPc4	potíž
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
něco	něco	k3yInSc4	něco
podniknout	podniknout	k5eAaPmF	podniknout
<g/>
.	.	kIx.	.
</s>
<s>
Divadelní	divadelní	k2eAgFnSc1d1	divadelní
společnost	společnost	k1gFnSc1	společnost
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
ukrást	ukrást	k5eAaPmF	ukrást
své	svůj	k3xOyFgNnSc4	svůj
vlastní	vlastní	k2eAgNnSc4d1	vlastní
divadlo	divadlo	k1gNnSc4	divadlo
na	na	k7c6	na
základě	základ	k1gInSc6	základ
prošlé	prošlý	k2eAgFnSc2d1	prošlá
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
psalo	psát	k5eAaImAgNnS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
budou	být	k5eAaImBp3nP	být
chtít	chtít	k5eAaImF	chtít
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
divadlo	divadlo	k1gNnSc4	divadlo
rozebrat	rozebrat	k5eAaPmF	rozebrat
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc4	divadlo
přesunuli	přesunout	k5eAaPmAgMnP	přesunout
na	na	k7c4	na
jižní	jižní	k2eAgInSc4d1	jižní
břeh	břeh	k1gInSc4	břeh
řeky	řeka	k1gFnSc2	řeka
Temže	Temže	k1gFnSc2	Temže
a	a	k8xC	a
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
ho	on	k3xPp3gMnSc4	on
Globe	globus	k1gInSc5	globus
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
vynikající	vynikající	k2eAgInSc1d1	vynikající
tah	tah	k1gInSc1	tah
<g/>
,	,	kIx,	,
Služebníci	služebník	k1gMnPc1	služebník
lorda	lord	k1gMnSc2	lord
komořího	komoří	k1gMnSc2	komoří
zachránili	zachránit	k5eAaPmAgMnP	zachránit
své	svůj	k3xOyFgNnSc4	svůj
divadlo	divadlo	k1gNnSc4	divadlo
i	i	k9	i
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Jednu	jeden	k4xCgFnSc4	jeden
chybu	chyba	k1gFnSc4	chyba
však	však	k9	však
udělali	udělat	k5eAaPmAgMnP	udělat
<g/>
,	,	kIx,	,
střechu	střecha	k1gFnSc4	střecha
nového	nový	k2eAgNnSc2d1	nové
divadla	divadlo	k1gNnSc2	divadlo
vyrobili	vyrobit	k5eAaPmAgMnP	vyrobit
z	z	k7c2	z
hořlavých	hořlavý	k2eAgInPc2d1	hořlavý
došků	došek	k1gInPc2	došek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1613	[number]	k4	1613
Globe	globus	k1gInSc5	globus
do	do	k7c2	do
základů	základ	k1gInPc2	základ
vyhořel	vyhořet	k5eAaPmAgInS	vyhořet
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc4d1	velký
oheň	oheň	k1gInSc4	oheň
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
dělo	dělo	k1gNnSc1	dělo
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgInSc2	který
vystřelili	vystřelit	k5eAaPmAgMnP	vystřelit
při	při	k7c6	při
představení	představení	k1gNnSc6	představení
<g/>
.	.	kIx.	.
</s>
<s>
Dělo	dělo	k1gNnSc1	dělo
se	se	k3xPyFc4	se
nestrefilo	strefit	k5eNaPmAgNnS	strefit
a	a	k8xC	a
zapálilo	zapálit	k5eAaPmAgNnS	zapálit
doškovou	doškový	k2eAgFnSc4d1	došková
střechu	střecha	k1gFnSc4	střecha
<g/>
.	.	kIx.	.
</s>
<s>
Podílníci	podílník	k1gMnPc1	podílník
měli	mít	k5eAaImAgMnP	mít
povinnost	povinnost	k1gFnSc4	povinnost
postavit	postavit	k5eAaPmF	postavit
nové	nový	k2eAgNnSc4d1	nové
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
Globu	globus	k1gInSc2	globus
přežila	přežít	k5eAaPmAgFnS	přežít
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
dokud	dokud	k6eAd1	dokud
nebylo	být	k5eNaImAgNnS	být
divadlo	divadlo	k1gNnSc1	divadlo
z	z	k7c2	z
nařízení	nařízení	k1gNnSc2	nařízení
puritána	puritán	k1gMnSc4	puritán
Olivera	Oliver	k1gMnSc4	Oliver
Cromwella	Cromwell	k1gMnSc4	Cromwell
uzavřeno	uzavřen	k2eAgNnSc1d1	uzavřeno
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
budova	budova	k1gFnSc1	budova
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1644	[number]	k4	1644
stržena	strhnout	k5eAaPmNgFnS	strhnout
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
přestávce	přestávka	k1gFnSc6	přestávka
trvající	trvající	k2eAgFnSc1d1	trvající
350	[number]	k4	350
let	léto	k1gNnPc2	léto
postaven	postavit	k5eAaPmNgInS	postavit
další	další	k2eAgNnPc1d1	další
Globe	globus	k1gInSc5	globus
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
Shakespearův	Shakespearův	k2eAgMnSc1d1	Shakespearův
milovník	milovník	k1gMnSc1	milovník
Sam	Sam	k1gMnSc1	Sam
Wanamaker	Wanamaker	k1gMnSc1	Wanamaker
(	(	kIx(	(
<g/>
otec	otec	k1gMnSc1	otec
Zoë	Zoë	k1gMnSc1	Zoë
Wanamakerové	Wanamakerové	k2eAgMnSc1d1	Wanamakerové
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Bankside	Banksid	k1gInSc5	Banksid
postaví	postavit	k5eAaPmIp3nP	postavit
přesnou	přesný	k2eAgFnSc4d1	přesná
repliku	replika	k1gFnSc4	replika
Globu	globus	k1gInSc2	globus
<g/>
.	.	kIx.	.
</s>
<s>
Teď	teď	k6eAd1	teď
zde	zde	k6eAd1	zde
stojí	stát	k5eAaImIp3nS	stát
divadlo	divadlo	k1gNnSc4	divadlo
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
vypadalo	vypadat	k5eAaPmAgNnS	vypadat
za	za	k7c2	za
Shakespearových	Shakespearových	k2eAgMnPc2d1	Shakespearových
časů	čas	k1gInPc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Globu	globus	k1gInSc6	globus
probíhá	probíhat	k5eAaImIp3nS	probíhat
každoročně	každoročně	k6eAd1	každoročně
letní	letní	k2eAgFnSc1d1	letní
sezona	sezona	k1gFnSc1	sezona
Shakespearových	Shakespearových	k2eAgFnPc2d1	Shakespearových
her	hra	k1gFnPc2	hra
a	a	k8xC	a
také	také	k9	také
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
největší	veliký	k2eAgFnSc1d3	veliký
trvalá	trvalý	k2eAgFnSc1d1	trvalá
Shakespearova	Shakespearův	k2eAgFnSc1d1	Shakespearova
výstava	výstava	k1gFnSc1	výstava
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
replika	replika	k1gFnSc1	replika
divadla	divadlo	k1gNnSc2	divadlo
Globe	globus	k1gInSc5	globus
také	také	k9	také
na	na	k7c6	na
pražském	pražský	k2eAgNnSc6d1	Pražské
Výstavišti	výstaviště	k1gNnSc6	výstaviště
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
divadlo	divadlo	k1gNnSc1	divadlo
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
zničeno	zničit	k5eAaPmNgNnS	zničit
požárem	požár	k1gInSc7	požár
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
zeměkoule	zeměkoule	k1gFnSc2	zeměkoule
divadlo	divadlo	k1gNnSc4	divadlo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
V	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
našli	najít	k5eAaPmAgMnP	najít
Shakespearovo	Shakespearův	k2eAgNnSc4d1	Shakespearovo
ztracené	ztracený	k2eAgNnSc4d1	ztracené
divadlo	divadlo	k1gNnSc4	divadlo
–	–	k?	–
iDNES	iDNES	k?	iDNES
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
dostupné	dostupný	k2eAgInPc1d1	dostupný
on-line	onin	k1gInSc5	on-lin
</s>
