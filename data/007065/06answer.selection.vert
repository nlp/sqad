<s>
Divadlo	divadlo	k1gNnSc1	divadlo
Globe	globus	k1gInSc5	globus
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1599	[number]	k4	1599
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
spjato	spjat	k2eAgNnSc1d1	spjato
s	s	k7c7	s
osobou	osoba	k1gFnSc7	osoba
Williama	William	k1gMnSc2	William
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
členem	člen	k1gMnSc7	člen
a	a	k8xC	a
podílníkem	podílník	k1gMnSc7	podílník
divadelní	divadelní	k2eAgFnSc2d1	divadelní
společnosti	společnost	k1gFnSc2	společnost
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
Služebníci	služebník	k1gMnPc1	služebník
lorda	lord	k1gMnSc2	lord
komořího	komoří	k1gMnSc2	komoří
<g/>
.	.	kIx.	.
</s>
