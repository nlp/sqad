<p>
<s>
Claudio	Claudio	k1gNnSc1	Claudio
Fogolin	Fogolina	k1gFnPc2	Fogolina
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1872	[number]	k4	1872
<g/>
,	,	kIx,	,
San	San	k1gFnPc1	San
Vito	vit	k2eAgNnSc1d1	Vito
al	ala	k1gFnPc2	ala
Tagliamento	Tagliamento	k1gNnSc1	Tagliamento
–	–	k?	–
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
San	San	k1gFnPc1	San
Vito	vit	k2eAgNnSc1d1	Vito
al	ala	k1gFnPc2	ala
Tagliamento	Tagliamento	k1gNnSc1	Tagliamento
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
cyklista	cyklista	k1gMnSc1	cyklista
<g/>
,	,	kIx,	,
automobilový	automobilový	k2eAgMnSc1d1	automobilový
závodník	závodník	k1gMnSc1	závodník
<g/>
,	,	kIx,	,
podnikatel	podnikatel	k1gMnSc1	podnikatel
a	a	k8xC	a
spoluzakladatel	spoluzakladatel	k1gMnSc1	spoluzakladatel
firmy	firma	k1gFnSc2	firma
Lancia	Lancia	k1gFnSc1	Lancia
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Claudio	Claudio	k6eAd1	Claudio
Fogolin	Fogolin	k1gInSc1	Fogolin
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
bohaté	bohatý	k2eAgFnSc2d1	bohatá
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
ukončil	ukončit	k5eAaPmAgInS	ukončit
studium	studium	k1gNnSc4	studium
na	na	k7c6	na
zemědělské	zemědělský	k2eAgFnSc6d1	zemědělská
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Udine	Udin	k1gMnSc5	Udin
<g/>
,	,	kIx,	,
nejvíc	hodně	k6eAd3	hodně
jej	on	k3xPp3gMnSc4	on
ale	ale	k9	ale
vždy	vždy	k6eAd1	vždy
zajímala	zajímat	k5eAaImAgFnS	zajímat
cyklistika	cyklistika	k1gFnSc1	cyklistika
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1893	[number]	k4	1893
úspěšně	úspěšně	k6eAd1	úspěšně
závodil	závodit	k5eAaImAgMnS	závodit
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
přešel	přejít	k5eAaPmAgMnS	přejít
jeho	jeho	k3xOp3gInSc4	jeho
zájem	zájem	k1gInSc4	zájem
k	k	k7c3	k
automobilismu	automobilismus	k1gInSc3	automobilismus
<g/>
.	.	kIx.	.
</s>
<s>
Claudio	Claudio	k1gMnSc1	Claudio
získal	získat	k5eAaPmAgMnS	získat
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
u	u	k7c2	u
firmy	firma	k1gFnSc2	firma
FIAT	fiat	k1gInSc1	fiat
jako	jako	k8xC	jako
testovací	testovací	k2eAgInSc1d1	testovací
pilot	pilot	k1gInSc1	pilot
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Fiatu	fiat	k1gInSc2	fiat
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
odešel	odejít	k5eAaPmAgMnS	odejít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vincenzo	Vincenza	k1gFnSc5	Vincenza
Lancia	Lancia	k1gFnSc1	Lancia
a	a	k8xC	a
Claudio	Claudio	k6eAd1	Claudio
Fogolin	Fogolin	k2eAgInSc4d1	Fogolin
firmu	firma	k1gFnSc4	firma
Lancia	Lancia	k1gFnSc1	Lancia
&	&	k?	&
C.	C.	kA	C.
Fabrica	Fabrica	k1gMnSc1	Fabrica
Automobili	Automobili	k1gMnSc1	Automobili
založili	založit	k5eAaPmAgMnP	založit
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Fogolin	Fogolin	k1gInSc1	Fogolin
hrál	hrát	k5eAaImAgInS	hrát
velmi	velmi	k6eAd1	velmi
aktivní	aktivní	k2eAgFnSc4d1	aktivní
roli	role	k1gFnSc4	role
jak	jak	k8xC	jak
při	při	k7c6	při
založení	založení	k1gNnSc6	založení
firmy	firma	k1gFnSc2	firma
tak	tak	k9	tak
i	i	k9	i
při	při	k7c6	při
návrhu	návrh	k1gInSc6	návrh
prvních	první	k4xOgInPc2	první
modelů	model	k1gInPc2	model
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
neshod	neshoda	k1gFnPc2	neshoda
ohledně	ohledně	k7c2	ohledně
výnosů	výnos	k1gInPc2	výnos
z	z	k7c2	z
podnikání	podnikání	k1gNnSc2	podnikání
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
odprodat	odprodat	k5eAaPmF	odprodat
společníkovi	společníkův	k2eAgMnPc1d1	společníkův
svůj	svůj	k3xOyFgInSc4	svůj
podíl	podíl	k1gInSc4	podíl
za	za	k7c4	za
šest	šest	k4xCc4	šest
milionů	milion	k4xCgInPc2	milion
lir	lira	k1gFnPc2	lira
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Claudio	Claudio	k6eAd1	Claudio
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
vrátil	vrátit	k5eAaPmAgInS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
rodného	rodný	k2eAgNnSc2d1	rodné
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgInS	věnovat
správě	správa	k1gFnSc3	správa
rodového	rodový	k2eAgInSc2d1	rodový
majetku	majetek	k1gInSc2	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Založil	založit	k5eAaPmAgInS	založit
zde	zde	k6eAd1	zde
i	i	k9	i
tenisový	tenisový	k2eAgInSc4d1	tenisový
klub	klub	k1gInSc4	klub
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
italské	italský	k2eAgFnSc2d1	italská
fašistické	fašistický	k2eAgFnSc2d1	fašistická
strany	strana	k1gFnSc2	strana
téměř	téměř	k6eAd1	téměř
od	od	k7c2	od
jejích	její	k3xOp3gInPc2	její
počátků	počátek	k1gInPc2	počátek
a	a	k8xC	a
tak	tak	k9	tak
stranu	strana	k1gFnSc4	strana
ve	v	k7c6	v
městě	město	k1gNnSc6	město
i	i	k9	i
zastupoval	zastupovat	k5eAaImAgInS	zastupovat
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
sekretáře	sekretář	k1gMnSc2	sekretář
<g/>
,	,	kIx,	,
rok	rok	k1gInSc4	rok
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Italské	italský	k2eAgFnSc2d1	italská
sociální	sociální	k2eAgFnSc2d1	sociální
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prefektem	prefekt	k1gMnSc7	prefekt
okresu	okres	k1gInSc2	okres
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
samém	samý	k3xTgInSc6	samý
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
byl	být	k5eAaImAgInS	být
italskými	italský	k2eAgMnPc7d1	italský
partyzány	partyzán	k1gMnPc7	partyzán
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
řeky	řeka	k1gFnSc2	řeka
Piavy	Piava	k1gFnSc2	Piava
zastřelen	zastřelit	k5eAaPmNgMnS	zastřelit
<g/>
.	.	kIx.	.
</s>
</p>
