<s>
Luna	luna	k1gFnSc1
(	(	kIx(
<g/>
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuPřírodní	infoboxuPřírodní	k2eAgFnSc3d1
památkaLunaIUCN	památkaLunaIUCN	k?
kategorie	kategorie	k1gFnPc4
IV	IV	kA
(	(	kIx(
<g/>
Oblast	oblast	k1gFnSc1
výskytu	výskyt	k1gInSc2
druhu	druh	k1gInSc2
<g/>
)	)	kIx)
PP	PP	kA
LunaZákladní	LunaZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc1
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2011	#num#	k4
Vyhlásil	vyhlásit	k5eAaPmAgMnS
</s>
<s>
Krajský	krajský	k2eAgInSc4d1
úřad	úřad	k1gInSc4
Jihočeského	jihočeský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
Nadm	Nadma	k1gFnPc2
<g/>
.	.	kIx.
výška	výška	k1gFnSc1
</s>
<s>
400	#num#	k4
<g/>
–	–	k?
<g/>
418	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
1,01	1,01	k4
ha	ha	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Okres	okres	k1gInSc1
</s>
<s>
Tábor	Tábor	k1gInSc1
Umístění	umístění	k1gNnSc1
</s>
<s>
Sezimovo	Sezimův	k2eAgNnSc1d1
Ústí	ústí	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
23	#num#	k4
<g/>
′	′	k?
<g/>
12,3	12,3	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
42	#num#	k4
<g/>
′	′	k?
<g/>
14,9	14,9	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Luna	luna	k1gFnSc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc1
</s>
<s>
1289	#num#	k4
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Luna	luna	k1gFnSc1
je	být	k5eAaImIp3nS
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
v	v	k7c6
okrese	okres	k1gInSc6
Tábor	Tábor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
Táborské	Táborské	k2eAgFnSc6d1
pahorkatině	pahorkatina	k1gFnSc6
<g/>
,	,	kIx,
na	na	k7c6
stráni	stráň	k1gFnSc6
nad	nad	k7c7
Kozským	Kozský	k2eAgInSc7d1
potokem	potok	k1gInSc7
<g/>
,	,	kIx,
na	na	k7c6
severovýchodním	severovýchodní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
města	město	k1gNnSc2
Sezimovo	Sezimův	k2eAgNnSc1d1
Ústí	ústí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Předmětem	předmět	k1gInSc7
ochrany	ochrana	k1gFnSc2
je	být	k5eAaImIp3nS
slunná	slunný	k2eAgFnSc1d1
jižně	jižně	k6eAd1
orientovaná	orientovaný	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
se	s	k7c7
suchomilnými	suchomilný	k2eAgNnPc7d1
travinnými	travinný	k2eAgNnPc7d1
a	a	k8xC
květnatými	květnatý	k2eAgNnPc7d1
společenstvy	společenstvo	k1gNnPc7
i	i	k8xC
společenstvy	společenstvo	k1gNnPc7
skalních	skalní	k2eAgFnPc2d1
štěrbin	štěrbina	k1gFnPc2
<g/>
,	,	kIx,
herpetofaunou	herpetofauna	k1gFnSc7
a	a	k8xC
entomofaunou	entomofauna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Mykologie	mykologie	k1gFnSc1
</s>
<s>
Mezi	mezi	k7c4
vzácné	vzácný	k2eAgFnPc4d1
houby	houba	k1gFnPc4
<g/>
,	,	kIx,
rostoucí	rostoucí	k2eAgFnSc4d1
v	v	k7c6
prostoru	prostor	k1gInSc6
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
Luna	luna	k1gFnSc1
patří	patřit	k5eAaImIp3nS
voskovka	voskovka	k1gFnSc1
citronová	citronový	k2eAgFnSc1d1
nebo	nebo	k8xC
palečka	paleček	k1gMnSc4
zimní	zimní	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
nich	on	k3xPp3gMnPc2
lze	lze	k6eAd1
na	na	k7c6
lokalitě	lokalita	k1gFnSc6
najít	najít	k5eAaPmF
i	i	k9
množství	množství	k1gNnSc4
dalších	další	k2eAgInPc2d1
druhů	druh	k1gInPc2
<g/>
,	,	kIx,
především	především	k6eAd1
voskovek	voskovka	k1gFnPc2
(	(	kIx(
<g/>
voskovka	voskovka	k1gFnSc1
panenská	panenský	k2eAgFnSc1d1
<g/>
,	,	kIx,
voskovka	voskovka	k1gFnSc1
luční	luční	k2eAgFnSc1d1
<g/>
)	)	kIx)
nebo	nebo	k8xC
šťavnatek	šťavnatka	k1gFnPc2
(	(	kIx(
<g/>
šťavnatka	šťavnatka	k1gFnSc1
tečkovaná	tečkovaný	k2eAgFnSc1d1
<g/>
,	,	kIx,
šťavnatka	šťavnatka	k1gFnSc1
pomrazka	pomrazka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Flóra	Flóra	k1gFnSc1
</s>
<s>
V	v	k7c6
prostoru	prostor	k1gInSc6
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
dominují	dominovat	k5eAaImIp3nP
sušší	suchý	k2eAgInPc4d2
luční	luční	k2eAgInPc4d1
porosty	porost	k1gInPc4
<g/>
,	,	kIx,
tvořené	tvořený	k2eAgInPc4d1
především	především	k9
ovsíkem	ovsík	k1gInSc7
vyvýšeným	vyvýšený	k2eAgInSc7d1
<g/>
,	,	kIx,
smělkem	smělek	k1gInSc7
jehlancovitým	jehlancovitý	k2eAgInSc7d1
a	a	k8xC
bojínkem	bojínek	k1gInSc7
tuhým	tuhý	k2eAgInSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
se	se	k3xPyFc4
zde	zde	k6eAd1
objevuje	objevovat	k5eAaImIp3nS
válečka	válečka	k1gFnSc1
prapořitá	prapořitý	k2eAgFnSc1d1
<g/>
,	,	kIx,
divizna	divizna	k1gFnSc1
knotkovitá	knotkovitý	k2eAgFnSc1d1
<g/>
,	,	kIx,
hadinec	hadinec	k1gInSc1
obecný	obecný	k2eAgInSc1d1
<g/>
,	,	kIx,
sléz	sléz	k1gInSc1
velkokvětý	velkokvětý	k2eAgInSc1d1
<g/>
,	,	kIx,
starček	starček	k1gInSc1
přímětník	přímětník	k1gInSc4
<g/>
,	,	kIx,
řepík	řepík	k1gInSc4
lékařský	lékařský	k2eAgInSc4d1
<g/>
,	,	kIx,
pavinec	pavinec	k1gInSc4
horský	horský	k2eAgInSc4d1
<g/>
,	,	kIx,
chrastavec	chrastavec	k1gInSc4
rolní	rolní	k2eAgFnSc1d1
i	i	k9
čičorka	čičorka	k1gFnSc1
pestrá	pestrý	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pěšinu	pěšina	k1gFnSc4
při	při	k7c6
horním	horní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
lokality	lokalita	k1gFnSc2
lemují	lemovat	k5eAaImIp3nP
osívka	osívka	k1gFnSc1
jarní	jarní	k2eAgFnSc1d1
<g/>
,	,	kIx,
huseníček	huseníčko	k1gNnPc2
rolní	rolní	k2eAgMnSc1d1
<g/>
,	,	kIx,
mochna	mochna	k1gFnSc1
jarní	jarní	k2eAgFnSc1d1
<g/>
,	,	kIx,
hvozdík	hvozdík	k1gInSc1
kropenatý	kropenatý	k2eAgInSc1d1
a	a	k8xC
vzácně	vzácně	k6eAd1
i	i	k9
hvozdíček	hvozdíček	k1gInSc1
prorostlý	prorostlý	k2eAgInSc1d1
nebo	nebo	k8xC
ohrožený	ohrožený	k2eAgInSc1d1
rozrazil	rozrazil	k1gInSc1
rozprostřený	rozprostřený	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hojná	hojný	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
třezalka	třezalka	k1gFnSc1
tečkovaná	tečkovaný	k2eAgFnSc1d1
a	a	k8xC
různé	různý	k2eAgInPc1d1
druhy	druh	k1gInPc1
chrp	chrpa	k1gFnPc2
(	(	kIx(
<g/>
chrpa	chrpa	k1gFnSc1
luční	luční	k2eAgFnSc1d1
<g/>
,	,	kIx,
chrpa	chrpa	k1gFnSc1
čekánek	čekánek	k1gInSc1
<g/>
,	,	kIx,
chrpa	chrpa	k1gFnSc1
latnatá	latnatý	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objevují	objevovat	k5eAaImIp3nP
se	se	k3xPyFc4
tu	tu	k6eAd1
také	také	k9
všechny	všechen	k3xTgInPc4
tři	tři	k4xCgInPc4
druhy	druh	k1gInPc4
jahodníku	jahodník	k1gInSc2
(	(	kIx(
<g/>
jahodník	jahodník	k1gInSc1
obecný	obecný	k2eAgInSc1d1
<g/>
,	,	kIx,
jahodník	jahodník	k1gInSc1
trávnice	trávnice	k1gFnSc2
i	i	k8xC
jahodník	jahodník	k1gInSc1
truskavec	truskavec	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
několik	několik	k4yIc1
druhů	druh	k1gInPc2
svízele	svízel	k1gInSc2
(	(	kIx(
<g/>
svízel	svízel	k1gInSc1
nízký	nízký	k2eAgInSc1d1
<g/>
,	,	kIx,
svízel	svízel	k1gInSc1
bílý	bílý	k2eAgInSc1d1
<g/>
,	,	kIx,
svízel	svízel	k1gInSc1
syřišťový	syřišťový	k2eAgInSc1d1
<g/>
,	,	kIx,
svízel	svízel	k1gInSc1
pomořanský	pomořanský	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
devaterník	devaterník	k1gInSc1
velkokvětý	velkokvětý	k2eAgInSc1d1
i	i	k8xC
rozchodník	rozchodník	k1gInSc1
šestiřadý	šestiřadý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skalní	skalní	k2eAgFnPc4d1
štěrbiny	štěrbina	k1gFnPc4
ve	v	k7c6
stráni	stráň	k1gFnSc6
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
kapradina	kapradina	k1gFnSc1
sleziník	sleziník	k1gInSc1
severní	severní	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Naopak	naopak	k6eAd1
podél	podél	k7c2
potoka	potok	k1gInSc2
rostou	růst	k5eAaImIp3nP
vlhkomilné	vlhkomilný	k2eAgFnPc4d1
trávy	tráva	k1gFnPc4
-	-	kIx~
zblochan	zblochan	k1gInSc1
vodní	vodní	k2eAgInSc1d1
a	a	k8xC
chrastice	chrastice	k1gFnSc1
rákosovitá	rákosovitý	k2eAgFnSc1d1
<g/>
,	,	kIx,
z	z	k7c2
bylin	bylina	k1gFnPc2
dále	daleko	k6eAd2
kopřiva	kopřiva	k1gFnSc1
dvoudomá	dvoudomý	k2eAgFnSc1d1
a	a	k8xC
bršlice	bršlice	k1gFnSc1
kozí	kozí	k2eAgFnSc1d1
noha	noha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doplňují	doplňovat	k5eAaImIp3nP
je	on	k3xPp3gFnPc4
olše	olše	k1gFnPc4
i	i	k8xC
vrby	vrba	k1gFnPc4
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInPc4
kmeny	kmen	k1gInPc4
popíná	popínat	k5eAaImIp3nS
chmel	chmel	k1gInSc1
otáčivý	otáčivý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Fauna	fauna	k1gFnSc1
</s>
<s>
Chráněné	chráněný	k2eAgNnSc1d1
území	území	k1gNnSc1
poskytuje	poskytovat	k5eAaImIp3nS
útočiště	útočiště	k1gNnSc4
především	především	k9
pro	pro	k7c4
početný	početný	k2eAgInSc4d1
hmyz	hmyz	k1gInSc4
a	a	k8xC
jiné	jiný	k2eAgNnSc4d1
bezobratlé	bezobratlý	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
hmyzu	hmyz	k1gInSc2
zde	zde	k6eAd1
byly	být	k5eAaImAgInP
nalezeny	naleznout	k5eAaPmNgInP,k5eAaBmNgInP
především	především	k6eAd1
některé	některý	k3yIgInPc1
vzácné	vzácný	k2eAgInPc1d1
druhy	druh	k1gInPc1
motýlů	motýl	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Typické	typický	k2eAgInPc1d1
motýly	motýl	k1gMnPc7
výslunných	výslunný	k2eAgFnPc2d1
luk	louka	k1gFnPc2
představují	představovat	k5eAaImIp3nP
okáč	okáč	k1gMnSc1
bojínkový	bojínkový	k2eAgMnSc1d1
<g/>
,	,	kIx,
přástevník	přástevník	k1gMnSc1
chrastavcový	chrastavcový	k2eAgMnSc1d1
a	a	k8xC
vřetenuška	vřetenuška	k1gFnSc1
obecná	obecná	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
zástupcům	zástupce	k1gMnPc3
brouků	brouk	k1gMnPc2
patří	patřit	k5eAaImIp3nP
mandelinky	mandelinka	k1gFnPc4
Coptocephala	Coptocephal	k1gMnSc2
rubicunda	rubicund	k1gMnSc2
a	a	k8xC
Sermylassa	Sermylass	k1gMnSc2
halensis	halensis	k1gFnSc1
<g/>
,	,	kIx,
mandelinka	mandelinka	k1gFnSc1
trnitá	trnitý	k2eAgFnSc1d1
<g/>
,	,	kIx,
krytohlav	krytohlat	k5eAaPmDgInS
hedvábitý	hedvábitý	k2eAgInSc1d1
<g/>
,	,	kIx,
květomil	květomit	k5eAaPmAgInS
žlutý	žlutý	k2eAgInSc1d1
<g/>
,	,	kIx,
zlatohlávek	zlatohlávek	k1gInSc1
zlatý	zlatý	k2eAgInSc1d1
i	i	k8xC
zlatohlávek	zlatohlávek	k1gInSc1
tmavý	tmavý	k2eAgInSc1d1
<g/>
,	,	kIx,
tesařík	tesařík	k1gMnSc1
černošpičký	černošpičký	k2eAgMnSc1d1
<g/>
,	,	kIx,
tesaříci	tesařík	k1gMnPc1
Agapanthia	Agapanthium	k1gNnSc2
intermedia	intermedium	k1gNnSc2
<g/>
,	,	kIx,
Stenurella	Stenurell	k1gMnSc2
bifasciata	bifasciat	k1gMnSc2
i	i	k9
Pseudovadonia	Pseudovadonium	k1gNnSc2
livida	livid	k1gMnSc2
nebo	nebo	k8xC
krasec	krasec	k1gMnSc1
Trachys	Trachysa	k1gFnPc2
fragariae	fragariae	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nivě	niva	k1gFnSc6
Kozského	Kozský	k2eAgInSc2d1
potoka	potok	k1gInSc2
žijí	žít	k5eAaImIp3nP
například	například	k6eAd1
rákosníček	rákosníček	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
<g/>
,	,	kIx,
mandelinka	mandelinka	k1gFnSc1
nádherná	nádherný	k2eAgFnSc1d1
<g/>
,	,	kIx,
mandelinka	mandelinka	k1gFnSc1
šťovíková	šťovíkový	k2eAgFnSc1d1
<g/>
,	,	kIx,
mandelinka	mandelinka	k1gFnSc1
olšová	olšový	k2eAgFnSc1d1
<g/>
,	,	kIx,
štítonoš	štítonoš	k1gMnSc1
zelený	zelený	k2eAgMnSc1d1
<g/>
,	,	kIx,
bázlivec	bázlivec	k1gMnSc1
olšový	olšový	k2eAgMnSc1d1
<g/>
,	,	kIx,
z	z	k7c2
vážek	vážka	k1gFnPc2
pak	pak	k6eAd1
motýlice	motýlice	k1gFnSc1
lesklá	lesklý	k2eAgFnSc1d1
a	a	k8xC
motýlice	motýlice	k1gFnSc1
obecná	obecná	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Nejčastějším	častý	k2eAgInSc7d3
druhem	druh	k1gInSc7
plazů	plaz	k1gMnPc2
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
ještěrka	ještěrka	k1gFnSc1
obecná	obecná	k1gFnSc1
<g/>
,	,	kIx,
vzácně	vzácně	k6eAd1
i	i	k9
zmije	zmije	k1gFnSc1
obecná	obecná	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Otevřená	otevřený	k2eAgFnSc1d1
data	datum	k1gNnSc2
AOPK	AOPK	kA
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Informační	informační	k2eAgInSc4d1
leták	leták	k1gInSc4
k	k	k7c3
PP	PP	kA
Luna	luna	k1gFnSc1
a	a	k8xC
PP	PP	kA
Ostrov	ostrov	k1gInSc1
Markéta	Markéta	k1gFnSc1
<g/>
.	.	kIx.
www.kraj-jihocesky.cz	www.kraj-jihocesky.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ČSOP	ČSOP	kA
-	-	kIx~
Zvláště	zvláště	k6eAd1
chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Tábor	Tábor	k1gInSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
ALBRECHT	Albrecht	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
a	a	k8xC
kolektiv	kolektiv	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Českobudějovicko	Českobudějovicko	k1gNnSc1
v	v	k7c6
<g/>
:	:	kIx,
Mackovčin	Mackovčin	k2eAgMnSc1d1
<g/>
,	,	kIx,
P.	P.	kA
a	a	k8xC
Sedláček	Sedláček	k1gMnSc1
<g/>
,	,	kIx,
M.	M.	kA
(	(	kIx(
<g/>
eds	eds	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Chráněná	chráněný	k2eAgNnPc4d1
území	území	k1gNnPc4
ČR	ČR	kA
<g/>
,	,	kIx,
svazek	svazek	k1gInSc1
VIII	VIII	kA
<g/>
..	..	k?
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Agentura	agentura	k1gFnSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
ČR	ČR	kA
a	a	k8xC
EkoCentrum	EkoCentrum	k1gNnSc1
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
807	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86064	#num#	k4
<g/>
-	-	kIx~
<g/>
65	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Luna	luna	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
453	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Luna	luna	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
AOKP	AOKP	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Tábor	Tábor	k1gInSc1
Přírodní	přírodní	k2eAgInPc1d1
parky	park	k1gInPc1
</s>
<s>
Černická	Černický	k2eAgFnSc1d1
obora	obora	k1gFnSc1
•	•	k?
Jistebnická	jistebnický	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
•	•	k?
Kukle	kukle	k1gFnSc2
•	•	k?
Plziny	Plzina	k1gMnSc2
•	•	k?
Polánka	Polánek	k1gMnSc2
•	•	k?
Turovecký	Turovecký	k2eAgInSc4d1
les	les	k1gInSc4
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Třeboňsko	Třeboňsko	k1gNnSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Chýnovská	Chýnovský	k2eAgFnSc1d1
jeskyně	jeskyně	k1gFnSc1
•	•	k?
Luční	luční	k2eAgFnSc1d1
•	•	k?
Ruda	ruda	k1gFnSc1
•	•	k?
Stročov	Stročov	k1gInSc4
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Borkovická	Borkovický	k2eAgNnPc1d1
blata	blata	k1gNnPc1
•	•	k?
Dráchovské	Dráchovský	k2eAgFnSc2d1
louky	louka	k1gFnSc2
•	•	k?
Dráchovské	Dráchovský	k2eAgFnSc2d1
tůně	tůně	k1gFnSc2
•	•	k?
Horusická	horusický	k2eAgNnPc4d1
blata	blata	k1gNnPc4
•	•	k?
Choustník	Choustník	k1gMnSc1
•	•	k?
Kladrubská	kladrubský	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Kozohlůdky	Kozohlůdka	k1gFnSc2
•	•	k?
Pacova	Pacův	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Písečný	písečný	k2eAgInSc4d1
přesyp	přesyp	k1gInSc4
u	u	k7c2
Vlkova	Vlkův	k2eAgNnSc2d1
•	•	k?
Rod	rod	k1gInSc1
•	•	k?
V	v	k7c6
Luhu	luh	k1gInSc6
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Černická	Černický	k2eAgFnSc1d1
obora	obora	k1gFnSc1
•	•	k?
Černýšovické	Černýšovický	k2eAgInPc1d1
jalovce	jalovec	k1gInPc1
•	•	k?
Doubí	doubí	k1gNnSc6
u	u	k7c2
Žíšova	Žíšov	k1gInSc2
•	•	k?
Farářský	farářský	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Granátová	granátový	k2eAgFnSc1d1
skála	skála	k1gFnSc1
•	•	k?
Hroby	hrob	k1gInPc1
•	•	k?
Jesení	jeseň	k1gFnPc2
•	•	k?
Kozí	kozí	k2eAgInSc1d1
vršek	vršek	k1gInSc1
•	•	k?
Kozlov	Kozlov	k1gInSc4
•	•	k?
Kutiny	kutin	k1gInPc4
•	•	k?
Luna	luna	k1gFnSc1
•	•	k?
Lužnice	Lužnice	k1gFnSc2
•	•	k?
Nový	nový	k2eAgInSc4d1
rybník	rybník	k1gInSc4
u	u	k7c2
Soběslavi	Soběslav	k1gFnSc2
•	•	k?
Ostrov	ostrov	k1gInSc1
Markéta	Markéta	k1gFnSc1
•	•	k?
Stříbrná	stříbrný	k2eAgFnSc1d1
Huť	huť	k1gFnSc1
•	•	k?
Tábor	Tábor	k1gInSc1
-	-	kIx~
Zahrádka	Zahrádka	k1gMnSc1
•	•	k?
Veselská	Veselská	k1gFnSc1
blata	blata	k1gNnPc1
•	•	k?
Vlásenický	Vlásenický	k2eAgInSc1d1
potok	potok	k1gInSc1
•	•	k?
Vlašimská	vlašimský	k2eAgFnSc1d1
Blanice	Blanice	k1gFnSc1
•	•	k?
Zeman	Zeman	k1gMnSc1
•	•	k?
Židova	Židův	k2eAgFnSc1d1
strouha	strouha	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Příroda	příroda	k1gFnSc1
</s>
