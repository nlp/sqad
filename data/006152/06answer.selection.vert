<s>
Dario	Daria	k1gFnSc5	Daria
Fo	Fo	k1gFnSc5	Fo
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1926	[number]	k4	1926
Sangiano	Sangiana	k1gFnSc5	Sangiana
–	–	k?	–
13	[number]	k4	13
<g/>
.	.	kIx.	.
říjen	říjen	k1gInSc4	říjen
2016	[number]	k4	2016
Milán	Milán	k1gInSc1	Milán
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
satirický	satirický	k2eAgMnSc1d1	satirický
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgMnSc1d1	divadelní
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
výtvarník	výtvarník	k1gMnSc1	výtvarník
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1997	[number]	k4	1997
(	(	kIx(	(
<g/>
nominován	nominovat	k5eAaBmNgMnS	nominovat
byl	být	k5eAaImAgMnS	být
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
