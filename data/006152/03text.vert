<s>
Dario	Daria	k1gFnSc5	Daria
Fo	Fo	k1gFnSc5	Fo
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1926	[number]	k4	1926
Sangiano	Sangiana	k1gFnSc5	Sangiana
–	–	k?	–
13	[number]	k4	13
<g/>
.	.	kIx.	.
říjen	říjen	k1gInSc4	říjen
2016	[number]	k4	2016
Milán	Milán	k1gInSc1	Milán
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
satirický	satirický	k2eAgMnSc1d1	satirický
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgMnSc1d1	divadelní
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
výtvarník	výtvarník	k1gMnSc1	výtvarník
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1997	[number]	k4	1997
(	(	kIx(	(
<g/>
nominován	nominovat	k5eAaBmNgMnS	nominovat
byl	být	k5eAaImAgMnS	být
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Využíval	využívat	k5eAaImAgMnS	využívat
metody	metoda	k1gFnPc4	metoda
commedie	commedie	k1gFnSc2	commedie
dell	della	k1gFnPc2	della
<g/>
'	'	kIx"	'
<g/>
arte	arte	k6eAd1	arte
a	a	k8xC	a
úzce	úzko	k6eAd1	úzko
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
Francou	Francý	k2eAgFnSc4d1	Francý
Rame	Rame	k1gFnSc4	Rame
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
známý	známý	k2eAgMnSc1d1	známý
svou	svůj	k3xOyFgFnSc7	svůj
politickou	politický	k2eAgFnSc7d1	politická
a	a	k8xC	a
společenskou	společenský	k2eAgFnSc7d1	společenská
satirou	satira	k1gFnSc7	satira
a	a	k8xC	a
levicovou	levicový	k2eAgFnSc7d1	levicová
politickou	politický	k2eAgFnSc7d1	politická
angažovaností	angažovanost	k1gFnSc7	angažovanost
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
obdivovatelem	obdivovatel	k1gMnSc7	obdivovatel
Mao	Mao	k1gMnSc2	Mao
Ce-tunga	Ceung	k1gMnSc2	Ce-tung
a	a	k8xC	a
představitelem	představitel	k1gMnSc7	představitel
levicové	levicový	k2eAgFnSc2d1	levicová
organizace	organizace	k1gFnSc2	organizace
Soccorso	Soccorsa	k1gFnSc5	Soccorsa
Rosso	Rossa	k1gFnSc5	Rossa
Militante	militant	k1gMnSc5	militant
<g/>
.	.	kIx.	.
</s>
<s>
Syn	syn	k1gMnSc1	syn
Felice	Felice	k1gFnSc2	Felice
a	a	k8xC	a
Pina	Pinum	k1gNnSc2	Pinum
Rotových	Rotová	k1gFnPc2	Rotová
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
v	v	k7c6	v
intelektuálním	intelektuální	k2eAgNnSc6d1	intelektuální
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
mu	on	k3xPp3gMnSc3	on
babička	babička	k1gFnSc1	babička
vyprávěla	vyprávět	k5eAaImAgFnS	vyprávět
bajky	bajka	k1gFnPc4	bajka
a	a	k8xC	a
povídky	povídka	k1gFnPc4	povídka
o	o	k7c6	o
cestovatelích	cestovatel	k1gMnPc6	cestovatel
a	a	k8xC	a
řemeslnících	řemeslník	k1gMnPc6	řemeslník
<g/>
.	.	kIx.	.
</s>
<s>
Fo	Fo	k?	Fo
se	se	k3xPyFc4	se
jimi	on	k3xPp3gMnPc7	on
po	po	k7c4	po
léta	léto	k1gNnPc4	léto
nechává	nechávat	k5eAaImIp3nS	nechávat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
tvorbě	tvorba	k1gFnSc6	tvorba
inspirovat	inspirovat	k5eAaBmF	inspirovat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgInS	nechat
dobrovolně	dobrovolně	k6eAd1	dobrovolně
naverbovat	naverbovat	k5eAaPmF	naverbovat
mezi	mezi	k7c7	mezi
parašutisty	parašutista	k1gMnPc7	parašutista
Italské	italský	k2eAgFnSc2d1	italská
sociální	sociální	k2eAgFnSc2d1	sociální
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Fo	Fo	k?	Fo
mluvil	mluvit	k5eAaImAgMnS	mluvit
o	o	k7c4	o
"	"	kIx"	"
<g/>
momentální	momentální	k2eAgFnPc4d1	momentální
a	a	k8xC	a
nucené	nucený	k2eAgFnPc4d1	nucená
přítomnosti	přítomnost	k1gFnPc4	přítomnost
na	na	k7c4	na
cvičení	cvičení	k1gNnSc4	cvičení
protileteckého	protiletecký	k2eAgNnSc2d1	protiletecké
dělestřelectva	dělestřelectvo	k1gNnSc2	dělestřelectvo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
stoupenec	stoupenec	k1gMnSc1	stoupenec
italské	italský	k2eAgFnSc2d1	italská
sociální	sociální	k2eAgFnSc2d1	sociální
republiky	republika	k1gFnSc2	republika
<g/>
?	?	kIx.	?
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
jsem	být	k5eAaImIp1nS	být
to	ten	k3xDgNnSc4	ten
nepopíral	popírat	k5eNaImAgMnS	popírat
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
26	[number]	k4	26
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
43	[number]	k4	43
mi	já	k3xPp1nSc3	já
bylo	být	k5eAaImAgNnS	být
17	[number]	k4	17
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
jsem	být	k5eAaImIp1nS	být
měl	mít	k5eAaImAgMnS	mít
příležitost	příležitost	k1gFnSc4	příležitost
<g/>
,	,	kIx,	,
zběhl	zběhnout	k5eAaPmAgMnS	zběhnout
jsem	být	k5eAaImIp1nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
přišlo	přijít	k5eAaPmAgNnS	přijít
nařízení	nařízení	k1gNnSc1	nařízení
k	k	k7c3	k
zabití	zabití	k1gNnSc3	zabití
<g/>
.	.	kIx.	.
</s>
<s>
Mohl	moct	k5eAaImAgMnS	moct
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
ukázat	ukázat	k5eAaPmF	ukázat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ukrýt	ukrýt	k5eAaPmF	ukrýt
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
"	"	kIx"	"
a	a	k8xC	a
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dobrovolný	dobrovolný	k2eAgInSc4d1	dobrovolný
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
byl	být	k5eAaImAgInS	být
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
otcovy	otcův	k2eAgFnPc1d1	otcova
protifašistické	protifašistický	k2eAgFnPc1d1	protifašistická
aktivity	aktivita	k1gFnPc1	aktivita
nebudily	budit	k5eNaImAgFnP	budit
podezření	podezření	k1gNnSc4	podezření
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozsudku	rozsudek	k1gInSc6	rozsudek
soudu	soud	k1gInSc2	soud
ve	v	k7c6	v
Varese	Vares	k1gInSc6	Vares
ze	z	k7c2	z
dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1979	[number]	k4	1979
stojí	stát	k5eAaImIp3nS	stát
mj.	mj.	kA	mj.
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
že	že	k8xS	že
Fo	Fo	k1gMnSc1	Fo
nosil	nosit	k5eAaImAgMnS	nosit
uniformu	uniforma	k1gFnSc4	uniforma
republikánských	republikánský	k2eAgMnPc2d1	republikánský
parašutistů	parašutista	k1gMnPc2	parašutista
v	v	k7c6	v
řadách	řada	k1gFnPc6	řada
Battaglione	Battaglion	k1gInSc5	Battaglion
Azzurro	Azzurra	k1gFnSc5	Azzurra
di	di	k?	di
Tradate	Tradat	k1gInSc5	Tradat
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Je	být	k5eAaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
že	že	k8xS	že
fašistických	fašistický	k2eAgFnPc2d1	fašistická
operací	operace	k1gFnPc2	operace
ve	v	k7c4	v
Val	val	k1gInSc4	val
Cannobina	Cannobin	k2eAgInSc2d1	Cannobin
se	se	k3xPyFc4	se
účastnili	účastnit	k5eAaImAgMnP	účastnit
i	i	k9	i
parašutisti	parašutisti	k?	parašutisti
z	z	k7c2	z
Battaglione	Battaglion	k1gInSc5	Battaglion
Azzurro	Azzurra	k1gFnSc5	Azzurra
di	di	k?	di
Tradate	Tradat	k1gInSc5	Tradat
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Není	být	k5eNaImIp3nS	být
ale	ale	k9	ale
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
lépe	dobře	k6eAd2	dobře
řečeno	říct	k5eAaPmNgNnS	říct
je	být	k5eAaImIp3nS	být
sporné	sporný	k2eAgNnSc1d1	sporné
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
účastnil	účastnit	k5eAaImAgInS	účastnit
i	i	k9	i
Dario	Daria	k1gFnSc5	Daria
Fo	Fo	k1gMnPc6	Fo
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Foova	Foova	k1gFnSc1	Foova
minulost	minulost	k1gFnSc1	minulost
následně	následně	k6eAd1	následně
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
kritiku	kritika	k1gFnSc4	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
dokončil	dokončit	k5eAaPmAgMnS	dokončit
studia	studio	k1gNnSc2	studio
na	na	k7c6	na
Accademia	Accademia	k1gFnSc1	Accademia
di	di	k?	di
Belle	bell	k1gInSc5	bell
Arti	Arti	k1gNnPc7	Arti
di	di	k?	di
Brera	Brera	k1gMnSc1	Brera
v	v	k7c6	v
Miláně	Milán	k1gInSc6	Milán
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
pracovat	pracovat	k5eAaImF	pracovat
pro	pro	k7c4	pro
rozhlas	rozhlas	k1gInSc4	rozhlas
a	a	k8xC	a
televizi	televize	k1gFnSc4	televize
jako	jako	k8xS	jako
satirický	satirický	k2eAgMnSc1d1	satirický
autor	autor	k1gMnSc1	autor
a	a	k8xC	a
herec	herec	k1gMnSc1	herec
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1954	[number]	k4	1954
se	se	k3xPyFc4	se
Dario	Daria	k1gFnSc5	Daria
Fo	Fo	k1gMnSc6	Fo
v	v	k7c6	v
Miláně	Milán	k1gInSc6	Milán
v	v	k7c6	v
bazilice	bazilika	k1gFnSc6	bazilika
svatého	svatý	k2eAgMnSc4d1	svatý
Ambrože	Ambrož	k1gMnSc4	Ambrož
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
herečkou	herečka	k1gFnSc7	herečka
a	a	k8xC	a
kolegyní	kolegyně	k1gFnSc7	kolegyně
Francou	Franca	k1gFnSc7	Franca
Rame	Rame	k1gFnSc7	Rame
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1955	[number]	k4	1955
až	až	k9	až
1958	[number]	k4	1958
pracoval	pracovat	k5eAaImAgInS	pracovat
jako	jako	k9	jako
filmový	filmový	k2eAgMnSc1d1	filmový
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
se	se	k3xPyFc4	se
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
činnosti	činnost	k1gFnSc2	činnost
organizace	organizace	k1gFnSc2	organizace
Soccorso	Soccorsa	k1gFnSc5	Soccorsa
Rosso	Rossa	k1gFnSc5	Rossa
Militante	militant	k1gMnSc5	militant
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1958	[number]	k4	1958
až	až	k9	až
1968	[number]	k4	1968
Fo	Fo	k1gFnPc2	Fo
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mezitím	mezitím	k6eAd1	mezitím
založili	založit	k5eAaPmAgMnP	založit
společnost	společnost	k1gFnSc4	společnost
"	"	kIx"	"
<g/>
Compagnia	Compagnium	k1gNnSc2	Compagnium
Dario	Daria	k1gFnSc5	Daria
Fo	Fo	k1gFnSc6	Fo
-	-	kIx~	-
Franca	Franca	k?	Franca
Rame	Rame	k1gInSc1	Rame
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
připravovali	připravovat	k5eAaImAgMnP	připravovat
sérii	série	k1gFnSc4	série
krátkých	krátký	k2eAgFnPc2d1	krátká
scének	scénka	k1gFnPc2	scénka
pro	pro	k7c4	pro
varieté	varieté	k1gNnSc4	varieté
televize	televize	k1gFnSc2	televize
RAI	RAI	kA	RAI
Canzonissima	Canzonissim	k1gMnSc2	Canzonissim
<g/>
.	.	kIx.	.
</s>
<s>
Cenzura	cenzura	k1gFnSc1	cenzura
ale	ale	k9	ale
zasahovala	zasahovat	k5eAaImAgFnS	zasahovat
tak	tak	k6eAd1	tak
často	často	k6eAd1	často
<g/>
,	,	kIx,	,
že	že	k8xS	že
manželé	manžel	k1gMnPc1	manžel
opustili	opustit	k5eAaPmAgMnP	opustit
televizi	televize	k1gFnSc4	televize
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
se	se	k3xPyFc4	se
věnovat	věnovat	k5eAaPmF	věnovat
divadlu	divadlo	k1gNnSc3	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
kritickým	kritický	k2eAgInSc7d1	kritický
postojem	postoj	k1gInSc7	postoj
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
nazýval	nazývat	k5eAaImAgMnS	nazývat
"	"	kIx"	"
<g/>
lesní	lesní	k2eAgNnSc1d1	lesní
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
Fo	Fo	k1gMnSc1	Fo
vystupovat	vystupovat	k5eAaImF	vystupovat
na	na	k7c6	na
jiných	jiný	k2eAgNnPc6d1	jiné
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
,	,	kIx,	,
lidové	lidový	k2eAgInPc4d1	lidový
domy	dům	k1gInPc4	dům
nebo	nebo	k8xC	nebo
továrny	továrna	k1gFnPc4	továrna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nacházel	nacházet	k5eAaImAgMnS	nacházet
různé	různý	k2eAgMnPc4d1	různý
diváky	divák	k1gMnPc4	divák
<g/>
,	,	kIx,	,
především	především	k9	především
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
nižších	nízký	k2eAgFnPc2d2	nižší
tříd	třída	k1gFnPc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
založil	založit	k5eAaPmAgMnS	založit
divadelní	divadelní	k2eAgFnSc4d1	divadelní
skupinu	skupina	k1gFnSc4	skupina
Nuova	Nuov	k1gMnSc2	Nuov
Scena	Scen	k1gMnSc2	Scen
(	(	kIx(	(
<g/>
Nová	nový	k2eAgFnSc1d1	nová
scéna	scéna	k1gFnSc1	scéna
<g/>
)	)	kIx)	)
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
vrátit	vrátit	k5eAaPmF	vrátit
se	se	k3xPyFc4	se
k	k	k7c3	k
lidovým	lidový	k2eAgInPc3d1	lidový
kořenům	kořen	k1gInPc3	kořen
divadla	divadlo	k1gNnSc2	divadlo
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
společenské	společenský	k2eAgFnSc3d1	společenská
hodnotě	hodnota	k1gFnSc3	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
přinesl	přinést	k5eAaPmAgMnS	přinést
Fo	Fo	k1gFnSc4	Fo
na	na	k7c4	na
scénu	scéna	k1gFnSc4	scéna
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
úspěchem	úspěch	k1gInSc7	úspěch
Mistero	Mistero	k1gNnSc4	Mistero
buffo	buffo	k2eAgMnPc1d1	buffo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jediný	jediný	k2eAgMnSc1d1	jediný
herec	herec	k1gMnSc1	herec
na	na	k7c6	na
jevišti	jeviště	k1gNnSc6	jeviště
recitoval	recitovat	k5eAaImAgInS	recitovat
bizarní	bizarní	k2eAgNnPc4d1	bizarní
přepracování	přepracování	k1gNnPc4	přepracování
starých	starý	k2eAgInPc2d1	starý
textů	text	k1gInPc2	text
v	v	k7c6	v
grammelotu	grammelot	k1gInSc6	grammelot
<g/>
.	.	kIx.	.
</s>
<s>
Grammelot	Grammelot	k1gInSc1	Grammelot
je	být	k5eAaImIp3nS	být
divadelní	divadelní	k2eAgInSc1d1	divadelní
jazyk	jazyk	k1gInSc1	jazyk
odvozený	odvozený	k2eAgInSc1d1	odvozený
od	od	k7c2	od
tradice	tradice	k1gFnSc2	tradice
commedie	commedie	k1gFnSc1	commedie
dell	dell	k1gMnSc1	dell
<g/>
'	'	kIx"	'
<g/>
arte	arte	k1gInSc1	arte
složený	složený	k2eAgInSc1d1	složený
ze	z	k7c2	z
zvuků	zvuk	k1gInPc2	zvuk
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
napodobují	napodobovat	k5eAaImIp3nP	napodobovat
rytmus	rytmus	k1gInSc4	rytmus
a	a	k8xC	a
intonanci	intonance	k1gFnSc4	intonance
skutečné	skutečný	k2eAgFnSc2d1	skutečná
řeči	řeč	k1gFnSc2	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Fo	Fo	k?	Fo
využil	využít	k5eAaPmAgInS	využít
pádštinu	pádština	k1gFnSc4	pádština
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
imitovala	imitovat	k5eAaBmAgFnS	imitovat
různé	různý	k2eAgInPc4d1	různý
dialekty	dialekt	k1gInPc4	dialekt
<g/>
,	,	kIx,	,
kterými	který	k3yQgInPc7	který
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
v	v	k7c6	v
Pádské	pádský	k2eAgFnSc6d1	Pádská
nížině	nížina	k1gFnSc6	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Mistero	Mistero	k1gNnSc4	Mistero
buffo	buffo	k2eAgNnSc4d1	buffo
stálo	stát	k5eAaImAgNnS	stát
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
žánru	žánr	k1gInSc2	žánr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
nazývá	nazývat	k5eAaImIp3nS	nazývat
narativní	narativní	k2eAgNnSc4d1	narativní
divadlo	divadlo	k1gNnSc4	divadlo
a	a	k8xC	a
mezi	mezi	k7c4	mezi
jehož	jehož	k3xOyRp3gInSc4	jehož
významné	významný	k2eAgFnSc3d1	významná
autory-herce	autoryerka	k1gFnSc3	autory-herka
patří	patřit	k5eAaImIp3nP	patřit
Marco	Marco	k6eAd1	Marco
Paolini	Paolin	k2eAgMnPc1d1	Paolin
<g/>
,	,	kIx,	,
Marco	Marco	k6eAd1	Marco
Baliani	Baliaň	k1gFnSc3	Baliaň
<g/>
,	,	kIx,	,
Laura	Laura	k1gFnSc1	Laura
Curino	Curino	k1gNnSc4	Curino
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
první	první	k4xOgFnSc1	první
generace	generace	k1gFnSc1	generace
<g/>
,	,	kIx,	,
narození	narození	k1gNnSc1	narození
v	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ascanio	Ascanio	k6eAd1	Ascanio
Celestini	Celestin	k2eAgMnPc1d1	Celestin
a	a	k8xC	a
Davide	David	k1gMnSc5	David
Enia	Eni	k2eAgFnSc1d1	Eni
(	(	kIx(	(
<g/>
druhá	druhý	k4xOgFnSc1	druhý
generace	generace	k1gFnSc1	generace
<g/>
,	,	kIx,	,
narození	narození	k1gNnSc1	narození
v	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Foův	Foův	k1gInSc4	Foův
odkaz	odkaz	k1gInSc4	odkaz
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
i	i	k9	i
u	u	k7c2	u
komiků	komik	k1gMnPc2	komik
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Paolo	Paolo	k1gNnSc4	Paolo
Rossi	Rosse	k1gFnSc3	Rosse
<g/>
,	,	kIx,	,
a	a	k8xC	a
autorů	autor	k1gMnPc2	autor
jako	jako	k8xS	jako
Andrea	Andrea	k1gFnSc1	Andrea
Cosentino	Cosentina	k1gFnSc5	Cosentina
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
přinesli	přinést	k5eAaPmAgMnP	přinést
narativní	narativní	k2eAgNnSc4d1	narativní
divadlo	divadlo	k1gNnSc4	divadlo
do	do	k7c2	do
televizního	televizní	k2eAgInSc2d1	televizní
kabaretu	kabaret	k1gInSc2	kabaret
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
Fo	Fo	k1gFnSc2	Fo
dal	dát	k5eAaPmAgInS	dát
dohromady	dohromady	k6eAd1	dohromady
s	s	k7c7	s
mimoparlamentními	mimoparlamentní	k2eAgFnPc7d1	mimoparlamentní
organizacemi	organizace	k1gFnPc7	organizace
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
společnost	společnost	k1gFnSc4	společnost
"	"	kIx"	"
<g/>
La	la	k0	la
Comune	Comun	k1gMnSc5	Comun
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
zaujetím	zaujetí	k1gNnSc7	zaujetí
snažil	snažit	k5eAaImAgMnS	snažit
podporovat	podporovat	k5eAaImF	podporovat
pouliční	pouliční	k2eAgNnSc4d1	pouliční
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
se	se	k3xPyFc4	se
komedií	komedie	k1gFnPc2	komedie
Morte	Mort	k1gInSc5	Mort
accidentale	accidental	k1gMnSc5	accidental
di	di	k?	di
un	un	k?	un
anarchico	anarchico	k6eAd1	anarchico
vrátil	vrátit	k5eAaPmAgMnS	vrátit
k	k	k7c3	k
frašce	fraška	k1gFnSc3	fraška
a	a	k8xC	a
politice	politika	k1gFnSc3	politika
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
evidentně	evidentně	k6eAd1	evidentně
inspirováno	inspirovat	k5eAaBmNgNnS	inspirovat
smrtí	smrt	k1gFnSc7	smrt
anarchisty	anarchista	k1gMnPc4	anarchista
Giuseppe	Giusepp	k1gMnSc5	Giusepp
Pinelliho	Pinelliha	k1gMnSc5	Pinelliha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
aby	aby	kYmCp3nS	aby
Fo	Fo	k1gMnSc1	Fo
se	se	k3xPyFc4	se
vyhnul	vyhnout	k5eAaPmAgMnS	vyhnout
cenzuře	cenzura	k1gFnSc3	cenzura
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
inspirováno	inspirovat	k5eAaBmNgNnS	inspirovat
obdobným	obdobný	k2eAgInSc7d1	obdobný
případem	případ	k1gInSc7	případ
ze	z	k7c2	z
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Případ	případ	k1gInSc1	případ
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
sídle	sídlo	k1gNnSc6	sídlo
centrální	centrální	k2eAgFnSc2d1	centrální
prokuratury	prokuratura	k1gFnSc2	prokuratura
v	v	k7c6	v
Miláně	Milán	k1gInSc6	Milán
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgMnSc1d1	hlavní
protagonista	protagonista	k1gMnSc1	protagonista
"	"	kIx"	"
<g/>
Blázen	blázen	k1gMnSc1	blázen
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
často	často	k6eAd1	často
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
Foova	Foovo	k1gNnSc2	Foovo
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
odhalit	odhalit	k5eAaPmF	odhalit
nepříjemnou	příjemný	k2eNgFnSc4d1	nepříjemná
pravdu	pravda	k1gFnSc4	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Blázen	blázen	k1gMnSc1	blázen
přebírá	přebírat	k5eAaImIp3nS	přebírat
různé	různý	k2eAgFnPc4d1	různá
role	role	k1gFnPc4	role
(	(	kIx(	(
<g/>
psychiatr	psychiatr	k1gMnSc1	psychiatr
<g/>
,	,	kIx,	,
soudce	soudce	k1gMnSc1	soudce
<g/>
,	,	kIx,	,
kapitán	kapitán	k1gMnSc1	kapitán
služby	služba	k1gFnSc2	služba
k	k	k7c3	k
identifikaci	identifikace	k1gFnSc3	identifikace
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
biskup	biskup	k1gMnSc1	biskup
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
nichž	jenž	k3xRgInPc2	jenž
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
všechny	všechen	k3xTgFnPc4	všechen
nesrovnalosti	nesrovnalost	k1gFnPc4	nesrovnalost
oficiální	oficiální	k2eAgFnSc2d1	oficiální
verze	verze	k1gFnSc2	verze
<g/>
,	,	kIx,	,
a	a	k8xC	a
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
vytvořit	vytvořit	k5eAaPmF	vytvořit
hodnověrnou	hodnověrný	k2eAgFnSc4d1	hodnověrná
verzi	verze	k1gFnSc4	verze
se	se	k3xPyFc4	se
vynořují	vynořovat	k5eAaImIp3nP	vynořovat
i	i	k9	i
další	další	k2eAgInPc1d1	další
veselé	veselý	k2eAgInPc1d1	veselý
rozpory	rozpor	k1gInPc1	rozpor
<g/>
.	.	kIx.	.
</s>
<s>
Fo	Fo	k?	Fo
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
televize	televize	k1gFnSc2	televize
s	s	k7c7	s
cyklem	cyklus	k1gInSc7	cyklus
nazvaným	nazvaný	k2eAgInSc7d1	nazvaný
Il	Il	k1gMnSc7	Il
teatro	teatra	k1gFnSc5	teatra
Dario	Daria	k1gFnSc5	Daria
Fo	Fo	k1gFnSc1	Fo
(	(	kIx(	(
<g/>
Divadlo	divadlo	k1gNnSc1	divadlo
Daria	Daria	k1gFnSc1	Daria
Fo	Fo	k1gFnSc1	Fo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
série	série	k1gFnSc1	série
vysílání	vysílání	k1gNnSc2	vysílání
později	pozdě	k6eAd2	pozdě
přinesla	přinést	k5eAaPmAgFnS	přinést
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
díky	díky	k7c3	díky
masovosti	masovost	k1gFnSc3	masovost
televize	televize	k1gFnSc2	televize
dostalo	dostat	k5eAaPmAgNnS	dostat
většího	veliký	k2eAgNnSc2d2	veliký
ocenění	ocenění	k1gNnSc2	ocenění
než	než	k8xS	než
dřív	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejdůležitější	důležitý	k2eAgInPc4d3	nejdůležitější
tituly	titul	k1gInPc4	titul
patří	patřit	k5eAaImIp3nS	patřit
Mistero	Mistero	k1gNnSc1	Mistero
buffo	buffo	k2eAgNnSc1d1	buffo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
otevírá	otevírat	k5eAaImIp3nS	otevírat
cyklus	cyklus	k1gInSc1	cyklus
<g/>
,	,	kIx,	,
Settimo	Settima	k1gFnSc5	Settima
<g/>
:	:	kIx,	:
ruba	rub	k2eAgMnSc4d1	rub
un	un	k?	un
po	po	k7c4	po
<g/>
'	'	kIx"	'
meno	meno	k1gNnSc4	meno
(	(	kIx(	(
<g/>
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
<g/>
:	:	kIx,	:
Nepokradeš	pokrást	k5eNaPmIp2nS	pokrást
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Isabella	Isabella	k1gMnSc1	Isabella
<g/>
,	,	kIx,	,
tre	tre	k?	tre
caravelle	caravelle	k1gInSc1	caravelle
e	e	k0	e
un	un	k?	un
cacciaballe	cacciaballe	k1gNnPc4	cacciaballe
a	a	k8xC	a
Parliamo	Parliama	k1gFnSc5	Parliama
di	di	k?	di
donne	donnout	k5eAaImIp3nS	donnout
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
Mistero	Mistero	k1gNnSc1	Mistero
buffo	buffo	k2eAgNnSc1d1	buffo
<g/>
,	,	kIx,	,
přitáhla	přitáhnout	k5eAaPmAgFnS	přitáhnout
pozornost	pozornost	k1gFnSc4	pozornost
Vatikánu	Vatikán	k1gInSc2	Vatikán
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
podle	podle	k7c2	podle
slov	slovo	k1gNnPc2	slovo
kardinála	kardinál	k1gMnSc2	kardinál
Poletti	Poletť	k1gFnSc2	Poletť
reagoval	reagovat	k5eAaBmAgInS	reagovat
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
závadný	závadný	k2eAgInSc4d1	závadný
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
u	u	k7c2	u
slavné	slavný	k2eAgFnSc2d1	slavná
dvojice	dvojice	k1gFnSc2	dvojice
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
napsal	napsat	k5eAaBmAgInS	napsat
divadelní	divadelní	k2eAgFnSc4d1	divadelní
hru	hra	k1gFnSc4	hra
Bum	bum	k0	bum
<g/>
!	!	kIx.	!
</s>
<s>
Bum	bum	k0	bum
<g/>
!	!	kIx.	!
</s>
<s>
Kdo	kdo	k3yRnSc1	kdo
je	být	k5eAaImIp3nS	být
<g/>
?	?	kIx.	?
</s>
<s>
Policie	policie	k1gFnSc1	policie
<g/>
!	!	kIx.	!
</s>
<s>
Foovo	Foovo	k6eAd1	Foovo
divadlo	divadlo	k1gNnSc1	divadlo
je	být	k5eAaImIp3nS	být
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
postihuje	postihovat	k5eAaImIp3nS	postihovat
skutečnost	skutečnost	k1gFnSc1	skutečnost
i	i	k8xC	i
tématy	téma	k1gNnPc7	téma
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
vzdálena	vzdálit	k5eAaPmNgFnS	vzdálit
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
stálou	stálý	k2eAgFnSc7d1	stálá
součástí	součást	k1gFnSc7	součást
her	hra	k1gFnPc2	hra
je	být	k5eAaImIp3nS	být
antiklerikalismus	antiklerikalismus	k1gInSc1	antiklerikalismus
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
dva	dva	k4xCgInPc1	dva
prvky	prvek	k1gInPc1	prvek
jsou	být	k5eAaImIp3nP	být
patrné	patrný	k2eAgInPc1d1	patrný
v	v	k7c6	v
komedii	komedie	k1gFnSc6	komedie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
Il	Il	k1gMnSc1	Il
papa	papa	k1gMnSc1	papa
e	e	k0	e
la	la	k1gNnPc1	la
strega	strega	k1gFnSc1	strega
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
inspirována	inspirovat	k5eAaBmNgFnS	inspirovat
schválením	schválení	k1gNnSc7	schválení
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
drogách	droga	k1gFnPc6	droga
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
přísný	přísný	k2eAgMnSc1d1	přísný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
měl	mít	k5eAaImAgInS	mít
jen	jen	k9	jen
slabé	slabý	k2eAgInPc4d1	slabý
důsledky	důsledek	k1gInPc4	důsledek
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
základem	základ	k1gInSc7	základ
fraška	fraška	k1gFnSc1	fraška
a	a	k8xC	a
obětí	oběť	k1gFnSc7	oběť
satiry	satira	k1gFnSc2	satira
je	být	k5eAaImIp3nS	být
tu	ten	k3xDgFnSc4	ten
krátkozrakost	krátkozrakost	k1gFnSc4	krátkozrakost
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
prokázala	prokázat	k5eAaPmAgFnS	prokázat
vláda	vláda	k1gFnSc1	vláda
sepsaním	sepsaní	k1gNnSc7	sepsaní
zákona	zákon	k1gInSc2	zákon
za	za	k7c2	za
podpory	podpora	k1gFnSc2	podpora
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
bylo	být	k5eAaImAgNnS	být
pětisté	pětistý	k2eAgNnSc1d1	pětistý
výročí	výročí	k1gNnSc1	výročí
objevení	objevení	k1gNnSc1	objevení
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
Fo	Fo	k1gMnSc1	Fo
ho	on	k3xPp3gInSc4	on
převyprávěl	převyprávět	k5eAaPmAgMnS	převyprávět
svým	svůj	k3xOyFgInSc7	svůj
způsobem	způsob	k1gInSc7	způsob
v	v	k7c6	v
Johan	Johan	k1gMnSc1	Johan
Padan	Padan	k1gInSc1	Padan
a	a	k8xC	a
la	la	k1gNnSc1	la
descoverta	descovert	k1gMnSc2	descovert
de	de	k?	de
le	le	k?	le
Americhe	Americhe	k1gInSc1	Americhe
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
chudák	chudák	k1gMnSc1	chudák
z	z	k7c2	z
provincie	provincie	k1gFnSc2	provincie
Bergamo	Bergamo	k1gNnSc1	Bergamo
pokoušející	pokoušející	k2eAgNnSc1d1	pokoušející
se	se	k3xPyFc4	se
uprchnout	uprchnout	k5eAaPmF	uprchnout
před	před	k7c7	před
inkvizicí	inkvizice	k1gFnSc7	inkvizice
utíká	utíkat	k5eAaImIp3nS	utíkat
z	z	k7c2	z
Benátek	Benátky	k1gFnPc2	Benátky
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
a	a	k8xC	a
po	po	k7c6	po
sérii	série	k1gFnSc6	série
příhod	příhoda	k1gFnPc2	příhoda
do	do	k7c2	do
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Tady	tady	k6eAd1	tady
Fo	Fo	k1gMnSc1	Fo
užil	užít	k5eAaPmAgMnS	užít
úskoku	úskok	k1gInSc3	úskok
náhodného	náhodný	k2eAgMnSc2d1	náhodný
hrdiny	hrdina	k1gMnSc2	hrdina
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
hraje	hrát	k5eAaImIp3nS	hrát
svou	svůj	k3xOyFgFnSc4	svůj
malou	malý	k2eAgFnSc4d1	malá
roli	role	k1gFnSc4	role
v	v	k7c6	v
příběhu	příběh	k1gInSc6	příběh
větším	veliký	k2eAgInSc6d2	veliký
než	než	k8xS	než
on	on	k3xPp3gMnSc1	on
<g/>
.	.	kIx.	.
</s>
<s>
Komedie	komedie	k1gFnSc1	komedie
má	mít	k5eAaImIp3nS	mít
mnoho	mnoho	k4c4	mnoho
společných	společný	k2eAgInPc2d1	společný
bodů	bod	k1gInPc2	bod
s	s	k7c7	s
Mistero	Mistero	k1gNnSc4	Mistero
buffo	buffo	k2eAgMnPc1d1	buffo
<g/>
,	,	kIx,	,
také	také	k9	také
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
využíván	využívat	k5eAaImNgInS	využívat
zábavný	zábavný	k2eAgInSc1d1	zábavný
pádsko-benátský	pádskoenátský	k2eAgInSc1d1	pádsko-benátský
grammelot	grammelot	k1gInSc1	grammelot
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
samotné	samotný	k2eAgNnSc1d1	samotné
sdělení	sdělení	k1gNnSc1	sdělení
podáno	podat	k5eAaPmNgNnS	podat
zábavnou	zábavný	k2eAgFnSc7d1	zábavná
formou	forma	k1gFnSc7	forma
bajky	bajka	k1gFnSc2	bajka
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgFnPc4	který
komik	komik	k1gMnSc1	komik
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
svůj	svůj	k3xOyFgInSc4	svůj
nepokřivený	pokřivený	k2eNgInSc4d1	nepokřivený
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
scéně	scéna	k1gFnSc6	scéna
sám	sám	k3xTgMnSc1	sám
Fo	Fo	k1gMnSc1	Fo
a	a	k8xC	a
hrál	hrát	k5eAaImAgMnS	hrát
všechny	všechen	k3xTgFnPc4	všechen
postavy	postava	k1gFnPc4	postava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
Foovo	Foovo	k1gNnSc1	Foovo
dílo	dílo	k1gNnSc4	dílo
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
dvěma	dva	k4xCgInPc7	dva
paralelními	paralelní	k2eAgInPc7d1	paralelní
směry	směr	k1gInPc7	směr
<g/>
,	,	kIx,	,
fraškovitou	fraškovitý	k2eAgFnSc7d1	fraškovitá
komedií	komedie	k1gFnSc7	komedie
(	(	kIx(	(
<g/>
Il	Il	k1gFnSc1	Il
diavolo	diavola	k1gFnSc5	diavola
con	con	k?	con
le	le	k?	le
zinne	zinnout	k5eAaImIp3nS	zinnout
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
a	a	k8xC	a
monology	monolog	k1gInPc7	monolog
vystavěnými	vystavěný	k2eAgInPc7d1	vystavěný
na	na	k7c6	na
modelu	model	k1gInSc6	model
Mistero	Mistero	k1gNnSc4	Mistero
buffo	buffo	k2eAgFnSc2d1	buffo
(	(	kIx(	(
<g/>
od	od	k7c2	od
Lu	Lu	k1gFnSc2	Lu
santo	santo	k1gNnSc1	santo
jullare	jullar	k1gMnSc5	jullar
Francesco	Francesco	k6eAd1	Francesco
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
až	až	k6eAd1	až
po	po	k7c6	po
Il	Il	k1gFnSc6	Il
tempio	tempio	k6eAd1	tempio
degli	degnout	k5eAaPmAgMnP	degnout
uomini	uomin	k2eAgMnPc1d1	uomin
liberi	liber	k1gMnPc1	liber
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nástup	nástup	k1gInSc1	nástup
druhé	druhý	k4xOgFnSc2	druhý
Berlusconiho	Berlusconiha	k1gFnSc5	Berlusconiha
vlády	vláda	k1gFnSc2	vláda
ho	on	k3xPp3gMnSc4	on
znovu	znovu	k6eAd1	znovu
podnítil	podnítit	k5eAaPmAgMnS	podnítit
k	k	k7c3	k
občanským	občanský	k2eAgNnPc3d1	občanské
a	a	k8xC	a
politickým	politický	k2eAgNnPc3d1	politické
dílům	dílo	k1gNnPc3	dílo
<g/>
,	,	kIx,	,
satirickým	satirický	k2eAgMnSc7d1	satirický
přímo	přímo	k6eAd1	přímo
vůči	vůči	k7c3	vůči
Berlusconimu	Berlusconimo	k1gNnSc3	Berlusconimo
<g/>
,	,	kIx,	,
od	od	k7c2	od
Ubu	Ubu	k1gFnSc2	Ubu
rois	rois	k1gInSc1	rois
<g/>
,	,	kIx,	,
Ubu	Ubu	k1gFnSc1	Ubu
bas	basa	k1gFnPc2	basa
po	po	k7c6	po
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Anomalo	Anomalo	k1gMnSc2	Anomalo
Bicefalo	Bicefalo	k1gMnSc2	Bicefalo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
napsal	napsat	k5eAaPmAgMnS	napsat
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
Francou	Francý	k2eAgFnSc4d1	Francý
Rame	Rame	k1gFnSc4	Rame
<g/>
.	.	kIx.	.
</s>
<s>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Anomalo	Anomalo	k1gMnSc2	Anomalo
Bicefalo	Bicefalo	k1gMnSc2	Bicefalo
je	být	k5eAaImIp3nS	být
komedie	komedie	k1gFnSc1	komedie
o	o	k7c6	o
soudních	soudní	k2eAgFnPc6d1	soudní
<g/>
,	,	kIx,	,
politických	politický	k2eAgFnPc6d1	politická
a	a	k8xC	a
ekonomických	ekonomický	k2eAgFnPc6d1	ekonomická
událostech	událost	k1gFnPc6	událost
kolem	kolem	k7c2	kolem
bývalého	bývalý	k2eAgMnSc2d1	bývalý
italského	italský	k2eAgMnSc2d1	italský
premiéra	premiér	k1gMnSc2	premiér
Berlusconiho	Berlusconi	k1gMnSc2	Berlusconi
<g/>
,	,	kIx,	,
do	do	k7c2	do
nějž	jenž	k3xRgNnSc2	jenž
Fo	Fo	k1gFnSc1	Fo
vtěluje	vtělovat	k5eAaImIp3nS	vtělovat
expremiéra	expremiér	k1gMnSc4	expremiér
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
po	po	k7c6	po
nehodě	nehoda	k1gFnSc6	nehoda
ztratil	ztratit	k5eAaPmAgMnS	ztratit
paměť	paměť	k1gFnSc4	paměť
a	a	k8xC	a
kterému	který	k3yIgInSc3	který
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nS	podařit
znovu	znovu	k6eAd1	znovu
ji	on	k3xPp3gFnSc4	on
získat	získat	k5eAaPmF	získat
<g/>
,	,	kIx,	,
když	když	k8xS	když
přizná	přiznat	k5eAaPmIp3nS	přiznat
pravdu	pravda	k1gFnSc4	pravda
o	o	k7c6	o
svých	svůj	k3xOyFgInPc6	svůj
činech	čin	k1gInPc6	čin
<g/>
.	.	kIx.	.
</s>
<s>
Komedie	komedie	k1gFnSc1	komedie
nemohla	moct	k5eNaImAgFnS	moct
být	být	k5eAaImF	být
po	po	k7c4	po
určitou	určitý	k2eAgFnSc4d1	určitá
vysílána	vysílat	k5eAaImNgFnS	vysílat
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
kvůli	kvůli	k7c3	kvůli
žalobě	žaloba	k1gFnSc3	žaloba
ministra	ministr	k1gMnSc2	ministr
Marcella	Marcell	k1gMnSc2	Marcell
Dell	Dell	kA	Dell
<g/>
'	'	kIx"	'
<g/>
Ultri	Ultre	k1gFnSc4	Ultre
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
citovala	citovat	k5eAaBmAgFnS	citovat
některé	některý	k3yIgInPc4	některý
jeho	jeho	k3xOp3gInPc4	jeho
soudní	soudní	k2eAgInPc4d1	soudní
případy	případ	k1gInPc4	případ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Fo	Fo	k1gMnSc1	Fo
titul	titul	k1gInSc4	titul
honoris	honoris	k1gFnSc2	honoris
causa	causa	k1gFnSc1	causa
na	na	k7c6	na
pařížské	pařížský	k2eAgFnSc6d1	Pařížská
univerzitě	univerzita	k1gFnSc6	univerzita
Sorbona	Sorbona	k1gFnSc1	Sorbona
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
mu	on	k3xPp3gInSc3	on
stejné	stejný	k2eAgFnPc4d1	stejná
pocty	pocta	k1gFnPc4	pocta
dostalo	dostat	k5eAaPmAgNnS	dostat
i	i	k9	i
od	od	k7c2	od
římské	římský	k2eAgFnSc2d1	římská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
kandidátem	kandidát	k1gMnSc7	kandidát
na	na	k7c6	na
starostu	starosta	k1gMnSc4	starosta
Milána	Milán	k1gInSc2	Milán
v	v	k7c6	v
primárkách	primárky	k1gFnPc6	primárky
středo-levicové	středoevicový	k2eAgFnSc2d1	středo-levicová
strany	strana	k1gFnSc2	strana
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Unione	union	k1gInSc5	union
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
skončil	skončit	k5eAaPmAgInS	skončit
druhý	druhý	k4xOgInSc1	druhý
za	za	k7c7	za
Brunem	Bruno	k1gMnSc7	Bruno
Ferrantem	Ferrant	k1gMnSc7	Ferrant
s	s	k7c7	s
23,1	[number]	k4	23,1
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
následných	následný	k2eAgFnPc2d1	následná
milánských	milánský	k2eAgFnPc2d1	Milánská
komunálních	komunální	k2eAgFnPc2d1	komunální
voleb	volba	k1gFnPc2	volba
tak	tak	k6eAd1	tak
šel	jít	k5eAaImAgMnS	jít
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
kandidátkou	kandidátka	k1gFnSc7	kandidátka
<g/>
;	;	kIx,	;
ta	ten	k3xDgFnSc1	ten
získala	získat	k5eAaPmAgFnS	získat
jen	jen	k9	jen
2,12	[number]	k4	2,12
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
to	ten	k3xDgNnSc1	ten
stačilo	stačit	k5eAaBmAgNnS	stačit
ke	k	k7c3	k
zvolení	zvolení	k1gNnSc3	zvolení
Daria	Daria	k1gFnSc1	Daria
Fo	Fo	k1gMnSc7	Fo
zastupitelem	zastupitel	k1gMnSc7	zastupitel
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Foově	Foova	k1gFnSc6	Foova
volební	volební	k2eAgFnSc3d1	volební
kampani	kampaň	k1gFnSc3	kampaň
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
dokument	dokument	k1gInSc1	dokument
Io	Io	k1gFnSc2	Io
non	non	k?	non
sono	sono	k1gMnSc1	sono
un	un	k?	un
moderato	moderato	k6eAd1	moderato
Andrey	Andrea	k1gFnSc2	Andrea
Nobileho	Nobile	k1gMnSc2	Nobile
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2016	[number]	k4	2016
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
90	[number]	k4	90
let	léto	k1gNnPc2	léto
v	v	k7c6	v
Miláně	Milán	k1gInSc6	Milán
<g/>
.	.	kIx.	.
</s>
<s>
Archandělé	archanděl	k1gMnPc1	archanděl
nehrají	hrát	k5eNaImIp3nP	hrát
biliár	biliár	k1gInSc4	biliár
(	(	kIx(	(
<g/>
Gli	Gli	k1gFnSc4	Gli
arcangeli	arcanget	k5eAaPmAgMnP	arcanget
non	non	k?	non
giocano	giocana	k1gFnSc5	giocana
al	ala	k1gFnPc2	ala
flipper	flipper	k1gInSc1	flipper
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
<g/>
;	;	kIx,	;
překlad	překlad	k1gInSc1	překlad
Eva	Eva	k1gFnSc1	Eva
Bezděková	Bezděková	k1gFnSc1	Bezděková
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
Isabella	Isabella	k1gFnSc1	Isabella
<g/>
,	,	kIx,	,
tři	tři	k4xCgFnPc4	tři
karavely	karavela	k1gFnPc4	karavela
a	a	k8xC	a
podfukář	podfukář	k1gMnSc1	podfukář
(	(	kIx(	(
<g/>
Isabella	Isabella	k1gMnSc1	Isabella
<g/>
,	,	kIx,	,
tre	tre	k?	tre
caravelle	caravelle	k1gInSc1	caravelle
e	e	k0	e
un	un	k?	un
cacciaballe	cacciaballe	k1gNnPc7	cacciaballe
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
<g/>
;	;	kIx,	;
překlad	překlad	k1gInSc1	překlad
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Digrin	Digrin	k1gInSc1	Digrin
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
Bílková	Bílková	k1gFnSc1	Bílková
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
Nepokradeš	pokrást	k5eNaPmIp2nS	pokrást
(	(	kIx(	(
<g/>
Settimo	Settima	k1gFnSc5	Settima
<g/>
:	:	kIx,	:
ruba	rub	k2eAgMnSc4d1	rub
un	un	k?	un
po	po	k7c4	po
<g/>
'	'	kIx"	'
meno	meno	k1gNnSc4	meno
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
<g/>
;	;	kIx,	;
překlad	překlad	k1gInSc1	překlad
Eva	Eva	k1gFnSc1	Eva
Burešová	Burešová	k1gFnSc1	Burešová
-	-	kIx~	-
Ruxová	Ruxová	k1gFnSc1	Ruxová
<g/>
)	)	kIx)	)
Mistero	Mistero	k1gNnSc1	Mistero
buffo	buffo	k2eAgNnSc1d1	buffo
(	(	kIx(	(
<g/>
Mistero	Mistero	k1gNnSc1	Mistero
buffo	buffo	k2eAgInPc1d1	buffo
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
;	;	kIx,	;
překlad	překlad	k1gInSc1	překlad
Alena	Alena	k1gFnSc1	Alena
Světlíková	Světlíková	k1gFnSc1	Světlíková
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
pro	pro	k7c4	pro
Divadlo	divadlo	k1gNnSc4	divadlo
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
<g/>
)	)	kIx)	)
Otevřené	otevřený	k2eAgNnSc1d1	otevřené
manželství	manželství	k1gNnSc1	manželství
(	(	kIx(	(
<g/>
Coppia	Coppia	k1gFnSc1	Coppia
aperta	aperta	k1gFnSc1	aperta
quasi	quasi	k6eAd1	quasi
spalancata	spalancat	k2eAgFnSc1d1	spalancat
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
;	;	kIx,	;
překlad	překlad	k1gInSc1	překlad
Věra	Věra	k1gFnSc1	Věra
Vrbová	Vrbová	k1gFnSc1	Vrbová
<g/>
)	)	kIx)	)
Jak	jak	k6eAd1	jak
byla	být	k5eAaImAgFnS	být
Františka	Františka	k1gFnSc1	Františka
celá	celý	k2eAgFnSc1d1	celá
unešená	unešený	k2eAgFnSc1d1	unešená
(	(	kIx(	(
<g/>
Il	Il	k1gFnSc1	Il
ratto	ratto	k1gNnSc1	ratto
della	dell	k1gMnSc2	dell
Francesca	Francescus	k1gMnSc2	Francescus
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
;	;	kIx,	;
překlad	překlad	k1gInSc1	překlad
Jiří	Jiří	k1gMnSc1	Jiří
Matějíček	Matějíček	k1gMnSc1	Matějíček
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Svatý	svatý	k2eAgMnSc1d1	svatý
komediant	komediant	k1gMnSc1	komediant
František	František	k1gMnSc1	František
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Lu	Lu	k1gMnSc5	Lu
Santo	Santa	k1gMnSc5	Santa
jullare	jullar	k1gMnSc5	jullar
Francesco	Francesca	k1gMnSc5	Francesca
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
;	;	kIx,	;
překlad	překlad	k1gInSc1	překlad
Marina	Marina	k1gFnSc1	Marina
Castiellová	Castiellová	k1gFnSc1	Castiellová
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Objevení	objevení	k1gNnSc4	objevení
Ameriky	Amerika	k1gFnSc2	Amerika
Johanem	Johan	k1gMnSc7	Johan
Padanem	Padan	k1gMnSc7	Padan
(	(	kIx(	(
<g/>
Johan	Johan	k1gMnSc1	Johan
Padan	Padan	k1gMnSc1	Padan
a	a	k8xC	a
la	la	k1gNnPc1	la
descoverta	descovert	k1gMnSc2	descovert
de	de	k?	de
la	la	k1gNnSc1	la
Americhe	Americh	k1gInSc2	Americh
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
;	;	kIx,	;
překlad	překlad	k1gInSc1	překlad
Veronika	Veronika	k1gFnSc1	Veronika
Valentová	Valentová	k1gFnSc1	Valentová
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
–	–	k?	–
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
První	první	k4xOgInSc1	první
Ježíškův	Ježíškův	k2eAgInSc1d1	Ježíškův
zázrak	zázrak	k1gInSc1	zázrak
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Il	Il	k1gMnSc6	Il
Primo	primo	k1gNnSc1	primo
miracolo	miracola	k1gFnSc5	miracola
di	di	k?	di
Gesú	Gesú	k1gMnSc1	Gesú
bambino	bambino	k1gNnSc1	bambino
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
cyklu	cyklus	k1gInSc2	cyklus
Mistero	Mistero	k1gNnSc1	Mistero
buffo	buffo	k2eAgNnSc7d1	buffo
<g/>
;	;	kIx,	;
překlad	překlad	k1gInSc1	překlad
Veronika	Veronika	k1gFnSc1	Veronika
Valentová	Valentová	k1gFnSc1	Valentová
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
–	–	k?	–
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Marcolfa	Marcolf	k1gMnSc2	Marcolf
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
Marcolfa	Marcolf	k1gMnSc2	Marcolf
<g/>
,	,	kIx,	,
1958	[number]	k4	1958
<g/>
;	;	kIx,	;
překlad	překlad	k1gInSc1	překlad
Matteo	Matteo	k6eAd1	Matteo
Difumato	Difumat	k2eAgNnSc4d1	Difumat
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Alenka	Alenka	k1gFnSc1	Alenka
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
nedivů	nediv	k1gInPc2	nediv
(	(	kIx(	(
<g/>
Alice	Alice	k1gFnSc1	Alice
nel	nel	k?	nel
paese	paese	k6eAd1	paese
senza	senza	k2eAgFnSc1d1	senza
meraviglie	meraviglie	k1gFnSc1	meraviglie
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
;	;	kIx,	;
překlad	překlad	k1gInSc1	překlad
Kateřina	Kateřina	k1gFnSc1	Kateřina
Bohadlová	Bohadlový	k2eAgFnSc1d1	Bohadlová
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
