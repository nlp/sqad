<p>
<s>
Obština	Obštin	k2eAgFnSc1d1	Obštin
Gorna	Goren	k2eAgFnSc1d1	Gorna
Orjachovica	Orjachovica	k1gFnSc1	Orjachovica
(	(	kIx(	(
<g/>
bulharsky	bulharsky	k6eAd1	bulharsky
О	О	k?	О
Г	Г	k?	Г
О	О	k?	О
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bulharská	bulharský	k2eAgFnSc1d1	bulharská
jednotka	jednotka	k1gFnSc1	jednotka
územní	územní	k2eAgFnSc2d1	územní
samosprávy	samospráva	k1gFnSc2	samospráva
ve	v	k7c6	v
Velikotărnovské	Velikotărnovský	k2eAgFnSc6d1	Velikotărnovský
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
středním	střední	k2eAgNnSc6d1	střední
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
v	v	k7c4	v
Předbalkánu	Předbalkán	k2eAgFnSc4d1	Předbalkán
<g/>
.	.	kIx.	.
</s>
<s>
Správním	správní	k2eAgNnSc7d1	správní
střediskem	středisko	k1gNnSc7	středisko
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
Gorna	Gorn	k1gMnSc2	Gorn
Orjachovica	Orjachovicus	k1gMnSc2	Orjachovicus
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
něj	on	k3xPp3gMnSc2	on
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
obština	obština	k1gFnSc1	obština
1	[number]	k4	1
město	město	k1gNnSc1	město
a	a	k8xC	a
12	[number]	k4	12
vesnic	vesnice	k1gFnPc2	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
zhruba	zhruba	k6eAd1	zhruba
45	[number]	k4	45
tisíc	tisíc	k4xCgInPc2	tisíc
stálých	stálý	k2eAgMnPc2d1	stálý
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sídla	sídlo	k1gNnSc2	sídlo
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Sousední	sousední	k2eAgInPc1d1	sousední
obštiny	obštin	k1gInPc1	obštin
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
obštině	obština	k1gFnSc6	obština
žije	žít	k5eAaImIp3nS	žít
44	[number]	k4	44
776	[number]	k4	776
stálých	stálý	k2eAgMnPc2d1	stálý
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
včetně	včetně	k7c2	včetně
přechodně	přechodně	k6eAd1	přechodně
hlášených	hlášený	k2eAgMnPc2d1	hlášený
obyvatel	obyvatel	k1gMnPc2	obyvatel
52	[number]	k4	52
677	[number]	k4	677
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítáni	sčítat	k5eAaImNgMnP	sčítat
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2011	[number]	k4	2011
bylo	být	k5eAaImAgNnS	být
národnostní	národnostní	k2eAgNnSc1d1	národnostní
složení	složení	k1gNnSc1	složení
následující	následující	k2eAgMnSc1d1	následující
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
