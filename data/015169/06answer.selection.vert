<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
Praze	Praha	k1gFnSc6
jako	jako	k8xC,k8xS
druhý	druhý	k4xOgMnSc1
z	z	k7c2
pěti	pět	k4xCc2
synů	syn	k1gMnPc2
sochaře	sochař	k1gMnSc2
a	a	k8xC
modeléra	modelér	k1gMnSc2
porcelánu	porcelán	k1gInSc2
Arnošta	Arnošt	k1gMnSc2
Poppa	Popp	k1gMnSc2
(	(	kIx(
<g/>
1819	#num#	k4
<g/>
–	–	k?
<g/>
1883	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přišlého	přišlý	k2eAgMnSc4d1
z	z	k7c2
Koburgu	Koburg	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
jeho	jeho	k3xOp3gFnPc1
české	český	k2eAgFnPc1d1
manželky	manželka	k1gFnPc1
Aloisie	Aloisie	k1gFnSc2
<g/>
,	,	kIx,
rozené	rozený	k2eAgFnSc2d1
Bartoníčkové	Bartoníčková	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gMnSc1
starší	starý	k2eAgMnSc1d2
bratr	bratr	k1gMnSc1
Karel	Karel	k1gMnSc1
Popp	Popp	k1gMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
fotografem	fotograf	k1gMnSc7
<g/>
.	.	kIx.
</s>