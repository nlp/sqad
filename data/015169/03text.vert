<s>
Antonín	Antonín	k1gMnSc1
Popp	Popp	k1gMnSc1
</s>
<s>
Antonín	Antonín	k1gMnSc1
Popp	Popp	k1gMnSc1
Narození	narození	k1gNnSc4
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1850	#num#	k4
PrahaRakouské	PrahaRakouský	k2eAgNnSc4d1
císařství	císařství	k1gNnSc4
Rakouské	rakouský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1915	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
64	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
PrahaRakousko-Uhersko	PrahaRakousko-Uherska	k1gMnSc5
Rakousko-Uhersko	Rakousko-Uherska	k1gMnSc5
Povolání	povolání	k1gNnSc5
</s>
<s>
sochař	sochař	k1gMnSc1
<g/>
,	,	kIx,
medailér	medailér	k1gMnSc1
a	a	k8xC
učitel	učitel	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
databázi	databáze	k1gFnSc6
Národní	národní	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Antonín	Antonín	k1gMnSc1
Popp	Popp	k1gMnSc1
(	(	kIx(
<g/>
30	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1850	#num#	k4
Praha	Praha	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
–	–	k?
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1915	#num#	k4
Praha	Praha	k1gFnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
sochař	sochař	k1gMnSc1
<g/>
,	,	kIx,
medailér	medailér	k1gMnSc1
a	a	k8xC
pedagog	pedagog	k1gMnSc1
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
Praze	Praha	k1gFnSc6
jako	jako	k8xC,k8xS
druhý	druhý	k4xOgMnSc1
z	z	k7c2
pěti	pět	k4xCc2
synů	syn	k1gMnPc2
sochaře	sochař	k1gMnSc2
a	a	k8xC
modeléra	modelér	k1gMnSc2
porcelánu	porcelán	k1gInSc2
Arnošta	Arnošt	k1gMnSc2
Poppa	Popp	k1gMnSc2
(	(	kIx(
<g/>
1819	#num#	k4
<g/>
–	–	k?
<g/>
1883	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přišlého	přišlý	k2eAgMnSc4d1
z	z	k7c2
Koburgu	Koburg	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
jeho	jeho	k3xOp3gFnPc1
české	český	k2eAgFnPc1d1
manželky	manželka	k1gFnPc1
Aloisie	Aloisie	k1gFnSc2
<g/>
,	,	kIx,
rozené	rozený	k2eAgFnSc2d1
Bartoníčkové	Bartoníčková	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gMnSc1
starší	starý	k2eAgMnSc1d2
bratr	bratr	k1gMnSc1
Karel	Karel	k1gMnSc1
Popp	Popp	k1gMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
fotografem	fotograf	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc1
díla	dílo	k1gNnPc1
bývají	bývat	k5eAaImIp3nP
často	často	k6eAd1
zaměňována	zaměňován	k2eAgFnSc1d1
s	s	k7c7
otcovými	otcův	k2eAgFnPc7d1
<g/>
,	,	kIx,
díky	díky	k7c3
práci	práce	k1gFnSc3
na	na	k7c6
společných	společný	k2eAgFnPc6d1
zakázkách	zakázka	k1gFnPc6
a	a	k8xC
shodné	shodný	k2eAgFnSc3d1
signatuře	signatura	k1gFnSc3
AP	ap	kA
(	(	kIx(
<g/>
jindy	jindy	k6eAd1
signoval	signovat	k5eAaBmAgMnS
APP	APP	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyučil	vyučit	k5eAaPmAgMnS
se	se	k3xPyFc4
řemeslu	řemeslo	k1gNnSc3
v	v	k7c6
dílně	dílna	k1gFnSc6
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
<g/>
,	,	kIx,
zároveň	zároveň	k6eAd1
navštěvoval	navštěvovat	k5eAaImAgMnS
průmyslovou	průmyslový	k2eAgFnSc4d1
školu	škola	k1gFnSc4
Jednoty	jednota	k1gFnSc2
pro	pro	k7c4
povzbuzení	povzbuzení	k1gNnSc4
průmyslu	průmysl	k1gInSc2
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1870	#num#	k4
začal	začít	k5eAaPmAgMnS
studovat	studovat	k5eAaImF
kresbu	kresba	k1gFnSc4
na	na	k7c4
Akademii	akademie	k1gFnSc4
výtvarných	výtvarný	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
studia	studio	k1gNnSc2
nedokončil	dokončit	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podnikl	podniknout	k5eAaPmAgMnS
studijní	studijní	k2eAgFnPc4d1
cesty	cesta	k1gFnPc4
do	do	k7c2
Mnichova	Mnichov	k1gInSc2
a	a	k8xC
Vídně	Vídeň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Brzy	brzy	k6eAd1
si	se	k3xPyFc3
otevřel	otevřít	k5eAaPmAgInS
vlastní	vlastní	k2eAgInSc4d1
ateliér	ateliér	k1gInSc4
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proslavila	proslavit	k5eAaPmAgFnS
ho	on	k3xPp3gNnSc4
monumentální	monumentální	k2eAgFnSc1d1
alegorická	alegorický	k2eAgFnSc1d1
<g/>
,	,	kIx,
historická	historický	k2eAgFnSc1d1
a	a	k8xC
portrétní	portrétní	k2eAgNnPc1d1
díla	dílo	k1gNnPc1
<g/>
,	,	kIx,
jimiž	jenž	k3xRgNnPc7
se	se	k3xPyFc4
zúčastnil	zúčastnit	k5eAaPmAgMnS
výzdoby	výzdoba	k1gFnPc4
veřejných	veřejný	k2eAgFnPc2d1
budov	budova	k1gFnPc2
<g/>
,	,	kIx,
ponejvíce	ponejvíce	k6eAd1
pražských	pražský	k2eAgNnPc2d1
<g/>
,	,	kIx,
zejména	zejména	k9
Národního	národní	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
(	(	kIx(
<g/>
32	#num#	k4
portrétních	portrétní	k2eAgInPc2d1
medailonů	medailon	k1gInPc2
českých	český	k2eAgMnPc2d1
králů	král	k1gMnPc2
<g/>
)	)	kIx)
a	a	k8xC
Národního	národní	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
pedagog	pedagog	k1gMnSc1
vyučoval	vyučovat	k5eAaImAgMnS
modelování	modelování	k1gNnSc1
na	na	k7c6
České	český	k2eAgFnSc6d1
technice	technika	k1gFnSc6
(	(	kIx(
<g/>
ČVUT	ČVUT	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
roku	rok	k1gInSc2
1896	#num#	k4
dosáhl	dosáhnout	k5eAaPmAgInS
hodnosti	hodnost	k1gFnSc2
docenta	docent	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Učil	učit	k5eAaImAgMnS,k5eAaPmAgMnS
rovněž	rovněž	k9
na	na	k7c4
UMPRUM	umprum	k1gInSc4
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
nejproduktivnějších	produktivní	k2eAgMnPc2d3
českých	český	k2eAgMnPc2d1
sochařů	sochař	k1gMnPc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
svou	svůj	k3xOyFgFnSc7
technickou	technický	k2eAgFnSc7d1
zdatností	zdatnost	k1gFnSc7
předčil	předčít	k5eAaPmAgInS,k5eAaBmAgInS
mnohé	mnohý	k2eAgMnPc4d1
i	i	k8xC
známější	známý	k2eAgMnPc4d2
autory	autor	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS
roku	rok	k1gInSc2
1915	#num#	k4
a	a	k8xC
byl	být	k5eAaImAgMnS
pohřben	pohřbít	k5eAaPmNgMnS
na	na	k7c6
Vinohradském	vinohradský	k2eAgInSc6d1
hřbitově	hřbitov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
bronzové	bronzový	k2eAgFnPc4d1
busty	busta	k1gFnPc4
Františka	František	k1gMnSc2
Škroupa	Škroup	k1gMnSc2
<g/>
,	,	kIx,
Josefa	Josef	k1gMnSc2
Ressela	Ressel	k1gMnSc2
<g/>
,	,	kIx,
Pavla	Pavel	k1gMnSc2
Josefa	Josef	k1gMnSc2
Šafaříka	Šafařík	k1gMnSc2
<g/>
,	,	kIx,
hraběte	hrabě	k1gMnSc2
F.	F.	kA
A.	A.	kA
Kolovrata	Kolovrat	k1gMnSc2
Libštejnského	Libštejnský	k1gMnSc2
<g/>
,	,	kIx,
Františka	František	k1gMnSc2
Palackého	Palacký	k1gMnSc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
v	v	k7c6
Národním	národní	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
reliéf	reliéf	k1gInSc1
Císař	Císař	k1gMnSc1
Rudolf	Rudolf	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
v	v	k7c6
kruhu	kruh	k1gInSc6
svých	svůj	k3xOyFgMnPc2
umělců	umělec	k1gMnPc2
a	a	k8xC
vědců	vědec	k1gMnPc2
<g/>
,	,	kIx,
model	model	k1gInSc4
pro	pro	k7c4
Národní	národní	k2eAgNnSc4d1
muzeum	muzeum	k1gNnSc4
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
reliéf	reliéf	k1gInSc1
císař	císař	k1gMnSc1
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
se	s	k7c7
zástupci	zástupce	k1gMnPc7
čtyř	čtyři	k4xCgInPc2
stavů	stav	k1gInPc2
<g/>
,	,	kIx,
model	model	k1gInSc1
pro	pro	k7c4
Národní	národní	k2eAgNnSc4d1
muzeum	muzeum	k1gNnSc4
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
kamenný	kamenný	k2eAgInSc1d1
reliéf	reliéf	k1gInSc1
na	na	k7c6
budově	budova	k1gFnSc6
Okresního	okresní	k2eAgInSc2d1
domu	dům	k1gInSc2
ve	v	k7c6
Slaném	Slaný	k1gInSc6
vytvořil	vytvořit	k5eAaPmAgMnS
sochař	sochař	k1gMnSc1
Adolf	Adolf	k1gMnSc1
Havel	Havel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
portrétní	portrétní	k2eAgInPc1d1
medailony	medailon	k1gInPc1
českých	český	k2eAgMnPc2d1
králů	král	k1gMnPc2
Karla	Karel	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Jiřího	Jiří	k1gMnSc4
z	z	k7c2
Poděbrad	Poděbrady	k1gInPc2
<g/>
,	,	kIx,
Rudolfa	Rudolf	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Ferdinanda	Ferdinand	k1gMnSc2
V.	V.	kA
a	a	k8xC
další	další	k2eAgMnPc1d1
na	na	k7c6
fasádě	fasáda	k1gFnSc6
<g/>
,	,	kIx,
Národní	národní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
</s>
<s>
alegorie	alegorie	k1gFnSc1
Historie	historie	k1gFnSc1
<g/>
,	,	kIx,
Archeologie	archeologie	k1gFnSc1
na	na	k7c4
průčelí	průčelí	k1gNnSc4
a	a	k8xC
Géniové	génius	k1gMnPc1
nesou	nést	k5eAaImIp3nP
českou	český	k2eAgFnSc4d1
královskou	královský	k2eAgFnSc4d1
korunu	koruna	k1gFnSc4
na	na	k7c6
atice	atika	k1gFnSc6
Národního	národní	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
sochy	socha	k1gFnPc1
na	na	k7c4
průčelí	průčelí	k1gNnSc4
Uměleckoprůmyslového	uměleckoprůmyslový	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
alegorické	alegorický	k2eAgNnSc4d1
sousoší	sousoší	k1gNnSc4
Svornost	svornost	k1gFnSc1
a	a	k8xC
Ušlechtilost	ušlechtilost	k1gFnSc1
s	s	k7c7
českým	český	k2eAgMnSc7d1
lvem	lev	k1gMnSc7
na	na	k7c6
štítu	štít	k1gInSc6
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
středu	střed	k1gInSc6
v	v	k7c6
průčelí	průčelí	k1gNnSc6
Měšťanské	měšťanský	k2eAgFnSc2d1
besedy	beseda	k1gFnSc2
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
a	a	k8xC
plastická	plastický	k2eAgFnSc1d1
výzdoba	výzdoba	k1gFnSc1
Velkého	velký	k2eAgInSc2d1
sálu	sál	k1gInSc2
téže	týž	k3xTgFnSc2,k3xDgFnSc2
budovy	budova	k1gFnSc2
</s>
<s>
část	část	k1gFnSc1
plastické	plastický	k2eAgFnSc2d1
výzdoby	výzdoba	k1gFnSc2
dvorany	dvorana	k1gFnSc2
Městské	městský	k2eAgFnSc2d1
spořitelny	spořitelna	k1gFnSc2
(	(	kIx(
<g/>
Rytířská	rytířský	k2eAgFnSc1d1
ul	ul	kA
<g/>
.	.	kIx.
29	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
tepaná	tepaný	k2eAgFnSc1d1
plastika	plastika	k1gFnSc1
Génia	génius	k1gMnSc2
se	s	k7c7
lvem	lev	k1gMnSc7
na	na	k7c6
budově	budova	k1gFnSc6
České	český	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
banky	banka	k1gFnSc2
(	(	kIx(
<g/>
původně	původně	k6eAd1
Živnostenské	živnostenský	k2eAgFnSc2d1
banky	banka	k1gFnSc2
<g/>
)	)	kIx)
(	(	kIx(
<g/>
ul	ul	kA
<g/>
.	.	kIx.
Na	na	k7c6
příkopech	příkop	k1gInPc6
<g/>
)	)	kIx)
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
kamenné	kamenný	k2eAgFnPc1d1
alegorické	alegorický	k2eAgFnPc1d1
sochy	socha	k1gFnPc1
Věda	věda	k1gFnSc1
a	a	k8xC
Práce	práce	k1gFnSc1
v	v	k7c6
nikách	nika	k1gFnPc6
hlavního	hlavní	k2eAgInSc2d1
vchodu	vchod	k1gInSc2
budovy	budova	k1gFnSc2
ČVUT	ČVUT	kA
na	na	k7c6
Karlově	Karlův	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
1874	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
čtyři	čtyři	k4xCgFnPc1
kamenné	kamenný	k2eAgFnPc1d1
alegorické	alegorický	k2eAgFnPc1d1
sochy	socha	k1gFnPc1
v	v	k7c6
průčelí	průčelí	k1gNnSc6
budovyHypoteční	budovyHypoteční	k2eAgFnSc2d1
banky	banka	k1gFnSc2
Království	království	k1gNnSc1
českého	český	k2eAgNnSc2d1
na	na	k7c6
Senovážném	Senovážný	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
(	(	kIx(
<g/>
1890	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
dvě	dva	k4xCgFnPc1
bronzové	bronzový	k2eAgFnPc1d1
sochy	socha	k1gFnPc1
Viktorie	Viktoria	k1gFnSc2
(	(	kIx(
<g/>
Niké	Niké	k1gFnSc1
<g/>
)	)	kIx)
s	s	k7c7
pochodněmi	pochodeň	k1gFnPc7
v	v	k7c6
rukou	ruka	k1gFnPc6
na	na	k7c6
sloupech	sloup	k1gInPc6
mostu	most	k1gInSc2
Svatopluka	Svatopluk	k1gMnSc2
Čecha	Čech	k1gMnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
plastika	plastik	k1gMnSc4
rytíře	rytíř	k1gMnSc4
na	na	k7c6
štítu	štít	k1gInSc6
radnice	radnice	k1gFnSc2
v	v	k7c6
Kladně	Kladno	k1gNnSc6
</s>
<s>
bronzové	bronzový	k2eAgFnPc4d1
busty	busta	k1gFnPc4
Josefa	Josef	k1gMnSc2
Kajetána	Kajetán	k1gMnSc2
Tyla	Tyl	k1gMnSc2
a	a	k8xC
Václava	Václav	k1gMnSc2
Klimenta	Kliment	k1gMnSc2
Klicpery	Klicpera	k1gFnSc2
pro	pro	k7c4
foyer	foyer	k1gNnSc4
Národního	národní	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
</s>
<s>
náhrobky	náhrobek	k1gInPc4
na	na	k7c6
Vyšehradském	vyšehradský	k2eAgMnSc6d1
(	(	kIx(
<g/>
Rodina	rodina	k1gFnSc1
Škardova	Škardův	k2eAgFnSc1d1
<g/>
)	)	kIx)
a	a	k8xC
Olšanském	olšanský	k2eAgInSc6d1
hřbitově	hřbitov	k1gInSc6
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Alegorické	alegorický	k2eAgNnSc4d1
sousoší	sousoší	k1gNnSc4
Svornost	svornost	k1gFnSc1
a	a	k8xC
Ušlechtilost	ušlechtilost	k1gFnSc1
na	na	k7c6
Měšťanské	měšťanský	k2eAgFnSc6d1
besedě	beseda	k1gFnSc6
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
</s>
<s>
Plastika	plastik	k1gMnSc4
rytíře	rytíř	k1gMnSc4
na	na	k7c6
radnici	radnice	k1gFnSc6
v	v	k7c6
Kladně	Kladno	k1gNnSc6
</s>
<s>
Reliéf	reliéf	k1gInSc1
pro	pro	k7c4
Okresní	okresní	k2eAgInSc4d1
dům	dům	k1gInSc4
ve	v	k7c6
Slaném	Slaný	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umístěn	umístěn	k2eAgInSc1d1
nad	nad	k7c7
vstupními	vstupní	k2eAgFnPc7d1
dveřmi	dveře	k1gFnPc7
</s>
<s>
Alegorie	alegorie	k1gFnSc1
Nadšení	nadšení	k1gNnSc2
na	na	k7c6
Národním	národní	k2eAgInSc6d1
domě	dům	k1gInSc6
v	v	k7c6
Praze-Vinohradech	Praze-Vinohrad	k1gInPc6
</s>
<s>
Pozůstalost	pozůstalost	k1gFnSc1
</s>
<s>
Kolekce	kolekce	k1gFnSc1
sádrových	sádrový	k2eAgInPc2d1
modelů	model	k1gInPc2
jeho	jeho	k3xOp3gFnPc2
soch	socha	k1gFnPc2
a	a	k8xC
reliéfů	reliéf	k1gInPc2
je	být	k5eAaImIp3nS
uložena	uložit	k5eAaPmNgFnS
ve	v	k7c6
sbírce	sbírka	k1gFnSc6
Národního	národní	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Matriční	matriční	k2eAgInSc4d1
záznam	záznam	k1gInSc4
o	o	k7c6
narození	narození	k1gNnSc6
a	a	k8xC
křtu	křest	k1gInSc6
farnosti	farnost	k1gFnSc2
při	při	k7c6
kostele	kostel	k1gInSc6
sv.	sv.	kA
<g/>
Jiljí	Jiljí	k1gMnSc2
na	na	k7c6
Starém	starý	k2eAgNnSc6d1
Městě	město	k1gNnSc6
pražském	pražský	k2eAgNnSc6d1
↑	↑	k?
Matriční	matriční	k2eAgInSc4d1
záznam	záznam	k1gInSc4
o	o	k7c6
úmrtí	úmrtí	k1gNnSc6
a	a	k8xC
pohřbu	pohřeb	k1gInSc6
farnost	farnost	k1gFnSc1
při	při	k7c6
kostele	kostel	k1gInSc6
sv.	sv.	kA
<g/>
Ludmily	Ludmila	k1gFnSc2
na	na	k7c6
pražských	pražský	k2eAgInPc6d1
Vinohradech	Vinohrady	k1gInPc6
↑	↑	k?
dle	dle	k7c2
matriky	matrika	k1gFnSc2
zemřelých	zemřelý	k1gMnPc2
Královských	královský	k2eAgInPc2d1
Vinohrad	Vinohrady	k1gInPc2
zemřel	zemřít	k5eAaPmAgMnS
Antonín	Antonín	k1gMnSc1
Poop	Poop	k1gMnSc1
8	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1915	#num#	k4
na	na	k7c6
Kr.	Kr.	k1gFnSc6
Vinohradech	Vinohrady	k1gInPc6
čp.	čp.	k?
955	#num#	k4
<g/>
↑	↑	k?
Pobytová	pobytový	k2eAgFnSc1d1
přihláška	přihláška	k1gFnSc1
rodiny	rodina	k1gFnSc2
<g/>
↑	↑	k?
Dačevová	Dačevová	k1gFnSc1
R	R	kA
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
118-119	118-119	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Rumjana	Rumjana	k1gFnSc1
Dačevová	Dačevová	k1gFnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Karáskova	Karáskův	k2eAgFnSc1d1
galerie	galerie	k1gFnSc1
<g/>
,	,	kIx,
Památník	památník	k1gInSc1
národního	národní	k2eAgNnSc2d1
písemnictví	písemnictví	k1gNnSc2
Praha	Praha	k1gFnSc1
2012	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-80-87376-01-0	978-80-87376-01-0	k4
</s>
<s>
Nový	nový	k2eAgInSc1d1
slovník	slovník	k1gInSc1
českého	český	k2eAgNnSc2d1
výtvarného	výtvarný	k2eAgNnSc2d1
umění	umění	k1gNnSc2
<g/>
,	,	kIx,
díl	díl	k1gInSc1
2	#num#	k4
<g/>
,	,	kIx,
N	N	kA
<g/>
–	–	k?
<g/>
Ž	Ž	kA
<g/>
,	,	kIx,
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anděla	Anděla	k1gFnSc1
Horová	Horová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
Academia	academia	k1gFnSc1
1999	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
633	#num#	k4
<g/>
–	–	k?
<g/>
634	#num#	k4
</s>
<s>
N	N	kA
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Antonín	Antonín	k1gMnSc1
Popp	Popp	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Antonín	Antonín	k1gMnSc1
Popp	Popp	k1gMnSc1
</s>
<s>
Antonín	Antonín	k1gMnSc1
Popp	Popp	k1gMnSc1
v	v	k7c6
informačním	informační	k2eAgInSc6d1
systému	systém	k1gInSc6
abART	abART	k?
</s>
<s>
Centrum	centrum	k1gNnSc1
pro	pro	k7c4
dějinysochařství	dějinysochařství	k1gNnSc4
</s>
<s>
Soupis	soupis	k1gInSc1
pražských	pražský	k2eAgMnPc2d1
domovských	domovský	k2eAgMnPc2d1
příslušníků	příslušník	k1gMnPc2
1830	#num#	k4
<g/>
,	,	kIx,
1910	#num#	k4
<g/>
,	,	kIx,
Popp	Popp	k1gMnSc1
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
1850	#num#	k4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
pag	pag	k?
<g/>
2014804045	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
306300551	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Umění	umění	k1gNnSc1
</s>
