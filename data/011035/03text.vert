<p>
<s>
QTV	QTV	kA	QTV
byl	být	k5eAaImAgInS	být
první	první	k4xOgInSc4	první
testovací	testovací	k2eAgInSc4d1	testovací
let	let	k1gInSc4	let
rakety	raketa	k1gFnPc1	raketa
Little	Little	k1gFnSc2	Little
Joe	Joe	k1gFnSc2	Joe
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Start	start	k1gInSc1	start
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1963	[number]	k4	1963
na	na	k7c6	na
White	Whit	k1gInSc5	Whit
Sands	Sands	k1gInSc1	Sands
Missile	Missila	k1gFnSc3	Missila
Range	Range	k1gNnSc6	Range
<g/>
.	.	kIx.	.
</s>
<s>
Úkolem	úkol	k1gInSc7	úkol
mise	mise	k1gFnSc2	mise
bylo	být	k5eAaImAgNnS	být
ověřit	ověřit	k5eAaPmF	ověřit
funkčnost	funkčnost	k1gFnSc4	funkčnost
rakety	raketa	k1gFnSc2	raketa
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc4	její
kompatibilitu	kompatibilita	k1gFnSc4	kompatibilita
s	s	k7c7	s
kosmickou	kosmický	k2eAgFnSc7d1	kosmická
lodí	loď	k1gFnSc7	loď
Apollo	Apollo	k1gNnSc4	Apollo
a	a	k8xC	a
určit	určit	k5eAaPmF	určit
teploty	teplota	k1gFnPc4	teplota
a	a	k8xC	a
tlaky	tlak	k1gInPc4	tlak
při	při	k7c6	při
počáteční	počáteční	k2eAgFnSc6d1	počáteční
fázi	fáze	k1gFnSc6	fáze
letu	let	k1gInSc2	let
<g/>
.	.	kIx.	.
</s>
<s>
Raketa	raketa	k1gFnSc1	raketa
nesla	nést	k5eAaImAgFnS	nést
maketu	maketa	k1gFnSc4	maketa
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodi	loď	k1gFnSc2	loď
a	a	k8xC	a
únikového	únikový	k2eAgInSc2d1	únikový
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Test	test	k1gInSc1	test
byl	být	k5eAaImAgInS	být
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
a	a	k8xC	a
raketa	raketa	k1gFnSc1	raketa
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
při	při	k7c6	při
dalších	další	k2eAgInPc6d1	další
čtyřech	čtyři	k4xCgInPc6	čtyři
startech	start	k1gInPc6	start
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Únikový	únikový	k2eAgInSc1d1	únikový
systém	systém	k1gInSc1	systém
</s>
</p>
<p>
<s>
Little	Little	k6eAd1	Little
Joe	Joe	k1gMnSc1	Joe
II	II	kA	II
</s>
</p>
<p>
<s>
Program	program	k1gInSc1	program
Apollo	Apollo	k1gMnSc1	Apollo
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
The	The	k1gMnSc1	The
Apollo	Apollo	k1gMnSc1	Apollo
Spacecraft	Spacecraft	k1gMnSc1	Spacecraft
<g/>
:	:	kIx,	:
A	A	kA	A
Chronology	chronolog	k1gMnPc7	chronolog
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
Apollo	Apollo	k1gMnSc1	Apollo
Program	program	k1gInSc4	program
Summary	Summara	k1gFnSc2	Summara
Report	report	k1gInSc4	report
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
Little	Little	k1gFnSc1	Little
Joe	Joe	k1gMnSc1	Joe
II	II	kA	II
Qualification	Qualification	k1gInSc1	Qualification
Test	test	k1gInSc1	test
Flight	Flight	k2eAgInSc1d1	Flight
Report	report	k1gInSc1	report
-	-	kIx~	-
September	September	k1gInSc1	September
1963	[number]	k4	1963
(	(	kIx(	(
<g/>
PDF	PDF	kA	PDF
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
Little	Little	k1gFnSc1	Little
Joe	Joe	k1gMnSc1	Joe
II	II	kA	II
Progress	Progress	k1gInSc1	Progress
Report	report	k1gInSc1	report
-	-	kIx~	-
December	December	k1gInSc1	December
1964	[number]	k4	1964
(	(	kIx(	(
<g/>
PDF	PDF	kA	PDF
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
QTV	QTV	kA	QTV
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
