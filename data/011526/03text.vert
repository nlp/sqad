<p>
<s>
Kyril	Kyril	k1gMnSc1	Kyril
Reščuk	Reščuk	k1gMnSc1	Reščuk
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1876	[number]	k4	1876
Jasiňa	Jasiňa	k1gMnSc1	Jasiňa
–	–	k?	–
???	???	k?	???
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
československý	československý	k2eAgMnSc1d1	československý
politik	politik	k1gMnSc1	politik
rusínské	rusínský	k2eAgFnSc2d1	rusínská
národnosti	národnost	k1gFnSc2	národnost
z	z	k7c2	z
Podkarpatské	podkarpatský	k2eAgFnSc2d1	Podkarpatská
Rusi	Rus	k1gFnSc2	Rus
a	a	k8xC	a
meziválečný	meziválečný	k2eAgMnSc1d1	meziválečný
senátor	senátor	k1gMnSc1	senátor
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
ČSR	ČSR	kA	ČSR
za	za	k7c4	za
Komunistickou	komunistický	k2eAgFnSc4d1	komunistická
stranu	strana	k1gFnSc4	strana
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Povoláním	povolání	k1gNnSc7	povolání
byl	být	k5eAaImAgInS	být
zemědělcem	zemědělec	k1gMnSc7	zemědělec
v	v	k7c6	v
Jasini	Jasin	k2eAgMnPc1d1	Jasin
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
získal	získat	k5eAaPmAgInS	získat
za	za	k7c4	za
Komunistickou	komunistický	k2eAgFnSc4d1	komunistická
stranu	strana	k1gFnSc4	strana
Československa	Československo	k1gNnSc2	Československo
senátorské	senátorský	k2eAgNnSc4d1	senátorské
křeslo	křeslo	k1gNnSc4	křeslo
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
shromáždění	shromáždění	k1gNnSc6	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
senátu	senát	k1gInSc6	senát
setrval	setrvat	k5eAaPmAgInS	setrvat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
z	z	k7c2	z
klubu	klub	k1gInSc2	klub
KSČ	KSČ	kA	KSČ
a	a	k8xC	a
uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
nezařazený	zařazený	k2eNgMnSc1d1	nezařazený
senátor	senátor	k1gMnSc1	senátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Kyril	Kyril	k1gMnSc1	Kyril
Reščuk	Reščuk	k1gMnSc1	Reščuk
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
shromáždění	shromáždění	k1gNnSc6	shromáždění
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
</s>
</p>
