<s>
Miloš	Miloš	k1gMnSc1	Miloš
Zapletal	Zapletal	k1gMnSc1	Zapletal
(	(	kIx(	(
<g/>
*	*	kIx~	*
13	[number]	k4	13
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1930	[number]	k4	1930
Prostějov	Prostějov	k1gInSc1	Prostějov
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
literatury	literatura	k1gFnSc2	literatura
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
<g/>
,	,	kIx,	,
skautský	skautský	k2eAgMnSc1d1	skautský
redaktor	redaktor	k1gMnSc1	redaktor
a	a	k8xC	a
činovník	činovník	k1gMnSc1	činovník
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
pokračovatele	pokračovatel	k1gMnSc4	pokračovatel
Jaroslava	Jaroslav	k1gMnSc4	Jaroslav
Foglara	Foglar	k1gMnSc4	Foglar
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
také	také	k6eAd1	také
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
Pedagogickou	pedagogický	k2eAgFnSc4d1	pedagogická
fakultu	fakulta	k1gFnSc4	fakulta
UK	UK	kA	UK
<g/>
,	,	kIx,	,
obor	obor	k1gInSc4	obor
tělesná	tělesný	k2eAgFnSc1d1	tělesná
výchova	výchova	k1gFnSc1	výchova
<g/>
,	,	kIx,	,
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
vedl	vést	k5eAaImAgInS	vést
skautský	skautský	k2eAgInSc4d1	skautský
oddíl	oddíl	k1gInSc4	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
kniha	kniha	k1gFnSc1	kniha
Encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
her	hra	k1gFnPc2	hra
(	(	kIx(	(
<g/>
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
vyšla	vyjít	k5eAaPmAgFnS	vyjít
kromě	kromě	k7c2	kromě
češtiny	čeština	k1gFnSc2	čeština
také	také	k9	také
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
<g/>
,	,	kIx,	,
japonštině	japonština	k1gFnSc6	japonština
<g/>
,	,	kIx,	,
rumunštině	rumunština	k1gFnSc6	rumunština
a	a	k8xC	a
estonštině	estonština	k1gFnSc6	estonština
<g/>
.	.	kIx.	.
</s>
<s>
Stěžejním	stěžejní	k2eAgNnSc7d1	stěžejní
dílem	dílo	k1gNnSc7	dílo
je	být	k5eAaImIp3nS	být
čtyřdílná	čtyřdílný	k2eAgFnSc1d1	čtyřdílná
Velká	velký	k2eAgFnSc1d1	velká
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
her	hra	k1gFnPc2	hra
(	(	kIx(	(
<g/>
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
1985	[number]	k4	1985
<g/>
-	-	kIx~	-
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
edici	edice	k1gFnSc4	edice
Skautské	skautský	k2eAgInPc1d1	skautský
prameny	pramen	k1gInPc1	pramen
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
svazků	svazek	k1gInPc2	svazek
autorem	autor	k1gMnSc7	autor
či	či	k8xC	či
spoluautorem	spoluautor	k1gMnSc7	spoluautor
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
některá	některý	k3yIgNnPc4	některý
díla	dílo	k1gNnPc4	dílo
E.	E.	kA	E.
T.	T.	kA	T.
Setona	Setona	k1gFnSc1	Setona
a	a	k8xC	a
působil	působit	k5eAaImAgInS	působit
jako	jako	k9	jako
editor	editor	k1gInSc1	editor
kronikových	kronikový	k2eAgFnPc2d1	Kroniková
knih	kniha	k1gFnPc2	kniha
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
založil	založit	k5eAaPmAgMnS	založit
a	a	k8xC	a
vedl	vést	k5eAaImAgMnS	vést
skautský	skautský	k2eAgInSc4d1	skautský
oddíl	oddíl	k1gInSc4	oddíl
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
<g/>
.	.	kIx.	.
</s>
<s>
Níže	nízce	k6eAd2	nízce
je	být	k5eAaImIp3nS	být
uveden	uveden	k2eAgInSc1d1	uveden
výběr	výběr	k1gInSc1	výběr
z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Světlušky	světluška	k1gFnPc1	světluška
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
Lovec	lovec	k1gMnSc1	lovec
hvězd	hvězda	k1gFnPc2	hvězda
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
Sedmička	sedmička	k1gFnSc1	sedmička
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
Stezka	stezka	k1gFnSc1	stezka
odvahy	odvaha	k1gFnSc2	odvaha
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
Ostrov	ostrov	k1gInSc1	ostrov
přátelství	přátelství	k1gNnSc1	přátelství
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
pokračování	pokračování	k1gNnSc4	pokračování
Sedmičky	sedmička	k1gFnSc2	sedmička
Soví	soví	k2eAgFnSc2d1	soví
jeskyně	jeskyně	k1gFnSc2	jeskyně
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Cvoci	cvok	k1gMnPc1	cvok
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
volné	volný	k2eAgNnSc1d1	volné
pokračování	pokračování	k1gNnSc1	pokračování
<g />
.	.	kIx.	.
</s>
<s>
Sedmičky	sedmička	k1gFnPc1	sedmička
a	a	k8xC	a
Ostrova	ostrov	k1gInSc2	ostrov
přátelství	přátelství	k1gNnSc2	přátelství
Severka	Severka	k1gFnSc1	Severka
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Hry	hra	k1gFnPc1	hra
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
Hry	hra	k1gFnPc1	hra
a	a	k8xC	a
závody	závod	k1gInPc1	závod
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
Hry	hra	k1gFnPc1	hra
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
Drobné	drobný	k2eAgFnPc1d1	drobná
hry	hra	k1gFnPc1	hra
<g/>
,	,	kIx,	,
závody	závod	k1gInPc1	závod
a	a	k8xC	a
soutěže	soutěž	k1gFnPc1	soutěž
při	při	k7c6	při
výcviku	výcvik	k1gInSc6	výcvik
mladých	mladý	k2eAgMnPc2d1	mladý
turistů	turist	k1gMnPc2	turist
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Pokladnice	pokladnice	k1gFnSc1	pokladnice
her	hra	k1gFnPc2	hra
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
Junácké	junácký	k2eAgFnSc2d1	Junácká
hry	hra	k1gFnSc2	hra
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
her	hra	k1gFnPc2	hra
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
japonsky	japonsky	k6eAd1	japonsky
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
rumunsky	rumunsky	k6eAd1	rumunsky
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
estonsky	estonsky	k6eAd1	estonsky
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
Kniha	kniha	k1gFnSc1	kniha
hlavolamů	hlavolam	k1gInPc2	hlavolam
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
Velká	velký	k2eAgFnSc1d1	velká
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
her	hra	k1gFnPc2	hra
<g/>
;	;	kIx,	;
I.	I.	kA	I.
Hry	hra	k1gFnSc2	hra
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
Velká	velký	k2eAgFnSc1d1	velká
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
her	hra	k1gFnPc2	hra
<g/>
;	;	kIx,	;
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Hry	hra	k1gFnPc1	hra
v	v	k7c6	v
klubovně	klubovna	k1gFnSc6	klubovna
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
Velká	velký	k2eAgFnSc1d1	velká
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
her	hra	k1gFnPc2	hra
<g/>
;	;	kIx,	;
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Hry	hra	k1gFnPc1	hra
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
a	a	k8xC	a
v	v	k7c6	v
tělocvičně	tělocvična	k1gFnSc6	tělocvična
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
Velká	velký	k2eAgFnSc1d1	velká
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
her	hra	k1gFnPc2	hra
<g/>
;	;	kIx,	;
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
Hry	hra	k1gFnPc1	hra
ve	v	k7c6	v
městě	město	k1gNnSc6	město
a	a	k8xC	a
na	na	k7c6	na
vsi	ves	k1gFnSc6	ves
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
Špalíček	špalíček	k1gInSc1	špalíček
her	hra	k1gFnPc2	hra
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
Velká	velký	k2eAgFnSc1d1	velká
kniha	kniha	k1gFnSc1	kniha
deskových	deskový	k2eAgFnPc2d1	desková
her	hra	k1gFnPc2	hra
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Vycházky	vycházka	k1gFnPc4	vycházka
a	a	k8xC	a
výlety	výlet	k1gInPc4	výlet
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Hry	hra	k1gFnPc1	hra
do	do	k7c2	do
kapsy	kapsa	k1gFnSc2	kapsa
X.	X.	kA	X.
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Pět	pět	k4xCc4	pět
olympijských	olympijský	k2eAgInPc2d1	olympijský
kruhů	kruh	k1gInPc2	kruh
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
Objevy	objev	k1gInPc7	objev
bez	bez	k7c2	bez
konce	konec	k1gInSc2	konec
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Schmidem	Schmid	k1gMnSc7	Schmid
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
Výpravy	výprava	k1gFnPc4	výprava
za	za	k7c7	za
dobrodružstvím	dobrodružství	k1gNnSc7	dobrodružství
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
Záhady	záhada	k1gFnSc2	záhada
a	a	k8xC	a
tajemství	tajemství	k1gNnSc2	tajemství
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Rok	rok	k1gInSc1	rok
malých	malý	k2eAgNnPc2d1	malé
dobrodružství	dobrodružství	k1gNnPc2	dobrodružství
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
