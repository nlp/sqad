<s>
Vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
opera	opera	k1gFnSc1	opera
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
hanlivý	hanlivý	k2eAgInSc1d1	hanlivý
termín	termín	k1gInSc1	termín
(	(	kIx(	(
<g/>
variace	variace	k1gFnSc1	variace
na	na	k7c4	na
"	"	kIx"	"
<g/>
koňskou	koňský	k2eAgFnSc4d1	koňská
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
mýdlovou	mýdlový	k2eAgFnSc4d1	mýdlová
operu	opera	k1gFnSc4	opera
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
zavedený	zavedený	k2eAgInSc1d1	zavedený
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
americkým	americký	k2eAgMnSc7d1	americký
spisovatelem	spisovatel	k1gMnSc7	spisovatel
sci-fi	scii	k1gFnSc2	sci-fi
Wilsonem	Wilson	k1gMnSc7	Wilson
Tuckerem	Tucker	k1gMnSc7	Tucker
pro	pro	k7c4	pro
odlišení	odlišení	k1gNnSc4	odlišení
tohoto	tento	k3xDgInSc2	tento
žánru	žánr	k1gInSc2	žánr
od	od	k7c2	od
"	"	kIx"	"
<g/>
vážné	vážná	k1gFnSc2	vážná
<g/>
"	"	kIx"	"
science	science	k1gFnSc1	science
fiction	fiction	k1gInSc1	fiction
zaměřující	zaměřující	k2eAgFnSc2d1	zaměřující
se	se	k3xPyFc4	se
na	na	k7c4	na
důsledky	důsledek	k1gInPc4	důsledek
vědeckého	vědecký	k2eAgNnSc2d1	vědecké
bádání	bádání	k1gNnSc2	bádání
<g/>
.	.	kIx.	.
</s>
