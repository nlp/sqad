<s>
Mistr	mistr	k1gMnSc1	mistr
a	a	k8xC	a
Markétka	Markétka	k1gFnSc1	Markétka
je	být	k5eAaImIp3nS	být
román	román	k1gInSc1	román
ruského	ruský	k2eAgMnSc2d1	ruský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Michaila	Michail	k1gMnSc2	Michail
Bulgakova	Bulgakův	k2eAgMnSc2d1	Bulgakův
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
napsán	napsat	k5eAaBmNgInS	napsat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1928	[number]	k4	1928
až	až	k9	až
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
třicet	třicet	k4xCc4	třicet
dvě	dva	k4xCgFnPc4	dva
kapitoly	kapitola	k1gFnPc4	kapitola
a	a	k8xC	a
epilog	epilog	k1gInSc4	epilog
<g/>
.	.	kIx.	.
</s>
<s>
Michail	Michail	k1gMnSc1	Michail
Bulgakov	Bulgakov	k1gInSc4	Bulgakov
začal	začít	k5eAaPmAgMnS	začít
na	na	k7c6	na
knize	kniha	k1gFnSc6	kniha
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
verze	verze	k1gFnPc1	verze
nesly	nést	k5eAaImAgFnP	nést
názvy	název	k1gInPc4	název
Černý	Černý	k1gMnSc1	Černý
mág	mág	k1gMnSc1	mág
(	(	kIx(	(
<g/>
Ч	Ч	k?	Ч
м	м	k?	м
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Inženýrovo	inženýrův	k2eAgNnSc4d1	inženýrův
kopyto	kopyto	k1gNnSc4	kopyto
(	(	kIx(	(
<g/>
К	К	k?	К
и	и	k?	и
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Žonglér	žonglér	k1gMnSc1	žonglér
s	s	k7c7	s
kopytem	kopyto	k1gNnSc7	kopyto
(	(	kIx(	(
<g/>
Ж	Ж	k?	Ж
с	с	k?	с
к	к	k?	к
<g/>
)	)	kIx)	)
či	či	k8xC	či
Veliarův	Veliarův	k2eAgMnSc1d1	Veliarův
syn	syn	k1gMnSc1	syn
(	(	kIx(	(
<g/>
С	С	k?	С
В	В	k?	В
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rukopis	rukopis	k1gInSc1	rukopis
byl	být	k5eAaImAgInS	být
připravován	připravovat	k5eAaImNgInS	připravovat
pro	pro	k7c4	pro
vydavatelství	vydavatelství	k1gNnSc4	vydavatelství
Nědra	Nědr	k1gInSc2	Nědr
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
první	první	k4xOgFnSc1	první
verze	verze	k1gFnSc1	verze
byla	být	k5eAaImAgFnS	být
(	(	kIx(	(
<g/>
po	po	k7c6	po
zákazu	zákaz	k1gInSc6	zákaz
hry	hra	k1gFnSc2	hra
Kabala	kabala	k1gFnSc1	kabala
pobožnůstkářů	pobožnůstkář	k1gMnPc2	pobožnůstkář
–	–	k?	–
К	К	k?	К
с	с	k?	с
<g/>
)	)	kIx)	)
autorem	autor	k1gMnSc7	autor
spálena	spálen	k2eAgFnSc1d1	spálena
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
<s>
Dochoval	dochovat	k5eAaPmAgInS	dochovat
se	se	k3xPyFc4	se
autorův	autorův	k2eAgInSc1d1	autorův
dopis	dopis	k1gInSc1	dopis
<g/>
,	,	kIx,	,
datovaný	datovaný	k2eAgInSc1d1	datovaný
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1930	[number]	k4	1930
a	a	k8xC	a
adresovaný	adresovaný	k2eAgInSc1d1	adresovaný
vládě	vláda	k1gFnSc3	vláda
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
autor	autor	k1gMnSc1	autor
doslovně	doslovně	k6eAd1	doslovně
píše	psát	k5eAaImIp3nS	psát
"	"	kIx"	"
<g/>
...	...	k?	...
osobně	osobně	k6eAd1	osobně
<g/>
,	,	kIx,	,
vlastníma	vlastní	k2eAgFnPc7d1	vlastní
rukama	ruka	k1gFnPc7	ruka
jsem	být	k5eAaImIp1nS	být
v	v	k7c6	v
kamnech	kamna	k1gNnPc6	kamna
spálil	spálit	k5eAaPmAgMnS	spálit
všechny	všechen	k3xTgInPc4	všechen
koncepty	koncept	k1gInPc4	koncept
románu	román	k1gInSc2	román
o	o	k7c6	o
ďáblovi	ďábel	k1gMnSc6	ďábel
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Motiv	motiv	k1gInSc1	motiv
pálení	pálení	k1gNnSc2	pálení
díla	dílo	k1gNnSc2	dílo
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
samotné	samotný	k2eAgNnSc1d1	samotné
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Práci	práce	k1gFnSc4	práce
nad	nad	k7c7	nad
románem	román	k1gInSc7	román
autor	autor	k1gMnSc1	autor
obnovil	obnovit	k5eAaPmAgMnS	obnovit
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
verzi	verze	k1gFnSc6	verze
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
postavy	postava	k1gFnPc1	postava
Markéty	Markéta	k1gFnSc2	Markéta
a	a	k8xC	a
Mistra	mistr	k1gMnSc2	mistr
a	a	k8xC	a
román	román	k1gInSc1	román
získává	získávat	k5eAaImIp3nS	získávat
definitivní	definitivní	k2eAgInSc4d1	definitivní
název	název	k1gInSc4	název
Mistr	mistr	k1gMnSc1	mistr
a	a	k8xC	a
Markétka	Markétka	k1gFnSc1	Markétka
(	(	kIx(	(
<g/>
М	М	k?	М
и	и	k?	и
М	М	k?	М
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
druhá	druhý	k4xOgFnSc1	druhý
verze	verze	k1gFnSc1	verze
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
podobě	podoba	k1gFnSc6	podoba
již	již	k9	již
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
větší	veliký	k2eAgFnSc4d2	veliký
část	část	k1gFnSc4	část
zápletky	zápletka	k1gFnSc2	zápletka
i	i	k9	i
všechny	všechen	k3xTgFnPc4	všechen
důležité	důležitý	k2eAgFnPc4d1	důležitá
pasáže	pasáž	k1gFnPc4	pasáž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
Bulgakov	Bulgakov	k1gInSc1	Bulgakov
román	román	k1gInSc4	román
ještě	ještě	k9	ještě
jednou	jeden	k4xCgFnSc7	jeden
zredigoval	zredigovat	k5eAaPmAgMnS	zredigovat
a	a	k8xC	a
do	do	k7c2	do
názvu	název	k1gInSc2	název
doplnil	doplnit	k5eAaPmAgMnS	doplnit
podtitul	podtitul	k1gInSc4	podtitul
Fantastický	fantastický	k2eAgInSc4d1	fantastický
román	román	k1gInSc4	román
<g/>
.	.	kIx.	.
</s>
<s>
Začišťováním	začišťování	k1gNnSc7	začišťování
a	a	k8xC	a
slohovým	slohový	k2eAgNnSc7d1	slohové
pilováním	pilování	k1gNnSc7	pilování
textu	text	k1gInSc2	text
(	(	kIx(	(
<g/>
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
své	svůj	k3xOyFgFnSc2	svůj
ženy	žena	k1gFnSc2	žena
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
téměř	téměř	k6eAd1	téměř
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
–	–	k?	–
poslední	poslední	k2eAgFnPc1d1	poslední
úpravy	úprava	k1gFnPc1	úprava
rukopisu	rukopis	k1gInSc2	rukopis
jsou	být	k5eAaImIp3nP	být
datovány	datován	k2eAgInPc1d1	datován
13	[number]	k4	13
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1940	[number]	k4	1940
(	(	kIx(	(
<g/>
necelý	celý	k2eNgInSc4d1	necelý
měsíc	měsíc	k1gInSc4	měsíc
před	před	k7c7	před
Bulgakovovou	Bulgakovový	k2eAgFnSc7d1	Bulgakovový
smrtí	smrt	k1gFnSc7	smrt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
fabulačně	fabulačně	k6eAd1	fabulačně
završen	završit	k5eAaPmNgInS	završit
<g/>
.	.	kIx.	.
</s>
<s>
Bulgakovova	Bulgakovův	k2eAgFnSc1d1	Bulgakovova
žena	žena	k1gFnSc1	žena
však	však	k9	však
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c6	v
redakci	redakce	k1gFnSc6	redakce
románu	román	k1gInSc2	román
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
ze	z	k7c2	z
zbývajících	zbývající	k2eAgMnPc2d1	zbývající
a	a	k8xC	a
Bulgakovem	Bulgakov	k1gInSc7	Bulgakov
i	i	k8xC	i
jeho	jeho	k3xOp3gFnSc7	jeho
ženou	žena	k1gFnSc7	žena
nepostřehnutých	postřehnutý	k2eNgInPc2d1	postřehnutý
rozporů	rozpor	k1gInPc2	rozpor
jsou	být	k5eAaImIp3nP	být
přesto	přesto	k8xC	přesto
předmětem	předmět	k1gInSc7	předmět
kvízových	kvízový	k2eAgFnPc2d1	kvízová
otázek	otázka	k1gFnPc2	otázka
znalců	znalec	k1gMnPc2	znalec
autorova	autorův	k2eAgNnSc2d1	autorovo
díla	dílo	k1gNnSc2	dílo
(	(	kIx(	(
<g/>
Mistr	mistr	k1gMnSc1	mistr
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
v	v	k7c6	v
Kapitole	kapitola	k1gFnSc6	kapitola
třinácté	třináctý	k4xOgFnSc6	třináctý
hladce	hladko	k6eAd1	hladko
oholen	oholit	k5eAaPmNgInS	oholit
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
v	v	k7c6	v
Kapitole	kapitola	k1gFnSc6	kapitola
čtyřiadvacáté	čtyřiadvacátý	k4xOgFnSc6	čtyřiadvacátý
–	–	k?	–
dějově	dějově	k6eAd1	dějově
následující	následující	k2eAgFnSc1d1	následující
za	za	k7c4	za
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
–	–	k?	–
má	mít	k5eAaImIp3nS	mít
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
bradku	bradka	k1gFnSc4	bradka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cenzurovaná	cenzurovaný	k2eAgFnSc1d1	cenzurovaná
verze	verze	k1gFnSc1	verze
(	(	kIx(	(
<g/>
12	[number]	k4	12
%	%	kIx~	%
textu	text	k1gInSc2	text
vynecháno	vynechán	k2eAgNnSc1d1	vynecháno
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
pozměněna	pozměněn	k2eAgFnSc1d1	pozměněna
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
publikována	publikovat	k5eAaBmNgFnS	publikovat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Moskva	Moskva	k1gFnSc1	Moskva
(	(	kIx(	(
<g/>
ročník	ročník	k1gInSc1	ročník
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
č.	č.	k?	č.
11	[number]	k4	11
a	a	k8xC	a
ročník	ročník	k1gInSc1	ročník
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
č.	č.	k?	č.
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Text	text	k1gInSc1	text
odstraněných	odstraněný	k2eAgFnPc2d1	odstraněná
a	a	k8xC	a
upravených	upravený	k2eAgFnPc2d1	upravená
částí	část	k1gFnPc2	část
vyšel	vyjít	k5eAaPmAgInS	vyjít
samizdatově	samizdatově	k6eAd1	samizdatově
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
doplněn	doplnit	k5eAaPmNgInS	doplnit
o	o	k7c4	o
údaje	údaj	k1gInPc4	údaj
nezbytné	nezbytný	k2eAgInPc4d1	nezbytný
ke	k	k7c3	k
kompletnímu	kompletní	k2eAgNnSc3d1	kompletní
nahrazení	nahrazení	k1gNnSc3	nahrazení
originální	originální	k2eAgFnSc2d1	originální
verze	verze	k1gFnSc2	verze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
nakladatelství	nakladatelství	k1gNnPc2	nakladatelství
Posev	posít	k5eAaPmDgInS	posít
(	(	kIx(	(
<g/>
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
<g/>
)	)	kIx)	)
vydalo	vydat	k5eAaPmAgNnS	vydat
kompletní	kompletní	k2eAgFnSc4d1	kompletní
verzi	verze	k1gFnSc4	verze
(	(	kIx(	(
<g/>
právě	právě	k6eAd1	právě
díky	díky	k7c3	díky
samizdatovým	samizdatový	k2eAgInPc3d1	samizdatový
doplňkům	doplněk	k1gInPc3	doplněk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
se	se	k3xPyFc4	se
prvního	první	k4xOgNnSc2	první
necenzurovaného	cenzurovaný	k2eNgNnSc2d1	necenzurované
znění	znění	k1gNnSc2	znění
dočkalo	dočkat	k5eAaPmAgNnS	dočkat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Chudožestvenaja	Chudožestvenaj	k2eAgFnSc1d1	Chudožestvenaj
Litěratura	Litěratura	k1gFnSc1	Litěratura
(	(	kIx(	(
<g/>
Х	Х	k?	Х
Л	Л	k?	Л
<g/>
)	)	kIx)	)
vyšla	vyjít	k5eAaPmAgFnS	vyjít
verze	verze	k1gFnSc1	verze
opírající	opírající	k2eAgFnSc1d1	opírající
se	se	k3xPyFc4	se
o	o	k7c4	o
rukopisy	rukopis	k1gInPc4	rukopis
sepsané	sepsaný	k2eAgInPc4d1	sepsaný
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
znění	znění	k1gNnSc1	znění
bylo	být	k5eAaImAgNnS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
kanonické	kanonický	k2eAgNnSc4d1	kanonické
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
redaktorky	redaktorka	k1gFnSc2	redaktorka
Lydie	Lydie	k1gFnSc2	Lydie
Janovské	Janovské	k2eAgFnSc1d1	Janovské
vydána	vydán	k2eAgFnSc1d1	vydána
verze	verze	k1gFnSc1	verze
respektující	respektující	k2eAgInPc1d1	respektující
veškeré	veškerý	k3xTgInPc1	veškerý
existující	existující	k2eAgInPc1d1	existující
rukopisy	rukopis	k1gInPc1	rukopis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc4	dva
profesionální	profesionální	k2eAgInPc4d1	profesionální
překlady	překlad	k1gInPc4	překlad
tohoto	tento	k3xDgNnSc2	tento
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc4	první
a	a	k8xC	a
dosud	dosud	k6eAd1	dosud
patrně	patrně	k6eAd1	patrně
nejrozšířenější	rozšířený	k2eAgFnSc1d3	nejrozšířenější
pořídila	pořídit	k5eAaPmAgFnS	pořídit
Alena	Alena	k1gFnSc1	Alena
Morávková	Morávková	k1gFnSc1	Morávková
(	(	kIx(	(
<g/>
překlad	překlad	k1gInSc1	překlad
vydal	vydat	k5eAaPmAgInS	vydat
poprvé	poprvé	k6eAd1	poprvé
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
také	také	k9	také
přeložila	přeložit	k5eAaPmAgFnS	přeložit
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgFnPc2d1	další
Bulgakovových	Bulgakovová	k1gFnPc2	Bulgakovová
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Morávkové	Morávková	k1gFnSc2	Morávková
byl	být	k5eAaImAgInS	být
vůbec	vůbec	k9	vůbec
prvním	první	k4xOgInSc7	první
překladem	překlad	k1gInSc7	překlad
<g/>
,	,	kIx,	,
využívajícím	využívající	k2eAgInSc7d1	využívající
necenzurované	cenzurovaný	k2eNgNnSc1d1	necenzurované
znění	znění	k1gNnSc3	znění
<g/>
,	,	kIx,	,
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
podkladem	podklad	k1gInSc7	podklad
několika	několik	k4yIc2	několik
rozhlasových	rozhlasový	k2eAgMnPc2d1	rozhlasový
i	i	k8xC	i
divadelních	divadelní	k2eAgFnPc2d1	divadelní
inscenací	inscenace	k1gFnPc2	inscenace
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
druhého	druhý	k4xOgInSc2	druhý
překladu	překlad	k1gInSc2	překlad
je	být	k5eAaImIp3nS	být
Libor	Libor	k1gMnSc1	Libor
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
v	v	k7c6	v
Odeonu	odeon	k1gInSc6	odeon
<g/>
.	.	kIx.	.
</s>
<s>
Dvořák	Dvořák	k1gMnSc1	Dvořák
přeložil	přeložit	k5eAaPmAgMnS	přeložit
rovněž	rovněž	k9	rovněž
podklady	podklad	k1gInPc4	podklad
pro	pro	k7c4	pro
dabing	dabing	k1gInSc4	dabing
ruského	ruský	k2eAgInSc2d1	ruský
seriálu	seriál	k1gInSc2	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
překlady	překlad	k1gInPc1	překlad
se	se	k3xPyFc4	se
v	v	k7c6	v
podstatných	podstatný	k2eAgInPc6d1	podstatný
detailech	detail	k1gInPc6	detail
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ve	v	k7c6	v
jménech	jméno	k1gNnPc6	jméno
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
(	(	kIx(	(
<g/>
básník	básník	k1gMnSc1	básník
Bezdomnij	Bezdomnij	k1gMnSc1	Bezdomnij
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
Б	Б	k?	Б
-	-	kIx~	-
u	u	k7c2	u
Morávkové	Morávková	k1gFnSc2	Morávková
Bezprizorný	bezprizorný	k2eAgMnSc1d1	bezprizorný
<g/>
,	,	kIx,	,
u	u	k7c2	u
Dvořáka	Dvořák	k1gMnSc2	Dvořák
Bezdomovec	bezdomovec	k1gMnSc1	bezdomovec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
v	v	k7c6	v
období	období	k1gNnSc6	období
NEPu	Nep	k1gInSc2	Nep
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
scény	scéna	k1gFnPc1	scéna
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
v	v	k7c6	v
bytu	byt	k1gInSc6	byt
č.	č.	k?	č.
50	[number]	k4	50
v	v	k7c6	v
domě	dům	k1gInSc6	dům
302	[number]	k4	302
<g/>
b	b	k?	b
v	v	k7c6	v
Sadové	sadový	k2eAgFnSc6d1	Sadová
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Bulgakov	Bulgakov	k1gInSc1	Bulgakov
bydlel	bydlet	k5eAaImAgInS	bydlet
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1921-1924	[number]	k4	1921-1924
v	v	k7c6	v
bytu	byt	k1gInSc6	byt
č.	č.	k?	č.
50	[number]	k4	50
na	na	k7c6	na
Velké	velký	k2eAgFnSc6d1	velká
sadové	sadový	k2eAgFnSc6d1	Sadová
ulici	ulice	k1gFnSc6	ulice
10	[number]	k4	10
(	(	kIx(	(
<g/>
у	у	k?	у
<g/>
.	.	kIx.	.
Б	Б	k?	Б
С	С	k?	С
<g/>
,	,	kIx,	,
д	д	k?	д
10,6	[number]	k4	10,6
<g/>
-й	-й	k?	-й
п	п	k?	п
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
-й	-й	k?	-й
э	э	k?	э
<g/>
,	,	kIx,	,
к	к	k?	к
50	[number]	k4	50
<g/>
)	)	kIx)	)
a	a	k8xC	a
dům	dům	k1gInSc4	dům
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
obyvatelé	obyvatel	k1gMnPc1	obyvatel
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
předobrazem	předobraz	k1gInSc7	předobraz
románu	román	k1gInSc2	román
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
bytu	byt	k1gInSc6	byt
č.	č.	k?	č.
50	[number]	k4	50
muzeum	muzeum	k1gNnSc4	muzeum
Michaila	Michail	k1gMnSc2	Michail
Bulgakova	Bulgakův	k2eAgMnSc2d1	Bulgakův
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
postav	postava	k1gFnPc2	postava
má	mít	k5eAaImIp3nS	mít
symbolická	symbolický	k2eAgNnPc4d1	symbolické
jména	jméno	k1gNnPc4	jméno
–	–	k?	–
některá	některý	k3yIgFnSc1	některý
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
významu	význam	k1gInSc2	význam
příslušných	příslušný	k2eAgNnPc2d1	příslušné
jmen	jméno	k1gNnPc2	jméno
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
(	(	kIx(	(
<g/>
Lichodějov	Lichodějov	k1gInSc1	Lichodějov
<g/>
,	,	kIx,	,
Bezprizorný	bezprizorný	k1gMnSc1	bezprizorný
<g/>
,	,	kIx,	,
Korovjev	Korovjev	k1gMnSc1	Korovjev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jiná	jiný	k2eAgNnPc1d1	jiné
mají	mít	k5eAaImIp3nP	mít
dávný	dávný	k2eAgInSc1d1	dávný
mytologický	mytologický	k2eAgInSc1d1	mytologický
původ	původ	k1gInSc1	původ
(	(	kIx(	(
<g/>
Azazelius	Azazelius	k1gInSc1	Azazelius
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
vedlejších	vedlejší	k2eAgFnPc2d1	vedlejší
postav	postava	k1gFnPc2	postava
je	být	k5eAaImIp3nS	být
také	také	k9	také
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
podle	podle	k7c2	podle
hudebních	hudební	k2eAgMnPc2d1	hudební
skladatelů	skladatel	k1gMnPc2	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Wolland	Wolland	k1gInSc1	Wolland
(	(	kIx(	(
<g/>
Woland	Woland	k1gInSc1	Woland
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
В	В	k?	В
<g/>
)	)	kIx)	)
–	–	k?	–
personifikace	personifikace	k1gFnPc1	personifikace
Ďábla	ďábel	k1gMnSc2	ďábel
Markéta	Markéta	k1gFnSc1	Markéta
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
М	М	k?	М
<g/>
)	)	kIx)	)
–	–	k?	–
milenka	milenka	k1gFnSc1	milenka
Mistrova	mistrův	k2eAgFnSc1d1	Mistrova
<g/>
,	,	kIx,	,
vědma	vědma	k1gFnSc1	vědma
<g/>
,	,	kIx,	,
zosobnění	zosobnění	k1gNnSc1	zosobnění
hrdé	hrdý	k2eAgFnSc2d1	hrdá
a	a	k8xC	a
vášnivé	vášnivý	k2eAgFnSc2d1	vášnivá
ženy	žena	k1gFnSc2	žena
Mistr	mistr	k1gMnSc1	mistr
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
М	М	k?	М
<g/>
)	)	kIx)	)
–	–	k?	–
jinak	jinak	k6eAd1	jinak
bezejmenný	bezejmenný	k2eAgMnSc1d1	bezejmenný
umělec	umělec	k1gMnSc1	umělec
<g/>
,	,	kIx,	,
pronásledovaný	pronásledovaný	k2eAgInSc1d1	pronásledovaný
kritiky	kritik	k1gMnPc7	kritik
i	i	k8xC	i
režimem	režim	k1gInSc7	režim
kocour	kocour	k1gMnSc1	kocour
Behemot	behemot	k1gMnSc1	behemot
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
Б	Б	k?	Б
<g/>
,	,	kIx,	,
u	u	k7c2	u
Morávkové	Morávkové	k2eAgFnPc2d1	Morávkové
Kňour	kňoura	k1gFnPc2	kňoura
<g/>
;	;	kIx,	;
slovo	slovo	k1gNnSc1	slovo
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
znamená	znamenat	k5eAaImIp3nS	znamenat
hrocha	hroch	k1gMnSc4	hroch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
biblického	biblický	k2eAgMnSc4d1	biblický
Behemota	behemot	k1gMnSc4	behemot
<g/>
)	)	kIx)	)
–	–	k?	–
člen	člen	k1gInSc1	člen
Wollandovy	Wollandův	k2eAgInPc1d1	Wollandův
svity	svit	k1gInPc1	svit
<g/>
,	,	kIx,	,
zakletý	zakletý	k2eAgMnSc1d1	zakletý
princ	princ	k1gMnSc1	princ
<g/>
,	,	kIx,	,
vynikající	vynikající	k2eAgMnSc1d1	vynikající
žertéř	žertéř	k1gMnSc1	žertéř
a	a	k8xC	a
šprýmař	šprýmař	k1gMnSc1	šprýmař
<g/>
,	,	kIx,	,
nejbližší	blízký	k2eAgMnSc1d3	nejbližší
z	z	k7c2	z
Wollandových	Wollandový	k2eAgMnPc2d1	Wollandový
společníků	společník	k1gMnPc2	společník
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
některých	některý	k3yIgMnPc2	některý
autorů	autor	k1gMnPc2	autor
byla	být	k5eAaImAgFnS	být
postava	postava	k1gFnSc1	postava
Behemota	behemot	k1gMnSc2	behemot
inspirována	inspirovat	k5eAaBmNgNnP	inspirovat
kocourem	kocour	k1gInSc7	kocour
Bulgakovových	Bulgakovových	k2eAgFnSc7d1	Bulgakovových
Fljuškou	Fljuška	k1gFnSc7	Fljuška
<g/>
.	.	kIx.	.
</s>
<s>
Kravinkin	Kravinkin	k1gInSc1	Kravinkin
(	(	kIx(	(
<g/>
též	též	k9	též
Fagot	fagot	k1gInSc1	fagot
nebo	nebo	k8xC	nebo
podle	podle	k7c2	podle
originálu	originál	k1gInSc2	originál
Korovjev	Korovjev	k1gFnSc2	Korovjev
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
К	К	k?	К
<g/>
)	)	kIx)	)
–	–	k?	–
Wollandův	Wollandův	k2eAgMnSc1d1	Wollandův
důvěrník	důvěrník	k1gMnSc1	důvěrník
a	a	k8xC	a
Behemotův	behemotův	k2eAgMnSc1d1	behemotův
přítel	přítel	k1gMnSc1	přítel
a	a	k8xC	a
kumpán	kumpán	k?	kumpán
Azazelius	Azazelius	k1gMnSc1	Azazelius
(	(	kIx(	(
<g/>
Azazelo	Azazelo	k1gMnSc1	Azazelo
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
А	А	k?	А
<g/>
)	)	kIx)	)
–	–	k?	–
démon	démon	k1gMnSc1	démon
<g/>
,	,	kIx,	,
vystupující	vystupující	k2eAgMnSc1d1	vystupující
jako	jako	k8xS	jako
ramenatý	ramenatý	k2eAgMnSc1d1	ramenatý
zrzavý	zrzavý	k2eAgMnSc1d1	zrzavý
pořízek	pořízek	k1gMnSc1	pořízek
ve	v	k7c6	v
slamáčku	slamáček	k1gInSc6	slamáček
<g/>
,	,	kIx,	,
zosobnění	zosobnění	k1gNnSc1	zosobnění
hrubé	hrubý	k2eAgFnSc2d1	hrubá
síly	síla	k1gFnSc2	síla
Hella	Hella	k1gMnSc1	Hella
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
Г	Г	k?	Г
<g/>
)	)	kIx)	)
–	–	k?	–
čertice	čertice	k1gFnSc1	čertice
<g/>
,	,	kIx,	,
zastupující	zastupující	k2eAgFnSc1d1	zastupující
ve	v	k7c6	v
Wollandově	Wollandův	k2eAgFnSc6d1	Wollandův
svitě	svita	k1gFnSc6	svita
ženský	ženský	k2eAgInSc4d1	ženský
element	element	k1gInSc4	element
Ješua	Ješuum	k1gNnSc2	Ješuum
Ha-Nocri	Ha-Nocr	k1gFnSc2	Ha-Nocr
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
И	И	k?	И
г	г	k?	г
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Ježíš	ježit	k5eAaImIp2nS	ježit
Nazaretský	nazaretský	k2eAgInSc1d1	nazaretský
(	(	kIx(	(
<g/>
Bulgakov	Bulgakov	k1gInSc1	Bulgakov
jej	on	k3xPp3gMnSc4	on
nazývá	nazývat	k5eAaImIp3nS	nazývat
předpokládanou	předpokládaný	k2eAgFnSc7d1	předpokládaná
hebrejskou	hebrejský	k2eAgFnSc7d1	hebrejská
podobou	podoba	k1gFnSc7	podoba
jména	jméno	k1gNnSc2	jméno
<g/>
)	)	kIx)	)
Pilát	pilát	k1gInSc1	pilát
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
П	П	k?	П
<g/>
)	)	kIx)	)
–	–	k?	–
Pilát	Pilát	k1gMnSc1	Pilát
<g />
.	.	kIx.	.
</s>
<s>
Pontský	pontský	k2eAgMnSc1d1	pontský
<g/>
,	,	kIx,	,
prokurátor	prokurátor	k1gMnSc1	prokurátor
galilejský	galilejský	k2eAgMnSc1d1	galilejský
Berlioz	Berlioz	k1gMnSc1	Berlioz
–	–	k?	–
první	první	k4xOgFnSc1	první
Wollandova	Wollandův	k2eAgFnSc1d1	Wollandův
oběť	oběť	k1gFnSc1	oběť
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
kulturní	kulturní	k2eAgFnSc2d1	kulturní
komise	komise	k1gFnSc2	komise
Massolitu	Massolit	k1gInSc2	Massolit
(	(	kIx(	(
<g/>
Masové	masový	k2eAgFnPc1d1	masová
organizace	organizace	k1gFnPc1	organizace
literátů	literát	k1gMnPc2	literát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
v	v	k7c6	v
nezapomenutelné	zapomenutelný	k2eNgFnSc6d1	nezapomenutelná
scéně	scéna	k1gFnSc6	scéna
"	"	kIx"	"
<g/>
odřízne	odříznout	k5eAaPmIp3nS	odříznout
hlavu	hlava	k1gFnSc4	hlava
ruská	ruský	k2eAgFnSc1d1	ruská
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
komsomolka	komsomolka	k1gFnSc1	komsomolka
<g/>
"	"	kIx"	"
Ivan	Ivan	k1gMnSc1	Ivan
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Potápka	potápka	k1gMnSc1	potápka
-	-	kIx~	-
Bezdomovec	bezdomovec	k1gMnSc1	bezdomovec
(	(	kIx(	(
<g/>
u	u	k7c2	u
Morávkové	Morávková	k1gFnSc2	Morávková
Bezprizorný	bezprizorný	k1gMnSc1	bezprizorný
<g />
.	.	kIx.	.
</s>
<s>
<g/>
;	;	kIx,	;
rusky	rusky	k6eAd1	rusky
И	И	k?	И
Н	Н	k?	Н
Б	Б	k?	Б
<g/>
)	)	kIx)	)
–	–	k?	–
mladý	mladý	k1gMnSc1	mladý
<g/>
,	,	kIx,	,
naivní	naivní	k2eAgMnSc1d1	naivní
<g/>
,	,	kIx,	,
leč	leč	k8xS	leč
zapálený	zapálený	k2eAgMnSc1d1	zapálený
sovětský	sovětský	k2eAgMnSc1d1	sovětský
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
po	po	k7c6	po
setkání	setkání	k1gNnSc6	setkání
s	s	k7c7	s
Wolandem	Woland	k1gInSc7	Woland
zešílí	zešílet	k5eAaPmIp3nS	zešílet
a	a	k8xC	a
na	na	k7c6	na
klinice	klinika	k1gFnSc6	klinika
se	se	k3xPyFc4	se
setká	setkat	k5eAaPmIp3nS	setkat
s	s	k7c7	s
Mistrem	mistr	k1gMnSc7	mistr
Stěpan	Stěpana	k1gFnPc2	Stěpana
Mizerov	Mizerov	k1gInSc1	Mizerov
(	(	kIx(	(
<g/>
u	u	k7c2	u
Morávkové	Morávkové	k2eAgNnPc2d1	Morávkové
Lotrov	Lotrovo	k1gNnPc2	Lotrovo
<g/>
,	,	kIx,	,
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
Lichodějev	Lichodějev	k1gFnSc2	Lichodějev
<g/>
)	)	kIx)	)
–	–	k?	–
umělecký	umělecký	k2eAgMnSc1d1	umělecký
vedoucí	vedoucí	k1gMnSc1	vedoucí
Moskevského	moskevský	k2eAgNnSc2d1	moskevské
varieté	varieté	k1gNnSc2	varieté
<g/>
,	,	kIx,	,
nájemce	nájemce	k1gMnSc1	nájemce
bytu	byt	k1gInSc2	byt
50	[number]	k4	50
na	na	k7c6	na
Sadové	sadový	k2eAgFnSc6d1	Sadová
ulici	ulice	k1gFnSc6	ulice
302	[number]	k4	302
bis	bis	k?	bis
(	(	kIx(	(
<g/>
н	н	k?	н
к	к	k?	к
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
o	o	k7c4	o
který	který	k3yIgInSc4	který
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
s	s	k7c7	s
Berliozem	Berlioz	k1gMnSc7	Berlioz
a	a	k8xC	a
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
Wolland	Wolland	k1gInSc1	Wolland
vybere	vybrat	k5eAaPmIp3nS	vybrat
jako	jako	k9	jako
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
Bál	bál	k1gInSc4	bál
jarního	jarní	k2eAgInSc2d1	jarní
úplňku	úplněk	k1gInSc2	úplněk
<g/>
.	.	kIx.	.
</s>
<s>
Wollandem	Wollando	k1gNnSc7	Wollando
teleportován	teleportovat	k5eAaBmNgMnS	teleportovat
do	do	k7c2	do
Jalty	Jalta	k1gFnSc2	Jalta
<g/>
.	.	kIx.	.
</s>
<s>
Rimský	Rimský	k1gMnSc1	Rimský
–	–	k?	–
finanční	finanční	k2eAgMnSc1d1	finanční
ředitel	ředitel	k1gMnSc1	ředitel
Varieté	varieté	k1gNnSc2	varieté
<g/>
,	,	kIx,	,
zatčen	zatčen	k2eAgMnSc1d1	zatčen
Varenucha	Varenuch	k1gMnSc2	Varenuch
–	–	k?	–
administrátor	administrátor	k1gMnSc1	administrátor
Varieté	varieté	k1gNnSc1	varieté
<g/>
,	,	kIx,	,
pokousán	pokousán	k2eAgInSc1d1	pokousán
Hellou	Hella	k1gMnSc7	Hella
a	a	k8xC	a
změněn	změněn	k2eAgInSc1d1	změněn
v	v	k7c4	v
upíra	upír	k1gMnSc4	upír
Nikanor	Nikanor	k1gInSc4	Nikanor
Bosoj	Bosoj	k1gInSc4	Bosoj
(	(	kIx(	(
<g/>
Bosý	bosý	k2eAgInSc4d1	bosý
<g/>
)	)	kIx)	)
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
domovní	domovní	k2eAgFnSc2d1	domovní
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
zatčen	zatčen	k2eAgMnSc1d1	zatčen
za	za	k7c4	za
spekulaci	spekulace	k1gFnSc4	spekulace
s	s	k7c7	s
Korovjevem	Korovjev	k1gInSc7	Korovjev
podvrženými	podvržený	k2eAgInPc7d1	podvržený
falešnými	falešný	k2eAgInPc7d1	falešný
dolary	dolar	k1gInPc7	dolar
Žorž	Žorž	k1gFnSc4	Žorž
Bengalskij	Bengalskij	k1gFnSc2	Bengalskij
–	–	k?	–
konferenciér	konferenciér	k1gMnSc1	konferenciér
Varieté	varieté	k1gNnSc1	varieté
<g/>
,	,	kIx,	,
Behemot	behemot	k1gMnSc1	behemot
mu	on	k3xPp3gMnSc3	on
na	na	k7c4	na
četná	četný	k2eAgNnPc4d1	četné
přání	přání	k1gNnPc4	přání
diváků	divák	k1gMnPc2	divák
utrhne	utrhnout	k5eAaPmIp3nS	utrhnout
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
mu	on	k3xPp3gMnSc3	on
ji	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
na	na	k7c4	na
přání	přání	k1gNnPc4	přání
diváků	divák	k1gMnPc2	divák
<g/>
,	,	kIx,	,
navrátí	navrátit	k5eAaPmIp3nS	navrátit
nazpět	nazpět	k6eAd1	nazpět
<g/>
.	.	kIx.	.
</s>
<s>
Dotyčný	dotyčný	k2eAgMnSc1d1	dotyčný
přežije	přežít	k5eAaPmIp3nS	přežít
a	a	k8xC	a
dostane	dostat	k5eAaPmIp3nS	dostat
se	se	k3xPyFc4	se
na	na	k7c4	na
psychiatrickou	psychiatrický	k2eAgFnSc4d1	psychiatrická
kliniku	klinika	k1gFnSc4	klinika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
dá	dát	k5eAaPmIp3nS	dát
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
vzor	vzor	k1gInSc1	vzor
postavy	postava	k1gFnSc2	postava
posloužil	posloužit	k5eAaPmAgInS	posloužit
reálně	reálně	k6eAd1	reálně
žijící	žijící	k2eAgMnSc1d1	žijící
konferenciér	konferenciér	k1gMnSc1	konferenciér
Georg	Georg	k1gMnSc1	Georg
Razdolskij	Razdolskij	k1gMnSc1	Razdolskij
<g/>
.	.	kIx.	.
</s>
<s>
Andrej	Andrej	k1gMnSc1	Andrej
Sokov	Sokovo	k1gNnPc2	Sokovo
–	–	k?	–
bufetář	bufetář	k1gMnSc1	bufetář
Varieté	varieté	k1gNnSc1	varieté
<g/>
,	,	kIx,	,
jemu	on	k3xPp3gNnSc3	on
jsou	být	k5eAaImIp3nP	být
určena	určen	k2eAgNnPc1d1	určeno
Wollandova	Wollandův	k2eAgNnPc1d1	Wollandův
slova	slovo	k1gNnPc1	slovo
"	"	kIx"	"
<g/>
Brynza	brynza	k1gFnSc1	brynza
nemá	mít	k5eNaImIp3nS	mít
mít	mít	k5eAaImF	mít
zelenou	zelený	k2eAgFnSc4d1	zelená
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
příteli	přítel	k1gMnSc5	přítel
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
si	se	k3xPyFc3	se
z	z	k7c2	z
vás	vy	k3xPp2nPc4	vy
někdo	někdo	k3yInSc1	někdo
udělal	udělat	k5eAaPmAgMnS	udělat
dobrý	dobrý	k2eAgInSc4d1	dobrý
den	den	k1gInSc4	den
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
"	"	kIx"	"
<g/>
Jeseteřina	jeseteřina	k1gFnSc1	jeseteřina
druhé	druhý	k4xOgFnSc2	druhý
jakosti	jakost	k1gFnSc2	jakost
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
,	,	kIx,	,
jeseteřina	jeseteřina	k1gFnSc1	jeseteřina
má	mít	k5eAaImIp3nS	mít
jakost	jakost	k1gFnSc4	jakost
jedinou	jediný	k2eAgFnSc4d1	jediná
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
<g/>
,	,	kIx,	,
kterážto	kterážto	k?	kterážto
jest	být	k5eAaImIp3nS	být
i	i	k9	i
jakostí	jakost	k1gFnSc7	jakost
poslední	poslední	k2eAgFnSc7d1	poslední
<g/>
"	"	kIx"	"
Maximilian	Maximiliana	k1gFnPc2	Maximiliana
Andrejevič	Andrejevič	k1gInSc1	Andrejevič
Poplavskij	Poplavskij	k1gMnSc1	Poplavskij
–	–	k?	–
Berliozův	Berliozův	k2eAgMnSc1d1	Berliozův
strýc	strýc	k1gMnSc1	strýc
z	z	k7c2	z
Kyjeva	Kyjev	k1gInSc2	Kyjev
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Bulgakov	Bulgakov	k1gInSc1	Bulgakov
narodil	narodit	k5eAaPmAgInS	narodit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
tamní	tamní	k2eAgFnSc2d1	tamní
plánovací	plánovací	k2eAgFnSc2d1	plánovací
komise	komise	k1gFnSc2	komise
<g/>
.	.	kIx.	.
</s>
<s>
Přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
získat	získat	k5eAaPmF	získat
byt	byt	k1gInSc4	byt
po	po	k7c6	po
mrtvém	mrtvý	k2eAgMnSc6d1	mrtvý
Berliozovi	Berlioz	k1gMnSc6	Berlioz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
šokujícím	šokující	k2eAgNnSc6d1	šokující
setkání	setkání	k1gNnSc6	setkání
s	s	k7c7	s
Wollandem	Wolland	k1gInSc7	Wolland
raději	rád	k6eAd2	rád
mizí	mizet	k5eAaImIp3nS	mizet
a	a	k8xC	a
uvážlivě	uvážlivě	k6eAd1	uvážlivě
úřadům	úřad	k1gInPc3	úřad
nic	nic	k3yNnSc1	nic
nehlásí	hlásit	k5eNaImIp3nP	hlásit
<g/>
.	.	kIx.	.
</s>
<s>
Nataša	Nataša	k1gFnSc1	Nataša
–	–	k?	–
Markétina	Markétin	k2eAgFnSc1d1	Markétina
služka	služka	k1gFnSc1	služka
<g/>
,	,	kIx,	,
po	po	k7c6	po
zralé	zralý	k2eAgFnSc6d1	zralá
úvaze	úvaha	k1gFnSc6	úvaha
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
vědmou	vědma	k1gFnSc7	vědma
profesor	profesor	k1gMnSc1	profesor
Stravinskij	Stravinskij	k1gFnSc2	Stravinskij
–	–	k?	–
kapacita	kapacita	k1gFnSc1	kapacita
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
psychiatrie	psychiatrie	k1gFnSc2	psychiatrie
<g/>
,	,	kIx,	,
léčí	léčit	k5eAaImIp3nP	léčit
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
Wollandových	Wollandový	k2eAgFnPc2d1	Wollandový
obětí	oběť	k1gFnPc2	oběť
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
jednajících	jednající	k2eAgFnPc2d1	jednající
postav	postava	k1gFnPc2	postava
nikterak	nikterak	k6eAd1	nikterak
neutrpí	utrpět	k5eNaPmIp3nS	utrpět
<g/>
.	.	kIx.	.
</s>
<s>
Postava	postava	k1gFnSc1	postava
má	mít	k5eAaImIp3nS	mít
živý	živý	k2eAgInSc4d1	živý
prototyp	prototyp	k1gInSc4	prototyp
-	-	kIx~	-
ředitele	ředitel	k1gMnSc2	ředitel
kliniky	klinika	k1gFnSc2	klinika
1	[number]	k4	1
MGU	MGU	kA	MGU
profesora	profesor	k1gMnSc2	profesor
G.	G.	kA	G.
I.	I.	kA	I.
Rossolima	Rossolima	k1gFnSc1	Rossolima
Kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
vyprávěna	vyprávět	k5eAaImNgFnS	vyprávět
v	v	k7c6	v
er-formě	erorma	k1gFnSc6	er-forma
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
si	se	k3xPyFc3	se
vypravěč	vypravěč	k1gMnSc1	vypravěč
neodpouští	odpouštět	k5eNaImIp3nS	odpouštět
subjektivní	subjektivní	k2eAgFnPc4d1	subjektivní
poznámky	poznámka	k1gFnPc4	poznámka
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
je	být	k5eAaImIp3nS	být
psáno	psát	k5eAaImNgNnS	psát
čtivým	čtivý	k2eAgInSc7d1	čtivý
stylem	styl	k1gInSc7	styl
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
vyprávěn	vyprávět	k5eAaImNgInS	vyprávět
ne	ne	k9	ne
úplně	úplně	k6eAd1	úplně
striktně	striktně	k6eAd1	striktně
dodržovanou	dodržovaný	k2eAgFnSc7d1	dodržovaná
spisovnou	spisovný	k2eAgFnSc7d1	spisovná
řečí	řeč	k1gFnSc7	řeč
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
vše	všechen	k3xTgNnSc1	všechen
působí	působit	k5eAaImIp3nS	působit
přirozeně	přirozeně	k6eAd1	přirozeně
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
též	též	k6eAd1	též
proložena	proložit	k5eAaPmNgFnS	proložit
zábavnými	zábavný	k2eAgInPc7d1	zábavný
dialogy	dialog	k1gInPc7	dialog
a	a	k8xC	a
postavami	postava	k1gFnPc7	postava
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
také	také	k9	také
přidává	přidávat	k5eAaImIp3nS	přidávat
na	na	k7c6	na
čitelnosti	čitelnost	k1gFnSc6	čitelnost
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
často	často	k6eAd1	často
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
také	také	k9	také
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
vyprávění	vyprávění	k1gNnSc4	vyprávění
jiného	jiný	k2eAgInSc2d1	jiný
příběhu	příběh	k1gInSc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
všechno	všechen	k3xTgNnSc4	všechen
lze	lze	k6eAd1	lze
uvést	uvést	k5eAaPmF	uvést
příběh	příběh	k1gInSc4	příběh
o	o	k7c6	o
Pilátovi	Pilát	k1gMnSc6	Pilát
Pontském	pontský	k2eAgMnSc6d1	pontský
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
provází	provázet	k5eAaImIp3nS	provázet
čtenáře	čtenář	k1gMnSc4	čtenář
celou	celý	k2eAgFnSc7d1	celá
knihou	kniha	k1gFnSc7	kniha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
autor	autor	k1gMnSc1	autor
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
příběh	příběh	k1gInSc4	příběh
biblické	biblický	k2eAgFnSc2d1	biblická
postavy	postava	k1gFnSc2	postava
dle	dle	k7c2	dle
své	svůj	k3xOyFgFnSc2	svůj
fantazie	fantazie	k1gFnSc2	fantazie
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vlastně	vlastně	k9	vlastně
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
jakýsi	jakýsi	k3yIgInSc4	jakýsi
apokryf	apokryf	k1gInSc4	apokryf
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
popisů	popis	k1gInPc2	popis
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
je	být	k5eAaImIp3nS	být
vedena	vést	k5eAaImNgFnS	vést
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
čtenář	čtenář	k1gMnSc1	čtenář
má	mít	k5eAaImIp3nS	mít
mnoho	mnoho	k4c4	mnoho
příležitostí	příležitost	k1gFnPc2	příležitost
zapojit	zapojit	k5eAaPmF	zapojit
svou	svůj	k3xOyFgFnSc4	svůj
fantazii	fantazie	k1gFnSc4	fantazie
a	a	k8xC	a
představit	představit	k5eAaPmF	představit
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
popisované	popisovaný	k2eAgInPc1d1	popisovaný
jevy	jev	k1gInPc4	jev
podle	podle	k7c2	podle
svého	svůj	k3xOyFgNnSc2	svůj
gusta	gusto	k1gNnSc2	gusto
<g/>
.	.	kIx.	.
</s>
<s>
Podání	podání	k1gNnSc1	podání
ďáblových	ďáblův	k2eAgMnPc2d1	ďáblův
pomocníků	pomocník	k1gMnPc2	pomocník
je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
taktéž	taktéž	k?	taktéž
velice	velice	k6eAd1	velice
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
.	.	kIx.	.
</s>
<s>
Vyprávěním	vyprávění	k1gNnSc7	vyprávění
o	o	k7c6	o
Ďáblu	ďábel	k1gMnSc6	ďábel
jako	jako	k8xS	jako
o	o	k7c6	o
postavě	postava	k1gFnSc6	postava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nepáchá	páchat	k5eNaImIp3nS	páchat
pouze	pouze	k6eAd1	pouze
zlo	zlo	k1gNnSc4	zlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
chová	chovat	k5eAaImIp3nS	chovat
se	se	k3xPyFc4	se
docela	docela	k6eAd1	docela
lidsky	lidsky	k6eAd1	lidsky
<g/>
,	,	kIx,	,
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
Bulgakov	Bulgakov	k1gInSc4	Bulgakov
parafrázi	parafráze	k1gFnSc4	parafráze
na	na	k7c4	na
Fausta	Faust	k1gMnSc4	Faust
<g/>
.	.	kIx.	.
</s>
<s>
Vložený	vložený	k2eAgInSc1d1	vložený
příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
Pilátovi	Pilát	k1gMnSc6	Pilát
<g/>
,	,	kIx,	,
čtivost	čtivost	k1gFnSc4	čtivost
<g/>
,	,	kIx,	,
popisy	popis	k1gInPc4	popis
poskytující	poskytující	k2eAgInSc4d1	poskytující
dobrý	dobrý	k2eAgInSc4d1	dobrý
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
fantazii	fantazie	k1gFnSc4	fantazie
a	a	k8xC	a
humor	humor	k1gInSc4	humor
činí	činit	k5eAaImIp3nS	činit
z	z	k7c2	z
románu	román	k1gInSc2	román
knihu	kniha	k1gFnSc4	kniha
pro	pro	k7c4	pro
velmi	velmi	k6eAd1	velmi
široký	široký	k2eAgInSc4d1	široký
okruh	okruh	k1gInSc4	okruh
čtenářů	čtenář	k1gMnPc2	čtenář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
vyšla	vyjít	k5eAaPmAgFnS	vyjít
též	též	k9	též
komiksová	komiksový	k2eAgFnSc1d1	komiksová
adaptace	adaptace	k1gFnSc1	adaptace
Andrzeje	Andrzeje	k1gMnSc2	Andrzeje
Klimowského	Klimowský	k1gMnSc2	Klimowský
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
BB	BB	kA	BB
art	art	k?	art
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc4	jejíž
textové	textový	k2eAgFnPc4d1	textová
části	část	k1gFnPc4	část
přeložil	přeložit	k5eAaPmAgMnS	přeložit
podle	podle	k7c2	podle
anglické	anglický	k2eAgFnSc2d1	anglická
verze	verze	k1gFnSc2	verze
a	a	k8xC	a
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
Dvořákova	Dvořákův	k2eAgInSc2d1	Dvořákův
překladu	překlad	k1gInSc2	překlad
Viktor	Viktor	k1gMnSc1	Viktor
Janiš	Janiš	k1gMnSc1	Janiš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Italsko-Jugoslávský	italskougoslávský	k2eAgInSc1d1	italsko-jugoslávský
film	film	k1gInSc1	film
Mistr	mistr	k1gMnSc1	mistr
a	a	k8xC	a
Markétka	Markétka	k1gFnSc1	Markétka
<g/>
,	,	kIx,	,
režíroval	režírovat	k5eAaImAgMnS	režírovat
ho	on	k3xPp3gMnSc4	on
Aleksandar	Aleksandar	k1gMnSc1	Aleksandar
Petrović	Petrović	k1gMnSc1	Petrović
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
délku	délka	k1gFnSc4	délka
95	[number]	k4	95
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Polská	polský	k2eAgFnSc1d1	polská
verze	verze	k1gFnSc1	verze
Mistrz	Mistrz	k1gMnSc1	Mistrz
i	i	k8xC	i
Małgorzata	Małgorzata	k1gFnSc1	Małgorzata
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
natočena	natočit	k5eAaBmNgFnS	natočit
jako	jako	k8xC	jako
televizní	televizní	k2eAgInSc4d1	televizní
miniseriál	miniseriál	k1gInSc4	miniseriál
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
ruská	ruský	k2eAgFnSc1d1	ruská
adaptace	adaptace	k1gFnSc1	adaptace
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
byl	být	k5eAaImAgInS	být
desetidílný	desetidílný	k2eAgInSc1d1	desetidílný
seriál	seriál	k1gInSc1	seriál
natočený	natočený	k2eAgInSc1d1	natočený
s	s	k7c7	s
rozpočtem	rozpočet	k1gInSc7	rozpočet
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
Českou	český	k2eAgFnSc7d1	Česká
televizí	televize	k1gFnSc7	televize
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
premiéře	premiéra	k1gFnSc6	premiéra
uveden	uvést	k5eAaPmNgMnS	uvést
v	v	k7c6	v
březnu-dubnu	březnuubnout	k5eAaPmIp1nS	březnu-dubnout
2010	[number]	k4	2010
na	na	k7c6	na
programu	program	k1gInSc6	program
ČT	ČT	kA	ČT
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
