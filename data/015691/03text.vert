<s>
Vojenský	vojenský	k2eAgInSc1d1
řád	řád	k1gInSc1
Marie	Maria	k1gFnSc2
Terezie	Terezie	k1gFnSc2
</s>
<s>
Vojenský	vojenský	k2eAgInSc1d1
řád	řád	k1gInSc1
Marie	Maria	k1gFnSc2
Terezie	Terezie	k1gFnSc2
</s>
<s>
Militär-Maria-Theresien-OrdenKatonai	Militär-Maria-Theresien-OrdenKatonai	k6eAd1
Mária	Mária	k1gFnSc1
Terézia-rend	Terézia-renda	k1gFnPc2
</s>
<s>
Uděluje	udělovat	k5eAaImIp3nS
Habsburská	habsburský	k2eAgFnSc1d1
monarchie	monarchie	k1gFnSc1
<g/>
,	,	kIx,
<g/>
Rakouské	rakouský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
a	a	k8xC
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
</s>
<s>
Typ	typ	k1gInSc1
</s>
<s>
vojenský	vojenský	k2eAgInSc1d1
záslužný	záslužný	k2eAgInSc1d1
řád	řád	k1gInSc1
</s>
<s>
Založeno	založen	k2eAgNnSc1d1
</s>
<s>
1757	#num#	k4
</s>
<s>
Stát	stát	k1gInSc1
</s>
<s>
Habsburská	habsburský	k2eAgFnSc1d1
monarchie	monarchie	k1gFnSc1
Habsburská	habsburský	k2eAgFnSc1d1
monarchieRakouské	monarchieRakouský	k2eAgNnSc4d1
císařství	císařství	k1gNnSc4
Rakouské	rakouský	k2eAgNnSc4d1
císařstvíRakousko-Uhersko	císařstvíRakousko-Uhersko	k6eAd1
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc4
</s>
<s>
Heslo	heslo	k1gNnSc1
</s>
<s>
Fortitudini	Fortitudin	k2eAgMnPc1d1
</s>
<s>
Zakladatel	zakladatel	k1gMnSc1
</s>
<s>
Marie	Marie	k1gFnSc1
Terezie	Terezie	k1gFnSc2
</s>
<s>
Třídy	třída	k1gFnPc1
</s>
<s>
velkokřížkomturrytíř	velkokřížkomturrytíř	k1gFnSc1
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
zlatý	zlatý	k2eAgInSc1d1
tlapatý	tlapatý	k2eAgInSc1d1
kříž	kříž	k1gInSc1
v	v	k7c6
bílém	bílé	k1gNnSc6
smaltu	smalt	k1gInSc2
<g/>
,	,	kIx,
ve	v	k7c6
středu	střed	k1gInSc6
je	být	k5eAaImIp3nS
medailon	medailon	k1gInSc1
s	s	k7c7
bílým	bílý	k2eAgNnSc7d1
břevnem	břevno	k1gNnSc7
na	na	k7c6
červeném	červený	k2eAgNnSc6d1
poli	pole	k1gNnSc6
a	a	k8xC
kolem	kolem	k6eAd1
je	být	k5eAaImIp3nS
opis	opis	k1gInSc1
řádového	řádový	k2eAgInSc2d1
heslahvězdou	heslahvězda	k1gMnSc7
je	být	k5eAaImIp3nS
větší	veliký	k2eAgInSc1d2
stříbrný	stříbrný	k2eAgInSc1d1
řádový	řádový	k2eAgInSc1d1
kříž	kříž	k1gInSc1
<g/>
,	,	kIx,
nesmaltovaný	smaltovaný	k2eNgInSc1d1
<g/>
,	,	kIx,
s	s	k7c7
briliantujícím	briliantující	k2eAgInSc7d1
povrchem	povrch	k1gInSc7
<g/>
,	,	kIx,
mezi	mezi	k7c7
rameny	rameno	k1gNnPc7
je	být	k5eAaImIp3nS
umístěn	umístěn	k2eAgInSc1d1
zeleně	zeleně	k6eAd1
smaltovaný	smaltovaný	k2eAgInSc1d1
vavřín	vavřín	k1gInSc1
</s>
<s>
Zadní	zadní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
řádu	řád	k1gInSc2
</s>
<s>
Vojenský	vojenský	k2eAgInSc1d1
řád	řád	k1gInSc1
Marie	Maria	k1gFnSc2
Terezie	Terezie	k1gFnSc2
(	(	kIx(
<g/>
německy	německy	k6eAd1
Militär-Maria-Theresien-Orden	Militär-Maria-Theresien-Ordna	k1gFnPc2
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
nejvyšší	vysoký	k2eAgInSc1d3
rakouský	rakouský	k2eAgInSc1d1
vojenský	vojenský	k2eAgInSc1d1
řád	řád	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1757	#num#	k4
a	a	k8xC
udělen	udělen	k2eAgMnSc1d1
poprvé	poprvé	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1758	#num#	k4
<g/>
,	,	kIx,
nejprve	nejprve	k6eAd1
v	v	k7c6
habsburské	habsburský	k2eAgFnSc6d1
monarchii	monarchie	k1gFnSc6
<g/>
,	,	kIx,
po	po	k7c6
rozpadu	rozpad	k1gInSc6
Svaté	svatý	k2eAgFnSc2d1
říše	říš	k1gFnSc2
římské	římský	k2eAgFnSc2d1
národa	národ	k1gInSc2
německého	německý	k2eAgInSc2d1
v	v	k7c6
Rakouském	rakouský	k2eAgNnSc6d1
císařství	císařství	k1gNnSc6
a	a	k8xC
následně	následně	k6eAd1
v	v	k7c6
Rakousku-Uhersku	Rakousku-Uhersek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
rozpadu	rozpad	k1gInSc6
Rakouska-Uherska	Rakouska-Uhersko	k1gNnSc2
byl	být	k5eAaImAgInS
ještě	ještě	k6eAd1
několikrát	několikrát	k6eAd1
udělen	udělit	k5eAaPmNgInS
v	v	k7c6
tichosti	tichost	k1gFnSc6
do	do	k7c2
roku	rok	k1gInSc2
1931	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Maďarsku	Maďarsko	k1gNnSc6
se	se	k3xPyFc4
objevily	objevit	k5eAaPmAgFnP
snahy	snaha	k1gFnPc1
o	o	k7c6
jeho	jeho	k3xOp3gNnSc6
obnovení	obnovení	k1gNnSc6
<g/>
,	,	kIx,
naposledy	naposledy	k6eAd1
byl	být	k5eAaImAgInS
udělen	udělit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1944	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Řád	řád	k1gInSc1
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
Marií	Maria	k1gFnSc7
Terezií	Terezie	k1gFnPc2
na	na	k7c4
oslavu	oslava	k1gFnSc4
vítězství	vítězství	k1gNnSc2
rakouských	rakouský	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Kolína	Kolín	k1gInSc2
18	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1757	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řád	řád	k1gInSc1
byl	být	k5eAaImAgInS
udělován	udělovat	k5eAaImNgInS
důstojníkům	důstojník	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
prokázali	prokázat	k5eAaPmAgMnP
neobvyklou	obvyklý	k2eNgFnSc4d1
odvahu	odvaha	k1gFnSc4
při	při	k7c6
boji	boj	k1gInSc6
nepříteli	nepřítel	k1gMnSc3
a	a	k8xC
za	za	k7c4
službu	služba	k1gFnSc4
svému	svůj	k3xOyFgNnSc3
mocnářství	mocnářství	k1gNnSc1
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
byl	být	k5eAaImAgInS
udělován	udělován	k2eAgInSc1d1
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
Für	Für	k?
aus	aus	k?
eigener	eigener	k1gInSc1
Initiative	Initiativ	k1gInSc5
unternommene	unternommenout	k5eAaPmIp3nS
<g/>
,	,	kIx,
erfolgreiche	erfolgreichat	k5eAaPmIp3nS
und	und	k?
einen	einen	k1gInSc1
Feldzug	Feldzug	k1gMnSc1
wesentlich	wesentlich	k1gMnSc1
beeinflussende	beeinflussend	k1gInSc5
Waffentaten	Waffentaten	k2eAgInSc4d1
<g/>
,	,	kIx,
die	die	k?
ein	ein	k?
Offizier	Offizier	k1gInSc1
von	von	k1gInSc4
Ehre	Ehr	k1gFnSc2
hätte	hätte	k5eAaPmIp2nP
ohne	ohnout	k5eAaPmIp3nS
Tadel	Tadel	k1gMnSc1
auch	auch	k1gMnSc1
unterlassen	unterlassen	k2eAgInSc4d1
können	können	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
Což	což	k3yQnSc1,k3yRnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
řád	řád	k1gInSc1
byl	být	k5eAaImAgInS
udělen	udělen	k2eAgInSc4d1
„	„	k?
<g/>
za	za	k7c4
vykonání	vykonání	k1gNnSc4
takového	takový	k3xDgInSc2
činu	čin	k1gInSc2
osobní	osobní	k2eAgFnSc2d1
statečnosti	statečnost	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
by	by	kYmCp3nS
za	za	k7c2
běžných	běžný	k2eAgFnPc2d1
okolností	okolnost	k1gFnPc2
nebyl	být	k5eNaImAgInS
vyžadován	vyžadován	k2eAgInSc1d1
a	a	k8xC
přesto	přesto	k8xC
byl	být	k5eAaImAgInS
vykonán	vykonat	k5eAaPmNgInS
<g/>
,	,	kIx,
případně	případně	k6eAd1
spolu	spolu	k6eAd1
s	s	k7c7
tímto	tento	k3xDgInSc7
činem	čin	k1gInSc7
při	při	k7c6
vojenské	vojenský	k2eAgFnSc6d1
akci	akce	k1gFnSc6
provedení	provedení	k1gNnSc2
mimořádně	mimořádně	k6eAd1
důležitého	důležitý	k2eAgNnSc2d1
a	a	k8xC
dovedného	dovedný	k2eAgNnSc2d1
rozhodnutí	rozhodnutí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Interpretace	interpretace	k1gFnPc1
této	tento	k3xDgFnSc2
části	část	k1gFnSc2
stanov	stanova	k1gFnPc2
dala	dát	k5eAaPmAgFnS
vzniknout	vzniknout	k5eAaPmF
velmi	velmi	k6eAd1
populární	populární	k2eAgFnSc3d1
legendě	legenda	k1gFnSc3
<g/>
,	,	kIx,
že	že	k8xS
byl	být	k5eAaImAgInS
řád	řád	k1gInSc1
udělován	udělovat	k5eAaImNgInS
<g/>
,	,	kIx,
za	za	k7c4
úspěšné	úspěšný	k2eAgNnSc4d1
neuposlechnutí	neuposlechnutí	k1gNnSc4
rozkazu	rozkaz	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yQnSc4,k3yRnSc4
však	však	k9
nebyla	být	k5eNaImAgFnS
pravda	pravda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
udílení	udílení	k1gNnSc6
nehrálo	hrát	k5eNaImAgNnS
roli	role	k1gFnSc4
náboženské	náboženský	k2eAgNnSc1d1
vyznání	vyznání	k1gNnSc1
<g/>
,	,	kIx,
rasa	rasa	k1gFnSc1
<g/>
,	,	kIx,
hodnost	hodnost	k1gFnSc4
ani	ani	k8xC
společenské	společenský	k2eAgNnSc4d1
postavení	postavení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řád	řád	k1gInSc1
byl	být	k5eAaImAgInS
výlučně	výlučně	k6eAd1
udělován	udělovat	k5eAaImNgInS
za	za	k7c4
hrdinské	hrdinský	k2eAgInPc4d1
činy	čin	k1gInPc4
<g/>
,	,	kIx,
ve	v	k7c6
výjimečných	výjimečný	k2eAgInPc6d1
případech	případ	k1gInPc6
z	z	k7c2
dynastických	dynastický	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Zprvu	zprvu	k6eAd1
měl	mít	k5eAaImAgInS
řád	řád	k1gInSc1
dvě	dva	k4xCgFnPc4
třídy	třída	k1gFnPc4
<g/>
,	,	kIx,
velký	velký	k2eAgInSc4d1
a	a	k8xC
rytířský	rytířský	k2eAgInSc4d1
kříž	kříž	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
byl	být	k5eAaImAgInS
řád	řád	k1gInSc1
Josefem	Josef	k1gMnSc7
II	II	kA
<g/>
.	.	kIx.
doplněn	doplnit	k5eAaPmNgInS
ještě	ještě	k9
o	o	k7c4
střední	střední	k2eAgFnSc4d1
komandérskou	komandérský	k2eAgFnSc4d1
třídou	třída	k1gFnSc7
a	a	k8xC
hvězdu	hvězda	k1gFnSc4
pro	pro	k7c4
velkokřižovníky	velkokřižovník	k1gInPc4
15	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1765	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nositelé	nositel	k1gMnPc1
řádu	řád	k1gInSc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
získali	získat	k5eAaPmAgMnP
rytířský	rytířský	k2eAgInSc4d1
kříž	kříž	k1gInSc4
měli	mít	k5eAaImAgMnP
nárok	nárok	k1gInSc4
na	na	k7c4
doživotní	doživotní	k2eAgInSc4d1
šlechtický	šlechtický	k2eAgInSc4d1
titul	titul	k1gInSc4
rytíř	rytíř	k1gMnSc1
(	(	kIx(
<g/>
Ritter	Ritter	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
následně	následně	k6eAd1
se	se	k3xPyFc4
mohli	moct	k5eAaImAgMnP
domáhat	domáhat	k5eAaImF
dědičného	dědičný	k2eAgInSc2d1
titulu	titul	k1gInSc2
svobodného	svobodný	k2eAgMnSc4d1
pána	pán	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řád	řád	k1gInSc1
byl	být	k5eAaImAgInS
také	také	k9
spjat	spjat	k2eAgMnSc1d1
s	s	k7c7
doživotní	doživotní	k2eAgFnSc7d1
rentou	renta	k1gFnSc7
od	od	k7c2
1.500	1.500	k4
do	do	k7c2
400	#num#	k4
zlatých	zlatá	k1gFnPc2
ročně	ročně	k6eAd1
a	a	k8xC
vdovy	vdova	k1gFnPc1
po	po	k7c6
příslušnících	příslušník	k1gMnPc6
řádu	řád	k1gInSc2
měly	mít	k5eAaImAgFnP
nárok	nárok	k1gInSc4
na	na	k7c4
polovic	polovice	k1gFnPc2
této	tento	k3xDgFnSc2
renty	renta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
roce	rok	k1gInSc6
1918	#num#	k4
císař	císař	k1gMnSc1
Karel	Karel	k1gMnSc1
I.	I.	kA
převedl	převést	k5eAaPmAgMnS
svá	svůj	k3xOyFgNnPc4
práva	právo	k1gNnPc4
k	k	k7c3
řádu	řád	k1gInSc3
na	na	k7c4
řádovou	řádový	k2eAgFnSc4d1
kapitulu	kapitula	k1gFnSc4
a	a	k8xC
proběhlo	proběhnout	k5eAaPmAgNnS
ještě	ještě	k9
několik	několik	k4yIc1
tichých	tichý	k2eAgFnPc2d1
řádových	řádový	k2eAgFnPc2d1
promocí	promoce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc1d1
řádová	řádový	k2eAgFnSc1d1
promoce	promoce	k1gFnSc1
proběhla	proběhnout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1931	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
bylo	být	k5eAaImAgNnS
také	také	k9
dohodnuto	dohodnout	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
již	již	k6eAd1
nebude	být	k5eNaImBp3nS
řád	řád	k1gInSc1
dále	daleko	k6eAd2
udělován	udělovat	k5eAaImNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
tohoto	tento	k3xDgInSc2
roku	rok	k1gInSc2
bylo	být	k5eAaImAgNnS
uděleno	udělit	k5eAaPmNgNnS
1039	#num#	k4
malých	malý	k2eAgInPc2d1
křížů	kříž	k1gInPc2
<g/>
,	,	kIx,
140	#num#	k4
komandérských	komandérský	k2eAgInPc2d1
křížů	kříž	k1gInPc2
a	a	k8xC
61	#num#	k4
velkokřížů	velkokříž	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Později	pozdě	k6eAd2
se	se	k3xPyFc4
o	o	k7c6
obnovení	obnovení	k1gNnSc6
řádu	řád	k1gInSc2
snažil	snažit	k5eAaImAgMnS
Miklós	Miklós	k1gInSc4
Horthy	Hortha	k1gFnSc2
v	v	k7c6
Maďarsku	Maďarsko	k1gNnSc6
z	z	k7c2
titulu	titul	k1gInSc2
regenta	regens	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
byl	být	k5eAaImAgMnS
tímto	tento	k3xDgInSc7
"	"	kIx"
<g/>
maďarským	maďarský	k2eAgInSc7d1
<g/>
"	"	kIx"
řádem	řád	k1gInSc7
vyznamenaný	vyznamenaný	k2eAgInSc4d1
pouze	pouze	k6eAd1
jeden	jeden	k4xCgMnSc1
člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
generálmajor	generálmajor	k1gMnSc1
Ladislaus	Ladislaus	k1gMnSc1
Oszlanyi	Oszlanye	k1gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
1944	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posledním	poslední	k2eAgInSc7d1
žijícím	žijící	k2eAgInSc7d1
členem	člen	k1gInSc7
řádu	řád	k1gInSc2
byl	být	k5eAaImAgInS
Gottfried	Gottfried	k1gInSc1
von	von	k1gInSc1
Banfield	Banfield	k1gInSc4
<g/>
,	,	kIx,
vyznamenaný	vyznamenaný	k2eAgInSc4d1
v	v	k7c6
roce	rok	k1gInSc6
1917	#num#	k4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1986	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Organizace	organizace	k1gFnSc1
a	a	k8xC
insignie	insignie	k1gFnPc1
</s>
<s>
Velmistry	velmistr	k1gMnPc7
řádu	řád	k1gInSc2
byli	být	k5eAaImAgMnP
nástupci	nástupce	k1gMnPc1
Marie	Marie	k1gFnSc1
Terezie	Terezie	k1gFnSc1
–	–	k?
habsburští	habsburský	k2eAgMnPc1d1
monarchové	monarcha	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
ještě	ještě	k6eAd1
věci	věc	k1gFnSc2
řádu	řád	k1gInSc2
spravovali	spravovat	k5eAaImAgMnP
další	další	k2eAgMnPc1d1
tři	tři	k4xCgMnPc1
řádoví	řádový	k2eAgMnPc1d1
úředníci	úředník	k1gMnPc1
<g/>
:	:	kIx,
kancléř	kancléř	k1gMnSc1
<g/>
,	,	kIx,
pokladník	pokladník	k1gMnSc1
a	a	k8xC
grefiér	grefiér	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Řádovým	řádový	k2eAgInSc7d1
odznakem	odznak	k1gInSc7
byl	být	k5eAaImAgInS
specifický	specifický	k2eAgInSc1d1
<g/>
,	,	kIx,
tzv.	tzv.	kA
tereziánský	tereziánský	k2eAgMnSc1d1
<g/>
,	,	kIx,
kříž	kříž	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
měl	mít	k5eAaImAgInS
tlapatá	tlapatý	k2eAgNnPc4d1
ramena	rameno	k1gNnPc4
<g/>
,	,	kIx,
ukončená	ukončený	k2eAgNnPc4d1
vlnovitě	vlnovitě	k6eAd1
prohnutou	prohnutý	k2eAgFnSc7d1
linií	linie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
středu	střed	k1gInSc6
byl	být	k5eAaImAgInS
medailon	medailon	k1gInSc1
vyplněný	vyplněný	k2eAgInSc1d1
Habsburky	Habsburk	k1gMnPc7
adoptovaným	adoptovaný	k2eAgInSc7d1
babenberským	babenberský	k2eAgInSc7d1
erbem	erb	k1gInSc7
s	s	k7c7
okružím	okruží	k1gNnSc7
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yRgNnSc6,k3yQgNnSc6,k3yIgNnSc6
napsáno	napsat	k5eAaBmNgNnS,k5eAaPmNgNnS
„	„	k?
<g/>
FORTITUDINI	FORTITUDINI	kA
<g/>
“	“	k?
(	(	kIx(
<g/>
statečnost	statečnost	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
reverzu	reverz	k1gInSc6
řádu	řád	k1gInSc2
jsou	být	k5eAaImIp3nP
iniciály	iniciála	k1gFnPc1
„	„	k?
<g/>
MTF	MTF	kA
<g/>
“	“	k?
(	(	kIx(
<g/>
MT	MT	kA
pro	pro	k7c4
Marii	Maria	k1gFnSc4
Terezii	Terezie	k1gFnSc4
a	a	k8xC
F	F	kA
pro	pro	k7c4
Františka	František	k1gMnSc4
I.	I.	kA
<g/>
)	)	kIx)
uzavřený	uzavřený	k2eAgMnSc1d1
do	do	k7c2
zlatého	zlatý	k2eAgInSc2d1
vavřínového	vavřínový	k2eAgInSc2d1
věnce	věnec	k1gInSc2
a	a	k8xC
zeleného	zelený	k2eAgNnSc2d1
mezikruží	mezikruží	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Později	pozdě	k6eAd2
přidaná	přidaný	k2eAgFnSc1d1
řádová	řádový	k2eAgFnSc1d1
hvězda	hvězda	k1gFnSc1
kopírovala	kopírovat	k5eAaImAgFnS
tvar	tvar	k1gInSc4
tereziánského	tereziánský	k2eAgInSc2d1
kříže	kříž	k1gInSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
však	však	k9
poněkud	poněkud	k6eAd1
větší	veliký	k2eAgFnSc1d2
a	a	k8xC
měla	mít	k5eAaImAgFnS
mezi	mezi	k7c7
rameny	rameno	k1gNnPc7
vložený	vložený	k2eAgInSc4d1
zelený	zelený	k2eAgInSc4d1
věnec	věnec	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc4
ramena	rameno	k1gNnPc1
byla	být	k5eAaImAgNnP
stříbrně	stříbrně	k6eAd1
dracounovaná	dracounovaný	k2eAgNnPc1d1
nebo	nebo	k8xC
briliantovaná	briliantovaný	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stuha	stuha	k1gFnSc1
byla	být	k5eAaImAgFnS
bílá	bílý	k2eAgFnSc1d1
se	s	k7c7
širokými	široký	k2eAgInPc7d1
červenými	červený	k2eAgInPc7d1
okraji	okraj	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Třídy	třída	k1gFnPc1
a	a	k8xC
způsoby	způsob	k1gInPc1
nošení	nošení	k1gNnSc2
</s>
<s>
Řád	řád	k1gInSc1
Marie	Maria	k1gFnSc2
Terezie	Terezie	k1gFnSc2
se	se	k3xPyFc4
dělil	dělit	k5eAaImAgInS
do	do	k7c2
celkem	celkem	k6eAd1
3	#num#	k4
tříd	třída	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
po	po	k7c6
úpravě	úprava	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1765	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
velkokříž	velkokříž	k1gInSc1
(	(	kIx(
<g/>
odznak	odznak	k1gInSc1
na	na	k7c4
velkostuze	velkostuze	k1gFnPc4
<g/>
,	,	kIx,
hvězda	hvězda	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
komandér	komandér	k1gMnSc1
(	(	kIx(
<g/>
odznak	odznak	k1gInSc1
na	na	k7c6
krku	krk	k1gInSc6
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1765	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
rytíř	rytíř	k1gMnSc1
(	(	kIx(
<g/>
odznak	odznak	k1gInSc1
na	na	k7c6
prsou	prsa	k1gNnPc6
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Způsob	způsob	k1gInSc1
nošení	nošení	k1gNnSc2
řádu	řád	k1gInSc2
Marie	Maria	k1gFnSc2
Terezie	Terezie	k1gFnSc2
na	na	k7c6
vojenské	vojenský	k2eAgFnSc6d1
uniformě	uniforma	k1gFnSc6
od	od	k7c2
nejnižší	nízký	k2eAgFnSc2d3
k	k	k7c3
nejvyšší	vysoký	k2eAgMnSc1d3
třídy	třída	k1gFnPc4
(	(	kIx(
<g/>
zleva	zleva	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
pravidel	pravidlo	k1gNnPc2
se	se	k3xPyFc4
musel	muset	k5eAaImAgInS
řád	řád	k1gInSc1
nosit	nosit	k5eAaImF
in	in	k?
natura	natura	k1gFnSc1
pouze	pouze	k6eAd1
na	na	k7c6
vojenské	vojenský	k2eAgFnSc6d1
uniformě	uniforma	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
toho	ten	k3xDgInSc2
důvodu	důvod	k1gInSc2
byly	být	k5eAaImAgFnP
zavedeny	zaveden	k2eAgFnPc1d1
miniatury	miniatura	k1gFnPc1
na	na	k7c6
řetízku	řetízek	k1gInSc6
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
připínaly	připínat	k5eAaImAgFnP
na	na	k7c4
civilní	civilní	k2eAgNnSc4d1
oblečení	oblečení	k1gNnSc4
(	(	kIx(
<g/>
konkrétně	konkrétně	k6eAd1
na	na	k7c4
levou	levý	k2eAgFnSc4d1
klopu	klopa	k1gFnSc4
saka	sako	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Významní	významný	k2eAgMnPc1d1
nositelé	nositel	k1gMnPc1
</s>
<s>
V	v	k7c6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
patřil	patřit	k5eAaImAgMnS
k	k	k7c3
nejvýznamnějším	významný	k2eAgMnPc3d3
nositelům	nositel	k1gMnPc3
například	například	k6eAd1
maršál	maršál	k1gMnSc1
Ernst	Ernst	k1gMnSc1
Gideon	Gideona	k1gFnPc2
von	von	k1gInSc1
Laudon	Laudon	k1gMnSc1
(	(	kIx(
<g/>
velkokříž	velkokříž	k1gInSc1
s	s	k7c7
brilianty	briliant	k1gInPc7
<g/>
)	)	kIx)
nebo	nebo	k8xC
ruský	ruský	k2eAgMnSc1d1
vojevůdce	vojevůdce	k1gMnSc1
maršál	maršál	k1gMnSc1
Alexandr	Alexandr	k1gMnSc1
Vasiljevič	Vasiljevič	k1gMnSc1
Suvorov	Suvorov	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byli	být	k5eAaImAgMnP
vyznamenáni	vyznamenán	k2eAgMnPc1d1
například	například	k6eAd1
polní	polní	k2eAgMnSc1d1
maršál	maršál	k1gMnSc1
Karel	Karel	k1gMnSc1
Filip	Filip	k1gMnSc1
ze	z	k7c2
Schwarzenbergu	Schwarzenberg	k1gInSc2
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
postupně	postupně	k6eAd1
rytíř	rytíř	k1gMnSc1
<g/>
,	,	kIx,
komandér	komandér	k1gMnSc1
<g/>
,	,	kIx,
velkokříž	velkokříž	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
polní	polní	k2eAgMnSc1d1
maršál	maršál	k1gMnSc1
Josef	Josef	k1gMnSc1
Václav	Václav	k1gMnSc1
Radecký	Radecký	k1gMnSc1
z	z	k7c2
Radče	Radč	k1gFnSc2
(	(	kIx(
<g/>
postupně	postupně	k6eAd1
rytíř	rytíř	k1gMnSc1
<g/>
,	,	kIx,
komandér	komandér	k1gMnSc1
a	a	k8xC
později	pozdě	k6eAd2
velkokříž	velkokříž	k1gInSc1
řádu	řád	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
polní	polní	k2eAgMnSc1d1
maršál	maršál	k1gMnSc1
arcivévoda	arcivévoda	k1gMnSc1
Karel	Karel	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
(	(	kIx(
<g/>
velkokříž	velkokříž	k1gInSc1
s	s	k7c7
brilianty	briliant	k1gInPc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
generál	generál	k1gMnSc1
Leopold	Leopold	k1gMnSc1
ze	z	k7c2
Šternberka	Šternberk	k1gInSc2
(	(	kIx(
<g/>
velkokříž	velkokříž	k1gInSc1
s	s	k7c7
brilianty	briliant	k1gInPc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
generálmajor	generálmajor	k1gMnSc1
Josef	Josefa	k1gFnPc2
von	von	k1gInSc1
Smola	Smola	k1gMnSc1
(	(	kIx(
<g/>
rytíř	rytíř	k1gMnSc1
a	a	k8xC
komandér	komandér	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
generál	generál	k1gMnSc1
z	z	k7c2
období	období	k1gNnSc2
napoleonských	napoleonský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
Václav	Václav	k1gMnSc1
Jan	Jan	k1gMnSc1
Frierenberger	Frierenberger	k1gMnSc1
a	a	k8xC
další	další	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Hvězda	Hvězda	k1gMnSc1
velkokříže	velkokříž	k1gInSc2
řádu	řád	k1gInSc2
s	s	k7c7
brilianty	briliant	k1gInPc7
</s>
<s>
Revers	revers	k1gInSc1
řádu	řád	k1gInSc2
s	s	k7c7
iniciálami	iniciála	k1gFnPc7
Marie	Maria	k1gFnSc2
Terezie	Terezie	k1gFnSc2
a	a	k8xC
Františka	František	k1gMnSc2
Lotrinského	lotrinský	k2eAgInSc2d1
</s>
<s>
Maršál	maršál	k1gMnSc1
Radecký	Radecký	k1gMnSc1
s	s	k7c7
hvězdou	hvězda	k1gFnSc7
velkokříže	velkokříž	k1gInSc2
řádu	řád	k1gInSc2
</s>
<s>
Maršál	maršál	k1gMnSc1
Karel	Karel	k1gMnSc1
Filip	Filip	k1gMnSc1
ze	z	k7c2
Schwarzenbergu	Schwarzenberg	k1gInSc2
s	s	k7c7
velkostuhou	velkostuha	k1gFnSc7
a	a	k8xC
hvězdou	hvězda	k1gFnSc7
velkokříže	velkokříž	k1gInSc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
překlady	překlad	k1gInPc1
textů	text	k1gInPc2
z	z	k7c2
článků	článek	k1gInPc2
Maria-Theresia-Orden	Maria-Theresia-Ordno	k1gNnPc2
na	na	k7c6
německé	německý	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
a	a	k8xC
Military	Militara	k1gFnSc2
Order	Order	k1gMnSc1
of	of	k?
Maria	Maria	k1gFnSc1
Theresa	Theresa	k1gFnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
http://www.militaria.cz/cz/clanky/vojenska-symbolika/vojensky-rad-marie-terezie.html1	http://www.militaria.cz/cz/clanky/vojenska-symbolika/vojensky-rad-marie-terezie.html1	k4
2	#num#	k4
3	#num#	k4
LOBKOWICZ	Lobkowicz	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyklopedie	encyklopedie	k1gFnSc1
řádů	řád	k1gInPc2
a	a	k8xC
vyznamenání	vyznamenání	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
238	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
901579	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heslo	heslo	k1gNnSc4
Vojenský	vojenský	k2eAgInSc1d1
řád	řád	k1gInSc1
Marie	Maria	k1gFnSc2
Terezie	Terezie	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
124	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
KOLÁČNÝ	KOLÁČNÝ	kA
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řády	řád	k1gInPc1
a	a	k8xC
vyznamenání	vyznamenání	k1gNnSc1
habsburské	habsburský	k2eAgFnSc2d1
monarchie	monarchie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Elka	Elka	k1gFnSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
304	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
902745	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
88	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Encyklopedie	encyklopedie	k1gFnPc4
řádů	řád	k1gInPc2
<g/>
,	,	kIx,
s.	s.	k?
124	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Řády	řád	k1gInPc7
a	a	k8xC
vyznamenání	vyznamenání	k1gNnSc1
<g/>
,	,	kIx,
s.	s.	k?
89	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
PERNES	PERNES	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
FUČÍK	Fučík	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
;	;	kIx,
HAVEL	Havel	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
císařským	císařský	k2eAgInSc7d1
praporem	prapor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historie	historie	k1gFnSc1
habsburské	habsburský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
1526	#num#	k4
<g/>
-	-	kIx~
<g/>
1918	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Elka	Elka	k1gFnSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
555	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
902745	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Ivan	Ivan	k1gMnSc1
Koláčný	Koláčný	k2eAgMnSc1d1
<g/>
,	,	kIx,
Řády	řád	k1gInPc7
a	a	k8xC
vyznamenání	vyznamenání	k1gNnSc1
Habsburské	habsburský	k2eAgFnSc2d1
monarchie	monarchie	k1gFnSc2
<g/>
,	,	kIx,
Elka	Elk	k1gInSc2
Press	Press	k1gInSc1
Praha	Praha	k1gFnSc1
2006	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-902745-9-5	80-902745-9-5	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Vojenský	vojenský	k2eAgInSc4d1
řád	řád	k1gInSc4
Marie	Maria	k1gFnSc2
Terezie	Terezie	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Vojenský	vojenský	k2eAgInSc1d1
řád	řád	k1gInSc1
Marie	Maria	k1gFnSc2
Terezie	Terezie	k1gFnSc2
–	–	k?
článek	článek	k1gInSc1
na	na	k7c4
militaria	militarium	k1gNnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Vojenský	vojenský	k2eAgInSc1d1
řád	řád	k1gInSc1
Marie	Maria	k1gFnSc2
Terezie	Terezie	k1gFnSc2
</s>
<s>
Rád	rád	k6eAd1
Márie	Márie	k1gFnSc1
Terézie	Terézie	k1gFnSc2
–	–	k?
článek	článek	k1gInSc1
na	na	k7c4
slachta	slachto	k1gNnPc4
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
1243389-5	1243389-5	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
127629568	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Rakousko	Rakousko	k1gNnSc1
|	|	kIx~
Válka	válka	k1gFnSc1
</s>
