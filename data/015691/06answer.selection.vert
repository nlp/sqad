<s>
Vojenský	vojenský	k2eAgInSc1d1
řád	řád	k1gInSc1
Marie	Maria	k1gFnSc2
Terezie	Terezie	k1gFnSc2
(	(	kIx(
<g/>
německy	německy	k6eAd1
Militär-Maria-Theresien-Orden	Militär-Maria-Theresien-Ordna	k1gFnPc2
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
nejvyšší	vysoký	k2eAgInSc1d3
rakouský	rakouský	k2eAgInSc1d1
vojenský	vojenský	k2eAgInSc1d1
řád	řád	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1757	#num#	k4
a	a	k8xC
udělen	udělen	k2eAgMnSc1d1
poprvé	poprvé	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1758	#num#	k4
<g/>
,	,	kIx,
nejprve	nejprve	k6eAd1
v	v	k7c6
habsburské	habsburský	k2eAgFnSc6d1
monarchii	monarchie	k1gFnSc6
<g/>
,	,	kIx,
po	po	k7c6
rozpadu	rozpad	k1gInSc6
Svaté	svatý	k2eAgFnSc2d1
říše	říš	k1gFnSc2
římské	římský	k2eAgFnSc2d1
národa	národ	k1gInSc2
německého	německý	k2eAgInSc2d1
v	v	k7c6
Rakouském	rakouský	k2eAgNnSc6d1
císařství	císařství	k1gNnSc6
a	a	k8xC
následně	následně	k6eAd1
v	v	k7c6
Rakousku-Uhersku	Rakousku-Uhersek	k1gInSc6
<g/>
.	.	kIx.
</s>