<p>
<s>
Ametyst	ametyst	k1gInSc1	ametyst
je	být	k5eAaImIp3nS	být
minerál	minerál	k1gInSc1	minerál
<g/>
,	,	kIx,	,
odrůda	odrůda	k1gFnSc1	odrůda
křemene	křemen	k1gInSc2	křemen
různých	různý	k2eAgInPc2d1	různý
odstínů	odstín	k1gInPc2	odstín
fialové	fialový	k2eAgFnSc2d1	fialová
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Ametyst	ametyst	k1gInSc1	ametyst
je	být	k5eAaImIp3nS	být
počítán	počítat	k5eAaImNgInS	počítat
mezi	mezi	k7c4	mezi
drahé	drahý	k2eAgInPc4d1	drahý
kameny	kámen	k1gInPc4	kámen
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
materiálem	materiál	k1gInSc7	materiál
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
různých	různý	k2eAgFnPc2d1	různá
ozdob	ozdoba	k1gFnPc2	ozdoba
a	a	k8xC	a
šperků	šperk	k1gInPc2	šperk
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
především	především	k9	především
v	v	k7c6	v
dutinách	dutina	k1gFnPc6	dutina
zásaditých	zásaditý	k2eAgFnPc2d1	zásaditá
hornin	hornina	k1gFnPc2	hornina
(	(	kIx(	(
<g/>
geodách	geoda	k1gFnPc6	geoda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ametyst	ametyst	k1gInSc1	ametyst
má	mít	k5eAaImIp3nS	mít
barvu	barva	k1gFnSc4	barva
od	od	k7c2	od
jemně	jemně	k6eAd1	jemně
fialové	fialový	k2eAgFnSc2d1	fialová
po	po	k7c4	po
purpurovou	purpurový	k2eAgFnSc4d1	purpurová
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
ametysty	ametyst	k1gInPc1	ametyst
jsou	být	k5eAaImIp3nP	být
průhledné	průhledný	k2eAgFnPc4d1	průhledná
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc4d1	jiná
jen	jen	k6eAd1	jen
průsvitné	průsvitný	k2eAgFnPc4d1	průsvitná
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc4d1	další
neprůsvitné	průsvitný	k2eNgFnPc4d1	neprůsvitná
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
opakní	opakní	k2eAgFnPc4d1	opakní
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgNnSc3	svůj
zbarvení	zbarvení	k1gNnSc3	zbarvení
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Termín	termín	k1gInSc1	termín
==	==	k?	==
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
snad	snad	k9	snad
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
slova	slovo	k1gNnSc2	slovo
amethystos	amethystos	k1gInSc1	amethystos
=	=	kIx~	=
neopojivý	opojivý	k2eNgMnSc1d1	opojivý
<g/>
.	.	kIx.	.
</s>
<s>
Indové	Ind	k1gMnPc1	Ind
nazývají	nazývat	k5eAaImIp3nP	nazývat
ametyst	ametyst	k1gInSc4	ametyst
Katajílá	Katajílý	k2eAgFnSc1d1	Katajílý
podle	podle	k7c2	podle
barvy	barva	k1gFnSc2	barva
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
připomíná	připomínat	k5eAaImIp3nS	připomínat
barvu	barva	k1gFnSc4	barva
ametystu	ametyst	k1gInSc2	ametyst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Světová	světový	k2eAgNnPc1d1	světové
naleziště	naleziště	k1gNnPc1	naleziště
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
šperk	šperk	k1gInSc4	šperk
měl	mít	k5eAaImAgInS	mít
ametyst	ametyst	k1gInSc1	ametyst
význam	význam	k1gInSc4	význam
už	už	k9	už
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Korále	korál	k1gMnPc1	korál
z	z	k7c2	z
ametystu	ametyst	k1gInSc2	ametyst
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
ze	z	k7c2	z
starověkých	starověký	k2eAgFnPc2d1	starověká
kultur	kultura	k1gFnPc2	kultura
civilizace	civilizace	k1gFnSc1	civilizace
Mohendžo-dara	Mohendžoara	k1gFnSc1	Mohendžo-dara
<g/>
,	,	kIx,	,
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
Sumeru	Sumer	k1gInSc2	Sumer
a	a	k8xC	a
Baktrie	Baktrie	k1gFnSc2	Baktrie
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
a	a	k8xC	a
Tádžikistánu	Tádžikistán	k1gInSc6	Tádžikistán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Purpurové	purpurový	k2eAgInPc1d1	purpurový
ametysty	ametyst	k1gInPc1	ametyst
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
ze	z	k7c2	z
Srí	Srí	k1gFnSc2	Srí
Lanky	lanko	k1gNnPc7	lanko
a	a	k8xC	a
z	z	k7c2	z
Bornea	Borneo	k1gNnSc2	Borneo
<g/>
.	.	kIx.	.
</s>
<s>
Ametyst	ametyst	k1gInSc1	ametyst
se	se	k3xPyFc4	se
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
exportuje	exportovat	k5eAaBmIp3nS	exportovat
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
Brazílie	Brazílie	k1gFnSc2	Brazílie
a	a	k8xC	a
Uruguaye	Uruguay	k1gFnSc2	Uruguay
<g/>
.	.	kIx.	.
</s>
<s>
Nádherné	nádherný	k2eAgInPc1d1	nádherný
ametysty	ametyst	k1gInPc1	ametyst
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
afrických	africký	k2eAgFnPc2d1	africká
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
Zambie	Zambie	k1gFnSc1	Zambie
<g/>
,	,	kIx,	,
Kongo	Kongo	k1gNnSc1	Kongo
<g/>
,	,	kIx,	,
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Cejlonu	Cejlon	k1gInSc2	Cejlon
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
České	český	k2eAgInPc1d1	český
a	a	k8xC	a
slovenské	slovenský	k2eAgInPc1d1	slovenský
ametysty	ametyst	k1gInPc1	ametyst
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
nejproslulejší	proslulý	k2eAgFnSc1d3	nejproslulejší
výzdoba	výzdoba	k1gFnSc1	výzdoba
fialovými	fialový	k2eAgFnPc7d1	fialová
ametysty	ametyst	k1gInPc1	ametyst
s	s	k7c7	s
bílými	bílý	k2eAgFnPc7d1	bílá
vrostlicemi	vrostlice	k1gFnPc7	vrostlice
achátu	achát	k1gInSc2	achát
z	z	k7c2	z
lokality	lokalita	k1gFnSc2	lokalita
Ciboušov	Ciboušov	k1gInSc1	Ciboušov
na	na	k7c4	na
Klášterecku	Klášterecka	k1gFnSc4	Klášterecka
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgFnPc2	který
se	se	k3xPyFc4	se
řezaly	řezat	k5eAaImAgFnP	řezat
a	a	k8xC	a
leštily	leštit	k5eAaImAgFnP	leštit
čtyř-	čtyř-	k?	čtyř-
až	až	k6eAd1	až
pětiboké	pětiboký	k2eAgFnPc1d1	pětiboká
desky	deska	k1gFnPc1	deska
<g/>
;	;	kIx,	;
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
se	se	k3xPyFc4	se
v	v	k7c6	v
obkladech	obklad	k1gInPc6	obklad
kaplí	kaple	k1gFnPc2	kaple
sv.	sv.	kA	sv.
Kateřiny	Kateřina	k1gFnSc2	Kateřina
a	a	k8xC	a
Svatého	svatý	k2eAgInSc2d1	svatý
Kříže	kříž	k1gInSc2	kříž
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Karlštejně	Karlštejn	k1gInSc6	Karlštejn
a	a	k8xC	a
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Písemně	písemně	k6eAd1	písemně
doložené	doložená	k1gFnPc1	doložená
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
Karlova	Karlův	k2eAgInSc2d1	Karlův
hradu	hrad	k1gInSc2	hrad
v	v	k7c6	v
Tangermünde	Tangermünd	k1gMnSc5	Tangermünd
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedochovaly	dochovat	k5eNaPmAgFnP	dochovat
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
chtěl	chtít	k5eAaImAgMnS	chtít
pomocí	pomocí	k7c2	pomocí
výzdoby	výzdoba	k1gFnSc2	výzdoba
polodrahokamy	polodrahokam	k1gInPc1	polodrahokam
v	v	k7c6	v
kaplích	kaple	k1gFnPc6	kaple
evokovat	evokovat	k5eAaBmF	evokovat
Nebeský	nebeský	k2eAgInSc4d1	nebeský
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
hradby	hradba	k1gFnPc1	hradba
podle	podle	k7c2	podle
biblického	biblický	k2eAgInSc2d1	biblický
textu	text	k1gInSc2	text
Vidění	vidění	k1gNnSc4	vidění
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
teorie	teorie	k1gFnSc1	teorie
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
meditačním	meditační	k2eAgInSc6d1	meditační
účinku	účinek	k1gInSc6	účinek
ametystu	ametyst	k1gInSc2	ametyst
na	na	k7c4	na
mysl	mysl	k1gFnSc4	mysl
císaře	císař	k1gMnSc2	císař
za	za	k7c2	za
jeho	jeho	k3xOp3gInSc2	jeho
pobytu	pobyt	k1gInSc2	pobyt
na	na	k7c6	na
Karlštejně	Karlštejn	k1gInSc6	Karlštejn
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
ametysty	ametyst	k1gInPc1	ametyst
dal	dát	k5eAaPmAgMnS	dát
císař	císař	k1gMnSc1	císař
vložit	vložit	k5eAaPmF	vložit
do	do	k7c2	do
Svatováclavské	svatováclavský	k2eAgFnSc2d1	Svatováclavská
koruny	koruna	k1gFnSc2	koruna
<g/>
,	,	kIx,	,
do	do	k7c2	do
zlatého	zlatý	k2eAgInSc2d1	zlatý
korunovačního	korunovační	k2eAgInSc2d1	korunovační
kříže	kříž	k1gInSc2	kříž
a	a	k8xC	a
do	do	k7c2	do
několika	několik	k4yIc2	několik
relikviářů	relikviář	k1gInPc2	relikviář
ve	v	k7c6	v
Svatovítském	svatovítský	k2eAgInSc6d1	svatovítský
pokladu	poklad	k1gInSc6	poklad
<g/>
.	.	kIx.	.
</s>
<s>
Ametyst	ametyst	k1gInSc1	ametyst
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
bývalého	bývalý	k2eAgNnSc2d1	bývalé
Československa	Československo	k1gNnSc2	Československo
nalézá	nalézat	k5eAaImIp3nS	nalézat
v	v	k7c6	v
Krušných	krušný	k2eAgFnPc6d1	krušná
horách	hora	k1gFnPc6	hora
<g/>
,	,	kIx,	,
okolo	okolo	k6eAd1	okolo
Kozákova	Kozákův	k2eAgFnSc1d1	Kozákova
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
ráji	ráj	k1gInSc6	ráj
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
na	na	k7c6	na
Chebsku	Chebsko	k1gNnSc6	Chebsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
Jeseníkách	Jeseníky	k1gInPc6	Jeseníky
<g/>
,	,	kIx,	,
v	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
krasu	kras	k1gInSc6	kras
<g/>
,	,	kIx,	,
na	na	k7c6	na
Vysočině	vysočina	k1gFnSc6	vysočina
na	na	k7c6	na
Žďársku	Žďársko	k1gNnSc6	Žďársko
a	a	k8xC	a
Třebíčsku	Třebíčsko	k1gNnSc6	Třebíčsko
(	(	kIx(	(
<g/>
Bochovice	Bochovice	k1gFnSc1	Bochovice
<g/>
,	,	kIx,	,
Hostákov	Hostákov	k1gInSc1	Hostákov
<g/>
,	,	kIx,	,
Valdíkov	Valdíkov	k1gInSc1	Valdíkov
<g/>
,	,	kIx,	,
Smrk	smrk	k1gInSc1	smrk
<g/>
,	,	kIx,	,
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proslulé	proslulý	k2eAgFnPc1d1	proslulá
byly	být	k5eAaImAgFnP	být
žezlové	žezlový	k2eAgInPc4d1	žezlový
krystaly	krystal	k1gInPc4	krystal
ametystu	ametyst	k1gInSc2	ametyst
z	z	k7c2	z
dolů	dol	k1gInPc2	dol
okolo	okolo	k7c2	okolo
Banské	banský	k2eAgFnSc2d1	Banská
Štiavnice	Štiavnica	k1gFnSc2	Štiavnica
ve	v	k7c6	v
středním	střední	k2eAgNnSc6d1	střední
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
a	a	k8xC	a
využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
historie	historie	k1gFnSc2	historie
se	se	k3xPyFc4	se
ametyst	ametyst	k1gInSc1	ametyst
využíval	využívat	k5eAaPmAgInS	využívat
v	v	k7c6	v
klenotnictví	klenotnictví	k1gNnSc6	klenotnictví
<g/>
,	,	kIx,	,
řezaly	řezat	k5eAaImAgInP	řezat
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
sumerské	sumerský	k2eAgFnPc1d1	sumerská
a	a	k8xC	a
mezopotámské	mezopotámský	k2eAgFnPc1d1	mezopotámská
pečetní	pečetní	k2eAgFnPc1d1	pečetní
válečky	válečka	k1gFnPc1	válečka
<g/>
,	,	kIx,	,
a	a	k8xC	a
především	především	k9	především
antické	antický	k2eAgFnSc2d1	antická
(	(	kIx(	(
<g/>
řecké	řecký	k2eAgFnSc2d1	řecká
i	i	k8xC	i
římské	římský	k2eAgFnSc2d1	římská
<g/>
)	)	kIx)	)
gemy	gema	k1gFnSc2	gema
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
evropském	evropský	k2eAgNnSc6d1	Evropské
klenotnictví	klenotnictví	k1gNnSc6	klenotnictví
a	a	k8xC	a
v	v	k7c6	v
glyptice	glyptika	k1gFnSc6	glyptika
se	se	k3xPyFc4	se
ametyst	ametyst	k1gInSc1	ametyst
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
až	až	k6eAd1	až
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
hojně	hojně	k6eAd1	hojně
využívá	využívat	k5eAaPmIp3nS	využívat
<g/>
,	,	kIx,	,
proslulé	proslulý	k2eAgFnPc1d1	proslulá
jsou	být	k5eAaImIp3nP	být
misky	miska	k1gFnPc1	miska
a	a	k8xC	a
konvice	konvice	k1gFnSc1	konvice
dvorských	dvorský	k2eAgMnPc2d1	dvorský
umělců	umělec	k1gMnPc2	umělec
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
či	či	k8xC	či
nádobí	nádobí	k1gNnSc1	nádobí
z	z	k7c2	z
dílen	dílna	k1gFnPc2	dílna
brusičů	brusič	k1gMnPc2	brusič
rodiny	rodina	k1gFnSc2	rodina
Miseroniů	Miseroni	k1gMnPc2	Miseroni
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
pracovali	pracovat	k5eAaImAgMnP	pracovat
v	v	k7c6	v
Praze-Bubenči	Praze-Bubenči	k1gFnSc6	Praze-Bubenči
pro	pro	k7c4	pro
císaře	císař	k1gMnSc4	císař
Rudolfa	Rudolf	k1gMnSc2	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
dvůr	dvůr	k1gInSc1	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
dováží	dovázat	k5eAaPmIp3nP	dovázat
zejména	zejména	k9	zejména
brazilské	brazilský	k2eAgInPc4d1	brazilský
ametysty	ametyst	k1gInPc4	ametyst
a	a	k8xC	a
užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
také	také	k9	také
barvený	barvený	k2eAgMnSc1d1	barvený
na	na	k7c4	na
černo	černo	k1gNnSc4	černo
či	či	k8xC	či
v	v	k7c6	v
kobaltové	kobaltový	k2eAgFnSc6d1	kobaltová
modři	modř	k1gFnSc6	modř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
se	se	k3xPyFc4	se
ametyst	ametyst	k1gInSc1	ametyst
používal	používat	k5eAaImAgInS	používat
jako	jako	k9	jako
talisman	talisman	k1gInSc1	talisman
proti	proti	k7c3	proti
následkům	následek	k1gInPc3	následek
nadměrného	nadměrný	k2eAgNnSc2d1	nadměrné
pití	pití	k1gNnSc2	pití
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
kámen	kámen	k1gInSc1	kámen
měl	mít	k5eAaImAgInS	mít
probouzet	probouzet	k5eAaImF	probouzet
bdělost	bdělost	k1gFnSc4	bdělost
vyšší	vysoký	k2eAgFnSc2d2	vyšší
mysli	mysl	k1gFnSc2	mysl
<g/>
,	,	kIx,	,
zasvěcovat	zasvěcovat	k5eAaImF	zasvěcovat
do	do	k7c2	do
tajných	tajný	k2eAgFnPc2d1	tajná
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
zesilovat	zesilovat	k5eAaImF	zesilovat
vyšší	vysoký	k2eAgFnPc4d2	vyšší
duševní	duševní	k2eAgFnPc4d1	duševní
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
(	(	kIx(	(
<g/>
arci	arci	k0	arci
<g/>
)	)	kIx)	)
<g/>
biskupských	biskupský	k2eAgInPc6d1	biskupský
prstenech	prsten	k1gInPc6	prsten
pak	pak	k6eAd1	pak
manifestuje	manifestovat	k5eAaBmIp3nS	manifestovat
duševní	duševní	k2eAgFnSc4d1	duševní
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
mravní	mravní	k2eAgFnSc4d1	mravní
čistotu	čistota	k1gFnSc4	čistota
<g/>
.	.	kIx.	.
</s>
<s>
Ametyst	ametyst	k1gInSc1	ametyst
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
používán	používán	k2eAgInSc1d1	používán
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
údajné	údajný	k2eAgInPc4d1	údajný
léčebné	léčebný	k2eAgInPc4d1	léčebný
účinky	účinek	k1gInPc4	účinek
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
léčitelstvích	léčitelství	k1gNnPc6	léčitelství
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Isidor	Isidor	k1gMnSc1	Isidor
ze	z	k7c2	z
Sevilly	Sevilla	k1gFnSc2	Sevilla
<g/>
,	,	kIx,	,
Etymologiae	Etymologiae	k1gNnSc1	Etymologiae
<g/>
,	,	kIx,	,
díl	díl	k1gInSc1	díl
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Hana	Hana	k1gFnSc1	Hana
Šedinová	Šedinová	k1gFnSc1	Šedinová
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
Oikumena	oikumena	k1gFnSc1	oikumena
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
N.	N.	kA	N.
I.	I.	kA	I.
Kornilov	Kornilov	k1gInSc1	Kornilov
<g/>
,	,	kIx,	,
Juvelirnye	Juvelirny	k1gFnPc1	Juvelirny
kamni	kameň	k1gFnSc3	kameň
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
KOUŘIMSKÝ	kouřimský	k2eAgMnSc1d1	kouřimský
<g/>
,	,	kIx,	,
Drahé	drahý	k2eAgInPc1d1	drahý
kameny	kámen	k1gInPc1	kámen
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgNnSc1d1	národní
muzeum	muzeum	k1gNnSc1	muzeum
Praha	Praha	k1gFnSc1	Praha
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Kouřimský	kouřimský	k2eAgMnSc1d1	kouřimský
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Bauer	Bauer	k1gMnSc1	Bauer
<g/>
,	,	kIx,	,
Atlas	Atlas	k1gInSc1	Atlas
drahých	drahý	k2eAgInPc2d1	drahý
kamenů	kámen	k1gInPc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
ĎUĎA	ĎUĎA	kA	ĎUĎA
<g/>
,	,	kIx,	,
Luboš	Luboš	k1gMnSc1	Luboš
REJL	REJL	kA	REJL
<g/>
:	:	kIx,	:
Drahé	drahý	k2eAgInPc1d1	drahý
kameny	kámen	k1gInPc1	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
Granit	granit	k1gInSc1	granit
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
BOUŠKA	Bouška	k1gMnSc1	Bouška
<g/>
,	,	kIx,	,
Klenoty	klenot	k1gInPc1	klenot
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
R.	R.	kA	R.
L.	L.	kA	L.
BONEWITZ	BONEWITZ	kA	BONEWITZ
<g/>
,	,	kIx,	,
Kameny	kámen	k1gInPc7	kámen
a	a	k8xC	a
drahokamy	drahokam	k1gInPc7	drahokam
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
překlad	překlad	k1gInSc1	překlad
Robert	Roberta	k1gFnPc2	Roberta
<g/>
.	.	kIx.	.
</s>
<s>
A.	A.	kA	A.
Nový	nový	k2eAgInSc1d1	nový
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
Slovart	Slovart	k1gInSc1	Slovart
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Drahé	drahý	k2eAgInPc1d1	drahý
kameny	kámen	k1gInPc1	kámen
starověku	starověk	k1gInSc2	starověk
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Clare	Clar	k1gMnSc5	Clar
PHILLIPS	PHILLIPS	kA	PHILLIPS
<g/>
,	,	kIx,	,
Jewellery	Jeweller	k1gInPc7	Jeweller
from	from	k6eAd1	from
Antiquity	Antiquit	k1gInPc4	Antiquit
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Present	Present	k1gInSc1	Present
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
ametyst	ametyst	k1gInSc1	ametyst
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
ametyst	ametyst	k1gInSc1	ametyst
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
