<p>
<s>
HMS	HMS	kA	HMS
King	King	k1gMnSc1	King
George	Georg	k1gMnSc2	Georg
V	V	kA	V
byla	být	k5eAaImAgFnS	být
britská	britský	k2eAgFnSc1d1	britská
bitevní	bitevní	k2eAgFnSc1d1	bitevní
loď	loď	k1gFnSc1	loď
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
z	z	k7c2	z
pěti	pět	k4xCc2	pět
lodí	loď	k1gFnPc2	loď
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
do	do	k7c2	do
služby	služba	k1gFnSc2	služba
vstupovaly	vstupovat	k5eAaImAgInP	vstupovat
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
King	King	k1gMnSc1	King
George	Georg	k1gFnSc2	Georg
V	V	kA	V
se	s	k7c7	s
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1941	[number]	k4	1941
výrazně	výrazně	k6eAd1	výrazně
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c4	na
potopení	potopení	k1gNnSc4	potopení
německé	německý	k2eAgFnSc2d1	německá
bitevní	bitevní	k2eAgFnSc2d1	bitevní
lodi	loď	k1gFnSc2	loď
Bismarck	Bismarck	k1gMnSc1	Bismarck
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
přečkala	přečkat	k5eAaPmAgFnS	přečkat
válku	válka	k1gFnSc4	válka
a	a	k8xC	a
dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1949	[number]	k4	1949
byla	být	k5eAaImAgFnS	být
převedena	převést	k5eAaPmNgFnS	převést
do	do	k7c2	do
rezervy	rezerva	k1gFnSc2	rezerva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
byla	být	k5eAaImAgFnS	být
prodána	prodat	k5eAaPmNgFnS	prodat
k	k	k7c3	k
sešrotování	sešrotování	k1gNnSc3	sešrotování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
HMS	HMS	kA	HMS
King	King	k1gInSc4	King
George	Georg	k1gInSc2	Georg
V	V	kA	V
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
PEJČOCH	PEJČOCH	kA	PEJČOCH
<g/>
,	,	kIx,	,
Ivo	Ivo	k1gMnSc1	Ivo
<g/>
;	;	kIx,	;
NOVÁK	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
;	;	kIx,	;
HÁJEK	Hájek	k1gMnSc1	Hájek
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g/>
.	.	kIx.	.
</s>
<s>
Válečné	válečný	k2eAgFnPc1d1	válečná
lodě	loď	k1gFnPc1	loď
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Naše	náš	k3xOp1gNnSc1	náš
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
206	[number]	k4	206
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
357	[number]	k4	357
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
374	[number]	k4	374
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
