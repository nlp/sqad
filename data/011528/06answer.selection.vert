<s>
Friedrich	Friedrich	k1gMnSc1	Friedrich
Miescher	Mieschra	k1gFnPc2	Mieschra
objevil	objevit	k5eAaPmAgMnS	objevit
DNA	dno	k1gNnPc4	dno
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
neznal	neznat	k5eAaImAgMnS	neznat
její	její	k3xOp3gFnSc4	její
strukturu	struktura	k1gFnSc4	struktura
a	a	k8xC	a
přesnou	přesný	k2eAgFnSc4d1	přesná
roli	role	k1gFnSc4	role
v	v	k7c6	v
dědičnosti	dědičnost	k1gFnSc6	dědičnost
<g/>
.	.	kIx.	.
</s>
