<s>
Brno	Brno	k1gNnSc1	Brno
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
řek	řeka	k1gFnPc2	řeka
Svratky	Svratka	k1gFnSc2	Svratka
a	a	k8xC	a
Svitavy	Svitava	k1gFnSc2	Svitava
a	a	k8xC	a
protékají	protékat	k5eAaImIp3nP	protékat
jím	on	k3xPp3gMnSc7	on
potoky	potok	k1gInPc4	potok
Veverka	veverek	k1gMnSc4	veverek
<g/>
,	,	kIx,	,
Ponávka	Ponávka	k1gFnSc1	Ponávka
<g/>
,	,	kIx,	,
Říčka	říčka	k1gFnSc1	říčka
a	a	k8xC	a
několik	několik	k4yIc1	několik
dalších	další	k2eAgInPc2d1	další
menších	malý	k2eAgInPc2d2	menší
toků	tok	k1gInPc2	tok
<g/>
.	.	kIx.	.
</s>
