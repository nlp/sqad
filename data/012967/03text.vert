<p>
<s>
Zámek	zámek	k1gInSc1	zámek
Čečovice	Čečovice	k1gFnSc2	Čečovice
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Čečovice	Čečovice	k1gFnSc2	Čečovice
<g/>
,	,	kIx,	,
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Domažlice	Domažlice	k1gFnPc1	Domažlice
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
jako	jako	k8xS	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
zámku	zámek	k1gInSc2	zámek
vede	vést	k5eAaImIp3nS	vést
cyklotrasa	cyklotrasa	k1gFnSc1	cyklotrasa
2223	[number]	k4	2223
na	na	k7c4	na
Černovice	Černovice	k1gFnPc4	Černovice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Tvrz	tvrz	k1gFnSc4	tvrz
===	===	k?	===
</s>
</p>
<p>
<s>
Tvrz	tvrz	k1gFnSc1	tvrz
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zámku	zámek	k1gInSc6	zámek
předcházela	předcházet	k5eAaImAgNnP	předcházet
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
patrně	patrně	k6eAd1	patrně
již	již	k6eAd1	již
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
vystavět	vystavět	k5eAaPmF	vystavět
některý	některý	k3yIgInSc4	některý
z	z	k7c2	z
jménem	jméno	k1gNnSc7	jméno
neznámých	známý	k2eNgMnPc2d1	neznámý
potomků	potomek	k1gMnPc2	potomek
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
z	z	k7c2	z
Bukovce	bukovka	k1gFnSc6	bukovka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1355	[number]	k4	1355
<g/>
–	–	k?	–
<g/>
1358	[number]	k4	1358
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
majitel	majitel	k1gMnSc1	majitel
Čečovic	Čečovice	k1gFnPc2	Čečovice
<g/>
,	,	kIx,	,
Bukovce	bukovka	k1gFnSc6	bukovka
a	a	k8xC	a
tvrze	tvrz	k1gFnSc2	tvrz
uváděn	uváděn	k2eAgMnSc1d1	uváděn
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
z	z	k7c2	z
Čečovic	Čečovice	k1gFnPc2	Čečovice
<g/>
;	;	kIx,	;
ze	z	k7c2	z
stejné	stejný	k2eAgFnSc2d1	stejná
doby	doba	k1gFnSc2	doba
pochází	pocházet	k5eAaImIp3nS	pocházet
také	také	k9	také
první	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
tvrzi	tvrz	k1gFnSc6	tvrz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1364	[number]	k4	1364
obce	obec	k1gFnSc2	obec
i	i	k8xC	i
s	s	k7c7	s
tvrzí	tvrz	k1gFnSc7	tvrz
od	od	k7c2	od
něho	on	k3xPp3gInSc2	on
odkupují	odkupovat	k5eAaImIp3nP	odkupovat
bratři	bratr	k1gMnPc1	bratr
Jan	Jan	k1gMnSc1	Jan
Bušek	Bušek	k1gMnSc1	Bušek
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
z	z	k7c2	z
Velhartic	Velhartice	k1gFnPc2	Velhartice
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
je	být	k5eAaImIp3nS	být
jako	jako	k8xS	jako
majitel	majitel	k1gMnSc1	majitel
uváděn	uvádět	k5eAaImNgMnS	uvádět
už	už	k9	už
jen	jen	k9	jen
Jan	Jan	k1gMnSc1	Jan
Bušek	Bušek	k1gMnSc1	Bušek
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zboží	zboží	k1gNnSc4	zboží
spravoval	spravovat	k5eAaImAgMnS	spravovat
z	z	k7c2	z
hradu	hrad	k1gInSc2	hrad
Velhartice	Velhartice	k1gFnPc1	Velhartice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1395	[number]	k4	1395
získala	získat	k5eAaPmAgFnS	získat
Čečovice	Čečovice	k1gFnSc1	Čečovice
Anna	Anna	k1gFnSc1	Anna
z	z	k7c2	z
Velhartic	Velhartice	k1gFnPc2	Velhartice
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
svůj	svůj	k3xOyFgInSc4	svůj
podíl	podíl	k1gInSc4	podíl
zde	zde	k6eAd1	zde
měl	mít	k5eAaImAgInS	mít
i	i	k8xC	i
její	její	k3xOp3gMnSc1	její
druhý	druhý	k4xOgMnSc1	druhý
manžel	manžel	k1gMnSc1	manžel
Václav	Václav	k1gMnSc1	Václav
z	z	k7c2	z
Vartemberka	Vartemberka	k1gFnSc1	Vartemberka
(	(	kIx(	(
<g/>
uváděn	uváděn	k2eAgInSc1d1	uváděn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1401	[number]	k4	1401
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1409	[number]	k4	1409
ji	on	k3xPp3gFnSc4	on
pak	pak	k9	pak
Anna	Anna	k1gFnSc1	Anna
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
již	již	k6eAd1	již
provdána	provdán	k2eAgFnSc1d1	provdána
za	za	k7c4	za
Hynka	Hynek	k1gMnSc4	Hynek
staršího	starý	k2eAgMnSc4d2	starší
z	z	k7c2	z
Náchoda	Náchod	k1gInSc2	Náchod
<g/>
,	,	kIx,	,
postoupila	postoupit	k5eAaPmAgFnS	postoupit
Jindřichovi	Jindřichův	k2eAgMnPc1d1	Jindřichův
III	III	kA	III
<g/>
.	.	kIx.	.
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
statek	statek	k1gInSc1	statek
spravoval	spravovat	k5eAaImAgInS	spravovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
purkrabích	purkrabí	k1gMnPc2	purkrabí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1390	[number]	k4	1390
zde	zde	k6eAd1	zde
Jindřich	Jindřich	k1gMnSc1	Jindřich
nechal	nechat	k5eAaPmAgMnS	nechat
popravit	popravit	k5eAaPmF	popravit
Přibíka	Přibík	k1gMnSc4	Přibík
z	z	k7c2	z
Janovic	Janovice	k1gFnPc2	Janovice
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
němu	on	k3xPp3gMnSc3	on
vzbouřil	vzbouřit	k5eAaPmAgMnS	vzbouřit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
boje	boj	k1gInPc4	boj
s	s	k7c7	s
Rýzmburskými	Rýzmburský	k2eAgFnPc7d1	Rýzmburský
z	z	k7c2	z
Janovic	Janovice	k1gFnPc2	Janovice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
trvaly	trvat	k5eAaImAgFnP	trvat
až	až	k9	až
do	do	k7c2	do
Jindřichovy	Jindřichův	k2eAgFnSc2d1	Jindřichova
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1412	[number]	k4	1412
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
statek	statek	k1gInSc1	statek
odkoupil	odkoupit	k5eAaPmAgMnS	odkoupit
Dobeš	Dobeš	k1gMnSc1	Dobeš
Snopoušovský	Snopoušovský	k1gMnSc1	Snopoušovský
z	z	k7c2	z
Čečovic	Čečovice	k1gFnPc2	Čečovice
a	a	k8xC	a
následně	následně	k6eAd1	následně
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
rychlému	rychlý	k2eAgNnSc3d1	rychlé
střídání	střídání	k1gNnSc3	střídání
majitelů	majitel	k1gMnPc2	majitel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1546	[number]	k4	1546
ji	on	k3xPp3gFnSc4	on
odkoupil	odkoupit	k5eAaPmAgInS	odkoupit
Jan	Jan	k1gMnSc1	Jan
mladší	mladý	k2eAgInSc1d2	mladší
Popel	popel	k1gInSc1	popel
z	z	k7c2	z
Lobkovic	Lobkovice	k1gInPc2	Lobkovice
a	a	k8xC	a
následně	následně	k6eAd1	následně
připojil	připojit	k5eAaPmAgInS	připojit
k	k	k7c3	k
horšovskotýnskému	horšovskotýnský	k2eAgNnSc3d1	horšovskotýnské
panství	panství	k1gNnSc3	panství
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
ztratila	ztratit	k5eAaPmAgFnS	ztratit
svoji	svůj	k3xOyFgFnSc4	svůj
sídelní	sídelní	k2eAgFnSc4d1	sídelní
funkci	funkce	k1gFnSc4	funkce
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
zpustla	zpustnout	k5eAaPmAgFnS	zpustnout
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
roku	rok	k1gInSc2	rok
1587	[number]	k4	1587
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
dochoval	dochovat	k5eAaPmAgInS	dochovat
popis	popis	k1gInSc4	popis
komplexu	komplex	k1gInSc2	komplex
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
kromě	kromě	k7c2	kromě
již	již	k6eAd1	již
zmíněné	zmíněný	k2eAgFnSc2d1	zmíněná
tvrze	tvrz	k1gFnSc2	tvrz
také	také	k9	také
sladovnu	sladovna	k1gFnSc4	sladovna
<g/>
,	,	kIx,	,
pivovar	pivovar	k1gInSc1	pivovar
<g/>
,	,	kIx,	,
dvůr	dvůr	k1gInSc1	dvůr
s	s	k7c7	s
ovčín	ovčín	k1gInSc4	ovčín
a	a	k8xC	a
nacházela	nacházet	k5eAaImAgFnS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
i	i	k9	i
chmelnice	chmelnice	k1gFnSc1	chmelnice
a	a	k8xC	a
štěpnice	štěpnice	k1gFnSc1	štěpnice
a	a	k8xC	a
pod	pod	k7c7	pod
tvrzí	tvrz	k1gFnSc7	tvrz
rybník	rybník	k1gInSc4	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1623	[number]	k4	1623
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
stále	stále	k6eAd1	stále
za	za	k7c2	za
Popelů	popel	k1gInPc2	popel
z	z	k7c2	z
Lobkovic	Lobkovice	k1gInPc2	Lobkovice
<g/>
,	,	kIx,	,
prošel	projít	k5eAaPmAgInS	projít
přestavbou	přestavba	k1gFnSc7	přestavba
na	na	k7c4	na
manýristický	manýristický	k2eAgInSc4d1	manýristický
zámek	zámek	k1gInSc4	zámek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zámek	zámek	k1gInSc1	zámek
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1623	[number]	k4	1623
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
horšovskotýnské	horšovskotýnský	k2eAgNnSc1d1	horšovskotýnské
panství	panství	k1gNnSc1	panství
Popelům	Popela	k1gMnPc3	Popela
zkonfiskováno	zkonfiskovat	k5eAaPmNgNnS	zkonfiskovat
a	a	k8xC	a
zakoupil	zakoupit	k5eAaPmAgMnS	zakoupit
jej	on	k3xPp3gMnSc4	on
hrabě	hrabě	k1gMnSc1	hrabě
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
Trauttmansdorff	Trauttmansdorff	k1gMnSc1	Trauttmansdorff
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Trauttmansdorffů	Trauttmansdorff	k1gInPc2	Trauttmansdorff
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1715	[number]	k4	1715
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
výstavbě	výstavba	k1gFnSc3	výstavba
barokního	barokní	k2eAgNnSc2d1	barokní
východního	východní	k2eAgNnSc2d1	východní
křídla	křídlo	k1gNnSc2	křídlo
a	a	k8xC	a
dalším	další	k2eAgFnPc3d1	další
úpravám	úprava	k1gFnPc3	úprava
zámku	zámek	k1gInSc2	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přestavbě	přestavba	k1gFnSc3	přestavba
gotického	gotický	k2eAgInSc2d1	gotický
paláce	palác	k1gInSc2	palác
původní	původní	k2eAgFnSc2d1	původní
tvrze	tvrz	k1gFnSc2	tvrz
na	na	k7c4	na
sýpku	sýpka	k1gFnSc4	sýpka
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgNnSc4d1	dnešní
západní	západní	k2eAgNnSc4d1	západní
křídlo	křídlo	k1gNnSc4	křídlo
zámku	zámek	k1gInSc2	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
sloužil	sloužit	k5eAaImAgInS	sloužit
zámek	zámek	k1gInSc1	zámek
jako	jako	k8xC	jako
zázemí	zázemí	k1gNnSc1	zázemí
pro	pro	k7c4	pro
knížete	kníže	k1gNnSc4wR	kníže
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc4	jeho
hosty	host	k1gMnPc4	host
při	při	k7c6	při
návštěvách	návštěva	k1gFnPc6	návštěva
zdejší	zdejší	k2eAgFnSc2d1	zdejší
obory	obora	k1gFnSc2	obora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
v	v	k7c6	v
nezměněné	změněný	k2eNgFnSc6d1	nezměněná
podobě	podoba	k1gFnSc6	podoba
zůstal	zůstat	k5eAaPmAgMnS	zůstat
až	až	k9	až
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
pozemkové	pozemkový	k2eAgFnSc6d1	pozemková
reformě	reforma	k1gFnSc6	reforma
jako	jako	k9	jako
zbytkový	zbytkový	k2eAgInSc1d1	zbytkový
statek	statek	k1gInSc1	statek
prodán	prodat	k5eAaPmNgInS	prodat
a	a	k8xC	a
jeho	jeho	k3xOp3gNnPc2	jeho
přízemí	přízemí	k1gNnPc2	přízemí
upraveno	upravit	k5eAaPmNgNnS	upravit
na	na	k7c4	na
hospodářské	hospodářský	k2eAgInPc4d1	hospodářský
prostory	prostor	k1gInPc4	prostor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
byl	být	k5eAaImAgInS	být
státním	státní	k2eAgInSc7d1	státní
statkem	statek	k1gInSc7	statek
upraven	upravit	k5eAaPmNgInS	upravit
na	na	k7c4	na
ubytovnu	ubytovna	k1gFnSc4	ubytovna
zemědělských	zemědělský	k2eAgMnPc2d1	zemědělský
brigádníků	brigádník	k1gMnPc2	brigádník
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgInSc1d1	státní
statek	statek	k1gInSc1	statek
o	o	k7c4	o
zámek	zámek	k1gInSc4	zámek
však	však	k9	však
brzy	brzy	k6eAd1	brzy
ztratil	ztratit	k5eAaPmAgMnS	ztratit
zájem	zájem	k1gInSc4	zájem
a	a	k8xC	a
po	po	k7c6	po
útocích	útok	k1gInPc6	útok
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
přízemní	přízemnit	k5eAaPmIp3nP	přízemnit
prostory	prostora	k1gFnPc4	prostora
upravili	upravit	k5eAaPmAgMnP	upravit
na	na	k7c4	na
garáže	garáž	k1gFnPc4	garáž
<g/>
,	,	kIx,	,
značně	značně	k6eAd1	značně
zchátral	zchátrat	k5eAaPmAgInS	zchátrat
<g/>
.	.	kIx.	.
</s>
<s>
Situaci	situace	k1gFnSc4	situace
se	se	k3xPyFc4	se
nezlepšila	zlepšit	k5eNaPmAgFnS	zlepšit
ani	ani	k9	ani
za	za	k7c2	za
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
JZD	JZD	kA	JZD
Puclice	Puclice	k1gFnSc2	Puclice
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
objekt	objekt	k1gInSc1	objekt
vyhořel	vyhořet	k5eAaPmAgInS	vyhořet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
se	se	k3xPyFc4	se
zámek	zámek	k1gInSc1	zámek
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
majetku	majetek	k1gInSc2	majetek
Českého	český	k2eAgInSc2d1	český
svazu	svaz	k1gInSc2	svaz
ochránců	ochránce	k1gMnPc2	ochránce
památek	památka	k1gFnPc2	památka
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
prochází	procházet	k5eAaImIp3nS	procházet
generální	generální	k2eAgFnSc7d1	generální
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
statickému	statický	k2eAgNnSc3d1	statické
zajištění	zajištění	k1gNnSc3	zajištění
budov	budova	k1gFnPc2	budova
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
k	k	k7c3	k
dokončení	dokončení	k1gNnSc3	dokončení
kompletního	kompletní	k2eAgNnSc2d1	kompletní
zastřešení	zastřešení	k1gNnSc2	zastřešení
objektu	objekt	k1gInSc2	objekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
zámků	zámek	k1gInPc2	zámek
v	v	k7c6	v
Plzeňském	plzeňský	k2eAgInSc6d1	plzeňský
kraji	kraj	k1gInSc6	kraj
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
zámek	zámek	k1gInSc1	zámek
Čečovice	Čečovice	k1gFnSc2	Čečovice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Čečovice	Čečovice	k1gFnPc1	Čečovice
<g/>
,	,	kIx,	,
hrady	hrad	k1gInPc1	hrad
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Zámek	zámek	k1gInSc1	zámek
Čečovice	Čečovice	k1gFnSc2	Čečovice
<g/>
,	,	kIx,	,
cecovice	cecovice	k1gFnSc2	cecovice
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
zámku	zámek	k1gInSc2	zámek
Čečovice	Čečovice	k1gFnSc1	Čečovice
<g/>
,	,	kIx,	,
csopa	csopa	k1gFnSc1	csopa
<g/>
.	.	kIx.	.
<g/>
info	info	k6eAd1	info
</s>
</p>
<p>
<s>
Zámek	zámek	k1gInSc1	zámek
v	v	k7c6	v
Čečovicích	Čečovice	k1gFnPc6	Čečovice
spasila	spasit	k5eAaPmAgFnS	spasit
střecha	střecha	k1gFnSc1	střecha
<g/>
,	,	kIx,	,
denik	denik	k1gInSc1	denik
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
