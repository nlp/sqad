<p>
<s>
Mezi	mezi	k7c7	mezi
listopadem	listopad	k1gInSc7	listopad
1944	[number]	k4	1944
a	a	k8xC	a
lednem	leden	k1gInSc7	leden
1945	[number]	k4	1945
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
série	série	k1gFnSc1	série
japonských	japonský	k2eAgInPc2d1	japonský
náletů	nálet	k1gInPc2	nálet
na	na	k7c4	na
Marianské	Marianský	k2eAgInPc4d1	Marianský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
těchto	tento	k3xDgInPc2	tento
náletů	nálet	k1gInPc2	nálet
byly	být	k5eAaImAgFnP	být
základny	základna	k1gFnPc1	základna
amerického	americký	k2eAgNnSc2d1	americké
armádního	armádní	k2eAgNnSc2d1	armádní
letectva	letectvo	k1gNnSc2	letectvo
(	(	kIx(	(
<g/>
USAAF	USAAF	kA	USAAF
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Japonci	Japonec	k1gMnPc1	Japonec
si	se	k3xPyFc3	se
kladli	klást	k5eAaImAgMnP	klást
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
narušit	narušit	k5eAaPmF	narušit
bombardování	bombardování	k1gNnSc4	bombardování
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
prováděly	provádět	k5eAaImAgInP	provádět
bombardéry	bombardér	k1gInPc1	bombardér
startující	startující	k2eAgInPc1d1	startující
právě	právě	k9	právě
ze	z	k7c2	z
základen	základna	k1gFnPc2	základna
na	na	k7c6	na
Marianských	Marianský	k2eAgInPc6d1	Marianský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Japoncům	Japonec	k1gMnPc3	Japonec
se	se	k3xPyFc4	se
během	během	k7c2	během
těchto	tento	k3xDgInPc2	tento
útoků	útok	k1gInPc2	útok
podařilo	podařit	k5eAaPmAgNnS	podařit
zničit	zničit	k5eAaPmF	zničit
11	[number]	k4	11
B-29	B-29	k1gFnPc2	B-29
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
43	[number]	k4	43
poškodit	poškodit	k5eAaPmF	poškodit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bitva	bitva	k1gFnSc1	bitva
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
letecká	letecký	k2eAgFnSc1d1	letecká
ofenzíva	ofenzíva	k1gFnSc1	ofenzíva
proti	proti	k7c3	proti
letištím	letiště	k1gNnPc3	letiště
na	na	k7c6	na
Marianách	Mariana	k1gFnPc6	Mariana
započala	započnout	k5eAaPmAgFnS	započnout
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
listopadu	listopad	k1gInSc2	listopad
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1944	[number]	k4	1944
první	první	k4xOgInSc4	první
B-29	B-29	k1gFnPc2	B-29
startující	startující	k2eAgFnSc1d1	startující
z	z	k7c2	z
Marian	Mariana	k1gFnPc2	Mariana
přeletěl	přeletět	k5eAaPmAgMnS	přeletět
prostor	prostor	k1gInSc4	prostor
nad	nad	k7c7	nad
Tokiem	Tokio	k1gNnSc7	Tokio
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
9	[number]	k4	9
až	až	k9	až
10	[number]	k4	10
japonských	japonský	k2eAgInPc2d1	japonský
bombardérů	bombardér	k1gInPc2	bombardér
Micubiši	Micubiše	k1gFnSc4	Micubiše
G4M	G4M	k1gFnSc2	G4M
udeřilo	udeřit	k5eAaPmAgNnS	udeřit
na	na	k7c4	na
letiště	letiště	k1gNnSc4	letiště
Isley	Islea	k1gMnSc2	Islea
Field	Field	k1gMnSc1	Field
a	a	k8xC	a
Kobler	Kobler	k1gMnSc1	Kobler
Field	Field	k1gMnSc1	Field
na	na	k7c4	na
Saipanu	Saipana	k1gFnSc4	Saipana
<g/>
.	.	kIx.	.
</s>
<s>
Způsobily	způsobit	k5eAaPmAgFnP	způsobit
pouze	pouze	k6eAd1	pouze
malé	malý	k2eAgFnPc4d1	malá
škody	škoda	k1gFnPc4	škoda
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
G4M	G4M	k1gMnSc1	G4M
byl	být	k5eAaImAgMnS	být
sestřelen	sestřelen	k2eAgInSc4d1	sestřelen
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
byly	být	k5eAaImAgFnP	být
ztraceny	ztratit	k5eAaPmNgFnP	ztratit
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
útok	útok	k1gInSc1	útok
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
způsobil	způsobit	k5eAaPmAgInS	způsobit
opět	opět	k6eAd1	opět
malé	malý	k2eAgFnPc4d1	malá
škody	škoda	k1gFnPc4	škoda
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
brzy	brzy	k6eAd1	brzy
ráno	ráno	k6eAd1	ráno
provedly	provést	k5eAaPmAgInP	provést
dva	dva	k4xCgInPc1	dva
nízkoletící	nízkoletící	k2eAgInPc1d1	nízkoletící
G4M	G4M	k1gMnSc1	G4M
další	další	k2eAgInSc4d1	další
nálet	nálet	k1gInSc4	nálet
na	na	k7c4	na
Isley	Islea	k1gFnPc4	Islea
Field	Fielda	k1gFnPc2	Fielda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zničily	zničit	k5eAaPmAgFnP	zničit
jeden	jeden	k4xCgInSc1	jeden
B-29	B-29	k1gMnPc2	B-29
a	a	k8xC	a
poškodily	poškodit	k5eAaPmAgInP	poškodit
11	[number]	k4	11
dalších	další	k2eAgFnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
udeřilo	udeřit	k5eAaPmAgNnS	udeřit
několik	několik	k4yIc1	několik
Micubiši	Micubiš	k1gMnSc3	Micubiš
A6M	A6M	k1gFnSc2	A6M
vyzbrojených	vyzbrojený	k2eAgInPc2d1	vyzbrojený
bombami	bomba	k1gFnPc7	bomba
na	na	k7c4	na
Isley	Islea	k1gFnPc4	Islea
Field	Fielda	k1gFnPc2	Fielda
a	a	k8xC	a
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
zničit	zničit	k5eAaPmF	zničit
3	[number]	k4	3
až	až	k9	až
4	[number]	k4	4
B-29	B-29	k1gFnPc2	B-29
a	a	k8xC	a
poškodit	poškodit	k5eAaPmF	poškodit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dva	dva	k4xCgInPc1	dva
další	další	k2eAgInPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
28	[number]	k4	28
<g/>
.	.	kIx.	.
na	na	k7c4	na
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc6	listopad
proběhl	proběhnout	k5eAaPmAgMnS	proběhnout
další	další	k2eAgInSc4d1	další
nálet	nálet	k1gInSc4	nálet
na	na	k7c4	na
Isley	Islea	k1gFnPc4	Islea
Field	Fielda	k1gFnPc2	Fielda
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
nálety	nálet	k1gInPc1	nálet
proběhly	proběhnout	k5eAaPmAgInP	proběhnout
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
<g/>
,	,	kIx,	,
největší	veliký	k2eAgMnSc1d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
25	[number]	k4	25
<g/>
.	.	kIx.	.
na	na	k7c4	na
26	[number]	k4	26
<g/>
.	.	kIx.	.
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
25	[number]	k4	25
japonských	japonský	k2eAgNnPc2d1	Japonské
letadel	letadlo	k1gNnPc2	letadlo
útočících	útočící	k2eAgNnPc2d1	útočící
z	z	k7c2	z
malých	malý	k2eAgFnPc2d1	malá
i	i	k8xC	i
velkých	velký	k2eAgFnPc2d1	velká
výšek	výška	k1gFnPc2	výška
<g/>
,	,	kIx,	,
kterým	který	k3yRgFnPc3	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
jeden	jeden	k4xCgMnSc1	jeden
B-29	B-29	k1gMnSc1	B-29
zničit	zničit	k5eAaPmF	zničit
a	a	k8xC	a
několik	několik	k4yIc4	několik
dalších	další	k1gNnPc2	další
poškodit	poškodit	k5eAaPmF	poškodit
<g/>
.	.	kIx.	.
</s>
<s>
Nálety	nálet	k1gInPc1	nálet
probíhaly	probíhat	k5eAaImAgInP	probíhat
s	s	k7c7	s
menší	malý	k2eAgFnSc7d2	menší
intenzitou	intenzita	k1gFnSc7	intenzita
až	až	k6eAd1	až
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
února	únor	k1gInSc2	únor
<g/>
.	.	kIx.	.
</s>
<s>
Americkou	americký	k2eAgFnSc7d1	americká
odpovědí	odpověď	k1gFnSc7	odpověď
na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
nálety	nálet	k1gInPc4	nálet
bylo	být	k5eAaImAgNnS	být
zesílení	zesílení	k1gNnSc4	zesílení
protivzdušné	protivzdušný	k2eAgFnSc2d1	protivzdušná
obrany	obrana	k1gFnSc2	obrana
na	na	k7c6	na
Marianách	Mariana	k1gFnPc6	Mariana
a	a	k8xC	a
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
kolem	kolem	k7c2	kolem
nich	on	k3xPp3gInPc2	on
a	a	k8xC	a
bombardování	bombardování	k1gNnSc1	bombardování
japonského	japonský	k2eAgNnSc2d1	Japonské
letiště	letiště	k1gNnSc2	letiště
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Iwo	Iwo	k1gMnSc1	Iwo
Jima	Jima	k1gMnSc1	Jima
<g/>
.	.	kIx.	.
</s>
<s>
Japonci	Japonec	k1gMnPc1	Japonec
proti	proti	k7c3	proti
základnám	základna	k1gFnPc3	základna
amerických	americký	k2eAgMnPc2d1	americký
bombardérů	bombardér	k1gMnPc2	bombardér
plánovali	plánovat	k5eAaImAgMnP	plánovat
také	také	k9	také
speciální	speciální	k2eAgFnPc4d1	speciální
sebevražedné	sebevražedný	k2eAgFnPc4d1	sebevražedná
operace	operace	k1gFnPc4	operace
<g/>
,	,	kIx,	,
k	k	k7c3	k
těm	ten	k3xDgMnPc3	ten
ale	ale	k9	ale
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výsledek	výsledek	k1gInSc1	výsledek
==	==	k?	==
</s>
</p>
<p>
<s>
Japonské	japonský	k2eAgInPc1d1	japonský
nálety	nálet	k1gInPc1	nálet
na	na	k7c4	na
základny	základna	k1gFnPc4	základna
amerických	americký	k2eAgInPc2d1	americký
bombardérů	bombardér	k1gInPc2	bombardér
vážněji	vážně	k6eAd2	vážně
nenarušily	narušit	k5eNaPmAgFnP	narušit
probíhající	probíhající	k2eAgFnSc4d1	probíhající
leteckou	letecký	k2eAgFnSc4d1	letecká
kampaň	kampaň	k1gFnSc4	kampaň
proti	proti	k7c3	proti
Japonsku	Japonsko	k1gNnSc3	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Přinutily	přinutit	k5eAaPmAgFnP	přinutit
však	však	k9	však
Američany	Američan	k1gMnPc4	Američan
umístit	umístit	k5eAaPmF	umístit
na	na	k7c4	na
Mariany	Mariana	k1gFnPc4	Mariana
větší	veliký	k2eAgFnSc2d2	veliký
síly	síla	k1gFnSc2	síla
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
ochrany	ochrana	k1gFnSc2	ochrana
velice	velice	k6eAd1	velice
drahých	drahý	k2eAgFnPc2d1	drahá
B-29	B-29	k1gFnPc2	B-29
a	a	k8xC	a
utvrdily	utvrdit	k5eAaPmAgInP	utvrdit
je	on	k3xPp3gFnPc4	on
v	v	k7c6	v
nezbytnosti	nezbytnost	k1gFnSc6	nezbytnost
obsazení	obsazení	k1gNnSc2	obsazení
ostrova	ostrov	k1gInSc2	ostrov
Iwo	Iwo	k1gMnSc2	Iwo
Jima	Jimus	k1gMnSc2	Jimus
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
později	pozdě	k6eAd2	pozdě
přišlo	přijít	k5eAaPmAgNnS	přijít
velmi	velmi	k6eAd1	velmi
draho	draho	k6eAd1	draho
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Japanese	Japanese	k1gFnSc2	Japanese
air	air	k?	air
attacks	attacks	k1gInSc1	attacks
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Mariana	Mariana	k1gFnSc1	Mariana
Islands	Islandsa	k1gFnPc2	Islandsa
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Japonské	japonský	k2eAgInPc1d1	japonský
nálety	nálet	k1gInPc1	nálet
na	na	k7c4	na
Marianské	Marianský	k2eAgInPc4d1	Marianský
ostrovy	ostrov	k1gInPc4	ostrov
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
