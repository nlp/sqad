<s>
Matematický	matematický	k2eAgMnSc1d1	matematický
klokan	klokan	k1gMnSc1	klokan
je	být	k5eAaImIp3nS	být
matematická	matematický	k2eAgFnSc1d1	matematická
soutěž	soutěž	k1gFnSc1	soutěž
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
organizuje	organizovat	k5eAaBmIp3nS	organizovat
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
třiceti	třicet	k4xCc6	třicet
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
včetně	včetně	k7c2	včetně
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
pořádán	pořádán	k2eAgInSc1d1	pořádán
Jednotou	jednota	k1gFnSc7	jednota
českých	český	k2eAgMnPc2d1	český
matematiků	matematik	k1gMnPc2	matematik
a	a	k8xC	a
fyziků	fyzik	k1gMnPc2	fyzik
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Katedrou	katedra	k1gFnSc7	katedra
matematiky	matematika	k1gFnSc2	matematika
PdF	PdF	k1gFnSc2	PdF
UP	UP	kA	UP
a	a	k8xC	a
Katedrou	katedra	k1gFnSc7	katedra
algebry	algebra	k1gFnSc2	algebra
a	a	k8xC	a
geometrie	geometrie	k1gFnSc2	geometrie
PřF	PřF	k1gMnPc2	PřF
UP	UP	kA	UP
<g/>
.	.	kIx.	.
</s>
<s>
Soutěž	soutěž	k1gFnSc1	soutěž
se	se	k3xPyFc4	se
nedělí	dělit	k5eNaImIp3nS	dělit
na	na	k7c4	na
kola	kolo	k1gNnPc4	kolo
<g/>
,	,	kIx,	,
žáci	žák	k1gMnPc1	žák
základních	základní	k2eAgInPc2d1	základní
a	a	k8xC	a
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgFnPc4	který
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
absolvují	absolvovat	k5eAaPmIp3nP	absolvovat
v	v	k7c4	v
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Soutěžící	soutěžící	k1gFnSc1	soutěžící
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
pěti	pět	k4xCc2	pět
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
,	,	kIx,	,
zařazení	zařazení	k1gNnSc1	zařazení
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
věkem	věk	k1gInSc7	věk
<g/>
:	:	kIx,	:
Cvrček	Cvrček	k1gMnSc1	Cvrček
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
ZŠ	ZŠ	kA	ZŠ
<g/>
)	)	kIx)	)
Klokánek	klokánek	k1gMnSc1	klokánek
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
ZŠ	ZŠ	kA	ZŠ
<g/>
)	)	kIx)	)
Benjamín	Benjamín	k1gMnSc1	Benjamín
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
–	–	k?	–
7	[number]	k4	7
<g />
.	.	kIx.	.
</s>
<s>
<g/>
třída	třída	k1gFnSc1	třída
ZŠ	ZŠ	kA	ZŠ
<g/>
)	)	kIx)	)
Kadet	kadet	k1gMnSc1	kadet
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
ZŠ	ZŠ	kA	ZŠ
<g/>
)	)	kIx)	)
Junior	junior	k1gMnSc1	junior
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
SŠ	SŠ	kA	SŠ
<g/>
)	)	kIx)	)
Student	student	k1gMnSc1	student
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
SŠ	SŠ	kA	SŠ
<g/>
)	)	kIx)	)
V	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Cvrček	Cvrček	k1gMnSc1	Cvrček
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
řešení	řešení	k1gNnSc4	řešení
vyhrazeno	vyhradit	k5eAaPmNgNnS	vyhradit
45	[number]	k4	45
minut	minuta	k1gFnPc2	minuta
čistého	čistý	k2eAgInSc2d1	čistý
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
v	v	k7c6	v
kategoriích	kategorie	k1gFnPc6	kategorie
Klokánek	klokánek	k1gMnSc1	klokánek
<g/>
,	,	kIx,	,
Benjamín	Benjamín	k1gMnSc1	Benjamín
a	a	k8xC	a
Kadet	kadet	k1gMnSc1	kadet
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
řešení	řešení	k1gNnSc4	řešení
hodina	hodina	k1gFnSc1	hodina
času	čas	k1gInSc2	čas
a	a	k8xC	a
v	v	k7c6	v
kategoriích	kategorie	k1gFnPc6	kategorie
Junior	junior	k1gMnSc1	junior
a	a	k8xC	a
Student	student	k1gMnSc1	student
75	[number]	k4	75
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Řeší	řešit	k5eAaImIp3nS	řešit
se	se	k3xPyFc4	se
sada	sada	k1gFnSc1	sada
24	[number]	k4	24
úloh	úloha	k1gFnPc2	úloha
(	(	kIx(	(
<g/>
u	u	k7c2	u
cvrčka	cvrček	k1gMnSc2	cvrček
jen	jen	k9	jen
12	[number]	k4	12
úloh	úloha	k1gFnPc2	úloha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
každé	každý	k3xTgFnSc2	každý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
nabídnuto	nabídnout	k5eAaPmNgNnS	nabídnout
pět	pět	k4xCc1	pět
možností	možnost	k1gFnPc2	možnost
(	(	kIx(	(
<g/>
u	u	k7c2	u
cvrčka	cvrček	k1gMnSc2	cvrček
4	[number]	k4	4
možnosti	možnost	k1gFnSc2	možnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Žák	Žák	k1gMnSc1	Žák
tedy	tedy	k9	tedy
jako	jako	k9	jako
výsledek	výsledek	k1gInSc1	výsledek
zapíše	zapsat	k5eAaPmIp3nS	zapsat
sérii	série	k1gFnSc4	série
písmen	písmeno	k1gNnPc2	písmeno
(	(	kIx(	(
<g/>
A	a	k9	a
–	–	k?	–
E	E	kA	E
<g/>
)	)	kIx)	)
označující	označující	k2eAgNnSc1d1	označující
řešení	řešení	k1gNnSc1	řešení
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
zadání	zadání	k1gNnPc2	zadání
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
získat	získat	k5eAaPmF	získat
120	[number]	k4	120
bodů	bod	k1gInPc2	bod
(	(	kIx(	(
<g/>
u	u	k7c2	u
cvrčka	cvrček	k1gMnSc2	cvrček
jen	jen	k9	jen
60	[number]	k4	60
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úlohy	úloha	k1gFnPc1	úloha
jsou	být	k5eAaImIp3nP	být
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
do	do	k7c2	do
třech	tři	k4xCgFnPc2	tři
kategorií	kategorie	k1gFnPc2	kategorie
po	po	k7c6	po
8	[number]	k4	8
příkladech	příklad	k1gInPc6	příklad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
části	část	k1gFnSc6	část
jsou	být	k5eAaImIp3nP	být
příklady	příklad	k1gInPc1	příklad
za	za	k7c7	za
3	[number]	k4	3
body	bod	k1gInPc7	bod
<g/>
,	,	kIx,	,
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
za	za	k7c2	za
4	[number]	k4	4
a	a	k8xC	a
v	v	k7c4	v
třetí	třetí	k4xOgNnSc4	třetí
za	za	k7c2	za
5	[number]	k4	5
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nesprávnou	správný	k2eNgFnSc4d1	nesprávná
odpověď	odpověď	k1gFnSc4	odpověď
se	se	k3xPyFc4	se
strhává	strhávat	k5eAaImIp3nS	strhávat
1	[number]	k4	1
bod	bod	k1gInSc1	bod
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
má	mít	k5eAaImIp3nS	mít
soutěžící	soutěžící	k1gFnSc1	soutěžící
přidělených	přidělený	k2eAgInPc2d1	přidělený
24	[number]	k4	24
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
nemůže	moct	k5eNaImIp3nS	moct
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
mínusu	mínus	k1gInSc2	mínus
<g/>
.	.	kIx.	.
</s>
<s>
Vyhodnocení	vyhodnocení	k1gNnSc1	vyhodnocení
(	(	kIx(	(
<g/>
a	a	k8xC	a
tvorba	tvorba	k1gFnSc1	tvorba
výsledkových	výsledkový	k2eAgFnPc2d1	výsledková
listin	listina	k1gFnPc2	listina
<g/>
)	)	kIx)	)
probíhá	probíhat	k5eAaImIp3nS	probíhat
jak	jak	k6eAd1	jak
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
okresu	okres	k1gInSc6	okres
<g/>
,	,	kIx,	,
kraji	kraj	k1gInSc6	kraj
a	a	k8xC	a
na	na	k7c6	na
celorepublikové	celorepublikový	k2eAgFnSc6d1	celorepubliková
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
