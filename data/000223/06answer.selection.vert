<s>
Matematický	matematický	k2eAgMnSc1d1	matematický
klokan	klokan	k1gMnSc1	klokan
je	být	k5eAaImIp3nS	být
matematická	matematický	k2eAgFnSc1d1	matematická
soutěž	soutěž	k1gFnSc1	soutěž
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
organizuje	organizovat	k5eAaBmIp3nS	organizovat
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
třiceti	třicet	k4xCc6	třicet
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
včetně	včetně	k7c2	včetně
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
pořádán	pořádán	k2eAgInSc1d1	pořádán
Jednotou	jednota	k1gFnSc7	jednota
českých	český	k2eAgMnPc2d1	český
matematiků	matematik	k1gMnPc2	matematik
a	a	k8xC	a
fyziků	fyzik	k1gMnPc2	fyzik
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Katedrou	katedra	k1gFnSc7	katedra
matematiky	matematika	k1gFnSc2	matematika
PdF	PdF	k1gFnSc2	PdF
UP	UP	kA	UP
a	a	k8xC	a
Katedrou	katedra	k1gFnSc7	katedra
algebry	algebra	k1gFnSc2	algebra
a	a	k8xC	a
geometrie	geometrie	k1gFnSc2	geometrie
PřF	PřF	k1gMnPc2	PřF
UP	UP	kA	UP
<g/>
.	.	kIx.	.
</s>
