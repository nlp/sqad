odolnost	odolnost	k1gFnSc4	odolnost
vůči	vůči	k7c3	vůči
škůdcům	škůdce	k1gMnPc3	škůdce
(	(	kIx(	(
<g/>
Bt	Bt	k1gFnSc1	Bt
toxin	toxin	k1gInSc1	toxin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
některým	některý	k3yIgInPc3	některý
pesticidům	pesticid	k1gInPc3	pesticid
<g/>
,	,	kIx,	,
lepší	dobrý	k2eAgFnSc1d2	lepší
nutriční	nutriční	k2eAgFnPc4d1	nutriční
hodnoty	hodnota	k1gFnPc4	hodnota
a	a	k8xC	a
odolnost	odolnost	k1gFnSc4	odolnost
vůči	vůči	k7c3	vůči
nepříznivému	příznivý	k2eNgNnSc3d1	nepříznivé
klimatu	klima	k1gNnSc3	klima
