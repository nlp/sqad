<s>
K	k	k7c3	k
výhodným	výhodný	k2eAgFnPc3d1	výhodná
vlastnostem	vlastnost	k1gFnPc3	vlastnost
<g/>
,	,	kIx,	,
kterých	který	k3yQgFnPc2	který
se	se	k3xPyFc4	se
docílí	docílit	k5eAaPmIp3nP	docílit
genetickou	genetický	k2eAgFnSc7d1	genetická
modifikací	modifikace	k1gFnSc7	modifikace
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
odolnost	odolnost	k1gFnSc4	odolnost
vůči	vůči	k7c3	vůči
škůdcům	škůdce	k1gMnPc3	škůdce
(	(	kIx(	(
<g/>
Bt	Bt	k1gFnSc1	Bt
toxin	toxin	k1gInSc1	toxin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
některým	některý	k3yIgInPc3	některý
pesticidům	pesticid	k1gInPc3	pesticid
<g/>
,	,	kIx,	,
lepší	dobrý	k2eAgFnSc1d2	lepší
nutriční	nutriční	k2eAgFnPc4d1	nutriční
hodnoty	hodnota	k1gFnPc4	hodnota
a	a	k8xC	a
odolnost	odolnost	k1gFnSc4	odolnost
vůči	vůči	k7c3	vůči
nepříznivému	příznivý	k2eNgNnSc3d1	nepříznivé
klimatu	klima	k1gNnSc3	klima
<g/>
.	.	kIx.	.
</s>
