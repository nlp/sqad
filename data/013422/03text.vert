<p>
<s>
Heinrich	Heinrich	k1gMnSc1	Heinrich
Hoerle	Hoerle	k1gFnSc2	Hoerle
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
Kolín	Kolín	k1gInSc1	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc4	Německo
–	–	k?	–
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
tamtéž	tamtéž	k6eAd1	tamtéž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
malíř	malíř	k1gMnSc1	malíř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Heinrich	Heinrich	k1gMnSc1	Heinrich
Hoerle	Hoerle	k1gNnSc2	Hoerle
byl	být	k5eAaImAgMnS	být
malíř-samouk	malířamouk	k1gInSc4	malíř-samouk
a	a	k8xC	a
jen	jen	k9	jen
občasně	občasně	k6eAd1	občasně
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
Kölner	Kölner	k1gInSc1	Kölner
Werkschulen	Werkschulen	k2eAgInSc1d1	Werkschulen
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
ateliér	ateliér	k1gInSc4	ateliér
si	se	k3xPyFc3	se
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
domě	dům	k1gInSc6	dům
svých	svůj	k3xOyFgMnPc2	svůj
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
skupiny	skupina	k1gFnSc2	skupina
Lunisten	Lunisten	k2eAgMnSc1d1	Lunisten
<g/>
,	,	kIx,	,
jejímiž	jejíž	k3xOyRp3gMnPc7	jejíž
členy	člen	k1gMnPc7	člen
byli	být	k5eAaImAgMnP	být
i	i	k9	i
Max	Max	k1gMnSc1	Max
Ernst	Ernst	k1gMnSc1	Ernst
a	a	k8xC	a
Otto	Otto	k1gMnSc1	Otto
Freundlich	Freundlich	k1gMnSc1	Freundlich
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
ze	z	k7c2	z
služby	služba	k1gFnSc2	služba
v	v	k7c6	v
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
s	s	k7c7	s
Franzem	Franz	k1gMnSc7	Franz
Wilhelmem	Wilhelm	k1gMnSc7	Wilhelm
Seiwertem	Seiwert	k1gMnSc7	Seiwert
na	na	k7c6	na
žurnálu	žurnál	k1gInSc6	žurnál
Ventilator	Ventilator	k1gInSc1	Ventilator
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
Angelikou	Angelika	k1gFnSc7	Angelika
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
<g/>
–	–	k?	–
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
Hoerle	Hoerle	k1gInSc1	Hoerle
stal	stát	k5eAaPmAgInS	stát
aktivním	aktivní	k2eAgInSc7d1	aktivní
členem	člen	k1gInSc7	člen
kolínské	kolínský	k2eAgFnSc2d1	Kolínská
dadaistické	dadaistický	k2eAgFnSc2d1	dadaistická
scény	scéna	k1gFnSc2	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
místními	místní	k2eAgMnPc7d1	místní
dadaisty	dadaista	k1gMnPc7	dadaista
a	a	k8xC	a
konstruktivisty	konstruktivista	k1gMnPc7	konstruktivista
založil	založit	k5eAaPmAgInS	založit
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
skupinu	skupina	k1gFnSc4	skupina
Stupid	Stupida	k1gFnPc2	Stupida
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
spoluzaložil	spoluzaložit	k5eAaPmAgInS	spoluzaložit
"	"	kIx"	"
<g/>
skupinu	skupina	k1gFnSc4	skupina
progresivních	progresivní	k2eAgMnPc2d1	progresivní
umělců	umělec	k1gMnPc2	umělec
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
prezentovala	prezentovat	k5eAaBmAgFnS	prezentovat
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
<g/>
,	,	kIx,	,
Norimberku	Norimberk	k1gInSc6	Norimberk
<g/>
,	,	kIx,	,
Berlínu	Berlín	k1gInSc3	Berlín
<g/>
,	,	kIx,	,
Amsterdamu	Amsterdam	k1gInSc3	Amsterdam
<g/>
,	,	kIx,	,
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
začal	začít	k5eAaPmAgInS	začít
se	s	k7c7	s
Seiwertem	Seiwert	k1gMnSc7	Seiwert
a	a	k8xC	a
Walterem	Walter	k1gMnSc7	Walter
Sternem	sternum	k1gNnSc7	sternum
vydávat	vydávat	k5eAaImF	vydávat
progresivistický	progresivistický	k2eAgInSc1d1	progresivistický
žurnál	žurnál	k1gInSc1	žurnál
a-	a-	k?	a-
<g/>
z.	z.	k?	z.
Byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
německých	německý	k2eAgMnPc2d1	německý
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
odsouzeno	odsoudit	k5eAaPmNgNnS	odsoudit
jako	jako	k9	jako
zvrhlé	zvrhlý	k2eAgNnSc4d1	zvrhlé
umění	umění	k1gNnSc4	umění
<g/>
;	;	kIx,	;
mnoho	mnoho	k4c4	mnoho
jeho	jeho	k3xOp3gInPc2	jeho
obrazů	obraz	k1gInPc2	obraz
bylo	být	k5eAaImAgNnS	být
zničeno	zničen	k2eAgNnSc1d1	zničeno
<g/>
.	.	kIx.	.
<g/>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
mnoho	mnoho	k4c1	mnoho
jeho	jeho	k3xOp3gFnPc2	jeho
příbuzných	příbuzná	k1gFnPc2	příbuzná
bojoval	bojovat	k5eAaImAgInS	bojovat
s	s	k7c7	s
tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
<g/>
,	,	kIx,	,
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
léčit	léčit	k5eAaImF	léčit
na	na	k7c6	na
Mallorce	Mallorka	k1gFnSc6	Mallorka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1936	[number]	k4	1936
jí	jíst	k5eAaImIp3nS	jíst
v	v	k7c6	v
rodném	rodný	k2eAgInSc6d1	rodný
Kolíně	Kolín	k1gInSc6	Kolín
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
<g/>
,	,	kIx,	,
pohřben	pohřben	k2eAgMnSc1d1	pohřben
je	být	k5eAaImIp3nS	být
tamtéž	tamtéž	k6eAd1	tamtéž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Heinrich	Heinrich	k1gMnSc1	Heinrich
Hoerle	Hoerle	k1gFnPc4	Hoerle
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
