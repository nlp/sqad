<p>
<s>
Hloubkoměr	hloubkoměr	k1gInSc1	hloubkoměr
je	být	k5eAaImIp3nS	být
délkové	délkový	k2eAgNnSc4d1	délkové
měřidlo	měřidlo	k1gNnSc4	měřidlo
nebo	nebo	k8xC	nebo
měřicí	měřicí	k2eAgInSc4d1	měřicí
přístroj	přístroj	k1gInSc4	přístroj
k	k	k7c3	k
měření	měření	k1gNnSc3	měření
hloubky	hloubka	k1gFnSc2	hloubka
<g/>
.	.	kIx.	.
</s>
<s>
Užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
k	k	k7c3	k
různým	různý	k2eAgInPc3d1	různý
účelům	účel	k1gInPc3	účel
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
s	s	k7c7	s
rozdílnou	rozdílný	k2eAgFnSc7d1	rozdílná
konstrukcí	konstrukce	k1gFnSc7	konstrukce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Strojírenství	strojírenství	k1gNnSc1	strojírenství
==	==	k?	==
</s>
</p>
<p>
<s>
Hloubkoměr	hloubkoměr	k1gInSc1	hloubkoměr
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
měření	měření	k1gNnSc3	měření
hloubek	hloubka	k1gFnPc2	hloubka
slepých	slepý	k2eAgInPc2d1	slepý
otvorů	otvor	k1gInPc2	otvor
<g/>
,	,	kIx,	,
drážek	drážka	k1gFnPc2	drážka
a	a	k8xC	a
prohlubní	prohlubeň	k1gFnPc2	prohlubeň
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgFnPc1d3	nejčastější
konstrukce	konstrukce	k1gFnPc1	konstrukce
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Posuvné	posuvný	k2eAgNnSc1d1	posuvné
měřítko	měřítko	k1gNnSc1	měřítko
bývá	bývat	k5eAaImIp3nS	bývat
vybaveno	vybavit	k5eAaPmNgNnS	vybavit
ocelovým	ocelový	k2eAgInSc7d1	ocelový
páskem	pásek	k1gInSc7	pásek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
posuvnou	posuvna	k1gFnSc7	posuvna
čelistí	čelist	k1gFnPc2	čelist
a	a	k8xC	a
vysouvá	vysouvat	k5eAaImIp3nS	vysouvat
se	se	k3xPyFc4	se
ze	z	k7c2	z
spodního	spodní	k2eAgInSc2d1	spodní
konce	konec	k1gInSc2	konec
měřidla	měřidlo	k1gNnSc2	měřidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hloubkový	hloubkový	k2eAgInSc1d1	hloubkový
mikrometr	mikrometr	k1gInSc1	mikrometr
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
mikrometrického	mikrometrický	k2eAgInSc2d1	mikrometrický
šroubu	šroub	k1gInSc2	šroub
s	s	k7c7	s
odečítacím	odečítací	k2eAgNnSc7d1	odečítací
zařízením	zařízení	k1gNnSc7	zařízení
<g/>
,	,	kIx,	,
z	z	k7c2	z
příčného	příčný	k2eAgNnSc2d1	příčné
ramena	rameno	k1gNnSc2	rameno
s	s	k7c7	s
broušenou	broušený	k2eAgFnSc7d1	broušená
spodní	spodní	k2eAgFnSc7d1	spodní
plochou	plocha	k1gFnSc7	plocha
a	a	k8xC	a
z	z	k7c2	z
měřicího	měřicí	k2eAgInSc2d1	měřicí
trnu	trn	k1gInSc2	trn
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
z	z	k7c2	z
příčného	příčný	k2eAgNnSc2d1	příčné
ramene	rameno	k1gNnSc2	rameno
vysouvá	vysouvat	k5eAaImIp3nS	vysouvat
<g/>
.	.	kIx.	.
</s>
<s>
Měřící	měřící	k2eAgInPc1d1	měřící
trny	trn	k1gInPc1	trn
mívají	mívat	k5eAaImIp3nP	mívat
průměr	průměr	k1gInSc4	průměr
3	[number]	k4	3
až	až	k9	až
5	[number]	k4	5
mm	mm	kA	mm
a	a	k8xC	a
pro	pro	k7c4	pro
větší	veliký	k2eAgInPc4d2	veliký
hloubky	hloubek	k1gInPc4	hloubek
jsou	být	k5eAaImIp3nP	být
výměnné	výměnný	k2eAgInPc4d1	výměnný
<g/>
,	,	kIx,	,
odstupňované	odstupňovaný	k2eAgInPc4d1	odstupňovaný
po	po	k7c4	po
25	[number]	k4	25
mm	mm	kA	mm
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
maximální	maximální	k2eAgFnSc1d1	maximální
délka	délka	k1gFnSc1	délka
mikrometrického	mikrometrický	k2eAgInSc2d1	mikrometrický
šroubu	šroub	k1gInSc2	šroub
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ručkový	ručkový	k2eAgInSc1d1	ručkový
úchylkoměr	úchylkoměr	k1gInSc1	úchylkoměr
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
hodinky	hodinka	k1gFnPc4	hodinka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
i	i	k9	i
k	k	k7c3	k
přesnému	přesný	k2eAgNnSc3d1	přesné
měření	měření	k1gNnSc3	měření
hloubek	hloubka	k1gFnPc2	hloubka
<g/>
,	,	kIx,	,
měřicí	měřicí	k2eAgInSc1d1	měřicí
rozsah	rozsah	k1gInSc1	rozsah
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
malý	malý	k2eAgInSc1d1	malý
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
do	do	k7c2	do
10	[number]	k4	10
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potápění	potápění	k1gNnSc2	potápění
==	==	k?	==
</s>
</p>
<p>
<s>
Náramkový	náramkový	k2eAgInSc1d1	náramkový
hloubkoměr	hloubkoměr	k1gInSc1	hloubkoměr
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
také	také	k9	také
bathymetr	bathymetr	k1gInSc1	bathymetr
<g/>
,	,	kIx,	,
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
potápěči	potápěč	k1gMnPc1	potápěč
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
hluboko	hluboko	k6eAd1	hluboko
se	se	k3xPyFc4	se
ponořil	ponořit	k5eAaPmAgInS	ponořit
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgFnSc1d3	nejčastější
konstrukce	konstrukce	k1gFnSc1	konstrukce
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
stejném	stejný	k2eAgInSc6d1	stejný
principu	princip	k1gInSc6	princip
jako	jako	k9	jako
aneroid	aneroid	k1gInSc4	aneroid
nebo	nebo	k8xC	nebo
manometr	manometr	k1gInSc4	manometr
a	a	k8xC	a
měří	měřit	k5eAaImIp3nS	měřit
hydrostatický	hydrostatický	k2eAgInSc4d1	hydrostatický
tlak	tlak	k1gInSc4	tlak
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
stupnice	stupnice	k1gFnSc1	stupnice
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
cejchována	cejchován	k2eAgFnSc1d1	cejchována
v	v	k7c6	v
metrech	metr	k1gInPc6	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Potápěčský	potápěčský	k2eAgInSc1d1	potápěčský
počítač	počítač	k1gInSc1	počítač
je	být	k5eAaImIp3nS	být
kombinace	kombinace	k1gFnSc1	kombinace
tlakového	tlakový	k2eAgNnSc2d1	tlakové
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
piezoelektrického	piezoelektrický	k2eAgInSc2d1	piezoelektrický
<g/>
)	)	kIx)	)
hloubkoměru	hloubkoměr	k1gInSc2	hloubkoměr
a	a	k8xC	a
hodin	hodina	k1gFnPc2	hodina
se	s	k7c7	s
stopkami	stopka	k1gFnPc7	stopka
ve	v	k7c6	v
společném	společný	k2eAgNnSc6d1	společné
kruhovém	kruhový	k2eAgNnSc6d1	kruhové
pouzdře	pouzdro	k1gNnSc6	pouzdro
s	s	k7c7	s
číselnou	číselný	k2eAgFnSc7d1	číselná
indikací	indikace	k1gFnSc7	indikace
na	na	k7c6	na
displeji	displej	k1gInSc6	displej
<g/>
.	.	kIx.	.
</s>
<s>
Hodnoty	hodnota	k1gFnPc1	hodnota
se	se	k3xPyFc4	se
zapisují	zapisovat	k5eAaImIp3nP	zapisovat
do	do	k7c2	do
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Plavba	plavba	k1gFnSc1	plavba
==	==	k?	==
</s>
</p>
<p>
<s>
Hloubkoměr	hloubkoměr	k1gInSc1	hloubkoměr
slouží	sloužit	k5eAaImIp3nS	sloužit
na	na	k7c6	na
lodích	loď	k1gFnPc6	loď
k	k	k7c3	k
měření	měření	k1gNnSc3	měření
hloubky	hloubka	k1gFnSc2	hloubka
dna	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
ručnímu	ruční	k2eAgNnSc3d1	ruční
měření	měření	k1gNnSc3	měření
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
těžká	těžký	k2eAgFnSc1d1	těžká
olovnice	olovnice	k1gFnSc1	olovnice
na	na	k7c6	na
laně	lano	k1gNnSc6	lano
s	s	k7c7	s
metrovými	metrový	k2eAgInPc7d1	metrový
(	(	kIx(	(
<g/>
sáhovými	sáhový	k2eAgInPc7d1	sáhový
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
značkami	značka	k1gFnPc7	značka
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
použít	použít	k5eAaPmF	použít
jen	jen	k9	jen
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
desítek	desítka	k1gFnPc2	desítka
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
v	v	k7c6	v
klidné	klidný	k2eAgFnSc6d1	klidná
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
při	při	k7c6	při
pomalé	pomalý	k2eAgFnSc6d1	pomalá
plavbě	plavba	k1gFnSc6	plavba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Moderní	moderní	k2eAgNnSc1d1	moderní
plavidla	plavidlo	k1gNnPc1	plavidlo
používají	používat	k5eAaImIp3nP	používat
ultrazvukové	ultrazvukový	k2eAgInPc1d1	ultrazvukový
hloubkoměry	hloubkoměr	k1gInPc1	hloubkoměr
(	(	kIx(	(
<g/>
echolot	echolot	k1gInSc1	echolot
<g/>
)	)	kIx)	)
na	na	k7c6	na
principu	princip	k1gInSc6	princip
sonaru	sonar	k1gInSc2	sonar
<g/>
.	.	kIx.	.
</s>
<s>
Měří	měřit	k5eAaImIp3nS	měřit
čas	čas	k1gInSc4	čas
mezi	mezi	k7c7	mezi
vysláním	vyslání	k1gNnSc7	vyslání
ultrazvukového	ultrazvukový	k2eAgInSc2d1	ultrazvukový
pulzu	pulz	k1gInSc2	pulz
a	a	k8xC	a
přijetím	přijetí	k1gNnSc7	přijetí
ozvěny	ozvěna	k1gFnSc2	ozvěna
ode	ode	k7c2	ode
dna	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
<s>
Měří	měřit	k5eAaImIp3nS	měřit
tedy	tedy	k9	tedy
spojitě	spojitě	k6eAd1	spojitě
a	a	k8xC	a
dají	dát	k5eAaPmIp3nP	dát
se	se	k3xPyFc4	se
použít	použít	k5eAaPmF	použít
i	i	k9	i
pro	pro	k7c4	pro
velké	velký	k2eAgFnPc4d1	velká
hloubky	hloubka	k1gFnPc4	hloubka
<g/>
.	.	kIx.	.
</s>
</p>
