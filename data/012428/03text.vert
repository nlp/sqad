<p>
<s>
Žebravé	žebravý	k2eAgInPc4d1	žebravý
řády	řád	k1gInPc4	řád
neboli	neboli	k8xC	neboli
mendikanti	mendikant	k1gMnPc1	mendikant
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
mendicans	mendicans	k6eAd1	mendicans
–	–	k?	–
žebrající	žebrající	k2eAgFnPc4d1	žebrající
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
řeholních	řeholní	k2eAgInPc2d1	řeholní
řádů	řád	k1gInPc2	řád
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
zřekly	zřeknout	k5eAaPmAgInP	zřeknout
vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
společenství	společenství	k1gNnSc2	společenství
jako	jako	k8xC	jako
celku	celek	k1gInSc2	celek
(	(	kIx(	(
<g/>
řádu	řád	k1gInSc2	řád
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
nežebravých	žebravý	k2eNgInPc2d1	žebravý
řádů	řád	k1gInPc2	řád
(	(	kIx(	(
<g/>
např.	např.	kA	např.
cisterciáků	cisterciák	k1gMnPc2	cisterciák
<g/>
,	,	kIx,	,
benediktinů	benediktin	k1gMnPc2	benediktin
nebo	nebo	k8xC	nebo
premonstrátů	premonstrát	k1gMnPc2	premonstrát
<g/>
)	)	kIx)	)
tedy	tedy	k8xC	tedy
nedisponují	disponovat	k5eNaBmIp3nP	disponovat
pozemkovým	pozemkový	k2eAgInSc7d1	pozemkový
ani	ani	k8xC	ani
jiným	jiný	k2eAgInSc7d1	jiný
majetkem	majetek	k1gInSc7	majetek
k	k	k7c3	k
zabezpečení	zabezpečení	k1gNnSc3	zabezpečení
provozu	provoz	k1gInSc2	provoz
svých	svůj	k3xOyFgInPc2	svůj
klášterů	klášter	k1gInPc2	klášter
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
jsou	být	k5eAaImIp3nP	být
závislé	závislý	k2eAgInPc1d1	závislý
na	na	k7c6	na
vlastní	vlastní	k2eAgFnSc6d1	vlastní
práci	práce	k1gFnSc6	práce
a	a	k8xC	a
darech	dar	k1gInPc6	dar
dobrodinců	dobrodinec	k1gMnPc2	dobrodinec
<g/>
.	.	kIx.	.
</s>
<s>
Kláštery	klášter	k1gInPc1	klášter
žebravých	žebravý	k2eAgInPc2d1	žebravý
řádů	řád	k1gInPc2	řád
vznikají	vznikat	k5eAaImIp3nP	vznikat
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
<g/>
Ze	z	k7c2	z
středověku	středověk	k1gInSc2	středověk
dodnes	dodnes	k6eAd1	dodnes
přetrvaly	přetrvat	k5eAaPmAgInP	přetrvat
čtyři	čtyři	k4xCgInPc1	čtyři
velké	velký	k2eAgInPc1d1	velký
mendikantské	mendikantský	k2eAgInPc1d1	mendikantský
řády	řád	k1gInPc1	řád
<g/>
,	,	kIx,	,
uznané	uznaný	k2eAgInPc1d1	uznaný
Druhým	druhý	k4xOgInSc7	druhý
lyonským	lyonský	k2eAgInSc7d1	lyonský
koncilem	koncil	k1gInSc7	koncil
(	(	kIx(	(
<g/>
1274	[number]	k4	1274
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
dominikáni	dominikán	k1gMnPc5	dominikán
(	(	kIx(	(
<g/>
schválení	schválení	k1gNnSc4	schválení
1216	[number]	k4	1216
<g/>
)	)	kIx)	)
a	a	k8xC	a
františkáni	františkán	k1gMnPc1	františkán
(	(	kIx(	(
<g/>
1223	[number]	k4	1223
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
působili	působit	k5eAaImAgMnP	působit
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
ve	v	k7c6	v
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
karmelitáni	karmelitán	k1gMnPc1	karmelitán
(	(	kIx(	(
<g/>
1245	[number]	k4	1245
<g/>
)	)	kIx)	)
a	a	k8xC	a
augustiniáni-poustevníci	augustiniánioustevník	k1gMnPc1	augustiniáni-poustevník
(	(	kIx(	(
<g/>
1256	[number]	k4	1256
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tridentský	tridentský	k2eAgInSc1d1	tridentský
koncil	koncil	k1gInSc1	koncil
(	(	kIx(	(
<g/>
1545	[number]	k4	1545
<g/>
-	-	kIx~	-
<g/>
1563	[number]	k4	1563
<g/>
)	)	kIx)	)
povolil	povolit	k5eAaPmAgInS	povolit
vlastnění	vlastnění	k1gNnSc4	vlastnění
majetku	majetek	k1gInSc2	majetek
všem	všecek	k3xTgInPc3	všecek
žebravým	žebravý	k2eAgInPc3d1	žebravý
řádům	řád	k1gInPc3	řád
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
františkánů	františkán	k1gMnPc2	františkán
a	a	k8xC	a
kapucínů	kapucín	k1gMnPc2	kapucín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Žebravé	žebravý	k2eAgInPc1d1	žebravý
řády	řád	k1gInPc1	řád
kladou	klást	k5eAaImIp3nP	klást
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
asketický	asketický	k2eAgInSc4d1	asketický
způsob	způsob	k1gInSc4	způsob
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
chudobu	chudoba	k1gFnSc4	chudoba
<g/>
,	,	kIx,	,
odříkání	odříkání	k1gNnSc4	odříkání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
na	na	k7c4	na
službu	služba	k1gFnSc4	služba
bližním	bližní	k1gMnPc3	bližní
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
hmotnou	hmotný	k2eAgFnSc7d1	hmotná
(	(	kIx(	(
<g/>
provozování	provozování	k1gNnSc1	provozování
špitálů	špitál	k1gInPc2	špitál
–	–	k?	–
Milosrdní	milosrdný	k2eAgMnPc1d1	milosrdný
bratři	bratr	k1gMnPc1	bratr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
duševní	duševní	k2eAgNnSc4d1	duševní
(	(	kIx(	(
<g/>
vyučování	vyučování	k1gNnSc4	vyučování
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
duchovní	duchovní	k2eAgFnPc1d1	duchovní
(	(	kIx(	(
<g/>
pastorace	pastorace	k1gFnPc1	pastorace
<g/>
,	,	kIx,	,
misie	misie	k1gFnPc1	misie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přiklání	přiklánět	k5eAaImIp3nS	přiklánět
se	se	k3xPyFc4	se
k	k	k7c3	k
podobě	podoba	k1gFnSc3	podoba
apoštolského	apoštolský	k2eAgInSc2d1	apoštolský
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
vita	vit	k2eAgFnSc1d1	Vita
apostolica	apostolica	k1gFnSc1	apostolica
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgFnSc1d1	významná
je	být	k5eAaImIp3nS	být
i	i	k9	i
činnost	činnost	k1gFnSc1	činnost
kazatelská	kazatelský	k2eAgFnSc1d1	kazatelská
<g/>
,	,	kIx,	,
např.	např.	kA	např.
dominikánům	dominikán	k1gMnPc3	dominikán
se	se	k3xPyFc4	se
říkalo	říkat	k5eAaImAgNnS	říkat
bratři	bratr	k1gMnPc1	bratr
Kazatelé	kazatel	k1gMnPc1	kazatel
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
působení	působení	k1gNnSc4	působení
Svatého	svatý	k2eAgMnSc2d1	svatý
Dominika	Dominik	k1gMnSc2	Dominik
a	a	k8xC	a
jeho	jeho	k3xOp3gNnPc2	jeho
kázání	kázání	k1gNnPc2	kázání
proti	proti	k7c3	proti
herezi	hereze	k1gFnSc3	hereze
katarů	katar	k1gMnPc2	katar
a	a	k8xC	a
albigenských	albigenští	k1gMnPc2	albigenští
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Žebraví	žebravý	k2eAgMnPc1d1	žebravý
bratři	bratr	k1gMnPc1	bratr
==	==	k?	==
</s>
</p>
<p>
<s>
Zřeknutí	zřeknutí	k1gNnSc1	zřeknutí
se	se	k3xPyFc4	se
majetku	majetek	k1gInSc2	majetek
a	a	k8xC	a
spoléhání	spoléhání	k1gNnSc2	spoléhání
se	se	k3xPyFc4	se
na	na	k7c4	na
žebrotu	žebrota	k1gFnSc4	žebrota
jako	jako	k8xS	jako
na	na	k7c4	na
způsob	způsob	k1gInSc4	způsob
obživy	obživa	k1gFnSc2	obživa
znamenalo	znamenat	k5eAaImAgNnS	znamenat
odtržení	odtržení	k1gNnSc1	odtržení
se	se	k3xPyFc4	se
od	od	k7c2	od
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
nejzákladnějších	základní	k2eAgInPc2d3	nejzákladnější
principů	princip	k1gInPc2	princip
tradičního	tradiční	k2eAgInSc2d1	tradiční
monasticismu	monasticismus	k1gInSc2	monasticismus
<g/>
.	.	kIx.	.
</s>
<s>
Kázání	kázání	k1gNnSc1	kázání
a	a	k8xC	a
poskytování	poskytování	k1gNnSc1	poskytování
duchovní	duchovní	k2eAgFnSc2d1	duchovní
péče	péče	k1gFnSc2	péče
byl	být	k5eAaImAgInS	být
raison	raison	k1gInSc1	raison
d	d	k?	d
<g/>
́	́	k?	́
<g/>
ê	ê	k?	ê
žebravých	žebravý	k2eAgMnPc2d1	žebravý
mnichů	mnich	k1gMnPc2	mnich
<g/>
.	.	kIx.	.
</s>
<s>
Jasně	jasně	k6eAd1	jasně
dokazovali	dokazovat	k5eAaImAgMnP	dokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
angažovaný	angažovaný	k2eAgMnSc1d1	angažovaný
křesťan	křesťan	k1gMnSc1	křesťan
dokáže	dokázat	k5eAaPmIp3nS	dokázat
žít	žít	k5eAaImF	žít
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
běžných	běžný	k2eAgMnPc2d1	běžný
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
nepatřit	patřit	k5eNaImF	patřit
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
nebylo	být	k5eNaImAgNnS	být
třeba	třeba	k6eAd1	třeba
unikat	unikat	k5eAaImF	unikat
z	z	k7c2	z
lidského	lidský	k2eAgInSc2d1	lidský
světa	svět	k1gInSc2	svět
a	a	k8xC	a
uzavírat	uzavírat	k5eAaPmF	uzavírat
se	se	k3xPyFc4	se
do	do	k7c2	do
odlehlých	odlehlý	k2eAgInPc2d1	odlehlý
klášterů	klášter	k1gInPc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgNnSc1d1	jediné
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
čeho	co	k3yQnSc2	co
se	se	k3xPyFc4	se
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
obejít	obejít	k5eAaPmF	obejít
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
káli	kát	k5eAaImAgMnP	kát
a	a	k8xC	a
žili	žít	k5eAaImAgMnP	žít
ve	v	k7c6	v
shodě	shoda	k1gFnSc6	shoda
s	s	k7c7	s
evangeliem	evangelium	k1gNnSc7	evangelium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nezbytným	nezbytný	k2eAgInSc7d1	nezbytný
předpokladem	předpoklad	k1gInSc7	předpoklad
této	tento	k3xDgFnSc2	tento
misijní	misijní	k2eAgFnSc2d1	misijní
činnosti	činnost	k1gFnSc2	činnost
bylo	být	k5eAaImAgNnS	být
zřeknutí	zřeknutí	k1gNnSc1	zřeknutí
se	se	k3xPyFc4	se
veškerého	veškerý	k3xTgInSc2	veškerý
majetku	majetek	k1gInSc2	majetek
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
přicházející	přicházející	k2eAgFnSc1d1	přicházející
pohyblivost	pohyblivost	k1gFnSc1	pohyblivost
oproti	oproti	k7c3	oproti
mnichům	mnich	k1gMnPc3	mnich
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
vázáni	vázat	k5eAaImNgMnP	vázat
k	k	k7c3	k
domu	dům	k1gInSc3	dům
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
složili	složit	k5eAaPmAgMnP	složit
své	svůj	k3xOyFgInPc4	svůj
sliby	slib	k1gInPc4	slib
<g/>
.	.	kIx.	.
</s>
<s>
Konzervativnější	konzervativní	k2eAgMnPc1d2	konzervativnější
protivníci	protivník	k1gMnPc1	protivník
nazývali	nazývat	k5eAaImAgMnP	nazývat
tyto	tento	k3xDgInPc4	tento
mnichy	mnich	k1gInPc4	mnich
gyrovagi	gyrovagi	k1gNnSc4	gyrovagi
–	–	k?	–
potulní	potulný	k2eAgMnPc1d1	potulný
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
<g/>
svatý	svatý	k2eAgInSc4d1	svatý
Benedikt	benedikt	k1gInSc4	benedikt
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
choval	chovat	k5eAaImAgMnS	chovat
nejhlubší	hluboký	k2eAgNnSc4d3	nejhlubší
opovržení	opovržení	k1gNnSc4	opovržení
<g/>
.	.	kIx.	.
</s>
<s>
Takovíto	takovýto	k3xDgMnPc1	takovýto
mniši	mnich	k1gMnPc1	mnich
však	však	k9	však
měli	mít	k5eAaImAgMnP	mít
volnost	volnost	k1gFnSc4	volnost
pohybu	pohyb	k1gInSc2	pohyb
a	a	k8xC	a
mohli	moct	k5eAaImAgMnP	moct
putovat	putovat	k5eAaImF	putovat
od	od	k7c2	od
kláštera	klášter	k1gInSc2	klášter
ke	k	k7c3	k
klášteru	klášter	k1gInSc3	klášter
<g/>
,	,	kIx,	,
z	z	k7c2	z
provincie	provincie	k1gFnSc2	provincie
do	do	k7c2	do
provincie	provincie	k1gFnSc2	provincie
<g/>
,	,	kIx,	,
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
kázání	kázání	k1gNnSc2	kázání
čí	čí	k3xOyQgFnSc2	čí
studia	studio	k1gNnPc1	studio
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
co	co	k3yQnSc1	co
jim	on	k3xPp3gMnPc3	on
předepsali	předepsat	k5eAaPmAgMnP	předepsat
jejich	jejich	k3xOp3gNnPc4	jejich
představení	představení	k1gNnPc4	představení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Společenský	společenský	k2eAgInSc1d1	společenský
kontext	kontext	k1gInSc1	kontext
==	==	k?	==
</s>
</p>
<p>
<s>
Celých	celý	k2eAgNnPc2d1	celé
150	[number]	k4	150
let	léto	k1gNnPc2	léto
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1200	[number]	k4	1200
zažívala	zažívat	k5eAaImAgFnS	zažívat
Evropa	Evropa	k1gFnSc1	Evropa
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
období	období	k1gNnSc4	období
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
a	a	k8xC	a
demografické	demografický	k2eAgFnSc2d1	demografická
expanze	expanze	k1gFnSc2	expanze
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
bohatství	bohatství	k1gNnSc3	bohatství
plynoucímu	plynoucí	k2eAgInSc3d1	plynoucí
z	z	k7c2	z
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
výroby	výroba	k1gFnSc2	výroba
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
města	město	k1gNnSc2	město
markantně	markantně	k6eAd1	markantně
rozrůstat	rozrůstat	k5eAaImF	rozrůstat
a	a	k8xC	a
nabývat	nabývat	k5eAaImF	nabývat
dnešní	dnešní	k2eAgFnSc2d1	dnešní
moderní	moderní	k2eAgFnSc2d1	moderní
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
podmínkách	podmínka	k1gFnPc6	podmínka
začal	začít	k5eAaPmAgInS	začít
vzrůstat	vzrůstat	k5eAaImF	vzrůstat
antiklerikalismus	antiklerikalismus	k1gInSc1	antiklerikalismus
a	a	k8xC	a
náboženský	náboženský	k2eAgInSc1d1	náboženský
nonkonformismus	nonkonformismus	k1gInSc1	nonkonformismus
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
byla	být	k5eAaImAgFnS	být
mobilnější	mobilní	k2eAgFnPc4d2	mobilnější
<g/>
,	,	kIx,	,
kritičtější	kritický	k2eAgFnPc4d2	kritičtější
a	a	k8xC	a
její	její	k3xOp3gFnPc4	její
horní	horní	k2eAgFnPc4d1	horní
vrstvy	vrstva	k1gFnPc4	vrstva
blahobytnější	blahobytný	k2eAgFnPc4d2	blahobytnější
<g/>
,	,	kIx,	,
než	než	k8xS	než
kdy	kdy	k6eAd1	kdy
předtím	předtím	k6eAd1	předtím
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
laiky	laik	k1gMnPc7	laik
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
šířit	šířit	k5eAaImF	šířit
gramotnost	gramotnost	k1gFnSc1	gramotnost
a	a	k8xC	a
přestala	přestat	k5eAaPmAgFnS	přestat
být	být	k5eAaImF	být
výsadou	výsada	k1gFnSc7	výsada
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
vrstva	vrstva	k1gFnSc1	vrstva
gramotných	gramotný	k2eAgMnPc2d1	gramotný
a	a	k8xC	a
vzdělanějších	vzdělaný	k2eAgMnPc2d2	vzdělanější
laiků	laik	k1gMnPc2	laik
začala	začít	k5eAaPmAgFnS	začít
kriticky	kriticky	k6eAd1	kriticky
nahlížet	nahlížet	k5eAaImF	nahlížet
na	na	k7c4	na
rozumové	rozumový	k2eAgInPc4d1	rozumový
a	a	k8xC	a
mravní	mravní	k2eAgInPc4d1	mravní
nedostatky	nedostatek	k1gInPc4	nedostatek
duchovenstva	duchovenstvo	k1gNnSc2	duchovenstvo
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
napětí	napětí	k1gNnSc4	napětí
mezi	mezi	k7c7	mezi
tradičními	tradiční	k2eAgFnPc7d1	tradiční
premisami	premisa	k1gFnPc7	premisa
mnišské	mnišský	k2eAgFnSc2d1	mnišská
spirituality	spiritualita	k1gFnSc2	spiritualita
a	a	k8xC	a
tužbami	tužba	k1gFnPc7	tužba
laického	laický	k2eAgInSc2d1	laický
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgNnSc6	který
se	se	k3xPyFc4	se
právě	právě	k6eAd1	právě
probudilo	probudit	k5eAaPmAgNnS	probudit
vědomí	vědomí	k1gNnSc4	vědomí
jeho	on	k3xPp3gNnSc2	on
křesťanského	křesťanský	k2eAgNnSc2d1	křesťanské
povolání	povolání	k1gNnSc2	povolání
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tyto	tento	k3xDgFnPc4	tento
potřeby	potřeba	k1gFnPc4	potřeba
laického	laický	k2eAgInSc2d1	laický
lidu	lid	k1gInSc2	lid
nebyla	být	k5eNaImAgFnS	být
církev	církev	k1gFnSc1	církev
vybavena	vybavit	k5eAaPmNgFnS	vybavit
<g/>
.	.	kIx.	.
</s>
<s>
Diecézní	diecézní	k2eAgFnSc1d1	diecézní
a	a	k8xC	a
farní	farní	k2eAgFnSc1d1	farní
struktura	struktura	k1gFnSc1	struktura
byla	být	k5eAaImAgFnS	být
uzpůsobena	uzpůsobit	k5eAaPmNgFnS	uzpůsobit
především	především	k9	především
potřebě	potřeba	k1gFnSc6	potřeba
venkovského	venkovský	k2eAgNnSc2d1	venkovské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
duchovních	duchovní	k1gMnPc2	duchovní
byla	být	k5eAaImAgFnS	být
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
svobodných	svobodný	k2eAgMnPc2d1	svobodný
rolníků	rolník	k1gMnPc2	rolník
a	a	k8xC	a
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
míry	míra	k1gFnSc2	míra
vzdělání	vzdělání	k1gNnSc2	vzdělání
příliš	příliš	k6eAd1	příliš
nepřevyšovala	převyšovat	k5eNaImAgFnS	převyšovat
úroveň	úroveň	k1gFnSc1	úroveň
jejich	jejich	k3xOp3gMnPc2	jejich
venkovských	venkovský	k2eAgMnPc2d1	venkovský
farníků	farník	k1gMnPc2	farník
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tuto	tento	k3xDgFnSc4	tento
mezeru	mezera	k1gFnSc4	mezera
měli	mít	k5eAaImAgMnP	mít
zaplnit	zaplnit	k5eAaPmF	zaplnit
žebraví	žebravý	k2eAgMnPc1d1	žebravý
bratři	bratr	k1gMnPc1	bratr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
přístupném	přístupný	k2eAgNnSc6d1	přístupné
novým	nový	k2eAgFnPc3d1	nová
myšlenkám	myšlenka	k1gFnPc3	myšlenka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
začala	začít	k5eAaPmAgNnP	začít
šířit	šířit	k5eAaImF	šířit
různá	různý	k2eAgNnPc1d1	různé
nekonformní	konformní	k2eNgNnPc1d1	nekonformní
hnutí	hnutí	k1gNnPc1	hnutí
obviňovaná	obviňovaný	k2eAgNnPc1d1	obviňované
z	z	k7c2	z
hereze	hereze	k1gFnSc2	hereze
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
nedokázali	dokázat	k5eNaPmAgMnP	dokázat
sekulární	sekulární	k2eAgMnPc4d1	sekulární
duchovní	duchovní	k1gMnPc4	duchovní
úspěšně	úspěšně	k6eAd1	úspěšně
vzdorovat	vzdorovat	k5eAaImF	vzdorovat
<g/>
.	.	kIx.	.
</s>
<s>
Kacířství	kacířství	k1gNnSc1	kacířství
bylo	být	k5eAaImAgNnS	být
všude	všude	k6eAd1	všude
a	a	k8xC	a
církev	církev	k1gFnSc1	církev
čelila	čelit	k5eAaImAgFnS	čelit
krizi	krize	k1gFnSc4	krize
<g/>
.	.	kIx.	.
</s>
<s>
Objevovaly	objevovat	k5eAaImAgFnP	objevovat
se	se	k3xPyFc4	se
nové	nový	k2eAgFnPc1d1	nová
radikální	radikální	k2eAgFnPc1d1	radikální
podoby	podoba	k1gFnPc1	podoba
laické	laický	k2eAgFnSc2d1	laická
zbožnosti	zbožnost	k1gFnSc2	zbožnost
<g/>
,	,	kIx,	,
inspirující	inspirující	k2eAgInSc4d1	inspirující
se	se	k3xPyFc4	se
nezprostředkovaným	zprostředkovaný	k2eNgInSc7d1	nezprostředkovaný
výkladem	výklad	k1gInSc7	výklad
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc1	jehož
části	část	k1gFnPc1	část
kolovaly	kolovat	k5eAaImAgFnP	kolovat
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
neoficiálních	neoficiální	k2eAgInPc6d1	neoficiální
překladech	překlad	k1gInPc6	překlad
a	a	k8xC	a
potenciálně	potenciálně	k6eAd1	potenciálně
ohrožovaly	ohrožovat	k5eAaImAgFnP	ohrožovat
hierarchickou	hierarchický	k2eAgFnSc4d1	hierarchická
strukturu	struktura	k1gFnSc4	struktura
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Otázkou	otázka	k1gFnSc7	otázka
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
bouřlivý	bouřlivý	k2eAgInSc1d1	bouřlivý
nával	nával	k1gInSc1	nával
nadšení	nadšení	k1gNnSc1	nadšení
možné	možný	k2eAgNnSc1d1	možné
usměrnit	usměrnit	k5eAaPmF	usměrnit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
sloužil	sloužit	k5eAaImAgMnS	sloužit
věci	věc	k1gFnSc2	věc
pravověří	pravověří	k?	pravověří
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
Inocenc	Inocenc	k1gMnSc1	Inocenc
III	III	kA	III
<g/>
.	.	kIx.	.
riskoval	riskovat	k5eAaBmAgInS	riskovat
mnohé	mnohé	k1gNnSc4	mnohé
<g/>
,	,	kIx,	,
když	když	k8xS	když
udělil	udělit	k5eAaPmAgInS	udělit
podmínečné	podmínečný	k2eAgNnSc4d1	podmínečné
svolení	svolení	k1gNnSc4	svolení
určitým	určitý	k2eAgFnPc3d1	určitá
skupinám	skupina	k1gFnPc3	skupina
žebravých	žebravý	k2eAgMnPc2d1	žebravý
laických	laický	k2eAgMnPc2d1	laický
kazatelů	kazatel	k1gMnPc2	kazatel
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
však	však	k9	však
vyplatilo	vyplatit	k5eAaPmAgNnS	vyplatit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Noví	nový	k2eAgMnPc1d1	nový
evangelisté	evangelista	k1gMnPc1	evangelista
==	==	k?	==
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
žebravých	žebravý	k2eAgMnPc2d1	žebravý
mnichů	mnich	k1gMnPc2	mnich
existovalo	existovat	k5eAaImAgNnS	existovat
ještě	ještě	k6eAd1	ještě
několik	několik	k4yIc1	několik
skupin	skupina	k1gFnPc2	skupina
potulných	potulný	k2eAgMnPc2d1	potulný
hlasatelů	hlasatel	k1gMnPc2	hlasatel
evangelia	evangelium	k1gNnSc2	evangelium
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
společným	společný	k2eAgInSc7d1	společný
znakem	znak	k1gInSc7	znak
byla	být	k5eAaImAgFnS	být
nová	nový	k2eAgFnSc1d1	nová
<g/>
,	,	kIx,	,
radikálnější	radikální	k2eAgFnSc1d2	radikálnější
interpretace	interpretace	k1gFnSc1	interpretace
apoštolského	apoštolský	k2eAgInSc2d1	apoštolský
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
podstatnou	podstatný	k2eAgFnSc7d1	podstatná
součástí	součást	k1gFnSc7	součást
byla	být	k5eAaImAgFnS	být
dobrovolně	dobrovolně	k6eAd1	dobrovolně
přijímaná	přijímaný	k2eAgFnSc1d1	přijímaná
chudoba	chudoba	k1gFnSc1	chudoba
a	a	k8xC	a
ideál	ideál	k1gInSc1	ideál
kazatelské	kazatelský	k2eAgFnSc2d1	kazatelská
misie	misie	k1gFnSc2	misie
<g/>
,	,	kIx,	,
obracející	obracející	k2eAgMnPc4d1	obracející
lidi	člověk	k1gMnPc4	člověk
k	k	k7c3	k
nové	nový	k2eAgFnSc3d1	nová
víře	víra	k1gFnSc3	víra
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starém	starý	k2eAgInSc6d1	starý
ideálu	ideál	k1gInSc6	ideál
byl	být	k5eAaImAgInS	být
odhalen	odhalit	k5eAaPmNgInS	odhalit
nový	nový	k2eAgInSc1d1	nový
rozměr	rozměr	k1gInSc1	rozměr
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
následování	následování	k1gNnSc1	následování
Ježíše	Ježíš	k1gMnSc2	Ježíš
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
jeho	jeho	k3xOp3gInSc2	jeho
pozemského	pozemský	k2eAgInSc2d1	pozemský
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
jak	jak	k8xC	jak
jej	on	k3xPp3gMnSc4	on
vyjevuje	vyjevovat	k5eAaImIp3nS	vyjevovat
evangelium	evangelium	k1gNnSc1	evangelium
<g/>
.	.	kIx.	.
</s>
<s>
Docházelo	docházet	k5eAaImAgNnS	docházet
ke	k	k7c3	k
znovuobjevení	znovuobjevení	k1gNnSc3	znovuobjevení
doslovného	doslovný	k2eAgInSc2d1	doslovný
smyslu	smysl	k1gInSc2	smysl
evangelia	evangelium	k1gNnSc2	evangelium
<g/>
.	.	kIx.	.
</s>
<s>
Podnět	podnět	k1gInSc1	podnět
k	k	k7c3	k
formování	formování	k1gNnSc3	formování
skupin	skupina	k1gFnPc2	skupina
evangelistů	evangelista	k1gMnPc2	evangelista
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
vycházel	vycházet	k5eAaImAgInS	vycházet
ze	z	k7c2	z
zámožných	zámožný	k2eAgFnPc2d1	zámožná
a	a	k8xC	a
vyhraněnějších	vyhraněný	k2eAgFnPc2d2	vyhraněnější
sfér	sféra	k1gFnPc2	sféra
laických	laický	k2eAgMnPc2d1	laický
obyvatel	obyvatel	k1gMnPc2	obyvatel
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zakladatelem	zakladatel	k1gMnSc7	zakladatel
jedné	jeden	k4xCgFnSc2	jeden
takové	takový	k3xDgFnSc2	takový
skupiny	skupina	k1gFnSc2	skupina
byl	být	k5eAaImAgMnS	být
Valdè	Valdè	k1gMnSc1	Valdè
<g/>
,	,	kIx,	,
obchodník	obchodník	k1gMnSc1	obchodník
a	a	k8xC	a
bankéř	bankéř	k1gMnSc1	bankéř
z	z	k7c2	z
Lyonu	Lyon	k1gInSc2	Lyon
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
náhodném	náhodný	k2eAgNnSc6d1	náhodné
vyslechnutí	vyslechnutí	k1gNnSc6	vyslechnutí
příběhu	příběh	k1gInSc2	příběh
svatého	svatý	k2eAgMnSc2d1	svatý
Alexia	Alexius	k1gMnSc2	Alexius
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgMnS	vzdát
všeho	všecek	k3xTgMnSc4	všecek
a	a	k8xC	a
oddal	oddat	k5eAaPmAgMnS	oddat
se	se	k3xPyFc4	se
chudobě	chudoba	k1gFnSc3	chudoba
z	z	k7c2	z
lásky	láska	k1gFnSc2	láska
k	k	k7c3	k
Bohu	bůh	k1gMnSc3	bůh
<g/>
,	,	kIx,	,
vyhledal	vyhledat	k5eAaPmAgMnS	vyhledat
Valdè	Valdè	k1gFnSc2	Valdè
jistého	jistý	k2eAgMnSc2d1	jistý
učence	učenec	k1gMnSc2	učenec
a	a	k8xC	a
požádal	požádat	k5eAaPmAgMnS	požádat
ho	on	k3xPp3gMnSc4	on
o	o	k7c4	o
radu	rada	k1gFnSc4	rada
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
má	mít	k5eAaImIp3nS	mít
změnit	změnit	k5eAaPmF	změnit
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Mistr	mistr	k1gMnSc1	mistr
ho	on	k3xPp3gMnSc4	on
odkázal	odkázat	k5eAaPmAgMnS	odkázat
na	na	k7c4	na
úryvek	úryvek	k1gInSc4	úryvek
z	z	k7c2	z
evangelia	evangelium	k1gNnSc2	evangelium
"	"	kIx"	"
<g/>
Chceš	chtít	k5eAaImIp2nS	chtít
<g/>
-li	i	k?	-li
být	být	k5eAaImF	být
dokonalý	dokonalý	k2eAgInSc4d1	dokonalý
<g/>
,	,	kIx,	,
jdi	jít	k5eAaImRp2nS	jít
<g/>
,	,	kIx,	,
prodej	prodej	k1gInSc4	prodej
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
ti	ty	k3xPp2nSc3	ty
patří	patřit	k5eAaImIp3nS	patřit
<g/>
,	,	kIx,	,
rozdej	rozdat	k5eAaPmRp2nS	rozdat
chudým	chudý	k2eAgFnPc3d1	chudá
a	a	k8xC	a
budeš	být	k5eAaImBp2nS	být
mít	mít	k5eAaImF	mít
poklad	poklad	k1gInSc4	poklad
v	v	k7c6	v
nebi	nebe	k1gNnSc6	nebe
<g/>
;	;	kIx,	;
pak	pak	k6eAd1	pak
přijď	přijít	k5eAaPmRp2nS	přijít
a	a	k8xC	a
následuj	následovat	k5eAaImRp2nS	následovat
mne	já	k3xPp1nSc4	já
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Mt	Mt	k1gFnSc1	Mt
19	[number]	k4	19
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
)	)	kIx)	)
Poté	poté	k6eAd1	poté
Valdè	Valdè	k1gMnSc1	Valdè
učinil	učinit	k5eAaPmAgMnS	učinit
radikální	radikální	k2eAgInSc4d1	radikální
krok	krok	k1gInSc4	krok
<g/>
:	:	kIx,	:
část	část	k1gFnSc4	část
svého	svůj	k3xOyFgInSc2	svůj
majetku	majetek	k1gInSc2	majetek
přepsal	přepsat	k5eAaPmAgMnS	přepsat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
ženu	žena	k1gFnSc4	žena
<g/>
,	,	kIx,	,
dvěma	dva	k4xCgFnPc7	dva
dcerám	dcera	k1gFnPc3	dcera
dal	dát	k5eAaPmAgMnS	dát
věno	věno	k1gNnSc4	věno
a	a	k8xC	a
poslal	poslat	k5eAaPmAgMnS	poslat
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
kláštera	klášter	k1gInSc2	klášter
<g/>
,	,	kIx,	,
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
rozdal	rozdat	k5eAaPmAgInS	rozdat
chudým	chudý	k1gMnPc3	chudý
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
potulným	potulný	k2eAgMnSc7d1	potulný
kazatelem	kazatel	k1gMnSc7	kazatel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
kázal	kázat	k5eAaImAgInS	kázat
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
a	a	k8xC	a
živil	živit	k5eAaImAgMnS	živit
se	se	k3xPyFc4	se
žebrotou	žebrota	k1gFnSc7	žebrota
a	a	k8xC	a
zanedlouho	zanedlouho	k6eAd1	zanedlouho
začal	začít	k5eAaPmAgInS	začít
získávat	získávat	k5eAaImF	získávat
první	první	k4xOgMnPc4	první
žáky	žák	k1gMnPc4	žák
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1181	[number]	k4	1181
vyzván	vyzván	k2eAgInSc1d1	vyzván
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
zodpovídal	zodpovídat	k5eAaPmAgMnS	zodpovídat
legátské	legátský	k2eAgFnSc3d1	legátský
synodě	synoda	k1gFnSc3	synoda
<g/>
,	,	kIx,	,
konající	konající	k2eAgMnSc1d1	konající
se	se	k3xPyFc4	se
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
v	v	k7c6	v
Lyonu	Lyon	k1gInSc6	Lyon
<g/>
,	,	kIx,	,
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
se	se	k3xPyFc4	se
k	k	k7c3	k
pravé	pravý	k2eAgFnSc3d1	pravá
víře	víra	k1gFnSc3	víra
a	a	k8xC	a
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ani	ani	k8xC	ani
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
jeho	jeho	k3xOp3gMnPc1	jeho
stoupenci	stoupenec	k1gMnPc1	stoupenec
nebudou	být	k5eNaImBp3nP	být
nikdy	nikdy	k6eAd1	nikdy
hromadit	hromadit	k5eAaImF	hromadit
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
potraviny	potravina	k1gFnPc4	potravina
či	či	k8xC	či
šatstvo	šatstvo	k1gNnSc4	šatstvo
a	a	k8xC	a
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
žít	žít	k5eAaImF	žít
v	v	k7c6	v
úplné	úplný	k2eAgFnSc6d1	úplná
chudobě	chudoba	k1gFnSc6	chudoba
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
označováni	označován	k2eAgMnPc1d1	označován
za	za	k7c4	za
valdenské	valdenští	k1gMnPc4	valdenští
nebo	nebo	k8xC	nebo
také	také	k9	také
za	za	k7c4	za
Lyonské	lyonský	k2eAgMnPc4d1	lyonský
chudé	chudý	k1gMnPc4	chudý
<g/>
.	.	kIx.	.
</s>
<s>
Setkali	setkat	k5eAaPmAgMnP	setkat
se	se	k3xPyFc4	se
však	však	k9	však
s	s	k7c7	s
nepřátelstvím	nepřátelství	k1gNnSc7	nepřátelství
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
kněží	kněz	k1gMnPc2	kněz
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
mnozí	mnohý	k2eAgMnPc1d1	mnohý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
později	pozdě	k6eAd2	pozdě
propadli	propadnout	k5eAaPmAgMnP	propadnout
radikálnímu	radikální	k2eAgInSc3d1	radikální
antiklerikalismu	antiklerikalismus	k1gInSc3	antiklerikalismus
<g/>
.	.	kIx.	.
<g/>
Další	další	k2eAgFnSc7d1	další
skupinou	skupina	k1gFnSc7	skupina
snažící	snažící	k2eAgFnSc2d1	snažící
se	se	k3xPyFc4	se
o	o	k7c4	o
návrat	návrat	k1gInSc4	návrat
k	k	k7c3	k
evangeliu	evangelium	k1gNnSc3	evangelium
byli	být	k5eAaImAgMnP	být
humiliáti	humiliát	k1gMnPc1	humiliát
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
řeholní	řeholní	k2eAgNnSc4d1	řeholní
bratrstvo	bratrstvo	k1gNnSc4	bratrstvo
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
získalo	získat	k5eAaPmAgNnS	získat
množství	množství	k1gNnSc1	množství
přívrženců	přívrženec	k1gMnPc2	přívrženec
ve	v	k7c6	v
městech	město	k1gNnPc6	město
Lombardie	Lombardie	k1gFnSc2	Lombardie
a	a	k8xC	a
Benátska	Benátsko	k1gNnSc2	Benátsko
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
papež	papež	k1gMnSc1	papež
povolil	povolit	k5eAaPmAgMnS	povolit
kázání	kázání	k1gNnSc4	kázání
i	i	k8xC	i
laickým	laický	k2eAgMnPc3d1	laický
členům	člen	k1gMnPc3	člen
bratrstva	bratrstvo	k1gNnSc2	bratrstvo
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
s	s	k7c7	s
určitými	určitý	k2eAgNnPc7d1	určité
omezeními	omezení	k1gNnPc7	omezení
<g/>
.	.	kIx.	.
</s>
<s>
Povolení	povolení	k1gNnSc4	povolení
kázat	kázat	k5eAaImF	kázat
laikům	laik	k1gMnPc3	laik
bylo	být	k5eAaImAgNnS	být
první	první	k4xOgFnSc6	první
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
autorit	autorita	k1gFnPc2	autorita
církve	církev	k1gFnSc2	církev
však	však	k9	však
toto	tento	k3xDgNnSc4	tento
povolení	povolení	k1gNnSc4	povolení
brala	brát	k5eAaImAgFnS	brát
jako	jako	k9	jako
zásah	zásah	k1gInSc4	zásah
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
pravomocí	pravomoc	k1gFnPc2	pravomoc
a	a	k8xC	a
snahu	snaha	k1gFnSc4	snaha
o	o	k7c4	o
jejich	jejich	k3xOp3gFnSc4	jejich
uzurpaci	uzurpace	k1gFnSc4	uzurpace
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
podvratnou	podvratný	k2eAgFnSc4d1	podvratná
činnost	činnost	k1gFnSc4	činnost
a	a	k8xC	a
herezi	hereze	k1gFnSc4	hereze
a	a	k8xC	a
snažili	snažit	k5eAaImAgMnP	snažit
se	se	k3xPyFc4	se
tuto	tento	k3xDgFnSc4	tento
výhradu	výhrada	k1gFnSc4	výhrada
laikům	laik	k1gMnPc3	laik
opět	opět	k6eAd1	opět
odepřít	odepřít	k5eAaPmF	odepřít
<g/>
.	.	kIx.	.
</s>
<s>
Lucius	Lucius	k1gMnSc1	Lucius
III	III	kA	III
<g/>
.	.	kIx.	.
pak	pak	k6eAd1	pak
roku	rok	k1gInSc2	rok
1184	[number]	k4	1184
valdenské	valdenský	k2eAgInPc1d1	valdenský
i	i	k8xC	i
humiliáty	humiliáta	k1gFnPc4	humiliáta
vyobcoval	vyobcovat	k5eAaPmAgMnS	vyobcovat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
sektami	sekta	k1gFnPc7	sekta
z	z	k7c2	z
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
nástupce	nástupce	k1gMnSc1	nástupce
Inocenc	Inocenc	k1gMnSc1	Inocenc
III	III	kA	III
<g/>
.	.	kIx.	.
nicméně	nicméně	k8xC	nicméně
humiliáty	humiliát	k1gInPc4	humiliát
opět	opět	k6eAd1	opět
rehabilitoval	rehabilitovat	k5eAaBmAgInS	rehabilitovat
a	a	k8xC	a
s	s	k7c7	s
církví	církev	k1gFnSc7	církev
smířil	smířit	k5eAaPmAgInS	smířit
i	i	k9	i
umírněné	umírněný	k2eAgNnSc1d1	umírněné
a	a	k8xC	a
kompromisu	kompromis	k1gInSc3	kompromis
nakloněné	nakloněný	k2eAgNnSc1d1	nakloněné
křídlo	křídlo	k1gNnSc1	křídlo
valdenských	valdenští	k1gMnPc2	valdenští
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
začalo	začít	k5eAaPmAgNnS	začít
být	být	k5eAaImF	být
označováno	označovat	k5eAaImNgNnS	označovat
za	za	k7c4	za
Katolické	katolický	k2eAgMnPc4d1	katolický
chudé	chudý	k1gMnPc4	chudý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hlavní	hlavní	k2eAgInPc1d1	hlavní
žebravé	žebravý	k2eAgInPc1d1	žebravý
řády	řád	k1gInPc1	řád
==	==	k?	==
</s>
</p>
<p>
<s>
Františkáni	františkán	k1gMnPc1	františkán
<g/>
–	–	k?	–
zakladatel	zakladatel	k1gMnSc1	zakladatel
svatý	svatý	k1gMnSc1	svatý
František	František	k1gMnSc1	František
z	z	k7c2	z
Assisi	Assise	k1gFnSc4	Assise
<g/>
,	,	kIx,	,
schváleni	schválen	k2eAgMnPc1d1	schválen
1223	[number]	k4	1223
</s>
</p>
<p>
<s>
Dominikáni	dominikán	k1gMnPc1	dominikán
–	–	k?	–
zakladatel	zakladatel	k1gMnSc1	zakladatel
svatý	svatý	k1gMnSc1	svatý
Dominik	Dominik	k1gMnSc1	Dominik
<g/>
,	,	kIx,	,
schváleni	schválen	k2eAgMnPc1d1	schválen
1216	[number]	k4	1216
</s>
</p>
<p>
<s>
Karmelitáni	karmelitán	k1gMnPc1	karmelitán
–	–	k?	–
schváleni	schválit	k5eAaPmNgMnP	schválit
1245	[number]	k4	1245
</s>
</p>
<p>
<s>
Augustiniáni	augustinián	k1gMnPc1	augustinián
–	–	k?	–
schváleni	schválen	k2eAgMnPc1d1	schválen
1256	[number]	k4	1256
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgInPc1d1	další
žebravé	žebravý	k2eAgInPc1d1	žebravý
řády	řád	k1gInPc1	řád
==	==	k?	==
</s>
</p>
<p>
<s>
Trinitáři	trinitář	k1gMnPc1	trinitář
</s>
</p>
<p>
<s>
Servité	servita	k1gMnPc1	servita
</s>
</p>
<p>
<s>
Pauláni	paulán	k1gMnPc1	paulán
</s>
</p>
<p>
<s>
Pavlíni	pavlín	k1gMnPc1	pavlín
</s>
</p>
<p>
<s>
Milosrdní	milosrdný	k2eAgMnPc1d1	milosrdný
bratři	bratr	k1gMnPc1	bratr
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
BUBEN	buben	k1gInSc1	buben
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
<g/>
.	.	kIx.	.
</s>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
řádů	řád	k1gInPc2	řád
<g/>
,	,	kIx,	,
kongregací	kongregace	k1gFnPc2	kongregace
a	a	k8xC	a
řeholních	řeholní	k2eAgFnPc2d1	řeholní
společností	společnost	k1gFnPc2	společnost
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-7277-140-0	[number]	k4	978-80-7277-140-0
</s>
</p>
<p>
<s>
LAWRENCE	LAWRENCE	kA	LAWRENCE
<g/>
,	,	kIx,	,
Hugh	Hugh	k1gInSc1	Hugh
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
středověkého	středověký	k2eAgNnSc2d1	středověké
mnišství	mnišství	k1gNnSc2	mnišství
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-85959-88-7	[number]	k4	80-85959-88-7
</s>
</p>
