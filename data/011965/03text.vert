<p>
<s>
Účastníci	účastník	k1gMnPc1	účastník
zájezdu	zájezd	k1gInSc2	zájezd
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc4d1	český
film	film	k1gInSc4	film
natočený	natočený	k2eAgInSc4d1	natočený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
podle	podle	k7c2	podle
románu	román	k1gInSc2	román
Michala	Michal	k1gMnSc4	Michal
Viewegha	Viewegh	k1gMnSc4	Viewegh
Účastníci	účastník	k1gMnPc1	účastník
zájezdu	zájezd	k1gInSc2	zájezd
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
prázdninové	prázdninový	k2eAgFnSc6d1	prázdninová
cestě	cesta	k1gFnSc6	cesta
několika	několik	k4yIc2	několik
náhodně	náhodně	k6eAd1	náhodně
se	se	k3xPyFc4	se
setkavších	setkavší	k2eAgMnPc2d1	setkavší
lidí	člověk	k1gMnPc2	člověk
k	k	k7c3	k
moři	moře	k1gNnSc3	moře
do	do	k7c2	do
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Účastníci	účastník	k1gMnPc1	účastník
zájezdu	zájezd	k1gInSc2	zájezd
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Účastníci	účastník	k1gMnPc1	účastník
zájezdu	zájezd	k1gInSc2	zájezd
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Účastníci	účastník	k1gMnPc1	účastník
zájezdu	zájezd	k1gInSc2	zájezd
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
