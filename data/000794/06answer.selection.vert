<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
společnosti	společnost	k1gFnSc2	společnost
je	být	k5eAaImIp3nS	být
The	The	k1gMnSc7	The
National	National	k1gFnPc2	National
Railroad	Railroad	k1gInSc1	Railroad
Passenger	Passenger	k1gMnSc1	Passenger
Corporation	Corporation	k1gInSc1	Corporation
<g/>
,	,	kIx,	,
značka	značka	k1gFnSc1	značka
Amtrak	Amtrak	k1gInSc1	Amtrak
<g/>
,	,	kIx,	,
pod	pod	k7c4	pod
kterou	který	k3yQgFnSc4	který
společnost	společnost	k1gFnSc4	společnost
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
působí	působit	k5eAaImIp3nS	působit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
složeninou	složenina	k1gFnSc7	složenina
slov	slovo	k1gNnPc2	slovo
"	"	kIx"	"
<g/>
America	Americ	k2eAgFnSc1d1	America
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
track	track	k1gInSc1	track
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
