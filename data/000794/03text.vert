<s>
Amtrak	Amtrak	k6eAd1	Amtrak
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
státní	státní	k2eAgFnSc1d1	státní
železniční	železniční	k2eAgFnSc1d1	železniční
společnost	společnost	k1gFnSc1	společnost
provozující	provozující	k2eAgFnSc4d1	provozující
osobní	osobní	k2eAgFnSc4d1	osobní
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
společnosti	společnost	k1gFnSc2	společnost
je	být	k5eAaImIp3nS	být
The	The	k1gMnSc7	The
National	National	k1gFnPc2	National
Railroad	Railroad	k1gInSc1	Railroad
Passenger	Passenger	k1gMnSc1	Passenger
Corporation	Corporation	k1gInSc1	Corporation
<g/>
,	,	kIx,	,
značka	značka	k1gFnSc1	značka
Amtrak	Amtrak	k1gInSc1	Amtrak
<g/>
,	,	kIx,	,
pod	pod	k7c4	pod
kterou	který	k3yIgFnSc4	který
společnost	společnost	k1gFnSc4	společnost
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
působí	působit	k5eAaImIp3nS	působit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
složeninou	složenina	k1gFnSc7	složenina
slov	slovo	k1gNnPc2	slovo
"	"	kIx"	"
<g/>
America	Americ	k2eAgFnSc1d1	America
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
track	track	k1gInSc1	track
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
sídlí	sídlet	k5eAaImIp3nS	sídlet
na	na	k7c4	na
Union	union	k1gInSc4	union
Station	station	k1gInSc4	station
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
D.C.	D.C.	k1gFnSc2	D.C.
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
převzala	převzít	k5eAaPmAgFnS	převzít
dálkovou	dálkový	k2eAgFnSc4d1	dálková
osobní	osobní	k2eAgFnSc4d1	osobní
dopravu	doprava	k1gFnSc4	doprava
od	od	k7c2	od
26	[number]	k4	26
železničních	železniční	k2eAgFnPc2d1	železniční
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
obsluhuje	obsluhovat	k5eAaImIp3nS	obsluhovat
34	[number]	k4	34
000	[number]	k4	000
km	km	kA	km
linek	linka	k1gFnPc2	linka
spojujících	spojující	k2eAgFnPc2d1	spojující
500	[number]	k4	500
nádraží	nádraží	k1gNnPc2	nádraží
ve	v	k7c6	v
46	[number]	k4	46
amerických	americký	k2eAgInPc6d1	americký
státech	stát	k1gInPc6	stát
a	a	k8xC	a
3	[number]	k4	3
kanadských	kanadský	k2eAgFnPc6d1	kanadská
provinciích	provincie	k1gFnPc6	provincie
<g/>
,	,	kIx,	,
přímo	přímo	k6eAd1	přímo
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
jen	jen	k9	jen
1	[number]	k4	1
175	[number]	k4	175
km	km	kA	km
tratí	trať	k1gFnPc2	trať
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
přes	přes	k7c4	přes
20	[number]	k4	20
000	[number]	k4	000
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
přepravila	přepravit	k5eAaPmAgFnS	přepravit
31,2	[number]	k4	31,2
milionu	milion	k4xCgInSc2	milion
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Nejrušnější	rušný	k2eAgFnSc7d3	nejrušnější
trasou	trasa	k1gFnSc7	trasa
je	být	k5eAaImIp3nS	být
plně	plně	k6eAd1	plně
elektrifikovaný	elektrifikovaný	k2eAgInSc1d1	elektrifikovaný
Severovýchodní	severovýchodní	k2eAgInSc1d1	severovýchodní
koridor	koridor	k1gInSc1	koridor
<g/>
,	,	kIx,	,
spojující	spojující	k2eAgInSc1d1	spojující
Washington	Washington	k1gInSc1	Washington
D.C.	D.C.	k1gFnSc2	D.C.
a	a	k8xC	a
Boston	Boston	k1gInSc1	Boston
(	(	kIx(	(
<g/>
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
leží	ležet	k5eAaImIp3nS	ležet
i	i	k9	i
Baltimore	Baltimore	k1gInSc1	Baltimore
<g/>
,	,	kIx,	,
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
či	či	k8xC	či
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
přepravováno	přepravovat	k5eAaImNgNnS	přepravovat
asi	asi	k9	asi
10	[number]	k4	10
milionu	milion	k4xCgInSc2	milion
cestujících	cestující	k1gMnPc2	cestující
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
třetina	třetina	k1gFnSc1	třetina
výkonů	výkon	k1gInPc2	výkon
celého	celý	k2eAgInSc2d1	celý
Amtraku	Amtrak	k1gInSc2	Amtrak
<g/>
)	)	kIx)	)
a	a	k8xC	a
jezdí	jezdit	k5eAaImIp3nP	jezdit
zde	zde	k6eAd1	zde
i	i	k9	i
rychlovlak	rychlovlak	k1gInSc1	rychlovlak
Acela	Acelo	k1gNnSc2	Acelo
vyráběný	vyráběný	k2eAgInSc1d1	vyráběný
společností	společnost	k1gFnSc7	společnost
Bombardier	Bombardira	k1gFnPc2	Bombardira
a	a	k8xC	a
Alstom	Alstom	k1gInSc1	Alstom
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Amtrak	Amtrak	k1gInSc4	Amtrak
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
webové	webový	k2eAgFnSc2d1	webová
stránky	stránka	k1gFnSc2	stránka
</s>
