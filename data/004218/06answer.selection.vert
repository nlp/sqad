<s>
Protektor	protektor	k1gInSc1	protektor
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Marka	Marek	k1gMnSc2	Marek
Najbrta	Najbrt	k1gMnSc2	Najbrt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
získal	získat	k5eAaPmAgInS	získat
šest	šest	k4xCc4	šest
Českých	český	k2eAgInPc2d1	český
lvů	lev	k1gInPc2	lev
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
režii	režie	k1gFnSc4	režie
<g/>
,	,	kIx,	,
scénář	scénář	k1gInSc4	scénář
<g/>
,	,	kIx,	,
střih	střih	k1gInSc4	střih
<g/>
,	,	kIx,	,
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
hlavní	hlavní	k2eAgFnSc4d1	hlavní
ženskou	ženský	k2eAgFnSc4d1	ženská
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
