<s>
Letectví	letectví	k1gNnSc1
</s>
<s>
Letiště	letiště	k1gNnSc1
Londýn	Londýn	k1gInSc1
Heathrow	Heathrow	k1gFnSc7
ze	z	k7c2
vzlétajícího	vzlétající	k2eAgNnSc2d1
letadla	letadlo	k1gNnSc2
</s>
<s>
Pojem	pojem	k1gInSc1
letectví	letectví	k1gNnSc2
či	či	k8xC
aviatika	aviatika	k1gFnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
létání	létání	k1gNnSc2
letadly	letadlo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dnešní	dnešní	k2eAgFnSc6d1
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
většina	většina	k1gFnSc1
letadel	letadlo	k1gNnPc2
je	být	k5eAaImIp3nS
těžších	těžký	k2eAgInPc2d2
než	než	k8xS
vzduch	vzduch	k1gInSc1
<g/>
,	,	kIx,
jde	jít	k5eAaImIp3nS
o	o	k7c6
létání	létání	k1gNnSc6
letounů	letoun	k1gInPc2
a	a	k8xC
vrtulníků	vrtulník	k1gInPc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
se	se	k3xPyFc4
tento	tento	k3xDgInSc1
obor	obor	k1gInSc1
nazývá	nazývat	k5eAaImIp3nS
aviation	aviation	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
počátku	počátek	k1gInSc6
letectví	letectví	k1gNnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
po	po	k7c6
prvním	první	k4xOgInSc6
vzletu	vzlet	k1gInSc6
horkovzdušného	horkovzdušný	k2eAgInSc2d1
balónu	balón	k1gInSc2
bratrů	bratr	k1gMnPc2
Montgolfiérů	Montgolfiér	k1gInPc2
ve	v	k7c6
Francii	Francie	k1gFnSc6
byla	být	k5eAaImAgFnS
známa	znám	k2eAgFnSc1d1
jen	jen	k9
letadla	letadlo	k1gNnPc1
lehčí	lehčit	k5eAaImIp3nP
vzduchu	vzduch	k1gInSc2
<g/>
,	,	kIx,
tj.	tj.	kA
balóny	balón	k1gInPc4
a	a	k8xC
vzducholodě	vzducholoď	k1gFnPc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
používalo	používat	k5eAaImAgNnS
v	v	k7c6
češtině	čeština	k1gFnSc6
termínu	termín	k1gInSc2
„	„	k?
<g/>
aeronautika	aeronautika	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
vzduchoplavectví	vzduchoplavectví	k1gNnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dnešní	dnešní	k2eAgFnSc6d1
době	doba	k1gFnSc6
se	se	k3xPyFc4
v	v	k7c6
rámci	rámec	k1gInSc6
Chicagské	chicagský	k2eAgFnSc2d1
dohody	dohoda	k1gFnSc2
o	o	k7c6
mezinárodní	mezinárodní	k2eAgFnSc6d1
civilní	civilní	k2eAgFnSc6d1
letecké	letecký	k2eAgFnSc6d1
dopravě	doprava	k1gFnSc6
a	a	k8xC
po	po	k7c6
vzniku	vznik	k1gInSc6
ICAO	ICAO	kA
používají	používat	k5eAaImIp3nP
termíny	termín	k1gInPc1
aviation	aviation	k1gInSc1
(	(	kIx(
<g/>
pro	pro	k7c4
létání	létání	k1gNnSc4
letadel	letadlo	k1gNnPc2
těžších	těžký	k2eAgFnPc2d2
vzduchu	vzduch	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
aeronautics	aeronautics	k6eAd1
(	(	kIx(
<g/>
pro	pro	k7c4
veškerá	veškerý	k3xTgNnPc4
letadla	letadlo	k1gNnPc4
pohybující	pohybující	k2eAgFnPc1d1
se	se	k3xPyFc4
v	v	k7c6
zemské	zemský	k2eAgFnSc6d1
atmosféře	atmosféra	k1gFnSc6
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
už	už	k9
pomocí	pomocí	k7c2
aerostatických	aerostatický	k2eAgFnPc2d1
sil	síla	k1gFnPc2
–	–	k?
balóny	balón	k1gInPc1
<g/>
,	,	kIx,
vzducholodě	vzducholoď	k1gFnPc1
–	–	k?
nebo	nebo	k8xC
pomocí	pomocí	k7c2
aerodynamických	aerodynamický	k2eAgFnPc2d1
sil	síla	k1gFnPc2
–	–	k?
letouny	letoun	k1gInPc1
<g/>
,	,	kIx,
vrtulníky	vrtulník	k1gInPc1
<g/>
,	,	kIx,
padáky	padák	k1gInPc1
<g/>
,	,	kIx,
ultralehká	ultralehký	k2eAgNnPc1d1
letadla	letadlo	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s>
Český	český	k2eAgInSc1d1
termín	termín	k1gInSc1
„	„	k?
<g/>
letectví	letectví	k1gNnSc2
<g/>
“	“	k?
zahrnuje	zahrnovat	k5eAaImIp3nS
také	také	k9
obor	obor	k1gInSc1
aerospace	aerospace	k1gFnSc2
<g/>
,	,	kIx,
aerokosmonautiku	aerokosmonautikum	k1gNnSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
letectví	letectví	k1gNnSc4
a	a	k8xC
kosmonautiku	kosmonautika	k1gFnSc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
letadla	letadlo	k1gNnSc2
typu	typ	k1gInSc2
raketoplán	raketoplán	k1gInSc1
(	(	kIx(
<g/>
v	v	k7c6
kosmu	kosmos	k1gInSc6
létá	létat	k5eAaImIp3nS
jako	jako	k9
kosmická	kosmický	k2eAgFnSc1d1
loď	loď	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
atmosféře	atmosféra	k1gFnSc6
jako	jako	k9
letoun	letoun	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
zčásti	zčásti	k6eAd1
nebo	nebo	k8xC
zcela	zcela	k6eAd1
létají	létat	k5eAaImIp3nP
v	v	k7c6
kosmickém	kosmický	k2eAgInSc6d1
prostoru	prostor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Historie	historie	k1gFnSc2
letectví	letectví	k1gNnSc1
<g/>
.	.	kIx.
<g/>
První	první	k4xOgInPc1
opravdové	opravdový	k2eAgInPc1d1
pokusy	pokus	k1gInPc1
o	o	k7c4
let	let	k1gInSc4
se	se	k3xPyFc4
patrně	patrně	k6eAd1
uskutečnily	uskutečnit	k5eAaPmAgInP
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
pomocí	pomoc	k1gFnSc7
draků	drak	k1gMnPc2
<g/>
,	,	kIx,
především	především	k9
pro	pro	k7c4
vojenské	vojenský	k2eAgInPc4d1
účely	účel	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
válkách	válka	k1gFnPc6
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
probíhaly	probíhat	k5eAaImAgFnP
v	v	k7c6
5	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
3	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
se	se	k3xPyFc4
podle	podle	k7c2
pověsti	pověst	k1gFnSc2
nechal	nechat	k5eAaPmAgMnS
čínský	čínský	k2eAgMnSc1d1
generál	generál	k1gMnSc1
vynést	vynést	k5eAaPmF
na	na	k7c6
draku	drak	k1gInSc6
nad	nad	k7c4
nepřátelský	přátelský	k2eNgInSc4d1
tábor	tábor	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
zpíval	zpívat	k5eAaImAgMnS
písně	píseň	k1gFnPc4
z	z	k7c2
domoviny	domovina	k1gFnSc2
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
u	u	k7c2
nich	on	k3xPp3gMnPc2
vyvolal	vyvolat	k5eAaPmAgInS
stesk	stesk	k1gInSc4
po	po	k7c6
domově	domov	k1gInSc6
a	a	k8xC
podkopal	podkopat	k5eAaPmAgMnS
jejich	jejich	k3xOp3gFnSc4
morálku	morálka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zda	zda	k8xS
se	se	k3xPyFc4
tato	tento	k3xDgFnSc1
pověst	pověst	k1gFnSc1
zakládá	zakládat	k5eAaImIp3nS
na	na	k7c6
pravdě	pravda	k1gFnSc6
<g/>
,	,	kIx,
nevíme	vědět	k5eNaImIp1nP
<g/>
.	.	kIx.
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
První	první	k4xOgInPc1
vážné	vážný	k2eAgInPc1d1
pokusy	pokus	k1gInPc1
létat	létat	k5eAaImF
se	se	k3xPyFc4
konaly	konat	k5eAaImAgInP
v	v	k7c6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
s	s	k7c7
horkovzdušnými	horkovzdušný	k2eAgInPc7d1
balony	balon	k1gInPc7
-	-	kIx~
Bratři	bratřit	k5eAaImRp2nS
Montgolfierové	Montgolfierové	k2eAgInSc1d1
roku	rok	k1gInSc2
1783	#num#	k4
vypustili	vypustit	k5eAaPmAgMnP
balon	balon	k1gInSc4
o	o	k7c6
průměru	průměr	k1gInSc6
přes	přes	k7c4
10	#num#	k4
m	m	kA
na	na	k7c6
jehož	jehož	k3xOyRp3gFnSc6
palubě	paluba	k1gFnSc6
byla	být	k5eAaImAgFnS
ovce	ovce	k1gFnSc1
<g/>
,	,	kIx,
husa	husa	k1gFnSc1
a	a	k8xC
kohout	kohout	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
tentýž	týž	k3xTgInSc4
rok	rok	k1gInSc4
se	se	k3xPyFc4
vznesl	vznést	k5eAaPmAgMnS
i	i	k9
první	první	k4xOgInSc4
balon	balon	k1gInSc4
s	s	k7c7
lidskou	lidský	k2eAgFnSc7d1
posádkou	posádka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc4
řiditelnou	řiditelný	k2eAgFnSc4d1
vzducholoď	vzducholoď	k1gFnSc4
postavil	postavit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1852	#num#	k4
Francouz	Francouz	k1gMnSc1
Henri	Henre	k1gFnSc4
Giffard	Giffarda	k1gFnPc2
<g/>
,	,	kIx,
její	její	k3xOp3gInSc1
parní	parní	k2eAgInSc1d1
motor	motor	k1gInSc1
jí	on	k3xPp3gFnSc7
však	však	k9
neposkytoval	poskytovat	k5eNaImAgInS
dostatečný	dostatečný	k2eAgInSc1d1
výkon	výkon	k1gInSc1
v	v	k7c6
poměru	poměr	k1gInSc6
k	k	k7c3
váze	váha	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
byly	být	k5eAaImAgFnP
stavěny	stavěn	k2eAgFnPc1d1
vzducholodě	vzducholoď	k1gFnPc1
s	s	k7c7
celokovovou	celokovový	k2eAgFnSc7d1
kostrou	kostra	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
jejich	jejich	k3xOp3gInSc4
rozvoj	rozvoj	k1gInSc4
se	se	k3xPyFc4
zasloužil	zasloužit	k5eAaPmAgMnS
především	především	k9
německý	německý	k2eAgMnSc1d1
hrabě	hrabě	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
von	von	k1gInSc4
Zeppelin	Zeppelin	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s>
Letouny	letoun	k1gInPc1
těžší	těžký	k2eAgFnSc2d2
než	než	k8xS
vzduch	vzduch	k1gInSc1
se	se	k3xPyFc4
začaly	začít	k5eAaPmAgFnP
objevovat	objevovat	k5eAaImF
až	až	k9
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
vynálezem	vynález	k1gInSc7
motoru	motor	k1gInSc2
s	s	k7c7
vnitřním	vnitřní	k2eAgNnSc7d1
spalováním	spalování	k1gNnSc7
to	ten	k3xDgNnSc1
byly	být	k5eAaImAgFnP
především	především	k9
kluzáky	kluzák	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
vynálezem	vynález	k1gInSc7
zážehového	zážehový	k2eAgInSc2d1
motoru	motor	k1gInSc2
na	na	k7c6
konci	konec	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
otevřela	otevřít	k5eAaPmAgFnS
cesta	cesta	k1gFnSc1
pro	pro	k7c4
motorová	motorový	k2eAgNnPc4d1
letadla	letadlo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průkopníky	průkopník	k1gMnPc7
v	v	k7c6
této	tento	k3xDgFnSc6
oblasti	oblast	k1gFnSc6
byli	být	k5eAaImAgMnP
bratři	bratr	k1gMnPc1
Wrightové	Wrightová	k1gFnSc2
<g/>
.	.	kIx.
14	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
roku	rok	k1gInSc2
1903	#num#	k4
poprvé	poprvé	k6eAd1
vzlétli	vzlétnout	k5eAaPmAgMnP
s	s	k7c7
motorovým	motorový	k2eAgNnSc7d1
letadlem	letadlo	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yQgInPc4,k3yRgInPc4
pojmenovali	pojmenovat	k5eAaPmAgMnP
Flyer	Flyer	k1gInSc4
<g/>
,	,	kIx,
let	let	k1gInSc4
však	však	k9
nebyl	být	k5eNaImAgInS
úspěšný	úspěšný	k2eAgInSc1d1
a	a	k8xC
letoun	letoun	k1gInSc1
se	se	k3xPyFc4
poškodil	poškodit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
tři	tři	k4xCgInPc4
dny	den	k1gInPc4
později	pozdě	k6eAd2
byl	být	k5eAaImAgInS
proveden	provést	k5eAaPmNgInS
druhý	druhý	k4xOgInSc1
<g/>
,	,	kIx,
úspěšnější	úspěšný	k2eAgInSc1d2
pokus	pokus	k1gInSc1
<g/>
,	,	kIx,
při	při	k7c6
kterém	který	k3yQgInSc6,k3yRgInSc6,k3yIgInSc6
stroj	stroj	k1gInSc4
letěl	letět	k5eAaImAgMnS
12	#num#	k4
s.	s.	k?
</s>
<s>
V	v	k7c6
první	první	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
rozvoji	rozvoj	k1gInSc3
leteckých	letecký	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
<g/>
,	,	kIx,
poprvé	poprvé	k6eAd1
se	se	k3xPyFc4
také	také	k9
odehrávaly	odehrávat	k5eAaImAgInP
vzdušné	vzdušný	k2eAgInPc1d1
souboje	souboj	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejúspěšnějším	úspěšný	k2eAgMnSc7d3
stíhačem	stíhač	k1gMnSc7
byl	být	k5eAaImAgMnS
Manfred	Manfred	k1gMnSc1
von	von	k1gInSc4
Richthofen	Richthofen	k2eAgMnSc1d1
<g/>
,	,	kIx,
zvaný	zvaný	k2eAgMnSc1d1
Rudý	rudý	k1gMnSc1
baron	baron	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
byly	být	k5eAaImAgFnP
do	do	k7c2
sériové	sériový	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
zavedeny	zaveden	k2eAgFnPc1d1
a	a	k8xC
v	v	k7c6
malém	malý	k2eAgNnSc6d1
množství	množství	k1gNnSc6
poprvé	poprvé	k6eAd1
bojově	bojově	k6eAd1
nasazeny	nasazen	k2eAgInPc1d1
vrtulníky	vrtulník	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrtulová	vrtulový	k2eAgNnPc4d1
letadla	letadlo	k1gNnPc4
už	už	k9
dosáhla	dosáhnout	k5eAaPmAgFnS
hranice	hranice	k1gFnSc1
svých	svůj	k3xOyFgFnPc2
možností	možnost	k1gFnPc2
<g/>
,	,	kIx,
protože	protože	k8xS
při	při	k7c6
rychlostech	rychlost	k1gFnPc6
přes	přes	k7c4
700	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
ztrácejí	ztrácet	k5eAaImIp3nP
vrtule	vrtule	k1gFnPc1
účinnost	účinnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řešením	řešení	k1gNnSc7
bylo	být	k5eAaImAgNnS
použití	použití	k1gNnSc1
proudového	proudový	k2eAgInSc2d1
motoru	motor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
první	první	k4xOgFnSc4
postavilo	postavit	k5eAaPmAgNnS
proudový	proudový	k2eAgInSc4d1
letoun	letoun	k1gInSc4
Německo	Německo	k1gNnSc1
v	v	k7c6
roce	rok	k1gInSc6
1939	#num#	k4
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
to	ten	k3xDgNnSc1
Heinkel	Heinkel	k1gInSc1
He	he	k0
178	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Letectví	letectví	k1gNnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
</s>
<s>
letecký	letecký	k2eAgInSc1d1
provoz	provoz	k1gInSc1
a	a	k8xC
řízení	řízení	k1gNnSc1
letového	letový	k2eAgInSc2d1
provozu	provoz	k1gInSc2
</s>
<s>
letecký	letecký	k2eAgInSc1d1
technický	technický	k2eAgInSc1d1
servis	servis	k1gInSc1
</s>
<s>
službu	služba	k1gFnSc4
letecké	letecký	k2eAgFnSc2d1
meteorologie	meteorologie	k1gFnSc2
</s>
<s>
letecký	letecký	k2eAgInSc1d1
průmysl	průmysl	k1gInSc1
</s>
<s>
letecké	letecký	k2eAgNnSc4d1
školství	školství	k1gNnSc4
</s>
<s>
letecké	letecký	k2eAgFnPc1d1
školy	škola	k1gFnPc1
</s>
<s>
aerodynamiku	aerodynamika	k1gFnSc4
</s>
<s>
leteckou	letecký	k2eAgFnSc4d1
medicínu	medicína	k1gFnSc4
</s>
<s>
všeobecné	všeobecný	k2eAgNnSc1d1
letectví	letectví	k1gNnSc1
</s>
<s>
komerční	komerční	k2eAgNnSc1d1
letectví	letectví	k1gNnSc1
</s>
<s>
civilní	civilní	k2eAgNnPc1d1
letectví	letectví	k1gNnPc1
</s>
<s>
letecký	letecký	k2eAgInSc1d1
sport	sport	k1gInSc1
</s>
<s>
vojenské	vojenský	k2eAgNnSc1d1
letectví	letectví	k1gNnSc1
</s>
<s>
správu	správa	k1gFnSc4
a	a	k8xC
výstavbu	výstavba	k1gFnSc4
letišť	letiště	k1gNnPc2
</s>
<s>
Letecký	letecký	k2eAgInSc1d1
provoz	provoz	k1gInSc1
v	v	k7c6
číslech	číslo	k1gNnPc6
</s>
<s>
Letecký	letecký	k2eAgInSc1d1
provoz	provoz	k1gInSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
dopravu	doprava	k1gFnSc4
asi	asi	k9
9,5	9,5	k4
milionu	milion	k4xCgInSc2
lidí	člověk	k1gMnPc2
denně	denně	k6eAd1
(	(	kIx(
<g/>
přibližně	přibližně	k6eAd1
tolik	tolik	k6eAd1
jako	jako	k8xS,k8xC
za	za	k7c4
celý	celý	k2eAgInSc4d1
rok	rok	k1gInSc4
1947	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
má	mít	k5eAaImIp3nS
roční	roční	k2eAgFnSc4d1
spotřebu	spotřeba	k1gFnSc4
energie	energie	k1gFnSc2
přibližně	přibližně	k6eAd1
56	#num#	k4
TWh	TWh	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
světě	svět	k1gInSc6
existuje	existovat	k5eAaImIp3nS
přes	přes	k7c4
49	#num#	k4
000	#num#	k4
letišť	letiště	k1gNnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
zdaleka	zdaleka	k6eAd1
nejvíce	nejvíce	k6eAd1,k6eAd3
(	(	kIx(
<g/>
asi	asi	k9
15	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
leží	ležet	k5eAaImIp3nS
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
amerických	americký	k2eAgFnPc2d1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
nad	nad	k7c7
jejichž	jejichž	k3xOyRp3gInSc7
vzdušným	vzdušný	k2eAgInSc7d1
prostorem	prostor	k1gInSc7
se	se	k3xPyFc4
každou	každý	k3xTgFnSc4
sekundu	sekunda	k1gFnSc4
pohybují	pohybovat	k5eAaImIp3nP
až	až	k9
4000	#num#	k4
letounů	letoun	k1gInPc2
<g/>
,	,	kIx,
přenášejících	přenášející	k2eAgInPc2d1
61	#num#	k4
000	#num#	k4
cestujících	cestující	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
]	]	kIx)
Nejrušnější	rušný	k2eAgNnPc4d3
letiště	letiště	k1gNnPc4
mají	mít	k5eAaImIp3nP
města	město	k1gNnPc1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
Atlanta	Atlanta	k1gFnSc1
<g/>
,	,	kIx,
Chicago	Chicago	k1gNnSc1
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
<g/>
,	,	kIx,
Tokio	Tokio	k1gNnSc1
<g/>
,	,	kIx,
Los	los	k1gInSc1
Angeles	Angeles	k1gInSc1
<g/>
,	,	kIx,
Dubaj	Dubaj	k1gInSc1
nebo	nebo	k8xC
třeba	třeba	k6eAd1
Paříž	Paříž	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Letiště	letiště	k1gNnSc1
<g/>
,	,	kIx,
na	na	k7c6
kterých	který	k3yQgInPc6,k3yRgInPc6,k3yIgInPc6
se	se	k3xPyFc4
vymění	vyměnit	k5eAaPmIp3nP
nejvíce	hodně	k6eAd3,k6eAd1
nákladu	náklad	k1gInSc2
<g/>
,	,	kIx,
leží	ležet	k5eAaImIp3nS
na	na	k7c6
okraji	okraj	k1gInSc6
Memphisu	Memphis	k1gInSc2
a	a	k8xC
Hongkongu	Hongkong	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
kdy	kdy	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
Letecká	letecký	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
nejbezpečnější	bezpečný	k2eAgInPc4d3
druhy	druh	k1gInPc4
dopravy	doprava	k1gFnSc2
<g/>
;	;	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
zemřelo	zemřít	k5eAaPmAgNnS
při	při	k7c6
leteckých	letecký	k2eAgFnPc6d1
nehodách	nehoda	k1gFnPc6
539	#num#	k4
osob	osoba	k1gFnPc2
<g/>
,	,	kIx,
jedno	jeden	k4xCgNnSc4
úmrtí	úmrtí	k1gNnSc4
připadá	připadat	k5eAaPmIp3nS,k5eAaImIp3nS
tedy	tedy	k9
na	na	k7c4
každý	každý	k3xTgInSc4
1,3	1,3	k4
<g/>
miliontý	miliontý	k4xOgInSc4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Při	při	k7c6
leteckých	letecký	k2eAgFnPc6d1
nehodách	nehoda	k1gFnPc6
tedy	tedy	k8xC
zemřelo	zemřít	k5eAaPmAgNnS
méně	málo	k6eAd2
lidí	člověk	k1gMnPc2
než	než	k8xS
na	na	k7c6
českých	český	k2eAgFnPc6d1
silnicích	silnice	k1gFnPc6
za	za	k7c4
daný	daný	k2eAgInSc4d1
rok	rok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jízda	jízda	k1gFnSc1
autem	auto	k1gNnSc7
je	být	k5eAaImIp3nS
62	#num#	k4
<g/>
krát	krát	k6eAd1
nebezpečnější	bezpečný	k2eNgFnSc1d2
než	než	k8xS
přeprava	přeprava	k1gFnSc1
letadlem	letadlo	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Komerční	komerční	k2eAgInPc4d1
lety	let	k1gInPc4
jsou	být	k5eAaImIp3nP
stále	stále	k6eAd1
bezpečnější	bezpečný	k2eAgFnPc1d2
a	a	k8xC
riziko	riziko	k1gNnSc1
se	se	k3xPyFc4
snižuje	snižovat	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
dvakrát	dvakrát	k6eAd1
každých	každý	k3xTgNnPc2
deset	deset	k4xCc4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Environmentální	environmentální	k2eAgInPc4d1
dopady	dopad	k1gInPc4
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Environmentální	environmentální	k2eAgInPc4d1
dopady	dopad	k1gInPc4
letectví	letectví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
environmentálním	environmentální	k2eAgInPc3d1
dopadům	dopad	k1gInPc3
letectví	letectví	k1gNnSc2
dochází	docházet	k5eAaImIp3nS
<g/>
,	,	kIx,
protože	protože	k8xS
letecké	letecký	k2eAgInPc1d1
motory	motor	k1gInPc1
emitují	emitovat	k5eAaBmIp3nP
teplo	teplo	k1gNnSc4
<g/>
,	,	kIx,
hluk	hluk	k1gInSc1
<g/>
,	,	kIx,
prachové	prachový	k2eAgFnPc1d1
částice	částice	k1gFnPc1
a	a	k8xC
plyny	plyn	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
přispívají	přispívat	k5eAaImIp3nP
ke	k	k7c3
globálnímu	globální	k2eAgNnSc3d1
oteplování	oteplování	k1gNnSc3
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
ke	k	k7c3
globálnímu	globální	k2eAgNnSc3d1
stmívání	stmívání	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
]	]	kIx)
Letadla	letadlo	k1gNnPc1
vypouštějí	vypouštět	k5eAaImIp3nP
částice	částice	k1gFnPc1
a	a	k8xC
plyny	plyn	k1gInPc1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
oxid	oxid	k1gInSc4
uhličitý	uhličitý	k2eAgInSc4d1
(	(	kIx(
<g/>
CO	co	k9
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vodní	vodní	k2eAgFnSc1d1
pára	pára	k1gFnSc1
<g/>
,	,	kIx,
uhlovodíky	uhlovodík	k1gInPc1
<g/>
,	,	kIx,
oxid	oxid	k1gInSc1
uhelnatý	uhelnatý	k2eAgInSc1d1
<g/>
,	,	kIx,
oxidy	oxid	k1gInPc1
dusíku	dusík	k1gInSc2
<g/>
,	,	kIx,
oxidy	oxid	k1gInPc1
síry	síra	k1gFnSc2
<g/>
,	,	kIx,
olovo	olovo	k1gNnSc1
a	a	k8xC
černý	černý	k2eAgInSc1d1
uhlík	uhlík	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
navzájem	navzájem	k6eAd1
a	a	k8xC
s	s	k7c7
atmosférou	atmosféra	k1gFnSc7
reagují	reagovat	k5eAaBmIp3nP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Navzdory	navzdory	k7c3
snížení	snížení	k1gNnSc3
emisí	emise	k1gFnPc2
z	z	k7c2
automobilů	automobil	k1gInPc2
a	a	k8xC
úspornějším	úsporný	k2eAgMnPc3d2
a	a	k8xC
méně	málo	k6eAd2
znečišťujícím	znečišťující	k2eAgInPc3d1
dvouproudovým	dvouproudový	k2eAgInPc3d1
a	a	k8xC
turbovrtulovým	turbovrtulový	k2eAgInPc3d1
motorům	motor	k1gInPc3
přispívá	přispívat	k5eAaImIp3nS
rychlý	rychlý	k2eAgInSc4d1
růst	růst	k1gInSc4
letecké	letecký	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
v	v	k7c6
uplynulých	uplynulý	k2eAgNnPc6d1
letech	léto	k1gNnPc6
k	k	k7c3
nárůstu	nárůst	k1gInSc3
celkového	celkový	k2eAgNnSc2d1
znečištění	znečištění	k1gNnSc2
způsobeného	způsobený	k2eAgInSc2d1
letectvím	letectví	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1992	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
rostly	růst	k5eAaImAgInP
osobokilometry	osobokilometr	k1gInPc1
o	o	k7c4
5,2	5,2	k4
%	%	kIx~
ročně	ročně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Evropské	evropský	k2eAgFnSc6d1
unii	unie	k1gFnSc6
vzrostly	vzrůst	k5eAaPmAgFnP
emise	emise	k1gFnPc1
skleníkových	skleníkový	k2eAgInPc2d1
plynů	plyn	k1gInPc2
z	z	k7c2
letectví	letectví	k1gNnSc2
mezi	mezi	k7c7
lety	léto	k1gNnPc7
1990	#num#	k4
a	a	k8xC
2006	#num#	k4
o	o	k7c4
87	#num#	k4
procent	procento	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Komplexní	komplexní	k2eAgInSc1d1
výzkum	výzkum	k1gInSc1
ukazuje	ukazovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
navzdory	navzdory	k7c3
očekávaným	očekávaný	k2eAgFnPc3d1
efektivním	efektivní	k2eAgFnPc3d1
inovacím	inovace	k1gFnPc3
letadel	letadlo	k1gNnPc2
<g/>
,	,	kIx,
motorů	motor	k1gInPc2
<g/>
,	,	kIx,
aerodynamiky	aerodynamika	k1gFnSc2
a	a	k8xC
letových	letový	k2eAgFnPc2d1
operací	operace	k1gFnPc2
není	být	k5eNaImIp3nS
v	v	k7c6
dohledu	dohled	k1gInSc6
žádný	žádný	k3yNgInSc1
konec	konec	k1gInSc1
rychlého	rychlý	k2eAgInSc2d1
růstu	růst	k1gInSc2
emisí	emise	k1gFnPc2
CO2	CO2	k1gFnSc2
ani	ani	k8xC
za	za	k7c4
mnoho	mnoho	k4c4
desetiletí	desetiletí	k1gNnPc2
z	z	k7c2
cestování	cestování	k1gNnPc2
letadlem	letadlo	k1gNnSc7
a	a	k8xC
letecké	letecký	k2eAgFnSc2d1
nákladní	nákladní	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
]	]	kIx)
v	v	k7c6
důsledku	důsledek	k1gInSc6
předpokládaného	předpokládaný	k2eAgInSc2d1
neustálého	neustálý	k2eAgInSc2d1
růstu	růst	k1gInSc2
letecké	letecký	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
tomu	ten	k3xDgNnSc3
tak	tak	k6eAd1
i	i	k9
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
mezinárodní	mezinárodní	k2eAgFnPc1d1
letecké	letecký	k2eAgFnPc1d1
emise	emise	k1gFnPc1
unikly	uniknout	k5eAaPmAgFnP
mezinárodní	mezinárodní	k2eAgFnSc4d1
regulaci	regulace	k1gFnSc4
až	až	k9
do	do	k7c2
konference	konference	k1gFnSc2
Mezinárodní	mezinárodní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
pro	pro	k7c4
civilní	civilní	k2eAgNnSc4d1
letectví	letectví	k1gNnSc4
v	v	k7c6
říjnu	říjen	k1gInSc6
2016	#num#	k4
<g/>
,	,	kIx,
na	na	k7c6
níž	jenž	k3xRgFnSc6
se	se	k3xPyFc4
dohodl	dohodnout	k5eAaPmAgInS
systém	systém	k1gInSc1
uhlíkových	uhlíkový	k2eAgFnPc2d1
kompenzací	kompenzace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	dík	k1gInPc1
nízké	nízký	k2eAgInPc1d1
nebo	nebo	k8xC
neexistující	existující	k2eNgFnSc6d1
dani	daň	k1gFnSc6
z	z	k7c2
leteckého	letecký	k2eAgNnSc2d1
paliva	palivo	k1gNnSc2
má	mít	k5eAaImIp3nS
letecká	letecký	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
navíc	navíc	k6eAd1
konkurenční	konkurenční	k2eAgFnSc4d1
výhodu	výhoda	k1gFnSc4
oproti	oproti	k7c3
jiným	jiný	k2eAgInPc3d1
druhům	druh	k1gInPc3
dopravy	doprava	k1gFnSc2
kvůli	kvůli	k7c3
nižším	nízký	k2eAgFnPc3d2
sazbám	sazba	k1gFnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Probíhá	probíhat	k5eAaImIp3nS
debata	debata	k1gFnSc1
o	o	k7c6
možném	možný	k2eAgNnSc6d1
zdanění	zdanění	k1gNnSc6
letecké	letecký	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
a	a	k8xC
začlenění	začlenění	k1gNnSc2
letectví	letectví	k1gNnSc2
do	do	k7c2
systému	systém	k1gInSc2
obchodování	obchodování	k1gNnPc2
s	s	k7c7
emisemi	emise	k1gFnPc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
zajistilo	zajistit	k5eAaPmAgNnS
zohlednění	zohlednění	k1gNnSc1
celkových	celkový	k2eAgInPc2d1
externích	externí	k2eAgInPc2d1
nákladů	náklad	k1gInPc2
letecké	letecký	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
BALEJ	BALEJ	k?
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ion-pairing	Ion-pairing	k1gInSc1
in	in	k?
aqueous	aqueous	k1gInSc1
solutions	solutions	k1gInSc1
of	of	k?
peroxodisulfates	peroxodisulfates	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Acta	Acta	k1gMnSc1
Chimica	Chimica	k1gMnSc1
Slovaca	Slovaca	k1gMnSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
5	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
180	#num#	k4
<g/>
–	–	k?
<g/>
185	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1337	#num#	k4
<g/>
-	-	kIx~
<g/>
978	#num#	k4
<g/>
X.	X.	kA
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.247	10.247	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
v	v	k7c6
<g/>
10188	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
28	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
FRIEDRICH	Friedrich	k1gMnSc1
<g/>
,	,	kIx,
Manuel	Manuel	k1gMnSc1
<g/>
;	;	kIx,
KRUŽÍK	KRUŽÍK	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
;	;	kIx,
VALDMAN	VALDMAN	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Numerical	Numerical	k1gFnPc2
approximation	approximation	k1gInSc4
of	of	k?
von	von	k1gInSc1
Kármán	Kármán	k2eAgInSc1d1
viscoelastic	viscoelastice	k1gFnPc2
plates	platesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Discrete	Discre	k1gNnSc2
&	&	k?
Continuous	Continuous	k1gInSc1
Dynamical	Dynamical	k1gMnSc1
Systems	Systems	k1gInSc1
-	-	kIx~
S.	S.	kA
2018	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
0	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
0	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
0	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1937	#num#	k4
<g/>
-	-	kIx~
<g/>
1179	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.393	10.393	k4
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
dcdss	dcdss	k6eAd1
<g/>
.2020322	.2020322	k4
<g/>
.	.	kIx.
↑	↑	k?
Počítače	počítač	k1gInPc1
mají	mít	k5eAaImIp3nP
stejné	stejný	k2eAgFnPc4d1
emise	emise	k1gFnPc4
jako	jako	k8xC,k8xS
letadla	letadlo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohou	moct	k5eAaImIp3nP
ubrat	ubrat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
The	The	k1gFnPc2
World	Worlda	k1gFnPc2
Factbook	Factbook	k1gInSc1
—	—	k?
Central	Central	k1gMnSc2
Intelligence	Intelligenec	k1gMnSc2
Agency	Agenca	k1gMnSc2
<g/>
.	.	kIx.
www.cia.gov	www.cia.gov	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
The	The	k1gFnSc1
World	World	k1gMnSc1
Factbook	Factbook	k1gInSc1
—	—	k?
Central	Central	k1gMnSc2
Intelligence	Intelligenec	k1gMnSc2
Agency	Agenca	k1gMnSc2
<g/>
.	.	kIx.
www.cia.gov	www.cia.gov	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
humour	humoura	k1gFnPc2
<g/>
.200	.200	k4
<g/>
ok	oka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
<g/>
au	au	k0
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Letecká	letecký	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
stále	stále	k6eAd1
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
nejbezpečnější	bezpečný	k2eAgInSc4d3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Letecké	letecký	k2eAgFnPc1d1
nehody	nehoda	k1gFnPc1
jsou	být	k5eAaImIp3nP
děsivé	děsivý	k2eAgInPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
jízda	jízda	k1gFnSc1
autem	auto	k1gNnSc7
je	být	k5eAaImIp3nS
62	#num#	k4
<g/>
krát	krát	k6eAd1
nebezpečnější	bezpečný	k2eNgFnSc4d2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-03-28	2015-03-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
https://techxplore.com/news/2020-01-commercial-air-safer.html	https://techxplore.com/news/2020-01-commercial-air-safer.html	k1gMnSc1
-	-	kIx~
Commercial	Commercial	k1gMnSc1
air	air	k?
travel	travel	k1gMnSc1
is	is	k?
safer	safer	k1gMnSc1
than	than	k1gMnSc1
ever	ever	k1gMnSc1
<g/>
,	,	kIx,
study	stud	k1gInPc1
finds	finds	k1gInSc1
<g/>
↑	↑	k?
Aircraft	Aircraft	k1gInSc1
Engine	Engin	k1gInSc5
Emissions	Emissions	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Environmental	Environmental	k1gMnSc1
efficiency	efficienca	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Air	Air	k1gFnPc2
Transport	transporta	k1gFnPc2
Action	Action	k1gInSc1
Group	Group	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
TRAVIS	TRAVIS	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
J.	J.	kA
<g/>
;	;	kIx,
Carleton	Carleton	k1gInSc1
<g/>
,	,	kIx,
Andrew	Andrew	k1gMnSc1
M.	M.	kA
&	&	k?
Lauritsen	Lauritsen	k1gInSc1
<g/>
,	,	kIx,
Ryan	Ryan	k1gInSc1
G.	G.	kA
Contrails	Contrails	k1gInSc1
reduce	reduce	k1gFnSc1
daily	dainout	k5eAaPmAgInP,k5eAaBmAgInP,k5eAaImAgInP
temperature	temperatur	k1gMnSc5
range	rangus	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
<g/>
.	.	kIx.
2002	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
418	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
6898	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
601	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
418601	#num#	k4
<g/>
a.	a.	k?
PMID	PMID	kA
12167846	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Archivováno	archivovat	k5eAaBmNgNnS
3	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2006	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
BRASSEUR	BRASSEUR	kA
<g/>
,	,	kIx,
Guy	Guy	k1gMnSc1
P.	P.	kA
<g/>
;	;	kIx,
GUPTA	GUPTA	kA
<g/>
,	,	kIx,
Mohan	Mohan	k1gInSc1
<g/>
;	;	kIx,
ANDERSON	Anderson	k1gMnSc1
<g/>
,	,	kIx,
Bruce	Bruce	k1gMnSc1
E.	E.	kA
<g/>
;	;	kIx,
BALASUBRAMANIAN	BALASUBRAMANIAN	kA
<g/>
,	,	kIx,
Sathya	Sathyum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Impact	Impact	k2eAgInSc1d1
of	of	k?
aviation	aviation	k1gInSc1
on	on	k3xPp3gMnSc1
climate	climat	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
FAA	FAA	kA
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Aviation	Aviation	k1gInSc1
Climate	Climat	k1gInSc5
Change	change	k1gFnPc1
Research	Research	k1gMnSc1
Initiative	Initiativ	k1gInSc5
(	(	kIx(
<g/>
ACCRI	ACCRI	kA
<g/>
)	)	kIx)
Phase	Phase	k1gFnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bulletin	bulletin	k1gInSc1
of	of	k?
the	the	k?
American	American	k1gInSc4
Meteorological	Meteorological	k1gMnSc2
Society	societa	k1gFnSc2
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
97	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
4	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
561	#num#	k4
<g/>
–	–	k?
<g/>
583	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.117	10.117	k4
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
BAMS-D-	BAMS-D-	k1gFnSc1
<g/>
13	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
89.1	89.1	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
EU	EU	kA
press	press	k6eAd1
release	release	k6eAd1
<g/>
:	:	kIx,
Climate	Climat	k1gInSc5
change	change	k1gFnSc1
<g/>
:	:	kIx,
Commission	Commission	k1gInSc1
proposes	proposes	k1gInSc1
bringing	bringing	k1gInSc1
air	air	k?
transport	transport	k1gInSc1
into	into	k1gMnSc1
EU	EU	kA
Emissions	Emissions	k1gInSc1
Trading	Trading	k1gInSc1
Scheme	Schem	k1gInSc5
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
tisková	tiskový	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
<g/>
,	,	kIx,
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Dostupné	dostupný	k2eAgFnSc2d1
on-line	on-lin	k1gInSc5
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
press	pressit	k5eAaPmRp2nS
release	release	k6eAd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bows-Larkin	Bows-Larkin	k1gInSc1
A.	A.	kA
<g/>
,	,	kIx,
Mander	Mander	k1gInSc1
S.	S.	kA
<g/>
,	,	kIx,
Traut	Traut	k2eAgMnSc1d1
M.	M.	kA
<g/>
,	,	kIx,
Anderson	Anderson	k1gMnSc1
K.	K.	kA
<g/>
,	,	kIx,
Wood	Wood	k1gMnSc1
P.	P.	kA
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
.	.	kIx.
</s>
<s desamb="1">
Encyclopedia	Encyclopedium	k1gNnSc2
of	of	k?
aerospace	aerospace	k1gFnSc1
engineering	engineering	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viz	vidět	k5eAaImRp2nS
speciálně	speciálně	k6eAd1
obrázek	obrázek	k1gInSc1
7	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
TIMMIS	TIMMIS	kA
<g/>
,	,	kIx,
A.	A.	kA
<g/>
;	;	kIx,
HODZIC	HODZIC	kA
<g/>
,	,	kIx,
A.	A.	kA
<g/>
;	;	kIx,
KOH	KOH	kA
<g/>
,	,	kIx,
L.	L.	kA
<g/>
;	;	kIx,
BONNER	BONNER	kA
<g/>
,	,	kIx,
M.	M.	kA
Environmental	Environmental	k1gMnSc1
impact	impact	k1gMnSc1
assessment	assessment	k1gMnSc1
of	of	k?
aviation	aviation	k1gInSc1
emission	emission	k1gInSc1
reduction	reduction	k1gInSc1
through	through	k1gInSc1
the	the	k?
implementation	implementation	k1gInSc1
of	of	k?
composite	composit	k1gInSc5
materials	materials	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Int	Int	k1gFnSc1
J	J	kA
Life	Life	k1gFnSc1
Cycle	Cycle	k1gFnSc2
Assess	Assessa	k1gFnPc2
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
20	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
233	#num#	k4
<g/>
–	–	k?
<g/>
243	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
11367	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
14	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
824	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Boeing	boeing	k1gInSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Current	Current	k1gInSc1
Market	market	k1gInSc1
Outlook	Outlook	k1gInSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
–	–	k?
<g/>
2033	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Airbus	airbus	k1gInSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Flying	Flying	k1gInSc1
by	by	kYmCp3nS
Numbers	Numbers	k1gInSc1
<g/>
:	:	kIx,
Global	globat	k5eAaImAgInS
Market	market	k1gInSc1
Forecast	Forecast	k1gFnSc4
2015	#num#	k4
<g/>
–	–	k?
<g/>
2034	#num#	k4
Archivováno	archivován	k2eAgNnSc4d1
15	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Study	stud	k1gInPc4
<g/>
:	:	kIx,
Aviation	Aviation	k1gInSc4
tax	taxa	k1gFnPc2
breaks	breaks	k6eAd1
cost	cost	k1gInSc1
EU	EU	kA
states	states	k1gInSc1
€	€	k?
<g/>
39	#num#	k4
billion	billion	k1gInSc4
a	a	k8xC
year	year	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
25	#num#	k4
červenec	červenec	k1gInSc1
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
EU	EU	kA
governments	governments	k6eAd1
miss	miss	k1gFnPc1
out	out	k?
on	on	k3xPp3gMnSc1
up	up	k?
to	ten	k3xDgNnSc1
€	€	k?
<g/>
39	#num#	k4
<g/>
bn	bn	k?
a	a	k8xC
year	year	k1gInSc1
due	due	k?
to	ten	k3xDgNnSc1
aviation	aviation	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
tax	taxa	k1gFnPc2
breaks	breaks	k1gInSc4
–	–	k?
Transport	transport	k1gInSc1
and	and	k?
Environment	Environment	k1gInSc1
<g/>
↑	↑	k?
Including	Including	k1gInSc1
Aviation	Aviation	k1gInSc1
into	into	k1gMnSc1
the	the	k?
EU	EU	kA
ETS	ETS	kA
<g/>
:	:	kIx,
Impact	Impact	k1gMnSc1
on	on	k3xPp3gMnSc1
EU	EU	kA
allowance	allowance	k1gFnSc1
prices	prices	k1gInSc1
ICF	ICF	kA
Consulting	Consulting	k1gInSc1
for	forum	k1gNnPc2
DEFRA	DEFRA	kA
February	Februara	k1gFnSc2
2006	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
GUNSTON	GUNSTON	kA
<g/>
,	,	kIx,
Bill	Bill	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Člověk	člověk	k1gMnSc1
a	a	k8xC
letectví	letectví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Knižní	knižní	k2eAgInSc1d1
klub	klub	k1gInSc1
;	;	kIx,
Ikar	Ikar	k1gInSc1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-7176-534-1	80-7176-534-1	k4
a	a	k8xC
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7202	#num#	k4
<g/>
-	-	kIx~
<g/>
156	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ŠOREL	ŠOREL	kA
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyklopedie	encyklopedie	k1gFnSc1
českého	český	k2eAgNnSc2d1
a	a	k8xC
slovenského	slovenský	k2eAgNnSc2d1
letectví	letectví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Computer	computer	k1gInSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
251	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
733	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ŠOREL	ŠOREL	kA
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyklopedie	encyklopedie	k1gFnSc1
českého	český	k2eAgNnSc2d1
a	a	k8xC
slovenského	slovenský	k2eAgNnSc2d1
letectví	letectví	k1gNnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Computer	computer	k1gInSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
251	#num#	k4
<g/>
-	-	kIx~
<g/>
1979	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
pro	pro	k7c4
civilní	civilní	k2eAgNnSc4d1
letectví	letectví	k1gNnSc4
</s>
<s>
Letecký	letecký	k2eAgInSc1d1
zákon	zákon	k1gInSc1
</s>
<s>
Letecké	letecký	k2eAgInPc1d1
předpisy	předpis	k1gInPc1
</s>
<s>
Strava	strava	k1gFnSc1
v	v	k7c6
letecké	letecký	k2eAgFnSc6d1
dopravě	doprava	k1gFnSc6
</s>
<s>
Letectví	letectví	k1gNnSc1
a	a	k8xC
kosmonautika	kosmonautika	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
letectví	letectví	k1gNnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Aéronautika	Aéronautikum	k1gNnSc2
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Letecký	letecký	k2eAgInSc1d1
informační	informační	k2eAgInSc1d1
server	server	k1gInSc1
–	–	k?
Databáze	databáze	k1gFnSc1
českého	český	k2eAgNnSc2d1
letectví	letectví	k1gNnSc2
</s>
<s>
Úřad	úřad	k1gInSc1
pro	pro	k7c4
civilní	civilní	k2eAgNnSc4d1
letectví	letectví	k1gNnSc4
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Výzkumný	výzkumný	k2eAgInSc1d1
a	a	k8xC
zkušební	zkušební	k2eAgInSc1d1
letecký	letecký	k2eAgInSc1d1
ústav	ústav	k1gInSc1
</s>
<s>
Katedra	katedra	k1gFnSc1
letecké	letecký	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
ČVUT	ČVUT	kA
</s>
<s>
Letecká	letecký	k2eAgFnSc1d1
amatérská	amatérský	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
ČR	ČR	kA
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Letectví	letectví	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4036557-8	4036557-8	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
1162	#num#	k4
</s>
