<s>
Letecká	letecký	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
nejbezpečnější	bezpečný	k2eAgInPc4d3
druhy	druh	k1gInPc4
dopravy	doprava	k1gFnSc2
<g/>
;	;	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
zemřelo	zemřít	k5eAaPmAgNnS
při	při	k7c6
leteckých	letecký	k2eAgFnPc6d1
nehodách	nehoda	k1gFnPc6
539	#num#	k4
osob	osoba	k1gFnPc2
<g/>
,	,	kIx,
jedno	jeden	k4xCgNnSc4
úmrtí	úmrtí	k1gNnSc4
připadá	připadat	k5eAaPmIp3nS,k5eAaImIp3nS
tedy	tedy	k9
na	na	k7c4
každý	každý	k3xTgInSc4
1,3	1,3	k4
<g/>
miliontý	miliontý	k4xOgInSc4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Při	při	k7c6
leteckých	letecký	k2eAgFnPc6d1
nehodách	nehoda	k1gFnPc6
tedy	tedy	k8xC
zemřelo	zemřít	k5eAaPmAgNnS
méně	málo	k6eAd2
lidí	člověk	k1gMnPc2
než	než	k8xS
na	na	k7c6
českých	český	k2eAgFnPc6d1
silnicích	silnice	k1gFnPc6
za	za	k7c4
daný	daný	k2eAgInSc4d1
rok	rok	k1gInSc4
<g/>
.	.	kIx.
</s>