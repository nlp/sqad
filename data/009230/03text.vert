<p>
<s>
Albuquerque	Albuquerque	k6eAd1	Albuquerque
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
státu	stát	k1gInSc2	stát
Nové	Nové	k2eAgNnSc1d1	Nové
Mexiko	Mexiko	k1gNnSc1	Mexiko
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
449	[number]	k4	449
236	[number]	k4	236
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
v	v	k7c6	v
aglomeraci	aglomerace	k1gFnSc6	aglomerace
797	[number]	k4	797
940	[number]	k4	940
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
okresu	okres	k1gInSc6	okres
Bernalillo	Bernalillo	k1gNnSc4	Bernalillo
County	Counta	k1gFnSc2	Counta
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
1706	[number]	k4	1706
španělskými	španělský	k2eAgMnPc7d1	španělský
kolonizátory	kolonizátor	k1gMnPc7	kolonizátor
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
si	se	k3xPyFc3	se
udržuje	udržovat	k5eAaImIp3nS	udržovat
některé	některý	k3yIgInPc4	některý
typické	typický	k2eAgInPc4d1	typický
španělské	španělský	k2eAgInPc4d1	španělský
kulturní	kulturní	k2eAgInPc4d1	kulturní
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2005	[number]	k4	2005
a	a	k8xC	a
2006	[number]	k4	2006
se	se	k3xPyFc4	se
slavilo	slavit	k5eAaImAgNnS	slavit
třísté	třístý	k2eAgNnSc1d1	třístý
výročí	výročí	k1gNnSc1	výročí
založení	založení	k1gNnSc2	založení
Albuquerque	Albuquerqu	k1gFnSc2	Albuquerqu
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Albuquerque	Albuquerque	k1gNnSc6	Albuquerque
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Univerzita	univerzita	k1gFnSc1	univerzita
státu	stát	k1gInSc2	stát
Nové	Nové	k2eAgNnSc1d1	Nové
Mexiko	Mexiko	k1gNnSc1	Mexiko
s	s	k7c7	s
Botanickou	botanický	k2eAgFnSc7d1	botanická
zahradou	zahrada	k1gFnSc7	zahrada
při	při	k7c6	při
Univerzitě	univerzita	k1gFnSc6	univerzita
státu	stát	k1gInSc2	stát
Nové	Nové	k2eAgNnSc1d1	Nové
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
,	,	kIx,	,
Státní	státní	k2eAgFnPc1d1	státní
laboratoře	laboratoř	k1gFnPc1	laboratoř
Sandia	Sandium	k1gNnSc2	Sandium
a	a	k8xC	a
Kirtlandova	Kirtlandův	k2eAgFnSc1d1	Kirtlandův
letecká	letecký	k2eAgFnSc1d1	letecká
základna	základna	k1gFnSc1	základna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Albuquerque	Albuquerque	k1gNnSc2	Albuquerque
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
pohoří	pohoří	k1gNnSc3	pohoří
Sandia	Sandium	k1gNnSc2	Sandium
<g/>
,	,	kIx,	,
řeka	řeka	k1gFnSc1	řeka
Rio	Rio	k1gFnSc1	Rio
Grande	grand	k1gMnSc5	grand
protéká	protékat	k5eAaImIp3nS	protékat
městem	město	k1gNnSc7	město
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
suché	suchý	k2eAgNnSc1d1	suché
a	a	k8xC	a
slunečné	slunečný	k2eAgNnSc1d1	slunečné
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
s	s	k7c7	s
méně	málo	k6eAd2	málo
než	než	k8xS	než
300	[number]	k4	300
milimetry	milimetr	k1gInPc1	milimetr
srážek	srážka	k1gFnPc2	srážka
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celý	celý	k2eAgInSc1d1	celý
říjen	říjen	k1gInSc1	říjen
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
Fiesta	fiesta	k1gFnSc1	fiesta
Internacional	Internacional	k1gFnSc2	Internacional
de	de	k?	de
Globos	Globos	k1gInSc1	Globos
de	de	k?	de
Albuquerque	Albuquerque	k1gInSc1	Albuquerque
<g/>
,	,	kIx,	,
největší	veliký	k2eAgNnSc1d3	veliký
shromáždění	shromáždění	k1gNnSc1	shromáždění
horkovzdušných	horkovzdušný	k2eAgInPc2d1	horkovzdušný
balonů	balon	k1gInPc2	balon
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
několikrát	několikrát	k6eAd1	několikrát
dějištěm	dějiště	k1gNnSc7	dějiště
závodu	závod	k1gInSc2	závod
o	o	k7c4	o
Pohár	pohár	k1gInSc4	pohár
Gordona	Gordon	k1gMnSc2	Gordon
Bennetta	Bennett	k1gMnSc2	Bennett
v	v	k7c6	v
balonovém	balonový	k2eAgNnSc6d1	balonové
létání	létání	k1gNnSc6	létání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Stará	starý	k2eAgFnSc1d1	stará
španělská	španělský	k2eAgFnSc1d1	španělská
čtvrť	čtvrť	k1gFnSc1	čtvrť
Albuquerque	Albuquerque	k1gFnPc2	Albuquerque
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
podél	podél	k7c2	podél
El	Ela	k1gFnPc2	Ela
Camino	Camin	k2eAgNnSc1d1	Camino
Real	Real	k1gInSc1	Real
roku	rok	k1gInSc2	rok
1706	[number]	k4	1706
jako	jako	k8xC	jako
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
obec	obec	k1gFnSc1	obec
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
v	v	k7c6	v
typickém	typický	k2eAgInSc6d1	typický
španělském	španělský	k2eAgInSc6d1	španělský
koloniálním	koloniální	k2eAgInSc6d1	koloniální
stylu	styl	k1gInSc6	styl
s	s	k7c7	s
centrálním	centrální	k2eAgNnSc7d1	centrální
náměstím	náměstí	k1gNnSc7	náměstí
obklopeným	obklopený	k2eAgNnSc7d1	obklopené
domy	dům	k1gInPc7	dům
<g/>
,	,	kIx,	,
správními	správní	k2eAgFnPc7d1	správní
budovami	budova	k1gFnPc7	budova
a	a	k8xC	a
kostelem	kostel	k1gInSc7	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
stará	starý	k2eAgFnSc1d1	stará
čtvrť	čtvrť	k1gFnSc1	čtvrť
se	se	k3xPyFc4	se
dochovala	dochovat	k5eAaPmAgFnS	dochovat
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
muzeum	muzeum	k1gNnSc4	muzeum
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgNnSc4d1	kulturní
místo	místo	k1gNnSc4	místo
a	a	k8xC	a
obchodní	obchodní	k2eAgNnSc4d1	obchodní
centrum	centrum	k1gNnSc4	centrum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Guvernér	guvernér	k1gMnSc1	guvernér
provincie	provincie	k1gFnSc2	provincie
Don	Don	k1gMnSc1	Don
Francisco	Francisco	k1gMnSc1	Francisco
Cuervo	Cuervo	k1gNnSc4	Cuervo
y	y	k?	y
Valdés	Valdés	k1gInSc1	Valdés
udělil	udělit	k5eAaPmAgInS	udělit
vesnici	vesnice	k1gFnSc4	vesnice
jméno	jméno	k1gNnSc1	jméno
"	"	kIx"	"
<g/>
Alburquerque	Alburquerque	k1gInSc1	Alburquerque
<g/>
"	"	kIx"	"
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
Vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Alburquerque	Alburquerqu	k1gMnSc2	Alburquerqu
<g/>
,	,	kIx,	,
místokrále	místokrál	k1gMnSc2	místokrál
Nového	Nového	k2eAgNnSc2d1	Nového
Španělska	Španělsko	k1gNnSc2	Španělsko
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1702	[number]	k4	1702
až	až	k9	až
1710	[number]	k4	1710
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
"	"	kIx"	"
<g/>
r	r	kA	r
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
během	během	k7c2	během
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vytratila	vytratit	k5eAaPmAgFnS	vytratit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
starý	starý	k2eAgInSc1d1	starý
pravopis	pravopis	k1gInSc1	pravopis
je	být	k5eAaImIp3nS	být
příležitostně	příležitostně	k6eAd1	příležitostně
stále	stále	k6eAd1	stále
používán	používat	k5eAaImNgInS	používat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1862	[number]	k4	1862
během	během	k7c2	během
Americké	americký	k2eAgFnSc2d1	americká
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
krátce	krátce	k6eAd1	krátce
okupováno	okupovat	k5eAaBmNgNnS	okupovat
vojskem	vojsko	k1gNnSc7	vojsko
Konfederace	konfederace	k1gFnSc2	konfederace
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
generála	generál	k1gMnSc2	generál
Henryho	Henry	k1gMnSc2	Henry
Hopkinse	Hopkins	k1gMnSc2	Hopkins
Sibleyho	Sibley	k1gMnSc2	Sibley
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
ústupu	ústup	k1gInSc2	ústup
do	do	k7c2	do
Texasu	Texas	k1gInSc2	Texas
se	s	k7c7	s
Sibley	Sibley	k1gInPc7	Sibley
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1862	[number]	k4	1862
obrnil	obrnit	k5eAaPmAgMnS	obrnit
v	v	k7c6	v
Albuquerque	Albuquerque	k1gFnSc6	Albuquerque
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
stavba	stavba	k1gFnSc1	stavba
železnice	železnice	k1gFnSc2	železnice
Atchison-Topeka-Santa	Atchison-Topeka-Sant	k1gMnSc2	Atchison-Topeka-Sant
Fe	Fe	k1gMnSc2	Fe
dorazila	dorazit	k5eAaPmAgFnS	dorazit
roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
nádraží	nádraží	k1gNnSc2	nádraží
a	a	k8xC	a
vozovna	vozovna	k1gFnSc1	vozovna
nebyly	být	k5eNaImAgFnP	být
postaveny	postavit	k5eAaPmNgFnP	postavit
ve	v	k7c4	v
staré	starý	k2eAgFnPc4d1	stará
čtvrti	čtvrt	k1gFnPc4	čtvrt
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
několik	několik	k4yIc4	několik
kilometrů	kilometr	k1gInPc2	kilometr
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
místo	místo	k1gNnSc1	místo
dostalo	dostat	k5eAaPmAgNnS	dostat
název	název	k1gInSc4	název
"	"	kIx"	"
<g/>
New	New	k1gFnSc4	New
Albuquerque	Albuquerqu	k1gInSc2	Albuquerqu
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
také	také	k9	také
"	"	kIx"	"
<g/>
New	New	k1gMnSc1	New
Town	Town	k1gMnSc1	Town
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
kolem	kolem	k7c2	kolem
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Old	Olda	k1gFnPc2	Olda
Town	Town	k1gInSc1	Town
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
samostatná	samostatný	k2eAgFnSc1d1	samostatná
obec	obec	k1gFnSc1	obec
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
New	New	k?	New
Albuquerque	Albuquerque	k1gInSc1	Albuquerque
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
stalo	stát	k5eAaPmAgNnS	stát
prosperujícím	prosperující	k2eAgNnSc7d1	prosperující
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
již	již	k6eAd1	již
8000	[number]	k4	8000
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
všechny	všechen	k3xTgFnPc4	všechen
vymoženosti	vymoženost	k1gFnPc4	vymoženost
moderní	moderní	k2eAgFnSc2d1	moderní
doby	doba	k1gFnSc2	doba
včetně	včetně	k7c2	včetně
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
sítě	síť	k1gFnSc2	síť
spojující	spojující	k2eAgNnPc4d1	spojující
obě	dva	k4xCgNnPc4	dva
Albuquerque	Albuquerque	k1gNnPc4	Albuquerque
a	a	k8xC	a
nově	nově	k6eAd1	nově
založenou	založený	k2eAgFnSc4d1	založená
Univerzitu	univerzita	k1gFnSc4	univerzita
Nového	Nového	k2eAgNnSc2d1	Nového
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1902	[number]	k4	1902
byl	být	k5eAaImAgInS	být
vedle	vedle	k7c2	vedle
nové	nový	k2eAgFnSc2d1	nová
železniční	železniční	k2eAgFnSc2d1	železniční
stanice	stanice	k1gFnSc2	stanice
postaven	postaven	k2eAgInSc1d1	postaven
známý	známý	k2eAgInSc1d1	známý
hotel	hotel	k1gInSc1	hotel
Alvarado	Alvarada	k1gFnSc5	Alvarada
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
symbolem	symbol	k1gInSc7	symbol
města	město	k1gNnSc2	město
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
zbourán	zbourat	k5eAaPmNgInS	zbourat
a	a	k8xC	a
nahrazen	nahradit	k5eAaPmNgInS	nahradit
parkovištěm	parkoviště	k1gNnSc7	parkoviště
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
stejném	stejný	k2eAgNnSc6d1	stejné
místě	místo	k1gNnSc6	místo
postaveno	postavit	k5eAaPmNgNnS	postavit
Alvarado	Alvarada	k1gFnSc5	Alvarada
Transportation	Transportation	k1gInSc1	Transportation
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
podobat	podobat	k5eAaImF	podobat
dřívějšímu	dřívější	k2eAgInSc3d1	dřívější
hotelu	hotel	k1gInSc6	hotel
Alvarado	Alvarada	k1gFnSc5	Alvarada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Route	Route	k5eAaPmIp2nP	Route
66	[number]	k4	66
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
silnice	silnice	k1gFnSc1	silnice
spojující	spojující	k2eAgNnSc1d1	spojující
Chicago	Chicago	k1gNnSc1	Chicago
<g/>
,	,	kIx,	,
Illinois	Illinois	k1gFnSc1	Illinois
a	a	k8xC	a
Santa	Santa	k1gFnSc1	Santa
Monica	Monica	k1gFnSc1	Monica
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
,	,	kIx,	,
dorazila	dorazit	k5eAaPmAgFnS	dorazit
do	do	k7c2	do
Albuquerque	Albuquerqu	k1gInSc2	Albuquerqu
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
a	a	k8xC	a
nedlouho	dlouho	k6eNd1	dlouho
potom	potom	k6eAd1	potom
začali	začít	k5eAaPmAgMnP	začít
přicházet	přicházet	k5eAaImF	přicházet
první	první	k4xOgMnPc1	první
návštěvníci	návštěvník	k1gMnPc1	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
bylo	být	k5eAaImAgNnS	být
podél	podél	k6eAd1	podél
Fourth	Fourth	k1gInSc4	Fourth
Street	Streeta	k1gFnPc2	Streeta
<g/>
,	,	kIx,	,
kudy	kudy	k6eAd1	kudy
projížděla	projíždět	k5eAaImAgNnP	projíždět
Route	Rout	k1gInSc5	Rout
66	[number]	k4	66
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
zbudováno	zbudovat	k5eAaPmNgNnS	zbudovat
mnoho	mnoho	k4c1	mnoho
motelů	motel	k1gInPc2	motel
<g/>
,	,	kIx,	,
restaurací	restaurace	k1gFnPc2	restaurace
a	a	k8xC	a
obchodů	obchod	k1gInPc2	obchod
se	s	k7c7	s
suvenýry	suvenýr	k1gInPc7	suvenýr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
byla	být	k5eAaImAgFnS	být
silnice	silnice	k1gFnSc1	silnice
přesunuta	přesunout	k5eAaPmNgFnS	přesunout
na	na	k7c4	na
Central	Central	k1gFnSc4	Central
Avenue	avenue	k1gFnSc2	avenue
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
budov	budova	k1gFnPc2	budova
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
Route	Rout	k1gInSc5	Rout
66	[number]	k4	66
nachází	nacházet	k5eAaImIp3nS	nacházet
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
několik	několik	k4yIc4	několik
jich	on	k3xPp3gFnPc2	on
můžeme	moct	k5eAaImIp1nP	moct
najít	najít	k5eAaPmF	najít
i	i	k9	i
na	na	k7c4	na
Fourth	Fourth	k1gInSc4	Fourth
Street	Streeta	k1gFnPc2	Streeta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
zbudování	zbudování	k1gNnSc3	zbudování
letecké	letecký	k2eAgFnSc2d1	letecká
základny	základna	k1gFnSc2	základna
a	a	k8xC	a
státních	státní	k2eAgFnPc2d1	státní
laboratoří	laboratoř	k1gFnPc2	laboratoř
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1939	[number]	k4	1939
a	a	k8xC	a
1949	[number]	k4	1949
mělo	mít	k5eAaImAgNnS	mít
Albuquerque	Albuquerqu	k1gInPc1	Albuquerqu
velký	velký	k2eAgInSc1d1	velký
význam	význam	k1gInSc4	význam
během	během	k7c2	během
Studené	Studené	k2eAgFnSc2d1	Studené
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Rozrůstalo	rozrůstat	k5eAaImAgNnS	rozrůstat
se	se	k3xPyFc4	se
a	a	k8xC	a
rozšiřovalo	rozšiřovat	k5eAaImAgNnS	rozšiřovat
na	na	k7c4	na
východ	východ	k1gInSc4	východ
a	a	k8xC	a
roku	rok	k1gInSc3	rok
1960	[number]	k4	1960
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
200	[number]	k4	200
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Demografie	demografie	k1gFnSc2	demografie
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
545	[number]	k4	545
852	[number]	k4	852
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rasové	rasový	k2eAgNnSc4d1	rasové
složení	složení	k1gNnSc4	složení
===	===	k?	===
</s>
</p>
<p>
<s>
69,7	[number]	k4	69,7
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
</s>
</p>
<p>
<s>
3,3	[number]	k4	3,3
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
</s>
</p>
<p>
<s>
4,6	[number]	k4	4,6
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
</s>
</p>
<p>
<s>
2,6	[number]	k4	2,6
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
</s>
</p>
<p>
<s>
0,0	[number]	k4	0,0
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
</s>
</p>
<p>
<s>
15,1	[number]	k4	15,1
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
</s>
</p>
<p>
<s>
4,6	[number]	k4	4,6
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
rasObyvatelé	rasObyvatel	k1gMnPc1	rasObyvatel
hispánského	hispánský	k2eAgInSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
46,7	[number]	k4	46,7
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
==	==	k?	==
</s>
</p>
<p>
<s>
Dallas	Dallas	k1gInSc1	Dallas
<g/>
:	:	kIx,	:
1038	[number]	k4	1038
km	km	kA	km
(	(	kIx(	(
<g/>
645	[number]	k4	645
mil	míle	k1gFnPc2	míle
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Denver	Denver	k1gInSc1	Denver
<g/>
:	:	kIx,	:
716	[number]	k4	716
km	km	kA	km
(	(	kIx(	(
<g/>
450	[number]	k4	450
mil	míle	k1gFnPc2	míle
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Phoenix	Phoenix	k1gInSc1	Phoenix
<g/>
,	,	kIx,	,
Arizona	Arizona	k1gFnSc1	Arizona
<g/>
:	:	kIx,	:
748	[number]	k4	748
km	km	kA	km
(	(	kIx(	(
<g/>
465	[number]	k4	465
mil	míle	k1gFnPc2	míle
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Salt	salto	k1gNnPc2	salto
Lake	Lake	k1gFnPc7	Lake
City	City	k1gFnSc2	City
<g/>
:	:	kIx,	:
998	[number]	k4	998
km	km	kA	km
(	(	kIx(	(
<g/>
620	[number]	k4	620
mil	míle	k1gFnPc2	míle
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sesterská	sesterský	k2eAgNnPc1d1	sesterské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Albuquerque	Albuquerqu	k1gFnSc2	Albuquerqu
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Albuquerque	Albuquerqu	k1gFnSc2	Albuquerqu
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
internetové	internetový	k2eAgFnPc1d1	internetová
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
