<s>
Sonic	Sonic	k1gMnSc1	Sonic
Boom	boom	k1gInSc4	boom
je	být	k5eAaImIp3nS	být
devatenácté	devatenáctý	k4xOgNnSc1	devatenáctý
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
a	a	k8xC	a
první	první	k4xOgFnSc4	první
po	po	k7c4	po
11	[number]	k4	11
letech	léto	k1gNnPc6	léto
od	od	k7c2	od
americké	americký	k2eAgFnSc2d1	americká
hard	hard	k6eAd1	hard
rockové	rockový	k2eAgFnSc2d1	rocková
skupiny	skupina	k1gFnSc2	skupina
Kiss	Kissa	k1gFnPc2	Kissa
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
produkovali	produkovat	k5eAaImAgMnP	produkovat
Paul	Paul	k1gMnSc1	Paul
Stanley	Stanlea	k1gFnSc2	Stanlea
a	a	k8xC	a
Greg	Greg	k1gInSc4	Greg
Collins	Collinsa	k1gFnPc2	Collinsa
a	a	k8xC	a
také	také	k9	také
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
první	první	k4xOgNnSc1	první
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
v	v	k7c6	v
sestavě	sestava	k1gFnSc6	sestava
s	s	k7c7	s
kytaristou	kytarista	k1gMnSc7	kytarista
Tommy	Tomma	k1gFnSc2	Tomma
Thayerem	Thayer	k1gMnSc7	Thayer
<g/>
.	.	kIx.	.
<g/>
Desku	deska	k1gFnSc4	deska
přijala	přijmout	k5eAaPmAgFnS	přijmout
pozitivně	pozitivně	k6eAd1	pozitivně
jak	jak	k8xC	jak
kritika	kritika	k1gFnSc1	kritika
tak	tak	k8xC	tak
veřejnost	veřejnost	k1gFnSc1	veřejnost
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
týdnu	týden	k1gInSc6	týden
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
108.000	[number]	k4	108.000
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Paul	Paul	k1gMnSc1	Paul
Stanley	Stanlea	k1gFnSc2	Stanlea
-	-	kIx~	-
rytmická	rytmický	k2eAgFnSc1d1	rytmická
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
Gene	gen	k1gInSc5	gen
Simmons	Simmons	k1gInSc1	Simmons
-	-	kIx~	-
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
Tommy	Tomma	k1gFnSc2	Tomma
Thayer	Thayra	k1gFnPc2	Thayra
-	-	kIx~	-
sólová	sólový	k2eAgFnSc1d1	sólová
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
Eric	Eric	k1gInSc1	Eric
Singer	Singer	k1gInSc4	Singer
-	-	kIx~	-
bicí	bicí	k2eAgInSc4d1	bicí
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
Album	album	k1gNnSc1	album
</s>
