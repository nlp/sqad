<p>
<s>
Tenisový	tenisový	k2eAgInSc1d1	tenisový
turnaj	turnaj	k1gInSc1	turnaj
WTA	WTA	kA	WTA
Estoril	Estoril	k1gInSc1	Estoril
Open	Open	k1gInSc1	Open
2010	[number]	k4	2010
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
ve	v	k7c6	v
dnech	den	k1gInPc6	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
až	až	k9	až
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
v	v	k7c6	v
portugalském	portugalský	k2eAgInSc6d1	portugalský
Estorilu	Estoril	k1gInSc6	Estoril
venku	venku	k6eAd1	venku
na	na	k7c6	na
antukových	antukový	k2eAgInPc6d1	antukový
dvorcích	dvorec	k1gInPc6	dvorec
<g/>
.	.	kIx.	.
</s>
<s>
Turnaj	turnaj	k1gInSc1	turnaj
patřil	patřit	k5eAaImAgInS	patřit
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
WTA	WTA	kA	WTA
International	International	k1gFnSc1	International
Tournaments	Tournaments	k1gInSc1	Tournaments
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
součástí	součást	k1gFnSc7	součást
WTA	WTA	kA	WTA
Tour	Tour	k1gInSc4	Tour
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Odměny	odměna	k1gFnPc1	odměna
činily	činit	k5eAaImAgFnP	činit
220	[number]	k4	220
000	[number]	k4	000
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dvouhra	dvouhra	k1gFnSc1	dvouhra
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Nasazení	nasazení	k1gNnSc1	nasazení
hráček	hráčka	k1gFnPc2	hráčka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Ceny	cena	k1gFnPc1	cena
a	a	k8xC	a
body	bod	k1gInPc1	bod
do	do	k7c2	do
žebříčku	žebříček	k1gInSc2	žebříček
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Pavouk	pavouk	k1gMnSc1	pavouk
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Finálová	finálový	k2eAgFnSc1d1	finálová
fáze	fáze	k1gFnSc1	fáze
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Horní	horní	k2eAgFnSc1d1	horní
polovina	polovina	k1gFnSc1	polovina
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Dolní	dolní	k2eAgFnSc1d1	dolní
polovina	polovina	k1gFnSc1	polovina
====	====	k?	====
</s>
</p>
<p>
<s>
==	==	k?	==
Čtyřhra	čtyřhra	k1gFnSc1	čtyřhra
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Nasazení	nasazení	k1gNnSc1	nasazení
hráček	hráčka	k1gFnPc2	hráčka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Ceny	cena	k1gFnPc1	cena
a	a	k8xC	a
body	bod	k1gInPc1	bod
do	do	k7c2	do
žebříčku	žebříček	k1gInSc2	žebříček
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Pavouk	pavouk	k1gMnSc1	pavouk
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Estoril	Estoril	k1gInSc1	Estoril
Open	Open	k1gNnSc1	Open
2010	[number]	k4	2010
-	-	kIx~	-
muži	muž	k1gMnPc1	muž
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
Pavouk	pavouk	k1gMnSc1	pavouk
pro	pro	k7c4	pro
dvouhru	dvouhra	k1gFnSc4	dvouhra
</s>
</p>
<p>
<s>
Pavouk	pavouk	k1gMnSc1	pavouk
pro	pro	k7c4	pro
čtyřhru	čtyřhra	k1gFnSc4	čtyřhra
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
portugalsky	portugalsky	k6eAd1	portugalsky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
turnaje	turnaj	k1gInSc2	turnaj
</s>
</p>
