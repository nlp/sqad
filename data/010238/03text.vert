<p>
<s>
Okresy	okres	k1gInPc1	okres
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
jsou	být	k5eAaImIp3nP	být
administrativními	administrativní	k2eAgInPc7d1	administrativní
celky	celek	k1gInPc7	celek
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
mezi	mezi	k7c7	mezi
státy	stát	k1gInPc7	stát
a	a	k8xC	a
obcemi	obec	k1gFnPc7	obec
(	(	kIx(	(
<g/>
městy	město	k1gNnPc7	město
<g/>
,	,	kIx,	,
vesnicemi	vesnice	k1gFnPc7	vesnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
jsou	být	k5eAaImIp3nP	být
nazývány	nazýván	k2eAgInPc4d1	nazýván
borough	borough	k1gInSc4	borough
<g/>
,	,	kIx,	,
v	v	k7c6	v
Louisianě	Louisiana	k1gFnSc6	Louisiana
parish	parisha	k1gFnPc2	parisha
a	a	k8xC	a
ve	v	k7c6	v
zbylých	zbylý	k2eAgInPc6d1	zbylý
státech	stát	k1gInPc6	stát
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
ně	on	k3xPp3gInPc4	on
používá	používat	k5eAaImIp3nS	používat
termín	termín	k1gInSc1	termín
county	count	k1gInPc1	count
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc1	tento
termín	termín	k1gInSc1	termín
se	se	k3xPyFc4	se
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
hrabství	hrabství	k1gNnSc4	hrabství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
samostatné	samostatný	k2eAgInPc4d1	samostatný
okresy	okres	k1gInPc4	okres
se	se	k3xPyFc4	se
také	také	k9	také
považují	považovat	k5eAaImIp3nP	považovat
některá	některý	k3yIgNnPc4	některý
města	město	k1gNnPc4	město
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
nejsou	být	k5eNaImIp3nP	být
součástí	součást	k1gFnSc7	součást
okresů	okres	k1gInPc2	okres
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc2d1	americký
3140	[number]	k4	3140
území	území	k1gNnPc2	území
ekvivalentních	ekvivalentní	k2eAgMnPc2d1	ekvivalentní
okresům	okres	k1gInPc3	okres
<g/>
.	.	kIx.	.
</s>
<s>
Průměrně	průměrně	k6eAd1	průměrně
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
stát	stát	k1gInSc4	stát
62	[number]	k4	62
okresů	okres	k1gInPc2	okres
a	a	k8xC	a
100	[number]	k4	100
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
například	například	k6eAd1	například
okres	okres	k1gInSc1	okres
Los	los	k1gInSc1	los
Angeles	Angeles	k1gInSc4	Angeles
má	mít	k5eAaImIp3nS	mít
téměř	téměř	k6eAd1	téměř
10	[number]	k4	10
miliónů	milión	k4xCgInPc2	milión
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
víc	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
některé	některý	k3yIgInPc1	některý
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
okres	okres	k1gInSc1	okres
Loving	Loving	k1gInSc1	Loving
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
má	mít	k5eAaImIp3nS	mít
pouhých	pouhý	k2eAgMnPc2d1	pouhý
82	[number]	k4	82
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
okres	okres	k1gInSc1	okres
Kalawao	Kalawao	k6eAd1	Kalawao
na	na	k7c6	na
Havaji	Havaj	k1gFnSc6	Havaj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
býval	bývat	k5eAaImAgInS	bývat
kolonií	kolonie	k1gFnSc7	kolonie
malomocných	malomocný	k1gMnPc2	malomocný
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
147	[number]	k4	147
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
okresů	okres	k1gInPc2	okres
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
po	po	k7c6	po
osobnostech	osobnost	k1gFnPc6	osobnost
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zvláště	zvláště	k6eAd1	zvláště
oblíbená	oblíbený	k2eAgNnPc1d1	oblíbené
jména	jméno	k1gNnPc1	jméno
jsou	být	k5eAaImIp3nP	být
Washington	Washington	k1gInSc4	Washington
a	a	k8xC	a
Jefferson	Jefferson	k1gInSc4	Jefferson
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiné	k1gNnPc1	jiné
popisují	popisovat	k5eAaImIp3nP	popisovat
charakter	charakter	k1gInSc4	charakter
krajiny	krajina	k1gFnSc2	krajina
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Lake	Lake	k1gFnSc1	Lake
County	Counta	k1gFnSc2	Counta
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
jezerní	jezerní	k2eAgInSc1d1	jezerní
okres	okres	k1gInSc1	okres
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jména	jméno	k1gNnPc1	jméno
dalších	další	k2eAgFnPc2d1	další
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
francouzštiny	francouzština	k1gFnSc2	francouzština
nebo	nebo	k8xC	nebo
španělštiny	španělština	k1gFnSc2	španělština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
County	Count	k1gInPc4	Count
===	===	k?	===
</s>
</p>
<p>
<s>
County	Count	k1gInPc1	Count
se	se	k3xPyFc4	se
z	z	k7c2	z
angličtiny	angličtina	k1gFnSc2	angličtina
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
překládá	překládat	k5eAaImIp3nS	překládat
jako	jako	k9	jako
hrabství	hrabství	k1gNnSc1	hrabství
(	(	kIx(	(
<g/>
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
a	a	k8xC	a
distrikt	distrikt	k1gInSc1	distrikt
(	(	kIx(	(
<g/>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
a	a	k8xC	a
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
slovníku	slovník	k1gInSc2	slovník
Cambridge	Cambridge	k1gFnSc2	Cambridge
International	International	k1gFnPc2	International
Dictionary	Dictionara	k1gFnSc2	Dictionara
of	of	k?	of
English	English	k1gInSc1	English
je	být	k5eAaImIp3nS	být
County	Counta	k1gFnPc1	Counta
definované	definovaný	k2eAgFnPc1d1	definovaná
jako	jako	k8xC	jako
politická	politický	k2eAgFnSc1d1	politická
správa	správa	k1gFnSc1	správa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
stupeň	stupeň	k1gInSc4	stupeň
místní	místní	k2eAgFnSc2d1	místní
samosprávy	samospráva	k1gFnSc2	samospráva
<g/>
,	,	kIx,	,
vlády	vláda	k1gFnPc1	vláda
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
Irsku	Irsko	k1gNnSc6	Irsko
a	a	k8xC	a
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
stupeň	stupeň	k1gInSc1	stupeň
místní	místní	k2eAgFnSc2d1	místní
samosprávy	samospráva	k1gFnSc2	samospráva
<g/>
,	,	kIx,	,
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Taková	takový	k3xDgFnSc1	takový
definice	definice	k1gFnSc1	definice
podle	podle	k7c2	podle
právní	právní	k2eAgFnSc2d1	právní
úpravy	úprava	k1gFnSc2	úprava
územního	územní	k2eAgNnSc2d1	územní
členění	členění	k1gNnSc2	členění
státu	stát	k1gInSc2	stát
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
kraji	kraj	k1gInPc7	kraj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
termín	termín	k1gInSc4	termín
hrabství	hrabství	k1gNnSc3	hrabství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
