<s>
Lajka	lajka	k1gFnSc1	lajka
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
Л	Л	k?	Л
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
Štěkačka	Štěkačka	k1gFnSc1	Štěkačka
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
1954	[number]	k4	1954
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
fena	fena	k1gFnSc1	fena
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
živý	živý	k2eAgMnSc1d1	živý
tvor	tvor	k1gMnSc1	tvor
pocházející	pocházející	k2eAgMnSc1d1	pocházející
z	z	k7c2	z
planety	planeta	k1gFnSc2	planeta
Země	zem	k1gFnSc2	zem
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1957	[number]	k4	1957
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
sovětské	sovětský	k2eAgFnSc2d1	sovětská
družice	družice	k1gFnSc2	družice
Sputnik	sputnik	k1gInSc4	sputnik
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
přípravy	příprava	k1gFnSc2	příprava
jejího	její	k3xOp3gInSc2	její
letu	let	k1gInSc2	let
bylo	být	k5eAaImAgNnS	být
o	o	k7c6	o
vlivu	vliv	k1gInSc6	vliv
kosmického	kosmický	k2eAgInSc2d1	kosmický
letu	let	k1gInSc2	let
na	na	k7c4	na
živé	živý	k2eAgInPc4d1	živý
organismy	organismus	k1gInPc4	organismus
známo	znám	k2eAgNnSc1d1	známo
jen	jen	k9	jen
málo	málo	k4c1	málo
<g/>
.	.	kIx.	.
</s>
<s>
Vyslání	vyslání	k1gNnSc1	vyslání
zvířat	zvíře	k1gNnPc2	zvíře
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
bylo	být	k5eAaImAgNnS	být
proto	proto	k8xC	proto
nezbytným	nezbytný	k2eAgInSc7d1	nezbytný
předpokladem	předpoklad	k1gInSc7	předpoklad
letů	let	k1gInPc2	let
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Lajka	lajka	k1gFnSc1	lajka
<g/>
,	,	kIx,	,
potulný	potulný	k2eAgMnSc1d1	potulný
pes	pes	k1gMnSc1	pes
odchycený	odchycený	k2eAgMnSc1d1	odchycený
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
moskevských	moskevský	k2eAgFnPc2d1	Moskevská
ulic	ulice	k1gFnPc2	ulice
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
Kudrjavka	Kudrjavka	k1gFnSc1	Kudrjavka
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
К	К	k?	К
<g/>
;	;	kIx,	;
doslova	doslova	k6eAd1	doslova
Kudrnka	kudrnka	k1gFnSc1	kudrnka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
let	let	k1gInSc4	let
na	na	k7c6	na
Sputniku	sputnik	k1gInSc6	sputnik
2	[number]	k4	2
byla	být	k5eAaImAgFnS	být
cvičena	cvičen	k2eAgFnSc1d1	cvičena
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
dvěma	dva	k4xCgInPc7	dva
psy	pes	k1gMnPc7	pes
<g/>
.	.	kIx.	.
</s>
<s>
Technologie	technologie	k1gFnSc1	technologie
návratu	návrat	k1gInSc2	návrat
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodi	loď	k1gFnSc2	loď
z	z	k7c2	z
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
nebyly	být	k5eNaImAgFnP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
nebyl	být	k5eNaImAgMnS	být
Lajčin	lajčin	k2eAgInSc4d1	lajčin
návrat	návrat	k1gInSc4	návrat
na	na	k7c4	na
Zem	zem	k1gFnSc4	zem
plánován	plánovat	k5eAaImNgInS	plánovat
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
se	se	k3xPyFc4	se
očekávalo	očekávat	k5eAaImAgNnS	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
prožije	prožít	k5eAaPmIp3nS	prožít
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
jeden	jeden	k4xCgInSc4	jeden
týden	týden	k1gInSc4	týden
<g/>
,	,	kIx,	,
uhynula	uhynout	k5eAaPmAgFnS	uhynout
následkem	následkem	k7c2	následkem
stresu	stres	k1gInSc2	stres
a	a	k8xC	a
přehřátí	přehřátí	k1gNnSc2	přehřátí
už	už	k6eAd1	už
zhruba	zhruba	k6eAd1	zhruba
5	[number]	k4	5
až	až	k9	až
7	[number]	k4	7
hodin	hodina	k1gFnPc2	hodina
po	po	k7c6	po
startu	start	k1gInSc6	start
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
prvním	první	k4xOgMnSc7	první
tvorem	tvor	k1gMnSc7	tvor
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
však	však	k9	však
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Hranici	hranice	k1gFnSc4	hranice
vesmíru	vesmír	k1gInSc2	vesmír
-	-	kIx~	-
tedy	tedy	k8xC	tedy
výšku	výška	k1gFnSc4	výška
100	[number]	k4	100
km	km	kA	km
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
-	-	kIx~	-
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
z	z	k7c2	z
živých	živý	k2eAgMnPc2d1	živý
tvorů	tvor	k1gMnPc2	tvor
překonaly	překonat	k5eAaPmAgFnP	překonat
octomilky	octomilka	k1gFnPc1	octomilka
vynesené	vynesený	k2eAgFnPc1d1	vynesená
americkou	americký	k2eAgFnSc7d1	americká
raketou	raketa	k1gFnSc7	raketa
V-2	V-2	k1gFnSc2	V-2
už	už	k6eAd1	už
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
byli	být	k5eAaImAgMnP	být
prvními	první	k4xOgInPc7	první
psy	pes	k1gMnPc7	pes
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
dvojice	dvojice	k1gFnSc2	dvojice
Dezik	Dezika	k1gFnPc2	Dezika
a	a	k8xC	a
Cygan	Cygany	k1gInPc2	Cygany
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
Д	Д	k?	Д
и	и	k?	и
Ц	Ц	k?	Ц
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
vysláni	vyslat	k5eAaPmNgMnP	vyslat
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
z	z	k7c2	z
kosmodromu	kosmodrom	k1gInSc2	kosmodrom
Kapustin	Kapustin	k2eAgInSc4d1	Kapustin
Jar	jar	k1gInSc4	jar
po	po	k7c6	po
balistické	balistický	k2eAgFnSc6d1	balistická
dráze	dráha	k1gFnSc6	dráha
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
101	[number]	k4	101
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Sputnik	sputnik	k1gInSc1	sputnik
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
vypuštění	vypuštění	k1gNnSc6	vypuštění
družice	družice	k1gFnSc1	družice
Sputnik	sputnik	k1gInSc1	sputnik
1	[number]	k4	1
dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
požadoval	požadovat	k5eAaImAgMnS	požadovat
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
sovětský	sovětský	k2eAgMnSc1d1	sovětský
vůdce	vůdce	k1gMnSc1	vůdce
Nikita	Nikita	k1gMnSc1	Nikita
Chruščov	Chruščov	k1gInSc4	Chruščov
další	další	k2eAgInSc4d1	další
start	start	k1gInSc4	start
ke	k	k7c3	k
40	[number]	k4	40
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
Říjnové	říjnový	k2eAgFnSc2d1	říjnová
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
do	do	k7c2	do
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukční	konstrukční	k2eAgFnSc1d1	konstrukční
kancelář	kancelář	k1gFnSc1	kancelář
OKB-1	OKB-1	k1gFnSc1	OKB-1
připravovala	připravovat	k5eAaImAgFnS	připravovat
družici	družice	k1gFnSc4	družice
tentokrát	tentokrát	k6eAd1	tentokrát
vybavenou	vybavený	k2eAgFnSc4d1	vybavená
i	i	k8xC	i
vědeckými	vědecký	k2eAgInPc7d1	vědecký
přístroji	přístroj	k1gInPc7	přístroj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ta	ten	k3xDgFnSc1	ten
nemohla	moct	k5eNaImAgFnS	moct
být	být	k5eAaImF	být
hotova	hotov	k2eAgFnSc1d1	hotova
včas	včas	k6eAd1	včas
(	(	kIx(	(
<g/>
byla	být	k5eAaImAgFnS	být
vypuštěna	vypustit	k5eAaPmNgFnS	vypustit
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1958	[number]	k4	1958
jako	jako	k8xC	jako
Sputnik	sputnik	k1gInSc1	sputnik
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Konstruktéři	konstruktér	k1gMnPc1	konstruktér
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
uspokojili	uspokojit	k5eAaPmAgMnP	uspokojit
politické	politický	k2eAgNnSc4d1	politické
vedení	vedení	k1gNnSc4	vedení
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
vypustit	vypustit	k5eAaPmF	vypustit
družici	družice	k1gFnSc4	družice
se	s	k7c7	s
psem	pes	k1gMnSc7	pes
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
<g/>
.	.	kIx.	.
</s>
<s>
Lety	let	k1gInPc1	let
psů	pes	k1gMnPc2	pes
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
Země	zem	k1gFnSc2	zem
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
lidskými	lidský	k2eAgMnPc7d1	lidský
byly	být	k5eAaImAgFnP	být
zamýšleny	zamýšlet	k5eAaImNgFnP	zamýšlet
dlouho	dlouho	k6eAd1	dlouho
<g/>
;	;	kIx,	;
jejich	jejich	k3xOp3gFnSc7	jejich
přípravou	příprava	k1gFnSc7	příprava
se	se	k3xPyFc4	se
v	v	k7c6	v
Institutu	institut	k1gInSc6	institut
letecké	letecký	k2eAgFnSc2d1	letecká
medicíny	medicína	k1gFnSc2	medicína
zaobírala	zaobírat	k5eAaImAgFnS	zaobírat
skupina	skupina	k1gFnSc1	skupina
lékařů	lékař	k1gMnPc2	lékař
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Valerijem	Valerij	k1gInSc7	Valerij
Jazdovským	Jazdovský	k2eAgInSc7d1	Jazdovský
<g/>
.	.	kIx.	.
</s>
<s>
Stokilometrovou	stokilometrový	k2eAgFnSc4d1	stokilometrová
hranici	hranice	k1gFnSc4	hranice
vesmíru	vesmír	k1gInSc2	vesmír
překonali	překonat	k5eAaPmAgMnP	překonat
jejich	jejich	k3xOp3gMnPc1	jejich
pokusní	pokusný	k2eAgMnPc1d1	pokusný
psi	pes	k1gMnPc1	pes
při	při	k7c6	při
suborbitálních	suborbitální	k2eAgNnPc6d1	suborbitální
letech	léto	k1gNnPc6	léto
už	už	k9	už
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgNnSc1d1	oficiální
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
o	o	k7c4	o
vypuštění	vypuštění	k1gNnSc4	vypuštění
Sputniku	sputnik	k1gInSc2	sputnik
2	[number]	k4	2
se	s	k7c7	s
psem	pes	k1gMnSc7	pes
padlo	padnout	k5eAaPmAgNnS	padnout
10	[number]	k4	10
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
12	[number]	k4	12
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spěchu	spěch	k1gInSc6	spěch
byla	být	k5eAaImAgFnS	být
sestavena	sestaven	k2eAgFnSc1d1	sestavena
družice	družice	k1gFnSc1	družice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
kromě	kromě	k7c2	kromě
hermetizované	hermetizovaný	k2eAgFnSc2d1	hermetizovaná
válcové	válcový	k2eAgFnSc2d1	válcová
schránky	schránka	k1gFnSc2	schránka
využívané	využívaný	k2eAgInPc4d1	využívaný
při	při	k7c6	při
předchozích	předchozí	k2eAgInPc6d1	předchozí
suborbitálních	suborbitální	k2eAgInPc6d1	suborbitální
letech	let	k1gInPc6	let
psů	pes	k1gMnPc2	pes
i	i	k8xC	i
přístroje	přístroj	k1gInPc1	přístroj
pro	pro	k7c4	pro
měření	měření	k1gNnSc4	měření
solárního	solární	k2eAgNnSc2d1	solární
a	a	k8xC	a
kosmického	kosmický	k2eAgNnSc2d1	kosmické
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Družice	družice	k1gFnSc1	družice
byla	být	k5eAaImAgFnS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
i	i	k9	i
systémem	systém	k1gInSc7	systém
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
života	život	k1gInSc2	život
-	-	kIx~	-
generátorem	generátor	k1gInSc7	generátor
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zároveň	zároveň	k6eAd1	zároveň
pohlcoval	pohlcovat	k5eAaImAgInS	pohlcovat
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
a	a	k8xC	a
vzdušnou	vzdušný	k2eAgFnSc4d1	vzdušná
vlhkost	vlhkost	k1gFnSc4	vlhkost
<g/>
.	.	kIx.	.
</s>
<s>
Termoregulační	termoregulační	k2eAgInSc1d1	termoregulační
systém	systém	k1gInSc1	systém
měl	mít	k5eAaImAgInS	mít
udržovat	udržovat	k5eAaImF	udržovat
teplotu	teplota	k1gFnSc4	teplota
na	na	k7c4	na
15	[number]	k4	15
°	°	k?	°
<g/>
C.	C.	kA	C.
Potravu	potrava	k1gFnSc4	potrava
(	(	kIx(	(
<g/>
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
gelu	gel	k1gInSc2	gel
<g/>
)	)	kIx)	)
měla	mít	k5eAaImAgFnS	mít
Lajka	lajka	k1gFnSc1	lajka
na	na	k7c4	na
sedm	sedm	k4xCc4	sedm
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Čidla	čidlo	k1gNnPc1	čidlo
na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
psa	pes	k1gMnSc2	pes
snímala	snímat	k5eAaImAgFnS	snímat
srdeční	srdeční	k2eAgInSc4d1	srdeční
tep	tep	k1gInSc4	tep
<g/>
,	,	kIx,	,
dechovou	dechový	k2eAgFnSc4d1	dechová
frekvenci	frekvence	k1gFnSc4	frekvence
<g/>
,	,	kIx,	,
pohyb	pohyb	k1gInSc4	pohyb
a	a	k8xC	a
krevní	krevní	k2eAgInSc4d1	krevní
tlak	tlak	k1gInSc4	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Údaje	údaj	k1gInPc1	údaj
byly	být	k5eAaImAgInP	být
předávány	předávat	k5eAaImNgInP	předávat
pomocí	pomocí	k7c2	pomocí
vysílače	vysílač	k1gInSc2	vysílač
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Lajka	lajka	k1gFnSc1	lajka
byla	být	k5eAaImAgFnS	být
toulavý	toulavý	k2eAgMnSc1d1	toulavý
moskevský	moskevský	k2eAgMnSc1d1	moskevský
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
odchycený	odchycený	k2eAgMnSc1d1	odchycený
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
usoudili	usoudit	k5eAaPmAgMnP	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
toulaví	toulavý	k2eAgMnPc1d1	toulavý
psi	pes	k1gMnPc1	pes
zvyklí	zvyklý	k2eAgMnPc1d1	zvyklý
na	na	k7c4	na
těžký	těžký	k2eAgInSc4d1	těžký
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
hlad	hlad	k1gInSc4	hlad
a	a	k8xC	a
žízeň	žízeň	k1gFnSc4	žízeň
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
vydrží	vydržet	k5eAaPmIp3nS	vydržet
jak	jak	k6eAd1	jak
výcvik	výcvik	k1gInSc4	výcvik
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
kosmický	kosmický	k2eAgInSc4d1	kosmický
let	let	k1gInSc4	let
<g/>
.	.	kIx.	.
</s>
<s>
Lajka	lajka	k1gFnSc1	lajka
byla	být	k5eAaImAgFnS	být
kříženec	kříženec	k1gMnSc1	kříženec
<g/>
,	,	kIx,	,
fena	fena	k1gFnSc1	fena
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
tříletá	tříletý	k2eAgFnSc1d1	tříletá
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
dvouletá	dvouletý	k2eAgFnSc1d1	dvouletá
<g/>
)	)	kIx)	)
a	a	k8xC	a
vážila	vážit	k5eAaImAgFnS	vážit
6	[number]	k4	6
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchytu	odchyt	k1gInSc6	odchyt
dostala	dostat	k5eAaPmAgFnS	dostat
povícero	povícero	k6eAd1	povícero
jmen	jméno	k1gNnPc2	jméno
<g/>
,	,	kIx,	,
Kudrjavka	Kudrjavka	k1gFnSc1	Kudrjavka
<g/>
,	,	kIx,	,
Žučka	Žučka	k1gFnSc1	Žučka
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
Brouček	Brouček	k1gMnSc1	Brouček
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Limončik	Limončik	k1gMnSc1	Limončik
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
Citrónek	citrónek	k1gInSc1	citrónek
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Známou	známá	k1gFnSc7	známá
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jako	jako	k9	jako
Lajka	lajka	k1gFnSc1	lajka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
jméno	jméno	k1gNnSc4	jméno
plemen	plemeno	k1gNnPc2	plemeno
loveckých	lovecký	k2eAgMnPc2d1	lovecký
psů	pes	k1gMnPc2	pes
severního	severní	k2eAgNnSc2d1	severní
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
Sibiře	Sibiř	k1gFnSc2	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgNnPc1d1	americké
média	médium	k1gNnPc1	médium
ji	on	k3xPp3gFnSc4	on
pojmenovala	pojmenovat	k5eAaPmAgFnS	pojmenovat
Muttnik	Muttnik	k1gInSc1	Muttnik
(	(	kIx(	(
<g/>
složenina	složenina	k1gFnSc1	složenina
z	z	k7c2	z
mutt	mutta	k1gFnPc2	mutta
(	(	kIx(	(
<g/>
kříženec	kříženec	k1gMnSc1	kříženec
<g/>
)	)	kIx)	)
a	a	k8xC	a
-nik	ik	k6eAd1	-nik
ze	z	k7c2	z
Sputnik	sputnik	k1gInSc1	sputnik
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
Curly	Curla	k1gFnPc1	Curla
(	(	kIx(	(
<g/>
přeložená	přeložený	k2eAgFnSc1d1	přeložená
Kudrjavka	Kudrjavka	k1gFnSc1	Kudrjavka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Popisována	popisován	k2eAgFnSc1d1	popisována
byla	být	k5eAaImAgFnS	být
jako	jako	k9	jako
klidná	klidný	k2eAgFnSc1d1	klidná
<g/>
,	,	kIx,	,
nevyvolávající	vyvolávající	k2eNgFnPc1d1	nevyvolávající
rvačky	rvačka	k1gFnPc1	rvačka
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
psy	pes	k1gMnPc7	pes
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
let	let	k1gInSc4	let
na	na	k7c6	na
Sputniku	sputnik	k1gInSc6	sputnik
2	[number]	k4	2
byli	být	k5eAaImAgMnP	být
trénováni	trénován	k2eAgMnPc1d1	trénován
tři	tři	k4xCgMnPc1	tři
psi	pes	k1gMnPc1	pes
<g/>
:	:	kIx,	:
Albina	Albina	k1gMnSc1	Albina
<g/>
,	,	kIx,	,
Muška	muška	k1gFnSc1	muška
a	a	k8xC	a
Lajka	lajka	k1gFnSc1	lajka
<g/>
.	.	kIx.	.
</s>
<s>
Lékaři	lékař	k1gMnPc1	lékař
vybrali	vybrat	k5eAaPmAgMnP	vybrat
k	k	k7c3	k
letu	let	k1gInSc3	let
Lajku	lajka	k1gFnSc4	lajka
<g/>
,	,	kIx,	,
Albina	Albina	k1gFnSc1	Albina
předtím	předtím	k6eAd1	předtím
letěla	letět	k5eAaImAgFnS	letět
dvakrát	dvakrát	k6eAd1	dvakrát
na	na	k7c6	na
raketě	raketa	k1gFnSc6	raketa
R-1E	R-1E	k1gFnSc2	R-1E
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
byla	být	k5eAaImAgFnS	být
náhradníkem	náhradník	k1gMnSc7	náhradník
<g/>
,	,	kIx,	,
Muška	muška	k1gFnSc1	muška
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
k	k	k7c3	k
testování	testování	k1gNnSc3	testování
vybavení	vybavení	k1gNnSc2	vybavení
a	a	k8xC	a
systémů	systém	k1gInPc2	systém
podpory	podpora	k1gFnSc2	podpora
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Psi	pes	k1gMnPc1	pes
si	se	k3xPyFc3	se
na	na	k7c4	na
těsnou	těsný	k2eAgFnSc4d1	těsná
kabinu	kabina	k1gFnSc4	kabina
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
nemohli	moct	k5eNaImAgMnP	moct
otáčet	otáčet	k5eAaImF	otáčet
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
sednout	sednout	k5eAaPmF	sednout
<g/>
,	,	kIx,	,
postavit	postavit	k5eAaPmF	postavit
či	či	k8xC	či
lehnout	lehnout	k5eAaPmF	lehnout
<g/>
,	,	kIx,	,
zvykali	zvykat	k5eAaImAgMnP	zvykat
postupně	postupně	k6eAd1	postupně
<g/>
,	,	kIx,	,
když	když	k8xS	když
byli	být	k5eAaImAgMnP	být
drženi	držet	k5eAaImNgMnP	držet
ve	v	k7c6	v
stále	stále	k6eAd1	stále
menších	malý	k2eAgInPc6d2	menší
kotcích	kotec	k1gInPc6	kotec
<g/>
.	.	kIx.	.
</s>
<s>
Výzkumníci	výzkumník	k1gMnPc1	výzkumník
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
přizpůsobení	přizpůsobení	k1gNnSc3	přizpůsobení
se	se	k3xPyFc4	se
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
poměrně	poměrně	k6eAd1	poměrně
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
přetížení	přetížení	k1gNnSc4	přetížení
si	se	k3xPyFc3	se
zvykali	zvykat	k5eAaImAgMnP	zvykat
na	na	k7c6	na
centrifuze	centrifuga	k1gFnSc6	centrifuga
a	a	k8xC	a
taktéž	taktéž	k?	taktéž
byli	být	k5eAaImAgMnP	být
přivykáni	přivykat	k5eAaImNgMnP	přivykat
na	na	k7c4	na
hluk	hluk	k1gInSc4	hluk
při	při	k7c6	při
startu	start	k1gInSc6	start
rakety	raketa	k1gFnSc2	raketa
a	a	k8xC	a
na	na	k7c4	na
speciálně	speciálně	k6eAd1	speciálně
vyvinuté	vyvinutý	k2eAgNnSc4d1	vyvinuté
kašovité	kašovitý	k2eAgNnSc4d1	kašovité
jídlo	jídlo	k1gNnSc4	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
letem	let	k1gInSc7	let
vzal	vzít	k5eAaPmAgMnS	vzít
vedoucí	vedoucí	k1gMnSc1	vedoucí
lékařského	lékařský	k2eAgInSc2d1	lékařský
týmu	tým	k1gInSc2	tým
Valerij	Valerij	k1gMnSc1	Valerij
Jazdovskij	Jazdovskij	k1gMnSc1	Jazdovskij
Lajku	lajka	k1gFnSc4	lajka
k	k	k7c3	k
sobě	se	k3xPyFc3	se
domů	domů	k6eAd1	domů
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
pohrála	pohrát	k5eAaPmAgFnS	pohrát
s	s	k7c7	s
jeho	jeho	k3xOp3gFnPc7	jeho
dětmi	dítě	k1gFnPc7	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
vzpomínkách	vzpomínka	k1gFnPc6	vzpomínka
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Chtěl	chtít	k5eAaImAgMnS	chtít
jsem	být	k5eAaImIp1nS	být
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
udělat	udělat	k5eAaPmF	udělat
něco	něco	k3yInSc4	něco
pěkného	pěkný	k2eAgMnSc2d1	pěkný
<g/>
,	,	kIx,	,
když	když	k8xS	když
měla	mít	k5eAaImAgFnS	mít
žít	žít	k5eAaImF	žít
už	už	k6eAd1	už
jen	jen	k9	jen
krátce	krátce	k6eAd1	krátce
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
Podle	podle	k7c2	podle
NASA	NASA	kA	NASA
byla	být	k5eAaImAgFnS	být
Lajka	lajka	k1gFnSc1	lajka
umístěna	umístit	k5eAaPmNgFnS	umístit
do	do	k7c2	do
boxu	box	k1gInSc2	box
v	v	k7c6	v
družici	družice	k1gFnSc6	družice
už	už	k6eAd1	už
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1957	[number]	k4	1957
-	-	kIx~	-
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
před	před	k7c7	před
startem	start	k1gInSc7	start
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
zimě	zima	k1gFnSc3	zima
panující	panující	k2eAgFnSc2d1	panující
na	na	k7c6	na
kosmodromu	kosmodrom	k1gInSc6	kosmodrom
byla	být	k5eAaImAgFnS	být
družice	družice	k1gFnSc1	družice
vyhřívána	vyhřívat	k5eAaImNgFnS	vyhřívat
<g/>
.	.	kIx.	.
</s>
<s>
Lajčin	lajčin	k2eAgInSc4d1	lajčin
stav	stav	k1gInSc4	stav
hlídali	hlídat	k5eAaImAgMnP	hlídat
dva	dva	k4xCgMnPc1	dva
asistenti	asistent	k1gMnPc1	asistent
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
startem	start	k1gInSc7	start
ji	on	k3xPp3gFnSc4	on
pečlivě	pečlivě	k6eAd1	pečlivě
upravili	upravit	k5eAaPmAgMnP	upravit
a	a	k8xC	a
zkontrolovali	zkontrolovat	k5eAaPmAgMnP	zkontrolovat
čidla	čidlo	k1gNnPc4	čidlo
na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Raketa	raketa	k1gFnSc1	raketa
Sputnik	sputnik	k1gInSc1	sputnik
(	(	kIx(	(
<g/>
verze	verze	k1gFnSc1	verze
rakety	raketa	k1gFnSc2	raketa
R-	R-	k1gFnSc1	R-
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
vynesla	vynést	k5eAaPmAgFnS	vynést
Sputnik	sputnik	k1gInSc4	sputnik
2	[number]	k4	2
z	z	k7c2	z
kosmodromu	kosmodrom	k1gInSc2	kosmodrom
Bajkonur	Bajkonura	k1gFnPc2	Bajkonura
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
startu	start	k1gInSc2	start
se	se	k3xPyFc4	se
při	při	k7c6	při
nejvyšším	vysoký	k2eAgNnSc6d3	nejvyšší
přetížení	přetížení	k1gNnSc6	přetížení
dech	dech	k1gInSc4	dech
Lajky	lajka	k1gFnSc2	lajka
zrychlil	zrychlit	k5eAaPmAgMnS	zrychlit
třikrát	třikrát	k6eAd1	třikrát
až	až	k9	až
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
se	s	k7c7	s
stavem	stav	k1gInSc7	stav
před	před	k7c7	před
letem	let	k1gInSc7	let
<g/>
.	.	kIx.	.
</s>
<s>
Tep	tep	k1gInSc1	tep
se	se	k3xPyFc4	se
zvedl	zvednout	k5eAaPmAgInS	zvednout
ze	z	k7c2	z
103	[number]	k4	103
na	na	k7c4	na
240	[number]	k4	240
tepů	tep	k1gInPc2	tep
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
plánu	plán	k1gInSc2	plán
odhozen	odhozen	k2eAgInSc1d1	odhozen
kryt	kryt	k1gInSc1	kryt
družice	družice	k1gFnSc1	družice
<g/>
;	;	kIx,	;
družice	družice	k1gFnSc1	družice
však	však	k9	však
zůstala	zůstat	k5eAaPmAgFnS	zůstat
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
posledním	poslední	k2eAgInSc7d1	poslední
stupněm	stupeň	k1gInSc7	stupeň
nosné	nosný	k2eAgFnSc2d1	nosná
rakety	raketa	k1gFnSc2	raketa
(	(	kIx(	(
<g/>
blokem	blok	k1gInSc7	blok
A	A	kA	A
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
regulace	regulace	k1gFnSc2	regulace
teploty	teplota	k1gFnSc2	teplota
proto	proto	k8xC	proto
nebyl	být	k5eNaImAgInS	být
plně	plně	k6eAd1	plně
funkční	funkční	k2eAgInSc1d1	funkční
a	a	k8xC	a
kabina	kabina	k1gFnSc1	kabina
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
zahřála	zahřát	k5eAaPmAgFnS	zahřát
na	na	k7c4	na
40	[number]	k4	40
°	°	k?	°
<g/>
C.	C.	kA	C.
Po	po	k7c6	po
třech	tři	k4xCgFnPc6	tři
hodinách	hodina	k1gFnPc6	hodina
letu	let	k1gInSc2	let
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
beztíže	beztíže	k1gFnSc2	beztíže
se	se	k3xPyFc4	se
Lajčin	lajčin	k2eAgInSc1d1	lajčin
puls	puls	k1gInSc1	puls
vrátil	vrátit	k5eAaPmAgInS	vrátit
na	na	k7c4	na
102	[number]	k4	102
tepů	tep	k1gInPc2	tep
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
,	,	kIx,	,
trvalo	trvat	k5eAaImAgNnS	trvat
to	ten	k3xDgNnSc1	ten
třikrát	třikrát	k6eAd1	třikrát
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
při	při	k7c6	při
pozemních	pozemní	k2eAgInPc6d1	pozemní
testech	test	k1gInPc6	test
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
kvůli	kvůli	k7c3	kvůli
stresu	stres	k1gInSc2	stres
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Lajka	lajka	k1gFnSc1	lajka
byla	být	k5eAaImAgFnS	být
rozrušená	rozrušený	k2eAgFnSc1d1	rozrušená
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
najedla	najíst	k5eAaPmAgFnS	najíst
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pěti	pět	k4xCc6	pět
až	až	k9	až
sedmi	sedm	k4xCc6	sedm
hodinách	hodina	k1gFnPc6	hodina
letu	let	k1gInSc2	let
nejevila	jevit	k5eNaImAgFnS	jevit
známky	známka	k1gFnPc4	známka
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Sovětští	sovětský	k2eAgMnPc1d1	sovětský
vědci	vědec	k1gMnPc1	vědec
předem	předem	k6eAd1	předem
plánovali	plánovat	k5eAaImAgMnP	plánovat
že	že	k8xS	že
pes	pes	k1gMnSc1	pes
bude	být	k5eAaImBp3nS	být
usmrcen	usmrtit	k5eAaPmNgMnS	usmrtit
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
informací	informace	k1gFnPc2	informace
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
mělo	mít	k5eAaImAgNnS	mít
stát	stát	k5eAaImF	stát
naprogramovanou	naprogramovaný	k2eAgFnSc7d1	naprogramovaná
injekcí	injekce	k1gFnSc7	injekce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
po	po	k7c6	po
Lajčině	lajčin	k2eAgInSc6d1	lajčin
letu	let	k1gInSc6	let
vydával	vydávat	k5eAaImAgMnS	vydávat
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
rozporná	rozporný	k2eAgNnPc4d1	rozporné
prohlášení	prohlášení	k1gNnSc4	prohlášení
o	o	k7c6	o
jejím	její	k3xOp3gInSc6	její
osudu	osud	k1gInSc6	osud
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nich	on	k3xPp3gMnPc2	on
uhynula	uhynout	k5eAaPmAgFnS	uhynout
šestý	šestý	k4xOgInSc4	šestý
den	den	k1gInSc4	den
letu	let	k1gInSc2	let
následkem	následkem	k7c2	následkem
nedostatku	nedostatek	k1gInSc2	nedostatek
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
po	po	k7c6	po
selhání	selhání	k1gNnSc6	selhání
baterií	baterie	k1gFnPc2	baterie
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
byla	být	k5eAaImAgFnS	být
usmrcena	usmrcen	k2eAgFnSc1d1	usmrcena
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
se	se	k3xPyFc4	se
v	v	k7c6	v
ruských	ruský	k2eAgNnPc6d1	ruské
médiích	médium	k1gNnPc6	médium
objevily	objevit	k5eAaPmAgFnP	objevit
zprávy	zpráva	k1gFnPc1	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
zahynula	zahynout	k5eAaPmAgFnS	zahynout
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
den	den	k1gInSc4	den
následkem	následkem	k7c2	následkem
přehřátí	přehřátí	k1gNnSc2	přehřátí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2002	[number]	k4	2002
ve	v	k7c6	v
zprávě	zpráva	k1gFnSc6	zpráva
přednesené	přednesený	k2eAgFnSc2d1	přednesená
na	na	k7c6	na
Světovém	světový	k2eAgInSc6d1	světový
vesmírném	vesmírný	k2eAgInSc6d1	vesmírný
kongresu	kongres	k1gInSc6	kongres
(	(	kIx(	(
<g/>
World	World	k1gInSc1	World
Space	Spaec	k1gInSc2	Spaec
Congress	Congressa	k1gFnPc2	Congressa
<g/>
)	)	kIx)	)
v	v	k7c6	v
Houstonu	Houston	k1gInSc6	Houston
oznámil	oznámit	k5eAaPmAgMnS	oznámit
Dmitrij	Dmitrij	k1gMnSc3	Dmitrij
Malašenkov	Malašenkov	k1gInSc4	Malašenkov
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
vědců	vědec	k1gMnPc2	vědec
připravujících	připravující	k2eAgNnPc2d1	připravující
let	léto	k1gNnPc2	léto
Sputniku	sputnik	k1gInSc2	sputnik
2	[number]	k4	2
<g/>
,	,	kIx,	,
že	že	k8xS	že
uhynula	uhynout	k5eAaPmAgFnS	uhynout
během	během	k7c2	během
čtvrtého	čtvrtý	k4xOgInSc2	čtvrtý
oběhu	oběh	k1gInSc2	oběh
na	na	k7c4	na
následky	následek	k1gInPc4	následek
přehřátí	přehřátí	k1gNnSc2	přehřátí
<g/>
.	.	kIx.	.
</s>
<s>
Sputnik	sputnik	k1gInSc1	sputnik
2	[number]	k4	2
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Země	země	k1gFnSc1	země
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
výsledkem	výsledek	k1gInSc7	výsledek
letu	let	k1gInSc2	let
Lajky	lajka	k1gFnSc2	lajka
bylo	být	k5eAaImAgNnS	být
potvrzení	potvrzení	k1gNnSc1	potvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
živý	živý	k2eAgMnSc1d1	živý
tvor	tvor	k1gMnSc1	tvor
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
přežije	přežít	k5eAaPmIp3nS	přežít
vzlet	vzlet	k1gInSc1	vzlet
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
a	a	k8xC	a
delší	dlouhý	k2eAgInSc4d2	delší
pobyt	pobyt	k1gInSc4	pobyt
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
beztíže	beztíže	k1gFnSc2	beztíže
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
letu	let	k1gInSc6	let
před	před	k7c7	před
sovětskými	sovětský	k2eAgMnPc7d1	sovětský
vědci	vědec	k1gMnPc7	vědec
nadále	nadále	k6eAd1	nadále
nestála	stát	k5eNaImAgFnS	stát
otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jsou	být	k5eAaImIp3nP	být
lety	let	k1gInPc4	let
lidí	člověk	k1gMnPc2	člověk
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
bezpečně	bezpečně	k6eAd1	bezpečně
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
let	let	k1gInSc4	let
druhé	druhý	k4xOgFnSc2	druhý
sovětské	sovětský	k2eAgFnSc2d1	sovětská
družice	družice	k1gFnSc2	družice
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
s	s	k7c7	s
živým	živý	k1gMnSc7	živý
tvorem	tvor	k1gMnSc7	tvor
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
triumfem	triumf	k1gInSc7	triumf
sovětské	sovětský	k2eAgFnSc2d1	sovětská
propagandy	propaganda	k1gFnSc2	propaganda
a	a	k8xC	a
posílil	posílit	k5eAaPmAgInS	posílit
prestiž	prestiž	k1gFnSc4	prestiž
sovětské	sovětský	k2eAgFnSc2d1	sovětská
vědy	věda	k1gFnSc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
západní	západní	k2eAgMnPc1d1	západní
ochránci	ochránce	k1gMnPc1	ochránce
práv	právo	k1gNnPc2	právo
zvířat	zvíře	k1gNnPc2	zvíře
kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
vyslání	vyslání	k1gNnSc4	vyslání
Lajky	lajka	k1gFnSc2	lajka
na	na	k7c4	na
smrt	smrt	k1gFnSc4	smrt
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
spojovali	spojovat	k5eAaImAgMnP	spojovat
averzi	averze	k1gFnSc4	averze
k	k	k7c3	k
pokusům	pokus	k1gInPc3	pokus
na	na	k7c6	na
zvířatech	zvíře	k1gNnPc6	zvíře
s	s	k7c7	s
antikomunismem	antikomunismus	k1gInSc7	antikomunismus
<g/>
.	.	kIx.	.
</s>
<s>
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
vláda	vláda	k1gFnSc1	vláda
reagovala	reagovat	k5eAaBmAgFnS	reagovat
zavedením	zavedení	k1gNnSc7	zavedení
značky	značka	k1gFnSc2	značka
cigaret	cigareta	k1gFnPc2	cigareta
Lajka	lajka	k1gFnSc1	lajka
s	s	k7c7	s
jejím	její	k3xOp3gNnSc7	její
vyobrazením	vyobrazení	k1gNnSc7	vyobrazení
na	na	k7c6	na
krabičce	krabička	k1gFnSc6	krabička
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tak	tak	k6eAd1	tak
získala	získat	k5eAaPmAgFnS	získat
sympatie	sympatie	k1gFnPc4	sympatie
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
lety	let	k1gInPc1	let
psů	pes	k1gMnPc2	pes
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
Země	zem	k1gFnSc2	zem
probíhaly	probíhat	k5eAaImAgFnP	probíhat
od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
koncipovány	koncipovat	k5eAaBmNgFnP	koncipovat
jako	jako	k9	jako
návratové	návratový	k2eAgFnPc1d1	návratová
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
přípravy	příprava	k1gFnSc2	příprava
na	na	k7c4	na
let	let	k1gInSc4	let
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Lajka	lajka	k1gFnSc1	lajka
má	mít	k5eAaImIp3nS	mít
památník	památník	k1gInSc4	památník
-	-	kIx~	-
sochu	socha	k1gFnSc4	socha
a	a	k8xC	a
pamětní	pamětní	k2eAgFnSc4d1	pamětní
desku	deska	k1gFnSc4	deska
ve	v	k7c6	v
Hvězdném	hvězdný	k2eAgNnSc6d1	Hvězdné
městečku	městečko	k1gNnSc6	městečko
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
připravují	připravovat	k5eAaImIp3nP	připravovat
ruští	ruský	k2eAgMnPc1d1	ruský
kosmonauti	kosmonaut	k1gMnPc1	kosmonaut
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
pozemcích	pozemek	k1gInPc6	pozemek
Institutu	institut	k1gInSc2	institut
vojenské	vojenský	k2eAgFnSc2d1	vojenská
medicíny	medicína	k1gFnSc2	medicína
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
odhalen	odhalit	k5eAaPmNgInS	odhalit
další	další	k2eAgInSc1d1	další
Lajčin	lajčin	k2eAgInSc1d1	lajčin
památník	památník	k1gInSc1	památník
<g/>
.	.	kIx.	.
</s>
<s>
Lajka	lajka	k1gFnSc1	lajka
byla	být	k5eAaImAgFnS	být
navíc	navíc	k6eAd1	navíc
námětem	námět	k1gInSc7	námět
mnoha	mnoho	k4c2	mnoho
poštovních	poštovní	k2eAgFnPc2d1	poštovní
známek	známka	k1gFnPc2	známka
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
psa	pes	k1gMnSc2	pes
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
nevrátil	vrátit	k5eNaPmAgMnS	vrátit
z	z	k7c2	z
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
připomínán	připomínán	k2eAgInSc1d1	připomínán
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jejím	její	k3xOp3gInSc6	její
životě	život	k1gInSc6	život
byl	být	k5eAaImAgInS	být
napsán	napsat	k5eAaBmNgInS	napsat
komiks	komiks	k1gInSc4	komiks
Laika	laik	k1gMnSc2	laik
<g/>
,	,	kIx,	,
objevila	objevit	k5eAaPmAgFnS	objevit
se	se	k3xPyFc4	se
v	v	k7c6	v
písních	píseň	k1gFnPc6	píseň
-	-	kIx~	-
například	například	k6eAd1	například
Laika	laik	k1gMnSc2	laik
ICE	ICE	kA	ICE
MC	MC	kA	MC
<g/>
,	,	kIx,	,
její	její	k3xOp3gNnSc1	její
jméno	jméno	k1gNnSc1	jméno
nese	nést	k5eAaImIp3nS	nést
několik	několik	k4yIc4	několik
hudebních	hudební	k2eAgFnPc2d1	hudební
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jaro	jaro	k1gNnSc4	jaro
2013	[number]	k4	2013
připravuje	připravovat	k5eAaImIp3nS	připravovat
English	English	k1gInSc1	English
Touring	Touring	k1gInSc1	Touring
Opera	opera	k1gFnSc1	opera
uvedení	uvedení	k1gNnSc1	uvedení
opery	opera	k1gFnSc2	opera
Laika	laik	k1gMnSc2	laik
the	the	k?	the
Spacedog	Spacedog	k1gMnSc1	Spacedog
<g/>
.	.	kIx.	.
</s>
