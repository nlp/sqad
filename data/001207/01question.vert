<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
živý	živý	k2eAgMnSc1d1	živý
tvor	tvor	k1gMnSc1	tvor
z	z	k7c2	z
planety	planeta	k1gFnSc2	planeta
Země	zem	k1gFnSc2	zem
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
<g/>
?	?	kIx.	?
</s>
