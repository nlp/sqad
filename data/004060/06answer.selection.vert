<s>
Citrony	citron	k1gInPc1	citron
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
kyseliny	kyselina	k1gFnSc2	kyselina
askorbové	askorbový	k2eAgFnSc2d1	askorbová
a	a	k8xC	a
kyseliny	kyselina	k1gFnSc2	kyselina
citronové	citronový	k2eAgFnSc2d1	citronová
(	(	kIx(	(
<g/>
3,5	[number]	k4	3,5
<g/>
-	-	kIx~	-
<g/>
8,0	[number]	k4	8,0
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
100	[number]	k4	100
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
přibližně	přibližně	k6eAd1	přibližně
kolem	kolem	k7c2	kolem
87	[number]	k4	87
%	%	kIx~	%
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
