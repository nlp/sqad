<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Hastenbecku	Hastenbeck	k1gInSc2
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Hastenbecku	Hastenbeck	k1gInSc2
</s>
<s>
konflikt	konflikt	k1gInSc1
<g/>
:	:	kIx,
Sedmiletá	sedmiletý	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
1757	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Hastenbeck	Hastenbeck	k1gInSc1
<g/>
,	,	kIx,
Hannoverské	hannoverský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Francouzské	francouzský	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
Hannoversko	Hannoversko	k6eAd1
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
Hesensko-Kasselsko	Hesensko-Kasselsko	k1gNnSc1
Brunšvicko-Wolfenbüttelsko	Brunšvicko-Wolfenbüttelsko	k1gNnSc1
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
Vévoda	vévoda	k1gMnSc1
z	z	k7c2
Cumberlandu	Cumberland	k1gInSc2
</s>
<s>
Vévoda	vévoda	k1gMnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Estrées	Estrées	k1gMnSc1
Maršál	maršál	k1gMnSc1
Soubise	Soubise	k1gFnSc1
</s>
<s>
síla	síla	k1gFnSc1
</s>
<s>
~	~	kIx~
30	#num#	k4
000	#num#	k4
pěchota	pěchota	k1gFnSc1
<g/>
~	~	kIx~
5000	#num#	k4
jízda	jízda	k1gFnSc1
</s>
<s>
~	~	kIx~
50	#num#	k4
000	#num#	k4
pěchota	pěchota	k1gFnSc1
<g/>
~	~	kIx~
10	#num#	k4
000	#num#	k4
jízda	jízda	k1gFnSc1
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
~	~	kIx~
311	#num#	k4
mrtvých	mrtvý	k1gMnPc2
<g/>
~	~	kIx~
900	#num#	k4
raněných	raněný	k1gMnPc2
</s>
<s>
~	~	kIx~
1000	#num#	k4
mrtvých	mrtvý	k1gMnPc2
<g/>
~	~	kIx~
1300	#num#	k4
raněných	raněný	k1gMnPc2
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Hastenbecku	Hastenbeck	k1gInSc2
(	(	kIx(
<g/>
26	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1757	#num#	k4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
bitva	bitva	k1gFnSc1
probíhající	probíhající	k2eAgFnSc1d1
během	během	k7c2
sedmileté	sedmiletý	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Střetly	střetnout	k5eAaPmAgInP
se	se	k3xPyFc4
v	v	k7c6
ní	on	k3xPp3gFnSc6
vojska	vojsko	k1gNnPc1
Francie	Francie	k1gFnSc2
a	a	k8xC
spojeneckých	spojenecký	k2eAgFnPc2d1
sil	síla	k1gFnPc2
složených	složený	k2eAgInPc2d1
z	z	k7c2
armád	armáda	k1gFnPc2
Hannoverského	hannoverský	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
,	,	kIx,
Království	království	k1gNnSc2
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
<g/>
,	,	kIx,
Hesensko-Kasselsko	Hesensko-Kasselsko	k1gNnSc1
a	a	k8xC
Brunšvicko-Lünebursko	Brunšvicko-Lünebursko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojenecká	spojenecký	k2eAgNnPc1d1
vojska	vojsko	k1gNnPc1
byla	být	k5eAaImAgNnP
poražena	porazit	k5eAaPmNgNnP
nedaleko	nedaleko	k7c2
Hamelnu	Hamelna	k1gFnSc4
patřícího	patřící	k2eAgMnSc2d1
do	do	k7c2
Hannoverského	hannoverský	k2eAgNnSc2d1
kurfiřtství	kurfiřtství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Francouzskou	francouzský	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
vedl	vést	k5eAaImAgMnS
maršál	maršál	k1gMnSc1
vévoda	vévoda	k1gMnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Estrées	Estrées	k1gMnSc1
a	a	k8xC
Charles	Charles	k1gMnSc1
de	de	k?
Rohan	Rohan	k1gMnSc1
<g/>
,	,	kIx,
princ	princ	k1gMnSc1
de	de	k?
Soubise	Soubise	k1gFnSc1
<g/>
,	,	kIx,
spojenecká	spojenecký	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
byla	být	k5eAaImAgFnS
pod	pod	k7c7
velením	velení	k1gNnSc7
vévody	vévoda	k1gMnSc2
z	z	k7c2
Cumberlandu	Cumberland	k1gInSc2
syna	syn	k1gMnSc2
anglického	anglický	k2eAgMnSc2d1
krále	král	k1gMnSc2
Jiřího	Jiří	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzské	francouzský	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
porážce	porážka	k1gFnSc3
Hannoverského	hannoverský	k2eAgNnSc2d1
království	království	k1gNnSc2
a	a	k8xC
rozpadu	rozpad	k1gInSc2
personální	personální	k2eAgFnSc2d1
unie	unie	k1gFnSc2
Velká	velký	k2eAgFnSc1d1
Británie-Hannoverské	Británie-Hannoverský	k2eAgNnSc4d1
království	království	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Bitvy	bitva	k1gFnSc2
Sedmileté	sedmiletý	k2eAgFnSc2d1
války	válka	k1gFnSc2
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Lovosic	Lovosice	k1gInPc2
•	•	k?
Střetnutí	střetnutí	k1gNnSc4
u	u	k7c2
Liberce	Liberec	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Štěrbohol	Štěrbohol	k1gInSc4
(	(	kIx(
<g/>
u	u	k7c2
Prahy	Praha	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Obléhání	obléhání	k1gNnSc4
Prahy	Praha	k1gFnSc2
(	(	kIx(
<g/>
1757	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Kolína	Kolín	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Hastenbecku	Hastenbeck	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Groß-Jägersdorfu	Groß-Jägersdorf	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Moysu	Moys	k1gInSc2
•	•	k?
Obléhání	obléhání	k1gNnSc1
Olomouce	Olomouc	k1gFnSc2
(	(	kIx(
<g/>
1758	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
u	u	k7c2
Rossbachu	Rossbach	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Vratislavi	Vratislav	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Leuthenu	Leuthen	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Krefeldu	Krefeld	k1gInSc2
•	•	k?
Střetnutí	střetnutí	k1gNnSc1
u	u	k7c2
Guntramovic	Guntramovice	k1gFnPc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Domašova	Domašův	k2eAgInSc2d1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Sandershausenu	Sandershausen	k2eAgFnSc4d1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Zorndorfu	Zorndorf	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Hochkirchu	Hochkirch	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Bergenu	Bergen	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Mindenu	Minden	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Paltzigu	Paltzig	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Kunersdorfu	Kunersdorf	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Maxenu	Maxen	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Landeshutu	Landeshut	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Warburgu	Warburg	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Lehnice	Lehnice	k1gFnSc2
(	(	kIx(
<g/>
1760	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Torgau	Torgaus	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Burkersdorfu	Burkersdorf	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Freibergu	Freiberg	k1gInSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ph	ph	kA
<g/>
549745	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
7534691-6	7534691-6	k4
</s>
