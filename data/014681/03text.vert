<s>
Nakládané	nakládaný	k2eAgInPc4d1
okurky	okurek	k1gInPc4
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Nakládané	nakládaný	k2eAgInPc4d1
okurky	okurek	k1gInPc4
Nakládané	nakládaný	k2eAgFnSc2d1
okurky	okurka	k1gFnSc2
Složení	složení	k1gNnSc1
</s>
<s>
okurky	okurka	k1gFnPc1
<g/>
,	,	kIx,
lák	lák	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nakládané	nakládaný	k2eAgInPc4d1
okurky	okurek	k1gInPc4
<g/>
,	,	kIx,
v	v	k7c6
Česku	Česko	k1gNnSc6
známé	známá	k1gFnSc2
také	také	k9
jako	jako	k9
znojemské	znojemský	k2eAgInPc4d1
okurky	okurek	k1gInPc4
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
okurky	okurka	k1gFnPc1
zalité	zalitý	k2eAgNnSc1d1
sladkokyselým	sladkokyselý	k2eAgInSc7d1
nálevem	nálev	k1gInSc7
–	–	k?
lákem	lák	k1gInSc7
a	a	k8xC
posléze	posléze	k6eAd1
zavařené	zavařený	k2eAgNnSc1d1
(	(	kIx(
<g/>
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
kvašáků	kvašák	k1gInPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
konzervace	konzervace	k1gFnSc2
dosaženo	dosáhnout	k5eAaPmNgNnS
mléčným	mléčný	k2eAgNnSc7d1
kvašením	kvašení	k1gNnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakládané	nakládaný	k2eAgInPc1d1
okurky	okurek	k1gInPc1
jsou	být	k5eAaImIp3nP
klasický	klasický	k2eAgInSc4d1
pokrm	pokrm	k1gInSc4
mnoha	mnoho	k4c2
kuchyní	kuchyně	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejrozšířenější	rozšířený	k2eAgFnPc4d3
jsou	být	k5eAaImIp3nP
v	v	k7c6
české	český	k2eAgFnSc6d1
<g/>
,	,	kIx,
moravské	moravský	k2eAgFnSc6d1
<g/>
,	,	kIx,
slovenské	slovenský	k2eAgFnSc6d1
<g/>
,	,	kIx,
polské	polský	k2eAgFnSc6d1
a	a	k8xC
ruské	ruský	k2eAgFnSc6d1
kuchyni	kuchyně	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pověstné	pověstný	k2eAgInPc1d1
a	a	k8xC
ceněné	ceněný	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
také	také	k9
nakládané	nakládaný	k2eAgInPc4d1
okurky	okurek	k1gInPc4
z	z	k7c2
Lužice	Lužice	k1gFnSc2
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
zavařování	zavařování	k1gNnSc4
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
okurka	okurka	k1gFnSc1
polní	polní	k2eAgFnSc1d1
(	(	kIx(
<g/>
nakládačka	nakládačka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přidává	přidávat	k5eAaImIp3nS
se	se	k3xPyFc4
koření	koření	k1gNnSc1
a	a	k8xC
lák	lák	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
České	český	k2eAgFnPc1d1
nakládačky	nakládačka	k1gFnPc1
</s>
<s>
Tradičních	tradiční	k2eAgInPc2d1
receptů	recept	k1gInPc2
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
mnoho	mnoho	k4c1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následující	následující	k2eAgInSc4d1
recept	recept	k1gInSc4
i	i	k8xC
postup	postup	k1gInSc4
je	být	k5eAaImIp3nS
pouze	pouze	k6eAd1
ilustrační	ilustrační	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Příprava	příprava	k1gFnSc1
</s>
<s>
Do	do	k7c2
vypařených	vypařený	k2eAgFnPc2d1
sklenic	sklenice	k1gFnPc2
se	se	k3xPyFc4
naskládají	naskládat	k5eAaPmIp3nP
omyté	omytý	k2eAgFnPc4d1
propíchané	propíchaný	k2eAgFnPc4d1
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
okurky	okurka	k1gFnPc1
<g/>
,	,	kIx,
menší	malý	k2eAgFnPc1d2
a	a	k8xC
pevnější	pevný	k2eAgFnPc1d2
jsou	být	k5eAaImIp3nP
lepší	dobrý	k2eAgFnPc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přidá	přidat	k5eAaPmIp3nS
se	se	k3xPyFc4
několik	několik	k4yIc4
koleček	kolečko	k1gNnPc2
oloupané	oloupaný	k2eAgFnSc2d1
cibule	cibule	k1gFnSc2
<g/>
,	,	kIx,
mrkve	mrkev	k1gFnSc2
<g/>
,	,	kIx,
kořenové	kořenový	k2eAgFnSc2d1
petržele	petržel	k1gFnSc2
<g/>
,	,	kIx,
snítka	snítka	k1gFnSc1
kopru	kopr	k1gInSc2
<g/>
,	,	kIx,
1	#num#	k4
až	až	k9
2	#num#	k4
bobkové	bobkový	k2eAgInPc1d1
listy	list	k1gInPc1
<g/>
,	,	kIx,
3	#num#	k4
kuličky	kulička	k1gFnPc4
nového	nový	k2eAgNnSc2d1
koření	koření	k1gNnSc2
<g/>
,	,	kIx,
5	#num#	k4
kuliček	kulička	k1gFnPc2
pepře	pepř	k1gInSc2
<g/>
,	,	kIx,
10	#num#	k4
zrnek	zrnko	k1gNnPc2
hořčičného	hořčičný	k2eAgNnSc2d1
semínka	semínko	k1gNnSc2
a	a	k8xC
může	moct	k5eAaImIp3nS
se	se	k3xPyFc4
přidat	přidat	k5eAaPmF
několik	několik	k4yIc4
koleček	kolečko	k1gNnPc2
křenu	křen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takto	takto	k6eAd1
naplněná	naplněný	k2eAgFnSc1d1
sklenice	sklenice	k1gFnSc1
se	se	k3xPyFc4
poté	poté	k6eAd1
zalije	zalít	k5eAaPmIp3nS
lákem	lák	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Lák	lák	k1gInSc1
na	na	k7c4
okurky	okurek	k1gInPc4
</s>
<s>
Přípravek	přípravek	k1gInSc1
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
láku	lák	k1gInSc2
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
koupit	koupit	k5eAaPmF
v	v	k7c6
obchodě	obchod	k1gInSc6
(	(	kIx(
<g/>
např.	např.	kA
NOVA	nova	k1gFnSc1
a	a	k8xC
DEKA	deka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
vyrobit	vyrobit	k5eAaPmF
vlastní	vlastnit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Domácí	domácí	k2eAgInSc1d1
lák	lák	k1gInSc1
se	se	k3xPyFc4
připravuje	připravovat	k5eAaImIp3nS
přímo	přímo	k6eAd1
ve	v	k7c6
sklenici	sklenice	k1gFnSc6
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
přidá	přidat	k5eAaPmIp3nS
8	#num#	k4
lžiček	lžička	k1gFnPc2
cukru	cukr	k1gInSc2
nebo	nebo	k8xC
tabletek	tabletka	k1gFnPc2
sacharinu	sacharin	k1gInSc2
<g/>
,	,	kIx,
lžičku	lžička	k1gFnSc4
soli	sůl	k1gFnSc2
<g/>
,	,	kIx,
1	#num#	k4
dl	dl	k?
octa	ocet	k1gInSc2
a	a	k8xC
dolije	dolít	k5eAaPmIp3nS
se	s	k7c7
studenou	studený	k2eAgFnSc7d1
vodou	voda	k1gFnSc7
<g/>
,	,	kIx,
nebo	nebo	k8xC
se	se	k3xPyFc4
připraví	připravit	k5eAaPmIp3nS
zvlášť	zvlášť	k6eAd1
a	a	k8xC
do	do	k7c2
sklenice	sklenice	k1gFnSc2
dolévá	dolévat	k5eAaImIp3nS
hotový	hotový	k2eAgInSc1d1
<g/>
,	,	kIx,
vlažný	vlažný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
se	se	k3xPyFc4
svaří	svařit	k5eAaPmIp3nS
v	v	k7c6
1,7	1,7	k4
litru	litr	k1gInSc6
vody	voda	k1gFnSc2
<g/>
,	,	kIx,
380	#num#	k4
ml	ml	kA
octa	ocet	k1gInSc2
<g/>
,	,	kIx,
80	#num#	k4
g	g	kA
soli	sůl	k1gFnSc2
<g/>
,	,	kIx,
300	#num#	k4
g	g	kA
cukru	cukr	k1gInSc2
a	a	k8xC
16	#num#	k4
g	g	kA
kyseliny	kyselina	k1gFnSc2
citronové	citronový	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Zavaření	zavaření	k1gNnSc1
</s>
<s>
Sklenice	sklenice	k1gFnPc1
se	se	k3xPyFc4
pevně	pevně	k6eAd1
zašroubují	zašroubovat	k5eAaPmIp3nP
nebo	nebo	k8xC
zavřou	zavřít	k5eAaPmIp3nP
pomocí	pomocí	k7c2
zavařovací	zavařovací	k2eAgFnSc2d1
hlavy	hlava	k1gFnSc2
a	a	k8xC
vloží	vložit	k5eAaPmIp3nS
se	se	k3xPyFc4
do	do	k7c2
utěrkou	utěrka	k1gFnSc7
vyloženého	vyložený	k2eAgInSc2d1
hrnce	hrnec	k1gInSc2
s	s	k7c7
vodou	voda	k1gFnSc7
<g/>
,	,	kIx,
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
byly	být	k5eAaImAgFnP
sklenice	sklenice	k1gFnPc1
ponořené	ponořený	k2eAgFnPc1d1
zhruba	zhruba	k6eAd1
do	do	k7c2
tři	tři	k4xCgFnPc4
čtvrtě	čtvrt	k1gFnPc4
jejich	jejich	k3xOp3gFnSc2
výšky	výška	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sklenice	sklenice	k1gFnPc1
se	se	k3xPyFc4
sterilizují	sterilizovat	k5eAaImIp3nP
20	#num#	k4
minut	minuta	k1gFnPc2
při	při	k7c6
80	#num#	k4
až	až	k9
90	#num#	k4
stupních	stupeň	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dokončení	dokončení	k1gNnSc6
se	se	k3xPyFc4
nechájí	chájet	k5eNaPmIp3nS,k5eNaImIp3nS
zchladnout	zchladnout	k5eAaPmF
dnem	den	k1gInSc7
vzhůru	vzhůru	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Použití	použití	k1gNnSc1
</s>
<s>
Kyselé	kyselý	k2eAgFnPc1d1
okurky	okurka	k1gFnPc1
jsou	být	k5eAaImIp3nP
výborné	výborný	k2eAgFnPc1d1
k	k	k7c3
masům	maso	k1gNnPc3
<g/>
,	,	kIx,
pečeným	pečený	k2eAgInPc3d1
a	a	k8xC
vařeným	vařený	k2eAgInPc3d1
bramborům	brambor	k1gInPc3
<g/>
,	,	kIx,
těstovinám	těstovina	k1gFnPc3
<g/>
,	,	kIx,
hrachové	hrachový	k2eAgFnSc3d1
a	a	k8xC
bramborové	bramborový	k2eAgFnSc3d1
kaši	kaša	k1gFnSc3
a	a	k8xC
dalším	další	k2eAgInPc3d1
pokrmům	pokrm	k1gInPc3
a	a	k8xC
lák	láka	k1gFnPc2
od	od	k7c2
okurek	okurka	k1gFnPc2
může	moct	k5eAaImIp3nS
pomoci	pomoct	k5eAaPmF
při	při	k7c6
kocovině	kocovina	k1gFnSc6
kvůli	kvůli	k7c3
obsahu	obsah	k1gInSc3
solí	sůl	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Okurky	okurek	k1gInPc1
na	na	k7c4
nakládání	nakládání	k1gNnSc4
</s>
<s>
Hotové	hotový	k2eAgInPc4d1
kyselé	kyselý	k2eAgInPc4d1
okurky	okurek	k1gInPc4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
VESELÝ	Veselý	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
;	;	kIx,
PRÁVO	právo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
porazit	porazit	k5eAaPmF
kocovinu	kocovina	k1gFnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2010-12-31	2010-12-31	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Rychlokvašky	rychlokvašek	k1gInPc1
</s>
<s>
Nakládaná	nakládaný	k2eAgFnSc1d1
zelenina	zelenina	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
nakládané	nakládaný	k2eAgFnSc2d1
okurky	okurka	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Apetitonline	Apetitonlin	k1gInSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
Nakládané	nakládaný	k2eAgInPc4d1
okurky	okurek	k1gInPc4
</s>
<s>
Vareni	Varen	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
Nakládané	nakládaný	k2eAgInPc4d1
znojemské	znojemský	k2eAgInPc4d1
okurky	okurek	k1gInPc4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Gastronomie	gastronomie	k1gFnSc1
</s>
