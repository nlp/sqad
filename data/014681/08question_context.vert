<s desamb="1">
Domácí	domácí	k2eAgInSc1d1
lák	lák	k1gInSc1
se	se	k3xPyFc4
připravuje	připravovat	k5eAaImIp3nS
přímo	přímo	k6eAd1
ve	v	k7c6
sklenici	sklenice	k1gFnSc6
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
přidá	přidat	k5eAaPmIp3nS
8	#num#	k4
lžiček	lžička	k1gFnPc2
cukru	cukr	k1gInSc2
nebo	nebo	k8xC
tabletek	tabletka	k1gFnPc2
sacharinu	sacharin	k1gInSc2
<g/>
,	,	kIx,
lžičku	lžička	k1gFnSc4
soli	sůl	k1gFnSc2
<g/>
,	,	kIx,
1	#num#	k4
dl	dl	k?
octa	ocet	k1gInSc2
a	a	k8xC
dolije	dolít	k5eAaPmIp3nS
se	s	k7c7
studenou	studený	k2eAgFnSc7d1
vodou	voda	k1gFnSc7
<g/>
,	,	kIx,
nebo	nebo	k8xC
se	se	k3xPyFc4
připraví	připravit	k5eAaPmIp3nS
zvlášť	zvlášť	k6eAd1
a	a	k8xC
do	do	k7c2
sklenice	sklenice	k1gFnSc2
dolévá	dolévat	k5eAaImIp3nS
hotový	hotový	k2eAgInSc1d1
<g/>
,	,	kIx,
vlažný	vlažný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>