<s desamb="1">
Start	start	k1gInSc1
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
uskutečněn	uskutečnit	k5eAaPmNgInS
v	v	k7c6
lednu	leden	k1gInSc6
2022	#num#	k4
a	a	k8xC
má	mít	k5eAaImIp3nS
se	se	k3xPyFc4
jednat	jednat	k5eAaImF
o	o	k7c4
první	první	k4xOgNnSc4
let	léto	k1gNnPc2
vesmírných	vesmírný	k2eAgMnPc2d1
turistů	turist	k1gMnPc2
lodí	loď	k1gFnPc2
Crew	Crew	k1gMnSc1
Dragon	Dragon	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Přibližně	přibližně	k6eAd1
desetidenní	desetidenní	k2eAgFnSc4d1
misi	mise	k1gFnSc4
k	k	k7c3
Mezinárodní	mezinárodní	k2eAgFnSc3d1
vesmírné	vesmírný	k2eAgFnSc3d1
stanici	stanice	k1gFnSc3
(	(	kIx(
<g/>
ISS	ISS	kA
<g/>
)	)	kIx)
mají	mít	k5eAaImIp3nP
absolvovat	absolvovat	k5eAaPmF
čtyři	čtyři	k4xCgFnPc1
osoby	osoba	k1gFnPc1
<g/>
:	:	kIx,
profesionální	profesionální	k2eAgMnSc1d1
astronaut	astronaut	k1gMnSc1
Axiom	axiom	k1gInSc4
Space	Spaec	k1gInSc2
Michael	Michael	k1gMnSc1
López-Alegría	López-Alegría	k1gMnSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
turisté	turist	k1gMnPc1
Larry	Larra	k1gFnSc2
Connor	Connor	k1gMnSc1
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
Pathy	Patha	k1gFnSc2
a	a	k8xC
Ejtan	Ejtana	k1gFnPc2
Stibe	Stib	k1gInSc5
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
z	z	k7c2
nichž	jenž	k3xRgMnPc2
každý	každý	k3xTgMnSc1
zaplatil	zaplatit	k5eAaPmAgInS
za	za	k7c2
let	léto	k1gNnPc2
do	do	k7c2
vesmíru	vesmír	k1gInSc2
55	#num#	k4
milionů	milion	k4xCgInPc2
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
</s>