<s>
SpaceX	SpaceX	k?
Axiom	axiom	k1gInSc1
Space-	Space-	k1gFnSc1
<g/>
1	#num#	k4
</s>
<s>
SpaceX	SpaceX	k?
Axiom	axiom	k1gInSc1
Space-	Space-	k1gFnSc1
<g/>
1	#num#	k4
</s>
<s>
Údaje	údaj	k1gInPc1
o	o	k7c6
lodi	loď	k1gFnSc6
</s>
<s>
Typ	typ	k1gInSc1
</s>
<s>
Crew	Crew	k?
Dragon	Dragon	k1gMnSc1
</s>
<s>
Výrobní	výrobní	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
</s>
<s>
C207	C207	k4
(	(	kIx(
<g/>
Crew	Crew	k1gMnPc2
Dragon	Dragon	k1gMnSc1
Resilience	Resilience	k1gFnSc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výrobce	výrobce	k1gMnSc1
</s>
<s>
SpaceX	SpaceX	k?
</s>
<s>
Údaje	údaj	k1gInPc1
o	o	k7c6
letu	let	k1gInSc6
</s>
<s>
Členů	člen	k1gInPc2
posádky	posádka	k1gFnSc2
</s>
<s>
4	#num#	k4
</s>
<s>
Datum	datum	k1gNnSc1
startu	start	k1gInSc2
</s>
<s>
leden	leden	k1gInSc1
2022	#num#	k4
(	(	kIx(
<g/>
plánováno	plánovat	k5eAaImNgNnS
<g/>
)	)	kIx)
</s>
<s>
Kosmodrom	kosmodrom	k1gInSc1
</s>
<s>
Kennedyho	Kennedyze	k6eAd1
vesmírné	vesmírný	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
</s>
<s>
Vzletová	vzletový	k2eAgFnSc1d1
rampa	rampa	k1gFnSc1
</s>
<s>
LC-39A	LC-39A	k4
</s>
<s>
Nosná	nosný	k2eAgFnSc1d1
raketa	raketa	k1gFnSc1
</s>
<s>
Falcon	Falcon	k1gMnSc1
9	#num#	k4
Block	Block	k1gInSc1
5	#num#	k4
</s>
<s>
Spojení	spojení	k1gNnSc1
se	s	k7c7
stanicí	stanice	k1gFnSc7
</s>
<s>
Spojení	spojení	k1gNnSc1
se	s	k7c7
stanicí	stanice	k1gFnSc7
</s>
<s>
ISS	ISS	kA
(	(	kIx(
<g/>
plánováno	plánovat	k5eAaImNgNnS
<g/>
)	)	kIx)
</s>
<s>
Navigace	navigace	k1gFnSc1
</s>
<s>
Předchozí	předchozí	k2eAgFnSc1d1
</s>
<s>
Následující	následující	k2eAgInSc1d1
</s>
<s>
Crew-	Crew-	k?
<g/>
3	#num#	k4
</s>
<s>
Crew-	Crew-	k?
<g/>
4	#num#	k4
</s>
<s>
SpaceX	SpaceX	k?
Axiom	axiom	k1gInSc1
Space-	Space-	k1gFnSc1
<g/>
1	#num#	k4
(	(	kIx(
<g/>
Ax-	Ax-	k1gFnSc1
<g/>
1	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
plánovaný	plánovaný	k2eAgInSc4d1
let	let	k1gInSc4
americké	americký	k2eAgFnSc2d1
kosmické	kosmický	k2eAgFnSc2d1
lodi	loď	k1gFnSc2
Crew	Crew	k1gMnSc1
Dragon	Dragon	k1gMnSc1
firmy	firma	k1gFnSc2
SpaceX	SpaceX	k1gFnSc2
pro	pro	k7c4
společnost	společnost	k1gFnSc4
Axiom	axiom	k1gInSc4
Space	Spaec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Start	start	k1gInSc1
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
uskutečněn	uskutečnit	k5eAaPmNgInS
v	v	k7c6
lednu	leden	k1gInSc6
2022	#num#	k4
a	a	k8xC
má	mít	k5eAaImIp3nS
se	se	k3xPyFc4
jednat	jednat	k5eAaImF
o	o	k7c4
první	první	k4xOgNnSc4
let	léto	k1gNnPc2
vesmírných	vesmírný	k2eAgMnPc2d1
turistů	turist	k1gMnPc2
lodí	loď	k1gFnPc2
Crew	Crew	k1gMnSc1
Dragon	Dragon	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Přibližně	přibližně	k6eAd1
desetidenní	desetidenní	k2eAgFnSc4d1
misi	mise	k1gFnSc4
k	k	k7c3
Mezinárodní	mezinárodní	k2eAgFnSc3d1
vesmírné	vesmírný	k2eAgFnSc3d1
stanici	stanice	k1gFnSc3
(	(	kIx(
<g/>
ISS	ISS	kA
<g/>
)	)	kIx)
mají	mít	k5eAaImIp3nP
absolvovat	absolvovat	k5eAaPmF
čtyři	čtyři	k4xCgFnPc1
osoby	osoba	k1gFnPc1
<g/>
:	:	kIx,
profesionální	profesionální	k2eAgMnSc1d1
astronaut	astronaut	k1gMnSc1
Axiom	axiom	k1gInSc4
Space	Spaec	k1gInSc2
Michael	Michael	k1gMnSc1
López-Alegría	López-Alegría	k1gMnSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
turisté	turist	k1gMnPc1
Larry	Larra	k1gFnSc2
Connor	Connor	k1gMnSc1
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
Pathy	Patha	k1gFnSc2
a	a	k8xC
Ejtan	Ejtana	k1gFnPc2
Stibe	Stib	k1gInSc5
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
z	z	k7c2
nichž	jenž	k3xRgMnPc2
každý	každý	k3xTgMnSc1
zaplatil	zaplatit	k5eAaPmAgInS
za	za	k7c2
let	léto	k1gNnPc2
do	do	k7c2
vesmíru	vesmír	k1gInSc2
55	#num#	k4
milionů	milion	k4xCgInPc2
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
palubě	paluba	k1gFnSc6
ISS	ISS	kA
má	mít	k5eAaImIp3nS
tato	tento	k3xDgFnSc1
čtveřice	čtveřice	k1gFnSc1
pobývat	pobývat	k5eAaImF
osm	osm	k4xCc4
dní	den	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Původně	původně	k6eAd1
se	se	k3xPyFc4
předpokládalo	předpokládat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
mise	mise	k1gFnSc2
zúčastní	zúčastnit	k5eAaPmIp3nS
herec	herec	k1gMnSc1
Tom	Tom	k1gMnSc1
Cruise	Cruise	k1gFnSc2
s	s	k7c7
režisérem	režisér	k1gMnSc7
Dougem	Doug	k1gMnSc7
Limanem	Liman	k1gMnSc7
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
chtěli	chtít	k5eAaImAgMnP
na	na	k7c6
ISS	ISS	kA
natáčet	natáčet	k5eAaImF
film	film	k1gInSc4
<g/>
;	;	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
jejich	jejich	k3xOp3gInSc1
let	let	k1gInSc1
ale	ale	k9
byl	být	k5eAaImAgInS
o	o	k7c4
jeden	jeden	k4xCgInSc4
až	až	k8xS
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
odložen	odložen	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Posádka	posádka	k1gFnSc1
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1
posádka	posádka	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Michael	Michael	k1gMnSc1
López-Alegría	López-Alegría	k1gMnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Axiom	axiom	k1gInSc1
Space	Space	k1gFnSc2
–	–	k?
velitel	velitel	k1gMnSc1
</s>
<s>
Larry	Larr	k1gInPc1
Connor	Connora	k1gFnPc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
turista	turista	k1gMnSc1
–	–	k?
pilot	pilot	k1gMnSc1
</s>
<s>
Mark	Mark	k1gMnSc1
Pathy	Patha	k1gFnSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
turista	turista	k1gMnSc1
–	–	k?
letový	letový	k2eAgMnSc1d1
specialista	specialista	k1gMnSc1
</s>
<s>
Ejtan	Ejtan	k1gMnSc1
Stibe	Stib	k1gInSc5
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
turista	turista	k1gMnSc1
–	–	k?
letový	letový	k2eAgMnSc1d1
specialista	specialista	k1gMnSc1
</s>
<s>
V	v	k7c6
závorkách	závorka	k1gFnPc6
je	být	k5eAaImIp3nS
uveden	uveden	k2eAgInSc1d1
dosavadní	dosavadní	k2eAgInSc1d1
počet	počet	k1gInSc1
letů	let	k1gInPc2
do	do	k7c2
vesmíru	vesmír	k1gInSc2
včetně	včetně	k7c2
této	tento	k3xDgFnSc2
mise	mise	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Záložní	záložní	k2eAgFnSc1d1
posádka	posádka	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Peggy	Pegg	k1gInPc1
Whitsonová	Whitsonová	k1gFnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Axiom	axiom	k1gInSc1
Space	Space	k1gFnSc2
–	–	k?
velitelka	velitelka	k1gFnSc1
</s>
<s>
John	John	k1gMnSc1
Shoffner	Shoffner	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
turista	turista	k1gMnSc1
–	–	k?
pilot	pilot	k1gMnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
LÓPEZ-ALEGRÍA	LÓPEZ-ALEGRÍA	k1gMnSc1
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
God	God	k1gFnSc1
willin	willina	k1gFnPc2
<g/>
’	’	k?
and	and	k?
the	the	k?
creek	creek	k1gMnSc1
don	don	k1gMnSc1
<g/>
’	’	k?
<g/>
t	t	k?
rise	risat	k5eAaPmIp3nS
<g/>
,	,	kIx,
I	i	k8xC
<g/>
’	’	k?
<g/>
ll	ll	k?
be	be	k?
leading	leading	k1gInSc1
the	the	k?
Ax-	Ax-	k1gFnSc1
<g/>
1	#num#	k4
crew	crew	k?
on	on	k3xPp3gMnSc1
the	the	k?
first	first	k1gInSc1
purely	purela	k1gFnSc2
commercial	commerciat	k5eAaBmAgInS,k5eAaImAgInS,k5eAaPmAgInS
orbital	orbital	k1gInSc1
mission	mission	k1gInSc4
in	in	k?
history	histor	k1gInPc4
a	a	k8xC
little	little	k6eAd1
over	over	k1gMnSc1
a	a	k8xC
year	year	k1gMnSc1
from	from	k1gMnSc1
now	now	k?
-	-	kIx~
on	on	k3xPp3gInSc1
this	this	k1gInSc1
very	vera	k1gFnSc2
SpaceX	SpaceX	k1gMnSc2
Crew	Crew	k1gMnSc2
Dragon	Dragon	k1gMnSc1
capsule	capsule	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
It	It	k1gFnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
gonna	gonna	k6eAd1
be	be	k?
a	a	k8xC
#	#	kIx~
<g/>
Blast	Blast	k1gFnSc1
!	!	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Twitter	Twitter	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2020-11-16	2020-11-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
DUNN	DUNN	kA
<g/>
,	,	kIx,
Marcia	Marcia	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
st	st	kA
private	privat	k1gInSc5
space	spako	k6eAd1
crew	crew	k?
paying	paying	k1gInSc1
$	$	kIx~
<g/>
55	#num#	k4
<g/>
M	M	kA
each	eacha	k1gFnPc2
to	ten	k3xDgNnSc1
fly	fly	k?
to	ten	k3xDgNnSc1
station	station	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Apnews	Apnews	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2021-01-26	2021-01-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KLOTZ	KLOTZ	kA
<g/>
,	,	kIx,
Irene	Iren	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Space	Spaec	k1gInSc2
Hero	Hero	k6eAd1
mission	mission	k1gInSc1
is	is	k?
about	about	k2eAgInSc1d1
4	#num#	k4
<g/>
th	th	k?
on	on	k3xPp3gMnSc1
@	@	kIx~
<g/>
Axiom_Space	Axiom_Space	k1gFnSc1
manifest	manifest	k1gInSc1
<g/>
,	,	kIx,
Mike	Mike	k1gInSc1
Suffredini	Suffredin	k2eAgMnPc1d1
tells	tellsa	k1gFnPc2
@	@	kIx~
<g/>
AviationWeek	AviationWeky	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
First	First	k1gFnSc1
up	up	k?
in	in	k?
Oct	Oct	k1gFnSc2
'	'	kIx"
<g/>
21	#num#	k4
is	is	k?
flight	flight	k1gInSc1
of	of	k?
3	#num#	k4
private	privat	k1gInSc5
individuals	individuals	k6eAd1
and	and	k?
former	former	k1gInSc1
@	@	kIx~
<g/>
NASA_Astronauts	NASA_Astronauts	k1gInSc1
Mike	Mik	k1gInSc2
Lopez-Algeria	Lopez-Algerium	k1gNnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Twitter	Twitter	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2020-09-18	2020-09-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Meet	Meet	k1gInSc1
Ax-	Ax-	k1gFnSc1
<g/>
1	#num#	k4
<g/>
,	,	kIx,
The	The	k1gFnSc1
Beginning	Beginning	k1gInSc1
of	of	k?
a	a	k8xC
New	New	k1gMnSc1
Era	Era	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Axiom	axiom	k1gInSc1
Space	Space	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KEMP	kemp	k1gInSc1
<g/>
,	,	kIx,
Ella	Ella	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tom	Tom	k1gMnSc1
Cruise	Cruise	k1gFnSc2
is	is	k?
officially	officialla	k1gFnSc2
going	goinga	k1gFnPc2
to	ten	k3xDgNnSc1
space	space	k1gMnPc4
for	forum	k1gNnPc2
his	his	k1gNnSc2
next	next	k1gInSc4
movie	movie	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nme	Nme	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2020-09-22	2020-09-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BERGER	Berger	k1gMnSc1
<g/>
,	,	kIx,
Eric	Eric	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
There	Ther	k1gInSc5
are	ar	k1gInSc5
an	an	k?
insane	insanout	k5eAaPmIp3nS
amount	amount	k1gInSc1
of	of	k?
cool	coonout	k5eAaPmAgInS
space	spako	k6eAd1
things	things	k1gInSc1
happening	happening	k1gInSc1
in	in	k?
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ars	Ars	k1gFnSc1
Technica	Technica	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-01-05	2021-01-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Lety	let	k1gInPc1
Dragonů	Dragon	k1gMnPc2
a	a	k8xC
Dragonů	Dragon	k1gMnPc2
2	#num#	k4
od	od	k7c2
SpaceX	SpaceX	k1gFnSc2
Proběhlé	proběhlý	k2eAgFnSc2d1
mise	mise	k1gFnSc2
</s>
<s>
Zkušební	zkušební	k2eAgInPc4d1
lety	let	k1gInPc4
</s>
<s>
zkušební	zkušební	k2eAgInSc1d1
let	let	k1gInSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2010	#num#	k4
<g/>
,	,	kIx,
C	C	kA
<g/>
100	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
COTS	COTS	kA
Demo	demo	k2eAgInSc4d1
Flight	Flight	k1gInSc4
1	#num#	k4
(	(	kIx(
<g/>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
2010	#num#	k4
<g/>
,	,	kIx,
C	C	kA
<g/>
101	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
COTS	COTS	kA
Demo	demo	k2eAgInSc4d1
Flight	Flight	k1gInSc4
2	#num#	k4
(	(	kIx(
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
2012	#num#	k4
<g/>
,	,	kIx,
C	C	kA
<g/>
102	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pad	padnout	k5eAaPmDgInS
Abort	abort	k1gInSc4
Test	test	k1gInSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
<g/>
,	,	kIx,
C	C	kA
<g/>
200	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
SpX	SpX	k?
DM-1	DM-1	k1gFnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
<g/>
,	,	kIx,
C	C	kA
<g/>
201	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
test	test	k1gInSc1
úniku	únik	k1gInSc2
Dragonu	Dragon	k1gMnSc6
za	za	k7c2
letu	let	k1gInSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
<g/>
,	,	kIx,
C	C	kA
<g/>
205	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
SpX	SpX	k?
DM-2	DM-2	k1gFnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
<g/>
,	,	kIx,
C	C	kA
<g/>
206	#num#	k4
<g/>
)	)	kIx)
Nákladní	nákladní	k2eAgInPc4d1
lety	let	k1gInPc4
</s>
<s>
CRS-1	CRS-1	k4
(	(	kIx(
<g/>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
2012	#num#	k4
<g/>
,	,	kIx,
C	C	kA
<g/>
103	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CRS-2	CRS-2	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
2013	#num#	k4
<g/>
,	,	kIx,
C	C	kA
<g/>
104	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CRS-3	CRS-3	k4
(	(	kIx(
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
2014	#num#	k4
<g/>
,	,	kIx,
C	C	kA
<g/>
105	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CRS-4	CRS-4	k4
(	(	kIx(
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
2014	#num#	k4
<g/>
,	,	kIx,
C	C	kA
<g/>
106	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CRS-5	CRS-5	k4
(	(	kIx(
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
<g/>
,	,	kIx,
C	C	kA
<g/>
107	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CRS-6	CRS-6	k4
(	(	kIx(
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
<g/>
,	,	kIx,
C	C	kA
<g/>
108	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CRS-7	CRS-7	k4
†	†	k?
(	(	kIx(
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
<g/>
,	,	kIx,
C	C	kA
<g/>
109	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CRS-8	CRS-8	k4
(	(	kIx(
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
2016	#num#	k4
<g/>
,	,	kIx,
C	C	kA
<g/>
110	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CRS-9	CRS-9	k4
(	(	kIx(
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
2016	#num#	k4
<g/>
,	,	kIx,
C	C	kA
<g/>
111	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CRS-10	CRS-10	k4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
<g/>
,	,	kIx,
C	C	kA
<g/>
112	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CRS-11	CRS-11	k4
(	(	kIx(
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
<g/>
,	,	kIx,
C	C	kA
<g/>
106	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CRS-12	CRS-12	k4
(	(	kIx(
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
<g/>
,	,	kIx,
C	C	kA
<g/>
113	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CRS-13	CRS-13	k4
(	(	kIx(
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
<g/>
,	,	kIx,
C	C	kA
<g/>
108	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CRS-14	CRS-14	k4
(	(	kIx(
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
2018	#num#	k4
<g/>
,	,	kIx,
C	C	kA
<g/>
110	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CRS-15	CRS-15	k4
(	(	kIx(
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2018	#num#	k4
<g/>
,	,	kIx,
C	C	kA
<g/>
111	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CRS-16	CRS-16	k4
(	(	kIx(
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
2018	#num#	k4
<g/>
,	,	kIx,
C	C	kA
<g/>
112	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CRS-17	CRS-17	k4
(	(	kIx(
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
<g/>
,	,	kIx,
C	C	kA
<g/>
113	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CRS-18	CRS-18	k4
(	(	kIx(
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
<g/>
,	,	kIx,
C	C	kA
<g/>
108	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CRS-19	CRS-19	k4
(	(	kIx(
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
<g/>
,	,	kIx,
C	C	kA
<g/>
106	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CRS-20	CRS-20	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
<g/>
,	,	kIx,
C	C	kA
<g/>
112	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CRS-21	CRS-21	k4
(	(	kIx(
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
<g/>
,	,	kIx,
C	C	kA
<g/>
208	#num#	k4
<g/>
)	)	kIx)
Pilotované	pilotovaný	k2eAgInPc4d1
lety	let	k1gInPc4
</s>
<s>
SpX	SpX	k?
DM-2	DM-2	k1gFnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
<g/>
,	,	kIx,
C	C	kA
<g/>
206	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
SpX	SpX	k?
Crew-	Crew-	k1gFnSc1
<g/>
1	#num#	k4
(	(	kIx(
<g/>
11	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
<g/>
,	,	kIx,
C	C	kA
<g/>
207	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
SpX	SpX	k?
Crew-	Crew-	k1gFnSc1
<g/>
2	#num#	k4
(	(	kIx(
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
2021	#num#	k4
<g/>
,	,	kIx,
C	C	kA
<g/>
206	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Budoucí	budoucí	k2eAgFnSc1d1
mise	mise	k1gFnSc1
</s>
<s>
CRS-22	CRS-22	k4
</s>
<s>
Inspiration	Inspiration	k1gInSc1
<g/>
4	#num#	k4
</s>
<s>
SpX	SpX	k?
Crew-	Crew-	k1gFnSc1
<g/>
3	#num#	k4
</s>
<s>
SpX	SpX	k?
Ax-	Ax-	k1gFnSc1
<g/>
1	#num#	k4
</s>
<s>
SpX	SpX	k?
Crew-	Crew-	k1gFnSc1
<g/>
4	#num#	k4
Znovupoužité	znovupoužitý	k2eAgFnSc2d1
kabiny	kabina	k1gFnSc2
</s>
<s>
C106	C106	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
×	×	k?
<g/>
)	)	kIx)
</s>
<s>
C108	C108	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
×	×	k?
<g/>
)	)	kIx)
</s>
<s>
C110	C110	k4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
×	×	k?
<g/>
)	)	kIx)
</s>
<s>
C111	C111	k4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
×	×	k?
<g/>
)	)	kIx)
</s>
<s>
C112	C112	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
×	×	k?
<g/>
)	)	kIx)
</s>
<s>
C113	C113	k4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
×	×	k?
<g/>
)	)	kIx)
</s>
<s>
C206	C206	k4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
x	x	k?
<g/>
)	)	kIx)
Symbolem	symbol	k1gInSc7
†	†	k?
je	být	k5eAaImIp3nS
označena	označen	k2eAgFnSc1d1
neúspěšná	úspěšný	k2eNgFnSc1d1
mise	mise	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Starty	start	k1gInPc1
raket	raketa	k1gFnPc2
Falcon	Falcona	k1gFnPc2
společnosti	společnost	k1gFnSc2
SpaceX	SpaceX	k1gFnSc2
Rakety	raketa	k1gFnPc1
Falcon	Falcon	k1gNnSc1
</s>
<s>
Falcon	Falcon	k1gInSc1
1	#num#	k4
</s>
<s>
Falcon	Falcon	k1gInSc1
9	#num#	k4
</s>
<s>
v	v	k7c6
<g/>
1.0	1.0	k4
</s>
<s>
v	v	k7c6
<g/>
1.1	1.1	k4
</s>
<s>
Block	Block	k6eAd1
3	#num#	k4
</s>
<s>
Block	Block	k6eAd1
4	#num#	k4
</s>
<s>
Block	Block	k6eAd1
5	#num#	k4
</s>
<s>
Falcon	Falcon	k1gMnSc1
Heavy	Heava	k1gFnSc2
Falcon	Falcon	k1gInSc1
1	#num#	k4
</s>
<s>
Zkušební	zkušební	k2eAgInSc1d1
let	let	k1gInSc1
1	#num#	k4
<g/>
†	†	k?
(	(	kIx(
<g/>
FalconSAT-	FalconSAT-	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Zkušební	zkušební	k2eAgInSc1d1
let	let	k1gInSc1
2	#num#	k4
<g/>
†	†	k?
(	(	kIx(
<g/>
DemoSat	DemoSat	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Let	let	k1gInSc1
3	#num#	k4
<g/>
†	†	k?
</s>
<s>
Trailblazer	Trailblazer	k1gMnSc1
</s>
<s>
PRESat	PRESat	k1gInSc1
</s>
<s>
NanoSail-D	NanoSail-D	k?
</s>
<s>
Explorers	Explorers	k6eAd1
</s>
<s>
Ratsat	Ratsat	k5eAaImF,k5eAaPmF
</s>
<s>
RazakSAT	RazakSAT	k?
Falcon	Falcon	k1gNnSc1
9	#num#	k4
</s>
<s>
Zkušební	zkušební	k2eAgInPc4d1
lety	let	k1gInPc4
</s>
<s>
zkušební	zkušební	k2eAgInSc4d1
let	let	k1gInSc4
</s>
<s>
NASA	NASA	kA
COTS	COTS	kA
Demo	demo	k2eAgMnSc4d1
1	#num#	k4
</s>
<s>
NASA	NASA	kA
COTS	COTS	kA
Demo	demo	k2eAgInSc4d1
2	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
SpX	SpX	k?
DM-1	DM-1	k1gFnSc1
</s>
<s>
test	test	k1gInSc1
úniku	únik	k1gInSc2
Dragonu	Dragon	k1gMnSc6
za	za	k7c2
letu	let	k1gInSc2
</s>
<s>
SpX	SpX	k?
DM-2	DM-2	k1gMnSc1
Pilotované	pilotovaný	k2eAgInPc4d1
lety	let	k1gInPc4
</s>
<s>
SpX	SpX	k?
DM-2	DM-2	k1gFnSc1
</s>
<s>
SpX	SpX	k?
Crew-	Crew-	k1gFnSc1
<g/>
1	#num#	k4
</s>
<s>
SpX	SpX	k?
Crew-	Crew-	k1gFnSc1
<g/>
2	#num#	k4
</s>
<s>
SpX	SpX	k?
Crew-	Crew-	k1gFnSc1
<g/>
3	#num#	k4
</s>
<s>
SpX	SpX	k?
Ax-	Ax-	k1gFnSc1
<g/>
1	#num#	k4
Lety	let	k1gInPc1
Dragonu	Dragon	k1gMnSc3
k	k	k7c3
ISS	ISS	kA
</s>
<s>
CRS-1	CRS-1	k4
</s>
<s>
CRS-2	CRS-2	k4
</s>
<s>
CRS-3	CRS-3	k4
</s>
<s>
CRS-4	CRS-4	k4
</s>
<s>
CRS-5	CRS-5	k4
</s>
<s>
CRS-6	CRS-6	k4
</s>
<s>
CRS-	CRS-	k?
<g/>
7	#num#	k4
<g/>
†	†	k?
</s>
<s>
CRS-8	CRS-8	k4
</s>
<s>
CRS-9	CRS-9	k4
</s>
<s>
CRS-10	CRS-10	k4
</s>
<s>
CRS-11	CRS-11	k4
</s>
<s>
CRS-12	CRS-12	k4
</s>
<s>
CRS-13	CRS-13	k4
</s>
<s>
CRS-14	CRS-14	k4
</s>
<s>
CRS-15	CRS-15	k4
</s>
<s>
CRS-16	CRS-16	k4
</s>
<s>
CRS-17	CRS-17	k4
</s>
<s>
CRS-18	CRS-18	k4
</s>
<s>
CRS-19	CRS-19	k4
</s>
<s>
CRS-20	CRS-20	k4
</s>
<s>
CRS-21	CRS-21	k4
</s>
<s>
CRS-22	CRS-22	k4
</s>
<s>
CRS-23	CRS-23	k4
</s>
<s>
CRS-24	CRS-24	k4
</s>
<s>
CRS-25	CRS-25	k4
</s>
<s>
CRS-26	CRS-26	k4
Komerční	komerční	k2eAgInPc1d1
satelity	satelit	k1gInPc1
</s>
<s>
SES-8	SES-8	k4
</s>
<s>
Thaicom	Thaicom	k1gInSc1
6	#num#	k4
</s>
<s>
Orbcomm	Orbcomm	k1gInSc1
OG2	OG2	k1gFnPc2
let	léto	k1gNnPc2
1	#num#	k4
</s>
<s>
AsiaSat	AsiaSat	k1gInSc1
8	#num#	k4
</s>
<s>
AsiaSat	AsiaSat	k1gInSc1
6	#num#	k4
</s>
<s>
ABS-	ABS-	k?
<g/>
3	#num#	k4
<g/>
A	A	kA
<g/>
,	,	kIx,
Eutelsat	Eutelsat	k1gFnSc1
115	#num#	k4
West	West	k1gInSc1
B	B	kA
</s>
<s>
TurkmenSat	TurkmenSat	k1gInSc1
1	#num#	k4
</s>
<s>
Orbcomm	Orbcomm	k1gInSc1
OG2	OG2	k1gFnPc2
let	léto	k1gNnPc2
2	#num#	k4
</s>
<s>
SES-9	SES-9	k4
</s>
<s>
JCSAT-14	JCSAT-14	k4
</s>
<s>
Thaicom	Thaicom	k1gInSc1
8	#num#	k4
</s>
<s>
ABS-	ABS-	k?
<g/>
2	#num#	k4
<g/>
A	A	kA
<g/>
,	,	kIx,
Eutelsat	Eutelsat	k1gFnSc1
117	#num#	k4
West	West	k1gInSc1
B	B	kA
</s>
<s>
JCSAT-16	JCSAT-16	k4
</s>
<s>
Amos-	Amos-	k?
<g/>
6	#num#	k4
<g/>
†	†	k?
</s>
<s>
Iridium	iridium	k1gNnSc1
NEXT	NEXT	kA
1-10	1-10	k4
</s>
<s>
EchoStar	EchoStar	k1gInSc1
23	#num#	k4
</s>
<s>
SES-10	SES-10	k4
</s>
<s>
Inmarsat-	Inmarsat-	k?
<g/>
5	#num#	k4
F4	F4	k1gFnSc1
</s>
<s>
BulgariaSat-	BulgariaSat-	k?
<g/>
1	#num#	k4
</s>
<s>
Iridium	iridium	k1gNnSc1
NEXT	NEXT	kA
11-20	11-20	k4
</s>
<s>
Intelsat	Intelsat	k5eAaPmF,k5eAaImF
35	#num#	k4
<g/>
e	e	k0
</s>
<s>
Iridium	iridium	k1gNnSc1
NEXT	NEXT	kA
21-30	21-30	k4
</s>
<s>
SES-11	SES-11	k4
</s>
<s>
Koreasat	Koreasat	k5eAaImF,k5eAaPmF
5A	5A	k4
</s>
<s>
Iridium	iridium	k1gNnSc1
NEXT	NEXT	kA
31-40	31-40	k4
</s>
<s>
SES-16	SES-16	k4
(	(	kIx(
<g/>
GovSat-	GovSat-	k1gFnSc1
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Paz	Paz	k?
</s>
<s>
Hispasat	Hispasat	k5eAaPmF,k5eAaImF
30W-6	30W-6	k4
</s>
<s>
Iridium	iridium	k1gNnSc1
NEXT	NEXT	kA
41-50	41-50	k4
</s>
<s>
Bangabandhu-	Bangabandhu-	k?
<g/>
1	#num#	k4
</s>
<s>
Iridium	iridium	k1gNnSc1
NEXT	NEXT	kA
51-55	51-55	k4
</s>
<s>
SES-12	SES-12	k4
</s>
<s>
TelStar	TelStar	k1gInSc1
19V	19V	k4
</s>
<s>
Iridium	iridium	k1gNnSc1
NEXT	NEXT	kA
56-65	56-65	k4
</s>
<s>
Merah	Merah	k1gMnSc1
Putih	Putih	k1gMnSc1
</s>
<s>
TelStar	TelStar	k1gInSc1
18V	18V	k4
</s>
<s>
Es	es	k1gNnSc1
<g/>
'	'	kIx"
<g/>
hail	hail	k1gInSc1
2	#num#	k4
</s>
<s>
SSO-A	SSO-A	k?
</s>
<s>
Iridium	iridium	k1gNnSc1
NEXT	NEXT	kA
66-75	66-75	k4
</s>
<s>
Radarsat	Radarsat	k5eAaPmF,k5eAaImF
Constellation	Constellation	k1gInSc4
</s>
<s>
Amos-	Amos-	k?
<g/>
17	#num#	k4
</s>
<s>
JCSAT-	JCSAT-	k?
<g/>
18	#num#	k4
<g/>
/	/	kIx~
Kacific-	Kacific-	k1gFnSc2
<g/>
1	#num#	k4
</s>
<s>
SXM-7	SXM-7	k4
</s>
<s>
Türksat	Türksat	k5eAaPmF,k5eAaImF
5A	5A	k4
</s>
<s>
SXM-8	SXM-8	k4
</s>
<s>
Türksat	Türksat	k5eAaPmF,k5eAaImF
5B	5B	k4
</s>
<s>
WorldView-	WorldView-	k?
<g/>
1	#num#	k4
</s>
<s>
WorldView-	WorldView-	k?
<g/>
2	#num#	k4
</s>
<s>
ASBM	ASBM	kA
Interní	interní	k2eAgInSc1d1
lety	léto	k1gNnPc7
</s>
<s>
Starlink	Starlink	k1gInSc1
(	(	kIx(
<g/>
v	v	k7c4
<g/>
0.9	0.9	k4
<g/>
,	,	kIx,
V1	V1	k1gMnSc1
L1	L1	k1gFnSc2
‒	‒	k?
V1	V1	k1gFnSc2
L	L	kA
<g/>
16	#num#	k4
<g/>
,	,	kIx,
V1	V1	k1gFnSc1
L	L	kA
<g/>
17	#num#	k4
<g/>
,	,	kIx,
V1	V1	k1gFnSc1
L	L	kA
<g/>
18	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
SSP-1	SSP-1	k4
</s>
<s>
SSP-2	SSP-2	k4
</s>
<s>
SSP-3	SSP-3	k4
Vědecké	vědecký	k2eAgInPc1d1
satelity	satelit	k1gInPc1
</s>
<s>
CASSIOPE	CASSIOPE	kA
</s>
<s>
DSCOVR	DSCOVR	kA
</s>
<s>
Jason-	Jason-	k?
<g/>
3	#num#	k4
</s>
<s>
Formosat-	Formosat-	k?
<g/>
5	#num#	k4
</s>
<s>
TESS	TESS	kA
</s>
<s>
GRACE-FO	GRACE-FO	k?
</s>
<s>
SAOCOM	SAOCOM	kA
1A	1A	k4
</s>
<s>
SAOCOM	SAOCOM	kA
1B	1B	k4
</s>
<s>
Sentinel	sentinel	k1gInSc1
6A	6A	k4
</s>
<s>
IXPE	IXPE	kA
</s>
<s>
DART	DART	kA
</s>
<s>
SWOT	SWOT	kA
Vojenské	vojenský	k2eAgInPc1d1
satelity	satelit	k1gInPc1
</s>
<s>
NROL-76	NROL-76	k4
</s>
<s>
X-37B	X-37B	k4
OTV-5	OTV-5	k1gFnSc1
</s>
<s>
Zuma	Zuma	k6eAd1
</s>
<s>
GPSIII-SV01	GPSIII-SV01	k4
</s>
<s>
GPSIII-SV03	GPSIII-SV03	k4
</s>
<s>
Anasis-II	Anasis-II	k?
</s>
<s>
GPSIII-SV04	GPSIII-SV04	k4
</s>
<s>
GPSIII-SV05	GPSIII-SV05	k4
</s>
<s>
SARah	Sarah	k1gFnSc1
1	#num#	k4
</s>
<s>
GPSIII-SV06	GPSIII-SV06	k4
</s>
<s>
NROL-85	NROL-85	k4
</s>
<s>
NROL-87	NROL-87	k4
</s>
<s>
SARah	Sarah	k1gFnSc1
2	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
</s>
<s>
Falcon	Falcon	k1gInSc1
Heavy	Heava	k1gFnSc2
</s>
<s>
Zkušební	zkušební	k2eAgInSc4d1
let	let	k1gInSc4
</s>
<s>
ArabSat	ArabSat	k1gInSc1
6A	6A	k4
</s>
<s>
STP-2	STP-2	k4
</s>
<s>
AFSPC-44	AFSPC-44	k4
</s>
<s>
AFSPC-52	AFSPC-52	k4
</s>
<s>
Inmarsat	Inmarsat	k5eAaImF,k5eAaPmF
6B	6B	k4
</s>
<s>
ViaSat	ViaSat	k1gInSc1
3	#num#	k4
*	*	kIx~
Kurzíva	kurzíva	k1gFnSc1
značí	značit	k5eAaImIp3nS
budoucí	budoucí	k2eAgInPc4d1
lety	let	k1gInPc4
nebo	nebo	k8xC
vyvíjené	vyvíjený	k2eAgFnPc4d1
rakety	raketa	k1gFnPc4
<g/>
.	.	kIx.
†	†	k?
značí	značit	k5eAaImIp3nS
neúspěšné	úspěšný	k2eNgInPc4d1
lety	let	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Pilotované	pilotovaný	k2eAgInPc4d1
lety	let	k1gInPc4
na	na	k7c4
Mezinárodní	mezinárodní	k2eAgFnSc4d1
vesmírnou	vesmírný	k2eAgFnSc4d1
stanici	stanice	k1gFnSc4
Minulé	minulý	k2eAgFnSc2d1
</s>
<s>
1998	#num#	k4
(	(	kIx(
<g/>
STS-	STS-	k1gFnSc1
<g/>
88	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1999	#num#	k4
(	(	kIx(
<g/>
STS-	STS-	k1gFnSc1
<g/>
96	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2000	#num#	k4
(	(	kIx(
<g/>
STS-	STS-	k1gFnSc1
<g/>
101	#num#	k4
•	•	k?
106	#num#	k4
•	•	k?
92	#num#	k4
•	•	k?
Sojuz	Sojuz	k1gInSc1
TM-31	TM-31	k1gMnSc1
•	•	k?
STS-	STS-	k1gMnSc1
<g/>
97	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2001	#num#	k4
(	(	kIx(
<g/>
STS-	STS-	k1gFnSc1
<g/>
98	#num#	k4
•	•	k?
102	#num#	k4
•	•	k?
100	#num#	k4
•	•	k?
Sojuz	Sojuz	k1gInSc1
TM-32	TM-32	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
STS-104	STS-104	k1gFnSc2
•	•	k?
105	#num#	k4
•	•	k?
Sojuz	Sojuz	k1gInSc1
TM-33	TM-33	k1gMnSc1
•	•	k?
STS-	STS-	k1gMnSc1
<g/>
108	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2002	#num#	k4
(	(	kIx(
<g/>
STS-	STS-	k1gFnSc1
<g/>
110	#num#	k4
•	•	k?
Sojuz	Sojuz	k1gInSc1
TM-34	TM-34	k1gFnSc1
•	•	k?
STS-111	STS-111	k1gFnSc1
•	•	k?
112	#num#	k4
•	•	k?
Sojuz	Sojuz	k1gInSc1
TMA-1	TMA-1	k1gMnSc1
•	•	k?
STS-	STS-	k1gMnSc1
<g/>
113	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2003	#num#	k4
(	(	kIx(
<g/>
Sojuz	Sojuz	k1gInSc1
TMA-2	TMA-2	k1gFnSc2
•	•	k?
TMA-	TMA-	k1gMnSc1
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2004	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
Sojuz	Sojuz	k1gInSc1
TMA-4	TMA-4	k1gFnSc2
•	•	k?
TMA-	TMA-	k1gMnSc1
<g/>
5	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2005	#num#	k4
(	(	kIx(
<g/>
Sojuz	Sojuz	k1gInSc1
TMA-6	TMA-6	k1gFnSc2
•	•	k?
STS-114	STS-114	k1gFnSc2
•	•	k?
Sojuz	Sojuz	k1gInSc1
TMA-	TMA-	k1gFnSc1
<g/>
7	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2006	#num#	k4
(	(	kIx(
<g/>
Sojuz	Sojuz	k1gInSc1
TMA-8	TMA-8	k1gFnSc2
•	•	k?
STS-121	STS-121	k1gFnSc2
•	•	k?
115	#num#	k4
•	•	k?
Sojuz	Sojuz	k1gInSc1
TMA-9	TMA-9	k1gMnSc1
•	•	k?
STS-	STS-	k1gMnSc1
<g/>
116	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2007	#num#	k4
(	(	kIx(
<g/>
Sojuz	Sojuz	k1gInSc1
TMA-10	TMA-10	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
STS-117	STS-117	k1gFnSc2
•	•	k?
118	#num#	k4
•	•	k?
Sojuz	Sojuz	k1gInSc1
TMA-11	TMA-11	k1gMnSc1
•	•	k?
STS-	STS-	k1gMnSc1
<g/>
120	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2008	#num#	k4
(	(	kIx(
<g/>
STS-	STS-	k1gFnSc1
<g/>
122	#num#	k4
•	•	k?
123	#num#	k4
•	•	k?
Sojuz	Sojuz	k1gInSc1
TMA-12	TMA-12	k1gMnSc1
•	•	k?
STS-124	STS-124	k1gMnSc1
•	•	k?
Sojuz	Sojuz	k1gInSc1
TMA-13	TMA-13	k1gMnSc1
•	•	k?
STS-	STS-	k1gMnSc1
<g/>
126	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2009	#num#	k4
(	(	kIx(
<g/>
STS-	STS-	k1gFnSc1
<g/>
119	#num#	k4
•	•	k?
Sojuz	Sojuz	k1gInSc1
TMA-14	TMA-14	k1gMnSc1
•	•	k?
TMA-15	TMA-15	k1gMnSc1
•	•	k?
STS-127	STS-127	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
128	#num#	k4
•	•	k?
Sojuz	Sojuz	k1gInSc1
TMA-16	TMA-16	k1gMnSc1
•	•	k?
STS-129	STS-129	k1gMnSc1
•	•	k?
Sojuz	Sojuz	k1gInSc1
TMA-	TMA-	k1gFnSc1
<g/>
17	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2010	#num#	k4
(	(	kIx(
<g/>
STS-	STS-	k1gFnSc1
<g/>
130	#num#	k4
•	•	k?
Sojuz	Sojuz	k1gInSc1
TMA-18	TMA-18	k1gFnSc1
•	•	k?
STS-131	STS-131	k1gFnSc1
•	•	k?
132	#num#	k4
•	•	k?
Sojuz	Sojuz	k1gInSc1
TMA-19	TMA-19	k1gMnSc1
•	•	k?
TMA-01M	TMA-01M	k1gMnSc1
•	•	k?
TMA-	TMA-	k1gMnSc1
<g/>
20	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2011	#num#	k4
(	(	kIx(
<g/>
STS-	STS-	k1gFnSc1
<g/>
133	#num#	k4
•	•	k?
Sojuz	Sojuz	k1gInSc1
TMA-21	TMA-21	k1gFnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
STS-134	STS-134	k1gFnSc1
•	•	k?
Sojuz	Sojuz	k1gInSc1
TMA-02M	TMA-02M	k1gMnSc1
•	•	k?
STS-135	STS-135	k1gMnSc1
•	•	k?
Sojuz	Sojuz	k1gInSc1
TMA-22	TMA-22	k1gMnSc1
•	•	k?
TMA-	TMA-	k1gMnSc1
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
M	M	kA
<g/>
)	)	kIx)
•	•	k?
2012	#num#	k4
(	(	kIx(
<g/>
Sojuz	Sojuz	k1gInSc1
TMA-04M	TMA-04M	k1gFnSc2
•	•	k?
TMA-05M	TMA-05M	k1gMnSc1
•	•	k?
TMA-06M	TMA-06M	k1gMnSc1
•	•	k?
TMA-	TMA-	k1gMnSc1
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
M	M	kA
<g/>
)	)	kIx)
•	•	k?
2013	#num#	k4
(	(	kIx(
<g/>
Sojuz	Sojuz	k1gInSc1
TMA-08M	TMA-08M	k1gFnSc2
•	•	k?
TMA-09M	TMA-09M	k1gMnSc1
•	•	k?
TMA-10M	TMA-10M	k1gMnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
TMA-	TMA-	k1gFnSc1
<g/>
11	#num#	k4
<g/>
M	M	kA
<g/>
)	)	kIx)
•	•	k?
2014	#num#	k4
(	(	kIx(
<g/>
Sojuz	Sojuz	k1gInSc1
TMA-12M	TMA-12M	k1gFnSc2
•	•	k?
TMA-13M	TMA-13M	k1gMnSc1
•	•	k?
TMA-14M	TMA-14M	k1gMnSc1
•	•	k?
TMA-	TMA-	k1gMnSc1
<g/>
15	#num#	k4
<g/>
M	M	kA
<g/>
)	)	kIx)
•	•	k?
2015	#num#	k4
(	(	kIx(
<g/>
Sojuz	Sojuz	k1gInSc1
TMA-16M	TMA-16M	k1gFnSc2
•	•	k?
TMA-17M	TMA-17M	k1gMnSc1
•	•	k?
TMA-18M	TMA-18M	k1gMnSc1
•	•	k?
TMA-	TMA-	k1gMnSc1
<g/>
19	#num#	k4
<g/>
M	M	kA
<g/>
)	)	kIx)
•	•	k?
2016	#num#	k4
(	(	kIx(
<g/>
Sojuz	Sojuz	k1gInSc1
TMA-20M	TMA-20M	k1gFnSc2
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
MS-01	MS-01	k1gMnSc1
•	•	k?
MS-02	MS-02	k1gMnSc1
•	•	k?
MS-	MS-	k1gMnSc1
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2017	#num#	k4
(	(	kIx(
<g/>
Sojuz	Sojuz	k1gInSc1
MS-04	MS-04	k1gFnSc2
•	•	k?
MS-05	MS-05	k1gMnSc1
•	•	k?
MS-06	MS-06	k1gMnSc1
•	•	k?
MS-	MS-	k1gMnSc1
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2018	#num#	k4
(	(	kIx(
<g/>
Sojuz	Sojuz	k1gInSc1
MS-08	MS-08	k1gFnSc2
•	•	k?
MS-09	MS-09	k1gMnSc1
•	•	k?
MS-	MS-	k1gMnSc1
<g/>
10	#num#	k4
<g/>
†	†	k?
•	•	k?
MS-	MS-	k1gFnSc7
<g/>
11	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2019	#num#	k4
(	(	kIx(
<g/>
Sojuz	Sojuz	k1gInSc1
MS-12	MS-12	k1gFnSc2
•	•	k?
MS-13	MS-13	k1gMnSc1
•	•	k?
MS-14	MS-14	k1gMnSc1
•	•	k?
MS-	MS-	k1gMnSc1
<g/>
15	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2020	#num#	k4
(	(	kIx(
<g/>
Sojuz	Sojuz	k1gInSc1
MS-16	MS-16	k1gFnSc2
•	•	k?
SpaceX	SpaceX	k1gMnSc1
DM-2	DM-2	k1gMnSc1
•	•	k?
Sojuz	Sojuz	k1gInSc1
MS-	MS-	k1gFnSc1
<g/>
17	#num#	k4
<g/>
)	)	kIx)
Současné	současný	k2eAgNnSc1d1
</s>
<s>
SpaceX	SpaceX	k?
Crew-	Crew-	k1gFnSc1
<g/>
1	#num#	k4
•	•	k?
Sojuz	Sojuz	k1gInSc1
MS-18	MS-18	k1gMnSc1
•	•	k?
SpaceX	SpaceX	k1gMnSc1
Crew-	Crew-	k1gFnSc2
<g/>
2	#num#	k4
Plánované	plánovaný	k2eAgNnSc1d1
</s>
<s>
Starliner	Starliner	k1gMnSc1
Boe-CFT	Boe-CFT	k1gMnSc1
•	•	k?
SpaceX	SpaceX	k1gMnSc4
Crew-	Crew-	k1gMnSc4
<g/>
3	#num#	k4
•	•	k?
SpaceX	SpaceX	k1gMnSc1
Ax-	Ax-	k1gMnSc1
<g/>
1	#num#	k4
Symbolem	symbol	k1gInSc7
†	†	k?
je	být	k5eAaImIp3nS
označena	označen	k2eAgFnSc1d1
neúspěšná	úspěšný	k2eNgFnSc1d1
mise	mise	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Kosmonautika	kosmonautika	k1gFnSc1
</s>
