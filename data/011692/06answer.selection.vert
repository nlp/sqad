<s>
Noční	noční	k2eAgInSc4d1	noční
let	let	k1gInSc4	let
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Vol	vol	k6eAd1	vol
de	de	k?	de
nuit	nuit	k1gInSc1	nuit
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druhý	druhý	k4xOgInSc4	druhý
román	román	k1gInSc4	román
francouzského	francouzský	k2eAgMnSc2d1	francouzský
spisovatele	spisovatel	k1gMnSc2	spisovatel
a	a	k8xC	a
pilota	pilot	k1gMnSc2	pilot
Antoineho	Antoine	k1gMnSc2	Antoine
de	de	k?	de
Saint-Exupéryho	Saint-Exupéry	k1gMnSc2	Saint-Exupéry
<g/>
.	.	kIx.	.
</s>
