<p>
<s>
Noční	noční	k2eAgInSc4d1	noční
let	let	k1gInSc4	let
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Vol	vol	k6eAd1	vol
de	de	k?	de
nuit	nuit	k1gInSc1	nuit
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druhý	druhý	k4xOgInSc4	druhý
román	román	k1gInSc4	román
francouzského	francouzský	k2eAgMnSc2d1	francouzský
spisovatele	spisovatel	k1gMnSc2	spisovatel
a	a	k8xC	a
pilota	pilot	k1gMnSc2	pilot
Antoineho	Antoine	k1gMnSc2	Antoine
de	de	k?	de
Saint-Exupéryho	Saint-Exupéry	k1gMnSc2	Saint-Exupéry
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
věnován	věnovat	k5eAaPmNgInS	věnovat
Didieru	Didier	k1gMnSc3	Didier
Dauratovi	Daurat	k1gMnSc3	Daurat
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
bestsellerem	bestseller	k1gInSc7	bestseller
a	a	k8xC	a
zajistilo	zajistit	k5eAaPmAgNnS	zajistit
Exupérymu	Exupérym	k1gInSc2	Exupérym
dobré	dobrý	k2eAgNnSc4d1	dobré
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsah	obsah	k1gInSc1	obsah
díla	dílo	k1gNnSc2	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Celý	celý	k2eAgInSc1d1	celý
příběh	příběh	k1gInSc1	příběh
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
za	za	k7c4	za
jediné	jediný	k2eAgFnPc4d1	jediná
noci	noc	k1gFnPc4	noc
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
knihy	kniha	k1gFnSc2	kniha
autor	autor	k1gMnSc1	autor
popisuje	popisovat	k5eAaImIp3nS	popisovat
noční	noční	k2eAgInSc4d1	noční
let	let	k1gInSc4	let
pilota	pilot	k1gMnSc2	pilot
Fabiena	Fabien	k1gMnSc2	Fabien
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
Patagonie	Patagonie	k1gFnSc2	Patagonie
do	do	k7c2	do
Buenos	Buenosa	k1gFnPc2	Buenosa
Aires	Airesa	k1gFnPc2	Airesa
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
pocity	pocit	k1gInPc1	pocit
při	při	k7c6	při
něm.	něm.	k?	něm.
Zastihne	zastihnout	k5eAaPmIp3nS	zastihnout
ho	on	k3xPp3gInSc4	on
cyklónová	cyklónový	k2eAgFnSc1d1	Cyklónová
bouře	bouře	k1gFnSc1	bouře
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ho	on	k3xPp3gMnSc4	on
obklíčí	obklíčit	k5eAaPmIp3nS	obklíčit
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
Fabien	Fabien	k1gInSc4	Fabien
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
pilotů	pilot	k1gInPc2	pilot
<g/>
,	,	kIx,	,
neví	vědět	k5eNaImIp3nS	vědět
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
vystřelí	vystřelit	k5eAaPmIp3nS	vystřelit
svoji	svůj	k3xOyFgFnSc4	svůj
jedinou	jediný	k2eAgFnSc4d1	jediná
světlici	světlice	k1gFnSc4	světlice
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
letí	letět	k5eAaImIp3nS	letět
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
a	a	k8xC	a
nemá	mít	k5eNaImIp3nS	mít
tedy	tedy	k9	tedy
možnost	možnost	k1gFnSc4	možnost
kde	kde	k6eAd1	kde
přistát	přistát	k5eAaImF	přistát
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
vzlétnout	vzlétnout	k5eAaPmF	vzlétnout
do	do	k7c2	do
světla	světlo	k1gNnSc2	světlo
mraků	mrak	k1gInPc2	mrak
a	a	k8xC	a
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
uklidnil	uklidnit	k5eAaPmAgMnS	uklidnit
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
bude	být	k5eAaImBp3nS	být
znamenat	znamenat	k5eAaImF	znamenat
jeho	jeho	k3xOp3gInSc4	jeho
konec	konec	k1gInSc4	konec
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
tím	ten	k3xDgNnSc7	ten
vším	všecek	k3xTgNnSc7	všecek
autor	autor	k1gMnSc1	autor
popisuje	popisovat	k5eAaImIp3nS	popisovat
také	také	k9	také
situaci	situace	k1gFnSc4	situace
dole	dole	k6eAd1	dole
na	na	k7c4	na
zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
život	život	k1gInSc4	život
ředitele	ředitel	k1gMnSc2	ředitel
letectva	letectvo	k1gNnSc2	letectvo
Riviéra	Riviéra	k1gFnSc1	Riviéra
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vždy	vždy	k6eAd1	vždy
nese	nést	k5eAaImIp3nS	nést
velkou	velký	k2eAgFnSc4d1	velká
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
za	za	k7c4	za
životy	život	k1gInPc4	život
letců	letec	k1gMnPc2	letec
i	i	k8xC	i
celou	celý	k2eAgFnSc4d1	celá
rozváženou	rozvážený	k2eAgFnSc4d1	rozvážená
poštu	pošta	k1gFnSc4	pošta
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
na	na	k7c4	na
ostatní	ostatní	k2eAgMnPc4d1	ostatní
přísný	přísný	k2eAgMnSc1d1	přísný
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
dodržuje	dodržovat	k5eAaImIp3nS	dodržovat
předpisy	předpis	k1gInPc1	předpis
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
zda	zda	k8xS	zda
byly	být	k5eAaImAgInP	být
porušeny	porušit	k5eAaPmNgInP	porušit
vinou	vina	k1gFnSc7	vina
pilota	pilota	k1gFnSc1	pilota
nebo	nebo	k8xC	nebo
ne	ne	k9	ne
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
nitru	nitro	k1gNnSc6	nitro
duše	duše	k1gFnSc2	duše
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
úplně	úplně	k6eAd1	úplně
jiný	jiný	k2eAgMnSc1d1	jiný
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Inspektor	inspektor	k1gMnSc1	inspektor
Robineau	Robinea	k1gMnSc3	Robinea
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
spřátelit	spřátelit	k5eAaPmF	spřátelit
s	s	k7c7	s
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
pilotů	pilot	k1gMnPc2	pilot
-	-	kIx~	-
Pellerinem	Pellerin	k1gInSc7	Pellerin
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Riviére	Riviér	k1gMnSc5	Riviér
nabádá	nabádat	k5eAaImIp3nS	nabádat
Robineaua	Robineau	k1gInSc2	Robineau
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
mezi	mezi	k7c7	mezi
podřízenými	podřízený	k1gMnPc7	podřízený
nedělal	dělat	k5eNaImAgMnS	dělat
přátele	přítel	k1gMnPc4	přítel
a	a	k8xC	a
přikáže	přikázat	k5eAaPmIp3nS	přikázat
udělit	udělit	k5eAaPmF	udělit
Pellerinovi	Pellerin	k1gMnSc3	Pellerin
pokutu	pokuta	k1gFnSc4	pokuta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
pak	pak	k6eAd1	pak
čeká	čekat	k5eAaImIp3nS	čekat
Fabienova	Fabienův	k2eAgFnSc1d1	Fabienův
žena	žena	k1gFnSc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Riviére	Riviér	k1gMnSc5	Riviér
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
počasí	počasí	k1gNnSc6	počasí
nemá	mít	k5eNaImIp3nS	mít
pilot	pilot	k1gInSc1	pilot
šanci	šance	k1gFnSc4	šance
a	a	k8xC	a
určitě	určitě	k6eAd1	určitě
se	se	k3xPyFc4	se
už	už	k6eAd1	už
nevrátí	vrátit	k5eNaPmIp3nS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Fabien	Fabien	k1gInSc1	Fabien
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
nezvěstný	zvěstný	k2eNgInSc1d1	nezvěstný
<g/>
,	,	kIx,	,
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
vylétá	vylétat	k5eAaPmIp3nS	vylétat
další	další	k2eAgNnSc4d1	další
letadlo	letadlo	k1gNnSc4	letadlo
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
noční	noční	k2eAgInSc4d1	noční
let	let	k1gInSc4	let
s	s	k7c7	s
poštou	pošta	k1gFnSc7	pošta
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
