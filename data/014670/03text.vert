<s>
Bendersville	Bendersville	k1gFnSc1
(	(	kIx(
<g/>
Pensylvánie	Pensylvánie	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Bendersville	Bendersville	k1gFnSc1
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
39	#num#	k4
<g/>
°	°	k?
<g/>
58	#num#	k4
<g/>
′	′	k?
<g/>
57	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
77	#num#	k4
<g/>
°	°	k?
<g/>
14	#num#	k4
<g/>
′	′	k?
<g/>
59	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
215	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
UTC-	UTC-	k?
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
-	-	kIx~
<g/>
4	#num#	k4
v	v	k7c6
létě	léto	k1gNnSc6
Stát	stát	k1gInSc1
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgFnSc2d1
federální	federální	k2eAgFnSc2d1
stát	stát	k1gInSc1
</s>
<s>
Pensylvánie	Pensylvánie	k1gFnSc1
okres	okres	k1gInSc1
</s>
<s>
Adams	Adams	k1gInSc1
County	Counta	k1gFnSc2
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
1,1	1,1	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
641	#num#	k4
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
537	#num#	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Vznik	vznik	k1gInSc1
</s>
<s>
1832	#num#	k4
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
717	#num#	k4
PSČ	PSČ	kA
</s>
<s>
17306	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Bendersville	Bendersville	k6eAd1
je	být	k5eAaImIp3nS
obec	obec	k1gFnSc1
(	(	kIx(
<g/>
borough	borough	k1gInSc1
<g/>
)	)	kIx)
v	v	k7c4
Adams	Adams	k1gInSc4
County	Counta	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
Pensylvánii	Pensylvánie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Obec	obec	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1811	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
pojmenována	pojmenovat	k5eAaPmNgFnS
po	po	k7c6
pionýrovi	pionýr	k1gMnSc6
Henrym	Henry	k1gMnSc6
Benderovi	Bender	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
mezi	mezi	k7c7
lety	léto	k1gNnPc7
1890	#num#	k4
a	a	k8xC
2010	#num#	k4
</s>
<s>
1890	#num#	k4
</s>
<s>
1920	#num#	k4
</s>
<s>
1950	#num#	k4
</s>
<s>
1980	#num#	k4
</s>
<s>
2010	#num#	k4
</s>
<s>
370	#num#	k4
</s>
<s>
356	#num#	k4
</s>
<s>
409	#num#	k4
</s>
<s>
553	#num#	k4
</s>
<s>
641	#num#	k4
</s>
<s>
Náboženství	náboženství	k1gNnSc1
</s>
<s>
Nejvíce	nejvíce	k6eAd1,k6eAd3
obyvatel	obyvatel	k1gMnPc2
se	se	k3xPyFc4
hlásí	hlásit	k5eAaImIp3nS
ke	k	k7c3
katolické	katolický	k2eAgFnSc3d1
církvi	církev	k1gFnSc3
<g/>
,	,	kIx,
následuje	následovat	k5eAaImIp3nS
Evangelical	Evangelical	k1gMnSc1
Lutheran	Lutherana	k1gFnPc2
Church	Church	k1gMnSc1
in	in	k?
America	America	k1gMnSc1
a	a	k8xC
United	United	k1gMnSc1
Church	Church	k1gMnSc1
of	of	k?
Christ	Christ	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Bendersville	Bendersville	k1gNnSc2
<g/>
,	,	kIx,
Pennsylvania	Pennsylvanium	k1gNnPc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Bendersville	Bendersville	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
92088592	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
156082546	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
92088592	#num#	k4
</s>
