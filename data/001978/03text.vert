<s>
Sejm	Sejm	k1gInSc1	Sejm
Polské	polský	k2eAgFnSc2d1	polská
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
Sejm	Sejm	k1gInSc1	Sejm
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dolní	dolní	k2eAgFnSc1d1	dolní
komora	komora	k1gFnSc1	komora
Parlamentu	parlament	k1gInSc2	parlament
Polské	polský	k2eAgFnSc2d1	polská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Zasedá	zasedat	k5eAaImIp3nS	zasedat
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
460	[number]	k4	460
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
maršálek	maršálek	k1gMnSc1	maršálek
Sejmu	Sejm	k1gInSc2	Sejm
(	(	kIx(	(
<g/>
marszałek	marszałek	k6eAd1	marszałek
Sejmu	sejmout	k5eAaPmIp1nS	sejmout
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
posledních	poslední	k2eAgFnPc2d1	poslední
voleb	volba	k1gFnPc2	volba
jím	jíst	k5eAaImIp1nS	jíst
je	on	k3xPp3gNnSc4	on
Marek	Marek	k1gMnSc1	Marek
Kuchciński	Kuchciński	k1gNnSc2	Kuchciński
(	(	kIx(	(
<g/>
PiS	Pisa	k1gFnPc2	Pisa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Volební	volební	k2eAgNnSc1d1	volební
období	období	k1gNnSc1	období
je	být	k5eAaImIp3nS	být
čtyřleté	čtyřletý	k2eAgNnSc1d1	čtyřleté
<g/>
,	,	kIx,	,
společné	společný	k2eAgNnSc1d1	společné
pro	pro	k7c4	pro
obě	dva	k4xCgFnPc4	dva
komory	komora	k1gFnPc4	komora
a	a	k8xC	a
případné	případný	k2eAgNnSc4d1	případné
zkrácení	zkrácení	k1gNnSc4	zkrácení
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
pro	pro	k7c4	pro
Senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
příležitostně	příležitostně	k6eAd1	příležitostně
částí	část	k1gFnSc7	část
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
PR	pr	k0	pr
<g/>
,	,	kIx,	,
společného	společný	k2eAgNnSc2d1	společné
zasedání	zasedání	k1gNnSc2	zasedání
obou	dva	k4xCgFnPc2	dva
komor	komora	k1gFnPc2	komora
Parlamentu	parlament	k1gInSc2	parlament
(	(	kIx(	(
<g/>
Sejmu	Sejm	k1gInSc2	Sejm
a	a	k8xC	a
Senátu	senát	k1gInSc2	senát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
předsedá	předsedat	k5eAaImIp3nS	předsedat
maršálek	maršálek	k1gMnSc1	maršálek
Sejmu	Sejm	k1gInSc2	Sejm
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
zástupcem	zástupce	k1gMnSc7	zástupce
je	být	k5eAaImIp3nS	být
maršálek	maršálek	k1gMnSc1	maršálek
Senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Polské	polský	k2eAgFnSc2d1	polská
parlamentní	parlamentní	k2eAgFnSc2d1	parlamentní
volby	volba	k1gFnSc2	volba
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc4	výsledek
voleb	volba	k1gFnPc2	volba
do	do	k7c2	do
Sejmu	Sejm	k1gInSc2	Sejm
byly	být	k5eAaImAgFnP	být
<g/>
:	:	kIx,	:
</s>
