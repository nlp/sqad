<s>
Dobsonova	Dobsonův	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
</s>
<s>
Dobsonova	Dobsonův	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Dobson	Dobson	k1gMnSc1
Unit	Unit	k1gMnSc1
<g/>
,	,	kIx,
zkratka	zkratka	k1gFnSc1
DU	DU	k?
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
též	též	k9
D.U.	D.U.	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
standardní	standardní	k2eAgInSc1d1
způsob	způsob	k1gInSc1
vyjádření	vyjádření	k1gNnSc2
množství	množství	k1gNnSc2
ozonu	ozon	k1gInSc2
ve	v	k7c6
vertikálním	vertikální	k2eAgInSc6d1
sloupci	sloupec	k1gInSc6
atmosféry	atmosféra	k1gFnSc2
Země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
jednotka	jednotka	k1gFnSc1
je	být	k5eAaImIp3nS
2,69	2,69	k4
<g/>
×	×	k?
<g/>
1020	#num#	k4
ozonových	ozonový	k2eAgFnPc2d1
molekul	molekula	k1gFnPc2
na	na	k7c4
čtvereční	čtvereční	k2eAgInSc4d1
metr	metr	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
Dobsonova	Dobsonův	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
představuje	představovat	k5eAaImIp3nS
vrstvu	vrstva	k1gFnSc4
ozonu	ozon	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
by	by	kYmCp3nS
za	za	k7c2
standardních	standardní	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
byla	být	k5eAaImAgFnS
vysoká	vysoký	k2eAgFnSc1d1
10	#num#	k4
μ	μ	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Dlouhodobý	dlouhodobý	k2eAgInSc1d1
průměr	průměr	k1gInSc1
množství	množství	k1gNnSc2
ozonu	ozon	k1gInSc2
nad	nad	k7c7
Českou	český	k2eAgFnSc7d1
republikou	republika	k1gFnSc7
je	být	k5eAaImIp3nS
zhruba	zhruba	k6eAd1
400	#num#	k4
DU	DU	k?
<g/>
,	,	kIx,
tj.	tj.	kA
pouze	pouze	k6eAd1
4	#num#	k4
mm	mm	kA
<g/>
!	!	kIx.
</s>
<s desamb="1">
Světový	světový	k2eAgInSc1d1
průměr	průměr	k1gInSc1
je	být	k5eAaImIp3nS
350	#num#	k4
DU	DU	k?
(	(	kIx(
<g/>
3,5	3,5	k4
mm	mm	kA
<g/>
)	)	kIx)
a	a	k8xC
na	na	k7c6
Antarktidě	Antarktida	k1gFnSc6
byl	být	k5eAaImAgInS
pozorován	pozorovat	k5eAaImNgInS
i	i	k9
pokles	pokles	k1gInSc1
pod	pod	k7c7
90	#num#	k4
DU	DU	k?
<g/>
.	.	kIx.
</s>
<s>
Jednotka	jednotka	k1gFnSc1
byla	být	k5eAaImAgFnS
pojmenována	pojmenovat	k5eAaPmNgFnS
po	po	k7c6
Gordonovi	Gordon	k1gMnSc6
Dobsonovi	Dobson	k1gMnSc6
<g/>
,	,	kIx,
britském	britský	k2eAgInSc6d1
meteorologovi	meteorolog	k1gMnSc3
a	a	k8xC
fyzikovi	fyzik	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
vynalezl	vynaleznout	k5eAaPmAgMnS
způsob	způsob	k1gInSc4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
měřit	měřit	k5eAaImF
sílu	síla	k1gFnSc4
ozonové	ozonový	k2eAgFnSc2d1
vrstvy	vrstva	k1gFnSc2
z	z	k7c2
povrchu	povrch	k1gInSc2
Země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Meteorologický	meteorologický	k2eAgInSc4d1
slovník	slovník	k1gInSc4
-	-	kIx~
fulltextové	fulltextový	k2eAgNnSc4d1
hledání	hledání	k1gNnSc4
-	-	kIx~
jednotka	jednotka	k1gFnSc1
Dobsonova	Dobsonův	k2eAgFnSc1d1
<g/>
.	.	kIx.
slovnik	slovnik	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cmes	cmes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
