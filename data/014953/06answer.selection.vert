<s>
Jednotka	jednotka	k1gFnSc1
byla	být	k5eAaImAgFnS
pojmenována	pojmenovat	k5eAaPmNgFnS
po	po	k7c6
Gordonovi	Gordon	k1gMnSc6
Dobsonovi	Dobson	k1gMnSc6
<g/>
,	,	kIx,
britském	britský	k2eAgInSc6d1
meteorologovi	meteorolog	k1gMnSc3
a	a	k8xC
fyzikovi	fyzik	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
vynalezl	vynaleznout	k5eAaPmAgMnS
způsob	způsob	k1gInSc4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
měřit	měřit	k5eAaImF
sílu	síla	k1gFnSc4
ozonové	ozonový	k2eAgFnSc2d1
vrstvy	vrstva	k1gFnSc2
z	z	k7c2
povrchu	povrch	k1gInSc2
Země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>