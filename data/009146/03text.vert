<p>
<s>
Starověký	starověký	k2eAgInSc4d1	starověký
Řím	Řím	k1gInSc4	Řím
byla	být	k5eAaImAgFnS	být
civilizace	civilizace	k1gFnSc1	civilizace
vzešlá	vzešlý	k2eAgFnSc1d1	vzešlá
z	z	k7c2	z
města	město	k1gNnSc2	město
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
založeného	založený	k2eAgInSc2d1	založený
na	na	k7c6	na
Apeninském	apeninský	k2eAgInSc6d1	apeninský
poloostrově	poloostrov	k1gInSc6	poloostrov
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
expandovala	expandovat	k5eAaImAgFnS	expandovat
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
části	část	k1gFnSc2	část
antického	antický	k2eAgInSc2d1	antický
světa	svět	k1gInSc2	svět
a	a	k8xC	a
přetrvala	přetrvat	k5eAaPmAgFnS	přetrvat
až	až	k6eAd1	až
do	do	k7c2	do
5	[number]	k4	5
<g/>
.	.	kIx.	.
resp.	resp.	kA	resp.
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
Forma	forma	k1gFnSc1	forma
vlády	vláda	k1gFnSc2	vláda
Říma	Řím	k1gInSc2	Řím
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
staletí	staletí	k1gNnSc2	staletí
proměnila	proměnit	k5eAaPmAgFnS	proměnit
nejprve	nejprve	k6eAd1	nejprve
z	z	k7c2	z
království	království	k1gNnSc2	království
na	na	k7c4	na
republiku	republika	k1gFnSc4	republika
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
císařství	císařství	k1gNnSc6	císařství
<g/>
.	.	kIx.	.
</s>
<s>
Římské	římský	k2eAgNnSc1d1	římské
území	území	k1gNnSc1	území
bylo	být	k5eAaImAgNnS	být
obýváno	obývat	k5eAaImNgNnS	obývat
různými	různý	k2eAgInPc7d1	různý
národy	národ	k1gInPc7	národ
s	s	k7c7	s
rozlišnými	rozlišný	k2eAgInPc7d1	rozlišný
jazyky	jazyk	k1gInPc7	jazyk
<g/>
,	,	kIx,	,
náboženstvím	náboženství	k1gNnSc7	náboženství
a	a	k8xC	a
kulturou	kultura	k1gFnSc7	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
existence	existence	k1gFnSc2	existence
císařství	císařství	k1gNnSc4	císařství
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
univerzalistická	univerzalistický	k2eAgFnSc1d1	univerzalistická
myšlenka	myšlenka	k1gFnSc1	myšlenka
jediné	jediný	k2eAgFnSc2d1	jediná
a	a	k8xC	a
jednotné	jednotný	k2eAgFnSc2d1	jednotná
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
–	–	k?	–
imperium	imperium	k1gNnSc1	imperium
sine	sinus	k1gInSc5	sinus
fine	fine	k1gNnPc7	fine
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
říše	říše	k1gFnSc1	říše
bez	bez	k7c2	bez
hranic	hranice	k1gFnPc2	hranice
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vymezit	vymezit	k5eAaPmF	vymezit
dobu	doba	k1gFnSc4	doba
existence	existence	k1gFnSc2	existence
starověkého	starověký	k2eAgInSc2d1	starověký
Říma	Řím	k1gInSc2	Řím
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
jednak	jednak	k8xC	jednak
archaickým	archaický	k2eAgNnSc7d1	archaické
obdobím	období	k1gNnSc7	období
před	před	k7c7	před
založením	založení	k1gNnSc7	založení
města	město	k1gNnSc2	město
a	a	k8xC	a
jednak	jednak	k8xC	jednak
jeho	jeho	k3xOp3gInSc7	jeho
zánikem	zánik	k1gInSc7	zánik
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
transformací	transformace	k1gFnSc7	transformace
v	v	k7c4	v
pozdější	pozdní	k2eAgFnSc4d2	pozdější
byzantskou	byzantský	k2eAgFnSc4d1	byzantská
říši	říše	k1gFnSc4	říše
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgInSc2	svůj
největšího	veliký	k2eAgInSc2d3	veliký
rozsahu	rozsah	k1gInSc2	rozsah
<g/>
,	,	kIx,	,
za	za	k7c2	za
císaře	císař	k1gMnSc2	císař
Traiana	Traian	k1gMnSc2	Traian
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
moc	moc	k1gFnSc1	moc
Říma	Řím	k1gInSc2	Řím
rozprostírala	rozprostírat	k5eAaImAgFnS	rozprostírat
do	do	k7c2	do
všech	všecek	k3xTgFnPc2	všecek
zemí	zem	k1gFnPc2	zem
podél	podél	k7c2	podél
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
Galie	Galie	k1gFnSc2	Galie
<g/>
,	,	kIx,	,
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Řím	Řím	k1gInSc1	Řím
vládl	vládnout	k5eAaImAgInS	vládnout
nad	nad	k7c7	nad
většinou	většina	k1gFnSc7	většina
zemí	zem	k1gFnSc7	zem
tehdy	tehdy	k6eAd1	tehdy
známého	známý	k2eAgInSc2d1	známý
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
orbis	orbis	k1gInSc1	orbis
terrarum	terrarum	k1gInSc1	terrarum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Římem	Řím	k1gInSc7	Řím
založená	založený	k2eAgFnSc1d1	založená
říše	říše	k1gFnSc1	říše
posloužila	posloužit	k5eAaPmAgFnS	posloužit
jako	jako	k9	jako
nástroj	nástroj	k1gInSc4	nástroj
šíření	šíření	k1gNnSc2	šíření
klasické	klasický	k2eAgFnSc2d1	klasická
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
do	do	k7c2	do
všech	všecek	k3xTgFnPc2	všecek
jím	jíst	k5eAaImIp1nS	jíst
podmaněných	podmaněný	k2eAgInPc2d1	podmaněný
krajů	kraj	k1gInPc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
životní	životní	k2eAgFnSc2d1	životní
úrovně	úroveň	k1gFnSc2	úroveň
a	a	k8xC	a
počtu	počet	k1gInSc2	počet
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Africe	Afrika	k1gFnSc3	Afrika
znovu	znovu	k6eAd1	znovu
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
až	až	k9	až
o	o	k7c4	o
mnoho	mnoho	k4c4	mnoho
staletí	staletí	k1gNnPc2	staletí
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
impéria	impérium	k1gNnSc2	impérium
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
promísení	promísení	k1gNnSc3	promísení
latinské	latinský	k2eAgFnSc2d1	Latinská
kultury	kultura	k1gFnSc2	kultura
s	s	k7c7	s
helénistickými	helénistický	k2eAgInPc7d1	helénistický
a	a	k8xC	a
orientálními	orientální	k2eAgInPc7d1	orientální
elementy	element	k1gInPc7	element
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
západ	západ	k1gInSc1	západ
byl	být	k5eAaImAgInS	být
zcela	zcela	k6eAd1	zcela
romanizován	romanizovat	k5eAaBmNgInS	romanizovat
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
navíc	navíc	k6eAd1	navíc
neměli	mít	k5eNaImAgMnP	mít
vliv	vliv	k1gInSc4	vliv
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
území	území	k1gNnSc6	území
jimi	on	k3xPp3gFnPc7	on
bezprostředně	bezprostředně	k6eAd1	bezprostředně
ovládaná	ovládaný	k2eAgFnSc1d1	ovládaná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c4	na
okolní	okolní	k2eAgFnPc4d1	okolní
země	zem	k1gFnPc4	zem
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgFnP	nacházet
mimo	mimo	k7c4	mimo
jejich	jejich	k3xOp3gFnSc4	jejich
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Latina	latina	k1gFnSc1	latina
<g/>
,	,	kIx,	,
jazyk	jazyk	k1gInSc1	jazyk
Římanů	Říman	k1gMnPc2	Říman
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
úřední	úřední	k2eAgFnSc7d1	úřední
řečí	řeč	k1gFnSc7	řeč
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
částech	část	k1gFnPc6	část
impéria	impérium	k1gNnSc2	impérium
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
řečtina	řečtina	k1gFnSc1	řečtina
používána	používat	k5eAaImNgFnS	používat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
latina	latina	k1gFnSc1	latina
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
přesto	přesto	k8xC	přesto
dokázaly	dokázat	k5eAaPmAgFnP	dokázat
udržet	udržet	k5eAaPmF	udržet
i	i	k9	i
původní	původní	k2eAgInPc4d1	původní
jazyky	jazyk	k1gInPc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Latina	latina	k1gFnSc1	latina
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
po	po	k7c4	po
staletí	staletí	k1gNnPc4	staletí
jazykem	jazyk	k1gInSc7	jazyk
Evropanů	Evropan	k1gMnPc2	Evropan
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
až	až	k9	až
do	do	k7c2	do
nástupu	nástup	k1gInSc2	nástup
baroka	baroko	k1gNnSc2	baroko
také	také	k9	také
jazykem	jazyk	k1gInSc7	jazyk
vzdělanců	vzdělanec	k1gMnPc2	vzdělanec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
římskokatolické	římskokatolický	k2eAgFnSc6d1	Římskokatolická
církvi	církev	k1gFnSc6	církev
zůstala	zůstat	k5eAaPmAgFnS	zůstat
do	do	k7c2	do
dob	doba	k1gFnPc2	doba
Druhého	druhý	k4xOgInSc2	druhý
vatikánského	vatikánský	k2eAgInSc2d1	vatikánský
koncilu	koncil	k1gInSc2	koncil
jediným	jediný	k2eAgInSc7d1	jediný
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
byly	být	k5eAaImAgFnP	být
vykonávány	vykonáván	k2eAgFnPc4d1	vykonávána
mše	mše	k1gFnPc4	mše
a	a	k8xC	a
bohoslužby	bohoslužba	k1gFnPc4	bohoslužba
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
vědách	věda	k1gFnPc6	věda
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
nebo	nebo	k8xC	nebo
biologii	biologie	k1gFnSc6	biologie
<g/>
,	,	kIx,	,
uplatňovány	uplatňován	k2eAgInPc4d1	uplatňován
latinské	latinský	k2eAgInPc4d1	latinský
odborné	odborný	k2eAgInPc4d1	odborný
výrazy	výraz	k1gInPc4	výraz
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
jsou	být	k5eAaImIp3nP	být
vytvářeny	vytvářet	k5eAaImNgInP	vytvářet
<g/>
.	.	kIx.	.
</s>
<s>
Latinský	latinský	k2eAgInSc1d1	latinský
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
původ	původ	k1gInSc4	původ
moderních	moderní	k2eAgInPc2d1	moderní
románských	románský	k2eAgInPc2d1	románský
jazyků	jazyk	k1gInPc2	jazyk
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
italština	italština	k1gFnSc1	italština
<g/>
,	,	kIx,	,
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
,	,	kIx,	,
španělština	španělština	k1gFnSc1	španělština
<g/>
,	,	kIx,	,
portugalština	portugalština	k1gFnSc1	portugalština
<g/>
,	,	kIx,	,
rumunština	rumunština	k1gFnSc1	rumunština
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
mnoho	mnoho	k4c4	mnoho
převzatých	převzatý	k2eAgNnPc2d1	převzaté
latinských	latinský	k2eAgNnPc2d1	latinské
slov	slovo	k1gNnPc2	slovo
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
také	také	k9	také
v	v	k7c6	v
germánských	germánský	k2eAgInPc6d1	germánský
a	a	k8xC	a
slovanských	slovanský	k2eAgInPc6d1	slovanský
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Evropské	evropský	k2eAgNnSc1d1	Evropské
právo	právo	k1gNnSc1	právo
a	a	k8xC	a
státověda	státověda	k1gFnSc1	státověda
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
občanské	občanský	k2eAgNnSc1d1	občanské
právo	právo	k1gNnSc1	právo
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
kořeny	kořen	k1gInPc1	kořen
v	v	k7c6	v
někdejším	někdejší	k2eAgNnSc6d1	někdejší
římském	římský	k2eAgNnSc6d1	římské
právu	právo	k1gNnSc6	právo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Království	království	k1gNnSc1	království
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Legendy	legenda	k1gFnSc2	legenda
a	a	k8xC	a
králové	králová	k1gFnSc2	králová
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
starořímských	starořímský	k2eAgInPc2d1	starořímský
pramenů	pramen	k1gInPc2	pramen
se	se	k3xPyFc4	se
založení	založení	k1gNnSc1	založení
města	město	k1gNnSc2	město
Říma	Řím	k1gInSc2	Řím
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
753	[number]	k4	753
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
přijato	přijmout	k5eAaPmNgNnS	přijmout
za	za	k7c4	za
počátek	počátek	k1gInSc4	počátek
římského	římský	k2eAgInSc2d1	římský
letopočtu	letopočet	k1gInSc2	letopočet
(	(	kIx(	(
<g/>
ab	ab	k?	ab
urbe	urbat	k5eAaPmIp3nS	urbat
condita	condita	k1gFnSc1	condita
–	–	k?	–
od	od	k7c2	od
založení	založení	k1gNnSc2	založení
města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
již	již	k6eAd1	již
od	od	k7c2	od
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
existovala	existovat	k5eAaImAgFnS	existovat
na	na	k7c6	na
Kapitolu	Kapitol	k1gInSc6	Kapitol
a	a	k8xC	a
Palatinu	Palatin	k1gInSc6	Palatin
první	první	k4xOgNnPc4	první
sídla	sídlo	k1gNnPc4	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
osady	osada	k1gFnPc1	osada
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
i	i	k9	i
na	na	k7c4	na
okolní	okolní	k2eAgInPc4d1	okolní
pahorky	pahorek	k1gInPc4	pahorek
při	při	k7c6	při
řece	řeka	k1gFnSc6	řeka
Tibeře	Tibera	k1gFnSc6	Tibera
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterých	který	k3yIgFnPc2	který
pak	pak	k6eAd1	pak
jejich	jejich	k3xOp3gNnSc7	jejich
pozvolným	pozvolný	k2eAgNnSc7d1	pozvolné
spojováním	spojování	k1gNnSc7	spojování
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
náboženským	náboženský	k2eAgNnSc7d1	náboženské
a	a	k8xC	a
politickým	politický	k2eAgNnSc7d1	politické
centrem	centrum	k1gNnSc7	centrum
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Kapitol	Kapitol	k1gInSc4	Kapitol
<g/>
.	.	kIx.	.
</s>
<s>
Založení	založení	k1gNnSc1	založení
města	město	k1gNnSc2	město
uprostřed	uprostřed	k7c2	uprostřed
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
zhruba	zhruba	k6eAd1	zhruba
kryje	krýt	k5eAaImIp3nS	krýt
s	s	k7c7	s
počátkem	počátek	k1gInSc7	počátek
řecké	řecký	k2eAgFnSc2d1	řecká
kolonizace	kolonizace	k1gFnSc2	kolonizace
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
legend	legenda	k1gFnPc2	legenda
souvisí	souviset	k5eAaImIp3nS	souviset
vznik	vznik	k1gInSc1	vznik
Říma	Řím	k1gInSc2	Řím
s	s	k7c7	s
dějinami	dějiny	k1gFnPc7	dějiny
Tróje	Trója	k1gFnSc2	Trója
<g/>
.	.	kIx.	.
</s>
<s>
Přeživší	přeživší	k2eAgMnPc1d1	přeživší
Trójané	Trójan	k1gMnPc1	Trójan
měli	mít	k5eAaImAgMnP	mít
být	být	k5eAaImF	být
prý	prý	k9	prý
přivedeni	přiveden	k2eAgMnPc1d1	přiveden
na	na	k7c6	na
území	území	k1gNnSc6	území
Latia	Latium	k1gNnSc2	Latium
hrdinou	hrdina	k1gMnSc7	hrdina
Aeneem	Aeneas	k1gMnSc7	Aeneas
<g/>
,	,	kIx,	,
synem	syn	k1gMnSc7	syn
Anchísa	Anchís	k1gMnSc2	Anchís
a	a	k8xC	a
bohyně	bohyně	k1gFnSc2	bohyně
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
Potomky	potomek	k1gMnPc4	potomek
Aenea	Aeneas	k1gMnSc2	Aeneas
byli	být	k5eAaImAgMnP	být
bratři	bratr	k1gMnPc1	bratr
–	–	k?	–
dvojčata	dvojče	k1gNnPc1	dvojče
–	–	k?	–
Romulus	Romulus	k1gMnSc1	Romulus
a	a	k8xC	a
Remus	Remus	k1gMnSc1	Remus
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
krále	král	k1gMnSc2	král
města	město	k1gNnSc2	město
Alba	album	k1gNnSc2	album
Longa	Long	k1gMnSc2	Long
brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
vhozeni	vhodit	k5eAaPmNgMnP	vhodit
do	do	k7c2	do
Tibery	Tibera	k1gFnSc2	Tibera
<g/>
.	.	kIx.	.
</s>
<s>
Dvojčata	dvojče	k1gNnPc1	dvojče
se	se	k3xPyFc4	se
však	však	k9	však
neutopila	utopit	k5eNaPmAgFnS	utopit
a	a	k8xC	a
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
<g/>
,	,	kIx,	,
uslyšela	uslyšet	k5eAaPmAgFnS	uslyšet
jejich	jejich	k3xOp3gInSc4	jejich
pláč	pláč	k1gInSc4	pláč
vlčice	vlčice	k1gFnSc2	vlčice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
oba	dva	k4xCgMnPc4	dva
chlapce	chlapec	k1gMnPc4	chlapec
odkojila	odkojit	k5eAaPmAgFnS	odkojit
<g/>
.	.	kIx.	.
</s>
<s>
Bratři	bratr	k1gMnPc1	bratr
později	pozdě	k6eAd2	pozdě
krále	král	k1gMnSc4	král
Amulia	Amulium	k1gNnSc2	Amulium
porazili	porazit	k5eAaPmAgMnP	porazit
a	a	k8xC	a
založili	založit	k5eAaPmAgMnP	založit
si	se	k3xPyFc3	se
vlastní	vlastní	k2eAgNnSc4d1	vlastní
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
vládcem	vládce	k1gMnSc7	vládce
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
stal	stát	k5eAaPmAgMnS	stát
Romulus	Romulus	k1gMnSc1	Romulus
<g/>
.	.	kIx.	.
</s>
<s>
Remus	Remus	k1gMnSc1	Remus
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
Romulovi	Romulus	k1gMnSc3	Romulus
posmívat	posmívat	k5eAaImF	posmívat
a	a	k8xC	a
přeskočil	přeskočit	k5eAaPmAgMnS	přeskočit
brázdu	brázda	k1gFnSc4	brázda
označující	označující	k2eAgFnSc2d1	označující
budoucí	budoucí	k2eAgFnSc2d1	budoucí
hradby	hradba	k1gFnSc2	hradba
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
nato	nato	k6eAd1	nato
Romulem	Romulus	k1gMnSc7	Romulus
zabit	zabít	k5eAaPmNgMnS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Romulovi	Romulus	k1gMnSc6	Romulus
vládli	vládnout	k5eAaImAgMnP	vládnout
městu	město	k1gNnSc3	město
další	další	k2eAgMnPc1d1	další
tři	tři	k4xCgMnPc1	tři
mýtičtí	mýtický	k2eAgMnPc1d1	mýtický
králové	král	k1gMnPc1	král
<g/>
.	.	kIx.	.
</s>
<s>
Numa	Numa	k1gMnSc1	Numa
Pompilius	Pompilius	k1gMnSc1	Pompilius
zavedl	zavést	k5eAaPmAgMnS	zavést
římské	římský	k2eAgInPc4d1	římský
náboženské	náboženský	k2eAgInPc4d1	náboženský
obřady	obřad	k1gInPc4	obřad
a	a	k8xC	a
zvyky	zvyk	k1gInPc4	zvyk
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
Tulla	Tull	k1gMnSc4	Tull
Hostilia	Hostilius	k1gMnSc4	Hostilius
si	se	k3xPyFc3	se
Řím	Řím	k1gInSc4	Řím
v	v	k7c6	v
roce	rok	k1gInSc6	rok
659	[number]	k4	659
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
upevnil	upevnit	k5eAaPmAgInS	upevnit
své	své	k1gNnSc4	své
postavení	postavení	k1gNnSc2	postavení
zničením	zničení	k1gNnSc7	zničení
Alby	alba	k1gFnSc2	alba
<g/>
.	.	kIx.	.
</s>
<s>
Ancus	Ancus	k1gMnSc1	Ancus
Marcius	Marcius	k1gMnSc1	Marcius
založil	založit	k5eAaPmAgMnS	založit
přístav	přístav	k1gInSc4	přístav
v	v	k7c6	v
Ostii	Ostie	k1gFnSc6	Ostie
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
(	(	kIx(	(
<g/>
rex	rex	k?	rex
<g/>
)	)	kIx)	)
měl	mít	k5eAaImAgInS	mít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
moc	moc	k1gFnSc4	moc
správní	správní	k2eAgFnSc4d1	správní
<g/>
,	,	kIx,	,
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
i	i	k8xC	i
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
stál	stát	k5eAaImAgInS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
rodových	rodový	k2eAgMnPc2d1	rodový
vůdců	vůdce	k1gMnPc2	vůdce
–	–	k?	–
senátorů	senátor	k1gMnPc2	senátor
(	(	kIx(	(
<g/>
stařešinů	stařešina	k1gMnPc2	stařešina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vykonávali	vykonávat	k5eAaImAgMnP	vykonávat
poradní	poradní	k2eAgFnSc4d1	poradní
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
bylo	být	k5eAaImAgNnS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
tři	tři	k4xCgNnPc4	tři
tribue	tribue	k1gNnPc4	tribue
(	(	kIx(	(
<g/>
kmeny	kmen	k1gInPc1	kmen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
dělily	dělit	k5eAaImAgFnP	dělit
na	na	k7c4	na
kurie	kurie	k1gFnPc4	kurie
a	a	k8xC	a
rody	rod	k1gInPc4	rod
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
scházelo	scházet	k5eAaImAgNnS	scházet
lidové	lidový	k2eAgNnSc1d1	lidové
shromáždění	shromáždění	k1gNnSc1	shromáždění
(	(	kIx(	(
<g/>
comitia	comitia	k1gFnSc1	comitia
curiata	curiata	k1gFnSc1	curiata
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
napadli	napadnout	k5eAaPmAgMnP	napadnout
Řím	Řím	k1gInSc4	Řím
Etruskové	Etrusk	k1gMnPc1	Etrusk
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
zdálo	zdát	k5eAaImAgNnS	zdát
příliš	příliš	k6eAd1	příliš
mocné	mocný	k2eAgNnSc1d1	mocné
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
etruských	etruský	k2eAgMnPc2d1	etruský
králů	král	k1gMnPc2	král
Řím	Řím	k1gInSc1	Řím
přesto	přesto	k8xC	přesto
rychle	rychle	k6eAd1	rychle
vyrostl	vyrůst	k5eAaPmAgInS	vyrůst
v	v	k7c4	v
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
Apeninského	apeninský	k2eAgInSc2d1	apeninský
poloostrova	poloostrov	k1gInSc2	poloostrov
a	a	k8xC	a
domohl	domoct	k5eAaPmAgMnS	domoct
se	se	k3xPyFc4	se
velkého	velký	k2eAgInSc2d1	velký
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
významu	význam	k1gInSc2	význam
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
kontroloval	kontrolovat	k5eAaImAgMnS	kontrolovat
důležité	důležitý	k2eAgFnPc4d1	důležitá
obchodní	obchodní	k2eAgFnPc4d1	obchodní
trasy	trasa	k1gFnPc4	trasa
<g/>
:	:	kIx,	:
Via	via	k7c4	via
Latina	Latin	k1gMnSc4	Latin
a	a	k8xC	a
Via	via	k7c4	via
Salaria	Salarium	k1gNnPc4	Salarium
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
etruským	etruský	k2eAgMnSc7d1	etruský
králem	král	k1gMnSc7	král
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
616	[number]	k4	616
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
stal	stát	k5eAaPmAgMnS	stát
Tarquinius	Tarquinius	k1gMnSc1	Tarquinius
Priscus	Priscus	k1gMnSc1	Priscus
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Servius	Servius	k1gMnSc1	Servius
Tullius	Tullius	k1gMnSc1	Tullius
prý	prý	k9	prý
nechal	nechat	k5eAaPmAgMnS	nechat
postavit	postavit	k5eAaPmF	postavit
městské	městský	k2eAgFnSc2d1	městská
hradby	hradba	k1gFnSc2	hradba
a	a	k8xC	a
provedl	provést	k5eAaPmAgInS	provést
také	také	k9	také
setninovou	setninový	k2eAgFnSc4d1	setninový
reformu	reforma	k1gFnSc4	reforma
vojska	vojsko	k1gNnSc2	vojsko
a	a	k8xC	a
lidového	lidový	k2eAgNnSc2d1	lidové
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
římský	římský	k2eAgInSc1d1	římský
resp.	resp.	kA	resp.
etruský	etruský	k2eAgMnSc1d1	etruský
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
Tarquinius	Tarquinius	k1gMnSc1	Tarquinius
Superbus	Superbus	k1gMnSc1	Superbus
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Zpupný	zpupný	k2eAgMnSc1d1	zpupný
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
města	město	k1gNnSc2	město
vyhnán	vyhnán	k2eAgInSc1d1	vyhnán
aristokraty	aristokrat	k1gMnPc7	aristokrat
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Lucia	Lucius	k1gMnSc2	Lucius
Junia	Junius	k1gMnSc2	Junius
Bruta	Brut	k1gMnSc2	Brut
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
prameny	pramen	k1gInPc4	pramen
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc4	jeho
syn	syn	k1gMnSc1	syn
měl	mít	k5eAaImAgMnS	mít
znásilnit	znásilnit	k5eAaPmF	znásilnit
a	a	k8xC	a
donutit	donutit	k5eAaPmF	donutit
k	k	k7c3	k
sebevraždě	sebevražda	k1gFnSc3	sebevražda
ctnostnou	ctnostný	k2eAgFnSc4d1	ctnostná
římskou	římský	k2eAgFnSc4d1	římská
matrónu	matróna	k1gFnSc4	matróna
Lukrécii	Lukrécie	k1gFnSc4	Lukrécie
<g/>
.	.	kIx.	.
</s>
<s>
Tarquinius	Tarquinius	k1gMnSc1	Tarquinius
se	se	k3xPyFc4	se
nevzdal	vzdát	k5eNaPmAgMnS	vzdát
a	a	k8xC	a
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
jiného	jiný	k2eAgMnSc4d1	jiný
etruského	etruský	k2eAgMnSc4d1	etruský
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
Porsennu	Porsenna	k1gMnSc4	Porsenna
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
názoru	názor	k1gInSc2	názor
některých	některý	k3yIgMnPc2	některý
historiků	historik	k1gMnPc2	historik
zmocnil	zmocnit	k5eAaPmAgInS	zmocnit
Říma	Řím	k1gInSc2	Řím
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
507	[number]	k4	507
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
historici	historik	k1gMnPc1	historik
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Porsenna	Porsenn	k1gMnSc4	Porsenn
Řím	Řím	k1gInSc1	Řím
nedobyl	dobýt	k5eNaPmAgInS	dobýt
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
zachránil	zachránit	k5eAaPmAgInS	zachránit
Řím	Řím	k1gInSc1	Řím
před	před	k7c7	před
jeho	jeho	k3xOp3gInSc7	jeho
útokem	útok	k1gInSc7	útok
jediný	jediný	k2eAgMnSc1d1	jediný
muž	muž	k1gMnSc1	muž
–	–	k?	–
Horatius	Horatius	k1gMnSc1	Horatius
Cocles	Cocles	k1gMnSc1	Cocles
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobněji	pravděpodobně	k6eAd2	pravděpodobně
si	se	k3xPyFc3	se
však	však	k9	však
Římané	Říman	k1gMnPc1	Říman
zajistili	zajistit	k5eAaPmAgMnP	zajistit
svobodu	svoboda	k1gFnSc4	svoboda
vysokým	vysoký	k2eAgNnSc7d1	vysoké
výkupným	výkupné	k1gNnSc7	výkupné
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Řím	Řím	k1gInSc1	Řím
někdy	někdy	k6eAd1	někdy
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
500	[number]	k4	500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zbavil	zbavit	k5eAaPmAgInS	zbavit
etruské	etruský	k2eAgFnPc4d1	etruská
nadvlády	nadvláda	k1gFnPc4	nadvláda
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc7	jeho
vliv	vliv	k1gInSc1	vliv
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zatím	zatím	k6eAd1	zatím
spíše	spíše	k9	spíše
jenom	jenom	k9	jenom
v	v	k7c6	v
Latiu	Latium	k1gNnSc6	Latium
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rok	rok	k1gInSc1	rok
509	[number]	k4	509
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
udává	udávat	k5eAaImIp3nS	udávat
za	za	k7c4	za
datum	datum	k1gNnSc4	datum
této	tento	k3xDgFnSc2	tento
události	událost	k1gFnSc2	událost
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
až	až	k6eAd1	až
mnohem	mnohem	k6eAd1	mnohem
později	pozdě	k6eAd2	pozdě
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
pokládán	pokládat	k5eAaImNgInS	pokládat
za	za	k7c4	za
příliš	příliš	k6eAd1	příliš
jistý	jistý	k2eAgInSc4d1	jistý
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
byl	být	k5eAaImAgInS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
vyhnání	vyhnání	k1gNnSc2	vyhnání
Peisistratovců	Peisistratovec	k1gMnPc2	Peisistratovec
z	z	k7c2	z
Athén	Athéna	k1gFnPc2	Athéna
v	v	k7c6	v
roce	rok	k1gInSc6	rok
510	[number]	k4	510
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Pravděpodobnější	pravděpodobný	k2eAgFnSc7d2	pravděpodobnější
dobou	doba	k1gFnSc7	doba
přeměny	přeměna	k1gFnSc2	přeměna
království	království	k1gNnSc2	království
v	v	k7c4	v
římskou	římský	k2eAgFnSc4d1	římská
republiku	republika	k1gFnSc4	republika
–	–	k?	–
res	res	k?	res
publica	publicum	k1gNnSc2	publicum
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
věc	věc	k1gFnSc1	věc
veřejná	veřejný	k2eAgFnSc1d1	veřejná
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
–	–	k?	–
byl	být	k5eAaImAgMnS	být
patrně	patrně	k6eAd1	patrně
počátek	počátek	k1gInSc4	počátek
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
již	již	k9	již
moc	moc	k1gFnSc1	moc
Etrusků	Etrusk	k1gMnPc2	Etrusk
výrazně	výrazně	k6eAd1	výrazně
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
<g/>
.	.	kIx.	.
</s>
<s>
Kulturně	kulturně	k6eAd1	kulturně
byli	být	k5eAaImAgMnP	být
Římané	Říman	k1gMnPc1	Říman
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
počátcích	počátek	k1gInPc6	počátek
vystaveni	vystavit	k5eAaPmNgMnP	vystavit
silnému	silný	k2eAgInSc3d1	silný
vlivu	vliv	k1gInSc3	vliv
Etrusků	Etrusk	k1gMnPc2	Etrusk
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc7	jejich
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
rovněž	rovněž	k9	rovněž
do	do	k7c2	do
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
řeckou	řecký	k2eAgFnSc7d1	řecká
kulturou	kultura	k1gFnSc7	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
etruská	etruský	k2eAgNnPc1d1	etruské
čísla	číslo	k1gNnPc1	číslo
<g/>
,	,	kIx,	,
od	od	k7c2	od
Etrusků	Etrusk	k1gMnPc2	Etrusk
bylo	být	k5eAaImAgNnS	být
převzato	převzít	k5eAaPmNgNnS	převzít
rovněž	rovněž	k9	rovněž
písmo	písmo	k1gNnSc1	písmo
(	(	kIx(	(
<g/>
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
latinská	latinský	k2eAgFnSc1d1	Latinská
abeceda	abeceda	k1gFnSc1	abeceda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ptákopravectví	ptákopravectví	k1gNnSc1	ptákopravectví
(	(	kIx(	(
<g/>
auspicia	auspicia	k1gFnSc1	auspicia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pohřební	pohřební	k2eAgInPc4d1	pohřební
rituály	rituál	k1gInPc4	rituál
a	a	k8xC	a
mnohé	mnohý	k2eAgFnPc4d1	mnohá
další	další	k2eAgFnPc4d1	další
kulturní	kulturní	k2eAgFnPc4d1	kulturní
vymoženosti	vymoženost	k1gFnPc4	vymoženost
a	a	k8xC	a
náboženské	náboženský	k2eAgFnPc4d1	náboženská
zvyklosti	zvyklost	k1gFnPc4	zvyklost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jádro	jádro	k1gNnSc1	jádro
Říma	Řím	k1gInSc2	Řím
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Republika	republika	k1gFnSc1	republika
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vznik	vznik	k1gInSc1	vznik
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
dobytí	dobytí	k1gNnSc4	dobytí
Apeninského	apeninský	k2eAgInSc2d1	apeninský
poloostrova	poloostrov	k1gInSc2	poloostrov
===	===	k?	===
</s>
</p>
<p>
<s>
Určit	určit	k5eAaPmF	určit
charakter	charakter	k1gInSc4	charakter
římského	římský	k2eAgInSc2d1	římský
státu	stát	k1gInSc2	stát
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
uspořádání	uspořádání	k1gNnSc4	uspořádání
je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
,	,	kIx,	,
poněvadž	poněvadž	k8xS	poněvadž
jeho	jeho	k3xOp3gInSc1	jeho
vývoj	vývoj	k1gInSc1	vývoj
trval	trvat	k5eAaImAgInS	trvat
staletí	staletí	k1gNnPc4	staletí
<g/>
,	,	kIx,	,
během	během	k7c2	během
nichž	jenž	k3xRgMnPc2	jenž
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
mnohým	mnohý	k2eAgFnPc3d1	mnohá
změnám	změna	k1gFnPc3	změna
<g/>
.	.	kIx.	.
</s>
<s>
Polybios	Polybios	k1gMnSc1	Polybios
<g/>
,	,	kIx,	,
řecký	řecký	k2eAgMnSc1d1	řecký
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
učenec	učenec	k1gMnSc1	učenec
<g/>
,	,	kIx,	,
popsal	popsat	k5eAaPmAgMnS	popsat
římskou	římský	k2eAgFnSc4d1	římská
republiku	republika	k1gFnSc4	republika
jako	jako	k8xS	jako
kombinaci	kombinace	k1gFnSc4	kombinace
monarchie	monarchie	k1gFnSc2	monarchie
(	(	kIx(	(
<g/>
magistrátské	magistrátský	k2eAgInPc1d1	magistrátský
úřady	úřad	k1gInPc1	úřad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aristokracie	aristokracie	k1gFnSc1	aristokracie
(	(	kIx(	(
<g/>
římský	římský	k2eAgInSc1d1	římský
senát	senát	k1gInSc1	senát
<g/>
)	)	kIx)	)
a	a	k8xC	a
demokracie	demokracie	k1gFnPc1	demokracie
(	(	kIx(	(
<g/>
lidová	lidový	k2eAgNnPc1d1	lidové
shromáždění	shromáždění	k1gNnPc1	shromáždění
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vládu	vláda	k1gFnSc4	vláda
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
vykonávali	vykonávat	k5eAaImAgMnP	vykonávat
dva	dva	k4xCgMnPc1	dva
každoročně	každoročně	k6eAd1	každoročně
volení	volený	k2eAgMnPc1d1	volený
konzulové	konzul	k1gMnPc1	konzul
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
disponovali	disponovat	k5eAaBmAgMnP	disponovat
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
výkonnou	výkonný	k2eAgFnSc7d1	výkonná
mocí	moc	k1gFnSc7	moc
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
veliteli	velitel	k1gMnPc7	velitel
vojska	vojsko	k1gNnSc2	vojsko
a	a	k8xC	a
stáli	stát	k5eAaImAgMnP	stát
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
soustavy	soustava	k1gFnSc2	soustava
magistrátských	magistrátský	k2eAgInPc2d1	magistrátský
úřadů	úřad	k1gInPc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
volba	volba	k1gFnSc1	volba
náležela	náležet	k5eAaImAgFnS	náležet
lidovému	lidový	k2eAgNnSc3d1	lidové
shromáždění	shromáždění	k1gNnSc3	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Římský	římský	k2eAgInSc1d1	římský
senát	senát	k1gInSc1	senát
<g/>
,	,	kIx,	,
shromáždění	shromáždění	k1gNnSc1	shromáždění
urozených	urozený	k2eAgNnPc2d1	urozené
<g/>
,	,	kIx,	,
hrál	hrát	k5eAaImAgInS	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
principátu	principát	k1gInSc2	principát
nedisponoval	disponovat	k5eNaBmAgMnS	disponovat
zákonodárnou	zákonodárný	k2eAgFnSc7d1	zákonodárná
pravomocí	pravomoc	k1gFnSc7	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
Moc	moc	k1gFnSc1	moc
senátu	senát	k1gInSc2	senát
vycházela	vycházet	k5eAaImAgFnS	vycházet
především	především	k9	především
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
autority	autorita	k1gFnSc2	autorita
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
něho	on	k3xPp3gMnSc2	on
existovalo	existovat	k5eAaImAgNnS	existovat
lidové	lidový	k2eAgNnSc1d1	lidové
shromáždění	shromáždění	k1gNnSc1	shromáždění
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
rovněž	rovněž	k9	rovněž
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
rozhodovalo	rozhodovat	k5eAaImAgNnS	rozhodovat
o	o	k7c6	o
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
míru	mír	k1gInSc6	mír
<g/>
,	,	kIx,	,
schvalovalo	schvalovat	k5eAaImAgNnS	schvalovat
zákony	zákon	k1gInPc4	zákon
a	a	k8xC	a
vykonávalo	vykonávat	k5eAaImAgNnS	vykonávat
soudní	soudní	k2eAgFnSc4d1	soudní
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Ústřední	ústřední	k2eAgNnSc1d1	ústřední
místo	místo	k1gNnSc1	místo
římské	římský	k2eAgFnSc2d1	římská
republiky	republika	k1gFnSc2	republika
představovalo	představovat	k5eAaImAgNnS	představovat
Forum	forum	k1gNnSc1	forum
Romanum	Romanum	k?	Romanum
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
sloužilo	sloužit	k5eAaImAgNnS	sloužit
k	k	k7c3	k
politickým	politický	k2eAgFnPc3d1	politická
<g/>
,	,	kIx,	,
náboženským	náboženský	k2eAgFnPc3d1	náboženská
i	i	k8xC	i
společenským	společenský	k2eAgFnPc3d1	společenská
schůzím	schůze	k1gFnPc3	schůze
a	a	k8xC	a
setkáním	setkání	k1gNnSc7	setkání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
na	na	k7c6	na
konci	konec	k1gInSc6	konec
království	království	k1gNnSc2	království
a	a	k8xC	a
počátku	počátek	k1gInSc2	počátek
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
utvořilo	utvořit	k5eAaPmAgNnS	utvořit
uspořádání	uspořádání	k1gNnSc1	uspořádání
římské	římský	k2eAgFnSc2d1	římská
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
pak	pak	k6eAd1	pak
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
obměněné	obměněný	k2eAgFnSc6d1	obměněná
podobě	podoba	k1gFnSc6	podoba
přetrvalo	přetrvat	k5eAaPmAgNnS	přetrvat
po	po	k7c4	po
staletí	staletí	k1gNnPc4	staletí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
vrcholu	vrchol	k1gInSc6	vrchol
stály	stát	k5eAaImAgInP	stát
staré	starý	k2eAgInPc1d1	starý
římské	římský	k2eAgInPc1d1	římský
rody	rod	k1gInPc1	rod
a	a	k8xC	a
majitelé	majitel	k1gMnPc1	majitel
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
patricijové	patricij	k1gMnPc1	patricij
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
politicky	politicky	k6eAd1	politicky
nejvlivnější	vlivný	k2eAgMnPc1d3	nejvlivnější
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
část	část	k1gFnSc1	část
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
se	se	k3xPyFc4	se
ale	ale	k9	ale
skládala	skládat	k5eAaImAgFnS	skládat
z	z	k7c2	z
plebejů	plebej	k1gMnPc2	plebej
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
podrobené	podrobený	k2eAgNnSc4d1	podrobené
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
disponovali	disponovat	k5eAaBmAgMnP	disponovat
jen	jen	k9	jen
omezenými	omezený	k2eAgFnPc7d1	omezená
politickými	politický	k2eAgNnPc7d1	politické
právy	právo	k1gNnPc7	právo
a	a	k8xC	a
vůči	vůči	k7c3	vůči
patricijům	patricij	k1gMnPc3	patricij
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
v	v	k7c4	v
postavení	postavení	k1gNnSc4	postavení
klientů	klient	k1gMnPc2	klient
<g/>
.	.	kIx.	.
</s>
<s>
Klientelismus	klientelismus	k1gInSc1	klientelismus
zavazoval	zavazovat	k5eAaImAgMnS	zavazovat
patrona	patron	k1gMnSc4	patron
k	k	k7c3	k
zajištění	zajištění	k1gNnSc3	zajištění
ochrany	ochrana	k1gFnSc2	ochrana
klientovi	klient	k1gMnSc3	klient
a	a	k8xC	a
klienta	klient	k1gMnSc2	klient
ke	k	k7c3	k
službě	služba	k1gFnSc3	služba
a	a	k8xC	a
věrnosti	věrnost	k1gFnSc3	věrnost
patronovi	patronův	k2eAgMnPc1d1	patronův
<g/>
.	.	kIx.	.
</s>
<s>
Nejnižší	nízký	k2eAgFnSc7d3	nejnižší
vrstvou	vrstva	k1gFnSc7	vrstva
na	na	k7c6	na
společenském	společenský	k2eAgInSc6d1	společenský
žebříčku	žebříček	k1gInSc6	žebříček
byli	být	k5eAaImAgMnP	být
otroci	otrok	k1gMnPc1	otrok
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
pouhé	pouhý	k2eAgFnPc4d1	pouhá
věci	věc	k1gFnPc4	věc
a	a	k8xC	a
neměli	mít	k5eNaImAgMnP	mít
žádná	žádný	k3yNgNnPc4	žádný
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
mohli	moct	k5eAaImAgMnP	moct
získat	získat	k5eAaPmF	získat
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
první	první	k4xOgInSc4	první
pevný	pevný	k2eAgInSc4d1	pevný
bod	bod	k1gInSc4	bod
římských	římský	k2eAgFnPc2d1	římská
dějin	dějiny	k1gFnPc2	dějiny
je	být	k5eAaImIp3nS	být
pokládán	pokládat	k5eAaImNgInS	pokládat
okamžik	okamžik	k1gInSc1	okamžik
sepsání	sepsání	k1gNnSc2	sepsání
zákona	zákon	k1gInSc2	zákon
dvanácti	dvanáct	k4xCc2	dvanáct
desek	deska	k1gFnPc2	deska
v	v	k7c6	v
roce	rok	k1gInSc6	rok
450	[number]	k4	450
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Tímto	tento	k3xDgInSc7	tento
zákonem	zákon	k1gInSc7	zákon
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
písemně	písemně	k6eAd1	písemně
zachyceno	zachycen	k2eAgNnSc4d1	zachyceno
zvykové	zvykový	k2eAgNnSc4d1	zvykové
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
týkající	týkající	k2eAgFnSc4d1	týkající
se	se	k3xPyFc4	se
především	především	k9	především
oblasti	oblast	k1gFnPc1	oblast
občanského	občanský	k2eAgNnSc2d1	občanské
a	a	k8xC	a
trestního	trestní	k2eAgNnSc2d1	trestní
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
procesního	procesní	k2eAgInSc2d1	procesní
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Kodifikace	kodifikace	k1gFnSc1	kodifikace
měla	mít	k5eAaImAgFnS	mít
zamezit	zamezit	k5eAaPmF	zamezit
svévoli	svévole	k1gFnSc3	svévole
patricijů	patricij	k1gMnPc2	patricij
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
tendenci	tendence	k1gFnSc4	tendence
interpretovat	interpretovat	k5eAaBmF	interpretovat
práva	právo	k1gNnPc4	právo
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
prospěchu	prospěch	k1gInSc3	prospěch
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvyšším	vysoký	k2eAgInPc3d3	Nejvyšší
úřadům	úřad	k1gInPc3	úřad
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
svým	svůj	k3xOyFgMnPc3	svůj
nositelům	nositel	k1gMnPc3	nositel
zajišťovaly	zajišťovat	k5eAaImAgInP	zajišťovat
vážnost	vážnost	k1gFnSc4	vážnost
a	a	k8xC	a
autoritu	autorita	k1gFnSc4	autorita
(	(	kIx(	(
<g/>
dignitas	dignitas	k1gInSc1	dignitas
et	et	k?	et
auctoritas	auctoritas	k1gInSc1	auctoritas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
měli	mít	k5eAaImAgMnP	mít
zpočátku	zpočátku	k6eAd1	zpočátku
přístup	přístup	k1gInSc4	přístup
pouze	pouze	k6eAd1	pouze
patricijové	patricij	k1gMnPc1	patricij
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
nepřetržitý	přetržitý	k2eNgInSc4d1	nepřetržitý
válečný	válečný	k2eAgInSc4d1	válečný
stav	stav	k1gInSc4	stav
výrazně	výrazně	k6eAd1	výrazně
zatěžoval	zatěžovat	k5eAaImAgMnS	zatěžovat
plebeje	plebej	k1gMnPc4	plebej
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
tvořili	tvořit	k5eAaImAgMnP	tvořit
páteř	páteř	k1gFnSc4	páteř
vojska	vojsko	k1gNnSc2	vojsko
organizovaného	organizovaný	k2eAgNnSc2d1	organizované
na	na	k7c6	na
miličním	miliční	k2eAgInSc6d1	miliční
principu	princip	k1gInSc6	princip
<g/>
.	.	kIx.	.
</s>
<s>
Nepoměr	nepoměr	k1gInSc1	nepoměr
mezi	mezi	k7c7	mezi
rozsahem	rozsah	k1gInSc7	rozsah
jejich	jejich	k3xOp3gFnPc2	jejich
povinností	povinnost	k1gFnPc2	povinnost
a	a	k8xC	a
sociálním	sociální	k2eAgInSc7d1	sociální
statusem	status	k1gInSc7	status
resp.	resp.	kA	resp.
reálným	reálný	k2eAgInSc7d1	reálný
politickým	politický	k2eAgInSc7d1	politický
vlivem	vliv	k1gInSc7	vliv
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
třídnímu	třídní	k2eAgInSc3d1	třídní
boji	boj	k1gInSc3	boj
mezi	mezi	k7c7	mezi
plebeji	plebej	k1gMnPc7	plebej
a	a	k8xC	a
patriciji	patricij	k1gMnPc7	patricij
<g/>
.	.	kIx.	.
</s>
<s>
Plebejové	plebej	k1gMnPc1	plebej
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
zaměřili	zaměřit	k5eAaPmAgMnP	zaměřit
na	na	k7c4	na
požadavek	požadavek	k1gInSc4	požadavek
zrovnoprávnění	zrovnoprávnění	k1gNnSc2	zrovnoprávnění
s	s	k7c7	s
patriciji	patricij	k1gMnPc7	patricij
a	a	k8xC	a
na	na	k7c4	na
právo	právo	k1gNnSc4	právo
spolupodílet	spolupodílet	k5eAaImF	spolupodílet
se	se	k3xPyFc4	se
na	na	k7c6	na
závažných	závažný	k2eAgNnPc6d1	závažné
politických	politický	k2eAgNnPc6d1	politické
rozhodnutích	rozhodnutí	k1gNnPc6	rozhodnutí
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
prosazení	prosazení	k1gNnSc3	prosazení
svých	svůj	k3xOyFgInPc2	svůj
cílů	cíl	k1gInPc2	cíl
odešli	odejít	k5eAaPmAgMnP	odejít
z	z	k7c2	z
města	město	k1gNnSc2	město
a	a	k8xC	a
usadili	usadit	k5eAaPmAgMnP	usadit
se	se	k3xPyFc4	se
na	na	k7c6	na
svaté	svatý	k2eAgFnSc6d1	svatá
hoře	hora	k1gFnSc6	hora
(	(	kIx(	(
<g/>
Mons	Mons	k1gInSc1	Mons
Sacer	Sacer	k1gInSc1	Sacer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
znemožnili	znemožnit	k5eAaPmAgMnP	znemožnit
obranu	obrana	k1gFnSc4	obrana
Říma	Řím	k1gInSc2	Řím
a	a	k8xC	a
přinutili	přinutit	k5eAaPmAgMnP	přinutit
patricije	patricij	k1gMnSc4	patricij
k	k	k7c3	k
ústupkům	ústupek	k1gInPc3	ústupek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgFnPc1	tento
secese	secese	k1gFnPc1	secese
(	(	kIx(	(
<g/>
secessio	secessio	k1gNnSc1	secessio
plebis	plebis	k1gFnSc2	plebis
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
staletích	staletí	k1gNnPc6	staletí
ještě	ještě	k6eAd1	ještě
několikrát	několikrát	k6eAd1	několikrát
opakovaly	opakovat	k5eAaImAgFnP	opakovat
<g/>
.	.	kIx.	.
</s>
<s>
Plebejové	plebej	k1gMnPc1	plebej
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
domohli	domoct	k5eAaPmAgMnP	domoct
uznání	uznání	k1gNnSc3	uznání
úřadů	úřad	k1gInPc2	úřad
tribunů	tribun	k1gMnPc2	tribun
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
legalizace	legalizace	k1gFnSc1	legalizace
manželství	manželství	k1gNnSc2	manželství
s	s	k7c7	s
patriciji	patricij	k1gMnPc7	patricij
a	a	k8xC	a
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
výkonu	výkon	k1gInSc3	výkon
magistratur	magistratura	k1gFnPc2	magistratura
(	(	kIx(	(
<g/>
první	první	k4xOgMnSc1	první
plebej	plebej	k1gMnSc1	plebej
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
konzulem	konzul	k1gMnSc7	konzul
v	v	k7c6	v
roce	rok	k1gInSc6	rok
366	[number]	k4	366
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tribuni	tribun	k1gMnPc1	tribun
lidu	lid	k1gInSc2	lid
využívali	využívat	k5eAaImAgMnP	využívat
ochrany	ochrana	k1gFnPc4	ochrana
nedotknutelnosti	nedotknutelnost	k1gFnSc2	nedotknutelnost
své	svůj	k3xOyFgFnPc4	svůj
osoby	osoba	k1gFnPc4	osoba
a	a	k8xC	a
stavěli	stavět	k5eAaImAgMnP	stavět
se	se	k3xPyFc4	se
v	v	k7c6	v
případných	případný	k2eAgInPc6d1	případný
konfliktech	konflikt	k1gInPc6	konflikt
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
plebejů	plebej	k1gMnPc2	plebej
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
287	[number]	k4	287
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
(	(	kIx(	(
<g/>
po	po	k7c6	po
poslední	poslední	k2eAgFnSc6d1	poslední
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
jediné	jediný	k2eAgFnSc6d1	jediná
prokázané	prokázaný	k2eAgFnSc6d1	prokázaná
secesi	secese	k1gFnSc6	secese
<g/>
)	)	kIx)	)
skončil	skončit	k5eAaPmAgInS	skončit
boj	boj	k1gInSc1	boj
mezi	mezi	k7c7	mezi
plebeji	plebej	k1gMnPc7	plebej
a	a	k8xC	a
patriciji	patricij	k1gMnPc7	patricij
vydáním	vydání	k1gNnSc7	vydání
zákona	zákon	k1gInSc2	zákon
lex	lex	k?	lex
Hortensia	Hortensius	k1gMnSc2	Hortensius
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
propůjčoval	propůjčovat	k5eAaImAgMnS	propůjčovat
usnesením	usnesení	k1gNnSc7	usnesení
lidu	lid	k1gInSc2	lid
sílu	síla	k1gFnSc4	síla
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
jen	jen	k9	jen
relativně	relativně	k6eAd1	relativně
malému	malý	k2eAgInSc3d1	malý
počtu	počet	k1gInSc3	počet
plebejských	plebejský	k2eAgFnPc2d1	plebejská
rodin	rodina	k1gFnPc2	rodina
podařilo	podařit	k5eAaPmAgNnS	podařit
dostat	dostat	k5eAaPmF	dostat
mezi	mezi	k7c4	mezi
římskou	římský	k2eAgFnSc4d1	římská
vládnoucí	vládnoucí	k2eAgFnSc4d1	vládnoucí
vrstvu	vrstva	k1gFnSc4	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
boje	boj	k1gInSc2	boj
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgFnPc7	tento
společenskými	společenský	k2eAgFnPc7d1	společenská
třídami	třída	k1gFnPc7	třída
byl	být	k5eAaImAgMnS	být
tedy	tedy	k9	tedy
kompromis	kompromis	k1gInSc4	kompromis
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
integraci	integrace	k1gFnSc3	integrace
relativně	relativně	k6eAd1	relativně
malého	malý	k2eAgInSc2d1	malý
počtu	počet	k1gInSc2	počet
plebejských	plebejský	k2eAgFnPc2d1	plebejská
rodin	rodina	k1gFnPc2	rodina
do	do	k7c2	do
aristokratické	aristokratický	k2eAgFnSc2d1	aristokratická
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
nový	nový	k2eAgInSc1d1	nový
typ	typ	k1gInSc1	typ
aristokracie	aristokracie	k1gFnSc2	aristokracie
–	–	k?	–
nobilita	nobilita	k1gFnSc1	nobilita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
vedl	vést	k5eAaImAgInS	vést
Řím	Řím	k1gInSc1	Řím
četné	četný	k2eAgFnSc2d1	četná
války	válka	k1gFnSc2	válka
jak	jak	k8xC	jak
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
latinskými	latinský	k2eAgMnPc7d1	latinský
sousedy	soused	k1gMnPc7	soused
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
493	[number]	k4	493
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
uznali	uznat	k5eAaPmAgMnP	uznat
jeho	jeho	k3xOp3gNnSc4	jeho
výsadní	výsadní	k2eAgNnSc4d1	výsadní
postavení	postavení	k1gNnSc4	postavení
a	a	k8xC	a
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
městem	město	k1gNnSc7	město
na	na	k7c6	na
Tibeře	Tibera	k1gFnSc6	Tibera
hledali	hledat	k5eAaImAgMnP	hledat
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
vnějším	vnější	k2eAgNnSc7d1	vnější
ohrožením	ohrožení	k1gNnSc7	ohrožení
<g/>
,	,	kIx,	,
tak	tak	k9	tak
s	s	k7c7	s
horskými	horský	k2eAgInPc7d1	horský
italickými	italický	k2eAgInPc7d1	italický
kmeny	kmen	k1gInPc7	kmen
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byli	být	k5eAaImAgMnP	být
třeba	třeba	k6eAd1	třeba
Aequové	Aequové	k2eAgMnPc1d1	Aequové
či	či	k8xC	či
Volskové	Volskové	k2eAgMnPc1d1	Volskové
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
396	[number]	k4	396
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Římané	Říman	k1gMnPc1	Říman
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
diktátorem	diktátor	k1gMnSc7	diktátor
Marcem	Marce	k1gMnSc7	Marce
Furiem	Furius	k1gMnSc7	Furius
Camillem	Camill	k1gMnSc7	Camill
po	po	k7c6	po
téměř	téměř	k6eAd1	téměř
desetiletém	desetiletý	k2eAgInSc6d1	desetiletý
boji	boj	k1gInSc6	boj
zničili	zničit	k5eAaPmAgMnP	zničit
svého	svůj	k3xOyFgMnSc4	svůj
mocného	mocný	k2eAgMnSc4d1	mocný
soupeře	soupeř	k1gMnSc4	soupeř
–	–	k?	–
etruské	etruský	k2eAgNnSc1d1	etruské
město	město	k1gNnSc1	město
Veje	Vej	k1gFnSc2	Vej
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
prvním	první	k4xOgInSc7	první
krokem	krok	k1gInSc7	krok
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
římské	římský	k2eAgFnSc3d1	římská
hegemonii	hegemonie	k1gFnSc3	hegemonie
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
Allie	Allie	k1gFnSc2	Allie
byl	být	k5eAaImAgInS	být
přesto	přesto	k8xC	přesto
Řím	Řím	k1gInSc1	Řím
dobyt	dobyt	k2eAgMnSc1d1	dobyt
a	a	k8xC	a
vydrancován	vydrancován	k2eAgMnSc1d1	vydrancován
Galy	Gal	k1gMnPc7	Gal
vedenými	vedený	k2eAgFnPc7d1	vedená
náčelníkem	náčelník	k1gMnSc7	náčelník
Brennem	Brenn	k1gInSc7	Brenn
(	(	kIx(	(
<g/>
387	[number]	k4	387
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
Kapitol	Kapitol	k1gInSc1	Kapitol
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
opevněním	opevnění	k1gNnSc7	opevnění
dokázal	dokázat	k5eAaPmAgInS	dokázat
odolat	odolat	k5eAaPmF	odolat
jejich	jejich	k3xOp3gInSc3	jejich
útoku	útok	k1gInSc3	útok
<g/>
.	.	kIx.	.
</s>
<s>
Brennus	Brennus	k1gMnSc1	Brennus
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
Galové	Gal	k1gMnPc1	Gal
sice	sice	k8xC	sice
po	po	k7c6	po
zaplacení	zaplacení	k1gNnSc6	zaplacení
výkupného	výkupné	k1gNnSc2	výkupné
z	z	k7c2	z
města	město	k1gNnSc2	město
odtáhli	odtáhnout	k5eAaPmAgMnP	odtáhnout
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Vae	Vae	k1gFnSc1	Vae
Victis	Victis	k1gFnSc1	Victis
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
v	v	k7c6	v
opětovných	opětovný	k2eAgInPc6d1	opětovný
vpádech	vpád	k1gInPc6	vpád
do	do	k7c2	do
střední	střední	k2eAgFnSc2d1	střední
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
ohrožovali	ohrožovat	k5eAaImAgMnP	ohrožovat
tak	tak	k8xC	tak
Římany	Říman	k1gMnPc7	Říman
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
proto	proto	k6eAd1	proto
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
tzv.	tzv.	kA	tzv.
Serviovy	Serviův	k2eAgFnSc2d1	Serviův
hradby	hradba	k1gFnSc2	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Porážka	porážka	k1gFnSc1	porážka
Říma	Řím	k1gInSc2	Řím
oslabila	oslabit	k5eAaPmAgFnS	oslabit
jeho	jeho	k3xOp3gNnSc2	jeho
postavení	postavení	k1gNnSc2	postavení
mezi	mezi	k7c7	mezi
ostatními	ostatní	k2eAgInPc7d1	ostatní
italickými	italický	k2eAgInPc7d1	italický
kmeny	kmen	k1gInPc7	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Příští	příští	k2eAgNnSc1d1	příští
půlstoletí	půlstoletí	k1gNnSc1	půlstoletí
se	se	k3xPyFc4	se
tudíž	tudíž	k8xC	tudíž
vyznačovalo	vyznačovat	k5eAaImAgNnS	vyznačovat
neustálými	neustálý	k2eAgInPc7d1	neustálý
boji	boj	k1gInPc7	boj
s	s	k7c7	s
Galy	Gal	k1gMnPc7	Gal
a	a	k8xC	a
také	také	k6eAd1	také
spory	spor	k1gInPc1	spor
s	s	k7c7	s
latinskými	latinský	k2eAgMnPc7d1	latinský
spojenci	spojenec	k1gMnPc7	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
první	první	k4xOgFnSc1	první
samnitská	samnitský	k2eAgFnSc1d1	samnitská
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
343	[number]	k4	343
<g/>
–	–	k?	–
<g/>
341	[number]	k4	341
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
latinská	latinský	k2eAgFnSc1d1	Latinská
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
340	[number]	k4	340
<g/>
–	–	k?	–
<g/>
338	[number]	k4	338
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
skončení	skončení	k1gNnSc6	skončení
byl	být	k5eAaImAgInS	být
latinský	latinský	k2eAgInSc1d1	latinský
spolek	spolek	k1gInSc1	spolek
rozpuštěn	rozpuštěn	k2eAgInSc1d1	rozpuštěn
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
Latinů	Latin	k1gMnPc2	Latin
obdržela	obdržet	k5eAaPmAgFnS	obdržet
římské	římský	k2eAgNnSc4d1	římské
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
4	[number]	k4	4
<g/>
.	.	kIx.	.
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Římané	Říman	k1gMnPc1	Říman
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
dvou	dva	k4xCgFnPc6	dva
válkách	válka	k1gFnPc6	válka
definitivně	definitivně	k6eAd1	definitivně
srazili	srazit	k5eAaPmAgMnP	srazit
Samnity	Samnit	k1gInPc4	Samnit
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
si	se	k3xPyFc3	se
tyto	tento	k3xDgInPc4	tento
vnitrozemské	vnitrozemský	k2eAgInPc4d1	vnitrozemský
kmeny	kmen	k1gInPc4	kmen
Itálie	Itálie	k1gFnSc2	Itálie
definitivně	definitivně	k6eAd1	definitivně
podrobili	podrobit	k5eAaPmAgMnP	podrobit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tajemství	tajemství	k1gNnSc1	tajemství
relativně	relativně	k6eAd1	relativně
rychlého	rychlý	k2eAgInSc2d1	rychlý
římského	římský	k2eAgInSc2d1	římský
rozmachu	rozmach	k1gInSc2	rozmach
spočívalo	spočívat	k5eAaImAgNnS	spočívat
v	v	k7c6	v
uzavírání	uzavírání	k1gNnSc6	uzavírání
spojenectví	spojenectví	k1gNnSc2	spojenectví
s	s	k7c7	s
kdysi	kdysi	k6eAd1	kdysi
mocnými	mocný	k2eAgInPc7d1	mocný
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nakonec	nakonec	k6eAd1	nakonec
poraženými	poražený	k2eAgMnPc7d1	poražený
nepřáteli	nepřítel	k1gMnPc7	nepřítel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
stali	stát	k5eAaPmAgMnP	stát
loajálními	loajální	k2eAgMnPc7d1	loajální
římskými	římský	k2eAgMnPc7d1	římský
spojenci	spojenec	k1gMnPc7	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Podmaněnou	podmaněný	k2eAgFnSc4d1	Podmaněná
Itálii	Itálie	k1gFnSc4	Itálie
pokryli	pokrýt	k5eAaPmAgMnP	pokrýt
Římané	Říman	k1gMnPc1	Říman
sítí	sítí	k1gNnSc2	sítí
silnic	silnice	k1gFnPc2	silnice
<g/>
,	,	kIx,	,
systémem	systém	k1gInSc7	systém
předpolí	předpolí	k1gNnSc2	předpolí
a	a	k8xC	a
hlásek	hláska	k1gFnPc2	hláska
a	a	k8xC	a
na	na	k7c6	na
strategicky	strategicky	k6eAd1	strategicky
významných	významný	k2eAgNnPc6d1	významné
místech	místo	k1gNnPc6	místo
byly	být	k5eAaImAgFnP	být
zakládány	zakládat	k5eAaImNgFnP	zakládat
latinské	latinský	k2eAgFnPc1d1	Latinská
kolonie	kolonie	k1gFnPc1	kolonie
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jim	on	k3xPp3gMnPc3	on
zajistilo	zajistit	k5eAaPmAgNnS	zajistit
upevnění	upevnění	k1gNnSc1	upevnění
kontroly	kontrola	k1gFnSc2	kontrola
nad	nad	k7c7	nad
dobytým	dobytý	k2eAgNnSc7d1	dobyté
územím	území	k1gNnSc7	území
a	a	k8xC	a
utužení	utužení	k1gNnSc1	utužení
spojenectví	spojenectví	k1gNnSc2	spojenectví
s	s	k7c7	s
italskými	italský	k2eAgInPc7d1	italský
kmeny	kmen	k1gInPc7	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
svých	svůj	k3xOyFgFnPc2	svůj
dějin	dějiny	k1gFnPc2	dějiny
vyšel	vyjít	k5eAaPmAgInS	vyjít
Řím	Řím	k1gInSc1	Řím
jako	jako	k8xS	jako
pevný	pevný	k2eAgInSc1d1	pevný
státní	státní	k2eAgInSc1d1	státní
útvar	útvar	k1gInSc1	útvar
s	s	k7c7	s
mocnou	mocný	k2eAgFnSc7d1	mocná
armádou	armáda	k1gFnSc7	armáda
a	a	k8xC	a
značnými	značný	k2eAgInPc7d1	značný
předpoklady	předpoklad	k1gInPc7	předpoklad
k	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
růstu	růst	k1gInSc3	růst
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
byly	být	k5eAaImAgInP	být
položeny	položen	k2eAgInPc1d1	položen
základy	základ	k1gInPc1	základ
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
budoucímu	budoucí	k2eAgInSc3d1	budoucí
územnímu	územní	k2eAgInSc3d1	územní
rozmachu	rozmach	k1gInSc3	rozmach
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pyrrhos	Pyrrhos	k1gMnSc1	Pyrrhos
<g/>
,	,	kIx,	,
punské	punský	k2eAgFnSc2d1	punská
války	válka	k1gFnSc2	válka
–	–	k?	–
Hannibalova	Hannibalův	k2eAgFnSc1d1	Hannibalova
invaze	invaze	k1gFnSc1	invaze
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
samnitské	samnitský	k2eAgFnSc6d1	samnitská
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
298	[number]	k4	298
<g/>
–	–	k?	–
<g/>
290	[number]	k4	290
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Římané	Říman	k1gMnPc1	Říman
zdolali	zdolat	k5eAaPmAgMnP	zdolat
hrozivou	hrozivý	k2eAgFnSc4d1	hrozivá
koalici	koalice	k1gFnSc4	koalice
Samnitů	Samnit	k1gInPc2	Samnit
<g/>
,	,	kIx,	,
Lukánů	Lukán	k1gMnPc2	Lukán
<g/>
,	,	kIx,	,
Sabinů	Sabina	k1gMnPc2	Sabina
<g/>
,	,	kIx,	,
Umbrů	Umber	k1gMnPc2	Umber
<g/>
,	,	kIx,	,
Galů	Gal	k1gMnPc2	Gal
a	a	k8xC	a
Etrusků	Etrusk	k1gMnPc2	Etrusk
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jim	on	k3xPp3gMnPc3	on
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
získat	získat	k5eAaPmF	získat
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
celou	celý	k2eAgFnSc7d1	celá
Itálií	Itálie	k1gFnSc7	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
282	[number]	k4	282
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
Řím	Řím	k1gInSc1	Řím
zapletl	zaplést	k5eAaPmAgInS	zaplést
do	do	k7c2	do
soupeření	soupeření	k1gNnSc2	soupeření
řeckých	řecký	k2eAgFnPc2d1	řecká
obcí	obec	k1gFnPc2	obec
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zvané	zvaný	k2eAgFnPc4d1	zvaná
Magna	Magen	k2eAgNnPc4d1	Magen
Graecia	Graecium	k1gNnPc4	Graecium
<g/>
,	,	kIx,	,
když	když	k8xS	když
podporoval	podporovat	k5eAaImAgMnS	podporovat
města	město	k1gNnPc4	město
Thúrie	Thúrie	k1gFnSc2	Thúrie
a	a	k8xC	a
Rhégion	Rhégion	k1gInSc1	Rhégion
proti	proti	k7c3	proti
zdejšímu	zdejší	k2eAgNnSc3d1	zdejší
nejmocnějšímu	mocný	k2eAgNnSc3d3	nejmocnější
městu	město	k1gNnSc3	město
<g/>
,	,	kIx,	,
Tarentu	Tarento	k1gNnSc3	Tarento
<g/>
.	.	kIx.	.
</s>
<s>
Tarenťané	Tarenťan	k1gMnPc1	Tarenťan
se	se	k3xPyFc4	se
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
Římanům	Říman	k1gMnPc3	Říman
spojili	spojit	k5eAaPmAgMnP	spojit
s	s	k7c7	s
épeirským	épeirský	k2eAgMnSc7d1	épeirský
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
Pyrrhem	Pyrrhos	k1gMnSc7	Pyrrhos
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
280	[number]	k4	280
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
vylodil	vylodit	k5eAaPmAgMnS	vylodit
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
postavil	postavit	k5eAaPmAgMnS	postavit
se	se	k3xPyFc4	se
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
protiřímské	protiřímský	k2eAgFnSc2d1	protiřímský
aliance	aliance	k1gFnSc2	aliance
tvořené	tvořený	k2eAgFnSc6d1	tvořená
kromě	kromě	k7c2	kromě
místních	místní	k2eAgMnPc2d1	místní
Řeků	Řek	k1gMnPc2	Řek
také	také	k6eAd1	také
Lukány	Lukán	k2eAgFnPc1d1	Lukán
<g/>
,	,	kIx,	,
Brutijci	Brutijce	k1gMnPc1	Brutijce
a	a	k8xC	a
Samnity	Samnita	k1gFnPc1	Samnita
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
opakovaným	opakovaný	k2eAgFnPc3d1	opakovaná
porážkám	porážka	k1gFnPc3	porážka
se	se	k3xPyFc4	se
římský	římský	k2eAgInSc1d1	římský
politický	politický	k2eAgInSc1d1	politický
a	a	k8xC	a
spojenecký	spojenecký	k2eAgInSc1d1	spojenecký
systém	systém	k1gInSc1	systém
ukázal	ukázat	k5eAaPmAgInS	ukázat
být	být	k5eAaImF	být
dostatečně	dostatečně	k6eAd1	dostatečně
odolný	odolný	k2eAgInSc1d1	odolný
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
<g/>
,	,	kIx,	,
místo	místo	k6eAd1	místo
aby	aby	kYmCp3nP	aby
přijali	přijmout	k5eAaPmAgMnP	přijmout
Pyrrhem	Pyrrhos	k1gMnSc7	Pyrrhos
nabízené	nabízený	k2eAgFnSc2d1	nabízená
mírové	mírový	k2eAgFnSc2d1	mírová
podmínky	podmínka	k1gFnSc2	podmínka
<g/>
,	,	kIx,	,
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
tvrdohlavě	tvrdohlavě	k6eAd1	tvrdohlavě
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
znemožnili	znemožnit	k5eAaPmAgMnP	znemožnit
tak	tak	k9	tak
Épeirotům	Épeirot	k1gMnPc3	Épeirot
využít	využít	k5eAaPmF	využít
jejich	jejich	k3xOp3gNnPc2	jejich
příslovečně	příslovečně	k6eAd1	příslovečně
draze	draha	k1gFnSc3	draha
vykoupená	vykoupený	k2eAgNnPc4d1	vykoupené
"	"	kIx"	"
<g/>
Pyrrhova	Pyrrhův	k2eAgNnPc4d1	Pyrrhovo
vítězství	vítězství	k1gNnPc4	vítězství
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pyrrhovo	Pyrrhův	k2eAgNnSc1d1	Pyrrhovo
postavení	postavení	k1gNnSc1	postavení
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
stalo	stát	k5eAaPmAgNnS	stát
neudržitelným	udržitelný	k2eNgMnSc7d1	neudržitelný
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Beneventa	Benevento	k1gNnSc2	Benevento
v	v	k7c6	v
roce	rok	k1gInSc6	rok
275	[number]	k4	275
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
odplul	odplout	k5eAaPmAgMnS	odplout
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Římané	Říman	k1gMnPc1	Říman
všechna	všechen	k3xTgNnPc1	všechen
řecká	řecký	k2eAgNnPc1d1	řecké
města	město	k1gNnPc1	město
včetně	včetně	k7c2	včetně
Tarentu	Tarent	k1gInSc2	Tarent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Válka	válka	k1gFnSc1	válka
s	s	k7c7	s
Pyrrhem	Pyrrhos	k1gMnSc7	Pyrrhos
byla	být	k5eAaImAgFnS	být
jakousi	jakýsi	k3yIgFnSc7	jakýsi
předzvěstí	předzvěst	k1gFnSc7	předzvěst
dalších	další	k2eAgInPc2d1	další
<g/>
,	,	kIx,	,
mnohem	mnohem	k6eAd1	mnohem
náročnějších	náročný	k2eAgInPc2d2	náročnější
bojů	boj	k1gInPc2	boj
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
měly	mít	k5eAaImAgInP	mít
brzy	brzy	k6eAd1	brzy
následovat	následovat	k5eAaImF	následovat
<g/>
.	.	kIx.	.
</s>
<s>
Růst	růst	k1gInSc1	růst
římské	římský	k2eAgFnSc2d1	římská
moci	moc	k1gFnSc2	moc
na	na	k7c6	na
italském	italský	k2eAgInSc6d1	italský
poloostrově	poloostrov	k1gInSc6	poloostrov
vedl	vést	k5eAaImAgInS	vést
nevyhnutelně	vyhnutelně	k6eNd1	vyhnutelně
ke	k	k7c3	k
konfliktům	konflikt	k1gInPc3	konflikt
s	s	k7c7	s
jinou	jiný	k2eAgFnSc7d1	jiná
velkou	velký	k2eAgFnSc7d1	velká
mocností	mocnost	k1gFnSc7	mocnost
západního	západní	k2eAgNnSc2d1	západní
Středomoří	středomoří	k1gNnSc2	středomoří
–	–	k?	–
s	s	k7c7	s
Kartágem	Kartágo	k1gNnSc7	Kartágo
–	–	k?	–
které	který	k3yQgNnSc1	který
sledovalo	sledovat	k5eAaImAgNnS	sledovat
vlastní	vlastní	k2eAgInPc4d1	vlastní
dalekosáhlé	dalekosáhlý	k2eAgInPc4d1	dalekosáhlý
politické	politický	k2eAgInPc4d1	politický
a	a	k8xC	a
obchodní	obchodní	k2eAgInPc4d1	obchodní
zájmy	zájem	k1gInPc4	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Předmětem	předmět	k1gInSc7	předmět
sporu	spor	k1gInSc2	spor
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Sicílie	Sicílie	k1gFnSc1	Sicílie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
střetával	střetávat	k5eAaImAgMnS	střetávat
vliv	vliv	k1gInSc4	vliv
obou	dva	k4xCgInPc2	dva
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
fénické	fénický	k2eAgFnSc3d1	fénická
kolonizaci	kolonizace	k1gFnSc3	kolonizace
získala	získat	k5eAaPmAgFnS	získat
západní	západní	k2eAgFnSc1d1	západní
a	a	k8xC	a
severozápadní	severozápadní	k2eAgFnSc1d1	severozápadní
část	část	k1gFnSc1	část
ostrova	ostrov	k1gInSc2	ostrov
punský	punský	k2eAgInSc1d1	punský
charakter	charakter	k1gInSc1	charakter
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
pevnou	pevný	k2eAgFnSc7d1	pevná
součástí	součást	k1gFnSc7	součást
kartáginské	kartáginský	k2eAgFnSc2d1	kartáginská
námořní	námořní	k2eAgFnSc2d1	námořní
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
jihu	jih	k1gInSc2	jih
se	se	k3xPyFc4	se
od	od	k7c2	od
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
usazovali	usazovat	k5eAaImAgMnP	usazovat
Řekové	Řek	k1gMnPc1	Řek
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgNnSc1d1	zdejší
nejmocnější	mocný	k2eAgNnSc1d3	nejmocnější
řecké	řecký	k2eAgNnSc1d1	řecké
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
Syrakusy	Syrakusy	k1gFnPc1	Syrakusy
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
s	s	k7c7	s
Kartáginci	Kartáginec	k1gInPc7	Kartáginec
v	v	k7c6	v
trvalém	trvalý	k2eAgInSc6d1	trvalý
konfliktu	konflikt	k1gInSc6	konflikt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
incidentu	incident	k1gInSc6	incident
v	v	k7c6	v
Messaně	Messana	k1gFnSc6	Messana
a	a	k8xC	a
následném	následný	k2eAgInSc6d1	následný
římském	římský	k2eAgInSc6d1	římský
zásahu	zásah	k1gInSc6	zásah
se	se	k3xPyFc4	se
rovnováha	rovnováha	k1gFnSc1	rovnováha
sil	síla	k1gFnPc2	síla
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
vychýlila	vychýlit	k5eAaPmAgFnS	vychýlit
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Římanů	Říman	k1gMnPc2	Říman
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vyprovokovalo	vyprovokovat	k5eAaPmAgNnS	vyprovokovat
punskou	punský	k2eAgFnSc4d1	punská
flotilu	flotila	k1gFnSc4	flotila
k	k	k7c3	k
útoku	útok	k1gInSc3	útok
na	na	k7c4	na
italské	italský	k2eAgNnSc4d1	italské
pobřeží	pobřeží	k1gNnSc4	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
nepatrný	nepatrný	k2eAgInSc4d1	nepatrný
<g/>
,	,	kIx,	,
lokální	lokální	k2eAgInSc4d1	lokální
spor	spor	k1gInSc4	spor
tak	tak	k6eAd1	tak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
264	[number]	k4	264
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
přerostl	přerůst	k5eAaPmAgInS	přerůst
v	v	k7c4	v
rozhořčený	rozhořčený	k2eAgInSc4d1	rozhořčený
válečný	válečný	k2eAgInSc4d1	válečný
konflikt	konflikt	k1gInSc4	konflikt
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
mocnostmi	mocnost	k1gFnPc7	mocnost
–	–	k?	–
v	v	k7c4	v
první	první	k4xOgFnSc4	první
punskou	punský	k2eAgFnSc4d1	punská
válku	válka	k1gFnSc4	válka
(	(	kIx(	(
<g/>
264	[number]	k4	264
<g/>
–	–	k?	–
<g/>
241	[number]	k4	241
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
souši	souš	k1gFnSc6	souš
sice	sice	k8xC	sice
Římané	Říman	k1gMnPc1	Říman
sklízeli	sklízet	k5eAaImAgMnP	sklízet
jeden	jeden	k4xCgInSc4	jeden
úspěch	úspěch	k1gInSc4	úspěch
za	za	k7c7	za
druhým	druhý	k4xOgInSc7	druhý
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
byli	být	k5eAaImAgMnP	být
proti	proti	k7c3	proti
Kartágu	Kartágo	k1gNnSc3	Kartágo
bezmocní	bezmocný	k1gMnPc1	bezmocný
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
narušili	narušit	k5eAaPmAgMnP	narušit
kartáginskou	kartáginský	k2eAgFnSc4d1	kartáginská
námořní	námořní	k2eAgFnSc4d1	námořní
dominanci	dominance	k1gFnSc4	dominance
<g/>
,	,	kIx,	,
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
flotilu	flotila	k1gFnSc4	flotila
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
porážkách	porážka	k1gFnPc6	porážka
a	a	k8xC	a
zvratech	zvrat	k1gInPc6	zvrat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
Římané	Říman	k1gMnPc1	Říman
nakonec	nakonec	k6eAd1	nakonec
pevně	pevně	k6eAd1	pevně
usadili	usadit	k5eAaPmAgMnP	usadit
na	na	k7c4	na
Sicílii	Sicílie	k1gFnSc4	Sicílie
a	a	k8xC	a
rozdrtili	rozdrtit	k5eAaPmAgMnP	rozdrtit
kartáginskou	kartáginský	k2eAgFnSc4d1	kartáginská
flotilu	flotila	k1gFnSc4	flotila
v	v	k7c6	v
námořní	námořní	k2eAgFnSc6d1	námořní
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Aegatských	Aegatský	k2eAgInPc2d1	Aegatský
ostrovů	ostrov	k1gInPc2	ostrov
v	v	k7c6	v
roce	rok	k1gInSc6	rok
241	[number]	k4	241
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
porážce	porážka	k1gFnSc6	porážka
Kartáginci	Kartáginec	k1gMnPc1	Kartáginec
požádali	požádat	k5eAaPmAgMnP	požádat
o	o	k7c4	o
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mírově	mírově	k6eAd1	mírově
smlouvě	smlouva	k1gFnSc6	smlouva
s	s	k7c7	s
Římem	Řím	k1gInSc7	Řím
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
Kartágo	Kartágo	k1gNnSc1	Kartágo
Sicílii	Sicílie	k1gFnSc3	Sicílie
a	a	k8xC	a
zavázalo	zavázat	k5eAaPmAgNnS	zavázat
se	se	k3xPyFc4	se
k	k	k7c3	k
placení	placení	k1gNnSc3	placení
vysokých	vysoký	k2eAgFnPc2d1	vysoká
reparací	reparace	k1gFnPc2	reparace
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nedostávalo	dostávat	k5eNaImAgNnS	dostávat
peněz	peníze	k1gInPc2	peníze
k	k	k7c3	k
vyplacení	vyplacení	k1gNnSc3	vyplacení
žoldnéřské	žoldnéřský	k2eAgFnSc2d1	žoldnéřská
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
během	během	k7c2	během
její	její	k3xOp3gFnSc2	její
následné	následný	k2eAgFnSc2d1	následná
vzpoury	vzpoura	k1gFnSc2	vzpoura
se	se	k3xPyFc4	se
Římané	Říman	k1gMnPc1	Říman
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
také	také	k9	také
Sardinie	Sardinie	k1gFnSc2	Sardinie
<g/>
.	.	kIx.	.
</s>
<s>
Kartágo	Kartágo	k1gNnSc1	Kartágo
se	se	k3xPyFc4	se
však	však	k9	však
přesto	přesto	k8xC	přesto
velice	velice	k6eAd1	velice
rychle	rychle	k6eAd1	rychle
znovu	znovu	k6eAd1	znovu
postavilo	postavit	k5eAaPmAgNnS	postavit
na	na	k7c4	na
nohy	noha	k1gFnPc4	noha
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
mu	on	k3xPp3gNnSc3	on
dopomohl	dopomoct	k5eAaPmAgMnS	dopomoct
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
Hamilkar	Hamilkara	k1gFnPc2	Hamilkara
Barkas	barkasa	k1gFnPc2	barkasa
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc7	jeho
nástupci	nástupce	k1gMnPc7	nástupce
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Barkovců	Barkovec	k1gInPc2	Barkovec
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
vykompenzovali	vykompenzovat	k5eAaPmAgMnP	vykompenzovat
ztráty	ztráta	k1gFnPc4	ztráta
Kartága	Kartágo	k1gNnSc2	Kartágo
v	v	k7c6	v
první	první	k4xOgFnSc6	první
punské	punský	k2eAgFnSc6d1	punská
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
zaměřili	zaměřit	k5eAaPmAgMnP	zaměřit
své	svůj	k3xOyFgNnSc4	svůj
úsilí	úsilí	k1gNnSc4	úsilí
do	do	k7c2	do
Hispánie	Hispánie	k1gFnSc2	Hispánie
a	a	k8xC	a
během	během	k7c2	během
několika	několik	k4yIc2	několik
málo	málo	k6eAd1	málo
let	léto	k1gNnPc2	léto
zde	zde	k6eAd1	zde
zřídili	zřídit	k5eAaPmAgMnP	zřídit
novou	nový	k2eAgFnSc4d1	nová
punskou	punský	k2eAgFnSc4d1	punská
koloniální	koloniální	k2eAgFnSc4d1	koloniální
říši	říše	k1gFnSc4	říše
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInSc2	jejich
výboje	výboj	k1gInSc2	výboj
pokusili	pokusit	k5eAaPmAgMnP	pokusit
omezit	omezit	k5eAaPmF	omezit
<g/>
:	:	kIx,	:
v	v	k7c6	v
roce	rok	k1gInSc6	rok
226	[number]	k4	226
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
musel	muset	k5eAaImAgMnS	muset
Hasdrubal	Hasdrubal	k1gFnSc4	Hasdrubal
Sličný	sličný	k2eAgMnSc1d1	sličný
<g/>
,	,	kIx,	,
Hamilkarův	Hamilkarův	k2eAgMnSc1d1	Hamilkarův
zeť	zeť	k1gMnSc1	zeť
<g/>
,	,	kIx,	,
uznat	uznat	k5eAaPmF	uznat
řeku	řeka	k1gFnSc4	řeka
Ebro	Ebro	k6eAd1	Ebro
za	za	k7c4	za
hranici	hranice	k1gFnSc4	hranice
římské	římský	k2eAgFnSc2d1	římská
sféry	sféra	k1gFnSc2	sféra
vlivu	vliv	k1gInSc2	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
zůstávali	zůstávat	k5eAaImAgMnP	zůstávat
taktéž	taktéž	k?	taktéž
aktivní	aktivní	k2eAgMnPc1d1	aktivní
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Telamonu	telamon	k1gInSc2	telamon
odrazili	odrazit	k5eAaPmAgMnP	odrazit
ohromný	ohromný	k2eAgInSc4d1	ohromný
vpád	vpád	k1gInSc4	vpád
Galů	Gal	k1gMnPc2	Gal
do	do	k7c2	do
Etrurie	Etrurie	k1gFnSc2	Etrurie
a	a	k8xC	a
následně	následně	k6eAd1	následně
si	se	k3xPyFc3	se
podrobili	podrobit	k5eAaPmAgMnP	podrobit
celou	celý	k2eAgFnSc4d1	celá
Předalpskou	předalpský	k2eAgFnSc4d1	Předalpská
Galii	Galie	k1gFnSc4	Galie
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
severní	severní	k2eAgFnSc1d1	severní
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
ilyrských	ilyrský	k2eAgMnPc2d1	ilyrský
pirátů	pirát	k1gMnPc2	pirát
rovněž	rovněž	k9	rovněž
posílili	posílit	k5eAaPmAgMnP	posílit
své	svůj	k3xOyFgFnPc4	svůj
pozice	pozice	k1gFnPc4	pozice
na	na	k7c6	na
východním	východní	k2eAgInSc6d1	východní
břehu	břeh	k1gInSc6	břeh
Jaderského	jaderský	k2eAgNnSc2d1	Jaderské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
Hamilkarův	Hamilkarův	k2eAgMnSc1d1	Hamilkarův
syn	syn	k1gMnSc1	syn
Hannibal	Hannibal	k1gInSc4	Hannibal
napadl	napadnout	k5eAaPmAgMnS	napadnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
219	[number]	k4	219
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
město	město	k1gNnSc4	město
Saguntum	Saguntum	k1gNnSc1	Saguntum
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
sice	sice	k8xC	sice
bylo	být	k5eAaImAgNnS	být
římským	římský	k2eAgMnSc7d1	římský
spojencem	spojenec	k1gMnSc7	spojenec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
leželo	ležet	k5eAaImAgNnS	ležet
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Ebro	Ebro	k6eAd1	Ebro
<g/>
,	,	kIx,	,
žádali	žádat	k5eAaImAgMnP	žádat
Římané	Říman	k1gMnPc1	Říman
Hannibalovo	Hannibalův	k2eAgNnSc4d1	Hannibalovo
vydání	vydání	k1gNnSc4	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zamítavé	zamítavý	k2eAgFnSc6d1	zamítavá
reakci	reakce	k1gFnSc6	reakce
z	z	k7c2	z
Kartága	Kartágo	k1gNnSc2	Kartágo
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
mezi	mezi	k7c7	mezi
Římany	Říman	k1gMnPc7	Říman
a	a	k8xC	a
Kartáginci	Kartáginec	k1gMnPc7	Kartáginec
druhá	druhý	k4xOgFnSc1	druhý
punská	punský	k2eAgFnSc1d1	punská
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
218	[number]	k4	218
<g/>
–	–	k?	–
<g/>
201	[number]	k4	201
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hannibal	Hannibal	k1gInSc1	Hannibal
podnikl	podniknout	k5eAaPmAgInS	podniknout
namáhavou	namáhavý	k2eAgFnSc4d1	namáhavá
a	a	k8xC	a
strastiplnou	strastiplný	k2eAgFnSc4d1	strastiplná
cestu	cesta	k1gFnSc4	cesta
Hispánií	Hispánie	k1gFnPc2	Hispánie
a	a	k8xC	a
jižní	jižní	k2eAgFnSc7d1	jižní
Galií	Galie	k1gFnSc7	Galie
a	a	k8xC	a
po	po	k7c6	po
překročení	překročení	k1gNnSc6	překročení
Alp	Alpy	k1gFnPc2	Alpy
vpadl	vpadnout	k5eAaPmAgInS	vpadnout
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
vojskem	vojsko	k1gNnSc7	vojsko
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sérii	série	k1gFnSc6	série
několika	několik	k4yIc2	několik
geniálních	geniální	k2eAgMnPc2d1	geniální
vítězstvích	vítězství	k1gNnPc6	vítězství
nad	nad	k7c7	nad
Římany	Říman	k1gMnPc7	Říman
<g/>
,	,	kIx,	,
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Trasimenského	Trasimenský	k2eAgNnSc2d1	Trasimenské
jezera	jezero	k1gNnSc2	jezero
a	a	k8xC	a
především	především	k9	především
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Kann	Kanny	k1gFnPc2	Kanny
v	v	k7c6	v
roce	rok	k1gInSc6	rok
216	[number]	k4	216
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
(	(	kIx(	(
<g/>
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejtěžší	těžký	k2eAgFnSc4d3	nejtěžší
porážku	porážka	k1gFnSc4	porážka
v	v	k7c6	v
římských	římský	k2eAgFnPc6d1	římská
dějinách	dějiny	k1gFnPc6	dějiny
vůbec	vůbec	k9	vůbec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přivedl	přivést	k5eAaPmAgInS	přivést
Hannibal	Hannibal	k1gInSc1	Hannibal
Řím	Řím	k1gInSc1	Řím
na	na	k7c4	na
pokraj	pokraj	k1gInSc4	pokraj
záhuby	záhuba	k1gFnSc2	záhuba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
osudovém	osudový	k2eAgInSc6d1	osudový
momentě	moment	k1gInSc6	moment
se	se	k3xPyFc4	se
však	však	k9	však
opět	opět	k6eAd1	opět
osvědčila	osvědčit	k5eAaPmAgFnS	osvědčit
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
semknutost	semknutost	k1gFnSc1	semknutost
římské	římský	k2eAgFnSc2d1	římská
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
nezdolnost	nezdolnost	k1gFnSc1	nezdolnost
římského	římský	k2eAgInSc2d1	římský
spojeneckého	spojenecký	k2eAgInSc2d1	spojenecký
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
i	i	k9	i
přes	přes	k7c4	přes
svá	svůj	k3xOyFgNnPc4	svůj
vítězství	vítězství	k1gNnPc4	vítězství
zůstal	zůstat	k5eAaPmAgInS	zůstat
Hannibal	Hannibal	k1gInSc1	Hannibal
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
víceméně	víceméně	k9	víceméně
izolován	izolován	k2eAgInSc1d1	izolován
<g/>
.	.	kIx.	.
</s>
<s>
Římská	římský	k2eAgFnSc1d1	římská
protiofenzíva	protiofenzíva	k1gFnSc1	protiofenzíva
přivedla	přivést	k5eAaPmAgFnS	přivést
válku	válka	k1gFnSc4	válka
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
a	a	k8xC	a
do	do	k7c2	do
punské	punský	k2eAgFnSc2d1	punská
Hispánie	Hispánie	k1gFnSc2	Hispánie
<g/>
.	.	kIx.	.
</s>
<s>
Římský	římský	k2eAgMnSc1d1	římský
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
Scipio	Scipio	k1gMnSc1	Scipio
Africanus	Africanus	k1gMnSc1	Africanus
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dobyl	dobýt	k5eAaPmAgMnS	dobýt
na	na	k7c6	na
Kartágincích	Kartáginec	k1gInPc6	Kartáginec
Hispánii	Hispánie	k1gFnSc3	Hispánie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
204	[number]	k4	204
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
přesunul	přesunout	k5eAaPmAgMnS	přesunout
do	do	k7c2	do
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
202	[number]	k4	202
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
porazil	porazit	k5eAaPmAgInS	porazit
Hannibala	Hannibal	k1gMnSc4	Hannibal
v	v	k7c6	v
rozhodující	rozhodující	k2eAgFnSc6d1	rozhodující
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Zamy	Zama	k1gFnSc2	Zama
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kartágo	Kartágo	k1gNnSc1	Kartágo
se	se	k3xPyFc4	se
muselo	muset	k5eAaImAgNnS	muset
vzdát	vzdát	k5eAaPmF	vzdát
veškerých	veškerý	k3xTgFnPc2	veškerý
svých	svůj	k3xOyFgNnPc2	svůj
mimoafrických	mimoafrický	k2eAgNnPc2d1	mimoafrický
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
rozpustit	rozpustit	k5eAaPmF	rozpustit
svoji	svůj	k3xOyFgFnSc4	svůj
flotilu	flotila	k1gFnSc4	flotila
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
nuceno	nucen	k2eAgNnSc1d1	nuceno
vyplatit	vyplatit	k5eAaPmF	vyplatit
Římanům	Říman	k1gMnPc3	Říman
ohromné	ohromný	k2eAgFnPc4d1	ohromná
válečné	válečný	k2eAgFnPc4d1	válečná
kontribuce	kontribuce	k1gFnPc4	kontribuce
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
byla	být	k5eAaImAgFnS	být
definitivně	definitivně	k6eAd1	definitivně
zlomena	zlomit	k5eAaPmNgFnS	zlomit
jeho	jeho	k3xOp3gFnSc1	jeho
moc	moc	k1gFnSc1	moc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
politice	politika	k1gFnSc6	politika
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
zcela	zcela	k6eAd1	zcela
závislým	závislý	k2eAgInSc7d1	závislý
na	na	k7c6	na
vůli	vůle	k1gFnSc6	vůle
Římanů	Říman	k1gMnPc2	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
si	se	k3xPyFc3	se
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
významné	významný	k2eAgNnSc4d1	významné
obchodní	obchodní	k2eAgNnSc4d1	obchodní
postavení	postavení	k1gNnSc4	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Řím	Řím	k1gInSc1	Řím
se	se	k3xPyFc4	se
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
vítězství	vítězství	k1gNnSc6	vítězství
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
punské	punský	k2eAgFnSc6d1	punská
válce	válka	k1gFnSc6	válka
stal	stát	k5eAaPmAgMnS	stát
přední	přední	k2eAgFnSc7d1	přední
mocností	mocnost	k1gFnSc7	mocnost
západního	západní	k2eAgNnSc2d1	západní
Středomoří	středomoří	k1gNnSc2	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Sicílie	Sicílie	k1gFnSc1	Sicílie
a	a	k8xC	a
Sardinie	Sardinie	k1gFnSc1	Sardinie
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
Hispánie	Hispánie	k1gFnSc1	Hispánie
stala	stát	k5eAaPmAgFnS	stát
římskou	římský	k2eAgFnSc7d1	římská
provincií	provincie	k1gFnSc7	provincie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Expanze	expanze	k1gFnPc1	expanze
ve	v	k7c6	v
východním	východní	k2eAgNnSc6d1	východní
Středomoří	středomoří	k1gNnSc6	středomoří
===	===	k?	===
</s>
</p>
<p>
<s>
Bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
nad	nad	k7c7	nad
Kartágem	Kartágo	k1gNnSc7	Kartágo
se	se	k3xPyFc4	se
římská	římský	k2eAgFnSc1d1	římská
politika	politika	k1gFnSc1	politika
zaměřila	zaměřit	k5eAaPmAgFnS	zaměřit
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Seleukovský	Seleukovský	k2eAgMnSc1d1	Seleukovský
král	král	k1gMnSc1	král
Antiochos	Antiochos	k1gMnSc1	Antiochos
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
makedonský	makedonský	k2eAgMnSc1d1	makedonský
král	král	k1gMnSc1	král
Filip	Filip	k1gMnSc1	Filip
V.	V.	kA	V.
využili	využít	k5eAaPmAgMnP	využít
vnitřní	vnitřní	k2eAgFnPc4d1	vnitřní
nestability	nestabilita	k1gFnPc4	nestabilita
ptolemaiovské	ptolemaiovský	k2eAgFnSc2d1	ptolemaiovský
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
si	se	k3xPyFc3	se
její	její	k3xOp3gNnPc4	její
území	území	k1gNnPc4	území
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Helénistické	helénistický	k2eAgInPc1d1	helénistický
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
Pergamon	Pergamon	k1gNnSc1	Pergamon
a	a	k8xC	a
Rhodos	Rhodos	k1gInSc1	Rhodos
<g/>
,	,	kIx,	,
ohrožené	ohrožený	k2eAgNnSc1d1	ohrožené
touto	tento	k3xDgFnSc7	tento
aliancí	aliance	k1gFnSc7	aliance
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
201	[number]	k4	201
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
obrátily	obrátit	k5eAaPmAgFnP	obrátit
na	na	k7c4	na
Řím	Řím	k1gInSc4	Řím
se	s	k7c7	s
žádostí	žádost	k1gFnSc7	žádost
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Filipem	Filip	k1gMnSc7	Filip
V.	V.	kA	V.
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
punské	punský	k2eAgFnSc2d1	punská
války	válka	k1gFnSc2	válka
spojil	spojit	k5eAaPmAgMnS	spojit
s	s	k7c7	s
Hannibalem	Hannibal	k1gMnSc7	Hannibal
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Římané	Říman	k1gMnPc1	Říman
střetli	střetnout	k5eAaPmAgMnP	střetnout
již	již	k6eAd1	již
v	v	k7c4	v
nerozhodné	rozhodný	k2eNgInPc4d1	nerozhodný
první	první	k4xOgInPc4	první
makedonské	makedonský	k2eAgInPc4d1	makedonský
válce	válec	k1gInPc4	válec
<g/>
.	.	kIx.	.
</s>
<s>
Senát	senát	k1gInSc1	senát
se	se	k3xPyFc4	se
tudíž	tudíž	k8xC	tudíž
ochotně	ochotně	k6eAd1	ochotně
chopil	chopit	k5eAaPmAgMnS	chopit
této	tento	k3xDgFnSc2	tento
záminky	záminka	k1gFnSc2	záminka
k	k	k7c3	k
boji	boj	k1gInSc3	boj
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
druhá	druhý	k4xOgFnSc1	druhý
makedonská	makedonský	k2eAgFnSc1d1	makedonská
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Konflikt	konflikt	k1gInSc1	konflikt
skončil	skončit	k5eAaPmAgInS	skončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
197	[number]	k4	197
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
římským	římský	k2eAgNnSc7d1	římské
vítězstvím	vítězství	k1gNnSc7	vítězství
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Kynoskefal	Kynoskefal	k1gFnSc2	Kynoskefal
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
definitivně	definitivně	k6eAd1	definitivně
pohřbilo	pohřbít	k5eAaPmAgNnS	pohřbít
makedonskou	makedonský	k2eAgFnSc4d1	makedonská
hegemonii	hegemonie	k1gFnSc4	hegemonie
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
isthmických	isthmický	k2eAgFnPc6d1	isthmický
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Korintu	Korint	k1gInSc6	Korint
v	v	k7c6	v
roce	rok	k1gInSc6	rok
196	[number]	k4	196
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
tak	tak	k6eAd1	tak
mohl	moct	k5eAaImAgMnS	moct
Titus	Titus	k1gMnSc1	Titus
Quinctius	Quinctius	k1gMnSc1	Quinctius
Flamininus	Flamininus	k1gMnSc1	Flamininus
slavnostně	slavnostně	k6eAd1	slavnostně
vyhlásit	vyhlásit	k5eAaPmF	vyhlásit
svobodu	svoboda	k1gFnSc4	svoboda
Helénů	Helén	k1gMnPc2	Helén
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
angažování	angažování	k1gNnSc3	angažování
ve	v	k7c6	v
východních	východní	k2eAgFnPc6d1	východní
záležitostech	záležitost	k1gFnPc6	záležitost
přiměla	přimět	k5eAaPmAgFnS	přimět
Římany	Říman	k1gMnPc4	Říman
expanze	expanze	k1gFnSc2	expanze
ambiciózního	ambiciózní	k2eAgMnSc2d1	ambiciózní
seleukovského	seleukovský	k2eAgMnSc2d1	seleukovský
krále	král	k1gMnSc2	král
Antiocha	Antioch	k1gMnSc2	Antioch
III	III	kA	III
<g/>
.	.	kIx.	.
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
v	v	k7c6	v
letech	let	k1gInPc6	let
192	[number]	k4	192
<g/>
–	–	k?	–
<g/>
188	[number]	k4	188
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
vedli	vést	k5eAaImAgMnP	vést
boj	boj	k1gInSc4	boj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
římském	římský	k2eAgNnSc6d1	římské
vítězství	vítězství	k1gNnSc6	vítězství
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Magnésie	Magnésie	k1gFnSc2	Magnésie
se	se	k3xPyFc4	se
Antiochos	Antiochos	k1gMnSc1	Antiochos
musel	muset	k5eAaImAgMnS	muset
vzdát	vzdát	k5eAaPmF	vzdát
většiny	většina	k1gFnSc2	většina
svých	svůj	k3xOyFgNnPc2	svůj
území	území	k1gNnPc2	území
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
Římu	Řím	k1gInSc3	Řím
podařilo	podařit	k5eAaPmAgNnS	podařit
neutralizovat	neutralizovat	k5eAaBmF	neutralizovat
všechny	všechen	k3xTgMnPc4	všechen
své	svůj	k3xOyFgMnPc4	svůj
potenciální	potenciální	k2eAgMnPc4d1	potenciální
soupeře	soupeř	k1gMnPc4	soupeř
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nejmocnějším	mocný	k2eAgInSc7d3	nejmocnější
státem	stát	k1gInSc7	stát
rovněž	rovněž	k9	rovněž
ve	v	k7c6	v
východním	východní	k2eAgNnSc6d1	východní
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
pokus	pokus	k1gInSc1	pokus
o	o	k7c6	o
obnovení	obnovení	k1gNnSc6	obnovení
makedonské	makedonský	k2eAgFnSc2d1	makedonská
převahy	převaha	k1gFnSc2	převaha
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
byl	být	k5eAaImAgInS	být
potlačen	potlačit	k5eAaPmNgInS	potlačit
po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
Lucia	Lucius	k1gMnSc2	Lucius
Aemilia	Aemilius	k1gMnSc2	Aemilius
Paulla	Paull	k1gMnSc2	Paull
nad	nad	k7c7	nad
králem	král	k1gMnSc7	král
Perseem	Perseus	k1gMnSc7	Perseus
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Pydny	Pydna	k1gFnSc2	Pydna
v	v	k7c6	v
roce	rok	k1gInSc6	rok
168	[number]	k4	168
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
třetí	třetí	k4xOgFnSc2	třetí
makedonské	makedonský	k2eAgFnSc2d1	makedonská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Makedonské	makedonský	k2eAgNnSc1d1	makedonské
království	království	k1gNnSc1	království
bylo	být	k5eAaImAgNnS	být
následně	následně	k6eAd1	následně
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgInS	být
římskou	římský	k2eAgFnSc7d1	římská
diplomacií	diplomacie	k1gFnSc7	diplomacie
odkázán	odkázat	k5eAaPmNgInS	odkázat
do	do	k7c2	do
příslušných	příslušný	k2eAgNnPc2d1	příslušné
mezí	mezit	k5eAaImIp3nS	mezit
také	také	k9	také
seleukovský	seleukovský	k2eAgMnSc1d1	seleukovský
král	král	k1gMnSc1	král
Antiochos	Antiochos	k1gMnSc1	Antiochos
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Epifanés	Epifanés	k1gInSc1	Epifanés
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
zmocnit	zmocnit	k5eAaPmF	zmocnit
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
148	[number]	k4	148
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byla	být	k5eAaImAgFnS	být
Makedonie	Makedonie	k1gFnSc1	Makedonie
po	po	k7c6	po
poslední	poslední	k2eAgFnSc6d1	poslední
protiřímské	protiřímský	k2eAgFnSc6d1	protiřímský
vzpouře	vzpoura	k1gFnSc6	vzpoura
přeměněna	přeměnit	k5eAaPmNgFnS	přeměnit
v	v	k7c4	v
římskou	římský	k2eAgFnSc4d1	římská
provincii	provincie	k1gFnSc4	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Konflikt	konflikt	k1gInSc1	konflikt
s	s	k7c7	s
numidským	numidský	k2eAgMnSc7d1	numidský
králem	král	k1gMnSc7	král
Massinissou	Massinissa	k1gFnSc7	Massinissa
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
spojencem	spojenec	k1gMnSc7	spojenec
Říma	Řím	k1gInSc2	Řím
a	a	k8xC	a
svévolně	svévolně	k6eAd1	svévolně
napadal	napadat	k5eAaImAgMnS	napadat
kartáginské	kartáginský	k2eAgNnSc4d1	kartáginské
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
vehnal	vehnat	k5eAaPmAgMnS	vehnat
Kartágo	Kartágo	k1gNnSc4	Kartágo
do	do	k7c2	do
třetí	třetí	k4xOgFnSc2	třetí
punské	punský	k2eAgFnSc2d1	punská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
konci	konec	k1gInSc6	konec
byla	být	k5eAaImAgFnS	být
punská	punský	k2eAgFnSc1d1	punská
metropole	metropole	k1gFnSc1	metropole
zničena	zničit	k5eAaPmNgFnS	zničit
a	a	k8xC	a
prokleta	proklít	k5eAaPmNgFnS	proklít
Římany	Říman	k1gMnPc7	Říman
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
velel	velet	k5eAaImAgInS	velet
Scipio	Scipio	k6eAd1	Scipio
Aemilianus	Aemilianus	k1gInSc1	Aemilianus
<g/>
,	,	kIx,	,
adoptivní	adoptivní	k2eAgMnSc1d1	adoptivní
vnuk	vnuk	k1gMnSc1	vnuk
přemožitele	přemožitel	k1gMnSc4	přemožitel
Hannibala	Hannibal	k1gMnSc4	Hannibal
<g/>
.	.	kIx.	.
</s>
<s>
Dobytá	dobytý	k2eAgNnPc1d1	dobyté
území	území	k1gNnPc1	území
byla	být	k5eAaImAgNnP	být
přeměněna	přeměnit	k5eAaPmNgNnP	přeměnit
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Afriku	afrik	k1gInSc2	afrik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
146	[number]	k4	146
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
vzala	vzít	k5eAaPmAgFnS	vzít
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
achajského	achajský	k2eAgInSc2d1	achajský
spolku	spolek	k1gInSc2	spolek
za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
i	i	k8xC	i
nezávislost	nezávislost	k1gFnSc4	nezávislost
Helénů	Helén	k1gMnPc2	Helén
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc4d1	stejný
osud	osud	k1gInSc4	osud
jako	jako	k8xS	jako
Kartágo	Kartágo	k1gNnSc4	Kartágo
postihl	postihnout	k5eAaPmAgInS	postihnout
i	i	k9	i
Korint	Korint	k1gInSc1	Korint
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
zničeno	zničen	k2eAgNnSc1d1	zničeno
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc4	jeho
obyvatelé	obyvatel	k1gMnPc1	obyvatel
prodáni	prodán	k2eAgMnPc1d1	prodán
do	do	k7c2	do
otroctví	otroctví	k1gNnPc2	otroctví
a	a	k8xC	a
elity	elita	k1gFnPc4	elita
deportovány	deportován	k2eAgFnPc4d1	deportována
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
133	[number]	k4	133
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Pergamon	pergamon	k1gInSc1	pergamon
na	na	k7c6	na
základě	základ	k1gInSc6	základ
závěti	závěť	k1gFnSc2	závěť
posledního	poslední	k2eAgNnSc2d1	poslední
pergamského	pergamský	k2eAgNnSc2d1	Pergamské
krále	král	k1gMnPc4	král
římským	římský	k2eAgInSc7d1	římský
majetkem	majetek	k1gInSc7	majetek
a	a	k8xC	a
následně	následně	k6eAd1	následně
byl	být	k5eAaImAgInS	být
přeměněn	přeměnit	k5eAaPmNgInS	přeměnit
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
nových	nový	k2eAgFnPc6d1	nová
provinciích	provincie	k1gFnPc6	provincie
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
bohatých	bohatý	k2eAgFnPc6d1	bohatá
východních	východní	k2eAgFnPc6d1	východní
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
vybírány	vybírán	k2eAgFnPc4d1	vybírána
daně	daň	k1gFnPc4	daň
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
soukromých	soukromý	k2eAgMnPc2d1	soukromý
výběrčích	výběrčí	k1gMnPc2	výběrčí
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
římských	římský	k2eAgMnPc2d1	římský
jezdců	jezdec	k1gMnPc2	jezdec
(	(	kIx(	(
<g/>
equites	equites	k1gMnSc1	equites
<g/>
)	)	kIx)	)
a	a	k8xC	a
patricijů	patricij	k1gMnPc2	patricij
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
státu	stát	k1gInSc2	stát
odváděli	odvádět	k5eAaImAgMnP	odvádět
určitou	určitý	k2eAgFnSc4d1	určitá
fixní	fixní	k2eAgFnSc4d1	fixní
částku	částka	k1gFnSc4	částka
<g/>
.	.	kIx.	.
</s>
<s>
Přirozeně	přirozeně	k6eAd1	přirozeně
bylo	být	k5eAaImAgNnS	být
celkem	celkem	k6eAd1	celkem
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
,	,	kIx,	,
že	že	k8xS	že
skutečně	skutečně	k6eAd1	skutečně
vybrané	vybraný	k2eAgNnSc1d1	vybrané
množství	množství	k1gNnSc1	množství
daní	daň	k1gFnPc2	daň
bylo	být	k5eAaImAgNnS	být
mnohem	mnohem	k6eAd1	mnohem
vyšší	vysoký	k2eAgMnSc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Výkon	výkon	k1gInSc1	výkon
funkce	funkce	k1gFnSc2	funkce
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
stal	stát	k5eAaPmAgMnS	stát
soukromou	soukromý	k2eAgFnSc7d1	soukromá
záležitostí	záležitost	k1gFnSc7	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nadměrný	nadměrný	k2eAgInSc1d1	nadměrný
výběr	výběr	k1gInSc1	výběr
daní	daň	k1gFnPc2	daň
vedl	vést	k5eAaImAgInS	vést
v	v	k7c6	v
dotčených	dotčený	k2eAgFnPc6d1	dotčená
provinciích	provincie	k1gFnPc6	provincie
k	k	k7c3	k
drancování	drancování	k1gNnSc3	drancování
posvěcenému	posvěcený	k2eAgInSc3d1	posvěcený
zákonem	zákon	k1gInSc7	zákon
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
nespokojenosti	nespokojenost	k1gFnSc3	nespokojenost
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
a	a	k8xC	a
k	k	k7c3	k
povstáním	povstání	k1gNnPc3	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Římská	římský	k2eAgNnPc1d1	římské
vítězství	vítězství	k1gNnPc1	vítězství
byla	být	k5eAaImAgNnP	být
skutečně	skutečně	k6eAd1	skutečně
velkolepá	velkolepý	k2eAgNnPc1d1	velkolepé
<g/>
,	,	kIx,	,
uvnitř	uvnitř	k7c2	uvnitř
římského	římský	k2eAgInSc2d1	římský
státu	stát	k1gInSc2	stát
však	však	k9	však
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
pozvolnému	pozvolný	k2eAgInSc3d1	pozvolný
rozkladu	rozklad	k1gInSc3	rozklad
republikánských	republikánský	k2eAgFnPc2d1	republikánská
hodnot	hodnota	k1gFnPc2	hodnota
a	a	k8xC	a
uspořádání	uspořádání	k1gNnSc2	uspořádání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Krize	krize	k1gFnSc1	krize
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
bratři	bratr	k1gMnPc1	bratr
Gracchové	Gracchová	k1gFnSc2	Gracchová
===	===	k?	===
</s>
</p>
<p>
<s>
Vnitropoliticky	vnitropoliticky	k6eAd1	vnitropoliticky
provázelo	provázet	k5eAaImAgNnS	provázet
římský	římský	k2eAgInSc4d1	římský
mocenský	mocenský	k2eAgInSc4d1	mocenský
vzrůst	vzrůst	k1gInSc4	vzrůst
pokračující	pokračující	k2eAgNnSc4d1	pokračující
utváření	utváření	k1gNnSc4	utváření
nobility	nobilita	k1gFnSc2	nobilita
<g/>
.	.	kIx.	.
</s>
<s>
Nejmocnější	mocný	k2eAgInPc1d3	nejmocnější
aristokratické	aristokratický	k2eAgInPc1d1	aristokratický
rody	rod	k1gInPc1	rod
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc1	jejichž
příslušníci	příslušník	k1gMnPc1	příslušník
se	se	k3xPyFc4	se
vyznamenávali	vyznamenávat	k5eAaImAgMnP	vyznamenávat
ve	v	k7c6	v
válkách	válka	k1gFnPc6	válka
<g/>
,	,	kIx,	,
získaly	získat	k5eAaPmAgFnP	získat
faktický	faktický	k2eAgInSc4d1	faktický
monopol	monopol	k1gInSc4	monopol
v	v	k7c6	v
přístupu	přístup	k1gInSc6	přístup
k	k	k7c3	k
magistraturám	magistratura	k1gFnPc3	magistratura
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
do	do	k7c2	do
senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
stával	stávat	k5eAaImAgInS	stávat
rozhodujícím	rozhodující	k2eAgInSc7d1	rozhodující
orgánem	orgán	k1gInSc7	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Prestiž	prestiž	k1gFnSc1	prestiž
lidového	lidový	k2eAgNnSc2d1	lidové
shromáždění	shromáždění	k1gNnSc2	shromáždění
naopak	naopak	k6eAd1	naopak
upadala	upadat	k5eAaPmAgFnS	upadat
<g/>
.	.	kIx.	.
</s>
<s>
Nadále	nadále	k6eAd1	nadále
sice	sice	k8xC	sice
schvalovalo	schvalovat	k5eAaImAgNnS	schvalovat
zákony	zákon	k1gInPc4	zákon
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nedisponovalo	disponovat	k5eNaBmAgNnS	disponovat
zákonodárnou	zákonodárný	k2eAgFnSc7d1	zákonodárná
iniciativou	iniciativa	k1gFnSc7	iniciativa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Republika	republika	k1gFnSc1	republika
se	se	k3xPyFc4	se
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
územnímu	územní	k2eAgInSc3d1	územní
rozmachu	rozmach	k1gInSc3	rozmach
<g/>
,	,	kIx,	,
jakkoliv	jakkoliv	k8xS	jakkoliv
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
zdát	zdát	k5eAaPmF	zdát
paradoxní	paradoxní	k2eAgFnSc1d1	paradoxní
<g/>
,	,	kIx,	,
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
vnitropolitické	vnitropolitický	k2eAgFnSc2d1	vnitropolitická
krize	krize	k1gFnSc2	krize
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
vyústit	vyústit	k5eAaPmF	vyústit
v	v	k7c6	v
období	období	k1gNnSc6	období
občanských	občanský	k2eAgFnPc2d1	občanská
válek	válka	k1gFnPc2	válka
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
v	v	k7c4	v
zánik	zánik	k1gInSc4	zánik
dosavadní	dosavadní	k2eAgFnSc2d1	dosavadní
státní	státní	k2eAgFnSc2d1	státní
formy	forma	k1gFnSc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byla	být	k5eAaImAgFnS	být
hluboká	hluboký	k2eAgFnSc1d1	hluboká
sociální	sociální	k2eAgFnSc1d1	sociální
polarizace	polarizace	k1gFnSc1	polarizace
římské	římský	k2eAgFnSc2d1	římská
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
volání	volání	k1gNnSc1	volání
po	po	k7c6	po
reformách	reforma	k1gFnPc6	reforma
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
agrární	agrární	k2eAgFnSc6d1	agrární
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důsledků	důsledek	k1gInPc2	důsledek
římských	římský	k2eAgInPc2d1	římský
vojensko-politických	vojenskoolitický	k2eAgInPc2d1	vojensko-politický
úspěchů	úspěch	k1gInPc2	úspěch
byl	být	k5eAaImAgInS	být
příliv	příliv	k1gInSc1	příliv
ohromného	ohromný	k2eAgNnSc2d1	ohromné
množství	množství	k1gNnSc2	množství
bohatství	bohatství	k1gNnSc2	bohatství
a	a	k8xC	a
otroků	otrok	k1gMnPc2	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Drobní	drobný	k2eAgMnPc1d1	drobný
rolníci	rolník	k1gMnPc1	rolník
a	a	k8xC	a
řemeslníci	řemeslník	k1gMnPc1	řemeslník
plebejského	plebejský	k2eAgInSc2d1	plebejský
původu	původ	k1gInSc2	původ
byli	být	k5eAaImAgMnP	být
nuceni	nutit	k5eAaImNgMnP	nutit
k	k	k7c3	k
časté	častý	k2eAgFnSc3d1	častá
a	a	k8xC	a
dlouhé	dlouhý	k2eAgFnSc3d1	dlouhá
nepřítomnosti	nepřítomnost	k1gFnSc3	nepřítomnost
ve	v	k7c6	v
válkách	válka	k1gFnPc6	válka
mimo	mimo	k7c4	mimo
Itálii	Itálie	k1gFnSc4	Itálie
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ztěžovalo	ztěžovat	k5eAaImAgNnS	ztěžovat
údržbu	údržba	k1gFnSc4	údržba
jejich	jejich	k3xOp3gNnSc1	jejich
hospodářství	hospodářství	k1gNnSc1	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgInSc3	ten
velcí	velký	k2eAgMnPc1d1	velký
majitelé	majitel	k1gMnPc1	majitel
půdy	půda	k1gFnSc2	půda
zvětšovali	zvětšovat	k5eAaImAgMnP	zvětšovat
svůj	svůj	k3xOyFgInSc4	svůj
majetek	majetek	k1gInSc4	majetek
koupí	koupě	k1gFnPc2	koupě
nevýdělečných	výdělečný	k2eNgFnPc2d1	nevýdělečná
usedlostí	usedlost	k1gFnPc2	usedlost
nebo	nebo	k8xC	nebo
často	často	k6eAd1	často
i	i	k9	i
násilným	násilný	k2eAgNnSc7d1	násilné
vyháněním	vyhánění	k1gNnSc7	vyhánění
jejich	jejich	k3xOp3gMnPc2	jejich
vlastníků	vlastník	k1gMnPc2	vlastník
<g/>
.	.	kIx.	.
</s>
<s>
Koncentrace	koncentrace	k1gFnSc1	koncentrace
velkostatků	velkostatek	k1gInPc2	velkostatek
(	(	kIx(	(
<g/>
latifundia	latifundium	k1gNnSc2	latifundium
<g/>
)	)	kIx)	)
a	a	k8xC	a
masivní	masivní	k2eAgInSc4d1	masivní
přechod	přechod	k1gInSc4	přechod
k	k	k7c3	k
využívání	využívání	k1gNnSc3	využívání
otrocké	otrocký	k2eAgFnSc2d1	otrocká
pracovní	pracovní	k2eAgFnSc2d1	pracovní
síly	síla	k1gFnSc2	síla
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
a	a	k8xC	a
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
stupňovaly	stupňovat	k5eAaImAgFnP	stupňovat
tlak	tlak	k1gInSc4	tlak
na	na	k7c4	na
drobné	drobný	k2eAgMnPc4d1	drobný
rolníky	rolník	k1gMnPc4	rolník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
tvořili	tvořit	k5eAaImAgMnP	tvořit
páteř	páteř	k1gFnSc4	páteř
občanského	občanský	k2eAgNnSc2d1	občanské
miličního	miliční	k2eAgNnSc2d1	miliční
vojska	vojsko	k1gNnSc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Vykořenění	vykořeněný	k2eAgMnPc1d1	vykořeněný
rolníci	rolník	k1gMnPc1	rolník
odcházeli	odcházet	k5eAaImAgMnP	odcházet
do	do	k7c2	do
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zastávali	zastávat	k5eAaImAgMnP	zastávat
příležitostné	příležitostný	k2eAgFnPc4d1	příležitostná
práce	práce	k1gFnPc4	práce
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
narůstala	narůstat	k5eAaImAgFnS	narůstat
masa	masa	k1gFnSc1	masa
nemajetných	majetný	k2eNgInPc2d1	nemajetný
<g/>
.	.	kIx.	.
</s>
<s>
Klesající	klesající	k2eAgInSc1d1	klesající
počet	počet	k1gInSc1	počet
rolníků	rolník	k1gMnPc2	rolník
vedl	vést	k5eAaImAgInS	vést
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
k	k	k7c3	k
oslabování	oslabování	k1gNnSc3	oslabování
bojeschopnosti	bojeschopnost	k1gFnSc2	bojeschopnost
římského	římský	k2eAgNnSc2d1	římské
vojska	vojsko	k1gNnSc2	vojsko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
tvořeno	tvořit	k5eAaImNgNnS	tvořit
pouze	pouze	k6eAd1	pouze
držiteli	držitel	k1gMnSc3	držitel
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Rozmach	rozmach	k1gInSc1	rozmach
Říma	Řím	k1gInSc2	Řím
tudíž	tudíž	k8xC	tudíž
přivodil	přivodit	k5eAaPmAgInS	přivodit
zánik	zánik	k1gInSc1	zánik
těch	ten	k3xDgFnPc2	ten
vrstev	vrstva	k1gFnPc2	vrstva
římské	římský	k2eAgFnSc2d1	římská
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
měly	mít	k5eAaImAgInP	mít
hlavní	hlavní	k2eAgFnSc4d1	hlavní
zásluhu	zásluha	k1gFnSc4	zásluha
<g/>
.	.	kIx.	.
</s>
<s>
Zchudnutí	zchudnutí	k1gNnSc1	zchudnutí
širokých	široký	k2eAgFnPc2d1	široká
vrstev	vrstva	k1gFnPc2	vrstva
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
vylidnění	vylidnění	k1gNnSc3	vylidnění
venkova	venkov	k1gInSc2	venkov
a	a	k8xC	a
k	k	k7c3	k
rostoucí	rostoucí	k2eAgFnSc3d1	rostoucí
nespokojenosti	nespokojenost	k1gFnSc3	nespokojenost
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzrůstající	vzrůstající	k2eAgNnSc1d1	vzrůstající
sociální	sociální	k2eAgNnSc1d1	sociální
napětí	napětí	k1gNnSc1	napětí
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
vygradovalo	vygradovat	k5eAaPmAgNnS	vygradovat
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
133	[number]	k4	133
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
tribun	tribun	k1gMnSc1	tribun
lidu	lid	k1gInSc2	lid
Tiberius	Tiberius	k1gMnSc1	Tiberius
Gracchus	Gracchus	k1gMnSc1	Gracchus
pokusil	pokusit	k5eAaPmAgMnS	pokusit
prosadit	prosadit	k5eAaPmF	prosadit
pozemkový	pozemkový	k2eAgInSc4d1	pozemkový
zákon	zákon	k1gInSc4	zákon
<g/>
,	,	kIx,	,
kterým	který	k3yIgFnPc3	který
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
velkostatky	velkostatek	k1gInPc1	velkostatek
omezeny	omezen	k2eAgInPc1d1	omezen
a	a	k8xC	a
takto	takto	k6eAd1	takto
získaná	získaný	k2eAgFnSc1d1	získaná
půda	půda	k1gFnSc1	půda
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
potřebným	potřebný	k2eAgNnSc7d1	potřebné
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
tohoto	tento	k3xDgNnSc2	tento
reformního	reformní	k2eAgNnSc2d1	reformní
úsilí	úsilí	k1gNnSc2	úsilí
byl	být	k5eAaImAgInS	být
kritický	kritický	k2eAgInSc1d1	kritický
nedostatek	nedostatek	k1gInSc1	nedostatek
rekrutů	rekrut	k1gMnPc2	rekrut
při	při	k7c6	při
odvodech	odvod	k1gInPc6	odvod
do	do	k7c2	do
legií	legie	k1gFnPc2	legie
<g/>
.	.	kIx.	.
</s>
<s>
Urputný	urputný	k2eAgInSc4d1	urputný
odpor	odpor	k1gInSc4	odpor
většiny	většina	k1gFnSc2	většina
senátorů	senátor	k1gMnPc2	senátor
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
přinutili	přinutit	k5eAaPmAgMnP	přinutit
Gracchova	Gracchův	k2eAgMnSc4d1	Gracchův
kolegu	kolega	k1gMnSc4	kolega
vetovat	vetovat	k5eAaBmF	vetovat
jeho	on	k3xPp3gInSc4	on
návrh	návrh	k1gInSc4	návrh
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Gracchus	Gracchus	k1gMnSc1	Gracchus
pokusil	pokusit	k5eAaPmAgMnS	pokusit
zrušit	zrušit	k5eAaPmF	zrušit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
svého	svůj	k3xOyFgMnSc4	svůj
kolegu	kolega	k1gMnSc4	kolega
za	za	k7c4	za
sesazeného	sesazený	k2eAgMnSc4d1	sesazený
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
však	však	k9	však
ucházel	ucházet	k5eAaImAgMnS	ucházet
o	o	k7c4	o
úřad	úřad	k1gInSc4	úřad
tribuna	tribun	k1gMnSc2	tribun
lidu	lid	k1gInSc2	lid
na	na	k7c4	na
další	další	k2eAgInSc4d1	další
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zavražděn	zavraždit	k5eAaPmNgInS	zavraždit
(	(	kIx(	(
<g/>
133	[number]	k4	133
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Gaius	Gaius	k1gMnSc1	Gaius
Gracchus	Gracchus	k1gMnSc1	Gracchus
se	se	k3xPyFc4	se
o	o	k7c4	o
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
pokusil	pokusit	k5eAaPmAgInS	pokusit
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
bratrově	bratrův	k2eAgNnSc6d1	bratrovo
reformním	reformní	k2eAgNnSc6d1	reformní
úsilí	úsilí	k1gNnSc6	úsilí
<g/>
,	,	kIx,	,
čehož	což	k3yQnSc2	což
chtěl	chtít	k5eAaImAgMnS	chtít
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
jednak	jednak	k8xC	jednak
podporou	podpora	k1gFnSc7	podpora
jezdců	jezdec	k1gMnPc2	jezdec
<g/>
,	,	kIx,	,
po	po	k7c6	po
senátorech	senátor	k1gMnPc6	senátor
druhé	druhý	k4xOgFnSc2	druhý
nejbohatší	bohatý	k2eAgFnSc2d3	nejbohatší
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
a	a	k8xC	a
jednak	jednak	k8xC	jednak
slibem	slib	k1gInSc7	slib
římského	římský	k2eAgMnSc2d1	římský
občanství	občanství	k1gNnSc4	občanství
spojencům	spojenec	k1gMnPc3	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
i	i	k9	i
Gaius	Gaius	k1gMnSc1	Gaius
zemřel	zemřít	k5eAaPmAgMnS	zemřít
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
násilnou	násilný	k2eAgFnSc7d1	násilná
smrtí	smrt	k1gFnSc7	smrt
(	(	kIx(	(
<g/>
121	[number]	k4	121
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vítězství	vítězství	k1gNnSc1	vítězství
senátní	senátní	k2eAgFnSc2d1	senátní
většiny	většina	k1gFnSc2	většina
(	(	kIx(	(
<g/>
optimates	optimates	k1gInSc1	optimates
<g/>
)	)	kIx)	)
nad	nad	k7c7	nad
lidovou	lidový	k2eAgFnSc7d1	lidová
stranou	strana	k1gFnSc7	strana
(	(	kIx(	(
<g/>
populares	populares	k1gMnSc1	populares
<g/>
)	)	kIx)	)
republice	republika	k1gFnSc6	republika
přesto	přesto	k8xC	přesto
uklidnění	uklidnění	k1gNnSc4	uklidnění
nepřineslo	přinést	k5eNaPmAgNnS	přinést
<g/>
.	.	kIx.	.
</s>
<s>
Krize	krize	k1gFnSc1	krize
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
stále	stále	k6eAd1	stále
prohlubovala	prohlubovat	k5eAaImAgFnS	prohlubovat
<g/>
.	.	kIx.	.
</s>
<s>
Pouliční	pouliční	k2eAgInPc1d1	pouliční
boje	boj	k1gInPc1	boj
a	a	k8xC	a
politické	politický	k2eAgFnPc1d1	politická
vraždy	vražda	k1gFnPc1	vražda
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
denním	denní	k2eAgInSc6d1	denní
pořádku	pořádek	k1gInSc6	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
Římem	Řím	k1gInSc7	Řím
otřásala	otřásat	k5eAaImAgFnS	otřásat
povstání	povstání	k1gNnSc4	povstání
otroků	otrok	k1gMnPc2	otrok
(	(	kIx(	(
<g/>
především	především	k9	především
Spartakovo	Spartakův	k2eAgNnSc1d1	Spartakovo
povstání	povstání	k1gNnSc1	povstání
v	v	k7c6	v
letech	let	k1gInPc6	let
73	[number]	k4	73
<g/>
–	–	k?	–
<g/>
71	[number]	k4	71
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vpád	vpád	k1gInSc4	vpád
Kimbrů	Kimbr	k1gMnPc2	Kimbr
a	a	k8xC	a
Teutonů	Teuton	k1gMnPc2	Teuton
(	(	kIx(	(
<g/>
113	[number]	k4	113
<g/>
–	–	k?	–
<g/>
101	[number]	k4	101
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
namáhavé	namáhavý	k2eAgInPc1d1	namáhavý
konflikty	konflikt	k1gInPc1	konflikt
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
říše	říš	k1gFnSc2	říš
(	(	kIx(	(
<g/>
válka	válka	k1gFnSc1	válka
s	s	k7c7	s
Jugurthou	Jugurtha	k1gFnSc7	Jugurtha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
krizi	krize	k1gFnSc3	krize
vděčil	vděčit	k5eAaImAgInS	vděčit
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
strmý	strmý	k2eAgInSc4d1	strmý
vzestup	vzestup	k1gInSc4	vzestup
Gaius	Gaius	k1gMnSc1	Gaius
Marius	Marius	k1gMnSc1	Marius
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
reformě	reforma	k1gFnSc3	reforma
vojska	vojsko	k1gNnPc4	vojsko
porazil	porazit	k5eAaPmAgMnS	porazit
nejprve	nejprve	k6eAd1	nejprve
Jugurthu	Jugurtha	k1gFnSc4	Jugurtha
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
také	také	k9	také
germánské	germánský	k2eAgMnPc4d1	germánský
Teutony	Teuton	k1gMnPc4	Teuton
a	a	k8xC	a
Kimbry	Kimbr	k1gMnPc4	Kimbr
<g/>
.	.	kIx.	.
</s>
<s>
Mariova	Mariův	k2eAgFnSc1d1	Mariova
reforma	reforma	k1gFnSc1	reforma
armády	armáda	k1gFnSc2	armáda
však	však	k9	však
byla	být	k5eAaImAgFnS	být
odlišná	odlišný	k2eAgFnSc1d1	odlišná
od	od	k7c2	od
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
prosazovali	prosazovat	k5eAaImAgMnP	prosazovat
bratři	bratr	k1gMnPc1	bratr
Gracchové	Gracchová	k1gFnSc2	Gracchová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
nyní	nyní	k6eAd1	nyní
mohli	moct	k5eAaImAgMnP	moct
sloužit	sloužit	k5eAaImF	sloužit
i	i	k9	i
bezzemci	bezzemec	k1gMnPc1	bezzemec
(	(	kIx(	(
<g/>
capite	capit	k1gMnSc5	capit
censi	cens	k1gMnSc5	cens
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
později	pozdě	k6eAd2	pozdě
jako	jako	k8xS	jako
veteráni	veterán	k1gMnPc1	veterán
odměňováni	odměňovat	k5eAaImNgMnP	odměňovat
přidělením	přidělení	k1gNnPc3	přidělení
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
mezi	mezi	k7c7	mezi
vojevůdcem	vojevůdce	k1gMnSc7	vojevůdce
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc7	jeho
vojáky	voják	k1gMnPc7	voják
úzký	úzký	k2eAgInSc4d1	úzký
vztah	vztah	k1gInSc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
okamžiku	okamžik	k1gInSc2	okamžik
už	už	k6eAd1	už
vojáci	voják	k1gMnPc1	voják
nepociťovali	pociťovat	k5eNaImAgMnP	pociťovat
loajalitu	loajalita	k1gFnSc4	loajalita
vůči	vůči	k7c3	vůči
státu	stát	k1gInSc3	stát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
vůči	vůči	k7c3	vůči
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
jim	on	k3xPp3gMnPc3	on
zajišťoval	zajišťovat	k5eAaImAgMnS	zajišťovat
půdu	půda	k1gFnSc4	půda
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
byly	být	k5eAaImAgFnP	být
vytvářeny	vytvářen	k2eAgInPc4d1	vytvářen
předpoklady	předpoklad	k1gInPc4	předpoklad
k	k	k7c3	k
pozdějšímu	pozdní	k2eAgNnSc3d2	pozdější
propuknutí	propuknutí	k1gNnSc3	propuknutí
občanských	občanský	k2eAgFnPc2d1	občanská
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
sporů	spor	k1gInPc2	spor
uvnitř	uvnitř	k7c2	uvnitř
římské	římský	k2eAgFnSc2d1	římská
společnosti	společnost	k1gFnSc2	společnost
docházelo	docházet	k5eAaImAgNnS	docházet
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
2	[number]	k4	2
<g/>
.	.	kIx.	.
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
rovněž	rovněž	k9	rovněž
ke	k	k7c3	k
konfliktům	konflikt	k1gInPc3	konflikt
s	s	k7c7	s
italskými	italský	k2eAgMnPc7d1	italský
spojenci	spojenec	k1gMnPc7	spojenec
žádajícími	žádající	k2eAgFnPc7d1	žádající
zrovnoprávnění	zrovnoprávnění	k1gNnPc1	zrovnoprávnění
s	s	k7c7	s
Římany	Říman	k1gMnPc7	Říman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
91	[number]	k4	91
<g/>
–	–	k?	–
<g/>
88	[number]	k4	88
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
probíhala	probíhat	k5eAaImAgFnS	probíhat
tzv.	tzv.	kA	tzv.
spojenecká	spojenecký	k2eAgFnSc1d1	spojenecká
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Italové	Ital	k1gMnPc1	Ital
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
byli	být	k5eAaImAgMnP	být
sice	sice	k8xC	sice
poraženi	porazit	k5eAaPmNgMnP	porazit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
většině	většina	k1gFnSc3	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
brzy	brzy	k6eAd1	brzy
uděleno	udělen	k2eAgNnSc4d1	uděleno
římské	římský	k2eAgNnSc4d1	římské
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Sotva	sotva	k6eAd1	sotva
skončila	skončit	k5eAaPmAgFnS	skončit
spojenecká	spojenecký	k2eAgFnSc1d1	spojenecká
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
hrozilo	hrozit	k5eAaImAgNnS	hrozit
Římu	Řím	k1gInSc3	Řím
nové	nový	k2eAgNnSc1d1	nové
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
88	[number]	k4	88
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
povražděno	povraždit	k5eAaPmNgNnS	povraždit
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
římských	římský	k2eAgMnPc2d1	římský
a	a	k8xC	a
italských	italský	k2eAgMnPc2d1	italský
kolonistů	kolonista	k1gMnPc2	kolonista
<g/>
.	.	kIx.	.
</s>
<s>
Původcem	původce	k1gMnSc7	původce
tohoto	tento	k3xDgInSc2	tento
činu	čin	k1gInSc2	čin
byl	být	k5eAaImAgMnS	být
pontský	pontský	k2eAgMnSc1d1	pontský
král	král	k1gMnSc1	král
Mithridatés	Mithridatésa	k1gFnPc2	Mithridatésa
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
nato	nato	k6eAd1	nato
zmocnil	zmocnit	k5eAaPmAgMnS	zmocnit
provincie	provincie	k1gFnPc4	provincie
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
vylodil	vylodit	k5eAaPmAgMnS	vylodit
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
stranu	strana	k1gFnSc4	strana
přidaly	přidat	k5eAaPmAgFnP	přidat
Athény	Athéna	k1gFnPc1	Athéna
<g/>
.	.	kIx.	.
</s>
<s>
Populárové	populár	k1gMnPc1	populár
chtěli	chtít	k5eAaImAgMnP	chtít
vedení	vedení	k1gNnSc4	vedení
války	válka	k1gFnSc2	válka
svěřit	svěřit	k5eAaPmF	svěřit
Mariovi	Mario	k1gMnSc3	Mario
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
přimělo	přimět	k5eAaPmAgNnS	přimět
vůdce	vůdce	k1gMnPc4	vůdce
optimátů	optimát	k1gMnPc2	optimát
<g/>
,	,	kIx,	,
Lucia	Lucius	k1gMnSc2	Lucius
Cornelia	Cornelius	k1gMnSc4	Cornelius
Sullu	Sulla	k1gMnSc4	Sulla
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
vyznamenal	vyznamenat	k5eAaPmAgMnS	vyznamenat
ve	v	k7c6	v
spojenecké	spojenecký	k2eAgFnSc6d1	spojenecká
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
k	k	k7c3	k
postupu	postup	k1gInSc3	postup
na	na	k7c4	na
Řím	Řím	k1gInSc4	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
rivalita	rivalita	k1gFnSc1	rivalita
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
stranami	strana	k1gFnPc7	strana
získala	získat	k5eAaPmAgFnS	získat
nyní	nyní	k6eAd1	nyní
podobu	podoba	k1gFnSc4	podoba
vojenského	vojenský	k2eAgInSc2d1	vojenský
konfliktu	konflikt	k1gInSc2	konflikt
–	–	k?	–
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Sulla	Sulla	k1gFnSc1	Sulla
po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
nad	nad	k7c7	nad
populáry	populár	k1gMnPc7	populár
vypravil	vypravit	k5eAaPmAgMnS	vypravit
proti	proti	k7c3	proti
Mithridatovi	Mithridat	k1gMnSc3	Mithridat
<g/>
,	,	kIx,	,
převzali	převzít	k5eAaPmAgMnP	převzít
moc	moc	k6eAd1	moc
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
opět	opět	k6eAd1	opět
populárové	populár	k1gMnPc1	populár
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
optimátům	optimát	k1gMnPc3	optimát
připravili	připravit	k5eAaPmAgMnP	připravit
krvavou	krvavý	k2eAgFnSc4d1	krvavá
pomstu	pomsta	k1gFnSc4	pomsta
a	a	k8xC	a
Sullu	Sulla	k1gFnSc4	Sulla
prohlásili	prohlásit	k5eAaPmAgMnP	prohlásit
za	za	k7c4	za
nepřítele	nepřítel	k1gMnSc4	nepřítel
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Sulla	Sulla	k1gMnSc1	Sulla
však	však	k9	však
nekapituloval	kapitulovat	k5eNaBmAgMnS	kapitulovat
a	a	k8xC	a
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
vítězném	vítězný	k2eAgInSc6d1	vítězný
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
východu	východ	k1gInSc2	východ
populáry	populár	k1gInPc4	populár
přemohl	přemoct	k5eAaPmAgInS	přemoct
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
diktátorem	diktátor	k1gMnSc7	diktátor
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgMnPc4	svůj
politické	politický	k2eAgMnPc4d1	politický
odpůrce	odpůrce	k1gMnPc4	odpůrce
nechal	nechat	k5eAaPmAgInS	nechat
zmasakrovat	zmasakrovat	k5eAaPmF	zmasakrovat
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
proskripcí	proskripce	k1gFnPc2	proskripce
–	–	k?	–
seznamu	seznam	k1gInSc2	seznam
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
beztrestně	beztrestně	k6eAd1	beztrestně
zabít	zabít	k5eAaPmF	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Senát	senát	k1gInSc1	senát
získal	získat	k5eAaPmAgInS	získat
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
úlohu	úloha	k1gFnSc4	úloha
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
a	a	k8xC	a
počet	počet	k1gInSc1	počet
jeho	jeho	k3xOp3gInPc2	jeho
členů	člen	k1gInPc2	člen
byl	být	k5eAaImAgInS	být
téměř	téměř	k6eAd1	téměř
zdvojnásoben	zdvojnásobit	k5eAaPmNgInS	zdvojnásobit
přijetím	přijetí	k1gNnSc7	přijetí
nejvýznačnějších	význačný	k2eAgMnPc2d3	nejvýznačnější
jezdců	jezdec	k1gMnPc2	jezdec
<g/>
.	.	kIx.	.
</s>
<s>
Sulla	Sulla	k6eAd1	Sulla
se	se	k3xPyFc4	se
ale	ale	k9	ale
své	svůj	k3xOyFgFnSc2	svůj
funkce	funkce	k1gFnSc2	funkce
záhy	záhy	k6eAd1	záhy
vzdal	vzdát	k5eAaPmAgMnS	vzdát
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
ustoupil	ustoupit	k5eAaPmAgMnS	ustoupit
z	z	k7c2	z
veřejného	veřejný	k2eAgInSc2d1	veřejný
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
uvolnil	uvolnit	k5eAaPmAgMnS	uvolnit
cestu	cesta	k1gFnSc4	cesta
dvěma	dva	k4xCgMnPc3	dva
jiným	jiný	k2eAgMnPc3d1	jiný
ambiciózním	ambiciózní	k2eAgMnPc3d1	ambiciózní
jedincům	jedinec	k1gMnPc3	jedinec
<g/>
:	:	kIx,	:
bohatému	bohatý	k2eAgMnSc3d1	bohatý
patricijovi	patricij	k1gMnSc3	patricij
Marcu	Marc	k1gMnSc3	Marc
Liciniu	Licinium	k1gNnSc3	Licinium
Crassovi	Crass	k1gMnSc3	Crass
a	a	k8xC	a
úspěšnému	úspěšný	k2eAgMnSc3d1	úspěšný
vojevůdci	vojevůdce	k1gMnSc3	vojevůdce
Gnaeu	Gnae	k1gMnSc3	Gnae
Pompeiovi	Pompeius	k1gMnSc3	Pompeius
Magnovi	Magn	k1gMnSc3	Magn
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgMnSc1	druhý
jmenovaný	jmenovaný	k1gMnSc1	jmenovaný
definitivně	definitivně	k6eAd1	definitivně
porazil	porazit	k5eAaPmAgMnS	porazit
Mithridata	Mithride	k1gNnPc4	Mithride
a	a	k8xC	a
reorganizoval	reorganizovat	k5eAaBmAgMnS	reorganizovat
poměry	poměr	k1gInPc7	poměr
na	na	k7c6	na
Východě	východ	k1gInSc6	východ
(	(	kIx(	(
<g/>
vznik	vznik	k1gInSc1	vznik
provincie	provincie	k1gFnSc2	provincie
Sýrie	Sýrie	k1gFnSc2	Sýrie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
slabá	slabý	k2eAgFnSc1d1	slabá
říše	říše	k1gFnSc1	říše
Ptolemaiovců	Ptolemaiovec	k1gMnPc2	Ptolemaiovec
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
stala	stát	k5eAaPmAgFnS	stát
římským	římský	k2eAgInSc7d1	římský
protektorátem	protektorát	k1gInSc7	protektorát
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
Cicero	Cicero	k1gMnSc1	Cicero
zdolal	zdolat	k5eAaPmAgMnS	zdolat
Catilinovo	Catilinův	k2eAgNnSc4d1	Catilinovo
spiknutí	spiknutí	k1gNnSc4	spiknutí
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Pompeius	Pompeius	k1gMnSc1	Pompeius
spojil	spojit	k5eAaPmAgMnS	spojit
s	s	k7c7	s
Crassem	Crass	k1gMnSc7	Crass
a	a	k8xC	a
ctižádostivým	ctižádostivý	k2eAgMnSc7d1	ctižádostivý
Gaiem	Gai	k1gMnSc7	Gai
Juliem	Julius	k1gMnSc7	Julius
Caesarem	Caesar	k1gMnSc7	Caesar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tito	tento	k3xDgMnPc1	tento
tři	tři	k4xCgMnPc1	tři
muži	muž	k1gMnPc1	muž
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
60	[number]	k4	60
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
neformální	formální	k2eNgNnSc4d1	neformální
spojenectví	spojenectví	k1gNnSc4	spojenectví
k	k	k7c3	k
prosazení	prosazení	k1gNnSc3	prosazení
vlastních	vlastní	k2eAgInPc2d1	vlastní
zájmů	zájem	k1gInPc2	zájem
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
první	první	k4xOgInSc4	první
triumvirát	triumvirát	k1gInSc4	triumvirát
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
se	se	k3xPyFc4	se
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
ostatních	ostatní	k2eAgMnPc2d1	ostatní
triumvirů	triumvir	k1gMnPc2	triumvir
stal	stát	k5eAaPmAgInS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
59	[number]	k4	59
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
konzulem	konzul	k1gMnSc7	konzul
a	a	k8xC	a
zabezpečil	zabezpečit	k5eAaPmAgMnS	zabezpečit
si	se	k3xPyFc3	se
velení	velení	k1gNnSc4	velení
v	v	k7c6	v
Galii	Galie	k1gFnSc6	Galie
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
ohromnou	ohromný	k2eAgFnSc4d1	ohromná
a	a	k8xC	a
lidnatou	lidnatý	k2eAgFnSc4d1	lidnatá
zemi	zem	k1gFnSc4	zem
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
v	v	k7c6	v
několika	několik	k4yIc6	několik
málo	málo	k6eAd1	málo
letech	let	k1gInPc6	let
úplně	úplně	k6eAd1	úplně
podrobil	podrobit	k5eAaPmAgMnS	podrobit
(	(	kIx(	(
<g/>
galské	galský	k2eAgFnPc1d1	galská
války	válka	k1gFnPc1	válka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
obratně	obratně	k6eAd1	obratně
využil	využít	k5eAaPmAgMnS	využít
vnitřní	vnitřní	k2eAgFnPc4d1	vnitřní
rivality	rivalita	k1gFnPc4	rivalita
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
místními	místní	k2eAgInPc7d1	místní
kmeny	kmen	k1gInPc7	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Crassus	Crassus	k1gInSc1	Crassus
padl	padnout	k5eAaPmAgInS	padnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
53	[number]	k4	53
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Karrh	Karrha	k1gFnPc2	Karrha
během	během	k7c2	během
tažení	tažení	k1gNnSc2	tažení
proti	proti	k7c3	proti
Parthům	Parth	k1gInPc3	Parth
<g/>
,	,	kIx,	,
nepřátelství	nepřátelství	k1gNnSc1	nepřátelství
mezi	mezi	k7c7	mezi
Caesarem	Caesar	k1gMnSc7	Caesar
a	a	k8xC	a
Pompeiem	Pompeius	k1gMnSc7	Pompeius
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
měl	mít	k5eAaImAgInS	mít
podporu	podpora	k1gFnSc4	podpora
senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
otevřeně	otevřeně	k6eAd1	otevřeně
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
<g/>
,	,	kIx,	,
kterému	který	k3yQgNnSc3	který
v	v	k7c6	v
případě	případ	k1gInSc6	případ
jeho	on	k3xPp3gInSc2	on
návratu	návrat	k1gInSc2	návrat
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
hrozil	hrozit	k5eAaImAgInS	hrozit
politický	politický	k2eAgInSc1d1	politický
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
překročil	překročit	k5eAaPmAgInS	překročit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
49	[number]	k4	49
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
vojáky	voják	k1gMnPc7	voják
pohraniční	pohraniční	k2eAgInSc4d1	pohraniční
řeku	řeka	k1gFnSc4	řeka
Rubikon	Rubikon	k1gInSc4	Rubikon
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
započala	započnout	k5eAaPmAgFnS	započnout
další	další	k2eAgFnPc4d1	další
fáze	fáze	k1gFnPc4	fáze
občanských	občanský	k2eAgFnPc2d1	občanská
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
48	[number]	k4	48
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
nad	nad	k7c7	nad
Pompeiem	Pompeium	k1gNnSc7	Pompeium
rozhodného	rozhodný	k2eAgNnSc2d1	rozhodné
vítězství	vítězství	k1gNnSc2	vítězství
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Farsálu	Farsál	k1gInSc2	Farsál
<g/>
.	.	kIx.	.
</s>
<s>
Pompeius	Pompeius	k1gMnSc1	Pompeius
nato	nato	k6eAd1	nato
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
ale	ale	k9	ale
na	na	k7c4	na
popud	popud	k1gInSc4	popud
egyptského	egyptský	k2eAgMnSc2d1	egyptský
krále	král	k1gMnSc2	král
zákeřně	zákeřně	k6eAd1	zákeřně
zavražděn	zavražděn	k2eAgInSc1d1	zavražděn
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
45	[number]	k4	45
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
získal	získat	k5eAaPmAgMnS	získat
Caesar	Caesar	k1gMnSc1	Caesar
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
celým	celý	k2eAgInSc7d1	celý
římským	římský	k2eAgInSc7d1	římský
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Caesarův	Caesarův	k2eAgInSc1d1	Caesarův
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
institucionální	institucionální	k2eAgNnSc4d1	institucionální
zakotvení	zakotvení	k1gNnSc4	zakotvení
své	svůj	k3xOyFgFnSc2	svůj
samovlády	samovláda	k1gFnSc2	samovláda
však	však	k9	však
neuspěl	uspět	k5eNaPmAgMnS	uspět
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
jmenování	jmenování	k1gNnSc4	jmenování
doživotním	doživotní	k2eAgMnSc7d1	doživotní
diktátorem	diktátor	k1gMnSc7	diktátor
padl	padnout	k5eAaPmAgInS	padnout
o	o	k7c6	o
březnových	březnový	k2eAgFnPc6d1	březnová
idách	idy	k1gFnPc6	idy
roku	rok	k1gInSc2	rok
44	[number]	k4	44
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
za	za	k7c2	za
oběť	oběť	k1gFnSc1	oběť
republikánskému	republikánský	k2eAgNnSc3d1	republikánské
spiknutí	spiknutí	k1gNnSc3	spiknutí
v	v	k7c6	v
senátu	senát	k1gInSc6	senát
vedenému	vedený	k2eAgNnSc3d1	vedené
Brutem	Brut	k1gMnSc7	Brut
a	a	k8xC	a
Cassiem	Cassius	k1gMnSc7	Cassius
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
vzplanula	vzplanout	k5eAaPmAgFnS	vzplanout
téměř	téměř	k6eAd1	téměř
okamžitě	okamžitě	k6eAd1	okamžitě
nanovo	nanovo	k6eAd1	nanovo
<g/>
.	.	kIx.	.
</s>
<s>
Marcus	Marcus	k1gMnSc1	Marcus
Antonius	Antonius	k1gMnSc1	Antonius
a	a	k8xC	a
Caesarův	Caesarův	k2eAgMnSc1d1	Caesarův
synovec	synovec	k1gMnSc1	synovec
a	a	k8xC	a
adoptivní	adoptivní	k2eAgMnSc1d1	adoptivní
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
Gaius	Gaius	k1gMnSc1	Gaius
Octavius	Octavius	k1gMnSc1	Octavius
(	(	kIx(	(
<g/>
Octavianus	Octavianus	k1gMnSc1	Octavianus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
43	[number]	k4	43
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
spojili	spojit	k5eAaPmAgMnP	spojit
a	a	k8xC	a
s	s	k7c7	s
Aemiliem	Aemilium	k1gNnSc7	Aemilium
Lepidem	Lepid	k1gInSc7	Lepid
utvořili	utvořit	k5eAaPmAgMnP	utvořit
druhý	druhý	k4xOgInSc4	druhý
triumvirát	triumvirát	k1gInSc4	triumvirát
namířený	namířený	k2eAgInSc4d1	namířený
proti	proti	k7c3	proti
stoupencům	stoupenec	k1gMnPc3	stoupenec
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
Caesarovým	Caesarův	k2eAgMnPc3d1	Caesarův
vrahům	vrah	k1gMnPc3	vrah
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
nad	nad	k7c7	nad
republikány	republikán	k1gMnPc7	republikán
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Filipp	Filippy	k1gInPc2	Filippy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
42	[number]	k4	42
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
si	se	k3xPyFc3	se
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
říší	říš	k1gFnSc7	říš
<g/>
.	.	kIx.	.
</s>
<s>
Octavianus	Octavianus	k1gMnSc1	Octavianus
obdržel	obdržet	k5eAaPmAgMnS	obdržet
západní	západní	k2eAgFnSc2d1	západní
provincie	provincie	k1gFnSc2	provincie
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Antonius	Antonius	k1gInSc1	Antonius
se	se	k3xPyFc4	se
odebral	odebrat	k5eAaPmAgInS	odebrat
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
sblížil	sblížit	k5eAaPmAgMnS	sblížit
s	s	k7c7	s
Caesarovou	Caesarův	k2eAgFnSc7d1	Caesarova
milenkou	milenka	k1gFnSc7	milenka
<g/>
,	,	kIx,	,
egyptskou	egyptský	k2eAgFnSc7d1	egyptská
královnou	královna	k1gFnSc7	královna
Kleopatrou	Kleopatra	k1gFnSc7	Kleopatra
<g/>
.	.	kIx.	.
</s>
<s>
Vzájemné	vzájemný	k2eAgInPc1d1	vzájemný
rozpory	rozpor	k1gInPc1	rozpor
oba	dva	k4xCgMnPc4	dva
triumviry	triumvir	k1gMnPc4	triumvir
ale	ale	k8xC	ale
brzy	brzy	k6eAd1	brzy
postavily	postavit	k5eAaPmAgFnP	postavit
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
31	[number]	k4	31
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
Octavianus	Octavianus	k1gInSc4	Octavianus
nad	nad	k7c7	nad
Marcem	Marce	k1gMnSc7	Marce
Antoniem	Antonio	k1gMnSc7	Antonio
v	v	k7c6	v
námořní	námořní	k2eAgFnSc6d1	námořní
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Actia	Actium	k1gNnSc2	Actium
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
spáchali	spáchat	k5eAaPmAgMnP	spáchat
Marcus	Marcus	k1gMnSc1	Marcus
Antonius	Antonius	k1gMnSc1	Antonius
a	a	k8xC	a
Kleopatra	Kleopatra	k1gFnSc1	Kleopatra
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
skončilo	skončit	k5eAaPmAgNnS	skončit
období	období	k1gNnSc1	období
občanských	občanský	k2eAgFnPc2d1	občanská
válek	válka	k1gFnPc2	válka
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
byla	být	k5eAaImAgFnS	být
pohřbena	pohřben	k2eAgFnSc1d1	pohřbena
i	i	k8xC	i
římská	římský	k2eAgFnSc1d1	římská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
27	[number]	k4	27
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
Octavianus	Octavianus	k1gInSc1	Octavianus
na	na	k7c4	na
oko	oko	k1gNnSc4	oko
vzdal	vzdát	k5eAaPmAgMnS	vzdát
veškerých	veškerý	k3xTgFnPc2	veškerý
svých	svůj	k3xOyFgFnPc2	svůj
mimořádných	mimořádný	k2eAgFnPc2d1	mimořádná
pravomocí	pravomoc	k1gFnPc2	pravomoc
a	a	k8xC	a
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
obnovu	obnova	k1gFnSc4	obnova
republikánské	republikánský	k2eAgFnSc2d1	republikánská
ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Císařství	císařství	k1gNnSc2	císařství
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Zrod	zrod	k1gInSc1	zrod
principátu	principát	k1gInSc2	principát
===	===	k?	===
</s>
</p>
<p>
<s>
Octavianus	Octavianus	k1gMnSc1	Octavianus
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Caesar	Caesar	k1gMnSc1	Caesar
docílil	docílit	k5eAaPmAgMnS	docílit
samovlády	samovláda	k1gFnPc4	samovláda
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Caesara	Caesar	k1gMnSc2	Caesar
se	se	k3xPyFc4	se
nepokoušel	pokoušet	k5eNaImAgMnS	pokoušet
potvrdit	potvrdit	k5eAaPmF	potvrdit
svoji	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
získáním	získání	k1gNnSc7	získání
úřadu	úřad	k1gInSc2	úřad
diktátora	diktátor	k1gMnSc2	diktátor
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
zastřel	zastřít	k5eAaPmAgInS	zastřít
své	svůj	k3xOyFgNnSc4	svůj
autokratické	autokratický	k2eAgNnSc4d1	autokratické
postavení	postavení	k1gNnSc4	postavení
samovládce	samovládce	k1gMnSc2	samovládce
do	do	k7c2	do
hávu	háv	k1gInSc2	háv
republikánských	republikánský	k2eAgFnPc2d1	republikánská
institucí	instituce	k1gFnPc2	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Čistě	čistě	k6eAd1	čistě
formálně	formálně	k6eAd1	formálně
ponechal	ponechat	k5eAaPmAgMnS	ponechat
v	v	k7c6	v
platnosti	platnost	k1gFnSc6	platnost
starou	starý	k2eAgFnSc4d1	stará
republikánskou	republikánský	k2eAgFnSc4d1	republikánská
ústavu	ústava	k1gFnSc4	ústava
a	a	k8xC	a
svoji	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
si	se	k3xPyFc3	se
zajistil	zajistit	k5eAaPmAgMnS	zajistit
převzetím	převzetí	k1gNnSc7	převzetí
výkonu	výkon	k1gInSc2	výkon
různých	různý	k2eAgInPc2d1	různý
republikánských	republikánský	k2eAgInPc2d1	republikánský
úřadů	úřad	k1gInPc2	úřad
<g/>
,	,	kIx,	,
nabytím	nabytí	k1gNnSc7	nabytí
zvláštních	zvláštní	k2eAgFnPc2d1	zvláštní
plných	plný	k2eAgFnPc2d1	plná
mocí	moc	k1gFnPc2	moc
a	a	k8xC	a
především	především	k9	především
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
kontrole	kontrola	k1gFnSc3	kontrola
klíčových	klíčový	k2eAgFnPc2d1	klíčová
provincií	provincie	k1gFnPc2	provincie
s	s	k7c7	s
početnými	početný	k2eAgFnPc7d1	početná
legiemi	legie	k1gFnPc7	legie
<g/>
.	.	kIx.	.
</s>
<s>
Jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
se	s	k7c7	s
principem	princip	k1gInSc7	princip
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
prvním	první	k4xOgMnSc7	první
občanem	občan	k1gMnSc7	občan
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
názorně	názorně	k6eAd1	názorně
tak	tak	k6eAd1	tak
demonstroval	demonstrovat	k5eAaBmAgMnS	demonstrovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c2	za
rovného	rovný	k2eAgNnSc2d1	rovné
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
senátory	senátor	k1gMnPc7	senátor
(	(	kIx(	(
<g/>
princeps	princeps	k6eAd1	princeps
inter	inter	k1gInSc1	inter
pares	paresa	k1gFnPc2	paresa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
jako	jako	k9	jako
úředníci	úředník	k1gMnPc1	úředník
<g/>
,	,	kIx,	,
soudci	soudce	k1gMnPc1	soudce
<g/>
,	,	kIx,	,
správci	správce	k1gMnPc1	správce
provincií	provincie	k1gFnPc2	provincie
a	a	k8xC	a
velitelé	velitel	k1gMnPc1	velitel
vojsk	vojsko	k1gNnPc2	vojsko
měli	mít	k5eAaImAgMnP	mít
spolupodílet	spolupodílet	k5eAaImF	spolupodílet
na	na	k7c6	na
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Octavianem	Octavian	k1gInSc7	Octavian
vytvořený	vytvořený	k2eAgInSc4d1	vytvořený
vládní	vládní	k2eAgInSc4d1	vládní
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
podstatných	podstatný	k2eAgInPc6d1	podstatný
rysech	rys	k1gInPc6	rys
lišil	lišit	k5eAaImAgInS	lišit
od	od	k7c2	od
staré	starý	k2eAgFnSc2d1	stará
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
nazývá	nazývat	k5eAaImIp3nS	nazývat
principát	principát	k1gInSc4	principát
<g/>
.	.	kIx.	.
</s>
<s>
Senát	senát	k1gInSc1	senát
mu	on	k3xPp3gMnSc3	on
kromě	kromě	k7c2	kromě
mnoha	mnoho	k4c2	mnoho
jiných	jiný	k2eAgInPc2d1	jiný
titulů	titul	k1gInPc2	titul
udělil	udělit	k5eAaPmAgInS	udělit
čestné	čestný	k2eAgNnSc4d1	čestné
jméno	jméno	k1gNnSc4	jméno
Augustus	Augustus	k1gInSc4	Augustus
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Vznešený	vznešený	k2eAgMnSc1d1	vznešený
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přesto	přesto	k8xC	přesto
i	i	k9	i
za	za	k7c2	za
principátu	principát	k1gInSc2	principát
zůstaly	zůstat	k5eAaPmAgInP	zůstat
přinejmenším	přinejmenším	k6eAd1	přinejmenším
zpočátku	zpočátku	k6eAd1	zpočátku
zachovány	zachován	k2eAgFnPc4d1	zachována
tradiční	tradiční	k2eAgFnPc4d1	tradiční
republikánské	republikánský	k2eAgFnPc4d1	republikánská
instituce	instituce	k1gFnPc4	instituce
<g/>
:	:	kIx,	:
veškeré	veškerý	k3xTgFnPc1	veškerý
magistratury	magistratura	k1gFnPc1	magistratura
<g/>
,	,	kIx,	,
senát	senát	k1gInSc1	senát
<g/>
,	,	kIx,	,
organizace	organizace	k1gFnSc1	organizace
správy	správa	k1gFnSc2	správa
provincií	provincie	k1gFnPc2	provincie
a	a	k8xC	a
náboženské	náboženský	k2eAgFnSc2d1	náboženská
funkce	funkce	k1gFnSc2	funkce
(	(	kIx(	(
<g/>
úřad	úřad	k1gInSc1	úřad
pontifika	pontifex	k1gMnSc2	pontifex
však	však	k8xC	však
zastával	zastávat	k5eAaImAgMnS	zastávat
císař	císař	k1gMnSc1	císař
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
význam	význam	k1gInSc1	význam
těchto	tento	k3xDgFnPc2	tento
ctihodných	ctihodný	k2eAgFnPc2d1	ctihodná
a	a	k8xC	a
starobylých	starobylý	k2eAgFnPc2d1	starobylá
institucí	instituce	k1gFnPc2	instituce
postupně	postupně	k6eAd1	postupně
upadal	upadat	k5eAaPmAgMnS	upadat
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodující	rozhodující	k2eAgInPc1d1	rozhodující
politické	politický	k2eAgInPc1d1	politický
orgány	orgán	k1gInPc1	orgán
začaly	začít	k5eAaPmAgInP	začít
brzy	brzy	k6eAd1	brzy
fungovat	fungovat	k5eAaImF	fungovat
jako	jako	k9	jako
pouhé	pouhý	k2eAgInPc1d1	pouhý
správní	správní	k2eAgInPc1d1	správní
úřady	úřad	k1gInPc1	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Instituce	instituce	k1gFnSc2	instituce
vytvářené	vytvářený	k2eAgFnSc2d1	vytvářená
císařem	císař	k1gMnSc7	císař
přebíraly	přebírat	k5eAaImAgFnP	přebírat
krok	krok	k1gInSc4	krok
za	za	k7c7	za
krokem	krok	k1gInSc7	krok
kompetence	kompetence	k1gFnSc2	kompetence
republikánských	republikánský	k2eAgFnPc2d1	republikánská
magistratur	magistratura	k1gFnPc2	magistratura
<g/>
.	.	kIx.	.
</s>
<s>
Jezdci	jezdec	k1gMnPc1	jezdec
a	a	k8xC	a
vzrůstající	vzrůstající	k2eAgFnSc7d1	vzrůstající
měrou	míra	k1gFnSc7wR	míra
také	také	k9	také
propuštěnci	propuštěnec	k1gMnPc1	propuštěnec
zaujímali	zaujímat	k5eAaImAgMnP	zaujímat
klíčová	klíčový	k2eAgNnPc4d1	klíčové
místa	místo	k1gNnPc4	místo
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
úřadech	úřad	k1gInPc6	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
senát	senát	k1gInSc1	senát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
císaři	císař	k1gMnPc1	císař
obsazovali	obsazovat	k5eAaImAgMnP	obsazovat
svými	svůj	k3xOyFgMnPc7	svůj
oblíbenci	oblíbenec	k1gMnPc7	oblíbenec
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
ztrácel	ztrácet	k5eAaImAgMnS	ztrácet
na	na	k7c6	na
významu	význam	k1gInSc6	význam
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
mocenskou	mocenský	k2eAgFnSc4d1	mocenská
roli	role	k1gFnSc4	role
převzala	převzít	k5eAaPmAgFnS	převzít
armáda	armáda	k1gFnSc1	armáda
a	a	k8xC	a
především	především	k9	především
pretoriáni	pretorián	k1gMnPc1	pretorián
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
sloužili	sloužit	k5eAaImAgMnP	sloužit
jako	jako	k9	jako
císařova	císařův	k2eAgFnSc1d1	císařova
tělesná	tělesný	k2eAgFnSc1d1	tělesná
stráž	stráž	k1gFnSc1	stráž
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
sociální	sociální	k2eAgFnSc1d1	sociální
struktura	struktura	k1gFnSc1	struktura
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
proměňovat	proměňovat	k5eAaImF	proměňovat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
doby	doba	k1gFnSc2	doba
Augustovy	Augustův	k2eAgFnSc2d1	Augustova
vlády	vláda	k1gFnSc2	vláda
docházelo	docházet	k5eAaImAgNnS	docházet
ke	k	k7c3	k
vzestupu	vzestup	k1gInSc3	vzestup
příslušníků	příslušník	k1gMnPc2	příslušník
nových	nový	k2eAgFnPc2d1	nová
společenských	společenský	k2eAgFnPc2d1	společenská
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
Italů	Ital	k1gMnPc2	Ital
a	a	k8xC	a
provinciálů	provinciál	k1gMnPc2	provinciál
<g/>
.	.	kIx.	.
</s>
<s>
Jezdci	jezdec	k1gMnPc1	jezdec
byli	být	k5eAaImAgMnP	být
preferováni	preferovat	k5eAaImNgMnP	preferovat
při	při	k7c6	při
výkonu	výkon	k1gInSc6	výkon
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
senátorů	senátor	k1gMnPc2	senátor
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
fluktuaci	fluktuace	k1gFnSc4	fluktuace
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
společenských	společenský	k2eAgFnPc2d1	společenská
tříd	třída	k1gFnPc2	třída
a	a	k8xC	a
odstraňovalo	odstraňovat	k5eAaImAgNnS	odstraňovat
sociální	sociální	k2eAgFnPc4d1	sociální
přehrady	přehrada	k1gFnPc4	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
měli	mít	k5eAaImAgMnP	mít
nyní	nyní	k6eAd1	nyní
navíc	navíc	k6eAd1	navíc
mnohem	mnohem	k6eAd1	mnohem
snadnější	snadný	k2eAgFnSc4d2	snazší
možnost	možnost	k1gFnSc4	možnost
nabýt	nabýt	k5eAaPmF	nabýt
římské	římský	k2eAgNnSc4d1	římské
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Legitimita	legitimita	k1gFnSc1	legitimita
Augustovy	Augustův	k2eAgFnSc2d1	Augustova
vlády	vláda	k1gFnSc2	vláda
spočívala	spočívat	k5eAaImAgFnS	spočívat
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
roli	role	k1gFnSc6	role
ochránce	ochránce	k1gMnSc2	ochránce
míru	mír	k1gInSc2	mír
před	před	k7c7	před
chaosem	chaos	k1gInSc7	chaos
občanských	občanský	k2eAgFnPc2d1	občanská
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Imperium	Imperium	k1gNnSc1	Imperium
Romanum	Romanum	k?	Romanum
ovládalo	ovládat	k5eAaImAgNnS	ovládat
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
celou	celý	k2eAgFnSc4d1	celá
oblast	oblast	k1gFnSc4	oblast
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
římská	římský	k2eAgFnSc1d1	římská
expanze	expanze	k1gFnSc1	expanze
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
i	i	k9	i
za	za	k7c4	za
Augusta	August	k1gMnSc4	August
<g/>
:	:	kIx,	:
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
až	až	k9	až
k	k	k7c3	k
Dunaji	Dunaj	k1gInSc3	Dunaj
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
Hispánii	Hispánie	k1gFnSc6	Hispánie
(	(	kIx(	(
<g/>
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
dobytí	dobytí	k1gNnSc1	dobytí
bylo	být	k5eAaImAgNnS	být
tehdy	tehdy	k6eAd1	tehdy
završeno	završen	k2eAgNnSc1d1	završeno
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Germánii	Germánie	k1gFnSc6	Germánie
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
zde	zde	k6eAd1	zde
plány	plán	k1gInPc4	plán
na	na	k7c4	na
zřízení	zřízení	k1gNnSc4	zřízení
provincie	provincie	k1gFnSc2	provincie
ztroskotaly	ztroskotat	k5eAaPmAgInP	ztroskotat
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
byla	být	k5eAaImAgFnS	být
římská	římský	k2eAgFnSc1d1	římská
armáda	armáda	k1gFnSc1	armáda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
9	[number]	k4	9
n.	n.	k?	n.
l.	l.	k?	l.
poražena	poražen	k2eAgFnSc1d1	poražena
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
v	v	k7c6	v
Teutoburském	Teutoburský	k2eAgInSc6d1	Teutoburský
lese	les	k1gInSc6	les
Cherusky	Cherusky	k1gFnSc2	Cherusky
vedenými	vedený	k2eAgFnPc7d1	vedená
náčelníkem	náčelník	k1gInSc7	náčelník
Arminiem	Arminium	k1gNnSc7	Arminium
<g/>
.	.	kIx.	.
</s>
<s>
Augustus	Augustus	k1gMnSc1	Augustus
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
omezil	omezit	k5eAaPmAgInS	omezit
na	na	k7c4	na
zabezpečení	zabezpečení	k1gNnSc4	zabezpečení
stávajících	stávající	k2eAgFnPc2d1	stávající
hranic	hranice	k1gFnPc2	hranice
<g/>
,	,	kIx,	,
podél	podél	k7c2	podél
kterých	který	k3yQgInPc2	který
bylo	být	k5eAaImAgNnS	být
rozmístěno	rozmístěn	k2eAgNnSc1d1	rozmístěno
vojsko	vojsko	k1gNnSc1	vojsko
čítající	čítající	k2eAgNnSc1d1	čítající
celkem	celkem	k6eAd1	celkem
asi	asi	k9	asi
300	[number]	k4	300
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Říše	říše	k1gFnSc1	říše
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
přecházela	přecházet	k5eAaImAgFnS	přecházet
do	do	k7c2	do
obrany	obrana	k1gFnSc2	obrana
a	a	k8xC	a
soustřeďovala	soustřeďovat	k5eAaImAgFnS	soustřeďovat
svůj	svůj	k3xOyFgInSc4	svůj
vojenský	vojenský	k2eAgInSc4d1	vojenský
potenciál	potenciál	k1gInSc4	potenciál
k	k	k7c3	k
opevnění	opevnění	k1gNnSc3	opevnění
hranic	hranice	k1gFnPc2	hranice
(	(	kIx(	(
<g/>
Limes	Limes	k1gMnSc1	Limes
Romanus	Romanus	k1gMnSc1	Romanus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
císařova	císařův	k2eAgNnPc1d1	císařovo
opatření	opatření	k1gNnPc1	opatření
přispěla	přispět	k5eAaPmAgNnP	přispět
k	k	k7c3	k
upevnění	upevnění	k1gNnSc3	upevnění
tzv.	tzv.	kA	tzv.
římského	římský	k2eAgInSc2d1	římský
míru	mír	k1gInSc2	mír
–	–	k?	–
Pax	Pax	k1gMnSc2	Pax
Romana	Roman	k1gMnSc2	Roman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Augustova	Augustův	k2eAgFnSc1d1	Augustova
vláda	vláda	k1gFnSc1	vláda
přinesla	přinést	k5eAaPmAgFnS	přinést
státu	stát	k1gInSc3	stát
prospěch	prospěch	k1gInSc4	prospěch
a	a	k8xC	a
blahobyt	blahobyt	k1gInSc4	blahobyt
<g/>
.	.	kIx.	.
</s>
<s>
Nastolením	nastolení	k1gNnSc7	nastolení
principátu	principát	k1gInSc2	principát
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
epocha	epocha	k1gFnSc1	epocha
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
i	i	k8xC	i
vnější	vnější	k2eAgFnSc1d1	vnější
konsolidace	konsolidace	k1gFnSc1	konsolidace
<g/>
:	:	kIx,	:
císař	císař	k1gMnSc1	císař
nechal	nechat	k5eAaPmAgMnS	nechat
vybudovat	vybudovat	k5eAaPmF	vybudovat
síť	síť	k1gFnSc4	síť
nových	nový	k2eAgFnPc2d1	nová
silnic	silnice	k1gFnPc2	silnice
a	a	k8xC	a
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářství	hospodářství	k1gNnSc1	hospodářství
a	a	k8xC	a
kultura	kultura	k1gFnSc1	kultura
zažily	zažít	k5eAaPmAgFnP	zažít
po	po	k7c6	po
letech	léto	k1gNnPc6	léto
válek	válka	k1gFnPc2	válka
nový	nový	k2eAgInSc4d1	nový
rozkvět	rozkvět	k1gInSc4	rozkvět
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
podporována	podporován	k2eAgFnSc1d1	podporována
urbanizace	urbanizace	k1gFnSc1	urbanizace
provincií	provincie	k1gFnPc2	provincie
zřizováním	zřizování	k1gNnSc7	zřizování
kolonií	kolonie	k1gFnPc2	kolonie
veteránů	veterán	k1gMnPc2	veterán
a	a	k8xC	a
velkorysým	velkorysý	k2eAgNnSc7d1	velkorysé
udělováním	udělování	k1gNnSc7	udělování
římského	římský	k2eAgNnSc2d1	římské
občanství	občanství	k1gNnSc2	občanství
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
napomohlo	napomoct	k5eAaPmAgNnS	napomoct
rychlému	rychlý	k2eAgInSc3d1	rychlý
rozšiřovaní	rozšiřovaný	k2eAgMnPc1d1	rozšiřovaný
řecko-římské	řecko-římský	k2eAgFnSc2d1	řecko-římská
kultury	kultura	k1gFnSc2	kultura
do	do	k7c2	do
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
všem	všecek	k3xTgInPc3	všecek
opatřením	opatření	k1gNnSc7	opatření
k	k	k7c3	k
uchování	uchování	k1gNnSc3	uchování
tradičních	tradiční	k2eAgFnPc2d1	tradiční
římských	římský	k2eAgFnPc2d1	římská
institucí	instituce	k1gFnPc2	instituce
započal	započnout	k5eAaPmAgInS	započnout
již	již	k6eAd1	již
za	za	k7c2	za
Augustovy	Augustův	k2eAgFnSc2d1	Augustova
vlády	vláda	k1gFnSc2	vláda
pozvolný	pozvolný	k2eAgInSc1d1	pozvolný
proces	proces	k1gInSc1	proces
provincionalizace	provincionalizace	k1gFnSc2	provincionalizace
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
postupně	postupně	k6eAd1	postupně
klesal	klesat	k5eAaImAgMnS	klesat
význam	význam	k1gInSc4	význam
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
Itálie	Itálie	k1gFnSc2	Itálie
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
ostatních	ostatní	k2eAgInPc2d1	ostatní
leckdy	leckdy	k6eAd1	leckdy
teprve	teprve	k6eAd1	teprve
nedávno	nedávno	k6eAd1	nedávno
dobytých	dobytý	k2eAgFnPc2d1	dobytá
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Možnost	možnost	k1gFnSc1	možnost
vzestupu	vzestup	k1gInSc2	vzestup
provinciálů	provinciál	k1gMnPc2	provinciál
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
zvyšovala	zvyšovat	k5eAaImAgFnS	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
za	za	k7c4	za
císaře	císař	k1gMnSc4	císař
Claudia	Claudia	k1gFnSc1	Claudia
se	se	k3xPyFc4	se
někteří	některý	k3yIgMnPc1	některý
stali	stát	k5eAaPmAgMnP	stát
senátory	senátor	k1gMnPc4	senátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Augustově	Augustův	k2eAgFnSc6d1	Augustova
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
14	[number]	k4	14
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
jeho	jeho	k3xOp3gFnSc4	jeho
adoptivní	adoptivní	k2eAgMnSc1d1	adoptivní
syn	syn	k1gMnSc1	syn
Tiberius	Tiberius	k1gMnSc1	Tiberius
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
lidsky	lidsky	k6eAd1	lidsky
komplikovanou	komplikovaný	k2eAgFnSc7d1	komplikovaná
osobností	osobnost	k1gFnSc7	osobnost
a	a	k8xC	a
vnitřně	vnitřně	k6eAd1	vnitřně
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
cítil	cítit	k5eAaImAgMnS	cítit
spíše	spíše	k9	spíše
republikánem	republikán	k1gMnSc7	republikán
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
staral	starat	k5eAaImAgInS	starat
hlavně	hlavně	k9	hlavně
o	o	k7c6	o
zabezpečení	zabezpečení	k1gNnSc6	zabezpečení
hranic	hranice	k1gFnPc2	hranice
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
naplnění	naplnění	k1gNnSc2	naplnění
státní	státní	k2eAgFnSc2d1	státní
pokladny	pokladna	k1gFnSc2	pokladna
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
událostí	událost	k1gFnSc7	událost
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Tiberiova	Tiberiův	k2eAgNnSc2d1	Tiberiovo
panování	panování	k1gNnSc2	panování
bylo	být	k5eAaImAgNnS	být
ukřižování	ukřižování	k1gNnSc1	ukřižování
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Judea	Judea	k1gFnSc1	Judea
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
třetí	třetí	k4xOgInSc4	třetí
princeps	princeps	k1gInSc4	princeps
<g/>
,	,	kIx,	,
Caligula	Caligulum	k1gNnPc4	Caligulum
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ukázal	ukázat	k5eAaPmAgInS	ukázat
být	být	k5eAaImF	být
tyranem	tyran	k1gMnSc7	tyran
par	para	k1gFnPc2	para
excellance	excellance	k1gFnPc4	excellance
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
vláda	vláda	k1gFnSc1	vláda
skončila	skončit	k5eAaPmAgFnS	skončit
po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
pretoriány	pretorián	k1gMnPc7	pretorián
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
Claudia	Claudia	k1gFnSc1	Claudia
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
říši	říš	k1gFnSc3	říš
připojena	připojen	k2eAgFnSc1d1	připojena
Británie	Británie	k1gFnSc1	Británie
později	pozdě	k6eAd2	pozdě
následovaná	následovaný	k2eAgFnSc1d1	následovaná
Thrákií	Thrákie	k1gFnSc7	Thrákie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
status	status	k1gInSc4	status
klientského	klientský	k2eAgInSc2d1	klientský
státu	stát	k1gInSc2	stát
závislého	závislý	k2eAgInSc2d1	závislý
na	na	k7c6	na
Římu	Řím	k1gInSc3	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Claudiovi	Claudius	k1gMnSc6	Claudius
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
otráven	otráven	k2eAgMnSc1d1	otráven
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
Agrippinou	Agrippina	k1gFnSc7	Agrippina
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
vládu	vláda	k1gFnSc4	vláda
císařovnin	císařovnin	k2eAgMnSc1d1	císařovnin
syn	syn	k1gMnSc1	syn
Nero	Nero	k1gMnSc1	Nero
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
zpočátku	zpočátku	k6eAd1	zpočátku
nadějné	nadějný	k2eAgNnSc1d1	nadějné
panování	panování	k1gNnSc1	panování
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
změnilo	změnit	k5eAaPmAgNnS	změnit
v	v	k7c4	v
teror	teror	k1gInSc4	teror
<g/>
,	,	kIx,	,
během	během	k7c2	během
něhož	jenž	k3xRgInSc2	jenž
nechal	nechat	k5eAaPmAgMnS	nechat
císař	císař	k1gMnSc1	císař
povraždit	povraždit	k5eAaPmF	povraždit
mnoho	mnoho	k4c1	mnoho
členů	člen	k1gInPc2	člen
své	svůj	k3xOyFgFnSc2	svůj
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
spekuluje	spekulovat	k5eAaImIp3nS	spekulovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
dal	dát	k5eAaPmAgInS	dát
zapálit	zapálit	k5eAaPmF	zapálit
Řím	Řím	k1gInSc1	Řím
v	v	k7c6	v
roce	rok	k1gInSc6	rok
64	[number]	k4	64
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
došlo	dojít	k5eAaPmAgNnS	dojít
rovněž	rovněž	k9	rovněž
k	k	k7c3	k
prvnímu	první	k4xOgNnSc3	první
pronásledování	pronásledování	k1gNnSc3	pronásledování
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
proti	proti	k7c3	proti
jeho	jeho	k3xOp3gFnSc3	jeho
hrůzovládě	hrůzovláda	k1gFnSc3	hrůzovláda
vzbouřily	vzbouřit	k5eAaPmAgFnP	vzbouřit
legie	legie	k1gFnPc4	legie
<g/>
,	,	kIx,	,
spáchal	spáchat	k5eAaPmAgMnS	spáchat
Nero	Nero	k1gMnSc1	Nero
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Neronova	Neronův	k2eAgFnSc1d1	Neronova
smrt	smrt	k1gFnSc1	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
68	[number]	k4	68
znamenala	znamenat	k5eAaImAgFnS	znamenat
konec	konec	k1gInSc4	konec
vlády	vláda	k1gFnSc2	vláda
julsko-klaudijské	julskolaudijský	k2eAgFnSc2d1	julsko-klaudijský
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
datum	datum	k1gNnSc1	datum
také	také	k9	také
představuje	představovat	k5eAaImIp3nS	představovat
určitý	určitý	k2eAgInSc4d1	určitý
zlom	zlom	k1gInSc4	zlom
v	v	k7c6	v
římských	římský	k2eAgFnPc6d1	římská
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
okamžiku	okamžik	k1gInSc2	okamžik
totiž	totiž	k9	totiž
jen	jen	k9	jen
málokterý	málokterý	k3yIgMnSc1	málokterý
císař	císař	k1gMnSc1	císař
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
staré	starý	k2eAgFnSc2d1	stará
římské	římský	k2eAgFnSc2d1	římská
aristokracie	aristokracie	k1gFnSc2	aristokracie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vrchol	vrcholit	k5eAaImRp2nS	vrcholit
císařství	císařství	k1gNnSc4	císařství
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Neronově	Neronův	k2eAgFnSc6d1	Neronova
smrti	smrt	k1gFnSc6	smrt
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
chaos	chaos	k1gInSc1	chaos
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
rok	rok	k1gInSc4	rok
čtyř	čtyři	k4xCgMnPc2	čtyři
císařů	císař	k1gMnPc2	císař
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
bojů	boj	k1gInPc2	boj
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgMnPc7d1	jednotlivý
uchazeči	uchazeč	k1gMnPc7	uchazeč
o	o	k7c4	o
trůn	trůn	k1gInSc4	trůn
vyšel	vyjít	k5eAaPmAgMnS	vyjít
vítězně	vítězně	k6eAd1	vítězně
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
Vespasianus	Vespasianus	k1gMnSc1	Vespasianus
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
založil	založit	k5eAaPmAgMnS	založit
druhou	druhý	k4xOgFnSc4	druhý
císařskou	císařský	k2eAgFnSc4d1	císařská
dynastii	dynastie	k1gFnSc4	dynastie
Flaviovců	Flaviovec	k1gMnPc2	Flaviovec
<g/>
.	.	kIx.	.
</s>
<s>
Vespasianus	Vespasianus	k1gInSc1	Vespasianus
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
uspořádat	uspořádat	k5eAaPmF	uspořádat
rozvrácené	rozvrácený	k2eAgFnPc4d1	rozvrácená
státní	státní	k2eAgFnPc4d1	státní
finance	finance	k1gFnPc4	finance
a	a	k8xC	a
zajistit	zajistit	k5eAaPmF	zajistit
hranice	hranice	k1gFnPc4	hranice
říše	říš	k1gFnSc2	říš
na	na	k7c6	na
východě	východ	k1gInSc6	východ
před	před	k7c7	před
pronikáním	pronikání	k1gNnSc7	pronikání
Parthů	Parth	k1gInPc2	Parth
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
po	po	k7c6	po
vesměs	vesměs	k6eAd1	vesměs
úspěšné	úspěšný	k2eAgFnSc6d1	úspěšná
vládě	vláda	k1gFnSc6	vláda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
79	[number]	k4	79
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
císařem	císař	k1gMnSc7	císař
jeho	jeho	k3xOp3gMnSc7	jeho
syn	syn	k1gMnSc1	syn
Titus	Titus	k1gMnSc1	Titus
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
70	[number]	k4	70
převzal	převzít	k5eAaPmAgMnS	převzít
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
otci	otec	k1gMnSc3	otec
velení	velení	k1gNnSc2	velení
v	v	k7c6	v
židovské	židovský	k2eAgFnSc6d1	židovská
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
<g/>
.	.	kIx.	.
</s>
<s>
Titovi	Titův	k2eAgMnPc1d1	Titův
bylo	být	k5eAaImAgNnS	být
dopřáno	dopřát	k5eAaPmNgNnS	dopřát
jen	jen	k6eAd1	jen
krátké	krátký	k2eAgNnSc1d1	krátké
vládnutí	vládnutí	k1gNnSc1	vládnutí
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
vyznačovalo	vyznačovat	k5eAaImAgNnS	vyznačovat
převážně	převážně	k6eAd1	převážně
katastrofami	katastrofa	k1gFnPc7	katastrofa
(	(	kIx(	(
<g/>
výbuch	výbuch	k1gInSc1	výbuch
Vesuvu	Vesuv	k1gInSc2	Vesuv
<g/>
,	,	kIx,	,
morová	morový	k2eAgFnSc1d1	morová
epidemie	epidemie	k1gFnSc1	epidemie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
v	v	k7c6	v
roce	rok	k1gInSc6	rok
80	[number]	k4	80
dokončen	dokončen	k2eAgInSc4d1	dokončen
velkolepý	velkolepý	k2eAgInSc4d1	velkolepý
Flaviovský	Flaviovský	k2eAgInSc4d1	Flaviovský
amfiteátr	amfiteátr	k1gInSc4	amfiteátr
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Titově	Titův	k2eAgInSc6d1	Titův
předčasném	předčasný	k2eAgInSc6d1	předčasný
skonu	skon	k1gInSc6	skon
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Domitianus	Domitianus	k1gMnSc1	Domitianus
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
vláda	vláda	k1gFnSc1	vláda
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
pramenech	pramen	k1gInPc6	pramen
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Tacitem	Tacit	k1gInSc7	Tacit
<g/>
,	,	kIx,	,
vylíčena	vylíčen	k2eAgFnSc1d1	vylíčena
v	v	k7c6	v
temných	temný	k2eAgFnPc6d1	temná
barvách	barva	k1gFnPc6	barva
<g/>
.	.	kIx.	.
</s>
<s>
Domitianus	Domitianus	k1gMnSc1	Domitianus
požadoval	požadovat	k5eAaImAgMnS	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
prokazována	prokazován	k2eAgFnSc1d1	prokazována
božská	božský	k2eAgFnSc1d1	božská
úcta	úcta	k1gFnSc1	úcta
a	a	k8xC	a
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
se	se	k3xPyFc4	se
dominem	domino	k1gNnSc7	domino
(	(	kIx(	(
<g/>
pánem	pán	k1gMnSc7	pán
nad	nad	k7c4	nad
otroky	otrok	k1gMnPc4	otrok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
stylem	styl	k1gInSc7	styl
vlády	vláda	k1gFnSc2	vláda
provokoval	provokovat	k5eAaImAgMnS	provokovat
staré	starý	k2eAgFnPc4d1	stará
elity	elita	k1gFnPc4	elita
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
uznat	uznat	k5eAaPmF	uznat
i	i	k9	i
jeho	jeho	k3xOp3gInPc4	jeho
nepochybné	pochybný	k2eNgInPc4d1	nepochybný
úspěchy	úspěch	k1gInPc4	úspěch
ve	v	k7c6	v
válkách	válka	k1gFnPc6	válka
proti	proti	k7c3	proti
Britanům	Britan	k1gInPc3	Britan
<g/>
,	,	kIx,	,
Germánům	Germán	k1gMnPc3	Germán
a	a	k8xC	a
Dákům	Dák	k1gMnPc3	Dák
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
velmi	velmi	k6eAd1	velmi
efektivní	efektivní	k2eAgInSc1d1	efektivní
správní	správní	k2eAgInSc1d1	správní
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
96	[number]	k4	96
však	však	k9	však
padl	padnout	k5eAaPmAgInS	padnout
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
palácovému	palácový	k2eAgInSc3d1	palácový
převratu	převrat	k1gInSc3	převrat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Následující	následující	k2eAgNnSc1d1	následující
období	období	k1gNnSc1	období
adoptivních	adoptivní	k2eAgMnPc2d1	adoptivní
císařů	císař	k1gMnPc2	císař
je	být	k5eAaImIp3nS	být
všeobecně	všeobecně	k6eAd1	všeobecně
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
vrcholné	vrcholný	k2eAgNnSc4d1	vrcholné
období	období	k1gNnSc4	období
impéria	impérium	k1gNnSc2	impérium
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
kulturního	kulturní	k2eAgNnSc2d1	kulturní
<g/>
,	,	kIx,	,
hospodářského	hospodářský	k2eAgNnSc2d1	hospodářské
i	i	k8xC	i
mocenského	mocenský	k2eAgNnSc2d1	mocenské
<g/>
.	.	kIx.	.
</s>
<s>
Císaři	císař	k1gMnPc1	císař
vládli	vládnout	k5eAaImAgMnP	vládnout
ve	v	k7c6	v
shodě	shoda	k1gFnSc6	shoda
se	s	k7c7	s
senátem	senát	k1gInSc7	senát
a	a	k8xC	a
ten	ten	k3xDgInSc4	ten
na	na	k7c4	na
oplátku	oplátka	k1gFnSc4	oplátka
respektoval	respektovat	k5eAaImAgMnS	respektovat
státní	státní	k2eAgNnSc4d1	státní
uspořádání	uspořádání	k1gNnSc4	uspořádání
principátu	principát	k1gInSc2	principát
<g/>
.	.	kIx.	.
</s>
<s>
Senátem	senát	k1gInSc7	senát
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
dosazený	dosazený	k2eAgMnSc1d1	dosazený
císař	císař	k1gMnSc1	císař
Nerva	rvát	k5eNaImSgInS	rvát
adoptoval	adoptovat	k5eAaPmAgInS	adoptovat
vojevůdce	vojevůdce	k1gMnSc4	vojevůdce
Traiana	Traian	k1gMnSc4	Traian
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
jej	on	k3xPp3gMnSc4	on
učinil	učinit	k5eAaPmAgMnS	učinit
svým	svůj	k3xOyFgMnSc7	svůj
nástupcem	nástupce	k1gMnSc7	nástupce
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
je	být	k5eAaImIp3nS	být
oslavován	oslavován	k2eAgInSc1d1	oslavován
jako	jako	k8xC	jako
optimus	optimus	k1gInSc1	optimus
princeps	princepsa	k1gFnPc2	princepsa
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
císař	císař	k1gMnSc1	císař
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
římská	římský	k2eAgFnSc1d1	římská
říše	říše	k1gFnSc1	říše
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
117	[number]	k4	117
svého	svůj	k3xOyFgInSc2	svůj
největšího	veliký	k2eAgInSc2d3	veliký
územního	územní	k2eAgInSc2d1	územní
rozsahu	rozsah	k1gInSc2	rozsah
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vítězných	vítězný	k2eAgFnPc6d1	vítězná
válkách	válka	k1gFnPc6	válka
s	s	k7c7	s
Dáky	Dák	k1gMnPc7	Dák
a	a	k8xC	a
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
východním	východní	k2eAgNnSc6d1	východní
tažení	tažení	k1gNnSc6	tažení
proti	proti	k7c3	proti
Parthům	Parth	k1gInPc3	Parth
sahala	sahat	k5eAaImAgFnS	sahat
římská	římský	k2eAgFnSc1d1	římská
moc	moc	k1gFnSc1	moc
od	od	k7c2	od
Skotska	Skotsko	k1gNnSc2	Skotsko
až	až	k9	až
po	po	k7c6	po
Núbii	Núbie	k1gFnSc6	Núbie
v	v	k7c6	v
severojižním	severojižní	k2eAgInSc6d1	severojižní
směru	směr	k1gInSc6	směr
a	a	k8xC	a
od	od	k7c2	od
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
na	na	k7c6	na
západě	západ	k1gInSc6	západ
až	až	k9	až
k	k	k7c3	k
Mezopotámii	Mezopotámie	k1gFnSc3	Mezopotámie
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Traianův	Traianův	k2eAgMnSc1d1	Traianův
nástupce	nástupce	k1gMnSc1	nástupce
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
se	se	k3xPyFc4	se
však	však	k9	však
zisků	zisk	k1gInPc2	zisk
z	z	k7c2	z
výbojů	výboj	k1gInPc2	výboj
východně	východně	k6eAd1	východně
od	od	k7c2	od
Eufratu	Eufrat	k1gInSc2	Eufrat
prozíravě	prozíravě	k6eAd1	prozíravě
vzdal	vzdát	k5eAaPmAgMnS	vzdát
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
byly	být	k5eAaImAgInP	být
neudržitelné	udržitelný	k2eNgInPc1d1	neudržitelný
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
tohoto	tento	k3xDgMnSc2	tento
vzdělaného	vzdělaný	k2eAgMnSc2d1	vzdělaný
helénofila	helénofil	k1gMnSc2	helénofil
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
vnitřní	vnitřní	k2eAgFnSc3d1	vnitřní
konsolidaci	konsolidace	k1gFnSc3	konsolidace
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
k	k	k7c3	k
civilizačnímu	civilizační	k2eAgNnSc3d1	civilizační
<g/>
,	,	kIx,	,
kulturnímu	kulturní	k2eAgNnSc3d1	kulturní
a	a	k8xC	a
technickému	technický	k2eAgInSc3d1	technický
rozkvětu	rozkvět	k1gInSc3	rozkvět
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
napomohl	napomoct	k5eAaPmAgMnS	napomoct
rozšíření	rozšíření	k1gNnSc4	rozšíření
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k9	ještě
mladého	mladý	k2eAgMnSc2d1	mladý
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
již	již	k9	již
značně	značně	k6eAd1	značně
rostoucího	rostoucí	k2eAgNnSc2d1	rostoucí
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
především	především	k9	především
výstavbě	výstavba	k1gFnSc3	výstavba
efektivního	efektivní	k2eAgNnSc2d1	efektivní
pohraničního	pohraniční	k2eAgNnSc2d1	pohraniční
opevnění	opevnění	k1gNnSc2	opevnění
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Hadriánův	Hadriánův	k2eAgInSc1d1	Hadriánův
val	val	k1gInSc1	val
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
moderní	moderní	k2eAgMnPc1d1	moderní
historikové	historik	k1gMnPc1	historik
mu	on	k3xPp3gMnSc3	on
vytýkají	vytýkat	k5eAaImIp3nP	vytýkat
silné	silný	k2eAgNnSc1d1	silné
zadlužení	zadlužení	k1gNnSc4	zadlužení
státních	státní	k2eAgFnPc2d1	státní
financí	finance	k1gFnPc2	finance
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
začaly	začít	k5eAaPmAgFnP	začít
projevovat	projevovat	k5eAaImF	projevovat
první	první	k4xOgInPc1	první
příznaky	příznak	k1gInPc1	příznak
hospodářských	hospodářský	k2eAgFnPc2d1	hospodářská
potíží	potíž	k1gFnPc2	potíž
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
si	se	k3xPyFc3	se
však	však	k9	však
zatím	zatím	k6eAd1	zatím
nevyžadovaly	vyžadovat	k5eNaImAgFnP	vyžadovat
přijetí	přijetí	k1gNnSc4	přijetí
zvláštních	zvláštní	k2eAgNnPc2d1	zvláštní
opatření	opatření	k1gNnPc2	opatření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
za	za	k7c4	za
císaře	císař	k1gMnSc4	císař
Antonina	Antonin	k2eAgMnSc2d1	Antonin
Pia	Pius	k1gMnSc2	Pius
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
impérium	impérium	k1gNnSc1	impérium
vrcholu	vrchol	k1gInSc2	vrchol
svého	svůj	k3xOyFgInSc2	svůj
blahobytu	blahobyt	k1gInSc2	blahobyt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
"	"	kIx"	"
<g/>
filozofa	filozof	k1gMnSc2	filozof
na	na	k7c6	na
trůně	trůn	k1gInSc6	trůn
<g/>
"	"	kIx"	"
Marca	Marc	k2eAgFnSc1d1	Marca
Aurelia	Aurelia	k1gFnSc1	Aurelia
(	(	kIx(	(
<g/>
161	[number]	k4	161
<g/>
–	–	k?	–
<g/>
180	[number]	k4	180
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyskytly	vyskytnout	k5eAaPmAgInP	vyskytnout
první	první	k4xOgInPc1	první
závažné	závažný	k2eAgInPc1d1	závažný
problémy	problém	k1gInPc1	problém
<g/>
.	.	kIx.	.
</s>
<s>
Ohromná	ohromný	k2eAgFnSc1d1	ohromná
masa	masa	k1gFnSc1	masa
germánských	germánský	k2eAgInPc2d1	germánský
a	a	k8xC	a
sarmatských	sarmatský	k2eAgInPc2d1	sarmatský
kmenů	kmen	k1gInPc2	kmen
překročila	překročit	k5eAaPmAgNnP	překročit
takřka	takřka	k6eAd1	takřka
podél	podél	k7c2	podél
celého	celý	k2eAgInSc2d1	celý
Dunaje	Dunaj	k1gInSc2	Dunaj
hranice	hranice	k1gFnSc1	hranice
říše	říše	k1gFnSc1	říše
a	a	k8xC	a
ohrožovala	ohrožovat	k5eAaImAgFnS	ohrožovat
i	i	k9	i
samotnou	samotný	k2eAgFnSc4d1	samotná
Itálii	Itálie	k1gFnSc4	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Markomanů	Markoman	k1gMnPc2	Markoman
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc1	tento
boje	boj	k1gInPc1	boj
nazývány	nazýván	k2eAgInPc1d1	nazýván
markomanskými	markomanský	k2eAgFnPc7d1	markomanská
válkami	válka	k1gFnPc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
Parthové	Parth	k1gMnPc1	Parth
napadli	napadnout	k5eAaPmAgMnP	napadnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
161	[number]	k4	161
východní	východní	k2eAgFnSc2d1	východní
provincie	provincie	k1gFnSc2	provincie
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Vítězná	vítězný	k2eAgNnPc1d1	vítězné
římská	římský	k2eAgNnPc1d1	římské
vojska	vojsko	k1gNnPc1	vojsko
zavlekla	zavleknout	k5eAaPmAgNnP	zavleknout
z	z	k7c2	z
východu	východ	k1gInSc2	východ
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
mor	mor	k1gInSc1	mor
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
"	"	kIx"	"
<g/>
antoniniánská	antoniniánský	k2eAgFnSc1d1	antoniniánský
epidemie	epidemie	k1gFnSc1	epidemie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
těchto	tento	k3xDgNnPc2	tento
vážných	vážný	k2eAgNnPc2d1	vážné
vnějších	vnější	k2eAgNnPc2d1	vnější
ohrožení	ohrožení	k1gNnPc2	ohrožení
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
si	se	k3xPyFc3	se
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
vynaložení	vynaložení	k1gNnSc4	vynaložení
četných	četný	k2eAgInPc2d1	četný
zdrojů	zdroj	k1gInPc2	zdroj
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
také	také	k9	také
první	první	k4xOgFnPc1	první
známky	známka	k1gFnPc1	známka
rozkladu	rozklad	k1gInSc2	rozklad
uvnitř	uvnitř	k7c2	uvnitř
římského	římský	k2eAgInSc2d1	římský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgFnPc1d1	státní
finance	finance	k1gFnPc1	finance
se	se	k3xPyFc4	se
ocitly	ocitnout	k5eAaPmAgFnP	ocitnout
v	v	k7c6	v
nerovnováze	nerovnováha	k1gFnSc6	nerovnováha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
napravena	napravit	k5eAaPmNgFnS	napravit
jen	jen	k6eAd1	jen
zhoršením	zhoršení	k1gNnSc7	zhoršení
mince	mince	k1gFnSc2	mince
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Marcu	Marc	k1gMnSc6	Marc
Aureliovi	Aurelius	k1gMnSc6	Aurelius
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Commodus	Commodus	k1gMnSc1	Commodus
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
Nero	Nero	k1gMnSc1	Nero
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgInS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
192	[number]	k4	192
<g/>
,	,	kIx,	,
když	když	k8xS	když
senátoři	senátor	k1gMnPc1	senátor
nebyli	být	k5eNaImAgMnP	být
nadále	nadále	k6eAd1	nadále
schopní	schopný	k2eAgMnPc1d1	schopný
snášet	snášet	k5eAaImF	snášet
jeho	jeho	k3xOp3gFnSc4	jeho
tyranii	tyranie	k1gFnSc4	tyranie
<g/>
.	.	kIx.	.
</s>
<s>
Nato	nato	k6eAd1	nato
následovalo	následovat	k5eAaImAgNnS	následovat
další	další	k2eAgNnSc1d1	další
kolo	kolo	k1gNnSc1	kolo
občanských	občanský	k2eAgFnPc2d1	občanská
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Severovci	Severovec	k1gMnPc1	Severovec
a	a	k8xC	a
vojenští	vojenský	k2eAgMnPc1d1	vojenský
císaři	císař	k1gMnPc1	císař
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
193	[number]	k4	193
se	se	k3xPyFc4	se
chopil	chopit	k5eAaPmAgMnS	chopit
moci	moct	k5eAaImF	moct
Septimius	Septimius	k1gMnSc1	Septimius
Severus	Severus	k1gMnSc1	Severus
–	–	k?	–
první	první	k4xOgNnSc4	první
císař	císař	k1gMnSc1	císař
pocházející	pocházející	k2eAgMnSc1d1	pocházející
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgMnSc3	ten
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
zvítězit	zvítězit	k5eAaPmF	zvítězit
nad	nad	k7c4	nad
Parthy	Partha	k1gFnPc4	Partha
a	a	k8xC	a
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
říši	říš	k1gFnSc3	říš
dočasně	dočasně	k6eAd1	dočasně
navrátil	navrátit	k5eAaPmAgMnS	navrátit
stabilitu	stabilita	k1gFnSc4	stabilita
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
ale	ale	k8xC	ale
započal	započnout	k5eAaPmAgInS	započnout
nebezpečný	bezpečný	k2eNgInSc1d1	nebezpečný
vzrůst	vzrůst	k1gInSc1	vzrůst
moci	moc	k1gFnSc2	moc
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Caracalla	Caracalla	k1gMnSc1	Caracalla
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
navzdory	navzdory	k7c3	navzdory
svému	svůj	k3xOyFgNnSc3	svůj
krutému	krutý	k2eAgNnSc3d1	kruté
chování	chování	k1gNnSc3	chování
k	k	k7c3	k
senátu	senát	k1gInSc3	senát
a	a	k8xC	a
k	k	k7c3	k
vlastním	vlastní	k2eAgMnPc3d1	vlastní
příbuzným	příbuzný	k1gMnPc3	příbuzný
těšil	těšit	k5eAaImAgInS	těšit
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
a	a	k8xC	a
vojáky	voják	k1gMnPc7	voják
jisté	jistý	k2eAgFnSc3d1	jistá
popularitě	popularita	k1gFnSc3	popularita
<g/>
,	,	kIx,	,
padl	padnout	k5eAaPmAgInS	padnout
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
atentátu	atentát	k1gInSc2	atentát
během	během	k7c2	během
parthského	parthský	k2eAgNnSc2d1	parthský
tažení	tažení	k1gNnSc2	tažení
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
krátká	krátký	k2eAgFnSc1d1	krátká
vláda	vláda	k1gFnSc1	vláda
Elagabala	Elagabal	k1gMnSc2	Elagabal
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
neúspěšně	úspěšně	k6eNd1	úspěšně
pokusil	pokusit	k5eAaPmAgMnS	pokusit
o	o	k7c6	o
zavedení	zavedení	k1gNnSc6	zavedení
stejnojmenného	stejnojmenný	k2eAgNnSc2d1	stejnojmenné
orientálního	orientální	k2eAgNnSc2d1	orientální
božstva	božstvo	k1gNnSc2	božstvo
jako	jako	k8xS	jako
oficiálního	oficiální	k2eAgNnSc2d1	oficiální
státního	státní	k2eAgNnSc2d1	státní
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
222	[number]	k4	222
byl	být	k5eAaImAgInS	být
nenáviděný	nenáviděný	k2eAgInSc1d1	nenáviděný
a	a	k8xC	a
zhýralý	zhýralý	k2eAgInSc1d1	zhýralý
Elagabalus	Elagabalus	k1gInSc1	Elagabalus
zavražděn	zavraždit	k5eAaPmNgInS	zavraždit
a	a	k8xC	a
císařem	císař	k1gMnSc7	císař
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Alexander	Alexandra	k1gFnPc2	Alexandra
Severus	Severus	k1gMnSc1	Severus
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
marně	marně	k6eAd1	marně
snažil	snažit	k5eAaImAgMnS	snažit
osvědčit	osvědčit	k5eAaPmF	osvědčit
ve	v	k7c6	v
válkách	válka	k1gFnPc6	válka
proti	proti	k7c3	proti
Sásánovcům	Sásánovec	k1gMnPc3	Sásánovec
a	a	k8xC	a
Germánům	Germán	k1gMnPc3	Germán
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
235	[number]	k4	235
byl	být	k5eAaImAgInS	být
nespokojenými	spokojený	k2eNgFnPc7d1	nespokojená
vojáky	voják	k1gMnPc4	voják
zavražděn	zavražděn	k2eAgMnSc1d1	zavražděn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
císaři	císař	k1gMnPc1	císař
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
nástupu	nástup	k1gInSc6	nástup
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
zajistili	zajistit	k5eAaPmAgMnP	zajistit
loajalitu	loajalita	k1gFnSc4	loajalita
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
udělovali	udělovat	k5eAaImAgMnP	udělovat
vojákům	voják	k1gMnPc3	voják
peněžité	peněžitý	k2eAgFnPc1d1	peněžitá
dary	dar	k1gInPc4	dar
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc4	jejichž
výše	výše	k1gFnSc1	výše
neustále	neustále	k6eAd1	neustále
stoupala	stoupat	k5eAaImAgFnS	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Commoda	Commoda	k1gFnSc1	Commoda
bylo	být	k5eAaImAgNnS	být
císařství	císařství	k1gNnSc1	císařství
regulérně	regulérně	k6eAd1	regulérně
vydražováno	vydražovat	k5eAaImNgNnS	vydražovat
<g/>
.	.	kIx.	.
</s>
<s>
Armáda	armáda	k1gFnSc1	armáda
a	a	k8xC	a
především	především	k6eAd1	především
pretoriánská	pretoriánský	k2eAgFnSc1d1	pretoriánská
garda	garda	k1gFnSc1	garda
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
hlavním	hlavní	k2eAgInSc7d1	hlavní
faktorem	faktor	k1gInSc7	faktor
při	při	k7c6	při
destabilizaci	destabilizace	k1gFnSc6	destabilizace
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Septimius	Septimius	k1gMnSc1	Septimius
Severus	Severus	k1gMnSc1	Severus
se	se	k3xPyFc4	se
této	tento	k3xDgFnSc2	tento
skutečnosti	skutečnost	k1gFnSc2	skutečnost
snažil	snažit	k5eAaImAgMnS	snažit
čelit	čelit	k5eAaImF	čelit
zrušením	zrušení	k1gNnSc7	zrušení
těchto	tento	k3xDgInPc2	tento
darů	dar	k1gInPc2	dar
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
ale	ale	k8xC	ale
drastickým	drastický	k2eAgInSc7d1	drastický
způsobem	způsob	k1gInSc7	způsob
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
žold	žold	k1gInSc1	žold
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
výdaje	výdaj	k1gInPc1	výdaj
na	na	k7c4	na
armádu	armáda	k1gFnSc4	armáda
staly	stát	k5eAaPmAgFnP	stát
nesnesitelnými	snesitelný	k2eNgNnPc7d1	nesnesitelné
<g/>
.	.	kIx.	.
</s>
<s>
Výše	výše	k1gFnSc1	výše
žoldu	žold	k1gInSc2	žold
rozhoupala	rozhoupat	k5eAaPmAgFnS	rozhoupat
spirálu	spirála	k1gFnSc4	spirála
zvyšování	zvyšování	k1gNnSc2	zvyšování
cen	cena	k1gFnPc2	cena
a	a	k8xC	a
vedla	vést	k5eAaImAgFnS	vést
ke	k	k7c3	k
znehodnocování	znehodnocování	k1gNnSc3	znehodnocování
mince	mince	k1gFnSc2	mince
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
byl	být	k5eAaImAgMnS	být
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
úpadek	úpadek	k1gInSc4	úpadek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
ničivý	ničivý	k2eAgInSc4d1	ničivý
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
stabilitu	stabilita	k1gFnSc4	stabilita
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Současně	současně	k6eAd1	současně
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
ekonomické	ekonomický	k2eAgNnSc1d1	ekonomické
a	a	k8xC	a
mocenské	mocenský	k2eAgNnSc1d1	mocenské
těžiště	těžiště	k1gNnSc1	těžiště
říše	říš	k1gFnSc2	říš
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
do	do	k7c2	do
provincií	provincie	k1gFnPc2	provincie
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
rekrutovaly	rekrutovat	k5eAaImAgFnP	rekrutovat
římské	římský	k2eAgFnPc4d1	římská
elity	elita	k1gFnPc4	elita
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
trend	trend	k1gInSc1	trend
byl	být	k5eAaImAgInS	být
ještě	ještě	k6eAd1	ještě
posílen	posílit	k5eAaPmNgInS	posílit
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
Septimia	Septimius	k1gMnSc2	Septimius
Severa	Severa	k1gMnSc1	Severa
zpřístupnit	zpřístupnit	k5eAaPmF	zpřístupnit
důstojnické	důstojnický	k2eAgFnPc4d1	důstojnická
hodnosti	hodnost	k1gFnPc4	hodnost
v	v	k7c6	v
armádě	armáda	k1gFnSc3	armáda
všem	všecek	k3xTgMnPc3	všecek
vojákům	voják	k1gMnPc3	voják
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
pozvolna	pozvolna	k6eAd1	pozvolna
ztrácela	ztrácet	k5eAaImAgFnS	ztrácet
své	svůj	k3xOyFgNnSc4	svůj
privilegované	privilegovaný	k2eAgNnSc4d1	privilegované
postavení	postavení	k1gNnSc4	postavení
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
završeno	završit	k5eAaPmNgNnS	završit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
212	[number]	k4	212
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Caracalla	Caracalla	k1gMnSc1	Caracalla
udělil	udělit	k5eAaPmAgMnS	udělit
všem	všecek	k3xTgMnPc3	všecek
svobodným	svobodný	k2eAgMnPc3d1	svobodný
obyvatelům	obyvatel	k1gMnPc3	obyvatel
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgNnSc4d1	římské
občanství	občanství	k1gNnSc4	občanství
(	(	kIx(	(
<g/>
Constitutio	Constitutio	k6eAd1	Constitutio
Antoniniana	Antoninian	k1gMnSc2	Antoninian
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Severovců	Severovec	k1gMnPc2	Severovec
se	s	k7c7	s
zásadním	zásadní	k2eAgInSc7d1	zásadní
způsobem	způsob	k1gInSc7	způsob
proměnila	proměnit	k5eAaPmAgFnS	proměnit
také	také	k9	také
zahraničněpolitická	zahraničněpolitický	k2eAgFnSc1d1	zahraničněpolitická
pozice	pozice	k1gFnSc1	pozice
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
:	:	kIx,	:
na	na	k7c6	na
východě	východ	k1gInSc6	východ
nahradila	nahradit	k5eAaPmAgFnS	nahradit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
224	[number]	k4	224
slabou	slabý	k2eAgFnSc4d1	slabá
parthskou	parthský	k2eAgFnSc4d1	parthský
říši	říše	k1gFnSc4	říše
vláda	vláda	k1gFnSc1	vláda
dynastie	dynastie	k1gFnSc1	dynastie
Sásánovců	Sásánovec	k1gMnPc2	Sásánovec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téže	týž	k3xTgFnSc6	týž
době	doba	k1gFnSc6	doba
nastal	nastat	k5eAaPmAgInS	nastat
mezi	mezi	k7c7	mezi
Rýnem	Rýn	k1gInSc7	Rýn
a	a	k8xC	a
Dunajem	Dunaj	k1gInSc7	Dunaj
proces	proces	k1gInSc1	proces
konstituování	konstituování	k1gNnSc2	konstituování
mohutných	mohutný	k2eAgInPc2d1	mohutný
germánských	germánský	k2eAgInPc2d1	germánský
kmenových	kmenový	k2eAgInPc2d1	kmenový
svazů	svaz	k1gInPc2	svaz
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
útočily	útočit	k5eAaImAgInP	útočit
na	na	k7c4	na
římské	římský	k2eAgFnPc4d1	římská
hranice	hranice	k1gFnPc4	hranice
a	a	k8xC	a
pronikaly	pronikat	k5eAaImAgFnP	pronikat
hluboko	hluboko	k6eAd1	hluboko
na	na	k7c4	na
římské	římský	k2eAgNnSc4d1	římské
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rokem	rok	k1gInSc7	rok
235	[number]	k4	235
začalo	začít	k5eAaPmAgNnS	začít
neblahé	blahý	k2eNgNnSc1d1	neblahé
období	období	k1gNnSc1	období
vlády	vláda	k1gFnSc2	vláda
vojenských	vojenský	k2eAgMnPc2d1	vojenský
císařů	císař	k1gMnPc2	císař
známé	známá	k1gFnSc2	známá
jako	jako	k8xC	jako
krize	krize	k1gFnSc2	krize
třetího	třetí	k4xOgInSc2	třetí
století	století	k1gNnPc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nepřetržitá	přetržitý	k2eNgFnSc1d1	nepřetržitá
série	série	k1gFnSc1	série
občanských	občanský	k2eAgFnPc2d1	občanská
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
uzurpací	uzurpace	k1gFnPc2	uzurpace
<g/>
,	,	kIx,	,
vzpour	vzpoura	k1gFnPc2	vzpoura
a	a	k8xC	a
vraždění	vraždění	k1gNnPc2	vraždění
císařů	císař	k1gMnPc2	císař
oslabila	oslabit	k5eAaPmAgFnS	oslabit
obranu	obrana	k1gFnSc4	obrana
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
učinila	učinit	k5eAaPmAgFnS	učinit
jí	on	k3xPp3gFnSc3	on
tak	tak	k6eAd1	tak
bezmocnou	bezmocný	k2eAgFnSc7d1	bezmocná
vůči	vůči	k7c3	vůči
útokům	útok	k1gInPc3	útok
jejích	její	k3xOp3gMnPc2	její
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
padesáti	padesát	k4xCc2	padesát
let	léto	k1gNnPc2	léto
mezi	mezi	k7c7	mezi
zavražděním	zavraždění	k1gNnSc7	zavraždění
Alexandra	Alexandr	k1gMnSc2	Alexandr
Severa	Severa	k1gMnSc1	Severa
a	a	k8xC	a
nástupem	nástup	k1gInSc7	nástup
Diocletiana	Diocletian	k1gMnSc2	Diocletian
se	se	k3xPyFc4	se
na	na	k7c6	na
trůně	trůn	k1gInSc6	trůn
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
v	v	k7c6	v
rychlém	rychlý	k2eAgInSc6d1	rychlý
sledu	sled	k1gInSc6	sled
celkem	celkem	k6eAd1	celkem
dvacet	dvacet	k4xCc4	dvacet
jedna	jeden	k4xCgFnSc1	jeden
legitimních	legitimní	k2eAgMnPc2d1	legitimní
císařů	císař	k1gMnPc2	císař
a	a	k8xC	a
nesčíslné	sčíslný	k2eNgNnSc1d1	nesčíslné
množství	množství	k1gNnSc1	množství
nelegitimních	legitimní	k2eNgMnPc2d1	nelegitimní
uzurpátorů	uzurpátor	k1gMnPc2	uzurpátor
<g/>
.	.	kIx.	.
</s>
<s>
Vojenští	vojenský	k2eAgMnPc1d1	vojenský
císaři	císař	k1gMnPc1	císař
se	se	k3xPyFc4	se
museli	muset	k5eAaImAgMnP	muset
vypořádávat	vypořádávat	k5eAaImF	vypořádávat
s	s	k7c7	s
útoky	útok	k1gInPc7	útok
Germánů	Germán	k1gMnPc2	Germán
na	na	k7c6	na
Rýně	Rýn	k1gInSc6	Rýn
a	a	k8xC	a
Dunaji	Dunaj	k1gInSc6	Dunaj
<g/>
.	.	kIx.	.
</s>
<s>
Gótové	Gót	k1gMnPc1	Gót
sídlící	sídlící	k2eAgMnPc1d1	sídlící
při	při	k7c6	při
severních	severní	k2eAgInPc6d1	severní
březích	břeh	k1gInPc6	břeh
Černého	Černý	k1gMnSc2	Černý
moře	moře	k1gNnSc2	moře
podnikali	podnikat	k5eAaImAgMnP	podnikat
ničivé	ničivý	k2eAgInPc4d1	ničivý
nájezdy	nájezd	k1gInPc4	nájezd
na	na	k7c4	na
Balkán	Balkán	k1gInSc4	Balkán
a	a	k8xC	a
do	do	k7c2	do
Řecka	Řecko	k1gNnSc2	Řecko
a	a	k8xC	a
útočili	útočit	k5eAaImAgMnP	útočit
dokonce	dokonce	k9	dokonce
i	i	k9	i
na	na	k7c4	na
města	město	k1gNnPc4	město
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
vydrancovali	vydrancovat	k5eAaPmAgMnP	vydrancovat
Alamani	Alaman	k1gMnPc1	Alaman
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
Galii	Galie	k1gFnSc3	Galie
a	a	k8xC	a
Itálii	Itálie	k1gFnSc3	Itálie
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nejtěžší	těžký	k2eAgInPc1d3	nejtěžší
boje	boj	k1gInPc1	boj
se	se	k3xPyFc4	se
odehrály	odehrát	k5eAaPmAgInP	odehrát
s	s	k7c7	s
novoperskou	novoperský	k2eAgFnSc7d1	novoperská
sásánovskou	sásánovský	k2eAgFnSc7d1	sásánovská
říší	říš	k1gFnSc7	říš
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
ukázala	ukázat	k5eAaPmAgFnS	ukázat
jako	jako	k9	jako
mnohem	mnohem	k6eAd1	mnohem
vážnější	vážní	k2eAgMnSc1d2	vážnější
protivník	protivník	k1gMnSc1	protivník
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
než	než	k8xS	než
jakým	jaký	k3yRgMnPc3	jaký
kdy	kdy	k6eAd1	kdy
byli	být	k5eAaImAgMnP	být
Parthové	Parth	k1gMnPc1	Parth
<g/>
.	.	kIx.	.
</s>
<s>
Perský	perský	k2eAgMnSc1d1	perský
velkokrál	velkokrál	k1gMnSc1	velkokrál
Šápúr	Šápúr	k1gMnSc1	Šápúr
I.	I.	kA	I.
vpadl	vpadnout	k5eAaPmAgMnS	vpadnout
několikrát	několikrát	k6eAd1	několikrát
do	do	k7c2	do
Sýrie	Sýrie	k1gFnSc2	Sýrie
a	a	k8xC	a
opakovaně	opakovaně	k6eAd1	opakovaně
porážel	porážet	k5eAaImAgMnS	porážet
římské	římský	k2eAgFnSc2d1	římská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
260	[number]	k4	260
padl	padnout	k5eAaImAgInS	padnout
do	do	k7c2	do
jeho	jeho	k3xOp3gFnPc2	jeho
rukou	ruka	k1gFnPc2	ruka
císař	císař	k1gMnSc1	císař
Valerianus	Valerianus	k1gMnSc1	Valerianus
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
poté	poté	k6eAd1	poté
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
perském	perský	k2eAgNnSc6d1	perské
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
se	se	k3xPyFc4	se
Římané	Říman	k1gMnPc1	Říman
snažili	snažit	k5eAaImAgMnP	snažit
udržet	udržet	k5eAaPmF	udržet
východní	východní	k2eAgFnPc4d1	východní
provincie	provincie	k1gFnPc4	provincie
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
se	se	k3xPyFc4	se
římská	římský	k2eAgFnSc1d1	římská
vláda	vláda	k1gFnSc1	vláda
rozkládat	rozkládat	k5eAaImF	rozkládat
i	i	k9	i
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
Správci	správce	k1gMnPc1	správce
provincií	provincie	k1gFnPc2	provincie
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
veleli	velet	k5eAaImAgMnP	velet
mnoha	mnoho	k4c2	mnoho
legiím	legie	k1gFnPc3	legie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
čím	co	k3yQnSc7	co
dál	daleko	k6eAd2	daleko
častěji	často	k6eAd2	často
využívali	využívat	k5eAaImAgMnP	využívat
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
neustálým	neustálý	k2eAgFnPc3d1	neustálá
uzurpacím	uzurpace	k1gFnPc3	uzurpace
či	či	k8xC	či
k	k	k7c3	k
odpadnutím	odpadnutí	k1gNnSc7	odpadnutí
celých	celý	k2eAgFnPc2d1	celá
provincií	provincie	k1gFnPc2	provincie
(	(	kIx(	(
<g/>
galské	galský	k2eAgNnSc1d1	galské
císařství	císařství	k1gNnSc1	císařství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
konflikty	konflikt	k1gInPc1	konflikt
a	a	k8xC	a
vnější	vnější	k2eAgNnSc1d1	vnější
ohrožení	ohrožení	k1gNnSc1	ohrožení
přivedly	přivést	k5eAaPmAgInP	přivést
říši	říše	k1gFnSc4	říše
na	na	k7c4	na
pokraj	pokraj	k1gInSc4	pokraj
záhuby	záhuba	k1gFnSc2	záhuba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prohlubování	prohlubování	k1gNnSc1	prohlubování
chaosu	chaos	k1gInSc2	chaos
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
až	až	k9	až
do	do	k7c2	do
vlády	vláda	k1gFnSc2	vláda
císaře	císař	k1gMnSc2	císař
Claudia	Claudia	k1gFnSc1	Claudia
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
269	[number]	k4	269
podařilo	podařit	k5eAaPmAgNnS	podařit
zvítězit	zvítězit	k5eAaPmF	zvítězit
nad	nad	k7c7	nad
Góty	Gót	k1gMnPc7	Gót
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Naissu	Naiss	k1gInSc2	Naiss
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Niš	Niš	k1gFnSc1	Niš
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
272	[number]	k4	272
si	se	k3xPyFc3	se
císař	císař	k1gMnSc1	císař
Aurelianus	Aurelianus	k1gMnSc1	Aurelianus
podrobil	podrobit	k5eAaPmAgMnS	podrobit
Palmýru	Palmýra	k1gFnSc4	Palmýra
<g/>
,	,	kIx,	,
někdejšího	někdejší	k2eAgMnSc2d1	někdejší
římského	římský	k2eAgMnSc2d1	římský
spojence	spojenec	k1gMnSc2	spojenec
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
královna	královna	k1gFnSc1	královna
Zenobia	Zenobium	k1gNnSc2	Zenobium
využila	využít	k5eAaPmAgFnS	využít
římské	římský	k2eAgFnPc4d1	římská
slabosti	slabost	k1gFnPc4	slabost
a	a	k8xC	a
dočasně	dočasně	k6eAd1	dočasně
se	se	k3xPyFc4	se
zmocnila	zmocnit	k5eAaPmAgFnS	zmocnit
téměř	téměř	k6eAd1	téměř
všech	všecek	k3xTgFnPc2	všecek
východních	východní	k2eAgFnPc2d1	východní
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Aurelianus	Aurelianus	k1gMnSc1	Aurelianus
porazil	porazit	k5eAaPmAgMnS	porazit
také	také	k9	také
Germány	Germán	k1gMnPc4	Germán
a	a	k8xC	a
obnovil	obnovit	k5eAaPmAgMnS	obnovit
římskou	římský	k2eAgFnSc4d1	římská
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
Galií	Galie	k1gFnSc7	Galie
<g/>
.	.	kIx.	.
</s>
<s>
Krize	krize	k1gFnSc1	krize
vedla	vést	k5eAaImAgFnS	vést
ke	k	k7c3	k
značným	značný	k2eAgFnPc3d1	značná
společenským	společenský	k2eAgFnPc3d1	společenská
změnám	změna	k1gFnPc3	změna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
však	však	k9	však
nezasáhly	zasáhnout	k5eNaPmAgFnP	zasáhnout
všechna	všechen	k3xTgNnPc1	všechen
území	území	k1gNnPc1	území
říše	říš	k1gFnSc2	říš
stejně	stejně	k6eAd1	stejně
<g/>
.	.	kIx.	.
</s>
<s>
Římanům	Říman	k1gMnPc3	Říman
se	se	k3xPyFc4	se
tak	tak	k9	tak
podařilo	podařit	k5eAaPmAgNnS	podařit
odvrátit	odvrátit	k5eAaPmF	odvrátit
hrozící	hrozící	k2eAgInSc4d1	hrozící
zánik	zánik	k1gInSc4	zánik
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Nestabilita	nestabilita	k1gFnSc1	nestabilita
a	a	k8xC	a
chaos	chaos	k1gInSc4	chaos
vytvářely	vytvářet	k5eAaImAgFnP	vytvářet
příznivé	příznivý	k2eAgFnPc1d1	příznivá
podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
šíření	šíření	k1gNnSc4	šíření
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
stoupenci	stoupenec	k1gMnPc1	stoupenec
hlásali	hlásat	k5eAaImAgMnP	hlásat
mír	mír	k1gInSc4	mír
a	a	k8xC	a
spásu	spása	k1gFnSc4	spása
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgNnPc2d1	ostatní
náboženství	náboženství	k1gNnPc2	náboženství
a	a	k8xC	a
pohanských	pohanský	k2eAgInPc2d1	pohanský
kultů	kult	k1gInPc2	kult
však	však	k9	však
křesťané	křesťan	k1gMnPc1	křesťan
neprojevovali	projevovat	k5eNaImAgMnP	projevovat
patřičný	patřičný	k2eAgInSc4d1	patřičný
respekt	respekt	k1gInSc4	respekt
k	k	k7c3	k
instituci	instituce	k1gFnSc3	instituce
císařství	císařství	k1gNnSc2	císařství
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Decius	Decius	k1gMnSc1	Decius
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
obnovit	obnovit	k5eAaPmF	obnovit
uctívání	uctívání	k1gNnSc4	uctívání
starých	starý	k2eAgNnPc2d1	staré
božstev	božstvo	k1gNnPc2	božstvo
<g/>
,	,	kIx,	,
žádal	žádat	k5eAaImAgMnS	žádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jim	on	k3xPp3gMnPc3	on
všichni	všechen	k3xTgMnPc1	všechen
obyvatelé	obyvatel	k1gMnPc1	obyvatel
říše	říš	k1gFnSc2	říš
povinně	povinně	k6eAd1	povinně
obětovali	obětovat	k5eAaBmAgMnP	obětovat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
tomuto	tento	k3xDgMnSc3	tento
nařízení	nařízený	k2eAgMnPc1d1	nařízený
křesťané	křesťan	k1gMnPc1	křesťan
vzpírali	vzpírat	k5eAaImAgMnP	vzpírat
<g/>
,	,	kIx,	,
nařídil	nařídit	k5eAaPmAgMnS	nařídit
jejich	jejich	k3xOp3gNnPc4	jejich
pronásledování	pronásledování	k1gNnPc4	pronásledování
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Deciovi	Decius	k1gMnSc6	Decius
pronásledovali	pronásledovat	k5eAaImAgMnP	pronásledovat
křesťany	křesťan	k1gMnPc7	křesťan
také	také	k6eAd1	také
Valerianus	Valerianus	k1gMnSc1	Valerianus
a	a	k8xC	a
Diocletianus	Diocletianus	k1gMnSc1	Diocletianus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nástup	nástup	k1gInSc1	nástup
pozdní	pozdní	k2eAgFnSc2d1	pozdní
antiky	antika	k1gFnSc2	antika
===	===	k?	===
</s>
</p>
<p>
<s>
Nástup	nástup	k1gInSc1	nástup
Diocletiana	Diocletian	k1gMnSc2	Diocletian
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
284	[number]	k4	284
symbolizoval	symbolizovat	k5eAaImAgInS	symbolizovat
počátek	počátek	k1gInSc4	počátek
období	období	k1gNnSc2	období
pozdní	pozdní	k2eAgFnSc2d1	pozdní
antiky	antika	k1gFnSc2	antika
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
charakterizováno	charakterizovat	k5eAaBmNgNnS	charakterizovat
postupující	postupující	k2eAgFnSc7d1	postupující
centralizací	centralizace	k1gFnSc7	centralizace
a	a	k8xC	a
byrokratizací	byrokratizace	k1gFnSc7	byrokratizace
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
pozdějším	pozdní	k2eAgNnSc7d2	pozdější
vítězstvím	vítězství	k1gNnSc7	vítězství
křesťanství	křesťanství	k1gNnSc2	křesťanství
nad	nad	k7c7	nad
pohanstvím	pohanství	k1gNnSc7	pohanství
<g/>
.	.	kIx.	.
</s>
<s>
Diocletianovi	Diocletian	k1gMnSc3	Diocletian
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
celou	celý	k2eAgFnSc7d1	celá
řadou	řada	k1gFnSc7	řada
opatření	opatření	k1gNnPc2	opatření
a	a	k8xC	a
reforem	reforma	k1gFnPc2	reforma
odvrátit	odvrátit	k5eAaPmF	odvrátit
hrozící	hrozící	k2eAgInSc4d1	hrozící
zánik	zánik	k1gInSc4	zánik
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
dosavadní	dosavadní	k2eAgFnSc4d1	dosavadní
formu	forma	k1gFnSc4	forma
vlády	vláda	k1gFnSc2	vláda
–	–	k?	–
principát	principát	k1gInSc1	principát
–	–	k?	–
přitom	přitom	k6eAd1	přitom
postavil	postavit	k5eAaPmAgMnS	postavit
na	na	k7c6	na
zcela	zcela	k6eAd1	zcela
nových	nový	k2eAgInPc6d1	nový
základech	základ	k1gInPc6	základ
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
provedl	provést	k5eAaPmAgMnS	provést
reformu	reforma	k1gFnSc4	reforma
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
níž	jenž	k3xRgFnSc2	jenž
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
počet	počet	k1gInSc1	počet
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
byla	být	k5eAaImAgFnS	být
oddělena	oddělen	k2eAgFnSc1d1	oddělena
civilní	civilní	k2eAgFnSc1d1	civilní
a	a	k8xC	a
vojenská	vojenský	k2eAgFnSc1d1	vojenská
správa	správa	k1gFnSc1	správa
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
jejich	jejich	k3xOp3gMnPc3	jejich
správcům	správce	k1gMnPc3	správce
zamezit	zamezit	k5eAaPmF	zamezit
ve	v	k7c6	v
vzpouře	vzpoura	k1gFnSc6	vzpoura
proti	proti	k7c3	proti
ústřední	ústřední	k2eAgFnSc3d1	ústřední
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Stanovením	stanovení	k1gNnSc7	stanovení
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
možných	možný	k2eAgFnPc2d1	možná
cen	cena	k1gFnPc2	cena
se	se	k3xPyFc4	se
Diocletianus	Diocletianus	k1gMnSc1	Diocletianus
neúspěšně	úspěšně	k6eNd1	úspěšně
pokusil	pokusit	k5eAaPmAgMnS	pokusit
bojovat	bojovat	k5eAaImF	bojovat
s	s	k7c7	s
hospodářským	hospodářský	k2eAgInSc7d1	hospodářský
poklesem	pokles	k1gInSc7	pokles
a	a	k8xC	a
utlumit	utlumit	k5eAaPmF	utlumit
inflaci	inflace	k1gFnSc4	inflace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
řádila	řádit	k5eAaImAgFnS	řádit
už	už	k6eAd1	už
od	od	k7c2	od
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zajištění	zajištění	k1gNnSc3	zajištění
hranic	hranice	k1gFnPc2	hranice
říše	říš	k1gFnSc2	říš
provedl	provést	k5eAaPmAgMnS	provést
reformu	reforma	k1gFnSc4	reforma
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
si	se	k3xPyFc3	se
ale	ale	k9	ale
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
zvýšení	zvýšení	k1gNnSc4	zvýšení
už	už	k6eAd1	už
tak	tak	k6eAd1	tak
vysokých	vysoký	k2eAgFnPc2d1	vysoká
daní	daň	k1gFnPc2	daň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Diocletianus	Diocletianus	k1gMnSc1	Diocletianus
dále	daleko	k6eAd2	daleko
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
nový	nový	k2eAgInSc4d1	nový
systém	systém	k1gInSc4	systém
vlády	vláda	k1gFnSc2	vláda
zvaný	zvaný	k2eAgMnSc1d1	zvaný
tetrarchie	tetrarchie	k1gFnPc4	tetrarchie
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
čtyřvládí	čtyřvládit	k5eAaPmIp3nS	čtyřvládit
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
kolegu	kolega	k1gMnSc4	kolega
a	a	k8xC	a
druhého	druhý	k4xOgMnSc2	druhý
augusta	august	k1gMnSc2	august
povolal	povolat	k5eAaPmAgMnS	povolat
svého	svůj	k3xOyFgMnSc4	svůj
přítele	přítel	k1gMnSc4	přítel
Maximiana	Maximian	k1gMnSc4	Maximian
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
císaři	císař	k1gMnPc1	císař
později	pozdě	k6eAd2	pozdě
jmenovali	jmenovat	k5eAaImAgMnP	jmenovat
po	po	k7c6	po
jednom	jeden	k4xCgMnSc6	jeden
caesarovi	caesar	k1gMnSc6	caesar
jako	jako	k8xS	jako
svých	svůj	k3xOyFgInPc6	svůj
zástupcích	zástupek	k1gInPc6	zástupek
(	(	kIx(	(
<g/>
Constantius	Constantius	k1gMnSc1	Constantius
I.	I.	kA	I.
Chlorus	Chlorus	k1gMnSc1	Chlorus
a	a	k8xC	a
Galerius	Galerius	k1gMnSc1	Galerius
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
této	tento	k3xDgFnSc2	tento
reformy	reforma	k1gFnSc2	reforma
byla	být	k5eAaImAgFnS	být
jednak	jednak	k8xC	jednak
snaha	snaha	k1gFnSc1	snaha
zabránit	zabránit	k5eAaPmF	zabránit
uzurpacím	uzurpace	k1gFnPc3	uzurpace
velitelů	velitel	k1gMnPc2	velitel
pohraničních	pohraniční	k2eAgFnPc2d1	pohraniční
armád	armáda	k1gFnPc2	armáda
a	a	k8xC	a
jednak	jednak	k8xC	jednak
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebylo	být	k5eNaImAgNnS	být
v	v	k7c6	v
silách	síla	k1gFnPc6	síla
jediného	jediný	k2eAgMnSc2d1	jediný
člověka	člověk	k1gMnSc2	člověk
efektivně	efektivně	k6eAd1	efektivně
ovládat	ovládat	k5eAaImF	ovládat
celý	celý	k2eAgInSc4d1	celý
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
rozdělení	rozdělení	k1gNnSc2	rozdělení
vlády	vláda	k1gFnSc2	vláda
nebyla	být	k5eNaImAgFnS	být
úplnou	úplný	k2eAgFnSc7d1	úplná
novinkou	novinka	k1gFnSc7	novinka
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
teprve	teprve	k6eAd1	teprve
za	za	k7c4	za
Diocletiana	Diocletian	k1gMnSc4	Diocletian
byla	být	k5eAaImAgFnS	být
důsledně	důsledně	k6eAd1	důsledně
realizována	realizován	k2eAgFnSc1d1	realizována
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
jí	on	k3xPp3gFnSc3	on
nelze	lze	k6eNd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
ústup	ústup	k1gInSc4	ústup
od	od	k7c2	od
ideje	idea	k1gFnSc2	idea
říšské	říšský	k2eAgFnSc2d1	říšská
jednoty	jednota	k1gFnSc2	jednota
<g/>
.	.	kIx.	.
</s>
<s>
Řím	Řím	k1gInSc1	Řím
zůstával	zůstávat	k5eAaImAgInS	zůstávat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
všichni	všechen	k3xTgMnPc1	všechen
císařové	císař	k1gMnPc1	císař
přemístili	přemístit	k5eAaPmAgMnP	přemístit
své	svůj	k3xOyFgFnPc4	svůj
rezidence	rezidence	k1gFnPc4	rezidence
blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
hranicím	hranice	k1gFnPc3	hranice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mohli	moct	k5eAaImAgMnP	moct
lépe	dobře	k6eAd2	dobře
čelit	čelit	k5eAaImF	čelit
nepřátelům	nepřítel	k1gMnPc3	nepřítel
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Diocletianus	Diocletianus	k1gMnSc1	Diocletianus
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
snažil	snažit	k5eAaImAgMnS	snažit
utužit	utužit	k5eAaPmF	utužit
autoritu	autorita	k1gFnSc4	autorita
státu	stát	k1gInSc2	stát
a	a	k8xC	a
osoby	osoba	k1gFnPc4	osoba
císaře	císař	k1gMnSc2	císař
<g/>
,	,	kIx,	,
pročež	pročež	k6eAd1	pročež
přijal	přijmout	k5eAaPmAgMnS	přijmout
přízvisko	přízvisko	k1gNnSc4	přízvisko
Iovius	Iovius	k1gInSc1	Iovius
podle	podle	k7c2	podle
boha	bůh	k1gMnSc4	bůh
Jova	Jova	k1gMnSc1	Jova
<g/>
.	.	kIx.	.
</s>
<s>
Křesťany	Křesťan	k1gMnPc4	Křesťan
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mu	on	k3xPp3gMnSc3	on
odmítali	odmítat	k5eAaImAgMnP	odmítat
vzdávat	vzdávat	k5eAaImF	vzdávat
božské	božský	k2eAgFnPc4d1	božská
pocty	pocta	k1gFnPc4	pocta
<g/>
,	,	kIx,	,
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
neloajální	loajální	k2eNgNnSc4d1	neloajální
vůči	vůči	k7c3	vůči
říši	říš	k1gFnSc3	říš
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
čemuž	což	k3yRnSc3	což
neváhal	váhat	k5eNaImAgMnS	váhat
rozpoutat	rozpoutat	k5eAaPmF	rozpoutat
jejich	jejich	k3xOp3gNnSc4	jejich
poslední	poslední	k2eAgNnSc4d1	poslední
a	a	k8xC	a
nejtěžší	těžký	k2eAgNnSc4d3	nejtěžší
pronásledování	pronásledování	k1gNnSc4	pronásledování
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
orientálních	orientální	k2eAgMnPc2d1	orientální
despotů	despot	k1gMnPc2	despot
si	se	k3xPyFc3	se
Diocletianus	Diocletianus	k1gInSc4	Diocletianus
vsadil	vsadit	k5eAaPmAgInS	vsadit
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
diadém	diadém	k1gInSc4	diadém
a	a	k8xC	a
obklopil	obklopit	k5eAaPmAgMnS	obklopit
se	se	k3xPyFc4	se
ohromným	ohromný	k2eAgInSc7d1	ohromný
císařským	císařský	k2eAgInSc7d1	císařský
dvorem	dvůr	k1gInSc7	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
principátu	principát	k1gInSc2	principát
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
císaři	císař	k1gMnPc1	císař
snažili	snažit	k5eAaImAgMnP	snažit
alespoň	alespoň	k9	alespoň
zdánlivě	zdánlivě	k6eAd1	zdánlivě
vládnout	vládnout	k5eAaImF	vládnout
ve	v	k7c6	v
shodě	shoda	k1gFnSc6	shoda
se	s	k7c7	s
senátem	senát	k1gInSc7	senát
<g/>
,	,	kIx,	,
Diocletianus	Diocletianus	k1gMnSc1	Diocletianus
nebral	brát	k5eNaImAgMnS	brát
na	na	k7c4	na
senát	senát	k1gInSc1	senát
žádné	žádný	k3yNgInPc4	žádný
ohledy	ohled	k1gInPc4	ohled
a	a	k8xC	a
počínal	počínat	k5eAaImAgMnS	počínat
si	se	k3xPyFc3	se
jako	jako	k8xS	jako
neomezený	omezený	k2eNgMnSc1d1	neomezený
panovník	panovník	k1gMnSc1	panovník
(	(	kIx(	(
<g/>
dominus	dominus	k1gMnSc1	dominus
et	et	k?	et
deus	deus	k1gInSc1	deus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
systému	systém	k1gInSc6	systém
vlády	vláda	k1gFnSc2	vláda
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
občané	občan	k1gMnPc1	občan
čím	co	k3yRnSc7	co
dál	daleko	k6eAd2	daleko
více	hodně	k6eAd2	hodně
jako	jako	k8xS	jako
poddaní	poddaný	k1gMnPc1	poddaný
císaře	císař	k1gMnSc2	císař
<g/>
,	,	kIx,	,
čemuž	což	k3yRnSc3	což
napomohl	napomoct	k5eAaPmAgMnS	napomoct
také	také	k9	také
sociální	sociální	k2eAgInSc1d1	sociální
fenomén	fenomén	k1gInSc1	fenomén
rozvíjející	rozvíjející	k2eAgInSc1d1	rozvíjející
se	se	k3xPyFc4	se
již	již	k6eAd1	již
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
–	–	k?	–
kolonát	kolonát	k1gInSc1	kolonát
<g/>
.	.	kIx.	.
</s>
<s>
Postupný	postupný	k2eAgInSc1d1	postupný
útlum	útlum	k1gInSc1	útlum
přílivu	příliv	k1gInSc2	příliv
otroků	otrok	k1gMnPc2	otrok
nutil	nutit	k5eAaImAgMnS	nutit
velkostatkáře	velkostatkář	k1gMnSc4	velkostatkář
k	k	k7c3	k
pronajímání	pronajímání	k1gNnSc3	pronajímání
svých	svůj	k3xOyFgInPc2	svůj
pozemků	pozemek	k1gInPc2	pozemek
pachtýřům	pachtýř	k1gMnPc3	pachtýř
<g/>
,	,	kIx,	,
kolónům	kolón	k1gMnPc3	kolón
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
pachtovné	pachtovné	k1gNnSc4	pachtovné
museli	muset	k5eAaImAgMnP	muset
kolóni	kolón	k1gMnPc1	kolón
odvádět	odvádět	k5eAaImF	odvádět
statkářům	statkář	k1gMnPc3	statkář
nejprve	nejprve	k6eAd1	nejprve
peníze	peníz	k1gInPc4	peníz
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
část	část	k1gFnSc1	část
úrody	úroda	k1gFnSc2	úroda
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
kolóni	kolóeň	k1gFnSc3	kolóeň
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
byli	být	k5eAaImAgMnP	být
římskými	římský	k2eAgMnPc7d1	římský
občany	občan	k1gMnPc7	občan
<g/>
,	,	kIx,	,
stávali	stávat	k5eAaImAgMnP	stávat
závislými	závislý	k2eAgInPc7d1	závislý
na	na	k7c4	na
velkostatkářích	velkostatkář	k1gMnPc6	velkostatkář
a	a	k8xC	a
ztráceli	ztrácet	k5eAaImAgMnP	ztrácet
svoji	svůj	k3xOyFgFnSc4	svůj
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
trend	trend	k1gInSc1	trend
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
prohloubil	prohloubit	k5eAaPmAgMnS	prohloubit
za	za	k7c4	za
Diocletianova	Diocletianův	k2eAgMnSc4d1	Diocletianův
nástupce	nástupce	k1gMnSc4	nástupce
Konstantina	Konstantin	k1gMnSc4	Konstantin
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byli	být	k5eAaImAgMnP	být
kolóni	kolón	k1gMnPc1	kolón
připoutáni	připoutat	k5eAaPmNgMnP	připoutat
k	k	k7c3	k
půdě	půda	k1gFnSc3	půda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Systém	systém	k1gInSc1	systém
tetrarchie	tetrarchie	k1gFnSc2	tetrarchie
se	se	k3xPyFc4	se
zhroutil	zhroutit	k5eAaPmAgInS	zhroutit
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
Diocletianus	Diocletianus	k1gMnSc1	Diocletianus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
305	[number]	k4	305
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
bez	bez	k7c2	bez
jeho	jeho	k3xOp3gFnSc2	jeho
autority	autorita	k1gFnSc2	autorita
propukly	propuknout	k5eAaPmAgFnP	propuknout
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgMnPc7d1	jednotlivý
tetrarchy	tetrarch	k1gMnPc7	tetrarch
konflikty	konflikt	k1gInPc4	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následných	následný	k2eAgInPc2d1	následný
zmatků	zmatek	k1gInPc2	zmatek
a	a	k8xC	a
občanských	občanský	k2eAgFnPc2d1	občanská
válek	válka	k1gFnPc2	válka
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
306	[number]	k4	306
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
císaře	císař	k1gMnSc4	císař
syn	syn	k1gMnSc1	syn
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
Diocletianových	Diocletianův	k2eAgMnPc2d1	Diocletianův
caesarů	caesar	k1gMnPc2	caesar
Konstantin	Konstantin	k1gMnSc1	Konstantin
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
říše	říše	k1gFnSc1	říše
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
312	[number]	k4	312
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
Konstantinovi	Konstantinův	k2eAgMnPc1d1	Konstantinův
vojáci	voják	k1gMnPc1	voják
s	s	k7c7	s
Kristovým	Kristův	k2eAgInSc7d1	Kristův
monogramem	monogram	k1gInSc7	monogram
na	na	k7c6	na
štítech	štít	k1gInPc6	štít
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Milvijského	Milvijský	k2eAgInSc2d1	Milvijský
mostu	most	k1gInSc2	most
nad	nad	k7c7	nad
císařem	císař	k1gMnSc7	císař
Maxentiem	Maxentius	k1gMnSc7	Maxentius
a	a	k8xC	a
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
vydal	vydat	k5eAaPmAgMnS	vydat
Konstantin	Konstantin	k1gMnSc1	Konstantin
v	v	k7c4	v
Mediolanu	Mediolana	k1gFnSc4	Mediolana
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc1d1	dnešní
Milán	Milán	k1gInSc1	Milán
<g/>
)	)	kIx)	)
edikt	edikt	k1gInSc1	edikt
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
uznal	uznat	k5eAaPmAgMnS	uznat
křesťanství	křesťanství	k1gNnSc4	křesťanství
za	za	k7c4	za
rovnocenné	rovnocenný	k2eAgNnSc4d1	rovnocenné
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Přijetím	přijetí	k1gNnSc7	přijetí
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
formálně	formálně	k6eAd1	formálně
stalo	stát	k5eAaPmAgNnS	stát
pouze	pouze	k6eAd1	pouze
tolerovaným	tolerovaný	k2eAgNnSc7d1	tolerované
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
však	však	k9	však
preferovaným	preferovaný	k2eAgNnSc7d1	preferované
náboženstvím	náboženství	k1gNnSc7	náboženství
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
stát	stát	k1gInSc1	stát
získat	získat	k5eAaPmF	získat
novou	nový	k2eAgFnSc4d1	nová
jednotící	jednotící	k2eAgFnSc4d1	jednotící
ideologii	ideologie	k1gFnSc4	ideologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Církev	církev	k1gFnSc1	církev
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
složitou	složitý	k2eAgFnSc7d1	složitá
hierarchií	hierarchie	k1gFnSc7	hierarchie
rychle	rychle	k6eAd1	rychle
prorostla	prorůst	k5eAaPmAgFnS	prorůst
do	do	k7c2	do
římského	římský	k2eAgInSc2d1	římský
správního	správní	k2eAgInSc2d1	správní
systému	systém	k1gInSc2	systém
a	a	k8xC	a
již	již	k6eAd1	již
za	za	k7c2	za
Konstantina	Konstantin	k1gMnSc2	Konstantin
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
pilířem	pilíř	k1gInSc7	pilíř
říšské	říšský	k2eAgFnSc3d1	říšská
moci	moc	k1gFnSc3	moc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
312	[number]	k4	312
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
Konstantin	Konstantin	k1gMnSc1	Konstantin
vlády	vláda	k1gFnSc2	vláda
nad	nad	k7c7	nad
západem	západ	k1gInSc7	západ
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
324	[number]	k4	324
se	se	k3xPyFc4	se
po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
nad	nad	k7c7	nad
Liciniem	Licinium	k1gNnSc7	Licinium
ustavil	ustavit	k5eAaPmAgMnS	ustavit
jediným	jediný	k2eAgMnSc7d1	jediný
vládcem	vládce	k1gMnSc7	vládce
celého	celý	k2eAgNnSc2d1	celé
impéria	impérium	k1gNnSc2	impérium
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
uznání	uznání	k1gNnSc2	uznání
křesťanství	křesťanství	k1gNnSc2	křesťanství
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
vládnutí	vládnutí	k1gNnSc1	vládnutí
významné	významný	k2eAgNnSc1d1	významné
ještě	ještě	k6eAd1	ještě
jednou	jeden	k4xCgFnSc7	jeden
událostí	událost	k1gFnSc7	událost
<g/>
:	:	kIx,	:
založením	založení	k1gNnSc7	založení
Nového	Nového	k2eAgInSc2d1	Nového
Říma	Řím	k1gInSc2	Řím
–	–	k?	–
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
<g/>
.	.	kIx.	.
</s>
<s>
Těžiště	těžiště	k1gNnSc1	těžiště
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
tím	ten	k3xDgInSc7	ten
s	s	k7c7	s
definitivní	definitivní	k2eAgFnSc7d1	definitivní
platností	platnost	k1gFnSc7	platnost
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
do	do	k7c2	do
její	její	k3xOp3gFnSc2	její
bohatší	bohatý	k2eAgFnSc2d2	bohatší
a	a	k8xC	a
stabilnější	stabilní	k2eAgFnSc2d2	stabilnější
východní	východní	k2eAgFnSc2d1	východní
poloviny	polovina	k1gFnSc2	polovina
<g/>
.	.	kIx.	.
</s>
<s>
Konstantin	Konstantin	k1gMnSc1	Konstantin
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c6	v
Diocletianových	Diocletianův	k2eAgFnPc6d1	Diocletianova
správních	správní	k2eAgFnPc6d1	správní
reformách	reforma	k1gFnPc6	reforma
zavedením	zavedení	k1gNnSc7	zavedení
nových	nový	k2eAgInPc2d1	nový
územních	územní	k2eAgInPc2d1	územní
celků	celek	k1gInPc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Říše	říše	k1gFnSc1	říše
byla	být	k5eAaImAgFnS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
prefektur	prefektura	k1gFnPc2	prefektura
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
dále	daleko	k6eAd2	daleko
členily	členit	k5eAaImAgFnP	členit
na	na	k7c4	na
třináct	třináct	k4xCc4	třináct
diecézí	diecéze	k1gFnPc2	diecéze
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
byla	být	k5eAaImAgFnS	být
zvýšena	zvýšit	k5eAaPmNgFnS	zvýšit
výkonnost	výkonnost	k1gFnSc1	výkonnost
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
<s>
Diocletianovy	Diocletianův	k2eAgFnPc1d1	Diocletianova
a	a	k8xC	a
Konstantinovy	Konstantinův	k2eAgFnPc1d1	Konstantinova
reformy	reforma	k1gFnPc1	reforma
přispěly	přispět	k5eAaPmAgFnP	přispět
ke	k	k7c3	k
stabilizaci	stabilizace	k1gFnSc3	stabilizace
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
poskytly	poskytnout	k5eAaPmAgInP	poskytnout
státu	stát	k1gInSc2	stát
nové	nový	k2eAgInPc4d1	nový
finanční	finanční	k2eAgInSc4d1	finanční
zdroje	zdroj	k1gInPc4	zdroj
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
dovolily	dovolit	k5eAaPmAgFnP	dovolit
zvětšit	zvětšit	k5eAaPmF	zvětšit
vojsko	vojsko	k1gNnSc4	vojsko
a	a	k8xC	a
zajistit	zajistit	k5eAaPmF	zajistit
lepší	dobrý	k2eAgFnSc4d2	lepší
obranu	obrana	k1gFnSc4	obrana
ohrožených	ohrožený	k2eAgFnPc2d1	ohrožená
hranic	hranice	k1gFnPc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Enormně	enormně	k6eAd1	enormně
vyšroubovaná	vyšroubovaný	k2eAgFnSc1d1	vyšroubovaná
daňová	daňový	k2eAgFnSc1d1	daňová
zátěž	zátěž	k1gFnSc1	zátěž
ovšem	ovšem	k9	ovšem
podlomila	podlomit	k5eAaPmAgFnS	podlomit
hospodářství	hospodářství	k1gNnSc2	hospodářství
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
poddaných	poddaný	k1gMnPc2	poddaný
nutila	nutit	k5eAaImAgFnS	nutit
k	k	k7c3	k
úniku	únik	k1gInSc3	únik
od	od	k7c2	od
tohoto	tento	k3xDgNnSc2	tento
tíživého	tíživý	k2eAgNnSc2d1	tíživé
břemena	břemeno	k1gNnSc2	břemeno
<g/>
.	.	kIx.	.
</s>
<s>
Římskou	římský	k2eAgFnSc4d1	římská
společnost	společnost	k1gFnSc4	společnost
především	především	k9	především
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
provinciích	provincie	k1gFnPc6	provincie
říše	říše	k1gFnSc1	říše
zachvátila	zachvátit	k5eAaPmAgFnS	zachvátit
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
vlna	vlna	k1gFnSc1	vlna
deurbanizace	deurbanizace	k1gFnSc1	deurbanizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Záhy	záhy	k6eAd1	záhy
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Konstantina	Konstantin	k1gMnSc2	Konstantin
v	v	k7c6	v
roce	rok	k1gInSc6	rok
337	[number]	k4	337
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gMnPc7	jeho
třemi	tři	k4xCgMnPc7	tři
syny	syn	k1gMnPc4	syn
rozhořel	rozhořet	k5eAaPmAgInS	rozhořet
boj	boj	k1gInSc1	boj
o	o	k7c4	o
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
353	[number]	k4	353
se	se	k3xPyFc4	se
poslední	poslední	k2eAgMnSc1d1	poslední
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
Constantius	Constantius	k1gInSc1	Constantius
II	II	kA	II
<g/>
.	.	kIx.	.
stal	stát	k5eAaPmAgMnS	stát
opět	opět	k6eAd1	opět
jediným	jediný	k2eAgMnSc7d1	jediný
vládcem	vládce	k1gMnSc7	vládce
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
361	[number]	k4	361
jeho	jeho	k3xOp3gFnSc4	jeho
bratranec	bratranec	k1gMnSc1	bratranec
Julianus	Julianus	k1gMnSc1	Julianus
zvaný	zvaný	k2eAgMnSc1d1	zvaný
Apostata	apostata	k1gMnSc1	apostata
(	(	kIx(	(
<g/>
Odpadlík	odpadlík	k1gMnSc1	odpadlík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
o	o	k7c4	o
návrat	návrat	k1gInSc4	návrat
k	k	k7c3	k
pohanství	pohanství	k1gNnSc3	pohanství
<g/>
.	.	kIx.	.
</s>
<s>
Nezapočal	započnout	k5eNaPmAgMnS	započnout
však	však	k9	však
žádné	žádný	k3yNgNnSc4	žádný
nové	nový	k2eAgNnSc4d1	nové
pronásledování	pronásledování	k1gNnSc4	pronásledování
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
podporu	podpora	k1gFnSc4	podpora
původních	původní	k2eAgInPc2d1	původní
kultů	kult	k1gInPc2	kult
a	a	k8xC	a
také	také	k9	také
skrytě	skrytě	k6eAd1	skrytě
podporoval	podporovat	k5eAaImAgMnS	podporovat
rozpory	rozpor	k1gInPc4	rozpor
uvnitř	uvnitř	k7c2	uvnitř
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaPmAgInS	napsat
řadu	řada	k1gFnSc4	řada
spisů	spis	k1gInPc2	spis
a	a	k8xC	a
dopisů	dopis	k1gInPc2	dopis
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
nejznámější	známý	k2eAgMnPc4d3	nejznámější
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
Contra	Contra	k1gFnSc1	Contra
Galileos	Galileosa	k1gFnPc2	Galileosa
<g/>
.	.	kIx.	.
</s>
<s>
Obnova	obnova	k1gFnSc1	obnova
starých	starý	k2eAgInPc2d1	starý
kultů	kult	k1gInPc2	kult
však	však	k9	však
neměla	mít	k5eNaImAgFnS	mít
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
trvání	trvání	k1gNnSc2	trvání
a	a	k8xC	a
Julianovou	Julianův	k2eAgFnSc7d1	Julianova
předčasnou	předčasný	k2eAgFnSc7d1	předčasná
smrtí	smrt	k1gFnSc7	smrt
během	během	k7c2	během
tažení	tažení	k1gNnSc2	tažení
v	v	k7c6	v
Persii	Persie	k1gFnSc6	Persie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
363	[number]	k4	363
konstantinovská	konstantinovský	k2eAgFnSc1d1	konstantinovská
dynastie	dynastie	k1gFnSc1	dynastie
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Juliánův	Juliánův	k2eAgMnSc1d1	Juliánův
nástupce	nástupce	k1gMnSc1	nástupce
Joviauns	Joviaunsa	k1gFnPc2	Joviaunsa
pak	pak	k6eAd1	pak
většinu	většina	k1gFnSc4	většina
Juliánových	Juliánův	k2eAgNnPc2d1	Juliánův
nařízení	nařízení	k1gNnPc2	nařízení
podporující	podporující	k2eAgNnSc1d1	podporující
pohanství	pohanství	k1gNnSc1	pohanství
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
postavení	postavení	k1gNnSc4	postavení
v	v	k7c6	v
impériu	impérium	k1gNnSc6	impérium
zrušil	zrušit	k5eAaPmAgMnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Valentiniana	Valentinian	k1gMnSc2	Valentinian
I.	I.	kA	I.
byla	být	k5eAaImAgFnS	být
říše	říše	k1gFnSc1	říše
ze	z	k7c2	z
správních	správní	k2eAgInPc2d1	správní
důvodů	důvod	k1gInPc2	důvod
dočasně	dočasně	k6eAd1	dočasně
(	(	kIx(	(
<g/>
364	[number]	k4	364
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Theodosia	Theodosium	k1gNnSc2	Theodosium
I.	I.	kA	I.
trvale	trvale	k6eAd1	trvale
rozdělena	rozdělen	k2eAgFnSc1d1	rozdělena
(	(	kIx(	(
<g/>
395	[number]	k4	395
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Valentinianus	Valentinianus	k1gMnSc1	Valentinianus
se	se	k3xPyFc4	se
podělil	podělit	k5eAaPmAgMnS	podělit
o	o	k7c4	o
vládu	vláda	k1gFnSc4	vláda
se	s	k7c7	s
svým	svůj	k1gMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Valentem	Valentem	k?	Valentem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vpádu	vpád	k1gInSc6	vpád
Hunů	Hun	k1gMnPc2	Hun
do	do	k7c2	do
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
375	[number]	k4	375
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
započalo	započnout	k5eAaPmAgNnS	započnout
stěhování	stěhování	k1gNnSc4	stěhování
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
Gótům	Gót	k1gMnPc3	Gót
povoleno	povolen	k2eAgNnSc1d1	povoleno
překročit	překročit	k5eAaPmF	překročit
Dunaj	Dunaj	k1gInSc4	Dunaj
a	a	k8xC	a
usadit	usadit	k5eAaPmF	usadit
se	se	k3xPyFc4	se
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
nesnesitelným	snesitelný	k2eNgFnPc3d1	nesnesitelná
životním	životní	k2eAgFnPc3d1	životní
podmínkám	podmínka	k1gFnPc3	podmínka
brzy	brzy	k6eAd1	brzy
vzbouřili	vzbouřit	k5eAaPmAgMnP	vzbouřit
<g/>
.	.	kIx.	.
</s>
<s>
Valens	Valens	k6eAd1	Valens
se	se	k3xPyFc4	se
je	on	k3xPp3gFnPc4	on
pokusil	pokusit	k5eAaPmAgMnS	pokusit
zpacifikovat	zpacifikovat	k5eAaPmF	zpacifikovat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
378	[number]	k4	378
byl	být	k5eAaImAgMnS	být
poražen	porazit	k5eAaPmNgMnS	porazit
a	a	k8xC	a
zabit	zabít	k5eAaPmNgMnS	zabít
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Adrianopole	Adrianopole	k1gFnSc2	Adrianopole
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Kann	Kanny	k1gFnPc2	Kanny
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
druhou	druhý	k4xOgFnSc4	druhý
nejtěžší	těžký	k2eAgFnSc4d3	nejtěžší
porážku	porážka	k1gFnSc4	porážka
Římanů	Říman	k1gMnPc2	Říman
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozdělení	rozdělení	k1gNnSc1	rozdělení
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
křesťanství	křesťanství	k1gNnSc2	křesťanství
jako	jako	k9	jako
státní	státní	k2eAgNnSc1d1	státní
náboženství	náboženství	k1gNnSc1	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Nový	nový	k2eAgMnSc1d1	nový
císař	císař	k1gMnSc1	císař
Theodosius	Theodosius	k1gMnSc1	Theodosius
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
uzavřít	uzavřít	k5eAaPmF	uzavřít
s	s	k7c7	s
Góty	Gót	k1gMnPc7	Gót
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jim	on	k3xPp3gMnPc3	on
umožnila	umožnit	k5eAaPmAgFnS	umožnit
pobývat	pobývat	k5eAaImF	pobývat
na	na	k7c6	na
římském	římský	k2eAgNnSc6d1	římské
území	území	k1gNnSc6	území
jako	jako	k9	jako
foederati	foederat	k1gMnPc1	foederat
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
barbarů	barbar	k1gMnPc2	barbar
na	na	k7c6	na
římském	římský	k2eAgNnSc6d1	římské
území	území	k1gNnSc6	území
se	se	k3xPyFc4	se
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
okamžiku	okamžik	k1gInSc2	okamžik
stala	stát	k5eAaPmAgFnS	stát
trvalým	trvalý	k2eAgInSc7d1	trvalý
jevem	jev	k1gInSc7	jev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
394	[number]	k4	394
se	se	k3xPyFc4	se
Theodosius	Theodosius	k1gMnSc1	Theodosius
po	po	k7c6	po
zdolání	zdolání	k1gNnSc6	zdolání
poslední	poslední	k2eAgFnSc2d1	poslední
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
uzurpací	uzurpace	k1gFnPc2	uzurpace
a	a	k8xC	a
vzpour	vzpoura	k1gFnPc2	vzpoura
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgFnPc3	jenž
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
posledním	poslední	k2eAgMnSc7d1	poslední
císařem	císař	k1gMnSc7	císař
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vládl	vládnout	k5eAaImAgMnS	vládnout
nad	nad	k7c7	nad
celým	celý	k2eAgNnSc7d1	celé
územím	území	k1gNnSc7	území
impéria	impérium	k1gNnSc2	impérium
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jeho	on	k3xPp3gNnSc2	on
panování	panování	k1gNnSc2	panování
bylo	být	k5eAaImAgNnS	být
křesťanství	křesťanství	k1gNnSc4	křesťanství
oficiálně	oficiálně	k6eAd1	oficiálně
ustanoveno	ustanovit	k5eAaPmNgNnS	ustanovit
za	za	k7c4	za
jediné	jediný	k2eAgNnSc4d1	jediné
státní	státní	k2eAgNnSc4d1	státní
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Theodosiově	Theodosiův	k2eAgFnSc6d1	Theodosiova
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
395	[number]	k4	395
byla	být	k5eAaImAgFnS	být
římská	římský	k2eAgFnSc1d1	římská
říše	říše	k1gFnSc1	říše
rozdělena	rozdělen	k2eAgFnSc1d1	rozdělena
mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gMnPc4	jeho
dva	dva	k4xCgMnPc4	dva
neschopné	schopný	k2eNgMnPc4d1	neschopný
syny	syn	k1gMnPc4	syn
<g/>
:	:	kIx,	:
Honorius	Honorius	k1gMnSc1	Honorius
vládl	vládnout	k5eAaImAgMnS	vládnout
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Arcadiovi	Arcadius	k1gMnSc3	Arcadius
byl	být	k5eAaImAgInS	být
svěřen	svěřen	k2eAgInSc1d1	svěřen
Východ	východ	k1gInSc1	východ
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
toto	tento	k3xDgNnSc1	tento
rozdělení	rozdělení	k1gNnSc1	rozdělení
se	se	k3xPyFc4	se
postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
jako	jako	k9	jako
definitivní	definitivní	k2eAgNnSc1d1	definitivní
<g/>
,	,	kIx,	,
myšlenka	myšlenka	k1gFnSc1	myšlenka
říšské	říšský	k2eAgFnSc2d1	říšská
jednoty	jednota	k1gFnSc2	jednota
přesto	přesto	k8xC	přesto
trvala	trvat	k5eAaImAgFnS	trvat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
–	–	k?	–
zákony	zákon	k1gInPc1	zákon
vydané	vydaný	k2eAgInPc1d1	vydaný
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
císařů	císař	k1gMnPc2	císař
byly	být	k5eAaImAgFnP	být
proto	proto	k6eAd1	proto
běžně	běžně	k6eAd1	běžně
uznávány	uznávat	k5eAaImNgInP	uznávat
i	i	k9	i
druhým	druhý	k4xOgMnSc7	druhý
vládcem	vládce	k1gMnSc7	vládce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zánik	zánik	k1gInSc4	zánik
antického	antický	k2eAgNnSc2d1	antické
impéria	impérium	k1gNnSc2	impérium
===	===	k?	===
</s>
</p>
<p>
<s>
Východořímská	východořímský	k2eAgFnSc1d1	Východořímská
říše	říše	k1gFnSc1	říše
byla	být	k5eAaImAgFnS	být
ekonomicky	ekonomicky	k6eAd1	ekonomicky
konsolidovanější	konsolidovaný	k2eAgFnSc1d2	konsolidovanější
a	a	k8xC	a
hustěji	husto	k6eAd2	husto
obydlenou	obydlený	k2eAgFnSc7d1	obydlená
polovinou	polovina	k1gFnSc7	polovina
impéria	impérium	k1gNnSc2	impérium
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
bez	bez	k7c2	bez
vážnější	vážní	k2eAgFnSc2d2	vážnější
újmy	újma	k1gFnSc2	újma
přežila	přežít	k5eAaPmAgFnS	přežít
chaos	chaos	k1gInSc4	chaos
období	období	k1gNnSc2	období
stěhování	stěhování	k1gNnSc2	stěhování
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgMnSc3	ten
západořímská	západořímský	k2eAgFnSc1d1	Západořímská
říše	říše	k1gFnSc1	říše
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pozvolna	pozvolna	k6eAd1	pozvolna
upadala	upadat	k5eAaImAgFnS	upadat
<g/>
.	.	kIx.	.
</s>
<s>
Postup	postup	k1gInSc1	postup
Hunů	Hun	k1gMnPc2	Hun
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
spustil	spustit	k5eAaPmAgInS	spustit
dominový	dominový	k2eAgInSc1d1	dominový
efekt	efekt	k1gInSc1	efekt
pohybu	pohyb	k1gInSc2	pohyb
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zásadně	zásadně	k6eAd1	zásadně
změnil	změnit	k5eAaPmAgInS	změnit
podobu	podoba	k1gFnSc4	podoba
celého	celý	k2eAgInSc2d1	celý
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
desetiletích	desetiletí	k1gNnPc6	desetiletí
následujících	následující	k2eAgNnPc6d1	následující
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
u	u	k7c2	u
Adrianopole	Adrianopole	k1gFnSc2	Adrianopole
ztratila	ztratit	k5eAaPmAgFnS	ztratit
říše	říše	k1gFnSc1	říše
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
většinou	většina	k1gFnSc7	většina
svých	svůj	k3xOyFgFnPc2	svůj
západních	západní	k2eAgFnPc2d1	západní
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
zabránili	zabránit	k5eAaPmAgMnP	zabránit
vpádům	vpád	k1gInPc3	vpád
Germánů	Germán	k1gMnPc2	Germán
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
Římané	Říman	k1gMnPc1	Říman
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
století	století	k1gNnSc2	století
legie	legie	k1gFnSc2	legie
z	z	k7c2	z
rýnské	rýnský	k2eAgFnSc2d1	Rýnská
hranice	hranice	k1gFnSc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Nechráněný	chráněný	k2eNgInSc1d1	nechráněný
Rýn	Rýn	k1gInSc1	Rýn
poté	poté	k6eAd1	poté
v	v	k7c6	v
roce	rok	k1gInSc6	rok
406	[number]	k4	406
překročily	překročit	k5eAaPmAgInP	překročit
barbarské	barbarský	k2eAgInPc1d1	barbarský
kmeny	kmen	k1gInPc1	kmen
Svébů	Svéb	k1gMnPc2	Svéb
<g/>
,	,	kIx,	,
Vandalů	Vandal	k1gMnPc2	Vandal
a	a	k8xC	a
Alanů	Alan	k1gMnPc2	Alan
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
během	během	k7c2	během
několika	několik	k4yIc2	několik
málo	málo	k6eAd1	málo
let	let	k1gInSc4	let
vyvrátily	vyvrátit	k5eAaPmAgFnP	vyvrátit
římskou	římský	k2eAgFnSc4d1	římská
správu	správa	k1gFnSc4	správa
v	v	k7c6	v
Galii	Galie	k1gFnSc6	Galie
a	a	k8xC	a
v	v	k7c6	v
Hispánii	Hispánie	k1gFnSc6	Hispánie
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
Británie	Británie	k1gFnSc1	Británie
byla	být	k5eAaImAgFnS	být
ponechána	ponechat	k5eAaPmNgFnS	ponechat
svému	svůj	k3xOyFgInSc3	svůj
osudu	osud	k1gInSc3	osud
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
410	[number]	k4	410
vydrancovali	vydrancovat	k5eAaPmAgMnP	vydrancovat
Vizigóti	Vizigót	k1gMnPc1	Vizigót
vedení	vedení	k1gNnSc2	vedení
náčelníkem	náčelník	k1gMnSc7	náčelník
Alarichem	Alarich	k1gMnSc7	Alarich
Řím	Řím	k1gInSc4	Řím
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
tvrdý	tvrdý	k2eAgInSc4d1	tvrdý
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
římskou	římský	k2eAgFnSc4d1	římská
psychiku	psychika	k1gFnSc4	psychika
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
byli	být	k5eAaImAgMnP	být
sice	sice	k8xC	sice
Vizigóti	Vizigót	k1gMnPc1	Vizigót
vytlačeni	vytlačit	k5eAaPmNgMnP	vytlačit
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
do	do	k7c2	do
jižní	jižní	k2eAgFnSc2d1	jižní
Galie	Galie	k1gFnSc2	Galie
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
však	však	k9	však
dále	daleko	k6eAd2	daleko
představovali	představovat	k5eAaImAgMnP	představovat
závažné	závažný	k2eAgNnSc4d1	závažné
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
Vandalové	Vandal	k1gMnPc1	Vandal
pronikli	proniknout	k5eAaPmAgMnP	proniknout
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
tuto	tento	k3xDgFnSc4	tento
pro	pro	k7c4	pro
říši	říše	k1gFnSc4	říše
tolik	tolik	k6eAd1	tolik
důležitou	důležitý	k2eAgFnSc4d1	důležitá
provincii	provincie	k1gFnSc4	provincie
opanovali	opanovat	k5eAaPmAgMnP	opanovat
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Kartága	Kartágo	k1gNnSc2	Kartágo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
438	[number]	k4	438
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
455	[number]	k4	455
se	se	k3xPyFc4	se
jim	on	k3xPp3gFnPc3	on
pak	pak	k6eAd1	pak
podařilo	podařit	k5eAaPmAgNnS	podařit
podruhé	podruhé	k6eAd1	podruhé
vyplenit	vyplenit	k5eAaPmF	vyplenit
Řím	Řím	k1gInSc1	Řím
(	(	kIx(	(
<g/>
jejich	jejich	k3xOp3gNnSc2	jejich
jména	jméno	k1gNnSc2	jméno
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
užívá	užívat	k5eAaImIp3nS	užívat
jako	jako	k8xC	jako
nadávky	nadávka	k1gFnPc4	nadávka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolaps	kolaps	k1gInSc1	kolaps
západní	západní	k2eAgFnSc2d1	západní
říše	říš	k1gFnSc2	říš
měl	mít	k5eAaImAgMnS	mít
řadu	řada	k1gFnSc4	řada
příčin	příčina	k1gFnPc2	příčina
<g/>
.	.	kIx.	.
</s>
<s>
Jaké	jaký	k3yIgInPc1	jaký
procesy	proces	k1gInPc1	proces
vedly	vést	k5eAaImAgInP	vést
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
konečném	konečný	k2eAgInSc6d1	konečný
důsledku	důsledek	k1gInSc6	důsledek
k	k	k7c3	k
přeměně	přeměna	k1gFnSc3	přeměna
západořímské	západořímský	k2eAgFnSc2d1	Západořímská
říše	říš	k1gFnSc2	říš
v	v	k7c4	v
řadu	řada	k1gFnSc4	řada
germánských	germánský	k2eAgNnPc2d1	germánské
království	království	k1gNnPc2	království
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
velice	velice	k6eAd1	velice
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
předmětem	předmět	k1gInSc7	předmět
diskuzí	diskuze	k1gFnPc2	diskuze
historiků	historik	k1gMnPc2	historik
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
důvodů	důvod	k1gInPc2	důvod
je	být	k5eAaImIp3nS	být
pokládána	pokládán	k2eAgFnSc1d1	pokládána
barbarizace	barbarizace	k1gFnSc1	barbarizace
římského	římský	k2eAgNnSc2d1	římské
vojska	vojsko	k1gNnSc2	vojsko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
převážně	převážně	k6eAd1	převážně
germánskými	germánský	k2eAgMnPc7d1	germánský
žoldnéři	žoldnéř	k1gMnPc7	žoldnéř
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
Římanů	Říman	k1gMnPc2	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
velikost	velikost	k1gFnSc1	velikost
armády	armáda	k1gFnSc2	armáda
nebyla	být	k5eNaImAgFnS	být
dostačující	dostačující	k2eAgFnSc1d1	dostačující
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dokázala	dokázat	k5eAaPmAgFnS	dokázat
zabezpečit	zabezpečit	k5eAaPmF	zabezpečit
hranice	hranice	k1gFnSc1	hranice
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgFnSc1d1	státní
správa	správa	k1gFnSc1	správa
se	se	k3xPyFc4	se
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
v	v	k7c6	v
rozkladu	rozklad	k1gInSc6	rozklad
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
ekonomika	ekonomika	k1gFnSc1	ekonomika
impéria	impérium	k1gNnSc2	impérium
byla	být	k5eAaImAgFnS	být
rozvrácena	rozvrátit	k5eAaPmNgFnS	rozvrátit
příliš	příliš	k6eAd1	příliš
vysokými	vysoký	k2eAgFnPc7d1	vysoká
daněmi	daň	k1gFnPc7	daň
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zbavovaly	zbavovat	k5eAaImAgFnP	zbavovat
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
loajality	loajalita	k1gFnSc2	loajalita
k	k	k7c3	k
římskému	římský	k2eAgInSc3d1	římský
státu	stát	k1gInSc3	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Třebaže	třebaže	k8xS	třebaže
v	v	k7c6	v
roce	rok	k1gInSc6	rok
451	[number]	k4	451
dokázali	dokázat	k5eAaPmAgMnP	dokázat
Římané	Říman	k1gMnPc1	Říman
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
Vizigóty	Vizigót	k1gMnPc7	Vizigót
a	a	k8xC	a
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Aetia	Aetius	k1gMnSc2	Aetius
přemoci	přemoct	k5eAaPmF	přemoct
Huny	Hun	k1gMnPc4	Hun
vedené	vedený	k2eAgFnSc2d1	vedená
Attilou	Attila	k1gMnSc7	Attila
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Katalaunských	Katalaunský	k2eAgNnPc6d1	Katalaunský
polích	pole	k1gNnPc6	pole
<g/>
,	,	kIx,	,
římská	římský	k2eAgFnSc1d1	římská
říše	říše	k1gFnSc1	říše
na	na	k7c6	na
západě	západ	k1gInSc6	západ
spěla	spět	k5eAaImAgFnS	spět
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
rychlému	rychlý	k2eAgInSc3d1	rychlý
konci	konec	k1gInSc3	konec
<g/>
.	.	kIx.	.
</s>
<s>
Slabí	slabý	k2eAgMnPc1d1	slabý
císaři	císař	k1gMnPc1	císař
nebyli	být	k5eNaImAgMnP	být
schopní	schopný	k2eAgMnPc1d1	schopný
zabránit	zabránit	k5eAaPmF	zabránit
nevyhnutelnému	vyhnutelný	k2eNgInSc3d1	nevyhnutelný
rozvratu	rozvrat	k1gInSc3	rozvrat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
byli	být	k5eAaImAgMnP	být
často	často	k6eAd1	často
pouhými	pouhý	k2eAgFnPc7d1	pouhá
loutkami	loutka	k1gFnPc7	loutka
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
velitelů	velitel	k1gMnPc2	velitel
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
476	[number]	k4	476
Germán	Germán	k1gMnSc1	Germán
Odoaker	Odoaker	k1gMnSc1	Odoaker
sesadil	sesadit	k5eAaPmAgMnS	sesadit
císaře	císař	k1gMnSc4	císař
Romula	Romulus	k1gMnSc4	Romulus
Augusta	August	k1gMnSc4	August
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
okamžik	okamžik	k1gInSc4	okamžik
zániku	zánik	k1gInSc2	zánik
západořímské	západořímský	k2eAgFnSc2d1	Západořímská
říše	říš	k1gFnSc2	říš
(	(	kIx(	(
<g/>
posledním	poslední	k2eAgMnSc7d1	poslední
uznaným	uznaný	k2eAgMnSc7d1	uznaný
císařem	císař	k1gMnSc7	císař
Západu	západ	k1gInSc2	západ
byl	být	k5eAaImAgMnS	být
Julius	Julius	k1gMnSc1	Julius
Nepos	Nepos	k1gMnSc1	Nepos
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
žil	žít	k5eAaImAgMnS	žít
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
480	[number]	k4	480
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
sám	sám	k3xTgMnSc1	sám
Odoaker	Odoaker	k1gMnSc1	Odoaker
se	se	k3xPyFc4	se
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c7	za
"	"	kIx"	"
<g/>
Germána	Germán	k1gMnSc2	Germán
v	v	k7c6	v
římských	římský	k2eAgFnPc6d1	římská
službách	služba	k1gFnPc6	služba
<g/>
"	"	kIx"	"
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
vládu	vláda	k1gFnSc4	vláda
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
interpretovat	interpretovat	k5eAaBmF	interpretovat
jako	jako	k9	jako
správu	správa	k1gFnSc4	správa
Itálie	Itálie	k1gFnSc2	Itálie
jménem	jméno	k1gNnSc7	jméno
císaře	císař	k1gMnSc2	císař
v	v	k7c6	v
Konstantinopoli	Konstantinopol	k1gInSc6	Konstantinopol
<g/>
.	.	kIx.	.
</s>
<s>
Čistě	čistě	k6eAd1	čistě
formálně	formálně	k6eAd1	formálně
byla	být	k5eAaImAgFnS	být
tedy	tedy	k9	tedy
Itálie	Itálie	k1gFnSc1	Itálie
stále	stále	k6eAd1	stále
součástí	součást	k1gFnSc7	součást
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
Odoakerův	Odoakerův	k2eAgMnSc1d1	Odoakerův
nástupce	nástupce	k1gMnSc1	nástupce
<g/>
,	,	kIx,	,
ostrogótský	ostrogótský	k2eAgMnSc1d1	ostrogótský
král	král	k1gMnSc1	král
Theodorich	Theodorich	k1gMnSc1	Theodorich
Veliký	veliký	k2eAgMnSc1d1	veliký
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
získat	získat	k5eAaPmF	získat
uznání	uznání	k1gNnSc4	uznání
východního	východní	k2eAgMnSc2d1	východní
císaře	císař	k1gMnSc2	císař
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Západu	západ	k1gInSc2	západ
východní	východní	k2eAgFnSc2d1	východní
říše	říš	k1gFnSc2	říš
nebyla	být	k5eNaImAgNnP	být
germánskými	germánský	k2eAgFnPc7d1	germánská
nájezdy	nájezd	k1gInPc7	nájezd
zasažena	zasáhnout	k5eAaPmNgFnS	zasáhnout
tak	tak	k6eAd1	tak
ničivě	ničivě	k6eAd1	ničivě
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
disponovala	disponovat	k5eAaBmAgFnS	disponovat
mnohem	mnohem	k6eAd1	mnohem
většími	veliký	k2eAgInPc7d2	veliký
finančními	finanční	k2eAgInPc7d1	finanční
zdroji	zdroj	k1gInPc7	zdroj
a	a	k8xC	a
především	především	k6eAd1	především
obratnou	obratný	k2eAgFnSc7d1	obratná
diplomacií	diplomacie	k1gFnSc7	diplomacie
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
také	také	k9	také
geograficky	geograficky	k6eAd1	geograficky
lépe	dobře	k6eAd2	dobře
hájitelné	hájitelný	k2eAgFnPc4d1	hájitelná
hranice	hranice	k1gFnPc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Obzvláště	obzvláště	k6eAd1	obzvláště
mocné	mocný	k2eAgNnSc4d1	mocné
anatolské	anatolský	k2eAgNnSc4d1	anatolský
pohoří	pohoří	k1gNnSc4	pohoří
Taurus	Taurus	k1gInSc1	Taurus
a	a	k8xC	a
Propontida	Propontida	k1gFnSc1	Propontida
tvořily	tvořit	k5eAaImAgFnP	tvořit
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
překážku	překážka	k1gFnSc4	překážka
nepřátelským	přátelský	k2eNgFnPc3d1	nepřátelská
invazím	invaze	k1gFnPc3	invaze
<g/>
.	.	kIx.	.
</s>
<s>
Hunům	Hun	k1gMnPc3	Hun
ani	ani	k8xC	ani
Germánům	Germán	k1gMnPc3	Germán
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nepodařilo	podařit	k5eNaPmAgNnS	podařit
překročit	překročit	k5eAaPmF	překročit
Helléspont	Helléspont	k1gInSc4	Helléspont
a	a	k8xC	a
bohaté	bohatý	k2eAgFnSc2d1	bohatá
provincie	provincie	k1gFnSc2	provincie
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
a	a	k8xC	a
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
proto	proto	k8xC	proto
zůstaly	zůstat	k5eAaPmAgInP	zůstat
ušetřeny	ušetřen	k2eAgInPc1d1	ušetřen
jejich	jejich	k3xOp3gInPc2	jejich
vpádů	vpád	k1gInPc2	vpád
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
germánského	germánský	k2eAgInSc2d1	germánský
elementu	element	k1gInSc2	element
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
osudově	osudově	k6eAd1	osudově
přispěl	přispět	k5eAaPmAgMnS	přispět
ke	k	k7c3	k
zhroucení	zhroucení	k1gNnSc3	zhroucení
Západu	západ	k1gInSc2	západ
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
ještě	ještě	k9	ještě
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
eliminován	eliminovat	k5eAaBmNgInS	eliminovat
<g/>
.	.	kIx.	.
</s>
<s>
Příznačné	příznačný	k2eAgNnSc1d1	příznačné
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
mohlo	moct	k5eAaImAgNnS	moct
stát	stát	k5eAaPmF	stát
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
jiných	jiný	k2eAgMnPc2d1	jiný
barbarů	barbar	k1gMnPc2	barbar
–	–	k?	–
Isaurů	Isaur	k1gInPc2	Isaur
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
těžké	těžký	k2eAgNnSc4d1	těžké
a	a	k8xC	a
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
vítězné	vítězný	k2eAgInPc4d1	vítězný
boje	boj	k1gInPc4	boj
s	s	k7c7	s
Huny	Hun	k1gMnPc7	Hun
a	a	k8xC	a
Góty	Gót	k1gMnPc7	Gót
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
nepostihl	postihnout	k5eNaPmAgInS	postihnout
východní	východní	k2eAgFnSc3d1	východní
říši	říš	k1gFnSc3	říš
rozklad	rozklad	k1gInSc1	rozklad
tolik	tolik	k6eAd1	tolik
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
pro	pro	k7c4	pro
západní	západní	k2eAgFnSc4d1	západní
provincie	provincie	k1gFnPc4	provincie
v	v	k7c4	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Justiniána	Justinián	k1gMnSc2	Justinián
I.	I.	kA	I.
<g/>
,	,	kIx,	,
posledního	poslední	k2eAgMnSc2d1	poslední
císaře	císař	k1gMnSc2	císař
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
mateřským	mateřský	k2eAgInSc7d1	mateřský
jazykem	jazyk	k1gInSc7	jazyk
byla	být	k5eAaImAgFnS	být
latina	latina	k1gFnSc1	latina
<g/>
,	,	kIx,	,
dobyl	dobýt	k5eAaPmAgMnS	dobýt
východořímský	východořímský	k2eAgMnSc1d1	východořímský
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
Belisar	Belisar	k1gMnSc1	Belisar
zpět	zpět	k6eAd1	zpět
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
Západu	západ	k1gInSc2	západ
(	(	kIx(	(
<g/>
severní	severní	k2eAgFnSc1d1	severní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgNnSc1d1	jižní
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téže	týž	k3xTgFnSc6	týž
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
říše	říše	k1gFnSc1	říše
jen	jen	k9	jen
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
vypětím	vypětí	k1gNnSc7	vypětí
vypořádávala	vypořádávat	k5eAaImAgFnS	vypořádávat
s	s	k7c7	s
agresí	agrese	k1gFnSc7	agrese
Sásánovců	Sásánovec	k1gMnPc2	Sásánovec
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
východních	východní	k2eAgFnPc6d1	východní
hranicích	hranice	k1gFnPc6	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
útoky	útok	k1gInPc1	útok
nabývaly	nabývat	k5eAaImAgInP	nabývat
od	od	k7c2	od
čtyřicátých	čtyřicátý	k4xOgNnPc2	čtyřicátý
let	léto	k1gNnPc2	léto
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
intenzitě	intenzita	k1gFnSc6	intenzita
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Peršané	Peršan	k1gMnPc1	Peršan
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Západu	západ	k1gInSc2	západ
cítili	cítit	k5eAaImAgMnP	cítit
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
obnovení	obnovení	k1gNnSc4	obnovení
říše	říš	k1gFnSc2	říš
Achaimenovců	Achaimenovec	k1gMnPc2	Achaimenovec
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
jejím	její	k3xOp3gInSc6	její
někdejším	někdejší	k2eAgInSc6d1	někdejší
rozsahu	rozsah	k1gInSc6	rozsah
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
mírové	mírový	k2eAgFnSc2d1	mírová
koexistence	koexistence	k1gFnSc2	koexistence
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
státy	stát	k1gInPc7	stát
tím	ten	k3xDgNnSc7	ten
skončilo	skončit	k5eAaPmAgNnS	skončit
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
nastala	nastat	k5eAaPmAgFnS	nastat
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
a	a	k8xC	a
namáhavá	namáhavý	k2eAgFnSc1d1	namáhavá
etapa	etapa	k1gFnSc1	etapa
římsko-perských	římskoerský	k2eAgFnPc2d1	římsko-perský
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
trvala	trvat	k5eAaImAgFnS	trvat
prakticky	prakticky	k6eAd1	prakticky
celé	celý	k2eAgNnSc4d1	celé
jedno	jeden	k4xCgNnSc4	jeden
století	století	k1gNnSc4	století
<g/>
.	.	kIx.	.
</s>
<s>
Justinián	Justinián	k1gMnSc1	Justinián
se	se	k3xPyFc4	se
navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
stal	stát	k5eAaPmAgMnS	stát
nejmocnějším	mocný	k2eAgMnSc7d3	nejmocnější
vládcem	vládce	k1gMnSc7	vládce
Středomoří	středomoří	k1gNnSc2	středomoří
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
východořímská	východořímský	k2eAgFnSc1d1	Východořímská
říše	říše	k1gFnSc1	říše
kontrolovala	kontrolovat	k5eAaImAgFnS	kontrolovat
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
kdysi	kdysi	k6eAd1	kdysi
římského	římský	k2eAgNnSc2d1	římské
území	území	k1gNnSc2	území
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
Galie	Galie	k1gFnSc2	Galie
a	a	k8xC	a
většiny	většina	k1gFnSc2	většina
Hispánie	Hispánie	k1gFnSc2	Hispánie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
Justiniánově	Justiniánův	k2eAgFnSc6d1	Justiniánova
smrti	smrt	k1gFnSc6	smrt
(	(	kIx(	(
<g/>
565	[number]	k4	565
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
západní	západní	k2eAgNnPc1d1	západní
území	území	k1gNnPc1	území
ukázala	ukázat	k5eAaPmAgNnP	ukázat
jako	jako	k9	jako
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
neudržitelná	udržitelný	k2eNgFnSc1d1	neudržitelná
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
568	[number]	k4	568
si	se	k3xPyFc3	se
Langobardi	Langobard	k1gMnPc1	Langobard
podmanili	podmanit	k5eAaPmAgMnP	podmanit
téměř	téměř	k6eAd1	téměř
celou	celý	k2eAgFnSc4d1	celá
Itálii	Itálie	k1gFnSc4	Itálie
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
jižní	jižní	k2eAgFnSc2d1	jižní
Hispánie	Hispánie	k1gFnSc2	Hispánie
se	se	k3xPyFc4	se
po	po	k7c6	po
několika	několik	k4yIc6	několik
desetiletích	desetiletí	k1gNnPc6	desetiletí
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
znovu	znovu	k6eAd1	znovu
v	v	k7c6	v
moci	moc	k1gFnSc6	moc
Vizigótů	Vizigót	k1gMnPc2	Vizigót
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Východořímská	východořímský	k2eAgFnSc1d1	Východořímská
říše	říše	k1gFnSc1	říše
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zmítána	zmítat	k5eAaImNgFnS	zmítat
náboženskými	náboženský	k2eAgInPc7d1	náboženský
spory	spor	k1gInPc7	spor
mezi	mezi	k7c7	mezi
ortodoxními	ortodoxní	k2eAgMnPc7d1	ortodoxní
křesťany	křesťan	k1gMnPc7	křesťan
a	a	k8xC	a
monofyzity	monofyzita	k1gMnPc7	monofyzita
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
společně	společně	k6eAd1	společně
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
daňovou	daňový	k2eAgFnSc7d1	daňová
zátěží	zátěž	k1gFnSc7	zátěž
způsobenou	způsobený	k2eAgFnSc4d1	způsobená
ustavičnými	ustavičný	k2eAgFnPc7d1	ustavičná
válkami	válka	k1gFnPc7	válka
vzbuzovalo	vzbuzovat	k5eAaImAgNnS	vzbuzovat
nespokojenost	nespokojenost	k1gFnSc4	nespokojenost
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
a	a	k8xC	a
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
oslabení	oslabení	k1gNnSc3	oslabení
jeho	jeho	k3xOp3gFnSc2	jeho
loajality	loajalita	k1gFnSc2	loajalita
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
a	a	k8xC	a
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
si	se	k3xPyFc3	se
Sásánovci	Sásánovec	k1gMnPc1	Sásánovec
dočasně	dočasně	k6eAd1	dočasně
podrobili	podrobit	k5eAaPmAgMnP	podrobit
velké	velký	k2eAgFnPc4d1	velká
části	část	k1gFnPc4	část
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Perské	perský	k2eAgFnSc2d1	perská
armády	armáda	k1gFnSc2	armáda
velkokrále	velkokrála	k1gFnSc3	velkokrála
Husrava	Husrava	k1gFnSc1	Husrava
II	II	kA	II
<g/>
.	.	kIx.	.
dokonce	dokonce	k9	dokonce
dvakrát	dvakrát	k6eAd1	dvakrát
postoupily	postoupit	k5eAaPmAgFnP	postoupit
až	až	k9	až
k	k	k7c3	k
samotné	samotný	k2eAgFnSc3d1	samotná
Konstantinopoli	Konstantinopol	k1gInSc3	Konstantinopol
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
626	[number]	k4	626
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Avary	Avar	k1gMnPc7	Avar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Peršané	Peršan	k1gMnPc1	Peršan
navíc	navíc	k6eAd1	navíc
z	z	k7c2	z
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
odcizili	odcizit	k5eAaPmAgMnP	odcizit
svatý	svatý	k1gMnSc1	svatý
kříž	kříž	k1gInSc4	kříž
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
údajně	údajně	k6eAd1	údajně
nalezla	nalézt	k5eAaBmAgFnS	nalézt
matka	matka	k1gFnSc1	matka
císaře	císař	k1gMnSc2	císař
Konstantina	Konstantin	k1gMnSc2	Konstantin
Helena	Helena	k1gFnSc1	Helena
<g/>
,	,	kIx,	,
a	a	k8xC	a
jenž	jenž	k3xRgMnSc1	jenž
představoval	představovat	k5eAaImAgMnS	představovat
nejposvátnější	posvátný	k2eAgFnSc4d3	nejposvátnější
relikvii	relikvie	k1gFnSc4	relikvie
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Herakleios	Herakleios	k1gMnSc1	Herakleios
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
a	a	k8xC	a
náročné	náročný	k2eAgFnSc6d1	náročná
válce	válka	k1gFnSc6	válka
nakonec	nakonec	k6eAd1	nakonec
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
vyčerpaná	vyčerpaný	k2eAgFnSc1d1	vyčerpaná
říše	říše	k1gFnSc1	říše
nedokázala	dokázat	k5eNaPmAgFnS	dokázat
čelit	čelit	k5eAaImF	čelit
následné	následný	k2eAgFnSc3d1	následná
arabské	arabský	k2eAgFnSc3d1	arabská
expanzi	expanze	k1gFnSc3	expanze
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
definitivně	definitivně	k6eAd1	definitivně
pozbyla	pozbýt	k5eAaPmAgFnS	pozbýt
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
Sýrií	Sýrie	k1gFnSc7	Sýrie
a	a	k8xC	a
Afrikou	Afrika	k1gFnSc7	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Obzvláště	obzvláště	k6eAd1	obzvláště
ztráta	ztráta	k1gFnSc1	ztráta
bohatého	bohatý	k2eAgInSc2d1	bohatý
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgInS	vzdát
Arabům	Arab	k1gMnPc3	Arab
téměř	téměř	k6eAd1	téměř
bez	bez	k7c2	bez
boje	boj	k1gInSc2	boj
zradou	zrada	k1gFnSc7	zrada
monofyzitského	monofyzitský	k2eAgMnSc2d1	monofyzitský
alexandrijského	alexandrijský	k2eAgMnSc2d1	alexandrijský
patriarchy	patriarcha	k1gMnSc2	patriarcha
Kýra	Kýrus	k1gMnSc2	Kýrus
<g/>
,	,	kIx,	,
východořímskou	východořímský	k2eAgFnSc4d1	Východořímská
říši	říše	k1gFnSc4	říše
rozhodným	rozhodný	k2eAgInSc7d1	rozhodný
způsobem	způsob	k1gInSc7	způsob
oslabila	oslabit	k5eAaPmAgFnS	oslabit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Herakleios	Herakleios	k1gInSc1	Herakleios
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
římskými	římský	k2eAgFnPc7d1	římská
tradicemi	tradice	k1gFnPc7	tradice
přijal	přijmout	k5eAaPmAgInS	přijmout
namísto	namísto	k7c2	namísto
latinského	latinský	k2eAgInSc2d1	latinský
titulu	titul	k1gInSc2	titul
augustus	augustus	k1gInSc1	augustus
starý	starý	k2eAgInSc1d1	starý
řecký	řecký	k2eAgInSc4d1	řecký
královský	královský	k2eAgInSc4d1	královský
titul	titul	k1gInSc4	titul
basileus	basileus	k1gMnSc1	basileus
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
povýšil	povýšit	k5eAaPmAgMnS	povýšit
řečtinu	řečtina	k1gFnSc4	řečtina
na	na	k7c4	na
úřední	úřední	k2eAgInSc4d1	úřední
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Říše	říše	k1gFnSc1	říše
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
postupně	postupně	k6eAd1	postupně
ztratila	ztratit	k5eAaPmAgFnS	ztratit
svůj	svůj	k3xOyFgInSc4	svůj
římský	římský	k2eAgInSc4d1	římský
a	a	k8xC	a
antický	antický	k2eAgInSc4d1	antický
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Hluboké	hluboký	k2eAgFnPc1d1	hluboká
vnitřní	vnitřní	k2eAgFnPc1d1	vnitřní
a	a	k8xC	a
vnější	vnější	k2eAgFnPc1d1	vnější
změny	změna	k1gFnPc1	změna
vedly	vést	k5eAaImAgFnP	vést
tudíž	tudíž	k8xC	tudíž
také	také	k6eAd1	také
na	na	k7c6	na
Východě	východ	k1gInSc6	východ
k	k	k7c3	k
nástupu	nástup	k1gInSc3	nástup
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Východořímská	východořímský	k2eAgFnSc1d1	Východořímská
říše	říše	k1gFnSc1	říše
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Konstantinopolí	Konstantinopole	k1gFnPc2	Konstantinopole
přetrvala	přetrvat	k5eAaPmAgFnS	přetrvat
až	až	k6eAd1	až
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
se	se	k3xPyFc4	se
její	její	k3xOp3gNnSc1	její
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
nadále	nadále	k6eAd1	nadále
považovalo	považovat	k5eAaImAgNnS	považovat
za	za	k7c4	za
Rhomaioi	Rhomaioe	k1gFnSc4	Rhomaioe
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
Římany	Říman	k1gMnPc4	Říman
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vnitřních	vnitřní	k2eAgFnPc6d1	vnitřní
strukturách	struktura	k1gFnPc6	struktura
říše	říš	k1gFnSc2	říš
došlo	dojít	k5eAaPmAgNnS	dojít
po	po	k7c6	po
roce	rok	k1gInSc6	rok
640	[number]	k4	640
k	k	k7c3	k
zásadní	zásadní	k2eAgFnSc3d1	zásadní
transformaci	transformace	k1gFnSc3	transformace
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
celkem	celkem	k6eAd1	celkem
oprávněné	oprávněný	k2eAgNnSc1d1	oprávněné
hovořit	hovořit	k5eAaImF	hovořit
nejpozději	pozdě	k6eAd3	pozdě
od	od	k7c2	od
tohoto	tento	k3xDgNnSc2	tento
data	datum	k1gNnSc2	datum
o	o	k7c6	o
východořímské	východořímský	k2eAgFnSc6d1	Východořímská
říši	říš	k1gFnSc6	říš
jako	jako	k8xS	jako
o	o	k7c6	o
Byzanci	Byzanc	k1gFnSc6	Byzanc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
Říma	Řím	k1gInSc2	Řím
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Römisches	Römisches	k1gInSc1	Römisches
Reich	Reich	k?	Reich
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Klasická	klasický	k2eAgFnSc1d1	klasická
====	====	k?	====
</s>
</p>
<p>
<s>
APPIÁNOS	APPIÁNOS	kA	APPIÁNOS
<g/>
,	,	kIx,	,
Krize	krize	k1gFnSc1	krize
římské	římský	k2eAgFnSc2d1	římská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-205-0060-X	[number]	k4	80-205-0060-X
</s>
</p>
<p>
<s>
LIVIUS	LIVIUS	kA	LIVIUS
<g/>
,	,	kIx,	,
Titus	Titus	k1gInSc1	Titus
<g/>
,	,	kIx,	,
Dějiny	dějiny	k1gFnPc1	dějiny
I-VII	I-VII	k1gFnSc2	I-VII
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
</s>
</p>
<p>
<s>
AMMIANUS	AMMIANUS	kA	AMMIANUS
<g/>
,	,	kIx,	,
Marcellinus	Marcellinus	k1gInSc1	Marcellinus
<g/>
,	,	kIx,	,
Soumrak	soumrak	k1gInSc1	soumrak
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
</s>
</p>
<p>
<s>
PROKOPIOS	PROKOPIOS	kA	PROKOPIOS
Z	z	k7c2	z
KAISAREIE	KAISAREIE	kA	KAISAREIE
<g/>
,	,	kIx,	,
Válka	válka	k1gFnSc1	válka
s	s	k7c7	s
Góty	Gót	k1gMnPc7	Gót
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
</s>
</p>
<p>
<s>
TACITUS	TACITUS	kA	TACITUS
<g/>
,	,	kIx,	,
Publius	Publius	k1gMnSc1	Publius
Cornelius	Cornelius	k1gMnSc1	Cornelius
<g/>
,	,	kIx,	,
Letopisy	letopis	k1gInPc1	letopis
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
</s>
</p>
<p>
<s>
TACITUS	TACITUS	kA	TACITUS
<g/>
,	,	kIx,	,
Publius	Publius	k1gMnSc1	Publius
Cornelius	Cornelius	k1gMnSc1	Cornelius
<g/>
,	,	kIx,	,
Z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
císařského	císařský	k2eAgInSc2d1	císařský
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
</s>
</p>
<p>
<s>
SUETONIUS	SUETONIUS	kA	SUETONIUS
TRANQUILLUS	TRANQUILLUS	kA	TRANQUILLUS
<g/>
,	,	kIx,	,
Gaius	Gaius	k1gInSc1	Gaius
<g/>
,	,	kIx,	,
Životopisy	životopis	k1gInPc1	životopis
dvanácti	dvanáct	k4xCc2	dvanáct
císařů	císař	k1gMnPc2	císař
<g/>
:	:	kIx,	:
spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
zlomky	zlomek	k1gInPc1	zlomek
jeho	jeho	k3xOp3gInSc2	jeho
spisu	spis	k1gInSc2	spis
O	o	k7c6	o
význačných	význačný	k2eAgMnPc6d1	význačný
literátech	literát	k1gMnPc6	literát
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-902300-5-9	[number]	k4	80-902300-5-9
</s>
</p>
<p>
<s>
ZÓSIMOS	ZÓSIMOS	kA	ZÓSIMOS
<g/>
,	,	kIx,	,
Stesky	stesk	k1gInPc1	stesk
posledního	poslední	k2eAgMnSc2d1	poslední
Římana	Říman	k1gMnSc2	Říman
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
</s>
</p>
<p>
<s>
====	====	k?	====
Moderní	moderní	k2eAgMnSc1d1	moderní
====	====	k?	====
</s>
</p>
<p>
<s>
BEDNAŘÍKOVÁ	Bednaříková	k1gFnSc1	Bednaříková
<g/>
,	,	kIx,	,
Jarmila	Jarmila	k1gFnSc1	Jarmila
<g/>
.	.	kIx.	.
</s>
<s>
Podzimní	podzimní	k2eAgInSc1d1	podzimní
čas	čas	k1gInSc1	čas
mocné	mocný	k2eAgFnSc2d1	mocná
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
MOBA	MOBA	kA	MOBA
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
461	[number]	k4	461
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
243	[number]	k4	243
<g/>
-	-	kIx~	-
<g/>
1344	[number]	k4	1344
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BURIAN	Burian	k1gMnSc1	Burian
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
Římské	římský	k2eAgNnSc1d1	římské
impérium	impérium	k1gNnSc1	impérium
<g/>
:	:	kIx,	:
vrchol	vrchol	k1gInSc1	vrchol
a	a	k8xC	a
proměny	proměna	k1gFnPc1	proměna
antické	antický	k2eAgFnSc2d1	antická
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-205-0536-9	[number]	k4	80-205-0536-9
</s>
</p>
<p>
<s>
ČEŠKA	Češka	k1gFnSc1	Češka
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
Zánik	zánik	k1gInSc1	zánik
antického	antický	k2eAgInSc2d1	antický
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7021-386-8	[number]	k4	80-7021-386-8
</s>
</p>
<p>
<s>
Dahlheim	Dahlheim	k1gMnSc1	Dahlheim
<g/>
,	,	kIx,	,
W.	W.	kA	W.
<g/>
:	:	kIx,	:
U	u	k7c2	u
kolébky	kolébka	k1gFnSc2	kolébka
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Odkaz	odkaz	k1gInSc1	odkaz
antického	antický	k2eAgInSc2d1	antický
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DAWSON	DAWSON	kA	DAWSON
<g/>
,	,	kIx,	,
Christopher	Christophra	k1gFnPc2	Christophra
<g/>
.	.	kIx.	.
</s>
<s>
Zrození	zrození	k1gNnSc1	zrození
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
:	:	kIx,	:
úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
evropské	evropský	k2eAgFnSc2d1	Evropská
jednoty	jednota	k1gFnSc2	jednota
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
114	[number]	k4	114
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Goldsworthy	Goldsworth	k1gInPc1	Goldsworth
<g/>
,	,	kIx,	,
A.	A.	kA	A.
<g/>
:	:	kIx,	:
Armáda	armáda	k1gFnSc1	armáda
starého	starý	k2eAgInSc2d1	starý
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
Slovart	Slovart	k1gInSc1	Slovart
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GIBBON	gibbon	k1gMnSc1	gibbon
<g/>
,	,	kIx,	,
Edward	Edward	k1gMnSc1	Edward
<g/>
.	.	kIx.	.
</s>
<s>
Úpadek	úpadek	k1gInSc1	úpadek
a	a	k8xC	a
pád	pád	k1gInSc1	pád
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Levné	levný	k2eAgFnSc2d1	levná
knihy	kniha	k1gFnSc2	kniha
KMa	KMa	k1gFnSc2	KMa
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
380	[number]	k4	380
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7309	[number]	k4	7309
<g/>
-	-	kIx~	-
<g/>
189	[number]	k4	189
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GRANT	grant	k1gInSc1	grant
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
antického	antický	k2eAgInSc2d1	antický
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
BB	BB	kA	BB
art	art	k?	art
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
471	[number]	k4	471
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7341	[number]	k4	7341
<g/>
-	-	kIx~	-
<g/>
930	[number]	k4	930
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GRANT	grant	k1gInSc1	grant
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
<g/>
.	.	kIx.	.
</s>
<s>
Pád	Pád	k1gInSc1	Pád
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
BB	BB	kA	BB
art	art	k?	art
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
208	[number]	k4	208
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7381	[number]	k4	7381
<g/>
-	-	kIx~	-
<g/>
770	[number]	k4	770
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GRANT	grant	k1gInSc1	grant
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
<g/>
.	.	kIx.	.
</s>
<s>
Římští	římský	k2eAgMnPc1d1	římský
císařové	císař	k1gMnPc1	císař
:	:	kIx,	:
životopisy	životopis	k1gInPc1	životopis
vládců	vládce	k1gMnPc2	vládce
císařského	císařský	k2eAgInSc2d1	císařský
Říma	Řím	k1gInSc2	Řím
v	v	k7c6	v
letech	let	k1gInPc6	let
31	[number]	k4	31
př	př	kA	př
<g/>
.	.	kIx.	.
Kr.	Kr.	k1gMnSc1	Kr.
-	-	kIx~	-
476	[number]	k4	476
po	po	k7c6	po
Kr.	Kr.	k1gFnSc6	Kr.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
BB	BB	kA	BB
art	art	k?	art
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
387	[number]	k4	387
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7257	[number]	k4	7257
<g/>
-	-	kIx~	-
<g/>
731	[number]	k4	731
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
HOŠEK	Hošek	k1gMnSc1	Hošek
<g/>
,	,	kIx,	,
Radislav	Radislav	k1gMnSc1	Radislav
<g/>
,	,	kIx,	,
MAREK	Marek	k1gMnSc1	Marek
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
,	,	kIx,	,
Řím	Řím	k1gInSc1	Řím
Marka	marka	k1gFnSc1	marka
Aurelia	Aurelia	k1gFnSc1	Aurelia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-204-0083-4	[number]	k4	80-204-0083-4
</s>
</p>
<p>
<s>
CHRIST	CHRIST	kA	CHRIST
<g/>
,	,	kIx,	,
Karl	Karl	k1gMnSc1	Karl
<g/>
.	.	kIx.	.
</s>
<s>
Krize	krize	k1gFnSc1	krize
a	a	k8xC	a
zánik	zánik	k1gInSc1	zánik
římské	římský	k2eAgFnSc2d1	římská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
424	[number]	k4	424
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7429	[number]	k4	7429
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
29	[number]	k4	29
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PENROSE	PENROSE	kA	PENROSE
<g/>
,	,	kIx,	,
Jane	Jan	k1gMnSc5	Jan
<g/>
,	,	kIx,	,
Řím	Řím	k1gInSc1	Řím
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
nepřátelé	nepřítel	k1gMnPc1	nepřítel
<g/>
:	:	kIx,	:
říše	říše	k1gFnSc1	říše
stvořená	stvořený	k2eAgFnSc1d1	stvořená
a	a	k8xC	a
zničená	zničený	k2eAgFnSc1d1	zničená
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Fighters	Fighters	k1gInSc1	Fighters
Publications	Publicationsa	k1gFnPc2	Publicationsa
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-86977-10-2	[number]	k4	978-80-86977-10-2
</s>
</p>
<p>
<s>
ZAMAROVSKÝ	ZAMAROVSKÝ	kA	ZAMAROVSKÝ
<g/>
,	,	kIx,	,
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
psané	psaný	k2eAgFnPc1d1	psaná
Římem	Řím	k1gInSc7	Řím
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
:	:	kIx,	:
Perfekt	perfektum	k1gNnPc2	perfektum
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
267	[number]	k4	267
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
8046	[number]	k4	8046
<g/>
-	-	kIx~	-
<g/>
297	[number]	k4	297
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Starověká	starověký	k2eAgFnSc1d1	starověká
římská	římský	k2eAgFnSc1d1	římská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Římské	římský	k2eAgNnSc1d1	římské
právo	právo	k1gNnSc1	právo
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
římských	římský	k2eAgMnPc2d1	římský
králů	král	k1gMnPc2	král
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
římských	římský	k2eAgMnPc2d1	římský
císařů	císař	k1gMnPc2	císař
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
římských	římský	k2eAgMnPc2d1	římský
konzulů	konzul	k1gMnPc2	konzul
</s>
</p>
<p>
<s>
Byzantská	byzantský	k2eAgFnSc1d1	byzantská
říše	říše	k1gFnSc1	říše
</s>
</p>
<p>
<s>
Západořímská	západořímský	k2eAgFnSc1d1	Západořímská
říše	říše	k1gFnSc1	říše
</s>
</p>
<p>
<s>
Chronologie	chronologie	k1gFnSc1	chronologie
dějin	dějiny	k1gFnPc2	dějiny
starověkého	starověký	k2eAgInSc2d1	starověký
Říma	Řím	k1gInSc2	Řím
</s>
</p>
<p>
<s>
Nova	nova	k1gFnSc1	nova
Roma	Rom	k1gMnSc2	Rom
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Starověký	starověký	k2eAgInSc4d1	starověký
Řím	Řím	k1gInSc4	Řím
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Rodina	rodina	k1gFnSc1	rodina
ve	v	k7c6	v
starém	starý	k2eAgInSc6d1	starý
Římě	Řím	k1gInSc6	Řím
na	na	k7c6	na
sesterském	sesterský	k2eAgInSc6d1	sesterský
projektu	projekt	k1gInSc6	projekt
Wikiknihy	Wikiknih	k1gInPc4	Wikiknih
</s>
</p>
<p>
<s>
Římské	římský	k2eAgNnSc1d1	římské
císařství	císařství	k1gNnSc1	císařství
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
antického	antický	k2eAgInSc2d1	antický
Říma	Řím	k1gInSc2	Řím
</s>
</p>
<p>
<s>
Územní	územní	k2eAgInSc1d1	územní
vývoj	vývoj	k1gInSc1	vývoj
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
</s>
</p>
<p>
<s>
Staré	Staré	k2eAgInPc1d1	Staré
národy	národ	k1gInPc1	národ
a	a	k8xC	a
civilizace	civilizace	k1gFnSc2	civilizace
–	–	k?	–
Starověký	starověký	k2eAgInSc1d1	starověký
Řím	Řím	k1gInSc1	Řím
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Roman	Roman	k1gMnSc1	Roman
Empire	empir	k1gInSc5	empir
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Roman	Roman	k1gMnSc1	Roman
Empire	empir	k1gInSc5	empir
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Politische	Politische	k1gFnSc1	Politische
und	und	k?	und
kulturelle	kulturelle	k1gInSc1	kulturelle
Entwicklung	Entwicklung	k1gMnSc1	Entwicklung
Roms	Roms	k1gInSc1	Roms
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Roman	Roman	k1gMnSc1	Roman
Law	Law	k1gMnSc1	Law
Library	Librara	k1gFnSc2	Librara
</s>
</p>
