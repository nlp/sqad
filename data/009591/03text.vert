<p>
<s>
Marián	Marián	k1gMnSc1	Marián
Čalfa	Čalf	k1gMnSc2	Čalf
(	(	kIx(	(
<g/>
*	*	kIx~	*
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1946	[number]	k4	1946
Trebišov	Trebišov	k1gInSc1	Trebišov
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
slovenský	slovenský	k2eAgMnSc1d1	slovenský
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
politik	politik	k1gMnSc1	politik
působící	působící	k2eAgMnSc1d1	působící
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
za	za	k7c2	za
normalizace	normalizace	k1gFnSc2	normalizace
člen	člen	k1gMnSc1	člen
a	a	k8xC	a
funkcionář	funkcionář	k1gMnSc1	funkcionář
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
československý	československý	k2eAgMnSc1d1	československý
premiér	premiér	k1gMnSc1	premiér
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mládí	mládí	k1gNnSc1	mládí
a	a	k8xC	a
kariéra	kariéra	k1gFnSc1	kariéra
v	v	k7c6	v
KSČ	KSČ	kA	KSČ
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
vysokoškolských	vysokoškolský	k2eAgFnPc2d1	vysokoškolská
studií	studie	k1gFnPc2	studie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
Právnickou	právnický	k2eAgFnSc4d1	právnická
fakultu	fakulta	k1gFnSc4	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgMnS	pracovat
potom	potom	k6eAd1	potom
po	po	k7c4	po
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
na	na	k7c6	na
praxi	praxe	k1gFnSc6	praxe
v	v	k7c6	v
ČTK	ČTK	kA	ČTK
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
v	v	k7c6	v
legislativním	legislativní	k2eAgInSc6d1	legislativní
odboru	odbor	k1gInSc6	odbor
Úřadu	úřad	k1gInSc2	úřad
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
vlády	vláda	k1gFnSc2	vláda
ČSSR	ČSSR	kA	ČSSR
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
svých	svůj	k3xOyFgNnPc2	svůj
osmnácti	osmnáct	k4xCc2	osmnáct
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgInS	být
členem	člen	k1gMnSc7	člen
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
zástupcem	zástupce	k1gMnSc7	zástupce
vedoucího	vedoucí	k2eAgInSc2d1	vedoucí
Úřadu	úřad	k1gInSc2	úřad
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1988	[number]	k4	1988
byl	být	k5eAaImAgInS	být
Lubomírem	Lubomír	k1gMnSc7	Lubomír
Štrougalem	Štrougal	k1gMnSc7	Štrougal
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
ministrem	ministr	k1gMnSc7	ministr
pro	pro	k7c4	pro
legislativu	legislativa	k1gFnSc4	legislativa
a	a	k8xC	a
předsedou	předseda	k1gMnSc7	předseda
Legislativní	legislativní	k2eAgFnSc2d1	legislativní
rady	rada	k1gFnSc2	rada
vlády	vláda	k1gFnSc2	vláda
ČSSR	ČSSR	kA	ČSSR
v	v	k7c6	v
šesté	šestý	k4xOgFnSc6	šestý
vládě	vláda	k1gFnSc6	vláda
Lubomíra	Lubomíra	k1gFnSc1	Lubomíra
Štrougala	Štrougala	k1gFnSc1	Štrougala
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
zůstal	zůstat	k5eAaPmAgMnS	zůstat
i	i	k9	i
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
Štrougala	Štrougala	k1gFnSc1	Štrougala
nahradil	nahradit	k5eAaPmAgInS	nahradit
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
Ladislav	Ladislav	k1gMnSc1	Ladislav
Adamec	Adamec	k1gMnSc1	Adamec
(	(	kIx(	(
<g/>
vláda	vláda	k1gFnSc1	vláda
Ladislava	Ladislav	k1gMnSc2	Ladislav
Adamce	Adamec	k1gMnSc2	Adamec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
dostal	dostat	k5eAaPmAgMnS	dostat
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
vypracování	vypracování	k1gNnSc4	vypracování
přísnějšího	přísný	k2eAgInSc2d2	přísnější
tiskového	tiskový	k2eAgInSc2d1	tiskový
zákona	zákon	k1gInSc2	zákon
(	(	kIx(	(
<g/>
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1989	[number]	k4	1989
také	také	k6eAd1	také
předsedal	předsedat	k5eAaImAgMnS	předsedat
pracovní	pracovní	k2eAgFnSc4d1	pracovní
komisi	komise	k1gFnSc4	komise
při	při	k7c6	při
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
připravit	připravit	k5eAaPmF	připravit
novou	nový	k2eAgFnSc4d1	nová
ústavu	ústava	k1gFnSc4	ústava
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
ústavních	ústavní	k2eAgInPc2d1	ústavní
návrhů	návrh	k1gInPc2	návrh
se	se	k3xPyFc4	se
zvažovalo	zvažovat	k5eAaImAgNnS	zvažovat
vypuštění	vypuštění	k1gNnSc1	vypuštění
vedoucí	vedoucí	k2eAgFnSc2d1	vedoucí
úlohy	úloha	k1gFnSc2	úloha
KSČ	KSČ	kA	KSČ
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
a	a	k8xC	a
garantování	garantování	k1gNnSc6	garantování
svobody	svoboda	k1gFnSc2	svoboda
opuštění	opuštění	k1gNnSc4	opuštění
území	území	k1gNnSc2	území
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
ale	ale	k9	ale
nebyla	být	k5eNaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
a	a	k8xC	a
po	po	k7c6	po
událostech	událost	k1gFnPc6	událost
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1989	[number]	k4	1989
již	již	k6eAd1	již
nebylo	být	k5eNaImAgNnS	být
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
návrh	návrh	k1gInSc4	návrh
navázáno	navázat	k5eAaPmNgNnS	navázat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podzim	podzim	k1gInSc1	podzim
1989	[number]	k4	1989
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
politická	politický	k2eAgFnSc1d1	politická
kariéra	kariéra	k1gFnSc1	kariéra
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
i	i	k9	i
po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
listopadových	listopadový	k2eAgFnPc6d1	listopadová
událostech	událost	k1gFnPc6	událost
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1989	[number]	k4	1989
stal	stát	k5eAaPmAgInS	stát
1	[number]	k4	1
<g/>
.	.	kIx.	.
místopředsedou	místopředseda	k1gMnSc7	místopředseda
rekonstruované	rekonstruovaný	k2eAgFnSc2d1	rekonstruovaná
Adamcovy	Adamcův	k2eAgFnSc2d1	Adamcova
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
členem	člen	k1gMnSc7	člen
týmu	tým	k1gInSc2	tým
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
za	za	k7c2	za
KSČ	KSČ	kA	KSČ
jednal	jednat	k5eAaImAgInS	jednat
se	s	k7c7	s
zástupci	zástupce	k1gMnPc7	zástupce
Občanského	občanský	k2eAgNnSc2d1	občanské
fóra	fórum	k1gNnSc2	fórum
a	a	k8xC	a
VPN	VPN	kA	VPN
<g/>
.	.	kIx.	.
</s>
<s>
Funkci	funkce	k1gFnSc4	funkce
1	[number]	k4	1
<g/>
.	.	kIx.	.
místopředsedy	místopředseda	k1gMnSc2	místopředseda
zastával	zastávat	k5eAaImAgMnS	zastávat
do	do	k7c2	do
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
od	od	k7c2	od
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
byl	být	k5eAaImAgMnS	být
i	i	k8xC	i
pověřeným	pověřený	k2eAgMnSc7d1	pověřený
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
místo	místo	k7c2	místo
Ladislava	Ladislav	k1gMnSc2	Ladislav
Adamce	Adamec	k1gMnSc2	Adamec
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
vláda	vláda	k1gFnSc1	vláda
Mariána	Marián	k1gMnSc2	Marián
Čalfy	Čalf	k1gMnPc4	Čalf
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odstoupení	odstoupení	k1gNnSc6	odstoupení
Adamce	Adamec	k1gMnSc2	Adamec
z	z	k7c2	z
jednání	jednání	k1gNnSc2	jednání
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
hlavním	hlavní	k2eAgMnSc7d1	hlavní
vyjednavačem	vyjednavač	k1gMnSc7	vyjednavač
s	s	k7c7	s
nastupující	nastupující	k2eAgFnSc7d1	nastupující
mocí	moc	k1gFnSc7	moc
a	a	k8xC	a
následně	následně	k6eAd1	následně
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1989	[number]	k4	1989
premiérem	premiér	k1gMnSc7	premiér
v	v	k7c6	v
takzvané	takzvaný	k2eAgFnSc6d1	takzvaná
"	"	kIx"	"
<g/>
vládě	vláda	k1gFnSc6	vláda
národního	národní	k2eAgNnSc2d1	národní
porozumění	porozumění	k1gNnSc2	porozumění
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
fakticky	fakticky	k6eAd1	fakticky
druhá	druhý	k4xOgFnSc1	druhý
vláda	vláda	k1gFnSc1	vláda
Mariána	Marián	k1gMnSc2	Marián
Čalfy	Čalf	k1gMnPc7	Čalf
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dovedla	dovést	k5eAaPmAgFnS	dovést
zemi	zem	k1gFnSc4	zem
k	k	k7c3	k
prvním	první	k4xOgFnPc3	první
demokratickým	demokratický	k2eAgFnPc3d1	demokratická
volbám	volba	k1gFnPc3	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
vládě	vláda	k1gFnSc6	vláda
dočasně	dočasně	k6eAd1	dočasně
<g/>
,	,	kIx,	,
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1989	[number]	k4	1989
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
i	i	k9	i
post	post	k1gInSc4	post
pověřeného	pověřený	k2eAgMnSc2d1	pověřený
ministra	ministr	k1gMnSc2	ministr
vnitra	vnitro	k1gNnSc2	vnitro
ČSSR	ČSSR	kA	ČSSR
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zastával	zastávat	k5eAaImAgMnS	zastávat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Jánem	Ján	k1gMnSc7	Ján
Čarnogurským	Čarnogurský	k2eAgMnSc7d1	Čarnogurský
a	a	k8xC	a
Valtrem	Valtr	k1gMnSc7	Valtr
Komárkem	Komárek	k1gMnSc7	Komárek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sehrál	sehrát	k5eAaPmAgInS	sehrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
při	při	k7c6	při
prosazení	prosazení	k1gNnSc6	prosazení
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
na	na	k7c4	na
post	post	k1gInSc4	post
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
ozývaly	ozývat	k5eAaImAgInP	ozývat
hlasy	hlas	k1gInPc1	hlas
pro	pro	k7c4	pro
přímou	přímý	k2eAgFnSc4d1	přímá
volbu	volba	k1gFnSc4	volba
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
ale	ale	k9	ale
měl	mít	k5eAaImAgInS	mít
větší	veliký	k2eAgFnPc4d2	veliký
šance	šance	k1gFnPc4	šance
Alexander	Alexandra	k1gFnPc2	Alexandra
Dubček	Dubček	k1gInSc1	Dubček
než	než	k8xS	než
málo	málo	k6eAd1	málo
známý	známý	k2eAgMnSc1d1	známý
Havel	Havel	k1gMnSc1	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Čalfa	Čalf	k1gMnSc4	Čalf
ale	ale	k8xC	ale
zajistil	zajistit	k5eAaPmAgMnS	zajistit
Havlovo	Havlův	k2eAgNnSc4d1	Havlovo
jednomyslné	jednomyslný	k2eAgNnSc4d1	jednomyslné
zvolení	zvolení	k1gNnSc4	zvolení
v	v	k7c6	v
nepřímé	přímý	k2eNgFnSc6d1	nepřímá
volbě	volba	k1gFnSc6	volba
(	(	kIx(	(
<g/>
parlamentem	parlament	k1gInSc7	parlament
složeným	složený	k2eAgInSc7d1	složený
ještě	ještě	k6eAd1	ještě
z	z	k7c2	z
předlistopadových	předlistopadový	k2eAgMnPc2d1	předlistopadový
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
)	)	kIx)	)
a	a	k8xC	a
coby	coby	k?	coby
člověk	člověk	k1gMnSc1	člověk
obeznámený	obeznámený	k2eAgMnSc1d1	obeznámený
s	s	k7c7	s
fungováním	fungování	k1gNnSc7	fungování
vládních	vládní	k2eAgInPc2d1	vládní
orgánů	orgán	k1gInPc2	orgán
poskytoval	poskytovat	k5eAaImAgInS	poskytovat
Havlovi	Havel	k1gMnSc3	Havel
profesionální	profesionální	k2eAgFnSc2d1	profesionální
politické	politický	k2eAgFnSc2d1	politická
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politická	politický	k2eAgFnSc1d1	politická
kariéra	kariéra	k1gFnSc1	kariéra
po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
nejvyšších	vysoký	k2eAgFnPc6d3	nejvyšší
politických	politický	k2eAgFnPc6d1	politická
funkcích	funkce	k1gFnPc6	funkce
se	se	k3xPyFc4	se
udržel	udržet	k5eAaPmAgInS	udržet
i	i	k9	i
po	po	k7c4	po
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1990	[number]	k4	1990
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
Čalfa	Čalf	k1gMnSc2	Čalf
z	z	k7c2	z
KSČ	KSČ	kA	KSČ
a	a	k8xC	a
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
byl	být	k5eAaImAgInS	být
zároveň	zároveň	k6eAd1	zároveň
opětovně	opětovně	k6eAd1	opětovně
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
vláda	vláda	k1gFnSc1	vláda
národní	národní	k2eAgFnSc2d1	národní
oběti	oběť	k1gFnSc2	oběť
<g/>
,	,	kIx,	,
fakticky	fakticky	k6eAd1	fakticky
třetí	třetí	k4xOgFnSc1	třetí
vláda	vláda	k1gFnSc1	vláda
Mariána	Marián	k1gMnSc2	Marián
Čalfy	Čalf	k1gMnPc7	Čalf
<g/>
)	)	kIx)	)
a	a	k8xC	a
tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
zastával	zastávat	k5eAaImAgMnS	zastávat
až	až	k9	až
do	do	k7c2	do
voleb	volba	k1gFnPc2	volba
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
již	již	k6eAd1	již
jako	jako	k8xC	jako
člen	člen	k1gMnSc1	člen
hnutí	hnutí	k1gNnSc2	hnutí
Verejnosť	Verejnosť	k1gFnSc2	Verejnosť
proti	proti	k7c3	proti
násiliu	násilius	k1gMnSc3	násilius
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
řad	řada	k1gFnPc2	řada
přijalo	přijmout	k5eAaPmAgNnS	přijmout
i	i	k9	i
populární	populární	k2eAgFnSc2d1	populární
osobnosti	osobnost	k1gFnSc2	osobnost
s	s	k7c7	s
komunistickou	komunistický	k2eAgFnSc7d1	komunistická
předlistopadovou	předlistopadový	k2eAgFnSc7d1	předlistopadová
minulostí	minulost	k1gFnSc7	minulost
<g/>
.	.	kIx.	.
<g/>
Zároveň	zároveň	k6eAd1	zároveň
zasedal	zasedat	k5eAaImAgInS	zasedat
i	i	k9	i
v	v	k7c6	v
nejvyšším	vysoký	k2eAgInSc6d3	Nejvyšší
zákonodárném	zákonodárný	k2eAgInSc6d1	zákonodárný
sboru	sbor	k1gInSc6	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
slovenské	slovenský	k2eAgFnSc2d1	slovenská
části	část	k1gFnSc2	část
Sněmovny	sněmovna	k1gFnSc2	sněmovna
národů	národ	k1gInPc2	národ
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
(	(	kIx(	(
<g/>
volební	volební	k2eAgInSc1d1	volební
obvod	obvod	k1gInSc1	obvod
Východoslovenský	východoslovenský	k2eAgInSc1d1	východoslovenský
kraj	kraj	k1gInSc1	kraj
<g/>
)	)	kIx)	)
za	za	k7c4	za
VPN	VPN	kA	VPN
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozkladu	rozklad	k1gInSc6	rozklad
VPN	VPN	kA	VPN
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
přešel	přejít	k5eAaPmAgInS	přejít
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
nástupnických	nástupnický	k2eAgFnPc2d1	nástupnická
stran	strana	k1gFnPc2	strana
ODÚ-VPN	ODÚ-VPN	k1gMnPc2	ODÚ-VPN
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgInS	být
místopředsedou	místopředseda	k1gMnSc7	místopředseda
ODÚ-VPN	ODÚ-VPN	k1gMnSc1	ODÚ-VPN
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Federálním	federální	k2eAgNnSc6d1	federální
shromáždění	shromáždění	k1gNnSc6	shromáždění
setrval	setrvat	k5eAaPmAgMnS	setrvat
do	do	k7c2	do
voleb	volba	k1gFnPc2	volba
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
do	do	k7c2	do
parlamentu	parlament	k1gInSc2	parlament
za	za	k7c4	za
ODÚ	ODÚ	kA	ODÚ
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
neúspěšně	úspěšně	k6eNd1	úspěšně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
politiky	politika	k1gFnSc2	politika
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
volební	volební	k2eAgFnSc6d1	volební
porážce	porážka	k1gFnSc6	porážka
odešel	odejít	k5eAaPmAgMnS	odejít
Marián	Marián	k1gMnSc1	Marián
Čalfa	Čalf	k1gMnSc2	Čalf
z	z	k7c2	z
politiky	politika	k1gFnSc2	politika
(	(	kIx(	(
<g/>
do	do	k7c2	do
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
neúspěšně	úspěšně	k6eNd1	úspěšně
pokusil	pokusit	k5eAaPmAgMnS	pokusit
vrátit	vrátit	k5eAaPmF	vrátit
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c6	po
rozdělení	rozdělení	k1gNnSc6	rozdělení
Československa	Československo	k1gNnSc2	Československo
zůstal	zůstat	k5eAaPmAgInS	zůstat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
advokát	advokát	k1gMnSc1	advokát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
ho	on	k3xPp3gNnSc2	on
Deník	deník	k1gInSc1	deník
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
mezi	mezi	k7c4	mezi
nejbohatší	bohatý	k2eAgMnPc4d3	nejbohatší
a	a	k8xC	a
nejvlivnější	vlivný	k2eAgMnPc4d3	nejvlivnější
pražské	pražský	k2eAgMnPc4d1	pražský
advokáty	advokát	k1gMnPc4	advokát
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
politiky	politika	k1gFnSc2	politika
měl	mít	k5eAaImAgInS	mít
blízko	blízko	k6eAd1	blízko
k	k	k7c3	k
Václavu	Václav	k1gMnSc3	Václav
Havlovi	Havel	k1gMnSc3	Havel
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
ho	on	k3xPp3gMnSc4	on
Havel	Havel	k1gMnSc1	Havel
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
svým	svůj	k3xOyFgMnSc7	svůj
poradcem	poradce	k1gMnSc7	poradce
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
právník	právník	k1gMnSc1	právník
dojednával	dojednávat	k5eAaImAgMnS	dojednávat
prodej	prodej	k1gInSc4	prodej
paláce	palác	k1gInSc2	palác
Lucerna	lucerna	k1gFnSc1	lucerna
Chemapolu	chemapol	k1gInSc2	chemapol
<g/>
.	.	kIx.	.
<g/>
Byl	být	k5eAaImAgInS	být
také	také	k6eAd1	také
akcionářem	akcionář	k1gMnSc7	akcionář
<g/>
,	,	kIx,	,
členem	člen	k1gMnSc7	člen
představenstva	představenstvo	k1gNnSc2	představenstvo
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
-	-	kIx~	-
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
a	a	k8xC	a
dozorčí	dozorčí	k2eAgFnSc2d1	dozorčí
rady	rada	k1gFnSc2	rada
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
-	-	kIx~	-
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
pražské	pražský	k2eAgFnSc2d1	Pražská
IC	IC	kA	IC
banky	banka	k1gFnSc2	banka
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Equa	Equa	k1gFnSc1	Equa
bank	banka	k1gFnPc2	banka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1993-1995	[number]	k4	1993-1995
byl	být	k5eAaImAgInS	být
také	také	k9	také
členem	člen	k1gMnSc7	člen
dozorčí	dozorčí	k2eAgFnSc2d1	dozorčí
rady	rada	k1gFnSc2	rada
CS	CS	kA	CS
First	First	k1gInSc1	First
Boston	Boston	k1gInSc1	Boston
investiční	investiční	k2eAgFnSc2d1	investiční
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
WOOD	WOOD	kA	WOOD
&	&	k?	&
Company	Compana	k1gFnSc2	Compana
investiční	investiční	k2eAgFnSc1d1	investiční
společnost	společnost	k1gFnSc1	společnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1998-1999	[number]	k4	1998-1999
členem	člen	k1gMnSc7	člen
dozorčí	dozorčí	k2eAgFnSc2d1	dozorčí
rady	rada	k1gFnSc2	rada
chemičky	chemička	k1gFnSc2	chemička
Synthesia	Synthesia	k1gFnSc1	Synthesia
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
obchodních	obchodní	k2eAgFnPc6d1	obchodní
společnostech	společnost	k1gFnPc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgMnSc7d1	jediný
jednatelem	jednatel	k1gMnSc7	jednatel
a	a	k8xC	a
společníkem	společník	k1gMnSc7	společník
v	v	k7c6	v
PROGNEM	PROGNEM	kA	PROGNEM
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
(	(	kIx(	(
<g/>
likvidace	likvidace	k1gFnSc1	likvidace
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
5	[number]	k4	5
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
společníkem	společník	k1gMnSc7	společník
v	v	k7c4	v
Starlit	Starlit	k1gInSc4	Starlit
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
členem	člen	k1gInSc7	člen
představenstva	představenstvo	k1gNnSc2	představenstvo
Pražské	pražský	k2eAgFnSc2d1	Pražská
energetiky	energetika	k1gFnSc2	energetika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Kdo	kdo	k3yRnSc1	kdo
byl	být	k5eAaImAgMnS	být
kdo	kdo	k3yRnSc1	kdo
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
dějinách	dějiny	k1gFnPc6	dějiny
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
I.	I.	kA	I.
<g/>
,	,	kIx,	,
A	A	kA	A
<g/>
–	–	k?	–
<g/>
M	M	kA	M
/	/	kIx~	/
Milan	Milan	k1gMnSc1	Milan
Churaň	Churaň	k1gMnSc1	Churaň
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
..	..	k?	..
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
467	[number]	k4	467
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85983	[number]	k4	85983
<g/>
-	-	kIx~	-
<g/>
44	[number]	k4	44
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
84	[number]	k4	84
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kdo	kdo	k3yRnSc1	kdo
je	být	k5eAaImIp3nS	být
kdo	kdo	k3yQnSc1	kdo
:	:	kIx,	:
91	[number]	k4	91
<g/>
/	/	kIx~	/
<g/>
92	[number]	k4	92
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
federální	federální	k2eAgInPc4d1	federální
orgány	orgán	k1gInPc4	orgán
ČSFR	ČSFR	kA	ČSFR
<g/>
.	.	kIx.	.
</s>
<s>
Díl	díl	k1gInSc1	díl
1	[number]	k4	1
<g/>
,	,	kIx,	,
A	A	kA	A
<g/>
–	–	k?	–
<g/>
M.	M.	kA	M.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Kdo	kdo	k3yInSc1	kdo
je	být	k5eAaImIp3nS	být
kdo	kdo	k3yQnSc1	kdo
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
636	[number]	k4	636
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
901103	[number]	k4	901103
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
115	[number]	k4	115
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TOMEŠ	Tomeš	k1gMnSc1	Tomeš
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
XX	XX	kA	XX
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
:	:	kIx,	:
I.	I.	kA	I.
díl	díl	k1gInSc1	díl
:	:	kIx,	:
A	a	k9	a
<g/>
–	–	k?	–
<g/>
J.	J.	kA	J.
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
;	;	kIx,	;
Petr	Petr	k1gMnSc1	Petr
Meissner	Meissner	k1gMnSc1	Meissner
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
634	[number]	k4	634
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
245	[number]	k4	245
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
180	[number]	k4	180
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
vláda	vláda	k1gFnSc1	vláda
Mariána	Marián	k1gMnSc2	Marián
Čalfy	Čalf	k1gInPc7	Čalf
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
vláda	vláda	k1gFnSc1	vláda
Mariána	Marián	k1gMnSc2	Marián
Čalfy	Čalf	k1gInPc1	Čalf
(	(	kIx(	(
<g/>
Vláda	vláda	k1gFnSc1	vláda
národního	národní	k2eAgNnSc2d1	národní
porozumění	porozumění	k1gNnSc2	porozumění
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
vláda	vláda	k1gFnSc1	vláda
Mariána	Marián	k1gMnSc2	Marián
Čalfy	Čalf	k1gInPc1	Čalf
(	(	kIx(	(
<g/>
Vláda	vláda	k1gFnSc1	vláda
národní	národní	k2eAgFnSc2d1	národní
oběti	oběť	k1gFnSc2	oběť
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Marián	Marián	k1gMnSc1	Marián
Čalfa	Čalf	k1gMnSc2	Čalf
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Zídek	Zídek	k1gMnSc1	Zídek
<g/>
:	:	kIx,	:
Zachránce	zachránce	k1gMnSc1	zachránce
sametové	sametový	k2eAgFnSc2d1	sametová
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
Lidové	lidový	k2eAgFnPc4d1	lidová
noviny	novina	k1gFnPc4	novina
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2009	[number]	k4	2009
</s>
</p>
