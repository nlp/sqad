<p>
<s>
Velevrub	velevrub	k1gMnSc1	velevrub
tupý	tupý	k2eAgMnSc1d1	tupý
(	(	kIx(	(
<g/>
Unio	Unio	k1gMnSc1	Unio
crassus	crassus	k1gMnSc1	crassus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc1	druh
sladkovodního	sladkovodní	k2eAgMnSc2d1	sladkovodní
mlže	mlž	k1gMnSc2	mlž
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
lasturu	lastura	k1gFnSc4	lastura
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
50-70	[number]	k4	50-70
mm	mm	kA	mm
<g/>
,	,	kIx,	,
výšce	výška	k1gFnSc6	výška
30-40	[number]	k4	30-40
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Tloušťka	tloušťka	k1gFnSc1	tloušťka
schránky	schránka	k1gFnSc2	schránka
činí	činit	k5eAaImIp3nS	činit
25-35	[number]	k4	25-35
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ohrožení	ohrožení	k1gNnSc1	ohrožení
==	==	k?	==
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
celosvětově	celosvětově	k6eAd1	celosvětově
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
podle	podle	k7c2	podle
červeného	červený	k2eAgInSc2d1	červený
seznamu	seznam	k1gInSc2	seznam
IUCN	IUCN	kA	IUCN
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
uveden	uvést	k5eAaPmNgInS	uvést
také	také	k9	také
v	v	k7c6	v
příloze	příloha	k1gFnSc6	příloha
II	II	kA	II
a	a	k8xC	a
IV	IV	kA	IV
směrnice	směrnice	k1gFnSc2	směrnice
o	o	k7c6	o
stanovištích	stanoviště	k1gNnPc6	stanoviště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stupeň	stupeň	k1gInSc1	stupeň
ohrožení	ohrožení	k1gNnSc2	ohrožení
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
červeném	červený	k2eAgInSc6d1	červený
seznamu	seznam	k1gInSc6	seznam
je	být	k5eAaImIp3nS	být
taktéž	taktéž	k?	taktéž
"	"	kIx"	"
<g/>
ohrožený	ohrožený	k2eAgMnSc1d1	ohrožený
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
EN	EN	kA	EN
<g/>
)	)	kIx)	)
Vyhláška	vyhláška	k1gFnSc1	vyhláška
395	[number]	k4	395
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
ve	v	k7c6	v
znění	znění	k1gNnSc6	znění
vyhl	vyhla	k1gFnPc2	vyhla
<g/>
.	.	kIx.	.
175	[number]	k4	175
<g/>
/	/	kIx~	/
<g/>
2006	[number]	k4	2006
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
ho	on	k3xPp3gMnSc2	on
uvádí	uvádět	k5eAaImIp3nS	uvádět
mezi	mezi	k7c7	mezi
druhy	druh	k1gInPc7	druh
silně	silně	k6eAd1	silně
ohrožené	ohrožený	k2eAgNnSc1d1	ohrožené
(	(	kIx(	(
<g/>
uveden	uvést	k5eAaPmNgInS	uvést
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
vyhlášce	vyhláška	k1gFnSc6	vyhláška
175	[number]	k4	175
<g/>
/	/	kIx~	/
<g/>
2006	[number]	k4	2006
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
se	se	k3xPyFc4	se
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
polohách	poloha	k1gFnPc6	poloha
v	v	k7c6	v
čistších	čistý	k2eAgInPc6d2	čistší
potocích	potok	k1gInPc6	potok
nebo	nebo	k8xC	nebo
i	i	k8xC	i
řekách	řeka	k1gFnPc6	řeka
či	či	k8xC	či
říčkách	říčka	k1gFnPc6	říčka
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
ČR	ČR	kA	ČR
např.	např.	kA	např.
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
lokalitách	lokalita	k1gFnPc6	lokalita
<g/>
:	:	kIx,	:
PP	PP	kA	PP
Vlašimská	vlašimský	k2eAgFnSc1d1	Vlašimská
Blanice	Blanice	k1gFnSc1	Blanice
<g/>
,	,	kIx,	,
PP	PP	kA	PP
Bystřice	Bystřice	k1gFnSc1	Bystřice
<g/>
,	,	kIx,	,
PP	PP	kA	PP
Lukavecký	Lukavecký	k2eAgInSc1d1	Lukavecký
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
PP	PP	kA	PP
Lužnice	Lužnice	k1gFnSc1	Lužnice
<g/>
,	,	kIx,	,
EVL	EVL	kA	EVL
Rokytná	Rokytný	k2eAgFnSc1d1	Rokytná
<g/>
,	,	kIx,	,
Nežárka	Nežárka	k1gFnSc1	Nežárka
<g/>
,	,	kIx,	,
Sázava	Sázava	k1gFnSc1	Sázava
<g/>
,	,	kIx,	,
Klíčava	Klíčava	k1gFnSc1	Klíčava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
velevrub	velevrub	k1gMnSc1	velevrub
tupý	tupý	k2eAgMnSc1d1	tupý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Douda	Douda	k1gMnSc1	Douda
<g/>
,	,	kIx,	,
K.	K.	kA	K.
<g/>
,	,	kIx,	,
Horký	horký	k2eAgInSc1d1	horký
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
,	,	kIx,	,
Bílý	bílý	k2eAgMnSc1d1	bílý
<g/>
,	,	kIx,	,
M.	M.	kA	M.
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Host	host	k1gMnSc1	host
limitation	limitation	k1gInSc1	limitation
of	of	k?	of
the	the	k?	the
thick-shelled	thickhelled	k1gMnSc1	thick-shelled
river	river	k1gMnSc1	river
mussel	mussel	k1gMnSc1	mussel
<g/>
:	:	kIx,	:
identifying	identifying	k1gInSc1	identifying
the	the	k?	the
threats	threats	k6eAd1	threats
to	ten	k3xDgNnSc4	ten
declining	declining	k1gInSc1	declining
affiliate	affiliat	k1gInSc5	affiliat
species	species	k1gFnSc1	species
<g/>
.	.	kIx.	.
</s>
<s>
Animal	animal	k1gMnSc1	animal
Conservation	Conservation	k1gInSc1	Conservation
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
velevrub	velevrub	k1gMnSc1	velevrub
tupý	tupý	k2eAgMnSc1d1	tupý
na	na	k7c6	na
BioLibu	BioLib	k1gInSc6	BioLib
</s>
</p>
<p>
<s>
velevrub	velevrub	k1gMnSc1	velevrub
tupý	tupý	k2eAgMnSc1d1	tupý
na	na	k7c4	na
natura	natur	k1gMnSc4	natur
<g/>
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
velevrub	velevrub	k1gMnSc1	velevrub
tupý	tupý	k2eAgMnSc1d1	tupý
na	na	k7c6	na
The	The	k1gFnSc6	The
MUSSEL	MUSSEL	kA	MUSSEL
Project	Project	k2eAgInSc4d1	Project
Web	web	k1gInSc4	web
Site	Sit	k1gFnSc2	Sit
</s>
</p>
<p>
<s>
Beran	Beran	k1gMnSc1	Beran
L.	L.	kA	L.
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Příspěvek	příspěvek	k1gInSc1	příspěvek
k	k	k7c3	k
poznání	poznání	k1gNnSc3	poznání
vodních	vodní	k2eAgMnPc2d1	vodní
měkkýšů	měkkýš	k1gMnPc2	měkkýš
evropsky	evropsky	k6eAd1	evropsky
významné	významný	k2eAgFnPc1d1	významná
lokality	lokalita	k1gFnPc1	lokalita
Bystřice	Bystřice	k1gFnSc2	Bystřice
se	s	k7c7	s
zaměřením	zaměření	k1gNnSc7	zaměření
na	na	k7c6	na
populaci	populace	k1gFnSc6	populace
velevruba	velevrub	k1gMnSc2	velevrub
tupého	tupý	k2eAgMnSc2d1	tupý
(	(	kIx(	(
<g/>
Unio	Unio	k1gMnSc1	Unio
crassus	crassus	k1gMnSc1	crassus
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Malacologica	Malacologic	k2eAgFnSc1d1	Malacologic
Bohemoslovaca	Bohemoslovaca	k1gFnSc1	Bohemoslovaca
10	[number]	k4	10
<g/>
:	:	kIx,	:
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
</s>
<s>
Online	Onlinout	k5eAaPmIp3nS	Onlinout
verze	verze	k1gFnSc1	verze
<	<	kIx(	<
<g/>
http://mollusca.sav.sk	[url]	k1gInSc1	http://mollusca.sav.sk
<g/>
>	>	kIx)	>
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
PDF	PDF	kA	PDF
<g/>
.	.	kIx.	.
</s>
</p>
