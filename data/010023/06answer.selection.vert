<s>
Jedním	jeden	k4xCgInSc7
z	z	k7c2
nejvýznamnějších	významný	k2eAgMnPc2d3
žáků	žák	k1gMnPc2
Akademie	akademie	k1gFnSc2
byl	být	k5eAaImAgInS
Aristotelés	Aristotelés	k1gInSc1
<g/>
,	,	kIx,
po	po	k7c6
Platónově	Platónův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
však	však	k8xC
Akademii	akademie	k1gFnSc6
převzal	převzít	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc1
synovec	synovec	k1gMnSc1
Speusippos	Speusippos	k1gMnSc1
<g/>
.	.	kIx.
</s>