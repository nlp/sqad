<s>
Klínovec	Klínovec	k1gInSc1
</s>
<s>
Možná	možná	k9
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
vrchol	vrchol	k1gInSc1
v	v	k7c6
Hrubém	hrubý	k2eAgInSc6d1
Jeseníku	Jeseník	k1gInSc6
Velký	velký	k2eAgInSc4d1
Klínovec	Klínovec	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Klínovec	Klínovec	k1gInSc1
Rozhledna	rozhledna	k1gFnSc1
s	s	k7c7
hotelem	hotel	k1gInSc7
</s>
<s>
Vrchol	vrchol	k1gInSc1
</s>
<s>
1244	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Prominence	prominence	k1gFnSc2
</s>
<s>
748	#num#	k4
m	m	kA
↓	↓	k?
V	V	kA
od	od	k7c2
Nového	Nového	k2eAgInSc2d1
Klíčova	Klíčův	k2eAgInSc2d1
Izolace	izolace	k1gFnPc1
</s>
<s>
133	#num#	k4
km	km	kA
→	→	k?
Ostrý	ostrý	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Seznamy	seznam	k1gInPc1
</s>
<s>
Tisícovky	tisícovka	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
#	#	kIx~
<g/>
87	#num#	k4
<g/>
Ultratisícovky	Ultratisícovka	k1gFnPc1
#	#	kIx~
<g/>
31	#num#	k4
<g/>
Nejprominentnější	prominentní	k2eAgFnPc1d3
hory	hora	k1gFnPc1
CZ	CZ	kA
#	#	kIx~
<g/>
4	#num#	k4
<g/>
Nejizolovanější	izolovaný	k2eAgFnPc1d3
hory	hora	k1gFnPc1
CZ	CZ	kA
#	#	kIx~
<g/>
2	#num#	k4
<g/>
Hory	hora	k1gFnSc2
a	a	k8xC
kopce	kopec	k1gInSc2
Krušných	krušný	k2eAgFnPc2d1
hor	hora	k1gFnPc2
#	#	kIx~
<g/>
1	#num#	k4
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc4
Pohoří	pohořet	k5eAaPmIp3nS
</s>
<s>
Krušné	krušný	k2eAgFnPc1d1
hory	hora	k1gFnPc1
/	/	kIx~
Klínovecká	klínovecký	k2eAgFnSc1d1
hornatina	hornatina	k1gFnSc1
/	/	kIx~
Jáchymovská	jáchymovský	k2eAgFnSc1d1
hornatina	hornatina	k1gFnSc1
/	/	kIx~
Vykmanovská	Vykmanovský	k2eAgFnSc1d1
hornatina	hornatina	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
23	#num#	k4
<g/>
′	′	k?
<g/>
46	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
12	#num#	k4
<g/>
°	°	k?
<g/>
58	#num#	k4
<g/>
′	′	k?
<g/>
4	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Klínovec	Klínovec	k1gInSc1
</s>
<s>
Hornina	hornina	k1gFnSc1
</s>
<s>
svory	svor	k1gInPc1
a	a	k8xC
ortoruly	ortorul	k1gInPc1
Povodí	povodí	k1gNnSc2
</s>
<s>
Ohře	Ohře	k1gFnSc1
<g/>
,	,	kIx,
Mulda	mulda	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Klínovec	Klínovec	k1gInSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Keilberg	Keilberg	k1gInSc1
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
též	též	k9
Sonnenwirbel	Sonnenwirbel	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1244	#num#	k4
metrů	metr	k1gInPc2
nad	nad	k7c7
mořem	moře	k1gNnSc7
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
hora	hora	k1gFnSc1
v	v	k7c6
severozápadních	severozápadní	k2eAgFnPc6d1
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgInSc1d3
vrchol	vrchol	k1gInSc1
Krušných	krušný	k2eAgFnPc2d1
hor	hora	k1gFnPc2
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgInSc1d3
bod	bod	k1gInSc1
okresů	okres	k1gInPc2
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
a	a	k8xC
Chomutov	Chomutov	k1gInSc1
a	a	k8xC
také	také	k9
Karlovarského	karlovarský	k2eAgInSc2d1
a	a	k8xC
Ústeckého	ústecký	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
čtvrtou	čtvrtý	k4xOgFnSc4
nejprominentnější	prominentní	k2eAgFnSc4d3
českou	český	k2eAgFnSc4d1
horu	hora	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
vrcholové	vrcholový	k2eAgFnSc6d1
plošině	plošina	k1gFnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
současnosti	současnost	k1gFnSc6
uzavřený	uzavřený	k2eAgInSc1d1
hotel	hotel	k1gInSc1
s	s	k7c7
24	#num#	k4
metrů	metr	k1gInPc2
vysokou	vysoký	k2eAgFnSc7d1
rozhlednou	rozhledna	k1gFnSc7
(	(	kIx(
<g/>
nově	nově	k6eAd1
opravenou	opravený	k2eAgFnSc4d1
a	a	k8xC
zprovozněnou	zprovozněný	k2eAgFnSc4d1
<g/>
)	)	kIx)
a	a	k8xC
telekomunikační	telekomunikační	k2eAgFnSc1d1
betonová	betonový	k2eAgFnSc1d1
věž	věž	k1gFnSc1
vysoká	vysoký	k2eAgFnSc1d1
80	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
jižní	jižní	k2eAgFnSc2d1
strany	strana	k1gFnSc2
vede	vést	k5eAaImIp3nS
k	k	k7c3
vrcholu	vrchol	k1gInSc3
lanová	lanový	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
Jáchymov	Jáchymov	k1gInSc1
-	-	kIx~
Klínovec	Klínovec	k1gInSc1
<g/>
,	,	kIx,
ze	z	k7c2
severní	severní	k2eAgFnSc2d1
strany	strana	k1gFnSc2
vede	vést	k5eAaImIp3nS
další	další	k2eAgFnSc1d1
lanová	lanový	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
Dámská	dámský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhledna	rozhledna	k1gFnSc1
i	i	k8xC
hotel	hotel	k1gInSc1
jsou	být	k5eAaImIp3nP
nejstaršími	starý	k2eAgFnPc7d3
a	a	k8xC
nejvýše	vysoce	k6eAd3,k6eAd1
položenými	položený	k2eAgFnPc7d1
budovami	budova	k1gFnPc7
v	v	k7c6
Krušných	krušný	k2eAgFnPc6d1
horách	hora	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skiareál	Skiareál	k1gInSc1
Klínovec	Klínovec	k1gInSc4
je	být	k5eAaImIp3nS
největší	veliký	k2eAgInSc4d3
lyžařský	lyžařský	k2eAgInSc4d1
areál	areál	k1gInSc4
v	v	k7c6
Krušných	krušný	k2eAgFnPc6d1
horách	hora	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Poloha	poloha	k1gFnSc1
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
poblíž	poblíž	k7c2
Božího	boží	k2eAgInSc2d1
Daru	dar	k1gInSc2
<g/>
,	,	kIx,
4	#num#	k4
km	km	kA
severovýchodně	severovýchodně	k6eAd1
od	od	k7c2
Jáchymova	Jáchymov	k1gInSc2
v	v	k7c6
okrese	okres	k1gInSc6
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
na	na	k7c6
hřebeni	hřeben	k1gInSc6
Krušných	krušný	k2eAgFnPc2d1
hor.	hor.	k?
Asi	asi	k9
2	#num#	k4
kilometry	kilometr	k1gInPc7
severozápadně	severozápadně	k6eAd1
leží	ležet	k5eAaImIp3nP
hraniční	hraniční	k2eAgInSc4d1
přechod	přechod	k1gInSc4
s	s	k7c7
Německem	Německo	k1gNnSc7
a	a	k8xC
město	město	k1gNnSc1
Boží	božit	k5eAaImIp3nS
Dar	dar	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
severními	severní	k2eAgInPc7d1
svahy	svah	k1gInPc7
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
německé	německý	k2eAgNnSc1d1
lázeňské	lázeňský	k2eAgNnSc1d1
město	město	k1gNnSc1
Oberwiesenthal	Oberwiesenthal	k1gMnSc1
<g/>
,	,	kIx,
nad	nad	k7c7
ním	on	k3xPp3gMnSc7
se	se	k3xPyFc4
tyčí	tyčit	k5eAaImIp3nS
masiv	masiv	k1gInSc1
hory	hora	k1gFnSc2
Fichtelberg	Fichtelberg	k1gMnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
jen	jen	k9
o	o	k7c4
30	#num#	k4
metrů	metr	k1gInPc2
nižší	nízký	k2eAgMnSc1d2
než	než	k8xS
Klínovec	Klínovec	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Východně	východně	k6eAd1
<g/>
,	,	kIx,
podél	podél	k7c2
hraničního	hraniční	k2eAgInSc2d1
potoka	potok	k1gInSc2
Polava	Polava	k1gFnSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
pramení	pramenit	k5eAaImIp3nS
na	na	k7c6
svazích	svah	k1gInPc6
Klínovce	Klínovec	k1gInSc2
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
,	,	kIx,
leží	ležet	k5eAaImIp3nP
české	český	k2eAgNnSc4d1
město	město	k1gNnSc4
Loučná	Loučný	k2eAgNnPc4d1
pod	pod	k7c7
Klínovcem	Klínovec	k1gInSc7
a	a	k8xC
ještě	ještě	k9
východněji	východně	k6eAd2
za	za	k7c7
holým	holý	k2eAgInSc7d1
a	a	k8xC
plochým	plochý	k2eAgInSc7d1
hřebenem	hřeben	k1gInSc7
Háj	háj	k1gInSc1
u	u	k7c2
Loučné	Loučný	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1
roční	roční	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
vzduchu	vzduch	k1gInSc2
je	být	k5eAaImIp3nS
2,6	2,6	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
roční	roční	k2eAgInSc1d1
úhrn	úhrn	k1gInSc1
srážek	srážka	k1gFnPc2
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
kolem	kolem	k7c2
986	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Klinovec	Klinovec	k1gMnSc1
<g/>
,	,	kIx,
mapa	mapa	k1gFnSc1
lyžařského	lyžařský	k2eAgNnSc2d1
střediska	středisko	k1gNnSc2
</s>
<s>
V	v	k7c6
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
o	o	k7c6
Klínovci	Klínovec	k1gInSc6
se	se	k3xPyFc4
zmínil	zmínit	k5eAaPmAgMnS
Johannes	Johannes	k1gMnSc1
Mathesius	Mathesius	k1gMnSc1
i	i	k9
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
lavinami	lavina	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
měly	mít	k5eAaImAgFnP
za	za	k7c4
následek	následek	k1gInSc4
zničení	zničení	k1gNnSc2
lesů	les	k1gInPc2
<g/>
,	,	kIx,
poškození	poškození	k1gNnSc2
několika	několik	k4yIc2
domů	dům	k1gInPc2
a	a	k8xC
dokonce	dokonce	k9
několik	několik	k4yIc1
lidských	lidský	k2eAgFnPc2d1
obětí	oběť	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
historii	historie	k1gFnSc6
nesla	nést	k5eAaImAgFnS
oblast	oblast	k1gFnSc1
Klínovce	Klínovec	k1gInSc2
jméno	jméno	k1gNnSc1
Bartum	Bartum	k1gNnSc1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
zkomolenina	zkomolenina	k1gFnSc1
od	od	k7c2
Bartholomäusberg	Bartholomäusberg	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1817	#num#	k4
postavilo	postavit	k5eAaPmAgNnS
město	město	k1gNnSc1
Jáchymov	Jáchymov	k1gInSc1
na	na	k7c6
vrcholu	vrchol	k1gInSc6
vyhlídkovou	vyhlídkový	k2eAgFnSc4d1
věž	věž	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1868	#num#	k4
shořela	shořet	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
první	první	k4xOgFnSc4
vyhlídkovou	vyhlídkový	k2eAgFnSc4d1
věž	věž	k1gFnSc4
navštěvovali	navštěvovat	k5eAaImAgMnP
často	často	k6eAd1
i	i	k8xC
lázeňští	lázeňský	k2eAgMnPc1d1
hosté	host	k1gMnPc1
z	z	k7c2
Karlových	Karlův	k2eAgInPc2d1
Varů	Vary	k1gInPc2
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
přijížděli	přijíždět	k5eAaImAgMnP
kočáry	kočár	k1gInPc4
taženými	tažený	k2eAgMnPc7d1
koňmi	kůň	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1883	#num#	k4
začal	začít	k5eAaPmAgInS
krušnohorský	krušnohorský	k2eAgInSc1d1
spolek	spolek	k1gInSc1
z	z	k7c2
Jáchymova	Jáchymov	k1gInSc2
se	s	k7c7
stavbou	stavba	k1gFnSc7
kamenné	kamenný	k2eAgFnSc2d1
rozhledny	rozhledna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgNnPc1
byla	být	k5eAaImAgNnP
v	v	k7c6
srpnu	srpen	k1gInSc6
následujícího	následující	k2eAgInSc2d1
roku	rok	k1gInSc2
pod	pod	k7c7
názvem	název	k1gInSc7
„	„	k?
<g/>
Kaiser-Franz-Josephs-Turm	Kaiser-Franz-Josephs-Turm	k1gInSc1
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Věž	věž	k1gFnSc1
císaře	císař	k1gMnSc2
Františka	František	k1gMnSc2
Josefa	Josef	k1gMnSc2
<g/>
“	“	k?
slavnostně	slavnostně	k6eAd1
otevřena	otevřen	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Věž	věž	k1gFnSc1
vysoká	vysoká	k1gFnSc1
24	#num#	k4
metrů	metr	k1gInPc2
stojí	stát	k5eAaImIp3nS
na	na	k7c6
vrcholu	vrchol	k1gInSc6
dodnes	dodnes	k6eAd1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
po	po	k7c6
rekonstrukci	rekonstrukce	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
její	její	k3xOp3gInSc4
vrchol	vrchol	k1gInSc4
vede	vést	k5eAaImIp3nS
75	#num#	k4
schodů	schod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc4
návštěvníků	návštěvník	k1gMnPc2
rychle	rychle	k6eAd1
vzrůstal	vzrůstat	k5eAaImAgInS
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
například	například	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1885	#num#	k4
vrchol	vrchol	k1gInSc4
navštívilo	navštívit	k5eAaPmAgNnS
6000	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
však	však	k9
jen	jen	k9
4623	#num#	k4
vystoupilo	vystoupit	k5eAaPmAgNnS
na	na	k7c4
věž	věž	k1gFnSc4
<g/>
,	,	kIx,
důvodem	důvod	k1gInSc7
<g/>
,	,	kIx,
jak	jak	k6eAd1
popsal	popsat	k5eAaPmAgMnS
jeden	jeden	k4xCgMnSc1
ze	z	k7c2
současníků	současník	k1gMnPc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
především	především	k9
fyzická	fyzický	k2eAgFnSc1d1
náročnost	náročnost	k1gFnSc1
výstupu	výstup	k1gInSc2
a	a	k8xC
velmi	velmi	k6eAd1
úzký	úzký	k2eAgInSc4d1
prostor	prostor	k1gInSc4
schodiště	schodiště	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Vysílač	vysílač	k1gInSc1
Klínovec	Klínovec	k1gInSc1
v	v	k7c6
únoru	únor	k1gInSc6
2007	#num#	k4
</s>
<s>
Výhled	výhled	k1gInSc1
z	z	k7c2
Klínovce	Klínovec	k1gInSc2
na	na	k7c4
německou	německý	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
</s>
<s>
Kvůli	kvůli	k7c3
silnému	silný	k2eAgInSc3d1
větru	vítr	k1gInSc3
panujícímu	panující	k2eAgInSc3d1
téměř	téměř	k6eAd1
celoročně	celoročně	k6eAd1
byla	být	k5eAaImAgFnS
vrchní	vrchní	k2eAgFnSc1d1
plošina	plošina	k1gFnSc1
rozhledny	rozhledna	k1gFnSc2
zastřešena	zastřešen	k2eAgFnSc1d1
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1888	#num#	k4
ji	on	k3xPp3gFnSc4
kryla	krýt	k5eAaImAgFnS
skleněná	skleněný	k2eAgFnSc1d1
střecha	střecha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vstupné	vstupné	k1gNnSc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
bylo	být	k5eAaImAgNnS
10	#num#	k4
krejcarů	krejcar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
rozvojem	rozvoj	k1gInSc7
turistiky	turistika	k1gFnSc2
bylo	být	k5eAaImAgNnS
rozhodnuto	rozhodnout	k5eAaPmNgNnS
o	o	k7c6
přístavbě	přístavba	k1gFnSc6
sloužící	sloužící	k2eAgNnSc1d1
k	k	k7c3
ubytování	ubytování	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
byla	být	k5eAaImAgFnS
otevřena	otevřít	k5eAaPmNgFnS
18	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1893	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
provozu	provoz	k1gInSc6
byla	být	k5eAaImAgFnS
každoročně	každoročně	k6eAd1
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
a	a	k8xC
mimo	mimo	k6eAd1
provizorního	provizorní	k2eAgNnSc2d1
ubytování	ubytování	k1gNnSc2
pro	pro	k7c4
deset	deset	k4xCc4
osob	osoba	k1gFnPc2
poskytovala	poskytovat	k5eAaImAgFnS
i	i	k9
teplá	teplý	k2eAgNnPc4d1
a	a	k8xC
studená	studený	k2eAgNnPc4d1
jídla	jídlo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
přístavbou	přístavba	k1gFnSc7
to	ten	k3xDgNnSc4
byla	být	k5eAaImAgFnS
zřízena	zřízen	k2eAgFnSc1d1
i	i	k8xC
stáj	stáj	k1gFnSc1
pro	pro	k7c4
osm	osm	k4xCc4
koní	kůň	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
červnu	červen	k1gInSc3
1897	#num#	k4
byla	být	k5eAaImAgFnS
na	na	k7c6
Klínovci	Klínovec	k1gInSc6
zřízena	zřídit	k5eAaPmNgFnS
i	i	k8xC
poštovní	poštovní	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
za	za	k7c4
první	první	k4xOgNnSc4
léto	léto	k1gNnSc4
provozu	provoz	k1gInSc2
odeslala	odeslat	k5eAaPmAgFnS
do	do	k7c2
Božího	boží	k2eAgInSc2d1
Daru	dar	k1gInSc2
přes	přes	k7c4
7	#num#	k4
000	#num#	k4
zásilek	zásilka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1907	#num#	k4
byla	být	k5eAaImAgFnS
přistavěna	přistavět	k5eAaPmNgFnS
ještě	ještě	k9
jedna	jeden	k4xCgFnSc1
budova	budova	k1gFnSc1
s	s	k7c7
prostornou	prostorný	k2eAgFnSc7d1
halou	hala	k1gFnSc7
se	s	k7c7
stylovým	stylový	k2eAgInSc7d1
kazetovým	kazetový	k2eAgInSc7d1
stropem	strop	k1gInSc7
<g/>
,	,	kIx,
zobrazujícím	zobrazující	k2eAgInSc6d1
znaky	znak	k1gInPc7
českých	český	k2eAgNnPc2d1
krušnohorských	krušnohorský	k2eAgNnPc2d1
měst	město	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
příležitosti	příležitost	k1gFnSc2
jubilejní	jubilejní	k2eAgFnSc2d1
výstavy	výstava	k1gFnSc2
pořádané	pořádaný	k2eAgFnSc2d1
na	na	k7c4
počest	počest	k1gFnSc4
císaře	císař	k1gMnSc2
Františka	František	k1gMnSc2
Josefa	Josef	k1gMnSc2
I.	I.	kA
v	v	k7c6
ní	on	k3xPp3gFnSc6
byly	být	k5eAaImAgFnP
vystaveny	vystaven	k2eAgFnPc1d1
průmyslové	průmyslový	k2eAgFnPc1d1
a	a	k8xC
řemeslné	řemeslný	k2eAgInPc1d1
výrobky	výrobek	k1gInPc1
z	z	k7c2
českého	český	k2eAgNnSc2d1
krušnohoří	krušnohoří	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Před	před	k7c7
vypuknutím	vypuknutí	k1gNnSc7
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
byl	být	k5eAaImAgInS
hotel	hotel	k1gInSc1
rozšířen	rozšířit	k5eAaPmNgInS
a	a	k8xC
přistavěno	přistavěn	k2eAgNnSc1d1
bylo	být	k5eAaImAgNnS
další	další	k2eAgNnSc1d1
patro	patro	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
byla	být	k5eAaImAgFnS
dřívější	dřívější	k2eAgFnSc1d1
stáj	stáj	k1gFnSc1
přestavěna	přestavět	k5eAaPmNgFnS
na	na	k7c4
další	další	k2eAgFnPc4d1
ubytovací	ubytovací	k2eAgFnPc4d1
prostory	prostora	k1gFnPc4
a	a	k8xC
celý	celý	k2eAgInSc1d1
komplex	komplex	k1gInSc1
byl	být	k5eAaImAgInS
opláštěn	opláštit	k5eAaPmNgInS
dřevem	dřevo	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
na	na	k7c6
něm	on	k3xPp3gMnSc6
dochovalo	dochovat	k5eAaPmAgNnS
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1913	#num#	k4
měl	mít	k5eAaImAgInS
hotel	hotel	k1gInSc1
40	#num#	k4
ubytovacích	ubytovací	k2eAgFnPc2d1
místností	místnost	k1gFnPc2
pro	pro	k7c4
80	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
stájí	stáj	k1gFnPc2
pro	pro	k7c4
35	#num#	k4
koní	kůň	k1gMnPc2
zde	zde	k6eAd1
byly	být	k5eAaImAgFnP
i	i	k9
garáže	garáž	k1gFnPc1
pro	pro	k7c4
automobily	automobil	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Se	s	k7c7
vznikem	vznik	k1gInSc7
Československa	Československo	k1gNnSc2
<g/>
,	,	kIx,
ke	k	k7c3
kterému	který	k3yRgMnSc3,k3yQgMnSc3,k3yIgMnSc3
území	území	k1gNnSc1
připadlo	připadnout	k5eAaPmAgNnS
<g/>
,	,	kIx,
zmizela	zmizet	k5eAaPmAgFnS
i	i	k9
dvojjazyčnost	dvojjazyčnost	k1gFnSc1
v	v	k7c6
názvu	název	k1gInSc6
hory	hora	k1gFnSc2
<g/>
,	,	kIx,
nejprve	nejprve	k6eAd1
byl	být	k5eAaImAgInS
vybrán	vybrán	k2eAgInSc1d1
a	a	k8xC
používal	používat	k5eAaImAgInS
se	se	k3xPyFc4
název	název	k1gInSc1
Klín	klín	k1gInSc1
z	z	k7c2
německého	německý	k2eAgInSc2d1
názvu	název	k1gInSc2
Keilberg	Keilberg	k1gInSc1
-	-	kIx~
doslovně	doslovně	k6eAd1
„	„	k?
<g/>
klínová	klínový	k2eAgFnSc1d1
hora	hora	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
dvacátých	dvacátý	k4xOgNnPc2
let	léto	k1gNnPc2
se	se	k3xPyFc4
název	název	k1gInSc1
ustálil	ustálit	k5eAaPmAgInS
na	na	k7c4
dnešní	dnešní	k2eAgInSc4d1
Klínovec	Klínovec	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Dříve	dříve	k6eAd2
byla	být	k5eAaImAgFnS
na	na	k7c6
vrcholu	vrchol	k1gInSc6
i	i	k8xC
meteorologická	meteorologický	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc4
turistů	turist	k1gMnPc2
prudce	prudko	k6eAd1
vzrůstal	vzrůstat	k5eAaImAgInS
zejména	zejména	k9
díky	díky	k7c3
výstavbě	výstavba	k1gFnSc3
silnice	silnice	k1gFnSc2
přímo	přímo	k6eAd1
na	na	k7c4
vrchol	vrchol	k1gInSc4
a	a	k8xC
také	také	k9
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
z	z	k7c2
Karlových	Karlův	k2eAgInPc2d1
Varů	Vary	k1gInPc2
sem	sem	k6eAd1
pravidelně	pravidelně	k6eAd1
jezdil	jezdit	k5eAaImAgMnS
autobus	autobus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncem	koncem	k7c2
dvacátých	dvacátý	k4xOgNnPc2
let	léto	k1gNnPc2
byl	být	k5eAaImAgMnS
komplex	komplex	k1gInSc4
opět	opět	k6eAd1
rozšířen	rozšířen	k2eAgInSc1d1
a	a	k8xC
počet	počet	k1gInSc1
lůžek	lůžko	k1gNnPc2
se	se	k3xPyFc4
zvýšil	zvýšit	k5eAaPmAgInS
na	na	k7c4
100	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Hostinec	hostinec	k1gInSc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
provozovala	provozovat	k5eAaImAgFnS
rodina	rodina	k1gFnSc1
Wohlrab	Wohlrab	k1gMnSc1
<g/>
;	;	kIx,
měl	mít	k5eAaImAgInS
tři	tři	k4xCgFnPc4
velké	velký	k2eAgFnPc4d1
místnosti	místnost	k1gFnPc4
<g/>
:	:	kIx,
Dotzauerův	Dotzauerův	k2eAgInSc4d1
sál	sál	k1gInSc4
<g/>
,	,	kIx,
halu	hala	k1gFnSc4
Sobitschka	Sobitschek	k1gInSc2
a	a	k8xC
Müllerův	Müllerův	k2eAgInSc1d1
sál	sál	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc7d1
byl	být	k5eAaImAgInS
pojmenován	pojmenovat	k5eAaPmNgMnS
po	po	k7c6
okresním	okresní	k2eAgMnSc6d1
školním	školní	k2eAgMnSc6d1
inspektorovi	inspektor	k1gMnSc6
Antonu	Anton	k1gMnSc6
Müllerovi	Müller	k1gMnSc6
<g/>
,	,	kIx,
dlouholetém	dlouholetý	k2eAgInSc6d1
předsedovi	předseda	k1gMnSc3
jáchymovského	jáchymovský	k2eAgInSc2d1
krušnohorského	krušnohorský	k2eAgInSc2d1
spolku	spolek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
zimní	zimní	k2eAgInPc4d1
sporty	sport	k1gInPc4
byly	být	k5eAaImAgFnP
na	na	k7c6
svazích	svah	k1gInPc6
směrem	směr	k1gInSc7
k	k	k7c3
Jáchymovu	Jáchymov	k1gInSc3
vybudovány	vybudován	k2eAgMnPc4d1
sjezdovky	sjezdovka	k1gFnSc2
a	a	k8xC
sáňkařská	sáňkařský	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Směrem	směr	k1gInSc7
k	k	k7c3
Oberwiesenthalu	Oberwiesenthal	k1gInSc3
byl	být	k5eAaImAgMnS
od	od	k7c2
roku	rok	k1gInSc2
1922	#num#	k4
v	v	k7c6
provozu	provoz	k1gInSc6
skokanský	skokanský	k2eAgInSc4d1
můstek	můstek	k1gInSc4
<g/>
;	;	kIx,
zdejší	zdejší	k2eAgInSc4d1
rekord	rekord	k1gInSc4
ve	v	k7c6
skoku	skok	k1gInSc6
na	na	k7c6
lyžích	lyže	k1gFnPc6
50	#num#	k4
metrů	metr	k1gInPc2
vydržel	vydržet	k5eAaPmAgMnS
mnoho	mnoho	k4c1
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Před	před	k7c7
druhou	druhý	k4xOgFnSc7
světovou	světový	k2eAgFnSc7d1
válkou	válka	k1gFnSc7
měl	mít	k5eAaImAgInS
hotel	hotel	k1gInSc1
kapacitu	kapacita	k1gFnSc4
112	#num#	k4
míst	místo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
válce	válka	k1gFnSc6
nesla	nést	k5eAaImAgFnS
hora	hora	k1gFnSc1
oficiální	oficiální	k2eAgInSc4d1
český	český	k2eAgInSc4d1
název	název	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proud	proud	k1gInSc4
zejména	zejména	k9
německých	německý	k2eAgMnPc2d1
turistů	turist	k1gMnPc2
ustal	ustat	k5eAaPmAgMnS
a	a	k8xC
obnovil	obnovit	k5eAaPmAgMnS
se	se	k3xPyFc4
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1972	#num#	k4
<g/>
,	,	kIx,
po	po	k7c6
zrušení	zrušení	k1gNnSc6
vízové	vízový	k2eAgFnSc2d1
povinnosti	povinnost	k1gFnSc2
pro	pro	k7c4
občany	občan	k1gMnPc4
NDR	NDR	kA
<g/>
.	.	kIx.
</s>
<s>
Lanovky	lanovka	k1gFnPc1
na	na	k7c4
Klínovec	Klínovec	k1gInSc4
</s>
<s>
Lanovka	lanovka	k1gFnSc1
Jáchymov	Jáchymov	k1gInSc1
–	–	k?
Klínovec	Klínovec	k1gInSc1
</s>
<s>
Čtyřmístná	čtyřmístný	k2eAgFnSc1d1
lanová	lanový	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
Jáchymov	Jáchymov	k1gInSc1
–	–	k?
Klínovec	Klínovec	k1gInSc1
disponuje	disponovat	k5eAaBmIp3nS
odpojitelným	odpojitelný	k2eAgNnSc7d1
uchycením	uchycení	k1gNnSc7
(	(	kIx(
<g/>
lyží	lyže	k1gFnPc2
nebo	nebo	k8xC
kol	kolo	k1gNnPc2
<g/>
)	)	kIx)
a	a	k8xC
oranžovou	oranžový	k2eAgFnSc7d1
krycí	krycí	k2eAgFnSc7d1
bublinou	bublina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
byla	být	k5eAaImAgFnS
rekonstruovaná	rekonstruovaný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dolní	dolní	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
lanovky	lanovka	k1gFnSc2
je	být	k5eAaImIp3nS
vzdálena	vzdálen	k2eAgFnSc1d1
asi	asi	k9
3	#num#	k4
km	km	kA
od	od	k7c2
města	město	k1gNnSc2
Jáchymov	Jáchymov	k1gInSc1
<g/>
,	,	kIx,
délka	délka	k1gFnSc1
lanovky	lanovka	k1gFnSc2
je	být	k5eAaImIp3nS
2	#num#	k4
168	#num#	k4
m	m	kA
a	a	k8xC
převýšení	převýšení	k1gNnSc2
480	#num#	k4
m.	m.	k?
V	v	k7c6
zimě	zima	k1gFnSc6
přepravuje	přepravovat	k5eAaImIp3nS
lanovka	lanovka	k1gFnSc1
lyžaře	lyžař	k1gMnPc4
a	a	k8xC
v	v	k7c6
létě	léto	k1gNnSc6
umožňuje	umožňovat	k5eAaImIp3nS
nástup	nástup	k1gInSc1
i	i	k9
cyklistům	cyklista	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomu	ten	k3xDgInSc3
mohou	moct	k5eAaImIp3nP
cyklisté	cyklista	k1gMnPc1
bez	bez	k7c2
větší	veliký	k2eAgFnSc2d2
námahy	námaha	k1gFnSc2
nastoupit	nastoupit	k5eAaPmF
na	na	k7c4
cyklotrasy	cyklotrasa	k1gFnPc4
vedoucí	vedoucí	k1gFnSc2
z	z	k7c2
vrcholku	vrcholek	k1gInSc2
Klínovce	Klínovec	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nová	nový	k2eAgFnSc1d1
čtyřmístná	čtyřmístný	k2eAgFnSc1d1
lanová	lanový	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
Dámská	dámský	k2eAgFnSc1d1
ze	z	k7c2
severní	severní	k2eAgFnSc2d1
strany	strana	k1gFnSc2
byla	být	k5eAaImAgFnS
uvedena	uvést	k5eAaPmNgFnS
do	do	k7c2
provozu	provoz	k1gInSc2
o	o	k7c6
Vánocích	Vánoce	k1gFnPc6
2019	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Její	její	k3xOp3gFnSc1
délka	délka	k1gFnSc1
je	být	k5eAaImIp3nS
1210	#num#	k4
m	m	kA
a	a	k8xC
převýšení	převýšení	k1gNnSc2
232	#num#	k4
m.	m.	k?
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Prominence	prominence	k1gFnPc1
a	a	k8xC
izolace	izolace	k1gFnSc1
na	na	k7c4
Ultratisicovky	Ultratisicovka	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Základní	základní	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
ČR	ČR	kA
1	#num#	k4
<g/>
:	:	kIx,
<g/>
50	#num#	k4
000	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zeměměřický	zeměměřický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Lanovka	lanovka	k1gFnSc1
Klínovec	Klínovec	k1gInSc1
<g/>
↑	↑	k?
Lyžaře	lyžař	k1gMnSc2
vyveze	vyvézt	k5eAaPmIp3nS
na	na	k7c4
Klínovec	Klínovec	k1gInSc4
nová	nový	k2eAgFnSc1d1
lanovka	lanovka	k1gFnSc1
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
rychlejší	rychlý	k2eAgFnSc1d2
a	a	k8xC
vyhřívaná	vyhřívaný	k2eAgFnSc1d1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Klínovec	Klínovec	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Klínovec	Klínovec	k1gInSc1
na	na	k7c6
Krusnohorsky	Krusnohorsky	k1gFnSc6
<g/>
.	.	kIx.
<g/>
cz	cz	k?
Klínovec	Klínovec	k1gInSc1
na	na	k7c4
Krusnohorsky	Krusnohorsky	k1gFnSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
]	]	kIx)
</s>
<s>
Klínovec	Klínovec	k1gInSc1
na	na	k7c4
Tisicovky	Tisicovka	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
informace	informace	k1gFnPc4
a	a	k8xC
fotografie	fotografia	k1gFnPc4
</s>
<s>
Klínovec	Klínovec	k1gInSc1
na	na	k7c4
Holidayinfo	Holidayinfo	k1gNnSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
stav	stav	k1gInSc1
počasí	počasí	k1gNnSc1
a	a	k8xC
aktuální	aktuální	k2eAgInPc1d1
obrázky	obrázek	k1gInPc1
z	z	k7c2
kamery	kamera	k1gFnSc2
na	na	k7c6
Klínovci	Klínovec	k1gInSc6
</s>
<s>
Ski	ski	k1gFnSc1
areál	areál	k1gInSc1
Klínovec	Klínovec	k1gInSc1
</s>
<s>
Ski	ski	k1gFnSc1
areál	areál	k1gInSc1
Klínovec	Klínovec	k1gInSc1
na	na	k7c4
Ceske-sjezdovky	Ceske-sjezdovka	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Historické	historický	k2eAgInPc1d1
snímky	snímek	k1gInPc1
Klínovce	Klínovec	k1gInSc2
na	na	k7c4
Prirodakarlovarska	Prirodakarlovarsko	k1gNnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Krušnohorské	krušnohorský	k2eAgFnSc2d1
tisícovky	tisícovka	k1gFnSc2
Hlavní	hlavní	k2eAgInPc1d1
vrcholy	vrchol	k1gInPc1
(	(	kIx(
<g/>
14	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Klínovec	Klínovec	k1gInSc1
(	(	kIx(
<g/>
1244	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Božídarský	božídarský	k2eAgInSc4d1
Špičák	špičák	k1gInSc4
(	(	kIx(
<g/>
1116	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Macecha	macecha	k1gFnSc1
(	(	kIx(
<g/>
1114	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Meluzína	Meluzína	k1gFnSc1
(	(	kIx(
<g/>
1097	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Nad	nad	k7c7
Ryžovnou	ryžovna	k1gFnSc7
(	(	kIx(
<g/>
1054	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Blatenský	blatenský	k2eAgInSc4d1
vrch	vrch	k1gInSc4
(	(	kIx(
<g/>
1043	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Hubertky	Hubertka	k1gFnPc1
(	(	kIx(
<g/>
1032	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Plešivec	plešivec	k1gMnSc1
(	(	kIx(
<g/>
1028	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Loučná	Loučné	k1gNnPc1
(	(	kIx(
<g/>
1019	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Zaječí	zaječet	k5eAaPmIp3nS
vrch	vrch	k1gInSc1
(	(	kIx(
<g/>
1009	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
seč	seč	k1gFnSc1
(	(	kIx(
<g/>
1007	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Tetřeví	tetřeví	k2eAgFnSc1d1
hora	hora	k1gFnSc1
(	(	kIx(
<g/>
1007	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Dub	dub	k1gInSc1
S	s	k7c7
(	(	kIx(
<g/>
1005	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Perninský	Perninský	k2eAgInSc4d1
vrch	vrch	k1gInSc4
(	(	kIx(
<g/>
1000	#num#	k4
m	m	kA
<g/>
)	)	kIx)
Vedlejší	vedlejší	k2eAgInPc1d1
vrcholy	vrchol	k1gInPc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nad	nad	k7c7
Ryžovnou	ryžovna	k1gFnSc7
JV	JV	kA
(	(	kIx(
<g/>
1047	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Nad	nad	k7c7
Ryžovnou	ryžovna	k1gFnSc7
JZ	JZ	kA
(	(	kIx(
<g/>
1046	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Na	na	k7c6
skalách	skála	k1gFnPc6
(	(	kIx(
<g/>
1037	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Křížová	Křížová	k1gFnSc1
hora	hora	k1gFnSc1
(	(	kIx(
<g/>
1027	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Loučná	Loučný	k2eAgFnSc1d1
J	J	kA
(	(	kIx(
<g/>
1019	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Dub	dub	k1gInSc1
(	(	kIx(
<g/>
1001	#num#	k4
m	m	kA
<g/>
)	)	kIx)
Německé	německý	k2eAgInPc1d1
vrcholy	vrchol	k1gInPc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Fichtelberg	Fichtelberg	k1gInSc1
(	(	kIx(
<g/>
1214	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Kleiner	Kleiner	k1gMnSc1
Fichtelberg	Fichtelberg	k1gMnSc1
(	(	kIx(
<g/>
1206	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Eisenberg	Eisenberg	k1gInSc1
(	(	kIx(
<g/>
1028	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Auersberg	Auersberg	k1gInSc1
(	(	kIx(
<g/>
1019	#num#	k4
m	m	kA
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Geografie	geografie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
161025	#num#	k4
</s>
