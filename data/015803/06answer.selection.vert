<s>
Klínovec	Klínovec	k1gInSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Keilberg	Keilberg	k1gInSc1
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
též	též	k9
Sonnenwirbel	Sonnenwirbel	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1244	#num#	k4
metrů	metr	k1gInPc2
nad	nad	k7c7
mořem	moře	k1gNnSc7
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
hora	hora	k1gFnSc1
v	v	k7c6
severozápadních	severozápadní	k2eAgFnPc6d1
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgInSc1d3
vrchol	vrchol	k1gInSc1
Krušných	krušný	k2eAgFnPc2d1
hor	hora	k1gFnPc2
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgInSc1d3
bod	bod	k1gInSc1
okresů	okres	k1gInPc2
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
a	a	k8xC
Chomutov	Chomutov	k1gInSc1
a	a	k8xC
také	také	k9
Karlovarského	karlovarský	k2eAgInSc2d1
a	a	k8xC
Ústeckého	ústecký	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
</s>