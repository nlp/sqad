<p>
<s>
Počítačová	počítačový	k2eAgFnSc1d1	počítačová
klávesnice	klávesnice	k1gFnSc1	klávesnice
je	být	k5eAaImIp3nS	být
vstupní	vstupní	k2eAgNnSc1d1	vstupní
periferní	periferní	k2eAgNnSc1d1	periferní
zařízení	zařízení	k1gNnSc1	zařízení
určené	určený	k2eAgNnSc1d1	určené
pro	pro	k7c4	pro
vkládání	vkládání	k1gNnSc4	vkládání
znaků	znak	k1gInPc2	znak
do	do	k7c2	do
počítače	počítač	k1gInSc2	počítač
a	a	k8xC	a
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
ovládání	ovládání	k1gNnSc3	ovládání
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
odvozená	odvozený	k2eAgFnSc1d1	odvozená
od	od	k7c2	od
klávesnice	klávesnice	k1gFnSc2	klávesnice
psacího	psací	k2eAgInSc2d1	psací
stroje	stroj	k1gInSc2	stroj
či	či	k8xC	či
dálnopisu	dálnopis	k1gInSc2	dálnopis
<g/>
.	.	kIx.	.
</s>
<s>
Standardní	standardní	k2eAgFnPc1d1	standardní
počítačové	počítačový	k2eAgFnPc1d1	počítačová
klávesnice	klávesnice	k1gFnPc1	klávesnice
jsou	být	k5eAaImIp3nP	být
napájeny	napájet	k5eAaImNgFnP	napájet
z	z	k7c2	z
počítače	počítač	k1gInSc2	počítač
a	a	k8xC	a
komunikují	komunikovat	k5eAaImIp3nP	komunikovat
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
po	po	k7c6	po
sériové	sériový	k2eAgFnSc6d1	sériová
lince	linka	k1gFnSc6	linka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počítačová	počítačový	k2eAgFnSc1d1	počítačová
klávesnice	klávesnice	k1gFnSc1	klávesnice
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
vrchní	vrchní	k2eAgFnSc6d1	vrchní
straně	strana	k1gFnSc6	strana
tlačítka	tlačítko	k1gNnSc2	tlačítko
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgFnSc2d1	zvaná
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
stisk	stisk	k1gInSc4	stisk
klávesy	klávesa	k1gFnSc2	klávesa
způsobí	způsobit	k5eAaPmIp3nS	způsobit
odeslání	odeslání	k1gNnSc3	odeslání
jednoho	jeden	k4xCgInSc2	jeden
povelu	povel	k1gInSc2	povel
<g/>
,	,	kIx,	,
např.	např.	kA	např.
znaku	znak	k1gInSc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
klávesy	kláves	k1gInPc1	kláves
slouží	sloužit	k5eAaImIp3nP	sloužit
jen	jen	k9	jen
jako	jako	k9	jako
předvolba	předvolba	k1gFnSc1	předvolba
<g/>
.	.	kIx.	.
</s>
<s>
Odeslání	odeslání	k1gNnSc1	odeslání
některých	některý	k3yIgInPc2	některý
povelů	povel	k1gInPc2	povel
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pro	pro	k7c4	pro
symboly	symbol	k1gInPc4	symbol
<g/>
)	)	kIx)	)
pak	pak	k6eAd1	pak
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
stisk	stisk	k1gInSc1	stisk
(	(	kIx(	(
<g/>
úhoz	úhoz	k1gInSc1	úhoz
<g/>
)	)	kIx)	)
a	a	k8xC	a
současně	současně	k6eAd1	současně
držení	držení	k1gNnSc1	držení
jiné	jiný	k2eAgFnSc2d1	jiná
klávesy	klávesa	k1gFnSc2	klávesa
nebo	nebo	k8xC	nebo
dvou	dva	k4xCgFnPc2	dva
kláves	klávesa	k1gFnPc2	klávesa
současně	současně	k6eAd1	současně
nebo	nebo	k8xC	nebo
postupně	postupně	k6eAd1	postupně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kódová	kódový	k2eAgFnSc1d1	kódová
sada	sada	k1gFnSc1	sada
Unicode	Unicod	k1gInSc5	Unicod
má	mít	k5eAaImIp3nS	mít
znak	znak	k1gInSc1	znak
pro	pro	k7c4	pro
počítačovou	počítačový	k2eAgFnSc4d1	počítačová
klávesnici	klávesnice	k1gFnSc4	klávesnice
a	a	k8xC	a
její	její	k3xOp3gInSc4	její
kód	kód	k1gInSc4	kód
je	být	k5eAaImIp3nS	být
⌨	⌨	k?	⌨
(	(	kIx(	(
<g/>
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
2328	[number]	k4	2328
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Vzhled	vzhled	k1gInSc4	vzhled
==	==	k?	==
</s>
</p>
<p>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
různých	různý	k2eAgInPc2d1	různý
rozložení	rozložení	k1gNnSc4	rozložení
kláves	klávesa	k1gFnPc2	klávesa
<g/>
.	.	kIx.	.
</s>
<s>
Vznikají	vznikat	k5eAaImIp3nP	vznikat
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozdílní	rozdílný	k2eAgMnPc1d1	rozdílný
lidé	člověk	k1gMnPc1	člověk
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
snadný	snadný	k2eAgInSc4d1	snadný
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
rozdílným	rozdílný	k2eAgInPc3d1	rozdílný
symbolům	symbol	k1gInPc3	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
píší	psát	k5eAaImIp3nP	psát
odlišným	odlišný	k2eAgInSc7d1	odlišný
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existují	existovat	k5eAaImIp3nP	existovat
specializovaná	specializovaný	k2eAgNnPc1d1	specializované
rozložení	rozložení	k1gNnPc1	rozložení
pro	pro	k7c4	pro
matematické	matematický	k2eAgNnSc4d1	matematické
<g/>
,	,	kIx,	,
účetní	účetní	k2eAgNnSc4d1	účetní
<g/>
,	,	kIx,	,
programátorské	programátorský	k2eAgNnSc4d1	programátorské
použití	použití	k1gNnSc4	použití
<g/>
.	.	kIx.	.
</s>
<s>
Samostatnou	samostatný	k2eAgFnSc4d1	samostatná
kategorii	kategorie	k1gFnSc4	kategorie
pak	pak	k6eAd1	pak
představují	představovat	k5eAaImIp3nP	představovat
takzvané	takzvaný	k2eAgFnPc4d1	takzvaná
herní	herní	k2eAgFnPc4d1	herní
klávesnice	klávesnice	k1gFnPc4	klávesnice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
svou	svůj	k3xOyFgFnSc7	svůj
koncepcí	koncepce	k1gFnSc7	koncepce
v	v	k7c6	v
maximální	maximální	k2eAgFnSc6d1	maximální
možné	možný	k2eAgFnSc6d1	možná
míře	míra	k1gFnSc6	míra
usnadnit	usnadnit	k5eAaPmF	usnadnit
uživatelům	uživatel	k1gMnPc3	uživatel
hraní	hraní	k1gNnSc4	hraní
počítačových	počítačový	k2eAgFnPc2d1	počítačová
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
<g/>
Rozložení	rozložení	k1gNnSc1	rozložení
znaků	znak	k1gInPc2	znak
na	na	k7c6	na
počítačových	počítačový	k2eAgFnPc6d1	počítačová
klávesnicích	klávesnice	k1gFnPc6	klávesnice
kopíruje	kopírovat	k5eAaImIp3nS	kopírovat
standardy	standard	k1gInPc7	standard
rozložení	rozložení	k1gNnSc2	rozložení
na	na	k7c6	na
psacích	psací	k2eAgInPc6d1	psací
strojích	stroj	k1gInPc6	stroj
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
převzaly	převzít	k5eAaPmAgInP	převzít
organizační	organizační	k2eAgInPc4d1	organizační
automaty	automat	k1gInPc4	automat
<g/>
,	,	kIx,	,
pořizovače	pořizovač	k1gInPc4	pořizovač
děrných	děrný	k2eAgInPc2d1	děrný
štítků	štítek	k1gInPc2	štítek
atd.	atd.	kA	atd.
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
rozložení	rozložení	k1gNnSc1	rozložení
QWERTY	QWERTY	kA	QWERTY
<g/>
,	,	kIx,	,
jinde	jinde	k6eAd1	jinde
QWERTZ	QWERTZ	kA	QWERTZ
<g/>
,	,	kIx,	,
někde	někde	k6eAd1	někde
i	i	k9	i
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
francouzské	francouzský	k2eAgFnSc2d1	francouzská
AZERTY	AZERTY	kA	AZERTY
<g/>
.	.	kIx.	.
</s>
<s>
Rozložení	rozložení	k1gNnSc1	rozložení
kláves	klávesa	k1gFnPc2	klávesa
je	být	k5eAaImIp3nS	být
upraveno	upravit	k5eAaPmNgNnS	upravit
mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
normou	norma	k1gFnSc7	norma
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
IEC	IEC	kA	IEC
9995	[number]	k4	9995
"	"	kIx"	"
<g/>
Informační	informační	k2eAgFnSc2d1	informační
technologie	technologie	k1gFnSc2	technologie
–	–	k?	–
Uspořádání	uspořádání	k1gNnSc1	uspořádání
klávesnice	klávesnice	k1gFnSc2	klávesnice
pro	pro	k7c4	pro
textové	textový	k2eAgInPc4d1	textový
a	a	k8xC	a
kancelářské	kancelářský	k2eAgInPc4d1	kancelářský
systémy	systém	k1gInPc4	systém
<g/>
"	"	kIx"	"
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozmístění	rozmístění	k1gNnSc1	rozmístění
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
kláves	klávesa	k1gFnPc2	klávesa
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nP	řídit
národními	národní	k2eAgFnPc7d1	národní
normami	norma	k1gFnPc7	norma
a	a	k8xC	a
zvyklostmi	zvyklost	k1gFnPc7	zvyklost
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
příklad	příklad	k1gInSc1	příklad
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
možnost	možnost	k1gFnSc4	možnost
obsazení	obsazení	k1gNnSc2	obsazení
klávesy	klávesa	k1gFnSc2	klávesa
B01	B01	k1gFnSc2	B01
(	(	kIx(	(
<g/>
na	na	k7c4	na
české	český	k2eAgNnSc4d1	české
Y	Y	kA	Y
<g/>
)	)	kIx)	)
znakem	znak	k1gInSc7	znak
Z	z	k7c2	z
(	(	kIx(	(
<g/>
např.	např.	kA	např.
anglický	anglický	k2eAgMnSc1d1	anglický
nebo	nebo	k8xC	nebo
americký	americký	k2eAgInSc1d1	americký
standard	standard	k1gInSc1	standard
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
znakem	znak	k1gInSc7	znak
Y	Y	kA	Y
(	(	kIx(	(
<g/>
např.	např.	kA	např.
český	český	k2eAgInSc1d1	český
standard	standard	k1gInSc1	standard
<g/>
)	)	kIx)	)
či	či	k8xC	či
znakem	znak	k1gInSc7	znak
W	W	kA	W
(	(	kIx(	(
<g/>
např.	např.	kA	např.
francouzský	francouzský	k2eAgInSc1d1	francouzský
standard	standard	k1gInSc1	standard
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
D06	D06	k1gFnSc4	D06
(	(	kIx(	(
<g/>
na	na	k7c4	na
české	český	k2eAgNnSc4d1	české
Z	Z	kA	Z
<g/>
)	)	kIx)	)
Y	Y	kA	Y
nebo	nebo	k8xC	nebo
Z	Z	kA	Z
a	a	k8xC	a
pro	pro	k7c4	pro
D02	D02	k1gFnSc4	D02
(	(	kIx(	(
<g/>
na	na	k7c4	na
české	český	k2eAgNnSc4d1	české
W	W	kA	W
<g/>
)	)	kIx)	)
W	W	kA	W
nebo	nebo	k8xC	nebo
Z.	Z.	kA	Z.
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
normou	norma	k1gFnSc7	norma
<g/>
,	,	kIx,	,
stanoven	stanovit	k5eAaPmNgInS	stanovit
národní	národní	k2eAgInSc1d1	národní
standard	standard	k1gInSc1	standard
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
uspořádání	uspořádání	k1gNnSc2	uspořádání
QWERTZ	QWERTZ	kA	QWERTZ
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
ČSN	ČSN	kA	ČSN
36	[number]	k4	36
9050	[number]	k4	9050
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
rozmístění	rozmístění	k1gNnSc4	rozmístění
znaků	znak	k1gInPc2	znak
na	na	k7c6	na
48	[number]	k4	48
klávesách	klávesa	k1gFnPc6	klávesa
(	(	kIx(	(
<g/>
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
typu	typ	k1gInSc2	typ
QWERTZ	QWERTZ	kA	QWERTZ
<g/>
)	)	kIx)	)
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
úrovních	úroveň	k1gFnPc6	úroveň
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
základní	základní	k2eAgNnSc1d1	základní
a	a	k8xC	a
po	po	k7c6	po
stisknutí	stisknutí	k1gNnSc6	stisknutí
Shiftu	Shift	k1gInSc2	Shift
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
není	být	k5eNaImIp3nS	být
zde	zde	k6eAd1	zde
řešeno	řešen	k2eAgNnSc1d1	řešeno
umístění	umístění	k1gNnSc1	umístění
např.	např.	kA	např.
znaku	znak	k1gInSc3	znak
obrácené	obrácený	k2eAgNnSc1d1	obrácené
lomítko	lomítko	k1gNnSc1	lomítko
(	(	kIx(	(
<g/>
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
klávesnicích	klávesnice	k1gFnPc6	klávesnice
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
různém	různý	k2eAgNnSc6d1	různé
místě	místo	k1gNnSc6	místo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
znaků	znak	k1gInPc2	znak
generovaných	generovaný	k2eAgInPc2d1	generovaný
pomocí	pomocí	k7c2	pomocí
klávesy	klávesa	k1gFnSc2	klávesa
Alt	Alt	kA	Alt
Gr	Gr	k1gMnSc1	Gr
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
úroveň	úroveň	k1gFnSc1	úroveň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
umístění	umístění	k1gNnSc4	umístění
"	"	kIx"	"
<g/>
Z	Z	kA	Z
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Y	Y	kA	Y
<g/>
"	"	kIx"	"
ano	ano	k9	ano
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mnoho	mnoho	k4c1	mnoho
českých	český	k2eAgMnPc2d1	český
programátorů	programátor	k1gMnPc2	programátor
ale	ale	k8xC	ale
dává	dávat	k5eAaImIp3nS	dávat
přednost	přednost	k1gFnSc4	přednost
anglickému	anglický	k2eAgInSc3d1	anglický
standardu	standard	k1gInSc3	standard
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
rozložení	rozložení	k1gNnSc2	rozložení
QWERTY	QWERTY	kA	QWERTY
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
znaky	znak	k1gInPc1	znak
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
standardu	standard	k1gInSc6	standard
nejsou	být	k5eNaImIp3nP	být
<g/>
,	,	kIx,	,
případě	případ	k1gInSc6	případ
používají	používat	k5eAaImIp3nP	používat
tzv.	tzv.	kA	tzv.
českou	český	k2eAgFnSc4d1	Česká
programátorskou	programátorský	k2eAgFnSc4d1	programátorská
klávesnici	klávesnice	k1gFnSc4	klávesnice
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
českou	český	k2eAgFnSc4d1	Česká
QWERTY	QWERTY	kA	QWERTY
klávesnici	klávesnice	k1gFnSc4	klávesnice
<g/>
,	,	kIx,	,
lišící	lišící	k2eAgFnPc1d1	lišící
se	se	k3xPyFc4	se
jen	jen	k9	jen
prohozeným	prohozený	k2eAgMnPc3d1	prohozený
Z	z	k7c2	z
a	a	k8xC	a
Y	Y	kA	Y
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
již	již	k6eAd1	již
na	na	k7c4	na
anglickou	anglický	k2eAgFnSc4d1	anglická
klávesnici	klávesnice	k1gFnSc4	klávesnice
zvykli	zvyknout	k5eAaPmAgMnP	zvyknout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozložení	rozložení	k1gNnSc1	rozložení
QWERTY	QWERTY	kA	QWERTY
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
s	s	k7c7	s
úmyslem	úmysl	k1gInSc7	úmysl
snížit	snížit	k5eAaPmF	snížit
tak	tak	k9	tak
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
zaseknutí	zaseknutí	k1gNnSc2	zaseknutí
typových	typový	k2eAgFnPc2d1	typová
pák	páka	k1gFnPc2	páka
ručního	ruční	k2eAgInSc2d1	ruční
psacího	psací	k2eAgInSc2d1	psací
stroje	stroj	k1gInSc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
masovému	masový	k2eAgNnSc3d1	masové
rozšíření	rozšíření	k1gNnSc3	rozšíření
vedlo	vést	k5eAaImAgNnS	vést
vítězství	vítězství	k1gNnSc4	vítězství
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
v	v	k7c6	v
rychlosti	rychlost	k1gFnSc6	rychlost
a	a	k8xC	a
přesnosti	přesnost	k1gFnSc6	přesnost
psaní	psaní	k1gNnSc2	psaní
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1888	[number]	k4	1888
následovala	následovat	k5eAaImAgFnS	následovat
konference	konference	k1gFnSc1	konference
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
přijala	přijmout	k5eAaPmAgFnS	přijmout
klávesnici	klávesnice	k1gFnSc4	klávesnice
QWERTY	QWERTY	kA	QWERTY
za	za	k7c4	za
standard	standard	k1gInSc4	standard
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
speciální	speciální	k2eAgNnSc4d1	speciální
rozložení	rozložení	k1gNnSc4	rozložení
kláves	klávesa	k1gFnPc2	klávesa
zohledňující	zohledňující	k2eAgFnSc4d1	zohledňující
ergonomii	ergonomie	k1gFnSc4	ergonomie
(	(	kIx(	(
<g/>
Dvorak	Dvorak	k1gMnSc1	Dvorak
<g/>
,	,	kIx,	,
Colemak	Colemak	k1gMnSc1	Colemak
<g/>
,	,	kIx,	,
XPeRT	XPeRT	k1gMnSc1	XPeRT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
rozložení	rozložení	k1gNnPc1	rozložení
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
nejsou	být	k5eNaImIp3nP	být
moc	moc	k6eAd1	moc
rozšířena	rozšířen	k2eAgNnPc1d1	rozšířeno
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgNnP	být
navržena	navrhnout	k5eAaPmNgNnP	navrhnout
pro	pro	k7c4	pro
psaní	psaní	k1gNnSc4	psaní
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
a	a	k8xC	a
příliš	příliš	k6eAd1	příliš
se	se	k3xPyFc4	se
nehodí	hodit	k5eNaPmIp3nS	hodit
pro	pro	k7c4	pro
psaní	psaní	k1gNnSc4	psaní
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jsou	být	k5eAaImIp3nP	být
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
než	než	k8xS	než
QWERTY	QWERTY	kA	QWERTY
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
národním	národní	k2eAgInSc6d1	národní
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
například	například	k6eAd1	například
Dvorak	Dvorak	k1gInSc4	Dvorak
pro	pro	k7c4	pro
psaní	psaní	k1gNnSc4	psaní
českého	český	k2eAgInSc2d1	český
textu	text	k1gInSc2	text
nemá	mít	k5eNaImIp3nS	mít
zásadnější	zásadní	k2eAgInSc4d2	zásadnější
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
uvádí	uvádět	k5eAaImIp3nS	uvádět
publikace	publikace	k1gFnSc2	publikace
"	"	kIx"	"
<g/>
Profesionálem	profesionál	k1gMnSc7	profesionál
v	v	k7c6	v
administrativě	administrativa	k1gFnSc6	administrativa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
produktivitu	produktivita	k1gFnSc4	produktivita
práce	práce	k1gFnSc2	práce
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
1	[number]	k4	1
<g/>
%	%	kIx~	%
a	a	k8xC	a
se	s	k7c7	s
stejným	stejný	k2eAgInSc7d1	stejný
úspěchem	úspěch	k1gInSc7	úspěch
sníží	snížit	k5eAaPmIp3nS	snížit
zatížení	zatížení	k1gNnSc1	zatížení
prstů	prst	k1gInPc2	prst
a	a	k8xC	a
rukou	ruka	k1gFnPc2	ruka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Optimalizace	optimalizace	k1gFnSc1	optimalizace
rozložení	rozložení	k1gNnSc2	rozložení
kláves	klávesa	k1gFnPc2	klávesa
českého	český	k2eAgInSc2d1	český
standardu	standard	k1gInSc2	standard
nebyla	být	k5eNaImAgFnS	být
dosud	dosud	k6eAd1	dosud
provedena	provést	k5eAaPmNgFnS	provést
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozmístění	rozmístění	k1gNnSc3	rozmístění
znaků	znak	k1gInPc2	znak
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
výskytu	výskyt	k1gInSc6	výskyt
v	v	k7c6	v
textu	text	k1gInSc6	text
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
by	by	kYmCp3nS	by
k	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
produktivity	produktivita	k1gFnSc2	produktivita
práce	práce	k1gFnSc2	práce
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
12,2	[number]	k4	12,2
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
navíc	navíc	k6eAd1	navíc
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
k	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
úpravám	úprava	k1gFnPc3	úprava
klávesnice	klávesnice	k1gFnSc2	klávesnice
<g/>
,	,	kIx,	,
produktivita	produktivita	k1gFnSc1	produktivita
práce	práce	k1gFnSc2	práce
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
zvýšena	zvýšit	k5eAaPmNgFnS	zvýšit
o	o	k7c4	o
další	další	k2eAgInPc4d1	další
2	[number]	k4	2
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
umožňovalo	umožňovat	k5eAaImAgNnS	umožňovat
psát	psát	k5eAaImF	psát
59,5	[number]	k4	59,5
%	%	kIx~	%
obsahu	obsah	k1gInSc3	obsah
textu	text	k1gInSc2	text
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
poloze	poloha	k1gFnSc6	poloha
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
z	z	k7c2	z
kláves	klávesa	k1gFnPc2	klávesa
na	na	k7c6	na
nichž	jenž	k3xRgNnPc6	jenž
jsou	být	k5eAaImIp3nP	být
umístěny	umístěn	k2eAgInPc1d1	umístěn
prsty	prst	k1gInPc1	prst
při	při	k7c6	při
využívání	využívání	k1gNnSc6	využívání
hmatové	hmatový	k2eAgFnSc2d1	hmatová
metody	metoda	k1gFnSc2	metoda
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
při	při	k7c6	při
psaní	psaní	k1gNnSc6	psaní
anglického	anglický	k2eAgInSc2d1	anglický
textu	text	k1gInSc2	text
na	na	k7c6	na
klávesnici	klávesnice	k1gFnSc6	klávesnice
Dvorak	Dvorak	k1gMnSc1	Dvorak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
však	však	k9	však
znamenalo	znamenat	k5eAaImAgNnS	znamenat
ještě	ještě	k9	ještě
větší	veliký	k2eAgInPc4d2	veliký
rozdíly	rozdíl	k1gInPc4	rozdíl
nejen	nejen	k6eAd1	nejen
od	od	k7c2	od
QWERTZ	QWERTZ	kA	QWERTZ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
od	od	k7c2	od
QWERTY	QWERTY	kA	QWERTY
než	než	k8xS	než
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Statistiku	statistika	k1gFnSc4	statistika
psaní	psaní	k1gNnSc2	psaní
na	na	k7c6	na
těchto	tento	k3xDgNnPc6	tento
třech	tři	k4xCgNnPc6	tři
rozloženích	rozložení	k1gNnPc6	rozložení
si	se	k3xPyFc3	se
můžete	moct	k5eAaImIp2nP	moct
porovnat	porovnat	k5eAaPmF	porovnat
vložením	vložení	k1gNnSc7	vložení
libovolného	libovolný	k2eAgInSc2d1	libovolný
textu	text	k1gInSc2	text
do	do	k7c2	do
Java	Jav	k1gInSc2	Jav
apletu	aplet	k1gInSc2	aplet
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
stránce	stránka	k1gFnSc6	stránka
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozložení	rozložení	k1gNnSc1	rozložení
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
kláves	klávesa	k1gFnPc2	klávesa
má	mít	k5eAaImIp3nS	mít
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
efektivity	efektivita	k1gFnSc2	efektivita
význam	význam	k1gInSc1	význam
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
osoby	osoba	k1gFnPc4	osoba
ovládající	ovládající	k2eAgFnPc4d1	ovládající
klávesnici	klávesnice	k1gFnSc4	klávesnice
deseti	deset	k4xCc7	deset
prsty	prst	k1gInPc7	prst
nebo	nebo	k8xC	nebo
hmatovou	hmatový	k2eAgFnSc7d1	hmatová
metodou	metoda	k1gFnSc7	metoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Standardní	standardní	k2eAgFnSc1d1	standardní
klávesnice	klávesnice	k1gFnSc1	klávesnice
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
velká	velký	k2eAgFnSc1d1	velká
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
každá	každý	k3xTgFnSc1	každý
klávesa	klávesa	k1gFnSc1	klávesa
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
dost	dost	k6eAd1	dost
velká	velký	k2eAgFnSc1d1	velká
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
dala	dát	k5eAaPmAgFnS	dát
snadno	snadno	k6eAd1	snadno
stisknout	stisknout	k5eAaPmF	stisknout
prsty	prst	k1gInPc4	prst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
přenosná	přenosný	k2eAgNnPc4d1	přenosné
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
standardní	standardní	k2eAgFnSc1d1	standardní
klávesnice	klávesnice	k1gFnSc1	klávesnice
byla	být	k5eAaImAgFnS	být
příliš	příliš	k6eAd1	příliš
velká	velký	k2eAgFnSc1d1	velká
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
navrženy	navrhnout	k5eAaPmNgInP	navrhnout
redukované	redukovaný	k2eAgInPc1d1	redukovaný
typy	typ	k1gInPc1	typ
klávesnic	klávesnice	k1gFnPc2	klávesnice
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jiný	jiný	k2eAgInSc1d1	jiný
způsob	způsob	k1gInSc1	způsob
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ultramoderní	ultramoderní	k2eAgInPc1d1	ultramoderní
typy	typ	k1gInPc1	typ
klávesnic	klávesnice	k1gFnPc2	klávesnice
a	a	k8xC	a
kláves	klávesa	k1gFnPc2	klávesa
<g/>
,	,	kIx,	,
např.	např.	kA	např.
gelové	gelový	k2eAgFnPc4d1	gelová
nebo	nebo	k8xC	nebo
obalované	obalovaný	k2eAgFnPc4d1	obalovaná
měkkými	měkký	k2eAgInPc7d1	měkký
materiály	materiál	k1gInPc7	materiál
<g/>
;	;	kIx,	;
bývají	bývat	k5eAaImIp3nP	bývat
též	též	k9	též
ergonomicky	ergonomicky	k6eAd1	ergonomicky
tvarovány	tvarován	k2eAgInPc1d1	tvarován
pro	pro	k7c4	pro
lepší	dobrý	k2eAgInSc4d2	lepší
dosah	dosah	k1gInSc4	dosah
na	na	k7c4	na
klávesy	klávesa	k1gFnPc4	klávesa
<g/>
.	.	kIx.	.
</s>
<s>
Nezapomínejme	zapomínat	k5eNaImRp1nP	zapomínat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
IT	IT	kA	IT
svět	svět	k1gInSc1	svět
hemží	hemžit	k5eAaImIp3nS	hemžit
též	též	k9	též
interaktivními	interaktivní	k2eAgFnPc7d1	interaktivní
klávesnicemi	klávesnice	k1gFnPc7	klávesnice
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
u	u	k7c2	u
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
<g/>
,	,	kIx,	,
interaktivních	interaktivní	k2eAgFnPc6d1	interaktivní
tabulích	tabule	k1gFnPc6	tabule
či	či	k8xC	či
u	u	k7c2	u
samého	samý	k3xTgInSc2	samý
monitoru	monitor	k1gInSc2	monitor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Skupiny	skupina	k1gFnPc1	skupina
kláves	klávesa	k1gFnPc2	klávesa
==	==	k?	==
</s>
</p>
<p>
<s>
Klávesy	klávesa	k1gFnPc1	klávesa
počítačové	počítačový	k2eAgFnSc2d1	počítačová
klávesnice	klávesnice	k1gFnSc2	klávesnice
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
těchto	tento	k3xDgFnPc2	tento
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
základní	základní	k2eAgInPc1d1	základní
alfanumerické	alfanumerický	k2eAgInPc1d1	alfanumerický
klávesy	kláves	k1gInPc1	kláves
</s>
</p>
<p>
<s>
klávesy	klávesa	k1gFnPc4	klávesa
numerické	numerický	k2eAgFnSc2d1	numerická
klávesnice	klávesnice	k1gFnSc2	klávesnice
</s>
</p>
<p>
<s>
funkční	funkční	k2eAgFnPc4d1	funkční
klávesy	klávesa	k1gFnPc4	klávesa
</s>
</p>
<p>
<s>
speciální	speciální	k2eAgFnPc4d1	speciální
klávesy	klávesa	k1gFnPc4	klávesa
</s>
</p>
<p>
<s>
klávesy	kláves	k1gInPc1	kláves
určené	určený	k2eAgInPc1d1	určený
pro	pro	k7c4	pro
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
(	(	kIx(	(
<g/>
v	v	k7c6	v
systému	systém	k1gInSc6	systém
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
Windows	Windows	kA	Windows
a	a	k8xC	a
Application	Application	k1gInSc1	Application
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Základní	základní	k2eAgInPc1d1	základní
alfanumerické	alfanumerický	k2eAgInPc1d1	alfanumerický
klávesy	kláves	k1gInPc1	kláves
===	===	k?	===
</s>
</p>
<p>
<s>
Znaková	znakový	k2eAgFnSc1d1	znaková
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
alfanumerická	alfanumerický	k2eAgFnSc1d1	alfanumerická
klávesnice	klávesnice	k1gFnSc1	klávesnice
zabírá	zabírat	k5eAaImIp3nS	zabírat
asi	asi	k9	asi
třetinu	třetina	k1gFnSc4	třetina
plochy	plocha	k1gFnSc2	plocha
klávesnice	klávesnice	k1gFnSc2	klávesnice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
doplněná	doplněný	k2eAgFnSc1d1	doplněná
o	o	k7c4	o
další	další	k2eAgInPc4d1	další
klávesy	kláves	k1gInPc4	kláves
s	s	k7c7	s
nimiž	jenž	k3xRgFnPc7	jenž
tvoří	tvořit	k5eAaImIp3nP	tvořit
základní	základní	k2eAgFnSc4d1	základní
část	část	k1gFnSc4	část
klávesnice	klávesnice	k1gFnSc2	klávesnice
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
klávesy	klávesa	k1gFnSc2	klávesa
26	[number]	k4	26
písmen	písmeno	k1gNnPc2	písmeno
<g/>
,	,	kIx,	,
mezerník	mezerník	k1gInSc1	mezerník
<g/>
,	,	kIx,	,
klávesy	klávesa	k1gFnPc1	klávesa
s	s	k7c7	s
interpunkcí	interpunkce	k1gFnSc7	interpunkce
a	a	k8xC	a
horní	horní	k2eAgFnSc4d1	horní
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
řadu	řada	k1gFnSc4	řada
s	s	k7c7	s
číslicemi	číslice	k1gFnPc7	číslice
v	v	k7c6	v
anglické	anglický	k2eAgFnSc6d1	anglická
verzi	verze	k1gFnSc6	verze
a	a	k8xC	a
s	s	k7c7	s
diakritickými	diakritický	k2eAgInPc7d1	diakritický
znaky	znak	k1gInPc7	znak
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
verzi	verze	k1gFnSc6	verze
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
znakové	znakový	k2eAgFnPc1d1	znaková
klávesnice	klávesnice	k1gFnPc1	klávesnice
se	se	k3xPyFc4	se
významově	významově	k6eAd1	významově
obměňují	obměňovat	k5eAaImIp3nP	obměňovat
držením	držení	k1gNnSc7	držení
stisknutých	stisknutý	k2eAgFnPc2d1	stisknutá
pomocných	pomocný	k2eAgFnPc2d1	pomocná
kláves	klávesa	k1gFnPc2	klávesa
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
</s>
</p>
<p>
<s>
dvě	dva	k4xCgFnPc1	dva
klávesy	klávesa	k1gFnPc1	klávesa
jednorázového	jednorázový	k2eAgInSc2d1	jednorázový
přesmykače	přesmykač	k1gInSc2	přesmykač
(	(	kIx(	(
<g/>
Shift	Shift	kA	Shift
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
levý	levý	k2eAgMnSc1d1	levý
a	a	k8xC	a
pravý	pravý	k2eAgMnSc1d1	pravý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
změní	změnit	k5eAaPmIp3nS	změnit
písmena	písmeno	k1gNnPc4	písmeno
z	z	k7c2	z
malých	malý	k2eAgFnPc2d1	malá
na	na	k7c4	na
velká	velký	k2eAgNnPc4d1	velké
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
znaků	znak	k1gInPc2	znak
s	s	k7c7	s
diakritikou	diakritika	k1gFnSc7	diakritika
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
E	E	kA	E
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
změní	změnit	k5eAaPmIp3nS	změnit
na	na	k7c4	na
číselnou	číselný	k2eAgFnSc4d1	číselná
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
trvalý	trvalý	k2eAgInSc1d1	trvalý
přesmykač	přesmykač	k1gInSc1	přesmykač
CapsLock	CapsLock	k1gInSc1	CapsLock
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tento	tento	k3xDgInSc1	tento
účinek	účinek	k1gInSc1	účinek
jednorázového	jednorázový	k2eAgInSc2d1	jednorázový
přesmykače	přesmykač	k1gInSc2	přesmykač
obrátí	obrátit	k5eAaPmIp3nS	obrátit
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
klávesu	klávesa	k1gFnSc4	klávesa
tabulátoru	tabulátor	k1gInSc2	tabulátor
Tab	tab	kA	tab
a	a	k8xC	a
</s>
</p>
<p>
<s>
výmaz	výmaz	k1gInSc4	výmaz
předchozího	předchozí	k2eAgNnSc2d1	předchozí
(	(	kIx(	(
<g/>
vůči	vůči	k7c3	vůči
kurzoru	kurzor	k1gInSc3	kurzor
<g/>
)	)	kIx)	)
znaku	znak	k1gInSc2	znak
Backspace	Backspace	k1gFnSc2	Backspace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Klávesy	klávesa	k1gFnPc4	klávesa
numerické	numerický	k2eAgFnSc2d1	numerická
klávesnice	klávesnice	k1gFnSc2	klávesnice
===	===	k?	===
</s>
</p>
<p>
<s>
Numerická	numerický	k2eAgFnSc1d1	numerická
klávesnice	klávesnice	k1gFnSc1	klávesnice
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
klávesy	klávesa	k1gFnPc4	klávesa
s	s	k7c7	s
číslicemi	číslice	k1gFnPc7	číslice
<g/>
,	,	kIx,	,
desetinnou	desetinný	k2eAgFnSc4d1	desetinná
tečku	tečka	k1gFnSc4	tečka
<g/>
,	,	kIx,	,
klávesy	klávesa	k1gFnPc4	klávesa
využitelné	využitelný	k2eAgFnPc4d1	využitelná
pro	pro	k7c4	pro
4	[number]	k4	4
základní	základní	k2eAgFnSc2d1	základní
aritmerické	aritmerický	k2eAgFnSc2d1	aritmerický
operace	operace	k1gFnSc2	operace
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc4	druhý
klávesu	klávesa	k1gFnSc4	klávesa
Enter	Entra	k1gFnPc2	Entra
a	a	k8xC	a
klávesu	klávesa	k1gFnSc4	klávesa
Num	Num	k1gFnSc4	Num
Lock	Locka	k1gFnPc2	Locka
pro	pro	k7c4	pro
přepínání	přepínání	k1gNnSc4	přepínání
funkce	funkce	k1gFnSc2	funkce
číselných	číselný	k2eAgFnPc2d1	číselná
kláves	klávesa	k1gFnPc2	klávesa
z	z	k7c2	z
kurzorových	kurzorový	k2eAgFnPc2d1	kurzorová
na	na	k7c4	na
číslicové	číslicový	k2eAgFnPc4d1	číslicová
<g/>
.	.	kIx.	.
</s>
<s>
Číslice	číslice	k1gFnPc1	číslice
jsou	být	k5eAaImIp3nP	být
uspořádány	uspořádat	k5eAaPmNgFnP	uspořádat
zdola	zdola	k6eAd1	zdola
nahoru	nahoru	k6eAd1	nahoru
po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
1	[number]	k4	1
-	-	kIx~	-
3	[number]	k4	3
<g/>
,	,	kIx,	,
4	[number]	k4	4
-	-	kIx~	-
6	[number]	k4	6
a	a	k8xC	a
7	[number]	k4	7
až	až	k9	až
9	[number]	k4	9
<g/>
)	)	kIx)	)
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
kalkulátorů	kalkulátor	k1gInPc2	kalkulátor
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
klávesnici	klávesnice	k1gFnSc3	klávesnice
telefonů	telefon	k1gInPc2	telefon
(	(	kIx(	(
<g/>
klasických	klasický	k2eAgInPc2d1	klasický
tlačítkových	tlačítkový	k2eAgInPc2d1	tlačítkový
i	i	k8xC	i
dotykových	dotykový	k2eAgInPc2d1	dotykový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
naopak	naopak	k6eAd1	naopak
dole	dole	k6eAd1	dole
vyšší	vysoký	k2eAgFnSc1d2	vyšší
číslice	číslice	k1gFnSc1	číslice
<g/>
:	:	kIx,	:
0	[number]	k4	0
<g/>
,	,	kIx,	,
7	[number]	k4	7
-	-	kIx~	-
9	[number]	k4	9
<g/>
,	,	kIx,	,
4	[number]	k4	4
-	-	kIx~	-
6	[number]	k4	6
<g/>
,	,	kIx,	,
1	[number]	k4	1
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Funkční	funkční	k2eAgFnSc2d1	funkční
klávesy	klávesa	k1gFnSc2	klávesa
===	===	k?	===
</s>
</p>
<p>
<s>
Funkční	funkční	k2eAgInPc1d1	funkční
klávesy	kláves	k1gInPc1	kláves
mají	mít	k5eAaImIp3nP	mít
označení	označení	k1gNnSc4	označení
odleva	odleva	k6eAd1	odleva
F1	F1	k1gMnPc1	F1
až	až	k8xS	až
F12	F12	k1gMnPc1	F12
a	a	k8xC	a
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
je	on	k3xPp3gFnPc4	on
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
klávesnice	klávesnice	k1gFnSc2	klávesnice
nad	nad	k7c7	nad
základní	základní	k2eAgFnSc7d1	základní
částí	část	k1gFnSc7	část
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
řízení	řízení	k1gNnSc3	řízení
programů	program	k1gInPc2	program
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
funkce	funkce	k1gFnSc1	funkce
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
konkrétním	konkrétní	k2eAgInSc7d1	konkrétní
softwarem	software	k1gInSc7	software
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Speciální	speciální	k2eAgInPc1d1	speciální
klávesy	kláves	k1gInPc1	kláves
===	===	k?	===
</s>
</p>
<p>
<s>
Speciální	speciální	k2eAgInPc1d1	speciální
klávesy	kláves	k1gInPc1	kláves
jsou	být	k5eAaImIp3nP	být
popsány	popsat	k5eAaPmNgInP	popsat
v	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
tabulce	tabulka	k1gFnSc6	tabulka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hudební	hudební	k2eAgFnSc1d1	hudební
klávesnice	klávesnice	k1gFnSc1	klávesnice
klavírního	klavírní	k2eAgInSc2d1	klavírní
typu	typ	k1gInSc2	typ
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
standardního	standardní	k2eAgNnSc2d1	standardní
připojení	připojení	k1gNnSc2	připojení
k	k	k7c3	k
zesilovači	zesilovač	k1gInSc3	zesilovač
a	a	k8xC	a
reproduktoru	reproduktor	k1gInSc3	reproduktor
také	také	k6eAd1	také
může	moct	k5eAaImIp3nS	moct
připojit	připojit	k5eAaPmF	připojit
datovým	datový	k2eAgInSc7d1	datový
kabelem	kabel	k1gInSc7	kabel
k	k	k7c3	k
počítači	počítač	k1gInSc3	počítač
a	a	k8xC	a
nahrávat	nahrávat	k5eAaImF	nahrávat
tak	tak	k6eAd1	tak
hranou	hraný	k2eAgFnSc4d1	hraná
melodii	melodie	k1gFnSc4	melodie
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
software	software	k1gInSc1	software
zároveň	zároveň	k6eAd1	zároveň
zapíše	zapsat	k5eAaPmIp3nS	zapsat
do	do	k7c2	do
notové	notový	k2eAgFnSc2d1	notová
osnovy	osnova	k1gFnSc2	osnova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Připojení	připojení	k1gNnSc4	připojení
k	k	k7c3	k
počítači	počítač	k1gInSc3	počítač
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
dřívějších	dřívější	k2eAgNnPc6d1	dřívější
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
klávesnice	klávesnice	k1gFnSc1	klávesnice
připojovala	připojovat	k5eAaImAgFnS	připojovat
k	k	k7c3	k
počítači	počítač	k1gInSc3	počítač
konektorem	konektor	k1gInSc7	konektor
DIN-	DIN-	k1gFnSc1	DIN-
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
nahrazen	nahradit	k5eAaPmNgInS	nahradit
poněkud	poněkud	k6eAd1	poněkud
menším	malý	k2eAgInSc7d2	menší
konektorem	konektor	k1gInSc7	konektor
Mini-DIN	Mini-DIN	k1gFnSc2	Mini-DIN
<g/>
,	,	kIx,	,
častěji	často	k6eAd2	často
nazývaným	nazývaný	k2eAgInSc7d1	nazývaný
PS	PS	kA	PS
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
způsob	způsob	k1gInSc1	způsob
komunikace	komunikace	k1gFnSc2	komunikace
klávesnice	klávesnice	k1gFnSc2	klávesnice
s	s	k7c7	s
počítačem	počítač	k1gInSc7	počítač
zůstal	zůstat	k5eAaPmAgInS	zůstat
zachován	zachovat	k5eAaPmNgInS	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgFnSc1d2	starší
klávesnice	klávesnice	k1gFnSc1	klávesnice
s	s	k7c7	s
konektorem	konektor	k1gInSc7	konektor
DIN	din	k1gInSc1	din
bývají	bývat	k5eAaImIp3nP	bývat
nazývány	nazýván	k2eAgInPc1d1	nazýván
"	"	kIx"	"
<g/>
AT	AT	kA	AT
klávesnice	klávesnice	k1gFnSc1	klávesnice
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
osobního	osobní	k2eAgInSc2d1	osobní
počítače	počítač	k1gInSc2	počítač
IBM	IBM	kA	IBM
PC	PC	kA	PC
<g/>
/	/	kIx~	/
<g/>
AT	AT	kA	AT
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stejný	stejný	k2eAgInSc4d1	stejný
konektor	konektor	k1gInSc4	konektor
však	však	k9	však
používaly	používat	k5eAaImAgFnP	používat
i	i	k9	i
ještě	ještě	k6eAd1	ještě
starší	starý	k2eAgFnSc7d2	starší
nekompatibilní	kompatibilní	k2eNgFnSc7d1	nekompatibilní
"	"	kIx"	"
<g/>
XT	XT	kA	XT
klávesnice	klávesnice	k1gFnSc1	klávesnice
<g/>
"	"	kIx"	"
určené	určený	k2eAgNnSc1d1	určené
pro	pro	k7c4	pro
historický	historický	k2eAgInSc4d1	historický
IBM	IBM	kA	IBM
PC	PC	kA	PC
<g/>
/	/	kIx~	/
<g/>
XT	XT	kA	XT
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
způsoby	způsob	k1gInPc4	způsob
připojení	připojení	k1gNnSc2	připojení
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
patří	patřit	k5eAaImIp3nS	patřit
USB	USB	kA	USB
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
nejrozšířenější	rozšířený	k2eAgFnSc1d3	nejrozšířenější
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
již	již	k6eAd1	již
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
výrazně	výrazně	k6eAd1	výrazně
komplikovanější	komplikovaný	k2eAgInSc4d2	komplikovanější
způsob	způsob	k1gInSc4	způsob
komunikace	komunikace	k1gFnSc2	komunikace
umožňující	umožňující	k2eAgFnSc2d1	umožňující
například	například	k6eAd1	například
připojit	připojit	k5eAaPmF	připojit
ke	k	k7c3	k
klávesnici	klávesnice	k1gFnSc3	klávesnice
různá	různý	k2eAgNnPc4d1	různé
další	další	k2eAgNnPc4d1	další
zařízení	zařízení	k1gNnPc4	zařízení
(	(	kIx(	(
<g/>
myš	myš	k1gFnSc1	myš
<g/>
,	,	kIx,	,
USB	USB	kA	USB
flash	flash	k1gInSc1	flash
paměť	paměť	k1gFnSc1	paměť
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pak	pak	k6eAd1	pak
přes	přes	k7c4	přes
ni	on	k3xPp3gFnSc4	on
komunikují	komunikovat	k5eAaImIp3nP	komunikovat
s	s	k7c7	s
počítačem	počítač	k1gInSc7	počítač
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
připojení	připojení	k1gNnSc2	připojení
přes	přes	k7c4	přes
USB	USB	kA	USB
port	port	k1gInSc4	port
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
dvě	dva	k4xCgFnPc4	dva
varianty	varianta	k1gFnPc4	varianta
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
<g/>
:	:	kIx,	:
Přímé	přímý	k2eAgNnSc1d1	přímé
propojení	propojení	k1gNnSc1	propojení
kabelem	kabel	k1gInSc7	kabel
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
bezdrátové	bezdrátový	k2eAgNnSc4d1	bezdrátové
propojení	propojení	k1gNnSc4	propojení
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
fyzicky	fyzicky	k6eAd1	fyzicky
v	v	k7c6	v
USB	USB	kA	USB
portu	porta	k1gFnSc4	porta
přítomen	přítomen	k2eAgMnSc1d1	přítomen
pouze	pouze	k6eAd1	pouze
vysílač	vysílač	k1gInSc1	vysílač
(	(	kIx(	(
<g/>
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
i	i	k9	i
zároveň	zároveň	k6eAd1	zároveň
přijímač	přijímač	k1gInSc4	přijímač
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
transceiver	transceiver	k1gInSc1	transceiver
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
komunikuje	komunikovat	k5eAaImIp3nS	komunikovat
s	s	k7c7	s
klávesnicí	klávesnice	k1gFnSc7	klávesnice
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
variantou	varianta	k1gFnSc7	varianta
bezdrátové	bezdrátový	k2eAgFnSc2d1	bezdrátová
komunikace	komunikace	k1gFnSc2	komunikace
klávesnice	klávesnice	k1gFnSc2	klávesnice
s	s	k7c7	s
počítačem	počítač	k1gInSc7	počítač
je	být	k5eAaImIp3nS	být
připojení	připojení	k1gNnSc1	připojení
přes	přes	k7c4	přes
bluetooth	bluetooth	k1gInSc4	bluetooth
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
zadávaných	zadávaný	k2eAgNnPc2d1	zadávané
dat	datum	k1gNnPc2	datum
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
výsledků	výsledek	k1gInPc2	výsledek
výzkumu	výzkum	k1gInSc2	výzkum
uveřejněných	uveřejněný	k2eAgNnPc2d1	uveřejněné
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2008	[number]	k4	2008
pracovníky	pracovník	k1gMnPc7	pracovník
švýcarské	švýcarský	k2eAgFnSc2d1	švýcarská
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
EPFL	EPFL	kA	EPFL
v	v	k7c6	v
Lausanne	Lausanne	k1gNnSc6	Lausanne
představují	představovat	k5eAaImIp3nP	představovat
klávesnice	klávesnice	k1gFnPc1	klávesnice
slabé	slabý	k2eAgNnSc1d1	slabé
místo	místo	k1gNnSc1	místo
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
zadávaných	zadávaný	k2eAgInPc2d1	zadávaný
údajů	údaj	k1gInPc2	údaj
<g/>
.	.	kIx.	.
</s>
<s>
Signály	signál	k1gInPc1	signál
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
klávesy	kláves	k1gInPc1	kláves
byly	být	k5eAaImAgInP	být
stisknuty	stisknout	k5eAaPmNgInP	stisknout
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
totiž	totiž	k9	totiž
v	v	k7c6	v
naprosté	naprostý	k2eAgFnSc6d1	naprostá
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
zachytit	zachytit	k5eAaPmF	zachytit
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
antény	anténa	k1gFnSc2	anténa
a	a	k8xC	a
potřebného	potřebný	k2eAgNnSc2d1	potřebné
technického	technický	k2eAgNnSc2d1	technické
zařízení	zařízení	k1gNnSc2	zařízení
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
až	až	k9	až
20	[number]	k4	20
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
mezi	mezi	k7c7	mezi
klávesnicí	klávesnice	k1gFnSc7	klávesnice
a	a	k8xC	a
odposlouchávacím	odposlouchávací	k2eAgNnSc7d1	odposlouchávací
zařízením	zařízení	k1gNnSc7	zařízení
stojí	stát	k5eAaImIp3nS	stát
překážka	překážka	k1gFnSc1	překážka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
zeď	zeď	k1gFnSc1	zeď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Mueller	Mueller	k1gMnSc1	Mueller
S.	S.	kA	S.
<g/>
:	:	kIx,	:
Osobní	osobní	k2eAgInSc1d1	osobní
počítač	počítač	k1gInSc1	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Computer	computer	k1gInSc1	computer
press	press	k1gInSc1	press
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prášil	Prášil	k1gMnSc1	Prášil
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
Skripta	skripta	k1gNnPc1	skripta
výpočetní	výpočetní	k2eAgFnSc2d1	výpočetní
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
OAMB	OAMB	kA	OAMB
1997	[number]	k4	1997
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
počítačová	počítačový	k2eAgFnSc1d1	počítačová
klávesnice	klávesnice	k1gFnSc1	klávesnice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
počítačová	počítačový	k2eAgFnSc1d1	počítačová
klávesnice	klávesnice	k1gFnSc1	klávesnice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Klávesové	klávesový	k2eAgFnPc1d1	klávesová
zkratky	zkratka	k1gFnPc1	zkratka
</s>
</p>
<p>
<s>
Technologie	technologie	k1gFnSc1	technologie
klávesnic	klávesnice	k1gFnPc2	klávesnice
</s>
</p>
<p>
<s>
Klávesnice	klávesnice	k1gFnSc1	klávesnice
počítačů	počítač	k1gInPc2	počítač
ZX	ZX	kA	ZX
Spectrum	Spectrum	k1gNnSc1	Spectrum
</s>
</p>
<p>
<s>
Psaní	psaní	k1gNnSc1	psaní
na	na	k7c6	na
stroji	stroj	k1gInSc6	stroj
</s>
</p>
