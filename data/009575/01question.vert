<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
určené	určený	k2eAgNnSc1d1	určené
pro	pro	k7c4	pro
vkládání	vkládání	k1gNnSc4	vkládání
znaků	znak	k1gInPc2	znak
do	do	k7c2	do
počítače	počítač	k1gInSc2	počítač
<g/>
?	?	kIx.	?
</s>
