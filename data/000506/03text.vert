<s>
Park	park	k1gInSc1	park
Güell	Güell	k1gInSc1	Güell
(	(	kIx(	(
<g/>
katalánsky	katalánsky	k6eAd1	katalánsky
Parc	Parc	k1gFnSc1	Parc
Güell	Güella	k1gFnPc2	Güella
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
původní	původní	k2eAgInSc1d1	původní
název	název	k1gInSc1	název
Park	park	k1gInSc1	park
Güell	Güell	k1gInSc1	Güell
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rozlehlý	rozlehlý	k2eAgInSc4d1	rozlehlý
park	park	k1gInSc4	park
s	s	k7c7	s
architektonickými	architektonický	k2eAgInPc7d1	architektonický
prvky	prvek	k1gInPc7	prvek
situovaný	situovaný	k2eAgMnSc1d1	situovaný
nad	nad	k7c7	nad
Barcelonou	Barcelona	k1gFnSc7	Barcelona
(	(	kIx(	(
<g/>
Katalánsko	Katalánsko	k1gNnSc1	Katalánsko
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
)	)	kIx)	)
na	na	k7c6	na
svahu	svah	k1gInSc6	svah
hory	hora	k1gFnSc2	hora
Turó	Turó	k1gFnSc2	Turó
del	del	k?	del
Carmel	Carmel	k1gMnSc1	Carmel
obráceném	obrácený	k2eAgNnSc6d1	obrácené
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
moři	moře	k1gNnSc3	moře
<g/>
.	.	kIx.	.
</s>
<s>
Park	park	k1gInSc1	park
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
katalánský	katalánský	k2eAgMnSc1d1	katalánský
architekt	architekt	k1gMnSc1	architekt
Antoni	Anton	k1gMnPc1	Anton
Gaudí	Gaudí	k1gNnSc4	Gaudí
původně	původně	k6eAd1	původně
jako	jako	k8xC	jako
zahradní	zahradní	k2eAgNnSc1d1	zahradní
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
parku	park	k1gInSc2	park
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1900	[number]	k4	1900
<g/>
-	-	kIx~	-
<g/>
1914	[number]	k4	1914
<g/>
;	;	kIx,	;
projekt	projekt	k1gInSc4	projekt
zahradního	zahradní	k2eAgNnSc2d1	zahradní
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
však	však	k9	však
realizovat	realizovat	k5eAaBmF	realizovat
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Zakázku	zakázka	k1gFnSc4	zakázka
na	na	k7c4	na
projekt	projekt	k1gInSc4	projekt
zahradního	zahradní	k2eAgNnSc2d1	zahradní
města	město	k1gNnSc2	město
podle	podle	k7c2	podle
anglického	anglický	k2eAgInSc2d1	anglický
vzoru	vzor	k1gInSc2	vzor
zadal	zadat	k5eAaPmAgInS	zadat
architektu	architekt	k1gMnSc3	architekt
Gaudímu	Gaudí	k1gMnSc3	Gaudí
průmyslník	průmyslník	k1gMnSc1	průmyslník
Eusebi	Euseb	k1gMnSc3	Euseb
Güell	Güella	k1gFnPc2	Güella
<g/>
.	.	kIx.	.
</s>
<s>
Antoni	Anton	k1gMnPc1	Anton
Gaudí	Gaudí	k1gNnSc2	Gaudí
vyprojektoval	vyprojektovat	k5eAaPmAgMnS	vyprojektovat
zahradní	zahradní	k2eAgNnSc1d1	zahradní
město	město	k1gNnSc1	město
s	s	k7c7	s
asi	asi	k9	asi
60	[number]	k4	60
stavebními	stavební	k2eAgFnPc7d1	stavební
parcelami	parcela	k1gFnPc7	parcela
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
financován	financovat	k5eAaBmNgInS	financovat
z	z	k7c2	z
předem	předem	k6eAd1	předem
zaplacených	zaplacený	k2eAgFnPc2d1	zaplacená
záloh	záloha	k1gFnPc2	záloha
za	za	k7c4	za
dosud	dosud	k6eAd1	dosud
nepostavené	postavený	k2eNgInPc4d1	nepostavený
domy	dům	k1gInPc4	dům
<g/>
,	,	kIx,	,
našli	najít	k5eAaPmAgMnP	najít
se	se	k3xPyFc4	se
však	však	k9	však
pouze	pouze	k6eAd1	pouze
2	[number]	k4	2
kupci	kupec	k1gMnPc1	kupec
-	-	kIx~	-
samotní	samotný	k2eAgMnPc1d1	samotný
stavitelé	stavitel	k1gMnPc1	stavitel
parku	park	k1gInSc2	park
Gaudí	Gaudí	k1gNnSc2	Gaudí
a	a	k8xC	a
Güell	Güella	k1gFnPc2	Güella
<g/>
.	.	kIx.	.
</s>
<s>
Realizace	realizace	k1gFnSc1	realizace
projektu	projekt	k1gInSc2	projekt
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nezdařila	zdařit	k5eNaPmAgFnS	zdařit
a	a	k8xC	a
projekt	projekt	k1gInSc4	projekt
zahradního	zahradní	k2eAgNnSc2d1	zahradní
města	město	k1gNnSc2	město
nebyl	být	k5eNaImAgInS	být
nikdy	nikdy	k6eAd1	nikdy
dokončen	dokončit	k5eAaPmNgInS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Park	park	k1gInSc4	park
posléze	posléze	k6eAd1	posléze
odkoupilo	odkoupit	k5eAaPmAgNnS	odkoupit
město	město	k1gNnSc1	město
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
zpřístupnilo	zpřístupnit	k5eAaPmAgNnS	zpřístupnit
jako	jako	k8xC	jako
městský	městský	k2eAgInSc1d1	městský
park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Park	park	k1gInSc1	park
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
17,8	[number]	k4	17,8
hektarů	hektar	k1gInPc2	hektar
<g/>
.	.	kIx.	.
</s>
<s>
Osobitý	osobitý	k2eAgInSc1d1	osobitý
styl	styl	k1gInSc1	styl
architekta	architekt	k1gMnSc2	architekt
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
i	i	k9	i
v	v	k7c6	v
těch	ten	k3xDgInPc6	ten
nejmenších	malý	k2eAgInPc6d3	nejmenší
detailech	detail	k1gInPc6	detail
<g/>
.	.	kIx.	.
</s>
<s>
Promenády	promenáda	k1gFnPc1	promenáda
jsou	být	k5eAaImIp3nP	být
kryty	kryt	k2eAgInPc1d1	kryt
sloupořadím	sloupořadí	k1gNnSc7	sloupořadí
se	s	k7c7	s
sloupy	sloup	k1gInPc7	sloup
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
zvlněné	zvlněný	k2eAgInPc1d1	zvlněný
tvary	tvar	k1gInPc1	tvar
připomínají	připomínat	k5eAaImIp3nP	připomínat
potoky	potok	k1gInPc1	potok
lávy	láva	k1gFnSc2	láva
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
plochy	plocha	k1gFnPc1	plocha
architektonických	architektonický	k2eAgInPc2d1	architektonický
prvků	prvek	k1gInPc2	prvek
parku	park	k1gInSc2	park
jsou	být	k5eAaImIp3nP	být
kryty	kryt	k2eAgInPc1d1	kryt
barevnými	barevný	k2eAgFnPc7d1	barevná
mozaikami	mozaika	k1gFnPc7	mozaika
z	z	k7c2	z
glazovaných	glazovaný	k2eAgInPc2d1	glazovaný
keramických	keramický	k2eAgInPc2d1	keramický
střepů	střep	k1gInPc2	střep
či	či	k8xC	či
skla	sklo	k1gNnSc2	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Domky	domek	k1gInPc1	domek
stojící	stojící	k2eAgInPc1d1	stojící
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
hlavního	hlavní	k2eAgInSc2d1	hlavní
vchodu	vchod	k1gInSc2	vchod
nesou	nést	k5eAaImIp3nP	nést
typické	typický	k2eAgInPc1d1	typický
znaky	znak	k1gInPc1	znak
Gaudího	Gaudí	k2eAgInSc2d1	Gaudí
rukopisu	rukopis	k1gInSc2	rukopis
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
parku	park	k1gInSc2	park
je	být	k5eAaImIp3nS	být
řešen	řešen	k2eAgInSc1d1	řešen
širokým	široký	k2eAgNnSc7d1	široké
schodištěm	schodiště	k1gNnSc7	schodiště
<g/>
,	,	kIx,	,
vedoucím	vedoucí	k1gMnPc3	vedoucí
k	k	k7c3	k
Síni	síň	k1gFnSc3	síň
sta	sto	k4xCgNnPc4	sto
sloupů	sloup	k1gInPc2	sloup
(	(	kIx(	(
<g/>
katalánsky	katalánsky	k6eAd1	katalánsky
Sala	Salus	k1gMnSc2	Salus
de	de	k?	de
les	les	k1gInSc1	les
cent	cent	k1gInSc1	cent
columnes	columnes	k1gInSc4	columnes
<g/>
,	,	kIx,	,
španělsky	španělsky	k6eAd1	španělsky
Sala	Sala	k1gFnSc1	Sala
de	de	k?	de
las	laso	k1gNnPc2	laso
Cien	Cien	k1gMnSc1	Cien
Columnas	Columnas	k1gMnSc1	Columnas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Síň	síň	k1gFnSc1	síň
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
86	[number]	k4	86
sloupy	sloup	k1gInPc7	sloup
připomínajícími	připomínající	k2eAgInPc7d1	připomínající
stalagnáty	stalagnát	k1gInPc7	stalagnát
v	v	k7c6	v
obrovské	obrovský	k2eAgFnSc6d1	obrovská
jeskyni	jeskyně	k1gFnSc6	jeskyně
<g/>
.	.	kIx.	.
</s>
<s>
Sloupy	sloup	k1gInPc1	sloup
nesou	nést	k5eAaImIp3nP	nést
rozlehlou	rozlehlý	k2eAgFnSc4d1	rozlehlá
terasu	terasa	k1gFnSc4	terasa
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
lemována	lemovat	k5eAaImNgFnS	lemovat
zvlněným	zvlněný	k2eAgFnPc3d1	zvlněná
<g/>
,	,	kIx,	,
mozaikou	mozaika	k1gFnSc7	mozaika
zdobeným	zdobený	k2eAgNnSc7d1	zdobené
zábradlím	zábradlí	k1gNnSc7	zábradlí
<g/>
,	,	kIx,	,
upraveným	upravený	k2eAgMnSc7d1	upravený
současně	současně	k6eAd1	současně
jako	jako	k8xS	jako
lavice	lavice	k1gFnSc1	lavice
na	na	k7c6	na
sezení	sezení	k1gNnSc6	sezení
<g/>
.	.	kIx.	.
</s>
<s>
Terasa	terasa	k1gFnSc1	terasa
tvoří	tvořit	k5eAaImIp3nS	tvořit
dominantu	dominanta	k1gFnSc4	dominanta
parku	park	k1gInSc2	park
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
místem	místo	k1gNnSc7	místo
setkávání	setkávání	k1gNnSc2	setkávání
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
různých	různý	k2eAgFnPc2d1	různá
slavností	slavnost	k1gFnPc2	slavnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parku	park	k1gInSc6	park
stojí	stát	k5eAaImIp3nP	stát
dva	dva	k4xCgInPc1	dva
domy	dům	k1gInPc1	dům
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
bydleli	bydlet	k5eAaImAgMnP	bydlet
Eusebi	Euseb	k1gMnPc1	Euseb
Güell	Güella	k1gFnPc2	Güella
(	(	kIx(	(
<g/>
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
a	a	k8xC	a
Antoni	Anton	k1gMnPc1	Anton
Gaudí	Gaudí	k1gNnSc2	Gaudí
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
domě	dům	k1gInSc6	dům
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
nachází	nacházet	k5eAaImIp3nS	nacházet
museum	museum	k1gNnSc4	museum
věnované	věnovaný	k2eAgFnSc2d1	věnovaná
tomuto	tento	k3xDgMnSc3	tento
katalánskému	katalánský	k2eAgMnSc3d1	katalánský
architektovi	architekt	k1gMnSc3	architekt
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
vyvýšené	vyvýšený	k2eAgFnSc3d1	vyvýšená
poloze	poloha	k1gFnSc3	poloha
na	na	k7c6	na
kraji	kraj	k1gInSc6	kraj
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
park	park	k1gInSc1	park
místem	místo	k1gNnSc7	místo
klidu	klid	k1gInSc2	klid
kontrastujícím	kontrastující	k2eAgFnPc3d1	kontrastující
s	s	k7c7	s
ruchem	ruch	k1gInSc7	ruch
katalánské	katalánský	k2eAgFnSc2d1	katalánská
metropole	metropol	k1gFnSc2	metropol
<g/>
.	.	kIx.	.
</s>
