<s>
Georg	Georg	k1gInSc1
Haas	Haasa	k1gFnPc2
z	z	k7c2
Hasenfelsu	Hasenfels	k1gInSc2
</s>
<s>
Georg	Georg	k1gInSc1
Haas	Haasa	k1gFnPc2
z	z	k7c2
Hasenfelsu	Hasenfels	k1gInSc2
Jiří	Jiří	k1gMnSc1
Haas	Haasa	k1gFnPc2
z	z	k7c2
Hasenfelsu	Hasenfels	k1gInSc2
st.	st.	kA
Narození	narození	k1gNnSc5
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1841	#num#	k4
nebo	nebo	k8xC
1841	#num#	k4
<g/>
Kamenný	kamenný	k2eAgInSc4d1
Dvůr	Dvůr	k1gInSc4
Rakouské	rakouský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
Rakouské	rakouský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1914	#num#	k4
nebo	nebo	k8xC
1914	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
72	#num#	k4
<g/>
–	–	k?
<g/>
73	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
zámek	zámek	k1gInSc1
Mostov	Mostov	k1gInSc1
Rakousko-Uhersko	Rakousko-Uherska	k1gFnSc5
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
průmyslník	průmyslník	k1gMnSc1
a	a	k8xC
podnikatel	podnikatel	k1gMnSc1
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Řád	řád	k1gInSc1
Františka	František	k1gMnSc2
Josefa	Josef	k1gMnSc2
Děti	dítě	k1gFnPc1
</s>
<s>
Georg	Georg	k1gInSc1
Haas	Haas	k1gInSc1
von	von	k1gInSc1
Hasenfels	Hasenfelsa	k1gFnPc2
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
propagační	propagační	k2eAgFnSc1d1
tiskovina	tiskovina	k1gFnSc1
porcelánky	porcelánka	k1gFnSc2
Haas	Haas	k1gInSc1
<g/>
&	&	k?
<g/>
Czjzek	Czjzek	k1gInSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Haas	Haasa	k1gFnPc2
z	z	k7c2
Hasenfelsu	Hasenfels	k1gInSc2
st.	st.	kA
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1841	#num#	k4
Kamenný	kamenný	k2eAgInSc1d1
Dvůr	Dvůr	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
–	–	k?
29	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1914	#num#	k4
zámek	zámek	k1gInSc1
Mostov	Mostov	k1gInSc4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
rakousko-uherský	rakousko-uherský	k2eAgMnSc1d1
a	a	k8xC
český	český	k2eAgMnSc1d1
podnikatel	podnikatel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
spolumajitelem	spolumajitel	k1gMnSc7
porcelánky	porcelánka	k1gFnSc2
Haas	Haasa	k1gFnPc2
&	&	k?
Cžjžek	Cžjžka	k1gFnPc2
v	v	k7c6
Horním	horní	k2eAgInSc6d1
Slavkově	Slavkov	k1gInSc6
<g/>
,	,	kIx,
největší	veliký	k2eAgFnSc7d3
v	v	k7c6
rakousko-uherské	rakousko-uherský	k2eAgFnSc6d1
monarchii	monarchie	k1gFnSc6
a	a	k8xC
druhé	druhý	k4xOgNnSc4
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
(	(	kIx(
<g/>
založené	založený	k2eAgInPc1d1
r.	r.	kA
1792	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Haas	Haasa	k1gFnPc2
vystudoval	vystudovat	k5eAaPmAgMnS
chemii	chemie	k1gFnSc4
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
pracoval	pracovat	k5eAaImAgMnS
v	v	k7c6
porcelánce	porcelánka	k1gFnSc6
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
Augusta	August	k1gMnSc2
Haase	Haas	k1gMnSc2
v	v	k7c6
Horním	horní	k2eAgInSc6d1
Slavkově	Slavkov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
během	během	k7c2
života	život	k1gInSc2
zde	zde	k6eAd1
také	také	k9
zaměstnal	zaměstnat	k5eAaPmAgMnS
Jana	Jan	k1gMnSc4
Baptistu	baptista	k1gMnSc4
Čžjžka	Čžjžka	k1gFnSc1
(	(	kIx(
<g/>
20	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
1841	#num#	k4
Vídeň	Vídeň	k1gFnSc1
–	–	k?
6	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
1925	#num#	k4
tamtéž	tamtéž	k6eAd1
<g/>
)	)	kIx)
v	v	k7c6
roce	rok	k1gInSc6
1857	#num#	k4
<g/>
,	,	kIx,
se	s	k7c7
kterým	který	k3yQgMnSc7,k3yIgMnSc7,k3yRgMnSc7
se	se	k3xPyFc4
seznámil	seznámit	k5eAaPmAgMnS
během	během	k7c2
své	svůj	k3xOyFgFnSc2
návštěvy	návštěva	k1gFnSc2
Vídně	Vídeň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chybou	chyba	k1gFnSc7
v	v	k7c6
záznamech	záznam	k1gInPc6
se	se	k3xPyFc4
dosud	dosud	k6eAd1
přepisovaně	přepisovaně	k6eAd1
dlouhodobě	dlouhodobě	k6eAd1
uváděl	uvádět	k5eAaImAgInS
omyl	omyl	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
Jan	Jan	k1gMnSc1
Cžjžek	Cžjžek	k1gInSc1
a	a	k8xC
Jiří	Jiří	k1gMnPc1
Haas	Haasa	k1gFnPc2
byli	být	k5eAaImAgMnP
bratranci	bratranec	k1gMnPc1
(	(	kIx(
<g/>
či	či	k8xC
jiní	jiný	k2eAgMnPc1d1
příbuzní	příbuzný	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
omyl	omyl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebyli	být	k5eNaImAgMnP
v	v	k7c6
žádném	žádný	k3yNgInSc6
příbuzenském	příbuzenský	k2eAgInSc6d1
poměru	poměr	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
svou	svůj	k3xOyFgFnSc7
smrtí	smrt	k1gFnSc7
přepsal	přepsat	k5eAaPmAgInS
svou	svůj	k3xOyFgFnSc4
továrnu	továrna	k1gFnSc4
na	na	k7c4
svého	svůj	k3xOyFgMnSc4
syna	syn	k1gMnSc4
Jiřího	Jiří	k1gMnSc4
Haase	Haas	k1gMnSc4
ml.	ml.	kA
a	a	k8xC
Jana	Jana	k1gFnSc1
Čžjžka	Čžjžka	k1gFnSc1
<g/>
,	,	kIx,
každému	každý	k3xTgMnSc3
polovinu	polovina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgMnPc1
poté	poté	k6eAd1
sepsali	sepsat	k5eAaPmAgMnP
6	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1867	#num#	k4
společenskou	společenský	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
a	a	k8xC
pokračovali	pokračovat	k5eAaImAgMnP
od	od	k7c2
té	ten	k3xDgFnSc2
chvíle	chvíle	k1gFnSc2
dál	daleko	k6eAd2
ve	v	k7c6
výrobě	výroba	k1gFnSc6
jako	jako	k8xS,k8xC
porcelánka	porcelánka	k1gFnSc1
Haas	Haasa	k1gFnPc2
&	&	k?
Cžjžek	Cžjžka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podniku	podnik	k1gInSc2
se	se	k3xPyFc4
dařilo	dařit	k5eAaImAgNnS
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1871	#num#	k4
přikoupili	přikoupit	k5eAaPmAgMnP
porcelánku	porcelánka	k1gFnSc4
od	od	k7c2
Mosese	Mosese	k1gFnSc2
Porgese	Porgese	k1gFnSc2
z	z	k7c2
Portheimu	Portheim	k1gInSc2
v	v	k7c6
Chodově	Chodov	k1gInSc6
a	a	k8xC
dohromady	dohromady	k6eAd1
zaměstnávali	zaměstnávat	k5eAaImAgMnP
asi	asi	k9
tisíc	tisíc	k4xCgInSc1
dělníků	dělník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Věhlas	věhlas	k1gInSc1
značky	značka	k1gFnSc2
Haas	Haasa	k1gFnPc2
&	&	k?
Cžjžek	Cžjžek	k1gInSc1
rostl	růst	k5eAaImAgInS
<g/>
,	,	kIx,
na	na	k7c6
začátku	začátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
a	a	k8xC
v	v	k7c6
meziválečném	meziválečný	k2eAgNnSc6d1
Československu	Československo	k1gNnSc6
patřila	patřit	k5eAaImAgFnS
k	k	k7c3
největším	veliký	k2eAgMnPc3d3
výrobcům	výrobce	k1gMnPc3
porcelánu	porcelán	k1gInSc2
a	a	k8xC
jednou	jeden	k4xCgFnSc7
z	z	k7c2
nejuznávanějších	uznávaný	k2eAgFnPc2d3
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
16	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1909	#num#	k4
byl	být	k5eAaImAgMnS
Jiřímu	Jiří	k1gMnSc3
Haasovi	Haas	k1gMnSc3
udělen	udělen	k2eAgInSc4d1
šlechtický	šlechtický	k2eAgInSc4d1
titul	titul	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
titul	titul	k1gInSc1
připadl	připadnout	k5eAaPmAgInS
dědičně	dědičně	k6eAd1
na	na	k7c4
všechny	všechen	k3xTgMnPc4
členy	člen	k1gMnPc4
rodiny	rodina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Janu	Jan	k1gMnSc3
Čžjžkovi	Čžjžek	k1gMnSc3
se	se	k3xPyFc4
totéž	týž	k3xTgNnSc1
povedlo	povést	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1909	#num#	k4
o	o	k7c4
něco	něco	k3yInSc4
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gNnPc1
plná	plný	k2eAgNnPc1d1
jména	jméno	k1gNnPc1
pak	pak	k6eAd1
zněla	znět	k5eAaImAgNnP
Jiří	Jiří	k1gMnSc1
Baron	baron	k1gMnSc1
Haas	Haas	k1gInSc4
von	von	k1gInSc4
Hasenfels	Hasenfelsa	k1gFnPc2
a	a	k8xC
Jan	Jan	k1gMnSc1
Baptista	baptista	k1gMnSc1
Cžjžek	Cžjžka	k1gFnPc2
Adler	Adler	k1gMnSc1
von	von	k1gInSc4
Smidaich	Smidaich	k1gInSc1
(	(	kIx(
<g/>
Smidary	Smidara	k1gFnPc1
u	u	k7c2
Nového	Nového	k2eAgInSc2d1
Bydžova	Bydžov	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
Haas	Haasa	k1gFnPc2
již	již	k6eAd1
byl	být	k5eAaImAgInS
majitelem	majitel	k1gMnSc7
zámků	zámek	k1gInPc2
Kamenný	kamenný	k2eAgInSc4d1
Dvůr	Dvůr	k1gInSc4
u	u	k7c2
Kynšperka	Kynšperka	k1gFnSc1
nad	nad	k7c7
Ohří	Ohře	k1gFnSc7
a	a	k8xC
Mostova	Mostův	k2eAgFnSc1d1
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1912	#num#	k4
přikoupil	přikoupit	k5eAaPmAgMnS
ještě	ještě	k9
hrad	hrad	k1gInSc4
Bítov	Bítov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Firma	firma	k1gFnSc1
jako	jako	k8xC,k8xS
první	první	k4xOgNnSc1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
založila	založit	k5eAaPmAgFnS
pokladnu	pokladna	k1gFnSc4
pro	pro	k7c4
invalidy	invalid	k1gMnPc4
<g/>
,	,	kIx,
vdovy	vdova	k1gFnPc4
<g/>
,	,	kIx,
sirotky	sirotka	k1gFnPc4
a	a	k8xC
penzijní	penzijní	k2eAgFnSc4d1
pokladnu	pokladna	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
smrti	smrt	k1gFnSc6
Jiřího	Jiří	k1gMnSc2
Haase	Haase	k1gFnSc2
st.	st.	kA
(	(	kIx(
<g/>
pochován	pochován	k2eAgMnSc1d1
byl	být	k5eAaImAgMnS
v	v	k7c6
kamenné	kamenný	k2eAgFnSc6d1
hrobce	hrobka	k1gFnSc6
u	u	k7c2
zámku	zámek	k1gInSc2
Kamenný	kamenný	k2eAgInSc1d1
Dvůr	Dvůr	k1gInSc1
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
připadl	připadnout	k5eAaPmAgInS
jeho	jeho	k3xOp3gInSc1
podíl	podíl	k1gInSc1
manželce	manželka	k1gFnSc3
Olze	Olga	k1gFnSc3
a	a	k8xC
synovi	syn	k1gMnSc3
Jiřímu	Jiří	k1gMnSc3
ml.	ml.	kA
Podíl	podíl	k1gInSc1
Jana	Jan	k1gMnSc2
Baptisty	baptista	k1gMnSc2
Čžjžka	Čžjžka	k1gFnSc1
přešel	přejít	k5eAaPmAgInS
roku	rok	k1gInSc2
1923	#num#	k4
na	na	k7c4
jeho	on	k3xPp3gMnSc4,k3xOp3gMnSc4
syna	syn	k1gMnSc4
Felixe	Felix	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
závěru	závěr	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
dosahovala	dosahovat	k5eAaImAgFnS
výroba	výroba	k1gFnSc1
až	až	k9
200	#num#	k4
tun	tuna	k1gFnPc2
porcelánu	porcelán	k1gInSc2
měsíčně	měsíčně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
24	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1945	#num#	k4
byla	být	k5eAaImAgFnS
továrna	továrna	k1gFnSc1
znárodněna	znárodnit	k5eAaPmNgFnS
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1958	#num#	k4
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
součástí	součást	k1gFnSc7
podniku	podnik	k1gInSc2
Karlovarský	karlovarský	k2eAgInSc1d1
porcelán	porcelán	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
byla	být	k5eAaImAgFnS
privatizována	privatizovat	k5eAaImNgFnS
a	a	k8xC
vrátila	vrátit	k5eAaPmAgFnS
se	se	k3xPyFc4
k	k	k7c3
názvu	název	k1gInSc3
Haas	Haasa	k1gFnPc2
&	&	k?
Czjzek	Czjzka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
se	se	k3xPyFc4
majoritním	majoritní	k2eAgMnSc7d1
vlastníkem	vlastník	k1gMnSc7
firmy	firma	k1gFnSc2
stal	stát	k5eAaPmAgMnS
ruský	ruský	k2eAgMnSc1d1
majitel	majitel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lednu	leden	k1gInSc6
roku	rok	k1gInSc2
2011	#num#	k4
však	však	k9
zkrachovala	zkrachovat	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Oživit	oživit	k5eAaPmF
se	se	k3xPyFc4
ji	on	k3xPp3gFnSc4
pak	pak	k6eAd1
pokusila	pokusit	k5eAaPmAgFnS
pražská	pražský	k2eAgFnSc1d1
firma	firma	k1gFnSc1
Sideline	Sidelin	k1gInSc5
<g/>
,	,	kIx,
v	v	k7c6
únoru	únor	k1gInSc6
2014	#num#	k4
však	však	k9
stála	stát	k5eAaImAgFnS
před	před	k7c7
exekucí	exekuce	k1gFnSc7
a	a	k8xC
její	její	k3xOp3gInSc1
majetek	majetek	k1gInSc1
se	se	k3xPyFc4
prodával	prodávat	k5eAaImAgInS
v	v	k7c6
dražbě	dražba	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rodina	rodina	k1gFnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Haas	Haasa	k1gFnPc2
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Olgou	Olga	k1gFnSc7
Dannenbergovou	Dannenbergův	k2eAgFnSc7d1
z	z	k7c2
Žitavy	Žitava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
otec	otec	k1gMnSc1
Jan	Jan	k1gMnSc1
Karel	Karel	k1gMnSc1
Julius	Julius	k1gMnSc1
Dannenberg	Dannenberg	k1gMnSc1
(	(	kIx(
<g/>
1817	#num#	k4
<g/>
–	–	k?
<g/>
1884	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
obchodníkem	obchodník	k1gMnSc7
a	a	k8xC
pocházel	pocházet	k5eAaImAgMnS
z	z	k7c2
Berlína	Berlín	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
se	se	k3xPyFc4
přestěhoval	přestěhovat	k5eAaPmAgInS
do	do	k7c2
obce	obec	k1gFnSc2
Hirschfelde	Hirschfeld	k1gInSc5
u	u	k7c2
města	město	k1gNnSc2
Žitavy	Žitava	k1gFnSc2
<g/>
,	,	kIx,
Zde	zde	k6eAd1
založil	založit	k5eAaPmAgMnS
významou	významý	k2eAgFnSc4d1
textilní	textilní	k2eAgFnSc4d1
továrnu	továrna	k1gFnSc4
"	"	kIx"
<g/>
Fraenkelsche	Fraenkelsch	k1gInPc4
Orleans	Orleans	k1gInSc1
<g/>
"	"	kIx"
a	a	k8xC
oženil	oženit	k5eAaPmAgMnS
se	se	k3xPyFc4
zde	zde	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
manželkou	manželka	k1gFnSc7
a	a	k8xC
matkou	matka	k1gFnSc7
Olgy	Olga	k1gFnSc2
byla	být	k5eAaImAgFnS
Augusta	August	k1gMnSc4
Rosalie	Rosalie	k1gFnSc2
Hertzschová	Hertzschová	k1gFnSc1
z	z	k7c2
obce	obec	k1gFnSc2
Meerane	Meeran	k1gInSc5
(	(	kIx(
<g/>
1833	#num#	k4
<g/>
–	–	k?
<g/>
1816	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měli	mít	k5eAaImAgMnP
společně	společně	k6eAd1
celkem	celkem	k6eAd1
šest	šest	k4xCc4
dětí	dítě	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
Haas	Haas	k1gInSc4
s	s	k7c7
manželkou	manželka	k1gFnSc7
Olgou	Olga	k1gFnSc7
měli	mít	k5eAaImAgMnP
společně	společně	k6eAd1
jednoho	jeden	k4xCgMnSc4
syna	syn	k1gMnSc4
<g/>
,	,	kIx,
Jiřího	Jiří	k1gMnSc4
(	(	kIx(
<g/>
26	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
1876	#num#	k4
Mostov	Mostov	k1gInSc1
–	–	k?
11	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
1945	#num#	k4
Bítov	Bítov	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterému	který	k3yQgMnSc3,k3yRgMnSc3,k3yIgMnSc3
otec	otec	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1912	#num#	k4
koupil	koupit	k5eAaPmAgInS
hrad	hrad	k1gInSc1
Bítov	Bítov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
ml.	ml.	kA
zde	zde	k6eAd1
žil	žít	k5eAaImAgMnS
obklopen	obklopit	k5eAaPmNgMnS
mnoha	mnoho	k4c2
zvířaty	zvíře	k1gNnPc7
<g/>
,	,	kIx,
měl	mít	k5eAaImAgInS
rád	rád	k6eAd1
psy	pes	k1gMnPc4
a	a	k8xC
jezdecké	jezdecký	k2eAgMnPc4d1
koně	kůň	k1gMnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
choval	chovat	k5eAaImAgMnS
zde	zde	k6eAd1
např.	např.	kA
i	i	k9
mravenečníka	mravenečník	k1gMnSc4
nebo	nebo	k8xC
lvici	lvice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svou	svůj	k3xOyFgFnSc4
zoologickou	zoologický	k2eAgFnSc4d1
zahradu	zahrada	k1gFnSc4
nechával	nechávat	k5eAaImAgMnS
o	o	k7c6
víkendech	víkend	k1gInPc6
zpřístupnit	zpřístupnit	k5eAaPmF
zdarma	zdarma	k6eAd1
veřejnosti	veřejnost	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měl	mít	k5eAaImAgMnS
také	také	k9
velkou	velký	k2eAgFnSc4d1
slabost	slabost	k1gFnSc4
pro	pro	k7c4
ženy	žena	k1gFnPc4
<g/>
,	,	kIx,
v	v	k7c4
okolí	okolí	k1gNnSc4
měl	mít	k5eAaImAgMnS
dlouhou	dlouhý	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
milenek	milenka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
celou	celý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
svého	svůj	k3xOyFgInSc2
života	život	k1gInSc2
byl	být	k5eAaImAgInS
společně	společně	k6eAd1
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
matkou	matka	k1gFnSc7
spoluvlastníkem	spoluvlastník	k1gMnSc7
obou	dva	k4xCgFnPc2
porcelánek	porcelánka	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
nikterak	nikterak	k6eAd1
se	se	k3xPyFc4
o	o	k7c4
ně	on	k3xPp3gMnPc4
nezajímal	zajímat	k5eNaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žil	žít	k5eAaImAgMnS
si	se	k3xPyFc3
svým	svůj	k3xOyFgInSc7
životem	život	k1gInSc7
na	na	k7c6
Bítově	Bítov	k1gInSc6
a	a	k8xC
do	do	k7c2
Západních	západní	k2eAgFnPc2d1
Čech	Čechy	k1gFnPc2
zajížděl	zajíždět	k5eAaImAgInS
jen	jen	k9
mimořádně	mimořádně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
v	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
Jiří	Jiří	k1gMnPc2
přes	přes	k7c4
své	svůj	k3xOyFgInPc4
protinacistické	protinacistický	k2eAgInPc4d1
postoje	postoj	k1gInPc4
odsunut	odsunut	k2eAgInSc1d1
<g/>
,	,	kIx,
rozhodl	rozhodnout	k5eAaPmAgInS
se	se	k3xPyFc4
dobrovolně	dobrovolně	k6eAd1
ukončit	ukončit	k5eAaPmF
svůj	svůj	k3xOyFgInSc4
život	život	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
svůj	svůj	k3xOyFgInSc4
velice	velice	k6eAd1
bohatý	bohatý	k2eAgInSc4d1
sexuální	sexuální	k2eAgInSc4d1
život	život	k1gInSc4
mu	on	k3xPp3gMnSc3
byly	být	k5eAaImAgFnP
soudem	soud	k1gInSc7
uznané	uznaný	k2eAgInPc4d1
pouze	pouze	k6eAd1
dvě	dva	k4xCgFnPc1
děti	dítě	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
neměly	mít	k5eNaImAgFnP
další	další	k2eAgMnPc4d1
potomky	potomek	k1gMnPc4
<g/>
,	,	kIx,
Jiřím	Jiří	k1gMnSc7
tak	tak	k8xC,k8xS
rod	rod	k1gInSc4
Haasů	Haas	k1gMnPc2
vymřel	vymřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Matriční	matriční	k2eAgInSc4d1
záznam	záznam	k1gInSc4
o	o	k7c6
narození	narození	k1gNnSc6
a	a	k8xC
křtu	křest	k1gInSc6
farnost	farnost	k1gFnSc1
při	při	k7c6
kostele	kostel	k1gInSc6
Kynšperk	Kynšperk	k1gInSc1
nad	nad	k7c7
Ohří	Ohře	k1gFnSc7
↑	↑	k?
Matriční	matriční	k2eAgInSc4d1
záznam	záznam	k1gInSc4
o	o	k7c6
úmrtí	úmrtí	k1gNnSc6
a	a	k8xC
pohřbu	pohřeb	k1gInSc6
farnost	farnost	k1gFnSc1
Kynšperk	Kynšperk	k1gInSc1
nad	nad	k7c7
Ohří	Ohře	k1gFnSc7
1	#num#	k4
2	#num#	k4
Historie	historie	k1gFnSc1
značky	značka	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Haas-Czjzek	Haas-Czjzek	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Osobnosti	osobnost	k1gFnPc1
ze	z	k7c2
šuplíku	šuplíku	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znojemský	znojemský	k2eAgInSc1d1
týden	týden	k1gInSc1
<g/>
,	,	kIx,
2010	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
POLÁK	Polák	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejstarší	starý	k2eAgFnSc1d3
česká	český	k2eAgFnSc1d1
porcelánka	porcelánka	k1gFnSc1
končí	končit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ZEMAN	Zeman	k1gMnSc1
<g/>
,	,	kIx,
Bohumil	Bohumil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kdysi	kdysi	k6eAd1
slavnou	slavný	k2eAgFnSc4d1
porcelánku	porcelánka	k1gFnSc4
Haas	Haasa	k1gFnPc2
&	&	k?
Czjzek	Czjzka	k1gFnPc2
tíží	tížit	k5eAaImIp3nS
exekuce	exekuce	k1gFnSc1
<g/>
,	,	kIx,
hledá	hledat	k5eAaImIp3nS
však	však	k9
lidi	člověk	k1gMnPc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ZEDNÍK	Zedník	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Porcelánový	porcelánový	k2eAgMnSc1d1
baron	baron	k1gMnSc1
Jiří	Jiří	k1gMnSc1
Haas	Haas	k1gInSc4
byl	být	k5eAaImAgMnS
velkým	velký	k2eAgMnSc7d1
svůdníkem	svůdník	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Milenky	Milenka	k1gFnPc4
si	se	k3xPyFc3
známkoval	známkovat	k5eAaImAgInS
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
JAŠA	Jaša	k1gMnSc1
<g/>
,	,	kIx,
Luděk	Luděk	k1gMnSc1
ing	ing	kA
<g/>
:	:	kIx,
Jiří	Jiří	k1gMnSc1
Haas	Haasa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Porcelánový	porcelánový	k2eAgMnSc1d1
baron	baron	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sokolov	Sokolov	k1gInSc1
<g/>
,	,	kIx,
Fornica	Fornica	k1gFnSc1
Graphics	Graphicsa	k1gFnPc2
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-80-87194-39-3	978-80-87194-39-3	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Georg	Georg	k1gMnSc1
Haas	Haas	k1gInSc1
z	z	k7c2
Hasenfelsu	Hasenfels	k1gInSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Znojemský	znojemský	k2eAgInSc1d1
týden	týden	k1gInSc1
<g/>
,	,	kIx,
OSOBNOSTI	osobnost	k1gFnPc1
ZE	z	k7c2
ŠUPLÍKU-rodu	ŠUPLÍKU-rod	k1gInSc2
Haasů	Haas	k1gMnPc2
z	z	k7c2
Hasenfelsu	Hasenfels	k1gInSc2
</s>
<s>
Haas	Haas	k1gInSc1
<g/>
&	&	k?
<g/>
Czjzek	Czjzek	k1gInSc1
historie	historie	k1gFnSc2
značky	značka	k1gFnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jx	jx	k?
<g/>
20131108003	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
1054971404	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0004	#num#	k4
2349	#num#	k4
1236	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
nb	nb	k?
<g/>
2020005870	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
305463888	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-nb	lccn-nb	k1gInSc1
<g/>
2020005870	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
