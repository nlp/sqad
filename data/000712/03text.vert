<s>
The	The	k?	The
Walt	Walt	k1gInSc1	Walt
Disney	Disne	k2eAgInPc1d1	Disne
Company	Compan	k1gInPc7	Compan
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Společnost	společnost	k1gFnSc1	společnost
Walta	Walt	k1gMnSc2	Walt
Disneyho	Disney	k1gMnSc2	Disney
a	a	k8xC	a
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
Disney	Disnea	k1gFnSc2	Disnea
<g/>
,	,	kIx,	,
NYSE	NYSE	kA	NYSE
<g/>
:	:	kIx,	:
DIS	dis	k1gNnSc4	dis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
mediálních	mediální	k2eAgFnPc2d1	mediální
a	a	k8xC	a
zábavních	zábavní	k2eAgFnPc2d1	zábavní
společností	společnost	k1gFnPc2	společnost
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Založili	založit	k5eAaPmAgMnP	založit
ji	on	k3xPp3gFnSc4	on
bratři	bratr	k1gMnPc1	bratr
Walt	Waltum	k1gNnPc2	Waltum
a	a	k8xC	a
Roy	Roy	k1gMnSc1	Roy
Disneyovi	Disneya	k1gMnSc3	Disneya
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1923	[number]	k4	1923
jako	jako	k8xC	jako
původně	původně	k6eAd1	původně
animované	animovaný	k2eAgNnSc1d1	animované
studio	studio	k1gNnSc1	studio
Disney	Disnea	k1gFnSc2	Disnea
Brothers	Brothersa	k1gFnPc2	Brothersa
Studio	studio	k1gNnSc1	studio
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Walt	Walt	k1gInSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
Productions	Productionsa	k1gFnPc2	Productionsa
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
The	The	k1gFnSc1	The
Walt	Waltum	k1gNnPc2	Waltum
Disney	Disnea	k1gFnSc2	Disnea
Company	Compana	k1gFnSc2	Compana
stala	stát	k5eAaPmAgFnS	stát
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
hollywoodských	hollywoodský	k2eAgNnPc2d1	hollywoodské
studií	studio	k1gNnPc2	studio
(	(	kIx(	(
<g/>
Walt	Walt	k1gMnSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
Pictures	Pictures	k1gMnSc1	Pictures
<g/>
,	,	kIx,	,
Pixar	Pixar	k1gMnSc1	Pixar
<g/>
,	,	kIx,	,
Marvel	Marvel	k1gMnSc1	Marvel
a	a	k8xC	a
Lucasfilm	Lucasfilm	k1gMnSc1	Lucasfilm
<g/>
)	)	kIx)	)
a	a	k8xC	a
vlastníkem	vlastník	k1gMnSc7	vlastník
několika	několik	k4yIc2	několik
televizních	televizní	k2eAgFnPc2d1	televizní
stanic	stanice	k1gFnPc2	stanice
včetně	včetně	k7c2	včetně
ABC	ABC	kA	ABC
a	a	k8xC	a
ESPN	ESPN	kA	ESPN
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
a	a	k8xC	a
nejznámější	známý	k2eAgFnSc1d3	nejznámější
animovaná	animovaný	k2eAgFnSc1d1	animovaná
postavička	postavička	k1gFnSc1	postavička
studia	studio	k1gNnSc2	studio
Disney	Disnea	k1gMnSc2	Disnea
<g/>
,	,	kIx,	,
Mickey	Mickea	k1gMnSc2	Mickea
Mouse	Mous	k1gMnSc2	Mous
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
oficiální	oficiální	k2eAgInSc4d1	oficiální
maskot	maskot	k1gInSc4	maskot
The	The	k1gMnSc2	The
Walt	Walta	k1gFnPc2	Walta
Disney	Disnea	k1gMnSc2	Disnea
Company	Compana	k1gFnSc2	Compana
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1991	[number]	k4	1991
součástí	součást	k1gFnPc2	součást
Dow	Dow	k1gMnSc1	Dow
Jones	Jones	k1gMnSc1	Jones
Industrial	Industrial	k1gMnSc1	Industrial
Average	Average	k1gFnSc1	Average
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
animátor	animátor	k1gMnSc1	animátor
Walt	Walt	k1gInSc4	Walt
Disney	Disnea	k1gFnSc2	Disnea
krátký	krátký	k2eAgInSc4d1	krátký
film	film	k1gInSc4	film
pojmenovaný	pojmenovaný	k2eAgMnSc1d1	pojmenovaný
Alice	Alice	k1gFnPc4	Alice
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Wonderland	Wonderland	k1gInSc1	Wonderland
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
uváděla	uvádět	k5eAaImAgFnS	uvádět
mladá	mladý	k2eAgFnSc1d1	mladá
herečka	herečka	k1gFnSc1	herečka
Virgina	Virgina	k1gFnSc1	Virgina
Davisová	Davisový	k2eAgFnSc1d1	Davisová
v	v	k7c6	v
interakci	interakce	k1gFnSc6	interakce
s	s	k7c7	s
animovanými	animovaný	k2eAgFnPc7d1	animovaná
postavami	postava	k1gFnPc7	postava
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
bankrotu	bankrot	k1gInSc6	bankrot
své	svůj	k3xOyFgFnSc2	svůj
předchozí	předchozí	k2eAgFnSc2d1	předchozí
firmy	firma	k1gFnSc2	firma
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
se	se	k3xPyFc4	se
Walt	Walt	k1gInSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
vydal	vydat	k5eAaPmAgInS	vydat
do	do	k7c2	do
Hollywoodu	Hollywood	k1gInSc2	Hollywood
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgMnS	připojit
ke	k	k7c3	k
svému	svůj	k3xOyFgMnSc3	svůj
bratrovi	bratr	k1gMnSc3	bratr
Royi	Roy	k1gMnSc3	Roy
O.	O.	kA	O.
Disneymu	Disneym	k1gInSc6	Disneym
<g/>
.	.	kIx.	.
</s>
<s>
Filmový	filmový	k2eAgMnSc1d1	filmový
distributor	distributor	k1gMnSc1	distributor
Margaret	Margareta	k1gFnPc2	Margareta
J.	J.	kA	J.
Winkler	Winkler	k1gMnSc1	Winkler
z	z	k7c2	z
M.	M.	kA	M.
<g/>
J.	J.	kA	J.
Winkler	Winkler	k1gMnSc1	Winkler
Productions	Productions	k1gInSc1	Productions
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
zkontaktovala	zkontaktovat	k5eAaPmAgFnS	zkontaktovat
oba	dva	k4xCgMnPc4	dva
bratry	bratr	k1gMnPc4	bratr
Disneye	Disney	k1gInSc2	Disney
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gMnPc4	on
seznámila	seznámit	k5eAaPmAgFnS	seznámit
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
zájmem	zájem	k1gInSc7	zájem
o	o	k7c4	o
celou	celý	k2eAgFnSc4d1	celá
sérii	série	k1gFnSc4	série
Alice	Alice	k1gFnSc2	Alice
Comedies	Comediesa	k1gFnPc2	Comediesa
<g/>
.	.	kIx.	.
</s>
<s>
Platila	platit	k5eAaImAgFnS	platit
jim	on	k3xPp3gMnPc3	on
$	$	kIx~	$
<g/>
1,500	[number]	k4	1,500
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
filmový	filmový	k2eAgInSc4d1	filmový
kotouč	kotouč	k1gInSc4	kotouč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
Walt	Walt	k1gMnSc1	Walt
a	a	k8xC	a
Roy	Roy	k1gMnSc1	Roy
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
založit	založit	k5eAaPmF	založit
Disney	Disnea	k1gFnSc2	Disnea
Brothers	Brothersa	k1gFnPc2	Brothersa
Cartoon	Cartoon	k1gNnSc1	Cartoon
Studio	studio	k1gNnSc1	studio
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Alici	Alice	k1gFnSc6	Alice
následovaly	následovat	k5eAaImAgInP	následovat
další	další	k2eAgInPc1d1	další
animované	animovaný	k2eAgInPc1d1	animovaný
filmy	film	k1gInPc1	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1926	[number]	k4	1926
se	se	k3xPyFc4	se
s	s	k7c7	s
dokončením	dokončení	k1gNnSc7	dokončení
studia	studio	k1gNnSc2	studio
na	na	k7c4	na
Hyperion	Hyperion	k1gInSc4	Hyperion
Street	Street	k1gInSc1	Street
změnil	změnit	k5eAaPmAgInS	změnit
název	název	k1gInSc4	název
společnosti	společnost	k1gFnSc2	společnost
na	na	k7c4	na
Walt	Walt	k1gInSc4	Walt
Disney	Disnea	k1gFnSc2	Disnea
Studio	studio	k1gNnSc4	studio
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
Alice	Alice	k1gFnSc2	Alice
Comedies	Comedies	k1gInSc1	Comedies
vyrobil	vyrobit	k5eAaPmAgInS	vyrobit
Disney	Disnea	k1gFnSc2	Disnea
animovaný	animovaný	k2eAgInSc1d1	animovaný
seriál	seriál	k1gInSc1	seriál
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
s	s	k7c7	s
jeho	jeho	k3xOp3gMnPc7	jeho
vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc1	první
postavičkou	postavička	k1gFnSc7	postavička
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
"	"	kIx"	"
<g/>
Oswald	Oswaldo	k1gNnPc2	Oswaldo
the	the	k?	the
Lucky	Lucka	k1gFnSc2	Lucka
Rabbit	Rabbit	k1gFnSc2	Rabbit
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Šťastný	Šťastný	k1gMnSc1	Šťastný
králík	králík	k1gMnSc1	králík
Oswald	Oswald	k1gMnSc1	Oswald
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Distribuovala	distribuovat	k5eAaBmAgFnS	distribuovat
ho	on	k3xPp3gNnSc4	on
společnost	společnost	k1gFnSc1	společnost
Winkler	Winkler	k1gMnSc1	Winkler
Pictures	Pictures	k1gMnSc1	Pictures
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Universal	Universal	k1gFnSc2	Universal
Pictures	Picturesa	k1gFnPc2	Picturesa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Oswalda	Oswald	k1gMnSc4	Oswald
si	se	k3xPyFc3	se
bohužel	bohužel	k6eAd1	bohužel
nárokoval	nárokovat	k5eAaImAgMnS	nárokovat
autorská	autorský	k2eAgNnPc4d1	autorské
práva	právo	k1gNnPc1	právo
distributor	distributor	k1gMnSc1	distributor
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
Disney	Disnea	k1gFnPc4	Disnea
vydělal	vydělat	k5eAaPmAgMnS	vydělat
jen	jen	k9	jen
pár	pár	k4xCyI	pár
set	set	k1gInSc4	set
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Disney	Disney	k1gInPc1	Disney
stihl	stihnout	k5eAaPmAgMnS	stihnout
do	do	k7c2	do
února	únor	k1gInSc2	únor
1928	[number]	k4	1928
dokončit	dokončit	k5eAaPmF	dokončit
jen	jen	k9	jen
26	[number]	k4	26
kraťasů	kraťas	k1gInPc2	kraťas
s	s	k7c7	s
Oswaldem	Oswald	k1gInSc7	Oswald
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
mu	on	k3xPp3gMnSc3	on
vypršela	vypršet	k5eAaPmAgFnS	vypršet
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
distribuční	distribuční	k2eAgFnSc1d1	distribuční
společnost	společnost	k1gFnSc1	společnost
převzal	převzít	k5eAaPmAgMnS	převzít
manžel	manžel	k1gMnSc1	manžel
Winklerové	Winklerová	k1gFnSc2	Winklerová
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
Mintz	Mintz	k1gMnSc1	Mintz
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nepodařilo	podařit	k5eNaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c4	nad
Walt	Walt	k1gInSc4	Walt
Disney	Disnea	k1gFnSc2	Disnea
Studio	studio	k1gNnSc4	studio
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
Mintz	Mintz	k1gMnSc1	Mintz
najal	najmout	k5eAaPmAgMnS	najmout
čtyři	čtyři	k4xCgInPc4	čtyři
Disneyho	Disneyha	k1gFnSc5	Disneyha
hlavní	hlavní	k2eAgFnSc7d1	hlavní
animátory	animátor	k1gMnPc4	animátor
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Ub	Ub	k1gMnSc2	Ub
Iwerkse	Iwerks	k1gMnSc2	Iwerks
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
založil	založit	k5eAaPmAgMnS	založit
své	svůj	k3xOyFgNnSc4	svůj
vlastní	vlastní	k2eAgNnSc4d1	vlastní
animační	animační	k2eAgNnSc4d1	animační
studio	studio	k1gNnSc4	studio
a	a	k8xC	a
tvořil	tvořit	k5eAaImAgInS	tvořit
Snappy	Snapp	k1gInPc4	Snapp
Comedies	Comediesa	k1gFnPc2	Comediesa
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
najít	najít	k5eAaPmF	najít
náhradu	náhrada	k1gFnSc4	náhrada
za	za	k7c4	za
ztraceného	ztracený	k2eAgMnSc4d1	ztracený
Oswalda	Oswald	k1gMnSc4	Oswald
<g/>
,	,	kIx,	,
přišel	přijít	k5eAaPmAgMnS	přijít
Disney	Disnea	k1gMnSc2	Disnea
během	během	k7c2	během
cesty	cesta	k1gFnSc2	cesta
vlakem	vlak	k1gInSc7	vlak
mířícím	mířící	k2eAgInSc7d1	mířící
do	do	k7c2	do
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
s	s	k7c7	s
nápadem	nápad	k1gInSc7	nápad
nové	nový	k2eAgFnSc2d1	nová
postavy	postava	k1gFnSc2	postava
-	-	kIx~	-
myši	myš	k1gFnSc2	myš
-	-	kIx~	-
se	se	k3xPyFc4	se
jménem	jméno	k1gNnSc7	jméno
Mortimer	Mortimra	k1gFnPc2	Mortimra
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vlaku	vlak	k1gInSc6	vlak
nakreslil	nakreslit	k5eAaPmAgInS	nakreslit
pár	pár	k1gInSc1	pár
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
náčrtků	náčrtek	k1gInPc2	náčrtek
<g/>
.	.	kIx.	.
</s>
<s>
Myš	myš	k1gFnSc1	myš
později	pozdě	k6eAd2	pozdě
Disney	Disnea	k1gFnPc4	Disnea
přejmenovat	přejmenovat	k5eAaPmF	přejmenovat
na	na	k7c4	na
Mickey	Micke	k2eAgFnPc4d1	Micke
Mouse	Mouse	k1gFnPc4	Mouse
z	z	k7c2	z
iniciativy	iniciativa	k1gFnSc2	iniciativa
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
jméno	jméno	k1gNnSc1	jméno
Mortimer	Mortimer	k1gInSc1	Mortimer
příliš	příliš	k6eAd1	příliš
chladné	chladný	k2eAgFnSc2d1	chladná
a	a	k8xC	a
sedící	sedící	k2eAgFnSc2d1	sedící
spíš	spíš	k9	spíš
na	na	k7c4	na
krysu	krysa	k1gFnSc4	krysa
<g/>
,	,	kIx,	,
než	než	k8xS	než
na	na	k7c4	na
roztomilou	roztomilý	k2eAgFnSc4d1	roztomilá
myš	myš	k1gFnSc4	myš
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
si	se	k3xPyFc3	se
Mickey	Mickey	k1gInPc4	Mickey
zahrál	zahrát	k5eAaPmAgInS	zahrát
v	v	k7c6	v
několika	několik	k4yIc6	několik
filmech	film	k1gInPc6	film
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
produkoval	produkovat	k5eAaImAgInS	produkovat
sám	sám	k3xTgInSc1	sám
Walt	Walt	k1gInSc1	Walt
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Mickeyho	Mickey	k1gMnSc2	Mickey
Klubík	Klubík	k1gInSc4	Klubík
<g/>
.	.	kIx.	.
</s>
<s>
Vůbec	vůbec	k9	vůbec
první	první	k4xOgMnSc1	první
Mickeyho	Mickey	k1gMnSc4	Mickey
vzhled	vzhled	k1gInSc1	vzhled
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
Ub	Ub	k1gFnSc3	Ub
Iwerks	Iwerks	k1gInSc1	Iwerks
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
k	k	k7c3	k
Disneymu	Disneym	k1gInSc3	Disneym
vrátil	vrátit	k5eAaPmAgMnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Disneyho	Disney	k1gMnSc4	Disney
vůbec	vůbec	k9	vůbec
prvním	první	k4xOgInSc6	první
filmu	film	k1gInSc6	film
se	s	k7c7	s
zvukem	zvuk	k1gInSc7	zvuk
<g/>
,	,	kIx,	,
Steamboat	Steamboat	k1gInSc1	Steamboat
Willie	Willie	k1gFnSc2	Willie
<g/>
,	,	kIx,	,
hrál	hrát	k5eAaImAgInS	hrát
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
právě	právě	k6eAd1	právě
Mickey	Micke	k2eAgFnPc1d1	Micke
Mouse	Mouse	k1gFnPc1	Mouse
<g/>
.	.	kIx.	.
</s>
<s>
Uveden	uveden	k2eAgMnSc1d1	uveden
byl	být	k5eAaImAgMnS	být
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1928	[number]	k4	1928
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
distribuční	distribuční	k2eAgFnSc2d1	distribuční
společnosti	společnost	k1gFnSc2	společnost
Pata	pata	k1gFnSc1	pata
Powerse	Powerse	k1gFnSc1	Powerse
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
animované	animovaný	k2eAgInPc1d1	animovaný
filmy	film	k1gInPc1	film
se	s	k7c7	s
zvukem	zvuk	k1gInSc7	zvuk
následovaly	následovat	k5eAaImAgFnP	následovat
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
to	ten	k3xDgNnSc1	ten
Plane	planout	k5eAaImIp3nS	planout
Crazy	Craza	k1gFnPc4	Craza
a	a	k8xC	a
The	The	k1gMnSc1	The
Gallopin	Gallopin	k1gMnSc1	Gallopin
<g/>
'	'	kIx"	'
Gaucho	Gaucha	k1gFnSc5	Gaucha
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
filmu	film	k1gInSc2	film
Steamboat	Steamboat	k1gInSc1	Steamboat
Willie	Willie	k1gFnSc2	Willie
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
okamžitý	okamžitý	k2eAgInSc1d1	okamžitý
hit	hit	k1gInSc1	hit
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
počáteční	počáteční	k2eAgInSc1d1	počáteční
úspěch	úspěch	k1gInSc1	úspěch
byl	být	k5eAaImAgInS	být
přičítán	přičítat	k5eAaImNgInS	přičítat
nejen	nejen	k6eAd1	nejen
Mickeymu	Mickeymum	k1gNnSc3	Mickeymum
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
dokonalé	dokonalý	k2eAgFnSc3d1	dokonalá
postavě	postava	k1gFnSc3	postava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavně	hlavně	k9	hlavně
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
první	první	k4xOgInSc4	první
animovaný	animovaný	k2eAgInSc4d1	animovaný
film	film	k1gInSc4	film
se	s	k7c7	s
zvukem	zvuk	k1gInSc7	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Disney	Disne	k1gMnPc4	Disne
použil	použít	k5eAaPmAgInS	použít
Cinephone	Cinephon	k1gInSc5	Cinephon
system	syst	k1gInSc7	syst
Pata	pata	k1gFnSc1	pata
Powerse	Powerse	k1gFnSc1	Powerse
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
Powers	Powers	k1gInSc4	Powers
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Lee	Lea	k1gFnSc3	Lea
De	De	k?	De
Forest	Forest	k1gInSc1	Forest
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Phonofilm	Phonofilm	k1gInSc1	Phonofilm
system	syst	k1gInSc7	syst
<g/>
.	.	kIx.	.
</s>
<s>
Steamboat	Steamboat	k1gInSc1	Steamboat
Willie	Willie	k1gFnSc2	Willie
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
B.	B.	kA	B.
S.	S.	kA	S.
Moss	Mossa	k1gFnPc2	Mossa
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Colony	colon	k1gNnPc7	colon
v	v	k7c6	v
New	New	k1gFnSc6	New
York	York	k1gInSc1	York
City	City	k1gFnPc2	City
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
divadlo	divadlo	k1gNnSc4	divadlo
Broadway	Broadwaa	k1gFnSc2	Broadwaa
<g/>
.	.	kIx.	.
</s>
<s>
Plane	planout	k5eAaImIp3nS	planout
Crazy	Craza	k1gFnPc4	Craza
a	a	k8xC	a
The	The	k1gMnPc4	The
Galloping	Galloping	k1gInSc4	Galloping
Gaucho	Gaucha	k1gFnSc5	Gaucha
byly	být	k5eAaImAgFnP	být
znovu-uvedeny	znovuveden	k2eAgFnPc1d1	znovu-uveden
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
se	s	k7c7	s
synchronizovanými	synchronizovaný	k2eAgFnPc7d1	synchronizovaná
stopami	stopa	k1gFnPc7	stopa
<g/>
.	.	kIx.	.
</s>
<s>
Disney	Disney	k1gInPc4	Disney
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
s	s	k7c7	s
produkováním	produkování	k1gNnSc7	produkování
animovaných	animovaný	k2eAgInPc2d1	animovaný
filmů	film	k1gInPc2	film
s	s	k7c7	s
Mickey	Micke	k1gMnPc7	Micke
Mousem	Mous	k1gMnSc7	Mous
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
postavičkami	postavička	k1gFnPc7	postavička
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
zahájil	zahájit	k5eAaPmAgMnS	zahájit
tvorbu	tvorba	k1gFnSc4	tvorba
série	série	k1gFnSc2	série
Silly	Silla	k1gFnSc2	Silla
Symphonies	Symphoniesa	k1gFnPc2	Symphoniesa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
distribuovala	distribuovat	k5eAaBmAgFnS	distribuovat
společnost	společnost	k1gFnSc1	společnost
Columbia	Columbia	k1gFnSc1	Columbia
Pictures	Pictures	k1gInSc4	Pictures
<g/>
.	.	kIx.	.
</s>
<s>
Distributorem	distributor	k1gMnSc7	distributor
tohoto	tento	k3xDgInSc2	tento
pořadu	pořad	k1gInSc2	pořad
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
podpisem	podpis	k1gInSc7	podpis
smlouvy	smlouva	k1gFnSc2	smlouva
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1929	[number]	k4	1929
přišel	přijít	k5eAaPmAgMnS	přijít
divadelní	divadelní	k2eAgMnSc1d1	divadelní
manažer	manažer	k1gMnSc1	manažer
Harry	Harra	k1gFnSc2	Harra
Woodin	Woodina	k1gFnPc2	Woodina
s	s	k7c7	s
požadavkem	požadavek	k1gInSc7	požadavek
na	na	k7c4	na
založení	založení	k1gNnSc4	založení
klubu	klub	k1gInSc2	klub
Mickeyho	Mickey	k1gMnSc2	Mickey
Mouse	Mous	k1gMnSc2	Mous
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
Walt	Walt	k1gInSc4	Walt
schválil	schválit	k5eAaPmAgInS	schválit
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
reorganizaci	reorganizace	k1gFnSc3	reorganizace
Disneyho	Disney	k1gMnSc2	Disney
podniku	podnik	k1gInSc2	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Walt	Walta	k1gFnPc2	Walta
Disney	Disnea	k1gFnSc2	Disnea
Studio	studio	k1gNnSc1	studio
bylo	být	k5eAaImAgNnS	být
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
Walt	Walt	k1gInSc4	Walt
Disney	Disnea	k1gFnSc2	Disnea
Productions	Productionsa	k1gFnPc2	Productionsa
<g/>
,	,	kIx,	,
Limited	limited	k2eAgFnPc2d1	limited
(	(	kIx(	(
<g/>
s	s	k7c7	s
ručením	ručení	k1gNnSc7	ručení
omezeným	omezený	k2eAgNnSc7d1	omezené
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pod	pod	k7c4	pod
které	který	k3yQgFnPc4	který
spadala	spadat	k5eAaPmAgFnS	spadat
divize	divize	k1gFnSc1	divize
Walt	Walt	k1gInSc4	Walt
Disney	Disnea	k1gFnSc2	Disnea
Enterprises	Enterprisesa	k1gFnPc2	Enterprisesa
sloužící	sloužící	k2eAgMnSc1d1	sloužící
k	k	k7c3	k
prodeji	prodej	k1gInSc3	prodej
a	a	k8xC	a
správě	správa	k1gFnSc3	správa
licencí	licence	k1gFnPc2	licence
k	k	k7c3	k
reklamním	reklamní	k2eAgInPc3d1	reklamní
předmětům	předmět	k1gInPc3	předmět
<g/>
,	,	kIx,	,
a	a	k8xC	a
dvě	dva	k4xCgFnPc1	dva
pobočky	pobočka	k1gFnPc1	pobočka
<g/>
,	,	kIx,	,
Disney	Disnea	k1gFnPc1	Disnea
Film	film	k1gInSc1	film
Recording	Recording	k1gInSc4	Recording
Company	Compana	k1gFnSc2	Compana
<g/>
,	,	kIx,	,
Limited	limited	k2eAgFnSc2d1	limited
a	a	k8xC	a
Liled	Liled	k1gInSc1	Liled
Realty	Realt	k1gInPc1	Realt
and	and	k?	and
Investment	Investment	k1gInSc4	Investment
Company	Compana	k1gFnSc2	Compana
pro	pro	k7c4	pro
správu	správa	k1gFnSc4	správa
realit	realita	k1gFnPc2	realita
<g/>
.	.	kIx.	.
</s>
<s>
Walt	Walt	k1gInSc1	Walt
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
vlastnili	vlastnit	k5eAaImAgMnP	vlastnit
60	[number]	k4	60
%	%	kIx~	%
(	(	kIx(	(
<g/>
6,000	[number]	k4	6,000
akcií	akcie	k1gFnPc2	akcie
<g/>
)	)	kIx)	)
a	a	k8xC	a
Roy	Roy	k1gMnSc1	Roy
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
40	[number]	k4	40
%	%	kIx~	%
společnosti	společnost	k1gFnSc2	společnost
Walt	Walt	k1gInSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
Productions	Productionsa	k1gFnPc2	Productionsa
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
začal	začít	k5eAaPmAgMnS	začít
ve	v	k7c6	v
vůbec	vůbec	k9	vůbec
prvních	první	k4xOgFnPc6	první
novinách	novina	k1gFnPc6	novina
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
Mirror	Mirror	k1gInSc1	Mirror
s	s	k7c7	s
Waltovým	Waltův	k2eAgNnSc7d1	Waltův
povolením	povolení	k1gNnSc7	povolení
vycházet	vycházet	k5eAaImF	vycházet
komiks	komiks	k1gInSc4	komiks
s	s	k7c7	s
Mickey	Mickey	k1gInPc4	Mickey
Mousem	Mouso	k1gNnSc7	Mouso
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
podepsal	podepsat	k5eAaPmAgInS	podepsat
Disney	Disnea	k1gFnSc2	Disnea
exkluzivní	exkluzivní	k2eAgInSc1d1	exkluzivní
smluvní	smluvní	k2eAgInSc1d1	smluvní
kontrakt	kontrakt	k1gInSc1	kontrakt
s	s	k7c7	s
firmou	firma	k1gFnSc7	firma
Technicolor	Technicolor	k1gMnSc1	Technicolor
(	(	kIx(	(
<g/>
platil	platit	k5eAaImAgInS	platit
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
barevných	barevný	k2eAgInPc2d1	barevný
animovaných	animovaný	k2eAgInPc2d1	animovaný
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Vůbec	vůbec	k9	vůbec
prvním	první	k4xOgInSc7	první
barevným	barevný	k2eAgInSc7d1	barevný
počinem	počin	k1gInSc7	počin
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
Flowers	Flowers	k1gInSc4	Flowers
and	and	k?	and
Trees	Trees	k1gInSc1	Trees
ze	z	k7c2	z
Silly	Silla	k1gFnSc2	Silla
Symphonies	Symphonies	k1gInSc1	Symphonies
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Plně	plně	k6eAd1	plně
rozhodnut	rozhodnut	k2eAgMnSc1d1	rozhodnut
posunout	posunout	k5eAaPmF	posunout
hranice	hranice	k1gFnPc4	hranice
animace	animace	k1gFnPc4	animace
zase	zase	k9	zase
o	o	k7c4	o
něco	něco	k3yInSc4	něco
dál	daleko	k6eAd2	daleko
začal	začít	k5eAaPmAgMnS	začít
Disney	Disnea	k1gFnPc4	Disnea
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
prvním	první	k4xOgInSc6	první
celovečerním	celovečerní	k2eAgInSc6d1	celovečerní
filmu	film	k1gInSc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
práce	práce	k1gFnSc1	práce
spatřila	spatřit	k5eAaPmAgFnS	spatřit
světlo	světlo	k1gNnSc4	světlo
světa	svět	k1gInSc2	svět
v	v	k7c6	v
premiéře	premiéra	k1gFnSc6	premiéra
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1937	[number]	k4	1937
Sněhurka	Sněhurka	k1gFnSc1	Sněhurka
a	a	k8xC	a
sedm	sedm	k4xCc1	sedm
trpaslíků	trpaslík	k1gMnPc2	trpaslík
<g/>
,	,	kIx,	,
toho	ten	k3xDgInSc2	ten
času	čas	k1gInSc2	čas
nejvýdělečnější	výdělečný	k2eAgInSc1d3	nejvýdělečnější
film	film	k1gInSc1	film
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Sněhurku	Sněhurka	k1gFnSc4	Sněhurka
distribuovala	distribuovat	k5eAaBmAgFnS	distribuovat
společnost	společnost	k1gFnSc1	společnost
RKO	RKO	kA	RKO
Radio	radio	k1gNnSc1	radio
Pictures	Pictures	k1gInSc1	Pictures
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
si	se	k3xPyFc3	se
distribuci	distribuce	k1gFnSc6	distribuce
Disneyho	Disney	k1gMnSc2	Disney
filmu	film	k1gInSc2	film
vzala	vzít	k5eAaPmAgFnS	vzít
na	na	k7c4	na
starosti	starost	k1gFnPc4	starost
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
United	United	k1gMnSc1	United
Artists	Artistsa	k1gFnPc2	Artistsa
pokoušely	pokoušet	k5eAaImAgFnP	pokoušet
získat	získat	k5eAaPmF	získat
televizní	televizní	k2eAgNnPc4d1	televizní
práva	právo	k1gNnPc4	právo
na	na	k7c4	na
Disneyho	Disney	k1gMnSc4	Disney
budoucí	budoucí	k2eAgFnSc1d1	budoucí
kraťasy	kraťas	k1gInPc7	kraťas
<g/>
.	.	kIx.	.
</s>
<s>
Výdělkem	výdělek	k1gInSc7	výdělek
ze	z	k7c2	z
Sněhurky	Sněhurka	k1gFnSc2	Sněhurka
Disney	Disnea	k1gFnSc2	Disnea
financoval	financovat	k5eAaBmAgInS	financovat
stavbu	stavba	k1gFnSc4	stavba
nového	nový	k2eAgInSc2d1	nový
komplexu	komplex	k1gInSc2	komplex
filmových	filmový	k2eAgNnPc2d1	filmové
studií	studio	k1gNnPc2	studio
v	v	k7c4	v
Burbanku	Burbanka	k1gFnSc4	Burbanka
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
,	,	kIx,	,
velký	velký	k2eAgInSc1d1	velký
rovných	rovný	k2eAgInPc2d1	rovný
210	[number]	k4	210
000	[number]	k4	000
m2	m2	k4	m2
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgMnSc1d1	Nové
Walt	Walt	k1gMnSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
Studios	Studios	k?	Studios
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
sídlem	sídlo	k1gNnSc7	sídlo
společnosti	společnost	k1gFnSc2	společnost
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
dokončeny	dokončit	k5eAaPmNgFnP	dokončit
a	a	k8xC	a
otevřeny	otevřít	k5eAaPmNgFnP	otevřít
k	k	k7c3	k
podnikání	podnikání	k1gNnSc3	podnikání
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
měla	mít	k5eAaImAgFnS	mít
Disneyho	Disney	k1gMnSc4	Disney
společnost	společnost	k1gFnSc1	společnost
Walt	Walt	k1gInSc4	Walt
Disney	Disnea	k1gFnSc2	Disnea
Pictures	Pictures	k1gInSc4	Pictures
první	první	k4xOgFnSc4	první
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
nabídku	nabídka	k1gFnSc4	nabídka
akcií	akcie	k1gFnPc2	akcie
a	a	k8xC	a
slavnostně	slavnostně	k6eAd1	slavnostně
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
na	na	k7c4	na
burzu	burza	k1gFnSc4	burza
<g/>
.	.	kIx.	.
</s>
<s>
Studio	studio	k1gNnSc1	studio
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
s	s	k7c7	s
tvorbou	tvorba	k1gFnSc7	tvorba
animovaných	animovaný	k2eAgInPc2d1	animovaný
krátkých	krátký	k2eAgInPc2d1	krátký
filmů	film	k1gInPc2	film
a	a	k8xC	a
celovečerních	celovečerní	k2eAgInPc2d1	celovečerní
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Pinocchio	Pinocchio	k6eAd1	Pinocchio
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Fantasia	Fantasia	k1gFnSc1	Fantasia
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dumbo	Dumba	k1gMnSc5	Dumba
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
a	a	k8xC	a
Bambi	Bambe	k1gFnSc4	Bambe
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
začátkem	začátek	k1gInSc7	začátek
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
zisky	zisk	k1gInPc4	zisk
poklesly	poklesnout	k5eAaPmAgFnP	poklesnout
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
vstoupily	vstoupit	k5eAaPmAgInP	vstoupit
do	do	k7c2	do
války	válka	k1gFnSc2	válka
po	po	k7c6	po
japonském	japonský	k2eAgInSc6d1	japonský
útoku	útok	k1gInSc6	útok
na	na	k7c4	na
americkou	americký	k2eAgFnSc4d1	americká
základnu	základna	k1gFnSc4	základna
Pearl	Pearl	k1gMnSc1	Pearl
Harbor	Harbor	k1gMnSc1	Harbor
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
mnoho	mnoho	k6eAd1	mnoho
z	z	k7c2	z
Disneyho	Disney	k1gMnSc2	Disney
animátorů	animátor	k1gMnPc2	animátor
povoláno	povolat	k5eAaPmNgNnS	povolat
do	do	k7c2	do
Armády	armáda	k1gFnSc2	armáda
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
a	a	k8xC	a
kanadská	kanadský	k2eAgFnSc1d1	kanadská
vláda	vláda	k1gFnSc1	vláda
pověřila	pověřit	k5eAaPmAgFnS	pověřit
studio	studio	k1gNnSc4	studio
výrobou	výroba	k1gFnSc7	výroba
školících	školící	k2eAgInPc2d1	školící
a	a	k8xC	a
propagandistických	propagandistický	k2eAgInPc2d1	propagandistický
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
zhruba	zhruba	k6eAd1	zhruba
90	[number]	k4	90
%	%	kIx~	%
z	z	k7c2	z
550	[number]	k4	550
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
pracovalo	pracovat	k5eAaImAgNnS	pracovat
na	na	k7c6	na
projektech	projekt	k1gInPc6	projekt
s	s	k7c7	s
válečnou	válečný	k2eAgFnSc7d1	válečná
tematikou	tematika	k1gFnSc7	tematika
<g/>
.	.	kIx.	.
</s>
<s>
Filmy	film	k1gInPc4	film
jako	jako	k8xS	jako
Victory	Victor	k1gMnPc4	Victor
Through	Through	k1gMnSc1	Through
Air	Air	k1gMnSc1	Air
Power	Power	k1gMnSc1	Power
a	a	k8xC	a
krátký	krátký	k2eAgInSc4d1	krátký
film	film	k1gInSc4	film
Education	Education	k1gInSc4	Education
for	forum	k1gNnPc2	forum
Death	Death	k1gInSc1	Death
(	(	kIx(	(
<g/>
oba	dva	k4xCgMnPc1	dva
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
měly	mít	k5eAaImAgInP	mít
zvýšit	zvýšit	k5eAaPmF	zvýšit
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
podporu	podpora	k1gFnSc4	podpora
válečného	válečný	k2eAgNnSc2d1	válečné
úsilí	úsilí	k1gNnSc2	úsilí
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
i	i	k9	i
hlavní	hlavní	k2eAgFnPc4d1	hlavní
postavy	postava	k1gFnPc4	postava
studia	studio	k1gNnSc2	studio
se	se	k3xPyFc4	se
zapojily	zapojit	k5eAaPmAgFnP	zapojit
do	do	k7c2	do
těchto	tento	k3xDgInPc2	tento
válečných	válečný	k2eAgInPc2d1	válečný
animovaných	animovaný	k2eAgInPc2d1	animovaný
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
kačer	kačer	k1gMnSc1	kačer
Donald	Donald	k1gMnSc1	Donald
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
komických	komický	k2eAgInPc6d1	komický
propagandistických	propagandistický	k2eAgInPc6d1	propagandistický
krátkých	krátký	k2eAgInPc6d1	krátký
filmech	film	k1gInPc6	film
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
Der	drát	k5eAaImRp2nS	drát
Fuehrer	Fuehrer	k1gInSc1	Fuehrer
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Face	Face	k1gFnSc7	Face
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
omezeným	omezený	k2eAgInSc7d1	omezený
počtem	počet	k1gInSc7	počet
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
a	a	k8xC	a
malým	malý	k2eAgInSc7d1	malý
kapitálem	kapitál	k1gInSc7	kapitál
během	běh	k1gInSc7	běh
a	a	k8xC	a
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
Disney	Disney	k1gInPc4	Disney
produkoval	produkovat	k5eAaImAgMnS	produkovat
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
balíčkové	balíčkový	k2eAgInPc1d1	balíčkový
filmy	film	k1gInPc1	film
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
kolekce	kolekce	k1gFnSc1	kolekce
krátkých	krátký	k2eAgInPc2d1	krátký
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
The	The	k1gFnSc4	The
Three	Three	k1gNnSc2	Three
Caballeros	Caballerosa	k1gFnPc2	Caballerosa
(	(	kIx(	(
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
a	a	k8xC	a
Melody	Melod	k1gInPc1	Melod
Time	Tim	k1gFnSc2	Tim
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
<g/>
,	,	kIx,	,
co	co	k8xS	co
se	se	k3xPyFc4	se
výdělku	výdělek	k1gInSc2	výdělek
týče	týkat	k5eAaImIp3nS	týkat
<g/>
,	,	kIx,	,
úplně	úplně	k6eAd1	úplně
propadly	propadnout	k5eAaPmAgInP	propadnout
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
začalo	začít	k5eAaPmAgNnS	začít
studio	studio	k1gNnSc1	studio
vyrábět	vyrábět	k5eAaImF	vyrábět
hrané	hraný	k2eAgInPc4d1	hraný
filmy	film	k1gInPc4	film
a	a	k8xC	a
dokumenty	dokument	k1gInPc4	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Song	song	k1gInSc1	song
of	of	k?	of
the	the	k?	the
South	South	k1gInSc1	South
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
a	a	k8xC	a
So	So	kA	So
Dear	Dear	k1gInSc1	Dear
to	ten	k3xDgNnSc1	ten
My	my	k3xPp1nPc1	my
Heart	Hearta	k1gFnPc2	Hearta
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
populární	populární	k2eAgInPc1d1	populární
animované	animovaný	k2eAgInPc1d1	animovaný
filmy	film	k1gInPc1	film
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
hraná	hraný	k2eAgFnSc1d1	hraná
série	série	k1gFnSc1	série
True-Life	True-Lif	k1gInSc5	True-Lif
Adventures	Adventuresa	k1gFnPc2	Adventuresa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
filmy	film	k1gInPc4	film
jako	jako	k8xS	jako
Seal	Seal	k1gInSc4	Seal
Island	Island	k1gInSc1	Island
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
a	a	k8xC	a
The	The	k1gFnSc1	The
Vanishing	Vanishing	k1gInSc1	Vanishing
Prairie	Prairie	k1gFnSc1	Prairie
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
u	u	k7c2	u
publika	publikum	k1gNnSc2	publikum
také	také	k9	také
staly	stát	k5eAaPmAgFnP	stát
oblíbenými	oblíbený	k2eAgNnPc7d1	oblíbené
kousky	kousek	k1gInPc4	kousek
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Premiéra	premiéra	k1gFnSc1	premiéra
filmu	film	k1gInSc2	film
Popelka	Popelka	k1gFnSc1	Popelka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
dokázala	dokázat	k5eAaPmAgFnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
je	být	k5eAaImIp3nS	být
po	po	k7c4	po
celovečerní	celovečerní	k2eAgInPc4d1	celovečerní
animované	animovaný	k2eAgInPc4d1	animovaný
snímky	snímek	k1gInPc4	snímek
stále	stále	k6eAd1	stále
zájem	zájem	k1gInSc4	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
premiéry	premiéra	k1gFnPc1	premiéra
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
byly	být	k5eAaImAgFnP	být
například	například	k6eAd1	například
Alenka	Alenka	k1gFnSc1	Alenka
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
divů	div	k1gInPc2	div
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
a	a	k8xC	a
Petr	Petr	k1gMnSc1	Petr
Pan	Pan	k1gMnSc1	Pan
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oba	dva	k4xCgInPc1	dva
vyrobeny	vyroben	k2eAgInPc1d1	vyroben
už	už	k6eAd1	už
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
a	a	k8xC	a
Disneyho	Disney	k1gMnSc4	Disney
první	první	k4xOgInSc4	první
hraný	hraný	k2eAgInSc4d1	hraný
celovečerní	celovečerní	k2eAgInSc4d1	celovečerní
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
Treasure	Treasur	k1gMnSc5	Treasur
Island	Island	k1gInSc1	Island
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k1gNnSc4	další
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
hraných	hraný	k2eAgInPc2d1	hraný
Disneyho	Disney	k1gMnSc2	Disney
filmů	film	k1gInPc2	film
patří	patřit	k5eAaImIp3nS	patřit
The	The	k1gMnSc1	The
Story	story	k1gFnSc2	story
of	of	k?	of
Robin	robin	k2eAgInSc1d1	robin
Hood	Hood	k1gInSc1	Hood
a	a	k8xC	a
His	his	k1gNnSc1	his
Merrie	Merrie	k1gFnSc2	Merrie
Men	Men	k1gFnSc2	Men
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Sword	Sword	k1gMnSc1	Sword
and	and	k?	and
the	the	k?	the
Rose	Rose	k1gMnSc1	Rose
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
20,000	[number]	k4	20,000
Leagues	Leagues	k1gMnSc1	Leagues
Under	Under	k1gMnSc1	Under
the	the	k?	the
Sea	Sea	k1gMnSc1	Sea
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Disney	Disnea	k1gFnPc1	Disnea
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
ukončil	ukončit	k5eAaPmAgMnS	ukončit
smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c6	o
distribuci	distribuce	k1gFnSc6	distribuce
s	s	k7c7	s
RKO	RKO	kA	RKO
a	a	k8xC	a
založil	založit	k5eAaPmAgInS	založit
svoji	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
distribuční	distribuční	k2eAgFnSc4d1	distribuční
společnost	společnost	k1gFnSc4	společnost
Buena	Buen	k1gInSc2	Buen
Vista	vista	k2eAgInSc1d1	vista
Distribution	Distribution	k1gInSc1	Distribution
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1950	[number]	k4	1950
se	se	k3xPyFc4	se
společnosti	společnost	k1gFnSc2	společnost
Walt	Walta	k1gFnPc2	Walta
Disney	Disnea	k1gFnSc2	Disnea
Productions	Productions	k1gInSc1	Productions
a	a	k8xC	a
The	The	k1gFnSc1	The
Coca-Cola	cocaola	k1gFnSc1	coca-cola
Company	Compana	k1gFnSc2	Compana
spojily	spojit	k5eAaPmAgFnP	spojit
a	a	k8xC	a
společně	společně	k6eAd1	společně
podnikly	podniknout	k5eAaPmAgFnP	podniknout
Disneyho	Disneyha	k1gFnSc5	Disneyha
první	první	k4xOgInSc4	první
"	"	kIx"	"
<g/>
výlet	výlet	k1gInSc4	výlet
<g/>
"	"	kIx"	"
do	do	k7c2	do
televize	televize	k1gFnSc2	televize
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
speciálu	speciál	k1gInSc2	speciál
An	An	k1gMnSc1	An
Hour	Hour	k1gMnSc1	Hour
in	in	k?	in
Wonderland	Wonderland	k1gInSc1	Wonderland
(	(	kIx(	(
<g/>
Hodina	hodina	k1gFnSc1	hodina
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
divů	div	k1gInPc2	div
<g/>
)	)	kIx)	)
na	na	k7c6	na
NBC	NBC	kA	NBC
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1954	[number]	k4	1954
měl	mít	k5eAaImAgMnS	mít
na	na	k7c6	na
televizi	televize	k1gFnSc6	televize
ABC	ABC	kA	ABC
premiéru	premiéra	k1gFnSc4	premiéra
Disneyho	Disneyha	k1gFnSc5	Disneyha
první	první	k4xOgMnSc1	první
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
Disneyland	Disneyland	k1gInSc1	Disneyland
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
počítá	počítat	k5eAaImIp3nS	počítat
mezi	mezi	k7c4	mezi
nejdelší	dlouhý	k2eAgInPc4d3	nejdelší
pořady	pořad	k1gInPc4	pořad
vysílané	vysílaný	k2eAgInPc4d1	vysílaný
v	v	k7c6	v
prime	prim	k1gInSc5	prim
timu	timus	k1gInSc2	timus
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Disneyland	Disneyland	k1gInSc1	Disneyland
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Disneyho	Disney	k1gMnSc4	Disney
platformou	platforma	k1gFnSc7	platforma
pro	pro	k7c4	pro
vysílání	vysílání	k1gNnSc4	vysílání
nových	nový	k2eAgInPc2d1	nový
i	i	k8xC	i
starších	starý	k2eAgInPc2d2	starší
výtvorů	výtvor	k1gInPc2	výtvor
<g/>
,	,	kIx,	,
a	a	k8xC	a
ABC	ABC	kA	ABC
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
Disneyho	Disneyha	k1gFnSc5	Disneyha
partnerem	partner	k1gMnSc7	partner
při	při	k7c6	při
financování	financování	k1gNnSc6	financování
a	a	k8xC	a
přípravě	příprava	k1gFnSc6	příprava
Disneyho	Disney	k1gMnSc2	Disney
nového	nový	k2eAgInSc2d1	nový
riskantního	riskantní	k2eAgInSc2d1	riskantní
podniku	podnik	k1gInSc2	podnik
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
nacházet	nacházet	k5eAaImF	nacházet
uprostřed	uprostřed	k7c2	uprostřed
oranžového	oranžový	k2eAgInSc2d1	oranžový
hájku	hájek	k1gInSc2	hájek
poblíž	poblíž	k7c2	poblíž
města	město	k1gNnSc2	město
Anaheim	Anaheima	k1gFnPc2	Anaheima
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
firemního	firemní	k2eAgInSc2d1	firemní
vztahu	vztah	k1gInSc2	vztah
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
nemohl	moct	k5eNaImAgMnS	moct
toho	ten	k3xDgInSc2	ten
času	čas	k1gInSc2	čas
nikdo	nikdo	k3yNnSc1	nikdo
předpovídat	předpovídat	k5eAaImF	předpovídat
<g/>
,	,	kIx,	,
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
Disneyho	Disneyha	k1gFnSc5	Disneyha
akvizici	akvizice	k1gFnSc4	akvizice
mediální	mediální	k2eAgFnSc2d1	mediální
sítě	síť	k1gFnSc2	síť
ABC	ABC	kA	ABC
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
o	o	k7c4	o
čtyři	čtyři	k4xCgNnPc4	čtyři
desetiletí	desetiletí	k1gNnPc4	desetiletí
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
používal	používat	k5eAaImAgMnS	používat
Walt	Walt	k1gMnSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
seriál	seriál	k1gInSc1	seriál
Disneyland	Disneyland	k1gInSc1	Disneyland
k	k	k7c3	k
ukázkám	ukázka	k1gFnPc3	ukázka
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
ten	ten	k3xDgInSc4	ten
pravý	pravý	k2eAgInSc4d1	pravý
Disneyland	Disneyland	k1gInSc4	Disneyland
vypadat	vypadat	k5eAaPmF	vypadat
<g/>
.	.	kIx.	.
</s>
<s>
Toužil	toužit	k5eAaImAgMnS	toužit
vytvořit	vytvořit	k5eAaPmF	vytvořit
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
bavili	bavit	k5eAaImAgMnP	bavit
rodiče	rodič	k1gMnPc1	rodič
i	i	k8xC	i
děti	dítě	k1gFnPc1	dítě
zároveň	zároveň	k6eAd1	zároveň
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1955	[number]	k4	1955
Walt	Walt	k1gInSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
otevřel	otevřít	k5eAaPmAgInS	otevřít
Disneyland	Disneyland	k1gInSc1	Disneyland
široké	široký	k2eAgFnSc2d1	široká
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc4	den
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
z	z	k7c2	z
Disneylandu	Disneyland	k1gInSc2	Disneyland
vysílalo	vysílat	k5eAaImAgNnS	vysílat
živě	živě	k6eAd1	živě
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nalákali	nalákat	k5eAaPmAgMnP	nalákat
co	co	k9	co
nejvíce	hodně	k6eAd3	hodně
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Pořad	pořad	k1gInSc4	pořad
moderovali	moderovat	k5eAaBmAgMnP	moderovat
Art	Art	k1gMnSc1	Art
Linkletter	Linkletter	k1gMnSc1	Linkletter
a	a	k8xC	a
Ronald	Ronald	k1gMnSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
nejistý	jistý	k2eNgInSc4d1	nejistý
začátek	začátek	k1gInSc4	začátek
začal	začít	k5eAaPmAgInS	začít
Disneyland	Disneyland	k1gInSc1	Disneyland
růst	růst	k1gInSc1	růst
a	a	k8xC	a
lákal	lákat	k5eAaImAgInS	lákat
návštěvníky	návštěvník	k1gMnPc4	návštěvník
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
země	zem	k1gFnSc2	zem
a	a	k8xC	a
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
rozšíření	rozšíření	k1gNnSc1	rozšíření
parku	park	k1gInSc2	park
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
také	také	k6eAd1	také
první	první	k4xOgFnSc4	první
americkou	americký	k2eAgFnSc4d1	americká
nadzemní	nadzemní	k2eAgFnSc4d1	nadzemní
jednokolejovou	jednokolejový	k2eAgFnSc4d1	jednokolejový
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
světový	světový	k2eAgInSc4d1	světový
veletrh	veletrh	k1gInSc4	veletrh
New	New	k1gMnPc2	New
York	York	k1gInSc1	York
World	Worlda	k1gFnPc2	Worlda
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Fair	fair	k6eAd1	fair
1964	[number]	k4	1964
si	se	k3xPyFc3	se
Disney	Disney	k1gInPc4	Disney
připravil	připravit	k5eAaPmAgInS	připravit
čtyři	čtyři	k4xCgFnPc4	čtyři
samostatné	samostatný	k2eAgFnPc4d1	samostatná
stánky	stánka	k1gFnPc4	stánka
s	s	k7c7	s
atrakcí	atrakce	k1gFnSc7	atrakce
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
by	by	kYmCp3nS	by
každý	každý	k3xTgMnSc1	každý
dokázal	dokázat	k5eAaPmAgMnS	dokázat
přilákat	přilákat	k5eAaPmF	přilákat
sponzory	sponzor	k1gMnPc4	sponzor
do	do	k7c2	do
Disneylandu	Disneyland	k1gInSc2	Disneyland
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
Walt	Walt	k1gInSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
také	také	k9	také
hledal	hledat	k5eAaImAgInS	hledat
příhodné	příhodný	k2eAgNnSc4d1	příhodné
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
druhý	druhý	k4xOgInSc4	druhý
zábavní	zábavní	k2eAgInSc4d1	zábavní
park	park	k1gInSc4	park
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1965	[number]	k4	1965
bylo	být	k5eAaImAgNnS	být
ohlášen	ohlášen	k2eAgMnSc1d1	ohlášen
"	"	kIx"	"
<g/>
Disney	Disney	k1gInPc1	Disney
World	Worlda	k1gFnPc2	Worlda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc4	místo
se	s	k7c7	s
zábavními	zábavní	k2eAgInPc7d1	zábavní
parky	park	k1gInPc7	park
<g/>
,	,	kIx,	,
hotely	hotel	k1gInPc7	hotel
a	a	k8xC	a
modelovým	modelový	k2eAgNnSc7d1	modelové
městem	město	k1gNnSc7	město
na	na	k7c6	na
tisících	tisící	k4xOgInPc6	tisící
akrech	akr	k1gInPc6	akr
zakoupených	zakoupený	k2eAgInPc6d1	zakoupený
za	za	k7c7	za
městem	město	k1gNnSc7	město
Orlando	Orlanda	k1gFnSc5	Orlanda
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
<g/>
.	.	kIx.	.
</s>
<s>
Disney	Disnea	k1gFnPc4	Disnea
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
stále	stále	k6eAd1	stále
zaměřený	zaměřený	k2eAgInSc4d1	zaměřený
i	i	k9	i
na	na	k7c4	na
své	svůj	k3xOyFgInPc4	svůj
televizní	televizní	k2eAgInPc4d1	televizní
talenty	talent	k1gInPc4	talent
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
všední	všední	k2eAgInSc1d1	všední
den	den	k1gInSc1	den
se	se	k3xPyFc4	se
vysílal	vysílat	k5eAaImAgInS	vysílat
televizní	televizní	k2eAgInSc1d1	televizní
pořad	pořad	k1gInSc1	pořad
The	The	k1gMnSc2	The
Mickey	Mickea	k1gMnSc2	Mickea
Mouse	Mous	k1gMnSc2	Mous
Club	club	k1gInSc4	club
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hráli	hrát	k5eAaImAgMnP	hrát
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
"	"	kIx"	"
<g/>
Mouseketeers	Mouseketeers	k1gInSc4	Mouseketeers
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Myšketýři	Myšketýř	k1gMnPc1	Myšketýř
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
premiéře	premiéra	k1gFnSc6	premiéra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
mini-série	miniérie	k1gFnSc2	mini-série
Davy	Dav	k1gInPc1	Dav
Crokett	Croketta	k1gFnPc2	Croketta
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
s	s	k7c7	s
Fessem	Fess	k1gMnSc7	Fess
Parkerem	Parker	k1gMnSc7	Parker
<g/>
,	,	kIx,	,
vysílaná	vysílaný	k2eAgNnPc1d1	vysílané
v	v	k7c6	v
pásmu	pásmo	k1gNnSc6	pásmo
pořadů	pořad	k1gInPc2	pořad
Disneyland	Disneyland	k1gInSc1	Disneyland
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
nabíral	nabírat	k5eAaImAgInS	nabírat
na	na	k7c6	na
popularitě	popularita	k1gFnSc6	popularita
seriál	seriál	k1gInSc4	seriál
Zorro	Zorro	k1gNnSc1	Zorro
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ABC	ABC	kA	ABC
se	se	k3xPyFc4	se
odvysílaly	odvysílat	k5eAaPmAgInP	odvysílat
dvě	dva	k4xCgFnPc4	dva
série	série	k1gFnPc4	série
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
dvou	dva	k4xCgFnPc2	dva
epizod	epizoda	k1gFnPc2	epizoda
v	v	k7c6	v
bloku	blok	k1gInSc6	blok
Disneyland	Disneyland	k1gInSc1	Disneyland
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
takovém	takový	k3xDgInSc6	takový
úspěchu	úspěch	k1gInSc6	úspěch
Walt	Walt	k1gMnSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
Productions	Productions	k1gInSc1	Productions
přidali	přidat	k5eAaPmAgMnP	přidat
investice	investice	k1gFnPc4	investice
do	do	k7c2	do
televizní	televizní	k2eAgFnSc2d1	televizní
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Počítalo	počítat	k5eAaImAgNnS	počítat
se	se	k3xPyFc4	se
s	s	k7c7	s
dalším	další	k2eAgInSc7d1	další
dlouhotrvajícím	dlouhotrvající	k2eAgInSc7d1	dlouhotrvající
blokem	blok	k1gInSc7	blok
pořadů	pořad	k1gInPc2	pořad
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
The	The	k1gMnSc1	The
Wonderful	Wonderfula	k1gFnPc2	Wonderfula
World	World	k1gMnSc1	World
of	of	k?	of
Disney	Disnea	k1gFnSc2	Disnea
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
zůstávaly	zůstávat	k5eAaImAgInP	zůstávat
zaneprázdněna	zaneprázdněn	k2eAgFnSc1d1	zaneprázdněna
i	i	k8xC	i
Disneyho	Disneyha	k1gFnSc5	Disneyha
filmová	filmový	k2eAgNnPc1d1	filmové
studia	studio	k1gNnPc1	studio
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
produkovala	produkovat	k5eAaImAgNnP	produkovat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
pět	pět	k4xCc1	pět
až	až	k8xS	až
šest	šest	k4xCc1	šest
filmů	film	k1gInPc2	film
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
krátkých	krátký	k2eAgInPc2d1	krátký
filmů	film	k1gInPc2	film
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1950	[number]	k4	1950
<g/>
-	-	kIx~	-
<g/>
1960	[number]	k4	1960
výrazně	výrazně	k6eAd1	výrazně
ubylo	ubýt	k5eAaPmAgNnS	ubýt
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc4	studio
vydalo	vydat	k5eAaPmAgNnS	vydat
několik	několik	k4yIc1	několik
populárních	populární	k2eAgInPc2d1	populární
celovečerních	celovečerní	k2eAgInPc2d1	celovečerní
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Lady	lady	k1gFnSc1	lady
a	a	k8xC	a
Tramp	tramp	k1gInSc1	tramp
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Šípková	šípkový	k2eAgFnSc1d1	šípková
Růženka	Růženka	k1gFnSc1	Růženka
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
a	a	k8xC	a
101	[number]	k4	101
dalmatinů	dalmatin	k1gMnPc2	dalmatin
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
opět	opět	k6eAd1	opět
o	o	k7c4	o
něco	něco	k3yInSc4	něco
posunuly	posunout	k5eAaPmAgFnP	posunout
možnosti	možnost	k1gFnPc4	možnost
animace	animace	k1gFnPc4	animace
<g/>
.	.	kIx.	.
</s>
<s>
Disneyho	Disneyze	k6eAd1	Disneyze
hrané	hraný	k2eAgInPc1d1	hraný
snímky	snímek	k1gInPc1	snímek
byly	být	k5eAaImAgInP	být
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
do	do	k7c2	do
několika	několik	k4yIc2	několik
žánrů	žánr	k1gInPc2	žánr
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
historické	historický	k2eAgFnSc2d1	historická
beletrie	beletrie	k1gFnSc2	beletrie
(	(	kIx(	(
<g/>
Johnny	Johnna	k1gFnSc2	Johnna
Tremain	Tremain	k1gInSc1	Tremain
<g/>
,	,	kIx,	,
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
adaptací	adaptace	k1gFnSc7	adaptace
na	na	k7c4	na
dětské	dětský	k2eAgFnPc4d1	dětská
knížky	knížka	k1gFnPc4	knížka
(	(	kIx(	(
<g/>
Pollyanna	Pollyann	k1gMnSc2	Pollyann
<g/>
,	,	kIx,	,
1960	[number]	k4	1960
a	a	k8xC	a
novodobé	novodobý	k2eAgFnSc2d1	novodobá
komedie	komedie	k1gFnSc2	komedie
The	The	k1gFnSc2	The
Snaggy	Snagga	k1gFnSc2	Snagga
Dog	doga	k1gFnPc2	doga
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Disneyho	Disneyze	k6eAd1	Disneyze
nejúspěšnější	úspěšný	k2eAgInSc1d3	nejúspěšnější
film	film	k1gInSc1	film
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
je	být	k5eAaImIp3nS	být
napůl	napůl	k6eAd1	napůl
hraná	hraný	k2eAgFnSc1d1	hraná
<g/>
,	,	kIx,	,
napůl	napůl	k6eAd1	napůl
animovaná	animovaný	k2eAgFnSc1d1	animovaná
muzikálová	muzikálový	k2eAgFnSc1d1	muzikálová
adaptace	adaptace	k1gFnSc1	adaptace
Mary	Mary	k1gFnSc2	Mary
Poppins	Poppinsa	k1gFnPc2	Poppinsa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
hodně	hodně	k6eAd1	hodně
dlouho	dlouho	k6eAd1	dlouho
držela	držet	k5eAaImAgFnS	držet
na	na	k7c6	na
předních	přední	k2eAgFnPc6d1	přední
příčkách	příčka	k1gFnPc6	příčka
nejvýdělečnějších	výdělečný	k2eAgInPc2d3	nejvýdělečnější
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
dokonce	dokonce	k9	dokonce
pět	pět	k4xCc4	pět
Oscarů	Oscar	k1gInPc2	Oscar
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
kategorie	kategorie	k1gFnSc2	kategorie
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
herečka	herečka	k1gFnSc1	herečka
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
pro	pro	k7c4	pro
Julie	Julie	k1gFnPc4	Julie
Andrewsovou	Andrewsová	k1gFnSc4	Andrewsová
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1966	[number]	k4	1966
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Walt	Walt	k1gMnSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
na	na	k7c4	na
komplikace	komplikace	k1gFnPc4	komplikace
způsobené	způsobený	k2eAgFnPc4d1	způsobená
rakovinou	rakovina	k1gFnSc7	rakovina
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
společnosti	společnost	k1gFnSc2	společnost
převzal	převzít	k5eAaPmAgMnS	převzít
bratr	bratr	k1gMnSc1	bratr
Roy	Roy	k1gMnSc1	Roy
O.	O.	kA	O.
Disney	Disnea	k1gMnSc2	Disnea
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zastával	zastávat	k5eAaImAgMnS	zastávat
funkce	funkce	k1gFnSc2	funkce
předsedy	předseda	k1gMnSc2	předseda
představenstva	představenstvo	k1gNnSc2	představenstvo
<g/>
,	,	kIx,	,
CEO	CEO	kA	CEO
a	a	k8xC	a
prezidenta	prezident	k1gMnSc2	prezident
najednou	najednou	k6eAd1	najednou
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
prvních	první	k4xOgInPc2	první
kroků	krok	k1gInPc2	krok
bylo	být	k5eAaImAgNnS	být
přejmenování	přejmenování	k1gNnSc1	přejmenování
parku	park	k1gInSc2	park
Disney	Disnea	k1gFnSc2	Disnea
World	Worlda	k1gFnPc2	Worlda
na	na	k7c4	na
Walt	Walt	k1gInSc4	Walt
Disney	Disnea	k1gFnSc2	Disnea
World	World	k1gInSc4	World
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
vize	vize	k1gFnSc1	vize
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
byly	být	k5eAaImAgInP	být
premiérovány	premiérován	k2eAgInPc1d1	premiérován
dva	dva	k4xCgInPc1	dva
poslední	poslední	k2eAgInPc1d1	poslední
filmy	film	k1gInPc1	film
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgInPc4	který
měl	mít	k5eAaImAgInS	mít
Walt	Walt	k1gInSc1	Walt
přímý	přímý	k2eAgInSc4d1	přímý
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
animovaný	animovaný	k2eAgInSc1d1	animovaný
celovečerní	celovečerní	k2eAgInSc4d1	celovečerní
film	film	k1gInSc4	film
Kniha	kniha	k1gFnSc1	kniha
džunglí	džungle	k1gFnPc2	džungle
a	a	k8xC	a
muzikál	muzikál	k1gInSc1	muzikál
The	The	k1gMnSc1	The
Happiest	Happiest	k1gMnSc1	Happiest
Millionaire	Millionair	k1gMnSc5	Millionair
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
studio	studio	k1gNnSc4	studio
vydalo	vydat	k5eAaPmAgNnS	vydat
několik	několik	k4yIc1	několik
komedií	komedie	k1gFnPc2	komedie
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
The	The	k1gFnSc1	The
Love	lov	k1gInSc5	lov
Bug	Bug	k1gFnPc3	Bug
(	(	kIx(	(
<g/>
nejvýdělečnější	výdělečný	k2eAgInSc1d3	nejvýdělečnější
film	film	k1gInSc1	film
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
a	a	k8xC	a
The	The	k1gFnSc1	The
Computer	computer	k1gInSc1	computer
Wore	Wore	k1gInSc1	Wore
Tennis	Tennis	k1gFnSc1	Tennis
Shoes	Shoes	k1gMnSc1	Shoes
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrál	hrát	k5eAaImAgMnS	hrát
další	další	k2eAgInSc4d1	další
z	z	k7c2	z
Disneyho	Disney	k1gMnSc2	Disney
mladých	mladý	k2eAgInPc2d1	mladý
objevů	objev	k1gInPc2	objev
<g/>
,	,	kIx,	,
Kurt	Kurt	k1gMnSc1	Kurt
Russell	Russell	k1gMnSc1	Russell
<g/>
.	.	kIx.	.
80	[number]	k4	80
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
Disney	Disnea	k1gFnSc2	Disnea
zahájil	zahájit	k5eAaPmAgInS	zahájit
premiérou	premiéra	k1gFnSc7	premiéra
prvního	první	k4xOgInSc2	první
"	"	kIx"	"
<g/>
post-Waltovského	post-Waltovský	k2eAgInSc2d1	post-Waltovský
<g/>
"	"	kIx"	"
celovečerního	celovečerní	k2eAgInSc2d1	celovečerní
animovaného	animovaný	k2eAgInSc2d1	animovaný
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
Aristokočky	Aristokočka	k1gFnPc1	Aristokočka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
k	k	k7c3	k
fantasy	fantas	k1gInPc1	fantas
muzikálům	muzikál	k1gInPc3	muzikál
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInPc3	jaký
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
například	například	k6eAd1	například
Bedknobs	Bedknobs	k1gInSc1	Bedknobs
and	and	k?	and
Broomsticks	Broomsticks	k1gInSc1	Broomsticks
<g/>
.	.	kIx.	.
</s>
<s>
Blackbeard	Blackbeard	k1gInSc1	Blackbeard
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Ghost	Ghost	k1gInSc1	Ghost
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
dalším	další	k2eAgInSc7d1	další
úspěšným	úspěšný	k2eAgInSc7d1	úspěšný
filmem	film	k1gInSc7	film
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
premiérován	premiérovat	k5eAaBmNgInS	premiérovat
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1971	[number]	k4	1971
byl	být	k5eAaImAgMnS	být
Walt	Walt	k1gMnSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
World	World	k1gInSc1	World
slavnostně	slavnostně	k6eAd1	slavnostně
otevřen	otevřít	k5eAaPmNgInS	otevřít
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Roy	Roy	k1gMnSc1	Roy
Disney	Disnea	k1gFnSc2	Disnea
se	se	k3xPyFc4	se
do	do	k7c2	do
druhého	druhý	k4xOgInSc2	druhý
parku	park	k1gInSc2	park
osobně	osobně	k6eAd1	osobně
podíval	podívat	k5eAaImAgMnS	podívat
až	až	k9	až
za	za	k7c4	za
měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1971	[number]	k4	1971
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Roy	Roy	k1gMnSc1	Roy
Disney	Disnea	k1gFnSc2	Disnea
na	na	k7c4	na
mrtvici	mrtvice	k1gFnSc4	mrtvice
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
přešla	přejít	k5eAaPmAgFnS	přejít
pod	pod	k7c4	pod
vedení	vedení	k1gNnSc4	vedení
Donna	donna	k1gFnSc1	donna
Tatuma	Tatumum	k1gNnSc2	Tatumum
<g/>
,	,	kIx,	,
Carda	Carda	k1gMnSc1	Carda
Walkera	Walker	k1gMnSc2	Walker
a	a	k8xC	a
Waltova	Waltův	k2eAgMnSc2d1	Waltův
zetě	zeť	k1gMnSc2	zeť
Ronna	Ronn	k1gMnSc2	Ronn
Millera	Miller	k1gMnSc2	Miller
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
prošli	projít	k5eAaPmAgMnP	projít
zaškolením	zaškolení	k1gNnSc7	zaškolení
od	od	k7c2	od
Walta	Walto	k1gNnSc2	Walto
a	a	k8xC	a
Roye	Roy	k1gMnSc4	Roy
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Walt	Walt	k1gMnSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
Production	Production	k1gInSc1	Production
i	i	k8xC	i
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c6	v
uvádění	uvádění	k1gNnSc6	uvádění
rodinných	rodinný	k2eAgInPc2d1	rodinný
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byly	být	k5eAaImAgFnP	být
Escape	Escap	k1gInSc5	Escap
to	ten	k3xDgNnSc4	ten
Witch	Witch	k1gMnSc1	Witch
Mountain	Mountain	k1gMnSc1	Mountain
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
a	a	k8xC	a
Freaky	Freak	k1gInPc1	Freak
Friday	Fridaa	k1gFnSc2	Fridaa
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filmy	film	k1gInPc1	film
u	u	k7c2	u
pokladen	pokladna	k1gFnPc2	pokladna
nedopadly	dopadnout	k5eNaPmAgInP	dopadnout
tak	tak	k9	tak
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jejich	jejich	k3xOp3gMnPc1	jejich
předchůdci	předchůdce	k1gMnPc1	předchůdce
<g/>
.	.	kIx.	.
</s>
<s>
Animační	animační	k2eAgNnSc1d1	animační
studio	studio	k1gNnSc1	studio
nicméně	nicméně	k8xC	nicméně
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
úspěch	úspěch	k1gInSc4	úspěch
s	s	k7c7	s
filmy	film	k1gInPc7	film
Robin	Robina	k1gFnPc2	Robina
Hood	Hood	k1gInSc1	Hood
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Rescuers	Rescuersa	k1gFnPc2	Rescuersa
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
a	a	k8xC	a
Lišák	lišák	k1gMnSc1	lišák
a	a	k8xC	a
pes	pes	k1gMnSc1	pes
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
vedoucí	vedoucí	k1gFnSc1	vedoucí
studia	studio	k1gNnSc2	studio
se	se	k3xPyFc4	se
Miller	Miller	k1gMnSc1	Miller
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
zaměřit	zaměřit	k5eAaPmF	zaměřit
se	se	k3xPyFc4	se
také	také	k9	také
na	na	k7c4	na
ziskový	ziskový	k2eAgInSc4d1	ziskový
trh	trh	k1gInSc4	trh
s	s	k7c7	s
teenagerskou	teenagerský	k2eAgFnSc7d1	teenagerská
cílovou	cílový	k2eAgFnSc7d1	cílová
skupinou	skupina	k1gFnSc7	skupina
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
sledování	sledování	k1gNnSc1	sledování
Disneyho	Disney	k1gMnSc2	Disney
filmů	film	k1gInPc2	film
vyhýbali	vyhýbat	k5eAaImAgMnP	vyhýbat
<g/>
.	.	kIx.	.
</s>
<s>
Inspirováno	inspirovat	k5eAaBmNgNnS	inspirovat
popularitou	popularita	k1gFnSc7	popularita
Hvězdných	hvězdný	k2eAgFnPc2d1	hvězdná
válek	válka	k1gFnPc2	válka
vyprodukovalo	vyprodukovat	k5eAaPmAgNnS	vyprodukovat
Disneyho	Disneyha	k1gFnSc5	Disneyha
studio	studio	k1gNnSc1	studio
sci-fi	scii	k1gFnSc7	sci-fi
dobrodružství	dobrodružství	k1gNnSc2	dobrodružství
The	The	k1gMnPc2	The
Black	Black	k1gMnSc1	Black
Hole	hole	k1gFnSc2	hole
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vydělalo	vydělat	k5eAaPmAgNnS	vydělat
$	$	kIx~	$
<g/>
20	[number]	k4	20
milionů	milion	k4xCgInPc2	milion
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
se	se	k3xPyFc4	se
ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
Hvězdných	hvězdný	k2eAgFnPc2d1	hvězdná
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Black	Black	k1gInSc1	Black
Hole	hole	k6eAd1	hole
byl	být	k5eAaImAgInS	být
Disneyho	Disney	k1gMnSc4	Disney
první	první	k4xOgInSc4	první
film	film	k1gInSc4	film
s	s	k7c7	s
hodnocením	hodnocení	k1gNnSc7	hodnocení
PG	PG	kA	PG
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Disney	Disney	k1gInPc4	Disney
občas	občas	k6eAd1	občas
zabrousil	zabrousit	k5eAaPmAgInS	zabrousit
i	i	k9	i
do	do	k7c2	do
hororového	hororový	k2eAgInSc2d1	hororový
žánru	žánr	k1gInSc2	žánr
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Watcher	Watchra	k1gFnPc2	Watchra
in	in	k?	in
the	the	k?	the
Woods	Woods	k1gInSc1	Woods
<g/>
)	)	kIx)	)
a	a	k8xC	a
financoval	financovat	k5eAaBmAgInS	financovat
odvážného	odvážný	k2eAgMnSc4d1	odvážný
a	a	k8xC	a
inovativního	inovativní	k2eAgMnSc4d1	inovativní
Trona	Tron	k1gMnSc4	Tron
<g/>
;	;	kIx,	;
oba	dva	k4xCgInPc1	dva
filmy	film	k1gInPc1	film
se	se	k3xPyFc4	se
bohužel	bohužel	k9	bohužel
propadly	propadnout	k5eAaPmAgInP	propadnout
<g/>
.	.	kIx.	.
</s>
<s>
Disney	Disnea	k1gFnPc4	Disnea
také	také	k6eAd1	také
najal	najmout	k5eAaPmAgMnS	najmout
producenty	producent	k1gMnPc4	producent
pro	pro	k7c4	pro
filmové	filmový	k2eAgInPc4d1	filmový
projekty	projekt	k1gInPc4	projekt
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yRgFnSc6	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
studia	studio	k1gNnSc2	studio
ani	ani	k8xC	ani
nepomyslelo	pomyslet	k5eNaPmAgNnS	pomyslet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
Disney	Disnea	k1gFnSc2	Disnea
a	a	k8xC	a
Paramount	Paramount	k1gInSc1	Paramount
Pictures	Picturesa	k1gFnPc2	Picturesa
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
na	na	k7c6	na
výrobě	výroba	k1gFnSc6	výroba
filmové	filmový	k2eAgFnSc2d1	filmová
adaptace	adaptace	k1gFnSc2	adaptace
Popeye	Popey	k1gFnSc2	Popey
and	and	k?	and
Dragonslayer	Dragonslayer	k1gMnSc1	Dragonslayer
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
první	první	k4xOgFnSc6	první
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
cizím	cizí	k2eAgNnSc7d1	cizí
studiem	studio	k1gNnSc7	studio
<g/>
.	.	kIx.	.
</s>
<s>
Paramount	Paramount	k1gMnSc1	Paramount
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
distribuoval	distribuovat	k5eAaBmAgMnS	distribuovat
Disneyho	Disney	k1gMnSc2	Disney
filmy	film	k1gInPc1	film
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
-	-	kIx~	-
doufalo	doufat	k5eAaImAgNnS	doufat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Disneyho	Disney	k1gMnSc4	Disney
marketinková	marketinkový	k2eAgFnSc1d1	marketinková
prestiž	prestiž	k1gFnSc1	prestiž
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
zajistit	zajistit	k5eAaPmF	zajistit
lepší	dobrý	k2eAgInSc4d2	lepší
prodej	prodej	k1gInSc4	prodej
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
začala	začít	k5eAaPmAgFnS	začít
premiérováním	premiérování	k1gNnSc7	premiérování
Mickey	Mickea	k1gFnSc2	Mickea
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Christmas	Christmas	k1gInSc1	Christmas
Carol	Carol	k1gInSc1	Carol
řada	řada	k1gFnSc1	řada
úspěšných	úspěšný	k2eAgInPc2d1	úspěšný
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
počínaje	počínaje	k7c7	počínaje
Never	Never	k1gMnSc1	Never
Cry	Cry	k1gMnSc1	Cry
Wolf	Wolf	k1gMnSc1	Wolf
a	a	k8xC	a
filmová	filmový	k2eAgFnSc1d1	filmová
adaptace	adaptace	k1gFnSc1	adaptace
Raye	Ray	k1gMnSc2	Ray
Bradburyho	Bradbury	k1gMnSc2	Bradbury
Something	Something	k1gInSc1	Something
Wicked	Wicked	k1gMnSc1	Wicked
This	Thisa	k1gFnPc2	Thisa
Way	Way	k1gMnSc1	Way
Comes	Comes	k1gMnSc1	Comes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Disneyho	Disney	k1gMnSc2	Disney
CEO	CEO	kA	CEO
Ron	Ron	k1gMnSc1	Ron
Miller	Miller	k1gMnSc1	Miller
značku	značka	k1gFnSc4	značka
Touchstone	Touchston	k1gInSc5	Touchston
Pictures	Pictures	k1gMnSc1	Pictures
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
filmů	film	k1gInPc2	film
pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
filmem	film	k1gInSc7	film
od	od	k7c2	od
Touchstone	Touchston	k1gInSc5	Touchston
byla	být	k5eAaImAgFnS	být
komedie	komedie	k1gFnSc1	komedie
Splash	Splash	k1gMnSc1	Splash
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
kasovní	kasovní	k2eAgInSc4d1	kasovní
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
přetrvávajícím	přetrvávající	k2eAgInSc7d1	přetrvávající
pořadem	pořad	k1gInSc7	pořad
The	The	k1gFnSc2	The
Wonderful	Wonderful	k1gInSc1	Wonderful
World	World	k1gMnSc1	World
of	of	k?	of
Disney	Disnea	k1gFnSc2	Disnea
v	v	k7c6	v
prime-timeovém	primeimeový	k2eAgInSc6d1	prime-timeový
čase	čas	k1gInSc6	čas
se	se	k3xPyFc4	se
Disney	Disnea	k1gFnPc1	Disnea
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
vrátil	vrátit	k5eAaPmAgMnS	vrátit
k	k	k7c3	k
televizi	televize	k1gFnSc3	televize
s	s	k7c7	s
novým	nový	k2eAgInSc7d1	nový
seriálem	seriál	k1gInSc7	seriál
The	The	k1gMnPc2	The
Mouse	Mouse	k1gFnSc2	Mouse
Factory	Factor	k1gMnPc7	Factor
a	a	k8xC	a
krátkým	krátký	k2eAgNnSc7d1	krátké
oživením	oživení	k1gNnSc7	oživení
Mickey	Mickea	k1gFnSc2	Mickea
Mouse	Mouse	k1gFnSc2	Mouse
Club	club	k1gInSc1	club
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
Disney	Disnea	k1gFnSc2	Disnea
otevřel	otevřít	k5eAaPmAgMnS	otevřít
divizi	divize	k1gFnSc4	divize
Walt	Walt	k1gMnSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
Home	Hom	k1gFnSc2	Hom
Video	video	k1gNnSc1	video
k	k	k7c3	k
distribuci	distribuce	k1gFnSc3	distribuce
videokazet	videokazeta	k1gFnPc2	videokazeta
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
pomalu	pomalu	k6eAd1	pomalu
rozmáhat	rozmáhat	k5eAaImF	rozmáhat
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1983	[number]	k4	1983
zahájil	zahájit	k5eAaPmAgMnS	zahájit
svoje	svůj	k3xOyFgNnSc4	svůj
vysílání	vysílání	k1gNnSc4	vysílání
Disney	Disnea	k1gMnSc2	Disnea
Channel	Channel	k1gMnSc1	Channel
jako	jako	k8xC	jako
placený	placený	k2eAgInSc1d1	placený
kanál	kanál	k1gInSc1	kanál
v	v	k7c6	v
kabelové	kabelový	k2eAgFnSc6d1	kabelová
televizi	televize	k1gFnSc6	televize
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vysílal	vysílat	k5eAaImAgInS	vysílat
pro	pro	k7c4	pro
celé	celý	k2eAgNnSc4d1	celé
USA	USA	kA	USA
filmy	film	k1gInPc4	film
a	a	k8xC	a
TV	TV	kA	TV
seriály	seriál	k1gInPc1	seriál
z	z	k7c2	z
Disneyho	Disney	k1gMnSc2	Disney
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
původním	původní	k2eAgInSc7d1	původní
programem	program	k1gInSc7	program
a	a	k8xC	a
rodinnými	rodinný	k2eAgInPc7d1	rodinný
pořady	pořad	k1gInPc7	pořad
od	od	k7c2	od
třetích	třetí	k4xOgFnPc2	třetí
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Walt	Walt	k1gMnSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
World	World	k1gMnSc1	World
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1970	[number]	k4	1970
<g/>
-	-	kIx~	-
<g/>
1980	[number]	k4	1980
hodně	hodně	k6eAd1	hodně
pozornosti	pozornost	k1gFnSc3	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
Disneyho	Disneyha	k1gFnSc5	Disneyha
manažeři	manažer	k1gMnPc1	manažer
oznámili	oznámit	k5eAaPmAgMnP	oznámit
plán	plán	k1gInSc4	plán
na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
zábavní	zábavní	k2eAgInSc4d1	zábavní
park	park	k1gInSc4	park
ve	v	k7c4	v
Walt	Walt	k1gInSc4	Walt
Disney	Disnea	k1gFnSc2	Disnea
World	Worlda	k1gFnPc2	Worlda
<g/>
,	,	kIx,	,
EPCOT	EPCOT	kA	EPCOT
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
<s>
Inspirován	inspirován	k2eAgInSc1d1	inspirován
snem	sen	k1gInSc7	sen
Walta	Walt	k1gInSc2	Walt
Disneyho	Disney	k1gMnSc2	Disney
o	o	k7c6	o
futuristickém	futuristický	k2eAgInSc6d1	futuristický
modelu	model	k1gInSc6	model
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
EPCOT	EPCOT	kA	EPCOT
Center	centrum	k1gNnPc2	centrum
postaven	postavit	k5eAaPmNgInS	postavit
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
stálá	stálý	k2eAgFnSc1d1	stálá
světová	světový	k2eAgFnSc1d1	světová
výstava	výstava	k1gFnSc1	výstava
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
postavená	postavený	k2eAgFnSc1d1	postavená
díky	díky	k7c3	díky
sponzorům	sponzor	k1gMnPc3	sponzor
předních	přední	k2eAgFnPc2d1	přední
amerických	americký	k2eAgFnPc2d1	americká
firem	firma	k1gFnPc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
národů	národ	k1gInPc2	národ
odlišných	odlišný	k2eAgFnPc2d1	odlišná
kultur	kultura	k1gFnPc2	kultura
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
parku	park	k1gInSc6	park
svůj	svůj	k3xOyFgInSc4	svůj
pavilon	pavilon	k1gInSc4	pavilon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
společnost	společnost	k1gFnSc1	společnost
Oriental	Oriental	k1gMnSc1	Oriental
Land	Land	k1gMnSc1	Land
ve	v	k7c4	v
Walt	Walt	k1gInSc4	Walt
Disney	Disnea	k1gFnSc2	Disnea
Productions	Productions	k1gInSc4	Productions
na	na	k7c4	na
postavení	postavení	k1gNnSc4	postavení
prvního	první	k4xOgMnSc2	první
Disneyho	Disney	k1gMnSc2	Disney
zábavního	zábavní	k2eAgInSc2d1	zábavní
parku	park	k1gInSc2	park
mimo	mimo	k7c4	mimo
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
Tokyo	Tokyo	k6eAd1	Tokyo
Disneyland	Disneyland	k1gInSc1	Disneyland
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
úspěch	úspěch	k1gInSc4	úspěch
Disney	Disnea	k1gFnSc2	Disnea
Channel	Channela	k1gFnPc2	Channela
a	a	k8xC	a
nových	nový	k2eAgInPc2d1	nový
zábavních	zábavní	k2eAgInPc2d1	zábavní
parků	park	k1gInPc2	park
byly	být	k5eAaImAgInP	být
Walt	Walt	k1gInSc4	Walt
Disney	Disnea	k1gFnSc2	Disnea
Productions	Productionsa	k1gFnPc2	Productionsa
finančně	finančně	k6eAd1	finančně
zranitelné	zranitelný	k2eAgInPc1d1	zranitelný
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
filmová	filmový	k2eAgFnSc1d1	filmová
knihovna	knihovna	k1gFnSc1	knihovna
byla	být	k5eAaImAgFnS	být
cenná	cenný	k2eAgFnSc1d1	cenná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gNnSc1	jeho
vedení	vedení	k1gNnSc1	vedení
nebylo	být	k5eNaImAgNnS	být
schopné	schopný	k2eAgNnSc1d1	schopné
udržet	udržet	k5eAaPmF	udržet
krok	krok	k1gInSc4	krok
s	s	k7c7	s
ostatními	ostatní	k2eAgNnPc7d1	ostatní
studii	studio	k1gNnPc7	studio
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
s	s	k7c7	s
prací	práce	k1gFnSc7	práce
Dona	Don	k1gMnSc2	Don
Blutha	Bluth	k1gMnSc2	Bluth
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
Disneyho	Disney	k1gMnSc4	Disney
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
předběhl	předběhnout	k5eAaPmAgMnS	předběhnout
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
tvořily	tvořit	k5eAaImAgInP	tvořit
parky	park	k1gInPc1	park
70	[number]	k4	70
<g/>
%	%	kIx~	%
Disneyho	Disney	k1gMnSc2	Disney
celkových	celkový	k2eAgInPc2d1	celkový
příjmů	příjem	k1gInPc2	příjem
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
spustila	spustit	k5eAaPmAgFnS	spustit
společnost	společnost	k1gFnSc1	společnost
Realiance	Realiance	k1gFnSc2	Realiance
Group	Group	k1gInSc1	Group
Holdings	Holdings	k1gInSc1	Holdings
finančníka	finančník	k1gMnSc2	finančník
Saula	Saul	k1gMnSc2	Saul
Steinberga	Steinberg	k1gMnSc2	Steinberg
nepřátelskou	přátelský	k2eNgFnSc4d1	nepřátelská
nabídku	nabídka	k1gFnSc4	nabídka
na	na	k7c6	na
převzetí	převzetí	k1gNnSc6	převzetí
Walt	Walta	k1gFnPc2	Walta
Disney	Disnea	k1gFnSc2	Disnea
Productions	Productions	k1gInSc1	Productions
<g/>
,	,	kIx,	,
s	s	k7c7	s
úmyslem	úmysl	k1gInSc7	úmysl
zrušení	zrušení	k1gNnSc1	zrušení
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
rozprodejem	rozprodej	k1gInSc7	rozprodej
jejích	její	k3xOp3gFnPc2	její
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
aktiv	aktivum	k1gNnPc2	aktivum
<g/>
.	.	kIx.	.
</s>
<s>
Disney	Disne	k1gMnPc4	Disne
koupil	koupit	k5eAaPmAgInS	koupit
11,1	[number]	k4	11,1
<g/>
%	%	kIx~	%
podíl	podíl	k1gInSc4	podíl
v	v	k7c6	v
Reliance	Relianka	k1gFnSc6	Relianka
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
další	další	k2eAgMnSc1d1	další
akcionář	akcionář	k1gMnSc1	akcionář
podal	podat	k5eAaPmAgMnS	podat
žalobu	žaloba	k1gFnSc4	žaloba
s	s	k7c7	s
prohlášením	prohlášení	k1gNnSc7	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
obchod	obchod	k1gInSc1	obchod
devalvoval	devalvovat	k5eAaBmAgInS	devalvovat
Disneyho	Disney	k1gMnSc4	Disney
akcie	akcie	k1gFnSc2	akcie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
Disneyho	Disney	k1gMnSc4	Disney
management	management	k1gInSc1	management
zachoval	zachovat	k5eAaPmAgInS	zachovat
svoji	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
Soudní	soudní	k2eAgInSc1d1	soudní
spor	spor	k1gInSc1	spor
byl	být	k5eAaImAgInS	být
vyřešen	vyřešen	k2eAgInSc1d1	vyřešen
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
akcionář	akcionář	k1gMnSc1	akcionář
dostal	dostat	k5eAaPmAgMnS	dostat
od	od	k7c2	od
Disneyho	Disney	k1gMnSc2	Disney
a	a	k8xC	a
Reliance	Reliance	k1gFnPc4	Reliance
celkem	celkem	k6eAd1	celkem
$	$	kIx~	$
<g/>
45	[number]	k4	45
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
rodina	rodina	k1gFnSc1	rodina
Sida	Sidus	k1gMnSc2	Sidus
Basse	Bass	k1gMnSc2	Bass
získala	získat	k5eAaPmAgFnS	získat
18,7	[number]	k4	18,7
%	%	kIx~	%
Disneyho	Disney	k1gMnSc2	Disney
akcií	akcie	k1gFnPc2	akcie
<g/>
,	,	kIx,	,
dosadil	dosadit	k5eAaPmAgMnS	dosadit
Bass	Bass	k1gMnSc1	Bass
a	a	k8xC	a
představenstvo	představenstvo	k1gNnSc1	představenstvo
Michaela	Michael	k1gMnSc2	Michael
Eisnera	Eisner	k1gMnSc2	Eisner
z	z	k7c2	z
Paramount	Paramounta	k1gFnPc2	Paramounta
Pictures	Picturesa	k1gFnPc2	Picturesa
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
(	(	kIx(	(
<g/>
CEO	CEO	kA	CEO
<g/>
)	)	kIx)	)
a	a	k8xC	a
Franka	Frank	k1gMnSc2	Frank
Wellse	Wells	k1gMnSc2	Wells
z	z	k7c2	z
Warner	Warnra	k1gFnPc2	Warnra
Bros	Brosa	k1gFnPc2	Brosa
<g/>
.	.	kIx.	.
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
prezidenta	prezident	k1gMnSc2	prezident
The	The	k1gMnSc2	The
Walt	Walt	k1gMnSc1	Walt
Disney	Disnea	k1gMnSc2	Disnea
Company	Compana	k1gFnSc2	Compana
<g/>
.	.	kIx.	.
</s>
<s>
Eisner	Eisner	k1gMnSc1	Eisner
vyzdvihl	vyzdvihnout	k5eAaPmAgMnS	vyzdvihnout
důležitost	důležitost	k1gFnSc4	důležitost
filmů	film	k1gInPc2	film
Touchstone	Touchston	k1gInSc5	Touchston
Pictures	Pictures	k1gMnSc1	Pictures
<g/>
;	;	kIx,	;
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
měl	mít	k5eAaImAgMnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
Down	Downa	k1gFnPc2	Downa
and	and	k?	and
Out	Out	k1gMnSc1	Out
in	in	k?	in
Beverly	Beverla	k1gFnSc2	Beverla
Hills	Hillsa	k1gFnPc2	Hillsa
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
úspěch	úspěch	k1gInSc1	úspěch
vedl	vést	k5eAaImAgInS	vést
i	i	k9	i
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
tržeb	tržba	k1gFnPc2	tržba
u	u	k7c2	u
filmů	film	k1gInPc2	film
Outrageous	Outrageous	k1gInSc1	Outrageous
Fortune	Fortun	k1gInSc5	Fortun
<g/>
,	,	kIx,	,
Tin	Tina	k1gFnPc2	Tina
Men	Men	k1gFnSc2	Men
<g/>
,	,	kIx,	,
Ruthless	Ruthless	k1gInSc4	Ruthless
People	People	k1gFnPc2	People
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
hitů	hit	k1gInPc2	hit
<g/>
.	.	kIx.	.
</s>
<s>
Eisner	Eisner	k1gMnSc1	Eisner
využil	využít	k5eAaPmAgMnS	využít
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
rozšiřující	rozšiřující	k2eAgInPc1d1	rozšiřující
kabelové	kabelový	k2eAgInPc1d1	kabelový
trhy	trh	k1gInPc1	trh
a	a	k8xC	a
domácí	domácí	k2eAgNnPc1d1	domácí
videa	video	k1gNnPc1	video
k	k	k7c3	k
uzavření	uzavření	k1gNnSc3	uzavření
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
Disneyho	Disney	k1gMnSc2	Disney
seriálů	seriál	k1gInPc2	seriál
a	a	k8xC	a
filmů	film	k1gInPc2	film
se	se	k3xPyFc4	se
Showtime	Showtim	k1gInSc5	Showtim
Networks	Networks	k1gInSc1	Networks
pro	pro	k7c4	pro
Disneyho	Disney	k1gMnSc4	Disney
<g/>
/	/	kIx~	/
<g/>
Touchstone	Touchston	k1gInSc5	Touchston
trvající	trvající	k2eAgInSc1d1	trvající
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
k	k	k7c3	k
zajištění	zajištění	k1gNnSc3	zajištění
prodeje	prodej	k1gInSc2	prodej
a	a	k8xC	a
distribuce	distribuce	k1gFnSc2	distribuce
seriálů	seriál	k1gInPc2	seriál
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
The	The	k1gFnSc1	The
Golden	Goldna	k1gFnPc2	Goldna
Girl	girl	k1gFnSc2	girl
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
televizním	televizní	k2eAgFnPc3d1	televizní
stanicím	stanice	k1gFnPc3	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Disney	Disnea	k1gFnPc1	Disnea
na	na	k7c6	na
konci	konec	k1gInSc6	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
začal	začít	k5eAaPmAgInS	začít
s	s	k7c7	s
limitovaným	limitovaný	k2eAgNnSc7d1	limitované
vydáváním	vydávání	k1gNnSc7	vydávání
svým	svůj	k1gMnSc7	svůj
předchozích	předchozí	k2eAgInPc2d1	předchozí
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Disney	Disnea	k1gFnPc1	Disnea
za	za	k7c4	za
Eisnera	Eisner	k1gMnSc4	Eisner
koupil	koupit	k5eAaPmAgMnS	koupit
KHJ	KHJ	kA	KHJ
<g/>
,	,	kIx,	,
nezávislou	závislý	k2eNgFnSc4d1	nezávislá
televizní	televizní	k2eAgFnSc4d1	televizní
stanici	stanice	k1gFnSc4	stanice
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angeles	k1gInSc4	Angeles
<g/>
,	,	kIx,	,
Kalifornii	Kalifornie	k1gFnSc4	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
se	se	k3xPyFc4	se
zorganizovalo	zorganizovat	k5eAaPmAgNnS	zorganizovat
financování	financování	k1gNnSc4	financování
filmů	film	k1gInPc2	film
pro	pro	k7c4	pro
Walt	Walt	k1gInSc4	Walt
Disney	Disne	k2eAgInPc1d1	Disne
Company	Compan	k1gInPc1	Compan
společností	společnost	k1gFnPc2	společnost
Silver	Silvra	k1gFnPc2	Silvra
Screen	Screen	k2eAgInSc4d1	Screen
Partners	Partners	k1gInSc4	Partners
II	II	kA	II
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
rovných	rovný	k2eAgInPc2d1	rovný
$	$	kIx~	$
<g/>
193	[number]	k4	193
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1987	[number]	k4	1987
začal	začít	k5eAaPmAgInS	začít
Silver	Silver	k1gInSc1	Silver
Screen	Screen	k1gInSc4	Screen
III	III	kA	III
financovat	financovat	k5eAaBmF	financovat
další	další	k2eAgInSc4d1	další
film	film	k1gInSc4	film
pro	pro	k7c4	pro
Disneyho	Disney	k1gMnSc4	Disney
se	s	k7c7	s
zvýšeným	zvýšený	k2eAgInSc7d1	zvýšený
objemem	objem	k1gInSc7	objem
financí	finance	k1gFnPc2	finance
$	$	kIx~	$
<g/>
300	[number]	k4	300
milionů	milion	k4xCgInPc2	milion
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgFnP	být
limitovány	limitován	k2eAgInPc1d1	limitován
partnerstvím	partnerství	k1gNnSc7	partnerství
s	s	k7c7	s
E.	E.	kA	E.
<g/>
F.	F.	kA	F.
Huttonem	Hutton	k1gInSc7	Hutton
<g/>
.	.	kIx.	.
</s>
<s>
Silver	Silver	k1gInSc1	Silver
Screen	Screna	k1gFnPc2	Screna
IV	Iva	k1gFnPc2	Iva
také	také	k9	také
připravil	připravit	k5eAaPmAgInS	připravit
financování	financování	k1gNnSc4	financování
Disneyho	Disney	k1gMnSc4	Disney
studií	studie	k1gFnPc2	studie
<g/>
.	.	kIx.	.
</s>
<s>
Počínaje	počínaje	k7c7	počínaje
filmem	film	k1gInSc7	film
Falešná	falešný	k2eAgFnSc1d1	falešná
hra	hra	k1gFnSc1	hra
s	s	k7c7	s
králíkem	králík	k1gMnSc7	králík
Rogerem	Roger	k1gMnSc7	Roger
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
Malou	malý	k2eAgFnSc7d1	malá
mořskou	mořský	k2eAgFnSc7d1	mořská
vílou	víla	k1gFnSc7	víla
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Disneyho	Disney	k1gMnSc2	Disney
vlajkové	vlajkový	k2eAgNnSc1d1	vlajkové
studio	studio	k1gNnSc1	studio
setkalo	setkat	k5eAaPmAgNnS	setkat
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
komerčních	komerční	k2eAgInPc2d1	komerční
a	a	k8xC	a
kritických	kritický	k2eAgInPc2d1	kritický
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
společnost	společnost	k1gFnSc1	společnost
také	také	k9	také
úspěšně	úspěšně	k6eAd1	úspěšně
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
na	na	k7c4	na
pole	pole	k1gNnSc4	pole
televizní	televizní	k2eAgFnSc2d1	televizní
animace	animace	k1gFnSc2	animace
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
finančně	finančně	k6eAd1	finančně
náročných	náročný	k2eAgInPc2d1	náročný
a	a	k8xC	a
uznávaných	uznávaný	k2eAgInPc2d1	uznávaný
seriálu	seriál	k1gInSc6	seriál
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
Gumídci	Gumídce	k1gMnPc1	Gumídce
<g/>
,	,	kIx,	,
Kačeří	kačeří	k2eAgInPc1d1	kačeří
příběhy	příběh	k1gInPc1	příběh
a	a	k8xC	a
Gargoyles	Gargoyles	k1gInSc1	Gargoyles
<g/>
.	.	kIx.	.
</s>
<s>
Disney	Disnea	k1gFnPc1	Disnea
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
dostal	dostat	k5eAaPmAgInS	dostat
na	na	k7c4	na
přední	přední	k2eAgFnPc4d1	přední
příčky	příčka	k1gFnPc4	příčka
v	v	k7c6	v
tržbách	tržba	k1gFnPc6	tržba
a	a	k8xC	a
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
zvyšoval	zvyšovat	k5eAaImAgInS	zvyšovat
svůj	svůj	k3xOyFgInSc4	svůj
obrat	obrat	k1gInSc4	obrat
o	o	k7c4	o
20	[number]	k4	20
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
"	"	kIx"	"
<g/>
Disneyho	Disney	k1gMnSc2	Disney
dekádě	dekáda	k1gFnSc6	dekáda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
The	The	k1gFnSc1	The
Walt	Walta	k1gFnPc2	Walta
Disney	Disnea	k1gMnSc2	Disnea
Company	Compana	k1gFnSc2	Compana
pojmenovala	pojmenovat	k5eAaPmAgFnS	pojmenovat
90	[number]	k4	90
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nejvyšší	vysoký	k2eAgMnPc1d3	nejvyšší
představitelé	představitel	k1gMnPc1	představitel
společnosti	společnost	k1gFnSc2	společnost
snažili	snažit	k5eAaImAgMnP	snažit
dostat	dostat	k5eAaPmF	dostat
ji	on	k3xPp3gFnSc4	on
do	do	k7c2	do
nových	nový	k2eAgFnPc2d1	nová
výšin	výšina	k1gFnPc2	výšina
<g/>
,	,	kIx,	,
zajistit	zajistit	k5eAaPmF	zajistit
to	ten	k3xDgNnSc1	ten
měly	mít	k5eAaImAgFnP	mít
velké	velký	k2eAgFnPc1d1	velká
změny	změna	k1gFnPc1	změna
a	a	k8xC	a
nasvědčovaly	nasvědčovat	k5eAaImAgFnP	nasvědčovat
tomu	ten	k3xDgNnSc3	ten
především	především	k9	především
úspěchy	úspěch	k1gInPc7	úspěch
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
firma	firma	k1gFnSc1	firma
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1990	[number]	k4	1990
zařídila	zařídit	k5eAaPmAgFnS	zařídit
společnost	společnost	k1gFnSc1	společnost
Disney	Disnea	k1gFnSc2	Disnea
financování	financování	k1gNnSc2	financování
(	(	kIx(	(
<g/>
$	$	kIx~	$
<g/>
200	[number]	k4	200
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
filmy	film	k1gInPc4	film
od	od	k7c2	od
Interscope	Interscop	k1gInSc5	Interscop
vyrobených	vyrobený	k2eAgInPc2d1	vyrobený
pro	pro	k7c4	pro
Disney	Disne	k1gMnPc4	Disne
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
plynulo	plynout	k5eAaImAgNnS	plynout
z	z	k7c2	z
finanční	finanční	k2eAgFnSc2d1	finanční
skupiny	skupina	k1gFnSc2	skupina
Nomura	Nomura	k1gFnSc1	Nomura
Securities	Securities	k1gInSc1	Securities
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
23	[number]	k4	23
<g/>
.	.	kIx.	.
říjnu	říjen	k1gInSc6	říjen
utvořil	utvořit	k5eAaPmAgInS	utvořit
Disney	Disney	k1gInPc7	Disney
skupinu	skupina	k1gFnSc4	skupina
Touchwood	Touchwood	k1gInSc1	Touchwood
Pacific	Pacific	k1gMnSc1	Pacific
Partners	Partnersa	k1gFnPc2	Partnersa
I	I	kA	I
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
tvořila	tvořit	k5eAaImAgFnS	tvořit
primární	primární	k2eAgInSc4d1	primární
zdroj	zdroj	k1gInSc4	zdroj
příjmů	příjem	k1gInPc2	příjem
<g/>
,	,	kIx,	,
a	a	k8xC	a
která	který	k3yQgFnSc1	který
nahradila	nahradit	k5eAaPmAgFnS	nahradit
Silver	Silver	k1gInSc4	Silver
Screen	Screen	k2eAgInSc4d1	Screen
Partnership	Partnership	k1gInSc4	Partnership
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
tvořily	tvořit	k5eAaImAgInP	tvořit
hotely	hotel	k1gInPc1	hotel
<g/>
,	,	kIx,	,
distribuce	distribuce	k1gFnSc1	distribuce
domácího	domácí	k2eAgNnSc2d1	domácí
videa	video	k1gNnSc2	video
a	a	k8xC	a
Disneyho	Disney	k1gMnSc4	Disney
merchandising	merchandising	k1gInSc4	merchandising
28	[number]	k4	28
%	%	kIx~	%
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
obratu	obrat	k1gInSc2	obrat
společnosti	společnost	k1gFnSc2	společnost
<g/>
;	;	kIx,	;
v	v	k7c6	v
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
obratu	obrat	k1gInSc6	obrat
22	[number]	k4	22
%	%	kIx~	%
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kvartálu	kvartál	k1gInSc6	kvartál
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
svým	svůj	k3xOyFgNnPc3	svůj
studiím	studio	k1gNnPc3	studio
vyrobit	vyrobit	k5eAaPmF	vyrobit
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
celkem	celkem	k6eAd1	celkem
25	[number]	k4	25
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
příjmy	příjem	k1gInPc7	příjem
klesly	klesnout	k5eAaPmAgInP	klesnout
o	o	k7c6	o
23	[number]	k4	23
<g/>
%	%	kIx~	%
a	a	k8xC	a
nebyl	být	k5eNaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
žádný	žádný	k3yNgInSc1	žádný
růst	růst	k1gInSc1	růst
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
úspěchem	úspěch	k1gInSc7	úspěch
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
film	film	k1gInSc1	film
Kráska	kráska	k1gFnSc1	kráska
a	a	k8xC	a
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
,	,	kIx,	,
držitel	držitel	k1gMnSc1	držitel
2	[number]	k4	2
Oscarů	Oscar	k1gInPc2	Oscar
a	a	k8xC	a
nejvýdělečnější	výdělečný	k2eAgInSc1d3	nejvýdělečnější
film	film	k1gInSc1	film
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
cílové	cílový	k2eAgFnSc6d1	cílová
skupině	skupina	k1gFnSc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Disney	Disne	k1gMnPc4	Disne
se	se	k3xPyFc4	se
zacílil	zacílit	k5eAaPmAgMnS	zacílit
také	také	k9	také
na	na	k7c4	na
vydávání	vydávání	k1gNnSc4	vydávání
knih	kniha	k1gFnPc2	kniha
a	a	k8xC	a
časopisů	časopis	k1gInPc2	časopis
dceřinou	dceřin	k2eAgFnSc7d1	dceřina
společností	společnost	k1gFnSc7	společnost
Hyperion	Hyperion	k1gInSc1	Hyperion
Press	Pressa	k1gFnPc2	Pressa
a	a	k8xC	a
na	na	k7c4	na
hudbu	hudba	k1gFnSc4	hudba
pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
svým	svůj	k3xOyFgNnSc7	svůj
hudebním	hudební	k2eAgNnSc7d1	hudební
vydavatelstvím	vydavatelství	k1gNnSc7	vydavatelství
Hollywood	Hollywood	k1gInSc1	Hollywood
Records	Records	k1gInSc1	Records
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
vydává	vydávat	k5eAaPmIp3nS	vydávat
hudbu	hudba	k1gFnSc4	hudba
pro	pro	k7c4	pro
teenagery	teenager	k1gMnPc4	teenager
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Disney	Disne	k2eAgInPc1d1	Disne
Imagineering	Imagineering	k1gInSc1	Imagineering
musel	muset	k5eAaImAgInS	muset
propustit	propustit	k5eAaPmF	propustit
400	[number]	k4	400
svých	svůj	k3xOyFgMnPc2	svůj
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Disney	Disney	k1gInPc4	Disney
také	také	k6eAd1	také
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
své	svůj	k3xOyFgNnSc4	svůj
pole	pole	k1gNnSc4	pole
působnosti	působnost	k1gFnSc2	působnost
na	na	k7c4	na
dospělejší	dospělý	k2eAgNnSc4d2	dospělejší
publikum	publikum	k1gNnSc4	publikum
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
co	co	k9	co
předseda	předseda	k1gMnSc1	předseda
Disneyho	Disney	k1gMnSc2	Disney
studií	studie	k1gFnSc7	studie
Jeffrey	Jeffrea	k1gFnSc2	Jeffrea
Katzenberg	Katzenberg	k1gInSc1	Katzenberg
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
o	o	k7c6	o
akvizici	akvizice	k1gFnSc6	akvizice
studia	studio	k1gNnSc2	studio
Miramax	Miramax	k1gInSc4	Miramax
Films	Filmsa	k1gFnPc2	Filmsa
<g/>
.	.	kIx.	.
</s>
<s>
Wells	Wells	k1gInSc1	Wells
(	(	kIx(	(
<g/>
prezident	prezident	k1gMnSc1	prezident
<g/>
)	)	kIx)	)
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
při	při	k7c6	při
letecké	letecký	k2eAgFnSc6d1	letecká
nehodě	nehoda	k1gFnSc6	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
Katzenberg	Katzenberg	k1gMnSc1	Katzenberg
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
konkurenční	konkurenční	k2eAgNnSc4d1	konkurenční
studio	studio	k1gNnSc4	studio
pro	pro	k7c4	pro
Walt	Walt	k1gInSc4	Walt
Disney	Disnea	k1gFnSc2	Disnea
Pictures	Pictures	k1gInSc4	Pictures
s	s	k7c7	s
názvem	název	k1gInSc7	název
Dreamworks	Dreamworksa	k1gFnPc2	Dreamworksa
SKG	SKG	kA	SKG
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Eisner	Eisner	k1gMnSc1	Eisner
ho	on	k3xPp3gInSc4	on
nejmenoval	jmenovat	k5eNaImAgMnS	jmenovat
na	na	k7c4	na
nyní	nyní	k6eAd1	nyní
uvolněné	uvolněný	k2eAgNnSc4d1	uvolněné
Wellsovo	Wellsův	k2eAgNnSc4d1	Wellsův
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
Katzenberg	Katzenberg	k1gMnSc1	Katzenberg
soudil	soudit	k5eAaImAgMnS	soudit
kvůli	kvůli	k7c3	kvůli
podmínkám	podmínka	k1gFnPc3	podmínka
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
smlouvě	smlouva	k1gFnSc6	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
Eisner	Eisner	k1gMnSc1	Eisner
raději	rád	k6eAd2	rád
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
prezidenta	prezident	k1gMnSc2	prezident
dosadil	dosadit	k5eAaPmAgMnS	dosadit
svého	svůj	k3xOyFgMnSc4	svůj
kamaráda	kamarád	k1gMnSc4	kamarád
Michaela	Michael	k1gMnSc4	Michael
Ovitze	Ovitza	k1gFnSc6	Ovitza
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
Creative	Creativ	k1gInSc5	Creativ
Artists	Artistsa	k1gFnPc2	Artistsa
Agency	Agenc	k2eAgFnPc4d1	Agenc
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
s	s	k7c7	s
minimální	minimální	k2eAgFnSc7d1	minimální
podporou	podpora	k1gFnSc7	podpora
od	od	k7c2	od
představenstva	představenstvo	k1gNnSc2	představenstvo
(	(	kIx(	(
<g/>
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgMnS	být
i	i	k9	i
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
výherce	výherce	k1gMnSc1	výherce
Oscara	Oscara	k1gFnSc1	Oscara
<g/>
,	,	kIx,	,
Sidney	Sidnea	k1gFnPc1	Sidnea
Poitier	Poitier	k1gMnSc1	Poitier
<g/>
,	,	kIx,	,
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
Hilton	Hilton	k1gInSc4	Hilton
Hotels	Hotels	k1gInSc1	Hotels
Corporation	Corporation	k1gInSc1	Corporation
Stephen	Stephen	k2eAgInSc1d1	Stephen
Bollenbach	Bollenbach	k1gInSc1	Bollenbach
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
americký	americký	k2eAgMnSc1d1	americký
senátor	senátor	k1gMnSc1	senátor
George	George	k1gNnSc2	George
Mitchell	Mitchell	k1gMnSc1	Mitchell
<g/>
,	,	kIx,	,
děkan	děkan	k1gMnSc1	děkan
univerzity	univerzita	k1gFnSc2	univerzita
Yale	Yale	k1gFnSc1	Yale
Robert	Robert	k1gMnSc1	Robert
A.	A.	kA	A.
M.	M.	kA	M.
Stern	sternum	k1gNnPc2	sternum
a	a	k8xC	a
Eisnerovi	Eisnerův	k2eAgMnPc1d1	Eisnerův
předchůdci	předchůdce	k1gMnPc1	předchůdce
Raymond	Raymond	k1gMnSc1	Raymond
Watson	Watson	k1gMnSc1	Watson
a	a	k8xC	a
Card	Card	k1gMnSc1	Card
Walker	Walker	k1gMnSc1	Walker
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ovitzovo	Ovitzův	k2eAgNnSc1d1	Ovitzův
působení	působení	k1gNnSc1	působení
v	v	k7c6	v
roli	role	k1gFnSc6	role
prezidenta	prezident	k1gMnSc2	prezident
trvalo	trvat	k5eAaImAgNnS	trvat
pouhých	pouhý	k2eAgInPc2d1	pouhý
14	[number]	k4	14
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
Disneyho	Disney	k1gMnSc2	Disney
opustil	opustit	k5eAaPmAgMnS	opustit
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1996	[number]	k4	1996
kvůli	kvůli	k7c3	kvůli
"	"	kIx"	"
<g/>
no	no	k9	no
fault	fault	k2eAgInSc1d1	fault
termination	termination	k1gInSc1	termination
<g/>
"	"	kIx"	"
s	s	k7c7	s
odstupným	odstupné	k1gNnSc7	odstupné
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
$	$	kIx~	$
<g/>
38	[number]	k4	38
milionů	milion	k4xCgInPc2	milion
v	v	k7c6	v
hotovosti	hotovost	k1gFnSc6	hotovost
a	a	k8xC	a
vydáním	vydání	k1gNnSc7	vydání
tří	tři	k4xCgNnPc2	tři
milionů	milion	k4xCgInPc2	milion
akcií	akcie	k1gFnPc2	akcie
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
s	s	k7c7	s
hodnotou	hodnota	k1gFnSc7	hodnota
zhruba	zhruba	k6eAd1	zhruba
$	$	kIx~	$
<g/>
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Ovitzova	Ovitzův	k2eAgFnSc1d1	Ovitzův
epizoda	epizoda	k1gFnSc1	epizoda
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
dlouho	dlouho	k6eAd1	dlouho
trvající	trvající	k2eAgInSc4d1	trvající
neoriginální	originální	k2eNgInSc4d1	neoriginální
soudní	soudní	k2eAgInSc4d1	soudní
spor	spor	k1gInSc4	spor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
definitivně	definitivně	k6eAd1	definitivně
vyřešen	vyřešit	k5eAaPmNgInS	vyřešit
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
o	o	k7c4	o
10	[number]	k4	10
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
delawarského	delawarský	k2eAgInSc2d1	delawarský
soudu	soud	k1gInSc2	soud
William	William	k1gInSc1	William
B.	B.	kA	B.
Chandler	Chandler	k1gInSc1	Chandler
<g/>
,	,	kIx,	,
III	III	kA	III
<g/>
,	,	kIx,	,
popisuje	popisovat	k5eAaImIp3nS	popisovat
Eisnerovo	Eisnerův	k2eAgNnSc4d1	Eisnerovo
chování	chování	k1gNnSc4	chování
jako	jako	k9	jako
takové	takový	k3xDgNnSc1	takový
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
"	"	kIx"	"
<g/>
spadá	spadat	k5eAaImIp3nS	spadat
daleko	daleko	k6eAd1	daleko
za	za	k7c4	za
meze	mez	k1gFnPc4	mez
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
akcionáři	akcionář	k1gMnPc1	akcionář
očekávají	očekávat	k5eAaImIp3nP	očekávat
a	a	k8xC	a
požadují	požadovat	k5eAaImIp3nP	požadovat
od	od	k7c2	od
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
byla	být	k5eAaImAgFnS	být
svěřena	svěřen	k2eAgFnSc1d1	svěřena
takováto	takovýto	k3xDgFnSc1	takovýto
důvěrná	důvěrný	k2eAgFnSc1d1	důvěrná
funkce	funkce	k1gFnSc1	funkce
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
Eisner	Eisner	k1gInSc4	Eisner
a	a	k8xC	a
zbytek	zbytek	k1gInSc4	zbytek
Disneyho	Disney	k1gMnSc2	Disney
představenstva	představenstvo	k1gNnSc2	představenstvo
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
rozsudku	rozsudek	k1gInSc2	rozsudek
neprovinili	provinit	k5eNaPmAgMnP	provinit
proti	proti	k7c3	proti
liteře	litera	k1gFnSc3	litera
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Eisner	Eisner	k1gMnSc1	Eisner
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
pokusil	pokusit	k5eAaPmAgMnS	pokusit
od	od	k7c2	od
GE	GE	kA	GE
koupit	koupit	k5eAaPmF	koupit
NBC	NBC	kA	NBC
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nákup	nákup	k1gInSc1	nákup
se	se	k3xPyFc4	se
nezdařil	zdařit	k5eNaPmAgInS	zdařit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
GE	GE	kA	GE
si	se	k3xPyFc3	se
chtělo	chtít	k5eAaImAgNnS	chtít
udržet	udržet	k5eAaPmF	udržet
51	[number]	k4	51
%	%	kIx~	%
podíl	podíl	k1gInSc1	podíl
ve	v	k7c6	v
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Disneymu	Disneym	k1gInSc3	Disneym
se	se	k3xPyFc4	se
však	však	k9	však
během	během	k7c2	během
desetiletí	desetiletí	k1gNnSc2	desetiletí
podařilo	podařit	k5eAaPmAgNnS	podařit
koupit	koupit	k5eAaPmF	koupit
jiné	jiný	k2eAgInPc4d1	jiný
mediální	mediální	k2eAgInPc4d1	mediální
zdroje	zdroj	k1gInPc4	zdroj
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
American	Americany	k1gInPc2	Americany
Broadcasting	Broadcasting	k1gInSc4	Broadcasting
Company	Compana	k1gFnSc2	Compana
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
také	také	k9	také
A	A	kA	A
<g/>
&	&	k?	&
<g/>
E	E	kA	E
Television	Television	k1gInSc1	Television
Networks	Networks	k1gInSc1	Networks
and	and	k?	and
síť	síť	k1gFnSc4	síť
ESPN	ESPN	kA	ESPN
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
začal	začít	k5eAaPmAgInS	začít
Disney	Disnea	k1gFnSc2	Disnea
působit	působit	k5eAaImF	působit
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
internetu	internet	k1gInSc2	internet
nákupem	nákup	k1gInSc7	nákup
Starwave	Starwav	k1gInSc5	Starwav
a	a	k8xC	a
43	[number]	k4	43
%	%	kIx~	%
Infoseek	Infoseky	k1gFnPc2	Infoseky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
Disney	Disnea	k1gFnSc2	Disnea
nakoupil	nakoupit	k5eAaPmAgMnS	nakoupit
zbývající	zbývající	k2eAgFnPc4d1	zbývající
akcie	akcie	k1gFnPc4	akcie
Infoseeku	Infoseek	k1gInSc2	Infoseek
a	a	k8xC	a
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
spustil	spustit	k5eAaPmAgInS	spustit
internetový	internetový	k2eAgInSc1d1	internetový
portál	portál	k1gInSc1	portál
Go	Go	k1gFnSc2	Go
<g/>
.	.	kIx.	.
</s>
<s>
Disney	Disne	k1gMnPc4	Disne
rovněž	rovněž	k9	rovněž
začal	začít	k5eAaPmAgMnS	začít
provozovat	provozovat	k5eAaImF	provozovat
svoji	svůj	k3xOyFgFnSc4	svůj
lodní	lodní	k2eAgFnSc4d1	lodní
výletní	výletní	k2eAgFnSc4d1	výletní
linku	linka	k1gFnSc4	linka
pokřtěním	pokřtění	k1gNnSc7	pokřtění
lodě	loď	k1gFnSc2	loď
Disney	Disnea	k1gFnSc2	Disnea
Magic	Magice	k1gFnPc2	Magice
a	a	k8xC	a
její	její	k3xOp3gFnPc1	její
sestry	sestra	k1gFnPc1	sestra
<g/>
,	,	kIx,	,
Disney	Disney	k1gInPc1	Disney
Wonder	Wondra	k1gFnPc2	Wondra
<g/>
.	.	kIx.	.
</s>
<s>
Disney	Disney	k1gInPc1	Disney
pod	pod	k7c4	pod
svoje	své	k1gNnSc4	své
křídla	křídlo	k1gNnSc2	křídlo
získal	získat	k5eAaPmAgInS	získat
rovněž	rovněž	k9	rovněž
dva	dva	k4xCgInPc4	dva
profesionální	profesionální	k2eAgInPc4d1	profesionální
sportovní	sportovní	k2eAgInPc4d1	sportovní
týmy	tým	k1gInPc4	tým
<g/>
,	,	kIx,	,
Mighty	Mighta	k1gFnPc4	Mighta
Ducks	Ducksa	k1gFnPc2	Ducksa
of	of	k?	of
Anaheim	Anaheima	k1gFnPc2	Anaheima
a	a	k8xC	a
Anaheim	Anaheima	k1gFnPc2	Anaheima
Angels	Angelsa	k1gFnPc2	Angelsa
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
také	také	k9	také
vlekl	vleknout	k5eAaImAgInS	vleknout
Katzenbergův	Katzenbergův	k2eAgInSc1d1	Katzenbergův
případ	případ	k1gInSc1	případ
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
smlouva	smlouva	k1gFnSc1	smlouva
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
část	část	k1gFnSc4	část
příjmů	příjem	k1gInPc2	příjem
z	z	k7c2	z
finančního	finanční	k2eAgInSc2d1	finanční
obratu	obrat	k1gInSc2	obrat
filmů	film	k1gInPc2	film
z	z	k7c2	z
vedlejších	vedlejší	k2eAgInPc2d1	vedlejší
trhů	trh	k1gInPc2	trh
navždy	navždy	k6eAd1	navždy
<g/>
.	.	kIx.	.
</s>
<s>
Katzenberg	Katzenberg	k1gMnSc1	Katzenberg
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
$	$	kIx~	$
<g/>
100	[number]	k4	100
k	k	k7c3	k
urovnání	urovnání	k1gNnSc3	urovnání
případu	případ	k1gInSc2	případ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Eisnerovi	Eisnerův	k2eAgMnPc1d1	Eisnerův
připadala	připadat	k5eAaImAgFnS	připadat
jeho	jeho	k3xOp3gFnSc4	jeho
žádost	žádost	k1gFnSc4	žádost
na	na	k7c4	na
půl	půl	k1xP	půl
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
příliš	příliš	k6eAd1	příliš
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
však	však	k9	však
nalezen	nalezen	k2eAgInSc1d1	nalezen
dodatek	dodatek	k1gInSc1	dodatek
o	o	k7c6	o
vedlejších	vedlejší	k2eAgInPc6d1	vedlejší
trzích	trh	k1gInPc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
Disneyho	Disneyze	k6eAd1	Disneyze
právníci	právník	k1gMnPc1	právník
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
naznačovat	naznačovat	k5eAaImF	naznačovat
úpadek	úpadek	k1gInSc4	úpadek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
společnost	společnost	k1gFnSc4	společnost
postihl	postihnout	k5eAaPmAgInS	postihnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
díky	díky	k7c3	díky
které	který	k3yIgFnSc3	který
se	se	k3xPyFc4	se
přišlo	přijít	k5eAaPmAgNnS	přijít
na	na	k7c4	na
některé	některý	k3yIgInPc4	některý
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
ABC	ABC	kA	ABC
klesalo	klesat	k5eAaImAgNnS	klesat
hodnocení	hodnocení	k1gNnSc1	hodnocení
a	a	k8xC	a
zvyšovaly	zvyšovat	k5eAaImAgFnP	zvyšovat
se	se	k3xPyFc4	se
náklady	náklad	k1gInPc7	náklad
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
filmový	filmový	k2eAgInSc1d1	filmový
segment	segment	k1gInSc1	segment
zažil	zažít	k5eAaPmAgMnS	zažít
dva	dva	k4xCgInPc4	dva
fatální	fatální	k2eAgInPc4d1	fatální
propady	propad	k1gInPc4	propad
<g/>
.	.	kIx.	.
</s>
<s>
Žádná	žádný	k3yNgFnSc1	žádný
strana	strana	k1gFnSc1	strana
ukončeného	ukončený	k2eAgInSc2d1	ukončený
soudního	soudní	k2eAgInSc2d1	soudní
sporu	spor	k1gInSc2	spor
neprozradila	prozradit	k5eNaPmAgFnS	prozradit
vypořádací	vypořádací	k2eAgFnSc4d1	vypořádací
částku	částka	k1gFnSc4	částka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odhad	odhad	k1gInSc1	odhad
činí	činit	k5eAaImIp3nS	činit
$	$	kIx~	$
<g/>
200	[number]	k4	200
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Eisnerův	Eisnerův	k2eAgInSc1d1	Eisnerův
plachý	plachý	k2eAgInSc1d1	plachý
styl	styl	k1gInSc1	styl
řízení	řízení	k1gNnSc2	řízení
společnosti	společnost	k1gFnSc2	společnost
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
řadu	řada	k1gFnSc4	řada
kritiky	kritika	k1gFnSc2	kritika
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ostatní	ostatní	k2eAgMnPc1d1	ostatní
odborníci	odborník	k1gMnPc1	odborník
na	na	k7c4	na
zábavní	zábavní	k2eAgInSc4d1	zábavní
průmysl	průmysl	k1gInSc4	průmysl
tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
"	"	kIx"	"
<g/>
věkové	věkový	k2eAgNnSc4d1	věkové
stěsnání	stěsnání	k1gNnSc4	stěsnání
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
pozice	pozice	k1gFnSc2	pozice
podniku	podnik	k1gInSc2	podnik
na	na	k7c6	na
cílovém	cílový	k2eAgInSc6d1	cílový
trhu	trh	k1gInSc6	trh
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
děti	dítě	k1gFnPc1	dítě
se	se	k3xPyFc4	se
začínají	začínat	k5eAaImIp3nP	začínat
chovat	chovat	k5eAaImF	chovat
jako	jako	k9	jako
teenageři	teenager	k1gMnPc1	teenager
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
200	[number]	k4	200
přinesl	přinést	k5eAaPmAgInS	přinést
zvýšení	zvýšení	k1gNnSc4	zvýšení
obratu	obrat	k1gInSc2	obrat
o	o	k7c4	o
9	[number]	k4	9
%	%	kIx~	%
a	a	k8xC	a
čistého	čistý	k2eAgInSc2d1	čistý
příjmu	příjem	k1gInSc2	příjem
o	o	k7c4	o
39	[number]	k4	39
%	%	kIx~	%
s	s	k7c7	s
ABC	ABC	kA	ABC
a	a	k8xC	a
ESPN	ESPN	kA	ESPN
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
<g/>
,	,	kIx,	,
a	a	k8xC	a
parky	park	k1gInPc1	park
a	a	k8xC	a
resorty	resort	k1gInPc1	resort
hned	hned	k6eAd1	hned
v	v	k7c6	v
závěsu	závěs	k1gInSc6	závěs
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zažívaly	zažívat	k5eAaImAgFnP	zažívat
už	už	k9	už
šestý	šestý	k4xOgInSc4	šestý
rok	rok	k1gInSc4	rok
růstu	růst	k1gInSc2	růst
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
teroristické	teroristický	k2eAgInPc4d1	teroristický
útoky	útok	k1gInPc4	útok
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2001	[number]	k4	2001
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
plošnému	plošný	k2eAgInSc3d1	plošný
útlumu	útlum	k1gInSc3	útlum
dovolených	dovolená	k1gFnPc2	dovolená
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
a	a	k8xC	a
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
recesi	recese	k1gFnSc3	recese
<g/>
.	.	kIx.	.
</s>
<s>
Recese	recese	k1gFnSc1	recese
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
obratu	obrat	k1gInSc2	obrat
ABC	ABC	kA	ABC
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
Eisner	Eisner	k1gMnSc1	Eisner
dovedl	dovést	k5eAaPmAgMnS	dovést
společnost	společnost	k1gFnSc4	společnost
k	k	k7c3	k
drahému	drahý	k2eAgInSc3d1	drahý
nákupu	nákup	k1gInSc3	nákup
sítě	síť	k1gFnSc2	síť
Fox	fox	k1gInSc1	fox
Family	Famila	k1gFnSc2	Famila
Worldwide	Worldwid	k1gInSc5	Worldwid
od	od	k7c2	od
News	Newsa	k1gFnPc2	Newsa
Corporation	Corporation	k1gInSc1	Corporation
a	a	k8xC	a
Haima	Haima	k1gFnSc1	Haima
Sabana	Sabana	k1gFnSc1	Sabana
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
2001	[number]	k4	2001
byl	být	k5eAaImAgInS	být
rokem	rok	k1gInSc7	rok
snižování	snižování	k1gNnSc2	snižování
stavů	stav	k1gInPc2	stav
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
k	k	k7c3	k
propuštění	propuštění	k1gNnSc3	propuštění
4000	[number]	k4	4000
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
,	,	kIx,	,
omezení	omezení	k1gNnSc4	omezení
provozu	provoz	k1gInSc2	provoz
Disneylandů	Disneyland	k1gInPc2	Disneyland
osekání	osekání	k1gNnSc1	osekání
investic	investice	k1gFnPc2	investice
pro	pro	k7c4	pro
hrané	hraný	k2eAgInPc4d1	hraný
filmy	film	k1gInPc4	film
a	a	k8xC	a
k	k	k7c3	k
minimalizaci	minimalizace	k1gFnSc3	minimalizace
aktivit	aktivita	k1gFnPc2	aktivita
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
obrat	obrat	k1gInSc1	obrat
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
mírně	mírně	k6eAd1	mírně
snížil	snížit	k5eAaPmAgInS	snížit
<g/>
,	,	kIx,	,
čistý	čistý	k2eAgInSc1d1	čistý
zisk	zisk	k1gInSc1	zisk
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
na	na	k7c6	na
$	$	kIx~	$
<g/>
1,2	[number]	k4	1,2
miliard	miliarda	k4xCgFnPc2	miliarda
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
filmovými	filmový	k2eAgInPc7d1	filmový
hity	hit	k1gInPc7	hit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
studio	studio	k1gNnSc1	studio
prvním	první	k4xOgMnPc3	první
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
překročilo	překročit	k5eAaPmAgNnS	překročit
$	$	kIx~	$
<g/>
3	[number]	k4	3
miliardy	miliarda	k4xCgFnSc2	miliarda
v	v	k7c6	v
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
.	.	kIx.	.
</s>
<s>
Eisner	Eisner	k1gMnSc1	Eisner
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
představenstvo	představenstvo	k1gNnSc1	představenstvo
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
opětovnému	opětovný	k2eAgNnSc3d1	opětovné
jmenování	jmenování	k1gNnSc6	jmenování
Roye	Roy	k1gMnSc2	Roy
E.	E.	kA	E.
Disneyho	Disney	k1gMnSc2	Disney
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc2	syn
spoluzakladatele	spoluzakladatel	k1gMnSc2	spoluzakladatel
společnosti	společnost	k1gFnSc2	společnost
Roye	Roy	k1gMnSc2	Roy
O.	O.	kA	O.
Disneyho	Disney	k1gMnSc2	Disney
<g/>
,	,	kIx,	,
předsedou	předseda	k1gMnSc7	předseda
představenstva	představenstvo	k1gNnSc2	představenstvo
<g/>
;	;	kIx,	;
jako	jako	k8xC	jako
záminku	záminka	k1gFnSc4	záminka
si	se	k3xPyFc3	se
našel	najít	k5eAaPmAgInS	najít
jeho	jeho	k3xOp3gInSc1	jeho
věk	věk	k1gInSc1	věk
(	(	kIx(	(
<g/>
72	[number]	k4	72
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
odcházelo	odcházet	k5eAaImAgNnS	odcházet
do	do	k7c2	do
důchodu	důchod	k1gInSc2	důchod
<g/>
.	.	kIx.	.
</s>
<s>
Stanley	Stanlea	k1gMnSc2	Stanlea
Gold	Gold	k1gMnSc1	Gold
pohrozil	pohrozit	k5eAaPmAgMnS	pohrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
odejde	odejít	k5eAaPmIp3nS	odejít
z	z	k7c2	z
představenstva	představenstvo	k1gNnSc2	představenstvo
a	a	k8xC	a
přiměje	přimět	k5eAaPmIp3nS	přimět
ostatní	ostatní	k2eAgMnPc4d1	ostatní
členy	člen	k1gMnPc4	člen
k	k	k7c3	k
sesazení	sesazení	k1gNnSc3	sesazení
Eisnera	Eisner	k1gMnSc2	Eisner
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
Disney	Disnea	k1gFnSc2	Disnea
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
funkce	funkce	k1gFnSc2	funkce
firemního	firemní	k2eAgMnSc2d1	firemní
zástupce	zástupce	k1gMnSc2	zástupce
předsedy	předseda	k1gMnSc2	předseda
a	a	k8xC	a
předsedy	předseda	k1gMnSc2	předseda
Walt	Walta	k1gFnPc2	Walta
Disney	Disnea	k1gMnSc2	Disnea
Feature	Featur	k1gMnSc5	Featur
Animation	Animation	k1gInSc4	Animation
a	a	k8xC	a
obvinil	obvinit	k5eAaPmAgMnS	obvinit
Eisnera	Eisner	k1gMnSc4	Eisner
z	z	k7c2	z
micromanagementu	micromanagement	k1gInSc2	micromanagement
<g/>
,	,	kIx,	,
propadu	propad	k1gInSc2	propad
televizí	televize	k1gFnPc2	televize
ABC	ABC	kA	ABC
u	u	k7c2	u
diváků	divák	k1gMnPc2	divák
<g/>
,	,	kIx,	,
rozpačitosti	rozpačitost	k1gFnPc1	rozpačitost
v	v	k7c6	v
byznysu	byznys	k1gInSc6	byznys
týkajícího	týkající	k2eAgMnSc2d1	týkající
se	se	k3xPyFc4	se
zábavních	zábavní	k2eAgInPc2d1	zábavní
parků	park	k1gInPc2	park
<g/>
,	,	kIx,	,
změnu	změna	k1gFnSc4	změna
Walt	Walt	k1gInSc4	Walt
Disney	Disnea	k1gFnSc2	Disnea
Company	Compana	k1gFnSc2	Compana
v	v	k7c4	v
"	"	kIx"	"
<g/>
chamtivou	chamtivý	k2eAgFnSc4d1	chamtivá
<g/>
,	,	kIx,	,
málo	málo	k6eAd1	málo
morální	morální	k2eAgFnSc4d1	morální
<g/>
"	"	kIx"	"
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
odmítal	odmítat	k5eAaImAgMnS	odmítat
zřízení	zřízení	k1gNnSc4	zřízení
jasného	jasný	k2eAgInSc2d1	jasný
nástupnického	nástupnický	k2eAgInSc2d1	nástupnický
plánu	plán	k1gInSc2	plán
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
kasovních	kasovní	k2eAgInPc2d1	kasovní
propadáků	propadák	k1gInPc2	propadák
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
Pixar	Pixara	k1gFnPc2	Pixara
Animation	Animation	k1gInSc1	Animation
Studios	Studios	k?	Studios
po	po	k7c6	po
dvanáctiletém	dvanáctiletý	k2eAgInSc6d1	dvanáctiletý
kontraktu	kontrakt	k1gInSc6	kontrakt
s	s	k7c7	s
Disneym	Disneym	k1gInSc1	Disneym
začaly	začít	k5eAaPmAgInP	začít
poohlížet	poohlížet	k5eAaImF	poohlížet
po	po	k7c6	po
jiném	jiný	k2eAgMnSc6d1	jiný
distributorovi	distributor	k1gMnSc6	distributor
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
Disneym	Disneymum	k1gNnPc2	Disneymum
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
problematice	problematika	k1gFnSc3	problematika
kontroly	kontrola	k1gFnSc2	kontrola
a	a	k8xC	a
peněz	peníze	k1gInPc2	peníze
s	s	k7c7	s
Eisnerem	Eisner	k1gMnSc7	Eisner
napnuly	napnout	k5eAaPmAgInP	napnout
<g/>
.	.	kIx.	.
</s>
<s>
Comcast	Comcast	k1gFnSc1	Comcast
Corporation	Corporation	k1gInSc4	Corporation
podala	podat	k5eAaPmAgFnS	podat
nevyžádanou	vyžádaný	k2eNgFnSc4d1	nevyžádaná
nabídku	nabídka	k1gFnSc4	nabídka
$	$	kIx~	$
<g/>
54	[number]	k4	54
miliard	miliarda	k4xCgFnPc2	miliarda
k	k	k7c3	k
akvizici	akvizice	k1gFnSc3	akvizice
The	The	k1gFnSc2	The
Walt	Walta	k1gFnPc2	Walta
Disney	Disnea	k1gFnSc2	Disnea
Company	Compana	k1gFnSc2	Compana
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
filmů	film	k1gInPc2	film
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
rozpočtem	rozpočet	k1gInSc7	rozpočet
finančně	finančně	k6eAd1	finančně
propadla	propadnout	k5eAaPmAgFnS	propadnout
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
těmito	tento	k3xDgInPc7	tento
problémy	problém	k1gInPc7	problém
a	a	k8xC	a
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
nespokojených	spokojený	k2eNgMnPc2d1	nespokojený
členů	člen	k1gMnPc2	člen
představenstva	představenstvo	k1gNnSc2	představenstvo
Eisner	Eisnra	k1gFnPc2	Eisnra
postoupil	postoupit	k5eAaPmAgMnS	postoupit
funkci	funkce	k1gFnSc4	funkce
předsedy	předseda	k1gMnSc2	předseda
představenstva	představenstvo	k1gNnSc2	představenstvo
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2004	[number]	k4	2004
se	se	k3xPyFc4	se
na	na	k7c4	na
Disneyho	Disney	k1gMnSc4	Disney
výročním	výroční	k2eAgFnPc3d1	výroční
setkání	setkání	k1gNnSc6	setkání
akcionářů	akcionář	k1gMnPc2	akcionář
45	[number]	k4	45
%	%	kIx~	%
Disneyho	Disney	k1gMnSc4	Disney
akcionářů	akcionář	k1gMnPc2	akcionář
<g/>
,	,	kIx,	,
shromážděných	shromážděný	k2eAgInPc2d1	shromážděný
převážně	převážně	k6eAd1	převážně
Stanley	Stanle	k1gMnPc7	Stanle
Goldem	Gold	k1gMnSc7	Gold
a	a	k8xC	a
Royem	Roy	k1gMnSc7	Roy
E.	E.	kA	E.
Disneym	Disneym	k1gInSc4	Disneym
<g/>
,	,	kIx,	,
vyslovilo	vyslovit	k5eAaPmAgNnS	vyslovit
proti	proti	k7c3	proti
znovuzvolení	znovuzvolení	k1gNnSc3	znovuzvolení
Eisnera	Eisner	k1gMnSc2	Eisner
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
představenstva	představenstvo	k1gNnSc2	představenstvo
<g/>
.	.	kIx.	.
</s>
<s>
Disneyho	Disney	k1gMnSc2	Disney
představenstvo	představenstvo	k1gNnSc1	představenstvo
poté	poté	k6eAd1	poté
zvolilo	zvolit	k5eAaPmAgNnS	zvolit
jako	jako	k9	jako
svého	svůj	k3xOyFgMnSc4	svůj
předsedu	předseda	k1gMnSc4	předseda
George	Georg	k1gMnSc4	Georg
Mitchella	Mitchell	k1gMnSc4	Mitchell
a	a	k8xC	a
okamžitě	okamžitě	k6eAd1	okamžitě
sesadilo	sesadit	k5eAaPmAgNnS	sesadit
Eisnera	Eisner	k1gMnSc4	Eisner
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
(	(	kIx(	(
<g/>
CEO	CEO	kA	CEO
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2005	[number]	k4	2005
byl	být	k5eAaImAgInS	být
jako	jako	k9	jako
Eisnerův	Eisnerův	k2eAgMnSc1d1	Eisnerův
nástupce	nástupce	k1gMnSc1	nástupce
jako	jako	k8xS	jako
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
oznámen	oznámen	k2eAgMnSc1d1	oznámen
Bob	Bob	k1gMnSc1	Bob
Iger	Iger	k1gMnSc1	Iger
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
Eisner	Eisnra	k1gFnPc2	Eisnra
rezignoval	rezignovat	k5eAaBmAgInS	rezignovat
na	na	k7c4	na
funkci	funkce	k1gFnSc4	funkce
manažera	manažer	k1gMnSc2	manažer
a	a	k8xC	a
člena	člen	k1gMnSc2	člen
představenstva	představenstvo	k1gNnSc2	představenstvo
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2005	[number]	k4	2005
se	se	k3xPyFc4	se
synovec	synovec	k1gMnSc1	synovec
Walta	Walt	k1gMnSc4	Walt
Disneyho	Disney	k1gMnSc4	Disney
<g/>
,	,	kIx,	,
Roy	Roy	k1gMnSc1	Roy
E.	E.	kA	E.
Disney	Disnea	k1gFnPc1	Disnea
<g/>
,	,	kIx,	,
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
The	The	k1gMnSc2	The
Walt	Walta	k1gFnPc2	Walta
Disney	Disnea	k1gMnSc2	Disnea
Company	Compana	k1gFnSc2	Compana
jako	jako	k8xS	jako
konzultant	konzultant	k1gMnSc1	konzultant
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
mu	on	k3xPp3gMnSc3	on
udělen	udělen	k2eAgInSc4d1	udělen
titul	titul	k1gInSc4	titul
emeritního	emeritní	k2eAgMnSc2d1	emeritní
ředitele	ředitel	k1gMnSc2	ředitel
bez	bez	k7c2	bez
hlasovacího	hlasovací	k2eAgNnSc2d1	hlasovací
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Divize	divize	k1gFnSc1	divize
Walt	Walta	k1gFnPc2	Walta
Disney	Disnea	k1gFnSc2	Disnea
Parks	Parks	k1gInSc1	Parks
and	and	k?	and
Resorts	Resorts	k1gInSc1	Resorts
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
oslavila	oslavit	k5eAaPmAgFnS	oslavit
50	[number]	k4	50
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
vzniku	vznik	k1gInSc2	vznik
parku	park	k1gInSc2	park
Disneyland	Disneyland	k1gInSc4	Disneyland
Resort	resort	k1gInSc1	resort
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
se	se	k3xPyFc4	se
otevřel	otevřít	k5eAaPmAgInS	otevřít
Hong	Hong	k1gInSc1	Hong
Kong	Kongo	k1gNnPc2	Kongo
Disneyland	Disneyland	k1gInSc4	Disneyland
v	v	k7c6	v
Hongkongu	Hongkong	k1gInSc6	Hongkong
<g/>
,	,	kIx,	,
bývalé	bývalý	k2eAgFnSc3d1	bývalá
britské	britský	k2eAgFnSc3d1	britská
kolonii	kolonie	k1gFnSc3	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Walt	Walt	k1gMnSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
Studios	Studios	k?	Studios
Motion	Motion	k1gInSc1	Motion
Pictures	Picturesa	k1gFnPc2	Picturesa
ohlásili	ohlásit	k5eAaPmAgMnP	ohlásit
film	film	k1gInSc4	film
Strašpytlík	strašpytlík	k1gMnSc1	strašpytlík
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
Disneyho	Disneyha	k1gFnSc5	Disneyha
film	film	k1gInSc4	film
vytvořený	vytvořený	k2eAgInSc4d1	vytvořený
pomocí	pomocí	k7c2	pomocí
3D	[number]	k4	3D
technologie	technologie	k1gFnPc1	technologie
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Bob	Bob	k1gMnSc1	Bob
Iger	Iger	k1gMnSc1	Iger
na	na	k7c6	na
pozici	pozice	k1gFnSc6	pozice
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
(	(	kIx(	(
<g/>
CEO	CEO	kA	CEO
<g/>
)	)	kIx)	)
Michaela	Michael	k1gMnSc2	Michael
Eisnera	Eisner	k1gMnSc2	Eisner
<g/>
.	.	kIx.	.
</s>
<s>
Spoluzakladatelé	spoluzakladatel	k1gMnPc1	spoluzakladatel
studií	studie	k1gFnPc2	studie
Miramax	Miramax	k1gInSc1	Miramax
Bob	Bob	k1gMnSc1	Bob
Weinstein	Weinsteina	k1gFnPc2	Weinsteina
a	a	k8xC	a
Harvey	Harvea	k1gFnSc2	Harvea
Weinstein	Weinsteina	k1gFnPc2	Weinsteina
společnost	společnost	k1gFnSc4	společnost
také	také	k6eAd1	také
opustili	opustit	k5eAaPmAgMnP	opustit
a	a	k8xC	a
vydali	vydat	k5eAaPmAgMnP	vydat
se	se	k3xPyFc4	se
budovat	budovat	k5eAaImF	budovat
nové	nový	k2eAgNnSc4d1	nové
vlastní	vlastní	k2eAgNnSc4d1	vlastní
studio	studio	k1gNnSc4	studio
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2005	[number]	k4	2005
Disney	Disnea	k1gFnSc2	Disnea
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2006	[number]	k4	2006
uzavře	uzavřít	k5eAaPmIp3nS	uzavřít
DisneyToon	DisneyToon	k1gNnSc4	DisneyToon
Studios	Studios	k?	Studios
Australia	Australium	k1gNnSc2	Australium
po	po	k7c6	po
jejich	jejich	k3xOp3gNnPc6	jejich
sedmnácti	sedmnáct	k4xCc6	sedmnáct
letech	léto	k1gNnPc6	léto
existence	existence	k1gFnSc2	existence
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
po	po	k7c6	po
několikaletých	několikaletý	k2eAgInPc6d1	několikaletý
sporech	spor	k1gInPc6	spor
Disney	Disnea	k1gFnSc2	Disnea
konečně	konečně	k9	konečně
koupil	koupit	k5eAaPmAgMnS	koupit
zpátky	zpátky	k6eAd1	zpátky
postavu	postava	k1gFnSc4	postava
Oswald	Oswaldo	k1gNnPc2	Oswaldo
the	the	k?	the
Lucky	Lucka	k1gFnSc2	Lucka
Rabbit	Rabbit	k1gMnSc2	Rabbit
<g/>
,	,	kIx,	,
předchůdce	předchůdce	k1gMnSc2	předchůdce
Mickey	Mickea	k1gMnSc2	Mickea
Mouse	Mous	k1gMnSc2	Mous
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vědomím	vědomí	k1gNnSc7	vědomí
<g/>
,	,	kIx,	,
že	že	k8xS	že
Disneyho	Disney	k1gMnSc2	Disney
vztah	vztah	k1gInSc4	vztah
ke	k	k7c3	k
studiím	studie	k1gFnPc3	studie
Pixar	Pixara	k1gFnPc2	Pixara
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
úzký	úzký	k2eAgInSc1d1	úzký
<g/>
,	,	kIx,	,
zahájil	zahájit	k5eAaPmAgMnS	zahájit
prezident	prezident	k1gMnSc1	prezident
a	a	k8xC	a
CEO	CEO	kA	CEO
Bob	bob	k1gInSc4	bob
Iger	Igera	k1gFnPc2	Igera
jednání	jednání	k1gNnSc1	jednání
s	s	k7c7	s
vedením	vedení	k1gNnSc7	vedení
Pixar	Pixar	k1gInSc1	Pixar
Animation	Animation	k1gInSc1	Animation
Studios	Studios	k?	Studios
<g/>
,	,	kIx,	,
Stevem	Steve	k1gMnSc7	Steve
Jobsem	Jobs	k1gMnSc7	Jobs
a	a	k8xC	a
Edem	Edem	k?	Edem
Catmullem	Catmull	k1gInSc7	Catmull
o	o	k7c6	o
případné	případný	k2eAgFnSc6d1	případná
budoucí	budoucí	k2eAgFnSc6d1	budoucí
fúzi	fúze	k1gFnSc6	fúze
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2006	[number]	k4	2006
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Disney	Disnea	k1gFnPc1	Disnea
za	za	k7c2	za
Pixar	Pixara	k1gFnPc2	Pixara
zaplatí	zaplatit	k5eAaPmIp3nS	zaplatit
vydáním	vydání	k1gNnSc7	vydání
akcií	akcie	k1gFnPc2	akcie
v	v	k7c6	v
celkové	celkový	k2eAgFnSc6d1	celková
hodnotě	hodnota	k1gFnSc6	hodnota
$	$	kIx~	$
<g/>
7.4	[number]	k4	7.4
miliard	miliarda	k4xCgFnPc2	miliarda
<g/>
.	.	kIx.	.
</s>
<s>
Obchod	obchod	k1gInSc1	obchod
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
a	a	k8xC	a
mezi	mezi	k7c4	mezi
nejpozoruhodnější	pozoruhodný	k2eAgInSc4d3	nejpozoruhodnější
výsledek	výsledek	k1gInSc4	výsledek
tohoto	tento	k3xDgInSc2	tento
nákupu	nákup	k1gInSc2	nákup
patří	patřit	k5eAaImIp3nS	patřit
Steve	Steve	k1gMnSc1	Steve
Jobs	Jobs	k1gInSc1	Jobs
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
CEO	CEO	kA	CEO
Pixaru	Pixar	k1gInSc2	Pixar
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gInSc3	jeho
prodeji	prodej	k1gInSc3	prodej
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
podílem	podíl	k1gInSc7	podíl
50,1	[number]	k4	50,1
<g/>
%	%	kIx~	%
akcií	akcie	k1gFnPc2	akcie
většinovým	většinový	k2eAgMnSc7d1	většinový
podílníkem	podílník	k1gMnSc7	podílník
v	v	k7c6	v
The	The	k1gFnSc6	The
Walt	Walta	k1gFnPc2	Walta
Disney	Disnea	k1gFnSc2	Disnea
Company	Compana	k1gFnSc2	Compana
a	a	k8xC	a
členem	člen	k1gInSc7	člen
představenstva	představenstvo	k1gNnSc2	představenstvo
<g/>
.	.	kIx.	.
</s>
<s>
Ed	Ed	k?	Ed
Catmull	Catmull	k1gInSc1	Catmull
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgInS	ujmout
role	role	k1gFnSc2	role
prezidenta	prezident	k1gMnSc2	prezident
Pixar	Pixara	k1gFnPc2	Pixara
Animation	Animation	k1gInSc4	Animation
Studios	Studios	k?	Studios
<g/>
.	.	kIx.	.
</s>
<s>
Bývalý	bývalý	k2eAgMnSc1d1	bývalý
výkonný	výkonný	k2eAgMnSc1d1	výkonný
viceprezident	viceprezident	k1gMnSc1	viceprezident
Pixaru	Pixar	k1gInSc2	Pixar
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Lasseter	Lasseter	k1gMnSc1	Lasseter
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
šéfem	šéf	k1gMnSc7	šéf
(	(	kIx(	(
<g/>
CCO	CCO	kA	CCO
<g/>
)	)	kIx)	)
jak	jak	k8xC	jak
Walt	Walt	k1gMnSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
Animation	Animation	k1gInSc1	Animation
Studios	Studios	k?	Studios
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
Pixar	Pixar	k1gInSc1	Pixar
Animation	Animation	k1gInSc1	Animation
Studios	Studios	k?	Studios
<g/>
,	,	kIx,	,
také	také	k9	také
se	se	k3xPyFc4	se
zhostil	zhostit	k5eAaPmAgInS	zhostit
funkce	funkce	k1gFnSc2	funkce
Principal	Principal	k1gMnSc2	Principal
Creative	Creativ	k1gInSc5	Creativ
Advisor	Advisora	k1gFnPc2	Advisora
ve	v	k7c4	v
Walt	Walt	k1gInSc4	Walt
Disney	Disnea	k1gFnSc2	Disnea
Imagineering	Imagineering	k1gInSc1	Imagineering
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dlouhém	dlouhý	k2eAgNnSc6d1	dlouhé
pracovním	pracovní	k2eAgNnSc6d1	pracovní
zapojení	zapojení	k1gNnSc6	zapojení
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
jako	jako	k8xC	jako
výkonný	výkonný	k2eAgMnSc1d1	výkonný
vedoucí	vedoucí	k1gMnSc1	vedoucí
a	a	k8xC	a
velký	velký	k2eAgMnSc1d1	velký
akcionář	akcionář	k1gMnSc1	akcionář
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
emeritní	emeritní	k2eAgMnSc1d1	emeritní
ředitel	ředitel	k1gMnSc1	ředitel
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Roy	Roy	k1gMnSc1	Roy
E.	E.	kA	E.
Disney	Disnea	k1gFnSc2	Disnea
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2009	[number]	k4	2009
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
žaludku	žaludek	k1gInSc2	žaludek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
zhruba	zhruba	k6eAd1	zhruba
1	[number]	k4	1
%	%	kIx~	%
ze	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
akcií	akcie	k1gFnPc2	akcie
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
činil	činit	k5eAaImAgInS	činit
téměř	téměř	k6eAd1	téměř
16	[number]	k4	16
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
posledního	poslední	k2eAgMnSc4d1	poslední
člena	člen	k1gMnSc4	člen
Disneyho	Disney	k1gMnSc4	Disney
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
chodu	chod	k1gInSc6	chod
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
na	na	k7c4	na
práci	práce	k1gFnSc4	práce
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2009	[number]	k4	2009
Disney	Disnea	k1gFnSc2	Disnea
oznámil	oznámit	k5eAaPmAgInS	oznámit
nákup	nákup	k1gInSc1	nákup
společnosti	společnost	k1gFnSc2	společnost
Marvel	Marvel	k1gMnSc1	Marvel
Entertainment	Entertainment	k1gMnSc1	Entertainment
za	za	k7c2	za
rovných	rovný	k2eAgInPc2d1	rovný
$	$	kIx~	$
<g/>
4.24	[number]	k4	4.24
miliard	miliarda	k4xCgFnPc2	miliarda
<g/>
.	.	kIx.	.
</s>
<s>
Obchod	obchod	k1gInSc1	obchod
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Disney	Disney	k1gInPc1	Disney
nad	nad	k7c7	nad
podnikem	podnik	k1gInSc7	podnik
získal	získat	k5eAaPmAgMnS	získat
plnou	plný	k2eAgFnSc4d1	plná
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
Disney	Disne	k1gMnPc4	Disne
ubezpečil	ubezpečit	k5eAaPmAgMnS	ubezpečit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
nákup	nákup	k1gInSc1	nákup
nebude	být	k5eNaImBp3nS	být
mít	mít	k5eAaImF	mít
žádný	žádný	k3yNgInSc4	žádný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
produkty	produkt	k1gInPc4	produkt
Marvelu	Marvel	k1gInSc2	Marvel
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
nedotkne	dotknout	k5eNaPmIp3nS	dotknout
žádné	žádný	k3yNgNnSc1	žádný
z	z	k7c2	z
několika	několik	k4yIc2	několik
tisíců	tisíc	k4xCgInPc2	tisíc
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
na	na	k7c4	na
než	než	k8xS	než
nákupem	nákup	k1gInSc7	nákup
získal	získat	k5eAaPmAgMnS	získat
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2009	[number]	k4	2009
nahradil	nahradit	k5eAaPmAgMnS	nahradit
prezident	prezident	k1gMnSc1	prezident
Disney	Disnea	k1gFnSc2	Disnea
Channel	Channel	k1gMnSc1	Channel
Rich	Rich	k1gMnSc1	Rich
Ross	Ross	k1gInSc4	Ross
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
si	se	k3xPyFc3	se
přivedl	přivést	k5eAaPmAgInS	přivést
Iger	Iger	k1gInSc4	Iger
<g/>
,	,	kIx,	,
Dicka	Dicek	k1gMnSc4	Dicek
Cooka	Cooek	k1gMnSc4	Cooek
jako	jako	k8xS	jako
předsedu	předseda	k1gMnSc4	předseda
společnosti	společnost	k1gFnSc2	společnost
<g/>
;	;	kIx,	;
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
začala	začít	k5eAaPmAgFnS	začít
restrukturalizace	restrukturalizace	k1gFnSc1	restrukturalizace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obnášela	obnášet	k5eAaImAgFnS	obnášet
především	především	k6eAd1	především
větší	veliký	k2eAgFnSc4d2	veliký
orientaci	orientace	k1gFnSc4	orientace
na	na	k7c4	na
produkty	produkt	k1gInPc4	produkt
pro	pro	k7c4	pro
rodiny	rodina	k1gFnPc4	rodina
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2010	[number]	k4	2010
se	se	k3xPyFc4	se
Disney	Disnea	k1gFnSc2	Disnea
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
po	po	k7c6	po
útlumu	útlum	k1gInSc6	útlum
studia	studio	k1gNnSc2	studio
Touchstone	Touchston	k1gInSc5	Touchston
ukončit	ukončit	k5eAaPmF	ukončit
veškeré	veškerý	k3xTgFnPc4	veškerý
aktivity	aktivita	k1gFnPc4	aktivita
studií	studie	k1gFnPc2	studie
Miramax	Miramax	k1gInSc1	Miramax
<g/>
,	,	kIx,	,
a	a	k8xC	a
o	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
začal	začít	k5eAaPmAgInS	začít
prodávat	prodávat	k5eAaImF	prodávat
značky	značka	k1gFnPc4	značka
Miramaxu	Miramax	k1gInSc2	Miramax
a	a	k8xC	a
tituly	titul	k1gInPc4	titul
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
knihovny	knihovna	k1gFnSc2	knihovna
obnášející	obnášející	k2eAgMnPc1d1	obnášející
přes	přes	k7c4	přes
700	[number]	k4	700
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
skončila	skončit	k5eAaPmAgFnS	skončit
společnost	společnost	k1gFnSc1	společnost
Roberta	Robert	k1gMnSc2	Robert
Zemeckise	Zemeckise	k1gFnSc1	Zemeckise
<g/>
,	,	kIx,	,
ImageMovers	ImageMovers	k1gInSc1	ImageMovers
Digital	Digital	kA	Digital
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Disney	Disnea	k1gFnPc1	Disnea
koupil	koupit	k5eAaPmAgInS	koupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2010	[number]	k4	2010
ukončilo	ukončit	k5eAaPmAgNnS	ukončit
svoji	svůj	k3xOyFgFnSc4	svůj
aktivitu	aktivita	k1gFnSc4	aktivita
hudební	hudební	k2eAgNnSc1d1	hudební
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
Lyric	Lyric	k1gMnSc1	Lyric
Street	Street	k1gMnSc1	Street
Records	Records	k1gInSc4	Records
<g/>
,	,	kIx,	,
Disneyho	Disney	k1gMnSc4	Disney
countryové	countryový	k2eAgFnSc2d1	countryová
hudební	hudební	k2eAgFnSc2d1	hudební
studio	studio	k1gNnSc4	studio
v	v	k7c4	v
Nasvillu	Nasvilla	k1gFnSc4	Nasvilla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2010	[number]	k4	2010
společnost	společnost	k1gFnSc1	společnost
prodala	prodat	k5eAaPmAgFnS	prodat
značku	značka	k1gFnSc4	značka
Power	Powra	k1gFnPc2	Powra
Rangers	Rangersa	k1gFnPc2	Rangersa
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
jeho	jeho	k3xOp3gInPc7	jeho
700	[number]	k4	700
epizodami	epizoda	k1gFnPc7	epizoda
<g/>
,	,	kIx,	,
zpět	zpět	k6eAd1	zpět
Haimu	Haim	k1gMnSc3	Haim
Sabanovi	Saban	k1gMnSc3	Saban
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
se	se	k3xPyFc4	se
společnost	společnost	k1gFnSc1	společnost
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
ukončil	ukončit	k5eAaPmAgInS	ukončit
filmový	filmový	k2eAgInSc1d1	filmový
projekt	projekt	k1gInSc1	projekt
Jerryho	Jerry	k1gMnSc2	Jerry
Bruckheimera	Bruckheimera	k1gFnSc1	Bruckheimera
Killing	Killing	k1gInSc1	Killing
Rommel	Rommel	k1gInSc1	Rommel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2011	[number]	k4	2011
se	se	k3xPyFc4	se
snižovaly	snižovat	k5eAaImAgInP	snižovat
stavy	stav	k1gInPc1	stav
v	v	k7c6	v
divizi	divize	k1gFnSc6	divize
Disney	Disnea	k1gFnSc2	Disnea
Interactive	Interactiv	k1gInSc5	Interactiv
Studios	Studios	k?	Studios
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
Disney	Disnea	k1gMnSc2	Disnea
prodal	prodat	k5eAaPmAgMnS	prodat
dvě	dva	k4xCgFnPc4	dva
stanice	stanice	k1gFnPc4	stanice
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
ABC	ABC	kA	ABC
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
premiérou	premiéra	k1gFnSc7	premiéra
filmu	film	k1gInSc2	film
Na	na	k7c6	na
vlásku	vlásek	k1gInSc6	vlásek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
Ed	Ed	k1gMnSc1	Ed
Catmull	Catmull	k1gMnSc1	Catmull
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
princeznovský	princeznovský	k2eAgMnSc1d1	princeznovský
<g/>
"	"	kIx"	"
žánr	žánr	k1gInSc1	žánr
měl	mít	k5eAaImAgInS	mít
pauzu	pauza	k1gFnSc4	pauza
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
"	"	kIx"	"
<g/>
někdo	někdo	k3yInSc1	někdo
nepřišel	přijít	k5eNaPmAgMnS	přijít
a	a	k8xC	a
nerozjel	rozjet	k5eNaPmAgMnS	rozjet
to	ten	k3xDgNnSc4	ten
...	...	k?	...
ale	ale	k9	ale
nemá	mít	k5eNaImIp3nS	mít
žádné	žádný	k3yNgFnPc4	žádný
další	další	k2eAgFnPc4d1	další
postavy	postava	k1gFnPc4	postava
nebo	nebo	k8xC	nebo
příběh	příběh	k1gInSc4	příběh
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
bychom	by	kYmCp1nP	by
uvedli	uvést	k5eAaPmAgMnP	uvést
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
filmy	film	k1gInPc1	film
s	s	k7c7	s
princeznami	princezna	k1gFnPc7	princezna
omezit	omezit	k5eAaPmF	omezit
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
změny	změna	k1gFnSc2	změna
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
publika	publikum	k1gNnSc2	publikum
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
preferenci	preference	k1gFnSc4	preference
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
stránce	stránka	k1gFnSc6	stránka
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
Ed	Ed	k1gMnSc1	Ed
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
jen	jen	k9	jen
o	o	k7c4	o
drby	drb	k1gInPc4	drb
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2011	[number]	k4	2011
Disney	Disnea	k1gFnSc2	Disnea
připravil	připravit	k5eAaPmAgInS	připravit
pozemek	pozemek	k1gInSc4	pozemek
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
Shangai	Shanga	k1gFnSc2	Shanga
Disney	Disnea	k1gFnSc2	Disnea
Resort	resort	k1gInSc1	resort
<g/>
.	.	kIx.	.
</s>
<s>
Obří	obří	k2eAgNnSc1d1	obří
letovisko	letovisko	k1gNnSc1	letovisko
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
stavba	stavba	k1gFnSc1	stavba
stála	stát	k5eAaImAgFnS	stát
Disneyho	Disney	k1gMnSc4	Disney
$	$	kIx~	$
<g/>
4.4	[number]	k4	4.4
miliard	miliarda	k4xCgFnPc2	miliarda
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
podle	podle	k7c2	podle
plánu	plán	k1gInSc2	plán
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
Bob	Bob	k1gMnSc1	Bob
Iger	Iger	k1gMnSc1	Iger
na	na	k7c6	na
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
úspěšných	úspěšný	k2eAgInPc6d1	úspěšný
nákupech	nákup	k1gInPc6	nákup
Pixaru	Pixar	k1gInSc2	Pixar
a	a	k8xC	a
Marvelu	Marvel	k1gInSc2	Marvel
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
on	on	k3xPp3gMnSc1	on
a	a	k8xC	a
Walt	Walt	k2eAgMnSc1d1	Walt
Disney	Disne	k2eAgInPc4d1	Disne
Company	Compan	k1gInPc4	Compan
dívají	dívat	k5eAaImIp3nP	dívat
po	po	k7c6	po
"	"	kIx"	"
<g/>
další	další	k2eAgFnSc6d1	další
koupi	koupě	k1gFnSc6	koupě
buďto	buďto	k8xC	buďto
nových	nový	k2eAgFnPc2d1	nová
postav	postava	k1gFnPc2	postava
nebo	nebo	k8xC	nebo
podniků	podnik	k1gInPc2	podnik
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgInPc1d1	schopen
vytvářet	vytvářet	k5eAaImF	vytvářet
skvělé	skvělý	k2eAgInPc4d1	skvělý
postavy	postav	k1gInPc4	postav
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
skvělé	skvělý	k2eAgInPc1d1	skvělý
příběhy	příběh	k1gInPc1	příběh
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Za	za	k7c4	za
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
Disney	Disnea	k1gFnSc2	Disnea
dokončil	dokončit	k5eAaPmAgMnS	dokončit
akvizici	akvizice	k1gFnSc4	akvizice
společnosti	společnost	k1gFnSc2	společnost
UTV	UTV	kA	UTV
Software	software	k1gInSc1	software
Communications	Communications	k1gInSc1	Communications
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
které	který	k3yQgFnSc3	který
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
své	svůj	k3xOyFgNnSc4	svůj
pole	pole	k1gNnSc4	pole
působnosti	působnost	k1gFnSc2	působnost
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
Disney	Disnea	k1gFnSc2	Disnea
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
plánuje	plánovat	k5eAaImIp3nS	plánovat
koupit	koupit	k5eAaPmF	koupit
Lucasfilm	Lucasfilm	k1gInSc1	Lucasfilm
za	za	k7c2	za
$	$	kIx~	$
<g/>
4.05	[number]	k4	4.05
miliard	miliarda	k4xCgFnPc2	miliarda
s	s	k7c7	s
plánem	plán	k1gInSc7	plán
na	na	k7c4	na
natočení	natočení	k1gNnSc4	natočení
Star	star	k1gFnSc2	star
Wars	Warsa	k1gFnPc2	Warsa
<g/>
:	:	kIx,	:
Epizoda	epizoda	k1gFnSc1	epizoda
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
mít	mít	k5eAaImF	mít
premiéru	premiéra	k1gFnSc4	premiéra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
byla	být	k5eAaImAgFnS	být
fúze	fúze	k1gFnSc1	fúze
Disney-Lucasfilm	Disney-Lucasfilma	k1gFnPc2	Disney-Lucasfilma
schválena	schválit	k5eAaPmNgFnS	schválit
Federální	federální	k2eAgFnSc7d1	federální
obchodní	obchodní	k2eAgFnSc7d1	obchodní
komisí	komise	k1gFnSc7	komise
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
rovněž	rovněž	k9	rovněž
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
akvizice	akvizice	k1gFnSc1	akvizice
může	moct	k5eAaImIp3nS	moct
proběhnout	proběhnout	k5eAaPmF	proběhnout
bez	bez	k7c2	bez
řešení	řešení	k1gNnSc2	řešení
antimonopolních	antimonopolní	k2eAgInPc2d1	antimonopolní
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
byl	být	k5eAaImAgInS	být
obchod	obchod	k1gInSc1	obchod
dokončen	dokončit	k5eAaPmNgInS	dokončit
a	a	k8xC	a
Lucasfilm	Lucasfilm	k1gInSc1	Lucasfilm
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
plně	plně	k6eAd1	plně
vlastněnou	vlastněný	k2eAgFnSc7d1	vlastněná
dceřinou	dceřin	k2eAgFnSc7d1	dceřina
společností	společnost	k1gFnSc7	společnost
The	The	k1gMnSc2	The
Walt	Walta	k1gFnPc2	Walta
Disney	Disnea	k1gMnSc2	Disnea
Company	Compana	k1gFnSc2	Compana
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2015	[number]	k4	2015
společnost	společnost	k1gFnSc1	společnost
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
pozici	pozice	k1gFnSc6	pozice
chief	chief	k1gInSc4	chief
operating	operating	k1gInSc1	operating
officer	officer	k1gInSc1	officer
(	(	kIx(	(
<g/>
COO	COO	kA	COO
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
jmenován	jmenován	k2eAgMnSc1d1	jmenován
Tom	Tom	k1gMnSc1	Tom
Staggs	Staggsa	k1gFnPc2	Staggsa
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
The	The	k1gFnSc6	The
Walt	Walta	k1gFnPc2	Walta
Disney	Disnea	k1gFnSc2	Disnea
Company	Compana	k1gFnSc2	Compana
pracuje	pracovat	k5eAaImIp3nS	pracovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
a	a	k8xC	a
sehrál	sehrát	k5eAaPmAgInS	sehrát
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
roli	role	k1gFnSc4	role
při	při	k7c6	při
akvizici	akvizice	k1gFnSc6	akvizice
Marvelu	Marvel	k1gInSc2	Marvel
<g/>
.	.	kIx.	.
</s>
<s>
ESPN	ESPN	kA	ESPN
Disney-ABC	Disney-ABC	k1gFnSc1	Disney-ABC
Television	Television	k1gInSc1	Television
Group	Group	k1gMnSc1	Group
ABC	ABC	kA	ABC
Entertainment	Entertainment	k1gMnSc1	Entertainment
Group	Group	k1gMnSc1	Group
ABC	ABC	kA	ABC
News	News	k1gInSc1	News
ABC	ABC	kA	ABC
Owned	Owned	k1gInSc1	Owned
Television	Television	k1gInSc1	Television
Stations	Stations	k1gInSc1	Stations
Group	Group	k1gInSc1	Group
ABC	ABC	kA	ABC
Family	Famil	k1gInPc1	Famil
Disney	Disnea	k1gFnSc2	Disnea
Channels	Channelsa	k1gFnPc2	Channelsa
Worldwide	Worldwid	k1gInSc5	Worldwid
Hyperion	Hyperion	k1gInSc4	Hyperion
Books	Books	k1gInSc1	Books
Disneyland	Disneyland	k1gInSc1	Disneyland
Resort	resort	k1gInSc1	resort
Walt	Walt	k1gInSc4	Walt
Disney	Disnea	k1gFnSc2	Disnea
World	Worlda	k1gFnPc2	Worlda
Resort	resort	k1gInSc1	resort
Tokyo	Tokyo	k1gMnSc1	Tokyo
Disney	Disnea	k1gFnSc2	Disnea
Resort	resort	k1gInSc1	resort
Disneyland	Disneyland	k1gInSc1	Disneyland
Paris	Paris	k1gMnSc1	Paris
Hong	Hong	k1gMnSc1	Hong
Kong	Kongo	k1gNnPc2	Kongo
Disneyland	Disneyland	k1gInSc1	Disneyland
Shangai	Shangai	k1gNnSc6	Shangai
Disney	Disnea	k1gFnSc2	Disnea
Resort	resort	k1gInSc4	resort
Disney	Disnea	k1gFnSc2	Disnea
Cruise	Cruise	k1gFnSc1	Cruise
Line	linout	k5eAaImIp3nS	linout
Disney	Disne	k1gMnPc4	Disne
Vacation	Vacation	k1gInSc1	Vacation
Club	club	k1gInSc1	club
Aulani	Aulaň	k1gFnSc3	Aulaň
<g/>
,	,	kIx,	,
a	a	k8xC	a
Disney	Disnea	k1gFnPc1	Disnea
Resort	resort	k1gInSc1	resort
&	&	k?	&
Spa	Spa	k1gFnPc1	Spa
Adventures	Adventuresa	k1gFnPc2	Adventuresa
by	by	kYmCp3nP	by
Disney	Disnea	k1gFnPc1	Disnea
Walt	Walta	k1gFnPc2	Walta
Disney	Disnea	k1gFnSc2	Disnea
Imagineering	Imagineering	k1gInSc1	Imagineering
Související	související	k2eAgFnSc2d1	související
informace	informace	k1gFnSc2	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
The	The	k1gFnSc2	The
Walt	Walta	k1gFnPc2	Walta
Disney	Disnea	k1gFnSc2	Disnea
Studios	Studios	k?	Studios
<g/>
.	.	kIx.	.
</s>
<s>
Walt	Walt	k1gMnSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
Studios	Studios	k?	Studios
Motion	Motion	k1gInSc1	Motion
Pictures	Pictures	k1gMnSc1	Pictures
Marvel	Marvel	k1gMnSc1	Marvel
Studios	Studios	k?	Studios
Touchstone	Touchston	k1gInSc5	Touchston
Pictures	Pictures	k1gMnSc1	Pictures
Disneynature	Disneynatur	k1gMnSc5	Disneynatur
Lucasfilm	Lucasfilmo	k1gNnPc2	Lucasfilmo
Walt	Walt	k1gMnSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
Animation	Animation	k1gInSc1	Animation
Studios	Studios	k?	Studios
Pixar	Pixar	k1gInSc1	Pixar
Animation	Animation	k1gInSc1	Animation
Studios	Studios	k?	Studios
Disney	Disnea	k1gFnSc2	Disnea
Music	Musice	k1gFnPc2	Musice
Group	Group	k1gMnSc1	Group
Disney	Disnea	k1gFnSc2	Disnea
Theatrical	Theatrical	k1gMnSc1	Theatrical
Group	Group	k1gMnSc1	Group
Disney	Disnea	k1gFnSc2	Disnea
Licensing	Licensing	k1gInSc1	Licensing
Disney	Disnea	k1gFnSc2	Disnea
Publishing	Publishing	k1gInSc1	Publishing
Worldwide	Worldwid	k1gInSc5	Worldwid
Disney	Disney	k1gInPc1	Disney
Store	Stor	k1gInSc5	Stor
Disney	Disney	k1gInPc1	Disney
Interactive	Interactiv	k1gInSc5	Interactiv
Media	medium	k1gNnPc1	medium
Disney	Disney	k1gInPc1	Disney
Interactive	Interactiv	k1gInSc5	Interactiv
Games	Games	k1gInSc1	Games
1923	[number]	k4	1923
<g/>
-	-	kIx~	-
<g/>
1966	[number]	k4	1966
<g/>
:	:	kIx,	:
Walt	Walt	k1gInSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
1966	[number]	k4	1966
<g/>
-	-	kIx~	-
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1971	[number]	k4	1971
<g/>
:	:	kIx,	:
Roy	Roy	k1gMnSc1	Roy
O.	O.	kA	O.
Disney	Disnea	k1gFnSc2	Disnea
1968	[number]	k4	1968
<g/>
-	-	kIx~	-
<g/>
1972	[number]	k4	1972
<g/>
:	:	kIx,	:
Donn	donna	k1gFnPc2	donna
Tatum	Tatum	k?	Tatum
1971	[number]	k4	1971
<g/>
-	-	kIx~	-
<g/>
1977	[number]	k4	1977
<g/>
:	:	kIx,	:
Card	Carda	k1gFnPc2	Carda
Walker	Walkero	k1gNnPc2	Walkero
1980	[number]	k4	1980
<g/>
-	-	kIx~	-
<g/>
1984	[number]	k4	1984
<g/>
:	:	kIx,	:
Ron	Ron	k1gMnSc1	Ron
W.	W.	kA	W.
Miller	Miller	k1gMnSc1	Miller
1984	[number]	k4	1984
<g/>
-	-	kIx~	-
<g/>
1994	[number]	k4	1994
<g/>
:	:	kIx,	:
Frank	Frank	k1gMnSc1	Frank
Wells	Wells	k1gInSc4	Wells
1995	[number]	k4	1995
<g/>
-	-	kIx~	-
<g/>
1997	[number]	k4	1997
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Michael	Michael	k1gMnSc1	Michael
Ovitz	Ovitz	k1gMnSc1	Ovitz
2000	[number]	k4	2000
<g/>
-	-	kIx~	-
<g/>
2012	[number]	k4	2012
<g/>
:	:	kIx,	:
Robert	Robert	k1gMnSc1	Robert
Iger	Iger	k1gMnSc1	Iger
1929	[number]	k4	1929
<g/>
-	-	kIx~	-
<g/>
1971	[number]	k4	1971
<g/>
:	:	kIx,	:
Roy	Roy	k1gMnSc1	Roy
O.	O.	kA	O.
Disney	Disnea	k1gFnSc2	Disnea
1971	[number]	k4	1971
<g/>
-	-	kIx~	-
<g/>
1976	[number]	k4	1976
<g/>
:	:	kIx,	:
Donn	donna	k1gFnPc2	donna
Tatum	Tatum	k?	Tatum
1976	[number]	k4	1976
<g/>
-	-	kIx~	-
<g/>
1983	[number]	k4	1983
<g/>
:	:	kIx,	:
Card	Carda	k1gFnPc2	Carda
Walker	Walkero	k1gNnPc2	Walkero
1983	[number]	k4	1983
<g/>
-	-	kIx~	-
<g/>
1984	[number]	k4	1984
<g/>
:	:	kIx,	:
Ron	Ron	k1gMnSc1	Ron
W.	W.	kA	W.
Miller	Miller	k1gMnSc1	Miller
1984	[number]	k4	1984
<g/>
-	-	kIx~	-
<g/>
2005	[number]	k4	2005
<g/>
:	:	kIx,	:
Michael	Michael	k1gMnSc1	Michael
Eisner	Eisner	k1gMnSc1	Eisner
2005	[number]	k4	2005
<g/>
-	-	kIx~	-
<g/>
nyní	nyní	k6eAd1	nyní
<g/>
:	:	kIx,	:
Robert	Robert	k1gMnSc1	Robert
Iger	Iger	k1gMnSc1	Iger
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
Walt	Walta	k1gFnPc2	Walta
a	a	k8xC	a
Roy	Roy	k1gMnSc1	Roy
Disney	Disnea	k1gFnSc2	Disnea
pozici	pozice	k1gFnSc4	pozice
předsedy	předseda	k1gMnSc2	předseda
představenstva	představenstvo	k1gNnSc2	představenstvo
sdíleli	sdílet	k5eAaImAgMnP	sdílet
<g/>
.	.	kIx.	.
</s>
<s>
Walt	Walt	k1gMnSc1	Walt
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
vzdal	vzdát	k5eAaPmAgMnS	vzdát
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
více	hodně	k6eAd2	hodně
zaměřovat	zaměřovat	k5eAaImF	zaměřovat
na	na	k7c4	na
kreativní	kreativní	k2eAgInPc4d1	kreativní
aspekty	aspekt	k1gInPc4	aspekt
směřování	směřování	k1gNnSc4	směřování
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Roy	Roy	k1gMnSc1	Roy
O.	O.	kA	O.
Disney	Disnea	k1gFnSc2	Disnea
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
jako	jako	k9	jako
předseda	předseda	k1gMnSc1	předseda
představenstva	představenstvo	k1gNnSc2	představenstvo
a	a	k8xC	a
CEO	CEO	kA	CEO
<g/>
.	.	kIx.	.
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
1960	[number]	k4	1960
<g/>
:	:	kIx,	:
Walt	Walt	k1gInSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
1971	[number]	k4	1971
<g/>
:	:	kIx,	:
Roy	Roy	k1gMnSc1	Roy
O.	O.	kA	O.
Disney	Disney	k1gInPc4	Disney
(	(	kIx(	(
<g/>
sdílel	sdílet	k5eAaImAgMnS	sdílet
pozici	pozice	k1gFnSc4	pozice
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
1971	[number]	k4	1971
<g/>
-	-	kIx~	-
<g/>
1980	[number]	k4	1980
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Donn	donna	k1gFnPc2	donna
Tatum	Tatum	k?	Tatum
1980	[number]	k4	1980
<g/>
-	-	kIx~	-
<g/>
1983	[number]	k4	1983
<g/>
:	:	kIx,	:
Card	Carda	k1gFnPc2	Carda
Walker	Walkero	k1gNnPc2	Walkero
1983	[number]	k4	1983
<g/>
-	-	kIx~	-
<g/>
1984	[number]	k4	1984
<g/>
:	:	kIx,	:
Raymond	Raymond	k1gInSc1	Raymond
Watson	Watson	k1gNnSc1	Watson
1984	[number]	k4	1984
<g/>
-	-	kIx~	-
<g/>
2004	[number]	k4	2004
<g/>
:	:	kIx,	:
Michael	Michael	k1gMnSc1	Michael
Eisner	Eisner	k1gMnSc1	Eisner
2004	[number]	k4	2004
<g/>
-	-	kIx~	-
<g/>
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
George	George	k1gInSc1	George
J.	J.	kA	J.
Mitchell	Mitchell	k1gInSc4	Mitchell
2007	[number]	k4	2007
<g/>
-	-	kIx~	-
<g/>
2012	[number]	k4	2012
<g/>
:	:	kIx,	:
John	John	k1gMnSc1	John
E.	E.	kA	E.
Pepper	Pepper	k1gMnSc1	Pepper
mladší	mladý	k2eAgMnSc1d2	mladší
<g />
.	.	kIx.	.
</s>
<s>
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
nyní	nyní	k6eAd1	nyní
<g/>
:	:	kIx,	:
Robert	Robert	k1gMnSc1	Robert
Iger	Iger	k1gMnSc1	Iger
1984	[number]	k4	1984
<g/>
-	-	kIx~	-
<g/>
2003	[number]	k4	2003
<g/>
:	:	kIx,	:
Roy	Roy	k1gMnSc1	Roy
E.	E.	kA	E.
Disney	Disnea	k1gFnSc2	Disnea
1999	[number]	k4	1999
<g/>
-	-	kIx~	-
<g/>
2000	[number]	k4	2000
<g/>
:	:	kIx,	:
Sanford	Sanford	k1gMnSc1	Sanford
Litvack	Litvack	k1gMnSc1	Litvack
(	(	kIx(	(
<g/>
sdílel	sdílet	k5eAaImAgInS	sdílet
pozici	pozice	k1gFnSc4	pozice
<g/>
)	)	kIx)	)
1984	[number]	k4	1984
<g/>
-	-	kIx~	-
<g/>
1994	[number]	k4	1994
<g/>
:	:	kIx,	:
Frank	Frank	k1gMnSc1	Frank
Wells	Wells	k1gInSc4	Wells
1997	[number]	k4	1997
<g/>
-	-	kIx~	-
<g/>
1999	[number]	k4	1999
<g/>
:	:	kIx,	:
Sanford	Sanforda	k1gFnPc2	Sanforda
Litvack	Litvacko	k1gNnPc2	Litvacko
2000	[number]	k4	2000
<g/>
-	-	kIx~	-
<g/>
2005	[number]	k4	2005
<g/>
:	:	kIx,	:
Robert	Robert	k1gMnSc1	Robert
Iger	Iger	k1gMnSc1	Iger
Disney	Disnea	k1gMnSc2	Disnea
Channel	Channel	k1gMnSc1	Channel
Disney	Disnea	k1gMnSc2	Disnea
XD	XD	kA	XD
Disney	Disnea	k1gMnSc2	Disnea
Junior	junior	k1gMnSc1	junior
Disney	Disnea	k1gMnSc2	Disnea
Cinemagic	Cinemagic	k1gMnSc1	Cinemagic
Mickey	Mickea	k1gMnSc2	Mickea
Mouse	Mouse	k1gFnSc2	Mouse
Touchstone	Touchston	k1gInSc5	Touchston
Pictures	Picturesa	k1gFnPc2	Picturesa
Lucasfilm	Lucasfilm	k1gMnSc1	Lucasfilm
Pixar	Pixar	k1gMnSc1	Pixar
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
The	The	k1gFnSc2	The
Walt	Walta	k1gFnPc2	Walta
Disney	Disnea	k1gFnSc2	Disnea
Company	Compana	k1gFnSc2	Compana
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
The	The	k1gFnSc2	The
Walt	Walta	k1gFnPc2	Walta
Disney	Disnea	k1gFnSc2	Disnea
Company	Compan	k1gMnPc4	Compan
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Historie	historie	k1gFnSc1	historie
v	v	k7c6	v
datech	datum	k1gNnPc6	datum
s	s	k7c7	s
obrázky	obrázek	k1gInPc4	obrázek
</s>
