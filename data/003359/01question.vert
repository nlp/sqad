<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
baskytarista	baskytarista	k1gMnSc1	baskytarista
skupiny	skupina	k1gFnSc2	skupina
Slayer	Slayer	k1gMnSc1	Slayer
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Kerry	Kerr	k1gMnPc7	Kerr
Kingem	King	k1gMnSc7	King
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejproduktivnějších	produktivní	k2eAgFnPc2d3	nejproduktivnější
autorských	autorský	k2eAgFnPc2d1	autorská
dvojic	dvojice	k1gFnPc2	dvojice
na	na	k7c6	na
metalové	metalový	k2eAgFnSc6d1	metalová
scéně	scéna	k1gFnSc6	scéna
<g/>
?	?	kIx.	?
</s>
