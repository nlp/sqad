<p>
<s>
Tatranský	tatranský	k2eAgInSc1d1	tatranský
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
Tatranský	tatranský	k2eAgInSc1d1	tatranský
národný	národný	k2eAgInSc1d1	národný
park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
TANAP	TANAP	kA	TANAP
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc1d3	nejstarší
z	z	k7c2	z
devíti	devět	k4xCc2	devět
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Tatry	Tatra	k1gFnSc2	Tatra
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Slovenska	Slovensko	k1gNnSc2	Slovensko
při	při	k7c6	při
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
straně	strana	k1gFnSc6	strana
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
navazuje	navazovat	k5eAaImIp3nS	navazovat
menší	malý	k2eAgFnSc3d2	menší
Tatrzański	Tatrzańsk	k1gFnSc3	Tatrzańsk
Park	park	k1gInSc4	park
Narodowy	Narodowa	k1gFnSc2	Narodowa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
738	[number]	k4	738
km2	km2	k4	km2
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
ochranné	ochranný	k2eAgNnSc1d1	ochranné
pásmo	pásmo	k1gNnSc1	pásmo
zabírá	zabírat	k5eAaImIp3nS	zabírat
307,03	[number]	k4	307,03
km2	km2	k4	km2
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
dohromady	dohromady	k6eAd1	dohromady
1045,03	[number]	k4	1045,03
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parku	park	k1gInSc6	park
je	být	k5eAaImIp3nS	být
celkem	celkem	k6eAd1	celkem
600	[number]	k4	600
km	km	kA	km
turistických	turistický	k2eAgFnPc2d1	turistická
stezek	stezka	k1gFnPc2	stezka
a	a	k8xC	a
16	[number]	k4	16
značených	značený	k2eAgFnPc2d1	značená
a	a	k8xC	a
udržovaných	udržovaný	k2eAgFnPc2d1	udržovaná
cyklistických	cyklistický	k2eAgFnPc2d1	cyklistická
tras	trasa	k1gFnPc2	trasa
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nejvyšší	vysoký	k2eAgInPc4d3	Nejvyšší
vrcholy	vrchol	k1gInPc4	vrchol
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgInPc4d3	Nejvyšší
je	být	k5eAaImIp3nS	být
Gerlachovský	Gerlachovský	k2eAgInSc1d1	Gerlachovský
štít	štít	k1gInSc1	štít
(	(	kIx(	(
<g/>
2655	[number]	k4	2655
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Park	park	k1gInSc1	park
je	být	k5eAaImIp3nS	být
významný	významný	k2eAgInSc1d1	významný
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
rozmanitou	rozmanitý	k2eAgFnSc4d1	rozmanitá
faunu	fauna	k1gFnSc4	fauna
a	a	k8xC	a
flóru	flóra	k1gFnSc4	flóra
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
endemity	endemit	k1gInPc7	endemit
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
tatranských	tatranský	k2eAgMnPc2d1	tatranský
kamzíků	kamzík	k1gMnPc2	kamzík
a	a	k8xC	a
svišťů	svišť	k1gMnPc2	svišť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
parku	park	k1gInSc2	park
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
historie	historie	k1gFnSc2	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Park	park	k1gInSc1	park
byl	být	k5eAaImAgInS	být
zřízen	zřídit	k5eAaPmNgInS	zřídit
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejstarším	starý	k2eAgInSc7d3	nejstarší
národním	národní	k2eAgInSc7d1	národní
parkem	park	k1gInSc7	park
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
byly	být	k5eAaImAgFnP	být
ke	k	k7c3	k
stávajícímu	stávající	k2eAgInSc3d1	stávající
parku	park	k1gInSc2	park
přidruženy	přidružen	k2eAgFnPc1d1	přidružena
Západní	západní	k2eAgFnPc1d1	západní
Tatry	Tatra	k1gFnPc1	Tatra
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
TANAP	TANAP	kA	TANAP
biosférickou	biosférický	k2eAgFnSc7d1	biosférická
rezervací	rezervace	k1gFnSc7	rezervace
UNESCO	Unesco	k1gNnSc1	Unesco
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
byly	být	k5eAaImAgFnP	být
upraveny	upravit	k5eAaPmNgFnP	upravit
hranice	hranice	k1gFnPc1	hranice
území	území	k1gNnSc2	území
parku	park	k1gInSc2	park
a	a	k8xC	a
ochranného	ochranný	k2eAgNnSc2d1	ochranné
pásma	pásmo	k1gNnSc2	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
je	být	k5eAaImIp3nS	být
park	park	k1gInSc1	park
součástí	součást	k1gFnPc2	součást
sítě	síť	k1gFnSc2	síť
evropsky	evropsky	k6eAd1	evropsky
významných	významný	k2eAgFnPc2d1	významná
lokalit	lokalita	k1gFnPc2	lokalita
Natura	Natura	k1gFnSc1	Natura
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
oba	dva	k4xCgInPc4	dva
nejvyšší	vysoký	k2eAgInPc4d3	Nejvyšší
podcelky	podcelek	k1gInPc4	podcelek
Tater	Tatra	k1gFnPc2	Tatra
<g/>
,	,	kIx,	,
Západní	západní	k2eAgFnSc2d1	západní
i	i	k8xC	i
Východní	východní	k2eAgFnSc2d1	východní
Tatry	Tatra	k1gFnSc2	Tatra
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnPc1d1	západní
Tatry	Tatra	k1gFnPc1	Tatra
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
Osobitou	osobitý	k2eAgFnSc4d1	osobitá
<g/>
,	,	kIx,	,
Roháče	roháč	k1gInPc1	roháč
<g/>
,	,	kIx,	,
Sivý	sivý	k2eAgInSc1d1	sivý
vrch	vrch	k1gInSc1	vrch
<g/>
,	,	kIx,	,
Liptovské	liptovský	k2eAgFnPc1d1	Liptovská
Tatry	Tatra	k1gFnPc1	Tatra
<g/>
,	,	kIx,	,
Liptovské	liptovský	k2eAgFnPc1d1	Liptovská
Kopy	kopa	k1gFnPc1	kopa
a	a	k8xC	a
Červené	Červené	k2eAgInPc1d1	Červené
vrchy	vrch	k1gInPc1	vrch
<g/>
,	,	kIx,	,
Východní	východní	k2eAgFnPc1d1	východní
Tatry	Tatra	k1gFnPc1	Tatra
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
Vysokých	vysoký	k2eAgFnPc2d1	vysoká
Tater	Tatra	k1gFnPc2	Tatra
a	a	k8xC	a
Belianských	Belianský	k2eAgFnPc2d1	Belianská
Tater	Tatra	k1gFnPc2	Tatra
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
Tatranského	tatranský	k2eAgInSc2d1	tatranský
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Žilinském	žilinský	k2eAgInSc6d1	žilinský
kraji	kraj	k1gInSc6	kraj
a	a	k8xC	a
východní	východní	k2eAgFnSc4d1	východní
část	část	k1gFnSc4	část
v	v	k7c6	v
Prešovském	prešovský	k2eAgInSc6d1	prešovský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
parku	park	k1gInSc6	park
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sto	sto	k4xCgNnSc4	sto
horských	horský	k2eAgFnPc2d1	horská
ples	pleso	k1gNnPc2	pleso
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
je	být	k5eAaImIp3nS	být
Veľké	Veľký	k2eAgNnSc1d1	Veľké
Hincovo	Hincův	k2eAgNnSc1d1	Hincovo
pleso	pleso	k1gNnSc1	pleso
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zabírá	zabírat	k5eAaImIp3nS	zabírat
0,2	[number]	k4	0,2
km2	km2	k4	km2
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
současně	současně	k6eAd1	současně
i	i	k9	i
nejhlubší	hluboký	k2eAgMnSc1d3	nejhlubší
z	z	k7c2	z
tatranských	tatranský	k2eAgMnPc2d1	tatranský
ples	ples	k1gInSc4	ples
–	–	k?	–
58	[number]	k4	58
m.	m.	k?	m.
Prostor	prostora	k1gFnPc2	prostora
u	u	k7c2	u
Štrbského	štrbský	k2eAgNnSc2d1	Štrbské
plesa	pleso	k1gNnSc2	pleso
je	být	k5eAaImIp3nS	být
hranicí	hranice	k1gFnSc7	hranice
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgNnPc7	dva
úmořími	úmoří	k1gNnPc7	úmoří
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
východ	východ	k1gInSc4	východ
teče	téct	k5eAaImIp3nS	téct
řeka	řeka	k1gFnSc1	řeka
Poprad	Poprad	k1gInSc1	Poprad
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
úmoří	úmoří	k1gNnSc2	úmoří
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
pramení	pramenit	k5eAaImIp3nS	pramenit
přítoky	přítok	k1gInPc4	přítok
Váhu	Váh	k1gInSc2	Váh
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
náleží	náležet	k5eAaImIp3nS	náležet
k	k	k7c3	k
úmoří	úmoří	k1gNnSc3	úmoří
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
vodopády	vodopád	k1gInPc4	vodopád
parku	park	k1gInSc2	park
patří	patřit	k5eAaImIp3nP	patřit
Studenovodské	Studenovodský	k2eAgInPc1d1	Studenovodský
vodopády	vodopád	k1gInPc1	vodopád
<g/>
,	,	kIx,	,
Kmeťov	Kmeťov	k1gInSc1	Kmeťov
vodopád	vodopád	k1gInSc1	vodopád
<g/>
,	,	kIx,	,
Vajanského	Vajanský	k2eAgInSc2d1	Vajanský
vodopád	vodopád	k1gInSc1	vodopád
<g/>
,	,	kIx,	,
Roháčsky	Roháčsky	k1gFnSc1	Roháčsky
vodopád	vodopád	k1gInSc1	vodopád
a	a	k8xC	a
Vodopád	vodopád	k1gInSc1	vodopád
Skok	skok	k1gInSc1	skok
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
parku	park	k1gInSc2	park
je	být	k5eAaImIp3nS	být
Gerlachovský	Gerlachovský	k2eAgInSc1d1	Gerlachovský
štít	štít	k1gInSc1	štít
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
Tater	Tatra	k1gFnPc2	Tatra
a	a	k8xC	a
celých	celý	k2eAgInPc2d1	celý
Karpat	Karpaty	k1gInPc2	Karpaty
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
Západních	západní	k2eAgFnPc2d1	západní
Tater	Tatra	k1gFnPc2	Tatra
je	být	k5eAaImIp3nS	být
Bystrá	bystrý	k2eAgFnSc1d1	bystrá
(	(	kIx(	(
<g/>
2248	[number]	k4	2248
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
Belianských	Belianský	k2eAgFnPc2d1	Belianská
Tater	Tatra	k1gFnPc2	Tatra
Havran	Havran	k1gMnSc1	Havran
(	(	kIx(	(
<g/>
2152	[number]	k4	2152
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parku	park	k1gInSc6	park
je	být	k5eAaImIp3nS	být
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
300	[number]	k4	300
jeskyní	jeskyně	k1gFnPc2	jeskyně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
Belianska	Beliansko	k1gNnSc2	Beliansko
jeskyně	jeskyně	k1gFnSc2	jeskyně
blízko	blízko	k7c2	blízko
obce	obec	k1gFnSc2	obec
Lendak	Lendak	k1gInSc1	Lendak
je	být	k5eAaImIp3nS	být
zpřístupněna	zpřístupněn	k2eAgFnSc1d1	zpřístupněna
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Doposud	doposud	k6eAd1	doposud
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
objevený	objevený	k2eAgInSc1d1	objevený
jeskynní	jeskynní	k2eAgInSc1d1	jeskynní
systém	systém	k1gInSc1	systém
má	mít	k5eAaImIp3nS	mít
jeskyně	jeskyně	k1gFnSc1	jeskyně
Javorinka	Javorinka	k1gFnSc1	Javorinka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biologie	biologie	k1gFnSc1	biologie
a	a	k8xC	a
ekologie	ekologie	k1gFnSc1	ekologie
==	==	k?	==
</s>
</p>
<p>
<s>
Geologické	geologický	k2eAgNnSc1d1	geologické
složení	složení	k1gNnSc1	složení
<g/>
,	,	kIx,	,
půdní	půdní	k2eAgFnPc1d1	půdní
vlastnosti	vlastnost	k1gFnPc1	vlastnost
a	a	k8xC	a
klimatické	klimatický	k2eAgFnPc1d1	klimatická
podmínky	podmínka	k1gFnPc1	podmínka
přispívají	přispívat	k5eAaImIp3nP	přispívat
k	k	k7c3	k
originální	originální	k2eAgFnSc3d1	originální
flóře	flóra	k1gFnSc3	flóra
a	a	k8xC	a
fauně	fauna	k1gFnSc3	fauna
v	v	k7c6	v
parku	park	k1gInSc6	park
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Flóra	Flóra	k1gFnSc1	Flóra
===	===	k?	===
</s>
</p>
<p>
<s>
Téměř	téměř	k6eAd1	téměř
dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
parku	park	k1gInSc2	park
jsou	být	k5eAaImIp3nP	být
zalesněny	zalesněn	k2eAgFnPc1d1	zalesněna
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
smrkem	smrk	k1gInSc7	smrk
a	a	k8xC	a
jedlí	jedle	k1gFnSc7	jedle
<g/>
.	.	kIx.	.
</s>
<s>
Nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
stromem	strom	k1gInSc7	strom
je	být	k5eAaImIp3nS	být
smrk	smrk	k1gInSc1	smrk
ztepilý	ztepilý	k2eAgInSc1d1	ztepilý
následovaný	následovaný	k2eAgInSc4d1	následovaný
borovicí	borovice	k1gFnSc7	borovice
lesní	lesní	k2eAgFnSc7d1	lesní
<g/>
,	,	kIx,	,
borovicí	borovice	k1gFnSc7	borovice
limbou	limba	k1gFnSc7	limba
<g/>
,	,	kIx,	,
modřínem	modřín	k1gInSc7	modřín
opadavým	opadavý	k2eAgInSc7d1	opadavý
a	a	k8xC	a
borovicí	borovice	k1gFnSc7	borovice
kleč	kleč	k1gFnSc4	kleč
<g/>
.	.	kIx.	.
</s>
<s>
Listnaté	listnatý	k2eAgInPc1d1	listnatý
stromy	strom	k1gInPc1	strom
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
javory	javor	k1gInPc1	javor
<g/>
,	,	kIx,	,
rostou	růst	k5eAaImIp3nP	růst
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Belianských	Belianský	k2eAgFnPc6d1	Belianská
Tatrách	Tatra	k1gFnPc6	Tatra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
parku	park	k1gInSc6	park
roste	růst	k5eAaImIp3nS	růst
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1300	[number]	k4	1300
cévnatých	cévnatý	k2eAgFnPc2d1	cévnatá
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
57	[number]	k4	57
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
neroste	růst	k5eNaImIp3nS	růst
nikde	nikde	k6eAd1	nikde
jinde	jinde	k6eAd1	jinde
než	než	k8xS	než
v	v	k7c6	v
Karpatech	Karpaty	k1gInPc6	Karpaty
<g/>
,	,	kIx,	,
41	[number]	k4	41
roste	růst	k5eAaImIp3nS	růst
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Západních	západní	k2eAgInPc6d1	západní
Karpatech	Karpaty	k1gInPc6	Karpaty
a	a	k8xC	a
37	[number]	k4	37
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Tatrách	Tatra	k1gFnPc6	Tatra
<g/>
.	.	kIx.	.
</s>
<s>
Významnými	významný	k2eAgInPc7d1	významný
endemity	endemit	k1gInPc7	endemit
jsou	být	k5eAaImIp3nP	být
Erysimum	Erysimum	k1gInSc4	Erysimum
wahlenbergii	wahlenbergie	k1gFnSc4	wahlenbergie
<g/>
,	,	kIx,	,
Cochlearia	Cochlearium	k1gNnPc1	Cochlearium
tatrae	tatrae	k1gFnSc1	tatrae
<g/>
,	,	kIx,	,
Erigeron	Erigeron	k1gMnSc1	Erigeron
hungaricus	hungaricus	k1gMnSc1	hungaricus
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fauna	fauna	k1gFnSc1	fauna
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
parku	park	k1gInSc6	park
žije	žít	k5eAaImIp3nS	žít
155	[number]	k4	155
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
42	[number]	k4	42
druhů	druh	k1gInPc2	druh
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
8	[number]	k4	8
druhů	druh	k1gInPc2	druh
plazů	plaz	k1gInPc2	plaz
a	a	k8xC	a
3	[number]	k4	3
druhy	druh	k1gInPc7	druh
obojživelníků	obojživelník	k1gMnPc2	obojživelník
<g/>
.	.	kIx.	.
</s>
<s>
Domov	domov	k1gInSc1	domov
zde	zde	k6eAd1	zde
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
mnoho	mnoho	k4c1	mnoho
bezobratlých	bezobratlí	k1gMnPc2	bezobratlí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
savců	savec	k1gMnPc2	savec
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
například	například	k6eAd1	například
medvěd	medvěd	k1gMnSc1	medvěd
hnědý	hnědý	k2eAgMnSc1d1	hnědý
<g/>
,	,	kIx,	,
svišť	svišť	k1gMnSc1	svišť
horský	horský	k2eAgMnSc1d1	horský
<g/>
,	,	kIx,	,
kamzík	kamzík	k1gMnSc1	kamzík
horský	horský	k2eAgMnSc1d1	horský
a	a	k8xC	a
rys	rys	k1gMnSc1	rys
ostrovid	ostrovid	k1gMnSc1	ostrovid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Tatra	Tatra	k1gFnSc1	Tatra
National	National	k1gMnSc1	National
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
Slovakia	Slovakia	k1gFnSc1	Slovakia
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Tatranský	tatranský	k2eAgInSc1d1	tatranský
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
(	(	kIx(	(
<g/>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Národní	národní	k2eAgInPc1d1	národní
parky	park	k1gInPc1	park
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tatranský	tatranský	k2eAgInSc4d1	tatranský
národní	národní	k2eAgInSc4d1	národní
park	park	k1gInSc4	park
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
správy	správa	k1gFnSc2	správa
Tatranského	tatranský	k2eAgInSc2d1	tatranský
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
</s>
</p>
<p>
<s>
Průvodce	průvodce	k1gMnSc1	průvodce
po	po	k7c6	po
národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
na	na	k7c4	na
tanap	tanap	k1gInSc4	tanap
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
<p>
<s>
Biosfereická	Biosfereická	k1gFnSc1	Biosfereická
rezervace	rezervace	k1gFnSc2	rezervace
Tatry	Tatra	k1gFnSc2	Tatra
na	na	k7c6	na
webu	web	k1gInSc6	web
ŠOP	šopa	k1gFnPc2	šopa
SR	SR	kA	SR
</s>
</p>
<p>
<s>
Tatranský	tatranský	k2eAgInSc1d1	tatranský
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
na	na	k7c4	na
slovakia	slovakius	k1gMnSc4	slovakius
<g/>
.	.	kIx.	.
<g/>
travel	travel	k1gMnSc1	travel
</s>
</p>
<p>
<s>
Tatranský	tatranský	k2eAgInSc1d1	tatranský
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
na	na	k7c4	na
Enviwiki	Enviwike	k1gFnSc4	Enviwike
</s>
</p>
<p>
<s>
Tatranský	tatranský	k2eAgInSc1d1	tatranský
národný	národný	k2eAgInSc1d1	národný
park	park	k1gInSc1	park
–	–	k?	–
video	video	k1gNnSc1	video
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Návraty	návrat	k1gInPc1	návrat
k	k	k7c3	k
divočině	divočina	k1gFnSc3	divočina
</s>
</p>
