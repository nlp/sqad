<s>
Tatranský	tatranský	k2eAgInSc1d1	tatranský
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
Tatranský	tatranský	k2eAgInSc1d1	tatranský
národný	národný	k2eAgInSc1d1	národný
park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
TANAP	TANAP	kA	TANAP
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc1d3	nejstarší
z	z	k7c2	z
devíti	devět	k4xCc2	devět
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
