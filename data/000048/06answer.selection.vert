<s>
Zpráva	zpráva	k1gFnSc1
o	o	k7c4
revoluci	revoluce	k1gFnSc4
v	v	k7c6
Praze	Praha	k1gFnSc6
a	a	k8xC
vzniku	vznik	k1gInSc6
Československa	Československo	k1gNnSc2
dne	den	k1gInSc2
28	[number]	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1918	[number]	k4
zastihla	zastihnout	k5eAaPmAgFnS
Masaryka	Masaryk	k1gMnSc2
ještě	ještě	k9
v	v	k7c6
Americe	Amerika	k1gFnSc6
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xS
zpráva	zpráva	k1gFnSc1
o	o	k7c6
jeho	jeho	k3xOp3gNnSc6
zvolení	zvolení	k1gNnSc6
prezidentem	prezident	k1gMnSc7
<g/>
.	.	kIx.
</s>