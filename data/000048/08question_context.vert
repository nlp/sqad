<s>
Tomáš	Tomáš	k1gMnSc1
Garrigue	Garrigue	k1gNnSc2
Masaryk	Masaryk	k1gMnSc1
<g/>
,	,	kIx,
označovaný	označovaný	k2eAgMnSc1d1
T.	T.	kA
G.	G.	kA
M.	M.	kA
<g/>
,	,	kIx,
TGM	TGM	kA
nebo	nebo	k8xC
president	president	k1gMnSc1
Osvoboditel	osvoboditel	k1gMnSc1
(	(	kIx(
<g/>
7	[number]	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1850	[number]	k4
Hodonín	Hodonín	k1gInSc1
–	–	k?
14	[number]	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1937	[number]	k4
Lány	lán	k1gInPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
státník	státník	k1gMnSc1
<g/>
,	,	kIx,
filozof	filozof	k1gMnSc1
a	a	k8xC
pedagog	pedagog	k1gMnSc1
<g/>
,	,	kIx,
první	první	k4xOgMnSc1
prezident	prezident	k1gMnSc1
Československé	československý	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>