<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
hráčů	hráč	k1gMnPc2
do	do	k7c2
17	#num#	k4
let	léto	k1gNnPc2
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
hráčů	hráč	k1gMnPc2
do	do	k7c2
17	#num#	k4
let	rok	k1gInPc2
je	být	k5eAaImIp3nS
turnajem	turnaj	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1
každoročně	každoročně	k6eAd1
pořádá	pořádat	k5eAaImIp3nS
organizace	organizace	k1gFnSc1
UEFA	UEFA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k7c4
určení	určení	k1gNnSc4
evropského	evropský	k2eAgMnSc2d1
mistra	mistr	k1gMnSc2
do	do	k7c2
17	#num#	k4
let	léto	k1gNnPc2
slouží	sloužit	k5eAaImIp3nS
tento	tento	k3xDgInSc4
finálový	finálový	k2eAgInSc4d1
turnaj	turnaj	k1gInSc4
v	v	k7c6
lichých	lichý	k2eAgNnPc6d1
letech	léto	k1gNnPc6
jako	jako	k8xS,k8xC
kvalifikace	kvalifikace	k1gFnSc1
na	na	k7c6
mistrovství	mistrovství	k1gNnSc6
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
hráčů	hráč	k1gMnPc2
do	do	k7c2
17	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
2001	#num#	k4
byl	být	k5eAaImAgInS
turnaj	turnaj	k1gInSc1
určen	určit	k5eAaPmNgInS
hráčům	hráč	k1gMnPc3
do	do	k7c2
16	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Výsledky	výsledek	k1gInPc1
jednotlivých	jednotlivý	k2eAgInPc2d1
ročníků	ročník	k1gInPc2
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
hráčů	hráč	k1gMnPc2
do	do	k7c2
16	#num#	k4
let	léto	k1gNnPc2
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Pořadatel	pořadatel	k1gMnSc1
</s>
<s>
Finále	finále	k1gNnSc1
</s>
<s>
O	o	k7c4
třetí	třetí	k4xOgNnSc4
místo	místo	k1gNnSc4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
Skóre	skóre	k1gNnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Skóre	skóre	k1gNnSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1982	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
Itálie	Itálie	k1gFnSc2
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
SRN	SRN	kA
</s>
<s>
Jugoslávie	Jugoslávie	k1gFnSc1
</s>
<s>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
(	(	kIx(
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
1984	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Západní	západní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Západní	západní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
</s>
<s>
SRN	SRN	kA
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
</s>
<s>
Anglie	Anglie	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Jugoslávie	Jugoslávie	k1gFnSc1
</s>
<s>
1985	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
</s>
<s>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Řecko	Řecko	k1gNnSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
NDR	NDR	kA
</s>
<s>
1986	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Řecko	Řecko	k1gNnSc1
Řecko	Řecko	k1gNnSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
(	(	kIx(
<g/>
9	#num#	k4
<g/>
:	:	kIx,
<g/>
8	#num#	k4
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
NDR	NDR	kA
</s>
<s>
1987	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Francie	Francie	k1gFnSc1
Francie	Francie	k1gFnSc2
</s>
<s>
Itálietitul	Itálietitout	k5eAaPmAgInS
neudělen	udělit	k5eNaPmNgInS
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
(	(	kIx(
<g/>
původně	původně	k6eAd1
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
kontumačně	kontumačně	k6eAd1
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Turecko	Turecko	k1gNnSc1
</s>
<s>
1988	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
(	(	kIx(
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
NDR	NDR	kA
</s>
<s>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
(	(	kIx(
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
SRN	SRN	kA
</s>
<s>
1989	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Dánsko	Dánsko	k1gNnSc1
Dánsko	Dánsko	k1gNnSc1
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
NDR	NDR	kA
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
1990	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Východní	východní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Východní	východní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
prodl	prodnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Jugoslávie	Jugoslávie	k1gFnSc1
</s>
<s>
Polsko	Polsko	k1gNnSc1
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
1991	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
Řecko	Řecko	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
(	(	kIx(
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
1992	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Kypr	Kypr	k1gInSc1
Kypr	Kypr	k1gInSc1
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
1993	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Turecko	Turecko	k1gNnSc1
Turecko	Turecko	k1gNnSc1
</s>
<s>
Polsko	Polsko	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
1994	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Irsko	Irsko	k1gNnSc1
Irsko	Irsko	k1gNnSc1
</s>
<s>
Turecko	Turecko	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Dánsko	Dánsko	k1gNnSc1
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
1995	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Belgie	Belgie	k1gFnSc1
Belgie	Belgie	k1gFnSc1
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
prodl	prodnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
1996	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
Izrael	Izrael	k1gInSc1
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Řecko	Řecko	k1gNnSc1
</s>
<s>
1997	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Německo	Německo	k1gNnSc1
Německo	Německo	k1gNnSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
(	(	kIx(
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
1998	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Skotsko	Skotsko	k1gNnSc1
Skotsko	Skotsko	k1gNnSc1
</s>
<s>
Irsko	Irsko	k1gNnSc1
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
1999	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Polsko	Polsko	k1gNnSc1
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
2000	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Izrael	Izrael	k1gInSc1
Izrael	Izrael	k1gInSc1
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
zlatý	zlatý	k2eAgInSc4d1
gól	gól	k1gInSc4
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Řecko	Řecko	k1gNnSc1
</s>
<s>
2001	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Anglie	Anglie	k1gFnSc1
Anglie	Anglie	k1gFnSc2
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Anglie	Anglie	k1gFnSc1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
hráčů	hráč	k1gMnPc2
do	do	k7c2
17	#num#	k4
let	léto	k1gNnPc2
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Pořadatel	pořadatel	k1gMnSc1
</s>
<s>
Finále	finále	k1gNnSc1
</s>
<s>
O	o	k7c4
třetí	třetí	k4xOgNnSc4
místo	místo	k1gNnSc4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
Skóre	skóre	k1gNnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Skóre	skóre	k1gNnSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
2002	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Dánsko	Dánsko	k1gNnSc1
Dánsko	Dánsko	k1gNnSc1
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
(	(	kIx(
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
Anglie	Anglie	k1gFnSc1
</s>
<s>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
2003	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Anglie	Anglie	k1gFnSc1
</s>
<s>
2004	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Francie	Francie	k1gFnSc1
Francie	Francie	k1gFnSc2
</s>
<s>
Francie	Francie	k1gFnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
<g/>
(	(	kIx(
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Anglie	Anglie	k1gFnSc1
</s>
<s>
2005	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
Itálie	Itálie	k1gFnSc2
</s>
<s>
Turecko	Turecko	k1gNnSc1
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
prodl	prodnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
2006	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Lucembursko	Lucembursko	k1gNnSc1
Lucembursko	Lucembursko	k1gNnSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
(	(	kIx(
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
(	(	kIx(
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Hostitel	hostitel	k1gMnSc1
</s>
<s>
Finále	finále	k1gNnSc1
</s>
<s>
Poražení	poražený	k2eAgMnPc1d1
semifinalisté	semifinalista	k1gMnPc1
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
Skóre	skóre	k1gNnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
2007	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Belgie	Belgie	k1gFnSc1
Belgie	Belgie	k1gFnSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Anglie	Anglie	k1gFnSc1
</s>
<s>
Belgie	Belgie	k1gFnSc1
a	a	k8xC
Francie	Francie	k1gFnSc1
</s>
<s>
2008	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Turecko	Turecko	k1gNnSc1
Turecko	Turecko	k1gNnSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
a	a	k8xC
Turecko	Turecko	k1gNnSc1
</s>
<s>
2009	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Německo	Německo	k1gNnSc1
Německo	Německo	k1gNnSc1
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
prodl	prodnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
a	a	k8xC
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
2010	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1
</s>
<s>
Anglie	Anglie	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Francie	Francie	k1gFnSc1
a	a	k8xC
Turecko	Turecko	k1gNnSc1
</s>
<s>
2011	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
Srbsko	Srbsko	k1gNnSc1
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
Dánsko	Dánsko	k1gNnSc1
a	a	k8xC
Anglie	Anglie	k1gFnSc1
</s>
<s>
2012	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1
Slovinsko	Slovinsko	k1gNnSc1
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
(	(	kIx(
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
Gruzie	Gruzie	k1gFnSc1
a	a	k8xC
Polsko	Polsko	k1gNnSc1
</s>
<s>
2013	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
(	(	kIx(
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
a	a	k8xC
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
2014	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Malta	Malta	k1gFnSc1
Malta	Malta	k1gFnSc1
</s>
<s>
Anglie	Anglie	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
(	(	kIx(
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
a	a	k8xC
Skotsko	Skotsko	k1gNnSc1
</s>
<s>
2015	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
Belgie	Belgie	k1gFnSc1
a	a	k8xC
Rusko	Rusko	k1gNnSc1
</s>
<s>
2016	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Ázerbájdžán	Ázerbájdžán	k1gInSc1
Ázerbájdžán	Ázerbájdžán	k1gInSc1
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
(	(	kIx(
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Německo	Německo	k1gNnSc1
a	a	k8xC
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
2017	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
(	(	kIx(
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Anglie	Anglie	k1gFnSc1
</s>
<s>
Německo	Německo	k1gNnSc1
a	a	k8xC
Turecko	Turecko	k1gNnSc1
</s>
<s>
2018	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Anglie	Anglie	k1gFnSc1
Anglie	Anglie	k1gFnSc2
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
(	(	kIx(
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
Belgie	Belgie	k1gFnSc1
a	a	k8xC
Anglie	Anglie	k1gFnSc1
</s>
<s>
2019	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Irsko	Irsko	k1gNnSc1
Irsko	Irsko	k1gNnSc1
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
Francie	Francie	k1gFnSc1
a	a	k8xC
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
2020	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Estonsko	Estonsko	k1gNnSc1
Estonsko	Estonsko	k1gNnSc1
</s>
<s>
Zrušeno	zrušit	k5eAaPmNgNnS
kvůli	kvůli	k7c3
pandemii	pandemie	k1gFnSc3
covidu-	covidu-	k?
<g/>
19	#num#	k4
</s>
<s>
2021	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Kypr	Kypr	k1gInSc1
Kypr	Kypr	k1gInSc1
</s>
<s>
2022	#num#	k4
<g/>
Detaily	detail	k1gInPc1
</s>
<s>
Izrael	Izrael	k1gInSc1
Izrael	Izrael	k1gInSc1
</s>
<s>
Úspěchy	úspěch	k1gInPc1
</s>
<s>
ZeměVítězství	ZeměVítězstvit	k5eAaPmIp3nS
</s>
<s>
Druhé	druhý	k4xOgNnSc1
místo	místo	k1gNnSc1
</s>
<s>
Třetí	třetí	k4xOgNnSc1
místo	místo	k1gNnSc1
<g/>
*	*	kIx~
</s>
<s>
Čtvrté	čtvrtý	k4xOgNnSc1
místo	místo	k1gNnSc1
<g/>
*	*	kIx~
</s>
<s>
Semifinále	semifinále	k1gNnSc1
<g/>
*	*	kIx~
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
9	#num#	k4
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
6	#num#	k4
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
3	#num#	k4
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
6	#num#	k4
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
3	#num#	k4
(	(	kIx(
<g/>
1990,1992	1990,1992	k4
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
4	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
3	#num#	k4
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Německo	Německo	k1gNnSc1
SRN	srna	k1gFnPc2
</s>
<s>
3	#num#	k4
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
5	#num#	k4
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
4	#num#	k4
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
6	#num#	k4
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
,	,	kIx,
1888	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rusko	Rusko	k1gNnSc4
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
</s>
<s>
3	#num#	k4
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
2	#num#	k4
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
4	#num#	k4
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
3	#num#	k4
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
3	#num#	k4
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Anglie	Anglie	k1gFnSc1
</s>
<s>
2	#num#	k4
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
3	#num#	k4
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Turecko	Turecko	k1gNnSc1
</s>
<s>
2	#num#	k4
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
6	#num#	k4
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Česko	Česko	k1gNnSc1
Československo	Československo	k1gNnSc1
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Polsko	Polsko	k1gNnSc1
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Irsko	Irsko	k1gNnSc1
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Řecko	Řecko	k1gNnSc1
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
(	(	kIx(
<g/>
1996	#num#	k4
,2000	,2000	k4
<g/>
)	)	kIx)
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jugoslávie	Jugoslávie	k1gFnSc1
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Dánsko	Dánsko	k1gNnSc1
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Izrael	Izrael	k1gInSc1
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Belgie	Belgie	k1gFnSc1
</s>
<s>
3	#num#	k4
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Gruzie	Gruzie	k1gFnSc1
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Skotsko	Skotsko	k1gNnSc1
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
*	*	kIx~
Od	od	k7c2
roku	rok	k1gInSc2
2007	#num#	k4
se	se	k3xPyFc4
nekoná	konat	k5eNaImIp3nS
zápas	zápas	k1gInSc1
o	o	k7c4
třetí	třetí	k4xOgNnSc4
místo	místo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Vzhledem	vzhledem	k7c3
k	k	k7c3
neoprávněnému	oprávněný	k2eNgInSc3d1
startu	start	k1gInSc3
italského	italský	k2eAgMnSc2d1
hráče	hráč	k1gMnSc2
nebyl	být	k5eNaImAgInS
titul	titul	k1gInSc4
udělen	udělen	k2eAgInSc4d1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
France	Franc	k1gMnSc4
2	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
Spain	Spaina	k1gFnPc2
Archivováno	archivovat	k5eAaBmNgNnS
14	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
U17	U17	k1gMnSc1
Championship	Championship	k1gMnSc1
Final	Final	k1gMnSc1
<g/>
,	,	kIx,
UEFA	UEFA	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
citováno	citován	k2eAgNnSc4d1
18	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
UEFA	UEFA	kA
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
UEFA	UEFA	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
UefaU	UefaU	k?
<g/>
17	#num#	k4
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Soutěže	soutěž	k1gFnSc2
pořádané	pořádaný	k2eAgFnSc2d1
asociací	asociace	k1gFnSc7
UEFA	UEFA	kA
Reprezentační	reprezentační	k2eAgFnSc1d1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
•	•	k?
Liga	liga	k1gFnSc1
národů	národ	k1gInPc2
UEFA	UEFA	kA
•	•	k?
ME	ME	kA
do	do	k7c2
21	#num#	k4
let	léto	k1gNnPc2
•	•	k?
ME	ME	kA
do	do	k7c2
19	#num#	k4
let	léto	k1gNnPc2
•	•	k?
ME	ME	kA
do	do	k7c2
17	#num#	k4
let	léto	k1gNnPc2
•	•	k?
ME	ME	kA
žen	žena	k1gFnPc2
•	•	k?
ME	ME	kA
žen	žena	k1gFnPc2
do	do	k7c2
19	#num#	k4
let	léto	k1gNnPc2
•	•	k?
ME	ME	kA
žen	žena	k1gFnPc2
do	do	k7c2
17	#num#	k4
let	léto	k1gNnPc2
•	•	k?
ME	ME	kA
ve	v	k7c6
futsalu	futsal	k1gInSc6
•	•	k?
Meridian	Meridian	k1gInSc1
Cup	cup	k1gInSc1
(	(	kIx(
<g/>
zaniklý	zaniklý	k2eAgMnSc1d1
<g/>
)	)	kIx)
Klubové	klubový	k2eAgNnSc1d1
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
•	•	k?
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
•	•	k?
Evropská	evropský	k2eAgFnSc1d1
konferenční	konferenční	k2eAgFnSc1d1
liga	liga	k1gFnSc1
(	(	kIx(
<g/>
budoucí	budoucí	k2eAgFnSc1d1
<g/>
)	)	kIx)
•	•	k?
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
•	•	k?
Pohár	pohár	k1gInSc4
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
(	(	kIx(
<g/>
zaniklý	zaniklý	k2eAgInSc1d1
<g/>
)	)	kIx)
•	•	k?
Pohár	pohár	k1gInSc1
Intertoto	Intertota	k1gFnSc5
(	(	kIx(
<g/>
zaniklý	zaniklý	k2eAgInSc4d1
<g/>
)	)	kIx)
•	•	k?
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
ve	v	k7c4
futsalu	futsala	k1gFnSc4
•	•	k?
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
žen	žena	k1gFnPc2
Ostatní	ostatní	k2eAgFnSc2d1
významné	významný	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
<g/>
:	:	kIx,
Středoevropský	středoevropský	k2eAgInSc1d1
pohár	pohár	k1gInSc1
(	(	kIx(
<g/>
zaniklý	zaniklý	k2eAgInSc1d1
<g/>
)	)	kIx)
•	•	k?
Veletržní	veletržní	k2eAgInSc4d1
pohár	pohár	k1gInSc4
(	(	kIx(
<g/>
zaniklý	zaniklý	k2eAgMnSc1d1
<g/>
)	)	kIx)
Seznam	seznam	k1gInSc1
českých	český	k2eAgMnPc2d1
vítězů	vítěz	k1gMnPc2
nadnárodních	nadnárodní	k2eAgFnPc2d1
fotbalových	fotbalový	k2eAgFnPc2d1
klubových	klubový	k2eAgFnPc2d1
soutěží	soutěž	k1gFnPc2
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
hráčů	hráč	k1gMnPc2
do	do	k7c2
17	#num#	k4
let	léto	k1gNnPc2
</s>
<s>
Itálie	Itálie	k1gFnSc1
1982	#num#	k4
•	•	k?
Západní	západní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
1984	#num#	k4
•	•	k?
Maďarsko	Maďarsko	k1gNnSc1
1985	#num#	k4
•	•	k?
Řecko	Řecko	k1gNnSc1
1986	#num#	k4
•	•	k?
Francie	Francie	k1gFnSc1
1987	#num#	k4
•	•	k?
Španělsko	Španělsko	k1gNnSc1
1988	#num#	k4
•	•	k?
Dánsko	Dánsko	k1gNnSc1
1989	#num#	k4
•	•	k?
Východní	východní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
1990	#num#	k4
•	•	k?
Švýcarsko	Švýcarsko	k1gNnSc1
1991	#num#	k4
•	•	k?
Kypr	Kypr	k1gInSc1
1992	#num#	k4
•	•	k?
Turecko	Turecko	k1gNnSc1
1993	#num#	k4
•	•	k?
Irsko	Irsko	k1gNnSc1
<g/>
1994	#num#	k4
•	•	k?
Belgie	Belgie	k1gFnSc1
1995	#num#	k4
•	•	k?
Rakousko	Rakousko	k1gNnSc1
1996	#num#	k4
•	•	k?
Německo	Německo	k1gNnSc1
1997	#num#	k4
•	•	k?
Skotsko	Skotsko	k1gNnSc1
1998	#num#	k4
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Česko	Česko	k1gNnSc1
1999	#num#	k4
•	•	k?
Izrael	Izrael	k1gInSc1
2000	#num#	k4
•	•	k?
Anglie	Anglie	k1gFnSc2
2001	#num#	k4
•	•	k?
Dánsko	Dánsko	k1gNnSc1
2002	#num#	k4
•	•	k?
Portugalsko	Portugalsko	k1gNnSc1
2003	#num#	k4
•	•	k?
Francie	Francie	k1gFnSc1
2004	#num#	k4
•	•	k?
Itálie	Itálie	k1gFnSc2
2005	#num#	k4
•	•	k?
Lucembursko	Lucembursko	k1gNnSc1
2006	#num#	k4
•	•	k?
Belgie	Belgie	k1gFnSc1
2007	#num#	k4
•	•	k?
Turecko	Turecko	k1gNnSc1
2008	#num#	k4
•	•	k?
Německo	Německo	k1gNnSc1
2009	#num#	k4
•	•	k?
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1
2010	#num#	k4
•	•	k?
Srbsko	Srbsko	k1gNnSc1
2011	#num#	k4
•	•	k?
Slovinsko	Slovinsko	k1gNnSc1
2012	#num#	k4
•	•	k?
Slovensko	Slovensko	k1gNnSc1
2013	#num#	k4
•	•	k?
Malta	Malta	k1gFnSc1
2014	#num#	k4
•	•	k?
Bulharsko	Bulharsko	k1gNnSc1
2015	#num#	k4
•	•	k?
Ázerbájdžán	Ázerbájdžán	k1gInSc1
2016	#num#	k4
•	•	k?
Chorvatsko	Chorvatsko	k1gNnSc1
2017	#num#	k4
•	•	k?
Anglie	Anglie	k1gFnSc2
2018	#num#	k4
•	•	k?
Irsko	Irsko	k1gNnSc1
2019	#num#	k4
•	•	k?
Estonsko	Estonsko	k1gNnSc1
2020	#num#	k4
•	•	k?
Kypr	Kypr	k1gInSc1
2021	#num#	k4
•	•	k?
Izrael	Izrael	k1gInSc1
2022	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
</s>
