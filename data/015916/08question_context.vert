<s>
Zámek	zámek	k1gInSc1
Světlá	Světlá	k2gFnSc1
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
stojí	stát	k5eAaImIp3nS
v	v	k7c6
Zámecké	zámecký	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
ve	v	k7c4
Světlé	světlý	k2eAgNnSc4d1
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
<g/>
,	,	kIx,
na	na	k7c6
levém	levý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
Sázavy	Sázava	k1gFnSc2
<g/>
,	,	kIx,
několik	několik	k4yIc4
metrů	metr	k1gInPc2
od	od	k7c2
centra	centrum	k1gNnSc2
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>