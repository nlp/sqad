<s>
Světlá	světlat	k5eAaImIp3nS
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
(	(	kIx(
<g/>
zámek	zámek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Světlá	světlat	k5eAaImIp3nS
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
Zámek	zámek	k1gInSc1
Světlá	světlat	k5eAaImIp3nS
nad	nad	k7c7
SázavouÚčel	SázavouÚčel	k1gFnSc7
stavby	stavba	k1gFnSc2
</s>
<s>
Prohlídkové	prohlídkový	k2eAgInPc4d1
okruhy	okruh	k1gInPc4
pro	pro	k7c4
malé	malý	k2eAgMnPc4d1
i	i	k9
velké	velký	k2eAgNnSc1d1
<g/>
,	,	kIx,
zámecká	zámecký	k2eAgFnSc1d1
kavárna	kavárna	k1gFnSc1
<g/>
,	,	kIx,
oranžerie	oranžerie	k1gFnSc1
<g/>
,	,	kIx,
zámecký	zámecký	k2eAgInSc1d1
park	park	k1gInSc1
s	s	k7c7
kaskádou	kaskáda	k1gFnSc7
rybníků	rybník	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Sloh	sloha	k1gFnPc2
</s>
<s>
novorenesanční	novorenesanční	k2eAgMnSc1d1
Architekt	architekt	k1gMnSc1
</s>
<s>
Svatoš	Svatoš	k1gMnSc1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
1393	#num#	k4
(	(	kIx(
<g/>
tvrz	tvrz	k1gFnSc1
<g/>
)	)	kIx)
Přestavba	přestavba	k1gFnSc1
</s>
<s>
1567	#num#	k4
(	(	kIx(
<g/>
lovecký	lovecký	k2eAgInSc1d1
zámeček	zámeček	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1668	#num#	k4
<g/>
–	–	k?
<g/>
1672	#num#	k4
<g/>
,	,	kIx,
okolo	okolo	k7c2
roku	rok	k1gInSc2
1737	#num#	k4
<g/>
,	,	kIx,
1868	#num#	k4
<g/>
–	–	k?
<g/>
1873	#num#	k4
(	(	kIx(
<g/>
novorenesanční	novorenesanční	k2eAgMnSc1d1
<g/>
)	)	kIx)
Stavebník	stavebník	k1gMnSc1
</s>
<s>
Štěpán	Štěpán	k1gMnSc1
ze	z	k7c2
Šternberka	Šternberk	k1gInSc2
Další	další	k2eAgMnPc1d1
majitelé	majitel	k1gMnPc1
</s>
<s>
Šternberkové	Šternberková	k1gFnPc1
<g/>
,	,	kIx,
klášter	klášter	k1gInSc1
Vilémov	Vilémov	k1gInSc1
<g/>
,	,	kIx,
Lucemburkové	Lucemburk	k1gMnPc1
<g/>
,	,	kIx,
Trčkové	Trčkové	k2eAgMnPc1d1
z	z	k7c2
Lípy	lípa	k1gFnSc2
<g/>
,	,	kIx,
páni	pan	k1gMnPc1
z	z	k7c2
Vernieru	vernier	k1gInSc2
<g/>
,	,	kIx,
Černínové	Černínové	k2eAgNnSc1d1
z	z	k7c2
Chudenic	Chudenice	k1gFnPc2
<g/>
,	,	kIx,
Krakovští	krakovský	k2eAgMnPc1d1
z	z	k7c2
Kolovrat	kolovrat	k1gInSc1
<g/>
,	,	kIx,
Zichyové	Zichyové	k2eAgNnSc1d1
z	z	k7c2
Vazsonykeö	Vazsonykeö	k1gFnSc2
<g/>
,	,	kIx,
Salm-Reifferscheidtové	Salm-Reifferscheidtové	k2eAgMnSc1d1
Současný	současný	k2eAgMnSc1d1
majitel	majitel	k1gMnSc1
</s>
<s>
rodina	rodina	k1gFnSc1
Degerme	Degerme	k1gFnSc1
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Zámecká	zámecký	k2eAgFnSc1d1
1	#num#	k4
<g/>
,	,	kIx,
Světlá	světlat	k5eAaImIp3nS
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Ulice	ulice	k1gFnSc2
</s>
<s>
Zámecká	zámecký	k2eAgFnSc1d1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
40	#num#	k4
<g/>
′	′	k?
<g/>
2,51	2,51	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
15	#num#	k4
<g/>
°	°	k?
<g/>
24	#num#	k4
<g/>
′	′	k?
<g/>
27,24	27,24	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Světlá	světlat	k5eAaImIp3nS
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
</s>
<s>
Světlá	světlat	k5eAaImIp3nS
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Rejstříkové	rejstříkový	k2eAgFnPc1d1
číslo	číslo	k1gNnSc4
památky	památka	k1gFnSc2
</s>
<s>
21081	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
341	#num#	k4
(	(	kIx(
<g/>
Pk	Pk	k1gFnSc1
<g/>
•	•	k?
<g/>
MIS	mísa	k1gFnPc2
<g/>
•	•	k?
<g/>
Sez	Sez	k1gMnSc1
<g/>
•	•	k?
<g/>
Obr	obr	k1gMnSc1
<g/>
)	)	kIx)
Web	web	k1gInSc1
</s>
<s>
www.zameksvetla.cz	www.zameksvetla.cz	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zámek	zámek	k1gInSc1
Světlá	Světlá	k2gFnSc1
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
stojí	stát	k5eAaImIp3nS
v	v	k7c6
Zámecké	zámecký	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
ve	v	k7c4
Světlé	světlý	k2eAgNnSc4d1
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
<g/>
,	,	kIx,
na	na	k7c6
levém	levý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
Sázavy	Sázava	k1gFnSc2
<g/>
,	,	kIx,
několik	několik	k4yIc4
metrů	metr	k1gInPc2
od	od	k7c2
centra	centrum	k1gNnSc2
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1963	#num#	k4
je	být	k5eAaImIp3nS
chráněn	chránit	k5eAaImNgInS
jako	jako	k8xS,k8xC
kulturní	kulturní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1385	#num#	k4
získal	získat	k5eAaPmAgInS
Světlou	světlý	k2eAgFnSc7d1
od	od	k7c2
kláštera	klášter	k1gInSc2
ve	v	k7c6
Vilémově	Vilémův	k2eAgFnSc6d1
do	do	k7c2
zástavy	zástava	k1gFnSc2
Albert	Alberta	k1gFnPc2
ze	z	k7c2
Šternberka	Šternberk	k1gInSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc4
syn	syn	k1gMnSc1
Štěpán	Štěpán	k1gMnSc1
zde	zde	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1393	#num#	k4
nechal	nechat	k5eAaPmAgMnS
postavit	postavit	k5eAaPmF
tvrz	tvrz	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
zániku	zánik	k1gInSc6
kláštera	klášter	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1421	#num#	k4
přešly	přejít	k5eAaPmAgInP
veškeré	veškerý	k3xTgInPc1
majetky	majetek	k1gInPc1
na	na	k7c4
českého	český	k2eAgMnSc4d1
krále	král	k1gMnSc4
<g/>
,	,	kIx,
od	od	k7c2
něhož	jenž	k3xRgInSc2
Světlou	světlý	k2eAgFnSc4d1
v	v	k7c6
roce	rok	k1gInSc6
1429	#num#	k4
získal	získat	k5eAaPmAgMnS
Mikuláš	Mikuláš	k1gMnSc1
Trčka	Trčka	k1gMnSc1
z	z	k7c2
Lípy	lípa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1567	#num#	k4
ji	on	k3xPp3gFnSc4
Burian	Burian	k1gMnSc1
Trčka	Trčka	k1gMnSc1
z	z	k7c2
Lípy	lípa	k1gFnSc2
nechal	nechat	k5eAaPmAgMnS
přestavět	přestavět	k5eAaPmF
na	na	k7c4
lovecký	lovecký	k2eAgInSc4d1
zámeček	zámeček	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnPc4d1
úpravy	úprava	k1gFnPc4
pak	pak	k6eAd1
provedli	provést	k5eAaPmAgMnP
v	v	k7c6
letech	let	k1gInPc6
1668	#num#	k4
<g/>
–	–	k?
<g/>
1672	#num#	k4
Jan	Jan	k1gMnSc1
Bartoloměj	Bartoloměj	k1gMnSc1
z	z	k7c2
Vernieru	vernier	k1gInSc2
a	a	k8xC
především	především	k6eAd1
okolo	okolo	k7c2
roku	rok	k1gInSc2
1737	#num#	k4
hrabě	hrabě	k1gMnSc1
František	František	k1gMnSc1
Antonín	Antonín	k1gMnSc1
Černín	Černín	k1gMnSc1
z	z	k7c2
Chudenic	Chudenice	k1gFnPc2
<g/>
,	,	kIx,
od	od	k7c2
něhož	jenž	k3xRgNnSc2
jej	on	k3xPp3gInSc4
v	v	k7c6
roce	rok	k1gInSc6
1748	#num#	k4
zakoupil	zakoupit	k5eAaPmAgMnS
Filip	Filip	k1gMnSc1
Krakovský	krakovský	k2eAgMnSc1d1
z	z	k7c2
Kolovrat	kolovrat	k1gInSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
napoleonských	napoleonský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
sloužil	sloužit	k5eAaImAgInS
zámek	zámek	k1gInSc1
jako	jako	k8xS,k8xC
lazaret	lazaret	k1gInSc1
<g/>
,	,	kIx,
mrtví	mrtvý	k1gMnPc1
bývali	bývat	k5eAaImAgMnP
pohřbíváni	pohřbívat	k5eAaImNgMnP
na	na	k7c6
pastvině	pastvina	k1gFnSc6
u	u	k7c2
nedaleké	daleký	k2eNgFnSc2d1
Dolní	dolní	k2eAgFnSc2d1
Březinky	březinka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roce	rok	k1gInSc6
1817	#num#	k4
za	za	k7c4
Františka	František	k1gMnSc4
Josefa	Josef	k1gMnSc4
Zichyho	Zichy	k1gMnSc4
z	z	k7c2
Vazsonykeö	Vazsonykeö	k1gFnSc2
přibylo	přibýt	k5eAaPmAgNnS
západní	západní	k2eAgNnSc1d1
<g/>
,	,	kIx,
empírové	empírový	k2eAgFnPc1d1
<g/>
,	,	kIx,
křídlo	křídlo	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
uzavíralo	uzavírat	k5eAaImAgNnS,k5eAaPmAgNnS
nádvoří	nádvoří	k1gNnSc3
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
štuková	štukový	k2eAgFnSc1d1
erbovní	erbovní	k2eAgFnSc1d1
výzdoba	výzdoba	k1gFnSc1
v	v	k7c6
rytířském	rytířský	k2eAgInSc6d1
sále	sál	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současnou	současný	k2eAgFnSc4d1
podobu	podoba	k1gFnSc4
zámku	zámek	k1gInSc2
vtiskl	vtisknout	k5eAaPmAgInS
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
ze	z	k7c2
Salm-Reifferscheidtu	Salm-Reifferscheidt	k1gInSc2
během	během	k7c2
novorenesanční	novorenesanční	k2eAgFnSc2d1
přestavby	přestavba	k1gFnSc2
v	v	k7c6
letech	let	k1gInPc6
1868	#num#	k4
<g/>
–	–	k?
<g/>
1873	#num#	k4
<g/>
,	,	kIx,
na	na	k7c6
které	který	k3yRgFnSc6,k3yQgFnSc6,k3yIgFnSc6
se	se	k3xPyFc4
podílel	podílet	k5eAaImAgMnS
vídeňský	vídeňský	k2eAgMnSc1d1
sochař	sochař	k1gMnSc1
Josef	Josef	k1gMnSc1
Gasser	Gasser	k1gMnSc1
a	a	k8xC
vedl	vést	k5eAaImAgMnS
ji	on	k3xPp3gFnSc4
vídeňský	vídeňský	k2eAgMnSc1d1
architekt	architekt	k1gMnSc1
Svatoš	Svatoš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
jej	on	k3xPp3gMnSc4
využívala	využívat	k5eAaPmAgFnS,k5eAaImAgFnS
německá	německý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
,	,	kIx,
po	po	k7c6
konfiskaci	konfiskace	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
ji	on	k3xPp3gFnSc4
nahradila	nahradit	k5eAaPmAgFnS
československá	československý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
50	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
zde	zde	k6eAd1
bývala	bývat	k5eAaImAgFnS
zemědělská	zemědělský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
na	na	k7c6
zámku	zámek	k1gInSc6
skončila	skončit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
části	část	k1gFnSc6
zámku	zámek	k1gInSc2
nachází	nacházet	k5eAaImIp3nS
Muzeum	muzeum	k1gNnSc1
Světelska	Světelsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
však	však	k8xC
zámek	zámek	k1gInSc4
nabízí	nabízet	k5eAaImIp3nS
4	#num#	k4
prohlídkové	prohlídkový	k2eAgInPc4d1
okruhy	okruh	k1gInPc4
<g/>
:	:	kIx,
I.	I.	kA
Expozice	expozice	k1gFnSc1
historického	historický	k2eAgNnSc2d1
skla	sklo	k1gNnSc2
odkazuje	odkazovat	k5eAaImIp3nS
na	na	k7c4
bohatou	bohatý	k2eAgFnSc4d1
tradici	tradice	k1gFnSc4
sklářství	sklářství	k1gNnSc2
na	na	k7c6
Světelsku	Světelsek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
II	II	kA
<g/>
.	.	kIx.
okruh	okruh	k1gInSc1
představuje	představovat	k5eAaImIp3nS
expozici	expozice	k1gFnSc4
historických	historický	k2eAgFnPc2d1
hodin	hodina	k1gFnPc2
<g/>
,	,	kIx,
obrazů	obraz	k1gInPc2
a	a	k8xC
plastik	plastika	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Současnost	současnost	k1gFnSc1
</s>
<s>
V	v	k7c6
červnu	červen	k1gInSc6
2012	#num#	k4
se	se	k3xPyFc4
město	město	k1gNnSc1
domluvilo	domluvit	k5eAaPmAgNnS
s	s	k7c7
vedením	vedení	k1gNnSc7
Kraje	kraj	k1gInSc2
Vysočina	vysočina	k1gFnSc1
o	o	k7c6
výměně	výměna	k1gFnSc6
zámku	zámek	k1gInSc2
za	za	k7c4
jednu	jeden	k4xCgFnSc4
z	z	k7c2
bytovek	bytovka	k1gFnPc2
v	v	k7c6
areálu	areál	k1gInSc6
uměleckoprůmyslové	uměleckoprůmyslový	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
a	a	k8xC
doplacení	doplacení	k1gNnSc2
12	#num#	k4
milionů	milion	k4xCgInPc2
Kč	Kč	kA
za	za	k7c4
rozdíl	rozdíl	k1gInSc4
v	v	k7c6
ceně	cena	k1gFnSc6
nemovitostí	nemovitost	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
listopadu	listopad	k1gInSc6
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
projevila	projevit	k5eAaPmAgFnS
o	o	k7c4
zámek	zámek	k1gInSc4
zájem	zájem	k1gInSc1
společnost	společnost	k1gFnSc1
Ithaka	Ithaka	k1gFnSc1
Development	Development	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
v	v	k7c6
něm	on	k3xPp3gInSc6
chtěla	chtít	k5eAaImAgFnS
vytvořit	vytvořit	k5eAaPmF
domov	domov	k1gInSc4
důchodců	důchodce	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
přislíbila	přislíbit	k5eAaPmAgFnS
městu	město	k1gNnSc3
zaplatit	zaplatit	k5eAaPmF
potřebných	potřebný	k2eAgInPc2d1
13	#num#	k4
milionů	milion	k4xCgInPc2
na	na	k7c4
odkup	odkup	k1gInSc4
zámku	zámek	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
prosinci	prosinec	k1gInSc6
však	však	k9
společnost	společnost	k1gFnSc1
nabídku	nabídka	k1gFnSc4
stáhla	stáhnout	k5eAaPmAgFnS
a	a	k8xC
do	do	k7c2
hry	hra	k1gFnSc2
vstoupila	vstoupit	k5eAaPmAgFnS
pardubická	pardubický	k2eAgFnSc1d1
stavební	stavební	k2eAgFnSc1d1
firma	firma	k1gFnSc1
Ingbau	Ingbaus	k1gInSc2
CZ	CZ	kA
<g/>
,	,	kIx,
resp.	resp.	kA
jeden	jeden	k4xCgMnSc1
z	z	k7c2
jejích	její	k3xOp3gMnPc2
jednatelů	jednatel	k1gMnPc2
Igor	Igor	k1gMnSc1
Malý	Malý	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
únoru	únor	k1gInSc6
2013	#num#	k4
však	však	k9
z	z	k7c2
svou	svůj	k3xOyFgFnSc4
nabídku	nabídka	k1gFnSc4
stáhla	stáhnout	k5eAaPmAgFnS
i	i	k9
pardubická	pardubický	k2eAgFnSc1d1
firma	firma	k1gFnSc1
a	a	k8xC
město	město	k1gNnSc1
nabídlo	nabídnout	k5eAaPmAgNnS
kraji	kraj	k1gInSc6
prodloužení	prodloužení	k1gNnSc4
pronájmu	pronájem	k1gInSc2
do	do	k7c2
září	září	k1gNnSc2
toho	ten	k3xDgInSc2
roku	rok	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Kraj	kraj	k1gInSc1
se	se	k3xPyFc4
však	však	k9
rozhodl	rozhodnout	k5eAaPmAgInS
pronájem	pronájem	k1gInSc1
prodloužit	prodloužit	k5eAaPmF
jen	jen	k9
do	do	k7c2
března	březen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
se	se	k3xPyFc4
mezitím	mezitím	k6eAd1
finanční	finanční	k2eAgFnSc1d1
situace	situace	k1gFnSc1
města	město	k1gNnSc2
zlepšila	zlepšit	k5eAaPmAgFnS
<g/>
,	,	kIx,
rozhodlo	rozhodnout	k5eAaPmAgNnS
se	se	k3xPyFc4
jej	on	k3xPp3gInSc2
již	již	k6eAd1
odkoupit	odkoupit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
červnu	červen	k1gInSc6
se	se	k3xPyFc4
pak	pak	k6eAd1
objevili	objevit	k5eAaPmAgMnP
noví	nový	k2eAgMnPc1d1
zájemci	zájemce	k1gMnPc1
o	o	k7c4
odkup	odkup	k1gInSc4
zámku	zámek	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Jednou	jednou	k6eAd1
z	z	k7c2
nich	on	k3xPp3gMnPc2
byla	být	k5eAaImAgFnS
i	i	k9
rodina	rodina	k1gFnSc1
Degerme	Degerme	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
za	za	k7c4
něj	on	k3xPp3gNnSc4
nabídla	nabídnout	k5eAaPmAgFnS
17	#num#	k4
milionů	milion	k4xCgInPc2
<g/>
,	,	kIx,
druhou	druhý	k4xOgFnSc7
pak	pak	k6eAd1
(	(	kIx(
<g/>
opětovně	opětovně	k6eAd1
<g/>
)	)	kIx)
společnost	společnost	k1gFnSc1
Ithaka	Ithaka	k1gFnSc1
Development	Development	k1gInSc1
s	s	k7c7
nabídkou	nabídka	k1gFnSc7
12	#num#	k4
milionů	milion	k4xCgInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Vedení	vedení	k1gNnSc1
města	město	k1gNnSc2
dalo	dát	k5eAaPmAgNnS
přednost	přednost	k1gFnSc4
právě	právě	k6eAd1
rodině	rodina	k1gFnSc6
Degerme	Degerme	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
plánovala	plánovat	k5eAaImAgFnS
zámek	zámek	k1gInSc4
zrekonstruovat	zrekonstruovat	k5eAaPmF
a	a	k8xC
vytvořit	vytvořit	k5eAaPmF
v	v	k7c6
něm	on	k3xPp3gNnSc6
dvě	dva	k4xCgFnPc1
prohlídkové	prohlídkový	k2eAgFnPc1d1
trasy	trasa	k1gFnPc1
a	a	k8xC
část	část	k1gFnSc1
zámku	zámek	k1gInSc2
upravit	upravit	k5eAaPmF
na	na	k7c4
penzion	penzion	k1gInSc4
s	s	k7c7
restaurací	restaurace	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
konci	konec	k1gInSc6
července	červenec	k1gInSc2
pak	pak	k6eAd1
transakci	transakce	k1gFnSc4
posvětilo	posvětit	k5eAaPmAgNnS
i	i	k9
městské	městský	k2eAgNnSc1d1
zastupitelstvo	zastupitelstvo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
června	červen	k1gInSc2
2014	#num#	k4
je	být	k5eAaImIp3nS
zámek	zámek	k1gInSc1
přístupný	přístupný	k2eAgInSc1d1
veřejnosti	veřejnost	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
byla	být	k5eAaImAgFnS
zpřístupněna	zpřístupněn	k2eAgFnSc1d1
Expozice	expozice	k1gFnSc1
historického	historický	k2eAgNnSc2d1
evropského	evropský	k2eAgNnSc2d1
skla	sklo	k1gNnSc2
s	s	k7c7
předměty	předmět	k1gInPc7
ze	z	k7c2
sbírek	sbírka	k1gFnPc2
Uměleckoprůmyslového	uměleckoprůmyslový	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červnu	červen	k1gInSc6
roku	rok	k1gInSc2
2015	#num#	k4
byl	být	k5eAaImAgInS
pro	pro	k7c4
veřejnost	veřejnost	k1gFnSc4
otevřen	otevřen	k2eAgInSc4d1
nový	nový	k2eAgInSc4d1
prohlídkový	prohlídkový	k2eAgInSc4d1
okruh	okruh	k1gInSc4
Expozice	expozice	k1gFnSc2
historických	historický	k2eAgFnPc2d1
hodin	hodina	k1gFnPc2
obrazů	obraz	k1gInPc2
a	a	k8xC
soch	socha	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
sestavena	sestavit	k5eAaPmNgFnS
ze	z	k7c2
sbírek	sbírka	k1gFnPc2
Uměleckoprůmyslového	uměleckoprůmyslový	k2eAgNnSc2d1
musea	museum	k1gNnSc2
Praha	Praha	k1gFnSc1
a	a	k8xC
Muzea	muzeum	k1gNnSc2
Vysočiny	vysočina	k1gFnSc2
Třebíč	Třebíč	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
děti	dítě	k1gFnPc4
jsou	být	k5eAaImIp3nP
připraveny	připravit	k5eAaPmNgInP
dva	dva	k4xCgInPc1
dětské	dětský	k2eAgInPc1d1
prohlídkové	prohlídkový	k2eAgInPc1d1
okruhy	okruh	k1gInPc1
–	–	k?
Dětské	dětský	k2eAgNnSc4d1
království	království	k1gNnSc4
a	a	k8xC
pohádkový	pohádkový	k2eAgInSc4d1
okruh	okruh	k1gInSc4
Na	na	k7c6
Sázavě	Sázava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Mimo	mimo	k7c4
prohlídkové	prohlídkový	k2eAgInPc4d1
okruhy	okruh	k1gInPc4
jsou	být	k5eAaImIp3nP
na	na	k7c6
zámku	zámek	k1gInSc6
připraveny	připravit	k5eAaPmNgFnP
dvě	dva	k4xCgFnPc1
únikové	únikový	k2eAgFnPc1d1
místnosti	místnost	k1gFnPc1
pro	pro	k7c4
únikovou	únikový	k2eAgFnSc4d1
hru	hra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místnosti	místnost	k1gFnPc1
jsou	být	k5eAaImIp3nP
ve	v	k7c6
stylu	styl	k1gInSc6
starověkých	starověký	k2eAgFnPc2d1
bájí	báj	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kromě	kromě	k7c2
prohlídkových	prohlídkový	k2eAgInPc2d1
okruhů	okruh	k1gInPc2
se	se	k3xPyFc4
v	v	k7c6
areálu	areál	k1gInSc6
zámku	zámek	k1gInSc2
nachází	nacházet	k5eAaImIp3nS
zámecká	zámecký	k2eAgFnSc1d1
kavárna	kavárna	k1gFnSc1
Ladurée	Laduré	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Návštěvníci	návštěvník	k1gMnPc1
zámku	zámek	k1gInSc2
mohou	moct	k5eAaImIp3nP
navštívit	navštívit	k5eAaPmF
upravený	upravený	k2eAgInSc4d1
18	#num#	k4
hektarový	hektarový	k2eAgInSc4d1
anglický	anglický	k2eAgInSc4d1
park	park	k1gInSc4
s	s	k7c7
kaskádou	kaskáda	k1gFnSc7
rybníků	rybník	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Návštěvnost	návštěvnost	k1gFnSc1
zámku	zámek	k1gInSc2
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
RokPočet	RokPočet	k1gInSc1
návštěvníků	návštěvník	k1gMnPc2
</s>
<s>
20156	#num#	k4
061	#num#	k4
</s>
<s>
20175	#num#	k4
528	#num#	k4
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
obdržel	obdržet	k5eAaPmAgInS
zámek	zámek	k1gInSc1
třetí	třetí	k4xOgNnSc4
místo	místo	k1gNnSc4
v	v	k7c6
kategorii	kategorie	k1gFnSc6
zvláštní	zvláštní	k2eAgFnSc1d1
cena	cena	k1gFnSc1
ministryně	ministryně	k1gFnSc1
pro	pro	k7c4
místní	místní	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
za	za	k7c4
nejlepší	dobrý	k2eAgFnSc4d3
turistickou	turistický	k2eAgFnSc4d1
prezentaci	prezentace	k1gFnSc4
na	na	k7c6
webových	webový	k2eAgFnPc6d1
stránkách	stránka	k1gFnPc6
v	v	k7c6
kategorii	kategorie	k1gFnSc6
turistické	turistický	k2eAgFnSc2d1
atraktivity	atraktivita	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
K	k	k7c3
listopadu	listopad	k1gInSc3
2020	#num#	k4
je	být	k5eAaImIp3nS
zámek	zámek	k1gInSc4
na	na	k7c4
prodej	prodej	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Ústřední	ústřední	k2eAgInSc4d1
seznam	seznam	k1gInSc4
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Identifikátor	identifikátor	k1gInSc1
záznamu	záznam	k1gInSc2
131944	#num#	k4
:	:	kIx,
Zámek	zámek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památkový	památkový	k2eAgInSc1d1
katalog	katalog	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hledat	hledat	k5eAaImF
dokumenty	dokument	k1gInPc4
v	v	k7c6
Metainformačním	Metainformační	k2eAgInSc6d1
systému	systém	k1gInSc6
NPÚ	NPÚ	kA
.	.	kIx.
↑	↑	k?
http://www.turistika.cz/mista/svetla-nad-sazavou--1	http://www.turistika.cz/mista/svetla-nad-sazavou--1	k4
<g/>
↑	↑	k?
Světlá	světlat	k5eAaImIp3nS
vymění	vyměnit	k5eAaPmIp3nS
s	s	k7c7
krajem	kraj	k1gInSc7
bytovku	bytovka	k1gFnSc4
za	za	k7c4
zámek	zámek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doplatí	doplatit	k5eAaPmIp3nS
mu	on	k3xPp3gMnSc3
12	#num#	k4
milionů	milion	k4xCgInPc2
<g/>
↑	↑	k?
Ze	z	k7c2
zámku	zámek	k1gInSc2
ve	v	k7c4
Světlé	světlý	k2eAgNnSc4d1
chce	chtít	k5eAaImIp3nS
soukromník	soukromník	k1gMnSc1
udělat	udělat	k5eAaPmF
domov	domov	k1gInSc4
pro	pro	k7c4
seniory	senior	k1gMnPc4
<g/>
,	,	kIx,
idnes	idnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Uchazeč	uchazeč	k1gMnSc1
o	o	k7c4
zámek	zámek	k1gInSc4
ve	v	k7c4
Světlé	světlý	k2eAgNnSc4d1
ztratil	ztratit	k5eAaPmAgMnS
zájem	zájem	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
obchodu	obchod	k1gInSc6
větří	větřit	k5eAaImIp3nP
politikaření	politikařený	k2eAgMnPc1d1
<g/>
,	,	kIx,
idnes	idnes	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Prodej	prodej	k1gInSc1
zámku	zámek	k1gInSc2
ve	v	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
Světlé	světlý	k2eAgMnPc4d1
se	se	k3xPyFc4
odkládá	odkládat	k5eAaImIp3nS
<g/>
,	,	kIx,
kupci	kupec	k1gMnPc1
o	o	k7c4
něj	on	k3xPp3gMnSc4
ztratili	ztratit	k5eAaPmAgMnP
zájem	zájem	k1gInSc4
<g/>
,	,	kIx,
idnes	idnes	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Zámek	zámek	k1gInSc1
ve	v	k7c4
Světlé	světlý	k2eAgFnPc4d1
radnice	radnice	k1gFnPc4
koupí	koupě	k1gFnPc2
a	a	k8xC
pak	pak	k6eAd1
se	se	k3xPyFc4
ho	on	k3xPp3gMnSc2
pokusí	pokusit	k5eAaPmIp3nS
co	co	k9
nejdříve	dříve	k6eAd3
prodat	prodat	k5eAaPmF
<g/>
,	,	kIx,
idnes	idnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Zámek	zámek	k1gInSc1
ve	v	k7c4
Světlé	světlý	k2eAgNnSc4d1
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
chtějí	chtít	k5eAaImIp3nP
koupit	koupit	k5eAaPmF
dva	dva	k4xCgMnPc1
zájemci	zájemce	k1gMnPc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
denik	denik	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
O	o	k7c4
zámek	zámek	k1gInSc4
ve	v	k7c4
Světlé	světlý	k2eAgInPc4d1
má	mít	k5eAaImIp3nS
zájem	zájem	k1gInSc4
rodina	rodina	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
pronajímá	pronajímat	k5eAaImIp3nS
jachty	jachta	k1gFnPc1
i	i	k8xC
paláce	palác	k1gInPc1
<g/>
,	,	kIx,
idnes	idnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Rodina	rodina	k1gFnSc1
Degermee	Degermee	k1gFnSc1
nabídla	nabídnout	k5eAaPmAgFnS
za	za	k7c4
zámek	zámek	k1gInSc4
sedmnáct	sedmnáct	k4xCc4
milionů	milion	k4xCgInPc2
<g/>
,	,	kIx,
denik	denik	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Degermee	Degermee	k1gInSc1
<g/>
:	:	kIx,
Zámek	zámek	k1gInSc1
ve	v	k7c6
Světlé	světlý	k2eAgFnSc6d1
potřebuje	potřebovat	k5eAaImIp3nS
čerstvý	čerstvý	k2eAgInSc1d1
vzduch	vzduch	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
20	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
5	#num#	k4
<g/>
plus	plus	k1gInSc1
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Hotovo	hotov	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Světlá	světlat	k5eAaImIp3nS
svůj	svůj	k3xOyFgInSc4
zámek	zámek	k1gInSc4
prodala	prodat	k5eAaPmAgFnS
rodině	rodina	k1gFnSc3
Degermee	Degermee	k1gFnPc2
<g/>
,	,	kIx,
denik	denika	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Informace	informace	k1gFnPc1
o	o	k7c6
prodeji	prodej	k1gInSc6
zámku	zámek	k1gInSc2
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
svetlans	svetlans	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Exit	exit	k1gInSc1
Games	Gamesa	k1gFnPc2
(	(	kIx(
<g/>
únikové	únikový	k2eAgFnSc2d1
mistnosti	mistnost	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zámek	zámek	k1gInSc1
Světlá	světlat	k5eAaImIp3nS
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Návštěvnost	návštěvnost	k1gFnSc1
památek	památka	k1gFnPc2
v	v	k7c6
krajích	kraj	k1gInPc6
ČR	ČR	kA
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
[	[	kIx(
<g/>
PDF	PDF	kA
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgInSc1d1
informační	informační	k2eAgInSc1d1
a	a	k8xC
poradenské	poradenský	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
pro	pro	k7c4
kulturu	kultura	k1gFnSc4
[	[	kIx(
<g/>
cit	cit	k1gInSc4
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
41	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
SVATOŠOVÁ	Svatošová	k1gFnSc1
<g/>
,	,	kIx,
Jitka	Jitka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
národním	národní	k2eAgNnSc6d1
kole	kolo	k1gNnSc6
Zlatého	zlatý	k2eAgInSc2d1
erbu	erb	k1gInSc2
uspěly	uspět	k5eAaPmAgInP
weby	web	k1gInPc1
obce	obec	k1gFnSc2
Myslibořice	Myslibořice	k1gFnSc2
<g/>
,	,	kIx,
cena	cena	k1gFnSc1
patří	patřit	k5eAaImIp3nS
i	i	k9
Krajské	krajský	k2eAgFnSc6d1
knihovně	knihovna	k1gFnSc6
Vysočiny	vysočina	k1gFnSc2
a	a	k8xC
zámkům	zámek	k1gInPc3
ve	v	k7c4
Světlé	světlý	k2eAgInPc4d1
na	na	k7c4
Sázavou	Sázava	k1gFnSc7
a	a	k8xC
ve	v	k7c6
Žďáru	Žďár	k1gInSc6
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kraj	kraj	k1gInSc1
Vysočina	vysočina	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kraj	kraj	k1gInSc1
Vysočina	vysočina	k1gFnSc1
<g/>
,	,	kIx,
2018-04-10	2018-04-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zámecká	zámecký	k2eAgFnSc1d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mansion	Mansion	k1gInSc1
Global	globat	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
zámek	zámek	k1gInSc1
Světlá	světlat	k5eAaImIp3nS
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Stránky	stránka	k1gFnPc1
zámku	zámek	k1gInSc2
</s>
<s>
Zámek	zámek	k1gInSc1
na	na	k7c6
atlasceska	atlasceska	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Česko	Česko	k1gNnSc1
</s>
