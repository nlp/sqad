<s>
Pravděpodobně	pravděpodobně	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1385	#num#	k4
získal	získat	k5eAaPmAgInS
Světlou	světlý	k2eAgFnSc7d1
od	od	k7c2
kláštera	klášter	k1gInSc2
ve	v	k7c6
Vilémově	Vilémův	k2eAgFnSc6d1
do	do	k7c2
zástavy	zástava	k1gFnSc2
Albert	Alberta	k1gFnPc2
ze	z	k7c2
Šternberka	Šternberk	k1gInSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc4
syn	syn	k1gMnSc1
Štěpán	Štěpán	k1gMnSc1
zde	zde	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1393	#num#	k4
nechal	nechat	k5eAaPmAgMnS
postavit	postavit	k5eAaPmF
tvrz	tvrz	k1gFnSc4
<g/>
.	.	kIx.
</s>