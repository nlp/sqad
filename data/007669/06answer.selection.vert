<s>
Císařem	Císař	k1gMnSc7	Císař
byl	být	k5eAaImAgInS	být
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1745	[number]	k4	1745
zvolen	zvolit	k5eAaPmNgMnS	zvolit
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
František	František	k1gMnSc1	František
I.	I.	kA	I.
Štěpán	Štěpán	k1gMnSc1	Štěpán
Lotrinský	lotrinský	k2eAgMnSc1d1	lotrinský
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
císařskou	císařský	k2eAgFnSc7d1	císařská
manželkou	manželka	k1gFnSc7	manželka
<g/>
.	.	kIx.	.
</s>
