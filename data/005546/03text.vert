<s>
Ústa	ústa	k1gNnPc1	ústa
a	a	k8xC	a
ústní	ústní	k2eAgFnSc1d1	ústní
dutina	dutina	k1gFnSc1	dutina
tvoří	tvořit	k5eAaImIp3nS	tvořit
počátek	počátek	k1gInSc4	počátek
trávicí	trávicí	k2eAgFnSc2d1	trávicí
soustavy	soustava	k1gFnSc2	soustava
většiny	většina	k1gFnSc2	většina
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
přijímat	přijímat	k5eAaImF	přijímat
<g/>
,	,	kIx,	,
naporcovat	naporcovat	k5eAaPmF	naporcovat
a	a	k8xC	a
mechanicky	mechanicky	k6eAd1	mechanicky
zpracovávat	zpracovávat	k5eAaImF	zpracovávat
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
promíšení	promíšení	k1gNnSc3	promíšení
potravy	potrava	k1gFnSc2	potrava
se	s	k7c7	s
slinami	slina	k1gFnPc7	slina
a	a	k8xC	a
trávicími	trávicí	k2eAgInPc7d1	trávicí
enzymy	enzym	k1gInPc7	enzym
v	v	k7c6	v
nich	on	k3xPp3gInPc2	on
obsažených	obsažený	k2eAgInPc2d1	obsažený
<g/>
.	.	kIx.	.
</s>
<s>
Zvířecí	zvířecí	k2eAgFnSc1d1	zvířecí
ústní	ústní	k2eAgFnSc1d1	ústní
dutina	dutina	k1gFnSc1	dutina
je	být	k5eAaImIp3nS	být
nazývána	nazýván	k2eAgFnSc1d1	nazývána
tlama	tlama	k1gFnSc1	tlama
nebo	nebo	k8xC	nebo
huba	huba	k1gFnSc1	huba
<g/>
.	.	kIx.	.
</s>
<s>
Ústa	ústa	k1gNnPc1	ústa
(	(	kIx(	(
<g/>
os	osa	k1gFnPc2	osa
<g/>
)	)	kIx)	)
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
ústní	ústní	k2eAgFnSc4d1	ústní
dutinu	dutina	k1gFnSc4	dutina
a	a	k8xC	a
struktury	struktura	k1gFnPc4	struktura
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
umístěné	umístěný	k2eAgInPc1d1	umístěný
-	-	kIx~	-
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
zuby	zub	k1gInPc1	zub
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
orgány	orgán	k1gInPc1	orgán
které	který	k3yQgMnPc4	který
do	do	k7c2	do
dutiny	dutina	k1gFnSc2	dutina
ústí	ústit	k5eAaImIp3nS	ústit
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
slinné	slinný	k2eAgFnPc1d1	slinná
žlázy	žláza	k1gFnPc1	žláza
<g/>
.	.	kIx.	.
</s>
<s>
Dutina	dutina	k1gFnSc1	dutina
ústní	ústní	k2eAgFnSc1d1	ústní
(	(	kIx(	(
<g/>
cavum	cavum	k1gInSc1	cavum
oris	oris	k1gInSc1	oris
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
otevírá	otevírat	k5eAaImIp3nS	otevírat
ústní	ústní	k2eAgFnSc7d1	ústní
štěrbinou	štěrbina	k1gFnSc7	štěrbina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
ohraničena	ohraničit	k5eAaPmNgFnS	ohraničit
pysky	pysk	k1gInPc7	pysk
(	(	kIx(	(
<g/>
rty	ret	k1gInPc7	ret
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Laterálně	laterálně	k6eAd1	laterálně
ohraničena	ohraničen	k2eAgFnSc1d1	ohraničena
tvářemi	tvář	k1gFnPc7	tvář
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
dutině	dutina	k1gFnSc3	dutina
nosní	nosní	k2eAgFnSc1d1	nosní
je	být	k5eAaImIp3nS	být
vymezena	vymezit	k5eAaPmNgFnS	vymezit
tvrdým	tvrdý	k2eAgNnSc7d1	tvrdé
a	a	k8xC	a
měkkým	měkký	k2eAgNnSc7d1	měkké
patrem	patro	k1gNnSc7	patro
<g/>
.	.	kIx.	.
</s>
<s>
Vzadu	vzadu	k6eAd1	vzadu
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
hltanu	hltan	k1gInSc2	hltan
<g/>
.	.	kIx.	.
</s>
<s>
Sliznice	sliznice	k1gFnSc1	sliznice
tváří	tvář	k1gFnPc2	tvář
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
hladká	hladký	k2eAgFnSc1d1	hladká
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
u	u	k7c2	u
přežvýkavců	přežvýkavec	k1gMnPc2	přežvýkavec
tvoří	tvořit	k5eAaImIp3nS	tvořit
kuželovité	kuželovitý	k2eAgFnPc4d1	kuželovitá
mechanické	mechanický	k2eAgFnPc4d1	mechanická
papily	papila	k1gFnPc4	papila
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k6eAd1	uprostřed
na	na	k7c4	na
sliznici	sliznice	k1gFnSc4	sliznice
tvrdého	tvrdý	k2eAgNnSc2d1	tvrdé
patra	patro	k1gNnSc2	patro
je	být	k5eAaImIp3nS	být
patrový	patrový	k2eAgInSc1d1	patrový
šev	šev	k1gInSc1	šev
a	a	k8xC	a
série	série	k1gFnSc1	série
nízkých	nízký	k2eAgInPc2d1	nízký
valů	val	k1gInPc2	val
(	(	kIx(	(
<g/>
rugae	rugae	k6eAd1	rugae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
brání	bránit	k5eAaImIp3nP	bránit
vypadávání	vypadávání	k1gNnSc4	vypadávání
sousta	sousto	k1gNnSc2	sousto
z	z	k7c2	z
ústní	ústní	k2eAgFnSc2d1	ústní
dutiny	dutina	k1gFnSc2	dutina
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
živočichů	živočich	k1gMnPc2	živočich
na	na	k7c6	na
patře	patro	k1gNnSc6	patro
za	za	k7c7	za
řezáky	řezák	k1gInPc7	řezák
ústí	ústit	k5eAaImIp3nS	ústit
řezákový	řezákový	k2eAgInSc1d1	řezákový
průchod	průchod	k1gInSc1	průchod
<g/>
,	,	kIx,	,
spojený	spojený	k2eAgInSc1d1	spojený
s	s	k7c7	s
Jacobsonovým	Jacobsonův	k2eAgInSc7d1	Jacobsonův
orgánem	orgán	k1gInSc7	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
boční	boční	k2eAgFnSc6d1	boční
stěně	stěna	k1gFnSc6	stěna
jsou	být	k5eAaImIp3nP	být
krční	krční	k2eAgFnPc4d1	krční
mandle	mandle	k1gFnPc4	mandle
<g/>
.	.	kIx.	.
</s>
<s>
Potrava	potrava	k1gFnSc1	potrava
požívaná	požívaný	k2eAgFnSc1d1	požívaná
člověkem	člověk	k1gMnSc7	člověk
se	se	k3xPyFc4	se
pomocí	pomoc	k1gFnSc7	pomoc
žvýkání	žvýkání	k1gNnSc2	žvýkání
a	a	k8xC	a
kousání	kousání	k1gNnSc2	kousání
mísí	mísit	k5eAaImIp3nP	mísit
se	s	k7c7	s
slinami	slina	k1gFnPc7	slina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
enzym	enzym	k1gInSc4	enzym
ptyalin	ptyalin	k1gInSc1	ptyalin
(	(	kIx(	(
<g/>
amyláza	amyláza	k1gFnSc1	amyláza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
škroby	škrob	k1gInPc4	škrob
na	na	k7c4	na
dextriny	dextrin	k1gInPc4	dextrin
<g/>
,	,	kIx,	,
za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
podmínek	podmínka	k1gFnPc2	podmínka
až	až	k9	až
na	na	k7c4	na
maltosu	maltosa	k1gFnSc4	maltosa
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
-	-	kIx~	-
největší	veliký	k2eAgInSc1d3	veliký
orgán	orgán	k1gInSc1	orgán
v	v	k7c6	v
dutině	dutina	k1gFnSc6	dutina
ústní	ústní	k2eAgInSc4d1	ústní
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
pohyblivý	pohyblivý	k2eAgMnSc1d1	pohyblivý
a	a	k8xC	a
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
při	při	k7c6	při
polykání	polykání	k1gNnSc6	polykání
potravy	potrava	k1gFnSc2	potrava
a	a	k8xC	a
tvorbě	tvorba	k1gFnSc6	tvorba
řeči	řeč	k1gFnSc2	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
jsou	být	k5eAaImIp3nP	být
soustředěny	soustředěn	k2eAgInPc4d1	soustředěn
chuťové	chuťový	k2eAgInPc4d1	chuťový
receptory	receptor	k1gInPc4	receptor
<g/>
.	.	kIx.	.
</s>
<s>
Chrup	chrup	k1gInSc1	chrup
-	-	kIx~	-
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
32	[number]	k4	32
zubů	zub	k1gInPc2	zub
stálého	stálý	k2eAgInSc2d1	stálý
chrupu	chrup	k1gInSc2	chrup
a	a	k8xC	a
20	[number]	k4	20
zuby	zub	k1gInPc7	zub
u	u	k7c2	u
dočasného	dočasný	k2eAgInSc2d1	dočasný
chrupu	chrup	k1gInSc2	chrup
<g/>
.	.	kIx.	.
</s>
<s>
Zuby	zub	k1gInPc1	zub
dočasného	dočasný	k2eAgInSc2d1	dočasný
chrupu	chrup	k1gInSc2	chrup
se	se	k3xPyFc4	se
prořezávají	prořezávat	k5eAaImIp3nP	prořezávat
od	od	k7c2	od
6	[number]	k4	6
do	do	k7c2	do
24	[number]	k4	24
měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
setrvávají	setrvávat	k5eAaImIp3nP	setrvávat
do	do	k7c2	do
6	[number]	k4	6
roku	rok	k1gInSc2	rok
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
individuální	individuální	k2eAgNnSc1d1	individuální
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
sklovina	sklovina	k1gFnSc1	sklovina
je	být	k5eAaImIp3nS	být
nejtvrdší	tvrdý	k2eAgFnSc7d3	nejtvrdší
strukturou	struktura	k1gFnSc7	struktura
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Chrup	chrup	k1gInSc1	chrup
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
porcování	porcování	k1gNnSc3	porcování
a	a	k8xC	a
rozmělňování	rozmělňování	k1gNnSc3	rozmělňování
potravy	potrava	k1gFnSc2	potrava
a	a	k8xC	a
mluvení	mluvení	k1gNnSc2	mluvení
<g/>
.	.	kIx.	.
</s>
<s>
Korunka	korunka	k1gFnSc1	korunka
je	být	k5eAaImIp3nS	být
vyčnívající	vyčnívající	k2eAgFnSc1d1	vyčnívající
část	část	k1gFnSc1	část
zubu	zub	k1gInSc2	zub
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zužuje	zužovat	k5eAaImIp3nS	zužovat
do	do	k7c2	do
krčku	krček	k1gInSc2	krček
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
dásni	dáseň	k1gFnSc6	dáseň
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
kořene	kořen	k1gInSc2	kořen
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zubním	zubní	k2eAgNnSc6d1	zubní
lůžku	lůžko	k1gNnSc6	lůžko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hrotu	hrot	k1gInSc6	hrot
kořene	kořen	k1gInSc2	kořen
je	být	k5eAaImIp3nS	být
zub	zub	k1gInSc1	zub
zakončen	zakončit	k5eAaPmNgInS	zakončit
otvorem	otvor	k1gInSc7	otvor
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterého	který	k3yRgNnSc2	který
vedou	vést	k5eAaImIp3nP	vést
cévy	céva	k1gFnPc4	céva
a	a	k8xC	a
nervy	nerv	k1gInPc4	nerv
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
součástí	součást	k1gFnSc7	součást
zubu	zub	k1gInSc2	zub
je	být	k5eAaImIp3nS	být
zubovina	zubovina	k1gFnSc1	zubovina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
korunce	korunka	k1gFnSc6	korunka
potažena	potažen	k2eAgFnSc1d1	potažena
sklovinou	sklovina	k1gFnSc7	sklovina
a	a	k8xC	a
na	na	k7c6	na
kořeni	kořen	k1gInSc6	kořen
potažena	potáhnout	k5eAaPmNgFnS	potáhnout
tmelem	tmel	k1gInSc7	tmel
a	a	k8xC	a
ozubicí	ozubice	k1gFnSc7	ozubice
<g/>
.	.	kIx.	.
</s>
<s>
Druhy	druh	k1gInPc1	druh
zubů	zub	k1gInPc2	zub
<g/>
:	:	kIx,	:
řezáky	řezák	k1gInPc1	řezák
<g/>
,	,	kIx,	,
špičáky	špičák	k1gInPc1	špičák
<g/>
,	,	kIx,	,
třenové	třenový	k2eAgInPc1d1	třenový
zuby	zub	k1gInPc1	zub
a	a	k8xC	a
stoličky	stolička	k1gFnPc1	stolička
<g/>
.	.	kIx.	.
</s>
<s>
Vyústění	vyústění	k1gNnSc1	vyústění
slinných	slinný	k2eAgFnPc2d1	slinná
žláz	žláza	k1gFnPc2	žláza
-	-	kIx~	-
exokrinní	exokrinní	k2eAgFnPc1d1	exokrinní
žlázy	žláza	k1gFnPc1	žláza
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
výměšky	výměšek	k1gInPc1	výměšek
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
trávicí	trávicí	k2eAgInPc1d1	trávicí
enzymy	enzym	k1gInPc1	enzym
<g/>
.	.	kIx.	.
</s>
<s>
Ústní	ústní	k2eAgFnSc7d1	ústní
ústrojí	ústroj	k1gFnSc7	ústroj
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ústa	ústa	k1gNnPc1	ústa
a	a	k8xC	a
ústní	ústní	k2eAgFnSc1d1	ústní
dutina	dutina	k1gFnSc1	dutina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
ústa	ústa	k1gNnPc4	ústa
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
