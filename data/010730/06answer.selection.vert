<s>
Kůra	kůra	k1gFnSc1	kůra
plodu	plod	k1gInSc2	plod
(	(	kIx(	(
<g/>
perikarp	perikarp	k1gInSc1	perikarp
<g/>
)	)	kIx)	)
bývá	bývat	k5eAaImIp3nS	bývat
barvy	barva	k1gFnPc4	barva
žluté	žlutý	k2eAgFnPc4d1	žlutá
<g/>
,	,	kIx,	,
oranžové	oranžový	k2eAgFnPc4d1	oranžová
nebo	nebo	k8xC	nebo
šarlatově	šarlatově	k6eAd1	šarlatově
červené	červený	k2eAgNnSc1d1	červené
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
tenká	tenký	k2eAgFnSc1d1	tenká
<g/>
,	,	kIx,	,
přiléhající	přiléhající	k2eAgFnSc1d1	přiléhající
k	k	k7c3	k
dužnině	dužnina	k1gFnSc3	dužnina
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
obtížně	obtížně	k6eAd1	obtížně
loupatelná	loupatelný	k2eAgFnSc1d1	loupatelná
<g/>
.	.	kIx.	.
</s>
