<s>
Latinka	latinka	k1gFnSc1	latinka
(	(	kIx(	(
<g/>
popř.	popř.	kA	popř.
latinské	latinský	k2eAgNnSc1d1	latinské
písmo	písmo	k1gNnSc1	písmo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejpoužívanější	používaný	k2eAgNnSc4d3	nejpoužívanější
písmo	písmo	k1gNnSc4	písmo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
písmo	písmo	k1gNnSc4	písmo
hláskové	hláskový	k2eAgNnSc1d1	hláskové
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgInPc1d1	obsahující
znaky	znak	k1gInPc1	znak
jak	jak	k8xC	jak
pro	pro	k7c4	pro
souhlásky	souhláska	k1gFnPc4	souhláska
<g/>
,	,	kIx,	,
tak	tak	k9	tak
pro	pro	k7c4	pro
samohlásky	samohláska	k1gFnPc4	samohláska
<g/>
.	.	kIx.	.
</s>
<s>
Latinka	latinka	k1gFnSc1	latinka
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
pro	pro	k7c4	pro
latinu	latina	k1gFnSc4	latina
odvozením	odvození	k1gNnSc7	odvození
z	z	k7c2	z
řecké	řecký	k2eAgFnSc2d1	řecká
abecedy	abeceda	k1gFnSc2	abeceda
zprostředkované	zprostředkovaný	k2eAgFnPc1d1	zprostředkovaná
Etrusky	etrusky	k6eAd1	etrusky
asi	asi	k9	asi
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
dvě	dva	k4xCgFnPc1	dva
formy	forma	k1gFnPc1	forma
latinského	latinský	k2eAgNnSc2d1	latinské
písma	písmo	k1gNnSc2	písmo
<g/>
:	:	kIx,	:
kapitála	kapitála	k1gFnSc1	kapitála
a	a	k8xC	a
starší	starý	k2eAgFnSc1d2	starší
římská	římský	k2eAgFnSc1d1	římská
kurzíva	kurzíva	k1gFnSc1	kurzíva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
v	v	k7c4	v
mladší	mladý	k2eAgFnSc4d2	mladší
římskou	římský	k2eAgFnSc4d1	římská
kurzívu	kurzíva	k1gFnSc4	kurzíva
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
kapitálu	kapitál	k1gInSc6	kapitál
navázala	navázat	k5eAaPmAgFnS	navázat
unciála	unciála	k1gFnSc1	unciála
a	a	k8xC	a
polounciála	polounciála	k1gFnSc1	polounciála
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
stěhování	stěhování	k1gNnSc2	stěhování
národů	národ	k1gInPc2	národ
a	a	k8xC	a
christianizace	christianizace	k1gFnSc1	christianizace
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
národní	národní	k2eAgFnPc4d1	národní
písma	písmo	k1gNnPc4	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
sjednotit	sjednotit	k5eAaPmF	sjednotit
způsob	způsob	k1gInSc4	způsob
zápisu	zápis	k1gInSc2	zápis
latinky	latinka	k1gFnSc2	latinka
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
karolinská	karolinský	k2eAgFnSc1d1	Karolinská
minuskule	minuskule	k1gFnSc1	minuskule
<g/>
;	;	kIx,	;
i	i	k9	i
ta	ten	k3xDgFnSc1	ten
ovšem	ovšem	k9	ovšem
podléhala	podléhat	k5eAaImAgFnS	podléhat
změnám	změna	k1gFnPc3	změna
až	až	k6eAd1	až
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
proměnila	proměnit	k5eAaPmAgFnS	proměnit
v	v	k7c4	v
gotické	gotický	k2eAgNnSc4d1	gotické
písmo	písmo	k1gNnSc4	písmo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
rostoucí	rostoucí	k2eAgFnPc1d1	rostoucí
potřeby	potřeba	k1gFnPc1	potřeba
hojnějšího	hojný	k2eAgNnSc2d2	hojnější
a	a	k8xC	a
rychlejšího	rychlý	k2eAgNnSc2d2	rychlejší
psaní	psaní	k1gNnSc2	psaní
hledaly	hledat	k5eAaImAgInP	hledat
nové	nový	k2eAgInPc1d1	nový
tvary	tvar	k1gInPc1	tvar
písma	písmo	k1gNnSc2	písmo
<g/>
:	:	kIx,	:
humanisté	humanista	k1gMnPc1	humanista
proto	proto	k8xC	proto
na	na	k7c6	na
základě	základ	k1gInSc6	základ
tvarů	tvar	k1gInPc2	tvar
kapitály	kapitála	k1gFnSc2	kapitála
a	a	k8xC	a
karolinské	karolinský	k2eAgFnSc2d1	Karolinská
minuskuly	minuskula	k1gFnSc2	minuskula
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
humanistické	humanistický	k2eAgNnSc4d1	humanistické
písmo	písmo	k1gNnSc4	písmo
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
gotické	gotický	k2eAgNnSc1d1	gotické
písmo	písmo	k1gNnSc1	písmo
se	se	k3xPyFc4	se
přeměnilo	přeměnit	k5eAaPmAgNnS	přeměnit
v	v	k7c4	v
novogotické	novogotický	k2eAgNnSc4d1	novogotické
písmo	písmo	k1gNnSc4	písmo
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
nabyla	nabýt	k5eAaPmAgFnS	nabýt
navrch	navrch	k6eAd1	navrch
jeho	jeho	k3xOp3gFnSc1	jeho
kurzívní	kurzívní	k2eAgFnSc1d1	kurzívní
podoba	podoba	k1gFnSc1	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Humanistické	humanistický	k2eAgNnSc4d1	humanistické
písmo	písmo	k1gNnSc4	písmo
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
staletí	staletí	k1gNnPc4	staletí
postupně	postupně	k6eAd1	postupně
novogotické	novogotický	k2eAgNnSc1d1	novogotické
vytlačovalo	vytlačovat	k5eAaImAgNnS	vytlačovat
<g/>
,	,	kIx,	,
až	až	k8xS	až
se	se	k3xPyFc4	se
novogotické	novogotický	k2eAgNnSc1d1	novogotické
písmo	písmo	k1gNnSc1	písmo
nakonec	nakonec	k6eAd1	nakonec
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
přestalo	přestat	k5eAaPmAgNnS	přestat
používat	používat	k5eAaImF	používat
úplně	úplně	k6eAd1	úplně
<g/>
.	.	kIx.	.
</s>
<s>
Latinské	latinský	k2eAgNnSc1d1	latinské
písmo	písmo	k1gNnSc1	písmo
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
asi	asi	k9	asi
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
odvozením	odvození	k1gNnSc7	odvození
od	od	k7c2	od
řeckého	řecký	k2eAgNnSc2d1	řecké
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
Římané	Říman	k1gMnPc1	Říman
přejali	přejmout	k5eAaPmAgMnP	přejmout
od	od	k7c2	od
Etrusků	Etrusk	k1gMnPc2	Etrusk
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
tak	tak	k6eAd1	tak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
archaická	archaický	k2eAgFnSc1d1	archaická
latinka	latinka	k1gFnSc1	latinka
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
mělo	mít	k5eAaImAgNnS	mít
jedno	jeden	k4xCgNnSc4	jeden
písmeno	písmeno	k1gNnSc4	písmeno
více	hodně	k6eAd2	hodně
tvarů	tvar	k1gInPc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
probíhala	probíhat	k5eAaImAgFnS	probíhat
stabilizace	stabilizace	k1gFnSc1	stabilizace
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
liter	litera	k1gFnPc2	litera
a	a	k8xC	a
na	na	k7c4	na
epigrafické	epigrafický	k2eAgFnPc4d1	epigrafická
památky	památka	k1gFnPc4	památka
po	po	k7c6	po
roce	rok	k1gInSc6	rok
200	[number]	k4	200
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
jsou	být	k5eAaImIp3nP	být
psány	psát	k5eAaImNgInP	psát
již	již	k6eAd1	již
stabilizovaným	stabilizovaný	k2eAgNnSc7d1	stabilizované
písmem	písmo	k1gNnSc7	písmo
monumentálním	monumentální	k2eAgNnSc7d1	monumentální
(	(	kIx(	(
<g/>
capitalis	capitalis	k1gInSc1	capitalis
monumentalis	monumentalis	k1gFnSc2	monumentalis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc7	jehož
nejvýznamnějšími	významný	k2eAgMnPc7d3	nejvýznamnější
zástupci	zástupce	k1gMnPc7	zástupce
jsou	být	k5eAaImIp3nP	být
scriptura	scriptura	k1gFnSc1	scriptura
quadrata	quadrata	k1gFnSc1	quadrata
a	a	k8xC	a
scriptura	scriptura	k1gFnSc1	scriptura
actuaria	actuarium	k1gNnSc2	actuarium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knižních	knižní	k2eAgNnPc6d1	knižní
písmech	písmo	k1gNnPc6	písmo
se	se	k3xPyFc4	se
objevovala	objevovat	k5eAaImAgFnS	objevovat
zejména	zejména	k9	zejména
kvadrátní	kvadrátní	k2eAgFnSc1d1	kvadrátní
kapitála	kapitála	k1gFnSc1	kapitála
(	(	kIx(	(
<g/>
capitalis	capitalis	k1gFnSc1	capitalis
quadrata	quadrata	k1gFnSc1	quadrata
<g/>
)	)	kIx)	)
a	a	k8xC	a
rustická	rustický	k2eAgFnSc1d1	rustický
kapitála	kapitála	k1gFnSc1	kapitála
(	(	kIx(	(
<g/>
capitalis	capitalis	k1gInSc1	capitalis
rustica	rustic	k1gInSc2	rustic
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
běžnější	běžný	k2eAgFnPc4d2	běžnější
potřeby	potřeba	k1gFnPc4	potřeba
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
užívána	užíván	k2eAgFnSc1d1	užívána
starší	starý	k2eAgFnSc1d2	starší
římská	římský	k2eAgFnSc1d1	římská
kurzíva	kurzíva	k1gFnSc1	kurzíva
(	(	kIx(	(
<g/>
majuskulní	majuskulní	k2eAgFnSc1d1	majuskulní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
na	na	k7c4	na
starší	starý	k2eAgFnSc4d2	starší
římskou	římský	k2eAgFnSc4d1	římská
kurzívu	kurzíva	k1gFnSc4	kurzíva
navázala	navázat	k5eAaPmAgFnS	navázat
minuskulní	minuskulní	k2eAgFnSc1d1	minuskulní
mladší	mladý	k2eAgFnSc1d2	mladší
římská	římský	k2eAgFnSc1d1	římská
kurzíva	kurzíva	k1gFnSc1	kurzíva
a	a	k8xC	a
z	z	k7c2	z
kvadrátní	kvadrátní	k2eAgFnSc2d1	kvadrátní
kapitály	kapitála	k1gFnSc2	kapitála
se	se	k3xPyFc4	se
až	až	k9	až
do	do	k7c2	do
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
okrouhlejší	okrouhlý	k2eAgFnSc1d2	okrouhlejší
unciála	unciála	k1gFnSc1	unciála
<g/>
.	.	kIx.	.
</s>
<s>
Unciála	unciála	k1gFnSc1	unciála
postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
přešla	přejít	k5eAaPmAgFnS	přejít
v	v	k7c6	v
polounciálu	polounciál	k1gInSc6	polounciál
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
mísily	mísit	k5eAaImAgFnP	mísit
majuskulní	majuskulní	k2eAgInPc1d1	majuskulní
a	a	k8xC	a
minuskulní	minuskulní	k2eAgInPc1d1	minuskulní
tvary	tvar	k1gInPc1	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
stěhování	stěhování	k1gNnSc2	stěhování
národů	národ	k1gInPc2	národ
a	a	k8xC	a
christianizace	christianizace	k1gFnSc1	christianizace
začala	začít	k5eAaPmAgFnS	začít
formovat	formovat	k5eAaImF	formovat
také	také	k9	také
národní	národní	k2eAgNnPc4d1	národní
písma	písmo	k1gNnPc4	písmo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc4	který
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
zhruba	zhruba	k6eAd1	zhruba
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
hlavních	hlavní	k2eAgFnPc2d1	hlavní
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
)	)	kIx)	)
kontinentální	kontinentální	k2eAgInSc1d1	kontinentální
(	(	kIx(	(
<g/>
v	v	k7c6	v
kontinentální	kontinentální	k2eAgFnSc6d1	kontinentální
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
navazovala	navazovat	k5eAaImAgFnS	navazovat
na	na	k7c4	na
mladší	mladý	k2eAgFnSc4d2	mladší
římskou	římský	k2eAgFnSc4d1	římská
kurzívu	kurzíva	k1gFnSc4	kurzíva
<g/>
)	)	kIx)	)
a	a	k8xC	a
2	[number]	k4	2
<g/>
)	)	kIx)	)
insulární	insulární	k2eAgInSc1d1	insulární
(	(	kIx(	(
<g/>
na	na	k7c6	na
Britských	britský	k2eAgInPc6d1	britský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
,	,	kIx,	,
navazovala	navazovat	k5eAaImAgFnS	navazovat
na	na	k7c4	na
unciálu	unciála	k1gFnSc4	unciála
a	a	k8xC	a
polounciálu	polounciál	k1gInSc2	polounciál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
rozrůzněnost	rozrůzněnost	k1gFnSc1	rozrůzněnost
národních	národní	k2eAgNnPc2d1	národní
písem	písmo	k1gNnPc2	písmo
ovšem	ovšem	k9	ovšem
znesnadňovala	znesnadňovat	k5eAaImAgFnS	znesnadňovat
jejich	jejich	k3xOp3gFnSc4	jejich
četbu	četba	k1gFnSc4	četba
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
učiněno	učinit	k5eAaPmNgNnS	učinit
několik	několik	k4yIc1	několik
pokusů	pokus	k1gInPc2	pokus
o	o	k7c4	o
sjednocení	sjednocení	k1gNnSc3	sjednocení
těchto	tento	k3xDgNnPc2	tento
písem	písmo	k1gNnPc2	písmo
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
nejúspěšnější	úspěšný	k2eAgMnSc1d3	nejúspěšnější
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
ve	v	k7c6	v
Franské	franský	k2eAgFnSc6d1	Franská
říši	říš	k1gFnSc6	říš
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
během	během	k7c2	během
karolinské	karolinský	k2eAgFnSc2d1	Karolinská
renesance	renesance	k1gFnSc2	renesance
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
karolinská	karolinský	k2eAgFnSc1d1	Karolinská
minuskula	minuskula	k1gFnSc1	minuskula
(	(	kIx(	(
<g/>
carolina	carolina	k1gFnSc1	carolina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyhovovala	vyhovovat	k5eAaImAgFnS	vyhovovat
všem	všecek	k3xTgInPc3	všecek
důležitým	důležitý	k2eAgInPc3d1	důležitý
požadavkům	požadavek	k1gInPc3	požadavek
<g/>
:	:	kIx,	:
byla	být	k5eAaImAgFnS	být
čitelná	čitelný	k2eAgFnSc1d1	čitelná
<g/>
,	,	kIx,	,
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
i	i	k8xC	i
estetická	estetický	k2eAgFnSc1d1	estetická
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
proto	proto	k8xC	proto
zatlačila	zatlačit	k5eAaPmAgNnP	zatlačit
národní	národní	k2eAgNnPc1d1	národní
písma	písmo	k1gNnPc1	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
variantou	varianta	k1gFnSc7	varianta
byla	být	k5eAaImAgFnS	být
diplomatická	diplomatický	k2eAgFnSc1d1	diplomatická
minuskula	minuskula	k1gFnSc1	minuskula
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
písmena	písmeno	k1gNnPc1	písmeno
izolovala	izolovat	k5eAaBmAgNnP	izolovat
a	a	k8xC	a
omezila	omezit	k5eAaPmAgFnS	omezit
používání	používání	k1gNnPc4	používání
nepřeberného	přeberný	k2eNgNnSc2d1	nepřeberné
množství	množství	k1gNnSc2	množství
zkratek	zkratka	k1gFnPc2	zkratka
(	(	kIx(	(
<g/>
s	s	k7c7	s
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
však	však	k8xC	však
zkratek	zkratka	k1gFnPc2	zkratka
znovu	znovu	k6eAd1	znovu
začalo	začít	k5eAaPmAgNnS	začít
přibývat	přibývat	k5eAaImF	přibývat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
změny	změna	k1gFnSc2	změna
estetického	estetický	k2eAgNnSc2d1	estetické
cítění	cítění	k1gNnSc2	cítění
<g/>
,	,	kIx,	,
způsobu	způsob	k1gInSc2	způsob
seříznutí	seříznutí	k1gNnSc2	seříznutí
pera	pero	k1gNnSc2	pero
i	i	k8xC	i
kvůli	kvůli	k7c3	kvůli
rostoucí	rostoucí	k2eAgFnSc3d1	rostoucí
potřeby	potřeba	k1gFnPc4	potřeba
psaní	psaní	k1gNnSc2	psaní
se	se	k3xPyFc4	se
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
písmo	písmo	k1gNnSc4	písmo
zvertikálnilo	zvertikálnit	k5eAaImAgNnS	zvertikálnit
a	a	k8xC	a
obloučky	oblouček	k1gInPc4	oblouček
caroliny	carolin	k2eAgInPc4d1	carolin
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
lámat	lámat	k5eAaImF	lámat
<g/>
,	,	kIx,	,
části	část	k1gFnPc1	část
písmen	písmeno	k1gNnPc2	písmeno
se	se	k3xPyFc4	se
také	také	k9	také
začaly	začít	k5eAaPmAgFnP	začít
stínovat	stínovat	k5eAaImF	stínovat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
písmena	písmeno	k1gNnSc2	písmeno
byla	být	k5eAaImAgFnS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
silnými	silný	k2eAgInPc7d1	silný
a	a	k8xC	a
vlasovými	vlasový	k2eAgInPc7d1	vlasový
tahy	tah	k1gInPc7	tah
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
gotická	gotický	k2eAgFnSc1d1	gotická
minuskula	minuskula	k1gFnSc1	minuskula
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
s	s	k7c7	s
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
podskupin	podskupina	k1gFnPc2	podskupina
<g/>
:	:	kIx,	:
gotica	gotic	k2eAgFnSc1d1	gotic
texturalis	texturalis	k1gFnSc1	texturalis
(	(	kIx(	(
<g/>
dekorativní	dekorativní	k2eAgFnSc1d1	dekorativní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
gotica	gotic	k2eAgFnSc1d1	gotic
cursiva	cursiva	k1gFnSc1	cursiva
(	(	kIx(	(
<g/>
zběžné	zběžný	k2eAgNnSc1d1	zběžné
písmo	písmo	k1gNnSc1	písmo
<g/>
)	)	kIx)	)
a	a	k8xC	a
bastarda	bastard	k1gMnSc2	bastard
(	(	kIx(	(
<g/>
křížením	křížení	k1gNnSc7	křížení
dvou	dva	k4xCgFnPc2	dva
předchozích	předchozí	k2eAgFnPc2d1	předchozí
<g/>
,	,	kIx,	,
polokurzíva	polokurzíva	k6eAd1	polokurzíva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Novověké	novověký	k2eAgNnSc4d1	novověké
latinské	latinský	k2eAgNnSc4d1	latinské
písmo	písmo	k1gNnSc4	písmo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
středověké	středověký	k2eAgNnSc4d1	středověké
latinské	latinský	k2eAgNnSc4d1	latinské
písmo	písmo	k1gNnSc4	písmo
rozštěpilo	rozštěpit	k5eAaPmAgNnS	rozštěpit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
větve	větev	k1gFnPc4	větev
<g/>
:	:	kIx,	:
zatímco	zatímco	k8xS	zatímco
novogotické	novogotický	k2eAgNnSc1d1	novogotické
písmo	písmo	k1gNnSc1	písmo
navazovalo	navazovat	k5eAaImAgNnS	navazovat
na	na	k7c4	na
středověké	středověký	k2eAgNnSc4d1	středověké
gotické	gotický	k2eAgNnSc4d1	gotické
písmo	písmo	k1gNnSc4	písmo
<g/>
,	,	kIx,	,
humanistické	humanistický	k2eAgNnSc1d1	humanistické
písmo	písmo	k1gNnSc1	písmo
přejalo	přejmout	k5eAaPmAgNnS	přejmout
své	svůj	k3xOyFgInPc4	svůj
tvary	tvar	k1gInPc4	tvar
z	z	k7c2	z
antické	antický	k2eAgFnSc2d1	antická
kapitály	kapitála	k1gFnSc2	kapitála
a	a	k8xC	a
raně	raně	k6eAd1	raně
středověké	středověký	k2eAgFnPc1d1	středověká
karolinské	karolinský	k2eAgFnPc1d1	Karolinská
minuskule	minuskule	k1gFnPc1	minuskule
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
písma	písmo	k1gNnSc2	písmo
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
větvích	větev	k1gFnPc6	větev
reagoval	reagovat	k5eAaBmAgInS	reagovat
především	především	k6eAd1	především
na	na	k7c4	na
rostoucí	rostoucí	k2eAgFnSc4d1	rostoucí
potřebu	potřeba	k1gFnSc4	potřeba
psát	psát	k5eAaImF	psát
častěji	často	k6eAd2	často
a	a	k8xC	a
rychleji	rychle	k6eAd2	rychle
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
kreslené	kreslený	k2eAgNnSc1d1	kreslené
písmo	písmo	k1gNnSc1	písmo
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
psaných	psaný	k2eAgInPc6d1	psaný
textech	text	k1gInPc6	text
rychle	rychle	k6eAd1	rychle
zatlačeno	zatlačen	k2eAgNnSc1d1	zatlačeno
do	do	k7c2	do
pozadí	pozadí	k1gNnSc2	pozadí
polokurzívou	polokurzívý	k2eAgFnSc4d1	polokurzívý
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
humanistického	humanistický	k2eAgNnSc2d1	humanistické
písma	písmo	k1gNnSc2	písmo
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
kurzívou	kurzíva	k1gFnSc7	kurzíva
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
novogotického	novogotický	k2eAgNnSc2d1	novogotické
písma	písmo	k1gNnSc2	písmo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kreslené	kreslený	k2eAgNnSc1d1	kreslené
písmo	písmo	k1gNnSc1	písmo
nicméně	nicméně	k8xC	nicméně
našlo	najít	k5eAaPmAgNnS	najít
hojné	hojný	k2eAgNnSc1d1	hojné
uplatnění	uplatnění	k1gNnSc1	uplatnění
v	v	k7c6	v
knihtisku	knihtisk	k1gInSc6	knihtisk
<g/>
:	:	kIx,	:
humanistické	humanistický	k2eAgNnSc1d1	humanistické
písmo	písmo	k1gNnSc1	písmo
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
antikvy	antikva	k1gFnSc2	antikva
a	a	k8xC	a
italiky	italika	k1gFnSc2	italika
<g/>
,	,	kIx,	,
novogotické	novogotický	k2eAgNnSc1d1	novogotické
písmo	písmo	k1gNnSc1	písmo
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
bastardy	bastard	k1gMnPc4	bastard
<g/>
,	,	kIx,	,
textury	textura	k1gFnSc2	textura
<g/>
,	,	kIx,	,
švabachu	švabach	k1gInSc2	švabach
a	a	k8xC	a
fraktury	fraktura	k1gFnSc2	fraktura
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc4	dva
typy	typ	k1gInPc4	typ
písma	písmo	k1gNnSc2	písmo
spolu	spolu	k6eAd1	spolu
soupeřily	soupeřit	k5eAaImAgFnP	soupeřit
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
humanistické	humanistický	k2eAgNnSc1d1	humanistické
písmo	písmo	k1gNnSc1	písmo
postupně	postupně	k6eAd1	postupně
vytlačovalo	vytlačovat	k5eAaImAgNnS	vytlačovat
novogotické	novogotický	k2eAgNnSc1d1	novogotické
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
úplně	úplně	k6eAd1	úplně
přestalo	přestat	k5eAaPmAgNnS	přestat
používat	používat	k5eAaImF	používat
<g/>
.	.	kIx.	.
</s>
<s>
Humanistické	humanistický	k2eAgNnSc1d1	humanistické
písmo	písmo	k1gNnSc1	písmo
se	se	k3xPyFc4	se
již	již	k6eAd1	již
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
stalo	stát	k5eAaPmAgNnS	stát
výhradním	výhradní	k2eAgInSc7d1	výhradní
typem	typ	k1gInSc7	typ
písma	písmo	k1gNnSc2	písmo
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
textů	text	k1gInPc2	text
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
a	a	k8xC	a
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
se	se	k3xPyFc4	se
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
ostatních	ostatní	k2eAgInPc2d1	ostatní
národních	národní	k2eAgInPc2d1	národní
jazyků	jazyk	k1gInPc2	jazyk
ovšem	ovšem	k9	ovšem
začalo	začít	k5eAaPmAgNnS	začít
pronikat	pronikat	k5eAaImF	pronikat
později	pozdě	k6eAd2	pozdě
<g/>
:	:	kIx,	:
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
francouzštiny	francouzština	k1gFnSc2	francouzština
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
užívalo	užívat	k5eAaImAgNnS	užívat
až	až	k9	až
od	od	k7c2	od
konce	konec	k1gInSc2	konec
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
angličtinu	angličtina	k1gFnSc4	angličtina
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
češtinu	čeština	k1gFnSc4	čeština
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Novogotické	novogotický	k2eAgNnSc1d1	novogotické
písmo	písmo	k1gNnSc1	písmo
se	se	k3xPyFc4	se
užívalo	užívat	k5eAaImAgNnS	užívat
nejdéle	dlouho	k6eAd3	dlouho
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
němčiny	němčina	k1gFnSc2	němčina
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nacistická	nacistický	k2eAgFnSc1d1	nacistická
administrativa	administrativa	k1gFnSc1	administrativa
používání	používání	k1gNnSc2	používání
tohoto	tento	k3xDgNnSc2	tento
písma	písmo	k1gNnSc2	písmo
zakázala	zakázat	k5eAaPmAgFnS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
jazyky	jazyk	k1gInPc4	jazyk
zapisovaných	zapisovaný	k2eAgFnPc2d1	zapisovaná
latinkou	latinka	k1gFnSc7	latinka
používá	používat	k5eAaImIp3nS	používat
pouze	pouze	k6eAd1	pouze
humanistické	humanistický	k2eAgNnSc4d1	humanistické
písmo	písmo	k1gNnSc4	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
latinská	latinský	k2eAgFnSc1d1	Latinská
abeceda	abeceda	k1gFnSc1	abeceda
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
pouze	pouze	k6eAd1	pouze
tato	tento	k3xDgNnPc1	tento
písmena	písmeno	k1gNnPc1	písmeno
<g/>
:	:	kIx,	:
Písmo	písmo	k1gNnSc1	písmo
bylo	být	k5eAaImAgNnS	být
původně	původně	k6eAd1	původně
ryze	ryze	k6eAd1	ryze
majuskulní	majuskulní	k2eAgFnSc1d1	majuskulní
<g/>
.	.	kIx.	.
</s>
<s>
Minuskule	minuskule	k1gFnSc1	minuskule
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
malá	malý	k2eAgNnPc1d1	malé
písmena	písmeno	k1gNnPc1	písmeno
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
z	z	k7c2	z
majuskulí	majuskule	k1gFnPc2	majuskule
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
velkých	velký	k2eAgNnPc2d1	velké
písmen	písmeno	k1gNnPc2	písmeno
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
až	až	k9	až
cca	cca	kA	cca
<g/>
.	.	kIx.	.
v	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
pro	pro	k7c4	pro
snazší	snadný	k2eAgInSc4d2	snadnější
zápis	zápis	k1gInSc4	zápis
pomocí	pomocí	k7c2	pomocí
četných	četný	k2eAgFnPc2d1	četná
ligatur	ligatura	k1gFnPc2	ligatura
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgNnSc4d1	přesné
rozlišení	rozlišení	k1gNnSc4	rozlišení
na	na	k7c4	na
majuskule	majuskule	k1gFnPc4	majuskule
a	a	k8xC	a
minuskule	minuskule	k1gFnSc2	minuskule
a	a	k8xC	a
použití	použití	k1gNnSc2	použití
obojího	obojí	k4xRgMnSc2	obojí
podle	podle	k7c2	podle
přesných	přesný	k2eAgNnPc2d1	přesné
pravidel	pravidlo	k1gNnPc2	pravidlo
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
textu	text	k1gInSc6	text
pochází	pocházet	k5eAaImIp3nS	pocházet
až	až	k9	až
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
humanismu	humanismus	k1gInSc2	humanismus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
zachovat	zachovat	k5eAaPmF	zachovat
jak	jak	k6eAd1	jak
minuskule	minuskule	k1gFnSc1	minuskule
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
majuskule	majuskule	k1gFnSc1	majuskule
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
na	na	k7c4	na
majuskule	majuskule	k1gFnPc4	majuskule
a	a	k8xC	a
minuskule	minuskule	k1gFnPc4	minuskule
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
jen	jen	k9	jen
pro	pro	k7c4	pro
latinku	latinka	k1gFnSc4	latinka
a	a	k8xC	a
řeckou	řecký	k2eAgFnSc4d1	řecká
abecedu	abeceda	k1gFnSc4	abeceda
(	(	kIx(	(
<g/>
a	a	k8xC	a
abecedy	abeceda	k1gFnSc2	abeceda
od	od	k7c2	od
nich	on	k3xPp3gInPc6	on
odvozené	odvozený	k2eAgInPc1d1	odvozený
<g/>
,	,	kIx,	,
např.	např.	kA	např.
cyrilici	cyrilice	k1gFnSc6	cyrilice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
<g/>
,	,	kIx,	,
některá	některý	k3yIgNnPc1	některý
písmena	písmeno	k1gNnPc1	písmeno
se	se	k3xPyFc4	se
měnila	měnit	k5eAaImAgNnP	měnit
<g/>
,	,	kIx,	,
některá	některý	k3yIgNnPc1	některý
přibyla	přibýt	k5eAaPmAgFnS	přibýt
<g/>
.	.	kIx.	.
</s>
<s>
Formou	forma	k1gFnSc7	forma
písmen	písmeno	k1gNnPc2	písmeno
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc7	jejich
ligaturami	ligatura	k1gFnPc7	ligatura
a	a	k8xC	a
použitím	použití	k1gNnSc7	použití
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
latinská	latinský	k2eAgFnSc1d1	Latinská
paleografie	paleografie	k1gFnSc1	paleografie
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
úsporu	úspora	k1gFnSc4	úspora
psacím	psací	k2eAgInSc7d1	psací
materiálem	materiál	k1gInSc7	materiál
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
latina	latina	k1gFnSc1	latina
komplexní	komplexní	k2eAgInSc4d1	komplexní
systém	systém	k1gInSc4	systém
zkratek	zkratka	k1gFnPc2	zkratka
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
text	text	k1gInSc1	text
značně	značně	k6eAd1	značně
zkracoval	zkracovat	k5eAaImAgInS	zkracovat
<g/>
.	.	kIx.	.
</s>
<s>
Zkratky	zkratka	k1gFnPc1	zkratka
nahrazovaly	nahrazovat	k5eAaImAgFnP	nahrazovat
celá	celý	k2eAgNnPc1d1	celé
slova	slovo	k1gNnPc1	slovo
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
jména	jméno	k1gNnSc2	jméno
jako	jako	k8xC	jako
Iesus	Iesus	k1gInSc1	Iesus
<g/>
,	,	kIx,	,
Christus	Christus	k1gInSc1	Christus
<g/>
,	,	kIx,	,
Deus	Deus	k1gInSc1	Deus
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
předložky	předložka	k1gFnSc2	předložka
<g/>
,	,	kIx,	,
spojky	spojka	k1gFnSc2	spojka
<g/>
,	,	kIx,	,
pravidelné	pravidelný	k2eAgFnSc2d1	pravidelná
pádové	pádový	k2eAgFnSc2d1	pádová
koncovky	koncovka	k1gFnSc2	koncovka
apod.	apod.	kA	apod.
(	(	kIx(	(
<g/>
např.	např.	kA	např.
b	b	k?	b
<g/>
9	[number]	k4	9
namísto	namísto	k7c2	namísto
koncovky	koncovka	k1gFnSc2	koncovka
"	"	kIx"	"
<g/>
bus	bus	k1gInSc1	bus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tr	tr	k?	tr
namísto	namísto	k7c2	namísto
koncovky	koncovka	k1gFnSc2	koncovka
"	"	kIx"	"
<g/>
tur	tur	k1gMnSc1	tur
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vypouštění	vypouštění	k1gNnSc4	vypouštění
"	"	kIx"	"
<g/>
m	m	kA	m
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
n	n	k0	n
<g/>
"	"	kIx"	"
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
nahrazování	nahrazování	k1gNnSc4	nahrazování
nadepsanou	nadepsaný	k2eAgFnSc7d1	nadepsaná
čárkou	čárka	k1gFnSc7	čárka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
zkratek	zkratka	k1gFnPc2	zkratka
několik	několik	k4yIc1	několik
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
i	i	k9	i
v	v	k7c6	v
národních	národní	k2eAgInPc6d1	národní
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
např.	např.	kA	např.
&	&	k?	&
(	(	kIx(	(
<g/>
et	et	k?	et
čili	čili	k8xC	čili
slučovací	slučovací	k2eAgFnSc1d1	slučovací
spojka	spojka	k1gFnSc1	spojka
"	"	kIx"	"
<g/>
a	a	k8xC	a
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
italštině	italština	k1gFnSc6	italština
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
kurzívním	kurzívní	k2eAgNnSc6d1	kurzívní
psaní	psaní	k1gNnSc6	psaní
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
velmi	velmi	k6eAd1	velmi
zřídka	zřídka	k6eAd1	zřídka
<g/>
)	)	kIx)	)
používá	používat	k5eAaImIp3nS	používat
vodorovná	vodorovný	k2eAgFnSc1d1	vodorovná
čárka	čárka	k1gFnSc1	čárka
nad	nad	k7c7	nad
písmenem	písmeno	k1gNnSc7	písmeno
jako	jako	k8xC	jako
náhrada	náhrada	k1gFnSc1	náhrada
za	za	k7c2	za
vypuštěné	vypuštěný	k2eAgFnSc2d1	vypuštěná
"	"	kIx"	"
<g/>
m	m	kA	m
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
n	n	k0	n
<g/>
"	"	kIx"	"
apod.	apod.	kA	apod.
Původ	původ	k1gInSc1	původ
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
ad	ad	k7c4	ad
má	mít	k5eAaImIp3nS	mít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
zavináč	zavináč	k1gInSc1	zavináč
@	@	kIx~	@
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
se	se	k3xPyFc4	se
latinka	latinka	k1gFnSc1	latinka
začala	začít	k5eAaPmAgFnS	začít
používat	používat	k5eAaImF	používat
i	i	k9	i
pro	pro	k7c4	pro
jiné	jiný	k2eAgInPc4d1	jiný
jazyky	jazyk	k1gInPc4	jazyk
než	než	k8xS	než
latinu	latina	k1gFnSc4	latina
<g/>
,	,	kIx,	,
tyto	tento	k3xDgInPc4	tento
jazyky	jazyk	k1gInPc4	jazyk
obsahovaly	obsahovat	k5eAaImAgInP	obsahovat
hlásky	hlásek	k1gInPc1	hlásek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dalo	dát	k5eAaPmAgNnS	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
dalším	další	k2eAgMnPc3d1	další
písmenům	písmeno	k1gNnPc3	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
jazyky	jazyk	k1gInPc1	jazyk
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
aby	aby	kYmCp3nS	aby
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
hlásky	hlásek	k1gInPc1	hlásek
vytvářely	vytvářet	k5eAaImAgInP	vytvářet
zcela	zcela	k6eAd1	zcela
nová	nový	k2eAgNnPc4d1	nové
písmena	písmeno	k1gNnPc4	písmeno
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ß	ß	k?	ß
æ	æ	k?	æ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
modifikovaly	modifikovat	k5eAaBmAgInP	modifikovat
stávající	stávající	k2eAgNnPc4d1	stávající
písmena	písmeno	k1gNnPc4	písmeno
pomocí	pomocí	k7c2	pomocí
diakritiky	diakritika	k1gFnSc2	diakritika
(	(	kIx(	(
<g/>
např.	např.	kA	např.
á	á	k0	á
ě	ě	k?	ě
ü	ü	k?	ü
ő	ő	k?	ő
ň	ň	k?	ň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
upadá	upadat	k5eAaPmIp3nS	upadat
znalost	znalost	k1gFnSc1	znalost
latiny	latina	k1gFnSc2	latina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
dávno	dávno	k6eAd1	dávno
mrtvým	mrtvý	k2eAgInSc7d1	mrtvý
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
a	a	k8xC	a
upevňuje	upevňovat	k5eAaImIp3nS	upevňovat
se	se	k3xPyFc4	se
celosvětový	celosvětový	k2eAgInSc1d1	celosvětový
význam	význam	k1gInSc1	význam
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
;	;	kIx,	;
tento	tento	k3xDgInSc1	tento
trend	trend	k1gInSc1	trend
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
posiluje	posilovat	k5eAaImIp3nS	posilovat
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
výpočetní	výpočetní	k2eAgFnSc2d1	výpočetní
techniky	technika	k1gFnSc2	technika
koncem	koncem	k7c2	koncem
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgInSc2	ten
se	se	k3xPyFc4	se
za	za	k7c4	za
"	"	kIx"	"
<g/>
standardní	standardní	k2eAgFnSc4d1	standardní
latinku	latinka	k1gFnSc4	latinka
<g/>
"	"	kIx"	"
běžně	běžně	k6eAd1	běžně
<g/>
[	[	kIx(	[
<g/>
kým	kdo	k3yQnSc7	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
považuje	považovat	k5eAaImIp3nS	považovat
anglická	anglický	k2eAgFnSc1d1	anglická
abeceda	abeceda	k1gFnSc1	abeceda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
následující	následující	k2eAgInPc4d1	následující
znaky	znak	k1gInPc4	znak
<g/>
:	:	kIx,	:
Vedle	vedle	k7c2	vedle
písmen	písmeno	k1gNnPc2	písmeno
se	s	k7c7	s
součástí	součást	k1gFnSc7	součást
písma	písmo	k1gNnSc2	písmo
(	(	kIx(	(
<g/>
ne	ne	k9	ne
však	však	k9	však
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
)	)	kIx)	)
stala	stát	k5eAaPmAgFnS	stát
i	i	k9	i
tzv.	tzv.	kA	tzv.
interpunkční	interpunkční	k2eAgMnPc1d1	interpunkční
znaménka	znaménko	k1gNnSc2	znaménko
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
latinka	latinka	k1gFnSc1	latinka
sdílí	sdílet	k5eAaImIp3nS	sdílet
s	s	k7c7	s
mnohými	mnohý	k2eAgNnPc7d1	mnohé
dalšími	další	k2eAgNnPc7d1	další
písmy	písmo	k1gNnPc7	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
sem	sem	k6eAd1	sem
zejména	zejména	k9	zejména
<g/>
:	:	kIx,	:
.	.	kIx.	.
,	,	kIx,	,
;	;	kIx,	;
:	:	kIx,	:
-	-	kIx~	-
!	!	kIx.	!
</s>
<s>
</s>
<s>
"	"	kIx"	"
'	'	kIx"	'
(	(	kIx(	(
)	)	kIx)	)
...	...	k?	...
pro	pro	k7c4	pro
oddělování	oddělování	k1gNnSc4	oddělování
vět	věta	k1gFnPc2	věta
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
částí	část	k1gFnPc2	část
+	+	kIx~	+
/	/	kIx~	/
<	<	kIx(	<
=	=	kIx~	=
>	>	kIx)	>
[	[	kIx(	[
]	]	kIx)	]
{	{	kIx(	{
}	}	kIx)	}
~	~	kIx~	~
%	%	kIx~	%
&	&	k?	&
...	...	k?	...
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
matematické	matematický	k2eAgInPc4d1	matematický
výrazy	výraz	k1gInPc4	výraz
#	#	kIx~	#
$	$	kIx~	$
*	*	kIx~	*
@	@	kIx~	@
\	\	kIx~	\
^	^	kIx~	^
_	_	kIx~	_
'	'	kIx"	'
|	|	kIx~	|
...	...	k?	...
zvláštní	zvláštní	k2eAgInPc1d1	zvláštní
znaky	znak	k1gInPc1	znak
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nabyly	nabýt	k5eAaPmAgFnP	nabýt
na	na	k7c6	na
významu	význam	k1gInSc6	význam
kvůli	kvůli	k7c3	kvůli
počítačům	počítač	k1gMnPc3	počítač
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
jazyky	jazyk	k1gInPc1	jazyk
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
dnes	dnes	k6eAd1	dnes
píší	psát	k5eAaImIp3nP	psát
latinkou	latinka	k1gFnSc7	latinka
<g/>
,	,	kIx,	,
používají	používat	k5eAaImIp3nP	používat
také	také	k9	také
tzv.	tzv.	kA	tzv.
arabské	arabský	k2eAgFnSc2d1	arabská
číslice	číslice	k1gFnSc2	číslice
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
původně	původně	k6eAd1	původně
používali	používat	k5eAaImAgMnP	používat
římské	římský	k2eAgFnPc4d1	římská
číslice	číslice	k1gFnPc4	číslice
<g/>
,	,	kIx,	,
tvořené	tvořený	k2eAgFnPc4d1	tvořená
kombinacemi	kombinace	k1gFnPc7	kombinace
některých	některý	k3yIgNnPc2	některý
písmen	písmeno	k1gNnPc2	písmeno
latinky	latinka	k1gFnSc2	latinka
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
používané	používaný	k2eAgInPc1d1	používaný
v	v	k7c6	v
jazycích	jazyk	k1gInPc6	jazyk
píšících	píšící	k2eAgFnPc2d1	píšící
latinkou	latinka	k1gFnSc7	latinka
ke	k	k7c3	k
zvláštním	zvláštní	k2eAgInPc3d1	zvláštní
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Arabské	arabský	k2eAgFnPc1d1	arabská
číslice	číslice	k1gFnPc1	číslice
jsou	být	k5eAaImIp3nP	být
následující	následující	k2eAgMnSc1d1	následující
<g/>
:	:	kIx,	:
0	[number]	k4	0
1	[number]	k4	1
2	[number]	k4	2
3	[number]	k4	3
4	[number]	k4	4
5	[number]	k4	5
6	[number]	k4	6
7	[number]	k4	7
8	[number]	k4	8
9	[number]	k4	9
Římská	římský	k2eAgNnPc1d1	římské
čísla	číslo	k1gNnPc1	číslo
jsou	být	k5eAaImIp3nP	být
tvořena	tvořit	k5eAaImNgNnP	tvořit
pomocí	pomocí	k7c2	pomocí
těchto	tento	k3xDgNnPc2	tento
písmen	písmeno	k1gNnPc2	písmeno
latinky	latinka	k1gFnSc2	latinka
<g/>
:	:	kIx,	:
I	i	k9	i
V	v	k7c6	v
X	X	kA	X
L	L	kA	L
C	C	kA	C
D	D	kA	D
M	M	kA	M
Podle	podle	k7c2	podle
mluvčích	mluvčí	k1gFnPc2	mluvčí
Latinku	latinka	k1gFnSc4	latinka
používá	používat	k5eAaImIp3nS	používat
přes	přes	k7c4	přes
2,5	[number]	k4	2,5
miliardy	miliarda	k4xCgFnSc2	miliarda
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jazykových	jazykový	k2eAgMnPc2d1	jazykový
větví	větvit	k5eAaImIp3nS	větvit
Latinku	latinka	k1gFnSc4	latinka
používá	používat	k5eAaImIp3nS	používat
většina	většina	k1gFnSc1	většina
germánských	germánský	k2eAgInPc2d1	germánský
a	a	k8xC	a
románských	románský	k2eAgInPc2d1	románský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
také	také	k9	také
všechny	všechen	k3xTgInPc4	všechen
keltské	keltský	k2eAgInPc4d1	keltský
a	a	k8xC	a
baltské	baltský	k2eAgInPc4d1	baltský
jazyky	jazyk	k1gInPc4	jazyk
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovina	polovina	k1gFnSc1	polovina
slovanských	slovanský	k2eAgInPc2d1	slovanský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
latinka	latinka	k1gFnSc1	latinka
stala	stát	k5eAaPmAgFnS	stát
prostředkem	prostředek	k1gInSc7	prostředek
i	i	k9	i
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
řady	řada	k1gFnSc2	řada
mimoevropských	mimoevropský	k2eAgInPc2d1	mimoevropský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
výsledek	výsledek	k1gInSc4	výsledek
práce	práce	k1gFnSc2	práce
křesťanských	křesťanský	k2eAgMnPc2d1	křesťanský
misionářů	misionář	k1gMnPc2	misionář
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
překládali	překládat	k5eAaImAgMnP	překládat
do	do	k7c2	do
domorodých	domorodý	k2eAgInPc2d1	domorodý
jazyků	jazyk	k1gInPc2	jazyk
Bibli	bible	k1gFnSc4	bible
nebo	nebo	k8xC	nebo
její	její	k3xOp3gFnPc4	její
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Exonymum	Exonymum	k1gInSc1	Exonymum
Indoevropské	indoevropský	k2eAgInPc1d1	indoevropský
jazyky	jazyk	k1gInPc1	jazyk
Jazykové	jazykový	k2eAgFnSc2d1	jazyková
skupiny	skupina	k1gFnSc2	skupina
Pravopis	pravopis	k1gInSc1	pravopis
SAMPA	SAMPA	kA	SAMPA
Seznam	seznam	k1gInSc1	seznam
jazyků	jazyk	k1gMnPc2	jazyk
(	(	kIx(	(
<g/>
čeština	čeština	k1gFnSc1	čeština
<g/>
)	)	kIx)	)
Výslovnost	výslovnost	k1gFnSc1	výslovnost
Abeceda	abeceda	k1gFnSc1	abeceda
Diakritické	diakritický	k2eAgNnSc4d1	diakritické
znaménko	znaménko	k1gNnSc4	znaménko
Písmena	písmeno	k1gNnSc2	písmeno
latinky	latinka	k1gFnSc2	latinka
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
latinka	latinka	k1gFnSc1	latinka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
