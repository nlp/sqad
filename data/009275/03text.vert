<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Čadu	Čad	k1gInSc2	Čad
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
svislých	svislý	k2eAgInPc2d1	svislý
pruhů	pruh	k1gInPc2	pruh
<g/>
:	:	kIx,	:
modrého	modrý	k2eAgMnSc2d1	modrý
<g/>
,	,	kIx,	,
žlutého	žlutý	k2eAgMnSc2d1	žlutý
a	a	k8xC	a
červeného	červený	k2eAgInSc2d1	červený
<g/>
.	.	kIx.	.
</s>
<s>
Barvy	barva	k1gFnPc1	barva
vlajky	vlajka	k1gFnPc1	vlajka
jsou	být	k5eAaImIp3nP	být
kombinací	kombinace	k1gFnSc7	kombinace
francouzského	francouzský	k2eAgInSc2d1	francouzský
vzoru	vzor	k1gInSc2	vzor
a	a	k8xC	a
panafrických	panafrický	k2eAgFnPc2d1	panafrická
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
<g/>
Vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
nejdříve	dříve	k6eAd3	dříve
přijata	přijmout	k5eAaPmNgFnS	přijmout
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
59	[number]	k4	59
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
o	o	k7c6	o
francouzské	francouzský	k2eAgFnSc6d1	francouzská
autonomní	autonomní	k2eAgFnSc6d1	autonomní
republice	republika	k1gFnSc6	republika
Čad	Čad	k1gInSc1	Čad
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
a	a	k8xC	a
používána	používán	k2eAgFnSc1d1	používána
i	i	k9	i
později	pozdě	k6eAd2	pozdě
po	po	k7c6	po
získání	získání	k1gNnSc6	získání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
Potvrzena	potvrzen	k2eAgFnSc1d1	potvrzena
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
ústavou	ústava	k1gFnSc7	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
mnoha	mnoho	k4c3	mnoho
politickým	politický	k2eAgInPc3d1	politický
otřesům	otřes	k1gInPc3	otřes
od	od	k7c2	od
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
,	,	kIx,	,
vlajka	vlajka	k1gFnSc1	vlajka
nebyla	být	k5eNaImAgFnS	být
změněna	změnit	k5eAaPmNgFnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
barvy	barva	k1gFnPc1	barva
a	a	k8xC	a
uspořádání	uspořádání	k1gNnPc1	uspořádání
vlajky	vlajka	k1gFnSc2	vlajka
nejsou	být	k5eNaImIp3nP	být
spojeny	spojit	k5eAaPmNgFnP	spojit
s	s	k7c7	s
žádnou	žádný	k3yNgFnSc7	žádný
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podobné	podobný	k2eAgFnPc1d1	podobná
vlajky	vlajka	k1gFnPc1	vlajka
==	==	k?	==
</s>
</p>
<p>
<s>
Téměř	téměř	k6eAd1	téměř
stejnou	stejný	k2eAgFnSc4d1	stejná
vlajku	vlajka	k1gFnSc4	vlajka
užívá	užívat	k5eAaImIp3nS	užívat
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
rumunská	rumunský	k2eAgFnSc1d1	rumunská
vlajka	vlajka	k1gFnSc1	vlajka
užívá	užívat	k5eAaImIp3nS	užívat
světlejší	světlý	k2eAgInSc1d2	světlejší
odstín	odstín	k1gInSc1	odstín
modré	modrý	k2eAgFnSc2d1	modrá
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
andorrská	andorrský	k2eAgFnSc1d1	andorrská
národní	národní	k2eAgFnSc1d1	národní
vlajka	vlajka	k1gFnSc1	vlajka
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
pouze	pouze	k6eAd1	pouze
odstínem	odstín	k1gInSc7	odstín
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgMnSc1d1	státní
má	mít	k5eAaImIp3nS	mít
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
Moldavská	moldavský	k2eAgFnSc1d1	Moldavská
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
)	)	kIx)	)
uprostřed	uprostřed	k7c2	uprostřed
vlajky	vlajka	k1gFnSc2	vlajka
znak	znak	k1gInSc1	znak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Čadský	čadský	k2eAgInSc1d1	čadský
znak	znak	k1gInSc1	znak
</s>
</p>
<p>
<s>
Čadská	čadský	k2eAgFnSc1d1	Čadská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Čadská	čadský	k2eAgFnSc1d1	Čadská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
