<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
vynálezcem	vynálezce	k1gMnSc7	vynálezce
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1877	[number]	k4	1877
Thomas	Thomas	k1gMnSc1	Thomas
Alva	Alva	k1gMnSc1	Alva
Edison	Edison	k1gMnSc1	Edison
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
jej	on	k3xPp3gMnSc4	on
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1878	[number]	k4	1878
nechal	nechat	k5eAaPmAgMnS	nechat
patentovat	patentovat	k5eAaBmF	patentovat
a	a	k8xC	a
prvním	první	k4xOgInSc7	první
záznamem	záznam	k1gInSc7	záznam
byla	být	k5eAaImAgFnS	být
dětská	dětský	k2eAgFnSc1d1	dětská
říkanka	říkanka	k1gFnSc1	říkanka
Mary	Mary	k1gFnSc2	Mary
had	had	k1gMnSc1	had
a	a	k8xC	a
little	little	k1gFnSc1	little
lamb	lamba	k1gFnPc2	lamba
<g/>
.	.	kIx.	.
</s>
