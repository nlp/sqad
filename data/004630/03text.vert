<s>
Fonograf	fonograf	k1gInSc1	fonograf
je	být	k5eAaImIp3nS	být
první	první	k4xOgInSc1	první
přístroj	přístroj	k1gInSc1	přístroj
na	na	k7c4	na
nahrávání	nahrávání	k1gNnSc4	nahrávání
a	a	k8xC	a
reprodukci	reprodukce	k1gFnSc4	reprodukce
hlasu	hlas	k1gInSc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
vynálezcem	vynálezce	k1gMnSc7	vynálezce
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1877	[number]	k4	1877
Thomas	Thomas	k1gMnSc1	Thomas
Alva	Alva	k1gMnSc1	Alva
Edison	Edison	k1gMnSc1	Edison
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
jej	on	k3xPp3gMnSc4	on
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1878	[number]	k4	1878
nechal	nechat	k5eAaPmAgMnS	nechat
patentovat	patentovat	k5eAaBmF	patentovat
a	a	k8xC	a
prvním	první	k4xOgInSc7	první
záznamem	záznam	k1gInSc7	záznam
byla	být	k5eAaImAgFnS	být
dětská	dětský	k2eAgFnSc1d1	dětská
říkanka	říkanka	k1gFnSc1	říkanka
Mary	Mary	k1gFnSc2	Mary
had	had	k1gMnSc1	had
a	a	k8xC	a
little	little	k1gFnSc1	little
lamb	lamba	k1gFnPc2	lamba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1888	[number]	k4	1888
berlínský	berlínský	k2eAgInSc1d1	berlínský
herec	herec	k1gMnSc1	herec
Emile	Emil	k1gMnSc5	Emil
Berliner	Berlinero	k1gNnPc2	Berlinero
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
gramofon	gramofon	k1gInSc1	gramofon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
jej	on	k3xPp3gMnSc4	on
následně	následně	k6eAd1	následně
nahradil	nahradit	k5eAaPmAgMnS	nahradit
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
následovníka	následovník	k1gMnSc4	následovník
automatofonů	automatofon	k1gInPc2	automatofon
<g/>
.	.	kIx.	.
</s>
<s>
Záznam	záznam	k1gInSc1	záznam
na	na	k7c6	na
fonografu	fonograf	k1gInSc6	fonograf
byl	být	k5eAaImAgInS	být
uchováván	uchovávat	k5eAaImNgInS	uchovávat
na	na	k7c4	na
válečku	válečka	k1gFnSc4	válečka
-	-	kIx~	-
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
lišil	lišit	k5eAaImAgInS	lišit
od	od	k7c2	od
pozdějšího	pozdní	k2eAgInSc2d2	pozdější
gramofonu	gramofon	k1gInSc2	gramofon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
médiem	médium	k1gNnSc7	médium
byla	být	k5eAaImAgFnS	být
plochá	plochý	k2eAgFnSc1d1	plochá
deska	deska	k1gFnSc1	deska
<g/>
.	.	kIx.	.
</s>
<s>
Edison	Edison	k1gInSc1	Edison
byl	být	k5eAaImAgInS	být
při	při	k7c6	při
vynálezu	vynález	k1gInSc6	vynález
fonografu	fonograf	k1gInSc2	fonograf
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
inspirován	inspirovat	k5eAaBmNgInS	inspirovat
přístrojem	přístroj	k1gInSc7	přístroj
zvaným	zvaný	k2eAgInSc7d1	zvaný
phonautograph	phonautograph	k1gInSc4	phonautograph
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
vynálezcem	vynálezce	k1gMnSc7	vynálezce
byl	být	k5eAaImAgMnS	být
Léon	Léon	k1gMnSc1	Léon
Scott	Scott	k1gMnSc1	Scott
a	a	k8xC	a
patentoval	patentovat	k5eAaBmAgMnS	patentovat
jej	on	k3xPp3gNnSc4	on
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1857	[number]	k4	1857
<g/>
.	.	kIx.	.
</s>
<s>
Patrná	patrný	k2eAgFnSc1d1	patrná
je	být	k5eAaImIp3nS	být
podoba	podoba	k1gFnSc1	podoba
názvu	název	k1gInSc2	název
i	i	k8xC	i
některé	některý	k3yIgInPc4	některý
konstrukční	konstrukční	k2eAgInPc4d1	konstrukční
rysy	rys	k1gInPc4	rys
přístroje	přístroj	k1gInSc2	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Phonautograph	Phonautograph	k1gMnSc1	Phonautograph
zakresloval	zakreslovat	k5eAaImAgMnS	zakreslovat
akustické	akustický	k2eAgInPc4d1	akustický
kmity	kmit	k1gInPc4	kmit
na	na	k7c4	na
skleněný	skleněný	k2eAgInSc4d1	skleněný
váleček	váleček	k1gInSc4	váleček
pokrytý	pokrytý	k2eAgInSc4d1	pokrytý
sazemi	saze	k1gFnPc7	saze
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
tak	tak	k9	tak
možno	možno	k6eAd1	možno
studovat	studovat	k5eAaImF	studovat
zvukové	zvukový	k2eAgInPc4d1	zvukový
kmity	kmit	k1gInPc4	kmit
<g/>
,	,	kIx,	,
přístroj	přístroj	k1gInSc1	přístroj
však	však	k9	však
neumožňoval	umožňovat	k5eNaImAgInS	umožňovat
zaznamenaný	zaznamenaný	k2eAgInSc1d1	zaznamenaný
zvuk	zvuk	k1gInSc1	zvuk
přehrát	přehrát	k5eAaPmF	přehrát
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
současně	současně	k6eAd1	současně
s	s	k7c7	s
Edisonem	Edison	k1gInSc7	Edison
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Charles	Charles	k1gMnSc1	Charles
Cros	Cros	k1gInSc4	Cros
jiný	jiný	k2eAgInSc4d1	jiný
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
svůj	svůj	k3xOyFgInSc4	svůj
phonautograph	phonautograph	k1gInSc4	phonautograph
upravit	upravit	k5eAaPmF	upravit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
umožnil	umožnit	k5eAaPmAgInS	umožnit
i	i	k9	i
přehrávání	přehrávání	k1gNnSc4	přehrávání
a	a	k8xC	a
svůj	svůj	k3xOyFgInSc4	svůj
přístroj	přístroj	k1gInSc4	přístroj
nazval	nazvat	k5eAaPmAgMnS	nazvat
paleophone	paleophon	k1gInSc5	paleophon
<g/>
.	.	kIx.	.
</s>
<s>
Cros	Cros	k1gInSc1	Cros
však	však	k9	však
nedovedl	dovést	k5eNaPmAgInS	dovést
svůj	svůj	k3xOyFgInSc4	svůj
přístroj	přístroj	k1gInSc4	přístroj
do	do	k7c2	do
funkční	funkční	k2eAgFnSc2d1	funkční
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
došlo	dojít	k5eAaPmAgNnS	dojít
poprvé	poprvé	k6eAd1	poprvé
k	k	k7c3	k
nahrávání	nahrávání	k1gNnSc3	nahrávání
hlasů	hlas	k1gInPc2	hlas
známých	známý	k2eAgMnPc2d1	známý
herců	herec	k1gMnPc2	herec
a	a	k8xC	a
pěvců	pěvec	k1gMnPc2	pěvec
z	z	k7c2	z
divadel	divadlo	k1gNnPc2	divadlo
na	na	k7c4	na
fonografické	fonografický	k2eAgInPc4d1	fonografický
válečky	váleček	k1gInPc4	váleček
v	v	k7c6	v
ateliéru	ateliér	k1gInSc6	ateliér
fotografa	fotograf	k1gMnSc2	fotograf
Jana	Jan	k1gMnSc2	Jan
Tomáše	Tomáš	k1gMnSc2	Tomáš
<g/>
.	.	kIx.	.
</s>
<s>
Váleček	váleček	k1gInSc1	váleček
byl	být	k5eAaImAgInS	být
zhotoven	zhotovit	k5eAaPmNgInS	zhotovit
z	z	k7c2	z
kovu	kov	k1gInSc2	kov
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
vysoustružena	vysoustružen	k2eAgFnSc1d1	vysoustružena
spirálová	spirálový	k2eAgFnSc1d1	spirálová
drážka	drážka	k1gFnSc1	drážka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vedla	vést	k5eAaImAgFnS	vést
jehlu	jehla	k1gFnSc4	jehla
<g/>
.	.	kIx.	.
</s>
<s>
Edison	Edison	k1gInSc1	Edison
přes	přes	k7c4	přes
tento	tento	k3xDgInSc4	tento
váleček	váleček	k1gInSc4	váleček
u	u	k7c2	u
svého	svůj	k3xOyFgInSc2	svůj
původního	původní	k2eAgInSc2d1	původní
přístroje	přístroj	k1gInSc2	přístroj
nalepil	nalepit	k5eAaPmAgMnS	nalepit
staniol	staniol	k1gInSc4	staniol
(	(	kIx(	(
<g/>
u	u	k7c2	u
pozdějších	pozdní	k2eAgInPc2d2	pozdější
fonografů	fonograf	k1gInPc2	fonograf
se	se	k3xPyFc4	se
namísto	namísto	k7c2	namísto
staniolu	staniol	k1gInSc2	staniol
používala	používat	k5eAaImAgFnS	používat
vrstva	vrstva	k1gFnSc1	vrstva
vosku	vosk	k1gInSc2	vosk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Záznam	záznam	k1gInSc1	záznam
se	se	k3xPyFc4	se
prováděl	provádět	k5eAaImAgInS	provádět
přes	přes	k7c4	přes
kovový	kovový	k2eAgInSc4d1	kovový
trychtýř	trychtýř	k1gInSc4	trychtýř
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zesiloval	zesilovat	k5eAaImAgInS	zesilovat
zvuk	zvuk	k1gInSc4	zvuk
a	a	k8xC	a
dno	dno	k1gNnSc4	dno
měl	mít	k5eAaImAgMnS	mít
přelepené	přelepený	k2eAgFnPc4d1	přelepená
membránou	membrána	k1gFnSc7	membrána
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
z	z	k7c2	z
rybího	rybí	k2eAgInSc2d1	rybí
plynového	plynový	k2eAgInSc2d1	plynový
měchýře	měchýř	k1gInSc2	měchýř
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
membrány	membrána	k1gFnSc2	membrána
byla	být	k5eAaImAgFnS	být
přilepená	přilepený	k2eAgFnSc1d1	přilepená
jehla	jehla	k1gFnSc1	jehla
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
záznamu	záznam	k1gInSc6	záznam
se	se	k3xPyFc4	se
akustické	akustický	k2eAgInPc1d1	akustický
kmity	kmit	k1gInPc1	kmit
membrány	membrána	k1gFnSc2	membrána
přenášely	přenášet	k5eAaImAgInP	přenášet
na	na	k7c4	na
jehlu	jehla	k1gFnSc4	jehla
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
protlačovala	protlačovat	k5eAaImAgFnS	protlačovat
staniol	staniol	k1gInSc4	staniol
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
snímání	snímání	k1gNnSc6	snímání
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
kmity	kmit	k1gInPc1	kmit
jehly	jehla	k1gFnSc2	jehla
přenášely	přenášet	k5eAaImAgInP	přenášet
na	na	k7c4	na
membránu	membrána	k1gFnSc4	membrána
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
kmitáním	kmitání	k1gNnSc7	kmitání
vytvářela	vytvářet	k5eAaImAgFnS	vytvářet
zvuk	zvuk	k1gInSc4	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
válečku	váleček	k1gInSc2	váleček
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
stejné	stejný	k2eAgFnSc3d1	stejná
úhlové	úhlový	k2eAgFnSc3d1	úhlová
rychlosti	rychlost	k1gFnSc3	rychlost
otáčení	otáčení	k1gNnSc2	otáčení
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
i	i	k9	i
stejná	stejný	k2eAgFnSc1d1	stejná
rychlost	rychlost	k1gFnSc1	rychlost
pohybu	pohyb	k1gInSc2	pohyb
celého	celý	k2eAgInSc2d1	celý
povrchu	povrch	k1gInSc2	povrch
válce	válec	k1gInSc2	válec
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
u	u	k7c2	u
desky	deska	k1gFnSc2	deska
neplatí	platit	k5eNaImIp3nS	platit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
fonografu	fonograf	k1gInSc2	fonograf
se	se	k3xPyFc4	se
drážka	drážka	k1gFnSc1	drážka
neodchyluje	odchylovat	k5eNaImIp3nS	odchylovat
do	do	k7c2	do
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
záznam	záznam	k1gInSc1	záznam
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
změnou	změna	k1gFnSc7	změna
její	její	k3xOp3gFnSc2	její
hloubky	hloubka	k1gFnSc2	hloubka
<g/>
,	,	kIx,	,
drážka	drážka	k1gFnSc1	drážka
na	na	k7c6	na
gramofonové	gramofonový	k2eAgFnSc6d1	gramofonová
desce	deska	k1gFnSc6	deska
má	mít	k5eAaImIp3nS	mít
naopak	naopak	k6eAd1	naopak
konstantní	konstantní	k2eAgFnSc4d1	konstantní
hloubku	hloubka	k1gFnSc4	hloubka
a	a	k8xC	a
záznam	záznam	k1gInSc1	záznam
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
vychýlením	vychýlení	k1gNnSc7	vychýlení
drážky	drážka	k1gFnSc2	drážka
do	do	k7c2	do
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
výhodou	výhoda	k1gFnSc7	výhoda
gramofonu	gramofon	k1gInSc2	gramofon
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
stranový	stranový	k2eAgInSc1d1	stranový
záznam	záznam	k1gInSc1	záznam
se	se	k3xPyFc4	se
již	již	k6eAd1	již
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vzniku	vznik	k1gInSc2	vznik
gramofonu	gramofon	k1gInSc2	gramofon
dařilo	dařit	k5eAaImAgNnS	dařit
kopírovat	kopírovat	k5eAaImF	kopírovat
pomocí	pomocí	k7c2	pomocí
pákového	pákový	k2eAgInSc2d1	pákový
mechanismu	mechanismus	k1gInSc2	mechanismus
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
snadno	snadno	k6eAd1	snadno
vyrábět	vyrábět	k5eAaImF	vyrábět
kopie	kopie	k1gFnPc4	kopie
gramofonových	gramofonový	k2eAgFnPc2d1	gramofonová
desek	deska	k1gFnPc2	deska
z	z	k7c2	z
jednou	jednou	k6eAd1	jednou
provedené	provedený	k2eAgFnSc2d1	provedená
nahrávky	nahrávka	k1gFnSc2	nahrávka
a	a	k8xC	a
prodávat	prodávat	k5eAaImF	prodávat
desky	deska	k1gFnPc4	deska
se	s	k7c7	s
záznamem	záznam	k1gInSc7	záznam
ve	v	k7c6	v
větších	veliký	k2eAgFnPc6d2	veliký
sériích	série	k1gFnPc6	série
a	a	k8xC	a
levněji	levně	k6eAd2	levně
<g/>
,	,	kIx,	,
než	než	k8xS	než
fonografické	fonografický	k2eAgInPc1d1	fonografický
válečky	váleček	k1gInPc1	váleček
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
fonografu	fonograf	k1gInSc2	fonograf
se	se	k3xPyFc4	se
tehdejšími	tehdejší	k2eAgFnPc7d1	tehdejší
technologiemi	technologie	k1gFnPc7	technologie
záznam	záznam	k1gInSc4	záznam
kopírovat	kopírovat	k5eAaImF	kopírovat
nezdařilo	zdařit	k5eNaPmAgNnS	zdařit
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávání	nahrávání	k1gNnSc1	nahrávání
na	na	k7c4	na
fonograf	fonograf	k1gInSc4	fonograf
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
provádělo	provádět	k5eAaImAgNnS	provádět
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
zpěvák	zpěvák	k1gMnSc1	zpěvák
zpíval	zpívat	k5eAaImAgMnS	zpívat
před	před	k7c7	před
několika	několik	k4yIc7	několik
fonografy	fonograf	k1gInPc7	fonograf
a	a	k8xC	a
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
písně	píseň	k1gFnSc2	píseň
nahrával	nahrávat	k5eAaImAgInS	nahrávat
tutéž	týž	k3xTgFnSc4	týž
píseň	píseň	k1gFnSc4	píseň
na	na	k7c4	na
dalších	další	k2eAgInPc2d1	další
několik	několik	k4yIc4	několik
válečků	váleček	k1gInPc2	váleček
<g/>
.	.	kIx.	.
</s>
<s>
Plochý	plochý	k2eAgInSc1d1	plochý
tvar	tvar	k1gInSc1	tvar
desky	deska	k1gFnSc2	deska
navíc	navíc	k6eAd1	navíc
později	pozdě	k6eAd2	pozdě
umožnil	umožnit	k5eAaPmAgMnS	umožnit
gramofonové	gramofonový	k2eAgFnSc2d1	gramofonová
desky	deska	k1gFnSc2	deska
již	již	k6eAd1	již
se	s	k7c7	s
záznamem	záznam	k1gInSc7	záznam
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
sériích	série	k1gFnPc6	série
zhotovovat	zhotovovat	k5eAaImF	zhotovovat
lisováním	lisování	k1gNnSc7	lisování
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
nevýhodami	nevýhoda	k1gFnPc7	nevýhoda
fonografu	fonograf	k1gInSc2	fonograf
byly	být	k5eAaImAgInP	být
velké	velký	k2eAgInPc1d1	velký
rozměry	rozměr	k1gInPc1	rozměr
a	a	k8xC	a
kratší	krátký	k2eAgFnSc1d2	kratší
hrací	hrací	k2eAgFnSc1d1	hrací
doba	doba	k1gFnSc1	doba
válečků	váleček	k1gInPc2	váleček
<g/>
.	.	kIx.	.
</s>
