<s>
Letní	letní	k2eAgFnPc1d1	letní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
(	(	kIx(	(
<g/>
též	též	k9	též
letní	letní	k2eAgFnSc1d1	letní
olympiáda	olympiáda	k1gFnSc1	olympiáda
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
LOH	LOH	kA	LOH
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
sportovních	sportovní	k2eAgFnPc2d1	sportovní
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
