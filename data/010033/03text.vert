<p>
<s>
Její	její	k3xOp3gFnSc1	její
královská	královský	k2eAgFnSc1d1	královská
Výsost	výsost	k1gFnSc1	výsost
vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
ze	z	k7c2	z
Sussexu	Sussex	k1gInSc2	Sussex
(	(	kIx(	(
<g/>
rodným	rodný	k2eAgNnSc7d1	rodné
jménem	jméno	k1gNnSc7	jméno
Rachel	Rachel	k1gMnSc1	Rachel
Meghan	Meghan	k1gMnSc1	Meghan
Markle	Markl	k1gInSc5	Markl
<g/>
,	,	kIx,	,
přechýleně	přechýlena	k1gFnSc6	přechýlena
Markleová	Markleová	k1gFnSc1	Markleová
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
*	*	kIx~	*
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1981	[number]	k4	1981
Los	los	k1gInSc1	los
Angeles	Angeles	k1gInSc4	Angeles
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
manželka	manželka	k1gFnSc1	manželka
britského	britský	k2eAgMnSc2d1	britský
královského	královský	k2eAgMnSc2d1	královský
prince	princ	k1gMnSc2	princ
Harryho	Harry	k1gMnSc2	Harry
<g/>
,	,	kIx,	,
vévody	vévoda	k1gMnSc2	vévoda
ze	z	k7c2	z
Sussexu	Sussex	k1gInSc2	Sussex
<g/>
,	,	kIx,	,
humanitární	humanitární	k2eAgFnSc1d1	humanitární
pracovnice	pracovnice	k1gFnSc1	pracovnice
a	a	k8xC	a
bývalá	bývalý	k2eAgFnSc1d1	bývalá
americká	americký	k2eAgFnSc1d1	americká
herečka	herečka	k1gFnSc1	herečka
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgNnP	narodit
se	se	k3xPyFc4	se
a	a	k8xC	a
vyrůstala	vyrůstat	k5eAaImAgFnS	vyrůstat
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mládí	mládí	k1gNnSc1	mládí
a	a	k8xC	a
vzdělání	vzdělání	k1gNnSc1	vzdělání
==	==	k?	==
</s>
</p>
<p>
<s>
Narodila	narodit	k5eAaPmAgNnP	narodit
se	se	k3xPyFc4	se
a	a	k8xC	a
vyrůstala	vyrůstat	k5eAaImAgFnS	vyrůstat
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
smíšeného	smíšený	k2eAgInSc2d1	smíšený
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
Doria	Doria	k1gFnSc1	Doria
Loyce	Loyce	k1gFnSc1	Loyce
Ragland	Ragland	k1gInSc4	Ragland
je	být	k5eAaImIp3nS	být
Afroameričanka	Afroameričanka	k1gFnSc1	Afroameričanka
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgInSc1d1	vlastní
magisterský	magisterský	k2eAgInSc4d1	magisterský
titul	titul	k1gInSc4	titul
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
sociálních	sociální	k2eAgFnPc2d1	sociální
prací	práce	k1gFnPc2	práce
<g/>
,	,	kIx,	,
pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
terapeutka	terapeutka	k1gFnSc1	terapeutka
a	a	k8xC	a
instruktorka	instruktorka	k1gFnSc1	instruktorka
jógy	jóga	k1gFnSc2	jóga
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
Thomas	Thomas	k1gMnSc1	Thomas
Wayne	Wayn	k1gInSc5	Wayn
Markle	Markl	k1gInSc5	Markl
je	být	k5eAaImIp3nS	být
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
držitelem	držitel	k1gMnSc7	držitel
ceny	cena	k1gFnSc2	cena
Emmy	Emma	k1gFnSc2	Emma
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
osvětlovače	osvětlovač	k1gMnSc2	osvětlovač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
pěti	pět	k4xCc2	pět
let	léto	k1gNnPc2	léto
navštěvovala	navštěvovat	k5eAaImAgNnP	navštěvovat
řadu	řada	k1gFnSc4	řada
soukromých	soukromý	k2eAgFnPc2d1	soukromá
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
počínaje	počínaje	k7c7	počínaje
Hollywood	Hollywood	k1gInSc4	Hollywood
Little	Little	k1gFnPc1	Little
Red	Red	k1gFnSc2	Red
Schoolhouse	Schoolhouse	k1gFnSc2	Schoolhouse
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvanácti	dvanáct	k4xCc6	dvanáct
letech	let	k1gInPc6	let
začala	začít	k5eAaPmAgFnS	začít
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
římskokatolické	římskokatolický	k2eAgFnSc6d1	Římskokatolická
dívčí	dívčí	k2eAgFnSc6d1	dívčí
škole	škola	k1gFnSc6	škola
Immaculate	Immaculat	k1gInSc5	Immaculat
Heart	Heart	k1gInSc4	Heart
High	High	k1gInSc4	High
School	Schoola	k1gFnPc2	Schoola
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
Severozápadní	severozápadní	k2eAgFnSc4d1	severozápadní
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
studovala	studovat	k5eAaImAgNnP	studovat
divadelní	divadelní	k2eAgNnPc1d1	divadelní
umění	umění	k1gNnPc1	umění
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
začala	začít	k5eAaPmAgFnS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
americkém	americký	k2eAgNnSc6d1	americké
velvyslanectví	velvyslanectví	k1gNnSc6	velvyslanectví
v	v	k7c4	v
Buenos	Buenos	k1gInSc4	Buenos
Aires	Airesa	k1gFnPc2	Airesa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Herecká	herecký	k2eAgFnSc1d1	herecká
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Ještě	ještě	k9	ještě
v	v	k7c6	v
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgNnSc2	svůj
hereckého	herecký	k2eAgNnSc2d1	herecké
studia	studio	k1gNnSc2	studio
pracovala	pracovat	k5eAaImAgFnS	pracovat
jako	jako	k9	jako
kaligrafka	kaligrafka	k1gFnSc1	kaligrafka
na	na	k7c6	na
volné	volný	k2eAgFnSc6d1	volná
noze	noha	k1gFnSc6	noha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevila	objevit	k5eAaPmAgFnS	objevit
jako	jako	k9	jako
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
v	v	k7c6	v
epizodě	epizoda	k1gFnSc6	epizoda
televizního	televizní	k2eAgInSc2d1	televizní
seriálu	seriál	k1gInSc2	seriál
General	General	k1gMnSc1	General
Hospital	Hospital	k1gMnSc1	Hospital
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
dostala	dostat	k5eAaPmAgFnS	dostat
role	role	k1gFnPc4	role
ve	v	k7c6	v
filmech	film	k1gInPc6	film
<g/>
,	,	kIx,	,
Get	Get	k1gMnSc3	Get
Him	Him	k1gMnSc3	Him
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Greek	Greek	k1gInSc1	Greek
a	a	k8xC	a
Nezapomeň	zapomenout	k5eNaPmRp2nS	zapomenout
na	na	k7c4	na
mě	já	k3xPp1nSc4	já
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
hrála	hrát	k5eAaImAgFnS	hrát
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Šéfové	šéfová	k1gFnSc2	šéfová
na	na	k7c4	na
zabití	zabití	k1gNnSc4	zabití
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
podílela	podílet	k5eAaImAgFnS	podílet
na	na	k7c6	na
natáčení	natáčení	k1gNnSc6	natáčení
krátkého	krátký	k2eAgInSc2d1	krátký
filmu	film	k1gInSc2	film
The	The	k1gMnSc5	The
Candidate	Candidat	k1gMnSc5	Candidat
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
hrála	hrát	k5eAaImAgFnS	hrát
roli	role	k1gFnSc4	role
sekretářky	sekretářka	k1gFnSc2	sekretářka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgNnP	objevit
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
epizodách	epizoda	k1gFnPc6	epizoda
seriálu	seriál	k1gInSc2	seriál
Fringe	Fringe	k1gNnSc2	Fringe
<g/>
,	,	kIx,	,
natáčeného	natáčený	k2eAgNnSc2d1	natáčené
televizní	televizní	k2eAgInSc1d1	televizní
sítí	síť	k1gFnPc2	síť
Fox	fox	k1gInSc1	fox
Broadcasting	Broadcasting	k1gInSc4	Broadcasting
Company	Compana	k1gFnSc2	Compana
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
objevovala	objevovat	k5eAaImAgFnS	objevovat
v	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
seriálu	seriál	k1gInSc6	seriál
Kravaťáci	Kravaťák	k1gMnPc1	Kravaťák
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
roli	role	k1gFnSc6	role
koncipientky	koncipientka	k1gFnSc2	koncipientka
Rachel	Rachel	k1gInSc4	Rachel
Zane	Zan	k1gFnSc2	Zan
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
také	také	k9	také
známa	znám	k2eAgFnSc1d1	známa
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
Amy	Amy	k1gMnSc1	Amy
Jessup	Jessup	k1gMnSc1	Jessup
<g/>
,	,	kIx,	,
speciální	speciální	k2eAgFnPc4d1	speciální
agentky	agentka	k1gFnPc4	agentka
FBI	FBI	kA	FBI
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
ve	v	k7c6	v
sci-fi	scii	k1gNnSc6	sci-fi
seriálu	seriál	k1gInSc2	seriál
Hranice	hranice	k1gFnSc2	hranice
nemožného	možný	k2eNgNnSc2d1	nemožné
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
epizodách	epizoda	k1gFnPc6	epizoda
snímků	snímek	k1gInPc2	snímek
Century	Centura	k1gFnSc2	Centura
City	city	k1gNnSc1	city
<g/>
,	,	kIx,	,
Kriminálka	kriminálka	k1gFnSc1	kriminálka
New	New	k1gMnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
90210	[number]	k4	90210
<g/>
:	:	kIx,	:
Nová	nový	k2eAgFnSc1d1	nová
generace	generace	k1gFnSc1	generace
<g/>
,	,	kIx,	,
Cuts	Cuts	k1gInSc1	Cuts
<g/>
,	,	kIx,	,
Knight	Knight	k2eAgInSc1d1	Knight
Rider	Rider	k1gInSc1	Rider
–	–	k?	–
Legenda	legenda	k1gFnSc1	legenda
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Apostles	Apostles	k1gMnSc1	Apostles
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
League	League	k1gFnSc1	League
<g/>
,	,	kIx,	,
Castle	Castle	k1gFnSc1	Castle
na	na	k7c4	na
zabití	zabití	k1gNnSc4	zabití
<g/>
,	,	kIx,	,
Extant	Extant	k1gInSc4	Extant
a	a	k8xC	a
v	v	k7c6	v
několika	několik	k4yIc6	několik
dalších	další	k1gNnPc6	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Společenské	společenský	k2eAgFnPc4d1	společenská
aktivity	aktivita	k1gFnPc4	aktivita
==	==	k?	==
</s>
</p>
<p>
<s>
Byla	být	k5eAaImAgFnS	být
zakladatelkou	zakladatelka	k1gFnSc7	zakladatelka
a	a	k8xC	a
šéfredaktorkou	šéfredaktorka	k1gFnSc7	šéfredaktorka
webové	webový	k2eAgFnSc2d1	webová
stránky	stránka	k1gFnSc2	stránka
The	The	k1gMnSc1	The
Tig	Tig	k1gMnSc1	Tig
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	se	k3xPyFc4	se
otázkami	otázka	k1gFnPc7	otázka
životního	životní	k2eAgInSc2d1	životní
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2017	[number]	k4	2017
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tuto	tento	k3xDgFnSc4	tento
práci	práce	k1gFnSc4	práce
ukončila	ukončit	k5eAaPmAgFnS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2016	[number]	k4	2016
vyprodukovala	vyprodukovat	k5eAaPmAgFnS	vyprodukovat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
kanadskou	kanadský	k2eAgFnSc7d1	kanadská
oděvní	oděvní	k2eAgFnSc7d1	oděvní
společností	společnost	k1gFnSc7	společnost
Reitmans	Reitmansa	k1gFnPc2	Reitmansa
řadu	řad	k1gInSc2	řad
cenově	cenově	k6eAd1	cenově
dostupných	dostupný	k2eAgInPc2d1	dostupný
dámských	dámský	k2eAgInPc2d1	dámský
pracovních	pracovní	k2eAgInPc2d1	pracovní
oděvů	oděv	k1gInPc2	oděv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
velvyslankyní	velvyslankyně	k1gFnSc7	velvyslankyně
World	Worlda	k1gFnPc2	Worlda
Vision	vision	k1gInSc1	vision
Canada	Canada	k1gFnSc1	Canada
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
niž	jenž	k3xRgFnSc4	jenž
cestovala	cestovat	k5eAaImAgFnS	cestovat
do	do	k7c2	do
Rwandy	Rwanda	k1gFnSc2	Rwanda
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
kampaně	kampaň	k1gFnSc2	kampaň
pro	pro	k7c4	pro
čistou	čistý	k2eAgFnSc4d1	čistá
vodu	voda	k1gFnSc4	voda
(	(	kIx(	(
<g/>
Clean	Clean	k1gMnSc1	Clean
Water	Water	k1gMnSc1	Water
Campaign	Campaign	k1gMnSc1	Campaign
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
poradkyní	poradkyně	k1gFnSc7	poradkyně
pro	pro	k7c4	pro
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
charitu	charita	k1gFnSc4	charita
One	One	k1gMnSc1	One
Young	Young	k1gMnSc1	Young
World	World	k1gMnSc1	World
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
niž	jenž	k3xRgFnSc4	jenž
promluvila	promluvit	k5eAaPmAgFnS	promluvit
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
výročním	výroční	k2eAgInSc6d1	výroční
summitu	summit	k1gInSc6	summit
v	v	k7c6	v
Dublinu	Dublin	k1gInSc6	Dublin
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
tohoto	tento	k3xDgNnSc2	tento
shromáždění	shromáždění	k1gNnSc2	shromáždění
i	i	k9	i
v	v	k7c6	v
kanadské	kanadský	k2eAgFnSc6d1	kanadská
Ottawě	Ottawa	k1gFnSc6	Ottawa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
též	též	k9	též
s	s	k7c7	s
Organizací	organizace	k1gFnSc7	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
programu	program	k1gInSc6	program
United	United	k1gInSc1	United
Nations	Nations	k1gInSc1	Nations
Women	Women	k2eAgInSc1d1	Women
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
při	při	k7c6	při
United	United	k1gInSc1	United
Nations	Nations	k1gInSc4	Nations
Entity	entita	k1gFnSc2	entita
for	forum	k1gNnPc2	forum
Gender	Gender	k1gMnSc1	Gender
Equality	Equalita	k1gFnSc2	Equalita
and	and	k?	and
the	the	k?	the
Empowerment	Empowerment	k1gInSc1	Empowerment
of	of	k?	of
Women	Women	k1gInSc1	Women
(	(	kIx(	(
<g/>
Odbor	odbor	k1gInSc1	odbor
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
rovnost	rovnost	k1gFnSc4	rovnost
pohlaví	pohlaví	k1gNnSc3	pohlaví
a	a	k8xC	a
posílení	posílení	k1gNnSc3	posílení
žen	žena	k1gFnPc2	žena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Záliby	zálib	k1gInPc4	zálib
a	a	k8xC	a
přátelství	přátelství	k1gNnSc4	přátelství
==	==	k?	==
</s>
</p>
<p>
<s>
Označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
za	za	k7c4	za
gurmánku	gurmánka	k1gFnSc4	gurmánka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
vneseno	vnesen	k2eAgNnSc1d1	vneseno
do	do	k7c2	do
její	její	k3xOp3gFnSc2	její
role	role	k1gFnSc2	role
Rachel	Rachel	k1gInSc1	Rachel
Zane	Zane	k1gFnSc4	Zane
v	v	k7c6	v
Kravaťácích	Kravaťák	k1gInPc6	Kravaťák
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
blízkou	blízký	k2eAgFnSc7d1	blízká
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
tenistky	tenistka	k1gFnSc2	tenistka
Sereny	Serena	k1gFnSc2	Serena
Williamsové	Williamsová	k1gFnSc2	Williamsová
<g/>
,	,	kIx,	,
Jessicy	Jessica	k1gMnSc2	Jessica
Mulroney	Mulronea	k1gMnSc2	Mulronea
a	a	k8xC	a
Bena	Bena	k?	Bena
Mulroneyho	Mulroney	k1gMnSc2	Mulroney
ze	z	k7c2	z
zpravodajství	zpravodajství	k1gNnSc2	zpravodajství
E	E	kA	E
<g/>
!	!	kIx.	!
</s>
<s>
News	News	k6eAd1	News
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vztahy	vztah	k1gInPc1	vztah
a	a	k8xC	a
manželství	manželství	k1gNnSc1	manželství
==	==	k?	==
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2011	[number]	k4	2011
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c2	za
filmového	filmový	k2eAgMnSc2d1	filmový
herce	herec	k1gMnSc2	herec
a	a	k8xC	a
producenta	producent	k1gMnSc2	producent
Trevora	Trevor	k1gMnSc2	Trevor
Engelsona	Engelson	k1gMnSc2	Engelson
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgNnSc7	který
měla	mít	k5eAaImAgFnS	mít
vztah	vztah	k1gInSc4	vztah
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2013	[number]	k4	2013
se	se	k3xPyFc4	se
ale	ale	k9	ale
rozešli	rozejít	k5eAaPmAgMnP	rozejít
a	a	k8xC	a
po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
měsících	měsíc	k1gInPc6	měsíc
následoval	následovat	k5eAaImAgInS	následovat
rozvod	rozvod	k1gInSc1	rozvod
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
seznámila	seznámit	k5eAaPmAgFnS	seznámit
s	s	k7c7	s
princem	princ	k1gMnSc7	princ
Harrym	Harrym	k1gInSc4	Harrym
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
s	s	k7c7	s
Corym	Corym	k1gInSc4	Corym
Vitiello	Vitiello	k1gNnSc1	Vitiello
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Její	její	k3xOp3gNnSc1	její
seznámení	seznámení	k1gNnSc1	seznámení
s	s	k7c7	s
britským	britský	k2eAgMnSc7d1	britský
princem	princ	k1gMnSc7	princ
Harrym	Harrymum	k1gNnPc2	Harrymum
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2016	[number]	k4	2016
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
následné	následný	k2eAgNnSc1d1	následné
přátelství	přátelství	k1gNnSc1	přátelství
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
udělaly	udělat	k5eAaPmAgFnP	udělat
nejvyhledávanější	vyhledávaný	k2eAgFnSc4d3	nejvyhledávanější
herečku	herečka	k1gFnSc4	herečka
na	na	k7c6	na
internetovém	internetový	k2eAgMnSc6d1	internetový
vyhledávači	vyhledávač	k1gMnSc6	vyhledávač
Google	Google	k1gFnSc2	Google
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2017	[number]	k4	2017
byla	být	k5eAaImAgFnS	být
ohlášena	ohlásit	k5eAaPmNgFnS	ohlásit
jejich	jejich	k3xOp3gFnSc1	jejich
svatba	svatba	k1gFnSc1	svatba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
19	[number]	k4	19
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2018	[number]	k4	2018
na	na	k7c6	na
královském	královský	k2eAgInSc6d1	královský
zámku	zámek	k1gInSc6	zámek
Windsor	Windsor	k1gInSc1	Windsor
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
svatby	svatba	k1gFnSc2	svatba
žili	žít	k5eAaImAgMnP	žít
společně	společně	k6eAd1	společně
v	v	k7c6	v
domě	dům	k1gInSc6	dům
zvaném	zvaný	k2eAgInSc6d1	zvaný
Nottingham	Nottingham	k1gInSc4	Nottingham
Cottage	Cottag	k1gMnSc2	Cottag
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
pozemcích	pozemek	k1gInPc6	pozemek
Kensingtonského	Kensingtonský	k2eAgInSc2d1	Kensingtonský
paláce	palác	k1gInSc2	palác
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2018	[number]	k4	2018
oznámil	oznámit	k5eAaPmAgInS	oznámit
manželský	manželský	k2eAgInSc1d1	manželský
pár	pár	k1gInSc1	pár
<g/>
,	,	kIx,	,
že	že	k8xS	že
očekávají	očekávat	k5eAaImIp3nP	očekávat
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
dítě	dítě	k1gNnSc4	dítě
a	a	k8xC	a
narození	narození	k1gNnSc1	narození
zdravého	zdravý	k2eAgMnSc2d1	zdravý
syna	syn	k1gMnSc2	syn
oznámili	oznámit	k5eAaPmAgMnP	oznámit
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2019	[number]	k4	2019
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
oficiálním	oficiální	k2eAgInSc6d1	oficiální
instagramovém	instagramový	k2eAgInSc6d1	instagramový
účtu	účet	k1gInSc6	účet
<g/>
.	.	kIx.	.
</s>
<s>
Syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
jménem	jméno	k1gNnSc7	jméno
Archie	Archie	k1gFnSc2	Archie
Harrison	Harrison	k1gMnSc1	Harrison
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2019	[number]	k4	2019
v	v	k7c4	v
5.26	[number]	k4	5.26
ráno	ráno	k1gNnSc4	ráno
britského	britský	k2eAgInSc2d1	britský
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmografie	filmografie	k1gFnSc2	filmografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Film	film	k1gInSc1	film
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Meghan	Meghan	k1gInSc1	Meghan
<g/>
,	,	kIx,	,
Duchess	Duchess	k1gInSc1	Duchess
of	of	k?	of
Sussex	Sussex	k1gInSc1	Sussex
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
