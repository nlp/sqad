<s>
Je	být	k5eAaImIp3nS	být
smíšeného	smíšený	k2eAgInSc2d1	smíšený
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
Doria	Doria	k1gFnSc1	Doria
Loyce	Loyce	k1gFnSc1	Loyce
Ragland	Ragland	k1gInSc4	Ragland
je	být	k5eAaImIp3nS	být
Afroameričanka	Afroameričanka	k1gFnSc1	Afroameričanka
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgInSc1d1	vlastní
magisterský	magisterský	k2eAgInSc4d1	magisterský
titul	titul	k1gInSc4	titul
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
sociálních	sociální	k2eAgFnPc2d1	sociální
prací	práce	k1gFnPc2	práce
<g/>
,	,	kIx,	,
pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
terapeutka	terapeutka	k1gFnSc1	terapeutka
a	a	k8xC	a
instruktorka	instruktorka	k1gFnSc1	instruktorka
jógy	jóga	k1gFnSc2	jóga
<g/>
.	.	kIx.	.
</s>
