<s>
Soustava	soustava	k1gFnSc1	soustava
SI	si	k1gNnSc2	si
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
z	z	k7c2	z
francouzského	francouzský	k2eAgMnSc2d1	francouzský
Le	Le	k1gMnSc2	Le
Systè	Systè	k1gMnSc2	Systè
International	International	k1gMnSc2	International
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Unités	Unités	k1gInSc1	Unités
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mezinárodně	mezinárodně	k6eAd1	mezinárodně
domluvená	domluvený	k2eAgFnSc1d1	domluvená
soustava	soustava	k1gFnSc1	soustava
jednotek	jednotka	k1gFnPc2	jednotka
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
veličin	veličina	k1gFnPc2	veličina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
odvozených	odvozený	k2eAgFnPc2d1	odvozená
jednotek	jednotka	k1gFnPc2	jednotka
a	a	k8xC	a
násobků	násobek	k1gInPc2	násobek
a	a	k8xC	a
dílů	díl	k1gInPc2	díl
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodně	mezinárodně	k6eAd1	mezinárodně
garantuje	garantovat	k5eAaBmIp3nS	garantovat
definice	definice	k1gFnSc1	definice
jednotek	jednotka	k1gFnPc2	jednotka
a	a	k8xC	a
uchování	uchování	k1gNnSc4	uchování
etalonů	etalon	k1gInPc2	etalon
Bureau	Bureaus	k1gInSc2	Bureaus
International	International	k1gFnSc4	International
des	des	k1gNnSc2	des
Poids	Poidsa	k1gFnPc2	Poidsa
et	et	k?	et
Mesures	Mesures	k1gMnSc1	Mesures
v	v	k7c6	v
Sè	Sè	k1gFnSc6	Sè
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
Český	český	k2eAgInSc1d1	český
metrologický	metrologický	k2eAgInSc1d1	metrologický
institut	institut	k1gInSc1	institut
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Základních	základní	k2eAgFnPc2d1	základní
jednotek	jednotka	k1gFnPc2	jednotka
je	být	k5eAaImIp3nS	být
sedm	sedm	k4xCc1	sedm
<g/>
:	:	kIx,	:
metr	metr	k1gMnSc1	metr
<g/>
,	,	kIx,	,
kilogram	kilogram	k1gInSc1	kilogram
<g/>
,	,	kIx,	,
sekunda	sekunda	k1gFnSc1	sekunda
<g/>
,	,	kIx,	,
kelvin	kelvin	k1gInSc1	kelvin
<g/>
,	,	kIx,	,
ampér	ampér	k1gInSc1	ampér
<g/>
,	,	kIx,	,
kandela	kandela	k1gFnSc1	kandela
<g/>
,	,	kIx,	,
mol	mol	k1gInSc1	mol
<g/>
.	.	kIx.	.
</s>
<s>
Odvozené	odvozený	k2eAgFnPc1d1	odvozená
jednotky	jednotka	k1gFnPc1	jednotka
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
výhradně	výhradně	k6eAd1	výhradně
jako	jako	k9	jako
součiny	součin	k1gInPc1	součin
a	a	k8xC	a
podíly	podíl	k1gInPc1	podíl
jednotek	jednotka	k1gFnPc2	jednotka
základních	základní	k2eAgFnPc2d1	základní
<g/>
.	.	kIx.	.
</s>
<s>
Násobky	násobek	k1gInPc1	násobek
a	a	k8xC	a
díly	díl	k1gInPc1	díl
(	(	kIx(	(
<g/>
výhradně	výhradně	k6eAd1	výhradně
dekadické	dekadický	k2eAgNnSc1d1	dekadické
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
pomocí	pomocí	k7c2	pomocí
předpon	předpona	k1gFnPc2	předpona
před	před	k7c7	před
jednotkami	jednotka	k1gFnPc7	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Soustava	soustava	k1gFnSc1	soustava
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
ze	z	k7c2	z
soustavy	soustava	k1gFnSc2	soustava
metr-kilogram-sekunda	metrilogramekund	k1gMnSc2	metr-kilogram-sekund
(	(	kIx(	(
<g/>
MKS	MKS	kA	MKS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
za	za	k7c2	za
Velké	velký	k2eAgFnSc2d1	velká
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
(	(	kIx(	(
<g/>
1789	[number]	k4	1789
až	až	k9	až
1799	[number]	k4	1799
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
letech	let	k1gInPc6	let
rozšiřován	rozšiřován	k2eAgMnSc1d1	rozšiřován
(	(	kIx(	(
<g/>
Británie	Británie	k1gFnSc1	Británie
1860	[number]	k4	1860
<g/>
,	,	kIx,	,
1921	[number]	k4	1921
fyzikální	fyzikální	k2eAgFnPc1d1	fyzikální
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
pak	pak	k6eAd1	pak
začal	začít	k5eAaPmAgInS	začít
samotný	samotný	k2eAgInSc1d1	samotný
standardizační	standardizační	k2eAgInSc1d1	standardizační
proces	proces	k1gInSc1	proces
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
pro	pro	k7c4	pro
subjekty	subjekt	k1gInPc4	subjekt
a	a	k8xC	a
orgány	orgán	k1gInPc4	orgán
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
povinnost	povinnost	k1gFnSc4	povinnost
používat	používat	k5eAaImF	používat
soustavu	soustava	k1gFnSc4	soustava
jednotek	jednotka	k1gFnPc2	jednotka
SI	si	k1gNnSc2	si
ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
505	[number]	k4	505
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
ze	z	k7c2	z
dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1990	[number]	k4	1990
(	(	kIx(	(
<g/>
Zákon	zákon	k1gInSc1	zákon
o	o	k7c4	o
metrologii	metrologie	k1gFnSc4	metrologie
<g/>
;	;	kIx,	;
se	s	k7c7	s
změnami	změna	k1gFnPc7	změna
podle	podle	k7c2	podle
zákonů	zákon	k1gInPc2	zákon
č.	č.	k?	č.
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
20	[number]	k4	20
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
119	[number]	k4	119
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
137	[number]	k4	137
<g/>
/	/	kIx~	/
<g/>
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
/	/	kIx~	/
<g/>
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
226	[number]	k4	226
<g/>
/	/	kIx~	/
<g/>
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
444	[number]	k4	444
<g/>
/	/	kIx~	/
<g/>
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
481	[number]	k4	481
<g/>
/	/	kIx~	/
<g/>
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
223	[number]	k4	223
<g/>
/	/	kIx~	/
<g/>
2009	[number]	k4	2009
a	a	k8xC	a
155	[number]	k4	155
<g/>
/	/	kIx~	/
<g/>
2010	[number]	k4	2010
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
souvisejících	související	k2eAgFnPc2d1	související
vyhlášek	vyhláška	k1gFnPc2	vyhláška
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
MPO	MPO	kA	MPO
vyhlášky	vyhláška	k1gFnSc2	vyhláška
č.	č.	k?	č.
264	[number]	k4	264
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
</s>
<s>
Základních	základní	k2eAgFnPc2d1	základní
jednotek	jednotka	k1gFnPc2	jednotka
je	být	k5eAaImIp3nS	být
sedm	sedm	k4xCc1	sedm
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
stanoveny	stanovit	k5eAaPmNgInP	stanovit
pro	pro	k7c4	pro
následující	následující	k2eAgFnPc4d1	následující
fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
veličiny	veličina	k1gFnPc4	veličina
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
považované	považovaný	k2eAgFnSc2d1	považovaná
v	v	k7c6	v
SI	si	k1gNnSc6	si
za	za	k7c4	za
základní	základní	k2eAgInSc4d1	základní
<g/>
:	:	kIx,	:
Délka	délka	k1gFnSc1	délka
Základní	základní	k2eAgFnSc1d1	základní
jednotkou	jednotka	k1gFnSc7	jednotka
je	být	k5eAaImIp3nS	být
metr	metr	k1gInSc1	metr
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
"	"	kIx"	"
<g/>
m	m	kA	m
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
metr	metr	k1gInSc1	metr
je	být	k5eAaImIp3nS	být
délka	délka	k1gFnSc1	délka
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
urazí	urazit	k5eAaPmIp3nS	urazit
světlo	světlo	k1gNnSc1	světlo
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
za	za	k7c2	za
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
299	[number]	k4	299
:	:	kIx,	:
792	[number]	k4	792
:	:	kIx,	:
458	[number]	k4	458
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
tfrac	tfrac	k1gInSc1	tfrac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
299	[number]	k4	299
<g/>
\	\	kIx~	\
<g/>
,792	,792	k4	,792
<g/>
\	\	kIx~	\
<g/>
,458	,458	k4	,458
<g/>
}}}	}}}	k?	}}}
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc4	hmotnost
Základní	základní	k2eAgFnSc7d1	základní
jednotkou	jednotka	k1gFnSc7	jednotka
hmotnosti	hmotnost	k1gFnSc2	hmotnost
je	být	k5eAaImIp3nS	být
kilogram	kilogram	k1gInSc1	kilogram
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
"	"	kIx"	"
<g/>
kg	kg	kA	kg
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
definován	definovat	k5eAaBmNgInS	definovat
hmotností	hmotnost	k1gFnSc7	hmotnost
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
prototypu	prototyp	k1gInSc2	prototyp
kilogramu	kilogram	k1gInSc2	kilogram
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
uložen	uložit	k5eAaPmNgInS	uložit
v	v	k7c6	v
Mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
úřadě	úřad	k1gInSc6	úřad
pro	pro	k7c4	pro
váhy	váha	k1gFnPc4	váha
a	a	k8xC	a
míry	míra	k1gFnPc4	míra
v	v	k7c6	v
Sè	Sè	k1gFnSc6	Sè
u	u	k7c2	u
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Připravovaná	připravovaný	k2eAgFnSc1d1	připravovaná
nová	nový	k2eAgFnSc1d1	nová
definice	definice	k1gFnSc1	definice
kilogramu	kilogram	k1gInSc2	kilogram
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
opírat	opírat	k5eAaImF	opírat
o	o	k7c4	o
pevně	pevně	k6eAd1	pevně
stanovenou	stanovený	k2eAgFnSc4d1	stanovená
Planckovu	Planckův	k2eAgFnSc4d1	Planckova
konstantu	konstanta	k1gFnSc4	konstanta
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
<s>
Čas	čas	k1gInSc1	čas
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
jednotkou	jednotka	k1gFnSc7	jednotka
času	čas	k1gInSc2	čas
je	být	k5eAaImIp3nS	být
sekunda	sekunda	k1gFnSc1	sekunda
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
"	"	kIx"	"
<g/>
s	s	k7c7	s
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
sekunda	sekunda	k1gFnSc1	sekunda
je	být	k5eAaImIp3nS	být
doba	doba	k1gFnSc1	doba
trvání	trvání	k1gNnPc2	trvání
9	[number]	k4	9
192	[number]	k4	192
631	[number]	k4	631
770	[number]	k4	770
period	perioda	k1gFnPc2	perioda
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
odpovídající	odpovídající	k2eAgMnSc1d1	odpovídající
přechodu	přechod	k1gInSc2	přechod
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
hyperjemnými	hyperjemný	k2eAgFnPc7d1	hyperjemná
hladinami	hladina	k1gFnPc7	hladina
základního	základní	k2eAgInSc2d1	základní
stavu	stav	k1gInSc2	stav
atomu	atom	k1gInSc2	atom
133	[number]	k4	133
<g/>
Cs	Cs	k1gFnPc2	Cs
<g/>
.	.	kIx.	.
</s>
<s>
Elektrický	elektrický	k2eAgInSc4d1	elektrický
proud	proud	k1gInSc4	proud
Základní	základní	k2eAgFnSc7d1	základní
jednotkou	jednotka	k1gFnSc7	jednotka
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
je	být	k5eAaImIp3nS	být
ampér	ampér	k1gInSc1	ampér
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
"	"	kIx"	"
<g/>
A	A	kA	A
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
ampér	ampér	k1gInSc1	ampér
je	být	k5eAaImIp3nS	být
takový	takový	k3xDgInSc4	takový
elektrický	elektrický	k2eAgInSc4d1	elektrický
proud	proud	k1gInSc4	proud
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
přímých	přímý	k2eAgInPc6d1	přímý
rovnoběžných	rovnoběžný	k2eAgInPc6d1	rovnoběžný
vodičích	vodič	k1gInPc6	vodič
o	o	k7c6	o
nekonečné	konečný	k2eNgFnSc6d1	nekonečná
délce	délka	k1gFnSc6	délka
a	a	k8xC	a
zanedbatelném	zanedbatelný	k2eAgInSc6d1	zanedbatelný
průřezu	průřez	k1gInSc6	průřez
vzájemně	vzájemně	k6eAd1	vzájemně
vzdálených	vzdálený	k2eAgFnPc2d1	vzdálená
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
jeden	jeden	k4xCgInSc4	jeden
metr	metr	k1gInSc4	metr
vyvolá	vyvolat	k5eAaPmIp3nS	vyvolat
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
vodiči	vodič	k1gInPc7	vodič
<g />
.	.	kIx.	.
</s>
<s>
sílu	síla	k1gFnSc4	síla
rovnou	rovnou	k6eAd1	rovnou
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
N	N	kA	N
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
metr	metr	k1gInSc4	metr
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Připravovaná	připravovaný	k2eAgFnSc1d1	připravovaná
nová	nový	k2eAgFnSc1d1	nová
definice	definice	k1gFnSc1	definice
ampéru	ampér	k1gInSc2	ampér
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
opírat	opírat	k5eAaImF	opírat
o	o	k7c4	o
pevně	pevně	k6eAd1	pevně
stanovenou	stanovený	k2eAgFnSc4d1	stanovená
hodnotu	hodnota	k1gFnSc4	hodnota
elementárního	elementární	k2eAgInSc2d1	elementární
náboje	náboj	k1gInSc2	náboj
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Termodynamická	termodynamický	k2eAgFnSc1d1	termodynamická
teplota	teplota	k1gFnSc1	teplota
Základní	základní	k2eAgFnSc1d1	základní
jednotkou	jednotka	k1gFnSc7	jednotka
termodynamické	termodynamický	k2eAgFnSc2d1	termodynamická
teploty	teplota	k1gFnSc2	teplota
je	být	k5eAaImIp3nS	být
kelvin	kelvin	k1gInSc1	kelvin
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
kelvin	kelvin	k1gInSc1	kelvin
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
273,16	[number]	k4	273,16
díl	díl	k1gInSc1	díl
absolutní	absolutní	k2eAgFnSc2d1	absolutní
teploty	teplota	k1gFnSc2	teplota
trojného	trojný	k2eAgInSc2d1	trojný
bodu	bod	k1gInSc2	bod
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Připravovaná	připravovaný	k2eAgFnSc1d1	připravovaná
nová	nový	k2eAgFnSc1d1	nová
definice	definice	k1gFnSc1	definice
kelvinu	kelvin	k1gInSc2	kelvin
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
opírat	opírat	k5eAaImF	opírat
o	o	k7c4	o
pevně	pevně	k6eAd1	pevně
stanovenou	stanovený	k2eAgFnSc4d1	stanovená
Boltzmannovu	Boltzmannův	k2eAgFnSc4d1	Boltzmannova
konstantu	konstanta	k1gFnSc4	konstanta
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
<s>
Svítivost	svítivost	k1gFnSc1	svítivost
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
jednotkou	jednotka	k1gFnSc7	jednotka
svítivosti	svítivost	k1gFnSc2	svítivost
je	být	k5eAaImIp3nS	být
kandela	kandela	k1gFnSc1	kandela
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
"	"	kIx"	"
<g/>
cd	cd	kA	cd
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
kandela	kandela	k1gFnSc1	kandela
je	být	k5eAaImIp3nS	být
svítivost	svítivost	k1gFnSc4	svítivost
zdroje	zdroj	k1gInSc2	zdroj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
směru	směr	k1gInSc6	směr
vysílá	vysílat	k5eAaImIp3nS	vysílat
monochromatické	monochromatický	k2eAgNnSc1d1	monochromatické
záření	záření	k1gNnSc1	záření
s	s	k7c7	s
frekvencí	frekvence	k1gFnSc7	frekvence
540	[number]	k4	540
<g/>
×	×	k?	×
<g/>
1012	[number]	k4	1012
Hz	Hz	kA	Hz
<g/>
,	,	kIx,	,
a	a	k8xC	a
jehož	jehož	k3xOyRp3gFnSc1	jehož
zářivost	zářivost	k1gFnSc1	zářivost
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
683	[number]	k4	683
W	W	kA	W
<g/>
/	/	kIx~	/
<g/>
sr	sr	k?	sr
<g/>
.	.	kIx.	.
</s>
<s>
Látkové	látkový	k2eAgNnSc1d1	látkové
množství	množství	k1gNnSc1	množství
Základní	základní	k2eAgFnSc7d1	základní
jednotkou	jednotka	k1gFnSc7	jednotka
látkového	látkový	k2eAgNnSc2d1	látkové
množství	množství	k1gNnSc2	množství
je	být	k5eAaImIp3nS	být
mol	mol	k1gInSc1	mol
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
"	"	kIx"	"
<g/>
mol	mol	k1gInSc1	mol
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
mol	molo	k1gNnPc2	molo
je	být	k5eAaImIp3nS	být
takové	takový	k3xDgNnSc1	takový
množství	množství	k1gNnSc1	množství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
tolik	tolik	k4yIc1	tolik
elementárních	elementární	k2eAgFnPc2d1	elementární
jednotek	jednotka	k1gFnPc2	jednotka
(	(	kIx(	(
<g/>
atomů	atom	k1gInPc2	atom
<g/>
,	,	kIx,	,
molekul	molekula	k1gFnPc2	molekula
<g/>
,	,	kIx,	,
iontů	ion	k1gInPc2	ion
<g/>
,	,	kIx,	,
elektronů	elektron	k1gInPc2	elektron
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
kolik	kolik	k9	kolik
je	být	k5eAaImIp3nS	být
uhlíkových	uhlíkový	k2eAgInPc2d1	uhlíkový
atomů	atom	k1gInPc2	atom
v	v	k7c6	v
12	[number]	k4	12
g	g	kA	g
uhlíku	uhlík	k1gInSc2	uhlík
12	[number]	k4	12
<g/>
C.	C.	kA	C.
Podle	podle	k7c2	podle
současných	současný	k2eAgFnPc2d1	současná
znalostí	znalost	k1gFnPc2	znalost
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
množství	množství	k1gNnSc6	množství
uhlíku	uhlík	k1gInSc2	uhlík
(	(	kIx(	(
<g/>
6,022	[number]	k4	6,022
140	[number]	k4	140
857	[number]	k4	857
±	±	k?	±
0,000	[number]	k4	0,000
000	[number]	k4	000
0	[number]	k4	0
<g/>
74	[number]	k4	74
<g/>
)	)	kIx)	)
<g/>
×	×	k?	×
<g/>
1023	[number]	k4	1023
atomů	atom	k1gInPc2	atom
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Připravovaná	připravovaný	k2eAgFnSc1d1	připravovaná
nová	nový	k2eAgFnSc1d1	nová
definice	definice	k1gFnSc1	definice
molu	mol	k1gInSc2	mol
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
opírat	opírat	k5eAaImF	opírat
o	o	k7c4	o
pevně	pevně	k6eAd1	pevně
stanovenou	stanovený	k2eAgFnSc4d1	stanovená
Avogadrovu	Avogadrův	k2eAgFnSc4d1	Avogadrova
konstantu	konstanta	k1gFnSc4	konstanta
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Původně	původně	k6eAd1	původně
byly	být	k5eAaImAgFnP	být
základní	základní	k2eAgFnPc1d1	základní
jednotky	jednotka	k1gFnPc1	jednotka
stanoveny	stanoven	k2eAgFnPc1d1	stanovena
jako	jako	k9	jako
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
vzájemně	vzájemně	k6eAd1	vzájemně
nezávislé	závislý	k2eNgNnSc1d1	nezávislé
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
jsou	být	k5eAaImIp3nP	být
některé	některý	k3yIgInPc4	některý
definovány	definován	k2eAgInPc4d1	definován
odvozením	odvození	k1gNnSc7	odvození
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
základních	základní	k2eAgFnPc2d1	základní
jednotek	jednotka	k1gFnPc2	jednotka
pomocí	pomocí	k7c2	pomocí
pevně	pevně	k6eAd1	pevně
stanovené	stanovený	k2eAgFnSc2d1	stanovená
hodnoty	hodnota	k1gFnSc2	hodnota
fundamentálních	fundamentální	k2eAgFnPc2d1	fundamentální
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
konstant	konstanta	k1gFnPc2	konstanta
(	(	kIx(	(
<g/>
např.	např.	kA	např.
metr	metr	k1gInSc1	metr
ze	z	k7c2	z
sekundy	sekunda	k1gFnSc2	sekunda
pomocí	pomocí	k7c2	pomocí
rychlosti	rychlost	k1gFnSc2	rychlost
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vyhnout	vyhnout	k5eAaPmF	vyhnout
se	se	k3xPyFc4	se
problémům	problém	k1gInPc3	problém
s	s	k7c7	s
prototypy	prototyp	k1gInPc7	prototyp
jednotek	jednotka	k1gFnPc2	jednotka
a	a	k8xC	a
dosahovat	dosahovat	k5eAaImF	dosahovat
přesnějšího	přesný	k2eAgNnSc2d2	přesnější
stanovení	stanovení	k1gNnSc2	stanovení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
vrcholí	vrcholí	k1gNnSc2	vrcholí
přípravy	příprava	k1gFnSc2	příprava
na	na	k7c6	na
redefinici	redefinice	k1gFnSc6	redefinice
základních	základní	k2eAgFnPc2d1	základní
jednotek	jednotka	k1gFnPc2	jednotka
SI	se	k3xPyFc3	se
tak	tak	k9	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
všechny	všechen	k3xTgFnPc1	všechen
odvozeny	odvozen	k2eAgFnPc1d1	odvozena
od	od	k7c2	od
přírodních	přírodní	k2eAgFnPc2d1	přírodní
konstant	konstanta	k1gFnPc2	konstanta
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
stávající	stávající	k2eAgFnSc2d1	stávající
definice	definice	k1gFnSc2	definice
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Odvozená	odvozený	k2eAgFnSc1d1	odvozená
jednotka	jednotka	k1gFnSc1	jednotka
SI	si	k1gNnSc2	si
<g/>
.	.	kIx.	.
</s>
<s>
Odvozené	odvozený	k2eAgFnPc1d1	odvozená
jednotky	jednotka	k1gFnPc1	jednotka
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
kombinacemi	kombinace	k1gFnPc7	kombinace
(	(	kIx(	(
<g/>
povoleny	povolen	k2eAgFnPc1d1	povolena
jsou	být	k5eAaImIp3nP	být
výhradně	výhradně	k6eAd1	výhradně
součiny	součin	k1gInPc1	součin
a	a	k8xC	a
podíly	podíl	k1gInPc1	podíl
<g/>
)	)	kIx)	)
základních	základní	k2eAgFnPc2d1	základní
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
veličin	veličina	k1gFnPc2	veličina
dostaly	dostat	k5eAaPmAgInP	dostat
samostatné	samostatný	k2eAgInPc1d1	samostatný
názvy	název	k1gInPc1	název
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
kilogram	kilogram	k1gInSc1	kilogram
na	na	k7c4	na
metr	metr	k1gInSc4	metr
krychlový	krychlový	k2eAgInSc4d1	krychlový
<g/>
,	,	kIx,	,
metr	metr	k1gInSc4	metr
čtverečný	čtverečný	k2eAgInSc4d1	čtverečný
<g/>
,	,	kIx,	,
metr	metr	k1gInSc4	metr
krychlový	krychlový	k2eAgInSc4d1	krychlový
<g/>
,	,	kIx,	,
metr	metr	k1gInSc4	metr
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
...	...	k?	...
Odvozené	odvozený	k2eAgFnPc1d1	odvozená
jednotky	jednotka	k1gFnPc1	jednotka
se	s	k7c7	s
samostatným	samostatný	k2eAgInSc7d1	samostatný
názvem	název	k1gInSc7	název
<g/>
:	:	kIx,	:
becquerel	becquerel	k1gInSc1	becquerel
<g/>
,	,	kIx,	,
coulomb	coulomb	k1gInSc1	coulomb
<g/>
,	,	kIx,	,
farad	farad	k1gInSc1	farad
<g/>
,	,	kIx,	,
gray	gray	k1gInPc1	gray
<g/>
,	,	kIx,	,
henry	henry	k1gInPc1	henry
<g/>
,	,	kIx,	,
hertz	hertz	k1gInSc1	hertz
<g/>
,	,	kIx,	,
joule	joule	k1gInSc1	joule
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
katal	katal	k1gInSc1	katal
<g/>
,	,	kIx,	,
lumen	lumen	k1gInSc1	lumen
<g/>
,	,	kIx,	,
lux	lux	k1gInSc1	lux
<g/>
,	,	kIx,	,
newton	newton	k1gInSc1	newton
<g/>
,	,	kIx,	,
ohm	ohm	k1gInSc1	ohm
<g/>
,	,	kIx,	,
pascal	pascal	k1gInSc1	pascal
<g/>
,	,	kIx,	,
radián	radián	k1gInSc1	radián
<g/>
,	,	kIx,	,
siemens	siemens	k1gInSc1	siemens
<g/>
,	,	kIx,	,
sievert	sievert	k1gInSc1	sievert
<g/>
,	,	kIx,	,
steradián	steradián	k1gInSc1	steradián
<g/>
,	,	kIx,	,
tesla	tesla	k1gFnSc1	tesla
<g/>
,	,	kIx,	,
volt	volt	k1gInSc1	volt
<g/>
,	,	kIx,	,
watt	watt	k1gInSc1	watt
<g/>
,	,	kIx,	,
weber	weber	k1gInSc1	weber
<g/>
,	,	kIx,	,
stupeň	stupeň	k1gInSc1	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
Definice	definice	k1gFnSc2	definice
<g/>
,	,	kIx,	,
doporučené	doporučený	k2eAgNnSc1d1	Doporučené
značení	značení	k1gNnSc1	značení
odvozených	odvozený	k2eAgFnPc2d1	odvozená
veličin	veličina	k1gFnPc2	veličina
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc1	jejich
jednotky	jednotka	k1gFnPc1	jednotka
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
závazné	závazný	k2eAgFnPc1d1	závazná
značky	značka	k1gFnPc1	značka
jsou	být	k5eAaImIp3nP	být
upraveny	upravit	k5eAaPmNgFnP	upravit
normami	norma	k1gFnPc7	norma
řady	řada	k1gFnSc2	řada
ČSN	ČSN	kA	ČSN
ISO	ISO	kA	ISO
IEC	IEC	kA	IEC
80000	[number]	k4	80000
"	"	kIx"	"
<g/>
Veličiny	veličina	k1gFnPc1	veličina
a	a	k8xC	a
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
postupně	postupně	k6eAd1	postupně
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
předchozí	předchozí	k2eAgFnSc4d1	předchozí
řadu	řada	k1gFnSc4	řada
ČSN	ČSN	kA	ČSN
ISO	ISO	kA	ISO
31	[number]	k4	31
stejného	stejný	k2eAgInSc2d1	stejný
názvu	název	k1gInSc2	název
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
násobků	násobek	k1gInPc2	násobek
nebo	nebo	k8xC	nebo
dílů	díl	k1gInPc2	díl
základních	základní	k2eAgFnPc2d1	základní
nebo	nebo	k8xC	nebo
odvozených	odvozený	k2eAgFnPc2d1	odvozená
jednotek	jednotka	k1gFnPc2	jednotka
(	(	kIx(	(
<g/>
výhradně	výhradně	k6eAd1	výhradně
dekadických	dekadický	k2eAgInPc2d1	dekadický
<g/>
)	)	kIx)	)
slouží	sloužit	k5eAaImIp3nS	sloužit
předpony	předpona	k1gFnPc4	předpona
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
Předpony	předpona	k1gFnPc1	předpona
soustavy	soustava	k1gFnSc2	soustava
SI	si	k1gNnSc2	si
<g/>
.	.	kIx.	.
</s>
<s>
Vedlejší	vedlejší	k2eAgFnPc1d1	vedlejší
jednotky	jednotka	k1gFnPc1	jednotka
jsou	být	k5eAaImIp3nP	být
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
dříve	dříve	k6eAd2	dříve
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
všeobecnou	všeobecný	k2eAgFnSc4d1	všeobecná
rozšířenost	rozšířenost	k1gFnSc4	rozšířenost
a	a	k8xC	a
užitečnost	užitečnost	k1gFnSc4	užitečnost
řazeny	řadit	k5eAaImNgFnP	řadit
do	do	k7c2	do
soustavy	soustava	k1gFnSc2	soustava
SI	si	k1gNnSc2	si
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
nebyly	být	k5eNaImAgFnP	být
odvozeny	odvodit	k5eAaPmNgFnP	odvodit
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
mimosoustavové	mimosoustavový	k2eAgNnSc4d1	mimosoustavový
<g/>
.	.	kIx.	.
</s>
<s>
Soustava	soustava	k1gFnSc1	soustava
SI	se	k3xPyFc3	se
akceptuje	akceptovat	k5eAaBmIp3nS	akceptovat
používat	používat	k5eAaImF	používat
souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
jednotkami	jednotka	k1gFnPc7	jednotka
SI	se	k3xPyFc3	se
následující	následující	k2eAgFnPc1d1	následující
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
:	:	kIx,	:
minuta	minuta	k1gFnSc1	minuta
<g/>
,	,	kIx,	,
hodina	hodina	k1gFnSc1	hodina
<g/>
,	,	kIx,	,
den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
úhlový	úhlový	k2eAgInSc1d1	úhlový
stupeň	stupeň	k1gInSc1	stupeň
<g/>
,	,	kIx,	,
úhlová	úhlový	k2eAgFnSc1d1	úhlová
minuta	minuta	k1gFnSc1	minuta
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
úhlová	úhlový	k2eAgFnSc1d1	úhlová
<g/>
)	)	kIx)	)
vteřina	vteřina	k1gFnSc1	vteřina
<g/>
,	,	kIx,	,
hektar	hektar	k1gInSc1	hektar
<g/>
,	,	kIx,	,
litr	litr	k1gInSc1	litr
<g/>
,	,	kIx,	,
tuna	tuna	k1gFnSc1	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Připouští	připouštět	k5eAaImIp3nS	připouštět
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
používání	používání	k1gNnSc1	používání
některých	některý	k3yIgFnPc2	některý
mimosoustavových	mimosoustavový	k2eAgFnPc2d1	mimosoustavový
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
vztah	vztah	k1gInSc1	vztah
k	k	k7c3	k
jednotkám	jednotka	k1gFnPc3	jednotka
SI	si	k1gNnSc2	si
není	být	k5eNaImIp3nS	být
definován	definovat	k5eAaBmNgInS	definovat
pevně	pevně	k6eAd1	pevně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
experimentálním	experimentální	k2eAgNnSc6d1	experimentální
určení	určení	k1gNnSc6	určení
<g/>
:	:	kIx,	:
elektronvolt	elektronvolt	k1gInSc1	elektronvolt
<g/>
,	,	kIx,	,
dalton	dalton	k1gInSc1	dalton
<g/>
,	,	kIx,	,
astronomická	astronomický	k2eAgFnSc1d1	astronomická
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Jednotky	jednotka	k1gFnPc1	jednotka
SI	se	k3xPyFc3	se
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c6	na
teorii	teorie	k1gFnSc6	teorie
relativity	relativita	k1gFnSc2	relativita
realizovány	realizovat	k5eAaBmNgFnP	realizovat
podle	podle	k7c2	podle
svých	svůj	k3xOyFgFnPc2	svůj
definic	definice	k1gFnPc2	definice
lokálně	lokálně	k6eAd1	lokálně
jako	jako	k9	jako
vlastní	vlastní	k2eAgFnSc2d1	vlastní
jednotky	jednotka	k1gFnSc2	jednotka
(	(	kIx(	(
<g/>
veličin	veličina	k1gFnPc2	veličina
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
vlastní	vlastní	k2eAgInSc4d1	vlastní
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
délka	délka	k1gFnSc1	délka
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgFnSc1d1	vlastní
hmotnost	hmotnost	k1gFnSc1	hmotnost
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
