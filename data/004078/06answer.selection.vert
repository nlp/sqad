<s>
Fotosyntéza	fotosyntéza	k1gFnSc1	fotosyntéza
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
fós	fós	k?	fós
<g/>
,	,	kIx,	,
fótos	fótos	k1gInSc1	fótos
–	–	k?	–
"	"	kIx"	"
<g/>
světlo	světlo	k1gNnSc1	světlo
<g/>
"	"	kIx"	"
a	a	k8xC	a
synthesis	synthesis	k1gInSc1	synthesis
–	–	k?	–
"	"	kIx"	"
<g/>
shrnutí	shrnutí	k1gNnPc2	shrnutí
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
skládání	skládání	k1gNnSc2	skládání
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
také	také	k9	také
fotosyntetická	fotosyntetický	k2eAgFnSc1d1	fotosyntetická
asimilace	asimilace	k1gFnSc1	asimilace
je	být	k5eAaImIp3nS	být
složitý	složitý	k2eAgInSc4d1	složitý
biochemický	biochemický	k2eAgInSc4d1	biochemický
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
přijatá	přijatý	k2eAgFnSc1d1	přijatá
energie	energie	k1gFnSc1	energie
světelného	světelný	k2eAgNnSc2d1	světelné
záření	záření	k1gNnSc2	záření
na	na	k7c4	na
energii	energie	k1gFnSc4	energie
chemických	chemický	k2eAgFnPc2d1	chemická
vazeb	vazba	k1gFnPc2	vazba
<g/>
.	.	kIx.	.
</s>
