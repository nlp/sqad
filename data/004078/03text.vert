<s>
Fotosyntéza	fotosyntéza	k1gFnSc1	fotosyntéza
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
fós	fós	k?	fós
<g/>
,	,	kIx,	,
fótos	fótos	k1gInSc1	fótos
–	–	k?	–
"	"	kIx"	"
<g/>
světlo	světlo	k1gNnSc1	světlo
<g/>
"	"	kIx"	"
a	a	k8xC	a
synthesis	synthesis	k1gInSc1	synthesis
–	–	k?	–
"	"	kIx"	"
<g/>
shrnutí	shrnutí	k1gNnPc2	shrnutí
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
skládání	skládání	k1gNnSc2	skládání
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
také	také	k9	také
fotosyntetická	fotosyntetický	k2eAgFnSc1d1	fotosyntetická
asimilace	asimilace	k1gFnSc1	asimilace
je	být	k5eAaImIp3nS	být
složitý	složitý	k2eAgInSc4d1	složitý
biochemický	biochemický	k2eAgInSc4d1	biochemický
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
přijatá	přijatý	k2eAgFnSc1d1	přijatá
energie	energie	k1gFnSc1	energie
světelného	světelný	k2eAgNnSc2d1	světelné
záření	záření	k1gNnSc2	záření
na	na	k7c4	na
energii	energie	k1gFnSc4	energie
chemických	chemický	k2eAgFnPc2d1	chemická
vazeb	vazba	k1gFnPc2	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
světelného	světelný	k2eAgInSc2d1	světelný
<g/>
,	,	kIx,	,
např.	např.	kA	např.
slunečního	sluneční	k2eAgNnSc2d1	sluneční
<g/>
,	,	kIx,	,
záření	záření	k1gNnSc2	záření
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
(	(	kIx(	(
<g/>
syntéze	syntéza	k1gFnSc3	syntéza
<g/>
)	)	kIx)	)
energeticky	energeticky	k6eAd1	energeticky
bohatých	bohatý	k2eAgFnPc2d1	bohatá
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
–	–	k?	–
cukrů	cukr	k1gInPc2	cukr
–	–	k?	–
z	z	k7c2	z
jednoduchých	jednoduchý	k2eAgFnPc2d1	jednoduchá
anorganických	anorganický	k2eAgFnPc2d1	anorganická
látek	látka	k1gFnPc2	látka
–	–	k?	–
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
(	(	kIx(	(
<g/>
CO	co	k8xS	co
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
a	a	k8xC	a
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Fotosyntéza	fotosyntéza	k1gFnSc1	fotosyntéza
má	mít	k5eAaImIp3nS	mít
zásadní	zásadní	k2eAgInSc4d1	zásadní
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
život	život	k1gInSc4	život
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Fotosyntéza	fotosyntéza	k1gFnSc1	fotosyntéza
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
chloroplastech	chloroplast	k1gInPc6	chloroplast
zelených	zelený	k2eAgFnPc2d1	zelená
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
mnohých	mnohý	k2eAgInPc2d1	mnohý
dalších	další	k2eAgInPc2d1	další
eukaryotických	eukaryotický	k2eAgInPc2d1	eukaryotický
organizmů	organizmus	k1gInPc2	organizmus
(	(	kIx(	(
<g/>
různé	různý	k2eAgFnPc1d1	různá
řasy	řasa	k1gFnPc1	řasa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
sinic	sinice	k1gFnPc2	sinice
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
dalších	další	k2eAgFnPc2d1	další
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tzv.	tzv.	kA	tzv.
autotrofní	autotrofní	k2eAgFnSc4d1	autotrofní
výživu	výživa	k1gFnSc4	výživa
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
otázky	otázka	k1gFnPc1	otázka
týkající	týkající	k2eAgFnPc1d1	týkající
se	se	k3xPyFc4	se
jejího	její	k3xOp3gInSc2	její
průběhu	průběh	k1gInSc2	průběh
dosud	dosud	k6eAd1	dosud
nejsou	být	k5eNaImIp3nP	být
dostatečně	dostatečně	k6eAd1	dostatečně
objasněny	objasněn	k2eAgFnPc1d1	objasněna
<g/>
.	.	kIx.	.
</s>
<s>
Průběh	průběh	k1gInSc1	průběh
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
fází	fáze	k1gFnPc2	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
světelné	světelný	k2eAgFnSc6d1	světelná
fázi	fáze	k1gFnSc6	fáze
barevné	barevný	k2eAgInPc1d1	barevný
pigmenty	pigment	k1gInPc1	pigment
pohlcují	pohlcovat	k5eAaImIp3nP	pohlcovat
světlo	světlo	k1gNnSc4	světlo
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgMnSc2	jenž
získávají	získávat	k5eAaImIp3nP	získávat
energii	energie	k1gFnSc3	energie
pro	pro	k7c4	pro
následné	následný	k2eAgInPc4d1	následný
děje	děj	k1gInPc4	děj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rozkladu	rozklad	k1gInSc3	rozklad
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
uvolnění	uvolnění	k1gNnSc4	uvolnění
kyslíku	kyslík	k1gInSc2	kyslík
(	(	kIx(	(
<g/>
který	který	k3yQgInSc1	který
pak	pak	k6eAd1	pak
využívají	využívat	k5eAaPmIp3nP	využívat
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
organismy	organismus	k1gInPc4	organismus
k	k	k7c3	k
dýchání	dýchání	k1gNnSc3	dýchání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Biochemické	biochemický	k2eAgInPc1d1	biochemický
děje	děj	k1gInPc1	děj
v	v	k7c6	v
temnostní	temnostní	k2eAgFnSc6d1	temnostní
fázi	fáze	k1gFnSc6	fáze
již	již	k6eAd1	již
světlo	světlo	k1gNnSc4	světlo
nepotřebují	potřebovat	k5eNaImIp3nP	potřebovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
využívají	využívat	k5eAaPmIp3nP	využívat
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
světelné	světelný	k2eAgFnSc6d1	světelná
fázi	fáze	k1gFnSc6	fáze
získána	získat	k5eAaPmNgFnS	získat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
zabudování	zabudování	k1gNnSc3	zabudování
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
do	do	k7c2	do
molekul	molekula	k1gFnPc2	molekula
cukrů	cukr	k1gInPc2	cukr
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
dále	daleko	k6eAd2	daleko
slouží	sloužit	k5eAaImIp3nP	sloužit
buď	buď	k8xC	buď
jako	jako	k9	jako
zásobárna	zásobárna	k1gFnSc1	zásobárna
a	a	k8xC	a
zdroj	zdroj	k1gInSc1	zdroj
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
stavební	stavební	k2eAgFnPc4d1	stavební
složky	složka	k1gFnPc4	složka
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
složitějších	složitý	k2eAgFnPc2d2	složitější
molekul	molekula	k1gFnPc2	molekula
(	(	kIx(	(
<g/>
polysacharidů	polysacharid	k1gInPc2	polysacharid
<g/>
,	,	kIx,	,
glykosidů	glykosid	k1gInPc2	glykosid
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Procesy	proces	k1gInPc1	proces
temnostní	temnostní	k2eAgFnSc2d1	temnostní
fáze	fáze	k1gFnSc2	fáze
probíhají	probíhat	k5eAaImIp3nP	probíhat
v	v	k7c6	v
cyklech	cyklus	k1gInPc6	cyklus
a	a	k8xC	a
liší	lišit	k5eAaImIp3nP	lišit
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgInPc1d1	vnější
faktory	faktor	k1gInPc1	faktor
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
průběh	průběh	k1gInSc1	průběh
fotosyntézy	fotosyntéza	k1gFnPc4	fotosyntéza
závisí	záviset	k5eAaImIp3nS	záviset
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
koncentrace	koncentrace	k1gFnSc1	koncentrace
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
fotosyntézu	fotosyntéza	k1gFnSc4	fotosyntéza
oxygenní	oxygenní	k2eAgInPc1d1	oxygenní
(	(	kIx(	(
<g/>
při	při	k7c6	při
které	který	k3yRgFnSc6	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
kyslík	kyslík	k1gInSc1	kyslík
a	a	k8xC	a
pro	pro	k7c4	pro
jejíž	jejíž	k3xOyRp3gNnSc4	jejíž
zahájení	zahájení	k1gNnSc4	zahájení
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
voda	voda	k1gFnSc1	voda
<g/>
)	)	kIx)	)
a	a	k8xC	a
anoxygenní	anoxygenní	k2eAgMnPc1d1	anoxygenní
(	(	kIx(	(
<g/>
při	při	k7c6	při
které	který	k3yIgFnPc1	který
kyslík	kyslík	k1gInSc1	kyslík
nevzniká	vznikat	k5eNaImIp3nS	vznikat
a	a	k8xC	a
do	do	k7c2	do
jejíhož	jejíž	k3xOyRp3gNnSc2	jejíž
zahájení	zahájení	k1gNnSc2	zahájení
nezasahuje	zasahovat	k5eNaImIp3nS	zasahovat
voda	voda	k1gFnSc1	voda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
rozeznáváme	rozeznávat	k5eAaImIp1nP	rozeznávat
různé	různý	k2eAgInPc4d1	různý
typy	typ	k1gInPc4	typ
anoxygenní	anoxygenní	k2eAgFnSc2d1	anoxygenní
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
její	její	k3xOp3gNnSc4	její
zahájení	zahájení	k1gNnSc4	zahájení
potřeba	potřeba	k6eAd1	potřeba
sulfan	sulfany	k1gInPc2	sulfany
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
organické	organický	k2eAgFnPc1d1	organická
kyseliny	kyselina	k1gFnPc1	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
průběh	průběh	k1gInSc1	průběh
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
rovnice	rovnice	k1gFnSc1	rovnice
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
6	[number]	k4	6
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
12	[number]	k4	12
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c4	o
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
6	[number]	k4	6
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
12	[number]	k4	12
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c6	o
:	:	kIx,	:
6	[number]	k4	6
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
+	+	kIx~	+
6	[number]	k4	6
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c6	o
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
6	[number]	k4	6
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c4	o
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gInSc1	mathsf
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
CO_	CO_	k1gMnSc1	CO_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
12	[number]	k4	12
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
H_	H_	k1gMnSc1	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
O	o	k7c4	o
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
C_	C_	k1gFnSc3	C_
<g />
.	.	kIx.	.
</s>
<s>
<g/>
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
}	}	kIx)	}
<g/>
H_	H_	k1gFnSc2	H_
<g/>
{	{	kIx(	{
<g/>
12	[number]	k4	12
<g/>
}	}	kIx)	}
<g/>
O_	O_	k1gFnSc2	O_
<g/>
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
6	[number]	k4	6
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
O_	O_	k1gMnSc1	O_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
6	[number]	k4	6
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
H_	H_	k1gFnSc1	H_
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
O	o	k7c4	o
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Tato	tento	k3xDgFnSc1	tento
rovnice	rovnice	k1gFnSc1	rovnice
se	se	k3xPyFc4	se
však	však	k9	však
často	často	k6eAd1	často
zjednodušuje	zjednodušovat	k5eAaImIp3nS	zjednodušovat
na	na	k7c4	na
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
6	[number]	k4	6
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
6	[number]	k4	6
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c4	o
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
6	[number]	k4	6
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
12	[number]	k4	12
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c6	o
:	:	kIx,	:
6	[number]	k4	6
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
6	[number]	k4	6
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c6	o
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gInSc1	mathsf
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
CO_	CO_	k1gMnSc1	CO_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
+	+	kIx~	+
<g/>
6	[number]	k4	6
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
H_	H_	k1gMnSc1	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
O	o	k7c4	o
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
C_	C_	k1gMnSc6	C_
<g/>
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
}	}	kIx)	}
<g/>
H_	H_	k1gFnSc2	H_
<g/>
{	{	kIx(	{
<g/>
12	[number]	k4	12
<g/>
}	}	kIx)	}
<g/>
O_	O_	k1gFnSc2	O_
<g/>
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
6	[number]	k4	6
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
O_	O_	k1gMnSc1	O_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
:	:	kIx,	:
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
energie	energie	k1gFnSc1	energie
získaná	získaný	k2eAgFnSc1d1	získaná
fotosyntézou	fotosyntéza	k1gFnSc7	fotosyntéza
Δ	Δ	k?	Δ
=	=	kIx~	=
2870	[number]	k4	2870
kJ	kJ	k?	kJ
<g/>
/	/	kIx~	/
<g/>
mol	mol	k1gMnSc1	mol
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
objasnění	objasnění	k1gNnSc6	objasnění
rovnice	rovnice	k1gFnSc2	rovnice
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
se	se	k3xPyFc4	se
účastnila	účastnit	k5eAaImAgFnS	účastnit
řada	řada	k1gFnSc1	řada
vědců	vědec	k1gMnPc2	vědec
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdým	tvrdý	k2eAgInSc7d1	tvrdý
oříškem	oříšek	k1gInSc7	oříšek
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
zjistit	zjistit	k5eAaPmF	zjistit
původ	původ	k1gInSc4	původ
vznikajícího	vznikající	k2eAgInSc2d1	vznikající
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1648	[number]	k4	1648
vlámský	vlámský	k2eAgInSc1d1	vlámský
lékař	lékař	k1gMnSc1	lékař
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
van	van	k1gInSc4	van
Helmont	Helmont	k1gMnSc1	Helmont
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
vypěstování	vypěstování	k1gNnSc6	vypěstování
vrby	vrba	k1gFnSc2	vrba
z	z	k7c2	z
výhonků	výhonek	k1gInPc2	výhonek
v	v	k7c6	v
květináči	květináč	k1gInSc6	květináč
se	se	k3xPyFc4	se
nepatrně	patrně	k6eNd1	patrně
změnila	změnit	k5eAaPmAgFnS	změnit
hmotnost	hmotnost	k1gFnSc1	hmotnost
zeminy	zemina	k1gFnSc2	zemina
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
ještě	ještě	k6eAd1	ještě
neexistoval	existovat	k5eNaImAgInS	existovat
zákon	zákon	k1gInSc1	zákon
zachování	zachování	k1gNnSc2	zachování
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
,	,	kIx,	,
usoudil	usoudit	k5eAaPmAgMnS	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hmotnost	hmotnost	k1gFnSc1	hmotnost
rostliny	rostlina	k1gFnSc2	rostlina
se	se	k3xPyFc4	se
zvětšila	zvětšit	k5eAaPmAgFnS	zvětšit
o	o	k7c4	o
přijatou	přijatý	k2eAgFnSc4d1	přijatá
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1727	[number]	k4	1727
usoudil	usoudit	k5eAaPmAgInS	usoudit
Stephen	Stephen	k2eAgInSc1d1	Stephen
Hales	Hales	k1gInSc1	Hales
<g/>
,	,	kIx,	,
že	že	k8xS	že
rostliny	rostlina	k1gFnPc1	rostlina
berou	brát	k5eAaImIp3nP	brát
část	část	k1gFnSc4	část
své	svůj	k3xOyFgFnSc2	svůj
hmoty	hmota	k1gFnSc2	hmota
také	také	k9	také
ze	z	k7c2	z
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1771	[number]	k4	1771
anglický	anglický	k2eAgInSc1d1	anglický
duchovní	duchovní	k2eAgInSc1d1	duchovní
Joseph	Joseph	k1gInSc1	Joseph
Priestley	Priestlea	k1gFnSc2	Priestlea
zjistil	zjistit	k5eAaPmAgInS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
rostliny	rostlina	k1gFnPc1	rostlina
"	"	kIx"	"
<g/>
napravují	napravovat	k5eAaImIp3nP	napravovat
<g/>
"	"	kIx"	"
vzduch	vzduch	k1gInSc1	vzduch
poškozený	poškozený	k2eAgInSc1d1	poškozený
dýcháním	dýchání	k1gNnSc7	dýchání
či	či	k8xC	či
hořením	hoření	k1gNnSc7	hoření
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
objevil	objevit	k5eAaPmAgInS	objevit
kyslík	kyslík	k1gInSc1	kyslík
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nazval	nazvat	k5eAaBmAgInS	nazvat
"	"	kIx"	"
<g/>
deflogistonovaný	deflogistonovaný	k2eAgInSc1d1	deflogistonovaný
vzduch	vzduch	k1gInSc1	vzduch
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Inspirován	inspirován	k2eAgInSc1d1	inspirován
Priestleyeho	Priestleyeha	k1gFnSc5	Priestleyeha
pokusy	pokus	k1gInPc1	pokus
roku	rok	k1gInSc2	rok
1779	[number]	k4	1779
holandský	holandský	k2eAgMnSc1d1	holandský
lékař	lékař	k1gMnSc1	lékař
Jan	Jan	k1gMnSc1	Jan
Ingenhousz	Ingenhousz	k1gMnSc1	Ingenhousz
prokázal	prokázat	k5eAaPmAgMnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rostliny	rostlina	k1gFnPc1	rostlina
"	"	kIx"	"
<g/>
čistí	čistit	k5eAaImIp3nS	čistit
<g/>
"	"	kIx"	"
vzduch	vzduch	k1gInSc1	vzduch
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
svítí	svítit	k5eAaImIp3nS	svítit
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
zelené	zelený	k2eAgFnSc6d1	zelená
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
pastor	pastor	k1gMnSc1	pastor
Jean	Jean	k1gMnSc1	Jean
Senebier	Senebier	k1gMnSc1	Senebier
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1782	[number]	k4	1782
dokázal	dokázat	k5eAaPmAgInS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
při	při	k7c6	při
fotosyntéze	fotosyntéza	k1gFnSc6	fotosyntéza
pohlcuje	pohlcovat	k5eAaImIp3nS	pohlcovat
CO2	CO2	k1gFnSc1	CO2
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
svázaný	svázaný	k2eAgInSc1d1	svázaný
vzduch	vzduch	k1gInSc1	vzduch
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Švýcar	Švýcar	k1gMnSc1	Švýcar
Nicolas-Theodoré	Nicolas-Theodorý	k2eAgFnSc2d1	Nicolas-Theodorý
de	de	k?	de
Saussure	Saussur	k1gMnSc5	Saussur
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1804	[number]	k4	1804
zjistil	zjistit	k5eAaPmAgInS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hmotnost	hmotnost	k1gFnSc1	hmotnost
organické	organický	k2eAgFnSc2d1	organická
hmoty	hmota	k1gFnSc2	hmota
a	a	k8xC	a
kyslíku	kyslík	k1gInSc2	kyslík
(	(	kIx(	(
<g/>
produktů	produkt	k1gInPc2	produkt
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
hmotnost	hmotnost	k1gFnSc1	hmotnost
spotřebovaného	spotřebovaný	k2eAgNnSc2d1	spotřebované
CO	co	k8xS	co
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
usoudil	usoudit	k5eAaPmAgMnS	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
spotřebovává	spotřebovávat	k5eAaImIp3nS	spotřebovávat
také	také	k9	také
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
jediná	jediný	k2eAgFnSc1d1	jediná
další	další	k2eAgFnSc1d1	další
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
do	do	k7c2	do
soustavy	soustava	k1gFnSc2	soustava
přidával	přidávat	k5eAaImAgMnS	přidávat
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
část	část	k1gFnSc1	část
do	do	k7c2	do
rovnice	rovnice	k1gFnSc2	rovnice
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
přidal	přidat	k5eAaPmAgMnS	přidat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1842	[number]	k4	1842
německý	německý	k2eAgMnSc1d1	německý
fyziolog	fyziolog	k1gMnSc1	fyziolog
Robert	Robert	k1gMnSc1	Robert
Mayer	Mayer	k1gMnSc1	Mayer
<g/>
,	,	kIx,	,
když	když	k8xS	když
došel	dojít	k5eAaPmAgInS	dojít
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
fotosyntéza	fotosyntéza	k1gFnSc1	fotosyntéza
přeměňuje	přeměňovat	k5eAaImIp3nS	přeměňovat
světelnou	světelný	k2eAgFnSc4d1	světelná
energii	energie	k1gFnSc4	energie
v	v	k7c4	v
energii	energie	k1gFnSc4	energie
chemickou	chemický	k2eAgFnSc4d1	chemická
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
světlo	světlo	k1gNnSc1	světlo
redukuje	redukovat	k5eAaBmIp3nS	redukovat
CO2	CO2	k1gFnSc4	CO2
na	na	k7c4	na
meziprodukt	meziprodukt	k1gInSc4	meziprodukt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
slučuje	slučovat	k5eAaImIp3nS	slučovat
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
na	na	k7c4	na
sacharid	sacharid	k1gInSc4	sacharid
<g/>
.	.	kIx.	.
</s>
<s>
Zdrojem	zdroj	k1gInSc7	zdroj
kyslíku	kyslík	k1gInSc2	kyslík
uvolněného	uvolněný	k2eAgInSc2d1	uvolněný
při	při	k7c6	při
fotosyntéze	fotosyntéza	k1gFnSc6	fotosyntéza
byl	být	k5eAaImAgMnS	být
podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
názoru	názor	k1gInSc2	názor
CO	co	k6eAd1	co
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
Cornelius	Cornelius	k1gInSc4	Cornelius
Van	van	k1gInSc4	van
Niel	nielo	k1gNnPc2	nielo
dokázal	dokázat	k5eAaPmAgMnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zelené	zelené	k1gNnSc1	zelené
fotosyntetické	fotosyntetický	k2eAgFnSc2d1	fotosyntetická
bakterie	bakterie	k1gFnSc2	bakterie
využívající	využívající	k2eAgInSc1d1	využívající
sulfan	sulfan	k1gInSc1	sulfan
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
)	)	kIx)	)
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
síru	síra	k1gFnSc4	síra
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
chemické	chemický	k2eAgFnSc3d1	chemická
podobnosti	podobnost	k1gFnSc3	podobnost
H2O	H2O	k1gMnSc1	H2O
a	a	k8xC	a
H2S	H2S	k1gMnSc1	H2S
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
obecnou	obecný	k2eAgFnSc4d1	obecná
rovnici	rovnice	k1gFnSc4	rovnice
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
+	+	kIx~	+
:	:	kIx,	:
světlo	světlo	k1gNnSc1	světlo
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
(	(	kIx(	(
C	C	kA	C
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c4	o
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s hack="1">
+	+	kIx~	+
2	[number]	k4	2
:	:	kIx,	:
A	A	kA	A
+	+	kIx~	+
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c4	o
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gMnSc1	mathsf
{	{	kIx(	{
<g/>
CO_	CO_	k1gMnSc1	CO_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
H_	H_	k1gMnSc1	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
<g/>
A	A	kA	A
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
světlo	světlo	k1gNnSc1	světlo
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
(	(	kIx(	(
<g/>
CH_	CH_	k1gMnSc1	CH_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
O	o	k7c4	o
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
A	a	k8xC	a
<g/>
+	+	kIx~	+
<g/>
H_	H_	k1gFnSc1	H_
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
O	o	k7c4	o
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Tato	tento	k3xDgFnSc1	tento
rovnice	rovnice	k1gFnSc1	rovnice
napovídá	napovídat	k5eAaBmIp3nS	napovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
fotosyntéza	fotosyntéza	k1gFnSc1	fotosyntéza
je	být	k5eAaImIp3nS	být
dvoustupňový	dvoustupňový	k2eAgInSc4d1	dvoustupňový
děj	děj	k1gInSc4	děj
<g/>
:	:	kIx,	:
Světlo	světlo	k1gNnSc1	světlo
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
k	k	k7c3	k
oxidaci	oxidace	k1gFnSc3	oxidace
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
A	A	kA	A
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
+	+	kIx~	+
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
světlo	světlo	k1gNnSc1	světlo
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
A	A	kA	A
+	+	kIx~	+
4	[number]	k4	4
:	:	kIx,	:
[	[	kIx(	[
H	H	kA	H
]	]	kIx)	]
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gInSc1	mathsf
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
H_	H_	k1gMnSc1	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
A	A	kA	A
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
světlo	světlo	k1gNnSc1	světlo
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
A	A	kA	A
<g/>
+	+	kIx~	+
<g/>
4	[number]	k4	4
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
H	H	kA	H
<g/>
]	]	kIx)	]
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Reduktans	Reduktans	k1gInSc1	Reduktans
[	[	kIx(	[
<g/>
H	H	kA	H
<g/>
]	]	kIx)	]
pak	pak	k6eAd1	pak
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
světle	světlo	k1gNnSc6	světlo
<g />
.	.	kIx.	.
</s>
<s hack="1">
redukuje	redukovat	k5eAaBmIp3nS	redukovat
CO	co	k6eAd1	co
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
[	[	kIx(	[
H	H	kA	H
]	]	kIx)	]
+	+	kIx~	+
C	C	kA	C
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
(	(	kIx(	(
C	C	kA	C
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c4	o
)	)	kIx)	)
+	+	kIx~	+
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c4	o
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gInSc1	mathsf
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
H	H	kA	H
<g/>
]	]	kIx)	]
<g/>
+	+	kIx~	+
<g/>
CO_	CO_	k1gMnSc1	CO_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
(	(	kIx(	(
<g/>
CH_	CH_	k1gMnSc1	CH_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
O	o	k7c6	o
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
H_	H_	k1gMnSc1	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
O	o	k7c4	o
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Správnost	správnost	k1gFnSc1	správnost
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
potvrdily	potvrdit	k5eAaPmAgInP	potvrdit
dva	dva	k4xCgInPc1	dva
pokusy	pokus	k1gInPc1	pokus
<g/>
:	:	kIx,	:
Roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
Robert	Robert	k1gMnSc1	Robert
Hill	Hill	k1gMnSc1	Hill
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
izolované	izolovaný	k2eAgInPc1d1	izolovaný
chloroplasty	chloroplast	k1gInPc1	chloroplast
bez	bez	k7c2	bez
přístupu	přístup	k1gInSc2	přístup
CO	co	k6eAd1	co
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
umělým	umělý	k2eAgInSc7d1	umělý
akceptorem	akceptor	k1gInSc7	akceptor
elektronů	elektron	k1gInPc2	elektron
(	(	kIx(	(
<g/>
ferrikyanid	ferrikyanida	k1gFnPc2	ferrikyanida
[	[	kIx(	[
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
Fe	Fe	k1gFnSc1	Fe
<g/>
(	(	kIx(	(
<g/>
CN	CN	kA	CN
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
3	[number]	k4	3
<g/>
−	−	k?	−
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
tento	tento	k3xDgInSc4	tento
akceptor	akceptor	k1gInSc4	akceptor
redukovat	redukovat	k5eAaBmF	redukovat
(	(	kIx(	(
<g/>
na	na	k7c4	na
ferrokyanid	ferrokyanid	k1gInSc4	ferrokyanid
[	[	kIx(	[
<g/>
Fe	Fe	k1gMnSc1	Fe
<g/>
(	(	kIx(	(
<g/>
CN	CN	kA	CN
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
4	[number]	k4	4
<g/>
−	−	k?	−
<g/>
)	)	kIx)	)
a	a	k8xC	a
současně	současně	k6eAd1	současně
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
kyslík	kyslík	k1gInSc4	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
tzv.	tzv.	kA	tzv.
Hillova	Hillův	k2eAgFnSc1d1	Hillova
reakce	reakce	k1gFnSc1	reakce
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
CO2	CO2	k1gFnSc1	CO2
se	se	k3xPyFc4	se
bezprostředně	bezprostředně	k6eAd1	bezprostředně
neúčastní	účastnit	k5eNaImIp3nS	účastnit
reakce	reakce	k1gFnSc1	reakce
uvolňující	uvolňující	k2eAgFnSc1d1	uvolňující
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
přirozeným	přirozený	k2eAgInSc7d1	přirozený
akceptorem	akceptor	k1gInSc7	akceptor
je	být	k5eAaImIp3nS	být
NADP	NADP	kA	NADP
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
redukuje	redukovat	k5eAaBmIp3nS	redukovat
na	na	k7c6	na
NADPH	NADPH	kA	NADPH
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
Sam	Sam	k1gMnSc1	Sam
Ruben	ruben	k2eAgMnSc1d1	ruben
a	a	k8xC	a
Martin	Martin	k1gMnSc1	Martin
Kamen	kamna	k1gNnPc2	kamna
dokázali	dokázat	k5eAaPmAgMnP	dokázat
izotopem	izotop	k1gInSc7	izotop
18	[number]	k4	18
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
že	že	k8xS	že
zdrojem	zdroj	k1gInSc7	zdroj
O2	O2	k1gFnSc2	O2
je	být	k5eAaImIp3nS	být
voda	voda	k1gFnSc1	voda
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
18	[number]	k4	18
:	:	kIx,	:
:	:	kIx,	:
O	O	kA	O
+	+	kIx~	+
C	C	kA	C
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
světlo	světlo	k1gNnSc1	světlo
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
(	(	kIx(	(
C	C	kA	C
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c4	o
)	)	kIx)	)
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
18	[number]	k4	18
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c6	o
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c4	o
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gInSc1	mathsf
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
H_	H_	k1gMnSc1	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
18	[number]	k4	18
<g/>
}	}	kIx)	}
<g/>
O	o	k7c4	o
<g/>
+	+	kIx~	+
<g/>
CO_	CO_	k1gFnSc4	CO_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
světlo	světlo	k1gNnSc1	světlo
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
(	(	kIx(	(
<g/>
CH_	CH_	k1gMnSc1	CH_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
O	o	k7c4	o
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
18	[number]	k4	18
<g/>
}	}	kIx)	}
<g/>
O_	O_	k1gFnSc2	O_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
H_	H_	k1gMnSc1	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
O	o	k7c4	o
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Organismy	organismus	k1gInPc1	organismus
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
tvorbu	tvorba	k1gFnSc4	tvorba
energeticky	energeticky	k6eAd1	energeticky
bohatých	bohatý	k2eAgFnPc2d1	bohatá
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
pomocí	pomocí	k7c2	pomocí
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
autotrofní	autotrofní	k2eAgFnPc1d1	autotrofní
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
fotoautotrofní	fotoautotrofní	k2eAgFnSc1d1	fotoautotrofní
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gFnPc4	on
především	především	k6eAd1	především
zelené	zelený	k2eAgFnPc4d1	zelená
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc4	některý
druhy	druh	k1gInPc4	druh
baktérií	baktérie	k1gFnPc2	baktérie
včetně	včetně	k7c2	včetně
sinic	sinice	k1gFnPc2	sinice
(	(	kIx(	(
<g/>
Cyanobacteria	Cyanobacterium	k1gNnSc2	Cyanobacterium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mnohé	mnohý	k2eAgFnPc1d1	mnohá
ruduchy	ruducha	k1gFnPc1	ruducha
(	(	kIx(	(
<g/>
Rhodophyta	Rhodophyta	k1gFnSc1	Rhodophyta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obrněnky	obrněnka	k1gFnSc2	obrněnka
(	(	kIx(	(
<g/>
Dinophyta	Dinophyta	k1gMnSc1	Dinophyta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skrytěnky	skrytěnka	k1gFnSc2	skrytěnka
(	(	kIx(	(
<g/>
Cryptophyta	Cryptophyta	k1gMnSc1	Cryptophyta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hnědé	hnědý	k2eAgFnPc1d1	hnědá
řasy	řasa	k1gFnPc1	řasa
(	(	kIx(	(
<g/>
Phaeophyceae	Phaeophycea	k1gFnPc1	Phaeophycea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krásnoočka	krásnoočko	k1gNnPc1	krásnoočko
(	(	kIx(	(
<g/>
Euglenophyta	Euglenophyta	k1gFnSc1	Euglenophyta
<g/>
)	)	kIx)	)
a	a	k8xC	a
zelené	zelený	k2eAgFnPc1d1	zelená
řasy	řasa	k1gFnPc1	řasa
(	(	kIx(	(
<g/>
Chlorophyta	Chlorophyta	k1gFnSc1	Chlorophyta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Schopnost	schopnost	k1gFnSc1	schopnost
měnit	měnit	k5eAaImF	měnit
energii	energie	k1gFnSc4	energie
světla	světlo	k1gNnSc2	světlo
na	na	k7c4	na
chemickou	chemický	k2eAgFnSc4d1	chemická
přináší	přinášet	k5eAaImIp3nS	přinášet
těmto	tento	k3xDgInPc3	tento
organismům	organismus	k1gInPc3	organismus
výhody	výhoda	k1gFnSc2	výhoda
během	během	k7c2	během
evoluce	evoluce	k1gFnSc2	evoluce
ve	v	k7c6	v
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
konkurenci	konkurence	k1gFnSc6	konkurence
<g/>
.	.	kIx.	.
</s>
<s>
Prvotní	prvotní	k2eAgInPc1d1	prvotní
fotosyntetické	fotosyntetický	k2eAgInPc1d1	fotosyntetický
systémy	systém	k1gInPc1	systém
zřejmě	zřejmě	k6eAd1	zřejmě
byly	být	k5eAaImAgInP	být
anoxygenní	anoxygenní	k2eAgInPc1d1	anoxygenní
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
neprodukovaly	produkovat	k5eNaImAgFnP	produkovat
kyslík	kyslík	k1gInSc4	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Stáří	stáří	k1gNnSc1	stáří
nejstarších	starý	k2eAgInPc2d3	nejstarší
fotosyntetizujících	fotosyntetizující	k2eAgInPc2d1	fotosyntetizující
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
3,5	[number]	k4	3,5
miliardy	miliarda	k4xCgFnSc2	miliarda
let	léto	k1gNnPc2	léto
či	či	k8xC	či
i	i	k9	i
více	hodně	k6eAd2	hodně
než	než	k8xS	než
3,8	[number]	k4	3,8
miliardy	miliarda	k4xCgFnPc4	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Kyslík	kyslík	k1gInSc1	kyslík
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
atmosféře	atmosféra	k1gFnSc6	atmosféra
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
až	až	k9	až
díky	díky	k7c3	díky
oxygenní	oxygenní	k2eAgFnSc3d1	oxygenní
fotosyntéze	fotosyntéza	k1gFnSc3	fotosyntéza
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
sinice	sinice	k1gFnPc1	sinice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
uvolňovaly	uvolňovat	k5eAaImAgFnP	uvolňovat
kyslík	kyslík	k1gInSc4	kyslík
štěpením	štěpení	k1gNnSc7	štěpení
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
2	[number]	k4	2
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Chloroplasty	chloroplast	k1gInPc1	chloroplast
eukaryotních	eukaryotní	k2eAgFnPc2d1	eukaryotní
buněk	buňka	k1gFnPc2	buňka
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pohlcením	pohlcení	k1gNnSc7	pohlcení
buňky	buňka	k1gFnSc2	buňka
sinice	sinice	k1gFnSc2	sinice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
pak	pak	k6eAd1	pak
žily	žít	k5eAaImAgFnP	žít
uvnitř	uvnitř	k7c2	uvnitř
těchto	tento	k3xDgFnPc2	tento
buněk	buňka	k1gFnPc2	buňka
v	v	k7c6	v
endosymbióze	endosymbióza	k1gFnSc6	endosymbióza
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
staly	stát	k5eAaPmAgFnP	stát
organely	organela	k1gFnPc4	organela
<g/>
.	.	kIx.	.
</s>
<s>
Podobným	podobný	k2eAgInSc7d1	podobný
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
i	i	k9	i
mitochondrie	mitochondrie	k1gFnPc1	mitochondrie
u	u	k7c2	u
aerobních	aerobní	k2eAgInPc2d1	aerobní
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
teorie	teorie	k1gFnSc1	teorie
vzniku	vznik	k1gInSc2	vznik
organel	organela	k1gFnPc2	organela
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
endosymbiotická	endosymbiotický	k2eAgFnSc1d1	endosymbiotická
a	a	k8xC	a
svědčí	svědčit	k5eAaImIp3nS	svědčit
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
fakt	fakt	k9	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
chloroplasty	chloroplast	k1gInPc1	chloroplast
mají	mít	k5eAaImIp3nP	mít
vlastní	vlastní	k2eAgMnPc4d1	vlastní
kruhovou	kruhový	k2eAgFnSc7d1	kruhová
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
sinice	sinice	k1gFnSc1	sinice
<g/>
.	.	kIx.	.
</s>
<s>
Fotosyntéza	fotosyntéza	k1gFnSc1	fotosyntéza
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
chloroplastech	chloroplast	k1gInPc6	chloroplast
eukaryotních	eukaryotní	k2eAgFnPc2d1	eukaryotní
buněk	buňka	k1gFnPc2	buňka
a	a	k8xC	a
v	v	k7c6	v
chromatoforech	chromatofor	k1gInPc6	chromatofor
prokaryot	prokaryota	k1gFnPc2	prokaryota
<g/>
.	.	kIx.	.
</s>
<s>
Chloroplasty	chloroplast	k1gInPc1	chloroplast
jsou	být	k5eAaImIp3nP	být
plastidy	plastid	k1gInPc4	plastid
v	v	k7c6	v
cytoplazmě	cytoplazma	k1gFnSc6	cytoplazma
rostlin	rostlina	k1gFnPc2	rostlina
(	(	kIx(	(
<g/>
především	především	k9	především
v	v	k7c6	v
listech	list	k1gInPc6	list
<g/>
)	)	kIx)	)
obsahujících	obsahující	k2eAgFnPc6d1	obsahující
asimilační	asimilační	k2eAgMnPc1d1	asimilační
barviva	barvivo	k1gNnSc2	barvivo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
fotosyntéza	fotosyntéza	k1gFnSc1	fotosyntéza
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
dvojitou	dvojitý	k2eAgFnSc4d1	dvojitá
membránu	membrána	k1gFnSc4	membrána
<g/>
,	,	kIx,	,
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
vlastní	vlastní	k2eAgNnPc4d1	vlastní
DNA	dno	k1gNnPc4	dno
a	a	k8xC	a
ribozomy	ribozom	k1gInPc4	ribozom
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
jsou	být	k5eAaImIp3nP	být
zeleně	zeleň	k1gFnPc1	zeleň
zbarveny	zbarvit	k5eAaPmNgFnP	zbarvit
díky	díky	k7c3	díky
chlorofylu	chlorofyl	k1gInSc3	chlorofyl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
plazmatické	plazmatický	k2eAgFnSc6d1	plazmatická
hmotě	hmota	k1gFnSc6	hmota
chloroplastů	chloroplast	k1gInPc2	chloroplast
(	(	kIx(	(
<g/>
stromatu	stroma	k1gNnSc2	stroma
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
malé	malý	k2eAgFnPc1d1	malá
<g/>
,	,	kIx,	,
okrouhlé	okrouhlý	k2eAgFnPc1d1	okrouhlá
<g/>
,	,	kIx,	,
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
navrstvené	navrstvený	k2eAgFnPc1d1	navrstvená
destičky	destička	k1gFnPc1	destička
(	(	kIx(	(
<g/>
grana	grana	k1gFnSc1	grana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
soubor	soubor	k1gInSc4	soubor
uzavřených	uzavřený	k2eAgFnPc2d1	uzavřená
dvojitých	dvojitý	k2eAgFnPc2d1	dvojitá
lamel	lamela	k1gFnPc2	lamela
(	(	kIx(	(
<g/>
thylakoidů	thylakoid	k1gInPc2	thylakoid
<g/>
)	)	kIx)	)
obsahujících	obsahující	k2eAgFnPc2d1	obsahující
fotosyntetická	fotosyntetický	k2eAgNnPc1d1	fotosyntetické
barviva	barvivo	k1gNnPc1	barvivo
(	(	kIx(	(
<g/>
pigmenty	pigment	k1gInPc1	pigment
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInPc1d1	základní
pigmenty	pigment	k1gInPc1	pigment
<g/>
:	:	kIx,	:
chlorofyly	chlorofyl	k1gInPc1	chlorofyl
a	a	k8xC	a
bakteriochlorofyly	bakteriochlorofyl	k1gInPc1	bakteriochlorofyl
Doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
pigmenty	pigment	k1gInPc1	pigment
<g/>
:	:	kIx,	:
karotenoidy	karotenoid	k1gInPc1	karotenoid
(	(	kIx(	(
<g/>
karoteny	karoten	k1gInPc1	karoten
a	a	k8xC	a
xanthofyly	xanthofyl	k1gInPc1	xanthofyl
<g/>
)	)	kIx)	)
a	a	k8xC	a
fykobiliny	fykobilin	k1gInPc4	fykobilin
"	"	kIx"	"
<g/>
Světelná	světelný	k2eAgFnSc1d1	světelná
fáze	fáze	k1gFnSc1	fáze
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
primární	primární	k2eAgInPc1d1	primární
děje	děj	k1gInPc1	děj
<g/>
"	"	kIx"	"
jsou	být	k5eAaImIp3nP	být
reakce	reakce	k1gFnPc1	reakce
závislé	závislý	k2eAgFnPc1d1	závislá
na	na	k7c6	na
světle	světlo	k1gNnSc6	světlo
a	a	k8xC	a
uskutečňují	uskutečňovat	k5eAaImIp3nP	uskutečňovat
se	se	k3xPyFc4	se
v	v	k7c6	v
thylakoidech	thylakoid	k1gInPc6	thylakoid
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nich	on	k3xPp3gInPc6	on
probíhá	probíhat	k5eAaImIp3nS	probíhat
přeměna	přeměna	k1gFnSc1	přeměna
světelné	světelný	k2eAgFnSc2d1	světelná
energie	energie	k1gFnSc2	energie
(	(	kIx(	(
<g/>
fotonů	foton	k1gInPc2	foton
<g/>
)	)	kIx)	)
na	na	k7c4	na
chemickou	chemický	k2eAgFnSc4d1	chemická
energii	energie	k1gFnSc4	energie
(	(	kIx(	(
<g/>
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
NADPH	NADPH	kA	NADPH
a	a	k8xC	a
ATP	atp	kA	atp
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
vedlejší	vedlejší	k2eAgInSc1d1	vedlejší
produkt	produkt	k1gInSc1	produkt
světelné	světelný	k2eAgFnSc2d1	světelná
fáze	fáze	k1gFnSc2	fáze
vzniká	vznikat	k5eAaImIp3nS	vznikat
kyslík	kyslík	k1gInSc1	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Zachycením	zachycení	k1gNnSc7	zachycení
světla	světlo	k1gNnSc2	světlo
rostlinným	rostlinný	k2eAgInSc7d1	rostlinný
pigmentem	pigment	k1gInSc7	pigment
začíná	začínat	k5eAaImIp3nS	začínat
vlastní	vlastní	k2eAgInSc1d1	vlastní
proces	proces	k1gInSc1	proces
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
energii	energie	k1gFnSc4	energie
světelného	světelný	k2eAgNnSc2d1	světelné
kvanta	kvantum	k1gNnSc2	kvantum
a	a	k8xC	a
přemění	přeměnit	k5eAaPmIp3nS	přeměnit
ji	on	k3xPp3gFnSc4	on
na	na	k7c4	na
energii	energie	k1gFnSc4	energie
chemické	chemický	k2eAgFnSc2d1	chemická
vazby	vazba	k1gFnSc2	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Vlnové	vlnový	k2eAgFnPc4d1	vlnová
délky	délka	k1gFnPc4	délka
viditelného	viditelný	k2eAgNnSc2d1	viditelné
světla	světlo	k1gNnSc2	světlo
mají	mít	k5eAaImIp3nP	mít
hodnoty	hodnota	k1gFnPc4	hodnota
v	v	k7c6	v
intervalu	interval	k1gInSc6	interval
380	[number]	k4	380
<g/>
–	–	k?	–
<g/>
760	[number]	k4	760
nm	nm	k?	nm
<g/>
.	.	kIx.	.
</s>
<s>
Fotosyntéza	fotosyntéza	k1gFnSc1	fotosyntéza
zelených	zelený	k2eAgFnPc2d1	zelená
rostlin	rostlina	k1gFnPc2	rostlina
využívá	využívat	k5eAaImIp3nS	využívat
světlo	světlo	k1gNnSc1	světlo
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
pouze	pouze	k6eAd1	pouze
400	[number]	k4	400
<g/>
–	–	k?	–
<g/>
750	[number]	k4	750
nm	nm	k?	nm
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgNnSc3	tento
světlu	světlo	k1gNnSc3	světlo
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
fotosynteticky	fotosynteticky	k6eAd1	fotosynteticky
aktivní	aktivní	k2eAgNnSc1d1	aktivní
záření	záření	k1gNnSc1	záření
(	(	kIx(	(
<g/>
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
FAR	fara	k1gFnPc2	fara
nebo	nebo	k8xC	nebo
PhAR	PhAR	k1gFnPc2	PhAR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Různá	různý	k2eAgNnPc1d1	různé
barviva	barvivo	k1gNnPc1	barvivo
absorbují	absorbovat	k5eAaBmIp3nP	absorbovat
různou	různý	k2eAgFnSc4d1	různá
část	část	k1gFnSc4	část
světelného	světelný	k2eAgNnSc2d1	světelné
spektra	spektrum	k1gNnSc2	spektrum
–	–	k?	–
např.	např.	kA	např.
chlorofyly	chlorofyl	k1gInPc7	chlorofyl
absorbují	absorbovat	k5eAaBmIp3nP	absorbovat
nejvíce	nejvíce	k6eAd1	nejvíce
světlo	světlo	k1gNnSc4	světlo
v	v	k7c6	v
modrofialové	modrofialový	k2eAgFnSc6d1	modrofialová
a	a	k8xC	a
červené	červený	k2eAgFnSc6d1	červená
části	část	k1gFnSc6	část
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Zelená	zelený	k2eAgFnSc1d1	zelená
část	část	k1gFnSc1	část
spektra	spektrum	k1gNnSc2	spektrum
se	se	k3xPyFc4	se
tolik	tolik	k6eAd1	tolik
neabsorbuje	absorbovat	k5eNaBmIp3nS	absorbovat
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
tedy	tedy	k9	tedy
zelená	zelený	k2eAgFnSc1d1	zelená
barva	barva	k1gFnSc1	barva
chloroplastů	chloroplast	k1gInPc2	chloroplast
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ovšem	ovšem	k9	ovšem
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zelené	zelený	k2eAgNnSc1d1	zelené
světlo	světlo	k1gNnSc1	světlo
je	být	k5eAaImIp3nS	být
neužitečné	užitečný	k2eNgNnSc1d1	neužitečné
nebo	nebo	k8xC	nebo
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
účinné	účinný	k2eAgNnSc1d1	účinné
při	při	k7c6	při
fotosyntéze	fotosyntéza	k1gFnSc6	fotosyntéza
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
molekula	molekula	k1gFnSc1	molekula
pigmentu	pigment	k1gInSc2	pigment
absorbuje	absorbovat	k5eAaBmIp3nS	absorbovat
kvantum	kvantum	k1gNnSc1	kvantum
světelné	světelný	k2eAgFnSc2d1	světelná
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
dostane	dostat	k5eAaPmIp3nS	dostat
se	se	k3xPyFc4	se
do	do	k7c2	do
prvního	první	k4xOgInSc2	první
či	či	k8xC	či
do	do	k7c2	do
druhého	druhý	k4xOgInSc2	druhý
excitovaného	excitovaný	k2eAgInSc2d1	excitovaný
stavu	stav	k1gInSc2	stav
(	(	kIx(	(
<g/>
přeskok	přeskok	k1gInSc1	přeskok
elektronu	elektron	k1gInSc2	elektron
na	na	k7c4	na
vyšší	vysoký	k2eAgFnSc4d2	vyšší
energetickou	energetický	k2eAgFnSc4d1	energetická
hladinu	hladina	k1gFnSc4	hladina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Získané	získaný	k2eAgFnPc4d1	získaná
energie	energie	k1gFnPc4	energie
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
molekula	molekula	k1gFnSc1	molekula
zbavit	zbavit	k5eAaPmF	zbavit
více	hodně	k6eAd2	hodně
způsoby	způsob	k1gInPc4	způsob
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
jsou	být	k5eAaImIp3nP	být
nejdůležitější	důležitý	k2eAgInPc1d3	nejdůležitější
tyto	tento	k3xDgInPc1	tento
čtyři	čtyři	k4xCgMnPc4	čtyři
<g/>
:	:	kIx,	:
Vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
přeměnou	přeměna	k1gFnSc7	přeměna
se	se	k3xPyFc4	se
přebytečná	přebytečný	k2eAgFnSc1d1	přebytečná
excitační	excitační	k2eAgFnSc1d1	excitační
energie	energie	k1gFnSc1	energie
přemění	přeměnit	k5eAaPmIp3nS	přeměnit
na	na	k7c4	na
kinetickou	kinetický	k2eAgFnSc4d1	kinetická
energii	energie	k1gFnSc4	energie
pohybu	pohyb	k1gInSc2	pohyb
molekuly	molekula	k1gFnSc2	molekula
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
na	na	k7c4	na
teplo	teplo	k1gNnSc4	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
děj	děj	k1gInSc1	děj
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
rychlý	rychlý	k2eAgInSc1d1	rychlý
<g/>
,	,	kIx,	,
trvá	trvat	k5eAaImIp3nS	trvat
méně	málo	k6eAd2	málo
než	než	k8xS	než
10	[number]	k4	10
ps	ps	k0	ps
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
přechází	přecházet	k5eAaImIp3nS	přecházet
chlorofyl	chlorofyl	k1gInSc4	chlorofyl
vždy	vždy	k6eAd1	vždy
z	z	k7c2	z
druhého	druhý	k4xOgInSc2	druhý
excitovaného	excitovaný	k2eAgInSc2d1	excitovaný
stavu	stav	k1gInSc2	stav
do	do	k7c2	do
prvního	první	k4xOgNnSc2	první
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokáže	dokázat	k5eAaPmIp3nS	dokázat
využít	využít	k5eAaPmF	využít
pouze	pouze	k6eAd1	pouze
energii	energie	k1gFnSc4	energie
odpovídající	odpovídající	k2eAgFnSc4d1	odpovídající
prvnímu	první	k4xOgNnSc3	první
excitovanému	excitovaný	k2eAgInSc3d1	excitovaný
stavu	stav	k1gInSc3	stav
<g/>
.	.	kIx.	.
</s>
<s>
Fluorescencí	fluorescence	k1gFnSc7	fluorescence
je	být	k5eAaImIp3nS	být
bez	bez	k7c2	bez
užitku	užitek	k1gInSc2	užitek
vyzářen	vyzářen	k2eAgInSc1d1	vyzářen
foton	foton	k1gInSc1	foton
s	s	k7c7	s
nižší	nízký	k2eAgFnSc7d2	nižší
energií	energie	k1gFnSc7	energie
než	než	k8xS	než
přijatý	přijatý	k2eAgInSc1d1	přijatý
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pochod	pochod	k1gInSc1	pochod
je	být	k5eAaImIp3nS	být
pomalejší	pomalý	k2eAgFnSc1d2	pomalejší
než	než	k8xS	než
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
přeměna	přeměna	k1gFnSc1	přeměna
<g/>
,	,	kIx,	,
trvá	trvat	k5eAaImIp3nS	trvat
okolo	okolo	k7c2	okolo
10	[number]	k4	10
ns	ns	k?	ns
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
způsobené	způsobený	k2eAgFnPc1d1	způsobená
ztráty	ztráta	k1gFnPc1	ztráta
běžně	běžně	k6eAd1	běžně
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
%	%	kIx~	%
pohlcené	pohlcený	k2eAgFnSc2d1	pohlcená
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Přenos	přenos	k1gInSc1	přenos
excitační	excitační	k2eAgFnSc2d1	excitační
energie	energie	k1gFnSc2	energie
na	na	k7c4	na
okolní	okolní	k2eAgFnPc4d1	okolní
molekuly	molekula	k1gFnPc4	molekula
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
díky	díky	k7c3	díky
vzájemnému	vzájemný	k2eAgNnSc3d1	vzájemné
překrývání	překrývání	k1gNnSc3	překrývání
molekulových	molekulový	k2eAgInPc2d1	molekulový
orbitalů	orbital	k1gInPc2	orbital
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
fotosyntetických	fotosyntetický	k2eAgNnPc2d1	fotosyntetické
barviv	barvivo	k1gNnPc2	barvivo
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
anténní	anténní	k2eAgNnPc1d1	anténní
barviva	barvivo	k1gNnPc1	barvivo
<g/>
)	)	kIx)	)
slouží	sloužit	k5eAaImIp3nP	sloužit
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
absorpci	absorpce	k1gFnSc3	absorpce
fotonů	foton	k1gInPc2	foton
a	a	k8xC	a
přenosu	přenos	k1gInSc2	přenos
excitační	excitační	k2eAgFnSc2d1	excitační
energie	energie	k1gFnSc2	energie
do	do	k7c2	do
reakčních	reakční	k2eAgNnPc2d1	reakční
center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
rozhodujícím	rozhodující	k2eAgInSc7d1	rozhodující
akceptorem	akceptor	k1gInSc7	akceptor
(	(	kIx(	(
<g/>
příjemcem	příjemce	k1gMnSc7	příjemce
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
chlorofyl	chlorofyl	k1gInSc1	chlorofyl
a.	a.	k?	a.
Účinnost	účinnost	k1gFnSc4	účinnost
energetického	energetický	k2eAgInSc2d1	energetický
přenosu	přenos	k1gInSc2	přenos
z	z	k7c2	z
různých	různý	k2eAgNnPc2d1	různé
doplňkových	doplňkový	k2eAgNnPc2d1	doplňkové
barviv	barvivo	k1gNnPc2	barvivo
na	na	k7c4	na
chlorofyl	chlorofyl	k1gInSc4	chlorofyl
a	a	k8xC	a
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
%	%	kIx~	%
<g/>
:	:	kIx,	:
z	z	k7c2	z
karotenů	karoten	k1gInPc2	karoten
70	[number]	k4	70
<g/>
–	–	k?	–
<g/>
80	[number]	k4	80
%	%	kIx~	%
<g/>
,	,	kIx,	,
z	z	k7c2	z
fykoerythrinu	fykoerythrin	k1gInSc2	fykoerythrin
a	a	k8xC	a
fykocyaninu	fykocyanina	k1gFnSc4	fykocyanina
70	[number]	k4	70
<g/>
–	–	k?	–
<g/>
90	[number]	k4	90
%	%	kIx~	%
<g/>
,	,	kIx,	,
z	z	k7c2	z
chlorofylu	chlorofyl	k1gInSc2	chlorofyl
a	a	k8xC	a
až	až	k9	až
100	[number]	k4	100
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnSc1	energie
se	se	k3xPyFc4	se
z	z	k7c2	z
reakčního	reakční	k2eAgNnSc2d1	reakční
centra	centrum	k1gNnSc2	centrum
dále	daleko	k6eAd2	daleko
nešíří	šířit	k5eNaImIp3nS	šířit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
zasazen	zasadit	k5eAaPmNgInS	zasadit
do	do	k7c2	do
vhodného	vhodný	k2eAgNnSc2d1	vhodné
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
snižuje	snižovat	k5eAaImIp3nS	snižovat
energii	energie	k1gFnSc4	energie
jeho	jeho	k3xOp3gInSc2	jeho
prvního	první	k4xOgInSc2	první
excitovaného	excitovaný	k2eAgInSc2d1	excitovaný
stavu	stav	k1gInSc2	stav
a	a	k8xC	a
excitace	excitace	k1gFnSc1	excitace
je	být	k5eAaImIp3nS	být
polapena	polapen	k2eAgFnSc1d1	polapena
<g/>
.	.	kIx.	.
</s>
<s>
Fotooxidace	Fotooxidace	k1gFnSc1	Fotooxidace
je	být	k5eAaImIp3nS	být
zapříčiněna	zapříčinit	k5eAaPmNgFnS	zapříčinit
vypuzením	vypuzení	k1gNnSc7	vypuzení
slaběji	slabo	k6eAd2	slabo
vázaného	vázaný	k2eAgInSc2d1	vázaný
elektronu	elektron	k1gInSc2	elektron
z	z	k7c2	z
molekuly	molekula	k1gFnSc2	molekula
chlorofylu	chlorofyl	k1gInSc2	chlorofyl
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
oxidovaná	oxidovaný	k2eAgFnSc1d1	oxidovaná
forma	forma	k1gFnSc1	forma
chlorofylu	chlorofyl	k1gInSc2	chlorofyl
(	(	kIx(	(
<g/>
Chl	Chl	k1gFnSc1	Chl
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
povahu	povaha	k1gFnSc4	povaha
kationtového	kationtový	k2eAgInSc2d1	kationtový
volného	volný	k2eAgInSc2d1	volný
radikálu	radikál	k1gInSc2	radikál
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
energie	energie	k1gFnSc1	energie
vyexcitovaného	vyexcitovaný	k2eAgInSc2d1	vyexcitovaný
elektronu	elektron	k1gInSc2	elektron
je	být	k5eAaImIp3nS	být
využita	využít	k5eAaPmNgFnS	využít
v	v	k7c6	v
následných	následný	k2eAgFnPc6d1	následná
chemických	chemický	k2eAgFnPc6d1	chemická
reakcích	reakce	k1gFnPc6	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Reakce	reakce	k1gFnPc1	reakce
probíhají	probíhat	k5eAaImIp3nP	probíhat
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
bílkovinných	bílkovinný	k2eAgInPc6d1	bílkovinný
komplexech	komplex	k1gInPc6	komplex
–	–	k?	–
fotosystému	fotosystý	k2eAgMnSc3d1	fotosystý
I	I	kA	I
<g/>
,	,	kIx,	,
komplexu	komplex	k1gInSc2	komplex
cytochromů	cytochrom	k1gInPc2	cytochrom
b	b	k?	b
<g/>
6	[number]	k4	6
<g/>
/	/	kIx~	/
<g/>
f	f	k?	f
a	a	k8xC	a
fotosystému	fotosystý	k2eAgMnSc3d1	fotosystý
II	II	kA	II
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
propojeny	propojen	k2eAgInPc1d1	propojen
pohyblivými	pohyblivý	k2eAgInPc7d1	pohyblivý
elektronovými	elektronový	k2eAgInPc7d1	elektronový
přenašeči	přenašeč	k1gInPc7	přenašeč
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
přenos	přenos	k1gInSc1	přenos
elektronů	elektron	k1gInPc2	elektron
dostal	dostat	k5eAaPmAgInS	dostat
název	název	k1gInSc1	název
schéma	schéma	k1gNnSc4	schéma
Z	Z	kA	Z
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc1d1	hlavní
výchozí	výchozí	k2eAgFnSc7d1	výchozí
látkou	látka	k1gFnSc7	látka
je	být	k5eAaImIp3nS	být
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
donorem	donor	k1gInSc7	donor
elektronů	elektron	k1gInPc2	elektron
a	a	k8xC	a
vodíkových	vodíkový	k2eAgInPc2d1	vodíkový
kationtů	kation	k1gInPc2	kation
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c4	o
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c6	o
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
−	−	k?	−
0,815	[number]	k4	0,815
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gInSc1	mathsf
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
H_	H_	k1gMnSc1	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
O	o	k7c4	o
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
O_	O_	k1gMnSc6	O_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
4	[number]	k4	4
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
e	e	k0	e
<g />
.	.	kIx.	.
</s>
<s>
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
4	[number]	k4	4
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
H	H	kA	H
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
qquad	qquad	k1gInSc1	qquad
}}	}}	k?	}}
<g/>
E	E	kA	E
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}	}	kIx)	}
<g/>
815	[number]	k4	815
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
V	V	kA	V
<g/>
}	}	kIx)	}
}	}	kIx)	}
:	:	kIx,	:
Elektrony	elektron	k1gInPc1	elektron
a	a	k8xC	a
vodíkové	vodíkový	k2eAgInPc1d1	vodíkový
kationty	kation	k1gInPc1	kation
se	se	k3xPyFc4	se
využijí	využít	k5eAaPmIp3nP	využít
k	k	k7c3	k
redukci	redukce	k1gFnSc3	redukce
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
N	N	kA	N
A	a	k8xC	a
D	D	kA	D
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
2	[number]	k4	2
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
N	N	kA	N
A	a	k8xC	a
D	D	kA	D
P	P	kA	P
H	H	kA	H
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
−	−	k?	−
0,320	[number]	k4	0,320
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gMnSc1	mathsf
{	{	kIx(	{
<g/>
NADP	NADP	kA	NADP
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
+	+	kIx~	+
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
H	H	kA	H
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
NADPH	NADPH	kA	NADPH
<g/>
\	\	kIx~	\
<g/>
qquad	qquad	k1gInSc1	qquad
}}	}}	k?	}}
<g/>
E	E	kA	E
<g/>
^	^	kIx~	^
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
320	[number]	k4	320
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
V	V	kA	V
<g/>
}	}	kIx)	}
}	}	kIx)	}
:	:	kIx,	:
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
rovnice	rovnice	k1gFnSc1	rovnice
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
N	N	kA	N
A	a	k8xC	a
D	D	kA	D
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
+	+	kIx~	+
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c4	o
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
N	N	kA	N
A	a	k8xC	a
D	D	kA	D
P	P	kA	P
H	H	kA	H
+	+	kIx~	+
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
O	o	k7c6	o
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
−	−	k?	−
1,135	[number]	k4	1,135
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gInSc1	mathsf
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
NADP	NADP	kA	NADP
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
H_	H_	k1gMnSc1	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
O	o	k7c4	o
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
<g />
.	.	kIx.	.
</s>
<s>
\	\	kIx~	\
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
NADPH	NADPH	kA	NADPH
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
H	H	kA	H
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
O_	O_	k1gMnSc1	O_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
qquad	qquad	k1gInSc1	qquad
}}	}}	k?	}}
<g/>
E	E	kA	E
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
135	[number]	k4	135
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
V	V	kA	V
<g/>
}	}	kIx)	}
}	}	kIx)	}
:	:	kIx,	:
Reakce	reakce	k1gFnSc1	reakce
fotosystému	fotosystý	k2eAgMnSc3d1	fotosystý
II	II	kA	II
a	a	k8xC	a
fotosystému	fotosystý	k2eAgInSc3d1	fotosystý
I	i	k8xC	i
dohromady	dohromady	k6eAd1	dohromady
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
přemístit	přemístit	k5eAaPmF	přemístit
1	[number]	k4	1
elektron	elektron	k1gInSc4	elektron
a	a	k8xC	a
spotřebují	spotřebovat	k5eAaPmIp3nP	spotřebovat
2	[number]	k4	2
fotony	foton	k1gInPc4	foton
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vznik	vznik	k1gInSc4	vznik
jedné	jeden	k4xCgFnSc2	jeden
molekuly	molekula	k1gFnSc2	molekula
kyslíku	kyslík	k1gInSc2	kyslík
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
uvolnit	uvolnit	k5eAaPmF	uvolnit
a	a	k8xC	a
přemístit	přemístit	k5eAaPmF	přemístit
4	[number]	k4	4
elektrony	elektron	k1gInPc4	elektron
(	(	kIx(	(
<g/>
2	[number]	k4	2
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
atom	atom	k1gInSc4	atom
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
potřeba	potřeba	k1gFnSc1	potřeba
8	[number]	k4	8
fotonů	foton	k1gInPc2	foton
(	(	kIx(	(
<g/>
2	[number]	k4	2
na	na	k7c4	na
každý	každý	k3xTgInSc4	každý
elektron	elektron	k1gInSc4	elektron
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
syntézu	syntéza	k1gFnSc4	syntéza
jednoho	jeden	k4xCgInSc2	jeden
molu	mol	k1gInSc2	mol
glukózy	glukóza	k1gFnSc2	glukóza
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
6	[number]	k4	6
×	×	k?	×
8	[number]	k4	8
=	=	kIx~	=
48	[number]	k4	48
molů	mol	k1gInPc2	mol
fotonů	foton	k1gInPc2	foton
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
energii	energie	k1gFnSc4	energie
8440,6	[number]	k4	8440,6
kJ	kJ	k?	kJ
<g/>
/	/	kIx~	/
<g/>
mol	mol	k1gMnSc1	mol
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
glukózy	glukóza	k1gFnSc2	glukóza
se	se	k3xPyFc4	se
ale	ale	k9	ale
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
pouze	pouze	k6eAd1	pouze
2884,5	[number]	k4	2884,5
kJ	kJ	k?	kJ
<g/>
/	/	kIx~	/
<g/>
mol	mol	k1gMnSc1	mol
<g/>
,	,	kIx,	,
účinnost	účinnost	k1gFnSc1	účinnost
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
34,2	[number]	k4	34,2
%	%	kIx~	%
(	(	kIx(	(
<g/>
2884,4	[number]	k4	2884,4
×	×	k?	×
100	[number]	k4	100
%	%	kIx~	%
:	:	kIx,	:
8440,6	[number]	k4	8440,6
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kokův	kokův	k2eAgInSc4d1	kokův
cyklus	cyklus	k1gInSc4	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Fotosystém	Fotosystý	k2eAgInSc6d1	Fotosystý
II	II	kA	II
(	(	kIx(	(
<g/>
PS	PS	kA	PS
II	II	kA	II
<g/>
)	)	kIx)	)
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
reakční	reakční	k2eAgNnPc4d1	reakční
centra	centrum	k1gNnPc4	centrum
P680	P680	k1gFnSc2	P680
(	(	kIx(	(
<g/>
při	při	k7c6	při
vlnové	vlnový	k2eAgFnSc6d1	vlnová
délce	délka	k1gFnSc6	délka
680	[number]	k4	680
nm	nm	k?	nm
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
absorpčního	absorpční	k2eAgNnSc2d1	absorpční
maxima	maximum	k1gNnSc2	maximum
–	–	k?	–
aktivace	aktivace	k1gFnSc1	aktivace
žlutozeleným	žlutozelený	k2eAgNnSc7d1	žlutozelené
světlem	světlo	k1gNnSc7	světlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
základním	základní	k2eAgInSc6d1	základní
stavu	stav	k1gInSc6	stav
má	mít	k5eAaImIp3nS	mít
redoxní	redoxní	k2eAgInSc1d1	redoxní
potenciál	potenciál	k1gInSc1	potenciál
přes	přes	k7c4	přes
+1,0	+1,0	k4	+1,0
eV	eV	k?	eV
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vzbuzeném	vzbuzený	k2eAgMnSc6d1	vzbuzený
(	(	kIx(	(
<g/>
excitovaném	excitovaný	k2eAgInSc6d1	excitovaný
<g/>
)	)	kIx)	)
okolo	okolo	k7c2	okolo
0	[number]	k4	0
eV.	eV.	k?	eV.
Pohlcením	pohlcení	k1gNnSc7	pohlcení
světla	světlo	k1gNnSc2	světlo
se	se	k3xPyFc4	se
P680	P680	k1gFnSc1	P680
excituje	excitovat	k5eAaBmIp3nS	excitovat
na	na	k7c6	na
P	P	kA	P
<g/>
680	[number]	k4	680
<g/>
*	*	kIx~	*
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
je	být	k5eAaImIp3nS	být
vymrštěn	vymrštěn	k2eAgInSc1d1	vymrštěn
elektron	elektron	k1gInSc1	elektron
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
přejde	přejít	k5eAaPmIp3nS	přejít
na	na	k7c4	na
feofytin	feofytin	k1gInSc4	feofytin
a	a	k8xC	a
(	(	kIx(	(
<g/>
Pheo	Pheo	k1gNnSc1	Pheo
a	a	k8xC	a
–	–	k?	–
chlorofyl	chlorofyl	k1gInSc1	chlorofyl
a	a	k8xC	a
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
Mg	mg	kA	mg
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
nahrazen	nahradit	k5eAaPmNgInS	nahradit
2	[number]	k4	2
H	H	kA	H
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
a	a	k8xC	a
poté	poté	k6eAd1	poté
zredukuje	zredukovat	k5eAaPmIp3nS	zredukovat
plastochinon	plastochinon	k1gMnSc1	plastochinon
(	(	kIx(	(
<g/>
PQ	PQ	kA	PQ
<g/>
)	)	kIx)	)
na	na	k7c4	na
plastochinol	plastochinol	k1gInSc4	plastochinol
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
PQH	PQH	kA	PQH
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgNnSc2	který
přejde	přejít	k5eAaPmIp3nS	přejít
na	na	k7c4	na
komplex	komplex	k1gInSc4	komplex
cytochromů	cytochrom	k1gInPc2	cytochrom
b	b	k?	b
<g/>
6	[number]	k4	6
<g/>
/	/	kIx~	/
<g/>
f.	f.	k?	f.
Fotosystém	Fotosystý	k2eAgNnSc6d1	Fotosystý
II	II	kA	II
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
silné	silný	k2eAgNnSc1d1	silné
oxidační	oxidační	k2eAgNnSc1d1	oxidační
činidlo	činidlo	k1gNnSc1	činidlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
schopno	schopen	k2eAgNnSc1d1	schopno
oxidovat	oxidovat	k5eAaBmF	oxidovat
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
a	a	k8xC	a
současně	současně	k6eAd1	současně
slabé	slabý	k2eAgNnSc1d1	slabé
redukční	redukční	k2eAgNnSc1d1	redukční
činidlo	činidlo	k1gNnSc1	činidlo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
redukuje	redukovat	k5eAaBmIp3nS	redukovat
slabé	slabý	k2eAgNnSc1d1	slabé
oxidační	oxidační	k2eAgNnSc1d1	oxidační
činidlo	činidlo	k1gNnSc1	činidlo
z	z	k7c2	z
fotosystému	fotosystý	k2eAgInSc3d1	fotosystý
I.	I.	kA	I.
Fotooxidovaný	Fotooxidovaný	k2eAgInSc4d1	Fotooxidovaný
P	P	kA	P
<g/>
680	[number]	k4	680
<g/>
+	+	kIx~	+
(	(	kIx(	(
<g/>
kladný	kladný	k2eAgInSc1d1	kladný
náboj	náboj	k1gInSc1	náboj
je	být	k5eAaImIp3nS	být
důsledkem	důsledek	k1gInSc7	důsledek
ztráty	ztráta	k1gFnSc2	ztráta
elektronu	elektron	k1gInSc2	elektron
<g/>
)	)	kIx)	)
získá	získat	k5eAaPmIp3nS	získat
elektron	elektron	k1gInSc1	elektron
zpět	zpět	k6eAd1	zpět
z	z	k7c2	z
Kokova	kokův	k2eAgInSc2d1	kokův
cyklu	cyklus	k1gInSc2	cyklus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pomocí	pomocí	k7c2	pomocí
kyslík	kyslík	k1gInSc4	kyslík
tvořícího	tvořící	k2eAgInSc2d1	tvořící
komplexu	komplex	k1gInSc2	komplex
(	(	kIx(	(
<g/>
OEC	OEC	kA	OEC
–	–	k?	–
oxygen	oxygen	k1gInSc1	oxygen
envolving	envolving	k1gInSc1	envolving
complex	complex	k1gInSc1	complex
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
získává	získávat	k5eAaImIp3nS	získávat
elektrony	elektron	k1gInPc4	elektron
rozkladem	rozklad	k1gInSc7	rozklad
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
oxiduje	oxidovat	k5eAaBmIp3nS	oxidovat
ionty	ion	k1gInPc4	ion
OH	OH	kA	OH
<g/>
−	−	k?	−
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
peroxidu	peroxid	k1gInSc2	peroxid
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
kyslík	kyslík	k1gInSc4	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Komplex	komplex	k1gInSc1	komplex
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
vázaný	vázaný	k2eAgInSc1d1	vázaný
mangan	mangan	k1gInSc1	mangan
a	a	k8xC	a
každý	každý	k3xTgInSc1	každý
vývoj	vývoj	k1gInSc1	vývoj
kyslíku	kyslík	k1gInSc2	kyslík
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
5	[number]	k4	5
stavech	stav	k1gInPc6	stav
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
značí	značit	k5eAaImIp3nP	značit
S0	S0	k1gFnPc1	S0
až	až	k9	až
S	s	k7c7	s
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Kroky	krok	k1gInPc4	krok
S0	S0	k1gMnPc2	S0
až	až	k8xS	až
S4	S4	k1gMnPc2	S4
jsou	být	k5eAaImIp3nP	být
oxidačně-redukční	oxidačněedukční	k2eAgInPc1d1	oxidačně-redukční
pochody	pochod	k1gInPc1	pochod
poháněné	poháněný	k2eAgFnSc2d1	poháněná
energií	energie	k1gFnSc7	energie
fotonů	foton	k1gInPc2	foton
<g/>
,	,	kIx,	,
při	při	k7c6	při
přechodu	přechod	k1gInSc6	přechod
ze	z	k7c2	z
stavu	stav	k1gInSc2	stav
S4	S4	k1gFnSc2	S4
na	na	k7c6	na
S0	S0	k1gFnSc6	S0
se	se	k3xPyFc4	se
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
molekula	molekula	k1gFnSc1	molekula
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Komplex	komplex	k1gInSc1	komplex
přitom	přitom	k6eAd1	přitom
mění	měnit	k5eAaImIp3nS	měnit
svoji	svůj	k3xOyFgFnSc4	svůj
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
strukturu	struktura	k1gFnSc4	struktura
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
stavy	stav	k1gInPc4	stav
S0	S0	k1gMnPc2	S0
až	až	k8xS	až
S2	S2	k1gMnPc2	S2
je	být	k5eAaImIp3nS	být
složení	složení	k1gNnSc1	složení
Mn	Mn	k1gFnSc2	Mn
<g/>
4	[number]	k4	4
<g/>
O	o	k7c4	o
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
stavy	stav	k1gInPc4	stav
S3	S3	k1gMnPc2	S3
a	a	k8xC	a
S4	S4	k1gMnPc2	S4
Mn	Mn	k1gFnSc2	Mn
<g/>
4	[number]	k4	4
<g/>
O	o	k7c4	o
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
uvolnění	uvolnění	k1gNnSc6	uvolnění
molekuly	molekula	k1gFnSc2	molekula
O2	O2	k1gFnPc2	O2
se	se	k3xPyFc4	se
komplex	komplex	k1gInSc1	komplex
Mn	Mn	k1gFnSc2	Mn
<g/>
4	[number]	k4	4
<g/>
O	o	k7c4	o
<g/>
6	[number]	k4	6
přemění	přeměnit	k5eAaPmIp3nS	přeměnit
na	na	k7c6	na
Mn	Mn	k1gFnSc6	Mn
<g/>
4	[number]	k4	4
<g/>
O	o	k7c4	o
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
jsou	být	k5eAaImIp3nP	být
reakční	reakční	k2eAgNnPc1d1	reakční
centra	centrum	k1gNnPc1	centrum
převážně	převážně	k6eAd1	převážně
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
S	s	k7c7	s
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vznik	vznik	k1gInSc4	vznik
jedné	jeden	k4xCgFnSc2	jeden
molekuly	molekula	k1gFnSc2	molekula
O2	O2	k1gFnSc2	O2
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
8	[number]	k4	8
fotonů	foton	k1gInPc2	foton
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
se	se	k3xPyFc4	se
do	do	k7c2	do
dutiny	dutina	k1gFnSc2	dutina
thylakoidů	thylakoid	k1gInPc2	thylakoid
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
4	[number]	k4	4
protony	proton	k1gInPc7	proton
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Fotosystém	Fotosystý	k2eAgInSc6d1	Fotosystý
I	I	kA	I
(	(	kIx(	(
<g/>
PS	PS	kA	PS
I	i	k9	i
<g/>
)	)	kIx)	)
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
reakční	reakční	k2eAgNnPc4d1	reakční
centra	centrum	k1gNnPc4	centrum
P700	P700	k1gFnSc2	P700
(	(	kIx(	(
<g/>
při	při	k7c6	při
vlnové	vlnový	k2eAgFnSc6d1	vlnová
délce	délka	k1gFnSc6	délka
700	[number]	k4	700
nm	nm	k?	nm
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
absorpčního	absorpční	k2eAgNnSc2d1	absorpční
maxima	maximum	k1gNnSc2	maximum
–	–	k?	–
aktivace	aktivace	k1gFnSc1	aktivace
červeným	červený	k2eAgNnSc7d1	červené
světlem	světlo	k1gNnSc7	světlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
základním	základní	k2eAgInSc6d1	základní
stavu	stav	k1gInSc6	stav
má	mít	k5eAaImIp3nS	mít
redoxní	redoxní	k2eAgInSc1d1	redoxní
potenciál	potenciál	k1gInSc1	potenciál
přes	přes	k7c4	přes
+0,46	+0,46	k4	+0,46
eV	eV	k?	eV
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vzbuzeném	vzbuzený	k2eAgMnSc6d1	vzbuzený
(	(	kIx(	(
<g/>
excitovaném	excitovaný	k2eAgInSc6d1	excitovaný
<g/>
)	)	kIx)	)
o	o	k7c6	o
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1	[number]	k4	1
eV	eV	k?	eV
negativnější	negativní	k2eAgFnSc1d2	negativnější
<g/>
.	.	kIx.	.
</s>
<s>
Pohlcením	pohlcení	k1gNnSc7	pohlcení
světla	světlo	k1gNnSc2	světlo
se	se	k3xPyFc4	se
P700	P700	k1gFnSc1	P700
excituje	excitovat	k5eAaBmIp3nS	excitovat
na	na	k7c6	na
P	P	kA	P
<g/>
700	[number]	k4	700
<g/>
*	*	kIx~	*
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
je	být	k5eAaImIp3nS	být
vymrštěn	vymrštěn	k2eAgInSc1d1	vymrštěn
elektron	elektron	k1gInSc1	elektron
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
prochází	procházet	k5eAaImIp3nS	procházet
přes	přes	k7c4	přes
chlorofyl	chlorofyl	k1gInSc4	chlorofyl
a	a	k8xC	a
<g/>
,	,	kIx,	,
fylochinon	fylochinon	k1gInSc1	fylochinon
(	(	kIx(	(
<g/>
vitamín	vitamín	k1gInSc1	vitamín
K	k	k7c3	k
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
a	a	k8xC	a
tři	tři	k4xCgInPc4	tři
ferredoxiny	ferredoxin	k1gInPc4	ferredoxin
(	(	kIx(	(
<g/>
protein	protein	k1gInSc4	protein
obsahující	obsahující	k2eAgNnSc4d1	obsahující
železo	železo	k1gNnSc4	železo
a	a	k8xC	a
síru	síra	k1gFnSc4	síra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
velkému	velký	k2eAgInSc3d1	velký
negativnímu	negativní	k2eAgInSc3d1	negativní
redoxnímu	redoxní	k2eAgInSc3d1	redoxní
potenciálu	potenciál	k1gInSc3	potenciál
(	(	kIx(	(
<g/>
−	−	k?	−
<g/>
0,43	[number]	k4	0,43
eV	eV	k?	eV
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
enzymu	enzym	k1gInSc2	enzym
ferredoxin-NADP	ferredoxin-NADP	k?	ferredoxin-NADP
<g/>
+	+	kIx~	+
<g/>
-reduktázy	eduktáza	k1gFnSc2	-reduktáza
redukovat	redukovat	k5eAaBmF	redukovat
NADP	NADP	kA	NADP
<g/>
+	+	kIx~	+
na	na	k7c6	na
NADPH	NADPH	kA	NADPH
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
produktem	produkt	k1gInSc7	produkt
tohoto	tento	k3xDgNnSc2	tento
tzv.	tzv.	kA	tzv.
necyklického	cyklický	k2eNgInSc2d1	necyklický
přenosu	přenos	k1gInSc2	přenos
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
podstupuje	podstupovat	k5eAaImIp3nS	podstupovat
většina	většina	k1gFnSc1	většina
elektronů	elektron	k1gInPc2	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Fotosystém	Fotosystý	k2eAgInSc6d1	Fotosystý
I	i	k9	i
tedy	tedy	k9	tedy
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
slabé	slabý	k2eAgNnSc1d1	slabé
oxidační	oxidační	k2eAgNnSc1d1	oxidační
činidlo	činidlo	k1gNnSc1	činidlo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
schopno	schopen	k2eAgNnSc1d1	schopno
oxidovat	oxidovat	k5eAaBmF	oxidovat
plastocyanin	plastocyanin	k2eAgMnSc1d1	plastocyanin
<g/>
,	,	kIx,	,
a	a	k8xC	a
současně	současně	k6eAd1	současně
silné	silný	k2eAgNnSc4d1	silné
redukční	redukční	k2eAgNnSc4d1	redukční
činidlo	činidlo	k1gNnSc4	činidlo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
redukuje	redukovat	k5eAaBmIp3nS	redukovat
NADP	NADP	kA	NADP
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
necyklického	cyklický	k2eNgInSc2d1	necyklický
přenosu	přenos	k1gInSc2	přenos
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
i	i	k8xC	i
cyklický	cyklický	k2eAgInSc1d1	cyklický
přenos	přenos	k1gInSc1	přenos
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
elektrony	elektron	k1gInPc1	elektron
přenesou	přenést	k5eAaPmIp3nP	přenést
z	z	k7c2	z
třetího	třetí	k4xOgInSc2	třetí
ferredoxinu	ferredoxin	k1gInSc2	ferredoxin
na	na	k7c4	na
cytochrom	cytochrom	k1gInSc4	cytochrom
b	b	k?	b
<g/>
6	[number]	k4	6
<g/>
/	/	kIx~	/
<g/>
f	f	k?	f
a	a	k8xC	a
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
tak	tak	k6eAd1	tak
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
přenos	přenos	k1gInSc4	přenos
vodíkových	vodíkový	k2eAgInPc2d1	vodíkový
kationtů	kation	k1gInPc2	kation
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
ze	z	k7c2	z
stromatu	stroma	k1gNnSc2	stroma
do	do	k7c2	do
dutiny	dutina	k1gFnSc2	dutina
thylakoidu	thylakoid	k1gInSc2	thylakoid
<g/>
.	.	kIx.	.
</s>
<s>
Fotooxidovaný	Fotooxidovaný	k2eAgInSc1d1	Fotooxidovaný
P	P	kA	P
<g/>
700	[number]	k4	700
<g/>
+	+	kIx~	+
získá	získat	k5eAaPmIp3nS	získat
elektrony	elektron	k1gInPc4	elektron
zpět	zpět	k6eAd1	zpět
přes	přes	k7c4	přes
komplex	komplex	k1gInSc4	komplex
u	u	k7c2	u
cytochromů	cytochrom	k1gInPc2	cytochrom
b	b	k?	b
<g/>
6	[number]	k4	6
<g/>
/	/	kIx~	/
<g/>
f	f	k?	f
a	a	k8xC	a
plastocyanin	plastocyanina	k1gFnPc2	plastocyanina
z	z	k7c2	z
PS	PS	kA	PS
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnSc1	energie
elektronů	elektron	k1gInPc2	elektron
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
využita	využít	k5eAaPmNgFnS	využít
k	k	k7c3	k
přenosu	přenos	k1gInSc3	přenos
vodíkových	vodíkový	k2eAgInPc2d1	vodíkový
kationtů	kation	k1gInPc2	kation
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
ze	z	k7c2	z
stromatu	stroma	k1gNnSc2	stroma
do	do	k7c2	do
dutiny	dutina	k1gFnSc2	dutina
thylakoidu	thylakoid	k1gInSc2	thylakoid
<g/>
.	.	kIx.	.
</s>
<s>
Ionty	ion	k1gInPc1	ion
H	H	kA	H
<g/>
+	+	kIx~	+
v	v	k7c6	v
dutině	dutina	k1gFnSc6	dutina
thylakoidu	thylakoid	k1gInSc2	thylakoid
jsou	být	k5eAaImIp3nP	být
využity	využít	k5eAaPmNgFnP	využít
k	k	k7c3	k
fotofosforylaci	fotofosforylace	k1gFnSc3	fotofosforylace
<g/>
.	.	kIx.	.
</s>
<s>
Fotofosforylace	fotofosforylace	k1gFnSc1	fotofosforylace
je	být	k5eAaImIp3nS	být
děj	děj	k1gInSc4	děj
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
protonového	protonový	k2eAgInSc2d1	protonový
gradientu	gradient	k1gInSc2	gradient
(	(	kIx(	(
<g/>
rozdílných	rozdílný	k2eAgFnPc2d1	rozdílná
koncentrací	koncentrace	k1gFnPc2	koncentrace
H	H	kA	H
<g/>
+	+	kIx~	+
mezi	mezi	k7c7	mezi
stromatem	stroma	k1gNnSc7	stroma
a	a	k8xC	a
thylakoidní	thylakoidní	k2eAgFnSc7d1	thylakoidní
dutinou	dutina	k1gFnSc7	dutina
<g/>
)	)	kIx)	)
syntetizuje	syntetizovat	k5eAaImIp3nS	syntetizovat
ATP.	atp.	kA	atp.
ATP	atp	kA	atp
vzniká	vznikat	k5eAaImIp3nS	vznikat
z	z	k7c2	z
ADP	ADP	kA	ADP
a	a	k8xC	a
Pi	pi	k0	pi
(	(	kIx(	(
<g/>
volného	volný	k2eAgInSc2d1	volný
zbytku	zbytek	k1gInSc2	zbytek
kyseliny	kyselina	k1gFnSc2	kyselina
fosforečné	fosforečný	k2eAgFnSc2d1	fosforečná
H	H	kA	H
<g/>
3	[number]	k4	3
<g/>
PO	po	k7c4	po
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
pomocí	pomocí	k7c2	pomocí
ATP-syntázy	ATPyntáza	k1gFnSc2	ATP-syntáza
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
pohonný	pohonný	k2eAgInSc1d1	pohonný
motor	motor	k1gInSc1	motor
funguje	fungovat	k5eAaImIp3nS	fungovat
vyrovnávání	vyrovnávání	k1gNnSc1	vyrovnávání
koncentrací	koncentrace	k1gFnPc2	koncentrace
protonů	proton	k1gInPc2	proton
mezi	mezi	k7c7	mezi
stromatem	stroma	k1gNnSc7	stroma
a	a	k8xC	a
thylakoidní	thylakoidní	k2eAgFnSc7d1	thylakoidní
dutinou	dutina	k1gFnSc7	dutina
<g/>
.	.	kIx.	.
</s>
<s>
ATP	atp	kA	atp
je	být	k5eAaImIp3nS	být
sloučenina	sloučenina	k1gFnSc1	sloučenina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
makroergní	makroergní	k2eAgFnPc4d1	makroergní
vazby	vazba	k1gFnPc4	vazba
(	(	kIx(	(
<g/>
vazby	vazba	k1gFnPc4	vazba
<g/>
,	,	kIx,	,
při	při	k7c6	při
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
štěpení	štěpení	k1gNnSc6	štěpení
se	se	k3xPyFc4	se
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
energie	energie	k1gFnSc2	energie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ATP	atp	kA	atp
i	i	k8xC	i
NADPH	NADPH	kA	NADPH
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
ve	v	k7c6	v
světelné	světelný	k2eAgFnSc6d1	světelná
fázi	fáze	k1gFnSc6	fáze
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
využijí	využít	k5eAaPmIp3nP	využít
v	v	k7c6	v
temnostní	temnostní	k2eAgFnSc6d1	temnostní
fázi	fáze	k1gFnSc6	fáze
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
glukózy	glukóza	k1gFnSc2	glukóza
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
jedné	jeden	k4xCgFnSc2	jeden
molekuly	molekula	k1gFnSc2	molekula
O2	O2	k1gFnPc2	O2
z	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
molekul	molekula	k1gFnPc2	molekula
H2O	H2O	k1gMnPc2	H2O
se	se	k3xPyFc4	se
do	do	k7c2	do
thylakoidní	thylakoidní	k2eAgFnSc2d1	thylakoidní
dutiny	dutina	k1gFnSc2	dutina
uvolní	uvolnit	k5eAaPmIp3nP	uvolnit
4	[number]	k4	4
protony	proton	k1gInPc7	proton
<g/>
.	.	kIx.	.
</s>
<s>
Transportem	transport	k1gInSc7	transport
čtyř	čtyři	k4xCgInPc2	čtyři
elektronů	elektron	k1gInPc2	elektron
z	z	k7c2	z
kyslíku	kyslík	k1gInSc2	kyslík
se	se	k3xPyFc4	se
do	do	k7c2	do
thylakoidní	thylakoidní	k2eAgFnSc2d1	thylakoidní
dutiny	dutina	k1gFnSc2	dutina
pomocí	pomocí	k7c2	pomocí
cytochromu	cytochrom	k1gInSc2	cytochrom
b	b	k?	b
<g/>
6	[number]	k4	6
<g/>
/	/	kIx~	/
<g/>
f	f	k?	f
přemístí	přemístit	k5eAaPmIp3nS	přemístit
ze	z	k7c2	z
stromatu	stroma	k1gNnSc2	stroma
8	[number]	k4	8
protonů	proton	k1gInPc2	proton
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
redukci	redukce	k1gFnSc6	redukce
dvou	dva	k4xCgFnPc2	dva
molekul	molekula	k1gFnPc2	molekula
NADP	NADP	kA	NADP
<g/>
+	+	kIx~	+
+	+	kIx~	+
H	H	kA	H
<g/>
+	+	kIx~	+
na	na	k7c6	na
NADPH	NADPH	kA	NADPH
se	se	k3xPyFc4	se
vážou	vázat	k5eAaImIp3nP	vázat
4	[number]	k4	4
protony	proton	k1gInPc7	proton
ze	z	k7c2	z
stromatu	stroma	k1gNnSc2	stroma
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
získá	získat	k5eAaPmIp3nS	získat
protonový	protonový	k2eAgInSc1d1	protonový
gradient	gradient	k1gInSc1	gradient
12	[number]	k4	12
protonů	proton	k1gInPc2	proton
(	(	kIx(	(
<g/>
rozdíl	rozdíl	k1gInSc4	rozdíl
protonů	proton	k1gInPc2	proton
mezi	mezi	k7c7	mezi
stromatem	stroma	k1gNnSc7	stroma
a	a	k8xC	a
thylakoidní	thylakoidní	k2eAgFnSc7d1	thylakoidní
dutinou	dutina	k1gFnSc7	dutina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
zachována	zachován	k2eAgFnSc1d1	zachována
elektroneutralita	elektroneutralita	k1gFnSc1	elektroneutralita
<g/>
,	,	kIx,	,
přemisťují	přemisťovat	k5eAaImIp3nP	přemisťovat
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
membránu	membrána	k1gFnSc4	membrána
ionty	ion	k1gInPc1	ion
Mg	mg	kA	mg
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
a	a	k8xC	a
Cl	Cl	k1gFnSc1	Cl
<g/>
−	−	k?	−
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
necyklickém	cyklický	k2eNgInSc6d1	necyklický
přenosu	přenos	k1gInSc6	přenos
elektronů	elektron	k1gInPc2	elektron
se	s	k7c7	s
z	z	k7c2	z
12	[number]	k4	12
protonů	proton	k1gInPc2	proton
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
4	[number]	k4	4
molekuly	molekula	k1gFnSc2	molekula
ATP	atp	kA	atp
(	(	kIx(	(
<g/>
ze	z	k7c2	z
3	[number]	k4	3
protonů	proton	k1gInPc2	proton
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
1	[number]	k4	1
ATP	atp	kA	atp
<g/>
)	)	kIx)	)
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
molekulu	molekula	k1gFnSc4	molekula
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
foton	foton	k1gInSc4	foton
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
0,5	[number]	k4	0,5
ATP.	atp.	kA	atp.
Kromě	kromě	k7c2	kromě
ATP	atp	kA	atp
též	též	k9	též
při	při	k7c6	při
necyklickém	cyklický	k2eNgInSc6d1	necyklický
přenosu	přenos	k1gInSc6	přenos
vznikají	vznikat	k5eAaImIp3nP	vznikat
2	[number]	k4	2
molekuly	molekula	k1gFnPc1	molekula
NADPH	NADPH	kA	NADPH
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
energie	energie	k1gFnSc1	energie
se	se	k3xPyFc4	se
rovná	rovnat	k5eAaImIp3nS	rovnat
6	[number]	k4	6
molekulám	molekula	k1gFnPc3	molekula
ATP	atp	kA	atp
(	(	kIx(	(
<g/>
0,75	[number]	k4	0,75
ATP	atp	kA	atp
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
foton	foton	k1gInSc4	foton
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
při	při	k7c6	při
necyklickém	cyklický	k2eNgInSc6d1	necyklický
přenosu	přenos	k1gInSc6	přenos
elektronů	elektron	k1gInPc2	elektron
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
1,25	[number]	k4	1,25
ATP	atp	kA	atp
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
pohlcený	pohlcený	k2eAgInSc4d1	pohlcený
foton	foton	k1gInSc4	foton
<g/>
.	.	kIx.	.
</s>
<s>
Cyklický	cyklický	k2eAgInSc1d1	cyklický
přenos	přenos	k1gInSc1	přenos
je	být	k5eAaImIp3nS	být
účinnější	účinný	k2eAgMnSc1d2	účinnější
při	při	k7c6	při
přímé	přímý	k2eAgFnSc6d1	přímá
tvorbě	tvorba	k1gFnSc6	tvorba
ATP	atp	kA	atp
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c4	na
vznik	vznik	k1gInSc4	vznik
jedné	jeden	k4xCgFnSc2	jeden
molekuly	molekula	k1gFnSc2	molekula
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
pouze	pouze	k6eAd1	pouze
2	[number]	k4	2
protonů	proton	k1gInPc2	proton
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
při	při	k7c6	při
něm	on	k3xPp3gInSc6	on
nevzniká	vznikat	k5eNaImIp3nS	vznikat
NADPH	NADPH	kA	NADPH
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
výsledku	výsledek	k1gInSc6	výsledek
výtěžek	výtěžek	k1gInSc1	výtěžek
cyklické	cyklický	k2eAgFnSc2d1	cyklická
dráhy	dráha	k1gFnSc2	dráha
elektronu	elektron	k1gInSc2	elektron
pouze	pouze	k6eAd1	pouze
0,75	[number]	k4	0,75
ATP	atp	kA	atp
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
pohlcený	pohlcený	k2eAgInSc4d1	pohlcený
foton	foton	k1gInSc4	foton
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Z	z	k7c2	z
12	[number]	k4	12
protonů	proton	k1gInPc2	proton
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
dvou	dva	k4xCgInPc2	dva
na	na	k7c4	na
vznik	vznik	k1gInSc4	vznik
jednoho	jeden	k4xCgInSc2	jeden
ATP.	atp.	kA	atp.
Vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
6	[number]	k4	6
ATP	atp	kA	atp
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
molekulu	molekula	k1gFnSc4	molekula
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vydělení	vydělení	k1gNnSc6	vydělení
osmi	osm	k4xCc2	osm
získáme	získat	k5eAaPmIp1nP	získat
počet	počet	k1gInSc4	počet
ATP	atp	kA	atp
vzniklých	vzniklý	k2eAgInPc2d1	vzniklý
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
proton	proton	k1gInSc4	proton
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Temnostní	Temnostní	k2eAgFnSc1d1	Temnostní
fáze	fáze	k1gFnSc1	fáze
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
sekundární	sekundární	k2eAgInPc1d1	sekundární
děje	děj	k1gInPc1	děj
<g/>
"	"	kIx"	"
jsou	být	k5eAaImIp3nP	být
reakce	reakce	k1gFnPc1	reakce
nezávislé	závislý	k2eNgFnPc1d1	nezávislá
na	na	k7c6	na
světle	světlo	k1gNnSc6	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Sice	sice	k8xC	sice
neprobíhají	probíhat	k5eNaImIp3nP	probíhat
ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepotřebují	potřebovat	k5eNaImIp3nP	potřebovat
světelnou	světelný	k2eAgFnSc4d1	světelná
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Temnostní	Temnostní	k2eAgFnPc1d1	Temnostní
reakce	reakce	k1gFnPc1	reakce
probíhají	probíhat	k5eAaImIp3nP	probíhat
ve	v	k7c6	v
stromatu	stroma	k1gNnSc6	stroma
a	a	k8xC	a
ukládají	ukládat	k5eAaImIp3nP	ukládat
chemickou	chemický	k2eAgFnSc4d1	chemická
energii	energie	k1gFnSc4	energie
získanou	získaný	k2eAgFnSc4d1	získaná
ve	v	k7c6	v
světelné	světelný	k2eAgFnSc6d1	světelná
fázi	fáze	k1gFnSc6	fáze
(	(	kIx(	(
<g/>
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
NADPH	NADPH	kA	NADPH
a	a	k8xC	a
ATP	atp	kA	atp
<g/>
)	)	kIx)	)
fixací	fixace	k1gFnPc2	fixace
CO2	CO2	k1gFnPc2	CO2
do	do	k7c2	do
sacharidů	sacharid	k1gInPc2	sacharid
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
tři	tři	k4xCgInPc1	tři
cykly	cyklus	k1gInPc1	cyklus
fixace	fixace	k1gFnSc2	fixace
CO2	CO2	k1gFnSc2	CO2
–	–	k?	–
Calvinův	Calvinův	k2eAgInSc1d1	Calvinův
<g/>
,	,	kIx,	,
Hatch-Slackův	Hatch-Slackův	k2eAgInSc1d1	Hatch-Slackův
a	a	k8xC	a
CAM	CAM	kA	CAM
cyklus	cyklus	k1gInSc1	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
fotosyntézou	fotosyntéza	k1gFnSc7	fotosyntéza
souvisí	souviset	k5eAaImIp3nS	souviset
také	také	k9	také
fotorespirační	fotorespirační	k2eAgInSc1d1	fotorespirační
cyklus	cyklus	k1gInSc1	cyklus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
snižuje	snižovat	k5eAaImIp3nS	snižovat
výnos	výnos	k1gInSc4	výnos
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
především	především	k9	především
u	u	k7c2	u
C	C	kA	C
<g/>
3	[number]	k4	3
<g/>
-rostlin	ostlina	k1gFnPc2	-rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgInPc1d1	různý
cykly	cyklus	k1gInPc1	cyklus
probíhají	probíhat	k5eAaImIp3nP	probíhat
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
v	v	k7c6	v
jakých	jaký	k3yQgFnPc6	jaký
rostlinách	rostlina	k1gFnPc6	rostlina
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
dochází	docházet	k5eAaImIp3nS	docházet
<g/>
,	,	kIx,	,
např.	např.	kA	např.
C	C	kA	C
<g/>
3	[number]	k4	3
<g/>
-cyklus	yklus	k1gInSc4	-cyklus
v	v	k7c6	v
pšenici	pšenice	k1gFnSc6	pšenice
<g/>
,	,	kIx,	,
ječmeni	ječmen	k1gInSc3	ječmen
nebo	nebo	k8xC	nebo
hrachu	hrách	k1gInSc3	hrách
(	(	kIx(	(
<g/>
mírný	mírný	k2eAgInSc1d1	mírný
podnebný	podnebný	k2eAgInSc1d1	podnebný
pás	pás	k1gInSc1	pás
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
4	[number]	k4	4
<g/>
-cyklus	yklus	k1gInSc4	-cyklus
v	v	k7c6	v
kukuřici	kukuřice	k1gFnSc6	kukuřice
<g/>
,	,	kIx,	,
prosu	proso	k1gNnSc6	proso
nebo	nebo	k8xC	nebo
cukrové	cukrový	k2eAgFnSc3d1	cukrová
třtině	třtina	k1gFnSc3	třtina
(	(	kIx(	(
<g/>
subtropický	subtropický	k2eAgInSc1d1	subtropický
podnebný	podnebný	k2eAgInSc1d1	podnebný
pás	pás	k1gInSc1	pás
<g/>
)	)	kIx)	)
a	a	k8xC	a
CAM	CAM	kA	CAM
cyklus	cyklus	k1gInSc1	cyklus
v	v	k7c6	v
kaktusech	kaktus	k1gInPc6	kaktus
(	(	kIx(	(
<g/>
tropický	tropický	k2eAgInSc1d1	tropický
podnebný	podnebný	k2eAgInSc1d1	podnebný
pás	pás	k1gInSc1	pás
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Calvinův	Calvinův	k2eAgInSc4d1	Calvinův
cyklus	cyklus	k1gInSc4	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Calvinův	Calvinův	k2eAgInSc1d1	Calvinův
cyklus	cyklus	k1gInSc1	cyklus
neboli	neboli	k8xC	neboli
C	C	kA	C
<g/>
3	[number]	k4	3
<g/>
-cyklus	yklus	k1gInSc1	-cyklus
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc7	první
stálé	stálý	k2eAgFnPc1d1	stálá
produkty	produkt	k1gInPc4	produkt
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
při	při	k7c6	při
něm	on	k3xPp3gInSc6	on
vznikají	vznikat	k5eAaImIp3nP	vznikat
<g/>
,	,	kIx,	,
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
3	[number]	k4	3
atomy	atom	k1gInPc4	atom
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
3	[number]	k4	3
fází	fáze	k1gFnPc2	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Karboxylace	Karboxylace	k1gFnPc1	Karboxylace
neboli	neboli	k8xC	neboli
fixace	fixace	k1gFnPc1	fixace
CO	co	k9	co
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
redukce	redukce	k1gFnSc1	redukce
a	a	k8xC	a
regenerace	regenerace	k1gFnSc1	regenerace
ribulóza-	ribulóza-	k?	ribulóza-
<g/>
1,5	[number]	k4	1,5
<g/>
-bisfosfátu	isfosfát	k1gInSc2	-bisfosfát
<g/>
.	.	kIx.	.
</s>
<s>
Navázání	navázání	k1gNnSc1	navázání
CO2	CO2	k1gFnSc2	CO2
na	na	k7c6	na
ribulóza-	ribulóza-	k?	ribulóza-
<g/>
1,5	[number]	k4	1,5
<g/>
-bisfosfát	isfosfát	k1gInSc1	-bisfosfát
je	být	k5eAaImIp3nS	být
katalyzováno	katalyzovat	k5eAaBmNgNnS	katalyzovat
enzymem	enzym	k1gInSc7	enzym
Rubisco	Rubisco	k1gNnSc4	Rubisco
(	(	kIx(	(
<g/>
ribulóza-	ribulóza-	k?	ribulóza-
<g/>
1,5	[number]	k4	1,5
<g/>
-bisfosfát-karboxyláza	isfosfátarboxyláza	k1gFnSc1	-bisfosfát-karboxyláza
<g/>
/	/	kIx~	/
<g/>
oxygenáza	oxygenáza	k1gFnSc1	oxygenáza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
reakcí	reakce	k1gFnSc7	reakce
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
adiční	adiční	k2eAgInSc1d1	adiční
produkt	produkt	k1gInSc1	produkt
se	se	k3xPyFc4	se
štěpí	štěpit	k5eAaImIp3nS	štěpit
na	na	k7c4	na
2	[number]	k4	2
molekuly	molekula	k1gFnSc2	molekula
3	[number]	k4	3
<g/>
-fosfoglycerátu	osfoglycerát	k1gInSc2	-fosfoglycerát
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
enzymu	enzym	k1gInSc2	enzym
a	a	k8xC	a
NADPH	NADPH	kA	NADPH
redukuje	redukovat	k5eAaBmIp3nS	redukovat
na	na	k7c4	na
glyceraldehyd-	glyceraldehyd-	k?	glyceraldehyd-
<g/>
3	[number]	k4	3
<g/>
-fosfát	osfát	k1gInSc1	-fosfát
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
jedna	jeden	k4xCgFnSc1	jeden
molekula	molekula	k1gFnSc1	molekula
glyceraldehyd-	glyceraldehyd-	k?	glyceraldehyd-
<g/>
3	[number]	k4	3
<g/>
-fosfátu	osfát	k1gInSc2	-fosfát
ze	z	k7c2	z
6	[number]	k4	6
opouští	opouštět	k5eAaImIp3nS	opouštět
cyklus	cyklus	k1gInSc4	cyklus
a	a	k8xC	a
syntetizují	syntetizovat	k5eAaImIp3nP	syntetizovat
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
další	další	k2eAgFnSc2d1	další
látky	látka	k1gFnSc2	látka
(	(	kIx(	(
<g/>
sacharidy	sacharid	k1gInPc1	sacharid
<g/>
,	,	kIx,	,
škrob	škrob	k1gInSc1	škrob
<g/>
,	,	kIx,	,
bílkoviny	bílkovina	k1gFnPc1	bílkovina
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zbývajících	zbývající	k2eAgFnPc2d1	zbývající
pěti	pět	k4xCc2	pět
molekul	molekula	k1gFnPc2	molekula
glyceraldehyd-	glyceraldehyd-	k?	glyceraldehyd-
<g/>
3	[number]	k4	3
<g/>
-fosfátu	osfát	k1gInSc2	-fosfát
v	v	k7c6	v
regenerační	regenerační	k2eAgFnSc6d1	regenerační
fázi	fáze	k1gFnSc6	fáze
vznikají	vznikat	k5eAaImIp3nP	vznikat
opět	opět	k6eAd1	opět
3	[number]	k4	3
molekuly	molekula	k1gFnSc2	molekula
ribulóza-	ribulóza-	k?	ribulóza-
<g/>
5	[number]	k4	5
<g/>
-fosfátu	osfát	k1gInSc2	-fosfát
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc7	jejich
následnou	následný	k2eAgFnSc7d1	následná
fosforylací	fosforylace	k1gFnSc7	fosforylace
ATP	atp	kA	atp
vznikají	vznikat	k5eAaImIp3nP	vznikat
3	[number]	k4	3
molekuly	molekula	k1gFnPc4	molekula
ribulóza-	ribulóza-	k?	ribulóza-
<g/>
1,5	[number]	k4	1,5
<g/>
-bisfosfátu	isfosfát	k1gInSc2	-bisfosfát
<g/>
.	.	kIx.	.
</s>
<s>
Calvinův	Calvinův	k2eAgInSc1d1	Calvinův
cyklus	cyklus	k1gInSc1	cyklus
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
uzavírá	uzavírat	k5eAaPmIp3nS	uzavírat
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
molekul	molekula	k1gFnPc2	molekula
ribulóza-	ribulóza-	k?	ribulóza-
<g/>
1,5	[number]	k4	1,5
<g/>
-bisfosfátu	isfosfát	k1gInSc2	-bisfosfát
a	a	k8xC	a
tří	tři	k4xCgFnPc2	tři
molekul	molekula	k1gFnPc2	molekula
CO	co	k8xS	co
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
vzniknou	vzniknout	k5eAaPmIp3nP	vzniknout
tři	tři	k4xCgFnPc4	tři
molekuly	molekula	k1gFnPc4	molekula
ribulóza-	ribulóza-	k?	ribulóza-
<g/>
1,5	[number]	k4	1,5
<g/>
-bisfosfátu	isfosfát	k1gInSc2	-bisfosfát
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
molekula	molekula	k1gFnSc1	molekula
glyceraldehyd-	glyceraldehyd-	k?	glyceraldehyd-
<g/>
3	[number]	k4	3
<g/>
-fosfátu	osfát	k1gInSc2	-fosfát
<g/>
.	.	kIx.	.
</s>
<s>
C	C	kA	C
<g/>
3	[number]	k4	3
<g/>
-cyklus	yklus	k1gInSc1	-cyklus
využívají	využívat	k5eAaImIp3nP	využívat
především	především	k9	především
rostliny	rostlina	k1gFnPc1	rostlina
mírného	mírný	k2eAgNnSc2d1	mírné
a	a	k8xC	a
chladných	chladný	k2eAgInPc2d1	chladný
pásů	pás	k1gInPc2	pás
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
oblastech	oblast	k1gFnPc6	oblast
není	být	k5eNaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
a	a	k8xC	a
fotorespirace	fotorespirace	k1gFnSc1	fotorespirace
nepřevládá	převládat	k5eNaImIp3nS	převládat
nad	nad	k7c7	nad
fotosyntézou	fotosyntéza	k1gFnSc7	fotosyntéza
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Hatch-Slackův	Hatch-Slackův	k2eAgInSc4d1	Hatch-Slackův
cyklus	cyklus	k1gInSc4	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgNnPc6	jenž
probíhá	probíhat	k5eAaImIp3nS	probíhat
Hatch-Slackův	Hatch-Slackův	k2eAgInSc1d1	Hatch-Slackův
cyklus	cyklus	k1gInSc1	cyklus
neboli	neboli	k8xC	neboli
C	C	kA	C
<g/>
4	[number]	k4	4
<g/>
-cyklus	yklus	k1gInSc4	-cyklus
(	(	kIx(	(
<g/>
vznikají	vznikat	k5eAaImIp3nP	vznikat
látky	látka	k1gFnPc1	látka
se	s	k7c7	s
4	[number]	k4	4
atomy	atom	k1gInPc7	atom
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
charakteristickou	charakteristický	k2eAgFnSc7d1	charakteristická
anatomickou	anatomický	k2eAgFnSc7d1	anatomická
stavbou	stavba	k1gFnSc7	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Obsahují	obsahovat	k5eAaImIp3nP	obsahovat
mezofylové	mezofylový	k2eAgFnPc1d1	mezofylový
buňky	buňka	k1gFnPc1	buňka
(	(	kIx(	(
<g/>
fixace	fixace	k1gFnPc1	fixace
CO	co	k8xS	co
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
a	a	k8xC	a
buňky	buňka	k1gFnPc1	buňka
pochvy	pochva	k1gFnSc2	pochva
cévního	cévní	k2eAgInSc2d1	cévní
svazku	svazek	k1gInSc2	svazek
(	(	kIx(	(
<g/>
uvolnění	uvolnění	k1gNnSc4	uvolnění
CO2	CO2	k1gFnSc2	CO2
do	do	k7c2	do
Calvinova	Calvinův	k2eAgInSc2d1	Calvinův
cyklu	cyklus	k1gInSc2	cyklus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chloroplastech	chloroplast	k1gInPc6	chloroplast
mezofylových	mezofylový	k2eAgFnPc2d1	mezofylový
buněk	buňka	k1gFnPc2	buňka
chybí	chybit	k5eAaPmIp3nS	chybit
enzym	enzym	k1gInSc4	enzym
Rubisco	Rubisco	k6eAd1	Rubisco
a	a	k8xC	a
CO2	CO2	k1gFnSc1	CO2
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
HCO	HCO	kA	HCO
<g/>
3	[number]	k4	3
<g/>
−	−	k?	−
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
fosfoenolpyruátem	fosfoenolpyruát	k1gInSc7	fosfoenolpyruát
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
oxalacetátu	oxalacetát	k1gInSc2	oxalacetát
<g/>
.	.	kIx.	.
</s>
<s>
Oxalacetát	Oxalacetát	k1gInSc1	Oxalacetát
je	být	k5eAaImIp3nS	být
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
enzymu	enzym	k1gInSc2	enzym
malátdehydrogenázy	malátdehydrogenáza	k1gFnSc2	malátdehydrogenáza
a	a	k8xC	a
NADPH	NADPH	kA	NADPH
redukován	redukován	k2eAgInSc1d1	redukován
na	na	k7c4	na
malát	malát	k1gInSc4	malát
<g/>
.	.	kIx.	.
</s>
<s>
Malát	Malát	k1gInSc1	Malát
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
buněk	buňka	k1gFnPc2	buňka
pochvy	pochva	k1gFnSc2	pochva
cévního	cévní	k2eAgInSc2d1	cévní
svazku	svazek	k1gInSc2	svazek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
pomocí	pomocí	k7c2	pomocí
NADP	NADP	kA	NADP
<g/>
+	+	kIx~	+
oxidován	oxidovat	k5eAaBmNgInS	oxidovat
na	na	k7c4	na
pyruát	pyruát	k1gInSc4	pyruát
a	a	k8xC	a
současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
CO	co	k9	co
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
do	do	k7c2	do
Calvinova	Calvinův	k2eAgInSc2d1	Calvinův
cyklu	cyklus	k1gInSc2	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Pyruvát	Pyruvát	k1gInSc1	Pyruvát
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
buněk	buňka	k1gFnPc2	buňka
mezofylu	mezofyl	k1gInSc2	mezofyl
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
za	za	k7c2	za
spotřeby	spotřeba	k1gFnSc2	spotřeba
ATP	atp	kA	atp
fosforylován	fosforylován	k2eAgInSc1d1	fosforylován
na	na	k7c4	na
fosfoenolpyruát	fosfoenolpyruát	k1gInSc4	fosfoenolpyruát
<g/>
.	.	kIx.	.
</s>
<s>
C4	C4	k4	C4
cyklus	cyklus	k1gInSc1	cyklus
využívají	využívat	k5eAaImIp3nP	využívat
především	především	k9	především
teplomilné	teplomilný	k2eAgFnPc1d1	teplomilná
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
při	při	k7c6	při
zvýšené	zvýšený	k2eAgFnSc6d1	zvýšená
teplotě	teplota	k1gFnSc6	teplota
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
fotorespirace	fotorespirace	k1gFnSc1	fotorespirace
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
klesá	klesat	k5eAaImIp3nS	klesat
účinnost	účinnost	k1gFnSc4	účinnost
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
koncentrují	koncentrovat	k5eAaBmIp3nP	koncentrovat
CO2	CO2	k1gMnPc1	CO2
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
vejde	vejít	k5eAaPmIp3nS	vejít
do	do	k7c2	do
Calvinova	Calvinův	k2eAgInSc2d1	Calvinův
cyklu	cyklus	k1gInSc2	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
CAM	CAM	kA	CAM
cyklus	cyklus	k1gInSc1	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
CAM	CAM	kA	CAM
cyklus	cyklus	k1gInSc1	cyklus
neboli	neboli	k8xC	neboli
Crassulacean	Crassulacean	k1gMnSc1	Crassulacean
Acid	Acid	k1gMnSc1	Acid
Metabolism	Metabolism	k1gMnSc1	Metabolism
(	(	kIx(	(
<g/>
metabolismus	metabolismus	k1gInSc1	metabolismus
kyselin	kyselina	k1gFnPc2	kyselina
u	u	k7c2	u
tučnolistých	tučnolistý	k2eAgFnPc2d1	tučnolistá
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yQgInPc2	který
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
cyklus	cyklus	k1gInSc1	cyklus
poprvé	poprvé	k6eAd1	poprvé
pozorován	pozorován	k2eAgMnSc1d1	pozorován
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
obměnou	obměna	k1gFnSc7	obměna
Hatch-Slackova	Hatch-Slackův	k2eAgInSc2d1	Hatch-Slackův
cyklu	cyklus	k1gInSc2	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
CO2	CO2	k4	CO2
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
ukládán	ukládán	k2eAgInSc1d1	ukládán
jako	jako	k8xS	jako
zásoba	zásoba	k1gFnSc1	zásoba
do	do	k7c2	do
vakuol	vakuola	k1gFnPc2	vakuola
a	a	k8xC	a
ve	v	k7c6	v
dne	den	k1gInSc2	den
opětovně	opětovně	k6eAd1	opětovně
zpracován	zpracovat	k5eAaPmNgInS	zpracovat
Calvinovým	Calvinův	k2eAgInSc7d1	Calvinův
cyklem	cyklus	k1gInSc7	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
cyklus	cyklus	k1gInSc1	cyklus
využívají	využívat	k5eAaPmIp3nP	využívat
pouštní	pouštní	k2eAgFnPc4d1	pouštní
rostliny	rostlina	k1gFnPc4	rostlina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
sukulenty	sukulent	k1gInPc1	sukulent
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
musí	muset	k5eAaImIp3nP	muset
velmi	velmi	k6eAd1	velmi
šetřit	šetřit	k5eAaImF	šetřit
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
otevírají	otevírat	k5eAaImIp3nP	otevírat
průduchy	průduch	k1gInPc1	průduch
jenom	jenom	k9	jenom
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vážou	vázat	k5eAaImIp3nP	vázat
CO2	CO2	k1gFnPc1	CO2
do	do	k7c2	do
malátu	malát	k1gInSc2	malát
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
uložení	uložení	k1gNnSc4	uložení
zásoby	zásoba	k1gFnSc2	zásoba
CO2	CO2	k1gMnPc2	CO2
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
fosfoenolpyruátu	fosfoenolpyruát	k1gInSc2	fosfoenolpyruát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
získávají	získávat	k5eAaImIp3nP	získávat
glykolytickým	glykolytický	k2eAgNnSc7d1	glykolytický
štěpením	štěpení	k1gNnSc7	štěpení
škrobu	škrob	k1gInSc2	škrob
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
malát	malát	k1gInSc1	malát
štěpí	štěpit	k5eAaImIp3nS	štěpit
na	na	k7c4	na
CO	co	k6eAd1	co
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
Calvinova	Calvinův	k2eAgInSc2d1	Calvinův
cyklu	cyklus	k1gInSc2	cyklus
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
pyruát	pyruát	k1gInSc4	pyruát
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
syntetizuje	syntetizovat	k5eAaImIp3nS	syntetizovat
škrob	škrob	k1gInSc4	škrob
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
provádějí	provádět	k5eAaImIp3nP	provádět
CAM	CAM	kA	CAM
rostliny	rostlina	k1gFnPc1	rostlina
fotosyntézu	fotosyntéza	k1gFnSc4	fotosyntéza
s	s	k7c7	s
minimálními	minimální	k2eAgFnPc7d1	minimální
ztrátami	ztráta	k1gFnPc7	ztráta
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Fotorespirace	fotorespirace	k1gFnSc2	fotorespirace
<g/>
.	.	kIx.	.
</s>
<s>
Fotorespirace	fotorespirace	k1gFnSc1	fotorespirace
neboli	neboli	k8xC	neboli
světelné	světelný	k2eAgNnSc1d1	světelné
dýchání	dýchání	k1gNnSc1	dýchání
rostlin	rostlina	k1gFnPc2	rostlina
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
rostlina	rostlina	k1gFnSc1	rostlina
přijímá	přijímat	k5eAaImIp3nS	přijímat
kyslík	kyslík	k1gInSc4	kyslík
a	a	k8xC	a
produkuje	produkovat	k5eAaImIp3nS	produkovat
CO	co	k6eAd1	co
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
fotorespiraci	fotorespirace	k1gFnSc6	fotorespirace
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
neuvolňuje	uvolňovat	k5eNaImIp3nS	uvolňovat
ATP	atp	kA	atp
(	(	kIx(	(
<g/>
energie	energie	k1gFnSc1	energie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
štěpení	štěpení	k1gNnSc3	štěpení
meziproduktů	meziprodukt	k1gInPc2	meziprodukt
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
<g/>
,	,	kIx,	,
produkci	produkce	k1gFnSc4	produkce
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
ke	k	k7c3	k
ztrátám	ztráta	k1gFnPc3	ztráta
na	na	k7c6	na
substrátu	substrát	k1gInSc6	substrát
a	a	k8xC	a
energii	energie	k1gFnSc6	energie
<g/>
.	.	kIx.	.
</s>
<s>
Fotorespirace	fotorespirace	k1gFnSc1	fotorespirace
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
karboxylačně-oxidační	karboxylačněxidační	k2eAgFnSc7d1	karboxylačně-oxidační
aktivitou	aktivita	k1gFnSc7	aktivita
enzymu	enzym	k1gInSc2	enzym
Rubisco	Rubisco	k1gMnSc1	Rubisco
(	(	kIx(	(
<g/>
ribulóza-	ribulóza-	k?	ribulóza-
<g/>
1,5	[number]	k4	1,5
<g/>
-bisfosfátkarboxyláza	isfosfátkarboxyláza	k1gFnSc1	-bisfosfátkarboxyláza
<g/>
/	/	kIx~	/
<g/>
oxygenáza	oxygenáza	k1gFnSc1	oxygenáza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
běžných	běžný	k2eAgFnPc2d1	běžná
atmosférických	atmosférický	k2eAgFnPc2d1	atmosférická
podmínek	podmínka	k1gFnPc2	podmínka
při	při	k7c6	při
25	[number]	k4	25
°	°	k?	°
<g/>
C	C	kA	C
převažuje	převažovat	k5eAaImIp3nS	převažovat
karboxylace	karboxylace	k1gFnSc1	karboxylace
nad	nad	k7c7	nad
oxygenací	oxygenace	k1gFnSc7	oxygenace
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
asi	asi	k9	asi
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Fotorespirace	fotorespirace	k1gFnSc1	fotorespirace
je	být	k5eAaImIp3nS	být
významná	významný	k2eAgFnSc1d1	významná
hlavně	hlavně	k9	hlavně
u	u	k7c2	u
C	C	kA	C
<g/>
3	[number]	k4	3
<g/>
-rostlin	ostlina	k1gFnPc2	-rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
fotorespiraci	fotorespirace	k1gFnSc6	fotorespirace
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
navázáním	navázání	k1gNnSc7	navázání
kyslíku	kyslík	k1gInSc2	kyslík
na	na	k7c4	na
ribulóza-	ribulóza-	k?	ribulóza-
<g/>
1,5	[number]	k4	1,5
<g/>
-bisfosfát	isfosfát	k5eAaPmF	-bisfosfát
pětiuhlíkatý	pětiuhlíkatý	k2eAgInSc4d1	pětiuhlíkatý
meziprodukt	meziprodukt	k1gInSc4	meziprodukt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
není	být	k5eNaImIp3nS	být
stálý	stálý	k2eAgInSc1d1	stálý
a	a	k8xC	a
rozpadá	rozpadat	k5eAaImIp3nS	rozpadat
se	se	k3xPyFc4	se
na	na	k7c4	na
3	[number]	k4	3
<g/>
-fosfoglycerát	osfoglycerát	k1gInSc4	-fosfoglycerát
a	a	k8xC	a
2	[number]	k4	2
<g/>
-fosfoglykolát	osfoglykolát	k1gInSc1	-fosfoglykolát
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
-fosfoglycerát	osfoglycerát	k1gInSc1	-fosfoglycerát
vstoupí	vstoupit	k5eAaPmIp3nP	vstoupit
do	do	k7c2	do
Calvinova	Calvinův	k2eAgInSc2d1	Calvinův
cyklu	cyklus	k1gInSc2	cyklus
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
2	[number]	k4	2
<g/>
-fosfoglykolát	osfoglykolát	k1gInSc1	-fosfoglykolát
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
metabolizován	metabolizován	k2eAgMnSc1d1	metabolizován
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
využit	využít	k5eAaPmNgInS	využít
k	k	k7c3	k
syntéze	syntéza	k1gFnSc3	syntéza
některých	některý	k3yIgFnPc2	některý
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
je	být	k5eAaImIp3nS	být
však	však	k9	však
rozložena	rozložit	k5eAaPmNgFnS	rozložit
až	až	k9	až
na	na	k7c4	na
CO	co	k6eAd1	co
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Fotorespirace	fotorespirace	k1gFnSc1	fotorespirace
neprobíhá	probíhat	k5eNaImIp3nS	probíhat
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
chloroplastech	chloroplast	k1gInPc6	chloroplast
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
fáze	fáze	k1gFnPc1	fáze
se	se	k3xPyFc4	se
uskutečňují	uskutečňovat	k5eAaImIp3nP	uskutečňovat
také	také	k9	také
v	v	k7c6	v
cytosolu	cytosol	k1gInSc6	cytosol
<g/>
,	,	kIx,	,
peroxizomech	peroxizom	k1gInPc6	peroxizom
a	a	k8xC	a
mitochondriích	mitochondrie	k1gFnPc6	mitochondrie
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
z	z	k7c2	z
měření	měření	k1gNnSc2	měření
produkce	produkce	k1gFnSc2	produkce
O2	O2	k1gFnSc2	O2
nebo	nebo	k8xC	nebo
spotřeby	spotřeba	k1gFnSc2	spotřeba
CO	co	k9	co
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
řadě	řada	k1gFnSc6	řada
faktorů	faktor	k1gInPc2	faktor
<g/>
,	,	kIx,	,
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
i	i	k8xC	i
vnějších	vnější	k2eAgInPc2d1	vnější
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nepůsobí	působit	k5eNaImIp3nP	působit
samy	sám	k3xTgFnPc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzájemně	vzájemně	k6eAd1	vzájemně
podmíněně	podmíněně	k6eAd1	podmíněně
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
faktorů	faktor	k1gInPc2	faktor
jde	jít	k5eAaImIp3nS	jít
zejména	zejména	k9	zejména
o	o	k7c4	o
množství	množství	k1gNnSc4	množství
chlorofylu	chlorofyl	k1gInSc2	chlorofyl
<g/>
,	,	kIx,	,
stáří	stáří	k1gNnSc4	stáří
listů	list	k1gInPc2	list
a	a	k8xC	a
minerální	minerální	k2eAgFnSc4d1	minerální
výživu	výživa	k1gFnSc4	výživa
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
vnější	vnější	k2eAgInPc4d1	vnější
činitele	činitel	k1gInPc4	činitel
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Světlo	světlo	k1gNnSc1	světlo
–	–	k?	–
fotosyntézu	fotosyntéza	k1gFnSc4	fotosyntéza
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
spektrálním	spektrální	k2eAgNnSc7d1	spektrální
složením	složení	k1gNnSc7	složení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
intenzitou	intenzita	k1gFnSc7	intenzita
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Rostoucí	rostoucí	k2eAgFnSc1d1	rostoucí
intenzita	intenzita	k1gFnSc1	intenzita
může	moct	k5eAaImIp3nS	moct
rychlost	rychlost	k1gFnSc4	rychlost
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
jen	jen	k9	jen
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
úrovně	úroveň	k1gFnSc2	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Koncentrace	koncentrace	k1gFnSc1	koncentrace
CO2	CO2	k1gFnSc2	CO2
–	–	k?	–
v	v	k7c6	v
ovzduší	ovzduší	k1gNnSc6	ovzduší
asi	asi	k9	asi
0,03	[number]	k4	0,03
%	%	kIx~	%
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
optimální	optimální	k2eAgFnSc1d1	optimální
<g/>
,	,	kIx,	,
rostliny	rostlina	k1gFnPc1	rostlina
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
více	hodně	k6eAd2	hodně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
závislost	závislost	k1gFnSc4	závislost
rychlosti	rychlost	k1gFnSc2	rychlost
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
na	na	k7c4	na
množství	množství	k1gNnSc4	množství
CO2	CO2	k1gFnSc2	CO2
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
stejného	stejný	k2eAgInSc2d1	stejný
charakteru	charakter	k1gInSc2	charakter
jako	jako	k8xC	jako
u	u	k7c2	u
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
zvýšit	zvýšit	k5eAaPmF	zvýšit
přesunutím	přesunutí	k1gNnSc7	přesunutí
rostlin	rostlina	k1gFnPc2	rostlina
do	do	k7c2	do
skleníku	skleník	k1gInSc2	skleník
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
–	–	k?	–
výrazně	výrazně	k6eAd1	výrazně
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
fotosyntézu	fotosyntéza	k1gFnSc4	fotosyntéza
<g/>
,	,	kIx,	,
u	u	k7c2	u
našich	náš	k3xOp1gFnPc2	náš
rostlin	rostlina	k1gFnPc2	rostlina
je	být	k5eAaImIp3nS	být
optimum	optimum	k1gNnSc1	optimum
asi	asi	k9	asi
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
nad	nad	k7c7	nad
30	[number]	k4	30
°	°	k?	°
<g/>
C	C	kA	C
nastává	nastávat	k5eAaImIp3nS	nastávat
výrazný	výrazný	k2eAgInSc1d1	výrazný
pokles	pokles	k1gInSc1	pokles
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
–	–	k?	–
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
nezbytná	nezbytný	k2eAgFnSc1d1	nezbytná
<g/>
,	,	kIx,	,
nedostatek	nedostatek	k1gInSc1	nedostatek
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
uzavíráním	uzavírání	k1gNnSc7	uzavírání
průduchů	průduch	k1gInPc2	průduch
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobí	způsobit	k5eAaPmIp3nS	způsobit
zastavení	zastavení	k1gNnSc4	zastavení
přístupu	přístup	k1gInSc2	přístup
CO	co	k8xS	co
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Fotosyntézu	fotosyntéza	k1gFnSc4	fotosyntéza
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
spektrum	spektrum	k1gNnSc1	spektrum
a	a	k8xC	a
intenzita	intenzita	k1gFnSc1	intenzita
světelného	světelný	k2eAgNnSc2d1	světelné
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Spektrum	spektrum	k1gNnSc1	spektrum
vhodné	vhodný	k2eAgNnSc1d1	vhodné
pro	pro	k7c4	pro
fotosyntézu	fotosyntéza	k1gFnSc4	fotosyntéza
(	(	kIx(	(
<g/>
FAR	fara	k1gFnPc2	fara
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
380	[number]	k4	380
<g/>
–	–	k?	–
<g/>
760	[number]	k4	760
nm	nm	k?	nm
<g/>
.	.	kIx.	.
</s>
<s>
Přechodem	přechod	k1gInSc7	přechod
od	od	k7c2	od
červených	červená	k1gFnPc2	červená
k	k	k7c3	k
fialovým	fialový	k2eAgInPc3d1	fialový
paprskům	paprsek	k1gInPc3	paprsek
se	se	k3xPyFc4	se
kvantový	kvantový	k2eAgInSc1d1	kvantový
zisk	zisk	k1gInSc1	zisk
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
snižující	snižující	k2eAgFnSc1d1	snižující
se	se	k3xPyFc4	se
absorpce	absorpce	k1gFnSc1	absorpce
chlorofylů	chlorofyl	k1gInPc2	chlorofyl
částečně	částečně	k6eAd1	částečně
kompenzuje	kompenzovat	k5eAaBmIp3nS	kompenzovat
doprovodnými	doprovodný	k2eAgInPc7d1	doprovodný
pigmenty	pigment	k1gInPc7	pigment
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
efektivnost	efektivnost	k1gFnSc1	efektivnost
je	být	k5eAaImIp3nS	být
však	však	k9	však
menší	malý	k2eAgMnSc1d2	menší
než	než	k8xS	než
u	u	k7c2	u
chlorofylů	chlorofyl	k1gInPc2	chlorofyl
<g/>
.	.	kIx.	.
</s>
<s>
Minimální	minimální	k2eAgFnSc1d1	minimální
intenzita	intenzita	k1gFnSc1	intenzita
světla	světlo	k1gNnSc2	světlo
pro	pro	k7c4	pro
začátek	začátek	k1gInSc4	začátek
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
různých	různý	k2eAgFnPc2d1	různá
rostlin	rostlina	k1gFnPc2	rostlina
různá	různý	k2eAgFnSc1d1	různá
<g/>
.	.	kIx.	.
</s>
<s>
Některým	některý	k3yIgMnPc3	některý
stačí	stačit	k5eAaBmIp3nS	stačit
již	již	k9	již
intenzita	intenzita	k1gFnSc1	intenzita
petrolejové	petrolejový	k2eAgFnPc1d1	petrolejová
lampy	lampa	k1gFnPc1	lampa
<g/>
,	,	kIx,	,
řasy	řasa	k1gFnPc1	řasa
(	(	kIx(	(
<g/>
Algae	Alga	k1gFnPc1	Alga
<g/>
)	)	kIx)	)
fotosyntetizují	fotosyntetizovat	k5eAaPmIp3nP	fotosyntetizovat
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
záření	záření	k1gNnSc1	záření
intenzitu	intenzita	k1gFnSc4	intenzita
měsíčního	měsíční	k2eAgNnSc2d1	měsíční
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
přibývající	přibývající	k2eAgFnSc7d1	přibývající
intenzitou	intenzita	k1gFnSc7	intenzita
světla	světlo	k1gNnSc2	světlo
se	se	k3xPyFc4	se
rychlost	rychlost	k1gFnSc1	rychlost
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
příjem	příjem	k1gInSc1	příjem
a	a	k8xC	a
výdej	výdej	k1gInSc1	výdej
CO2	CO2	k1gFnSc2	CO2
vyrovná	vyrovnat	k5eAaBmIp3nS	vyrovnat
<g/>
,	,	kIx,	,
nastává	nastávat	k5eAaImIp3nS	nastávat
tzv.	tzv.	kA	tzv.
kompenzační	kompenzační	k2eAgInSc1d1	kompenzační
světelný	světelný	k2eAgInSc1d1	světelný
bod	bod	k1gInSc1	bod
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
pak	pak	k6eAd1	pak
dále	daleko	k6eAd2	daleko
roste	růst	k5eAaImIp3nS	růst
až	až	k9	až
do	do	k7c2	do
bodu	bod	k1gInSc2	bod
světelného	světelný	k2eAgNnSc2d1	světelné
nasycení	nasycení	k1gNnSc2	nasycení
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
ustálí	ustálit	k5eAaPmIp3nS	ustálit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vysokých	vysoký	k2eAgFnPc6d1	vysoká
intenzitách	intenzita	k1gFnPc6	intenzita
světla	světlo	k1gNnSc2	světlo
vzniká	vznikat	k5eAaImIp3nS	vznikat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
kyslíkových	kyslíkový	k2eAgInPc2d1	kyslíkový
radikálů	radikál	k1gInPc2	radikál
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
porušit	porušit	k5eAaPmF	porušit
fotosystém	fotosystý	k2eAgNnSc6d1	fotosystý
II	II	kA	II
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
zastavit	zastavit	k5eAaPmF	zastavit
fotosyntézu	fotosyntéza	k1gFnSc4	fotosyntéza
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
proto	proto	k8xC	proto
obětují	obětovat	k5eAaBmIp3nP	obětovat
bílkovinu	bílkovina	k1gFnSc4	bílkovina
D1	D1	k1gFnSc2	D1
(	(	kIx(	(
<g/>
OEC	OEC	kA	OEC
–	–	k?	–
bílkovinný	bílkovinný	k2eAgInSc4d1	bílkovinný
komplex	komplex	k1gInSc4	komplex
s	s	k7c7	s
manganem	mangan	k1gInSc7	mangan
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
kyslík	kyslík	k1gInSc1	kyslík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zamezily	zamezit	k5eAaPmAgFnP	zamezit
vzniku	vznik	k1gInSc3	vznik
nebezpečných	bezpečný	k2eNgMnPc2d1	nebezpečný
radikálů	radikál	k1gMnPc2	radikál
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
bílkovinu	bílkovina	k1gFnSc4	bílkovina
mohou	moct	k5eAaImIp3nP	moct
rostliny	rostlina	k1gFnPc1	rostlina
při	při	k7c6	při
zlepšení	zlepšení	k1gNnSc6	zlepšení
podmínek	podmínka	k1gFnPc2	podmínka
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
opět	opět	k6eAd1	opět
nasyntetizovat	nasyntetizovat	k5eAaPmF	nasyntetizovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
gen	gen	k1gInSc1	gen
pro	pro	k7c4	pro
její	její	k3xOp3gInSc4	její
vznik	vznik	k1gInSc4	vznik
je	být	k5eAaImIp3nS	být
uložen	uložit	k5eAaPmNgInS	uložit
v	v	k7c6	v
DNA	DNA	kA	DNA
chloroplastu	chloroplast	k1gInSc6	chloroplast
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
vysoké	vysoký	k2eAgFnSc3d1	vysoká
intenzitě	intenzita	k1gFnSc3	intenzita
světla	světlo	k1gNnSc2	světlo
brání	bránit	k5eAaImIp3nP	bránit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
čas	čas	k1gInSc4	čas
přeruší	přerušit	k5eAaPmIp3nS	přerušit
svůj	svůj	k3xOyFgInSc4	svůj
fotosyntetický	fotosyntetický	k2eAgInSc4d1	fotosyntetický
aparát	aparát	k1gInSc4	aparát
<g/>
.	.	kIx.	.
</s>
<s>
Závislost	závislost	k1gFnSc1	závislost
intenzity	intenzita	k1gFnSc2	intenzita
světla	světlo	k1gNnSc2	světlo
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
světelná	světelný	k2eAgFnSc1d1	světelná
křivka	křivka	k1gFnSc1	křivka
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
požadavků	požadavek	k1gInPc2	požadavek
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c6	na
světlomilné	světlomilný	k2eAgFnSc6d1	světlomilná
a	a	k8xC	a
stínomilné	stínomilný	k2eAgFnSc6d1	stínomilná
<g/>
.	.	kIx.	.
</s>
<s>
Stínomilné	stínomilný	k2eAgInPc4d1	stínomilný
(	(	kIx(	(
<g/>
sciofyty	sciofyt	k1gInPc4	sciofyt
<g/>
)	)	kIx)	)
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
nižší	nízký	k2eAgFnSc4d2	nižší
intenzitu	intenzita	k1gFnSc4	intenzita
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Obsahují	obsahovat	k5eAaImIp3nP	obsahovat
více	hodně	k6eAd2	hodně
zelených	zelený	k2eAgInPc2d1	zelený
a	a	k8xC	a
žlutých	žlutý	k2eAgInPc2d1	žlutý
pigmentů	pigment	k1gInPc2	pigment
(	(	kIx(	(
<g/>
chlorofyly	chlorofyl	k1gInPc1	chlorofyl
<g/>
,	,	kIx,	,
xanthofyly	xanthofyl	k1gInPc1	xanthofyl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
intenzivněji	intenzivně	k6eAd2	intenzivně
využívají	využívat	k5eAaPmIp3nP	využívat
modrofialové	modrofialový	k2eAgNnSc4d1	modrofialové
světlo	světlo	k1gNnSc4	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
jim	on	k3xPp3gMnPc3	on
uvedená	uvedený	k2eAgFnSc1d1	uvedená
světelná	světelný	k2eAgFnSc1d1	světelná
křivka	křivka	k1gFnSc1	křivka
<g/>
.	.	kIx.	.
</s>
<s>
Světlomilné	světlomilný	k2eAgInPc4d1	světlomilný
(	(	kIx(	(
<g/>
heliofyty	heliofyt	k1gInPc4	heliofyt
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
vysoké	vysoký	k2eAgInPc4d1	vysoký
požadavky	požadavek	k1gInPc4	požadavek
na	na	k7c4	na
intenzitu	intenzita	k1gFnSc4	intenzita
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Obsahují	obsahovat	k5eAaImIp3nP	obsahovat
méně	málo	k6eAd2	málo
karotenoidů	karotenoid	k1gInPc2	karotenoid
a	a	k8xC	a
intenzivněji	intenzivně	k6eAd2	intenzivně
využívají	využívat	k5eAaPmIp3nP	využívat
červené	červený	k2eAgNnSc4d1	červené
světlo	světlo	k1gNnSc4	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
světelná	světelný	k2eAgFnSc1d1	světelná
křivka	křivka	k1gFnSc1	křivka
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
má	mít	k5eAaImIp3nS	mít
menší	malý	k2eAgInSc4d2	menší
úhel	úhel	k1gInSc4	úhel
a	a	k8xC	a
delší	dlouhý	k2eAgInSc4d2	delší
podíl	podíl	k1gInSc4	podíl
"	"	kIx"	"
<g/>
lineární	lineární	k2eAgFnSc2d1	lineární
části	část	k1gFnSc2	část
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kompenzační	kompenzační	k2eAgInSc1d1	kompenzační
bod	bod	k1gInSc1	bod
a	a	k8xC	a
bod	bod	k1gInSc1	bod
světelného	světelný	k2eAgNnSc2d1	světelné
nasycení	nasycení	k1gNnSc2	nasycení
leží	ležet	k5eAaImIp3nS	ležet
při	při	k7c6	při
vyšších	vysoký	k2eAgFnPc6d2	vyšší
intenzitách	intenzita	k1gFnPc6	intenzita
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Vzdušný	vzdušný	k2eAgInSc1d1	vzdušný
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
dodavatelem	dodavatel	k1gMnSc7	dodavatel
CO2	CO2	k1gFnSc2	CO2
pro	pro	k7c4	pro
fotosyntézu	fotosyntéza	k1gFnSc4	fotosyntéza
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
koncentrace	koncentrace	k1gFnSc1	koncentrace
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
kolísá	kolísat	k5eAaImIp3nS	kolísat
od	od	k7c2	od
0,02	[number]	k4	0,02
<g/>
–	–	k?	–
<g/>
0,03	[number]	k4	0,03
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Nejnižší	nízký	k2eAgFnPc1d3	nejnižší
koncentrace	koncentrace	k1gFnPc1	koncentrace
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
začíná	začínat	k5eAaImIp3nS	začínat
fotosyntéza	fotosyntéza	k1gFnSc1	fotosyntéza
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
0,008	[number]	k4	0,008
<g/>
–	–	k?	–
<g/>
0,010	[number]	k4	0,010
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zvyšování	zvyšování	k1gNnSc6	zvyšování
koncentrace	koncentrace	k1gFnSc2	koncentrace
se	se	k3xPyFc4	se
rychlost	rychlost	k1gFnSc1	rychlost
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
příjem	příjem	k1gInSc1	příjem
a	a	k8xC	a
výdej	výdej	k1gInSc1	výdej
CO2	CO2	k1gFnSc2	CO2
vyrovnán	vyrovnat	k5eAaBmNgInS	vyrovnat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
kompenzační	kompenzační	k2eAgInSc1d1	kompenzační
bod	bod	k1gInSc1	bod
<g/>
.	.	kIx.	.
</s>
<s>
Fotosyntéza	fotosyntéza	k1gFnSc1	fotosyntéza
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
až	až	k9	až
do	do	k7c2	do
nasycení	nasycení	k1gNnSc2	nasycení
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
ustálí	ustálit	k5eAaPmIp3nS	ustálit
(	(	kIx(	(
<g/>
0,06	[number]	k4	0,06
<g/>
–	–	k?	–
<g/>
0,4	[number]	k4	0,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvyšováním	zvyšování	k1gNnSc7	zvyšování
za	za	k7c4	za
hranici	hranice	k1gFnSc4	hranice
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
%	%	kIx~	%
CO2	CO2	k1gFnSc2	CO2
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
fotosyntéza	fotosyntéza	k1gFnSc1	fotosyntéza
ustane	ustat	k5eAaPmIp3nS	ustat
<g/>
.	.	kIx.	.
</s>
<s>
Graf	graf	k1gInSc1	graf
čisté	čistý	k2eAgFnSc2d1	čistá
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
koncentraci	koncentrace	k1gFnSc6	koncentrace
CO2	CO2	k1gFnSc2	CO2
začíná	začínat	k5eAaImIp3nS	začínat
u	u	k7c2	u
C	C	kA	C
<g/>
3	[number]	k4	3
<g/>
-rostlin	ostlina	k1gFnPc2	-rostlina
v	v	k7c6	v
záporných	záporný	k2eAgFnPc6d1	záporná
hodnotách	hodnota	k1gFnPc6	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
výrazná	výrazný	k2eAgFnSc1d1	výrazná
fotorespirace	fotorespirace	k1gFnSc1	fotorespirace
<g/>
.	.	kIx.	.
</s>
<s>
C	C	kA	C
<g/>
4	[number]	k4	4
<g/>
-rostliny	ostlina	k1gFnPc1	-rostlina
mají	mít	k5eAaImIp3nP	mít
mechanismus	mechanismus	k1gInSc4	mechanismus
zajišťující	zajišťující	k2eAgNnSc4d1	zajišťující
koncentrování	koncentrování	k1gNnSc4	koncentrování
CO2	CO2	k1gFnSc2	CO2
(	(	kIx(	(
<g/>
Hatch-Slackův	Hatch-Slackův	k2eAgInSc1d1	Hatch-Slackův
cyklus	cyklus	k1gInSc1	cyklus
<g/>
)	)	kIx)	)
a	a	k8xC	a
fotorespirace	fotorespirace	k1gFnSc1	fotorespirace
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
téměř	téměř	k6eAd1	téměř
potlačena	potlačit	k5eAaPmNgNnP	potlačit
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
také	také	k9	také
dosáhnou	dosáhnout	k5eAaPmIp3nP	dosáhnout
bodu	bod	k1gInSc3	bod
nasycení	nasycení	k1gNnSc2	nasycení
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Průběh	průběh	k1gInSc1	průběh
reakcí	reakce	k1gFnPc2	reakce
obecně	obecně	k6eAd1	obecně
urychluje	urychlovat	k5eAaImIp3nS	urychlovat
zvýšení	zvýšení	k1gNnSc4	zvýšení
teploty	teplota	k1gFnSc2	teplota
o	o	k7c4	o
10	[number]	k4	10
°	°	k?	°
<g/>
C	C	kA	C
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
krát	krát	k6eAd1	krát
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
exponenciálně	exponenciálně	k6eAd1	exponenciálně
a	a	k8xC	a
limitujícím	limitující	k2eAgInSc7d1	limitující
faktorem	faktor	k1gInSc7	faktor
je	být	k5eAaImIp3nS	být
intenzita	intenzita	k1gFnSc1	intenzita
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
teplota	teplota	k1gFnSc1	teplota
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
i	i	k9	i
další	další	k2eAgInPc4d1	další
fyziologické	fyziologický	k2eAgInPc4d1	fyziologický
pochody	pochod	k1gInPc4	pochod
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
závislost	závislost	k1gFnSc4	závislost
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
složitá	složitý	k2eAgFnSc1d1	složitá
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nízkých	nízký	k2eAgFnPc6d1	nízká
teplotách	teplota	k1gFnPc6	teplota
rostliny	rostlina	k1gFnSc2	rostlina
fotosyntetizují	fotosyntetizovat	k5eAaPmIp3nP	fotosyntetizovat
pomalu	pomalu	k6eAd1	pomalu
<g/>
.	.	kIx.	.
</s>
<s>
Zvyšováním	zvyšování	k1gNnSc7	zvyšování
teploty	teplota	k1gFnSc2	teplota
rychlost	rychlost	k1gFnSc1	rychlost
roste	růst	k5eAaImIp3nS	růst
až	až	k9	až
po	po	k7c6	po
hranici	hranice	k1gFnSc6	hranice
teplotního	teplotní	k2eAgNnSc2d1	teplotní
optima	optimum	k1gNnSc2	optimum
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
pomalu	pomalu	k6eAd1	pomalu
klesá	klesat	k5eAaImIp3nS	klesat
a	a	k8xC	a
při	při	k7c6	při
35	[number]	k4	35
<g/>
–	–	k?	–
<g/>
45	[number]	k4	45
°	°	k?	°
<g/>
C	C	kA	C
se	se	k3xPyFc4	se
zastavuje	zastavovat	k5eAaImIp3nS	zastavovat
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
s	s	k7c7	s
fotosyntézou	fotosyntéza	k1gFnSc7	fotosyntéza
se	se	k3xPyFc4	se
však	však	k9	však
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
i	i	k9	i
dýchání	dýchání	k1gNnSc1	dýchání
a	a	k8xC	a
obě	dva	k4xCgFnPc1	dva
rychlosti	rychlost	k1gFnPc1	rychlost
se	se	k3xPyFc4	se
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
nerovnoměrně	rovnoměrně	k6eNd1	rovnoměrně
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
teploty	teplota	k1gFnSc2	teplota
na	na	k7c4	na
fotosyntézu	fotosyntéza	k1gFnSc4	fotosyntéza
se	se	k3xPyFc4	se
výrazněji	výrazně	k6eAd2	výrazně
projevuje	projevovat	k5eAaImIp3nS	projevovat
při	při	k7c6	při
vyšší	vysoký	k2eAgFnSc6d2	vyšší
intenzitě	intenzita	k1gFnSc6	intenzita
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Různý	různý	k2eAgInSc1d1	různý
vliv	vliv	k1gInSc1	vliv
má	mít	k5eAaImIp3nS	mít
teplota	teplota	k1gFnSc1	teplota
u	u	k7c2	u
C3	C3	k1gFnSc2	C3
a	a	k8xC	a
C	C	kA	C
<g/>
4	[number]	k4	4
<g/>
-rostlin	ostlina	k1gFnPc2	-rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Evolucí	evoluce	k1gFnSc7	evoluce
rostlin	rostlina	k1gFnPc2	rostlina
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
adaptaci	adaptace	k1gFnSc3	adaptace
na	na	k7c4	na
různé	různý	k2eAgFnPc4d1	různá
teplotní	teplotní	k2eAgFnPc4d1	teplotní
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Působnost	působnost	k1gFnSc1	působnost
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
přímá	přímý	k2eAgFnSc1d1	přímá
i	i	k8xC	i
nepřímá	přímý	k2eNgFnSc1d1	nepřímá
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
závislost	závislost	k1gFnSc4	závislost
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
na	na	k7c6	na
vodě	voda	k1gFnSc6	voda
složitá	složitý	k2eAgFnSc1d1	složitá
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
donor	donor	k1gInSc1	donor
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
hydratuje	hydratovat	k5eAaBmIp3nS	hydratovat
asimilační	asimilační	k2eAgNnPc4d1	asimilační
pletiva	pletivo	k1gNnPc4	pletivo
<g/>
,	,	kIx,	,
ovládá	ovládat	k5eAaImIp3nS	ovládat
regulaci	regulace	k1gFnSc4	regulace
velikosti	velikost	k1gFnSc2	velikost
štěrbiny	štěrbina	k1gFnSc2	štěrbina
průduchů	průduch	k1gInPc2	průduch
a	a	k8xC	a
transpiraci	transpirace	k1gFnSc4	transpirace
<g/>
,	,	kIx,	,
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
růst	růst	k1gInSc1	růst
asimilační	asimilační	k2eAgFnSc2d1	asimilační
plochy	plocha	k1gFnSc2	plocha
<g/>
,	,	kIx,	,
přivádí	přivádět	k5eAaImIp3nS	přivádět
ionty	ion	k1gInPc4	ion
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
rozvádí	rozvádět	k5eAaImIp3nS	rozvádět
asimiláty	asimilát	k1gInPc4	asimilát
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
rychlost	rychlost	k1gFnSc1	rychlost
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
vodním	vodní	k2eAgInSc6d1	vodní
deficitu	deficit	k1gInSc6	deficit
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
%	%	kIx~	%
plného	plný	k2eAgNnSc2d1	plné
nasycení	nasycení	k1gNnSc2	nasycení
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
deficitu	deficit	k1gInSc2	deficit
40	[number]	k4	40
<g/>
–	–	k?	–
<g/>
60	[number]	k4	60
%	%	kIx~	%
se	se	k3xPyFc4	se
rychlost	rychlost	k1gFnSc1	rychlost
výrazně	výrazně	k6eAd1	výrazně
snižuje	snižovat	k5eAaImIp3nS	snižovat
a	a	k8xC	a
klesá	klesat	k5eAaImIp3nS	klesat
k	k	k7c3	k
nule	nula	k1gFnSc3	nula
<g/>
.	.	kIx.	.
</s>
<s>
Snížení	snížení	k1gNnSc1	snížení
množství	množství	k1gNnSc1	množství
vody	voda	k1gFnSc2	voda
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
zavírání	zavírání	k1gNnSc3	zavírání
průduchů	průduch	k1gInPc2	průduch
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
k	k	k7c3	k
menšímu	malý	k2eAgInSc3d2	menší
příjmu	příjem	k1gInSc3	příjem
CO	co	k9	co
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatek	nedostatek	k1gInSc1	nedostatek
vody	voda	k1gFnSc2	voda
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
i	i	k9	i
složení	složení	k1gNnSc1	složení
produktů	produkt	k1gInPc2	produkt
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
<g/>
.	.	kIx.	.
</s>
<s>
Převládají	převládat	k5eAaImIp3nP	převládat
spíše	spíše	k9	spíše
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
látky	látka	k1gFnPc1	látka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
osmoticky	osmoticky	k6eAd1	osmoticky
aktivní	aktivní	k2eAgFnSc4d1	aktivní
látky	látka	k1gFnPc4	látka
jako	jako	k8xS	jako
sacharidy	sacharid	k1gInPc4	sacharid
a	a	k8xC	a
aminokyseliny	aminokyselina	k1gFnPc4	aminokyselina
<g/>
)	)	kIx)	)
a	a	k8xC	a
tvorba	tvorba	k1gFnSc1	tvorba
makromolekulárních	makromolekulární	k2eAgFnPc2d1	makromolekulární
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
snížena	snížit	k5eAaPmNgFnS	snížit
<g/>
.	.	kIx.	.
</s>
<s>
Primitivní	primitivní	k2eAgInPc1d1	primitivní
fotosyntetické	fotosyntetický	k2eAgInPc1d1	fotosyntetický
mikroorganismy	mikroorganismus	k1gInPc1	mikroorganismus
(	(	kIx(	(
<g/>
např.	např.	kA	např.
zelené	zelená	k1gFnSc2	zelená
sirné	sirný	k2eAgFnSc2d1	sirná
bakterie	bakterie	k1gFnSc2	bakterie
<g/>
)	)	kIx)	)
používají	používat	k5eAaImIp3nP	používat
jako	jako	k8xS	jako
donor	donor	k1gInSc4	donor
elektronů	elektron	k1gInPc2	elektron
místo	místo	k1gNnSc4	místo
vody	voda	k1gFnSc2	voda
sulfan	sulfan	k1gMnSc1	sulfan
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
železité	železitý	k2eAgInPc4d1	železitý
ionty	ion	k1gInPc4	ion
nebo	nebo	k8xC	nebo
organické	organický	k2eAgFnSc2d1	organická
kyseliny	kyselina	k1gFnSc2	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
reakci	reakce	k1gFnSc6	reakce
nedochází	docházet	k5eNaImIp3nS	docházet
k	k	k7c3	k
uvolňování	uvolňování	k1gNnSc3	uvolňování
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
anoxygenní	anoxygenní	k2eAgFnSc1d1	anoxygenní
fotosyntéza	fotosyntéza	k1gFnSc1	fotosyntéza
<g/>
.	.	kIx.	.
</s>
<s>
Anoxygenní	Anoxygenní	k2eAgFnSc1d1	Anoxygenní
fotosyntéza	fotosyntéza	k1gFnSc1	fotosyntéza
má	mít	k5eAaImIp3nS	mít
oproti	oproti	k7c3	oproti
oxygenní	oxygenní	k2eAgFnSc3d1	oxygenní
fotosyntéze	fotosyntéza	k1gFnSc3	fotosyntéza
mnohem	mnohem	k6eAd1	mnohem
nižší	nízký	k2eAgFnSc4d2	nižší
účinnost	účinnost	k1gFnSc4	účinnost
využití	využití	k1gNnSc2	využití
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
Δ	Δ	k?	Δ
=	=	kIx~	=
357	[number]	k4	357
kJ	kJ	k?	kJ
<g/>
/	/	kIx~	/
<g/>
mol	mol	k1gMnSc1	mol
(	(	kIx(	(
<g/>
hodnota	hodnota	k1gFnSc1	hodnota
vztažená	vztažený	k2eAgFnSc1d1	vztažená
na	na	k7c6	na
fixaci	fixace	k1gFnSc6	fixace
1	[number]	k4	1
molu	mol	k1gInSc2	mol
CO	co	k6eAd1	co
<g/>
2	[number]	k4	2
<g/>
;	;	kIx,	;
v	v	k7c6	v
případě	případ	k1gInSc6	případ
oxygenní	oxygenní	k2eAgFnSc2d1	oxygenní
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
475	[number]	k4	475
kJ	kJ	k?	kJ
<g/>
/	/	kIx~	/
<g/>
mol	mol	k1gMnSc1	mol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
rovnice	rovnice	k1gFnSc1	rovnice
anoxygenní	anoxygenní	k2eAgFnSc2d1	anoxygenní
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
využívající	využívající	k2eAgInSc1d1	využívající
sirovodík	sirovodík	k1gInSc1	sirovodík
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
(	(	kIx(	(
C	C	kA	C
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c4	o
)	)	kIx)	)
+	+	kIx~	+
2	[number]	k4	2
:	:	kIx,	:
S	s	k7c7	s
+	+	kIx~	+
:	:	kIx,	:
H	H	kA	H
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c4	o
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gMnSc1	mathsf
{	{	kIx(	{
<g/>
CO_	CO_	k1gMnSc1	CO_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
H_	H_	k1gMnSc1	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
S	s	k7c7	s
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
(	(	kIx(	(
<g/>
CH_	CH_	k1gMnSc1	CH_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
O	o	k7c4	o
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
S	s	k7c7	s
<g/>
+	+	kIx~	+
<g/>
H_	H_	k1gMnSc1	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
O	o	k7c4	o
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Obecnou	obecný	k2eAgFnSc4d1	obecná
rovnici	rovnice	k1gFnSc4	rovnice
anoxygenní	anoxygenní	k2eAgFnSc2d1	anoxygenní
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
viz	vidět	k5eAaImRp2nS	vidět
v	v	k7c6	v
části	část	k1gFnPc4	část
Původ	původ	k1gInSc4	původ
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Fotosyntéza	fotosyntéza	k1gFnSc1	fotosyntéza
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
existence	existence	k1gFnSc2	existence
současného	současný	k2eAgInSc2d1	současný
života	život	k1gInSc2	život
pokládána	pokládat	k5eAaImNgFnS	pokládat
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
biochemických	biochemický	k2eAgInPc2d1	biochemický
procesů	proces	k1gInPc2	proces
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
autorů	autor	k1gMnPc2	autor
přímo	přímo	k6eAd1	přímo
nejdůležitější	důležitý	k2eAgMnSc1d3	nejdůležitější
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
ní	on	k3xPp3gFnSc2	on
by	by	kYmCp3nS	by
biosféra	biosféra	k1gFnSc1	biosféra
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
podobě	podoba	k1gFnSc6	podoba
nebyla	být	k5eNaImAgFnS	být
zásobena	zásobit	k5eAaPmNgFnS	zásobit
organickými	organický	k2eAgFnPc7d1	organická
látkami	látka	k1gFnPc7	látka
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
omezené	omezený	k2eAgFnSc3d1	omezená
míře	míra	k1gFnSc3	míra
(	(	kIx(	(
<g/>
chemoautotrofními	chemoautotrofní	k2eAgFnPc7d1	chemoautotrofní
bakteriemi	bakterie	k1gFnPc7	bakterie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Organické	organický	k2eAgFnPc1d1	organická
látky	látka	k1gFnPc1	látka
vytvářené	vytvářený	k2eAgFnPc1d1	vytvářená
při	při	k7c6	při
fotosyntéze	fotosyntéza	k1gFnSc6	fotosyntéza
spotřebovávají	spotřebovávat	k5eAaImIp3nP	spotřebovávat
heterotrofní	heterotrofní	k2eAgInPc1d1	heterotrofní
organismy	organismus	k1gInPc1	organismus
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yQgFnPc4	který
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
výživě	výživa	k1gFnSc6	výživa
<g/>
.	.	kIx.	.
</s>
<s>
Fototrofní	Fototrofní	k2eAgInPc1d1	Fototrofní
organismy	organismus	k1gInPc1	organismus
zachytávají	zachytávat	k5eAaImIp3nP	zachytávat
globálně	globálně	k6eAd1	globálně
asi	asi	k9	asi
130	[number]	k4	130
TW	TW	kA	TW
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
ročně	ročně	k6eAd1	ročně
tedy	tedy	k9	tedy
zhruba	zhruba	k6eAd1	zhruba
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
1021	[number]	k4	1021
J	J	kA	J
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
tři	tři	k4xCgFnPc4	tři
čtvrtě	čtvrt	k1gFnPc4	čtvrt
promile	promile	k1gNnPc2	promile
ze	z	k7c2	z
sluneční	sluneční	k2eAgFnSc2d1	sluneční
energie	energie	k1gFnSc2	energie
dopadající	dopadající	k2eAgMnSc1d1	dopadající
ze	z	k7c2	z
Slunce	slunce	k1gNnSc2	slunce
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
biomasy	biomasa	k1gFnSc2	biomasa
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
zakomponuje	zakomponovat	k5eAaPmIp3nS	zakomponovat
asi	asi	k9	asi
1,00	[number]	k4	1,00
<g/>
-	-	kIx~	-
<g/>
1,15	[number]	k4	1,15
<g/>
×	×	k?	×
<g/>
1011	[number]	k4	1011
tun	tuna	k1gFnPc2	tuna
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
3,7	[number]	k4	3,7
<g/>
-	-	kIx~	-
<g/>
4,2	[number]	k4	4,2
<g/>
×	×	k?	×
<g/>
1011	[number]	k4	1011
tun	tuna	k1gFnPc2	tuna
fixovaného	fixovaný	k2eAgNnSc2d1	fixované
CO	co	k8xS	co
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
a	a	k8xC	a
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
2,7	[number]	k4	2,7
<g/>
-	-	kIx~	-
<g/>
3,1	[number]	k4	3,1
<g/>
×	×	k?	×
<g/>
1011	[number]	k4	1011
tun	tuna	k1gFnPc2	tuna
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
produktech	produkt	k1gInPc6	produkt
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
je	být	k5eAaImIp3nS	být
závislý	závislý	k2eAgInSc1d1	závislý
i	i	k8xC	i
dnešní	dnešní	k2eAgInSc1d1	dnešní
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
uhlí	uhlí	k1gNnSc1	uhlí
<g/>
,	,	kIx,	,
ropa	ropa	k1gFnSc1	ropa
a	a	k8xC	a
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
fosilní	fosilní	k2eAgNnPc1d1	fosilní
paliva	palivo	k1gNnPc1	palivo
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
zbytky	zbytek	k1gInPc4	zbytek
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
žily	žít	k5eAaImAgFnP	žít
v	v	k7c6	v
dávné	dávný	k2eAgFnSc6d1	dávná
minulosti	minulost	k1gFnSc6	minulost
a	a	k8xC	a
bez	bez	k7c2	bez
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
by	by	kYmCp3nP	by
nevznikly	vzniknout	k5eNaPmAgInP	vzniknout
<g/>
.	.	kIx.	.
</s>
<s>
Procesem	proces	k1gInSc7	proces
s	s	k7c7	s
opačným	opačný	k2eAgInSc7d1	opačný
průběhem	průběh	k1gInSc7	průběh
než	než	k8xS	než
oxygenní	oxygenní	k2eAgFnSc1d1	oxygenní
fotosyntéza	fotosyntéza	k1gFnSc1	fotosyntéza
je	být	k5eAaImIp3nS	být
buněčné	buněčný	k2eAgNnSc4d1	buněčné
dýchání	dýchání	k1gNnSc4	dýchání
(	(	kIx(	(
<g/>
respirace	respirace	k1gFnSc1	respirace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
fotosyntézou	fotosyntéza	k1gFnSc7	fotosyntéza
a	a	k8xC	a
následnými	následný	k2eAgInPc7d1	následný
procesy	proces	k1gInPc7	proces
energie	energie	k1gFnSc2	energie
nahromaděná	nahromaděný	k2eAgFnSc1d1	nahromaděná
do	do	k7c2	do
chemických	chemický	k2eAgFnPc2d1	chemická
látek	látka	k1gFnPc2	látka
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
oxidací	oxidace	k1gFnPc2	oxidace
za	za	k7c2	za
spotřeby	spotřeba	k1gFnSc2	spotřeba
vzdušného	vzdušný	k2eAgInSc2d1	vzdušný
kyslíku	kyslík	k1gInSc2	kyslík
(	(	kIx(	(
<g/>
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
a	a	k8xC	a
přeměňuje	přeměňovat	k5eAaImIp3nS	přeměňovat
se	se	k3xPyFc4	se
na	na	k7c4	na
adenosintrifosfát	adenosintrifosfát	k1gInSc4	adenosintrifosfát
(	(	kIx(	(
<g/>
ATP	atp	kA	atp
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
ATP	atp	kA	atp
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
energie	energie	k1gFnSc1	energie
přímo	přímo	k6eAd1	přímo
dostupná	dostupný	k2eAgFnSc1d1	dostupná
pro	pro	k7c4	pro
další	další	k2eAgNnSc4d1	další
potřebné	potřebné	k1gNnSc4	potřebné
reakce	reakce	k1gFnSc2	reakce
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
