<s>
Švýcarská	švýcarský	k2eAgFnSc1d1
garda	garda	k1gFnSc1
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc1
pro	pro	k7c4
švýcarské	švýcarský	k2eAgMnPc4d1
žoldnéře	žoldnéř	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
v	v	k7c6
minulosti	minulost	k1gFnSc6
sloužili	sloužit	k5eAaImAgMnP
v	v	k7c6
armádách	armáda	k1gFnPc6
mnoha	mnoho	k4c2
evropských	evropský	k2eAgMnPc2d1
panovníků	panovník	k1gMnPc2
<g/>
,	,	kIx,
fungovali	fungovat	k5eAaImAgMnP
jako	jako	k9
osobní	osobní	k2eAgMnPc1d1
strážci	strážce	k1gMnPc1
či	či	k8xC
střežili	střežit	k5eAaImAgMnP
panovnická	panovnický	k2eAgNnPc4d1
sídla	sídlo	k1gNnPc4
<g/>
.	.	kIx.
</s>