<s>
Švýcarská	švýcarský	k2eAgFnSc1d1
garda	garda	k1gFnSc1
</s>
<s>
Švýcarská	švýcarský	k2eAgFnSc1d1
garda	garda	k1gFnSc1
Tradiční	tradiční	k2eAgFnSc2d1
uniformy	uniforma	k1gFnSc2
papežské	papežský	k2eAgFnSc2d1
švýcarské	švýcarský	k2eAgFnSc2d1
gardy	garda	k1gFnSc2
Tradiční	tradiční	k2eAgFnSc2d1
uniformy	uniforma	k1gFnSc2
papežské	papežský	k2eAgFnSc2d1
švýcarské	švýcarský	k2eAgFnSc2d1
gardy	garda	k1gFnSc2
Vznik	vznik	k1gInSc1
</s>
<s>
1506	#num#	k4
Velikost	velikost	k1gFnSc1
</s>
<s>
135	#num#	k4
mužů	muž	k1gMnPc2
Motto	motto	k1gNnSc4
</s>
<s>
Acriter	Acriter	k1gMnSc1
et	et	k?
Fideliter	Fideliter	k1gMnSc1
</s>
<s>
Švýcarská	švýcarský	k2eAgFnSc1d1
garda	garda	k1gFnSc1
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc1
pro	pro	k7c4
švýcarské	švýcarský	k2eAgMnPc4d1
žoldnéře	žoldnéř	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
v	v	k7c6
minulosti	minulost	k1gFnSc6
sloužili	sloužit	k5eAaImAgMnP
v	v	k7c6
armádách	armáda	k1gFnPc6
mnoha	mnoho	k4c2
evropských	evropský	k2eAgMnPc2d1
panovníků	panovník	k1gMnPc2
<g/>
,	,	kIx,
fungovali	fungovat	k5eAaImAgMnP
jako	jako	k9
osobní	osobní	k2eAgMnPc1d1
strážci	strážce	k1gMnPc1
či	či	k8xC
střežili	střežit	k5eAaImAgMnP
panovnická	panovnický	k2eAgNnPc4d1
sídla	sídlo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
existuje	existovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
papežská	papežský	k2eAgFnSc1d1
švýcarská	švýcarský	k2eAgFnSc1d1
garda	garda	k1gFnSc1
<g/>
,	,	kIx,
zastávající	zastávající	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
vatikánské	vatikánský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1
Švýcaři	Švýcar	k1gMnPc1
se	se	k3xPyFc4
od	od	k7c2
starověku	starověk	k1gInSc2
často	často	k6eAd1
nechávali	nechávat	k5eAaImAgMnP
kvůli	kvůli	k7c3
chudobě	chudoba	k1gFnSc3
najímat	najímat	k5eAaImF
do	do	k7c2
cizích	cizí	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
si	se	k3xPyFc3
ve	v	k7c6
středověké	středověký	k2eAgFnSc6d1
přelidněné	přelidněný	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byla	být	k5eAaImAgFnS
nouze	nouze	k1gFnSc1
o	o	k7c4
práci	práce	k1gFnSc4
<g/>
,	,	kIx,
zajistili	zajistit	k5eAaPmAgMnP
obživu	obživa	k1gFnSc4
na	na	k7c4
zimu	zima	k1gFnSc4
<g/>
,	,	kIx,
dávalo	dávat	k5eAaImAgNnS
se	se	k3xPyFc4
na	na	k7c4
15	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
přes	přes	k7c4
léto	léto	k1gNnSc4
verbovat	verbovat	k5eAaImF
do	do	k7c2
válek	válka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
brzy	brzy	k6eAd1
si	se	k3xPyFc3
získali	získat	k5eAaPmAgMnP
skvělou	skvělý	k2eAgFnSc4d1
reputaci	reputace	k1gFnSc4
jako	jako	k8xS,k8xC
odvážní	odvážný	k2eAgMnPc1d1
a	a	k8xC
věrní	věrný	k2eAgMnPc1d1
válečníci	válečník	k1gMnPc1
a	a	k8xC
prosadili	prosadit	k5eAaPmAgMnP
se	se	k3xPyFc4
v	v	k7c6
mnoha	mnoho	k4c6
armádách	armáda	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Švýcarská	švýcarský	k2eAgFnSc1d1
garda	garda	k1gFnSc1
ve	v	k7c6
Francii	Francie	k1gFnSc6
</s>
<s>
Z	z	k7c2
již	již	k6eAd1
zaniklých	zaniklý	k2eAgFnPc2d1
švýcarských	švýcarský	k2eAgFnPc2d1
gard	garda	k1gFnPc2
je	být	k5eAaImIp3nS
nejznámější	známý	k2eAgFnSc1d3
švýcarská	švýcarský	k2eAgFnSc1d1
garda	garda	k1gFnSc1
ve	v	k7c6
službách	služba	k1gFnPc6
francouzského	francouzský	k2eAgMnSc2d1
krále	král	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smlouvu	smlouva	k1gFnSc4
s	s	k7c7
nimi	on	k3xPp3gMnPc7
uzavřel	uzavřít	k5eAaPmAgMnS
již	již	k9
v	v	k7c6
roce	rok	k1gInSc6
1453	#num#	k4
Karel	Karel	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
a	a	k8xC
obnovil	obnovit	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
roku	rok	k1gInSc2
1474	#num#	k4
Ludvík	Ludvík	k1gMnSc1
XI	XI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
najal	najmout	k5eAaPmAgMnS
švýcarské	švýcarský	k2eAgMnPc4d1
vojáky	voják	k1gMnPc4
jako	jako	k8xC,k8xS
instruktory	instruktor	k1gMnPc4
pro	pro	k7c4
svoji	svůj	k3xOyFgFnSc4
armádu	armáda	k1gFnSc4
a	a	k8xC
jejich	jejich	k3xOp3gInPc1
sbory	sbor	k1gInPc1
se	se	k3xPyFc4
brzy	brzy	k6eAd1
staly	stát	k5eAaPmAgFnP
významnou	významný	k2eAgFnSc7d1
oporou	opora	k1gFnSc7
Francouzů	Francouz	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prestiž	prestiž	k1gFnSc1
švýcarské	švýcarský	k2eAgFnSc2d1
gardy	garda	k1gFnSc2
stoupala	stoupat	k5eAaImAgFnS
zejména	zejména	k9
v	v	k7c6
17	#num#	k4
<g/>
.	.	kIx.
–	–	k?
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejslavnějším	slavný	k2eAgInSc7d3
činem	čin	k1gInSc7
je	být	k5eAaImIp3nS
obrana	obrana	k1gFnSc1
Tuilerijského	tuilerijský	k2eAgInSc2d1
paláce	palác	k1gInSc2
v	v	k7c6
Paříži	Paříž	k1gFnSc6
během	během	k7c2
francouzské	francouzský	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1789	#num#	k4
byla	být	k5eAaImAgFnS
garda	garda	k1gFnSc1
francouzskými	francouzský	k2eAgNnPc7d1
revolucionáři	revolucionář	k1gMnPc7
rozpuštěna	rozpuštěn	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
10	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1792	#num#	k4
bránilo	bránit	k5eAaImAgNnS
palác	palác	k1gInSc4
900	#num#	k4
gardistů	gardista	k1gMnPc2
<g/>
,	,	kIx,
věrných	věrný	k2eAgFnPc2d1
králi	král	k1gMnPc7
a	a	k8xC
na	na	k7c4
600	#num#	k4
jich	on	k3xPp3gMnPc2
bylo	být	k5eAaImAgNnS
pobito	pobít	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalších	další	k2eAgMnPc2d1
asi	asi	k9
160	#num#	k4
členů	člen	k1gMnPc2
gardy	garda	k1gFnSc2
zemřelo	zemřít	k5eAaPmAgNnS
později	pozdě	k6eAd2
na	na	k7c4
následky	následek	k1gInPc4
zranění	zranění	k1gNnSc2
ve	v	k7c6
vězení	vězení	k1gNnSc6
nebo	nebo	k8xC
pod	pod	k7c7
gilotinou	gilotina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
několika	několik	k4yIc2
desítek	desítka	k1gFnPc2
přeživších	přeživší	k2eAgFnPc2d1
zůstalo	zůstat	k5eAaPmAgNnS
ze	z	k7c2
švýcarské	švýcarský	k2eAgFnSc2d1
gardy	garda	k1gFnSc2
jen	jen	k9
300	#num#	k4
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
byli	být	k5eAaImAgMnP
několik	několik	k4yIc4
dní	den	k1gInPc2
před	před	k7c7
masakrem	masakr	k1gInSc7
v	v	k7c6
Tuilerijském	tuilerijský	k2eAgInSc6d1
paláci	palác	k1gInSc6
vysláni	vyslat	k5eAaPmNgMnP
do	do	k7c2
Normandie	Normandie	k1gFnSc2
na	na	k7c4
obranu	obrana	k1gFnSc4
zásobovacích	zásobovací	k2eAgInPc2d1
vozů	vůz	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
Napoleon	Napoleon	k1gMnSc1
obnovil	obnovit	k5eAaPmAgMnS
monarchii	monarchie	k1gFnSc4
<g/>
,	,	kIx,
znovu	znovu	k6eAd1
zavedl	zavést	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1814	#num#	k4
také	také	k6eAd1
jednotku	jednotka	k1gFnSc4
švýcarské	švýcarský	k2eAgFnSc2d1
gardy	garda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pěší	pěší	k2eAgInPc1d1
pluky	pluk	k1gInPc1
bojovaly	bojovat	k5eAaImAgInP
v	v	k7c6
Napoleonově	Napoleonův	k2eAgFnSc6d1
armádě	armáda	k1gFnSc6
po	po	k7c6
celé	celý	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1817	#num#	k4
byla	být	k5eAaImAgFnS
garda	garda	k1gFnSc1
definitivně	definitivně	k6eAd1
rozpuštěna	rozpustit	k5eAaPmNgFnS
a	a	k8xC
nahrazena	nahradit	k5eAaPmNgFnS
novou	nový	k2eAgFnSc7d1
gardou	garda	k1gFnSc7
<g/>
,	,	kIx,
tvořenou	tvořený	k2eAgFnSc7d1
rodilými	rodilý	k2eAgMnPc7d1
Francouzi	Francouz	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1832	#num#	k4
byli	být	k5eAaImAgMnP
veteráni	veterán	k1gMnPc1
ze	z	k7c2
švýcarské	švýcarský	k2eAgFnSc2d1
gardy	garda	k1gFnSc2
spolu	spolu	k6eAd1
s	s	k7c7
dalšími	další	k2eAgMnPc7d1
cizinci	cizinec	k1gMnPc7
převeleni	převelet	k5eAaPmNgMnP
do	do	k7c2
francouzské	francouzský	k2eAgFnSc2d1
cizinecké	cizinecký	k2eAgFnSc2d1
legie	legie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ostatní	ostatní	k1gNnSc1
historické	historický	k2eAgFnSc2d1
švýcarské	švýcarský	k2eAgFnSc2d1
gardy	garda	k1gFnSc2
</s>
<s>
Další	další	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
švýcarské	švýcarský	k2eAgFnSc2d1
gardy	garda	k1gFnSc2
fungovaly	fungovat	k5eAaImAgFnP
zejména	zejména	k9
v	v	k7c6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
na	na	k7c6
panovnických	panovnický	k2eAgInPc6d1
dvorech	dvůr	k1gInPc6
po	po	k7c6
celé	celý	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
-	-	kIx~
v	v	k7c6
Savojsku	Savojsko	k1gNnSc6
a	a	k8xC
Sardinském	sardinský	k2eAgNnSc6d1
království	království	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
Prusku	Prusko	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
Saském	saský	k2eAgNnSc6d1
království	království	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
Neapolském	neapolský	k2eAgNnSc6d1
království	království	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
Nizozemí	Nizozemí	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
Toskánském	toskánský	k2eAgNnSc6d1
velkovévodství	velkovévodství	k1gNnSc6
i	i	k8xC
v	v	k7c6
Rakousku	Rakousko	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
střežila	střežit	k5eAaImAgFnS
sídlo	sídlo	k1gNnSc4
Habsburků	Habsburk	k1gMnPc2
<g/>
,	,	kIx,
Hofburg	Hofburg	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Papežská	papežský	k2eAgFnSc1d1
švýcarská	švýcarský	k2eAgFnSc1d1
garda	garda	k1gFnSc1
</s>
<s>
Slavnostní	slavnostní	k2eAgFnSc1d1
přísaha	přísaha	k1gFnSc1
gardistů	gardista	k1gMnPc2
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
</s>
<s>
Papežská	papežský	k2eAgFnSc1d1
švýcarská	švýcarský	k2eAgFnSc1d1
garda	garda	k1gFnSc1
(	(	kIx(
<g/>
něm.	něm.	k?
Schweizergarde	Schweizergard	k1gMnSc5
<g/>
,	,	kIx,
it.	it.	k?
Guardia	Guardia	k?
Svizzera	Svizzer	k1gMnSc2
Pontificia	Pontificius	k1gMnSc2
<g/>
,	,	kIx,
lat.	lat.	k?
Pontificia	Pontificium	k1gNnSc2
Cohors	Cohorsa	k1gFnPc2
Helvetica	Helvetic	k1gInSc2
nebo	nebo	k8xC
Cohors	Cohorsa	k1gFnPc2
Pedestris	Pedestris	k1gFnPc2
Helvetiorum	Helvetiorum	k1gInSc1
a	a	k8xC
Sacra	Sacra	k1gFnSc1
Custodia	Custodium	k1gNnSc2
Pontifici	Pontifik	k1gMnPc1
<g/>
)	)	kIx)
oficiálně	oficiálně	k6eAd1
vznikla	vzniknout	k5eAaPmAgFnS
roku	rok	k1gInSc2
1506	#num#	k4
a	a	k8xC
jako	jako	k9
jediná	jediný	k2eAgFnSc1d1
funguje	fungovat	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
Sixtus	Sixtus	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1471	#num#	k4
<g/>
–	–	k?
<g/>
1484	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
spojil	spojit	k5eAaPmAgMnS
s	s	k7c7
několika	několik	k4yIc7
kantony	kanton	k1gInPc7
švýcarské	švýcarský	k2eAgFnSc2d1
konfederace	konfederace	k1gFnSc2
<g/>
,	,	kIx,
požádal	požádat	k5eAaPmAgMnS
o	o	k7c4
švýcarské	švýcarský	k2eAgMnPc4d1
vojáky	voják	k1gMnPc4
a	a	k8xC
nechal	nechat	k5eAaPmAgMnS
postavit	postavit	k5eAaPmF
kasárna	kasárna	k1gNnPc4
na	na	k7c4
Via	via	k7c4
Pellegrino	Pellegrino	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úmluvu	úmluva	k1gFnSc4
obnovil	obnovit	k5eAaPmAgMnS
i	i	k9
jeho	jeho	k3xOp3gMnSc1
nástupce	nástupce	k1gMnSc1
Inocenc	Inocenc	k1gMnSc1
VIII	VIII	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1484	#num#	k4
<g/>
–	–	k?
<g/>
1492	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
chtěl	chtít	k5eAaImAgMnS
gardisty	gardista	k1gMnPc4
využít	využít	k5eAaPmF
v	v	k7c6
boji	boj	k1gInSc6
proti	proti	k7c3
milánskému	milánský	k2eAgMnSc3d1
vévodovi	vévoda	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významný	významný	k2eAgInSc4d1
pro	pro	k7c4
dějiny	dějiny	k1gFnPc4
gardy	garda	k1gFnSc2
je	být	k5eAaImIp3nS
zejména	zejména	k9
papež	papež	k1gMnSc1
Julius	Julius	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1503	#num#	k4
<g/>
–	–	k?
<g/>
1513	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
požádal	požádat	k5eAaPmAgMnS
o	o	k7c6
poskytnutí	poskytnutí	k1gNnSc6
stálé	stálý	k2eAgFnSc2d1
dvousetčlenné	dvousetčlenný	k2eAgFnSc2d1
posádky	posádka	k1gFnSc2
švýcarských	švýcarský	k2eAgMnPc2d1
gardistů	gardista	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvních	první	k4xOgInPc2
150	#num#	k4
mužů	muž	k1gMnPc2
dorazilo	dorazit	k5eAaPmAgNnS
do	do	k7c2
Říma	Řím	k1gInSc2
22	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1506	#num#	k4
po	po	k7c6
tříměsíčním	tříměsíční	k2eAgInSc6d1
pochodu	pochod	k1gInSc6
a	a	k8xC
toto	tento	k3xDgNnSc1
datum	datum	k1gNnSc1
je	být	k5eAaImIp3nS
pokládáno	pokládat	k5eAaImNgNnS
za	za	k7c4
oficiální	oficiální	k2eAgNnSc4d1
datum	datum	k1gNnSc4
vzniku	vznik	k1gInSc2
papežské	papežský	k2eAgFnSc2d1
gardy	garda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Prvním	první	k4xOgInSc7
a	a	k8xC
nejznámějším	známý	k2eAgInSc7d3
činem	čin	k1gInSc7
švýcarské	švýcarský	k2eAgFnSc2d1
gardy	garda	k1gFnSc2
byla	být	k5eAaImAgFnS
obrana	obrana	k1gFnSc1
Říma	Řím	k1gInSc2
6	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1527	#num#	k4
proti	proti	k7c3
vojsku	vojsko	k1gNnSc3
Karla	Karel	k1gMnSc2
V.	V.	kA
Při	při	k7c6
tzv.	tzv.	kA
„	„	k?
<g/>
Plenění	plenění	k1gNnSc1
Říma	Řím	k1gInSc2
<g/>
“	“	k?
pouhých	pouhý	k2eAgInPc2d1
189	#num#	k4
mužů	muž	k1gMnPc2
bránilo	bránit	k5eAaImAgNnS
papeže	papež	k1gMnPc4
před	před	k7c7
španělskou	španělský	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
s	s	k7c7
mnoha	mnoho	k4c7
tisíci	tisíc	k4xCgInPc7
vojáky	voják	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umožnili	umožnit	k5eAaPmAgMnP
Klementu	Klement	k1gMnSc3
VII	VII	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1523	#num#	k4
<g/>
–	–	k?
<g/>
1534	#num#	k4
<g/>
)	)	kIx)
uprchnout	uprchnout	k5eAaPmF
do	do	k7c2
bezpečí	bezpečí	k1gNnSc2
<g/>
,	,	kIx,
přestože	přestože	k8xS
většina	většina	k1gFnSc1
(	(	kIx(
<g/>
147	#num#	k4
mužů	muž	k1gMnPc2
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
pobita	pobit	k2eAgFnSc1d1
<g/>
;	;	kIx,
přežili	přežít	k5eAaPmAgMnP
jen	jen	k9
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
papeže	papež	k1gMnSc4
eskortovali	eskortovat	k5eAaBmAgMnP
do	do	k7c2
nedaleké	daleký	k2eNgFnSc2d1
pevnosti	pevnost	k1gFnSc2
Andělský	andělský	k2eAgInSc4d1
hrad	hrad	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednou	jednou	k6eAd1
z	z	k7c2
podmínek	podmínka	k1gFnPc2
dobyvatelů	dobyvatel	k1gMnPc2
bylo	být	k5eAaImAgNnS
kromě	kromě	k7c2
darů	dar	k1gInPc2
finančních	finanční	k2eAgFnPc2d1
i	i	k8xC
hmotných	hmotný	k2eAgFnPc2d1
také	také	k9
rozpuštění	rozpuštění	k1gNnSc1
zbytků	zbytek	k1gInPc2
Švýcarské	švýcarský	k2eAgFnSc2d1
gardy	garda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgNnPc1
byla	být	k5eAaImAgNnP
obnovena	obnovit	k5eAaPmNgNnP
až	až	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1548	#num#	k4
za	za	k7c2
Pavla	Pavel	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1534	#num#	k4
<g/>
–	–	k?
<g/>
1549	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
současnosti	současnost	k1gFnSc6
má	mít	k5eAaImIp3nS
garda	garda	k1gFnSc1
110	#num#	k4
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
se	se	k3xPyFc4
stále	stále	k6eAd1
těší	těšit	k5eAaImIp3nS
velké	velký	k2eAgFnSc3d1
úctě	úcta	k1gFnSc3
pro	pro	k7c4
svou	svůj	k3xOyFgFnSc4
disciplínu	disciplína	k1gFnSc4
a	a	k8xC
věrnost	věrnost	k1gFnSc4
jako	jako	k8xS,k8xC
ve	v	k7c6
středověku	středověk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
nejmenší	malý	k2eAgFnSc4d3
<g/>
,	,	kIx,
nejstarší	starý	k2eAgFnSc4d3
<g/>
,	,	kIx,
nejbarevnější	barevný	k2eAgFnSc4d3
a	a	k8xC
nejfotografovanější	fotografovaný	k2eAgFnSc4d3
oficiální	oficiální	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Funguje	fungovat	k5eAaImIp3nS
zejména	zejména	k9
jako	jako	k9
reprezentativní	reprezentativní	k2eAgFnSc1d1
hradní	hradní	k2eAgFnSc1d1
stráž	stráž	k1gFnSc1
a	a	k8xC
ochranka	ochranka	k1gFnSc1
papeže	papež	k1gMnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgInSc7
ze	z	k7c2
symbolů	symbol	k1gInPc2
Vatikánu	Vatikán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Příslušníkem	příslušník	k1gMnSc7
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
stát	stát	k5eAaPmF,k5eAaImF
pouze	pouze	k6eAd1
svobodný	svobodný	k2eAgMnSc1d1
Švýcar	Švýcar	k1gMnSc1
–	–	k?
katolík	katolík	k1gMnSc1
ve	v	k7c6
věku	věk	k1gInSc6
19	#num#	k4
<g/>
–	–	k?
<g/>
30	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
vyšší	vysoký	k2eAgMnSc1d2
než	než	k8xS
174	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Musí	muset	k5eAaImIp3nS
dokončit	dokončit	k5eAaPmF
základní	základní	k2eAgInSc4d1
výcvik	výcvik	k1gInSc4
ve	v	k7c6
švýcarské	švýcarský	k2eAgFnSc6d1
armádě	armáda	k1gFnSc6
a	a	k8xC
mít	mít	k5eAaImF
středoškolské	středoškolský	k2eAgNnSc4d1
nebo	nebo	k8xC
odborné	odborný	k2eAgNnSc4d1
vzdělání	vzdělání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všichni	všechen	k3xTgMnPc1
nováčci	nováček	k1gMnPc1
vždy	vždy	k6eAd1
6	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
(	(	kIx(
<g/>
výroční	výroční	k2eAgInSc1d1
den	den	k1gInSc1
„	„	k?
<g/>
Plenění	plenění	k1gNnSc1
Říma	Řím	k1gInSc2
<g/>
“	“	k?
<g/>
)	)	kIx)
na	na	k7c6
nádvoří	nádvoří	k1gNnSc6
sv.	sv.	kA
Damase	Damas	k1gInSc6
přísahají	přísahat	k5eAaImIp3nP
na	na	k7c4
gardový	gardový	k2eAgInSc4d1
prapor	prapor	k1gInSc4
věrnost	věrnost	k1gFnSc4
papeži	papež	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
přísaze	přísaha	k1gFnSc6
zdvihají	zdvihat	k5eAaImIp3nP
pravou	pravý	k2eAgFnSc4d1
ruku	ruka	k1gFnSc4
se	s	k7c7
zvednutým	zvednutý	k2eAgInSc7d1
palcem	palec	k1gInSc7
<g/>
,	,	kIx,
ukazovákem	ukazovák	k1gInSc7
a	a	k8xC
prostředníčkem	prostředníček	k1gInSc7
jako	jako	k8xS,k8xC
symbol	symbol	k1gInSc1
sv.	sv.	kA
Trojice	trojice	k1gFnSc2
<g/>
,	,	kIx,
levou	levý	k2eAgFnSc7d1
rukou	ruka	k1gFnSc7
pevně	pevně	k6eAd1
drží	držet	k5eAaImIp3nS
žerď	žerď	k1gFnSc4
praporu	prapor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členství	členství	k1gNnSc1
v	v	k7c6
gardě	garda	k1gFnSc6
je	být	k5eAaImIp3nS
ve	v	k7c6
Švýcarsku	Švýcarsko	k1gNnSc6
často	často	k6eAd1
rodinnou	rodinný	k2eAgFnSc7d1
tradicí	tradice	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gardisté	gardista	k1gMnPc1
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
oženit	oženit	k5eAaPmF
<g/>
,	,	kIx,
pokud	pokud	k8xS
jsou	být	k5eAaImIp3nP
starší	starý	k2eAgFnPc1d2
25	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
odsloužili	odsloužit	k5eAaPmAgMnP
v	v	k7c6
gardě	garda	k1gFnSc6
minimálně	minimálně	k6eAd1
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
a	a	k8xC
upsali	upsat	k5eAaPmAgMnP
se	se	k3xPyFc4
na	na	k7c4
další	další	k2eAgInPc4d1
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
služby	služba	k1gFnSc2
a	a	k8xC
mají	mít	k5eAaImIp3nP
nejméně	málo	k6eAd3
hodnost	hodnost	k1gFnSc1
desátníka	desátník	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Uniforma	uniforma	k1gFnSc1
</s>
<s>
Členové	člen	k1gMnPc1
Švýcarské	švýcarský	k2eAgFnSc2d1
gardy	garda	k1gFnSc2
chodí	chodit	k5eAaImIp3nP
v	v	k7c6
nápadných	nápadný	k2eAgFnPc6d1
barevných	barevný	k2eAgFnPc6d1
uniformách	uniforma	k1gFnPc6
renesančního	renesanční	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Garda	garda	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
přišla	přijít	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1506	#num#	k4
do	do	k7c2
Říma	Řím	k1gInSc2
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
žádné	žádný	k3yNgFnPc4
zvláštní	zvláštní	k2eAgFnPc4d1
uniformy	uniforma	k1gFnPc4
neměla	mít	k5eNaImAgFnS
a	a	k8xC
byla	být	k5eAaImAgFnS
oblečena	obléct	k5eAaPmNgFnS
jako	jako	k9
většina	většina	k1gFnSc1
vojáků	voják	k1gMnPc2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
–	–	k?
v	v	k7c6
dubletech	dublet	k1gInPc6
s	s	k7c7
širokými	široký	k2eAgInPc7d1
rukávy	rukáv	k1gInPc7
<g/>
,	,	kIx,
v	v	k7c6
nohavicích	nohavice	k1gFnPc6
po	po	k7c4
kolena	koleno	k1gNnPc4
a	a	k8xC
v	v	k7c6
punčochách	punčocha	k1gFnPc6
<g/>
,	,	kIx,
s	s	k7c7
rameny	rameno	k1gNnPc7
a	a	k8xC
hrudí	hruď	k1gFnSc7
chráněnými	chráněný	k2eAgFnPc7d1
pancířem	pancíř	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Unifikované	unifikovaný	k2eAgFnPc4d1
odění	odění	k1gNnPc2
vojáků	voják	k1gMnPc2
se	se	k3xPyFc4
vytvořilo	vytvořit	k5eAaPmAgNnS
až	až	k9
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snad	snad	k9
mohli	moct	k5eAaImAgMnP
mít	mít	k5eAaImF
na	na	k7c6
hrudi	hruď	k1gFnSc6
švýcarský	švýcarský	k2eAgInSc4d1
kříž	kříž	k1gInSc4
nebo	nebo	k8xC
papežské	papežský	k2eAgInPc4d1
zkřížené	zkřížený	k2eAgInPc4d1
klíče	klíč	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
zprávách	zpráva	k1gFnPc6
o	o	k7c6
jejich	jejich	k3xOp3gInSc6
příchodu	příchod	k1gInSc6
do	do	k7c2
Říma	Řím	k1gInSc2
zmínka	zmínka	k1gFnSc1
o	o	k7c6
odění	odění	k1gNnSc6
není	být	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Historická	historický	k2eAgFnSc1d1
uniforma	uniforma	k1gFnSc1
</s>
<s>
Pracovní	pracovní	k2eAgFnSc1d1
uniforma	uniforma	k1gFnSc1
</s>
<s>
První	první	k4xOgFnPc1
uniformy	uniforma	k1gFnPc1
měly	mít	k5eAaImAgFnP
jen	jen	k9
dvě	dva	k4xCgFnPc1
barvy	barva	k1gFnPc1
–	–	k?
modrou	modrý	k2eAgFnSc7d1
a	a	k8xC
žlutou	žlutý	k2eAgFnSc7d1
<g/>
,	,	kIx,
odvozené	odvozený	k2eAgInPc1d1
od	od	k7c2
rodových	rodový	k2eAgFnPc2d1
barev	barva	k1gFnPc2
papeže	papež	k1gMnSc4
Julia	Julius	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k8xS
Lev	Lev	k1gMnSc1
X.	X.	kA
(	(	kIx(
<g/>
1513	#num#	k4
–	–	k?
1521	#num#	k4
<g/>
)	)	kIx)
z	z	k7c2
rodu	rod	k1gInSc2
Medici	medik	k1gMnPc1
přidal	přidat	k5eAaPmAgMnS
na	na	k7c4
uniformy	uniforma	k1gFnPc4
medicejskou	medicejský	k2eAgFnSc4d1
červenou	červená	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
se	se	k3xPyFc4
lidé	člověk	k1gMnPc1
často	často	k6eAd1
mylně	mylně	k6eAd1
domnívají	domnívat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
obleky	oblek	k1gInPc1
Švýcarské	švýcarský	k2eAgFnSc2d1
gardy	garda	k1gFnSc2
vymyslel	vymyslet	k5eAaPmAgMnS
sám	sám	k3xTgMnSc1
Michelangelo	Michelangela	k1gFnSc5
<g/>
,	,	kIx,
současná	současný	k2eAgFnSc1d1
úprava	úprava	k1gFnSc1
pochází	pocházet	k5eAaImIp3nS
až	až	k9
z	z	k7c2
roku	rok	k1gInSc2
1914	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navrhl	navrhnout	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
velitel	velitel	k1gMnSc1
Jules	Jules	k1gMnSc1
Répond	Répond	k1gMnSc1
ve	v	k7c6
snaze	snaha	k1gFnSc6
navrátit	navrátit	k5eAaPmF
časem	časem	k6eAd1
měnícím	měnící	k2eAgFnPc3d1
se	se	k3xPyFc4
uniformám	uniforma	k1gFnPc3
jejich	jejich	k3xOp3gInSc4
původní	původní	k2eAgInSc4d1
vzhled	vzhled	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inspiroval	inspirovat	k5eAaBmAgMnS
se	se	k3xPyFc4
Rafaelovými	Rafaelův	k2eAgFnPc7d1
freskami	freska	k1gFnPc7
<g/>
,	,	kIx,
klobouky	klobouk	k1gInPc7
nahradil	nahradit	k5eAaPmAgInS
barety	baret	k1gInPc4
a	a	k8xC
plisovaný	plisovaný	k2eAgInSc4d1
nákrčník	nákrčník	k1gInSc4
bílým	bílý	k2eAgInSc7d1
límcem	límec	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k9
pancíř	pancíř	k1gInSc1
vymodeloval	vymodelovat	k5eAaPmAgInS
podle	podle	k7c2
starých	starý	k2eAgNnPc2d1
vyobrazení	vyobrazení	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
se	se	k3xPyFc4
uniformy	uniforma	k1gFnPc1
šijí	šít	k5eAaImIp3nP
v	v	k7c6
Třešti	třeštit	k5eAaImRp2nS
u	u	k7c2
Jihlavy	Jihlava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
běžné	běžný	k2eAgFnSc6d1
stráži	stráž	k1gFnSc6
nosí	nosit	k5eAaImIp3nP
gardisté	gardista	k1gMnPc1
k	k	k7c3
uniformě	uniforma	k1gFnSc3
černý	černý	k2eAgInSc4d1
baret	baret	k1gInSc4
a	a	k8xC
bílé	bílý	k2eAgFnPc4d1
rukavice	rukavice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
nepříznivém	příznivý	k2eNgNnSc6d1
počasí	počasí	k1gNnSc6
stráže	stráž	k1gFnSc2
oblékají	oblékat	k5eAaImIp3nP
přes	přes	k7c4
uniformy	uniforma	k1gFnPc4
tmavě	tmavě	k6eAd1
modrý	modrý	k2eAgInSc4d1
plášť	plášť	k1gInSc4
bez	bez	k7c2
rukávů	rukáv	k1gInPc2
<g/>
,	,	kIx,
svazovaný	svazovaný	k2eAgInSc4d1
na	na	k7c6
bocích	bok	k1gInPc6
modrými	modrý	k2eAgFnPc7d1
stuhami	stuha	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
slavnostnějších	slavnostní	k2eAgFnPc6d2
příležitostech	příležitost	k1gFnPc6
mění	měnit	k5eAaImIp3nS
prostý	prostý	k2eAgInSc1d1
bílý	bílý	k2eAgInSc1d1
límec	límec	k1gInSc1
za	za	k7c4
plisovaný	plisovaný	k2eAgInSc4d1
nákrčník	nákrčník	k1gInSc4
a	a	k8xC
baret	baret	k1gInSc4
za	za	k7c4
morion	morion	k1gInSc4
se	s	k7c7
znakem	znak	k1gInSc7
Julia	Julius	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
je	být	k5eAaImIp3nS
ozdobený	ozdobený	k2eAgInSc1d1
pštrosím	pštrosí	k2eAgNnSc7d1
perem	pero	k1gNnSc7
podle	podle	k7c2
hodnosti	hodnost	k1gFnSc2
a	a	k8xC
funkce	funkce	k1gFnSc2
–	–	k?
bílé	bílý	k2eAgNnSc1d1
pero	pero	k1gNnSc1
je	být	k5eAaImIp3nS
určené	určený	k2eAgNnSc1d1
pro	pro	k7c4
velitele	velitel	k1gMnSc4
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
zástupce	zástupce	k1gMnSc1
<g/>
,	,	kIx,
fialové	fialový	k2eAgNnSc1d1
pro	pro	k7c4
důstojníky	důstojník	k1gMnPc4
<g/>
,	,	kIx,
červené	červený	k2eAgFnPc1d1
pro	pro	k7c4
poddůstojníky	poddůstojník	k1gMnPc4
a	a	k8xC
řadové	řadový	k2eAgMnPc4d1
gardisty	gardista	k1gMnPc4
-	-	kIx~
halapartníky	halapartník	k1gMnPc4
a	a	k8xC
černožluté	černožlutý	k2eAgMnPc4d1
(	(	kIx(
<g/>
na	na	k7c6
černé	černý	k2eAgFnSc6d1
přilbě	přilba	k1gFnSc6
<g/>
)	)	kIx)
pro	pro	k7c4
bubeníky	bubeník	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
obzvláště	obzvláště	k6eAd1
slavnostní	slavnostní	k2eAgFnPc4d1
příležitosti	příležitost	k1gFnPc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
přísaha	přísaha	k1gFnSc1
nových	nový	k2eAgInPc2d1
členů	člen	k1gInPc2
<g/>
,	,	kIx,
oblékají	oblékat	k5eAaImIp3nP
gardisté	gardista	k1gMnPc1
navíc	navíc	k6eAd1
ještě	ještě	k6eAd1
hrudní	hrudní	k2eAgInSc4d1
pancíř	pancíř	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Méně	málo	k6eAd2
známá	známý	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
nenápadnější	nápadní	k2eNgFnSc1d2
tmavě	tmavě	k6eAd1
modrá	modrý	k2eAgFnSc1d1
„	„	k?
<g/>
pracovní	pracovní	k2eAgFnSc1d1
<g/>
“	“	k?
uniforma	uniforma	k1gFnSc1
s	s	k7c7
širokým	široký	k2eAgInSc7d1
bílým	bílý	k2eAgInSc7d1
límcem	límec	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
ní	on	k3xPp3gFnSc3
nosí	nosit	k5eAaImIp3nP
gardisté	gardista	k1gMnPc1
také	také	k9
černý	černý	k2eAgInSc4d1
baret	baret	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Výzbroj	výzbroj	k1gFnSc1
</s>
<s>
Vedle	vedle	k7c2
velmi	velmi	k6eAd1
nápadné	nápadný	k2eAgFnSc2d1
reprezentativní	reprezentativní	k2eAgFnSc2d1
a	a	k8xC
ceremoniální	ceremoniální	k2eAgFnSc2d1
funkce	funkce	k1gFnSc2
plní	plnit	k5eAaImIp3nS
švýcarská	švýcarský	k2eAgFnSc1d1
garda	garda	k1gFnSc1
standardní	standardní	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
ochranky	ochranka	k1gFnSc2
papeže	papež	k1gMnSc2
a	a	k8xC
ozbrojených	ozbrojený	k2eAgFnPc2d1
sil	síla	k1gFnPc2
Vatikánu	Vatikán	k1gInSc2
-	-	kIx~
je	být	k5eAaImIp3nS
plně	plně	k6eAd1
funkčním	funkční	k2eAgInSc7d1
výběrovým	výběrový	k2eAgInSc7d1
vojenským	vojenský	k2eAgInSc7d1
útvarem	útvar	k1gInSc7
a	a	k8xC
její	její	k3xOp3gInSc4
muži	muž	k1gMnPc1
procházejí	procházet	k5eAaImIp3nP
náročným	náročný	k2eAgInSc7d1
výcvikem	výcvik	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
spíše	spíše	k9
ceremoniálních	ceremoniální	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
(	(	kIx(
<g/>
gardisté	gardista	k1gMnPc1
tradičně	tradičně	k6eAd1
používají	používat	k5eAaImIp3nP
halapartnu	halapartna	k1gFnSc4
a	a	k8xC
dlouhý	dlouhý	k2eAgInSc1d1
meč	meč	k1gInSc1
<g/>
,	,	kIx,
důstojníci	důstojník	k1gMnPc1
mají	mít	k5eAaImIp3nP
rapír	rapír	k1gInSc4
nebo	nebo	k8xC
šavli	šavle	k1gFnSc4
<g/>
)	)	kIx)
má	mít	k5eAaImIp3nS
garda	garda	k1gFnSc1
ve	v	k7c6
výzbroji	výzbroj	k1gFnSc6
i	i	k9
moderní	moderní	k2eAgFnPc1d1
palné	palný	k2eAgFnPc1d1
zbraně	zbraň	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
SIG	SIG	kA
P220	P220	k1gFnSc1
(	(	kIx(
<g/>
P	P	kA
<g/>
75	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Glock	Glock	k6eAd1
19	#num#	k4
</s>
<s>
Steyr	Steyr	k1gMnSc1
TMP	TMP	kA
</s>
<s>
Heckler	Heckler	k1gMnSc1
&	&	k?
Koch	Koch	k1gMnSc1
MP5A3	MP5A3	k1gMnSc1
</s>
<s>
Heckler	Heckler	k1gMnSc1
&	&	k?
Koch	Koch	k1gMnSc1
MP7A1	MP7A1	k1gMnSc1
</s>
<s>
SIG	SIG	kA
SG	SG	kA
550	#num#	k4
</s>
<s>
SIG	SIG	kA
SG	SG	kA
552	#num#	k4
</s>
<s>
Prapor	prapor	k1gInSc1
</s>
<s>
Prapor	prapor	k1gInSc1
švýcarské	švýcarský	k2eAgFnSc2d1
gardy	garda	k1gFnSc2
v	v	k7c6
letech	let	k1gInPc6
2013	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
</s>
<s>
Prapor	prapor	k1gInSc1
Švýcarské	švýcarský	k2eAgFnSc2d1
gardy	garda	k1gFnSc2
vychází	vycházet	k5eAaImIp3nS
ze	z	k7c2
švýcarské	švýcarský	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
a	a	k8xC
v	v	k7c6
současné	současný	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
se	se	k3xPyFc4
užívá	užívat	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1913	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
čtvercový	čtvercový	k2eAgInSc1d1
<g/>
,	,	kIx,
o	o	k7c6
straně	strana	k1gFnSc6
2,2	2,2	k4
m	m	kA
<g/>
,	,	kIx,
rozdělený	rozdělený	k2eAgInSc1d1
bílým	bílý	k2eAgInSc7d1
křížem	kříž	k1gInSc7
na	na	k7c4
čtyři	čtyři	k4xCgNnPc4
pole	pole	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
středu	střed	k1gInSc6
je	být	k5eAaImIp3nS
umístěn	umístěn	k2eAgInSc1d1
znak	znak	k1gInSc1
aktuálního	aktuální	k2eAgMnSc2d1
velitele	velitel	k1gMnSc2
gardy	garda	k1gFnSc2
(	(	kIx(
<g/>
od	od	k7c2
7	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2015	#num#	k4
Christoph	Christoph	k1gInSc1
Graf	graf	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prvním	první	k4xOgInSc6
poli	pole	k1gNnSc6
je	být	k5eAaImIp3nS
znak	znak	k1gInSc1
vládnoucího	vládnoucí	k2eAgMnSc2d1
papeže	papež	k1gMnSc2
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
2013	#num#	k4
Františka	Františka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
zůstala	zůstat	k5eAaPmAgFnS
tiara	tiara	k1gFnSc1
jako	jako	k8xS,k8xC
symbol	symbol	k1gInSc1
panovníka	panovník	k1gMnSc2
<g/>
,	,	kIx,
druhé	druhý	k4xOgNnSc4
a	a	k8xC
třetí	třetí	k4xOgNnSc4
pole	pole	k1gNnSc4
obsahuje	obsahovat	k5eAaImIp3nS
5	#num#	k4
pruhů	pruh	k1gInPc2
v	v	k7c6
barvách	barva	k1gFnPc6
švýcarské	švýcarský	k2eAgFnSc2d1
gardy	garda	k1gFnSc2
(	(	kIx(
<g/>
modrá	modrý	k2eAgFnSc1d1
<g/>
,	,	kIx,
žlutá	žlutý	k2eAgFnSc1d1
<g/>
,	,	kIx,
červená	červený	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
čtvrtém	čtvrtý	k4xOgInSc6
poli	pole	k1gNnSc6
je	být	k5eAaImIp3nS
umístěn	umístěn	k2eAgInSc1d1
znak	znak	k1gInSc1
Julia	Julius	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
Švýcarskou	švýcarský	k2eAgFnSc4d1
gardu	garda	k1gFnSc4
založil	založit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prapor	prapor	k1gInSc1
se	se	k3xPyFc4
mění	měnit	k5eAaImIp3nS
pokaždé	pokaždé	k6eAd1
s	s	k7c7
novým	nový	k2eAgMnSc7d1
papežem	papež	k1gMnSc7
či	či	k8xC
velitelem	velitel	k1gMnSc7
gardy	garda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heslem	heslo	k1gNnSc7
gardy	garda	k1gFnSc2
je	být	k5eAaImIp3nS
„	„	k?
<g/>
Odvaha	odvaha	k1gFnSc1
a	a	k8xC
věrnost	věrnost	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Velitelé	velitel	k1gMnPc1
gardy	garda	k1gFnSc2
</s>
<s>
Slavnostní	slavnostní	k2eAgFnSc1d1
uniforma	uniforma	k1gFnSc1
</s>
<s>
1506	#num#	k4
–	–	k?
1517	#num#	k4
<g/>
Kaspar	Kaspar	k1gInSc1
von	von	k1gInSc1
Silenen	Silenen	k2eAgInSc1d1
</s>
<s>
1518	#num#	k4
–	–	k?
1524	#num#	k4
<g/>
Markus	Markus	k1gInSc1
Röist	Röist	k1gInSc4
</s>
<s>
1524	#num#	k4
–	–	k?
1527	#num#	k4
<g/>
Kaspar	Kaspar	k1gInSc1
Röist	Röist	k1gInSc4
</s>
<s>
1548	#num#	k4
–	–	k?
1559	#num#	k4
<g/>
Jost	Jost	k2eAgInSc4d1
von	von	k1gInSc4
Meggen	Meggen	k1gInSc1
</s>
<s>
1559	#num#	k4
–	–	k?
1564	#num#	k4
<g/>
Kaspar	Kaspar	k1gMnSc1
Leo	Leo	k1gMnSc1
von	von	k1gInSc4
Silenen	Silenen	k2eAgInSc4d1
</s>
<s>
1566	#num#	k4
–	–	k?
1592	#num#	k4
<g/>
Jost	Jost	k2eAgInSc4d1
Segesser	Segesser	k1gInSc4
von	von	k1gInSc1
Brunegg	Brunegg	k1gInSc1
</s>
<s>
1592	#num#	k4
–	–	k?
1629	#num#	k4
<g/>
Stephan	Stephany	k1gInPc2
Alexander	Alexandra	k1gFnPc2
Segesser	Segessra	k1gFnPc2
von	von	k1gInSc1
Brunegg	Brunegg	k1gMnSc1
</s>
<s>
1629	#num#	k4
–	–	k?
1640	#num#	k4
<g/>
Nikolaus	Nikolaus	k1gInSc1
Fleckenstein	Fleckenstein	k2eAgInSc1d1
</s>
<s>
1640	#num#	k4
–	–	k?
1652	#num#	k4
<g/>
Jost	Jost	k2eAgInSc4d1
Fleckenstein	Fleckenstein	k1gInSc4
</s>
<s>
1652	#num#	k4
–	–	k?
1657	#num#	k4
<g/>
Johann	Johanno	k1gNnPc2
Rudolf	Rudolf	k1gMnSc1
Pfyffer	Pfyffer	k1gMnSc1
von	von	k1gInSc4
Altishofen	Altishofen	k2eAgInSc4d1
</s>
<s>
1658	#num#	k4
–	–	k?
1686	#num#	k4
<g/>
Ludwig	Ludwig	k1gMnSc1
Pfyffer	Pfyffer	k1gMnSc1
von	von	k1gInSc4
Altishofen	Altishofen	k2eAgInSc4d1
</s>
<s>
1686	#num#	k4
–	–	k?
1696	#num#	k4
<g/>
Franz	Franz	k1gMnSc1
Pfyffer	Pfyffer	k1gMnSc1
von	von	k1gInSc4
Altishofen	Altishofen	k2eAgInSc4d1
</s>
<s>
1696	#num#	k4
–	–	k?
1704	#num#	k4
<g/>
Johann	Johanno	k1gNnPc2
Kaspar	Kaspara	k1gFnPc2
Mayr	Mayra	k1gFnPc2
von	von	k1gInSc1
Baldegg	Baldegg	k1gMnSc1
</s>
<s>
1712	#num#	k4
–	–	k?
1727	#num#	k4
<g/>
Johann	Johann	k1gInSc1
Konrad	Konrad	k1gInSc4
Pfyffer	Pfyffer	k1gInSc1
von	von	k1gInSc1
Altishofen	Altishofen	k2eAgInSc1d1
</s>
<s>
1727	#num#	k4
–	–	k?
1754	#num#	k4
<g/>
Franz	Franz	k1gMnSc1
Ludwig	Ludwig	k1gMnSc1
Pfyffer	Pfyffer	k1gMnSc1
von	von	k1gInSc4
Altishofen	Altishofen	k2eAgInSc4d1
</s>
<s>
1754	#num#	k4
–	–	k?
1782	#num#	k4
<g/>
Jost	Jost	k2eAgInSc4d1
Ignaz	Ignaz	k1gInSc4
Pfyffer	Pfyffer	k1gInSc1
von	von	k1gInSc1
Altishofen	Altishofen	k2eAgInSc1d1
</s>
<s>
1783	#num#	k4
–	–	k?
1798	#num#	k4
<g/>
Franz	Franz	k1gMnSc1
Alois	Alois	k1gMnSc1
Pfyffer	Pfyffer	k1gMnSc1
von	von	k1gInSc4
Altishofen	Altishofen	k2eAgInSc4d1
</s>
<s>
1800	#num#	k4
–	–	k?
1834	#num#	k4
<g/>
Karl	Karla	k1gFnPc2
Leodegar	Leodegar	k1gMnSc1
Pfyffer	Pfyffer	k1gMnSc1
von	von	k1gInSc4
Altishofen	Altishofen	k2eAgInSc4d1
</s>
<s>
1835	#num#	k4
–	–	k?
1847	#num#	k4
<g/>
Martin	Martin	k1gMnSc1
Pfyffer	Pfyffer	k1gMnSc1
von	von	k1gInSc4
Altishofen	Altishofen	k2eAgInSc4d1
</s>
<s>
1847	#num#	k4
–	–	k?
1860	#num#	k4
<g/>
Franz	Franz	k1gMnSc1
Xaver	Xaver	k1gMnSc1
Leopold	Leopold	k1gMnSc1
Meyer	Meyer	k1gMnSc1
von	von	k1gInSc4
Schauensee	Schauensee	k1gNnSc2
</s>
<s>
1860	#num#	k4
–	–	k?
1878	#num#	k4
<g/>
Alfred	Alfred	k1gMnSc1
von	von	k1gInSc1
Sonnenberg	Sonnenberg	k1gMnSc1
</s>
<s>
1878	#num#	k4
–	–	k?
1901	#num#	k4
<g/>
Louis-Martin	Louis-Martin	k2eAgMnSc1d1
de	de	k?
Courten	Courten	k2eAgInSc1d1
</s>
<s>
1901	#num#	k4
–	–	k?
1910	#num#	k4
<g/>
Leopold	Leopolda	k1gFnPc2
Meyer	Meyra	k1gFnPc2
von	von	k1gInSc1
Schauensee	Schauense	k1gInSc2
</s>
<s>
1910	#num#	k4
–	–	k?
1921	#num#	k4
<g/>
Jules	Jules	k1gInSc1
Repond	Repond	k1gInSc4
</s>
<s>
1921	#num#	k4
–	–	k?
1935	#num#	k4
<g/>
Alois	Alois	k1gMnSc1
Hirschbühl	Hirschbühl	k1gMnSc1
</s>
<s>
1935	#num#	k4
–	–	k?
1942	#num#	k4
<g/>
Georg	Georg	k1gInSc1
von	von	k1gInSc4
Sury	Sura	k1gFnSc2
d	d	k?
<g/>
'	'	kIx"
<g/>
Aspremont	Aspremont	k1gMnSc1
</s>
<s>
1942	#num#	k4
–	–	k?
1957	#num#	k4
<g/>
Heinrich	Heinrich	k1gMnSc1
Pfyffer	Pfyffer	k1gMnSc1
von	von	k1gInSc4
Altishofen	Altishofen	k2eAgInSc4d1
</s>
<s>
1957	#num#	k4
–	–	k?
1972	#num#	k4
<g/>
Robert	Roberta	k1gFnPc2
Nünlis	Nünlis	k1gFnPc2
</s>
<s>
1972	#num#	k4
–	–	k?
1982	#num#	k4
<g/>
Franz	Franz	k1gMnSc1
Pfyffer	Pfyffer	k1gMnSc1
von	von	k1gInSc4
Altishofen	Altishofen	k2eAgInSc4d1
</s>
<s>
1982	#num#	k4
–	–	k?
1998	#num#	k4
<g/>
Roland	Rolando	k1gNnPc2
Buchs	Buchsa	k1gFnPc2
</s>
<s>
1998	#num#	k4
-	-	kIx~
1998	#num#	k4
<g/>
Alois	Alois	k1gMnSc1
Estermann	Estermann	k1gMnSc1
</s>
<s>
1998	#num#	k4
-	-	kIx~
2002	#num#	k4
<g/>
Pius	Pius	k1gMnSc1
Segmüller	Segmüller	k1gMnSc1
</s>
<s>
2002	#num#	k4
-	-	kIx~
2008	#num#	k4
<g/>
Elmar	Elmar	k1gMnSc1
Theodor	Theodor	k1gMnSc1
Moder	moder	k1gInSc4
</s>
<s>
2008	#num#	k4
-	-	kIx~
2015	#num#	k4
<g/>
Daniel	Daniel	k1gMnSc1
Rudolf	Rudolf	k1gMnSc1
Anrig	Anrig	k1gMnSc1
</s>
<s>
2015	#num#	k4
-	-	kIx~
Christoph	Christoph	k1gInSc1
Graf	graf	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Papež	Papež	k1gMnSc1
jmenoval	jmenovat	k5eAaBmAgMnS,k5eAaImAgMnS
nového	nový	k2eAgMnSc4d1
velitele	velitel	k1gMnSc4
Švýcarské	švýcarský	k2eAgFnSc2d1
gardy	garda	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
radiovaticana	radiovaticana	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Švýcarská	švýcarský	k2eAgFnSc1d1
garda	garda	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
gardy	garda	k1gFnSc2
</s>
<s>
Informace	informace	k1gFnPc1
o	o	k7c6
gardě	garda	k1gFnSc6
na	na	k7c6
oficiálních	oficiální	k2eAgFnPc6d1
stránkách	stránka	k1gFnPc6
Vatikánu	Vatikán	k1gInSc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
muzea	muzeum	k1gNnSc2
Švýcarské	švýcarský	k2eAgFnPc1d1
gardy	garda	k1gFnPc1
v	v	k7c4
Naters	Naters	k1gInSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Římská	římský	k2eAgFnSc1d1
kurie	kurie	k1gFnSc1
Dikasteria	Dikasterium	k1gNnSc2
</s>
<s>
Státní	státní	k2eAgInSc1d1
sekretariát	sekretariát	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
Kardinál	kardinál	k1gMnSc1
státní	státní	k2eAgMnSc1d1
sekretář	sekretář	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
I.	I.	kA
sekce	sekce	k1gFnSc2
(	(	kIx(
<g/>
vedoucí	vedoucí	k1gMnSc1
<g/>
:	:	kIx,
Substitut	substitut	k1gMnSc1
pro	pro	k7c4
všeobecné	všeobecný	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
<g/>
)	)	kIx)
•	•	k?
II	II	kA
<g/>
.	.	kIx.
sekce	sekce	k1gFnSc1
(	(	kIx(
<g/>
vedoucí	vedoucí	k1gMnSc1
<g/>
:	:	kIx,
Sekretář	sekretář	k1gMnSc1
pro	pro	k7c4
vztahy	vztah	k1gInPc4
se	s	k7c7
státy	stát	k1gInPc7
<g/>
)	)	kIx)
•	•	k?
III	III	kA
<g/>
.	.	kIx.
sekce	sekce	k1gFnSc1
(	(	kIx(
<g/>
vedoucí	vedoucí	k1gMnSc1
<g/>
:	:	kIx,
Delegát	delegát	k1gMnSc1
pro	pro	k7c4
papežská	papežský	k2eAgNnPc4d1
diplomatická	diplomatický	k2eAgNnPc4d1
zastoupení	zastoupení	k1gNnPc4
<g/>
)	)	kIx)
Kongregace	kongregace	k1gFnSc1
</s>
<s>
Kongregace	kongregace	k1gFnSc1
pro	pro	k7c4
nauku	nauka	k1gFnSc4
víry	víra	k1gFnSc2
•	•	k?
Kongregace	kongregace	k1gFnSc2
pro	pro	k7c4
východní	východní	k2eAgFnPc4d1
církve	církev	k1gFnPc4
•	•	k?
Kongregace	kongregace	k1gFnSc2
pro	pro	k7c4
bohoslužbu	bohoslužba	k1gFnSc4
a	a	k8xC
svátosti	svátost	k1gFnPc4
•	•	k?
Kongregace	kongregace	k1gFnSc2
pro	pro	k7c4
blahořečení	blahořečení	k1gNnSc4
a	a	k8xC
svatořečení	svatořečení	k1gNnSc4
•	•	k?
Kongregace	kongregace	k1gFnSc2
pro	pro	k7c4
evangelizaci	evangelizace	k1gFnSc4
národů	národ	k1gInPc2
•	•	k?
Kongregace	kongregace	k1gFnSc1
pro	pro	k7c4
klérus	klérus	k1gInSc4
•	•	k?
Kongregace	kongregace	k1gFnSc2
pro	pro	k7c4
společnosti	společnost	k1gFnPc4
zasvěceného	zasvěcený	k2eAgInSc2d1
života	život	k1gInSc2
a	a	k8xC
společnosti	společnost	k1gFnSc2
apoštolského	apoštolský	k2eAgInSc2d1
života	život	k1gInSc2
•	•	k?
Kongregace	kongregace	k1gFnSc2
pro	pro	k7c4
katolickou	katolický	k2eAgFnSc4d1
výchovu	výchova	k1gFnSc4
•	•	k?
Kongregace	kongregace	k1gFnPc1
pro	pro	k7c4
biskupy	biskup	k1gInPc4
Dikasteria	Dikasterium	k1gNnSc2
</s>
<s>
Dikasterium	Dikasterium	k1gNnSc1
pro	pro	k7c4
laiky	laik	k1gMnPc4
<g/>
,	,	kIx,
rodinu	rodina	k1gFnSc4
a	a	k8xC
život	život	k1gInSc1
•	•	k?
Dikasterium	Dikasterium	k1gNnSc1
pro	pro	k7c4
službu	služba	k1gFnSc4
integrálnímu	integrální	k2eAgInSc3d1
lidskému	lidský	k2eAgInSc3d1
rozvoji	rozvoj	k1gInSc3
•	•	k?
Dikasterium	Dikasterium	k1gNnSc1
pro	pro	k7c4
komunikaci	komunikace	k1gFnSc4
(	(	kIx(
<g/>
Tiskový	tiskový	k2eAgInSc1d1
úřad	úřad	k1gInSc1
Svatého	svatý	k2eAgInSc2d1
Stolce	stolec	k1gInSc2
•	•	k?
Vatikánský	vatikánský	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
•	•	k?
Vatican	Vatican	k1gMnSc1
Media	medium	k1gNnSc2
•	•	k?
L	L	kA
<g/>
'	'	kIx"
<g/>
Osservatore	Osservator	k1gMnSc5
Romano	Romano	k1gMnSc5
•	•	k?
Vatikánská	vatikánský	k2eAgFnSc1d1
tiskárna	tiskárna	k1gFnSc1
•	•	k?
Vatikánské	vatikánský	k2eAgNnSc4d1
nakladatelství	nakladatelství	k1gNnSc4
a	a	k8xC
knihkupectví	knihkupectví	k1gNnSc4
<g/>
)	)	kIx)
Tribunály	tribunál	k1gInPc1
</s>
<s>
Apoštolská	apoštolský	k2eAgFnSc1d1
penitenciárie	penitenciárie	k1gFnSc1
•	•	k?
Nejvyšší	vysoký	k2eAgInSc1d3
tribunál	tribunál	k1gInSc1
apoštolské	apoštolský	k2eAgFnSc2d1
signatury	signatura	k1gFnSc2
•	•	k?
Tribunál	tribunál	k1gInSc1
Římské	římský	k2eAgFnSc2d1
roty	rota	k1gFnSc2
Papežské	papežský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
</s>
<s>
Papežská	papežský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
pro	pro	k7c4
jednotu	jednota	k1gFnSc4
křesťanů	křesťan	k1gMnPc2
•	•	k?
Papežská	papežský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
pro	pro	k7c4
výklad	výklad	k1gInSc4
legislativních	legislativní	k2eAgInPc2d1
textů	text	k1gInPc2
•	•	k?
Papežská	papežský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
pro	pro	k7c4
mezináboženský	mezináboženský	k2eAgInSc4d1
dialog	dialog	k1gInSc4
•	•	k?
Papežská	papežský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
pro	pro	k7c4
kulturu	kultura	k1gFnSc4
•	•	k?
Papežská	papežský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
pro	pro	k7c4
novou	nový	k2eAgFnSc4d1
evangelizaci	evangelizace	k1gFnSc4
Úřady	úřada	k1gMnSc2
</s>
<s>
Apoštolská	apoštolský	k2eAgFnSc1d1
Komora	komora	k1gFnSc1
•	•	k?
Správa	správa	k1gFnSc1
majetku	majetek	k1gInSc2
Apoštolského	apoštolský	k2eAgInSc2d1
stolce	stolec	k1gInSc2
Ekonomický	ekonomický	k2eAgInSc1d1
sekretariát	sekretariát	k1gInSc1
</s>
<s>
Rada	rada	k1gFnSc1
pro	pro	k7c4
ekonomii	ekonomie	k1gFnSc4
•	•	k?
Generální	generální	k2eAgMnSc1d1
revizor	revizor	k1gMnSc1
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1
složky	složka	k1gFnPc1
římské	římský	k2eAgFnSc2d1
kurie	kurie	k1gFnSc2
</s>
<s>
Papežské	papežský	k2eAgFnPc1d1
komise	komise	k1gFnPc1
</s>
<s>
Ecclesia	Ecclesia	k1gFnSc1
Dei	Dei	k1gFnSc1
•	•	k?
pro	pro	k7c4
archeologii	archeologie	k1gFnSc4
•	•	k?
biblická	biblický	k2eAgFnSc1d1
•	•	k?
mezinárodní	mezinárodní	k2eAgFnSc1d1
teologická	teologický	k2eAgFnSc1d1
•	•	k?
pro	pro	k7c4
Katechismus	katechismus	k1gInSc4
•	•	k?
pro	pro	k7c4
Lat.	Lat.	k1gFnSc4
Ameriku	Amerika	k1gFnSc4
•	•	k?
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
nezletilých	zletilý	k2eNgMnPc2d1,k2eAgMnPc2d1
•	•	k?
pro	pro	k7c4
zdravotnictví	zdravotnictví	k1gNnSc4
Papežské	papežský	k2eAgInPc4d1
výbory	výbor	k1gInPc4
</s>
<s>
Papežský	papežský	k2eAgInSc1d1
výbor	výbor	k1gInSc1
pro	pro	k7c4
historické	historický	k2eAgFnPc4d1
vědy	věda	k1gFnPc4
•	•	k?
Papežský	papežský	k2eAgInSc1d1
výbor	výbor	k1gInSc1
pro	pro	k7c4
mezinárodní	mezinárodní	k2eAgInPc4d1
eucharistické	eucharistický	k2eAgInPc4d1
kongresy	kongres	k1gInPc4
Ostatní	ostatní	k2eAgInPc4d1
</s>
<s>
Rada	rada	k1gFnSc1
kardinálů	kardinál	k1gMnPc2
•	•	k?
Švýcarská	švýcarský	k2eAgFnSc1d1
garda	garda	k1gFnSc1
•	•	k?
Řád	řád	k1gInSc4
Božího	boží	k2eAgInSc2d1
hrobu	hrob	k1gInSc2
•	•	k?
Prefektura	prefektura	k1gFnSc1
papežského	papežský	k2eAgInSc2d1
domu	dům	k1gInSc2
•	•	k?
Úřad	úřad	k1gInSc1
pro	pro	k7c4
papežské	papežský	k2eAgFnPc4d1
bohoslužby	bohoslužba	k1gFnPc4
•	•	k?
Centrální	centrální	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
církve	církev	k1gFnSc2
</s>
<s>
Instituce	instituce	k1gFnPc4
spojené	spojený	k2eAgFnPc4d1
se	s	k7c7
Svatým	svatý	k2eAgInSc7d1
Stolcem	stolec	k1gInSc7
(	(	kIx(
<g/>
nejsou	být	k5eNaImIp3nP
součástí	součást	k1gFnSc7
kurie	kurie	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Papežské	papežský	k2eAgFnPc1d1
baziliky	bazilika	k1gFnPc1
</s>
<s>
Bazilika	bazilika	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Petra	Petr	k1gMnSc2
•	•	k?
Lateránská	lateránský	k2eAgFnSc1d1
bazilika	bazilika	k1gFnSc1
•	•	k?
Bazilika	bazilika	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Pavla	Pavel	k1gMnSc2
za	za	k7c7
hradbami	hradba	k1gFnPc7
•	•	k?
Bazilika	bazilika	k1gFnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
Sněžné	sněžný	k2eAgFnSc2d1
Papežské	papežský	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
</s>
<s>
Papežská	papežský	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
Cultorum	Cultorum	k1gInSc1
Martyrum	Martyrum	k?
•	•	k?
Papežská	papežský	k2eAgFnSc1d1
církevní	církevní	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
•	•	k?
Papežská	papežský	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
Neposkvrněné	poskvrněný	k2eNgFnSc2d1
•	•	k?
Papežská	papežský	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
sv.	sv.	kA
Tomáše	Tomáš	k1gMnSc2
Akvinského	Akvinský	k2eAgMnSc2d1
•	•	k?
Papežská	papežský	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
věd	věda	k1gFnPc2
•	•	k?
Papežská	papežský	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
společenských	společenský	k2eAgFnPc2d1
věd	věda	k1gFnPc2
•	•	k?
Papežská	papežský	k2eAgFnSc1d1
teologická	teologický	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
•	•	k?
Papežská	papežský	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
pro	pro	k7c4
život	život	k1gInSc4
•	•	k?
Papežská	papežský	k2eAgFnSc1d1
latinská	latinský	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
Ostatní	ostatní	k2eAgFnSc1d1
</s>
<s>
Vatikánský	vatikánský	k2eAgInSc1d1
apoštolský	apoštolský	k2eAgInSc1d1
archiv	archiv	k1gInSc1
•	•	k?
Vatikánská	vatikánský	k2eAgFnSc1d1
apoštolská	apoštolský	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
•	•	k?
Úřad	úřad	k1gInSc4
apoštolské	apoštolský	k2eAgFnSc2d1
charity	charita	k1gFnSc2
•	•	k?
AVEPRO	AVEPRO	kA
•	•	k?
Autorita	autorita	k1gFnSc1
finančního	finanční	k2eAgInSc2d1
dohledu	dohled	k1gInSc2
a	a	k8xC
informovanosti	informovanost	k1gFnSc2
•	•	k?
Pracovní	pracovní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
Apoštolského	apoštolský	k2eAgInSc2d1
stolce	stolec	k1gInSc2
•	•	k?
Řád	řád	k1gInSc1
Božího	boží	k2eAgInSc2d1
hrobu	hrob	k1gInSc2
</s>
<s>
Městský	městský	k2eAgInSc1d1
stát	stát	k1gInSc1
Vatikán	Vatikán	k1gInSc1
</s>
<s>
Papežská	papežský	k2eAgFnSc1d1
komise	komise	k1gFnSc1
pro	pro	k7c4
Městský	městský	k2eAgInSc4d1
stát	stát	k1gInSc4
Vatikán	Vatikán	k1gInSc1
•	•	k?
Governatorát	Governatorát	k1gInSc4
Městského	městský	k2eAgInSc2d1
státu	stát	k1gInSc2
Vatikán	Vatikán	k1gInSc1
•	•	k?
Vatikánská	vatikánský	k2eAgFnSc1d1
banka	banka	k1gFnSc1
•	•	k?
Vatikánská	vatikánský	k2eAgFnSc1d1
observatoř	observatoř	k1gFnSc1
•	•	k?
Stavební	stavební	k2eAgFnSc1d1
huť	huť	k1gFnSc1
baziliky	bazilika	k1gFnSc2
sv.	sv.	kA
Petra	Petr	k1gMnSc2
•	•	k?
Generální	generální	k2eAgMnSc1d1
vikář	vikář	k1gMnSc1
Jeho	jeho	k3xOp3gFnPc4
Svatosti	svatost	k1gFnPc4
pro	pro	k7c4
Vatikánské	vatikánský	k2eAgNnSc4d1
město	město	k1gNnSc4
Diecéze	diecéze	k1gFnSc2
Řím	Řím	k1gInSc4
</s>
<s>
Generální	generální	k2eAgMnSc1d1
vikář	vikář	k1gMnSc1
Jeho	jeho	k3xOp3gFnPc4
Svatosti	svatost	k1gFnPc4
pro	pro	k7c4
římskou	římský	k2eAgFnSc4d1
diecézi	diecéze	k1gFnSc4
(	(	kIx(
<g/>
Kardinál	kardinál	k1gMnSc1
vikář	vikář	k1gMnSc1
<g/>
)	)	kIx)
Zaniklé	zaniklý	k2eAgInPc4d1
úřady	úřad	k1gInPc4
(	(	kIx(
<g/>
některé	některý	k3yIgFnPc1
<g/>
)	)	kIx)
</s>
<s>
Papežská	papežský	k2eAgFnSc1d1
kancelář	kancelář	k1gFnSc1
•	•	k?
Datárie	Datárie	k1gFnSc2
•	•	k?
Římská	římský	k2eAgFnSc1d1
inkvizice	inkvizice	k1gFnSc1
•	•	k?
Posvátná	posvátný	k2eAgFnSc1d1
kongregace	kongregace	k1gFnSc1
Svatého	svatý	k2eAgNnSc2d1
Oficia	oficium	k1gNnSc2
•	•	k?
Kongregace	kongregace	k1gFnSc1
Indexu	index	k1gInSc2
•	•	k?
Opus	opus	k1gInSc1
Fundatum	Fundatum	k1gNnSc1
Latinitas	Latinitas	k1gInSc1
•	•	k?
Papežská	papežský	k2eAgFnSc1d1
komise	komise	k1gFnSc1
pro	pro	k7c4
kulturní	kulturní	k2eAgNnSc4d1
dědictví	dědictví	k1gNnSc4
církve	církev	k1gFnSc2
•	•	k?
Kongregace	kongregace	k1gFnSc2
pro	pro	k7c4
mimořádné	mimořádný	k2eAgFnPc4d1
církevní	církevní	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
•	•	k?
Kongregace	kongregace	k1gFnPc4
Koncilu	koncil	k1gInSc2
•	•	k?
Kongregace	kongregace	k1gFnSc1
ritů	rit	k1gInPc2
•	•	k?
Papežská	papežský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
pro	pro	k7c4
laiky	laik	k1gMnPc4
•	•	k?
Papežská	papežský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
pro	pro	k7c4
rodinu	rodina	k1gFnSc4
•	•	k?
Papežská	papežský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
pro	pro	k7c4
sdělovací	sdělovací	k2eAgInPc4d1
prostředky	prostředek	k1gInPc4
•	•	k?
Papežská	papežský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
„	„	k?
<g/>
Justitia	Justitia	k1gFnSc1
et	et	k?
pax	pax	k?
<g/>
“	“	k?
•	•	k?
Papežská	papežský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
„	„	k?
<g/>
Cor	Cor	k1gMnSc1
Unum	Unum	k1gMnSc1
<g/>
“	“	k?
•	•	k?
Papežská	papežský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
pro	pro	k7c4
pastoraci	pastorace	k1gFnSc4
migrantů	migrant	k1gMnPc2
a	a	k8xC
lidí	člověk	k1gMnPc2
mimo	mimo	k7c4
domov	domov	k1gInSc4
•	•	k?
Papežská	papežský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
pro	pro	k7c4
pastoraci	pastorace	k1gFnSc4
zdravotníků	zdravotník	k1gMnPc2
•	•	k?
Prefektura	prefektura	k1gFnSc1
ekonomických	ekonomický	k2eAgFnPc2d1
záležitostí	záležitost	k1gFnPc2
Apoštolského	apoštolský	k2eAgInSc2d1
stolce	stolec	k1gInSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
olak	olak	k1gInSc1
<g/>
2004203660	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
2127514-2	2127514-2	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0663	#num#	k4
6155	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
94069781	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
134191354	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
94069781	#num#	k4
</s>
