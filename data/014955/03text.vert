<s>
Socialistický	socialistický	k2eAgInSc1d1
ekonomický	ekonomický	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
Socialistický	socialistický	k2eAgInSc1d1
ekonomický	ekonomický	k2eAgInSc1d1
systém	systém	k1gInSc1
je	být	k5eAaImIp3nS
charakterizován	charakterizován	k2eAgInSc1d1
společenským	společenský	k2eAgNnSc7d1
vlastnictvím	vlastnictví	k1gNnSc7
výrobních	výrobní	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
a	a	k8xC
ekonomickým	ekonomický	k2eAgInSc7d1
intervencionismem	intervencionismus	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgInSc7d1
principem	princip	k1gInSc7
je	být	k5eAaImIp3nS
skrze	skrze	k?
centrální	centrální	k2eAgNnSc4d1
plánování	plánování	k1gNnSc4
koordinovat	koordinovat	k5eAaBmF
produkci	produkce	k1gFnSc4
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
uspokojila	uspokojit	k5eAaPmAgFnS
ekonomickou	ekonomický	k2eAgFnSc4d1
poptávku	poptávka	k1gFnSc4
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
dosahování	dosahování	k1gNnSc2
zisku	zisk	k1gInSc2
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
je	být	k5eAaImIp3nS
tomu	ten	k3xDgNnSc3
u	u	k7c2
kapitalistického	kapitalistický	k2eAgInSc2d1
ekonomického	ekonomický	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Socialistické	socialistický	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
využívají	využívat	k5eAaImIp3nP,k5eAaPmIp3nP
trh	trh	k1gInSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
nazývají	nazývat	k5eAaImIp3nP
socialistické	socialistický	k2eAgFnSc2d1
tržní	tržní	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
druh	druh	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
současnosti	současnost	k1gFnSc6
v	v	k7c6
Čínské	čínský	k2eAgFnSc6d1
lidové	lidový	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
se	se	k3xPyFc4
v	v	k7c6
ČLR	ČLR	kA
stále	stále	k6eAd1
používá	používat	k5eAaImIp3nS
pětileté	pětiletý	k2eAgNnSc1d1
plánování	plánování	k1gNnSc1
<g/>
,	,	kIx,
země	země	k1gFnSc1
se	se	k3xPyFc4
otevřela	otevřít	k5eAaPmAgFnS
zahraničním	zahraniční	k2eAgMnPc3d1
investorům	investor	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
tak	tak	k6eAd1
mohou	moct	k5eAaImIp3nP
využívat	využívat	k5eAaImF,k5eAaPmF
velký	velký	k2eAgInSc4d1
potenciál	potenciál	k1gInSc4
Čínské	čínský	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
pracovní	pracovní	k2eAgFnSc2d1
síly	síla	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
současnosti	současnost	k1gFnSc6
je	být	k5eAaImIp3nS
centrálně	centrálně	k6eAd1
plánovaný	plánovaný	k2eAgInSc1d1
socialistický	socialistický	k2eAgInSc1d1
ekonomický	ekonomický	k2eAgInSc1d1
systém	systém	k1gInSc1
využíván	využíván	k2eAgInSc1d1
např.	např.	kA
na	na	k7c6
Kubě	Kuba	k1gFnSc6
nebo	nebo	k8xC
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Koreji	Korea	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
a	a	k8xC
příklady	příklad	k1gInPc1
ekonomického	ekonomický	k2eAgInSc2d1
systému	systém	k1gInSc2
</s>
<s>
Karl	Karl	k1gMnSc1
Marx	Marx	k1gMnSc1
vydal	vydat	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1867	#num#	k4
první	první	k4xOgFnSc1
část	část	k1gFnSc1
Kapitálu	kapitál	k1gInSc2
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
:	:	kIx,
Das	Das	k1gFnSc1
Kapital	Kapital	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
životního	životní	k2eAgNnSc2d1
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
díle	díl	k1gInSc6
Marx	Marx	k1gMnSc1
rozebral	rozebrat	k5eAaPmAgMnS
vzorce	vzorec	k1gInPc4
kapitalistické	kapitalistický	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
kapitalistického	kapitalistický	k2eAgInSc2d1
způsobu	způsob	k1gInSc2
výroby	výroba	k1gFnSc2
a	a	k8xC
tvorbu	tvorba	k1gFnSc4
nadhodnoty	nadhodnota	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Marx	Marx	k1gMnSc1
vnímal	vnímat	k5eAaImAgMnS
problém	problém	k1gInSc4
společnosti	společnost	k1gFnSc2
jako	jako	k9
problém	problém	k1gInSc1
dvou	dva	k4xCgFnPc2
tříd	třída	k1gFnPc2
–	–	k?
vykořisťovatelů	vykořisťovatel	k1gMnPc2
a	a	k8xC
vykořisťovaných	vykořisťovaný	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
klíč	klíč	k1gInSc4
viděl	vidět	k5eAaImAgMnS
zrušení	zrušení	k1gNnSc4
soukromého	soukromý	k2eAgNnSc2d1
vlastnictví	vlastnictví	k1gNnSc2
výrobních	výrobní	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
a	a	k8xC
nastolením	nastolení	k1gNnSc7
beztřídní	beztřídní	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
by	by	kYmCp3nS
bylo	být	k5eAaImAgNnS
docíleno	docílen	k2eAgNnSc1d1
socialistickou	socialistický	k2eAgFnSc7d1
revolucí	revoluce	k1gFnSc7
<g/>
.	.	kIx.
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Za	za	k7c4
prvním	první	k4xOgMnSc6
příklad	příklad	k1gInSc4
socialistické	socialistický	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
Pařížská	pařížský	k2eAgFnSc1d1
komuna	komuna	k1gFnSc1
<g/>
,	,	kIx,
dočasné	dočasný	k2eAgNnSc1d1
zřízení	zřízení	k1gNnSc1
ve	v	k7c6
Francii	Francie	k1gFnSc6
mezi	mezi	k7c7
březnem	březen	k1gInSc7
a	a	k8xC
květnem	květno	k1gNnSc7
1871	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vypukla	vypuknout	k5eAaPmAgFnS
z	z	k7c2
důvodů	důvod	k1gInPc2
špatných	špatný	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
ve	v	k7c6
Francii	Francie	k1gFnSc6
během	během	k7c2
Prusko-francouzské	prusko-francouzský	k2eAgFnSc2d1
války	válka	k1gFnSc2
a	a	k8xC
byla	být	k5eAaImAgFnS
záhy	záhy	k6eAd1
potlačena	potlačit	k5eAaPmNgFnS
francouzskou	francouzský	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3
socialistickou	socialistický	k2eAgFnSc7d1
revolucí	revoluce	k1gFnSc7
byla	být	k5eAaImAgFnS
Velká	velký	k2eAgFnSc1d1
říjnová	říjnový	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
v	v	k7c6
Rusku	Rusko	k1gNnSc6
roku	rok	k1gInSc2
1917	#num#	k4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
svrhla	svrhnout	k5eAaPmAgFnS
tehdejší	tehdejší	k2eAgFnSc4d1
prozatímní	prozatímní	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
a	a	k8xC
nastolila	nastolit	k5eAaPmAgFnS
vládu	vláda	k1gFnSc4
krajně	krajně	k6eAd1
levicových	levicový	k2eAgMnPc2d1
bolševiků	bolševik	k1gMnPc2
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
Vladimirem	Vladimir	k1gMnSc7
Leninem	Lenin	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následovala	následovat	k5eAaImAgFnS
občanská	občanský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
skončila	skončit	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
vítězstvím	vítězství	k1gNnSc7
bolševiků	bolševik	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
této	tento	k3xDgFnSc2
války	válka	k1gFnSc2
byla	být	k5eAaImAgFnS
bolševiky	bolševik	k1gMnPc4
vytvořena	vytvořen	k2eAgFnSc1d1
hospodářská	hospodářský	k2eAgFnSc1d1
politika	politika	k1gFnSc1
tzv.	tzv.	kA
válečného	válečný	k2eAgInSc2d1
komunismu	komunismus	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
zahájil	zahájit	k5eAaPmAgInS
proces	proces	k1gInSc4
nacionalizace	nacionalizace	k1gFnSc2
<g/>
,	,	kIx,
zavedl	zavést	k5eAaPmAgInS
přídělový	přídělový	k2eAgInSc4d1
systém	systém	k1gInSc4
a	a	k8xC
zavedl	zavést	k5eAaPmAgMnS
tzv.	tzv.	kA
prodrazvjorstku	prodrazvjorstka	k1gFnSc4
<g/>
,	,	kIx,
centralizaci	centralizace	k1gFnSc4
zásobování	zásobování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
zásobovat	zásobovat	k5eAaImF
Rudou	rudý	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
jídlem	jídlo	k1gNnSc7
a	a	k8xC
zbraněmi	zbraň	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
politika	politika	k1gFnSc1
byla	být	k5eAaImAgFnS
nahrazena	nahradit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1921	#num#	k4
novou	nový	k2eAgFnSc7d1
ekonomickou	ekonomický	k2eAgFnSc7d1
politikou	politika	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
měla	mít	k5eAaImAgFnS
zemi	zem	k1gFnSc4
dostat	dostat	k5eAaPmF
z	z	k7c2
katastrofální	katastrofální	k2eAgFnSc2d1
ekonomické	ekonomický	k2eAgFnSc2d1
situace	situace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemědělská	zemědělský	k2eAgFnSc1d1
produkce	produkce	k1gFnSc1
se	se	k3xPyFc4
výrazně	výrazně	k6eAd1
zvýšila	zvýšit	k5eAaPmAgFnS
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
rolníci	rolník	k1gMnPc1
mohli	moct	k5eAaImAgMnP
nakládat	nakládat	k5eAaImF
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
produkcí	produkce	k1gFnSc7
podle	podle	k7c2
sebe	se	k3xPyFc2
<g/>
,	,	kIx,
místo	místo	k6eAd1
odevzdávání	odevzdávání	k1gNnSc4
armádě	armáda	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
NEP	Nep	k1gInSc4
tolerovala	tolerovat	k5eAaImAgFnS
limitovanou	limitovaný	k2eAgFnSc4d1
existenci	existence	k1gFnSc4
soukromého	soukromý	k2eAgNnSc2d1
podnikání	podnikání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rolníci	rolník	k1gMnPc1
mohli	moct	k5eAaImAgMnP
své	svůj	k3xOyFgInPc4
výrobky	výrobek	k1gInPc4
prodávat	prodávat	k5eAaImF
a	a	k8xC
dostat	dostat	k5eAaPmF
se	se	k3xPyFc4
tak	tak	k6eAd1
z	z	k7c2
chudoby	chudoba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvorba	tvorba	k1gFnSc1
živností	živnost	k1gFnPc2
byla	být	k5eAaImAgFnS
podporována	podporovat	k5eAaImNgFnS
<g/>
,	,	kIx,
větší	veliký	k2eAgInPc4d2
podniky	podnik	k1gInPc4
byly	být	k5eAaImAgFnP
státem	stát	k1gInSc7
nabízeny	nabízet	k5eAaImNgFnP
do	do	k7c2
pronájmu	pronájem	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
NEP	Nep	k1gInSc1
byla	být	k5eAaImAgFnS
zrušena	zrušit	k5eAaPmNgFnS
po	po	k7c6
příchodu	příchod	k1gInSc6
a	a	k8xC
konsolidaci	konsolidace	k1gFnSc3
moci	moct	k5eAaImF
Stalinem	Stalin	k1gMnSc7
v	v	k7c6
roce	rok	k1gInSc6
1929	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Následně	následně	k6eAd1
začala	začít	k5eAaPmAgFnS
probíhat	probíhat	k5eAaImF
kolektivizace	kolektivizace	k1gFnSc1
<g/>
,	,	kIx,
mnohdy	mnohdy	k6eAd1
až	až	k9
v	v	k7c6
násilné	násilný	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ty	k3xPp2nSc3
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
se	se	k3xPyFc4
bránili	bránit	k5eAaImAgMnP
kolektivizaci	kolektivizace	k1gFnSc3
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
označeni	označit	k5eAaPmNgMnP
za	za	k7c4
kulaky	kulak	k1gMnPc4
a	a	k8xC
velmi	velmi	k6eAd1
často	často	k6eAd1
končili	končit	k5eAaImAgMnP
v	v	k7c6
pracovních	pracovní	k2eAgInPc6d1
táborech	tábor	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
byla	být	k5eAaImAgFnS
sestavena	sestavit	k5eAaPmNgFnS
první	první	k4xOgFnSc1
pětiletka	pětiletka	k1gFnSc1
–	–	k?
hospodářský	hospodářský	k2eAgInSc4d1
plán	plán	k1gInSc4
pro	pro	k7c4
období	období	k1gNnSc4
mezi	mezi	k7c7
lety	let	k1gInPc7
1928	#num#	k4
<g/>
–	–	k?
<g/>
1932	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
se	se	k3xPyFc4
většina	většina	k1gFnSc1
východní	východní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
dostala	dostat	k5eAaPmAgFnS
do	do	k7c2
sovětské	sovětský	k2eAgFnSc2d1
sféry	sféra	k1gFnSc2
vlivu	vliv	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
státech	stát	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
byly	být	k5eAaImAgInP
osvobozeny	osvobodit	k5eAaPmNgInP
Rudou	rudý	k2eAgFnSc7d1
armádu	armáda	k1gFnSc4
<g/>
,	,	kIx,
vládly	vládnout	k5eAaImAgFnP
pro-sovětské	pro-sovětský	k2eAgFnPc4d1
nálady	nálada	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
byl	být	k5eAaImAgInS
vnímán	vnímat	k5eAaImNgInS
jako	jako	k8xS,k8xC
hlavní	hlavní	k2eAgMnSc1d1
porazitel	porazitel	k1gMnSc1
Nacistického	nacistický	k2eAgNnSc2d1
Německa	Německo	k1gNnSc2
a	a	k8xC
po	po	k7c6
krachu	krach	k1gInSc6
na	na	k7c6
newyorské	newyorský	k2eAgFnSc6d1
burze	burza	k1gFnSc6
a	a	k8xC
následné	následný	k2eAgFnSc6d1
hospodářské	hospodářský	k2eAgFnSc6d1
krizi	krize	k1gFnSc6
mnoho	mnoho	k6eAd1
lidí	člověk	k1gMnPc2
ztratilo	ztratit	k5eAaPmAgNnS
víru	víra	k1gFnSc4
v	v	k7c4
kapitalismus	kapitalismus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
vše	všechen	k3xTgNnSc1
přispělo	přispět	k5eAaPmAgNnS
k	k	k7c3
etablování	etablování	k1gNnSc3
komunistických	komunistický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
ve	v	k7c6
východním	východní	k2eAgInSc6d1
bloku	blok	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
Komunisté	komunista	k1gMnPc1
se	se	k3xPyFc4
k	k	k7c3
moci	moc	k1gFnSc3
v	v	k7c6
Československu	Československo	k1gNnSc6
dostali	dostat	k5eAaPmAgMnP
v	v	k7c6
únoru	únor	k1gInSc6
roku	rok	k1gInSc2
1948	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Klement	Klement	k1gMnSc1
Gottwald	Gottwald	k1gMnSc1
provedl	provést	k5eAaPmAgMnS
státní	státní	k2eAgInSc4d1
převrat	převrat	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
velkým	velký	k2eAgFnPc3d1
změnám	změna	k1gFnPc3
v	v	k7c6
hospodářství	hospodářství	k1gNnSc6
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
budováno	budovat	k5eAaImNgNnS
centrálně	centrálně	k6eAd1
po	po	k7c6
vzoru	vzor	k1gInSc6
SSSR	SSSR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1949	#num#	k4
byla	být	k5eAaImAgFnS
vydána	vydán	k2eAgFnSc1d1
první	první	k4xOgFnSc1
pětiletka	pětiletka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzniklo	vzniknout	k5eAaPmAgNnS
Jednotné	jednotný	k2eAgNnSc1d1
zemědělské	zemědělský	k2eAgNnSc1d1
družstvo	družstvo	k1gNnSc1
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
JZD	JZD	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zemědělská	zemědělský	k2eAgFnSc1d1
půda	půda	k1gFnSc1
byla	být	k5eAaImAgFnS
sjednocena	sjednotit	k5eAaPmNgFnS
a	a	k8xC
zespolečněna	zespolečnit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupem	postupem	k7c2
času	čas	k1gInSc2
se	se	k3xPyFc4
mělo	mít	k5eAaImAgNnS
JZD	JZD	kA
podobat	podobat	k5eAaImF
sovětským	sovětský	k2eAgInPc3d1
kolchozům	kolchoz	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolektivizace	kolektivizace	k1gFnSc1
půdy	půda	k1gFnSc2
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k9
v	v	k7c6
Sovětském	sovětský	k2eAgInSc6d1
svazu	svaz	k1gInSc6
<g/>
,	,	kIx,
probíhala	probíhat	k5eAaImAgFnS
mnohdy	mnohdy	k6eAd1
i	i	k9
násilně	násilně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1953	#num#	k4
byla	být	k5eAaImAgFnS
zavedena	zavést	k5eAaPmNgFnS
měnová	měnový	k2eAgFnSc1d1
reforma	reforma	k1gFnSc1
prezidentem	prezident	k1gMnSc7
Antonínem	Antonín	k1gMnSc7
Zápotockým	Zápotocký	k1gMnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
měla	mít	k5eAaImAgFnS
omezit	omezit	k5eAaPmF
černý	černý	k2eAgInSc4d1
trh	trh	k1gInSc4
a	a	k8xC
snížit	snížit	k5eAaPmF
míru	míra	k1gFnSc4
poptávky	poptávka	k1gFnSc2
obyvatelstva	obyvatelstvo	k1gNnSc2
vůči	vůči	k7c3
omezené	omezený	k2eAgFnSc3d1
poválečné	poválečný	k2eAgFnSc3d1
nabídce	nabídka	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
praxi	praxe	k1gFnSc6
ale	ale	k9
znamenala	znamenat	k5eAaImAgNnP
znehodnocení	znehodnocení	k1gNnPc1
úspor	úspora	k1gFnPc2
a	a	k8xC
výrazný	výrazný	k2eAgInSc1d1
pokles	pokles	k1gInSc1
životní	životní	k2eAgFnSc2d1
úrovně	úroveň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1963	#num#	k4
byly	být	k5eAaImAgFnP
přijaty	přijmout	k5eAaPmNgFnP
některá	některý	k3yIgNnPc1
tržní	tržní	k2eAgNnPc1d1
liberalizační	liberalizační	k2eAgNnPc1d1
opatření	opatření	k1gNnPc1
jako	jako	k8xS,k8xC
reakce	reakce	k1gFnPc1
na	na	k7c4
značné	značný	k2eAgInPc4d1
ekonomické	ekonomický	k2eAgInPc4d1
problémy	problém	k1gInPc4
Československa	Československo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvolnila	uvolnit	k5eAaPmAgFnS
se	se	k3xPyFc4
i	i	k9
nálada	nálada	k1gFnSc1
ve	v	k7c6
společnosti	společnost	k1gFnSc6
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vyústilo	vyústit	k5eAaPmAgNnS
v	v	k7c4
Pražské	pražský	k2eAgNnSc4d1
jaro	jaro	k1gNnSc4
a	a	k8xC
následnou	následný	k2eAgFnSc4d1
invazi	invaze	k1gFnSc4
vojsk	vojsko	k1gNnPc2
Varšavské	varšavský	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
21	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1968	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následovalo	následovat	k5eAaImAgNnS
období	období	k1gNnSc1
normalizace	normalizace	k1gFnSc2
a	a	k8xC
zrušeny	zrušit	k5eAaPmNgFnP
byly	být	k5eAaImAgFnP
ekonomické	ekonomický	k2eAgFnPc1d1
reformy	reforma	k1gFnPc1
Pražského	pražský	k2eAgNnSc2d1
jara	jaro	k1gNnSc2
obsažené	obsažený	k2eAgFnPc1d1
v	v	k7c6
Akčním	akční	k2eAgInSc6d1
programu	program	k1gInSc6
KSČ	KSČ	kA
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
snažily	snažit	k5eAaImAgFnP
docílit	docílit	k5eAaPmF
přechodu	přechod	k1gInSc2
z	z	k7c2
centrálně	centrálně	k6eAd1
řízeného	řízený	k2eAgInSc2d1
socialistického	socialistický	k2eAgInSc2d1
ekonomického	ekonomický	k2eAgInSc2d1
systému	systém	k1gInSc2
směrem	směr	k1gInSc7
k	k	k7c3
větší	veliký	k2eAgFnSc3d2
orientaci	orientace	k1gFnSc3
ekonomiky	ekonomika	k1gFnSc2
na	na	k7c4
spotřební	spotřební	k2eAgNnSc4d1
zboží	zboží	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Pád	Pád	k1gInSc1
socialistických	socialistický	k2eAgInPc2d1
ekonomických	ekonomický	k2eAgInPc2d1
systémů	systém	k1gInPc2
</s>
<s>
V	v	k7c6
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
se	se	k3xPyFc4
v	v	k7c6
sovětském	sovětský	k2eAgInSc6d1
bloku	blok	k1gInSc6
naplno	naplno	k6eAd1
projevovaly	projevovat	k5eAaImAgInP
jeho	jeho	k3xOp3gInPc1
problémy	problém	k1gInPc1
<g/>
,	,	kIx,
zejména	zejména	k9
neschopnost	neschopnost	k1gFnSc1
inovovat	inovovat	k5eAaBmF
skrz	skrz	k7c4
kreativní	kreativní	k2eAgFnSc4d1
destrukci	destrukce	k1gFnSc4
a	a	k8xC
držet	držet	k5eAaImF
krok	krok	k1gInSc4
s	s	k7c7
hospodářským	hospodářský	k2eAgInSc7d1
a	a	k8xC
technologickým	technologický	k2eAgInSc7d1
pokrokem	pokrok	k1gInSc7
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Polsku	Polsko	k1gNnSc6
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
vzestupu	vzestup	k1gInSc3
Wałęsovy	Wałęsův	k2eAgFnSc2d1
Solidarity	solidarita	k1gFnSc2
a	a	k8xC
celkovému	celkový	k2eAgInSc3d1
nedostatku	nedostatek	k1gInSc3
spotřebního	spotřební	k2eAgNnSc2d1
zboží	zboží	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1985	#num#	k4
se	se	k3xPyFc4
k	k	k7c3
moci	moc	k1gFnSc3
v	v	k7c6
SSSR	SSSR	kA
dostává	dostávat	k5eAaImIp3nS
Michail	Michail	k1gMnSc1
Gorbačov	Gorbačov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
se	se	k3xPyFc4
pokusil	pokusit	k5eAaPmAgMnS
problémy	problém	k1gInPc4
řešit	řešit	k5eAaImF
reformami	reforma	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvýznamnější	významný	k2eAgFnSc1d3
byla	být	k5eAaImAgFnS
perestrojka	perestrojka	k1gFnSc1
(	(	kIx(
<g/>
přestavba	přestavba	k1gFnSc1
ekonomického	ekonomický	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
glasnosť	glasnosť	k1gFnSc1
(	(	kIx(
<g/>
zrušení	zrušení	k1gNnSc1
cenzury	cenzura	k1gFnSc2
a	a	k8xC
nabádání	nabádání	k1gNnSc2
k	k	k7c3
politické	politický	k2eAgFnSc3d1
otevřenosti	otevřenost	k1gFnSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Reformy	reforma	k1gFnPc1
vedly	vést	k5eAaImAgFnP
k	k	k7c3
dominovému	dominový	k2eAgInSc3d1
pádu	pád	k1gInSc3
komunistických	komunistický	k2eAgInPc2d1
režimů	režim	k1gInPc2
ve	v	k7c6
východním	východní	k2eAgInSc6d1
bloku	blok	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Československu	Československo	k1gNnSc6
pád	pád	k1gInSc4
režimu	režim	k1gInSc2
způsobily	způsobit	k5eAaPmAgFnP
události	událost	k1gFnPc1
17	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
přetvoření	přetvoření	k1gNnSc3
centrálně	centrálně	k6eAd1
řízeného	řízený	k2eAgInSc2d1
socialistického	socialistický	k2eAgInSc2d1
ekonomického	ekonomický	k2eAgInSc2d1
systému	systém	k1gInSc2
a	a	k8xC
k	k	k7c3
privatizaci	privatizace	k1gFnSc3
státního	státní	k2eAgInSc2d1
majetku	majetek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Přechod	přechod	k1gInSc1
k	k	k7c3
tržní	tržní	k2eAgFnSc3d1
ekonomice	ekonomika	k1gFnSc3
</s>
<s>
Tranzitivní	tranzitivní	k2eAgFnSc1d1
ekonomika	ekonomika	k1gFnSc1
</s>
<s>
Tranzitivní	tranzitivní	k2eAgFnSc1d1
neboli	neboli	k8xC
přechodová	přechodový	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
ekonomika	ekonomika	k1gFnSc1
měnící	měnící	k2eAgFnSc1d1
se	se	k3xPyFc4
z	z	k7c2
plánované	plánovaný	k2eAgFnSc2d1
na	na	k7c4
tržní	tržní	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dochází	docházet	k5eAaImIp3nS
k	k	k7c3
ekonomické	ekonomický	k2eAgFnSc3d1
liberalizaci	liberalizace	k1gFnSc3
a	a	k8xC
trh	trh	k1gInSc1
začíná	začínat	k5eAaImIp3nS
vyvářet	vyvářet	k5eAaImF
ceny	cena	k1gFnSc2
sám	sám	k3xTgMnSc1
<g/>
,	,	kIx,
místo	místo	k1gNnSc4
orgánů	orgán	k1gInPc2
plánované	plánovaný	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzniká	vznikat	k5eAaImIp3nS
tlak	tlak	k1gInSc4
na	na	k7c4
privatizaci	privatizace	k1gFnSc4
státem	stát	k1gInSc7
vlastněných	vlastněný	k2eAgFnPc2d1
firem	firma	k1gFnPc2
a	a	k8xC
přírodních	přírodní	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
,	,	kIx,
dále	daleko	k6eAd2
vzniká	vznikat	k5eAaImIp3nS
nový	nový	k2eAgInSc1d1
finanční	finanční	k2eAgInSc1d1
sektor	sektor	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
má	mít	k5eAaImIp3nS
usnadnit	usnadnit	k5eAaPmF
makroekonomickou	makroekonomický	k2eAgFnSc4d1
stabilizaci	stabilizace	k1gFnSc4
a	a	k8xC
kontrolovat	kontrolovat	k5eAaImF
pohyb	pohyb	k1gInSc4
soukromého	soukromý	k2eAgInSc2d1
kapitálu	kapitál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
přechodu	přechod	k1gInSc6
ekonomiky	ekonomika	k1gFnSc2
často	často	k6eAd1
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
řadě	řada	k1gFnSc3
krátkodobých	krátkodobý	k2eAgInPc2d1
negativních	negativní	k2eAgInPc2d1
jevů	jev	k1gInPc2
<g/>
,	,	kIx,
například	například	k6eAd1
k	k	k7c3
nárůstu	nárůst	k1gInSc3
nezaměstnanosti	nezaměstnanost	k1gFnSc2
způsobenému	způsobený	k2eAgInSc3d1
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
nově	nově	k6eAd1
zprivatizované	zprivatizovaný	k2eAgFnPc1d1
společnosti	společnost	k1gFnPc1
začnou	začít	k5eAaPmIp3nP
pracovat	pracovat	k5eAaImF
efektivněji	efektivně	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgInSc7d1
častým	častý	k2eAgInSc7d1
jevem	jev	k1gInSc7
je	být	k5eAaImIp3nS
nárůst	nárůst	k1gInSc1
inflace	inflace	k1gFnSc2
jako	jako	k8xC,k8xS
výsledek	výsledek	k1gInSc1
odstranění	odstranění	k1gNnSc2
státní	státní	k2eAgFnSc2d1
kontroly	kontrola	k1gFnSc2
nad	nad	k7c7
cenami	cena	k1gFnPc7
zboží	zboží	k1gNnSc2
a	a	k8xC
služeb	služba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Transformační	transformační	k2eAgFnSc1d1
strategie	strategie	k1gFnSc1
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgInP
hlavní	hlavní	k2eAgInPc1d1
dva	dva	k4xCgInPc1
přístupy	přístup	k1gInPc1
k	k	k7c3
ideální	ideální	k2eAgFnSc3d1
transformační	transformační	k2eAgFnSc3d1
strategii	strategie	k1gFnSc3
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
na	na	k7c4
podstatu	podstata	k1gFnSc4
a	a	k8xC
rozvoj	rozvoj	k1gInSc4
tržní	tržní	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
nahlíží	nahlížet	k5eAaImIp3nS
odlišně	odlišně	k6eAd1
<g/>
,	,	kIx,
především	především	k6eAd1
se	se	k3xPyFc4
rozchází	rozcházet	k5eAaImIp3nS
v	v	k7c6
názoru	názor	k1gInSc6
na	na	k7c4
ideální	ideální	k2eAgFnSc4d1
rychlost	rychlost	k1gFnSc4
transformace	transformace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Šoková	šokový	k2eAgFnSc1d1
terapie	terapie	k1gFnSc1
</s>
<s>
Šoková	šokový	k2eAgFnSc1d1
terapie	terapie	k1gFnSc1
nebo	nebo	k8xC
‚	‚	k?
<g/>
teorie	teorie	k1gFnPc4
velkého	velký	k2eAgInSc2d1
třesku	třesk	k1gInSc2
<g/>
‘	‘	k?
podporuje	podporovat	k5eAaImIp3nS
nejrychlejší	rychlý	k2eAgInSc4d3
možný	možný	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
tržního	tržní	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přiznává	přiznávat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
nemůžeme	moct	k5eNaImIp1nP
všechno	všechen	k3xTgNnSc4
dosáhnout	dosáhnout	k5eAaPmF
okamžitě	okamžitě	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
prosazuje	prosazovat	k5eAaImIp3nS
takový	takový	k3xDgInSc4
postoj	postoj	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
provést	provést	k5eAaPmF
privatizaci	privatizace	k1gFnSc4
tak	tak	k9
rychle	rychle	k6eAd1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
jen	jen	k9
možné	možný	k2eAgNnSc1d1
a	a	k8xC
pak	pak	k6eAd1
přejít	přejít	k5eAaPmF
k	k	k7c3
dalším	další	k2eAgInPc3d1
krokům	krok	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
přístup	přístup	k1gInSc4
počítá	počítat	k5eAaImIp3nS
s	s	k7c7
jednoduchostí	jednoduchost	k1gFnSc7
tržního	tržní	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
vytvořen	vytvořit	k5eAaPmNgInS
rychle	rychle	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
svého	svůj	k3xOyFgInSc2
vzniku	vznik	k1gInSc2
teorie	teorie	k1gFnSc1
korespondovala	korespondovat	k5eAaImAgFnS
s	s	k7c7
postoji	postoj	k1gInPc7
Mezinárodního	mezinárodní	k2eAgInSc2d1
měnového	měnový	k2eAgInSc2d1
fondu	fond	k1gInSc2
a	a	k8xC
Světové	světový	k2eAgFnSc2d1
banky	banka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Gradualismus	Gradualismus	k1gInSc1
</s>
<s>
Gradualismus	Gradualismus	k1gInSc4
nelze	lze	k6eNd1
označit	označit	k5eAaPmF
za	za	k7c4
úplně	úplně	k6eAd1
opačnou	opačný	k2eAgFnSc4d1
tendenci	tendence	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
zastává	zastávat	k5eAaImIp3nS
pomalejší	pomalý	k2eAgInSc4d2
postup	postup	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obává	obávat	k5eAaImIp3nS
se	se	k3xPyFc4
možných	možný	k2eAgInPc2d1
důsledků	důsledek	k1gInPc2
šokové	šokový	k2eAgFnSc2d1
terapie	terapie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládá	předpokládat	k5eAaImIp3nS
aktivní	aktivní	k2eAgFnSc1d1
účast	účast	k1gFnSc1
státu	stát	k1gInSc2
při	při	k7c6
restrukturalizaci	restrukturalizace	k1gFnSc6
firem	firma	k1gFnPc2
před	před	k7c7
privatizací	privatizace	k1gFnSc7
a	a	k8xC
institucionální	institucionální	k2eAgNnSc1d1
zastřešení	zastřešení	k1gNnSc1
transformace	transformace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Washingtonský	washingtonský	k2eAgInSc1d1
konsenzus	konsenzus	k1gInSc1
</s>
<s>
Strategie	strategie	k1gFnSc1
Washingtonského	washingtonský	k2eAgInSc2d1
konsenzu	konsenz	k1gInSc2
byla	být	k5eAaImAgFnS
podporována	podporovat	k5eAaImNgFnS
mezinárodními	mezinárodní	k2eAgFnPc7d1
institucemi	instituce	k1gFnPc7
a	a	k8xC
týmy	tým	k1gInPc1
zahraničních	zahraniční	k2eAgMnPc2d1
poradců	poradce	k1gMnPc2
<g/>
,	,	kIx,
šlo	jít	k5eAaImAgNnS
o	o	k7c6
strategii	strategie	k1gFnSc6
pro	pro	k7c4
tranzitivní	tranzitivní	k2eAgFnPc4d1
a	a	k8xC
rozvojové	rozvojový	k2eAgFnPc4d1
ekonomiky	ekonomika	k1gFnPc4
zemí	zem	k1gFnPc2
střední	střední	k2eAgFnSc2d1
i	i	k8xC
východní	východní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
Ruska	Rusko	k1gNnSc2
i	i	k8xC
Asie	Asie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Princip	princip	k1gInSc1
spočíval	spočívat	k5eAaImAgInS
ve	v	k7c6
vytvoření	vytvoření	k1gNnSc6
moderního	moderní	k2eAgInSc2d1
kapitalistického	kapitalistický	k2eAgInSc2d1
systému	systém	k1gInSc2
inspirovaného	inspirovaný	k2eAgInSc2d1
již	již	k6eAd1
existujícími	existující	k2eAgFnPc7d1
ekonomikami	ekonomika	k1gFnPc7
ve	v	k7c6
vyspělejších	vyspělý	k2eAgFnPc6d2
zemích	zem	k1gFnPc6
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc7d1
myšlenkou	myšlenka	k1gFnSc7
bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
si	se	k3xPyFc3
země	země	k1gFnSc1
s	s	k7c7
tranzitivní	tranzitivní	k2eAgFnSc7d1
ekonomikou	ekonomika	k1gFnSc7
nechaly	nechat	k5eAaPmAgFnP
od	od	k7c2
zkušených	zkušený	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
poradit	poradit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
strategie	strategie	k1gFnPc4
byla	být	k5eAaImAgFnS
formována	formovat	k5eAaImNgFnS
v	v	k7c6
duchu	duch	k1gMnSc6
neoliberálního	neoliberální	k2eAgInSc2d1
přístupu	přístup	k1gInSc2
k	k	k7c3
ekonomické	ekonomický	k2eAgFnSc3d1
transformaci	transformace	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
vlády	vláda	k1gFnSc2
USA	USA	kA
a	a	k8xC
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
založily	založit	k5eAaPmAgFnP
svou	svůj	k3xOyFgFnSc4
politiku	politika	k1gFnSc4
na	na	k7c6
výhodách	výhoda	k1gFnPc6
deregulace	deregulace	k1gFnSc2
<g/>
,	,	kIx,
privatizace	privatizace	k1gFnSc2
<g/>
,	,	kIx,
upřednostňovaného	upřednostňovaný	k2eAgNnSc2d1
využívání	využívání	k1gNnSc2
tržních	tržní	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
<g/>
,	,	kIx,
minimalizace	minimalizace	k1gFnSc1
státních	státní	k2eAgInPc2d1
zásahů	zásah	k1gInPc2
a	a	k8xC
snížení	snížení	k1gNnSc4
úrovně	úroveň	k1gFnSc2
zdanění	zdanění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Problém	problém	k1gInSc1
se	se	k3xPyFc4
vyskytl	vyskytnout	k5eAaPmAgInS
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
přechod	přechod	k1gInSc1
ze	z	k7c2
státem	stát	k1gInSc7
řízeného	řízený	k2eAgInSc2d1
socialistického	socialistický	k2eAgInSc2d1
systému	systém	k1gInSc2
byl	být	k5eAaImAgInS
značně	značně	k6eAd1
odlišný	odlišný	k2eAgInSc1d1
od	od	k7c2
úrovně	úroveň	k1gFnSc2
deregulace	deregulace	k1gFnSc2
ve	v	k7c6
vyspělých	vyspělý	k2eAgFnPc6d1
ekonomikách	ekonomika	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Státy	stát	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
snažily	snažit	k5eAaImAgFnP
přejít	přejít	k5eAaPmF
z	z	k7c2
plánované	plánovaný	k2eAgFnSc2d1
socialistické	socialistický	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
na	na	k7c4
tržní	tržní	k2eAgFnPc4d1
<g/>
,	,	kIx,
musely	muset	k5eAaImAgInP
nejen	nejen	k6eAd1
tvořit	tvořit	k5eAaImF
nová	nový	k2eAgNnPc4d1
pravidla	pravidlo	k1gNnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
rušit	rušit	k5eAaImF
ta	ten	k3xDgFnSc1
stávající	stávající	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Termín	termín	k1gInSc1
Washingtonský	washingtonský	k2eAgInSc4d1
konsenzus	konsenzus	k1gInSc4
poprvé	poprvé	k6eAd1
použil	použít	k5eAaPmAgMnS
John	John	k1gMnSc1
Williamson	Williamson	k1gMnSc1
pro	pro	k7c4
označení	označení	k1gNnSc4
typických	typický	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
finanční	finanční	k2eAgFnSc2d1
pomoci	pomoc	k1gFnSc2
stanovených	stanovený	k2eAgFnPc2d1
Mezinárodním	mezinárodní	k2eAgInSc7d1
Měnovým	měnový	k2eAgInSc7d1
Fondem	fond	k1gInSc7
pro	pro	k7c4
země	zem	k1gFnPc4
Latinské	latinský	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
také	také	k9
o	o	k7c4
doporučení	doporučení	k1gNnSc4
pro	pro	k7c4
politiku	politika	k1gFnSc4
dané	daný	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Williamson	Williamson	k1gInSc1
tato	tento	k3xDgNnPc4
doporučení	doporučení	k1gNnPc4
shrnul	shrnout	k5eAaPmAgMnS
do	do	k7c2
deseti	deset	k4xCc2
bodů	bod	k1gInPc2
a	a	k8xC
dal	dát	k5eAaPmAgMnS
jim	on	k3xPp3gFnPc3
název	název	k1gInSc1
‚	‚	k?
<g/>
Obezřetná	obezřetný	k2eAgFnSc1d1
makroekonomická	makroekonomický	k2eAgFnSc1d1
politika	politika	k1gFnSc1
<g/>
,	,	kIx,
vnější	vnější	k2eAgFnSc1d1
orientace	orientace	k1gFnSc1
a	a	k8xC
kapitalismus	kapitalismus	k1gInSc4
volného	volný	k2eAgInSc2d1
trhu	trh	k1gInSc2
<g/>
.	.	kIx.
<g/>
‘	‘	k?
Principem	princip	k1gInSc7
byla	být	k5eAaImAgFnS
především	především	k9
stabilita	stabilita	k1gFnSc1
v	v	k7c6
makroekonomice	makroekonomika	k1gFnSc6
a	a	k8xC
liberalizace	liberalizace	k1gFnSc2
v	v	k7c6
mikroekonomice	mikroekonomika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Body	bod	k1gInPc1
‚	‚	k?
<g/>
Obezřetné	obezřetný	k2eAgFnSc2d1
makroekonomické	makroekonomický	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
<g/>
,	,	kIx,
vnější	vnější	k2eAgFnSc2d1
orientace	orientace	k1gFnSc2
a	a	k8xC
kapitalismu	kapitalismus	k1gInSc2
volného	volný	k2eAgInSc2d1
trhu	trh	k1gInSc2
<g/>
‘	‘	k?
<g/>
:	:	kIx,
</s>
<s>
Disciplína	disciplína	k1gFnSc1
ve	v	k7c6
fiskální	fiskální	k2eAgFnSc6d1
politice	politika	k1gFnSc6
</s>
<s>
Přesměrování	přesměrování	k1gNnSc1
veřejných	veřejný	k2eAgInPc2d1
výdajů	výdaj	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
méně	málo	k6eAd2
směřovat	směřovat	k5eAaImF
do	do	k7c2
oblasti	oblast	k1gFnSc2
dotací	dotace	k1gFnPc2
a	a	k8xC
více	hodně	k6eAd2
do	do	k7c2
sféry	sféra	k1gFnSc2
vzdělávání	vzdělávání	k1gNnSc2
<g/>
,	,	kIx,
zdravotní	zdravotní	k2eAgFnSc2d1
péče	péče	k1gFnSc2
a	a	k8xC
rozvoje	rozvoj	k1gInSc2
infrastruktury	infrastruktura	k1gFnSc2
</s>
<s>
Daňová	daňový	k2eAgFnSc1d1
reforma	reforma	k1gFnSc1
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gInSc7
cílem	cíl	k1gInSc7
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
rozšíření	rozšíření	k1gNnSc4
daňové	daňový	k2eAgFnSc2d1
základny	základna	k1gFnSc2
a	a	k8xC
snížení	snížení	k1gNnSc2
vysokých	vysoký	k2eAgFnPc2d1
marginálních	marginální	k2eAgFnPc2d1
daňových	daňový	k2eAgFnPc2d1
sazeb	sazba	k1gFnPc2
</s>
<s>
Úrokové	úrokový	k2eAgFnPc1d1
sazby	sazba	k1gFnPc1
by	by	kYmCp3nP
měly	mít	k5eAaImAgFnP
být	být	k5eAaImF
určovány	určovat	k5eAaImNgInP
trhem	trh	k1gInSc7
a	a	k8xC
reálná	reálný	k2eAgFnSc1d1
úroková	úrokový	k2eAgFnSc1d1
míra	míra	k1gFnSc1
by	by	kYmCp3nS
měla	mít	k5eAaImAgFnS
nabývat	nabývat	k5eAaImF
kladných	kladný	k2eAgFnPc2d1
hodnot	hodnota	k1gFnPc2
</s>
<s>
Konkurenceschopné	konkurenceschopný	k2eAgInPc1d1
měnové	měnový	k2eAgInPc1d1
kurzy	kurz	k1gInPc1
</s>
<s>
Liberalizace	liberalizace	k1gFnSc1
zahraničního	zahraniční	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
bez	bez	k7c2
ochranných	ochranný	k2eAgNnPc2d1
opatření	opatření	k1gNnPc2
a	a	k8xC
v	v	k7c6
prostředí	prostředí	k1gNnSc6
relativně	relativně	k6eAd1
jednotných	jednotný	k2eAgInPc2d1
tarifů	tarif	k1gInPc2
</s>
<s>
Liberalizace	liberalizace	k1gFnSc1
zahraničních	zahraniční	k2eAgFnPc2d1
přímých	přímý	k2eAgFnPc2d1
investic	investice	k1gFnPc2
</s>
<s>
Privatizace	privatizace	k1gFnSc1
státem	stát	k1gInSc7
vlastněných	vlastněný	k2eAgInPc2d1
podniků	podnik	k1gInPc2
</s>
<s>
Deregulace	deregulace	k1gFnSc1
s	s	k7c7
přiměřeným	přiměřený	k2eAgInSc7d1
dohledem	dohled	k1gInSc7
na	na	k7c4
finanční	finanční	k2eAgFnPc4d1
instituce	instituce	k1gFnPc4
</s>
<s>
Zákonné	zákonný	k2eAgNnSc1d1
zajištění	zajištění	k1gNnSc1
vlastnických	vlastnický	k2eAgNnPc2d1
práv	právo	k1gNnPc2
</s>
<s>
Na	na	k7c6
seznamu	seznam	k1gInSc6
podmínek	podmínka	k1gFnPc2
byla	být	k5eAaImAgFnS
také	také	k9
privatizace	privatizace	k1gFnSc1
<g/>
,	,	kIx,
ve	v	k7c6
Williamsonově	Williamsonův	k2eAgInSc6d1
výčtu	výčet	k1gInSc6
byla	být	k5eAaImAgFnS
sice	sice	k8xC
zmíněna	zmíněn	k2eAgFnSc1d1
jen	jen	k9
okrajově	okrajově	k6eAd1
<g/>
,	,	kIx,
v	v	k7c6
transformačních	transformační	k2eAgFnPc6d1
strategiích	strategie	k1gFnPc6
však	však	k9
měla	mít	k5eAaImAgFnS
dále	daleko	k6eAd2
podstatný	podstatný	k2eAgInSc4d1
význam	význam	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Postupně	postupně	k6eAd1
vzniklo	vzniknout	k5eAaPmAgNnS
několik	několik	k4yIc4
verzí	verze	k1gFnPc2
Washingtonského	washingtonský	k2eAgInSc2d1
konsenzu	konsenz	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
různých	různý	k2eAgFnPc6d1
verzích	verze	k1gFnPc6
se	se	k3xPyFc4
pracovalo	pracovat	k5eAaImAgNnS
se	s	k7c7
slučováním	slučování	k1gNnSc7
tradičního	tradiční	k2eAgNnSc2d1
názorového	názorový	k2eAgNnSc2d1
zaměření	zaměření	k1gNnSc2
Mezinárodního	mezinárodní	k2eAgInSc2d1
Měnového	měnový	k2eAgInSc2d1
Fondu	fond	k1gInSc2
a	a	k8xC
prvních	první	k4xOgFnPc2
zkušeností	zkušenost	k1gFnPc2
s	s	k7c7
aplikací	aplikace	k1gFnSc7
konsenzu	konsenz	k1gInSc2
v	v	k7c6
Polsku	Polsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důležitými	důležitý	k2eAgFnPc7d1
osobnostmi	osobnost	k1gFnPc7
byli	být	k5eAaImAgMnP
Stanley	Stanlea	k1gFnPc1
Fischer	Fischer	k1gMnSc1
<g/>
,	,	kIx,
hlavní	hlavní	k2eAgMnSc1d1
ekonom	ekonom	k1gMnSc1
Světové	světový	k2eAgFnSc2d1
banky	banka	k1gFnSc2
a	a	k8xC
Alan	Alan	k1gMnSc1
Gelb	Gelb	k1gMnSc1
<g/>
,	,	kIx,
vedoucí	vedoucí	k1gMnSc1
výzkumu	výzkum	k1gInSc2
ve	v	k7c6
Světové	světový	k2eAgFnSc6d1
bance	banka	k1gFnSc6
o	o	k7c6
tranzitivních	tranzitivní	k2eAgFnPc6d1
ekonomikách	ekonomika	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Ekonomové	ekonom	k1gMnPc1
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
mohli	moct	k5eAaImAgMnP
konzultovat	konzultovat	k5eAaImF
situaci	situace	k1gFnSc4
v	v	k7c6
zemi	zem	k1gFnSc6
s	s	k7c7
ekonomy	ekonom	k1gMnPc7
mezinárodních	mezinárodní	k2eAgFnPc2d1
institucí	instituce	k1gFnPc2
<g/>
,	,	kIx,
konzultace	konzultace	k1gFnPc1
byly	být	k5eAaImAgFnP
samostatné	samostatný	k2eAgFnPc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
situace	situace	k1gFnSc1
a	a	k8xC
postup	postup	k1gInSc1
se	se	k3xPyFc4
lišily	lišit	k5eAaImAgFnP
pro	pro	k7c4
každou	každý	k3xTgFnSc4
zemi	zem	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Přechod	přechod	k1gInSc1
z	z	k7c2
plánované	plánovaný	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
na	na	k7c4
tržní	tržní	k2eAgFnSc4d1
v	v	k7c6
ČSR	ČSR	kA
</s>
<s>
Jelikož	jelikož	k8xS
tržní	tržní	k2eAgFnSc1d1
ekonomika	ekonomika	k1gFnSc1
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgInSc7
ze	z	k7c2
znaků	znak	k1gInPc2
demokratického	demokratický	k2eAgNnSc2d1
zřízení	zřízení	k1gNnSc2
státu	stát	k1gInSc2
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
po	po	k7c6
změně	změna	k1gFnSc6
režimu	režim	k1gInSc2
také	také	k9
ke	k	k7c3
změně	změna	k1gFnSc3
ekonomického	ekonomický	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Československu	Československo	k1gNnSc6
byla	být	k5eAaImAgFnS
využita	využít	k5eAaPmNgFnS
teorie	teorie	k1gFnSc1
šokové	šokový	k2eAgFnSc2d1
terapie	terapie	k1gFnSc2
a	a	k8xC
byl	být	k5eAaImAgInS
tedy	tedy	k9
kladen	kladen	k2eAgInSc1d1
důraz	důraz	k1gInSc1
na	na	k7c4
urychlenou	urychlený	k2eAgFnSc4d1
privatizaci	privatizace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Restrukturalizace	restrukturalizace	k1gFnSc1
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
podle	podle	k7c2
této	tento	k3xDgFnSc2
teorie	teorie	k1gFnSc2
již	již	k9
prací	práce	k1gFnSc7
nových	nový	k2eAgMnPc2d1
soukromých	soukromý	k2eAgMnPc2d1
vlastníků	vlastník	k1gMnPc2
firem	firma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Kupónová	kupónový	k2eAgFnSc1d1
privatizace	privatizace	k1gFnSc1
</s>
<s>
Privatizace	privatizace	k1gFnSc1
po	po	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
byla	být	k5eAaImAgFnS
v	v	k7c6
Československu	Československo	k1gNnSc6
a	a	k8xC
později	pozdě	k6eAd2
už	už	k6eAd1
pouze	pouze	k6eAd1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
uskutečněna	uskutečnit	k5eAaPmNgFnS
prostřednictvím	prostřednictví	k1gNnSc7
Kupónové	kupónový	k2eAgFnSc2d1
privatizace	privatizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
tohoto	tento	k3xDgInSc2
procesu	proces	k1gInSc2
bylo	být	k5eAaImAgNnS
1664	#num#	k4
státních	státní	k2eAgFnPc2d1
firem	firma	k1gFnPc2
přetvořeno	přetvořit	k5eAaPmNgNnS
na	na	k7c6
akciové	akciový	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akcie	akcie	k1gFnSc1
byly	být	k5eAaImAgFnP
prodávány	prodávat	k5eAaImNgFnP
ve	v	k7c6
formě	forma	k1gFnSc6
kupónů	kupón	k1gInPc2
občanům	občan	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kupónová	kupónový	k2eAgFnSc1d1
privatizace	privatizace	k1gFnSc1
probíhala	probíhat	k5eAaImAgFnS
v	v	k7c6
několika	několik	k4yIc6
vlnách	vlna	k1gFnPc6
od	od	k7c2
roku	rok	k1gInSc2
1991	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Privatizace	privatizace	k1gFnSc1
byla	být	k5eAaImAgFnS
kritizována	kritizovat	k5eAaImNgFnS
kvůli	kvůli	k7c3
špatné	špatný	k2eAgFnSc3d1
<g/>
,	,	kIx,
či	či	k8xC
neexistující	existující	k2eNgFnSc4d1
evidenci	evidence	k1gFnSc4
hospodaření	hospodaření	k1gNnSc2
se	s	k7c7
státním	státní	k2eAgInSc7d1
majetkem	majetek	k1gInSc7
během	během	k7c2
tzv.	tzv.	kA
předprivatizační	předprivatizační	k2eAgFnSc1d1
agónie	agónie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Problematická	problematický	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
také	také	k9
rychlost	rychlost	k1gFnSc1
privatizace	privatizace	k1gFnSc1
<g/>
,	,	kIx,
protože	protože	k8xS
měla	mít	k5eAaImAgFnS
za	za	k7c4
následek	následek	k1gInSc4
absenci	absence	k1gFnSc4
vštěpování	vštěpování	k1gNnSc3
etických	etický	k2eAgFnPc2d1
norem	norma	k1gFnPc2
nezkušeným	zkušený	k2eNgMnPc3d1
účastníkům	účastník	k1gMnPc3
procesu	proces	k1gInSc2
privatizace	privatizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
MARX	Marx	k1gMnSc1
<g/>
,	,	kIx,
Karl	Karl	k1gMnSc1
<g/>
,	,	kIx,
ENGELS	Engels	k1gMnSc1
<g/>
,	,	kIx,
Friedrich	Friedrich	k1gMnSc1
<g/>
,	,	kIx,
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Das	Das	k1gMnSc1
Kapital	Kapital	k1gMnSc1
<g/>
:	:	kIx,
Kritik	kritik	k1gMnSc1
der	drát	k5eAaImRp2nS
politischen	politischna	k1gFnPc2
Okonomie	Okonomie	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Berlin	berlina	k1gFnPc2
<g/>
:	:	kIx,
J.H.W.	J.H.W.	k1gMnSc1
Dietz	Dietz	k1gMnSc1
Nachf	Nachf	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
1948	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
MARX	Marx	k1gMnSc1
<g/>
,	,	kIx,
Karl	Karl	k1gMnSc1
<g/>
,	,	kIx,
ENGELS	Engels	k1gMnSc1
<g/>
,	,	kIx,
Friedrich	Friedrich	k1gMnSc1
<g/>
,	,	kIx,
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Das	Das	k1gMnSc1
Kapital	Kapital	k1gMnSc1
<g/>
:	:	kIx,
Kritik	kritik	k1gMnSc1
der	drát	k5eAaImRp2nS
politischen	politischna	k1gFnPc2
Okonomie	Okonomie	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Berlin	berlina	k1gFnPc2
<g/>
:	:	kIx,
J.H.W.	J.H.W.	k1gMnSc1
Dietz	Dietz	k1gMnSc1
Nachf	Nachf	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
1948	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
MYANT	MYANT	kA
<g/>
,	,	kIx,
M.	M.	kA
R.	R.	kA
Tranzitivní	tranzitivní	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
<g/>
:	:	kIx,
politická	politický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
Ruska	Rusko	k1gNnSc2
<g/>
,	,	kIx,
východní	východní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
a	a	k8xC
střední	střední	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
XXI	XXI	kA
<g/>
.	.	kIx.
století	století	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
2268	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Válečný	válečný	k2eAgInSc1d1
komunismus	komunismus	k1gInSc1
–	–	k?
represivní	represivní	k2eAgInSc1d1
systém	systém	k1gInSc1
sov	sova	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruska	Ruska	k1gFnSc1
–	–	k?
Jiří	Jiří	k1gMnSc1
SEKYRA	Sekyra	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysokoškolské	vysokoškolský	k2eAgFnPc1d1
kvalifikační	kvalifikační	k2eAgFnPc1d1
práce	práce	k1gFnPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
https://theses.cz/id/nzl6mq?lang=cs	https://theses.cz/id/nzl6mq?lang=cs	k6eAd1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1
komunistického	komunistický	k2eAgNnSc2d1
Československa	Československo	k1gNnSc2
</s>
