<s>
Sval	sval	k1gInSc1	sval
(	(	kIx(	(
<g/>
musculus	musculus	k1gInSc1	musculus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
také	také	k9	také
svalovina	svalovina	k1gFnSc1	svalovina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
orgán	orgán	k1gInSc1	orgán
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
funkcí	funkce	k1gFnSc7	funkce
je	být	k5eAaImIp3nS	být
umožnění	umožnění	k1gNnSc1	umožnění
aktivního	aktivní	k2eAgInSc2d1	aktivní
pohybu	pohyb	k1gInSc2	pohyb
živočicha	živočich	k1gMnSc2	živočich
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gFnSc2	jeho
části	část	k1gFnSc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Sval	sval	k1gInSc1	sval
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
především	především	k9	především
svalovou	svalový	k2eAgFnSc7d1	svalová
tkání	tkáň	k1gFnSc7	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
svaly	sval	k1gInPc1	sval
tvoří	tvořit	k5eAaImIp3nP	tvořit
svalovou	svalový	k2eAgFnSc4d1	svalová
soustavu	soustava	k1gFnSc4	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
svaly	sval	k1gInPc1	sval
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
myologie	myologie	k1gFnSc1	myologie
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
všichni	všechen	k3xTgMnPc1	všechen
živočichové	živočich	k1gMnPc1	živočich
ale	ale	k8xC	ale
mají	mít	k5eAaImIp3nP	mít
svaly	sval	k1gInPc4	sval
<g/>
.	.	kIx.	.
</s>
<s>
Svaly	sval	k1gInPc1	sval
mají	mít	k5eAaImIp3nP	mít
schopnost	schopnost	k1gFnSc4	schopnost
přeměnit	přeměnit	k5eAaPmF	přeměnit
chemickou	chemický	k2eAgFnSc4d1	chemická
energii	energie	k1gFnSc4	energie
živin	živina	k1gFnPc2	živina
v	v	k7c4	v
kinetickou	kinetický	k2eAgFnSc4d1	kinetická
energii	energie	k1gFnSc4	energie
(	(	kIx(	(
<g/>
účinnost	účinnost	k1gFnSc1	účinnost
0	[number]	k4	0
<g/>
,	,	kIx,	,
2	[number]	k4	2
)	)	kIx)	)
a	a	k8xC	a
teplo	teplo	k1gNnSc4	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
vlastností	vlastnost	k1gFnSc7	vlastnost
svalové	svalový	k2eAgFnSc2d1	svalová
tkáně	tkáň	k1gFnSc2	tkáň
je	být	k5eAaImIp3nS	být
schopnost	schopnost	k1gFnSc4	schopnost
se	se	k3xPyFc4	se
stahovat	stahovat	k5eAaImF	stahovat
(	(	kIx(	(
<g/>
kontrahovat	kontrahovat	k5eAaBmF	kontrahovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
umožněno	umožnit	k5eAaPmNgNnS	umožnit
speciálními	speciální	k2eAgFnPc7d1	speciální
mnohojadernými	mnohojaderný	k2eAgFnPc7d1	mnohojaderná
vláknitými	vláknitý	k2eAgFnPc7d1	vláknitá
strukturami	struktura	k1gFnPc7	struktura
uloženými	uložený	k2eAgFnPc7d1	uložená
v	v	k7c6	v
cytoplazmě	cytoplazma	k1gFnSc6	cytoplazma
všech	všecek	k3xTgFnPc2	všecek
svalových	svalový	k2eAgFnPc2d1	svalová
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
myofibrilami	myofibrila	k1gFnPc7	myofibrila
<g/>
.	.	kIx.	.
</s>
<s>
Myofibrily	Myofibrila	k1gFnPc1	Myofibrila
jsou	být	k5eAaImIp3nP	být
složeny	složit	k5eAaPmNgFnP	složit
z	z	k7c2	z
uspořádaných	uspořádaný	k2eAgFnPc2d1	uspořádaná
molekul	molekula	k1gFnPc2	molekula
aktinu	aktin	k1gInSc2	aktin
a	a	k8xC	a
myosinu	myosina	k1gFnSc4	myosina
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
průměr	průměr	k1gInSc4	průměr
20	[number]	k4	20
až	až	k9	až
150	[number]	k4	150
mikrometrů	mikrometr	k1gInPc2	mikrometr
a	a	k8xC	a
délku	délka	k1gFnSc4	délka
0,5	[number]	k4	0,5
až	až	k9	až
20	[number]	k4	20
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
koncentrace	koncentrace	k1gFnSc2	koncentrace
vápenatých	vápenatý	k2eAgInPc2d1	vápenatý
iontů	ion	k1gInPc2	ion
a	a	k8xC	a
přítomnosti	přítomnost	k1gFnSc2	přítomnost
ATP	atp	kA	atp
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
zasouvání	zasouvání	k1gNnSc3	zasouvání
tenkých	tenký	k2eAgFnPc2d1	tenká
aktinových	aktinová	k1gFnPc2	aktinová
vláken	vlákno	k1gNnPc2	vlákno
mezi	mezi	k7c7	mezi
tlustá	tlustý	k2eAgNnPc1d1	tlusté
vlákna	vlákno	k1gNnPc1	vlákno
myosinová	myosinová	k1gFnSc1	myosinová
<g/>
,	,	kIx,	,
myofibrila	myofibrila	k1gFnSc1	myofibrila
se	se	k3xPyFc4	se
zkrátí	zkrátit	k5eAaPmIp3nS	zkrátit
a	a	k8xC	a
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
kontrakci	kontrakce	k1gFnSc3	kontrakce
<g/>
.	.	kIx.	.
</s>
<s>
Stah	stah	k1gInSc1	stah
svalu	sval	k1gInSc2	sval
je	být	k5eAaImIp3nS	být
podkladem	podklad	k1gInSc7	podklad
pro	pro	k7c4	pro
veškerý	veškerý	k3xTgInSc4	veškerý
svalový	svalový	k2eAgInSc4d1	svalový
pohyb	pohyb	k1gInSc4	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Svalový	svalový	k2eAgInSc1d1	svalový
stah	stah	k1gInSc1	stah
je	být	k5eAaImIp3nS	být
důsledkem	důsledek	k1gInSc7	důsledek
řetězce	řetězec	k1gInSc2	řetězec
chemických	chemický	k2eAgFnPc2d1	chemická
reakcí	reakce	k1gFnPc2	reakce
<g/>
,	,	kIx,	,
k	k	k7c3	k
jejichž	jejichž	k3xOyRp3gNnSc3	jejichž
proběhnutí	proběhnutí	k1gNnSc3	proběhnutí
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
splnění	splnění	k1gNnSc2	splnění
několika	několik	k4yIc2	několik
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
,	,	kIx,	,
v	v	k7c6	v
první	první	k4xOgFnSc6	první
řadě	řada	k1gFnSc6	řada
podráždění	podráždění	k1gNnSc2	podráždění
svalu	sval	k1gInSc2	sval
a	a	k8xC	a
dostatečná	dostatečný	k2eAgFnSc1d1	dostatečná
zásoba	zásoba	k1gFnSc1	zásoba
energie	energie	k1gFnSc2	energie
ve	v	k7c6	v
svalu	sval	k1gInSc6	sval
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
duhovce	duhovka	k1gFnSc6	duhovka
oka	oko	k1gNnSc2	oko
můžeme	moct	k5eAaImIp1nP	moct
nalézt	nalézt	k5eAaBmF	nalézt
hladké	hladký	k2eAgInPc4d1	hladký
svaly	sval	k1gInPc4	sval
které	který	k3yRgFnPc4	který
při	při	k7c6	při
větším	veliký	k2eAgNnSc6d2	veliký
množství	množství	k1gNnSc6	množství
světla	světlo	k1gNnSc2	světlo
zmenší	zmenšit	k5eAaPmIp3nP	zmenšit
zorničku	zornička	k1gFnSc4	zornička
a	a	k8xC	a
pří	přít	k5eAaImIp3nS	přít
jeho	on	k3xPp3gInSc2	on
nedostatku	nedostatek	k1gInSc2	nedostatek
ji	on	k3xPp3gFnSc4	on
zase	zase	k9	zase
roztáhnou	roztáhnout	k5eAaPmIp3nP	roztáhnout
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
do	do	k7c2	do
oka	oko	k1gNnSc2	oko
proniká	pronikat	k5eAaImIp3nS	pronikat
ideální	ideální	k2eAgNnSc4d1	ideální
množství	množství	k1gNnSc4	množství
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Vzpřimovač	vzpřimovač	k1gInSc1	vzpřimovač
chlupu	chlup	k1gInSc2	chlup
je	být	k5eAaImIp3nS	být
sval	sval	k1gInSc1	sval
v	v	k7c6	v
kůži	kůže	k1gFnSc6	kůže
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
po	po	k7c6	po
podráždění	podráždění	k1gNnSc6	podráždění
vzpřímí	vzpřímit	k5eAaPmIp3nS	vzpřímit
chlup	chlup	k1gInSc1	chlup
<g/>
.	.	kIx.	.
</s>
<s>
Signálem	signál	k1gInSc7	signál
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
stahu	stah	k1gInSc3	stah
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pocit	pocit	k1gInSc4	pocit
chladu	chlad	k1gInSc2	chlad
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
husí	husí	k2eAgFnSc1d1	husí
kůže	kůže	k1gFnSc1	kůže
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzpřímením	vzpřímení	k1gNnSc7	vzpřímení
chlupů	chlup	k1gInPc2	chlup
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
zlepšení	zlepšení	k1gNnSc3	zlepšení
izolační	izolační	k2eAgFnSc2d1	izolační
schopnosti	schopnost	k1gFnSc2	schopnost
chlupového	chlupový	k2eAgInSc2d1	chlupový
pokryvu	pokryv	k1gInSc2	pokryv
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
chlupy	chlup	k1gInPc1	chlup
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vzpřímit	vzpřímit	k5eAaPmF	vzpřímit
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
stresu	stres	k1gInSc2	stres
(	(	kIx(	(
<g/>
naježený	naježený	k2eAgInSc1d1	naježený
ocas	ocas	k1gInSc1	ocas
kočky	kočka	k1gFnSc2	kočka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Lidské	lidský	k2eAgNnSc1d1	lidské
svalstvo	svalstvo	k1gNnSc1	svalstvo
<g/>
.	.	kIx.	.
</s>
<s>
Lidské	lidský	k2eAgNnSc1d1	lidské
svalstvo	svalstvo	k1gNnSc1	svalstvo
se	se	k3xPyFc4	se
nevymyká	vymykat	k5eNaImIp3nS	vymykat
typickému	typický	k2eAgNnSc3d1	typické
svalstvu	svalstvo	k1gNnSc3	svalstvo
primátů	primát	k1gMnPc2	primát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
samozřejmě	samozřejmě	k6eAd1	samozřejmě
soustředěna	soustředit	k5eAaPmNgFnS	soustředit
největší	veliký	k2eAgFnSc1d3	veliký
pozornost	pozornost	k1gFnSc1	pozornost
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
mezi	mezi	k7c7	mezi
vědci	vědec	k1gMnPc7	vědec
<g/>
,	,	kIx,	,
tak	tak	k9	tak
laiky	laik	k1gMnPc4	laik
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
cíleným	cílený	k2eAgInSc7d1	cílený
tréninkem	trénink	k1gInSc7	trénink
(	(	kIx(	(
<g/>
posilováním	posilování	k1gNnSc7	posilování
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
docílit	docílit	k5eAaPmF	docílit
nárůstu	nárůst	k1gInSc3	nárůst
svalové	svalový	k2eAgFnSc2d1	svalová
hmoty	hmota	k1gFnSc2	hmota
i	i	k9	i
zvětšení	zvětšení	k1gNnSc4	zvětšení
svalových	svalový	k2eAgNnPc2d1	svalové
vláken	vlákno	k1gNnPc2	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Kosterní	kosterní	k2eAgFnSc1d1	kosterní
svalovina	svalovina	k1gFnSc1	svalovina
(	(	kIx(	(
<g/>
příčně	příčně	k6eAd1	příčně
pruhovaná	pruhovaný	k2eAgFnSc1d1	pruhovaná
či	či	k8xC	či
žíhaná	žíhaný	k2eAgFnSc1d1	žíhaná
svalovina	svalovina	k1gFnSc1	svalovina
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
řízena	řízen	k2eAgFnSc1d1	řízena
nervy	nerv	k1gInPc4	nerv
mozkovými	mozkový	k2eAgInPc7d1	mozkový
a	a	k8xC	a
míšními	míšní	k2eAgInPc7d1	míšní
<g/>
,	,	kIx,	,
ovládána	ovládat	k5eAaImNgFnS	ovládat
naší	náš	k3xOp1gFnSc7	náš
vůlí	vůle	k1gFnSc7	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
sval	sval	k1gInSc1	sval
přemosťuje	přemosťovat	k5eAaImIp3nS	přemosťovat
jeden	jeden	k4xCgInSc4	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
kostěných	kostěný	k2eAgInPc2d1	kostěný
spojů	spoj	k1gInPc2	spoj
<g/>
.	.	kIx.	.
</s>
<s>
Hladká	hladký	k2eAgFnSc1d1	hladká
svalovina	svalovina	k1gFnSc1	svalovina
(	(	kIx(	(
<g/>
útrobní	útrobní	k2eAgFnSc1d1	útrobní
svalovina	svalovina	k1gFnSc1	svalovina
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
mimovolní	mimovolní	k2eAgFnSc2d1	mimovolní
<g/>
,	,	kIx,	,
výstelky	výstelka	k1gFnSc2	výstelka
útrob	útroba	k1gFnPc2	útroba
Srdeční	srdeční	k2eAgFnSc1d1	srdeční
svalovina	svalovina	k1gFnSc1	svalovina
(	(	kIx(	(
<g/>
myokard	myokard	k1gInSc1	myokard
<g/>
)	)	kIx)	)
Myoepiteliální	Myoepiteliální	k2eAgFnSc1d1	Myoepiteliální
tkáň	tkáň	k1gFnSc1	tkáň
<g/>
:	:	kIx,	:
druh	druh	k1gInSc1	druh
epitelu	epitel	k1gInSc2	epitel
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
stahuje	stahovat	k5eAaImIp3nS	stahovat
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
např.	např.	kA	např.
vylučování	vylučování	k1gNnSc1	vylučování
(	(	kIx(	(
<g/>
sekreci	sekrece	k1gFnSc3	sekrece
<g/>
)	)	kIx)	)
žláz	žláza	k1gFnPc2	žláza
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Příčně	příčně	k6eAd1	příčně
pruhované	pruhovaný	k2eAgNnSc1d1	pruhované
svalstvo	svalstvo	k1gNnSc1	svalstvo
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
mikroskopem	mikroskop	k1gInSc7	mikroskop
můžeme	moct	k5eAaImIp1nP	moct
pozorovat	pozorovat	k5eAaImF	pozorovat
žíhání	žíhání	k1gNnSc4	žíhání
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
pravidelným	pravidelný	k2eAgNnSc7d1	pravidelné
střídáním	střídání	k1gNnSc7	střídání
aktinu	aktin	k1gInSc2	aktin
a	a	k8xC	a
myosinu	myosina	k1gFnSc4	myosina
v	v	k7c6	v
myofibrilách	myofibrila	k1gFnPc6	myofibrila
<g/>
.	.	kIx.	.
</s>
<s>
Kosterní	kosterní	k2eAgNnSc1d1	kosterní
svalstvo	svalstvo	k1gNnSc1	svalstvo
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
příčně	příčně	k6eAd1	příčně
pruhovanou	pruhovaný	k2eAgFnSc7d1	pruhovaná
svalovinou	svalovina	k1gFnSc7	svalovina
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
upíná	upínat	k5eAaImIp3nS	upínat
ke	k	k7c3	k
kostem	kost	k1gFnPc3	kost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ovladatelné	ovladatelný	k2eAgNnSc1d1	ovladatelné
vůlí	vůle	k1gFnSc7	vůle
a	a	k8xC	a
díky	díky	k7c3	díky
němu	on	k3xPp3gInSc3	on
se	se	k3xPyFc4	se
tělo	tělo	k1gNnSc1	tělo
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
například	například	k6eAd1	například
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
svaly	sval	k1gInPc1	sval
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
končetin	končetina	k1gFnPc2	končetina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
bránice	bránice	k1gFnSc1	bránice
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgInSc1d1	hlavní
dýchací	dýchací	k2eAgInSc4d1	dýchací
sval	sval	k1gInSc4	sval
savců	savec	k1gMnPc2	savec
<g/>
.	.	kIx.	.
</s>
<s>
Stavbu	stavba	k1gFnSc4	stavba
kosterního	kosterní	k2eAgInSc2d1	kosterní
svalu	sval	k1gInSc2	sval
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zkoumat	zkoumat	k5eAaImF	zkoumat
na	na	k7c6	na
několika	několik	k4yIc6	několik
úrovních	úroveň	k1gFnPc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Kosterní	kosterní	k2eAgInSc1d1	kosterní
sval	sval	k1gInSc1	sval
se	se	k3xPyFc4	se
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
svalové	svalový	k2eAgNnSc1d1	svalové
bříško	bříško	k1gNnSc1	bříško
–	–	k?	–
masitá	masitý	k2eAgFnSc1d1	masitá
nejširší	široký	k2eAgFnSc1d3	nejširší
část	část	k1gFnSc1	část
hlava	hlava	k1gFnSc1	hlava
svalu	sval	k1gInSc2	sval
šlacha	šlacha	k1gFnSc1	šlacha
mnoho	mnoho	k4c1	mnoho
specializovaných	specializovaný	k2eAgInPc2d1	specializovaný
vazivových	vazivový	k2eAgInPc2d1	vazivový
útvarů	útvar	k1gInPc2	útvar
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
usnadňují	usnadňovat	k5eAaImIp3nP	usnadňovat
a	a	k8xC	a
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
jeho	jeho	k3xOp3gFnSc4	jeho
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Kosterní	kosterní	k2eAgInSc1d1	kosterní
sval	sval	k1gInSc1	sval
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
svalovými	svalový	k2eAgInPc7d1	svalový
vlákny	vlákna	k1gFnPc4	vlákna
(	(	kIx(	(
<g/>
až	až	k9	až
40	[number]	k4	40
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tvořené	tvořený	k2eAgFnSc2d1	tvořená
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
jednou	jednou	k6eAd1	jednou
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mnohojadernou	mnohojaderný	k2eAgFnSc7d1	mnohojaderná
buňkou	buňka	k1gFnSc7	buňka
<g/>
,	,	kIx,	,
takzvaným	takzvaný	k2eAgInSc7d1	takzvaný
rhabdomyocytem	rhabdomyocyt	k1gInSc7	rhabdomyocyt
<g/>
.	.	kIx.	.
</s>
<s>
Vlákna	vlákno	k1gNnPc1	vlákno
jsou	být	k5eAaImIp3nP	být
obalena	obalit	k5eAaPmNgFnS	obalit
řídkou	řídký	k2eAgFnSc7d1	řídká
vazivovou	vazivový	k2eAgFnSc7d1	vazivová
pochvou	pochva	k1gFnSc7	pochva
<g/>
.	.	kIx.	.
</s>
<s>
Podélně	podélně	k6eAd1	podélně
uložená	uložený	k2eAgFnSc1d1	uložená
příčně	příčně	k6eAd1	příčně
pruhovaná	pruhovaný	k2eAgNnPc1d1	pruhované
vlákna	vlákno	k1gNnPc1	vlákno
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
myofibrily	myofibrila	k1gFnPc1	myofibrila
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
kontrakci	kontrakce	k1gFnSc4	kontrakce
<g/>
.	.	kIx.	.
</s>
<s>
Svalová	svalový	k2eAgNnPc1d1	svalové
vlákna	vlákno	k1gNnPc1	vlákno
se	se	k3xPyFc4	se
spojují	spojovat	k5eAaImIp3nP	spojovat
ve	v	k7c4	v
snopečky	snopeček	k1gInPc4	snopeček
a	a	k8xC	a
snopce	snopec	k1gInPc4	snopec
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
svalových	svalový	k2eAgFnPc2d1	svalová
vláken	vlákna	k1gFnPc2	vlákna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
kryté	krytý	k2eAgMnPc4d1	krytý
silným	silný	k2eAgInSc7d1	silný
vazivovým	vazivový	k2eAgInSc7d1	vazivový
obalem	obal	k1gInSc7	obal
<g/>
.	.	kIx.	.
</s>
<s>
Snopce	snopec	k1gInPc1	snopec
se	se	k3xPyFc4	se
pojí	pojit	k5eAaImIp3nP	pojit
ve	v	k7c4	v
svaly	sval	k1gInPc4	sval
kryté	krytý	k2eAgInPc4d1	krytý
pevnou	pevný	k2eAgFnSc7d1	pevná
a	a	k8xC	a
pružnou	pružný	k2eAgFnSc7d1	pružná
vazivovou	vazivový	k2eAgFnSc7d1	vazivová
blanou	blána	k1gFnSc7	blána
–	–	k?	–
fascií	fascie	k1gFnPc2	fascie
(	(	kIx(	(
<g/>
povázkou	povázka	k1gFnSc7	povázka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
koncích	konec	k1gInPc6	konec
svalu	sval	k1gInSc2	sval
přechází	přecházet	k5eAaImIp3nS	přecházet
fascie	fascie	k1gFnSc1	fascie
ve	v	k7c4	v
šlachy	šlacha	k1gFnPc4	šlacha
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
pevně	pevně	k6eAd1	pevně
napojeny	napojen	k2eAgFnPc1d1	napojena
na	na	k7c4	na
kosti	kost	k1gFnPc4	kost
jako	jako	k8xC	jako
začátky	začátek	k1gInPc4	začátek
a	a	k8xC	a
úpony	úpon	k1gInPc4	úpon
svalů	sval	k1gInPc2	sval
<g/>
.	.	kIx.	.
</s>
<s>
Svalová	svalový	k2eAgNnPc1d1	svalové
i	i	k8xC	i
vazivová	vazivový	k2eAgNnPc1d1	vazivové
vlákna	vlákno	k1gNnPc1	vlákno
jsou	být	k5eAaImIp3nP	být
elastická	elastický	k2eAgNnPc1d1	elastické
<g/>
,	,	kIx,	,
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
až	až	k9	až
stoprocentní	stoprocentní	k2eAgNnSc4d1	stoprocentní
protažení	protažení	k1gNnSc4	protažení
své	svůj	k3xOyFgFnSc2	svůj
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc4	každý
svalové	svalový	k2eAgNnSc4d1	svalové
vlákno	vlákno	k1gNnSc4	vlákno
(	(	kIx(	(
<g/>
rhabdomyocyt	rhabdomyocyt	k1gInSc1	rhabdomyocyt
<g/>
)	)	kIx)	)
uvnitř	uvnitř	k7c2	uvnitř
v	v	k7c6	v
cytoplazmě	cytoplazma	k1gFnSc6	cytoplazma
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
myofibrily	myofibril	k1gInPc4	myofibril
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
je	být	k5eAaImIp3nS	být
ještě	ještě	k6eAd1	ještě
příčně	příčně	k6eAd1	příčně
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
přepážky	přepážka	k1gFnPc4	přepážka
<g/>
,	,	kIx,	,
sarkomery	sarkomer	k1gInPc4	sarkomer
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
těmto	tento	k3xDgFnPc3	tento
sarkomerám	sarkomera	k1gFnPc3	sarkomera
dostaly	dostat	k5eAaPmAgInP	dostat
příčně	příčně	k6eAd1	příčně
pruhované	pruhovaný	k2eAgInPc1d1	pruhovaný
svaly	sval	k1gInPc1	sval
svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Srdeční	srdeční	k2eAgFnSc1d1	srdeční
svalovina	svalovina	k1gFnSc1	svalovina
<g/>
.	.	kIx.	.
</s>
<s>
Srdeční	srdeční	k2eAgFnSc1d1	srdeční
svalovina	svalovina	k1gFnSc1	svalovina
či	či	k8xC	či
myokard	myokard	k1gInSc1	myokard
(	(	kIx(	(
<g/>
myokardum	myokardum	k1gInSc1	myokardum
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nP	tvořit
tzv.	tzv.	kA	tzv.
kardiomyocyty	kardiomyocyt	k1gInPc4	kardiomyocyt
či	či	k8xC	či
kardiocyty	kardiocyt	k1gInPc4	kardiocyt
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
příčně	příčně	k6eAd1	příčně
pruhované	pruhovaný	k2eAgFnPc1d1	pruhovaná
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
přítomnosti	přítomnost	k1gFnSc2	přítomnost
sarkomer	sarkomra	k1gFnPc2	sarkomra
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
délka	délka	k1gFnSc1	délka
činí	činit	k5eAaImIp3nS	činit
85	[number]	k4	85
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
mikrometrů	mikrometr	k1gInPc2	mikrometr
a	a	k8xC	a
šířka	šířka	k1gFnSc1	šířka
15	[number]	k4	15
mikrometrů	mikrometr	k1gInPc2	mikrometr
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnSc1	funkce
a	a	k8xC	a
stavba	stavba	k1gFnSc1	stavba
kontraktilních	kontraktilní	k2eAgFnPc2d1	kontraktilní
bílkovin	bílkovina	k1gFnPc2	bílkovina
je	být	k5eAaImIp3nS	být
shodná	shodný	k2eAgFnSc1d1	shodná
s	s	k7c7	s
kosterním	kosterní	k2eAgNnSc7d1	kosterní
svalstvem	svalstvo	k1gNnSc7	svalstvo
<g/>
.	.	kIx.	.
</s>
<s>
Buňky	buňka	k1gFnPc1	buňka
jsou	být	k5eAaImIp3nP	být
spojeny	spojen	k2eAgInPc1d1	spojen
interskalárními	interskalární	k2eAgInPc7d1	interskalární
disky	disk	k1gInPc7	disk
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
jedno	jeden	k4xCgNnSc1	jeden
až	až	k9	až
dvě	dva	k4xCgNnPc1	dva
buněčná	buněčný	k2eAgNnPc1d1	buněčné
jádra	jádro	k1gNnPc1	jádro
uspořádaná	uspořádaný	k2eAgNnPc1d1	uspořádané
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
směrech	směr	k1gInPc6	směr
<g/>
.	.	kIx.	.
</s>
<s>
Myokard	myokard	k1gInSc1	myokard
není	být	k5eNaImIp3nS	být
ovladatelný	ovladatelný	k2eAgInSc4d1	ovladatelný
vůlí	vůle	k1gFnSc7	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Podněty	podnět	k1gInPc1	podnět
ke	k	k7c3	k
stahování	stahování	k1gNnSc3	stahování
vznikají	vznikat	k5eAaImIp3nP	vznikat
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
srdci	srdce	k1gNnSc6	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
téměř	téměř	k6eAd1	téměř
celou	celý	k2eAgFnSc4d1	celá
srdeční	srdeční	k2eAgFnSc4d1	srdeční
stěnu	stěna	k1gFnSc4	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Hladké	Hladké	k2eAgNnSc4d1	Hladké
svalstvo	svalstvo	k1gNnSc4	svalstvo
<g/>
.	.	kIx.	.
</s>
<s>
Hladká	hladký	k2eAgFnSc1d1	hladká
svalovina	svalovina	k1gFnSc1	svalovina
je	být	k5eAaImIp3nS	být
evolučně	evolučně	k6eAd1	evolučně
původnější	původní	k2eAgNnSc1d2	původnější
<g/>
,	,	kIx,	,
u	u	k7c2	u
bezobratlých	bezobratlý	k2eAgFnPc2d1	bezobratlý
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
jediným	jediný	k2eAgInSc7d1	jediný
typem	typ	k1gInSc7	typ
svalové	svalový	k2eAgFnSc2d1	svalová
tkáně	tkáň	k1gFnSc2	tkáň
v	v	k7c6	v
jejich	jejich	k3xOp3gNnPc6	jejich
tělech	tělo	k1gNnPc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
savců	savec	k1gMnPc2	savec
je	být	k5eAaImIp3nS	být
přítomna	přítomen	k2eAgFnSc1d1	přítomna
pouze	pouze	k6eAd1	pouze
jako	jako	k8xS	jako
vůlí	vůle	k1gFnSc7	vůle
neovladatelná	ovladatelný	k2eNgFnSc1d1	neovladatelná
svalovina	svalovina	k1gFnSc1	svalovina
stěn	stěna	k1gFnPc2	stěna
cév	céva	k1gFnPc2	céva
<g/>
,	,	kIx,	,
trávicí	trávicí	k2eAgFnSc2d1	trávicí
trubice	trubice	k1gFnSc2	trubice
<g/>
,	,	kIx,	,
žaludku	žaludek	k1gInSc2	žaludek
<g/>
,	,	kIx,	,
vývodů	vývoda	k1gMnPc2	vývoda
žláz	žláza	k1gFnPc2	žláza
<g/>
,	,	kIx,	,
dělohy	děloha	k1gFnSc2	děloha
nebo	nebo	k8xC	nebo
svalů	sval	k1gInPc2	sval
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
vzpřimovač	vzpřimovač	k1gInSc4	vzpřimovač
chlupu	chlup	k1gInSc2	chlup
nebo	nebo	k8xC	nebo
svaly	sval	k1gInPc4	sval
duhovky	duhovka	k1gFnSc2	duhovka
oka	oko	k1gNnSc2	oko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podnět	podnět	k1gInSc4	podnět
reaguje	reagovat	k5eAaBmIp3nS	reagovat
pomaleji	pomale	k6eAd2	pomale
než	než	k8xS	než
příčně	příčně	k6eAd1	příčně
pruhovaný	pruhovaný	k2eAgInSc4d1	pruhovaný
sval	sval	k1gInSc4	sval
<g/>
.	.	kIx.	.
</s>
<s>
Hladká	hladký	k2eAgFnSc1d1	hladká
svalovina	svalovina	k1gFnSc1	svalovina
není	být	k5eNaImIp3nS	být
tvořena	tvořen	k2eAgFnSc1d1	tvořena
vlákny	vlákna	k1gFnPc1	vlákna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
samostatnými	samostatný	k2eAgFnPc7d1	samostatná
buňkami	buňka	k1gFnPc7	buňka
vřetenovitého	vřetenovitý	k2eAgInSc2d1	vřetenovitý
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gFnSc6	jejichž
cytoplazmě	cytoplazma	k1gFnSc6	cytoplazma
myofibrily	myofibrila	k1gFnSc2	myofibrila
nejsou	být	k5eNaImIp3nP	být
uspořádané	uspořádaný	k2eAgFnPc1d1	uspořádaná
jako	jako	k8xS	jako
u	u	k7c2	u
příčně	příčně	k6eAd1	příčně
pruhovaného	pruhovaný	k2eAgInSc2d1	pruhovaný
svalu	sval	k1gInSc2	sval
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
ani	ani	k8xC	ani
pod	pod	k7c7	pod
mikroskopem	mikroskop	k1gInSc7	mikroskop
nemůžeme	moct	k5eNaImIp1nP	moct
pozorovat	pozorovat	k5eAaImF	pozorovat
příčné	příčný	k2eAgNnSc4d1	příčné
žíhání	žíhání	k1gNnSc4	žíhání
<g/>
.	.	kIx.	.
</s>
