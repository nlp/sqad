<s>
Copa	Copa	k1gMnSc1
Airlines	Airlines	k1gMnSc1
</s>
<s>
Copa	Copa	k1gMnSc1
Airlines	Airlines	k1gMnSc1
</s>
<s>
IATACM	IATACM	kA
</s>
<s>
ICAOCMP	ICAOCMP	kA
</s>
<s>
CALLSIGNCOPA	CALLSIGNCOPA	kA
</s>
<s>
Zahájení	zahájení	k1gNnSc1
činnosti	činnost	k1gFnSc2
<g/>
1944	#num#	k4
<g/>
Sídlo	sídlo	k1gNnSc1
Ciudad	Ciudad	k1gInSc1
de	de	k?
Panamá	Panamý	k2eAgFnSc1d1
<g/>
,	,	kIx,
PanamaHlavní	PanamaHlavní	k2eAgNnSc1d1
základnaLetiště	základnaLetiště	k1gNnSc1
Panama	panama	k2eAgInSc2d1
<g/>
–	–	k?
<g/>
TocumenPestinace	TocumenPestinace	k1gFnSc1
<g/>
78	#num#	k4
(	(	kIx(
<g/>
listopad	listopad	k1gInSc1
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
Velikost	velikost	k1gFnSc1
flotily	flotila	k1gFnSc2
<g/>
92	#num#	k4
(	(	kIx(
<g/>
listopad	listopad	k1gInSc1
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
Věrnostní	věrnostní	k2eAgInSc1d1
programConnectMilesČlen	programConnectMilesČlen	k2eAgInSc1d1
alianceStar	alianceStar	k1gInSc1
AllianceOficiální	AllianceOficiální	k2eAgInSc1d1
web	web	k1gInSc1
<g/>
www.copaair.com	www.copaair.com	k1gInSc1
</s>
<s>
Copa	Copa	k1gMnSc1
Airlines	Airlines	k1gMnSc1
(	(	kIx(
<g/>
čínsky	čínsky	k6eAd1
<g/>
:	:	kIx,
深	深	k?
<g/>
)	)	kIx)
(	(	kIx(
<g/>
ICAO	ICAO	kA
<g/>
:	:	kIx,
CMP	CMP	kA
<g/>
,	,	kIx,
IATA	IATA	kA
<g/>
:	:	kIx,
CM	cm	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
panamská	panamský	k2eAgFnSc1d1
letecká	letecký	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
se	s	k7c7
sídlem	sídlo	k1gNnSc7
v	v	k7c6
hlavní	hlavní	k2eAgFnSc6d1
městě	město	k1gNnSc6
Panama	panama	k2eAgNnSc6d1
na	na	k7c6
zdejším	zdejší	k2eAgNnSc6d1
letišti	letiště	k1gNnSc6
Tocumen	Tocumen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1944	#num#	k4
<g/>
,	,	kIx,
operuje	operovat	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1947	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2012	#num#	k4
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c7
členy	člen	k1gInPc7
aliance	aliance	k1gFnSc2
Star	Star	kA
Alliance	Allianec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejími	její	k3xOp3gFnPc7
dceřinými	dceřin	k2eAgFnPc7d1
společnostmi	společnost	k1gFnPc7
jsou	být	k5eAaImIp3nP
Wingo	Wingo	k1gNnSc1
a	a	k8xC
Copa	Copa	k1gFnSc1
Airlines	Airlines	k1gInSc1
Columbia	Columbia	k1gFnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společnost	společnost	k1gFnSc1
provozuje	provozovat	k5eAaImIp3nS
především	především	k6eAd1
lety	léto	k1gNnPc7
po	po	k7c6
Střední	střední	k2eAgFnSc6d1
<g/>
,	,	kIx,
Jižní	jižní	k2eAgFnSc6d1
a	a	k8xC
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
říjnu	říjen	k1gInSc3
2018	#num#	k4
létá	létat	k5eAaImIp3nS
do	do	k7c2
78	#num#	k4
destinací	destinace	k1gFnPc2
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
flotilu	flotila	k1gFnSc4
92	#num#	k4
letounů	letoun	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tu	tu	k6eAd1
tvoří	tvořit	k5eAaImIp3nS
jen	jen	k9
úzkotrupá	úzkotrupat	k5eAaPmIp3nS
letadla	letadlo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Copa	Copa	k1gMnSc1
Airlines	Airlines	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Copa	Cop	k1gInSc2
Airlines	Airlinesa	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Členové	člen	k1gMnPc1
aliance	aliance	k1gFnSc2
Star	Star	kA
Alliance	Alliance	k1gFnSc1
Zakládající	zakládající	k2eAgMnPc1d1
členové	člen	k1gMnPc1
</s>
<s>
Air	Air	k?
Canada	Canada	k1gFnSc1
•	•	k?
Lufthansa	Lufthans	k1gMnSc2
•	•	k?
Scandinavian	Scandinavian	k1gMnSc1
Airlines	Airlines	k1gMnSc1
•	•	k?
Thai	Thai	k1gNnPc2
Airways	Airways	k1gInSc1
•	•	k?
United	United	k1gInSc1
Airlines	Airlinesa	k1gFnPc2
Ostatní	ostatní	k2eAgMnPc1d1
členové	člen	k1gMnPc1
</s>
<s>
Aegean	Aegean	k1gMnSc1
Airlines	Airlines	k1gMnSc1
•	•	k?
Air	Air	k1gFnSc1
China	China	k1gFnSc1
•	•	k?
Air	Air	k1gFnSc2
India	indium	k1gNnSc2
•	•	k?
Air	Air	k1gMnSc1
New	New	k1gMnSc1
Zealand	Zealando	k1gNnPc2
•	•	k?
All	All	k1gFnSc2
Nippon	Nippona	k1gFnPc2
Airways	Airways	k1gInSc1
•	•	k?
Asiana	Asiana	k1gFnSc1
Airlines	Airlines	k1gMnSc1
•	•	k?
Austrian	Austrian	k1gMnSc1
Airlines	Airlines	k1gMnSc1
•	•	k?
Avianca	Avianca	k1gMnSc1
•	•	k?
Brussels	Brussels	k1gInSc1
Airlines	Airlines	k1gMnSc1
•	•	k?
Copa	Copa	k1gMnSc1
Airlines	Airlines	k1gMnSc1
•	•	k?
Croatia	Croatia	k1gFnSc1
Airlines	Airlines	k1gMnSc1
•	•	k?
EgyptAir	EgyptAir	k1gMnSc1
•	•	k?
Ethiopian	Ethiopian	k1gMnSc1
Airlines	Airlines	k1gMnSc1
•	•	k?
EVA	Eva	k1gFnSc1
Air	Air	k1gFnSc2
•	•	k?
LOT	lot	k1gInSc1
•	•	k?
Shenzhen	Shenzhen	k2eAgInSc1d1
Airlines	Airlines	k1gInSc1
•	•	k?
Singapore	Singapor	k1gInSc5
Airlines	Airlines	k1gMnSc1
•	•	k?
South	South	k1gMnSc1
African	African	k1gMnSc1
Airways	Airwaysa	k1gFnPc2
•	•	k?
Swiss	Swissa	k1gFnPc2
International	International	k1gMnSc1
Air	Air	k1gMnSc1
Lines	Lines	k1gMnSc1
•	•	k?
TAP	TAP	kA
Portugal	portugal	k1gInSc1
•	•	k?
Turkish	Turkish	k1gMnSc1
Airlines	Airlines	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Letectví	letectví	k1gNnSc1
|	|	kIx~
Střední	střední	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
a	a	k8xC
Karibik	Karibik	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
2003097655	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
149125494	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
2003097655	#num#	k4
</s>
