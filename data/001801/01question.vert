<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
navržený	navržený	k2eAgInSc1d1	navržený
pro	pro	k7c4	pro
využití	využití	k1gNnSc4	využití
v	v	k7c6	v
mobilních	mobilní	k2eAgInPc6d1	mobilní
zařízeních	zařízení	k1gNnPc6	zařízení
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
běží	běžet	k5eAaImIp3nS	běžet
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c6	na
procesorech	procesor	k1gInPc6	procesor
ARM	ARM	kA	ARM
<g/>
?	?	kIx.	?
</s>
