<s>
Symbian	Symbian	k1gInSc1	Symbian
OS	OS	kA	OS
je	být	k5eAaImIp3nS	být
platforma	platforma	k1gFnSc1	platforma
navržená	navržený	k2eAgFnSc1d1	navržená
do	do	k7c2	do
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
značky	značka	k1gFnSc2	značka
Nokia	Nokia	kA	Nokia
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
základní	základní	k2eAgInSc4d1	základní
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
OS	OS	kA	OS
-	-	kIx~	-
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Doplňují	doplňovat	k5eAaImIp3nP	doplňovat
ho	on	k3xPp3gInSc4	on
knihovny	knihovna	k1gFnPc1	knihovna
<g/>
,	,	kIx,	,
grafické	grafický	k2eAgNnSc1d1	grafické
uživatelské	uživatelský	k2eAgNnSc1d1	Uživatelské
rozhraní	rozhraní	k1gNnSc1	rozhraní
a	a	k8xC	a
referenční	referenční	k2eAgFnSc1d1	referenční
implementace	implementace	k1gFnSc1	implementace
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
firma	firma	k1gFnSc1	firma
Symbian	Symbian	k1gMnSc1	Symbian
Ltd	ltd	kA	ltd
<g/>
.	.	kIx.	.
</s>
<s>
Symbian	Symbian	k1gInSc1	Symbian
OS	OS	kA	OS
je	být	k5eAaImIp3nS	být
následovníkem	následovník	k1gMnSc7	následovník
systému	systém	k1gInSc2	systém
EPOC	EPOC	kA	EPOC
používaného	používaný	k2eAgInSc2d1	používaný
v	v	k7c6	v
kapesních	kapesní	k2eAgInPc6d1	kapesní
počítačích	počítač	k1gInPc6	počítač
Psion	Psion	kA	Psion
a	a	k8xC	a
běží	běžet	k5eAaImIp3nS	běžet
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c6	na
procesorech	procesor	k1gInPc6	procesor
ARM	ARM	kA	ARM
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
reálného	reálný	k2eAgInSc2d1	reálný
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
únor	únor	k1gInSc1	únor
2010	[number]	k4	2010
až	až	k9	až
březen	březen	k1gInSc1	březen
2011	[number]	k4	2011
používal	používat	k5eAaImAgInS	používat
licenci	licence	k1gFnSc4	licence
EPL	EPL	kA	EPL
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
tedy	tedy	k9	tedy
otevřený	otevřený	k2eAgInSc1d1	otevřený
software	software	k1gInSc1	software
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
a	a	k8xC	a
potom	potom	k6eAd1	potom
používal	používat	k5eAaImAgMnS	používat
uzavřenou	uzavřený	k2eAgFnSc4d1	uzavřená
licenci	licence	k1gFnSc4	licence
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
systému	systém	k1gInSc2	systém
může	moct	k5eAaImIp3nS	moct
uživatel	uživatel	k1gMnSc1	uživatel
přidávat	přidávat	k5eAaImF	přidávat
nativní	nativní	k2eAgFnPc4d1	nativní
aplikace	aplikace	k1gFnPc4	aplikace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
však	však	k9	však
závisí	záviset	k5eAaImIp3nP	záviset
na	na	k7c4	na
verzi	verze	k1gFnSc4	verze
použitého	použitý	k2eAgInSc2d1	použitý
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Symbian	Symbian	k1gInSc1	Symbian
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
používán	používat	k5eAaImNgInS	používat
především	především	k9	především
v	v	k7c6	v
mobilních	mobilní	k2eAgInPc6d1	mobilní
telefonech	telefon	k1gInPc6	telefon
značky	značka	k1gFnSc2	značka
Nokia	Nokia	kA	Nokia
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2011	[number]	k4	2011
Nokia	Nokia	kA	Nokia
oznámila	oznámit	k5eAaPmAgFnS	oznámit
ústup	ústup	k1gInSc4	ústup
od	od	k7c2	od
dalšího	další	k2eAgInSc2d1	další
vývoje	vývoj	k1gInSc2	vývoj
Symbianu	Symbian	k1gInSc2	Symbian
a	a	k8xC	a
přechod	přechod	k1gInSc4	přechod
k	k	k7c3	k
platformě	platforma	k1gFnSc3	platforma
Windows	Windows	kA	Windows
Phone	Phon	k1gInSc5	Phon
7	[number]	k4	7
a	a	k8xC	a
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
také	také	k9	také
Nokia	Nokia	kA	Nokia
ukončila	ukončit	k5eAaPmAgFnS	ukončit
provoz	provoz	k1gInSc4	provoz
stránky	stránka	k1gFnSc2	stránka
http://symbian.nokia.com	[url]	k1gInSc1	http://symbian.nokia.com
<g/>
.	.	kIx.	.
</s>
<s>
Smartphony	Smartphona	k1gFnPc1	Smartphona
řady	řada	k1gFnSc2	řada
S60	S60	k1gFnSc2	S60
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
nejčastější	častý	k2eAgInPc1d3	nejčastější
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dáno	dát	k5eAaPmNgNnS	dát
jejich	jejich	k3xOp3gMnPc4	jejich
snadnou	snadný	k2eAgFnSc7d1	snadná
softwarovou	softwarový	k2eAgFnSc7d1	softwarová
rozšiřitelností	rozšiřitelnost	k1gFnSc7	rozšiřitelnost
a	a	k8xC	a
poměrně	poměrně	k6eAd1	poměrně
nízkou	nízký	k2eAgFnSc7d1	nízká
cenou	cena	k1gFnSc7	cena
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
typičtí	typický	k2eAgMnPc1d1	typický
zástupci	zástupce	k1gMnPc1	zástupce
této	tento	k3xDgFnSc2	tento
řady	řada	k1gFnSc2	řada
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
jmenovat	jmenovat	k5eAaImF	jmenovat
modely	model	k1gInPc4	model
Nokia	Nokia	kA	Nokia
3650	[number]	k4	3650
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
N	N	kA	N
<g/>
70	[number]	k4	70
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
E	E	kA	E
<g/>
61	[number]	k4	61
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
E	E	kA	E
<g/>
51	[number]	k4	51
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
E	E	kA	E
<g/>
52	[number]	k4	52
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
E	E	kA	E
<g/>
72	[number]	k4	72
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
E	E	kA	E
<g/>
50	[number]	k4	50
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
E90	E90	k1gFnSc1	E90
nebo	nebo	k8xC	nebo
Siemens	siemens	k1gInSc1	siemens
SX	SX	kA	SX
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
existuje	existovat	k5eAaImIp3nS	existovat
již	již	k6eAd1	již
pátá	pátý	k4xOgFnSc1	pátý
generace	generace	k1gFnSc1	generace
S60	S60	k1gFnSc2	S60
podporující	podporující	k2eAgFnSc2d1	podporující
smartphony	smartphona	k1gFnSc2	smartphona
s	s	k7c7	s
čistě	čistě	k6eAd1	čistě
dotykovým	dotykový	k2eAgNnSc7d1	dotykové
ovládáním	ovládání	k1gNnSc7	ovládání
<g/>
.	.	kIx.	.
</s>
<s>
Klady	klad	k1gInPc1	klad
a	a	k8xC	a
zápory	zápor	k1gInPc1	zápor
S	s	k7c7	s
<g/>
60	[number]	k4	60
<g/>
:	:	kIx,	:
-	-	kIx~	-
Nekompatibilita	nekompatibilita	k1gFnSc1	nekompatibilita
aplikací	aplikace	k1gFnPc2	aplikace
a	a	k8xC	a
her	hra	k1gFnPc2	hra
ze	z	k7c2	z
Symbianu	Symbian	k1gInSc2	Symbian
6.1	[number]	k4	6.1
<g/>
,	,	kIx,	,
7.0	[number]	k4	7.0
<g/>
,	,	kIx,	,
8.1	[number]	k4	8.1
na	na	k7c4	na
Symbian	Symbian	k1gInSc4	Symbian
9.1	[number]	k4	9.1
+	+	kIx~	+
Možnost	možnost	k1gFnSc1	možnost
upgrade	upgrade	k1gInSc1	upgrade
firmwaru	firmware	k1gInSc2	firmware
přes	přes	k7c4	přes
internet	internet	k1gInSc4	internet
(	(	kIx(	(
<g/>
novější	nový	k2eAgInPc1d2	novější
telefony	telefon	k1gInPc1	telefon
od	od	k7c2	od
Symbianu	Symbian	k1gInSc2	Symbian
7.0	[number]	k4	7.0
<g/>
)	)	kIx)	)
+	+	kIx~	+
Do	do	k7c2	do
nových	nový	k2eAgInPc2d1	nový
telefonů	telefon	k1gInPc2	telefon
přibývá	přibývat	k5eAaImIp3nS	přibývat
podpora	podpora	k1gFnSc1	podpora
Wi-Fi	Wi-F	k1gFnSc2	Wi-F
sítí	síť	k1gFnPc2	síť
(	(	kIx(	(
<g/>
Nokia	Nokia	kA	Nokia
E	E	kA	E
<g/>
61	[number]	k4	61
<g/>
i	i	k8xC	i
...	...	k?	...
<g/>
)	)	kIx)	)
+	+	kIx~	+
Přibývá	přibývat	k5eAaImIp3nS	přibývat
podpora	podpora	k1gFnSc1	podpora
HW	HW	kA	HW
klávesnic	klávesnice	k1gFnPc2	klávesnice
+	+	kIx~	+
N-Gage	N-Gage	k1gInSc1	N-Gage
2.0	[number]	k4	2.0
a	a	k8xC	a
Sis	Sis	k1gMnSc3	Sis
3D	[number]	k4	3D
hry	hra	k1gFnSc2	hra
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kulečník	kulečník	k1gInSc1	kulečník
...	...	k?	...
<g/>
)	)	kIx)	)
Odlišná	odlišný	k2eAgFnSc1d1	odlišná
verze	verze	k1gFnSc1	verze
systému	systém	k1gInSc2	systém
S80	S80	k1gFnSc1	S80
je	být	k5eAaImIp3nS	být
uzpůsobena	uzpůsobit	k5eAaPmNgFnS	uzpůsobit
také	také	k9	také
pro	pro	k7c4	pro
přístroje	přístroj	k1gInPc4	přístroj
s	s	k7c7	s
dotykovým	dotykový	k2eAgInSc7d1	dotykový
displejem	displej	k1gInSc7	displej
a	a	k8xC	a
širokým	široký	k2eAgInSc7d1	široký
displejem	displej	k1gInSc7	displej
<g/>
.	.	kIx.	.
</s>
<s>
Komunikátory	komunikátor	k1gInPc1	komunikátor
této	tento	k3xDgFnSc2	tento
třídy	třída	k1gFnSc2	třída
bývají	bývat	k5eAaImIp3nP	bývat
vybaveny	vybavit	k5eAaPmNgInP	vybavit
lépe	dobře	k6eAd2	dobře
než	než	k8xS	než
S	s	k7c7	s
<g/>
60	[number]	k4	60
<g/>
.	.	kIx.	.
</s>
<s>
S80	S80	k4	S80
si	se	k3xPyFc3	se
musí	muset	k5eAaImIp3nP	muset
umět	umět	k5eAaImF	umět
poradit	poradit	k5eAaPmF	poradit
například	například	k6eAd1	například
i	i	k9	i
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
Wi-Fi	Wi-Fi	k1gNnPc2	Wi-Fi
<g/>
,	,	kIx,	,
dokumentů	dokument	k1gInPc2	dokument
MS	MS	kA	MS
Office	Office	kA	Office
...	...	k?	...
což	což	k3yRnSc1	což
už	už	k6eAd1	už
ovšem	ovšem	k9	ovšem
dneska	dneska	k?	dneska
zvládá	zvládat	k5eAaImIp3nS	zvládat
i	i	k9	i
S	s	k7c7	s
<g/>
60	[number]	k4	60
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgNnSc3	ten
ovšem	ovšem	k9	ovšem
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
i	i	k9	i
jejich	jejich	k3xOp3gFnSc1	jejich
cena	cena	k1gFnSc1	cena
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
neklesne	klesnout	k5eNaPmIp3nS	klesnout
pod	pod	k7c7	pod
10	[number]	k4	10
000	[number]	k4	000
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
zástupce	zástupce	k1gMnSc1	zástupce
může	moct	k5eAaImIp3nS	moct
posloužit	posloužit	k5eAaPmF	posloužit
Nokia	Nokia	kA	Nokia
9300	[number]	k4	9300
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
9300	[number]	k4	9300
<g/>
i	i	k8xC	i
nebo	nebo	k8xC	nebo
Nokia	Nokia	kA	Nokia
9500	[number]	k4	9500
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
používá	používat	k5eAaImIp3nS	používat
verzi	verze	k1gFnSc4	verze
S90	S90	k1gFnSc2	S90
jen	jen	k9	jen
Nokia	Nokia	kA	Nokia
7710	[number]	k4	7710
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
svým	svůj	k3xOyFgInSc7	svůj
vzhledem	vzhled	k1gInSc7	vzhled
telefon	telefon	k1gInSc1	telefon
připomíná	připomínat	k5eAaImIp3nS	připomínat
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gNnSc7	její
hlavním	hlavní	k2eAgNnSc7d1	hlavní
specifikem	specifikon	k1gNnSc7	specifikon
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc1d1	velký
dotykový	dotykový	k2eAgInSc1d1	dotykový
displej	displej	k1gInSc1	displej
<g/>
,	,	kIx,	,
podpora	podpora	k1gFnSc1	podpora
sítí	síť	k1gFnPc2	síť
GPRS	GPRS	kA	GPRS
<g/>
/	/	kIx~	/
<g/>
EDGE	EDGE	kA	EDGE
<g/>
,	,	kIx,	,
BlueTooth	BlueTooth	k1gInSc1	BlueTooth
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
telefon	telefon	k1gInSc4	telefon
je	být	k5eAaImIp3nS	být
uzpůsoben	uzpůsobit	k5eAaPmNgMnS	uzpůsobit
pro	pro	k7c4	pro
prohlížení	prohlížení	k1gNnSc4	prohlížení
internetu	internet	k1gInSc2	internet
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nazýván	nazývat	k5eAaImNgInS	nazývat
jako	jako	k8xS	jako
multimediální	multimediální	k2eAgInSc1d1	multimediální
smartphone	smartphon	k1gInSc5	smartphon
<g/>
.	.	kIx.	.
</s>
<s>
UIQ	UIQ	kA	UIQ
podporuje	podporovat	k5eAaImIp3nS	podporovat
velký	velký	k2eAgInSc1d1	velký
dotykový	dotykový	k2eAgInSc1d1	dotykový
displej	displej	k1gInSc1	displej
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
<g/>
,	,	kIx,	,
než	než	k8xS	než
ostatní	ostatní	k2eAgFnPc1d1	ostatní
verze	verze	k1gFnPc1	verze
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
telefonech	telefon	k1gInPc6	telefon
značky	značka	k1gFnSc2	značka
Sony	Sony	kA	Sony
Ericsson	Ericsson	kA	Ericsson
a	a	k8xC	a
Motorola	Motorola	kA	Motorola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
se	se	k3xPyFc4	se
spojily	spojit	k5eAaPmAgFnP	spojit
firmy	firma	k1gFnPc1	firma
Motorola	Motorola	kA	Motorola
<g/>
,	,	kIx,	,
Ericsson	Ericsson	kA	Ericsson
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
a	a	k8xC	a
Psion	Psion	kA	Psion
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
sdružení	sdružení	k1gNnSc4	sdružení
Symbian	Symbiana	k1gFnPc2	Symbiana
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
používání	používání	k1gNnSc1	používání
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
EPOC	EPOC	kA	EPOC
navrženého	navržený	k2eAgNnSc2d1	navržené
specificky	specificky	k6eAd1	specificky
pro	pro	k7c4	pro
mobilní	mobilní	k2eAgNnSc4d1	mobilní
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Připojení	připojení	k1gNnSc1	připojení
společnosti	společnost	k1gFnSc2	společnost
iMatsushita	iMatsushit	k1gMnSc2	iMatsushit
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Symbian	Symbian	k1gInSc1	Symbian
představuje	představovat	k5eAaImIp3nS	představovat
85	[number]	k4	85
%	%	kIx~	%
světového	světový	k2eAgInSc2d1	světový
objemu	objem	k1gInSc2	objem
prodeje	prodej	k1gInSc2	prodej
chytrých	chytrý	k2eAgInPc2d1	chytrý
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
<g/>
.	.	kIx.	.
</s>
<s>
Symbian	Symbian	k1gInSc1	Symbian
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2008	[number]	k4	2008
vlastněn	vlastněn	k2eAgInSc4d1	vlastněn
firmou	firma	k1gFnSc7	firma
Nokia	Nokia	kA	Nokia
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
barcelonském	barcelonský	k2eAgInSc6d1	barcelonský
kongresu	kongres	k1gInSc6	kongres
oznámila	oznámit	k5eAaPmAgFnS	oznámit
vykoupení	vykoupení	k1gNnSc6	vykoupení
zbývajících	zbývající	k2eAgNnPc2d1	zbývající
52	[number]	k4	52
%	%	kIx~	%
akcií	akcie	k1gFnPc2	akcie
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nyní	nyní	k6eAd1	nyní
vlastní	vlastní	k2eAgMnPc4d1	vlastní
více	hodně	k6eAd2	hodně
než	než	k8xS	než
99.9	[number]	k4	99.9
%	%	kIx~	%
akcií	akcie	k1gFnPc2	akcie
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Předchozí	předchozí	k2eAgNnSc1d1	předchozí
zastoupení	zastoupení	k1gNnSc1	zastoupení
47,9	[number]	k4	47,9
%	%	kIx~	%
<g/>
,	,	kIx,	,
Ericsson	Ericsson	kA	Ericsson
15,6	[number]	k4	15,6
%	%	kIx~	%
<g/>
,	,	kIx,	,
Sony	Sony	kA	Sony
Ericsson	Ericsson	kA	Ericsson
13,1	[number]	k4	13,1
%	%	kIx~	%
<g/>
,	,	kIx,	,
Panasonic	Panasonic	kA	Panasonic
10,5	[number]	k4	10,5
%	%	kIx~	%
<g/>
,	,	kIx,	,
Siemens	siemens	k1gInSc1	siemens
AG	AG	kA	AG
8,4	[number]	k4	8,4
%	%	kIx~	%
a	a	k8xC	a
Samsung	Samsung	kA	Samsung
4,5	[number]	k4	4,5
%	%	kIx~	%
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
také	také	k9	také
organizace	organizace	k1gFnSc1	organizace
Symbian	Symbian	k1gMnSc1	Symbian
Foundation	Foundation	k1gInSc1	Foundation
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gMnPc1	jejíž
členové	člen	k1gMnPc1	člen
(	(	kIx(	(
<g/>
Nokia	Nokia	kA	Nokia
<g/>
,	,	kIx,	,
Motorola	Motorola	kA	Motorola
<g/>
,	,	kIx,	,
Ericsson	Ericsson	kA	Ericsson
<g/>
,	,	kIx,	,
LG	LG	kA	LG
<g/>
,	,	kIx,	,
Samsung	Samsung	kA	Samsung
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
)	)	kIx)	)
chtějí	chtít	k5eAaImIp3nP	chtít
vytvořit	vytvořit	k5eAaPmF	vytvořit
jednotnou	jednotný	k2eAgFnSc4d1	jednotná
a	a	k8xC	a
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
platformu	platforma	k1gFnSc4	platforma
pro	pro	k7c4	pro
mobilní	mobilní	k2eAgInPc4d1	mobilní
telefony	telefon	k1gInPc4	telefon
<g/>
.	.	kIx.	.
</s>
<s>
Kód	kód	k1gInSc1	kód
systému	systém	k1gInSc2	systém
byl	být	k5eAaImAgInS	být
pozvolně	pozvolně	k6eAd1	pozvolně
uvolňován	uvolňován	k2eAgInSc1d1	uvolňován
pod	pod	k7c7	pod
licencí	licence	k1gFnSc7	licence
Eclipse	Eclipse	k1gFnSc1	Eclipse
Public	publicum	k1gNnPc2	publicum
License	License	k1gFnSc2	License
(	(	kIx(	(
<g/>
EPL	EPL	kA	EPL
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
části	část	k1gFnPc1	část
však	však	k9	však
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
EKA2	EKA2	k1gFnSc7	EKA2
mikrokernel	mikrokernela	k1gFnPc2	mikrokernela
Symbianu	Symbian	k1gInSc2	Symbian
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
uvedení	uvedení	k1gNnSc6	uvedení
aktualizace	aktualizace	k1gFnSc2	aktualizace
Belle	bell	k1gInSc5	bell
Nokia	Nokia	kA	Nokia
de	de	k?	de
facto	facto	k1gNnSc1	facto
značka	značka	k1gFnSc1	značka
Symbian	Symbian	k1gInSc4	Symbian
zaniká	zanikat	k5eAaImIp3nS	zanikat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
jej	on	k3xPp3gInSc4	on
Nokia	Nokia	kA	Nokia
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
komunikovat	komunikovat	k5eAaImF	komunikovat
pouze	pouze	k6eAd1	pouze
jako	jako	k8xS	jako
Nokia	Nokia	kA	Nokia
Belle	bell	k1gInSc5	bell
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
ale	ale	k9	ale
začal	začít	k5eAaPmAgInS	začít
Symbian	Symbian	k1gInSc1	Symbian
mezi	mezi	k7c7	mezi
ostatními	ostatní	k2eAgInPc7d1	ostatní
systémy	systém	k1gInPc7	systém
(	(	kIx(	(
<g/>
iOS	iOS	k?	iOS
a	a	k8xC	a
Android	android	k1gInSc1	android
<g/>
)	)	kIx)	)
propadat	propadat	k5eAaImF	propadat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
nakonec	nakonec	k6eAd1	nakonec
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Nokia	Nokia	kA	Nokia
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tomuto	tento	k3xDgInSc3	tento
systému	systém	k1gInSc3	systém
plně	plně	k6eAd1	plně
důvěřovala	důvěřovat	k5eAaImAgFnS	důvěřovat
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
mít	mít	k5eAaImF	mít
velké	velký	k2eAgFnPc4d1	velká
ztráty	ztráta	k1gFnPc4	ztráta
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
Psion	Psion	kA	Psion
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
Davidem	David	k1gMnSc7	David
Potterem	Potter	k1gMnSc7	Potter
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
neúspěchu	neúspěch	k1gInSc6	neúspěch
MC400	MC400	k1gFnSc2	MC400
Psion	Psion	kA	Psion
uvolnil	uvolnit	k5eAaPmAgMnS	uvolnit
zařízení	zařízení	k1gNnSc4	zařízení
Serie	serie	k1gFnSc2	serie
<g/>
3	[number]	k4	3
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
využívali	využívat	k5eAaPmAgMnP	využívat
EPOC16	EPOC16	k1gMnPc1	EPOC16
OS	OS	kA	OS
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
známý	známý	k2eAgMnSc1d1	známý
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
SIBO	SIBO	kA	SIBO
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
zařízení	zařízení	k1gNnPc1	zařízení
podporovala	podporovat	k5eAaImAgNnP	podporovat
jednoduchý	jednoduchý	k2eAgInSc4d1	jednoduchý
programovací	programovací	k2eAgInSc4d1	programovací
jazyk	jazyk	k1gInSc4	jazyk
OPL	OPL	kA	OPL
a	a	k8xC	a
vývojové	vývojový	k2eAgNnSc1d1	vývojové
prostředí	prostředí	k1gNnSc1	prostředí
OVAL	ovalit	k5eAaPmRp2nS	ovalit
<g/>
.	.	kIx.	.
</s>
<s>
Zařízení	zařízení	k1gNnSc1	zařízení
Verze	verze	k1gFnSc2	verze
5	[number]	k4	5
byly	být	k5eAaImAgFnP	být
vydány	vydat	k5eAaPmNgFnP	vydat
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1998	[number]	k4	1998
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
společnost	společnost	k1gFnSc1	společnost
Symbian	Symbian	k1gMnSc1	Symbian
Ltd	ltd	kA	ltd
<g/>
.	.	kIx.	.
jako	jako	k8xC	jako
partnerství	partnerství	k1gNnSc1	partnerství
mezi	mezi	k7c7	mezi
Ericssonem	Ericsson	k1gInSc7	Ericsson
<g/>
,	,	kIx,	,
Nokií	Nokie	k1gFnSc7	Nokie
<g/>
,	,	kIx,	,
Motorolou	Motorola	k1gFnSc7	Motorola
a	a	k8xC	a
Psionem	Psion	k1gInSc7	Psion
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
využívat	využívat	k5eAaPmF	využívat
konvergenci	konvergence	k1gFnSc4	konvergence
mezi	mezi	k7c7	mezi
zařízeními	zařízení	k1gNnPc7	zařízení
PDA	PDA	kA	PDA
a	a	k8xC	a
mobilními	mobilní	k2eAgInPc7d1	mobilní
telefony	telefon	k1gInPc7	telefon
<g/>
.	.	kIx.	.
</s>
<s>
Psion	Psion	kA	Psion
Series	Series	k1gMnSc1	Series
5	[number]	k4	5
<g/>
mx	mx	k?	mx
<g/>
,	,	kIx,	,
Series	Series	k1gInSc1	Series
7	[number]	k4	7
<g/>
,	,	kIx,	,
Psion	Psion	kA	Psion
Revo	Revo	k1gNnSc4	Revo
<g/>
,	,	kIx,	,
Diamond	Diamond	k1gInSc4	Diamond
Mako	mako	k1gNnSc4	mako
<g/>
,	,	kIx,	,
Psion	Psion	kA	Psion
Netbook	Netbook	k1gInSc1	Netbook
<g/>
,	,	kIx,	,
netPad	netPad	k1gInSc1	netPad
<g/>
,	,	kIx,	,
GeoFox	GeoFox	k1gInSc1	GeoFox
One	One	k1gFnSc1	One
<g/>
,	,	kIx,	,
Oregon	Oregon	k1gInSc1	Oregon
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Osaris	Osaris	k1gFnSc7	Osaris
<g/>
,	,	kIx,	,
a	a	k8xC	a
Ericsson	Ericsson	kA	Ericsson
MC218	MC218	k1gFnPc1	MC218
byly	být	k5eAaImAgFnP	být
představeny	představit	k5eAaPmNgFnP	představit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
jako	jako	k8xS	jako
zařízení	zařízení	k1gNnPc4	zařízení
využívající	využívající	k2eAgNnPc4d1	využívající
EPOC	EPOC	kA	EPOC
Release	Releasa	k1gFnSc3	Releasa
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
byla	být	k5eAaImAgFnS	být
někdy	někdy	k6eAd1	někdy
také	také	k6eAd1	také
nazývána	nazývat	k5eAaImNgFnS	nazývat
jako	jako	k8xS	jako
Symbian	Symbian	k1gInSc1	Symbian
OS	OS	kA	OS
5	[number]	k4	5
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc4	tento
označení	označení	k1gNnSc4	označení
však	však	k9	však
příliš	příliš	k6eAd1	příliš
nezlidovělo	zlidovět	k5eNaPmAgNnS	zlidovět
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
mobilní	mobilní	k2eAgInSc4d1	mobilní
telefon	telefon	k1gInSc4	telefon
využívající	využívající	k2eAgInSc4d1	využívající
ER	ER	kA	ER
<g/>
5	[number]	k4	5
<g/>
u	u	k7c2	u
<g/>
,	,	kIx,	,
Ericsson	Ericsson	kA	Ericsson
R380	R380	k1gFnSc4	R380
byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgInS	představit
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Nešlo	jít	k5eNaImAgNnS	jít
však	však	k9	však
ještě	ještě	k6eAd1	ještě
o	o	k7c4	o
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
platformu	platforma	k1gFnSc4	platforma
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
Nešlo	jít	k5eNaImAgNnS	jít
instalovat	instalovat	k5eAaBmF	instalovat
vlastní	vlastní	k2eAgFnPc4d1	vlastní
aplikace	aplikace	k1gFnPc4	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Písmenko	písmenko	k1gNnSc1	písmenko
U	U	kA	U
v	v	k7c6	v
názvu	název	k1gInSc6	název
verze	verze	k1gFnSc2	verze
OS	OS	kA	OS
upozorňovalo	upozorňovat	k5eAaImAgNnS	upozorňovat
na	na	k7c4	na
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
podporoval	podporovat	k5eAaImAgInS	podporovat
Unicode	Unicod	k1gInSc5	Unicod
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
telefon	telefon	k1gInSc1	telefon
s	s	k7c7	s
otevřenou	otevřený	k2eAgFnSc7d1	otevřená
platformou	platforma	k1gFnSc7	platforma
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
9210	[number]	k4	9210
Communicator	Communicator	k1gInSc4	Communicator
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgInS	představit
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Takřka	takřka	k6eAd1	takřka
500	[number]	k4	500
000	[number]	k4	000
bylo	být	k5eAaImAgNnS	být
prodáno	prodat	k5eAaPmNgNnS	prodat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
roce	rok	k1gInSc6	rok
toto	tento	k3xDgNnSc1	tento
číslo	číslo	k1gNnSc1	číslo
vzrostlo	vzrůst	k5eAaPmAgNnS	vzrůst
2.1	[number]	k4	2.1
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
odlišných	odlišný	k2eAgInPc2d1	odlišný
UI	UI	kA	UI
(	(	kIx(	(
<g/>
user	usrat	k5eAaPmRp2nS	usrat
interface	interface	k1gInSc1	interface
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
všeobecně	všeobecně	k6eAd1	všeobecně
použitelný	použitelný	k2eAgInSc1d1	použitelný
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
smartphony	smartphona	k1gFnPc4	smartphona
<g/>
"	"	kIx"	"
i	i	k9	i
komunikátory	komunikátor	k1gInPc4	komunikátor
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
dělené	dělený	k2eAgInPc1d1	dělený
na	na	k7c4	na
zařízení	zařízení	k1gNnSc4	zařízení
s	s	k7c7	s
klávesnicí	klávesnice	k1gFnSc7	klávesnice
nebo	nebo	k8xC	nebo
dotykovou	dotykový	k2eAgFnSc7d1	dotyková
obrazovkou	obrazovka	k1gFnSc7	obrazovka
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
osvědčené	osvědčený	k2eAgFnPc1d1	osvědčená
UI	UI	kA	UI
byly	být	k5eAaImAgInP	být
potom	potom	k6eAd1	potom
dále	daleko	k6eAd2	daleko
rozšířeny	rozšířen	k2eAgFnPc1d1	rozšířena
-	-	kIx~	-
Quartz	Quartz	k1gMnSc1	Quartz
a	a	k8xC	a
Crystal	Crystal	k1gMnSc1	Crystal
<g/>
.	.	kIx.	.
</s>
<s>
Sloučením	sloučení	k1gNnSc7	sloučení
s	s	k7c7	s
konstrukcí	konstrukce	k1gFnSc7	konstrukce
vytvořenou	vytvořený	k2eAgFnSc7d1	vytvořená
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
Ericssonem	Ericsson	k1gInSc7	Ericsson
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
UIQ	UIQ	kA	UIQ
interface	interface	k1gInSc4	interface
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
uvedeno	uveden	k2eAgNnSc1d1	uvedeno
jako	jako	k8xS	jako
Nokia	Nokia	kA	Nokia
Series	Series	k1gInSc4	Series
80	[number]	k4	80
UI	UI	kA	UI
<g/>
.	.	kIx.	.
</s>
<s>
UI	UI	kA	UI
označováno	označován	k2eAgNnSc1d1	označováno
jako	jako	k8xS	jako
Nokia	Nokia	kA	Nokia
Series	Series	k1gInSc4	Series
60	[number]	k4	60
<g/>
,	,	kIx,	,
sloužilo	sloužit	k5eAaImAgNnS	sloužit
jako	jako	k9	jako
základ	základ	k1gInSc4	základ
pro	pro	k7c4	pro
první	první	k4xOgFnSc4	první
smartphone	smartphon	k1gInSc5	smartphon
s	s	k7c7	s
klávesnicí	klávesnice	k1gFnSc7	klávesnice
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
uveden	uvést	k5eAaPmNgInS	uvést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
verzi	verze	k1gFnSc4	verze
Symbian	Symbiana	k1gFnPc2	Symbiana
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
jako	jako	k8xS	jako
uživatelské	uživatelský	k2eAgNnSc1d1	Uživatelské
rozhraní	rozhraní	k1gNnSc1	rozhraní
v	v	k7c6	v
telefonech	telefon	k1gInPc6	telefon
UIQ	UIQ	kA	UIQ
(	(	kIx(	(
<g/>
Sony	Sony	kA	Sony
Ericsson	Ericsson	kA	Ericsson
P	P	kA	P
<g/>
800	[number]	k4	800
<g/>
,	,	kIx,	,
P	P	kA	P
<g/>
900	[number]	k4	900
<g/>
,	,	kIx,	,
P	P	kA	P
<g/>
910	[number]	k4	910
<g/>
,	,	kIx,	,
P	P	kA	P
<g/>
1	[number]	k4	1
<g/>
i	i	k8xC	i
<g/>
,	,	kIx,	,
Motorola	Motorola	kA	Motorola
A	a	k9	a
<g/>
925	[number]	k4	925
<g/>
,	,	kIx,	,
A	a	k8xC	a
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1000	[number]	k4	1000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Series	Series	k1gInSc1	Series
80	[number]	k4	80
(	(	kIx(	(
<g/>
Nokia	Nokia	kA	Nokia
9300	[number]	k4	9300
<g/>
,	,	kIx,	,
9500	[number]	k4	9500
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Series	Series	k1gInSc1	Series
90	[number]	k4	90
(	(	kIx(	(
<g/>
Nokia	Nokia	kA	Nokia
7710	[number]	k4	7710
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Series	Series	k1gInSc1	Series
60	[number]	k4	60
(	(	kIx(	(
<g/>
Nokia	Nokia	kA	Nokia
3230	[number]	k4	3230
<g/>
,	,	kIx,	,
6600	[number]	k4	6600
<g/>
,	,	kIx,	,
7310	[number]	k4	7310
<g/>
)	)	kIx)	)
ale	ale	k8xC	ale
v	v	k7c6	v
telefonech	telefon	k1gInPc6	telefon
FOMA	FOMA	kA	FOMA
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
byl	být	k5eAaImAgInS	být
podporován	podporovat	k5eAaImNgInS	podporovat
rychlejší	rychlý	k2eAgInSc1d2	rychlejší
přenos	přenos	k1gInSc1	přenos
dat	datum	k1gNnPc2	datum
EDGE	EDGE	kA	EDGE
<g/>
.	.	kIx.	.
</s>
<s>
Podpora	podpora	k1gFnSc1	podpora
Javy	Java	k1gFnSc2	Java
byla	být	k5eAaImAgFnS	být
změněna	změnit	k5eAaPmNgFnS	změnit
z	z	k7c2	z
Java	Javum	k1gNnSc2	Javum
a	a	k8xC	a
JavaPhone	JavaPhon	k1gInSc5	JavaPhon
na	na	k7c4	na
verzi	verze	k1gFnSc4	verze
Java	Javus	k1gMnSc2	Javus
ME	ME	kA	ME
Standard	standard	k1gInSc1	standard
<g/>
.	.	kIx.	.
</s>
<s>
Symbian	Symbian	k1gInSc1	Symbian
OS	OS	kA	OS
7.0	[number]	k4	7.0
<g/>
s	s	k7c7	s
byla	být	k5eAaImAgFnS	být
speciální	speciální	k2eAgFnPc4d1	speciální
verze	verze	k1gFnPc4	verze
OS	OS	kA	OS
7.0	[number]	k4	7.0
pro	pro	k7c4	pro
lepší	dobrý	k2eAgFnSc4d2	lepší
zpětnou	zpětný	k2eAgFnSc4d1	zpětná
kompatibilitu	kompatibilita	k1gFnSc4	kompatibilita
s	s	k7c7	s
Symbian	Symbiana	k1gFnPc2	Symbiana
OS	OS	kA	OS
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
x	x	k?	x
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
také	také	k9	také
pro	pro	k7c4	pro
kompatibilitu	kompatibilita	k1gFnSc4	kompatibilita
mezi	mezi	k7c7	mezi
Nokia	Nokia	kA	Nokia
Communicatorem	Communicator	k1gInSc7	Communicator
9500	[number]	k4	9500
jeho	jeho	k3xOp3gNnSc1	jeho
předchůdcem	předchůdce	k1gMnSc7	předchůdce
Nokia	Nokia	kA	Nokia
Communicator	Communicator	k1gInSc4	Communicator
9210	[number]	k4	9210
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
Psion	Psion	kA	Psion
prodal	prodat	k5eAaPmAgInS	prodat
svůj	svůj	k3xOyFgInSc4	svůj
podíl	podíl	k1gInSc4	podíl
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
Symbian	Symbiana	k1gFnPc2	Symbiana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
první	první	k4xOgInSc1	první
virus	virus	k1gInSc1	virus
pro	pro	k7c4	pro
telefony	telefon	k1gInPc4	telefon
se	se	k3xPyFc4	se
Symbian	Symbian	k1gInSc1	Symbian
OS	OS	kA	OS
Cabir	Cabir	k1gInSc1	Cabir
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jiné	jiný	k2eAgInPc4d1	jiný
telefony	telefon	k1gInPc4	telefon
se	se	k3xPyFc4	se
šířil	šířit	k5eAaImAgInS	šířit
automaticky	automaticky	k6eAd1	automaticky
pomocí	pomocí	k7c2	pomocí
rozhraní	rozhraní	k1gNnSc2	rozhraní
Bluetooth	Bluetootha	k1gFnPc2	Bluetootha
<g/>
.	.	kIx.	.
</s>
<s>
Uveden	uveden	k2eAgMnSc1d1	uveden
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k6eAd1	jednou
s	s	k7c7	s
výhod	výhoda	k1gFnPc2	výhoda
byla	být	k5eAaImAgFnS	být
možnost	možnost	k1gFnSc1	možnost
výběru	výběr	k1gInSc2	výběr
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgNnPc7	dva
jádry	jádro	k1gNnPc7	jádro
(	(	kIx(	(
<g/>
EKA	EKA	kA	EKA
<g/>
1	[number]	k4	1
nebo	nebo	k8xC	nebo
EKA	EKA	kA	EKA
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
verze	verze	k1gFnSc1	verze
jádra	jádro	k1gNnSc2	jádro
EKA2	EKA2	k1gFnSc2	EKA2
nebyla	být	k5eNaImAgFnS	být
využita	využít	k5eAaPmNgFnS	využít
do	do	k7c2	do
příchodu	příchod	k1gInSc2	příchod
Symbian	Symbiana	k1gFnPc2	Symbiana
OS	OS	kA	OS
8.1	[number]	k4	8.1
<g/>
b.	b.	k?	b.
Jádra	jádro	k1gNnSc2	jádro
se	se	k3xPyFc4	se
chovala	chovat	k5eAaImAgFnS	chovat
navenek	navenek	k6eAd1	navenek
zcela	zcela	k6eAd1	zcela
identicky	identicky	k6eAd1	identicky
a	a	k8xC	a
uživatel	uživatel	k1gMnSc1	uživatel
rozdíl	rozdíl	k1gInSc4	rozdíl
nepoznal	poznat	k5eNaPmAgMnS	poznat
<g/>
.	.	kIx.	.
</s>
<s>
EKA1	EKA1	k4	EKA1
bylo	být	k5eAaImAgNnS	být
výrobci	výrobce	k1gMnPc7	výrobce
užíváno	užíván	k2eAgNnSc1d1	užíváno
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
kompatibilitu	kompatibilita	k1gFnSc4	kompatibilita
se	s	k7c7	s
staršími	starý	k2eAgInPc7d2	starší
ovladači	ovladač	k1gInPc7	ovladač
<g/>
,	,	kIx,	,
EKA2	EKA2	k1gFnPc7	EKA2
bylo	být	k5eAaImAgNnS	být
realtime	realtimat	k5eAaPmIp3nS	realtimat
jádro	jádro	k1gNnSc1	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
podpora	podpora	k1gFnSc1	podpora
CDMA	CDMA	kA	CDMA
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
G	G	kA	G
<g/>
,	,	kIx,	,
DVB-H	DVB-H	k1gFnSc1	DVB-H
a	a	k8xC	a
OpenGL	OpenGL	k1gFnSc1	OpenGL
ES	es	k1gNnPc2	es
s	s	k7c7	s
vektorovou	vektorový	k2eAgFnSc7d1	vektorová
grafikou	grafika	k1gFnSc7	grafika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
"	"	kIx"	"
<g/>
vychytaná	vychytaný	k2eAgFnSc1d1	vychytaná
<g/>
"	"	kIx"	"
verze	verze	k1gFnSc1	verze
8.0	[number]	k4	8.0
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
verzích	verze	k1gFnPc6	verze
8.1	[number]	k4	8.1
<g/>
a	a	k8xC	a
a	a	k8xC	a
8.1	[number]	k4	8.1
<g/>
b.	b.	k?	b.
Verze	verze	k1gFnSc1	verze
8.1	[number]	k4	8.1
<g/>
b	b	k?	b
s	s	k7c7	s
jádrem	jádro	k1gNnSc7	jádro
EKA2	EKA2	k1gFnSc2	EKA2
byla	být	k5eAaImAgFnS	být
populární	populární	k2eAgFnSc1d1	populární
u	u	k7c2	u
japonských	japonský	k2eAgMnPc2d1	japonský
výrobců	výrobce	k1gMnPc2	výrobce
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
byla	být	k5eAaImAgFnS	být
vývojová	vývojový	k2eAgFnSc1d1	vývojová
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
9.0	[number]	k4	9.0
bylo	být	k5eAaImAgNnS	být
naposled	naposled	k6eAd1	naposled
použito	použit	k2eAgNnSc1d1	použito
jádro	jádro	k1gNnSc1	jádro
EKA	EKA	kA	EKA
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Uveden	uveden	k2eAgMnSc1d1	uveden
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Podporuje	podporovat	k5eAaImIp3nS	podporovat
vyšší	vysoký	k2eAgFnSc4d2	vyšší
úroveň	úroveň	k1gFnSc4	úroveň
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Nokia	Nokia	kA	Nokia
platforma	platforma	k1gFnSc1	platforma
S60	S60	k1gFnSc1	S60
3	[number]	k4	3
<g/>
rd	rd	k?	rd
Edition	Edition	k1gInSc1	Edition
užívá	užívat	k5eAaImIp3nS	užívat
tuto	tento	k3xDgFnSc4	tento
verzi	verze	k1gFnSc4	verze
OS	OS	kA	OS
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
také	také	k9	také
pro	pro	k7c4	pro
telefony	telefon	k1gInPc4	telefon
Sony	Sony	kA	Sony
Ericsson	Ericsson	kA	Ericsson
M600	M600	k1gMnSc7	M600
a	a	k8xC	a
P	P	kA	P
<g/>
990	[number]	k4	990
<g/>
.	.	kIx.	.
</s>
<s>
Podpora	podpora	k1gFnSc1	podpora
Bluetooth	Bluetooth	k1gInSc1	Bluetooth
2.0	[number]	k4	2.0
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
1.2	[number]	k4	1.2
<g/>
)	)	kIx)	)
Uvedení	uvedení	k1gNnSc4	uvedení
Q1	Q1	k1gFnSc2	Q1
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Podpora	podpora	k1gFnSc1	podpora
OMA	OMA	kA	OMA
Device	device	k1gInSc1	device
Management	management	k1gInSc1	management
1.2	[number]	k4	1.2
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
1.1	[number]	k4	1.1
<g/>
.2	.2	k4	.2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nokia	Nokia	kA	Nokia
telefony	telefon	k1gInPc1	telefon
s	s	k7c7	s
Symbian	Symbiana	k1gFnPc2	Symbiana
OS	OS	kA	OS
9.2	[number]	k4	9.2
OS	OS	kA	OS
<g/>
:	:	kIx,	:
Nokia	Nokia	kA	Nokia
N	N	kA	N
<g/>
75	[number]	k4	75
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
N	N	kA	N
<g/>
76	[number]	k4	76
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
6120	[number]	k4	6120
Classic	Classice	k1gInPc2	Classice
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
E	E	kA	E
<g/>
90	[number]	k4	90
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
N	N	kA	N
<g/>
95	[number]	k4	95
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
5700	[number]	k4	5700
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
N	N	kA	N
<g/>
81	[number]	k4	81
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
E	E	kA	E
<g/>
51	[number]	k4	51
<g/>
.	.	kIx.	.
</s>
<s>
Uveden	uveden	k2eAgMnSc1d1	uveden
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Zlepšená	zlepšený	k2eAgFnSc1d1	zlepšená
správa	správa	k1gFnSc1	správa
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
podpora	podpora	k1gFnSc1	podpora
Wifi	Wifi	k1gNnSc1	Wifi
802.11	[number]	k4	802.11
<g/>
b	b	k?	b
<g/>
/	/	kIx~	/
<g/>
g	g	kA	g
<g/>
,	,	kIx,	,
HSPDA	HSPDA	kA	HSPDA
<g/>
.	.	kIx.	.
</s>
<s>
Nokia	Nokia	kA	Nokia
E	E	kA	E
<g/>
52	[number]	k4	52
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
5630	[number]	k4	5630
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
5730	[number]	k4	5730
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
6220	[number]	k4	6220
classic	classice	k1gInPc2	classice
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
C	C	kA	C
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Uveden	uveden	k2eAgMnSc1d1	uveden
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Podpora	podpora	k1gFnSc1	podpora
digitální	digitální	k2eAgFnSc2d1	digitální
televize	televize	k1gFnSc2	televize
a	a	k8xC	a
lokalizačních	lokalizační	k2eAgFnPc2d1	lokalizační
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
podpora	podpora	k1gFnSc1	podpora
přepínání	přepínání	k1gNnSc2	přepínání
mezi	mezi	k7c7	mezi
sítěmi	síť	k1gFnPc7	síť
mobilních	mobilní	k2eAgMnPc2d1	mobilní
operátorů	operátor	k1gMnPc2	operátor
a	a	k8xC	a
Wi-Fi	Wi-Fi	k1gNnPc7	Wi-Fi
<g/>
,	,	kIx,	,
podpora	podpora	k1gFnSc1	podpora
dotykových	dotykový	k2eAgInPc2d1	dotykový
displejů	displej	k1gInPc2	displej
<g/>
,	,	kIx,	,
menší	malý	k2eAgInPc4d2	menší
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
hardware	hardware	k1gInSc4	hardware
než	než	k8xS	než
doposud	doposud	k6eAd1	doposud
<g/>
.	.	kIx.	.
</s>
<s>
Nokia	Nokia	kA	Nokia
telefony	telefon	k1gInPc1	telefon
s	s	k7c7	s
Symbian	Symbiana	k1gFnPc2	Symbiana
OS	OS	kA	OS
9.4	[number]	k4	9.4
OS	OS	kA	OS
<g/>
:	:	kIx,	:
Nokia	Nokia	kA	Nokia
5800	[number]	k4	5800
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
N	N	kA	N
<g/>
97	[number]	k4	97
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
N	N	kA	N
<g/>
98	[number]	k4	98
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
5530	[number]	k4	5530
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
C	C	kA	C
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
...	...	k?	...
Uvedení	uvedení	k1gNnSc4	uvedení
1Q	[number]	k4	1Q
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Podpora	podpora	k1gFnSc1	podpora
HDMI	HDMI	kA	HDMI
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
obrazovek	obrazovka	k1gFnPc2	obrazovka
(	(	kIx(	(
<g/>
maximálně	maximálně	k6eAd1	maximálně
3	[number]	k4	3
<g/>
)	)	kIx)	)
Nokia	Nokia	kA	Nokia
N	N	kA	N
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
E	E	kA	E
<g/>
7	[number]	k4	7
<g/>
...	...	k?	...
Rychlejší	rychlý	k2eAgInSc1d2	rychlejší
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
menší	malý	k2eAgInPc1d2	menší
nároky	nárok	k1gInPc1	nárok
na	na	k7c6	na
HW	HW	kA	HW
konfiguraci	konfigurace	k1gFnSc6	konfigurace
<g/>
.	.	kIx.	.
</s>
<s>
Nokia	Nokia	kA	Nokia
X	X	kA	X
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
E	E	kA	E
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
500	[number]	k4	500
<g/>
...	...	k?	...
Představení	představení	k1gNnSc4	představení
3Q	[number]	k4	3Q
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Přepracované	přepracovaný	k2eAgNnSc1d1	přepracované
uživatelské	uživatelský	k2eAgNnSc1d1	Uživatelské
prostředí	prostředí	k1gNnSc1	prostředí
postavené	postavený	k2eAgNnSc1d1	postavené
na	na	k7c6	na
QT	QT	kA	QT
<g/>
.	.	kIx.	.
</s>
<s>
Rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
a	a	k8xC	a
stabilnější	stabilní	k2eAgMnSc1d2	stabilnější
<g/>
.	.	kIx.	.
</s>
<s>
Možnost	možnost	k1gFnSc1	možnost
až	až	k9	až
6	[number]	k4	6
domovských	domovský	k2eAgFnPc2d1	domovská
obrazovek	obrazovka	k1gFnPc2	obrazovka
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
uspořádat	uspořádat	k5eAaPmF	uspořádat
domácí	domácí	k2eAgFnSc4d1	domácí
obrazovku	obrazovka	k1gFnSc4	obrazovka
jednotlivě	jednotlivě	k6eAd1	jednotlivě
(	(	kIx(	(
<g/>
ne	ne	k9	ne
po	po	k7c6	po
panelech	panel	k1gInPc6	panel
jako	jako	k8xS	jako
u	u	k7c2	u
S	s	k7c7	s
<g/>
^	^	kIx~	^
<g/>
3	[number]	k4	3
a	a	k8xC	a
Anny	Anna	k1gFnPc1	Anna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Verze	verze	k1gFnSc1	verze
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
označována	označovat	k5eAaImNgFnS	označovat
pouze	pouze	k6eAd1	pouze
jako	jako	k8xC	jako
Nokia	Nokia	kA	Nokia
Belle	bell	k1gInSc5	bell
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
značky	značka	k1gFnSc2	značka
Symbian	Symbiany	k1gInPc2	Symbiany
<g/>
.	.	kIx.	.
</s>
<s>
Nokia	Nokia	kA	Nokia
600	[number]	k4	600
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
603	[number]	k4	603
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
700	[number]	k4	700
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
701	[number]	k4	701
Představení	představení	k1gNnSc4	představení
1Q	[number]	k4	1Q
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xS	jako
Symbian	Symbian	k1gMnSc1	Symbian
Carla	Carla	k1gMnSc1	Carla
<g/>
.	.	kIx.	.
</s>
<s>
Zrychlené	zrychlený	k2eAgNnSc1d1	zrychlené
prostředí	prostředí	k1gNnSc1	prostředí
prodělalo	prodělat	k5eAaPmAgNnS	prodělat
kosmetické	kosmetický	k2eAgFnPc4d1	kosmetická
změny	změna	k1gFnPc4	změna
<g/>
.	.	kIx.	.
</s>
<s>
Přidání	přidání	k1gNnSc1	přidání
plnohodnotné	plnohodnotný	k2eAgFnSc2d1	plnohodnotná
podpory	podpora	k1gFnSc2	podpora
HTML5	HTML5	k1gFnSc2	HTML5
a	a	k8xC	a
zvukové	zvukový	k2eAgNnSc1d1	zvukové
vylepšení	vylepšení	k1gNnSc1	vylepšení
Dolby	Dolba	k1gFnSc2	Dolba
Headphone	Headphon	k1gMnSc5	Headphon
<g/>
.	.	kIx.	.
</s>
<s>
Nokia	Nokia	kA	Nokia
808	[number]	k4	808
Představení	představení	k1gNnSc4	představení
3Q	[number]	k4	3Q
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
mírná	mírný	k2eAgFnSc1d1	mírná
úprava	úprava	k1gFnSc1	úprava
původního	původní	k2eAgInSc2d1	původní
Symbianu	Symbian	k1gInSc2	Symbian
Belle	bell	k1gInSc5	bell
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
widgety	widget	k1gInPc1	widget
<g/>
,	,	kIx,	,
webový	webový	k2eAgInSc1d1	webový
prohlížeč	prohlížeč	k1gInSc1	prohlížeč
a	a	k8xC	a
hudební	hudební	k2eAgInSc1d1	hudební
přehrávač	přehrávač	k1gInSc1	přehrávač
<g/>
.	.	kIx.	.
</s>
<s>
Určen	určen	k2eAgMnSc1d1	určen
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
se	s	k7c7	s
systémy	systém	k1gInPc7	systém
Symbian	Symbiany	k1gInPc2	Symbiany
3	[number]	k4	3
a	a	k8xC	a
Symbian	Symbian	k1gMnSc1	Symbian
Anna	Anna	k1gFnSc1	Anna
<g/>
.	.	kIx.	.
</s>
<s>
Představení	představení	k1gNnSc1	představení
3Q	[number]	k4	3Q
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xC	jako
Symbian	Symbian	k1gMnSc1	Symbian
Donna	donna	k1gFnSc1	donna
<g/>
.	.	kIx.	.
</s>
<s>
Klávesnice	klávesnice	k1gFnSc1	klávesnice
získala	získat	k5eAaPmAgFnS	získat
upravené	upravený	k2eAgNnSc4d1	upravené
rozložení	rozložení	k1gNnSc4	rozložení
písmen	písmeno	k1gNnPc2	písmeno
<g/>
,	,	kIx,	,
výrazných	výrazný	k2eAgFnPc2d1	výrazná
změn	změna	k1gFnPc2	změna
se	se	k3xPyFc4	se
také	také	k9	také
dostalo	dostat	k5eAaPmAgNnS	dostat
prediktivnímu	prediktivní	k2eAgNnSc3d1	prediktivní
vyhledávání	vyhledávání	k1gNnSc3	vyhledávání
psaných	psaný	k2eAgNnPc2d1	psané
slov	slovo	k1gNnPc2	slovo
a	a	k8xC	a
opravám	oprava	k1gFnPc3	oprava
chyb	chyba	k1gFnPc2	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Změn	změna	k1gFnPc2	změna
se	se	k3xPyFc4	se
dočkalo	dočkat	k5eAaPmAgNnS	dočkat
také	také	k9	také
odemykání	odemykání	k1gNnSc1	odemykání
telefonu	telefon	k1gInSc2	telefon
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
k	k	k7c3	k
odemknutí	odemknutí	k1gNnSc3	odemknutí
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
provést	provést	k5eAaPmF	provést
Swipe	Swip	k1gInSc5	Swip
gesto	gesto	k1gNnSc4	gesto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
domovské	domovský	k2eAgFnSc6d1	domovská
obrazovce	obrazovka	k1gFnSc6	obrazovka
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
liště	lišta	k1gFnSc6	lišta
přibylo	přibýt	k5eAaPmAgNnS	přibýt
tlačítko	tlačítko	k1gNnSc1	tlačítko
pro	pro	k7c4	pro
vyhledávání	vyhledávání	k1gNnSc4	vyhledávání
<g/>
.	.	kIx.	.
</s>
<s>
Modernizací	modernizace	k1gFnSc7	modernizace
také	také	k9	také
prošly	projít	k5eAaPmAgFnP	projít
aplikace	aplikace	k1gFnPc1	aplikace
jako	jako	k8xS	jako
Hudba	hudba	k1gFnSc1	hudba
nebo	nebo	k8xC	nebo
Fotoaparát	fotoaparát	k1gInSc1	fotoaparát
<g/>
,	,	kIx,	,
další	další	k2eAgMnPc1d1	další
dostali	dostat	k5eAaPmAgMnP	dostat
dílčí	dílčí	k2eAgFnPc4d1	dílčí
aktualizace	aktualizace	k1gFnPc4	aktualizace
<g/>
.	.	kIx.	.
</s>
<s>
Určen	určen	k2eAgMnSc1d1	určen
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
se	s	k7c7	s
systémy	systém	k1gInPc7	systém
Symbian	Symbiany	k1gInPc2	Symbiany
Belle	bell	k1gInSc5	bell
a	a	k8xC	a
Symbian	Symbian	k1gMnSc1	Symbian
Belle	bell	k1gInSc5	bell
FP	FP	kA	FP
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Mobilní	mobilní	k2eAgInSc1d1	mobilní
telefon	telefon	k1gInSc1	telefon
Smartphone	Smartphon	k1gInSc5	Smartphon
Windows	Windows	kA	Windows
Mobile	mobile	k1gNnSc4	mobile
PalmOS	PalmOS	k1gFnSc2	PalmOS
iPhone	iPhon	k1gInSc5	iPhon
OS	OS	kA	OS
Android	android	k1gInSc4	android
bada	bada	k6eAd1	bada
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Symbian	Symbiana	k1gFnPc2	Symbiana
OS	OS	kA	OS
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
České	český	k2eAgFnSc2d1	Česká
stránky	stránka	k1gFnSc2	stránka
o	o	k7c6	o
telefonech	telefon	k1gInPc6	telefon
s	s	k7c7	s
OS	OS	kA	OS
Symbian	Symbian	k1gInSc1	Symbian
Belle	bell	k1gInSc5	bell
</s>
