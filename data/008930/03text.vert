<p>
<s>
Michel	Michel	k1gMnSc1	Michel
de	de	k?	de
Nostredame	Nostredam	k1gInSc5	Nostredam
zvaný	zvaný	k2eAgInSc1d1	zvaný
Nostradamus	Nostradamus	k1gInSc4	Nostradamus
(	(	kIx(	(
<g/>
*	*	kIx~	*
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
/	/	kIx~	/
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
-	-	kIx~	-
gregor	gregor	k1gInSc1	gregor
<g/>
.	.	kIx.	.
kal	kal	k1gInSc1	kal
<g/>
.	.	kIx.	.
1503	[number]	k4	1503
Saint-Rémy-de-Provence	Saint-Rémye-Provence	k1gFnSc2	Saint-Rémy-de-Provence
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
greg	greg	k1gInSc1	greg
<g/>
.	.	kIx.	.
<g/>
kal	kal	k1gInSc1	kal
<g/>
.	.	kIx.	.
1566	[number]	k4	1566
Salon-de-Provence	Salone-Provence	k1gFnSc2	Salon-de-Provence
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
proslul	proslout	k5eAaPmAgMnS	proslout
jako	jako	k9	jako
věštec	věštec	k1gMnSc1	věštec
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
konvertovali	konvertovat	k5eAaBmAgMnP	konvertovat
ke	k	k7c3	k
křesťanství	křesťanství	k1gNnSc3	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
patnácti	patnáct	k4xCc6	patnáct
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
studoval	studovat	k5eAaImAgMnS	studovat
v	v	k7c6	v
Avignonu	Avignon	k1gInSc6	Avignon
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
již	již	k6eAd1	již
zkušenosti	zkušenost	k1gFnPc4	zkušenost
s	s	k7c7	s
astrologií	astrologie	k1gFnSc7	astrologie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1529	[number]	k4	1529
začal	začít	k5eAaPmAgInS	začít
studovat	studovat	k5eAaImF	studovat
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
vědu	věda	k1gFnSc4	věda
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Montpellieru	Montpellier	k1gInSc6	Montpellier
(	(	kIx(	(
<g/>
věnoval	věnovat	k5eAaImAgMnS	věnovat
se	s	k7c7	s
zejm.	zejm.	k?	zejm.
starým	starý	k2eAgInPc3d1	starý
lékařským	lékařský	k2eAgInPc3d1	lékařský
a	a	k8xC	a
filozofickým	filozofický	k2eAgInPc3d1	filozofický
textům	text	k1gInPc3	text
<g/>
,	,	kIx,	,
anatomii	anatomie	k1gFnSc3	anatomie
a	a	k8xC	a
lékopisu	lékopis	k1gInSc3	lékopis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgMnS	být
ze	z	k7c2	z
studia	studio	k1gNnSc2	studio
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
zdokonaloval	zdokonalovat	k5eAaImAgMnS	zdokonalovat
v	v	k7c6	v
Agenu	Agen	k1gInSc6	Agen
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
lékaře	lékař	k1gMnSc2	lékař
a	a	k8xC	a
filologa	filolog	k1gMnSc2	filolog
Julese-Césara	Julese-César	k1gMnSc2	Julese-César
Scaligera	Scaliger	k1gMnSc2	Scaliger
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
a	a	k8xC	a
dvě	dva	k4xCgFnPc1	dva
děti	dítě	k1gFnPc1	dítě
podlehly	podlehnout	k5eAaPmAgFnP	podlehnout
morové	morový	k2eAgFnSc6d1	morová
epidemii	epidemie	k1gFnSc6	epidemie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejich	jejich	k3xOp3gFnSc6	jejich
smrti	smrt	k1gFnSc6	smrt
počal	počnout	k5eAaPmAgMnS	počnout
Nostradamus	Nostradamus	k1gMnSc1	Nostradamus
studovat	studovat	k5eAaImF	studovat
morovou	morový	k2eAgFnSc4d1	morová
problematiku	problematika	k1gFnSc4	problematika
v	v	k7c4	v
Marseilles	Marseilles	k1gInSc4	Marseilles
a	a	k8xC	a
v	v	k7c4	v
Aix-en-Provence	Aixn-Provence	k1gFnPc4	Aix-en-Provence
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
odborník	odborník	k1gMnSc1	odborník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
současníků	současník	k1gMnPc2	současník
nevěřil	věřit	k5eNaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mor	mor	k1gInSc1	mor
je	být	k5eAaImIp3nS	být
nezvratným	zvratný	k2eNgInSc7d1	nezvratný
Božím	boží	k2eAgInSc7d1	boží
trestem	trest	k1gInSc7	trest
<g/>
.	.	kIx.	.
</s>
<s>
Snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
vyrobit	vyrobit	k5eAaPmF	vyrobit
lék	lék	k1gInSc1	lék
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
cypřiše	cypřiš	k1gFnSc2	cypřiš
<g/>
,	,	kIx,	,
kosatce	kosatec	k1gInSc2	kosatec
<g/>
,	,	kIx,	,
červené	červený	k2eAgFnSc2d1	červená
růže	růž	k1gFnSc2	růž
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
složek	složka	k1gFnPc2	složka
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
všichni	všechen	k3xTgMnPc1	všechen
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
si	se	k3xPyFc3	se
směsí	směs	k1gFnSc7	směs
potřou	potřít	k5eAaPmIp3nP	potřít
rty	ret	k1gInPc4	ret
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
před	před	k7c7	před
morem	mor	k1gInSc7	mor
chráněni	chránit	k5eAaImNgMnP	chránit
<g/>
.	.	kIx.	.
</s>
<s>
Navrhoval	navrhovat	k5eAaImAgInS	navrhovat
hygienická	hygienický	k2eAgNnPc4d1	hygienické
opatření	opatření	k1gNnPc4	opatření
<g/>
,	,	kIx,	,
radil	radit	k5eAaImAgMnS	radit
polévat	polévat	k5eAaImF	polévat
vše	všechen	k3xTgNnSc4	všechen
octem	ocet	k1gInSc7	ocet
a	a	k8xC	a
výtažky	výtažek	k1gInPc7	výtažek
z	z	k7c2	z
rostlin	rostlina	k1gFnPc2	rostlina
kvůli	kvůli	k7c3	kvůli
dezinfekci	dezinfekce	k1gFnSc3	dezinfekce
<g/>
,	,	kIx,	,
chránit	chránit	k5eAaImF	chránit
si	se	k3xPyFc3	se
nos	nos	k1gInSc4	nos
a	a	k8xC	a
ústa	ústa	k1gNnPc4	ústa
<g/>
,	,	kIx,	,
mýt	mýt	k5eAaImF	mýt
si	se	k3xPyFc3	se
ruce	ruka	k1gFnPc4	ruka
před	před	k7c7	před
jídlem	jídlo	k1gNnSc7	jídlo
atp.	atp.	kA	atp.
</s>
</p>
<p>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
všichni	všechen	k3xTgMnPc1	všechen
tehdejší	tehdejší	k2eAgMnPc1d1	tehdejší
lékaři	lékař	k1gMnPc1	lékař
opíral	opírat	k5eAaImAgInS	opírat
se	se	k3xPyFc4	se
Nostradamus	Nostradamus	k1gInSc1	Nostradamus
při	při	k7c6	při
určování	určování	k1gNnSc6	určování
příčin	příčina	k1gFnPc2	příčina
nemoci	nemoc	k1gFnSc2	nemoc
o	o	k7c4	o
astrologii	astrologie	k1gFnSc4	astrologie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
věřilo	věřit	k5eAaImAgNnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgFnPc1	všechen
části	část	k1gFnPc1	část
těla	tělo	k1gNnSc2	tělo
jsou	být	k5eAaImIp3nP	být
ovlivněny	ovlivnit	k5eAaPmNgFnP	ovlivnit
pohybem	pohyb	k1gInSc7	pohyb
planet	planeta	k1gFnPc2	planeta
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
konstelací	konstelace	k1gFnPc2	konstelace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hvězdách	hvězda	k1gFnPc6	hvězda
zakládá	zakládat	k5eAaImIp3nS	zakládat
svá	svůj	k3xOyFgNnPc4	svůj
proroctví	proroctví	k1gNnPc4	proroctví
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
předpověděl	předpovědět	k5eAaPmAgMnS	předpovědět
téměř	téměř	k6eAd1	téměř
přesně	přesně	k6eAd1	přesně
smrt	smrt	k1gFnSc4	smrt
Jindřicha	Jindřich	k1gMnSc2	Jindřich
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
si	se	k3xPyFc3	se
důvěru	důvěra	k1gFnSc4	důvěra
Kateřiny	Kateřina	k1gFnSc2	Kateřina
Medičejské	Medičejský	k2eAgFnSc2d1	Medičejská
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jej	on	k3xPp3gMnSc4	on
přijala	přijmout	k5eAaPmAgFnS	přijmout
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholu	vrchol	k1gInSc3	vrchol
kariéry	kariéra	k1gFnSc2	kariéra
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
roku	rok	k1gInSc2	rok
1564	[number]	k4	1564
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
dvorním	dvorní	k2eAgMnSc7d1	dvorní
lékařem	lékař	k1gMnSc7	lékař
a	a	k8xC	a
rádcem	rádce	k1gMnSc7	rádce
krále	král	k1gMnSc2	král
Karla	Karel	k1gMnSc2	Karel
IX	IX	kA	IX
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
Nostradamus	Nostradamus	k1gInSc1	Nostradamus
umírá	umírat	k5eAaImIp3nS	umírat
v	v	k7c4	v
Salon-de-Provence	Salone-Provenec	k1gMnSc4	Salon-de-Provenec
jako	jako	k8xC	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejuznávanějších	uznávaný	k2eAgMnPc2d3	nejuznávanější
mužů	muž	k1gMnPc2	muž
francouzského	francouzský	k2eAgNnSc2d1	francouzské
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Dětství	dětství	k1gNnSc4	dětství
===	===	k?	===
</s>
</p>
<p>
<s>
Narozen	narozen	k2eAgMnSc1d1	narozen
14	[number]	k4	14
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
roku	rok	k1gInSc2	rok
1503	[number]	k4	1503
v	v	k7c4	v
Saint-Rémy-de-Provence	Saint-Rémye-Provenec	k1gMnPc4	Saint-Rémy-de-Provenec
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
dosud	dosud	k6eAd1	dosud
dochován	dochován	k2eAgInSc1d1	dochován
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
<g/>
.	.	kIx.	.
</s>
<s>
Michel	Michel	k1gMnSc1	Michel
de	de	k?	de
Nostredame	Nostredam	k1gInSc5	Nostredam
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
početné	početný	k2eAgFnSc2d1	početná
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Jaume	Jaum	k1gInSc5	Jaum
(	(	kIx(	(
<g/>
Jacques	Jacques	k1gMnSc1	Jacques
<g/>
)	)	kIx)	)
de	de	k?	de
Nostredame	Nostredam	k1gInSc5	Nostredam
(	(	kIx(	(
<g/>
obchodník	obchodník	k1gMnSc1	obchodník
s	s	k7c7	s
obilím	obilí	k1gNnSc7	obilí
a	a	k8xC	a
církevní	církevní	k2eAgMnSc1d1	církevní
notář	notář	k1gMnSc1	notář
<g/>
)	)	kIx)	)
a	a	k8xC	a
matka	matka	k1gFnSc1	matka
Reyniè	Reyniè	k1gFnSc1	Reyniè
(	(	kIx(	(
<g/>
Renée	René	k1gMnPc4	René
<g/>
)	)	kIx)	)
de	de	k?	de
St-Rémy	St-Rém	k1gInPc7	St-Rém
měli	mít	k5eAaImAgMnP	mít
nejméně	málo	k6eAd3	málo
dalších	další	k2eAgInPc2d1	další
osm	osm	k4xCc4	osm
potomků	potomek	k1gMnPc2	potomek
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
nosila	nosit	k5eAaImAgFnS	nosit
původně	původně	k6eAd1	původně
židovské	židovský	k2eAgNnSc4d1	Židovské
příjmení	příjmení	k1gNnSc4	příjmení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
otec	otec	k1gMnSc1	otec
Jaume	Jaum	k1gInSc5	Jaum
<g/>
,	,	kIx,	,
Guy	Guy	k1gMnSc1	Guy
Gassonet	Gassonet	k1gMnSc1	Gassonet
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1455	[number]	k4	1455
konvertoval	konvertovat	k5eAaBmAgMnS	konvertovat
ke	k	k7c3	k
křesťanství	křesťanství	k1gNnSc3	křesťanství
a	a	k8xC	a
přijal	přijmout	k5eAaPmAgMnS	přijmout
křesťanské	křesťanský	k2eAgNnSc4d1	křesťanské
jméno	jméno	k1gNnSc4	jméno
"	"	kIx"	"
<g/>
Pierre	Pierr	k1gInSc5	Pierr
<g/>
"	"	kIx"	"
a	a	k8xC	a
příjmení	příjmení	k1gNnSc4	příjmení
"	"	kIx"	"
<g/>
Nostredame	Nostredam	k1gInSc5	Nostredam
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Michelovi	Michelův	k2eAgMnPc1d1	Michelův
sourozenci	sourozenec	k1gMnPc1	sourozenec
–	–	k?	–
Delphine	Delphin	k1gMnSc5	Delphin
<g/>
,	,	kIx,	,
Jehan	Jehany	k1gInPc2	Jehany
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1507	[number]	k4	1507
<g/>
–	–	k?	–
<g/>
1577	[number]	k4	1577
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pierre	Pierr	k1gMnSc5	Pierr
<g/>
,	,	kIx,	,
Hector	Hector	k1gMnSc1	Hector
<g/>
,	,	kIx,	,
Louis	Louis	k1gMnSc1	Louis
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1522	[number]	k4	1522
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bertrand	Bertrand	k1gInSc1	Bertrand
<g/>
,	,	kIx,	,
Jean	Jean	k1gMnSc1	Jean
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1522	[number]	k4	1522
<g/>
)	)	kIx)	)
a	a	k8xC	a
Antoine	Antoin	k1gInSc5	Antoin
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1523	[number]	k4	1523
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
dětství	dětství	k1gNnSc6	dětství
Nostradama	Nostradamum	k1gNnSc2	Nostradamum
není	být	k5eNaImIp3nS	být
mnoho	mnoho	k6eAd1	mnoho
známo	znám	k2eAgNnSc1d1	známo
<g/>
;	;	kIx,	;
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
vzdělával	vzdělávat	k5eAaImAgMnS	vzdělávat
dědeček	dědeček	k1gMnSc1	dědeček
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
matky	matka	k1gFnSc2	matka
Jean	Jean	k1gMnSc1	Jean
de	de	k?	de
St.	st.	kA	st.
Rémy	Réma	k1gFnSc2	Réma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Studentská	studentský	k2eAgNnPc4d1	studentské
léta	léto	k1gNnPc4	léto
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
patnácti	patnáct	k4xCc6	patnáct
letech	léto	k1gNnPc6	léto
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
Nostradamus	Nostradamus	k1gInSc1	Nostradamus
na	na	k7c4	na
Avignonskou	avignonský	k2eAgFnSc4d1	avignonská
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Avignonu	Avignon	k1gInSc2	Avignon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nP	by
za	za	k7c2	za
běžných	běžný	k2eAgFnPc2d1	běžná
okolností	okolnost	k1gFnPc2	okolnost
dostudoval	dostudovat	k5eAaPmAgInS	dostudovat
trivium	trivium	k1gNnSc1	trivium
(	(	kIx(	(
<g/>
skládalo	skládat	k5eAaImAgNnS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
gramatiky	gramatika	k1gFnSc2	gramatika
<g/>
,	,	kIx,	,
rétoriky	rétorika	k1gFnSc2	rétorika
a	a	k8xC	a
logiky	logika	k1gFnSc2	logika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
nucen	nutit	k5eAaImNgMnS	nutit
odejít	odejít	k5eAaPmF	odejít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
univerzita	univerzita	k1gFnSc1	univerzita
byla	být	k5eAaImAgFnS	být
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
kvůli	kvůli	k7c3	kvůli
propuknutí	propuknutí	k1gNnSc3	propuknutí
morové	morový	k2eAgFnSc2d1	morová
epidemie	epidemie	k1gFnSc2	epidemie
<g/>
.	.	kIx.	.
</s>
<s>
Nostradamus	Nostradamus	k1gInSc1	Nostradamus
poté	poté	k6eAd1	poté
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
svých	svůj	k3xOyFgNnPc2	svůj
vlastních	vlastní	k2eAgNnPc2d1	vlastní
slov	slovo	k1gNnPc2	slovo
<g/>
)	)	kIx)	)
cestoval	cestovat	k5eAaImAgMnS	cestovat
osm	osm	k4xCc4	osm
let	léto	k1gNnPc2	léto
krajem	krajem	k6eAd1	krajem
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1521	[number]	k4	1521
vynalézal	vynalézat	k5eAaImAgInS	vynalézat
léky	lék	k1gInPc7	lék
z	z	k7c2	z
bylinek	bylinka	k1gFnPc2	bylinka
(	(	kIx(	(
<g/>
našimi	náš	k3xOp1gMnPc7	náš
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
amatérsky	amatérsky	k6eAd1	amatérsky
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
lékárnictvím	lékárnictví	k1gNnSc7	lékárnictví
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c6	po
několika	několik	k4yIc6	několik
letech	let	k1gInPc6	let
této	tento	k3xDgFnSc2	tento
činnosti	činnost	k1gFnSc2	činnost
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1529	[number]	k4	1529
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
v	v	k7c4	v
Montpellier	Montpellier	k1gInSc4	Montpellier
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
získat	získat	k5eAaPmF	získat
diplom	diplom	k1gInSc4	diplom
z	z	k7c2	z
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
Zanedlouho	zanedlouho	k6eAd1	zanedlouho
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
univerzity	univerzita	k1gFnSc2	univerzita
vykázán	vykázán	k2eAgMnSc1d1	vykázán
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
lékárníkem	lékárník	k1gMnSc7	lékárník
<g/>
,	,	kIx,	,
což	což	k9	což
stanovy	stanova	k1gFnPc1	stanova
univerzity	univerzita	k1gFnSc2	univerzita
zakazovaly	zakazovat	k5eAaImAgFnP	zakazovat
<g/>
.	.	kIx.	.
</s>
<s>
Listina	listina	k1gFnSc1	listina
o	o	k7c4	o
vyloučení	vyloučení	k1gNnSc4	vyloučení
ze	z	k7c2	z
studia	studio	k1gNnSc2	studio
je	být	k5eAaImIp3nS	být
uložena	uložit	k5eAaPmNgFnS	uložit
ve	v	k7c6	v
fondu	fond	k1gInSc6	fond
fakultní	fakultní	k2eAgFnSc2d1	fakultní
knihovny	knihovna	k1gFnSc2	knihovna
(	(	kIx(	(
<g/>
BIU	BIU	k?	BIU
Montpellier	Montpellier	k1gInSc1	Montpellier
<g/>
,	,	kIx,	,
Registre	registr	k1gInSc5	registr
S	s	k7c7	s
2	[number]	k4	2
folio	folio	k1gNnSc1	folio
87	[number]	k4	87
<g/>
,	,	kIx,	,
et	et	k?	et
voir	voir	k1gMnSc1	voir
ci-dessous	ciessous	k1gMnSc1	ci-dessous
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
bývá	bývat	k5eAaImIp3nS	bývat
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
korespondenci	korespondence	k1gFnSc6	korespondence
či	či	k8xC	či
nakladateli	nakladatel	k1gMnSc6	nakladatel
označován	označovat	k5eAaImNgMnS	označovat
jako	jako	k9	jako
lékař	lékař	k1gMnSc1	lékař
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyloučení	vyloučení	k1gNnSc6	vyloučení
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
Nostradamus	Nostradamus	k1gInSc1	Nostradamus
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jako	jako	k9	jako
lékárník	lékárník	k1gMnSc1	lékárník
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1533	[number]	k4	1533
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
Agen	Agena	k1gFnPc2	Agena
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
praktikoval	praktikovat	k5eAaImAgInS	praktikovat
léčitelství	léčitelství	k1gNnSc4	léčitelství
při	při	k7c6	při
domácím	domácí	k2eAgNnSc6d1	domácí
ošetřování	ošetřování	k1gNnSc6	ošetřování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Agen	Agen	k1gInSc4	Agen
Nostradama	Nostradama	k1gNnSc4	Nostradama
přizval	přizvat	k5eAaPmAgInS	přizvat
ke	k	k7c3	k
spolupráci	spolupráce	k1gFnSc3	spolupráce
Julius	Julius	k1gMnSc1	Julius
Caesar	Caesar	k1gMnSc1	Caesar
Scaliger	Scaliger	k1gMnSc1	Scaliger
<g/>
,	,	kIx,	,
čelní	čelní	k2eAgMnSc1d1	čelní
renesanční	renesanční	k2eAgMnSc1d1	renesanční
učenec	učenec	k1gMnSc1	učenec
<g/>
.	.	kIx.	.
</s>
<s>
Přesná	přesný	k2eAgFnSc1d1	přesná
délka	délka	k1gFnSc1	délka
Nostradamova	Nostradamův	k2eAgInSc2d1	Nostradamův
pobytu	pobyt	k1gInSc2	pobyt
ve	v	k7c6	v
městě	město	k1gNnSc6	město
není	být	k5eNaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
<g/>
;	;	kIx,	;
možná	možná	k9	možná
tři	tři	k4xCgFnPc1	tři
<g/>
,	,	kIx,	,
možná	možná	k9	možná
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Manželství	manželství	k1gNnSc1	manželství
a	a	k8xC	a
léčitelská	léčitelský	k2eAgFnSc1d1	léčitelská
praxe	praxe	k1gFnSc1	praxe
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c4	v
Agen	Agen	k1gNnSc4	Agen
se	se	k3xPyFc4	se
Nostradamus	Nostradamus	k1gMnSc1	Nostradamus
oženil	oženit	k5eAaPmAgMnS	oženit
se	s	k7c7	s
ženou	žena	k1gFnSc7	žena
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
jméno	jméno	k1gNnSc4	jméno
je	být	k5eAaImIp3nS	být
nejisté	jistý	k2eNgNnSc1d1	nejisté
(	(	kIx(	(
<g/>
asi	asi	k9	asi
Henriette	Henriett	k1gInSc5	Henriett
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Encausse	Encausse	k1gFnSc1	Encausse
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
porodila	porodit	k5eAaPmAgFnS	porodit
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc4	syn
a	a	k8xC	a
dceru	dcera	k1gFnSc4	dcera
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1534	[number]	k4	1534
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
i	i	k8xC	i
děti	dítě	k1gFnPc1	dítě
zemřely	zemřít	k5eAaPmAgFnP	zemřít
při	při	k7c6	při
epidemii	epidemie	k1gFnSc6	epidemie
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
moru	mora	k1gFnSc4	mora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1540	[number]	k4	1540
<g/>
–	–	k?	–
<g/>
1545	[number]	k4	1545
Nostradamus	Nostradamus	k1gInSc1	Nostradamus
cestoval	cestovat	k5eAaImAgInS	cestovat
po	po	k7c6	po
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
setkal	setkat	k5eAaPmAgMnS	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
mnohými	mnohý	k2eAgFnPc7d1	mnohá
osobnostmi	osobnost	k1gFnPc7	osobnost
<g/>
,	,	kIx,	,
učenci	učenec	k1gMnPc7	učenec
a	a	k8xC	a
lékaři	lékař	k1gMnPc7	lékař
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
cest	cesta	k1gFnPc2	cesta
roku	rok	k1gInSc2	rok
1545	[number]	k4	1545
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
lékaři	lékař	k1gMnSc3	lékař
jménem	jméno	k1gNnSc7	jméno
Louis	Louis	k1gMnSc1	Louis
Serre	Serr	k1gInSc5	Serr
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
moru	mor	k1gInSc3	mor
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
ohnisku	ohnisko	k1gNnSc6	ohnisko
<g/>
,	,	kIx,	,
v	v	k7c6	v
Marseille	Marseille	k1gFnSc6	Marseille
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
léčil	léčit	k5eAaImAgMnS	léčit
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
ohniscích	ohnisko	k1gNnPc6	ohnisko
nákazy	nákaza	k1gFnSc2	nákaza
<g/>
,	,	kIx,	,
v	v	k7c4	v
Salon-de-Provence	Salone-Provence	k1gFnPc4	Salon-de-Provence
a	a	k8xC	a
v	v	k7c6	v
krajském	krajský	k2eAgNnSc6d1	krajské
městě	město	k1gNnSc6	město
Aix-en-Provence	Aixn-Provence	k1gFnSc2	Aix-en-Provence
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1547	[number]	k4	1547
se	se	k3xPyFc4	se
usadil	usadit	k5eAaPmAgMnS	usadit
v	v	k7c4	v
Salon-de-Provence	Salone-Provence	k1gFnPc4	Salon-de-Provence
<g/>
,	,	kIx,	,
v	v	k7c6	v
domě	dům	k1gInSc6	dům
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
stojí	stát	k5eAaImIp3nS	stát
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
bohatou	bohatý	k2eAgFnSc7d1	bohatá
vdovou	vdova	k1gFnSc7	vdova
jménem	jméno	k1gNnSc7	jméno
Anne	Annus	k1gMnSc5	Annus
Ponsarde	Ponsard	k1gMnSc5	Ponsard
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
měl	mít	k5eAaImAgMnS	mít
šest	šest	k4xCc4	šest
dětí	dítě	k1gFnPc2	dítě
–	–	k?	–
tři	tři	k4xCgFnPc4	tři
dcery	dcera	k1gFnPc4	dcera
a	a	k8xC	a
tři	tři	k4xCgMnPc4	tři
syny	syn	k1gMnPc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
1556	[number]	k4	1556
a	a	k8xC	a
1567	[number]	k4	1567
získal	získat	k5eAaPmAgInS	získat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
jednu	jeden	k4xCgFnSc4	jeden
třináctinu	třináctina	k1gFnSc4	třináctina
podílu	podíl	k1gInSc2	podíl
z	z	k7c2	z
obrovského	obrovský	k2eAgInSc2d1	obrovský
projektu	projekt	k1gInSc2	projekt
zavlažovacího	zavlažovací	k2eAgInSc2d1	zavlažovací
kanálu	kanál	k1gInSc2	kanál
(	(	kIx(	(
<g/>
Canal	Canal	k1gInSc1	Canal
de	de	k?	de
Craponne	Craponn	k1gInSc5	Craponn
<g/>
)	)	kIx)	)
iniciovaného	iniciovaný	k2eAgInSc2d1	iniciovaný
francouzským	francouzský	k2eAgMnSc7d1	francouzský
králem	král	k1gMnSc7	král
Henrym	Henry	k1gMnSc7	Henry
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
projektantem	projektant	k1gMnSc7	projektant
a	a	k8xC	a
realizátorem	realizátor	k1gMnSc7	realizátor
byl	být	k5eAaImAgMnS	být
šlechtic	šlechtic	k1gMnSc1	šlechtic
a	a	k8xC	a
inženýr	inženýr	k1gMnSc1	inženýr
Adam	Adam	k1gMnSc1	Adam
de	de	k?	de
Craponne	Craponn	k1gInSc5	Craponn
<g/>
.	.	kIx.	.
</s>
<s>
Kanálem	kanál	k1gInSc7	kanál
byla	být	k5eAaImAgFnS	být
vedena	vést	k5eAaImNgFnS	vést
voda	voda	k1gFnSc1	voda
z	z	k7c2	z
řeky	řeka	k1gFnSc2	řeka
Durance	durance	k1gFnSc2	durance
na	na	k7c4	na
suchou	suchý	k2eAgFnSc4d1	suchá
planinu	planina	k1gFnSc4	planina
Crau	Craus	k1gInSc2	Craus
<g/>
,	,	kIx,	,
k	k	k7c3	k
městu	město	k1gNnSc3	město
Salon-de-Provence	Salone-Provence	k1gFnSc2	Salon-de-Provence
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nostradamova	Nostradamův	k2eAgNnPc1d1	Nostradamovo
proroctví	proroctví	k1gNnPc1	proroctví
==	==	k?	==
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
úcty	úcta	k1gFnSc2	úcta
a	a	k8xC	a
reputace	reputace	k1gFnSc2	reputace
si	se	k3xPyFc3	se
již	již	k9	již
od	od	k7c2	od
prvního	první	k4xOgNnSc2	první
vydání	vydání	k1gNnSc2	vydání
proroctví	proroctví	k1gNnSc2	proroctví
roku	rok	k1gInSc2	rok
1555	[number]	k4	1555
Nostradamus	Nostradamus	k1gMnSc1	Nostradamus
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
posměch	posměch	k1gInSc4	posměch
a	a	k8xC	a
odpor	odpor	k1gInSc4	odpor
kvůli	kvůli	k7c3	kvůli
chybným	chybný	k2eAgInPc3d1	chybný
výpočtům	výpočet	k1gInPc3	výpočet
a	a	k8xC	a
opsaným	opsaný	k2eAgFnPc3d1	opsaná
interpretacím	interpretace	k1gFnPc3	interpretace
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejzuřivějších	zuřivý	k2eAgMnPc2d3	nejzuřivější
odpůrců	odpůrce	k1gMnPc2	odpůrce
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc1	jeho
bývalý	bývalý	k2eAgMnSc1d1	bývalý
mistr	mistr	k1gMnSc1	mistr
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
Scaliger	Scaliger	k1gMnSc1	Scaliger
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Nostradamovy	Nostradamův	k2eAgFnPc1d1	Nostradamova
předpovědi	předpověď	k1gFnPc1	předpověď
jsou	být	k5eAaImIp3nP	být
snůškou	snůška	k1gFnSc7	snůška
lží	lež	k1gFnPc2	lež
a	a	k8xC	a
výmyslů	výmysl	k1gInPc2	výmysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
Nostradamovo	Nostradamův	k2eAgNnSc1d1	Nostradamovo
jméno	jméno	k1gNnSc1	jméno
vžilo	vžít	k5eAaPmAgNnS	vžít
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
jako	jako	k8xC	jako
synonymum	synonymum	k1gNnSc4	synonymum
pro	pro	k7c4	pro
výrazy	výraz	k1gInPc4	výraz
"	"	kIx"	"
<g/>
kouzelník	kouzelník	k1gMnSc1	kouzelník
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
mág	mág	k1gMnSc1	mág
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
astrolog	astrolog	k1gMnSc1	astrolog
<g/>
"	"	kIx"	"
v	v	k7c6	v
posměšném	posměšný	k2eAgInSc6d1	posměšný
významu	význam	k1gInSc6	význam
(	(	kIx(	(
<g/>
Littrého	Littrý	k2eAgInSc2d1	Littrý
slovník	slovník	k1gInSc4	slovník
<g/>
,	,	kIx,	,
dictionnaire	dictionnair	k1gInSc5	dictionnair
de	de	k?	de
français	français	k1gFnPc7	français
Littré	Littrý	k2eAgFnSc2d1	Littrý
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
výroky	výrok	k1gInPc1	výrok
byly	být	k5eAaImAgInP	být
mj.	mj.	kA	mj.
zneužívány	zneužíván	k2eAgInPc1d1	zneužíván
k	k	k7c3	k
politické	politický	k2eAgFnSc3d1	politická
propagandě	propaganda	k1gFnSc3	propaganda
(	(	kIx(	(
<g/>
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
interpretoval	interpretovat	k5eAaBmAgInS	interpretovat
Goebbels	Goebbels	k1gInSc1	Goebbels
některá	některý	k3yIgNnPc4	některý
jeho	jeho	k3xOp3gNnPc4	jeho
čtyřverší	čtyřverší	k1gNnPc4	čtyřverší
jako	jako	k8xC	jako
předzvěst	předzvěst	k1gFnSc4	předzvěst
porážky	porážka	k1gFnSc2	porážka
spojeneckých	spojenecký	k2eAgNnPc2d1	spojenecké
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
útoku	útok	k1gInSc6	útok
na	na	k7c4	na
World	World	k1gInSc4	World
Trade	Trad	k1gInSc5	Trad
Center	centrum	k1gNnPc2	centrum
se	se	k3xPyFc4	se
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
objevily	objevit	k5eAaPmAgInP	objevit
verše	verš	k1gInPc1	verš
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
údajně	údajně	k6eAd1	údajně
Nostradamus	Nostradamus	k1gMnSc1	Nostradamus
událost	událost	k1gFnSc4	událost
předpověděl	předpovědět	k5eAaPmAgMnS	předpovědět
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
montáž	montáž	k1gFnSc4	montáž
z	z	k7c2	z
několika	několik	k4yIc2	několik
čtyřverší	čtyřverší	k1gNnPc2	čtyřverší
nebo	nebo	k8xC	nebo
o	o	k7c4	o
napodobeninu	napodobenina	k1gFnSc4	napodobenina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
době	doba	k1gFnSc6	doba
existovali	existovat	k5eAaImAgMnP	existovat
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
Nostradama	Nostradam	k1gMnSc4	Nostradam
hájili	hájit	k5eAaImAgMnP	hájit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nich	on	k3xPp3gMnPc2	on
samotné	samotný	k2eAgInPc1d1	samotný
životopisné	životopisný	k2eAgInPc1d1	životopisný
údaje	údaj	k1gInPc1	údaj
svědčí	svědčit	k5eAaImIp3nP	svědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mínil	mínit	k5eAaImAgMnS	mínit
své	svůj	k3xOyFgFnPc4	svůj
předpovědi	předpověď	k1gFnPc4	předpověď
upřímně	upřímně	k6eAd1	upřímně
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
prototypem	prototyp	k1gInSc7	prototyp
renesančního	renesanční	k2eAgMnSc2d1	renesanční
člověka	člověk	k1gMnSc2	člověk
s	s	k7c7	s
širokým	široký	k2eAgInSc7d1	široký
záběrem	záběr	k1gInSc7	záběr
<g/>
.	.	kIx.	.
</s>
<s>
Věnoval	věnovat	k5eAaImAgInS	věnovat
se	se	k3xPyFc4	se
lékařské	lékařský	k2eAgFnSc3d1	lékařská
vědě	věda	k1gFnSc3	věda
<g/>
,	,	kIx,	,
astrologii	astrologie	k1gFnSc6	astrologie
<g/>
,	,	kIx,	,
astronomii	astronomie	k1gFnSc6	astronomie
<g/>
,	,	kIx,	,
výrobě	výroba	k1gFnSc6	výroba
léků	lék	k1gInPc2	lék
atd.	atd.	kA	atd.
Nesrozumitelnost	Nesrozumitelnost	k1gFnSc1	Nesrozumitelnost
a	a	k8xC	a
tajuplnost	tajuplnost	k1gFnSc1	tajuplnost
jeho	jeho	k3xOp3gNnPc2	jeho
proroctví	proroctví	k1gNnPc2	proroctví
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gMnPc2	jeho
příznivců	příznivec	k1gMnPc2	příznivec
dána	dán	k2eAgFnSc1d1	dána
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
nebezpečné	bezpečný	k2eNgFnSc6d1	nebezpečná
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
být	být	k5eAaImF	být
opatrný	opatrný	k2eAgMnSc1d1	opatrný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Právě	právě	k9	právě
nejednoznačnost	nejednoznačnost	k1gFnSc4	nejednoznačnost
Nostradamových	Nostradamový	k2eAgNnPc2d1	Nostradamový
čtyřverší	čtyřverší	k1gNnPc2	čtyřverší
svádí	svádět	k5eAaImIp3nS	svádět
mnohé	mnohý	k2eAgNnSc1d1	mnohé
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
o	o	k7c4	o
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
výklad	výklad	k1gInSc4	výklad
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvé	prvý	k4xOgFnSc6	prvý
řadě	řada	k1gFnSc6	řada
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
neexistovala	existovat	k5eNaImAgFnS	existovat
jednotná	jednotný	k2eAgFnSc1d1	jednotná
typografie	typografie	k1gFnSc1	typografie
<g/>
.	.	kIx.	.
</s>
<s>
Pravopis	pravopis	k1gInSc1	pravopis
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
slov	slovo	k1gNnPc2	slovo
se	se	k3xPyFc4	se
od	od	k7c2	od
jednoho	jeden	k4xCgNnSc2	jeden
vydání	vydání	k1gNnSc2	vydání
k	k	k7c3	k
dalšímu	další	k1gNnSc3	další
mění	měnit	k5eAaImIp3nP	měnit
<g/>
,	,	kIx,	,
až	až	k8xS	až
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
naprosté	naprostý	k2eAgFnSc3d1	naprostá
změně	změna	k1gFnSc3	změna
původního	původní	k2eAgInSc2d1	původní
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
z	z	k7c2	z
výrazu	výraz	k1gInSc2	výraz
Par	para	k1gFnPc2	para
le	le	k?	le
cinquiesme	cinquiést	k5eAaPmRp1nP	cinquiést
(	(	kIx(	(
<g/>
=	=	kIx~	=
skrze	skrze	k?	skrze
pátého	pátý	k4xOgInSc2	pátý
<g/>
/	/	kIx~	/
<g/>
ý	ý	k?	ý
<g/>
/	/	kIx~	/
<g/>
ou	ou	k0	ou
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
stává	stávat	k5eAaImIp3nS	stávat
Carle	Carle	k1gFnSc1	Carle
cinquiesme	cinquiést	k5eAaPmRp1nP	cinquiést
(	(	kIx(	(
<g/>
=	=	kIx~	=
Karel	Karel	k1gMnSc1	Karel
V.	V.	kA	V.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
kamenem	kámen	k1gInSc7	kámen
úrazu	úraz	k1gInSc2	úraz
je	být	k5eAaImIp3nS	být
volnost	volnost	k1gFnSc1	volnost
<g/>
,	,	kIx,	,
jakou	jaký	k3yIgFnSc4	jaký
si	se	k3xPyFc3	se
sám	sám	k3xTgMnSc1	sám
autor	autor	k1gMnSc1	autor
dopřál	dopřát	k5eAaPmAgMnS	dopřát
při	při	k7c6	při
psaní	psaní	k1gNnSc6	psaní
čtyřverší	čtyřverší	k1gNnSc2	čtyřverší
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
často	často	k6eAd1	často
archaickou	archaický	k2eAgFnSc4d1	archaická
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
originální	originální	k2eAgFnSc4d1	originální
větnou	větný	k2eAgFnSc4d1	větná
skladbu	skladba	k1gFnSc4	skladba
<g/>
,	,	kIx,	,
francouzské	francouzský	k2eAgInPc4d1	francouzský
výrazy	výraz	k1gInPc4	výraz
míchá	míchat	k5eAaImIp3nS	míchat
s	s	k7c7	s
latinskými	latinský	k2eAgMnPc7d1	latinský
<g/>
,	,	kIx,	,
řeckými	řecký	k2eAgMnPc7d1	řecký
a	a	k8xC	a
provensálskými	provensálský	k2eAgMnPc7d1	provensálský
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
si	se	k3xPyFc3	se
s	s	k7c7	s
anagramy	anagram	k1gInPc7	anagram
<g/>
,	,	kIx,	,
hojně	hojně	k6eAd1	hojně
užívá	užívat	k5eAaImIp3nS	užívat
alegorických	alegorický	k2eAgMnPc2d1	alegorický
obratů	obrat	k1gInPc2	obrat
a	a	k8xC	a
mytologických	mytologický	k2eAgFnPc2d1	mytologická
narážek	narážka	k1gFnPc2	narážka
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
přiznává	přiznávat	k5eAaImIp3nS	přiznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
některá	některý	k3yIgNnPc1	některý
slova	slovo	k1gNnPc4	slovo
upravil	upravit	k5eAaPmAgInS	upravit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
činí	činit	k5eAaImIp3nS	činit
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
veršů	verš	k1gInPc2	verš
tvrdý	tvrdý	k2eAgInSc4d1	tvrdý
interpretační	interpretační	k2eAgInSc4d1	interpretační
oříšek	oříšek	k1gInSc4	oříšek
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
verše	verš	k1gInPc1	verš
naopak	naopak	k6eAd1	naopak
zarážejí	zarážet	k5eAaImIp3nP	zarážet
přesností	přesnost	k1gFnSc7	přesnost
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnějším	slavný	k2eAgMnSc7d3	nejslavnější
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
pětatřicáté	pětatřicátý	k4xOgNnSc4	pětatřicátý
čtyřverší	čtyřverší	k1gNnPc2	čtyřverší
z	z	k7c2	z
první	první	k4xOgFnSc2	první
stovky	stovka	k1gFnSc2	stovka
veršů	verš	k1gInPc2	verš
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
podle	podle	k7c2	podle
Nostradamových	Nostradamový	k2eAgMnPc2d1	Nostradamový
současníků	současník	k1gMnPc2	současník
předpovědělo	předpovědět	k5eAaPmAgNnS	předpovědět
smrt	smrt	k1gFnSc4	smrt
krále	král	k1gMnSc2	král
Jindřicha	Jindřich	k1gMnSc2	Jindřich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Hovoří	hovořit	k5eAaImIp3nS	hovořit
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
o	o	k7c6	o
dvou	dva	k4xCgMnPc6	dva
lvech	lev	k1gMnPc6	lev
<g/>
,	,	kIx,	,
starém	starý	k2eAgInSc6d1	starý
a	a	k8xC	a
mladém	mladý	k2eAgMnSc6d1	mladý
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
utkají	utkat	k5eAaPmIp3nP	utkat
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
lev	lev	k1gMnSc1	lev
při	při	k7c6	při
něm	on	k3xPp3gInSc6	on
starému	staré	k1gNnSc3	staré
vypíchne	vypíchnout	k5eAaPmIp3nS	vypíchnout
oko	oko	k1gNnSc4	oko
a	a	k8xC	a
zraněný	zraněný	k2eAgMnSc1d1	zraněný
sok	sok	k1gMnSc1	sok
zemře	zemřít	k5eAaPmIp3nS	zemřít
v	v	k7c6	v
krutých	krutý	k2eAgFnPc6d1	krutá
bolestech	bolest	k1gFnPc6	bolest
<g/>
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
II	II	kA	II
<g/>
.	.	kIx.	.
skutečně	skutečně	k6eAd1	skutečně
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
následky	následek	k1gInPc4	následek
poranění	poranění	k1gNnSc2	poranění
oka	oko	k1gNnSc2	oko
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mu	on	k3xPp3gNnSc3	on
při	při	k7c6	při
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
probodl	probodnout	k5eAaPmAgMnS	probodnout
kopím	kopit	k5eAaImIp1nS	kopit
hrabě	hrabě	k1gMnSc1	hrabě
Montgomery	Montgomera	k1gFnSc2	Montgomera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
vykladači	vykladač	k1gMnPc7	vykladač
Nostradamových	Nostradamový	k2eAgInPc2d1	Nostradamový
veršů	verš	k1gInPc2	verš
existuje	existovat	k5eAaImIp3nS	existovat
přinejmenším	přinejmenším	k6eAd1	přinejmenším
jeden	jeden	k4xCgMnSc1	jeden
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
dvakrát	dvakrát	k6eAd1	dvakrát
nespletl	splést	k5eNaPmAgMnS	splést
<g/>
.	.	kIx.	.
</s>
<s>
Jean-Charles	Jean-Charles	k1gInSc1	Jean-Charles
de	de	k?	de
Fontbrune	Fontbrun	k1gInSc5	Fontbrun
na	na	k7c6	na
základě	základ	k1gInSc6	základ
čtyřverší	čtyřverší	k1gNnSc2	čtyřverší
předpověděl	předpovědět	k5eAaPmAgInS	předpovědět
rok	rok	k1gInSc4	rok
předem	předem	k6eAd1	předem
zvolení	zvolení	k1gNnSc1	zvolení
Francoise	Francois	k1gMnSc2	Francois
Mitterranda	Mitterranda	k1gFnSc1	Mitterranda
francouzským	francouzský	k2eAgMnSc7d1	francouzský
prezidentem	prezident	k1gMnSc7	prezident
a	a	k8xC	a
atentát	atentát	k1gInSc4	atentát
na	na	k7c4	na
papeže	papež	k1gMnSc4	papež
Jana	Jan	k1gMnSc4	Jan
Pavla	Pavel	k1gMnSc4	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
kniha	kniha	k1gFnSc1	kniha
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
prodávala	prodávat	k5eAaImAgFnS	prodávat
rychlostí	rychlost	k1gFnSc7	rychlost
5000	[number]	k4	5000
výtisků	výtisk	k1gInPc2	výtisk
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgFnPc4d1	související
stránky	stránka	k1gFnPc4	stránka
==	==	k?	==
</s>
</p>
<p>
<s>
Nostradamus	Nostradamus	k1gInSc1	Nostradamus
-	-	kIx~	-
album	album	k1gNnSc1	album
kapely	kapela	k1gFnPc1	kapela
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Nostradamus	Nostradamus	k1gInSc1	Nostradamus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Nostradamus	Nostradamus	k1gInSc4	Nostradamus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Nostradamus	Nostradamus	k1gInSc1	Nostradamus
-	-	kIx~	-
Česky	česky	k6eAd1	česky
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Dokument	dokument	k1gInSc1	dokument
televize	televize	k1gFnSc2	televize
Discovery	Discovera	k1gFnSc2	Discovera
Nostradamus	Nostradamus	k1gMnSc1	Nostradamus
Fakta	faktum	k1gNnSc2	faktum
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Nostradamus	Nostradamus	k1gInSc1	Nostradamus
-	-	kIx~	-
Provence	Provence	k1gFnSc1	Provence
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Dílo	dílo	k1gNnSc1	dílo
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Nostradamus	Nostradamus	k1gInSc1	Nostradamus
2012	[number]	k4	2012
</s>
</p>
