<p>
<s>
Garm	Garm	k1gInSc1	Garm
(	(	kIx(	(
<g/>
či	či	k8xC	či
Garmr	Garmr	k1gInSc1	Garmr
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pekelný	pekelný	k2eAgMnSc1d1	pekelný
pes	pes	k1gMnSc1	pes
strážící	strážící	k2eAgInSc4d1	strážící
vchod	vchod	k1gInSc4	vchod
do	do	k7c2	do
Helheimu	Helheim	k1gInSc2	Helheim
(	(	kIx(	(
<g/>
v	v	k7c6	v
severské	severský	k2eAgFnSc6d1	severská
mytologii	mytologie	k1gFnSc6	mytologie
říše	říš	k1gFnSc2	říš
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgNnPc4	čtyři
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
hruď	hruď	k1gFnSc4	hruď
promočenou	promočený	k2eAgFnSc7d1	promočená
krví	krev	k1gFnSc7	krev
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
krví	krev	k1gFnSc7	krev
všech	všecek	k3xTgMnPc2	všecek
mrtvých	mrtvý	k2eAgMnPc2d1	mrtvý
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
jeskyni	jeskyně	k1gFnSc6	jeskyně
Gnippahellir	Gnippahellira	k1gFnPc2	Gnippahellira
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
u	u	k7c2	u
vstupu	vstup	k1gInSc2	vstup
do	do	k7c2	do
říše	říš	k1gFnSc2	říš
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Spěchá	spěchat	k5eAaImIp3nS	spěchat
naproti	naproti	k7c3	naproti
každému	každý	k3xTgNnSc3	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
sem	sem	k6eAd1	sem
putuje	putovat	k5eAaImIp3nS	putovat
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
setkal	setkat	k5eAaPmAgMnS	setkat
i	i	k9	i
Ódin	Ódin	k1gMnSc1	Ódin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kdokoli	kdokoli	k3yInSc1	kdokoli
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
obdaroval	obdarovat	k5eAaPmAgMnS	obdarovat
chlebem	chléb	k1gInSc7	chléb
chudé	chudý	k2eAgInPc4d1	chudý
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
možnost	možnost	k1gFnSc4	možnost
jej	on	k3xPp3gMnSc4	on
uklidnit	uklidnit	k5eAaPmF	uklidnit
koláčem	koláč	k1gInSc7	koláč
od	od	k7c2	od
bohyně	bohyně	k1gFnSc2	bohyně
Hel	Hela	k1gFnPc2	Hela
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Až	až	k6eAd1	až
nadejde	nadejít	k5eAaPmIp3nS	nadejít
Ragnarök	Ragnarök	k1gInSc1	Ragnarök
(	(	kIx(	(
<g/>
den	den	k1gInSc1	den
"	"	kIx"	"
<g/>
osudu	osud	k1gInSc6	osud
bohů	bůh	k1gMnPc2	bůh
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
soudný	soudný	k2eAgInSc4d1	soudný
den	den	k1gInSc4	den
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Garm	Garm	k1gInSc1	Garm
se	se	k3xPyFc4	se
připojí	připojit	k5eAaPmIp3nS	připojit
k	k	k7c3	k
obrům	obr	k1gMnPc3	obr
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
bohům	bůh	k1gMnPc3	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
protivníkem	protivník	k1gMnSc7	protivník
bude	být	k5eAaImBp3nS	být
severský	severský	k2eAgMnSc1d1	severský
bůh	bůh	k1gMnSc1	bůh
války	válka	k1gFnSc2	válka
Týr	Týr	k1gMnSc1	Týr
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
jej	on	k3xPp3gMnSc4	on
zabije	zabít	k5eAaPmIp3nS	zabít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sám	sám	k3xTgMnSc1	sám
také	také	k9	také
podlehne	podlehnout	k5eAaPmIp3nS	podlehnout
zraněním	zranění	k1gNnSc7	zranění
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
mu	on	k3xPp3gMnSc3	on
Garm	Garm	k1gInSc1	Garm
způsobí	způsobit	k5eAaPmIp3nS	způsobit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Garm	Garm	k6eAd1	Garm
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
zaměňován	zaměňován	k2eAgInSc1d1	zaměňován
s	s	k7c7	s
Fenrirem	Fenrir	k1gInSc7	Fenrir
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
mytologii	mytologie	k1gFnSc6	mytologie
jej	on	k3xPp3gNnSc2	on
lze	lze	k6eAd1	lze
pak	pak	k6eAd1	pak
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
ekvivalent	ekvivalent	k1gInSc4	ekvivalent
Cerbera	Cerber	k1gMnSc2	Cerber
<g/>
.	.	kIx.	.
</s>
</p>
