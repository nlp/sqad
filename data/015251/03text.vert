<s>
Magnetický	magnetický	k2eAgInSc1d1
zápis	zápis	k1gInSc1
</s>
<s>
Podélný	podélný	k2eAgInSc1d1
a	a	k8xC
kolmý	kolmý	k2eAgInSc1d1
zápis	zápis	k1gInSc1
</s>
<s>
Magnetický	magnetický	k2eAgInSc1d1
zápis	zápis	k1gInSc1
je	být	k5eAaImIp3nS
způsob	způsob	k1gInSc4
ukládání	ukládání	k1gNnSc2
a	a	k8xC
čtení	čtení	k1gNnSc2
informace	informace	k1gFnSc2
pomocí	pomocí	k7c2
magnetického	magnetický	k2eAgNnSc2d1
pole	pole	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
umožňuje	umožňovat	k5eAaImIp3nS
její	její	k3xOp3gNnSc4
zpracování	zpracování	k1gNnSc4
počítačem	počítač	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Digitální	digitální	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
posloupnost	posloupnost	k1gFnSc4
jedniček	jednička	k1gFnPc2
a	a	k8xC
nul	nula	k1gFnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
zobrazena	zobrazit	k5eAaPmNgFnS
pomocí	pomoc	k1gFnSc7
magnetických	magnetický	k2eAgFnPc2d1
veličin	veličina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
praxi	praxe	k1gFnSc6
se	se	k3xPyFc4
k	k	k7c3
tomu	ten	k3xDgNnSc3
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
orientace	orientace	k1gFnSc1
vektoru	vektor	k1gInSc2
magnetické	magnetický	k2eAgFnSc2d1
indukce	indukce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analogový	analogový	k2eAgInSc1d1
magnetický	magnetický	k2eAgInSc1d1
zápis	zápis	k1gInSc1
se	se	k3xPyFc4
užíval	užívat	k5eAaImAgInS
u	u	k7c2
záznamových	záznamový	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
zvuku	zvuk	k1gInSc2
a	a	k8xC
obrazu	obraz	k1gInSc2
(	(	kIx(
<g/>
magnetofon	magnetofon	k1gInSc1
<g/>
,	,	kIx,
Ampex	ampex	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
digitální	digitální	k2eAgInSc4d1
magnetický	magnetický	k2eAgInSc4d1
zápis	zápis	k1gInSc4
u	u	k7c2
páskových	páskový	k2eAgFnPc2d1
pamětí	paměť	k1gFnPc2
a	a	k8xC
bubnů	buben	k1gInPc2
<g/>
,	,	kIx,
u	u	k7c2
disků	disk	k1gInPc2
a	a	k8xC
disket	disketa	k1gFnPc2
a	a	k8xC
později	pozdě	k6eAd2
i	i	k9
u	u	k7c2
nejrůznějších	různý	k2eAgFnPc2d3
magnetických	magnetický	k2eAgFnPc2d1
karet	kareta	k1gFnPc2
<g/>
,	,	kIx,
</s>
<s>
Podélný	podélný	k2eAgInSc1d1
zápis	zápis	k1gInSc1
(	(	kIx(
<g/>
Longitudinal	Longitudinal	k1gFnSc1
Magnetic	Magnetice	k1gFnPc2
Recording	Recording	k1gInSc4
–	–	k?
LMR	LMR	kA
<g/>
)	)	kIx)
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1
bity	bit	k1gInPc1
<g/>
,	,	kIx,
interpretované	interpretovaný	k2eAgInPc1d1
jako	jako	k8xS,k8xC
malá	malý	k2eAgFnSc1d1
opačně	opačně	k6eAd1
orientovaná	orientovaný	k2eAgFnSc1d1
magnetická	magnetický	k2eAgFnSc1d1
pole	pole	k1gFnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
uchovávají	uchovávat	k5eAaImIp3nP
jako	jako	k9
vektory	vektor	k1gInPc1
rovnoběžné	rovnoběžný	k2eAgInPc1d1
s	s	k7c7
plotnou	plotna	k1gFnSc7
disku	disk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgInSc7
způsobem	způsob	k1gInSc7
lze	lze	k6eAd1
však	však	k9
dosáhnout	dosáhnout	k5eAaPmF
hustoty	hustota	k1gFnPc4
zápisu	zápis	k1gInSc2
jen	jen	k9
kolem	kolem	k7c2
150	#num#	k4
gigabitů	gigabit	k1gInPc2
na	na	k7c4
čtverečný	čtverečný	k2eAgInSc4d1
palec	palec	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
vyšších	vysoký	k2eAgFnPc6d2
hustotách	hustota	k1gFnPc6
dochází	docházet	k5eAaImIp3nS
vlivem	vlivem	k7c2
paramagnetismu	paramagnetismus	k1gInSc2
k	k	k7c3
samovolné	samovolný	k2eAgFnSc3d1
ztrátě	ztráta	k1gFnSc3
uložených	uložený	k2eAgNnPc2d1
dat	datum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
této	tento	k3xDgFnSc6
hustotě	hustota	k1gFnSc6
jednotlivých	jednotlivý	k2eAgFnPc2d1
magnetických	magnetický	k2eAgFnPc2d1
polí	pole	k1gFnPc2
<g/>
,	,	kIx,
bitů	bit	k1gInPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
již	již	k6eAd1
nedaří	dařit	k5eNaImIp3nS
udržet	udržet	k5eAaPmF
jednotlivá	jednotlivý	k2eAgNnPc4d1
pole	pole	k1gNnPc4
izolovaná	izolovaný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
se	se	k3xPyFc4
pak	pak	k6eAd1
vzájemně	vzájemně	k6eAd1
ovlivňují	ovlivňovat	k5eAaImIp3nP
a	a	k8xC
dochází	docházet	k5eAaImIp3nS
ke	k	k7c3
ztrátě	ztráta	k1gFnSc3
uložené	uložený	k2eAgFnSc2d1
informace	informace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Superparamagnetismus	Superparamagnetismus	k1gInSc1
je	být	k5eAaImIp3nS
přirozený	přirozený	k2eAgInSc1d1
fyzikální	fyzikální	k2eAgInSc1d1
jev	jev	k1gInSc1
<g/>
,	,	kIx,
nejedná	jednat	k5eNaImIp3nS
se	se	k3xPyFc4
tedy	tedy	k9
o	o	k7c4
konstrukční	konstrukční	k2eAgFnSc4d1
nedokonalost	nedokonalost	k1gFnSc4
disku	disk	k1gInSc2
či	či	k8xC
technickou	technický	k2eAgFnSc4d1
bariéru	bariéra	k1gFnSc4
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
o	o	k7c4
fyzikální	fyzikální	k2eAgFnSc4d1
mez	mez	k1gFnSc4
této	tento	k3xDgFnSc2
metody	metoda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kolmý	kolmý	k2eAgInSc1d1
zápis	zápis	k1gInSc1
(	(	kIx(
<g/>
Perpendicular	Perpendicular	k1gInSc1
Magnetic	Magnetice	k1gFnPc2
Recording	Recording	k1gInSc4
–	–	k?
PMR	PMR	kA
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1984	#num#	k4
předvedl	předvést	k5eAaPmAgMnS
profesor	profesor	k1gMnSc1
Jack	Jack	k1gMnSc1
Judy	judo	k1gNnPc7
první	první	k4xOgInSc4
disk	disk	k1gInSc4
s	s	k7c7
kolmým	kolmý	k2eAgInSc7d1
zápisem	zápis	k1gInSc7
a	a	k8xC
IBM	IBM	kA
předvedla	předvést	k5eAaPmAgFnS
disketovou	disketový	k2eAgFnSc4d1
mechaniku	mechanika	k1gFnSc4
s	s	k7c7
kapacitou	kapacita	k1gFnSc7
5	#num#	k4
MB	MB	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
2005	#num#	k4
uvedla	uvést	k5eAaPmAgFnS
Toshiba	toshiba	k1gFnSc1
na	na	k7c4
trh	trh	k1gInSc4
první	první	k4xOgInSc1
pevný	pevný	k2eAgInSc1d1
disk	disk	k1gInSc1
využívající	využívající	k2eAgInSc1d1
technologii	technologie	k1gFnSc3
kolmého	kolmý	k2eAgInSc2d1
zápisu	zápis	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vektory	vektor	k1gInPc7
magnetické	magnetický	k2eAgFnSc2d1
indukce	indukce	k1gFnSc2
jednotlivých	jednotlivý	k2eAgInPc2d1
bitů	bit	k1gInPc2
zde	zde	k6eAd1
nejsou	být	k5eNaImIp3nP
orientovány	orientovat	k5eAaBmNgFnP
rovnoběžně	rovnoběžně	k6eAd1
s	s	k7c7
plotnou	plotna	k1gFnSc7
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
kolmo	kolmo	k6eAd1
na	na	k7c4
ni	on	k3xPp3gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
zvýšit	zvýšit	k5eAaPmF
kapacitu	kapacita	k1gFnSc4
pevných	pevný	k2eAgInPc2d1
disků	disk	k1gInPc2
až	až	k9
desetinásobně	desetinásobně	k6eAd1
a	a	k8xC
přiblížit	přiblížit	k5eAaPmF
se	se	k3xPyFc4
hranici	hranice	k1gFnSc3
hustoty	hustota	k1gFnSc2
1	#num#	k4
terabit	terabit	k5eAaImF,k5eAaPmF
na	na	k7c4
čtverečný	čtverečný	k2eAgInSc4d1
palec	palec	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
se	se	k3xPyFc4
v	v	k7c6
laboratořích	laboratoř	k1gFnPc6
dosáhlo	dosáhnout	k5eAaPmAgNnS
hustot	hustota	k1gFnPc2
mezi	mezi	k7c7
800	#num#	k4
a	a	k8xC
900	#num#	k4
gigabity	gigabita	k1gFnPc4
na	na	k7c4
čtverečný	čtverečný	k2eAgInSc4d1
palec	palec	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
2009	#num#	k4
přesáhla	přesáhnout	k5eAaPmAgFnS
kapacita	kapacita	k1gFnSc1
disků	disk	k1gInPc2
(	(	kIx(
<g/>
Hitachi	Hitachi	k1gNnSc1
<g/>
,	,	kIx,
Seagate	Seagate	kA
<g/>
)	)	kIx)
o	o	k7c6
průměru	průměr	k1gInSc6
2,5	2,5	k4
<g/>
″	″	k?
1	#num#	k4
TB	TB	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Disky	disk	k1gInPc4
s	s	k7c7
PMR	PMR	kA
rychle	rychle	k6eAd1
ovládly	ovládnout	k5eAaPmAgFnP
pole	pole	k1gNnSc4
a	a	k8xC
podélný	podélný	k2eAgInSc4d1
záznam	záznam	k1gInSc4
se	se	k3xPyFc4
už	už	k6eAd1
u	u	k7c2
disků	disk	k1gInPc2
neužívá	užívat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
novou	nový	k2eAgFnSc7d1
technologií	technologie	k1gFnSc7
však	však	k9
přichází	přicházet	k5eAaImIp3nS
i	i	k9
větší	veliký	k2eAgFnSc1d2
technologická	technologický	k2eAgFnSc1d1
náročnost	náročnost	k1gFnSc1
řešení	řešení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
kolmého	kolmý	k2eAgInSc2d1
zápisu	zápis	k1gInSc2
bylo	být	k5eAaImAgNnS
nutné	nutný	k2eAgNnSc1d1
vyvinout	vyvinout	k5eAaPmF
novou	nový	k2eAgFnSc4d1
diskovou	diskový	k2eAgFnSc4d1
hlavu	hlava	k1gFnSc4
pro	pro	k7c4
zápis	zápis	k1gInSc4
a	a	k8xC
přidat	přidat	k5eAaPmF
pod	pod	k7c4
datovou	datový	k2eAgFnSc4d1
vrstvu	vrstva	k1gFnSc4
ještě	ještě	k9
vrstvu	vrstva	k1gFnSc4
z	z	k7c2
magneticky	magneticky	k6eAd1
měkkého	měkký	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc1
tyto	tento	k3xDgFnPc1
novinky	novinka	k1gFnPc1
pomáhají	pomáhat	k5eAaImIp3nP
optimalizovat	optimalizovat	k5eAaBmF
magnetické	magnetický	k2eAgFnPc1d1
pole	pole	k1gFnPc1
indukované	indukovaný	k2eAgFnPc1d1
hlavou	hlava	k1gFnSc7
disku	disk	k1gInSc2
a	a	k8xC
tím	ten	k3xDgNnSc7
umožňují	umožňovat	k5eAaImIp3nP
přesnější	přesný	k2eAgNnPc4d2
zaostření	zaostření	k1gNnPc4
a	a	k8xC
následný	následný	k2eAgInSc4d1
zápis	zápis	k1gInSc4
na	na	k7c4
konkrétní	konkrétní	k2eAgNnSc4d1
místo	místo	k1gNnSc4
disku	disk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Použitá	použitý	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
</s>
<s>
↑	↑	k?
Hard	Hard	k1gMnSc1
Drives	Drives	k1gMnSc1
Get	Get	k1gMnSc1
New	New	k1gMnSc1
Record	Record	k1gMnSc1
Density	Densit	k1gInPc7
–	–	k?
Where	Wher	k1gMnSc5
Is	Is	k1gMnSc5
The	The	k1gMnSc4
Limit	limit	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
www.conceivablytech.com	www.conceivablytech.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Hitachi	Hitachi	k6eAd1
Global	globat	k5eAaImAgInS
Storage	Storage	k1gInSc1
Technologies	Technologies	k1gInSc1
<g/>
:	:	kIx,
Perpendicular	Perpendicular	k1gInSc1
Magnetic	Magnetice	k1gFnPc2
Recording	Recording	k1gInSc1
Technology	technolog	k1gMnPc7
<g/>
,	,	kIx,
2006	#num#	k4
</s>
