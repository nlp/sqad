<s>
Šíleně	šíleně	k6eAd1	šíleně
smutná	smutný	k2eAgFnSc1d1	smutná
princezna	princezna	k1gFnSc1	princezna
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
filmová	filmový	k2eAgFnSc1d1	filmová
pohádka	pohádka	k1gFnSc1	pohádka
žánru	žánr	k1gInSc2	žánr
hudební	hudební	k2eAgFnSc2d1	hudební
komedie	komedie	k1gFnSc2	komedie
režiséra	režisér	k1gMnSc2	režisér
Bořivoje	Bořivoj	k1gMnSc2	Bořivoj
Zemana	Zeman	k1gMnSc2	Zeman
natočená	natočený	k2eAgFnSc1d1	natočená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
s	s	k7c7	s
Helenou	Helena	k1gFnSc7	Helena
Vondráčkovou	Vondráčková	k1gFnSc7	Vondráčková
a	a	k8xC	a
Václavem	Václav	k1gMnSc7	Václav
Neckářem	Neckář	k1gMnSc7	Neckář
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
