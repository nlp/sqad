<p>
<s>
Nepoznatelno	nepoznatelno	k1gNnSc1	nepoznatelno
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
the	the	k?	the
Unknowable	Unknowable	k1gFnSc1	Unknowable
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
filosofii	filosofie	k1gFnSc6	filosofie
Herberta	Herbert	k1gMnSc2	Herbert
Spencera	Spencero	k1gNnSc2	Spencero
označení	označení	k1gNnSc2	označení
pro	pro	k7c4	pro
zásadně	zásadně	k6eAd1	zásadně
nepoznatelné	poznatelný	k2eNgFnPc4d1	nepoznatelná
skutečnosti	skutečnost	k1gFnPc4	skutečnost
"	"	kIx"	"
<g/>
za	za	k7c7	za
horizontem	horizont	k1gInSc7	horizont
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
o	o	k7c4	o
které	který	k3yIgNnSc4	který
se	se	k3xPyFc4	se
přesto	přesto	k8xC	přesto
jak	jak	k6eAd1	jak
vědecké	vědecký	k2eAgNnSc1d1	vědecké
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
náboženské	náboženský	k2eAgNnSc4d1	náboženské
poznání	poznání	k1gNnSc4	poznání
opírá	opírat	k5eAaImIp3nS	opírat
<g/>
.	.	kIx.	.
</s>
<s>
Ať	ať	k9	ať
už	už	k6eAd1	už
začneme	začít	k5eAaPmIp1nP	začít
myslet	myslet	k5eAaImF	myslet
od	od	k7c2	od
Stvoření	stvoření	k1gNnSc2	stvoření
a	a	k8xC	a
Stvořitele	Stvořitel	k1gMnSc2	Stvořitel
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
od	od	k7c2	od
nepoznatelného	poznatelný	k2eNgInSc2d1	nepoznatelný
"	"	kIx"	"
<g/>
substrátu	substrát	k1gInSc2	substrát
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgMnSc2	jenž
vychází	vycházet	k5eAaImIp3nS	vycházet
zkušenost	zkušenost	k1gFnSc1	zkušenost
a	a	k8xC	a
fenomény	fenomén	k1gInPc1	fenomén
<g/>
,	,	kIx,	,
vždycky	vždycky	k6eAd1	vždycky
dospějeme	dochvít	k5eAaPmIp1nP	dochvít
k	k	k7c3	k
jistotě	jistota	k1gFnSc3	jistota
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidské	lidský	k2eAgNnSc1d1	lidské
poznání	poznání	k1gNnSc1	poznání
nemůže	moct	k5eNaImIp3nS	moct
dál	daleko	k6eAd2	daleko
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
relativní	relativní	k2eAgFnSc1d1	relativní
<g/>
.	.	kIx.	.
</s>
<s>
Představa	představa	k1gFnSc1	představa
nepoznatelného	poznatelný	k2eNgInSc2d1	nepoznatelný
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
Spencera	Spencer	k1gMnSc4	Spencer
tak	tak	k9	tak
základní	základní	k2eAgNnSc1d1	základní
<g/>
,	,	kIx,	,
že	že	k8xS	že
jí	on	k3xPp3gFnSc3	on
začíná	začínat	k5eAaImIp3nS	začínat
první	první	k4xOgInSc4	první
svazek	svazek	k1gInSc4	svazek
jeho	jeho	k3xOp3gNnSc2	jeho
desetisvazkového	desetisvazkový	k2eAgNnSc2d1	desetisvazkový
encyklopedického	encyklopedický	k2eAgNnSc2d1	encyklopedické
díla	dílo	k1gNnSc2	dílo
A	a	k8xC	a
system	syst	k1gInSc7	syst
of	of	k?	of
synthetic	synthetice	k1gFnPc2	synthetice
philosophy	philosopha	k1gFnPc4	philosopha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nepoznatelno	nepoznatelno	k1gNnSc1	nepoznatelno
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
interpretací	interpretace	k1gFnPc2	interpretace
transhorizontové	transhorizontový	k2eAgFnSc2d1	transhorizontový
reality	realita	k1gFnSc2	realita
<g/>
,	,	kIx,	,
o	o	k7c6	o
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
–	–	k?	–
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
transhorizontová	transhorizontový	k2eAgFnSc1d1	transhorizontový
–	–	k?	–
nedá	dát	k5eNaPmIp3nS	dát
vědět	vědět	k5eAaImF	vědět
ani	ani	k8xC	ani
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
neznatelná	znatelný	k2eNgFnSc1d1	neznatelná
<g/>
.	.	kIx.	.
</s>
<s>
Zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
něco	něco	k3yInSc1	něco
je	být	k5eAaImIp3nS	být
neznatelné	znatelný	k2eNgNnSc1d1	neznatelné
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
znamenalo	znamenat	k5eAaImAgNnS	znamenat
proniknout	proniknout	k5eAaPmF	proniknout
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
a	a	k8xC	a
dozvědět	dozvědět	k5eAaPmF	dozvědět
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
jenomže	jenomže	k8xC	jenomže
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
případě	případ	k1gInSc6	případ
by	by	kYmCp3nS	by
již	již	k9	již
nešlo	jít	k5eNaImAgNnS	jít
o	o	k7c4	o
transhorizontovou	transhorizontový	k2eAgFnSc4d1	transhorizontový
realitu	realita	k1gFnSc4	realita
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
Spencera	Spencero	k1gNnSc2	Spencero
nepoznatelno	nepoznatelno	k1gNnSc1	nepoznatelno
<g/>
,	,	kIx,	,
neznatelná	znatelný	k2eNgFnSc1d1	neznatelná
realita	realita	k1gFnSc1	realita
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
jako	jako	k9	jako
metafyzický	metafyzický	k2eAgInSc1d1	metafyzický
základ	základ	k1gInSc1	základ
všech	všecek	k3xTgFnPc2	všecek
věcí	věc	k1gFnPc2	věc
a	a	k8xC	a
manifestuje	manifestovat	k5eAaBmIp3nS	manifestovat
se	se	k3xPyFc4	se
tak	tak	k9	tak
v	v	k7c6	v
materiálních	materiální	k2eAgInPc6d1	materiální
i	i	k8xC	i
duchovních	duchovní	k2eAgInPc6d1	duchovní
útvarech	útvar	k1gInPc6	útvar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nepoznatelnu	nepoznatelno	k1gNnSc6	nepoznatelno
se	se	k3xPyFc4	se
nabízí	nabízet	k5eAaImIp3nS	nabízet
možnost	možnost	k1gFnSc1	možnost
sblížení	sblížení	k1gNnSc2	sblížení
náboženství	náboženství	k1gNnSc2	náboženství
a	a	k8xC	a
filozofie	filozofie	k1gFnSc2	filozofie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Nepoznateľno	Nepoznateľno	k6eAd1	Nepoznateľno
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Herbert	Herbert	k1gMnSc1	Herbert
Spencer	Spencer	k1gMnSc1	Spencer
<g/>
,	,	kIx,	,
First	First	k1gMnSc1	First
principles	principles	k1gMnSc1	principles
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
system	syst	k1gInSc7	syst
of	of	k?	of
synthetic	synthetice	k1gFnPc2	synthetice
philosophy	philosopha	k1gMnSc2	philosopha
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1862	[number]	k4	1862
<g/>
)	)	kIx)	)
Dostupné	dostupný	k2eAgFnSc2d1	dostupná
on-line	onin	k1gInSc5	on-lin
</s>
</p>
