<s>
Nepoznatelno	nepoznatelno	k1gNnSc1	nepoznatelno
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
interpretací	interpretace	k1gFnPc2	interpretace
transhorizontové	transhorizontový	k2eAgFnSc2d1	transhorizontový
reality	realita	k1gFnSc2	realita
<g/>
,	,	kIx,	,
o	o	k7c6	o
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
–	–	k?	–
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
transhorizontová	transhorizontový	k2eAgFnSc1d1	transhorizontový
–	–	k?	–
nedá	dát	k5eNaPmIp3nS	dát
vědět	vědět	k5eAaImF	vědět
ani	ani	k8xC	ani
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
neznatelná	znatelný	k2eNgFnSc1d1	neznatelná
<g/>
.	.	kIx.	.
</s>
