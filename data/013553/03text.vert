<s>
Falcon	Falcon	k1gInSc1
9	#num#	k4
</s>
<s>
Falcon	Falcon	k1gMnSc1
9	#num#	k4
Falcon	Falcon	k1gInSc1
9	#num#	k4
FT	FT	kA
při	při	k7c6
startu	start	k1gInSc6
mise	mise	k1gFnSc2
THAICOM	THAICOM	kA
8	#num#	k4
<g/>
Země	zem	k1gFnSc2
původu	původ	k1gInSc2
</s>
<s>
USA	USA	kA
Rodina	rodina	k1gFnSc1
raket	raketa	k1gFnPc2
</s>
<s>
Falcon	Falcon	k1gMnSc1
Výrobce	výrobce	k1gMnSc1
</s>
<s>
SpaceX	SpaceX	k?
Rozměry	rozměr	k1gInPc1
Výška	výška	k1gFnSc1
</s>
<s>
v	v	k7c6
<g/>
1.0	1.0	k4
<g/>
:	:	kIx,
54,9	54,9	k4
metrů	metr	k1gInPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
v	v	k7c6
<g/>
1.1	1.1	k4
<g/>
:	:	kIx,
68,4	68,4	k4
metrů	metr	k1gInPc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Full	Full	k1gMnSc1
Thrust	Thrust	k1gMnSc1
<g/>
:	:	kIx,
70	#num#	k4
metrů	metr	k1gInPc2
Průměr	průměr	k1gInSc1
</s>
<s>
3,66	3,66	k4
metrů	metr	k1gInPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Hmotnost	hmotnost	k1gFnSc1
</s>
<s>
v	v	k7c6
<g/>
1.0	1.0	k4
<g/>
:	:	kIx,
333	#num#	k4
400	#num#	k4
kg	kg	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
v	v	k7c6
<g/>
1.1	1.1	k4
<g/>
:	:	kIx,
505	#num#	k4
846	#num#	k4
kg	kg	kA
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
FT	FT	kA
<g/>
:	:	kIx,
549	#num#	k4
054	#num#	k4
kg	kg	kA
Nosnost	nosnost	k1gFnSc1
na	na	k7c6
LEO	Leo	k1gMnSc1
</s>
<s>
v	v	k7c6
<g/>
1.0	1.0	k4
<g/>
:	:	kIx,
10	#num#	k4
450	#num#	k4
kg	kg	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
v	v	k7c6
<g/>
1.1	1.1	k4
<g/>
:	:	kIx,
13	#num#	k4
150	#num#	k4
kg	kg	kA
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
FT	FT	kA
<g/>
:	:	kIx,
22	#num#	k4
800	#num#	k4
kg	kg	kA
na	na	k7c4
GTO	GTO	kA
</s>
<s>
v	v	k7c6
<g/>
1.0	1.0	k4
<g/>
:	:	kIx,
4	#num#	k4
540	#num#	k4
kg	kg	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
v	v	k7c6
<g/>
1.1	1.1	k4
<g/>
:	:	kIx,
4	#num#	k4
850	#num#	k4
kg	kg	kA
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
FT	FT	kA
<g/>
:	:	kIx,
8	#num#	k4
300	#num#	k4
kg	kg	kA
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Historie	historie	k1gFnSc1
startů	start	k1gInPc2
Status	status	k1gInSc1
</s>
<s>
FT	FT	kA
Block	Block	k1gInSc4
5	#num#	k4
<g/>
:	:	kIx,
aktivní	aktivní	k2eAgFnSc2d1
</s>
<s>
FT	FT	kA
Block	Block	k1gInSc4
4	#num#	k4
<g/>
:	:	kIx,
vyřazen	vyřadit	k5eAaPmNgMnS
</s>
<s>
FT	FT	kA
Block	Block	k1gInSc4
3	#num#	k4
<g/>
:	:	kIx,
vyřazen	vyřadit	k5eAaPmNgMnS
</s>
<s>
F9	F9	k4
v	v	k7c4
<g/>
1.1	1.1	k4
<g/>
:	:	kIx,
vyřazen	vyřadit	k5eAaPmNgMnS
</s>
<s>
F9	F9	k4
v	v	k7c4
<g/>
1.0	1.0	k4
<g/>
:	:	kIx,
vyřazen	vyřazen	k2eAgInSc1d1
Kosmodrom	kosmodrom	k1gInSc1
</s>
<s>
Cape	capat	k5eAaImIp3nS
Canaveral	Canaveral	k1gFnSc4
AFS	AFS	kA
</s>
<s>
Vandenberg	Vandenberg	k1gMnSc1
AFB	AFB	kA
Celkem	celkem	k6eAd1
startů	start	k1gInPc2
</s>
<s>
106	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
<g/>
1.0	1.0	k4
<g/>
:	:	kIx,
5	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
<g/>
1.1	1.1	k4
<g/>
:	:	kIx,
15	#num#	k4
<g/>
,	,	kIx,
FT	FT	kA
<g/>
:	:	kIx,
86	#num#	k4
<g/>
)	)	kIx)
Úspěšné	úspěšný	k2eAgInPc1d1
starty	start	k1gInPc1
</s>
<s>
104	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
<g/>
1.0	1.0	k4
<g/>
:	:	kIx,
4	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
<g/>
1.1	1.1	k4
<g/>
:	:	kIx,
14	#num#	k4
<g/>
,	,	kIx,
FT	FT	kA
<g/>
:	:	kIx,
86	#num#	k4
<g/>
)	)	kIx)
Selhání	selhání	k1gNnSc1
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
<g/>
1.0	1.0	k4
<g/>
:	:	kIx,
0	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
<g/>
1.1	1.1	k4
<g/>
:	:	kIx,
1	#num#	k4
<g/>
,	,	kIx,
FT	FT	kA
<g/>
:	:	kIx,
0	#num#	k4
<g/>
)	)	kIx)
Částečná	částečný	k2eAgNnPc4d1
selhání	selhání	k1gNnPc4
</s>
<s>
2	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
<g/>
1.0	1.0	k4
<g/>
:	:	kIx,
1	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
<g/>
1.1	1.1	k4
<g/>
:	:	kIx,
0	#num#	k4
<g/>
,	,	kIx,
FT	FT	kA
<g/>
:	:	kIx,
1	#num#	k4
<g/>
(	(	kIx(
<g/>
předstartovní	předstartovní	k2eAgFnSc1d1
příprava	příprava	k1gFnSc1
<g/>
))	))	k?
První	první	k4xOgInSc1
start	start	k1gInSc1
</s>
<s>
v	v	k7c6
<g/>
1.0	1.0	k4
<g/>
:	:	kIx,
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2010	#num#	k4
</s>
<s>
v	v	k7c6
<g/>
1.1	1.1	k4
<g/>
:	:	kIx,
29	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2013	#num#	k4
</s>
<s>
FT	FT	kA
<g/>
:	:	kIx,
22	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2015	#num#	k4
Poslední	poslední	k2eAgInSc1d1
start	start	k1gInSc1
</s>
<s>
v	v	k7c6
<g/>
1.0	1.0	k4
<g/>
:	:	kIx,
1	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2013	#num#	k4
<g/>
v	v	k7c4
<g/>
1.1	1.1	k4
<g/>
:	:	kIx,
17	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2016	#num#	k4
</s>
<s>
První	první	k4xOgInSc1
stupeň	stupeň	k1gInSc1
–	–	k?
Falcon	Falcon	k1gInSc4
9	#num#	k4
Motor	motor	k1gInSc1
</s>
<s>
v	v	k7c6
<g/>
1.0	1.0	k4
<g/>
:	:	kIx,
9	#num#	k4
<g/>
x	x	k?
Merlin	Merlina	k1gFnPc2
1	#num#	k4
<g/>
Cv	Cv	k1gFnPc2
<g/>
1.1	1.1	k4
<g/>
:	:	kIx,
9	#num#	k4
<g/>
x	x	k?
Merlin	Merlin	k2eAgInSc4d1
1	#num#	k4
<g/>
DFT	DFT	kA
<g/>
:	:	kIx,
9	#num#	k4
<g/>
x	x	k?
Merlin	Merlin	k2eAgInSc4d1
1	#num#	k4
<g/>
D	D	kA
<g/>
+	+	kIx~
Tah	tah	k1gInSc1
</s>
<s>
v	v	k7c6
<g/>
1.0	1.0	k4
<g/>
:	:	kIx,
4	#num#	k4
940	#num#	k4
kN	kN	k?
(	(	kIx(
<g/>
ve	v	k7c6
vakuu	vakuum	k1gNnSc6
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
v	v	k7c6
<g/>
1.1	1.1	k4
<g/>
:	:	kIx,
5	#num#	k4
885	#num#	k4
kN	kN	k?
(	(	kIx(
<g/>
na	na	k7c6
hladině	hladina	k1gFnSc6
moře	moře	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
6	#num#	k4
672	#num#	k4
kN	kN	k?
(	(	kIx(
<g/>
ve	v	k7c6
vakuu	vakuum	k1gNnSc6
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
FT	FT	kA
<g/>
:	:	kIx,
6	#num#	k4
700	#num#	k4
kN	kN	k?
(	(	kIx(
<g/>
na	na	k7c6
hladině	hladina	k1gFnSc6
moře	moře	k1gNnSc2
<g/>
)	)	kIx)
Specifický	specifický	k2eAgInSc1d1
impuls	impuls	k1gInSc1
</s>
<s>
v	v	k7c6
<g/>
1.0	1.0	k4
<g/>
:	:	kIx,
275	#num#	k4
sekund	sekunda	k1gFnPc2
na	na	k7c6
hladině	hladina	k1gFnSc6
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
304	#num#	k4
sekund	sekunda	k1gFnPc2
ve	v	k7c6
vakuu	vakuum	k1gNnSc6
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
v	v	k7c4
<g/>
1.1	1.1	k4
<g/>
,	,	kIx,
FT	FT	kA
<g/>
:	:	kIx,
282	#num#	k4
sekund	sekunda	k1gFnPc2
na	na	k7c6
hladině	hladina	k1gFnSc6
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
311	#num#	k4
sekund	sekunda	k1gFnPc2
ve	v	k7c6
vakuu	vakuum	k1gNnSc6
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Doba	doba	k1gFnSc1
zážehu	zážeh	k1gInSc2
</s>
<s>
v	v	k7c6
<g/>
1.0	1.0	k4
<g/>
:	:	kIx,
170	#num#	k4
sekundv	sekundv	k1gInSc1
<g/>
1.1	1.1	k4
<g/>
:	:	kIx,
180	#num#	k4
sekund	sekunda	k1gFnPc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
FT	FT	kA
<g/>
:	:	kIx,
162	#num#	k4
sekund	sekunda	k1gFnPc2
Palivo	palivo	k1gNnSc4
</s>
<s>
RP-	RP-	k?
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
LOX	LOX	kA
</s>
<s>
Druhý	druhý	k4xOgInSc1
stupeň	stupeň	k1gInSc1
–	–	k?
Falcon	Falcon	k1gInSc4
9	#num#	k4
Motor	motor	k1gInSc1
</s>
<s>
v	v	k7c6
<g/>
1.0	1.0	k4
<g/>
:	:	kIx,
Merlin	Merlin	k1gInSc1
1C	1C	k4
(	(	kIx(
<g/>
vakuová	vakuový	k2eAgFnSc1d1
modifikace	modifikace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
v	v	k7c6
<g/>
1.1	1.1	k4
<g/>
:	:	kIx,
Merlin	Merlin	k1gInSc1
1D	1D	k4
(	(	kIx(
<g/>
vakuová	vakuový	k2eAgFnSc1d1
modifikace	modifikace	k1gFnSc1
<g/>
)	)	kIx)
Tah	tah	k1gInSc1
</s>
<s>
v	v	k7c6
<g/>
1.0	1.0	k4
<g/>
:	:	kIx,
445	#num#	k4
kNv	kNv	k?
<g/>
1.1	1.1	k4
<g/>
:	:	kIx,
801	#num#	k4
kN	kN	k?
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
FT	FT	kA
<g/>
:	:	kIx,
934	#num#	k4
kN	kN	k?
Specifický	specifický	k2eAgInSc4d1
impuls	impuls	k1gInSc4
</s>
<s>
v	v	k7c6
<g/>
1.0	1.0	k4
<g/>
:	:	kIx,
342	#num#	k4
sekundv	sekundv	k1gInSc1
<g/>
1.1	1.1	k4
<g/>
:	:	kIx,
340	#num#	k4
sekundFT	sekundFT	k?
<g/>
:	:	kIx,
348	#num#	k4
sekund	sekunda	k1gFnPc2
Doba	doba	k1gFnSc1
zážehu	zážeh	k1gInSc2
</s>
<s>
v	v	k7c6
<g/>
1.0	1.0	k4
<g/>
:	:	kIx,
345	#num#	k4
sekund	sekunda	k1gFnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
v	v	k7c6
<g/>
1.1	1.1	k4
<g/>
:	:	kIx,
375	#num#	k4
sekund	sekunda	k1gFnPc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
FT	FT	kA
<g/>
:	:	kIx,
397	#num#	k4
sekund	sekunda	k1gFnPc2
Palivo	palivo	k1gNnSc4
</s>
<s>
RP-	RP-	k?
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
LOX	LOX	kA
</s>
<s>
Falcon	Falcon	k1gInSc1
9	#num#	k4
je	být	k5eAaImIp3nS
nosná	nosný	k2eAgFnSc1d1
raketa	raketa	k1gFnSc1
americké	americký	k2eAgFnSc2d1
soukromé	soukromý	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
SpaceX	SpaceX	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svým	svůj	k3xOyFgInSc7
tahem	tah	k1gInSc7
a	a	k8xC
nosností	nosnost	k1gFnSc7
je	být	k5eAaImIp3nS
srovnatelná	srovnatelný	k2eAgFnSc1d1
s	s	k7c7
ruskými	ruský	k2eAgFnPc7d1
raketami	raketa	k1gFnPc7
Angara	Angara	k1gFnSc1
5	#num#	k4
a	a	k8xC
Proton	proton	k1gInSc1
<g/>
,	,	kIx,
americkými	americký	k2eAgMnPc7d1
Delta	delta	k1gFnSc1
IV	Iva	k1gFnPc2
a	a	k8xC
Atlas	Atlas	k1gInSc1
V	V	kA
a	a	k8xC
čínskou	čínský	k2eAgFnSc7d1
Čchang-čeng	Čchang-čeng	k1gInSc4
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Falcon	Falcon	k1gInSc1
9	#num#	k4
startuje	startovat	k5eAaBmIp3nS
na	na	k7c6
Floridě	Florida	k1gFnSc6
z	z	k7c2
Kennedyho	Kennedy	k1gMnSc2
vesmírného	vesmírný	k2eAgNnSc2d1
střediska	středisko	k1gNnSc2
startovací	startovací	k2eAgInSc1d1
komplex	komplex	k1gInSc1
LC-	LC-	k1gFnSc1
<g/>
39	#num#	k4
<g/>
A	A	kA
<g/>
,	,	kIx,
z	z	k7c2
komplexu	komplex	k1gInSc2
SLC-40	SLC-40	k1gFnSc2
na	na	k7c6
Mysu	mys	k1gInSc6
Canaveral	Canaveral	k1gFnSc2
,	,	kIx,
nebo	nebo	k8xC
z	z	k7c2
komplexu	komplex	k1gInSc2
SLC-4E	SLC-4E	k1gMnSc2
Vandenbergovy	Vandenbergův	k2eAgFnSc2d1
letecké	letecký	k2eAgFnSc2d1
základny	základna	k1gFnSc2
v	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nově	nově	k6eAd1
startuje	startovat	k5eAaBmIp3nS
ze	z	k7c2
soukromého	soukromý	k2eAgInSc2d1
Kosmodromu	kosmodrom	k1gInSc2
SpaceX	SpaceX	k1gFnSc2
v	v	k7c6
jižním	jižní	k2eAgInSc6d1
Texasu	Texas	k1gInSc6
ve	v	k7c6
vesnici	vesnice	k1gFnSc6
Boca	Bocus	k1gMnSc2
Chica	Chicus	k1gMnSc2
u	u	k7c2
Brownsville	Brownsville	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
rámci	rámec	k1gInSc6
programu	program	k1gInSc2
na	na	k7c4
vývoj	vývoj	k1gInSc4
znovupoužitelného	znovupoužitelný	k2eAgInSc2d1
nosného	nosný	k2eAgInSc2d1
systému	systém	k1gInSc2
testovala	testovat	k5eAaImAgFnS
a	a	k8xC
demonstrovala	demonstrovat	k5eAaBmAgFnS
společnost	společnost	k1gFnSc1
SpaceX	SpaceX	k1gFnSc2
potřebné	potřebný	k2eAgFnSc2d1
technologie	technologie	k1gFnSc2
na	na	k7c6
raketě	raketa	k1gFnSc6
Falcon	Falcon	k1gNnSc1
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
se	se	k3xPyFc4
Falcon	Falcon	k1gInSc1
9	#num#	k4
stal	stát	k5eAaPmAgMnS
první	první	k4xOgNnSc4
a	a	k8xC
prozatím	prozatím	k6eAd1
jedinou	jediný	k2eAgFnSc7d1
kosmickou	kosmický	k2eAgFnSc7d1
raketou	raketa	k1gFnSc7
u	u	k7c2
které	který	k3yQgFnSc2,k3yIgFnSc2,k3yRgFnSc2
byl	být	k5eAaImAgMnS
zrealizovám	zrealizovat	k5eAaPmIp1nS,k5eAaImIp1nS
koncept	koncept	k1gInSc4
znovupoužitelnosti	znovupoužitelnost	k1gFnSc2
prvního	první	k4xOgInSc2
stupně	stupeň	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
svém	svůj	k3xOyFgInSc6
dvacátém	dvacátý	k4xOgInSc6
startu	start	k1gInSc6
22	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2015	#num#	k4
první	první	k4xOgInSc1
stupeň	stupeň	k1gInSc1
rakety	raketa	k1gFnSc2
úspěšně	úspěšně	k6eAd1
provedl	provést	k5eAaPmAgMnS
experimentální	experimentální	k2eAgNnSc4d1
přistání	přistání	k1gNnSc4
v	v	k7c6
přistávací	přistávací	k2eAgFnSc6d1
zóně	zóna	k1gFnSc6
1	#num#	k4
(	(	kIx(
<g/>
LZ-	LZ-	k1gFnSc1
<g/>
1	#num#	k4
<g/>
)	)	kIx)
na	na	k7c6
mysu	mys	k1gInSc6
Canaveral	Canaveral	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
čtyři	čtyři	k4xCgInPc4
měsíce	měsíc	k1gInPc4
později	pozdě	k6eAd2
<g/>
,	,	kIx,
8	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2016	#num#	k4
<g/>
,	,	kIx,
první	první	k4xOgInSc1
stupeň	stupeň	k1gInSc1
úspěšně	úspěšně	k6eAd1
dosedl	dosednout	k5eAaPmAgInS
na	na	k7c4
plovoucí	plovoucí	k2eAgFnSc4d1
autonomní	autonomní	k2eAgFnSc4d1
přistávací	přistávací	k2eAgFnSc4d1
plošinu	plošina	k1gFnSc4
Of	Of	k1gFnSc2
Course	Course	k1gFnSc2
I	i	k9
Still	Still	k1gMnSc1
Love	lov	k1gInSc5
You	You	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2017	#num#	k4
společnost	společnost	k1gFnSc1
SpaceX	SpaceX	k1gFnSc1
jako	jako	k8xS,k8xC
první	první	k4xOgFnSc1
na	na	k7c6
světě	svět	k1gInSc6
úspěšně	úspěšně	k6eAd1
vyslala	vyslat	k5eAaPmAgFnS
do	do	k7c2
vesmíru	vesmír	k1gInSc2
již	již	k6eAd1
dříve	dříve	k6eAd2
použitý	použitý	k2eAgInSc4d1
první	první	k4xOgInSc4
stupeň	stupeň	k1gInSc4
nosné	nosný	k2eAgFnSc2d1
rakety	raketa	k1gFnSc2
Falcon	Falcon	k1gNnSc1
9	#num#	k4
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
to	ten	k3xDgNnSc1
při	při	k7c6
misi	mise	k1gFnSc6
SES-	SES-	k1gFnSc1
<g/>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
mise	mise	k1gFnSc2
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
demonstrovat	demonstrovat	k5eAaBmF
opakovanou	opakovaný	k2eAgFnSc4d1
použitelnost	použitelnost	k1gFnSc4
nejdražší	drahý	k2eAgFnSc2d3
části	část	k1gFnSc2
rakety	raketa	k1gFnSc2
-	-	kIx~
1	#num#	k4
<g/>
.	.	kIx.
stupně	stupeň	k1gInSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
stupeň	stupeň	k1gInSc1
navíc	navíc	k6eAd1
po	po	k7c6
vynesení	vynesení	k1gNnSc6
nákladu	náklad	k1gInSc2
na	na	k7c4
orbitu	orbita	k1gFnSc4
znovu	znovu	k6eAd1
úspěšně	úspěšně	k6eAd1
přistál	přistát	k5eAaPmAgInS,k5eAaImAgInS
na	na	k7c6
autonomní	autonomní	k2eAgFnSc6d1
plovoucí	plovoucí	k2eAgFnSc6d1
plošině	plošina	k1gFnSc6
Of	Of	k1gFnSc2
Course	Course	k1gFnSc2
I	i	k9
Still	Still	k1gMnSc1
Love	lov	k1gInSc5
You	You	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
byl	být	k5eAaImAgInS
tento	tento	k3xDgInSc1
1	#num#	k4
<g/>
.	.	kIx.
stupeň	stupeň	k1gInSc1
použit	použít	k5eAaPmNgInS
při	při	k7c6
úspěšné	úspěšný	k2eAgFnSc6d1
misi	mise	k1gFnSc6
CRS-8	CRS-8	k1gFnSc2
v	v	k7c6
dubnu	duben	k1gInSc6
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
K	k	k7c3
listopadu	listopad	k1gInSc3
2020	#num#	k4
se	se	k3xPyFc4
již	již	k9
přistávání	přistávání	k1gNnSc2
prvních	první	k4xOgInPc2
stupňů	stupeň	k1gInPc2
a	a	k8xC
jejich	jejich	k3xOp3gNnPc2
znovupoužití	znovupoužití	k1gNnPc2
stalo	stát	k5eAaPmAgNnS
rutinou	rutina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
68	#num#	k4
pokusů	pokus	k1gInPc2
o	o	k7c4
přistání	přistání	k1gNnSc4
skončilo	skončit	k5eAaPmAgNnS
úspěchem	úspěch	k1gInSc7
60	#num#	k4
z	z	k7c2
nich	on	k3xPp3gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
rekordního	rekordní	k2eAgInSc2d1
stupně	stupeň	k1gInSc2
je	být	k5eAaImIp3nS
touto	tento	k3xDgFnSc7
dobou	doba	k1gFnSc7
zrealizováno	zrealizován	k2eAgNnSc1d1
sedminásobné	sedminásobný	k2eAgNnSc1d1
použití	použití	k1gNnSc1
a	a	k8xC
je	být	k5eAaImIp3nS
velice	velice	k6eAd1
pravděpodobné	pravděpodobný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
do	do	k7c2
budoucna	budoucno	k1gNnSc2
tato	tento	k3xDgNnPc1
čísla	číslo	k1gNnPc1
budou	být	k5eAaImBp3nP
dále	daleko	k6eAd2
narůstat	narůstat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Verze	verze	k1gFnSc1
</s>
<s>
Zleva	zleva	k6eAd1
Falcon	Falcon	k1gInSc1
1	#num#	k4
<g/>
,	,	kIx,
Falcon	Falcon	k1gInSc1
9	#num#	k4
v	v	k7c4
<g/>
1.0	1.0	k4
<g/>
,	,	kIx,
Falcon	Falcon	k1gInSc1
9	#num#	k4
v	v	k7c4
<g/>
1.1	1.1	k4
a	a	k8xC
Falcon	Falcon	k1gInSc1
HeavyAktuálně	HeavyAktuálně	k1gFnSc2
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
pět	pět	k4xCc1
vývojových	vývojový	k2eAgFnPc2d1
verzí	verze	k1gFnPc2
Falconu	Falcon	k1gInSc2
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Verze	verze	k1gFnSc1
1.0	1.0	k4
a	a	k8xC
1.1	1.1	k4
už	už	k6eAd1
byly	být	k5eAaImAgInP
z	z	k7c2
provozu	provoz	k1gInSc2
vyřazeny	vyřazen	k2eAgInPc4d1
a	a	k8xC
byly	být	k5eAaImAgFnP
nahrazeny	nahradit	k5eAaPmNgFnP
aktuální	aktuální	k2eAgFnSc7d1
verzí	verze	k1gFnSc7
FT	FT	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
byla	být	k5eAaImAgFnS
dále	daleko	k6eAd2
optimalizována	optimalizovat	k5eAaBmNgFnS
v	v	k7c6
rámci	rámec	k1gInSc6
tzv.	tzv.	kA
verzí	verze	k1gFnPc2
<g/>
/	/	kIx~
<g/>
bloků	blok	k1gInPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
zejména	zejména	k9
poslední	poslední	k2eAgInSc4d1
Block	Block	k1gInSc4
4	#num#	k4
a	a	k8xC
Block	Block	k1gInSc4
5	#num#	k4
byly	být	k5eAaImAgFnP
optimalizovány	optimalizovat	k5eAaBmNgFnP
pro	pro	k7c4
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
nejefektivnější	efektivní	k2eAgFnSc1d3
znovupoužitelnost	znovupoužitelnost	k1gFnSc1
prvních	první	k4xOgInPc2
stupňů	stupeň	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dríve	Dríev	k1gFnSc2
bylo	být	k5eAaImAgNnS
počítáno	počítat	k5eAaImNgNnS
i	i	k9
s	s	k7c7
dalším	další	k2eAgInSc7d1
vývojem	vývoj	k1gInSc7
<g/>
,	,	kIx,
při	při	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
by	by	kYmCp3nP
byly	být	k5eAaImAgFnP
stávající	stávající	k2eAgInPc4d1
motory	motor	k1gInPc4
Merlin	Merlina	k1gFnPc2
nahrazeny	nahrazen	k2eAgInPc1d1
motory	motor	k1gInPc1
Raptor	Raptor	k1gInSc4
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
však	však	k9
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
jestli	jestli	k8xS
jen	jen	k9
na	na	k7c6
druhém	druhý	k4xOgInSc6
<g/>
,	,	kIx,
nebo	nebo	k8xC
i	i	k8xC
prvním	první	k4xOgInSc6
stupni	stupeň	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
však	však	k9
SpaceX	SpaceX	k1gFnSc1
cílí	cílit	k5eAaImIp3nS
zejména	zejména	k9
na	na	k7c4
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
nejrychlejší	rychlý	k2eAgInSc1d3
vývoj	vývoj	k1gInSc1
nosného	nosný	k2eAgInSc2d1
systému	systém	k1gInSc2
Starship	Starship	k1gMnSc1
a	a	k8xC
své	svůj	k3xOyFgFnSc2
aktivity	aktivita	k1gFnSc2
směřující	směřující	k2eAgFnSc2d1
k	k	k7c3
další	další	k2eAgFnSc3d1
modernizaci	modernizace	k1gFnSc3
Falcon	Falcon	k1gInSc1
9	#num#	k4
výrazně	výrazně	k6eAd1
utlumila	utlumit	k5eAaPmAgNnP
zejména	zejména	k9
proto	proto	k8xC
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
nedocházelo	docházet	k5eNaImAgNnS
ke	k	k7c3
štěpení	štěpení	k1gNnSc3
vývojových	vývojový	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Falcon	Falcon	k1gInSc1
9	#num#	k4
v	v	k7c4
<g/>
1.0	1.0	k4
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Falcon	Falcon	k1gNnSc1
9	#num#	k4
v	v	k7c4
<g/>
1.0	1.0	k4
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
začal	začít	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
a	a	k8xC
první	první	k4xOgInSc4
let	let	k1gInSc4
přišel	přijít	k5eAaPmAgMnS
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letů	let	k1gInPc2
bylo	být	k5eAaImAgNnS
celkem	celkem	k6eAd1
pět	pět	k4xCc1
a	a	k8xC
všechny	všechen	k3xTgFnPc1
splnily	splnit	k5eAaPmAgFnP
primární	primární	k2eAgFnSc4d1
část	část	k1gFnSc4
mise	mise	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgInSc1d1
start	start	k1gInSc1
proběhl	proběhnout	k5eAaPmAgInS
1	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Falcon	Falcon	k1gInSc1
9	#num#	k4
v	v	k7c4
<g/>
1.0	1.0	k4
postupně	postupně	k6eAd1
vynesl	vynést	k5eAaPmAgMnS
čtyři	čtyři	k4xCgFnPc4
zásobovací	zásobovací	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
Dragon	Dragon	k1gMnSc1
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
dva	dva	k4xCgInPc4
byly	být	k5eAaImAgInP
testovací	testovací	k2eAgInSc4d1
lety	let	k1gInPc4
a	a	k8xC
dva	dva	k4xCgInPc4
operační	operační	k2eAgInPc4d1
podle	podle	k7c2
smlouvy	smlouva	k1gFnSc2
CRS	CRS	kA
(	(	kIx(
<g/>
CRS-	CRS-	k1gFnSc1
<g/>
1	#num#	k4
<g/>
,	,	kIx,
CRS-	CRS-	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Falcon	Falcon	k1gInSc1
9	#num#	k4
v	v	k7c4
<g/>
1.1	1.1	k4
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Falcon	Falcon	k1gNnSc1
9	#num#	k4
v	v	k7c4
<g/>
1.1	1.1	k4
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
verze	verze	k1gFnSc1
létala	létat	k5eAaImAgFnS
mezi	mezi	k7c4
roky	rok	k1gInPc4
2013	#num#	k4
a	a	k8xC
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oproti	oproti	k7c3
předchozí	předchozí	k2eAgFnSc3d1
verzi	verze	k1gFnSc3
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
nárůstu	nárůst	k1gInSc3
výkonu	výkon	k1gInSc2
a	a	k8xC
hmotnosti	hmotnost	k1gFnSc2
o	o	k7c4
60	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
tak	tak	k9
i	i	k9
nádrže	nádrž	k1gFnPc1
byly	být	k5eAaImAgFnP
takto	takto	k6eAd1
zvětšeny	zvětšen	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Motory	motor	k1gInPc4
na	na	k7c6
prvním	první	k4xOgInSc6
stupni	stupeň	k1gInSc6
byly	být	k5eAaImAgFnP
nahrazeny	nahradit	k5eAaPmNgFnP
modernizovanou	modernizovaný	k2eAgFnSc7d1
verzí	verze	k1gFnSc7
Merlin	Merlin	k2eAgInSc1d1
1	#num#	k4
<g/>
D.	D.	kA
Tato	tento	k3xDgFnSc1
zlepšení	zlepšení	k1gNnSc3
zvýšila	zvýšit	k5eAaPmAgFnS
užitečné	užitečný	k2eAgNnSc4d1
zatížení	zatížení	k1gNnSc4
až	až	k9
na	na	k7c4
13	#num#	k4
150	#num#	k4
kg	kg	kA
na	na	k7c4
LEO	Leo	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
prvním	první	k4xOgInSc6
stupni	stupeň	k1gInSc6
se	se	k3xPyFc4
změnilo	změnit	k5eAaPmAgNnS
uspořádání	uspořádání	k1gNnSc4
motorů	motor	k1gInPc2
<g/>
,	,	kIx,
uspořádání	uspořádání	k1gNnSc1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
octaweb	octawba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
této	tento	k3xDgFnSc2
verze	verze	k1gFnSc2
už	už	k6eAd1
se	se	k3xPyFc4
začalo	začít	k5eAaPmAgNnS
zkoušet	zkoušet	k5eAaImF
přistávání	přistávání	k1gNnSc3
prvního	první	k4xOgInSc2
stupně	stupeň	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
této	tento	k3xDgFnSc3
verzi	verze	k1gFnSc3
se	se	k3xPyFc4
nikdy	nikdy	k6eAd1
přistát	přistát	k5eAaImF,k5eAaPmF
nepodařilo	podařit	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
verze	verze	k1gFnSc1
měla	mít	k5eAaImAgFnS
jednu	jeden	k4xCgFnSc4
nehodu	nehoda	k1gFnSc4
a	a	k8xC
to	ten	k3xDgNnSc1
při	při	k7c6
letu	let	k1gInSc6
CRS-	CRS-	k1gFnSc2
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Falcon	Falcon	k1gInSc1
9	#num#	k4
FT	FT	kA
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Falcon	Falcona	k1gFnPc2
9	#num#	k4
FT	FT	kA
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
v	v	k7c6
současnosti	současnost	k1gFnSc6
používaná	používaný	k2eAgFnSc1d1
verze	verze	k1gFnSc1
má	mít	k5eAaImIp3nS
údajně	údajně	k6eAd1
až	až	k9
o	o	k7c4
30	#num#	k4
%	%	kIx~
vyšší	vysoký	k2eAgInSc1d2
tah	tah	k1gInSc1
než	než	k8xS
verze	verze	k1gFnSc1
předchozí	předchozí	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
tato	tento	k3xDgFnSc1
verze	verze	k1gFnSc1
začala	začít	k5eAaPmAgFnS
létat	létat	k5eAaImF
s	s	k7c7
více	hodně	k6eAd2
podchlazeným	podchlazený	k2eAgNnSc7d1
palivem	palivo	k1gNnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
po	po	k7c6
nehodě	nehoda	k1gFnSc6
při	při	k7c6
tankování	tankování	k1gNnSc6
druhého	druhý	k4xOgInSc2
stupně	stupeň	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
na	na	k7c6
raketě	raketa	k1gFnSc6
umístěná	umístěný	k2eAgFnSc1d1
družice	družice	k1gFnSc1
Amos-	Amos-	k1gFnSc2
<g/>
6	#num#	k4
<g/>
,	,	kIx,
se	se	k3xPyFc4
přešlo	přejít	k5eAaPmAgNnS
zpět	zpět	k6eAd1
na	na	k7c4
teplejší	teplý	k2eAgNnSc4d2
palivo	palivo	k1gNnSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
mělo	mít	k5eAaImAgNnS
za	za	k7c4
následek	následek	k1gInSc4
mírné	mírný	k2eAgNnSc1d1
snížení	snížení	k1gNnSc1
výkonu	výkon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
svém	svůj	k3xOyFgInSc6
prvním	první	k4xOgInSc6
letu	let	k1gInSc6
v	v	k7c6
prosinci	prosinec	k1gInSc6
2015	#num#	k4
(	(	kIx(
<g/>
se	s	k7c7
satelitem	satelit	k1gMnSc7
ORBCOMM-	ORBCOMM-	k1gMnSc7
<g/>
2	#num#	k4
<g/>
)	)	kIx)
dokázal	dokázat	k5eAaPmAgInS
první	první	k4xOgInSc4
stupeň	stupeň	k1gInSc4
rakety	raketa	k1gFnSc2
poprvé	poprvé	k6eAd1
přistát	přistát	k5eAaPmF,k5eAaImF
zpět	zpět	k6eAd1
na	na	k7c4
Zemi	zem	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
březnu	březen	k1gInSc6
2017	#num#	k4
byl	být	k5eAaImAgMnS
ke	k	k7c3
startu	start	k1gInSc3
se	s	k7c7
satelitem	satelit	k1gInSc7
SES-10	SES-10	k1gFnSc2
poprvé	poprvé	k6eAd1
použit	použít	k5eAaPmNgInS
již	již	k6eAd1
jednou	jeden	k4xCgFnSc7
letěný	letěný	k2eAgInSc4d1
první	první	k4xOgInSc4
stupeň	stupeň	k1gInSc4
rakety	raketa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1
varianta	varianta	k1gFnSc1
FT	FT	kA
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nS
ještě	ještě	k9
na	na	k7c4
varianty	varianta	k1gFnPc4
Block	Blocka	k1gFnPc2
3	#num#	k4
<g/>
,	,	kIx,
Block	Block	k1gInSc1
4	#num#	k4
a	a	k8xC
Block	Block	k1gInSc1
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nástup	nástup	k1gInSc1
Blocku	Block	k1gInSc2
4	#num#	k4
byl	být	k5eAaImAgInS
postupný	postupný	k2eAgInSc1d1
a	a	k8xC
nejdříve	dříve	k6eAd3
v	v	k7c6
této	tento	k3xDgFnSc6
verzi	verze	k1gFnSc6
létaly	létat	k5eAaImAgInP
pouze	pouze	k6eAd1
druhé	druhý	k4xOgInPc1
stupně	stupeň	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
prvnímu	první	k4xOgInSc3
letu	let	k1gInSc3
kompletní	kompletní	k2eAgFnSc2d1
rakety	raketa	k1gFnSc2
Block	Block	k1gInSc4
4	#num#	k4
došlo	dojít	k5eAaPmAgNnS
při	při	k7c6
misi	mise	k1gFnSc6
CRS-	CRS-	k1gFnSc1
<g/>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
listopadu	listopad	k1gInSc3
2020	#num#	k4
je	být	k5eAaImIp3nS
takřka	takřka	k6eAd1
výhradně	výhradně	k6eAd1
používanou	používaný	k2eAgFnSc4d1
poslední	poslední	k2eAgFnSc4d1
verze	verze	k1gFnPc1
rakety	raketa	k1gFnPc1
-	-	kIx~
Block	Block	k1gInSc1
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Testovací	testovací	k2eAgFnSc1d1
verze	verze	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Grasshopper	Grasshoppra	k1gFnPc2
(	(	kIx(
<g/>
raketa	raketa	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
počátcích	počátek	k1gInPc6
testování	testování	k1gNnSc2
přistávání	přistávání	k1gNnSc2
prvních	první	k4xOgInPc2
stupňů	stupeň	k1gInPc2
byly	být	k5eAaImAgFnP
použity	použit	k2eAgFnPc1d1
rakety	raketa	k1gFnPc1
Grasshoper	Grasshopra	k1gFnPc2
a	a	k8xC
Falcon	Falcona	k1gFnPc2
9R	9R	k4
Dev	Dev	k1gFnPc2
<g/>
,	,	kIx,
obě	dva	k4xCgFnPc1
derivované	derivovaný	k2eAgFnPc1d1
z	z	k7c2
letové	letový	k2eAgFnSc2d1
konfigurace	konfigurace	k1gFnSc2
v	v	k7c4
<g/>
1.1	1.1	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
zmíněná	zmíněný	k2eAgFnSc1d1
testovala	testovat	k5eAaImAgFnS
schopnost	schopnost	k1gFnSc1
vzlétat	vzlétat	k5eAaImF
a	a	k8xC
přistávat	přistávat	k5eAaImF
při	při	k7c6
malých	malý	k2eAgFnPc6d1
rychlostech	rychlost	k1gFnPc6
a	a	k8xC
v	v	k7c6
malých	malý	k2eAgFnPc6d1
výškách	výška	k1gFnPc6
<g/>
,	,	kIx,
s	s	k7c7
minimálním	minimální	k2eAgInSc7d1
laterálním	laterální	k2eAgInSc7d1
pohybem	pohyb	k1gInSc7
-	-	kIx~
maximální	maximální	k2eAgFnSc1d1
dosažená	dosažený	k2eAgFnSc1d1
výška	výška	k1gFnSc1
byla	být	k5eAaImAgFnS
744	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhá	druhý	k4xOgFnSc1
byla	být	k5eAaImAgFnS
více	hodně	k6eAd2
podobná	podobný	k2eAgFnSc1d1
své	svůj	k3xOyFgFnSc3
předloze	předloha	k1gFnSc3
(	(	kIx(
<g/>
hlavní	hlavní	k2eAgFnSc1d1
část	část	k1gFnSc1
trupu	trup	k1gInSc2
byla	být	k5eAaImAgFnS
standardní	standardní	k2eAgFnSc1d1
<g/>
)	)	kIx)
a	a	k8xC
sloužila	sloužit	k5eAaImAgFnS
k	k	k7c3
testování	testování	k1gNnSc3
některých	některý	k3yIgInPc2
zásadních	zásadní	k2eAgInPc2d1
prvků	prvek	k1gInPc2
později	pozdě	k6eAd2
použitých	použitý	k2eAgInPc2d1
v	v	k7c6
letových	letový	k2eAgFnPc6d1
verzích	verze	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bohužel	bohužel	k6eAd1
při	při	k7c6
pátém	pátý	k4xOgInSc6
testu	test	k1gInSc6
v	v	k7c6
srpnu	srpen	k1gInSc6
2014	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
fatální	fatální	k2eAgFnSc3d1
anomálii	anomálie	k1gFnSc3
na	na	k7c6
senzorovém	senzorový	k2eAgNnSc6d1
vybavení	vybavení	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
způsobilo	způsobit	k5eAaPmAgNnS
její	její	k3xOp3gNnSc1
zničení	zničení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
uvedeno	uvést	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
letová	letový	k2eAgFnSc1d1
verze	verze	k1gFnSc1
má	mít	k5eAaImIp3nS
pro	pro	k7c4
daný	daný	k2eAgInSc4d1
senzor	senzor	k1gInSc4
redundantní	redundantní	k2eAgFnSc4d1
zálohu	záloha	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
tudíž	tudíž	k8xC
by	by	kYmCp3nP
ke	k	k7c3
stejné	stejný	k2eAgFnSc3d1
anomálii	anomálie	k1gFnSc3
dojít	dojít	k5eAaPmF
nemohlo	moct	k5eNaImAgNnS
<g/>
.	.	kIx.
</s>
<s>
Design	design	k1gInSc1
</s>
<s>
První	první	k4xOgInSc1
stupeň	stupeň	k1gInSc1
je	být	k5eAaImIp3nS
osazen	osadit	k5eAaPmNgInS
devíti	devět	k4xCc7
motory	motor	k1gInPc7
typu	typ	k1gInSc2
Merlin	Merlin	k2eAgInSc4d1
1	#num#	k4
<g/>
D	D	kA
<g/>
+	+	kIx~
na	na	k7c4
kapalný	kapalný	k2eAgInSc4d1
kyslík	kyslík	k1gInSc4
a	a	k8xC
čistý	čistý	k2eAgInSc1d1
letecký	letecký	k2eAgInSc1d1
petrolej	petrolej	k1gInSc1
(	(	kIx(
<g/>
LOX	LOX	kA
<g/>
/	/	kIx~
<g/>
RP-	RP-	k1gFnSc1
<g/>
1	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
konfiguraci	konfigurace	k1gFnSc6
octaweb	octawba	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
osm	osm	k4xCc1
motorů	motor	k1gInPc2
rozmístěných	rozmístěný	k2eAgInPc2d1
po	po	k7c6
obvodu	obvod	k1gInSc6
obklopuje	obklopovat	k5eAaImIp3nS
jeden	jeden	k4xCgInSc1
motor	motor	k1gInSc1
středový	středový	k2eAgInSc1d1
(	(	kIx(
<g/>
označován	označovat	k5eAaImNgInS
jako	jako	k9
motor	motor	k1gInSc1
číslo	číslo	k1gNnSc1
pět	pět	k4xCc1
-	-	kIx~
zároveň	zároveň	k6eAd1
nabízí	nabízet	k5eAaImIp3nS
i	i	k9
nejširší	široký	k2eAgFnSc4d3
možnost	možnost	k1gFnSc4
vektorování	vektorování	k1gNnSc2
tahu	tah	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Motory	motor	k1gInPc1
jsou	být	k5eAaImIp3nP
se	s	k7c7
zbytkem	zbytek	k1gInSc7
prvního	první	k4xOgNnSc2
stupně	stupeň	k1gInPc1
spojeny	spojen	k2eAgInPc1d1
kovovou	kovový	k2eAgFnSc7d1
podpůrnou	podpůrný	k2eAgFnSc7d1
strukturou	struktura	k1gFnSc7
<g/>
,	,	kIx,
do	do	k7c2
které	který	k3yIgFnSc2,k3yQgFnSc2,k3yRgFnSc2
jsou	být	k5eAaImIp3nP
motory	motor	k1gInPc1
zavařeny	zavařen	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
předchozí	předchozí	k2eAgFnSc2d1
konfigurace	konfigurace	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byly	být	k5eAaImAgInP
motory	motor	k1gInPc1
rozmístěny	rozmístěn	k2eAgInPc1d1
v	v	k7c6
mřížce	mřížka	k1gFnSc6
3	#num#	k4
<g/>
x	x	k?
<g/>
3	#num#	k4
(	(	kIx(
<g/>
tzv.	tzv.	kA
tic-tac-toe	tic-tac-toe	k1gFnSc1
konfigurace	konfigurace	k1gFnSc1
<g/>
)	)	kIx)
tato	tento	k3xDgFnSc1
konstrukce	konstrukce	k1gFnSc1
zjednodušuje	zjednodušovat	k5eAaImIp3nS
výrobní	výrobní	k2eAgInSc4d1
proces	proces	k1gInSc4
a	a	k8xC
i	i	k9
design	design	k1gInSc1
samotný	samotný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
startu	start	k1gInSc6
je	být	k5eAaImIp3nS
všech	všecek	k3xTgInPc2
devět	devět	k4xCc4
motorů	motor	k1gInPc2
zažehnuto	zažehnout	k5eAaPmNgNnS
pyroforickou	pyroforický	k2eAgFnSc7d1
směsí	směs	k1gFnSc7
TEA-TEB	TEA-TEB	k1gFnSc2
(	(	kIx(
<g/>
triethylhliník-triethylboritan	triethylhliník-triethylboritan	k1gInSc1
<g/>
)	)	kIx)
z	z	k7c2
pozemních	pozemní	k2eAgFnPc2d1
nádrží	nádrž	k1gFnPc2
startovního	startovní	k2eAgInSc2d1
komplexu	komplex	k1gInSc2
<g/>
,	,	kIx,
raketa	raketa	k1gFnSc1
nicméně	nicméně	k8xC
disponuje	disponovat	k5eAaBmIp3nS
i	i	k9
vlastními	vlastní	k2eAgFnPc7d1
nádržemi	nádrž	k1gFnPc7
na	na	k7c4
tuto	tento	k3xDgFnSc4
směs	směs	k1gFnSc4
-	-	kIx~
kvůli	kvůli	k7c3
pozdějším	pozdní	k2eAgInPc3d2
zážehům	zážeh	k1gInPc3
při	při	k7c6
přistávacích	přistávací	k2eAgInPc6d1
manévrech	manévr	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Směs	směsa	k1gFnPc2
se	se	k3xPyFc4
vyznačuje	vyznačovat	k5eAaImIp3nS
typickým	typický	k2eAgInSc7d1
zeleným	zelený	k2eAgInSc7d1
plamenem	plamen	k1gInSc7
(	(	kIx(
<g/>
po	po	k7c6
přistání	přistání	k1gNnSc6
1	#num#	k4
<g/>
.	.	kIx.
stupně	stupeň	k1gInSc2
mise	mise	k1gFnSc2
JCSAT-14	JCSAT-14	k1gFnSc1
byl	být	k5eAaImAgInS
v	v	k7c6
motorové	motorový	k2eAgFnSc6d1
sekci	sekce	k1gFnSc6
zpozorován	zpozorován	k2eAgInSc1d1
zelený	zelený	k2eAgInSc1d1
plamen	plamen	k1gInSc1
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Nad	nad	k7c7
motorovou	motorový	k2eAgFnSc7d1
sekcí	sekce	k1gFnSc7
jsou	být	k5eAaImIp3nP
umístěny	umístit	k5eAaPmNgFnP
čtyři	čtyři	k4xCgFnPc1
přistávací	přistávací	k2eAgFnPc1d1
nohy	noha	k1gFnPc1
<g/>
,	,	kIx,
vyrobené	vyrobený	k2eAgInPc1d1
z	z	k7c2
uhlíkového	uhlíkový	k2eAgNnSc2d1
vlákna	vlákno	k1gNnSc2
a	a	k8xC
voštinového	voštinový	k2eAgInSc2d1
kompozitního	kompozitní	k2eAgInSc2d1
hliníku	hliník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dohromady	dohromady	k6eAd1
váží	vážit	k5eAaImIp3nP
méně	málo	k6eAd2
než	než	k8xS
2,1	2,1	k4
tuny	tuna	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
vzletu	vzlet	k1gInSc6
jsou	být	k5eAaImIp3nP
složeny	složit	k5eAaPmNgInP
podél	podél	k7c2
trupu	trup	k1gInSc2
<g/>
,	,	kIx,
před	před	k7c7
přistáním	přistání	k1gNnSc7
je	on	k3xPp3gNnSc4
písty	píst	k1gInPc1
roztáhnou	roztáhnout	k5eAaPmIp3nP
pomocí	pomocí	k7c2
stlačeného	stlačený	k2eAgNnSc2d1
hélia	hélium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkové	celkový	k2eAgNnSc1d1
rozpětí	rozpětí	k1gNnSc4
noh	noha	k1gFnPc2
je	být	k5eAaImIp3nS
až	až	k9
18	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Gwynne	Gwynn	k1gInSc5
Shotwellové	Shotwellové	k2eAgMnPc1d1
jsou	být	k5eAaImIp3nP
přistávací	přistávací	k2eAgFnPc4d1
nohy	noha	k1gFnPc4
konfigurace	konfigurace	k1gFnSc2
FT	FT	kA
silnější	silný	k2eAgFnSc2d2
než	než	k8xS
ty	ten	k3xDgInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
byly	být	k5eAaImAgFnP
montované	montovaný	k2eAgInPc1d1
na	na	k7c4
trup	trup	k1gInSc4
v	v	k7c4
<g/>
1.1	1.1	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prohlásila	prohlásit	k5eAaPmAgFnS
to	ten	k3xDgNnSc4
v	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
selhání	selhání	k1gNnSc4
jedné	jeden	k4xCgFnSc2
z	z	k7c2
nich	on	k3xPp3gFnPc2
během	během	k7c2
přistání	přistání	k1gNnSc2
letu	let	k1gInSc2
21	#num#	k4
(	(	kIx(
<g/>
Jason-	Jason-	k1gFnSc1
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Motory	motor	k1gInPc1
prvního	první	k4xOgInSc2
stupně	stupeň	k1gInSc2
hoří	hořet	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
140	#num#	k4
sekund	sekunda	k1gFnPc2
(	(	kIx(
<g/>
pokud	pokud	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
misi	mise	k1gFnSc4
na	na	k7c4
nízkou	nízký	k2eAgFnSc4d1
oběžnou	oběžný	k2eAgFnSc4d1
dráhu	dráha	k1gFnSc4
<g/>
)	)	kIx)
nebo	nebo	k8xC
až	až	k9
156	#num#	k4
sekund	sekunda	k1gFnPc2
(	(	kIx(
<g/>
vysokoenergetické	vysokoenergetický	k2eAgFnSc2d1
mise	mise	k1gFnSc2
na	na	k7c4
přechodovou	přechodový	k2eAgFnSc4d1
dráhu	dráha	k1gFnSc4
ke	k	k7c3
geostacionární	geostacionární	k2eAgFnSc3d1
dráze	dráha	k1gFnSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krajní	krajní	k2eAgInPc1d1
případy	případ	k1gInPc1
letů	let	k1gInPc2
na	na	k7c6
GTO	GTO	kA
pak	pak	k6eAd1
značně	značně	k6eAd1
komplikují	komplikovat	k5eAaBmIp3nP
přistání	přistání	k1gNnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
kvůli	kvůli	k7c3
úspoře	úspora	k1gFnSc3
paliva	palivo	k1gNnSc2
není	být	k5eNaImIp3nS
prováděn	prováděn	k2eAgInSc1d1
tzv.	tzv.	kA
boostback	boostback	k1gInSc1
burn	burn	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
upravuje	upravovat	k5eAaImIp3nS
přistávací	přistávací	k2eAgFnSc4d1
trajektorii	trajektorie	k1gFnSc4
stupně	stupeň	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
raketa	raketa	k1gFnSc1
proto	proto	k8xC
letí	letět	k5eAaImIp3nS
k	k	k7c3
přistávací	přistávací	k2eAgFnSc3d1
zóně	zóna	k1gFnSc3
po	po	k7c6
takřka	takřka	k6eAd1
balistické	balistický	k2eAgFnSc6d1
křivce	křivka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Nad	nad	k7c7
hlavní	hlavní	k2eAgFnSc7d1
částí	část	k1gFnSc7
trupu	trup	k1gInSc2
prvního	první	k4xOgInSc2
stupně	stupeň	k1gInSc2
se	se	k3xPyFc4
pak	pak	k6eAd1
nachází	nacházet	k5eAaImIp3nS
tzv.	tzv.	kA
mezistupeň	mezistupeň	k1gInSc1
(	(	kIx(
<g/>
interstage	interstage	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
kompozitní	kompozitní	k2eAgFnSc1d1
konstrukce	konstrukce	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
chrání	chránit	k5eAaImIp3nS
motor	motor	k1gInSc1
druhého	druhý	k4xOgInSc2
stupně	stupeň	k1gInSc2
během	během	k7c2
vzletu	vzlet	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
následně	následně	k6eAd1
zajišťuje	zajišťovat	k5eAaImIp3nS
jeho	jeho	k3xOp3gNnSc1
bezpečné	bezpečný	k2eAgNnSc1d1
oddělení	oddělení	k1gNnSc1
<g/>
,	,	kIx,
čehož	což	k3yRnSc2,k3yQnSc2
je	být	k5eAaImIp3nS
dosaženo	dosáhnout	k5eAaPmNgNnS
pomocí	pomocí	k7c2
pneumatického	pneumatický	k2eAgInSc2d1
oddělovacího	oddělovací	k2eAgInSc2d1
systému	systém	k1gInSc2
-	-	kIx~
ten	ten	k3xDgMnSc1
je	být	k5eAaImIp3nS
unikátní	unikátní	k2eAgMnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
většina	většina	k1gFnSc1
nosných	nosný	k2eAgFnPc2d1
raket	raketa	k1gFnPc2
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
pyrotechnického	pyrotechnický	k2eAgNnSc2d1
oddělení	oddělení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezistupeň	mezistupeň	k1gInSc1
se	se	k3xPyFc4
následně	následně	k6eAd1
vrací	vracet	k5eAaImIp3nS
k	k	k7c3
přistání	přistání	k1gNnSc3
spolu	spolu	k6eAd1
s	s	k7c7
prvním	první	k4xOgInSc7
stupněm	stupeň	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
oddělení	oddělení	k1gNnSc6
prvního	první	k4xOgInSc2
stupně	stupeň	k1gInSc2
dochází	docházet	k5eAaImIp3nS
po	po	k7c6
pěti	pět	k4xCc6
až	až	k9
sedmi	sedm	k4xCc6
sekundách	sekunda	k1gFnPc6
k	k	k7c3
zážehu	zážeh	k1gInSc3
jediného	jediný	k2eAgInSc2d1
motoru	motor	k1gInSc2
druhého	druhý	k4xOgInSc2
stupně	stupeň	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
vakuový	vakuový	k2eAgInSc4d1
derivát	derivát	k1gInSc4
motorů	motor	k1gInPc2
prvního	první	k4xOgInSc2
stupně	stupeň	k1gInSc2
<g/>
,	,	kIx,
běžně	běžně	k6eAd1
označovaný	označovaný	k2eAgInSc1d1
jako	jako	k8xS,k8xC
M	M	kA
<g/>
1	#num#	k4
<g/>
DVac	DVac	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
vyznačuje	vyznačovat	k5eAaImIp3nS
se	se	k3xPyFc4
především	především	k9
podstatně	podstatně	k6eAd1
větší	veliký	k2eAgFnSc7d2
tryskou	tryska	k1gFnSc7
s	s	k7c7
větším	veliký	k2eAgInSc7d2
expanzním	expanzní	k2eAgInSc7d1
poměrem	poměr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
motorů	motor	k1gInPc2
prvního	první	k4xOgInSc2
stupně	stupeň	k1gInSc2
M	M	kA
<g/>
1	#num#	k4
<g/>
DVac	DVac	k1gInSc1
nabízí	nabízet	k5eAaImIp3nS
také	také	k9
širší	široký	k2eAgFnSc1d2
možnosti	možnost	k1gFnPc1
regulace	regulace	k1gFnSc2
tahu	tah	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
se	s	k7c7
spodní	spodní	k2eAgFnSc7d1
hranicí	hranice	k1gFnSc7
až	až	k9
39	#num#	k4
<g/>
%	%	kIx~
nominálního	nominální	k2eAgInSc2d1
výkonu	výkon	k1gInSc2
(	(	kIx(
<g/>
u	u	k7c2
motorů	motor	k1gInPc2
prvního	první	k4xOgInSc2
stupně	stupeň	k1gInSc2
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
70	#num#	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
druhém	druhý	k4xOgInSc6
stupni	stupeň	k1gInSc6
je	být	k5eAaImIp3nS
ve	v	k7c6
většině	většina	k1gFnSc6
případů	případ	k1gInPc2
osazen	osazen	k2eAgInSc4d1
aerodynamický	aerodynamický	k2eAgInSc4d1
kryt	kryt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oproti	oproti	k7c3
nosným	nosný	k2eAgInPc3d1
systémům	systém	k1gInPc3
jako	jako	k9
je	být	k5eAaImIp3nS
např.	např.	kA
Atlas	Atlas	k1gInSc1
V	V	kA
<g/>
,	,	kIx,
raketa	raketa	k1gFnSc1
Falcon	Falcona	k1gFnPc2
disponuje	disponovat	k5eAaBmIp3nS
krytem	kryt	k1gInSc7
vyráběným	vyráběný	k2eAgInSc7d1
pouze	pouze	k6eAd1
v	v	k7c6
jedné	jeden	k4xCgFnSc6
velikosti	velikost	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
o	o	k7c6
průměru	průměr	k1gInSc6
5,2	5,2	k4
metru	metr	k1gInSc2
a	a	k8xC
výšce	výška	k1gFnSc6
13,2	13,2	k4
metru	metr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc1
dvě	dva	k4xCgFnPc1
poloviny	polovina	k1gFnPc1
jsou	být	k5eAaImIp3nP
vyrobeny	vyrobit	k5eAaPmNgFnP
z	z	k7c2
uhlíkového	uhlíkový	k2eAgNnSc2d1
vlákna	vlákno	k1gNnSc2
a	a	k8xC
voštinového	voštinový	k2eAgInSc2d1
kompozitního	kompozitní	k2eAgInSc2d1
hliníku	hliník	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
mezi	mezi	k7c7
oddělením	oddělení	k1gNnSc7
druhého	druhý	k4xOgInSc2
stupně	stupeň	k1gInSc2
a	a	k8xC
ukončením	ukončení	k1gNnSc7
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
prvního	první	k4xOgInSc2
zážehu	zážeh	k1gInSc2
jsou	být	k5eAaImIp3nP
pneumaticky	pneumaticky	k6eAd1
odděleny	oddělit	k5eAaPmNgInP
<g/>
.	.	kIx.
</s>
<s>
Aerodynamický	aerodynamický	k2eAgInSc1d1
kryt	kryt	k1gInSc1
je	být	k5eAaImIp3nS
užíván	užíván	k2eAgInSc1d1
k	k	k7c3
ochraně	ochrana	k1gFnSc3
citlivého	citlivý	k2eAgInSc2d1
nákladu	náklad	k1gInSc2
během	během	k7c2
vzletu	vzlet	k1gInSc2
-	-	kIx~
většinou	většinou	k6eAd1
jde	jít	k5eAaImIp3nS
o	o	k7c4
citlivé	citlivý	k2eAgInPc4d1
satelity	satelit	k1gInPc4
určené	určený	k2eAgInPc4d1
k	k	k7c3
nasazení	nasazení	k1gNnSc3
ve	v	k7c6
vakuu	vakuum	k1gNnSc6
vesmíru	vesmír	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
když	když	k8xS
americká	americký	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Sierra	Sierra	k1gFnSc1
Nevada	Nevada	k1gFnSc1
zvažovala	zvažovat	k5eAaImAgFnS
raketu	raketa	k1gFnSc4
Falcon	Falcona	k1gFnPc2
jako	jako	k8xC,k8xS
alternativu	alternativa	k1gFnSc4
pro	pro	k7c4
vynášení	vynášení	k1gNnSc4
své	svůj	k3xOyFgFnSc2
lodi	loď	k1gFnSc2
Dreamchaser	Dreamchaser	k1gInSc1
(	(	kIx(
<g/>
místo	místo	k1gNnSc1
Atlasu	Atlas	k1gInSc2
V	V	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
uvedeno	uvést	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
loď	loď	k1gFnSc1
byla	být	k5eAaImAgFnS
také	také	k9
vynášena	vynášet	k5eAaImNgFnS
uvnitř	uvnitř	k7c2
aerodynamického	aerodynamický	k2eAgInSc2d1
krytu	kryt	k1gInSc2
(	(	kIx(
<g/>
došlo	dojít	k5eAaPmAgNnS
by	by	kYmCp3nP
ale	ale	k9
ke	k	k7c3
složení	složení	k1gNnSc3
křídel	křídlo	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
probíhá	probíhat	k5eAaImIp3nS
let	let	k1gInSc4
s	s	k7c7
lodí	loď	k1gFnSc7
Dragon	Dragon	k1gMnSc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
Dragon	Dragon	k1gMnSc1
v	v	k7c6
<g/>
2	#num#	k4
pak	pak	k6eAd1
aerodynamický	aerodynamický	k2eAgInSc1d1
kryt	kryt	k1gInSc1
není	být	k5eNaImIp3nS
osazován	osazován	k2eAgInSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
ho	on	k3xPp3gMnSc4
není	být	k5eNaImIp3nS
třeba	třeba	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Znovupoužitelnost	Znovupoužitelnost	k1gFnSc1
a	a	k8xC
ekonomika	ekonomika	k1gFnSc1
letů	let	k1gInPc2
</s>
<s>
K	k	k7c3
listopadu	listopad	k1gInSc3
2020	#num#	k4
je	být	k5eAaImIp3nS
takřka	takřka	k6eAd1
výhradně	výhradně	k6eAd1
používanou	používaný	k2eAgFnSc4d1
poslední	poslední	k2eAgFnSc4d1
verze	verze	k1gFnSc2
rakety	raketa	k1gFnPc1
Falcon	Falcon	k1gNnSc1
9	#num#	k4
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
verzi	verze	k1gFnSc6
Block	Block	k1gMnSc1
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znovupoužívaní	Znovupoužívaný	k2eAgMnPc5d1
prvních	první	k4xOgNnPc2
stupňů	stupeň	k1gInPc2
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
rutinním	rutinní	k2eAgMnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dle	dle	k7c2
dostupných	dostupný	k2eAgFnPc2d1
informací	informace	k1gFnPc2
to	ten	k3xDgNnSc4
významně	významně	k6eAd1
přispívá	přispívat	k5eAaImIp3nS
k	k	k7c3
ekonomice	ekonomika	k1gFnSc3
provozu	provoz	k1gInSc2
této	tento	k3xDgFnSc2
rakety	raketa	k1gFnSc2
a	a	k8xC
rovněž	rovněž	k9
jsou	být	k5eAaImIp3nP
díky	díky	k7c3
tomuto	tento	k3xDgNnSc3
značně	značně	k6eAd1
zredukovány	zredukován	k2eAgInPc1d1
požadavky	požadavek	k1gInPc1
na	na	k7c4
výrobní	výrobní	k2eAgFnPc4d1
kapacity	kapacita	k1gFnPc4
<g/>
,	,	kIx,
zejména	zejména	k9
raketových	raketový	k2eAgInPc2d1
motorů	motor	k1gInPc2
Merlin	Merlina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Již	již	k6eAd1
letěné	letěný	k2eAgInPc1d1
první	první	k4xOgInPc1
stupně	stupeň	k1gInPc1
rakety	raketa	k1gFnSc2
<g/>
,	,	kIx,
resp.	resp.	kA
jejich	jejich	k3xOp3gFnSc4
znovupoužitelnost	znovupoužitelnost	k1gFnSc4
jsou	být	k5eAaImIp3nP
rovněž	rovněž	k9
představovány	představován	k2eAgFnPc1d1
jako	jako	k8xS,k8xC
významná	významný	k2eAgFnSc1d1
spolehlivostní	spolehlivostní	k2eAgFnSc1d1
výhoda	výhoda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
prezentovány	prezentován	k2eAgFnPc1d1
jako	jako	k8xS,k8xC
prověřené	prověřený	k2eAgFnPc1d1
letem	letem	k6eAd1
(	(	kIx(
<g/>
flight-proven	flight-proven	k2eAgMnSc1d1
<g/>
)	)	kIx)
a	a	k8xC
tudíž	tudíž	k8xC
jasně	jasně	k6eAd1
demonstrovanou	demonstrovaný	k2eAgFnSc7d1
letovou	letový	k2eAgFnSc7d1
spolehlivostí	spolehlivost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
zákazníků	zákazník	k1gMnPc2
Falconu	Falcon	k1gInSc2
9	#num#	k4
původně	původně	k6eAd1
převažovala	převažovat	k5eAaImAgFnS
jistá	jistý	k2eAgFnSc1d1
nedůvěra	nedůvěra	k1gFnSc1
k	k	k7c3
recyklovaným	recyklovaný	k2eAgInPc3d1
stupňům	stupeň	k1gInPc3
<g/>
,	,	kIx,
jakmile	jakmile	k8xS
ale	ale	k8xC
bylo	být	k5eAaImAgNnS
desítkami	desítka	k1gFnPc7
letů	let	k1gInPc2
demonstrováno	demonstrován	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
znovupoužitelnost	znovupoužitelnost	k1gFnSc1
nemá	mít	k5eNaImIp3nS
negativní	negativní	k2eAgInSc4d1
vliv	vliv	k1gInSc4
na	na	k7c4
spolehlivost	spolehlivost	k1gFnSc4
rakety	raketa	k1gFnSc2
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
přehodnocení	přehodnocení	k1gNnSc3
tohoto	tento	k3xDgInSc2
přístupu	přístup	k1gInSc2
a	a	k8xC
i	i	k9
klíčoví	klíčový	k2eAgMnPc1d1
zákazníci	zákazník	k1gMnPc1
jako	jako	k9
NASA	NASA	kA
či	či	k8xC
americké	americký	k2eAgNnSc1d1
ministerstvo	ministerstvo	k1gNnSc1
obrany	obrana	k1gFnSc2
začínají	začínat	k5eAaImIp3nP
výrazněji	výrazně	k6eAd2
využívat	využívat	k5eAaPmF,k5eAaImF
recyklovaných	recyklovaný	k2eAgInPc2d1
strojů	stroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významným	významný	k2eAgInSc7d1
faktorem	faktor	k1gInSc7
je	být	k5eAaImIp3nS
ekonomika	ekonomika	k1gFnSc1
provozu	provoz	k1gInSc2
recyklovaných	recyklovaný	k2eAgFnPc2d1
raket	raketa	k1gFnPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
cena	cena	k1gFnSc1
za	za	k7c4
start	start	k1gInSc4
takovéto	takovýto	k3xDgFnSc2
rakety	raketa	k1gFnSc2
je	být	k5eAaImIp3nS
pro	pro	k7c4
zákazníka	zákazník	k1gMnSc4
významně	významně	k6eAd1
levnější	levný	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>
<s>
Kromě	kromě	k7c2
prvních	první	k4xOgInPc2
stupňů	stupeň	k1gInPc2
rakety	raketa	k1gFnSc2
byla	být	k5eAaImAgFnS
rovněž	rovněž	k9
vyladěna	vyladěn	k2eAgFnSc1d1
schopnost	schopnost	k1gFnSc1
zachycování	zachycování	k1gNnSc2
aerodynamických	aerodynamický	k2eAgInPc2d1
krytů	kryt	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
se	se	k3xPyFc4
rovněž	rovněž	k9
daří	dařit	k5eAaImIp3nS
stále	stále	k6eAd1
častěji	často	k6eAd2
znovupoužívat	znovupoužívat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
zejména	zejména	k9
u	u	k7c2
interních	interní	k2eAgInPc2d1
letů	let	k1gInPc2
firmy	firma	k1gFnSc2
SpaceX	SpaceX	k1gFnSc1
s	s	k7c7
družicemi	družice	k1gFnPc7
Starlink	Starlink	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
ředitelky	ředitelka	k1gFnSc2
SpaceX	SpaceX	k1gFnSc2
Gwynne	Gwynn	k1gInSc5
Shotwellové	Shotwellové	k2eAgFnSc7d1
jsou	být	k5eAaImIp3nP
náklady	náklad	k1gInPc1
spojené	spojený	k2eAgInPc1d1
s	s	k7c7
kontrolou	kontrola	k1gFnSc7
a	a	k8xC
manipulací	manipulace	k1gFnSc7
se	s	k7c7
zachráněnými	zachráněný	k2eAgInPc7d1
prvními	první	k4xOgInPc7
stupni	stupeň	k1gInPc7
výrazně	výrazně	k6eAd1
nižší	nízký	k2eAgMnSc1d2
<g/>
,	,	kIx,
než	než	k8xS
polovina	polovina	k1gFnSc1
ceny	cena	k1gFnSc2
nového	nový	k2eAgInSc2d1
stupně	stupeň	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
náklady	náklad	k1gInPc1
by	by	kYmCp3nP
se	se	k3xPyFc4
navíc	navíc	k6eAd1
časem	časem	k6eAd1
měly	mít	k5eAaImAgFnP
dále	daleko	k6eAd2
snižovat	snižovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
prvních	první	k4xOgInPc2
letů	let	k1gInPc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
letů	let	k1gInPc2
Falconu	Falcona	k1gFnSc4
9	#num#	k4
a	a	k8xC
Falconu	Falcona	k1gFnSc4
Heavy	Heava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
13	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2010	#num#	k4
proběhl	proběhnout	k5eAaPmAgMnS
v	v	k7c6
rámci	rámec	k1gInSc6
testování	testování	k1gNnSc2
rakety	raketa	k1gFnSc2
na	na	k7c6
startovacím	startovací	k2eAgInSc6d1
komplexu	komplex	k1gInSc6
40	#num#	k4
mysu	mys	k1gInSc6
Canaveral	Canaveral	k1gFnSc2
zkušební	zkušební	k2eAgInSc4d1
zážeh	zážeh	k1gInSc4
všech	všecek	k3xTgInPc2
9	#num#	k4
motorů	motor	k1gInPc2
prvního	první	k4xOgInSc2
stupně	stupeň	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
3,5	3,5	k4
<g/>
s	s	k7c7
test	test	k1gInSc1
proběhl	proběhnout	k5eAaPmAgInS
úspěšně	úspěšně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc2
let	léto	k1gNnPc2
byl	být	k5eAaImAgInS
uskutečněn	uskutečnit	k5eAaPmNgInS
v	v	k7c4
pátek	pátek	k1gInSc4
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2010	#num#	k4
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
po	po	k7c6
prověrce	prověrka	k1gFnSc6
a	a	k8xC
instalaci	instalace	k1gFnSc6
autodestrukčního	autodestrukční	k2eAgInSc2d1
systému	systém	k1gInSc2
nosiče	nosič	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
10	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2015	#num#	k4
byl	být	k5eAaImAgInS
poprvé	poprvé	k6eAd1
během	během	k7c2
běžného	běžný	k2eAgInSc2d1
startu	start	k1gInSc2
otestován	otestován	k2eAgInSc1d1
řízený	řízený	k2eAgInSc1d1
návrat	návrat	k1gInSc1
prvního	první	k4xOgInSc2
stupně	stupeň	k1gInSc2
rakety	raketa	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
byl	být	k5eAaImAgInS
úspěšně	úspěšně	k6eAd1
naveden	navést	k5eAaPmNgInS
na	na	k7c4
přistávací	přistávací	k2eAgFnSc4d1
plošinu	plošina	k1gFnSc4
v	v	k7c6
oceánu	oceán	k1gInSc6
<g/>
,	,	kIx,
během	během	k7c2
tvrdého	tvrdý	k2eAgInSc2d1
dopadu	dopad	k1gInSc2
byl	být	k5eAaImAgInS
však	však	k9
zničen	zničit	k5eAaPmNgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Při	při	k7c6
druhém	druhý	k4xOgInSc6
pokusu	pokus	k1gInSc6
o	o	k7c4
návrat	návrat	k1gInSc4
v	v	k7c6
dubnu	duben	k1gInSc6
2015	#num#	k4
se	se	k3xPyFc4
raketa	raketa	k1gFnSc1
po	po	k7c6
dosednutí	dosednutí	k1gNnSc6
na	na	k7c4
přistávací	přistávací	k2eAgFnSc4d1
plošinu	plošina	k1gFnSc4
převrátila	převrátit	k5eAaPmAgFnS
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
devatenáctém	devatenáctý	k4xOgInSc6
letu	let	k1gInSc6
došlo	dojít	k5eAaPmAgNnS
dvě	dva	k4xCgFnPc4
a	a	k8xC
půl	půl	k1xP
minuty	minuta	k1gFnSc2
po	po	k7c6
startu	start	k1gInSc6
k	k	k7c3
explozi	exploze	k1gFnSc3
a	a	k8xC
ztrátě	ztráta	k1gFnSc3
rakety	raketa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
předběžných	předběžný	k2eAgInPc2d1
závěrů	závěr	k1gInPc2
vyšetřování	vyšetřování	k1gNnSc2
(	(	kIx(
<g/>
z	z	k7c2
konce	konec	k1gInSc2
července	červenec	k1gInSc2
2015	#num#	k4
<g/>
)	)	kIx)
havárii	havárie	k1gFnSc4
způsobila	způsobit	k5eAaPmAgFnS
prasklá	prasklý	k2eAgFnSc1d1
vzpěra	vzpěra	k1gFnSc1
<g/>
,	,	kIx,
vinou	vina	k1gFnSc7
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
explozi	exploze	k1gFnSc3
nádrže	nádrž	k1gFnSc2
s	s	k7c7
tekutým	tekutý	k2eAgInSc7d1
kyslíkem	kyslík	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
září	září	k1gNnSc6
2015	#num#	k4
firma	firma	k1gFnSc1
SpaceX	SpaceX	k1gFnSc1
oznámila	oznámit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
obnově	obnova	k1gFnSc3
letů	let	k1gInPc2
dojde	dojít	k5eAaPmIp3nS
„	„	k?
<g/>
nejdříve	dříve	k6eAd3
za	za	k7c4
několik	několik	k4yIc4
měsíců	měsíc	k1gInPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Falcon	Falcon	k1gInSc1
9	#num#	k4
provádí	provádět	k5eAaImIp3nS
experimentální	experimentální	k2eAgNnSc4d1
přistání	přistání	k1gNnSc4
na	na	k7c6
mysu	mys	k1gInSc6
Canaveral	Canaveral	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
22	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2015	#num#	k4
byl	být	k5eAaImAgInS
následně	následně	k6eAd1
s	s	k7c7
vylepšenou	vylepšený	k2eAgFnSc7d1
verzí	verze	k1gFnSc7
rakety	raketa	k1gFnSc2
označovanou	označovaný	k2eAgFnSc7d1
jako	jako	k8xS,k8xC
FT	FT	kA
(	(	kIx(
<g/>
prodloužený	prodloužený	k2eAgInSc4d1
2	#num#	k4
<g/>
.	.	kIx.
stupeň	stupeň	k1gInSc1
<g/>
,	,	kIx,
použití	použití	k1gNnSc1
přechlazeného	přechlazený	k2eAgNnSc2d1
paliva	palivo	k1gNnSc2
<g/>
)	)	kIx)
úspěšně	úspěšně	k6eAd1
demonstrován	demonstrován	k2eAgInSc1d1
návrat	návrat	k1gInSc1
1	#num#	k4
<g/>
.	.	kIx.
stupně	stupeň	k1gInSc2
zpět	zpět	k6eAd1
na	na	k7c4
kosmodrom	kosmodrom	k1gInSc4
i	i	k9
s	s	k7c7
úspěšným	úspěšný	k2eAgNnSc7d1
přistáním	přistání	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
historicky	historicky	k6eAd1
první	první	k4xOgNnSc4
přistání	přistání	k1gNnSc4
prvního	první	k4xOgNnSc2
stupně	stupeň	k1gInPc4
rakety	raketa	k1gFnPc1
navedené	navedený	k2eAgFnPc1d1
na	na	k7c4
orbitální	orbitální	k2eAgInSc4d1
let	let	k1gInSc4
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následný	následný	k2eAgInSc1d1
statický	statický	k2eAgInSc1d1
zážehový	zážehový	k2eAgInSc1d1
test	test	k1gInSc1
<g/>
,	,	kIx,
provedený	provedený	k2eAgInSc1d1
15	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
na	na	k7c6
odpalovací	odpalovací	k2eAgFnSc6d1
rampě	rampa	k1gFnSc6
CC	CC	kA
<g/>
40	#num#	k4
<g/>
,	,	kIx,
prokázal	prokázat	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
raketa	raketa	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
dobrém	dobrý	k2eAgInSc6d1
stavu	stav	k1gInSc6
<g/>
,	,	kIx,
až	až	k9
na	na	k7c4
jeden	jeden	k4xCgInSc4
z	z	k7c2
vnějších	vnější	k2eAgInPc2d1
motorů	motor	k1gInPc2
Merlin	Merlin	k1gInSc1
1	#num#	k4
<g/>
D	D	kA
<g/>
+	+	kIx~
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
vykazoval	vykazovat	k5eAaImAgInS
tahové	tahový	k2eAgFnPc4d1
fluktuace	fluktuace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Musk	Musk	k1gInSc1
to	ten	k3xDgNnSc4
na	na	k7c6
svém	svůj	k3xOyFgInSc6
twitterovém	twitterový	k2eAgInSc6d1
účtu	účet	k1gInSc6
následně	následně	k6eAd1
připsal	připsat	k5eAaPmAgMnS
náhodné	náhodný	k2eAgFnSc3d1
ingesci	ingesce	k1gFnSc3
trosek	troska	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Stupeň	stupeň	k1gInSc1
byl	být	k5eAaImAgInS
následně	následně	k6eAd1
uskladněn	uskladnit	k5eAaPmNgInS
v	v	k7c6
integračním	integrační	k2eAgInSc6d1
hangáru	hangár	k1gInSc6
na	na	k7c6
rampě	rampa	k1gFnSc6
39A	39A	k4
v	v	k7c4
Kennedyho	Kennedy	k1gMnSc4
vesmírném	vesmírný	k2eAgNnSc6d1
středisku	středisko	k1gNnSc6
před	před	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
než	než	k8xS
byl	být	k5eAaImAgInS
v	v	k7c6
červnu	červen	k1gInSc6
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
převezen	převezen	k2eAgInSc4d1
do	do	k7c2
Kalifornie	Kalifornie	k1gFnSc2
k	k	k7c3
vystavení	vystavení	k1gNnSc3
před	před	k7c7
centrálou	centrála	k1gFnSc7
SpaceX	SpaceX	k1gFnSc2
v	v	k7c6
tamějším	tamější	k2eAgInSc6d1
Hawthorne	Hawthorn	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
Navazující	navazující	k2eAgFnSc1d1
mise	mise	k1gFnSc1
Jason-	Jason-	k1gFnSc1
<g/>
3	#num#	k4
ze	z	k7c2
dne	den	k1gInSc2
17	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
úspěch	úspěch	k1gInSc1
letu	let	k1gInSc2
ORBCOMM-2	ORBCOMM-2	k1gFnSc4
s	s	k7c7
přistáním	přistání	k1gNnSc7
na	na	k7c6
zemi	zem	k1gFnSc6
nezopakovala	zopakovat	k5eNaPmAgFnS
<g/>
,	,	kIx,
protože	protože	k8xS
satelit	satelit	k1gMnSc1
mířil	mířit	k5eAaImAgMnS
na	na	k7c4
vysoce	vysoce	k6eAd1
prográdní	prográdní	k2eAgFnSc4d1
orbitální	orbitální	k2eAgFnSc4d1
dráhu	dráha	k1gFnSc4
a	a	k8xC
tudíž	tudíž	k8xC
startoval	startovat	k5eAaBmAgMnS
z	z	k7c2
k	k	k7c3
tomu	ten	k3xDgNnSc3
lépe	dobře	k6eAd2
uzpůsobené	uzpůsobený	k2eAgInPc1d1
Vandenberg	Vandenberg	k1gInSc1
AFB	AFB	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
SpaceX	SpaceX	k1gFnPc2
zde	zde	k6eAd1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
neměla	mít	k5eNaImAgFnS
infrastrukturu	infrastruktura	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
by	by	kYmCp3nS
takový	takový	k3xDgInSc4
manévr	manévr	k1gInSc4
mohla	moct	k5eAaImAgFnS
podpořit	podpořit	k5eAaPmF
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
jí	on	k3xPp3gFnSc3
stále	stále	k6eAd1
nebylo	být	k5eNaImAgNnS
uděleno	udělit	k5eAaPmNgNnS
povolení	povolení	k1gNnSc4
od	od	k7c2
Federální	federální	k2eAgFnSc2d1
letecké	letecký	k2eAgFnSc2d1
správy	správa	k1gFnSc2
(	(	kIx(
<g/>
FAA	FAA	kA
<g/>
)	)	kIx)
pro	pro	k7c4
přelet	přelet	k1gInSc4
nad	nad	k7c7
blízkou	blízký	k2eAgFnSc7d1
přírodní	přírodní	k2eAgFnSc7d1
rezervací	rezervace	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
schraňuje	schraňovat	k5eAaImIp3nS
i	i	k9
několik	několik	k4yIc4
ohrožených	ohrožený	k2eAgInPc2d1
druhů	druh	k1gInPc2
<g/>
;	;	kIx,
pokusili	pokusit	k5eAaPmAgMnP
se	se	k3xPyFc4
proto	proto	k8xC
o	o	k7c4
přistání	přistání	k1gNnSc4
na	na	k7c4
již	již	k6eAd1
třetí	třetí	k4xOgFnSc6
autonomní	autonomní	k2eAgFnSc6d1
plošině	plošina	k1gFnSc6
<g/>
,	,	kIx,
pojmenované	pojmenovaný	k2eAgInPc4d1
po	po	k7c6
své	svůj	k3xOyFgFnSc6
vysloužilé	vysloužilý	k2eAgFnSc6d1
předchůdkyni	předchůdkyně	k1gFnSc6
Just	just	k6eAd1
Read	Read	k1gInSc4
the	the	k?
Instructions	Instructions	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
Tichém	tichý	k2eAgInSc6d1
oceáně	oceán	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Raketa	raketa	k1gFnSc1
na	na	k7c4
plošinu	plošina	k1gFnSc4
dosedla	dosednout	k5eAaPmAgFnS
<g/>
,	,	kIx,
ale	ale	k8xC
ocelový	ocelový	k2eAgInSc4d1
zámek	zámek	k1gInSc4
jedné	jeden	k4xCgFnSc2
z	z	k7c2
přistávacích	přistávací	k2eAgFnPc2d1
noh	noha	k1gFnPc2
se	se	k3xPyFc4
nedomkl	domknout	k5eNaPmAgMnS
a	a	k8xC
raketa	raketa	k1gFnSc1
se	se	k3xPyFc4
tudíž	tudíž	k8xC
převrátila	převrátit	k5eAaPmAgFnS
a	a	k8xC
explodovala	explodovat	k5eAaBmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Muska	Musko	k1gNnSc2
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
způsobeno	způsobit	k5eAaPmNgNnS
hustou	hustý	k2eAgFnSc7d1
ranní	ranní	k2eAgFnSc7d1
mlhou	mlha	k1gFnSc7
na	na	k7c6
odpalovací	odpalovací	k2eAgFnSc6d1
rampě	rampa	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
kondenzovala	kondenzovat	k5eAaImAgFnS
a	a	k8xC
následně	následně	k6eAd1
mrzla	mrznout	k5eAaImAgFnS
na	na	k7c6
raketě	raketa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velké	velký	k2eAgFnSc6d1
části	část	k1gFnSc6
trupu	trup	k1gInSc2
nicméně	nicméně	k8xC
explozi	exploze	k1gFnSc4
přežily	přežít	k5eAaPmAgFnP
<g/>
,	,	kIx,
a	a	k8xC
byly	být	k5eAaImAgFnP
úspěšně	úspěšně	k6eAd1
dopraveny	dopravit	k5eAaPmNgFnP
do	do	k7c2
losangeleského	losangeleský	k2eAgInSc2d1
přístavu	přístav	k1gInSc2
k	k	k7c3
dalšímu	další	k2eAgNnSc3d1
ohledání	ohledání	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgInSc1
stupeň	stupeň	k1gInSc1
úspěšně	úspěšně	k6eAd1
dopravil	dopravit	k5eAaPmAgInS
satelit	satelit	k1gInSc1
Jason-	Jason-	k1gFnSc2
<g/>
3	#num#	k4
na	na	k7c4
žádanou	žádaný	k2eAgFnSc4d1
orbitu	orbita	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
průběhu	průběh	k1gInSc6
demonstrujíc	demonstrovat	k5eAaImSgFnS
svoji	svůj	k3xOyFgFnSc4
schopnost	schopnost	k1gFnSc4
několikrát	několikrát	k6eAd1
zažehnout	zažehnout	k5eAaPmF
motor	motor	k1gInSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
za	za	k7c4
kruciální	kruciální	k2eAgNnSc4d1
k	k	k7c3
dopravě	doprava	k1gFnSc3
těžších	těžký	k2eAgNnPc2d2
zatížení	zatížení	k1gNnPc2
na	na	k7c4
orbitu	orbita	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Gwynne	Gwynnout	k5eAaPmIp3nS,k5eAaImIp3nS
Shotwell	Shotwell	k1gInSc1
<g/>
,	,	kIx,
ředitelka	ředitelka	k1gFnSc1
SpaceX	SpaceX	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c6
konferenci	konference	k1gFnSc6
FAA	FAA	kA
CST	CST	kA
4	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2016	#num#	k4
zmínila	zmínit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
přistávací	přistávací	k2eAgFnPc4d1
nohy	noha	k1gFnPc4
konfigurace	konfigurace	k1gFnSc2
FT	FT	kA
byly	být	k5eAaImAgInP
podstatně	podstatně	k6eAd1
vylepšeny	vylepšen	k2eAgInPc1d1
(	(	kIx(
<g/>
Jason-	Jason-	k1gFnSc1
<g/>
3	#num#	k4
byl	být	k5eAaImAgInS
posledním	poslední	k2eAgInSc7d1
letem	let	k1gInSc7
předchozí	předchozí	k2eAgFnSc2d1
konfigurace	konfigurace	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
<g/>
1.1	1.1	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Falcon	Falcon	k1gInSc1
9	#num#	k4
při	při	k7c6
prvním	první	k4xOgInSc6
úspěšném	úspěšný	k2eAgInSc6d1
pokusu	pokus	k1gInSc6
o	o	k7c4
přistání	přistání	k1gNnSc4
na	na	k7c6
autonomní	autonomní	k2eAgFnSc6d1
oceánské	oceánský	k2eAgFnSc6d1
přistávací	přistávací	k2eAgFnSc6d1
plošině	plošina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Následující	následující	k2eAgFnSc7d1
misí	mise	k1gFnSc7
bylo	být	k5eAaImAgNnS
úspěšné	úspěšný	k2eAgNnSc1d1
vynesení	vynesení	k1gNnSc1
lucemburského	lucemburský	k2eAgInSc2d1
satelitu	satelit	k1gInSc2
SES-	SES-	k1gFnSc2
<g/>
9	#num#	k4
<g/>
,	,	kIx,
mířícího	mířící	k2eAgMnSc2d1
na	na	k7c4
geostacionární	geostacionární	k2eAgFnSc4d1
oběžnou	oběžný	k2eAgFnSc4d1
dráhu	dráha	k1gFnSc4
(	(	kIx(
<g/>
GEO	GEO	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
svojí	svůj	k3xOyFgFnSc7
váhou	váha	k1gFnSc7
<g/>
,	,	kIx,
přesahující	přesahující	k2eAgFnSc4d1
5	#num#	k4
330	#num#	k4
kg	kg	kA
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
zatím	zatím	k6eAd1
nejtěžší	těžký	k2eAgInSc4d3
satelit	satelit	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
raketa	raketa	k1gFnSc1
Falcon	Falcon	k1gNnSc1
9	#num#	k4
vynesla	vynést	k5eAaPmAgFnS
na	na	k7c4
přechodovou	přechodový	k2eAgFnSc4d1
dráhu	dráha	k1gFnSc4
ke	k	k7c3
GEO	GEO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Misi	mise	k1gFnSc6
samotné	samotný	k2eAgFnSc6d1
ale	ale	k8xC
předcházelo	předcházet	k5eAaImAgNnS
mnoho	mnoho	k4c1
neočekávaných	očekávaný	k2eNgFnPc2d1
změn	změna	k1gFnPc2
<g/>
,	,	kIx,
počínaje	počínaje	k7c7
vyžádáním	vyžádání	k1gNnSc7
změny	změna	k1gFnSc2
v	v	k7c6
letovém	letový	k2eAgInSc6d1
profilu	profil	k1gInSc6
od	od	k7c2
společnosti	společnost	k1gFnSc2
SES	SES	kA
S.	S.	kA
<g/>
A.	A.	kA
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
satelit	satelit	k1gInSc1
dosáhl	dosáhnout	k5eAaPmAgInS
vyššího	vysoký	k2eAgNnSc2d2
apogea	apogeum	k1gNnSc2
a	a	k8xC
zkrátil	zkrátit	k5eAaPmAgMnS
se	se	k3xPyFc4
tak	tak	k9
čas	čas	k1gInSc1
nutný	nutný	k2eAgInSc1d1
pro	pro	k7c4
dodatečné	dodatečný	k2eAgNnSc4d1
manévrování	manévrování	k1gNnSc4
satelitu	satelit	k1gInSc2
svépomocí	svépomoc	k1gFnSc7
(	(	kIx(
<g/>
úspora	úspora	k1gFnSc1
byla	být	k5eAaImAgFnS
následně	následně	k6eAd1
vyčíslena	vyčíslit	k5eAaPmNgFnS
na	na	k7c4
45	#num#	k4
dní	den	k1gInPc2
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
a	a	k8xC
konče	konče	k7c7
čtyřmi	čtyři	k4xCgInPc7
neúspěšnými	úspěšný	k2eNgInPc7d1
pokusy	pokus	k1gInPc7
o	o	k7c4
start	start	k1gInSc4
-	-	kIx~
první	první	k4xOgInSc1
byl	být	k5eAaImAgInS
zrušen	zrušit	k5eAaPmNgInS
půl	půl	k1xP
hodiny	hodina	k1gFnSc2
předem	předem	k6eAd1
a	a	k8xC
to	ten	k3xDgNnSc1
kvůli	kvůli	k7c3
nepříznivému	příznivý	k2eNgNnSc3d1
počasí	počasí	k1gNnSc3
nad	nad	k7c7
mysem	mys	k1gInSc7
Canaveral	Canaveral	k1gMnSc1
<g/>
,	,	kIx,
druhý	druhý	k4xOgMnSc1
minutu	minuta	k1gFnSc4
a	a	k8xC
tři	tři	k4xCgFnPc4
čtvrtě	čtvrt	k1gFnPc4
před	před	k7c7
startem	start	k1gInSc7
kvůli	kvůli	k7c3
pomalému	pomalý	k2eAgNnSc3d1
tankování	tankování	k1gNnSc3
tekutého	tekutý	k2eAgInSc2d1
kyslíku	kyslík	k1gInSc2
<g/>
,	,	kIx,
třetí	třetí	k4xOgMnSc1
skončil	skončit	k5eAaPmAgInS
přerušením	přerušení	k1gNnSc7
při	při	k7c6
zážehu	zážeh	k1gInSc6
kvůli	kvůli	k7c3
nízkému	nízký	k2eAgInSc3d1
tahu	tah	k1gInSc3
způsobenému	způsobený	k2eAgInSc3d1
ohřátím	ohřátit	k5eAaPmIp1nS
tekutého	tekutý	k2eAgInSc2d1
kyslíku	kyslík	k1gInSc2
během	během	k7c2
čekání	čekání	k1gNnSc2
na	na	k7c4
vyklizení	vyklizení	k1gNnSc4
příbřežní	příbřežní	k2eAgFnSc2d1
hazardní	hazardní	k2eAgFnSc2d1
zóny	zóna	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
čtvrtý	čtvrtý	k4xOgMnSc1
byl	být	k5eAaImAgMnS
odvolán	odvolán	k2eAgMnSc1d1
několik	několik	k4yIc4
hodin	hodina	k1gFnPc2
před	před	k7c7
startem	start	k1gInSc7
kvůli	kvůli	k7c3
horním	horní	k2eAgInPc3d1
větrům	vítr	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Raketa	raketa	k1gFnSc1
Falcon	Falcon	k1gInSc1
9	#num#	k4
se	se	k3xPyFc4
vznesla	vznést	k5eAaPmAgFnS
k	k	k7c3
oblohám	obloha	k1gFnPc3
až	až	k9
při	při	k7c6
pátém	pátý	k4xOgInSc6
pokusu	pokus	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
4	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2016	#num#	k4
<g/>
,	,	kIx,
okolo	okolo	k7c2
23	#num#	k4
<g/>
:	:	kIx,
<g/>
35	#num#	k4
GMT	GMT	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
vzletu	vzlet	k1gInSc3
prostému	prostý	k2eAgInSc3d1
všech	všecek	k3xTgFnPc2
anomálií	anomálie	k1gFnPc2
<g/>
,	,	kIx,
druhý	druhý	k4xOgInSc1
stupeň	stupeň	k1gInSc1
se	se	k3xPyFc4
oddělil	oddělit	k5eAaPmAgInS
přibližně	přibližně	k6eAd1
dvě	dva	k4xCgFnPc1
minuty	minuta	k1gFnPc1
čtyřicet	čtyřicet	k4xCc1
osm	osm	k4xCc1
sekund	sekunda	k1gFnPc2
po	po	k7c6
startu	start	k1gInSc6
a	a	k8xC
pokračoval	pokračovat	k5eAaImAgInS
ke	k	k7c3
své	svůj	k3xOyFgFnSc3
koncové	koncový	k2eAgFnSc3d1
destinaci	destinace	k1gFnSc3
na	na	k7c6
přechodové	přechodový	k2eAgFnSc6d1
dráze	dráha	k1gFnSc6
ke	k	k7c3
GEO	GEO	kA
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
první	první	k4xOgInSc4
stupeň	stupeň	k1gInSc4
po	po	k7c6
balistické	balistický	k2eAgFnSc6d1
křivce	křivka	k1gFnSc6
zamířil	zamířit	k5eAaPmAgMnS
k	k	k7c3
přistávací	přistávací	k2eAgFnSc3d1
plošině	plošina	k1gFnSc3
Of	Of	k1gFnSc2
Course	Course	k1gFnSc2
I	i	k9
Still	Still	k1gMnSc1
Love	lov	k1gInSc5
You	You	k1gMnSc6
<g/>
,	,	kIx,
umístěné	umístěný	k2eAgFnSc2d1
632	#num#	k4
kilometrů	kilometr	k1gInPc2
od	od	k7c2
floridského	floridský	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
po	po	k7c6
osmé	osmý	k4xOgFnSc6
minutě	minuta	k1gFnSc6
první	první	k4xOgInSc1
stupeň	stupeň	k1gInSc1
inicioval	iniciovat	k5eAaBmAgInS
svůj	svůj	k3xOyFgInSc4
přistávací	přistávací	k2eAgInSc4d1
zážeh	zážeh	k1gInSc4
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
excesivní	excesivní	k2eAgFnSc3d1
rychlosti	rychlost	k1gFnSc3
výjimečně	výjimečně	k6eAd1
prováděný	prováděný	k2eAgInSc1d1
o	o	k7c6
třech	tři	k4xCgInPc6
motorech	motor	k1gInPc6
místo	místo	k7c2
jednoho	jeden	k4xCgMnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
přesto	přesto	k8xC
bylo	být	k5eAaImAgNnS
přistání	přistání	k1gNnSc4
na	na	k7c6
plošině	plošina	k1gFnSc6
tvrdé	tvrdá	k1gFnSc2
a	a	k8xC
nezdařilo	zdařit	k5eNaPmAgNnS
se	se	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plošina	plošina	k1gFnSc1
samotná	samotný	k2eAgFnSc1d1
utrpěla	utrpět	k5eAaPmAgFnS
dílčí	dílčí	k2eAgNnSc4d1
poškození	poškození	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
SpaceX	SpaceX	k1gFnPc2
už	už	k6eAd1
předem	předem	k6eAd1
prohlásilo	prohlásit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
kvůli	kvůli	k7c3
extrémnímu	extrémní	k2eAgInSc3d1
letovému	letový	k2eAgInSc3d1
profilu	profil	k1gInSc3
s	s	k7c7
úspěšným	úspěšný	k2eAgNnSc7d1
přistáním	přistání	k1gNnSc7
nepočítá	počítat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Prvním	první	k4xOgInSc7
letem	let	k1gInSc7
konfigurace	konfigurace	k1gFnSc2
FT	FT	kA
byl	být	k5eAaImAgInS
22	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2015	#num#	k4
start	start	k1gInSc4
mise	mise	k1gFnSc2
Orbcomm	Orbcomm	k1gInSc4
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc1
stupeň	stupeň	k1gInSc1
úspěšně	úspěšně	k6eAd1
přistál	přistát	k5eAaPmAgInS,k5eAaImAgInS
na	na	k7c6
oceánské	oceánský	k2eAgFnSc6d1
plošině	plošina	k1gFnSc6
Of	Of	k1gFnSc2
Course	Course	k1gFnSc2
I	i	k9
Still	Still	k1gMnSc1
Love	lov	k1gInSc5
You	You	k1gMnPc3
(	(	kIx(
<g/>
ta	ten	k3xDgFnSc1
samá	samý	k3xTgFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
použita	použít	k5eAaPmNgFnS
při	při	k7c6
vynesení	vynesení	k1gNnSc6
satelitu	satelit	k1gInSc2
SES-	SES-	k1gFnSc2
<g/>
9	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
SpaceX	SpaceX	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Falcon	Falcon	k1gNnSc1
9	#num#	k4
overview	overview	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
SpaceX	SpaceX	k1gFnPc2
<g/>
,	,	kIx,
2012	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
originálu	originál	k1gInSc2
archivováno	archivovat	k5eAaBmNgNnS
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
SpaceX	SpaceX	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Falcon	Falcon	k1gInSc1
9	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
SpaceX	SpaceX	k1gFnPc2
<g/>
,	,	kIx,
2013	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
SpaceX	SpaceX	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Falcon	Falcon	k1gNnSc1
9	#num#	k4
overview	overview	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
SpaceX	SpaceX	k1gFnPc2
<g/>
,	,	kIx,
2012	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
originálu	originál	k1gInSc2
archivováno	archivovat	k5eAaBmNgNnS
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SpaceX	SpaceX	k1gFnSc2
Breaks	Breaksa	k1gFnPc2
Ground	Ground	k1gMnSc1
on	on	k3xPp3gMnSc1
Texas	Texas	k1gInSc1
Spaceport	Spaceport	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Space	Space	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SpaceX	SpaceX	k1gFnSc3
kosmodrom	kosmodrom	k1gInSc4
v	v	k7c6
jižním	jižní	k2eAgInSc6d1
Texasu	Texas	k1gInSc6
|	|	kIx~
forum	forum	k1gNnSc1
<g/>
.	.	kIx.
<g/>
kosmonautix	kosmonautix	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
forum	forum	k1gNnSc1
<g/>
.	.	kIx.
<g/>
kosmonautix	kosmonautix	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SHANKLIN	SHANKLIN	kA
<g/>
,	,	kIx,
Emily	Emil	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
SES-10	SES-10	k1gFnSc1
MISSION	MISSION	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
SpaceX	SpaceX	k1gFnSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Jeff	Jeff	k1gMnSc1
Foust	Foust	k1gFnSc4
on	on	k3xPp3gMnSc1
Twitter	Twitter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Twitter	Twittra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SpaceX	SpaceX	k1gFnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
final	final	k1gMnSc1
Falcon	Falcon	k1gMnSc1
9	#num#	k4
design	design	k1gInSc1
coming	coming	k1gInSc4
this	this	k1gInSc1
year	year	k1gInSc1
<g/>
,	,	kIx,
two	two	k?
Falcon	Falcon	k1gMnSc1
Heavy	Heava	k1gFnSc2
launches	launches	k1gMnSc1
next	next	k1gMnSc1
year	year	k1gMnSc1
-	-	kIx~
SpaceNews	SpaceNews	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
SpaceNews	SpaceNews	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
JCSat-	JCSat-	k1gFnSc7
<g/>
14	#num#	k4
|	|	kIx~
forum	forum	k1gNnSc1
<g/>
.	.	kIx.
<g/>
kosmonautix	kosmonautix	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
forum	forum	k1gNnSc1
<g/>
.	.	kIx.
<g/>
kosmonautix	kosmonautix	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zkušební	zkušební	k2eAgInSc4d1
start	start	k1gInSc4
soukromé	soukromý	k2eAgFnSc2d1
rakety	raketa	k1gFnSc2
Falcon	Falcon	k1gInSc1
9	#num#	k4
byl	být	k5eAaImAgInS
úspěšný	úspěšný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Redakce	redakce	k1gFnSc1
BBC	BBC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-06-04	2010-06-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
TOUFAR	Toufar	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příliš	příliš	k6eAd1
tvrdé	tvrdý	k2eAgNnSc4d1
přistání	přistání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Převratná	převratný	k2eAgFnSc1d1
raketa	raketa	k1gFnSc1
při	při	k7c6
ostrém	ostrý	k2eAgInSc6d1
testu	test	k1gInSc6
neuspěla	uspět	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technet	Technet	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-01-10	2014-01-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
VŠETEČKA	Všetečka	k1gMnSc1
<g/>
,	,	kIx,
Roman	Roman	k1gMnSc1
<g/>
;	;	kIx,
LÁZŇOVSKÝ	LÁZŇOVSKÝ	kA
<g/>
,	,	kIx,
Matouš	Matouš	k1gMnSc1
<g/>
;	;	kIx,
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
třetice	třetice	k1gFnSc2
všeho	všecek	k3xTgNnSc2
zlého	zlé	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Raketa	raketa	k1gFnSc1
přistála	přistát	k5eAaPmAgFnS,k5eAaImAgFnS
přesně	přesně	k6eAd1
<g/>
,	,	kIx,
pak	pak	k6eAd1
se	se	k3xPyFc4
převrátila	převrátit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technet	Technet	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-04-15	2015-04-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zkázu	zkáza	k1gFnSc4
vesmírné	vesmírný	k2eAgFnSc2d1
lodi	loď	k1gFnSc2
Dragon	Dragon	k1gMnSc1
způsobila	způsobit	k5eAaPmAgFnS
asi	asi	k9
prasklá	prasklý	k2eAgFnSc1d1
vzpěra	vzpěra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-07-21	2015-07-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc1d1
start	start	k1gInSc1
soukromé	soukromý	k2eAgFnSc2d1
rakety	raketa	k1gFnSc2
Falcon	Falcon	k1gInSc1
9	#num#	k4
se	se	k3xPyFc4
odkládá	odkládat	k5eAaImIp3nS
na	na	k7c4
neurčito	neurčito	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-09-01	2015-09-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SpaceX	SpaceX	k1gMnSc1
rocket	rocket	k1gMnSc1
in	in	k?
historic	historic	k1gMnSc1
upright	upright	k1gMnSc1
landing	landing	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-12-22	2015-12-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Ken	Ken	k1gMnSc1
Kremer	Kremer	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
SpaceX	SpaceX	k1gMnSc1
Test	test	k1gMnSc1
Fires	Fires	k1gMnSc1
Recovered	Recovered	k1gMnSc1
Falcon	Falcon	k1gInSc4
9	#num#	k4
Booster	Booster	k1gMnSc1
in	in	k?
Major	major	k1gMnSc1
Step	step	k1gFnSc4
To	to	k9
Reusable	Reusable	k1gFnSc4
Rockets	Rocketsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Universe	Universe	k1gFnSc1
Today	Todaa	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-01-16	2016-01-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Chris	Chris	k1gFnSc2
Gebhardt	Gebhardta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
SpaceX	SpaceX	k1gFnSc1
Falcon	Falcon	k1gNnSc1
9	#num#	k4
v	v	k7c4
<g/>
1.1	1.1	k4
conducts	conductsa	k1gFnPc2
static	statice	k1gFnPc2
fire	fir	k1gFnSc2
test	test	k1gInSc1
ahead	ahead	k1gInSc1
of	of	k?
Jason-	Jason-	k1gFnSc2
<g/>
3	#num#	k4
mission	mission	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASASpaceflight	NASASpaceflight	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-01-08	2016-01-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
2016	#num#	k4
FAA	FAA	kA
Commercial	Commercial	k1gMnSc1
Space	Space	k1gMnSc1
Transportation	Transportation	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
YouTube	YouTub	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-02-03	2016-02-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Falcon	Falcon	k1gInSc1
1	#num#	k4
</s>
<s>
Falcon	Falcon	k1gInSc1
9	#num#	k4
v	v	k7c4
<g/>
1.0	1.0	k4
</s>
<s>
Falcon	Falcon	k1gInSc1
9	#num#	k4
v	v	k7c4
<g/>
1.1	1.1	k4
</s>
<s>
Falcon	Falcon	k1gInSc1
9	#num#	k4
FT	FT	kA
</s>
<s>
Falcon	Falcon	k1gInSc1
Heavy	Heava	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Falcon	Falcon	k1gInSc1
9	#num#	k4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Téma	téma	k1gFnSc1
Falcon	Falcon	k1gInSc1
9	#num#	k4
na	na	k7c6
fóru	fórum	k1gNnSc6
Kosmonauix	Kosmonauix	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Nosné	nosný	k2eAgFnPc1d1
rakety	raketa	k1gFnPc1
USA	USA	kA
aktivní	aktivní	k2eAgInPc1d1
</s>
<s>
AntaresAtlas	AntaresAtlas	k1gMnSc1
VDelta	VDelta	k1gMnSc1
(	(	kIx(
<g/>
IV	IV	kA
<g/>
)	)	kIx)
<g/>
Falcon	Falcon	k1gInSc4
9	#num#	k4
<g/>
Falcon	Falcon	k1gMnSc1
HeavyMinotaur	HeavyMinotaur	k1gMnSc1
(	(	kIx(
<g/>
I	I	kA
<g/>
,	,	kIx,
IV	IV	kA
<g/>
,	,	kIx,
VC	VC	kA
<g/>
)	)	kIx)
<g/>
Pegasus	Pegasus	k1gMnSc1
ve	v	k7c6
vývoji	vývoj	k1gInSc6
</s>
<s>
Firefly	Firefnout	k5eAaPmAgFnP
α	α	k?
GlennOmegASLSStarshipVulcan	GlennOmegASLSStarshipVulcan	k1gInSc1
vyřazené	vyřazený	k2eAgInPc1d1
</s>
<s>
Ares	Ares	k1gMnSc1
(	(	kIx(
<g/>
IV	IV	kA
<g/>
)	)	kIx)
<g/>
AthenaAtlas	AthenaAtlas	k1gMnSc1
(	(	kIx(
<g/>
D	D	kA
<g/>
/	/	kIx~
<g/>
E	E	kA
<g/>
/	/	kIx~
<g/>
F	F	kA
<g/>
/	/	kIx~
<g/>
G	G	kA
<g/>
/	/	kIx~
<g/>
HIIIIIILV-	HIIIIIILV-	k1gFnPc2
<g/>
3	#num#	k4
<g/>
BSLV-	BSLV-	k1gFnPc2
<g/>
3	#num#	k4
<g/>
AbleAgenaCentaur	AbleAgenaCentaura	k1gFnPc2
<g/>
)	)	kIx)
<g/>
Delta	delta	k1gFnSc1
(	(	kIx(
<g/>
ABIIIII	ABIIIII	kA
<g/>
))	))	k?
<g/>
Falcon	Falcona	k1gFnPc2
1	#num#	k4
<g/>
Juno	Juno	k1gFnPc2
IJuno	IJuen	k2eAgNnSc4d1
IISaturn	IISaturn	k1gNnSc4
(	(	kIx(
<g/>
IIBV	IIBV	kA
<g/>
)	)	kIx)
<g/>
ScoutSPARKThorTitan	ScoutSPARKThorTitan	k1gInSc1
(	(	kIx(
<g/>
2	#num#	k4
GLV	GLV	kA
<g/>
34	#num#	k4
<g/>
)	)	kIx)
<g/>
Vanguard	Vanguard	k1gInSc1
</s>
<s>
Orbitální	orbitální	k2eAgInPc1d1
nosné	nosný	k2eAgInPc1d1
systémy	systém	k1gInPc1
aktivní	aktivní	k2eAgInPc1d1
</s>
<s>
Angara	Angara	k1gFnSc1
</s>
<s>
1.2	1.2	k4
</s>
<s>
5	#num#	k4
</s>
<s>
Antares	Antares	k1gMnSc1
</s>
<s>
230	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
Ariane	Arianout	k5eAaPmIp3nS
5	#num#	k4
</s>
<s>
Atlas	Atlas	k1gInSc1
V	v	k7c6
</s>
<s>
Delta	delta	k1gFnSc1
</s>
<s>
IV	IV	kA
</s>
<s>
Dlouhý	dlouhý	k2eAgInSc1d1
pochod	pochod	k1gInSc1
</s>
<s>
2C	2C	k4
</s>
<s>
2D	2D	k4
</s>
<s>
2F	2F	k4
</s>
<s>
3A	3A	k4
</s>
<s>
3B	3B	k4
</s>
<s>
3C	3C	k4
</s>
<s>
4B	4B	k4
</s>
<s>
4C	4C	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
Electron	Electron	k1gInSc1
</s>
<s>
Epsilon	epsilon	k1gNnSc1
</s>
<s>
Falcon	Falcon	k1gInSc1
9	#num#	k4
</s>
<s>
FT	FT	kA
</s>
<s>
Falcon	Falcon	k1gInSc1
Heavy	Heava	k1gFnSc2
</s>
<s>
GSLV	GSLV	kA
</s>
<s>
GSLV	GSLV	kA
Mk	Mk	k1gFnSc1
<g/>
.3	.3	k4
</s>
<s>
H-IIA	H-IIA	k?
</s>
<s>
H-IIB	H-IIB	k?
</s>
<s>
Hyperbola-	Hyperbola-	k?
<g/>
1	#num#	k4
</s>
<s>
Jielong	Jielong	k1gInSc1
1	#num#	k4
</s>
<s>
Kuaizhou	Kuaizha	k1gMnSc7
1A	1A	k4
</s>
<s>
Minotaur	Minotaur	k1gMnSc1
</s>
<s>
I	i	k9
</s>
<s>
IV	IV	kA
</s>
<s>
V	v	k7c6
</s>
<s>
C	C	kA
</s>
<s>
Pegasus	Pegasus	k1gMnSc1
XL	XL	kA
</s>
<s>
Proton-M	Proton-M	k?
</s>
<s>
PSLV	PSLV	kA
</s>
<s>
Safír	safír	k1gInSc1
</s>
<s>
Šavit	Šavit	k1gMnSc1
</s>
<s>
Simorgh	Simorgh	k1gMnSc1
</s>
<s>
Strela	Strela	k1gFnSc1
</s>
<s>
Sojuz	Sojuz	k1gInSc1
2	#num#	k4
</s>
<s>
2.1	2.1	k4
<g/>
a	a	k8xC
/	/	kIx~
STA	sto	k4xCgNnPc5
</s>
<s>
2.1	2.1	k4
<g/>
b	b	k?
/	/	kIx~
STB	STB	kA
</s>
<s>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
v	v	k7c6
</s>
<s>
Unha	Unha	k6eAd1
</s>
<s>
Vega	Vega	k6eAd1
</s>
<s>
Zenit	zenit	k1gInSc1
</s>
<s>
3SL	3SL	k4
</s>
<s>
3SLB	3SLB	k4
</s>
<s>
3F	3F	k4
ve	v	k7c6
vývoji	vývoj	k1gInSc6
</s>
<s>
Angara	Angara	k1gFnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
A	a	k9
<g/>
5	#num#	k4
<g/>
PA	Pa	kA
<g/>
5	#num#	k4
<g/>
V	V	kA
<g/>
)	)	kIx)
<g/>
Antares	Antares	k1gMnSc1
(	(	kIx(
<g/>
300	#num#	k4
<g/>
)	)	kIx)
<g/>
Ariane	Arian	k1gMnSc5
6	#num#	k4
<g/>
Ciklon-	Ciklon-	k1gMnSc2
<g/>
4	#num#	k4
<g/>
Dlouhý	dlouhý	k2eAgInSc1d1
pochod	pochod	k1gInSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
B	B	kA
<g/>
89	#num#	k4
<g/>
)	)	kIx)
<g/>
Firefly	Firefly	k1gFnSc1
α	α	k?
<g/>
3	#num#	k4
<g/>
IrtyšLauncherOneMayakNaga-LNaro-	IrtyšLauncherOneMayakNaga-LNaro-	k1gFnSc1
<g/>
2	#num#	k4
<g/>
New	New	k1gMnSc1
GlennOmegARPSSLSSPARKStarshipTronador	GlennOmegARPSSLSSPARKStarshipTronador	k1gMnSc1
IIULVVLMVulcan	IIULVVLMVulcan	k1gMnSc1
vyřazené	vyřazený	k2eAgFnSc2d1
</s>
<s>
Antares	Antares	k1gInSc1
(	(	kIx(
<g/>
100	#num#	k4
<g/>
)	)	kIx)
<g/>
Ariane	Arian	k1gMnSc5
(	(	kIx(
<g/>
1234	#num#	k4
<g/>
)	)	kIx)
<g/>
ASLVAthena	ASLVAthena	k1gFnSc1
(	(	kIx(
<g/>
III	III	kA
<g/>
)	)	kIx)
<g/>
Atlas	Atlas	k1gMnSc1
(	(	kIx(
<g/>
BDE	BDE	kA
<g/>
/	/	kIx~
<g/>
FGHIIIIIILV-	FGHIIIIIILV-	k1gFnPc2
<g/>
3	#num#	k4
<g/>
BSLV-	BSLV-	k1gFnPc2
<g/>
3	#num#	k4
<g/>
AbleAgenaCentaur	AbleAgenaCentaura	k1gFnPc2
<g/>
)	)	kIx)
<g/>
Black	Black	k1gInSc1
ArrowCiklon	ArrowCiklon	k1gInSc1
(	(	kIx(
<g/>
23	#num#	k4
<g/>
)	)	kIx)
<g/>
ConestogaDelta	ConestogaDelt	k1gInSc2
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
ABCDEGJLMN	ABCDEGJLMN	kA
<g/>
0	#num#	k4
<g/>
10010002000300040005000	#num#	k4
<g/>
IIIII	IIIII	kA
<g/>
)	)	kIx)
<g/>
DiamantDlouhý	DiamantDlouhý	k2eAgInSc1d1
pochod	pochod	k1gInSc1
(	(	kIx(
<g/>
11	#num#	k4
<g/>
D	D	kA
<g/>
2	#num#	k4
<g/>
A	a	k9
<g/>
2	#num#	k4
<g/>
E	E	kA
<g/>
34	#num#	k4
<g/>
A	A	kA
<g/>
)	)	kIx)
<g/>
DněprEněrgijaEuropaFalcon	DněprEněrgijaEuropaFalcon	k1gMnSc1
1	#num#	k4
<g/>
Falcon	Falcon	k1gInSc1
9	#num#	k4
(	(	kIx(
<g/>
v	v	k7c4
<g/>
1.0	1.0	k4
<g/>
v	v	k7c4
<g/>
1.1	1.1	k4
<g/>
)	)	kIx)
<g/>
Feng	Feng	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Bao	Bao	k1gFnSc1
1	#num#	k4
<g/>
H-IH-IIJuno	H-IH-IIJuna	k1gFnSc5
IJuno	IJuno	k6eAd1
IIKaituozhe-	IIKaituozhe-	k1gFnSc3
<g/>
1	#num#	k4
<g/>
Kosmos	kosmos	k1gInSc1
(	(	kIx(
<g/>
12	#num#	k4
<g/>
I	i	k9
<g/>
33	#num#	k4
<g/>
M	M	kA
<g/>
)	)	kIx)
<g/>
Lambda	lambda	k1gNnSc4
4	#num#	k4
<g/>
SMu	SMu	k1gFnPc2
(	(	kIx(
<g/>
4	#num#	k4
<g/>
S	s	k7c7
3C	3C	k4
3H	3H	k4
3S	3S	k4
3	#num#	k4
<g/>
SIIV	SIIV	kA
<g/>
)	)	kIx)
<g/>
N-	N-	k1gFnSc1
<g/>
1	#num#	k4
<g/>
N-IN-IINaro-	N-IN-IINaro-	k1gFnSc1
<g/>
1	#num#	k4
<g/>
PaektusanPilotProton	PaektusanPilotProton	k1gInSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
UR-	UR-	k1gFnSc1
<g/>
500	#num#	k4
<g/>
K	k	k7c3
<g/>
)	)	kIx)
<g/>
R-	R-	k1gMnSc1
<g/>
7	#num#	k4
(	(	kIx(
<g/>
LunaMolnijaPoljotSojuzoriginálLMUU	LunaMolnijaPoljotSojuzoriginálLMUU	k1gFnPc2
<g/>
2	#num#	k4
<g/>
FGSojuz	FGSojuza	k1gFnPc2
<g/>
/	/	kIx~
<g/>
VostokSputnikVoschodVostokLK	VostokSputnikVoschodVostokLK	k1gFnSc1
<g/>
22	#num#	k4
<g/>
M	M	kA
<g/>
)	)	kIx)
<g/>
R-	R-	k1gFnSc1
<g/>
29	#num#	k4
(	(	kIx(
<g/>
Shtil	Shtila	k1gFnPc2
<g/>
'	'	kIx"
<g/>
Volna	volno	k1gNnSc2
<g/>
)	)	kIx)
<g/>
RokotSaturn	RokotSaturn	k1gNnSc1
(	(	kIx(
<g/>
IIBVINT-	IIBVINT-	k1gFnSc1
<g/>
21	#num#	k4
<g/>
)	)	kIx)
<g/>
ScoutSLVSpace	ScoutSLVSpace	k1gFnSc1
ShuttleSpartaStart-	ShuttleSpartaStart-	k1gFnSc2
<g/>
1	#num#	k4
<g/>
Thor	Thora	k1gFnPc2
(	(	kIx(
<g/>
AbleAblestarAgenaBurneeltaDSV-	AbleAblestarAgenaBurneeltaDSV-	k1gFnSc1
<g/>
2	#num#	k4
<g/>
U	U	kA
<g/>
)	)	kIx)
<g/>
Thorad-AgenaTitan	Thorad-AgenaTitan	k1gInSc1
(	(	kIx(
<g/>
2	#num#	k4
GLVIIIAIIIBIIICIIIDIIIE	GLVIIIAIIIBIIICIIIDIIIE	kA
<g/>
34	#num#	k4
<g/>
D	D	kA
<g/>
23	#num#	k4
<g/>
GCT-	GCT-	k1gFnSc1
<g/>
34	#num#	k4
<g/>
)	)	kIx)
<g/>
VanguardZenit	VanguardZenit	k1gFnSc2
(	(	kIx(
<g/>
22	#num#	k4
<g/>
M	M	kA
<g/>
)	)	kIx)
</s>
<s>
SpaceX	SpaceX	k?
Technika	technika	k1gFnSc1
</s>
<s>
Rakety	raketa	k1gFnPc1
</s>
<s>
činné	činný	k2eAgFnPc1d1
</s>
<s>
Falcon	Falcon	k1gInSc1
9	#num#	k4
</s>
<s>
Block	Block	k6eAd1
5	#num#	k4
</s>
<s>
Falcon	Falcon	k1gInSc1
Heavy	Heava	k1gFnSc2
ve	v	k7c6
vývoji	vývoj	k1gInSc6
</s>
<s>
Starship	Starship	k1gInSc1
-	-	kIx~
Super	super	k1gInSc1
Heavy	Heava	k1gFnSc2
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
BFR	BFR	kA
<g/>
)	)	kIx)
vyřazené	vyřazený	k2eAgFnSc2d1
</s>
<s>
Falcon	Falcon	k1gInSc1
1	#num#	k4
</s>
<s>
Falcon	Falcon	k1gInSc1
9	#num#	k4
</s>
<s>
v	v	k7c6
<g/>
1.0	1.0	k4
</s>
<s>
v	v	k7c6
<g/>
1.1	1.1	k4
</s>
<s>
Block	Block	k6eAd1
3	#num#	k4
</s>
<s>
Block	Block	k6eAd1
4	#num#	k4
zrušené	zrušený	k2eAgInPc4d1
</s>
<s>
Falcon	Falcon	k1gInSc1
1	#num#	k4
<g/>
e	e	k0
</s>
<s>
Falcon	Falcon	k1gInSc1
5	#num#	k4
</s>
<s>
Falcon	Falcon	k1gInSc1
9	#num#	k4
Air	Air	k1gFnPc2
</s>
<s>
Meziplanetární	meziplanetární	k2eAgInSc1d1
dopravní	dopravní	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
Testovací	testovací	k2eAgFnSc1d1
technika	technika	k1gFnSc1
</s>
<s>
Grasshopper	Grasshopper	k1gInSc1
(	(	kIx(
<g/>
vyřazeno	vyřazen	k2eAgNnSc1d1
<g/>
)	)	kIx)
</s>
<s>
F9R	F9R	k4
Dev	Dev	k1gFnSc1
<g/>
1	#num#	k4
<g/>
†	†	k?
</s>
<s>
DragonFly	DragonFla	k1gFnPc1
(	(	kIx(
<g/>
vyřazeno	vyřazen	k2eAgNnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Starhopper	Starhopper	k1gInSc4
a	a	k8xC
prototypy	prototyp	k1gInPc4
Starship	Starship	k1gMnSc1
Kosmické	kosmický	k2eAgFnSc2d1
lodě	loď	k1gFnSc2
</s>
<s>
nákladní	nákladní	k2eAgFnSc1d1
</s>
<s>
Dragon	Dragon	k1gMnSc1
</s>
<s>
Starship	Starship	k1gMnSc1
Cargo	Cargo	k1gMnSc1
</s>
<s>
Starship	Starship	k1gInSc1
Tanker	tanker	k1gInSc1
pilotované	pilotovaný	k2eAgFnSc2d1
</s>
<s>
Crew	Crew	k?
Dragon	Dragon	k1gMnSc1
</s>
<s>
Starship	Starship	k1gMnSc1
</s>
<s>
Raketové	raketový	k2eAgInPc1d1
motory	motor	k1gInPc1
</s>
<s>
Merlin	Merlin	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
A	A	kA
<g/>
,	,	kIx,
1	#num#	k4
<g/>
B	B	kA
<g/>
,	,	kIx,
1	#num#	k4
<g/>
C	C	kA
<g/>
,	,	kIx,
1	#num#	k4
<g/>
D	D	kA
<g/>
,	,	kIx,
1	#num#	k4
<g/>
D	D	kA
<g/>
+	+	kIx~
<g/>
,	,	kIx,
vakuum	vakuum	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Kestrel	Kestrel	k1gMnSc1
</s>
<s>
Draco	Draco	k6eAd1
</s>
<s>
Superaco	Superaco	k6eAd1
</s>
<s>
Raptor	Raptor	k1gMnSc1
</s>
<s>
Methalox	Methalox	k1gInSc1
thruster	thrustra	k1gFnPc2
</s>
<s>
Znovupoužitelnost	Znovupoužitelnost	k1gFnSc1
</s>
<s>
První	první	k4xOgInPc1
stupně	stupeň	k1gInPc1
</s>
<s>
Dragony	Dragon	k1gMnPc4
Lety	let	k1gInPc1
</s>
<s>
Přehled	přehled	k1gInSc1
letů	let	k1gInPc2
Falconu	Falcona	k1gFnSc4
1	#num#	k4
</s>
<s>
Seznam	seznam	k1gInSc1
letů	let	k1gInPc2
Falconu	Falcona	k1gFnSc4
9	#num#	k4
a	a	k8xC
Falconu	Falcona	k1gFnSc4
Heavy	Heava	k1gFnSc2
</s>
<s>
Lety	let	k1gInPc1
Dragonů	Dragon	k1gMnPc2
Odpalovací	odpalovací	k2eAgNnSc1d1
zařízení	zařízení	k1gNnSc1
</s>
<s>
Orbitální	orbitální	k2eAgFnSc1d1
</s>
<s>
Cape	capat	k5eAaImIp3nS
Canaveral	Canaveral	k1gFnSc4
AFS	AFS	kA
<g/>
,	,	kIx,
Florida	Florida	k1gFnSc1
</s>
<s>
SLC-40	SLC-40	k4
</s>
<s>
LC-39A	LC-39A	k4
</s>
<s>
Vandenberg	Vandenberg	k1gInSc1
<g/>
,	,	kIx,
Kalifornie	Kalifornie	k1gFnSc1
</s>
<s>
SLC-4E	SLC-4E	k4
</s>
<s>
Kwajalein	Kwajalein	k1gInSc1
<g/>
,	,	kIx,
Marshallovy	Marshallův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
<g/>
†	†	k?
</s>
<s>
Omelek	omelek	k1gInSc1
Island	Island	k1gInSc1
<g/>
†	†	k?
Suborbitální	Suborbitální	k2eAgInSc1d1
</s>
<s>
Brownsville	Brownsville	k1gFnSc1
<g/>
,	,	kIx,
Texas	Texas	k1gInSc1
(	(	kIx(
<g/>
SpaceX	SpaceX	k1gFnSc1
South	South	k1gInSc4
Texas	Texas	k1gInSc1
launch	launch	k1gInSc1
site	site	k1gFnPc2
v	v	k7c4
Boca	Boc	k2eAgNnPc4d1
Chica	Chicum	k1gNnPc4
<g/>
)	)	kIx)
</s>
<s>
McGregor	McGregor	k1gMnSc1
</s>
<s>
Spaceport	Spaceport	k1gInSc1
America	Americ	k1gInSc2
<g/>
†	†	k?
</s>
<s>
Přistávací	přistávací	k2eAgFnPc1d1
plochy	plocha	k1gFnPc1
</s>
<s>
Autonomní	autonomní	k2eAgFnSc1d1
plovoucí	plovoucí	k2eAgFnSc1d1
přistávací	přistávací	k2eAgFnSc1d1
plošina	plošina	k1gFnSc1
</s>
<s>
Přistávací	přistávací	k2eAgFnPc1d1
plochy	plocha	k1gFnPc1
1	#num#	k4
a	a	k8xC
2	#num#	k4
(	(	kIx(
<g/>
Cape	capat	k5eAaImIp3nS
Canaveral	Canaveral	k1gMnSc1
AFS	AFS	kA
<g/>
)	)	kIx)
</s>
<s>
Přistávací	přistávací	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
4	#num#	k4
(	(	kIx(
<g/>
Vandenberg	Vandenberg	k1gInSc4
<g/>
)	)	kIx)
Ostatní	ostatní	k2eAgNnPc4d1
zařízení	zařízení	k1gNnPc4
</s>
<s>
Ústředí	ústředí	k1gNnSc1
a	a	k8xC
výrobní	výrobní	k2eAgInSc1d1
závod	závod	k1gInSc1
(	(	kIx(
<g/>
Hawthorne	Hawthorn	k1gInSc5
<g/>
,	,	kIx,
California	Californium	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Testovací	testovací	k2eAgNnPc1d1
a	a	k8xC
poletová	poletový	k2eAgNnPc1d1
demontážní	demontážní	k2eAgNnPc1d1
zařízení	zařízení	k1gNnPc1
(	(	kIx(
<g/>
McGregor	McGregor	k1gInSc1
<g/>
,	,	kIx,
Texas	Texas	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Zařízení	zařízení	k1gNnSc1
pro	pro	k7c4
vývoj	vývoj	k1gInSc4
satelitů	satelit	k1gInPc2
(	(	kIx(
<g/>
Redmond	Redmond	k1gInSc1
<g/>
,	,	kIx,
Washington	Washington	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Oblastní	oblastní	k2eAgFnPc1d1
kanceláře	kancelář	k1gFnPc1
(	(	kIx(
<g/>
Chantilly	Chantilla	k1gFnPc1
<g/>
,	,	kIx,
Houston	Houston	k1gInSc1
<g/>
,	,	kIx,
Seattle	Seattle	k1gFnSc1
<g/>
,	,	kIx,
Washington	Washington	k1gInSc1
DC	DC	kA
<g/>
)	)	kIx)
Zakázky	zakázka	k1gFnPc1
</s>
<s>
Commercial	Commercial	k1gInSc1
Orbital	orbital	k1gInSc1
Transportation	Transportation	k1gInSc1
Services	Services	k1gInSc1
(	(	kIx(
<g/>
COTS	COTS	kA
<g/>
)	)	kIx)
</s>
<s>
Commercial	Commercial	k1gMnSc1
Resupply	Resupply	k1gMnSc1
Services	Services	k1gMnSc1
(	(	kIx(
<g/>
CRS	CRS	kA
<g/>
)	)	kIx)
</s>
<s>
Commercial	Commercial	k1gMnSc1
Crew	Crew	k1gMnSc1
Development	Development	k1gMnSc1
(	(	kIx(
<g/>
CCDev	CCDev	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Commercial	Commercial	k1gMnSc1
Crew	Crew	k1gMnSc1
integrated	integrated	k1gMnSc1
Capability	Capabilita	k1gFnSc2
(	(	kIx(
<g/>
CCiCap	CCiCap	k1gInSc1
<g/>
)	)	kIx)
Výzkum	výzkum	k1gInSc1
a	a	k8xC
vývoj	vývoj	k1gInSc1
</s>
<s>
Vývoj	vývoj	k1gInSc1
znovupoužitelné	znovupoužitelný	k2eAgFnSc2d1
rakety	raketa	k1gFnSc2
</s>
<s>
Testování	testování	k1gNnPc1
přistávání	přistávání	k1gNnSc3
prvního	první	k4xOgInSc2
stupně	stupeň	k1gInSc2
Falconu	Falcon	k1gInSc2
9	#num#	k4
</s>
<s>
Marsovská	marsovský	k2eAgFnSc1d1
dopravní	dopravní	k2eAgFnSc1d1
infrastruktura	infrastruktura	k1gFnSc1
</s>
<s>
Red	Red	k?
Dragon	Dragon	k1gMnSc1
(	(	kIx(
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
<g/>
)	)	kIx)
</s>
<s>
Starlink	Starlink	k1gInSc4
Lidé	člověk	k1gMnPc1
</s>
<s>
Elon	Elon	k1gMnSc1
Musk	Musk	k1gMnSc1
(	(	kIx(
<g/>
CEO	CEO	kA
<g/>
,	,	kIx,
CTO	CTO	kA
<g/>
)	)	kIx)
</s>
<s>
Gwynne	Gwynnout	k5eAaImIp3nS,k5eAaPmIp3nS
Shotwell	Shotwell	k1gInSc1
(	(	kIx(
<g/>
prezidentka	prezidentka	k1gFnSc1
a	a	k8xC
COO	COO	kA
<g/>
)	)	kIx)
</s>
<s>
Tom	Tom	k1gMnSc1
Mueller	Mueller	k1gMnSc1
(	(	kIx(
<g/>
vedoucí	vedoucí	k1gMnSc1
oddělení	oddělení	k1gNnSc2
pohonu	pohon	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Hans	Hans	k1gMnSc1
Koenigsmann	Koenigsmann	k1gMnSc1
(	(	kIx(
<g/>
vedoucí	vedoucí	k1gMnSc1
oddělení	oddělení	k1gNnSc2
zajištění	zajištění	k1gNnSc2
mise	mise	k1gFnSc2
<g/>
)	)	kIx)
Kurzíva	kurzíva	k1gFnSc1
značí	značit	k5eAaImIp3nS
neletěné	letěný	k2eNgInPc4d1
stroje	stroj	k1gInPc4
a	a	k8xC
budoucí	budoucí	k2eAgInPc4d1
lety	let	k1gInPc4
nebo	nebo	k8xC
nedokončené	dokončený	k2eNgFnPc4d1
základny	základna	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Symbol	symbol	k1gInSc1
†	†	k?
značí	značit	k5eAaImIp3nS
neúspěšné	úspěšný	k2eNgInPc4d1
lety	let	k1gInPc4
<g/>
,	,	kIx,
opuštěné	opuštěný	k2eAgFnPc4d1
lokality	lokalita	k1gFnPc4
a	a	k8xC
zničené	zničený	k2eAgInPc4d1
stroje	stroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Kosmonautika	kosmonautika	k1gFnSc1
|	|	kIx~
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
