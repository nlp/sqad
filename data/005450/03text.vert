<p>
<s>
Objem	objem	k1gInSc1	objem
je	být	k5eAaImIp3nS	být
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
veličina	veličina	k1gFnSc1	veličina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
velikost	velikost	k1gFnSc4	velikost
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
zabírá	zabírat	k5eAaImIp3nS	zabírat
těleso	těleso	k1gNnSc1	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
matematického	matematický	k2eAgNnSc2d1	matematické
hlediska	hledisko	k1gNnSc2	hledisko
představuje	představovat	k5eAaImIp3nS	představovat
objem	objem	k1gInSc4	objem
míru	mír	k1gInSc2	mír
charakterizující	charakterizující	k2eAgFnSc4d1	charakterizující
danou	daný	k2eAgFnSc4d1	daná
vymezenou	vymezený	k2eAgFnSc4d1	vymezená
část	část	k1gFnSc4	část
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Symbol	symbol	k1gInSc1	symbol
veličiny	veličina	k1gFnSc2	veličina
<g/>
:	:	kIx,	:
V	V	kA	V
(	(	kIx(	(
<g/>
angl	angl	k1gMnSc1	angl
<g/>
.	.	kIx.	.
volume	volum	k1gInSc5	volum
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jednotka	jednotka	k1gFnSc1	jednotka
SI	si	k1gNnSc2	si
<g/>
:	:	kIx,	:
metr	metr	k1gInSc4	metr
krychlový	krychlový	k2eAgInSc4d1	krychlový
<g/>
,	,	kIx,	,
značka	značka	k1gFnSc1	značka
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
:	:	kIx,	:
m3	m3	k4	m3
<g/>
,	,	kIx,	,
lidově	lidově	k6eAd1	lidově
zvaný	zvaný	k2eAgInSc1d1	zvaný
kubík	kubík	k1gInSc1	kubík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Rovnice	rovnice	k1gFnSc1	rovnice
pro	pro	k7c4	pro
výpočet	výpočet	k1gInSc4	výpočet
objemu	objem	k1gInSc2	objem
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Jednotky	jednotka	k1gFnSc2	jednotka
objemu	objem	k1gInSc2	objem
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Odvozené	odvozený	k2eAgInPc1d1	odvozený
z	z	k7c2	z
SI	si	k1gNnSc2	si
===	===	k?	===
</s>
</p>
<p>
<s>
decimetr	decimetr	k1gInSc1	decimetr
krychlový	krychlový	k2eAgInSc1d1	krychlový
[	[	kIx(	[
<g/>
dm3	dm3	k4	dm3
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
centimetr	centimetr	k1gInSc4	centimetr
krychlový	krychlový	k2eAgInSc4d1	krychlový
[	[	kIx(	[
<g/>
cm3	cm3	k4	cm3
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
milimetr	milimetr	k1gInSc1	milimetr
krychlový	krychlový	k2eAgInSc1d1	krychlový
[	[	kIx(	[
<g/>
mm3	mm3	k4	mm3
<g/>
]	]	kIx)	]
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
z	z	k7c2	z
evropské	evropský	k2eAgFnSc2d1	Evropská
praxe	praxe	k1gFnSc2	praxe
====	====	k?	====
</s>
</p>
<p>
<s>
hektolitr	hektolitr	k1gInSc1	hektolitr
[	[	kIx(	[
<g/>
hl	hl	k?	hl
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
litr	litr	k1gInSc1	litr
[	[	kIx(	[
<g/>
l	l	kA	l
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
1	[number]	k4	1
l	l	kA	l
=	=	kIx~	=
1	[number]	k4	1
dm3	dm3	k4	dm3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
decilitr	decilitr	k1gInSc1	decilitr
[	[	kIx(	[
<g/>
dl	dl	k?	dl
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
centilitr	centilitr	k1gInSc1	centilitr
[	[	kIx(	[
<g/>
cl	cl	k?	cl
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
mililitr	mililitr	k1gInSc1	mililitr
[	[	kIx(	[
<g/>
ml	ml	kA	ml
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
1	[number]	k4	1
ml	ml	kA	ml
=	=	kIx~	=
1	[number]	k4	1
cm3	cm3	k4	cm3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Převody	převod	k1gInPc1	převod
objemových	objemový	k2eAgFnPc2d1	objemová
jednotek	jednotka	k1gFnPc2	jednotka
podle	podle	k7c2	podle
SI	si	k1gNnSc2	si
====	====	k?	====
</s>
</p>
<p>
<s>
1	[number]	k4	1
m3	m3	k4	m3
=	=	kIx~	=
1 000	[number]	k4	1 000
dm3	dm3	k4	dm3
=	=	kIx~	=
1 000 000	[number]	k4	1 000 000
cm3	cm3	k4	cm3
=	=	kIx~	=
1 000 000 000	[number]	k4	1 000 000 000
mm3	mm3	k4	mm3
</s>
</p>
<p>
<s>
0,01	[number]	k4	0,01
hl	hl	k?	hl
=	=	kIx~	=
1	[number]	k4	1
l	l	kA	l
=	=	kIx~	=
10	[number]	k4	10
dl	dl	k?	dl
=	=	kIx~	=
100	[number]	k4	100
cl	cl	k?	cl
=	=	kIx~	=
1 000	[number]	k4	1 000
ml	ml	kA	ml
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Jednotky	jednotka	k1gFnSc2	jednotka
mimo	mimo	k7c4	mimo
soustavu	soustava	k1gFnSc4	soustava
SI	si	k1gNnSc2	si
===	===	k?	===
</s>
</p>
<p>
<s>
Britské	britský	k2eAgFnPc1d1	britská
(	(	kIx(	(
<g/>
UK	UK	kA	UK
<g/>
,	,	kIx,	,
imperiální	imperiální	k2eAgFnSc1d1	imperiální
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
bušl	bušl	k1gInSc1	bušl
(	(	kIx(	(
<g/>
8	[number]	k4	8
×	×	k?	×
UK	UK	kA	UK
gallon	gallon	k1gInSc1	gallon
≈	≈	k?	≈
36,3687	[number]	k4	36,3687
dm3	dm3	k4	dm3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
gallon	gallon	k1gInSc1	gallon
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
<g/>
54609	[number]	k4	54609
dm3	dm3	k4	dm3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
fluid	fluid	k1gInSc1	fluid
ounce	ounce	k1gFnSc2	ounce
(	(	kIx(	(
<g/>
UK	UK	kA	UK
gallon	gallon	k1gInSc1	gallon
/	/	kIx~	/
160	[number]	k4	160
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
pintaAmerické	pintaAmerický	k2eAgInPc1d1	pintaAmerický
(	(	kIx(	(
<g/>
US	US	kA	US
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
barel	barel	k1gInSc1	barel
</s>
</p>
<p>
<s>
bušl	bušl	k1gInSc1	bušl
(	(	kIx(	(
<g/>
8	[number]	k4	8
×	×	k?	×
"	"	kIx"	"
<g/>
suchý	suchý	k2eAgMnSc1d1	suchý
<g/>
"	"	kIx"	"
US	US	kA	US
gallon	gallon	k1gInSc1	gallon
≈	≈	k?	≈
35,2391	[number]	k4	35,2391
dm3	dm3	k4	dm3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
suchý	suchý	k2eAgInSc4d1	suchý
<g/>
"	"	kIx"	"
gallon	gallon	k1gInSc4	gallon
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
<g/>
40488377086	[number]	k4	40488377086
dm3	dm3	k4	dm3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
tekutý	tekutý	k2eAgInSc1d1	tekutý
<g/>
"	"	kIx"	"
gallon	gallon	k1gInSc1	gallon
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
<g/>
785411784	[number]	k4	785411784
dm3	dm3	k4	dm3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
fluid	fluid	k1gInSc1	fluid
ounce	ounec	k1gInSc2	ounec
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
tekutý	tekutý	k2eAgMnSc1d1	tekutý
<g/>
"	"	kIx"	"
US	US	kA	US
gallon	gallon	k1gInSc1	gallon
/	/	kIx~	/
128	[number]	k4	128
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
liquid	liquid	k1gInSc1	liquid
pintHistorické	pintHistorický	k2eAgFnSc2d1	pintHistorický
</s>
</p>
<p>
<s>
aam	aam	k?	aam
</s>
</p>
<p>
<s>
acre-pulgada	acreulgada	k1gFnSc1	acre-pulgada
</s>
</p>
<p>
<s>
prostice	prostice	k1gFnSc1	prostice
</s>
</p>
<p>
<s>
pipe	pipe	k6eAd1	pipe
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
objemů	objem	k1gInPc2	objem
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
porovnání	porovnání	k1gNnSc4	porovnání
řádové	řádový	k2eAgFnSc2d1	řádová
velikosti	velikost	k1gFnSc2	velikost
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
uveden	uvést	k5eAaPmNgInS	uvést
seznam	seznam	k1gInSc1	seznam
některých	některý	k3yIgInPc2	některý
zajímavých	zajímavý	k2eAgInPc2d1	zajímavý
objemů	objem	k1gInPc2	objem
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
10	[number]	k4	10
litrů	litr	k1gInPc2	litr
je	být	k5eAaImIp3nS	být
rovno	roven	k2eAgNnSc1d1	rovno
</s>
</p>
<p>
<s>
0,01	[number]	k4	0,01
m3	m3	k4	m3
</s>
</p>
<p>
<s>
100	[number]	k4	100
dl	dl	k?	dl
</s>
</p>
<p>
<s>
0,353	[number]	k4	0,353
krychlové	krychlový	k2eAgFnPc4d1	krychlová
stopy	stopa	k1gFnPc4	stopa
</s>
</p>
<p>
<s>
610,237	[number]	k4	610,237
krychlových	krychlový	k2eAgInPc2d1	krychlový
palců	palec	k1gInPc2	palec
</s>
</p>
<p>
<s>
objemu	objem	k1gInSc3	objem
krychle	krychle	k1gFnSc1	krychle
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
hrany	hrana	k1gFnSc2	hrana
21,54	[number]	k4	21,54
cm	cm	kA	cm
</s>
</p>
<p>
<s>
objemu	objem	k1gInSc3	objem
vody	voda	k1gFnSc2	voda
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
10	[number]	k4	10
kg	kg	kA	kg
</s>
</p>
<p>
<s>
28,316846592	[number]	k4	28,316846592
l	l	kA	l
–	–	k?	–
1	[number]	k4	1
krychlová	krychlový	k2eAgFnSc1d1	krychlová
stopa	stopa	k1gFnSc1	stopa
</s>
</p>
<p>
<s>
55	[number]	k4	55
l	l	kA	l
–	–	k?	–
objem	objem	k1gInSc1	objem
palivové	palivový	k2eAgFnSc2d1	palivová
nádrže	nádrž	k1gFnSc2	nádrž
u	u	k7c2	u
vozu	vůz	k1gInSc2	vůz
Škoda	škoda	k1gFnSc1	škoda
Octavia	octavia	k1gFnSc1	octavia
</s>
</p>
<p>
<s>
100	[number]	k4	100
l	l	kA	l
=	=	kIx~	=
0,1	[number]	k4	0,1
m3	m3	k4	m3
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
<g/>
2	[number]	k4	2
l	l	kA	l
–	–	k?	–
typický	typický	k2eAgInSc4d1	typický
rozsah	rozsah	k1gInSc4	rozsah
objemu	objem	k1gInSc2	objem
motoru	motor	k1gInSc2	motor
automobilů	automobil	k1gInPc2	automobil
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1,4	[number]	k4	1,4
l	l	kA	l
–	–	k?	–
typický	typický	k2eAgInSc4d1	typický
objem	objem	k1gInSc4	objem
lidské	lidský	k2eAgFnSc2d1	lidská
mozkovny	mozkovna	k1gFnSc2	mozkovna
</s>
</p>
<p>
<s>
3,78541	[number]	k4	3,78541
l	l	kA	l
–	–	k?	–
1	[number]	k4	1
US	US	kA	US
galon	galon	k1gInSc1	galon
</s>
</p>
<p>
<s>
4	[number]	k4	4
l	l	kA	l
–	–	k?	–
množství	množství	k1gNnSc1	množství
paliva	palivo	k1gNnSc2	palivo
spotřebovaného	spotřebovaný	k2eAgInSc2d1	spotřebovaný
každou	každý	k3xTgFnSc4	každý
sekundu	sekunda	k1gFnSc4	sekunda
v	v	k7c6	v
letadle	letadlo	k1gNnSc6	letadlo
Boeing	boeing	k1gInSc1	boeing
747.	[number]	k4	747.
</s>
</p>
<p>
<s>
4,54609	[number]	k4	4,54609
l	l	kA	l
–	–	k?	–
1	[number]	k4	1
imperiální	imperiální	k2eAgInSc4d1	imperiální
galon	galon	k1gInSc4	galon
</s>
</p>
<p>
<s>
4,7	[number]	k4	4,7
l	l	kA	l
–	–	k?	–
obvyklý	obvyklý	k2eAgInSc4d1	obvyklý
celkový	celkový	k2eAgInSc4d1	celkový
objem	objem	k1gInSc4	objem
krve	krev	k1gFnSc2	krev
v	v	k7c6	v
lidském	lidský	k2eAgInSc6d1	lidský
organismu	organismus	k1gInSc6	organismus
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
l	l	kA	l
–	–	k?	–
typické	typický	k2eAgNnSc1d1	typické
množství	množství	k1gNnSc1	množství
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
lidské	lidský	k2eAgNnSc1d1	lidské
srdce	srdce	k1gNnSc1	srdce
přepumpuje	přepumpovat	k5eAaPmIp3nS	přepumpovat
každou	každý	k3xTgFnSc4	každý
minutu	minuta	k1gFnSc4	minuta
</s>
</p>
<p>
<s>
5,5	[number]	k4	5,5
l	l	kA	l
–	–	k?	–
průměrné	průměrný	k2eAgNnSc1d1	průměrné
množství	množství	k1gNnSc1	množství
nafty	nafta	k1gFnSc2	nafta
spotřebované	spotřebovaný	k2eAgFnSc2d1	spotřebovaná
na	na	k7c6	na
100	[number]	k4	100
km	km	kA	km
při	při	k7c6	při
jízdě	jízda	k1gFnSc6	jízda
ve	v	k7c6	v
Škodě	škoda	k1gFnSc6	škoda
Octavia	octavia	k1gFnSc1	octavia
s	s	k7c7	s
motorem	motor	k1gInSc7	motor
2.	[number]	k4	2.
<g/>
0	[number]	k4	0
TDI-PD	TDI-PD	k1gFnPc2	TDI-PD
<g/>
/	/	kIx~	/
<g/>
103	[number]	k4	103
kW	kW	kA	kW
</s>
</p>
<p>
<s>
6	[number]	k4	6
l	l	kA	l
–	–	k?	–
typický	typický	k2eAgInSc4d1	typický
objem	objem	k1gInSc4	objem
mužských	mužský	k2eAgFnPc2d1	mužská
plic	plíce	k1gFnPc2	plíce
</s>
</p>
<p>
<s>
10	[number]	k4	10
l	l	kA	l
=	=	kIx~	=
0,01	[number]	k4	0,01
m3	m3	k4	m3
</s>
</p>
<p>
<s>
1	[number]	k4	1
litr	litr	k1gInSc1	litr
je	být	k5eAaImIp3nS	být
roven	roven	k2eAgInSc1d1	roven
</s>
</p>
<p>
<s>
0,001	[number]	k4	0,001
m3	m3	k4	m3
</s>
</p>
<p>
<s>
1000	[number]	k4	1000
cm3	cm3	k4	cm3
</s>
</p>
<p>
<s>
10	[number]	k4	10
dl	dl	k?	dl
</s>
</p>
<p>
<s>
100	[number]	k4	100
cl	cl	k?	cl
</s>
</p>
<p>
<s>
1000	[number]	k4	1000
ml	ml	kA	ml
</s>
</p>
<p>
<s>
objemu	objem	k1gInSc3	objem
krychle	krychle	k1gFnSc1	krychle
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
hrany	hrana	k1gFnSc2	hrana
10	[number]	k4	10
cm	cm	kA	cm
</s>
</p>
<p>
<s>
objemu	objem	k1gInSc3	objem
vody	voda	k1gFnSc2	voda
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
1	[number]	k4	1
kg	kg	kA	kg
</s>
</p>
<p>
<s>
61,0237	[number]	k4	61,0237
krychlového	krychlový	k2eAgInSc2d1	krychlový
palce	palec	k1gInSc2	palec
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
<g/>
2	[number]	k4	2
l	l	kA	l
–	–	k?	–
typický	typický	k2eAgInSc4d1	typický
rozsah	rozsah	k1gInSc4	rozsah
objemu	objem	k1gInSc2	objem
motoru	motor	k1gInSc2	motor
automobilů	automobil	k1gInPc2	automobil
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1,4	[number]	k4	1,4
l	l	kA	l
–	–	k?	–
typický	typický	k2eAgInSc4d1	typický
objem	objem	k1gInSc4	objem
lidské	lidský	k2eAgFnSc2d1	lidská
mozkovny	mozkovna	k1gFnSc2	mozkovna
</s>
</p>
<p>
<s>
3,78541	[number]	k4	3,78541
l	l	kA	l
–	–	k?	–
1	[number]	k4	1
US	US	kA	US
galon	galon	k1gInSc1	galon
</s>
</p>
<p>
<s>
4	[number]	k4	4
l	l	kA	l
–	–	k?	–
množství	množství	k1gNnSc1	množství
paliva	palivo	k1gNnSc2	palivo
spotřebovaného	spotřebovaný	k2eAgInSc2d1	spotřebovaný
každou	každý	k3xTgFnSc4	každý
sekundu	sekunda	k1gFnSc4	sekunda
v	v	k7c6	v
letadle	letadlo	k1gNnSc6	letadlo
Boeing	boeing	k1gInSc1	boeing
747.	[number]	k4	747.
</s>
</p>
<p>
<s>
4,54609	[number]	k4	4,54609
l	l	kA	l
–	–	k?	–
1	[number]	k4	1
imperiální	imperiální	k2eAgInSc4d1	imperiální
galon	galon	k1gInSc4	galon
</s>
</p>
<p>
<s>
4,7	[number]	k4	4,7
l	l	kA	l
–	–	k?	–
obvyklý	obvyklý	k2eAgInSc4d1	obvyklý
celkový	celkový	k2eAgInSc4d1	celkový
objem	objem	k1gInSc4	objem
krve	krev	k1gFnSc2	krev
v	v	k7c6	v
lidském	lidský	k2eAgInSc6d1	lidský
organismu	organismus	k1gInSc6	organismus
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
l	l	kA	l
–	–	k?	–
typické	typický	k2eAgNnSc1d1	typické
množství	množství	k1gNnSc1	množství
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
lidské	lidský	k2eAgNnSc1d1	lidské
srdce	srdce	k1gNnSc1	srdce
přepumpuje	přepumpovat	k5eAaPmIp3nS	přepumpovat
každou	každý	k3xTgFnSc4	každý
minutu	minuta	k1gFnSc4	minuta
</s>
</p>
<p>
<s>
5,5	[number]	k4	5,5
l	l	kA	l
–	–	k?	–
průměrné	průměrný	k2eAgNnSc1d1	průměrné
množství	množství	k1gNnSc1	množství
nafty	nafta	k1gFnSc2	nafta
spotřebované	spotřebovaný	k2eAgFnSc2d1	spotřebovaná
na	na	k7c6	na
100	[number]	k4	100
km	km	kA	km
při	při	k7c6	při
jízdě	jízda	k1gFnSc6	jízda
ve	v	k7c6	v
Škodě	škoda	k1gFnSc6	škoda
Octavia	octavia	k1gFnSc1	octavia
s	s	k7c7	s
motorem	motor	k1gInSc7	motor
2.	[number]	k4	2.
<g/>
0	[number]	k4	0
TDI-PD	TDI-PD	k1gFnPc2	TDI-PD
<g/>
/	/	kIx~	/
<g/>
103	[number]	k4	103
kW	kW	kA	kW
</s>
</p>
<p>
<s>
6	[number]	k4	6
l	l	kA	l
–	–	k?	–
typický	typický	k2eAgInSc4d1	typický
objem	objem	k1gInSc4	objem
mužských	mužský	k2eAgFnPc2d1	mužská
plic	plíce	k1gFnPc2	plíce
</s>
</p>
<p>
<s>
10	[number]	k4	10
l	l	kA	l
=	=	kIx~	=
0,01	[number]	k4	0,01
m3	m3	k4	m3
</s>
</p>
<p>
<s>
300	[number]	k4	300
ml	ml	kA	ml
=	=	kIx~	=
3	[number]	k4	3
dl	dl	k?	dl
–	–	k?	–
"	"	kIx"	"
<g/>
třetinka	třetinka	k1gFnSc1	třetinka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
malé	malý	k2eAgNnSc4d1	malé
pivo	pivo	k1gNnSc4	pivo
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
400	[number]	k4	400
ml	ml	kA	ml
–	–	k?	–
typický	typický	k2eAgInSc4d1	typický
objem	objem	k1gInSc4	objem
lidského	lidský	k2eAgInSc2d1	lidský
močového	močový	k2eAgInSc2d1	močový
měchýře	měchýř	k1gInSc2	měchýř
</s>
</p>
<p>
<s>
473	[number]	k4	473
ml	ml	kA	ml
–	–	k?	–
1	[number]	k4	1
US	US	kA	US
pinta	pinta	k1gFnSc1	pinta
</s>
</p>
<p>
<s>
500	[number]	k4	500
ml	ml	kA	ml
=	=	kIx~	=
0,5	[number]	k4	0,5
l	l	kA	l
–	–	k?	–
Půllitr	půllitr	k1gInSc1	půllitr
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
velké	velký	k2eAgNnSc4d1	velké
pivo	pivo	k1gNnSc4	pivo
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
568	[number]	k4	568
ml	ml	kA	ml
–	–	k?	–
1	[number]	k4	1
imperiální	imperiální	k2eAgFnSc1d1	imperiální
pinta	pinta	k1gFnSc1	pinta
</s>
</p>
<p>
<s>
750	[number]	k4	750
ml	ml	kA	ml
–	–	k?	–
nejběžnější	běžný	k2eAgInSc4d3	nejběžnější
objem	objem	k1gInSc4	objem
láhve	láhev	k1gFnSc2	láhev
vína	víno	k1gNnSc2	víno
</s>
</p>
<p>
<s>
946	[number]	k4	946
ml	ml	kA	ml
–	–	k?	–
1	[number]	k4	1
US	US	kA	US
quart	quart	k1gInSc1	quart
</s>
</p>
<p>
<s>
1000	[number]	k4	1000
ml	ml	kA	ml
=	=	kIx~	=
1	[number]	k4	1
litr	litr	k1gInSc1	litr
</s>
</p>
<p>
<s>
100	[number]	k4	100
ml	ml	kA	ml
je	být	k5eAaImIp3nS	být
rovno	roven	k2eAgNnSc1d1	rovno
</s>
</p>
<p>
<s>
100	[number]	k4	100
cm3	cm3	k4	cm3
</s>
</p>
<p>
<s>
1	[number]	k4	1
dl	dl	k?	dl
</s>
</p>
<p>
<s>
1/10	[number]	k4	1/10
l	l	kA	l
</s>
</p>
<p>
<s>
10	[number]	k4	10
cl	cl	k?	cl
</s>
</p>
<p>
<s>
100	[number]	k4	100
ml	ml	kA	ml
</s>
</p>
<p>
<s>
objemu	objem	k1gInSc3	objem
krychle	krychle	k1gFnSc1	krychle
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
hrany	hrana	k1gFnSc2	hrana
46	[number]	k4	46
mm	mm	kA	mm
</s>
</p>
<p>
<s>
6,1	[number]	k4	6,1
krychlového	krychlový	k2eAgInSc2d1	krychlový
palce	palec	k1gInSc2	palec
</s>
</p>
<p>
<s>
10	[number]	k4	10
ml	ml	kA	ml
je	být	k5eAaImIp3nS	být
rovno	roven	k2eAgNnSc1d1	rovno
</s>
</p>
<p>
<s>
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
5	[number]	k4	5
m3	m3	k4	m3
</s>
</p>
<p>
<s>
1	[number]	k4	1
cl	cl	k?	cl
</s>
</p>
<p>
<s>
10	[number]	k4	10
cm3	cm3	k4	cm3
</s>
</p>
<p>
<s>
0,1	[number]	k4	0,1
dl	dl	k?	dl
</s>
</p>
<p>
<s>
0,01	[number]	k4	0,01
l	l	kA	l
</s>
</p>
<p>
<s>
objemu	objem	k1gInSc3	objem
krychle	krychle	k1gFnSc1	krychle
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
hrany	hrana	k1gFnSc2	hrana
21,5	[number]	k4	21,5
mm	mm	kA	mm
</s>
</p>
<p>
<s>
objemu	objem	k1gInSc3	objem
koule	koule	k1gFnSc1	koule
o	o	k7c6	o
poloměru	poloměr	k1gInSc6	poloměr
13,3	[number]	k4	13,3
mm	mm	kA	mm
</s>
</p>
<p>
<s>
16,387064	[number]	k4	16,387064
ml	ml	kA	ml
=	=	kIx~	=
1	[number]	k4	1
krychlový	krychlový	k2eAgInSc4d1	krychlový
palec	palec	k1gInSc4	palec
</s>
</p>
<p>
<s>
20	[number]	k4	20
ml	ml	kA	ml
–	–	k?	–
"	"	kIx"	"
<g/>
malý	malý	k2eAgInSc1d1	malý
panák	panák	k1gInSc1	panák
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
40	[number]	k4	40
ml	ml	kA	ml
–	–	k?	–
"	"	kIx"	"
<g/>
panák	panák	k1gMnSc1	panák
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
50	[number]	k4	50
ml	ml	kA	ml
=	=	kIx~	=
0,5	[number]	k4	0,5
dl	dl	k?	dl
–	–	k?	–
"	"	kIx"	"
<g/>
velký	velký	k2eAgInSc1d1	velký
panák	panák	k1gInSc1	panák
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
100	[number]	k4	100
ml	ml	kA	ml
=	=	kIx~	=
1	[number]	k4	1
dl	dl	k?	dl
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odměrná	odměrný	k2eAgNnPc1d1	odměrné
měřidla	měřidlo	k1gNnPc1	měřidlo
a	a	k8xC	a
nástroje	nástroj	k1gInPc1	nástroj
==	==	k?	==
</s>
</p>
<p>
<s>
odměrný	odměrný	k2eAgInSc1d1	odměrný
válec	válec	k1gInSc1	válec
</s>
</p>
<p>
<s>
pipeta	pipeta	k1gFnSc1	pipeta
</s>
</p>
<p>
<s>
byreta	byreta	k1gFnSc1	byreta
</s>
</p>
<p>
<s>
vodoměr	vodoměr	k1gInSc1	vodoměr
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
objem	objem	k1gInSc1	objem
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Lokální	lokální	k2eAgFnSc1d1	lokální
šablona	šablona	k1gFnSc1	šablona
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
kategorii	kategorie	k1gFnSc4	kategorie
Commons	Commonsa	k1gFnPc2	Commonsa
než	než	k8xS	než
přiřazená	přiřazený	k2eAgFnSc1d1	přiřazená
položka	položka	k1gFnSc1	položka
Wikidat	Wikidat	k1gFnSc2	Wikidat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Lokální	lokální	k2eAgInSc1d1	lokální
odkaz	odkaz	k1gInSc1	odkaz
<g/>
:	:	kIx,	:
Volume	volum	k1gInSc5	volum
</s>
</p>
<p>
<s>
Wikidata	Wikidata	k1gFnSc1	Wikidata
<g/>
:	:	kIx,	:
Volumes	Volumes	k1gInSc1	Volumes
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
objem	objem	k1gInSc1	objem
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
