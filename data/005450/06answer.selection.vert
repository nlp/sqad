<s>
Objem	objem	k1gInSc1	objem
je	být	k5eAaImIp3nS	být
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
veličina	veličina	k1gFnSc1	veličina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
velikost	velikost	k1gFnSc4	velikost
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
zabírá	zabírat	k5eAaImIp3nS	zabírat
těleso	těleso	k1gNnSc1	těleso
<g/>
.	.	kIx.	.
</s>
