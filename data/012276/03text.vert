<p>
<s>
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
(	(	kIx(	(
<g/>
FF	ff	kA	ff
UP	UP	kA	UP
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
osmi	osm	k4xCc2	osm
fakult	fakulta	k1gFnPc2	fakulta
této	tento	k3xDgFnSc2	tento
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
obnovila	obnovit	k5eAaPmAgFnS	obnovit
v	v	k7c6	v
zimním	zimní	k2eAgInSc6d1	zimní
semestru	semestr	k1gInSc6	semestr
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
katedrách	katedra	k1gFnPc6	katedra
a	a	k8xC	a
dalších	další	k2eAgNnPc6d1	další
odborných	odborný	k2eAgNnPc6d1	odborné
pracovištích	pracoviště	k1gNnPc6	pracoviště
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
studentům	student	k1gMnPc3	student
a	a	k8xC	a
uchazečům	uchazeč	k1gMnPc3	uchazeč
více	hodně	k6eAd2	hodně
než	než	k8xS	než
šest	šest	k4xCc4	šest
set	sto	k4xCgNnPc2	sto
kombinací	kombinace	k1gFnPc2	kombinace
filologických	filologický	k2eAgMnPc2d1	filologický
<g/>
,	,	kIx,	,
humanitních	humanitní	k2eAgInPc2d1	humanitní
<g/>
,	,	kIx,	,
sociálních	sociální	k2eAgInPc2d1	sociální
a	a	k8xC	a
uměnovědných	uměnovědný	k2eAgInPc2d1	uměnovědný
oborů	obor	k1gInPc2	obor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
21	[number]	k4	21
katedrami	katedra	k1gFnPc7	katedra
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
šesti	šest	k4xCc7	šest
tisíci	tisíc	k4xCgInPc7	tisíc
studenty	student	k1gMnPc7	student
největší	veliký	k2eAgFnSc1d3	veliký
z	z	k7c2	z
osmi	osm	k4xCc2	osm
fakult	fakulta	k1gFnPc2	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Palackého	Palackého	k2eAgFnSc2d1	Palackého
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Funguje	fungovat	k5eAaImIp3nS	fungovat
na	na	k7c6	na
samosprávném	samosprávný	k2eAgInSc6d1	samosprávný
principu	princip	k1gInSc6	princip
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc4	každý
třetí	třetí	k4xOgInSc4	třetí
rok	rok	k1gInSc4	rok
akademičtí	akademický	k2eAgMnPc1d1	akademický
pracovníci	pracovník	k1gMnPc1	pracovník
a	a	k8xC	a
studenti	student	k1gMnPc1	student
volí	volit	k5eAaImIp3nP	volit
akademický	akademický	k2eAgInSc4d1	akademický
senát	senát	k1gInSc4	senát
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gFnPc6	jehož
kompetencích	kompetence	k1gFnPc6	kompetence
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
dále	daleko	k6eAd2	daleko
volba	volba	k1gFnSc1	volba
děkana	děkan	k1gMnSc2	děkan
fakulty	fakulta	k1gFnSc2	fakulta
na	na	k7c4	na
čtyřleté	čtyřletý	k2eAgNnSc4d1	čtyřleté
období	období	k1gNnSc4	období
působení	působení	k1gNnSc2	působení
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
děkanem	děkan	k1gMnSc7	děkan
je	být	k5eAaImIp3nS	být
slavista	slavista	k1gMnSc1	slavista
prof.	prof.	kA	prof.
PhDr.	PhDr.	kA	PhDr.
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Pechal	Pechal	k1gMnSc1	Pechal
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
Akademickým	akademický	k2eAgInSc7d1	akademický
senátem	senát	k1gInSc7	senát
FF	ff	kA	ff
UP	UP	kA	UP
dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2017	[number]	k4	2017
a	a	k8xC	a
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
byl	být	k5eAaImAgInS	být
jmenován	jmenovat	k5eAaBmNgInS	jmenovat
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nahradil	nahradit	k5eAaPmAgInS	nahradit
historika	historik	k1gMnSc4	historik
Jiřího	Jiří	k1gMnSc4	Jiří
Lacha	Lach	k1gMnSc4	Lach
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Výuku	výuka	k1gFnSc4	výuka
na	na	k7c6	na
filosofické	filosofický	k2eAgFnSc6d1	filosofická
fakultě	fakulta	k1gFnSc6	fakulta
zahájil	zahájit	k5eAaPmAgInS	zahájit
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1576	[number]	k4	1576
Angličan	Angličan	k1gMnSc1	Angličan
George	Georg	k1gInSc2	Georg
Warr	Warr	k1gInSc1	Warr
přednáškou	přednáška	k1gFnSc7	přednáška
z	z	k7c2	z
logiky	logika	k1gFnSc2	logika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byla	být	k5eAaImAgFnS	být
filozofie	filozofie	k1gFnSc1	filozofie
chápána	chápat	k5eAaImNgFnS	chápat
šířeji	šířej	k1gMnPc7	šířej
než	než	k8xS	než
dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
láska	láska	k1gFnSc1	láska
k	k	k7c3	k
moudrosti	moudrost	k1gFnSc3	moudrost
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
mezi	mezi	k7c7	mezi
známými	známý	k2eAgFnPc7d1	známá
osobnostmi	osobnost	k1gFnPc7	osobnost
spjatými	spjatý	k2eAgFnPc7d1	spjatá
s	s	k7c7	s
fakultou	fakulta	k1gFnSc7	fakulta
nenajdeme	najít	k5eNaPmIp1nP	najít
jen	jen	k9	jen
filosofy	filosof	k1gMnPc4	filosof
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Likavec	Likavec	k1gMnSc1	Likavec
<g/>
,	,	kIx,	,
Franz	Franz	k1gMnSc1	Franz
Samuel	Samuel	k1gMnSc1	Samuel
Karpe	Karp	k1gMnSc5	Karp
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
z	z	k7c2	z
dnešního	dnešní	k2eAgInSc2d1	dnešní
pohledu	pohled	k1gInSc2	pohled
vědce	vědec	k1gMnSc2	vědec
v	v	k7c6	v
oborech	obor	k1gInPc6	obor
matematiky	matematika	k1gFnSc2	matematika
<g/>
,	,	kIx,	,
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
astronomie	astronomie	k1gFnSc2	astronomie
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Významné	významný	k2eAgFnPc4d1	významná
osobnosti	osobnost	k1gFnPc4	osobnost
Olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
tradičním	tradiční	k2eAgInSc6d1	tradiční
smyslu	smysl	k1gInSc6	smysl
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
fakultu	fakulta	k1gFnSc4	fakulta
svobodných	svobodný	k2eAgNnPc2d1	svobodné
umění	umění	k1gNnPc2	umění
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
vyučoval	vyučovat	k5eAaImAgInS	vyučovat
úplný	úplný	k2eAgInSc1d1	úplný
kurz	kurz	k1gInSc1	kurz
tzv.	tzv.	kA	tzv.
sedmi	sedm	k4xCc2	sedm
svobodných	svobodný	k2eAgNnPc2d1	svobodné
umění	umění	k1gNnPc2	umění
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
stupních	stupeň	k1gInPc6	stupeň
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
nejdříve	dříve	k6eAd3	dříve
trivium	trivium	k1gNnSc1	trivium
(	(	kIx(	(
<g/>
gramatika	gramatika	k1gFnSc1	gramatika
<g/>
,	,	kIx,	,
rétorika	rétorika	k1gFnSc1	rétorika
a	a	k8xC	a
dialektika	dialektika	k1gFnSc1	dialektika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jejich	jejich	k3xOp3gMnSc1	jejich
absolvent	absolvent	k1gMnSc1	absolvent
byl	být	k5eAaImAgMnS	být
baccalaureus	baccalaureus	k1gInSc4	baccalaureus
–	–	k?	–
ověnčený	ověnčený	k2eAgMnSc1d1	ověnčený
vavřínem	vavřín	k1gInSc7	vavřín
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
poté	poté	k6eAd1	poté
mohl	moct	k5eAaImAgInS	moct
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
kvadrivia	kvadrivium	k1gNnSc2	kvadrivium
(	(	kIx(	(
<g/>
aritmetika	aritmetika	k1gFnSc1	aritmetika
<g/>
,	,	kIx,	,
geometrie	geometrie	k1gFnSc1	geometrie
<g/>
,	,	kIx,	,
astronomie	astronomie	k1gFnSc1	astronomie
a	a	k8xC	a
hudba	hudba	k1gFnSc1	hudba
<g/>
)	)	kIx)	)
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
titul	titul	k1gInSc4	titul
magister	magistra	k1gFnPc2	magistra
artium	artium	k1gNnSc1	artium
(	(	kIx(	(
<g/>
liberalium	liberalium	k1gNnSc1	liberalium
<g/>
)	)	kIx)	)
–	–	k?	–
učitel	učitel	k1gMnSc1	učitel
či	či	k8xC	či
mistr	mistr	k1gMnSc1	mistr
(	(	kIx(	(
<g/>
svobodných	svobodný	k2eAgNnPc2d1	svobodné
<g/>
)	)	kIx)	)
umění	umění	k1gNnPc2	umění
<g/>
.	.	kIx.	.
<g/>
Jak	jak	k8xS	jak
po	po	k7c6	po
bakalářském	bakalářský	k2eAgInSc6d1	bakalářský
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
po	po	k7c6	po
magisterském	magisterský	k2eAgInSc6d1	magisterský
stupni	stupeň	k1gInSc6	stupeň
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
zapsat	zapsat	k5eAaPmF	zapsat
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
na	na	k7c6	na
některé	některý	k3yIgFnSc6	některý
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
odborných	odborný	k2eAgFnPc2d1	odborná
fakult	fakulta	k1gFnPc2	fakulta
středověké	středověký	k2eAgFnSc2d1	středověká
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1626	[number]	k4	1626
se	se	k3xPyFc4	se
výuka	výuka	k1gFnSc1	výuka
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
<g/>
,	,	kIx,	,
když	když	k8xS	když
k	k	k7c3	k
logice	logika	k1gFnSc3	logika
<g/>
,	,	kIx,	,
metafyzice	metafyzika	k1gFnSc3	metafyzika
a	a	k8xC	a
matematice	matematika	k1gFnSc3	matematika
přibyla	přibýt	k5eAaPmAgFnS	přibýt
i	i	k9	i
katedra	katedra	k1gFnSc1	katedra
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
přestěhování	přestěhování	k1gNnSc6	přestěhování
univerzity	univerzita	k1gFnSc2	univerzita
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1778	[number]	k4	1778
měla	mít	k5eAaImAgFnS	mít
fakulta	fakulta	k1gFnSc1	fakulta
jen	jen	k9	jen
čtyři	čtyři	k4xCgMnPc4	čtyři
profesory	profesor	k1gMnPc4	profesor
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jeden	jeden	k4xCgMnSc1	jeden
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
politické	politický	k2eAgFnSc2d1	politická
vědy	věda	k1gFnSc2	věda
(	(	kIx(	(
<g/>
tato	tento	k3xDgFnSc1	tento
katedra	katedra	k1gFnSc1	katedra
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1780	[number]	k4	1780
přesunula	přesunout	k5eAaPmAgFnS	přesunout
na	na	k7c4	na
právnickou	právnický	k2eAgFnSc4d1	právnická
fakultu	fakulta	k1gFnSc4	fakulta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
profesor	profesor	k1gMnSc1	profesor
Gärtler	Gärtler	k1gMnSc1	Gärtler
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
arabštinu	arabština	k1gFnSc4	arabština
<g/>
,	,	kIx,	,
sýrštinu	sýrština	k1gFnSc4	sýrština
a	a	k8xC	a
aramejštinu	aramejština	k1gFnSc4	aramejština
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
Karpe	Karp	k1gInSc5	Karp
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
pedagogiku	pedagogika	k1gFnSc4	pedagogika
a	a	k8xC	a
profesor	profesor	k1gMnSc1	profesor
Dürnbacher	Dürnbachra	k1gFnPc2	Dürnbachra
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
přírodní	přírodní	k2eAgFnPc4d1	přírodní
dějiny	dějiny	k1gFnPc4	dějiny
<g/>
.	.	kIx.	.
<g/>
Univerzita	univerzita	k1gFnSc1	univerzita
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
zpátky	zpátky	k6eAd1	zpátky
do	do	k7c2	do
Olomouce	Olomouc	k1gFnSc2	Olomouc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1782	[number]	k4	1782
degradována	degradovat	k5eAaBmNgFnS	degradovat
na	na	k7c4	na
akademické	akademický	k2eAgNnSc4d1	akademické
lyceum	lyceum	k1gNnSc4	lyceum
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1783	[number]	k4	1783
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
na	na	k7c6	na
filosofické	filosofický	k2eAgFnSc6d1	filosofická
fakultě	fakulta	k1gFnSc6	fakulta
udělovat	udělovat	k5eAaImF	udělovat
doktorské	doktorský	k2eAgInPc4d1	doktorský
a	a	k8xC	a
magisterské	magisterský	k2eAgInPc4d1	magisterský
tituly	titul	k1gInPc4	titul
(	(	kIx(	(
<g/>
právo	právo	k1gNnSc4	právo
udělovat	udělovat	k5eAaImF	udělovat
tituly	titul	k1gInPc4	titul
bakalářské	bakalářský	k2eAgFnSc2d1	Bakalářská
bylo	být	k5eAaImAgNnS	být
odebráno	odebrat	k5eAaPmNgNnS	odebrat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1821	[number]	k4	1821
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1805	[number]	k4	1805
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
reorganizaci	reorganizace	k1gFnSc3	reorganizace
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Filosofické	filosofický	k2eAgNnSc1d1	filosofické
studium	studium	k1gNnSc1	studium
stále	stále	k6eAd1	stále
plnilo	plnit	k5eAaImAgNnS	plnit
funkci	funkce	k1gFnSc4	funkce
předstupně	předstupeň	k1gInSc2	předstupeň
studia	studio	k1gNnSc2	studio
na	na	k7c6	na
specializovaných	specializovaný	k2eAgFnPc6d1	specializovaná
fakultách	fakulta	k1gFnPc6	fakulta
(	(	kIx(	(
<g/>
teologie	teologie	k1gFnSc1	teologie
<g/>
,	,	kIx,	,
právo	právo	k1gNnSc1	právo
<g/>
,	,	kIx,	,
medicína	medicína	k1gFnSc1	medicína
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
univerzitách	univerzita	k1gFnPc6	univerzita
trvalo	trvat	k5eAaImAgNnS	trvat
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
na	na	k7c6	na
lyceích	lyceum	k1gNnPc6	lyceum
pak	pak	k9	pak
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
byla	být	k5eAaImAgFnS	být
ustavena	ustaven	k2eAgFnSc1d1	ustavena
nová	nový	k2eAgFnSc1d1	nová
katedra	katedra	k1gFnSc1	katedra
světových	světový	k2eAgFnPc2d1	světová
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
vedlejší	vedlejší	k2eAgFnPc1d1	vedlejší
k	k	k7c3	k
dějinám	dějiny	k1gFnPc3	dějiny
byly	být	k5eAaImAgFnP	být
vyučovány	vyučován	k2eAgFnPc1d1	vyučována
diplomatika	diplomatika	k1gFnSc1	diplomatika
<g/>
,	,	kIx,	,
heraldika	heraldika	k1gFnSc1	heraldika
a	a	k8xC	a
numismatika	numismatika	k1gFnSc1	numismatika
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgFnP	být
také	také	k9	také
katedra	katedra	k1gFnSc1	katedra
estetiky	estetika	k1gFnSc2	estetika
<g/>
,	,	kIx,	,
vyšší	vysoký	k2eAgFnSc2d2	vyšší
matematiky	matematika	k1gFnSc2	matematika
<g/>
,	,	kIx,	,
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
astronomie	astronomie	k1gFnSc2	astronomie
<g/>
;	;	kIx,	;
katedra	katedra	k1gFnSc1	katedra
přírodních	přírodní	k2eAgFnPc2d1	přírodní
dějin	dějiny	k1gFnPc2	dějiny
zahrnující	zahrnující	k2eAgNnSc4d1	zahrnující
také	také	k9	také
zemědělství	zemědělství	k1gNnSc4	zemědělství
a	a	k8xC	a
lesní	lesní	k2eAgNnPc4d1	lesní
hospodářství	hospodářství	k1gNnPc4	hospodářství
<g/>
,	,	kIx,	,
a	a	k8xC	a
katedra	katedra	k1gFnSc1	katedra
dějin	dějiny	k1gFnPc2	dějiny
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
,	,	kIx,	,
pedagogiky	pedagogika	k1gFnSc2	pedagogika
a	a	k8xC	a
řecké	řecký	k2eAgFnSc2d1	řecká
filologie	filologie	k1gFnSc2	filologie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1808	[number]	k4	1808
byla	být	k5eAaImAgFnS	být
pak	pak	k6eAd1	pak
ustavena	ustaven	k2eAgFnSc1d1	ustavena
katedra	katedra	k1gFnSc1	katedra
ekonomie	ekonomie	k1gFnSc2	ekonomie
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
univerzita	univerzita	k1gFnSc1	univerzita
disponovala	disponovat	k5eAaBmAgFnS	disponovat
také	také	k9	také
minerálním	minerální	k2eAgInSc7d1	minerální
kabinetem	kabinet	k1gInSc7	kabinet
<g/>
,	,	kIx,	,
kabinetem	kabinet	k1gInSc7	kabinet
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
chemickou	chemický	k2eAgFnSc7d1	chemická
laboratoří	laboratoř	k1gFnSc7	laboratoř
a	a	k8xC	a
botanickou	botanický	k2eAgFnSc7d1	botanická
zahradou	zahrada	k1gFnSc7	zahrada
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1816	[number]	k4	1816
bylo	být	k5eAaImAgNnS	být
studium	studium	k1gNnSc1	studium
filosofie	filosofie	k1gFnSc2	filosofie
prodlouženo	prodloužit	k5eAaPmNgNnS	prodloužit
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
o	o	k7c4	o
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
zkráceno	zkrátit	k5eAaPmNgNnS	zkrátit
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
dva	dva	k4xCgMnPc4	dva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
účasti	účast	k1gFnSc6	účast
studentů	student	k1gMnPc2	student
na	na	k7c6	na
revoluci	revoluce	k1gFnSc6	revoluce
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1851	[number]	k4	1851
k	k	k7c3	k
uzavření	uzavření	k1gNnSc3	uzavření
fakulty	fakulta	k1gFnSc2	fakulta
a	a	k8xC	a
rozpuštění	rozpuštění	k1gNnSc2	rozpuštění
jejich	jejich	k3xOp3gInPc2	jejich
šesti	šest	k4xCc2	šest
profesorů	profesor	k1gMnPc2	profesor
<g/>
.	.	kIx.	.
</s>
<s>
Uzavření	uzavření	k1gNnSc1	uzavření
však	však	k9	však
přečkaly	přečkat	k5eAaPmAgFnP	přečkat
katedry	katedra	k1gFnPc1	katedra
historie	historie	k1gFnSc2	historie
a	a	k8xC	a
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
přičleněny	přičlenit	k5eAaPmNgFnP	přičlenit
k	k	k7c3	k
právnické	právnický	k2eAgFnSc3d1	právnická
fakultě	fakulta	k1gFnSc3	fakulta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
sama	sám	k3xTgMnSc4	sám
byla	být	k5eAaImAgFnS	být
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1855	[number]	k4	1855
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Činnost	činnost	k1gFnSc1	činnost
fakulty	fakulta	k1gFnSc2	fakulta
byla	být	k5eAaImAgFnS	být
datum	datum	k1gNnSc4	datum
obnovení	obnovení	k1gNnSc2	obnovení
až	až	k9	až
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začala	začít	k5eAaPmAgFnS	začít
postupně	postupně	k6eAd1	postupně
vytvářet	vytvářet	k5eAaImF	vytvářet
mnoho	mnoho	k4c4	mnoho
individuálních	individuální	k2eAgFnPc2d1	individuální
kateder	katedra	k1gFnPc2	katedra
<g/>
:	:	kIx,	:
filozofie	filozofie	k1gFnSc1	filozofie
<g/>
,	,	kIx,	,
sociologie	sociologie	k1gFnSc1	sociologie
<g/>
,	,	kIx,	,
historie	historie	k1gFnSc1	historie
<g/>
,	,	kIx,	,
psychologie	psychologie	k1gFnSc1	psychologie
<g/>
,	,	kIx,	,
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
historie	historie	k1gFnSc1	historie
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
estetiky	estetika	k1gFnSc2	estetika
<g/>
,	,	kIx,	,
filologie	filologie	k1gFnSc2	filologie
a	a	k8xC	a
Českou	český	k2eAgFnSc4d1	Česká
katedru	katedra	k1gFnSc4	katedra
západních	západní	k2eAgInPc2d1	západní
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
slavistiky	slavistika	k1gFnSc2	slavistika
<g/>
.	.	kIx.	.
</s>
<s>
Šedesátá	šedesátý	k4xOgNnPc1	šedesátý
léta	léto	k1gNnPc1	léto
byla	být	k5eAaImAgNnP	být
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
liberalizace	liberalizace	k1gFnSc2	liberalizace
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
dobrý	dobrý	k2eAgInSc4d1	dobrý
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
rozvoj	rozvoj	k1gInSc4	rozvoj
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
normalizačních	normalizační	k2eAgNnPc2d1	normalizační
let	léto	k1gNnPc2	léto
1969-1989	[number]	k4	1969-1989
však	však	k8xC	však
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
FF	ff	kA	ff
UP	UP	kA	UP
masovým	masový	k2eAgFnPc3d1	masová
personálním	personální	k2eAgFnPc3d1	personální
čistkám	čistka	k1gFnPc3	čistka
mezi	mezi	k7c7	mezi
učiteli	učitel	k1gMnPc7	učitel
a	a	k8xC	a
zaměstnanci	zaměstnanec	k1gMnPc7	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
profesorů	profesor	k1gMnPc2	profesor
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
fakulty	fakulta	k1gFnSc2	fakulta
vypovězeno	vypovědět	k5eAaPmNgNnS	vypovědět
<g/>
,	,	kIx,	,
dostali	dostat	k5eAaPmAgMnP	dostat
zákaz	zákaz	k1gInSc4	zákaz
publikování	publikování	k1gNnSc2	publikování
i	i	k9	i
vyučování	vyučování	k1gNnSc1	vyučování
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
bylo	být	k5eAaImAgNnS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejhorších	zlý	k2eAgNnPc2d3	nejhorší
<g/>
,	,	kIx,	,
kterými	který	k3yRgNnPc7	který
fakulta	fakulta	k1gFnSc1	fakulta
prošla	projít	k5eAaPmAgFnS	projít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velké	velký	k2eAgFnPc1d1	velká
změny	změna	k1gFnPc1	změna
pak	pak	k6eAd1	pak
nastaly	nastat	k5eAaPmAgFnP	nastat
po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
dřívějších	dřívější	k2eAgFnPc2d1	dřívější
vyučujících	vyučující	k2eAgFnPc2d1	vyučující
vrátil	vrátit	k5eAaPmAgMnS	vrátit
na	na	k7c4	na
fakultu	fakulta	k1gFnSc4	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
mnoho	mnoho	k4c1	mnoho
nových	nový	k2eAgFnPc2d1	nová
kateder	katedra	k1gFnPc2	katedra
<g/>
:	:	kIx,	:
politická	politický	k2eAgNnPc4d1	politické
a	a	k8xC	a
evropská	evropský	k2eAgNnPc4d1	Evropské
studia	studio	k1gNnPc4	studio
<g/>
,	,	kIx,	,
žurnalistika	žurnalistika	k1gFnSc1	žurnalistika
<g/>
,	,	kIx,	,
klasická	klasický	k2eAgFnSc1d1	klasická
filologie	filologie	k1gFnSc1	filologie
<g/>
,	,	kIx,	,
katedra	katedra	k1gFnSc1	katedra
asijských	asijský	k2eAgFnPc2d1	asijská
studií	studie	k1gFnPc2	studie
nebo	nebo	k8xC	nebo
nederlandistiky	nederlandistika	k1gFnSc2	nederlandistika
<g/>
.	.	kIx.	.
</s>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
umožnila	umožnit	k5eAaPmAgFnS	umožnit
studovat	studovat	k5eAaImF	studovat
obory	obor	k1gInPc4	obor
na	na	k7c6	na
těchto	tento	k3xDgFnPc6	tento
katedrách	katedra	k1gFnPc6	katedra
v	v	k7c6	v
bakalářském	bakalářský	k2eAgMnSc6d1	bakalářský
<g/>
,	,	kIx,	,
magisterském	magisterský	k2eAgInSc6d1	magisterský
i	i	k8xC	i
doktorském	doktorský	k2eAgInSc6d1	doktorský
stupni	stupeň	k1gInSc6	stupeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Symboly	symbol	k1gInPc1	symbol
fakulty	fakulta	k1gFnSc2	fakulta
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
symboly	symbol	k1gInPc7	symbol
Filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
UP	UP	kA	UP
patří	patřit	k5eAaImIp3nP	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
žezlo	žezlo	k1gNnSc4	žezlo
Filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
:	:	kIx,	:
Bylo	být	k5eAaImAgNnS	být
vypracováno	vypracovat	k5eAaPmNgNnS	vypracovat
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
prof.	prof.	kA	prof.
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Horejce	Horejec	k1gMnSc2	Horejec
<g/>
,	,	kIx,	,
vyrobeno	vyroben	k2eAgNnSc1d1	vyrobeno
firmou	firma	k1gFnSc7	firma
J.	J.	kA	J.
Mňuk	Mňuk	k1gMnSc1	Mňuk
Mohelnice	Mohelnice	k1gFnSc2	Mohelnice
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1948	[number]	k4	1948
dodáno	dodat	k5eAaPmNgNnS	dodat
zlatnickou	zlatnický	k2eAgFnSc7d1	Zlatnická
firmou	firma	k1gFnSc7	firma
K.	K.	kA	K.
Ziegelheim	Ziegelheim	k1gMnSc1	Ziegelheim
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
žezla	žezlo	k1gNnSc2	žezlo
je	být	k5eAaImIp3nS	být
151	[number]	k4	151
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Hlavici	hlavice	k1gFnSc4	hlavice
žezla	žezlo	k1gNnSc2	žezlo
tvoří	tvořit	k5eAaImIp3nS	tvořit
postava	postava	k1gFnSc1	postava
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
představující	představující	k2eAgFnSc4d1	představující
bohyni	bohyně	k1gFnSc4	bohyně
moudrosti	moudrost	k1gFnSc2	moudrost
Pallas	Pallas	k1gMnSc1	Pallas
Athénu	Athéna	k1gFnSc4	Athéna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
řetězy	řetěz	k1gInPc1	řetěz
<g/>
:	:	kIx,	:
Všechny	všechen	k3xTgInPc1	všechen
řetězy	řetěz	k1gInPc1	řetěz
filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
druhů	druh	k1gInPc2	druh
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
17	[number]	k4	17
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
plných	plný	k2eAgInPc2d1	plný
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
tvar	tvar	k1gInSc4	tvar
štítu	štít	k1gInSc2	štít
Pallas	Pallas	k1gMnSc1	Pallas
Athény	Athéna	k1gFnSc2	Athéna
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
je	on	k3xPp3gFnPc4	on
kruhové	kruhový	k2eAgFnPc4d1	kruhová
terče	terč	k1gFnPc4	terč
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
24	[number]	k4	24
mm	mm	kA	mm
pro	pro	k7c4	pro
děkana	děkan	k1gMnSc4	děkan
a	a	k8xC	a
19	[number]	k4	19
mm	mm	kA	mm
pro	pro	k7c4	pro
řetězy	řetěz	k1gInPc4	řetěz
proděkanů	proděkan	k1gMnPc2	proděkan
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
řetězů	řetěz	k1gInPc2	řetěz
je	být	k5eAaImIp3nS	být
dole	dole	k6eAd1	dole
sponový	sponový	k2eAgInSc1d1	sponový
článek	článek	k1gInSc1	článek
<g/>
,	,	kIx,	,
utvořený	utvořený	k2eAgInSc1d1	utvořený
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
měsíčních	měsíční	k2eAgInPc2d1	měsíční
srpků	srpek	k1gInPc2	srpek
sestavených	sestavený	k2eAgInPc2d1	sestavený
do	do	k7c2	do
trojúhelníku	trojúhelník	k1gInSc6	trojúhelník
<g/>
,	,	kIx,	,
postaveného	postavený	k2eAgMnSc2d1	postavený
na	na	k7c6	na
špici	špice	k1gFnSc6	špice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
místo	místo	k7c2	místo
záře	zář	k1gFnSc2	zář
mají	mít	k5eAaImIp3nP	mít
umístěny	umístěn	k2eAgFnPc1d1	umístěna
iniciály	iniciála	k1gFnPc1	iniciála
"	"	kIx"	"
<g/>
UPO	UPO	kA	UPO
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Barvou	barva	k1gFnSc7	barva
olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
je	být	k5eAaImIp3nS	být
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Studium	studium	k1gNnSc1	studium
<g/>
,	,	kIx,	,
katedry	katedra	k1gFnPc1	katedra
a	a	k8xC	a
pracoviště	pracoviště	k1gNnPc1	pracoviště
==	==	k?	==
</s>
</p>
<p>
<s>
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
UP	UP	kA	UP
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
studia	studio	k1gNnPc4	studio
humanitních	humanitní	k2eAgFnPc2d1	humanitní
<g/>
,	,	kIx,	,
filologických	filologický	k2eAgFnPc2d1	filologická
a	a	k8xC	a
uměnovědných	uměnovědný	k2eAgFnPc2d1	uměnovědná
disciplín	disciplína	k1gFnPc2	disciplína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
přirozeného	přirozený	k2eAgInSc2d1	přirozený
vývoje	vývoj	k1gInSc2	vývoj
fakulty	fakulta	k1gFnSc2	fakulta
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
proměňují	proměňovat	k5eAaImIp3nP	proměňovat
studijní	studijní	k2eAgInPc4d1	studijní
programy	program	k1gInPc4	program
tradičních	tradiční	k2eAgInPc2d1	tradiční
oborů	obor	k1gInPc2	obor
a	a	k8xC	a
tradičních	tradiční	k2eAgFnPc2d1	tradiční
kateder	katedra	k1gFnPc2	katedra
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
volitelnosti	volitelnost	k1gFnSc2	volitelnost
a	a	k8xC	a
variability	variabilita	k1gFnSc2	variabilita
<g/>
.	.	kIx.	.
</s>
<s>
Studenti	student	k1gMnPc1	student
mají	mít	k5eAaImIp3nP	mít
možnost	možnost	k1gFnSc4	možnost
modifikovat	modifikovat	k5eAaBmF	modifikovat
své	svůj	k3xOyFgInPc4	svůj
studijní	studijní	k2eAgInPc4d1	studijní
programy	program	k1gInPc4	program
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
volby	volba	k1gFnSc2	volba
kombinace	kombinace	k1gFnSc2	kombinace
dvouoborového	dvouoborový	k2eAgNnSc2d1	dvouoborový
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
na	na	k7c6	na
Filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
je	být	k5eAaImIp3nS	být
pojato	pojmout	k5eAaPmNgNnS	pojmout
jako	jako	k8xS	jako
odborné	odborný	k2eAgFnPc1d1	odborná
<g/>
,	,	kIx,	,
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
získat	získat	k5eAaPmF	získat
učitelskou	učitelský	k2eAgFnSc4d1	učitelská
způsobilost	způsobilost	k1gFnSc4	způsobilost
pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
na	na	k7c6	na
středních	střední	k2eAgFnPc6d1	střední
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
modulovanou	modulovaný	k2eAgFnSc4d1	modulovaná
bakalářsko-magisterskou	bakalářskoagisterský	k2eAgFnSc4d1	bakalářsko-magisterský
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
oborů	obor	k1gInPc2	obor
je	být	k5eAaImIp3nS	být
akreditována	akreditovat	k5eAaBmNgFnS	akreditovat
i	i	k9	i
pro	pro	k7c4	pro
doktorské	doktorský	k2eAgInPc4d1	doktorský
studijní	studijní	k2eAgInPc4d1	studijní
programy	program	k1gInPc4	program
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
možnost	možnost	k1gFnSc1	možnost
studia	studio	k1gNnSc2	studio
prakticky	prakticky	k6eAd1	prakticky
zaměřených	zaměřený	k2eAgInPc2d1	zaměřený
bakalářských	bakalářský	k2eAgInPc2d1	bakalářský
programů	program	k1gInPc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
Filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
lze	lze	k6eAd1	lze
prezenční	prezenční	k2eAgFnSc7d1	prezenční
i	i	k8xC	i
kombinovanou	kombinovaný	k2eAgFnSc7d1	kombinovaná
formou	forma	k1gFnSc7	forma
<g/>
.	.	kIx.	.
</s>
<s>
Kombinované	kombinovaný	k2eAgNnSc1d1	kombinované
studium	studium	k1gNnSc1	studium
zabezpečuje	zabezpečovat	k5eAaImIp3nS	zabezpečovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
Institut	institut	k1gInSc1	institut
celoživotního	celoživotní	k2eAgNnSc2d1	celoživotní
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
působí	působit	k5eAaImIp3nS	působit
specializovaná	specializovaný	k2eAgNnPc4d1	specializované
vědecká	vědecký	k2eAgNnPc4d1	vědecké
a	a	k8xC	a
odborná	odborný	k2eAgNnPc4d1	odborné
pracoviště	pracoviště	k1gNnPc4	pracoviště
<g/>
:	:	kIx,	:
Arbeitsstelle	Arbeitsstell	k1gInSc6	Arbeitsstell
für	für	k?	für
deutschmährische	deutschmährische	k1gNnSc2	deutschmährische
Literatur	literatura	k1gFnPc2	literatura
<g/>
,	,	kIx,	,
Centrum	centrum	k1gNnSc1	centrum
judaistických	judaistický	k2eAgFnPc2d1	judaistická
studií	studie	k1gFnPc2	studie
Kurta	kurta	k1gFnSc1	kurta
a	a	k8xC	a
Ursuly	Ursula	k1gFnPc1	Ursula
Schubertových	Schubertových	k2eAgFnPc1d1	Schubertových
<g/>
,	,	kIx,	,
Centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
československá	československý	k2eAgNnPc4d1	Československé
exilová	exilový	k2eAgNnPc4d1	exilové
studia	studio	k1gNnPc4	studio
<g/>
,	,	kIx,	,
Centrum	centrum	k1gNnSc4	centrum
pro	pro	k7c4	pro
jazyk	jazyk	k1gInSc4	jazyk
a	a	k8xC	a
kulturu	kultura	k1gFnSc4	kultura
v	v	k7c6	v
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
a	a	k8xC	a
Vlámska	Vlámska	k1gFnSc1	Vlámska
"	"	kIx"	"
<g/>
Erasmianum	Erasmianum	k1gInSc1	Erasmianum
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Dokumentační	dokumentační	k2eAgNnSc1d1	dokumentační
centrum	centrum	k1gNnSc1	centrum
dramatických	dramatický	k2eAgNnPc2d1	dramatické
umění	umění	k1gNnPc2	umění
<g/>
,	,	kIx,	,
Kabinet	kabinet	k1gInSc1	kabinet
interkulturních	interkulturní	k2eAgFnPc2d1	interkulturní
studií	studie	k1gFnPc2	studie
<g/>
,	,	kIx,	,
Kabinet	kabinet	k1gInSc1	kabinet
obecné	obecný	k2eAgFnSc2d1	obecná
lingvistiky	lingvistika	k1gFnSc2	lingvistika
<g/>
,	,	kIx,	,
Kabinet	kabinet	k1gInSc1	kabinet
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
uměleckohistorických	uměleckohistorický	k2eAgFnPc2d1	Uměleckohistorická
památek	památka	k1gFnPc2	památka
<g/>
,	,	kIx,	,
Kabinet	kabinet	k1gInSc1	kabinet
regionálních	regionální	k2eAgFnPc2d1	regionální
dějin	dějiny	k1gFnPc2	dějiny
<g/>
,	,	kIx,	,
Kabinet	kabinet	k1gInSc1	kabinet
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
dějin	dějiny	k1gFnPc2	dějiny
filozofie	filozofie	k1gFnSc1	filozofie
středověku	středověk	k1gInSc2	středověk
a	a	k8xC	a
renesance	renesance	k1gFnSc2	renesance
<g/>
,	,	kIx,	,
Psychologické	psychologický	k2eAgNnSc1d1	psychologické
poradenské	poradenský	k2eAgNnSc1d1	poradenské
centrum	centrum	k1gNnSc1	centrum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
má	mít	k5eAaImIp3nS	mít
21	[number]	k4	21
kateder	katedra	k1gFnPc2	katedra
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
anglistiky	anglistika	k1gFnSc2	anglistika
a	a	k8xC	a
amerikanistiky	amerikanistika	k1gFnSc2	amerikanistika
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
aplikované	aplikovaný	k2eAgFnSc2d1	aplikovaná
lingvistiky	lingvistika	k1gFnSc2	lingvistika
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
asijských	asijský	k2eAgFnPc2d1	asijská
studií	studie	k1gFnPc2	studie
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
bohemistiky	bohemistika	k1gFnSc2	bohemistika
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
dějin	dějiny	k1gFnPc2	dějiny
umění	umění	k1gNnSc2	umění
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
filozofie	filozofie	k1gFnSc1	filozofie
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
germanistiky	germanistika	k1gFnSc2	germanistika
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
historie	historie	k1gFnSc2	historie
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
politologie	politologie	k1gFnSc2	politologie
a	a	k8xC	a
evropských	evropský	k2eAgFnPc2d1	Evropská
studií	studie	k1gFnPc2	studie
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
aplikované	aplikovaný	k2eAgFnSc2d1	aplikovaná
ekonomie	ekonomie	k1gFnSc2	ekonomie
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
klasické	klasický	k2eAgFnSc2d1	klasická
filologie	filologie	k1gFnSc2	filologie
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
muzikologie	muzikologie	k1gFnSc2	muzikologie
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
nederlandistiky	nederlandistika	k1gFnSc2	nederlandistika
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
obecné	obecný	k2eAgFnSc2d1	obecná
lingvistiky	lingvistika	k1gFnSc2	lingvistika
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
psychologie	psychologie	k1gFnSc2	psychologie
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
romanistiky	romanistika	k1gFnSc2	romanistika
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
slavistiky	slavistika	k1gFnSc2	slavistika
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
sociologie	sociologie	k1gFnSc2	sociologie
<g/>
,	,	kIx,	,
andragogiky	andragogika	k1gFnSc2	andragogika
a	a	k8xC	a
kulturní	kulturní	k2eAgFnSc2d1	kulturní
antropologie	antropologie	k1gFnSc2	antropologie
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
divadelních	divadelní	k2eAgFnPc2d1	divadelní
a	a	k8xC	a
filmových	filmový	k2eAgFnPc2d1	filmová
studií	studie	k1gFnPc2	studie
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
mediálních	mediální	k2eAgFnPc2d1	mediální
a	a	k8xC	a
kulturálních	kulturální	k2eAgFnPc2d1	kulturální
studií	studie	k1gFnPc2	studie
a	a	k8xC	a
žurnalistiky	žurnalistika	k1gFnSc2	žurnalistika
</s>
</p>
<p>
<s>
Centrum	centrum	k1gNnSc1	centrum
judaistických	judaistický	k2eAgNnPc2d1	judaistické
studií	studio	k1gNnPc2	studio
</s>
</p>
<p>
<s>
==	==	k?	==
Vedení	vedení	k1gNnSc1	vedení
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Seznam	seznam	k1gInSc1	seznam
děkanů	děkan	k1gMnPc2	děkan
===	===	k?	===
</s>
</p>
<p>
<s>
Zahrnuti	zahrnut	k2eAgMnPc1d1	zahrnut
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
děkani	děkan	k1gMnPc1	děkan
společenskovědní	společenskovědní	k2eAgFnSc2d1	společenskovědní
větve	větev	k1gFnSc2	větev
pedagogické	pedagogický	k2eAgFnSc2d1	pedagogická
fakulty	fakulta	k1gFnSc2	fakulta
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
–	–	k?	–
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
a	a	k8xC	a
děkani	děkan	k1gMnPc1	děkan
fakulty	fakulta	k1gFnSc2	fakulta
společenských	společenský	k2eAgFnPc2d1	společenská
věd	věda	k1gFnPc2	věda
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
pedagogické	pedagogický	k2eAgFnSc2d1	pedagogická
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
–	–	k?	–
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1946	[number]	k4	1946
prof.	prof.	kA	prof.
PhDr.	PhDr.	kA	PhDr.
Jindřich	Jindřich	k1gMnSc1	Jindřich
Šebánek	Šebánek	k1gMnSc1	Šebánek
</s>
</p>
<p>
<s>
1947	[number]	k4	1947
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
prof.	prof.	kA	prof.
PhDr.	PhDr.	kA	PhDr.
Vincenc	Vincenc	k1gMnSc1	Vincenc
Lesný	lesný	k2eAgMnSc1d1	lesný
</s>
</p>
<p>
<s>
1948	[number]	k4	1948
<g/>
–	–	k?	–
<g/>
1949	[number]	k4	1949
prof.	prof.	kA	prof.
PhDr.	PhDr.	kA	PhDr.
Karel	Karel	k1gMnSc1	Karel
Janáček	Janáček	k1gMnSc1	Janáček
<g/>
,	,	kIx,	,
DrSc	DrSc	kA	DrSc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1949	[number]	k4	1949
<g/>
–	–	k?	–
<g/>
1953	[number]	k4	1953
prof.	prof.	kA	prof.
PhDr.	PhDr.	kA	PhDr.
Jaromír	Jaromír	k1gMnSc1	Jaromír
Bělič	bělič	k1gMnSc1	bělič
<g/>
,	,	kIx,	,
DrSc	DrSc	kA	DrSc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1953	[number]	k4	1953
<g/>
–	–	k?	–
<g/>
1954	[number]	k4	1954
doc.	doc.	kA	doc.
PhDr.	PhDr.	kA	PhDr.
Otakar	Otakar	k1gMnSc1	Otakar
Vašek	Vašek	k1gMnSc1	Vašek
</s>
</p>
<p>
<s>
1954	[number]	k4	1954
<g/>
–	–	k?	–
<g/>
1957	[number]	k4	1957
doc.	doc.	kA	doc.
PhDr.	PhDr.	kA	PhDr.
Václav	Václav	k1gMnSc1	Václav
Křístek	Křístek	k1gInSc1	Křístek
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
</s>
</p>
<p>
<s>
1960	[number]	k4	1960
<g/>
–	–	k?	–
<g/>
1961	[number]	k4	1961
prof.	prof.	kA	prof.
PhDr.	PhDr.	kA	PhDr.
Emil	Emil	k1gMnSc1	Emil
Holas	Holas	k1gMnSc1	Holas
<g/>
,	,	kIx,	,
DrSc	DrSc	kA	DrSc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1961	[number]	k4	1961
<g/>
–	–	k?	–
<g/>
1963	[number]	k4	1963
doc.	doc.	kA	doc.
PhDr.	PhDr.	kA	PhDr.
Jaromír	Jaromír	k1gMnSc1	Jaromír
Lang	Lang	k1gMnSc1	Lang
</s>
</p>
<p>
<s>
1963	[number]	k4	1963
<g/>
–	–	k?	–
<g/>
1964	[number]	k4	1964
doc.	doc.	kA	doc.
PhDr.	PhDr.	kA	PhDr.
Antonín	Antonín	k1gMnSc1	Antonín
Václavík	Václavík	k1gMnSc1	Václavík
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
</s>
</p>
<p>
<s>
1964	[number]	k4	1964
<g/>
–	–	k?	–
<g/>
1966	[number]	k4	1966
doc.	doc.	kA	doc.
PhDr.	PhDr.	kA	PhDr.
Josef	Josef	k1gMnSc1	Josef
Bieberle	Bieberl	k1gMnSc2	Bieberl
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
</s>
</p>
<p>
<s>
1966	[number]	k4	1966
<g/>
–	–	k?	–
<g/>
1969	[number]	k4	1969
prof.	prof.	kA	prof.
PhDr.	PhDr.	kA	PhDr.
Jiří	Jiří	k1gMnSc1	Jiří
Daňhelka	Daňhelka	k1gMnSc1	Daňhelka
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
</s>
</p>
<p>
<s>
1969	[number]	k4	1969
<g/>
–	–	k?	–
<g/>
1971	[number]	k4	1971
prof.	prof.	kA	prof.
PhDr.	PhDr.	kA	PhDr.
Dimitr	Dimitr	k1gInSc1	Dimitr
Krandžalov	Krandžalov	k1gInSc1	Krandžalov
</s>
</p>
<p>
<s>
1971	[number]	k4	1971
<g/>
–	–	k?	–
<g/>
1974	[number]	k4	1974
prof.	prof.	kA	prof.
ak.	ak.	k?	ak.
arch	arch	k1gInSc1	arch
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Novák	Novák	k1gMnSc1	Novák
</s>
</p>
<p>
<s>
1974	[number]	k4	1974
<g/>
–	–	k?	–
<g/>
1986	[number]	k4	1986
prof.	prof.	kA	prof.
PhDr.	PhDr.	kA	PhDr.
Jan	Jan	k1gMnSc1	Jan
Navrátil	Navrátil	k1gMnSc1	Navrátil
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
</s>
</p>
<p>
<s>
1986	[number]	k4	1986
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
doc.	doc.	kA	doc.
PhDr.	PhDr.	kA	PhDr.
Karel	Karel	k1gMnSc1	Karel
Motyka	motyka	k1gFnSc1	motyka
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
prof.	prof.	kA	prof.
PhDr.	PhDr.	kA	PhDr.
Ludvík	Ludvík	k1gMnSc1	Ludvík
Václavek	Václavek	k1gMnSc1	Václavek
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
<g/>
–	–	k?	–
<g/>
1997	[number]	k4	1997
doc.	doc.	kA	doc.
PhDr.	PhDr.	kA	PhDr.
Jindřich	Jindřich	k1gMnSc1	Jindřich
Schulz	Schulz	k1gMnSc1	Schulz
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
doc.	doc.	kA	doc.
PhDr.	PhDr.	kA	PhDr.
Vladimír	Vladimír	k1gMnSc1	Vladimír
Řehan	Řehan	k1gMnSc1	Řehan
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
prof.	prof.	kA	prof.
PhDr.	PhDr.	kA	PhDr.
Ivo	Ivo	k1gMnSc1	Ivo
Barteček	Barteček	k1gInSc1	Barteček
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
(	(	kIx(	(
<g/>
dvě	dva	k4xCgNnPc4	dva
období	období	k1gNnPc4	období
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
–	–	k?	–
2018	[number]	k4	2018
prof.	prof.	kA	prof.
PhDr.	PhDr.	kA	PhDr.
Jiří	Jiří	k1gMnSc1	Jiří
Lach	Lach	k1gMnSc1	Lach
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
A.	A.	kA	A.
(	(	kIx(	(
<g/>
dvě	dva	k4xCgNnPc4	dva
období	období	k1gNnPc4	období
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2018	[number]	k4	2018
–	–	k?	–
prof.	prof.	kA	prof.
PhDr.	PhDr.	kA	PhDr.
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Pechal	Pechal	k1gMnSc1	Pechal
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
</s>
</p>
<p>
<s>
===	===	k?	===
Vedení	vedení	k1gNnSc1	vedení
fakulty	fakulta	k1gFnSc2	fakulta
===	===	k?	===
</s>
</p>
<p>
<s>
prof.	prof.	kA	prof.
PhDr.	PhDr.	kA	PhDr.
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Pechal	Pechal	k1gMnSc1	Pechal
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
–	–	k?	–
děkan	děkan	k1gMnSc1	děkan
</s>
</p>
<p>
<s>
prof.	prof.	kA	prof.
Mgr.	Mgr.	kA	Mgr.
Lic	lícit	k5eAaImRp2nS	lícit
<g/>
.	.	kIx.	.
</s>
<s>
Lenka	Lenka	k1gFnSc1	Lenka
Zajícová	Zajícová	k1gFnSc1	Zajícová
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
–	–	k?	–
proděkanka	proděkanka	k1gFnSc1	proděkanka
pro	pro	k7c4	pro
vědu	věda	k1gFnSc4	věda
a	a	k8xC	a
výzkum	výzkum	k1gInSc4	výzkum
</s>
</p>
<p>
<s>
Mgr.	Mgr.	kA	Mgr.
Ondřej	Ondřej	k1gMnSc1	Ondřej
Molnár	Molnár	k1gMnSc1	Molnár
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
–	–	k?	–
proděkan	proděkan	k1gMnSc1	proděkan
pro	pro	k7c4	pro
organizaci	organizace	k1gFnSc4	organizace
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
</s>
</p>
<p>
<s>
doc.	doc.	kA	doc.
Mgr.	Mgr.	kA	Mgr.
Miroslav	Miroslava	k1gFnPc2	Miroslava
Dopita	dopit	k2eAgFnSc1d1	Dopita
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
–	–	k?	–
proděkan	proděkan	k1gMnSc1	proděkan
pro	pro	k7c4	pro
studijní	studijní	k2eAgFnPc4d1	studijní
a	a	k8xC	a
sociální	sociální	k2eAgFnPc4d1	sociální
záležitosti	záležitost	k1gFnPc4	záležitost
</s>
</p>
<p>
<s>
Mgr.	Mgr.	kA	Mgr.
Dana	Dana	k1gFnSc1	Dana
Bilíková	Bilíková	k1gFnSc1	Bilíková
–	–	k?	–
proděkanka	proděkanka	k1gFnSc1	proděkanka
pro	pro	k7c4	pro
vnější	vnější	k2eAgInPc4d1	vnější
vztahy	vztah	k1gInPc4	vztah
a	a	k8xC	a
projektové	projektový	k2eAgNnSc4d1	projektové
řízení	řízení	k1gNnSc4	řízení
</s>
</p>
<p>
<s>
Mgr.	Mgr.	kA	Mgr.
Pavlína	Pavlína	k1gFnSc1	Pavlína
Flajšarová	Flajšarová	k1gFnSc1	Flajšarová
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
–	–	k?	–
proděkanka	proděkanka	k1gFnSc1	proděkanka
pro	pro	k7c4	pro
zahraniční	zahraniční	k2eAgFnPc4d1	zahraniční
záležitosti	záležitost	k1gFnPc4	záležitost
</s>
</p>
<p>
<s>
doc.	doc.	kA	doc.
Mgr.	Mgr.	kA	Mgr.
Jan	Jan	k1gMnSc1	Jan
Stejskal	Stejskal	k1gMnSc1	Stejskal
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
akademického	akademický	k2eAgInSc2d1	akademický
senátu	senát	k1gInSc2	senát
</s>
</p>
<p>
<s>
Ing.	ing.	kA	ing.
Jiřina	Jiřina	k1gFnSc1	Jiřina
Menšíková	Menšíková	k1gFnSc1	Menšíková
–	–	k?	–
tajemník	tajemník	k1gMnSc1	tajemník
</s>
</p>
<p>
<s>
==	==	k?	==
Věda	věda	k1gFnSc1	věda
a	a	k8xC	a
výzkum	výzkum	k1gInSc1	výzkum
==	==	k?	==
</s>
</p>
<p>
<s>
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
vědu	věda	k1gFnSc4	věda
a	a	k8xC	a
výzkum	výzkum	k1gInSc4	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Vědeckou	vědecký	k2eAgFnSc7d1	vědecká
činností	činnost	k1gFnSc7	činnost
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
pedagogové	pedagog	k1gMnPc1	pedagog
a	a	k8xC	a
vědečtí	vědecký	k2eAgMnPc1d1	vědecký
pracovníci	pracovník	k1gMnPc1	pracovník
všech	všecek	k3xTgFnPc2	všecek
kateder	katedra	k1gFnPc2	katedra
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
deseti	deset	k4xCc6	deset
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
jsou	být	k5eAaImIp3nP	být
specializovaná	specializovaný	k2eAgNnPc1d1	specializované
výzkumná	výzkumný	k2eAgNnPc1d1	výzkumné
pracoviště	pracoviště	k1gNnPc1	pracoviště
<g/>
.	.	kIx.	.
</s>
<s>
Významných	významný	k2eAgInPc2d1	významný
vědeckých	vědecký	k2eAgInPc2d1	vědecký
výsledků	výsledek	k1gInPc2	výsledek
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
zejména	zejména	k9	zejména
katedry	katedra	k1gFnPc4	katedra
s	s	k7c7	s
vyhraněným	vyhraněný	k2eAgInSc7d1	vyhraněný
vědeckým	vědecký	k2eAgInSc7d1	vědecký
profilem	profil	k1gInSc7	profil
<g/>
,	,	kIx,	,
zázemím	zázemí	k1gNnSc7	zázemí
významných	významný	k2eAgFnPc2d1	významná
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
osobností	osobnost	k1gFnPc2	osobnost
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
zajištěnou	zajištěný	k2eAgFnSc7d1	zajištěná
výchovou	výchova	k1gFnSc7	výchova
vědeckého	vědecký	k2eAgInSc2d1	vědecký
dorostu	dorost	k1gInSc2	dorost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
filologickými	filologický	k2eAgFnPc7d1	filologická
katedrami	katedra	k1gFnPc7	katedra
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
Katedra	katedra	k1gFnSc1	katedra
anglistiky	anglistika	k1gFnSc2	anglistika
a	a	k8xC	a
amerikanistiky	amerikanistika	k1gFnSc2	amerikanistika
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
badateli	badatel	k1gMnPc7	badatel
šesti	šest	k4xCc2	šest
dalších	další	k2eAgFnPc2d1	další
kateder	katedra	k1gFnPc2	katedra
FF	ff	kA	ff
řešení	řešení	k1gNnSc1	řešení
výzkumného	výzkumný	k2eAgInSc2d1	výzkumný
záměru	záměr	k1gInSc2	záměr
Pluralita	pluralita	k1gFnSc1	pluralita
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Katedra	katedra	k1gFnSc1	katedra
germanistiky	germanistika	k1gFnSc2	germanistika
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
výzkum	výzkum	k1gInSc1	výzkum
německy	německy	k6eAd1	německy
píšících	píšící	k2eAgMnPc2d1	píšící
autorů	autor	k1gMnPc2	autor
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
v	v	k7c6	v
Arbeitsstelle	Arbeitsstella	k1gFnSc6	Arbeitsstella
für	für	k?	für
deutschmährische	deutschmährische	k1gNnSc2	deutschmährische
Literatur	literatura	k1gFnPc2	literatura
a	a	k8xC	a
judaistický	judaistický	k2eAgInSc4d1	judaistický
výzkum	výzkum	k1gInSc4	výzkum
v	v	k7c6	v
Centru	centrum	k1gNnSc6	centrum
judaistických	judaistický	k2eAgFnPc2d1	judaistická
studií	studie	k1gFnPc2	studie
Kurta	kurta	k1gFnSc1	kurta
a	a	k8xC	a
Ursuly	Ursula	k1gFnPc1	Ursula
Schubertových	Schubertová	k1gFnPc2	Schubertová
podporovaný	podporovaný	k2eAgInSc1d1	podporovaný
řadou	řada	k1gFnSc7	řada
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
i	i	k8xC	i
domácích	domácí	k2eAgInPc2d1	domácí
grantů	grant	k1gInPc2	grant
<g/>
.	.	kIx.	.
</s>
<s>
Katedra	katedra	k1gFnSc1	katedra
asijských	asijský	k2eAgFnPc2d1	asijská
studií	studie	k1gFnPc2	studie
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
činnosti	činnost	k1gFnSc6	činnost
Konfuciovy	Konfuciův	k2eAgFnSc2d1	Konfuciova
akademie	akademie	k1gFnSc2	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Katedra	katedra	k1gFnSc1	katedra
bohemistiky	bohemistika	k1gFnSc2	bohemistika
zahájila	zahájit	k5eAaPmAgFnS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
vydávání	vydávání	k1gNnSc4	vydávání
recenzovaného	recenzovaný	k2eAgInSc2d1	recenzovaný
časopisu	časopis	k1gInSc2	časopis
Bohemica	Bohemicus	k1gMnSc4	Bohemicus
Olomucensia	Olomucensius	k1gMnSc4	Olomucensius
a	a	k8xC	a
také	také	k9	také
každoročně	každoročně	k6eAd1	každoročně
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
pro	pro	k7c4	pro
FF	ff	kA	ff
soutěž	soutěž	k1gFnSc1	soutěž
Student	student	k1gMnSc1	student
a	a	k8xC	a
věda	věda	k1gFnSc1	věda
<g/>
.	.	kIx.	.
</s>
<s>
Katedra	katedra	k1gFnSc1	katedra
romanistiky	romanistika	k1gFnSc2	romanistika
vydává	vydávat	k5eAaPmIp3nS	vydávat
odborný	odborný	k2eAgInSc1d1	odborný
časopis	časopis	k1gInSc1	časopis
Romanica	Romanicus	k1gMnSc2	Romanicus
Olomucensia	Olomucensius	k1gMnSc2	Olomucensius
<g/>
.	.	kIx.	.
</s>
<s>
Katedra	katedra	k1gFnSc1	katedra
slavistiky	slavistika	k1gFnSc2	slavistika
pravidelně	pravidelně	k6eAd1	pravidelně
pořádá	pořádat	k5eAaImIp3nS	pořádat
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
konferenci	konference	k1gFnSc4	konference
"	"	kIx"	"
<g/>
Olomoucké	olomoucký	k2eAgInPc1d1	olomoucký
dny	den	k1gInPc1	den
rusistů	rusista	k1gMnPc2	rusista
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Katedra	katedra	k1gFnSc1	katedra
nederlandistiky	nederlandistika	k1gFnSc2	nederlandistika
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
svou	svůj	k3xOyFgFnSc4	svůj
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
činnost	činnost	k1gFnSc4	činnost
v	v	k7c6	v
Centru	centrum	k1gNnSc6	centrum
pro	pro	k7c4	pro
jazyk	jazyk	k1gInSc4	jazyk
a	a	k8xC	a
kulturu	kultura	k1gFnSc4	kultura
Nizozemí	Nizozemí	k1gNnSc2	Nizozemí
a	a	k8xC	a
Vlámska	Vlámsko	k1gNnSc2	Vlámsko
–	–	k?	–
Erasmianum	Erasmianum	k1gInSc4	Erasmianum
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
evropského	evropský	k2eAgInSc2d1	evropský
projektu	projekt	k1gInSc2	projekt
NIFLAR	NIFLAR	kA	NIFLAR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
historie	historie	k1gFnSc2	historie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
ukončila	ukončit	k5eAaPmAgFnS	ukončit
dlouhodobý	dlouhodobý	k2eAgInSc4d1	dlouhodobý
projekt	projekt	k1gInSc4	projekt
Akademické	akademický	k2eAgNnSc4d1	akademické
dějiny	dějiny	k1gFnPc4	dějiny
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
řešen	řešit	k5eAaImNgInS	řešit
grant	grant	k1gInSc1	grant
GA	GA	kA	GA
ČR	ČR	kA	ČR
Soupis	soupis	k1gInSc1	soupis
písemných	písemný	k2eAgFnPc2d1	písemná
památek	památka	k1gFnPc2	památka
městského	městský	k2eAgNnSc2d1	Městské
dějepisectví	dějepisectví	k1gNnSc2	dějepisectví
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
(	(	kIx(	(
<g/>
rakouského	rakouský	k2eAgNnSc2d1	rakouské
<g/>
)	)	kIx)	)
Slezska	Slezsko	k1gNnSc2	Slezsko
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1800	[number]	k4	1800
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
první	první	k4xOgFnSc1	první
etapa	etapa	k1gFnSc1	etapa
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Katedra	katedra	k1gFnSc1	katedra
dějin	dějiny	k1gFnPc2	dějiny
umění	umění	k1gNnSc4	umění
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Katedrou	katedra	k1gFnSc7	katedra
muzikologie	muzikologie	k1gFnSc2	muzikologie
<g/>
,	,	kIx,	,
Katedrou	katedra	k1gFnSc7	katedra
divadelních	divadelní	k2eAgInPc2d1	divadelní
<g/>
,	,	kIx,	,
filmových	filmový	k2eAgNnPc2d1	filmové
a	a	k8xC	a
mediálních	mediální	k2eAgNnPc2d1	mediální
studií	studio	k1gNnPc2	studio
<g/>
,	,	kIx,	,
Katedrou	katedra	k1gFnSc7	katedra
germanistiky	germanistika	k1gFnSc2	germanistika
<g/>
,	,	kIx,	,
Katedrou	katedra	k1gFnSc7	katedra
historie	historie	k1gFnSc2	historie
a	a	k8xC	a
Katedrou	katedra	k1gFnSc7	katedra
bohemistiky	bohemistika	k1gFnSc2	bohemistika
řeší	řešit	k5eAaImIp3nS	řešit
výzkumný	výzkumný	k2eAgInSc1d1	výzkumný
záměr	záměr	k1gInSc1	záměr
Morava	Morava	k1gFnSc1	Morava
a	a	k8xC	a
svět	svět	k1gInSc1	svět
<g/>
:	:	kIx,	:
Umění	umění	k1gNnSc1	umění
v	v	k7c6	v
otevřeném	otevřený	k2eAgInSc6d1	otevřený
multikulturním	multikulturní	k2eAgInSc6d1	multikulturní
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
realizovaný	realizovaný	k2eAgInSc1d1	realizovaný
v	v	k7c6	v
Centru	centrum	k1gNnSc6	centrum
italských	italský	k2eAgFnPc2d1	italská
studií	studie	k1gFnPc2	studie
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
a	a	k8xC	a
kultuře	kultura	k1gFnSc6	kultura
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
pod	pod	k7c4	pod
Katedru	katedra	k1gFnSc4	katedra
dějin	dějiny	k1gFnPc2	dějiny
umění	umění	k1gNnSc2	umění
spadá	spadat	k5eAaImIp3nS	spadat
<g/>
.	.	kIx.	.
</s>
<s>
Katedra	katedra	k1gFnSc1	katedra
filozofie	filozofie	k1gFnSc1	filozofie
je	být	k5eAaImIp3nS	být
zapojena	zapojit	k5eAaPmNgFnS	zapojit
do	do	k7c2	do
výzkumného	výzkumný	k2eAgInSc2d1	výzkumný
záměru	záměr	k1gInSc2	záměr
"	"	kIx"	"
<g/>
Centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
patristickými	patristický	k2eAgInPc7d1	patristický
<g/>
,	,	kIx,	,
středověkými	středověký	k2eAgInPc7d1	středověký
a	a	k8xC	a
renesančními	renesanční	k2eAgInPc7d1	renesanční
texty	text	k1gInPc7	text
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
řešený	řešený	k2eAgInSc1d1	řešený
převážně	převážně	k6eAd1	převážně
na	na	k7c4	na
CMTF	CMTF	kA	CMTF
UP	UP	kA	UP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
společenskými	společenský	k2eAgFnPc7d1	společenská
vědami	věda	k1gFnPc7	věda
se	se	k3xPyFc4	se
Katedra	katedra	k1gFnSc1	katedra
politologie	politologie	k1gFnSc2	politologie
a	a	k8xC	a
evropských	evropský	k2eAgFnPc2d1	Evropská
studií	studie	k1gFnPc2	studie
zabývá	zabývat	k5eAaImIp3nS	zabývat
výzkumem	výzkum	k1gInSc7	výzkum
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
politického	politický	k2eAgNnSc2d1	politické
stranictví	stranictví	k1gNnSc2	stranictví
a	a	k8xC	a
voleb	volba	k1gFnPc2	volba
<g/>
,	,	kIx,	,
politických	politický	k2eAgInPc2d1	politický
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
se	s	k7c7	s
zaměřením	zaměření	k1gNnSc7	zaměření
na	na	k7c4	na
institucionální	institucionální	k2eAgFnSc4d1	institucionální
reformu	reforma	k1gFnSc4	reforma
<g/>
,	,	kIx,	,
regionální	regionální	k2eAgFnSc4d1	regionální
a	a	k8xC	a
environmentální	environmentální	k2eAgFnSc4d1	environmentální
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
rozšiřování	rozšiřování	k1gNnSc4	rozšiřování
a	a	k8xC	a
externí	externí	k2eAgInPc4d1	externí
vztahy	vztah	k1gInPc4	vztah
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Katedra	katedra	k1gFnSc1	katedra
žurnalistiky	žurnalistika	k1gFnSc2	žurnalistika
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
především	především	k6eAd1	především
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
nově	nově	k6eAd1	nově
realizovanými	realizovaný	k2eAgInPc7d1	realizovaný
studijními	studijní	k2eAgInPc7d1	studijní
programy	program	k1gInPc7	program
(	(	kIx(	(
<g/>
mediální	mediální	k2eAgNnPc1d1	mediální
studia	studio	k1gNnPc1	studio
<g/>
,	,	kIx,	,
kulturální	kulturální	k2eAgNnPc1d1	kulturální
studia	studio	k1gNnPc1	studio
a	a	k8xC	a
komunikační	komunikační	k2eAgNnPc1d1	komunikační
studia	studio	k1gNnPc1	studio
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
výzkumné	výzkumný	k2eAgNnSc1d1	výzkumné
Centrum	centrum	k1gNnSc1	centrum
kulturálních	kulturální	k2eAgFnPc2d1	kulturální
<g/>
,	,	kIx,	,
mediálních	mediální	k2eAgFnPc2d1	mediální
a	a	k8xC	a
komunikačních	komunikační	k2eAgFnPc2d1	komunikační
studií	studie	k1gFnPc2	studie
s	s	k7c7	s
novou	nový	k2eAgFnSc7d1	nová
publikační	publikační	k2eAgFnSc7d1	publikační
platformou	platforma	k1gFnSc7	platforma
<g/>
,	,	kIx,	,
recenzovaným	recenzovaný	k2eAgNnSc7d1	recenzované
periodikem	periodikum	k1gNnSc7	periodikum
Kultura	kultura	k1gFnSc1	kultura
-	-	kIx~	-
média	médium	k1gNnPc1	médium
-	-	kIx~	-
komunikace	komunikace	k1gFnSc1	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Katedra	katedra	k1gFnSc1	katedra
psychologie	psychologie	k1gFnSc2	psychologie
pořádá	pořádat	k5eAaImIp3nS	pořádat
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
příbuznými	příbuzný	k1gMnPc7	příbuzný
pracovišti	pracoviště	k1gNnSc6	pracoviště
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
pravidelně	pravidelně	k6eAd1	pravidelně
česko-slovenskou	českolovenský	k2eAgFnSc4d1	česko-slovenská
konferenci	konference	k1gFnSc4	konference
"	"	kIx"	"
<g/>
Kvalitativní	kvalitativní	k2eAgInSc1d1	kvalitativní
přístup	přístup	k1gInSc1	přístup
a	a	k8xC	a
metody	metoda	k1gFnPc1	metoda
ve	v	k7c6	v
vědách	věda	k1gFnPc6	věda
o	o	k7c6	o
člověku	člověk	k1gMnSc6	člověk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
již	již	k6eAd1	již
8	[number]	k4	8
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
Kvalitativní	kvalitativní	k2eAgInSc1d1	kvalitativní
přístup	přístup	k1gInSc1	přístup
pro	pro	k7c4	pro
praxi	praxe	k1gFnSc4	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Katedra	katedra	k1gFnSc1	katedra
aplikované	aplikovaný	k2eAgFnSc2d1	aplikovaná
ekonomie	ekonomie	k1gFnSc2	ekonomie
se	se	k3xPyFc4	se
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
na	na	k7c4	na
problematiku	problematika	k1gFnSc4	problematika
Second	Seconda	k1gFnPc2	Seconda
Life	Lif	k1gFnSc2	Lif
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
ve	v	k7c6	v
virtuálním	virtuální	k2eAgInSc6d1	virtuální
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalšími	další	k2eAgInPc7d1	další
aktivními	aktivní	k2eAgInPc7d1	aktivní
vědeckými	vědecký	k2eAgInPc7d1	vědecký
pracovišti	pracoviště	k1gNnSc6	pracoviště
jsou	být	k5eAaImIp3nP	být
Kabinet	kabinet	k1gInSc4	kabinet
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
dějin	dějiny	k1gFnPc2	dějiny
filozofie	filozofie	k1gFnSc1	filozofie
středověku	středověk	k1gInSc2	středověk
a	a	k8xC	a
renesance	renesance	k1gFnSc2	renesance
<g/>
,	,	kIx,	,
Dokumentační	dokumentační	k2eAgNnSc1d1	dokumentační
centrum	centrum	k1gNnSc1	centrum
dramatických	dramatický	k2eAgNnPc2d1	dramatické
umění	umění	k1gNnPc2	umění
<g/>
,	,	kIx,	,
Kabinet	kabinet	k1gInSc1	kabinet
regionálních	regionální	k2eAgFnPc2d1	regionální
dějin	dějiny	k1gFnPc2	dějiny
<g/>
,	,	kIx,	,
Kabinet	kabinet	k1gInSc1	kabinet
interkulturních	interkulturní	k2eAgFnPc2d1	interkulturní
studií	studie	k1gFnPc2	studie
a	a	k8xC	a
Centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
československá	československý	k2eAgNnPc4d1	Československé
exilová	exilový	k2eAgNnPc4d1	exilové
studia	studio	k1gNnPc4	studio
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Studentské	studentský	k2eAgFnPc1d1	studentská
aktivity	aktivita	k1gFnPc1	aktivita
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
Filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
UP	UP	kA	UP
funguje	fungovat	k5eAaImIp3nS	fungovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
desítka	desítka	k1gFnSc1	desítka
studentských	studentský	k2eAgMnPc2d1	studentský
spolků	spolek	k1gInPc2	spolek
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
spolky	spolek	k1gInPc1	spolek
buď	buď	k8xC	buď
přímo	přímo	k6eAd1	přímo
pracují	pracovat	k5eAaImIp3nP	pracovat
pod	pod	k7c7	pod
záštitou	záštita	k1gFnSc7	záštita
a	a	k8xC	a
za	za	k7c4	za
podpory	podpora	k1gFnPc4	podpora
Filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
fakultu	fakulta	k1gFnSc4	fakulta
vázány	vázat	k5eAaImNgFnP	vázat
personálně	personálně	k6eAd1	personálně
<g/>
.	.	kIx.	.
</s>
<s>
Zaměření	zaměření	k1gNnSc1	zaměření
spolků	spolek	k1gInPc2	spolek
je	být	k5eAaImIp3nS	být
různé	různý	k2eAgInPc4d1	různý
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
se	se	k3xPyFc4	se
věnují	věnovat	k5eAaImIp3nP	věnovat
realizaci	realizace	k1gFnSc4	realizace
kulturního	kulturní	k2eAgInSc2d1	kulturní
programu	program	k1gInSc2	program
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
Uměleckého	umělecký	k2eAgMnSc2d1	umělecký
centra	centr	k1gMnSc2	centr
Univerzity	univerzita	k1gFnSc2	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
občanské	občanský	k2eAgNnSc1d1	občanské
sdružení	sdružení	k1gNnSc1	sdružení
Pastiche	Pastich	k1gFnSc2	Pastich
filmz	filmza	k1gFnPc2	filmza
nebo	nebo	k8xC	nebo
Divadlo	divadlo	k1gNnSc1	divadlo
Konvikt	konvikt	k1gInSc1	konvikt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Studenti	student	k1gMnPc1	student
FF	ff	kA	ff
UP	UP	kA	UP
ale	ale	k8xC	ale
i	i	k9	i
jeho	jeho	k3xOp3gMnPc1	jeho
absolventi	absolvent	k1gMnPc1	absolvent
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
starají	starat	k5eAaImIp3nP	starat
o	o	k7c4	o
chod	chod	k1gInSc4	chod
nezávislého	závislý	k2eNgNnSc2d1	nezávislé
olomouckého	olomoucký	k2eAgNnSc2d1	olomoucké
Divadla	divadlo	k1gNnSc2	divadlo
Tramtarie	Tramtarie	k1gFnSc2	Tramtarie
<g/>
,	,	kIx,	,
studenti	student	k1gMnPc1	student
divadelní	divadelní	k2eAgFnSc2d1	divadelní
vědy	věda	k1gFnSc2	věda
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
dva	dva	k4xCgInPc4	dva
studentské	studentský	k2eAgInPc4d1	studentský
divadelní	divadelní	k2eAgInPc4d1	divadelní
soubory	soubor	k1gInPc4	soubor
–	–	k?	–
Divadlo	divadlo	k1gNnSc1	divadlo
na	na	k7c4	na
cucky	cucek	k1gInPc4	cucek
a	a	k8xC	a
Ansámbl	ansámbl	k1gInSc4	ansámbl
OZ	OZ	kA	OZ
<g/>
.	.	kIx.	.
</s>
<s>
Studenti	student	k1gMnPc1	student
filmové	filmový	k2eAgFnSc2d1	filmová
vědy	věda	k1gFnSc2	věda
před	před	k7c7	před
několika	několik	k4yIc7	několik
roky	rok	k1gInPc7	rok
založili	založit	k5eAaPmAgMnP	založit
internetový	internetový	k2eAgInSc4d1	internetový
studentský	studentský	k2eAgInSc4d1	studentský
filmový	filmový	k2eAgInSc4d1	filmový
časopis	časopis	k1gInSc4	časopis
25	[number]	k4	25
<g/>
fps	fps	k?	fps
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
filmu	film	k1gInSc2	film
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc7d3	veliký
doménou	doména	k1gFnSc7	doména
FF	ff	kA	ff
UP	UP	kA	UP
organizace	organizace	k1gFnSc2	organizace
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
festivalu	festival	k1gInSc2	festival
populárně-vědeckého	populárněědecký	k2eAgInSc2d1	populárně-vědecký
a	a	k8xC	a
dokumentárního	dokumentární	k2eAgInSc2d1	dokumentární
filmu	film	k1gInSc2	film
Academia	academia	k1gFnSc1	academia
film	film	k1gInSc1	film
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gFnSc6	jehož
organizaci	organizace	k1gFnSc6	organizace
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
jak	jak	k8xC	jak
učitelé	učitel	k1gMnPc1	učitel
tak	tak	k6eAd1	tak
každoročně	každoročně	k6eAd1	každoročně
přibližně	přibližně	k6eAd1	přibližně
sto	sto	k4xCgNnSc1	sto
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Popularizaci	popularizace	k1gFnSc4	popularizace
Japonska	Japonsko	k1gNnSc2	Japonsko
i	i	k8xC	i
jeho	jeho	k3xOp3gFnSc2	jeho
kultury	kultura	k1gFnSc2	kultura
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
Japonský	japonský	k2eAgInSc1d1	japonský
klub	klub	k1gInSc1	klub
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Kulturní	kulturní	k2eAgFnPc1d1	kulturní
<g/>
,	,	kIx,	,
sportovní	sportovní	k2eAgFnPc1d1	sportovní
a	a	k8xC	a
společenské	společenský	k2eAgFnPc1d1	společenská
akce	akce	k1gFnPc1	akce
pro	pro	k7c4	pro
studenty	student	k1gMnPc4	student
Katedry	katedra	k1gFnSc2	katedra
historie	historie	k1gFnSc2	historie
a	a	k8xC	a
jiné	jiný	k2eAgMnPc4d1	jiný
zájemce	zájemce	k1gMnPc4	zájemce
pořádá	pořádat	k5eAaImIp3nS	pořádat
Cech	cech	k1gInSc1	cech
invalidů	invalid	k1gMnPc2	invalid
středoevropské	středoevropský	k2eAgFnSc2d1	středoevropská
historiografie	historiografie	k1gFnSc2	historiografie
<g/>
.	.	kIx.	.
</s>
<s>
Aktivita	aktivita	k1gFnSc1	aktivita
s	s	k7c7	s
názvem	název	k1gInSc7	název
Pomáháme	pomáhat	k5eAaImIp1nP	pomáhat
pomáhat	pomáhat	k5eAaImF	pomáhat
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
podporovat	podporovat	k5eAaImF	podporovat
dárcovství	dárcovství	k1gNnSc4	dárcovství
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
rozšíření	rozšíření	k1gNnSc2	rozšíření
znalostí	znalost	k1gFnPc2	znalost
základů	základ	k1gInPc2	základ
první	první	k4xOgFnSc2	první
pomoci	pomoc	k1gFnSc2	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Rozvojem	rozvoj	k1gInSc7	rozvoj
spolupráce	spolupráce	k1gFnSc2	spolupráce
kateder	katedra	k1gFnPc2	katedra
psychologie	psychologie	k1gFnSc2	psychologie
a	a	k8xC	a
zlepšením	zlepšení	k1gNnSc7	zlepšení
podmínek	podmínka	k1gFnPc2	podmínka
studia	studio	k1gNnSc2	studio
se	se	k3xPyFc4	se
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
profesionálními	profesionální	k2eAgFnPc7d1	profesionální
psychologickými	psychologický	k2eAgFnPc7d1	psychologická
organizacemi	organizace	k1gFnPc7	organizace
zabývá	zabývat	k5eAaImIp3nS	zabývat
Česká	český	k2eAgFnSc1d1	Česká
asociace	asociace	k1gFnSc1	asociace
studentů	student	k1gMnPc2	student
psychologie	psychologie	k1gFnSc2	psychologie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
FF	ff	kA	ff
UP	UP	kA	UP
také	také	k9	také
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
občanské	občanský	k2eAgNnSc1d1	občanské
sdružení	sdružení	k1gNnSc1	sdružení
Studenti	student	k1gMnPc1	student
UP	UP	kA	UP
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
podporovat	podporovat	k5eAaImF	podporovat
a	a	k8xC	a
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
aktivity	aktivita	k1gFnPc4	aktivita
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
dnešního	dnešní	k2eAgNnSc2d1	dnešní
univerzitního	univerzitní	k2eAgNnSc2d1	univerzitní
prostředí	prostředí	k1gNnSc2	prostředí
je	být	k5eAaImIp3nS	být
existence	existence	k1gFnSc1	existence
olomouckých	olomoucký	k2eAgFnPc2d1	olomoucká
poboček	pobočka	k1gFnPc2	pobočka
Erasmus	Erasmus	k1gMnSc1	Erasmus
Student	student	k1gMnSc1	student
Network	network	k1gInSc1	network
a	a	k8xC	a
AIESEC	AIESEC	kA	AIESEC
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
fakulty	fakulta	k1gFnSc2	fakulta
</s>
</p>
