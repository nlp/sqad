<s>
Odkaz	odkaz	k1gInSc1	odkaz
Dračích	dračí	k2eAgMnPc2d1	dračí
jezdců	jezdec	k1gMnPc2	jezdec
(	(	kIx(	(
<g/>
původní	původní	k2eAgInSc1d1	původní
název	název	k1gInSc1	název
Inheritance	inheritance	k1gFnSc2	inheritance
Cycle	Cycle	k1gFnSc2	Cycle
<g/>
)	)	kIx)	)
je	on	k3xPp3gInPc4	on
fantasy	fantas	k1gInPc4	fantas
tetralogie	tetralogie	k1gFnSc2	tetralogie
amerického	americký	k2eAgMnSc2d1	americký
spisovatele	spisovatel	k1gMnSc2	spisovatel
Christophera	Christopher	k1gMnSc2	Christopher
Paoliniho	Paolini	k1gMnSc2	Paolini
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
ve	v	k7c6	v
fiktivním	fiktivní	k2eAgInSc6d1	fiktivní
světě	svět	k1gInSc6	svět
Alagaësia	Alagaësium	k1gNnSc2	Alagaësium
a	a	k8xC	a
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
mladém	mladý	k2eAgMnSc6d1	mladý
chlapci	chlapec	k1gMnSc6	chlapec
Eragonovi	Eragon	k1gMnSc6	Eragon
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
dračici	dračice	k1gFnSc3	dračice
Safiře	Safiř	k1gFnSc2	Safiř
<g/>
.	.	kIx.	.
</s>
<s>
Dračí	dračí	k2eAgMnSc1d1	dračí
jezdec	jezdec	k1gMnSc1	jezdec
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
příbězích	příběh	k1gInPc6	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
vlastního	vlastní	k2eAgMnSc4d1	vlastní
draka	drak	k1gMnSc4	drak
<g/>
,	,	kIx,	,
umí	umět	k5eAaImIp3nS	umět
kouzlit	kouzlit	k5eAaImF	kouzlit
a	a	k8xC	a
dokáže	dokázat	k5eAaPmIp3nS	dokázat
se	se	k3xPyFc4	se
myslí	mysl	k1gFnSc7	mysl
spojit	spojit	k5eAaPmF	spojit
s	s	k7c7	s
jakýmkoli	jakýkoli	k3yIgMnSc7	jakýkoli
jiným	jiný	k2eAgMnSc7d1	jiný
živým	živý	k1gMnSc7	živý
tvorem	tvor	k1gMnSc7	tvor
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
by	by	kYmCp3nS	by
ovládat	ovládat	k5eAaImF	ovládat
starověký	starověký	k2eAgInSc4d1	starověký
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Eragon	Eragon	k1gNnSc1	Eragon
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
a	a	k8xC	a
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Eldest	Eldest	k1gFnSc1	Eldest
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Brisingr	Brisingra	k1gFnPc2	Brisingra
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Inheritance	inheritance	k1gFnSc2	inheritance
(	(	kIx(	(
<g/>
8.11	[number]	k4	8.11
<g/>
.2011	.2011	k4	.2011
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
30.3	[number]	k4	30.3
<g/>
.2012	.2012	k4	.2012
<g/>
)	)	kIx)	)
Poslední	poslední	k2eAgInSc4d1	poslední
díl	díl	k1gInSc4	díl
série	série	k1gFnSc1	série
Odkaz	odkaz	k1gInSc1	odkaz
Dračích	dračí	k2eAgMnPc2d1	dračí
jezdců	jezdec	k1gMnPc2	jezdec
vyšel	vyjít	k5eAaPmAgInS	vyjít
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
má	mít	k5eAaImIp3nS	mít
zelenou	zelený	k2eAgFnSc4d1	zelená
barvu	barva	k1gFnSc4	barva
obálky	obálka	k1gFnSc2	obálka
a	a	k8xC	a
anglický	anglický	k2eAgInSc1d1	anglický
název	název	k1gInSc1	název
i	i	k8xC	i
český	český	k2eAgInSc1d1	český
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
Inheritance	inheritance	k1gFnPc4	inheritance
<g/>
.	.	kIx.	.
</s>
<s>
Paolini	Paolin	k2eAgMnPc1d1	Paolin
začal	začít	k5eAaPmAgInS	začít
knihu	kniha	k1gFnSc4	kniha
psát	psát	k5eAaImF	psát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
jeho	jeho	k3xOp3gMnPc2	jeho
rodičů	rodič	k1gMnPc2	rodič
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
upozornil	upozornit	k5eAaPmAgMnS	upozornit
spisovatel	spisovatel	k1gMnSc1	spisovatel
Carl	Carl	k1gMnSc1	Carl
Hiaasen	Hiaasen	k2eAgMnSc1d1	Hiaasen
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
nevlastní	vlastní	k2eNgMnSc1d1	nevlastní
syn	syn	k1gMnSc1	syn
si	se	k3xPyFc3	se
Eragona	Eragona	k1gFnSc1	Eragona
oblíbil	oblíbit	k5eAaPmAgMnS	oblíbit
<g/>
,	,	kIx,	,
na	na	k7c4	na
knihu	kniha	k1gFnSc4	kniha
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
Alfred	Alfred	k1gMnSc1	Alfred
A.	A.	kA	A.
Knopf	Knopf	k1gMnSc1	Knopf
<g/>
.	.	kIx.	.
</s>
<s>
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
získalo	získat	k5eAaPmAgNnS	získat
práva	právo	k1gNnPc4	právo
k	k	k7c3	k
vydávání	vydávání	k1gNnSc3	vydávání
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
úpravám	úprava	k1gFnPc3	úprava
knihy	kniha	k1gFnSc2	kniha
i	i	k8xC	i
jeho	jeho	k3xOp3gInSc2	jeho
obalu	obal	k1gInSc2	obal
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
John	John	k1gMnSc1	John
Jude	Jude	k1gFnPc2	Jude
Palencar	Palencar	k1gMnSc1	Palencar
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nás	my	k3xPp1nPc2	my
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc1	tento
knihy	kniha	k1gFnPc1	kniha
vydávány	vydáván	k2eAgFnPc1d1	vydávána
nakladatelstvím	nakladatelství	k1gNnSc7	nakladatelství
Fragment	fragment	k1gInSc1	fragment
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2006	[number]	k4	2006
přišla	přijít	k5eAaPmAgFnS	přijít
do	do	k7c2	do
českých	český	k2eAgNnPc2d1	české
kin	kino	k1gNnPc2	kino
filmová	filmový	k2eAgFnSc1d1	filmová
podoba	podoba	k1gFnSc1	podoba
Eragona	Eragona	k1gFnSc1	Eragona
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hráli	hrát	k5eAaImAgMnP	hrát
herci	herec	k1gMnPc1	herec
Edward	Edward	k1gMnSc1	Edward
Speleers	Speleers	k1gInSc1	Speleers
<g/>
,	,	kIx,	,
Jeremy	Jerem	k1gInPc1	Jerem
Irons	Irons	k1gInSc1	Irons
a	a	k8xC	a
John	John	k1gMnSc1	John
Malkovich	Malkovich	k1gMnSc1	Malkovich
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
film	film	k1gInSc4	film
Eragona	Eragon	k1gMnSc2	Eragon
režíroval	režírovat	k5eAaImAgMnS	režírovat
Stefen	Stefen	k2eAgMnSc1d1	Stefen
Fangmeier	Fangmeier	k1gMnSc1	Fangmeier
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc4	film
vyrobila	vyrobit	k5eAaPmAgFnS	vyrobit
společnost	společnost	k1gFnSc1	společnost
20	[number]	k4	20
<g/>
th	th	k?	th
Century	Centura	k1gFnSc2	Centura
Fox	fox	k1gInSc1	fox
<g/>
.	.	kIx.	.
</s>
<s>
Hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
filmu	film	k1gInSc3	film
napsal	napsat	k5eAaPmAgMnS	napsat
Patrick	Patrick	k1gMnSc1	Patrick
Doyle	Doyle	k1gFnSc2	Doyle
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgMnPc4d1	jiný
tvůrce	tvůrce	k1gMnPc4	tvůrce
hudby	hudba	k1gFnSc2	hudba
ke	k	k7c3	k
čtvrtému	čtvrtý	k4xOgInSc3	čtvrtý
dílu	díl	k1gInSc3	díl
Harryho	Harry	k1gMnSc2	Harry
Pottera	Potter	k1gMnSc2	Potter
<g/>
,	,	kIx,	,
úvodní	úvodní	k2eAgFnSc1d1	úvodní
píseň	píseň	k1gFnSc1	píseň
nazpívala	nazpívat	k5eAaBmAgFnS	nazpívat
Avril	Avril	k1gInSc4	Avril
Lavigne	Lavign	k1gInSc5	Lavign
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
záběrů	záběr	k1gInPc2	záběr
byla	být	k5eAaImAgFnS	být
pořízena	pořídit	k5eAaPmNgFnS	pořídit
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
a	a	k8xC	a
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
sice	sice	k8xC	sice
vydělal	vydělat	k5eAaPmAgInS	vydělat
249	[number]	k4	249
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
setkal	setkat	k5eAaPmAgMnS	setkat
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
s	s	k7c7	s
negativními	negativní	k2eAgInPc7d1	negativní
ohlasy	ohlas	k1gInPc7	ohlas
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
DVD	DVD	kA	DVD
s	s	k7c7	s
filmem	film	k1gInSc7	film
Eragon	Eragona	k1gFnPc2	Eragona
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Paolini	Paolin	k2eAgMnPc1d1	Paolin
stvořil	stvořit	k5eAaPmAgMnS	stvořit
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
svých	svůj	k3xOyFgFnPc2	svůj
knih	kniha	k1gFnPc2	kniha
3	[number]	k4	3
různé	různý	k2eAgInPc4d1	různý
jazyky	jazyk	k1gInPc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Urgalština	Urgalština	k1gFnSc1	Urgalština
(	(	kIx(	(
<g/>
jazyk	jazyk	k1gInSc1	jazyk
urgalů	urgal	k1gMnPc2	urgal
<g/>
,	,	kIx,	,
válečné	válečný	k2eAgFnPc1d1	válečná
rasy	rasa	k1gFnPc1	rasa
vzdáleně	vzdáleně	k6eAd1	vzdáleně
podobné	podobný	k2eAgFnPc1d1	podobná
lidem	lid	k1gInSc7	lid
)	)	kIx)	)
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
trilogii	trilogie	k1gFnSc6	trilogie
moc	moc	k6eAd1	moc
často	často	k6eAd1	často
používaná	používaný	k2eAgFnSc1d1	používaná
<g/>
,	,	kIx,	,
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
jen	jen	k9	jen
pár	pár	k4xCyI	pár
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Značně	značně	k6eAd1	značně
používanější	používaný	k2eAgFnSc1d2	používanější
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
knihách	kniha	k1gFnPc6	kniha
jazyk	jazyk	k1gInSc1	jazyk
trpaslíků	trpaslík	k1gMnPc2	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Nejpropracovanějším	propracovaný	k2eAgInSc7d3	nejpropracovanější
smyšleným	smyšlený	k2eAgInSc7d1	smyšlený
jazykem	jazyk	k1gInSc7	jazyk
trilogie	trilogie	k1gFnSc2	trilogie
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
Starověký	starověký	k2eAgInSc1d1	starověký
jazyk	jazyk	k1gInSc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
trilogie	trilogie	k1gFnSc2	trilogie
díky	díky	k7c3	díky
mocnému	mocný	k2eAgNnSc3d1	mocné
kouzlu	kouzlo	k1gNnSc3	kouzlo
dávné	dávný	k2eAgFnSc2d1	dávná
rasy	rasa	k1gFnSc2	rasa
není	být	k5eNaImIp3nS	být
pouze	pouze	k6eAd1	pouze
pozemským	pozemský	k2eAgInSc7d1	pozemský
jazykem	jazyk	k1gInSc7	jazyk
smyšleným	smyšlený	k2eAgInSc7d1	smyšlený
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Popisuje	popisovat	k5eAaImIp3nS	popisovat
pravou	pravý	k2eAgFnSc4d1	pravá
podstatu	podstata	k1gFnSc4	podstata
věci	věc	k1gFnSc2	věc
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
osoba	osoba	k1gFnSc1	osoba
zná	znát	k5eAaImIp3nS	znát
slovo	slovo	k1gNnSc4	slovo
<g/>
,	,	kIx,	,
naprosto	naprosto	k6eAd1	naprosto
ovládá	ovládat	k5eAaImIp3nS	ovládat
danou	daný	k2eAgFnSc4d1	daná
věc	věc	k1gFnSc4	věc
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgNnSc2	ten
využívají	využívat	k5eAaImIp3nP	využívat
Paoliniho	Paoliniha	k1gFnSc5	Paoliniha
elfové	elf	k1gMnPc1	elf
<g/>
,	,	kIx,	,
dračí	dračí	k2eAgMnPc1d1	dračí
jezdci	jezdec	k1gMnPc1	jezdec
aj.	aj.	kA	aj.
ke	k	k7c3	k
kouzlení	kouzlení	k1gNnSc3	kouzlení
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
knihy	kniha	k1gFnSc2	kniha
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
výjimečný	výjimečný	k2eAgInSc4d1	výjimečný
jazyk	jazyk	k1gInSc4	jazyk
stínové	stínový	k2eAgFnPc1d1	stínová
<g/>
,	,	kIx,	,
černokněžníci	černokněžník	k1gMnPc1	černokněžník
ovládaní	ovládaný	k2eAgMnPc1d1	ovládaný
zlými	zlý	k2eAgMnPc7d1	zlý
duchy	duch	k1gMnPc7	duch
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
ovšem	ovšem	k9	ovšem
neobjevuje	objevovat	k5eNaImIp3nS	objevovat
žádná	žádný	k3yNgFnSc1	žádný
citace	citace	k1gFnSc1	citace
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
zvaném	zvaný	k2eAgInSc6d1	zvaný
Alagaësie	Alagaësie	k1gFnPc4	Alagaësie
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
klasické	klasický	k2eAgInPc4d1	klasický
fantasy	fantas	k1gInPc4	fantas
bytosti	bytost	k1gFnSc2	bytost
jako	jako	k8xC	jako
elfové	elf	k1gMnPc1	elf
<g/>
,	,	kIx,	,
draci	drak	k1gMnPc1	drak
<g/>
,	,	kIx,	,
trpaslíci	trpaslík	k1gMnPc1	trpaslík
<g/>
,	,	kIx,	,
urgalové	urgal	k1gMnPc1	urgal
(	(	kIx(	(
<g/>
něco	něco	k6eAd1	něco
na	na	k7c4	na
způsob	způsob	k1gInSc4	způsob
orků	orkus	k1gInPc2	orkus
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
World	World	k1gMnSc1	World
of	of	k?	of
Warcraft	Warcraft	k1gMnSc1	Warcraft
<g/>
)	)	kIx)	)
aj.	aj.	kA	aj.
Na	na	k7c6	na
území	území	k1gNnSc6	území
Alagaësie	Alagaësie	k1gFnSc2	Alagaësie
jsou	být	k5eAaImIp3nP	být
čtyři	čtyři	k4xCgInPc1	čtyři
velké	velký	k2eAgInPc1d1	velký
státy	stát	k1gInPc1	stát
<g/>
:	:	kIx,	:
Království	království	k1gNnSc1	království
–	–	k?	–
vládne	vládnout	k5eAaImIp3nS	vládnout
zde	zde	k6eAd1	zde
formou	forma	k1gFnSc7	forma
teroru	teror	k1gInSc2	teror
Galbatorix	Galbatorix	k1gInSc1	Galbatorix
<g/>
.	.	kIx.	.
</s>
<s>
Surda	Surda	k1gFnSc1	Surda
–	–	k?	–
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
svrhnutí	svrhnutí	k1gNnSc6	svrhnutí
Jezdců	jezdec	k1gInPc2	jezdec
odtrhl	odtrhnout	k5eAaPmAgInS	odtrhnout
od	od	k7c2	od
Království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Vládne	vládnout	k5eAaImIp3nS	vládnout
zde	zde	k6eAd1	zde
král	král	k1gMnSc1	král
Orrin	Orrin	k1gMnSc1	Orrin
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Alagaësie	Alagaësie	k1gFnSc2	Alagaësie
<g/>
.	.	kIx.	.
</s>
<s>
Du	Du	k?	Du
Weldenvarden	Weldenvardno	k1gNnPc2	Weldenvardno
–	–	k?	–
rozlehlý	rozlehlý	k2eAgInSc4d1	rozlehlý
les	les	k1gInSc4	les
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Alagaësie	Alagaësie	k1gFnSc2	Alagaësie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
přebývají	přebývat	k5eAaImIp3nP	přebývat
elfové	elf	k1gMnPc1	elf
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
před	před	k7c7	před
Galbatorixem	Galbatorix	k1gInSc7	Galbatorix
<g/>
.	.	kIx.	.
</s>
<s>
Elfům	elf	k1gMnPc3	elf
vládne	vládnout	k5eAaImIp3nS	vládnout
královna	královna	k1gFnSc1	královna
Islanzádí	Islanzádí	k1gNnSc2	Islanzádí
<g/>
.	.	kIx.	.
</s>
<s>
Beorské	Beorský	k2eAgFnPc1d1	Beorský
hory	hora	k1gFnPc1	hora
–	–	k?	–
Domov	domov	k1gInSc1	domov
společenstva	společenstvo	k1gNnSc2	společenstvo
trpaslíků	trpaslík	k1gMnPc2	trpaslík
a	a	k8xC	a
Město	město	k1gNnSc1	město
trpaslíků	trpaslík	k1gMnPc2	trpaslík
–	–	k?	–
Farthen	Farthno	k1gNnPc2	Farthno
Dû	Dû	k1gMnPc2	Dû
<g/>
.	.	kIx.	.
</s>
<s>
Vládl	vládnout	k5eAaImAgMnS	vládnout
tam	tam	k6eAd1	tam
v	v	k7c6	v
prvním	první	k4xOgMnSc6	první
a	a	k8xC	a
druhém	druhý	k4xOgInSc6	druhý
díle	díl	k1gInSc6	díl
král	král	k1gMnSc1	král
Hrothgar	Hrothgar	k1gMnSc1	Hrothgar
a	a	k8xC	a
dál	daleko	k6eAd2	daleko
král	král	k1gMnSc1	král
Orik	Orik	k1gMnSc1	Orik
<g/>
.	.	kIx.	.
</s>
<s>
Poušť	poušť	k1gFnSc1	poušť
Hadarak	Hadarak	k1gInSc1	Hadarak
-	-	kIx~	-
vyprahlá	vyprahlý	k2eAgFnSc1d1	vyprahlá
poušť	poušť	k1gFnSc1	poušť
na	na	k7c4	na
které	který	k3yIgFnPc4	který
žijí	žít	k5eAaImIp3nP	žít
kočovníci	kočovník	k1gMnPc1	kočovník
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
této	tento	k3xDgFnSc3	tento
zemi	zem	k1gFnSc3	zem
nevládne	vládnout	k5eNaImIp3nS	vládnout
<g/>
.	.	kIx.	.
</s>
<s>
Vroengard	Vroengard	k1gInSc1	Vroengard
-	-	kIx~	-
domov	domov	k1gInSc1	domov
jezdců	jezdec	k1gMnPc2	jezdec
(	(	kIx(	(
<g/>
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
Pevnost	pevnost	k1gFnSc1	pevnost
duší	duše	k1gFnPc2	duše
<g/>
)	)	kIx)	)
Carvahall	Carvahall	k1gInSc1	Carvahall
–	–	k?	–
Malá	malý	k2eAgFnSc1d1	malá
vesnice	vesnice	k1gFnSc1	vesnice
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Dračích	dračí	k2eAgFnPc2d1	dračí
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyrůstal	vyrůstat	k5eAaImAgInS	vyrůstat
Eragon	Eragon	k1gInSc1	Eragon
<g/>
.	.	kIx.	.
</s>
<s>
Dračí	dračí	k2eAgFnPc1d1	dračí
hory	hora	k1gFnPc1	hora
–	–	k?	–
Hřeben	hřeben	k1gInSc1	hřeben
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
táhnoucí	táhnoucí	k2eAgInPc4d1	táhnoucí
se	se	k3xPyFc4	se
podél	podél	k7c2	podél
západního	západní	k2eAgNnSc2d1	západní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
záhadné	záhadný	k2eAgNnSc1d1	záhadné
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
mnozí	mnohý	k2eAgMnPc1d1	mnohý
lidé	člověk	k1gMnPc1	člověk
bojí	bát	k5eAaImIp3nP	bát
vkročit	vkročit	k5eAaPmF	vkročit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Eragon	Eragon	k1gMnSc1	Eragon
nalezl	nalézt	k5eAaBmAgMnS	nalézt
Safiřino	Safiřin	k2eAgNnSc4d1	Safiřino
vejce	vejce	k1gNnSc4	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Dras-Leona	Dras-Leona	k1gFnSc1	Dras-Leona
–	–	k?	–
Velké	velký	k2eAgNnSc4d1	velké
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgNnPc1d1	ležící
na	na	k7c6	na
východním	východní	k2eAgInSc6d1	východní
břehu	břeh	k1gInSc6	břeh
jezera	jezero	k1gNnSc2	jezero
Leona	Leo	k1gMnSc2	Leo
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k6eAd1	poblíž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
hora	hora	k1gFnSc1	hora
Helgrind	Helgrind	k1gInSc1	Helgrind
<g/>
(	(	kIx(	(
<g/>
sídlo	sídlo	k1gNnSc1	sídlo
razaků	razak	k1gInPc2	razak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Farthen	Farthen	k1gInSc1	Farthen
Dû	Dû	k1gFnSc2	Dû
–	–	k?	–
Obrovská	obrovský	k2eAgFnSc1d1	obrovská
<g/>
,	,	kIx,	,
dutá	dutý	k2eAgFnSc1d1	dutá
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
nedobytná	dobytný	k2eNgFnSc1d1	nedobytná
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
trpasličí	trpasličí	k2eAgNnSc1d1	trpasličí
město	město	k1gNnSc1	město
Tronjheim	Tronjheima	k1gFnPc2	Tronjheima
a	a	k8xC	a
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
ukrytí	ukrytý	k2eAgMnPc1d1	ukrytý
Vardenové	Varden	k1gMnPc1	Varden
před	před	k7c7	před
Královstvím	království	k1gNnSc7	království
Gil	Gil	k1gFnSc2	Gil
<g/>
'	'	kIx"	'
<g/>
ead	ead	k?	ead
–	–	k?	–
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
spíše	spíše	k9	spíše
obrovská	obrovský	k2eAgNnPc4d1	obrovské
kasárna	kasárna	k1gNnPc4	kasárna
pro	pro	k7c4	pro
Galbatorixovy	Galbatorixův	k2eAgFnPc4d1	Galbatorixova
jednotky	jednotka	k1gFnPc4	jednotka
nežli	nežli	k8xS	nežli
normální	normální	k2eAgNnSc4d1	normální
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Sem	sem	k6eAd1	sem
byla	být	k5eAaImAgFnS	být
poslána	poslat	k5eAaPmNgFnS	poslat
<g/>
,	,	kIx,	,
vězněna	věznit	k5eAaImNgFnS	věznit
a	a	k8xC	a
mučena	mučit	k5eAaImNgFnS	mučit
elfka	elfka	k6eAd1	elfka
Arya	Ary	k2eAgFnSc1d1	Arya
<g/>
,	,	kIx,	,
když	když	k8xS	když
jí	jíst	k5eAaImIp3nS	jíst
chytil	chytit	k5eAaPmAgMnS	chytit
stín	stín	k1gInSc4	stín
Durza	Durz	k1gMnSc2	Durz
poblíž	poblíž	k7c2	poblíž
elfského	elfský	k2eAgNnSc2d1	elfské
města	město	k1gNnSc2	město
Osilon	Osilon	k1gInSc1	Osilon
<g/>
.	.	kIx.	.
</s>
<s>
Hadarak	Hadarak	k6eAd1	Hadarak
–	–	k?	–
Rozlehlá	rozlehlý	k2eAgFnSc1d1	rozlehlá
poušť	poušť	k1gFnSc1	poušť
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
Alagaësie	Alagaësie	k1gFnSc2	Alagaësie
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
museli	muset	k5eAaImAgMnP	muset
Eragon	Eragon	k1gInSc4	Eragon
s	s	k7c7	s
Murthagem	Murthago	k1gNnSc7	Murthago
přejít	přejít	k5eAaPmF	přejít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
k	k	k7c3	k
Beorským	Beorský	k2eAgFnPc3d1	Beorský
horám	hora	k1gFnPc3	hora
<g/>
.	.	kIx.	.
</s>
<s>
Helgrind	Helgrind	k1gInSc1	Helgrind
–	–	k?	–
Zcela	zcela	k6eAd1	zcela
oblá	oblý	k2eAgFnSc1d1	oblá
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc1d1	ležící
poblíž	poblíž	k7c2	poblíž
města	město	k1gNnSc2	město
Dras-Leony	Dras-Leona	k1gFnSc2	Dras-Leona
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žijí	žít	k5eAaImIp3nP	žít
královi	králův	k2eAgMnPc1d1	králův
posluhovači	posluhovač	k1gMnPc1	posluhovač
Ra	ra	k0	ra
<g/>
'	'	kIx"	'
<g/>
zaci	zac	k1gInSc3	zac
<g/>
.	.	kIx.	.
</s>
<s>
Palancar	Palancar	k1gMnSc1	Palancar
–	–	k?	–
Údolí	údolí	k1gNnPc2	údolí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
leží	ležet	k5eAaImIp3nS	ležet
Carvahall	Carvahall	k1gInSc4	Carvahall
a	a	k8xC	a
Therinsford	Therinsford	k1gInSc4	Therinsford
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
Alagaësie	Alagaësie	k1gFnSc2	Alagaësie
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
ohraničen	ohraničit	k5eAaPmNgMnS	ohraničit
<g/>
:	:	kIx,	:
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
Dračími	dračí	k2eAgFnPc7d1	dračí
horami	hora	k1gFnPc7	hora
a	a	k8xC	a
z	z	k7c2	z
východu	východ	k1gInSc2	východ
řekou	řeka	k1gFnSc7	řeka
Anora	Anoro	k1gNnSc2	Anoro
<g/>
.	.	kIx.	.
</s>
<s>
Teirm	Teirm	k1gInSc1	Teirm
–	–	k?	–
Přístavní	přístavní	k2eAgNnSc4d1	přístavní
město	město	k1gNnSc4	město
na	na	k7c6	na
západě	západ	k1gInSc6	západ
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgInS	setkat
Eragon	Eragon	k1gInSc1	Eragon
a	a	k8xC	a
Brom	brom	k1gInSc1	brom
s	s	k7c7	s
Jeodem	Jeod	k1gMnSc7	Jeod
–	–	k?	–
členem	člen	k1gMnSc7	člen
Vardenů	Varden	k1gInPc2	Varden
<g/>
.	.	kIx.	.
</s>
<s>
Urû	Urû	k?	Urû
<g/>
'	'	kIx"	'
<g/>
baen	baen	k1gMnSc1	baen
–	–	k?	–
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
Galbatorix	Galbatorix	k1gInSc1	Galbatorix
Ceris	Ceris	k1gFnSc2	Ceris
–	–	k?	–
Předsunutá	předsunutý	k2eAgFnSc1d1	předsunutá
hlídka	hlídka	k1gFnSc1	hlídka
elfů	elf	k1gMnPc2	elf
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Edda	Edda	k1gFnSc1	Edda
<g/>
.	.	kIx.	.
</s>
<s>
Ellesméra	Ellesmér	k1gMnSc4	Ellesmér
–	–	k?	–
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
elfů	elf	k1gMnPc2	elf
<g/>
,	,	kIx,	,
schované	schovaný	k2eAgFnPc1d1	schovaná
hluboko	hluboko	k6eAd1	hluboko
v	v	k7c6	v
lese	les	k1gInSc6	les
Du	Du	k?	Du
Weldenvarden	Weldenvardna	k1gFnPc2	Weldenvardna
<g/>
.	.	kIx.	.
</s>
<s>
Gaena	Gaena	k1gFnSc1	Gaena
–	–	k?	–
Řeka	Řek	k1gMnSc2	Řek
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
teče	téct	k5eAaImIp3nS	téct
z	z	k7c2	z
lesa	les	k1gInSc2	les
Du	Du	k?	Du
Weldenvarden	Weldenvardna	k1gFnPc2	Weldenvardna
<g/>
.	.	kIx.	.
</s>
<s>
Sílthrim	Sílthrim	k1gInSc1	Sílthrim
–	–	k?	–
Město	město	k1gNnSc1	město
v	v	k7c6	v
lese	les	k1gInSc6	les
Du	Du	k?	Du
Weldenvarden	Weldenvardna	k1gFnPc2	Weldenvardna
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
též	též	k9	též
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
slyšel	slyšet	k5eAaImAgMnS	slyšet
poprvé	poprvé	k6eAd1	poprvé
Eragon	Eragon	k1gMnSc1	Eragon
elfské	elfský	k2eAgFnSc2d1	elfská
písně	píseň	k1gFnSc2	píseň
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
dokážou	dokázat	k5eAaPmIp3nP	dokázat
člověka	člověk	k1gMnSc4	člověk
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
<g/>
.	.	kIx.	.
</s>
<s>
Aberon	Aberon	k1gMnSc1	Aberon
–	–	k?	–
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Surdy	Surda	k1gFnSc2	Surda
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
Orrin	Orrin	k2eAgInSc1d1	Orrin
<g/>
.	.	kIx.	.
</s>
<s>
Dauth	Dauth	k1gMnSc1	Dauth
–	–	k?	–
Přístavní	přístavní	k2eAgNnSc1d1	přístavní
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
připluje	připlout	k5eAaPmIp3nS	připlout
Roran	Roran	k1gInSc1	Roran
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Carvahallu	Carvahalla	k1gFnSc4	Carvahalla
před	před	k7c7	před
útoky	útok	k1gInPc7	útok
Království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
řád	řád	k1gInSc1	řád
<g/>
,	,	kIx,	,
vyskytující	vyskytující	k2eAgInPc1d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
dílech	díl	k1gInPc6	díl
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
ustanoven	ustanovit	k5eAaPmNgInS	ustanovit
před	před	k7c7	před
dávnými	dávný	k2eAgInPc7d1	dávný
časy	čas	k1gInPc7	čas
po	po	k7c6	po
Du	Du	k?	Du
Fyrn	Fyrn	k1gMnSc1	Fyrn
Skulblaka	Skulblak	k1gMnSc2	Skulblak
(	(	kIx(	(
<g/>
Válce	válec	k1gInPc1	válec
s	s	k7c7	s
draky	drak	k1gInPc7	drak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
jelikož	jelikož	k8xS	jelikož
nějaký	nějaký	k3yIgInSc4	nějaký
podepsaný	podepsaný	k2eAgInSc4d1	podepsaný
papír	papír	k1gInSc4	papír
pro	pro	k7c4	pro
draky	drak	k1gMnPc4	drak
nic	nic	k6eAd1	nic
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
museli	muset	k5eAaImAgMnP	muset
elfové	elf	k1gMnPc1	elf
vymyslet	vymyslet	k5eAaPmF	vymyslet
speciální	speciální	k2eAgNnSc4d1	speciální
zaklínadlo	zaklínadlo	k1gNnSc4	zaklínadlo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
jim	on	k3xPp3gMnPc3	on
trvalo	trvat	k5eAaImAgNnS	trvat
vytvořit	vytvořit	k5eAaPmF	vytvořit
9	[number]	k4	9
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
i	i	k9	i
v	v	k7c6	v
elfském	elfský	k2eAgNnSc6d1	elfské
měřítku	měřítko	k1gNnSc6	měřítko
hodně	hodně	k6eAd1	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Kouzlo	kouzlo	k1gNnSc1	kouzlo
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
každým	každý	k3xTgNnSc7	každý
mládětem	mládě	k1gNnSc7	mládě
a	a	k8xC	a
Jezdcem	jezdec	k1gInSc7	jezdec
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
nezrušitelné	zrušitelný	k2eNgNnSc1d1	nezrušitelné
pouto	pouto	k1gNnSc1	pouto
(	(	kIx(	(
<g/>
pochopitelně	pochopitelně	k6eAd1	pochopitelně
jen	jen	k9	jen
s	s	k7c7	s
mláďaty	mládě	k1gNnPc7	mládě
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
očarovaných	očarovaný	k2eAgNnPc6d1	očarované
vejcích	vejce	k1gNnPc6	vejce
darovaných	darovaný	k2eAgNnPc6d1	darované
draky	drak	k1gInPc1	drak
Jezdcům	jezdec	k1gMnPc3	jezdec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
jim	on	k3xPp3gMnPc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
komunikovat	komunikovat	k5eAaImF	komunikovat
v	v	k7c6	v
myšlenkách	myšlenka	k1gFnPc6	myšlenka
a	a	k8xC	a
spojí	spojit	k5eAaPmIp3nP	spojit
tyto	tento	k3xDgFnPc4	tento
dvě	dva	k4xCgFnPc4	dva
bytosti	bytost	k1gFnPc4	bytost
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
odděleně	odděleně	k6eAd1	odděleně
působí	působit	k5eAaImIp3nS	působit
jen	jen	k9	jen
jako	jako	k9	jako
půlka	půlka	k1gFnSc1	půlka
jedné	jeden	k4xCgFnSc2	jeden
bytosti	bytost	k1gFnSc2	bytost
<g/>
,	,	kIx,	,
jedné	jeden	k4xCgFnSc2	jeden
duše	duše	k1gFnSc2	duše
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byli	být	k5eAaImAgMnP	být
ke	k	k7c3	k
kouzlu	kouzlo	k1gNnSc3	kouzlo
připojeni	připojen	k2eAgMnPc1d1	připojen
i	i	k9	i
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
trpaslíci	trpaslík	k1gMnPc1	trpaslík
<g/>
(	(	kIx(	(
<g/>
právě	právě	k9	právě
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
neobjevil	objevit	k5eNaPmAgInS	objevit
žádný	žádný	k3yNgInSc1	žádný
trpasličí	trpasličí	k2eAgInSc1d1	trpasličí
Jezdec	jezdec	k1gInSc1	jezdec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fungovali	fungovat	k5eAaImAgMnP	fungovat
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
jako	jako	k9	jako
poslové	posel	k1gMnPc1	posel
mezi	mezi	k7c7	mezi
elfy	elf	k1gMnPc7	elf
a	a	k8xC	a
Draky	drak	k1gMnPc7	drak
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
Jezdcem	jezdec	k1gInSc7	jezdec
byl	být	k5eAaImAgInS	být
Eragon	Eragon	k1gInSc4	Eragon
(	(	kIx(	(
<g/>
pozor	pozor	k1gInSc4	pozor
<g/>
,	,	kIx,	,
nezaměňovat	zaměňovat	k5eNaImF	zaměňovat
s	s	k7c7	s
hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
odkazu	odkaz	k1gInSc2	odkaz
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
Eragon	Eragon	k1gInSc4	Eragon
byl	být	k5eAaImAgMnS	být
elf	elf	k1gMnSc1	elf
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
drak	drak	k1gMnSc1	drak
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
Bid	Bid	k1gMnSc1	Bid
<g/>
'	'	kIx"	'
<g/>
Darm	Darm	k1gMnSc1	Darm
<g/>
.	.	kIx.	.
</s>
<s>
Draci	drak	k1gMnPc1	drak
věnovali	věnovat	k5eAaPmAgMnP	věnovat
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
1-2	[number]	k4	1-2
vejce	vejce	k1gNnSc2	vejce
pro	pro	k7c4	pro
Jezdce	jezdec	k1gMnSc4	jezdec
<g/>
.	.	kIx.	.
</s>
<s>
Zbraně	zbraň	k1gFnPc4	zbraň
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
elfská	elfský	k2eAgFnSc1d1	elfská
kovářka	kovářka	k1gFnSc1	kovářka
Rhunön	Rhunöna	k1gFnPc2	Rhunöna
ze	z	k7c2	z
speciálního	speciální	k2eAgInSc2d1	speciální
superpevného	superpevný	k2eAgInSc2d1	superpevný
materiálu	materiál	k1gInSc2	materiál
z	z	k7c2	z
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
nazvala	nazvat	k5eAaBmAgFnS	nazvat
zářocel	zářocet	k5eAaPmAgMnS	zářocet
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
Jezdci	jezdec	k1gMnPc1	jezdec
získávali	získávat	k5eAaImAgMnP	získávat
stále	stále	k6eAd1	stále
větší	veliký	k2eAgFnSc4d2	veliký
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
až	až	k9	až
nakonec	nakonec	k6eAd1	nakonec
vládli	vládnout	k5eAaImAgMnP	vládnout
celé	celý	k2eAgFnSc3d1	celá
Alagaesii	Alagaesie	k1gFnSc3	Alagaesie
<g/>
.	.	kIx.	.
</s>
<s>
Časem	časem	k6eAd1	časem
se	se	k3xPyFc4	se
k	k	k7c3	k
jezdcům	jezdec	k1gMnPc3	jezdec
přidal	přidat	k5eAaPmAgMnS	přidat
i	i	k9	i
ambiciózní	ambiciózní	k2eAgInSc4d1	ambiciózní
Galbatorix	Galbatorix	k1gInSc4	Galbatorix
a	a	k8xC	a
jednou	jednou	k6eAd1	jednou
vyletěl	vyletět	k5eAaPmAgMnS	vyletět
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
dvěma	dva	k4xCgMnPc7	dva
přáteli	přítel	k1gMnPc7	přítel
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
u	u	k7c2	u
ohně	oheň	k1gInSc2	oheň
je	on	k3xPp3gMnPc4	on
přepadli	přepadnout	k5eAaPmAgMnP	přepadnout
urgalové	urgal	k1gMnPc1	urgal
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nakonec	nakonec	k6eAd1	nakonec
zabili	zabít	k5eAaPmAgMnP	zabít
jeho	jeho	k3xOp3gMnPc4	jeho
přátele	přítel	k1gMnPc4	přítel
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
jeho	on	k3xPp3gMnSc4	on
draka	drak	k1gMnSc4	drak
<g/>
.	.	kIx.	.
</s>
<s>
Zešílel	zešílet	k5eAaPmAgMnS	zešílet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
touha	touha	k1gFnSc1	touha
po	po	k7c6	po
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
Jezdci	jezdec	k1gMnPc1	jezdec
vydali	vydat	k5eAaPmAgMnP	vydat
nového	nový	k2eAgMnSc4d1	nový
draka	drak	k1gMnSc4	drak
ho	on	k3xPp3gMnSc4	on
donutila	donutit	k5eAaPmAgFnS	donutit
přežít	přežít	k5eAaPmF	přežít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Jezdci	jezdec	k1gMnPc1	jezdec
mu	on	k3xPp3gMnSc3	on
nového	nový	k2eAgMnSc2d1	nový
draka	drak	k1gMnSc2	drak
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
vydat	vydat	k5eAaPmF	vydat
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
tehdejší	tehdejší	k2eAgFnSc7d1	tehdejší
radou	rada	k1gFnSc7	rada
starších	starý	k2eAgMnPc2d2	starší
Jezdců	jezdec	k1gMnPc2	jezdec
byl	být	k5eAaImAgInS	být
i	i	k9	i
budoucí	budoucí	k2eAgMnSc1d1	budoucí
Eragonův	Eragonův	k2eAgMnSc1d1	Eragonův
učitel	učitel	k1gMnSc1	učitel
Oromis	Oromis	k1gFnSc2	Oromis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Galbatorix	Galbatorix	k1gInSc1	Galbatorix
naverboval	naverbovat	k5eAaPmAgInS	naverbovat
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
Jezdců	jezdec	k1gMnPc2	jezdec
(	(	kIx(	(
<g/>
Morzan	Morzan	k1gMnSc1	Morzan
<g/>
,	,	kIx,	,
Murtaghův	Murtaghův	k2eAgMnSc1d1	Murtaghův
otec	otec	k1gMnSc1	otec
<g/>
)	)	kIx)	)
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
stranu	strana	k1gFnSc4	strana
a	a	k8xC	a
společnými	společný	k2eAgFnPc7d1	společná
silami	síla	k1gFnPc7	síla
v	v	k7c6	v
městě	město	k1gNnSc6	město
tehdy	tehdy	k6eAd1	tehdy
zvaném	zvaný	k2eAgNnSc6d1	zvané
Ilirea	Ilireus	k1gMnSc2	Ilireus
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
Galbatorixem	Galbatorix	k1gInSc7	Galbatorix
přejmenované	přejmenovaný	k2eAgFnSc2d1	přejmenovaná
na	na	k7c6	na
Uru	Ur	k1gInSc6	Ur
<g/>
'	'	kIx"	'
<g/>
baen	baen	k1gInSc1	baen
<g/>
)	)	kIx)	)
ukradli	ukradnout	k5eAaPmAgMnP	ukradnout
dračí	dračí	k2eAgNnSc4d1	dračí
mládě	mládě	k1gNnSc4	mládě
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
Galbatorix	Galbatorix	k1gInSc1	Galbatorix
opředl	opříst	k5eAaPmAgInS	opříst
temnými	temný	k2eAgNnPc7d1	temné
kouzly	kouzlo	k1gNnPc7	kouzlo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
sloužilo	sloužit	k5eAaImAgNnS	sloužit
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
mu	on	k3xPp3gMnSc3	on
jméno	jméno	k1gNnSc4	jméno
Šruikan	Šruikan	k1gInSc4	Šruikan
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Morzanem	Morzan	k1gMnSc7	Morzan
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
připojili	připojit	k5eAaPmAgMnP	připojit
(	(	kIx(	(
<g/>
13	[number]	k4	13
Křivopřísežníků	křivopřísežník	k1gMnPc2	křivopřísežník
<g/>
)	)	kIx)	)
zabíjel	zabíjet	k5eAaImAgInS	zabíjet
Jezdce	jezdec	k1gMnPc4	jezdec
jednoho	jeden	k4xCgMnSc4	jeden
po	po	k7c6	po
druhém	druhý	k4xOgInSc6	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Vrael	Vrael	k1gMnSc1	Vrael
<g/>
,	,	kIx,	,
vůdce	vůdce	k1gMnSc1	vůdce
jezdců	jezdec	k1gMnPc2	jezdec
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
možnost	možnost	k1gFnSc4	možnost
Galbatorixe	Galbatorixe	k1gFnSc2	Galbatorixe
zabít	zabít	k5eAaPmF	zabít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zaváhal	zaváhat	k5eAaPmAgMnS	zaváhat
<g/>
,	,	kIx,	,
Galbatorix	Galbatorix	k1gInSc1	Galbatorix
toho	ten	k3xDgMnSc4	ten
využil	využít	k5eAaPmAgInS	využít
a	a	k8xC	a
Vraela	Vrael	k1gMnSc4	Vrael
zabil	zabít	k5eAaPmAgMnS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
králem	král	k1gMnSc7	král
Alagaësie	Alagaësie	k1gFnSc2	Alagaësie
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgMnSc7d1	poslední
žijícím	žijící	k2eAgMnSc7d1	žijící
tedy	tedy	k8xC	tedy
zůstal	zůstat	k5eAaPmAgInS	zůstat
Brom	brom	k1gInSc1	brom
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
dračice	dračice	k1gFnSc2	dračice
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
mu	on	k3xPp3gMnSc3	on
zabili	zabít	k5eAaPmAgMnP	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Jmenovala	jmenovat	k5eAaBmAgNnP	jmenovat
se	se	k3xPyFc4	se
Safira	Safiro	k1gNnPc1	Safiro
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Eragonova	Eragonův	k2eAgFnSc1d1	Eragonova
dračice	dračice	k1gFnSc1	dračice
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
i	i	k9	i
stejnou	stejný	k2eAgFnSc4d1	stejná
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Oromis	Oromis	k1gInSc1	Oromis
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc4	jeho
drak	drak	k1gInSc4	drak
Glaedr	Glaedr	k1gInSc1	Glaedr
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Broma	Bromum	k1gNnSc2	Bromum
a	a	k8xC	a
odcestování	odcestování	k1gNnSc2	odcestování
Eragona	Eragon	k1gMnSc2	Eragon
se	s	k7c7	s
Safirou	Safira	k1gMnSc7	Safira
do	do	k7c2	do
Du	Du	k?	Du
Weldenvarden	Weldenvardna	k1gFnPc2	Weldenvardna
<g/>
,	,	kIx,	,
stali	stát	k5eAaPmAgMnP	stát
jejich	jejich	k3xOp3gMnPc7	jejich
učiteli	učitel	k1gMnPc7	učitel
<g/>
,	,	kIx,	,
Galbatorix	Galbatorix	k1gInSc1	Galbatorix
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
drakem	drak	k1gMnSc7	drak
Šruikanem	Šruikan	k1gMnSc7	Šruikan
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
mu	on	k3xPp3gNnSc3	on
neslouží	sloužit	k5eNaImIp3nP	sloužit
dobrovolně	dobrovolně	k6eAd1	dobrovolně
<g/>
,	,	kIx,	,
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
Eragon	Eragon	k1gMnSc1	Eragon
se	s	k7c7	s
Safirou	Safira	k1gMnSc7	Safira
a	a	k8xC	a
Murtagh	Murtagh	k1gInSc4	Murtagh
s	s	k7c7	s
Trnem	trn	k1gInSc7	trn
<g/>
.	.	kIx.	.
</s>
<s>
Eldunarí	Eldunarit	k5eAaPmIp3nS	Eldunarit
nebo	nebo	k8xC	nebo
také	také	k9	také
česky	česky	k6eAd1	česky
srdce	srdce	k1gNnSc1	srdce
srdcí	srdce	k1gNnPc2	srdce
je	být	k5eAaImIp3nS	být
orgán	orgán	k1gInSc1	orgán
draků	drak	k1gInPc2	drak
z	z	k7c2	z
románů	román	k1gInPc2	román
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
kamene	kámen	k1gInSc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
společně	společně	k6eAd1	společně
s	s	k7c7	s
drakem	drak	k1gInSc7	drak
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgMnPc1d3	nejstarší
a	a	k8xC	a
největší	veliký	k2eAgMnPc1d3	veliký
draci	drak	k1gMnPc1	drak
měli	mít	k5eAaImAgMnP	mít
Eldunarí	Eldunarí	k1gFnSc4	Eldunarí
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
asi	asi	k9	asi
0,5	[number]	k4	0,5
m	m	kA	m
<g/>
,	,	kIx,	,
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
stejná	stejný	k2eAgFnSc1d1	stejná
jako	jako	k8xC	jako
barva	barva	k1gFnSc1	barva
draka	drak	k1gMnSc2	drak
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
jej	on	k3xPp3gNnSc4	on
každý	každý	k3xTgMnSc1	každý
již	již	k6eAd1	již
od	od	k7c2	od
narození	narození	k1gNnSc2	narození
<g/>
.	.	kIx.	.
</s>
<s>
Drak	drak	k1gInSc1	drak
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
může	moct	k5eAaImIp3nS	moct
schovat	schovat	k5eAaPmF	schovat
celou	celý	k2eAgFnSc4d1	celá
svoji	svůj	k3xOyFgFnSc4	svůj
osobnost	osobnost	k1gFnSc4	osobnost
<g/>
,	,	kIx,	,
vyvrhnout	vyvrhnout	k5eAaPmF	vyvrhnout
ho	on	k3xPp3gMnSc4	on
a	a	k8xC	a
někomu	někdo	k3yInSc3	někdo
darovat	darovat	k5eAaPmF	darovat
dobrovolně	dobrovolně	k6eAd1	dobrovolně
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
nějaký	nějaký	k3yIgMnSc1	nějaký
tvor	tvor	k1gMnSc1	tvor
měl	mít	k5eAaImAgMnS	mít
Eldunarí	Eldunarí	k1gFnPc4	Eldunarí
toho	ten	k3xDgMnSc2	ten
draka	drak	k1gMnSc2	drak
u	u	k7c2	u
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgInS	moct
by	by	kYmCp3nS	by
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
v	v	k7c6	v
myšlenkách	myšlenka	k1gFnPc6	myšlenka
komunikovat	komunikovat	k5eAaImF	komunikovat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
si	se	k3xPyFc3	se
od	od	k7c2	od
něj	on	k3xPp3gNnSc2	on
půjčovat	půjčovat	k5eAaImF	půjčovat
energii	energie	k1gFnSc4	energie
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
by	by	kYmCp3nS	by
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
byli	být	k5eAaImAgMnP	být
vzdálení	vzdálený	k2eAgMnPc1d1	vzdálený
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
nebo	nebo	k8xC	nebo
nedobrovolně	dobrovolně	k6eNd1	dobrovolně
<g/>
,	,	kIx,	,
když	když	k8xS	když
drak	drak	k1gMnSc1	drak
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
,	,	kIx,	,
nevyvrhnul	vyvrhnout	k5eNaPmAgMnS	vyvrhnout
jej	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vložil	vložit	k5eAaPmAgInS	vložit
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
svou	svůj	k3xOyFgFnSc4	svůj
osobnost	osobnost	k1gFnSc4	osobnost
<g/>
,	,	kIx,	,
přežije	přežít	k5eAaPmIp3nS	přežít
jeho	jeho	k3xOp3gFnSc4	jeho
Eldunarí	Eldunarí	k2eAgFnSc4d1	Eldunarí
smrt	smrt	k1gFnSc4	smrt
jeho	jeho	k3xOp3gNnSc2	jeho
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Eragon	Eragon	k1gNnSc1	Eragon
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
Oficiální	oficiální	k2eAgInSc1d1	oficiální
český	český	k2eAgInSc1d1	český
web	web	k1gInSc1	web
Odkazu	odkaz	k1gInSc2	odkaz
Dračích	dračí	k2eAgMnPc2d1	dračí
jezdců	jezdec	k1gMnPc2	jezdec
Alagaesie	Alagaesie	k1gFnSc2	Alagaesie
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
-	-	kIx~	-
Nejnovější	nový	k2eAgFnSc1d3	nejnovější
informace	informace	k1gFnSc1	informace
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
Alagaësie	Alagaësie	k1gFnSc2	Alagaësie
Dragon-Riders	Dragon-Ridersa	k1gFnPc2	Dragon-Ridersa
<g/>
.	.	kIx.	.
<g/>
eu	eu	k?	eu
-	-	kIx~	-
První	první	k4xOgFnSc1	první
česká	český	k2eAgFnSc1d1	Česká
textová	textový	k2eAgFnSc1d1	textová
RPG	RPG	kA	RPG
hra	hra	k1gFnSc1	hra
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
Eragona	Eragon	k1gMnSc2	Eragon
Alagaesia	Alagaesius	k1gMnSc2	Alagaesius
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
Druhá	druhý	k4xOgFnSc1	druhý
česká	český	k2eAgFnSc1d1	Česká
textová	textový	k2eAgFnSc1d1	textová
RPG	RPG	kA	RPG
hra	hra	k1gFnSc1	hra
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
Eragona	Eragona	k1gFnSc1	Eragona
Shurtugal	Shurtugal	k1gMnSc1	Shurtugal
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
-	-	kIx~	-
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
Odkazu	odkaz	k1gInSc2	odkaz
Dračích	dračí	k2eAgInPc2d1	dračí
jezdců	jezdec	k1gInPc2	jezdec
Alagaesia	Alagaesium	k1gNnSc2	Alagaesium
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
-	-	kIx~	-
Vše	všechen	k3xTgNnSc1	všechen
o	o	k7c6	o
Odkazu	odkaz	k1gInSc6	odkaz
Dračích	dračí	k2eAgMnPc2d1	dračí
jezdců	jezdec	k1gMnPc2	jezdec
</s>
