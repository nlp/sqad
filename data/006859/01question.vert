<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
chráněná	chráněný	k2eAgFnSc1d1	chráněná
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
výskytu	výskyt	k1gInSc2	výskyt
přestárlého	přestárlý	k2eAgInSc2d1	přestárlý
dubového	dubový	k2eAgInSc2d1	dubový
porostu	porost	k1gInSc2	porost
<g/>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
západně	západně	k6eAd1	západně
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
Velká	velká	k1gFnSc1	velká
u	u	k7c2	u
Milevska	Milevsko	k1gNnSc2	Milevsko
poblíž	poblíž	k7c2	poblíž
křižovatky	křižovatka	k1gFnSc2	křižovatka
silnice	silnice	k1gFnSc2	silnice
121	[number]	k4	121
a	a	k8xC	a
odbočky	odbočka	k1gFnSc2	odbočka
směr	směr	k1gInSc1	směr
Sobědraž	Sobědraž	k1gFnSc4	Sobědraž
<g/>
?	?	kIx.	?
</s>
