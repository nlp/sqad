<s>
Sobědražský	Sobědražský	k2eAgInSc1d1	Sobědražský
prales	prales	k1gInSc1	prales
je	být	k5eAaImIp3nS	být
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
chráněná	chráněný	k2eAgFnSc1d1	chráněná
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
výskytu	výskyt	k1gInSc2	výskyt
přestárlého	přestárlý	k2eAgInSc2d1	přestárlý
dubového	dubový	k2eAgInSc2d1	dubový
porostu	porost	k1gInSc2	porost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
západně	západně	k6eAd1	západně
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
Velká	velká	k1gFnSc1	velká
u	u	k7c2	u
Milevska	Milevsko	k1gNnSc2	Milevsko
poblíž	poblíž	k7c2	poblíž
křižovatky	křižovatka	k1gFnSc2	křižovatka
silnice	silnice	k1gFnSc2	silnice
121	[number]	k4	121
a	a	k8xC	a
odbočky	odbočka	k1gFnSc2	odbočka
směr	směr	k1gInSc1	směr
Sobědraž	Sobědraž	k1gFnSc4	Sobědraž
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
malý	malý	k2eAgInSc4d1	malý
zbytek	zbytek	k1gInSc4	zbytek
přirozeného	přirozený	k2eAgInSc2d1	přirozený
porostu	porost	k1gInSc2	porost
acidofilní	acidofilní	k2eAgFnSc2d1	acidofilní
bučiny	bučina	k1gFnSc2	bučina
<g/>
.	.	kIx.	.
</s>
<s>
Dubový	dubový	k2eAgInSc1d1	dubový
porost	porost	k1gInSc1	porost
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
památky	památka	k1gFnSc2	památka
vysazován	vysazován	k2eAgInSc4d1	vysazován
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1809	[number]	k4	1809
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
převážně	převážně	k6eAd1	převážně
dubem	dub	k1gInSc7	dub
letním	letní	k2eAgInSc7d1	letní
(	(	kIx(	(
<g/>
Quercus	Quercus	k1gInSc1	Quercus
robur	robura	k1gFnPc2	robura
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
z	z	k7c2	z
50	[number]	k4	50
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
bukem	buk	k1gInSc7	buk
lesního	lesní	k2eAgMnSc2d1	lesní
(	(	kIx(	(
<g/>
Fagus	Fagus	k1gInSc1	Fagus
sylvatica	sylvatic	k1gInSc2	sylvatic
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ca	ca	kA	ca
45	[number]	k4	45
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
místy	místy	k6eAd1	místy
prorůstá	prorůstat	k5eAaImIp3nS	prorůstat
modřín	modřín	k1gInSc1	modřín
opadavý	opadavý	k2eAgInSc1d1	opadavý
(	(	kIx(	(
<g/>
Larix	Larix	k1gInSc1	Larix
decidua	decidu	k1gInSc2	decidu
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgFnSc1d1	stará
skladba	skladba	k1gFnSc1	skladba
lesa	les	k1gInSc2	les
s	s	k7c7	s
postupně	postupně	k6eAd1	postupně
odumírajícími	odumírající	k2eAgInPc7d1	odumírající
stromy	strom	k1gInPc7	strom
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
hnízdění	hnízdění	k1gNnSc1	hnízdění
v	v	k7c6	v
dutinách	dutina	k1gFnPc6	dutina
stromů	strom	k1gInPc2	strom
pro	pro	k7c4	pro
druhy	druh	k1gInPc4	druh
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
lejsek	lejsek	k1gMnSc1	lejsek
malý	malý	k2eAgMnSc1d1	malý
(	(	kIx(	(
<g/>
Ficedula	Ficedul	k1gMnSc4	Ficedul
parva	parva	k?	parva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
je	být	k5eAaImIp3nS	být
také	také	k9	také
hojný	hojný	k2eAgInSc1d1	hojný
výskyt	výskyt	k1gInSc1	výskyt
vyšších	vysoký	k2eAgMnPc2d2	vyšší
savců	savec	k1gMnPc2	savec
<g/>
.	.	kIx.	.
</s>
<s>
Přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
v	v	k7c6	v
lesích	les	k1gInPc6	les
vlastněných	vlastněný	k2eAgFnPc2d1	vlastněná
Janem	Jan	k1gMnSc7	Jan
Schwarzenbergem	Schwarzenberg	k1gMnSc7	Schwarzenberg
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Velká	velká	k1gFnSc1	velká
u	u	k7c2	u
Milevska	Milevsko	k1gNnSc2	Milevsko
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Písek	Písek	k1gInSc1	Písek
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
úbočí	úbočí	k1gNnSc6	úbočí
552	[number]	k4	552
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
vrchu	vrch	k1gInSc2	vrch
Chlum	chlum	k1gInSc1	chlum
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
300	[number]	k4	300
metrů	metr	k1gInPc2	metr
severně	severně	k6eAd1	severně
od	od	k7c2	od
silnice	silnice	k1gFnSc2	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
121	[number]	k4	121
vedoucí	vedoucí	k1gMnSc1	vedoucí
z	z	k7c2	z
Milevska	Milevsko	k1gNnSc2	Milevsko
do	do	k7c2	do
Zvíkovského	zvíkovský	k2eAgNnSc2d1	Zvíkovské
Podhradí	Podhradí	k1gNnSc2	Podhradí
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
489	[number]	k4	489
až	až	k9	až
494	[number]	k4	494
m	m	kA	m
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
místní	místní	k2eAgFnSc2d1	místní
lesní	lesní	k2eAgFnSc2d1	lesní
cesty	cesta	k1gFnSc2	cesta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
území	území	k1gNnSc4	území
protíná	protínat	k5eAaImIp3nS	protínat
na	na	k7c6	na
nesouměrně	souměrně	k6eNd1	souměrně
velké	velký	k2eAgFnSc6d1	velká
části	část	k1gFnSc6	část
(	(	kIx(	(
<g/>
východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejnižší	nízký	k2eAgFnSc1d3	nejnižší
část	část	k1gFnSc1	část
území	území	k1gNnSc2	území
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k6eAd1	okolo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
50	[number]	k4	50
metrové	metrový	k2eAgFnSc2d1	metrová
ochranné	ochranný	k2eAgFnSc2d1	ochranná
pásmo	pásmo	k1gNnSc1	pásmo
dle	dle	k7c2	dle
§	§	k?	§
37	[number]	k4	37
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
114	[number]	k4	114
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
Lesní	lesní	k2eAgInPc1d1	lesní
pozemky	pozemek	k1gInPc1	pozemek
jsou	být	k5eAaImIp3nP	být
vedeny	vést	k5eAaImNgInP	vést
jakožto	jakožto	k8xS	jakožto
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
les	les	k1gInSc1	les
spadající	spadající	k2eAgInSc1d1	spadající
pod	pod	k7c4	pod
správu	správa	k1gFnSc4	správa
Lesního	lesní	k2eAgInSc2d1	lesní
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
celku	celek	k1gInSc2	celek
Orlík	orlík	k1gMnSc1	orlík
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Dubový	dubový	k2eAgInSc1d1	dubový
porost	porost	k1gInSc1	porost
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
památky	památka	k1gFnSc2	památka
vysazován	vysazován	k2eAgInSc4d1	vysazován
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1809	[number]	k4	1809
<g/>
.	.	kIx.	.
</s>
<s>
Chráněné	chráněný	k2eAgNnSc1d1	chráněné
území	území	k1gNnSc1	území
bylo	být	k5eAaImAgNnS	být
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1985	[number]	k4	1985
(	(	kIx(	(
<g/>
s	s	k7c7	s
datem	datum	k1gNnSc7	datum
účinnosti	účinnost	k1gFnSc2	účinnost
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
vyhláškou	vyhláška	k1gFnSc7	vyhláška
o	o	k7c6	o
chráněných	chráněný	k2eAgInPc6d1	chráněný
přírodních	přírodní	k2eAgInPc6d1	přírodní
výtvorech	výtvor	k1gInPc6	výtvor
vydanou	vydaný	k2eAgFnSc4d1	vydaná
Okresním	okresní	k2eAgInSc7d1	okresní
národním	národní	k2eAgInSc7d1	národní
výborem	výbor	k1gInSc7	výbor
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
ochránit	ochránit	k5eAaPmF	ochránit
zbytek	zbytek	k1gInSc4	zbytek
přestárlého	přestárlý	k2eAgInSc2d1	přestárlý
dubobukového	dubobukový	k2eAgInSc2d1	dubobukový
porostu	porost	k1gInSc2	porost
v	v	k7c6	v
lesním	lesní	k2eAgInSc6d1	lesní
komplexu	komplex	k1gInSc6	komplex
Chlum	chlum	k1gInSc1	chlum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
proběhl	proběhnout	k5eAaPmAgMnS	proběhnout
na	na	k7c6	na
lokalitě	lokalita	k1gFnSc6	lokalita
inventarizační	inventarizační	k2eAgInSc4d1	inventarizační
průzkum	průzkum	k1gInSc4	průzkum
po	po	k7c6	po
stránce	stránka	k1gFnSc6	stránka
botanické	botanický	k2eAgFnSc6d1	botanická
a	a	k8xC	a
zoologické	zoologický	k2eAgFnSc6d1	zoologická
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
přírodní	přírodní	k2eAgFnSc4d1	přírodní
památku	památka	k1gFnSc4	památka
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
plán	plán	k1gInSc1	plán
péče	péče	k1gFnSc2	péče
pro	pro	k7c4	pro
roky	rok	k1gInPc4	rok
2001	[number]	k4	2001
až	až	k8xS	až
2010	[number]	k4	2010
a	a	k8xC	a
další	další	k2eAgInSc1d1	další
2011	[number]	k4	2011
až	až	k8xS	až
2020	[number]	k4	2020
<g/>
.	.	kIx.	.
</s>
<s>
Sobědrážský	Sobědrážský	k2eAgInSc1d1	Sobědrážský
prales	prales	k1gInSc1	prales
se	se	k3xPyFc4	se
z	z	k7c2	z
geomorfologického	geomorfologický	k2eAgInSc2d1	geomorfologický
pohledu	pohled	k1gInSc2	pohled
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Táborské	Táborské	k2eAgFnSc6d1	Táborské
pahorkatině	pahorkatina	k1gFnSc6	pahorkatina
spadající	spadající	k2eAgFnSc6d1	spadající
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
Blatenské	blatenský	k2eAgFnSc2d1	Blatenská
pahorkatiny	pahorkatina	k1gFnSc2	pahorkatina
<g/>
,	,	kIx,	,
podsoustavy	podsoustava	k1gFnSc2	podsoustava
Středočeské	středočeský	k2eAgFnSc2d1	Středočeská
pahorkatiny	pahorkatina	k1gFnSc2	pahorkatina
<g/>
,	,	kIx,	,
soustavy	soustava	k1gFnSc2	soustava
Česko-moravské	českooravský	k2eAgFnSc2d1	česko-moravská
a	a	k8xC	a
provincii	provincie	k1gFnSc6	provincie
Česká	český	k2eAgFnSc1d1	Česká
vysočina	vysočina	k1gFnSc1	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
Klimaticky	klimaticky	k6eAd1	klimaticky
spadá	spadat	k5eAaPmIp3nS	spadat
oblast	oblast	k1gFnSc4	oblast
do	do	k7c2	do
mírně	mírně	k6eAd1	mírně
teplé	teplý	k2eAgFnSc2d1	teplá
oblasti	oblast	k1gFnSc2	oblast
MT	MT	kA	MT
10	[number]	k4	10
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgNnPc4d1	charakteristické
dlouhým	dlouhý	k2eAgMnSc7d1	dlouhý
<g/>
,	,	kIx,	,
teplým	teplý	k2eAgNnSc7d1	teplé
a	a	k8xC	a
suchým	suchý	k2eAgNnSc7d1	suché
létem	léto	k1gNnSc7	léto
a	a	k8xC	a
krátkou	krátká	k1gFnSc7	krátká
<g/>
,	,	kIx,	,
mírně	mírně	k6eAd1	mírně
teplou	teplý	k2eAgFnSc7d1	teplá
a	a	k8xC	a
suchou	suchý	k2eAgFnSc7d1	suchá
zimou	zima	k1gFnSc7	zima
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
7	[number]	k4	7
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
průměrným	průměrný	k2eAgInSc7d1	průměrný
úhrnem	úhrn	k1gInSc7	úhrn
ročních	roční	k2eAgFnPc2d1	roční
srážek	srážka	k1gFnPc2	srážka
okolo	okolo	k7c2	okolo
600	[number]	k4	600
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
přírodní	přírodní	k2eAgFnSc2d1	přírodní
památky	památka	k1gFnSc2	památka
je	být	k5eAaImIp3nS	být
odvodňováno	odvodňovat	k5eAaImNgNnS	odvodňovat
do	do	k7c2	do
bezejmenného	bezejmenný	k2eAgInSc2d1	bezejmenný
potoka	potok	k1gInSc2	potok
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
vlévajícím	vlévající	k2eAgFnPc3d1	vlévající
do	do	k7c2	do
Jickovického	Jickovický	k2eAgInSc2d1	Jickovický
potoka	potok	k1gInSc2	potok
a	a	k8xC	a
pak	pak	k6eAd1	pak
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
Vltavy	Vltava	k1gFnSc2	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podloží	podloží	k1gNnSc6	podloží
přírodní	přírodní	k2eAgFnSc2d1	přírodní
památky	památka	k1gFnSc2	památka
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
nachází	nacházet	k5eAaImIp3nS	nacházet
porfyricko	porfyricko	k6eAd1	porfyricko
amfibol-biotitický	amfiboliotitický	k2eAgInSc1d1	amfibol-biotitický
melanokratní	melanokratní	k2eAgInSc1d1	melanokratní
žula	žula	k1gFnSc1	žula
typu	typ	k1gInSc2	typ
Čertova	čertův	k2eAgNnSc2d1	Čertovo
břemene	břemeno	k1gNnSc2	břemeno
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
do	do	k7c2	do
severní	severní	k2eAgFnSc2d1	severní
části	část	k1gFnSc2	část
území	území	k1gNnSc2	území
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
žíla	žíla	k1gFnSc1	žíla
biotitické	biotitický	k2eAgFnSc2d1	biotitická
žilné	žilný	k2eAgFnSc2d1	žilná
ruly	rula	k1gFnSc2	rula
<g/>
.	.	kIx.	.
</s>
<s>
Žíla	žíla	k1gFnSc1	žíla
nicméně	nicméně	k8xC	nicméně
nikde	nikde	k6eAd1	nikde
na	na	k7c6	na
území	území	k1gNnSc6	území
přírodní	přírodní	k2eAgFnSc2d1	přírodní
památky	památka	k1gFnSc2	památka
nevystupuje	vystupovat	k5eNaImIp3nS	vystupovat
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Půdní	půdní	k2eAgInSc1d1	půdní
profil	profil	k1gInSc1	profil
pak	pak	k6eAd1	pak
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
kambizem	kambiz	k1gInSc7	kambiz
modální	modální	k2eAgInPc4d1	modální
<g/>
,	,	kIx,	,
jen	jen	k9	jen
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
je	být	k5eAaImIp3nS	být
i	i	k9	i
kambizem	kambiz	k1gInSc7	kambiz
oglejená	oglejený	k2eAgFnSc5d1	oglejená
<g/>
.	.	kIx.	.
</s>
<s>
Rozlohu	rozloha	k1gFnSc4	rozloha
celého	celý	k2eAgNnSc2d1	celé
území	území	k1gNnSc2	území
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
acidofilní	acidofilní	k2eAgFnSc1d1	acidofilní
biková	bikový	k2eAgFnSc1d1	biková
bučina	bučina	k1gFnSc1	bučina
svazu	svaz	k1gInSc2	svaz
Luzulo	Luzula	k1gFnSc5	Luzula
-	-	kIx~	-
Fagion	Fagion	k1gInSc1	Fagion
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
reprezentována	reprezentován	k2eAgFnSc1d1	reprezentována
přestárlým	přestárlý	k1gMnPc3	přestárlý
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
180	[number]	k4	180
až	až	k9	až
200	[number]	k4	200
let	léto	k1gNnPc2	léto
starým	starý	k1gMnPc3	starý
<g/>
,	,	kIx,	,
porostem	porost	k1gInSc7	porost
dubu	dub	k1gInSc2	dub
letního	letní	k2eAgInSc2d1	letní
(	(	kIx(	(
<g/>
Quercus	Quercus	k1gMnSc1	Quercus
robur	robur	k1gMnSc1	robur
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zaujímajícího	zaujímající	k2eAgInSc2d1	zaujímající
přibližně	přibližně	k6eAd1	přibližně
50	[number]	k4	50
%	%	kIx~	%
<g/>
,	,	kIx,	,
a	a	k8xC	a
buku	buk	k1gInSc2	buk
lesního	lesní	k2eAgMnSc2d1	lesní
(	(	kIx(	(
<g/>
Fagus	Fagus	k1gInSc1	Fagus
sylvatica	sylvatic	k1gInSc2	sylvatic
<g/>
)	)	kIx)	)
zaujímajícího	zaujímající	k2eAgInSc2d1	zaujímající
přibližně	přibližně	k6eAd1	přibližně
45	[number]	k4	45
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
porostu	porost	k1gInSc6	porost
místy	místy	k6eAd1	místy
rostou	růst	k5eAaImIp3nP	růst
i	i	k9	i
jedinci	jedinec	k1gMnPc1	jedinec
dalších	další	k2eAgFnPc2d1	další
na	na	k7c6	na
lokalitě	lokalita	k1gFnSc6	lokalita
nepůvodních	původní	k2eNgInPc2d1	nepůvodní
druhů	druh	k1gInPc2	druh
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
modřín	modřín	k1gInSc1	modřín
opadavý	opadavý	k2eAgInSc1d1	opadavý
(	(	kIx(	(
<g/>
Larix	Larix	k1gInSc1	Larix
decidua	decidu	k1gInSc2	decidu
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
borovice	borovice	k1gFnSc1	borovice
lesní	lesní	k2eAgFnSc1d1	lesní
(	(	kIx(	(
<g/>
Pinus	Pinus	k1gMnSc1	Pinus
sylvestris	sylvestris	k1gFnSc2	sylvestris
<g/>
)	)	kIx)	)
a	a	k8xC	a
smrk	smrk	k1gInSc1	smrk
ztepilý	ztepilý	k2eAgInSc1d1	ztepilý
(	(	kIx(	(
<g/>
Picea	Picea	k1gMnSc1	Picea
abies	abies	k1gMnSc1	abies
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nižším	nízký	k2eAgNnSc6d2	nižší
patře	patro	k1gNnSc6	patro
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mladší	mladý	k2eAgInSc1d2	mladší
porost	porost	k1gInSc1	porost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
převládá	převládat	k5eAaImIp3nS	převládat
převážně	převážně	k6eAd1	převážně
buk	buk	k1gInSc4	buk
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
pak	pak	k6eAd1	pak
dub	dub	k1gInSc1	dub
a	a	k8xC	a
místy	místo	k1gNnPc7	místo
i	i	k8xC	i
smrk	smrk	k1gInSc4	smrk
a	a	k8xC	a
v	v	k7c6	v
oplocené	oplocený	k2eAgFnSc6d1	oplocená
části	část	k1gFnSc6	část
území	území	k1gNnSc2	území
pak	pak	k6eAd1	pak
i	i	k8xC	i
jedle	jedle	k6eAd1	jedle
bělokorá	bělokorý	k2eAgFnSc1d1	bělokorá
(	(	kIx(	(
<g/>
Abies	Abies	k1gInSc1	Abies
alba	album	k1gNnSc2	album
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolních	okolní	k2eAgInPc6d1	okolní
porostech	porost	k1gInPc6	porost
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nespadají	spadat	k5eNaImIp3nP	spadat
do	do	k7c2	do
přírodní	přírodní	k2eAgFnSc2d1	přírodní
památky	památka	k1gFnSc2	památka
<g/>
,	,	kIx,	,
roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
mladším	mladý	k2eAgNnSc6d2	mladší
patře	patro	k1gNnSc6	patro
lesa	les	k1gInSc2	les
i	i	k8xC	i
douglaska	douglaska	k1gFnSc1	douglaska
tisolistá	tisolistý	k2eAgFnSc1d1	tisolistá
(	(	kIx(	(
<g/>
Pseudotsuga	Pseudotsuga	k1gFnSc1	Pseudotsuga
menziesii	menziesie	k1gFnSc4	menziesie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
stromovým	stromový	k2eAgNnSc7d1	stromové
patrem	patro	k1gNnSc7	patro
chybí	chybět	k5eAaImIp3nS	chybět
keřové	keřový	k2eAgNnSc1d1	keřové
patro	patro	k1gNnSc1	patro
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
rovnou	rovnou	k6eAd1	rovnou
nachází	nacházet	k5eAaImIp3nS	nacházet
chudé	chudý	k2eAgNnSc1d1	chudé
bylinné	bylinný	k2eAgNnSc1d1	bylinné
patro	patro	k1gNnSc1	patro
typické	typický	k2eAgNnSc1d1	typické
pro	pro	k7c4	pro
acidofilní	acidofilní	k2eAgFnSc4d1	acidofilní
bikovou	bikový	k2eAgFnSc4d1	biková
bučinu	bučina	k1gFnSc4	bučina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
možné	možný	k2eAgNnSc1d1	možné
najít	najít	k5eAaPmF	najít
konvalinku	konvalinka	k1gFnSc4	konvalinka
vonnou	vonný	k2eAgFnSc4d1	vonná
(	(	kIx(	(
<g/>
Convallaria	Convallarium	k1gNnPc1	Convallarium
majalis	majalis	k1gFnPc2	majalis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šťavel	šťavel	k1gInSc4	šťavel
kyselý	kyselý	k2eAgInSc4d1	kyselý
(	(	kIx(	(
<g/>
Oxalis	Oxalis	k1gInSc4	Oxalis
acetosella	acetosello	k1gNnSc2	acetosello
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
třtinu	třtina	k1gFnSc4	třtina
rákosovitou	rákosovitý	k2eAgFnSc4d1	rákosovitá
(	(	kIx(	(
<g/>
Calamagrostis	Calamagrostis	k1gFnSc4	Calamagrostis
arundinacea	arundinace	k1gInSc2	arundinace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pstroček	pstroček	k1gInSc1	pstroček
dvoulistý	dvoulistý	k2eAgInSc1d1	dvoulistý
(	(	kIx(	(
<g/>
Maianthemum	Maianthemum	k1gInSc1	Maianthemum
bifolium	bifolium	k1gNnSc1	bifolium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hrachor	hrachor	k1gInSc1	hrachor
jarní	jarní	k2eAgInSc1d1	jarní
(	(	kIx(	(
<g/>
Lathyrus	Lathyrus	k1gInSc1	Lathyrus
vernus	vernus	k1gInSc1	vernus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kokořík	kokořík	k1gInSc1	kokořík
mnohokvětý	mnohokvětý	k2eAgInSc1d1	mnohokvětý
(	(	kIx(	(
<g/>
Polygonatum	Polygonatum	k1gNnSc1	Polygonatum
multiflorum	multiflorum	k1gNnSc1	multiflorum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jaterník	jaterník	k1gInSc1	jaterník
podléška	podléška	k1gFnSc1	podléška
(	(	kIx(	(
<g/>
Hepatica	Hepatic	k2eAgFnSc1d1	Hepatica
nobilis	nobilis	k1gFnSc1	nobilis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
svízel	svízel	k1gFnSc1	svízel
vonná	vonný	k2eAgFnSc1d1	vonná
(	(	kIx(	(
<g/>
Galium	galium	k1gNnSc1	galium
odoratum	odoratum	k1gNnSc1	odoratum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
terénních	terénní	k2eAgFnPc6d1	terénní
depresích	deprese	k1gFnPc6	deprese
roste	růst	k5eAaImIp3nS	růst
ostřice	ostřice	k1gFnSc1	ostřice
třeslicovitá	třeslicovitý	k2eAgFnSc1d1	třeslicovitá
(	(	kIx(	(
<g/>
Carex	Carex	k1gInSc1	Carex
brizoides	brizoides	k1gInSc1	brizoides
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
lokalitě	lokalita	k1gFnSc6	lokalita
byli	být	k5eAaImAgMnP	být
pozorováni	pozorován	k2eAgMnPc1d1	pozorován
následující	následující	k2eAgMnPc1d1	následující
zástupci	zástupce	k1gMnPc1	zástupce
bezobratlých	bezobratlý	k2eAgMnPc2d1	bezobratlý
<g/>
:	:	kIx,	:
lesklec	lesklec	k1gMnSc1	lesklec
Rhizophagus	Rhizophagus	k1gMnSc1	Rhizophagus
nutidulus	nutidulus	k1gMnSc1	nutidulus
<g/>
,	,	kIx,	,
dřevožrout	dřevožrout	k1gMnSc1	dřevožrout
Ceylon	Ceylon	k1gInSc4	Ceylon
fagi	fagi	k1gNnSc2	fagi
<g/>
,	,	kIx,	,
maločlenec	maločlenec	k1gMnSc1	maločlenec
Crytophagus	Crytophagus	k1gMnSc1	Crytophagus
ferrugineus	ferrugineus	k1gMnSc1	ferrugineus
či	či	k8xC	či
kožojed	kožojed	k1gMnSc1	kožojed
Globicornis	Globicornis	k1gFnSc2	Globicornis
corticalis	corticalis	k1gFnSc2	corticalis
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
z	z	k7c2	z
ptactva	ptactvo	k1gNnSc2	ptactvo
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
byli	být	k5eAaImAgMnP	být
pozorováni	pozorován	k2eAgMnPc1d1	pozorován
zástupci	zástupce	k1gMnPc1	zástupce
těchto	tento	k3xDgInPc2	tento
druhů	druh	k1gInPc2	druh
<g/>
:	:	kIx,	:
silně	silně	k6eAd1	silně
ohrožený	ohrožený	k2eAgMnSc1d1	ohrožený
lejsek	lejsek	k1gMnSc1	lejsek
malý	malý	k1gMnSc1	malý
(	(	kIx(	(
<g/>
Ficedula	Ficedula	k1gFnSc1	Ficedula
parva	parva	k?	parva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
datel	datel	k1gMnSc1	datel
černý	černý	k1gMnSc1	černý
(	(	kIx(	(
<g/>
Dryocopus	Dryocopus	k1gMnSc1	Dryocopus
Martinus	Martinus	k1gMnSc1	Martinus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
budníček	budníček	k1gMnSc1	budníček
lesní	lesní	k2eAgMnSc1d1	lesní
(	(	kIx(	(
<g/>
Phylloscopus	Phylloscopus	k1gInSc1	Phylloscopus
sibilatrix	sibilatrix	k1gInSc1	sibilatrix
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
brhlík	brhlík	k1gMnSc1	brhlík
<g />
.	.	kIx.	.
</s>
<s>
lesní	lesní	k2eAgFnSc1d1	lesní
(	(	kIx(	(
<g/>
Sitta	Sitta	k1gFnSc1	Sitta
europaea	europaea	k1gMnSc1	europaea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hýl	hýl	k1gMnSc1	hýl
obecný	obecný	k2eAgMnSc1d1	obecný
(	(	kIx(	(
<g/>
Pyrrhula	Pyrrhul	k1gMnSc4	Pyrrhul
pyrrhula	pyrrhul	k1gMnSc4	pyrrhul
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pěnkava	pěnkava	k1gFnSc1	pěnkava
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Fringilla	Fringilla	k1gFnSc1	Fringilla
coelebs	coelebsa	k1gFnPc2	coelebsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sýkora	sýkora	k1gFnSc1	sýkora
parukářka	parukářka	k1gFnSc1	parukářka
(	(	kIx(	(
<g/>
Parus	Parus	k1gMnSc1	Parus
cristatus	cristatus	k1gMnSc1	cristatus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
červenka	červenka	k1gFnSc1	červenka
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Erithacus	Erithacus	k1gInSc1	Erithacus
rubecula	rubeculum	k1gNnSc2	rubeculum
<g/>
)	)	kIx)	)
či	či	k8xC	či
strakapoud	strakapoud	k1gMnSc1	strakapoud
velký	velký	k2eAgMnSc1d1	velký
(	(	kIx(	(
<g/>
Dendrocopos	Dendrocopos	k1gMnSc1	Dendrocopos
major	major	k1gMnSc1	major
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
savců	savec	k1gMnPc2	savec
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
veverka	veverka	k1gFnSc1	veverka
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Sciurus	Sciurus	k1gInSc1	Sciurus
vulgaris	vulgaris	k1gFnSc2	vulgaris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prase	prase	k1gNnSc1	prase
divoké	divoký	k2eAgNnSc1d1	divoké
(	(	kIx(	(
<g/>
Sus	Sus	k1gMnSc4	Sus
scrofa	scrof	k1gMnSc4	scrof
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
srnec	srnec	k1gMnSc1	srnec
obecný	obecný	k2eAgMnSc1d1	obecný
(	(	kIx(	(
<g/>
Capreolus	Capreolus	k1gMnSc1	Capreolus
capreolus	capreolus	k1gMnSc1	capreolus
<g/>
)	)	kIx)	)
či	či	k8xC	či
daněk	daněk	k1gMnSc1	daněk
evropský	evropský	k2eAgMnSc1d1	evropský
(	(	kIx(	(
<g/>
Dama	Dama	k1gMnSc1	Dama
dama	dama	k1gMnSc1	dama
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
managementové	managementový	k2eAgFnSc2d1	managementová
kategorie	kategorie	k1gFnSc2	kategorie
IV	Iva	k1gFnPc2	Iva
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
do	do	k7c2	do
řízené	řízený	k2eAgFnSc2d1	řízená
rezervace	rezervace	k1gFnSc2	rezervace
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
povoleny	povolen	k2eAgInPc4d1	povolen
plánované	plánovaný	k2eAgInPc4d1	plánovaný
zásahy	zásah	k1gInPc4	zásah
do	do	k7c2	do
přírodních	přírodní	k2eAgInPc2d1	přírodní
procesů	proces	k1gInPc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
na	na	k7c6	na
lokalitě	lokalita	k1gFnSc6	lokalita
zásahy	zásah	k1gInPc1	zásah
víceméně	víceméně	k9	víceméně
neprobíhají	probíhat	k5eNaImIp3nP	probíhat
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
původní	původní	k2eAgInSc4d1	původní
lesní	lesní	k2eAgInSc4d1	lesní
porosty	porost	k1gInPc4	porost
jsou	být	k5eAaImIp3nP	být
ponechávány	ponecháván	k2eAgInPc4d1	ponecháván
přirozenému	přirozený	k2eAgInSc3d1	přirozený
vývoji	vývoj	k1gInSc3	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
ochrany	ochrana	k1gFnSc2	ochrana
je	být	k5eAaImIp3nS	být
zachovat	zachovat	k5eAaPmF	zachovat
a	a	k8xC	a
zlepšit	zlepšit	k5eAaPmF	zlepšit
stav	stav	k1gInSc4	stav
acidofilní	acidofilní	k2eAgFnSc2d1	acidofilní
bikové	bikové	k2eAgFnSc2d1	bikové
bučiny	bučina	k1gFnSc2	bučina
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
záměrů	záměr	k1gInPc2	záměr
je	být	k5eAaImIp3nS	být
vytvořit	vytvořit	k5eAaPmF	vytvořit
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
různorodou	různorodý	k2eAgFnSc4d1	různorodá
věkovou	věkový	k2eAgFnSc4d1	věková
skladbu	skladba	k1gFnSc4	skladba
lesa	les	k1gInSc2	les
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgInP	být
přiměřeně	přiměřeně	k6eAd1	přiměřeně
zastoupeny	zastoupit	k5eAaPmNgInP	zastoupit
jak	jak	k8xC	jak
staré	starý	k2eAgInPc1d1	starý
stromy	strom	k1gInPc1	strom
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
stromy	strom	k1gInPc1	strom
se	s	k7c7	s
středním	střední	k2eAgNnSc7d1	střední
a	a	k8xC	a
mladším	mladý	k2eAgNnSc7d2	mladší
stářím	stáří	k1gNnSc7	stáří
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
je	být	k5eAaImIp3nS	být
snaha	snaha	k1gFnSc1	snaha
odstranit	odstranit	k5eAaPmF	odstranit
jehličnaté	jehličnatý	k2eAgFnPc4d1	jehličnatá
dřeviny	dřevina	k1gFnPc4	dřevina
a	a	k8xC	a
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgMnSc3	ten
doplnit	doplnit	k5eAaPmF	doplnit
do	do	k7c2	do
druhové	druhový	k2eAgFnSc2d1	druhová
skladby	skladba	k1gFnSc2	skladba
lesa	les	k1gInSc2	les
jedli	jíst	k5eAaImAgMnP	jíst
bělokorou	bělokorý	k2eAgFnSc4d1	bělokorá
a	a	k8xC	a
lípu	lípa	k1gFnSc4	lípa
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
na	na	k7c6	na
jiných	jiný	k2eAgNnPc6d1	jiné
územích	území	k1gNnPc6	území
i	i	k9	i
v	v	k7c6	v
Sobědrážském	Sobědrážský	k2eAgInSc6d1	Sobědrážský
pralese	prales	k1gInSc6	prales
je	být	k5eAaImIp3nS	být
snaha	snaha	k1gFnSc1	snaha
redukovat	redukovat	k5eAaBmF	redukovat
tlak	tlak	k1gInSc4	tlak
spárkaté	spárkatý	k2eAgFnSc2d1	spárkatá
zvěře	zvěř	k1gFnSc2	zvěř
okusující	okusující	k2eAgMnPc4d1	okusující
mladé	mladý	k2eAgMnPc4d1	mladý
stromky	stromek	k1gInPc4	stromek
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zabraňující	zabraňující	k2eAgFnSc1d1	zabraňující
přirozenému	přirozený	k2eAgNnSc3d1	přirozené
zmlazování	zmlazování	k1gNnSc3	zmlazování
porostu	porost	k1gInSc2	porost
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
porost	porost	k1gInSc1	porost
přírodní	přírodní	k2eAgFnSc2d1	přírodní
památky	památka	k1gFnSc2	památka
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
převážně	převážně	k6eAd1	převážně
přestárlými	přestárlý	k2eAgInPc7d1	přestárlý
stromy	strom	k1gInPc7	strom
s	s	k7c7	s
chybějícím	chybějící	k2eAgNnSc7d1	chybějící
přirozeným	přirozený	k2eAgNnSc7d1	přirozené
zmlazením	zmlazení	k1gNnSc7	zmlazení
<g/>
.	.	kIx.	.
</s>
<s>
Vysoký	vysoký	k2eAgInSc1d1	vysoký
věk	věk	k1gInSc1	věk
stromů	strom	k1gInPc2	strom
současně	současně	k6eAd1	současně
dokládá	dokládat	k5eAaImIp3nS	dokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
přemnožená	přemnožený	k2eAgFnSc1d1	přemnožená
vysoká	vysoký	k2eAgFnSc1d1	vysoká
zvěř	zvěř	k1gFnSc1	zvěř
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
lokalitě	lokalita	k1gFnSc6	lokalita
dlouhodobý	dlouhodobý	k2eAgInSc4d1	dlouhodobý
neřešený	řešený	k2eNgInSc4d1	neřešený
problém	problém	k1gInSc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Plán	plán	k1gInSc1	plán
péče	péče	k1gFnSc2	péče
pro	pro	k7c4	pro
roky	rok	k1gInPc4	rok
2011	[number]	k4	2011
až	až	k8xS	až
2020	[number]	k4	2020
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
staré	starý	k2eAgNnSc1d1	staré
stromové	stromový	k2eAgNnSc1d1	stromové
patro	patro	k1gNnSc1	patro
se	se	k3xPyFc4	se
časem	časem	k6eAd1	časem
rozpadne	rozpadnout	k5eAaPmIp3nS	rozpadnout
a	a	k8xC	a
jelikož	jelikož	k8xS	jelikož
nedochází	docházet	k5eNaImIp3nS	docházet
k	k	k7c3	k
dostatečné	dostatečný	k2eAgFnSc3d1	dostatečná
ochraně	ochrana	k1gFnSc3	ochrana
mladých	mladý	k2eAgInPc2d1	mladý
porostů	porost	k1gInPc2	porost
oplocenkami	oplocenka	k1gFnPc7	oplocenka
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
výhledově	výhledově	k6eAd1	výhledově
nutné	nutný	k2eAgNnSc1d1	nutné
pak	pak	k6eAd1	pak
uměle	uměle	k6eAd1	uměle
obnovit	obnovit	k5eAaPmF	obnovit
porost	porost	k1gInSc4	porost
přírodní	přírodní	k2eAgFnSc2d1	přírodní
památky	památka	k1gFnSc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
vhodné	vhodný	k2eAgNnSc1d1	vhodné
řešení	řešení	k1gNnSc1	řešení
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
buď	buď	k8xC	buď
snížit	snížit	k5eAaPmF	snížit
stavy	stav	k1gInPc4	stav
spárkaté	spárkatý	k2eAgFnSc2d1	spárkatá
zvěře	zvěř	k1gFnSc2	zvěř
<g/>
,	,	kIx,	,
či	či	k8xC	či
oplotit	oplotit	k5eAaPmF	oplotit
celou	celý	k2eAgFnSc4d1	celá
oblast	oblast	k1gFnSc4	oblast
přírodní	přírodní	k2eAgFnSc2d1	přírodní
památky	památka	k1gFnSc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pouze	pouze	k6eAd1	pouze
dvě	dva	k4xCgFnPc1	dva
oplocenky	oplocenka	k1gFnPc1	oplocenka
<g/>
.	.	kIx.	.
</s>
