<p>
<s>
Chaac	Chaac	k1gInSc1	Chaac
je	být	k5eAaImIp3nS	být
mayský	mayský	k2eAgMnSc1d1	mayský
bůh	bůh	k1gMnSc1	bůh
deště	dešť	k1gInSc2	dešť
<g/>
,	,	kIx,	,
blesků	blesk	k1gInPc2	blesk
a	a	k8xC	a
bouře	bouř	k1gFnSc2	bouř
uctívaný	uctívaný	k2eAgInSc1d1	uctívaný
po	po	k7c6	po
mnoho	mnoho	k4c4	mnoho
staletí	staletí	k1gNnPc2	staletí
a	a	k8xC	a
v	v	k7c6	v
domorodém	domorodý	k2eAgNnSc6d1	domorodé
mayském	mayský	k2eAgNnSc6d1	mayské
náboženství	náboženství	k1gNnSc6	náboženství
jsou	být	k5eAaImIp3nP	být
rituály	rituál	k1gInPc4	rituál
jeho	jeho	k3xOp3gInSc2	jeho
kultu	kult	k1gInSc2	kult
udržovány	udržovat	k5eAaImNgInP	udržovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Chaac	Chaac	k1gInSc1	Chaac
ovlivňoval	ovlivňovat	k5eAaImAgInS	ovlivňovat
pomocí	pomocí	k7c2	pomocí
deště	dešť	k1gInSc2	dešť
všechny	všechen	k3xTgInPc4	všechen
vodní	vodní	k2eAgInPc4d1	vodní
zdroje	zdroj	k1gInPc4	zdroj
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
tudíž	tudíž	k8xC	tudíž
i	i	k9	i
patronem	patron	k1gInSc7	patron
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
.	.	kIx.	.
</s>
</p>
