<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
vápník	vápník	k1gInSc1	vápník
nezbytný	zbytný	k2eNgInSc1d1	zbytný
pro	pro	k7c4	pro
zdravý	zdravý	k2eAgInSc4d1	zdravý
vývin	vývin	k1gInSc4	vývin
a	a	k8xC	a
růst	růst	k1gInSc4	růst
kostí	kost	k1gFnPc2	kost
a	a	k8xC	a
zubů	zub	k1gInPc2	zub
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
vyskytoval	vyskytovat	k5eAaImAgInS	vyskytovat
především	především	k9	především
v	v	k7c6	v
jídelníčku	jídelníček	k1gInSc6	jídelníček
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
</s>
