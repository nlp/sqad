<s>
Jupiter	Jupiter	k1gMnSc1	Jupiter
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Iuppiter	Iuppiter	k1gInSc1	Iuppiter
<g/>
;	;	kIx,	;
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
nepravidelné	pravidelný	k2eNgNnSc4d1	nepravidelné
skloňování	skloňování	k1gNnSc4	skloňování
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
,	,	kIx,	,
Jova	Jova	k1gMnSc1	Jova
<g/>
,	,	kIx,	,
Jovovi	Jovův	k2eAgMnPc1d1	Jovův
<g/>
,	,	kIx,	,
Jova	Jova	k1gMnSc1	Jova
<g/>
,	,	kIx,	,
Jove	Jove	k1gFnSc1	Jove
<g/>
,	,	kIx,	,
Jovovi	Jovův	k2eAgMnPc1d1	Jovův
<g/>
,	,	kIx,	,
Jovem	Jovem	k?	Jovem
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
římská	římský	k2eAgFnSc1d1	římská
obdoba	obdoba	k1gFnSc1	obdoba
řeckého	řecký	k2eAgMnSc2d1	řecký
boha	bůh	k1gMnSc2	bůh
Dia	Dia	k1gMnSc2	Dia
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
z	z	k7c2	z
bohů	bůh	k1gMnPc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
byl	být	k5eAaImAgInS	být
spojován	spojovat	k5eAaImNgInS	spojovat
především	především	k6eAd1	především
s	s	k7c7	s
živly	živel	k1gInPc7	živel
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pánem	pán	k1gMnSc7	pán
nebes	nebesa	k1gNnPc2	nebesa
<g/>
,	,	kIx,	,
hromů	hrom	k1gInPc2	hrom
a	a	k8xC	a
blesků	blesk	k1gInPc2	blesk
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
ještě	ještě	k6eAd1	ještě
připsána	připsán	k2eAgFnSc1d1	připsána
role	role	k1gFnSc1	role
ochránce	ochránce	k1gMnSc2	ochránce
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
náklonnost	náklonnost	k1gFnSc1	náklonnost
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgInSc7d1	významný
momentem	moment	k1gInSc7	moment
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
byl	být	k5eAaImAgInS	být
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c2	za
dárce	dárce	k1gMnSc2	dárce
vítězství	vítězství	k1gNnSc2	vítězství
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
naklonit	naklonit	k5eAaPmF	naklonit
si	se	k3xPyFc3	se
jeho	jeho	k3xOp3gFnSc4	jeho
přízeň	přízeň	k1gFnSc4	přízeň
především	především	k6eAd1	především
pořádáním	pořádání	k1gNnSc7	pořádání
her	hra	k1gFnPc2	hra
v	v	k7c6	v
cirku	cirk	k1gInSc6	cirk
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
byla	být	k5eAaImAgFnS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
bohyně	bohyně	k1gFnSc1	bohyně
Juno	Juno	k1gFnSc2	Juno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
září	září	k1gNnSc2	září
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
slavnosti	slavnost	k1gFnPc1	slavnost
na	na	k7c4	na
Jovovu	Jovův	k2eAgFnSc4d1	Jovova
počest	počest	k1gFnSc4	počest
<g/>
,	,	kIx,	,
skládající	skládající	k2eAgInPc4d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
obětí	oběť	k1gFnPc2	oběť
<g/>
,	,	kIx,	,
slavnostních	slavnostní	k2eAgInPc2d1	slavnostní
průvodů	průvod	k1gInPc2	průvod
a	a	k8xC	a
her	hra	k1gFnPc2	hra
v	v	k7c6	v
cirku	cirk	k1gInSc6	cirk
<g/>
.	.	kIx.	.
</s>
<s>
Kult	kult	k1gInSc4	kult
Jova	Jova	k1gMnSc1	Jova
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterým	který	k3yQgNnSc7	který
často	často	k6eAd1	často
splývaly	splývat	k5eAaImAgInP	splývat
různé	různý	k2eAgInPc1d1	různý
kulty	kult	k1gInPc1	kult
bohů	bůh	k1gMnPc2	bůh
východních	východní	k2eAgNnPc2d1	východní
náboženství	náboženství	k1gNnPc2	náboženství
<g/>
,	,	kIx,	,
přetrval	přetrvat	k5eAaPmAgInS	přetrvat
až	až	k9	až
do	do	k7c2	do
dob	doba	k1gFnPc2	doba
raného	raný	k2eAgNnSc2d1	rané
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc4d3	veliký
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgInSc4d3	nejstarší
<g/>
,	,	kIx,	,
nejhonosnější	honosný	k2eAgInSc4d3	nejhonosnější
a	a	k8xC	a
nejdůležitější	důležitý	k2eAgInSc4d3	nejdůležitější
chrám	chrám	k1gInSc4	chrám
zasvěcený	zasvěcený	k2eAgInSc4d1	zasvěcený
Jovovi	Jova	k1gMnSc3	Jova
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Junonou	Juno	k1gFnSc7	Juno
a	a	k8xC	a
Minervou	Minerva	k1gFnSc7	Minerva
stál	stát	k5eAaImAgMnS	stát
na	na	k7c6	na
Kapitolu	Kapitol	k1gInSc6	Kapitol
(	(	kIx(	(
<g/>
zachován	zachovat	k5eAaPmNgInS	zachovat
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
fragmentech	fragment	k1gInPc6	fragment
základů	základ	k1gInPc2	základ
Kapitolského	kapitolský	k2eAgNnSc2d1	Kapitolské
muzea	muzeum	k1gNnSc2	muzeum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jupiter	Jupiter	k1gMnSc1	Jupiter
je	být	k5eAaImIp3nS	být
původem	původ	k1gInSc7	původ
indoevropské	indoevropský	k2eAgNnSc4d1	indoevropské
božstvo	božstvo	k1gNnSc4	božstvo
nebes	nebesa	k1gNnPc2	nebesa
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
indoevropského	indoevropský	k2eAgInSc2d1	indoevropský
kořene	kořen	k1gInSc2	kořen
Djaus	Djaus	k1gMnSc1	Djaus
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
paralelu	paralela	k1gFnSc4	paralela
ve	v	k7c6	v
jménech	jméno	k1gNnPc6	jméno
Deus	Deusa	k1gFnPc2	Deusa
<g/>
,	,	kIx,	,
Jupiter	Jupiter	k1gMnSc1	Jupiter
(	(	kIx(	(
<g/>
řím	řím	k?	řím
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zeus	Zeus	k1gInSc1	Zeus
pater	patro	k1gNnPc2	patro
<g/>
(	(	kIx(	(
<g/>
řec.	řec.	k?	řec.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Djaus	Djaus	k1gMnSc1	Djaus
Pitar	Pitar	k1gMnSc1	Pitar
(	(	kIx(	(
<g/>
védský	védský	k2eAgMnSc1d1	védský
Nebeský	nebeský	k2eAgMnSc1d1	nebeský
otec	otec	k1gMnSc1	otec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Týr	Týr	k1gMnSc1	Týr
(	(	kIx(	(
<g/>
germ.	germ.	k?	germ.
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
aj.	aj.	kA	aj.
Nacházíme	nacházet	k5eAaImIp1nP	nacházet
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
atribut	atribut	k1gInSc1	atribut
ztracené	ztracený	k2eAgFnSc2d1	ztracená
ruky	ruka	k1gFnSc2	ruka
<g/>
,	,	kIx,	,
při	při	k7c6	při
jeho	jeho	k3xOp3gFnPc6	jeho
slavnostech	slavnost	k1gFnPc6	slavnost
byl	být	k5eAaImAgInS	být
představován	představovat	k5eAaImNgInS	představovat
mužem	muž	k1gMnSc7	muž
jedoucím	jedoucí	k2eAgMnSc7d1	jedoucí
na	na	k7c6	na
voze	vůz	k1gInSc6	vůz
s	s	k7c7	s
v	v	k7c6	v
hávu	háv	k1gInSc6	háv
zakrytým	zakrytý	k2eAgNnSc7d1	zakryté
pravým	pravý	k2eAgNnSc7d1	pravé
předloktím	předloktí	k1gNnSc7	předloktí
<g/>
,	,	kIx,	,
tak	tak	k9	tak
jako	jako	k9	jako
u	u	k7c2	u
jeho	jeho	k3xOp3gFnSc2	jeho
germánské	germánský	k2eAgFnSc2d1	germánská
podoby	podoba	k1gFnSc2	podoba
Tyra	Tyrum	k1gNnSc2	Tyrum
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
také	také	k9	také
Sabazia	Sabazius	k1gMnSc4	Sabazius
<g/>
,	,	kIx,	,
frýžského	frýžský	k2eAgMnSc4d1	frýžský
boha	bůh	k1gMnSc4	bůh
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
zobrazován	zobrazovat	k5eAaImNgMnS	zobrazovat
s	s	k7c7	s
bronzovou	bronzový	k2eAgFnSc7d1	bronzová
pravou	pravý	k2eAgFnSc7d1	pravá
rukou	ruka	k1gFnSc7	ruka
zdobenou	zdobený	k2eAgFnSc7d1	zdobená
symboly	symbol	k1gInPc7	symbol
dobrotivosti	dobrotivost	k1gFnSc2	dobrotivost
a	a	k8xC	a
laskavosti	laskavost	k1gFnSc2	laskavost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
védách	véda	k1gFnPc6	véda
se	se	k3xPyFc4	se
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
podobě	podoba	k1gFnSc6	podoba
Djaus	Djaus	k1gMnSc1	Djaus
Pitar	Pitar	k1gMnSc1	Pitar
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
původně	původně	k6eAd1	původně
nejstarší	starý	k2eAgNnSc4d3	nejstarší
božstvo	božstvo	k1gNnSc4	božstvo
<g/>
,	,	kIx,	,
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
indoevropských	indoevropský	k2eAgNnPc6d1	indoevropské
náboženstvích	náboženství	k1gNnPc6	náboženství
byl	být	k5eAaImAgMnS	být
později	pozdě	k6eAd2	pozdě
zastíněn	zastínit	k5eAaPmNgMnS	zastínit
původně	původně	k6eAd1	původně
nižšími	nízký	k2eAgNnPc7d2	nižší
božstvy	božstvo	k1gNnPc7	božstvo
(	(	kIx(	(
<g/>
Odinem	Odin	k1gInSc7	Odin
u	u	k7c2	u
severních	severní	k2eAgMnPc2d1	severní
germánských	germánský	k2eAgMnPc2d1	germánský
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
Indrou	Indra	k1gMnSc7	Indra
u	u	k7c2	u
indů	ind	k1gMnPc2	ind
<g/>
)	)	kIx)	)
a	a	k8xC	a
nebo	nebo	k8xC	nebo
si	se	k3xPyFc3	se
udržel	udržet	k5eAaPmAgMnS	udržet
vůdčí	vůdčí	k2eAgNnSc4d1	vůdčí
postavení	postavení	k1gNnSc4	postavení
v	v	k7c6	v
trojici	trojice	k1gFnSc6	trojice
nejvyšších	vysoký	k2eAgNnPc2d3	nejvyšší
božstev	božstvo	k1gNnPc2	božstvo
(	(	kIx(	(
<g/>
keltský	keltský	k2eAgInSc1d1	keltský
Teutatis	Teutatis	k1gInSc1	Teutatis
<g/>
,	,	kIx,	,
baltský	baltský	k2eAgInSc1d1	baltský
Dievs	Dievs	k1gInSc1	Dievs
<g/>
,	,	kIx,	,
římský	římský	k2eAgMnSc1d1	římský
Jupiter	Jupiter	k1gMnSc1	Jupiter
a	a	k8xC	a
řecký	řecký	k2eAgInSc1d1	řecký
Zeus	Zeus	k1gInSc1	Zeus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
slovanském	slovanský	k2eAgInSc6d1	slovanský
panteonu	panteon	k1gInSc6	panteon
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
projevil	projevit	k5eAaPmAgMnS	projevit
jako	jako	k9	jako
Svarog	Svarog	k1gMnSc1	Svarog
<g/>
,	,	kIx,	,
základem	základ	k1gInSc7	základ
jehož	jenž	k3xRgNnSc2	jenž
jména	jméno	k1gNnSc2	jméno
je	být	k5eAaImIp3nS	být
indoevropské	indoevropský	k2eAgInPc4d1	indoevropský
svar	svar	k1gInSc4	svar
(	(	kIx(	(
<g/>
světlo	světlo	k1gNnSc4	světlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
souvisí	souviset	k5eAaImIp3nS	souviset
i	i	k9	i
se	s	k7c7	s
sanskrtským	sanskrtský	k2eAgInSc7d1	sanskrtský
výrazem	výraz	k1gInSc7	výraz
svarga	svarg	k1gMnSc2	svarg
(	(	kIx(	(
<g/>
nebe	nebe	k1gNnSc2	nebe
<g/>
)	)	kIx)	)
a	a	k8xC	a
íránským	íránský	k2eAgMnSc7d1	íránský
hvar	hvar	k1gInSc1	hvar
(	(	kIx(	(
<g/>
slunce	slunce	k1gNnSc1	slunce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zeus	Zeus	k6eAd1	Zeus
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jupiter	Jupiter	k1gInSc1	Jupiter
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
