<s>
Jupiter	Jupiter	k1gMnSc1	Jupiter
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Iuppiter	Iuppiter	k1gInSc1	Iuppiter
<g/>
;	;	kIx,	;
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
nepravidelné	pravidelný	k2eNgNnSc4d1	nepravidelné
skloňování	skloňování	k1gNnSc4	skloňování
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
,	,	kIx,	,
Jova	Jova	k1gMnSc1	Jova
<g/>
,	,	kIx,	,
Jovovi	Jovův	k2eAgMnPc1d1	Jovův
<g/>
,	,	kIx,	,
Jova	Jova	k1gMnSc1	Jova
<g/>
,	,	kIx,	,
Jove	Jove	k1gFnSc1	Jove
<g/>
,	,	kIx,	,
Jovovi	Jovův	k2eAgMnPc1d1	Jovův
<g/>
,	,	kIx,	,
Jovem	Jovem	k?	Jovem
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
římská	římský	k2eAgFnSc1d1	římská
obdoba	obdoba	k1gFnSc1	obdoba
řeckého	řecký	k2eAgMnSc2d1	řecký
boha	bůh	k1gMnSc2	bůh
Dia	Dia	k1gMnSc2	Dia
<g/>
.	.	kIx.	.
</s>
