<p>
<s>
Noviny	novina	k1gFnPc1	novina
Těšínské	Těšínská	k1gFnSc2	Těšínská
byly	být	k5eAaImAgInP	být
regionálním	regionální	k2eAgInSc7d1	regionální
týdeníkem	týdeník	k1gInSc7	týdeník
<g/>
,	,	kIx,	,
vycházejícím	vycházející	k2eAgMnSc7d1	vycházející
v	v	k7c6	v
letech	let	k1gInPc6	let
1894	[number]	k4	1894
–	–	k?	–
asi	asi	k9	asi
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vycházely	vycházet	k5eAaImAgFnP	vycházet
v	v	k7c6	v
Těšíně	Těšín	k1gInSc6	Těšín
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Frýdku	Frýdek	k1gInSc6	Frýdek
a	a	k8xC	a
v	v	k7c6	v
Opavě	Opava	k1gFnSc6	Opava
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
přispěvatele	přispěvatel	k1gMnSc4	přispěvatel
Novin	novina	k1gFnPc2	novina
patřili	patřit	k5eAaImAgMnP	patřit
např.	např.	kA	např.
Vincenc	Vincenc	k1gMnSc1	Vincenc
Prasek	Prasek	k1gMnSc1	Prasek
či	či	k8xC	či
Josef	Josef	k1gMnSc1	Josef
Zukal	Zukal	k1gMnSc1	Zukal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bibliografie	bibliografie	k1gFnSc2	bibliografie
==	==	k?	==
</s>
</p>
<p>
<s>
CHROMCOVÁ	CHROMCOVÁ	kA	CHROMCOVÁ
<g/>
,	,	kIx,	,
Gabriela	Gabriela	k1gFnSc1	Gabriela
<g/>
:	:	kIx,	:
Noviny	novina	k1gFnPc1	novina
Těšínské	Těšínská	k1gFnSc2	Těšínská
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
Těšín	Těšín	k1gInSc1	Těšín
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Obrana	obrana	k1gFnSc1	obrana
Slezska	Slezsko	k1gNnSc2	Slezsko
</s>
</p>
