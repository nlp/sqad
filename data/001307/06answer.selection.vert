<s>
Co	co	k3yRnSc1	co
záleží	záležet	k5eAaImIp3nS	záležet
včele	včela	k1gFnSc3	včela
<g/>
...	...	k?	...
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
Does	Does	k1gInSc1	Does
a	a	k8xC	a
Bee	Bee	k1gFnSc1	Bee
Care	car	k1gMnSc5	car
<g/>
?	?	kIx.	?
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
krátká	krátký	k2eAgFnSc1d1	krátká
vědeckofantastická	vědeckofantastický	k2eAgFnSc1d1	vědeckofantastická
povídka	povídka	k1gFnSc1	povídka
spisovatele	spisovatel	k1gMnSc2	spisovatel
Isaaca	Isaacus	k1gMnSc2	Isaacus
Asimova	Asimův	k2eAgFnSc1d1	Asimova
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1957	[number]	k4	1957
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
If	If	k1gFnSc2	If
<g/>
:	:	kIx,	:
Worlds	Worlds	k1gInSc1	Worlds
of	of	k?	of
Science	Science	k1gFnSc1	Science
Fiction	Fiction	k1gInSc1	Fiction
<g/>
.	.	kIx.	.
</s>
