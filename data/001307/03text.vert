<s>
Co	co	k3yInSc1	co
záleží	záležet	k5eAaImIp3nS	záležet
včele	včela	k1gFnSc3	včela
<g/>
...	...	k?	...
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
Does	Does	k1gInSc1	Does
a	a	k8xC	a
Bee	Bee	k1gFnSc1	Bee
Care	car	k1gMnSc5	car
<g/>
?	?	kIx.	?
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
krátká	krátký	k2eAgFnSc1d1	krátká
vědeckofantastická	vědeckofantastický	k2eAgFnSc1d1	vědeckofantastická
povídka	povídka	k1gFnSc1	povídka
spisovatele	spisovatel	k1gMnSc2	spisovatel
Isaaca	Isaacus	k1gMnSc2	Isaacus
Asimova	Asimův	k2eAgFnSc1d1	Asimova
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1957	[number]	k4	1957
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
If	If	k1gFnSc2	If
<g/>
:	:	kIx,	:
Worlds	Worlds	k1gInSc1	Worlds
of	of	k?	of
Science	Science	k1gFnSc1	Science
Fiction	Fiction	k1gInSc1	Fiction
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
následně	následně	k6eAd1	následně
zařazena	zařadit	k5eAaPmNgFnS	zařadit
do	do	k7c2	do
povídkových	povídkový	k2eAgFnPc2d1	povídková
sbírek	sbírka	k1gFnPc2	sbírka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Nightfall	Nightfall	k1gMnSc1	Nightfall
and	and	k?	and
Other	Other	k1gMnSc1	Other
Stories	Stories	k1gMnSc1	Stories
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Česky	česky	k6eAd1	česky
vyšla	vyjít	k5eAaPmAgFnS	vyjít
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
Sny	sen	k1gInPc4	sen
robotů	robot	k1gMnPc2	robot
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Thornton	Thornton	k1gInSc1	Thornton
Hammer	Hammra	k1gFnPc2	Hammra
Theodore	Theodor	k1gMnSc5	Theodor
Lengyel	Lengyel	k1gInSc1	Lengyel
Kane	kanout	k5eAaImIp3nS	kanout
V	v	k7c4	v
nejmenovanou	jmenovaný	k2eNgFnSc4d1	nejmenovaná
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
staví	stavit	k5eAaBmIp3nS	stavit
na	na	k7c6	na
pozemské	pozemský	k2eAgFnSc6d1	pozemská
základně	základna	k1gFnSc6	základna
kosmická	kosmický	k2eAgFnSc1d1	kosmická
loď	loď	k1gFnSc1	loď
<g/>
.	.	kIx.	.
</s>
<s>
Theodore	Theodor	k1gMnSc5	Theodor
Lengyel	Lengyel	k1gMnSc1	Lengyel
<g/>
,	,	kIx,	,
zástupce	zástupce	k1gMnSc1	zástupce
společnosti	společnost	k1gFnSc2	společnost
financující	financující	k2eAgFnSc4d1	financující
stavbu	stavba	k1gFnSc4	stavba
si	se	k3xPyFc3	se
stěžuje	stěžovat	k5eAaImIp3nS	stěžovat
vedoucímu	vedoucí	k2eAgInSc3d1	vedoucí
projektu	projekt	k1gInSc3	projekt
Thorntonu	Thornton	k1gInSc6	Thornton
Hammerovi	Hammerův	k2eAgMnPc1d1	Hammerův
na	na	k7c4	na
jednoho	jeden	k4xCgMnSc4	jeden
pracovníka	pracovník	k1gMnSc4	pracovník
jménem	jméno	k1gNnSc7	jméno
Kane	kanout	k5eAaImIp3nS	kanout
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
nic	nic	k6eAd1	nic
nedělá	dělat	k5eNaImIp3nS	dělat
<g/>
.	.	kIx.	.
</s>
<s>
Thornton	Thornton	k1gInSc1	Thornton
to	ten	k3xDgNnSc1	ten
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nehodlá	hodlat	k5eNaImIp3nS	hodlat
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
zbavit	zbavit	k5eAaPmF	zbavit
<g/>
,	,	kIx,	,
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
přítomnosti	přítomnost	k1gFnSc6	přítomnost
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
lépe	dobře	k6eAd2	dobře
přemýšlí	přemýšlet	k5eAaImIp3nS	přemýšlet
<g/>
.	.	kIx.	.
</s>
<s>
Kane	kanout	k5eAaImIp3nS	kanout
se	se	k3xPyFc4	se
motá	motat	k5eAaImIp3nS	motat
kolem	kolem	k7c2	kolem
stavby	stavba	k1gFnSc2	stavba
a	a	k8xC	a
nikdo	nikdo	k3yNnSc1	nikdo
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
už	už	k6eAd1	už
nevšímá	všímat	k5eNaImIp3nS	všímat
<g/>
,	,	kIx,	,
lidé	člověk	k1gMnPc1	člověk
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
zvyklí	zvyklý	k2eAgMnPc1d1	zvyklý
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
přitahují	přitahovat	k5eAaImIp3nP	přitahovat
pouze	pouze	k6eAd1	pouze
hvězdy	hvězda	k1gFnPc4	hvězda
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
jeden	jeden	k4xCgInSc4	jeden
bod	bod	k1gInSc4	bod
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejasně	jasně	k6eNd1	jasně
si	se	k3xPyFc3	se
uvědomuje	uvědomovat	k5eAaImIp3nS	uvědomovat
proces	proces	k1gInSc4	proces
výstavby	výstavba	k1gFnSc2	výstavba
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
až	až	k9	až
bude	být	k5eAaImBp3nS	být
dokončena	dokončen	k2eAgFnSc1d1	dokončena
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
on	on	k3xPp3gMnSc1	on
uvnitř	uvnitř	k6eAd1	uvnitř
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
předek	předek	k1gMnSc1	předek
si	se	k3xPyFc3	se
vybral	vybrat	k5eAaPmAgMnS	vybrat
planetu	planeta	k1gFnSc4	planeta
Zemi	zem	k1gFnSc3	zem
po	po	k7c6	po
pečlivém	pečlivý	k2eAgNnSc6d1	pečlivé
rozvážení	rozvážení	k1gNnSc6	rozvážení
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
každá	každý	k3xTgFnSc1	každý
planeta	planeta	k1gFnSc1	planeta
byla	být	k5eAaImAgFnS	být
vyhovující	vyhovující	k2eAgFnSc1d1	vyhovující
<g/>
.	.	kIx.	.
</s>
<s>
Umístil	umístit	k5eAaPmAgInS	umístit
zde	zde	k6eAd1	zde
vajíčko	vajíčko	k1gNnSc4	vajíčko
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
vylíhl	vylíhnout	k5eAaPmAgMnS	vylíhnout
tvor	tvor	k1gMnSc1	tvor
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
později	pozdě	k6eAd2	pozdě
přijal	přijmout	k5eAaPmAgMnS	přijmout
lidskou	lidský	k2eAgFnSc4d1	lidská
podobu	podoba	k1gFnSc4	podoba
(	(	kIx(	(
<g/>
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
se	se	k3xPyFc4	se
lidská	lidský	k2eAgFnSc1d1	lidská
rasa	rasa	k1gFnSc1	rasa
stala	stát	k5eAaPmAgFnS	stát
dominantní	dominantní	k2eAgFnSc1d1	dominantní
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
lidského	lidský	k2eAgInSc2d1	lidský
pohledu	pohled	k1gInSc2	pohled
nesmrtelný	nesmrtelný	k1gMnSc1	nesmrtelný
a	a	k8xC	a
dokázal	dokázat	k5eAaPmAgMnS	dokázat
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
ty	ten	k3xDgInPc4	ten
nejinteligentnější	inteligentní	k2eAgInPc4d3	nejinteligentnější
lidské	lidský	k2eAgInPc4d1	lidský
mozky	mozek	k1gInPc4	mozek
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
podnítil	podnítit	k5eAaPmAgMnS	podnítit
revoluční	revoluční	k2eAgInPc4d1	revoluční
vynálezy	vynález	k1gInPc4	vynález
a	a	k8xC	a
objevy	objev	k1gInPc4	objev
<g/>
.	.	kIx.	.
</s>
<s>
Ovlivnil	ovlivnit	k5eAaPmAgMnS	ovlivnit
Isaaca	Isaaca	k1gMnSc1	Isaaca
Newtona	Newton	k1gMnSc2	Newton
<g/>
,	,	kIx,	,
Lise	Lisa	k1gFnSc3	Lisa
Meitnerovou	Meitnerová	k1gFnSc4	Meitnerová
<g/>
,	,	kIx,	,
Alberta	Albert	k1gMnSc4	Albert
Einsteina	Einstein	k1gMnSc4	Einstein
a	a	k8xC	a
další	další	k2eAgMnPc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
povzbudit	povzbudit	k5eAaPmF	povzbudit
lidstvo	lidstvo	k1gNnSc1	lidstvo
ke	k	k7c3	k
stavbě	stavba	k1gFnSc3	stavba
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
loď	loď	k1gFnSc1	loď
hotová	hotový	k2eAgFnSc1d1	hotová
<g/>
,	,	kIx,	,
Kane	kanout	k5eAaImIp3nS	kanout
si	se	k3xPyFc3	se
vleze	vlézt	k5eAaPmIp3nS	vlézt
dovnitř	dovnitř	k6eAd1	dovnitř
a	a	k8xC	a
čeká	čekat	k5eAaImIp3nS	čekat
na	na	k7c4	na
start	start	k1gInSc4	start
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
byla	být	k5eAaImAgFnS	být
loď	loď	k1gFnSc1	loď
plánována	plánovat	k5eAaImNgFnS	plánovat
bez	bez	k7c2	bez
posádky	posádka	k1gFnSc2	posádka
<g/>
,	,	kIx,	,
Kane	kanout	k5eAaImIp3nS	kanout
dokázal	dokázat	k5eAaPmAgMnS	dokázat
manipulovat	manipulovat	k5eAaImF	manipulovat
tvůrci	tvůrce	k1gMnSc3	tvůrce
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
nechali	nechat	k5eAaPmAgMnP	nechat
dostatečně	dostatečně	k6eAd1	dostatečně
velký	velký	k2eAgInSc4d1	velký
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
vzápětí	vzápětí	k6eAd1	vzápětí
zapomněli	zapomenout	k5eAaPmAgMnP	zapomenout
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
kosmoplán	kosmoplán	k2eAgInSc1d1	kosmoplán
dostane	dostat	k5eAaPmIp3nS	dostat
za	za	k7c4	za
hranici	hranice	k1gFnSc4	hranice
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
tvor	tvor	k1gMnSc1	tvor
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
býval	bývat	k5eAaImAgMnS	bývat
Kanem	Kanem	k?	Kanem
odvrhne	odvrhnout	k5eAaPmIp3nS	odvrhnout
své	svůj	k3xOyFgNnSc4	svůj
lidské	lidský	k2eAgNnSc4d1	lidské
tělo	tělo	k1gNnSc4	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Skončilo	skončit	k5eAaPmAgNnS	skončit
larvální	larvální	k2eAgNnSc1d1	larvální
období	období	k1gNnSc1	období
<g/>
,	,	kIx,	,
teď	teď	k6eAd1	teď
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
dospělý	dospělý	k1gMnSc1	dospělý
jedinec	jedinec	k1gMnSc1	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Opustí	opustit	k5eAaPmIp3nS	opustit
prostory	prostora	k1gFnPc4	prostora
lodi	loď	k1gFnSc2	loď
a	a	k8xC	a
žene	hnát	k5eAaImIp3nS	hnát
se	s	k7c7	s
závratnou	závratný	k2eAgFnSc7d1	závratná
rychlostí	rychlost	k1gFnSc7	rychlost
k	k	k7c3	k
domovu	domov	k1gInSc3	domov
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
vzlétne	vzlétnout	k5eAaPmIp3nS	vzlétnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
sám	sám	k3xTgMnSc1	sám
oplodnil	oplodnit	k5eAaPmAgMnS	oplodnit
nějakou	nějaký	k3yIgFnSc4	nějaký
další	další	k2eAgFnSc4d1	další
planetu	planeta	k1gFnSc4	planeta
<g/>
.	.	kIx.	.
</s>
