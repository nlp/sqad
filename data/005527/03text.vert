<s>
Illinois	Illinois	k1gFnSc1	Illinois
(	(	kIx(	(
<g/>
anglická	anglický	k2eAgFnSc1d1	anglická
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
ˌ	ˌ	k?	ˌ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
State	status	k1gInSc5	status
of	of	k?	of
Illinois	Illinois	k1gInSc1	Illinois
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k1gInSc4	stát
nacházející	nacházející	k2eAgInSc4d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
východních	východní	k2eAgInPc2d1	východní
severních	severní	k2eAgInPc2d1	severní
států	stát	k1gInPc2	stát
ve	v	k7c6	v
středozápadním	středozápadní	k2eAgInSc6d1	středozápadní
regionu	region	k1gInSc6	region
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Illinois	Illinois	k1gFnSc1	Illinois
hraničí	hraničit	k5eAaImIp3nS	hraničit
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Indianou	Indiana	k1gFnSc7	Indiana
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
s	s	k7c7	s
Kentucky	Kentuck	k1gInPc7	Kentuck
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
Missouri	Missouri	k1gFnSc7	Missouri
a	a	k8xC	a
Iowou	Iowa	k1gFnSc7	Iowa
a	a	k8xC	a
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
Wisconsinem	Wisconsin	k1gInSc7	Wisconsin
<g/>
.	.	kIx.	.
</s>
<s>
Severovýchodní	severovýchodní	k2eAgNnSc1d1	severovýchodní
ohraničení	ohraničení	k1gNnSc1	ohraničení
tvoří	tvořit	k5eAaImIp3nS	tvořit
Michiganské	michiganský	k2eAgNnSc1d1	Michiganské
jezero	jezero	k1gNnSc1	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rozlohou	rozloha	k1gFnSc7	rozloha
149	[number]	k4	149
932	[number]	k4	932
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
Illinois	Illinois	k1gInSc4	Illinois
25	[number]	k4	25
<g/>
.	.	kIx.	.
největším	veliký	k2eAgInSc7d3	veliký
státem	stát	k1gInSc7	stát
USA	USA	kA	USA
<g/>
,	,	kIx,	,
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
12,8	[number]	k4	12,8
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pátým	pátý	k4xOgInSc7	pátý
nejlidnatějším	lidnatý	k2eAgInSc7d3	nejlidnatější
státem	stát	k1gInSc7	stát
a	a	k8xC	a
s	s	k7c7	s
hodnotou	hodnota	k1gFnSc7	hodnota
hustoty	hustota	k1gFnSc2	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
89	[number]	k4	89
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c6	na
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
dvanáctém	dvanáctý	k4xOgInSc6	dvanáctý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Springfield	Springfield	k1gInSc1	Springfield
se	s	k7c7	s
120	[number]	k4	120
tisíci	tisíc	k4xCgInPc7	tisíc
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgNnPc7d3	veliký
městy	město	k1gNnPc7	město
jsou	být	k5eAaImIp3nP	být
Chicago	Chicago	k1gNnSc4	Chicago
s	s	k7c7	s
2,7	[number]	k4	2,7
miliony	milion	k4xCgInPc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Aurora	Aurora	k1gFnSc1	Aurora
(	(	kIx(	(
<g/>
200	[number]	k4	200
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rockford	Rockford	k1gMnSc1	Rockford
(	(	kIx(	(
<g/>
150	[number]	k4	150
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Joliet	Joliet	k1gMnSc1	Joliet
(	(	kIx(	(
<g/>
150	[number]	k4	150
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
Naperville	Naperville	k1gFnSc1	Naperville
(	(	kIx(	(
<g/>
150	[number]	k4	150
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Illinois	Illinois	k1gFnSc1	Illinois
patří	patřit	k5eAaImIp3nS	patřit
101	[number]	k4	101
km	km	kA	km
pobřeží	pobřeží	k1gNnSc4	pobřeží
Michiganského	michiganský	k2eAgNnSc2d1	Michiganské
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
vrchol	vrchol	k1gInSc1	vrchol
Charles	Charles	k1gMnSc1	Charles
Mound	Mound	k1gMnSc1	Mound
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
376	[number]	k4	376
m	m	kA	m
na	na	k7c6	na
severu	sever	k1gInSc6	sever
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgInPc7d3	veliký
toky	tok	k1gInPc7	tok
jsou	být	k5eAaImIp3nP	být
řeky	řeka	k1gFnSc2	řeka
Mississippi	Mississippi	k1gFnSc2	Mississippi
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Iowou	Iowa	k1gFnSc7	Iowa
a	a	k8xC	a
Missouri	Missouri	k1gFnSc7	Missouri
<g/>
,	,	kIx,	,
Ohio	Ohio	k1gNnSc1	Ohio
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
je	být	k5eAaImIp3nS	být
hranicí	hranice	k1gFnSc7	hranice
s	s	k7c7	s
Kentucky	Kentuck	k1gInPc7	Kentuck
<g/>
,	,	kIx,	,
a	a	k8xC	a
Wabash	Wabash	k1gInSc1	Wabash
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
část	část	k1gFnSc4	část
hranice	hranice	k1gFnSc2	hranice
s	s	k7c7	s
Indianou	Indiana	k1gFnSc7	Indiana
<g/>
.	.	kIx.	.
</s>
<s>
Francouzští	francouzský	k2eAgMnPc1d1	francouzský
průzkumníci	průzkumník	k1gMnPc1	průzkumník
objevovali	objevovat	k5eAaImAgMnP	objevovat
okolí	okolí	k1gNnSc4	okolí
řeky	řeka	k1gFnSc2	řeka
Illinois	Illinois	k1gFnSc2	Illinois
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1673	[number]	k4	1673
a	a	k8xC	a
záhy	záhy	k6eAd1	záhy
se	se	k3xPyFc4	se
území	území	k1gNnSc1	území
stalo	stát	k5eAaPmAgNnS	stát
součástí	součást	k1gFnSc7	součást
Nové	Nové	k2eAgFnSc2d1	Nové
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
regionu	region	k1gInSc2	region
byl	být	k5eAaImAgInS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
ze	z	k7c2	z
slova	slovo	k1gNnSc2	slovo
irenwe	irenw	k1gFnSc2	irenw
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
mluví	mluvit	k5eAaImIp3nS	mluvit
běžnou	běžný	k2eAgFnSc7d1	běžná
řečí	řeč	k1gFnSc7	řeč
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
z	z	k7c2	z
miamsko-illinoiského	miamskollinoiský	k2eAgInSc2d1	miamsko-illinoiský
jazyka	jazyk	k1gInSc2	jazyk
místních	místní	k2eAgMnPc2d1	místní
indiánů	indián	k1gMnPc2	indián
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgNnSc1d1	zdejší
evropské	evropský	k2eAgNnSc1d1	Evropské
osídlení	osídlení	k1gNnSc1	osídlení
ale	ale	k9	ale
nebylo	být	k5eNaImAgNnS	být
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
desetiletích	desetiletí	k1gNnPc6	desetiletí
početné	početný	k2eAgFnSc2d1	početná
ani	ani	k8xC	ani
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1763	[number]	k4	1763
získali	získat	k5eAaPmAgMnP	získat
Illinois	Illinois	k1gFnSc4	Illinois
díky	díky	k7c3	díky
výsledku	výsledek	k1gInSc3	výsledek
sedmileté	sedmiletý	k2eAgFnSc2d1	sedmiletá
války	válka	k1gFnSc2	válka
Britové	Brit	k1gMnPc1	Brit
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1783	[number]	k4	1783
se	se	k3xPyFc4	se
území	území	k1gNnSc2	území
stalo	stát	k5eAaPmAgNnS	stát
součástí	součást	k1gFnSc7	součást
nově	nově	k6eAd1	nově
vzniklých	vzniklý	k2eAgInPc2d1	vzniklý
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
do	do	k7c2	do
Severozápadního	severozápadní	k2eAgNnSc2d1	severozápadní
teritoria	teritorium	k1gNnSc2	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1800	[number]	k4	1800
tvořilo	tvořit	k5eAaImAgNnS	tvořit
část	část	k1gFnSc4	část
indianského	indianský	k2eAgNnSc2d1	indianský
teritoria	teritorium	k1gNnSc2	teritorium
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1809	[number]	k4	1809
vlastního	vlastní	k2eAgNnSc2d1	vlastní
illinoiského	illinoiské	k1gNnSc2	illinoiské
teritoria	teritorium	k1gNnSc2	teritorium
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
severní	severní	k2eAgFnSc1d1	severní
oblast	oblast	k1gFnSc1	oblast
(	(	kIx(	(
<g/>
pozdější	pozdní	k2eAgInSc1d2	pozdější
Wisconsin	Wisconsin	k1gInSc1	Wisconsin
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1818	[number]	k4	1818
připojena	připojen	k2eAgFnSc1d1	připojena
k	k	k7c3	k
michiganskému	michiganský	k2eAgNnSc3d1	Michiganské
teritoriu	teritorium	k1gNnSc3	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
Illinois	Illinois	k1gInSc1	Illinois
se	s	k7c7	s
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1818	[number]	k4	1818
stalo	stát	k5eAaPmAgNnS	stát
21	[number]	k4	21
<g/>
.	.	kIx.	.
státem	stát	k1gInSc7	stát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
v	v	k7c6	v
Illinois	Illinois	k1gFnSc6	Illinois
žilo	žít	k5eAaImAgNnS	žít
12	[number]	k4	12
830	[number]	k4	830
632	[number]	k4	632
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
71,5	[number]	k4	71,5
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
14,5	[number]	k4	14,5
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,3	[number]	k4	0,3
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
4,6	[number]	k4	4,6
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,0	[number]	k4	0,0
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
6,7	[number]	k4	6,7
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
2,3	[number]	k4	2,3
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
15,8	[number]	k4	15,8
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
skupinou	skupina	k1gFnSc7	skupina
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
protestanti	protestant	k1gMnPc1	protestant
<g/>
,	,	kIx,	,
zvlášť	zvlášť	k6eAd1	zvlášť
kolem	kolem	k7c2	kolem
Chicaga	Chicago	k1gNnSc2	Chicago
je	být	k5eAaImIp3nS	být
silná	silný	k2eAgFnSc1d1	silná
i	i	k8xC	i
katolická	katolický	k2eAgFnSc1d1	katolická
komunita	komunita	k1gFnSc1	komunita
(	(	kIx(	(
Poláci	Polák	k1gMnPc1	Polák
<g/>
,	,	kIx,	,
Irové	Ir	k1gMnPc1	Ir
<g/>
,	,	kIx,	,
Němci	Němec	k1gMnPc1	Němec
a	a	k8xC	a
Italové	Ital	k1gMnPc1	Ital
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zde	zde	k6eAd1	zde
mají	mít	k5eAaImIp3nP	mít
své	svůj	k3xOyFgFnPc4	svůj
specifické	specifický	k2eAgFnPc4d1	specifická
čtvrtě	čtvrt	k1gFnPc4	čtvrt
)	)	kIx)	)
a	a	k8xC	a
celkem	celkem	k6eAd1	celkem
velkou	velký	k2eAgFnSc7d1	velká
skupinou	skupina	k1gFnSc7	skupina
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Chicaga	Chicago	k1gNnSc2	Chicago
i	i	k8xC	i
muslimové	muslim	k1gMnPc1	muslim
<g/>
.	.	kIx.	.
křesťané	křesťan	k1gMnPc1	křesťan
80	[number]	k4	80
%	%	kIx~	%
protestanti	protestant	k1gMnPc1	protestant
<g />
.	.	kIx.	.
</s>
<s>
49	[number]	k4	49
%	%	kIx~	%
baptisté	baptista	k1gMnPc1	baptista
12	[number]	k4	12
%	%	kIx~	%
metodisté	metodista	k1gMnPc1	metodista
7	[number]	k4	7
%	%	kIx~	%
luteráni	luterán	k1gMnPc1	luterán
7	[number]	k4	7
%	%	kIx~	%
presbyteriáni	presbyterián	k1gMnPc1	presbyterián
3	[number]	k4	3
%	%	kIx~	%
ostatní	ostatní	k2eAgMnPc1d1	ostatní
protestanti	protestant	k1gMnPc1	protestant
20	[number]	k4	20
%	%	kIx~	%
římští	římský	k2eAgMnPc1d1	římský
katolíci	katolík	k1gMnPc1	katolík
30	[number]	k4	30
%	%	kIx~	%
ostatní	ostatní	k2eAgMnPc1d1	ostatní
křesťané	křesťan	k1gMnPc1	křesťan
1	[number]	k4	1
%	%	kIx~	%
jiná	jiný	k2eAgNnPc4d1	jiné
náboženství	náboženství	k1gNnPc4	náboženství
4	[number]	k4	4
%	%	kIx~	%
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnPc2	vyznání
16	[number]	k4	16
%	%	kIx~	%
Nejvýznamnějšími	významný	k2eAgInPc7d3	nejvýznamnější
zemědělskými	zemědělský	k2eAgInPc7d1	zemědělský
produkty	produkt	k1gInPc7	produkt
jsou	být	k5eAaImIp3nP	být
kukuřice	kukuřice	k1gFnSc2	kukuřice
<g/>
,	,	kIx,	,
sójové	sójový	k2eAgInPc1d1	sójový
boby	bob	k1gInPc1	bob
<g/>
,	,	kIx,	,
skot	skot	k1gInSc1	skot
<g/>
,	,	kIx,	,
mléčné	mléčný	k2eAgInPc1d1	mléčný
produkty	produkt	k1gInPc1	produkt
a	a	k8xC	a
pšenice	pšenice	k1gFnPc1	pšenice
<g/>
,	,	kIx,	,
zemědělství	zemědělství	k1gNnSc1	zemědělství
se	se	k3xPyFc4	se
nejvíce	nejvíce	k6eAd1	nejvíce
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nejrozšířenějšími	rozšířený	k2eAgFnPc7d3	nejrozšířenější
průmyslovými	průmyslový	k2eAgFnPc7d1	průmyslová
odvětvími	odvětví	k1gNnPc7	odvětví
jsou	být	k5eAaImIp3nP	být
strojírenství	strojírenství	k1gNnPc1	strojírenství
<g/>
,	,	kIx,	,
potravinářství	potravinářství	k1gNnPc1	potravinářství
<g/>
,	,	kIx,	,
chemický	chemický	k2eAgInSc1d1	chemický
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
elektrotechnický	elektrotechnický	k2eAgInSc1d1	elektrotechnický
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
metalurgie	metalurgie	k1gFnSc1	metalurgie
barevných	barevný	k2eAgInPc2d1	barevný
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
dopravní	dopravní	k2eAgNnPc1d1	dopravní
strojírenství	strojírenství	k1gNnPc1	strojírenství
<g/>
.	.	kIx.	.
</s>
<s>
Důležitá	důležitý	k2eAgNnPc1d1	důležité
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
naleziště	naleziště	k1gNnSc4	naleziště
uhlí	uhlí	k1gNnSc2	uhlí
a	a	k8xC	a
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Illinois	Illinois	k1gFnSc1	Illinois
je	být	k5eAaImIp3nS	být
i	i	k9	i
významným	významný	k2eAgInSc7d1	významný
dopravním	dopravní	k2eAgInSc7d1	dopravní
uzlem	uzel	k1gInSc7	uzel
<g/>
.	.	kIx.	.
</s>
<s>
Chicagský	chicagský	k2eAgInSc1d1	chicagský
přístav	přístav	k1gInSc1	přístav
spojuje	spojovat	k5eAaImIp3nS	spojovat
stát	stát	k5eAaPmF	stát
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
světovými	světový	k2eAgInPc7d1	světový
přístavy	přístav	k1gInPc7	přístav
a	a	k8xC	a
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Hare	Hare	k1gInSc4	Hare
International	International	k1gMnPc2	International
Airport	Airport	k1gInSc1	Airport
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
je	být	k5eAaImIp3nS	být
jedím	jedet	k5eAaImIp1nS	jedet
z	z	k7c2	z
nejfrekventovanějších	frekventovaný	k2eAgNnPc2d3	nejfrekventovanější
letišť	letiště	k1gNnPc2	letiště
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mottem	motto	k1gNnSc7	motto
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
State	status	k1gInSc5	status
Sovereignty-National	Sovereignty-National	k1gMnSc1	Sovereignty-National
Union	union	k1gInSc1	union
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
květinou	květina	k1gFnSc7	květina
violka	violka	k1gFnSc1	violka
<g/>
,	,	kIx,	,
stromem	strom	k1gInSc7	strom
dub	dub	k1gInSc1	dub
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
ptákem	pták	k1gMnSc7	pták
kardinál	kardinál	k1gMnSc1	kardinál
a	a	k8xC	a
písní	píseň	k1gFnPc2	píseň
Illinois	Illinois	k1gFnSc2	Illinois
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2011	[number]	k4	2011
zrušilo	zrušit	k5eAaPmAgNnS	zrušit
Illinois	Illinois	k1gFnSc4	Illinois
trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
