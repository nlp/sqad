<s>
Space	Spako	k6eAd1	Spako
Exploration	Exploration	k1gInSc1	Exploration
Technologies	Technologiesa	k1gFnPc2	Technologiesa
Corporation	Corporation	k1gInSc1	Corporation
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
SpaceX	SpaceX	k1gFnSc1	SpaceX
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
americkou	americký	k2eAgFnSc7d1	americká
technologickou	technologický	k2eAgFnSc7d1	technologická
společností	společnost	k1gFnSc7	společnost
působící	působící	k2eAgFnSc7d1	působící
v	v	k7c6	v
aerokosmickém	aerokosmický	k2eAgInSc6d1	aerokosmický
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
založil	založit	k5eAaPmAgMnS	založit
Elon	Elon	k1gMnSc1	Elon
Musk	Musk	k1gMnSc1	Musk
z	z	k7c2	z
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
vydělal	vydělat	k5eAaPmAgInS	vydělat
na	na	k7c6	na
prodeji	prodej	k1gInSc6	prodej
svého	svůj	k3xOyFgInSc2	svůj
podílu	podíl	k1gInSc2	podíl
v	v	k7c6	v
systému	systém	k1gInSc6	systém
PayPal	PayPal	k1gInSc1	PayPal
<g/>
.	.	kIx.	.
</s>
