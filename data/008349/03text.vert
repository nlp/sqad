<p>
<s>
Space	Spako	k6eAd1	Spako
Exploration	Exploration	k1gInSc1	Exploration
Technologies	Technologiesa	k1gFnPc2	Technologiesa
Corporation	Corporation	k1gInSc1	Corporation
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
SpaceX	SpaceX	k1gFnSc1	SpaceX
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
americkou	americký	k2eAgFnSc7d1	americká
technologickou	technologický	k2eAgFnSc7d1	technologická
společností	společnost	k1gFnSc7	společnost
působící	působící	k2eAgFnSc7d1	působící
v	v	k7c6	v
aerokosmickém	aerokosmický	k2eAgInSc6d1	aerokosmický
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
založil	založit	k5eAaPmAgMnS	založit
Elon	Elon	k1gMnSc1	Elon
Musk	Musk	k1gMnSc1	Musk
z	z	k7c2	z
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
vydělal	vydělat	k5eAaPmAgInS	vydělat
na	na	k7c6	na
prodeji	prodej	k1gInSc6	prodej
svého	svůj	k3xOyFgInSc2	svůj
podílu	podíl	k1gInSc2	podíl
v	v	k7c6	v
systému	systém	k1gInSc6	systém
PayPal	PayPal	k1gInSc1	PayPal
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
raketové	raketový	k2eAgInPc4d1	raketový
nosiče	nosič	k1gInPc4	nosič
Falcon	Falcon	k1gInSc1	Falcon
1	[number]	k4	1
<g/>
,	,	kIx,	,
Falcon	Falcon	k1gInSc1	Falcon
9	[number]	k4	9
,	,	kIx,	,
těžkou	těžký	k2eAgFnSc4d1	těžká
nosnou	nosný	k2eAgFnSc4d1	nosná
raketu	raketa	k1gFnSc4	raketa
Falcon	Falcona	k1gFnPc2	Falcona
Heavy	Heava	k1gFnSc2	Heava
a	a	k8xC	a
kosmickou	kosmický	k2eAgFnSc4d1	kosmická
loď	loď	k1gFnSc4	loď
Dragon	Dragon	k1gMnSc1	Dragon
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
s	s	k7c7	s
komerčním	komerční	k2eAgNnSc7d1	komerční
vypouštěním	vypouštění	k1gNnSc7	vypouštění
telekomunikačních	telekomunikační	k2eAgFnPc2d1	telekomunikační
družic	družice	k1gFnPc2	družice
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
druhou	druhý	k4xOgFnSc4	druhý
generaci	generace	k1gFnSc4	generace
lodi	loď	k1gFnSc2	loď
Dragon	Dragon	k1gMnSc1	Dragon
<g/>
,	,	kIx,	,
Dragon	Dragon	k1gMnSc1	Dragon
2	[number]	k4	2
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
jak	jak	k6eAd1	jak
nákladní	nákladní	k2eAgNnSc1d1	nákladní
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
i	i	k9	i
pilotovanou	pilotovaný	k2eAgFnSc4d1	pilotovaná
verzi	verze	k1gFnSc4	verze
a	a	k8xC	a
supertěžký	supertěžký	k2eAgInSc1d1	supertěžký
raketový	raketový	k2eAgInSc1d1	raketový
systém	systém	k1gInSc1	systém
BFR	BFR	kA	BFR
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
vyjádření	vyjádření	k1gNnSc2	vyjádření
Elona	Eloen	k2eAgFnSc1d1	Elona
Muska	Muska	k1gFnSc1	Muska
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
cílem	cíl	k1gInSc7	cíl
společnosti	společnost	k1gFnSc2	společnost
významně	významně	k6eAd1	významně
snížit	snížit	k5eAaPmF	snížit
náklady	náklad	k1gInPc4	náklad
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
umožnit	umožnit	k5eAaPmF	umožnit
lidstvu	lidstvo	k1gNnSc3	lidstvo
kolonizaci	kolonizace	k1gFnSc4	kolonizace
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
firma	firma	k1gFnSc1	firma
SpaceX	SpaceX	k1gFnSc2	SpaceX
řady	řada	k1gFnSc2	řada
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgFnPc1	první
soukromé	soukromý	k2eAgFnPc1d1	soukromá
společnosti	společnost	k1gFnPc1	společnost
na	na	k7c6	na
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2008	[number]	k4	2008
povedlo	povést	k5eAaPmAgNnS	povést
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
orbitální	orbitální	k2eAgFnPc4d1	orbitální
dráhy	dráha	k1gFnPc4	dráha
Země	zem	k1gFnSc2	zem
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
motorů	motor	k1gInPc2	motor
na	na	k7c4	na
kapalné	kapalný	k2eAgNnSc4d1	kapalné
palivo	palivo	k1gNnSc4	palivo
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
jako	jako	k8xS	jako
první	první	k4xOgFnSc2	první
soukromé	soukromý	k2eAgFnSc2d1	soukromá
společnosti	společnost	k1gFnSc2	společnost
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
povedlo	povést	k5eAaPmAgNnS	povést
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2010	[number]	k4	2010
vyslat	vyslat	k5eAaPmF	vyslat
na	na	k7c4	na
orbitu	orbita	k1gFnSc4	orbita
stroj	stroj	k1gInSc1	stroj
a	a	k8xC	a
pak	pak	k6eAd1	pak
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
přistát	přistát	k5eAaImF	přistát
ve	v	k7c6	v
vodách	voda	k1gFnPc6	voda
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
soukromá	soukromý	k2eAgFnSc1d1	soukromá
firma	firma	k1gFnSc1	firma
úspěšně	úspěšně	k6eAd1	úspěšně
vyslala	vyslat	k5eAaPmAgFnS	vyslat
kosmickou	kosmický	k2eAgFnSc4d1	kosmická
loď	loď	k1gFnSc4	loď
k	k	k7c3	k
Mezinárodní	mezinárodní	k2eAgFnSc3d1	mezinárodní
vesmírné	vesmírný	k2eAgFnSc3d1	vesmírná
stanici	stanice	k1gFnSc3	stanice
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2013	[number]	k4	2013
se	se	k3xPyFc4	se
zapsala	zapsat	k5eAaPmAgFnS	zapsat
do	do	k7c2	do
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
když	když	k8xS	když
jako	jako	k8xS	jako
první	první	k4xOgFnSc1	první
soukromá	soukromý	k2eAgFnSc1d1	soukromá
firma	firma	k1gFnSc1	firma
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
geosynchronní	geosynchronní	k2eAgFnPc4d1	geosynchronní
dráhy	dráha	k1gFnPc4	dráha
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
startu	start	k1gInSc2	start
mise	mise	k1gFnSc2	mise
SES-	SES-	k1gFnSc1	SES-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2015	[number]	k4	2015
společnost	společnost	k1gFnSc1	společnost
získala	získat	k5eAaPmAgFnS	získat
další	další	k2eAgNnSc4d1	další
důležité	důležitý	k2eAgNnSc4d1	důležité
prvenství	prvenství	k1gNnSc4	prvenství
-	-	kIx~	-
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
přistát	přistát	k5eAaPmF	přistát
s	s	k7c7	s
prvním	první	k4xOgInSc7	první
stupněm	stupeň	k1gInSc7	stupeň
rakety	raketa	k1gFnSc2	raketa
Falcon	Falcon	k1gInSc1	Falcon
9	[number]	k4	9
FT	FT	kA	FT
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
31	[number]	k4	31
<g/>
.	.	kIx.	.
3	[number]	k4	3
2017	[number]	k4	2017
došlo	dojít	k5eAaPmAgNnS	dojít
při	při	k7c6	při
misi	mise	k1gFnSc6	mise
SES-10	SES-10	k1gFnSc2	SES-10
k	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
znovupoužití	znovupoužití	k1gNnSc3	znovupoužití
již	již	k6eAd1	již
letěného	letěný	k2eAgInSc2d1	letěný
stupně	stupeň	k1gInSc2	stupeň
(	(	kIx(	(
<g/>
z	z	k7c2	z
CRS-	CRS-	k1gFnSc2	CRS-
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oproti	oproti	k7c3	oproti
vládním	vládní	k2eAgFnPc3d1	vládní
agenturám	agentura	k1gFnPc3	agentura
i	i	k8xC	i
konkurenčním	konkurenční	k2eAgFnPc3d1	konkurenční
firmám	firma	k1gFnPc3	firma
využívá	využívat	k5eAaImIp3nS	využívat
SpaceX	SpaceX	k1gFnSc1	SpaceX
systém	systém	k1gInSc4	systém
stavby	stavba	k1gFnSc2	stavba
raket	raketa	k1gFnPc2	raketa
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
celý	celý	k2eAgInSc1d1	celý
konstrukční	konstrukční	k2eAgInSc1d1	konstrukční
proces	proces	k1gInSc1	proces
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jedné	jeden	k4xCgFnSc2	jeden
továrny	továrna	k1gFnSc2	továrna
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
SpaceX	SpaceX	k?	SpaceX
tak	tak	k9	tak
není	být	k5eNaImIp3nS	být
nucena	nutit	k5eAaImNgFnS	nutit
převážet	převážet	k5eAaImF	převážet
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
díly	díl	k1gInPc4	díl
rakety	raketa	k1gFnSc2	raketa
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
USA	USA	kA	USA
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
nákladů	náklad	k1gInPc2	náklad
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
raket	raketa	k1gFnPc2	raketa
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
zlevňování	zlevňování	k1gNnSc1	zlevňování
startů	start	k1gInPc2	start
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
přístup	přístup	k1gInSc1	přístup
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
frekvencí	frekvence	k1gFnSc7	frekvence
startů	start	k1gInPc2	start
(	(	kIx(	(
<g/>
SpaceX	SpaceX	k1gMnSc1	SpaceX
plánuje	plánovat	k5eAaImIp3nS	plánovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
vykonat	vykonat	k5eAaPmF	vykonat
téměř	téměř	k6eAd1	téměř
40	[number]	k4	40
startů	start	k1gInPc2	start
raket	raketa	k1gFnPc2	raketa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
firmě	firma	k1gFnSc3	firma
snižovat	snižovat	k5eAaImF	snižovat
náklady	náklad	k1gInPc4	náklad
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
polovině	polovina	k1gFnSc3	polovina
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
firmě	firma	k1gFnSc3	firma
SpaceX	SpaceX	k1gMnPc2	SpaceX
snížit	snížit	k5eAaPmF	snížit
náklady	náklad	k1gInPc4	náklad
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
start	start	k1gInSc4	start
rakety	raketa	k1gFnSc2	raketa
Falcon	Falcon	k1gInSc4	Falcon
9	[number]	k4	9
na	na	k7c4	na
nízkou	nízký	k2eAgFnSc4d1	nízká
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
Země	zem	k1gFnSc2	zem
na	na	k7c4	na
přibližně	přibližně	k6eAd1	přibližně
60	[number]	k4	60
miliónů	milión	k4xCgInPc2	milión
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
zlevňování	zlevňování	k1gNnSc4	zlevňování
postupně	postupně	k6eAd1	postupně
reaguje	reagovat	k5eAaBmIp3nS	reagovat
i	i	k9	i
konkurence	konkurence	k1gFnSc1	konkurence
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
společnost	společnost	k1gFnSc1	společnost
Arianespace	Arianespace	k1gFnSc2	Arianespace
<g/>
,	,	kIx,	,
světový	světový	k2eAgMnSc1d1	světový
leader	leader	k1gMnSc1	leader
v	v	k7c6	v
kosmické	kosmický	k2eAgFnSc6d1	kosmická
dopravě	doprava	k1gFnSc6	doprava
<g/>
,	,	kIx,	,
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
klesající	klesající	k2eAgInPc4d1	klesající
náklady	náklad	k1gInPc4	náklad
společnosti	společnost	k1gFnSc2	společnost
SpaceX	SpaceX	k1gFnSc7	SpaceX
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
přistoupit	přistoupit	k5eAaPmF	přistoupit
na	na	k7c6	na
zlevnění	zlevnění	k1gNnSc6	zlevnění
startů	start	k1gInPc2	start
svých	svůj	k3xOyFgFnPc2	svůj
raket	raketa	k1gFnPc2	raketa
na	na	k7c4	na
geostacionární	geostacionární	k2eAgFnSc4d1	geostacionární
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
<g/>
SpaceX	SpaceX	k1gFnSc1	SpaceX
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
v	v	k7c6	v
dlouhodobém	dlouhodobý	k2eAgInSc6d1	dlouhodobý
plánu	plán	k1gInSc6	plán
zkonstruovat	zkonstruovat	k5eAaPmF	zkonstruovat
raketu	raketa	k1gFnSc4	raketa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dala	dát	k5eAaPmAgFnS	dát
opakovaně	opakovaně	k6eAd1	opakovaně
použít	použít	k5eAaPmF	použít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2015	[number]	k4	2015
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgInS	odehrát
test	test	k1gInSc1	test
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
zásobovacího	zásobovací	k2eAgInSc2d1	zásobovací
letu	let	k1gInSc2	let
lodě	loď	k1gFnSc2	loď
Dragon	Dragon	k1gMnSc1	Dragon
k	k	k7c3	k
ISS	ISS	kA	ISS
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
společnost	společnost	k1gFnSc1	společnost
pokusila	pokusit	k5eAaPmAgFnS	pokusit
o	o	k7c6	o
přistání	přistání	k1gNnSc6	přistání
prvního	první	k4xOgInSc2	první
stupně	stupeň	k1gInSc2	stupeň
rakety	raketa	k1gFnPc1	raketa
Falcon	Falcon	k1gNnSc1	Falcon
9	[number]	k4	9
na	na	k7c4	na
plošinu	plošina	k1gFnSc4	plošina
umístěnou	umístěný	k2eAgFnSc7d1	umístěná
v	v	k7c6	v
Atlantském	atlantský	k2eAgInSc6d1	atlantský
oceánu	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Test	test	k1gInSc1	test
skončil	skončit	k5eAaPmAgInS	skončit
dílčím	dílčí	k2eAgInSc7d1	dílčí
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
povedlo	povést	k5eAaPmAgNnS	povést
první	první	k4xOgInSc4	první
stupeň	stupeň	k1gInSc4	stupeň
navést	navést	k5eAaPmF	navést
na	na	k7c4	na
plošinu	plošina	k1gFnSc4	plošina
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
během	během	k7c2	během
závěrečného	závěrečný	k2eAgInSc2d1	závěrečný
manévru	manévr	k1gInSc2	manévr
se	se	k3xPyFc4	se
raketa	raketa	k1gFnSc1	raketa
ve	v	k7c6	v
vyšší	vysoký	k2eAgFnSc6d2	vyšší
rychlosti	rychlost	k1gFnSc6	rychlost
než	než	k8xS	než
bylo	být	k5eAaImAgNnS	být
plánováno	plánován	k2eAgNnSc4d1	plánováno
zřítila	zřítit	k5eAaPmAgFnS	zřítit
na	na	k7c4	na
plošinu	plošina	k1gFnSc4	plošina
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
úspěšně	úspěšně	k6eAd1	úspěšně
podaří	podařit	k5eAaPmIp3nS	podařit
zvládnout	zvládnout	k5eAaPmF	zvládnout
měkké	měkký	k2eAgNnSc4d1	měkké
přistání	přistání	k1gNnSc4	přistání
rakety	raketa	k1gFnSc2	raketa
po	po	k7c6	po
použití	použití	k1gNnSc6	použití
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
cena	cena	k1gFnSc1	cena
startu	start	k1gInSc6	start
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
sníží	snížit	k5eAaPmIp3nS	snížit
na	na	k7c4	na
5	[number]	k4	5
až	až	k9	až
7	[number]	k4	7
miliónů	milión	k4xCgInPc2	milión
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
SpaceX	SpaceX	k?	SpaceX
plánuje	plánovat	k5eAaImIp3nS	plánovat
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
demonstrativní	demonstrativní	k2eAgFnSc4d1	demonstrativní
pilotovanou	pilotovaný	k2eAgFnSc4d1	pilotovaná
misi	mise	k1gFnSc4	mise
Crew	Crew	k1gMnSc1	Crew
Dragon	Dragon	k1gMnSc1	Dragon
2	[number]	k4	2
na	na	k7c4	na
ISS	ISS	kA	ISS
na	na	k7c4	na
červenec	červenec	k1gInSc4	červenec
2019	[number]	k4	2019
a	a	k8xC	a
první	první	k4xOgInSc4	první
operační	operační	k2eAgInSc4d1	operační
pilotovaný	pilotovaný	k2eAgInSc4d1	pilotovaný
let	let	k1gInSc4	let
na	na	k7c6	na
ISS	ISS	kA	ISS
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2019	[number]	k4	2019
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
firmě	firma	k1gFnSc6	firma
==	==	k?	==
</s>
</p>
<p>
<s>
Firma	firma	k1gFnSc1	firma
sídlí	sídlet	k5eAaImIp3nS	sídlet
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Hawthorne	Hawthorn	k1gInSc5	Hawthorn
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
zaměstnávala	zaměstnávat	k5eAaImAgFnS	zaměstnávat
přes	přes	k7c4	přes
1250	[number]	k4	1250
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2013	[number]	k4	2013
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
3800	[number]	k4	3800
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2006	[number]	k4	2006
společnost	společnost	k1gFnSc1	společnost
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
iniciativy	iniciativa	k1gFnSc2	iniciativa
COTS	COTS	kA	COTS
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
kontrakt	kontrakt	k1gInSc4	kontrakt
vyhlášený	vyhlášený	k2eAgInSc1d1	vyhlášený
americkou	americký	k2eAgFnSc7d1	americká
kosmickou	kosmický	k2eAgFnSc7d1	kosmická
agenturou	agentura	k1gFnSc7	agentura
NASA	NASA	kA	NASA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tohoto	tento	k3xDgInSc2	tento
kontraktu	kontrakt	k1gInSc2	kontrakt
byla	být	k5eAaImAgFnS	být
společnosti	společnost	k1gFnPc4	společnost
poskytnuta	poskytnut	k2eAgFnSc1d1	poskytnuta
dotace	dotace	k1gFnSc1	dotace
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
až	až	k9	až
278	[number]	k4	278
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
dolarů	dolar	k1gInPc2	dolar
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
a	a	k8xC	a
demonstrační	demonstrační	k2eAgInPc4d1	demonstrační
lety	let	k1gInPc4	let
kosmického	kosmický	k2eAgInSc2d1	kosmický
dopravního	dopravní	k2eAgInSc2d1	dopravní
systému	systém	k1gInSc2	systém
složeného	složený	k2eAgInSc2d1	složený
z	z	k7c2	z
rakety	raketa	k1gFnSc2	raketa
Falcon	Falcon	k1gNnSc1	Falcon
9	[number]	k4	9
a	a	k8xC	a
lodi	loď	k1gFnSc2	loď
Dragon	Dragon	k1gMnSc1	Dragon
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
definitivně	definitivně	k6eAd1	definitivně
ukončen	ukončen	k2eAgInSc1d1	ukončen
provoz	provoz	k1gInSc1	provoz
amerických	americký	k2eAgInPc2d1	americký
raketoplánů	raketoplán	k1gInPc2	raketoplán
<g/>
,	,	kIx,	,
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
alternativní	alternativní	k2eAgFnSc4d1	alternativní
dopravu	doprava	k1gFnSc4	doprava
nákladů	náklad	k1gInPc2	náklad
na	na	k7c4	na
stanici	stanice	k1gFnSc4	stanice
ISS	ISS	kA	ISS
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
23	[number]	k4	23
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2008	[number]	k4	2008
společnost	společnost	k1gFnSc1	společnost
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
kontrakt	kontrakt	k1gInSc4	kontrakt
CRS	CRS	kA	CRS
(	(	kIx(	(
<g/>
Commercial	Commercial	k1gMnSc1	Commercial
Resupply	Resupply	k1gMnSc1	Resupply
Services	Services	k1gMnSc1	Services
<g/>
)	)	kIx)	)
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
1,6	[number]	k4	1,6
mld.	mld.	k?	mld.
USD	USD	kA	USD
pro	pro	k7c4	pro
financování	financování	k1gNnSc4	financování
12	[number]	k4	12
dopravních	dopravní	k2eAgInPc2d1	dopravní
letů	let	k1gInPc2	let
ke	k	k7c3	k
stanici	stanice	k1gFnSc3	stanice
ISS	ISS	kA	ISS
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
byla	být	k5eAaImAgFnS	být
společnost	společnost	k1gFnSc1	společnost
reprezentována	reprezentovat	k5eAaImNgFnS	reprezentovat
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
také	také	k9	také
Gwynne	Gwynn	k1gInSc5	Gwynn
Shotwellovou	Shotwellová	k1gFnSc7	Shotwellová
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
šéf	šéf	k1gMnSc1	šéf
firmy	firma	k1gFnSc2	firma
Elon	Elon	k1gMnSc1	Elon
Musk	Musk	k1gMnSc1	Musk
představil	představit	k5eAaPmAgMnS	představit
projekt	projekt	k1gInSc4	projekt
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
vybudovat	vybudovat	k5eAaPmF	vybudovat
kolonii	kolonie	k1gFnSc4	kolonie
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
pro	pro	k7c4	pro
80	[number]	k4	80
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2014	[number]	k4	2014
představil	představit	k5eAaPmAgInS	představit
veřejnosti	veřejnost	k1gFnSc2	veřejnost
loď	loď	k1gFnSc1	loď
Dragon	Dragon	k1gMnSc1	Dragon
V	V	kA	V
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k8xC	i
ta	ten	k3xDgFnSc1	ten
bude	být	k5eAaImBp3nS	být
dopravována	dopravovat	k5eAaImNgFnS	dopravovat
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
raketou	raketa	k1gFnSc7	raketa
Falcon	Falcon	k1gNnSc1	Falcon
9	[number]	k4	9
v	v	k7c4	v
<g/>
1.2	[number]	k4	1.2
Block	Blocko	k1gNnPc2	Blocko
514	[number]	k4	514
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2016	[number]	k4	2016
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
společnost	společnost	k1gFnSc1	společnost
kontrakt	kontrakt	k1gInSc1	kontrakt
CRS	CRS	kA	CRS
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
po	po	k7c6	po
boku	bok	k1gInSc6	bok
soukromých	soukromý	k2eAgFnPc2d1	soukromá
společností	společnost	k1gFnPc2	společnost
Sierra	Sierra	k1gFnSc1	Sierra
Nevada	Nevada	k1gFnSc1	Nevada
a	a	k8xC	a
Orbital	orbital	k1gInSc1	orbital
ATK	ATK	kA	ATK
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tři	tři	k4xCgFnPc1	tři
firmy	firma	k1gFnPc1	firma
tím	ten	k3xDgNnSc7	ten
získaly	získat	k5eAaPmAgFnP	získat
zajištění	zajištění	k1gNnSc4	zajištění
na	na	k7c4	na
dalších	další	k2eAgInPc2d1	další
šest	šest	k4xCc4	šest
letů	let	k1gInPc2	let
k	k	k7c3	k
ISS	ISS	kA	ISS
v	v	k7c6	v
časovém	časový	k2eAgInSc6d1	časový
horizontu	horizont	k1gInSc6	horizont
od	od	k7c2	od
2019	[number]	k4	2019
-	-	kIx~	-
2024	[number]	k4	2024
<g/>
.	.	kIx.	.
</s>
<s>
Konečná	konečný	k2eAgFnSc1d1	konečná
hodnota	hodnota	k1gFnSc1	hodnota
kontraktu	kontrakt	k1gInSc2	kontrakt
nicméně	nicméně	k8xC	nicméně
nebyla	být	k5eNaImAgFnS	být
zveřejněna	zveřejnit	k5eAaPmNgFnS	zveřejnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
Elon	Elon	k1gInSc1	Elon
Musk	Musk	k1gInSc4	Musk
koncipoval	koncipovat	k5eAaBmAgInS	koncipovat
projekt	projekt	k1gInSc1	projekt
Mars	Mars	k1gInSc1	Mars
Oasis	Oasis	k1gInSc4	Oasis
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
přenést	přenést	k5eAaPmF	přenést
miniaturní	miniaturní	k2eAgInSc4d1	miniaturní
experimentální	experimentální	k2eAgInSc4d1	experimentální
skleník	skleník	k1gInSc4	skleník
pro	pro	k7c4	pro
pěstování	pěstování	k1gNnSc4	pěstování
rostlin	rostlina	k1gFnPc2	rostlina
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
Marsu	Mars	k1gInSc2	Mars
<g/>
,	,	kIx,	,
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
znovu	znovu	k6eAd1	znovu
přilákat	přilákat	k5eAaPmF	přilákat
veřejný	veřejný	k2eAgInSc4d1	veřejný
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
průzkum	průzkum	k1gInSc4	průzkum
vesmíru	vesmír	k1gInSc2	vesmír
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
navýšit	navýšit	k5eAaPmF	navýšit
rozpočet	rozpočet	k1gInSc1	rozpočet
NASA	NASA	kA	NASA
<g/>
.	.	kIx.	.
</s>
<s>
Musk	Musk	k1gInSc1	Musk
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
koupit	koupit	k5eAaPmF	koupit
levné	levný	k2eAgFnPc4d1	levná
rakety	raketa	k1gFnPc4	raketa
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žádné	žádný	k3yNgFnPc4	žádný
rakety	raketa	k1gFnPc4	raketa
za	za	k7c4	za
přijatelnou	přijatelný	k2eAgFnSc4d1	přijatelná
cenu	cena	k1gFnSc4	cena
nesehnal	sehnat	k5eNaPmAgInS	sehnat
<g/>
.	.	kIx.	.
<g/>
Při	při	k7c6	při
letu	let	k1gInSc6	let
domů	dům	k1gInPc2	dům
si	se	k3xPyFc3	se
Musk	Musk	k1gInSc1	Musk
uvědomil	uvědomit	k5eAaPmAgInS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
založit	založit	k5eAaPmF	založit
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
rakety	raketa	k1gFnPc4	raketa
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
by	by	kYmCp3nP	by
nebyly	být	k5eNaImAgFnP	být
tak	tak	k6eAd1	tak
nákladné	nákladný	k2eAgFnPc1d1	nákladná
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
investorů	investor	k1gMnPc2	investor
Tesly	Tesla	k1gFnSc2	Tesla
a	a	k8xC	a
SpaceX	SpaceX	k1gMnSc2	SpaceX
<g/>
,	,	kIx,	,
Steva	Steve	k1gMnSc2	Steve
Juvetsona	Juvetson	k1gMnSc2	Juvetson
<g/>
,	,	kIx,	,
Musk	Musk	k1gInSc4	Musk
spočítal	spočítat	k5eAaPmAgMnS	spočítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
materiál	materiál	k1gInSc1	materiál
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
rakety	raketa	k1gFnSc2	raketa
tvoří	tvořit	k5eAaImIp3nP	tvořit
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jen	jen	k9	jen
3	[number]	k4	3
%	%	kIx~	%
z	z	k7c2	z
výsledné	výsledný	k2eAgFnSc2d1	výsledná
ceny	cena	k1gFnSc2	cena
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
použití	použití	k1gNnSc4	použití
vertikální	vertikální	k2eAgFnSc2d1	vertikální
integrace	integrace	k1gFnSc2	integrace
a	a	k8xC	a
produkci	produkce	k1gFnSc4	produkce
85	[number]	k4	85
%	%	kIx~	%
dílů	díl	k1gInPc2	díl
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
modulárním	modulární	k2eAgInSc7d1	modulární
přístupem	přístup	k1gInSc7	přístup
by	by	kYmCp3nS	by
SpaceX	SpaceX	k1gMnPc4	SpaceX
mohla	moct	k5eAaImAgFnS	moct
snížit	snížit	k5eAaPmF	snížit
náklady	náklad	k1gInPc4	náklad
na	na	k7c4	na
desetinu	desetina	k1gFnSc4	desetina
a	a	k8xC	a
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
70	[number]	k4	70
<g/>
%	%	kIx~	%
marže	marže	k1gFnSc2	marže
<g/>
.	.	kIx.	.
</s>
<s>
SpaceX	SpaceX	k?	SpaceX
začala	začít	k5eAaPmAgFnS	začít
s	s	k7c7	s
malou	malý	k2eAgFnSc7d1	malá
orbitální	orbitální	k2eAgFnSc7d1	orbitální
raketou	raketa	k1gFnSc7	raketa
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
rovnou	rovnou	k6eAd1	rovnou
postavila	postavit	k5eAaPmAgFnS	postavit
velkou	velká	k1gFnSc4	velká
<g/>
,	,	kIx,	,
složitější	složitý	k2eAgInPc4d2	složitější
a	a	k8xC	a
rizikovější	rizikový	k2eAgInSc4d2	rizikovější
nosič	nosič	k1gInSc4	nosič
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
selhat	selhat	k5eAaPmF	selhat
a	a	k8xC	a
přivést	přivést	k5eAaPmF	přivést
společnost	společnost	k1gFnSc4	společnost
k	k	k7c3	k
bankrotu	bankrot	k1gInSc3	bankrot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
začal	začít	k5eAaPmAgInS	začít
Musk	Musk	k1gInSc4	Musk
hledat	hledat	k5eAaImF	hledat
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
novou	nový	k2eAgFnSc4d1	nová
vesmírnou	vesmírný	k2eAgFnSc4d1	vesmírná
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
Space	Space	k1gFnSc1	Space
Exploration	Exploration	k1gInSc1	Exploration
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
SpaceX	SpaceX	k1gFnSc4	SpaceX
<g/>
.	.	kIx.	.
</s>
<s>
Musk	Musk	k1gMnSc1	Musk
požádal	požádat	k5eAaPmAgMnS	požádat
proslulého	proslulý	k2eAgMnSc4d1	proslulý
raketového	raketový	k2eAgMnSc4d1	raketový
inženýra	inženýr	k1gMnSc4	inženýr
Toma	Tom	k1gMnSc4	Tom
Muellera	Mueller	k1gMnSc4	Mueller
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
CTO	CTO	kA	CTO
pro	pro	k7c4	pro
vesmírný	vesmírný	k2eAgInSc4d1	vesmírný
pohon	pohon	k1gInSc4	pohon
<g/>
,	,	kIx,	,
a	a	k8xC	a
Mueller	Mueller	k1gMnSc1	Mueller
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
<g/>
,	,	kIx,	,
že	že	k8xS	že
půjde	jít	k5eAaImIp3nS	jít
pracovat	pracovat	k5eAaImF	pracovat
pro	pro	k7c4	pro
Muska	Musek	k1gMnSc4	Musek
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
SpaceX	SpaceX	k1gFnSc1	SpaceX
nejdříve	dříve	k6eAd3	dříve
sídlila	sídlit	k5eAaImAgFnS	sídlit
ve	v	k7c6	v
skladu	sklad	k1gInSc6	sklad
v	v	k7c6	v
El	Ela	k1gFnPc2	Ela
Segundo	Segundo	k1gNnSc4	Segundo
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
se	se	k3xPyFc4	se
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
založení	založení	k1gNnSc2	založení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
rychle	rychle	k6eAd1	rychle
rozrostla	rozrůst	k5eAaPmAgFnS	rozrůst
a	a	k8xC	a
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
2005	[number]	k4	2005
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
ze	z	k7c2	z
160	[number]	k4	160
na	na	k7c4	na
1100	[number]	k4	1100
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
3800	[number]	k4	3800
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
a	a	k8xC	a
smluvních	smluvní	k2eAgMnPc2d1	smluvní
partnerů	partner	k1gMnPc2	partner
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2013	[number]	k4	2013
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
5000	[number]	k4	5000
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dubna	duben	k1gInSc2	duben
2017	[number]	k4	2017
má	mít	k5eAaImIp3nS	mít
společnost	společnost	k1gFnSc1	společnost
téměř	téměř	k6eAd1	téměř
6000	[number]	k4	6000
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
na	na	k7c6	na
Mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
vesmírném	vesmírný	k2eAgInSc6d1	vesmírný
kongresu	kongres	k1gInSc6	kongres
Musk	Musk	k1gMnSc1	Musk
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
SpaceX	SpaceX	k1gFnSc1	SpaceX
může	moct	k5eAaImIp3nS	moct
najímat	najímat	k5eAaImF	najímat
pouze	pouze	k6eAd1	pouze
Američany	Američan	k1gMnPc4	Američan
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
pracují	pracovat	k5eAaImIp3nP	pracovat
na	na	k7c6	na
"	"	kIx"	"
<g/>
moderních	moderní	k2eAgFnPc6d1	moderní
zbraňových	zbraňový	k2eAgFnPc6d1	zbraňová
technologiích	technologie	k1gFnPc6	technologie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
měla	mít	k5eAaImAgFnS	mít
Společnost	společnost	k1gFnSc1	společnost
SpaceX	SpaceX	k1gFnSc1	SpaceX
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
seznamu	seznam	k1gInSc6	seznam
startů	start	k1gInPc2	start
více	hodně	k6eAd2	hodně
než	než	k8xS	než
40	[number]	k4	40
zakázek	zakázka	k1gFnPc2	zakázka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
představovalo	představovat	k5eAaImAgNnS	představovat
zhruba	zhruba	k6eAd1	zhruba
čtyři	čtyři	k4xCgFnPc4	čtyři
miliardy	miliarda	k4xCgFnPc4	miliarda
dolarů	dolar	k1gInPc2	dolar
ze	z	k7c2	z
smluvních	smluvní	k2eAgInPc2d1	smluvní
výnosů	výnos	k1gInPc2	výnos
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
mnohé	mnohý	k2eAgFnPc1d1	mnohá
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
smluv	smlouva	k1gFnPc2	smlouva
již	již	k6eAd1	již
byly	být	k5eAaImAgInP	být
částečně	částečně	k6eAd1	částečně
proplaceny	proplatit	k5eAaPmNgInP	proplatit
<g/>
.	.	kIx.	.
</s>
<s>
Smlouvy	smlouva	k1gFnPc1	smlouva
zahrnovaly	zahrnovat	k5eAaImAgFnP	zahrnovat
jak	jak	k6eAd1	jak
komerční	komerční	k2eAgFnPc1d1	komerční
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
vládní	vládní	k2eAgFnSc7d1	vládní
(	(	kIx(	(
<g/>
NASA	NASA	kA	NASA
<g/>
,	,	kIx,	,
armáda	armáda	k1gFnSc1	armáda
<g/>
)	)	kIx)	)
zákazníky	zákazník	k1gMnPc4	zákazník
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2013	[number]	k4	2013
měla	mít	k5eAaImAgFnS	mít
společnost	společnost	k1gFnSc1	společnost
SpaceX	SpaceX	k1gFnSc2	SpaceX
celkem	celkem	k6eAd1	celkem
50	[number]	k4	50
domluvených	domluvený	k2eAgFnPc2d1	domluvená
zakázek	zakázka	k1gFnPc2	zakázka
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
komerční	komerční	k2eAgFnSc2d1	komerční
zakázky	zakázka	k1gFnSc2	zakázka
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
začala	začít	k5eAaPmAgFnS	začít
média	médium	k1gNnPc4	médium
v	v	k7c6	v
kosmickém	kosmický	k2eAgInSc6d1	kosmický
průmyslu	průmysl	k1gInSc6	průmysl
vyslovovat	vyslovovat	k5eAaImF	vyslovovat
názory	názor	k1gInPc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
SpaceX	SpaceX	k1gFnSc1	SpaceX
snížila	snížit	k5eAaPmAgFnS	snížit
ceny	cena	k1gFnPc4	cena
pod	pod	k7c4	pod
úroveň	úroveň	k1gFnSc4	úroveň
hlavních	hlavní	k2eAgMnPc2d1	hlavní
hráčů	hráč	k1gMnPc2	hráč
na	na	k7c6	na
komerčním	komerční	k2eAgInSc6d1	komerční
trhu	trh	k1gInSc6	trh
s	s	k7c7	s
vynášením	vynášení	k1gNnSc7	vynášení
nákladu	náklad	k1gInSc2	náklad
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
-	-	kIx~	-
Ariane	Arian	k1gMnSc5	Arian
5	[number]	k4	5
a	a	k8xC	a
Proton	proton	k1gInSc1	proton
M	M	kA	M
-	-	kIx~	-
kdy	kdy	k6eAd1	kdy
SpaceX	SpaceX	k1gFnSc1	SpaceX
měla	mít	k5eAaImAgFnS	mít
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
seznamu	seznam	k1gInSc6	seznam
alespoň	alespoň	k9	alespoň
deset	deset	k4xCc4	deset
zakázek	zakázka	k1gFnPc2	zakázka
na	na	k7c4	na
geostacionární	geostacionární	k2eAgFnSc4d1	geostacionární
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Cíle	Cíla	k1gFnSc6	Cíla
===	===	k?	===
</s>
</p>
<p>
<s>
Musk	Musk	k1gMnSc1	Musk
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
cílů	cíl	k1gInPc2	cíl
je	být	k5eAaImIp3nS	být
snížit	snížit	k5eAaPmF	snížit
náklady	náklad	k1gInPc4	náklad
a	a	k8xC	a
zvýšit	zvýšit	k5eAaPmF	zvýšit
spolehlivost	spolehlivost	k1gFnSc4	spolehlivost
vynášení	vynášení	k1gNnSc2	vynášení
nákladu	náklad	k1gInSc2	náklad
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
desetkrát	desetkrát	k6eAd1	desetkrát
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
plánovala	plánovat	k5eAaImAgFnS	plánovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
začít	začít	k5eAaPmF	začít
"	"	kIx"	"
<g/>
s	s	k7c7	s
vývojem	vývoj	k1gInSc7	vývoj
těžkého	těžký	k2eAgInSc2d1	těžký
nosiče	nosič	k1gInSc2	nosič
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
supertěžkého	supertěžký	k2eAgMnSc2d1	supertěžký
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
bude	být	k5eAaImBp3nS	být
poptávka	poptávka	k1gFnSc1	poptávka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
zvýšení	zvýšení	k1gNnSc1	zvýšení
nosnosti	nosnost	k1gFnSc2	nosnost
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
nákladů	náklad	k1gInPc2	náklad
na	na	k7c4	na
vynesený	vynesený	k2eAgInSc4d1	vynesený
kilogram	kilogram	k1gInSc4	kilogram
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Elon	Elon	k1gMnSc1	Elon
Musk	Musk	k1gMnSc1	Musk
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Věřím	věřit	k5eAaImIp1nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
cena	cena	k1gFnSc1	cena
1100	[number]	k4	1100
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
kilogram	kilogram	k1gInSc4	kilogram
nebo	nebo	k8xC	nebo
méně	málo	k6eAd2	málo
je	být	k5eAaImIp3nS	být
dosažitelná	dosažitelný	k2eAgFnSc1d1	dosažitelná
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
Společnosti	společnost	k1gFnSc2	společnost
SpaceX	SpaceX	k1gFnSc2	SpaceX
bylo	být	k5eAaImAgNnS	být
vyvinout	vyvinout	k5eAaPmF	vyvinout
systém	systém	k1gInSc4	systém
rychlého	rychlý	k2eAgNnSc2d1	rychlé
opětovného	opětovný	k2eAgNnSc2d1	opětovné
použití	použití	k1gNnSc2	použití
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
jsou	být	k5eAaImIp3nP	být
veřejně	veřejně	k6eAd1	veřejně
oznámeny	oznámen	k2eAgInPc4d1	oznámen
aspekty	aspekt	k1gInPc4	aspekt
tohoto	tento	k3xDgNnSc2	tento
úsilí	úsilí	k1gNnSc2	úsilí
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zahrnovaly	zahrnovat	k5eAaImAgFnP	zahrnovat
aktivní	aktivní	k2eAgNnSc4d1	aktivní
testování	testování	k1gNnSc4	testování
demonstrační	demonstrační	k2eAgFnSc2d1	demonstrační
rakety	raketa	k1gFnSc2	raketa
pro	pro	k7c4	pro
vertikální	vertikální	k2eAgNnSc4d1	vertikální
přistávání	přistávání	k1gNnSc4	přistávání
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
výškách	výška	k1gFnPc6	výška
a	a	k8xC	a
následující	následující	k2eAgFnPc4d1	následující
zkoušky	zkouška	k1gFnPc4	zkouška
Falconu	Falcon	k1gInSc2	Falcon
9	[number]	k4	9
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
po	po	k7c6	po
splnění	splnění	k1gNnSc6	splnění
mise	mise	k1gFnSc2	mise
zkoušel	zkoušet	k5eAaImAgMnS	zkoušet
úkony	úkon	k1gInPc4	úkon
nutné	nutný	k2eAgInPc4d1	nutný
k	k	k7c3	k
přistání	přistání	k1gNnSc3	přistání
prvního	první	k4xOgInSc2	první
stupně	stupeň	k1gInSc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
účely	účel	k1gInPc4	účel
byly	být	k5eAaImAgFnP	být
rakety	raketa	k1gFnPc1	raketa
vybaveny	vybavit	k5eAaPmNgFnP	vybavit
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
testovací	testovací	k2eAgFnSc1d1	testovací
raketa	raketa	k1gFnSc1	raketa
a	a	k8xC	a
probíhaly	probíhat	k5eAaImAgFnP	probíhat
zkoušky	zkouška	k1gFnPc1	zkouška
přistávání	přistávání	k1gNnPc2	přistávání
na	na	k7c4	na
mořskou	mořský	k2eAgFnSc4d1	mořská
hladinu	hladina	k1gFnSc4	hladina
<g/>
.	.	kIx.	.
</s>
<s>
COO	COO	kA	COO
Gwynne	Gwynn	k1gInSc5	Gwynn
Shotwell	Shotwell	k1gMnSc1	Shotwell
řekla	říct	k5eAaPmAgFnS	říct
na	na	k7c6	na
Singapurském	singapurský	k2eAgNnSc6d1	singapurské
fóru	fórum	k1gNnSc6	fórum
satelitního	satelitní	k2eAgInSc2d1	satelitní
průmyslu	průmysl	k1gInSc2	průmysl
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
2013	[number]	k4	2013
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pokud	pokud	k8xS	pokud
toho	ten	k3xDgMnSc4	ten
dosáhneme	dosáhnout	k5eAaPmIp1nP	dosáhnout
(	(	kIx(	(
<g/>
opakovatelně	opakovatelně	k6eAd1	opakovatelně
použitelné	použitelný	k2eAgFnPc1d1	použitelná
technologie	technologie	k1gFnPc1	technologie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
my	my	k3xPp1nPc1	my
se	se	k3xPyFc4	se
snažíme	snažit	k5eAaImIp1nP	snažit
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
to	ten	k3xDgNnSc4	ten
udělali	udělat	k5eAaPmAgMnP	udělat
správně	správně	k6eAd1	správně
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
podíváme	podívat	k5eAaPmIp1nP	podívat
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
starty	start	k1gInPc1	start
<g />
.	.	kIx.	.
</s>
<s>
můžou	můžou	k?	můžou
být	být	k5eAaImF	být
od	od	k7c2	od
pěti	pět	k4xCc2	pět
do	do	k7c2	do
sedmi	sedm	k4xCc2	sedm
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
dramaticky	dramaticky	k6eAd1	dramaticky
změnilo	změnit	k5eAaPmAgNnS	změnit
situaci	situace	k1gFnSc4	situace
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Musk	Musk	k1gMnSc1	Musk
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
doufá	doufat	k5eAaImIp3nS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podaří	podařit	k5eAaPmIp3nS	podařit
za	za	k7c4	za
deset	deset	k4xCc4	deset
až	až	k9	až
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
dopravit	dopravit	k5eAaPmF	dopravit
lidi	člověk	k1gMnPc4	člověk
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
ho	on	k3xPp3gMnSc4	on
jeho	jeho	k3xOp3gNnSc4	jeho
výpočty	výpočet	k1gInPc1	výpočet
přesvědčily	přesvědčit	k5eAaPmAgInP	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kolonizace	kolonizace	k1gFnSc1	kolonizace
Marsu	Mars	k1gInSc2	Mars
je	být	k5eAaImIp3nS	být
možná	možný	k2eAgFnSc1d1	možná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2013	[number]	k4	2013
Musk	Musk	k1gMnSc1	Musk
použil	použít	k5eAaPmAgMnS	použít
název	název	k1gInSc4	název
"	"	kIx"	"
<g/>
Mars	Mars	k1gMnSc1	Mars
Colonial	Colonial	k1gMnSc1	Colonial
Transporter	Transporter	k1gMnSc1	Transporter
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
také	také	k9	také
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
Interplanetary	Interplanetara	k1gFnPc1	Interplanetara
Transport	transport	k1gInSc1	transport
System	Syst	k1gMnSc7	Syst
-	-	kIx~	-
Meziplanetární	meziplanetární	k2eAgInSc1d1	meziplanetární
dopravní	dopravní	k2eAgInSc1d1	dopravní
systém	systém	k1gInSc1	systém
<g/>
)	)	kIx)	)
a	a	k8xC	a
odkazoval	odkazovat	k5eAaImAgMnS	odkazovat
na	na	k7c4	na
soukromé	soukromý	k2eAgNnSc4d1	soukromé
financování	financování	k1gNnSc4	financování
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
by	by	kYmCp3nS	by
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
vývoj	vývoj	k1gInSc4	vývoj
raketových	raketový	k2eAgMnPc2d1	raketový
motorů	motor	k1gInPc2	motor
<g/>
,	,	kIx,	,
a	a	k8xC	a
vesmírné	vesmírný	k2eAgFnPc1d1	vesmírná
lodě	loď	k1gFnPc1	loď
pro	pro	k7c4	pro
přepravu	přeprava	k1gFnSc4	přeprava
lidí	člověk	k1gMnPc2	člověk
na	na	k7c4	na
Mars	Mars	k1gInSc4	Mars
a	a	k8xC	a
návrat	návrat	k1gInSc4	návrat
na	na	k7c4	na
Zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2014	[number]	k4	2014
Gwynne	Gwynn	k1gInSc5	Gwynn
Shotwell	Shotwell	k1gInSc1	Shotwell
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
uskuteční	uskutečnit	k5eAaPmIp3nS	uskutečnit
let	let	k1gInSc1	let
s	s	k7c7	s
posádku	posádka	k1gFnSc4	posádka
v	v	k7c6	v
lodi	loď	k1gFnSc6	loď
Dragon	Dragon	k1gMnSc1	Dragon
2	[number]	k4	2
a	a	k8xC	a
let	let	k1gInSc4	let
Falconu	Falcon	k1gInSc2	Falcon
Heavy	Heava	k1gFnSc2	Heava
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
inženýrský	inženýrský	k2eAgInSc1d1	inženýrský
tým	tým	k1gInSc1	tým
SpaceX	SpaceX	k1gFnSc2	SpaceX
zaměří	zaměřit	k5eAaPmIp3nS	zaměřit
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
technologií	technologie	k1gFnPc2	technologie
nutných	nutný	k2eAgFnPc2d1	nutná
pro	pro	k7c4	pro
let	let	k1gInSc4	let
k	k	k7c3	k
Marsu	Mars	k1gInSc3	Mars
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Úspěchy	úspěch	k1gInPc1	úspěch
===	===	k?	===
</s>
</p>
<p>
<s>
Nejdůležitější	důležitý	k2eAgInPc1d3	nejdůležitější
úspěchy	úspěch	k1gInPc1	úspěch
SpaceX	SpaceX	k1gFnSc2	SpaceX
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
soukromě	soukromě	k6eAd1	soukromě
financovaná	financovaný	k2eAgFnSc1d1	financovaná
raketa	raketa	k1gFnSc1	raketa
na	na	k7c4	na
kapalné	kapalný	k2eAgNnSc4d1	kapalné
palivo	palivo	k1gNnSc4	palivo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
oběžné	oběžný	k2eAgFnPc4d1	oběžná
dráhy	dráha	k1gFnPc4	dráha
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
Falconu	Falcon	k1gInSc2	Falcon
1	[number]	k4	1
-	-	kIx~	-
Ratsat	Ratsat	k1gFnSc2	Ratsat
-	-	kIx~	-
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
soukromě	soukromě	k6eAd1	soukromě
financovaná	financovaný	k2eAgFnSc1d1	financovaná
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
úspěšně	úspěšně	k6eAd1	úspěšně
vynést	vynést	k5eAaPmF	vynést
kosmickou	kosmický	k2eAgFnSc4d1	kosmická
loď	loď	k1gFnSc4	loď
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
a	a	k8xC	a
zase	zase	k9	zase
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
přistát	přistát	k5eAaPmF	přistát
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
Falconu	Falcon	k1gInSc2	Falcon
9	[number]	k4	9
-	-	kIx~	-
NASA	NASA	kA	NASA
COTS	COTS	kA	COTS
Demo	demo	k2eAgInSc4d1	demo
1	[number]	k4	1
-	-	kIx~	-
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
soukromá	soukromý	k2eAgFnSc1d1	soukromá
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
poslala	poslat	k5eAaPmAgFnS	poslat
svojí	svojit	k5eAaImIp3nP	svojit
loď	loď	k1gFnSc4	loď
k	k	k7c3	k
Mezinárodní	mezinárodní	k2eAgFnSc3d1	mezinárodní
vesmírné	vesmírný	k2eAgFnSc3d1	vesmírná
stanici	stanice	k1gFnSc3	stanice
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
Falconu	Falcon	k1gInSc2	Falcon
9	[number]	k4	9
-	-	kIx~	-
NASA	NASA	kA	NASA
COTS	COTS	kA	COTS
Demo	demo	k2eAgInSc4d1	demo
2	[number]	k4	2
<g/>
+	+	kIx~	+
-	-	kIx~	-
22	[number]	k4	22
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
soukromá	soukromý	k2eAgFnSc1d1	soukromá
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vynesla	vynést	k5eAaPmAgFnS	vynést
satelit	satelit	k1gInSc4	satelit
na	na	k7c4	na
geostacionární	geostacionární	k2eAgFnSc4d1	geostacionární
dráhu	dráha	k1gFnSc4	dráha
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
Falconu	Falcon	k1gInSc2	Falcon
9	[number]	k4	9
-	-	kIx~	-
SES-8	SES-8	k1gFnSc2	SES-8
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
přistání	přistání	k1gNnSc3	přistání
prvního	první	k4xOgInSc2	první
stupně	stupeň	k1gInSc2	stupeň
orbitální	orbitální	k2eAgFnSc2d1	orbitální
rakety	raketa	k1gFnSc2	raketa
na	na	k7c6	na
pevnině	pevnina	k1gFnSc6	pevnina
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
Falconu	Falcon	k1gInSc2	Falcon
9	[number]	k4	9
-	-	kIx~	-
Orbcomm	Orbcomm	k1gInSc1	Orbcomm
OG2	OG2	k1gFnPc2	OG2
let	léto	k1gNnPc2	léto
2	[number]	k4	2
-	-	kIx~	-
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
přistání	přistání	k1gNnSc3	přistání
prvního	první	k4xOgInSc2	první
stupně	stupeň	k1gInSc2	stupeň
na	na	k7c6	na
mořské	mořský	k2eAgFnSc6d1	mořská
plošině	plošina	k1gFnSc6	plošina
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
Falconu	Falcon	k1gInSc2	Falcon
9	[number]	k4	9
-	-	kIx~	-
SpaceX	SpaceX	k1gMnSc1	SpaceX
CRS-8	CRS-8	k1gMnSc1	CRS-8
-	-	kIx~	-
8	[number]	k4	8
<g/>
.	.	kIx.	.
duben	duben	k1gInSc1	duben
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
start	start	k1gInSc1	start
a	a	k8xC	a
přistání	přistání	k1gNnSc1	přistání
již	již	k6eAd1	již
použitého	použitý	k2eAgInSc2d1	použitý
prvního	první	k4xOgInSc2	první
stupně	stupeň	k1gInSc2	stupeň
orbitální	orbitální	k2eAgFnSc2d1	orbitální
rakety	raketa	k1gFnSc2	raketa
(	(	kIx(	(
<g/>
32	[number]	k4	32
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
Falconu	Falcon	k1gInSc2	Falcon
9	[number]	k4	9
-	-	kIx~	-
SES-10	SES-10	k1gFnSc2	SES-10
-	-	kIx~	-
30	[number]	k4	30
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
řízený	řízený	k2eAgInSc1d1	řízený
sestup	sestup	k1gInSc1	sestup
jedné	jeden	k4xCgFnSc2	jeden
poloviny	polovina	k1gFnSc2	polovina
aerodynamického	aerodynamický	k2eAgInSc2d1	aerodynamický
krytu	kryt	k1gInSc2	kryt
(	(	kIx(	(
<g/>
32	[number]	k4	32
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
Falconu	Falcon	k1gInSc2	Falcon
9	[number]	k4	9
-	-	kIx~	-
SES-10	SES-10	k1gFnSc2	SES-10
-	-	kIx~	-
30	[number]	k4	30
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Znovupoužití	znovupoužití	k1gNnSc1	znovupoužití
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodě	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
Dragonu	Dragon	k1gMnSc3	Dragon
C	C	kA	C
<g/>
106	[number]	k4	106
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
byla	být	k5eAaImAgFnS	být
při	při	k7c6	při
druhém	druhý	k4xOgInSc6	druhý
letu	let	k1gInSc6	let
většina	většina	k1gFnSc1	většina
systémů	systém	k1gInPc2	systém
původních	původní	k2eAgInPc2d1	původní
(	(	kIx(	(
<g/>
35	[number]	k4	35
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
Falconu	Falcon	k1gInSc2	Falcon
9	[number]	k4	9
-	-	kIx~	-
SpaceX	SpaceX	k1gMnSc1	SpaceX
CRS-11	CRS-11	k1gMnSc1	CRS-11
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
červen	červen	k1gInSc1	červen
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2015	[number]	k4	2015
vypustila	vypustit	k5eAaPmAgFnS	vypustit
SpaceX	SpaceX	k1gFnSc1	SpaceX
modernizovanou	modernizovaný	k2eAgFnSc4d1	modernizovaná
verzi	verze	k1gFnSc4	verze
Falconu	Falcon	k1gInSc2	Falcon
9	[number]	k4	9
ze	z	k7c2	z
základny	základna	k1gFnSc2	základna
na	na	k7c6	na
Mysu	mys	k1gInSc6	mys
Canaveral	Canaveral	k1gFnSc2	Canaveral
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
celkově	celkově	k6eAd1	celkově
dvacátém	dvacátý	k4xOgInSc6	dvacátý
letu	let	k1gInSc6	let
se	se	k3xPyFc4	se
jako	jako	k9	jako
obvykle	obvykle	k6eAd1	obvykle
oddělil	oddělit	k5eAaPmAgInS	oddělit
druhý	druhý	k4xOgInSc1	druhý
stupeň	stupeň	k1gInSc1	stupeň
od	od	k7c2	od
prvního	první	k4xOgInSc2	první
a	a	k8xC	a
první	první	k4xOgInSc1	první
stupeň	stupeň	k1gInSc1	stupeň
provedl	provést	k5eAaPmAgInS	provést
třemi	tři	k4xCgInPc7	tři
motory	motor	k1gInPc7	motor
zážeh	zážeh	k1gInSc1	zážeh
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ho	on	k3xPp3gMnSc4	on
poslal	poslat	k5eAaPmAgInS	poslat
zpět	zpět	k6eAd1	zpět
nad	nad	k7c4	nad
pevninu	pevnina	k1gFnSc4	pevnina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
první	první	k4xOgInSc4	první
stupeň	stupeň	k1gInSc4	stupeň
úspěšně	úspěšně	k6eAd1	úspěšně
vertikálně	vertikálně	k6eAd1	vertikálně
přistál	přistát	k5eAaPmAgInS	přistát
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
poprvé	poprvé	k6eAd1	poprvé
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
přistál	přistát	k5eAaImAgInS	přistát
první	první	k4xOgInSc1	první
stupeň	stupeň	k1gInSc1	stupeň
rakety	raketa	k1gFnSc2	raketa
mířící	mířící	k2eAgFnSc2d1	mířící
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
<g/>
SpaceX	SpaceX	k1gFnSc1	SpaceX
také	také	k9	také
začala	začít	k5eAaPmAgFnS	začít
používat	používat	k5eAaImF	používat
od	od	k7c2	od
verze	verze	k1gFnSc2	verze
1.1	[number]	k4	1.1
FT	FT	kA	FT
velmi	velmi	k6eAd1	velmi
podchlazené	podchlazený	k2eAgNnSc4d1	podchlazené
palivo	palivo	k1gNnSc4	palivo
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
narostla	narůst	k5eAaPmAgFnS	narůst
nosnost	nosnost	k1gFnSc1	nosnost
rakety	raketa	k1gFnSc2	raketa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nehodě	nehoda	k1gFnSc6	nehoda
s	s	k7c7	s
družicí	družice	k1gFnSc7	družice
Amos-	Amos-	k1gFnSc2	Amos-
<g/>
6	[number]	k4	6
není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
podchlazené	podchlazený	k2eAgNnSc1d1	podchlazené
palivo	palivo	k1gNnSc1	palivo
u	u	k7c2	u
verze	verze	k1gFnSc2	verze
1.1	[number]	k4	1.1
FT	FT	kA	FT
používáno	používán	k2eAgNnSc1d1	používáno
i	i	k8xC	i
nadále	nadále	k6eAd1	nadále
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
technika	technika	k1gFnSc1	technika
použila	použít	k5eAaPmAgFnS	použít
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
motorů	motor	k1gInPc2	motor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
nedostala	dostat	k5eNaPmAgFnS	dostat
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Neúspěšně	úspěšně	k6eNd1	úspěšně
se	se	k3xPyFc4	se
také	také	k9	také
používala	používat	k5eAaImAgFnS	používat
na	na	k7c6	na
sovětské	sovětský	k2eAgFnSc6d1	sovětská
lunární	lunární	k2eAgFnSc6d1	lunární
raketě	raketa	k1gFnSc6	raketa
N	N	kA	N
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nezdary	nezdar	k1gInPc4	nezdar
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2013	[number]	k4	2013
postihl	postihnout	k5eAaPmAgMnS	postihnout
loď	loď	k1gFnSc4	loď
Dragon	Dragon	k1gMnSc1	Dragon
problém	problém	k1gInSc4	problém
s	s	k7c7	s
manévrovacími	manévrovací	k2eAgFnPc7d1	manévrovací
tryskami	tryska	k1gFnPc7	tryska
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
zablokovaných	zablokovaný	k2eAgInPc2d1	zablokovaný
palivových	palivový	k2eAgInPc2d1	palivový
ventilů	ventil	k1gInPc2	ventil
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
loď	loď	k1gFnSc4	loď
správně	správně	k6eAd1	správně
ovládat	ovládat	k5eAaImF	ovládat
<g/>
,	,	kIx,	,
technici	technik	k1gMnPc1	technik
ze	z	k7c2	z
SpaceX	SpaceX	k1gFnSc2	SpaceX
ale	ale	k8xC	ale
dokázali	dokázat	k5eAaPmAgMnP	dokázat
dálkově	dálkově	k6eAd1	dálkově
problém	problém	k1gInSc4	problém
vyřešit	vyřešit	k5eAaPmF	vyřešit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Dragon	Dragon	k1gMnSc1	Dragon
dorazil	dorazit	k5eAaPmAgMnS	dorazit
k	k	k7c3	k
Mezinárodní	mezinárodní	k2eAgFnSc3d1	mezinárodní
vesmírné	vesmírný	k2eAgFnSc3d1	vesmírná
stanici	stanice	k1gFnSc3	stanice
s	s	k7c7	s
jednodenním	jednodenní	k2eAgNnSc7d1	jednodenní
zpožděním	zpoždění	k1gNnSc7	zpoždění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2015	[number]	k4	2015
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
raketa	raketa	k1gFnSc1	raketa
Falcon	Falcon	k1gInSc1	Falcon
9	[number]	k4	9
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
vrcholu	vrchol	k1gInSc6	vrchol
byla	být	k5eAaImAgFnS	být
usazená	usazený	k2eAgFnSc1d1	usazená
zásobovací	zásobovací	k2eAgFnSc1d1	zásobovací
loď	loď	k1gFnSc1	loď
Dragon	Dragon	k1gMnSc1	Dragon
CRS-	CRS-	k1gMnSc1	CRS-
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mířila	mířit	k5eAaImAgFnS	mířit
k	k	k7c3	k
ISS	ISS	kA	ISS
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
měření	měření	k1gNnPc1	měření
telemetrie	telemetrie	k1gFnSc2	telemetrie
byla	být	k5eAaImAgNnP	být
nominální	nominální	k2eAgFnSc1d1	nominální
až	až	k8xS	až
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
minuty	minuta	k1gFnSc2	minuta
a	a	k8xC	a
devatenácté	devatenáctý	k4xOgFnPc4	devatenáctý
sekundy	sekunda	k1gFnPc4	sekunda
letu	let	k1gInSc2	let
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
ztráta	ztráta	k1gFnSc1	ztráta
tlaku	tlak	k1gInSc2	tlak
helia	helium	k1gNnSc2	helium
a	a	k8xC	a
u	u	k7c2	u
druhého	druhý	k4xOgInSc2	druhý
stupně	stupeň	k1gInSc2	stupeň
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
oblak	oblak	k1gInSc1	oblak
páry	pára	k1gFnSc2	pára
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
sekundách	sekunda	k1gFnPc6	sekunda
druhý	druhý	k4xOgInSc1	druhý
stupeň	stupeň	k1gInSc1	stupeň
explodoval	explodovat	k5eAaBmAgInS	explodovat
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
stupeň	stupeň	k1gInSc1	stupeň
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
pár	pár	k4xCyI	pár
sekund	sekunda	k1gFnPc2	sekunda
v	v	k7c6	v
letu	let	k1gInSc6	let
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
se	se	k3xPyFc4	se
působením	působení	k1gNnSc7	působení
aerodynamických	aerodynamický	k2eAgFnPc2d1	aerodynamická
sil	síla	k1gFnPc2	síla
rozpadl	rozpadnout	k5eAaPmAgInS	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
Dragon	Dragon	k1gMnSc1	Dragon
rozpad	rozpad	k1gInSc4	rozpad
rakety	raketa	k1gFnSc2	raketa
přežila	přežít	k5eAaPmAgFnS	přežít
a	a	k8xC	a
vysílala	vysílat	k5eAaImAgFnS	vysílat
telemetrii	telemetrie	k1gFnSc4	telemetrie
až	až	k9	až
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
nárazu	náraz	k1gInSc2	náraz
o	o	k7c4	o
hladinu	hladina	k1gFnSc4	hladina
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
kapsle	kapsle	k1gFnSc1	kapsle
mohla	moct	k5eAaImAgFnS	moct
přistát	přistát	k5eAaPmF	přistát
neporušená	porušený	k2eNgFnSc1d1	neporušená
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
byl	být	k5eAaImAgInS	být
řídící	řídící	k2eAgInSc1d1	řídící
systém	systém	k1gInSc1	systém
upraven	upravit	k5eAaPmNgInS	upravit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nehody	nehoda	k1gFnSc2	nehoda
otevřel	otevřít	k5eAaPmAgMnS	otevřít
padáky	padák	k1gInPc4	padák
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
viníka	viník	k1gMnSc2	viník
nehody	nehoda	k1gFnSc2	nehoda
byla	být	k5eAaImAgFnS	být
označena	označen	k2eAgFnSc1d1	označena
ocelová	ocelový	k2eAgFnSc1d1	ocelová
vzpěra	vzpěra	k1gFnSc1	vzpěra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
držela	držet	k5eAaImAgFnS	držet
tlakovou	tlakový	k2eAgFnSc4d1	tlaková
nádobu	nádoba	k1gFnSc4	nádoba
s	s	k7c7	s
héliem	hélium	k1gNnSc7	hélium
<g/>
,	,	kIx,	,
ta	ten	k3xDgNnPc1	ten
se	se	k3xPyFc4	se
ale	ale	k9	ale
působením	působení	k1gNnSc7	působení
přetížení	přetížení	k1gNnSc2	přetížení
uvolnila	uvolnit	k5eAaPmAgFnS	uvolnit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
zvýšení	zvýšení	k1gNnSc1	zvýšení
tlaku	tlak	k1gInSc2	tlak
v	v	k7c6	v
nádrži	nádrž	k1gFnSc6	nádrž
paliva	palivo	k1gNnSc2	palivo
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
rozpad	rozpad	k1gInSc1	rozpad
jelikož	jelikož	k8xS	jelikož
ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
stavěná	stavěný	k2eAgFnSc1d1	stavěná
na	na	k7c4	na
nižší	nízký	k2eAgInSc4d2	nižší
tlak	tlak	k1gInSc4	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
prokázalo	prokázat	k5eAaPmAgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dodavatel	dodavatel	k1gMnSc1	dodavatel
vzpěry	vzpěra	k1gFnSc2	vzpěra
nedodržel	dodržet	k5eNaPmAgMnS	dodržet
předepsanou	předepsaný	k2eAgFnSc4d1	předepsaná
kvalitu	kvalita	k1gFnSc4	kvalita
výroby	výroba	k1gFnSc2	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
se	se	k3xPyFc4	se
řídícím	řídící	k2eAgInSc7d1	řídící
systémem	systém	k1gInSc7	systém
Dragonu	Dragon	k1gMnSc3	Dragon
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
celé	celý	k2eAgFnSc3d1	celá
analýze	analýza	k1gFnSc3	analýza
a	a	k8xC	a
jeho	on	k3xPp3gNnSc2	on
přenastavení	přenastavení	k1gNnSc2	přenastavení
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
loď	loď	k1gFnSc1	loď
přečkala	přečkat	k5eAaPmAgFnS	přečkat
podobné	podobný	k2eAgFnPc4d1	podobná
havárie	havárie	k1gFnPc4	havárie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2016	[number]	k4	2016
explodoval	explodovat	k5eAaBmAgInS	explodovat
Falcon	Falcon	k1gInSc4	Falcon
9	[number]	k4	9
během	běh	k1gInSc7	běh
plnění	plnění	k1gNnSc2	plnění
paliva	palivo	k1gNnSc2	palivo
před	před	k7c7	před
obvyklým	obvyklý	k2eAgInSc7d1	obvyklý
statickým	statický	k2eAgInSc7d1	statický
zážehem	zážeh	k1gInSc7	zážeh
před	před	k7c7	před
startem	start	k1gInSc7	start
<g/>
.	.	kIx.	.
</s>
<s>
Statický	statický	k2eAgInSc1d1	statický
zážeh	zážeh	k1gInSc1	zážeh
měl	mít	k5eAaImAgInS	mít
proběhnout	proběhnout	k5eAaPmF	proběhnout
s	s	k7c7	s
připojeným	připojený	k2eAgInSc7d1	připojený
nákladem	náklad	k1gInSc7	náklad
<g/>
,	,	kIx,	,
telekomunikační	telekomunikační	k2eAgFnSc7d1	telekomunikační
družicí	družice	k1gFnSc7	družice
Amos-	Amos-	k1gFnSc2	Amos-
<g/>
6	[number]	k4	6
s	s	k7c7	s
hodnotou	hodnota	k1gFnSc7	hodnota
200	[number]	k4	200
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
při	při	k7c6	při
výbuchu	výbuch	k1gInSc6	výbuch
zničena	zničit	k5eAaPmNgFnS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Elon	Elon	k1gMnSc1	Elon
Musk	Musk	k1gMnSc1	Musk
popsal	popsat	k5eAaPmAgMnS	popsat
tuto	tento	k3xDgFnSc4	tento
událost	událost	k1gFnSc4	událost
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
nejtěžší	těžký	k2eAgNnSc1d3	nejtěžší
a	a	k8xC	a
složité	složitý	k2eAgNnSc1d1	složité
selhání	selhání	k1gNnSc1	selhání
<g/>
"	"	kIx"	"
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
SpaceX	SpaceX	k1gFnSc2	SpaceX
<g/>
.	.	kIx.	.
</s>
<s>
Technici	technik	k1gMnPc1	technik
společnosti	společnost	k1gFnSc2	společnost
přezkoumávali	přezkoumávat	k5eAaImAgMnP	přezkoumávat
téměř	téměř	k6eAd1	téměř
tři	tři	k4xCgInPc1	tři
tisíce	tisíc	k4xCgInPc1	tisíc
kanálů	kanál	k1gInPc2	kanál
telemetrie	telemetrie	k1gFnSc2	telemetrie
a	a	k8xC	a
videozáznamů	videozáznam	k1gInPc2	videozáznam
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
pokrývaly	pokrývat	k5eAaImAgFnP	pokrývat
dobu	doba	k1gFnSc4	doba
35	[number]	k4	35
až	až	k9	až
55	[number]	k4	55
po	po	k7c6	po
objevení	objevení	k1gNnSc6	objevení
anomálie	anomálie	k1gFnSc2	anomálie
<g/>
.	.	kIx.	.
</s>
<s>
Musk	Musk	k1gMnSc1	Musk
zveřejnil	zveřejnit	k5eAaPmAgMnS	zveřejnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
výbuch	výbuch	k1gInSc1	výbuch
byl	být	k5eAaImAgInS	být
způsoben	způsobit	k5eAaPmNgInS	způsobit
námrazou	námraza	k1gFnSc7	námraza
kapalného	kapalný	k2eAgInSc2d1	kapalný
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
okysličovadlo	okysličovadlo	k1gNnSc1	okysličovadlo
<g/>
.	.	kIx.	.
</s>
<s>
Kyslík	kyslík	k1gInSc1	kyslík
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
struktury	struktura	k1gFnSc2	struktura
uhlíkových	uhlíkový	k2eAgFnPc2d1	uhlíková
kompozitních	kompozitní	k2eAgFnPc2d1	kompozitní
nádrží	nádrž	k1gFnPc2	nádrž
a	a	k8xC	a
způsobil	způsobit	k5eAaPmAgMnS	způsobit
jejich	jejich	k3xOp3gNnPc4	jejich
vznícení	vznícení	k1gNnPc4	vznícení
<g/>
.	.	kIx.	.
</s>
<s>
Nehoda	nehoda	k1gFnSc1	nehoda
způsobila	způsobit	k5eAaPmAgFnS	způsobit
odstávku	odstávka	k1gFnSc4	odstávka
trvající	trvající	k2eAgInSc4d1	trvající
čtyři	čtyři	k4xCgInPc4	čtyři
měsíce	měsíc	k1gInPc4	měsíc
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2017	[number]	k4	2017
začaly	začít	k5eAaPmAgFnP	začít
Falcony	Falcona	k1gFnPc1	Falcona
9	[number]	k4	9
znovu	znovu	k6eAd1	znovu
létat	létat	k5eAaImF	létat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
a	a	k8xC	a
financování	financování	k1gNnSc1	financování
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2008	[number]	k4	2008
společnost	společnost	k1gFnSc1	společnost
SpaceX	SpaceX	k1gFnSc1	SpaceX
přijala	přijmout	k5eAaPmAgFnS	přijmout
investici	investice	k1gFnSc4	investice
od	od	k7c2	od
Founders	Foundersa	k1gFnPc2	Foundersa
Fund	fund	k1gInSc4	fund
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
20	[number]	k4	20
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
byly	být	k5eAaImAgFnP	být
přibližně	přibližně	k6eAd1	přibližně
dvě	dva	k4xCgFnPc4	dva
třetiny	třetina	k1gFnPc4	třetina
společnosti	společnost	k1gFnSc2	společnost
vlastněny	vlastněn	k2eAgFnPc4d1	vlastněna
jeho	jeho	k3xOp3gMnPc7	jeho
zakladatelem	zakladatel	k1gMnSc7	zakladatel
a	a	k8xC	a
cena	cena	k1gFnSc1	cena
jeho	jeho	k3xOp3gInPc2	jeho
70	[number]	k4	70
milionů	milion	k4xCgInPc2	milion
akcií	akcie	k1gFnPc2	akcie
byla	být	k5eAaImAgFnS	být
odhadnuta	odhadnout	k5eAaPmNgFnS	odhadnout
na	na	k7c4	na
875	[number]	k4	875
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
hodnota	hodnota	k1gFnSc1	hodnota
SpaceX	SpaceX	k1gFnSc1	SpaceX
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
1,3	[number]	k4	1,3
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
letu	let	k1gInSc6	let
COTS	COTS	kA	COTS
2	[number]	k4	2
<g/>
+	+	kIx~	+
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2012	[number]	k4	2012
se	se	k3xPyFc4	se
cena	cena	k1gFnSc1	cena
společnosti	společnost	k1gFnSc2	společnost
téměř	téměř	k6eAd1	téměř
zdvojnásobila	zdvojnásobit	k5eAaPmAgFnS	zdvojnásobit
na	na	k7c4	na
2,4	[number]	k4	2,4
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2015	[number]	k4	2015
společnost	společnost	k1gFnSc1	společnost
SpaceX	SpaceX	k1gFnSc1	SpaceX
získala	získat	k5eAaPmAgFnS	získat
finanční	finanční	k2eAgFnSc4d1	finanční
injekci	injekce	k1gFnSc4	injekce
od	od	k7c2	od
společností	společnost	k1gFnPc2	společnost
Google	Google	k1gFnSc2	Google
a	a	k8xC	a
Fidelity	Fidelita	k1gFnSc2	Fidelita
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
jedné	jeden	k4xCgFnSc2	jeden
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
8,333	[number]	k4	8,333
<g/>
%	%	kIx~	%
podíl	podíl	k1gInSc1	podíl
a	a	k8xC	a
hodnota	hodnota	k1gFnSc1	hodnota
společnosti	společnost	k1gFnSc2	společnost
byla	být	k5eAaImAgFnS	být
stanovena	stanovit	k5eAaPmNgFnS	stanovit
přibližně	přibližně	k6eAd1	přibližně
na	na	k7c4	na
12	[number]	k4	12
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Společnosti	společnost	k1gFnPc1	společnost
Google	Google	k1gFnSc1	Google
a	a	k8xC	a
Fidelity	Fidelit	k1gInPc1	Fidelit
se	se	k3xPyFc4	se
připojily	připojit	k5eAaPmAgInP	připojit
k	k	k7c3	k
tehdejší	tehdejší	k2eAgFnSc3d1	tehdejší
investiční	investiční	k2eAgFnSc3d1	investiční
skupině	skupina	k1gFnSc3	skupina
Draper	Drapra	k1gFnPc2	Drapra
Fisher	Fishra	k1gFnPc2	Fishra
Jurvetson	Jurvetson	k1gInSc1	Jurvetson
<g/>
,	,	kIx,	,
Founders	Founders	k1gInSc1	Founders
Fund	fund	k1gInSc1	fund
<g/>
,	,	kIx,	,
Valor	valor	k1gInSc1	valor
Equity	Equita	k1gFnSc2	Equita
Partners	Partnersa	k1gFnPc2	Partnersa
a	a	k8xC	a
Capricorn	Capricorna	k1gFnPc2	Capricorna
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
prvních	první	k4xOgInPc6	první
deseti	deset	k4xCc6	deset
letech	let	k1gInPc6	let
provozu	provoz	k1gInSc2	provoz
společnost	společnost	k1gFnSc1	společnost
hospodařila	hospodařit	k5eAaImAgFnS	hospodařit
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
miliardou	miliarda	k4xCgFnSc7	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
soukromý	soukromý	k2eAgInSc1d1	soukromý
kapitál	kapitál	k1gInSc1	kapitál
tvořil	tvořit	k5eAaImAgInS	tvořit
asi	asi	k9	asi
200	[number]	k4	200
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Musk	Musk	k1gMnSc1	Musk
vložil	vložit	k5eAaPmAgMnS	vložit
zhruba	zhruba	k6eAd1	zhruba
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
investoři	investor	k1gMnPc1	investor
také	také	k9	také
zhruba	zhruba	k6eAd1	zhruba
stejnou	stejný	k2eAgFnSc4d1	stejná
částku	částka	k1gFnSc4	částka
(	(	kIx(	(
<g/>
fond	fond	k1gInSc1	fond
zakladatelů	zakladatel	k1gMnPc2	zakladatel
-	-	kIx~	-
Draper	Draper	k1gMnSc1	Draper
Fisher	Fishra	k1gFnPc2	Fishra
Jurvetson	Jurvetson	k1gMnSc1	Jurvetson
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
průběžných	průběžný	k2eAgFnPc2d1	průběžná
plateb	platba	k1gFnPc2	platba
za	za	k7c4	za
dlouhodobé	dlouhodobý	k2eAgFnPc4d1	dlouhodobá
smlouvy	smlouva	k1gFnPc4	smlouva
o	o	k7c6	o
startech	start	k1gInPc6	start
a	a	k8xC	a
vývoji	vývoj	k1gInSc6	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dubna	duben	k1gInSc2	duben
2012	[number]	k4	2012
NASA	NASA	kA	NASA
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
částky	částka	k1gFnSc2	částka
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
přibližně	přibližně	k6eAd1	přibližně
400	[number]	k4	400
až	až	k9	až
500	[number]	k4	500
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
byla	být	k5eAaImAgFnS	být
poskytnuta	poskytnut	k2eAgFnSc1d1	poskytnuta
za	za	k7c4	za
smlouvy	smlouva	k1gFnPc4	smlouva
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
vynášení	vynášení	k1gNnSc4	vynášení
nákladu	náklad	k1gInSc2	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
SpaceX	SpaceX	k1gFnSc1	SpaceX
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
smlouvy	smlouva	k1gFnPc4	smlouva
na	na	k7c4	na
40	[number]	k4	40
startů	start	k1gInPc2	start
a	a	k8xC	a
z	z	k7c2	z
každé	každý	k3xTgFnSc2	každý
této	tento	k3xDgFnSc2	tento
smlouvy	smlouva	k1gFnSc2	smlouva
získala	získat	k5eAaPmAgFnS	získat
zálohu	záloha	k1gFnSc4	záloha
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
potřebný	potřebný	k2eAgInSc1d1	potřebný
nosič	nosič	k1gInSc1	nosič
je	být	k5eAaImIp3nS	být
postaven	postavit	k5eAaPmNgInS	postavit
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
startem	start	k1gInSc7	start
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zařízení	zařízení	k1gNnSc1	zařízení
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Ústředí	ústředí	k1gNnPc4	ústředí
a	a	k8xC	a
výrobní	výrobní	k2eAgInSc4d1	výrobní
závod	závod	k1gInSc4	závod
===	===	k?	===
</s>
</p>
<p>
<s>
Ústředí	ústředí	k1gNnSc1	ústředí
SpaceX	SpaceX	k1gFnSc2	SpaceX
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
u	u	k7c2	u
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Hawthorne	Hawthorn	k1gInSc5	Hawthorn
<g/>
,	,	kIx,	,
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Rocket	Rocket	k1gMnSc1	Rocket
Road	Road	k1gMnSc1	Road
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
třípodlažní	třípodlažní	k2eAgInSc1d1	třípodlažní
objekt	objekt	k1gInSc1	objekt
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
postavený	postavený	k2eAgInSc1d1	postavený
Northrop	Northrop	k1gInSc1	Northrop
Corporation	Corporation	k1gInSc4	Corporation
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
letadel	letadlo	k1gNnPc2	letadlo
Boeing	boeing	k1gInSc4	boeing
747	[number]	k4	747
<g/>
,	,	kIx,	,
SpaceX	SpaceX	k1gFnSc1	SpaceX
zde	zde	k6eAd1	zde
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgFnSc2	svůj
kanceláře	kancelář	k1gFnSc2	kancelář
<g/>
,	,	kIx,	,
řídící	řídící	k2eAgNnSc1d1	řídící
středisko	středisko	k1gNnSc1	středisko
a	a	k8xC	a
výrobní	výrobní	k2eAgInSc1d1	výrobní
závod	závod	k1gInSc1	závod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
koncentrací	koncentrace	k1gFnPc2	koncentrace
sídel	sídlo	k1gNnPc2	sídlo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
poboček	pobočka	k1gFnPc2	pobočka
firem	firma	k1gFnPc2	firma
z	z	k7c2	z
oboru	obor	k1gInSc2	obor
leteckého	letecký	k2eAgInSc2d1	letecký
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Boeingu	boeing	k1gInSc2	boeing
<g/>
,	,	kIx,	,
Raytheonu	Raytheon	k1gInSc2	Raytheon
<g/>
,	,	kIx,	,
Jet	jet	k2eAgInSc1d1	jet
Propulsion	Propulsion	k1gInSc1	Propulsion
Laboratory	Laborator	k1gInPc1	Laborator
(	(	kIx(	(
<g/>
NASA	NASA	kA	NASA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lockheed	Lockheed	k1gMnSc1	Lockheed
Martin	Martin	k1gMnSc1	Martin
<g/>
,	,	kIx,	,
BAE	BAE	kA	BAE
Systems	Systems	k1gInSc1	Systems
<g/>
,	,	kIx,	,
Northrop	Northrop	k1gInSc1	Northrop
Grumman	Grumman	k1gMnSc1	Grumman
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
<g/>
.	.	kIx.	.
<g/>
SpaceX	SpaceX	k1gFnPc6	SpaceX
využívá	využívat	k5eAaImIp3nS	využívat
vysoký	vysoký	k2eAgInSc1d1	vysoký
stupeň	stupeň	k1gInSc1	stupeň
vertikální	vertikální	k2eAgFnSc2d1	vertikální
integrace	integrace	k1gFnSc2	integrace
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
svých	svůj	k3xOyFgFnPc2	svůj
raket	raketa	k1gFnPc2	raketa
a	a	k8xC	a
raketových	raketový	k2eAgInPc2d1	raketový
motorů	motor	k1gInPc2	motor
<g/>
.	.	kIx.	.
</s>
<s>
SpaceX	SpaceX	k?	SpaceX
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
veškeré	veškerý	k3xTgInPc4	veškerý
své	svůj	k3xOyFgInPc4	svůj
raketové	raketový	k2eAgInPc4d1	raketový
motory	motor	k1gInPc4	motor
<g/>
,	,	kIx,	,
raketové	raketový	k2eAgInPc4d1	raketový
stupně	stupeň	k1gInPc4	stupeň
<g/>
,	,	kIx,	,
kosmické	kosmický	k2eAgFnPc4d1	kosmická
lodě	loď	k1gFnPc4	loď
a	a	k8xC	a
veškerý	veškerý	k3xTgInSc4	veškerý
software	software	k1gInSc4	software
na	na	k7c6	na
jednom	jeden	k4xCgNnSc6	jeden
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
Hawthornu	Hawthorno	k1gNnSc6	Hawthorno
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
neobvyklé	obvyklý	k2eNgNnSc1d1	neobvyklé
pro	pro	k7c4	pro
letecký	letecký	k2eAgInSc4d1	letecký
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
SpaceX	SpaceX	k1gFnSc1	SpaceX
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
3000	[number]	k4	3000
dodavatelů	dodavatel	k1gMnPc2	dodavatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
třetina	třetina	k1gFnSc1	třetina
dodává	dodávat	k5eAaImIp3nS	dodávat
výrobky	výrobek	k1gInPc4	výrobek
skoro	skoro	k6eAd1	skoro
každý	každý	k3xTgInSc4	každý
týden	týden	k1gInSc4	týden
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Testovací	testovací	k2eAgNnPc4d1	testovací
a	a	k8xC	a
poletová	poletový	k2eAgNnPc4d1	poletový
demontážní	demontážní	k2eAgNnPc4d1	demontážní
zařízení	zařízení	k1gNnPc4	zařízení
===	===	k?	===
</s>
</p>
<p>
<s>
SpaceX	SpaceX	k?	SpaceX
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgNnPc4	dva
testovací	testovací	k2eAgNnPc4d1	testovací
zařízení	zařízení	k1gNnPc4	zařízení
pro	pro	k7c4	pro
rakety	raketa	k1gFnPc4	raketa
<g/>
:	:	kIx,	:
Vývojové	vývojový	k2eAgFnPc4d1	vývojová
a	a	k8xC	a
testovací	testovací	k2eAgNnSc1d1	testovací
zařízení	zařízení	k1gNnSc1	zařízení
SpaceX	SpaceX	k1gFnSc2	SpaceX
v	v	k7c6	v
McGregoru	McGregor	k1gInSc6	McGregor
<g/>
,	,	kIx,	,
Texas	Texas	k1gInSc1	Texas
a	a	k8xC	a
pronajaté	pronajatý	k2eAgNnSc1d1	pronajaté
testovací	testovací	k2eAgNnSc1d1	testovací
zařízení	zařízení	k1gNnSc1	zařízení
Spaceport	Spaceport	k1gInSc1	Spaceport
America	America	k1gMnSc1	America
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
provádět	provádět	k5eAaImF	provádět
testy	test	k1gInPc4	test
vertikálního	vertikální	k2eAgInSc2d1	vertikální
vzletu	vzlet	k1gInSc2	vzlet
a	a	k8xC	a
přistání	přistání	k1gNnSc2	přistání
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
raketové	raketový	k2eAgInPc1d1	raketový
motory	motor	k1gInPc1	motor
SpaceX	SpaceX	k1gMnPc2	SpaceX
jsou	být	k5eAaImIp3nP	být
testovány	testovat	k5eAaImNgInP	testovat
na	na	k7c6	na
raketových	raketový	k2eAgInPc6d1	raketový
zkušebních	zkušební	k2eAgInPc6d1	zkušební
stojanech	stojan	k1gInPc6	stojan
<g/>
.	.	kIx.	.
</s>
<s>
Testování	testování	k1gNnSc1	testování
letu	let	k1gInSc2	let
zkušebních	zkušební	k2eAgFnPc2d1	zkušební
raket	raketa	k1gFnPc2	raketa
Grasshopper	Grasshoppero	k1gNnPc2	Grasshoppero
v	v	k7c6	v
<g/>
1.0	[number]	k4	1.0
a	a	k8xC	a
F9R	F9R	k1gMnSc1	F9R
Dev	Dev	k1gFnSc2	Dev
<g/>
1	[number]	k4	1
v	v	k7c6	v
nízké	nízký	k2eAgFnSc6d1	nízká
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
v	v	k7c6	v
McGregoru	McGregor	k1gInSc6	McGregor
<g/>
.	.	kIx.	.
</s>
<s>
Testování	testování	k1gNnPc1	testování
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
výškách	výška	k1gFnPc6	výška
mělo	mít	k5eAaImAgNnS	mít
probíhat	probíhat	k5eAaImF	probíhat
na	na	k7c6	na
základně	základna	k1gFnSc6	základna
Spaceport	Spaceport	k1gInSc4	Spaceport
America	Americum	k1gNnSc2	Americum
s	s	k7c7	s
testovací	testovací	k2eAgFnSc7d1	testovací
raketou	raketa	k1gFnSc7	raketa
F9R	F9R	k1gMnSc7	F9R
Dev	Dev	k1gMnSc7	Dev
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc4	tento
plán	plán	k1gInSc4	plán
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
nebyl	být	k5eNaImAgInS	být
uskutečněn	uskutečněn	k2eAgInSc1d1	uskutečněn
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
McGregoru	McGregor	k1gInSc6	McGregor
provádí	provádět	k5eAaImIp3nS	provádět
poletová	poletový	k2eAgFnSc1d1	poletová
demontáž	demontáž	k1gFnSc1	demontáž
kabin	kabina	k1gFnPc2	kabina
Dragon	Dragon	k1gMnSc1	Dragon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Společnost	společnost	k1gFnSc1	společnost
zakoupila	zakoupit	k5eAaPmAgFnS	zakoupit
testovací	testovací	k2eAgNnSc4d1	testovací
zařízení	zařízení	k1gNnSc4	zařízení
v	v	k7c6	v
McGregoru	McGregor	k1gInSc6	McGregor
od	od	k7c2	od
zaniklé	zaniklý	k2eAgFnSc2d1	zaniklá
Beal	Beal	k1gMnSc1	Beal
Aerospace	Aerospace	k1gFnPc4	Aerospace
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
obnovila	obnovit	k5eAaPmAgFnS	obnovit
největší	veliký	k2eAgInSc4d3	veliký
testovací	testovací	k2eAgInSc4d1	testovací
stojan	stojan	k1gInSc4	stojan
pro	pro	k7c4	pro
testování	testování	k1gNnSc4	testování
motorů	motor	k1gInPc2	motor
Merlin	Merlina	k1gFnPc2	Merlina
<g/>
.	.	kIx.	.
</s>
<s>
SpaceX	SpaceX	k?	SpaceX
provedla	provést	k5eAaPmAgFnS	provést
od	od	k7c2	od
zakoupení	zakoupení	k1gNnSc2	zakoupení
objektu	objekt	k1gInSc2	objekt
řadu	řad	k1gInSc2	řad
zlepšení	zlepšení	k1gNnSc2	zlepšení
a	a	k8xC	a
také	také	k9	také
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
jeho	jeho	k3xOp3gFnSc1	jeho
velikost	velikost	k1gFnSc1	velikost
zakoupením	zakoupení	k1gNnSc7	zakoupení
několika	několik	k4yIc2	několik
kusů	kus	k1gInPc2	kus
přilehlé	přilehlý	k2eAgFnSc2d1	přilehlá
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
společnost	společnost	k1gFnSc1	společnost
oznámila	oznámit	k5eAaPmAgFnS	oznámit
plány	plán	k1gInPc4	plán
na	na	k7c4	na
vybudování	vybudování	k1gNnSc4	vybudování
půl	půl	k6eAd1	půl
akrové	akrový	k2eAgFnPc1d1	akrová
betonové	betonový	k2eAgFnPc1d1	betonová
plošiny	plošina	k1gFnPc1	plošina
pro	pro	k7c4	pro
provádění	provádění	k1gNnSc4	provádění
testů	test	k1gInPc2	test
vertikálního	vertikální	k2eAgInSc2d1	vertikální
vzletu	vzlet	k1gInSc2	vzlet
a	a	k8xC	a
přistání	přistání	k1gNnSc2	přistání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
plošinu	plošina	k1gFnSc4	plošina
vybudovala	vybudovat	k5eAaPmAgFnS	vybudovat
pro	pro	k7c4	pro
testování	testování	k1gNnSc4	testování
rakety	raketa	k1gFnSc2	raketa
Grasshopper	Grasshoppra	k1gFnPc2	Grasshoppra
<g/>
.	.	kIx.	.
<g/>
SpaceX	SpaceX	k1gFnSc1	SpaceX
staví	stavit	k5eAaImIp3nS	stavit
všechny	všechen	k3xTgMnPc4	všechen
svoje	svůj	k3xOyFgMnPc4	svůj
raketové	raketový	k2eAgMnPc4d1	raketový
motory	motor	k1gInPc4	motor
a	a	k8xC	a
rakety	raketa	k1gFnPc4	raketa
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
hlavním	hlavní	k2eAgInSc6d1	hlavní
závodě	závod	k1gInSc6	závod
v	v	k7c6	v
Hawthornu	Hawthorn	k1gInSc6	Hawthorn
<g/>
.	.	kIx.	.
</s>
<s>
Testovací	testovací	k2eAgNnPc1d1	testovací
zařízení	zařízení	k1gNnPc1	zařízení
v	v	k7c6	v
McGregoru	McGregor	k1gInSc6	McGregor
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
ze	z	k7c2	z
sekundárních	sekundární	k2eAgNnPc2d1	sekundární
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
společnost	společnost	k1gFnSc1	společnost
testuje	testovat	k5eAaImIp3nS	testovat
každý	každý	k3xTgInSc4	každý
nově	nově	k6eAd1	nově
vyrobený	vyrobený	k2eAgInSc4d1	vyrobený
motor	motor	k1gInSc4	motor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Odpalovací	odpalovací	k2eAgNnPc1d1	odpalovací
zařízení	zařízení	k1gNnPc1	zařízení
===	===	k?	===
</s>
</p>
<p>
<s>
SpaceX	SpaceX	k?	SpaceX
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
provozuje	provozovat	k5eAaImIp3nS	provozovat
tři	tři	k4xCgInPc4	tři
kosmodromy	kosmodrom	k1gInPc4	kosmodrom
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
na	na	k7c6	na
Mysu	mys	k1gInSc6	mys
Canaveral	Canaveral	k1gFnSc2	Canaveral
<g/>
,	,	kIx,	,
Vandenbergově	Vandenbergův	k2eAgFnSc3d1	Vandenbergova
letecké	letecký	k2eAgFnSc3d1	letecká
základně	základna	k1gFnSc3	základna
a	a	k8xC	a
Kennedyho	Kennedy	k1gMnSc2	Kennedy
vesmírném	vesmírný	k2eAgNnSc6d1	vesmírné
středisku	středisko	k1gNnSc6	středisko
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
aktuálně	aktuálně	k6eAd1	aktuálně
buduje	budovat	k5eAaImIp3nS	budovat
nový	nový	k2eAgInSc1d1	nový
<g/>
,	,	kIx,	,
plně	plně	k6eAd1	plně
soukromý	soukromý	k2eAgInSc1d1	soukromý
kosmodrom	kosmodrom	k1gInSc1	kosmodrom
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
<g/>
.	.	kIx.	.
</s>
<s>
Testovací	testovací	k2eAgInPc4d1	testovací
lety	let	k1gInPc4	let
Falconu	Falcon	k1gInSc2	Falcon
1	[number]	k4	1
probíhaly	probíhat	k5eAaImAgFnP	probíhat
na	na	k7c6	na
kosmodromu	kosmodrom	k1gInSc6	kosmodrom
Omelek	omelek	k1gInSc1	omelek
Island	Island	k1gInSc1	Island
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rampu	rampa	k1gFnSc4	rampa
SLC-40	SLC-40	k1gFnSc2	SLC-40
na	na	k7c6	na
Mysu	mys	k1gInSc6	mys
Canaveral	Canaveral	k1gFnSc2	Canaveral
využívá	využívat	k5eAaPmIp3nS	využívat
Falcon	Falcon	k1gNnSc1	Falcon
9	[number]	k4	9
pro	pro	k7c4	pro
lety	let	k1gInPc4	let
na	na	k7c6	na
LEO	Leo	k1gMnSc1	Leo
a	a	k8xC	a
geostacionární	geostacionární	k2eAgFnSc4d1	geostacionární
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2016	[number]	k4	2016
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
rampě	rampa	k1gFnSc6	rampa
k	k	k7c3	k
výbuchu	výbuch	k1gInSc3	výbuch
rakety	raketa	k1gFnSc2	raketa
Falcon	Falcon	k1gInSc1	Falcon
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nehodě	nehoda	k1gFnSc3	nehoda
došlo	dojít	k5eAaPmAgNnS	dojít
při	při	k7c6	při
testovacím	testovací	k2eAgMnSc6d1	testovací
tankování	tankování	k1gNnSc3	tankování
druhého	druhý	k4xOgInSc2	druhý
stupně	stupeň	k1gInSc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Rampa	rampa	k1gFnSc1	rampa
byla	být	k5eAaImAgFnS	být
silně	silně	k6eAd1	silně
poškozena	poškodit	k5eAaPmNgFnS	poškodit
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
by	by	kYmCp3nS	by
být	být	k5eAaImF	být
zprovozněna	zprovoznit	k5eAaPmNgFnS	zprovoznit
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
kosmodromu	kosmodrom	k1gInSc2	kosmodrom
je	být	k5eAaImIp3nS	být
také	také	k9	také
bývalý	bývalý	k2eAgMnSc1d1	bývalý
Launch	Launch	k1gMnSc1	Launch
Complex	Complex	k1gInSc4	Complex
13	[number]	k4	13
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
přejmenovaný	přejmenovaný	k2eAgInSc1d1	přejmenovaný
na	na	k7c4	na
Landing	Landing	k1gInSc4	Landing
zone	zon	k1gInSc2	zon
1	[number]	k4	1
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaImNgInS	využívat
pro	pro	k7c4	pro
přistávání	přistávání	k1gNnSc4	přistávání
prvních	první	k4xOgInPc2	první
stupňů	stupeň	k1gInPc2	stupeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rampu	rampa	k1gFnSc4	rampa
SLC-4E	SLC-4E	k1gFnSc2	SLC-4E
na	na	k7c6	na
Vandenbergově	Vandenbergův	k2eAgFnSc6d1	Vandenbergova
letecké	letecký	k2eAgFnSc6d1	letecká
základně	základna	k1gFnSc6	základna
využívá	využívat	k5eAaPmIp3nS	využívat
Falcon	Falcon	k1gNnSc1	Falcon
9	[number]	k4	9
pro	pro	k7c4	pro
lety	let	k1gInPc4	let
na	na	k7c4	na
polární	polární	k2eAgFnSc4d1	polární
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
rampy	rampa	k1gFnSc2	rampa
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
mít	mít	k5eAaImF	mít
možnost	možnost	k1gFnSc4	možnost
startovat	startovat	k5eAaBmF	startovat
i	i	k9	i
Falcon	Falcon	k1gInSc4	Falcon
Heavy	Heava	k1gFnSc2	Heava
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
možnosti	možnost	k1gFnSc6	možnost
už	už	k6eAd1	už
nemluví	mluvit	k5eNaImIp3nS	mluvit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rampu	rampa	k1gFnSc4	rampa
LC-39A	LC-39A	k1gMnSc2	LC-39A
v	v	k7c4	v
Kennedyho	Kennedy	k1gMnSc2	Kennedy
vesmírném	vesmírný	k2eAgNnSc6d1	vesmírné
středisku	středisko	k1gNnSc6	středisko
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
NASA	NASA	kA	NASA
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
pro	pro	k7c4	pro
komerční	komerční	k2eAgMnPc4d1	komerční
zájemce	zájemce	k1gMnPc4	zájemce
<g/>
.	.	kIx.	.
</s>
<s>
SpaceX	SpaceX	k?	SpaceX
podepsala	podepsat	k5eAaPmAgFnS	podepsat
nájemní	nájemní	k2eAgFnSc4d1	nájemní
smlouvu	smlouva	k1gFnSc4	smlouva
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Nájemní	nájemní	k2eAgFnSc1d1	nájemní
smlouva	smlouva	k1gFnSc1	smlouva
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
platit	platit	k5eAaImF	platit
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
SpaceX	SpaceX	k?	SpaceX
vybudovala	vybudovat	k5eAaPmAgFnS	vybudovat
u	u	k7c2	u
rampy	rampa	k1gFnSc2	rampa
nový	nový	k2eAgInSc4d1	nový
hangár	hangár	k1gInSc4	hangár
<g/>
.	.	kIx.	.
</s>
<s>
Rampa	rampa	k1gFnSc1	rampa
byla	být	k5eAaImAgFnS	být
plánována	plánovat	k5eAaImNgFnS	plánovat
pro	pro	k7c4	pro
starty	start	k1gInPc4	start
Falconu	Falcon	k1gInSc2	Falcon
Heavy	Heava	k1gFnSc2	Heava
a	a	k8xC	a
lety	let	k1gInPc4	let
s	s	k7c7	s
Crew	Crew	k1gMnSc7	Crew
Dragonem	Dragon	k1gMnSc7	Dragon
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yRgInPc2	který
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
potřeba	potřeba	k1gFnSc1	potřeba
delší	dlouhý	k2eAgFnSc2d2	delší
přípravy	příprava	k1gFnSc2	příprava
na	na	k7c4	na
start	start	k1gInSc4	start
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nehodě	nehoda	k1gFnSc6	nehoda
Falconu	Falcon	k1gInSc2	Falcon
9	[number]	k4	9
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
na	na	k7c6	na
rampě	rampa	k1gFnSc6	rampa
SLC-40	SLC-40	k1gFnPc2	SLC-40
byla	být	k5eAaImAgFnS	být
úprava	úprava	k1gFnSc1	úprava
rampy	rampa	k1gFnSc2	rampa
urychlena	urychlen	k2eAgFnSc1d1	urychlena
a	a	k8xC	a
první	první	k4xOgInSc4	první
start	start	k1gInSc4	start
Falconu	Falcon	k1gInSc2	Falcon
9	[number]	k4	9
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
rampy	rampa	k1gFnSc2	rampa
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2017	[number]	k4	2017
(	(	kIx(	(
<g/>
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
oprav	oprava	k1gFnPc2	oprava
rampy	rampa	k1gFnSc2	rampa
SLC-	SLC-	k1gFnSc7	SLC-
<g/>
40	[number]	k4	40
<g/>
)	)	kIx)	)
bude	být	k5eAaImBp3nS	být
rampa	rampa	k1gFnSc1	rampa
LC-39A	LC-39A	k1gMnPc2	LC-39A
dodatečně	dodatečně	k6eAd1	dodatečně
upravena	upravit	k5eAaPmNgFnS	upravit
pro	pro	k7c4	pro
starty	start	k1gInPc4	start
Falconu	Falcon	k1gInSc2	Falcon
Heavy	Heava	k1gFnSc2	Heava
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
demontováno	demontován	k2eAgNnSc4d1	demontováno
i	i	k8xC	i
obslužné	obslužný	k2eAgNnSc4d1	obslužné
rameno	rameno	k1gNnSc4	rameno
pro	pro	k7c4	pro
raketoplány	raketoplán	k1gInPc4	raketoplán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2014	[number]	k4	2014
SpaceX	SpaceX	k1gFnSc1	SpaceX
oznámila	oznámit	k5eAaPmAgFnS	oznámit
a	a	k8xC	a
zahájila	zahájit	k5eAaPmAgFnS	zahájit
stavbu	stavba	k1gFnSc4	stavba
plně	plně	k6eAd1	plně
komerčního	komerční	k2eAgInSc2d1	komerční
kosmodromu	kosmodrom	k1gInSc2	kosmodrom
na	na	k7c4	na
Boca	Boc	k2eAgNnPc4d1	Boc
Chica	Chicum	k1gNnPc4	Chicum
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zařízení	zařízení	k1gNnSc1	zařízení
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
satelitů	satelit	k1gInPc2	satelit
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
SpaceX	SpaceX	k1gFnSc1	SpaceX
oznámila	oznámit	k5eAaPmAgFnS	oznámit
svůj	svůj	k3xOyFgInSc4	svůj
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
podnikání	podnikání	k1gNnSc2	podnikání
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
výroby	výroba	k1gFnSc2	výroba
satelitů	satelit	k1gMnPc2	satelit
<g/>
.	.	kIx.	.
</s>
<s>
Továrna	továrna	k1gFnSc1	továrna
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
Seattlu	Seattl	k1gInSc6	Seattl
<g/>
,	,	kIx,	,
státě	stát	k1gInSc6	stát
Washington	Washington	k1gInSc1	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
zde	zde	k6eAd1	zde
bude	být	k5eAaImBp3nS	být
zaměstnáno	zaměstnat	k5eAaPmNgNnS	zaměstnat
přibližně	přibližně	k6eAd1	přibližně
60	[number]	k4	60
inženýrů	inženýr	k1gMnPc2	inženýr
<g/>
,	,	kIx,	,
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
několika	několik	k4yIc2	několik
let	léto	k1gNnPc2	léto
by	by	kYmCp3nS	by
však	však	k9	však
počet	počet	k1gInSc1	počet
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
mohl	moct	k5eAaImAgInS	moct
vzrůst	vzrůst	k1gInSc4	vzrůst
až	až	k9	až
na	na	k7c4	na
1000	[number]	k4	1000
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2016	[number]	k4	2016
SpaceX	SpaceX	k1gFnSc1	SpaceX
získala	získat	k5eAaPmAgFnS	získat
dalších	další	k2eAgInPc2d1	další
740	[number]	k4	740
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
metrů	metr	k1gInPc2	metr
v	v	k7c6	v
Irvine	Irvin	k1gInSc5	Irvin
<g/>
,	,	kIx,	,
CA	ca	kA	ca
<g/>
,	,	kIx,	,
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zaměřit	zaměřit	k5eAaPmF	zaměřit
se	se	k3xPyFc4	se
na	na	k7c4	na
satelitní	satelitní	k2eAgFnSc4d1	satelitní
komunikaci	komunikace	k1gFnSc4	komunikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Oblastní	oblastní	k2eAgFnSc2d1	oblastní
kanceláře	kancelář	k1gFnSc2	kancelář
===	===	k?	===
</s>
</p>
<p>
<s>
SpaceX	SpaceX	k?	SpaceX
má	mít	k5eAaImIp3nS	mít
oblastní	oblastní	k2eAgFnPc4d1	oblastní
kanceláře	kancelář	k1gFnPc4	kancelář
v	v	k7c6	v
Hustonu	Huston	k1gInSc6	Huston
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
<g/>
,	,	kIx,	,
Chantilly	Chantilla	k1gFnPc4	Chantilla
ve	v	k7c6	v
Virginii	Virginie	k1gFnSc6	Virginie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
D.C.	D.C.	k1gFnSc2	D.C.
a	a	k8xC	a
otevřela	otevřít	k5eAaPmAgFnS	otevřít
kanceláře	kancelář	k1gFnPc4	kancelář
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Seattlu	Seattl	k1gInSc2	Seattl
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nabírá	nabírat	k5eAaImIp3nS	nabírat
inženýry	inženýr	k1gMnPc4	inženýr
a	a	k8xC	a
vývojáře	vývojář	k1gMnPc4	vývojář
softwaru	software	k1gInSc2	software
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úspěchy	úspěch	k1gInPc1	úspěch
==	==	k?	==
</s>
</p>
<p>
<s>
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2008	[number]	k4	2008
při	při	k7c6	při
4	[number]	k4	4
<g/>
.	.	kIx.	.
zkušebním	zkušební	k2eAgInSc6d1	zkušební
letu	let	k1gInSc6	let
společnost	společnost	k1gFnSc4	společnost
úspěšně	úspěšně	k6eAd1	úspěšně
vypustila	vypustit	k5eAaPmAgFnS	vypustit
kosmickou	kosmický	k2eAgFnSc4d1	kosmická
raketu	raketa	k1gFnSc4	raketa
Falcon	Falcon	k1gInSc1	Falcon
1	[number]	k4	1
která	který	k3yIgFnSc1	který
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
165	[number]	k4	165
kg	kg	kA	kg
těžkou	těžký	k2eAgFnSc4d1	těžká
družici	družice	k1gFnSc4	družice
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stala	stát	k5eAaPmAgFnS	stát
historicky	historicky	k6eAd1	historicky
1	[number]	k4	1
<g/>
.	.	kIx.	.
soukromým	soukromý	k2eAgInSc7d1	soukromý
subjektem	subjekt	k1gInSc7	subjekt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
z	z	k7c2	z
ryze	ryze	k6eAd1	ryze
privátních	privátní	k2eAgInPc2d1	privátní
zdrojů	zdroj	k1gInPc2	zdroj
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
kapalinovou	kapalinový	k2eAgFnSc4d1	kapalinová
raketu	raketa	k1gFnSc4	raketa
a	a	k8xC	a
úspěšně	úspěšně	k6eAd1	úspěšně
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
oběžné	oběžný	k2eAgFnPc4d1	oběžná
dráhy	dráha	k1gFnPc4	dráha
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
raketa	raketa	k1gFnSc1	raketa
Falcon	Falcona	k1gFnPc2	Falcona
9	[number]	k4	9
dopravila	dopravit	k5eAaPmAgFnS	dopravit
po	po	k7c6	po
startu	start	k1gInSc6	start
z	z	k7c2	z
floridského	floridský	k2eAgInSc2d1	floridský
mysu	mys	k1gInSc2	mys
Canaveral	Canaveral	k1gFnSc4	Canaveral
maketu	maketa	k1gFnSc4	maketa
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodi	loď	k1gFnSc2	loď
Dragon	Dragon	k1gMnSc1	Dragon
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
250	[number]	k4	250
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
uskutečněn	uskutečnit	k5eAaPmNgInS	uskutečnit
první	první	k4xOgInSc4	první
(	(	kIx(	(
<g/>
bezpilotní	bezpilotní	k2eAgInSc4d1	bezpilotní
<g/>
)	)	kIx)	)
zkušební	zkušební	k2eAgInSc4d1	zkušební
let	let	k1gInSc4	let
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodi	loď	k1gFnSc2	loď
Dragon	Dragon	k1gMnSc1	Dragon
<g/>
,	,	kIx,	,
když	když	k8xS	když
ji	on	k3xPp3gFnSc4	on
raketa	raketa	k1gFnSc1	raketa
Falcon	Falcon	k1gNnSc1	Falcon
9	[number]	k4	9
z	z	k7c2	z
téhož	týž	k3xTgInSc2	týž
mysu	mys	k1gInSc2	mys
dopravila	dopravit	k5eAaPmAgFnS	dopravit
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
(	(	kIx(	(
<g/>
Dragon	Dragon	k1gMnSc1	Dragon
se	se	k3xPyFc4	se
od	od	k7c2	od
rakety	raketa	k1gFnSc2	raketa
Falcon	Falcon	k1gInSc4	Falcon
9	[number]	k4	9
oddělila	oddělit	k5eAaPmAgFnS	oddělit
po	po	k7c6	po
10	[number]	k4	10
minutách	minuta	k1gFnPc6	minuta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Dragon	Dragon	k1gMnSc1	Dragon
3	[number]	k4	3
hodiny	hodina	k1gFnSc2	hodina
manévrovala	manévrovat	k5eAaImAgNnP	manévrovat
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
zpátky	zpátky	k6eAd1	zpátky
atmosférou	atmosféra	k1gFnSc7	atmosféra
a	a	k8xC	a
úspěšně	úspěšně	k6eAd1	úspěšně
přistála	přistát	k5eAaImAgFnS	přistát
na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
mysu	mys	k1gInSc2	mys
Canaveral	Canaveral	k1gFnPc2	Canaveral
raketou	raketa	k1gFnSc7	raketa
Falcon	Falcona	k1gFnPc2	Falcona
9	[number]	k4	9
vynesena	vynesen	k2eAgFnSc1d1	vynesena
loď	loď	k1gFnSc1	loď
Dragon	Dragon	k1gMnSc1	Dragon
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
spojila	spojit	k5eAaPmAgFnS	spojit
s	s	k7c7	s
Mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
stanici	stanice	k1gFnSc3	stanice
(	(	kIx(	(
<g/>
ISS	ISS	kA	ISS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
posádce	posádka	k1gFnSc3	posádka
nesla	nést	k5eAaImAgFnS	nést
521	[number]	k4	521
kg	kg	kA	kg
nákladu	náklad	k1gInSc2	náklad
jako	jako	k8xC	jako
zásoby	zásoba	k1gFnPc4	zásoba
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
oblečení	oblečení	k1gNnSc2	oblečení
a	a	k8xC	a
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgInSc1	první
případ	případ	k1gInSc1	případ
připojení	připojení	k1gNnSc2	připojení
soukromé	soukromý	k2eAgFnSc2d1	soukromá
vesmírné	vesmírný	k2eAgFnSc2d1	vesmírná
lodi	loď	k1gFnSc2	loď
k	k	k7c3	k
ISS	ISS	kA	ISS
<g/>
,	,	kIx,	,
mluví	mluvit	k5eAaImIp3nS	mluvit
se	se	k3xPyFc4	se
tak	tak	k9	tak
o	o	k7c6	o
nové	nový	k2eAgFnSc6d1	nová
éře	éra	k1gFnSc6	éra
dobývání	dobývání	k1gNnSc2	dobývání
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
skončila	skončit	k5eAaPmAgFnS	skončit
mise	mise	k1gFnSc1	mise
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Modul	modul	k1gInSc1	modul
Dragon	Dragon	k1gMnSc1	Dragon
přistál	přistát	k5eAaPmAgMnS	přistát
podle	podle	k7c2	podle
plánu	plán	k1gInSc2	plán
v	v	k7c6	v
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
a	a	k8xC	a
dopravil	dopravit	k5eAaPmAgMnS	dopravit
ze	z	k7c2	z
stanice	stanice	k1gFnSc2	stanice
ISS	ISS	kA	ISS
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
660	[number]	k4	660
kg	kg	kA	kg
nákladu	náklad	k1gInSc2	náklad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
ISS	ISS	kA	ISS
znovu	znovu	k6eAd1	znovu
raketou	raketa	k1gFnSc7	raketa
Falcon	Falcona	k1gFnPc2	Falcona
9	[number]	k4	9
vypuštěna	vypuštěn	k2eAgFnSc1d1	vypuštěna
kosmická	kosmický	k2eAgFnSc1d1	kosmická
loď	loď	k1gFnSc1	loď
Dragon	Dragon	k1gMnSc1	Dragon
s	s	k7c7	s
nákladem	náklad	k1gInSc7	náklad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2013	[number]	k4	2013
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
rakety	raketa	k1gFnSc2	raketa
Falcon	Falcon	k1gNnSc4	Falcon
dopravila	dopravit	k5eAaPmAgFnS	dopravit
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
Země	zem	k1gFnSc2	zem
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
36	[number]	k4	36
000	[number]	k4	000
km	km	kA	km
telekomunikační	telekomunikační	k2eAgInSc4d1	telekomunikační
satelit	satelit	k1gInSc4	satelit
SES-8	SES-8	k1gMnPc2	SES-8
patřící	patřící	k2eAgFnSc2d1	patřící
lucemburské	lucemburský	k2eAgFnSc2d1	Lucemburská
společnosti	společnost	k1gFnSc2	společnost
SES	SES	kA	SES
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
službu	služba	k1gFnSc4	služba
je	být	k5eAaImIp3nS	být
55	[number]	k4	55
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2014	[number]	k4	2014
se	se	k3xPyFc4	se
nákladní	nákladní	k2eAgFnSc1d1	nákladní
loď	loď	k1gFnSc1	loď
Dragon	Dragon	k1gMnSc1	Dragon
vynesená	vynesený	k2eAgFnSc1d1	vynesená
raketou	raketa	k1gFnSc7	raketa
Falcon	Falcon	k1gNnSc1	Falcon
9	[number]	k4	9
připojila	připojit	k5eAaPmAgFnS	připojit
k	k	k7c3	k
ISS	ISS	kA	ISS
s	s	k7c7	s
2300	[number]	k4	2300
kg	kg	kA	kg
nákladu	náklad	k1gInSc2	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Let	let	k1gInSc1	let
byl	být	k5eAaImAgInS	být
uskutečněn	uskutečnit	k5eAaPmNgInS	uskutečnit
po	po	k7c6	po
několika	několik	k4yIc6	několik
odkladech	odklad	k1gInPc6	odklad
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
letu	let	k1gInSc2	let
byl	být	k5eAaImAgMnS	být
test	test	k1gInSc4	test
přistání	přistání	k1gNnSc2	přistání
prvního	první	k4xOgMnSc2	první
stupně	stupeň	k1gInSc2	stupeň
nosné	nosný	k2eAgFnPc1d1	nosná
rakety	raketa	k1gFnPc1	raketa
na	na	k7c4	na
mořskou	mořský	k2eAgFnSc4d1	mořská
hladinu	hladina	k1gFnSc4	hladina
<g/>
,	,	kIx,	,
stupeň	stupeň	k1gInSc4	stupeň
přistál	přistát	k5eAaImAgMnS	přistát
<g/>
,	,	kIx,	,
zachránit	zachránit	k5eAaPmF	zachránit
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2015	[number]	k4	2015
úspěšně	úspěšně	k6eAd1	úspěšně
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
modernizovaná	modernizovaný	k2eAgFnSc1d1	modernizovaná
verze	verze	k1gFnSc1	verze
Falconu	Falcon	k1gInSc2	Falcon
9	[number]	k4	9
(	(	kIx(	(
<g/>
1.2	[number]	k4	1.2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
Země	zem	k1gFnSc2	zem
vynesla	vynést	k5eAaPmAgFnS	vynést
11	[number]	k4	11
satelitů	satelit	k1gInPc2	satelit
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
letu	let	k1gInSc2	let
bylo	být	k5eAaImAgNnS	být
přistání	přistání	k1gNnSc1	přistání
prvního	první	k4xOgMnSc2	první
stupně	stupeň	k1gInSc2	stupeň
nosné	nosný	k2eAgFnPc1d1	nosná
rakety	raketa	k1gFnPc1	raketa
zpátky	zpátky	k6eAd1	zpátky
na	na	k7c4	na
pozemní	pozemní	k2eAgFnSc4d1	pozemní
plochu	plocha	k1gFnSc4	plocha
letiště	letiště	k1gNnSc2	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Let	let	k1gInSc1	let
byl	být	k5eAaImAgInS	být
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
a	a	k8xC	a
SpaceX	SpaceX	k1gFnSc1	SpaceX
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
první	první	k4xOgFnSc7	první
společností	společnost	k1gFnSc7	společnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dokázala	dokázat	k5eAaPmAgFnS	dokázat
vrátit	vrátit	k5eAaPmF	vrátit
první	první	k4xOgInSc4	první
stupeň	stupeň	k1gInSc4	stupeň
rakety	raketa	k1gFnSc2	raketa
na	na	k7c4	na
pevnou	pevný	k2eAgFnSc4d1	pevná
zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2016	[number]	k4	2016
úspěšně	úspěšně	k6eAd1	úspěšně
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
mise	mise	k1gFnSc2	mise
CRS-8	CRS-8	k1gFnSc2	CRS-8
loď	loď	k1gFnSc1	loď
Falcon	Falcon	k1gInSc1	Falcon
9	[number]	k4	9
(	(	kIx(	(
<g/>
1.2	[number]	k4	1.2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
stupeň	stupeň	k1gInSc1	stupeň
se	se	k3xPyFc4	se
po	po	k7c6	po
oddělení	oddělení	k1gNnSc6	oddělení
úspěšně	úspěšně	k6eAd1	úspěšně
vrátil	vrátit	k5eAaPmAgInS	vrátit
na	na	k7c4	na
plovoucí	plovoucí	k2eAgFnSc4d1	plovoucí
plošinu	plošina	k1gFnSc4	plošina
Of	Of	k1gFnSc2	Of
Course	Course	k1gFnSc2	Course
I	i	k9	i
Still	Still	k1gMnSc1	Still
Love	lov	k1gInSc5	lov
You	You	k1gMnSc6	You
v	v	k7c6	v
Atlantiku	Atlantik	k1gInSc6	Atlantik
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
stupeň	stupeň	k1gInSc1	stupeň
<g/>
,	,	kIx,	,
loď	loď	k1gFnSc1	loď
Dragon	Dragon	k1gMnSc1	Dragon
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
úspěšně	úspěšně	k6eAd1	úspěšně
doletěl	doletět	k5eAaPmAgInS	doletět
k	k	k7c3	k
vesmírné	vesmírný	k2eAgFnSc3d1	vesmírná
stanici	stanice	k1gFnSc3	stanice
ISS	ISS	kA	ISS
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yRgFnSc3	který
vynesl	vynést	k5eAaPmAgInS	vynést
nafukovací	nafukovací	k2eAgInSc1d1	nafukovací
modul	modul	k1gInSc1	modul
BEAM	BEAM	kA	BEAM
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2017	[number]	k4	2017
dopravila	dopravit	k5eAaPmAgFnS	dopravit
raketa	raketa	k1gFnSc1	raketa
Falcon	Falcon	k1gInSc1	Falcon
9	[number]	k4	9
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
780	[number]	k4	780
km	km	kA	km
prvních	první	k4xOgNnPc6	první
10	[number]	k4	10
ze	z	k7c2	z
72	[number]	k4	72
plánovaných	plánovaný	k2eAgFnPc2d1	plánovaná
družic	družice	k1gFnPc2	družice
satelitní	satelitní	k2eAgFnSc2d1	satelitní
sítě	síť	k1gFnSc2	síť
IRIDIUM	iridium	k1gNnSc1	iridium
Next	Next	k1gInSc1	Next
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
ze	z	k7c2	z
satelitů	satelit	k1gInPc2	satelit
vážil	vážit	k5eAaImAgInS	vážit
860	[number]	k4	860
kilogramů	kilogram	k1gInPc2	kilogram
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
stupeň	stupeň	k1gInSc4	stupeň
rakety	raketa	k1gFnSc2	raketa
Falcon	Falcon	k1gInSc4	Falcon
9	[number]	k4	9
navíc	navíc	k6eAd1	navíc
úspěšně	úspěšně	k6eAd1	úspěšně
přistál	přistát	k5eAaPmAgInS	přistát
na	na	k7c6	na
plovoucí	plovoucí	k2eAgFnSc6d1	plovoucí
plošině	plošina	k1gFnSc6	plošina
a	a	k8xC	a
SpaceX	SpaceX	k1gFnSc1	SpaceX
ho	on	k3xPp3gMnSc4	on
tak	tak	k9	tak
může	moct	k5eAaImIp3nS	moct
znovu	znovu	k6eAd1	znovu
použít	použít	k5eAaPmF	použít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Projekty	projekt	k1gInPc1	projekt
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Meziplanetární	meziplanetární	k2eAgInSc1d1	meziplanetární
dopravní	dopravní	k2eAgInSc1d1	dopravní
systém	systém	k1gInSc1	systém
===	===	k?	===
</s>
</p>
<p>
<s>
SpaceX	SpaceX	k?	SpaceX
v	v	k7c6	v
září	září	k1gNnSc6	září
2016	[number]	k4	2016
oznámila	oznámit	k5eAaPmAgFnS	oznámit
plány	plán	k1gInPc4	plán
ohledně	ohledně	k7c2	ohledně
Meziplanetárního	meziplanetární	k2eAgInSc2d1	meziplanetární
dopravního	dopravní	k2eAgInSc2d1	dopravní
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
umožnit	umožnit	k5eAaPmF	umožnit
lidskou	lidský	k2eAgFnSc4d1	lidská
misi	mise	k1gFnSc4	mise
na	na	k7c4	na
Mars	Mars	k1gInSc4	Mars
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
kolonizaci	kolonizace	k1gFnSc4	kolonizace
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
vývoj	vývoj	k1gInSc4	vývoj
velké	velký	k2eAgFnSc2d1	velká
nosné	nosný	k2eAgFnSc2d1	nosná
rakety	raketa	k1gFnSc2	raketa
a	a	k8xC	a
vesmírné	vesmírný	k2eAgFnSc2d1	vesmírná
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
zkouškách	zkouška	k1gFnPc6	zkouška
motoru	motor	k1gInSc2	motor
nového	nový	k2eAgInSc2d1	nový
motoru	motor	k1gInSc2	motor
Raptor	Raptor	k1gMnSc1	Raptor
a	a	k8xC	a
velké	velký	k2eAgFnSc3d1	velká
nádrži	nádrž	k1gFnSc3	nádrž
z	z	k7c2	z
uhlíkových	uhlíkový	k2eAgFnPc2d1	uhlíková
vláken	vlákna	k1gFnPc2	vlákna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Satelitní	satelitní	k2eAgInSc1d1	satelitní
internet	internet	k1gInSc1	internet
===	===	k?	===
</s>
</p>
<p>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
SpaceX	SpaceX	k1gFnSc1	SpaceX
oznámila	oznámit	k5eAaPmAgFnS	oznámit
záměr	záměr	k1gInSc4	záměr
vybudovat	vybudovat	k5eAaPmF	vybudovat
telekomunikační	telekomunikační	k2eAgInSc4d1	telekomunikační
satelitní	satelitní	k2eAgInSc4d1	satelitní
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
šířit	šířit	k5eAaImF	šířit
internetové	internetový	k2eAgNnSc4d1	internetové
připojení	připojení	k1gNnSc4	připojení
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Řádově	řádově	k6eAd1	řádově
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInPc2	tisíc
satelitů	satelit	k1gInPc2	satelit
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgFnPc1	první
byly	být	k5eAaImAgFnP	být
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2018	[number]	k4	2018
vypuštěny	vypuštěn	k2eAgInPc1d1	vypuštěn
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
sekundární	sekundární	k2eAgInSc1d1	sekundární
náklad	náklad	k1gInSc1	náklad
družice	družice	k1gFnSc2	družice
Paz	Paz	k1gFnSc2	Paz
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
testovací	testovací	k2eAgInPc4d1	testovací
satelity	satelit	k1gInPc4	satelit
Microsat-	Microsat-	k1gFnSc2	Microsat-
<g/>
2	[number]	k4	2
<g/>
a	a	k8xC	a
a	a	k8xC	a
Microsat-	Microsat-	k1gFnSc1	Microsat-
<g/>
2	[number]	k4	2
<g/>
b	b	k?	b
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
znamé	znamý	k2eAgFnPc1d1	znamá
také	také	k9	také
jako	jako	k9	jako
Tintin	Tintin	k2eAgInSc1d1	Tintin
A	a	k9	a
a	a	k8xC	a
Tintin	Tintin	k1gMnSc1	Tintin
B.	B.	kA	B.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Dragon	Dragon	k1gMnSc1	Dragon
(	(	kIx(	(
<g/>
kosmická	kosmický	k2eAgFnSc1d1	kosmická
loď	loď	k1gFnSc1	loď
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
COTS	COTS	kA	COTS
Demo	demo	k2eAgInSc4d1	demo
Flight	Flight	k1gInSc4	Flight
2	[number]	k4	2
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
SpaceX	SpaceX	k1gFnSc2	SpaceX
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Domovská	domovský	k2eAgFnSc1d1	domovská
stránka	stránka	k1gFnSc1	stránka
společnosti	společnost	k1gFnSc2	společnost
</s>
</p>
<p>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
letu	let	k1gInSc6	let
z	z	k7c2	z
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
</s>
</p>
