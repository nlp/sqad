<s>
Kaleidoskop	kaleidoskop	k1gInSc1
</s>
<s>
Pohled	pohled	k1gInSc1
na	na	k7c4
měnící	měnící	k2eAgNnSc4d1
se	se	k3xPyFc4
obrazce	obrazec	k1gInSc2
kaleidoskopu	kaleidoskop	k1gInSc2
s	s	k7c7
korálky	korálek	k1gInPc7
</s>
<s>
Obraz	obraz	k1gInSc1
z	z	k7c2
krasohledu	krasohled	k1gInSc2
se	s	k7c7
skleněnou	skleněný	k2eAgFnSc7d1
kuličkou	kulička	k1gFnSc7
na	na	k7c6
konci	konec	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Video	video	k1gNnSc1
<g/>
:	:	kIx,
<g/>
Obraz	obraz	k1gInSc1
z	z	k7c2
krasohledu	krasohled	k1gInSc2
se	s	k7c7
skleněnou	skleněný	k2eAgFnSc7d1
kuličkou	kulička	k1gFnSc7
na	na	k7c6
konci	konec	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Kaleidoskop	kaleidoskop	k1gInSc1
<g/>
,	,	kIx,
česky	česky	k6eAd1
krasohled	krasohled	k1gInSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
dětská	dětský	k2eAgFnSc1d1
hračka	hračka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
pomocí	pomocí	k7c2
soustavy	soustava	k1gFnSc2
zrcadel	zrcadlo	k1gNnPc2
a	a	k8xC
barevných	barevný	k2eAgNnPc2d1
tělísek	tělísko	k1gNnPc2
nebo	nebo	k8xC
skleněné	skleněný	k2eAgFnSc2d1
kuličky	kulička	k1gFnSc2
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
neopakovatelné	opakovatelný	k2eNgInPc4d1
obrazce	obrazec	k1gInPc4
při	při	k7c6
pohledu	pohled	k1gInSc6
proti	proti	k7c3
světelnému	světelný	k2eAgInSc3d1
zdroji	zdroj	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Stavba	stavba	k1gFnSc1
</s>
<s>
Kaleidoskop	kaleidoskop	k1gInSc1
–	–	k?
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
válec	válec	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
má	mít	k5eAaImIp3nS
z	z	k7c2
jedné	jeden	k4xCgFnSc2
strany	strana	k1gFnSc2
otevřenou	otevřený	k2eAgFnSc4d1
dírku	dírka	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc7,k3yIgFnSc7,k3yRgFnSc7
se	se	k3xPyFc4
do	do	k7c2
válce	válec	k1gInSc2
hledí	hledět	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
válci	válec	k1gInSc6
jsou	být	k5eAaImIp3nP
podélně	podélně	k6eAd1
vložena	vložen	k2eAgNnPc4d1
tři	tři	k4xCgNnPc4
zrcadla	zrcadlo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prostor	prostor	k1gInSc1
mezi	mezi	k7c7
nimi	on	k3xPp3gInPc7
má	mít	k5eAaImIp3nS
tvar	tvar	k1gInSc1
rovnoramenného	rovnoramenný	k2eAgInSc2d1
trojúhelníka	trojúhelník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
druhé	druhý	k4xOgFnSc6
straně	strana	k1gFnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
malý	malý	k2eAgInSc1d1
prostor	prostor	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
jsou	být	k5eAaImIp3nP
umístěna	umístěn	k2eAgNnPc1d1
barevná	barevný	k2eAgNnPc1d1
tělesa	těleso	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
matnice	matnice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
soustavě	soustava	k1gFnSc3
zrcadel	zrcadlo	k1gNnPc2
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
pravidelnému	pravidelný	k2eAgInSc3d1
vícenásobnému	vícenásobný	k2eAgInSc3d1
odrazu	odraz	k1gInSc3
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
požadované	požadovaný	k2eAgInPc4d1
optické	optický	k2eAgInPc4d1
jevy	jev	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kaleidoskopem	kaleidoskop	k1gInSc7
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
otáčet	otáčet	k5eAaImF
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
se	se	k3xPyFc4
drobná	drobný	k2eAgNnPc1d1
barevná	barevný	k2eAgNnPc1d1
tělesa	těleso	k1gNnPc1
přeskupují	přeskupovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
projevuje	projevovat	k5eAaImIp3nS
změnou	změna	k1gFnSc7
tvarů	tvar	k1gInPc2
pro	pro	k7c4
pozorovatele	pozorovatel	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Teleidoskop	Teleidoskop	k1gInSc1
–	–	k?
je	být	k5eAaImIp3nS
podobný	podobný	k2eAgInSc4d1
válec	válec	k1gInSc4
<g/>
,	,	kIx,
jenom	jenom	k9
na	na	k7c6
jeho	jeho	k3xOp3gInSc6
konci	konec	k1gInSc6
se	se	k3xPyFc4
místo	místo	k7c2
matnice	matnice	k1gFnSc2
a	a	k8xC
prostoru	prostor	k1gInSc2
s	s	k7c7
korálky	korálek	k1gInPc7
nachází	nacházet	k5eAaImIp3nS
skleněná	skleněný	k2eAgFnSc1d1
kulička	kulička	k1gFnSc1
<g/>
,	,	kIx,
přes	přes	k7c4
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
vidíme	vidět	k5eAaImIp1nP
sféricky	sféricky	k6eAd1
zkreslený	zkreslený	k2eAgInSc4d1
obraz	obraz	k1gInSc4
okolí	okolí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obraz	obraz	k1gInSc1
je	být	k5eAaImIp3nS
taktéž	taktéž	k?
několikrát	několikrát	k6eAd1
násobený	násobený	k2eAgInSc1d1
soustavou	soustava	k1gFnSc7
zrcadel	zrcadlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Změna	změna	k1gFnSc1
sledovaného	sledovaný	k2eAgInSc2d1
obrazu	obraz	k1gInSc2
se	se	k3xPyFc4
dosáhne	dosáhnout	k5eAaPmIp3nS
změnou	změna	k1gFnSc7
nasměrování	nasměrování	k1gNnSc2
válce	válec	k1gInSc2
vůči	vůči	k7c3
okolí	okolí	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Kaleidoskop	kaleidoskop	k1gInSc4
vynalezl	vynaleznout	k5eAaPmAgMnS
Angličan	Angličan	k1gMnSc1
Sir	sir	k1gMnSc1
David	David	k1gMnSc1
Brewster	Brewster	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1815	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
kaleidoskop	kaleidoskop	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
kaleidoskop	kaleidoskop	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
