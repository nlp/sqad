<s>
Kaleidoskop	kaleidoskop	k1gInSc1
<g/>
,	,	kIx,
česky	česky	k6eAd1
krasohled	krasohled	k1gInSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
dětská	dětský	k2eAgFnSc1d1
hračka	hračka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
pomocí	pomocí	k7c2
soustavy	soustava	k1gFnSc2
zrcadel	zrcadlo	k1gNnPc2
a	a	k8xC
barevných	barevný	k2eAgNnPc2d1
tělísek	tělísko	k1gNnPc2
nebo	nebo	k8xC
skleněné	skleněný	k2eAgFnSc2d1
kuličky	kulička	k1gFnSc2
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
neopakovatelné	opakovatelný	k2eNgInPc4d1
obrazce	obrazec	k1gInPc4
při	při	k7c6
pohledu	pohled	k1gInSc6
proti	proti	k7c3
světelnému	světelný	k2eAgInSc3d1
zdroji	zdroj	k1gInSc3
<g/>
.	.	kIx.
</s>