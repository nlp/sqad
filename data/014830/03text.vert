<s>
Fotbal	fotbal	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
kopané	kopaná	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Fotbal	fotbal	k1gInSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Fotbal	fotbal	k1gInSc1
Vrcholné	vrcholný	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
(	(	kIx(
<g/>
muži	muž	k1gMnPc1
<g/>
)	)	kIx)
</s>
<s>
MS	MS	kA
1930	#num#	k4
Mistrovství	mistrovství	k1gNnPc2
Evropy	Evropa	k1gFnSc2
(	(	kIx(
<g/>
muži	muž	k1gMnPc1
<g/>
)	)	kIx)
</s>
<s>
ME	ME	kA
1960	#num#	k4
Mistrovství	mistrovství	k1gNnPc2
světa	svět	k1gInSc2
(	(	kIx(
<g/>
ženy	žena	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
MS	MS	kA
1991	#num#	k4
Mistrovství	mistrovství	k1gNnPc2
Evropy	Evropa	k1gFnSc2
(	(	kIx(
<g/>
ženy	žena	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
ME	ME	kA
1984	#num#	k4
Olympijský	olympijský	k2eAgInSc1d1
turnaj	turnaj	k1gInSc1
</s>
<s>
LOH	LOH	kA
1900	#num#	k4
–	–	k?
neoficiálníLOH	neoficiálníLOH	k?
1908	#num#	k4
–	–	k?
oficiální	oficiální	k2eAgFnSc1d1
Mezinárodní	mezinárodní	k2eAgFnSc1d1
federace	federace	k1gFnSc1
Název	název	k1gInSc1
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
federace	federace	k1gFnSc1
(	(	kIx(
<g/>
FIFA	FIFA	kA
<g/>
)	)	kIx)
Založena	založen	k2eAgFnSc1d1
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1904	#num#	k4
Web	web	k1gInSc1
</s>
<s>
www.fifa.com	www.fifa.com	k1gInSc1
Národní	národní	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Název	název	k1gInSc1
</s>
<s>
FAČR	FAČR	kA
Web	web	k1gInSc1
</s>
<s>
www.fotbal.cz	www.fotbal.cz	k1gMnSc1
</s>
<s>
Fotbal	fotbal	k1gInSc1
(	(	kIx(
<g/>
z	z	k7c2
anglického	anglický	k2eAgNnSc2d1
football	footbalnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
foot	foot	k1gInSc1
=	=	kIx~
noha	noha	k1gFnSc1
<g/>
,	,	kIx,
ball	ball	k1gInSc1
=	=	kIx~
míč	míč	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
též	též	k9
kopaná	kopaná	k1gFnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
kolektivní	kolektivní	k2eAgFnSc1d1
míčová	míčový	k2eAgFnSc1d1
hra	hra	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
nejpopulárnějším	populární	k2eAgMnSc7d3
kolektivním	kolektivní	k2eAgInSc7d1
sportem	sport	k1gInSc7
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
fotbale	fotbal	k1gInSc6
hrají	hrát	k5eAaImIp3nP
dvě	dva	k4xCgNnPc1
družstva	družstvo	k1gNnPc1
po	po	k7c6
jedenácti	jedenáct	k4xCc6
hráčích	hráč	k1gMnPc6
na	na	k7c6
obdélníkovém	obdélníkový	k2eAgMnSc6d1
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
travnatém	travnatý	k2eAgNnSc6d1
hřišti	hřiště	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInSc7
cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
dosáhnout	dosáhnout	k5eAaPmF
více	hodně	k6eAd2
branek	branka	k1gFnPc2
(	(	kIx(
<g/>
gólů	gól	k1gInPc2
<g/>
)	)	kIx)
než	než	k8xS
soupeř	soupeř	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Branky	branka	k1gFnSc2
je	být	k5eAaImIp3nS
dosaženo	dosáhnout	k5eAaPmNgNnS
tehdy	tehdy	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
míč	míč	k1gInSc1
přejde	přejít	k5eAaPmIp3nS
brankovou	brankový	k2eAgFnSc4d1
čáru	čára	k1gFnSc4
mezi	mezi	k7c7
tyčemi	tyč	k1gFnPc7
branky	branka	k1gFnSc2
celým	celý	k2eAgInSc7d1
objemem	objem	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hraje	hrát	k5eAaImIp3nS
se	se	k3xPyFc4
hlavně	hlavně	k9
nohama	noha	k1gFnPc7
<g/>
,	,	kIx,
ale	ale	k8xC
hráči	hráč	k1gMnPc1
mohou	moct	k5eAaImIp3nP
k	k	k7c3
hraní	hraní	k1gNnSc3
míčem	míč	k1gInSc7
používat	používat	k5eAaImF
libovolné	libovolný	k2eAgFnPc4d1
části	část	k1gFnPc4
těla	tělo	k1gNnSc2
kromě	kromě	k7c2
rukou	ruka	k1gFnPc2
a	a	k8xC
paží	paže	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouze	pouze	k6eAd1
brankář	brankář	k1gMnSc1
(	(	kIx(
<g/>
tzn.	tzn.	kA
jeden	jeden	k4xCgMnSc1
z	z	k7c2
hráčů	hráč	k1gMnPc2
<g/>
,	,	kIx,
odlišený	odlišený	k2eAgInSc4d1
barvou	barva	k1gFnSc7
dresu	dres	k1gInSc2
<g/>
)	)	kIx)
může	moct	k5eAaImIp3nS
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
vlastní	vlastní	k2eAgFnSc2d1
branky	branka	k1gFnSc2
hrát	hrát	k5eAaImF
i	i	k9
rukama	ruka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Průběh	průběh	k1gInSc1
a	a	k8xC
způsob	způsob	k1gInSc1
hry	hra	k1gFnSc2
</s>
<s>
Fotbalový	fotbalový	k2eAgInSc1d1
zápas	zápas	k1gInSc1
(	(	kIx(
<g/>
AOL-Arena	AOL-Arena	k1gFnSc1
<g/>
,	,	kIx,
Hamburk	Hamburk	k1gInSc1
<g/>
,	,	kIx,
květen	květen	k1gInSc1
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hry	hra	k1gFnPc1
se	se	k3xPyFc4
účastní	účastnit	k5eAaImIp3nP
dva	dva	k4xCgInPc1
týmy	tým	k1gInPc1
<g/>
,	,	kIx,
každý	každý	k3xTgInSc1
o	o	k7c6
jedenácti	jedenáct	k4xCc6
hráčích	hráč	k1gMnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
hru	hra	k1gFnSc4
dohlíží	dohlížet	k5eAaImIp3nP
rozhodčí	rozhodčí	k1gMnPc1
<g/>
:	:	kIx,
hlavní	hlavní	k2eAgMnSc1d1
rozhodčí	rozhodčí	k1gMnSc1
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
mezi	mezi	k7c7
hráči	hráč	k1gMnPc7
na	na	k7c6
hrací	hrací	k2eAgFnSc6d1
ploše	plocha	k1gFnSc6
a	a	k8xC
má	mít	k5eAaImIp3nS
plnou	plný	k2eAgFnSc4d1
odpovědnost	odpovědnost	k1gFnSc4
k	k	k7c3
řízení	řízení	k1gNnSc3
zápasu	zápas	k1gInSc2
<g/>
;	;	kIx,
je	být	k5eAaImIp3nS
vybaven	vybavit	k5eAaPmNgInS
píšťalkou	píšťalka	k1gFnSc7
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc7,k3yIgFnSc7,k3yQgFnSc7
signalizuje	signalizovat	k5eAaImIp3nS
zahájení	zahájení	k1gNnSc1
a	a	k8xC
přerušení	přerušení	k1gNnSc1
hry	hra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těsně	těsně	k6eAd1
za	za	k7c7
okrajem	okraj	k1gInSc7
hrací	hrací	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
na	na	k7c6
delších	dlouhý	k2eAgNnPc6d2
okrajích	okrají	k1gNnPc6
hřiště	hřiště	k1gNnSc2
se	se	k3xPyFc4
pohybují	pohybovat	k5eAaImIp3nP
dva	dva	k4xCgMnPc1
asistenti	asistent	k1gMnPc1
rozhodčího	rozhodčí	k1gMnSc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
hlavnímu	hlavní	k2eAgInSc3d1
rozhodčímu	rozhodčí	k1gMnSc3
praporkem	praporek	k1gInSc7
signalizují	signalizovat	k5eAaImIp3nP
některé	některý	k3yIgInPc4
druhy	druh	k1gInPc4
porušení	porušení	k1gNnSc2
pravidel	pravidlo	k1gNnPc2
<g/>
,	,	kIx,
nemají	mít	k5eNaImIp3nP
však	však	k9
samostatnou	samostatný	k2eAgFnSc4d1
rozhodovací	rozhodovací	k2eAgFnSc4d1
pravomoc	pravomoc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgFnPc6
soutěžích	soutěž	k1gFnPc6
jsou	být	k5eAaImIp3nP
také	také	k9
dva	dva	k4xCgMnPc1
další	další	k2eAgMnPc1d1
rozhodčí	rozhodčí	k1gMnPc1
<g/>
,	,	kIx,
tzv.	tzv.	kA
brankoví	brankový	k2eAgMnPc1d1
rozhodčí	rozhodčí	k1gMnPc1
<g/>
,	,	kIx,
stojící	stojící	k2eAgMnSc1d1
mimo	mimo	k7c4
hřiště	hřiště	k1gNnPc4
těsně	těsně	k6eAd1
vedle	vedle	k7c2
každé	každý	k3xTgFnSc2
branky	branka	k1gFnSc2
<g/>
;	;	kIx,
ti	ten	k3xDgMnPc1
do	do	k7c2
hry	hra	k1gFnSc2
přímo	přímo	k6eAd1
nezasahují	zasahovat	k5eNaImIp3nP
a	a	k8xC
nemají	mít	k5eNaImIp3nP
ani	ani	k8xC
praporek	praporek	k1gInSc4
<g/>
,	,	kIx,
fungují	fungovat	k5eAaImIp3nP
jako	jako	k9
poradci	poradce	k1gMnPc1
hlavního	hlavní	k2eAgMnSc2d1
rozhodčího	rozhodčí	k1gMnSc2
<g/>
,	,	kIx,
s	s	k7c7
nímž	jenž	k3xRgInSc7
jsou	být	k5eAaImIp3nP
ve	v	k7c6
spojení	spojení	k1gNnSc4
minivysílačkou	minivysílačka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS
se	se	k3xPyFc4
jedním	jeden	k4xCgInSc7
kulatým	kulatý	k2eAgInSc7d1
míčem	míč	k1gInSc7
<g/>
,	,	kIx,
kterým	který	k3yIgMnPc3,k3yRgMnPc3,k3yQgMnPc3
hráči	hráč	k1gMnPc1
pohybují	pohybovat	k5eAaImIp3nP
na	na	k7c6
hrací	hrací	k2eAgFnSc6d1
ploše	plocha	k1gFnSc6
<g/>
:	:	kIx,
„	„	k?
<g/>
driblováním	driblování	k1gNnSc7
<g/>
“	“	k?
(	(	kIx(
<g/>
běháním	běhání	k1gNnSc7
s	s	k7c7
míčem	míč	k1gInSc7
ovládaným	ovládaný	k2eAgInSc7d1
jemnými	jemný	k2eAgInPc7d1
kopy	kop	k1gInPc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přihrávkami	přihrávka	k1gFnPc7
spoluhráčům	spoluhráč	k1gMnPc3
a	a	k8xC
konečně	konečně	k6eAd1
střelbou	střelba	k1gFnSc7
na	na	k7c4
soupeřovu	soupeřův	k2eAgFnSc4d1
branku	branka	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
brání	bránit	k5eAaImIp3nS
protivníkův	protivníkův	k2eAgMnSc1d1
brankář	brankář	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Družstvo	družstvo	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
zrovna	zrovna	k6eAd1
nemá	mít	k5eNaImIp3nS
pod	pod	k7c7
kontrolou	kontrola	k1gFnSc7
míč	míč	k1gInSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
jej	on	k3xPp3gInSc2
snaží	snažit	k5eAaImIp3nS
získat	získat	k5eAaPmF
<g/>
:	:	kIx,
zachycením	zachycení	k1gNnSc7
soupeřovy	soupeřův	k2eAgFnSc2d1
přihrávky	přihrávka	k1gFnSc2
nebo	nebo	k8xC
„	„	k?
<g/>
napadáním	napadání	k1gNnSc7
<g/>
“	“	k?
soupeře	soupeř	k1gMnSc2
<g/>
,	,	kIx,
u	u	k7c2
kterého	který	k3yQgInSc2,k3yRgInSc2,k3yIgInSc2
je	být	k5eAaImIp3nS
míč	míč	k1gInSc1
<g/>
;	;	kIx,
dovolený	dovolený	k2eAgInSc1d1
fyzický	fyzický	k2eAgInSc1d1
kontakt	kontakt	k1gInSc1
mezi	mezi	k7c7
hráči	hráč	k1gMnPc7
je	být	k5eAaImIp3nS
však	však	k9
výrazně	výrazně	k6eAd1
omezen	omezit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s>
Fotbalový	fotbalový	k2eAgInSc1d1
zápas	zápas	k1gInSc1
obecně	obecně	k6eAd1
probíhá	probíhat	k5eAaImIp3nS
plynule	plynule	k6eAd1
<g/>
,	,	kIx,
přerušení	přerušení	k1gNnPc1
jsou	být	k5eAaImIp3nP
způsobena	způsoben	k2eAgNnPc1d1
porušením	porušení	k1gNnSc7
pravidel	pravidlo	k1gNnPc2
(	(	kIx(
<g/>
např.	např.	kA
když	když	k8xS
se	se	k3xPyFc4
míč	míč	k1gInSc1
dostane	dostat	k5eAaPmIp3nS
mimo	mimo	k7c4
hrací	hrací	k2eAgFnSc4d1
plochu	plocha	k1gFnSc4
<g/>
,	,	kIx,
když	když	k8xS
hráč	hráč	k1gMnSc1
mimo	mimo	k7c4
brankáře	brankář	k1gMnPc4
zahraje	zahrát	k5eAaPmIp3nS
rukou	ruka	k1gFnSc7
<g/>
,	,	kIx,
když	když	k8xS
při	při	k7c6
snaze	snaha	k1gFnSc6
o	o	k7c4
odebrání	odebrání	k1gNnSc4
míče	míč	k1gInSc2
soupeři	soupeř	k1gMnSc3
hráč	hráč	k1gMnSc1
protivníka	protivník	k1gMnSc2
kopne	kopnout	k5eAaPmIp3nS
či	či	k8xC
jinak	jinak	k6eAd1
překročí	překročit	k5eAaPmIp3nP
dovolené	dovolený	k2eAgInPc4d1
způsoby	způsob	k1gInPc4
napadání	napadání	k1gNnSc2
atd.	atd.	kA
<g/>
)	)	kIx)
a	a	k8xC
hra	hra	k1gFnSc1
po	po	k7c6
nich	on	k3xPp3gInPc6
většinou	většinou	k6eAd1
jen	jen	k6eAd1
s	s	k7c7
malým	malý	k2eAgNnSc7d1
zdržením	zdržení	k1gNnSc7
pokračuje	pokračovat	k5eAaImIp3nS
dál	daleko	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hraje	hrát	k5eAaImIp3nS
se	se	k3xPyFc4
celkem	celkem	k6eAd1
devadesát	devadesát	k4xCc4
minut	minuta	k1gFnPc2
hrubého	hrubý	k2eAgInSc2d1
času	čas	k1gInSc2
(	(	kIx(
<g/>
čas	čas	k1gInSc1
běží	běžet	k5eAaImIp3nS
i	i	k9
v	v	k7c6
přerušené	přerušený	k2eAgFnSc6d1
hře	hra	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s	s	k7c7
patnáctiminutovou	patnáctiminutový	k2eAgFnSc7d1
přestávkou	přestávka	k1gFnSc7
v	v	k7c6
polovině	polovina	k1gFnSc6
<g/>
,	,	kIx,
po	po	k7c4
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
si	se	k3xPyFc3
soupeři	soupeř	k1gMnPc1
vymění	vyměnit	k5eAaPmIp3nP
poloviny	polovina	k1gFnPc4
hřiště	hřiště	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
obou	dva	k4xCgInPc2
poločasů	poločas	k1gInPc2
rozhodčí	rozhodčí	k1gMnSc1
stanoví	stanovit	k5eAaPmIp3nS
<g/>
,	,	kIx,
kolik	kolika	k1gFnPc2
času	čas	k1gInSc2
se	se	k3xPyFc4
bude	být	k5eAaImBp3nS
hrát	hrát	k5eAaImF
nad	nad	k7c4
rámec	rámec	k1gInSc4
45	#num#	k4
minut	minuta	k1gFnPc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
se	se	k3xPyFc4
nahrazuje	nahrazovat	k5eAaImIp3nS
čas	čas	k1gInSc4
ztracený	ztracený	k2eAgInSc4d1
např.	např.	kA
ošetřováním	ošetřování	k1gNnSc7
zraněných	zraněný	k2eAgMnPc2d1
hráčů	hráč	k1gMnPc2
či	či	k8xC
střídáním	střídání	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typicky	typicky	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
zhruba	zhruba	k6eAd1
o	o	k7c4
jednu	jeden	k4xCgFnSc4
minutu	minuta	k1gFnSc4
na	na	k7c6
konci	konec	k1gInSc6
prvního	první	k4xOgInSc2
poločasu	poločas	k1gInSc2
a	a	k8xC
tři	tři	k4xCgFnPc4
minuty	minuta	k1gFnPc4
na	na	k7c6
konci	konec	k1gInSc6
druhého	druhý	k4xOgInSc2
poločasu	poločas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
skončení	skončení	k1gNnSc6
druhého	druhý	k4xOgInSc2
poločasu	poločas	k1gInSc2
je	být	k5eAaImIp3nS
vítězem	vítěz	k1gMnSc7
zápasu	zápas	k1gInSc2
družstvo	družstvo	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc4,k3yQgNnSc4,k3yRgNnSc4
soupeři	soupeř	k1gMnSc3
vstřelilo	vstřelit	k5eAaPmAgNnS
víc	hodně	k6eAd2
branek	branka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
obě	dva	k4xCgNnPc4
družstva	družstvo	k1gNnPc4
obdržela	obdržet	k5eAaPmAgFnS
stejný	stejný	k2eAgInSc4d1
počet	počet	k1gInSc4
branek	branka	k1gFnPc2
<g/>
,	,	kIx,
končí	končit	k5eAaImIp3nS
zápas	zápas	k1gInSc1
remízou	remíza	k1gFnSc7
<g/>
;	;	kIx,
v	v	k7c6
některých	některý	k3yIgFnPc6
soutěžích	soutěž	k1gFnPc6
je	být	k5eAaImIp3nS
však	však	k9
remíza	remíza	k1gFnSc1
nepřípustná	přípustný	k2eNgFnSc1d1
a	a	k8xC
hra	hra	k1gFnSc1
se	se	k3xPyFc4
musí	muset	k5eAaImIp3nS
rozhodnout	rozhodnout	k5eAaPmF
<g/>
,	,	kIx,
zpravidla	zpravidla	k6eAd1
prodloužením	prodloužení	k1gNnSc7
2	#num#	k4
<g/>
×	×	k?
<g/>
15	#num#	k4
minut	minuta	k1gFnPc2
a	a	k8xC
pokud	pokud	k8xS
ani	ani	k8xC
poté	poté	k6eAd1
není	být	k5eNaImIp3nS
rozhodnuto	rozhodnout	k5eAaPmNgNnS
<g/>
,	,	kIx,
následuje	následovat	k5eAaImIp3nS
penaltový	penaltový	k2eAgInSc4d1
rozstřel	rozstřel	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
moderním	moderní	k2eAgInSc6d1
fotbale	fotbal	k1gInSc6
padá	padat	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
málo	málo	k4c1
branek	branka	k1gFnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
na	na	k7c4
MS	MS	kA
2006	#num#	k4
byl	být	k5eAaImAgInS
průměr	průměr	k1gInSc1
2,3	2,3	k4
gólu	gól	k1gInSc2
na	na	k7c4
zápas	zápas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
jedenácti	jedenáct	k4xCc2
hráčů	hráč	k1gMnPc2
každého	každý	k3xTgNnSc2
družstva	družstvo	k1gNnSc2
je	být	k5eAaImIp3nS
jeden	jeden	k4xCgMnSc1
podle	podle	k7c2
pravidel	pravidlo	k1gNnPc2
určen	určen	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
brankář	brankář	k1gMnSc1
<g/>
:	:	kIx,
má	mít	k5eAaImIp3nS
odlišnou	odlišný	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
dresu	dres	k1gInSc2
a	a	k8xC
platí	platit	k5eAaImIp3nS
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
ta	ten	k3xDgFnSc1
výjimka	výjimka	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
smí	smět	k5eAaImIp3nS
v	v	k7c6
okolí	okolí	k1gNnSc6
vlastní	vlastní	k2eAgFnSc2d1
branky	branka	k1gFnSc2
hrát	hrát	k5eAaImF
míč	míč	k1gInSc4
rukama	ruka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
hlediska	hledisko	k1gNnSc2
pravidel	pravidlo	k1gNnPc2
jsou	být	k5eAaImIp3nP
všichni	všechen	k3xTgMnPc1
ostatní	ostatní	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
stejní	stejný	k2eAgMnPc1d1
<g/>
,	,	kIx,
v	v	k7c6
praxi	praxe	k1gFnSc6
se	se	k3xPyFc4
však	však	k9
vyvinuly	vyvinout	k5eAaPmAgFnP
specializované	specializovaný	k2eAgFnPc1d1
funkce	funkce	k1gFnPc1
dané	daný	k2eAgFnPc1d1
konkrétními	konkrétní	k2eAgInPc7d1
schopnostmi	schopnost	k1gFnPc7
jednotlivých	jednotlivý	k2eAgMnPc2d1
hráčů	hráč	k1gMnPc2
<g/>
,	,	kIx,
taktickými	taktický	k2eAgInPc7d1
záměry	záměr	k1gInPc7
a	a	k8xC
úkoly	úkol	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
na	na	k7c6
hřišti	hřiště	k1gNnSc6
plní	plnit	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
Jak	jak	k6eAd1
už	už	k6eAd1
bylo	být	k5eAaImAgNnS
uvedeno	uvést	k5eAaPmNgNnS
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
brankář	brankář	k1gMnSc1
za	za	k7c4
úkol	úkol	k1gInSc4
bránit	bránit	k5eAaImF
soupeři	soupeř	k1gMnPc1
ve	v	k7c6
vstřelení	vstřelení	k1gNnSc6
gólu	gól	k1gInSc2
<g/>
;	;	kIx,
většinu	většina	k1gFnSc4
času	čas	k1gInSc2
proto	proto	k8xC
setrvává	setrvávat	k5eAaImIp3nS
v	v	k7c6
okolí	okolí	k1gNnSc6
vlastní	vlastní	k2eAgFnSc2d1
branky	branka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
zásadě	zásada	k1gFnSc6
podobný	podobný	k2eAgInSc4d1
cíl	cíl	k1gInSc4
mají	mít	k5eAaImIp3nP
hráči	hráč	k1gMnPc1
označovaní	označovaný	k2eAgMnPc1d1
jako	jako	k8xS,k8xC
obránci	obránce	k1gMnPc1
(	(	kIx(
<g/>
slangově	slangově	k6eAd1
„	„	k?
<g/>
beci	bek	k1gMnPc1
<g/>
“	“	k?
z	z	k7c2
anglického	anglický	k2eAgMnSc2d1
back	back	k6eAd1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
pohybují	pohybovat	k5eAaImIp3nP
se	se	k3xPyFc4
spíše	spíše	k9
na	na	k7c6
vlastní	vlastní	k2eAgFnSc6d1
polovině	polovina	k1gFnSc6
hřiště	hřiště	k1gNnSc2
a	a	k8xC
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
pokutovém	pokutový	k2eAgNnSc6d1
území	území	k1gNnSc6
a	a	k8xC
mají	mít	k5eAaImIp3nP
za	za	k7c4
úkol	úkol	k1gInSc4
zejména	zejména	k9
zastavit	zastavit	k5eAaPmF
přicházející	přicházející	k2eAgInSc4d1
soupeřův	soupeřův	k2eAgInSc4d1
útok	útok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
moderním	moderní	k2eAgNnSc6d1
pojetí	pojetí	k1gNnSc6
hry	hra	k1gFnSc2
však	však	k9
zejména	zejména	k9
krajní	krajní	k2eAgMnPc1d1
obránci	obránce	k1gMnPc1
pomáhají	pomáhat	k5eAaImIp3nP
i	i	k9
při	při	k7c6
útoku	útok	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Záložníci	záložník	k1gMnPc1
tvoří	tvořit	k5eAaImIp3nP
středovou	středový	k2eAgFnSc4d1
část	část	k1gFnSc4
družstva	družstvo	k1gNnSc2
<g/>
:	:	kIx,
pohybují	pohybovat	k5eAaImIp3nP
se	se	k3xPyFc4
po	po	k7c6
celé	celý	k2eAgFnSc6d1
ploše	plocha	k1gFnSc6
hřiště	hřiště	k1gNnSc2
<g/>
,	,	kIx,
podle	podle	k7c2
potřeby	potřeba	k1gFnSc2
se	se	k3xPyFc4
zapojují	zapojovat	k5eAaImIp3nP
do	do	k7c2
obrany	obrana	k1gFnSc2
i	i	k8xC
útoku	útok	k1gInSc2
(	(	kIx(
<g/>
ale	ale	k8xC
i	i	k9
zde	zde	k6eAd1
se	se	k3xPyFc4
užívá	užívat	k5eAaImIp3nS
specializace	specializace	k1gFnSc1
na	na	k7c4
defenzivní	defenzivní	k2eAgInPc4d1
a	a	k8xC
ofenzivní	ofenzivní	k2eAgMnPc4d1
záložníky	záložník	k1gMnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zejména	zejména	k9
středoví	středový	k2eAgMnPc1d1
záložníci	záložník	k1gMnPc1
mívají	mívat	k5eAaImIp3nP
za	za	k7c4
úkol	úkol	k1gInSc4
„	„	k?
<g/>
tvořit	tvořit	k5eAaImF
hru	hra	k1gFnSc4
<g/>
“	“	k?
<g/>
:	:	kIx,
rozehrávat	rozehrávat	k5eAaImF
útočné	útočný	k2eAgFnPc4d1
akce	akce	k1gFnPc4
<g/>
,	,	kIx,
rozdělovat	rozdělovat	k5eAaImF
přihrávkami	přihrávka	k1gFnPc7
míče	míč	k1gInSc2
spoluhráčům	spoluhráč	k1gMnPc3
atd.	atd.	kA
<g/>
,	,	kIx,
záložníky	záložník	k1gMnPc7
proto	proto	k8xC
bývají	bývat	k5eAaImIp3nP
technicky	technicky	k6eAd1
velmi	velmi	k6eAd1
schopní	schopný	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Útočníci	útočník	k1gMnPc1
mají	mít	k5eAaImIp3nP
za	za	k7c4
úkol	úkol	k1gInSc4
zakončovat	zakončovat	k5eAaImF
útočné	útočný	k2eAgFnPc4d1
akce	akce	k1gFnPc4
a	a	k8xC
střílet	střílet	k5eAaImF
soupeři	soupeř	k1gMnSc3
góly	gól	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohybují	pohybovat	k5eAaImIp3nP
se	se	k3xPyFc4
proto	proto	k8xC
nejdále	daleko	k6eAd3
od	od	k7c2
vlastní	vlastní	k2eAgFnSc2d1
branky	branka	k1gFnSc2
a	a	k8xC
do	do	k7c2
obrany	obrana	k1gFnSc2
se	se	k3xPyFc4
zapojují	zapojovat	k5eAaImIp3nP
jen	jen	k9
zřídkakdy	zřídkakdy	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
hrají	hrát	k5eAaImIp3nP
hráči	hráč	k1gMnPc1
převážně	převážně	k6eAd1
v	v	k7c6
rozestavení	rozestavení	k1gNnSc6
4-4-2	4-4-2	k4
(	(	kIx(
<g/>
4	#num#	k4
obránci	obránce	k1gMnPc7
<g/>
,	,	kIx,
4	#num#	k4
záložníci	záložník	k1gMnPc1
a	a	k8xC
2	#num#	k4
útočníci	útočník	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Dříve	dříve	k6eAd2
se	se	k3xPyFc4
však	však	k9
hrávalo	hrávat	k5eAaImAgNnS
v	v	k7c6
rozestaveních	rozestavení	k1gNnPc6
3-2-3-2	3-2-3-2	k4
(	(	kIx(
<g/>
80	#num#	k4
<g/>
.	.	kIx.
léta	léto	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
4-2-4	4-2-4	k4
(	(	kIx(
<g/>
60	#num#	k4
<g/>
.	.	kIx.
léta	léto	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
3-2-2-3	3-2-2-3	k4
(	(	kIx(
<g/>
30	#num#	k4
<g/>
.	.	kIx.
léta	léto	k1gNnSc2
<g/>
)	)	kIx)
i	i	k9
1-2-7	1-2-7	k4
(	(	kIx(
<g/>
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1
hráčů	hráč	k1gMnPc2
na	na	k7c4
jednotlivé	jednotlivý	k2eAgInPc4d1
posty	post	k1gInPc4
a	a	k8xC
stanovení	stanovení	k1gNnSc4
jejich	jejich	k3xOp3gInPc2
úkolů	úkol	k1gInPc2
a	a	k8xC
obecně	obecně	k6eAd1
herní	herní	k2eAgFnSc2d1
taktiky	taktika	k1gFnSc2
je	být	k5eAaImIp3nS
zodpovědností	zodpovědnost	k1gFnSc7
trenéra	trenér	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trenéři	trenér	k1gMnPc1
obou	dva	k4xCgFnPc2
týmů	tým	k1gInPc2
při	při	k7c6
zápase	zápas	k1gInSc6
pobývají	pobývat	k5eAaImIp3nP
nedaleko	nedaleko	k7c2
okraje	okraj	k1gInSc2
hrací	hrací	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
(	(	kIx(
<g/>
poblíž	poblíž	k7c2
středu	střed	k1gInSc2
jednoho	jeden	k4xCgInSc2
delšího	dlouhý	k2eAgInSc2d2
okraje	okraj	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
udílejí	udílet	k5eAaImIp3nP
rady	rada	k1gFnPc1
a	a	k8xC
pokyny	pokyn	k1gInPc1
hráčům	hráč	k1gMnPc3
na	na	k7c6
hřišti	hřiště	k1gNnSc6
a	a	k8xC
případně	případně	k6eAd1
vyměňují	vyměňovat	k5eAaImIp3nP
hráče	hráč	k1gMnPc4
na	na	k7c6
hřišti	hřiště	k1gNnSc6
za	za	k7c4
náhradníky	náhradník	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
oběma	dva	k4xCgFnPc7
lavičkami	lavička	k1gFnPc7
náhradníků	náhradník	k1gMnPc2
se	se	k3xPyFc4
zdržuje	zdržovat	k5eAaImIp3nS
čtvrtý	čtvrtý	k4xOgMnSc1
rozhodčí	rozhodčí	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
řídí	řídit	k5eAaImIp3nS
střídání	střídání	k1gNnSc1
<g/>
,	,	kIx,
kontroluje	kontrolovat	k5eAaImIp3nS
dodržování	dodržování	k1gNnSc1
pravidel	pravidlo	k1gNnPc2
osobami	osoba	k1gFnPc7
na	na	k7c6
lavičkách	lavička	k1gFnPc6
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
případech	případ	k1gInPc6
zranění	zranění	k1gNnSc2
některého	některý	k3yIgMnSc2
ze	z	k7c2
tří	tři	k4xCgMnPc2
rozhodčích	rozhodčí	k1gMnPc2
na	na	k7c6
hřišti	hřiště	k1gNnSc6
může	moct	k5eAaImIp3nS
zraněného	zraněný	k2eAgMnSc4d1
rozhodčího	rozhodčí	k1gMnSc4
vystřídat	vystřídat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
fotbalu	fotbal	k1gInSc2
ve	v	k7c6
Florencii	Florencie	k1gFnSc6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Existence	existence	k1gFnSc1
hry	hra	k1gFnSc2
podobné	podobný	k2eAgFnSc2d1
fotbalu	fotbal	k1gInSc2
je	být	k5eAaImIp3nS
doložena	doložit	k5eAaPmNgFnS
až	až	k6eAd1
do	do	k7c2
období	období	k1gNnSc2
starověku	starověk	k1gInSc2
<g/>
:	:	kIx,
nejstarší	starý	k2eAgMnSc1d3
takovou	takový	k3xDgFnSc7
hrou	hra	k1gFnSc7
bylo	být	k5eAaImAgNnS
čínské	čínský	k2eAgNnSc1d1
cchu-ťü	cchu-ťü	k?
<g/>
,	,	kIx,
hrané	hraný	k2eAgFnSc6d1
již	již	k6eAd1
ve	v	k7c6
2	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
V	v	k7c6
Řecku	Řecko	k1gNnSc6
a	a	k8xC
Římě	Řím	k1gInSc6
se	se	k3xPyFc4
hrálo	hrát	k5eAaImAgNnS
mnoho	mnoho	k4c1
míčových	míčový	k2eAgFnPc2d1
her	hra	k1gFnPc2
<g/>
,	,	kIx,
při	při	k7c6
některých	některý	k3yIgFnPc6
se	se	k3xPyFc4
hrálo	hrát	k5eAaImAgNnS
nohama	noha	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgMnSc7
ze	z	k7c2
vzdálených	vzdálený	k2eAgMnPc2d1
předchůdců	předchůdce	k1gMnPc2
fotbalu	fotbal	k1gInSc2
je	být	k5eAaImIp3nS
tak	tak	k9
např.	např.	kA
římská	římský	k2eAgFnSc1d1
hra	hra	k1gFnSc1
harpastum	harpastum	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
středověku	středověk	k1gInSc6
se	se	k3xPyFc4
různé	různý	k2eAgFnPc1d1
hry	hra	k1gFnPc1
podobné	podobný	k2eAgFnPc1d1
fotbalu	fotbal	k1gInSc2
hrály	hrát	k5eAaImAgFnP
po	po	k7c6
celé	celý	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gNnPc1
pravidla	pravidlo	k1gNnPc1
se	se	k3xPyFc4
však	však	k9
výrazně	výrazně	k6eAd1
lišila	lišit	k5eAaImAgFnS
místo	místo	k1gNnSc4
od	od	k7c2
místa	místo	k1gNnSc2
i	i	k9
v	v	k7c6
průběhu	průběh	k1gInSc6
doby	doba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vznik	vznik	k1gInSc1
dnešního	dnešní	k2eAgInSc2d1
fotbalu	fotbal	k1gInSc2
má	mít	k5eAaImIp3nS
kořeny	kořen	k1gInPc4
v	v	k7c6
Anglii	Anglie	k1gFnSc6
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tamní	tamní	k2eAgFnPc1d1
soukromé	soukromý	k2eAgFnPc1d1
střední	střední	k2eAgFnPc1d1
školy	škola	k1gFnPc1
(	(	kIx(
<g/>
public	publicum	k1gNnPc2
schools	schoolsa	k1gFnPc2
<g/>
)	)	kIx)
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
začaly	začít	k5eAaPmAgFnP
žáky	žák	k1gMnPc7
nutit	nutit	k5eAaImF
ke	k	k7c3
sportu	sport	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
ze	z	k7c2
škol	škola	k1gFnPc2
však	však	k9
používala	používat	k5eAaImAgNnP
vlastní	vlastní	k2eAgNnPc1d1
pravidla	pravidlo	k1gNnPc1
<g/>
,	,	kIx,
odrážející	odrážející	k2eAgFnPc1d1
místní	místní	k2eAgFnPc1d1
podmínky	podmínka	k1gFnPc1
(	(	kIx(
<g/>
velikost	velikost	k1gFnSc1
hřiště	hřiště	k1gNnSc2
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pravidla	pravidlo	k1gNnPc1
se	se	k3xPyFc4
navíc	navíc	k6eAd1
postupně	postupně	k6eAd1
měnila	měnit	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
vykrystalizovaly	vykrystalizovat	k5eAaPmAgInP
dvě	dva	k4xCgFnPc4
hlavní	hlavní	k2eAgFnPc4d1
formy	forma	k1gFnPc4
fotbalu	fotbal	k1gInSc2
<g/>
:	:	kIx,
v	v	k7c6
jedné	jeden	k4xCgFnSc6
verzi	verze	k1gFnSc6
pravidel	pravidlo	k1gNnPc2
(	(	kIx(
<g/>
používané	používaný	k2eAgFnPc4d1
např.	např.	kA
v	v	k7c6
Rugby	rugby	k1gNnSc6
<g/>
,	,	kIx,
Marlborough	Marlborougha	k1gFnPc2
či	či	k8xC
Cheltenhamu	Cheltenham	k1gInSc2
<g/>
)	)	kIx)
hráči	hráč	k1gMnSc3
míč	míč	k1gInSc1
po	po	k7c6
hřišti	hřiště	k1gNnSc6
přenášeli	přenášet	k5eAaImAgMnP
rukama	ruka	k1gFnPc7
<g/>
,	,	kIx,
v	v	k7c6
druhé	druhý	k4xOgFnSc6
verzi	verze	k1gFnSc6
(	(	kIx(
<g/>
např.	např.	kA
na	na	k7c6
Etonu	Etono	k1gNnSc6
<g/>
,	,	kIx,
Harrow	Harrow	k1gFnSc6
<g/>
,	,	kIx,
Westminsteru	Westminster	k1gInSc6
či	či	k8xC
Charterhouse	Charterhous	k1gInSc6
<g/>
)	)	kIx)
se	se	k3xPyFc4
upřednostňovalo	upřednostňovat	k5eAaImAgNnS
kopání	kopání	k1gNnSc1
do	do	k7c2
míče	míč	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
40	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
Anglii	Anglie	k1gFnSc6
zasáhl	zasáhnout	k5eAaPmAgInS
boom	boom	k1gInSc4
železniční	železniční	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
technologický	technologický	k2eAgInSc1d1
rozvoj	rozvoj	k1gInSc1
umožnil	umožnit	k5eAaPmAgInS
pořádání	pořádání	k1gNnSc4
soutěžních	soutěžní	k2eAgInPc2d1
zápasů	zápas	k1gInPc2
mezi	mezi	k7c7
jednotlivými	jednotlivý	k2eAgFnPc7d1
školami	škola	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
zatímco	zatímco	k8xS
rozdíly	rozdíl	k1gInPc1
v	v	k7c6
místních	místní	k2eAgNnPc6d1
pravidlech	pravidlo	k1gNnPc6
atletiky	atletika	k1gFnSc2
byly	být	k5eAaImAgInP
nevýznamné	významný	k2eNgInPc1d1
<g/>
,	,	kIx,
meziškolní	meziškolní	k2eAgInPc1d1
zápasy	zápas	k1gInPc1
ve	v	k7c6
fotbale	fotbal	k1gInSc6
byly	být	k5eAaImAgInP
kvůli	kvůli	k7c3
zásadním	zásadní	k2eAgInPc3d1
rozdílům	rozdíl	k1gInPc3
v	v	k7c6
pojetí	pojetí	k1gNnSc6
hry	hra	k1gFnSc2
prakticky	prakticky	k6eAd1
nemožné	možný	k2eNgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1848	#num#	k4
sešlo	sejít	k5eAaPmAgNnS
14	#num#	k4
zástupců	zástupce	k1gMnPc2
škol	škola	k1gFnPc2
na	na	k7c6
jednání	jednání	k1gNnSc6
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
výsledkem	výsledek	k1gInSc7
byla	být	k5eAaImAgFnS
první	první	k4xOgFnSc1
ucelená	ucelený	k2eAgFnSc1d1
sada	sada	k1gFnSc1
pravidel	pravidlo	k1gNnPc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
Cambridgeská	cambridgeský	k2eAgNnPc4d1
pravidla	pravidlo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgNnPc1
pravidla	pravidlo	k1gNnPc1
upřednostňovala	upřednostňovat	k5eAaImAgNnP
kopání	kopání	k1gNnPc1
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
však	však	k9
dovoleno	dovolit	k5eAaPmNgNnS
také	také	k6eAd1
čisté	čistý	k2eAgNnSc1d1
zachycení	zachycení	k1gNnSc1
míče	míč	k1gInSc2
rukama	ruka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Sjednocující	sjednocující	k2eAgFnPc1d1
tendence	tendence	k1gFnPc1
vyústily	vyústit	k5eAaPmAgFnP
v	v	k7c6
založení	založení	k1gNnSc6
The	The	k1gFnSc2
Football	Footballa	k1gFnPc2
Association	Association	k1gInSc1
<g/>
,	,	kIx,
prvního	první	k4xOgNnSc2
oficiálního	oficiální	k2eAgNnSc2d1
fotbalového	fotbalový	k2eAgNnSc2d1
sdružení	sdružení	k1gNnSc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
26	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1863	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
několika	několik	k4yIc6
setkáních	setkání	k1gNnPc6
vytvořila	vytvořit	k5eAaPmAgFnS
asociace	asociace	k1gFnSc1
sadu	sad	k1gInSc2
pravidel	pravidlo	k1gNnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
nakonec	nakonec	k6eAd1
vypustila	vypustit	k5eAaPmAgFnS
pravidla	pravidlo	k1gNnPc4
dovolující	dovolující	k2eAgInSc4d1
běh	běh	k1gInSc4
s	s	k7c7
míčem	míč	k1gInSc7
v	v	k7c6
ruce	ruka	k1gFnSc6
a	a	k8xC
držení	držení	k1gNnSc6
a	a	k8xC
podrážení	podrážení	k1gNnSc6
protivníka	protivník	k1gMnSc2
s	s	k7c7
míčem	míč	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
tomu	ten	k3xDgNnSc3
zástupce	zástupce	k1gMnSc1
Blackheathu	Blackheatha	k1gFnSc4
asociaci	asociace	k1gFnSc4
opustil	opustit	k5eAaPmAgMnS
<g/>
,	,	kIx,
a	a	k8xC
spolu	spolu	k6eAd1
s	s	k7c7
několika	několik	k4yIc7
dalšími	další	k2eAgInPc7d1
kluby	klub	k1gInPc7
v	v	k7c6
roce	rok	k1gInSc6
1871	#num#	k4
založil	založit	k5eAaPmAgMnS
Rugby	rugby	k1gNnSc4
Football	Football	k1gInSc1
Union	union	k1gInSc4
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
vzniklo	vzniknout	k5eAaPmAgNnS
ragby	ragby	k1gNnSc1
jako	jako	k8xC,k8xS
sport	sport	k1gInSc1
odlišný	odlišný	k2eAgInSc1d1
od	od	k7c2
kopané	kopaná	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
v	v	k7c6
pravidlech	pravidlo	k1gNnPc6
FA	fa	kA
však	však	k9
byla	být	k5eAaImAgFnS
některá	některý	k3yIgNnPc4
ustanovení	ustanovení	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yRgNnPc1,k3yQgNnPc1
dnes	dnes	k6eAd1
zůstávají	zůstávat	k5eAaImIp3nP
jen	jen	k9
ve	v	k7c6
sportech	sport	k1gInPc6
jako	jako	k8xC,k8xS
ragby	ragby	k1gNnSc1
či	či	k8xC
americký	americký	k2eAgInSc1d1
fotbal	fotbal	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
pravidla	pravidlo	k1gNnPc1
také	také	k9
například	například	k6eAd1
ani	ani	k8xC
nestanovovala	stanovovat	k5eNaImAgFnS
počet	počet	k1gInSc4
hráčů	hráč	k1gMnPc2
či	či	k8xC
tvar	tvar	k1gInSc1
míče	míč	k1gInSc2
<g/>
;	;	kIx,
na	na	k7c6
tom	ten	k3xDgNnSc6
se	se	k3xPyFc4
měli	mít	k5eAaImAgMnP
soupeři	soupeř	k1gMnPc1
dohodnout	dohodnout	k5eAaPmF
před	před	k7c7
každým	každý	k3xTgInSc7
zápasem	zápas	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Popularita	popularita	k1gFnSc1
fotbalu	fotbal	k1gInSc2
(	(	kIx(
<g/>
tmavší	tmavý	k2eAgFnSc1d2
barva	barva	k1gFnSc1
označuje	označovat	k5eAaImIp3nS
vyšší	vysoký	k2eAgFnSc4d2
popularitu	popularita	k1gFnSc4
<g/>
,	,	kIx,
zeleně	zeleně	k6eAd1
jsou	být	k5eAaImIp3nP
vyznačeny	vyznačen	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yRgFnPc6,k3yQgFnPc6,k3yIgFnPc6
je	být	k5eAaImIp3nS
fotbal	fotbal	k1gInSc1
nejpopulárnějším	populární	k2eAgInSc7d3
sportem	sport	k1gInSc7
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
Anglii	Anglie	k1gFnSc6
se	se	k3xPyFc4
postupně	postupně	k6eAd1
vyvinulo	vyvinout	k5eAaPmAgNnS
několik	několik	k4yIc1
soutěží	soutěž	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
přispívaly	přispívat	k5eAaImAgFnP
k	k	k7c3
šíření	šíření	k1gNnSc3
jednotných	jednotný	k2eAgNnPc2d1
pravidel	pravidlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc4
mezinárodní	mezinárodní	k2eAgInSc4d1
zápas	zápas	k1gInSc4
proběhl	proběhnout	k5eAaPmAgMnS
mezi	mezi	k7c7
Anglií	Anglie	k1gFnSc7
a	a	k8xC
Skotskem	Skotsko	k1gNnSc7
30	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1872	#num#	k4
a	a	k8xC
skončil	skončit	k5eAaPmAgInS
bezbrankovou	bezbrankový	k2eAgFnSc7d1
remízou	remíza	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgNnSc4
mimoevropské	mimoevropský	k2eAgNnSc4d1
mezinárodní	mezinárodní	k2eAgNnSc4d1
fotbalové	fotbalový	k2eAgNnSc4d1
utkání	utkání	k1gNnSc4
se	se	k3xPyFc4
uskutečnilo	uskutečnit	k5eAaPmAgNnS
28	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1885	#num#	k4
v	v	k7c6
Newarku	Newark	k1gInSc6
mezi	mezi	k7c7
Spojenými	spojený	k2eAgInPc7d1
státy	stát	k1gInPc7
a	a	k8xC
Kanadou	Kanada	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
zvítězila	zvítězit	k5eAaPmAgFnS
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Fotbal	fotbal	k1gInSc1
se	se	k3xPyFc4
postupně	postupně	k6eAd1
rozšířil	rozšířit	k5eAaPmAgInS
i	i	k9
na	na	k7c4
kontinent	kontinent	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
nejstarším	starý	k2eAgNnSc7d3
fotbalovým	fotbalový	k2eAgNnSc7d1
mužstvem	mužstvo	k1gNnSc7
patrně	patrně	k6eAd1
švýcarský	švýcarský	k2eAgInSc1d1
Lausanne	Lausanne	k1gNnSc1
Football	Football	k1gMnSc1
and	and	k?
Cricket	Cricket	k1gInSc1
Club	club	k1gInSc1
založený	založený	k2eAgInSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1860	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
anglickým	anglický	k2eAgMnPc3d1
dělníkům	dělník	k1gMnPc3
pracujícím	pracující	k2eAgMnPc3d1
na	na	k7c6
stavbě	stavba	k1gFnSc6
železnice	železnice	k1gFnSc2
se	se	k3xPyFc4
fotbal	fotbal	k1gInSc1
dostal	dostat	k5eAaPmAgInS
až	až	k9
do	do	k7c2
Jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
ale	ale	k8xC
tato	tento	k3xDgFnSc1
teorie	teorie	k1gFnSc1
rozšíření	rozšíření	k1gNnSc2
fotbalu	fotbal	k1gInSc2
na	na	k7c4
jihoamerický	jihoamerický	k2eAgInSc4d1
kontinent	kontinent	k1gInSc4
nebyla	být	k5eNaImAgFnS
nikdy	nikdy	k6eAd1
přesvědčivě	přesvědčivě	k6eAd1
doložena	doložit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s>
Dnes	dnes	k6eAd1
fotbal	fotbal	k1gInSc4
hrají	hrát	k5eAaImIp3nP
profesionální	profesionální	k2eAgMnPc1d1
fotbalisté	fotbalista	k1gMnPc1
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
,	,	kIx,
mnoho	mnoho	k4c1
dalších	další	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
pak	pak	k6eAd1
věnuje	věnovat	k5eAaPmIp3nS,k5eAaImIp3nS
na	na	k7c6
amatérské	amatérský	k2eAgFnSc6d1
či	či	k8xC
rekreační	rekreační	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
průzkumu	průzkum	k1gInSc2
<g/>
,	,	kIx,
uspořádaného	uspořádaný	k2eAgInSc2d1
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
mezinárodní	mezinárodní	k2eAgFnSc7d1
fotbalovou	fotbalový	k2eAgFnSc7d1
federací	federace	k1gFnSc7
FIFA	FIFA	kA
<g/>
,	,	kIx,
hraje	hrát	k5eAaImIp3nS
pravidelně	pravidelně	k6eAd1
fotbal	fotbal	k1gInSc1
nejméně	málo	k6eAd3
240	#num#	k4
miliónů	milión	k4xCgInPc2
lidí	člověk	k1gMnPc2
ve	v	k7c6
více	hodně	k6eAd2
než	než	k8xS
200	#num#	k4
zemích	zem	k1gFnPc6
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
příčinami	příčina	k1gFnPc7
jeho	jeho	k3xOp3gFnSc2
popularity	popularita	k1gFnSc2
jsou	být	k5eAaImIp3nP
bezesporu	bezesporu	k9
jednoduchá	jednoduchý	k2eAgNnPc1d1
pravidla	pravidlo	k1gNnPc1
a	a	k8xC
naprosto	naprosto	k6eAd1
minimální	minimální	k2eAgFnSc4d1
náročnost	náročnost	k1gFnSc4
na	na	k7c4
vybavení	vybavení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Oficiální	oficiální	k2eAgNnPc1d1
pravidla	pravidlo	k1gNnPc1
</s>
<s>
Oficiální	oficiální	k2eAgNnPc1d1
pravidla	pravidlo	k1gNnPc1
fotbalu	fotbal	k1gInSc2
(	(	kIx(
<g/>
Laws	Laws	k1gInSc1
of	of	k?
the	the	k?
Game	game	k1gInSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
ve	v	k7c6
všech	všecek	k3xTgNnPc6
soutěžních	soutěžní	k2eAgNnPc6d1
utkáních	utkání	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
rozdělena	rozdělit	k5eAaPmNgNnP
do	do	k7c2
sedmnácti	sedmnáct	k4xCc2
částí	část	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrací	hrací	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
</s>
<s>
Hrací	hrací	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
</s>
<s>
Fotbal	fotbal	k1gInSc1
se	se	k3xPyFc4
hraje	hrát	k5eAaImIp3nS
na	na	k7c6
hrací	hrací	k2eAgFnSc6d1
ploše	plocha	k1gFnSc6
tvaru	tvar	k1gInSc2
obdélníku	obdélník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Délka	délka	k1gFnSc1
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
90	#num#	k4
<g/>
–	–	k?
<g/>
120	#num#	k4
m	m	kA
<g/>
,	,	kIx,
šířka	šířka	k1gFnSc1
45	#num#	k4
<g/>
–	–	k?
<g/>
90	#num#	k4
m	m	kA
(	(	kIx(
<g/>
pro	pro	k7c4
mezinárodní	mezinárodní	k2eAgNnSc4d1
utkání	utkání	k1gNnSc4
100	#num#	k4
<g/>
–	–	k?
<g/>
110	#num#	k4
×	×	k?
64	#num#	k4
<g/>
–	–	k?
<g/>
75	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrací	hrací	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
je	být	k5eAaImIp3nS
vyznačena	vyznačen	k2eAgFnSc1d1
(	(	kIx(
<g/>
obvykle	obvykle	k6eAd1
bílými	bílý	k2eAgFnPc7d1
<g/>
)	)	kIx)
čárami	čára	k1gFnPc7
(	(	kIx(
<g/>
max	max	kA
<g/>
.	.	kIx.
šířka	šířka	k1gFnSc1
čáry	čára	k1gFnSc2
12	#num#	k4
cm	cm	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
pomezní	pomezní	k2eAgFnPc1d1
čáry	čára	k1gFnPc1
vymezují	vymezovat	k5eAaImIp3nP
delší	dlouhý	k2eAgFnSc4d2
hranu	hrana	k1gFnSc4
hrací	hrací	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
<g/>
,	,	kIx,
kratší	krátký	k2eAgInPc1d2
se	se	k3xPyFc4
nazývají	nazývat	k5eAaImIp3nP
brankové	brankový	k2eAgFnPc4d1
čáry	čára	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrací	hrací	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
je	být	k5eAaImIp3nS
rozdělena	rozdělit	k5eAaPmNgFnS
na	na	k7c4
poloviny	polovina	k1gFnSc2
středovou	středový	k2eAgFnSc7d1
čárou	čára	k1gFnSc7
<g/>
,	,	kIx,
v	v	k7c6
jejímž	jejíž	k3xOyRp3gInSc6
středu	střed	k1gInSc6
je	být	k5eAaImIp3nS
vyznačena	vyznačen	k2eAgFnSc1d1
středová	středový	k2eAgFnSc1d1
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
kolem	kolem	k7c2
které	který	k3yQgFnSc2,k3yIgFnSc2,k3yRgFnSc2
je	být	k5eAaImIp3nS
vyznačen	vyznačen	k2eAgInSc1d1
středový	středový	k2eAgInSc1d1
kruh	kruh	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jsou	být	k5eAaImIp3nP
na	na	k7c6
každé	každý	k3xTgFnSc6
polovině	polovina	k1gFnSc6
hrací	hrací	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
u	u	k7c2
příslušné	příslušný	k2eAgFnSc2d1
branky	branka	k1gFnSc2
vyznačena	vyznačen	k2eAgNnPc4d1
pokutová	pokutový	k2eAgNnPc4d1
území	území	k1gNnPc4
a	a	k8xC
menší	malý	k2eAgFnPc4d2
branková	brankový	k2eAgNnPc4d1
území	území	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvnitř	uvnitř	k7c2
každého	každý	k3xTgNnSc2
pokutového	pokutový	k2eAgNnSc2d1
území	území	k1gNnSc2
je	být	k5eAaImIp3nS
vyznačena	vyznačen	k2eAgFnSc1d1
pokutová	pokutový	k2eAgFnSc1d1
značka	značka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
je	být	k5eAaImIp3nS
vně	vně	k7c2
pokutového	pokutový	k2eAgNnSc2d1
území	území	k1gNnSc2
vyznačena	vyznačen	k2eAgFnSc1d1
část	část	k1gFnSc1
kruhového	kruhový	k2eAgInSc2d1
oblouku	oblouk	k1gInSc2
se	s	k7c7
středem	střed	k1gInSc7
v	v	k7c6
pokutové	pokutový	k2eAgFnSc6d1
značce	značka	k1gFnSc6
a	a	k8xC
poloměrem	poloměr	k1gInSc7
9,15	9,15	k4
m	m	kA
(	(	kIx(
<g/>
10	#num#	k4
yardů	yard	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
každém	každý	k3xTgInSc6
rohu	roh	k1gInSc6
hrací	hrací	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
je	být	k5eAaImIp3nS
na	na	k7c6
ohebné	ohebný	k2eAgFnSc6d1
tyči	tyč	k1gFnSc6
o	o	k7c6
výšce	výška	k1gFnSc6
nejméně	málo	k6eAd3
1,5	1,5	k4
m	m	kA
umístěn	umístěn	k2eAgInSc4d1
praporek	praporek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejné	stejný	k2eAgInPc1d1
praporky	praporek	k1gInPc1
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
umísťují	umísťovat	k5eAaImIp3nP
i	i	k9
uprostřed	uprostřed	k7c2
obou	dva	k4xCgFnPc2
pomezních	pomezní	k2eAgFnPc2d1
čar	čára	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
každého	každý	k3xTgInSc2
rohového	rohový	k2eAgInSc2d1
praporku	praporek	k1gInSc2
se	se	k3xPyFc4
na	na	k7c6
hrací	hrací	k2eAgFnSc6d1
ploše	plocha	k1gFnSc6
vyznačí	vyznačit	k5eAaPmIp3nS
čtvrtkruh	čtvrtkruh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
brankových	brankový	k2eAgFnPc6d1
čárách	čára	k1gFnPc6
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
také	také	k6eAd1
krátkými	krátký	k2eAgFnPc7d1
značkami	značka	k1gFnPc7
vyznačí	vyznačit	k5eAaPmIp3nS
vzdálenost	vzdálenost	k1gFnSc4
9,15	9,15	k4
m	m	kA
od	od	k7c2
rohového	rohový	k2eAgInSc2d1
praporku	praporek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Uprostřed	uprostřed	k7c2
obou	dva	k4xCgFnPc2
brankových	brankový	k2eAgFnPc2d1
čar	čára	k1gFnPc2
jsou	být	k5eAaImIp3nP
umístěny	umístěn	k2eAgFnPc1d1
branky	branka	k1gFnPc1
<g/>
,	,	kIx,
tvořené	tvořený	k2eAgFnPc1d1
dvěma	dva	k4xCgFnPc7
svislými	svislý	k2eAgFnPc7d1
brankovými	brankový	k2eAgFnPc7d1
tyčemi	tyč	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
jsou	být	k5eAaImIp3nP
nahoře	nahoře	k6eAd1
spojeny	spojit	k5eAaPmNgInP
břevnem	břevno	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc1
tyče	tyč	k1gFnPc1
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
bílé	bílý	k2eAgFnPc1d1
a	a	k8xC
mají	mít	k5eAaImIp3nP
stejnou	stejný	k2eAgFnSc4d1
šířku	šířka	k1gFnSc4
(	(	kIx(
<g/>
max	max	kA
<g/>
.	.	kIx.
12	#num#	k4
cm	cm	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
stejná	stejný	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
šířka	šířka	k1gFnSc1
brankové	brankový	k2eAgFnSc2d1
čáry	čára	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vnitřní	vnitřní	k2eAgInPc1d1
rozměry	rozměr	k1gInPc1
branky	branka	k1gFnSc2
činí	činit	k5eAaImIp3nS
7,32	7,32	k4
<g/>
×	×	k?
<g/>
2,44	2,44	k4
m	m	kA
(	(	kIx(
<g/>
8	#num#	k4
yardů	yard	k1gInPc2
<g/>
×	×	k?
<g/>
8	#num#	k4
stop	stopa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
brankové	brankový	k2eAgFnSc6d1
konstrukci	konstrukce	k1gFnSc6
je	být	k5eAaImIp3nS
připevněna	připevněn	k2eAgFnSc1d1
síť	síť	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
upevněna	upevnit	k5eAaPmNgFnS
k	k	k7c3
tyčím	tyčit	k5eAaImIp1nS
<g/>
,	,	kIx,
břevnům	břevno	k1gNnPc3
a	a	k8xC
k	k	k7c3
zemi	zem	k1gFnSc3
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
míč	míč	k1gInSc1
nemohl	moct	k5eNaImAgInS
projít	projít	k5eAaPmF
skrz	skrz	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Síť	síť	k1gFnSc1
je	být	k5eAaImIp3nS
vypnuta	vypnout	k5eAaPmNgFnS
dozadu	dozadu	k6eAd1
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
neomezovala	omezovat	k5eNaImAgFnS
v	v	k7c6
pohybu	pohyb	k1gInSc6
brankáře	brankář	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Míč	míč	k1gInSc1
</s>
<s>
Fotbalový	fotbalový	k2eAgInSc1d1
míč	míč	k1gInSc1
</s>
<s>
Hraje	hrát	k5eAaImIp3nS
se	se	k3xPyFc4
kulatým	kulatý	k2eAgInSc7d1
míčem	míč	k1gInSc7
o	o	k7c6
obvodu	obvod	k1gInSc6
mezi	mezi	k7c7
68	#num#	k4
a	a	k8xC
70	#num#	k4
cm	cm	kA
(	(	kIx(
<g/>
průměr	průměr	k1gInSc1
cca	cca	kA
22	#num#	k4
cm	cm	kA
<g/>
)	)	kIx)
a	a	k8xC
hmotnosti	hmotnost	k1gFnSc2
mezi	mezi	k7c7
410	#num#	k4
a	a	k8xC
450	#num#	k4
g	g	kA
(	(	kIx(
<g/>
na	na	k7c6
začátku	začátek	k1gInSc6
utkání	utkání	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
zápasech	zápas	k1gInPc6
na	na	k7c6
profesionální	profesionální	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
je	být	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
připraveno	připravit	k5eAaPmNgNnS
více	hodně	k6eAd2
míčů	míč	k1gInPc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
se	se	k3xPyFc4
zamezuje	zamezovat	k5eAaImIp3nS
zdržování	zdržování	k1gNnSc3
hry	hra	k1gFnSc2
způsobeném	způsobený	k2eAgInSc6d1
např.	např.	kA
hledáním	hledání	k1gNnSc7
zakopnutého	zakopnutý	k2eAgInSc2d1
míče	míč	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každoročně	každoročně	k6eAd1
se	se	k3xPyFc4
celosvětově	celosvětově	k6eAd1
prodá	prodat	k5eAaPmIp3nS
více	hodně	k6eAd2
než	než	k8xS
40	#num#	k4
milionů	milion	k4xCgInPc2
fotbalových	fotbalový	k2eAgInPc2d1
míčů	míč	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráči	hráč	k1gMnPc1
</s>
<s>
Utkání	utkání	k1gNnPc1
hrají	hrát	k5eAaImIp3nP
dvě	dva	k4xCgNnPc1
družstva	družstvo	k1gNnPc1
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
každé	každý	k3xTgNnSc1
má	mít	k5eAaImIp3nS
nejvýše	nejvýše	k6eAd1,k6eAd3
jedenáct	jedenáct	k4xCc1
hráčů	hráč	k1gMnPc2
<g/>
,	,	kIx,
ze	z	k7c2
kterých	který	k3yIgInPc2,k3yQgInPc2,k3yRgInPc2
je	být	k5eAaImIp3nS
jeden	jeden	k4xCgMnSc1
hráč	hráč	k1gMnSc1
označen	označit	k5eAaPmNgMnS
jako	jako	k9
brankář	brankář	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
zápasu	zápas	k1gInSc3
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
nominovat	nominovat	k5eAaBmF
také	také	k9
náhradníky	náhradník	k1gMnPc4
(	(	kIx(
<g/>
v	v	k7c6
soutěžních	soutěžní	k2eAgNnPc6d1
mezinárodních	mezinárodní	k2eAgNnPc6d1
utkáních	utkání	k1gNnPc6
nejvýše	vysoce	k6eAd3,k6eAd1
šest	šest	k4xCc4
<g/>
,	,	kIx,
v	v	k7c6
jiných	jiný	k2eAgInPc6d1
případech	případ	k1gInPc6
závisí	záviset	k5eAaImIp3nS
na	na	k7c6
pravidlech	pravidlo	k1gNnPc6
konkrétní	konkrétní	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
či	či	k8xC
předzápasové	předzápasový	k2eAgFnSc3d1
dohodě	dohoda	k1gFnSc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
do	do	k7c2
hry	hra	k1gFnSc2
nasazeni	nasadit	k5eAaPmNgMnP
nejvýše	vysoce	k6eAd3,k6eAd1
tři	tři	k4xCgMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Střídání	střídání	k1gNnSc1
hráčů	hráč	k1gMnPc2
probíhá	probíhat	k5eAaImIp3nS
pouze	pouze	k6eAd1
v	v	k7c6
přerušené	přerušený	k2eAgFnSc6d1
hře	hra	k1gFnSc6
se	s	k7c7
souhlasem	souhlas	k1gInSc7
rozhodčího	rozhodčí	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednou	jednou	k6eAd1
vystřídaný	vystřídaný	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
se	se	k3xPyFc4
v	v	k7c6
témže	týž	k3xTgNnSc6
utkání	utkání	k1gNnSc6
už	už	k9
nesmí	smět	k5eNaImIp3nS
zúčastnit	zúčastnit	k5eAaPmF
hry	hra	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
libovolném	libovolný	k2eAgNnSc6d1
přerušení	přerušení	k1gNnSc6
hry	hra	k1gFnSc2
si	se	k3xPyFc3
může	moct	k5eAaImIp3nS
libovolný	libovolný	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
na	na	k7c6
hřišti	hřiště	k1gNnSc6
se	s	k7c7
souhlasem	souhlas	k1gInSc7
rozhodčího	rozhodčí	k1gMnSc2
vyměnit	vyměnit	k5eAaPmF
místo	místo	k1gNnSc4
s	s	k7c7
brankářem	brankář	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
v	v	k7c6
průběhu	průběh	k1gInSc6
hry	hra	k1gFnSc2
klesne	klesnout	k5eAaPmIp3nS
z	z	k7c2
libovolných	libovolný	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
(	(	kIx(
<g/>
vyloučení	vyloučení	k1gNnSc1
<g/>
,	,	kIx,
zranění	zranění	k1gNnSc1
apod.	apod.	kA
<g/>
)	)	kIx)
počet	počet	k1gInSc1
hráčů	hráč	k1gMnPc2
v	v	k7c6
jednom	jeden	k4xCgNnSc6
mužstvu	mužstvo	k1gNnSc6
pod	pod	k7c4
sedm	sedm	k4xCc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
utkání	utkání	k1gNnSc1
předčasně	předčasně	k6eAd1
ukončeno	ukončit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výstroj	výstroj	k1gInSc4
hráčů	hráč	k1gMnPc2
</s>
<s>
Hráči	hráč	k1gMnPc1
nesmějí	smát	k5eNaImIp3nP
nosit	nosit	k5eAaImF
žádnou	žádný	k3yNgFnSc4
nebezpečnou	bezpečný	k2eNgFnSc4d1
výstroj	výstroj	k1gFnSc4
<g/>
,	,	kIx,
výslovně	výslovně	k6eAd1
je	být	k5eAaImIp3nS
zakázáno	zakázán	k2eAgNnSc4d1
nošení	nošení	k1gNnSc4
šperků	šperk	k1gInPc2
<g/>
,	,	kIx,
náramků	náramek	k1gInPc2
<g/>
,	,	kIx,
náušnic	náušnice	k1gFnPc2
apod.	apod.	kA
Všichni	všechen	k3xTgMnPc1
hráči	hráč	k1gMnPc1
jsou	být	k5eAaImIp3nP
povinni	povinen	k2eAgMnPc1d1
mít	mít	k5eAaImF
následující	následující	k2eAgFnSc4d1
výstroj	výstroj	k1gFnSc4
<g/>
:	:	kIx,
</s>
<s>
dres	dres	k1gInSc4
s	s	k7c7
rukávy	rukáv	k1gInPc7
–	–	k?
barvy	barva	k1gFnPc4
dresů	dres	k1gInPc2
obou	dva	k4xCgInPc2
týmů	tým	k1gInPc2
se	se	k3xPyFc4
musí	muset	k5eAaImIp3nS
dostatečně	dostatečně	k6eAd1
odlišovat	odlišovat	k5eAaImF
navzájem	navzájem	k6eAd1
i	i	k9
od	od	k7c2
dresů	dres	k1gInPc2
rozhodčích	rozhodčí	k1gMnPc2
<g/>
;	;	kIx,
většina	většina	k1gFnSc1
týmů	tým	k1gInPc2
má	mít	k5eAaImIp3nS
proto	proto	k8xC
alespoň	alespoň	k9
dvě	dva	k4xCgFnPc1
sady	sada	k1gFnPc1
dresů	dres	k1gInPc2
odlišné	odlišný	k2eAgFnPc1d1
barvy	barva	k1gFnPc1
<g/>
;	;	kIx,
brankáři	brankář	k1gMnPc1
mají	mít	k5eAaImIp3nP
barvu	barva	k1gFnSc4
dresů	dres	k1gInPc2
odlišnou	odlišný	k2eAgFnSc4d1
od	od	k7c2
ostatních	ostatní	k2eAgMnPc2d1
hráčů	hráč	k1gMnPc2
<g/>
,	,	kIx,
rozhodčího	rozhodčí	k1gMnSc2
i	i	k8xC
jeho	jeho	k3xOp3gMnPc2
asistentů	asistent	k1gMnPc2
<g/>
;	;	kIx,
</s>
<s>
trenýrky	trenýrky	k1gFnPc1
(	(	kIx(
<g/>
brankáři	brankář	k1gMnPc1
mohou	moct	k5eAaImIp3nP
nosit	nosit	k5eAaImF
tepláky	tepláky	k1gInPc4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
</s>
<s>
stulpny	stulpna	k1gFnPc1
(	(	kIx(
<g/>
podkolenky	podkolenka	k1gFnPc1
<g/>
)	)	kIx)
–	–	k?
zakrývají	zakrývat	k5eAaImIp3nP
chrániče	chránič	k1gInPc4
<g/>
;	;	kIx,
</s>
<s>
chrániče	chránič	k1gInPc1
holení	holení	k1gNnSc2
<g/>
;	;	kIx,
</s>
<s>
obuv	obuv	k1gFnSc1
–	–	k?
zpravidla	zpravidla	k6eAd1
speciální	speciální	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
obuv	obuv	k1gFnSc1
<g/>
,	,	kIx,
tzv.	tzv.	kA
kopačky	kopačka	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
mají	mít	k5eAaImIp3nP
na	na	k7c6
podrážce	podrážka	k1gFnSc6
kolíky	kolík	k1gInPc1
nebo	nebo	k8xC
špunty	špunty	k?
bránící	bránící	k2eAgNnSc1d1
podklouznutí	podklouznutí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodčí	rozhodčí	k1gMnSc1
</s>
<s>
Každé	každý	k3xTgNnSc1
utkání	utkání	k1gNnSc1
řídí	řídit	k5eAaImIp3nS
rozhodčí	rozhodčí	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
má	mít	k5eAaImIp3nS
na	na	k7c6
hřišti	hřiště	k1gNnSc6
neomezenou	omezený	k2eNgFnSc4d1
pravomoc	pravomoc	k1gFnSc4
k	k	k7c3
uplatňování	uplatňování	k1gNnSc3
pravidel	pravidlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
vybaven	vybavit	k5eAaPmNgInS
píšťalkou	píšťalka	k1gFnSc7
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
může	moct	k5eAaImIp3nS
přerušit	přerušit	k5eAaPmF
hru	hra	k1gFnSc4
v	v	k7c6
případě	případ	k1gInSc6
porušení	porušení	k1gNnSc2
pravidel	pravidlo	k1gNnPc2
nebo	nebo	k8xC
padnutí	padnutí	k1gNnSc2
branky	branka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
má	mít	k5eAaImIp3nS
u	u	k7c2
sebe	sebe	k3xPyFc4
dvě	dva	k4xCgFnPc1
kartičky	kartička	k1gFnPc1
žluté	žlutý	k2eAgFnPc1d1
a	a	k8xC
červené	červený	k2eAgFnPc1d1
barvy	barva	k1gFnPc1
<g/>
,	,	kIx,
kterými	který	k3yRgFnPc7,k3yQgFnPc7,k3yIgFnPc7
signalizuje	signalizovat	k5eAaImIp3nS
potrestání	potrestání	k1gNnSc2
hráče	hráč	k1gMnPc4
<g/>
,	,	kIx,
hodinky	hodinka	k1gFnPc4
pro	pro	k7c4
měření	měření	k1gNnSc4
času	čas	k1gInSc2
utkání	utkání	k1gNnPc2
a	a	k8xC
nějaký	nějaký	k3yIgInSc4
prostředek	prostředek	k1gInSc4
pro	pro	k7c4
uchovávání	uchovávání	k1gNnSc4
záznamů	záznam	k1gInPc2
o	o	k7c6
utkání	utkání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatní	ostatní	k2eAgMnPc1d1
rozhodčí	rozhodčí	k1gMnPc1
</s>
<s>
Asistent	asistent	k1gMnSc1
rozhodčího	rozhodčí	k1gMnSc2
signalizuje	signalizovat	k5eAaImIp3nS
praporkem	praporek	k1gInSc7
ofsajd	ofsajd	k1gInSc1
</s>
<s>
Každého	každý	k3xTgNnSc2
utkání	utkání	k1gNnSc2
se	se	k3xPyFc4
účastní	účastnit	k5eAaImIp3nP
dva	dva	k4xCgMnPc1
asistenti	asistent	k1gMnPc1
rozhodčího	rozhodčí	k1gMnSc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
pohybují	pohybovat	k5eAaImIp3nP
podél	podél	k7c2
pomezních	pomezní	k2eAgFnPc2d1
čar	čára	k1gFnPc2
těsně	těsně	k6eAd1
za	za	k7c7
okrajem	okraj	k1gInSc7
hřiště	hřiště	k1gNnSc2
<g/>
;	;	kIx,
jejich	jejich	k3xOp3gInSc7
úkolem	úkol	k1gInSc7
je	být	k5eAaImIp3nS
pomocí	pomocí	k7c2
praporku	praporek	k1gInSc2
signalizovat	signalizovat	k5eAaImF
rozhodčímu	rozhodčí	k1gMnSc3
<g/>
:	:	kIx,
</s>
<s>
když	když	k8xS
míč	míč	k1gInSc1
opustí	opustit	k5eAaPmIp3nS
hrací	hrací	k2eAgFnSc4d1
plochu	plocha	k1gFnSc4
a	a	k8xC
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
družstvo	družstvo	k1gNnSc1
má	mít	k5eAaImIp3nS
provádět	provádět	k5eAaImF
kop	kop	k1gInSc4
z	z	k7c2
rohu	roh	k1gInSc2
<g/>
,	,	kIx,
kop	kop	k1gInSc4
od	od	k7c2
branky	branka	k1gFnSc2
nebo	nebo	k8xC
vhazování	vhazování	k1gNnSc2
<g/>
,	,	kIx,
</s>
<s>
kdy	kdy	k6eAd1
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
hráč	hráč	k1gMnSc1
potrestán	potrestat	k5eAaPmNgMnS
za	za	k7c4
ofsajd	ofsajd	k1gInSc4
<g/>
,	,	kIx,
</s>
<s>
když	když	k8xS
je	být	k5eAaImIp3nS
nějaké	nějaký	k3yIgNnSc1
mužstvo	mužstvo	k1gNnSc1
připraveno	připravit	k5eAaPmNgNnS
střídat	střídat	k5eAaImF
<g/>
,	,	kIx,
</s>
<s>
při	při	k7c6
pokutovém	pokutový	k2eAgInSc6d1
kopu	kop	k1gInSc6
přestupky	přestupek	k1gInPc4
brankáře	brankář	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Mohou	moct	k5eAaImIp3nP
také	také	k6eAd1
rozhodčímu	rozhodčí	k1gMnSc3
pomáhat	pomáhat	k5eAaImF
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
mají	mít	k5eAaImIp3nP
o	o	k7c6
nějakém	nějaký	k3yIgInSc6
přestupku	přestupek	k1gInSc6
lepší	dobrý	k2eAgInSc4d2
přehled	přehled	k1gInSc4
než	než	k8xS
rozhodčí	rozhodčí	k1gMnPc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
v	v	k7c6
případě	případ	k1gInSc6
nesportovního	sportovní	k2eNgNnSc2d1
chování	chování	k1gNnSc2
či	či	k8xC
jiných	jiný	k2eAgFnPc2d1
událostí	událost	k1gFnPc2
<g/>
,	,	kIx,
ke	k	k7c3
kterým	který	k3yRgFnPc3,k3yQgFnPc3,k3yIgFnPc3
došlo	dojít	k5eAaPmAgNnS
mimo	mimo	k7c4
zorné	zorný	k2eAgNnSc4d1
pole	pole	k1gNnSc4
rozhodčího	rozhodčí	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
některých	některý	k3yIgFnPc6
soutěžích	soutěž	k1gFnPc6
pak	pak	k6eAd1
působí	působit	k5eAaImIp3nP
i	i	k9
další	další	k2eAgMnPc1d1
rozhodčí	rozhodčí	k1gMnPc1
<g/>
:	:	kIx,
Čtvrtý	čtvrtý	k4xOgMnSc1
rozhodčí	rozhodčí	k1gMnSc1
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
mezi	mezi	k7c7
lavičkami	lavička	k1gFnPc7
týmů	tým	k1gInPc2
<g/>
,	,	kIx,
řídí	řídit	k5eAaImIp3nS
proceduru	procedura	k1gFnSc4
střídání	střídání	k1gNnSc2
<g/>
,	,	kIx,
oznamuje	oznamovat	k5eAaImIp3nS
dobu	doba	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
rozhodčí	rozhodčí	k1gMnPc1
nastaví	nastavět	k5eAaBmIp3nP,k5eAaPmIp3nP
nad	nad	k7c4
rámec	rámec	k1gInSc4
základní	základní	k2eAgFnSc2d1
hrací	hrací	k2eAgFnSc2d1
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
dohlíží	dohlížet	k5eAaImIp3nS
na	na	k7c4
chování	chování	k1gNnSc4
osob	osoba	k1gFnPc2
v	v	k7c6
technické	technický	k2eAgFnSc6d1
zóně	zóna	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brankoví	brankový	k2eAgMnPc1d1
rozhodčí	rozhodčí	k1gMnPc1
mají	mít	k5eAaImIp3nP
stojí	stát	k5eAaImIp3nS
vedle	vedle	k7c2
obou	dva	k4xCgFnPc2
branek	branka	k1gFnPc2
a	a	k8xC
signalizují	signalizovat	k5eAaImIp3nP
rozhodčímu	rozhodčí	k1gMnSc3
<g/>
,	,	kIx,
zda	zda	k8xS
míč	míč	k1gInSc1
přešel	přejít	k5eAaPmAgInS
celým	celý	k2eAgInSc7d1
objemem	objem	k1gInSc7
brankovou	brankový	k2eAgFnSc4d1
čáru	čára	k1gFnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
družstvo	družstvo	k1gNnSc1
má	mít	k5eAaImIp3nS
provádět	provádět	k5eAaImF
kop	kop	k1gInSc4
z	z	k7c2
roku	rok	k1gInSc2
nebo	nebo	k8xC
od	od	k7c2
branky	branka	k1gFnSc2
a	a	k8xC
při	při	k7c6
pokutových	pokutový	k2eAgInPc6d1
kopech	kop	k1gInPc6
přestupky	přestupek	k1gInPc4
brankáře	brankář	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
utkáních	utkání	k1gNnPc6
některých	některý	k3yIgFnPc2
vrcholných	vrcholný	k2eAgFnPc2d1
soutěží	soutěž	k1gFnPc2
působí	působit	k5eAaImIp3nS
také	také	k9
video	video	k1gNnSc1
asistent	asistent	k1gMnSc1
rozhodčího	rozhodčí	k1gMnSc2
(	(	kIx(
<g/>
a	a	k8xC
případně	případně	k6eAd1
jeho	jeho	k3xOp3gMnSc1
asistent	asistent	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
s	s	k7c7
pomocí	pomoc	k1gFnSc7
televizní	televizní	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
a	a	k8xC
opakovaných	opakovaný	k2eAgInPc2d1
záběrů	záběr	k1gInPc2
může	moct	k5eAaImIp3nS
rozhodčího	rozhodčí	k1gMnSc2
upozornit	upozornit	k5eAaPmF
na	na	k7c4
jeho	jeho	k3xOp3gFnPc4
jasné	jasný	k2eAgFnPc4d1
chyby	chyba	k1gFnPc4
či	či	k8xC
přehlédnutí	přehlédnutí	k1gNnSc4
závažných	závažný	k2eAgFnPc2d1
situací	situace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doba	doba	k1gFnSc1
hry	hra	k1gFnSc2
</s>
<s>
Utkání	utkání	k1gNnSc1
se	se	k3xPyFc4
hraje	hrát	k5eAaImIp3nS
na	na	k7c4
dva	dva	k4xCgInPc4
poločasy	poločas	k1gInPc4
o	o	k7c6
45	#num#	k4
minutách	minuta	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
oběma	dva	k4xCgInPc7
poločasy	poločas	k1gInPc7
je	být	k5eAaImIp3nS
15	#num#	k4
<g/>
minutová	minutový	k2eAgFnSc1d1
přestávka	přestávka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doba	doba	k1gFnSc1
hry	hra	k1gFnSc2
se	se	k3xPyFc4
měří	měřit	k5eAaImIp3nS
včetně	včetně	k7c2
času	čas	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
míč	míč	k1gInSc4
ze	z	k7c2
hry	hra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
dobu	doba	k1gFnSc4
zameškanou	zameškaný	k2eAgFnSc4d1
střídáním	střídání	k1gNnSc7
hráčů	hráč	k1gMnPc2
<g/>
,	,	kIx,
zdržováním	zdržování	k1gNnSc7
hry	hra	k1gFnSc2
apod.	apod.	kA
může	moct	k5eAaImIp3nS
rozhodčí	rozhodčí	k1gMnSc1
každý	každý	k3xTgInSc4
poločas	poločas	k1gInSc4
nastavit	nastavit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poločas	poločas	k1gInSc1
se	se	k3xPyFc4
musí	muset	k5eAaImIp3nS
prodloužit	prodloužit	k5eAaPmF
také	také	k9
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
má	mít	k5eAaImIp3nS
jedno	jeden	k4xCgNnSc1
z	z	k7c2
mužstev	mužstvo	k1gNnPc2
kopat	kopat	k5eAaImF
pokutový	pokutový	k2eAgInSc4d1
kop	kop	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
dětských	dětský	k2eAgFnPc6d1
a	a	k8xC
mládežnických	mládežnický	k2eAgFnPc6d1
kategoriích	kategorie	k1gFnPc6
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
kratší	krátký	k2eAgFnSc1d2
doba	doba	k1gFnSc1
hry	hra	k1gFnSc2
–	–	k?
u	u	k7c2
mladšího	mladý	k2eAgInSc2d2
dorostu	dorost	k1gInSc2
a	a	k8xC
dorostenek	dorostenka	k1gFnPc2
má	mít	k5eAaImIp3nS
každý	každý	k3xTgInSc1
poločas	poločas	k1gInSc1
40	#num#	k4
minut	minuta	k1gFnPc2
(	(	kIx(
<g/>
u	u	k7c2
staršího	starý	k2eAgInSc2d2
dorostu	dorost	k1gInSc2
se	se	k3xPyFc4
již	již	k6eAd1
hraje	hrát	k5eAaImIp3nS
plná	plný	k2eAgFnSc1d1
délka	délka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
u	u	k7c2
starších	starý	k2eAgMnPc2d2
žáků	žák	k1gMnPc2
a	a	k8xC
u	u	k7c2
žákyň	žákyně	k1gFnPc2
35	#num#	k4
minut	minuta	k1gFnPc2
<g/>
,	,	kIx,
mladší	mladý	k2eAgMnPc1d2
žáci	žák	k1gMnPc1
hrají	hrát	k5eAaImIp3nP
2	#num#	k4
<g/>
×	×	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
35	#num#	k4
nebo	nebo	k8xC
2	#num#	k4
<g/>
×	×	k?
<g/>
30	#num#	k4
minut	minuta	k1gFnPc2
<g/>
,	,	kIx,
starší	starý	k2eAgInPc1d2
přípravky	přípravek	k1gInPc1
hrají	hrát	k5eAaImIp3nP
3	#num#	k4
<g/>
×	×	k?
<g/>
16	#num#	k4
<g/>
–	–	k?
<g/>
20	#num#	k4
nebo	nebo	k8xC
2	#num#	k4
<g/>
×	×	k?
<g/>
25	#num#	k4
minut	minuta	k1gFnPc2
<g/>
,	,	kIx,
mladší	mladý	k2eAgMnSc1d2
pak	pak	k6eAd1
3	#num#	k4
<g/>
×	×	k?
<g/>
12	#num#	k4
<g/>
–	–	k?
<g/>
16	#num#	k4
nebo	nebo	k8xC
2	#num#	k4
<g/>
×	×	k?
<g/>
20	#num#	k4
minut	minuta	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahájení	zahájení	k1gNnSc1
a	a	k8xC
navazování	navazování	k1gNnSc1
hry	hra	k1gFnSc2
</s>
<s>
Výkop	výkop	k1gInSc1
na	na	k7c6
začátku	začátek	k1gInSc6
utkání	utkání	k1gNnSc2
</s>
<s>
Před	před	k7c7
začátkem	začátek	k1gInSc7
utkání	utkání	k1gNnPc2
rozhodčí	rozhodčí	k1gMnSc1
losuje	losovat	k5eAaImIp3nS
mincí	mince	k1gFnSc7
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
vítězný	vítězný	k2eAgMnSc1d1
kapitán	kapitán	k1gMnSc1
si	se	k3xPyFc3
může	moct	k5eAaImIp3nS
zvolit	zvolit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
chce	chtít	k5eAaImIp3nS
zahajovat	zahajovat	k5eAaImF
hru	hra	k1gFnSc4
výkopem	výkop	k1gInSc7
<g/>
,	,	kIx,
případně	případně	k6eAd1
si	se	k3xPyFc3
zvolit	zvolit	k5eAaPmF
polovinu	polovina	k1gFnSc4
hrací	hrací	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
<g/>
;	;	kIx,
pokud	pokud	k8xS
si	se	k3xPyFc3
zvolil	zvolit	k5eAaPmAgMnS
provádět	provádět	k5eAaImF
výkop	výkop	k1gInSc4
<g/>
,	,	kIx,
vybere	vybrat	k5eAaPmIp3nS
si	se	k3xPyFc3
druhé	druhý	k4xOgNnSc4
družstvo	družstvo	k1gNnSc4
<g/>
,	,	kIx,
na	na	k7c4
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
branku	branka	k1gFnSc4
chce	chtít	k5eAaImIp3nS
v	v	k7c6
prvním	první	k4xOgInSc6
poločase	poločas	k1gInSc6
útočit	útočit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
poločasové	poločasový	k2eAgFnSc6d1
přestávce	přestávka	k1gFnSc6
si	se	k3xPyFc3
družstva	družstvo	k1gNnPc1
poloviny	polovina	k1gFnSc2
hrací	hrací	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
vymění	vyměnit	k5eAaPmIp3nS
a	a	k8xC
výkop	výkop	k1gInSc1
provádí	provádět	k5eAaImIp3nS
to	ten	k3xDgNnSc4
družstvo	družstvo	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc4,k3yRgNnSc4,k3yIgNnSc4
ho	on	k3xPp3gNnSc4
v	v	k7c6
prvním	první	k4xOgInSc6
poločase	poločas	k1gInSc6
neprovádělo	provádět	k5eNaImAgNnS
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
začátku	začátek	k1gInSc6
každého	každý	k3xTgInSc2
poločasu	poločas	k1gInSc2
a	a	k8xC
po	po	k7c6
dosažení	dosažení	k1gNnSc6
každé	každý	k3xTgFnSc2
branky	branka	k1gFnSc2
se	se	k3xPyFc4
hra	hra	k1gFnSc1
zahajuje	zahajovat	k5eAaImIp3nS
či	či	k8xC
navazuje	navazovat	k5eAaImIp3nS
pomocí	pomocí	k7c2
výkopu	výkop	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
ním	on	k3xPp3gNnSc7
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
všichni	všechen	k3xTgMnPc1
hráči	hráč	k1gMnPc1
na	na	k7c6
své	svůj	k3xOyFgFnSc6
polovině	polovina	k1gFnSc6
hrací	hrací	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
<g/>
,	,	kIx,
míč	míč	k1gInSc1
je	být	k5eAaImIp3nS
umístěn	umístit	k5eAaPmNgInS
na	na	k7c4
středovou	středový	k2eAgFnSc4d1
značku	značka	k1gFnSc4
a	a	k8xC
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
rozhodčí	rozhodčí	k1gMnSc1
dá	dát	k5eAaPmIp3nS
znamení	znamení	k1gNnSc4
<g/>
,	,	kIx,
rozehraje	rozehrát	k5eAaPmIp3nS
hráč	hráč	k1gMnSc1
příslušného	příslušný	k2eAgNnSc2d1
mužstva	mužstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráči	hráč	k1gMnPc1
soupeřova	soupeřův	k2eAgNnSc2d1
mužstva	mužstvo	k1gNnSc2
musí	muset	k5eAaImIp3nS
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
zůstat	zůstat	k5eAaPmF
mimo	mimo	k7c4
středový	středový	k2eAgInSc4d1
kruh	kruh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
dosažení	dosažení	k1gNnSc6
branky	branka	k1gFnSc2
rozehrává	rozehrávat	k5eAaImIp3nS
výkopem	výkop	k1gInSc7
to	ten	k3xDgNnSc4
mužstvo	mužstvo	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
branku	branka	k1gFnSc4
obdrželo	obdržet	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
byla	být	k5eAaImAgFnS
hra	hra	k1gFnSc1
přerušena	přerušit	k5eAaPmNgFnS
ze	z	k7c2
zvláštních	zvláštní	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
(	(	kIx(
<g/>
např.	např.	kA
zranění	zranění	k1gNnSc4
hráče	hráč	k1gMnSc4
<g/>
,	,	kIx,
vniknutí	vniknutí	k1gNnSc4
diváka	divák	k1gMnSc2
na	na	k7c4
hrací	hrací	k2eAgFnSc4d1
plochu	plocha	k1gFnSc4
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
navazuje	navazovat	k5eAaImIp3nS
se	se	k3xPyFc4
míčem	míč	k1gInSc7
rozhodčího	rozhodčí	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
se	se	k3xPyFc4
provádí	provádět	k5eAaImIp3nS
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
rozhodčí	rozhodčí	k1gMnPc1
z	z	k7c2
ruky	ruka	k1gFnSc2
nechá	nechat	k5eAaPmIp3nS
spadnout	spadnout	k5eAaPmF
míč	míč	k1gInSc4
na	na	k7c4
zem	zem	k1gFnSc4
<g/>
;	;	kIx,
jakmile	jakmile	k8xS
se	se	k3xPyFc4
míč	míč	k1gInSc1
dotkne	dotknout	k5eAaPmIp3nS
země	zem	k1gFnPc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
ve	v	k7c6
hře	hra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Míč	míč	k1gInSc4
ve	v	k7c6
hře	hra	k1gFnSc6
a	a	k8xC
míč	míč	k1gInSc4
ze	z	k7c2
hry	hra	k1gFnSc2
</s>
<s>
Míč	míč	k1gInSc1
je	být	k5eAaImIp3nS
ze	z	k7c2
hry	hra	k1gFnSc2
<g/>
,	,	kIx,
jestliže	jestliže	k8xS
buď	buď	k8xC
přešel	přejít	k5eAaPmAgInS
(	(	kIx(
<g/>
na	na	k7c6
zemi	zem	k1gFnSc6
nebo	nebo	k8xC
ve	v	k7c6
vzduchu	vzduch	k1gInSc6
<g/>
)	)	kIx)
úplně	úplně	k6eAd1
(	(	kIx(
<g/>
celým	celý	k2eAgInSc7d1
objemem	objem	k1gInSc7
<g/>
,	,	kIx,
tzn.	tzn.	kA
úplně	úplně	k6eAd1
celý	celý	k2eAgInSc4d1
míč	míč	k1gInSc4
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
zcela	zcela	k6eAd1
za	za	k7c7
vnějším	vnější	k2eAgInSc7d1
okrajem	okraj	k1gInSc7
příslušné	příslušný	k2eAgFnSc2d1
čáry	čára	k1gFnSc2
<g/>
)	)	kIx)
brankovou	brankový	k2eAgFnSc4d1
nebo	nebo	k8xC
pomezní	pomezní	k2eAgFnSc4d1
čáru	čára	k1gFnSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
rozhodčí	rozhodčí	k1gMnSc1
přeruší	přerušit	k5eAaPmIp3nS
hru	hra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Míč	míč	k1gInSc1
je	být	k5eAaImIp3nS
ze	z	k7c2
hry	hra	k1gFnSc2
také	také	k9
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
dotkne	dotknout	k5eAaPmIp3nS
rozhodčího	rozhodčí	k1gMnSc2
a	a	k8xC
v	v	k7c6
důsledku	důsledek	k1gInSc6
toho	ten	k3xDgNnSc2
dojde	dojít	k5eAaPmIp3nS
ke	k	k7c3
vstřelení	vstřelení	k1gNnSc3
branky	branka	k1gFnSc2
<g/>
,	,	kIx,
založení	založení	k1gNnSc2
útočné	útočný	k2eAgFnSc2d1
akce	akce	k1gFnSc2
nebo	nebo	k8xC
se	se	k3xPyFc4
míče	míč	k1gInSc2
zmocní	zmocnit	k5eAaPmIp3nS
druhé	druhý	k4xOgNnSc1
družstvo	družstvo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
každou	každý	k3xTgFnSc4
jinou	jiný	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
je	být	k5eAaImIp3nS
míč	míč	k1gInSc4
ve	v	k7c6
hře	hra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Určení	určení	k1gNnSc1
výsledku	výsledek	k1gInSc2
utkání	utkání	k1gNnSc2
</s>
<s>
Branky	branka	k1gFnSc2
je	být	k5eAaImIp3nS
dosaženo	dosáhnout	k5eAaPmNgNnS
ve	v	k7c6
chvíli	chvíle	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
míč	míč	k1gInSc4
úplně	úplně	k6eAd1
(	(	kIx(
<g/>
celým	celý	k2eAgInSc7d1
objemem	objem	k1gInSc7
<g/>
)	)	kIx)
přejde	přejít	k5eAaPmIp3nS
brankovou	brankový	k2eAgFnSc4d1
čáru	čára	k1gFnSc4
mezi	mezi	k7c7
brankovými	brankový	k2eAgFnPc7d1
tyčemi	tyč	k1gFnPc7
a	a	k8xC
pod	pod	k7c7
břevnem	břevno	k1gNnSc7
a	a	k8xC
útočící	útočící	k2eAgNnSc4d1
mužstvo	mužstvo	k1gNnSc4
před	před	k7c7
tím	ten	k3xDgNnSc7
neporušilo	porušit	k5eNaPmAgNnS
žádné	žádný	k3yNgNnSc1
pravidlo	pravidlo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Družstvo	družstvo	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
v	v	k7c6
průběhu	průběh	k1gInSc6
utkání	utkání	k1gNnSc2
dosáhlo	dosáhnout	k5eAaPmAgNnS
vyššího	vysoký	k2eAgInSc2d2
počtu	počet	k1gInSc2
branek	branka	k1gFnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
vítězem	vítěz	k1gMnSc7
utkání	utkání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
obě	dva	k4xCgNnPc4
mužstva	mužstvo	k1gNnPc4
dosáhla	dosáhnout	k5eAaPmAgFnS
stejného	stejný	k2eAgInSc2d1
počtu	počet	k1gInSc2
branek	branka	k1gFnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
výsledek	výsledek	k1gInSc1
utkání	utkání	k1gNnPc2
nerozhodný	rozhodný	k2eNgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgFnPc6
soutěžích	soutěž	k1gFnPc6
není	být	k5eNaImIp3nS
nerozhodný	rozhodný	k2eNgInSc1d1
výsledek	výsledek	k1gInSc1
přípustný	přípustný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
takovém	takový	k3xDgInSc6
případě	případ	k1gInSc6
příslušný	příslušný	k2eAgInSc1d1
soutěžní	soutěžní	k2eAgInSc1d1
řád	řád	k1gInSc1
určuje	určovat	k5eAaImIp3nS
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
dále	daleko	k6eAd2
postupuje	postupovat	k5eAaImIp3nS
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
prodloužením	prodloužení	k1gNnSc7
doby	doba	k1gFnSc2
hry	hra	k1gFnSc2
<g/>
,	,	kIx,
prováděním	provádění	k1gNnSc7
kopů	kop	k1gInPc2
z	z	k7c2
pokutové	pokutový	k2eAgFnSc2d1
značky	značka	k1gFnSc2
nebo	nebo	k8xC
vyšší	vysoký	k2eAgFnSc7d2
hodnotou	hodnota	k1gFnSc7
branek	branka	k1gFnPc2
vstřelených	vstřelený	k2eAgFnPc2d1
mužstvem	mužstvo	k1gNnSc7
hostí	host	k1gMnPc2wK
<g/>
.	.	kIx.
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ofsajd	ofsajd	k1gInSc1
</s>
<s>
Příklad	příklad	k1gInSc1
ofsajdu	ofsajd	k1gInSc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Ofsajd	ofsajd	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
se	se	k3xPyFc4
některý	některý	k3yIgMnSc1
hráč	hráč	k1gMnSc1
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
soupeřově	soupeřův	k2eAgFnSc6d1
polovině	polovina	k1gFnSc6
<g/>
,	,	kIx,
blíže	blízce	k6eAd2
soupeřově	soupeřův	k2eAgFnSc6d1
brankové	brankový	k2eAgFnSc6d1
čáře	čára	k1gFnSc6
než	než	k8xS
míč	míč	k1gInSc4
a	a	k8xC
než	než	k8xS
předposlední	předposlední	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
soupeře	soupeř	k1gMnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
v	v	k7c6
ofsajdové	ofsajdový	k2eAgFnSc6d1
pozici	pozice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
není	být	k5eNaImIp3nS
sama	sám	k3xTgFnSc1
o	o	k7c6
sobě	sebe	k3xPyFc6
porušením	porušení	k1gNnSc7
pravidel	pravidlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
však	však	k9
hráč	hráč	k1gMnSc1
v	v	k7c6
ofsajdové	ofsajdový	k2eAgFnSc6d1
pozici	pozice	k1gFnSc6
ve	v	k7c6
chvíli	chvíle	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
míče	míč	k1gInSc2
dotkne	dotknout	k5eAaPmIp3nS
některý	některý	k3yIgMnSc1
z	z	k7c2
jeho	jeho	k3xOp3gMnPc2
spoluhráčů	spoluhráč	k1gMnPc2
<g/>
,	,	kIx,
aktivně	aktivně	k6eAd1
zapojuje	zapojovat	k5eAaImIp3nS
do	do	k7c2
hry	hra	k1gFnSc2
(	(	kIx(
<g/>
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
zasahuje	zasahovat	k5eAaImIp3nS
do	do	k7c2
hry	hra	k1gFnSc2
hraním	hranit	k5eAaImIp1nS
míče	míč	k1gInSc2
<g/>
,	,	kIx,
ovlivňuje	ovlivňovat	k5eAaImIp3nS
soupeře	soupeř	k1gMnPc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
získává	získávat	k5eAaImIp3nS
ze	z	k7c2
svého	svůj	k3xOyFgNnSc2
postavení	postavení	k1gNnSc2
výhodu	výhoda	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
hraje	hrát	k5eAaImIp3nS
odražený	odražený	k2eAgInSc4d1
míč	míč	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
v	v	k7c6
ofsajdu	ofsajd	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
takové	takový	k3xDgFnSc6
situaci	situace	k1gFnSc6
rozhodčí	rozhodčí	k1gMnSc1
(	(	kIx(
<g/>
obvykle	obvykle	k6eAd1
na	na	k7c6
základě	základ	k1gInSc6
signalizace	signalizace	k1gFnSc2
asistenta	asistent	k1gMnSc2
<g/>
)	)	kIx)
přeruší	přerušit	k5eAaPmIp3nS
hru	hra	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
naváže	navázat	k5eAaPmIp3nS
soupeř	soupeř	k1gMnSc1
nepřímým	přímý	k2eNgInSc7d1
volným	volný	k2eAgInSc7d1
kopem	kop	k1gInSc7
z	z	k7c2
místa	místo	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgMnS
provinivší	provinivší	k2eAgMnPc4d1
se	se	k3xPyFc4
hráč	hráč	k1gMnSc1
ve	v	k7c6
chvíli	chvíle	k1gFnSc6
přestupku	přestupek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ofsajd	ofsajd	k1gInSc1
se	se	k3xPyFc4
netrestá	trestat	k5eNaImIp3nS
v	v	k7c6
situaci	situace	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
hráč	hráč	k1gMnSc1
dostane	dostat	k5eAaPmIp3nS
míč	míč	k1gInSc4
přímo	přímo	k6eAd1
z	z	k7c2
kopu	kop	k1gInSc2
od	od	k7c2
branky	branka	k1gFnSc2
<g/>
,	,	kIx,
z	z	k7c2
vhazování	vhazování	k1gNnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
z	z	k7c2
kopu	kop	k1gInSc2
z	z	k7c2
rohu	roh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestupky	přestupek	k1gInPc4
a	a	k8xC
provinění	provinění	k1gNnPc4
</s>
<s>
Když	když	k8xS
se	se	k3xPyFc4
některý	některý	k3yIgMnSc1
hráč	hráč	k1gMnSc1
na	na	k7c6
hrací	hrací	k2eAgFnSc6d1
ploše	plocha	k1gFnSc6
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
míč	míč	k1gInSc4
ve	v	k7c6
hře	hra	k1gFnSc6
<g/>
,	,	kIx,
dopustí	dopustit	k5eAaPmIp3nS
vůči	vůči	k7c3
soupeři	soupeř	k1gMnSc3
způsobem	způsob	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
podle	podle	k7c2
názoru	názor	k1gInSc2
rozhodčího	rozhodčí	k1gMnSc2
nedbalý	dbalý	k2eNgMnSc1d1
<g/>
,	,	kIx,
bezohledný	bezohledný	k2eAgMnSc1d1
nebo	nebo	k8xC
je	být	k5eAaImIp3nS
proveden	provést	k5eAaPmNgInS
s	s	k7c7
použitím	použití	k1gNnSc7
nepřiměřené	přiměřený	k2eNgFnSc2d1
síly	síla	k1gFnSc2
<g/>
,	,	kIx,
dopustí	dopustit	k5eAaPmIp3nS
některého	některý	k3yIgNnSc2
z	z	k7c2
dále	daleko	k6eAd2
vyjmenovaných	vyjmenovaný	k2eAgInPc2d1
přestupků	přestupek	k1gInPc2
<g/>
,	,	kIx,
nařídí	nařídit	k5eAaPmIp3nS
rozhodčí	rozhodčí	k1gMnSc1
proti	proti	k7c3
jeho	jeho	k3xOp3gNnSc3
mužstvu	mužstvo	k1gNnSc3
přímý	přímý	k2eAgInSc4d1
volný	volný	k2eAgInSc4d1
kop	kop	k1gInSc4
<g/>
:	:	kIx,
</s>
<s>
vrazí	vrazit	k5eAaPmIp3nS
do	do	k7c2
soupeře	soupeř	k1gMnSc2
<g/>
,	,	kIx,
</s>
<s>
skočí	skočit	k5eAaPmIp3nS
na	na	k7c4
soupeře	soupeř	k1gMnSc4
<g/>
,	,	kIx,
</s>
<s>
kopne	kopnout	k5eAaPmIp3nS
nebo	nebo	k8xC
se	se	k3xPyFc4
pokusí	pokusit	k5eAaPmIp3nS
kopnout	kopnout	k5eAaPmF
soupeře	soupeř	k1gMnSc4
<g/>
,	,	kIx,
</s>
<s>
strčí	strčit	k5eAaPmIp3nS
do	do	k7c2
soupeře	soupeř	k1gMnSc2
<g/>
,	,	kIx,
</s>
<s>
udeří	udeřit	k5eAaPmIp3nS
nebo	nebo	k8xC
se	se	k3xPyFc4
pokusí	pokusit	k5eAaPmIp3nS
udeřit	udeřit	k5eAaPmF
soupeře	soupeř	k1gMnSc4
<g/>
,	,	kIx,
</s>
<s>
nedovoleně	dovoleně	k6eNd1
zastaví	zastavit	k5eAaPmIp3nS
soupeře	soupeř	k1gMnSc4
<g/>
,	,	kIx,
</s>
<s>
podrazí	podrazit	k5eAaPmIp3nS
nebo	nebo	k8xC
se	se	k3xPyFc4
pokusí	pokusit	k5eAaPmIp3nS
podrazit	podrazit	k5eAaPmF
soupeře	soupeř	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Stejně	stejně	k6eAd1
tak	tak	k6eAd1
se	se	k3xPyFc4
přímý	přímý	k2eAgInSc1d1
volný	volný	k2eAgInSc1d1
kop	kop	k1gInSc1
nařídí	nařídit	k5eAaPmIp3nS
<g/>
,	,	kIx,
pokud	pokud	k8xS
hráč	hráč	k1gMnSc1
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
míč	míč	k1gInSc4
ve	v	k7c6
hře	hra	k1gFnSc6
<g/>
:	:	kIx,
</s>
<s>
hraje	hrát	k5eAaImIp3nS
míč	míč	k1gInSc4
rukou	ruka	k1gFnPc2
<g/>
,	,	kIx,
vyjma	vyjma	k7c2
brankáře	brankář	k1gMnSc2
v	v	k7c6
jeho	jeho	k3xOp3gNnSc6
vlastním	vlastní	k2eAgNnSc6d1
pokutovém	pokutový	k2eAgNnSc6d1
území	území	k1gNnSc6
<g/>
,	,	kIx,
</s>
<s>
drží	držet	k5eAaImIp3nP
soupeře	soupeř	k1gMnSc4
<g/>
,	,	kIx,
</s>
<s>
brání	bránit	k5eAaImIp3nP
soupeři	soupeř	k1gMnPc1
v	v	k7c6
pohybu	pohyb	k1gInSc6
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
dojde	dojít	k5eAaPmIp3nS
ke	k	k7c3
kontaktu	kontakt	k1gInSc3
<g/>
,	,	kIx,
</s>
<s>
někoho	někdo	k3yInSc4
kousne	kousnout	k5eAaPmIp3nS
nebo	nebo	k8xC
na	na	k7c4
někoho	někdo	k3yInSc4
plivne	plivnout	k5eAaPmIp3nS
<g/>
,	,	kIx,
</s>
<s>
hodí	hodit	k5eAaImIp3nS,k5eAaPmIp3nS
předmět	předmět	k1gInSc4
na	na	k7c4
míč	míč	k1gInSc4
<g/>
,	,	kIx,
soupeře	soupeř	k1gMnSc2
či	či	k8xC
rozhodčího	rozhodčí	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
takovém	takový	k3xDgInSc6
případě	případ	k1gInSc6
se	se	k3xPyFc4
přímý	přímý	k2eAgInSc1d1
volný	volný	k2eAgInSc1d1
kop	kop	k1gInSc1
provádí	provádět	k5eAaImIp3nS
z	z	k7c2
místa	místo	k1gNnSc2
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
k	k	k7c3
přestupku	přestupek	k1gInSc3
došlo	dojít	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
hráč	hráč	k1gMnSc1
ve	v	k7c6
chvíli	chvíle	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
míč	míč	k1gInSc4
ve	v	k7c6
hře	hra	k1gFnSc6
<g/>
,	,	kIx,
dopustí	dopustit	k5eAaPmIp3nS
jednoho	jeden	k4xCgMnSc4
z	z	k7c2
těchto	tento	k3xDgInPc2
přestupků	přestupek	k1gInPc2
ve	v	k7c6
vlastním	vlastní	k2eAgNnSc6d1
pokutovém	pokutový	k2eAgNnSc6d1
území	území	k1gNnSc6
(	(	kIx(
<g/>
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
místo	místo	k1gNnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
v	v	k7c4
tu	ten	k3xDgFnSc4
chvíli	chvíle	k1gFnSc4
míč	míč	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nařídí	nařídit	k5eAaPmIp3nS
rozhodčí	rozhodčí	k1gMnSc1
proti	proti	k7c3
jeho	jeho	k3xOp3gNnSc3
mužstvu	mužstvo	k1gNnSc3
namísto	namísto	k7c2
toho	ten	k3xDgNnSc2
pokutový	pokutový	k2eAgInSc4d1
kop	kop	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
se	se	k3xPyFc4
některý	některý	k3yIgMnSc1
hráč	hráč	k1gMnSc1
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
míč	míč	k1gInSc4
ve	v	k7c6
hře	hra	k1gFnSc6
<g/>
,	,	kIx,
dopustí	dopustit	k5eAaPmIp3nS
některého	některý	k3yIgNnSc2
z	z	k7c2
dále	daleko	k6eAd2
vyjmenovaných	vyjmenovaný	k2eAgInPc2d1
přestupků	přestupek	k1gInPc2
<g/>
,	,	kIx,
nařídí	nařídit	k5eAaPmIp3nS
rozhodčí	rozhodčí	k1gMnSc1
proti	proti	k7c3
jeho	jeho	k3xOp3gNnSc3
mužstvu	mužstvo	k1gNnSc3
nepřímý	přímý	k2eNgInSc4d1
volný	volný	k2eAgInSc4d1
kop	kop	k1gInSc4
<g/>
:	:	kIx,
</s>
<s>
hraje	hrát	k5eAaImIp3nS
nebezpečným	bezpečný	k2eNgInSc7d1
způsobem	způsob	k1gInSc7
<g/>
,	,	kIx,
</s>
<s>
brání	bránit	k5eAaImIp3nP
soupeři	soupeř	k1gMnPc1
v	v	k7c6
pohybu	pohyb	k1gInSc6
(	(	kIx(
<g/>
ale	ale	k8xC
aniž	aniž	k8xC,k8xS
by	by	kYmCp3nS
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
kontaktu	kontakt	k1gInSc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
projevuje	projevovat	k5eAaImIp3nS
nesouhlas	nesouhlas	k1gInSc4
s	s	k7c7
rozhodnutím	rozhodnutí	k1gNnSc7
rozhodčího	rozhodčí	k1gMnSc2
<g/>
,	,	kIx,
používá	používat	k5eAaImIp3nS
slovní	slovní	k2eAgInPc4d1
útoky	útok	k1gInPc4
<g/>
,	,	kIx,
útočné	útočný	k2eAgInPc4d1
<g/>
,	,	kIx,
urážlivé	urážlivý	k2eAgInPc4d1
nebo	nebo	k8xC
vulgární	vulgární	k2eAgInPc4d1
výrazy	výraz	k1gInPc4
nebo	nebo	k8xC
gesta	gesto	k1gNnPc4
<g/>
,	,	kIx,
</s>
<s>
znemožňuje	znemožňovat	k5eAaImIp3nS
brankáři	brankář	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
má	mít	k5eAaImIp3nS
míč	míč	k1gInSc4
v	v	k7c6
rukou	ruka	k1gFnPc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
míče	míč	k1gInPc4
zbavil	zbavit	k5eAaPmAgInS
<g/>
,	,	kIx,
</s>
<s>
dopustí	dopustit	k5eAaPmIp3nP
se	se	k3xPyFc4
nějakého	nějaký	k3yIgInSc2
jiného	jiný	k2eAgInSc2d1
přestupku	přestupek	k1gInSc2
<g/>
,	,	kIx,
za	za	k7c4
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
rozhodčí	rozhodčí	k1gMnPc1
udělí	udělit	k5eAaPmIp3nP
osobní	osobní	k2eAgInSc4d1
trest	trest	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Nepřímý	přímý	k2eNgInSc4d1
volný	volný	k2eAgInSc4d1
kop	kop	k1gInSc4
se	se	k3xPyFc4
nařizuje	nařizovat	k5eAaImIp3nS
také	také	k9
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
brankář	brankář	k1gMnSc1
ve	v	k7c6
vlastním	vlastní	k2eAgNnSc6d1
pokutovém	pokutový	k2eAgNnSc6d1
území	území	k1gNnSc6
dopustí	dopustit	k5eAaPmIp3nS
některého	některý	k3yIgInSc2
z	z	k7c2
těchto	tento	k3xDgInPc2
přestupků	přestupek	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
má	mít	k5eAaImIp3nS
míč	míč	k1gInSc4
v	v	k7c6
rukou	ruka	k1gFnPc6
pod	pod	k7c7
kontrolou	kontrola	k1gFnSc7
déle	dlouho	k6eAd2
než	než	k8xS
šest	šest	k4xCc4
sekund	sekunda	k1gFnPc2
<g/>
,	,	kIx,
</s>
<s>
rukou	ruka	k1gFnSc7
se	se	k3xPyFc4
míče	míč	k1gInSc2
dotkne	dotknout	k5eAaPmIp3nS
znovu	znovu	k6eAd1
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
se	se	k3xPyFc4
jej	on	k3xPp3gInSc2
zbavil	zbavit	k5eAaPmAgMnS
a	a	k8xC
míče	míč	k1gInPc1
se	se	k3xPyFc4
mezitím	mezitím	k6eAd1
nedotkl	dotknout	k5eNaPmAgMnS
jiný	jiný	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
rukou	ruka	k1gFnSc7
se	se	k3xPyFc4
dotkne	dotknout	k5eAaPmIp3nS
míče	míč	k1gInSc2
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
mu	on	k3xPp3gMnSc3
jej	on	k3xPp3gInSc4
úmyslně	úmyslně	k6eAd1
nohou	noha	k1gFnSc7
přihraje	přihrát	k5eAaPmIp3nS
spoluhráč	spoluhráč	k1gMnSc1
nebo	nebo	k8xC
ho	on	k3xPp3gMnSc4
obdržel	obdržet	k5eAaPmAgMnS
přímo	přímo	k6eAd1
z	z	k7c2
vhazování	vhazování	k1gNnSc2
spoluhráče	spoluhráč	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Nepřímý	přímý	k2eNgInSc4d1
volný	volný	k2eAgInSc4d1
kop	kop	k1gInSc4
se	se	k3xPyFc4
provádí	provádět	k5eAaImIp3nS
z	z	k7c2
místa	místo	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
k	k	k7c3
přestupku	přestupek	k1gInSc3
došlo	dojít	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volný	volný	k2eAgInSc4d1
kop	kop	k1gInSc4
(	(	kIx(
<g/>
přímý	přímý	k2eAgInSc4d1
či	či	k8xC
nepřímý	přímý	k2eNgInSc4d1
<g/>
)	)	kIx)
nařízený	nařízený	k2eAgMnSc1d1
ve	v	k7c4
prospěch	prospěch	k1gInSc4
mužstva	mužstvo	k1gNnSc2
v	v	k7c6
jeho	jeho	k3xOp3gNnSc6
vlastním	vlastní	k2eAgNnSc6d1
brankovém	brankový	k2eAgNnSc6d1
území	území	k1gNnSc6
se	se	k3xPyFc4
smí	smět	k5eAaImIp3nS
provést	provést	k5eAaPmF
z	z	k7c2
libovolného	libovolný	k2eAgNnSc2d1
místa	místo	k1gNnSc2
uvnitř	uvnitř	k7c2
tohoto	tento	k3xDgNnSc2
území	území	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nepřímý	přímý	k2eNgInSc4d1
volný	volný	k2eAgInSc4d1
kop	kop	k1gInSc4
nařízený	nařízený	k2eAgInSc4d1
proti	proti	k7c3
mužstvu	mužstvo	k1gNnSc3
v	v	k7c6
jeho	jeho	k3xOp3gNnSc6
brankovém	brankový	k2eAgNnSc6d1
území	území	k1gNnSc6
se	se	k3xPyFc4
provádí	provádět	k5eAaImIp3nS
z	z	k7c2
nejbližšího	blízký	k2eAgNnSc2d3
místa	místo	k1gNnSc2
na	na	k7c6
delší	dlouhý	k2eAgFnSc6d2
hraně	hrana	k1gFnSc6
tohoto	tento	k3xDgNnSc2
území	území	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Napomenutí	napomenutí	k1gNnSc1
hráče	hráč	k1gMnSc4
žlutou	žlutý	k2eAgFnSc7d1
kartou	karta	k1gFnSc7
</s>
<s>
Hráče	hráč	k1gMnPc4
nebo	nebo	k8xC
náhradníka	náhradník	k1gMnSc4
může	moct	k5eAaImIp3nS
rozhodčí	rozhodčí	k1gMnSc1
napomenout	napomenout	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
mu	on	k3xPp3gMnSc3
ukáže	ukázat	k5eAaPmIp3nS
žlutou	žlutý	k2eAgFnSc4d1
kartu	karta	k1gFnSc4
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
hráč	hráč	k1gMnSc1
dopustí	dopustit	k5eAaPmIp3nS
jednoho	jeden	k4xCgMnSc4
z	z	k7c2
následujících	následující	k2eAgMnPc2d1
přestupků	přestupek	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
zdržuje	zdržovat	k5eAaImIp3nS
navázání	navázání	k1gNnSc4
hry	hra	k1gFnSc2
<g/>
,	,	kIx,
</s>
<s>
slovy	slovo	k1gNnPc7
nebo	nebo	k8xC
jednáním	jednání	k1gNnSc7
projevuje	projevovat	k5eAaImIp3nS
nespokojenost	nespokojenost	k1gFnSc4
nebo	nebo	k8xC
protestuje	protestovat	k5eAaBmIp3nS
<g/>
,	,	kIx,
</s>
<s>
bez	bez	k7c2
souhlasu	souhlas	k1gInSc2
rozhodčího	rozhodčí	k1gMnSc2
vstoupí	vstoupit	k5eAaPmIp3nP
na	na	k7c4
hrací	hrací	k2eAgFnSc4d1
plochu	plocha	k1gFnSc4
nebo	nebo	k8xC
ji	on	k3xPp3gFnSc4
opustí	opustit	k5eAaPmIp3nP
<g/>
,	,	kIx,
</s>
<s>
nedodrží	dodržet	k5eNaPmIp3nS
předepsanou	předepsaný	k2eAgFnSc4d1
vzdálenost	vzdálenost	k1gFnSc4
při	při	k7c6
provádění	provádění	k1gNnSc6
volného	volný	k2eAgInSc2d1
kopu	kop	k1gInSc2
<g/>
,	,	kIx,
kopu	kopat	k5eAaImIp1nS
z	z	k7c2
rohu	roh	k1gInSc2
<g/>
,	,	kIx,
vhazování	vhazování	k1gNnSc2
nebo	nebo	k8xC
míče	míč	k1gInSc2
rozhodčího	rozhodčí	k1gMnSc2
<g/>
,	,	kIx,
</s>
<s>
soustavně	soustavně	k6eAd1
porušuje	porušovat	k5eAaImIp3nS
pravidla	pravidlo	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
chová	chovat	k5eAaImIp3nS
se	se	k3xPyFc4
nesportovně	sportovně	k6eNd1
(	(	kIx(
<g/>
sem	sem	k6eAd1
spadá	spadat	k5eAaImIp3nS,k5eAaPmIp3nS
i	i	k9
přestupek	přestupek	k1gInSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
hráč	hráč	k1gMnSc1
přestupkem	přestupek	k1gInSc7
zmaří	zmařit	k5eAaPmIp3nP
zjevnou	zjevný	k2eAgFnSc4d1
brankovou	brankový	k2eAgFnSc4d1
možnost	možnost	k1gFnSc4
soupeře	soupeř	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
vstoupí	vstoupit	k5eAaPmIp3nP
do	do	k7c2
prostoru	prostor	k1gInSc2
určeného	určený	k2eAgInSc2d1
pro	pro	k7c4
přezkoumání	přezkoumání	k1gNnSc4
videa	video	k1gNnSc2
rozhodčím	rozhodčí	k1gMnPc3
<g/>
,	,	kIx,
</s>
<s>
nepřiměřeně	přiměřeně	k6eNd1
se	se	k3xPyFc4
domáhá	domáhat	k5eAaImIp3nS
přezkoumání	přezkoumání	k1gNnSc1
videozáznamu	videozáznam	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Rozhodčí	rozhodčí	k1gMnPc1
musí	muset	k5eAaImIp3nP
vyloučit	vyloučit	k5eAaPmF
hráče	hráč	k1gMnPc4
ze	z	k7c2
hry	hra	k1gFnSc2
(	(	kIx(
<g/>
přičemž	přičemž	k6eAd1
mu	on	k3xPp3gMnSc3
ukáže	ukázat	k5eAaPmIp3nS
červenou	červený	k2eAgFnSc4d1
kartu	karta	k1gFnSc4
<g/>
)	)	kIx)
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
dopustí	dopustit	k5eAaPmIp3nS
jednoho	jeden	k4xCgInSc2
z	z	k7c2
následujících	následující	k2eAgInPc2d1
přestupků	přestupek	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
zabrání	zabránit	k5eAaPmIp3nS
soupeřovu	soupeřův	k2eAgNnSc3d1
družstvu	družstvo	k1gNnSc3
dosáhnout	dosáhnout	k5eAaPmF
branky	branka	k1gFnPc4
nebo	nebo	k8xC
zmaří	zmařit	k5eAaPmIp3nP
jeho	jeho	k3xOp3gFnSc4
zjevnou	zjevný	k2eAgFnSc4d1
možnost	možnost	k1gFnSc4
dosáhnout	dosáhnout	k5eAaPmF
branky	branka	k1gFnSc2
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
zahraje	zahrát	k5eAaPmIp3nS
míč	míč	k1gInSc4
rukou	ruka	k1gFnPc2
(	(	kIx(
<g/>
netýká	týkat	k5eNaImIp3nS
se	se	k3xPyFc4
brankáře	brankář	k1gMnSc2
v	v	k7c6
jeho	jeho	k3xOp3gNnSc6
pokutovém	pokutový	k2eAgNnSc6d1
území	území	k1gNnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
zmaří	zmařit	k5eAaPmIp3nS
soupeřovu	soupeřův	k2eAgFnSc4d1
zjevnou	zjevný	k2eAgFnSc4d1
možnost	možnost	k1gFnSc4
dosáhnout	dosáhnout	k5eAaPmF
branky	branka	k1gFnSc2
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
soupeře	soupeř	k1gMnSc4
útočícího	útočící	k2eAgMnSc4d1
na	na	k7c4
branku	branka	k1gFnSc4
zastaví	zastavit	k5eAaPmIp3nS
přestupkem	přestupek	k1gInSc7
<g/>
,	,	kIx,
za	za	k7c4
nějž	jenž	k3xRgMnSc4
se	se	k3xPyFc4
nařizuje	nařizovat	k5eAaImIp3nS
volný	volný	k2eAgInSc4d1
přímý	přímý	k2eAgInSc4d1
kop	kop	k1gInSc4
<g/>
,	,	kIx,
popř.	popř.	kA
pokutový	pokutový	k2eAgInSc4d1
kop	kop	k1gInSc4
<g/>
,	,	kIx,
</s>
<s>
hraje	hrát	k5eAaImIp3nS
surově	surově	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
kousne	kousnout	k5eAaPmIp3nS
nebo	nebo	k8xC
plivne	plivnout	k5eAaPmIp3nS
na	na	k7c4
soupeře	soupeř	k1gMnSc4
či	či	k8xC
jinou	jiný	k2eAgFnSc4d1
osobu	osoba	k1gFnSc4
<g/>
,	,	kIx,
</s>
<s>
chová	chovat	k5eAaImIp3nS
se	se	k3xPyFc4
hrubě	hrubě	k6eAd1
nesportovně	sportovně	k6eNd1
<g/>
,	,	kIx,
</s>
<s>
použije	použít	k5eAaPmIp3nS
pohoršujících	pohoršující	k2eAgInPc2d1
<g/>
,	,	kIx,
urážlivých	urážlivý	k2eAgInPc2d1
či	či	k8xC
ponižujících	ponižující	k2eAgInPc2d1
výroků	výrok	k1gInPc2
nebo	nebo	k8xC
gest	gesto	k1gNnPc2
<g/>
,	,	kIx,
</s>
<s>
během	během	k7c2
utkání	utkání	k1gNnSc2
je	být	k5eAaImIp3nS
podruhé	podruhé	k6eAd1
napomenut	napomenout	k5eAaPmNgMnS
žlutou	žlutý	k2eAgFnSc7d1
kartou	karta	k1gFnSc7
<g/>
,	,	kIx,
</s>
<s>
vstoupí	vstoupit	k5eAaPmIp3nS
na	na	k7c4
pracoviště	pracoviště	k1gNnSc4
video	video	k1gNnSc1
asistenta	asistent	k1gMnSc2
rozhodčího	rozhodčí	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Vyloučený	vyloučený	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
musí	muset	k5eAaImIp3nS
opustit	opustit	k5eAaPmF
prostor	prostor	k1gInSc4
hřiště	hřiště	k1gNnSc2
(	(	kIx(
<g/>
i	i	k9
lavičku	lavička	k1gFnSc4
náhradníků	náhradník	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Napomenuti	napomenut	k2eAgMnPc1d1
<g/>
,	,	kIx,
případně	případně	k6eAd1
vyloučeni	vyloučen	k2eAgMnPc1d1
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
i	i	k9
náhradníci	náhradník	k1gMnPc1
<g/>
,	,	kIx,
vystřídaní	vystřídaný	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
a	a	k8xC
funkcionáři	funkcionář	k1gMnPc1
družstva	družstvo	k1gNnSc2
vyskytující	vyskytující	k2eAgMnSc1d1
se	se	k3xPyFc4
na	na	k7c6
lavičce	lavička	k1gFnSc6
náhradníků	náhradník	k1gMnPc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
jejich	jejich	k3xOp3gNnSc1
chování	chování	k1gNnSc1
odporuje	odporovat	k5eAaImIp3nS
pravidlům	pravidlo	k1gNnPc3
<g/>
.	.	kIx.
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volné	volný	k2eAgInPc1d1
kopy	kop	k1gInPc1
</s>
<s>
Přímý	přímý	k2eAgInSc4d1
volný	volný	k2eAgInSc4d1
kop	kop	k1gInSc4
</s>
<s>
Existují	existovat	k5eAaImIp3nP
dva	dva	k4xCgInPc1
druhy	druh	k1gInPc1
volných	volný	k2eAgInPc2d1
kopů	kop	k1gInPc2
<g/>
:	:	kIx,
přímé	přímý	k2eAgInPc4d1
volné	volný	k2eAgInPc4d1
kopy	kop	k1gInPc4
a	a	k8xC
nepřímé	přímý	k2eNgInPc4d1
volné	volný	k2eAgInPc4d1
kopy	kop	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
přímo	přímo	k6eAd1
po	po	k7c6
provedení	provedení	k1gNnSc6
přímého	přímý	k2eAgInSc2d1
volného	volný	k2eAgInSc2d1
kopu	kop	k1gInSc2
dostane	dostat	k5eAaPmIp3nS
míč	míč	k1gInSc4
do	do	k7c2
soupeřovy	soupeřův	k2eAgFnSc2d1
branky	branka	k1gFnSc2
<g/>
,	,	kIx,
branka	branka	k1gFnSc1
platí	platit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
dostane	dostat	k5eAaPmIp3nS
přímo	přímo	k6eAd1
do	do	k7c2
vlastní	vlastní	k2eAgFnSc2d1
branky	branka	k1gFnSc2
<g/>
,	,	kIx,
branka	branka	k1gFnSc1
neplatí	platit	k5eNaImIp3nS
a	a	k8xC
hra	hra	k1gFnSc1
se	se	k3xPyFc4
naváže	navázat	k5eAaPmIp3nS
kopem	kop	k1gInSc7
z	z	k7c2
rohu	roh	k1gInSc2
ve	v	k7c4
prospěch	prospěch	k1gInSc4
soupeřova	soupeřův	k2eAgNnSc2d1
mužstva	mužstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Nepřímý	přímý	k2eNgInSc4d1
volný	volný	k2eAgInSc4d1
kop	kop	k1gInSc4
rozhodčí	rozhodčí	k1gMnSc1
signalizuje	signalizovat	k5eAaImIp3nS
paží	paže	k1gFnSc7
zvednutou	zvednutý	k2eAgFnSc7d1
nad	nad	k7c4
hlavu	hlava	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
tam	tam	k6eAd1
drží	držet	k5eAaImIp3nS
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
některý	některý	k3yIgMnSc1
z	z	k7c2
hráčů	hráč	k1gMnPc2
po	po	k7c6
provedení	provedení	k1gNnSc6
kopu	kop	k1gInSc2
dotkne	dotknout	k5eAaPmIp3nS
míče	míč	k1gInSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
než	než	k8xS
je	být	k5eAaImIp3nS
míč	míč	k1gInSc4
ze	z	k7c2
hry	hra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Branky	branka	k1gFnPc4
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
z	z	k7c2
nepřímého	přímý	k2eNgInSc2d1
volného	volný	k2eAgInSc2d1
kopu	kop	k1gInSc2
dosaženo	dosáhnout	k5eAaPmNgNnS
jen	jen	k9
tehdy	tehdy	k6eAd1
<g/>
,	,	kIx,
dotkl	dotknout	k5eAaPmAgMnS
<g/>
-li	-li	k?
se	se	k3xPyFc4
míče	míč	k1gInPc1
po	po	k7c6
jeho	jeho	k3xOp3gNnSc6
rozehrání	rozehrání	k1gNnSc6
alespoň	alespoň	k9
jeden	jeden	k4xCgMnSc1
další	další	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
před	před	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
než	než	k8xS
míč	míč	k1gInSc1
přejde	přejít	k5eAaPmIp3nS
do	do	k7c2
branky	branka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
míč	míč	k1gInSc1
skončí	skončit	k5eAaPmIp3nS
přímo	přímo	k6eAd1
z	z	k7c2
nepřímého	přímý	k2eNgInSc2d1
volného	volný	k2eAgInSc2d1
kopu	kop	k1gInSc2
v	v	k7c6
brance	branka	k1gFnSc6
<g/>
,	,	kIx,
aniž	aniž	k8xS,k8xC
by	by	kYmCp3nP
se	se	k3xPyFc4
ho	on	k3xPp3gMnSc4
kdokoli	kdokoli	k3yInSc1
další	další	k2eAgFnSc2d1
dotkl	dotknout	k5eAaPmAgMnS
<g/>
,	,	kIx,
branka	branka	k1gFnSc1
neplatí	platit	k5eNaImIp3nS
a	a	k8xC
hra	hra	k1gFnSc1
se	se	k3xPyFc4
naváže	navázat	k5eAaPmIp3nS
kopem	kop	k1gInSc7
od	od	k7c2
branky	branka	k1gFnSc2
<g/>
,	,	kIx,
resp.	resp.	kA
kopem	kop	k1gInSc7
z	z	k7c2
rohu	roh	k1gInSc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
by	by	kYmCp3nS
míč	míč	k1gInSc1
skončil	skončit	k5eAaPmAgInS
v	v	k7c6
brance	branka	k1gFnSc6
mužstva	mužstvo	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
kop	kop	k1gInSc4
provádělo	provádět	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s>
Před	před	k7c7
provedením	provedení	k1gNnSc7
volného	volný	k2eAgInSc2d1
kopu	kop	k1gInSc2
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
všichni	všechen	k3xTgMnPc1
hráči	hráč	k1gMnPc1
soupeře	soupeř	k1gMnSc2
vzdáleni	vzdálit	k5eAaPmNgMnP
od	od	k7c2
míče	míč	k1gInSc2
nejméně	málo	k6eAd3
9,15	9,15	k4
m	m	kA
<g/>
,	,	kIx,
dokud	dokud	k8xS
se	se	k3xPyFc4
míč	míč	k1gInSc1
nepohne	pohnout	k5eNaPmIp3nS
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
je	být	k5eAaImIp3nS
uveden	uvést	k5eAaPmNgMnS
do	do	k7c2
hry	hra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
je	být	k5eAaImIp3nS
nepřímý	přímý	k2eNgInSc4d1
volný	volný	k2eAgInSc4d1
kop	kop	k1gInSc4
prováděn	provádět	k5eAaImNgInS
v	v	k7c6
soupeřově	soupeřův	k2eAgNnSc6d1
pokutovém	pokutový	k2eAgNnSc6d1
území	území	k1gNnSc6
<g/>
,	,	kIx,
nemusí	muset	k5eNaImIp3nP
ti	ten	k3xDgMnPc1
hráči	hráč	k1gMnPc1
soupeře	soupeř	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
stojí	stát	k5eAaImIp3nP
na	na	k7c6
brankové	brankový	k2eAgFnSc6d1
čáře	čára	k1gFnSc6
mezi	mezi	k7c7
tyčemi	tyč	k1gFnPc7
branky	branka	k1gFnSc2
<g/>
,	,	kIx,
dodržovat	dodržovat	k5eAaImF
předepsanou	předepsaný	k2eAgFnSc4d1
vzdálenost	vzdálenost	k1gFnSc4
9,15	9,15	k4
m.	m.	k?
Pokud	pokud	k8xS
alespoň	alespoň	k9
tři	tři	k4xCgMnPc1
hráči	hráč	k1gMnPc1
soupeře	soupeř	k1gMnSc2
sestavují	sestavovat	k5eAaImIp3nP
při	při	k7c6
provádění	provádění	k1gNnSc6
volného	volný	k2eAgInSc2d1
kopu	kop	k1gInSc2
tzv.	tzv.	kA
zeď	zeď	k1gFnSc1
(	(	kIx(
<g/>
aby	aby	kYmCp3nP
bránili	bránit	k5eAaImAgMnP
přímé	přímý	k2eAgFnSc3d1
střele	střela	k1gFnSc3
do	do	k7c2
branky	branka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nP
hráči	hráč	k1gMnPc1
družstva	družstvo	k1gNnSc2
provádějícího	provádějící	k2eAgNnSc2d1
kop	kop	k1gInSc4
zůstat	zůstat	k5eAaPmF
alespoň	alespoň	k9
metr	metr	k1gInSc4
od	od	k7c2
této	tento	k3xDgFnSc2
zdi	zeď	k1gFnSc2
do	do	k7c2
chvíle	chvíle	k1gFnSc2
<g/>
,	,	kIx,
než	než	k8xS
je	být	k5eAaImIp3nS
míč	míč	k1gInSc4
ve	v	k7c6
hře	hra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
bránící	bránící	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
nedodrží	dodržet	k5eNaPmIp3nP
předepsanou	předepsaný	k2eAgFnSc4d1
vzdálenost	vzdálenost	k1gFnSc4
od	od	k7c2
míče	míč	k1gInSc2
<g/>
,	,	kIx,
nechá	nechat	k5eAaPmIp3nS
rozhodčí	rozhodčí	k1gMnSc1
kop	kop	k1gInSc4
opakovat	opakovat	k5eAaImF
(	(	kIx(
<g/>
a	a	k8xC
hráče	hráč	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
vzdálenost	vzdálenost	k1gFnSc4
nedodržel	dodržet	k5eNaPmAgMnS
<g/>
,	,	kIx,
napomene	napomenout	k5eAaPmIp3nS
žlutou	žlutý	k2eAgFnSc7d1
kartou	karta	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráč	hráč	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
rozehrává	rozehrávat	k5eAaImIp3nS
volný	volný	k2eAgInSc4d1
kop	kop	k1gInSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
míče	míč	k1gInPc1
nesmí	smět	k5eNaImIp3nP
znovu	znovu	k6eAd1
dotknout	dotknout	k5eAaPmF
do	do	k7c2
chvíle	chvíle	k1gFnSc2
<g/>
,	,	kIx,
než	než	k8xS
míčem	míč	k1gInSc7
zahraje	zahrát	k5eAaPmIp3nS
jiný	jiný	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
nebo	nebo	k8xC
je	být	k5eAaImIp3nS
míč	míč	k1gInSc4
ze	z	k7c2
hry	hra	k1gFnSc2
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
rozhodčí	rozhodčí	k1gMnSc1
nařídí	nařídit	k5eAaPmIp3nS
proti	proti	k7c3
jeho	jeho	k3xOp3gNnSc3
mužstvu	mužstvo	k1gNnSc3
nepřímý	přímý	k2eNgInSc4d1
volný	volný	k2eAgInSc4d1
kop	kop	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokutový	pokutový	k2eAgInSc4d1
kop	kop	k1gInSc4
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Pokutový	pokutový	k2eAgInSc4d1
kop	kop	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Pokutový	pokutový	k2eAgInSc4d1
kop	kop	k1gInSc4
(	(	kIx(
<g/>
MS	MS	kA
2006	#num#	k4
<g/>
,	,	kIx,
zápas	zápas	k1gInSc1
Pobřeží	pobřeží	k1gNnSc2
slonoviny	slonovina	k1gFnSc2
–	–	k?
Srbsko	Srbsko	k1gNnSc1
a	a	k8xC
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
</s>
<s>
Pokutový	pokutový	k2eAgInSc4d1
kop	kop	k1gInSc4
se	se	k3xPyFc4
proti	proti	k7c3
mužstvu	mužstvo	k1gNnSc3
nařídí	nařídit	k5eAaPmIp3nS
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
hráč	hráč	k1gMnSc1
tohoto	tento	k3xDgNnSc2
mužstva	mužstvo	k1gNnSc2
dopustí	dopustit	k5eAaPmIp3nS
ve	v	k7c6
vlastním	vlastní	k2eAgNnSc6d1
pokutovém	pokutový	k2eAgNnSc6d1
území	území	k1gNnSc6
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
míč	míč	k1gInSc4
ve	v	k7c6
hře	hra	k1gFnSc6
<g/>
,	,	kIx,
jednoho	jeden	k4xCgMnSc4
z	z	k7c2
přestupků	přestupek	k1gInPc2
jinak	jinak	k6eAd1
trestaných	trestaný	k2eAgInPc2d1
přímým	přímý	k2eAgInSc7d1
volným	volný	k2eAgInSc7d1
kopem	kop	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
pokutového	pokutový	k2eAgInSc2d1
kopu	kop	k1gInSc2
lze	lze	k6eAd1
přímo	přímo	k6eAd1
dosáhnout	dosáhnout	k5eAaPmF
branky	branka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
je	být	k5eAaImIp3nS
pokutový	pokutový	k2eAgInSc4d1
kop	kop	k1gInSc4
nařízen	nařídit	k5eAaPmNgInS
těsně	těsně	k6eAd1
před	před	k7c7
ukončením	ukončení	k1gNnSc7
poločasu	poločas	k1gInSc2
<g/>
,	,	kIx,
nastaví	nastavit	k5eAaPmIp3nS,k5eAaBmIp3nS
rozhodčí	rozhodčí	k1gMnSc1
dobu	doba	k1gFnSc4
hry	hra	k1gFnSc2
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
tento	tento	k3xDgInSc4
kop	kop	k1gInSc4
mohl	moct	k5eAaImAgInS
být	být	k5eAaImF
proveden	provést	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s>
Pokutový	pokutový	k2eAgInSc4d1
kop	kop	k1gInSc4
se	se	k3xPyFc4
provádí	provádět	k5eAaImIp3nS
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
míč	míč	k1gInSc1
umístí	umístit	k5eAaPmIp3nS
na	na	k7c4
pokutovou	pokutový	k2eAgFnSc4d1
značku	značka	k1gFnSc4
a	a	k8xC
jeden	jeden	k4xCgMnSc1
z	z	k7c2
hráčů	hráč	k1gMnPc2
mužstva	mužstvo	k1gNnSc2
provádějícího	provádějící	k2eAgMnSc2d1
pokutový	pokutový	k2eAgInSc4d1
kop	kop	k1gInSc4
je	být	k5eAaImIp3nS
jasně	jasně	k6eAd1
a	a	k8xC
zřetelně	zřetelně	k6eAd1
určen	určit	k5eAaPmNgMnS
k	k	k7c3
jeho	jeho	k3xOp3gNnSc3
vykonání	vykonání	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brankář	brankář	k1gMnSc1
bránícího	bránící	k2eAgNnSc2d1
mužstva	mužstvo	k1gNnSc2
musí	muset	k5eAaImIp3nS
stát	stát	k5eAaImF,k5eAaPmF
v	v	k7c6
brance	branka	k1gFnSc6
na	na	k7c6
brankové	brankový	k2eAgFnSc6d1
čáře	čára	k1gFnSc6
alespoň	alespoň	k9
jednou	jednou	k6eAd1
nohou	noha	k1gFnSc7
do	do	k7c2
chvíle	chvíle	k1gFnSc2
<g/>
,	,	kIx,
než	než	k8xS
je	být	k5eAaImIp3nS
míč	míč	k1gInSc4
ve	v	k7c6
hře	hra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatní	ostatní	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
na	na	k7c6
hrací	hrací	k2eAgFnSc6d1
ploše	plocha	k1gFnSc6
<g/>
,	,	kIx,
mimo	mimo	k7c4
pokutové	pokutový	k2eAgNnSc4d1
území	území	k1gNnSc4
<g/>
,	,	kIx,
nejméně	málo	k6eAd3
9,15	9,15	k4
m	m	kA
od	od	k7c2
míče	míč	k1gInSc2
(	(	kIx(
<g/>
tzn.	tzn.	kA
mimo	mimo	k7c4
vyznačený	vyznačený	k2eAgInSc4d1
oblouk	oblouk	k1gInSc4
<g/>
)	)	kIx)
a	a	k8xC
za	za	k7c7
pokutovou	pokutový	k2eAgFnSc7d1
značkou	značka	k1gFnSc7
(	(	kIx(
<g/>
tzn.	tzn.	kA
dále	daleko	k6eAd2
od	od	k7c2
brankové	brankový	k2eAgFnSc2d1
čáry	čára	k1gFnSc2
<g/>
,	,	kIx,
než	než	k8xS
je	být	k5eAaImIp3nS
pokutová	pokutový	k2eAgFnSc1d1
značka	značka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
provedení	provedení	k1gNnSc3
pokutového	pokutový	k2eAgInSc2d1
kopu	kop	k1gInSc2
dá	dát	k5eAaPmIp3nS
rozhodčí	rozhodčí	k1gMnSc1
signál	signál	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráč	hráč	k1gMnSc1
provádějící	provádějící	k2eAgFnSc2d1
pokutový	pokutový	k2eAgInSc4d1
kop	kop	k1gInSc4
se	se	k3xPyFc4
nesmí	smět	k5eNaImIp3nS
v	v	k7c6
rozběhu	rozběh	k1gInSc6
zastavit	zastavit	k5eAaPmF
nebo	nebo	k8xC
jiným	jiný	k2eAgInSc7d1
obdobným	obdobný	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
oklamat	oklamat	k5eAaPmF
soupeřova	soupeřův	k2eAgMnSc4d1
brankáře	brankář	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Míč	míč	k1gInSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
hře	hra	k1gFnSc6
<g/>
,	,	kIx,
jakmile	jakmile	k8xS
se	se	k3xPyFc4
po	po	k7c6
provedení	provedení	k1gNnSc6
kopu	kop	k1gInSc2
pohne	pohnout	k5eAaPmIp3nS
kupředu	kupředu	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráč	hráč	k1gMnSc1
provádějící	provádějící	k2eAgFnSc2d1
pokutový	pokutový	k2eAgInSc4d1
kop	kop	k1gInSc4
se	se	k3xPyFc4
míče	míč	k1gInSc2
nesmí	smět	k5eNaImIp3nS
podruhé	podruhé	k6eAd1
dotknout	dotknout	k5eAaPmF
dříve	dříve	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
ho	on	k3xPp3gInSc2
dotkne	dotknout	k5eAaPmIp3nS
jiný	jiný	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
jsou	být	k5eAaImIp3nP
při	při	k7c6
provádění	provádění	k1gNnSc6
kopu	kop	k1gInSc2
porušena	porušen	k2eAgNnPc1d1
pravidla	pravidlo	k1gNnPc1
hráčem	hráč	k1gMnSc7
bránícího	bránící	k2eAgNnSc2d1
mužstva	mužstvo	k1gNnSc2
<g/>
,	,	kIx,
pak	pak	k6eAd1
rozhodčí	rozhodčí	k1gMnSc1
uzná	uznat	k5eAaPmIp3nS
branku	branka	k1gFnSc4
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
byla	být	k5eAaImAgFnS
dosažena	dosáhnout	k5eAaPmNgFnS
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
nechá	nechat	k5eAaPmIp3nS
kop	kop	k1gInSc4
opakovat	opakovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
pravidla	pravidlo	k1gNnPc4
poruší	porušit	k5eAaPmIp3nS
hráč	hráč	k1gMnSc1
útočícího	útočící	k2eAgNnSc2d1
mužstva	mužstvo	k1gNnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
dosaženo	dosáhnout	k5eAaPmNgNnS
branky	branka	k1gFnSc2
<g/>
,	,	kIx,
nechá	nechat	k5eAaPmIp3nS
rozhodčí	rozhodčí	k1gMnSc1
kop	kop	k1gInSc4
opakovat	opakovat	k5eAaImF
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
nechá	nechat	k5eAaPmIp3nS
pokračovat	pokračovat	k5eAaImF
ve	v	k7c6
hře	hra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
pravidla	pravidlo	k1gNnPc4
poruší	porušit	k5eAaPmIp3nS
hráči	hráč	k1gMnPc7
obou	dva	k4xCgNnPc2
mužstev	mužstvo	k1gNnPc2
<g/>
,	,	kIx,
nechá	nechat	k5eAaPmIp3nS
rozhodčí	rozhodčí	k1gMnSc1
kop	kop	k1gInSc4
opakovat	opakovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vhazování	vhazování	k1gNnSc1
</s>
<s>
Pokud	pokud	k8xS
je	být	k5eAaImIp3nS
hra	hra	k1gFnSc1
přerušena	přerušen	k2eAgFnSc1d1
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
míč	míč	k1gInSc1
přešel	přejít	k5eAaPmAgInS
pomezní	pomezní	k2eAgFnSc4d1
čáru	čára	k1gFnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
hra	hra	k1gFnSc1
navázána	navázán	k2eAgFnSc1d1
vhazováním	vhazování	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
provede	provést	k5eAaPmIp3nS
z	z	k7c2
místa	místo	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
míč	míč	k1gInSc4
čáru	čár	k1gInSc2
přešel	přejít	k5eAaPmAgMnS
<g/>
,	,	kIx,
hráč	hráč	k1gMnSc1
družstva	družstvo	k1gNnSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc1
hráč	hráč	k1gMnSc1
se	se	k3xPyFc4
míče	míč	k1gInSc2
nedotkl	dotknout	k5eNaPmAgInS
jako	jako	k9
poslední	poslední	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přímo	přímo	k6eAd1
z	z	k7c2
vhazování	vhazování	k1gNnSc2
nemůže	moct	k5eNaImIp3nS
být	být	k5eAaImF
dosaženo	dosáhnout	k5eAaPmNgNnS
branky	branka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozehrávající	rozehrávající	k2eAgInSc1d1
hráč	hráč	k1gMnSc1
se	se	k3xPyFc4
postaví	postavit	k5eAaPmIp3nS
oběma	dva	k4xCgMnPc7
nohama	noha	k1gFnPc7
za	za	k7c4
pomezní	pomezní	k2eAgFnSc4d1
čáru	čára	k1gFnSc4
a	a	k8xC
oběma	dva	k4xCgFnPc7
rukama	ruka	k1gFnPc7
hodí	hodit	k5eAaPmIp3nP,k5eAaImIp3nP
přes	přes	k7c4
hlavu	hlava	k1gFnSc4
míč	míč	k1gInSc1
do	do	k7c2
hrací	hrací	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
(	(	kIx(
<g/>
pokud	pokud	k8xS
vhazování	vhazování	k1gNnSc1
provede	provést	k5eAaPmIp3nS
nesprávně	správně	k6eNd1
<g/>
,	,	kIx,
provede	provést	k5eAaPmIp3nS
vhazování	vhazování	k1gNnSc1
hráč	hráč	k1gMnSc1
soupeře	soupeř	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
se	se	k3xPyFc4
nesmí	smět	k5eNaImIp3nS
míče	míč	k1gInSc2
dotknout	dotknout	k5eAaPmF
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
jej	on	k3xPp3gMnSc4
dotkne	dotknout	k5eAaPmIp3nS
alespoň	alespoň	k9
jeden	jeden	k4xCgMnSc1
další	další	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Míč	míč	k1gInSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
hře	hra	k1gFnSc6
okamžitě	okamžitě	k6eAd1
<g/>
,	,	kIx,
jakmile	jakmile	k8xS
se	se	k3xPyFc4
dostane	dostat	k5eAaPmIp3nS
na	na	k7c4
hrací	hrací	k2eAgFnSc4d1
plochu	plocha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráči	hráč	k1gMnPc7
soupeře	soupeř	k1gMnSc2
musí	muset	k5eAaImIp3nS
při	při	k7c6
vhazování	vhazování	k1gNnSc6
stát	stát	k1gInSc1
nejméně	málo	k6eAd3
2	#num#	k4
metry	metr	k1gInPc4
od	od	k7c2
místa	místo	k1gNnSc2
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
se	se	k3xPyFc4
vhazování	vhazování	k1gNnSc1
provádí	provádět	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kop	kop	k1gInSc1
od	od	k7c2
branky	branka	k1gFnSc2
</s>
<s>
Pokud	pokud	k8xS
je	být	k5eAaImIp3nS
hra	hra	k1gFnSc1
přerušena	přerušen	k2eAgFnSc1d1
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
míč	míč	k1gInSc1
přešel	přejít	k5eAaPmAgInS
brankovou	brankový	k2eAgFnSc4d1
čáru	čára	k1gFnSc4
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
se	se	k3xPyFc4
jej	on	k3xPp3gInSc2
naposledy	naposledy	k6eAd1
dotkl	dotknout	k5eAaPmAgMnS
hráč	hráč	k1gMnSc1
útočícího	útočící	k2eAgNnSc2d1
družstva	družstvo	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
přitom	přitom	k6eAd1
nebylo	být	k5eNaImAgNnS
dosaženo	dosáhnout	k5eAaPmNgNnS
branky	branka	k1gFnPc4
<g/>
,	,	kIx,
naváže	navázat	k5eAaPmIp3nS
se	se	k3xPyFc4
hra	hra	k1gFnSc1
kopem	kop	k1gInSc7
od	od	k7c2
branky	branka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přímo	přímo	k6eAd1
z	z	k7c2
kopu	kop	k1gInSc2
od	od	k7c2
branky	branka	k1gFnSc2
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
dosáhnout	dosáhnout	k5eAaPmF
branky	branka	k1gFnPc4
(	(	kIx(
<g/>
ovšem	ovšem	k9
ne	ne	k9
vlastní	vlastní	k2eAgFnSc3d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kop	kop	k1gInSc1
se	se	k3xPyFc4
provede	provést	k5eAaPmIp3nS
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
míč	míč	k1gInSc1
položí	položit	k5eAaPmIp3nS
na	na	k7c4
libovolné	libovolný	k2eAgNnSc4d1
místo	místo	k1gNnSc4
uvnitř	uvnitř	k7c2
brankového	brankový	k2eAgNnSc2d1
území	území	k1gNnSc2
a	a	k8xC
libovolný	libovolný	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
bránícího	bránící	k2eAgNnSc2d1
mužstva	mužstvo	k1gNnSc2
jej	on	k3xPp3gMnSc4
rozehraje	rozehrát	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
se	se	k3xPyFc4
nesmí	smět	k5eNaImIp3nS
míče	míč	k1gInSc2
podruhé	podruhé	k6eAd1
dotknout	dotknout	k5eAaPmF
dříve	dříve	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
jej	on	k3xPp3gMnSc4
dotkne	dotknout	k5eAaPmIp3nS
jiný	jiný	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokud	dokud	k8xS
míč	míč	k1gInSc1
není	být	k5eNaImIp3nS
ve	v	k7c6
hře	hra	k1gFnSc6
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nP
všichni	všechen	k3xTgMnPc1
hráči	hráč	k1gMnPc1
soupeře	soupeř	k1gMnSc4
zůstat	zůstat	k5eAaPmF
mimo	mimo	k7c4
pokutové	pokutový	k2eAgNnSc4d1
území	území	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Míč	míč	k1gInSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
hře	hra	k1gFnSc6
<g/>
,	,	kIx,
jakmile	jakmile	k8xS
se	se	k3xPyFc4
po	po	k7c6
provedení	provedení	k1gNnSc6
kopu	kop	k1gInSc2
pohne	pohnout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kop	kop	k1gInSc1
z	z	k7c2
rohu	roh	k1gInSc2
</s>
<s>
Rohový	rohový	k2eAgInSc4d1
kop	kop	k1gInSc4
</s>
<s>
Pokud	pokud	k8xS
je	být	k5eAaImIp3nS
hra	hra	k1gFnSc1
přerušena	přerušen	k2eAgFnSc1d1
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
míč	míč	k1gInSc1
přešel	přejít	k5eAaPmAgInS
brankovou	brankový	k2eAgFnSc4d1
čáru	čára	k1gFnSc4
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
se	se	k3xPyFc4
jej	on	k3xPp3gInSc2
naposledy	naposledy	k6eAd1
dotkl	dotknout	k5eAaPmAgMnS
hráč	hráč	k1gMnSc1
bránícího	bránící	k2eAgNnSc2d1
družstva	družstvo	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
přitom	přitom	k6eAd1
nebylo	být	k5eNaImAgNnS
dosaženo	dosáhnout	k5eAaPmNgNnS
branky	branka	k1gFnPc4
<g/>
,	,	kIx,
naváže	navázat	k5eAaPmIp3nS
se	se	k3xPyFc4
hra	hra	k1gFnSc1
kopem	kop	k1gInSc7
z	z	k7c2
rohu	roh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přímo	přímo	k6eAd1
z	z	k7c2
kopu	kop	k1gInSc2
z	z	k7c2
rohu	roh	k1gInSc2
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
dosáhnout	dosáhnout	k5eAaPmF
branky	branka	k1gFnPc4
(	(	kIx(
<g/>
ovšem	ovšem	k9
ne	ne	k9
vlastní	vlastní	k2eAgFnSc3d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kop	kop	k1gInSc1
se	se	k3xPyFc4
provede	provést	k5eAaPmIp3nS
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
míč	míč	k1gInSc1
položí	položit	k5eAaPmIp3nS
na	na	k7c4
libovolné	libovolný	k2eAgNnSc4d1
místo	místo	k1gNnSc4
uvnitř	uvnitř	k7c2
rohového	rohový	k2eAgInSc2d1
čtvrtkruhu	čtvrtkruh	k1gInSc2
u	u	k7c2
nejbližšího	blízký	k2eAgInSc2d3
rohového	rohový	k2eAgInSc2d1
praporku	praporek	k1gInSc2
a	a	k8xC
libovolný	libovolný	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
útočícího	útočící	k2eAgNnSc2d1
mužstva	mužstvo	k1gNnSc2
jej	on	k3xPp3gMnSc4
rozehraje	rozehrát	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
se	se	k3xPyFc4
nesmí	smět	k5eNaImIp3nS
míče	míč	k1gInSc2
znovu	znovu	k6eAd1
dotknout	dotknout	k5eAaPmF
dříve	dříve	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
jej	on	k3xPp3gMnSc4
dotkne	dotknout	k5eAaPmIp3nS
jiný	jiný	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokud	dokud	k8xS
míč	míč	k1gInSc1
není	být	k5eNaImIp3nS
ve	v	k7c6
hře	hra	k1gFnSc6
<g/>
,	,	kIx,
nesmí	smět	k5eNaImIp3nS
se	se	k3xPyFc4
žádný	žádný	k3yNgMnSc1
hráč	hráč	k1gMnSc1
soupeře	soupeř	k1gMnSc4
přiblížit	přiblížit	k5eAaPmF
k	k	k7c3
míči	míč	k1gInSc3
na	na	k7c4
vzdálenost	vzdálenost	k1gFnSc4
menší	malý	k2eAgFnSc4d2
než	než	k8xS
9,15	9,15	k4
m.	m.	k?
Míč	míč	k1gInSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
hře	hra	k1gFnSc6
<g/>
,	,	kIx,
jakmile	jakmile	k8xS
se	se	k3xPyFc4
po	po	k7c6
provedení	provedení	k1gNnSc6
kopu	kop	k1gInSc2
pohne	pohnout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Asociace	asociace	k1gFnSc1
</s>
<s>
Fotbal	fotbal	k1gInSc1
(	(	kIx(
<g/>
stejně	stejně	k6eAd1
tak	tak	k9
i	i	k9
některé	některý	k3yIgInPc4
příbuzné	příbuzný	k2eAgInPc4d1
sporty	sport	k1gInPc4
jako	jako	k9
futsal	futsat	k5eAaPmAgInS,k5eAaImAgInS
a	a	k8xC
plážový	plážový	k2eAgInSc1d1
fotbal	fotbal	k1gInSc1
<g/>
)	)	kIx)
spravuje	spravovat	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
federace	federace	k1gFnSc1
FIFA	FIFA	kA
(	(	kIx(
<g/>
Fédération	Fédération	k1gInSc1
Internationale	Internationale	k1gFnSc2
de	de	k?
Football	Football	k1gInSc1
Association	Association	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
sdružuje	sdružovat	k5eAaImIp3nS
národní	národní	k2eAgFnPc4d1
fotbalové	fotbalový	k2eAgFnPc4d1
asociace	asociace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FIFA	FIFA	kA
pod	pod	k7c7
sebou	se	k3xPyFc7
má	mít	k5eAaImIp3nS
šest	šest	k4xCc4
regionálních	regionální	k2eAgFnPc2d1
konfederací	konfederace	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
Evropa	Evropa	k1gFnSc1
<g/>
:	:	kIx,
UEFA	UEFA	kA
(	(	kIx(
<g/>
Union	union	k1gInSc1
of	of	k?
European	European	k1gInSc1
Football	Football	k1gMnSc1
Associations	Associations	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
<g/>
:	:	kIx,
CONMEBOL	CONMEBOL	kA
(	(	kIx(
<g/>
Confederación	Confederación	k1gMnSc1
Sudamericana	Sudamerican	k1gMnSc2
de	de	k?
Fútbol	Fútbol	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Severní	severní	k2eAgFnSc1d1
a	a	k8xC
střední	střední	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
a	a	k8xC
Karibik	Karibik	k1gMnSc1
<g/>
:	:	kIx,
CONCACAF	CONCACAF	kA
(	(	kIx(
<g/>
Confederation	Confederation	k1gInSc1
of	of	k?
North	North	k1gMnSc1
<g/>
,	,	kIx,
Central	Central	k1gMnSc1
American	American	k1gMnSc1
and	and	k?
Caribbean	Caribbean	k1gInSc1
Association	Association	k1gInSc1
Football	Football	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Afrika	Afrika	k1gFnSc1
<g/>
:	:	kIx,
CAF	CAF	kA
(	(	kIx(
<g/>
Confederation	Confederation	k1gInSc1
of	of	k?
African	African	k1gInSc1
Football	Football	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Asie	Asie	k1gFnSc1
<g/>
:	:	kIx,
AFC	AFC	kA
(	(	kIx(
<g/>
Asian	Asian	k1gMnSc1
Football	Football	k1gMnSc1
Confederation	Confederation	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Oceánie	Oceánie	k1gFnSc1
<g/>
:	:	kIx,
OFC	OFC	kA
(	(	kIx(
<g/>
Oceania	Oceanium	k1gNnPc1
Football	Footballa	k1gFnPc2
Confederation	Confederation	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
jednotlivých	jednotlivý	k2eAgInPc6d1
státech	stát	k1gInPc6
(	(	kIx(
<g/>
či	či	k8xC
jejich	jejich	k3xOp3gFnPc6
částech	část	k1gFnPc6
<g/>
)	)	kIx)
fotbal	fotbal	k1gInSc4
spravují	spravovat	k5eAaImIp3nP
příslušné	příslušný	k2eAgFnPc1d1
národní	národní	k2eAgFnPc1d1
asociace	asociace	k1gFnPc1
<g/>
;	;	kIx,
v	v	k7c6
Česku	Česko	k1gNnSc6
je	být	k5eAaImIp3nS
tímto	tento	k3xDgInSc7
orgánem	orgán	k1gInSc7
Fotbalová	fotbalový	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
FAČR	FAČR	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Samotná	samotný	k2eAgNnPc4d1
pravidla	pravidlo	k1gNnPc4
fotbalu	fotbal	k1gInSc2
neudržuje	udržovat	k5eNaImIp3nS
přímo	přímo	k6eAd1
FIFA	FIFA	kA
<g/>
,	,	kIx,
ale	ale	k8xC
specializovaná	specializovaný	k2eAgFnSc1d1
mezinárodní	mezinárodní	k2eAgFnSc1d1
komise	komise	k1gFnSc1
IFAB	IFAB	kA
(	(	kIx(
<g/>
International	International	k1gFnSc1
Football	Football	k1gInSc4
Association	Association	k1gInSc1
Board	Board	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Soutěže	soutěž	k1gFnPc1
</s>
<s>
Ve	v	k7c6
fotbalu	fotbal	k1gInSc6
probíhá	probíhat	k5eAaImIp3nS
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
soutěží	soutěž	k1gFnPc2
jak	jak	k8xC,k8xS
mezinárodních	mezinárodní	k2eAgInPc2d1
<g/>
,	,	kIx,
tak	tak	k9
v	v	k7c6
jednotlivých	jednotlivý	k2eAgInPc6d1
státech	stát	k1gInPc6
či	či	k8xC
jejich	jejich	k3xOp3gFnPc6
částech	část	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
</s>
<s>
Na	na	k7c6
mezinárodní	mezinárodní	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
spolu	spolu	k6eAd1
zápasí	zápasit	k5eAaImIp3nS
jednak	jednak	k8xC
reprezentační	reprezentační	k2eAgNnPc4d1
družstva	družstvo	k1gNnPc4
jednotlivých	jednotlivý	k2eAgInPc2d1
států	stát	k1gInPc2
(	(	kIx(
<g/>
téměř	téměř	k6eAd1
všechny	všechen	k3xTgInPc1
státy	stát	k1gInPc1
mají	mít	k5eAaImIp3nP
vlastní	vlastní	k2eAgFnSc2d1
fotbalové	fotbalový	k2eAgFnSc2d1
reprezentace	reprezentace	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jednak	jednak	k8xC
soukromé	soukromý	k2eAgInPc4d1
fotbalové	fotbalový	k2eAgInPc4d1
kluby	klub	k1gInPc4
z	z	k7c2
různých	různý	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc7d3
mezinárodní	mezinárodní	k2eAgFnSc7d1
soutěží	soutěž	k1gFnSc7
reprezentací	reprezentace	k1gFnSc7
je	být	k5eAaImIp3nS
mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
<g/>
,	,	kIx,
pořádané	pořádaný	k2eAgFnSc6d1
každé	každý	k3xTgInPc4
čtyři	čtyři	k4xCgInPc4
roky	rok	k1gInPc4
<g/>
,	,	kIx,
na	na	k7c6
jehož	jehož	k3xOyRp3gInSc6
závěrečném	závěrečný	k2eAgInSc6d1
turnaji	turnaj	k1gInSc6
soutěží	soutěž	k1gFnPc2
32	#num#	k4
družstev	družstvo	k1gNnPc2
<g/>
,	,	kIx,
vybraných	vybraný	k2eAgFnPc2d1
kvalifikací	kvalifikace	k1gFnPc2
z	z	k7c2
více	hodně	k6eAd2
než	než	k8xS
200	#num#	k4
členských	členský	k2eAgInPc2d1
států	stát	k1gInPc2
FIFA	FIFA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fotbalový	fotbalový	k2eAgInSc1d1
turnaj	turnaj	k1gInSc1
je	být	k5eAaImIp3nS
také	také	k9
součástí	součást	k1gFnSc7
programu	program	k1gInSc2
letních	letní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
<g/>
;	;	kIx,
v	v	k7c6
současnosti	současnost	k1gFnSc6
se	se	k3xPyFc4
však	však	k9
tohoto	tento	k3xDgInSc2
turnaje	turnaj	k1gInSc2
účastní	účastnit	k5eAaImIp3nP
mládežnická	mládežnický	k2eAgNnPc1d1
mužstva	mužstvo	k1gNnPc1
hráčů	hráč	k1gMnPc2
do	do	k7c2
23	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
takže	takže	k8xS
turnaj	turnaj	k1gInSc1
nemá	mít	k5eNaImIp3nS
tak	tak	k6eAd1
vysokou	vysoký	k2eAgFnSc4d1
prestiž	prestiž	k1gFnSc4
jako	jako	k8xC,k8xS
mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
mistrovství	mistrovství	k1gNnSc6
světa	svět	k1gInSc2
nejvýznamnějšími	významný	k2eAgFnPc7d3
reprezentačními	reprezentační	k2eAgFnPc7d1
soutěžemi	soutěž	k1gFnPc7
jsou	být	k5eAaImIp3nP
regionální	regionální	k2eAgNnSc4d1
mistrovství	mistrovství	k1gNnSc4
pořádaná	pořádaný	k2eAgFnSc1d1
jednotlivými	jednotlivý	k2eAgFnPc7d1
konfederacemi	konfederace	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c6
mistrovství	mistrovství	k1gNnSc6
Evropy	Evropa	k1gFnSc2
(	(	kIx(
<g/>
EURO	euro	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Copa	Cop	k2eAgFnSc1d1
América	América	k1gFnSc1
<g/>
,	,	kIx,
Africký	africký	k2eAgInSc1d1
pohár	pohár	k1gInSc1
národů	národ	k1gInPc2
<g/>
,	,	kIx,
Mistrovství	mistrovství	k1gNnSc1
Asie	Asie	k1gFnSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
<g/>
,	,	kIx,
Zlatý	zlatý	k2eAgInSc1d1
pohár	pohár	k1gInSc1
CONCACAF	CONCACAF	kA
a	a	k8xC
Oceánský	oceánský	k2eAgInSc4d1
pohár	pohár	k1gInSc4
národů	národ	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Regionální	regionální	k2eAgFnSc1d1
konfederace	konfederace	k1gFnSc1
také	také	k9
pořádají	pořádat	k5eAaImIp3nP
nejvýznamnější	významný	k2eAgFnSc2d3
klubové	klubový	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těch	ten	k3xDgMnPc2
se	se	k3xPyFc4
účastní	účastnit	k5eAaImIp3nP
vítězové	vítěz	k1gMnPc1
národních	národní	k2eAgFnPc2d1
lig	liga	k1gFnPc2
a	a	k8xC
pohárů	pohár	k1gInPc2
(	(	kIx(
<g/>
případně	případně	k6eAd1
také	také	k9
účastníci	účastník	k1gMnPc1
na	na	k7c6
dalších	další	k2eAgNnPc6d1
místech	místo	k1gNnPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sem	sem	k6eAd1
patří	patřit	k5eAaImIp3nS
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
a	a	k8xC
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
Pohár	pohár	k1gInSc1
osvoboditelů	osvoboditel	k1gMnPc2
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
,	,	kIx,
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
AFC	AFC	kA
v	v	k7c6
Asii	Asie	k1gFnSc6
<g/>
,	,	kIx,
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
CAF	CAF	kA
v	v	k7c6
Africe	Afrika	k1gFnSc6
<g/>
,	,	kIx,
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
CONCACAF	CONCACAF	kA
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
a	a	k8xC
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
OFC	OFC	kA
v	v	k7c6
Oceánii	Oceánie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítěz	vítěz	k1gMnSc1
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
a	a	k8xC
Poháru	pohár	k1gInSc2
osvoboditelů	osvoboditel	k1gMnPc2
se	se	k3xPyFc4
každoročně	každoročně	k6eAd1
utkávali	utkávat	k5eAaImAgMnP
v	v	k7c6
zápase	zápas	k1gInSc6
o	o	k7c4
Interkontinentální	interkontinentální	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
jej	on	k3xPp3gInSc4
však	však	k9
nahradilo	nahradit	k5eAaPmAgNnS
mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
fotbalových	fotbalový	k2eAgInPc2d1
klubů	klub	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Národní	národní	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
</s>
<s>
Většina	většina	k1gFnSc1
národních	národní	k2eAgFnPc2d1
asociací	asociace	k1gFnPc2
pořádá	pořádat	k5eAaImIp3nS
národní	národní	k2eAgFnPc4d1
soutěže	soutěž	k1gFnPc4
<g/>
,	,	kIx,
ve	v	k7c6
větších	veliký	k2eAgInPc6d2
státech	stát	k1gInPc6
se	se	k3xPyFc4
zpravidla	zpravidla	k6eAd1
jedná	jednat	k5eAaImIp3nS
nejméně	málo	k6eAd3
o	o	k7c4
dvě	dva	k4xCgFnPc4
soutěže	soutěž	k1gFnPc4
lišící	lišící	k2eAgFnPc4d1
se	se	k3xPyFc4
formátem	formát	k1gInSc7
<g/>
:	:	kIx,
jednou	jednou	k6eAd1
je	být	k5eAaImIp3nS
ligová	ligový	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
rozdělená	rozdělený	k2eAgFnSc1d1
podle	podle	k7c2
výkonnosti	výkonnost	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
každé	každý	k3xTgFnSc6
výkonnostní	výkonnostní	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
se	se	k3xPyFc4
hraje	hrát	k5eAaImIp3nS
systémem	systém	k1gInSc7
„	„	k?
<g/>
každý	každý	k3xTgMnSc1
s	s	k7c7
každým	každý	k3xTgMnSc7
<g/>
“	“	k?
a	a	k8xC
o	o	k7c6
vítězi	vítěz	k1gMnSc6
rozhodují	rozhodovat	k5eAaImIp3nP
body	bod	k1gInPc4
v	v	k7c6
tabulce	tabulka	k1gFnSc6
zápasů	zápas	k1gInPc2
<g/>
;	;	kIx,
nejvýše	vysoce	k6eAd3,k6eAd1
umístěné	umístěný	k2eAgInPc1d1
týmy	tým	k1gInPc1
mohou	moct	k5eAaImIp3nP
postoupit	postoupit	k5eAaPmF
do	do	k7c2
vyšší	vysoký	k2eAgFnSc2d2
soutěže	soutěž	k1gFnSc2
<g/>
,	,	kIx,
nejnižší	nízký	k2eAgMnSc1d3
naopak	naopak	k6eAd1
sestoupit	sestoupit	k5eAaPmF
o	o	k7c4
úroveň	úroveň	k1gFnSc4
níže	nízce	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhou	druhý	k4xOgFnSc4
soutěží	soutěž	k1gFnPc2
bývá	bývat	k5eAaImIp3nS
národní	národní	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
,	,	kIx,
kterého	který	k3yRgNnSc2,k3yQgNnSc2,k3yIgNnSc2
se	se	k3xPyFc4
za	za	k7c2
rovných	rovný	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
mohou	moct	k5eAaImIp3nP
účastnit	účastnit	k5eAaImF
všechny	všechen	k3xTgInPc1
týmy	tým	k1gInPc1
splňující	splňující	k2eAgFnSc4d1
jistou	jistý	k2eAgFnSc4d1
minimální	minimální	k2eAgFnSc4d1
výkonnostní	výkonnostní	k2eAgFnSc4d1
úroveň	úroveň	k1gFnSc4
<g/>
,	,	kIx,
hraje	hrát	k5eAaImIp3nS
se	se	k3xPyFc4
vyřazovacím	vyřazovací	k2eAgInSc7d1
systémem	systém	k1gInSc7
–	–	k?
vítěz	vítěz	k1gMnSc1
postupuje	postupovat	k5eAaImIp3nS
do	do	k7c2
dalšího	další	k2eAgNnSc2d1
kola	kolo	k1gNnSc2
soutěže	soutěž	k1gFnSc2
<g/>
,	,	kIx,
tým	tým	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
prohrál	prohrát	k5eAaPmAgInS
<g/>
,	,	kIx,
v	v	k7c6
soutěži	soutěž	k1gFnSc6
pro	pro	k7c4
daný	daný	k2eAgInSc4d1
ročník	ročník	k1gInSc4
končí	končit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Česku	Česko	k1gNnSc6
je	být	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgInSc1d3
ligovou	ligový	k2eAgFnSc7d1
soutěží	soutěžit	k5eAaImIp3nS
první	první	k4xOgFnSc1
liga	liga	k1gFnSc1
<g/>
,	,	kIx,
vedle	vedle	k7c2
toho	ten	k3xDgNnSc2
existuje	existovat	k5eAaImIp3nS
pohár	pohár	k1gInSc1
(	(	kIx(
<g/>
v	v	k7c6
současnosti	současnost	k1gFnSc6
označovaný	označovaný	k2eAgInSc1d1
jako	jako	k8xC,k8xS
MOL	mol	k1gInSc1
Cup	cup	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
nejslavnější	slavný	k2eAgFnPc4d3
ligové	ligový	k2eAgFnPc4d1
soutěže	soutěž	k1gFnPc4
světa	svět	k1gInSc2
patří	patřit	k5eAaImIp3nS
anglická	anglický	k2eAgFnSc1d1
Premier	Premier	k1gInSc4
League	Leagu	k1gInSc2
<g/>
,	,	kIx,
španělská	španělský	k2eAgFnSc1d1
Primera	primera	k1gFnSc1
división	división	k1gInSc1
<g/>
,	,	kIx,
italská	italský	k2eAgFnSc1d1
Serie	serie	k1gFnSc1
A	A	kA
<g/>
,	,	kIx,
německá	německý	k2eAgFnSc1d1
Bundesliga	bundesliga	k1gFnSc1
<g/>
,	,	kIx,
francouzská	francouzský	k2eAgFnSc1d1
Ligue	Ligue	k1gFnSc1
1	#num#	k4
Orange	Orange	k1gFnPc2
či	či	k8xC
brazilská	brazilský	k2eAgFnSc1d1
Série	série	k1gFnSc1
A.	A.	kA
</s>
<s>
Mezi	mezi	k7c4
známé	známý	k2eAgInPc4d1
národní	národní	k2eAgInPc4d1
poháry	pohár	k1gInPc4
patří	patřit	k5eAaImIp3nS
například	například	k6eAd1
anglický	anglický	k2eAgMnSc1d1
FA	fa	kA
Cup	cup	k1gInSc4
<g/>
,	,	kIx,
nejstarší	starý	k2eAgFnSc1d3
fotbalová	fotbalový	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Systém	systém	k1gInSc1
soutěží	soutěž	k1gFnPc2
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Soutěže	soutěž	k1gFnPc1
mužských	mužský	k2eAgInPc2d1
fotbalových	fotbalový	k2eAgInPc2d1
klubů	klub	k1gInPc2
v	v	k7c6
Česku	Česko	k1gNnSc6
jsou	být	k5eAaImIp3nP
rozděleny	rozdělit	k5eAaPmNgInP
do	do	k7c2
několika	několik	k4yIc2
úrovní	úroveň	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
nejvyšší	vysoký	k2eAgFnSc6d3
úrovni	úroveň	k1gFnSc6
jsou	být	k5eAaImIp3nP
celostátní	celostátní	k2eAgFnPc1d1
profesionální	profesionální	k2eAgFnPc1d1
soutěže	soutěž	k1gFnPc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c6
nejnižší	nízký	k2eAgFnSc6d3
úrovni	úroveň	k1gFnSc6
jsou	být	k5eAaImIp3nP
III	III	kA
<g/>
.	.	kIx.
a	a	k8xC
IV	IV	kA
<g/>
.	.	kIx.
třídy	třída	k1gFnPc1
<g/>
,	,	kIx,
organizované	organizovaný	k2eAgFnPc1d1
regionálně	regionálně	k6eAd1
(	(	kIx(
<g/>
okresními	okresní	k2eAgInPc7d1
fotbalovými	fotbalový	k2eAgInPc7d1
svazy	svaz	k1gInPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Systém	systém	k1gInSc1
českých	český	k2eAgFnPc2d1
klubových	klubový	k2eAgFnPc2d1
soutěží	soutěž	k1gFnPc2
mužů	muž	k1gMnPc2
</s>
<s>
Soutěže	soutěž	k1gFnPc1
řízené	řízený	k2eAgInPc1d1
Ligovou	ligový	k2eAgFnSc7d1
fotbalovou	fotbalový	k2eAgFnSc7d1
asociací	asociace	k1gFnSc7
(	(	kIx(
<g/>
profesionální	profesionální	k2eAgMnPc1d1
<g/>
)	)	kIx)
</s>
<s>
1.1	1.1	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
Národní	národní	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
Soutěže	soutěž	k1gFnPc1
řízené	řízený	k2eAgFnSc2d1
řídící	řídící	k2eAgFnPc4d1
komisí	komise	k1gFnSc7
pro	pro	k7c4
ČechySoutěže	ČechySoutěž	k1gFnPc4
řízené	řízený	k2eAgNnSc1d1
řídící	řídící	k2eAgFnSc7d1
komisí	komise	k1gFnSc7
pro	pro	k7c4
Moravu	Morava	k1gFnSc4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
Česká	český	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
ligaMoravskoslezská	ligaMoravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
<g/>
Divize	divize	k1gFnSc2
ADivize	ADivize	k1gFnSc2
BDivize	BDivize	k1gFnSc2
CDivize	CDivize	k1gFnSc2
DDivize	DDivize	k1gFnSc2
E	E	kA
</s>
<s>
Soutěže	soutěž	k1gFnPc4
řízené	řízený	k2eAgFnPc4d1
krajskými	krajský	k2eAgInPc7d1
fotbalovými	fotbalový	k2eAgInPc7d1
svazy	svaz	k1gInPc7
a	a	k8xC
Pražským	pražský	k2eAgInSc7d1
fotbalovým	fotbalový	k2eAgInSc7d1
svazem	svaz	k1gInSc7
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
<g/>
Krajské	krajský	k2eAgInPc1d1
přebory	přebor	k1gInPc1
a	a	k8xC
Pražský	pražský	k2eAgInSc1d1
přebor	přebor	k1gInSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
<g/>
Fotbalové	fotbalový	k2eAgFnSc2d1
I.	I.	kA
A	a	k8xC
třídy	třída	k1gFnSc2
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
<g/>
Fotbalové	fotbalový	k2eAgInPc4d1
I.	I.	kA
B	B	kA
třídy	třída	k1gFnSc2
</s>
<s>
Soutěže	soutěž	k1gFnPc4
řízené	řízený	k2eAgFnPc4d1
okresními	okresní	k2eAgInPc7d1
fotbalovými	fotbalový	k2eAgInPc7d1
svazy	svaz	k1gInPc7
a	a	k8xC
v	v	k7c6
Praze	Praha	k1gFnSc6
Pražským	pražský	k2eAgInSc7d1
fotbalovým	fotbalový	k2eAgInSc7d1
svazem	svaz	k1gInSc7
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
<g/>
II	II	kA
<g/>
.	.	kIx.
třídy	třída	k1gFnSc2
(	(	kIx(
<g/>
okresní	okresní	k2eAgInPc1d1
přebory	přebor	k1gInPc1
<g/>
)	)	kIx)
a	a	k8xC
Pražská	pražský	k2eAgFnSc1d1
II	II	kA
<g/>
.	.	kIx.
třída	třída	k1gFnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
<g/>
III	III	kA
<g/>
.	.	kIx.
třídy	třída	k1gFnSc2
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
<g/>
IV	IV	kA
<g/>
.	.	kIx.
třídy	třída	k1gFnSc2
(	(	kIx(
<g/>
není	být	k5eNaImIp3nS
ve	v	k7c6
všech	všecek	k3xTgInPc6
okresech	okres	k1gInPc6
<g/>
)	)	kIx)
</s>
<s>
Pohárovou	pohárový	k2eAgFnSc7d1
soutěží	soutěž	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
hraje	hrát	k5eAaImIp3nS
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
Pohár	pohár	k1gInSc1
FAČR	FAČR	kA
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Pohár	pohár	k1gInSc1
ČMFS	ČMFS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
INFOGRAFIKA	INFOGRAFIKA	kA
<g/>
:	:	kIx,
Vývoj	vývoj	k1gInSc1
fotbalová	fotbalový	k2eAgFnSc1d1
taktiky	taktika	k1gFnPc1
<g/>
,	,	kIx,
aneb	aneb	k?
jak	jak	k6eAd1
porazit	porazit	k5eAaPmF
Portugalsko	Portugalsko	k1gNnSc1
<g/>
↑	↑	k?
FAČR	FAČR	kA
<g/>
:	:	kIx,
Soutěžní	soutěžní	k2eAgInSc1d1
řád	řád	k1gInSc1
fotbalu	fotbal	k1gInSc2
Archivováno	archivovat	k5eAaBmNgNnS
3	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
článek	článek	k1gInSc1
42	#num#	k4
<g/>
,	,	kIx,
Práva	právo	k1gNnSc2
a	a	k8xC
povinnosti	povinnost	k1gFnSc2
delegovaných	delegovaný	k2eAgMnPc2d1
rozhodčích	rozhodčí	k1gMnPc2
<g/>
↑	↑	k?
Pravidla	pravidlo	k1gNnPc4
fotbalu	fotbal	k1gInSc2
malých	malý	k2eAgFnPc2d1
forem	forma	k1gFnPc2
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
ŽURMAN	ŽURMAN	kA
<g/>
,	,	kIx,
Oldřich	Oldřich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zlatá	zlatý	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
kopané	kopaná	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolupracovali	spolupracovat	k5eAaImAgMnP
V.	V.	kA
Folprecht	Folprecht	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
Pechr	Pechr	k1gInSc1
a	a	k8xC
Z.	Z.	kA
Žurman	Žurman	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Olympia	Olympia	k1gFnSc1
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
.	.	kIx.
384	#num#	k4
s.	s.	k?
S	s	k7c7
92	#num#	k4
str	str	kA
<g/>
.	.	kIx.
obr	obr	k1gMnSc1
<g/>
.	.	kIx.
příloh	příloha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
PROCHÁZKA	Procházka	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fotbal	fotbal	k1gInSc1
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
hra	hra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Světový	světový	k2eAgInSc1d1
fotbal	fotbal	k1gInSc1
v	v	k7c6
obrazech	obraz	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obrazová	obrazový	k2eAgFnSc1d1
část	část	k1gFnSc1
spolupr	spolupra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bohumil	Bohumil	k1gMnSc1
Miran	Miran	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Olympia	Olympia	k1gFnSc1
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
.	.	kIx.
359	#num#	k4
s.	s.	k?
</s>
<s>
VANĚK	Vaněk	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malá	malý	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
fotbalu	fotbal	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Karel	Karel	k1gMnSc1
Vaněk	Vaněk	k1gMnSc1
<g/>
;	;	kIx,
foto	foto	k1gNnSc1
E.	E.	kA
Fafek	Fafek	k6eAd1
<g/>
,	,	kIx,
K.	K.	kA
Kučera	Kučera	k1gMnSc1
<g/>
,	,	kIx,
K.	K.	kA
Novák	Novák	k1gMnSc1
a	a	k8xC
d.	d.	k?
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Olympia	Olympia	k1gFnSc1
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
.	.	kIx.
404	#num#	k4
s.	s.	k?
</s>
<s>
PONDĚLÍK	PONDĚLÍK	kA
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Století	století	k1gNnSc1
fotbalu	fotbal	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
dějin	dějiny	k1gFnPc2
československé	československý	k2eAgFnSc2d1
kopané	kopaná	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokument	dokument	k1gInSc1
<g/>
.	.	kIx.
část	část	k1gFnSc1
Jindřich	Jindřich	k1gMnSc1
Pejchar	Pejchar	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Foto	foto	k1gNnSc1
J.	J.	kA
Koliš	Koliš	k1gInSc1
<g/>
,	,	kIx,
K.	K.	kA
Novák	Novák	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
Skála	Skála	k1gMnSc1
<g/>
,	,	kIx,
E.	E.	kA
Fafek	Fafek	k1gInSc1
a	a	k8xC
d.	d.	k?
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Olympia	Olympia	k1gFnSc1
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
.	.	kIx.
412	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Olymp	Olymp	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
176	#num#	k4
<g/>
]	]	kIx)
s.	s.	k?
obr	obr	k1gMnSc1
<g/>
.	.	kIx.
příl	příl	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
ROHR	ROHR	kA
<g/>
,	,	kIx,
Bernd	Bernd	k1gMnSc1
<g/>
;	;	kIx,
SIMON	Simon	k1gMnSc1
<g/>
,	,	kIx,
Günter	Günter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fotbal	fotbal	k1gInSc1
<g/>
:	:	kIx,
velký	velký	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
<g/>
:	:	kIx,
osobnosti	osobnost	k1gFnPc1
<g/>
,	,	kIx,
kluby	klub	k1gInPc1
<g/>
,	,	kIx,
názvosloví	názvosloví	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Luboš	Luboš	k1gMnSc1
Jeřábek	Jeřábek	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
494	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
247	#num#	k4
<g/>
-	-	kIx~
<g/>
1158	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
z	z	k7c2
němčiny	němčina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Upravil	upravit	k5eAaPmAgMnS
a	a	k8xC
doplnil	doplnit	k5eAaPmAgMnS
L.	L.	kA
Jeřábek	Jeřábek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
DOUGLAS	DOUGLAS	kA
<g/>
,	,	kIx,
Ian	Ian	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fotbal	fotbal	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Ludmila	Ludmila	k1gFnSc1
Štěrbová	Štěrbová	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plzeň	Plzeň	k1gFnSc1
<g/>
:	:	kIx,
Fraus	fraus	k1gFnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Čti	číst	k5eAaImRp2nS
<g/>
+	+	kIx~
<g/>
;	;	kIx,
sv.	sv.	kA
11	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7238	#num#	k4
<g/>
-	-	kIx~
<g/>
808	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
z	z	k7c2
angl.	angl.	k?
Publikace	publikace	k1gFnPc1
pro	pro	k7c4
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
DLABÁČEK	DLABÁČEK	kA
<g/>
,	,	kIx,
Vojtěch	Vojtěch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Abeceda	abeceda	k1gFnSc1
fotbalu	fotbal	k1gInSc2
<g/>
:	:	kIx,
metodika	metodika	k1gFnSc1
výuky	výuka	k1gFnSc2
pro	pro	k7c4
I.	I.	kA
stupeň	stupeň	k1gInSc4
ZŠ	ZŠ	kA
i	i	k8xC
starší	starý	k2eAgMnPc4d2
začátečníky	začátečník	k1gMnPc4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
upr	upr	k?
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
:	:	kIx,
Gaudeamus	Gaudeamus	k1gMnSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
56	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7041	#num#	k4
<g/>
-	-	kIx~
<g/>
922	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
TRUCHLÍK	TRUCHLÍK	kA
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
<g/>
;	;	kIx,
ZEMAN	Zeman	k1gMnSc1
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyklopedie	encyklopedie	k1gFnSc1
fotbalu	fotbal	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ottovo	Ottův	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
336	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7451	#num#	k4
<g/>
-	-	kIx~
<g/>
245	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Australský	australský	k2eAgInSc1d1
fotbal	fotbal	k1gInSc1
</s>
<s>
Americký	americký	k2eAgInSc1d1
fotbal	fotbal	k1gInSc1
</s>
<s>
Nohejbal	nohejbal	k1gInSc1
</s>
<s>
Sálová	sálový	k2eAgFnSc1d1
kopaná	kopaná	k1gFnSc1
</s>
<s>
Plážový	plážový	k2eAgInSc1d1
fotbal	fotbal	k1gInSc1
</s>
<s>
Přátelské	přátelský	k2eAgNnSc1d1
utkání	utkání	k1gNnSc1
</s>
<s>
Ragby	ragby	k1gNnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
fotbal	fotbal	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Téma	téma	k1gNnSc1
Fotbal	fotbal	k1gInSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
fotbal	fotbal	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Kategorie	kategorie	k1gFnSc1
Fotbal	fotbal	k1gInSc1
ve	v	k7c6
Wikizprávách	Wikizpráva	k1gFnPc6
</s>
<s>
www.fifa.com	www.fifa.com	k1gInSc4
–	–	k?
oficiální	oficiální	k2eAgFnSc2d1
stránky	stránka	k1gFnSc2
mezinárodní	mezinárodní	k2eAgFnSc2d1
fotbalové	fotbalový	k2eAgFnSc2d1
federace	federace	k1gFnSc2
FIFA	FIFA	kA
</s>
<s>
www.uefa.com	www.uefa.com	k1gInSc4
–	–	k?
oficiální	oficiální	k2eAgFnSc2d1
stránky	stránka	k1gFnSc2
evropské	evropský	k2eAgFnSc2d1
fotbalové	fotbalový	k2eAgFnSc2d1
federace	federace	k1gFnSc2
UEFA	UEFA	kA
</s>
<s>
www.fotbal.cz	www.fotbal.cz	k1gInSc4
–	–	k?
oficiální	oficiální	k2eAgFnSc2d1
stránky	stránka	k1gFnSc2
Fotbalové	fotbalový	k2eAgFnSc2d1
asociace	asociace	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
FAČR	FAČR	kA
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Mezinárodní	mezinárodní	k2eAgInSc1d1
fotbal	fotbal	k1gInSc1
FIFA	FIFA	kA
•	•	k?
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
•	•	k?
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
žen	žena	k1gFnPc2
•	•	k?
Konfederační	konfederační	k2eAgInSc4d1
pohár	pohár	k1gInSc4
•	•	k?
MS	MS	kA
do	do	k7c2
20	#num#	k4
let	léto	k1gNnPc2
•	•	k?
MS	MS	kA
do	do	k7c2
17	#num#	k4
let	léto	k1gNnPc2
•	•	k?
MS	MS	kA
žen	žena	k1gFnPc2
do	do	k7c2
20	#num#	k4
let	léto	k1gNnPc2
•	•	k?
MS	MS	kA
žen	žena	k1gFnPc2
do	do	k7c2
17	#num#	k4
let	léto	k1gNnPc2
•	•	k?
MS	MS	kA
ve	v	k7c6
futsalu	futsal	k1gInSc6
•	•	k?
MS	MS	kA
v	v	k7c6
plážovém	plážový	k2eAgInSc6d1
fotbale	fotbal	k1gInSc6
•	•	k?
Olympijské	olympijský	k2eAgFnPc1d1
hry	hra	k1gFnPc1
•	•	k?
Asijské	asijský	k2eAgFnPc1d1
hry	hra	k1gFnPc1
•	•	k?
Africké	africký	k2eAgFnPc1d1
hry	hra	k1gFnPc1
•	•	k?
Pan	Pan	k1gMnSc1
Americké	americký	k2eAgFnSc2d1
hry	hra	k1gFnSc2
•	•	k?
Ostrovní	ostrovní	k2eAgFnSc2d1
hry	hra	k1gFnSc2
•	•	k?
Žebříček	žebříček	k1gInSc1
FIFA	FIFA	kA
•	•	k?
Fotbalista	fotbalista	k1gMnSc1
roku	rok	k1gInSc2
podle	podle	k7c2
FIFA	FIFA	kA
•	•	k?
Reprezentace	reprezentace	k1gFnSc1
•	•	k?
Kódy	kód	k1gInPc1
Asie	Asie	k1gFnSc1
</s>
<s>
AFC	AFC	kA
–	–	k?
Mistrovství	mistrovství	k1gNnSc1
Asie	Asie	k1gFnSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
Afrika	Afrika	k1gFnSc1
</s>
<s>
CAF	CAF	kA
–	–	k?
Africký	africký	k2eAgInSc4d1
pohár	pohár	k1gInSc4
národů	národ	k1gInPc2
Severní	severní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
</s>
<s>
CONCACAF	CONCACAF	kA
–	–	k?
Zlatý	zlatý	k2eAgInSc4d1
pohár	pohár	k1gInSc4
CONCACAF	CONCACAF	kA
Jižní	jižní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
</s>
<s>
CONMEBOL	CONMEBOL	kA
–	–	k?
Copa	Cop	k2eAgFnSc1d1
América	América	k1gFnSc1
Oceánie	Oceánie	k1gFnSc2
</s>
<s>
OFC	OFC	kA
–	–	k?
Oceánský	oceánský	k2eAgInSc4d1
pohár	pohár	k1gInSc4
národů	národ	k1gInPc2
Evropa	Evropa	k1gFnSc1
</s>
<s>
UEFA	UEFA	kA
–	–	k?
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
mimo	mimo	k7c4
působnost	působnost	k1gFnSc4
FIFA	FIFA	kA
</s>
<s>
NF-Board	NF-Board	k1gInSc1
–	–	k?
VIVA	VIVA	kA
World	World	k1gInSc1
Cup	cup	k1gInSc1
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1
klubový	klubový	k2eAgInSc1d1
fotbal	fotbal	k1gInSc1
FIFA	FIFA	kA
•	•	k?
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
klubů	klub	k1gInPc2
•	•	k?
Interkontinentální	interkontinentální	k2eAgInSc4d1
pohár	pohár	k1gInSc4
(	(	kIx(
<g/>
zaniklý	zaniklý	k2eAgInSc4d1
<g/>
)	)	kIx)
•	•	k?
Fotbalista	fotbalista	k1gMnSc1
roku	rok	k1gInSc2
•	•	k?
Týmy	tým	k1gInPc1
Asie	Asie	k1gFnSc1
</s>
<s>
AFC	AFC	kA
–	–	k?
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
•	•	k?
Pohár	pohár	k1gInSc1
AFC	AFC	kA
Afrika	Afrika	k1gFnSc1
</s>
<s>
CAF	CAF	kA
–	–	k?
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
•	•	k?
Konfederační	konfederační	k2eAgInSc4d1
pohár	pohár	k1gInSc4
CAF	CAF	kA
•	•	k?
Superpohár	superpohár	k1gInSc1
CAF	CAF	kA
Severní	severní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
</s>
<s>
CONCACAF	CONCACAF	kA
–	–	k?
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
Jižní	jižní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
</s>
<s>
CONMEBOL	CONMEBOL	kA
–	–	k?
Pohár	pohár	k1gInSc1
osvoboditelů	osvoboditel	k1gMnPc2
•	•	k?
Copa	Cop	k1gInSc2
Sudamericana	Sudamerican	k1gMnSc2
•	•	k?
Recopa	Recop	k1gMnSc2
Sudamericana	Sudamerican	k1gMnSc2
Oceánie	Oceánie	k1gFnSc2
</s>
<s>
OFC	OFC	kA
–	–	k?
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
Evropa	Evropa	k1gFnSc1
</s>
<s>
UEFA	UEFA	kA
–	–	k?
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
•	•	k?
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
•	•	k?
Evropská	evropský	k2eAgFnSc1d1
konferenční	konferenční	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
•	•	k?
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
Soutěže	soutěž	k1gFnSc2
pořádané	pořádaný	k2eAgFnSc2d1
asociací	asociace	k1gFnSc7
UEFA	UEFA	kA
Reprezentační	reprezentační	k2eAgFnSc2d1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
•	•	k?
Liga	liga	k1gFnSc1
národů	národ	k1gInPc2
UEFA	UEFA	kA
•	•	k?
ME	ME	kA
do	do	k7c2
21	#num#	k4
let	léto	k1gNnPc2
•	•	k?
ME	ME	kA
do	do	k7c2
19	#num#	k4
let	léto	k1gNnPc2
•	•	k?
ME	ME	kA
do	do	k7c2
17	#num#	k4
let	léto	k1gNnPc2
•	•	k?
ME	ME	kA
žen	žena	k1gFnPc2
•	•	k?
ME	ME	kA
žen	žena	k1gFnPc2
do	do	k7c2
19	#num#	k4
let	léto	k1gNnPc2
•	•	k?
ME	ME	kA
žen	žena	k1gFnPc2
do	do	k7c2
17	#num#	k4
let	léto	k1gNnPc2
•	•	k?
ME	ME	kA
ve	v	k7c6
futsalu	futsal	k1gInSc6
•	•	k?
Meridian	Meridian	k1gInSc1
Cup	cup	k1gInSc1
(	(	kIx(
<g/>
zaniklý	zaniklý	k2eAgMnSc1d1
<g/>
)	)	kIx)
Klubové	klubový	k2eAgNnSc1d1
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
•	•	k?
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
•	•	k?
Evropská	evropský	k2eAgFnSc1d1
konferenční	konferenční	k2eAgFnSc1d1
liga	liga	k1gFnSc1
(	(	kIx(
<g/>
budoucí	budoucí	k2eAgFnSc1d1
<g/>
)	)	kIx)
•	•	k?
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
•	•	k?
Pohár	pohár	k1gInSc4
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
(	(	kIx(
<g/>
zaniklý	zaniklý	k2eAgInSc1d1
<g/>
)	)	kIx)
•	•	k?
Pohár	pohár	k1gInSc1
Intertoto	Intertota	k1gFnSc5
(	(	kIx(
<g/>
zaniklý	zaniklý	k2eAgInSc4d1
<g/>
)	)	kIx)
•	•	k?
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
ve	v	k7c4
futsalu	futsala	k1gFnSc4
•	•	k?
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
žen	žena	k1gFnPc2
Ostatní	ostatní	k2eAgFnSc2d1
významné	významný	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
<g/>
:	:	kIx,
Středoevropský	středoevropský	k2eAgInSc1d1
pohár	pohár	k1gInSc1
(	(	kIx(
<g/>
zaniklý	zaniklý	k2eAgInSc1d1
<g/>
)	)	kIx)
•	•	k?
Veletržní	veletržní	k2eAgInSc4d1
pohár	pohár	k1gInSc4
(	(	kIx(
<g/>
zaniklý	zaniklý	k2eAgMnSc1d1
<g/>
)	)	kIx)
Seznam	seznam	k1gInSc1
českých	český	k2eAgMnPc2d1
vítězů	vítěz	k1gMnPc2
nadnárodních	nadnárodní	k2eAgFnPc2d1
fotbalových	fotbalový	k2eAgFnPc2d1
klubových	klubový	k2eAgFnPc2d1
soutěží	soutěž	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4018968-5	4018968-5	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
9942	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85123840	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85123840	#num#	k4
</s>
