<s>
Leonardo	Leonardo	k1gMnSc1	Leonardo
di	di	k?	di
ser	srát	k5eAaImRp2nS	srát
Piero	Piero	k1gNnSc1	Piero
da	da	k?	da
Vinci	Vinca	k1gMnSc2	Vinca
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1452	[number]	k4	1452
Anchiano	Anchiana	k1gFnSc5	Anchiana
u	u	k7c2	u
Vinci	Vinca	k1gMnSc2	Vinca
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1519	[number]	k4	1519
Le	Le	k1gFnSc2	Le
Clos	Closa	k1gFnPc2	Closa
Lucé	Lucá	k1gFnSc2	Lucá
u	u	k7c2	u
Amboise	Amboise	k1gFnSc2	Amboise
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
čelní	čelní	k2eAgMnSc1d1	čelní
představitel	představitel	k1gMnSc1	představitel
renesanční	renesanční	k2eAgFnSc2d1	renesanční
malby	malba	k1gFnSc2	malba
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
autor	autor	k1gMnSc1	autor
nejslavnějšího	slavný	k2eAgInSc2d3	nejslavnější
obrazu	obraz	k1gInSc2	obraz
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
portrétu	portrét	k1gInSc2	portrét
zvaného	zvaný	k2eAgInSc2d1	zvaný
Mona	Mon	k1gInSc2	Mon
Lisa	Lisa	k1gFnSc1	Lisa
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
1503	[number]	k4	1503
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
Louvre	Louvre	k1gInSc1	Louvre
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proslul	proslout	k5eAaPmAgMnS	proslout
také	také	k9	také
jako	jako	k9	jako
všestranná	všestranný	k2eAgFnSc1d1	všestranná
renesanční	renesanční	k2eAgFnSc1d1	renesanční
osobnost	osobnost	k1gFnSc1	osobnost
<g/>
:	:	kIx,	:
vedle	vedle	k7c2	vedle
malířství	malířství	k1gNnSc2	malířství
byl	být	k5eAaImAgMnS	být
i	i	k9	i
sochař	sochař	k1gMnSc1	sochař
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
<g/>
,	,	kIx,	,
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
vynálezce	vynálezce	k1gMnSc1	vynálezce
a	a	k8xC	a
konstruktér	konstruktér	k1gMnSc1	konstruktér
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
nemanželským	manželský	k2eNgMnSc7d1	nemanželský
synem	syn	k1gMnSc7	syn
notáře	notář	k1gMnSc2	notář
Piera	Pier	k1gMnSc2	Pier
a	a	k8xC	a
venkovanky	venkovanka	k1gFnPc1	venkovanka
Cathariny	Catharin	k1gInPc4	Catharin
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
ve	v	k7c6	v
22.30	[number]	k4	22.30
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
pokřtěn	pokřtít	k5eAaPmNgInS	pokřtít
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
deseti	deset	k4xCc2	deset
kmotrů	kmotr	k1gMnPc2	kmotr
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c4	za
muže	muž	k1gMnSc4	muž
jménem	jméno	k1gNnSc7	jméno
Accattabrigo	Accattabrigo	k1gMnSc1	Accattabrigo
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
měsíce	měsíc	k1gInPc4	měsíc
dříve	dříve	k6eAd2	dříve
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
sňatek	sňatek	k1gInSc1	sňatek
s	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
florentského	florentský	k2eAgMnSc2d1	florentský
notáře	notář	k1gMnSc2	notář
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
Leonarda	Leonardo	k1gMnSc4	Leonardo
uznal	uznat	k5eAaPmAgMnS	uznat
za	za	k7c2	za
vlastního	vlastní	k2eAgMnSc2d1	vlastní
a	a	k8xC	a
zajistil	zajistit	k5eAaPmAgMnS	zajistit
mu	on	k3xPp3gMnSc3	on
všeobecné	všeobecný	k2eAgNnSc1d1	všeobecné
vzdělání	vzdělání	k1gNnSc1	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
dětství	dětství	k1gNnSc6	dětství
se	se	k3xPyFc4	se
nedochovaly	dochovat	k5eNaPmAgInP	dochovat
žádné	žádný	k3yNgInPc1	žádný
zápisy	zápis	k1gInPc1	zápis
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
o	o	k7c6	o
jeho	jeho	k3xOp3gNnPc6	jeho
mladických	mladický	k2eAgNnPc6d1	mladické
letech	léto	k1gNnPc6	léto
můžeme	moct	k5eAaImIp1nP	moct
pouze	pouze	k6eAd1	pouze
dohadovat	dohadovat	k5eAaImF	dohadovat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
mladém	mladý	k2eAgMnSc6d1	mladý
Leonardovi	Leonardo	k1gMnSc6	Leonardo
se	se	k3xPyFc4	se
ví	vědět	k5eAaImIp3nS	vědět
jen	jen	k9	jen
tolik	tolik	k4yIc1	tolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
příjemného	příjemný	k2eAgInSc2d1	příjemný
vzhledu	vzhled	k1gInSc2	vzhled
a	a	k8xC	a
tak	tak	k6eAd1	tak
silný	silný	k2eAgInSc1d1	silný
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohl	moct	k5eAaImAgInS	moct
ohýbat	ohýbat	k5eAaImF	ohýbat
železné	železný	k2eAgInPc4d1	železný
pruty	prut	k1gInPc4	prut
<g/>
.	.	kIx.	.
</s>
<s>
Leonardo	Leonardo	k1gMnSc1	Leonardo
byl	být	k5eAaImAgMnS	být
nadaný	nadaný	k2eAgMnSc1d1	nadaný
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
různých	různý	k2eAgInPc6d1	různý
oborech	obor	k1gInPc6	obor
<g/>
,	,	kIx,	,
počínaje	počínaje	k7c7	počínaje
malířstvím	malířství	k1gNnSc7	malířství
přes	přes	k7c4	přes
sochařství	sochařství	k1gNnSc4	sochařství
<g/>
,	,	kIx,	,
stavitelství	stavitelství	k1gNnSc4	stavitelství
<g/>
,	,	kIx,	,
hraní	hraní	k1gNnSc4	hraní
na	na	k7c4	na
loutnu	loutna	k1gFnSc4	loutna
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
byl	být	k5eAaImAgMnS	být
také	také	k9	také
výborný	výborný	k2eAgMnSc1d1	výborný
vynálezce	vynálezce	k1gMnSc1	vynálezce
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
vědec	vědec	k1gMnSc1	vědec
předznamenal	předznamenat	k5eAaPmAgMnS	předznamenat
činnost	činnost	k1gFnSc4	činnost
Galilea	Galilea	k1gFnSc1	Galilea
Galileie	Galileie	k1gFnSc1	Galileie
<g/>
,	,	kIx,	,
Francise	Francise	k1gFnSc1	Francise
Bacona	Bacon	k1gMnSc2	Bacon
<g/>
,	,	kIx,	,
Isaaca	Isaacus	k1gMnSc2	Isaacus
Newtona	Newton	k1gMnSc2	Newton
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
správnou	správný	k2eAgFnSc4d1	správná
představu	představa	k1gFnSc4	představa
o	o	k7c6	o
pohybu	pohyb	k1gInSc6	pohyb
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
gravitaci	gravitace	k1gFnSc6	gravitace
<g/>
,	,	kIx,	,
vlnění	vlnění	k1gNnSc6	vlnění
<g/>
,	,	kIx,	,
hoření	hoření	k1gNnSc6	hoření
<g/>
,	,	kIx,	,
o	o	k7c6	o
koloběhu	koloběh	k1gInSc6	koloběh
krve	krev	k1gFnSc2	krev
atd.	atd.	kA	atd.
Jako	jako	k8xS	jako
vynálezce	vynálezce	k1gMnSc1	vynálezce
předvídal	předvídat	k5eAaImAgMnS	předvídat
nebo	nebo	k8xC	nebo
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
princip	princip	k1gInSc1	princip
bagrů	bagr	k1gInPc2	bagr
<g/>
,	,	kIx,	,
odstředivky	odstředivka	k1gFnSc2	odstředivka
<g/>
,	,	kIx,	,
dmychadla	dmychadlo	k1gNnSc2	dmychadlo
<g/>
,	,	kIx,	,
zemních	zemní	k2eAgInPc2d1	zemní
vrtáků	vrták	k1gInPc2	vrták
<g/>
,	,	kIx,	,
kolesové	kolesový	k2eAgFnSc2d1	Kolesová
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
letadla	letadlo	k1gNnSc2	letadlo
<g/>
,	,	kIx,	,
padáku	padák	k1gInSc2	padák
<g/>
,	,	kIx,	,
potápěčského	potápěčský	k2eAgInSc2d1	potápěčský
úboru	úbor	k1gInSc2	úbor
<g/>
,	,	kIx,	,
rýhované	rýhovaný	k2eAgNnSc1d1	rýhované
hlavně	hlavně	k9	hlavně
<g/>
,	,	kIx,	,
šlapacího	šlapací	k2eAgInSc2d1	šlapací
soustruhu	soustruh	k1gInSc2	soustruh
<g/>
,	,	kIx,	,
tiskařského	tiskařský	k2eAgInSc2d1	tiskařský
lisu	lis	k1gInSc2	lis
<g/>
,	,	kIx,	,
závitníku	závitník	k1gInSc2	závitník
a	a	k8xC	a
závitnice	závitnice	k1gFnSc2	závitnice
<g/>
,	,	kIx,	,
tkacího	tkací	k2eAgInSc2d1	tkací
stroje	stroj	k1gInSc2	stroj
a	a	k8xC	a
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgFnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Nakreslil	nakreslit	k5eAaPmAgInS	nakreslit
spoustu	spousta	k1gFnSc4	spousta
různých	různý	k2eAgFnPc2d1	různá
skic	skica	k1gFnPc2	skica
<g/>
,	,	kIx,	,
plánů	plán	k1gInPc2	plán
či	či	k8xC	či
vynálezů	vynález	k1gInPc2	vynález
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
realizovaly	realizovat	k5eAaBmAgInP	realizovat
až	až	k9	až
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Leonardo	Leonardo	k1gMnSc1	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnSc2	Vinca
po	po	k7c6	po
sobě	se	k3xPyFc3	se
zanechal	zanechat	k5eAaPmAgInS	zanechat
mnoho	mnoho	k4c4	mnoho
nedokončených	dokončený	k2eNgFnPc2d1	nedokončená
maleb	malba	k1gFnPc2	malba
i	i	k8xC	i
vynálezů	vynález	k1gInPc2	vynález
<g/>
,	,	kIx,	,
a	a	k8xC	a
něco	něco	k3yInSc1	něco
se	se	k3xPyFc4	se
i	i	k9	i
za	za	k7c4	za
ta	ten	k3xDgNnPc4	ten
staletí	staletí	k1gNnPc4	staletí
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
–	–	k?	–
výzkum	výzkum	k1gInSc4	výzkum
Leonardových	Leonardův	k2eAgInPc2d1	Leonardův
deníků	deník	k1gInPc2	deník
není	být	k5eNaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
ukončen	ukončit	k5eAaPmNgInS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Zachovalo	zachovat	k5eAaPmAgNnS	zachovat
se	se	k3xPyFc4	se
3	[number]	k4	3
500	[number]	k4	500
svazků	svazek	k1gInPc2	svazek
technických	technický	k2eAgInPc2d1	technický
náčrtků	náčrtek	k1gInPc2	náčrtek
a	a	k8xC	a
48	[number]	k4	48
až	až	k9	až
dosud	dosud	k6eAd1	dosud
nalezených	nalezený	k2eAgInPc2d1	nalezený
svazků	svazek	k1gInPc2	svazek
rukopisů	rukopis	k1gInPc2	rukopis
a	a	k8xC	a
poznámek	poznámka	k1gFnPc2	poznámka
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
šifrovaných	šifrovaný	k2eAgMnPc2d1	šifrovaný
<g/>
.	.	kIx.	.
</s>
<s>
Leonardo	Leonardo	k1gMnSc1	Leonardo
také	také	k9	také
oplýval	oplývat	k5eAaImAgMnS	oplývat
neobyčejnou	obyčejný	k2eNgFnSc7d1	neobyčejná
sílou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
1457	[number]	k4	1457
<g/>
–	–	k?	–
<g/>
1466	[number]	k4	1466
chodil	chodit	k5eAaImAgInS	chodit
ve	v	k7c4	v
Vinci	Vinca	k1gMnPc4	Vinca
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
1466	[number]	k4	1466
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
do	do	k7c2	do
Florencie	Florencie	k1gFnSc2	Florencie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
15	[number]	k4	15
let	léto	k1gNnPc2	léto
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
do	do	k7c2	do
učení	učení	k1gNnSc2	učení
v	v	k7c6	v
renomované	renomovaný	k2eAgFnSc6d1	renomovaná
dílně	dílna	k1gFnSc6	dílna
Andrea	Andrea	k1gFnSc1	Andrea
del	del	k?	del
Verrocchio	Verrocchio	k6eAd1	Verrocchio
společně	společně	k6eAd1	společně
se	s	k7c7	s
Sandrem	Sandr	k1gInSc7	Sandr
Botticellim	Botticellima	k1gFnPc2	Botticellima
<g/>
,	,	kIx,	,
Pietrem	Pietr	k1gMnSc7	Pietr
Peruginem	Perugin	k1gMnSc7	Perugin
a	a	k8xC	a
Lorenzem	Lorenz	k1gMnSc7	Lorenz
di	di	k?	di
Credi	Cred	k1gMnPc1	Cred
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
namaloval	namalovat	k5eAaPmAgMnS	namalovat
anděla	anděl	k1gMnSc4	anděl
na	na	k7c6	na
obraze	obraz	k1gInSc6	obraz
"	"	kIx"	"
<g/>
Křest	křest	k1gInSc1	křest
Krista	Kristus	k1gMnSc2	Kristus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
malbě	malba	k1gFnSc6	malba
použil	použít	k5eAaPmAgMnS	použít
jinou	jiný	k2eAgFnSc4d1	jiná
techniku	technika	k1gFnSc4	technika
malování	malování	k1gNnSc2	malování
<g/>
,	,	kIx,	,
než	než	k8xS	než
jaká	jaký	k3yRgFnSc1	jaký
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
běžná	běžný	k2eAgFnSc1d1	běžná
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
jasnější	jasný	k2eAgFnSc1d2	jasnější
a	a	k8xC	a
výraznější	výrazný	k2eAgFnPc1d2	výraznější
barvy	barva	k1gFnPc1	barva
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
Leonardův	Leonardův	k2eAgMnSc1d1	Leonardův
anděl	anděl	k1gMnSc1	anděl
na	na	k7c6	na
celé	celý	k2eAgFnSc6d1	celá
kresbě	kresba	k1gFnSc6	kresba
vynikl	vyniknout	k5eAaPmAgInS	vyniknout
a	a	k8xC	a
stal	stát	k5eAaPmAgInS	stát
se	se	k3xPyFc4	se
dominantní	dominantní	k2eAgFnSc7d1	dominantní
postavou	postava	k1gFnSc7	postava
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
připočteme	připočíst	k5eAaPmIp1nP	připočíst
jemnou	jemný	k2eAgFnSc4d1	jemná
techniku	technika	k1gFnSc4	technika
da	da	k?	da
Vinciho	Vinci	k1gMnSc2	Vinci
rukou	ruka	k1gFnPc2	ruka
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
výsledek	výsledek	k1gInSc1	výsledek
byl	být	k5eAaImAgInS	být
natolik	natolik	k6eAd1	natolik
kolosální	kolosální	k2eAgInSc1d1	kolosální
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
sám	sám	k3xTgMnSc1	sám
velký	velký	k2eAgMnSc1d1	velký
Verrocchio	Verrocchio	k1gMnSc1	Verrocchio
uznal	uznat	k5eAaPmAgMnS	uznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gInSc4	on
jeho	jeho	k3xOp3gMnSc1	jeho
učedník	učedník	k1gMnSc1	učedník
s	s	k7c7	s
náskokem	náskok	k1gInSc7	náskok
předstihl	předstihnout	k5eAaPmAgMnS	předstihnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
údajně	údajně	k6eAd1	údajně
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
sám	sám	k3xTgMnSc1	sám
již	již	k6eAd1	již
nikdy	nikdy	k6eAd1	nikdy
malovat	malovat	k5eAaImF	malovat
nebude	být	k5eNaImBp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Leonardovi	Leonardo	k1gMnSc6	Leonardo
vrcholí	vrcholit	k5eAaImIp3nS	vrcholit
výtvarná	výtvarný	k2eAgFnSc1d1	výtvarná
renesance	renesance	k1gFnSc1	renesance
výrazným	výrazný	k2eAgNnSc7d1	výrazné
a	a	k8xC	a
výmluvným	výmluvný	k2eAgNnSc7d1	výmluvné
gestem	gesto	k1gNnSc7	gesto
<g/>
,	,	kIx,	,
šerosvitem	šerosvit	k1gInSc7	šerosvit
s	s	k7c7	s
jemnými	jemný	k2eAgInPc7d1	jemný
přechody	přechod	k1gInPc7	přechod
světelných	světelný	k2eAgInPc2d1	světelný
efektů	efekt	k1gInPc2	efekt
<g/>
,	,	kIx,	,
vytvářející	vytvářející	k2eAgInSc4d1	vytvářející
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
již	již	k6eAd1	již
napovídá	napovídat	k5eAaBmIp3nS	napovídat
malířskou	malířský	k2eAgFnSc4d1	malířská
problematiku	problematika	k1gFnSc4	problematika
baroka	baroko	k1gNnSc2	baroko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
nejznámější	známý	k2eAgInPc1d3	nejznámější
obrazy	obraz	k1gInPc1	obraz
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Mona	Mon	k2eAgFnSc1d1	Mona
Lisa	Lisa	k1gFnSc1	Lisa
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
Gioconda	Gioconda	k1gFnSc1	Gioconda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Poslední	poslední	k2eAgFnSc1d1	poslední
večeře	večeře	k1gFnSc1	večeře
<g/>
,	,	kIx,	,
Madona	Madona	k1gFnSc1	Madona
ve	v	k7c6	v
skalách	skála	k1gFnPc6	skála
a	a	k8xC	a
další	další	k2eAgFnSc6d1	další
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
získal	získat	k5eAaPmAgMnS	získat
technickou	technický	k2eAgFnSc4d1	technická
dovednost	dovednost	k1gFnSc4	dovednost
<g/>
,	,	kIx,	,
naučil	naučit	k5eAaPmAgMnS	naučit
se	se	k3xPyFc4	se
funkčnímu	funkční	k2eAgNnSc3d1	funkční
<g/>
,	,	kIx,	,
přesnému	přesný	k2eAgNnSc3d1	přesné
a	a	k8xC	a
objektivnímu	objektivní	k2eAgNnSc3d1	objektivní
zobrazování	zobrazování	k1gNnSc3	zobrazování
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
r.	r.	kA	r.
1472	[number]	k4	1472
Leonardo	Leonardo	k1gMnSc1	Leonardo
maluje	malovat	k5eAaImIp3nS	malovat
tvář	tvář	k1gFnSc4	tvář
anděla	anděl	k1gMnSc2	anděl
a	a	k8xC	a
krajinu	krajina	k1gFnSc4	krajina
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
Verrocchiova	Verrocchiův	k2eAgInSc2d1	Verrocchiův
obrazu	obraz	k1gInSc2	obraz
"	"	kIx"	"
<g/>
Křest	křest	k1gInSc1	křest
Kristův	Kristův	k2eAgInSc1d1	Kristův
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
zvykem	zvyk	k1gInSc7	zvyk
<g/>
,	,	kIx,	,
že	že	k8xS	že
mistr	mistr	k1gMnSc1	mistr
namaloval	namalovat	k5eAaPmAgMnS	namalovat
pouze	pouze	k6eAd1	pouze
základní	základní	k2eAgNnSc4d1	základní
rozvržení	rozvržení	k1gNnSc4	rozvržení
obrazu	obraz	k1gInSc2	obraz
a	a	k8xC	a
hlavní	hlavní	k2eAgFnSc2d1	hlavní
postavy	postava	k1gFnSc2	postava
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc4	zbytek
pak	pak	k6eAd1	pak
malovali	malovat	k5eAaImAgMnP	malovat
žáci	žák	k1gMnPc1	žák
<g/>
.	.	kIx.	.
</s>
<s>
Verrocchio	Verrocchio	k6eAd1	Verrocchio
byl	být	k5eAaImAgInS	být
dokonalostí	dokonalost	k1gFnSc7	dokonalost
Leonardovy	Leonardův	k2eAgFnSc2d1	Leonardova
malby	malba	k1gFnSc2	malba
tak	tak	k6eAd1	tak
unesen	unést	k5eAaPmNgMnS	unést
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
dalších	další	k2eAgInPc2d1	další
obrazů	obraz	k1gInPc2	obraz
sám	sám	k3xTgInSc4	sám
pouze	pouze	k6eAd1	pouze
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
rozvržení	rozvržení	k1gNnSc4	rozvržení
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
práce	práce	k1gFnSc2	práce
zůstal	zůstat	k5eAaPmAgInS	zůstat
na	na	k7c6	na
žácích	žák	k1gMnPc6	žák
a	a	k8xC	a
všechny	všechen	k3xTgFnPc4	všechen
tváře	tvář	k1gFnPc4	tvář
maloval	malovat	k5eAaImAgMnS	malovat
Leonardo	Leonardo	k1gMnSc1	Leonardo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1472	[number]	k4	1472
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
písemných	písemný	k2eAgInPc2d1	písemný
záznamů	záznam	k1gInPc2	záznam
přijat	přijmout	k5eAaPmNgInS	přijmout
do	do	k7c2	do
cechu	cech	k1gInSc2	cech
malířského	malířský	k2eAgMnSc2d1	malířský
San	San	k1gMnSc2	San
Luca	Lucus	k1gMnSc2	Lucus
<g/>
.	.	kIx.	.
1472	[number]	k4	1472
<g/>
–	–	k?	–
<g/>
1476	[number]	k4	1476
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
tovaryš	tovaryš	k1gMnSc1	tovaryš
ve	v	k7c6	v
Verrocchiově	Verrocchiův	k2eAgFnSc6d1	Verrocchiův
dílně	dílna	k1gFnSc6	dílna
<g/>
.	.	kIx.	.
1473	[number]	k4	1473
a	a	k8xC	a
výkres	výkres	k1gInSc1	výkres
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc7	první
podepsanou	podepsaný	k2eAgFnSc7d1	podepsaná
a	a	k8xC	a
datovanou	datovaný	k2eAgFnSc7d1	datovaná
Leonardovou	Leonardův	k2eAgFnSc7d1	Leonardova
prací	práce	k1gFnSc7	práce
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1476	[number]	k4	1476
samostatně	samostatně	k6eAd1	samostatně
maluje	malovat	k5eAaImIp3nS	malovat
obrazy	obraz	k1gInPc4	obraz
<g/>
:	:	kIx,	:
Zvěstování	zvěstování	k1gNnSc2	zvěstování
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Madona	Madona	k1gFnSc1	Madona
s	s	k7c7	s
karafiátem	karafiát	k1gInSc7	karafiát
<g/>
,	,	kIx,	,
portrét	portrét	k1gInSc1	portrét
Ginevry	Ginevra	k1gFnSc2	Ginevra
Benci	Benc	k1gMnPc7	Benc
aj.	aj.	kA	aj.
Roku	rok	k1gInSc2	rok
1476	[number]	k4	1476
byl	být	k5eAaImAgMnS	být
Leonardo	Leonardo	k1gMnSc1	Leonardo
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
nedovolených	dovolený	k2eNgInPc2d1	nedovolený
(	(	kIx(	(
<g/>
homosexuálních	homosexuální	k2eAgInPc2d1	homosexuální
<g/>
)	)	kIx)	)
styků	styk	k1gInPc2	styk
a	a	k8xC	a
vyslýchán	vyslýchat	k5eAaImNgMnS	vyslýchat
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
přátel	přítel	k1gMnPc2	přítel
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgMnPc7	jenž
byl	být	k5eAaImAgMnS	být
právě	právě	k9	právě
i	i	k9	i
Leonardo	Leonardo	k1gMnSc1	Leonardo
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
obcovat	obcovat	k5eAaImF	obcovat
s	s	k7c7	s
mladým	mladý	k2eAgMnSc7d1	mladý
prostitutem	prostitut	k1gMnSc7	prostitut
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
čin	čin	k1gInSc1	čin
byl	být	k5eAaImAgInS	být
protizákonný	protizákonný	k2eAgMnSc1d1	protizákonný
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
trestán	trestat	k5eAaImNgInS	trestat
i	i	k9	i
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
obžalovanými	obžalovaný	k1gMnPc7	obžalovaný
byl	být	k5eAaImAgMnS	být
naštěstí	naštěstí	k6eAd1	naštěstí
i	i	k9	i
syn	syn	k1gMnSc1	syn
jistého	jistý	k2eAgMnSc2d1	jistý
vlivného	vlivný	k2eAgMnSc2d1	vlivný
florentského	florentský	k2eAgMnSc2d1	florentský
občana	občan	k1gMnSc2	občan
–	–	k?	–
proces	proces	k1gInSc1	proces
byl	být	k5eAaImAgInS	být
tedy	tedy	k9	tedy
zastaven	zastavit	k5eAaPmNgMnS	zastavit
a	a	k8xC	a
aktéři	aktér	k1gMnPc1	aktér
vyvázli	vyváznout	k5eAaPmAgMnP	vyváznout
jen	jen	k9	jen
se	s	k7c7	s
symbolickým	symbolický	k2eAgInSc7d1	symbolický
tělesným	tělesný	k2eAgInSc7d1	tělesný
trestem	trest	k1gInSc7	trest
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
zkušenost	zkušenost	k1gFnSc1	zkušenost
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Leonardo	Leonardo	k1gMnSc1	Leonardo
přestal	přestat	k5eAaPmAgMnS	přestat
být	být	k5eAaImF	být
důvěřivý	důvěřivý	k2eAgMnSc1d1	důvěřivý
a	a	k8xC	a
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
se	se	k3xPyFc4	se
do	do	k7c2	do
sebe	se	k3xPyFc2	se
<g/>
;	;	kIx,	;
proto	proto	k6eAd1	proto
jeho	jeho	k3xOp3gInPc4	jeho
vynálezy	vynález	k1gInPc4	vynález
nedošly	dojít	k5eNaPmAgInP	dojít
většího	veliký	k2eAgNnSc2d2	veliký
uplatnění	uplatnění	k1gNnSc2	uplatnění
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
1478	[number]	k4	1478
maloval	malovat	k5eAaImAgMnS	malovat
"	"	kIx"	"
<g/>
druhé	druhý	k4xOgNnSc1	druhý
Zvěstování	zvěstování	k1gNnSc1	zvěstování
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
)	)	kIx)	)
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
další	další	k2eAgInPc4d1	další
obrazy	obraz	k1gInPc4	obraz
madon	madona	k1gFnPc2	madona
(	(	kIx(	(
<g/>
snad	snad	k9	snad
Madona	Madona	k1gFnSc1	Madona
Benois	Benois	k1gFnSc1	Benois
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1478	[number]	k4	1478
dostal	dostat	k5eAaPmAgMnS	dostat
Leonardo	Leonardo	k1gMnSc1	Leonardo
zakázku	zakázka	k1gFnSc4	zakázka
na	na	k7c4	na
obraz	obraz	k1gInSc4	obraz
pro	pro	k7c4	pro
kapli	kaple	k1gFnSc4	kaple
sv.	sv.	kA	sv.
Bernarda	Bernard	k1gMnSc2	Bernard
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nesplnil	splnit	k5eNaPmAgMnS	splnit
ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
patří	patřit	k5eAaImIp3nS	patřit
spiknutí	spiknutí	k1gNnSc1	spiknutí
Pazziů	Pazzi	k1gInPc2	Pazzi
proti	proti	k7c3	proti
Medicejským	Medicejský	k2eAgInPc3d1	Medicejský
a	a	k8xC	a
války	válka	k1gFnSc2	válka
proti	proti	k7c3	proti
Florencii	Florencie	k1gFnSc3	Florencie
<g/>
.	.	kIx.	.
</s>
<s>
Leonardo	Leonardo	k1gMnSc1	Leonardo
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
vojenským	vojenský	k2eAgNnSc7d1	vojenské
inženýrstvím	inženýrství	k1gNnSc7	inženýrství
<g/>
.	.	kIx.	.
1478	[number]	k4	1478
<g/>
–	–	k?	–
<g/>
1518	[number]	k4	1518
zpracovával	zpracovávat	k5eAaImAgInS	zpracovávat
Codex	Codex	k1gInSc1	Codex
Atlanticus	Atlanticus	k1gInSc1	Atlanticus
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1479	[number]	k4	1479
nakreslil	nakreslit	k5eAaPmAgMnS	nakreslit
Leonardo	Leonardo	k1gMnSc1	Leonardo
oběšeného	oběšený	k2eAgMnSc2d1	oběšený
Bernarda	Bernard	k1gMnSc2	Bernard
Bandiniho	Bandini	k1gMnSc2	Bandini
Baroncelliho	Baroncelli	k1gMnSc2	Baroncelli
<g/>
,	,	kIx,	,
vraha	vrah	k1gMnSc2	vrah
Giuliana	Giulian	k1gMnSc2	Giulian
de	de	k?	de
Medici	medik	k1gMnPc1	medik
<g/>
.	.	kIx.	.
1481	[number]	k4	1481
podepsal	podepsat	k5eAaPmAgMnS	podepsat
Leonardo	Leonardo	k1gMnSc1	Leonardo
smlouvu	smlouva	k1gFnSc4	smlouva
na	na	k7c4	na
obraz	obraz	k1gInSc4	obraz
"	"	kIx"	"
<g/>
Klanění	klanění	k1gNnSc1	klanění
králů	král	k1gMnPc2	král
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Rozpracoval	rozpracovat	k5eAaPmAgMnS	rozpracovat
jej	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
začal	začít	k5eAaPmAgInS	začít
obraz	obraz	k1gInSc1	obraz
"	"	kIx"	"
<g/>
sv.	sv.	kA	sv.
Jeronýma	Jeroným	k1gMnSc2	Jeroným
na	na	k7c6	na
poušti	poušť	k1gFnSc6	poušť
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1482	[number]	k4	1482
odešel	odejít	k5eAaPmAgMnS	odejít
na	na	k7c4	na
pozvání	pozvání	k1gNnSc4	pozvání
vévody	vévoda	k1gMnSc2	vévoda
Lodovica	Lodovicus	k1gMnSc2	Lodovicus
Sforzy	Sforza	k1gFnSc2	Sforza
z	z	k7c2	z
Florencie	Florencie	k1gFnSc2	Florencie
do	do	k7c2	do
Milána	Milán	k1gInSc2	Milán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
letech	léto	k1gNnPc6	léto
kolem	kolem	k6eAd1	kolem
sebe	sebe	k3xPyFc4	sebe
shromažďuje	shromažďovat	k5eAaImIp3nS	shromažďovat
skupinku	skupinka	k1gFnSc4	skupinka
následovníků	následovník	k1gMnPc2	následovník
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
někteří	některý	k3yIgMnPc1	některý
mu	on	k3xPp3gMnSc3	on
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
věrni	věren	k2eAgMnPc1d1	věren
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
nikdy	nikdy	k6eAd1	nikdy
takový	takový	k3xDgInSc4	takový
obdiv	obdiv	k1gInSc4	obdiv
nenalezl	nalézt	k5eNaBmAgMnS	nalézt
<g/>
,	,	kIx,	,
neb	neb	k8xC	neb
jeho	jeho	k3xOp3gInSc1	jeho
osobitý	osobitý	k2eAgInSc1d1	osobitý
styl	styl	k1gInSc1	styl
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
příliš	příliš	k6eAd1	příliš
novátorský	novátorský	k2eAgInSc1d1	novátorský
a	a	k8xC	a
nepokrokový	pokrokový	k2eNgInSc1d1	nepokrokový
skepticismus	skepticismus	k1gInSc1	skepticismus
Florenťanů	Florenťan	k1gMnPc2	Florenťan
mu	on	k3xPp3gMnSc3	on
tak	tak	k6eAd1	tak
nikdy	nikdy	k6eAd1	nikdy
nemohl	moct	k5eNaImAgMnS	moct
poskytnout	poskytnout	k5eAaPmF	poskytnout
uznání	uznání	k1gNnSc4	uznání
<g/>
,	,	kIx,	,
kterého	který	k3yQgInSc2	který
by	by	kYmCp3nS	by
zasluhoval	zasluhovat	k5eAaImAgMnS	zasluhovat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
styl	styl	k1gInSc1	styl
byl	být	k5eAaImAgInS	být
rovnovážným	rovnovážný	k2eAgNnSc7d1	rovnovážné
pojetím	pojetí	k1gNnSc7	pojetí
mezi	mezi	k7c7	mezi
silnými	silný	k2eAgInPc7d1	silný
poetickými	poetický	k2eAgInPc7d1	poetický
city	cit	k1gInPc7	cit
a	a	k8xC	a
vůlí	vůle	k1gFnSc7	vůle
umělce	umělec	k1gMnSc2	umělec
<g/>
,	,	kIx,	,
naplněný	naplněný	k2eAgInSc1d1	naplněný
jedinečným	jedinečný	k2eAgInSc7d1	jedinečný
mysticismem	mysticismus	k1gInSc7	mysticismus
gest	gesto	k1gNnPc2	gesto
<g/>
.	.	kIx.	.
</s>
<s>
Leonardo	Leonardo	k1gMnSc1	Leonardo
odmítá	odmítat	k5eAaImIp3nS	odmítat
geometrickou	geometrický	k2eAgFnSc4d1	geometrická
formu	forma	k1gFnSc4	forma
perspektivy	perspektiva	k1gFnSc2	perspektiva
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
hluboký	hluboký	k2eAgInSc1d1	hluboký
pohled	pohled	k1gInSc1	pohled
do	do	k7c2	do
krajiny	krajina	k1gFnSc2	krajina
v	v	k7c6	v
obraze	obraz	k1gInSc6	obraz
<g/>
,	,	kIx,	,
využívá	využívat	k5eAaImIp3nS	využívat
volně	volně	k6eAd1	volně
kompozice	kompozice	k1gFnSc1	kompozice
bohatých	bohatý	k2eAgInPc2d1	bohatý
vztahů	vztah	k1gInPc2	vztah
všeho	všecek	k3xTgInSc2	všecek
živého	živý	k2eAgInSc2d1	živý
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
obrazech	obraz	k1gInPc6	obraz
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
dynamiku	dynamika	k1gFnSc4	dynamika
postav	postava	k1gFnPc2	postava
v	v	k7c6	v
esovitém	esovitý	k2eAgInSc6d1	esovitý
tvaru	tvar	k1gInSc6	tvar
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stává	stávat	k5eAaImIp3nS	stávat
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
základních	základní	k2eAgInPc2d1	základní
modelů	model	k1gInPc2	model
pro	pro	k7c4	pro
klasické	klasický	k2eAgFnPc4d1	klasická
figury	figura	k1gFnPc4	figura
šestnáctého	šestnáctý	k4xOgNnSc2	šestnáctý
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Leonardo	Leonardo	k1gMnSc1	Leonardo
tak	tak	k6eAd1	tak
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
ojedinělý	ojedinělý	k2eAgInSc1d1	ojedinělý
jazyk	jazyk	k1gInSc1	jazyk
génia	génius	k1gMnSc2	génius
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1483	[number]	k4	1483
Leonardo	Leonardo	k1gMnSc1	Leonardo
začal	začít	k5eAaPmAgMnS	začít
studium	studium	k1gNnSc4	studium
aerodynamiky	aerodynamika	k1gFnSc2	aerodynamika
a	a	k8xC	a
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
anatomie	anatomie	k1gFnSc2	anatomie
<g/>
,	,	kIx,	,
meteorologie	meteorologie	k1gFnSc2	meteorologie
<g/>
,	,	kIx,	,
astronomie	astronomie	k1gFnSc2	astronomie
a	a	k8xC	a
kosmografie	kosmografie	k1gFnSc2	kosmografie
<g/>
.	.	kIx.	.
</s>
<s>
Zajímal	zajímat	k5eAaImAgMnS	zajímat
se	se	k3xPyFc4	se
o	o	k7c4	o
Pythagorovu	Pythagorův	k2eAgFnSc4d1	Pythagorova
matematiku	matematika	k1gFnSc4	matematika
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
si	se	k3xPyFc3	se
brzy	brzy	k6eAd1	brzy
osvojil	osvojit	k5eAaPmAgMnS	osvojit
a	a	k8xC	a
učinil	učinit	k5eAaPmAgMnS	učinit
ji	on	k3xPp3gFnSc4	on
základem	základ	k1gInSc7	základ
vlastních	vlastní	k2eAgNnPc2d1	vlastní
bádání	bádání	k1gNnPc2	bádání
<g/>
.	.	kIx.	.
</s>
<s>
Poznatky	poznatek	k1gInPc1	poznatek
získané	získaný	k2eAgInPc1d1	získaný
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
vědeckého	vědecký	k2eAgNnSc2d1	vědecké
zkoumání	zkoumání	k1gNnSc2	zkoumání
byly	být	k5eAaImAgFnP	být
využity	využít	k5eAaPmNgFnP	využít
k	k	k7c3	k
dramatickému	dramatický	k2eAgInSc3d1	dramatický
efektu	efekt	k1gInSc3	efekt
celé	celý	k2eAgFnSc2d1	celá
kompozice	kompozice	k1gFnSc2	kompozice
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1487	[number]	k4	1487
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
architektonické	architektonický	k2eAgFnSc2d1	architektonická
soutěže	soutěž	k1gFnSc2	soutěž
na	na	k7c4	na
model	model	k1gInSc4	model
kupole	kupole	k1gFnSc2	kupole
nad	nad	k7c7	nad
křížením	křížení	k1gNnSc7	křížení
milánského	milánský	k2eAgInSc2d1	milánský
Dómu	dóm	k1gInSc2	dóm
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
podané	podaný	k2eAgInPc1d1	podaný
návrhy	návrh	k1gInPc1	návrh
byly	být	k5eAaImAgInP	být
však	však	k9	však
příliš	příliš	k6eAd1	příliš
nové	nový	k2eAgNnSc1d1	nové
a	a	k8xC	a
smělé	smělý	k2eAgNnSc1d1	smělé
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
Dóm	dóm	k1gInSc1	dóm
dokončen	dokončit	k5eAaPmNgInS	dokončit
podle	podle	k7c2	podle
plánů	plán	k1gInPc2	plán
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
více	hodně	k6eAd2	hodně
v	v	k7c6	v
gotickém	gotický	k2eAgMnSc6d1	gotický
duchu	duch	k1gMnSc6	duch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1495	[number]	k4	1495
započal	započnout	k5eAaPmAgMnS	započnout
práci	práce	k1gFnSc4	práce
na	na	k7c4	na
Poslední	poslední	k2eAgFnSc4d1	poslední
večeři	večeře	k1gFnSc4	večeře
<g/>
.	.	kIx.	.
</s>
<s>
Užil	užít	k5eAaPmAgMnS	užít
nové	nový	k2eAgMnPc4d1	nový
techniky	technik	k1gMnPc4	technik
s	s	k7c7	s
temperou	tempera	k1gFnSc7	tempera
a	a	k8xC	a
olejem	olej	k1gInSc7	olej
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
volba	volba	k1gFnSc1	volba
se	se	k3xPyFc4	se
však	však	k9	však
ukázala	ukázat	k5eAaPmAgFnS	ukázat
jako	jako	k9	jako
nešťastná	šťastný	k2eNgFnSc1d1	nešťastná
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
dílo	dílo	k1gNnSc1	dílo
brzy	brzy	k6eAd1	brzy
zchátralo	zchátrat	k5eAaPmAgNnS	zchátrat
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
půli	půle	k1gFnSc6	půle
šestnáctého	šestnáctý	k4xOgNnSc2	šestnáctý
století	století	k1gNnSc2	století
bylo	být	k5eAaImAgNnS	být
popsáno	popsat	k5eAaPmNgNnS	popsat
jako	jako	k9	jako
úplně	úplně	k6eAd1	úplně
zničené	zničený	k2eAgNnSc1d1	zničené
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
Poslední	poslední	k2eAgFnSc1d1	poslední
večeře	večeře	k1gFnSc1	večeře
byla	být	k5eAaImAgFnS	být
podrobena	podrobit	k5eAaPmNgFnS	podrobit
mnoha	mnoho	k4c3	mnoho
pokusům	pokus	k1gInPc3	pokus
o	o	k7c4	o
opravu	oprava	k1gFnSc4	oprava
<g/>
.	.	kIx.	.
</s>
<s>
Leonardova	Leonardův	k2eAgFnSc1d1	Leonardova
kompozice	kompozice	k1gFnSc1	kompozice
obrazu	obraz	k1gInSc2	obraz
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
tajemné	tajemný	k2eAgNnSc1d1	tajemné
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
esoterické	esoterický	k2eAgInPc4d1	esoterický
rozměry	rozměr	k1gInPc4	rozměr
<g/>
,	,	kIx,	,
divák	divák	k1gMnSc1	divák
je	být	k5eAaImIp3nS	být
pozván	pozvat	k5eAaPmNgMnS	pozvat
do	do	k7c2	do
světa	svět	k1gInSc2	svět
citu	cit	k1gInSc2	cit
a	a	k8xC	a
nekonečné	konečný	k2eNgFnSc2d1	nekonečná
dynamiky	dynamika	k1gFnSc2	dynamika
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Volba	volba	k1gFnSc1	volba
barev	barva	k1gFnPc2	barva
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
prostředkem	prostředek	k1gInSc7	prostředek
k	k	k7c3	k
popsání	popsání	k1gNnSc3	popsání
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
jemná	jemný	k2eAgFnSc1d1	jemná
kombinace	kombinace	k1gFnSc1	kombinace
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
stínu	stín	k1gInSc2	stín
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
novou	nový	k2eAgFnSc4d1	nová
formu	forma	k1gFnSc4	forma
a	a	k8xC	a
podporuje	podporovat	k5eAaImIp3nS	podporovat
představivost	představivost	k1gFnSc4	představivost
diváka	divák	k1gMnSc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Postavy	postava	k1gFnPc1	postava
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
jsou	být	k5eAaImIp3nP	být
zobrazeny	zobrazit	k5eAaPmNgInP	zobrazit
typicky	typicky	k6eAd1	typicky
renesančním	renesanční	k2eAgInSc7d1	renesanční
stylem	styl	k1gInSc7	styl
a	a	k8xC	a
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
kombinacích	kombinace	k1gFnPc6	kombinace
s	s	k7c7	s
pozadím	pozadí	k1gNnSc7	pozadí
získávají	získávat	k5eAaImIp3nP	získávat
na	na	k7c6	na
konečném	konečný	k2eAgInSc6d1	konečný
dojmu	dojem	k1gInSc6	dojem
monumentality	monumentalita	k1gFnSc2	monumentalita
a	a	k8xC	a
rovnováhy	rovnováha	k1gFnSc2	rovnováha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1499	[number]	k4	1499
byli	být	k5eAaImAgMnP	být
ale	ale	k9	ale
Sforzové	Sforz	k1gMnPc1	Sforz
z	z	k7c2	z
Milána	Milán	k1gInSc2	Milán
vypuzeni	vypuzen	k2eAgMnPc1d1	vypuzen
a	a	k8xC	a
Leonardo	Leonardo	k1gMnSc1	Leonardo
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Florencie	Florencie	k1gFnSc2	Florencie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1500	[number]	k4	1500
<g/>
–	–	k?	–
<g/>
1507	[number]	k4	1507
pobýval	pobývat	k5eAaImAgMnS	pobývat
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jara	jaro	k1gNnSc2	jaro
roku	rok	k1gInSc2	rok
1500	[number]	k4	1500
se	se	k3xPyFc4	se
Leonardo	Leonardo	k1gMnSc1	Leonardo
opět	opět	k6eAd1	opět
usídlil	usídlit	k5eAaPmAgMnS	usídlit
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
<g/>
.	.	kIx.	.
</s>
<s>
Pobýval	pobývat	k5eAaImAgMnS	pobývat
u	u	k7c2	u
matematika	matematik	k1gMnSc2	matematik
Pacioliho	Pacioli	k1gMnSc2	Pacioli
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
jehož	jenž	k3xRgNnSc4	jenž
dílo	dílo	k1gNnSc4	dílo
o	o	k7c6	o
božské	božský	k2eAgFnSc6d1	božská
proporci	proporce	k1gFnSc6	proporce
a	a	k8xC	a
architektuře	architektura	k1gFnSc6	architektura
údajně	údajně	k6eAd1	údajně
měl	mít	k5eAaImAgMnS	mít
ilustrovat	ilustrovat	k5eAaBmF	ilustrovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
konal	konat	k5eAaImAgMnS	konat
svá	svůj	k3xOyFgNnPc4	svůj
anatomická	anatomický	k2eAgNnPc4d1	anatomické
studia	studio	k1gNnPc4	studio
<g/>
;	;	kIx,	;
zabýval	zabývat	k5eAaImAgInS	zabývat
se	se	k3xPyFc4	se
otázkou	otázka	k1gFnSc7	otázka
regulace	regulace	k1gFnSc2	regulace
řeky	řeka	k1gFnSc2	řeka
Arna	Arne	k1gMnSc2	Arne
ve	v	k7c6	v
spojitosti	spojitost	k1gFnSc6	spojitost
s	s	k7c7	s
vysoušením	vysoušení	k1gNnSc7	vysoušení
močálů	močál	k1gInPc2	močál
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
otázce	otázka	k1gFnSc3	otázka
Sforzova	Sforzův	k2eAgInSc2d1	Sforzův
pomníku	pomník	k1gInSc2	pomník
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
vrátit	vrátit	k5eAaPmF	vrátit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1503	[number]	k4	1503
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
uloženo	uložit	k5eAaPmNgNnS	uložit
vysokou	vysoký	k2eAgFnSc7d1	vysoká
radou	rada	k1gFnSc7	rada
města	město	k1gNnSc2	město
Florencie	Florencie	k1gFnSc2	Florencie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
sále	sál	k1gInSc6	sál
vládní	vládní	k2eAgFnSc2d1	vládní
budovy	budova	k1gFnSc2	budova
vymaloval	vymalovat	k5eAaPmAgInS	vymalovat
obraz	obraz	k1gInSc1	obraz
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Anghiari	Anghiar	k1gFnSc2	Anghiar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1503	[number]	k4	1503
až	až	k9	až
1506	[number]	k4	1506
maloval	malovat	k5eAaImAgInS	malovat
obraz	obraz	k1gInSc1	obraz
Mona	Mona	k1gMnSc1	Mona
Lisa	Lisa	k1gFnSc1	Lisa
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejslavnějších	slavný	k2eAgInPc2d3	nejslavnější
obrazů	obraz	k1gInPc2	obraz
světového	světový	k2eAgNnSc2d1	světové
malířství	malířství	k1gNnSc2	malířství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1506	[number]	k4	1506
<g/>
–	–	k?	–
<g/>
1513	[number]	k4	1513
působil	působit	k5eAaImAgInS	působit
opět	opět	k6eAd1	opět
v	v	k7c6	v
Miláně	Milán	k1gInSc6	Milán
<g/>
,	,	kIx,	,
1513	[number]	k4	1513
<g/>
–	–	k?	–
<g/>
1514	[number]	k4	1514
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
Guiliana	Guiliana	k1gFnSc1	Guiliana
de	de	k?	de
<g/>
'	'	kIx"	'
Medici	medik	k1gMnPc1	medik
(	(	kIx(	(
<g/>
mladšího	mladý	k2eAgMnSc4d2	mladší
bratra	bratr	k1gMnSc4	bratr
tehdejšího	tehdejší	k2eAgMnSc4d1	tehdejší
papeže	papež	k1gMnSc4	papež
Lva	Lev	k1gMnSc2	Lev
X.	X.	kA	X.
<g/>
)	)	kIx)	)
a	a	k8xC	a
jako	jako	k9	jako
jeho	jeho	k3xOp3gMnSc1	jeho
chráněnec	chráněnec	k1gMnSc1	chráněnec
dostával	dostávat	k5eAaImAgMnS	dostávat
pravidelný	pravidelný	k2eAgInSc4d1	pravidelný
plat	plat	k1gInSc4	plat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
Leonarda	Leonardo	k1gMnSc4	Leonardo
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
i	i	k9	i
přes	přes	k7c4	přes
svůj	svůj	k3xOyFgInSc4	svůj
věhlas	věhlas	k1gInSc4	věhlas
často	často	k6eAd1	často
potýkal	potýkat	k5eAaImAgMnS	potýkat
s	s	k7c7	s
bídou	bída	k1gFnSc7	bída
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
vítaná	vítaný	k2eAgFnSc1d1	vítaná
změna	změna	k1gFnSc1	změna
<g/>
.	.	kIx.	.
</s>
<s>
Řím	Řím	k1gInSc1	Řím
byl	být	k5eAaImAgInS	být
tehdy	tehdy	k6eAd1	tehdy
jednou	jeden	k4xCgFnSc7	jeden
velkou	velký	k2eAgFnSc7d1	velká
bottegou	bottega	k1gFnSc7	bottega
a	a	k8xC	a
konkurence	konkurence	k1gFnSc1	konkurence
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
obrovská	obrovský	k2eAgFnSc1d1	obrovská
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1516	[number]	k4	1516
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
žil	žít	k5eAaImAgMnS	žít
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
na	na	k7c4	na
pozvání	pozvání	k1gNnSc4	pozvání
krále	král	k1gMnSc4	král
Františka	František	k1gMnSc2	František
I.	I.	kA	I.
Jeho	jeho	k3xOp3gNnSc4	jeho
dílo	dílo	k1gNnSc4	dílo
je	být	k5eAaImIp3nS	být
obrovské	obrovský	k2eAgNnSc4d1	obrovské
a	a	k8xC	a
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
umění	umění	k1gNnSc4	umění
i	i	k8xC	i
vědu	věda	k1gFnSc4	věda
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
jeho	jeho	k3xOp3gNnPc2	jeho
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
děl	dělo	k1gNnPc2	dělo
i	i	k8xC	i
vědeckých	vědecký	k2eAgNnPc2d1	vědecké
pojednání	pojednání	k1gNnPc2	pojednání
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
jeho	jeho	k3xOp3gInPc2	jeho
deníků	deník	k1gInPc2	deník
stále	stále	k6eAd1	stále
probíhá	probíhat	k5eAaImIp3nS	probíhat
<g/>
.	.	kIx.	.
</s>
<s>
Madona	Madona	k1gFnSc1	Madona
ve	v	k7c6	v
skalách	skála	k1gFnPc6	skála
(	(	kIx(	(
<g/>
1483	[number]	k4	1483
<g/>
–	–	k?	–
<g/>
1493	[number]	k4	1493
<g/>
)	)	kIx)	)
Poslední	poslední	k2eAgFnSc1d1	poslední
večeře	večeře	k1gFnSc1	večeře
(	(	kIx(	(
<g/>
1495	[number]	k4	1495
<g/>
–	–	k?	–
<g/>
1498	[number]	k4	1498
<g/>
)	)	kIx)	)
–	–	k?	–
nástěnná	nástěnný	k2eAgFnSc1d1	nástěnná
malba	malba	k1gFnSc1	malba
440	[number]	k4	440
×	×	k?	×
880	[number]	k4	880
cm	cm	kA	cm
<g/>
,	,	kIx,	,
Milán	Milán	k1gInSc1	Milán
<g/>
,	,	kIx,	,
refektář	refektář	k1gInSc1	refektář
konventu	konvent	k1gInSc2	konvent
Santa	Sant	k1gMnSc2	Sant
Maria	Mario	k1gMnSc2	Mario
delle	dell	k1gMnSc2	dell
Grazie	Grazie	k1gFnSc2	Grazie
Sv.	sv.	kA	sv.
Anna	Anna	k1gFnSc1	Anna
samotřetí	samotřetit	k5eAaPmIp3nS	samotřetit
(	(	kIx(	(
<g/>
1501	[number]	k4	1501
<g/>
–	–	k?	–
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1507	[number]	k4	1507
<g/>
)	)	kIx)	)
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Anghiari	Anghiar	k1gFnSc2	Anghiar
(	(	kIx(	(
<g/>
1505	[number]	k4	1505
<g/>
)	)	kIx)	)
Mona	Mon	k2eAgFnSc1d1	Mona
Lisa	Lisa	k1gFnSc1	Lisa
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
Gioconda	Gioconda	k1gFnSc1	Gioconda
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1503	[number]	k4	1503
<g/>
–	–	k?	–
<g/>
1506	[number]	k4	1506
<g/>
)	)	kIx)	)
–	–	k?	–
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jeho	on	k3xPp3gInSc4	on
nejznámější	známý	k2eAgInSc4d3	nejznámější
obraz	obraz	k1gInSc4	obraz
Autoportrét	autoportrét	k1gInSc1	autoportrét
Dáma	dáma	k1gFnSc1	dáma
s	s	k7c7	s
hranostajem	hranostaj	k1gMnSc7	hranostaj
(	(	kIx(	(
<g/>
1488	[number]	k4	1488
<g/>
-	-	kIx~	-
<g/>
1490	[number]	k4	1490
<g/>
)	)	kIx)	)
–	–	k?	–
Krakov	Krakov	k1gInSc1	Krakov
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Czartoryski	Czartoryski	k6eAd1	Czartoryski
Museum	museum	k1gNnSc1	museum
Portrét	portrét	k1gInSc4	portrét
dámy	dáma	k1gFnSc2	dáma
zvaný	zvaný	k2eAgInSc4d1	zvaný
La	la	k1gNnSc4	la
Belle	bell	k1gInSc5	bell
Ferronniere	Ferronnier	k1gMnSc5	Ferronnier
-	-	kIx~	-
není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
byl	být	k5eAaImAgInS	být
objektem	objekt	k1gInSc7	objekt
portrétování	portrétování	k1gNnSc2	portrétování
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
znalců	znalec	k1gMnPc2	znalec
da	da	k?	da
Vinciho	Vinci	k1gMnSc4	Vinci
díla	dílo	k1gNnPc1	dílo
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
další	další	k2eAgInSc4d1	další
portrét	portrét	k1gInSc4	portrét
již	již	k6eAd1	již
starší	starý	k2eAgFnPc1d2	starší
Cecilie	Cecilie	k1gFnPc1	Cecilie
Gallerani	Galleran	k1gMnPc1	Galleran
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc1	výše
"	"	kIx"	"
<g/>
Dáma	dáma	k1gFnSc1	dáma
s	s	k7c7	s
hranostajem	hranostaj	k1gMnSc7	hranostaj
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Portrét	portrét	k1gInSc1	portrét
Ginevry	Ginevra	k1gFnSc2	Ginevra
de	de	k?	de
'	'	kIx"	'
<g/>
Benci	Benc	k1gMnSc6	Benc
(	(	kIx(	(
<g/>
1474	[number]	k4	1474
<g/>
–	–	k?	–
<g/>
1476	[number]	k4	1476
<g/>
)	)	kIx)	)
–	–	k?	–
olej	olej	k1gInSc1	olej
na	na	k7c6	na
dřevě	dřevo	k1gNnSc6	dřevo
42	[number]	k4	42
×	×	k?	×
37	[number]	k4	37
cm	cm	kA	cm
<g/>
,	,	kIx,	,
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
National	National	k1gFnSc1	National
Gallery	Galler	k1gInPc1	Galler
of	of	k?	of
Art	Art	k1gMnSc4	Art
Portrét	portrét	k1gInSc1	portrét
hudebníka	hudebník	k1gMnSc2	hudebník
(	(	kIx(	(
<g/>
1490	[number]	k4	1490
<g/>
)	)	kIx)	)
–	–	k?	–
olej	olej	k1gInSc1	olej
na	na	k7c6	na
dřevě	dřevo	k1gNnSc6	dřevo
43	[number]	k4	43
×	×	k?	×
31	[number]	k4	31
cm	cm	kA	cm
<g/>
,	,	kIx,	,
Milán	Milán	k1gInSc1	Milán
<g/>
,	,	kIx,	,
Pinacoteca	Pinacotec	k2eAgFnSc1d1	Pinacoteca
Ambrosiana	Ambrosiana	k1gFnSc1	Ambrosiana
Proporce	proporce	k1gFnSc2	proporce
lidské	lidský	k2eAgFnSc2d1	lidská
postavy	postava	k1gFnSc2	postava
(	(	kIx(	(
<g/>
Vitruvius	Vitruvius	k1gInSc1	Vitruvius
<g/>
)	)	kIx)	)
–	–	k?	–
pero	pero	k1gNnSc1	pero
na	na	k7c6	na
papíře	papír	k1gInSc6	papír
35	[number]	k4	35
×	×	k?	×
25	[number]	k4	25
cm	cm	kA	cm
<g/>
,	,	kIx,	,
Florencie	Florencie	k1gFnSc1	Florencie
<g/>
,	,	kIx,	,
Accademia	Accademia	k1gFnSc1	Accademia
Není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
žádné	žádný	k3yNgNnSc4	žádný
Leonardovo	Leonardův	k2eAgNnSc4d1	Leonardovo
zachovalé	zachovalý	k2eAgNnSc4d1	zachovalé
architektonické	architektonický	k2eAgNnSc4d1	architektonické
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
zápisnících	zápisník	k1gInPc6	zápisník
zanechal	zanechat	k5eAaPmAgMnS	zanechat
mnoho	mnoho	k4c1	mnoho
plánů	plán	k1gInPc2	plán
na	na	k7c4	na
nejrůznější	různý	k2eAgFnPc4d3	nejrůznější
budovy	budova	k1gFnPc4	budova
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
žádný	žádný	k3yNgInSc1	žádný
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
nerealizoval	realizovat	k5eNaBmAgInS	realizovat
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
řečeno	říct	k5eAaPmNgNnS	říct
výše	vysoce	k6eAd2	vysoce
<g/>
,	,	kIx,	,
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1487	[number]	k4	1487
neúspěšně	úspěšně	k6eNd1	úspěšně
soutěže	soutěž	k1gFnSc2	soutěž
na	na	k7c4	na
model	model	k1gInSc4	model
kupole	kupole	k1gFnSc2	kupole
nad	nad	k7c7	nad
křížením	křížení	k1gNnSc7	křížení
milánského	milánský	k2eAgInSc2d1	milánský
dómu	dóm	k1gInSc2	dóm
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zlomku	zlomek	k1gInSc2	zlomek
početných	početný	k2eAgInPc2d1	početný
analytických	analytický	k2eAgInPc2d1	analytický
náčrtků	náčrtek	k1gInPc2	náčrtek
se	se	k3xPyFc4	se
však	však	k9	však
dá	dát	k5eAaPmIp3nS	dát
tušit	tušit	k5eAaImF	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Leonardo	Leonardo	k1gMnSc1	Leonardo
zaníceně	zaníceně	k6eAd1	zaníceně
zabýval	zabývat	k5eAaImAgMnS	zabývat
teoretickým	teoretický	k2eAgNnPc3d1	teoretické
bádáním	bádání	k1gNnPc3	bádání
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
výsledkem	výsledek	k1gInSc7	výsledek
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
architektonický	architektonický	k2eAgInSc4d1	architektonický
ideál	ideál	k1gInSc4	ideál
<g/>
.	.	kIx.	.
</s>
<s>
Model	model	k1gInSc1	model
jezdeckého	jezdecký	k2eAgInSc2d1	jezdecký
pomníku	pomník	k1gInSc2	pomník
Francesca	Francesca	k1gMnSc1	Francesca
Sforzy	Sforza	k1gFnSc2	Sforza
–	–	k?	–
samotný	samotný	k2eAgInSc1d1	samotný
pomník	pomník	k1gInSc1	pomník
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
jeho	jeho	k3xOp3gFnSc7	jeho
monumentální	monumentální	k2eAgInSc1d1	monumentální
hliněný	hliněný	k2eAgInSc1d1	hliněný
předobraz	předobraz	k1gInSc1	předobraz
v	v	k7c6	v
nadživotní	nadživotní	k2eAgFnSc6d1	nadživotní
velikosti	velikost	k1gFnSc6	velikost
byl	být	k5eAaImAgMnS	být
ještě	ještě	k9	ještě
před	před	k7c7	před
odlitím	odlití	k1gNnSc7	odlití
do	do	k7c2	do
bronzu	bronz	k1gInSc2	bronz
zničen	zničit	k5eAaPmNgInS	zničit
francouzskými	francouzský	k2eAgInPc7d1	francouzský
vojáky	voják	k1gMnPc4	voják
při	při	k7c6	při
okupaci	okupace	k1gFnSc6	okupace
Milána	Milán	k1gInSc2	Milán
<g/>
.	.	kIx.	.
</s>
<s>
Leonardo	Leonardo	k1gMnSc1	Leonardo
se	se	k3xPyFc4	se
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
pokusil	pokusit	k5eAaPmAgMnS	pokusit
sepsat	sepsat	k5eAaPmF	sepsat
několik	několik	k4yIc4	několik
knih	kniha	k1gFnPc2	kniha
o	o	k7c6	o
různých	různý	k2eAgFnPc6d1	různá
oblastech	oblast	k1gFnPc6	oblast
svého	svůj	k3xOyFgNnSc2	svůj
poznání	poznání	k1gNnSc2	poznání
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
poznámky	poznámka	k1gFnPc1	poznámka
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
značně	značně	k6eAd1	značně
neucelené	ucelený	k2eNgFnPc1d1	neucelená
a	a	k8xC	a
roztříštěné	roztříštěný	k2eAgFnPc1d1	roztříštěná
<g/>
.	.	kIx.	.
</s>
<s>
Literární	literární	k2eAgInSc1d1	literární
odkaz	odkaz	k1gInSc1	odkaz
Leonardův	Leonardův	k2eAgInSc1d1	Leonardův
obnášející	obnášející	k2eAgInPc4d1	obnášející
asi	asi	k9	asi
7000	[number]	k4	7000
stránek	stránka	k1gFnPc2	stránka
byl	být	k5eAaImAgMnS	být
později	pozdě	k6eAd2	pozdě
sebrán	sebrán	k2eAgMnSc1d1	sebrán
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
jej	on	k3xPp3gInSc4	on
pod	pod	k7c7	pod
kodifikovanými	kodifikovaný	k2eAgInPc7d1	kodifikovaný
názvy	název	k1gInPc7	název
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
ve	v	k7c6	v
význačných	význačný	k2eAgFnPc6d1	význačná
galeriích	galerie	k1gFnPc6	galerie
či	či	k8xC	či
různých	různý	k2eAgFnPc6d1	různá
evropských	evropský	k2eAgFnPc6d1	Evropská
knihovnách	knihovna	k1gFnPc6	knihovna
ale	ale	k8xC	ale
i	i	k9	i
u	u	k7c2	u
soukromých	soukromý	k2eAgMnPc2d1	soukromý
sběratelů	sběratel	k1gMnPc2	sběratel
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Bill	Bill	k1gMnSc1	Bill
Gates	Gates	k1gMnSc1	Gates
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Leonardo	Leonardo	k1gMnSc1	Leonardo
připravoval	připravovat	k5eAaImAgMnS	připravovat
veledílo	veledílo	k1gNnSc1	veledílo
o	o	k7c6	o
anatomii	anatomie	k1gFnSc6	anatomie
<g/>
,	,	kIx,	,
malířství	malířství	k1gNnSc6	malířství
<g/>
,	,	kIx,	,
přírodě	příroda	k1gFnSc3	příroda
<g/>
,	,	kIx,	,
světlu	světlo	k1gNnSc3	světlo
a	a	k8xC	a
stínu	stín	k1gInSc3	stín
<g/>
.	.	kIx.	.
</s>
<s>
Bohužel	bohužel	k9	bohužel
však	však	k9	však
žádný	žádný	k3yNgMnSc1	žádný
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
poznámkových	poznámkový	k2eAgInPc2d1	poznámkový
cyklů	cyklus	k1gInPc2	cyklus
nedospěl	dochvít	k5eNaPmAgInS	dochvít
k	k	k7c3	k
ucelenější	ucelený	k2eAgFnSc3d2	ucelenější
a	a	k8xC	a
publikovatelné	publikovatelný	k2eAgFnSc3d1	publikovatelná
formě	forma	k1gFnSc3	forma
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
psal	psát	k5eAaImAgInS	psát
levou	levý	k2eAgFnSc7d1	levá
rukou	ruka	k1gFnSc7	ruka
zrcadlově	zrcadlově	k6eAd1	zrcadlově
obráceně	obráceně	k6eAd1	obráceně
zprava	zprava	k6eAd1	zprava
doleva	doleva	k6eAd1	doleva
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
neobvyklé	obvyklý	k2eNgNnSc1d1	neobvyklé
nejen	nejen	k6eAd1	nejen
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
jeho	jeho	k3xOp3gFnSc2	jeho
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Kdesi	kdesi	k6eAd1	kdesi
se	se	k3xPyFc4	se
Leonardo	Leonardo	k1gMnSc1	Leonardo
zmínil	zmínit	k5eAaPmAgMnS	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
napsal	napsat	k5eAaBmAgMnS	napsat
120	[number]	k4	120
resp.	resp.	kA	resp.
114	[number]	k4	114
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Míněny	míněn	k2eAgInPc1d1	míněn
jsou	být	k5eAaImIp3nP	být
patrně	patrně	k6eAd1	patrně
poznámkové	poznámkový	k2eAgInPc4d1	poznámkový
bloky	blok	k1gInPc4	blok
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
psal	psát	k5eAaImAgInS	psát
florentským	florentský	k2eAgNnSc7d1	florentské
nářečím	nářečí	k1gNnSc7	nářečí
tedy	tedy	k8xC	tedy
jazykem	jazyk	k1gInSc7	jazyk
obecným	obecný	k2eAgInSc7d1	obecný
(	(	kIx(	(
<g/>
volgare	volgar	k1gMnSc5	volgar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
letu	let	k1gInSc6	let
ptáků	pták	k1gMnPc2	pták
O	o	k7c6	o
pohybu	pohyb	k1gInSc6	pohyb
a	a	k8xC	a
měření	měření	k1gNnSc6	měření
vody	voda	k1gFnSc2	voda
O	o	k7c6	o
malířství	malířství	k1gNnSc6	malířství
O	o	k7c6	o
perspektivě	perspektiva	k1gFnSc6	perspektiva
a	a	k8xC	a
proporcích	proporce	k1gFnPc6	proporce
Značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
věnoval	věnovat	k5eAaImAgMnS	věnovat
Leonardo	Leonardo	k1gMnSc1	Leonardo
studiu	studio	k1gNnSc3	studio
anatomie	anatomie	k1gFnSc1	anatomie
<g/>
.	.	kIx.	.
</s>
<s>
Nezůstalo	zůstat	k5eNaPmAgNnS	zůstat
jen	jen	k9	jen
u	u	k7c2	u
lidské	lidský	k2eAgFnSc2d1	lidská
anatomie	anatomie	k1gFnSc2	anatomie
<g/>
,	,	kIx,	,
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
náčrtcích	náčrtek	k1gInPc6	náčrtek
můžeme	moct	k5eAaImIp1nP	moct
nalézt	nalézt	k5eAaPmF	nalézt
i	i	k9	i
koně	kůň	k1gMnPc1	kůň
a	a	k8xC	a
jiná	jiný	k2eAgNnPc1d1	jiné
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Plánoval	plánovat	k5eAaImAgMnS	plánovat
ostatně	ostatně	k6eAd1	ostatně
jako	jako	k8xC	jako
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
svých	svůj	k3xOyFgFnPc2	svůj
poznámek	poznámka	k1gFnPc2	poznámka
vydání	vydání	k1gNnSc2	vydání
souborného	souborný	k2eAgInSc2d1	souborný
spisu	spis	k1gInSc2	spis
<g/>
.	.	kIx.	.
</s>
<s>
Leonardo	Leonardo	k1gMnSc1	Leonardo
získával	získávat	k5eAaImAgMnS	získávat
vědomosti	vědomost	k1gFnPc4	vědomost
o	o	k7c6	o
stavbě	stavba	k1gFnSc6	stavba
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
z	z	k7c2	z
četných	četný	k2eAgFnPc2d1	četná
pitev	pitva	k1gFnPc2	pitva
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
prováděl	provádět	k5eAaImAgInS	provádět
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
ovšem	ovšem	k9	ovšem
udán	udán	k2eAgInSc1d1	udán
svými	svůj	k3xOyFgMnPc7	svůj
odpůrci	odpůrce	k1gMnPc7	odpůrce
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gInSc1	jeho
výzkum	výzkum	k1gInSc1	výzkum
zakázán	zakázán	k2eAgInSc1d1	zakázán
papežem	papež	k1gMnSc7	papež
Lvem	Lev	k1gMnSc7	Lev
X.	X.	kA	X.
Přesto	přesto	k8xC	přesto
svým	svůj	k3xOyFgNnSc7	svůj
studiem	studio	k1gNnSc7	studio
výrazně	výrazně	k6eAd1	výrazně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
tehdejší	tehdejší	k2eAgInPc4d1	tehdejší
anatomické	anatomický	k2eAgInPc4d1	anatomický
a	a	k8xC	a
fyziologické	fyziologický	k2eAgInPc4d1	fyziologický
názory	názor	k1gInPc4	názor
<g/>
.	.	kIx.	.
</s>
<s>
Leonardovy	Leonardův	k2eAgInPc1d1	Leonardův
rukopisy	rukopis	k1gInPc1	rukopis
a	a	k8xC	a
kresby	kresba	k1gFnPc1	kresba
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
května	květen	k1gInSc2	květen
2007	[number]	k4	2007
zpřístupněny	zpřístupněn	k2eAgInPc4d1	zpřístupněn
zdarma	zdarma	k6eAd1	zdarma
na	na	k7c6	na
digitálním	digitální	k2eAgInSc6d1	digitální
archivu	archiv	k1gInSc6	archiv
E-Leo	E-Leo	k6eAd1	E-Leo
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
založila	založit	k5eAaPmAgFnS	založit
Knihovna	knihovna	k1gFnSc1	knihovna
Leonarda	Leonardo	k1gMnSc2	Leonardo
da	da	k?	da
Vinciho	Vinci	k1gMnSc2	Vinci
ve	v	k7c4	v
Vinci	Vinca	k1gMnPc4	Vinca
financovaný	financovaný	k2eAgInSc1d1	financovaný
společně	společně	k6eAd1	společně
Leonardovou	Leonardův	k2eAgFnSc7d1	Leonardova
knihovnou	knihovna	k1gFnSc7	knihovna
a	a	k8xC	a
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
knihovna	knihovna	k1gFnSc1	knihovna
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
část	část	k1gFnSc4	část
jeho	on	k3xPp3gNnSc2	on
impozantního	impozantní	k2eAgNnSc2d1	impozantní
díla	dílo	k1gNnSc2	dílo
resp.	resp.	kA	resp.
mnoho	mnoho	k6eAd1	mnoho
tisíc	tisíc	k4xCgInPc2	tisíc
svazků	svazek	k1gInPc2	svazek
leonardovských	leonardovský	k2eAgFnPc2d1	leonardovský
monografií	monografie	k1gFnPc2	monografie
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
E-Leo	E-Leo	k6eAd1	E-Leo
má	mít	k5eAaImIp3nS	mít
adresu	adresa	k1gFnSc4	adresa
www.leonardodigitale.com	www.leonardodigitale.com	k1gInSc1	www.leonardodigitale.com
<g/>
.	.	kIx.	.
</s>
<s>
Leonardovy	Leonardův	k2eAgInPc4d1	Leonardův
vynálezy	vynález	k1gInPc4	vynález
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
základních	základní	k2eAgFnPc2d1	základní
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
létací	létací	k2eAgInPc1d1	létací
stroje	stroj	k1gInPc1	stroj
pracovní	pracovní	k2eAgInPc1d1	pracovní
nástroje	nástroj	k1gInPc1	nástroj
válečné	válečný	k2eAgInPc1d1	válečný
stroje	stroj	k1gInPc1	stroj
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
související	související	k2eAgNnSc4d1	související
vybavení	vybavení	k1gNnSc4	vybavení
Leonardo	Leonardo	k1gMnSc1	Leonardo
<g/>
,	,	kIx,	,
fascinovaný	fascinovaný	k2eAgInSc1d1	fascinovaný
fenoménem	fenomén	k1gInSc7	fenomén
létání	létání	k1gNnSc2	létání
<g/>
,	,	kIx,	,
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
podrobné	podrobný	k2eAgFnSc2d1	podrobná
studie	studie	k1gFnSc2	studie
letu	let	k1gInSc2	let
ptáků	pták	k1gMnPc2	pták
a	a	k8xC	a
plány	plán	k1gInPc1	plán
na	na	k7c4	na
sestrojení	sestrojení	k1gNnSc4	sestrojení
několika	několik	k4yIc2	několik
létajících	létající	k2eAgInPc2d1	létající
strojů	stroj	k1gInPc2	stroj
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
helikoptéry	helikoptéra	k1gFnSc2	helikoptéra
poháněné	poháněný	k2eAgFnSc2d1	poháněná
čtyřmi	čtyři	k4xCgNnPc7	čtyři
lidmi	člověk	k1gMnPc7	člověk
(	(	kIx(	(
<g/>
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
patrně	patrně	k6eAd1	patrně
nefungovala	fungovat	k5eNaImAgFnS	fungovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
by	by	kYmCp3nS	by
začala	začít	k5eAaPmAgFnS	začít
celá	celý	k2eAgFnSc1d1	celá
rotovat	rotovat	k5eAaImF	rotovat
<g/>
)	)	kIx)	)
a	a	k8xC	a
lehkého	lehký	k2eAgNnSc2d1	lehké
rogalla	rogallo	k1gNnSc2	rogallo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
poznámkách	poznámka	k1gFnPc6	poznámka
se	se	k3xPyFc4	se
též	též	k9	též
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
náčrt	náčrt	k1gInSc1	náčrt
létacího	létací	k2eAgInSc2d1	létací
stroje	stroj	k1gInSc2	stroj
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgNnSc6	který
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
povšimnutí	povšimnutí	k1gNnSc4	povšimnutí
precizní	precizní	k2eAgNnSc4d1	precizní
propracování	propracování	k1gNnSc4	propracování
koncového	koncový	k2eAgNnSc2d1	koncové
kormidla	kormidlo	k1gNnSc2	kormidlo
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1496	[number]	k4	1496
neúspěšně	úspěšně	k6eNd1	úspěšně
vyzkoušel	vyzkoušet	k5eAaPmAgInS	vyzkoušet
létací	létací	k2eAgInSc1d1	létací
stroj	stroj	k1gInSc1	stroj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
sám	sám	k3xTgMnSc1	sám
sestrojil	sestrojit	k5eAaPmAgMnS	sestrojit
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
konstrukcí	konstrukce	k1gFnPc2	konstrukce
napodobujících	napodobující	k2eAgNnPc2d1	napodobující
křídla	křídlo	k1gNnSc2	křídlo
ptáků	pták	k1gMnPc2	pták
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
též	též	k9	též
jednu	jeden	k4xCgFnSc4	jeden
podle	podle	k7c2	podle
netopýřích	netopýří	k2eAgFnPc2d1	netopýří
křídel	křídlo	k1gNnPc2	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
poznámkách	poznámka	k1gFnPc6	poznámka
se	se	k3xPyFc4	se
též	též	k9	též
zabýval	zabývat	k5eAaImAgInS	zabývat
myšlenkou	myšlenka	k1gFnSc7	myšlenka
padáku	padák	k1gInSc2	padák
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
však	však	k9	však
rozpracoval	rozpracovat	k5eAaPmAgInS	rozpracovat
jen	jen	k9	jen
teoreticky	teoreticky	k6eAd1	teoreticky
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
pohon	pohon	k1gInSc4	pohon
pro	pro	k7c4	pro
svoje	svůj	k3xOyFgInPc4	svůj
létací	létací	k2eAgInPc4d1	létací
stroje	stroj	k1gInPc4	stroj
častokrát	častokrát	k6eAd1	častokrát
využíval	využívat	k5eAaPmAgInS	využívat
lidskou	lidský	k2eAgFnSc4d1	lidská
sílu	síla	k1gFnSc4	síla
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pedály	pedál	k1gInPc1	pedál
jako	jako	k9	jako
na	na	k7c6	na
kole	kolo	k1gNnSc6	kolo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
si	se	k3xPyFc3	se
uvědomoval	uvědomovat	k5eAaImAgMnS	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
fungovat	fungovat	k5eAaImF	fungovat
jen	jen	k9	jen
teoreticky	teoreticky	k6eAd1	teoreticky
<g/>
.	.	kIx.	.
</s>
<s>
Vynalezl	vynaleznout	k5eAaPmAgInS	vynaleznout
anemometr	anemometr	k1gInSc1	anemometr
na	na	k7c4	na
zjišťování	zjišťování	k1gNnSc4	zjišťování
rychlosti	rychlost	k1gFnSc2	rychlost
větru	vítr	k1gInSc2	vítr
<g/>
,	,	kIx,	,
anemoskop	anemoskop	k1gInSc1	anemoskop
na	na	k7c4	na
zjišťování	zjišťování	k1gNnSc4	zjišťování
směru	směr	k1gInSc2	směr
větru	vítr	k1gInSc2	vítr
a	a	k8xC	a
inklinometr	inklinometr	k1gInSc1	inklinometr
na	na	k7c4	na
kontrolu	kontrola	k1gFnSc4	kontrola
vodorovné	vodorovný	k2eAgFnSc2d1	vodorovná
pozice	pozice	k1gFnSc2	pozice
při	při	k7c6	při
letu	let	k1gInSc6	let
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
deníky	deník	k1gInPc1	deník
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
též	též	k9	též
několik	několik	k4yIc4	několik
vynálezů	vynález	k1gInPc2	vynález
vojenského	vojenský	k2eAgInSc2d1	vojenský
charakteru	charakter	k1gInSc2	charakter
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
kulomety	kulomet	k1gInPc1	kulomet
<g/>
,	,	kIx,	,
obrněný	obrněný	k2eAgInSc4d1	obrněný
tank	tank	k1gInSc4	tank
s	s	k7c7	s
lidským	lidský	k2eAgInSc7d1	lidský
či	či	k8xC	či
koňským	koňský	k2eAgInSc7d1	koňský
pohonem	pohon	k1gInSc7	pohon
či	či	k8xC	či
klastrovou	klastrový	k2eAgFnSc4d1	klastrová
bombu	bomba	k1gFnSc4	bomba
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokládal	pokládat	k5eAaImAgMnS	pokládat
války	válka	k1gFnPc4	válka
za	za	k7c4	za
nejhorší	zlý	k2eAgFnSc4d3	nejhorší
lidskou	lidský	k2eAgFnSc4d1	lidská
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Zkonstruoval	zkonstruovat	k5eAaPmAgInS	zkonstruovat
i	i	k9	i
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
bojového	bojový	k2eAgNnSc2d1	bojové
děla	dělo	k1gNnSc2	dělo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
možnost	možnost	k1gFnSc4	možnost
nastavení	nastavení	k1gNnSc2	nastavení
úhlu	úhel	k1gInSc2	úhel
a	a	k8xC	a
výšky	výška	k1gFnSc2	výška
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
pohyblivé	pohyblivý	k2eAgNnSc1d1	pohyblivé
a	a	k8xC	a
využitelné	využitelný	k2eAgNnSc1d1	využitelné
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
.	.	kIx.	.
</s>
<s>
Oprášil	oprášit	k5eAaPmAgMnS	oprášit
středověké	středověký	k2eAgInPc4d1	středověký
náčrty	náčrt	k1gInPc4	náčrt
a	a	k8xC	a
vylepšil	vylepšit	k5eAaPmAgMnS	vylepšit
beranidlo	beranidlo	k1gNnSc4	beranidlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
používat	používat	k5eAaImF	používat
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
hradby	hradba	k1gFnPc4	hradba
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Leonardem	Leonardo	k1gMnSc7	Leonardo
navržený	navržený	k2eAgInSc4d1	navržený
tank	tank	k1gInSc4	tank
vypadal	vypadat	k5eAaImAgInS	vypadat
jako	jako	k8xC	jako
obrovská	obrovský	k2eAgFnSc1d1	obrovská
kovová	kovový	k2eAgFnSc1d1	kovová
kobliha	kobliha	k1gFnSc1	kobliha
na	na	k7c6	na
kolech	kolo	k1gNnPc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
se	se	k3xPyFc4	se
řítit	řítit	k5eAaImF	řítit
řadami	řada	k1gFnPc7	řada
nepřátel	nepřítel	k1gMnPc2	nepřítel
a	a	k8xC	a
rozrážet	rozrážet	k5eAaImF	rozrážet
je	být	k5eAaImIp3nS	být
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
krýt	krýt	k5eAaImF	krýt
vlastní	vlastní	k2eAgMnPc4d1	vlastní
vojáky	voják	k1gMnPc4	voják
<g/>
,	,	kIx,	,
ženoucí	ženoucí	k2eAgMnSc1d1	ženoucí
se	se	k3xPyFc4	se
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
aby	aby	kYmCp3nS	aby
nepřítel	nepřítel	k1gMnSc1	nepřítel
věděl	vědět	k5eAaImAgMnS	vědět
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
tuto	tento	k3xDgFnSc4	tento
hrůzu	hrůza	k1gFnSc4	hrůza
<g/>
,	,	kIx,	,
měli	mít	k5eAaImAgMnP	mít
střelci	střelec	k1gMnPc1	střelec
nahoře	nahoře	k6eAd1	nahoře
v	v	k7c6	v
kuželovité	kuželovitý	k2eAgFnSc6d1	kuželovitá
věži	věž	k1gFnSc6	věž
při	při	k7c6	při
postupu	postup	k1gInSc6	postup
vpřed	vpřed	k6eAd1	vpřed
pálit	pálit	k5eAaImF	pálit
ze	z	k7c2	z
střílen	střílna	k1gFnPc2	střílna
<g/>
.	.	kIx.	.
</s>
<s>
Tank	tank	k1gInSc1	tank
<g/>
,	,	kIx,	,
chráněný	chráněný	k2eAgInSc1d1	chráněný
kovovými	kovový	k2eAgInPc7d1	kovový
pláty	plát	k1gInPc7	plát
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
pohánět	pohánět	k5eAaImF	pohánět
osm	osm	k4xCc4	osm
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
by	by	k9	by
vměstnáni	vměstnán	k2eAgMnPc1d1	vměstnán
uvnitř	uvnitř	k6eAd1	uvnitř
točili	točit	k5eAaImAgMnP	točit
klikou	klika	k1gFnSc7	klika
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
pohyb	pohyb	k1gInSc1	pohyb
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
přenášel	přenášet	k5eAaImAgInS	přenášet
na	na	k7c4	na
kola	kolo	k1gNnPc4	kolo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
jednu	jeden	k4xCgFnSc4	jeden
chvíli	chvíle	k1gFnSc4	chvíle
Leonardo	Leonardo	k1gMnSc1	Leonardo
dokonce	dokonce	k9	dokonce
přemýšlel	přemýšlet	k5eAaImAgMnS	přemýšlet
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
místo	místo	k7c2	místo
mužů	muž	k1gMnPc2	muž
nasadil	nasadit	k5eAaPmAgMnS	nasadit
do	do	k7c2	do
tanku	tank	k1gInSc2	tank
koně	kůň	k1gMnPc4	kůň
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
by	by	kYmCp3nP	by
prostě	prostě	k6eAd1	prostě
jen	jen	k9	jen
cválali	cválat	k5eAaImAgMnP	cválat
a	a	k8xC	a
jeli	jet	k5eAaImAgMnP	jet
do	do	k7c2	do
bitvy	bitva	k1gFnSc2	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
tento	tento	k3xDgInSc4	tento
nápad	nápad	k1gInSc4	nápad
Leonardo	Leonardo	k1gMnSc1	Leonardo
zavrhl	zavrhnout	k5eAaPmAgMnS	zavrhnout
<g/>
.	.	kIx.	.
</s>
<s>
Trvalo	trvat	k5eAaImAgNnS	trvat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
čtyři	čtyři	k4xCgNnPc1	čtyři
století	století	k1gNnPc1	století
<g/>
,	,	kIx,	,
než	než	k8xS	než
lidé	člověk	k1gMnPc1	člověk
postavili	postavit	k5eAaPmAgMnP	postavit
tanky	tank	k1gInPc7	tank
podobného	podobný	k2eAgInSc2d1	podobný
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
tanky	tank	k1gInPc1	tank
vyrazily	vyrazit	k5eAaPmAgInP	vyrazit
do	do	k7c2	do
akce	akce	k1gFnSc2	akce
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
při	při	k7c6	při
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Somme	Somm	k1gInSc5	Somm
během	běh	k1gInSc7	běh
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
postupovaly	postupovat	k5eAaImAgFnP	postupovat
úplně	úplně	k6eAd1	úplně
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
to	ten	k3xDgNnSc4	ten
kdysi	kdysi	k6eAd1	kdysi
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Leonardo	Leonardo	k1gMnSc1	Leonardo
-	-	kIx~	-
postupovaly	postupovat	k5eAaImAgFnP	postupovat
po	po	k7c6	po
bitevním	bitevní	k2eAgNnSc6d1	bitevní
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
ničily	ničit	k5eAaImAgFnP	ničit
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k8xS	co
jim	on	k3xPp3gMnPc3	on
stálo	stát	k5eAaImAgNnS	stát
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
<g/>
,	,	kIx,	,
a	a	k8xC	a
za	za	k7c7	za
nimi	on	k3xPp3gInPc7	on
klusali	klusat	k5eAaImAgMnP	klusat
vojáci	voják	k1gMnPc1	voják
<g/>
,	,	kIx,	,
kterým	který	k3yIgFnPc3	který
sloužily	sloužit	k5eAaImAgInP	sloužit
tanky	tank	k1gInPc1	tank
jako	jako	k8xC	jako
ochrana	ochrana	k1gFnSc1	ochrana
před	před	k7c7	před
nepřátelskou	přátelský	k2eNgFnSc7d1	nepřátelská
palbou	palba	k1gFnSc7	palba
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
vynálezy	vynález	k1gInPc1	vynález
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
ponorku	ponorka	k1gFnSc4	ponorka
<g/>
,	,	kIx,	,
zařízení	zařízení	k1gNnPc4	zařízení
s	s	k7c7	s
ozubenými	ozubený	k2eAgInPc7d1	ozubený
kolečky	koleček	k1gInPc7	koleček
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
první	první	k4xOgFnSc4	první
mechanickou	mechanický	k2eAgFnSc4d1	mechanická
kalkulačku	kalkulačka	k1gFnSc4	kalkulačka
<g/>
,	,	kIx,	,
a	a	k8xC	a
auto	auto	k1gNnSc1	auto
poháněné	poháněný	k2eAgNnSc1d1	poháněné
pružinovým	pružinový	k2eAgInSc7d1	pružinový
mechanismem	mechanismus	k1gInSc7	mechanismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čase	čas	k1gInSc6	čas
svého	svůj	k3xOyFgInSc2	svůj
pobytu	pobyt	k1gInSc2	pobyt
ve	v	k7c6	v
Vatikánu	Vatikán	k1gInSc6	Vatikán
naplánoval	naplánovat	k5eAaBmAgMnS	naplánovat
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
využití	využití	k1gNnSc4	využití
sluneční	sluneční	k2eAgFnSc2d1	sluneční
energie	energie	k1gFnSc2	energie
pomocí	pomocí	k7c2	pomocí
parabolických	parabolický	k2eAgNnPc2d1	parabolické
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
na	na	k7c6	na
ohřívání	ohřívání	k1gNnSc6	ohřívání
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
těchto	tento	k3xDgInPc2	tento
vynálezů	vynález	k1gInPc2	vynález
vynalezl	vynaleznout	k5eAaPmAgInS	vynaleznout
skafandr	skafandr	k1gInSc1	skafandr
<g/>
,	,	kIx,	,
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
studiích	studie	k1gFnPc6	studie
jej	on	k3xPp3gInSc2	on
do	do	k7c2	do
detailů	detail	k1gInPc2	detail
rozpracoval	rozpracovat	k5eAaPmAgInS	rozpracovat
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
pozůstával	pozůstávat	k5eAaImAgMnS	pozůstávat
jen	jen	k9	jen
ze	z	k7c2	z
zvonu	zvon	k1gInSc2	zvon
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
plovacích	plovací	k2eAgFnPc2d1	plovací
ploutví	ploutev	k1gFnPc2	ploutev
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
později	pozdě	k6eAd2	pozdě
domyslel	domyslet	k5eAaPmAgInS	domyslet
kožený	kožený	k2eAgInSc1d1	kožený
oděv	oděv	k1gInSc1	oděv
určený	určený	k2eAgInSc1d1	určený
na	na	k7c4	na
pobyt	pobyt	k1gInSc4	pobyt
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
zkonstruoval	zkonstruovat	k5eAaPmAgMnS	zkonstruovat
vrtačku	vrtačka	k1gFnSc4	vrtačka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
sestávala	sestávat	k5eAaImAgFnS	sestávat
z	z	k7c2	z
hlavice	hlavice	k1gFnSc2	hlavice
<g/>
,	,	kIx,	,
kolečkového	kolečkový	k2eAgInSc2d1	kolečkový
mechanismu	mechanismus	k1gInSc2	mechanismus
a	a	k8xC	a
kliky	klika	k1gFnSc2	klika
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
otáčela	otáčet	k5eAaImAgFnS	otáčet
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejfuturističtějších	futuristický	k2eAgInPc2d3	futuristický
vynálezů	vynález	k1gInPc2	vynález
jeho	jeho	k3xOp3gFnSc2	jeho
doby	doba	k1gFnSc2	doba
patří	patřit	k5eAaImIp3nS	patřit
bicykl	bicykl	k1gInSc1	bicykl
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1817	[number]	k4	1817
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
konstruovat	konstruovat	k5eAaImF	konstruovat
první	první	k4xOgMnPc1	první
jízdní	jízdný	k1gMnPc1	jízdný
kolo	kolo	k1gNnSc1	kolo
(	(	kIx(	(
<g/>
převody	převod	k1gInPc1	převod
a	a	k8xC	a
řetězy	řetěz	k1gInPc1	řetěz
byly	být	k5eAaImAgInP	být
dodány	dodat	k5eAaPmNgInP	dodat
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
a	a	k8xC	a
celý	celý	k2eAgInSc1d1	celý
proces	proces	k1gInSc1	proces
výroby	výroba	k1gFnSc2	výroba
pomocí	pomocí	k7c2	pomocí
techniků	technik	k1gMnPc2	technik
<g/>
,	,	kIx,	,
továren	továrna	k1gFnPc2	továrna
a	a	k8xC	a
podnikatelů	podnikatel	k1gMnPc2	podnikatel
trvá	trvat	k5eAaImIp3nS	trvat
přes	přes	k7c4	přes
80	[number]	k4	80
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
Leonardo	Leonardo	k1gMnSc1	Leonardo
ho	on	k3xPp3gMnSc4	on
před	před	k7c7	před
čtyřmi	čtyři	k4xCgNnPc7	čtyři
sty	sto	k4xCgNnPc7	sto
lety	léto	k1gNnPc7	léto
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
jediným	jediný	k2eAgInSc7d1	jediný
skokem	skok	k1gInSc7	skok
<g/>
.	.	kIx.	.
</s>
<s>
Leonardovým	Leonardův	k2eAgMnSc7d1	Leonardův
žákem	žák	k1gMnSc7	žák
byl	být	k5eAaImAgMnS	být
velice	velice	k6eAd1	velice
zkušený	zkušený	k2eAgMnSc1d1	zkušený
a	a	k8xC	a
znalý	znalý	k2eAgMnSc1d1	znalý
Giovanni	Giovann	k1gMnPc1	Giovann
Antonio	Antonio	k1gMnSc1	Antonio
Boltraffio	Boltraffio	k1gMnSc1	Boltraffio
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1500	[number]	k4	1500
namaloval	namalovat	k5eAaPmAgMnS	namalovat
pro	pro	k7c4	pro
kostel	kostel	k1gInSc4	kostel
Milosrdenství	milosrdenství	k1gNnSc2	milosrdenství
u	u	k7c2	u
Bologne	Bologna	k1gFnSc2	Bologna
olejomalbu	olejomalba	k1gFnSc4	olejomalba
Madona	Madona	k1gFnSc1	Madona
s	s	k7c7	s
dítětem	dítě	k1gNnSc7	dítě
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Janem	Jan	k1gMnSc7	Jan
Křtitelem	křtitel	k1gMnSc7	křtitel
<g/>
,	,	kIx,	,
malým	malý	k1gMnSc7	malý
sv.	sv.	kA	sv.
Šebestiánem	Šebestián	k1gMnSc7	Šebestián
<g/>
.	.	kIx.	.
</s>
<s>
Boltraffio	Boltraffio	k1gMnSc1	Boltraffio
jej	on	k3xPp3gNnSc4	on
signoval	signovat	k5eAaBmAgMnS	signovat
svým	svůj	k3xOyFgNnSc7	svůj
jménem	jméno	k1gNnSc7	jméno
s	s	k7c7	s
poznámkou	poznámka	k1gFnSc7	poznámka
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Leonardovým	Leonardův	k2eAgMnSc7d1	Leonardův
žákem	žák	k1gMnSc7	žák
<g/>
.	.	kIx.	.
</s>
<s>
Udělal	udělat	k5eAaPmAgMnS	udělat
i	i	k9	i
jiné	jiný	k2eAgFnPc4d1	jiná
práce	práce	k1gFnPc4	práce
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
rodném	rodný	k2eAgInSc6d1	rodný
Miláně	Milán	k1gInSc6	Milán
a	a	k8xC	a
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
žákem	žák	k1gMnSc7	žák
byl	být	k5eAaImAgMnS	být
Marco	Marco	k6eAd1	Marco
Uggioni	Uggioň	k1gFnSc3	Uggioň
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
namaloval	namalovat	k5eAaPmAgMnS	namalovat
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
Santa	Santa	k1gFnSc1	Santa
Maria	Maria	k1gFnSc1	Maria
della	della	k1gFnSc1	della
Pace	Paka	k1gFnSc3	Paka
Smrt	smrt	k1gFnSc4	smrt
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
a	a	k8xC	a
Svatbu	svatba	k1gFnSc4	svatba
v	v	k7c6	v
Káni	káně	k1gFnSc6	káně
Galilejské	galilejský	k2eAgFnSc6d1	Galilejská
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
Leonardovým	Leonardův	k2eAgMnSc7d1	Leonardův
žákem	žák	k1gMnSc7	žák
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc1	jeho
adoptivní	adoptivní	k2eAgMnSc1d1	adoptivní
syn	syn	k1gMnSc1	syn
Salai	Sala	k1gFnSc2	Sala
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
neměl	mít	k5eNaImAgInS	mít
žádné	žádný	k3yNgFnPc4	žádný
výrazné	výrazný	k2eAgFnPc4d1	výrazná
výtvarné	výtvarný	k2eAgFnPc4d1	výtvarná
vlohy	vloha	k1gFnPc4	vloha
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
spekuluje	spekulovat	k5eAaImIp3nS	spekulovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
Leonardo	Leonardo	k1gMnSc1	Leonardo
přijal	přijmout	k5eAaPmAgMnS	přijmout
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gInSc4	jeho
pěkný	pěkný	k2eAgInSc4d1	pěkný
vzhled	vzhled	k1gInSc4	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Salai	Salae	k1gFnSc4	Salae
Leonarda	Leonardo	k1gMnSc2	Leonardo
mnohokrát	mnohokrát	k6eAd1	mnohokrát
okradl	okrást	k5eAaPmAgMnS	okrást
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gMnSc1	jeho
mistr	mistr	k1gMnSc1	mistr
byl	být	k5eAaImAgMnS	být
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
vždy	vždy	k6eAd1	vždy
velmi	velmi	k6eAd1	velmi
shovívavý	shovívavý	k2eAgInSc1d1	shovívavý
<g/>
.	.	kIx.	.
</s>
<s>
La	la	k1gNnPc1	la
Vita	vit	k2eAgNnPc1d1	Vito
di	di	k?	di
Leonardo	Leonardo	k1gMnSc1	Leonardo
Da	Da	k1gMnSc2	Da
Vinci	Vinca	k1gMnSc2	Vinca
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
italsko-španělský	italsko-španělský	k2eAgInSc1d1	italsko-španělský
životopisný	životopisný	k2eAgInSc1d1	životopisný
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
získal	získat	k5eAaPmAgInS	získat
2	[number]	k4	2
zlaté	zlatý	k2eAgInPc4d1	zlatý
glóby	glóbus	k1gInPc4	glóbus
(	(	kIx(	(
<g/>
IMDB	IMDB	kA	IMDB
<g/>
)	)	kIx)	)
V	v	k7c6	v
první	první	k4xOgFnSc6	první
sérii	série	k1gFnSc6	série
seriálu	seriál	k1gInSc2	seriál
Star	Star	kA	Star
Trek	Trek	k1gMnSc1	Trek
v	v	k7c6	v
epizodě	epizoda	k1gFnSc6	epizoda
Requiem	Requium	k1gNnSc7	Requium
for	forum	k1gNnPc2	forum
Methuselah	Methuselah	k1gMnSc1	Methuselah
je	být	k5eAaImIp3nS	být
jméno	jméno	k1gNnSc4	jméno
Leonardo	Leonardo	k1gMnSc1	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnPc4	Vinca
zmíněné	zmíněný	k2eAgMnPc4d1	zmíněný
jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
aliasů	alias	k1gInPc2	alias
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Flinta	flinta	k1gFnSc1	flinta
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nesmrtelného	smrtelný	k2eNgMnSc4d1	nesmrtelný
člověka	člověk	k1gMnSc4	člověk
narozeného	narozený	k2eAgMnSc4d1	narozený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
3834	[number]	k4	3834
před	před	k7c7	před
Kr.	Kr.	k1gFnSc7	Kr.
Leonardo	Leonardo	k1gMnSc1	Leonardo
<g/>
,	,	kIx,	,
ninja	ninj	k2eAgFnSc1d1	ninja
želva	želva	k1gFnSc1	želva
–	–	k?	–
zfilmovaná	zfilmovaný	k2eAgFnSc1d1	zfilmovaná
komiksová	komiksový	k2eAgFnSc1d1	komiksová
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
podle	podle	k7c2	podle
Leonarda	Leonardo	k1gMnSc2	Leonardo
Film	film	k1gInSc1	film
Ever	Ever	k1gMnSc1	Ever
After	After	k1gMnSc1	After
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
Drew	Drew	k1gFnSc2	Drew
Barrymore	Barrymor	k1gInSc5	Barrymor
a	a	k8xC	a
Patrick	Patrick	k1gMnSc1	Patrick
Godfrey	Godfrea	k1gFnSc2	Godfrea
jako	jako	k8xS	jako
Leonardo	Leonardo	k1gMnSc1	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnPc4	Vinca
Film	film	k1gInSc1	film
Hudson	Hudson	k1gMnSc1	Hudson
Hawk	Hawk	k1gMnSc1	Hawk
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
hlavní	hlavní	k2eAgFnSc2d1	hlavní
role	role	k1gFnSc2	role
Bruce	Bruce	k1gFnSc2	Bruce
Willis	Willis	k1gFnSc2	Willis
a	a	k8xC	a
Danny	Danna	k1gFnSc2	Danna
Aiello	Aiello	k1gNnSc1	Aiello
se	se	k3xPyFc4	se
točí	točit	k5eAaImIp3nS	točit
okolo	okolo	k7c2	okolo
Leonardových	Leonardův	k2eAgInPc2d1	Leonardův
vynálezů	vynález	k1gInPc2	vynález
Šifra	šifra	k1gFnSc1	šifra
mistra	mistr	k1gMnSc2	mistr
Leonarda	Leonardo	k1gMnSc2	Leonardo
–	–	k?	–
zfilmovaná	zfilmovaný	k2eAgFnSc1d1	zfilmovaná
stejnojmenná	stejnojmenný	k2eAgFnSc1d1	stejnojmenná
kniha	kniha	k1gFnSc1	kniha
od	od	k7c2	od
Dana	Dan	k1gMnSc2	Dan
Browna	Brown	k1gMnSc2	Brown
V	v	k7c6	v
divadelní	divadelní	k2eAgFnSc6d1	divadelní
hře	hra	k1gFnSc6	hra
Petra	Petr	k1gMnSc2	Petr
Barnse	Barns	k1gMnSc2	Barns
Leonardo	Leonardo	k1gMnSc1	Leonardo
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Last	Last	k1gMnSc1	Last
Supper	Supper	k1gMnSc1	Supper
(	(	kIx(	(
<g/>
Leonardova	Leonardův	k2eAgFnSc1d1	Leonardova
poslední	poslední	k2eAgFnSc1d1	poslední
večeře	večeře	k1gFnSc1	večeře
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
autor	autor	k1gMnSc1	autor
soustředí	soustředit	k5eAaPmIp3nS	soustředit
na	na	k7c4	na
Leonardovo	Leonardův	k2eAgNnSc4d1	Leonardovo
"	"	kIx"	"
<g/>
zmrtvýchvstání	zmrtvýchvstání	k1gNnSc4	zmrtvýchvstání
<g/>
"	"	kIx"	"
v	v	k7c6	v
márnici	márnice	k1gFnSc6	márnice
po	po	k7c6	po
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
byl	být	k5eAaImAgInS	být
prohlášený	prohlášený	k2eAgInSc1d1	prohlášený
za	za	k7c4	za
mrtvého	mrtvý	k1gMnSc4	mrtvý
Animovaná	animovaný	k2eAgFnSc1d1	animovaná
postava	postava	k1gFnSc1	postava
Léonard	Léonarda	k1gFnPc2	Léonarda
z	z	k7c2	z
kresleného	kreslený	k2eAgInSc2d1	kreslený
filmu	film	k1gInSc2	film
Dargaud	Dargaud	k1gInSc1	Dargaud
od	od	k7c2	od
Liegeoisa	Liegeois	k1gMnSc2	Liegeois
a	a	k8xC	a
De	De	k?	De
Groota	Groota	k1gFnSc1	Groota
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
Bathory	Bathora	k1gFnSc2	Bathora
představuje	představovat	k5eAaImIp3nS	představovat
nevinnou	vinný	k2eNgFnSc7d1	nevinná
Čachtickou	čachtický	k2eAgFnSc7d1	Čachtická
paní	paní	k1gFnSc7	paní
<g/>
,	,	kIx,	,
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
se	se	k3xPyFc4	se
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Leonardova	Leonardův	k2eAgInSc2d1	Leonardův
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
ve	v	k7c6	v
filmu	film	k1gInSc6	film
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
používány	používat	k5eAaImNgInP	používat
"	"	kIx"	"
<g/>
Leonardovy	Leonardův	k2eAgInPc1d1	Leonardův
vynálezy	vynález	k1gInPc1	vynález
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Animovaném	animovaný	k2eAgNnSc6d1	animované
seriálu	seriál	k1gInSc6	seriál
Futurama	Futurama	k?	Futurama
je	být	k5eAaImIp3nS	být
Leonardo	Leonardo	k1gMnSc1	Leonardo
návštěvník	návštěvník	k1gMnSc1	návštěvník
z	z	k7c2	z
cizí	cizí	k2eAgFnSc2d1	cizí
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c6	na
té	ten	k3xDgFnSc6	ten
své	svůj	k3xOyFgFnSc6	svůj
je	být	k5eAaImIp3nS	být
nejhloupější	hloupý	k2eAgInSc1d3	nejhloupější
a	a	k8xC	a
vyrobí	vyrobit	k5eAaPmIp3nS	vyrobit
stroj	stroj	k1gInSc1	stroj
zkázy	zkáza	k1gFnSc2	zkáza
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
porouchá	porouchat	k5eAaPmIp3nS	porouchat
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
Da	Da	k1gMnSc2	Da
Vinciho	Vinci	k1gMnSc2	Vinci
démoni	démon	k1gMnPc1	démon
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
-	-	kIx~	-
seriál	seriál	k1gInSc1	seriál
zabývající-se	zabývajícíe	k1gFnSc2	zabývající-se
Leonardovými	Leonardův	k2eAgInPc7d1	Leonardův
vynálezy	vynález	k1gInPc7	vynález
a	a	k8xC	a
děním	dění	k1gNnSc7	dění
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
za	za	k7c2	za
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
Leonardo	Leonardo	k1gMnSc1	Leonardo
-	-	kIx~	-
Britský	britský	k2eAgInSc1d1	britský
rodinný	rodinný	k2eAgInSc1d1	rodinný
seriál	seriál	k1gInSc1	seriál
o	o	k7c6	o
mladém	mladý	k2eAgMnSc6d1	mladý
Leonardovi	Leonardo	k1gMnSc6	Leonardo
Dan	Dan	k1gMnSc1	Dan
Brown	Brown	k1gMnSc1	Brown
<g/>
,	,	kIx,	,
Šifra	šifra	k1gFnSc1	šifra
mistra	mistr	k1gMnSc2	mistr
Leonarda	Leonardo	k1gMnSc2	Leonardo
Leo	Leo	k1gMnSc1	Leo
Perutz	Perutz	k1gMnSc1	Perutz
<g/>
,	,	kIx,	,
Leonardův	Leonardův	k2eAgMnSc1d1	Leonardův
Jidáš	Jidáš	k1gMnSc1	Jidáš
Povídka	povídka	k1gFnSc1	povídka
Theodora	Theodor	k1gMnSc2	Theodor
Mathiesona	Mathieson	k1gMnSc2	Mathieson
Leonardo	Leonardo	k1gMnSc1	Leonardo
Da	Da	k1gMnSc2	Da
Vinci	Vinca	k1gMnSc2	Vinca
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
Detective	Detectiv	k1gInSc5	Detectiv
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
Leonardo	Leonardo	k1gMnSc1	Leonardo
Da	Da	k1gMnSc2	Da
Vinci	Vinca	k1gMnSc2	Vinca
<g/>
:	:	kIx,	:
Detektiv	detektiv	k1gMnSc1	detektiv
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
jej	on	k3xPp3gNnSc4	on
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
řeší	řešit	k5eAaImIp3nS	řešit
vraždu	vražda	k1gFnSc4	vražda
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
pobytu	pobyt	k1gInSc2	pobyt
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
V	v	k7c6	v
románu	román	k1gInSc6	román
Pasquale	Pasquala	k1gFnSc3	Pasquala
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Angel	angel	k1gMnSc1	angel
Paul	Paul	k1gMnSc1	Paul
McAuley	McAulea	k1gFnSc2	McAulea
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
alternativní	alternativní	k2eAgInSc4d1	alternativní
svět	svět	k1gInSc4	svět
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
<g/>
,	,	kIx,	,
zobrazující	zobrazující	k2eAgFnSc6d1	zobrazující
Leonarda	Leonardo	k1gMnSc4	Leonardo
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Velkého	velký	k2eAgMnSc4d1	velký
konstruktéra	konstruktér	k1gMnSc4	konstruktér
<g/>
"	"	kIx"	"
Román	Román	k1gMnSc1	Román
Katedrála	katedrál	k1gMnSc2	katedrál
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
od	od	k7c2	od
Jacka	Jacek	k1gInSc2	Jacek
Danna	Dann	k1gInSc2	Dann
je	být	k5eAaImIp3nS	být
fikcí	fikce	k1gFnPc2	fikce
o	o	k7c6	o
"	"	kIx"	"
<g/>
ztracených	ztracený	k2eAgInPc6d1	ztracený
letech	let	k1gInPc6	let
<g/>
"	"	kIx"	"
v	v	k7c6	v
živote	život	k1gInSc5	život
Leonarda	Leonardo	k1gMnSc2	Leonardo
Postava	postava	k1gFnSc1	postava
Terryho	Terry	k1gMnSc2	Terry
Pratchetta	Pratchett	k1gMnSc2	Pratchett
Leonard	Leonard	k1gInSc1	Leonard
of	of	k?	of
Quirm	Quirm	k1gInSc1	Quirm
je	být	k5eAaImIp3nS	být
parodií	parodie	k1gFnSc7	parodie
na	na	k7c4	na
Leonarda	Leonardo	k1gMnSc4	Leonardo
<g/>
.	.	kIx.	.
</s>
<s>
Alex	Alex	k1gMnSc1	Alex
Parsonsová	Parsonsová	k1gFnSc1	Parsonsová
<g/>
,	,	kIx,	,
Leonardo	Leonardo	k1gMnSc1	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnSc2	Vinca
-	-	kIx~	-
Tajný	tajný	k2eAgInSc1d1	tajný
deník	deník	k1gInSc1	deník
Luigiho	Luigi	k1gMnSc2	Luigi
Caneloniho	Caneloni	k1gMnSc2	Caneloni
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc4	jeho
učně	učeň	k1gMnPc4	učeň
Javier	Javier	k1gInSc1	Javier
Sierra	Sierra	k1gFnSc1	Sierra
-	-	kIx~	-
Tajemství	tajemství	k1gNnSc1	tajemství
poslední	poslední	k2eAgFnSc2d1	poslední
večeře	večeře	k1gFnSc2	večeře
Dmitrij	Dmitrij	k1gMnSc1	Dmitrij
Sergejevič	Sergejevič	k1gMnSc1	Sergejevič
Merežkovskij	Merežkovskij	k1gMnSc1	Merežkovskij
-	-	kIx~	-
Leonardo	Leonardo	k1gMnSc1	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnSc2	Vinca
Christopher	Christophra	k1gFnPc2	Christophra
Grey	Grea	k1gMnSc2	Grea
-	-	kIx~	-
Leonardův	Leonardův	k2eAgInSc1d1	Leonardův
stín	stín	k1gInSc1	stín
(	(	kIx(	(
<g/>
život	život	k1gInSc1	život
sluhy	sluha	k1gMnSc2	sluha
Giacoma	Giacom	k1gMnSc2	Giacom
v	v	k7c6	v
Leonardových	Leonardův	k2eAgFnPc6d1	Leonardova
službách	služba	k1gFnPc6	služba
<g/>
)	)	kIx)	)
Raffael	Raffael	k1gInSc4	Raffael
<g/>
:	:	kIx,	:
Aténská	aténský	k2eAgFnSc1d1	aténská
škola	škola	k1gFnSc1	škola
–	–	k?	–
postava	postava	k1gFnSc1	postava
Platóna	Platón	k1gMnSc2	Platón
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
inspirována	inspirovat	k5eAaBmNgFnS	inspirovat
Leonardovou	Leonardův	k2eAgFnSc7d1	Leonardova
podobou	podoba	k1gFnSc7	podoba
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
