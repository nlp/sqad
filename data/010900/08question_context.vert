<s>
BuEV	BuEV	k?	BuEV
Danzig	Danzig	k1gInSc4	Danzig
(	(	kIx(	(
<g/>
celým	celý	k2eAgInSc7d1	celý
názvem	název	k1gInSc7	název
<g/>
:	:	kIx,	:
Ballspiel-	Ballspiel-	k1gMnSc1	Ballspiel-
und	und	k?	und
Eislauf-Verein	Eislauf-Verein	k1gMnSc1	Eislauf-Verein
Danzig	Danzig	k1gMnSc1	Danzig
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
německý	německý	k2eAgInSc1d1	německý
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
sídlil	sídlit	k5eAaImAgInS	sídlit
v	v	k7c6	v
západopruském	západopruský	k2eAgNnSc6d1	západopruský
městě	město	k1gNnSc6	město
Danzig	Danzig	k1gInSc4	Danzig
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc4d1	dnešní
Gdaňsk	Gdaňsk	k1gInSc4	Gdaňsk
v	v	k7c6	v
Pomořském	pomořský	k2eAgNnSc6d1	Pomořské
vojvodství	vojvodství	k1gNnSc6	vojvodství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Založen	založen	k2eAgMnSc1d1	založen
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
FC	FC	kA	FC
Danzig	Danzig	k1gInSc1	Danzig
<g/>
.	.	kIx.	.
</s>
