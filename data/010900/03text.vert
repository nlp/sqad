<p>
<s>
BuEV	BuEV	k?	BuEV
Danzig	Danzig	k1gInSc4	Danzig
(	(	kIx(	(
<g/>
celým	celý	k2eAgInSc7d1	celý
názvem	název	k1gInSc7	název
<g/>
:	:	kIx,	:
Ballspiel-	Ballspiel-	k1gMnSc1	Ballspiel-
und	und	k?	und
Eislauf-Verein	Eislauf-Verein	k1gMnSc1	Eislauf-Verein
Danzig	Danzig	k1gMnSc1	Danzig
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
německý	německý	k2eAgInSc1d1	německý
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
sídlil	sídlit	k5eAaImAgInS	sídlit
v	v	k7c6	v
západopruském	západopruský	k2eAgNnSc6d1	západopruský
městě	město	k1gNnSc6	město
Danzig	Danzig	k1gInSc4	Danzig
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc4d1	dnešní
Gdaňsk	Gdaňsk	k1gInSc4	Gdaňsk
v	v	k7c6	v
Pomořském	pomořský	k2eAgNnSc6d1	Pomořské
vojvodství	vojvodství	k1gNnSc6	vojvodství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Založen	založen	k2eAgMnSc1d1	založen
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
FC	FC	kA	FC
Danzig	Danzig	k1gInSc1	Danzig
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
baltským	baltský	k2eAgMnSc7d1	baltský
mistrem	mistr	k1gMnSc7	mistr
a	a	k8xC	a
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
právo	právo	k1gNnSc4	právo
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
se	se	k3xPyFc4	se
německého	německý	k2eAgNnSc2d1	německé
mistrovství	mistrovství	k1gNnSc2	mistrovství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
hned	hned	k6eAd1	hned
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
vypadl	vypadnout	k5eAaPmAgInS	vypadnout
s	s	k7c7	s
berlínskou	berlínský	k2eAgFnSc7d1	Berlínská
Viktorií	Viktoria	k1gFnSc7	Viktoria
poměrem	poměr	k1gInSc7	poměr
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
zakládajícím	zakládající	k2eAgMnSc7d1	zakládající
členem	člen	k1gMnSc7	člen
Gauligy	Gauliga	k1gFnSc2	Gauliga
Ostpreußen	Ostpreußna	k1gFnPc2	Ostpreußna
<g/>
.	.	kIx.	.
</s>
<s>
Zaniká	zanikat	k5eAaImIp3nS	zanikat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
po	po	k7c6	po
sovětsko-polské	sovětskoolský	k2eAgFnSc6d1	sovětsko-polská
anexi	anexe	k1gFnSc6	anexe
Pruska	Prusko	k1gNnSc2	Prusko
<g/>
.	.	kIx.	.
</s>
<s>
Klubové	klubový	k2eAgFnPc4d1	klubová
barvy	barva	k1gFnPc4	barva
byly	být	k5eAaImAgInP	být
černá	černý	k2eAgFnSc1d1	černá
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Své	svůj	k3xOyFgInPc4	svůj
domácí	domácí	k2eAgInPc4d1	domácí
zápasy	zápas	k1gInPc4	zápas
odehrával	odehrávat	k5eAaImAgInS	odehrávat
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
Sportplatz	Sportplatza	k1gFnPc2	Sportplatza
Reichskolonie	Reichskolonie	k1gFnSc2	Reichskolonie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historické	historický	k2eAgInPc1d1	historický
názvy	název	k1gInPc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
1903	[number]	k4	1903
–	–	k?	–
FC	FC	kA	FC
Danzig	Danzig	k1gMnSc1	Danzig
(	(	kIx(	(
<g/>
Fußballclub	Fußballclub	k1gMnSc1	Fußballclub
Danzig	Danzig	k1gMnSc1	Danzig
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1905	[number]	k4	1905
–	–	k?	–
BuEV	BuEV	k1gMnSc1	BuEV
Danzig	Danzig	k1gMnSc1	Danzig
(	(	kIx(	(
<g/>
Ballspiel-	Ballspiel-	k1gMnSc1	Ballspiel-
und	und	k?	und
Eislauf-Verein	Eislauf-Verein	k1gMnSc1	Eislauf-Verein
Danzig	Danzig	k1gMnSc1	Danzig
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1916	[number]	k4	1916
–	–	k?	–
VfL	VfL	k1gMnSc1	VfL
Danzig	Danzig	k1gMnSc1	Danzig
(	(	kIx(	(
<g/>
Verein	Verein	k1gMnSc1	Verein
für	für	k?	für
Leibesübungen	Leibesübungen	k1gInSc1	Leibesübungen
Danzig	Danzig	k1gInSc1	Danzig
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1930	[number]	k4	1930
–	–	k?	–
BuEV	BuEV	k1gMnSc1	BuEV
Danzig	Danzig	k1gMnSc1	Danzig
(	(	kIx(	(
<g/>
Ballspiel-	Ballspiel-	k1gMnSc1	Ballspiel-
und	und	k?	und
Eislauf-Verein	Eislauf-Verein	k1gMnSc1	Eislauf-Verein
Danzig	Danzig	k1gMnSc1	Danzig
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Získané	získaný	k2eAgFnPc4d1	získaná
trofeje	trofej	k1gFnPc4	trofej
==	==	k?	==
</s>
</p>
<p>
<s>
Baltische	Baltische	k1gFnSc1	Baltische
Fußballmeisterschaft	Fußballmeisterschafta	k1gFnPc2	Fußballmeisterschafta
(	(	kIx(	(
1	[number]	k4	1
<g/>
×	×	k?	×
)	)	kIx)	)
</s>
</p>
<p>
<s>
1911	[number]	k4	1911
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
</s>
</p>
<p>
<s>
==	==	k?	==
Umístění	umístění	k1gNnSc1	umístění
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
sezonách	sezona	k1gFnPc6	sezona
==	==	k?	==
</s>
</p>
<p>
<s>
Stručný	stručný	k2eAgInSc1d1	stručný
přehledZdroj	přehledZdroj	k1gInSc1	přehledZdroj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1933	[number]	k4	1933
<g/>
–	–	k?	–
<g/>
1935	[number]	k4	1935
<g/>
:	:	kIx,	:
Gauliga	Gauliga	k1gFnSc1	Gauliga
Ostpreußen	Ostpreußna	k1gFnPc2	Ostpreußna
-	-	kIx~	-
sk	sk	k?	sk
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
</s>
</p>
<p>
<s>
1935	[number]	k4	1935
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
<g/>
:	:	kIx,	:
Gauliga	Gauliga	k1gFnSc1	Gauliga
Ostpreußen	Ostpreußna	k1gFnPc2	Ostpreußna
-	-	kIx~	-
sk	sk	k?	sk
<g/>
.	.	kIx.	.
</s>
<s>
Danzig	Danzig	k1gMnSc1	Danzig
</s>
</p>
<p>
<s>
1938	[number]	k4	1938
<g/>
–	–	k?	–
<g/>
1940	[number]	k4	1940
<g/>
:	:	kIx,	:
Gauliga	Gauliga	k1gFnSc1	Gauliga
Ostpreußen	Ostpreußna	k1gFnPc2	Ostpreußna
</s>
</p>
<p>
<s>
1940	[number]	k4	1940
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
<g/>
:	:	kIx,	:
Gauliga	Gauliga	k1gFnSc1	Gauliga
Danzig-WestpreußenJednotlivé	Danzig-WestpreußenJednotlivý	k2eAgInPc4d1	Danzig-WestpreußenJednotlivý
ročníkyZdroj	ročníkyZdroj	k1gInSc4	ročníkyZdroj
<g/>
:	:	kIx,	:
Legenda	legenda	k1gFnSc1	legenda
<g/>
:	:	kIx,	:
Z	Z	kA	Z
-	-	kIx~	-
zápasy	zápas	k1gInPc1	zápas
<g/>
,	,	kIx,	,
V	v	k7c6	v
-	-	kIx~	-
výhry	výhra	k1gFnPc4	výhra
<g/>
,	,	kIx,	,
R	R	kA	R
-	-	kIx~	-
remízy	remíz	k1gInPc1	remíz
<g/>
,	,	kIx,	,
P	P	kA	P
-	-	kIx~	-
porážky	porážka	k1gFnSc2	porážka
<g/>
,	,	kIx,	,
VG	VG	kA	VG
-	-	kIx~	-
vstřelené	vstřelený	k2eAgInPc1d1	vstřelený
góly	gól	k1gInPc1	gól
<g/>
,	,	kIx,	,
OG	OG	kA	OG
-	-	kIx~	-
obdržené	obdržený	k2eAgInPc4d1	obdržený
góly	gól	k1gInPc4	gól
<g/>
,	,	kIx,	,
+	+	kIx~	+
<g/>
/	/	kIx~	/
<g/>
-	-	kIx~	-
-	-	kIx~	-
rozdíl	rozdíl	k1gInSc1	rozdíl
skóre	skóre	k1gNnSc2	skóre
<g/>
,	,	kIx,	,
B	B	kA	B
-	-	kIx~	-
body	bod	k1gInPc1	bod
<g/>
,	,	kIx,	,
červené	červený	k2eAgNnSc1d1	červené
podbarvení	podbarvení	k1gNnSc1	podbarvení
-	-	kIx~	-
sestup	sestup	k1gInSc1	sestup
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgNnSc1d1	zelené
podbarvení	podbarvení	k1gNnSc1	podbarvení
-	-	kIx~	-
postup	postup	k1gInSc1	postup
<g/>
,	,	kIx,	,
fialové	fialový	k2eAgNnSc1d1	fialové
podbarvení	podbarvení	k1gNnSc1	podbarvení
-	-	kIx~	-
reorganizace	reorganizace	k1gFnSc1	reorganizace
<g/>
,	,	kIx,	,
změna	změna	k1gFnSc1	změna
skupiny	skupina	k1gFnSc2	skupina
či	či	k8xC	či
soutěže	soutěž	k1gFnSc2	soutěž
</s>
</p>
<p>
<s>
Poznámky	poznámka	k1gFnPc1	poznámka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1937	[number]	k4	1937
<g/>
/	/	kIx~	/
<g/>
38	[number]	k4	38
<g/>
:	:	kIx,	:
BuEV	BuEV	k1gMnSc1	BuEV
(	(	kIx(	(
<g/>
vítěz	vítěz	k1gMnSc1	vítěz
sk	sk	k?	sk
<g/>
.	.	kIx.	.
</s>
<s>
Danzig	Danzig	k1gInSc1	Danzig
<g/>
)	)	kIx)	)
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
prohrál	prohrát	k5eAaPmAgMnS	prohrát
s	s	k7c7	s
insterburgským	insterburgský	k1gMnSc7	insterburgský
Yorck	Yorcka	k1gFnPc2	Yorcka
Boyenem	Boyen	k1gMnSc7	Boyen
(	(	kIx(	(
<g/>
vítěz	vítěz	k1gMnSc1	vítěz
sk	sk	k?	sk
<g/>
.	.	kIx.	.
</s>
<s>
Gumbinnen	Gumbinnen	k2eAgMnSc1d1	Gumbinnen
<g/>
)	)	kIx)	)
celkovým	celkový	k2eAgInSc7d1	celkový
poměrem	poměr	k1gInSc7	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
zápas	zápas	k1gInSc1	zápas
–	–	k?	–
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
zápas	zápas	k1gInSc1	zápas
–	–	k?	–
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Jürgen	Jürgen	k1gInSc1	Jürgen
Bitter	Bitter	k1gInSc1	Bitter
<g/>
:	:	kIx,	:
Deutschlands	Deutschlands	k1gInSc1	Deutschlands
Fußball	Fußballa	k1gFnPc2	Fußballa
<g/>
.	.	kIx.	.
</s>
<s>
Das	Das	k?	Das
Lexikon	lexikon	k1gInSc1	lexikon
<g/>
.	.	kIx.	.
</s>
<s>
Sportverlag	Sportverlaga	k1gFnPc2	Sportverlaga
<g/>
,	,	kIx,	,
Berlin	berlina	k1gFnPc2	berlina
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
328	[number]	k4	328
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
857	[number]	k4	857
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hardy	Hardy	k6eAd1	Hardy
Grüne	Grün	k1gInSc5	Grün
<g/>
:	:	kIx,	:
Enzyklopädie	Enzyklopädie	k1gFnSc1	Enzyklopädie
des	des	k1gNnSc2	des
deutschen	deutschen	k2eAgInSc4d1	deutschen
Ligafußballs	Ligafußballs	k1gInSc4	Ligafußballs
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Vom	Vom	k?	Vom
Kronprinzen	Kronprinzna	k1gFnPc2	Kronprinzna
bis	bis	k?	bis
zur	zur	k?	zur
Bundesliga	bundesliga	k1gFnSc1	bundesliga
1890	[number]	k4	1890
<g/>
–	–	k?	–
<g/>
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Agon-Sportverlag	Agon-Sportverlag	k1gInSc1	Agon-Sportverlag
<g/>
,	,	kIx,	,
Kassel	Kassel	k1gInSc1	Kassel
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
928562	[number]	k4	928562
<g/>
-	-	kIx~	-
<g/>
85	[number]	k4	85
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hardy	Hardy	k6eAd1	Hardy
Grüne	Grün	k1gInSc5	Grün
<g/>
:	:	kIx,	:
Enzyklopädie	Enzyklopädie	k1gFnSc1	Enzyklopädie
des	des	k1gNnSc2	des
deutschen	deutschen	k2eAgInSc4d1	deutschen
Ligafußballs	Ligafußballs	k1gInSc4	Ligafußballs
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Vereinslexikon	Vereinslexikon	k1gMnSc1	Vereinslexikon
<g/>
.	.	kIx.	.
</s>
<s>
Agon-Sportverlag	Agon-Sportverlag	k1gInSc1	Agon-Sportverlag
<g/>
,	,	kIx,	,
Kassel	Kassel	k1gInSc1	Kassel
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
89784	[number]	k4	89784
<g/>
-	-	kIx~	-
<g/>
147	[number]	k4	147
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
BuEV	BuEV	k1gMnSc1	BuEV
Danzig	Danzig	k1gMnSc1	Danzig
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
