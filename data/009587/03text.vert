<p>
<s>
81	[number]	k4	81
<g/>
.	.	kIx.	.
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Pořadateli	pořadatel	k1gMnSc3	pořadatel
byli	být	k5eAaImAgMnP	být
Německo	Německo	k1gNnSc4	Německo
a	a	k8xC	a
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
hrálo	hrát	k5eAaImAgNnS	hrát
se	se	k3xPyFc4	se
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
a	a	k8xC	a
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Pořadatelské	pořadatelský	k2eAgFnPc1d1	pořadatelská
země	zem	k1gFnPc1	zem
byly	být	k5eAaImAgFnP	být
zvoleny	zvolit	k5eAaPmNgFnP	zvolit
během	během	k7c2	během
kongresu	kongres	k1gInSc2	kongres
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
hokejové	hokejový	k2eAgFnSc2d1	hokejová
federace	federace	k1gFnSc2	federace
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2013	[number]	k4	2013
ve	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
se	se	k3xPyFc4	se
ujala	ujmout	k5eAaPmAgFnS	ujmout
pořadatelství	pořadatelství	k1gNnSc4	pořadatelství
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
35	[number]	k4	35
<g/>
.	.	kIx.	.
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
v	v	k7c6	v
Grenoblu	Grenoble	k1gInSc6	Grenoble
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
se	se	k3xPyFc4	se
hrálo	hrát	k5eAaImAgNnS	hrát
jedinkrát	jedinkrát	k6eAd1	jedinkrát
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
18	[number]	k4	18
<g/>
.	.	kIx.	.
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
se	se	k3xPyFc4	se
hrálo	hrát	k5eAaImAgNnS	hrát
naposledy	naposledy	k6eAd1	naposledy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oproti	oproti	k7c3	oproti
minulému	minulý	k2eAgInSc3d1	minulý
ročníku	ročník	k1gInSc3	ročník
se	se	k3xPyFc4	se
mistrovství	mistrovství	k1gNnSc4	mistrovství
účastnily	účastnit	k5eAaImAgFnP	účastnit
týmy	tým	k1gInPc1	tým
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
a	a	k8xC	a
Itálie	Itálie	k1gFnSc2	Itálie
(	(	kIx(	(
<g/>
postup	postup	k1gInSc1	postup
z	z	k7c2	z
Divize	divize	k1gFnSc2	divize
I.	I.	kA	I.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
minulého	minulý	k2eAgNnSc2d1	Minulé
mistrovství	mistrovství	k1gNnSc2	mistrovství
sestoupily	sestoupit	k5eAaPmAgFnP	sestoupit
týmy	tým	k1gInPc4	tým
Kazachstánu	Kazachstán	k1gInSc2	Kazachstán
a	a	k8xC	a
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výběr	výběr	k1gInSc1	výběr
pořadatelské	pořadatelský	k2eAgFnSc2d1	pořadatelská
země	zem	k1gFnSc2	zem
==	==	k?	==
</s>
</p>
<p>
<s>
O	o	k7c6	o
pořádání	pořádání	k1gNnSc6	pořádání
81	[number]	k4	81
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
MS	MS	kA	MS
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
Francii	Francie	k1gFnSc3	Francie
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
hlasování	hlasování	k1gNnSc2	hlasování
na	na	k7c6	na
kongresu	kongres	k1gInSc6	kongres
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
hokejové	hokejový	k2eAgFnSc2d1	hokejová
federace	federace	k1gFnSc2	federace
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2013	[number]	k4	2013
ve	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
poměrem	poměr	k1gInSc7	poměr
hlasů	hlas	k1gInPc2	hlas
63	[number]	k4	63
ku	k	k7c3	k
45	[number]	k4	45
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Německa	Německo	k1gNnSc2	Německo
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
kandidáty	kandidát	k1gMnPc7	kandidát
byly	být	k5eAaImAgFnP	být
Dánsko	Dánsko	k1gNnSc4	Dánsko
<g/>
,	,	kIx,	,
s	s	k7c7	s
pořadatelskými	pořadatelský	k2eAgNnPc7d1	pořadatelské
městy	město	k1gNnPc7	město
Kodaň	Kodaň	k1gFnSc1	Kodaň
a	a	k8xC	a
Herning	Herning	k1gInSc1	Herning
<g/>
,	,	kIx,	,
a	a	k8xC	a
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
by	by	kYmCp3nS	by
turnaj	turnaj	k1gInSc4	turnaj
uspořádalo	uspořádat	k5eAaPmAgNnS	uspořádat
v	v	k7c6	v
Rize	Riga	k1gFnSc6	Riga
<g/>
.	.	kIx.	.
<g/>
Kandidující	kandidující	k2eAgInPc1d1	kandidující
státy	stát	k1gInPc1	stát
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Stadiony	stadion	k1gInPc4	stadion
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
bude	být	k5eAaImBp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
známá	známý	k2eAgFnSc1d1	známá
Lanxess	Lanxess	k1gInSc4	Lanxess
Arena	Aren	k1gInSc2	Aren
<g/>
,	,	kIx,	,
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
bude	být	k5eAaImBp3nS	být
hrát	hrát	k5eAaImF	hrát
ve	v	k7c6	v
staré	starý	k2eAgFnSc6d1	stará
multifunkční	multifunkční	k2eAgFnSc6d1	multifunkční
hale	hala	k1gFnSc6	hala
AccorHotels	AccorHotels	k1gInSc4	AccorHotels
Arena	Areno	k1gNnSc2	Areno
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
má	mít	k5eAaImIp3nS	mít
projít	projít	k5eAaPmF	projít
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
navýšení	navýšení	k1gNnSc3	navýšení
kapacity	kapacita	k1gFnSc2	kapacita
míst	místo	k1gNnPc2	místo
na	na	k7c4	na
15	[number]	k4	15
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
města	město	k1gNnPc1	město
jsou	být	k5eAaImIp3nP	být
vzdálena	vzdálit	k5eAaPmNgNnP	vzdálit
přes	přes	k7c4	přes
500	[number]	k4	500
km	km	kA	km
a	a	k8xC	a
lze	lze	k6eAd1	lze
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
cestovat	cestovat	k5eAaImF	cestovat
jak	jak	k8xC	jak
vlakem	vlak	k1gInSc7	vlak
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
letadlem	letadlo	k1gNnSc7	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Nejrychlejší	rychlý	k2eAgNnSc1d3	nejrychlejší
přímé	přímý	k2eAgNnSc1d1	přímé
vlakové	vlakový	k2eAgNnSc1d1	vlakové
spojení	spojení	k1gNnSc1	spojení
nabízí	nabízet	k5eAaImIp3nS	nabízet
vysokorychlostní	vysokorychlostní	k2eAgInPc4d1	vysokorychlostní
vlaky	vlak	k1gInPc4	vlak
THA	THA	kA	THA
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
tuto	tento	k3xDgFnSc4	tento
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
překonají	překonat	k5eAaPmIp3nP	překonat
za	za	k7c4	za
tři	tři	k4xCgInPc4	tři
a	a	k8xC	a
čtvrt	čtvrt	k1xP	čtvrt
hodiny	hodina	k1gFnSc2	hodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Maskoti	Maskot	k1gMnPc1	Maskot
šampionátu	šampionát	k1gInSc2	šampionát
==	==	k?	==
</s>
</p>
<p>
<s>
Oficiálními	oficiální	k2eAgInPc7d1	oficiální
maskoty	maskot	k1gInPc7	maskot
pro	pro	k7c4	pro
letošní	letošní	k2eAgInSc4d1	letošní
ročník	ročník	k1gInSc4	ročník
šampionátu	šampionát	k1gInSc2	šampionát
Mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
zvolila	zvolit	k5eAaPmAgFnS	zvolit
francouzská	francouzský	k2eAgFnSc1d1	francouzská
a	a	k8xC	a
německá	německý	k2eAgFnSc1d1	německá
hokejová	hokejový	k2eAgFnSc1d1	hokejová
federace	federace	k1gFnSc1	federace
dva	dva	k4xCgInPc4	dva
nejznámější	známý	k2eAgInPc4d3	nejznámější
galy	galy	k1gInPc4	galy
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
jimi	on	k3xPp3gMnPc7	on
nikdo	nikdo	k3yNnSc1	nikdo
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
,	,	kIx,	,
než	než	k8xS	než
mazaný	mazaný	k2eAgInSc1d1	mazaný
Asterix	Asterix	k1gInSc1	Asterix
a	a	k8xC	a
sílák	sílák	k1gInSc1	sílák
Obelix	Obelix	k1gInSc1	Obelix
<g/>
.	.	kIx.	.
</s>
<s>
Nerozluční	rozluční	k2eNgMnPc1d1	nerozluční
přátelé	přítel	k1gMnPc1	přítel
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c7	mezi
nejúspěšnější	úspěšný	k2eAgFnSc7d3	nejúspěšnější
komiksovou	komiksový	k2eAgFnSc7d1	komiksová
sérií	série	k1gFnSc7	série
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
první	první	k4xOgNnSc4	první
vydání	vydání	k1gNnSc4	vydání
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
na	na	k7c4	na
rok	rok	k1gInSc4	rok
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
<s>
Komiksy	komiks	k1gInPc4	komiks
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
původním	původní	k2eAgMnSc7d1	původní
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
scenárista	scenárista	k1gMnSc1	scenárista
René	René	k1gMnSc1	René
Goscinny	Goscinna	k1gFnSc2	Goscinna
a	a	k8xC	a
kreslíř	kreslíř	k1gMnSc1	kreslíř
Albert	Albert	k1gMnSc1	Albert
Uderzo	Uderza	k1gFnSc5	Uderza
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
již	již	k6eAd1	již
přeloženy	přeložit	k5eAaPmNgFnP	přeložit
do	do	k7c2	do
více	hodně	k6eAd2	hodně
než	než	k8xS	než
111	[number]	k4	111
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
dialektů	dialekt	k1gInPc2	dialekt
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yQgMnPc4	který
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nP	řadit
taktéž	taktéž	k?	taktéž
místní	místní	k2eAgInSc1d1	místní
dialekt	dialekt	k1gInSc1	dialekt
z	z	k7c2	z
Kolína	Kolín	k1gInSc2	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
<g/>
,	,	kIx,	,
Kölschtina	Kölschtina	k1gFnSc1	Kölschtina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulých	minulý	k2eAgInPc6d1	minulý
ročnících	ročník	k1gInPc6	ročník
se	se	k3xPyFc4	se
za	za	k7c4	za
maskoty	maskot	k1gInPc4	maskot
objevil	objevit	k5eAaPmAgMnS	objevit
pes	pes	k1gMnSc1	pes
Lajka	lajka	k1gFnSc1	lajka
<g/>
,	,	kIx,	,
králící	králící	k2eAgMnSc1d1	králící
Bob	Bob	k1gMnSc1	Bob
a	a	k8xC	a
Bobek	Bobek	k1gMnSc1	Bobek
<g/>
,	,	kIx,	,
či	či	k8xC	či
zubr	zubr	k1gMnSc1	zubr
Volat	volat	k5eAaImF	volat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
==	==	k?	==
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
federace	federace	k1gFnSc1	federace
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
nominovala	nominovat	k5eAaBmAgFnS	nominovat
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
16	[number]	k4	16
hlavních	hlavní	k2eAgMnPc2d1	hlavní
rozhodčích	rozhodčí	k1gMnPc2	rozhodčí
a	a	k8xC	a
stejný	stejný	k2eAgInSc1d1	stejný
počet	počet	k1gInSc1	počet
čárových	čárový	k2eAgFnPc2d1	čárová
sudích	sudí	k1gFnPc2	sudí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Herní	herní	k2eAgInSc1d1	herní
systém	systém	k1gInSc1	systém
==	==	k?	==
</s>
</p>
<p>
<s>
Šestnáct	šestnáct	k4xCc1	šestnáct
účastníků	účastník	k1gMnPc2	účastník
je	být	k5eAaImIp3nS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
skupin	skupina	k1gFnPc2	skupina
po	po	k7c6	po
8	[number]	k4	8
týmech	tým	k1gInPc6	tým
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
utká	utkat	k5eAaPmIp3nS	utkat
každý	každý	k3xTgMnSc1	každý
s	s	k7c7	s
každým	každý	k3xTgMnSc7	každý
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
vítězství	vítězství	k1gNnSc4	vítězství
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
hrací	hrací	k2eAgFnSc6d1	hrací
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
udělují	udělovat	k5eAaImIp3nP	udělovat
3	[number]	k4	3
body	bod	k1gInPc4	bod
<g/>
,	,	kIx,	,
za	za	k7c4	za
vítězství	vítězství	k1gNnSc4	vítězství
po	po	k7c6	po
prodloužení	prodloužení	k1gNnSc6	prodloužení
či	či	k8xC	či
samostatných	samostatný	k2eAgInPc6d1	samostatný
nájezdech	nájezd	k1gInPc6	nájezd
2	[number]	k4	2
body	bod	k1gInPc4	bod
<g/>
,	,	kIx,	,
za	za	k7c4	za
prohru	prohra	k1gFnSc4	prohra
po	po	k7c6	po
prodloužení	prodloužení	k1gNnSc6	prodloužení
či	či	k8xC	či
samostatných	samostatný	k2eAgInPc6d1	samostatný
nájezdech	nájezd	k1gInPc6	nájezd
1	[number]	k4	1
bod	bod	k1gInSc4	bod
a	a	k8xC	a
za	za	k7c4	za
prohru	prohra	k1gFnSc4	prohra
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
hrací	hrací	k2eAgFnSc6d1	hrací
době	doba	k1gFnSc6	doba
0	[number]	k4	0
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
každé	každý	k3xTgFnSc2	každý
skupiny	skupina	k1gFnSc2	skupina
postoupí	postoupit	k5eAaPmIp3nS	postoupit
čtveřice	čtveřice	k1gFnSc1	čtveřice
týmů	tým	k1gInPc2	tým
s	s	k7c7	s
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
počtem	počet	k1gInSc7	počet
bodů	bod	k1gInPc2	bod
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
playoff	playoff	k1gInSc1	playoff
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
týmy	tým	k1gInPc4	tým
na	na	k7c6	na
pátých	pátá	k1gFnPc6	pátá
až	až	k9	až
osmých	osmý	k4xOgNnPc6	osmý
místech	místo	k1gNnPc6	místo
ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
turnaj	turnaj	k1gInSc1	turnaj
skončí	skončit	k5eAaPmIp3nP	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
týmy	tým	k1gInPc1	tým
z	z	k7c2	z
osmých	osmý	k4xOgNnPc2	osmý
míst	místo	k1gNnPc2	místo
automaticky	automaticky	k6eAd1	automaticky
sestoupí	sestoupit	k5eAaPmIp3nS	sestoupit
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
divize	divize	k1gFnSc2	divize
(	(	kIx(	(
<g/>
neplatí	platit	k5eNaImIp3nS	platit
pro	pro	k7c4	pro
Dánsko	Dánsko	k1gNnSc4	Dánsko
<g/>
,	,	kIx,	,
pořadatele	pořadatel	k1gMnPc4	pořadatel
MS	MS	kA	MS
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kritéria	kritérion	k1gNnPc4	kritérion
při	při	k7c6	při
rovnosti	rovnost	k1gFnSc6	rovnost
bodů	bod	k1gInPc2	bod
v	v	k7c6	v
základních	základní	k2eAgFnPc6d1	základní
skupinách	skupina	k1gFnPc6	skupina
===	===	k?	===
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
získají	získat	k5eAaPmIp3nP	získat
po	po	k7c6	po
konci	konec	k1gInSc6	konec
základních	základní	k2eAgFnPc2d1	základní
skupin	skupina	k1gFnPc2	skupina
dva	dva	k4xCgInPc4	dva
týmy	tým	k1gInPc4	tým
stejný	stejný	k2eAgInSc4d1	stejný
počet	počet	k1gInSc4	počet
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
rozhodují	rozhodovat	k5eAaImIp3nP	rozhodovat
o	o	k7c6	o
postupujícím	postupující	k2eAgNnSc6d1	postupující
nebo	nebo	k8xC	nebo
o	o	k7c6	o
lépe	dobře	k6eAd2	dobře
nasazeném	nasazený	k2eAgInSc6d1	nasazený
týmu	tým	k1gInSc6	tým
pro	pro	k7c4	pro
čtvrtfinále	čtvrtfinále	k1gNnSc4	čtvrtfinále
výsledek	výsledek	k1gInSc1	výsledek
jejich	jejich	k3xOp3gInSc2	jejich
vzájemného	vzájemný	k2eAgInSc2d1	vzájemný
zápasu	zápas	k1gInSc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
rovnost	rovnost	k1gFnSc1	rovnost
nastane	nastat	k5eAaPmIp3nS	nastat
mezi	mezi	k7c7	mezi
třemi	tři	k4xCgFnPc7	tři
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
týmy	tým	k1gInPc4	tým
<g/>
,	,	kIx,	,
postupuje	postupovat	k5eAaImIp3nS	postupovat
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
následujících	následující	k2eAgNnPc2d1	následující
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
,	,	kIx,	,
dokud	dokud	k6eAd1	dokud
nezbudou	zbýt	k5eNaPmIp3nP	zbýt
dva	dva	k4xCgInPc1	dva
týmy	tým	k1gInPc1	tým
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgMnPc7	jenž
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
výsledek	výsledek	k1gInSc1	výsledek
ze	z	k7c2	z
vzájemného	vzájemný	k2eAgInSc2d1	vzájemný
zápasu	zápas	k1gInSc2	zápas
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Body	bod	k1gInPc4	bod
z	z	k7c2	z
minitabulky	minitabulka	k1gFnSc2	minitabulka
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
zápasů	zápas	k1gInPc2	zápas
</s>
</p>
<p>
<s>
Brankový	brankový	k2eAgInSc1d1	brankový
rozdíl	rozdíl	k1gInSc1	rozdíl
z	z	k7c2	z
minitabulky	minitabulka	k1gFnSc2	minitabulka
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
zápasů	zápas	k1gInPc2	zápas
</s>
</p>
<p>
<s>
Vyšší	vysoký	k2eAgInSc1d2	vyšší
počet	počet	k1gInSc1	počet
vstřelených	vstřelený	k2eAgFnPc2d1	vstřelená
branek	branka	k1gFnPc2	branka
v	v	k7c6	v
minitabulce	minitabulka	k1gFnSc6	minitabulka
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
zápasů	zápas	k1gInPc2	zápas
</s>
</p>
<p>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
proti	proti	k7c3	proti
nejbližšímu	blízký	k2eAgInSc3d3	Nejbližší
nejvýše	nejvýše	k6eAd1	nejvýše
umístěnému	umístěný	k2eAgInSc3d1	umístěný
týmu	tým	k1gInSc3	tým
mimo	mimo	k7c4	mimo
týmy	tým	k1gInPc4	tým
v	v	k7c6	v
minitabulce	minitabulka	k1gFnSc6	minitabulka
(	(	kIx(	(
<g/>
pořadí	pořadí	k1gNnSc1	pořadí
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
body	bod	k1gInPc1	bod
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
brankový	brankový	k2eAgInSc1d1	brankový
rozdíl	rozdíl	k1gInSc1	rozdíl
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
více	hodně	k6eAd2	hodně
vstřelených	vstřelený	k2eAgFnPc2d1	vstřelená
branek	branka	k1gFnPc2	branka
<g/>
)	)	kIx)	)
vůči	vůči	k7c3	vůči
tomuto	tento	k3xDgNnSc3	tento
družstvu	družstvo	k1gNnSc3	družstvo
</s>
</p>
<p>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
proti	proti	k7c3	proti
nejbližšímu	blízký	k2eAgInSc3d3	Nejbližší
druhému	druhý	k4xOgInSc3	druhý
nejvýše	vysoce	k6eAd3	vysoce
umístěnému	umístěný	k2eAgInSc3d1	umístěný
týmu	tým	k1gInSc3	tým
mimo	mimo	k7c4	mimo
týmů	tým	k1gInPc2	tým
v	v	k7c6	v
minitabulce	minitabulka	k1gFnSc6	minitabulka
(	(	kIx(	(
<g/>
pořadí	pořadí	k1gNnSc1	pořadí
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
body	bod	k1gInPc1	bod
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
brankový	brankový	k2eAgInSc1d1	brankový
rozdíl	rozdíl	k1gInSc1	rozdíl
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
více	hodně	k6eAd2	hodně
vstřelených	vstřelený	k2eAgFnPc2d1	vstřelená
branek	branka	k1gFnPc2	branka
<g/>
)	)	kIx)	)
vůči	vůči	k7c3	vůči
tomuto	tento	k3xDgNnSc3	tento
družstvu	družstvo	k1gNnSc3	družstvo
</s>
</p>
<p>
<s>
Postavení	postavení	k1gNnSc1	postavení
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
IIHF	IIHF	kA	IIHF
před	před	k7c7	před
startem	start	k1gInSc7	start
mistrovstvíV	mistrovstvíV	k?	mistrovstvíV
průběhu	průběh	k1gInSc2	průběh
základních	základní	k2eAgFnPc2d1	základní
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
by	by	kYmCp3nP	by
ještě	ještě	k9	ještě
nebyly	být	k5eNaImAgInP	být
sehrány	sehrát	k5eAaPmNgInP	sehrát
všechny	všechen	k3xTgInPc1	všechen
zápasy	zápas	k1gInPc1	zápas
budou	být	k5eAaImBp3nP	být
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
bodové	bodový	k2eAgFnSc2d1	bodová
rovnosti	rovnost	k1gFnSc2	rovnost
tato	tento	k3xDgNnPc1	tento
kritéria	kritérion	k1gNnPc1	kritérion
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Nižší	nízký	k2eAgInSc1d2	nižší
počet	počet	k1gInSc1	počet
odehraných	odehraný	k2eAgNnPc2d1	odehrané
utkání	utkání	k1gNnPc2	utkání
</s>
</p>
<p>
<s>
Brankový	brankový	k2eAgInSc1d1	brankový
rozdíl	rozdíl	k1gInSc1	rozdíl
</s>
</p>
<p>
<s>
Vyšší	vysoký	k2eAgInSc1d2	vyšší
počet	počet	k1gInSc1	počet
vstřelených	vstřelený	k2eAgFnPc2d1	vstřelená
branek	branka	k1gFnPc2	branka
</s>
</p>
<p>
<s>
Postavení	postavení	k1gNnSc1	postavení
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
IIHF	IIHF	kA	IIHF
před	před	k7c7	před
startem	start	k1gInSc7	start
mistrovstvíČtvrtfinále	mistrovstvíČtvrtfinále	k1gNnSc1	mistrovstvíČtvrtfinále
bude	být	k5eAaImBp3nS	být
sehráno	sehrát	k5eAaPmNgNnS	sehrát
křížovým	křížový	k2eAgInSc7d1	křížový
systémem	systém	k1gInSc7	systém
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
1	[number]	k4	1
<g/>
.	.	kIx.	.
tým	tým	k1gInSc1	tým
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
A	a	k9	a
proti	proti	k7c3	proti
4	[number]	k4	4
<g/>
.	.	kIx.	.
týmu	tým	k1gInSc2	tým
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
B	B	kA	B
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
tým	tým	k1gInSc1	tým
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
A	a	k9	a
proti	proti	k7c3	proti
3	[number]	k4	3
<g/>
.	.	kIx.	.
týmu	tým	k1gInSc2	tým
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
B	B	kA	B
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
tým	tým	k1gInSc1	tým
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
B	B	kA	B
proti	proti	k7c3	proti
4	[number]	k4	4
<g/>
.	.	kIx.	.
týmu	tým	k1gInSc2	tým
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
A	A	kA	A
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
tým	tým	k1gInSc1	tým
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
B	B	kA	B
proti	proti	k7c3	proti
3	[number]	k4	3
<g/>
.	.	kIx.	.
týmu	tým	k1gInSc2	tým
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
A	A	kA	A
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgFnP	mít
utkat	utkat	k5eAaPmF	utkat
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
domácí	domácí	k2eAgInPc1d1	domácí
týmy	tým	k1gInPc1	tým
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
a	a	k8xC	a
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
čtvrtfinále	čtvrtfinále	k1gNnSc1	čtvrtfinále
hrálo	hrát	k5eAaImAgNnS	hrát
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
skupin	skupina	k1gFnPc2	skupina
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
1	[number]	k4	1
<g/>
A-	A-	k1gFnPc2	A-
<g/>
4	[number]	k4	4
<g/>
A	A	kA	A
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
A-	A-	k1gFnSc1	A-
<g/>
3	[number]	k4	3
<g/>
A	A	kA	A
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
B-	B-	k1gFnSc1	B-
<g/>
4	[number]	k4	4
<g/>
B	B	kA	B
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
B-	B-	k1gFnSc1	B-
<g/>
3	[number]	k4	3
<g/>
B	B	kA	B
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vítězové	vítěz	k1gMnPc1	vítěz
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
postoupí	postoupit	k5eAaPmIp3nP	postoupit
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
poražené	poražený	k1gMnPc4	poražený
čtvrtfinalisty	čtvrtfinalista	k1gMnPc4	čtvrtfinalista
turnaj	turnaj	k1gInSc4	turnaj
skončí	skončit	k5eAaPmIp3nS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
se	se	k3xPyFc4	se
dvojice	dvojice	k1gFnPc1	dvojice
utkají	utkat	k5eAaPmIp3nP	utkat
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
vzorci	vzorec	k1gInSc6	vzorec
<g/>
:	:	kIx,	:
vítěz	vítěz	k1gMnSc1	vítěz
utkání	utkání	k1gNnSc4	utkání
mezi	mezi	k7c4	mezi
1A	[number]	k4	1A
-	-	kIx~	-
4B	[number]	k4	4B
vs	vs	k?	vs
vítěz	vítěz	k1gMnSc1	vítěz
2B	[number]	k4	2B
-	-	kIx~	-
3	[number]	k4	3
<g/>
A	A	kA	A
<g/>
,	,	kIx,	,
vítěz	vítěz	k1gMnSc1	vítěz
1B	[number]	k4	1B
-	-	kIx~	-
4A	[number]	k4	4A
vs	vs	k?	vs
vítěz	vítěz	k1gMnSc1	vítěz
2A	[number]	k4	2A
-	-	kIx~	-
3	[number]	k4	3
<g/>
B.	B.	kA	B.
Oba	dva	k4xCgInPc1	dva
semifinálové	semifinálový	k2eAgInPc1d1	semifinálový
zápasy	zápas	k1gInPc1	zápas
se	se	k3xPyFc4	se
odehrají	odehrát	k5eAaPmIp3nP	odehrát
ve	v	k7c4	v
větší	veliký	k2eAgInSc4d2	veliký
Lanxess	Lanxess	k1gInSc4	Lanxess
Areně	Areeň	k1gFnSc2	Areeň
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
<g/>
.	.	kIx.	.
</s>
<s>
Vítězní	vítězný	k2eAgMnPc1d1	vítězný
semifinalisté	semifinalista	k1gMnPc1	semifinalista
postoupí	postoupit	k5eAaPmIp3nP	postoupit
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
o	o	k7c6	o
držitelích	držitel	k1gMnPc6	držitel
zlatých	zlatý	k2eAgFnPc2d1	zlatá
a	a	k8xC	a
stříbrných	stříbrný	k2eAgFnPc2d1	stříbrná
medailí	medaile	k1gFnPc2	medaile
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
poražení	poražený	k2eAgMnPc1d1	poražený
semifinalisté	semifinalista	k1gMnPc1	semifinalista
se	se	k3xPyFc4	se
střetnou	střetnout	k5eAaPmIp3nP	střetnout
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
o	o	k7c4	o
bronzové	bronzový	k2eAgFnPc4d1	bronzová
medaile	medaile	k1gFnPc4	medaile
</s>
</p>
<p>
<s>
====	====	k?	====
Systém	systém	k1gInSc1	systém
prodloužení	prodloužení	k1gNnSc2	prodloužení
v	v	k7c4	v
playoff	playoff	k1gInSc4	playoff
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
vyrovnaného	vyrovnaný	k2eAgInSc2d1	vyrovnaný
stavu	stav	k1gInSc2	stav
i	i	k9	i
po	po	k7c6	po
šedesáti	šedesát	k4xCc6	šedesát
minutách	minuta	k1gFnPc6	minuta
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
<g/>
,	,	kIx,	,
semifinále	semifinále	k1gNnSc2	semifinále
nebo	nebo	k8xC	nebo
zápasu	zápas	k1gInSc2	zápas
o	o	k7c4	o
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
se	se	k3xPyFc4	se
zápas	zápas	k1gInSc1	zápas
prodlužuje	prodlužovat	k5eAaImIp3nS	prodlužovat
o	o	k7c4	o
deset	deset	k4xCc4	deset
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
tříminutové	tříminutový	k2eAgFnSc6d1	tříminutová
přestávce	přestávka	k1gFnSc6	přestávka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
o	o	k7c4	o
zlaté	zlatý	k2eAgFnPc4d1	zlatá
medaile	medaile	k1gFnPc4	medaile
by	by	kYmCp3nS	by
následovalo	následovat	k5eAaImAgNnS	následovat
dvacetiminutové	dvacetiminutový	k2eAgNnSc1d1	dvacetiminutové
prodloužení	prodloužení	k1gNnSc1	prodloužení
<g/>
,	,	kIx,	,
před	před	k7c7	před
kterým	který	k3yIgNnSc7	který
by	by	kYmCp3nS	by
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
patnáctiminutová	patnáctiminutový	k2eAgFnSc1d1	patnáctiminutová
přestávka	přestávka	k1gFnSc1	přestávka
s	s	k7c7	s
úpravou	úprava	k1gFnSc7	úprava
ledové	ledový	k2eAgFnSc2d1	ledová
plochy	plocha	k1gFnSc2	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
by	by	kYmCp3nP	by
během	během	k7c2	během
prodloužení	prodloužení	k1gNnSc2	prodloužení
ani	ani	k8xC	ani
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
týmů	tým	k1gInPc2	tým
nedosáhl	dosáhnout	k5eNaPmAgInS	dosáhnout
branky	branka	k1gFnPc4	branka
<g/>
,	,	kIx,	,
na	na	k7c4	na
řadu	řada	k1gFnSc4	řada
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgInP	dostat
samostatné	samostatný	k2eAgInPc1d1	samostatný
nájezdy	nájezd	k1gInPc1	nájezd
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
by	by	kYmCp3nP	by
určily	určit	k5eAaPmAgFnP	určit
vítěze	vítěz	k1gMnPc4	vítěz
utkání	utkání	k1gNnSc2	utkání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kritéria	kritérion	k1gNnPc4	kritérion
pro	pro	k7c4	pro
určení	určení	k1gNnSc4	určení
konečného	konečný	k2eAgNnSc2d1	konečné
pořadí	pořadí	k1gNnSc2	pořadí
týmů	tým	k1gInPc2	tým
===	===	k?	===
</s>
</p>
<p>
<s>
Pořadí	pořadí	k1gNnSc1	pořadí
na	na	k7c6	na
prvních	první	k4xOgNnPc6	první
čtyřech	čtyři	k4xCgNnPc6	čtyři
místech	místo	k1gNnPc6	místo
určí	určit	k5eAaPmIp3nS	určit
výsledek	výsledek	k1gInSc1	výsledek
finálového	finálový	k2eAgInSc2d1	finálový
zápasu	zápas	k1gInSc2	zápas
a	a	k8xC	a
utkání	utkání	k1gNnSc2	utkání
o	o	k7c4	o
bronz	bronz	k1gInSc4	bronz
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
konečném	konečný	k2eAgNnSc6d1	konečné
umístění	umístění	k1gNnSc6	umístění
na	na	k7c4	na
5	[number]	k4	5
<g/>
.	.	kIx.	.
až	až	k9	až
16	[number]	k4	16
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
rozhodují	rozhodovat	k5eAaImIp3nP	rozhodovat
tato	tento	k3xDgNnPc1	tento
kritéria	kritérion	k1gNnPc1	kritérion
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Vyšší	vysoký	k2eAgNnSc1d2	vyšší
postavení	postavení	k1gNnSc1	postavení
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
</s>
</p>
<p>
<s>
Vyšší	vysoký	k2eAgInSc1d2	vyšší
počet	počet	k1gInSc1	počet
bodů	bod	k1gInPc2	bod
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
</s>
</p>
<p>
<s>
Lepší	dobrý	k2eAgInSc1d2	lepší
brankový	brankový	k2eAgInSc1d1	brankový
rozdíl	rozdíl	k1gInSc1	rozdíl
</s>
</p>
<p>
<s>
Vyšší	vysoký	k2eAgInSc1d2	vyšší
počet	počet	k1gInSc1	počet
vstřelených	vstřelený	k2eAgFnPc2d1	vstřelená
branek	branka	k1gFnPc2	branka
</s>
</p>
<p>
<s>
Postavení	postavení	k1gNnSc1	postavení
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
IIHF	IIHF	kA	IIHF
před	před	k7c7	před
startem	start	k1gInSc7	start
mistrovstvíPoznámka	mistrovstvíPoznámka	k1gFnSc1	mistrovstvíPoznámka
<g/>
:	:	kIx,	:
Poražení	poražený	k2eAgMnPc1d1	poražený
čtvrtfinalisté	čtvrtfinalista	k1gMnPc1	čtvrtfinalista
zaujmou	zaujmout	k5eAaPmIp3nP	zaujmout
automaticky	automaticky	k6eAd1	automaticky
5	[number]	k4	5
<g/>
.	.	kIx.	.
až	až	k9	až
8	[number]	k4	8
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
seřazeni	seřadit	k5eAaPmNgMnP	seřadit
podle	podle	k7c2	podle
výsledků	výsledek	k1gInPc2	výsledek
v	v	k7c6	v
základních	základní	k2eAgFnPc6d1	základní
skupinách	skupina	k1gFnPc6	skupina
dle	dle	k7c2	dle
kritérií	kritérion	k1gNnPc2	kritérion
uvedených	uvedený	k2eAgFnPc2d1	uvedená
výše	výše	k1gFnSc2	výše
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Legenda	legenda	k1gFnSc1	legenda
==	==	k?	==
</s>
</p>
<p>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
seznam	seznam	k1gInSc1	seznam
vysvětlivek	vysvětlivka	k1gFnPc2	vysvětlivka
použitých	použitý	k2eAgInPc2d1	použitý
v	v	k7c6	v
souhrnech	souhrn	k1gInPc6	souhrn
odehraných	odehraný	k2eAgInPc2d1	odehraný
zápasů	zápas	k1gInPc2	zápas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgFnPc1d1	základní
skupiny	skupina	k1gFnPc1	skupina
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Skupina	skupina	k1gFnSc1	skupina
A	a	k8xC	a
–	–	k?	–
Kolín	Kolín	k1gInSc1	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Tabulka	tabulka	k1gFnSc1	tabulka
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Zápasy	zápas	k1gInPc1	zápas
====	====	k?	====
</s>
</p>
<p>
<s>
===	===	k?	===
Skupina	skupina	k1gFnSc1	skupina
B	B	kA	B
–	–	k?	–
Paříž	Paříž	k1gFnSc1	Paříž
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Tabulka	tabulka	k1gFnSc1	tabulka
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Zápasy	zápas	k1gInPc1	zápas
====	====	k?	====
</s>
</p>
<p>
<s>
==	==	k?	==
Play	play	k0	play
off	off	k?	off
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Pavouk	pavouk	k1gMnSc1	pavouk
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Čtvrtfinále	čtvrtfinále	k1gNnSc1	čtvrtfinále
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Semifinále	semifinále	k1gNnPc3	semifinále
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Zápas	zápas	k1gInSc4	zápas
o	o	k7c4	o
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Finále	finále	k1gNnSc1	finále
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Konečné	Konečné	k2eAgNnSc1d1	Konečné
pořadí	pořadí	k1gNnSc1	pořadí
==	==	k?	==
</s>
<s>
Zlato	zlato	k1gNnSc1	zlato
-	-	kIx~	-
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
,	,	kIx,	,
stříbro	stříbro	k1gNnSc1	stříbro
-	-	kIx~	-
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
bronz	bronz	k1gInSc1	bronz
-	-	kIx~	-
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
2017	[number]	k4	2017
(	(	kIx(	(
<g/>
Divize	divize	k1gFnSc2	divize
I	I	kA	I
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
2017	[number]	k4	2017
(	(	kIx(	(
<g/>
Divize	divize	k1gFnSc2	divize
II	II	kA	II
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
2017	[number]	k4	2017
(	(	kIx(	(
<g/>
Divize	divize	k1gFnSc2	divize
III	III	kA	III
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
2017	[number]	k4	2017
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
MS	MS	kA	MS
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
2017	[number]	k4	2017
–	–	k?	–
oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
