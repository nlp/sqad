<s>
Hana	Hana	k1gFnSc1
Vagnerová	Vagnerová	k1gFnSc1
</s>
<s>
Hana	Hana	k1gFnSc1
Vagnerová	Vagnerový	k2eAgFnSc1d1
Narození	narození	k1gNnPc2
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1983	#num#	k4
(	(	kIx(
<g/>
38	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
PrahaČeskoslovensko	PrahaČeskoslovensko	k1gNnSc1
Československo	Československo	k1gNnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
AMU	AMU	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Praha	Praha	k1gFnSc1
Významné	významný	k2eAgFnSc2d1
role	role	k1gFnSc2
</s>
<s>
Erika	Erika	k1gFnSc1
Maxová	Maxová	k1gFnSc1
v	v	k7c6
seriálu	seriál	k1gInSc6
On	on	k3xPp3gMnSc1
je	být	k5eAaImIp3nS
žena	žena	k1gFnSc1
<g/>
!	!	kIx.
</s>
<s>
Eva	Eva	k1gFnSc1
Krátká	krátký	k2eAgFnSc1d1
v	v	k7c6
seriálu	seriál	k1gInSc6
Horákovi	Horák	k1gMnSc3
</s>
<s>
Zuzka	Zuzka	k1gFnSc1
Dvořáková	Dvořáková	k1gFnSc1
v	v	k7c6
seriálu	seriál	k1gInSc6
Vyprávěj	vyprávět	k5eAaImRp2nS
</s>
<s>
Denisa	Denisa	k1gFnSc1
v	v	k7c6
seriálu	seriál	k1gInSc6
Lajna	lajna	k1gFnSc1
<g/>
,	,	kIx,
Lajna	lajna	k1gFnSc1
2	#num#	k4
</s>
<s>
Tereza	Tereza	k1gFnSc1
Hodačová	Hodačový	k2eAgFnSc1d1
v	v	k7c6
seriálu	seriál	k1gInSc6
</s>
<s>
Expozitura	expozitura	k1gFnSc1
a	a	k8xC
Atentát	atentát	k1gInSc1
Umělecké	umělecký	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
</s>
<s>
Televizní	televizní	k2eAgInSc1d1
objev	objev	k1gInSc1
roku	rok	k1gInSc2
<g/>
2011	#num#	k4
–	–	k?
Vyprávěj	vyprávět	k5eAaImRp2nS
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hana	Hana	k1gFnSc1
Vagnerová	Vagnerová	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
*	*	kIx~
21	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1983	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
česká	český	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Vyrostla	vyrůst	k5eAaPmAgFnS
na	na	k7c6
pražském	pražský	k2eAgNnSc6d1
Jižním	jižní	k2eAgNnSc6d1
Městě	město	k1gNnSc6
<g/>
,	,	kIx,
oba	dva	k4xCgMnPc1
její	její	k3xOp3gFnSc7
rodiče	rodič	k1gMnPc1
jsou	být	k5eAaImIp3nP
programátoři	programátor	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
absolvování	absolvování	k1gNnSc6
gymnázia	gymnázium	k1gNnSc2
byla	být	k5eAaImAgFnS
přijata	přijmout	k5eAaPmNgFnS
na	na	k7c4
DAMU	DAMU	kA
<g/>
,	,	kIx,
VŠE	VŠE	kA
a	a	k8xC
Matematicko-fyzikální	matematicko-fyzikální	k2eAgFnSc4d1
fakultu	fakulta	k1gFnSc4
UK	UK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpočátku	zpočátku	k6eAd1
studovala	studovat	k5eAaImAgFnS
dvě	dva	k4xCgFnPc4
vysoké	vysoký	k2eAgFnPc4d1
školy	škola	k1gFnPc4
současně	současně	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
nakonec	nakonec	k6eAd1
u	u	k7c2
ní	on	k3xPp3gFnSc2
herectví	herectví	k1gNnSc1
zvítězilo	zvítězit	k5eAaPmAgNnS
nad	nad	k7c7
ekonomií	ekonomie	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
svých	svůj	k3xOyFgNnPc2
čtrnácti	čtrnáct	k4xCc2
let	léto	k1gNnPc2
se	se	k3xPyFc4
věnovala	věnovat	k5eAaImAgFnS,k5eAaPmAgFnS
modelingu	modeling	k1gInSc3
<g/>
,	,	kIx,
v	v	k7c6
osmnácti	osmnáct	k4xCc6
letech	léto	k1gNnPc6
s	s	k7c7
nástupem	nástup	k1gInSc7
na	na	k7c6
DAMU	DAMU	kA
kariéru	kariéra	k1gFnSc4
modelky	modelka	k1gFnSc2
ukončila	ukončit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
předtím	předtím	k6eAd1
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
modelingu	modeling	k1gInSc2
začala	začít	k5eAaPmAgFnS
věnovat	věnovat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
se	se	k3xPyFc4
potýkala	potýkat	k5eAaImAgFnS
s	s	k7c7
anorexií	anorexie	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Poté	poté	k6eAd1
se	se	k3xPyFc4
její	její	k3xOp3gFnPc1
potíže	potíž	k1gFnPc1
ještě	ještě	k6eAd1
zhoršily	zhoršit	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
do	do	k7c2
jedenadvaceti	jedenadvacet	k4xCc2
let	léto	k1gNnPc2
se	se	k3xPyFc4
potýkala	potýkat	k5eAaImAgFnS
s	s	k7c7
následky	následek	k1gInPc7
anorexie	anorexie	k1gFnSc2
a	a	k8xC
vážila	vážit	k5eAaImAgFnS
jen	jen	k9
43	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
jí	jíst	k5eAaImIp3nS
pomohl	pomoct	k5eAaPmAgMnS
její	její	k3xOp3gNnSc4
nový	nový	k2eAgMnSc1d1
přítel	přítel	k1gMnSc1
Marko	Marko	k1gMnSc1
Simić	Simić	k1gMnSc1
<g/>
,	,	kIx,
srbský	srbský	k2eAgMnSc1d1
režisér	režisér	k1gMnSc1
<g/>
,	,	kIx,
s	s	k7c7
nímž	jenž	k3xRgMnSc7
se	se	k3xPyFc4
seznámila	seznámit	k5eAaPmAgFnS
při	při	k7c6
natáčení	natáčení	k1gNnSc3
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
absolventského	absolventský	k2eAgInSc2d1
filmu	film	k1gInSc2
Seance	seance	k1gFnSc2
Fiction	Fiction	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
ním	on	k3xPp3gNnSc7
žila	žít	k5eAaImAgFnS
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
rozešli	rozejít	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Hrála	hrát	k5eAaImAgFnS
v	v	k7c6
divadlech	divadlo	k1gNnPc6
Extrém	extrém	k1gInSc1
<g/>
,	,	kIx,
Disk	disk	k1gInSc1
a	a	k8xC
v	v	k7c6
ABC	ABC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
divadle	divadlo	k1gNnSc6
A	a	k8xC
studio	studio	k1gNnSc4
Rubín	rubín	k1gInSc1
v	v	k7c6
těchto	tento	k3xDgFnPc6
hrách	hra	k1gFnPc6
<g/>
:	:	kIx,
J.	J.	kA
W.	W.	kA
Goethe	Goethe	k1gNnPc2
–	–	k?
Faust	Faust	k1gMnSc1
<g/>
,	,	kIx,
Enda	Enda	k1gMnSc1
Walsh	Walsh	k1gMnSc1
–	–	k?
Bedbound	Bedbound	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
Kolečko	kolečko	k1gNnSc1
–	–	k?
Zakázané	zakázaný	k2eAgNnSc4d1
uvolnění	uvolnění	k1gNnSc4
<g/>
,	,	kIx,
Adéla	Adéla	k1gFnSc1
Laštovková	Laštovková	k1gFnSc1
–	–	k?
Stodolová	stodolový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
Kolečko	kolečko	k1gNnSc1
–	–	k?
Poslední	poslední	k2eAgMnSc1d1
papež	papež	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Kvůli	kvůli	k7c3
časově	časově	k6eAd1
náročnému	náročný	k2eAgNnSc3d1
natáčení	natáčení	k1gNnSc3
seriálu	seriál	k1gInSc2
Expozitura	expozitura	k1gFnSc1
ze	z	k7c2
stálého	stálý	k2eAgNnSc2d1
angažmá	angažmá	k1gNnSc2
odešla	odejít	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
současnosti	současnost	k1gFnSc6
v	v	k7c6
Divadle	divadlo	k1gNnSc6
ABC	ABC	kA
hostuje	hostovat	k5eAaImIp3nS
ve	v	k7c6
hře	hra	k1gFnSc6
Anna	Anna	k1gFnSc1
Karenina	Karenina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Známou	známá	k1gFnSc7
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
především	především	k9
díky	díky	k7c3
seriálovým	seriálový	k2eAgFnPc3d1
rolím	role	k1gFnPc3
(	(	kIx(
<g/>
On	on	k3xPp3gMnSc1
je	být	k5eAaImIp3nS
žena	žena	k1gFnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
Horákovi	Horák	k1gMnSc3
<g/>
,	,	kIx,
Vyprávěj	vyprávět	k5eAaImRp2nS
<g/>
,	,	kIx,
<g/>
)	)	kIx)
<g/>
,	,	kIx,
účinkovala	účinkovat	k5eAaImAgFnS
také	také	k9
ve	v	k7c6
filmu	film	k1gInSc6
Bathory	Bathora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
33	#num#	k4
<g/>
.	.	kIx.
ročníku	ročník	k1gInSc2
Novoměstského	novoměstský	k2eAgInSc2d1
Hrnce	hrnec	k1gInSc2
smíchu	smích	k1gInSc2
<g/>
,	,	kIx,
festivalu	festival	k1gInSc2
české	český	k2eAgFnSc2d1
filmové	filmový	k2eAgFnSc2d1
a	a	k8xC
televizní	televizní	k2eAgFnSc2d1
komedie	komedie	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
Novém	nový	k2eAgNnSc6d1
Městě	město	k1gNnSc6
nad	nad	k7c7
Metují	Metuje	k1gFnSc7
jí	jíst	k5eAaImIp3nS
za	za	k7c4
roli	role	k1gFnSc4
Zuzky	Zuzka	k1gFnSc2
(	(	kIx(
<g/>
seriál	seriál	k1gInSc1
Vyprávěj	vyprávět	k5eAaImRp2nS
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
udělena	udělit	k5eAaPmNgFnS
cena	cena	k1gFnSc1
Televizní	televizní	k2eAgFnSc1d1
objev	objev	k1gInSc4
roku	rok	k1gInSc2
2011	#num#	k4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
určená	určený	k2eAgFnSc1d1
pro	pro	k7c4
mladého	mladý	k2eAgMnSc4d1
herce	herec	k1gMnSc4
nebo	nebo	k8xC
herečku	herečka	k1gFnSc4
za	za	k7c4
mimořádný	mimořádný	k2eAgInSc4d1
komediální	komediální	k2eAgInSc4d1
výkon	výkon	k1gInSc4
v	v	k7c6
televizních	televizní	k2eAgFnPc6d1
inscenacích	inscenace	k1gFnPc6
<g/>
,	,	kIx,
filmech	film	k1gInPc6
<g/>
,	,	kIx,
seriálech	seriál	k1gInPc6
či	či	k8xC
hraných	hraný	k2eAgInPc6d1
dokumentech	dokument	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prezídium	prezídium	k1gNnSc1
Herecké	herecký	k2eAgFnSc2d1
asociace	asociace	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc4,k3yRgNnSc4,k3yIgNnSc4
cenu	cena	k1gFnSc4
uděluje	udělovat	k5eAaImIp3nS
<g/>
,	,	kIx,
ocenilo	ocenit	k5eAaPmAgNnS
především	především	k6eAd1
jemný	jemný	k2eAgInSc4d1
tragikomický	tragikomický	k2eAgInSc4d1
tón	tón	k1gInSc4
postavy	postava	k1gFnSc2
Zuzky	Zuzka	k1gFnSc2
a	a	k8xC
inteligentní	inteligentní	k2eAgInSc1d1
humor	humor	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Filmografie	filmografie	k1gFnSc1
</s>
<s>
2000	#num#	k4
To	to	k9
jsem	být	k5eAaImIp1nS
z	z	k7c2
toho	ten	k3xDgNnSc2
jelen	jelen	k1gMnSc1
(	(	kIx(
<g/>
TV	TV	kA
seriál	seriál	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
2003	#num#	k4
Probuzená	probuzený	k2eAgFnSc1d1
skála	skála	k1gFnSc1
</s>
<s>
2003	#num#	k4
Seance	seance	k1gFnSc1
Fiction	Fiction	k1gInSc4
</s>
<s>
2004	#num#	k4
On	on	k3xPp3gMnSc1
je	on	k3xPp3gMnPc4
žena	žena	k1gFnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
TV	TV	kA
seriál	seriál	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2004	#num#	k4
Redakce	redakce	k1gFnSc1
(	(	kIx(
<g/>
TV	TV	kA
seriál	seriál	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2005	#num#	k4
3	#num#	k4
plus	plus	k1gInSc1
1	#num#	k4
s	s	k7c7
Miroslavem	Miroslav	k1gMnSc7
Donutilem	Donutil	k1gMnSc7
(	(	kIx(
<g/>
TV	TV	kA
seriál	seriál	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2005	#num#	k4
To	ten	k3xDgNnSc1
nevymyslíš	vymyslet	k5eNaPmIp2nS
<g/>
!	!	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
TV	TV	kA
seriál	seriál	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2006	#num#	k4
Horákovi	Horák	k1gMnSc3
(	(	kIx(
<g/>
TV	TV	kA
seriál	seriál	k1gInSc1
<g/>
)	)	kIx)
…	…	k?
Eva	Eva	k1gFnSc1
</s>
<s>
2006	#num#	k4
Růženka	Růženka	k1gFnSc1
<g/>
,	,	kIx,
zvaná	zvaný	k2eAgFnSc1d1
Šípková	Šípková	k1gFnSc1
</s>
<s>
2006	#num#	k4
Pokus	pokus	k1gInSc1
</s>
<s>
2007	#num#	k4
Zdivočelá	zdivočelý	k2eAgFnSc1d1
země	země	k1gFnSc1
(	(	kIx(
<g/>
TV	TV	kA
seriál	seriál	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2007	#num#	k4
Chyťte	chytit	k5eAaPmRp2nP
doktora	doktor	k1gMnSc4
<g/>
!	!	kIx.
</s>
<s>
2007	#num#	k4
Skeletoni	Skeleton	k1gMnPc5
</s>
<s>
2007	#num#	k4
Aussig	Aussig	k1gInSc1
(	(	kIx(
<g/>
nezávislý	závislý	k2eNgInSc1d1
film	film	k1gInSc1
<g/>
)	)	kIx)
…	…	k?
Lenka	Lenka	k1gFnSc1
Šimková	Šimková	k1gFnSc1
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2008	#num#	k4
Ďáblova	ďáblův	k2eAgFnSc1d1
lest	lest	k1gFnSc1
(	(	kIx(
<g/>
TV	TV	kA
seriál	seriál	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2008	#num#	k4
Bathory	Bathor	k1gMnPc7
</s>
<s>
2008	#num#	k4
Pohádkové	pohádkový	k2eAgInPc1d1
počasí	počasí	k1gNnSc3
</s>
<s>
2009	#num#	k4
Odsouzené	odsouzená	k1gFnPc1
(	(	kIx(
<g/>
TV	TV	kA
seriál	seriál	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2009	#num#	k4
Expozitura	expozitura	k1gFnSc1
(	(	kIx(
<g/>
TV	TV	kA
seriál	seriál	k1gInSc1
<g/>
)	)	kIx)
…	…	k?
Tereza	Tereza	k1gFnSc1
Hodačová	Hodačová	k1gFnSc1
</s>
<s>
2009	#num#	k4
Proč	proč	k6eAd1
bychom	by	kYmCp1nP
se	se	k3xPyFc4
netopili	topit	k5eNaImAgMnP
(	(	kIx(
<g/>
TV	TV	kA
seriál	seriál	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
Vyprávěj	vyprávět	k5eAaImRp2nS
(	(	kIx(
<g/>
TV	TV	kA
seriál	seriál	k1gInSc1
<g/>
)	)	kIx)
…	…	k?
Zuzka	Zuzka	k1gFnSc1
Dvořáková	Dvořáková	k1gFnSc1
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2010	#num#	k4
Cizí	cizí	k2eAgInSc1d1
příběh	příběh	k1gInSc1
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2010	#num#	k4
Kriminálka	kriminálka	k1gFnSc1
Anděl	Anděla	k1gFnPc2
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
III	III	kA
<g/>
.	.	kIx.
série	série	k1gFnSc1
–	–	k?
Pohřbená	pohřbený	k2eAgFnSc1d1
zaživa	zaživa	k6eAd1
<g/>
)	)	kIx)
...	...	k?
Tereza	Tereza	k1gFnSc1
Šmuková	Šmuková	k1gFnSc1
<g/>
,	,	kIx,
vedoucí	vedoucí	k1gMnSc1
prodejny	prodejna	k1gFnSc2
</s>
<s>
2010	#num#	k4
Hasiči	hasič	k1gMnPc1
</s>
<s>
2011	#num#	k4
Hlasy	hlas	k1gInPc4
za	za	k7c7
zdí	zeď	k1gFnSc7
</s>
<s>
2011	#num#	k4
Dr	dr	kA
<g/>
.	.	kIx.
Ludsky	Ludsek	k1gInPc1
</s>
<s>
2012	#num#	k4
Chci	chtít	k5eAaImIp1nS
tě	ty	k3xPp2nSc4
<g/>
?	?	kIx.
</s>
<s>
2012	#num#	k4
L	L	kA
<g/>
'	'	kIx"
<g/>
olimpiade	olimpiást	k5eAaPmIp3nS
nascosta	nascosta	k1gMnSc1
</s>
<s>
2013	#num#	k4
Bez	bez	k7c2
doteku	dotek	k1gInSc2
…	…	k?
Týna	Týn	k1gInSc2
</s>
<s>
2014	#num#	k4
Zakázané	zakázaný	k2eAgInPc1d1
uvolnění	uvolnění	k1gNnSc3
</s>
<s>
2014	#num#	k4
Hany	Hana	k1gFnSc2
</s>
<s>
2015	#num#	k4
Slíbená	slíbený	k2eAgFnSc1d1
princezna	princezna	k1gFnSc1
</s>
<s>
2015	#num#	k4
Padesátka	padesátka	k1gFnSc1
</s>
<s>
2015	#num#	k4
Já	já	k3xPp1nSc1
<g/>
,	,	kIx,
Olga	Olga	k1gFnSc1
Hepnarová	Hepnarová	k1gFnSc1
</s>
<s>
2015	#num#	k4
Miluji	milovat	k5eAaImIp1nS
tě	ty	k3xPp2nSc4
modře	modro	k6eAd1
</s>
<s>
2015	#num#	k4
Atentát	atentát	k1gInSc1
(	(	kIx(
<g/>
TV	TV	kA
seriál	seriál	k1gInSc1
<g/>
,	,	kIx,
volné	volný	k2eAgNnSc1d1
pokračování	pokračování	k1gNnSc1
Expozitury	expozitura	k1gFnSc2
<g/>
)	)	kIx)
…	…	k?
Tereza	Tereza	k1gFnSc1
Hodačová	Hodačová	k1gFnSc1
</s>
<s>
2015	#num#	k4
Vraždy	vražda	k1gFnSc2
v	v	k7c6
kruhu	kruh	k1gInSc6
(	(	kIx(
<g/>
TV	TV	kA
seriál	seriál	k1gInSc1
<g/>
)	)	kIx)
…	…	k?
astroložka	astroložka	k1gFnSc1
Sábina	Sábin	k2eAgFnSc1d1
Borová	borový	k2eAgFnSc1d1
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2016	#num#	k4
Hlas	hlas	k1gInSc1
pro	pro	k7c4
římského	římský	k2eAgMnSc4d1
krále	král	k1gMnSc4
(	(	kIx(
<g/>
TV	TV	kA
film	film	k1gInSc1
<g/>
)	)	kIx)
…	…	k?
Eliška	Eliška	k1gFnSc1
Přemyslovna	Přemyslovna	k1gFnSc1
</s>
<s>
2016	#num#	k4
Lída	Lída	k1gFnSc1
Baarová	Baarová	k1gFnSc1
...	...	k?
Journalist	Journalist	k1gInSc1
</s>
<s>
2017	#num#	k4
Kafe	Kafe	k?
a	a	k8xC
cigárko	cigárko	k?
</s>
<s>
2017	#num#	k4
Milada	Milada	k1gFnSc1
</s>
<s>
2017	#num#	k4
Nejlepší	dobrý	k2eAgFnSc7d3
přítel	přítel	k1gMnSc1
</s>
<s>
2017	#num#	k4
Specialisté	specialista	k1gMnPc1
</s>
<s>
2017	#num#	k4
Muzzikanti	Muzzikant	k1gMnPc1
(	(	kIx(
<g/>
CZ	CZ	kA
dabing	dabing	k1gInSc1
pro	pro	k7c4
polskou	polský	k2eAgFnSc4d1
herečku	herečka	k1gFnSc4
Michalinu	Michalin	k2eAgFnSc4d1
Olszańskou	Olszańská	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
2017	#num#	k4
All	All	k1gMnSc1
Wrong	Wrong	k1gMnSc1
</s>
<s>
2017	#num#	k4
Četníci	četník	k1gMnPc1
z	z	k7c2
Luhačovic	Luhačovice	k1gFnPc2
(	(	kIx(
<g/>
TV	TV	kA
seriál	seriál	k1gInSc1
<g/>
)	)	kIx)
…	…	k?
Růžena	Růžena	k1gFnSc1
Kraklová	Kraklová	k1gFnSc1
</s>
<s>
2017	#num#	k4
Bajkeři	Bajker	k1gMnPc5
...	...	k?
Tereza	Tereza	k1gFnSc1
</s>
<s>
2017	#num#	k4
Lajna	lajna	k1gFnSc1
(	(	kIx(
<g/>
TV	TV	kA
seriál	seriál	k1gInSc1
<g/>
)	)	kIx)
...	...	k?
Denisa	Denisa	k1gFnSc1
</s>
<s>
2017	#num#	k4
Single	singl	k1gInSc5
Man	Man	k1gMnSc1
</s>
<s>
2018	#num#	k4
Surfer	Surfer	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Paradise	Paradis	k1gInSc6
</s>
<s>
2018	#num#	k4
Carte	Cart	k1gMnSc5
Blanche	Blanchus	k1gMnSc5
...	...	k?
Olivia	Olivius	k1gMnSc4
Ray	Ray	k1gMnSc4
</s>
<s>
2019	#num#	k4
Lajna	lajna	k1gFnSc1
2	#num#	k4
(	(	kIx(
<g/>
seriál	seriál	k1gInSc1
<g/>
)	)	kIx)
...	...	k?
Denisa	Denisa	k1gFnSc1
</s>
<s>
2019	#num#	k4
Špindl	Špindl	k1gFnSc1
2	#num#	k4
</s>
<s>
2019	#num#	k4
Sněží	sněžit	k5eAaImIp3nP
<g/>
!	!	kIx.
</s>
<s>
2020	#num#	k4
Případ	případ	k1gInSc1
mrtvého	mrtvý	k2eAgMnSc2d1
nebožtíka	nebožtík	k1gMnSc2
</s>
<s>
2020	#num#	k4
Poslední	poslední	k2eAgInSc1d1
z	z	k7c2
Aporveru	Aporver	k1gInSc2
</s>
<s>
2020	#num#	k4
Tichý	Tichý	k1gMnSc1
společník	společník	k1gMnSc1
</s>
<s>
2020	#num#	k4
Ztraceni	ztracen	k2eAgMnPc1d1
v	v	k7c6
Ráji	rája	k1gFnSc6
</s>
<s>
2020	#num#	k4
Prvok	prvok	k1gMnSc1
<g/>
,	,	kIx,
Šampón	Šampón	k?
<g/>
,	,	kIx,
Tečka	tečka	k1gFnSc1
a	a	k8xC
Karel	Karel	k1gMnSc1
</s>
<s>
2020	#num#	k4
Prahala	Prahal	k1gMnSc4
</s>
<s>
Videoklipy	videoklip	k1gInPc1
</s>
<s>
2004	#num#	k4
Turisté	turist	k1gMnPc1
smrti	smrt	k1gFnSc2
(	(	kIx(
<g/>
hudební	hudební	k2eAgInSc1d1
klip	klip	k1gInSc1
skupiny	skupina	k1gFnSc2
Asmodeus	Asmodeus	k1gMnSc1
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
</s>
<s>
2006	#num#	k4
Zvyřátka	Zvyřátka	k1gFnSc1
(	(	kIx(
<g/>
hudební	hudební	k2eAgInSc1d1
klip	klip	k1gInSc1
skupiny	skupina	k1gFnSc2
Prago	Prago	k1gMnSc1
Union	union	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2009	#num#	k4
Ideál	ideál	k1gInSc1
hudební	hudební	k2eAgInSc4d1
klip	klip	k1gInSc4
skupiny	skupina	k1gFnSc2
POST	post	k1gInSc1
IT	IT	kA
</s>
<s>
2010	#num#	k4
Hotel	hotel	k1gInSc1
Morava	Morava	k1gFnSc1
(	(	kIx(
<g/>
hudební	hudební	k2eAgInSc1d1
klip	klip	k1gInSc1
Michala	Michal	k1gMnSc2
Hrůzy	hrůza	k1gFnSc2
s	s	k7c7
jeho	jeho	k3xOp3gFnSc7
Kapelou	kapela	k1gFnSc7
Hrůzy	hrůza	k1gFnSc2
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Jan	Jan	k1gMnSc1
Budař	Budař	k1gMnSc1
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
</s>
<s>
2011	#num#	k4
Přísahám	přísahat	k5eAaImIp1nS
(	(	kIx(
<g/>
hudební	hudební	k2eAgInSc4d1
klip	klip	k1gInSc4
Terezy	Tereza	k1gFnSc2
Kerndlové	Kerndlový	k2eAgFnSc2d1
<g/>
,	,	kIx,
dále	daleko	k6eAd2
hrají	hrát	k5eAaImIp3nP
mj.	mj.	kA
Jaromír	Jaromír	k1gMnSc1
Nosek	Nosek	k1gMnSc1
<g/>
,	,	kIx,
Andrea	Andrea	k1gFnSc1
Kerestešová	Kerestešová	k1gFnSc1
<g/>
,	,	kIx,
Kristýna	Kristýna	k1gFnSc1
Leichtová	Leichtová	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
2014	#num#	k4
Zakázané	zakázaný	k2eAgNnSc1d1
uvolnění	uvolnění	k1gNnSc1
(	(	kIx(
<g/>
hudební	hudební	k2eAgInSc1d1
klip	klip	k1gInSc1
Michala	Michal	k1gMnSc2
Hrůzy	Hrůza	k1gMnSc2
ke	k	k7c3
stejnojmennému	stejnojmenný	k2eAgInSc3d1
filmu	film	k1gInSc3
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2015	#num#	k4
Nafrněná	Nafrněný	k2eAgFnSc1d1
(	(	kIx(
<g/>
hudební	hudební	k2eAgInSc4d1
klip	klip	k1gInSc4
Barbory	Barbora	k1gFnSc2
Polákové	Poláková	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
2015	#num#	k4
Ty	ty	k3xPp2nSc1
a	a	k8xC
já	já	k3xPp1nSc1
(	(	kIx(
<g/>
hudební	hudební	k2eAgInSc1d1
klip	klip	k1gInSc1
skupiny	skupina	k1gFnSc2
Kryštof	Kryštof	k1gMnSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Karin	Karina	k1gFnPc2
Babinská	Babinský	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
</s>
<s>
Divadlo	divadlo	k1gNnSc1
</s>
<s>
A	A	kA
studio	studio	k1gNnSc1
Rubín	rubín	k1gInSc1
<g/>
:	:	kIx,
Moje	můj	k3xOp1gFnSc1
malá	malý	k2eAgFnSc1d1
úchylka	úchylka	k1gFnSc1
<g/>
,	,	kIx,
2017	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://ocko.stream.cz/inbox/505709-inbox-hana-vagnerova	http://ocko.stream.cz/inbox/505709-inbox-hana-vagnerův	k2eAgFnSc1d1
Archivováno	archivován	k2eAgNnSc4d1
10	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
(	(	kIx(
<g/>
čas	čas	k1gInSc1
4	#num#	k4
<g/>
:	:	kIx,
<g/>
50	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
http://ocko.stream.cz/inbox/505709-inbox-hana-vagnerova	http://ocko.stream.cz/inbox/505709-inbox-hana-vagnerův	k2eAgFnSc1d1
Archivováno	archivován	k2eAgNnSc4d1
10	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
(	(	kIx(
<g/>
čas	čas	k1gInSc1
4	#num#	k4
<g/>
:	:	kIx,
<g/>
40	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.televize.cz	www.televize.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
http://www.pluska.sk/soubiznis/spravy-klebety/zahranicne-celebrity/o-drogach-anorexii-komplexoch.html	http://www.pluska.sk/soubiznis/spravy-klebety/zahranicne-celebrity/o-drogach-anorexii-komplexoch.html	k1gMnSc1
<g/>
↑	↑	k?
http://www.pluska.sk/soubiznis/zo-sveta-zabavy/televizia/vagnerova-rozisla-frajerom-ktory-ju-zachranil-3.html	http://www.pluska.sk/soubiznis/zo-sveta-zabavy/televizia/vagnerova-rozisla-frajerom-ktory-ju-zachranil-3.html	k1gMnSc1
<g/>
↑	↑	k?
Festival	festival	k1gInSc1
Novoměstský	novoměstský	k2eAgInSc1d1
Hrnec	hrnec	k1gInSc1
smíchu	smích	k1gInSc2
–	–	k?
cena	cena	k1gFnSc1
Televizní	televizní	k2eAgFnSc1d1
objev	objev	k1gInSc4
roku	rok	k1gInSc2
2011	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
www.hereckaasociace.cz	www.hereckaasociace.cz	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.aussig-film.cz	www.aussig-film.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Zuzka	Zuzka	k1gFnSc1
Dvořáková	Dvořáková	k1gFnSc1
(	(	kIx(
<g/>
Hanka	Hanka	k1gFnSc1
Vagnerová	Vagnerová	k1gFnSc1
<g/>
)	)	kIx)
na	na	k7c6
webu	web	k1gInSc6
České	český	k2eAgFnSc2d1
televize	televize	k1gFnSc2
<g/>
↑	↑	k?
Cizí	cizí	k2eAgInSc1d1
příběh	příběh	k1gInSc1
<g/>
1	#num#	k4
2	#num#	k4
http://www.fdb.cz/serial/vrazdy-v-kruhu/109587	http://www.fdb.cz/serial/vrazdy-v-kruhu/109587	k4
<g/>
↑	↑	k?
ASMODEUS	ASMODEUS	kA
–	–	k?
Obrazová	obrazový	k2eAgFnSc1d1
prezentace	prezentace	k1gFnSc1
kapely	kapela	k1gFnSc2
aneb	aneb	k?
klipy	klip	k1gInPc1
<g/>
,	,	kIx,
herečky	herečka	k1gFnPc1
<g/>
,	,	kIx,
herci	herec	k1gMnPc1
<g/>
…	…	k?
a	a	k8xC
DVD	DVD	kA
<g/>
↑	↑	k?
Hrůza	hrůza	k1gFnSc1
s	s	k7c7
kapelou	kapela	k1gFnSc7
vyráží	vyrážet	k5eAaImIp3nS
na	na	k7c4
turné	turné	k1gNnSc4
z	z	k7c2
Hotelu	hotel	k1gInSc2
Morava	Morava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podívejte	podívat	k5eAaImRp2nP,k5eAaPmRp2nP
se	se	k3xPyFc4
<g/>
↑	↑	k?
VEDRAL	Vedral	k1gMnSc1
<g/>
,	,	kIx,
Honza	Honza	k1gMnSc1
<g/>
:	:	kIx,
VIDEO	video	k1gNnSc1
<g/>
:	:	kIx,
K	k	k7c3
novému	nový	k2eAgMnSc3d1
Hřebejkovi	Hřebejek	k1gMnSc3
nazpíval	nazpívat	k5eAaBmAgMnS,k5eAaPmAgMnS
titulní	titulní	k2eAgFnSc4d1
píseň	píseň	k1gFnSc4
Michal	Michal	k1gMnSc1
Hrůza	Hrůza	k1gMnSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
28	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
VIDEO	video	k1gNnSc1
<g/>
:	:	kIx,
Kryštof	Kryštof	k1gMnSc1
vypustil	vypustit	k5eAaPmAgInS
svůj	svůj	k3xOyFgInSc4
výpravný	výpravný	k2eAgInSc4d1
klip	klip	k1gInSc4
Ty	ty	k3xPp2nSc5
a	a	k8xC
já	já	k3xPp1nSc1
<g/>
,	,	kIx,
charitou	charita	k1gFnSc7
pomohl	pomoct	k5eAaPmAgMnS
dětem	dítě	k1gFnPc3
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Hana	Hana	k1gFnSc1
Vagnerová	Vagnerový	k2eAgFnSc1d1
</s>
<s>
Obrázky	obrázek	k1gInPc1
k	k	k7c3
tématu	téma	k1gNnSc3
Hana	Hana	k1gFnSc1
Vagnerová	Vagnerový	k2eAgFnSc1d1
na	na	k7c4
Obalkyknih	Obalkyknih	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Profil	profil	k1gInSc1
Hany	Hana	k1gFnSc2
Vagnerové	Vagnerová	k1gFnSc2
na	na	k7c6
webu	web	k1gInSc6
Městských	městský	k2eAgNnPc2d1
divadel	divadlo	k1gNnPc2
pražských	pražský	k2eAgNnPc2d1
</s>
<s>
Hana	Hana	k1gFnSc1
Vagnerová	Vagnerový	k2eAgFnSc1d1
na	na	k7c6
Kinoboxu	Kinobox	k1gInSc6
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Hana	Hana	k1gFnSc1
Vagnerová	Vagnerový	k2eAgFnSc1d1
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Hana	Hana	k1gFnSc1
Vagnerová	Vagnerový	k2eAgFnSc1d1
ve	v	k7c6
Filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Hana	Hana	k1gFnSc1
Vagnerová	Vagnerový	k2eAgFnSc1d1
v	v	k7c4
Internet	Internet	k1gInSc4
Movie	Movie	k1gFnSc2
Database	Databasa	k1gFnSc3
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
149662	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
3120	#num#	k4
256X	256X	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
249895021	#num#	k4
</s>
