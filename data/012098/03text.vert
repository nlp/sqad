<p>
<s>
Kdoulovec	kdoulovec	k1gInSc1	kdoulovec
(	(	kIx(	(
<g/>
Chaenomeles	Chaenomeles	k1gInSc1	Chaenomeles
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc1	rod
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
listnatých	listnatý	k2eAgFnPc2d1	listnatá
opadavých	opadavý	k2eAgFnPc2d1	opadavá
dřevin	dřevina	k1gFnPc2	dřevina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
růžovité	růžovitý	k2eAgFnSc2d1	růžovitý
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
jsou	být	k5eAaImIp3nP	být
pěstovány	pěstován	k2eAgFnPc1d1	pěstována
jako	jako	k8xC	jako
okrasné	okrasný	k2eAgFnPc1d1	okrasná
rostliny	rostlina	k1gFnPc1	rostlina
ozdobné	ozdobný	k2eAgFnSc2d1	ozdobná
květem	květ	k1gInSc7	květ
<g/>
.	.	kIx.	.
</s>
<s>
Keře	keř	k1gInPc1	keř
rodu	rod	k1gInSc2	rod
jsou	být	k5eAaImIp3nP	být
otrněné	otrněný	k2eAgFnPc1d1	otrněná
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
nepravidelného	pravidelný	k2eNgInSc2d1	nepravidelný
habitu	habitus	k1gInSc2	habitus
<g/>
,	,	kIx,	,
značně	značně	k6eAd1	značně
variabilní	variabilní	k2eAgInSc1d1	variabilní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Rod	rod	k1gInSc1	rod
kdoulovec	kdoulovec	k1gInSc1	kdoulovec
<g/>
,	,	kIx,	,
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
znám	znát	k5eAaImIp1nS	znát
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Chaenomeles	Chaenomelesa	k1gFnPc2	Chaenomelesa
<g/>
,	,	kIx,	,
býval	bývat	k5eAaImAgInS	bývat
botaniky	botanika	k1gFnSc2	botanika
považován	považován	k2eAgInSc1d1	považován
jednu	jeden	k4xCgFnSc4	jeden
dobu	doba	k1gFnSc4	doba
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
jabloní	jabloň	k1gFnPc2	jabloň
<g/>
,	,	kIx,	,
hrušní	hrušeň	k1gFnPc2	hrušeň
či	či	k8xC	či
později	pozdě	k6eAd2	pozdě
kdoulí	kdoule	k1gFnPc2	kdoule
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
podobnosti	podobnost	k1gFnSc3	podobnost
plodů	plod	k1gInPc2	plod
s	s	k7c7	s
vyjmenovanými	vyjmenovaný	k2eAgInPc7d1	vyjmenovaný
rody	rod	k1gInPc7	rod
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
dovezený	dovezený	k2eAgInSc4d1	dovezený
druh	druh	k1gInSc4	druh
z	z	k7c2	z
Japonska	Japonsko	k1gNnSc2	Japonsko
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
postupně	postupně	k6eAd1	postupně
Pyrus	Pyrus	k1gMnSc1	Pyrus
japonica	japonica	k1gMnSc1	japonica
(	(	kIx(	(
<g/>
1803	[number]	k4	1803
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Malus	Malus	k1gMnSc1	Malus
japonica	japonica	k1gMnSc1	japonica
(	(	kIx(	(
<g/>
1807	[number]	k4	1807
<g/>
)	)	kIx)	)
či	či	k8xC	či
Cydonia	Cydonium	k1gNnSc2	Cydonium
japonica	japonic	k1gInSc2	japonic
(	(	kIx(	(
<g/>
1817	[number]	k4	1817
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chaenomeles	Chaenomeles	k1gInSc1	Chaenomeles
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
pocházející	pocházející	k2eAgFnSc2d1	pocházející
odvozením	odvození	k1gNnPc3	odvození
vlastností	vlastnost	k1gFnPc2	vlastnost
jejich	jejich	k3xOp3gInPc2	jejich
plodů	plod	k1gInPc2	plod
–	–	k?	–
malvice	malvice	k1gFnSc1	malvice
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
meles	meles	k1gInSc1	meles
=	=	kIx~	=
jablko	jablko	k1gNnSc1	jablko
<g/>
)	)	kIx)	)
a	a	k8xC	a
pukat	pukat	k5eAaImF	pukat
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
chainein	chainein	k1gInSc1	chainein
=	=	kIx~	=
štěpit	štěpit	k5eAaImF	štěpit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnohé	mnohý	k2eAgFnSc6d1	mnohá
starší	starý	k2eAgFnSc6d2	starší
literatuře	literatura	k1gFnSc6	literatura
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgInS	uvádět
původní	původní	k2eAgInSc1d1	původní
název	název	k1gInSc1	název
Choenomeles	Choenomeles	k1gInSc1	Choenomeles
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
až	až	k6eAd1	až
posléze	posléze	k6eAd1	posléze
nahrazen	nahradit	k5eAaPmNgInS	nahradit
výrazem	výraz	k1gInSc7	výraz
Chaenomeles	Chaenomelesa	k1gFnPc2	Chaenomelesa
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
čeledě	čeleď	k1gFnSc2	čeleď
Rosaceae	Rosacea	k1gFnSc2	Rosacea
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
však	však	k8xC	však
rod	rod	k1gInSc1	rod
řadí	řadit	k5eAaImIp3nS	řadit
pod	pod	k7c4	pod
čeleď	čeleď	k1gFnSc4	čeleď
Malaceae	Malacea	k1gFnSc2	Malacea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Současná	současný	k2eAgFnSc1d1	současná
taxonomie	taxonomie	k1gFnSc1	taxonomie
uznává	uznávat	k5eAaImIp3nS	uznávat
4	[number]	k4	4
nebo	nebo	k8xC	nebo
5	[number]	k4	5
druhů	druh	k1gInPc2	druh
rodu	rod	k1gInSc2	rod
Chaenomeles	Chaenomelesa	k1gFnPc2	Chaenomelesa
<g/>
.	.	kIx.	.
</s>
<s>
Náleží	náležet	k5eAaImIp3nS	náležet
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
druhy	druh	k1gInPc4	druh
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
japonica	japonica	k1gMnSc1	japonica
<g/>
,	,	kIx,	,
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
speciosa	speciosa	k1gFnSc1	speciosa
<g/>
,	,	kIx,	,
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
cathayensis	cathayensis	k1gInSc1	cathayensis
<g/>
,	,	kIx,	,
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
thibetica	thibeticus	k1gMnSc2	thibeticus
a	a	k8xC	a
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
sinensis	sinensis	k1gInSc1	sinensis
<g/>
.	.	kIx.	.
</s>
<s>
Druh	druh	k1gInSc1	druh
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
sinensis	sinensis	k1gInSc1	sinensis
je	být	k5eAaImIp3nS	být
řazen	řadit	k5eAaImNgInS	řadit
některými	některý	k3yIgInPc7	některý
taxonomy	taxonom	k1gInPc7	taxonom
do	do	k7c2	do
samostatného	samostatný	k2eAgInSc2d1	samostatný
rodu	rod	k1gInSc2	rod
Pseudocydonia	Pseudocydonium	k1gNnSc2	Pseudocydonium
jako	jako	k8xC	jako
Pseudocydonia	Pseudocydonium	k1gNnSc2	Pseudocydonium
sinensis	sinensis	k1gFnSc2	sinensis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Rod	rod	k1gInSc1	rod
kdoulovec	kdoulovec	k1gInSc1	kdoulovec
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
opadavé	opadavý	k2eAgInPc4d1	opadavý
trnité	trnitý	k2eAgInPc4d1	trnitý
rozkladité	rozkladitý	k2eAgInPc4d1	rozkladitý
vytrvalé	vytrvalý	k2eAgInPc4d1	vytrvalý
keře	keř	k1gInPc4	keř
s	s	k7c7	s
podlouhle	podlouhle	k6eAd1	podlouhle
vejčitými	vejčitý	k2eAgFnPc7d1	vejčitá
<g/>
,	,	kIx,	,
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
pilovitými	pilovitý	k2eAgFnPc7d1	pilovitá
nebo	nebo	k8xC	nebo
vroubkovanými	vroubkovaný	k2eAgFnPc7d1	vroubkovaná
<g/>
,	,	kIx,	,
na	na	k7c6	na
povrchu	povrch	k1gInSc2	povrch
lesklými	lesklý	k2eAgInPc7d1	lesklý
listy	list	k1gInPc7	list
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
větvi	větev	k1gFnSc6	větev
uspořádané	uspořádaný	k2eAgInPc1d1	uspořádaný
střídavě	střídavě	k6eAd1	střídavě
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
velké	velký	k2eAgInPc4d1	velký
palisty	palist	k1gInPc4	palist
<g/>
.	.	kIx.	.
</s>
<s>
Květy	Květa	k1gFnPc4	Květa
má	mít	k5eAaImIp3nS	mít
oboupohlavné	oboupohlavný	k2eAgNnSc1d1	oboupohlavné
i	i	k8xC	i
prašníkové	prašníkový	k2eAgNnSc1d1	prašníkový
<g/>
,	,	kIx,	,
ve	v	k7c6	v
svazečcích	svazeček	k1gInPc6	svazeček
nebo	nebo	k8xC	nebo
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
<g/>
.	.	kIx.	.
</s>
<s>
Vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
na	na	k7c6	na
postranních	postranní	k2eAgFnPc6d1	postranní
větvičkách	větvička	k1gFnPc6	větvička
vyrůstajících	vyrůstající	k2eAgFnPc2d1	vyrůstající
ze	z	k7c2	z
starého	starý	k2eAgNnSc2d1	staré
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
a	a	k8xC	a
podobají	podobat	k5eAaImIp3nP	podobat
se	se	k3xPyFc4	se
květům	květ	k1gInPc3	květ
jabloní	jabloň	k1gFnPc2	jabloň
<g/>
.	.	kIx.	.
</s>
<s>
Kvetou	kvést	k5eAaImIp3nP	kvést
před	před	k7c7	před
olistěním	olistění	k1gNnSc7	olistění
nebo	nebo	k8xC	nebo
současně	současně	k6eAd1	současně
s	s	k7c7	s
rašením	rašení	k1gNnSc7	rašení
listů	list	k1gInPc2	list
<g/>
.	.	kIx.	.
</s>
<s>
Květ	květ	k1gInSc1	květ
má	mít	k5eAaImIp3nS	mít
opadavý	opadavý	k2eAgInSc4d1	opadavý
kalich	kalich	k1gInSc4	kalich
<g/>
,	,	kIx,	,
červenou	červený	k2eAgFnSc4d1	červená
korunu	koruna	k1gFnSc4	koruna
<g/>
,	,	kIx,	,
počet	počet	k1gInSc4	počet
tyčinek	tyčinka	k1gFnPc2	tyčinka
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
20	[number]	k4	20
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
</s>
<s>
Semeník	semeník	k1gInSc4	semeník
je	být	k5eAaImIp3nS	být
spodní	spodní	k2eAgNnSc1d1	spodní
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
5	[number]	k4	5
různě	různě	k6eAd1	různě
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
při	při	k7c6	při
bázi	báze	k1gFnSc6	báze
srostlých	srostlý	k2eAgFnPc2d1	srostlá
čnělek	čnělka	k1gFnPc2	čnělka
<g/>
.	.	kIx.	.
</s>
<s>
Vajíčka	vajíčko	k1gNnPc1	vajíčko
jsou	být	k5eAaImIp3nP	být
početná	početný	k2eAgNnPc1d1	početné
a	a	k8xC	a
Oplodí	oplodit	k5eAaPmIp3nP	oplodit
je	on	k3xPp3gMnPc4	on
blanité	blanitý	k2eAgMnPc4d1	blanitý
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
čtyři	čtyři	k4xCgInPc1	čtyři
druhy	druh	k1gInPc1	druh
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgInPc1	všechen
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
kultivary	kultivar	k1gInPc1	kultivar
a	a	k8xC	a
variety	varieta	k1gFnPc1	varieta
jsou	být	k5eAaImIp3nP	být
dřeviny	dřevina	k1gFnPc4	dřevina
slunných	slunný	k2eAgNnPc2d1	slunné
a	a	k8xC	a
teplých	teplý	k2eAgNnPc2d1	teplé
stanovišť	stanoviště	k1gNnPc2	stanoviště
<g/>
.	.	kIx.	.
</s>
<s>
Vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
propustné	propustný	k2eAgFnSc2d1	propustná
střední	střední	k2eAgFnSc2d1	střední
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
snášejí	snášet	k5eAaImIp3nP	snášet
sucho	sucho	k6eAd1	sucho
<g/>
.	.	kIx.	.
</s>
<s>
Kdoulovce	kdoulovec	k1gInPc1	kdoulovec
kvetou	kvést	k5eAaImIp3nP	kvést
v	v	k7c6	v
období	období	k1gNnSc6	období
mezi	mezi	k7c7	mezi
březnem	březen	k1gInSc7	březen
a	a	k8xC	a
dubnem	duben	k1gInSc7	duben
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
hlavní	hlavní	k2eAgFnSc1d1	hlavní
doba	doba	k1gFnSc1	doba
květu	květ	k1gInSc2	květ
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
<g/>
.	.	kIx.	.
</s>
<s>
Kdoulovec	kdoulovec	k1gInSc4	kdoulovec
kvete	kvést	k5eAaImIp3nS	kvést
bílými	bílý	k2eAgInPc7d1	bílý
<g/>
,	,	kIx,	,
růžovými	růžový	k2eAgInPc7d1	růžový
<g/>
,	,	kIx,	,
oranžovými	oranžový	k2eAgInPc7d1	oranžový
<g/>
,	,	kIx,	,
červenými	červený	k2eAgInPc7d1	červený
či	či	k8xC	či
tmavě	tmavě	k6eAd1	tmavě
červenými	červený	k2eAgInPc7d1	červený
květy	květ	k1gInPc7	květ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
plodí	plodit	k5eAaImIp3nS	plodit
tvrdé	tvrdé	k1gNnSc1	tvrdé
žluté	žlutý	k2eAgFnSc2d1	žlutá
malvice	malvice	k1gFnSc2	malvice
podobné	podobný	k2eAgFnSc2d1	podobná
jablku	jablko	k1gNnSc3	jablko
či	či	k8xC	či
kdoulím	kdoule	k1gFnPc3	kdoule
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
povaření	povaření	k1gNnSc6	povaření
jedlé	jedlý	k2eAgNnSc1d1	jedlé
<g/>
.	.	kIx.	.
</s>
<s>
Plody	plod	k1gInPc1	plod
zároveň	zároveň	k6eAd1	zároveň
silně	silně	k6eAd1	silně
voní	vonět	k5eAaImIp3nP	vonět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Chaenomeles	Chaenomeles	k1gMnSc1	Chaenomeles
japonica	japonica	k1gMnSc1	japonica
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
vyššímu	vysoký	k2eAgInSc3d2	vyšší
vzrůstu	vzrůst	k1gInSc3	vzrůst
(	(	kIx(	(
<g/>
80	[number]	k4	80
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
cm	cm	kA	cm
<g/>
)	)	kIx)	)
vhodná	vhodný	k2eAgFnSc1d1	vhodná
pro	pro	k7c4	pro
souvislé	souvislý	k2eAgNnSc4d1	souvislé
pokrytí	pokrytí	k1gNnSc4	pokrytí
větších	veliký	k2eAgFnPc2d2	veliký
ploch	plocha	k1gFnPc2	plocha
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
netvoří	tvořit	k5eNaImIp3nS	tvořit
zapojený	zapojený	k2eAgInSc4d1	zapojený
porost	porost	k1gInSc4	porost
tak	tak	k6eAd1	tak
jako	jako	k9	jako
tráva	tráva	k1gFnSc1	tráva
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
nahradit	nahradit	k5eAaPmF	nahradit
varietou	varieta	k1gFnSc7	varieta
Chaenomeles	Chaenomeles	k1gInSc1	Chaenomeles
japonica	japonica	k1gMnSc1	japonica
var.	var.	k?	var.
alpina	alpinum	k1gNnSc2	alpinum
jež	jenž	k3xRgNnSc1	jenž
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
nízkého	nízký	k2eAgInSc2d1	nízký
vzrůstu	vzrůst	k1gInSc2	vzrůst
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
cm	cm	kA	cm
<g/>
)	)	kIx)	)
a	a	k8xC	a
snadno	snadno	k6eAd1	snadno
zapojuje	zapojovat	k5eAaImIp3nS	zapojovat
<g/>
.	.	kIx.	.
</s>
<s>
Chaenomeles	Chaenomeles	k1gInSc1	Chaenomeles
japonica	japonic	k1gInSc2	japonic
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
kultivary	kultivar	k1gInPc1	kultivar
lze	lze	k6eAd1	lze
také	také	k9	také
použít	použít	k5eAaPmF	použít
pro	pro	k7c4	pro
výsadbu	výsadba	k1gFnSc4	výsadba
nízkých	nízký	k2eAgInPc2d1	nízký
živých	živý	k2eAgInPc2d1	živý
plůtků	plůtek	k1gInPc2	plůtek
<g/>
,	,	kIx,	,
obrub	obruba	k1gFnPc2	obruba
či	či	k8xC	či
trnitých	trnitý	k2eAgInPc2d1	trnitý
neproniknutelných	proniknutelný	k2eNgInPc2d1	neproniknutelný
plotů	plot	k1gInPc2	plot
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
40	[number]	k4	40
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Chaenomeles	Chaenomeles	k1gInSc1	Chaenomeles
speciosa	speciosa	k1gFnSc1	speciosa
a	a	k8xC	a
kultivary	kultivar	k1gInPc1	kultivar
a	a	k8xC	a
Chaenomeles	Chaenomeles	k1gInSc1	Chaenomeles
x	x	k?	x
superba	superba	k1gFnSc1	superba
a	a	k8xC	a
kultivary	kultivar	k1gInPc1	kultivar
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
taktéž	taktéž	k?	taktéž
pro	pro	k7c4	pro
živé	živý	k2eAgInPc4d1	živý
či	či	k8xC	či
trnité	trnitý	k2eAgInPc4d1	trnitý
a	a	k8xC	a
neproniknutelné	proniknutelný	k2eNgInPc4d1	neproniknutelný
ploty	plot	k1gInPc4	plot
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
mezi	mezi	k7c7	mezi
jedním	jeden	k4xCgNnSc7	jeden
a	a	k8xC	a
dvěma	dva	k4xCgInPc7	dva
metry	metr	k1gInPc7	metr
<g/>
.	.	kIx.	.
</s>
<s>
Chaenomeles	Chaenomeles	k1gMnSc1	Chaenomeles
japonica	japonicum	k1gNnSc2	japonicum
var.	var.	k?	var.
alpina	alpinum	k1gNnSc2	alpinum
<g/>
,	,	kIx,	,
Chaenomeles	Chaenomeles	k1gMnSc1	Chaenomeles
japonica	japonica	k1gMnSc1	japonica
'	'	kIx"	'
<g/>
Sargentii	Sargentie	k1gFnSc4	Sargentie
<g/>
'	'	kIx"	'
a	a	k8xC	a
zahradní	zahradní	k2eAgInPc4d1	zahradní
kultivary	kultivar	k1gInPc4	kultivar
Chosan	Chosana	k1gFnPc2	Chosana
a	a	k8xC	a
Naranja	Naranjum	k1gNnSc2	Naranjum
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vysazovat	vysazovat	k5eAaImF	vysazovat
do	do	k7c2	do
skalek	skalka	k1gFnPc2	skalka
<g/>
.	.	kIx.	.
</s>
<s>
Chaenomeles	Chaenomeles	k1gInSc1	Chaenomeles
speciosa	speciosa	k1gFnSc1	speciosa
'	'	kIx"	'
<g/>
Simonii	simonie	k1gFnSc4	simonie
<g/>
'	'	kIx"	'
je	být	k5eAaImIp3nS	být
však	však	k9	však
do	do	k7c2	do
skalek	skalka	k1gFnPc2	skalka
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
vysokému	vysoký	k2eAgInSc3d1	vysoký
vzrůstu	vzrůst	k1gInSc3	vzrůst
<g/>
,	,	kIx,	,
až	až	k9	až
100	[number]	k4	100
cm	cm	kA	cm
<g/>
,	,	kIx,	,
vhodná	vhodný	k2eAgFnSc1d1	vhodná
pouze	pouze	k6eAd1	pouze
jako	jako	k8xC	jako
solitér	solitér	k1gInSc4	solitér
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
stanovištní	stanovištní	k2eAgFnSc4d1	stanovištní
odolnost	odolnost	k1gFnSc4	odolnost
jsou	být	k5eAaImIp3nP	být
kdoulovce	kdoulovec	k1gInPc1	kdoulovec
často	často	k6eAd1	často
vysazovány	vysazován	k2eAgInPc1d1	vysazován
na	na	k7c4	na
veřejná	veřejný	k2eAgNnPc4d1	veřejné
prostranství	prostranství	k1gNnPc4	prostranství
<g/>
,	,	kIx,	,
do	do	k7c2	do
značně	značně	k6eAd1	značně
znečištěných	znečištěný	k2eAgFnPc2d1	znečištěná
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
větvích	větev	k1gFnPc6	větev
tvoří	tvořit	k5eAaImIp3nP	tvořit
kolce	kolec	k1gInPc1	kolec
jež	jenž	k3xRgMnPc4	jenž
brání	bránit	k5eAaImIp3nP	bránit
okusu	okus	k1gInSc3	okus
zvěře	zvěř	k1gFnSc2	zvěř
či	či	k8xC	či
procházení	procházení	k1gNnSc2	procházení
skrz	skrz	k7c4	skrz
křoví	křoví	k1gNnSc4	křoví
lidem	člověk	k1gMnPc3	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
jako	jako	k9	jako
okrasné	okrasný	k2eAgFnPc4d1	okrasná
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
,	,	kIx,	,
náleží	náležet	k5eAaImIp3nS	náležet
k	k	k7c3	k
velmi	velmi	k6eAd1	velmi
žádaným	žádaný	k2eAgInPc3d1	žádaný
kvetoucím	kvetoucí	k2eAgInPc3d1	kvetoucí
keřům	keř	k1gInPc3	keř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Během	během	k7c2	během
květu	květ	k1gInSc2	květ
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgInPc4	všechen
kdoulovce	kdoulovec	k1gInPc4	kdoulovec
významné	významný	k2eAgInPc4d1	významný
pro	pro	k7c4	pro
včelařství	včelařství	k1gNnSc4	včelařství
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
všechny	všechen	k3xTgInPc1	všechen
druhy	druh	k1gInPc1	druh
a	a	k8xC	a
kultivary	kultivar	k1gInPc1	kultivar
jsou	být	k5eAaImIp3nP	být
vhodné	vhodný	k2eAgInPc1d1	vhodný
pro	pro	k7c4	pro
hnízdění	hnízdění	k1gNnPc4	hnízdění
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc4	druh
==	==	k?	==
</s>
</p>
<p>
<s>
kdoulovec	kdoulovec	k1gInSc1	kdoulovec
lahvicovitý	lahvicovitý	k2eAgInSc1d1	lahvicovitý
(	(	kIx(	(
<g/>
Chaenomeles	Chaenomeles	k1gInSc1	Chaenomeles
speciosa	speciosa	k1gFnSc1	speciosa
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Chaenomeles	Chaenomeles	k1gInSc1	Chaenomeles
lagenaria	lagenarium	k1gNnSc2	lagenarium
<g/>
,	,	kIx,	,
Cydonia	Cydonium	k1gNnSc2	Cydonium
japonica	japonicum	k1gNnSc2	japonicum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
kdoulovec	kdoulovec	k1gInSc1	kdoulovec
ozdobný	ozdobný	k2eAgInSc1d1	ozdobný
(	(	kIx(	(
<g/>
japonská	japonský	k2eAgFnSc1d1	japonská
kdoule	kdoule	k1gFnSc1	kdoule
<g/>
)	)	kIx)	)
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
výšky	výška	k1gFnPc4	výška
přes	přes	k7c4	přes
dva	dva	k4xCgInPc4	dva
metry	metr	k1gInPc4	metr
a	a	k8xC	a
roste	růst	k5eAaImIp3nS	růst
rozložitě	rozložitě	k6eAd1	rozložitě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
i	i	k8xC	i
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
má	mít	k5eAaImIp3nS	mít
lysé	lysý	k2eAgFnSc2d1	Lysá
větve	větev	k1gFnSc2	větev
jež	jenž	k3xRgInPc1	jenž
jsou	být	k5eAaImIp3nP	být
přibližně	přibližně	k6eAd1	přibližně
vzpřímené	vzpřímený	k2eAgMnPc4d1	vzpřímený
<g/>
,	,	kIx,	,
listy	list	k1gInPc4	list
má	mít	k5eAaImIp3nS	mít
vejčitě	vejčitě	k6eAd1	vejčitě
podlouhlé	podlouhlý	k2eAgNnSc4d1	podlouhlé
a	a	k8xC	a
špičaté	špičatý	k2eAgNnSc4d1	špičaté
s	s	k7c7	s
jemně	jemně	k6eAd1	jemně
pilovitými	pilovitý	k2eAgInPc7d1	pilovitý
okraji	okraj	k1gInPc7	okraj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
jsou	být	k5eAaImIp3nP	být
lysé	lysý	k2eAgInPc1d1	lysý
a	a	k8xC	a
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Kvete	kvést	k5eAaImIp3nS	kvést
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
cm	cm	kA	cm
velkými	velký	k2eAgFnPc7d1	velká
červenými	červená	k1gFnPc7	červená
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jeho	jeho	k3xOp3gInPc4	jeho
kultivary	kultivar	k1gInPc4	kultivar
růžovými	růžový	k2eAgInPc7d1	růžový
nebo	nebo	k8xC	nebo
bílými	bílý	k2eAgInPc7d1	bílý
květy	květ	k1gInPc7	květ
(	(	kIx(	(
<g/>
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
shlukovány	shlukovat	k5eAaImNgInP	shlukovat
do	do	k7c2	do
svazečků	svazeček	k1gInPc2	svazeček
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
růst	růst	k5eAaImF	růst
i	i	k9	i
odděleně	odděleně	k6eAd1	odděleně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
bohatě	bohatě	k6eAd1	bohatě
větvený	větvený	k2eAgInSc1d1	větvený
<g/>
,	,	kIx,	,
kolcovitě	kolcovitě	k6eAd1	kolcovitě
ostnitý	ostnitý	k2eAgMnSc1d1	ostnitý
<g/>
.	.	kIx.	.
</s>
<s>
Malvice	malvice	k1gFnSc1	malvice
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
kulovitá	kulovitý	k2eAgFnSc1d1	kulovitá
<g/>
,	,	kIx,	,
žlutozelená	žlutozelený	k2eAgFnSc1d1	žlutozelená
až	až	k6eAd1	až
načervenalá	načervenalý	k2eAgFnSc1d1	načervenalá
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
cm	cm	kA	cm
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vonná	vonný	k2eAgFnSc1d1	vonná
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
pěstován	pěstovat	k5eAaImNgInS	pěstovat
pro	pro	k7c4	pro
okrasu	okrasa	k1gFnSc4	okrasa
v	v	k7c6	v
rozmanitých	rozmanitý	k2eAgInPc6d1	rozmanitý
kultivarech	kultivar	k1gInPc6	kultivar
v	v	k7c6	v
parcích	park	k1gInPc6	park
–	–	k?	–
'	'	kIx"	'
<g/>
Apple	Apple	kA	Apple
Blossom	Blossom	k1gInSc1	Blossom
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
Atroccocinea	Atroccocine	k2eAgFnSc1d1	Atroccocine
Plena	plena	k1gFnSc1	plena
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
Atrosanguinea	Atrosanguinea	k1gFnSc1	Atrosanguinea
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
Brilliant	Brilliant	k1gInSc1	Brilliant
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
Contorta	Contorta	k1gFnSc1	Contorta
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
Marmorata	Marmorata	k1gFnSc1	Marmorata
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
Snow	Snow	k1gMnSc1	Snow
<g/>
'	'	kIx"	'
aj.	aj.	kA	aj.
Původní	původní	k2eAgNnSc1d1	původní
je	on	k3xPp3gFnPc4	on
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
a	a	k8xC	a
k	k	k7c3	k
nám	my	k3xPp1nPc3	my
introdukován	introdukován	k2eAgInSc1d1	introdukován
v	v	k7c6	v
r.	r.	kA	r.
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
<g/>
kdoulovec	kdoulovec	k1gInSc1	kdoulovec
japonský	japonský	k2eAgInSc1d1	japonský
(	(	kIx(	(
<g/>
Chaenomeles	Chaenomeles	k1gMnSc1	Chaenomeles
japonica	japonica	k1gMnSc1	japonica
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Cydonia	Cydonium	k1gNnPc1	Cydonium
maulei	maule	k1gFnSc2	maule
<g/>
)	)	kIx)	)
–	–	k?	–
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
pouze	pouze	k6eAd1	pouze
jednoho	jeden	k4xCgInSc2	jeden
metru	metr	k1gInSc2	metr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
také	také	k9	také
rozložitý	rozložitý	k2eAgMnSc1d1	rozložitý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
má	mít	k5eAaImIp3nS	mít
plstnaté	plstnatý	k2eAgFnSc2d1	plstnatá
větve	větev	k1gFnSc2	větev
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
olysávající	olysávající	k2eAgFnPc1d1	olysávající
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ale	ale	k8xC	ale
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
drsné	drsný	k2eAgInPc1d1	drsný
<g/>
.	.	kIx.	.
</s>
<s>
Listy	lista	k1gFnPc4	lista
má	mít	k5eAaImIp3nS	mít
široce	široko	k6eAd1	široko
vejčité	vejčitý	k2eAgNnSc1d1	vejčité
a	a	k8xC	a
tupé	tupý	k2eAgNnSc1d1	tupé
<g/>
,	,	kIx,	,
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
hrubě	hrubě	k6eAd1	hrubě
pilovité	pilovitý	k2eAgInPc1d1	pilovitý
a	a	k8xC	a
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
lysé	lysý	k2eAgFnPc1d1	Lysá
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
cm	cm	kA	cm
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
<g/>
.	.	kIx.	.
</s>
<s>
Kvete	kvést	k5eAaImIp3nS	kvést
oranžově	oranžově	k6eAd1	oranžově
<g/>
–	–	k?	–
<g/>
červenými	červený	k2eAgInPc7d1	červený
květy	květ	k1gInPc7	květ
jež	jenž	k3xRgInPc1	jenž
jsou	být	k5eAaImIp3nP	být
menší	malý	k2eAgInPc1d2	menší
než	než	k8xS	než
u	u	k7c2	u
předchozího	předchozí	k2eAgInSc2d1	předchozí
druhu	druh	k1gInSc2	druh
(	(	kIx(	(
<g/>
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
asi	asi	k9	asi
tři	tři	k4xCgInPc4	tři
centimetry	centimetr	k1gInPc4	centimetr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
svazečcích	svazeček	k1gInPc6	svazeček
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
až	až	k6eAd1	až
čtyřech	čtyři	k4xCgMnPc6	čtyři
<g/>
.	.	kIx.	.
</s>
<s>
Kvetení	kvetení	k1gNnSc1	kvetení
probíhá	probíhat	k5eAaImIp3nS	probíhat
až	až	k9	až
po	po	k7c6	po
olistění	olistěný	k2eAgMnPc1d1	olistěný
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
hrubě	hrubě	k6eAd1	hrubě
vroubkované	vroubkovaný	k2eAgMnPc4d1	vroubkovaný
široce	široko	k6eAd1	široko
vejčité	vejčitý	k2eAgInPc1d1	vejčitý
listy	list	k1gInPc1	list
<g/>
,	,	kIx,	,
větvičky	větvička	k1gFnPc1	větvička
jsou	být	k5eAaImIp3nP	být
drsné	drsný	k2eAgFnPc1d1	drsná
<g/>
.	.	kIx.	.
</s>
<s>
Malvice	malvice	k1gFnSc1	malvice
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
kulovitá	kulovitý	k2eAgFnSc1d1	kulovitá
<g/>
,	,	kIx,	,
žlutozelená	žlutozelený	k2eAgFnSc1d1	žlutozelená
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgFnPc4	čtyři
až	až	k8xS	až
pět	pět	k4xCc4	pět
centimetrů	centimetr	k1gInPc2	centimetr
široká	široký	k2eAgFnSc1d1	široká
a	a	k8xC	a
vonná	vonný	k2eAgFnSc1d1	vonná
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
bývá	bývat	k5eAaImIp3nS	bývat
pěstován	pěstován	k2eAgInSc1d1	pěstován
pro	pro	k7c4	pro
okrasu	okrasa	k1gFnSc4	okrasa
v	v	k7c6	v
zahradách	zahrada	k1gFnPc6	zahrada
a	a	k8xC	a
parcích	park	k1gInPc6	park
<g/>
,	,	kIx,	,
v	v	k7c6	v
kultivarech	kultivar	k1gInPc6	kultivar
'	'	kIx"	'
<g/>
Dwarf	Dwarf	k1gMnSc1	Dwarf
Poppy	Poppa	k1gFnSc2	Poppa
Red	Red	k1gMnSc1	Red
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
Orange	Orang	k1gFnPc1	Orang
Beauty	Beaut	k2eAgFnPc1d1	Beaut
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
Sargentii	Sargentie	k1gFnSc4	Sargentie
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
často	často	k6eAd1	často
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
'	'	kIx"	'
<g/>
Tricolor	Tricolor	k1gInSc1	Tricolor
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgMnPc1d1	původní
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
a	a	k8xC	a
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
ČR	ČR	kA	ČR
introdukován	introdukovat	k5eAaImNgInS	introdukovat
v	v	k7c6	v
r.	r.	kA	r.
1865	[number]	k4	1865
<g/>
.	.	kIx.	.
<g/>
kdoulovec	kdoulovec	k1gInSc1	kdoulovec
nádherný	nádherný	k2eAgInSc1d1	nádherný
(	(	kIx(	(
<g/>
Chaenomeles	Chaenomeles	k1gInSc1	Chaenomeles
x	x	k?	x
superba	superba	k1gFnSc1	superba
<g/>
)	)	kIx)	)
–	–	k?	–
kdoulovec	kdoulovec	k1gInSc1	kdoulovec
nádherný	nádherný	k2eAgInSc1d1	nádherný
je	být	k5eAaImIp3nS	být
kříženec	kříženec	k1gMnSc1	kříženec
Chaenomeles	Chaenomeles	k1gMnSc1	Chaenomeles
speciosa	speciosa	k1gFnSc1	speciosa
a	a	k8xC	a
Chaenomeles	Chaenomeles	k1gMnSc1	Chaenomeles
japonica	japonica	k1gMnSc1	japonica
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
nese	nést	k5eAaImIp3nS	nést
znaky	znak	k1gInPc4	znak
obou	dva	k4xCgInPc2	dva
druhů	druh	k1gInPc2	druh
(	(	kIx(	(
<g/>
znaky	znak	k1gInPc1	znak
jsou	být	k5eAaImIp3nP	být
přechodové	přechodový	k2eAgInPc1d1	přechodový
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
plném	plný	k2eAgInSc6d1	plný
vzrůstu	vzrůst	k1gInSc6	vzrůst
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
1,5	[number]	k4	1,5
m.	m.	k?	m.
Kvete	kvést	k5eAaImIp3nS	kvést
velkými	velký	k2eAgInPc7d1	velký
jednoduchými	jednoduchý	k2eAgInPc7d1	jednoduchý
květy	květ	k1gInPc7	květ
které	který	k3yRgInPc1	který
podle	podle	k7c2	podle
kultivarů	kultivar	k1gInPc2	kultivar
můžou	můžou	k?	můžou
mít	mít	k5eAaImF	mít
červenou	červený	k2eAgFnSc4d1	červená
<g/>
,	,	kIx,	,
růžovou	růžový	k2eAgFnSc4d1	růžová
<g/>
,	,	kIx,	,
oranžovou	oranžový	k2eAgFnSc4d1	oranžová
nebo	nebo	k8xC	nebo
bílou	bílý	k2eAgFnSc4d1	bílá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Kultivar	kultivar	k1gInSc1	kultivar
'	'	kIx"	'
<g/>
Fascination	Fascination	k1gInSc1	Fascination
<g/>
'	'	kIx"	'
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
rozkladitými	rozkladitý	k2eAgFnPc7d1	rozkladitá
větvemi	větev	k1gFnPc7	větev
<g/>
.	.	kIx.	.
</s>
<s>
Keř	keř	k1gInSc1	keř
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výšky	výška	k1gFnSc2	výška
jednoho	jeden	k4xCgInSc2	jeden
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Kvete	kvést	k5eAaImIp3nS	kvést
růžovo	růžovo	k1gNnSc1	růžovo
<g/>
–	–	k?	–
<g/>
červenými	červený	k2eAgInPc7d1	červený
květy	květ	k1gInPc7	květ
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
shlukuje	shlukovat	k5eAaImIp3nS	shlukovat
do	do	k7c2	do
malých	malý	k2eAgNnPc2d1	malé
květenství	květenství	k1gNnPc2	květenství
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
květonosných	květonosný	k2eAgFnPc6d1	květonosná
větévkách	větévka	k1gFnPc6	větévka
<g/>
.	.	kIx.	.
</s>
<s>
Kultivar	kultivar	k1gInSc1	kultivar
'	'	kIx"	'
<g/>
Jet	jet	k2eAgInSc1d1	jet
Trail	Trail	k1gInSc1	Trail
<g/>
'	'	kIx"	'
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
něj	on	k3xPp3gNnSc2	on
kvete	kvést	k5eAaImIp3nS	kvést
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
letorostu	letorost	k1gInSc2	letorost
<g/>
.	.	kIx.	.
</s>
<s>
Kdoulovec	kdoulovec	k1gInSc1	kdoulovec
nádherný	nádherný	k2eAgInSc1d1	nádherný
se	se	k3xPyFc4	se
celkově	celkově	k6eAd1	celkově
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
nejméně	málo	k6eAd3	málo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Kirschner	Kirschner	k1gMnSc1	Kirschner
J.	J.	kA	J.
<g/>
,	,	kIx,	,
Křísa	křís	k1gMnSc2	křís
B.	B.	kA	B.
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Květena	květena	k1gFnSc1	květena
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
svazek	svazek	k1gInSc4	svazek
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Academia	academia	k1gFnSc1	academia
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
–	–	k?	–
<g/>
200	[number]	k4	200
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
256	[number]	k4	256
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hieke	Hieke	k1gFnSc1	Hieke
K.	K.	kA	K.
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Lexikon	lexikon	k1gInSc1	lexikon
okrasných	okrasný	k2eAgFnPc2d1	okrasná
dřevin	dřevina	k1gFnPc2	dřevina
<g/>
.	.	kIx.	.
</s>
<s>
Helma	helma	k1gFnSc1	helma
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hurych	Hurych	k1gInSc1	Hurych
V.	V.	kA	V.
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Okrasné	okrasný	k2eAgFnPc4d1	okrasná
dřeviny	dřevina	k1gFnPc4	dřevina
pro	pro	k7c4	pro
zahrady	zahrada	k1gFnPc4	zahrada
a	a	k8xC	a
parky	park	k1gInPc4	park
<g/>
.	.	kIx.	.
</s>
<s>
Květ	květ	k1gInSc1	květ
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
ČZS	ČZS	kA	ČZS
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
Těšín	Těšín	k1gInSc1	Těšín
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
–	–	k?	–
<g/>
85362	[number]	k4	85362
<g/>
–	–	k?	–
<g/>
46	[number]	k4	46
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vermeulen	Vermeulen	k2eAgInSc1d1	Vermeulen
N.	N.	kA	N.
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
keřů	keř	k1gInPc2	keř
<g/>
.	.	kIx.	.
</s>
<s>
Rebo	Rebo	k1gNnSc1	Rebo
Productions	Productionsa	k1gFnPc2	Productionsa
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
–	–	k?	–
<g/>
7234	[number]	k4	7234
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Větvička	větvička	k1gFnSc1	větvička
V.	V.	kA	V.
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Stromy	strom	k1gInPc1	strom
a	a	k8xC	a
keře	keř	k1gInPc1	keř
<g/>
.	.	kIx.	.
</s>
<s>
Aventinum	Aventinum	k1gInSc1	Aventinum
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
–	–	k?	–
<g/>
7151	[number]	k4	7151
<g/>
–	–	k?	–
<g/>
133	[number]	k4	133
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kdoulovec	kdoulovec	k1gInSc1	kdoulovec
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Chaenomeles	Chaenomeles	k1gInSc1	Chaenomeles
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
