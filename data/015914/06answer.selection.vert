<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
vyčerpání	vyčerpání	k1gNnSc1
zásob	zásoba	k1gFnPc2
vodíku	vodík	k1gInSc2
na	na	k7c6
Slunci	slunce	k1gNnSc6
je	být	k5eAaImIp3nS
očekáváno	očekávat	k5eAaImNgNnS
až	až	k9
v	v	k7c6
řádu	řád	k1gInSc6
miliard	miliarda	k4xCgFnPc2
let	léto	k1gNnPc2
<g/>
,	,	kIx,
označuje	označovat	k5eAaImIp3nS
se	se	k3xPyFc4
tento	tento	k3xDgInSc1
zdroj	zdroj	k1gInSc1
energie	energie	k1gFnSc2
jako	jako	k8xS,k8xC
obnovitelný	obnovitelný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>